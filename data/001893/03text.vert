<s>
Kinetoplast	Kinetoplast	k1gInSc1	Kinetoplast
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc4	úsek
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
prvoků	prvok	k1gMnPc2	prvok
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Kinetoplastea	Kinetoplaste	k1gInSc2	Kinetoplaste
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
trypanozomy	trypanozoma	k1gFnPc4	trypanozoma
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
evoluční	evoluční	k2eAgFnSc4d1	evoluční
novinku	novinka	k1gFnSc4	novinka
<g/>
,	,	kIx,	,
apomorfii	apomorfie	k1gFnSc4	apomorfie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kinetoplastu	kinetoplast	k1gInSc6	kinetoplast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
DNA	DNA	kA	DNA
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malých	malý	k2eAgInPc2d1	malý
(	(	kIx(	(
<g/>
minikroužků	minikroužek	k1gInPc2	minikroužek
<g/>
)	)	kIx)	)
a	a	k8xC	a
velkých	velký	k2eAgInPc2d1	velký
(	(	kIx(	(
<g/>
maxikroužků	maxikroužek	k1gInPc2	maxikroužek
<g/>
)	)	kIx)	)
kruhových	kruhový	k2eAgFnPc2d1	kruhová
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kinetoplast	kinetoplast	k1gInSc1	kinetoplast
nedaleko	nedaleko	k7c2	nedaleko
bazálního	bazální	k2eAgNnSc2d1	bazální
tělíska	tělísko	k1gNnSc2	tělísko
bičíku	bičík	k1gInSc2	bičík
<g/>
.	.	kIx.	.
</s>
<s>
Maxikroužky	Maxikroužek	k1gInPc1	Maxikroužek
nejsou	být	k5eNaImIp3nP	být
nic	nic	k3yNnSc4	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
mitochondriální	mitochondriální	k2eAgFnSc1d1	mitochondriální
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohem	mnohem	k6eAd1	mnohem
zajímavější	zajímavý	k2eAgFnPc1d2	zajímavější
jsou	být	k5eAaImIp3nP	být
minikroužky	minikroužka	k1gFnPc1	minikroužka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
geny	gen	k1gInPc1	gen
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
guide	guide	k6eAd1	guide
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
gRNA	gRNA	k?	gRNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tzv.	tzv.	kA	tzv.
RNA	RNA	kA	RNA
editing	editing	k1gInSc1	editing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kinetoplastu	kinetoplast	k1gInSc6	kinetoplast
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
maxikroužků	maxikroužek	k1gInPc2	maxikroužek
(	(	kIx(	(
<g/>
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
tisíc	tisíc	k4xCgInSc4	tisíc
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
a	a	k8xC	a
5000	[number]	k4	5000
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
000	[number]	k4	000
minikroužků	minikroužek	k1gInPc2	minikroužek
(	(	kIx(	(
<g/>
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
0,8	[number]	k4	0,8
<g/>
-	-	kIx~	-
<g/>
2,5	[number]	k4	2,5
tisíce	tisíc	k4xCgInSc2	tisíc
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
umístění	umístění	k1gNnSc4	umístění
kinetoplastu	kinetoplast	k1gInSc2	kinetoplast
jsou	být	k5eAaImIp3nP	být
důležitými	důležitý	k2eAgInPc7d1	důležitý
určovacími	určovací	k2eAgInPc7d1	určovací
znaky	znak	k1gInPc7	znak
bičivek	bičivka	k1gFnPc2	bičivka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
i	i	k9	i
během	během	k7c2	během
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
jediného	jediný	k2eAgInSc2d1	jediný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
