<p>
<s>
Devon	devon	k1gInSc1	devon
je	být	k5eAaImIp3nS	být
útvarem	útvar	k1gInSc7	útvar
paleozoika	paleozoikum	k1gNnSc2	paleozoikum
v	v	k7c6	v
nadloží	nadloží	k1gNnSc6	nadloží
siluru	silur	k1gInSc2	silur
a	a	k8xC	a
v	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
karbonu	karbon	k1gInSc2	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Časově	časově	k6eAd1	časově
je	být	k5eAaImIp3nS	být
vymezen	vymezit	k5eAaPmNgInS	vymezit
hranicí	hranice	k1gFnSc7	hranice
416	[number]	k4	416
±	±	k?	±
2,8	[number]	k4	2,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
až	až	k9	až
359,2	[number]	k4	359,2
±	±	k?	±
2,5	[number]	k4	2,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
graptolitové	graptolitový	k2eAgFnSc2d1	graptolitový
zóny	zóna	k1gFnSc2	zóna
Monograptus	Monograptus	k1gInSc4	Monograptus
uniformis	uniformis	k1gFnSc2	uniformis
a	a	k8xC	a
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
stratotypem	stratotyp	k1gInSc7	stratotyp
je	být	k5eAaImIp3nS	být
odkryv	odkryv	k1gInSc1	odkryv
u	u	k7c2	u
Klonku	klonek	k1gInSc2	klonek
u	u	k7c2	u
Suchomast	Suchomast	k1gMnSc1	Suchomast
(	(	kIx(	(
<g/>
usnesením	usnesení	k1gNnSc7	usnesení
24	[number]	k4	24
<g/>
.	.	kIx.	.
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
geologického	geologický	k2eAgInSc2d1	geologický
kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svrchní	svrchní	k2eAgFnSc1d1	svrchní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
horní	horní	k2eAgFnSc7d1	horní
hranicí	hranice	k1gFnSc7	hranice
goniatitové	goniatitový	k2eAgFnSc2d1	goniatitový
zóny	zóna	k1gFnSc2	zóna
Wocklumeria	Wocklumerium	k1gNnSc2	Wocklumerium
(	(	kIx(	(
<g/>
usnesení	usnesení	k1gNnSc1	usnesení
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
MGK	MGK	kA	MGK
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc7d1	klasická
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
hrabství	hrabství	k1gNnSc1	hrabství
Devonshire	Devonshir	k1gInSc5	Devonshir
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
popsán	popsat	k5eAaPmNgInS	popsat
Murchisonem	Murchison	k1gInSc7	Murchison
a	a	k8xC	a
Sedgwickem	Sedgwicko	k1gNnSc7	Sedgwicko
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
devon	devon	k1gInSc1	devon
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
oddělení	oddělení	k1gNnPc4	oddělení
-	-	kIx~	-
spodní	spodní	k2eAgNnSc4d1	spodní
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc4d1	střední
a	a	k8xC	a
svrchní	svrchní	k2eAgNnSc4d1	svrchní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bližší	bližší	k1gNnSc4	bližší
členění	členění	k1gNnSc2	členění
jsou	být	k5eAaImIp3nP	být
klasickými	klasický	k2eAgFnPc7d1	klasická
oblastmi	oblast	k1gFnPc7	oblast
Rýnské	rýnský	k2eAgFnSc2d1	Rýnská
břidličné	břidličný	k2eAgFnSc2d1	Břidličná
pohoří	pohoří	k1gNnSc3	pohoří
a	a	k8xC	a
Barrandien	barrandien	k1gInSc1	barrandien
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
devon	devon	k1gInSc1	devon
také	také	k6eAd1	také
nazýván	nazývat	k5eAaImNgInS	nazývat
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnSc3	Red
podle	podle	k7c2	podle
červených	červený	k2eAgInPc2d1	červený
a	a	k8xC	a
hnědých	hnědý	k2eAgInPc2d1	hnědý
terrestrických	terrestrický	k2eAgInPc2d1	terrestrický
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
známých	známý	k2eAgInPc2d1	známý
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bližší	blízký	k2eAgNnSc1d2	bližší
členění	členění	k1gNnSc1	členění
devonu	devon	k1gInSc2	devon
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
dělení	dělení	k1gNnSc4	dělení
na	na	k7c4	na
stupně	stupeň	k1gInPc4	stupeň
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dvě	dva	k4xCgFnPc1	dva
stupnice	stupnice	k1gFnPc1	stupnice
dle	dle	k7c2	dle
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
terrigenní	terrigenní	k2eAgInSc4d1	terrigenní
vývoj	vývoj	k1gInSc4	vývoj
rýnská	rýnský	k2eAgFnSc1d1	Rýnská
(	(	kIx(	(
<g/>
Rýnské	rýnský	k2eAgNnSc1d1	rýnské
břidličné	břidličný	k2eAgNnSc1d1	Břidličné
pohoří	pohoří	k1gNnSc1	pohoří
<g/>
,	,	kIx,	,
Ardeny	Ardeny	k1gFnPc1	Ardeny
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pelagický	pelagický	k2eAgInSc4d1	pelagický
vývoj	vývoj	k1gInSc4	vývoj
česká	český	k2eAgFnSc1d1	Česká
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgFnSc1d1	spodní
devon	devon	k1gInSc4	devon
Barrandienu	barrandien	k1gInSc2	barrandien
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
do	do	k7c2	do
zón	zóna	k1gFnPc2	zóna
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
goniatitové	goniatitový	k2eAgFnSc6d1	goniatitový
<g/>
,	,	kIx,	,
konodontové	konodontový	k2eAgFnSc6d1	konodontový
a	a	k8xC	a
tentakulitové	tentakulitový	k2eAgFnSc6d1	tentakulitový
fauně	fauna	k1gFnSc6	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bližší	blízký	k2eAgNnSc4d2	bližší
stratigrafické	stratigrafický	k2eAgNnSc4d1	stratigrafické
členění	členění	k1gNnSc4	členění
se	se	k3xPyFc4	se
ve	v	k7c6	v
spodním	spodní	k2eAgInSc6d1	spodní
devonu	devon	k1gInSc6	devon
používají	používat	k5eAaImIp3nP	používat
ještě	ještě	k9	ještě
graptoliti	graptolit	k1gMnPc1	graptolit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
a	a	k8xC	a
svrchním	svrchní	k2eAgInSc6d1	svrchní
devonu	devon	k1gInSc6	devon
ostrakodi	ostrakod	k1gMnPc1	ostrakod
a	a	k8xC	a
foraminifery	foraminifera	k1gFnPc1	foraminifera
<g/>
,	,	kIx,	,
v	v	k7c6	v
mělkovodních	mělkovodní	k2eAgFnPc6d1	mělkovodní
faciích	facie	k1gFnPc6	facie
brachiopodi	brachiopod	k1gMnPc5	brachiopod
<g/>
,	,	kIx,	,
v	v	k7c6	v
kontinentálních	kontinentální	k2eAgFnPc6d1	kontinentální
faciích	facie	k1gFnPc6	facie
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnSc2	Red
ryby	ryba	k1gFnSc2	ryba
a	a	k8xC	a
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
spóry	spóra	k1gFnSc2	spóra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Paleogeografie	paleogeografie	k1gFnSc2	paleogeografie
==	==	k?	==
</s>
</p>
<p>
<s>
Bloky	blok	k1gInPc1	blok
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Gondwany	Gondwana	k1gFnSc2	Gondwana
se	se	k3xPyFc4	se
posunovaly	posunovat	k5eAaImAgFnP	posunovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
,	,	kIx,	,
k	k	k7c3	k
severní	severní	k2eAgFnSc3d1	severní
Evropě	Evropa	k1gFnSc3	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc3d1	severní
Americe	Amerika	k1gFnSc3	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kaledonském	kaledonský	k2eAgNnSc6d1	kaledonské
vrásnění	vrásnění	k1gNnSc6	vrásnění
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kratonizaci	kratonizace	k1gFnSc3	kratonizace
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
severoatlantický	severoatlantický	k2eAgInSc1d1	severoatlantický
(	(	kIx(	(
<g/>
old-redský	oldedský	k2eAgInSc1d1	old-redský
<g/>
)	)	kIx)	)
kontinent	kontinent	k1gInSc1	kontinent
(	(	kIx(	(
<g/>
Euramerika	Euramerika	k1gFnSc1	Euramerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stáčel	stáčet	k5eAaImAgMnS	stáčet
do	do	k7c2	do
suchého	suchý	k2eAgNnSc2d1	suché
pásma	pásmo	k1gNnSc2	pásmo
podél	podél	k7c2	podél
obratníku	obratník	k1gInSc2	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
formovaly	formovat	k5eAaImAgInP	formovat
pískovce	pískovec	k1gInPc1	pískovec
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnPc2	Red
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
zabarvení	zabarvení	k1gNnSc4	zabarvení
způsobovaly	způsobovat	k5eAaImAgInP	způsobovat
oxidy	oxid	k1gInPc1	oxid
železa	železo	k1gNnSc2	železo
(	(	kIx(	(
<g/>
hematit	hematit	k1gInSc1	hematit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paleontologické	paleontologický	k2eAgInPc1d1	paleontologický
a	a	k8xC	a
litologické	litologický	k2eAgInPc1d1	litologický
indikátory	indikátor	k1gInPc1	indikátor
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
útesy	útes	k1gInPc1	útes
<g/>
,	,	kIx,	,
karbonátická	karbonátický	k2eAgFnSc1d1	karbonátický
facie	facie	k1gFnSc1	facie
<g/>
)	)	kIx)	)
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
bloky	blok	k1gInPc1	blok
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
v	v	k7c6	v
teplé	teplý	k2eAgFnSc6d1	teplá
klimatické	klimatický	k2eAgFnSc6d1	klimatická
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Ložiska	ložisko	k1gNnPc1	ložisko
evaporitů	evaporit	k1gInPc2	evaporit
a	a	k8xC	a
sedimenty	sediment	k1gInPc1	sediment
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnSc7	Red
dokládají	dokládat	k5eAaImIp3nP	dokládat
aridní	aridní	k2eAgNnSc4d1	aridní
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
devonu	devon	k1gInSc6	devon
se	se	k3xPyFc4	se
ochladilo	ochladit	k5eAaPmAgNnS	ochladit
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
nápadná	nápadný	k2eAgFnSc1d1	nápadná
změna	změna	k1gFnSc1	změna
fauny	fauna	k1gFnSc2	fauna
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
frasn-famen	frasnamen	k1gInSc4	frasn-famen
v	v	k7c6	v
sedimentech	sediment	k1gInPc6	sediment
tzv.	tzv.	kA	tzv.
malvinokaffrické	malvinokaffrický	k2eAgFnSc2d1	malvinokaffrický
provincie	provincie	k1gFnSc2	provincie
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chybí	chybět	k5eAaImIp3nP	chybět
karbonátické	karbonátický	k2eAgFnPc1d1	karbonátický
uloženiny	uloženina	k1gFnPc1	uloženina
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
má	mít	k5eAaImIp3nS	mít
ráz	ráz	k1gInSc4	ráz
chladných	chladný	k2eAgFnPc2d1	chladná
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
faciálního	faciální	k2eAgInSc2d1	faciální
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
provincie	provincie	k1gFnPc1	provincie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
český	český	k2eAgInSc1d1	český
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
pelagické	pelagický	k2eAgFnSc6d1	pelagická
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
bentická	bentický	k2eAgFnSc1d1	bentická
<g/>
,	,	kIx,	,
planktonická	planktonický	k2eAgFnSc1d1	planktonický
a	a	k8xC	a
nektonická	ktonický	k2eNgFnSc1d1	ktonický
fauna	fauna	k1gFnSc1	fauna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rýnský	rýnský	k2eAgInSc1d1	rýnský
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
příbřežní	příbřežní	k2eAgFnSc2d1	příbřežní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
snos	snos	k1gInSc1	snos
terrigenního	terrigenní	k2eAgInSc2d1	terrigenní
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
bentická	bentický	k2eAgFnSc1d1	bentická
fauna	fauna	k1gFnSc1	fauna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vývoj	vývoj	k1gInSc1	vývoj
typu	typ	k1gInSc2	typ
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnPc2	Red
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
<g/>
,	,	kIx,	,
lagunární	lagunární	k2eAgFnSc1d1	lagunární
klastika	klastika	k1gFnSc1	klastika
<g/>
,	,	kIx,	,
evapority	evaporit	k1gInPc1	evaporit
<g/>
,	,	kIx,	,
rybí	rybí	k2eAgFnSc1d1	rybí
fauna	fauna	k1gFnSc1	fauna
<g/>
,	,	kIx,	,
časté	častý	k2eAgInPc1d1	častý
zbytky	zbytek	k1gInPc1	zbytek
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
Podle	podle	k7c2	podle
brachiopodů	brachiopod	k1gMnPc2	brachiopod
a	a	k8xC	a
trilobitů	trilobit	k1gMnPc2	trilobit
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
provincie	provincie	k1gFnSc2	provincie
Starého	Starého	k2eAgInSc2d1	Starého
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
apalačská	apalačský	k2eAgFnSc1d1	apalačský
<g/>
,	,	kIx,	,
kordilerská	kordilerský	k2eAgFnSc1d1	kordilerský
<g/>
,	,	kIx,	,
tasmánsko-novozélandská	tasmánskoovozélandský	k2eAgFnSc1d1	tasmánsko-novozélandský
<g/>
,	,	kIx,	,
malvinokaffrická	malvinokaffrický	k2eAgFnSc1d1	malvinokaffrický
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
devonu	devon	k1gInSc2	devon
se	se	k3xPyFc4	se
však	však	k9	však
faciální	faciální	k2eAgInPc1d1	faciální
rozdíly	rozdíl	k1gInPc1	rozdíl
stírají	stírat	k5eAaImIp3nP	stírat
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
nabývá	nabývat	k5eAaImIp3nS	nabývat
celosvětově	celosvětově	k6eAd1	celosvětově
obdobného	obdobný	k2eAgInSc2d1	obdobný
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
nejspíše	nejspíše	k9	nejspíše
počínající	počínající	k2eAgFnSc1d1	počínající
transgrese	transgrese	k1gFnSc1	transgrese
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
v	v	k7c6	v
givetu	givet	k1gInSc6	givet
a	a	k8xC	a
frasnu	frasnout	k5eAaPmIp1nS	frasnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
frasn-famen	frasnamen	k1gInSc1	frasn-famen
mizí	mizet	k5eAaImIp3nP	mizet
mělkovodní	mělkovodní	k2eAgFnPc4d1	mělkovodní
facie	facie	k1gFnPc4	facie
a	a	k8xC	a
útesy	útes	k1gInPc4	útes
<g/>
,	,	kIx,	,
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
se	se	k3xPyFc4	se
pelagická	pelagický	k2eAgFnSc1d1	pelagická
fauna	fauna	k1gFnSc1	fauna
jako	jako	k8xS	jako
důsledek	důsledek	k1gInSc1	důsledek
ochlazení	ochlazení	k1gNnSc2	ochlazení
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vysvětlujících	vysvětlující	k2eAgFnPc2d1	vysvětlující
teorií	teorie	k1gFnPc2	teorie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
ochlazení	ochlazení	k1gNnSc1	ochlazení
vlivem	vlivem	k7c2	vlivem
značného	značný	k2eAgNnSc2d1	značné
rozšíření	rozšíření	k1gNnSc2	rozšíření
flóry	flóra	k1gFnSc2	flóra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
snížily	snížit	k5eAaPmAgFnP	snížit
obsah	obsah	k1gInSc4	obsah
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
<g/>
"	"	kIx"	"
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Finálním	finální	k2eAgInSc7d1	finální
dopadem	dopad	k1gInSc7	dopad
je	být	k5eAaImIp3nS	být
masivní	masivní	k2eAgNnSc4d1	masivní
vymírání	vymírání	k1gNnSc4	vymírání
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
v	v	k7c6	v
devonu	devon	k1gInSc6	devon
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Devonská	devonský	k2eAgFnSc1d1	Devonská
fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
silurských	silurský	k2eAgInPc2d1	silurský
rodů	rod	k1gInPc2	rod
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
plynule	plynule	k6eAd1	plynule
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
i	i	k8xC	i
v	v	k7c6	v
devonu	devon	k1gInSc6	devon
<g/>
.	.	kIx.	.
</s>
<s>
Přežívají	přežívat	k5eAaImIp3nP	přežívat
graptoliti	graptolit	k1gMnPc1	graptolit
<g/>
,	,	kIx,	,
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
však	však	k9	však
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
jediným	jediný	k2eAgInSc7d1	jediný
rodem	rod	k1gInSc7	rod
Monograptus	Monograptus	k1gInSc1	Monograptus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pragu	prag	k1gInSc2	prag
však	však	k9	však
definitivně	definitivně	k6eAd1	definitivně
vymírají	vymírat	k5eAaImIp3nP	vymírat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
měkkýšů	měkkýš	k1gMnPc2	měkkýš
jsou	být	k5eAaImIp3nP	být
nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
goniatiti	goniatit	k5eAaPmF	goniatit
<g/>
,	,	kIx,	,
drobní	drobný	k2eAgMnPc1d1	drobný
primitivní	primitivní	k2eAgMnPc1d1	primitivní
mořští	mořský	k2eAgMnPc1d1	mořský
amonoidní	amonoidní	k2eAgMnPc1d1	amonoidní
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
lalokovitými	lalokovitý	k2eAgFnPc7d1	lalokovitý
přepážkami	přepážka	k1gFnPc7	přepážka
ve	v	k7c6	v
schránkách	schránka	k1gFnPc6	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Hojní	hojný	k2eAgMnPc1d1	hojný
jsou	být	k5eAaImIp3nP	být
mlži	mlž	k1gMnPc1	mlž
(	(	kIx(	(
<g/>
Grammysia	Grammysia	k1gFnSc1	Grammysia
<g/>
,	,	kIx,	,
Palaeosolen	Palaeosolen	k2eAgInSc1d1	Palaeosolen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
sladkovodních	sladkovodní	k2eAgFnPc2d1	sladkovodní
forem	forma	k1gFnPc2	forma
z	z	k7c2	z
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnSc2	Red
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
biostratigraficky	biostratigraficky	k6eAd1	biostratigraficky
významné	významný	k2eAgFnPc4d1	významná
fosílie	fosílie	k1gFnPc4	fosílie
-	-	kIx~	-
tentakuliti	tentakulit	k5eAaPmF	tentakulit
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
podrobnější	podrobný	k2eAgNnSc4d2	podrobnější
členění	členění	k1gNnSc4	členění
od	od	k7c2	od
lochkova	lochkov	k1gInSc2	lochkov
do	do	k7c2	do
frasnu	frasnout	k5eAaPmIp1nS	frasnout
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
schránky	schránka	k1gFnPc1	schránka
jsou	být	k5eAaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
v	v	k7c6	v
mořských	mořský	k2eAgFnPc6d1	mořská
pelagických	pelagický	k2eAgFnPc6d1	pelagická
usazeninách	usazenina	k1gFnPc6	usazenina
(	(	kIx(	(
<g/>
Nowakia	Nowakia	k1gFnSc1	Nowakia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc3	vrchol
rozvoje	rozvoj	k1gInSc2	rozvoj
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ramenonožci	ramenonožec	k1gMnPc1	ramenonožec
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgMnSc1d1	obývající
zejména	zejména	k9	zejména
mělkovodní	mělkovodní	k2eAgNnSc1d1	mělkovodní
šelfové	šelfový	k2eAgNnSc1d1	šelfové
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
Hysterolites	Hysterolites	k1gMnSc1	Hysterolites
<g/>
,	,	kIx,	,
Acrospirifer	Acrospirifer	k1gMnSc1	Acrospirifer
spodního	spodní	k2eAgInSc2d1	spodní
devonu	devon	k1gInSc2	devon
rýnského	rýnský	k2eAgInSc2d1	rýnský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
Stringocephalus	Stringocephalus	k1gMnSc1	Stringocephalus
<g/>
,	,	kIx,	,
Zdimir	Zdimir	k1gMnSc1	Zdimir
v	v	k7c6	v
karbonátických	karbonátický	k2eAgFnPc6d1	karbonátický
faciích	facie	k1gFnPc6	facie
středního	střední	k2eAgInSc2d1	střední
devonu	devon	k1gInSc2	devon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vápnité	vápnitý	k2eAgFnPc1d1	vápnitá
kolonie	kolonie	k1gFnPc1	kolonie
stromatoporoideí	stromatoporoide	k1gFnPc2	stromatoporoide
(	(	kIx(	(
<g/>
Amphipora	Amphipora	k1gFnSc1	Amphipora
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
s	s	k7c7	s
korály	korál	k1gMnPc7	korál
a	a	k8xC	a
vápnitými	vápnitý	k2eAgFnPc7d1	vápnitá
řasami	řasa	k1gFnPc7	řasa
vytvářely	vytvářet	k5eAaImAgFnP	vytvářet
mohutné	mohutný	k2eAgInPc4d1	mohutný
útesy	útes	k1gInPc4	útes
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
devonského	devonský	k2eAgNnSc2d1	devonské
pobřeží	pobřeží	k1gNnSc2	pobřeží
australského	australský	k2eAgInSc2d1	australský
bloku	blok	k1gInSc2	blok
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
bariérový	bariérový	k2eAgInSc4d1	bariérový
útes	útes	k1gInSc4	útes
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
doložený	doložený	k2eAgMnSc1d1	doložený
v	v	k7c6	v
pánvi	pánev	k1gFnSc6	pánev
Kimberley	Kimberlea	k1gFnSc2	Kimberlea
na	na	k7c4	na
sz.	sz.	k?	sz.
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počínaje	počínaje	k7c7	počínaje
středním	střední	k2eAgInSc7d1	střední
devonem	devon	k1gInSc7	devon
začínají	začínat	k5eAaImIp3nP	začínat
vymírat	vymírat	k5eAaImF	vymírat
četné	četný	k2eAgFnPc1d1	četná
čeledi	čeleď	k1gFnPc1	čeleď
trilobitů	trilobit	k1gMnPc2	trilobit
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
nadále	nadále	k6eAd1	nadále
trvá	trvat	k5eAaImIp3nS	trvat
u	u	k7c2	u
řádu	řád	k1gInSc2	řád
Phacopida	Phacopida	k1gFnSc1	Phacopida
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Phacops	Phacopsa	k1gFnPc2	Phacopsa
<g/>
,	,	kIx,	,
Reedops	Reedopsa	k1gFnPc2	Reedopsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
čeledi	čeleď	k1gFnSc2	čeleď
Damanitidae	Damanitida	k1gInSc2	Damanitida
(	(	kIx(	(
<g/>
Odontochile	Odontochila	k1gFnSc6	Odontochila
<g/>
,	,	kIx,	,
Asteropyge	Asteropyge	k1gFnSc6	Asteropyge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
karbonem	karbon	k1gInSc7	karbon
řád	řád	k1gInSc1	řád
Phacopida	Phacopid	k1gMnSc2	Phacopid
vymírá	vymírat	k5eAaImIp3nS	vymírat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vedoucím	vedoucí	k2eAgFnPc3d1	vedoucí
fosíliím	fosílie	k1gFnPc3	fosílie
patří	patřit	k5eAaImIp3nS	patřit
drobní	drobný	k2eAgMnPc1d1	drobný
zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
Proetidae	Proetida	k1gFnSc2	Proetida
<g/>
,	,	kIx,	,
sledovatelní	sledovatelný	k2eAgMnPc1d1	sledovatelný
až	až	k9	až
do	do	k7c2	do
karbonu	karbon	k1gInSc2	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
členovců	členovec	k1gMnPc2	členovec
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
lagunárních	lagunární	k2eAgFnPc6d1	lagunární
faciích	facie	k1gFnPc6	facie
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnSc2	Red
časté	častý	k2eAgInPc1d1	častý
nálezy	nález	k1gInPc1	nález
zástupců	zástupce	k1gMnPc2	zástupce
řádu	řád	k1gInSc2	řád
Eurypteridae	Eurypteridae	k1gFnPc2	Eurypteridae
<g/>
,	,	kIx,	,
ze	z	k7c2	z
suchozemských	suchozemský	k2eAgInPc2d1	suchozemský
rodů	rod	k1gInPc2	rod
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
hmyz	hmyz	k1gInSc1	hmyz
(	(	kIx(	(
<g/>
Rhyniella	Rhyniella	k1gFnSc1	Rhyniella
<g/>
)	)	kIx)	)
a	a	k8xC	a
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horninotvorné	horninotvorný	k2eAgInPc1d1	horninotvorný
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc1	zbytek
(	(	kIx(	(
<g/>
články	článek	k1gInPc1	článek
<g/>
)	)	kIx)	)
lilijic	lilijice	k1gFnPc2	lilijice
(	(	kIx(	(
<g/>
krinoidové	krinoidový	k2eAgInPc4d1	krinoidový
vápence	vápenec	k1gInPc4	vápenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1	vzácný
jsou	být	k5eAaImIp3nP	být
nálezy	nález	k1gInPc1	nález
mořských	mořský	k2eAgFnPc2d1	mořská
hvězdic	hvězdice	k1gFnPc2	hvězdice
a	a	k8xC	a
hadic	hadice	k1gFnPc2	hadice
<g/>
.	.	kIx.	.
</s>
<s>
Vymírají	vymírat	k5eAaImIp3nP	vymírat
zástupci	zástupce	k1gMnPc7	zástupce
rodu	rod	k1gInSc2	rod
Cystoidea	Cystoide	k1gInSc2	Cystoide
a	a	k8xC	a
Carpoidea	Carpoide	k1gInSc2	Carpoide
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
devonskými	devonský	k2eAgMnPc7d1	devonský
obratlovci	obratlovec	k1gMnPc7	obratlovec
jsou	být	k5eAaImIp3nP	být
nejdůležitějšími	důležitý	k2eAgMnPc7d3	nejdůležitější
zástupci	zástupce	k1gMnPc7	zástupce
ryby	ryba	k1gFnSc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
rozvoj	rozvoj	k1gInSc1	rozvoj
umožnil	umožnit	k5eAaPmAgInS	umožnit
vznik	vznik	k1gInSc4	vznik
rozsáhlých	rozsáhlý	k2eAgNnPc2d1	rozsáhlé
teplých	teplý	k2eAgNnPc2d1	teplé
moří	moře	k1gNnPc2	moře
s	s	k7c7	s
hojnými	hojný	k2eAgInPc7d1	hojný
zálivy	záliv	k1gInPc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Bezčelistnaté	Bezčelistnatý	k2eAgFnPc1d1	Bezčelistnatý
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
Agnatha	Agnatha	k1gFnSc1	Agnatha
<g/>
)	)	kIx)	)
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
ve	v	k7c6	v
spodním	spodní	k2eAgInSc6d1	spodní
devonu	devon	k1gInSc6	devon
svého	svůj	k3xOyFgInSc2	svůj
maximálního	maximální	k2eAgInSc2d1	maximální
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
však	však	k9	však
vymírají	vymírat	k5eAaImIp3nP	vymírat
a	a	k8xC	a
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
místo	místo	k1gNnSc4	místo
vývojově	vývojově	k6eAd1	vývojově
vyšším	vysoký	k2eAgInPc3d2	vyšší
druhům	druh	k1gInPc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
pancéřnaté	pancéřnatý	k2eAgFnPc1d1	pancéřnatý
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
Placodermi	Placoder	k1gFnPc7	Placoder
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
již	již	k9	již
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
obě	dva	k4xCgFnPc1	dva
čelisti	čelist	k1gFnPc1	čelist
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
zkostnatělou	zkostnatělý	k2eAgFnSc4d1	zkostnatělá
páteř	páteř	k1gFnSc4	páteř
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
pancíře	pancíř	k1gInSc2	pancíř
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
hrudní	hrudní	k2eAgFnSc4d1	hrudní
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
skupiny	skupina	k1gFnSc2	skupina
Antiarchi	Antiarch	k1gFnSc2	Antiarch
byly	být	k5eAaImAgInP	být
hojnými	hojný	k2eAgMnPc7d1	hojný
obyvateli	obyvatel	k1gMnPc7	obyvatel
dna	dno	k1gNnSc2	dno
moří	mořit	k5eAaImIp3nP	mořit
<g/>
.	.	kIx.	.
</s>
<s>
Arthrodira	Arthrodira	k1gFnSc1	Arthrodira
(	(	kIx(	(
<g/>
kloubnatci	kloubnatec	k1gMnPc1	kloubnatec
<g/>
)	)	kIx)	)
s	s	k7c7	s
nápadně	nápadně	k6eAd1	nápadně
velkou	velký	k2eAgFnSc7d1	velká
a	a	k8xC	a
širokou	široký	k2eAgFnSc7d1	široká
lebkou	lebka	k1gFnSc7	lebka
měly	mít	k5eAaImAgFnP	mít
hlavový	hlavový	k2eAgInSc1d1	hlavový
a	a	k8xC	a
trupový	trupový	k2eAgInSc1d1	trupový
pancíř	pancíř	k1gInSc1	pancíř
spojen	spojit	k5eAaPmNgInS	spojit
párovým	párový	k2eAgInSc7d1	párový
kloubem	kloub	k1gInSc7	kloub
(	(	kIx(	(
<g/>
nálezy	nález	k1gInPc4	nález
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hojný	hojný	k2eAgInSc1d1	hojný
Coccosteus	Coccosteus	k1gInSc1	Coccosteus
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
30	[number]	k4	30
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
Dunkleosteus	Dunkleosteus	k1gMnSc1	Dunkleosteus
dorůstal	dorůstat	k5eAaImAgMnS	dorůstat
délky	délka	k1gFnPc4	délka
až	až	k9	až
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
devonu	devon	k1gInSc2	devon
zástupci	zástupce	k1gMnPc7	zástupce
Arthrodira	Arthrodiro	k1gNnSc2	Arthrodiro
vymírají	vymírat	k5eAaImIp3nP	vymírat
<g/>
.	.	kIx.	.
</s>
<s>
Trnoploutví	Trnoploutvět	k5eAaImIp3nS	Trnoploutvět
(	(	kIx(	(
<g/>
Acanthodii	Acanthodie	k1gFnSc3	Acanthodie
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
drobné	drobný	k2eAgInPc1d1	drobný
<g/>
,	,	kIx,	,
10-30	[number]	k4	10-30
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
rybky	rybka	k1gFnSc2	rybka
<g/>
,	,	kIx,	,
tělo	tělo	k1gNnSc4	tělo
měly	mít	k5eAaImAgInP	mít
kryté	krytý	k2eAgInPc1d1	krytý
pancířem	pancíř	k1gInSc7	pancíř
<g/>
,	,	kIx,	,
tvořeným	tvořený	k2eAgInSc7d1	tvořený
silnými	silný	k2eAgFnPc7d1	silná
šupinami	šupina	k1gFnPc7	šupina
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
pokrývaly	pokrývat	k5eAaImAgFnP	pokrývat
drobné	drobný	k2eAgFnPc1d1	drobná
kostěné	kostěný	k2eAgFnPc1d1	kostěná
destičky	destička	k1gFnPc1	destička
<g/>
.	.	kIx.	.
</s>
<s>
Obývaly	obývat	k5eAaImAgFnP	obývat
sladké	sladký	k2eAgFnPc1d1	sladká
i	i	k8xC	i
slané	slaný	k2eAgFnPc1d1	slaná
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
ocasních	ocasní	k2eAgFnPc2d1	ocasní
ploutví	ploutev	k1gFnPc2	ploutev
měly	mít	k5eAaImAgFnP	mít
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
vyztuženy	vyztužen	k2eAgMnPc4d1	vyztužen
dentinovým	dentinový	k2eAgInSc7d1	dentinový
trnem	trn	k1gInSc7	trn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zachoval	zachovat	k5eAaPmAgMnS	zachovat
jako	jako	k9	jako
fosílie	fosílie	k1gFnPc4	fosílie
<g/>
.	.	kIx.	.
</s>
<s>
Kostnaté	kostnatý	k2eAgFnPc1d1	kostnatá
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
Osteichthyes	Osteichthyes	k1gInSc1	Osteichthyes
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
skupiny	skupina	k1gFnPc4	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ryby	ryba	k1gFnPc1	ryba
paprskoploutvé	paprskoploutvá	k1gFnPc1	paprskoploutvá
(	(	kIx(	(
<g/>
Actinopterygii	Actinopterygie	k1gFnSc4	Actinopterygie
<g/>
)	)	kIx)	)
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
představují	představovat	k5eAaImIp3nP	představovat
jejich	jejich	k3xOp3gMnPc1	jejich
následníci	následník	k1gMnPc1	následník
asi	asi	k9	asi
90	[number]	k4	90
<g/>
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
devonu	devon	k1gInSc6	devon
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
zástupci	zástupce	k1gMnPc1	zástupce
řádu	řád	k1gInSc2	řád
Palaeomisciformes	Palaeomisciformes	k1gInSc1	Palaeomisciformes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ryby	ryba	k1gFnPc1	ryba
dvojdyšné	dvojdyšný	k2eAgFnSc2d1	dvojdyšný
(	(	kIx(	(
<g/>
Dipnoi	Dipno	k1gFnSc2	Dipno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ryby	ryba	k1gFnPc1	ryba
lalokoploutvé	lalokoploutvý	k2eAgFnPc1d1	lalokoploutvá
(	(	kIx(	(
<g/>
Crossopterygii	Crossopterygie	k1gFnSc6	Crossopterygie
<g/>
)	)	kIx)	)
-	-	kIx~	-
nadřád	nadřád	k1gInSc1	nadřád
Rhipidistia	Rhipidistium	k1gNnSc2	Rhipidistium
<g/>
,	,	kIx,	,
řády	řád	k1gInPc4	řád
Porolepiformes	Porolepiformesa	k1gFnPc2	Porolepiformesa
<g/>
,	,	kIx,	,
Osteolepiformes	Osteolepiformes	k1gMnSc1	Osteolepiformes
představuje	představovat	k5eAaImIp3nS	představovat
předchůdce	předchůdce	k1gMnSc1	předchůdce
všech	všecek	k3xTgFnPc2	všecek
skupin	skupina	k1gFnPc2	skupina
vyšších	vysoký	k2eAgMnPc2d2	vyšší
obratlovců	obratlovec	k1gMnPc2	obratlovec
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
rod	rod	k1gInSc1	rod
Eusthenopteron	Eusthenopteron	k1gInSc1	Eusthenopteron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
ulovena	uloven	k2eAgFnSc1d1	ulovena
první	první	k4xOgFnSc1	první
"	"	kIx"	"
<g/>
živoucí	živoucí	k2eAgFnSc1d1	živoucí
fosílie	fosílie	k1gFnSc1	fosílie
<g/>
"	"	kIx"	"
-	-	kIx~	-
latimérie	latimérie	k1gFnSc1	latimérie
podivná	podivný	k2eAgFnSc1d1	podivná
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Latimeria	Latimerium	k1gNnPc1	Latimerium
chalumnae	chalumnae	k1gFnPc2	chalumnae
<g/>
.	.	kIx.	.
<g/>
Konodonti	Konodont	k1gMnPc1	Konodont
<g/>
,	,	kIx,	,
zástupci	zástupce	k1gMnPc1	zástupce
problematické	problematický	k2eAgFnSc2d1	problematická
příslušnosti	příslušnost	k1gFnSc2	příslušnost
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
prvořadý	prvořadý	k2eAgInSc4d1	prvořadý
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
biostratigrafii	biostratigrafie	k1gFnSc4	biostratigrafie
devonu	devon	k1gInSc2	devon
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
vývoj	vývoj	k1gInSc1	vývoj
kousacího	kousací	k2eAgInSc2d1	kousací
aparátu	aparát	k1gInSc2	aparát
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vymezit	vymezit	k5eAaPmF	vymezit
množství	množství	k1gNnSc4	množství
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
mezikontinentální	mezikontinentální	k2eAgFnSc7d1	mezikontinentální
platností	platnost	k1gFnSc7	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Opotřebování	opotřebování	k1gNnSc1	opotřebování
zoubků	zoubek	k1gInPc2	zoubek
je	být	k5eAaImIp3nS	být
důkazem	důkaz	k1gInSc7	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
živočichové	živočich	k1gMnPc1	živočich
nebyli	být	k5eNaImAgMnP	být
jen	jen	k6eAd1	jen
pasivními	pasivní	k2eAgMnPc7d1	pasivní
příjemci	příjemce	k1gMnPc7	příjemce
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sami	sám	k3xTgMnPc1	sám
byli	být	k5eAaImAgMnP	být
aktivními	aktivní	k2eAgMnPc7d1	aktivní
lovci	lovec	k1gMnPc7	lovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Grónsku	Grónsko	k1gNnSc6	Grónsko
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
zbytky	zbytek	k1gInPc1	zbytek
prvních	první	k4xOgMnPc2	první
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
primitivní	primitivní	k2eAgMnPc4d1	primitivní
obojživelníky	obojživelník	k1gMnPc4	obojživelník
<g/>
,	,	kIx,	,
krytolebce	krytolebec	k1gMnSc4	krytolebec
čeledi	čeleď	k1gFnSc2	čeleď
Ichthyostegidae	Ichthyostegida	k1gFnSc2	Ichthyostegida
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
90	[number]	k4	90
cm	cm	kA	cm
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
vodní	vodní	k2eAgMnSc1d1	vodní
tvor	tvor	k1gMnSc1	tvor
měl	mít	k5eAaImAgMnS	mít
již	již	k6eAd1	již
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc4d1	umožňující
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
nálezy	nález	k1gInPc1	nález
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
stopy	stopa	k1gFnSc2	stopa
tohoto	tento	k3xDgMnSc2	tento
tvora	tvor	k1gMnSc2	tvor
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Devonská	devonský	k2eAgFnSc1d1	Devonská
flóra	flóra	k1gFnSc1	flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Charakteristický	charakteristický	k2eAgMnSc1d1	charakteristický
pro	pro	k7c4	pro
devon	devon	k1gInSc4	devon
je	být	k5eAaImIp3nS	být
vývoj	vývoj	k1gInSc1	vývoj
vyšších	vysoký	k2eAgFnPc2d2	vyšší
suchozemských	suchozemský	k2eAgFnPc2d1	suchozemská
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c2	mnoho
místech	místo	k1gNnPc6	místo
vznikaly	vznikat	k5eAaImAgFnP	vznikat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
úrodné	úrodný	k2eAgFnSc2d1	úrodná
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgInSc1d1	umožňující
rozmach	rozmach	k1gInSc1	rozmach
nových	nový	k2eAgInPc2d1	nový
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Třída	třída	k1gFnSc1	třída
Psilopsida	Psilopsida	k1gFnSc1	Psilopsida
(	(	kIx(	(
<g/>
výtrusné	výtrusný	k2eAgFnPc1d1	výtrusná
cévnaté	cévnatý	k2eAgFnPc1d1	cévnatá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
spodním-středním	spodnímtřední	k2eAgInSc6d1	spodním-střední
devonu	devon	k1gInSc6	devon
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
nízkého	nízký	k2eAgInSc2d1	nízký
vzrůstu	vzrůst	k1gInSc2	vzrůst
s	s	k7c7	s
malými	malý	k2eAgInPc7d1	malý
<g/>
,	,	kIx,	,
trnům	trn	k1gInPc3	trn
podobnými	podobný	k2eAgInPc7d1	podobný
listy	list	k1gInPc7	list
(	(	kIx(	(
<g/>
Psilophyton	Psilophyton	k1gInSc1	Psilophyton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
předchůdci	předchůdce	k1gMnPc1	předchůdce
primitivních	primitivní	k2eAgFnPc2d1	primitivní
plavuňovitých	plavuňovitý	k2eAgFnPc2d1	plavuňovitý
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
Lycophyta	Lycophyta	k1gMnSc1	Lycophyta
<g/>
,	,	kIx,	,
Protolepidodendron	Protolepidodendron	k1gMnSc1	Protolepidodendron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
spodním	spodní	k2eAgInSc6d1	spodní
devonu	devon	k1gInSc6	devon
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
předci	předek	k1gMnPc1	předek
nahosemenných	nahosemenný	k2eAgFnPc2d1	nahosemenná
rostlin	rostlina	k1gFnPc2	rostlina
-	-	kIx~	-
Progymnospermoipsida	Progymnospermoipsida	k1gFnSc1	Progymnospermoipsida
(	(	kIx(	(
<g/>
Protopteridium	Protopteridium	k1gNnSc1	Protopteridium
<g/>
,	,	kIx,	,
Pseudosporochnus	Pseudosporochnus	k1gInSc1	Pseudosporochnus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
středního	střední	k2eAgInSc2d1	střední
devonu	devon	k1gInSc2	devon
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
přesličkovité	přesličkovitý	k2eAgFnPc1d1	přesličkovitý
a	a	k8xC	a
plavuňovité	plavuňovitý	k2eAgFnPc1d1	plavuňovitý
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
koncem	koncem	k7c2	koncem
devonu	devon	k1gInSc2	devon
nabývají	nabývat	k5eAaImIp3nP	nabývat
stromového	stromový	k2eAgInSc2d1	stromový
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nižších	nízký	k2eAgFnPc2d2	nižší
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgFnPc1d1	důležitá
vápnité	vápnitý	k2eAgFnPc1d1	vápnitá
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
podílející	podílející	k2eAgInPc1d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Regionální	regionální	k2eAgInSc1d1	regionální
přehled	přehled	k1gInSc1	přehled
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Asie	Asie	k1gFnSc2	Asie
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
starých	starý	k2eAgInPc6d1	starý
kontinentech	kontinent	k1gInPc6	kontinent
dominuje	dominovat	k5eAaImIp3nS	dominovat
mělkovodní	mělkovodní	k2eAgFnSc1d1	mělkovodní
karbonátická	karbonátický	k2eAgFnSc1d1	karbonátický
facie	facie	k1gFnSc1	facie
<g/>
,	,	kIx,	,
na	na	k7c6	na
vynořených	vynořený	k2eAgFnPc6d1	vynořená
částech	část	k1gFnPc6	část
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnSc1	Red
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maximum	maximum	k1gNnSc1	maximum
transgrese	transgrese	k1gFnSc2	transgrese
v	v	k7c6	v
givetu	givet	k1gInSc6	givet
<g/>
,	,	kIx,	,
regrese	regrese	k1gFnSc1	regrese
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
famenu	famen	k1gInSc6	famen
<g/>
.	.	kIx.	.
</s>
<s>
Okrajové	okrajový	k2eAgFnPc1d1	okrajová
mobilní	mobilní	k2eAgFnPc1d1	mobilní
oblasti	oblast	k1gFnPc1	oblast
mají	mít	k5eAaImIp3nP	mít
pestrý	pestrý	k2eAgInSc4d1	pestrý
faciální	faciální	k2eAgInSc4d1	faciální
vývoj	vývoj	k1gInSc4	vývoj
s	s	k7c7	s
mohutnými	mohutný	k2eAgInPc7d1	mohutný
vulkanickými	vulkanický	k2eAgInPc7d1	vulkanický
projevy	projev	k1gInPc7	projev
(	(	kIx(	(
<g/>
uralská	uralský	k2eAgFnSc1d1	Uralská
<g/>
,	,	kIx,	,
altajská	altajský	k2eAgFnSc1d1	Altajská
<g/>
,	,	kIx,	,
mongolská	mongolský	k2eAgFnSc1d1	mongolská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Faunisticky	Faunisticky	k6eAd1	Faunisticky
je	být	k5eAaImIp3nS	být
vývoj	vývoj	k1gInSc1	vývoj
podobný	podobný	k2eAgInSc1d1	podobný
českému	český	k2eAgInSc3d1	český
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Mt	Mt	k1gFnSc1	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Everest	Everest	k1gInSc1	Everest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Vynořena	vynořen	k2eAgFnSc1d1	vynořen
zůstala	zůstat	k5eAaPmAgFnS	zůstat
sv.	sv.	kA	sv.
část	část	k1gFnSc1	část
kanadského	kanadský	k2eAgInSc2d1	kanadský
štítu	štít	k1gInSc2	štít
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sedimentovaly	sedimentovat	k5eAaImAgInP	sedimentovat
horniny	hornina	k1gFnPc4	hornina
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnPc4	Red
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
devonu	devon	k1gInSc6	devon
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zaplavení	zaplavení	k1gNnSc3	zaplavení
jižních	jižní	k2eAgFnPc2d1	jižní
částí	část	k1gFnPc2	část
štítu	štít	k1gInSc2	štít
<g/>
,	,	kIx,	,
sedimentují	sedimentovat	k5eAaImIp3nP	sedimentovat
karbonáty	karbonát	k1gInPc4	karbonát
s	s	k7c7	s
bentickou	bentický	k2eAgFnSc7d1	bentická
faunou	fauna	k1gFnSc7	fauna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pacifické	pacifický	k2eAgMnPc4d1	pacifický
(	(	kIx(	(
<g/>
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
)	)	kIx)	)
a	a	k8xC	a
franklinské	franklinský	k2eAgNnSc1d1	franklinský
(	(	kIx(	(
<g/>
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
)	)	kIx)	)
mobilní	mobilní	k2eAgFnSc6d1	mobilní
okrajové	okrajový	k2eAgFnSc6d1	okrajová
oblasti	oblast	k1gFnSc6	oblast
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
sedimenty	sediment	k1gInPc1	sediment
devonu	devon	k1gInSc2	devon
mocnosti	mocnost	k1gFnSc2	mocnost
až	až	k9	až
5.000	[number]	k4	5.000
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
klastika	klastika	k1gFnSc1	klastika
<g/>
,	,	kIx,	,
karbonáty	karbonát	k1gInPc1	karbonát
<g/>
,	,	kIx,	,
vulkanity	vulkanit	k1gInPc1	vulkanit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
appalačské	appalačský	k2eAgFnSc6d1	appalačský
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc6d1	východní
<g/>
)	)	kIx)	)
oblasti	oblast	k1gFnSc6	oblast
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
sedimentace	sedimentace	k1gFnSc1	sedimentace
nepřetržitě	přetržitě	k6eNd1	přetržitě
od	od	k7c2	od
siluru	silur	k1gInSc2	silur
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
devonu	devon	k1gInSc6	devon
akadská	akadský	k2eAgFnSc1d1	akadská
orogeneze	orogeneze	k1gFnSc1	orogeneze
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
značně	značně	k6eAd1	značně
zúžila	zúžit	k5eAaPmAgFnS	zúžit
a	a	k8xC	a
ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
devonu	devon	k1gInSc6	devon
začíná	začínat	k5eAaImIp3nS	začínat
mořská	mořský	k2eAgFnSc1d1	mořská
regrese	regrese	k1gFnSc1	regrese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Devon	devon	k1gInSc1	devon
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Uruguaje	Uruguaj	k1gInSc2	Uruguaj
a	a	k8xC	a
z	z	k7c2	z
Falklandských	Falklandský	k2eAgInPc2d1	Falklandský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
transgresívně	transgresívně	k6eAd1	transgresívně
uložen	uložit	k5eAaPmNgInS	uložit
na	na	k7c6	na
siluru	silur	k1gInSc6	silur
<g/>
,	,	kIx,	,
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
jsou	být	k5eAaImIp3nP	být
písčito-jílovité	písčitoílovitý	k2eAgInPc4d1	písčito-jílovitý
sedimenty	sediment	k1gInPc4	sediment
<g/>
,	,	kIx,	,
chybí	chybět	k5eAaImIp3nS	chybět
karbonáty	karbonát	k1gInPc4	karbonát
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
ráz	ráz	k1gInSc4	ráz
chladného	chladný	k2eAgNnSc2d1	chladné
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Austrálie	Austrálie	k1gFnSc2	Austrálie
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tasmánské	tasmánský	k2eAgFnSc6d1	tasmánská
mobilní	mobilní	k2eAgFnSc6d1	mobilní
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
mocné	mocný	k2eAgInPc1d1	mocný
sedimenty	sediment	k1gInPc1	sediment
devonu	devon	k1gInSc2	devon
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
klastikami	klastika	k1gFnPc7	klastika
<g/>
,	,	kIx,	,
flyšoidními	flyšoidní	k2eAgFnPc7d1	flyšoidní
a	a	k8xC	a
vulkanickými	vulkanický	k2eAgFnPc7d1	vulkanická
faciemi	facie	k1gFnPc7	facie
<g/>
.	.	kIx.	.
</s>
<s>
Fauna	fauna	k1gFnSc1	fauna
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
české	český	k2eAgFnSc3d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Orogeneze	orogeneze	k1gFnSc1	orogeneze
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
devonu	devon	k1gInSc6	devon
(	(	kIx(	(
<g/>
tabberabbenské	tabberabbenský	k2eAgNnSc1d1	tabberabbenský
vrásnění	vrásnění	k1gNnSc1	vrásnění
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
přesun	přesun	k1gInSc4	přesun
sedimentační	sedimentační	k2eAgFnSc2d1	sedimentační
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
australském	australský	k2eAgInSc6d1	australský
štítu	štít	k1gInSc6	štít
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
devon	devon	k1gInSc1	devon
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
depresi	deprese	k1gFnSc4	deprese
klastikami	klastika	k1gFnPc7	klastika
a	a	k8xC	a
evapority	evaporit	k1gInPc7	evaporit
o	o	k7c6	o
mocnosti	mocnost	k1gFnSc6	mocnost
kolem	kolem	k7c2	kolem
1.000	[number]	k4	1.000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zaplavení	zaplavení	k1gNnSc3	zaplavení
štítu	štít	k1gInSc2	štít
mělkým	mělký	k2eAgNnSc7d1	mělké
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Severoatlantický	severoatlantický	k2eAgInSc4d1	severoatlantický
kontinent	kontinent	k1gInSc4	kontinent
===	===	k?	===
</s>
</p>
<p>
<s>
Rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
je	být	k5eAaImIp3nS	být
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gFnSc2	Red
(	(	kIx(	(
<g/>
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
arkózy	arkóza	k1gFnPc1	arkóza
<g/>
,	,	kIx,	,
prachovce	prachovka	k1gFnSc6	prachovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
s	s	k7c7	s
evapority	evaporit	k1gInPc7	evaporit
<g/>
.	.	kIx.	.
</s>
<s>
Mocnost	mocnost	k1gFnSc1	mocnost
je	být	k5eAaImIp3nS	být
i	i	k9	i
tisíce	tisíc	k4xCgInPc4	tisíc
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
sedimentace	sedimentace	k1gFnSc2	sedimentace
probíhala	probíhat	k5eAaImAgNnP	probíhat
v	v	k7c6	v
brakickém	brakický	k2eAgMnSc6d1	brakický
či	či	k8xC	či
hypersalinním	hypersalinní	k2eAgNnSc6d1	hypersalinní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc7d1	klasická
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
severní	severní	k2eAgFnSc1d1	severní
Anglie	Anglie	k1gFnSc1	Anglie
a	a	k8xC	a
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Variská	variský	k2eAgFnSc1d1	variská
mobilní	mobilní	k2eAgFnSc1d1	mobilní
oblast	oblast	k1gFnSc1	oblast
===	===	k?	===
</s>
</p>
<p>
<s>
Zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
jižní	jižní	k2eAgFnSc4d1	jižní
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
sz.	sz.	k?	sz.
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
Ardeny	Ardeny	k1gFnPc4	Ardeny
<g/>
,	,	kIx,	,
Rýnské	rýnský	k2eAgNnSc4d1	rýnské
břidličné	břidličný	k2eAgNnSc4d1	Břidličné
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
Harz	Harz	k1gInSc4	Harz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgInSc1d1	spodní
devon	devon	k1gInSc1	devon
tvoří	tvořit	k5eAaImIp3nS	tvořit
klastika	klastika	k1gFnSc1	klastika
transportovaná	transportovaný	k2eAgFnSc1d1	transportovaná
z	z	k7c2	z
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
devonu	devon	k1gInSc6	devon
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
oblast	oblast	k1gFnSc1	oblast
karbonátické	karbonátický	k2eAgFnSc2d1	karbonátický
sedimentace	sedimentace	k1gFnSc2	sedimentace
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
vývoj	vývoj	k1gInSc1	vývoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
bazickým	bazický	k2eAgInSc7d1	bazický
submarinním	submarinní	k2eAgInSc7d1	submarinní
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
devonu	devon	k1gInSc6	devon
v	v	k7c6	v
pokleslých	pokleslý	k2eAgFnPc6d1	pokleslá
částech	část	k1gFnPc6	část
dominují	dominovat	k5eAaImIp3nP	dominovat
pelity	pelita	k1gFnPc1	pelita
<g/>
,	,	kIx,	,
na	na	k7c6	na
elevacích	elevace	k1gFnPc6	elevace
cefalopodové	cefalopodový	k2eAgInPc4d1	cefalopodový
vápence	vápenec	k1gInPc4	vápenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Český	český	k2eAgInSc1d1	český
masív	masív	k1gInSc1	masív
===	===	k?	===
</s>
</p>
<p>
<s>
Barrandien	barrandien	k1gInSc1	barrandien
-	-	kIx~	-
devonská	devonský	k2eAgFnSc1d1	Devonská
sedimentace	sedimentace	k1gFnSc1	sedimentace
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
nepřetržitě	přetržitě	k6eNd1	přetržitě
po	po	k7c6	po
siluru	silur	k1gInSc6	silur
(	(	kIx(	(
<g/>
přídol	přídol	k1gInSc1	přídol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
odspodu	odspodu	k6eAd1	odspodu
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
lochkov	lochkov	k1gInSc1	lochkov
-	-	kIx~	-
lochkovské	lochkovský	k2eAgNnSc1d1	lochkovský
souvrství	souvrství	k1gNnSc1	souvrství
-	-	kIx~	-
radotínské	radotínský	k2eAgInPc4d1	radotínský
(	(	kIx(	(
<g/>
bituminózní	bituminózní	k2eAgInPc4d1	bituminózní
<g/>
)	)	kIx)	)
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
vložky	vložka	k1gFnPc4	vložka
břidlic	břidlice	k1gFnPc2	břidlice
a	a	k8xC	a
rohovců	rohovec	k1gInPc2	rohovec
<g/>
,	,	kIx,	,
kotýské	kotýský	k2eAgInPc4d1	kotýský
vápence	vápenec	k1gInPc4	vápenec
(	(	kIx(	(
<g/>
organodetritické	organodetritický	k2eAgInPc4d1	organodetritický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
typickými	typický	k2eAgFnPc7d1	typická
lokalitami	lokalita	k1gFnPc7	lokalita
jsou	být	k5eAaImIp3nP	být
Klonk	Klonk	k1gInSc4	Klonk
u	u	k7c2	u
Suchomast	Suchomast	k1gFnSc1	Suchomast
<g/>
,	,	kIx,	,
Barrandova	Barrandov	k1gInSc2	Barrandov
skála	skála	k1gFnSc1	skála
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
okraji	okraj	k1gInSc6	okraj
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
prag	prag	k1gInSc1	prag
-	-	kIx~	-
pražské	pražský	k2eAgNnSc1d1	Pražské
souvrství	souvrství	k1gNnSc1	souvrství
-	-	kIx~	-
koněpruské	koněpruský	k2eAgInPc1d1	koněpruský
(	(	kIx(	(
<g/>
organodetritické	organodetritický	k2eAgInPc1d1	organodetritický
<g/>
,	,	kIx,	,
útesové	útesový	k2eAgInPc1d1	útesový
<g/>
)	)	kIx)	)
vápence	vápenec	k1gInPc1	vápenec
(	(	kIx(	(
<g/>
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
kůň	kůň	k1gMnSc1	kůň
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Berouna	Beroun	k1gInSc2	Beroun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slivenecké	slivenecký	k2eAgNnSc4d1	slivenecký
(	(	kIx(	(
<g/>
krinoidové	krinoidový	k2eAgNnSc4d1	krinoidový
<g/>
,	,	kIx,	,
organodetritické	organodetritický	k2eAgNnSc4d1	organodetritický
<g/>
)	)	kIx)	)
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
vinařické	vinařický	k2eAgInPc4d1	vinařický
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
loděnické	loděnický	k2eAgInPc4d1	loděnický
deskovité	deskovitý	k2eAgInPc4d1	deskovitý
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
řeporyjské	řeporyjský	k2eAgInPc4d1	řeporyjský
hlíznaté	hlíznatý	k2eAgInPc4d1	hlíznatý
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
dvorecko-prokopské	dvoreckorokopský	k2eAgInPc4d1	dvorecko-prokopský
hlíznaté	hlíznatý	k2eAgInPc4d1	hlíznatý
vápence	vápenec	k1gInPc4	vápenec
</s>
</p>
<p>
<s>
zlíchov	zlíchov	k1gInSc1	zlíchov
-	-	kIx~	-
zlíchovské	zlíchovský	k2eAgNnSc1d1	zlíchovské
souvrství	souvrství	k1gNnSc1	souvrství
-	-	kIx~	-
zlíchovské	zlíchovský	k2eAgInPc1d1	zlíchovský
organodetritické	organodetritický	k2eAgInPc1d1	organodetritický
vápence	vápenec	k1gInPc1	vápenec
s	s	k7c7	s
hojnými	hojný	k2eAgInPc7d1	hojný
rohovci	rohovec	k1gInPc7	rohovec
a	a	k8xC	a
korálovým	korálový	k2eAgInSc7d1	korálový
obzorem	obzor	k1gInSc7	obzor
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
</s>
</p>
<p>
<s>
dalej	dalej	k1gInSc1	dalej
-	-	kIx~	-
dalejsko-třebotovské	dalejskořebotovský	k2eAgNnSc1d1	dalejsko-třebotovský
souvrství	souvrství	k1gNnSc1	souvrství
-	-	kIx~	-
dalejské	dalejský	k2eAgFnPc1d1	dalejský
břidlice	břidlice	k1gFnPc1	břidlice
(	(	kIx(	(
<g/>
tentakulitové	tentakulitový	k2eAgFnPc1d1	tentakulitový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třebotovské	třebotovský	k2eAgInPc4d1	třebotovský
hlíznaté	hlíznatý	k2eAgInPc4d1	hlíznatý
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
suchomastské	suchomastský	k2eAgInPc4d1	suchomastský
mělkovodní	mělkovodní	k2eAgInPc4d1	mělkovodní
organodetritické	organodetritický	k2eAgInPc4d1	organodetritický
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
profil	profil	k1gInSc4	profil
v	v	k7c6	v
Praze-Holyni	Praze-Holyně	k1gFnSc6	Praze-Holyně
je	být	k5eAaImIp3nS	být
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
stratotypem	stratotyp	k1gInSc7	stratotyp
hranice	hranice	k1gFnSc2	hranice
spodní-střední	spodnítřednit	k5eAaPmIp3nS	spodní-střednit
devon	devon	k1gInSc1	devon
</s>
</p>
<p>
<s>
eifel	eifel	k1gInSc1	eifel
-	-	kIx~	-
chotečské	chotečský	k2eAgNnSc1d1	chotečský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
chotečské	chotečský	k2eAgInPc1d1	chotečský
vápence	vápenec	k1gInPc1	vápenec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
givet	givet	k1gInSc1	givet
-	-	kIx~	-
srbské	srbský	k2eAgNnSc1d1	srbské
souvrství	souvrství	k1gNnSc1	souvrství
-	-	kIx~	-
kačácké	kačácký	k2eAgFnPc1d1	kačácký
vrstvy	vrstva	k1gFnPc1	vrstva
(	(	kIx(	(
<g/>
vápence	vápenec	k1gInPc1	vápenec
<g/>
,	,	kIx,	,
břidlice	břidlice	k1gFnPc1	břidlice
s	s	k7c7	s
vložkami	vložka	k1gFnPc7	vložka
vápenců	vápenec	k1gInPc2	vápenec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roblínské	roblínský	k2eAgFnPc1d1	roblínský
vrstvy	vrstva	k1gFnPc1	vrstva
(	(	kIx(	(
<g/>
prachovce	prachovka	k1gFnSc3	prachovka
<g/>
,	,	kIx,	,
pískovce	pískovec	k1gInPc1	pískovec
<g/>
)	)	kIx)	)
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
splavené	splavený	k2eAgFnSc2d1	splavená
terrestrické	terrestrický	k2eAgFnSc2d1	terrestrický
flóry	flóra	k1gFnSc2	flóra
</s>
</p>
<p>
<s>
Rožmitálská	Rožmitálský	k2eAgFnSc1d1	Rožmitálská
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
devonu	devon	k1gInSc2	devon
patří	patřit	k5eAaImIp3nS	patřit
věšínské	věšínský	k2eAgNnSc1d1	věšínský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
prag-zlíchov	praglíchov	k1gInSc1	prag-zlíchov
<g/>
)	)	kIx)	)
s	s	k7c7	s
vápenci	vápenec	k1gInPc7	vápenec
<g/>
,	,	kIx,	,
břidlicemi	břidlice	k1gFnPc7	břidlice
<g/>
,	,	kIx,	,
bezděkovskými	bezděkovský	k2eAgInPc7d1	bezděkovský
slepenci	slepenec	k1gInPc7	slepenec
</s>
</p>
<p>
<s>
Metamorfované	metamorfovaný	k2eAgInPc1d1	metamorfovaný
ostrovy	ostrov	k1gInPc1	ostrov
-	-	kIx~	-
na	na	k7c6	na
sedlčansko-krásnohorském	sedlčanskorásnohorský	k2eAgInSc6d1	sedlčansko-krásnohorský
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
útesové	útesový	k2eAgInPc1d1	útesový
vápence	vápenec	k1gInPc1	vápenec
u	u	k7c2	u
Skoupého	Skoupého	k2eAgFnSc2d1	Skoupého
<g/>
,	,	kIx,	,
bohatší	bohatý	k2eAgFnSc2d2	bohatší
na	na	k7c4	na
grafit	grafit	k1gInSc4	grafit
<g/>
,	,	kIx,	,
rohovce	rohovec	k1gInPc4	rohovec
<g/>
,	,	kIx,	,
skoupské	skoupský	k2eAgInPc4d1	skoupský
slepence	slepenec	k1gInPc4	slepenec
</s>
</p>
<p>
<s>
Železné	železný	k2eAgFnPc1d1	železná
hory	hora	k1gFnPc1	hora
-	-	kIx~	-
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
vápenopodolské	vápenopodolský	k2eAgFnSc2d1	vápenopodolský
geosynklinály	geosynklinála	k1gFnSc2	geosynklinála
mezi	mezi	k7c7	mezi
Vápeným	Vápený	k2eAgInSc7d1	Vápený
Podolem	Podol	k1gInSc7	Podol
a	a	k8xC	a
Prachovicemi	Prachovice	k1gFnPc7	Prachovice
jsou	být	k5eAaImIp3nP	být
devonského	devonský	k2eAgNnSc2d1	devonské
stáří	stáří	k1gNnSc2	stáří
podolské	podolský	k2eAgInPc4d1	podolský
vápence	vápenec	k1gInPc4	vápenec
</s>
</p>
<p>
<s>
Sasko-durynská	saskourynský	k2eAgFnSc1d1	sasko-durynský
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
lochkovského	lochkovský	k2eAgNnSc2d1	lochkovský
stáří	stáří	k1gNnSc2	stáří
jsou	být	k5eAaImIp3nP	být
vrchní	vrchní	k2eAgMnPc1d1	vrchní
graptolitové	graptolit	k1gMnPc1	graptolit
břidlice	břidlice	k1gFnSc2	břidlice
<g/>
,	,	kIx,	,
prag	prag	k1gInSc4	prag
-	-	kIx~	-
tentakulitové	tentakulitový	k2eAgInPc4d1	tentakulitový
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
břidlice	břidlice	k1gFnPc4	břidlice
zlichovu-givetu	zlichovuivet	k1gInSc2	zlichovu-givet
<g/>
.	.	kIx.	.
</s>
<s>
Faciální	faciální	k2eAgFnPc1d1	faciální
změny	změna	k1gFnPc1	změna
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
svrchního	svrchní	k2eAgInSc2d1	svrchní
devonu	devon	k1gInSc2	devon
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
reusská	reusský	k2eAgFnSc1d1	reusský
orogeneze	orogeneze	k1gFnSc1	orogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
frasnu	frasnout	k5eAaPmIp1nS	frasnout
je	on	k3xPp3gMnPc4	on
důležitý	důležitý	k2eAgInSc1d1	důležitý
bazický	bazický	k2eAgInSc1d1	bazický
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
jsou	být	k5eAaImIp3nP	být
spojena	spojen	k2eAgNnPc4d1	spojeno
ložiska	ložisko	k1gNnPc4	ložisko
Fe-rud	Feuda	k1gFnPc2	Fe-ruda
<g/>
.	.	kIx.	.
</s>
<s>
Famenského	Famenský	k2eAgNnSc2d1	Famenský
stáří	stáří	k1gNnSc2	stáří
jsou	být	k5eAaImIp3nP	být
jemné	jemný	k2eAgFnPc1d1	jemná
břidlice	břidlice	k1gFnPc1	břidlice
s	s	k7c7	s
ostrakody	ostrakod	k1gInPc7	ostrakod
a	a	k8xC	a
vápence	vápenec	k1gInPc1	vápenec
s	s	k7c7	s
pelagickou	pelagický	k2eAgFnSc7d1	pelagická
faunou	fauna	k1gFnSc7	fauna
</s>
</p>
<p>
<s>
Lužická	lužický	k2eAgFnSc1d1	Lužická
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
u	u	k7c2	u
Jitravy	Jitrava	k1gFnSc2	Jitrava
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc1d1	doložen
famen	famen	k1gInSc1	famen
(	(	kIx(	(
<g/>
jitravská	jitravský	k2eAgFnSc1d1	jitravský
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
s	s	k7c7	s
grafitickými	grafitický	k2eAgFnPc7d1	grafitická
břidlicemi	břidlice	k1gFnPc7	břidlice
<g/>
,	,	kIx,	,
hlíznatými	hlíznatý	k2eAgInPc7d1	hlíznatý
vápenci	vápenec	k1gInPc7	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Nepasic	Nepasice	k1gInPc2	Nepasice
(	(	kIx(	(
<g/>
východně	východně	k6eAd1	východně
Hradce	Hradec	k1gInPc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zachycen	zachycen	k2eAgInSc4d1	zachycen
devon	devon	k1gInSc4	devon
ve	v	k7c6	v
vrtu	vrt	k1gInSc6	vrt
(	(	kIx(	(
<g/>
klyméniové	klyméniový	k2eAgFnSc6d1	klyméniový
vápence	vápenka	k1gFnSc6	vápenka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moravsko-slezská	moravskolezský	k2eAgFnSc1d1	moravsko-slezská
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
faciálně	faciálně	k6eAd1	faciálně
se	se	k3xPyFc4	se
vyčleňuje	vyčleňovat	k5eAaImIp3nS	vyčleňovat
drahanský	drahanský	k2eAgInSc1d1	drahanský
vývoj	vývoj	k1gInSc1	vývoj
-	-	kIx~	-
hlubokovodní	hlubokovodní	k2eAgFnSc1d1	hlubokovodní
sedimentace	sedimentace	k1gFnSc1	sedimentace
<g/>
,	,	kIx,	,
pelagická	pelagický	k2eAgFnSc1d1	pelagická
fauna	fauna	k1gFnSc1	fauna
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc1d1	přechodný
vývoj	vývoj	k1gInSc1	vývoj
-	-	kIx~	-
stínavsko-chabičovské	stínavskohabičovský	k2eAgNnSc1d1	stínavsko-chabičovský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
eifel	eifel	k1gInSc1	eifel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
josefovské	josefovský	k2eAgInPc4d1	josefovský
a	a	k8xC	a
lažánecké	lažánecký	k2eAgInPc4d1	lažánecký
vápence	vápenec	k1gInPc4	vápenec
(	(	kIx(	(
<g/>
givet	givet	k1gMnSc1	givet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vilémovické	vilémovický	k2eAgInPc4d1	vilémovický
vápence	vápenec	k1gInPc4	vápenec
(	(	kIx(	(
<g/>
frasn	frasn	k1gMnSc1	frasn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ponikevské	ponikevský	k2eAgNnSc1d1	ponikevský
souvrství	souvrství	k1gNnSc1	souvrství
(	(	kIx(	(
<g/>
famen	famen	k1gInSc1	famen
<g/>
)	)	kIx)	)
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
</s>
</p>
<p>
<s>
Silesikum	silesikum	k1gNnSc1	silesikum
-	-	kIx~	-
vrbenské	vrbenský	k2eAgFnPc1d1	vrbenská
vrstvy	vrstva	k1gFnPc1	vrstva
-	-	kIx~	-
kvarcity	kvarcit	k1gInPc1	kvarcit
<g/>
,	,	kIx,	,
fylity	fylit	k1gInPc1	fylit
<g/>
,	,	kIx,	,
metabazity	metabazit	k1gInPc1	metabazit
<g/>
,	,	kIx,	,
Fe-rudy	Feuda	k1gFnPc1	Fe-ruda
typu	typ	k1gInSc2	typ
Lahn-Dill	Lahn-Dill	k1gInSc1	Lahn-Dill
<g/>
,	,	kIx,	,
vápence	vápenec	k1gInPc1	vápenec
<g/>
,	,	kIx,	,
vápnité	vápnitý	k2eAgInPc1d1	vápnitý
fylity	fylit	k1gInPc1	fylit
a	a	k8xC	a
droby	droba	k1gFnPc1	droba
<g/>
,	,	kIx,	,
rejvízské	rejvízský	k2eAgFnPc1d1	rejvízský
vrstvy	vrstva	k1gFnPc1	vrstva
s	s	k7c7	s
hojnějšími	hojný	k2eAgInPc7d2	hojnější
metabazity	metabazit	k1gInPc7	metabazit
</s>
</p>
<p>
<s>
Drahanská	Drahanský	k2eAgFnSc1d1	Drahanská
oblast	oblast	k1gFnSc1	oblast
-	-	kIx~	-
zastoupeny	zastoupen	k2eAgInPc1d1	zastoupen
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
typy	typ	k1gInPc1	typ
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kladek	kladka	k1gFnPc2	kladka
(	(	kIx(	(
<g/>
drahanský	drahanský	k2eAgInSc1d1	drahanský
vývoj	vývoj	k1gInSc1	vývoj
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nP	patřit
pragu	praga	k1gFnSc4	praga
drakovské	drakovský	k2eAgInPc1d1	drakovský
kvarcity	kvarcit	k1gInPc1	kvarcit
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
s	s	k7c7	s
klastiky	klastik	k1gMnPc7	klastik
<g/>
,	,	kIx,	,
zlíchov-eifel	zlíchovifet	k5eAaPmAgMnS	zlíchov-eifet
-	-	kIx~	-
stínavsko-chabičovské	stínavskohabičovský	k2eAgFnPc1d1	stínavsko-chabičovský
vrstvy	vrstva	k1gFnPc1	vrstva
s	s	k7c7	s
vulkanickým	vulkanický	k2eAgInSc7d1	vulkanický
komplexem	komplex	k1gInSc7	komplex
(	(	kIx(	(
<g/>
pyroklastika	pyroklastika	k1gFnSc1	pyroklastika
<g/>
,	,	kIx,	,
tufity	tufit	k1gInPc1	tufit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
givet	givet	k1gInSc4	givet
-	-	kIx~	-
jesenické	jesenický	k2eAgInPc4d1	jesenický
vápence	vápenec	k1gInPc4	vápenec
<g/>
,	,	kIx,	,
famen	famen	k1gInSc1	famen
-	-	kIx~	-
ponikevské	ponikevský	k2eAgNnSc1d1	ponikevský
souvrství	souvrství	k1gNnSc1	souvrství
s	s	k7c7	s
pelity	pelita	k1gMnPc7	pelita
<g/>
,	,	kIx,	,
prachovci	prachovec	k1gInPc7	prachovec
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc1d1	přechodný
vývoj	vývoj	k1gInSc1	vývoj
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
u	u	k7c2	u
Němčic	Němčice	k1gFnPc2	Němčice
a	a	k8xC	a
v	v	k7c6	v
konicko-mladečském	konickoladečský	k2eAgInSc6d1	konicko-mladečský
devonu	devon	k1gInSc6	devon
(	(	kIx(	(
<g/>
chybí	chybit	k5eAaPmIp3nP	chybit
projevy	projev	k1gInPc1	projev
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
Moravského	moravský	k2eAgInSc2d1	moravský
krasu	kras	k1gInSc2	kras
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
u	u	k7c2	u
Boskovic	Boskovice	k1gInPc2	Boskovice
a	a	k8xC	a
Šebetova	Šebetův	k2eAgInSc2d1	Šebetův
</s>
</p>
<p>
<s>
Devon	devon	k1gInSc1	devon
u	u	k7c2	u
Stínavy	Stínava	k1gFnSc2	Stínava
-	-	kIx~	-
sedimentace	sedimentace	k1gFnSc1	sedimentace
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
od	od	k7c2	od
siluru	silur	k1gInSc2	silur
do	do	k7c2	do
eifelu	eifel	k1gInSc2	eifel
<g/>
,	,	kIx,	,
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
jsou	být	k5eAaImIp3nP	být
kvarcity	kvarcit	k1gInPc1	kvarcit
<g/>
,	,	kIx,	,
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
droby	droba	k1gFnPc1	droba
</s>
</p>
<p>
<s>
Hranický	hranický	k2eAgInSc4d1	hranický
devon	devon	k1gInSc4	devon
(	(	kIx(	(
<g/>
kra	kra	k1gFnSc1	kra
Maleníku	Maleník	k1gInSc2	Maleník
<g/>
)	)	kIx)	)
-	-	kIx~	-
biohermy	bioherma	k1gFnSc2	bioherma
spodního	spodní	k2eAgNnSc2d1	spodní
frasnu	frasnout	k5eAaPmIp1nS	frasnout
<g/>
,	,	kIx,	,
flyšové	flyšový	k2eAgFnPc4d1	flyšový
facie	facie	k1gFnPc4	facie
</s>
</p>
<p>
<s>
Šternbersko-hornobenešovský	Šternberskoornobenešovský	k2eAgInSc1d1	Šternbersko-hornobenešovský
pruh	pruh	k1gInSc1	pruh
(	(	kIx(	(
<g/>
drahanský	drahanský	k2eAgInSc1d1	drahanský
vývoj	vývoj	k1gInSc1	vývoj
<g/>
)	)	kIx)	)
-	-	kIx~	-
četný	četný	k2eAgInSc1d1	četný
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
s	s	k7c7	s
Fe-rudami	Feuda	k1gFnPc7	Fe-ruda
typu	typ	k1gInSc2	typ
Lahn-Dill	Lahn-Dilla	k1gFnPc2	Lahn-Dilla
středního	střední	k2eAgInSc2d1	střední
devonu	devon	k1gInSc2	devon
<g/>
,	,	kIx,	,
čabovské	čabovské	k2eAgFnSc1d1	čabovské
břidlice	břidlice	k1gFnSc1	břidlice
(	(	kIx(	(
<g/>
eifel-givet	eifelivet	k1gInSc1	eifel-givet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křemičité	křemičitý	k2eAgFnPc1d1	křemičitá
břidlice	břidlice	k1gFnPc1	břidlice
(	(	kIx(	(
<g/>
radiolarity	radiolarit	k1gInPc1	radiolarit
<g/>
)	)	kIx)	)
-	-	kIx~	-
famen	famen	k2eAgInSc1d1	famen
až	až	k8xS	až
spodní	spodní	k2eAgInSc1d1	spodní
karbon	karbon	k1gInSc1	karbon
</s>
</p>
<p>
<s>
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
-	-	kIx~	-
po	po	k7c6	po
bazálních	bazální	k2eAgFnPc6d1	bazální
klastikách	klastika	k1gFnPc6	klastika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sedimentují	sedimentovat	k5eAaImIp3nP	sedimentovat
od	od	k7c2	od
eifelu	eifel	k1gInSc2	eifel
do	do	k7c2	do
givetu	givet	k1gInSc2	givet
(	(	kIx(	(
<g/>
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
arkózy	arkóza	k1gFnPc1	arkóza
-	-	kIx~	-
Babí	babí	k2eAgInSc1d1	babí
lom	lom	k1gInSc1	lom
u	u	k7c2	u
Lelekovic	Lelekovice	k1gFnPc2	Lelekovice
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
kopec	kopec	k1gInSc1	kopec
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
macošské	macošský	k2eAgNnSc1d1	macošský
souvrství	souvrství	k1gNnSc1	souvrství
s	s	k7c7	s
josefovskými	josefovský	k2eAgMnPc7d1	josefovský
(	(	kIx(	(
<g/>
stringocefalovými	stringocefalův	k2eAgMnPc7d1	stringocefalův
<g/>
)	)	kIx)	)
a	a	k8xC	a
lažáneckými	lažánecký	k2eAgInPc7d1	lažánecký
(	(	kIx(	(
<g/>
amfiporovými	amfiporův	k2eAgInPc7d1	amfiporův
<g/>
)	)	kIx)	)
vápenci	vápenec	k1gInPc7	vápenec
givetu	givet	k1gInSc2	givet
<g/>
,	,	kIx,	,
vilémovickými	vilémovický	k2eAgInPc7d1	vilémovický
(	(	kIx(	(
<g/>
korálovými	korálový	k2eAgInPc7d1	korálový
<g/>
)	)	kIx)	)
vápenci	vápenec	k1gInPc7	vápenec
frasnu	frasnout	k5eAaPmIp1nS	frasnout
a	a	k8xC	a
líšeňské	líšeňský	k2eAgNnSc1d1	líšeňské
souvrství	souvrství	k1gNnSc1	souvrství
s	s	k7c7	s
křtinskými	křtinský	k2eAgInPc7d1	křtinský
hlíznatými	hlíznatý	k2eAgInPc7d1	hlíznatý
vápenci	vápenec	k1gInPc7	vápenec
(	(	kIx(	(
<g/>
frasn-famen	frasnamen	k1gInSc1	frasn-famen
<g/>
)	)	kIx)	)
a	a	k8xC	a
hádsko-říčskými	hádsko-říčský	k2eAgInPc7d1	hádsko-říčský
organodetritickými	organodetritický	k2eAgInPc7d1	organodetritický
vápenci	vápenec	k1gInPc7	vápenec
s	s	k7c7	s
vložkami	vložka	k1gFnPc7	vložka
vápnitých	vápnitý	k2eAgFnPc2d1	vápnitá
břidlic	břidlice	k1gFnPc2	břidlice
(	(	kIx(	(
<g/>
famen-spodní	famenpodní	k2eAgInSc1d1	famen-spodní
karbon	karbon	k1gInSc1	karbon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Devon	devon	k1gInSc1	devon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
