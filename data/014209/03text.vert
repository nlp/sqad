<s>
Generativní	generativní	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Noam	Noam	k1gMnSc1
Chomsky	Chomsky	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
generativní	generativní	k2eAgFnSc2d1
gramatiky	gramatika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Generativní	generativní	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
je	být	k5eAaImIp3nS
směr	směr	k1gInSc4
lingvistiky	lingvistika	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c4
různých	různý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všem	všecek	k3xTgMnPc3
těmto	tento	k3xDgFnPc3
školám	škola	k1gFnPc3
je	být	k5eAaImIp3nS
společná	společný	k2eAgFnSc1d1
snaha	snaha	k1gFnSc1
o	o	k7c4
formální	formální	k2eAgFnSc4d1
přesnost	přesnost	k1gFnSc4
a	a	k8xC
axiomatickou	axiomatický	k2eAgFnSc4d1
výstavbu	výstavba	k1gFnSc4
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
přírodních	přírodní	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
(	(	kIx(
<g/>
matematika	matematika	k1gFnSc1
<g/>
,	,	kIx,
fyzika	fyzika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generativní	generativní	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
nechce	chtít	k5eNaImIp3nS
být	být	k5eAaImF
pouhým	pouhý	k2eAgInSc7d1
induktivním	induktivní	k2eAgInSc7d1
popisem	popis	k1gInSc7
posbíraných	posbíraný	k2eAgFnPc2d1
vět	věta	k1gFnPc2
jako	jako	k8xS,k8xC
korpusová	korpusový	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
vysvětlit	vysvětlit	k5eAaPmF
<g/>
,	,	kIx,
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
děti	dítě	k1gFnPc1
dokážou	dokázat	k5eAaPmIp3nP
naučit	naučit	k5eAaPmF
používat	používat	k5eAaImF
jazyk	jazyk	k1gInSc4
svých	svůj	k3xOyFgMnPc2
rodičů	rodič	k1gMnPc2
tak	tak	k9
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
jak	jak	k6eAd1
se	se	k3xPyFc4
tomu	ten	k3xDgNnSc3
normálně	normálně	k6eAd1
při	při	k7c6
akvizici	akvizice	k1gFnSc6
přirozeného	přirozený	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
děje	dít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladatelem	zakladatel	k1gMnSc7
generativní	generativní	k2eAgFnSc2d1
gramatiky	gramatika	k1gFnSc2
je	být	k5eAaImIp3nS
Noam	Noam	k1gMnSc1
Chomsky	Chomsky	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
teorii	teorie	k1gFnSc6
formálních	formální	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
se	se	k3xPyFc4
termín	termín	k1gInSc1
generativní	generativní	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
používá	používat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
jako	jako	k8xS,k8xC
synonymum	synonymum	k1gNnSc4
termínu	termín	k1gInSc2
formální	formální	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vysvětlení	vysvětlení	k1gNnSc1
pojmu	pojem	k1gInSc2
</s>
<s>
Generativní	generativní	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
rozšířila	rozšířit	k5eAaPmAgFnS
kriteria	kriterium	k1gNnPc4
empirické	empirický	k2eAgFnSc2d1
správnosti	správnost	k1gFnSc2
výzkumu	výzkum	k1gInSc2
o	o	k7c4
nový	nový	k2eAgInSc4d1
druh	druh	k1gInSc4
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
generativní	generativní	k2eAgFnSc2d1
jazykovědy	jazykověda	k1gFnSc2
by	by	kYmCp3nS
jazykovědné	jazykovědný	k2eAgNnSc1d1
bádání	bádání	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
ustrnout	ustrnout	k5eAaPmF
pouze	pouze	k6eAd1
u	u	k7c2
popisné	popisný	k2eAgFnSc2d1
katalogizace	katalogizace	k1gFnSc2
jazykových	jazykový	k2eAgNnPc2d1
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
taxonomie	taxonomie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
pokrok	pokrok	k1gInSc4
jazykovědy	jazykověda	k1gFnSc2
jsou	být	k5eAaImIp3nP
důležité	důležitý	k2eAgFnPc1d1
nejen	nejen	k6eAd1
autentické	autentický	k2eAgFnPc1d1
či	či	k8xC
korektní	korektní	k2eAgFnPc1d1
věty	věta	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
věty	věta	k1gFnPc1
zkonstruované	zkonstruovaný	k2eAgFnPc1d1
a	a	k8xC
záměrně	záměrně	k6eAd1
nesprávné	správný	k2eNgFnSc2d1
jako	jako	k8xC,k8xS
např.	např.	kA
věta	věta	k1gFnSc1
„	„	k?
<g/>
Koho	kdo	k3yInSc4,k3yQnSc4,k3yRnSc4
jste	být	k5eAaImIp2nP
pozvali	pozvat	k5eAaPmAgMnP
Karla	Karel	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
porušen	porušen	k2eAgInSc1d1
princip	princip	k1gInSc1
přidělování	přidělování	k1gNnSc2
sémantických	sémantický	k2eAgFnPc2d1
rolí	role	k1gFnPc2
zvaný	zvaný	k2eAgInSc1d1
„	„	k?
<g/>
theta	theta	k1gFnSc1
kritérium	kritérium	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
pochopením	pochopení	k1gNnSc7
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
proč	proč	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
negramatická	gramatický	k2eNgFnSc1d1
věta	věta	k1gFnSc1
<g/>
,	,	kIx,
postupuje	postupovat	k5eAaImIp3nS
poznání	poznání	k1gNnSc1
gramatického	gramatický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nálepka	nálepka	k1gFnSc1
„	„	k?
<g/>
generativní	generativní	k2eAgFnSc1d1
<g/>
“	“	k?
v	v	k7c6
názvu	název	k1gInSc6
tohoto	tento	k3xDgInSc2
směru	směr	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
prvního	první	k4xOgNnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
již	již	k9
opuštěného	opuštěný	k2eAgMnSc2d1
<g/>
,	,	kIx,
záměru	záměr	k1gInSc2
jazykovědců	jazykovědec	k1gMnPc2
popsat	popsat	k5eAaPmF
jazykovou	jazykový	k2eAgFnSc4d1
kompetenci	kompetence	k1gFnSc4
rodilého	rodilý	k2eAgMnSc2d1
mluvčího	mluvčí	k1gMnSc2
jako	jako	k8xS,k8xC
úplný	úplný	k2eAgInSc1d1
systém	systém	k1gInSc1
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
dokázal	dokázat	k5eAaPmAgInS
vytvořit	vytvořit	k5eAaPmF
(	(	kIx(
<g/>
generovat	generovat	k5eAaImF
<g/>
)	)	kIx)
veškeré	veškerý	k3xTgNnSc4
gramaticky	gramaticky	k6eAd1
správné	správný	k2eAgFnPc1d1
věty	věta	k1gFnPc1
daného	daný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
jenom	jenom	k9
tyto	tento	k3xDgFnPc4
věty	věta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	kYmCp3nP
systém	systém	k1gInSc4
pravidel	pravidlo	k1gNnPc2
napsaný	napsaný	k2eAgInSc4d1
jazykovědcem	jazykovědec	k1gMnSc7
nebyl	být	k5eNaImAgInS
schopen	schopen	k2eAgInSc1d1
generovat	generovat	k5eAaImF
některé	některý	k3yIgFnPc4
věty	věta	k1gFnPc4
přirozeného	přirozený	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
kompetentní	kompetentní	k2eAgMnPc1d1
rodilí	rodilý	k2eAgMnPc1d1
mluvčí	mluvčí	k1gMnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
správné	správný	k2eAgNnSc4d1
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgMnS
by	by	kYmCp3nS
úplný	úplný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	kYmCp3nP
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
generoval	generovat	k5eAaImAgInS
i	i	k9
gramaticky	gramaticky	k6eAd1
nesprávné	správný	k2eNgFnSc2d1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
empirický	empirický	k2eAgInSc1d1
důkaz	důkaz	k1gInSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
obsahuje	obsahovat	k5eAaImIp3nS
nějakou	nějaký	k3yIgFnSc4
chybu	chyba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autoři	autor	k1gMnPc1
tradičních	tradiční	k2eAgFnPc2d1
mluvnic	mluvnice	k1gFnPc2
až	až	k9
na	na	k7c4
malé	malý	k2eAgFnPc4d1
výjimky	výjimka	k1gFnPc4
nepostupovali	postupovat	k5eNaImAgMnP
generativním	generativní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
spoléhali	spoléhat	k5eAaImAgMnP
se	se	k3xPyFc4
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
čtenář	čtenář	k1gMnSc1
je	být	k5eAaImIp3nS
už	už	k6eAd1
zkušený	zkušený	k2eAgMnSc1d1
uživatel	uživatel	k1gMnSc1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
si	se	k3xPyFc3
některé	některý	k3yIgFnPc4
otázky	otázka	k1gFnPc4
vůbec	vůbec	k9
neklade	klást	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Jazykovědci	jazykovědec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
rozvíjejí	rozvíjet	k5eAaImIp3nP
generativní	generativní	k2eAgFnSc4d1
gramatiku	gramatika	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
generativisté	generativista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generativisté	Generativista	k1gMnPc1
studují	studovat	k5eAaImIp3nP
jazyk	jazyk	k1gInSc4
jako	jako	k8xC,k8xS
přírodní	přírodní	k2eAgInSc4d1
objekt	objekt	k1gInSc4
<g/>
,	,	kIx,
nikoli	nikoli	k9
jako	jako	k9
atribut	atribut	k1gInSc4
lidské	lidský	k2eAgFnSc2d1
subjektivity	subjektivita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
Generativní	generativní	k2eAgFnSc2d1
gramatiky	gramatika	k1gFnSc2
</s>
<s>
Kritikem	kritik	k1gMnSc7
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
lingvista	lingvista	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
Daniel	Daniel	k1gMnSc1
Everett	Everett	k1gMnSc1
<g/>
,	,	kIx,
argumentující	argumentující	k2eAgMnSc1d1
brazilským	brazilský	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
pirahã	pirahã	k?
<g/>
,	,	kIx,
o	o	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
na	na	k7c6
základě	základ	k1gInSc6
svojí	svůj	k3xOyFgFnSc2
aktivní	aktivní	k2eAgFnSc2d1
znalosti	znalost	k1gFnSc2
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nesplňuje	splňovat	k5eNaImIp3nS
podmínku	podmínka	k1gFnSc4
tzv.	tzv.	kA
rekurze	rekurze	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
Interpreter	interpreter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
New	New	k1gMnSc1
Yorker	Yorker	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BREDOW	BREDOW	kA
VON	von	k1gInSc1
<g/>
,	,	kIx,
Rafaela	Rafaela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brazil	Brazil	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Pirahã	Pirahã	k1gFnSc7
Tribe	Trib	k1gInSc5
<g/>
:	:	kIx,
Living	Living	k1gInSc4
without	without	k5eAaPmF,k5eAaImF
Numbers	Numbers	k1gInSc4
or	or	k?
Time	Time	k1gInSc1
-	-	kIx~
SPIEGEL	SPIEGEL	kA
ONLINE	ONLINE	kA
-	-	kIx~
International	International	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SPIEGEL	SPIEGEL	kA
ONLINE	ONLINE	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Generativní	generativní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1
•	•	k?
Generativní	generativní	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
•	•	k?
Minimální	minimální	k2eAgInSc1d1
pár	pár	k4xCyI
•	•	k?
Negramatická	gramatický	k2eNgFnSc1d1
věta	věta	k1gFnSc1
•	•	k?
Opozice	opozice	k1gFnSc1
•	•	k?
Privativní	privativní	k2eAgFnSc1d1
opozice	opozice	k1gFnSc1
•	•	k?
Příznakovost	příznakovost	k1gFnSc1
•	•	k?
Lingvistický	lingvistický	k2eAgInSc1d1
rys	rys	k1gInSc1
•	•	k?
Transformační	transformační	k2eAgFnSc1d1
gramatika	gramatika	k1gFnSc1
•	•	k?
Vázání	vázání	k1gNnSc1
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1
Teoretická	teoretický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Fonologie	fonologie	k1gFnSc1
•	•	k?
Generativní	generativní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Intersubjektivita	intersubjektivita	k1gFnSc1
•	•	k?
Kognitivní	kognitivní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Kvantitativní	kvantitativní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Lexikologie	lexikologie	k1gFnSc2
•	•	k?
Morfologie	morfologie	k1gFnSc2
•	•	k?
Pragmatika	pragmatika	k1gFnSc1
•	•	k?
Sémantika	sémantika	k1gFnSc1
•	•	k?
Syntax	syntax	k1gFnSc1
Deskriptivní	deskriptivní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Etymologie	etymologie	k1gFnSc1
•	•	k?
Fonetika	fonetika	k1gFnSc1
•	•	k?
Historická	historický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Komparativní	komparativní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Sociolingvistika	sociolingvistika	k1gFnSc1
Aplikovaná	aplikovaný	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Forenzní	forenzní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Jazyková	jazykový	k2eAgFnSc1d1
akvizice	akvizice	k1gFnSc1
•	•	k?
Počítačová	počítačový	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Korpusová	korpusový	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Neurolingvistika	Neurolingvistika	k1gFnSc1
•	•	k?
Preskriptivní	Preskriptivní	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
•	•	k?
Psycholingvistika	psycholingvistika	k1gFnSc1
•	•	k?
Stylistika	stylistika	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Jazyk	jazyk	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4113707-3	4113707-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
6676	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85053821	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85053821	#num#	k4
</s>
