<s>
Dogecoin	Dogecoin	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
formulace	formulace	k1gFnPc4
</s>
<s>
DogecoinZemě	DogecoinZemě	k6eAd1
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
</s>
<s>
Dogecoin	Dogecoin	k1gInSc1
je	být	k5eAaImIp3nS
kryptoměna	kryptoměn	k2eAgFnSc1d1
a	a	k8xC
P2P	P2P	k1gFnSc3
platební	platební	k2eAgFnSc4d1
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
měna	měna	k1gFnSc1
používá	používat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dogecoin	Dogecoin	k1gInSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
zkratkami	zkratka	k1gFnPc7
DOGE	DOGE	kA
nebo	nebo	k8xC
XDG	XDG	kA
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
symbolem	symbol	k1gInSc7
Ð	Ð	k?
Síť	síť	k1gFnSc1
má	mít	k5eAaImIp3nS
veřejně	veřejně	k6eAd1
přístupný	přístupný	k2eAgInSc1d1
zdrojový	zdrojový	k2eAgInSc1d1
kód	kód	k1gInSc1
(	(	kIx(
<g/>
open	open	k1gInSc1
source	sourec	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
decentralizovaná	decentralizovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
centrální	centrální	k2eAgInSc1d1
server	server	k1gInSc1
<g/>
,	,	kIx,
přes	přes	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
by	by	kYmCp3nP
procházely	procházet	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
transakce	transakce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
silou	síla	k1gFnSc7
jsou	být	k5eAaImIp3nP
rychlé	rychlý	k2eAgFnPc1d1
transakce	transakce	k1gFnPc1
<g/>
,	,	kIx,
nízké	nízký	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
a	a	k8xC
silná	silný	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znakem	znak	k1gInSc7
kryptoměny	kryptoměn	k2eAgFnPc1d1
je	on	k3xPp3gInPc4
legendární	legendární	k2eAgMnSc1d1
pes	pes	k1gMnSc1
Shiba-Inu	Shiba-In	k1gInSc2
(	(	kIx(
<g/>
doge	doge	k6eAd1
<g/>
)	)	kIx)
z	z	k7c2
internetových	internetový	k2eAgInPc2d1
memů	mem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měna	měna	k1gFnSc1
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
založena	založit	k5eAaPmNgFnS
jako	jako	k8xS,k8xC
recese	recese	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Měna	měna	k1gFnSc1
Dogecoin	Dogecoina	k1gFnPc2
je	být	k5eAaImIp3nS
spojována	spojovat	k5eAaImNgFnS
s	s	k7c7
mottem	motto	k1gNnSc7
„	„	k?
<g/>
To	to	k9
the	the	k?
Moon	Moon	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
Na	na	k7c4
Měsíc	měsíc	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
Dogecoinu	Dogecoin	k1gInSc2
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgInP
kryptoměny	kryptoměn	k2eAgInPc1d1
Apecoin	Apecoin	k1gMnSc1
(	(	kIx(
<g/>
APE	APE	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nyancoin	Nyancoin	k1gMnSc1
(	(	kIx(
<g/>
NYAN	NYAN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Catcoin	Catcoin	k1gMnSc1
(	(	kIx(
<g/>
CAT	CAT	kA
<g/>
)	)	kIx)
a	a	k8xC
Pandacoin	Pandacoin	k1gMnSc1
(	(	kIx(
<g/>
PND	PND	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Dogecoin	Dogecoin	k1gInSc1
původně	původně	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
jako	jako	k9
recese	recese	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořil	vytvořit	k5eAaPmAgMnS
ho	on	k3xPp3gInSc4
programátor	programátor	k1gInSc4
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
IBM	IBM	kA
Bill	Bill	k1gMnSc1
Markus	Markus	k1gMnSc1
a	a	k8xC
markeťák	markeťák	k1gMnSc1
z	z	k7c2
Adobe	Adobe	kA
Systems	Systemsa	k1gFnPc2
Jackson	Jackson	k1gMnSc1
Palmer	Palmer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
zakoupit	zakoupit	k5eAaPmF
doménu	doména	k1gFnSc4
dogecoin	dogecoina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
a	a	k8xC
krátce	krátce	k6eAd1
po	po	k7c6
naprogramování	naprogramování	k1gNnSc6
první	první	k4xOgFnSc2
peněženky	peněženka	k1gFnSc2
web	web	k1gInSc4
spustili	spustit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dogecoin	Dogecoin	k1gInSc1
odkazoval	odkazovat	k5eAaImAgInS
na	na	k7c6
meme	meme	k1gNnSc6
se	s	k7c7
slavným	slavný	k2eAgMnSc7d1
japonským	japonský	k2eAgMnSc7d1
psem	pes	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novou	nový	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
založili	založit	k5eAaPmAgMnP
na	na	k7c6
kryptoměně	kryptoměna	k1gFnSc6
litecoin	litecoina	k1gFnPc2
a	a	k8xC
spustili	spustit	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
8	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nárůst	nárůst	k1gInSc1
hodnoty	hodnota	k1gFnSc2
této	tento	k3xDgFnSc2
kryptoměny	kryptoměn	k2eAgFnPc1d1
byl	být	k5eAaImAgInS
nevídaný	vídaný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
19	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
její	její	k3xOp3gInSc4
hodnota	hodnota	k1gFnSc1
vzrostla	vzrůst	k5eAaPmAgFnS
během	během	k7c2
72	#num#	k4
hodin	hodina	k1gFnPc2
o	o	k7c4
300	#num#	k4
%	%	kIx~
(	(	kIx(
<g/>
z	z	k7c2
0,00026	0,00026	k4
USD	USD	kA
na	na	k7c4
0,00095	0,00095	k4
USD	USD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohužel	bohužel	k6eAd1
se	se	k3xPyFc4
již	již	k9
o	o	k7c4
5	#num#	k4
dní	den	k1gInPc2
později	pozdě	k6eAd2
odehrála	odehrát	k5eAaPmAgFnS
první	první	k4xOgFnSc1
velká	velký	k2eAgFnSc1d1
krádež	krádež	k1gFnSc1
Dogecoinů	Dogecoin	k1gInPc2
–	–	k?
miliony	milion	k4xCgInPc4
mincí	mince	k1gFnSc7
byly	být	k5eAaImAgFnP
ukradeny	ukrást	k5eAaPmNgFnP
z	z	k7c2
internetové	internetový	k2eAgFnSc2d1
peněženky	peněženka	k1gFnSc2
Dogewallet	Dogewallet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočník	útočník	k1gMnSc1
tehdy	tehdy	k6eAd1
získal	získat	k5eAaPmAgMnS
přístup	přístup	k1gInSc4
k	k	k7c3
souborovému	souborový	k2eAgInSc3d1
systému	systém	k1gInSc3
serveru	server	k1gInSc2
<g/>
,	,	kIx,
díky	díky	k7c3
němuž	jenž	k3xRgMnSc3
modifikoval	modifikovat	k5eAaBmAgMnS
stránku	stránka	k1gFnSc4
na	na	k7c4
příjem	příjem	k1gInSc4
<g/>
/	/	kIx~
<g/>
zaslání	zaslání	k1gNnSc1
mincí	mince	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgInP
všechny	všechen	k3xTgFnPc4
zasílány	zasílán	k2eAgFnPc4d1
jemu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
již	již	k6eAd1
v	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
intenzita	intenzita	k1gFnSc1
obchodování	obchodování	k1gNnSc2
s	s	k7c7
Dogecoiny	Dogecoin	k1gMnPc7
předčila	předčit	k5eAaBmAgFnS,k5eAaPmAgFnS
objem	objem	k1gInSc4
transakcí	transakce	k1gFnPc2
Bitcoinu	Bitcoin	k1gInSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
kapitalizace	kapitalizace	k1gFnSc1
trhu	trh	k1gInSc2
s	s	k7c7
Dogecoiny	Dogecoin	k1gMnPc7
pohybovala	pohybovat	k5eAaImAgFnS
na	na	k7c6
téměř	téměř	k6eAd1
60	#num#	k4
milionech	milion	k4xCgInPc6
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgMnSc1
nárůst	nárůst	k1gInSc4
neměl	mít	k5eNaImAgMnS
ve	v	k7c6
světě	svět	k1gInSc6
kryptoměn	kryptoměn	k2eAgMnSc1d1
dosud	dosud	k6eAd1
obdoby	obdoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
ledna	leden	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
Dogecoin	Dogecoin	k1gMnSc1
zhodnotil	zhodnotit	k5eAaPmAgMnS
o	o	k7c4
1700	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
svého	svůj	k3xOyFgNnSc2
maxima	maximum	k1gNnSc2
zatím	zatím	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgInS
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
na	na	k7c6
ceně	cena	k1gFnSc6
$	$	kIx~
<g/>
0.0193	0.0193	k4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
tržní	tržní	k2eAgFnSc1d1
kapitalizace	kapitalizace	k1gFnSc1
se	se	k3xPyFc4
vyšplhala	vyšplhat	k5eAaPmAgFnS
na	na	k7c4
2,12	2,12	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dogecoin	Dogecoin	k1gInSc1
ale	ale	k9
není	být	k5eNaImIp3nS
pouze	pouze	k6eAd1
recesistickou	recesistický	k2eAgFnSc4d1
kryptoměnou	kryptoměný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
silná	silný	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
kolem	kolem	k7c2
něj	on	k3xPp3gInSc2
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
také	také	k9
charitě	charita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
založila	založit	k5eAaPmAgFnS
komunita	komunita	k1gFnSc1
sbírku	sbírka	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
získala	získat	k5eAaPmAgFnS
50	#num#	k4
tisíc	tisíc	k4xCgInPc2
USD	USD	kA
pro	pro	k7c4
jamajský	jamajský	k2eAgInSc4d1
bobový	bobový	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
nemohl	moct	k5eNaImAgMnS
si	se	k3xPyFc3
dovolit	dovolit	k5eAaPmF
jet	jet	k5eAaImF
na	na	k7c4
zimní	zimní	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
v	v	k7c6
Soči	Soči	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
dvou	dva	k4xCgInPc2
prvních	první	k4xOgInPc2
dnů	den	k1gInPc2
bylo	být	k5eAaImAgNnS
vybráno	vybrat	k5eAaPmNgNnS
36	#num#	k4
tisíc	tisíc	k4xCgInPc2
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunita	komunita	k1gFnSc1
též	též	k9
uspořádala	uspořádat	k5eAaPmAgFnS
sbírku	sbírka	k1gFnSc4
na	na	k7c4
start	start	k1gInSc4
závodníka	závodník	k1gMnSc2
NASCAR	NASCAR	kA
Joshe	Josh	k1gMnSc2
Wise	Wis	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
deseti	deset	k4xCc2
dní	den	k1gInPc2
do	do	k7c2
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
úspěšně	úspěšně	k6eAd1
vybrala	vybrat	k5eAaPmAgFnS
sponzorský	sponzorský	k2eAgInSc4d1
dar	dar	k1gInSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
55	#num#	k4
tisíc	tisíc	k4xCgInPc2
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
jeho	jeho	k3xOp3gInSc1
vůz	vůz	k1gInSc1
byl	být	k5eAaImAgInS
stylizován	stylizovat	k5eAaImNgInS
do	do	k7c2
tematiky	tematika	k1gFnSc2
Doge	Dog	k1gFnSc2
memu	memus	k1gInSc2
<g/>
,	,	kIx,
Dogecoinu	Dogecoin	k1gInSc2
a	a	k8xC
sociální	sociální	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
reddit	reddit	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc4
a	a	k8xC
logo	logo	k1gNnSc4
</s>
<s>
Logo	logo	k1gNnSc1
Dogecoinu	Dogecoin	k1gInSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
oblíbený	oblíbený	k2eAgInSc1d1
mem	mem	k?
s	s	k7c7
názvem	název	k1gInSc7
Doge	Dog	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
principu	princip	k1gInSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
vyobrazení	vyobrazení	k1gNnSc4
psa	pes	k1gMnSc2
rasy	rasa	k1gFnSc2
Shiba	Shib	k1gMnSc2
Inu	inu	k9
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
k	k	k7c3
obrázku	obrázek	k1gInSc3
přidány	přidán	k2eAgFnPc4d1
interní	interní	k2eAgFnPc4d1
poznámky	poznámka	k1gFnPc4
<g/>
,	,	kIx,
jakoby	jakoby	k8xS
jimi	on	k3xPp3gInPc7
něco	něco	k3yInSc4
konkrétního	konkrétní	k2eAgNnSc2d1
komentoval	komentovat	k5eAaBmAgMnS
právě	právě	k9
onen	onen	k3xDgMnSc1
pes	pes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poznámkách	poznámka	k1gFnPc6
se	se	k3xPyFc4
velice	velice	k6eAd1
četně	četně	k6eAd1
objevují	objevovat	k5eAaImIp3nP
pravopisné	pravopisný	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
a	a	k8xC
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
fontu	font	k1gInSc2
Comic	Comice	k1gInPc2
Sans	Sansa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ukázkový	ukázkový	k2eAgInSc1d1
mem	mem	k?
Doge	Doge	k1gInSc1
</s>
<s>
Tyto	tento	k3xDgFnPc1
memy	mema	k1gFnPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
ve	v	k7c6
více	hodně	k6eAd2
variantách	varianta	k1gFnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
pes	pes	k1gMnSc1
digitálně	digitálně	k6eAd1
zakomponován	zakomponovat	k5eAaPmNgMnS
do	do	k7c2
jiné	jiný	k2eAgFnSc2d1
fotky	fotka	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
jevil	jevit	k5eAaImAgMnS
jako	jako	k9
její	její	k3xOp3gFnSc4
součást	součást	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Dogecoin	Dogecoin	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
kryptoměny	kryptoměn	k2eAgInPc4d1
Litecoin	Litecoin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
tedy	tedy	k9
algoritmus	algoritmus	k1gInSc1
Scrypt	Scrypt	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
decentralizovaný	decentralizovaný	k2eAgMnSc1d1
<g/>
,	,	kIx,
postavený	postavený	k2eAgMnSc1d1
na	na	k7c4
peer-to-peer	peer-to-peer	k1gInSc4
síti	síť	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
všechny	všechen	k3xTgFnPc4
transakce	transakce	k1gFnSc1
zaznamenává	zaznamenávat	k5eAaImIp3nS
do	do	k7c2
bloků	blok	k1gInPc2
v	v	k7c6
blockchainu	blockchain	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záznamy	záznam	k1gInPc7
o	o	k7c6
transakcích	transakce	k1gFnPc6
se	se	k3xPyFc4
ukládají	ukládat	k5eAaImIp3nP
do	do	k7c2
bloků	blok	k1gInPc2
vždy	vždy	k6eAd1
jednou	jeden	k4xCgFnSc7
za	za	k7c4
minutu	minuta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Litecoinu	Litecoin	k1gInSc2
to	ten	k3xDgNnSc1
trvá	trvat	k5eAaImIp3nS
2,5	2,5	k4
minuty	minuta	k1gFnSc2
a	a	k8xC
Bitcoinu	Bitcoin	k1gInSc2
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dogecoin	Dogecoin	k1gInSc1
dokáže	dokázat	k5eAaPmIp3nS
také	také	k9
zpracovat	zpracovat	k5eAaPmF
10	#num#	k4
<g/>
x	x	k?
více	hodně	k6eAd2
transakcí	transakce	k1gFnPc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
než	než	k8xS
Bitcoin	Bitcoin	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poplatky	poplatek	k1gInPc1
za	za	k7c4
transakci	transakce	k1gFnSc4
tedy	tedy	k9
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
mnohonásobně	mnohonásobně	k6eAd1
nižší	nízký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platby	platba	k1gFnSc2
v	v	k7c6
síti	síť	k1gFnSc6
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
levnější	levný	k2eAgFnPc1d2
a	a	k8xC
rychlejší	rychlý	k2eAgFnPc1d2
než	než	k8xS
u	u	k7c2
Bitcoinu	Bitcoin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
Bitcoin	Bitcoin	k2eAgMnSc1d1
i	i	k8xC
Dogecoin	Dogecoin	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
proof-of-work	proof-of-work	k1gInSc1
kryptoměna	kryptoměn	k2eAgFnSc1d1
<g/>
,	,	kIx,
k	k	k7c3
potvrzování	potvrzování	k1gNnSc3
plateb	platba	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
vytváření	vytváření	k1gNnSc1
bloků	blok	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
zabezpečení	zabezpečení	k1gNnSc1
sítě	síť	k1gFnSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
tzv.	tzv.	kA
těžení	těžení	k1gNnSc1
<g/>
,	,	kIx,
řešení	řešení	k1gNnSc1
složitých	složitý	k2eAgFnPc2d1
matematických	matematický	k2eAgFnPc2d1
úloh	úloha	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
zajišťují	zajišťovat	k5eAaImIp3nP
počítače	počítač	k1gInPc1
uživatelů	uživatel	k1gMnPc2
napojených	napojený	k2eAgFnPc2d1
na	na	k7c4
síť	síť	k1gFnSc4
Dogecoin	Dogecoina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžaři	těžař	k1gMnPc7
tak	tak	k6eAd1
nahrazují	nahrazovat	k5eAaImIp3nP
centrální	centrální	k2eAgInSc4d1
server	server	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
obvykle	obvykle	k6eAd1
v	v	k7c6
platebních	platební	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
zpracovává	zpracovávat	k5eAaImIp3nS
data	datum	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžař	těžař	k1gMnSc1
je	být	k5eAaImIp3nS
za	za	k7c4
vyřešení	vyřešení	k1gNnSc4
matematické	matematický	k2eAgFnSc2d1
úlohy	úloha	k1gFnSc2
odměněn	odměnit	k5eAaPmNgInS
několika	několik	k4yIc7
XDG	XDG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
odměny	odměna	k1gFnSc2
za	za	k7c4
vytěžení	vytěžení	k1gNnSc4
bloku	blok	k1gInSc2
byla	být	k5eAaImAgFnS
zpočátku	zpočátku	k6eAd1
dána	dát	k5eAaPmNgFnS
náhodné	náhodný	k2eAgInPc4d1
mezi	mezi	k7c7
0	#num#	k4
a	a	k8xC
maximem	maximum	k1gNnSc7
(	(	kIx(
<g/>
milionem	milion	k4xCgInSc7
XDG	XDG	kA
za	za	k7c4
blok	blok	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
17	#num#	k4
<g/>
.	.	kIx.
březnů	březen	k1gInPc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
počet	počet	k1gInSc1
bloků	blok	k1gInPc2
překročil	překročit	k5eAaPmAgInS
145	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
velikost	velikost	k1gFnSc1
odměny	odměna	k1gFnSc2
stala	stát	k5eAaPmAgFnS
fixní	fixní	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
vytěžení	vytěžení	k1gNnSc6
600	#num#	k4
000	#num#	k4
bloků	blok	k1gInPc2
se	se	k3xPyFc4
navíc	navíc	k6eAd1
odměna	odměna	k1gFnSc1
přestala	přestat	k5eAaPmAgFnS
snižovat	snižovat	k5eAaImF
a	a	k8xC
ustálila	ustálit	k5eAaPmAgFnS
se	se	k3xPyFc4
na	na	k7c4
10	#num#	k4
tisíci	tisíc	k4xCgInPc7
XDG	XDG	kA
za	za	k7c4
blok	blok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
snižování	snižování	k1gNnSc1
odměny	odměna	k1gFnPc1
není	být	k5eNaImIp3nS
v	v	k7c6
plánu	plán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
doba	doba	k1gFnSc1
bloku	blok	k1gInSc2
nezkracovala	zkracovat	k5eNaImAgFnS
ani	ani	k8xC
neprodlužovala	prodlužovat	k5eNaImAgFnS
<g/>
,	,	kIx,
systém	systém	k1gInSc1
po	po	k7c6
každém	každý	k3xTgInSc6
vytěženém	vytěžený	k2eAgInSc6d1
bloku	blok	k1gInSc6
upraví	upravit	k5eAaPmIp3nS
složitost	složitost	k1gFnSc1
těžení	těžení	k1gNnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
neustálou	neustálý	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
výpočetního	výpočetní	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
v	v	k7c6
síti	síť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
složitost	složitost	k1gFnSc1
těžení	těžení	k1gNnSc2
upravovala	upravovat	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
za	za	k7c4
4	#num#	k4
hodiny	hodina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dogecoin	Dogecoin	k1gInSc1
používá	používat	k5eAaImIp3nS
asymetrickou	asymetrický	k2eAgFnSc4d1
kryptografii	kryptografie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatel	uživatel	k1gMnSc1
si	se	k3xPyFc3
nechá	nechat	k5eAaPmIp3nS
vygenerovat	vygenerovat	k5eAaPmF
veřejný	veřejný	k2eAgInSc4d1
a	a	k8xC
privátní	privátní	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejný	veřejný	k2eAgInSc1d1
klíč	klíč	k1gInSc1
poté	poté	k6eAd1
může	moct	k5eAaImIp3nS
bez	bez	k7c2
starostí	starost	k1gFnPc2
šířit	šířit	k5eAaImF
<g/>
,	,	kIx,
důležité	důležitý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
za	za	k7c2
všech	všecek	k3xTgFnPc2
okolností	okolnost	k1gFnPc2
udržet	udržet	k5eAaPmF
v	v	k7c6
tajnosti	tajnost	k1gFnSc6
klíč	klíč	k1gInSc4
privátní	privátní	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
Dogecoin	Dogecoin	k1gInSc4
adresy	adresa	k1gFnSc2
jsou	být	k5eAaImIp3nP
hashovací	hashovací	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
veřejných	veřejný	k2eAgInPc2d1
klíčů	klíč	k1gInPc2
–	–	k?
tedy	tedy	k8xC
řetězce	řetězec	k1gInSc2
34	#num#	k4
znaků	znak	k1gInPc2
a	a	k8xC
čísel	číslo	k1gNnPc2
(	(	kIx(
<g/>
vždy	vždy	k6eAd1
však	však	k9
začínají	začínat	k5eAaImIp3nP
písmenem	písmeno	k1gNnSc7
„	„	k?
<g/>
D	D	kA
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dogecoin	Dogecoina	k1gFnPc2
adresa	adresa	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
můžou	můžou	k?
další	další	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
trhu	trh	k1gInSc2
zasílat	zasílat	k5eAaImF
své	svůj	k3xOyFgFnPc4
mince	mince	k1gFnPc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
např.	např.	kA
následující	následující	k2eAgInSc1d1
tvar	tvar	k1gInSc1
<g/>
:	:	kIx,
DJ	DJ	kA
<g/>
7	#num#	k4
<g/>
zB	zB	k?
<g/>
7	#num#	k4
<g/>
c	c	k0
<g/>
5	#num#	k4
<g/>
BsB	BsB	k1gFnSc6
<g/>
9	#num#	k4
<g/>
UJLy	UJLa	k1gFnSc2
<g/>
1	#num#	k4
<g/>
rKQtY	rKQtY	k?
<g/>
7	#num#	k4
<g/>
c	c	k0
<g/>
6	#num#	k4
<g/>
CQfGiaRLM	CQfGiaRLM	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
konkrétní	konkrétní	k2eAgFnSc1d1
adresa	adresa	k1gFnSc1
patří	patřit	k5eAaImIp3nS
tzv.	tzv.	kA
„	„	k?
<g/>
Dogecoin	Dogecoin	k2eAgInSc4d1
Foundation	Foundation	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Zásoby	zásoba	k1gFnPc1
</s>
<s>
Zatímco	zatímco	k8xS
zpočátku	zpočátku	k6eAd1
byl	být	k5eAaImAgMnS
těžař	těžař	k1gMnSc1
odměněn	odměnit	k5eAaPmNgInS
náhodným	náhodný	k2eAgInSc7d1
počtem	počet	k1gInSc7
mincí	mince	k1gFnPc2
a	a	k8xC
změna	změna	k1gFnSc1
byla	být	k5eAaImAgFnS
plánována	plánovat	k5eAaImNgFnS
až	až	k6eAd1
od	od	k7c2
bloku	blok	k1gInSc2
600	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
fixní	fixní	k2eAgFnSc4d1
odměnu	odměna	k1gFnSc4
byla	být	k5eAaImAgFnS
těžba	těžba	k1gFnSc1
změněna	změnit	k5eAaPmNgFnS
již	již	k6eAd1
od	od	k7c2
bloku	blok	k1gInSc2
145000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
těžbu	těžba	k1gFnSc4
není	být	k5eNaImIp3nS
určený	určený	k2eAgInSc1d1
žádný	žádný	k3yNgInSc1
strop	strop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgFnPc2d1
měn	měna	k1gFnPc2
má	mít	k5eAaImIp3nS
Dogecoin	Dogecoin	k1gInSc1
mnohem	mnohem	k6eAd1
rychlejší	rychlý	k2eAgInSc4d2
plán	plán	k1gInSc4
produkce	produkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
bylo	být	k5eAaImAgNnS
vytěženo	vytěžit	k5eAaPmNgNnS
již	již	k9
100	#num#	k4
miliard	miliarda	k4xCgFnPc2
mincí	mince	k1gFnPc2
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
pokračuje	pokračovat	k5eAaImIp3nS
5,2	5,2	k4
miliardy	miliarda	k4xCgFnSc2
mincí	mince	k1gFnPc2
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
bylo	být	k5eAaImAgNnS
vytěženo	vytěžit	k5eAaPmNgNnS
již	již	k9
114	#num#	k4
601	#num#	k4
845	#num#	k4
018	#num#	k4
Dogecoinů	Dogecoin	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
tržní	tržní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
byla	být	k5eAaImAgFnS
408	#num#	k4
078	#num#	k4
834	#num#	k4
USD	USD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
bloků	blok	k1gInPc2
</s>
<s>
Odměna	odměna	k1gFnSc1
za	za	k7c4
blok	blok	k1gInSc4
</s>
<s>
První	první	k4xOgInSc1
blok	blok	k1gInSc1
</s>
<s>
Očekávaná	očekávaný	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
měny	měna	k1gFnSc2
(	(	kIx(
<g/>
cca	cca	kA
<g/>
)	)	kIx)
</s>
<s>
Očekávaná	očekávaný	k2eAgFnSc1d1
celková	celkový	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
(	(	kIx(
<g/>
cca	cca	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
–	–	k?
<g/>
100	#num#	k4
000	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
000	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
náhodně	náhodně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0,000	0,000	k4
000	#num#	k4
</s>
<s>
50	#num#	k4
000	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
144	#num#	k4
999	#num#	k4
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
500	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
náhodně	náhodně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
250	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
61	#num#	k4
250	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
145	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
200	#num#	k4
000	#num#	k4
</s>
<s>
250	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
fixní	fixní	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
13	#num#	k4
750	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
75	#num#	k4
000	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
000	#num#	k4
</s>
<s>
125	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
fixní	fixní	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
500	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
87	#num#	k4
500	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
300	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
400	#num#	k4
000	#num#	k4
</s>
<s>
62	#num#	k4
500	#num#	k4
(	(	kIx(
<g/>
fixní	fixní	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
250	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
93	#num#	k4
750	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
400	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
500	#num#	k4
000	#num#	k4
</s>
<s>
31	#num#	k4
250	#num#	k4
(	(	kIx(
<g/>
fixní	fixní	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
125	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
96	#num#	k4
875	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
600	#num#	k4
000	#num#	k4
</s>
<s>
15	#num#	k4
625	#num#	k4
(	(	kIx(
<g/>
fixní	fixní	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
562	#num#	k4
000	#num#	k4
000	#num#	k4
</s>
<s>
98	#num#	k4
437	#num#	k4
500	#num#	k4
000	#num#	k4
</s>
<s>
600,001	600,001	k4
<g/>
+	+	kIx~
</s>
<s>
10	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
fixní	fixní	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
200	#num#	k4
000	#num#	k4
000	#num#	k4
za	za	k7c4
rok	rok	k1gInSc4
</s>
<s>
Bez	bez	k7c2
limitu	limit	k1gInSc2
</s>
<s>
Směna	směna	k1gFnSc1
</s>
<s>
Několik	několik	k4yIc4
online	onlinout	k5eAaPmIp3nS
směnáren	směnárna	k1gFnPc2
již	již	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
směnu	směna	k1gFnSc4
DOGE	DOGE	kA
<g/>
/	/	kIx~
<g/>
BTC	BTC	kA
<g/>
(	(	kIx(
<g/>
Bitcoin	Bitcoin	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
DOGE	DOGE	kA
<g/>
/	/	kIx~
<g/>
LTC	LTC	kA
<g/>
(	(	kIx(
<g/>
Litecoin	Litecoin	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směnárna	směnárna	k1gFnSc1
AltQuick	AltQuicka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
co	co	k9
byla	být	k5eAaImAgFnS
první	první	k4xOgFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
obchodování	obchodování	k1gNnSc4
DOGE	DOGE	kA
<g/>
/	/	kIx~
<g/>
USD	USD	kA
<g/>
(	(	kIx(
<g/>
Americký	americký	k2eAgInSc1d1
dolar	dolar	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
natěžené	natěžený	k2eAgFnPc1d1
Dogecoiny	Dogecoina	k1gFnPc1
si	se	k3xPyFc3
tedy	tedy	k9
již	již	k6eAd1
dnes	dnes	k6eAd1
lze	lze	k6eAd1
směnit	směnit	k5eAaPmF
za	za	k7c4
„	„	k?
<g/>
reálné	reálný	k2eAgInPc4d1
peníze	peníz	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chvíli	chvíle	k1gFnSc6
po	po	k7c4
AltQuick	AltQuick	k1gInSc4
<g/>
.	.	kIx.
<g/>
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
k	k	k7c3
trendu	trend	k1gInSc3
přidala	přidat	k5eAaPmAgFnS
i	i	k9
Kanadská	kanadský	k2eAgFnSc1d1
směnárna	směnárna	k1gFnSc1
Vault	Vaultum	k1gNnPc2
of	of	k?
Sathoshi	Sathosh	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
nejen	nejen	k6eAd1
zprostředkovávala	zprostředkovávat	k5eAaImAgFnS
výměnu	výměna	k1gFnSc4
DOGE	DOGE	kA
<g/>
/	/	kIx~
<g/>
USD	USD	kA
<g/>
,	,	kIx,
ale	ale	k8xC
taky	taky	k9
DOGE	DOGE	kA
<g/>
/	/	kIx~
<g/>
CAD	CAD	kA
<g/>
(	(	kIx(
<g/>
Kanadský	kanadský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
únorem	únor	k1gInSc7
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
šířit	šířit	k5eAaImF
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Dogecoin	Dogecoin	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
podporován	podporovat	k5eAaImNgInS
všemi	všecek	k3xTgFnPc7
hlavními	hlavní	k2eAgFnPc7d1
světovými	světový	k2eAgFnPc7d1
měnami	měna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
původně	původně	k6eAd1
hongkongské	hongkongský	k2eAgFnSc2d1
směnárny	směnárna	k1gFnSc2
Asia	Asia	k1gFnSc1
Nexgen	Nexgen	k1gInSc1
podpořila	podpořit	k5eAaPmAgFnS
také	také	k6eAd1
Čínská	čínský	k2eAgFnSc1d1
směnárna	směnárna	k1gFnSc1
BTC	BTC	kA
<g/>
38	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
podepsalo	podepsat	k5eAaPmAgNnS
na	na	k7c6
nárůstu	nárůst	k1gInSc6
tržního	tržní	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
této	tento	k3xDgFnSc2
virtuální	virtuální	k2eAgFnSc2d1
měny	měna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvních	první	k4xOgInPc6
dnech	den	k1gInPc6
se	se	k3xPyFc4
měna	měna	k1gFnSc1
vyšplhala	vyšplhat	k5eAaPmAgFnS
na	na	k7c4
druhou	druhý	k4xOgFnSc4
příčku	příčka	k1gFnSc4
nejčastěji	často	k6eAd3
měněných	měněný	k2eAgFnPc2d1
virtuálních	virtuální	k2eAgFnPc2d1
měn	měna	k1gFnPc2
hned	hned	k6eAd1
za	za	k7c7
Bitcoinem	Bitcoin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
byl	být	k5eAaImAgInS
tržní	tržní	k2eAgInSc1d1
objem	objem	k1gInSc1
skrze	skrze	k?
všechny	všechen	k3xTgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
burzy	burza	k1gFnPc4
odhadován	odhadovat	k5eAaImNgInS
na	na	k7c4
1	#num#	k4
050	#num#	k4
000	#num#	k4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tržní	tržní	k2eAgFnSc1d1
kapitalizace	kapitalizace	k1gFnSc1
byla	být	k5eAaImAgFnS
60	#num#	k4
milionů	milion	k4xCgInPc2
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
většině	většina	k1gFnSc6
celkového	celkový	k2eAgInSc2d1
objemu	objem	k1gInSc2
se	se	k3xPyFc4
podílely	podílet	k5eAaImAgFnP
tři	tři	k4xCgFnPc1
směnárny	směnárna	k1gFnPc1
<g/>
,	,	kIx,
Bter	Bter	k1gInSc1
(	(	kIx(
<g/>
60	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Cryptsy	Crypts	k1gInPc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Vircurex	Vircurex	k1gInSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastější	častý	k2eAgFnSc2d3
výměny	výměna	k1gFnSc2
byly	být	k5eAaImAgInP
zaregistrovány	zaregistrovat	k5eAaPmNgInP
mezi	mezi	k7c4
DOGE	DOGE	kA
<g/>
/	/	kIx~
<g/>
BTC	BTC	kA
(	(	kIx(
<g/>
50	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
DOGE	DOGE	kA
<g/>
/	/	kIx~
<g/>
CNY	CNY	kA
(	(	kIx(
<g/>
44	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
DOGE	DOGE	kA
<g/>
/	/	kIx~
<g/>
LTC	LTC	kA
(	(	kIx(
<g/>
6	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dogecoin	Dogecoin	k1gMnSc1
lze	lze	k6eAd1
nakoupit	nakoupit	k5eAaPmF
v	v	k7c6
online	onlin	k1gInSc5
směnárnách	směnárna	k1gFnPc6
a	a	k8xC
na	na	k7c6
burzách	burza	k1gFnPc6
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
jej	on	k3xPp3gMnSc4
lze	lze	k6eAd1
získat	získat	k5eAaPmF
těžením	těžení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Coiny	Coino	k1gNnPc7
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
uživatel	uživatel	k1gMnSc1
uložit	uložit	k5eAaPmF
ve	v	k7c6
virtuální	virtuální	k2eAgFnSc6d1
peněžence	peněženka	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
online	onlin	k1gInSc5
peněženkách	peněženka	k1gFnPc6
nebo	nebo	k8xC
například	například	k6eAd1
v	v	k7c6
některých	některý	k3yIgFnPc6
směnárnách	směnárna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internetové	internetový	k2eAgFnSc2d1
peněženky	peněženka	k1gFnSc2
a	a	k8xC
úložiště	úložiště	k1gNnSc2
kryptoměn	kryptoměn	k2eAgMnSc1d1
se	se	k3xPyFc4
ale	ale	k8xC
už	už	k6eAd1
několikrát	několikrát	k6eAd1
staly	stát	k5eAaPmAgFnP
terčem	terč	k1gInSc7
útoku	útok	k1gInSc2
hackerů	hacker	k1gMnPc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
uživatelé	uživatel	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
svoje	svůj	k3xOyFgMnPc4
coiny	coin	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodně	rozhodně	k6eAd1
se	se	k3xPyFc4
tak	tak	k9
vyplatí	vyplatit	k5eAaPmIp3nS
vyhledat	vyhledat	k5eAaPmF
co	co	k9
nejbezpečnější	bezpečný	k2eAgInSc4d3
způsob	způsob	k1gInSc4
pro	pro	k7c4
uložení	uložení	k1gNnSc4
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
hodnota	hodnota	k1gFnSc1
kryptoměn	kryptoměn	k2eAgInSc1d1
poměrně	poměrně	k6eAd1
nestabilní	stabilní	k2eNgInSc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
kryptoměny	kryptomět	k5eAaPmNgFnP
často	často	k6eAd1
využívány	využíván	k2eAgFnPc1d1
při	při	k7c6
obchodování	obchodování	k1gNnSc6
nebo	nebo	k8xC
jako	jako	k9
dlouhodobá	dlouhodobý	k2eAgFnSc1d1
investice	investice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodování	obchodování	k1gNnSc1
s	s	k7c7
měnou	měna	k1gFnSc7
Dogecoin	Dogecoina	k1gFnPc2
prostřednictvím	prostřednictvím	k7c2
CFD	CFD	kA
zatím	zatím	k6eAd1
žádný	žádný	k3yNgMnSc1
z	z	k7c2
regulovaných	regulovaný	k2eAgMnPc2d1
brokerů	broker	k1gMnPc2
bohužel	bohužel	k9
nepodporuje	podporovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodovat	obchodovat	k5eAaImF
lze	lze	k6eAd1
ale	ale	k8xC
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
ostatních	ostatní	k2eAgFnPc2d1
virtuálních	virtuální	k2eAgFnPc2d1
měn	měna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Fundraising	Fundraising	k1gInSc1
</s>
<s>
Nadace	nadace	k1gFnSc1
Dogecoin	Dogecoina	k1gFnPc2
a	a	k8xC
komunita	komunita	k1gFnSc1
této	tento	k3xDgFnSc2
virtuální	virtuální	k2eAgFnSc2d1
měny	měna	k1gFnSc2
stojí	stát	k5eAaImIp3nS
i	i	k9
za	za	k7c7
několika	několik	k4yIc7
fundraisingy	fundraising	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	nejvíce	k6eAd1,k6eAd3
v	v	k7c6
povědomí	povědomí	k1gNnSc6
jsou	být	k5eAaImIp3nP
fundraisingy	fundraising	k1gInPc4
spojené	spojený	k2eAgInPc4d1
se	se	k3xPyFc4
Zimní	zimní	k2eAgFnSc7d1
olympiádou	olympiáda	k1gFnSc7
v	v	k7c6
Soči	Soči	k1gNnSc6
<g/>
,	,	kIx,
Světový	světový	k2eAgInSc4d1
den	den	k1gInSc4
vody	voda	k1gFnSc2
a	a	k8xC
podpora	podpora	k1gFnSc1
jezdce	jezdec	k1gInSc2
NASCAR	NASCAR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1
olympiáda	olympiáda	k1gFnSc1
v	v	k7c6
Soči	Soči	k1gNnSc6
</s>
<s>
Nadace	nadace	k1gFnSc1
Dogecoin	Dogecoina	k1gFnPc2
společně	společně	k6eAd1
s	s	k7c7
komunitním	komunitní	k2eAgNnSc7d1
zapojením	zapojení	k1gNnSc7
podpořili	podpořit	k5eAaPmAgMnP
jamajský	jamajský	k2eAgInSc4d1
bobový	bobový	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
si	se	k3xPyFc3
nemohl	moct	k5eNaImAgInS
dovolit	dovolit	k5eAaPmF
zúčastnit	zúčastnit	k5eAaPmF
se	se	k3xPyFc4
zimní	zimní	k2eAgFnSc2d1
olympiády	olympiáda	k1gFnSc2
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zdárně	zdárně	k6eAd1
kvalifikoval	kvalifikovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
účelem	účel	k1gInSc7
jeho	jeho	k3xOp3gFnSc2
podpory	podpora	k1gFnSc2
se	se	k3xPyFc4
vybralo	vybrat	k5eAaPmAgNnS
50	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
30	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
bylo	být	k5eAaImAgNnS
vybráno	vybrat	k5eAaPmNgNnS
v	v	k7c6
prvních	první	k4xOgNnPc6
dvou	dva	k4xCgNnPc6
dnech	den	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Doge	Doge	k6eAd1
<g/>
4	#num#	k4
<g/>
Water	Watra	k1gFnPc2
</s>
<s>
Dalším	další	k2eAgInSc7d1
významným	významný	k2eAgInSc7d1
projektem	projekt	k1gInSc7
byl	být	k5eAaImAgInS
fundraising	fundraising	k1gInSc1
na	na	k7c4
vybudování	vybudování	k1gNnSc4
studny	studna	k1gFnSc2
v	v	k7c6
povodí	povodí	k1gNnSc6
řeky	řeka	k1gFnSc2
Tana	tanout	k5eAaImSgInS
v	v	k7c6
Keni	Keňa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
vybrat	vybrat	k5eAaPmF
30	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
zhruba	zhruba	k6eAd1
40	#num#	k4
milionů	milion	k4xCgInPc2
Dogecoin	Dogecoina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částku	částka	k1gFnSc4
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
vybrali	vybrat	k5eAaPmAgMnP
díky	díky	k7c3
anonymnímu	anonymní	k2eAgInSc3d1
příspěvku	příspěvek	k1gInSc3
11	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
14	#num#	k4
milionů	milion	k4xCgInPc2
Dogecoinů	Dogecoin	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
NASCAR	NASCAR	kA
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
bylo	být	k5eAaImAgNnS
komunitně	komunitně	k6eAd1
vybráno	vybrat	k5eAaPmNgNnS
55	#num#	k4
000	#num#	k4
dolarů	dolar	k1gInPc2
(	(	kIx(
<g/>
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
67,8	67,8	k4
milionů	milion	k4xCgInPc2
Dogecoinů	Dogecoin	k1gInPc2
<g/>
)	)	kIx)
jako	jako	k8xC,k8xS
sponzorský	sponzorský	k2eAgInSc1d1
dar	dar	k1gInSc1
pro	pro	k7c4
NASCAR	NASCAR	kA
závodníka	závodník	k1gMnSc2
Joshe	Josh	k1gMnSc2
Wisea	Wiseus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závodě	závod	k1gInSc6
tehdy	tehdy	k6eAd1
skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
dvacáté	dvacátý	k4xOgFnSc6
pozici	pozice	k1gFnSc6
<g/>
,	,	kIx,
dostal	dostat	k5eAaPmAgMnS
se	se	k3xPyFc4
však	však	k9
do	do	k7c2
hledáčku	hledáček	k1gInSc2
internetové	internetový	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
a	a	k8xC
právě	právě	k9
díky	díky	k7c3
její	její	k3xOp3gFnSc3
obrovské	obrovský	k2eAgFnSc3d1
podpoře	podpora	k1gFnSc3
při	pře	k1gFnSc3
online	onlinout	k5eAaPmIp3nS
hlasování	hlasování	k1gNnSc3
získal	získat	k5eAaPmAgInS
i	i	k9
startovní	startovní	k2eAgFnSc4d1
příčku	příčka	k1gFnSc4
na	na	k7c4
Sprint	sprint	k1gInSc4
All-Star	All-Stara	k1gFnPc2
Race	Rac	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dojel	dojet	k5eAaPmAgMnS
na	na	k7c6
patnácté	patnáctý	k4xOgFnSc6
pozici	pozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Dogecoin	Dogecoina	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Recesistická	recesistický	k2eAgFnSc1d1
kryptoměna	kryptoměn	k2eAgFnSc1d1
Dogecoin	Dogecoin	k1gMnSc1
letí	letět	k5eAaImIp3nS
vzhůru	vzhůru	k6eAd1
<g/>
,	,	kIx,
už	už	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c4
2	#num#	k4
miliardách	miliarda	k4xCgFnPc6
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-01-08	2018-01-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Dogecoin	Dogecoin	k2eAgInSc1d1
–	–	k?
Vznik	vznik	k1gInSc1
<g/>
,	,	kIx,
kurz	kurz	k1gInSc1
<g/>
,	,	kIx,
graf	graf	k1gInSc1
<g/>
,	,	kIx,
mining	mining	k1gInSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finex	Finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Map	mapa	k1gFnPc2
of	of	k?
coins	coinsa	k1gFnPc2
<g/>
:	:	kIx,
the	the	k?
history	histor	k1gInPc1
of	of	k?
cryptocurrencies	cryptocurrencies	k1gInSc1
from	from	k1gMnSc1
bitcoin	bitcoin	k1gMnSc1
to	ten	k3xDgNnSc4
dogecoin	dogecoin	k1gMnSc1
and	and	k?
more	mor	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Map	mapa	k1gFnPc2
of	of	k?
coins	coins	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Dogechain	Dogechain	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
official	official	k1gMnSc1
dogecoin	dogecoin	k1gMnSc1
blockchain	blockchain	k1gMnSc1
explorer	explorer	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
dogechain	dogechain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
dogecoin	dogecoin	k1gMnSc1
<g/>
/	/	kIx~
<g/>
dogecoin	dogecoin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
GitHub	GitHub	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dogecoin	Dogecoin	k1gMnSc1
(	(	kIx(
<g/>
DOGE	DOGE	kA
<g/>
)	)	kIx)
price	price	k1gMnSc1
<g/>
,	,	kIx,
charts	charts	k1gInSc1
<g/>
,	,	kIx,
market	market	k1gInSc1
cap	capa	k1gFnPc2
<g/>
,	,	kIx,
and	and	k?
other	other	k1gInSc1
metrics	metrics	k1gInSc1
|	|	kIx~
CoinMarketCap	CoinMarketCap	k1gInSc1
<g/>
.	.	kIx.
coinmarketcap	coinmarketcap	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dogechain	Dogechain	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
official	official	k1gMnSc1
dogecoin	dogecoin	k1gMnSc1
blockchain	blockchain	k1gMnSc1
explorer	explorer	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
dogechain	dogechain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dogechain	Dogechain	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
official	official	k1gMnSc1
dogecoin	dogecoin	k1gMnSc1
blockchain	blockchain	k1gMnSc1
explorer	explorer	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
dogechain	dogechain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dogechain	Dogechain	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
official	official	k1gMnSc1
dogecoin	dogecoin	k1gMnSc1
blockchain	blockchain	k1gMnSc1
explorer	explorer	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
dogechain	dogechain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dogechain	Dogechain	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
official	official	k1gMnSc1
dogecoin	dogecoin	k1gMnSc1
blockchain	blockchain	k1gMnSc1
explorer	explorer	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
dogechain	dogechain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dogechain	Dogechain	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
official	official	k1gMnSc1
dogecoin	dogecoin	k1gMnSc1
blockchain	blockchain	k1gMnSc1
explorer	explorer	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
dogechain	dogechain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dogechain	Dogechain	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
official	official	k1gMnSc1
dogecoin	dogecoin	k1gMnSc1
blockchain	blockchain	k1gMnSc1
explorer	explorer	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
dogechain	dogechain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dogechain	Dogechain	k1gMnSc1
–	–	k?
The	The	k1gMnSc1
official	official	k1gMnSc1
dogecoin	dogecoin	k1gMnSc1
blockchain	blockchain	k1gMnSc1
explorer	explorer	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
dogechain	dogechain	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Peer-to-peer	Peer-to-peer	k1gMnSc1
</s>
<s>
Kryptoměna	Kryptoměn	k2eAgFnSc1d1
</s>
<s>
Litecoin	Litecoin	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Dogecoin	Dogecoin	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Dogecoin	Dogecoin	k1gInSc1
-	-	kIx~
kurz	kurz	k1gInSc1
<g/>
,	,	kIx,
graf	graf	k1gInSc1
a	a	k8xC
užitecné	užitecný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kryptoměny	Kryptoměn	k2eAgInPc1d1
založené	založený	k2eAgInPc1d1
na	na	k7c4
SHA-256	SHA-256	k1gFnSc4
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
•	•	k?
Bitcoin	Bitcoin	k1gMnSc1
cash	cash	k1gFnSc2
•	•	k?
Peercoin	Peercoin	k1gInSc1
•	•	k?
Namecoin	Namecoina	k1gFnPc2
založené	založený	k2eAgFnPc4d1
na	na	k7c6
Scryptu	Scrypt	k1gInSc6
</s>
<s>
Auroracoin	Auroracoin	k1gMnSc1
•	•	k?
Dogecoin	Dogecoin	k1gMnSc1
•	•	k?
Litecoin	Litecoin	k1gMnSc1
•	•	k?
PotCoin	PotCoin	k1gMnSc1
založené	založený	k2eAgInPc4d1
na	na	k7c4
Zerocoinu	Zerocoina	k1gFnSc4
</s>
<s>
Zcoin	Zcoin	k1gMnSc1
•	•	k?
ZeroVert	ZeroVert	k1gMnSc1
•	•	k?
Anoncoin	Anoncoin	k1gMnSc1
•	•	k?
SmartCash	SmartCash	k1gMnSc1
•	•	k?
PIVX	PIVX	kA
•	•	k?
Zcash	Zcash	k1gInSc1
•	•	k?
Komodo	komoda	k1gFnSc5
založené	založený	k2eAgInPc1d1
na	na	k7c4
CryptoNote	CryptoNot	k1gInSc5
</s>
<s>
Bytecoin	Bytecoin	k1gMnSc1
•	•	k?
Monero	Monero	k1gNnSc4
•	•	k?
DigitalNote	DigitalNot	k1gInSc5
•	•	k?
Boolberry	Boolberr	k1gInPc1
založené	založený	k2eAgInPc1d1
na	na	k7c4
Ethash	Ethash	k1gInSc4
</s>
<s>
Ethereum	Ethereum	k1gInSc1
•	•	k?
Ethereum	Ethereum	k1gInSc1
Classic	Classic	k1gMnSc1
•	•	k?
Ubiq	Ubiq	k1gMnSc1
Jiné	jiná	k1gFnSc2
proof-of-work	proof-of-work	k1gInSc1
skripty	skript	k1gInPc4
</s>
<s>
Dash	Dash	k1gMnSc1
•	•	k?
Primecoin	Primecoin	k1gMnSc1
•	•	k?
Digibyte	Digibyt	k1gInSc5
Bez	bez	k1gInSc4
proof-of-work	proof-of-work	k1gInSc1
skriptu	skript	k1gInSc2
</s>
<s>
BlackCoin	BlackCoin	k1gMnSc1
•	•	k?
Burstcoin	Burstcoin	k1gMnSc1
•	•	k?
Counterparty	Counterparta	k1gFnSc2
•	•	k?
Enigma	enigma	k1gFnSc1
•	•	k?
FunFair	FunFair	k1gMnSc1
•	•	k?
Gridcoin	Gridcoin	k1gMnSc1
•	•	k?
Lisk	Lisk	k1gMnSc1
•	•	k?
Melonport	Melonport	k1gInSc1
•	•	k?
NEM	NEM	kA
•	•	k?
NEO	NEO	kA
•	•	k?
Nxt	Nxt	k1gMnSc1
•	•	k?
OmiseGO	OmiseGO	k1gMnSc1
•	•	k?
Polkadot	Polkadot	k1gMnSc1
•	•	k?
Qtum	Qtum	k1gMnSc1
•	•	k?
RChain	RChain	k1gMnSc1
•	•	k?
Ripple	Ripple	k1gMnSc1
•	•	k?
Simple	Simple	k1gMnSc1
Token	Token	k2eAgMnSc1d1
•	•	k?
Stellar	Stellar	k1gMnSc1
•	•	k?
Shadow	Shadow	k1gMnSc1
Technologie	technologie	k1gFnSc2
</s>
<s>
Blockchain	Blockchain	k2eAgInSc1d1
•	•	k?
Proof-of-stake	Proof-of-stake	k1gInSc1
•	•	k?
Proof-of-work	Proof-of-work	k1gInSc1
system	syst	k1gInSc7
•	•	k?
Zerocash	Zerocash	k1gInSc1
•	•	k?
Zerocoin	Zerocoin	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Anonymní	anonymní	k2eAgFnSc1d1
internetové	internetový	k2eAgNnSc4d1
bankovnictví	bankovnictví	k1gNnSc4
•	•	k?
Kryptoanarchismus	Kryptoanarchismus	k1gInSc1
•	•	k?
Digitální	digitální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Digital	Digital	kA
currency	currenca	k1gFnPc1
exchanger	exchanger	k1gMnSc1
•	•	k?
Double-spending	Double-spending	k1gInSc1
•	•	k?
Virtuální	virtuální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
</s>
