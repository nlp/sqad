<s desamb="1">
Dogecoin	Dogecoin	k1gInSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
zkratkami	zkratka	k1gFnPc7
DOGE	DOGE	kA
nebo	nebo	k8xC
XDG	XDG	kA
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
symbolem	symbol	k1gInSc7
Ð	Ð	k?
Síť	síť	k1gFnSc1
má	mít	k5eAaImIp3nS
veřejně	veřejně	k6eAd1
přístupný	přístupný	k2eAgInSc1d1
zdrojový	zdrojový	k2eAgInSc1d1
kód	kód	k1gInSc1
(	(	kIx(
<g/>
open	open	k1gInSc1
source	sourec	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
decentralizovaná	decentralizovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
centrální	centrální	k2eAgInSc1d1
server	server	k1gInSc1
<g/>
,	,	kIx,
přes	přes	k7c4
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
by	by	kYmCp3nP
procházely	procházet	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
transakce	transakce	k1gFnPc1
<g/>
.	.	kIx.
</s>