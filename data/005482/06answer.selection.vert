<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ovládají	ovládat	k5eAaImIp3nP	ovládat
prakticky	prakticky	k6eAd1	prakticky
všichni	všechen	k3xTgMnPc1	všechen
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
autonomní	autonomní	k2eAgInPc1d1	autonomní
celky	celek	k1gInPc1	celek
vedle	vedle	k7c2	vedle
ruštiny	ruština	k1gFnSc2	ruština
používají	používat	k5eAaImIp3nP	používat
místní	místní	k2eAgInPc1d1	místní
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
tatarština	tatarština	k1gFnSc1	tatarština
<g/>
,	,	kIx,	,
ukrajinština	ukrajinština	k1gFnSc1	ukrajinština
<g/>
,	,	kIx,	,
čuvaština	čuvaština	k1gFnSc1	čuvaština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
celkem	celkem	k6eAd1	celkem
tak	tak	k9	tak
Rusko	Rusko	k1gNnSc4	Rusko
oficiálně	oficiálně	k6eAd1	oficiálně
hovoří	hovořit	k5eAaImIp3nP	hovořit
31	[number]	k4	31
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
