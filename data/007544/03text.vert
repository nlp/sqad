<s>
Lužánky	Lužánka	k1gFnPc1	Lužánka
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Augarten	Augarten	k2eAgInSc1d1	Augarten
<g/>
;	;	kIx,	;
v	v	k7c6	v
hantecu	hantecus	k1gInSc6	hantecus
Augec	Augec	k1gInSc1	Augec
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
brněnským	brněnský	k2eAgInSc7d1	brněnský
městským	městský	k2eAgInSc7d1	městský
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Brno-střed	Brnotřed	k1gInSc4	Brno-střed
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
hektarů	hektar	k1gInPc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejstarší	starý	k2eAgNnSc4d3	nejstarší
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
otevřený	otevřený	k2eAgInSc4d1	otevřený
městský	městský	k2eAgInSc4d1	městský
park	park	k1gInSc4	park
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
prohlášený	prohlášený	k2eAgMnSc1d1	prohlášený
též	též	k9	též
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Lužánkách	Lužánka	k1gFnPc6	Lužánka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
brněnský	brněnský	k2eAgMnSc1d1	brněnský
měšťan	měšťan	k1gMnSc1	měšťan
Niger	Niger	k1gInSc1	Niger
(	(	kIx(	(
<g/>
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
)	)	kIx)	)
věnoval	věnovat	k5eAaPmAgMnS	věnovat
"	"	kIx"	"
<g/>
lužnou	lužný	k2eAgFnSc4d1	lužný
louku	louka	k1gFnSc4	louka
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
postavený	postavený	k2eAgInSc4d1	postavený
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
dvorec	dvorec	k1gInSc4	dvorec
nedaleko	nedaleko	k7c2	nedaleko
brněnských	brněnský	k2eAgFnPc2d1	brněnská
hradeb	hradba	k1gFnPc2	hradba
herburskému	herburský	k2eAgInSc3d1	herburský
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přebírají	přebírat	k5eAaImIp3nP	přebírat
klášter	klášter	k1gInSc1	klášter
i	i	k9	i
dvorec	dvorec	k1gInSc1	dvorec
jezuité	jezuita	k1gMnPc1	jezuita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
budují	budovat	k5eAaImIp3nP	budovat
kapli	kaple	k1gFnSc4	kaple
a	a	k8xC	a
okrasnou	okrasný	k2eAgFnSc4d1	okrasná
zahradu	zahrada	k1gFnSc4	zahrada
pro	pro	k7c4	pro
meditace	meditace	k1gFnPc4	meditace
<g/>
,	,	kIx,	,
duševní	duševní	k2eAgFnSc4d1	duševní
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
klášter	klášter	k1gInSc1	klášter
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
prostor	prostor	k1gInSc1	prostor
byl	být	k5eAaImAgInS	být
Josefem	Josef	k1gMnSc7	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
věnován	věnovat	k5eAaImNgInS	věnovat
městu	město	k1gNnSc3	město
Brnu	Brno	k1gNnSc6	Brno
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
veřejných	veřejný	k2eAgInPc2d1	veřejný
parků	park	k1gInPc2	park
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
úpravu	úprava	k1gFnSc4	úprava
uvolnil	uvolnit	k5eAaPmAgMnS	uvolnit
i	i	k9	i
svého	svůj	k3xOyFgMnSc4	svůj
dvorního	dvorní	k2eAgMnSc4d1	dvorní
zahradníka	zahradník	k1gMnSc4	zahradník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
místo	místo	k7c2	místo
zchátralého	zchátralý	k2eAgInSc2d1	zchátralý
dvorce	dvorec	k1gInSc2	dvorec
dal	dát	k5eAaPmAgInS	dát
postavit	postavit	k5eAaPmF	postavit
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
kolonády	kolonáda	k1gFnPc4	kolonáda
proti	proti	k7c3	proti
dešti	dešť	k1gInSc3	dešť
s	s	k7c7	s
výletní	výletní	k2eAgFnSc7d1	výletní
restaurací	restaurace	k1gFnSc7	restaurace
a	a	k8xC	a
Lužánky	Lužánek	k1gInPc1	Lužánek
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
staly	stát	k5eAaPmAgInP	stát
výletním	výletní	k2eAgInSc7d1	výletní
a	a	k8xC	a
rekreačním	rekreační	k2eAgNnSc7d1	rekreační
místem	místo	k1gNnSc7	místo
Brňanů	Brňan	k1gMnPc2	Brňan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
odpálen	odpálit	k5eAaPmNgInS	odpálit
první	první	k4xOgInSc1	první
ohňostroj	ohňostroj	k1gInSc1	ohňostroj
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
dal	dát	k5eAaPmAgMnS	dát
Lužánkám	Lužánka	k1gFnPc3	Lužánka
brněnský	brněnský	k2eAgMnSc1d1	brněnský
městský	městský	k2eAgMnSc1d1	městský
zahradník	zahradník	k1gMnSc1	zahradník
Antonín	Antonín	k1gMnSc1	Antonín
Šebánek	Šebánek	k1gMnSc1	Šebánek
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
původní	původní	k2eAgInSc1d1	původní
francouzský	francouzský	k2eAgInSc1d1	francouzský
park	park	k1gInSc1	park
na	na	k7c4	na
park	park	k1gInSc4	park
přírodního	přírodní	k2eAgInSc2d1	přírodní
charakteru	charakter	k1gInSc2	charakter
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
vzácnými	vzácný	k2eAgFnPc7d1	vzácná
dřevinami	dřevina	k1gFnPc7	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
byl	být	k5eAaImAgInS	být
park	park	k1gInSc4	park
stavovským	stavovský	k2eAgInSc7d1	stavovský
sněmem	sněm	k1gInSc7	sněm
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
národní	národní	k2eAgFnSc7d1	národní
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
náleželo	náležet	k5eAaImAgNnS	náležet
území	území	k1gNnSc1	území
parku	park	k1gInSc2	park
ke	k	k7c3	k
katastru	katastr	k1gInSc3	katastr
Velké	velký	k2eAgFnSc2d1	velká
Nové	Nové	k2eAgFnSc2d1	Nové
Ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
Červené	Červená	k1gFnSc2	Červená
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
radikální	radikální	k2eAgFnSc2d1	radikální
katastrální	katastrální	k2eAgFnSc2d1	katastrální
reformy	reforma	k1gFnSc2	reforma
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
pak	pak	k6eAd1	pak
náleželo	náležet	k5eAaImAgNnS	náležet
jeho	jeho	k3xOp3gNnSc1	jeho
území	území	k1gNnSc1	území
ke	k	k7c3	k
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Lužánky	Lužánka	k1gFnSc2	Lužánka
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnPc2	součást
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1979	[number]	k4	1979
náleží	náležet	k5eAaImIp3nP	náležet
území	území	k1gNnSc4	území
parku	park	k1gInSc2	park
ke	k	k7c3	k
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Černá	černý	k2eAgFnSc1d1	černá
Pole	pole	k1gFnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgNnPc4	tento
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Černá	černý	k2eAgFnSc1d1	černá
Pole	pole	k1gFnSc1	pole
součástí	součást	k1gFnSc7	součást
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1850	[number]	k4	1850
se	se	k3xPyFc4	se
park	park	k1gInSc1	park
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
tehdejším	tehdejší	k2eAgNnSc7d1	tehdejší
předměstím	předměstí	k1gNnSc7	předměstí
a	a	k8xC	a
katastrálním	katastrální	k2eAgNnSc7d1	katastrální
územím	území	k1gNnSc7	území
Velkou	velký	k2eAgFnSc7d1	velká
Novou	nový	k2eAgFnSc7d1	nová
Ulicí	ulice	k1gFnSc7	ulice
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
stržené	stržený	k2eAgFnSc2d1	stržená
restaurace	restaurace	k1gFnSc2	restaurace
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
architekta	architekt	k1gMnSc2	architekt
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Förstera	Förster	k1gMnSc2	Förster
postaven	postaven	k2eAgInSc4d1	postaven
novorenesanční	novorenesanční	k2eAgInSc4d1	novorenesanční
pavilon	pavilon	k1gInSc4	pavilon
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
sálem	sál	k1gInSc7	sál
pro	pro	k7c4	pro
plesy	ples	k1gInPc4	ples
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
slavnosti	slavnost	k1gFnPc4	slavnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948-1949	[number]	k4	1948-1949
změněný	změněný	k2eAgInSc1d1	změněný
adaptacemi	adaptace	k1gFnPc7	adaptace
a	a	k8xC	a
přístavbou	přístavba	k1gFnSc7	přístavba
severního	severní	k2eAgNnSc2d1	severní
křídla	křídlo	k1gNnSc2	křídlo
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
/	/	kIx~	/
<g/>
50	[number]	k4	50
zde	zde	k6eAd1	zde
začalo	začít	k5eAaPmAgNnS	začít
pracovat	pracovat	k5eAaImF	pracovat
18	[number]	k4	18
zájmových	zájmový	k2eAgInPc2d1	zájmový
kroužků	kroužek	k1gInPc2	kroužek
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
175	[number]	k4	175
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Náplň	náplň	k1gFnSc1	náplň
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
i	i	k8xC	i
rozsah	rozsah	k1gInSc1	rozsah
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
s	s	k7c7	s
přibývajícími	přibývající	k2eAgInPc7d1	přibývající
lety	let	k1gInPc7	let
proměňovaly	proměňovat	k5eAaImAgFnP	proměňovat
podle	podle	k7c2	podle
společenské	společenský	k2eAgFnSc2d1	společenská
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
poptávky	poptávka	k1gFnSc2	poptávka
i	i	k8xC	i
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Pamětníci	pamětník	k1gMnPc1	pamětník
si	se	k3xPyFc3	se
vzpomínají	vzpomínat	k5eAaImIp3nP	vzpomínat
na	na	k7c4	na
slevačskou	slevačský	k2eAgFnSc4d1	slevačský
<g/>
,	,	kIx,	,
tkalcovskou	tkalcovský	k2eAgFnSc4d1	tkalcovská
či	či	k8xC	či
tiskařskou	tiskařský	k2eAgFnSc4d1	tiskařská
dílnu	dílna	k1gFnSc4	dílna
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnPc1d1	dnešní
děti	dítě	k1gFnPc1	dítě
si	se	k3xPyFc3	se
současných	současný	k2eAgNnPc2d1	současné
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc1	dva
stě	sto	k4xCgFnPc1	sto
kroužků	kroužek	k1gInPc2	kroužek
nedovedou	dovést	k5eNaPmIp3nP	dovést
představit	představit	k5eAaPmF	představit
bez	bez	k7c2	bez
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
country	country	k2eAgFnPc2d1	country
tanečních	taneční	k1gFnPc2	taneční
či	či	k8xC	či
jógy	jóga	k1gFnSc2	jóga
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
stavbami	stavba	k1gFnPc7	stavba
v	v	k7c6	v
parku	park	k1gInSc6	park
jsou	být	k5eAaImIp3nP	být
kašna	kašna	k1gFnSc1	kašna
se	s	k7c7	s
sousoším	sousoší	k1gNnSc7	sousoší
tří	tři	k4xCgInPc2	tři
puttů	putt	k1gInPc2	putt
(	(	kIx(	(
<g/>
andílků	andílek	k1gMnPc2	andílek
<g/>
)	)	kIx)	)
z	z	k7c2	z
r.	r.	kA	r.
1860	[number]	k4	1860
od	od	k7c2	od
F.	F.	kA	F.
Melnitzkého	Melnitzký	k1gMnSc2	Melnitzký
(	(	kIx(	(
<g/>
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
pomník	pomník	k1gInSc4	pomník
Josefa	Josef	k1gMnSc2	Josef
Merhauta	Merhaut	k1gMnSc2	Merhaut
z	z	k7c2	z
r.	r.	kA	r.
1929	[number]	k4	1929
od	od	k7c2	od
Emila	Emil	k1gMnSc2	Emil
Hlavici	Hlavica	k1gMnSc2	Hlavica
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
přesunut	přesunout	k5eAaPmNgInS	přesunout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stál	stát	k5eAaImAgMnS	stát
zde	zde	k6eAd1	zde
též	též	k9	též
pomník	pomník	k1gInSc4	pomník
zakladatele	zakladatel	k1gMnSc2	zakladatel
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
císaře	císař	k1gMnSc2	císař
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
zničen	zničit	k5eAaPmNgInS	zničit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
replikou	replika	k1gFnSc7	replika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
