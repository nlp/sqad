<s>
Mudrci	mudrc	k1gMnPc1	mudrc
z	z	k7c2	z
Východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xS	jako
Tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
postavy	postava	k1gFnPc1	postava
z	z	k7c2	z
Matoušova	Matoušův	k2eAgNnSc2d1	Matoušovo
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
navštívily	navštívit	k5eAaPmAgFnP	navštívit
Ježíše	Ježíš	k1gMnPc4	Ježíš
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
a	a	k8xC	a
přinesly	přinést	k5eAaPmAgInP	přinést
mu	on	k3xPp3gMnSc3	on
dary	dar	k1gInPc4	dar
<g/>
:	:	kIx,	:
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
kadidlo	kadidlo	k1gNnSc4	kadidlo
a	a	k8xC	a
myrhu	myrha	k1gFnSc4	myrha
<g/>
.	.	kIx.	.
</s>
