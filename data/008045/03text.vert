<s>
Mudrci	mudrc	k1gMnPc1	mudrc
z	z	k7c2	z
Východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xC	jako
Tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
postavy	postava	k1gFnPc1	postava
z	z	k7c2	z
Matoušova	Matoušův	k2eAgNnSc2d1	Matoušovo
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
navštívily	navštívit	k5eAaPmAgFnP	navštívit
Ježíše	Ježíš	k1gMnPc4	Ježíš
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
a	a	k8xC	a
přinesly	přinést	k5eAaPmAgInP	přinést
mu	on	k3xPp3gMnSc3	on
dary	dar	k1gInPc4	dar
<g/>
:	:	kIx,	:
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
kadidlo	kadidlo	k1gNnSc4	kadidlo
a	a	k8xC	a
myrhu	myrha	k1gFnSc4	myrha
<g/>
.	.	kIx.	.
</s>
<s>
Upřesnění	upřesnění	k1gNnSc1	upřesnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
tři	tři	k4xCgMnPc1	tři
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
králové	král	k1gMnPc1	král
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Melichar	Melichar	k1gMnSc1	Melichar
a	a	k8xC	a
Baltazar	Baltazar	k1gMnSc1	Baltazar
<g/>
,	,	kIx,	,
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
pozdější	pozdní	k2eAgFnPc4d2	pozdější
tradice	tradice	k1gFnPc4	tradice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
zákoně	zákon	k1gInSc6	zákon
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
údaje	údaj	k1gInPc1	údaj
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
liturgické	liturgický	k2eAgFnSc6d1	liturgická
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
svátek	svátek	k1gInSc1	svátek
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
ztotožněn	ztotožněn	k2eAgMnSc1d1	ztotožněn
se	s	k7c7	s
slavností	slavnost	k1gFnSc7	slavnost
Zjevení	zjevení	k1gNnSc2	zjevení
Páně	páně	k2eAgNnSc2d1	páně
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
i	i	k8xC	i
Východě	východ	k1gInSc6	východ
slaví	slavit	k5eAaImIp3nS	slavit
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
podstatou	podstata	k1gFnSc7	podstata
totožný	totožný	k2eAgInSc1d1	totožný
se	se	k3xPyFc4	se
slavností	slavnost	k1gFnSc7	slavnost
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dvanáctým	dvanáctý	k4xOgNnSc7	dvanáctý
dnem	dno	k1gNnSc7	dno
Vánoc	Vánoce	k1gFnPc2	Vánoce
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
jím	jíst	k5eAaImIp1nS	jíst
Vánoce	Vánoce	k1gFnPc1	Vánoce
vrcholí	vrcholí	k1gNnSc2	vrcholí
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc1	svátek
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
upomíná	upomínat	k5eAaImIp3nS	upomínat
na	na	k7c4	na
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Kristus	Kristus	k1gMnSc1	Kristus
zjevuje	zjevovat	k5eAaImIp3nS	zjevovat
nejen	nejen	k6eAd1	nejen
izraelskému	izraelský	k2eAgInSc3d1	izraelský
národu	národ	k1gInSc3	národ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všem	všecek	k3xTgMnPc3	všecek
<g/>
;	;	kIx,	;
dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
jako	jako	k9	jako
král	král	k1gMnSc1	král
všech	všecek	k3xTgInPc6	všecek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
mudrcové	mudrc	k1gMnPc1	mudrc
z	z	k7c2	z
daleka	daleko	k1gNnSc2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evangeliu	evangelium	k1gNnSc6	evangelium
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
král	král	k1gMnSc1	král
Herodes	Herodes	k1gMnSc1	Herodes
vyptával	vyptávat	k5eAaImAgMnS	vyptávat
na	na	k7c4	na
hvězdu	hvězda	k1gFnSc4	hvězda
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
Betléma	Betlém	k1gInSc2	Betlém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gNnSc3	on
pak	pak	k6eAd1	pak
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dítě	dítě	k1gNnSc1	dítě
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
jim	on	k3xPp3gMnPc3	on
pak	pak	k6eAd1	pak
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
cestu	cesta	k1gFnSc4	cesta
až	až	k9	až
k	k	k7c3	k
domu	dům	k1gInSc3	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
poklonili	poklonit	k5eAaPmAgMnP	poklonit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
předali	předat	k5eAaPmAgMnP	předat
dary	dar	k1gInPc4	dar
<g/>
.	.	kIx.	.
</s>
<s>
Mudrci	mudrc	k1gMnPc1	mudrc
pak	pak	k6eAd1	pak
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
odcestovali	odcestovat	k5eAaPmAgMnP	odcestovat
a	a	k8xC	a
místo	místo	k6eAd1	místo
Herodovi	Herodův	k2eAgMnPc1d1	Herodův
neoznámili	oznámit	k5eNaPmAgMnP	oznámit
<g/>
.	.	kIx.	.
</s>
<s>
Herodes	Herodes	k1gMnSc1	Herodes
se	se	k3xPyFc4	se
rozlítil	rozlítit	k5eAaPmAgMnS	rozlítit
a	a	k8xC	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
svého	svůj	k3xOyFgNnSc2	svůj
postavení	postavení	k1gNnSc1	postavení
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
Mesiáše-krále	Mesiášerála	k1gFnSc3	Mesiáše-krála
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgInS	nechat
usmrtit	usmrtit	k5eAaPmF	usmrtit
všechny	všechen	k3xTgMnPc4	všechen
chlapce	chlapec	k1gMnPc4	chlapec
do	do	k7c2	do
věku	věk	k1gInSc2	věk
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
však	však	k9	však
předtím	předtím	k6eAd1	předtím
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
ve	v	k7c6	v
snu	sen	k1gInSc6	sen
vzal	vzít	k5eAaPmAgMnS	vzít
Ježíše	Ježíš	k1gMnPc4	Ježíš
a	a	k8xC	a
Marii	Maria	k1gFnSc4	Maria
a	a	k8xC	a
utekli	utéct	k5eAaPmAgMnP	utéct
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
byli	být	k5eAaImAgMnP	být
tři	tři	k4xCgMnPc1	tři
mudrci	mudrc	k1gMnPc1	mudrc
či	či	k8xC	či
mágové	mág	k1gMnPc1	mág
z	z	k7c2	z
bible	bible	k1gFnSc2	bible
pohané	pohan	k1gMnPc1	pohan
<g/>
,	,	kIx,	,
hvězdopravci	hvězdopravec	k1gMnPc1	hvězdopravec
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
<g/>
,	,	kIx,	,
babylonští	babylonský	k2eAgMnPc1d1	babylonský
kněží	kněz	k1gMnPc1	kněz
kultu	kult	k1gInSc2	kult
Zarathustry	Zarathustra	k1gFnSc2	Zarathustra
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
znalost	znalost	k1gFnSc1	znalost
pohybů	pohyb	k1gInPc2	pohyb
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
vůči	vůči	k7c3	vůči
nim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
proroctvím	proroctví	k1gNnSc7	proroctví
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Numeri	Numer	k1gFnSc2	Numer
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Bileám	Bilea	k1gFnPc3	Bilea
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Jákobova	Jákobův	k2eAgFnSc1d1	Jákobova
vyjde	vyjít	k5eAaPmIp3nS	vyjít
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
nový	nový	k2eAgMnSc1d1	nový
král	král	k1gMnSc1	král
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evangelním	evangelní	k2eAgInSc6d1	evangelní
příběhu	příběh	k1gInSc6	příběh
samotném	samotný	k2eAgInSc6d1	samotný
není	být	k5eNaImIp3nS	být
řeč	řeč	k1gFnSc4	řeč
ani	ani	k8xC	ani
o	o	k7c6	o
králích	král	k1gMnPc6	král
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
počtu	počet	k1gInSc6	počet
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
o	o	k7c6	o
třech	tři	k4xCgMnPc6	tři
mudrcích	mudrc	k1gMnPc6	mudrc
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
církevní	církevní	k2eAgMnSc1d1	církevní
spisovatel	spisovatel	k1gMnSc1	spisovatel
Órigenés	Órigenésa	k1gFnPc2	Órigenésa
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
počet	počet	k1gInSc1	počet
tří	tři	k4xCgFnPc2	tři
zřejmě	zřejmě	k6eAd1	zřejmě
vyvozuje	vyvozovat	k5eAaImIp3nS	vyvozovat
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
darů	dar	k1gInPc2	dar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Ježíši	Ježíš	k1gMnPc7	Ježíš
přinášejí	přinášet	k5eAaImIp3nP	přinášet
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
králové	králová	k1gFnSc2	králová
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevuje	objevovat	k5eAaImIp3nS	objevovat
až	až	k9	až
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
u	u	k7c2	u
sv.	sv.	kA	sv.
Caesaria	Caesarium	k1gNnPc4	Caesarium
z	z	k7c2	z
Arles	Arlesa	k1gFnPc2	Arlesa
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podle	podle	k7c2	podle
zmínky	zmínka	k1gFnSc2	zmínka
v	v	k7c6	v
72	[number]	k4	72
<g/>
.	.	kIx.	.
žalmu	žalm	k1gInSc2	žalm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
mudrců	mudrc	k1gMnPc2	mudrc
však	však	k9	však
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
výkladech	výklad	k1gInPc6	výklad
kolísal	kolísat	k5eAaImAgInS	kolísat
od	od	k7c2	od
počtu	počet	k1gInSc2	počet
dvou	dva	k4xCgNnPc2	dva
až	až	k8xS	až
čtyř	čtyři	k4xCgNnPc2	čtyři
v	v	k7c6	v
západní	západní	k2eAgFnSc3d1	západní
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
po	po	k7c4	po
dvanáct	dvanáct	k4xCc4	dvanáct
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
<g/>
.	.	kIx.	.
</s>
<s>
Jmen	jméno	k1gNnPc2	jméno
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Melichar	Melichar	k1gMnSc1	Melichar
a	a	k8xC	a
Baltazar	Baltazar	k1gMnSc1	Baltazar
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
dostává	dostávat	k5eAaImIp3nS	dostávat
zřejmě	zřejmě	k6eAd1	zřejmě
až	až	k9	až
v	v	k7c6	v
lidových	lidový	k2eAgNnPc6d1	lidové
vyprávěních	vyprávění	k1gNnPc6	vyprávění
v	v	k7c4	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
napsána	napsán	k2eAgNnPc1d1	napsáno
také	také	k6eAd1	také
na	na	k7c6	na
fresce	freska	k1gFnSc6	freska
v	v	k7c6	v
Raveně	Ravena	k1gFnSc6	Ravena
datované	datovaný	k2eAgFnSc6d1	datovaná
po	po	k7c6	po
roce	rok	k1gInSc6	rok
560	[number]	k4	560
po	po	k7c4	po
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
u	u	k7c2	u
koho	kdo	k3yRnSc2	kdo
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
jména	jméno	k1gNnPc1	jméno
spolehlivě	spolehlivě	k6eAd1	spolehlivě
objevují	objevovat	k5eAaImIp3nP	objevovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kancléř	kancléř	k1gMnSc1	kancléř
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Comestor	Comestor	k1gMnSc1	Comestor
<g/>
.	.	kIx.	.
</s>
<s>
Středověk	středověk	k1gInSc1	středověk
je	být	k5eAaImIp3nS	být
také	také	k9	také
obdobím	období	k1gNnSc7	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
těmto	tento	k3xDgNnPc3	tento
jménům	jméno	k1gNnPc3	jméno
přiřazeny	přiřazen	k2eAgInPc4d1	přiřazen
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
dary	dar	k1gInPc4	dar
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
dary	dar	k1gInPc1	dar
měly	mít	k5eAaImAgInP	mít
také	také	k9	také
představovat	představovat	k5eAaImF	představovat
veškerou	veškerý	k3xTgFnSc4	veškerý
hmotu	hmota	k1gFnSc4	hmota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
veškerá	veškerý	k3xTgFnSc1	veškerý
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
známá	známá	k1gFnSc1	známá
skupenství	skupenství	k1gNnSc4	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
králové	král	k1gMnPc1	král
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
výkladů	výklad	k1gInPc2	výklad
představují	představovat	k5eAaImIp3nP	představovat
všechna	všechen	k3xTgNnPc1	všechen
období	období	k1gNnPc1	období
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
na	na	k7c6	na
scénách	scéna	k1gFnPc6	scéna
klanění	klanění	k1gNnSc2	klanění
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k9	jako
mladík	mladík	k1gMnSc1	mladík
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
jako	jako	k8xC	jako
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
jako	jako	k8xC	jako
stařec	stařec	k1gMnSc1	stařec
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
nebyli	být	k5eNaImAgMnP	být
nikdy	nikdy	k6eAd1	nikdy
oficiálně	oficiálně	k6eAd1	oficiálně
svatořečeni	svatořečit	k5eAaBmNgMnP	svatořečit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jsou	být	k5eAaImIp3nP	být
uctíváni	uctívat	k5eAaImNgMnP	uctívat
jako	jako	k9	jako
patroni	patron	k1gMnPc1	patron
poutníků	poutník	k1gMnPc2	poutník
a	a	k8xC	a
hříšníků	hříšník	k1gMnPc2	hříšník
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
svatých	svatá	k1gFnPc2	svatá
Tří	tři	k4xCgInPc2	tři
králů	král	k1gMnPc2	král
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
nalezla	nalézt	k5eAaBmAgFnS	nalézt
svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gFnPc4	on
přivezla	přivézt	k5eAaPmAgFnS	přivézt
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
jejím	její	k3xOp3gMnSc7	její
synem	syn	k1gMnSc7	syn
Konstantinem	Konstantin	k1gMnSc7	Konstantin
darovány	darovat	k5eAaPmNgFnP	darovat
milánskému	milánský	k2eAgMnSc3d1	milánský
biskupovi	biskup	k1gMnSc3	biskup
Eustorgiu	Eustorgius	k1gMnSc3	Eustorgius
<g/>
,	,	kIx,	,
rodáku	rodák	k1gMnSc3	rodák
z	z	k7c2	z
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Milána	Milán	k1gInSc2	Milán
roku	rok	k1gInSc2	rok
1158	[number]	k4	1158
císařem	císař	k1gMnSc7	císař
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Barbarossou	Barbarossa	k1gMnSc7	Barbarossa
kolínský	kolínský	k2eAgMnSc1d1	kolínský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Rainald	Rainald	k1gMnSc1	Rainald
z	z	k7c2	z
Dasselu	Dassel	k1gInSc2	Dassel
převezl	převézt	k5eAaPmAgMnS	převézt
tyto	tento	k3xDgInPc4	tento
ostatky	ostatek	k1gInPc4	ostatek
do	do	k7c2	do
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uložil	uložit	k5eAaPmAgInS	uložit
do	do	k7c2	do
chrámového	chrámový	k2eAgInSc2d1	chrámový
pokladu	poklad	k1gInSc2	poklad
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
místem	místo	k1gNnSc7	místo
úcty	úcta	k1gFnSc2	úcta
mnoha	mnoho	k4c2	mnoho
poutníků	poutník	k1gMnPc2	poutník
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
významných	významný	k2eAgMnPc2d1	významný
zlatotepců	zlatotepec	k1gMnPc2	zlatotepec
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
z	z	k7c2	z
Verdunu	Verdun	k1gInSc2	Verdun
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
relikviář	relikviář	k1gInSc1	relikviář
ze	z	k7c2	z
vzácných	vzácný	k2eAgInPc2d1	vzácný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
relikviář	relikviář	k1gInSc1	relikviář
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
gotické	gotický	k2eAgFnSc6d1	gotická
lodi	loď	k1gFnSc6	loď
před	před	k7c7	před
hlavním	hlavní	k2eAgInSc7d1	hlavní
oltářem	oltář	k1gInSc7	oltář
kolínského	kolínský	k2eAgInSc2d1	kolínský
dómu	dóm	k1gInSc2	dóm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
o	o	k7c6	o
svátku	svátek	k1gInSc6	svátek
Zjevení	zjevení	k1gNnSc1	zjevení
Páně	páně	k2eAgFnSc2d1	páně
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
)	)	kIx)	)
píše	psát	k5eAaImIp3nS	psát
posvěcenou	posvěcený	k2eAgFnSc7d1	posvěcená
křídou	křída	k1gFnSc7	křída
na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
domů	dům	k1gInPc2	dům
a	a	k8xC	a
chlévů	chlév	k1gInPc2	chlév
zkratka	zkratka	k1gFnSc1	zkratka
K	k	k7c3	k
†	†	k?	†
M	M	kA	M
†	†	k?	†
B	B	kA	B
†	†	k?	†
nebo	nebo	k8xC	nebo
latinský	latinský	k2eAgInSc1d1	latinský
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
C	C	kA	C
†	†	k?	†
M	M	kA	M
†	†	k?	†
B	B	kA	B
†	†	k?	†
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
†	†	k?	†
C	C	kA	C
†	†	k?	†
M	M	kA	M
†	†	k?	†
B	B	kA	B
)	)	kIx)	)
jako	jako	k8xC	jako
formule	formule	k1gFnSc1	formule
požehnání	požehnání	k1gNnSc2	požehnání
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
text	text	k1gInSc1	text
zní	znět	k5eAaImIp3nS	znět
Christus	Christus	k1gInSc4	Christus
mansionem	mansion	k1gInSc7	mansion
benedicat	benedicat	k5eAaPmF	benedicat
–	–	k?	–
Kristus	Kristus	k1gMnSc1	Kristus
ať	ať	k8xS	ať
obydlí	obydlí	k1gNnSc1	obydlí
žehná	žehnat	k5eAaImIp3nS	žehnat
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
Kristus	Kristus	k1gMnSc1	Kristus
požehná	požehnat	k5eAaPmIp3nS	požehnat
tomuto	tento	k3xDgInSc3	tento
příbytku	příbytek	k1gInSc3	příbytek
(	(	kIx(	(
<g/>
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
–	–	k?	–
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
za	za	k7c4	za
třetí	třetí	k4xOgInSc4	třetí
křížek	křížek	k1gInSc4	křížek
píše	psát	k5eAaImIp3nS	psát
letopočet	letopočet	k1gInSc1	letopočet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
křížky	křížek	k1gInPc1	křížek
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
Nejsvětější	nejsvětější	k2eAgFnSc4d1	nejsvětější
Trojici	trojice	k1gFnSc4	trojice
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Syna	syn	k1gMnSc2	syn
a	a	k8xC	a
Ducha	duch	k1gMnSc2	duch
Svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
znaménka	znaménko	k1gNnSc2	znaménko
"	"	kIx"	"
<g/>
plus	plus	k1gInSc1	plus
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
iniciálami	iniciála	k1gFnPc7	iniciála
<g/>
.	.	kIx.	.
</s>
