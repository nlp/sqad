<s>
Staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
kočky	kočka	k1gFnSc2	kočka
uctívali	uctívat	k5eAaImAgMnP	uctívat
jako	jako	k8xS	jako
božstvo	božstvo	k1gNnSc4	božstvo
a	a	k8xC	a
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
moc	moc	k6eAd1	moc
chránit	chránit	k5eAaImF	chránit
člověka	člověk	k1gMnSc4	člověk
před	před	k7c7	před
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
