<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
(	(	kIx(	(
<g/>
Felis	Felis	k1gFnSc1	Felis
silvestris	silvestris	k1gFnSc2	silvestris
f.	f.	k?	f.
catus	catus	k1gInSc1	catus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
forma	forma	k1gFnSc1	forma
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
průvodcem	průvodce	k1gMnSc7	průvodce
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
její	její	k3xOp3gFnSc1	její
divoká	divoký	k2eAgFnSc1d1	divoká
příbuzná	příbuzná	k1gFnSc1	příbuzná
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
podčeledi	podčeleď	k1gFnSc2	podčeleď
malé	malý	k2eAgFnSc2d1	malá
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgMnSc7d1	typický
zástupcem	zástupce	k1gMnSc7	zástupce
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pružné	pružný	k2eAgNnSc1d1	pružné
a	a	k8xC	a
svalnaté	svalnatý	k2eAgNnSc1d1	svalnaté
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
přizpůsobené	přizpůsobený	k2eAgInPc1d1	přizpůsobený
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
ostré	ostrý	k2eAgInPc4d1	ostrý
drápy	dráp	k1gInPc4	dráp
a	a	k8xC	a
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
vynikající	vynikající	k2eAgInSc4d1	vynikající
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
sluch	sluch	k1gInSc4	sluch
a	a	k8xC	a
čich	čich	k1gInSc4	čich
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gMnPc4	domácí
vždy	vždy	k6eAd1	vždy
sloužila	sloužit	k5eAaImAgFnS	sloužit
člověku	člověk	k1gMnSc6	člověk
především	především	k9	především
jako	jako	k9	jako
lovec	lovec	k1gMnSc1	lovec
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
také	také	k9	také
jako	jako	k9	jako
společník	společník	k1gMnSc1	společník
a	a	k8xC	a
mazlíček	mazlíček	k1gMnSc1	mazlíček
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Číny	Čína	k1gFnSc2	Čína
je	být	k5eAaImIp3nS	být
konzumováno	konzumován	k2eAgNnSc1d1	konzumováno
<g/>
/	/	kIx~	/
<g/>
konzervováno	konzervován	k2eAgNnSc1d1	konzervováno
kočičí	kočičí	k2eAgNnSc1d1	kočičí
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
rovněž	rovněž	k9	rovněž
kožešinovým	kožešinový	k2eAgNnSc7d1	kožešinové
zvířetem	zvíře	k1gNnSc7	zvíře
a	a	k8xC	a
výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
kočičí	kočičí	k2eAgFnSc2d1	kočičí
kožešiny	kožešina	k1gFnSc2	kožešina
se	se	k3xPyFc4	se
dostávaly	dostávat	k5eAaImAgInP	dostávat
i	i	k9	i
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
však	však	k9	však
dovoz	dovoz	k1gInSc1	dovoz
kočičí	kočičí	k2eAgFnSc2d1	kočičí
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
kožešiny	kožešina	k1gFnSc2	kožešina
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
kočku	kočka	k1gFnSc4	kočka
domácí	domácí	k2eAgMnSc1d1	domácí
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
zpětně	zpětně	k6eAd1	zpětně
zdivočela	zdivočet	k5eAaPmAgFnS	zdivočet
<g/>
.	.	kIx.	.
</s>
<s>
Divokým	divoký	k2eAgInSc7d1	divoký
předkem	předek	k1gInSc7	předek
domácích	domácí	k2eAgFnPc2d1	domácí
koček	kočka	k1gFnPc2	kočka
byla	být	k5eAaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
její	její	k3xOp3gInSc1	její
africký	africký	k2eAgInSc1d1	africký
poddruh	poddruh	k1gInSc1	poddruh
kočka	kočka	k1gFnSc1	kočka
plavá	plavý	k2eAgFnSc1d1	plavá
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
snad	snad	k9	snad
i	i	k9	i
kočka	kočka	k1gFnSc1	kočka
divoká	divoký	k2eAgFnSc1d1	divoká
evropská	evropský	k2eAgFnSc1d1	Evropská
a	a	k8xC	a
asijská	asijský	k2eAgFnSc1d1	asijská
kočka	kočka	k1gFnSc1	kočka
stepní	stepní	k2eAgFnSc1d1	stepní
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
kočky	kočka	k1gFnPc1	kočka
se	se	k3xPyFc4	se
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
lidí	člověk	k1gMnPc2	člověk
dostaly	dostat	k5eAaPmAgFnP	dostat
před	před	k7c4	před
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgNnPc4	první
trvalá	trvalý	k2eAgNnPc4d1	trvalé
lidská	lidský	k2eAgNnPc4d1	lidské
sídla	sídlo	k1gNnPc4	sídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
8	[number]	k4	8
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
pocházejí	pocházet	k5eAaImIp3nP	pocházet
první	první	k4xOgInPc4	první
důkazy	důkaz	k1gInPc4	důkaz
existence	existence	k1gFnSc2	existence
ochočených	ochočený	k2eAgFnPc2d1	ochočená
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
Anatolie	Anatolie	k1gFnSc2	Anatolie
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Kypru	Kypr	k1gInSc2	Kypr
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
kočka	kočka	k1gFnSc1	kočka
stala	stát	k5eAaPmAgFnS	stát
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
života	život	k1gInSc2	život
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
dokladem	doklad	k1gInSc7	doklad
jsou	být	k5eAaImIp3nP	být
obrazy	obraz	k1gInPc4	obraz
koček	kočka	k1gFnPc2	kočka
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
,	,	kIx,	,
sošky	soška	k1gFnSc2	soška
koček	kočka	k1gFnPc2	kočka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
mumie	mumie	k1gFnSc2	mumie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zemědělce	zemědělec	k1gMnSc4	zemědělec
měla	mít	k5eAaImAgFnS	mít
kočka	kočka	k1gFnSc1	kočka
obrovský	obrovský	k2eAgInSc4d1	obrovský
význam	význam	k1gInSc4	význam
jako	jako	k8xS	jako
lovec	lovec	k1gMnSc1	lovec
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ohrožovali	ohrožovat	k5eAaImAgMnP	ohrožovat
skladiště	skladiště	k1gNnSc4	skladiště
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
kočky	kočka	k1gFnSc2	kočka
uctívali	uctívat	k5eAaImAgMnP	uctívat
jako	jako	k8xC	jako
božstvo	božstvo	k1gNnSc4	božstvo
a	a	k8xC	a
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
moc	moc	k6eAd1	moc
chránit	chránit	k5eAaImF	chránit
člověka	člověk	k1gMnSc4	člověk
před	před	k7c7	před
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
kočky	kočka	k1gFnSc2	kočka
v	v	k7c6	v
domě	dům	k1gInSc6	dům
znamenala	znamenat	k5eAaImAgFnS	znamenat
požehnání	požehnání	k1gNnSc4	požehnání
<g/>
.	.	kIx.	.
</s>
<s>
Nejposvátnější	posvátný	k2eAgFnPc1d3	nejposvátnější
byly	být	k5eAaImAgFnP	být
černé	černý	k2eAgFnPc1d1	černá
kočky	kočka	k1gFnPc1	kočka
chránící	chránící	k2eAgInPc4d1	chránící
egyptské	egyptský	k2eAgInPc4d1	egyptský
chrámy	chrám	k1gInPc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
mumifikovaných	mumifikovaný	k2eAgNnPc2d1	mumifikované
kočičích	kočičí	k2eAgNnPc2d1	kočičí
těl	tělo	k1gNnPc2	tělo
a	a	k8xC	a
kočičích	kočičí	k2eAgInPc2d1	kočičí
amuletů	amulet	k1gInPc2	amulet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
i	i	k9	i
v	v	k7c6	v
hrobech	hrob	k1gInPc6	hrob
faraónů	faraón	k1gMnPc2	faraón
dokládají	dokládat	k5eAaImIp3nP	dokládat
kultovní	kultovní	k2eAgNnSc4d1	kultovní
uctívání	uctívání	k1gNnSc4	uctívání
koček	kočka	k1gFnPc2	kočka
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
bohyni	bohyně	k1gFnSc4	bohyně
-	-	kIx~	-
Bastet	Bastet	k1gInSc1	Bastet
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
jako	jako	k8xS	jako
malá	malý	k2eAgFnSc1d1	malá
kočka	kočka	k1gFnSc1	kočka
se	s	k7c7	s
lví	lví	k2eAgFnSc7d1	lví
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
žena	žena	k1gFnSc1	žena
s	s	k7c7	s
kočičí	kočičí	k2eAgFnSc7d1	kočičí
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
jejího	její	k3xOp3gNnSc2	její
uctívání	uctívání	k1gNnSc2	uctívání
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
500	[number]	k4	500
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Bastet	Bastet	k1gInSc4	Bastet
byla	být	k5eAaImAgFnS	být
kromě	kromě	k7c2	kromě
jiného	jiný	k2eAgNnSc2d1	jiné
i	i	k8xC	i
bohyní	bohyně	k1gFnPc2	bohyně
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
štědrosti	štědrost	k1gFnSc2	štědrost
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k6eAd1	poblíž
chrámu	chrámat	k5eAaImIp1nS	chrámat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bubastis	Bubastis	k1gFnSc2	Bubastis
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
byla	být	k5eAaImAgFnS	být
Bastet	Bastet	k1gInSc4	Bastet
ochránkyní	ochránkyně	k1gFnPc2	ochránkyně
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
kočičí	kočičí	k2eAgNnSc1d1	kočičí
pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
<g/>
.	.	kIx.	.
</s>
<s>
Slunečního	sluneční	k2eAgMnSc4d1	sluneční
boha	bůh	k1gMnSc4	bůh
Ra	ra	k0	ra
vzývali	vzývat	k5eAaImAgMnP	vzývat
Egypťané	Egypťan	k1gMnPc1	Egypťan
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
kočičí	kočičí	k2eAgFnSc6d1	kočičí
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Zabití	zabitý	k1gMnPc1	zabitý
kočky	kočka	k1gFnSc2	kočka
se	se	k3xPyFc4	se
trestalo	trestat	k5eAaImAgNnS	trestat
smrtí	smrt	k1gFnPc2	smrt
a	a	k8xC	a
uhynulé	uhynulý	k2eAgFnPc1d1	uhynulá
kočky	kočka	k1gFnPc1	kočka
byly	být	k5eAaImAgFnP	být
balzamovány	balzamovat	k5eAaImNgFnP	balzamovat
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
nepovolovali	povolovat	k5eNaImAgMnP	povolovat
vývoz	vývoz	k1gInSc4	vývoz
koček	kočka	k1gFnPc2	kočka
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
fénickým	fénický	k2eAgMnPc3d1	fénický
obchodníkům	obchodník	k1gMnPc3	obchodník
se	se	k3xPyFc4	se
kočky	kočka	k1gFnPc1	kočka
přesto	přesto	k8xC	přesto
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
doklady	doklad	k1gInPc4	doklad
o	o	k7c6	o
kočkách	kočka	k1gFnPc6	kočka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nacházíme	nacházet	k5eAaImIp1nP	nacházet
na	na	k7c6	na
řeckých	řecký	k2eAgFnPc6d1	řecká
vázách	váza	k1gFnPc6	váza
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
byla	být	k5eAaImAgFnS	být
kočka	kočka	k1gFnSc1	kočka
dávána	dávat	k5eAaImNgFnS	dávat
do	do	k7c2	do
spojitosti	spojitost	k1gFnSc2	spojitost
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
Afroditou	Afrodita	k1gFnSc7	Afrodita
a	a	k8xC	a
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
kult	kult	k1gInSc1	kult
částečně	částečně	k6eAd1	částečně
splynul	splynout	k5eAaPmAgInS	splynout
s	s	k7c7	s
kultem	kult	k1gInSc7	kult
Bastet	Basteta	k1gFnPc2	Basteta
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
byla	být	k5eAaImAgFnS	být
domestikována	domestikovat	k5eAaBmNgFnS	domestikovat
také	také	k9	také
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začal	začít	k5eAaPmAgInS	začít
jejich	jejich	k3xOp3gInSc1	jejich
chov	chov	k1gInSc1	chov
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
letopočtu	letopočet	k1gInSc2	letopočet
také	také	k9	také
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
byla	být	k5eAaImAgFnS	být
uctívána	uctívat	k5eAaImNgFnS	uctívat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
bohyně	bohyně	k1gFnSc2	bohyně
Šasthi	Šasth	k1gFnSc2	Šasth
<g/>
,	,	kIx,	,
ochránkyně	ochránkyně	k1gFnSc2	ochránkyně
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
šestinedělek	šestinedělka	k1gFnPc2	šestinedělka
<g/>
.	.	kIx.	.
</s>
<s>
Indové	Ind	k1gMnPc1	Ind
však	však	k9	však
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chlupy	chlup	k1gInPc1	chlup
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
kočce	kočka	k1gFnSc3	kočka
línají	línat	k5eAaImIp3nP	línat
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
rituálně	rituálně	k6eAd1	rituálně
znečistit	znečistit	k5eAaPmF	znečistit
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
s	s	k7c7	s
kočkou	kočka	k1gFnSc7	kočka
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byly	být	k5eAaImAgFnP	být
kočky	kočka	k1gFnPc1	kočka
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
spojovány	spojovat	k5eAaImNgFnP	spojovat
s	s	k7c7	s
chudobou	chudoba	k1gFnSc7	chudoba
a	a	k8xC	a
mnišstvím	mnišství	k1gNnSc7	mnišství
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
moc	moc	k6eAd1	moc
před	před	k7c7	před
chudobou	chudoba	k1gFnSc7	chudoba
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
Číňané	Číňan	k1gMnPc1	Číňan
dodnes	dodnes	k6eAd1	dodnes
příležiostně	příležiostně	k6eAd1	příležiostně
kočky	kočka	k1gFnPc1	kočka
chovají	chovat	k5eAaImIp3nP	chovat
i	i	k9	i
pro	pro	k7c4	pro
kožešinu	kožešina	k1gFnSc4	kožešina
a	a	k8xC	a
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kantonu	Kanton	k1gInSc2	Kanton
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
zvíře	zvíře	k1gNnSc1	zvíře
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
zvěrokruhu	zvěrokruh	k1gInSc6	zvěrokruh
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
zajíc	zajíc	k1gMnSc1	zajíc
či	či	k8xC	či
králík	králík	k1gMnSc1	králík
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jím	jíst	k5eAaImIp1nS	jíst
však	však	k9	však
být	být	k5eAaImF	být
i	i	k9	i
kočka	kočka	k1gFnSc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozpor	rozpor	k1gInSc1	rozpor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
kočka	kočka	k1gFnSc1	kočka
i	i	k8xC	i
králík	králík	k1gMnSc1	králík
se	se	k3xPyFc4	se
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
označují	označovat	k5eAaImIp3nP	označovat
shodným	shodný	k2eAgInSc7d1	shodný
názvem	název	k1gInSc7	název
mao	mao	k?	mao
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
známa	známo	k1gNnSc2	známo
také	také	k9	také
pověra	pověra	k1gFnSc1	pověra
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
kočky	kočka	k1gFnPc1	kočka
přinášejí	přinášet	k5eAaImIp3nP	přinášet
smůlu	smůla	k1gFnSc4	smůla
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
Octaviana	Octavian	k1gMnSc2	Octavian
Augusta	August	k1gMnSc2	August
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
kočky	kočka	k1gFnPc1	kočka
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
ozdobou	ozdoba	k1gFnSc7	ozdoba
salonů	salon	k1gInPc2	salon
bohatých	bohatý	k2eAgMnPc2d1	bohatý
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
i	i	k9	i
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
vrstev	vrstva	k1gFnPc2	vrstva
a	a	k8xC	a
římští	římský	k2eAgMnPc1d1	římský
legionáři	legionář	k1gMnPc1	legionář
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
brali	brát	k5eAaImAgMnP	brát
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
vojenská	vojenský	k2eAgNnPc4d1	vojenské
tažení	tažení	k1gNnPc4	tažení
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
kočky	kočka	k1gFnPc1	kočka
dostaly	dostat	k5eAaPmAgFnP	dostat
i	i	k9	i
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
byly	být	k5eAaImAgFnP	být
kočky	kočka	k1gFnPc1	kočka
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c2	za
hodné	hodný	k2eAgFnSc2d1	hodná
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
za	za	k7c4	za
přinášející	přinášející	k2eAgNnSc4d1	přinášející
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
v	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
krajinách	krajina	k1gFnPc6	krajina
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
zákaz	zákaz	k1gInSc1	zákaz
jejich	jejich	k3xOp3gNnSc2	jejich
zabíjení	zabíjení	k1gNnSc2	zabíjení
včetně	včetně	k7c2	včetně
stanovení	stanovení	k1gNnSc2	stanovení
pokuty	pokuta	k1gFnSc2	pokuta
za	za	k7c4	za
zabití	zabití	k1gNnSc4	zabití
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
převzali	převzít	k5eAaPmAgMnP	převzít
kult	kult	k1gInSc4	kult
bohyně	bohyně	k1gFnSc2	bohyně
Artemis	Artemis	k1gFnSc1	Artemis
<g/>
;	;	kIx,	;
kterou	který	k3yQgFnSc4	který
nazývali	nazývat	k5eAaImAgMnP	nazývat
Diana	Diana	k1gFnSc1	Diana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
kočkám	kočka	k1gFnPc3	kočka
zastávali	zastávat	k5eAaImAgMnP	zastávat
spíše	spíše	k9	spíše
praktický	praktický	k2eAgInSc4d1	praktický
přístup	přístup	k1gInSc4	přístup
jako	jako	k8xC	jako
k	k	k7c3	k
lovcům	lovec	k1gMnPc3	lovec
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
"	"	kIx"	"
<g/>
kočičí	kočičí	k2eAgFnSc1d1	kočičí
<g/>
"	"	kIx"	"
bohyně	bohyně	k1gFnSc1	bohyně
je	být	k5eAaImIp3nS	být
skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
bohyně	bohyně	k1gFnSc1	bohyně
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
Freya	Frey	k1gInSc2	Frey
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
kočár	kočár	k1gInSc4	kočár
tažený	tažený	k2eAgInSc4d1	tažený
kočkami	kočka	k1gFnPc7	kočka
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
divokými	divoký	k2eAgInPc7d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
místy	místy	k6eAd1	místy
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
či	či	k8xC	či
Dánsku	Dánsko	k1gNnSc6	Dánsko
doporučovalo	doporučovat	k5eAaImAgNnS	doporučovat
dívkám	dívka	k1gFnPc3	dívka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
chtěly	chtít	k5eAaImAgFnP	chtít
šťastně	šťastně	k6eAd1	šťastně
vdát	vdát	k5eAaPmF	vdát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dobře	dobře	k6eAd1	dobře
pečovaly	pečovat	k5eAaImAgInP	pečovat
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
kočku	kočka	k1gFnSc4	kočka
-	-	kIx~	-
Freyno	Freyno	k6eAd1	Freyno
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Kladný	kladný	k2eAgInSc1d1	kladný
vztah	vztah	k1gInSc1	vztah
ke	k	k7c3	k
kočkám	kočka	k1gFnPc3	kočka
má	mít	k5eAaImIp3nS	mít
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
prorok	prorok	k1gMnSc1	prorok
Mohamed	Mohamed	k1gMnSc1	Mohamed
měl	mít	k5eAaImAgMnS	mít
totiž	totiž	k9	totiž
kočku	kočka	k1gFnSc4	kočka
jménem	jméno	k1gNnSc7	jméno
Muezza	Muezz	k1gMnSc2	Muezz
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
velmi	velmi	k6eAd1	velmi
miloval	milovat	k5eAaImAgInS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
kočky	kočka	k1gFnPc1	kočka
dostaly	dostat	k5eAaPmAgFnP	dostat
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
ochraňovat	ochraňovat	k5eAaImF	ochraňovat
zámotky	zámotek	k1gInPc4	zámotek
bource	bourec	k1gMnSc2	bourec
morušového	morušový	k2eAgMnSc2d1	morušový
nebo	nebo	k8xC	nebo
chránit	chránit	k5eAaImF	chránit
staré	starý	k2eAgInPc4d1	starý
rukopisy	rukopis	k1gInPc4	rukopis
v	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
před	před	k7c7	před
potkany	potkan	k1gMnPc7	potkan
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
však	však	k9	však
také	také	k9	také
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dožije	dožít	k5eAaPmIp3nS	dožít
sta	sto	k4xCgNnPc4	sto
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c6	v
čarodějnici	čarodějnice	k1gFnSc6	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
v	v	k7c6	v
japonských	japonský	k2eAgFnPc6d1	japonská
pohádkách	pohádka	k1gFnPc6	pohádka
často	často	k6eAd1	často
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
teriantropicky	teriantropicky	k6eAd1	teriantropicky
proměňují	proměňovat	k5eAaImIp3nP	proměňovat
v	v	k7c4	v
kočky	kočka	k1gFnPc4	kočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
kočka	kočka	k1gFnSc1	kočka
ceněným	ceněný	k2eAgNnSc7d1	ceněné
zvířetem	zvíře	k1gNnSc7	zvíře
i	i	k9	i
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Nařízení	nařízení	k1gNnSc1	nařízení
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
uvádí	uvádět	k5eAaImIp3nS	uvádět
kočku	kočka	k1gFnSc4	kočka
mezi	mezi	k7c7	mezi
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
chována	chovat	k5eAaImNgFnS	chovat
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
selském	selské	k1gNnSc6	selské
či	či	k8xC	či
klášterním	klášterní	k2eAgInSc6d1	klášterní
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
byla	být	k5eAaImAgFnS	být
chráněna	chránit	k5eAaImNgFnS	chránit
přísnými	přísný	k2eAgInPc7d1	přísný
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ve	v	k7c4	v
Walesu	Walesa	k1gMnSc4	Walesa
měl	mít	k5eAaImAgMnS	mít
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
zabil	zabít	k5eAaPmAgMnS	zabít
či	či	k8xC	či
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
kočku	kočka	k1gFnSc4	kočka
z	z	k7c2	z
knížecích	knížecí	k2eAgFnPc2d1	knížecí
sýpek	sýpka	k1gFnPc2	sýpka
<g/>
,	,	kIx,	,
zaplatit	zaplatit	k5eAaPmF	zaplatit
tolik	tolik	k4xDc1	tolik
oblií	oblie	k1gFnPc2	oblie
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
k	k	k7c3	k
zasypání	zasypání	k1gNnSc3	zasypání
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
visí	viset	k5eAaImIp3nS	viset
za	za	k7c4	za
ocas	ocas	k1gInSc4	ocas
a	a	k8xC	a
čenichem	čenich	k1gInSc7	čenich
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
na	na	k7c4	na
kočky	kočka	k1gFnPc4	kočka
původně	původně	k6eAd1	původně
nahlíželi	nahlížet	k5eAaImAgMnP	nahlížet
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
legendy	legenda	k1gFnSc2	legenda
kočka	kočka	k1gFnSc1	kočka
zahřívala	zahřívat	k5eAaImAgFnS	zahřívat
a	a	k8xC	a
chránila	chránit	k5eAaImAgFnS	chránit
Ježíška	Ježíšek	k1gMnSc4	Ježíšek
v	v	k7c6	v
jesličkách	jesličky	k1gFnPc6	jesličky
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
ji	on	k3xPp3gFnSc4	on
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
vděčně	vděčně	k6eAd1	vděčně
pohladila	pohladit	k5eAaPmAgFnS	pohladit
po	po	k7c6	po
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
kresba	kresba	k1gFnSc1	kresba
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
mourovatých	mourovatý	k2eAgFnPc2d1	mourovatá
koček	kočka	k1gFnPc2	kočka
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
pokládána	pokládat	k5eAaImNgFnS	pokládat
za	za	k7c4	za
stopu	stopa	k1gFnSc4	stopa
tří	tři	k4xCgInPc2	tři
prstů	prst	k1gInPc2	prst
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
ikonografii	ikonografie	k1gFnSc6	ikonografie
atributem	atribut	k1gInSc7	atribut
svaté	svatá	k1gFnSc2	svatá
Gertrudy	Gertruda	k1gFnSc2	Gertruda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
kočku	kočka	k1gFnSc4	kočka
radikálně	radikálně	k6eAd1	radikálně
změnil	změnit	k5eAaPmAgInS	změnit
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
předtím	předtím	k6eAd1	předtím
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
lidé	člověk	k1gMnPc1	člověk
obdivovali	obdivovat	k5eAaImAgMnP	obdivovat
(	(	kIx(	(
<g/>
pružné	pružný	k2eAgInPc4d1	pružný
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
individualita	individualita	k1gFnSc1	individualita
a	a	k8xC	a
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
podezřelým	podezřelý	k2eAgMnSc7d1	podezřelý
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
kočku	kočka	k1gFnSc4	kočka
za	za	k7c4	za
posedlou	posedlý	k2eAgFnSc4d1	posedlá
zlými	zlý	k2eAgMnPc7d1	zlý
duchy	duch	k1gMnPc7	duch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
pekelný	pekelný	k2eAgInSc4d1	pekelný
oheň	oheň	k1gInSc4	oheň
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
spojence	spojenec	k1gMnPc4	spojenec
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
a	a	k8xC	a
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
kočky	kočka	k1gFnPc1	kočka
upalovány	upalovat	k5eAaImNgFnP	upalovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
čarodějnicemi	čarodějnice	k1gFnPc7	čarodějnice
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
kočka	kočka	k1gFnSc1	kočka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
ďábla	ďábel	k1gMnSc2	ďábel
a	a	k8xC	a
například	například	k6eAd1	například
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
černé	černý	k2eAgFnPc4d1	černá
kočky	kočka	k1gFnPc4	kočka
za	za	k7c4	za
ďábelská	ďábelský	k2eAgNnPc4d1	ďábelské
stvoření	stvoření	k1gNnPc4	stvoření
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
chovají	chovat	k5eAaImIp3nP	chovat
pouze	pouze	k6eAd1	pouze
kacíři	kacíř	k1gMnPc1	kacíř
a	a	k8xC	a
čarodějníci	čarodějník	k1gMnPc1	čarodějník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
další	další	k2eAgMnSc1d1	další
papež	papež	k1gMnSc1	papež
<g/>
,	,	kIx,	,
Inocenc	Inocenc	k1gMnSc1	Inocenc
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přikázal	přikázat	k5eAaPmAgMnS	přikázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
čarodějnicí	čarodějnice	k1gFnSc7	čarodějnice
upálena	upálen	k2eAgFnSc1d1	upálena
i	i	k8xC	i
její	její	k3xOp3gFnSc1	její
kočka	kočka	k1gFnSc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledování	pronásledování	k1gNnSc1	pronásledování
koček	kočka	k1gFnPc2	kočka
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
kočky	kočka	k1gFnPc4	kočka
věšeny	věšen	k2eAgFnPc4d1	věšen
<g/>
,	,	kIx,	,
přibíjeny	přibíjen	k2eAgFnPc4d1	přibíjena
na	na	k7c4	na
vrata	vrata	k1gNnPc4	vrata
nebo	nebo	k8xC	nebo
upalovány	upalován	k2eAgFnPc4d1	upalován
v	v	k7c6	v
koších	koš	k1gInPc6	koš
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
dnešní	dnešní	k2eAgFnSc3d1	dnešní
Belgii	Belgie	k1gFnSc3	Belgie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
belgickém	belgický	k2eAgNnSc6d1	Belgické
městě	město	k1gNnSc6	město
Ypres	Ypresa	k1gFnPc2	Ypresa
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
druhou	druhý	k4xOgFnSc4	druhý
středu	středa	k1gFnSc4	středa
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
koná	konat	k5eAaImIp3nS	konat
slavnost	slavnost	k1gFnSc4	slavnost
Kattenstoet	Kattenstoeta	k1gFnPc2	Kattenstoeta
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
byly	být	k5eAaImAgFnP	být
kočky	kočka	k1gFnPc1	kočka
upalovány	upalovat	k5eAaImNgFnP	upalovat
a	a	k8xC	a
shazovány	shazovat	k5eAaImNgFnP	shazovat
ze	z	k7c2	z
zvonice	zvonice	k1gFnSc2	zvonice
<g/>
,	,	kIx,	,
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tyto	tento	k3xDgFnPc1	tento
kruté	krutý	k2eAgFnPc1d1	krutá
praktiky	praktika	k1gFnPc1	praktika
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
naopak	naopak	k6eAd1	naopak
o	o	k7c4	o
oslavu	oslava	k1gFnSc4	oslava
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
schopnost	schopnost	k1gFnSc4	schopnost
ovládnout	ovládnout	k5eAaPmF	ovládnout
mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
proměnit	proměnit	k5eAaPmF	proměnit
je	on	k3xPp3gMnPc4	on
v	v	k7c4	v
upíra	upír	k1gMnSc4	upír
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
pronásledování	pronásledování	k1gNnSc4	pronásledování
koček	kočka	k1gFnPc2	kočka
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
novověku	novověk	k1gInSc6	novověk
používali	používat	k5eAaImAgMnP	používat
námořníci	námořník	k1gMnPc1	námořník
kočky	kočka	k1gFnSc2	kočka
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
lodích	loď	k1gFnPc6	loď
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
myší	myš	k1gFnPc2	myš
a	a	k8xC	a
krys	krysa	k1gFnPc2	krysa
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
"	"	kIx"	"
<g/>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
funkce	funkce	k1gFnSc1	funkce
<g/>
"	"	kIx"	"
lodních	lodní	k2eAgFnPc2d1	lodní
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
majících	mající	k2eAgMnPc2d1	mající
statut	statut	k1gInSc4	statut
člena	člen	k1gMnSc4	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Lodní	lodní	k2eAgFnPc1d1	lodní
kočky	kočka	k1gFnPc1	kočka
existovaly	existovat	k5eAaImAgFnP	existovat
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jménem	jméno	k1gNnSc7	jméno
Jenny	Jenna	k1gMnSc2	Jenna
se	se	k3xPyFc4	se
plavila	plavit	k5eAaImAgNnP	plavit
i	i	k9	i
na	na	k7c4	na
Titanicu	Titanica	k1gFnSc4	Titanica
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
kočky	kočka	k1gFnPc1	kočka
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
osvícenství	osvícenství	k1gNnSc2	osvícenství
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
koček	kočka	k1gFnPc2	kočka
znovu	znovu	k6eAd1	znovu
obrátila	obrátit	k5eAaPmAgFnS	obrátit
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
baroka	baroko	k1gNnSc2	baroko
a	a	k8xC	a
především	především	k9	především
rokoka	rokoko	k1gNnSc2	rokoko
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
ozdobou	ozdoba	k1gFnSc7	ozdoba
salonů	salon	k1gInPc2	salon
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
však	však	k9	však
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
kočka	kočka	k1gFnSc1	kočka
jevila	jevit	k5eAaImAgFnS	jevit
příliš	příliš	k6eAd1	příliš
obyčejnou	obyčejný	k2eAgFnSc4d1	obyčejná
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
její	její	k3xOp3gFnSc4	její
šlechtění	šlechtění	k1gNnSc1	šlechtění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přivezl	přivézt	k5eAaPmAgMnS	přivézt
cestovatel	cestovatel	k1gMnSc1	cestovatel
Pietro	Pietro	k1gNnSc4	Pietro
della	dell	k1gMnSc2	dell
Valle	Vall	k1gMnSc2	Vall
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
první	první	k4xOgFnSc2	první
dlouhosrsté	dlouhosrstý	k2eAgFnSc2d1	dlouhosrstá
kočky	kočka	k1gFnSc2	kočka
z	z	k7c2	z
perské	perský	k2eAgFnSc2d1	perská
provincie	provincie	k1gFnSc2	provincie
Chorasán	Chorasán	k2eAgInSc1d1	Chorasán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgFnPc4	všechen
dlouhosrsté	dlouhosrstý	k2eAgFnPc4d1	dlouhosrstá
kočky	kočka	k1gFnPc4	kočka
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
angorské	angorský	k2eAgFnSc2d1	angorská
podle	podle	k7c2	podle
Ankary	Ankara	k1gFnSc2	Ankara
<g/>
,	,	kIx,	,
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
však	však	k9	však
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
kočky	kočka	k1gFnPc4	kočka
angorské	angorský	k2eAgFnPc4d1	angorská
<g/>
,	,	kIx,	,
perské	perský	k2eAgFnPc4d1	perská
a	a	k8xC	a
vanské	vanská	k1gFnPc4	vanská
<g/>
.	.	kIx.	.
</s>
<s>
Systematický	systematický	k2eAgInSc1d1	systematický
chov	chov	k1gInSc1	chov
koček	kočka	k1gFnPc2	kočka
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
asi	asi	k9	asi
150	[number]	k4	150
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
výstava	výstava	k1gFnSc1	výstava
koček	kočka	k1gFnPc2	kočka
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Crystal	Crystal	k1gFnSc6	Crystal
Palace	Palace	k1gFnSc2	Palace
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
i	i	k9	i
několik	několik	k4yIc4	několik
exemplářů	exemplář	k1gInPc2	exemplář
siamské	siamský	k2eAgFnSc2d1	siamská
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
poprvé	poprvé	k6eAd1	poprvé
dovezené	dovezený	k2eAgNnSc1d1	dovezené
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Výstavu	výstava	k1gFnSc4	výstava
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
Harrison	Harrison	k1gMnSc1	Harrison
Weir	Weir	k1gMnSc1	Weir
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
také	také	k9	také
sepsal	sepsat	k5eAaPmAgMnS	sepsat
standardy	standard	k1gInPc4	standard
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
posuzovatelů	posuzovatel	k1gMnPc2	posuzovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
založen	založit	k5eAaPmNgInS	založit
Národní	národní	k2eAgInSc1d1	národní
klub	klub	k1gInSc1	klub
chovatelů	chovatel	k1gMnPc2	chovatel
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
pořádat	pořádat	k5eAaImF	pořádat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
první	první	k4xOgFnSc1	první
výstava	výstava	k1gFnSc1	výstava
koček	kočka	k1gFnPc2	kočka
také	také	k9	také
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
organizace	organizace	k1gFnPc1	organizace
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechaly	nechat	k5eNaPmAgFnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
:	:	kIx,	:
roku	rok	k1gInSc3	rok
1898	[number]	k4	1898
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
založen	založit	k5eAaPmNgInS	založit
Klub	klub	k1gInSc1	klub
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Asociace	asociace	k1gFnSc1	asociace
chovatelů	chovatel	k1gMnPc2	chovatel
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
CFA	CFA	kA	CFA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
přibyl	přibýt	k5eAaPmAgInS	přibýt
Řídící	řídící	k2eAgInSc1d1	řídící
výbor	výbor	k1gInSc1	výbor
chovatelů	chovatel	k1gMnPc2	chovatel
koček	kočka	k1gFnPc2	kočka
(	(	kIx(	(
<g/>
GCCF	GCCF	kA	GCCF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
felinologická	felinologický	k2eAgFnSc1d1	felinologická
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
FIFe	FIFe	k1gFnSc1	FIFe
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hlavní	hlavní	k2eAgFnSc7d1	hlavní
organizací	organizace	k1gFnSc7	organizace
chovatelů	chovatel	k1gMnPc2	chovatel
koček	kočka	k1gFnPc2	kočka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
včetně	včetně	k7c2	včetně
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
chovatelských	chovatelský	k2eAgInPc2d1	chovatelský
spolků	spolek	k1gInPc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
domácí	domácí	k2eAgFnPc1d1	domácí
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
dlouhému	dlouhý	k2eAgNnSc3d1	dlouhé
období	období	k1gNnSc3	období
domestikace	domestikace	k1gFnSc2	domestikace
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
.	.	kIx.	.
</s>
<s>
Obecné	obecný	k2eAgInPc1d1	obecný
rysy	rys	k1gInPc1	rys
stavby	stavba	k1gFnSc2	stavba
těla	tělo	k1gNnSc2	tělo
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgInPc1d1	stejný
-	-	kIx~	-
kočka	kočka	k1gFnSc1	kočka
má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
krátkými	krátký	k2eAgFnPc7d1	krátká
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc1d1	krátký
krk	krk	k1gInSc1	krk
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
širokou	široký	k2eAgFnSc4d1	široká
a	a	k8xC	a
krátkou	krátký	k2eAgFnSc4d1	krátká
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
středně	středně	k6eAd1	středně
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
vysoká	vysoký	k2eAgFnSc1d1	vysoká
asi	asi	k9	asi
30	[number]	k4	30
cm	cm	kA	cm
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
včetně	včetně	k7c2	včetně
ocasu	ocas	k1gInSc2	ocas
činí	činit	k5eAaImIp3nS	činit
pak	pak	k6eAd1	pak
kolem	kolem	k7c2	kolem
80	[number]	k4	80
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Kocouři	kocour	k1gMnPc1	kocour
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnPc1	samice
a	a	k8xC	a
váží	vážit	k5eAaImIp3nP	vážit
asi	asi	k9	asi
3,5	[number]	k4	3,5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
kg	kg	kA	kg
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
koček	kočka	k1gFnPc2	kočka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
2,5	[number]	k4	2,5
<g/>
-	-	kIx~	-
<g/>
4,5	[number]	k4	4,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
rovná	rovný	k2eAgFnSc1d1	rovná
a	a	k8xC	a
přilehlá	přilehlý	k2eAgFnSc1d1	přilehlá
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
forma	forma	k1gFnSc1	forma
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
teritorium	teritorium	k1gNnSc1	teritorium
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nS	lišit
od	od	k7c2	od
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc4	mládě
vrhá	vrhat	k5eAaImIp3nS	vrhat
a	a	k8xC	a
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
na	na	k7c6	na
suchých	suchý	k2eAgNnPc6d1	suché
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
stodolách	stodola	k1gFnPc6	stodola
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
<g/>
,	,	kIx,	,
podobných	podobný	k2eAgInPc6d1	podobný
hospodářských	hospodářský	k2eAgInPc6d1	hospodářský
objektech	objekt	k1gInPc6	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Lovit	lovit	k5eAaImF	lovit
chodí	chodit	k5eAaImIp3nP	chodit
především	především	k6eAd1	především
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
jídelníčku	jídelníček	k1gInSc6	jídelníček
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
drobní	drobný	k2eAgMnPc1d1	drobný
hlodavci	hlodavec	k1gMnPc1	hlodavec
a	a	k8xC	a
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
především	především	k9	především
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
a	a	k8xC	a
zahradách	zahrada	k1gFnPc6	zahrada
nebo	nebo	k8xC	nebo
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
krmeny	krmit	k5eAaImNgInP	krmit
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
loví	lovit	k5eAaImIp3nP	lovit
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
a	a	k8xC	a
často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
zvyku	zvyk	k1gInSc6	zvyk
ukazovat	ukazovat	k5eAaImF	ukazovat
své	svůj	k3xOyFgInPc4	svůj
úlovky	úlovek	k1gInPc4	úlovek
člověku	člověk	k1gMnSc6	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dne	den	k1gInSc2	den
kočka	kočka	k1gFnSc1	kočka
často	často	k6eAd1	často
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
především	především	k9	především
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
na	na	k7c6	na
vyvýšených	vyvýšený	k2eAgNnPc6d1	vyvýšené
teplých	teplý	k2eAgNnPc6d1	teplé
místech	místo	k1gNnPc6	místo
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
trámy	trám	k1gInPc1	trám
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
apod.	apod.	kA	apod.
Denně	denně	k6eAd1	denně
prospí	prospat	k5eAaPmIp3nS	prospat
až	až	k9	až
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
základní	základní	k2eAgInPc1d1	základní
tělesné	tělesný	k2eAgInPc1d1	tělesný
typy	typ	k1gInPc1	typ
koček	kočka	k1gFnPc2	kočka
<g/>
:	:	kIx,	:
zavalitý	zavalitý	k2eAgInSc1d1	zavalitý
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
kratším	krátký	k2eAgNnSc7d2	kratší
a	a	k8xC	a
kompaktním	kompaktní	k2eAgNnSc7d1	kompaktní
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
hlubokým	hluboký	k2eAgInSc7d1	hluboký
hrudníkem	hrudník	k1gInSc7	hrudník
<g/>
,	,	kIx,	,
širokou	široký	k2eAgFnSc7d1	široká
zádí	záď	k1gFnSc7	záď
<g/>
,	,	kIx,	,
krátkou	krátký	k2eAgFnSc7d1	krátká
a	a	k8xC	a
kulatou	kulatý	k2eAgFnSc7d1	kulatá
hlavou	hlava	k1gFnSc7	hlava
s	s	k7c7	s
plochým	plochý	k2eAgInSc7d1	plochý
obličejem	obličej	k1gInSc7	obličej
a	a	k8xC	a
krátkými	krátký	k2eAgFnPc7d1	krátká
končetinami	končetina	k1gFnPc7	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgNnPc7d1	typické
plemeny	plemeno	k1gNnPc7	plemeno
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
perská	perský	k2eAgFnSc1d1	perská
a	a	k8xC	a
exotická	exotický	k2eAgFnSc1d1	exotická
kočka	kočka	k1gFnSc1	kočka
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
krátkosrstá	krátkosrstý	k2eAgFnSc1d1	krátkosrstá
kočka	kočka	k1gFnSc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
je	být	k5eAaImIp3nS	být
svalnatý	svalnatý	k2eAgInSc1d1	svalnatý
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pod	pod	k7c4	pod
typ	typ	k1gInSc4	typ
zavalitý	zavalitý	k2eAgInSc4d1	zavalitý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
typické	typický	k2eAgNnSc4d1	typické
dobré	dobrý	k2eAgNnSc4d1	dobré
osvalení	osvalení	k1gNnSc4	osvalení
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnPc4d2	delší
končetiny	končetina	k1gFnPc4	končetina
než	než	k8xS	než
u	u	k7c2	u
předešlého	předešlý	k2eAgInSc2d1	předešlý
typu	typ	k1gInSc2	typ
a	a	k8xC	a
ne	ne	k9	ne
tak	tak	k6eAd1	tak
kulatá	kulatý	k2eAgFnSc1d1	kulatá
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgMnSc7d3	nejčastější
tělesným	tělesný	k2eAgInSc7d1	tělesný
typem	typ	k1gInSc7	typ
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
odlišný	odlišný	k2eAgInSc1d1	odlišný
je	být	k5eAaImIp3nS	být
útlý	útlý	k2eAgInSc1d1	útlý
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
též	též	k9	též
štihlý	štihlý	k2eAgInSc1d1	štihlý
či	či	k8xC	či
orientální	orientální	k2eAgInSc1d1	orientální
typ	typ	k1gInSc1	typ
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
mají	mít	k5eAaImIp3nP	mít
štíhlé	štíhlý	k2eAgNnSc4d1	štíhlé
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
protáhlou	protáhlý	k2eAgFnSc4d1	protáhlá
klínovitou	klínovitý	k2eAgFnSc4d1	klínovitá
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Extrémně	extrémně	k6eAd1	extrémně
štíhlý	štíhlý	k2eAgInSc1d1	štíhlý
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
současných	současný	k2eAgFnPc2d1	současná
siamských	siamský	k2eAgFnPc2d1	siamská
či	či	k8xC	či
orientálních	orientální	k2eAgFnPc2d1	orientální
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
zavalitý	zavalitý	k2eAgInSc4d1	zavalitý
a	a	k8xC	a
svalnatý	svalnatý	k2eAgInSc4d1	svalnatý
typ	typ	k1gInSc4	typ
kočky	kočka	k1gFnSc2	kočka
jsou	být	k5eAaImIp3nP	být
znakem	znak	k1gInSc7	znak
potomků	potomek	k1gMnPc2	potomek
kočky	kočka	k1gFnSc2	kočka
divoké	divoký	k2eAgFnSc2d1	divoká
evropské	evropský	k2eAgFnSc2d1	Evropská
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
útlý	útlý	k2eAgInSc1d1	útlý
typ	typ	k1gInSc1	typ
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
kočky	kočka	k1gFnSc2	kočka
plavé	plavý	k2eAgFnSc2d1	plavá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
tělesnými	tělesný	k2eAgInPc7d1	tělesný
typy	typ	k1gInPc7	typ
existují	existovat	k5eAaImIp3nP	existovat
plynulé	plynulý	k2eAgInPc1d1	plynulý
přechody	přechod	k1gInPc1	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc4	některý
plemena	plemeno	k1gNnPc4	plemeno
koček	kočka	k1gFnPc2	kočka
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
selekci	selekce	k1gFnSc3	selekce
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
výrazněji	výrazně	k6eAd2	výrazně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
japonský	japonský	k2eAgInSc1d1	japonský
bobtail	bobtail	k1gInSc1	bobtail
má	mít	k5eAaImIp3nS	mít
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
a	a	k8xC	a
deformovaný	deformovaný	k2eAgInSc1d1	deformovaný
ocas	ocas	k1gInSc1	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
curl	curl	k1gInSc1	curl
a	a	k8xC	a
skotská	skotský	k2eAgFnSc1d1	skotská
klapouchá	klapouchat	k5eAaPmIp3nS	klapouchat
kočka	kočka	k1gFnSc1	kočka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
změnami	změna	k1gFnPc7	změna
ušních	ušní	k2eAgMnPc2d1	ušní
boltců	boltec	k1gInPc2	boltec
<g/>
.	.	kIx.	.
</s>
<s>
Plemeno	plemeno	k1gNnSc1	plemeno
munchkin	munchkina	k1gFnPc2	munchkina
má	mít	k5eAaImIp3nS	mít
zase	zase	k9	zase
geneticky	geneticky	k6eAd1	geneticky
zkrácené	zkrácený	k2eAgFnPc4d1	zkrácená
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
jezevčík	jezevčík	k1gMnSc1	jezevčík
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
geneticky	geneticky	k6eAd1	geneticky
fixovanou	fixovaný	k2eAgFnSc4d1	fixovaná
chondrodysplazii	chondrodysplazie	k1gFnSc4	chondrodysplazie
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
kočky	kočka	k1gFnPc1	kočka
s	s	k7c7	s
polydaktylií	polydaktylie	k1gFnSc7	polydaktylie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pět	pět	k4xCc4	pět
prstů	prst	k1gInPc2	prst
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
je	být	k5eAaImIp3nS	být
především	především	k9	především
dokonalý	dokonalý	k2eAgMnSc1d1	dokonalý
predátor	predátor	k1gMnSc1	predátor
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
ostré	ostrý	k2eAgInPc4d1	ostrý
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
potichu	potichu	k6eAd1	potichu
plížit	plížit	k5eAaImF	plížit
i	i	k8xC	i
vyvinout	vyvinout	k5eAaPmF	vyvinout
velkou	velký	k2eAgFnSc4d1	velká
rychlost	rychlost	k1gFnSc4	rychlost
(	(	kIx(	(
<g/>
udává	udávat	k5eAaImIp3nS	udávat
se	s	k7c7	s
48	[number]	k4	48
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyzbrojena	vyzbrojen	k2eAgFnSc1d1	vyzbrojena
zuby	zub	k1gInPc7	zub
a	a	k8xC	a
zejména	zejména	k9	zejména
drápy	dráp	k1gInPc4	dráp
k	k	k7c3	k
usmrcení	usmrcení	k1gNnSc3	usmrcení
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
široká	široký	k2eAgFnSc1d1	široká
a	a	k8xC	a
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
plemeny	plemeno	k1gNnPc7	plemeno
však	však	k8xC	však
existují	existovat	k5eAaImIp3nP	existovat
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
tvaru	tvar	k1gInSc6	tvar
<g/>
:	:	kIx,	:
britské	britský	k2eAgFnSc2d1	britská
<g/>
,	,	kIx,	,
exotické	exotický	k2eAgFnSc2d1	exotická
a	a	k8xC	a
perské	perský	k2eAgFnSc2d1	perská
kočky	kočka	k1gFnSc2	kočka
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
zavalitý	zavalitý	k2eAgInSc1d1	zavalitý
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
hlavu	hlava	k1gFnSc4	hlava
nápadně	nápadně	k6eAd1	nápadně
kulatou	kulatý	k2eAgFnSc4d1	kulatá
<g/>
,	,	kIx,	,
evropská	evropský	k2eAgFnSc1d1	Evropská
krátkosrstá	krátkosrstý	k2eAgFnSc1d1	krátkosrstá
<g/>
,	,	kIx,	,
mainská	mainský	k2eAgFnSc1d1	mainská
mývalí	mývalit	k5eAaPmIp3nS	mývalit
kočka	kočka	k1gFnSc1	kočka
či	či	k8xC	či
turecká	turecký	k2eAgFnSc1d1	turecká
angora	angora	k1gFnSc1	angora
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
mírně	mírně	k6eAd1	mírně
protáhlejší	protáhlý	k2eAgFnSc7d2	protáhlejší
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
orientální	orientální	k2eAgFnSc2d1	orientální
kočky	kočka	k1gFnSc2	kočka
mají	mít	k5eAaImIp3nP	mít
lebku	lebka	k1gFnSc4	lebka
velmi	velmi	k6eAd1	velmi
protáhlou	protáhlý	k2eAgFnSc4d1	protáhlá
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInPc1d2	veliký
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nasazení	nasazení	k1gNnSc6	nasazení
uší	ucho	k1gNnPc2	ucho
či	či	k8xC	či
utváření	utváření	k1gNnSc2	utváření
stopu	stop	k1gInSc2	stop
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
jsou	být	k5eAaImIp3nP	být
nápadné	nápadný	k2eAgFnPc4d1	nápadná
velké	velký	k2eAgFnPc4d1	velká
čelisti	čelist	k1gFnPc4	čelist
a	a	k8xC	a
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
je	být	k5eAaImIp3nS	být
prstochodec	prstochodec	k1gMnSc1	prstochodec
<g/>
,	,	kIx,	,
došlapuje	došlapovat	k5eAaImIp3nS	došlapovat
na	na	k7c4	na
spodní	spodní	k2eAgFnPc4d1	spodní
plochy	plocha	k1gFnPc4	plocha
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
měkkými	měkký	k2eAgInPc7d1	měkký
polštářky	polštářek	k1gInPc7	polštářek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
má	mít	k5eAaImIp3nS	mít
kočka	kočka	k1gFnSc1	kočka
pět	pět	k4xCc4	pět
<g/>
,	,	kIx,	,
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
čtyři	čtyři	k4xCgInPc4	čtyři
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Drápy	dráp	k1gInPc1	dráp
jsou	být	k5eAaImIp3nP	být
zatažitelné	zatažitelný	k2eAgInPc1d1	zatažitelný
a	a	k8xC	a
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
skryté	skrytý	k2eAgFnSc6d1	skrytá
v	v	k7c6	v
kožních	kožní	k2eAgInPc6d1	kožní
záhybech	záhyb	k1gInPc6	záhyb
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
kočce	kočka	k1gFnSc3	kočka
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tichý	tichý	k2eAgInSc1d1	tichý
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
Mimovolným	mimovolný	k2eAgInSc7d1	mimovolný
důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
stopy	stopa	k1gFnPc1	stopa
kočkovitých	kočkovitý	k2eAgFnPc2d1	kočkovitá
šelem	šelma	k1gFnPc2	šelma
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
stop	stopa	k1gFnPc2	stopa
šelem	šelma	k1gFnPc2	šelma
psovitých	psovití	k1gMnPc2	psovití
<g/>
.	.	kIx.	.
</s>
<s>
Drápy	dráp	k1gInPc1	dráp
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
nohách	noha	k1gFnPc6	noha
si	se	k3xPyFc3	se
kočka	kočka	k1gFnSc1	kočka
obrušuje	obrušovat	k5eAaImIp3nS	obrušovat
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
škrabáním	škrabání	k1gNnSc7	škrabání
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
o	o	k7c4	o
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
předměty	předmět	k1gInPc4	předmět
(	(	kIx(	(
<g/>
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
stromy	strom	k1gInPc1	strom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnSc1d1	zadní
si	se	k3xPyFc3	se
okusuje	okusovat	k5eAaImIp3nS	okusovat
<g/>
.	.	kIx.	.
</s>
<s>
Kůže	kůže	k1gFnSc1	kůže
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
kryta	krýt	k5eAaImNgFnS	krýt
srstí	srst	k1gFnSc7	srst
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgNnPc1d1	jediné
neosrstěná	osrstěný	k2eNgNnPc1d1	osrstěný
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
nosní	nosní	k2eAgNnSc4d1	nosní
zrcátko	zrcátko	k1gNnSc4	zrcátko
<g/>
,	,	kIx,	,
polštářky	polštářek	k1gInPc4	polštářek
tlapek	tlapka	k1gFnPc2	tlapka
a	a	k8xC	a
bezprostřední	bezprostřední	k2eAgNnSc4d1	bezprostřední
okolí	okolí	k1gNnSc4	okolí
struků	struk	k1gInPc2	struk
<g/>
,	,	kIx,	,
genitálií	genitálie	k1gFnPc2	genitálie
a	a	k8xC	a
konečníku	konečník	k1gInSc2	konečník
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
až	až	k6eAd1	až
20	[number]	k4	20
000	[number]	k4	000
chlupy	chlup	k1gInPc7	chlup
na	na	k7c6	na
centimetru	centimetr	k1gInSc6	centimetr
čtverečním	čtvereční	k2eAgInSc6d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
koček	kočka	k1gFnPc2	kočka
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
typů	typ	k1gInPc2	typ
srsti	srst	k1gFnSc2	srst
-	-	kIx~	-
krátkosrstá	krátkosrstý	k2eAgFnSc1d1	krátkosrstá
kočka	kočka	k1gFnSc1	kočka
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
krátkou	krátká	k1gFnSc4	krátká
a	a	k8xC	a
přilehlou	přilehlý	k2eAgFnSc4d1	přilehlá
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
polodlouhosrsté	polodlouhosrstý	k2eAgInPc1d1	polodlouhosrstý
a	a	k8xC	a
dlouhosrsté	dlouhosrstý	k2eAgInPc1d1	dlouhosrstý
mají	mít	k5eAaImIp3nP	mít
srst	srst	k1gFnSc4	srst
delší	dlouhý	k2eAgFnSc4d2	delší
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
jemnější	jemný	k2eAgFnSc1d2	jemnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chovu	chov	k1gInSc6	chov
ušlechtilých	ušlechtilý	k2eAgFnPc2d1	ušlechtilá
koček	kočka	k1gFnPc2	kočka
se	se	k3xPyFc4	se
udržují	udržovat	k5eAaImIp3nP	udržovat
i	i	k9	i
mutace	mutace	k1gFnPc1	mutace
podmiňující	podmiňující	k2eAgFnSc4d1	podmiňující
zvlněnou	zvlněný	k2eAgFnSc4d1	zvlněná
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
mají	mít	k5eAaImIp3nP	mít
kočky	kočka	k1gFnPc1	kočka
typu	typ	k1gInSc2	typ
rex	rex	k?	rex
<g/>
,	,	kIx,	,
drátovitou	drátovitý	k2eAgFnSc4d1	drátovitá
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
plemenným	plemenný	k2eAgInSc7d1	plemenný
znakem	znak	k1gInSc7	znak
americké	americký	k2eAgFnSc2d1	americká
drsnosrsté	drsnosrstý	k2eAgFnSc2d1	drsnosrstá
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
či	či	k8xC	či
úplnou	úplný	k2eAgFnSc4d1	úplná
bezsrstost	bezsrstost	k1gFnSc4	bezsrstost
sphynxů	sphynx	k1gInPc2	sphynx
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhosrsté	dlouhosrstý	k2eAgFnPc1d1	dlouhosrstá
kočky	kočka	k1gFnPc1	kočka
línají	línat	k5eAaImIp3nP	línat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
krátkosrsté	krátkosrstý	k2eAgFnPc1d1	krátkosrstá
pak	pak	k6eAd1	pak
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zbavují	zbavovat	k5eAaImIp3nP	zbavovat
zimní	zimní	k2eAgFnSc2d1	zimní
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zbarvení	zbarvení	k1gNnSc6	zbarvení
srsti	srst	k1gFnSc2	srst
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
devět	devět	k4xCc1	devět
lokusů	lokus	k1gMnPc2	lokus
<g/>
:	:	kIx,	:
A	A	kA	A
<g/>
,	,	kIx,	,
aguti	aguti	k1gNnSc2	aguti
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nestejnoměrnou	stejnoměrný	k2eNgFnSc4d1	nestejnoměrná
pigmentaci	pigmentace	k1gFnSc4	pigmentace
chlupu	chlup	k1gInSc2	chlup
<g/>
;	;	kIx,	;
B	B	kA	B
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
plné	plný	k2eAgNnSc4d1	plné
černé	černý	k2eAgNnSc4d1	černé
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
;	;	kIx,	;
C	C	kA	C
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
plné	plný	k2eAgNnSc4d1	plné
zbarvení	zbarvení	k1gNnSc4	zbarvení
či	či	k8xC	či
odznaky	odznak	k1gInPc4	odznak
<g/>
;	;	kIx,	;
D	D	kA	D
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
případné	případný	k2eAgNnSc4d1	případné
ředění	ředění	k1gNnSc4	ředění
<g />
.	.	kIx.	.
</s>
<s>
barvy	barva	k1gFnPc1	barva
<g/>
;	;	kIx,	;
I	i	k9	i
<g/>
,	,	kIx,	,
inhibitor	inhibitor	k1gInSc4	inhibitor
zbarvení	zbarvení	k1gNnSc4	zbarvení
chlupu	chlup	k1gInSc2	chlup
<g/>
;	;	kIx,	;
O	O	kA	O
<g/>
,	,	kIx,	,
lokus	lokus	k1gInSc4	lokus
pro	pro	k7c4	pro
zrzavé	zrzavý	k2eAgNnSc4d1	zrzavé
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
;	;	kIx,	;
S	s	k7c7	s
<g/>
,	,	kIx,	,
lokus	lokus	k1gInSc1	lokus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bílou	bílý	k2eAgFnSc4d1	bílá
skvrnitost	skvrnitost	k1gFnSc4	skvrnitost
<g/>
;	;	kIx,	;
T	T	kA	T
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
kresbu	kresba	k1gFnSc4	kresba
srsti	srst	k1gFnSc2	srst
<g/>
;	;	kIx,	;
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
W	W	kA	W
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
těchto	tento	k3xDgInPc2	tento
genů	gen	k1gInPc2	gen
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
až	až	k9	až
235	[number]	k4	235
barevných	barevný	k2eAgFnPc2d1	barevná
variet	varieta	k1gFnPc2	varieta
domácí	domácí	k2eAgFnSc2d1	domácí
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgFnPc7d1	základní
barvami	barva	k1gFnPc7	barva
srsti	srst	k1gFnSc2	srst
kočky	kočka	k1gFnPc1	kočka
domácí	domácí	k1gFnPc1	domácí
jsou	být	k5eAaImIp3nP	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
čokoládová	čokoládový	k2eAgFnSc1d1	čokoládová
a	a	k8xC	a
skořicová	skořicový	k2eAgFnSc1d1	skořicová
<g/>
,	,	kIx,	,
ředěním	ředění	k1gNnSc7	ředění
těchto	tento	k3xDgFnPc2	tento
barev	barva	k1gFnPc2	barva
vzniká	vznikat	k5eAaImIp3nS	vznikat
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
želvovinová	želvovinový	k2eAgFnSc1d1	želvovinová
<g/>
,	,	kIx,	,
lila	lila	k2eAgFnSc1d1	lila
a	a	k8xC	a
plavá	plavý	k2eAgFnSc1d1	plavá
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
se	se	k3xPyFc4	se
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
barvu	barva	k1gFnSc4	barva
nepovažuje	považovat	k5eNaImIp3nS	považovat
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nedostatek	nedostatek	k1gInSc4	nedostatek
pigmentu	pigment	k1gInSc2	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgNnSc1d1	výsledné
zbarvení	zbarvení	k1gNnSc1	zbarvení
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
také	také	k9	také
kresbou	kresba	k1gFnSc7	kresba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
interakcí	interakce	k1gFnSc7	interakce
alely	alela	k1gFnSc2	alela
A	A	kA	A
a	a	k8xC	a
T.	T.	kA	T.
Rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
čtyři	čtyři	k4xCgInPc1	čtyři
druhy	druh	k1gInPc1	druh
kresby	kresba	k1gFnSc2	kresba
<g/>
:	:	kIx,	:
habešská	habešský	k2eAgFnSc1d1	habešská
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
srst	srst	k1gFnSc4	srst
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
tvořena	tvořit	k5eAaImNgFnS	tvořit
pískově	pískově	k6eAd1	pískově
žlutými	žlutý	k2eAgFnPc7d1	žlutá
chlupy	chlup	k1gInPc1	chlup
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
proužky	proužek	k1gInPc7	proužek
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
právě	právě	k9	právě
pro	pro	k7c4	pro
habešské	habešský	k2eAgFnPc4d1	habešská
kočky	kočka	k1gFnPc4	kočka
<g/>
,	,	kIx,	,
mramorovaná	mramorovaný	k2eAgFnSc1d1	mramorovaná
kresba	kresba	k1gFnSc1	kresba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
třemi	tři	k4xCgInPc7	tři
hřbetními	hřbetní	k2eAgInPc7d1	hřbetní
pruhy	pruh	k1gInPc7	pruh
a	a	k8xC	a
oválnou	oválný	k2eAgFnSc7d1	oválná
kresbou	kresba	k1gFnSc7	kresba
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
tygrovaná	tygrovaný	k2eAgFnSc1d1	tygrovaná
kresba	kresba	k1gFnSc1	kresba
a	a	k8xC	a
tečkovaná	tečkovaný	k2eAgFnSc1d1	tečkovaná
kresba	kresba	k1gFnSc1	kresba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
povoleným	povolený	k2eAgNnSc7d1	povolené
zbarvením	zbarvení	k1gNnSc7	zbarvení
ocicatů	ocicat	k1gInPc2	ocicat
a	a	k8xC	a
egyptských	egyptský	k2eAgFnPc2d1	egyptská
mau	mau	k?	mau
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
genů	gen	k1gInPc2	gen
albinotické	albinotický	k2eAgFnSc2d1	albinotická
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
omezuje	omezovat	k5eAaImIp3nS	omezovat
tvorbu	tvorba	k1gFnSc4	tvorba
pigmentu	pigment	k1gInSc2	pigment
v	v	k7c6	v
srsti	srst	k1gFnSc6	srst
<g/>
,	,	kIx,	,
kůži	kůže	k1gFnSc6	kůže
i	i	k8xC	i
oční	oční	k2eAgFnSc3d1	oční
duhovce	duhovka	k1gFnSc3	duhovka
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
siamské	siamský	k2eAgNnSc1d1	siamské
a	a	k8xC	a
barmské	barmský	k2eAgNnSc1d1	Barmské
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
světlé	světlý	k2eAgNnSc1d1	světlé
až	až	k6eAd1	až
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncových	koncový	k2eAgFnPc6d1	koncová
částech	část	k1gFnPc6	část
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc1	ucho
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pigment	pigment	k1gInSc1	pigment
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc4	oko
koček	kočka	k1gFnPc2	kočka
se	se	k3xPyFc4	se
siamským	siamský	k2eAgNnSc7d1	siamské
zesvětlením	zesvětlení	k1gNnSc7	zesvětlení
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
modré	modrý	k2eAgFnPc1d1	modrá
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
barmských	barmský	k2eAgFnPc2d1	barmská
koček	kočka	k1gFnPc2	kočka
jsou	být	k5eAaImIp3nP	být
jantarové	jantarový	k2eAgFnPc1d1	jantarová
<g/>
.	.	kIx.	.
</s>
<s>
Křížením	křížení	k1gNnSc7	křížení
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
typů	typ	k1gInPc2	typ
zbarvení	zbarvení	k1gNnPc2	zbarvení
vzniká	vznikat	k5eAaImIp3nS	vznikat
jejich	jejich	k3xOp3gInSc4	jejich
přechod	přechod	k1gInSc4	přechod
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
tonkinské	tonkinský	k2eAgNnSc4d1	tonkinský
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Anatomie	anatomie	k1gFnSc2	anatomie
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Dospělá	dospělý	k2eAgFnSc1d1	dospělá
kočka	kočka	k1gFnSc1	kočka
má	mít	k5eAaImIp3nS	mít
244	[number]	k4	244
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
512	[number]	k4	512
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Kostra	kostra	k1gFnSc1	kostra
kočky	kočka	k1gFnSc2	kočka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
páteře	páteř	k1gFnSc2	páteř
<g/>
,	,	kIx,	,
žeber	žebro	k1gNnPc2	žebro
s	s	k7c7	s
hrudní	hrudní	k2eAgFnSc7d1	hrudní
kostí	kost	k1gFnSc7	kost
a	a	k8xC	a
kostí	kost	k1gFnSc7	kost
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
pákovité	pákovitý	k2eAgFnSc2d1	pákovitý
stavby	stavba	k1gFnSc2	stavba
zadních	zadní	k2eAgFnPc2d1	zadní
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
silných	silný	k2eAgInPc2d1	silný
zádových	zádový	k2eAgInPc2d1	zádový
svalů	sval	k1gInPc2	sval
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kočce	kočka	k1gFnSc3	kočka
velice	velice	k6eAd1	velice
rychlý	rychlý	k2eAgInSc4d1	rychlý
start	start	k1gInSc4	start
a	a	k8xC	a
mohutné	mohutný	k2eAgInPc4d1	mohutný
skoky	skok	k1gInPc4	skok
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
pružnou	pružný	k2eAgFnSc4d1	pružná
páteř	páteř	k1gFnSc4	páteř
z	z	k7c2	z
volně	volně	k6eAd1	volně
spojených	spojený	k2eAgInPc2d1	spojený
obratlů	obratel	k1gInPc2	obratel
umožňující	umožňující	k2eAgNnSc1d1	umožňující
ohnutí	ohnutí	k1gNnSc1	ohnutí
těla	tělo	k1gNnSc2	tělo
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c4	o
180	[number]	k4	180
stupňů	stupeň	k1gInPc2	stupeň
tvoří	tvořit	k5eAaImIp3nS	tvořit
7	[number]	k4	7
krčních	krční	k2eAgMnPc2d1	krční
<g/>
,	,	kIx,	,
13	[number]	k4	13
hrudních	hrudní	k2eAgFnPc2d1	hrudní
a	a	k8xC	a
7	[number]	k4	7
bederních	bederní	k2eAgInPc2d1	bederní
obratlů	obratel	k1gInPc2	obratel
<g/>
,	,	kIx,	,
kost	kost	k1gFnSc1	kost
křížová	křížový	k2eAgFnSc1d1	křížová
(	(	kIx(	(
<g/>
ze	z	k7c2	z
3	[number]	k4	3
srostlých	srostlý	k2eAgInPc2d1	srostlý
obratlů	obratel	k1gInPc2	obratel
<g/>
)	)	kIx)	)
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
obratlů	obratel	k1gInPc2	obratel
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Lebka	lebka	k1gFnSc1	lebka
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
vyvinutou	vyvinutý	k2eAgFnSc7d1	vyvinutá
mozkovnou	mozkovna	k1gFnSc7	mozkovna
a	a	k8xC	a
silnými	silný	k2eAgFnPc7d1	silná
čelistmi	čelist	k1gFnPc7	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gMnPc4	domácí
má	mít	k5eAaImIp3nS	mít
úplný	úplný	k2eAgInSc1d1	úplný
sekodontní	sekodontní	k2eAgInSc1d1	sekodontní
chrup	chrup	k1gInSc1	chrup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
potravu	potrava	k1gFnSc4	potrava
trhat	trhat	k5eAaImF	trhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
žvýkat	žvýkat	k5eAaImF	žvýkat
<g/>
.	.	kIx.	.
</s>
<s>
Mléčný	mléčný	k2eAgInSc4d1	mléčný
chrup	chrup	k1gInSc4	chrup
kočky	kočka	k1gFnSc2	kočka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
26	[number]	k4	26
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
jako	jako	k8xS	jako
bezzubá	bezzubá	k1gFnSc1	bezzubá
<g/>
,	,	kIx,	,
zoubky	zoubek	k1gInPc1	zoubek
se	se	k3xPyFc4	se
prořezávají	prořezávat	k5eAaImIp3nP	prořezávat
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
dní	den	k1gInPc2	den
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
<g/>
.	.	kIx.	.
</s>
<s>
Výměna	výměna	k1gFnSc1	výměna
mléčného	mléčný	k2eAgInSc2d1	mléčný
chrupu	chrup	k1gInSc2	chrup
za	za	k7c4	za
trvalý	trvalý	k2eAgInSc4d1	trvalý
probíhá	probíhat	k5eAaImIp3nS	probíhat
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
měsíce	měsíc	k1gInSc2	měsíc
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Trvalý	trvalý	k2eAgInSc4d1	trvalý
chrup	chrup	k1gInSc4	chrup
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
30	[number]	k4	30
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
čelisti	čelist	k1gFnSc2	čelist
jich	on	k3xPp3gMnPc2	on
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
16	[number]	k4	16
<g/>
,	,	kIx,	,
z	z	k7c2	z
dolní	dolní	k2eAgFnSc2d1	dolní
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Skus	skus	k1gInSc1	skus
je	být	k5eAaImIp3nS	být
klešťovitý	klešťovitý	k2eAgInSc1d1	klešťovitý
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řezáky	řezák	k1gInPc1	řezák
obou	dva	k4xCgFnPc2	dva
čelistí	čelist	k1gFnPc2	čelist
se	se	k3xPyFc4	se
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
řeznými	řezný	k2eAgFnPc7d1	řezná
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
posuzování	posuzování	k1gNnSc6	posuzování
pravidelnosti	pravidelnost	k1gFnSc2	pravidelnost
skusu	skus	k1gInSc2	skus
u	u	k7c2	u
šlechtěných	šlechtěný	k2eAgNnPc2d1	šlechtěné
plemen	plemeno	k1gNnPc2	plemeno
koček	kočka	k1gFnPc2	kočka
se	se	k3xPyFc4	se
povoluje	povolovat	k5eAaImIp3nS	povolovat
tolerance	tolerance	k1gFnSc1	tolerance
2	[number]	k4	2
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
odchylky	odchylka	k1gFnPc1	odchylka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
předkus	předkus	k1gInSc4	předkus
či	či	k8xC	či
podkus	podkus	k1gInSc4	podkus
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
možné	možný	k2eAgFnSc3d1	možná
dědičné	dědičný	k2eAgFnSc3d1	dědičná
podmíněnosti	podmíněnost	k1gFnSc3	podmíněnost
se	se	k3xPyFc4	se
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
předkusem	předkus	k1gInSc7	předkus
či	či	k8xC	či
podkusem	podkus	k1gInSc7	podkus
vyřazují	vyřazovat	k5eAaImIp3nP	vyřazovat
z	z	k7c2	z
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gMnPc4	domácí
má	mít	k5eAaImIp3nS	mít
vynikající	vynikající	k2eAgInSc1d1	vynikající
zrak	zrak	k1gInSc1	zrak
<g/>
,	,	kIx,	,
sluch	sluch	k1gInSc1	sluch
i	i	k8xC	i
čich	čich	k1gInSc1	čich
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
člověku	člověk	k1gMnSc3	člověk
má	mít	k5eAaImIp3nS	mít
sice	sice	k8xC	sice
menší	malý	k2eAgNnSc1d2	menší
zorné	zorný	k2eAgNnSc1d1	zorné
pole	pole	k1gNnSc1	pole
(	(	kIx(	(
<g/>
205	[number]	k4	205
stupňů	stupeň	k1gInPc2	stupeň
oproti	oproti	k7c3	oproti
220	[number]	k4	220
stupňům	stupeň	k1gInPc3	stupeň
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
však	však	k9	však
ostřeji	ostro	k6eAd2	ostro
a	a	k8xC	a
v	v	k7c6	v
šeru	šer	k1gInSc6wB	šer
ji	on	k3xPp3gFnSc4	on
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
tapetum	tapetum	k1gNnSc1	tapetum
lucidum	lucidum	k1gInSc1	lucidum
<g/>
,	,	kIx,	,
vrstva	vrstva	k1gFnSc1	vrstva
buněk	buňka	k1gFnPc2	buňka
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
cévnaté	cévnatý	k2eAgFnSc6d1	cévnatá
vrstvě	vrstva	k1gFnSc6	vrstva
za	za	k7c7	za
sítnicí	sítnice	k1gFnSc7	sítnice
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odrážejí	odrážet	k5eAaImIp3nP	odrážet
dopadající	dopadající	k2eAgNnSc4d1	dopadající
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schopnost	schopnost	k1gFnSc1	schopnost
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
zlatozelený	zlatozelený	k2eAgInSc4d1	zlatozelený
svit	svit	k1gInSc4	svit
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
kočky	kočka	k1gFnSc2	kočka
za	za	k7c2	za
šera	šero	k1gNnSc2	šero
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světle	světlo	k1gNnSc6	světlo
se	se	k3xPyFc4	se
kočičí	kočičí	k2eAgFnSc1d1	kočičí
zornice	zornice	k1gFnSc1	zornice
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
do	do	k7c2	do
úzké	úzký	k2eAgFnSc2d1	úzká
štěrbiny	štěrbina	k1gFnSc2	štěrbina
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
vnímá	vnímat	k5eAaImIp3nS	vnímat
některé	některý	k3yIgFnPc4	některý
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
však	však	k9	však
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
pohyb	pohyb	k1gInSc1	pohyb
a	a	k8xC	a
barvy	barva	k1gFnPc1	barva
nehrají	hrát	k5eNaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
mají	mít	k5eAaImIp3nP	mít
výborný	výborný	k2eAgInSc4d1	výborný
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
napomáhají	napomáhat	k5eAaBmIp3nP	napomáhat
pohyblivé	pohyblivý	k2eAgInPc4d1	pohyblivý
ušní	ušní	k2eAgInPc4d1	ušní
boltce	boltec	k1gInPc4	boltec
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k2eAgFnSc1d1	domácí
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc1	schopnost
přesně	přesně	k6eAd1	přesně
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
daný	daný	k2eAgInSc4d1	daný
zvuk	zvuk	k1gInSc4	zvuk
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
slyšení	slyšení	k1gNnSc2	slyšení
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
30	[number]	k4	30
Hz	Hz	kA	Hz
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc1d1	horní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
asi	asi	k9	asi
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
oktávy	oktáva	k1gFnPc4	oktáva
výše	výše	k1gFnSc2	výše
než	než	k8xS	než
u	u	k7c2	u
lidského	lidský	k2eAgInSc2d1	lidský
sluchu	sluch	k1gInSc2	sluch
<g/>
,	,	kIx,	,
45	[number]	k4	45
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
<s>
Nosní	nosní	k2eAgFnSc1d1	nosní
sliznice	sliznice	k1gFnSc1	sliznice
kočky	kočka	k1gFnSc2	kočka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
200	[number]	k4	200
miliónů	milión	k4xCgInPc2	milión
čichových	čichový	k2eAgFnPc2d1	čichová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Čich	čich	k1gInSc1	čich
slouží	sloužit	k5eAaImIp3nS	sloužit
kočce	kočka	k1gFnSc3	kočka
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
k	k	k7c3	k
orientaci	orientace	k1gFnSc3	orientace
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
kočkami	kočka	k1gFnPc7	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gMnPc4	domácí
má	mít	k5eAaImIp3nS	mít
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
Jacobsonův	Jacobsonův	k2eAgInSc1d1	Jacobsonův
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
přijímat	přijímat	k5eAaImF	přijímat
určité	určitý	k2eAgInPc4d1	určitý
pachy	pach	k1gInPc4	pach
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
feromony	feromon	k1gInPc1	feromon
vylučované	vylučovaný	k2eAgInPc1d1	vylučovaný
říjícími	říjící	k2eAgFnPc7d1	říjící
kočkami	kočka	k1gFnPc7	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
těsně	těsně	k6eAd1	těsně
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
čichem	čich	k1gInSc7	čich
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
kočky	kočka	k1gFnPc1	kočka
nemají	mít	k5eNaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
vnímat	vnímat	k5eAaImF	vnímat
sladkou	sladký	k2eAgFnSc4d1	sladká
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Hmat	hmat	k1gInSc1	hmat
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
smyslů	smysl	k1gInPc2	smysl
nejméně	málo	k6eAd3	málo
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Nejcitlivějším	citlivý	k2eAgNnSc7d3	nejcitlivější
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
kočky	kočka	k1gFnSc2	kočka
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
čenich	čenich	k1gInSc1	čenich
a	a	k8xC	a
tlapky	tlapka	k1gFnPc1	tlapka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tlapkách	tlapka	k1gFnPc6	tlapka
jsou	být	k5eAaImIp3nP	být
citlivé	citlivý	k2eAgInPc1d1	citlivý
receptory	receptor	k1gInPc1	receptor
vnímající	vnímající	k2eAgFnSc2d1	vnímající
vibrace	vibrace	k1gFnSc2	vibrace
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yRgMnPc3	který
může	moct	k5eAaImIp3nS	moct
kočka	kočka	k1gFnSc1	kočka
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
i	i	k9	i
nepatrné	nepatrný	k2eAgNnSc4d1	nepatrný
chvění	chvění	k1gNnSc4	chvění
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
koček	kočka	k1gFnPc2	kočka
předpovídat	předpovídat	k5eAaImF	předpovídat
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
dána	dát	k5eAaPmNgFnS	dát
právě	právě	k9	právě
touto	tento	k3xDgFnSc7	tento
citlivostí	citlivost	k1gFnSc7	citlivost
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gMnPc4	domácí
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
hrudních	hrudní	k2eAgFnPc6d1	hrudní
končetinách	končetina	k1gFnPc6	končetina
a	a	k8xC	a
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
citlivé	citlivý	k2eAgInPc4d1	citlivý
hmatové	hmatový	k2eAgInPc4d1	hmatový
vousy	vous	k1gInPc4	vous
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
orientaci	orientace	k1gFnSc3	orientace
i	i	k9	i
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
tmě	tma	k1gFnSc6	tma
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
necitlivá	citlivý	k2eNgNnPc4d1	necitlivé
vůči	vůči	k7c3	vůči
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
-	-	kIx~	-
povrch	povrch	k1gInSc4	povrch
rozehřátý	rozehřátý	k2eAgInSc4d1	rozehřátý
na	na	k7c4	na
44	[number]	k4	44
°	°	k?	°
<g/>
C	C	kA	C
již	již	k6eAd1	již
člověku	člověk	k1gMnSc3	člověk
připadá	připadat	k5eAaPmIp3nS	připadat
jako	jako	k9	jako
horký	horký	k2eAgMnSc1d1	horký
a	a	k8xC	a
dotyk	dotyk	k1gInSc1	dotyk
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
bolest	bolest	k1gFnSc1	bolest
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
teplotu	teplota	k1gFnSc4	teplota
povrchu	povrch	k1gInSc2	povrch
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
nepříjemnou	příjemný	k2eNgFnSc4d1	nepříjemná
až	až	k6eAd1	až
při	při	k7c6	při
52	[number]	k4	52
°	°	k?	°
<g/>
C.	C.	kA	C.
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gFnSc2	domácí
je	být	k5eAaImIp3nS	být
teplokrevný	teplokrevný	k2eAgMnSc1d1	teplokrevný
živočich	živočich	k1gMnSc1	živočich
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
38,0	[number]	k4	38,0
<g/>
-	-	kIx~	-
<g/>
39,5	[number]	k4	39,5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
měří	měřit	k5eAaImIp3nS	měřit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
konečníku	konečník	k1gInSc2	konečník
lékařským	lékařský	k2eAgInSc7d1	lékařský
teploměrem	teploměr	k1gInSc7	teploměr
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
jedinci	jedinec	k1gMnPc1	jedinec
mají	mít	k5eAaImIp3nP	mít
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
trochu	trochu	k6eAd1	trochu
vyšší	vysoký	k2eAgFnSc4d2	vyšší
<g/>
,	,	kIx,	,
dospělí	dospělý	k2eAgMnPc1d1	dospělý
kastráti	kastrát	k1gMnPc1	kastrát
naopak	naopak	k6eAd1	naopak
trochu	trochu	k6eAd1	trochu
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Dechová	dechový	k2eAgFnSc1d1	dechová
frekvence	frekvence	k1gFnSc1	frekvence
zdravé	zdravý	k2eAgFnSc2d1	zdravá
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
20	[number]	k4	20
do	do	k7c2	do
30	[number]	k4	30
nádechů	nádech	k1gInPc2	nádech
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
,	,	kIx,	,
tepová	tepový	k2eAgFnSc1d1	tepová
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
110	[number]	k4	110
až	až	k9	až
130	[number]	k4	130
tepy	tep	k1gInPc4	tep
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tvor	tvor	k1gMnSc1	tvor
s	s	k7c7	s
krepuskulární	krepuskulární	k2eAgFnSc7d1	krepuskulární
aktivitou	aktivita	k1gFnSc7	aktivita
a	a	k8xC	a
přirozeně	přirozeně	k6eAd1	přirozeně
je	být	k5eAaImIp3nS	být
nejaktivnější	aktivní	k2eAgInSc4d3	nejaktivnější
večer	večer	k1gInSc4	večer
a	a	k8xC	a
v	v	k7c6	v
brzkých	brzký	k2eAgFnPc6d1	brzká
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
prospí	prospat	k5eAaPmIp3nS	prospat
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
útlý	útlý	k2eAgInSc4d1	útlý
<g/>
,	,	kIx,	,
orientální	orientální	k2eAgInSc4d1	orientální
typ	typ	k1gInSc4	typ
je	být	k5eAaImIp3nS	být
aktivnější	aktivní	k2eAgMnSc1d2	aktivnější
než	než	k8xS	než
typy	typ	k1gInPc1	typ
zavalité	zavalitý	k2eAgInPc1d1	zavalitý
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
žijící	žijící	k2eAgFnPc1d1	žijící
divoce	divoce	k6eAd1	divoce
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
doma	doma	k6eAd1	doma
chované	chovaný	k2eAgFnPc1d1	chovaná
kastrované	kastrovaný	k2eAgFnPc1d1	kastrovaná
kočky	kočka	k1gFnPc1	kočka
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Guinessova	Guinessův	k2eAgFnSc1d1	Guinessova
kniha	kniha	k1gFnSc1	kniha
rekordů	rekord	k1gInPc2	rekord
uznává	uznávat	k5eAaImIp3nS	uznávat
jako	jako	k9	jako
nejstarší	starý	k2eAgFnSc4d3	nejstarší
kočku	kočka	k1gFnSc4	kočka
Creme	Crem	k1gInSc5	Crem
Puff	puff	k1gInSc1	puff
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1967	[number]	k4	1967
a	a	k8xC	a
dožila	dožít	k5eAaPmAgFnS	dožít
se	s	k7c7	s
38	[number]	k4	38
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
věk	věk	k1gInSc4	věk
dožití	dožití	k1gNnSc4	dožití
kočky	kočka	k1gFnSc2	kočka
působí	působit	k5eAaImIp3nS	působit
mnoho	mnoho	k4c1	mnoho
faktorů	faktor	k1gInPc2	faktor
-	-	kIx~	-
její	její	k3xOp3gNnSc1	její
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
,	,	kIx,	,
výživa	výživa	k1gFnSc1	výživa
a	a	k8xC	a
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
chodí	chodit	k5eAaImIp3nP	chodit
ven	ven	k6eAd1	ven
a	a	k8xC	a
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
úrazu	úraz	k1gInSc2	úraz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistik	statistika	k1gFnPc2	statistika
se	se	k3xPyFc4	se
kastrovaná	kastrovaný	k2eAgNnPc1d1	kastrované
zvířata	zvíře	k1gNnPc1	zvíře
dožívají	dožívat	k5eAaImIp3nP	dožívat
delšího	dlouhý	k2eAgInSc2d2	delší
věku	věk	k1gInSc2	věk
než	než	k8xS	než
nekastrovaná	kastrovaný	k2eNgNnPc4d1	nekastrované
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
toulat	toulat	k5eAaImF	toulat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
koček	kočka	k1gFnPc2	kočka
pohlavně	pohlavně	k6eAd1	pohlavně
dospívá	dospívat	k5eAaImIp3nS	dospívat
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
obvykle	obvykle	k6eAd1	obvykle
dospívají	dospívat	k5eAaImIp3nP	dospívat
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
kocouři	kocour	k1gMnPc1	kocour
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
jedinců	jedinec	k1gMnPc2	jedinec
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
pohlavní	pohlavní	k2eAgFnSc3d1	pohlavní
dospělosti	dospělost	k1gFnSc3	dospělost
už	už	k6eAd1	už
v	v	k7c6	v
6	[number]	k4	6
měsících	měsíc	k1gInPc6	měsíc
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
větších	veliký	k2eAgFnPc6d2	veliký
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pohlavní	pohlavní	k2eAgFnSc3d1	pohlavní
dospělosti	dospělost	k1gFnSc3	dospělost
až	až	k9	až
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
věku	věk	k1gInSc2	věk
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Časnější	časný	k2eAgInSc1d2	časnější
nástup	nástup	k1gInSc1	nástup
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
styk	styk	k1gInSc4	styk
s	s	k7c7	s
již	již	k6eAd1	již
cyklujícími	cyklující	k2eAgFnPc7d1	cyklující
kočkami	kočka	k1gFnPc7	kočka
či	či	k8xC	či
kocoury	kocour	k1gInPc7	kocour
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
kočky	kočka	k1gFnPc1	kočka
tělesně	tělesně	k6eAd1	tělesně
dospívají	dospívat	k5eAaImIp3nP	dospívat
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
než	než	k8xS	než
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
dospělosti	dospělost	k1gFnSc3	dospělost
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
<g/>
,	,	kIx,	,
neměly	mít	k5eNaImAgInP	mít
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nakrývat	nakrývat	k5eAaImF	nakrývat
před	před	k7c7	před
dosažením	dosažení	k1gNnSc7	dosažení
alespoň	alespoň	k9	alespoň
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
dva	dva	k4xCgInPc4	dva
vrhy	vrh	k1gInPc4	vrh
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
deset	deset	k4xCc4	deset
koťat	kotě	k1gNnPc2	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
domácí	domácí	k1gMnPc4	domácí
je	být	k5eAaImIp3nS	být
sezónně	sezónně	k6eAd1	sezónně
polyestrické	polyestrický	k2eAgNnSc1d1	polyestrický
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
k	k	k7c3	k
říji	říje	k1gFnSc3	říje
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
vyjma	vyjma	k7c2	vyjma
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
horkého	horký	k2eAgNnSc2d1	horké
léta	léto	k1gNnSc2	léto
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
říje	říje	k1gFnSc1	říje
omezená	omezený	k2eAgFnSc1d1	omezená
jen	jen	k9	jen
na	na	k7c4	na
jaro	jaro	k1gNnSc4	jaro
a	a	k8xC	a
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Říje	říje	k1gFnSc1	říje
trvá	trvat	k5eAaImIp3nS	trvat
u	u	k7c2	u
koček	kočka	k1gFnPc2	kočka
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
u	u	k7c2	u
konkrétních	konkrétní	k2eAgMnPc2d1	konkrétní
jedinců	jedinec	k1gMnPc2	jedinec
velice	velice	k6eAd1	velice
variabilní	variabilní	k2eAgInSc1d1	variabilní
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
však	však	k9	však
vejde	vejít	k5eAaPmIp3nS	vejít
do	do	k7c2	do
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
říjemi	říje	k1gFnPc7	říje
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
až	až	k8xS	až
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Klasickými	klasický	k2eAgInPc7d1	klasický
příznaky	příznak	k1gInPc7	příznak
říje	říje	k1gFnSc2	říje
jsou	být	k5eAaImIp3nP	být
neklid	neklid	k1gInSc4	neklid
<g/>
,	,	kIx,	,
protahování	protahování	k1gNnSc4	protahování
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
otírání	otírání	k1gNnSc1	otírání
se	se	k3xPyFc4	se
o	o	k7c4	o
nohy	noha	k1gFnPc4	noha
chovatele	chovatel	k1gMnSc2	chovatel
<g/>
,	,	kIx,	,
nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
a	a	k8xC	a
snížený	snížený	k2eAgInSc4d1	snížený
příjem	příjem	k1gInSc4	příjem
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
vytrvalé	vytrvalý	k2eAgFnSc2d1	vytrvalá
snahy	snaha	k1gFnSc2	snaha
uniknout	uniknout	k5eAaPmF	uniknout
ven	ven	k6eAd1	ven
za	za	k7c7	za
kocourem	kocour	k1gInSc7	kocour
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
silnějším	silný	k2eAgNnSc6d2	silnější
pohlazení	pohlazení	k1gNnSc6	pohlazení
po	po	k7c6	po
zádech	záda	k1gNnPc6	záda
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
koček	kočka	k1gFnPc2	kočka
staví	stavit	k5eAaImIp3nS	stavit
do	do	k7c2	do
pářícího	pářící	k2eAgInSc2d1	pářící
postoje	postoj	k1gInSc2	postoj
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnSc2d1	přední
končetiny	končetina	k1gFnSc2	končetina
natažené	natažený	k2eAgFnPc1d1	natažená
vpředu	vpředu	k6eAd1	vpředu
<g/>
,	,	kIx,	,
hrudník	hrudník	k1gInSc1	hrudník
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
pokrčené	pokrčený	k2eAgFnPc1d1	pokrčená
zadní	zadní	k2eAgFnPc4d1	zadní
nohy	noha	k1gFnPc4	noha
posunuté	posunutý	k2eAgFnPc4d1	posunutá
pod	pod	k7c7	pod
břichem	břicho	k1gNnSc7	břicho
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
nahoře	nahoře	k6eAd1	nahoře
nebo	nebo	k8xC	nebo
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kocouři	kocour	k1gMnPc1	kocour
v	v	k7c6	v
období	období	k1gNnSc6	období
říje	říje	k1gFnSc2	říje
projevují	projevovat	k5eAaImIp3nP	projevovat
taktéž	taktéž	k?	taktéž
neklid	neklid	k1gInSc4	neklid
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
cítit	cítit	k5eAaImF	cítit
a	a	k8xC	a
ve	v	k7c6	v
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
míře	míra	k1gFnSc6	míra
značkují	značkovat	k5eAaImIp3nP	značkovat
močí	močit	k5eAaImIp3nP	močit
své	svůj	k3xOyFgNnSc4	svůj
teritorium	teritorium	k1gNnSc4	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Volně	volně	k6eAd1	volně
žijící	žijící	k2eAgFnPc1d1	žijící
kočky	kočka	k1gFnPc1	kočka
ohlašují	ohlašovat	k5eAaImIp3nP	ohlašovat
říji	říje	k1gFnSc4	říje
močí	moč	k1gFnPc2	moč
a	a	k8xC	a
naléhavým	naléhavý	k2eAgNnSc7d1	naléhavé
voláním	volání	k1gNnSc7	volání
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
často	často	k6eAd1	často
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
více	hodně	k6eAd2	hodně
kocourů	kocour	k1gMnPc2	kocour
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
se	se	k3xPyFc4	se
otírá	otírat	k5eAaImIp3nS	otírat
o	o	k7c4	o
předměty	předmět	k1gInPc4	předmět
a	a	k8xC	a
válí	válet	k5eAaImIp3nS	válet
se	se	k3xPyFc4	se
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kocouři	kocour	k1gMnPc1	kocour
přilákáni	přilákat	k5eAaPmNgMnP	přilákat
pachem	pach	k1gInSc7	pach
a	a	k8xC	a
voláním	volání	k1gNnSc7	volání
říjící	říjící	k2eAgFnSc2d1	říjící
se	se	k3xPyFc4	se
samice	samice	k1gFnSc2	samice
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
se	se	k3xPyFc4	se
pářit	pářit	k5eAaImF	pářit
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
stačí	stačit	k5eAaBmIp3nS	stačit
jen	jen	k9	jen
ritualizovaná	ritualizovaný	k2eAgFnSc1d1	ritualizovaná
agrese	agrese	k1gFnSc1	agrese
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
však	však	k9	však
dojde	dojít	k5eAaPmIp3nS	dojít
i	i	k9	i
k	k	k7c3	k
fyzické	fyzický	k2eAgFnSc3d1	fyzická
konfrontaci	konfrontace	k1gFnSc3	konfrontace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kočkou	kočka	k1gFnSc7	kočka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
spojí	spojit	k5eAaPmIp3nS	spojit
jen	jen	k9	jen
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
kocour	kocour	k1gMnSc1	kocour
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
výjimkou	výjimka	k1gFnSc7	výjimka
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
superfekundace	superfekundace	k1gFnSc1	superfekundace
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
říje	říje	k1gFnSc2	říje
oplodněna	oplodnit	k5eAaPmNgFnS	oplodnit
více	hodně	k6eAd2	hodně
kocoury	kocour	k1gMnPc4	kocour
a	a	k8xC	a
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
jsou	být	k5eAaImIp3nP	být
koťata	kotě	k1gNnPc1	kotě
různých	různý	k2eAgMnPc2d1	různý
otců	otec	k1gMnPc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
připravená	připravený	k2eAgFnSc1d1	připravená
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
to	ten	k3xDgNnSc4	ten
kocourovi	kocour	k1gMnSc3	kocour
najevo	najevo	k6eAd1	najevo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaujme	zaujmout	k5eAaPmIp3nS	zaujmout
pářící	pářící	k2eAgInSc4d1	pářící
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Koitus	koitus	k1gInSc1	koitus
trvá	trvat	k5eAaImIp3nS	trvat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
sekund	sekunda	k1gFnPc2	sekunda
a	a	k8xC	a
skončí	skončit	k5eAaPmIp3nS	skončit
násilným	násilný	k2eAgNnSc7d1	násilné
odstrčením	odstrčení	k1gNnSc7	odstrčení
kocoura	kocour	k1gMnSc2	kocour
kočkou	kočka	k1gFnSc7	kočka
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
může	moct	k5eAaImIp3nS	moct
kocoura	kocour	k1gMnSc4	kocour
i	i	k8xC	i
napadnout	napadnout	k5eAaPmF	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
styku	styk	k1gInSc2	styk
s	s	k7c7	s
kocourem	kocour	k1gInSc7	kocour
teprve	teprve	k6eAd1	teprve
drážděním	dráždění	k1gNnSc7	dráždění
samičích	samičí	k2eAgFnPc2d1	samičí
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
cest	cesta	k1gFnPc2	cesta
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
ovulaci	ovulace	k1gFnSc3	ovulace
(	(	kIx(	(
<g/>
provokovaná	provokovaný	k2eAgFnSc1d1	provokovaná
ovulace	ovulace	k1gFnSc1	ovulace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nespáří	spářit	k5eNaPmIp3nS	spářit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
říjový	říjový	k2eAgInSc1d1	říjový
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
proestru	proestr	k1gInSc2	proestr
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
říje	říje	k1gFnSc2	říje
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
už	už	k9	už
za	za	k7c4	za
další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kočky	kočka	k1gFnSc2	kočka
může	moct	k5eAaImIp3nS	moct
říje	říje	k1gFnSc1	říje
nastat	nastat	k5eAaPmF	nastat
i	i	k9	i
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
březosti	březost	k1gFnSc2	březost
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pářit	pářit	k5eAaImF	pářit
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
superfetace	superfetace	k1gFnSc1	superfetace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
děloze	děloha	k1gFnSc6	děloha
kočky	kočka	k1gFnSc2	kočka
plody	plod	k1gInPc4	plod
různého	různý	k2eAgNnSc2d1	různé
stáří	stáří	k1gNnSc2	stáří
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
domácí	domácí	k2eAgFnSc2d1	domácí
kočky	kočka	k1gFnSc2	kočka
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
fyziologická	fyziologický	k2eAgFnSc1d1	fyziologická
délka	délka	k1gFnSc1	délka
březosti	březost	k1gFnSc2	březost
počítána	počítat	k5eAaImNgFnS	počítat
od	od	k7c2	od
krytí	krytí	k1gNnSc2	krytí
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
56	[number]	k4	56
do	do	k7c2	do
71	[number]	k4	71
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
průměrně	průměrně	k6eAd1	průměrně
65	[number]	k4	65
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Březí	březí	k2eAgFnPc1d1	březí
kočky	kočka	k1gFnPc1	kočka
bývají	bývat	k5eAaImIp3nP	bývat
klidnější	klidný	k2eAgFnPc1d2	klidnější
a	a	k8xC	a
přijímají	přijímat	k5eAaImIp3nP	přijímat
více	hodně	k6eAd2	hodně
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
příznakem	příznak	k1gInSc7	příznak
březosti	březost	k1gFnSc2	březost
je	být	k5eAaImIp3nS	být
ztmavnutí	ztmavnutí	k1gNnSc1	ztmavnutí
struků	struk	k1gInPc2	struk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
za	za	k7c4	za
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
po	po	k7c4	po
krytí	krytí	k1gNnSc4	krytí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
třetině	třetina	k1gFnSc6	třetina
březosti	březost	k1gFnSc2	březost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
koček	kočka	k1gFnPc2	kočka
vidět	vidět	k5eAaImF	vidět
zvětšené	zvětšený	k2eAgNnSc4d1	zvětšené
břicho	břicho	k1gNnSc4	břicho
a	a	k8xC	a
slabiny	slabina	k1gFnPc4	slabina
<g/>
.	.	kIx.	.
</s>
<s>
Mléčné	mléčný	k2eAgFnPc1d1	mléčná
žlázy	žláza	k1gFnPc1	žláza
bývají	bývat	k5eAaImIp3nP	bývat
zvětšené	zvětšený	k2eAgFnPc1d1	zvětšená
a	a	k8xC	a
překrvené	překrvený	k2eAgFnPc1d1	překrvená
<g/>
,	,	kIx,	,
patrné	patrný	k2eAgFnPc1d1	patrná
v	v	k7c6	v
srsti	srst	k1gFnSc6	srst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
třetině	třetina	k1gFnSc6	třetina
březosti	březost	k1gFnSc2	březost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
větším	veliký	k2eAgInSc6d2	veliký
počtu	počet	k1gInSc6	počet
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc1	břicho
nápadně	nápadně	k6eAd1	nápadně
zvětšené	zvětšený	k2eAgNnSc1d1	zvětšené
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
26	[number]	k4	26
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
březosti	březost	k1gFnSc2	březost
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
diagnostika	diagnostika	k1gFnSc1	diagnostika
pomocí	pomocí	k7c2	pomocí
ultrazvukového	ultrazvukový	k2eAgNnSc2d1	ultrazvukové
vyšetření	vyšetření	k1gNnSc2	vyšetření
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
pářily	pářit	k5eAaImAgFnP	pářit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezabřezly	zabřeznout	k5eNaPmAgFnP	zabřeznout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
falešná	falešný	k2eAgFnSc1d1	falešná
březost	březost	k1gFnSc1	březost
<g/>
,	,	kIx,	,
pseudogravidita	pseudogravidita	k1gFnSc1	pseudogravidita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
věrně	věrně	k6eAd1	věrně
připomíná	připomínat	k5eAaImIp3nS	připomínat
březost	březost	k1gFnSc4	březost
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc4	stav
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgMnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ovulaci	ovulace	k1gFnSc3	ovulace
a	a	k8xC	a
hormonální	hormonální	k2eAgNnSc1d1	hormonální
vyladění	vyladění	k1gNnSc1	vyladění
organismu	organismus	k1gInSc2	organismus
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
skutečné	skutečný	k2eAgFnPc4d1	skutečná
březosti	březost	k1gFnPc4	březost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
plodů	plod	k1gInPc2	plod
v	v	k7c6	v
děloze	děloha	k1gFnSc6	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
porodu	porod	k1gInSc2	porod
končí	končit	k5eAaImIp3nS	končit
říjí	říje	k1gFnSc7	říje
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nastane	nastat	k5eAaPmIp3nS	nastat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
očekávaného	očekávaný	k2eAgInSc2d1	očekávaný
porodu	porod	k1gInSc2	porod
nebo	nebo	k8xC	nebo
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dnech	den	k1gInPc6	den
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zvětšení	zvětšení	k1gNnSc3	zvětšení
mléčných	mléčný	k2eAgFnPc2d1	mléčná
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
uvolnění	uvolnění	k1gNnSc4	uvolnění
pánve	pánev	k1gFnSc2	pánev
a	a	k8xC	a
ke	k	k7c3	k
zduření	zduření	k1gNnSc3	zduření
vulvy	vulva	k1gFnSc2	vulva
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
neklidná	klidný	k2eNgFnSc1d1	neklidná
<g/>
,	,	kIx,	,
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
tmavá	tmavý	k2eAgNnPc4d1	tmavé
a	a	k8xC	a
tichá	tichý	k2eAgNnPc4d1	tiché
místa	místo	k1gNnPc4	místo
k	k	k7c3	k
vrhu	vrh	k1gInSc3	vrh
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
porodu	porod	k1gInSc2	porod
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgFnSc1d1	individuální
a	a	k8xC	a
záleží	záležet	k5eAaImIp3nS	záležet
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
vrhu	vrh	k1gInSc2	vrh
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
až	až	k9	až
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejně	obyčejně	k6eAd1	obyčejně
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
4	[number]	k4	4
až	až	k9	až
6	[number]	k4	6
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
až	až	k9	až
10	[number]	k4	10
koťat	kotě	k1gNnPc2	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kotě	kotě	k1gNnSc1	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
slepá	slepý	k2eAgFnSc1d1	slepá
a	a	k8xC	a
hluchá	hluchý	k2eAgFnSc1d1	hluchá
<g/>
,	,	kIx,	,
osrstěná	osrstěný	k2eAgFnSc1d1	osrstěná
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
57	[number]	k4	57
<g/>
-	-	kIx~	-
<g/>
114	[number]	k4	114
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
mají	mít	k5eAaImIp3nP	mít
hned	hned	k6eAd1	hned
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
čich	čich	k1gInSc4	čich
a	a	k8xC	a
hmat	hmat	k1gInSc4	hmat
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
smysly	smysl	k1gInPc1	smysl
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
bradavky	bradavka	k1gFnSc2	bradavka
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
se	se	k3xPyFc4	se
matce	matka	k1gFnSc3	matka
začne	začít	k5eAaPmIp3nS	začít
vylučovat	vylučovat	k5eAaImF	vylučovat
mlezivo	mlezivo	k1gNnSc1	mlezivo
(	(	kIx(	(
<g/>
kolostrum	kolostrum	k1gNnSc1	kolostrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
potrava	potrava	k1gFnSc1	potrava
pro	pro	k7c4	pro
novorozená	novorozený	k2eAgNnPc4d1	novorozené
koťata	kotě	k1gNnPc4	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
jsou	být	k5eAaImIp3nP	být
koťata	kotě	k1gNnPc1	kotě
krmena	krmen	k2eAgNnPc1d1	krmeno
mlékem	mléko	k1gNnSc7	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Kočičí	kočičí	k2eAgNnSc1d1	kočičí
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
albuminové	albuminový	k2eAgNnSc1d1	albuminový
<g/>
,	,	kIx,	,
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
kasein	kasein	k1gInSc4	kasein
a	a	k8xC	a
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
laktózy	laktóza	k1gFnSc2	laktóza
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
kravské	kravský	k2eAgNnSc1d1	kravské
mléko	mléko	k1gNnSc1	mléko
jako	jako	k8xC	jako
náhradní	náhradní	k2eAgFnSc1d1	náhradní
potrava	potrava	k1gFnSc1	potrava
pro	pro	k7c4	pro
koťata	kotě	k1gNnPc4	kotě
nevhodné	vhodný	k2eNgFnSc2d1	nevhodná
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Náhradním	náhradní	k2eAgNnSc7d1	náhradní
řešením	řešení	k1gNnSc7	řešení
pro	pro	k7c4	pro
krmení	krmení	k1gNnSc4	krmení
koťat	kotě	k1gNnPc2	kotě
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ztratila	ztratit	k5eAaPmAgFnS	ztratit
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
mléčné	mléčný	k2eAgFnPc4d1	mléčná
náhražky	náhražka	k1gFnPc4	náhražka
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nouzi	nouze	k1gFnSc6	nouze
kondenzované	kondenzovaný	k2eAgNnSc1d1	kondenzované
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
ředěné	ředěný	k2eAgNnSc1d1	ředěné
vlažnou	vlažný	k2eAgFnSc7d1	vlažná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
podporují	podporovat	k5eAaImIp3nP	podporovat
produkci	produkce	k1gFnSc4	produkce
mléka	mléko	k1gNnSc2	mléko
masírováním	masírování	k1gNnPc3	masírování
okolí	okolí	k1gNnSc6	okolí
bradavek	bradavka	k1gFnPc2	bradavka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mléčný	mléčný	k2eAgInSc4d1	mléčný
krok	krok	k1gInSc4	krok
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
předením	předení	k1gNnSc7	předení
<g/>
.	.	kIx.	.
</s>
<s>
Silnější	silný	k2eAgMnPc1d2	silnější
jedinci	jedinec	k1gMnPc1	jedinec
obvykle	obvykle	k6eAd1	obvykle
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
přední	přední	k2eAgInPc1d1	přední
páry	pár	k1gInPc1	pár
bradavek	bradavka	k1gFnPc2	bradavka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
produkce	produkce	k1gFnSc1	produkce
mléka	mléko	k1gNnSc2	mléko
intenzivnější	intenzivní	k2eAgFnSc1d2	intenzivnější
<g/>
.	.	kIx.	.
</s>
<s>
Pijí	pít	k5eAaImIp3nP	pít
několikrát	několikrát	k6eAd1	několikrát
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
času	čas	k1gInSc2	čas
prospí	prospat	k5eAaPmIp3nS	prospat
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
koťatům	kotě	k1gNnPc3	kotě
masíruje	masírovat	k5eAaImIp3nS	masírovat
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
anální	anální	k2eAgInSc4d1	anální
otvor	otvor	k1gInSc4	otvor
a	a	k8xC	a
genitálie	genitálie	k1gFnPc4	genitálie
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
podporuje	podporovat	k5eAaImIp3nS	podporovat
vylučování	vylučování	k1gNnSc4	vylučování
moči	moč	k1gFnSc2	moč
a	a	k8xC	a
trusu	trus	k1gInSc2	trus
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
sama	sám	k3xTgNnPc1	sám
ještě	ještě	k9	ještě
nejsou	být	k5eNaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
samostatného	samostatný	k2eAgNnSc2d1	samostatné
vylučování	vylučování	k1gNnSc2	vylučování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tého	tého	k6eAd1	tého
fázi	fáze	k1gFnSc6	fáze
koťata	kotě	k1gNnPc1	kotě
rychle	rychle	k6eAd1	rychle
rostou	růst	k5eAaImIp3nP	růst
a	a	k8xC	a
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
zdvojnásobit	zdvojnásobit	k5eAaPmF	zdvojnásobit
svojí	svůj	k3xOyFgFnSc7	svůj
porodní	porodní	k2eAgFnSc7d1	porodní
váhu	váha	k1gFnSc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
otvírají	otvírat	k5eAaImIp3nP	otvírat
oči	oko	k1gNnPc4	oko
kolem	kolem	k6eAd1	kolem
10	[number]	k4	10
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
váží	vážit	k5eAaImIp3nS	vážit
okolo	okolo	k7c2	okolo
200	[number]	k4	200
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Začínají	začínat	k5eAaImIp3nP	začínat
vnímat	vnímat	k5eAaImF	vnímat
svoje	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
a	a	k8xC	a
aktivně	aktivně	k6eAd1	aktivně
ho	on	k3xPp3gMnSc4	on
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
<g/>
.	.	kIx.	.
</s>
<s>
Učí	učit	k5eAaImIp3nP	učit
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
a	a	k8xC	a
od	od	k7c2	od
třetího	třetí	k4xOgInSc2	třetí
týdne	týden	k1gInSc2	týden
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgFnPc1	první
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
běhat	běhat	k5eAaImF	běhat
<g/>
,	,	kIx,	,
skákat	skákat	k5eAaImF	skákat
<g/>
,	,	kIx,	,
plížit	plížit	k5eAaImF	plížit
se	se	k3xPyFc4	se
a	a	k8xC	a
lovit	lovit	k5eAaImF	lovit
<g/>
,	,	kIx,	,
koťata	kotě	k1gNnPc1	kotě
poznávají	poznávat	k5eAaImIp3nP	poznávat
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgInSc2	první
měsíce	měsíc	k1gInSc2	měsíc
věku	věk	k1gInSc2	věk
váží	vážit	k5eAaImIp3nS	vážit
okolo	okolo	k7c2	okolo
300	[number]	k4	300
gramů	gram	k1gInPc2	gram
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
se	se	k3xPyFc4	se
sama	sám	k3xTgMnSc4	sám
čistit	čistit	k5eAaImF	čistit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
chovatel	chovatel	k1gMnSc1	chovatel
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
přikrmovat	přikrmovat	k5eAaImF	přikrmovat
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
jsou	být	k5eAaImIp3nP	být
čistotná	čistotný	k2eAgNnPc1d1	čistotné
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
naučit	naučit	k5eAaPmF	naučit
používat	používat	k5eAaImF	používat
kočičí	kočičí	k2eAgFnSc4d1	kočičí
toaletu	toaleta	k1gFnSc4	toaleta
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
pátým	pátý	k4xOgInSc7	pátý
až	až	k8xS	až
sedmým	sedmý	k4xOgInSc7	sedmý
týdnem	týden	k1gInSc7	týden
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
socializaci	socializace	k1gFnSc4	socializace
koťat	kotě	k1gNnPc2	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
si	se	k3xPyFc3	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
druhu	druh	k1gInSc3	druh
a	a	k8xC	a
k	k	k7c3	k
druhům	druh	k1gInPc3	druh
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgNnPc7	který
přicházejí	přicházet	k5eAaImIp3nP	přicházet
do	do	k7c2	do
styku	styk	k1gInSc2	styk
<g/>
.	.	kIx.	.
</s>
<s>
Koťata	kotě	k1gNnPc1	kotě
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
neměla	mít	k5eNaImAgFnS	mít
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
lidem	lid	k1gInSc7	lid
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
nevyrostou	vyrůst	k5eNaPmIp3nP	vyrůst
přítulné	přítulný	k2eAgFnPc1d1	přítulná
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
koťat	kotě	k1gNnPc2	kotě
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
možnost	možnost	k1gFnSc1	možnost
socializace	socializace	k1gFnSc1	socializace
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
deseti	deset	k4xCc2	deset
týdnů	týden	k1gInPc2	týden
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
koťata	kotě	k1gNnPc4	kotě
vůči	vůči	k7c3	vůči
novým	nový	k2eAgFnPc3d1	nová
věcem	věc	k1gFnPc3	věc
obezřetnější	obezřetný	k2eAgFnSc2d2	obezřetnější
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
povahy	povaha	k1gFnSc2	povaha
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
plně	plně	k6eAd1	plně
odstavená	odstavený	k2eAgFnSc1d1	odstavená
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
týdnů	týden	k1gInPc2	týden
věku	věk	k1gInSc2	věk
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ušlechtilých	ušlechtilý	k2eAgNnPc2d1	ušlechtilé
plemen	plemeno	k1gNnPc2	plemeno
koček	kočka	k1gFnPc2	kočka
doporučený	doporučený	k2eAgInSc4d1	doporučený
věk	věk	k1gInSc4	věk
odběru	odběr	k1gInSc2	odběr
koťat	kotě	k1gNnPc2	kotě
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
prodeji	prodej	k1gInSc6	prodej
novému	nový	k2eAgMnSc3d1	nový
majiteli	majitel	k1gMnSc3	majitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
jsou	být	k5eAaImIp3nP	být
koťata	kotě	k1gNnPc4	kotě
velmi	velmi	k6eAd1	velmi
hravá	hravý	k2eAgNnPc4d1	hravé
a	a	k8xC	a
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
sociálnímu	sociální	k2eAgNnSc3d1	sociální
chování	chování	k1gNnSc3	chování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
koťata	kotě	k1gNnPc1	kotě
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
následují	následovat	k5eAaImIp3nP	následovat
při	při	k7c6	při
každodenních	každodenní	k2eAgFnPc6d1	každodenní
činnostech	činnost	k1gFnPc6	činnost
a	a	k8xC	a
učí	učit	k5eAaImIp3nP	učit
se	se	k3xPyFc4	se
samostatně	samostatně	k6eAd1	samostatně
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
pátého	pátý	k4xOgInSc2	pátý
měsíce	měsíc	k1gInSc2	měsíc
věku	věk	k1gInSc2	věk
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
vyniknou	vyniknout	k5eAaPmIp3nP	vyniknout
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
kočkou	kočka	k1gFnSc7	kočka
a	a	k8xC	a
kocourem	kocour	k1gInSc7	kocour
<g/>
.	.	kIx.	.
</s>
<s>
Kocour	kocour	k1gInSc1	kocour
bývá	bývat	k5eAaImIp3nS	bývat
větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
širší	široký	k2eAgInSc4d2	širší
krk	krk	k1gInSc4	krk
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
bývají	bývat	k5eAaImIp3nP	bývat
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
hlava	hlava	k1gFnSc1	hlava
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
(	(	kIx(	(
<g/>
záleží	záležet	k5eAaImIp3nS	záležet
však	však	k9	však
na	na	k7c6	na
plemeni	plemeno	k1gNnSc6	plemeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nekastrovaný	kastrovaný	k2eNgInSc1d1	nekastrovaný
kocour	kocour	k1gInSc1	kocour
je	být	k5eAaImIp3nS	být
teritoriální	teritoriální	k2eAgInSc1d1	teritoriální
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
revír	revír	k1gInSc4	revír
si	se	k3xPyFc3	se
značí	značit	k5eAaImIp3nS	značit
močí	moč	k1gFnSc7	moč
a	a	k8xC	a
otíráním	otírání	k1gNnSc7	otírání
se	se	k3xPyFc4	se
o	o	k7c4	o
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
teritoria	teritorium	k1gNnSc2	teritorium
vyhání	vyhánět	k5eAaImIp3nP	vyhánět
vetřelce	vetřelec	k1gMnPc4	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kočky	kočka	k1gFnSc2	kočka
se	se	k3xPyFc4	se
toulá	toulat	k5eAaImIp3nS	toulat
a	a	k8xC	a
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
samice	samice	k1gFnSc2	samice
v	v	k7c6	v
říji	říje	k1gFnSc6	říje
může	moct	k5eAaImIp3nS	moct
strávit	strávit	k5eAaPmF	strávit
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
mimo	mimo	k7c4	mimo
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Revír	revír	k1gInSc1	revír
nekastrovaného	kastrovaný	k2eNgMnSc2d1	nekastrovaný
kocoura	kocour	k1gMnSc2	kocour
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
třikrát	třikrát	k6eAd1	třikrát
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
teritorium	teritorium	k1gNnSc4	teritorium
nekastrované	kastrovaný	k2eNgFnSc2d1	nekastrovaná
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
nesnášejí	snášet	k5eNaImIp3nP	snášet
změnu	změna	k1gFnSc4	změna
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
nerady	nerad	k2eAgFnPc1d1	nerada
cestují	cestovat	k5eAaImIp3nP	cestovat
<g/>
,	,	kIx,	,
při	při	k7c6	při
hrách	hra	k1gFnPc6	hra
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
agresivní	agresivní	k2eAgMnPc1d1	agresivní
jako	jako	k9	jako
kocouři	kocour	k1gMnPc1	kocour
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kastraci	kastrace	k1gFnSc6	kastrace
se	se	k3xPyFc4	se
však	však	k9	však
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
povahových	povahový	k2eAgInPc2d1	povahový
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
kocourem	kocour	k1gInSc7	kocour
a	a	k8xC	a
kočkou	kočka	k1gFnSc7	kočka
ztratí	ztratit	k5eAaPmIp3nS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Kastrace	kastrace	k1gFnSc1	kastrace
koček	kočka	k1gFnPc2	kočka
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
u	u	k7c2	u
koček	kočka	k1gFnPc2	kočka
chovaných	chovaný	k2eAgFnPc2d1	chovaná
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
eliminovat	eliminovat	k5eAaBmF	eliminovat
říji	říje	k1gFnSc4	říje
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
projevy	projev	k1gInPc1	projev
(	(	kIx(	(
<g/>
rozstřikování	rozstřikování	k1gNnSc1	rozstřikování
moči	moč	k1gFnSc2	moč
<g/>
,	,	kIx,	,
hlasové	hlasový	k2eAgInPc1d1	hlasový
projevy	projev	k1gInPc1	projev
<g/>
)	)	kIx)	)
a	a	k8xC	a
jak	jak	k6eAd1	jak
u	u	k7c2	u
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgFnPc2d1	žijící
koček	kočka	k1gFnPc2	kočka
nebo	nebo	k8xC	nebo
koček	kočka	k1gFnPc2	kočka
s	s	k7c7	s
výběhem	výběh	k1gInSc7	výběh
zamezit	zamezit	k5eAaPmF	zamezit
produkci	produkce	k1gFnSc4	produkce
nežádoucího	žádoucí	k2eNgNnSc2d1	nežádoucí
potomstva	potomstvo	k1gNnSc2	potomstvo
<g/>
.	.	kIx.	.
</s>
<s>
Kastrované	kastrovaný	k2eAgFnPc1d1	kastrovaná
kočky	kočka	k1gFnPc1	kočka
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
délku	délka	k1gFnSc4	délka
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
přítulnější	přítulný	k2eAgFnPc1d2	přítulnější
<g/>
,	,	kIx,	,
mazlivější	mazlivý	k2eAgFnPc1d2	mazlivější
a	a	k8xC	a
klidnější	klidný	k2eAgFnPc1d2	klidnější
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
kastrovaná	kastrovaný	k2eAgFnSc1d1	kastrovaná
kočka	kočka	k1gFnSc1	kočka
ztratí	ztratit	k5eAaPmIp3nS	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
lovit	lovit	k5eAaImF	lovit
myši	myš	k1gFnPc4	myš
<g/>
,	,	kIx,	,
tloustnutí	tloustnutí	k1gNnSc4	tloustnutí
kastrátů	kastrát	k1gMnPc2	kastrát
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
než	než	k8xS	než
přímý	přímý	k2eAgInSc1d1	přímý
důsledek	důsledek	k1gInSc1	důsledek
kastrace	kastrace	k1gFnSc2	kastrace
následkem	následkem	k7c2	následkem
sníženého	snížený	k2eAgInSc2d1	snížený
energetického	energetický	k2eAgInSc2d1	energetický
výdeje	výdej	k1gInSc2	výdej
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Kastrace	kastrace	k1gFnSc1	kastrace
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
koček	kočka	k1gFnPc2	kočka
chovaných	chovaný	k2eAgFnPc6d1	chovaná
doma	doma	k6eAd1	doma
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
na	na	k7c4	na
kočky	kočka	k1gFnPc4	kočka
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgFnPc1d1	žijící
venku	venku	k6eAd1	venku
<g/>
.	.	kIx.	.
</s>
<s>
Ovariohysterektomie	Ovariohysterektomie	k1gFnSc1	Ovariohysterektomie
je	být	k5eAaImIp3nS	být
kastrace	kastrace	k1gFnSc1	kastrace
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
chirurgické	chirurgický	k2eAgNnSc4d1	chirurgické
odstranění	odstranění	k1gNnSc4	odstranění
vaječníků	vaječník	k1gInPc2	vaječník
<g/>
,	,	kIx,	,
vejcovodů	vejcovod	k1gInPc2	vejcovod
a	a	k8xC	a
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Zákrok	zákrok	k1gInSc1	zákrok
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
narkóze	narkóza	k1gFnSc6	narkóza
a	a	k8xC	a
kočka	kočka	k1gFnSc1	kočka
se	se	k3xPyFc4	se
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
zotavuje	zotavovat	k5eAaImIp3nS	zotavovat
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Kastrace	kastrace	k1gFnSc1	kastrace
kočky	kočka	k1gFnSc2	kočka
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
všech	všecek	k3xTgInPc2	všecek
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
projevů	projev	k1gInPc2	projev
řije	řij	k1gFnSc2	řij
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
koťata	kotě	k1gNnPc4	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Kastrace	kastrace	k1gFnSc1	kastrace
je	být	k5eAaImIp3nS	být
spolehlivou	spolehlivý	k2eAgFnSc7d1	spolehlivá
ochranou	ochrana	k1gFnSc7	ochrana
proti	proti	k7c3	proti
nemocem	nemoc	k1gFnPc3	nemoc
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
(	(	kIx(	(
<g/>
pyometra	pyometra	k1gFnSc1	pyometra
<g/>
,	,	kIx,	,
rakovina	rakovina	k1gFnSc1	rakovina
dělohy	děloha	k1gFnSc2	děloha
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výzkumy	výzkum	k1gInPc1	výzkum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrazně	výrazně	k6eAd1	výrazně
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
riziko	riziko	k1gNnSc1	riziko
výskytu	výskyt	k1gInSc2	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
mléčné	mléčný	k2eAgFnSc2d1	mléčná
žlázy	žláza	k1gFnSc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
alternativní	alternativní	k2eAgNnSc1d1	alternativní
řešení	řešení	k1gNnSc1	řešení
kastrace	kastrace	k1gFnSc2	kastrace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
sterilizace	sterilizace	k1gFnSc1	sterilizace
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
podvázání	podvázání	k1gNnSc6	podvázání
vaječníků	vaječník	k1gInPc2	vaječník
<g/>
;	;	kIx,	;
sterilizace	sterilizace	k1gFnSc1	sterilizace
však	však	k9	však
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
hormonální	hormonální	k2eAgFnSc4d1	hormonální
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
kočka	kočka	k1gFnSc1	kočka
stále	stále	k6eAd1	stále
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
říji	říje	k1gFnSc4	říje
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nebude	být	k5eNaImBp3nS	být
moci	moct	k5eAaImF	moct
zabřeznout	zabřeznout	k5eAaPmF	zabřeznout
<g/>
.	.	kIx.	.
</s>
<s>
Sterilizace	sterilizace	k1gFnSc1	sterilizace
také	také	k9	také
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc1	žádný
efekt	efekt	k1gInSc1	efekt
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
rakovinou	rakovina	k1gFnSc7	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
ke	k	k7c3	k
vstřebání	vstřebání	k1gNnSc3	vstřebání
podvazujícího	podvazující	k2eAgInSc2d1	podvazující
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
obnovení	obnovení	k1gNnSc4	obnovení
fertility	fertilita	k1gFnSc2	fertilita
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Orchiektomie	Orchiektomie	k1gFnSc1	Orchiektomie
je	být	k5eAaImIp3nS	být
kastrace	kastrace	k1gFnSc1	kastrace
kocourů	kocour	k1gMnPc2	kocour
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
chirurgický	chirurgický	k2eAgInSc4d1	chirurgický
zákrok	zákrok	k1gInSc4	zákrok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
odstraní	odstranit	k5eAaPmIp3nS	odstranit
varlata	varle	k1gNnPc1	varle
<g/>
,	,	kIx,	,
nadvarlata	nadvarle	k1gNnPc1	nadvarle
a	a	k8xC	a
část	část	k1gFnSc1	část
chámovodu	chámovod	k1gInSc2	chámovod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákrok	zákrok	k1gInSc1	zákrok
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
provádět	provádět	k5eAaImF	provádět
až	až	k9	až
v	v	k7c6	v
pohlavní	pohlavní	k2eAgFnSc6d1	pohlavní
dospělosti	dospělost	k1gFnSc6	dospělost
kocoura	kocour	k1gMnSc2	kocour
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc2	věk
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Brzká	brzký	k2eAgFnSc1d1	brzká
kastrace	kastrace	k1gFnSc1	kastrace
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
močových	močový	k2eAgInPc2d1	močový
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
močová	močový	k2eAgFnSc1d1	močová
trubice	trubice	k1gFnSc1	trubice
neměla	mít	k5eNaImAgFnS	mít
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
pořádně	pořádně	k6eAd1	pořádně
vyvinout	vyvinout	k5eAaPmF	vyvinout
a	a	k8xC	a
rozšířit	rozšířit	k5eAaPmF	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nevyvinutí	nevyvinutí	k1gNnSc3	nevyvinutí
sekundárních	sekundární	k2eAgInPc2d1	sekundární
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
kočku	kočka	k1gFnSc4	kočka
od	od	k7c2	od
kocoura	kocour	k1gMnSc2	kocour
(	(	kIx(	(
<g/>
větší	veliký	k2eAgInSc1d2	veliký
vzrůst	vzrůst	k1gInSc1	vzrůst
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kocouří	kocouří	k2eAgFnSc1d1	kocouří
hlava	hlava	k1gFnSc1	hlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
relativní	relativní	k2eAgFnSc3d1	relativní
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
zákroku	zákrok	k1gInSc2	zákrok
je	být	k5eAaImIp3nS	být
kocour	kocour	k1gInSc1	kocour
do	do	k7c2	do
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
kastraci	kastrace	k1gFnSc6	kastrace
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
kondici	kondice	k1gFnSc6	kondice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
kastraci	kastrace	k1gFnSc6	kastrace
je	být	k5eAaImIp3nS	být
však	však	k9	však
ještě	ještě	k6eAd1	ještě
schopen	schopen	k2eAgMnSc1d1	schopen
zplodit	zplodit	k5eAaPmF	zplodit
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
kastraci	kastrace	k1gFnSc6	kastrace
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
všech	všecek	k3xTgFnPc2	všecek
spermií	spermie	k1gFnPc2	spermie
z	z	k7c2	z
vývodných	vývodný	k2eAgFnPc2d1	vývodná
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Kastrace	kastrace	k1gFnSc1	kastrace
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
projevy	projev	k1gInPc4	projev
říje	říje	k1gFnSc2	říje
jako	jako	k8xC	jako
toulání	toulání	k1gNnSc2	toulání
<g/>
,	,	kIx,	,
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
koček	kočka	k1gFnPc2	kočka
v	v	k7c6	v
říji	říje	k1gFnSc6	říje
<g/>
,	,	kIx,	,
značkování	značkování	k1gNnSc4	značkování
teritoria	teritorium	k1gNnSc2	teritorium
a	a	k8xC	a
upravuje	upravovat	k5eAaImIp3nS	upravovat
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
zápach	zápach	k1gInSc4	zápach
moči	moč	k1gFnSc2	moč
<g/>
.	.	kIx.	.
</s>
<s>
Kastráti	kastrát	k1gMnPc1	kastrát
jsou	být	k5eAaImIp3nP	být
klidnější	klidný	k2eAgMnSc1d2	klidnější
a	a	k8xC	a
raději	rád	k6eAd2	rád
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
potřebu	potřeba	k1gFnSc4	potřeba
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
teritorium	teritorium	k1gNnSc4	teritorium
či	či	k8xC	či
kočky	kočka	k1gFnPc1	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Plemena	plemeno	k1gNnSc2	plemeno
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
šlechtění	šlechtění	k1gNnSc3	šlechtění
(	(	kIx(	(
<g/>
plemenitbě	plemenitba	k1gFnSc3	plemenitba
<g/>
,	,	kIx,	,
chovu	chov	k1gInSc3	chov
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
plemen	plemeno	k1gNnPc2	plemeno
kočky	kočka	k1gFnSc2	kočka
domácí	domácí	k2eAgFnSc2d1	domácí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
liší	lišit	k5eAaImIp3nS	lišit
nejenom	nejenom	k6eAd1	nejenom
barvou	barva	k1gFnSc7	barva
srsti	srst	k1gFnSc2	srst
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
kvalitou	kvalita	k1gFnSc7	kvalita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
barvou	barva	k1gFnSc7	barva
očí	oko	k1gNnPc2	oko
i	i	k8xC	i
povahovými	povahový	k2eAgFnPc7d1	povahová
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vznikají	vznikat	k5eAaImIp3nP	vznikat
nová	nový	k2eAgNnPc4d1	nové
plemena	plemeno	k1gNnPc4	plemeno
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Šlechtění	šlechtění	k1gNnSc1	šlechtění
koček	kočka	k1gFnPc2	kočka
si	se	k3xPyFc3	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
daném	daný	k2eAgNnSc6d1	dané
plemeni	plemeno	k1gNnSc6	plemeno
(	(	kIx(	(
<g/>
znalost	znalost	k1gFnSc1	znalost
uznaného	uznaný	k2eAgInSc2d1	uznaný
standardu	standard	k1gInSc2	standard
vyhlášeného	vyhlášený	k2eAgMnSc4d1	vyhlášený
FIFe	FIF	k1gMnSc4	FIF
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jiných	jiný	k2eAgFnPc2d1	jiná
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
dané	daný	k2eAgNnSc4d1	dané
plemeno	plemeno	k1gNnSc4	plemeno
uznávají	uznávat	k5eAaImIp3nP	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
být	být	k5eAaImF	být
členem	člen	k1gInSc7	člen
nějakého	nějaký	k3yIgInSc2	nějaký
klubu	klub	k1gInSc2	klub
chovatelů	chovatel	k1gMnPc2	chovatel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svými	svůj	k3xOyFgInPc7	svůj
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
předpisy	předpis	k1gInPc7	předpis
usměrňuje	usměrňovat	k5eAaImIp3nS	usměrňovat
pravidla	pravidlo	k1gNnPc4	pravidlo
chovu	chov	k1gInSc6	chov
koček	kočka	k1gFnPc2	kočka
s	s	k7c7	s
průkazem	průkaz	k1gInSc7	průkaz
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
chovu	chov	k1gInSc2	chov
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
připuštěni	připuštěn	k2eAgMnPc1d1	připuštěn
pouze	pouze	k6eAd1	pouze
jedinci	jedinec	k1gMnPc1	jedinec
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
platnému	platný	k2eAgInSc3d1	platný
standardu	standard	k1gInSc3	standard
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jedinec	jedinec	k1gMnSc1	jedinec
standardu	standard	k1gInSc2	standard
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
určí	určit	k5eAaPmIp3nS	určit
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
posuzovatel	posuzovatel	k1gMnSc1	posuzovatel
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
základní	základní	k2eAgFnSc1d1	základní
organizace	organizace	k1gFnSc1	organizace
chovatelů	chovatel	k1gMnPc2	chovatel
koček	kočka	k1gFnPc2	kočka
stanoví	stanovit	k5eAaPmIp3nS	stanovit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
předpisech	předpis	k1gInPc6	předpis
chovatelům	chovatel	k1gMnPc3	chovatel
určitá	určitý	k2eAgNnPc4d1	určité
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
,	,	kIx,	,
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
zásady	zásada	k1gFnPc4	zásada
plemenitby	plemenitba	k1gFnSc2	plemenitba
daného	daný	k2eAgNnSc2d1	dané
plemene	plemeno	k1gNnSc2	plemeno
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
chovatel	chovatel	k1gMnSc1	chovatel
musí	muset	k5eAaImIp3nS	muset
dodržovat	dodržovat	k5eAaImF	dodržovat
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
určuje	určovat	k5eAaImIp3nS	určovat
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
splnění	splnění	k1gNnSc6	splnění
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jedince	jedinko	k6eAd1	jedinko
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
chovu	chov	k1gInSc2	chov
(	(	kIx(	(
<g/>
určuje	určovat	k5eAaImIp3nS	určovat
kolik	kolik	k4yIc1	kolik
a	a	k8xC	a
jakých	jaký	k3yIgFnPc2	jaký
výstav	výstava	k1gFnPc2	výstava
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
jakým	jaký	k3yRgNnSc7	jaký
hodnocením	hodnocení	k1gNnSc7	hodnocení
musí	muset	k5eAaImIp3nS	muset
jedinec	jedinec	k1gMnSc1	jedinec
absolvovat	absolvovat	k5eAaPmF	absolvovat
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gInSc4	jeho
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
a	a	k8xC	a
věk	věk	k1gInSc4	věk
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
těchto	tento	k3xDgInPc2	tento
předpisů	předpis	k1gInPc2	předpis
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
poradce	poradce	k1gMnSc1	poradce
chovu	chov	k1gInSc2	chov
(	(	kIx(	(
<g/>
zkušený	zkušený	k2eAgMnSc1d1	zkušený
chovatel	chovatel	k1gMnSc1	chovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
vystavovat	vystavovat	k5eAaImF	vystavovat
příslušné	příslušný	k2eAgInPc4d1	příslušný
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
chovatelům	chovatel	k1gMnPc3	chovatel
a	a	k8xC	a
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
činnost	činnost	k1gFnSc4	činnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
takzvaném	takzvaný	k2eAgInSc6d1	takzvaný
týrání	týrání	k1gNnSc2	týrání
chovem	chov	k1gInSc7	chov
-	-	kIx~	-
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
přešlechťují	přešlechťovat	k5eAaImIp3nP	přešlechťovat
některé	některý	k3yIgInPc4	některý
fyziologické	fyziologický	k2eAgInPc4d1	fyziologický
znaky	znak	k1gInPc4	znak
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvířatům	zvíře	k1gNnPc3	zvíře
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
normální	normální	k2eAgInSc4d1	normální
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
perské	perský	k2eAgFnPc4d1	perská
kočky	kočka	k1gFnPc4	kočka
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
exotické	exotický	k2eAgFnPc1d1	exotická
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
deformovaného	deformovaný	k2eAgInSc2d1	deformovaný
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
tlamy	tlama	k1gFnSc2	tlama
dýchací	dýchací	k2eAgFnSc2d1	dýchací
potíže	potíž	k1gFnSc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
týraní	týraný	k2eAgMnPc1d1	týraný
není	být	k5eNaImIp3nS	být
doposud	doposud	k6eAd1	doposud
právně	právně	k6eAd1	právně
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
chovatelských	chovatelský	k2eAgFnPc6d1	chovatelská
organizacích	organizace	k1gFnPc6	organizace
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
problému	problém	k1gInSc3	problém
postaví	postavit	k5eAaPmIp3nP	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Vzteklina	vzteklina	k1gFnSc1	vzteklina
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
nakazit	nakazit	k5eAaPmF	nakazit
všechna	všechen	k3xTgNnPc4	všechen
teplokrevná	teplokrevný	k2eAgNnPc4d1	teplokrevné
zvířata	zvíře	k1gNnPc4	zvíře
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgNnSc1d1	každoroční
očkování	očkování	k1gNnSc1	očkování
<g/>
.	.	kIx.	.
</s>
<s>
Borelióza	borelióza	k1gFnSc1	borelióza
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
nakazit	nakazit	k5eAaPmF	nakazit
všichni	všechen	k3xTgMnPc1	všechen
savci	savec	k1gMnPc1	savec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
klíšťaty	klíště	k1gNnPc7	klíště
a	a	k8xC	a
komáry	komár	k1gMnPc7	komár
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgNnSc1d1	každoroční
očkování	očkování	k1gNnSc1	očkování
a	a	k8xC	a
používání	používání	k1gNnSc1	používání
repelentů	repelent	k1gInPc2	repelent
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zabrání	zabránit	k5eAaPmIp3nP	zabránit
přisátí	přisátí	k1gNnSc4	přisátí
klíštěte	klíště	k1gNnSc2	klíště
<g/>
.	.	kIx.	.
</s>
<s>
Panleukopénie	Panleukopénie	k1gFnSc1	Panleukopénie
-	-	kIx~	-
postihuje	postihovat	k5eAaImIp3nS	postihovat
pouze	pouze	k6eAd1	pouze
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
malá	malý	k2eAgNnPc4d1	malé
koťata	kotě	k1gNnPc4	kotě
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
očkování	očkování	k1gNnSc1	očkování
<g/>
.	.	kIx.	.
</s>
<s>
Virová	virový	k2eAgFnSc1d1	virová
rhinotracheitis	rhinotracheitis	k1gFnSc1	rhinotracheitis
-	-	kIx~	-
postihuje	postihovat	k5eAaImIp3nS	postihovat
pouze	pouze	k6eAd1	pouze
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
očkování	očkování	k1gNnSc1	očkování
<g/>
.	.	kIx.	.
</s>
<s>
Kaliciviróza	Kaliciviróza	k1gFnSc1	Kaliciviróza
-	-	kIx~	-
postihuje	postihovat	k5eAaImIp3nS	postihovat
pouze	pouze	k6eAd1	pouze
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
očkování	očkování	k1gNnSc1	očkování
<g/>
.	.	kIx.	.
</s>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
peritonitída	peritonitída	k1gFnSc1	peritonitída
(	(	kIx(	(
<g/>
Feline	Felin	k1gInSc5	Felin
Infectious	Infectious	k1gMnSc1	Infectious
Peritonitis	peritonitis	k1gFnSc1	peritonitis
<g/>
,	,	kIx,	,
FIP	FIP	kA	FIP
<g/>
)	)	kIx)	)
-	-	kIx~	-
postihuje	postihovat	k5eAaImIp3nS	postihovat
pouze	pouze	k6eAd1	pouze
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
vakcinace	vakcinace	k1gFnSc1	vakcinace
(	(	kIx(	(
<g/>
vakcinace	vakcinace	k1gFnSc1	vakcinace
není	být	k5eNaImIp3nS	být
nikdy	nikdy	k6eAd1	nikdy
100	[number]	k4	100
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
provést	provést	k5eAaPmF	provést
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
koček	kočka	k1gFnPc2	kočka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nepřišly	přijít	k5eNaPmAgFnP	přijít
nikdy	nikdy	k6eAd1	nikdy
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
koronaviry	koronavir	k1gMnPc7	koronavir
a	a	k8xC	a
takových	takový	k3xDgFnPc2	takový
koček	kočka	k1gFnPc2	kočka
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Koronaviry	Koronavira	k1gFnPc1	Koronavira
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
přítomny	přítomen	k2eAgInPc1d1	přítomen
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
kočky	kočka	k1gFnSc2	kočka
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
koček	kočka	k1gFnPc2	kočka
nikdy	nikdy	k6eAd1	nikdy
FIP	FIP	kA	FIP
neonemocní	onemocnět	k5eNaPmIp3nS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
oslabena	oslaben	k2eAgFnSc1d1	oslabena
-	-	kIx~	-
genetika	genetika	k1gFnSc1	genetika
<g/>
,	,	kIx,	,
stres	stres	k1gInSc1	stres
<g/>
,	,	kIx,	,
infekce	infekce	k1gFnSc1	infekce
-	-	kIx~	-
viry	vir	k1gInPc1	vir
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
se	se	k3xPyFc4	se
přemnoží	přemnožit	k5eAaPmIp3nP	přemnožit
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
zmutovat	zmutovat	k5eAaPmF	zmutovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
neexistuje	existovat	k5eNaImIp3nS	existovat
test	test	k1gInSc1	test
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
kočka	kočka	k1gFnSc1	kočka
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
koronaviry	koronavir	k1gInPc4	koronavir
"	"	kIx"	"
<g/>
normální	normální	k2eAgInSc4d1	normální
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
ty	ten	k3xDgFnPc4	ten
zmutované	zmutovaný	k2eAgFnPc4d1	zmutovaná
<g/>
,	,	kIx,	,
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
nemusí	muset	k5eNaImIp3nS	muset
znamenat	znamenat	k5eAaImF	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kočka	kočka	k1gFnSc1	kočka
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
FIP	FIP	kA	FIP
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
očkování	očkování	k1gNnSc2	očkování
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
negativních	negativní	k2eAgInPc6d1	negativní
účincích	účinek	k1gInPc6	účinek
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
chovatelů	chovatel	k1gMnPc2	chovatel
ho	on	k3xPp3gMnSc4	on
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
.	.	kIx.	.
</s>
<s>
FIV	FIV	kA	FIV
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kočičí	kočičí	k2eAgMnSc1d1	kočičí
AIDS	AIDS	kA	AIDS
<g/>
"	"	kIx"	"
-	-	kIx~	-
postihuje	postihovat	k5eAaImIp3nS	postihovat
pouze	pouze	k6eAd1	pouze
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
kočičí	kočičí	k2eAgFnSc1d1	kočičí
leukóza	leukóza	k1gFnSc1	leukóza
<g/>
,	,	kIx,	,
FeLV	FeLV	k1gFnSc1	FeLV
-	-	kIx~	-
postihuje	postihovat	k5eAaImIp3nS	postihovat
pouze	pouze	k6eAd1	pouze
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
končí	končit	k5eAaImIp3nS	končit
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
vakcinace	vakcinace	k1gFnSc1	vakcinace
<g/>
.	.	kIx.	.
</s>
<s>
Campylobakterióza	Campylobakterióza	k1gFnSc1	Campylobakterióza
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
nakazit	nakazit	k5eAaPmF	nakazit
všechna	všechen	k3xTgNnPc4	všechen
teplokrevná	teplokrevný	k2eAgNnPc4d1	teplokrevné
zvířata	zvíře	k1gNnPc4	zvíře
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nS	nakazit
z	z	k7c2	z
infekčního	infekční	k2eAgInSc2d1	infekční
trusu	trus	k1gInSc2	trus
<g/>
,	,	kIx,	,
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	s	k7c7	s
žaludečními	žaludeční	k2eAgInPc7d1	žaludeční
a	a	k8xC	a
střevními	střevní	k2eAgInPc7d1	střevní
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nP	nakazit
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Salmonelóza	salmonelóza	k1gFnSc1	salmonelóza
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
nakazit	nakazit	k5eAaPmF	nakazit
všechna	všechen	k3xTgNnPc4	všechen
teplokrevná	teplokrevný	k2eAgNnPc4d1	teplokrevné
zvířata	zvíře	k1gNnPc4	zvíře
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
i	i	k9	i
plazi	plaz	k1gMnPc1	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
-	-	kIx~	-
kočka	kočka	k1gFnSc1	kočka
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nS	nakazit
od	od	k7c2	od
nemocného	mocný	k2eNgMnSc2d1	nemocný
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dermatofytóza	Dermatofytóza	k1gFnSc1	Dermatofytóza
-	-	kIx~	-
kožní	kožní	k2eAgFnSc1d1	kožní
plíseň	plíseň	k1gFnSc1	plíseň
-	-	kIx~	-
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
nakazit	nakazit	k5eAaPmF	nakazit
všichni	všechen	k3xTgMnPc1	všechen
savci	savec	k1gMnPc1	savec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
k	k	k7c3	k
nákaze	nákaza	k1gFnSc3	nákaza
dochází	docházet	k5eAaImIp3nS	docházet
přímým	přímý	k2eAgInSc7d1	přímý
kontaktem	kontakt	k1gInSc7	kontakt
nebo	nebo	k8xC	nebo
pobytem	pobyt	k1gInSc7	pobyt
v	v	k7c6	v
kontaminovaném	kontaminovaný	k2eAgNnSc6d1	kontaminované
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
i	i	k9	i
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
kožními	kožní	k2eAgFnPc7d1	kožní
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
očkování	očkování	k1gNnSc1	očkování
a	a	k8xC	a
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Giardióza	Giardióza	k1gFnSc1	Giardióza
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
Giardia	Giardium	k1gNnPc4	Giardium
intestinalis	intestinalis	k1gFnSc2	intestinalis
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
průjmové	průjmový	k2eAgNnSc4d1	průjmové
onemocnění	onemocnění	k1gNnSc4	onemocnění
mladších	mladý	k2eAgFnPc2d2	mladší
koček	kočka	k1gFnPc2	kočka
nebo	nebo	k8xC	nebo
oslabených	oslabený	k2eAgMnPc2d1	oslabený
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Toxoplazmóza	toxoplazmóza	k1gFnSc1	toxoplazmóza
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
prvok	prvok	k1gMnSc1	prvok
Toxoplasma	Toxoplasmum	k1gNnSc2	Toxoplasmum
gondii	gondie	k1gFnSc3	gondie
<g/>
.	.	kIx.	.
</s>
<s>
Kočky	kočka	k1gFnPc1	kočka
jsou	být	k5eAaImIp3nP	být
definitivním	definitivní	k2eAgInSc7d1	definitivní
hostitelem	hostitel	k1gMnSc7	hostitel
<g/>
,	,	kIx,	,
mezihostitelem	mezihostitel	k1gMnSc7	mezihostitel
jsou	být	k5eAaImIp3nP	být
ostatní	ostatní	k2eAgMnPc1d1	ostatní
savci	savec	k1gMnPc1	savec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Trichomoniáza	Trichomoniáza	k1gFnSc1	Trichomoniáza
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
Tetratrichomonas	Tetratrichomonas	k1gMnSc1	Tetratrichomonas
felistomae	felistoma	k1gFnSc2	felistoma
<g/>
.	.	kIx.	.
</s>
<s>
Postihuje	postihovat	k5eAaImIp3nS	postihovat
nejčastěji	často	k6eAd3	často
ústní	ústní	k2eAgFnSc4d1	ústní
dutinu	dutina	k1gFnSc4	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Teilerióza	Teilerióza	k1gFnSc1	Teilerióza
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
Theileria	Theilerium	k1gNnPc4	Theilerium
felis	felis	k1gFnSc2	felis
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
koček	kočka	k1gFnPc2	kočka
a	a	k8xC	a
kočkovitých	kočkovití	k1gMnPc2	kočkovití
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
prvok	prvok	k1gMnSc1	prvok
napadá	napadat	k5eAaPmIp3nS	napadat
lymfocyty	lymfocyt	k1gInPc4	lymfocyt
a	a	k8xC	a
červené	červený	k2eAgFnPc4d1	červená
krvinky	krvinka	k1gFnPc4	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Izosporóza	Izosporóza	k1gFnSc1	Izosporóza
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
jsou	být	k5eAaImIp3nP	být
kokcidie	kokcidie	k1gFnSc1	kokcidie
Isospora	Isospora	k1gFnSc1	Isospora
felis	felis	k1gFnSc1	felis
a	a	k8xC	a
I.	I.	kA	I.
rivolta	rivolta	k1gFnSc1	rivolta
<g/>
.	.	kIx.	.
</s>
<s>
Střevní	střevní	k2eAgNnSc1d1	střevní
onemocnění	onemocnění	k1gNnSc1	onemocnění
postihující	postihující	k2eAgNnSc1d1	postihující
zejména	zejména	k9	zejména
koťata	kotě	k1gNnPc1	kotě
do	do	k7c2	do
stáří	stáří	k1gNnSc2	stáří
4	[number]	k4	4
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
motolice	motolice	k1gFnSc1	motolice
Paragonimus	Paragonimus	k1gInSc4	Paragonimus
westermani	westerman	k1gMnPc1	westerman
(	(	kIx(	(
<g/>
motolice	motolice	k1gFnSc1	motolice
plicní	plicní	k2eAgFnSc1d1	plicní
<g/>
)	)	kIx)	)
-	-	kIx~	-
motolice	motolice	k1gFnSc1	motolice
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nS	nakazit
pozřením	pozření	k1gNnSc7	pozření
sladkovodních	sladkovodní	k2eAgMnPc2d1	sladkovodní
raků	rak	k1gMnPc2	rak
a	a	k8xC	a
krabů	krab	k1gMnPc2	krab
<g/>
,	,	kIx,	,
motolice	motolice	k1gFnPc1	motolice
se	se	k3xPyFc4	se
lokalizují	lokalizovat	k5eAaBmIp3nP	lokalizovat
v	v	k7c6	v
plicích	plíce	k1gFnPc6	plíce
a	a	k8xC	a
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
zánět	zánět	k1gInSc4	zánět
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Opistorchis	Opistorchis	k1gInSc1	Opistorchis
felineus	felineus	k1gInSc1	felineus
a	a	k8xC	a
O.	O.	kA	O.
viverrini	viverrin	k1gMnPc1	viverrin
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
motolice	motolice	k1gFnPc1	motolice
postihující	postihující	k2eAgFnPc1d1	postihující
játra	játra	k1gNnPc1	játra
a	a	k8xC	a
žlučovody	žlučovod	k1gInPc1	žlučovod
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Kočka	kočka	k1gFnSc1	kočka
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nS	nakazit
pozřením	pozření	k1gNnSc7	pozření
masa	maso	k1gNnSc2	maso
kaprovitých	kaprovitý	k2eAgFnPc2d1	kaprovitá
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
tasemnice	tasemnice	k1gFnSc1	tasemnice
Mesocestoides	Mesocestoides	k1gInSc4	Mesocestoides
lineatus	lineatus	k1gInSc1	lineatus
Dipylidium	Dipylidium	k1gNnSc1	Dipylidium
caninum	caninum	k1gInSc1	caninum
(	(	kIx(	(
<g/>
tasemnice	tasemnice	k1gFnSc1	tasemnice
psí	psí	k2eAgFnSc1d1	psí
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
tasemnice	tasemnice	k1gFnSc1	tasemnice
u	u	k7c2	u
masožravců	masožravec	k1gMnPc2	masožravec
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Echinococcus	Echinococcus	k1gInSc1	Echinococcus
multilocularis	multilocularis	k1gFnSc1	multilocularis
-	-	kIx~	-
definitivním	definitivní	k2eAgMnSc7d1	definitivní
hostitelem	hostitel	k1gMnSc7	hostitel
je	být	k5eAaImIp3nS	být
kočka	kočka	k1gFnSc1	kočka
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
Tasemnice	tasemnice	k1gFnSc1	tasemnice
je	být	k5eAaImIp3nS	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
hlístice	hlístice	k1gFnSc1	hlístice
Toxocara	Toxocar	k1gMnSc2	Toxocar
cati	cat	k1gFnSc2	cat
(	(	kIx(	(
<g/>
škrkavka	škrkavka	k1gFnSc1	škrkavka
kočičí	kočičí	k2eAgFnSc1d1	kočičí
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
střevní	střevní	k2eAgFnSc1d1	střevní
hlístice	hlístice	k1gFnSc1	hlístice
u	u	k7c2	u
koček	kočka	k1gFnPc2	kočka
Toxascaris	Toxascaris	k1gFnSc1	Toxascaris
leonina	leonina	k1gFnSc1	leonina
(	(	kIx(	(
<g/>
škrkavka	škrkavka	k1gFnSc1	škrkavka
šelmí	šelmí	k2eAgFnSc1d1	šelmí
<g/>
)	)	kIx)	)
Ancylostoma	Ancylostoma	k1gFnSc1	Ancylostoma
tubaeforme	tubaeform	k1gInSc5	tubaeform
Aelurostrongylus	Aelurostrongylus	k1gMnSc1	Aelurostrongylus
abstrusus	abstrusus	k1gInSc1	abstrusus
-	-	kIx~	-
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
plicní	plicní	k2eAgFnSc1d1	plicní
červivost	červivost	k1gFnSc1	červivost
koček	kočka	k1gFnPc2	kočka
Trichuris	Trichuris	k1gFnSc1	Trichuris
vulpis	vulpis	k1gFnSc1	vulpis
(	(	kIx(	(
<g/>
tenkohlavec	tenkohlavec	k1gMnSc1	tenkohlavec
liščí	liščit	k5eAaPmIp3nS	liščit
<g/>
)	)	kIx)	)
-	-	kIx~	-
červi	červ	k1gMnPc1	červ
způsobující	způsobující	k2eAgInSc4d1	způsobující
záněty	zánět	k1gInPc1	zánět
slepého	slepý	k2eAgNnSc2d1	slepé
a	a	k8xC	a
tlustého	tlustý	k2eAgNnSc2d1	tlusté
střeva	střevo	k1gNnSc2	střevo
Capillaria	Capillarium	k1gNnSc2	Capillarium
aerophila	aerophila	k1gFnSc1	aerophila
-	-	kIx~	-
napadá	napadat	k5eAaPmIp3nS	napadat
plíce	plíce	k1gFnPc4	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Zaklíštění	zaklíštění	k1gNnSc1	zaklíštění
-	-	kIx~	-
kočky	kočka	k1gFnPc1	kočka
napadají	napadat	k5eAaImIp3nP	napadat
klíšťata	klíště	k1gNnPc1	klíště
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nejčastěji	často	k6eAd3	často
Ixodes	Ixodes	k1gMnSc1	Ixodes
ricinus	ricinus	k1gMnSc1	ricinus
<g/>
,	,	kIx,	,
význam	význam	k1gInSc1	význam
tkví	tkvět	k5eAaImIp3nS	tkvět
především	především	k9	především
v	v	k7c6	v
přenosu	přenos	k1gInSc6	přenos
infekčních	infekční	k2eAgNnPc2d1	infekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
(	(	kIx(	(
<g/>
tularémie	tularémie	k1gFnSc1	tularémie
<g/>
,	,	kIx,	,
borelióza	borelióza	k1gFnSc1	borelióza
<g/>
,	,	kIx,	,
babesióza	babesióza	k1gFnSc1	babesióza
<g/>
)	)	kIx)	)
Ušní	ušní	k2eAgInSc1d1	ušní
svrab	svrab	k1gInSc1	svrab
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
Otodectes	Otodectes	k1gInSc4	Otodectes
cynotis	cynotis	k1gFnSc2	cynotis
Notoedrový	Notoedrový	k2eAgInSc4d1	Notoedrový
svrab	svrab	k1gInSc4	svrab
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
zákožka	zákožka	k1gFnSc1	zákožka
Notoedres	Notoedres	k1gInSc4	Notoedres
cati	cat	k1gFnSc2	cat
Cheyletielóza	Cheyletielóza	k1gFnSc1	Cheyletielóza
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
Cheyletiella	Cheyletiella	k1gMnSc1	Cheyletiella
yasguri	yasgur	k1gFnSc2	yasgur
Trombikulóza	Trombikulóza	k1gFnSc1	Trombikulóza
-	-	kIx~	-
původcem	původce	k1gMnSc7	původce
je	být	k5eAaImIp3nS	být
sametka	sametka	k1gFnSc1	sametka
podzimní	podzimní	k2eAgFnSc1d1	podzimní
(	(	kIx(	(
<g/>
Neotrombicula	Neotrombicula	k1gFnSc1	Neotrombicula
autumnalis	autumnalis	k1gFnSc2	autumnalis
<g/>
)	)	kIx)	)
Všenky	všenka	k1gFnSc2	všenka
-	-	kIx~	-
Felicola	Felicola	k1gFnSc1	Felicola
subrostratus	subrostratus	k1gInSc4	subrostratus
Blechy	blecha	k1gFnSc2	blecha
-	-	kIx~	-
napadají	napadat	k5eAaBmIp3nP	napadat
všechny	všechen	k3xTgMnPc4	všechen
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
hygiena	hygiena	k1gFnSc1	hygiena
a	a	k8xC	a
odblešování	odblešování	k1gNnSc1	odblešování
zablešených	zablešený	k2eAgFnPc2d1	zablešená
koček	kočka	k1gFnPc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Cat	Cat	k1gMnSc1	Cat
Congress	Congress	k1gInSc1	Congress
/	/	kIx~	/
WCC	WCC	kA	WCC
-	-	kIx~	-
web	web	k1gInSc1	web
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
chovatelskou	chovatelský	k2eAgFnSc7d1	chovatelská
organizací	organizace	k1gFnSc7	organizace
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
(	(	kIx(	(
<g/>
neregistruje	registrovat	k5eNaBmIp3nS	registrovat
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
nepořádá	pořádat	k5eNaImIp3nS	pořádat
výstavy	výstava	k1gFnPc4	výstava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konfederací	konfederace	k1gFnSc7	konfederace
největších	veliký	k2eAgFnPc2d3	veliký
chovatelských	chovatelský	k2eAgFnPc2d1	chovatelská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
podpora	podpora	k1gFnSc1	podpora
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
koordinace	koordinace	k1gFnSc2	koordinace
společných	společný	k2eAgFnPc2d1	společná
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gMnSc5	Internationale
Féline	Félin	k1gMnSc5	Félin
(	(	kIx(	(
<g/>
FIFe	FIF	k1gInPc4	FIF
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
Cat	Cat	k1gMnSc1	Cat
Fanciers	Fanciers	k1gInSc1	Fanciers
<g/>
'	'	kIx"	'
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
CFA	CFA	kA	CFA
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
The	The	k1gMnSc2	The
International	International	k1gMnSc2	International
Cat	Cat	k1gFnSc1	Cat
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
TICA	TICA	kA	TICA
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
World	Worlda	k1gFnPc2	Worlda
Cat	Cat	k1gFnPc2	Cat
Federation	Federation	k1gInSc1	Federation
(	(	kIx(	(
<g/>
WCF	WCF	kA	WCF
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
The	The	k1gFnPc2	The
Governing	Governing	k1gInSc1	Governing
Council	Council	k1gMnSc1	Council
of	of	k?	of
the	the	k?	the
Cat	Cat	k1gMnSc1	Cat
Fancy	Fanca	k1gFnSc2	Fanca
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
GCCF	GCCF	kA	GCCF
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
The	The	k1gMnSc1	The
Australian	Australian	k1gMnSc1	Australian
Cat	Cat	k1gMnSc1	Cat
Federation	Federation	k1gInSc4	Federation
Inc	Inc	k1gFnSc2	Inc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ACF	ACF	kA	ACF
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
New	New	k1gFnPc2	New
Zealand	Zealanda	k1gFnPc2	Zealanda
Cat	Cat	k1gMnSc2	Cat
Fancy	Fanca	k1gMnSc2	Fanca
(	(	kIx(	(
<g/>
NCF	NCF	kA	NCF
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
The	The	k1gMnSc1	The
Southern	Southern	k1gMnSc1	Southern
Africa	Africa	k1gMnSc1	Africa
Cat	Cat	k1gMnSc1	Cat
Council	Council	k1gMnSc1	Council
(	(	kIx(	(
<g/>
SACC	SACC	kA	SACC
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
American	Americany	k1gInPc2	Americany
Cat	Cat	k1gFnSc1	Cat
Fanciers	Fanciers	k1gInSc1	Fanciers
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
ACFA	ACFA	kA	ACFA
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
The	The	k1gFnPc2	The
Canadian	Canadian	k1gInSc1	Canadian
Cat	Cat	k1gMnSc1	Cat
Association	Association	k1gInSc1	Association
<g/>
/	/	kIx~	/
<g/>
Association	Association	k1gInSc1	Association
Feline	Felin	k1gMnSc5	Felin
Canadienne	Canadienn	k1gMnSc5	Canadienn
(	(	kIx(	(
<g/>
CCA	cca	kA	cca
<g/>
/	/	kIx~	/
<g/>
AFC	AFC	kA	AFC
<g/>
)	)	kIx)	)
-	-	kIx~	-
web	web	k1gInSc1	web
</s>
