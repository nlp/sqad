<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Josef	Josef	k1gMnSc1	Josef
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Burian	Burian	k1gMnSc1	Burian
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1891	[number]	k4	1891
Liberec	Liberec	k1gInSc1	Liberec
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1962	[number]	k4	1962
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Král	Král	k1gMnSc1	Král
komiků	komik	k1gMnPc2	komik
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
mim	mim	k1gMnSc1	mim
a	a	k8xC	a
imitátor	imitátor	k1gMnSc1	imitátor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nespoutané	spoutaný	k2eNgFnSc3d1	nespoutaná
živelnosti	živelnost	k1gFnSc3	živelnost
a	a	k8xC	a
potřebě	potřeba	k1gFnSc3	potřeba
být	být	k5eAaImF	být
všude	všude	k6eAd1	všude
první	první	k4xOgInSc4	první
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
mezi	mezi	k7c4	mezi
skutečné	skutečný	k2eAgFnPc4d1	skutečná
hvězdy	hvězda	k1gFnPc4	hvězda
českého	český	k2eAgInSc2d1	český
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
i	i	k8xC	i
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
herců	herec	k1gMnPc2	herec
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
však	však	k9	však
neprávem	neprávo	k1gNnSc7	neprávo
nařčen	nařknout	k5eAaPmNgMnS	nařknout
z	z	k7c2	z
kolaborace	kolaborace	k1gFnSc2	kolaborace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
jeho	jeho	k3xOp3gFnSc1	jeho
obliba	obliba	k1gFnSc1	obliba
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
o	o	k7c4	o
"	"	kIx"	"
<g/>
Krále	Král	k1gMnPc4	Král
komiků	komik	k1gMnPc2	komik
<g/>
"	"	kIx"	"
a	a	k8xC	a
časté	častý	k2eAgFnPc1d1	častá
televizní	televizní	k2eAgFnPc1d1	televizní
reprízy	repríza	k1gFnPc1	repríza
jeho	jeho	k3xOp3gInPc2	jeho
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
libereckému	liberecký	k2eAgMnSc3d1	liberecký
krejčímu	krejčí	k1gMnSc3	krejčí
<g/>
,	,	kIx,	,
vlastenci	vlastenec	k1gMnSc3	vlastenec
a	a	k8xC	a
ochotníkovi	ochotník	k1gMnSc3	ochotník
Antonínu	Antonín	k1gMnSc3	Antonín
Burianovi	Burian	k1gMnSc3	Burian
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
starší	starý	k2eAgFnSc3d2	starší
manželce	manželka	k1gFnSc3	manželka
Marii	Maria	k1gFnSc3	Maria
Burianové	Burianová	k1gFnSc3	Burianová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
manželství	manželství	k1gNnSc2	manželství
dceru	dcera	k1gFnSc4	dcera
Žofii	Žofie	k1gFnSc4	Žofie
Pickovou	Picková	k1gFnSc4	Picková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
prožil	prožít	k5eAaPmAgMnS	prožít
prvních	první	k4xOgNnPc6	první
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1901	[number]	k4	1901
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
na	na	k7c4	na
pražské	pražský	k2eAgNnSc4d1	Pražské
předměstí	předměstí	k1gNnSc4	předměstí
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
německá	německý	k2eAgFnSc1d1	německá
<g/>
,	,	kIx,	,
dost	dost	k6eAd1	dost
příležitostí	příležitost	k1gFnPc2	příležitost
k	k	k7c3	k
vlasteneckým	vlastenecký	k2eAgFnPc3d1	vlastenecká
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
vybírající	vybírající	k2eAgFnSc2d1	vybírající
potravinové	potravinový	k2eAgFnSc2d1	potravinová
daně	daň	k1gFnSc2	daň
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
malého	malý	k2eAgMnSc2d1	malý
syna	syn	k1gMnSc2	syn
vodil	vodit	k5eAaImAgInS	vodit
pravidelně	pravidelně	k6eAd1	pravidelně
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c4	na
opery	opera	k1gFnPc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyučil	vyučit	k5eAaPmAgInS	vyučit
obchodním	obchodní	k2eAgMnSc7d1	obchodní
příručím	příručí	k1gMnSc7	příručí
na	na	k7c6	na
obchodní	obchodní	k2eAgFnSc6d1	obchodní
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
krejčího	krejčí	k1gMnSc2	krejčí
se	se	k3xPyFc4	se
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
setkával	setkávat	k5eAaImAgMnS	setkávat
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
možnost	možnost	k1gFnSc4	možnost
slýchat	slýchat	k5eAaImF	slýchat
příběhy	příběh	k1gInPc4	příběh
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
zpíval	zpívat	k5eAaImAgMnS	zpívat
na	na	k7c4	na
kůru	kůra	k1gFnSc4	kůra
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
rád	rád	k6eAd1	rád
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
velkou	velký	k2eAgFnSc4d1	velká
zálibu	záliba	k1gFnSc4	záliba
ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Vynikal	vynikat	k5eAaImAgMnS	vynikat
zejména	zejména	k9	zejména
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
brankářem	brankář	k1gMnSc7	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
Union	union	k1gInSc4	union
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
za	za	k7c4	za
Viktorii	Viktoria	k1gFnSc4	Viktoria
Žižkov	Žižkov	k1gInSc1	Žižkov
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
ve	v	k7c6	v
Spartě	Sparta	k1gFnSc6	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
také	také	k6eAd1	také
cyklistice	cyklistika	k1gFnSc3	cyklistika
a	a	k8xC	a
závodně	závodně	k6eAd1	závodně
hrál	hrát	k5eAaImAgMnS	hrát
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
zúčastňoval	zúčastňovat	k5eAaImAgMnS	zúčastňovat
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
často	často	k6eAd1	často
vyhrával	vyhrávat	k5eAaImAgMnS	vyhrávat
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnPc2	jeho
dosud	dosud	k6eAd1	dosud
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
trofejí	trofej	k1gFnPc2	trofej
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zabírají	zabírat	k5eAaImIp3nP	zabírat
jednu	jeden	k4xCgFnSc4	jeden
velkou	velký	k2eAgFnSc4d1	velká
vitrínu	vitrína	k1gFnSc4	vitrína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
však	však	k9	však
většina	většina	k1gFnSc1	většina
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
bavičské	bavičský	k2eAgNnSc4d1	bavičské
nadání	nadání	k1gNnSc4	nadání
zpočátku	zpočátku	k6eAd1	zpočátku
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
večírcích	večírek	k1gInPc6	večírek
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
nadání	nadání	k1gNnSc4	nadání
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
postupně	postupně	k6eAd1	postupně
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
cestu	cesta	k1gFnSc4	cesta
i	i	k8xC	i
k	k	k7c3	k
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
vystupování	vystupování	k1gNnSc3	vystupování
(	(	kIx(	(
<g/>
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
mu	on	k3xPp3gNnSc3	on
otec	otec	k1gMnSc1	otec
nejdřív	dříve	k6eAd3	dříve
bránil	bránit	k5eAaImAgMnS	bránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
viděl	vidět	k5eAaImAgMnS	vidět
syna	syn	k1gMnSc4	syn
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
představení	představení	k1gNnSc6	představení
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
<g/>
,	,	kIx,	,
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
výborný	výborný	k2eAgMnSc1d1	výborný
komik	komik	k1gMnSc1	komik
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
jej	on	k3xPp3gNnSc4	on
od	od	k7c2	od
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
neodrazoval	odrazovat	k5eNaImAgInS	odrazovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
soukromí	soukromí	k1gNnSc6	soukromí
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
poměru	poměr	k1gInSc2	poměr
s	s	k7c7	s
tanečnicí	tanečnice	k1gFnSc7	tanečnice
Annou	Anna	k1gFnSc7	Anna
Emílií	Emílie	k1gFnSc7	Emílie
Pírkovou	pírkový	k2eAgFnSc7d1	Pírková
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
svobodna	svoboden	k2eAgNnPc4d1	svobodno
dceru	dcera	k1gFnSc4	dcera
Emilku	Emilka	k1gFnSc4	Emilka
Burianovou	Burianová	k1gFnSc4	Burianová
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
1912	[number]	k4	1912
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Ninou	Nina	k1gFnSc7	Nina
Červenkovou-Burianovou	Červenkovou-Burianová	k1gFnSc7	Červenkovou-Burianová
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
největší	veliký	k2eAgFnSc7d3	veliký
oporou	opora	k1gFnSc7	opora
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
nejtěžších	těžký	k2eAgFnPc6d3	nejtěžší
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
ho	on	k3xPp3gNnSc4	on
velmi	velmi	k6eAd1	velmi
ráda	rád	k2eAgFnSc1d1	ráda
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
největší	veliký	k2eAgFnSc7d3	veliký
obdivovatelkou	obdivovatelka	k1gFnSc7	obdivovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
na	na	k7c6	na
každém	každý	k3xTgNnSc6	každý
Burianově	Burianův	k2eAgNnSc6d1	Burianovo
představení	představení	k1gNnSc6	představení
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zpozdila	zpozdit	k5eAaPmAgFnS	zpozdit
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gMnSc1	Vlasta
nechal	nechat	k5eAaPmAgMnS	nechat
pozdržet	pozdržet	k5eAaPmF	pozdržet
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
Nina	Nina	k1gFnSc1	Nina
nebyla	být	k5eNaImAgFnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
lóži	lóže	k1gFnSc6	lóže
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
Nina	Nina	k1gFnSc1	Nina
nemocná	nemocný	k2eAgFnSc1d1	nemocná
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
zavést	zavést	k5eAaPmF	zavést
mikrofon	mikrofon	k1gInSc4	mikrofon
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
a	a	k8xC	a
Nina	Nina	k1gFnSc1	Nina
poté	poté	k6eAd1	poté
jeho	on	k3xPp3gInSc4	on
výkon	výkon	k1gInSc4	výkon
poslouchala	poslouchat	k5eAaImAgFnS	poslouchat
telefonem	telefon	k1gInSc7	telefon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc3	jeho
dceři	dcera	k1gFnSc3	dcera
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Kristl	Kristl	k1gMnSc1	Kristl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
jej	on	k3xPp3gMnSc4	on
znala	znát	k5eAaImAgFnS	znát
jako	jako	k8xS	jako
zábavného	zábavný	k2eAgMnSc4d1	zábavný
společníka	společník	k1gMnSc4	společník
<g/>
,	,	kIx,	,
v	v	k7c6	v
soukromí	soukromí	k1gNnSc6	soukromí
byl	být	k5eAaImAgMnS	být
však	však	k9	však
melancholický	melancholický	k2eAgMnSc1d1	melancholický
<g/>
,	,	kIx,	,
náladový	náladový	k2eAgMnSc1d1	náladový
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
deprese	deprese	k1gFnPc4	deprese
–	–	k?	–
trpěl	trpět	k5eAaImAgMnS	trpět
maniodepresivními	maniodepresivní	k2eAgInPc7d1	maniodepresivní
stavy	stav	k1gInPc7	stav
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uzavíral	uzavírat	k5eAaPmAgInS	uzavírat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Dejvicích	Dejvice	k1gFnPc6	Dejvice
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
elegantně	elegantně	k6eAd1	elegantně
a	a	k8xC	a
luxusně	luxusně	k6eAd1	luxusně
zařízenou	zařízený	k2eAgFnSc4d1	zařízená
<g/>
.	.	kIx.	.
</s>
<s>
Každodenně	každodenně	k6eAd1	každodenně
sportoval	sportovat	k5eAaImAgMnS	sportovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vile	vila	k1gFnSc6	vila
velkou	velký	k2eAgFnSc4d1	velká
tělocvičnu	tělocvična	k1gFnSc4	tělocvična
a	a	k8xC	a
u	u	k7c2	u
vily	vila	k1gFnSc2	vila
postaven	postavit	k5eAaPmNgInS	postavit
bazén	bazén	k1gInSc1	bazén
a	a	k8xC	a
tenisový	tenisový	k2eAgInSc4d1	tenisový
kurt	kurt	k1gInSc4	kurt
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
ráno	ráno	k6eAd1	ráno
se	se	k3xPyFc4	se
projížděl	projíždět	k5eAaImAgMnS	projíždět
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
a	a	k8xC	a
pořádal	pořádat	k5eAaImAgMnS	pořádat
přátelské	přátelský	k2eAgInPc4d1	přátelský
tenisové	tenisový	k2eAgInPc4d1	tenisový
zápasy	zápas	k1gInPc4	zápas
i	i	k9	i
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
(	(	kIx(	(
<g/>
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Klamovce	Klamovka	k1gFnSc6	Klamovka
si	se	k3xPyFc3	se
například	například	k6eAd1	například
zahrál	zahrát	k5eAaPmAgMnS	zahrát
několik	několik	k4yIc4	několik
tenisových	tenisový	k2eAgInPc2d1	tenisový
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
Karlem	Karel	k1gMnSc7	Karel
Koželuhem	koželuh	k1gMnSc7	koželuh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
velkým	velký	k2eAgMnSc7d1	velký
mecenášem	mecenáš	k1gMnSc7	mecenáš
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
(	(	kIx(	(
<g/>
sponzoroval	sponzorovat	k5eAaImAgInS	sponzorovat
také	také	k9	také
národní	národní	k2eAgNnSc1d1	národní
cyklistické	cyklistický	k2eAgNnSc1d1	cyklistické
mužstvo	mužstvo	k1gNnSc1	mužstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
amatérských	amatérský	k2eAgFnPc2d1	amatérská
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
téměř	téměř	k6eAd1	téměř
neznámá	známý	k2eNgFnSc1d1	neznámá
je	být	k5eAaImIp3nS	být
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
jedné	jeden	k4xCgFnSc2	jeden
pamětnice	pamětnice	k1gFnSc2	pamětnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hází	házet	k5eAaImIp3nS	házet
nezaměstnaným	zaměstnaný	k2eNgMnPc3d1	nezaměstnaný
dělníkům	dělník	k1gMnPc3	dělník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
hráli	hrát	k5eAaImAgMnP	hrát
fotbal	fotbal	k1gInSc4	fotbal
se	s	k7c7	s
starým	starý	k2eAgInSc7d1	starý
roztrhaným	roztrhaný	k2eAgInSc7d1	roztrhaný
míčem	míč	k1gInSc7	míč
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc4d1	nový
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
lépe	dobře	k6eAd2	dobře
sportovat	sportovat	k5eAaImF	sportovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914	[number]	k4	1914
až	až	k9	až
1920	[number]	k4	1920
aktivně	aktivně	k6eAd1	aktivně
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c2	za
AC	AC	kA	AC
Sparta	Sparta	k1gFnSc1	Sparta
jako	jako	k8xC	jako
prvoligový	prvoligový	k2eAgMnSc1d1	prvoligový
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zlom	zlom	k1gInSc1	zlom
a	a	k8xC	a
dočasné	dočasný	k2eAgNnSc1d1	dočasné
přerušení	přerušení	k1gNnSc1	přerušení
kariéry	kariéra	k1gFnSc2	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
a	a	k8xC	a
v	v	k7c6	v
popularitě	popularita	k1gFnSc6	popularita
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
zlomu	zlom	k1gInSc3	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
jeho	jeho	k3xOp3gNnSc4	jeho
divadlo	divadlo	k1gNnSc4	divadlo
zavřeli	zavřít	k5eAaPmAgMnP	zavřít
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
Čechy	Čech	k1gMnPc7	Čech
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
znárodněno	znárodnit	k5eAaPmNgNnS	znárodnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
do	do	k7c2	do
společné	společný	k2eAgFnSc2d1	společná
cely	cela	k1gFnSc2	cela
s	s	k7c7	s
esesáky	esesáky	k?	esesáky
a	a	k8xC	a
kriminálníky	kriminálník	k1gMnPc7	kriminálník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
ponižován	ponižovat	k5eAaImNgInS	ponižovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
intervenci	intervence	k1gFnSc4	intervence
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
<g/>
,	,	kIx,	,
vyšetřován	vyšetřován	k2eAgMnSc1d1	vyšetřován
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
jej	on	k3xPp3gInSc4	on
soud	soud	k1gInSc4	soud
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
negativním	negativní	k2eAgInSc6d1	negativní
ohlasu	ohlas	k1gInSc6	ohlas
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
i	i	k9	i
na	na	k7c4	na
zásah	zásah	k1gInSc4	zásah
politiků	politik	k1gMnPc2	politik
(	(	kIx(	(
<g/>
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
chtěli	chtít	k5eAaImAgMnP	chtít
exemplárně	exemplárně	k6eAd1	exemplárně
potrestat	potrestat	k5eAaPmF	potrestat
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
vykonstruovaných	vykonstruovaný	k2eAgNnPc2d1	vykonstruované
obvinění	obvinění	k1gNnSc2	obvinění
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
za	za	k7c4	za
kolaboraci	kolaborace	k1gFnSc4	kolaborace
s	s	k7c7	s
německými	německý	k2eAgMnPc7d1	německý
okupanty	okupant	k1gMnPc7	okupant
na	na	k7c6	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
k	k	k7c3	k
těžkému	těžký	k2eAgInSc3d1	těžký
žaláři	žalář	k1gInSc3	žalář
a	a	k8xC	a
pokutě	pokuta	k1gFnSc3	pokuta
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
odsouzení	odsouzení	k1gNnSc2	odsouzení
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
skeči	skeč	k1gInSc6	skeč
Hvězdy	hvězda	k1gFnSc2	hvězda
nad	nad	k7c7	nad
Baltimorem	Baltimore	k1gInSc7	Baltimore
zesměšňoval	zesměšňovat	k5eAaImAgMnS	zesměšňovat
exilovou	exilový	k2eAgFnSc4d1	exilová
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Obhajoba	obhajoba	k1gFnSc1	obhajoba
<g/>
:	:	kIx,	:
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
byl	být	k5eAaImAgInS	být
donucen	donutit	k5eAaPmNgMnS	donutit
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
svědci	svědek	k1gMnPc1	svědek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
skeč	skeč	k1gInSc4	skeč
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
slyšeli	slyšet	k5eAaImAgMnP	slyšet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
divili	divit	k5eAaImAgMnP	divit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Burian	Burian	k1gMnSc1	Burian
nebyl	být	k5eNaImAgMnS	být
Němci	Němec	k1gMnSc3	Němec
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
vystoupení	vystoupení	k1gNnSc4	vystoupení
nařčen	nařknout	k5eAaPmNgMnS	nařknout
ze	z	k7c2	z
sabotáže	sabotáž	k1gFnSc2	sabotáž
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prý	prý	k9	prý
strašnější	strašný	k2eAgInSc4d2	strašnější
výkon	výkon	k1gInSc4	výkon
neslyšeli	slyšet	k5eNaImAgMnP	slyšet
<g/>
...	...	k?	...
A	a	k9	a
dalším	další	k2eAgNnSc7d1	další
vystoupením	vystoupení	k1gNnSc7	vystoupení
se	se	k3xPyFc4	se
vyhýbal	vyhýbat	k5eAaImAgInS	vyhýbat
předstíráním	předstírání	k1gNnSc7	předstírání
různých	různý	k2eAgFnPc2d1	různá
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prý	prý	k9	prý
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vile	vila	k1gFnSc6	vila
hostil	hostit	k5eAaImAgInS	hostit
německé	německý	k2eAgMnPc4d1	německý
okupanty	okupant	k1gMnPc4	okupant
včetně	včetně	k7c2	včetně
K.	K.	kA	K.
H.	H.	kA	H.
Franka	Frank	k1gMnSc2	Frank
<g/>
.	.	kIx.	.
</s>
<s>
Obhajoba	obhajoba	k1gFnSc1	obhajoba
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byli	být	k5eAaImAgMnP	být
u	u	k7c2	u
Buriana	Burian	k1gMnSc2	Burian
ve	v	k7c6	v
vile	vila	k1gFnSc6	vila
Němci	Němec	k1gMnPc1	Němec
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
udání	udání	k1gNnSc4	udání
sousedů	soused	k1gMnPc2	soused
hledali	hledat	k5eAaImAgMnP	hledat
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
údajně	údajně	k6eAd1	údajně
zakopané	zakopaný	k2eAgFnPc4d1	zakopaná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
s	s	k7c7	s
K.	K.	kA	K.
H.	H.	kA	H.
Frankem	Frank	k1gMnSc7	Frank
se	se	k3xPyFc4	se
viděl	vidět	k5eAaImAgMnS	vidět
Burian	Burian	k1gMnSc1	Burian
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
uplatil	uplatit	k5eAaPmAgMnS	uplatit
magistrátní	magistrátní	k2eAgMnPc4d1	magistrátní
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získal	získat	k5eAaPmAgMnS	získat
městský	městský	k2eAgInSc4d1	městský
pozemek	pozemek	k1gInSc4	pozemek
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
tenisového	tenisový	k2eAgInSc2d1	tenisový
kurtu	kurt	k1gInSc2	kurt
a	a	k8xC	a
když	když	k8xS	když
za	za	k7c2	za
Protektorátu	protektorát	k1gInSc2	protektorát
byl	být	k5eAaImAgInS	být
benzín	benzín	k1gInSc1	benzín
na	na	k7c4	na
příděl	příděl	k1gInSc4	příděl
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
ho	on	k3xPp3gInSc4	on
nepoctivým	poctivý	k2eNgInSc7d1	nepoctivý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
mohl	moct	k5eAaImAgMnS	moct
vůbec	vůbec	k9	vůbec
s	s	k7c7	s
autem	auto	k1gNnSc7	auto
jezdit	jezdit	k5eAaImF	jezdit
<g/>
.	.	kIx.	.
</s>
<s>
Obhajoba	obhajoba	k1gFnSc1	obhajoba
<g/>
:	:	kIx,	:
S	s	k7c7	s
autem	auto	k1gNnSc7	auto
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c4	na
fingované	fingovaný	k2eAgFnPc4d1	fingovaná
smlouvy	smlouva	k1gFnPc4	smlouva
s	s	k7c7	s
Lucernafilmem	Lucernafilmo	k1gNnSc7	Lucernafilmo
a	a	k8xC	a
ne	ne	k9	ne
z	z	k7c2	z
dovolení	dovolení	k1gNnSc2	dovolení
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Využitím	využití	k1gNnSc7	využití
německé	německý	k2eAgFnSc2d1	německá
protekce	protekce	k1gFnSc2	protekce
mohl	moct	k5eAaImAgInS	moct
vystoupit	vystoupit	k5eAaPmF	vystoupit
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
jako	jako	k8xS	jako
principál	principál	k1gInSc1	principál
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgFnSc6d1	Smetanova
Prodané	prodaný	k2eAgFnSc6d1	prodaná
nevěstě	nevěsta	k1gFnSc6	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Obhajoba	obhajoba	k1gFnSc1	obhajoba
<g/>
:	:	kIx,	:
Žádost	žádost	k1gFnSc1	žádost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
pohostinsky	pohostinsky	k6eAd1	pohostinsky
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
však	však	k9	však
Burian	Burian	k1gMnSc1	Burian
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
podal	podat	k5eAaPmAgInS	podat
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
své	svůj	k3xOyFgNnSc4	svůj
upřímné	upřímný	k2eAgNnSc4d1	upřímné
"	"	kIx"	"
<g/>
češství	češství	k1gNnSc4	češství
<g/>
"	"	kIx"	"
a	a	k8xC	a
vedení	vedení	k1gNnSc1	vedení
ho	on	k3xPp3gMnSc4	on
ochotně	ochotně	k6eAd1	ochotně
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nátlaku	nátlak	k1gInSc2	nátlak
kladně	kladně	k6eAd1	kladně
vyřídilo	vyřídit	k5eAaPmAgNnS	vyřídit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
soudu	soud	k1gInSc2	soud
se	se	k3xPyFc4	se
také	také	k9	také
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
schválně	schválně	k6eAd1	schválně
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
role	role	k1gFnPc4	role
v	v	k7c6	v
německých	německý	k2eAgInPc6d1	německý
filmech	film	k1gInPc6	film
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdravil	zdravit	k5eAaImAgInS	zdravit
nacistickým	nacistický	k2eAgInSc7d1	nacistický
pozdravem	pozdrav	k1gInSc7	pozdrav
(	(	kIx(	(
<g/>
zdviženou	zdvižený	k2eAgFnSc7d1	zdvižená
pravicí	pravice	k1gFnSc7	pravice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obhajoba	obhajoba	k1gFnSc1	obhajoba
<g/>
:	:	kIx,	:
Zde	zde	k6eAd1	zde
Burian	Burian	k1gMnSc1	Burian
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
nezdravil	zdravit	k5eNaImAgInS	zdravit
a	a	k8xC	a
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
zdravil	zdravit	k5eAaImAgMnS	zdravit
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
se	s	k7c7	s
zdviženou	zdvižený	k2eAgFnSc7d1	zdvižená
pravicí	pravice	k1gFnSc7	pravice
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
ruka	ruka	k1gFnSc1	ruka
se	s	k7c7	s
zaťatou	zaťatý	k2eAgFnSc7d1	zaťatá
pěstí	pěst	k1gFnSc7	pěst
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
úkon	úkon	k1gInSc4	úkon
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Takhle	takhle	k6eAd1	takhle
vysoko	vysoko	k6eAd1	vysoko
skáče	skákat	k5eAaImIp3nS	skákat
můj	můj	k3xOp1gMnSc1	můj
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
když	když	k8xS	když
přijdu	přijít	k5eAaPmIp1nS	přijít
domů	domů	k6eAd1	domů
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
u	u	k7c2	u
diváků	divák	k1gMnPc2	divák
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
salvami	salva	k1gFnPc7	salva
smíchu	smích	k1gInSc2	smích
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pamětníků	pamětník	k1gMnPc2	pamětník
dokonce	dokonce	k9	dokonce
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
za	za	k7c4	za
okupace	okupace	k1gFnPc4	okupace
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
s	s	k7c7	s
bičem	bič	k1gInSc7	bič
a	a	k8xC	a
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
připevněno	připevnit	k5eAaPmNgNnS	připevnit
velké	velká	k1gFnPc4	velká
tiskací	tiskací	k2eAgFnPc4d1	tiskací
C.	C.	kA	C.
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
poté	poté	k6eAd1	poté
ptali	ptat	k5eAaImAgMnP	ptat
co	co	k8xS	co
to	ten	k3xDgNnSc4	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
bič	bič	k1gInSc1	bič
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
cé	cé	k?	cé
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
což	což	k3yQnSc4	což
za	za	k7c2	za
protektorátu	protektorát	k1gInSc2	protektorát
mohl	moct	k5eAaImAgMnS	moct
hrozit	hrozit	k5eAaImF	hrozit
trest	trest	k1gInSc4	trest
vězení	vězení	k1gNnSc2	vězení
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
smrti	smrt	k1gFnSc2	smrt
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
autor	autor	k1gMnSc1	autor
Ondřej	Ondřej	k1gMnSc1	Ondřej
Suchý	Suchý	k1gMnSc1	Suchý
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
obvinění	obvinění	k1gNnSc4	obvinění
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
Burianovu	Burianův	k2eAgFnSc4d1	Burianova
obhajobu	obhajoba	k1gFnSc4	obhajoba
podle	podle	k7c2	podle
dobových	dobový	k2eAgInPc2d1	dobový
materiálů	materiál	k1gInPc2	materiál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
už	už	k6eAd1	už
divadlo	divadlo	k1gNnSc4	divadlo
nesměl	smět	k5eNaImAgMnS	smět
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mu	on	k3xPp3gMnSc3	on
zabaven	zabavit	k5eAaPmNgInS	zabavit
prakticky	prakticky	k6eAd1	prakticky
veškerý	veškerý	k3xTgInSc4	veškerý
majetek	majetek	k1gInSc4	majetek
(	(	kIx(	(
<g/>
také	také	k9	také
vila	vila	k1gFnSc1	vila
<g/>
)	)	kIx)	)
a	a	k8xC	a
následujících	následující	k2eAgNnPc2d1	následující
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
nesměl	smět	k5eNaImAgMnS	smět
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
vile	vila	k1gFnSc6	vila
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
školka	školka	k1gFnSc1	školka
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
šel	jít	k5eAaImAgMnS	jít
Burian	Burian	k1gMnSc1	Burian
navštívit	navštívit	k5eAaPmF	navštívit
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pobývaly	pobývat	k5eAaImAgInP	pobývat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
bývalé	bývalý	k2eAgFnSc6d1	bývalá
vile	vila	k1gFnSc6	vila
<g/>
,	,	kIx,	,
nepřišel	přijít	k5eNaPmAgInS	přijít
nikdy	nikdy	k6eAd1	nikdy
s	s	k7c7	s
prázdnou	prázdná	k1gFnSc7	prázdná
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
musel	muset	k5eAaImAgMnS	muset
pracovat	pracovat	k5eAaImF	pracovat
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
manuální	manuální	k2eAgFnSc7d1	manuální
prací	práce	k1gFnSc7	práce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vážným	vážný	k1gMnPc3	vážný
v	v	k7c6	v
severočeských	severočeský	k2eAgInPc6d1	severočeský
dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jako	jako	k9	jako
poslíček	poslíček	k1gMnSc1	poslíček
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
horských	horský	k2eAgFnPc6d1	horská
chatách	chata	k1gFnPc6	chata
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jezdil	jezdit	k5eAaImAgMnS	jezdit
s	s	k7c7	s
autem	auto	k1gNnSc7	auto
pro	pro	k7c4	pro
rekreanty	rekreant	k1gMnPc4	rekreant
ROH	roh	k1gInSc4	roh
a	a	k8xC	a
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
pak	pak	k6eAd1	pak
škrábal	škrábat	k5eAaImAgMnS	škrábat
brambory	brambor	k1gInPc4	brambor
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
už	už	k6eAd1	už
začal	začít	k5eAaPmAgInS	začít
ilegálně	ilegálně	k6eAd1	ilegálně
vystupovat	vystupovat	k5eAaImF	vystupovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smutný	smutný	k2eAgInSc4d1	smutný
návrat	návrat	k1gInSc4	návrat
a	a	k8xC	a
konec	konec	k1gInSc4	konec
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c4	za
těch	ten	k3xDgNnPc2	ten
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nesměl	smět	k5eNaImAgMnS	smět
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgMnS	změnit
<g/>
:	:	kIx,	:
byl	být	k5eAaImAgMnS	být
skromnější	skromný	k2eAgMnSc1d2	skromnější
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sportovce	sportovec	k1gMnSc2	sportovec
s	s	k7c7	s
výbornou	výborný	k2eAgFnSc7d1	výborná
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
kondicí	kondice	k1gFnSc7	kondice
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
stařík	stařík	k1gMnSc1	stařík
–	–	k?	–
viditelně	viditelně	k6eAd1	viditelně
sešel	sejít	k5eAaPmAgMnS	sejít
<g/>
,	,	kIx,	,
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
pronásledování	pronásledování	k1gNnSc2	pronásledování
mu	on	k3xPp3gNnSc3	on
podkopalo	podkopat	k5eAaPmAgNnS	podkopat
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
herecký	herecký	k2eAgInSc4d1	herecký
distanc	distanc	k?	distanc
zrušen	zrušit	k5eAaPmNgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkušebním	zkušební	k2eAgNnSc6d1	zkušební
představení	představení	k1gNnSc6	představení
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Karlíně	Karlín	k1gInSc6	Karlín
(	(	kIx(	(
<g/>
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
Jiřího	Jiří	k1gMnSc2	Jiří
Frejky	Frejka	k1gFnSc2	Frejka
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Wericha	Werich	k1gMnSc2	Werich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
vyhození	vyhození	k1gNnSc2	vyhození
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
začal	začít	k5eAaPmAgMnS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
estrádách	estráda	k1gFnPc6	estráda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
úplně	úplně	k6eAd1	úplně
zničily	zničit	k5eAaPmAgFnP	zničit
zdraví	zdraví	k1gNnSc4	zdraví
–	–	k?	–
měl	mít	k5eAaImAgMnS	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
žílami	žíla	k1gFnPc7	žíla
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
mu	on	k3xPp3gMnSc3	on
otékaly	otékat	k5eAaImAgFnP	otékat
nohy	noha	k1gFnPc1	noha
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nemohl	moct	k5eNaImAgMnS	moct
chodit	chodit	k5eAaImF	chodit
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
při	při	k7c6	při
představení	představení	k1gNnSc6	představení
sedět	sedět	k5eAaImF	sedět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
bavil	bavit	k5eAaImAgMnS	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
humor	humor	k1gInSc1	humor
i	i	k8xC	i
komika	komika	k1gFnSc1	komika
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
ztratily	ztratit	k5eAaPmAgFnP	ztratit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
oné	onen	k3xDgFnSc2	onen
výbušnosti	výbušnost	k1gFnSc2	výbušnost
a	a	k8xC	a
osobitosti	osobitost	k1gFnSc2	osobitost
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jim	on	k3xPp3gMnPc3	on
dřív	dříve	k6eAd2	dříve
dodávaly	dodávat	k5eAaImAgInP	dodávat
světovou	světový	k2eAgFnSc4d1	světová
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
neváhal	váhat	k5eNaImAgMnS	váhat
přiznat	přiznat	k5eAaPmF	přiznat
i	i	k9	i
Burian	Burian	k1gMnSc1	Burian
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pořady	pořad	k1gInPc1	pořad
natočené	natočený	k2eAgInPc1d1	natočený
pro	pro	k7c4	pro
rozhlas	rozhlas	k1gInSc4	rozhlas
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Právní	právní	k2eAgFnSc1d1	právní
poradna	poradna	k1gFnSc1	poradna
<g/>
"	"	kIx"	"
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
přesto	přesto	k8xC	přesto
vrcholem	vrchol	k1gInSc7	vrchol
realizovaného	realizovaný	k2eAgInSc2d1	realizovaný
humoru	humor	k1gInSc2	humor
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
smutná	smutný	k2eAgFnSc1d1	smutná
poznámka	poznámka	k1gFnSc1	poznámka
určená	určený	k2eAgFnSc1d1	určená
fanouškovi	fanoušek	k1gMnSc3	fanoušek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
na	na	k7c6	na
fotografii	fotografia	k1gFnSc6	fotografia
jako	jako	k9	jako
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
Král	Král	k1gMnSc1	Král
komiků	komik	k1gMnPc2	komik
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
<g/>
?	?	kIx.	?
</s>
<s>
Kdepak	kdepak	k9	kdepak
král	král	k1gMnSc1	král
<g/>
!	!	kIx.	!
</s>
<s>
Teď	teď	k6eAd1	teď
už	už	k9	už
jenom	jenom	k9	jenom
králíček	králíček	k1gMnSc1	králíček
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Tehdy	tehdy	k6eAd1	tehdy
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
povoleno	povolen	k2eAgNnSc1d1	povoleno
natáčet	natáčet	k5eAaImF	natáčet
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uživil	uživit	k5eAaPmAgMnS	uživit
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
vystupovat	vystupovat	k5eAaImF	vystupovat
na	na	k7c6	na
estrádách	estráda	k1gFnPc6	estráda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
nemocný	nemocný	k2eAgMnSc1d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
představení	představení	k1gNnSc1	představení
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
odehrál	odehrát	k5eAaPmAgMnS	odehrát
s	s	k7c7	s
těžkým	těžký	k2eAgInSc7d1	těžký
zápalem	zápal	k1gInSc7	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
umřel	umřít	k5eAaPmAgMnS	umřít
na	na	k7c6	na
plicní	plicní	k2eAgFnSc6d1	plicní
embolii	embolie	k1gFnSc6	embolie
<g/>
,	,	kIx,	,
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
své	svůj	k3xOyFgFnSc2	svůj
věrné	věrný	k2eAgFnSc2d1	věrná
družky	družka	k1gFnSc2	družka
Niny	Nina	k1gFnSc2	Nina
Burianové	Burianová	k1gFnSc2	Burianová
<g/>
.	.	kIx.	.
</s>
<s>
Nina	Nina	k1gFnSc1	Nina
pak	pak	k6eAd1	pak
zemřela	zemřít	k5eAaPmAgFnS	zemřít
o	o	k7c4	o
devět	devět	k4xCc4	devět
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pamětníků	pamětník	k1gMnPc2	pamětník
na	na	k7c6	na
manželově	manželův	k2eAgInSc6d1	manželův
hrobě	hrob	k1gInSc6	hrob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
Vinohradském	vinohradský	k2eAgInSc6d1	vinohradský
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
přeneseny	přenesen	k2eAgInPc4d1	přenesen
ostatky	ostatek	k1gInPc4	ostatek
na	na	k7c4	na
Vyšehradský	vyšehradský	k2eAgInSc4d1	vyšehradský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náhrobku	náhrobek	k1gInSc6	náhrobek
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
umístěna	umístit	k5eAaPmNgFnS	umístit
i	i	k9	i
bronzová	bronzový	k2eAgFnSc1d1	bronzová
bysta	bysta	k1gFnSc1	bysta
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
odcizena	odcizen	k2eAgFnSc1d1	odcizena
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
umístěna	umístěn	k2eAgFnSc1d1	umístěna
plastika	plastika	k1gFnSc1	plastika
znázorňující	znázorňující	k2eAgFnPc4d1	znázorňující
dvě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
s	s	k7c7	s
názvem	název	k1gInSc7	název
,,	,,	k?	,,
<g/>
Ruce	ruka	k1gFnPc4	ruka
komika	komika	k1gFnSc1	komika
<g/>
,,	,,	k?	,,
od	od	k7c2	od
ak.	ak.	k?	ak.
sochaře	sochař	k1gMnSc2	sochař
Tomáše	Tomáš	k1gMnSc2	Tomáš
Vejdovského	Vejdovský	k2eAgMnSc2d1	Vejdovský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Burianova	Burianův	k2eAgFnSc1d1	Burianova
rehabilitace	rehabilitace	k1gFnSc1	rehabilitace
==	==	k?	==
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
střihový	střihový	k2eAgInSc1d1	střihový
dokument	dokument	k1gInSc1	dokument
Král	Král	k1gMnSc1	Král
komiků	komik	k1gMnPc2	komik
(	(	kIx(	(
<g/>
o	o	k7c6	o
Burianovi	Burian	k1gMnSc6	Burian
byly	být	k5eAaImAgFnP	být
natočeny	natočit	k5eAaBmNgFnP	natočit
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgInPc1	čtyři
střihové	střihový	k2eAgInPc1d1	střihový
filmy	film	k1gInPc1	film
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
reedice	reedice	k1gFnSc1	reedice
jeho	jeho	k3xOp3gFnPc2	jeho
gramofonových	gramofonový	k2eAgFnPc2d1	gramofonová
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
rehabilitován	rehabilitovat	k5eAaBmNgInS	rehabilitovat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
divadelní	divadelní	k2eAgMnSc1d1	divadelní
historik	historik	k1gMnSc1	historik
Vladimír	Vladimír	k1gMnSc1	Vladimír
Just	just	k6eAd1	just
prosadil	prosadit	k5eAaPmAgMnS	prosadit
znovuprojednání	znovuprojednání	k1gNnSc4	znovuprojednání
jeho	on	k3xPp3gInSc2	on
případu	případ	k1gInSc2	případ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Just	just	k6eAd1	just
<g/>
,	,	kIx,	,
Causa	causa	k1gFnSc1	causa
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Divadelní	divadelní	k2eAgInPc1d1	divadelní
začátky	začátek	k1gInPc1	začátek
v	v	k7c6	v
kabaretech	kabaret	k1gInPc6	kabaret
===	===	k?	===
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
začal	začít	k5eAaPmAgInS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
ve	v	k7c6	v
vedlejších	vedlejší	k2eAgFnPc6d1	vedlejší
rolích	role	k1gFnPc6	role
velkých	velká	k1gFnPc2	velká
divadel	divadlo	k1gNnPc2	divadlo
Na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
a	a	k8xC	a
ve	v	k7c6	v
Švandově	Švandův	k2eAgNnSc6d1	Švandovo
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Karla	Karel	k1gMnSc2	Karel
Hašlera	Hašler	k1gMnSc2	Hašler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
povšiml	povšimnout	k5eAaPmAgMnS	povšimnout
jeho	jeho	k3xOp3gFnSc2	jeho
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
kabaretech	kabaret	k1gInPc6	kabaret
(	(	kIx(	(
<g/>
Rokoko	rokoko	k1gNnSc4	rokoko
<g/>
,	,	kIx,	,
Červená	červený	k2eAgFnSc1d1	červená
sedma	sedma	k1gFnSc1	sedma
<g/>
,	,	kIx,	,
Bum	bum	k0	bum
<g/>
,	,	kIx,	,
Revoluční	revoluční	k2eAgFnSc1d1	revoluční
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
U	u	k7c2	u
Deutschů	Deutsch	k1gInPc2	Deutsch
<g/>
,	,	kIx,	,
Montmartre	Montmartr	k1gMnSc5	Montmartr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
stal	stát	k5eAaPmAgInS	stát
miláčkem	miláček	k1gMnSc7	miláček
místního	místní	k2eAgNnSc2d1	místní
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
soupeřil	soupeřit	k5eAaImAgInS	soupeřit
se	s	k7c7	s
stejně	stejně	k6eAd1	stejně
populárním	populární	k2eAgMnSc7d1	populární
hercem	herec	k1gMnSc7	herec
Ferencem	Ferenc	k1gMnSc7	Ferenc
Futuristou	futurista	k1gMnSc7	futurista
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
,	,	kIx,	,
po	po	k7c6	po
zběhnutí	zběhnutí	k1gNnSc6	zběhnutí
putoval	putovat	k5eAaImAgInS	putovat
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
klavíristou	klavírista	k1gMnSc7	klavírista
Daliborem	Dalibor	k1gMnSc7	Dalibor
Ptákem	pták	k1gMnSc7	pták
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
kabaretních	kabaretní	k2eAgInPc6d1	kabaretní
výstupech	výstup	k1gInPc6	výstup
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
<g/>
,	,	kIx,	,
po	po	k7c6	po
českém	český	k2eAgInSc6d1	český
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
však	však	k9	však
chycen	chytit	k5eAaPmNgMnS	chytit
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
amnestii	amnestie	k1gFnSc6	amnestie
si	se	k3xPyFc3	se
konec	konec	k1gInSc4	konec
války	válka	k1gFnSc2	válka
odbyl	odbýt	k5eAaPmAgMnS	odbýt
ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
kapele	kapela	k1gFnSc6	kapela
a	a	k8xC	a
opět	opět	k6eAd1	opět
příležitostně	příležitostně	k6eAd1	příležitostně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
pražských	pražský	k2eAgInPc6d1	pražský
kabaretech	kabaret	k1gInPc6	kabaret
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
převzal	převzít	k5eAaPmAgMnS	převzít
komické	komický	k2eAgFnSc2d1	komická
role	role	k1gFnSc2	role
v	v	k7c6	v
několika	několik	k4yIc6	několik
seriózních	seriózní	k2eAgNnPc6d1	seriózní
divadlech	divadlo	k1gNnPc6	divadlo
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Vinohradské	vinohradský	k2eAgNnSc1d1	Vinohradské
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
však	však	k9	však
stále	stále	k6eAd1	stále
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
kabaretech	kabaret	k1gInPc6	kabaret
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1923	[number]	k4	1923
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
divadla	divadlo	k1gNnSc2	divadlo
Rokoko	rokoko	k1gNnSc4	rokoko
</s>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
divadle	divadlo	k1gNnSc6	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Několik	několik	k4yIc4	několik
představení	představení	k1gNnPc2	představení
denně	denně	k6eAd1	denně
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
však	však	k9	však
nestíhal	stíhat	k5eNaImAgMnS	stíhat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
divadelní	divadelní	k2eAgMnPc1d1	divadelní
ředitelé	ředitel	k1gMnPc1	ředitel
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
ho	on	k3xPp3gNnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nezaměstná	zaměstnat	k5eNaPmIp3nS	zaměstnat
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
<g/>
,	,	kIx,	,
když	když	k8xS	když
už	už	k6eAd1	už
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
slavnějším	slavný	k2eAgMnSc7d2	slavnější
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
divadlo	divadlo	k1gNnSc4	divadlo
–	–	k?	–
Divadlo	divadlo	k1gNnSc1	divadlo
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Divadlo	divadlo	k1gNnSc1	divadlo
Komedie	komedie	k1gFnSc2	komedie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
první	první	k4xOgNnSc1	první
představení	představení	k1gNnSc1	představení
–	–	k?	–
večer	večer	k6eAd1	večer
drobných	drobný	k2eAgFnPc2d1	drobná
frašek	fraška	k1gFnPc2	fraška
–	–	k?	–
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1925	[number]	k4	1925
v	v	k7c6	v
sále	sál	k1gInSc6	sál
hotelu	hotel	k1gInSc2	hotel
Adria	Adrium	k1gNnSc2	Adrium
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
č.	č.	k?	č.
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
navázalo	navázat	k5eAaPmAgNnS	navázat
dramaturgicky	dramaturgicky	k6eAd1	dramaturgicky
i	i	k8xC	i
inscenačně	inscenačně	k6eAd1	inscenačně
na	na	k7c4	na
divadlo	divadlo	k1gNnSc4	divadlo
Rokoko	rokoko	k1gNnSc1	rokoko
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1928	[number]	k4	1928
se	se	k3xPyFc4	se
DVB	DVB	kA	DVB
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
do	do	k7c2	do
Švandova	Švandův	k2eAgNnSc2d1	Švandovo
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
(	(	kIx(	(
<g/>
v	v	k7c6	v
Adrii	Adrie	k1gFnSc6	Adrie
začalo	začít	k5eAaPmAgNnS	začít
působit	působit	k5eAaImF	působit
Osvobozené	osvobozený	k2eAgNnSc4d1	osvobozené
divadlo	divadlo	k1gNnSc4	divadlo
V	V	kA	V
+	+	kIx~	+
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
DVB	DVB	kA	DVB
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
nově	nově	k6eAd1	nově
vybudovaného	vybudovaný	k2eAgInSc2d1	vybudovaný
paláce	palác	k1gInSc2	palác
Báňské	báňský	k2eAgFnSc2d1	báňská
a	a	k8xC	a
hutní	hutní	k2eAgFnSc2d1	hutní
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c6	v
Lazarské	Lazarský	k2eAgFnSc6d1	Lazarská
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
Burianovo	Burianův	k2eAgNnSc1d1	Burianovo
divadlo	divadlo	k1gNnSc1	divadlo
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
nehrálo	hrát	k5eNaImAgNnS	hrát
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
už	už	k6eAd1	už
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Joseph	Joseph	k1gInSc1	Joseph
Goebbels	Goebbelsa	k1gFnPc2	Goebbelsa
nechal	nechat	k5eAaPmAgInS	nechat
uzavřít	uzavřít	k5eAaPmF	uzavřít
všechna	všechen	k3xTgNnPc4	všechen
divadla	divadlo	k1gNnPc4	divadlo
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
ozbrojenou	ozbrojený	k2eAgFnSc7d1	ozbrojená
revoluční	revoluční	k2eAgFnSc7d1	revoluční
závodní	závodní	k2eAgFnSc7d1	závodní
gardou	garda	k1gFnSc7	garda
a	a	k8xC	a
V.	V.	kA	V.
Burian	Burian	k1gMnSc1	Burian
byl	být	k5eAaImAgInS	být
zatčen	zatčen	k2eAgInSc4d1	zatčen
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
pro	pro	k7c4	pro
údajnou	údajný	k2eAgFnSc4d1	údajná
kolaboraci	kolaborace	k1gFnSc4	kolaborace
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
.	.	kIx.	.
"	"	kIx"	"
<g/>
Znárodněné	znárodněný	k2eAgNnSc1d1	znárodněné
<g/>
"	"	kIx"	"
divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Divadlo	divadlo	k1gNnSc4	divadlo
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
šéfem	šéf	k1gMnSc7	šéf
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podlipný	Podlipný	k1gMnSc1	Podlipný
<g/>
.	.	kIx.	.
</s>
<s>
Burian	Burian	k1gMnSc1	Burian
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Báňské	báňský	k2eAgFnSc2d1	báňská
a	a	k8xC	a
hutní	hutní	k2eAgFnSc2d1	hutní
společnosti	společnost	k1gFnSc2	společnost
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
nejen	nejen	k6eAd1	nejen
prosperující	prosperující	k2eAgNnSc4d1	prosperující
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taktéž	taktéž	k?	taktéž
dílny	dílna	k1gFnSc2	dílna
<g/>
,	,	kIx,	,
foyer	foyer	k1gInSc4	foyer
<g/>
,	,	kIx,	,
ateliér	ateliér	k1gInSc4	ateliér
<g/>
,	,	kIx,	,
módní	módní	k2eAgInSc4d1	módní
salón	salón	k1gInSc4	salón
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
vlastní	vlastní	k2eAgNnSc4d1	vlastní
kino	kino	k1gNnSc4	kino
Vlasta	Vlasta	k1gFnSc1	Vlasta
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
"	"	kIx"	"
<g/>
Cinema	Cinema	k1gFnSc1	Cinema
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
se	se	k3xPyFc4	se
promítaly	promítat	k5eAaImAgFnP	promítat
Burianovy	Burianův	k2eAgFnPc1d1	Burianova
starší	starý	k2eAgFnPc1d2	starší
i	i	k8xC	i
novější	nový	k2eAgInPc1d2	novější
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
také	také	k9	také
němé	němý	k2eAgInPc4d1	němý
i	i	k8xC	i
zvukové	zvukový	k2eAgInPc4d1	zvukový
grotesky	grotesk	k1gInPc4	grotesk
<g/>
,	,	kIx,	,
kreslené	kreslený	k2eAgInPc4d1	kreslený
filmy	film	k1gInPc4	film
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
také	také	k9	také
vydávalo	vydávat	k5eAaPmAgNnS	vydávat
vlastní	vlastní	k2eAgInSc4d1	vlastní
časopis	časopis	k1gInSc4	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
divadlem	divadlo	k1gNnSc7	divadlo
prošlo	projít	k5eAaPmAgNnS	projít
mnoho	mnoho	k6eAd1	mnoho
skvělých	skvělý	k2eAgInPc2d1	skvělý
herců	herc	k1gInPc2	herc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
<g/>
,	,	kIx,	,
Čeněk	Čeněk	k1gMnSc1	Čeněk
Šlégl	Šlégl	k1gMnSc1	Šlégl
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Noll	Noll	k1gMnSc1	Noll
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Plachta	plachta	k1gFnSc1	plachta
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Trégl	Trégl	k1gMnSc1	Trégl
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
zde	zde	k6eAd1	zde
vystupovaly	vystupovat	k5eAaImAgInP	vystupovat
také	také	k9	také
např.	např.	kA	např.
Lída	Lída	k1gFnSc1	Lída
Baarová	Baarová	k1gFnSc1	Baarová
<g/>
,	,	kIx,	,
Meda	Med	k2eAgFnSc1d1	Meda
Valentová	Valentová	k1gFnSc1	Valentová
<g/>
,	,	kIx,	,
Andula	Andula	k1gFnSc1	Andula
Sedláčková	Sedláčková	k1gFnSc1	Sedláčková
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
===	===	k?	===
</s>
</p>
<p>
<s>
Točil	točit	k5eAaImAgInS	točit
také	také	k9	také
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
němých	němý	k2eAgInPc6d1	němý
a	a	k8xC	a
36	[number]	k4	36
zvukových	zvukový	k2eAgFnPc2d1	zvuková
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
komedie	komedie	k1gFnPc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
točil	točit	k5eAaImAgMnS	točit
filmy	film	k1gInPc4	film
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Karlem	Karel	k1gMnSc7	Karel
Lamačem	lamač	k1gMnSc7	lamač
<g/>
:	:	kIx,	:
C.	C.	kA	C.
a	a	k8xC	a
k.	k.	k?	k.
polní	polní	k2eAgMnSc1d1	polní
maršálek	maršálek	k1gMnSc1	maršálek
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
neznáte	neznat	k5eAaImIp2nP	neznat
Hadimršku	hadimrška	k1gFnSc4	hadimrška
<g/>
,	,	kIx,	,
Lelíček	lelíček	k1gInSc4	lelíček
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
<g/>
,	,	kIx,	,
Ducháček	Ducháček	k1gMnSc1	Ducháček
to	ten	k3xDgNnSc4	ten
zařídí	zařídit	k5eAaPmIp3nS	zařídit
<g/>
,	,	kIx,	,
U	u	k7c2	u
pokladny	pokladna	k1gFnSc2	pokladna
stál	stát	k5eAaImAgInS	stát
<g/>
...	...	k?	...
Když	když	k8xS	když
Lamač	lamač	k1gMnSc1	lamač
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
před	před	k7c7	před
hrozícím	hrozící	k2eAgNnSc7d1	hrozící
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc2	jeho
"	"	kIx"	"
<g/>
dvorním	dvorní	k2eAgMnSc7d1	dvorní
<g/>
"	"	kIx"	"
režisérem	režisér	k1gMnSc7	režisér
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
<g/>
:	:	kIx,	:
Anton	Anton	k1gMnSc1	Anton
Špelec	Špelec	k1gMnSc1	Špelec
<g/>
,	,	kIx,	,
ostrostřelec	ostrostřelec	k1gMnSc1	ostrostřelec
<g/>
,	,	kIx,	,
Tři	tři	k4xCgNnPc4	tři
vejce	vejce	k1gNnPc4	vejce
do	do	k7c2	do
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
Katakomby	katakomby	k1gFnPc4	katakomby
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
.	.	kIx.	.
</s>
<s>
Točil	točit	k5eAaImAgMnS	točit
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
předními	přední	k2eAgMnPc7d1	přední
českými	český	k2eAgMnPc7d1	český
režiséry	režisér	k1gMnPc7	režisér
<g/>
:	:	kIx,	:
s	s	k7c7	s
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Cikánem	cikán	k1gMnSc7	cikán
(	(	kIx(	(
<g/>
Hrdinný	hrdinný	k2eAgMnSc1d1	hrdinný
kapitán	kapitán	k1gMnSc1	kapitán
Korkorán	Korkorán	k2eAgMnSc1d1	Korkorán
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Slavínským	Slavínský	k2eAgMnSc7d1	Slavínský
(	(	kIx(	(
<g/>
Zlaté	zlatý	k2eAgNnSc4d1	Zlaté
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Svitákem	Sviták	k1gMnSc7	Sviták
(	(	kIx(	(
<g/>
Přednosta	přednosta	k1gMnSc1	přednosta
stanice	stanice	k1gFnSc2	stanice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
společně	společně	k6eAd1	společně
s	s	k7c7	s
Čeňkem	Čeněk	k1gMnSc7	Čeněk
Šléglem	Šlégl	k1gMnSc7	Šlégl
a	a	k8xC	a
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Bromem	brom	k1gInSc7	brom
režíroval	režírovat	k5eAaImAgMnS	režírovat
komedii	komedie	k1gFnSc4	komedie
Ulice	ulice	k1gFnSc2	ulice
zpívá	zpívat	k5eAaImIp3nS	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
filmy	film	k1gInPc7	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
převážně	převážně	k6eAd1	převážně
míval	mívat	k5eAaImAgMnS	mívat
spíše	spíše	k9	spíše
nenáročné	náročný	k2eNgFnPc4d1	nenáročná
komediální	komediální	k2eAgFnPc4d1	komediální
role	role	k1gFnPc4	role
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vymykají	vymykat	k5eAaImIp3nP	vymykat
filmy	film	k1gInPc1	film
U	u	k7c2	u
snědeného	snědený	k2eAgInSc2d1	snědený
krámu	krám	k1gInSc2	krám
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Ignáta	Ignát	k1gMnSc2	Ignát
Herrmanna	Herrmann	k1gMnSc2	Herrmann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Revizor	revizor	k1gMnSc1	revizor
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
N.	N.	kA	N.
V.	V.	kA	V.
Gogola	Gogol	k1gMnSc2	Gogol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
jako	jako	k8xS	jako
charakterní	charakterní	k2eAgMnSc1d1	charakterní
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pěti	pět	k4xCc2	pět
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
filmů	film	k1gInPc2	film
natočil	natočit	k5eAaBmAgMnS	natočit
německé	německý	k2eAgFnPc4d1	německá
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
promítány	promítat	k5eAaImNgFnP	promítat
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
jeho	jeho	k3xOp3gMnSc1	jeho
film	film	k1gInSc4	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
česko-polské	českoolský	k2eAgFnSc6d1	česko-polská
koprodukci	koprodukce	k1gFnSc6	koprodukce
(	(	kIx(	(
<g/>
Dvanáct	dvanáct	k4xCc4	dvanáct
křesel	křeslo	k1gNnPc2	křeslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
film	film	k1gInSc4	film
U	u	k7c2	u
snědeného	snědený	k2eAgInSc2d1	snědený
krámu	krám	k1gInSc2	krám
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
Českého	český	k2eAgInSc2d1	český
filmového	filmový	k2eAgInSc2d1	filmový
zpravodaje	zpravodaj	k1gInSc2	zpravodaj
Bílou	bílý	k2eAgFnSc4d1	bílá
stuhu	stuha	k1gFnSc4	stuha
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
získal	získat	k5eAaPmAgMnS	získat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
herectví	herectví	k1gNnSc4	herectví
Státní	státní	k2eAgFnSc4d1	státní
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmových	filmový	k2eAgInPc6d1	filmový
ateliérech	ateliér	k1gInPc6	ateliér
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
živlu	živel	k1gInSc6	živel
<g/>
,	,	kIx,	,
neuznával	uznávat	k5eNaImAgInS	uznávat
autoritu	autorita	k1gFnSc4	autorita
režiséra	režisér	k1gMnSc2	režisér
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
říkal	říkat	k5eAaImAgMnS	říkat
po	po	k7c6	po
každém	každý	k3xTgInSc6	každý
záběru	záběr	k1gInSc6	záběr
"	"	kIx"	"
<g/>
stop	stop	k1gInSc1	stop
<g/>
"	"	kIx"	"
a	a	k8xC	a
libovolně	libovolně	k6eAd1	libovolně
si	se	k3xPyFc3	se
improvizoval	improvizovat	k5eAaImAgMnS	improvizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ho	on	k3xPp3gMnSc4	on
dokázal	dokázat	k5eAaPmAgMnS	dokázat
trochu	trochu	k6eAd1	trochu
zkrotit	zkrotit	k5eAaPmF	zkrotit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Frič	Frič	k1gMnSc1	Frič
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
také	také	k9	také
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
roli	role	k1gFnSc4	role
dostával	dostávat	k5eAaImAgMnS	dostávat
honorář	honorář	k1gInSc4	honorář
přesahující	přesahující	k2eAgInSc4d1	přesahující
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInSc4	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
velké	velký	k2eAgFnPc1d1	velká
filmové	filmový	k2eAgFnPc1d1	filmová
dámské	dámský	k2eAgFnPc1d1	dámská
hvězdy	hvězda	k1gFnPc1	hvězda
(	(	kIx(	(
<g/>
Lída	Lída	k1gFnSc1	Lída
Baarová	Baarová	k1gFnSc1	Baarová
<g/>
,	,	kIx,	,
Adina	Adina	k1gFnSc1	Adina
Mandlová	mandlový	k2eAgFnSc1d1	Mandlová
<g/>
,	,	kIx,	,
Nataša	Nataša	k1gFnSc1	Nataša
Gollová	Gollová	k1gFnSc1	Gollová
či	či	k8xC	či
Věra	Věra	k1gFnSc1	Věra
Ferbasová	Ferbasový	k2eAgFnSc1d1	Ferbasová
<g/>
)	)	kIx)	)
dostávaly	dostávat	k5eAaImAgFnP	dostávat
za	za	k7c4	za
roli	role	k1gFnSc4	role
pouze	pouze	k6eAd1	pouze
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
některé	některý	k3yIgInPc1	některý
filmy	film	k1gInPc1	film
přímo	přímo	k6eAd1	přímo
vycházely	vycházet	k5eAaImAgInP	vycházet
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
divadelních	divadelní	k2eAgNnPc2d1	divadelní
představení	představení	k1gNnPc2	představení
<g/>
.	.	kIx.	.
</s>
<s>
Mnoha	mnoho	k4c3	mnoho
filmům	film	k1gInPc3	film
bylo	být	k5eAaImAgNnS	být
kritikou	kritika	k1gFnSc7	kritika
vytýkáno	vytýkán	k2eAgNnSc4d1	vytýkáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
filmy	film	k1gInPc4	film
jednoho	jeden	k4xCgMnSc4	jeden
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ostatní	ostatní	k2eAgMnPc1d1	ostatní
herci	herec	k1gMnPc1	herec
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
stíny	stín	k1gInPc7	stín
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Burianovi	Burianův	k2eAgMnPc1d1	Burianův
nemohou	moct	k5eNaImIp3nP	moct
konkurovat	konkurovat	k5eAaImF	konkurovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Ducháček	Ducháček	k1gMnSc1	Ducháček
to	ten	k3xDgNnSc4	ten
zařídí	zařídit	k5eAaPmIp3nS	zařídit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc7	první
poválečnou	poválečný	k2eAgFnSc7d1	poválečná
rolí	role	k1gFnSc7	role
byl	být	k5eAaImAgMnS	být
kostelník	kostelník	k1gMnSc1	kostelník
Kodýtek	Kodýtek	k1gMnSc1	Kodýtek
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Slepice	slepice	k1gFnSc2	slepice
a	a	k8xC	a
kostelník	kostelník	k1gMnSc1	kostelník
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přišly	přijít	k5eAaPmAgFnP	přijít
tři	tři	k4xCgFnPc1	tři
role	role	k1gFnPc1	role
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
místy	místy	k6eAd1	místy
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
svým	svůj	k3xOyFgInPc3	svůj
dřívějším	dřívější	k2eAgInPc3d1	dřívější
výkonům	výkon	k1gInPc3	výkon
<g/>
:	:	kIx,	:
poštmistr	poštmistr	k1gMnSc1	poštmistr
Plíšek	plíšek	k1gInSc4	plíšek
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
rádce	rádce	k1gMnSc2	rádce
Atakdále	Atakdála	k1gFnSc6	Atakdála
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Byl	být	k5eAaImAgInS	být
jednou	jednou	k6eAd1	jednou
jeden	jeden	k4xCgMnSc1	jeden
král	král	k1gMnSc1	král
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
svým	svůj	k3xOyFgInSc7	svůj
hlasem	hlas	k1gInSc7	hlas
účinkoval	účinkovat	k5eAaImAgInS	účinkovat
ještě	ještě	k9	ještě
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Werichem	Werich	k1gMnSc7	Werich
v	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
filmu	film	k1gInSc6	film
Dva	dva	k4xCgMnPc1	dva
mrazíci	mrazík	k1gMnPc1	mrazík
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sylvestr	Sylvestr	k1gInSc1	Sylvestr
Čáp	Čáp	k1gMnSc1	Čáp
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Muž	muž	k1gMnSc1	muž
v	v	k7c6	v
povětří	povětří	k1gNnSc6	povětří
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
rolí	role	k1gFnSc7	role
byl	být	k5eAaImAgMnS	být
účetní	účetní	k1gMnSc1	účetní
Dušek	Dušek	k1gMnSc1	Dušek
v	v	k7c6	v
satirickém	satirický	k2eAgInSc6d1	satirický
filmu	film	k1gInSc6	film
Zaostřit	zaostřit	k5eAaPmF	zaostřit
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
Burianova	Burianův	k2eAgNnSc2d1	Burianovo
herectví	herectví	k1gNnSc2	herectví
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
divadelního	divadelní	k2eAgInSc2d1	divadelní
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
herectví	herectví	k1gNnSc1	herectví
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
improvizaci	improvizace	k1gFnSc6	improvizace
<g/>
,	,	kIx,	,
černém	černý	k2eAgInSc6d1	černý
i	i	k8xC	i
laskavém	laskavý	k2eAgInSc6d1	laskavý
humoru	humor	k1gInSc6	humor
a	a	k8xC	a
satiře	satira	k1gFnSc6	satira
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
ztvárnit	ztvárnit	k5eAaPmF	ztvárnit
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
role	role	k1gFnPc4	role
všemožných	všemožný	k2eAgNnPc2d1	všemožné
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
a	a	k8xC	a
charakterů	charakter	k1gInPc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Burian	Burian	k1gMnSc1	Burian
byl	být	k5eAaImAgMnS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgMnSc1d1	lidový
svou	svůj	k3xOyFgFnSc7	svůj
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
i	i	k8xC	i
lidskou	lidský	k2eAgFnSc7d1	lidská
mentalitou	mentalita	k1gFnSc7	mentalita
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgInSc7	svůj
bezprostředním	bezprostřední	k2eAgInSc7d1	bezprostřední
<g/>
,	,	kIx,	,
spontánně	spontánně	k6eAd1	spontánně
nekomplikovaným	komplikovaný	k2eNgInSc7d1	nekomplikovaný
humorem	humor	k1gInSc7	humor
i	i	k8xC	i
mimořádným	mimořádný	k2eAgInSc7d1	mimořádný
darem	dar	k1gInSc7	dar
okamžitě	okamžitě	k6eAd1	okamžitě
parodovat	parodovat	k5eAaImF	parodovat
všechno	všechen	k3xTgNnSc4	všechen
patetické	patetický	k2eAgNnSc4d1	patetické
<g/>
,	,	kIx,	,
vznešené	vznešený	k2eAgNnSc4d1	vznešené
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
aristokratické	aristokratický	k2eAgMnPc4d1	aristokratický
<g/>
"	"	kIx"	"
i	i	k9	i
snobské	snobský	k2eAgNnSc1d1	snobské
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
byl	být	k5eAaImAgInS	být
blízký	blízký	k2eAgInSc1d1	blízký
plebejskému	plebejský	k2eAgInSc3d1	plebejský
pohledu	pohled	k1gInSc3	pohled
nejširších	široký	k2eAgFnPc2d3	nejširší
lidových	lidový	k2eAgFnPc2d1	lidová
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
taneční	taneční	k1gFnPc1	taneční
<g/>
,	,	kIx,	,
akrobaticko-klaunské	akrobatickolaunský	k2eAgFnPc1d1	akrobaticko-klaunský
pohybové	pohybový	k2eAgFnPc4d1	pohybová
schopnosti	schopnost	k1gFnPc4	schopnost
a	a	k8xC	a
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
tréninkem	trénink	k1gInSc7	trénink
získané	získaný	k2eAgFnSc2d1	získaná
dovednosti	dovednost	k1gFnSc2	dovednost
i	i	k8xC	i
neobyčejné	obyčejný	k2eNgNnSc4d1	neobyčejné
hlasové	hlasový	k2eAgNnSc4d1	hlasové
rozpětí	rozpětí	k1gNnSc4	rozpětí
tvořily	tvořit	k5eAaImAgInP	tvořit
základ	základ	k1gInSc4	základ
jeho	jeho	k3xOp3gNnPc2	jeho
klaunských	klaunský	k2eAgNnPc2d1	klaunské
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živé	k1gNnSc1	živé
<g/>
,	,	kIx,	,
dynamické	dynamický	k2eAgNnSc1d1	dynamické
až	až	k8xS	až
nespoutané	spoutaný	k2eNgNnSc1d1	nespoutané
herectví	herectví	k1gNnSc1	herectví
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
neustálou	neustálý	k2eAgFnSc7d1	neustálá
improvizací	improvizace	k1gFnSc7	improvizace
<g/>
.	.	kIx.	.
</s>
<s>
Hereckou	herecký	k2eAgFnSc4d1	herecká
postavu	postava	k1gFnSc4	postava
chápal	chápat	k5eAaImAgMnS	chápat
jako	jako	k8xC	jako
montáž	montáž	k1gFnSc4	montáž
proměn	proměna	k1gFnPc2	proměna
<g/>
,	,	kIx,	,
etud	etuda	k1gFnPc2	etuda
a	a	k8xC	a
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Nerespektoval	respektovat	k5eNaImAgInS	respektovat
text	text	k1gInSc1	text
daný	daný	k2eAgInSc1d1	daný
autorem	autor	k1gMnSc7	autor
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
improvizačním	improvizační	k2eAgNnSc7d1	improvizační
uměním	umění	k1gNnSc7	umění
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
každý	každý	k3xTgInSc1	každý
večer	večer	k1gInSc1	večer
dílo	dílo	k1gNnSc1	dílo
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnPc4d1	nová
<g/>
,	,	kIx,	,
neopakovatelné	opakovatelný	k2eNgFnPc4d1	neopakovatelná
a	a	k8xC	a
neustále	neustále	k6eAd1	neustále
překvapoval	překvapovat	k5eAaImAgInS	překvapovat
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
kolegy	kolega	k1gMnPc4	kolega
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
udivoval	udivovat	k5eAaImAgMnS	udivovat
taktéž	taktéž	k?	taktéž
svou	svůj	k3xOyFgFnSc4	svůj
imitační	imitační	k2eAgFnSc4d1	imitační
schopností	schopnost	k1gFnSc7	schopnost
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgInS	dokázat
imitovat	imitovat	k5eAaBmF	imitovat
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
gramofon	gramofon	k1gInSc1	gramofon
i	i	k8xC	i
větrák	větrák	k1gInSc1	větrák
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
jednou	jeden	k4xCgFnSc7	jeden
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgInS	dokázat
imitovat	imitovat	k5eAaBmF	imitovat
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
zvuky	zvuk	k1gInPc4	zvuk
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
mohl	moct	k5eAaImAgMnS	moct
závidět	závidět	k5eAaImF	závidět
i	i	k9	i
"	"	kIx"	"
<g/>
strýček	strýček	k1gMnSc1	strýček
Jedlička	Jedlička	k1gMnSc1	Jedlička
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Popularita	popularita	k1gFnSc1	popularita
==	==	k?	==
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc4d1	populární
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
tvář	tvář	k1gFnSc4	tvář
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
využívány	využívat	k5eAaImNgInP	využívat
reklamou	reklama	k1gFnSc7	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Obchodníci	obchodník	k1gMnPc1	obchodník
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
i	i	k9	i
bonbóny	bonbón	k1gInPc4	bonbón
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Burianky	Burianka	k1gFnSc2	Burianka
<g/>
"	"	kIx"	"
a	a	k8xC	a
také	také	k9	také
nově	nově	k6eAd1	nově
vyšlechtěná	vyšlechtěný	k2eAgFnSc1d1	vyšlechtěná
růže	růže	k1gFnSc1	růže
–	–	k?	–
"	"	kIx"	"
<g/>
Růže	růže	k1gFnSc1	růže
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
<g/>
"	"	kIx"	"
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
nesla	nést	k5eAaImAgFnS	nést
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Kamkoli	kamkoli	k6eAd1	kamkoli
přijel	přijet	k5eAaPmAgInS	přijet
<g/>
,	,	kIx,	,
budil	budit	k5eAaImAgInS	budit
rozruch	rozruch	k1gInSc1	rozruch
–	–	k?	–
byl	být	k5eAaImAgInS	být
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
a	a	k8xC	a
slavný	slavný	k2eAgInSc4d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
Miloval	milovat	k5eAaImAgInS	milovat
rychlá	rychlý	k2eAgNnPc4d1	rychlé
auta	auto	k1gNnPc4	auto
<g/>
,	,	kIx,	,
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
uniformy	uniforma	k1gFnPc4	uniforma
<g/>
.	.	kIx.	.
</s>
<s>
Uniformy	uniforma	k1gFnPc1	uniforma
pak	pak	k6eAd1	pak
také	také	k9	také
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
sám	sám	k3xTgMnSc1	sám
nosil	nosit	k5eAaImAgInS	nosit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
taktéž	taktéž	k?	taktéž
příslušníkem	příslušník	k1gMnSc7	příslušník
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
za	za	k7c4	za
první	první	k4xOgFnPc4	první
republiky	republika	k1gFnPc4	republika
vyběhal	vyběhat	k5eAaPmAgMnS	vyběhat
a	a	k8xC	a
uplatil	uplatit	k5eAaPmAgMnS	uplatit
hodnost	hodnost	k1gFnSc4	hodnost
nadporučíka	nadporučík	k1gMnSc2	nadporučík
a	a	k8xC	a
podle	podle	k7c2	podle
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
pamětníků	pamětník	k1gMnPc2	pamětník
též	též	k6eAd1	též
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
kandidatuře	kandidatura	k1gFnSc6	kandidatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
své	svůj	k3xOyFgFnSc2	svůj
slávy	sláva	k1gFnSc2	sláva
měl	mít	k5eAaImAgInS	mít
pronajatý	pronajatý	k2eAgInSc1d1	pronajatý
zámeček	zámeček	k1gInSc1	zámeček
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
jezdil	jezdit	k5eAaImAgMnS	jezdit
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
však	však	k9	však
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
přilehlém	přilehlý	k2eAgInSc6d1	přilehlý
domě	dům	k1gInSc6	dům
správce	správce	k1gMnSc2	správce
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
Prahy	Praha	k1gFnSc2	Praha
měl	mít	k5eAaImAgInS	mít
statek	statek	k1gInSc1	statek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
choval	chovat	k5eAaImAgMnS	chovat
krávy	kráva	k1gFnSc2	kráva
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
měl	mít	k5eAaImAgInS	mít
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
podle	podle	k7c2	podle
postav	postava	k1gFnPc2	postava
ze	z	k7c2	z
známých	známý	k2eAgFnPc2d1	známá
oper	opera	k1gFnPc2	opera
(	(	kIx(	(
<g/>
Carmen	Carmen	k1gInSc1	Carmen
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
,	,	kIx,	,
Mařenka	Mařenka	k1gFnSc1	Mařenka
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnSc2	jméno
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
na	na	k7c6	na
cedulích	cedule	k1gFnPc6	cedule
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
měly	mít	k5eAaImAgFnP	mít
krávy	kráva	k1gFnPc1	kráva
připevněné	připevněný	k2eAgFnPc1d1	připevněná
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
bohatstvím	bohatství	k1gNnSc7	bohatství
se	se	k3xPyFc4	se
rád	rád	k6eAd1	rád
chlubil	chlubit	k5eAaImAgMnS	chlubit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolávalo	vyvolávat	k5eAaImAgNnS	vyvolávat
velkou	velký	k2eAgFnSc4d1	velká
závist	závist	k1gFnSc4	závist
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
jeho	jeho	k3xOp3gInSc2	jeho
poválečného	poválečný	k2eAgInSc2d1	poválečný
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Posmrtné	posmrtný	k2eAgNnSc4d1	posmrtné
uznání	uznání	k1gNnSc4	uznání
==	==	k?	==
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Dejvicích	Dejvice	k1gFnPc6	Dejvice
odhalena	odhalit	k5eAaPmNgFnS	odhalit
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
u	u	k7c2	u
Divadla	divadlo	k1gNnSc2	divadlo
Komedie	komedie	k1gFnSc2	komedie
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Divadlo	divadlo	k1gNnSc1	divadlo
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
<g/>
)	)	kIx)	)
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
pasáž	pasáž	k1gFnSc1	pasáž
a	a	k8xC	a
ve	v	k7c6	v
foyeru	foyer	k1gInSc6	foyer
odhalena	odhalit	k5eAaPmNgFnS	odhalit
busta	busta	k1gFnSc1	busta
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
pasáž	pasáž	k1gFnSc1	pasáž
u	u	k7c2	u
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
odhalena	odhalen	k2eAgFnSc1d1	odhalena
busta	busta	k1gFnSc1	busta
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
pamětní	pamětní	k2eAgFnSc2d1	pamětní
desky	deska	k1gFnSc2	deska
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
pražské	pražský	k2eAgInPc4d1	pražský
A.	A.	kA	A.
C.	C.	kA	C.
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Burian	Burian	k1gMnSc1	Burian
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
profesionálně	profesionálně	k6eAd1	profesionálně
chytal	chytat	k5eAaImAgInS	chytat
v	v	k7c6	v
bráně	brána	k1gFnSc6	brána
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
u	u	k7c2	u
stadionu	stadion	k1gInSc2	stadion
Viktorie	Viktoria	k1gFnSc2	Viktoria
Žižkov	Žižkov	k1gInSc1	Žižkov
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
voskových	voskový	k2eAgFnPc2d1	vosková
figurín	figurína	k1gFnPc2	figurína
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Celetné	Celetný	k2eAgFnSc6d1	Celetná
ulici	ulice	k1gFnSc6	ulice
odhalena	odhalit	k5eAaPmNgFnS	odhalit
jeho	jeho	k3xOp3gFnSc1	jeho
vosková	voskový	k2eAgFnSc1d1	vosková
podoba	podoba	k1gFnSc1	podoba
v	v	k7c6	v
převleku	převlek	k1gInSc6	převlek
C.	C.	kA	C.
a	a	k8xC	a
k.	k.	k?	k.
polního	polní	k2eAgMnSc4d1	polní
maršálka	maršálek	k1gMnSc4	maršálek
</s>
</p>
<p>
<s>
Na	na	k7c6	na
závodišti	závodiště	k1gNnSc6	závodiště
v	v	k7c6	v
Chuchli	Chuchle	k1gFnSc6	Chuchle
se	se	k3xPyFc4	se
opakovaně	opakovaně	k6eAd1	opakovaně
pořádala	pořádat	k5eAaImAgFnS	pořádat
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2002	[number]	k4	2002
byly	být	k5eAaImAgInP	být
ostatky	ostatek	k1gInPc1	ostatek
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Niny	Nina	k1gFnSc2	Nina
přeneseny	přenést	k5eAaPmNgFnP	přenést
z	z	k7c2	z
Vinohradského	vinohradský	k2eAgInSc2d1	vinohradský
hřbitova	hřbitov	k1gInSc2	hřbitov
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
hrobu	hrob	k1gInSc2	hrob
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
</s>
</p>
<p>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
o	o	k7c4	o
"	"	kIx"	"
<g/>
Krále	Král	k1gMnPc4	Král
komiků	komik	k1gMnPc2	komik
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
:	:	kIx,	:
Jiřina	Jiřina	k1gFnSc1	Jiřina
Bohdalová	Bohdalová	k1gFnSc1	Bohdalová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Největší	veliký	k2eAgMnSc1d3	veliký
Čech	Čech	k1gMnSc1	Čech
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
31	[number]	k4	31
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
udělována	udělován	k2eAgFnSc1d1	udělována
Cena	cena	k1gFnSc1	cena
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
brankářům	brankář	k1gMnPc3	brankář
Fortuna	Fortuna	k1gFnSc1	Fortuna
ligy	liga	k1gFnSc2	liga
ale	ale	k8xC	ale
také	také	k9	také
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
komikům	komik	k1gMnPc3	komik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
Burianově	Burianův	k2eAgFnSc6d1	Burianova
popularitě	popularita	k1gFnSc6	popularita
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
filmy	film	k1gInPc4	film
postupně	postupně	k6eAd1	postupně
vyšly	vyjít	k5eAaPmAgInP	vyjít
na	na	k7c4	na
VHS	VHS	kA	VHS
kazetách	kazeta	k1gFnPc6	kazeta
a	a	k8xC	a
DVD	DVD	kA	DVD
nosičích	nosič	k1gInPc6	nosič
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
reprízovány	reprízovat	k5eAaImNgInP	reprízovat
v	v	k7c6	v
televizích	televize	k1gFnPc6	televize
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
snímky	snímek	k1gInPc1	snímek
byly	být	k5eAaImAgInP	být
zrestaurovány	zrestaurovat	k5eAaPmNgInP	zrestaurovat
do	do	k7c2	do
vysokého	vysoký	k2eAgNnSc2d1	vysoké
rozlišení	rozlišení	k1gNnSc2	rozlišení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
životě	život	k1gInSc6	život
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
encyklopediích	encyklopedie	k1gFnPc6	encyklopedie
a	a	k8xC	a
životopisných	životopisný	k2eAgFnPc6d1	životopisná
knihách	kniha	k1gFnPc6	kniha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ČÁSLAVSKÝ	Čáslavský	k1gMnSc1	Čáslavský
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
:	:	kIx,	:
Filmový	filmový	k2eAgMnSc1d1	filmový
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakl	nakl	k1gInSc4	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Fragment	fragment	k1gInSc1	fragment
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7200-159-0	[number]	k4	80-7200-159-0
informace	informace	k1gFnSc1	informace
od	od	k7c2	od
nakladatele	nakladatel	k1gMnSc2	nakladatel
</s>
</p>
<p>
<s>
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
R.	R.	kA	R.
<g/>
:	:	kIx,	:
Byl	být	k5eAaImAgInS	být
jednou	jednou	k6eAd1	jednou
jeden	jeden	k4xCgMnSc1	jeden
Vlasta	Vlasta	k1gMnSc1	Vlasta
<g/>
...	...	k?	...
aneb	aneb	k?	aneb
o	o	k7c6	o
králi	král	k1gMnPc7	král
českých	český	k2eAgMnPc2d1	český
komiků	komik	k1gMnPc2	komik
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Magnet	magnet	k1gInSc1	magnet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85110-60-1	[number]	k4	80-85110-60-1
</s>
</p>
<p>
<s>
KOLEKTIV	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Česká	český	k2eAgNnPc1d1	české
divadla	divadlo	k1gNnPc1	divadlo
:	:	kIx,	:
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
divadelních	divadelní	k2eAgMnPc2d1	divadelní
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Divadelní	divadelní	k2eAgInSc1d1	divadelní
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
615	[number]	k4	615
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7008	[number]	k4	7008
<g/>
-	-	kIx~	-
<g/>
107	[number]	k4	107
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
3	[number]	k4	3
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
,	,	kIx,	,
53	[number]	k4	53
<g/>
,	,	kIx,	,
85	[number]	k4	85
<g/>
,	,	kIx,	,
95	[number]	k4	95
<g/>
,	,	kIx,	,
151	[number]	k4	151
<g/>
–	–	k?	–
<g/>
154	[number]	k4	154
<g/>
,	,	kIx,	,
176	[number]	k4	176
<g/>
,	,	kIx,	,
186	[number]	k4	186
<g/>
,	,	kIx,	,
199	[number]	k4	199
<g/>
,	,	kIx,	,
201	[number]	k4	201
<g/>
,	,	kIx,	,
205	[number]	k4	205
<g/>
,	,	kIx,	,
300	[number]	k4	300
<g/>
,	,	kIx,	,
369	[number]	k4	369
<g/>
,	,	kIx,	,
380	[number]	k4	380
<g/>
,	,	kIx,	,
381	[number]	k4	381
<g/>
,	,	kIx,	,
414	[number]	k4	414
<g/>
–	–	k?	–
<g/>
417	[number]	k4	417
<g/>
,	,	kIx,	,
428	[number]	k4	428
<g/>
,	,	kIx,	,
492	[number]	k4	492
<g/>
,	,	kIx,	,
518	[number]	k4	518
<g/>
,	,	kIx,	,
519	[number]	k4	519
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIKEJZ	FIKEJZ	kA	FIKEJZ
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
:	:	kIx,	:
herci	herec	k1gMnPc1	herec
a	a	k8xC	a
herečky	herečka	k1gFnPc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
K.	K.	kA	K.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
(	(	kIx(	(
<g/>
dotisk	dotisk	k1gInSc1	dotisk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
750	[number]	k4	750
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
332	[number]	k4	332
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
134	[number]	k4	134
<g/>
–	–	k?	–
<g/>
137	[number]	k4	137
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FRAIS	FRAIS	kA	FRAIS
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
a	a	k8xC	a
FRAIS	FRAIS	kA	FRAIS
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
:	:	kIx,	:
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
–	–	k?	–
Obrazový	obrazový	k2eAgInSc1d1	obrazový
životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Práh	práh	k1gInSc1	práh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Burian	Burian	k1gMnSc1	Burian
ve	v	k7c6	v
fotografiích	fotografia	k1gFnPc6	fotografia
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ze	z	k7c2	z
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
soukromí	soukromí	k1gNnSc2	soukromí
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86155-65-X	[number]	k4	80-86155-65-X
</s>
</p>
<p>
<s>
GÖTZOVÁ	GÖTZOVÁ	kA	GÖTZOVÁ
<g/>
,	,	kIx,	,
Joža	Joža	k1gFnSc1	Joža
<g/>
:	:	kIx,	:
Profily	profil	k1gInPc1	profil
českých	český	k2eAgInPc2d1	český
herců	herc	k1gInPc2	herc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
V.	V.	kA	V.
U.	U.	kA	U.
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
nedat	dat	k2eNgMnSc1d1	nedat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
str	str	kA	str
<g/>
.	.	kIx.	.
159	[number]	k4	159
<g/>
–	–	k?	–
<g/>
161	[number]	k4	161
</s>
</p>
<p>
<s>
JUST	just	k6eAd1	just
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
:	:	kIx,	:
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
:	:	kIx,	:
paradoxy	paradox	k1gInPc1	paradox
krále	král	k1gMnSc2	král
komiků	komik	k1gMnPc2	komik
<g/>
.	.	kIx.	.
</s>
<s>
Liberec	Liberec	k1gInSc1	Liberec
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
23	[number]	k4	23
s.	s.	k?	s.
</s>
</p>
<p>
<s>
JUST	just	k6eAd1	just
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
:	:	kIx,	:
Věc	věc	k1gFnSc1	věc
<g/>
:	:	kIx,	:
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Rozmluvy	rozmluva	k1gFnSc2	rozmluva
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
142	[number]	k4	142
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-85336-04-9	[number]	k4	80-85336-04-9
</s>
</p>
<p>
<s>
JUST	just	k6eAd1	just
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
:	:	kIx,	:
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
:	:	kIx,	:
mystérium	mystérium	k1gNnSc1	mystérium
smíchu	smích	k1gInSc2	smích
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
AVČR	AVČR	kA	AVČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
a	a	k8xC	a
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-200-0861-6	[number]	k4	80-200-0861-6
</s>
</p>
<p>
<s>
KAŠPAR	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
a	a	k8xC	a
filmaři	filmař	k1gMnPc1	filmař
za	za	k7c2	za
protektorátu	protektorát	k1gInSc2	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Propaganda	propaganda	k1gFnSc1	propaganda
<g/>
,	,	kIx,	,
kolaborace	kolaborace	k1gFnSc1	kolaborace
<g/>
,	,	kIx,	,
rezistence	rezistence	k1gFnSc1	rezistence
<g/>
,	,	kIx,	,
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7277-347-3	[number]	k4	978-80-7277-347-3
</s>
</p>
<p>
<s>
KRÁL	Král	k1gMnSc1	Král
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
<g/>
:	:	kIx,	:
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Orbis	orbis	k1gInSc1	orbis
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
KUDĚLKA	KUDĚLKA	kA	KUDĚLKA
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
:	:	kIx,	:
Hvězdy	hvězda	k1gFnPc1	hvězda
nad	nad	k7c7	nad
Barrandovem	Barrandov	k1gInSc7	Barrandov
:	:	kIx,	:
průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
starších	starý	k2eAgInPc6d2	starší
českých	český	k2eAgInPc6d1	český
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
Michala	Michal	k1gMnSc2	Michal
Ženíška	Ženíšek	k1gMnSc2	Ženíšek
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
289	[number]	k4	289
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-901887-6-1	[number]	k4	80-901887-6-1
</s>
</p>
<p>
<s>
LONGEN	LONGEN	kA	LONGEN
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Artur	Artur	k1gMnSc1	Artur
<g/>
:	:	kIx,	:
Král	Král	k1gMnSc1	Král
komiků	komik	k1gMnPc2	komik
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Melantrich	Melantrich	k1gInSc1	Melantrich
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
–	–	k?	–
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
85	[number]	k4	85
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PELC	Pelc	k1gMnSc1	Pelc
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
:	:	kIx,	:
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Mladá	mladá	k1gFnSc1	mladá
fronta	fronta	k1gFnSc1	fronta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
165	[number]	k4	165
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
"	"	kIx"	"
<g/>
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
divadlo	divadlo	k1gNnSc1	divadlo
neznámé	známý	k2eNgNnSc1d1	neznámé
<g/>
"	"	kIx"	"
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
na	na	k7c4	na
s.	s.	k?	s.
378	[number]	k4	378
<g/>
–	–	k?	–
<g/>
387	[number]	k4	387
společný	společný	k2eAgInSc1d1	společný
silvestrovský	silvestrovský	k2eAgInSc1d1	silvestrovský
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
skeč	skeč	k1gInSc1	skeč
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
a	a	k8xC	a
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
W	W	kA	W
Sanatorium	sanatorium	k1gNnSc1	sanatorium
doktora	doktor	k1gMnSc2	doktor
Hormona	Hormon	k1gMnSc2	Hormon
<g/>
,	,	kIx,	,
otištěný	otištěný	k2eAgInSc1d1	otištěný
z	z	k7c2	z
rukopisu	rukopis	k1gInSc2	rukopis
Jana	Jan	k1gMnSc2	Jan
Wericha	Werich	k1gMnSc2	Werich
<g/>
.	.	kIx.	.
</s>
<s>
Blíže	blíže	k1gFnSc1	blíže
in	in	k?	in
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
SUCHÝ	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
:	:	kIx,	:
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
–	–	k?	–
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Brána	brána	k1gFnSc1	brána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7243-218-4	[number]	k4	80-7243-218-4
informace	informace	k1gFnSc1	informace
od	od	k7c2	od
nakladatele	nakladatel	k1gMnSc2	nakladatel
</s>
</p>
<p>
<s>
SUCHÝ	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
;	;	kIx,	;
FARNÍK	farník	k1gMnSc1	farník
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
:	:	kIx,	:
Album	album	k1gNnSc1	album
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Ametyst	ametyst	k1gInSc1	ametyst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Burian	Burian	k1gMnSc1	Burian
ve	v	k7c6	v
fotografiích	fotografia	k1gFnPc6	fotografia
ze	z	k7c2	z
soukromí	soukromí	k1gNnSc2	soukromí
i	i	k9	i
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
CD	CD	kA	CD
"	"	kIx"	"
<g/>
Vlasta	Vlasta	k1gFnSc1	Vlasta
s	s	k7c7	s
kytkou	kytka	k1gFnSc7	kytka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85837-68-4	[number]	k4	80-85837-68-4
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
162	[number]	k4	162
<g/>
–	–	k?	–
<g/>
163	[number]	k4	163
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TUNYS	TUNYS	kA	TUNYS
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
:	:	kIx,	:
Hodně	hodně	k6eAd1	hodně
si	se	k3xPyFc3	se
pamatuju-	pamatuju-	k?	pamatuju-
Perličky	perlička	k1gFnPc1	perlička
v	v	k7c6	v
duši	duše	k1gFnSc6	duše
Raoula	Raoula	k1gFnSc1	Raoula
Schránila	schránit	k5eAaBmAgFnS	schránit
<g/>
.	.	kIx.	.
</s>
<s>
Raoula	Raoula	k1gFnSc1	Raoula
Schránila	schránit	k5eAaBmAgFnS	schránit
<g/>
,	,	kIx,	,
Ametyst	ametyst	k1gInSc1	ametyst
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
37	[number]	k4	37
<g/>
,	,	kIx,	,
49	[number]	k4	49
<g/>
–	–	k?	–
<g/>
53	[number]	k4	53
<g/>
,	,	kIx,	,
62	[number]	k4	62
<g/>
,	,	kIx,	,
75	[number]	k4	75
<g/>
,	,	kIx,	,
95	[number]	k4	95
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
,	,	kIx,	,
104	[number]	k4	104
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
109	[number]	k4	109
<g/>
,	,	kIx,	,
112	[number]	k4	112
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
125	[number]	k4	125
<g/>
,	,	kIx,	,
139	[number]	k4	139
<g/>
,	,	kIx,	,
140	[number]	k4	140
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85837-35-8	[number]	k4	80-85837-35-8
</s>
</p>
<p>
<s>
TUNYS	TUNYS	kA	TUNYS
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
:	:	kIx,	:
Otomar	Otomar	k1gMnSc1	Otomar
Korbelář	korbelář	k1gMnSc1	korbelář
<g/>
,	,	kIx,	,
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
XYZ	XYZ	kA	XYZ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
83	[number]	k4	83
<g/>
,	,	kIx,	,
148	[number]	k4	148
<g/>
,	,	kIx,	,
184	[number]	k4	184
<g/>
,	,	kIx,	,
244	[number]	k4	244
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7388-552-6	[number]	k4	978-80-7388-552-6
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
8	[number]	k4	8
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Brun	Brun	k1gMnSc1	Brun
<g/>
–	–	k?	–
<g/>
By	by	k9	by
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
225	[number]	k4	225
<g/>
–	–	k?	–
<g/>
368	[number]	k4	368
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
346	[number]	k4	346
<g/>
–	–	k?	–
<g/>
348	[number]	k4	348
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Slavní	slavný	k2eAgMnPc1d1	slavný
rodáci	rodák	k1gMnPc1	rodák
z	z	k7c2	z
Liberce	Liberec	k1gInSc2	Liberec
</s>
</p>
<p>
<s>
Kabaret	kabaret	k1gInSc1	kabaret
Červená	červený	k2eAgFnSc1d1	červená
sedma	sedma	k1gFnSc1	sedma
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
Čech	Čech	k1gMnSc1	Čech
</s>
</p>
<p>
<s>
Osvobozené	osvobozený	k2eAgNnSc1d1	osvobozené
divadlo	divadlo	k1gNnSc1	divadlo
neznámé	známý	k2eNgFnSc2d1	neznámá
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
v	v	k7c6	v
souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
<g/>
:	:	kIx,	:
Oběť	oběť	k1gFnSc1	oběť
povahy	povaha	k1gFnSc2	povaha
české	český	k2eAgFnSc2d1	Česká
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
společnosti	společnost	k1gFnSc2	společnost
Supraphon	supraphon	k1gInSc1	supraphon
</s>
</p>
<p>
<s>
Burianův	Burianův	k2eAgInSc1d1	Burianův
životopis	životopis	k1gInSc1	životopis
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yRnSc1	kdo
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
prvorepublikových	prvorepublikový	k2eAgInPc2d1	prvorepublikový
herců	herc	k1gInPc2	herc
–	–	k?	–
Vlasta	Vlasta	k1gFnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
</s>
</p>
<p>
<s>
O	o	k7c6	o
Burianově	Burianův	k2eAgFnSc6d1	Burianova
vile	vila	k1gFnSc6	vila
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
pražské	pražský	k2eAgFnSc2d1	Pražská
informační	informační	k2eAgFnSc2d1	informační
služby	služba	k1gFnSc2	služba
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Příběhy	příběh	k1gInPc1	příběh
slavných	slavný	k2eAgMnPc2d1	slavný
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
deseti	deset	k4xCc2	deset
Hrdinů	Hrdina	k1gMnPc2	Hrdina
s	s	k7c7	s
otazníkem	otazník	k1gInSc7	otazník
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
komiků	komik	k1gMnPc2	komik
král	král	k1gMnSc1	král
i	i	k8xC	i
mučedník	mučedník	k1gMnSc1	mučedník
Pořad	pořad	k1gInSc4	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některé	některý	k3yIgFnPc4	některý
málo	málo	k6eAd1	málo
známé	známý	k2eAgFnPc4d1	známá
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
životě	život	k1gInSc6	život
Vlasty	Vlasta	k1gMnSc2	Vlasta
Buriana	Burian	k1gMnSc2	Burian
<g/>
.	.	kIx.	.
</s>
</p>
