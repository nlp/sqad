<s>
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Josef	Josef	k1gMnSc1	Josef
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Burian	Burian	k1gMnSc1	Burian
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1891	[number]	k4	1891
Liberec	Liberec	k1gInSc1	Liberec
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1962	[number]	k4	1962
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Král	Král	k1gMnSc1	Král
komiků	komik	k1gMnPc2	komik
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
mim	mim	k1gMnSc1	mim
a	a	k8xC	a
imitátor	imitátor	k1gMnSc1	imitátor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
nespoutané	spoutaný	k2eNgFnSc3d1	nespoutaná
živelnosti	živelnost	k1gFnSc3	živelnost
a	a	k8xC	a
potřebě	potřeba	k1gFnSc3	potřeba
být	být	k5eAaImF	být
všude	všude	k6eAd1	všude
první	první	k4xOgInSc4	první
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
mezi	mezi	k7c4	mezi
skutečné	skutečný	k2eAgFnPc4d1	skutečná
hvězdy	hvězda	k1gFnPc4	hvězda
českého	český	k2eAgInSc2d1	český
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
i	i	k8xC	i
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
