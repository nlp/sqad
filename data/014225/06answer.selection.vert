<s>
Sídlí	sídlet	k5eAaImIp3nS
zde	zde	k6eAd1
ředitelství	ředitelství	k1gNnSc4
společností	společnost	k1gFnPc2
Ivoclar	Ivoclar	k1gMnSc1
Vivadent	Vivadent	k1gMnSc1
AG	AG	kA
<g/>
,	,	kIx,
největšího	veliký	k2eAgMnSc2d3
výrobce	výrobce	k1gMnSc2
zubních	zubní	k2eAgFnPc2d1
protéz	protéza	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
Hilti	Hilti	k1gNnSc1
Aktiengesellschaft	Aktiengesellschafta	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
významným	významný	k2eAgMnSc7d1
výrobcem	výrobce	k1gMnSc7
elektrického	elektrický	k2eAgNnSc2d1
ručního	ruční	k2eAgNnSc2d1
nářadí	nářadí	k1gNnSc2
a	a	k8xC
předním	přední	k2eAgMnSc7d1
dodavatelem	dodavatel	k1gMnSc7
produktů	produkt	k1gInPc2
do	do	k7c2
stavebnictví	stavebnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>