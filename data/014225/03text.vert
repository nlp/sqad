<s>
Schaan	Schaan	k1gMnSc1
</s>
<s>
Schaan	Schaan	k1gInSc1
Kostel	kostel	k1gInSc1
v	v	k7c4
Schaan	Schaan	k1gInSc4
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
47	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
9	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
40	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
450	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
</s>
<s>
Schaan	Schaan	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
26,8	26,8	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
6	#num#	k4
039	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
225,3	225,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Oficiální	oficiální	k2eAgFnSc1d1
web	web	k1gInSc4
</s>
<s>
www.schaan.li	www.schaan.l	k1gMnPc1
PSČ	PSČ	kA
</s>
<s>
9494	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Schaan	Schaan	k1gInSc1
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
v	v	k7c6
Lichtenštejnsku	Lichtenštejnsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
země	zem	k1gFnSc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
2	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Vaduzu	Vaduz	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
tvoří	tvořit	k5eAaImIp3nS
souměstí	souměstí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Schaan	Schaan	k1gInSc1
je	být	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc7d1
dopravní	dopravní	k2eAgFnSc7d1
křižovatkou	křižovatka	k1gFnSc7
a	a	k8xC
průmyslovým	průmyslový	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
přes	přes	k7c4
6039	#num#	k4
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
tak	tak	k6eAd1
největší	veliký	k2eAgFnSc7d3
obcí	obec	k1gFnSc7
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
západním	západní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
obce	obec	k1gFnSc2
protéká	protékat	k5eAaImIp3nS
Rýn	Rýn	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zde	zde	k6eAd1
tvoří	tvořit	k5eAaImIp3nS
lichtenštejnsko-švýcarskou	lichtenštejnsko-švýcarský	k2eAgFnSc4d1
přirozenou	přirozený	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obcí	obec	k1gFnPc2
prochází	procházet	k5eAaImIp3nS
železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
z	z	k7c2
rakouského	rakouský	k2eAgInSc2d1
Feldkirchu	Feldkirch	k1gInSc2
do	do	k7c2
švýcarského	švýcarský	k2eAgInSc2d1
Buchsu	Buchs	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS
zde	zde	k6eAd1
ředitelství	ředitelství	k1gNnSc4
společností	společnost	k1gFnPc2
Ivoclar	Ivoclar	k1gMnSc1
Vivadent	Vivadent	k1gMnSc1
AG	AG	kA
<g/>
,	,	kIx,
největšího	veliký	k2eAgMnSc2d3
výrobce	výrobce	k1gMnSc2
zubních	zubní	k2eAgFnPc2d1
protéz	protéza	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
Hilti	Hilti	k1gNnSc1
Aktiengesellschaft	Aktiengesellschafta	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
významným	významný	k2eAgMnSc7d1
výrobcem	výrobce	k1gMnSc7
elektrického	elektrický	k2eAgNnSc2d1
ručního	ruční	k2eAgNnSc2d1
nářadí	nářadí	k1gNnSc2
a	a	k8xC
předním	přední	k2eAgMnSc7d1
dodavatelem	dodavatel	k1gMnSc7
produktů	produkt	k1gInPc2
do	do	k7c2
stavebnictví	stavebnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1893	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelní	kostelní	k2eAgFnSc1d1
věž	věž	k1gFnSc1
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
81	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
6	#num#	k4
zvonů	zvon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2003	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
byl	být	k5eAaImAgInS
kostel	kostel	k1gInSc1
rekonstruován	rekonstruovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavbu	výstavba	k1gFnSc4
kostela	kostel	k1gInSc2
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
financoval	financovat	k5eAaBmAgMnS
kníže	kníže	k1gMnSc1
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
Lichtenštejna	Lichtenštejn	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zde	zde	k6eAd1
má	mít	k5eAaImIp3nS
památník	památník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Základy	základ	k1gInPc1
římské	římský	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
ze	z	k7c2
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Petra	Petr	k1gMnSc2
</s>
<s>
Radnice	radnice	k1gFnSc1
a	a	k8xC
muzeum	muzeum	k1gNnSc1
z	z	k7c2
let	léto	k1gNnPc2
1844	#num#	k4
<g/>
–	–	k?
<g/>
1846	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
sloužila	sloužit	k5eAaImAgFnS
budova	budova	k1gFnSc1
jako	jako	k8xC,k8xS
škola	škola	k1gFnSc1
</s>
<s>
Klášter	klášter	k1gInSc1
sv.	sv.	kA
Alžběty	Alžběta	k1gFnSc2
řádu	řád	k1gInSc2
sester	sestra	k1gFnPc2
Ctitelek	ctitelka	k1gFnPc2
krve	krev	k1gFnSc2
Kristovy	Kristův	k2eAgFnSc2d1
(	(	kIx(
<g/>
ASC	ASC	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Schaan	Schaana	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
948495	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4367460-4	4367460-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
98068251	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
236984171	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
98068251	#num#	k4
</s>
<s>
↑	↑	k?
https://www.llv.li/files/as/bevolkerungsstatistik-vorlaufige-ergebnisse-31-dezember-2019.pdf	https://www.llv.li/files/as/bevolkerungsstatistik-vorlaufige-ergebnisse-31-dezember-2019.pdf	k1gInSc1
</s>
