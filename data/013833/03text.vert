<s>
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Rettig	Rettig	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Rettig	Rettig	k1gMnSc1
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Rettig	Rettiga	k1gFnPc2
Narození	narození	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1821	#num#	k4
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c4
OrlicíRakouské	OrlicíRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1871	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
50	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Nepomuk	Nepomuk	k1gInSc1
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Povolání	povolání	k1gNnSc5
</s>
<s>
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
<g/>
,	,	kIx,
katolický	katolický	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
přírodovědec	přírodovědec	k1gMnSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
a	a	k8xC
učitel	učitel	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Jan	Jan	k1gMnSc1
Alois	Alois	k1gMnSc1
Sudiprav	Sudiprava	k1gFnPc2
Rettig	Rettig	k1gMnSc1
otec	otec	k1gMnSc1
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Dobromila	Dobromila	k1gFnSc1
Rettigová	Rettigový	k2eAgFnSc1d1
matka	matka	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Jindřiška	Jindřiška	k1gFnSc1
Rettigová	Rettigový	k2eAgFnSc1d1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslav	k1gMnSc1
Rettig	Rettig	k1gMnSc1xF
<g/>
,	,	kIx,
Sch	Sch	kA
<g/>
.	.	kIx.
<g/>
P.	P.	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1821	#num#	k4
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1871	#num#	k4
Nepomuk	Nepomuk	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
botanik	botanik	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
piaristického	piaristický	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
příležitostný	příležitostný	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
syn	syn	k1gMnSc1
Magdaleny	Magdalena	k1gFnSc2
Dobromily	Dobromila	k1gFnSc2
Rettigové	Rettigový	k1gFnSc2xF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
za	za	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
byl	být	k5eAaImAgInS
uznávanou	uznávaný	k2eAgFnSc7d1
vědeckou	vědecký	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
a	a	k8xC
člen	člen	k1gMnSc1
několika	několik	k4yIc2
vědeckých	vědecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
mnoho	mnoho	k6eAd1
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
publikační	publikační	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
a	a	k8xC
znám	znám	k2eAgInSc1d1
je	být	k5eAaImIp3nS
především	především	k9
jako	jako	k9
pedagog	pedagog	k1gMnSc1
(	(	kIx(
<g/>
působil	působit	k5eAaImAgMnS
na	na	k7c6
piaristickém	piaristický	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Kroměříži	Kroměříž	k1gFnSc6
a	a	k8xC
potom	potom	k6eAd1
v	v	k7c6
Nepomuku	Nepomuk	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známá	známá	k1gFnSc1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
veselohra	veselohra	k1gFnSc1
Sňatek	sňatek	k1gInSc4
ze	z	k7c2
žertu	žert	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŠPAČKOVÁ	Špačková	k1gFnSc1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Rettig	Rettig	k1gMnSc1
:	:	kIx,
Biografická	biografický	k2eAgFnSc1d1
črta	črta	k1gFnSc1
zapomenutého	zapomenutý	k2eAgMnSc2d1
syna	syn	k1gMnSc2
slavné	slavný	k2eAgFnSc2d1
matky	matka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
:	:	kIx,
řada	řada	k1gFnSc1
historická	historický	k2eAgFnSc1d1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
178	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
139	#num#	k4
<g/>
-	-	kIx~
<g/>
9543	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
V	v	k7c6
literatuře	literatura	k1gFnSc6
též	též	k9
chybně	chybně	k6eAd1
uváděno	uvádět	k5eAaImNgNnS
datum	datum	k1gNnSc4
úmrtí	úmrtí	k1gNnSc2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SOA	SOA	kA
Zámrsk	Zámrsk	k1gInSc1
<g/>
,	,	kIx,
Matrika	matrika	k1gFnSc1
narozených	narozený	k2eAgInPc2d1
1812	#num#	k4
-1837	-1837	k4
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
,	,	kIx,
sign	signum	k1gNnPc2
<g/>
.3067	.3067	k4
<g/>
,	,	kIx,
ukn	ukn	k?
<g/>
.10178	.10178	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.101	.101	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlin	k1gInSc5
<g/>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
úmrtí	úmrtí	k1gNnSc6
a	a	k8xC
pohřbu	pohřeb	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Rettig	Rettiga	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Rettig	Rettig	k1gMnSc1
</s>
<s>
Šumavský	šumavský	k2eAgInSc1d1
rozcestník	rozcestník	k1gInSc1
:	:	kIx,
Rettig	Rettig	k1gMnSc1
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
regionu	region	k1gInSc2
::	::	k?
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslava	k1gFnPc2
Rettig	Rettig	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1102377	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5653	#num#	k4
8126	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83879784	#num#	k4
</s>
