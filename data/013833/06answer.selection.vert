<s>
Josef	Josef	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
Liboslav	Liboslav	k1gMnSc1
Rettig	Rettig	k1gMnSc1xF
<g/>
,	,	kIx,
Sch	Sch	kA
<g/>
.	.	kIx.
<g/>
P.	P.	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1821	#num#	k4
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1871	#num#	k4
Nepomuk	Nepomuk	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
botanik	botanik	k1gMnSc1
<g/>
,	,	kIx,
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
piaristického	piaristický	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
příležitostný	příležitostný	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
syn	syn	k1gMnSc1
Magdaleny	Magdalena	k1gFnSc2
Dobromily	Dobromila	k1gFnSc2
Rettigové	Rettigový	k1gFnSc2xF
<g/>
.	.	kIx.
</s>