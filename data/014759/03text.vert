<s>
Kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
</s>
<s>
Disciplíny	disciplína	k1gFnPc1
tvořící	tvořící	k2eAgFnPc1d1
základ	základ	k1gInSc4
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
vztahy	vztah	k1gInPc1
<g/>
:	:	kIx,
lingvistika	lingvistika	k1gFnSc1
<g/>
,	,	kIx,
neurověda	neurověda	k1gFnSc1
<g/>
,	,	kIx,
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
<g/>
,	,	kIx,
antropologie	antropologie	k1gFnSc1
a	a	k8xC
psychologie	psychologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založeno	založit	k5eAaPmNgNnS
na	na	k7c4
Miller	Miller	k1gMnSc1
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
A	A	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gFnSc1
cognitive	cognitiv	k1gInSc5
revolution	revolution	k1gInSc1
<g/>
:	:	kIx,
a	a	k8xC
historical	historicat	k5eAaPmAgMnS
perspective	perspectiv	k1gInSc5
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
TRENDS	TRENDS	kA
in	in	k?
Cognitive	Cognitiv	k1gInSc5
Sciences	Sciences	k1gInSc4
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
cognoscere	cognoscrat	k5eAaPmIp3nS
=	=	kIx~
poznávat	poznávat	k5eAaImF
<g/>
)	)	kIx)
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
interdisciplinárním	interdisciplinární	k2eAgInSc7d1
výzkumem	výzkum	k1gInSc7
mysli	mysl	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gInPc2
procesů	proces	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
disciplíny	disciplína	k1gFnPc4
jako	jako	k9
je	být	k5eAaImIp3nS
psychologie	psychologie	k1gFnSc1
<g/>
,	,	kIx,
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
<g/>
,	,	kIx,
lingvistika	lingvistika	k1gFnSc1
<g/>
,	,	kIx,
neurověda	neurověda	k1gFnSc1
<g/>
,	,	kIx,
antropologie	antropologie	k1gFnSc1
či	či	k8xC
filosofie	filosofie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předmětem	předmět	k1gInSc7
výzkumu	výzkum	k1gInSc2
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
je	být	k5eAaImIp3nS
mysl	mysl	k1gFnSc1
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
struktura	struktura	k1gFnSc1
a	a	k8xC
operace	operace	k1gFnSc1
<g/>
,	,	kIx,
například	například	k6eAd1
myšlení	myšlení	k1gNnSc1
<g/>
,	,	kIx,
inteligence	inteligence	k1gFnSc1
<g/>
,	,	kIx,
paměť	paměť	k1gFnSc1
<g/>
,	,	kIx,
vnímání	vnímání	k1gNnSc1
<g/>
,	,	kIx,
pozornost	pozornost	k1gFnSc1
<g/>
,	,	kIx,
vědomí	vědomí	k1gNnSc1
či	či	k8xC
jazyk	jazyk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metody	metoda	k1gFnPc1
zkoumání	zkoumání	k1gNnSc2
jsou	být	k5eAaImIp3nP
rozmanité	rozmanitý	k2eAgFnPc1d1
a	a	k8xC
specifické	specifický	k2eAgFnPc1d1
pro	pro	k7c4
dané	daný	k2eAgFnPc4d1
disciplíny	disciplína	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
ně	on	k3xPp3gInPc4
např.	např.	kA
psychologické	psychologický	k2eAgInPc1d1
experimenty	experiment	k1gInPc1
a	a	k8xC
výpočetní	výpočetní	k2eAgNnSc1d1
modelování	modelování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dominantní	dominantní	k2eAgInSc4d1
jednotící	jednotící	k2eAgInSc4d1
prvek	prvek	k1gInSc4
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
komputačně-reprezentační	komputačně-reprezentační	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
mysl	mysl	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
autoři	autor	k1gMnPc1
dávají	dávat	k5eAaImIp3nP
kvůli	kvůli	k7c3
rozmanitosti	rozmanitost	k1gFnSc3
disciplín	disciplína	k1gFnPc2
přednost	přednost	k1gFnSc4
formě	forma	k1gFnSc3
množného	množný	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
a	a	k8xC
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
kognitivních	kognitivní	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Mysl	mysl	k1gFnSc1
je	být	k5eAaImIp3nS
tradičním	tradiční	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
filosofie	filosofie	k1gFnSc2
již	již	k6eAd1
od	od	k7c2
dob	doba	k1gFnPc2
starověkého	starověký	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
metodou	metoda	k1gFnSc7
byla	být	k5eAaImAgFnS
především	především	k9
teoretická	teoretický	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
problémů	problém	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
poznání	poznání	k1gNnSc1
<g/>
)	)	kIx)
založená	založený	k2eAgFnSc1d1
na	na	k7c4
introspekci	introspekce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlom	zlom	k1gInSc1
nastal	nastat	k5eAaPmAgInS
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
s	s	k7c7
příchodem	příchod	k1gInSc7
empiricky	empiricky	k6eAd1
založené	založený	k2eAgFnSc2d1
experimentální	experimentální	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
psychologové	psycholog	k1gMnPc1
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
spoléhali	spoléhat	k5eAaImAgMnP
na	na	k7c4
introspekci	introspekce	k1gFnSc4
jako	jako	k8xC,k8xS
cenný	cenný	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
poznání	poznání	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
byla	být	k5eAaImAgFnS
její	její	k3xOp3gFnSc1
nespolehlivost	nespolehlivost	k1gFnSc1
univerzálně	univerzálně	k6eAd1
uznávána	uznáván	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Psychologie	psychologie	k1gFnSc1
především	především	k9
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
prakticky	prakticky	k6eAd1
všeobecně	všeobecně	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
přiklonila	přiklonit	k5eAaPmAgFnS
k	k	k7c3
behaviorismu	behaviorismus	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
prosazoval	prosazovat	k5eAaImAgMnS
např.	např.	kA
John	John	k1gMnSc1
Watson	Watson	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Dle	dle	k7c2
behavioristů	behaviorista	k1gMnPc2
nejsou	být	k5eNaImIp3nP
mentální	mentální	k2eAgFnPc1d1
události	událost	k1gFnPc1
otevřeně	otevřeně	k6eAd1
pozorovatelné	pozorovatelný	k2eAgFnSc2d1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
chování	chování	k1gNnSc1
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
psychologie	psychologie	k1gFnSc1
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
objektivní	objektivní	k2eAgFnSc7d1
vědou	věda	k1gFnSc7
jen	jen	k9
skrze	skrze	k?
vytváření	vytváření	k1gNnSc1
teorií	teorie	k1gFnSc7
chování	chování	k1gNnSc2
bez	bez	k7c2
užívání	užívání	k1gNnSc2
mentálních	mentální	k2eAgInPc2d1
konceptů	koncept	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
místo	místo	k7c2
termínů	termín	k1gInPc2
"	"	kIx"
<g/>
vnímání	vnímání	k1gNnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
paměť	paměť	k1gFnSc1
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
jazyk	jazyk	k1gInSc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
mluvilo	mluvit	k5eAaImAgNnS
o	o	k7c4
"	"	kIx"
<g/>
diskriminaci	diskriminace	k1gFnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
učení	učení	k1gNnSc2
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
verbálním	verbální	k2eAgNnSc6d1
chování	chování	k1gNnSc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
však	však	k9
začalo	začít	k5eAaPmAgNnS
ukazovat	ukazovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
behavioristický	behavioristický	k2eAgInSc1d1
přístup	přístup	k1gInSc1
je	být	k5eAaImIp3nS
nedostačující	dostačující	k2eNgInSc1d1
pro	pro	k7c4
adekvátní	adekvátní	k2eAgNnSc4d1
vysvětlení	vysvětlení	k1gNnSc4
mysli	mysl	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
kognitivní	kognitivní	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
(	(	kIx(
<g/>
a	a	k8xC
nastupující	nastupující	k2eAgInSc4d1
psychologický	psychologický	k2eAgInSc4d1
směr	směr	k1gInSc4
jako	jako	k8xS,k8xC
kognitivismus	kognitivismus	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
mnoha	mnoho	k4c3
významným	významný	k2eAgInPc3d1
pokrokům	pokrok	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Noam	Noam	k1gMnSc1
Chomsky	Chomsky	k1gMnSc1
poukázal	poukázat	k5eAaPmAgMnS
na	na	k7c4
neschopnost	neschopnost	k1gFnSc4
behaviorismu	behaviorismus	k1gInSc2
vysvětlit	vysvětlit	k5eAaPmF
jazyk	jazyk	k1gInSc4
a	a	k8xC
v	v	k7c6
podstatě	podstata	k1gFnSc6
redefinoval	redefinovat	k5eAaImAgMnS
lingvistiku	lingvistika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
poli	pole	k1gNnSc6
psychologie	psychologie	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Jerome	Jerom	k1gInSc5
Brunera	Bruner	k1gMnSc4
k	k	k7c3
založení	založení	k1gNnSc3
Centra	centrum	k1gNnSc2
pro	pro	k7c4
kognitivní	kognitivní	k2eAgNnPc4d1
studia	studio	k1gNnPc4
na	na	k7c6
Harvardu	Harvard	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
s	s	k7c7
významnými	významný	k2eAgMnPc7d1
psychology	psycholog	k1gMnPc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
jakými	jaký	k3yRgInPc7,k3yQgInPc7,k3yIgInPc7
byli	být	k5eAaImAgMnP
např.	např.	kA
Sir	sir	k1gMnSc1
Frederic	Frederic	k1gMnSc1
Bartlett	Bartlett	k1gMnSc1
<g/>
,	,	kIx,
Jean	Jean	k1gMnSc1
Piaget	Piaget	k1gMnSc1
nebo	nebo	k8xC
Alexandr	Alexandr	k1gMnSc1
Lurija	Lurija	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
obrovskému	obrovský	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
poli	pole	k1gNnSc6
počítačové	počítačový	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc1
počátky	počátek	k1gInPc1
se	se	k3xPyFc4
pojí	pojit	k5eAaImIp3nP
s	s	k7c7
teorií	teorie	k1gFnSc7
informace	informace	k1gFnSc2
Clauda	Claud	k1gMnSc2
Shannona	Shannon	k1gMnSc2
<g/>
,	,	kIx,
prací	práce	k1gFnSc7
Alana	Alan	k1gMnSc2
Turinga	Turing	k1gMnSc2
a	a	k8xC
Johna	John	k1gMnSc2
von	von	k1gInSc1
Neumanna	Neumann	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popularitu	popularita	k1gFnSc4
nyní	nyní	k6eAd1
začala	začít	k5eAaPmAgFnS
získávat	získávat	k5eAaImF
kybernetika	kybernetik	k1gMnSc4
Norberta	Norbert	k1gMnSc4
Wienera	Wiener	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marvin	Marvina	k1gFnPc2
Minsky	minsky	k6eAd1
a	a	k8xC
John	John	k1gMnSc1
McCarthy	McCartha	k1gFnSc2
stáli	stát	k5eAaImAgMnP
u	u	k7c2
zrodu	zrod	k1gInSc2
výzkumu	výzkum	k1gInSc2
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alan	Alan	k1gMnSc1
Newell	Newell	k1gMnSc1
a	a	k8xC
Herbert	Herbert	k1gMnSc1
Simon	Simon	k1gMnSc1
prvně	prvně	k?
výpočetně	výpočetně	k6eAd1
modelovali	modelovat	k5eAaImAgMnP
kognitivní	kognitivní	k2eAgInPc4d1
procesy	proces	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
nejvýznamnější	významný	k2eAgMnPc4d3
pro	pro	k7c4
vznik	vznik	k1gInSc4
budoucí	budoucí	k2eAgFnSc2d1
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
rok	rok	k1gInSc4
1956	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
J.	J.	kA
McCarthy	McCartha	k1gFnSc2
<g/>
,	,	kIx,
M.	M.	kA
Minsky	Minsk	k1gInPc1
<g/>
,	,	kIx,
C.	C.	kA
Shannon	Shannon	k1gInSc1
a	a	k8xC
N.	N.	kA
Rochester	Rochester	k1gInSc1
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
uspořádali	uspořádat	k5eAaPmAgMnP
konferenci	konference	k1gFnSc4
o	o	k7c6
umělé	umělý	k2eAgFnSc6d1
inteligenci	inteligence	k1gFnSc6
v	v	k7c6
Dartmouthu	Dartmouth	k1gInSc6
a	a	k8xC
dali	dát	k5eAaPmAgMnP
vzniknout	vzniknout	k5eAaPmF
mnoha	mnoho	k4c2
důležitým	důležitý	k2eAgNnPc3d1
pracím	prací	k2eAgNnSc7d1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc1d1
pokroky	pokrok	k1gInPc1
v	v	k7c4
psychologii	psychologie	k1gFnSc4
přinesli	přinést	k5eAaPmAgMnP
např.	např.	kA
J.	J.	kA
Bruner	Bruner	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
Goodenough	Goodenough	k1gInSc1
a	a	k8xC
G.	G.	kA
Austin	Austin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
W.	W.	kA
<g/>
P.	P.	kA
Tanner	Tanner	k1gMnSc1
<g/>
,	,	kIx,
J.A.	J.A.	k1gMnSc1
Swets	Swets	k1gInSc1
<g/>
,	,	kIx,
T.G.	T.G.	k1gMnSc1
Birdsall	Birdsall	k1gMnSc1
a	a	k8xC
jiní	jiný	k2eAgMnPc1d1
rozvinuli	rozvinout	k5eAaPmAgMnP
teorii	teorie	k1gFnSc4
detekce	detekce	k1gFnSc2
signálů	signál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
George	Georg	k1gInSc2
Miller	Miller	k1gMnSc1
demonstroval	demonstrovat	k5eAaBmAgMnS
omezenost	omezenost	k1gFnSc4
kapacity	kapacita	k1gFnSc2
informačního	informační	k2eAgNnSc2d1
zpracování	zpracování	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
kapacita	kapacita	k1gFnSc1
pracovní	pracovní	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
–	–	k?
magické	magický	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
7	#num#	k4
plus	plus	k1gNnSc2
minus	minus	k6eAd1
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
W.	W.	kA
Goodenough	Goodenough	k1gInSc1
a	a	k8xC
F.	F.	kA
Lounsbury	Lounsbura	k1gFnSc2
přispěli	přispět	k5eAaPmAgMnP
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
kognitivní	kognitivní	k2eAgFnSc2d1
antropologie	antropologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
publikovány	publikován	k2eAgFnPc1d1
práce	práce	k1gFnPc1
B.L.	B.L.	k1gMnSc2
Whorfa	Whorf	k1gMnSc2
o	o	k7c6
vlivu	vliv	k1gInSc6
jazyka	jazyk	k1gInSc2
na	na	k7c4
myšlení	myšlení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
G.	G.	kA
Miller	Miller	k1gMnSc1
dokonce	dokonce	k9
uvádí	uvádět	k5eAaImIp3nS
přesné	přesný	k2eAgNnSc4d1
datum	datum	k1gNnSc4
vzniku	vznik	k1gInSc2
interdisciplinární	interdisciplinární	k2eAgFnSc2d1
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
jím	on	k3xPp3gInSc7
být	být	k5eAaImF
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1956	#num#	k4
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
sympozia	sympozion	k1gNnSc2
pořádaného	pořádaný	k2eAgNnSc2d1
"	"	kIx"
<g/>
Special	Special	k1gMnSc1
Interest	Interest	k1gMnSc1
Group	Group	k1gMnSc1
in	in	k?
Information	Information	k1gInSc1
Theory	Theora	k1gFnSc2
<g/>
"	"	kIx"
na	na	k7c6
americké	americký	k2eAgFnSc6d1
MIT	MIT	kA
<g/>
,	,	kIx,
kterého	který	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
množství	množství	k1gNnSc1
výše	výše	k1gFnSc2,k1gFnSc2wB
zmíněných	zmíněný	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
Millera	Miller	k1gMnSc2
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
experimentální	experimentální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g/>
,	,	kIx,
teoretická	teoretický	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc1d1
simulace	simulace	k1gFnSc1
kognitivních	kognitivní	k2eAgInPc2d1
procesů	proces	k1gInPc2
staly	stát	k5eAaPmAgFnP
součástmi	součást	k1gFnPc7
většího	veliký	k2eAgInSc2d2
celku	celek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
zvyšovat	zvyšovat	k5eAaImF
interdisciplinární	interdisciplinární	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c7
těmito	tento	k3xDgInPc7
obory	obor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Harvardu	Harvard	k1gInSc6
používali	používat	k5eAaImAgMnP
název	název	k1gInSc4
kognitivní	kognitivní	k2eAgNnPc4d1
studia	studio	k1gNnPc4
<g/>
,	,	kIx,
na	na	k7c4
Carnegie-Mellon	Carnegie-Mellon	k1gInSc4
information-processing	information-processing	k1gInSc4
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
La	la	k1gNnSc6
Jolla	Jollo	k1gNnSc2
kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1976	#num#	k4
dokončila	dokončit	k5eAaPmAgFnS
nadace	nadace	k1gFnSc1
Alfreda	Alfred	k1gMnSc2
P.	P.	kA
Sloana	Sloan	k1gMnSc2
program	program	k1gInSc1
pro	pro	k7c4
tehdy	tehdy	k6eAd1
mladou	mladý	k2eAgFnSc4d1
disciplínu	disciplína	k1gFnSc4
–	–	k?
neurovědu	neurověda	k1gFnSc4
–	–	k?
a	a	k8xC
měli	mít	k5eAaImAgMnP
zájem	zájem	k1gInSc4
podpořit	podpořit	k5eAaPmF
iniciativu	iniciativa	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
překročila	překročit	k5eAaPmAgFnS
pomyslnou	pomyslný	k2eAgFnSc4d1
mezeru	mezera	k1gFnSc4
mezi	mezi	k7c7
mozkem	mozek	k1gInSc7
a	a	k8xC
myslí	mysl	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tento	tento	k3xDgInSc4
program	program	k1gInSc4
byl	být	k5eAaImAgMnS
nakonec	nakonec	k6eAd1
zvolen	zvolen	k2eAgInSc4d1
název	název	k1gInSc4
kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k8xC
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
i	i	k9
nově	nově	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
kognitivní	kognitivní	k2eAgFnSc1d1
neurověda	neurověda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
polovině	polovina	k1gFnSc6
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
vznikla	vzniknout	k5eAaPmAgFnS
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
Society	societa	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
začala	začít	k5eAaPmAgFnS
vydávat	vydávat	k5eAaImF,k5eAaPmF
žurnál	žurnál	k1gInSc4
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
a	a	k8xC
později	pozdě	k6eAd2
mezi	mezi	k7c4
disciplíny	disciplína	k1gFnPc4
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
začlenila	začlenit	k5eAaPmAgFnS
také	také	k9
pedagogiku	pedagogika	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Disciplíny	disciplína	k1gFnPc1
</s>
<s>
Kognitivní	kognitivní	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
za	za	k7c4
průsečík	průsečík	k1gInSc4
mezi	mezi	k7c7
mnoha	mnoho	k4c7
zavedenými	zavedený	k2eAgFnPc7d1
disciplínami	disciplína	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
mají	mít	k5eAaImIp3nP
širší	široký	k2eAgInSc4d2
záběr	záběr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
těchto	tento	k3xDgFnPc2
disciplín	disciplína	k1gFnPc2
se	se	k3xPyFc4
však	však	k9
často	často	k6eAd1
etablovaly	etablovat	k5eAaBmAgFnP
subdisciplíny	subdisciplína	k1gFnPc4
zaměřené	zaměřený	k2eAgFnPc4d1
právě	právě	k6eAd1
na	na	k7c4
studium	studium	k1gNnSc4
mysli	mysl	k1gFnSc2
a	a	k8xC
mnoho	mnoho	k4c4
hraničních	hraniční	k2eAgFnPc2d1
disciplín	disciplína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Psychologie	psychologie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
grafického	grafický	k2eAgNnSc2d1
znázornění	znázornění	k1gNnSc2
kognitivně-psychologické	kognitivně-psychologický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
:	:	kIx,
Baddeleyho	Baddeley	k1gMnSc2
model	model	k1gInSc4
pracovní	pracovní	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
</s>
<s>
Za	za	k7c4
základ	základ	k1gInSc4
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
psychologii	psychologie	k1gFnSc4
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
obecnou	obecný	k2eAgFnSc7d1
<g/>
,	,	kIx,
t.j.	t.j.	k?
kognitivní	kognitivní	k2eAgFnSc4d1
psychologii	psychologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
jsou	být	k5eAaImIp3nP
experimenty	experiment	k1gInPc1
s	s	k7c7
lidmi	člověk	k1gMnPc7
zkoumající	zkoumající	k2eAgNnSc1d1
konkrétní	konkrétní	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
mysli	mysl	k1gFnSc2
z	z	k7c2
mnoha	mnoho	k4c2
směrů	směr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
experimentů	experiment	k1gInPc2
se	se	k3xPyFc4
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
,	,	kIx,
ověřují	ověřovat	k5eAaImIp3nP
a	a	k8xC
upravují	upravovat	k5eAaImIp3nP
psychologické	psychologický	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
<g/>
,	,	kIx,
často	často	k6eAd1
v	v	k7c6
pojmech	pojem	k1gInPc6
reprezentací	reprezentace	k1gFnPc2
a	a	k8xC
procedur	procedura	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
výpočetních	výpočetní	k2eAgInPc2d1
modelů	model	k1gInPc2
simulujících	simulující	k2eAgInPc2d1
lidské	lidský	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
na	na	k7c6
základě	základ	k1gInSc6
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
pak	pak	k6eAd1
lze	lze	k6eAd1
porovnat	porovnat	k5eAaPmF
se	s	k7c7
skutečnými	skutečný	k2eAgInPc7d1
experimentálními	experimentální	k2eAgInPc7d1
výsledky	výsledek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
klasických	klasický	k2eAgFnPc2d1
behaviorálních	behaviorální	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
měření	měření	k1gNnSc4
reakčních	reakční	k2eAgInPc2d1
časů	čas	k1gInPc2
a	a	k8xC
psychofyzika	psychofyzika	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c6
psychologii	psychologie	k1gFnSc6
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
i	i	k9
neurovědných	urovědný	k2eNgFnPc2d1
metod	metoda	k1gFnPc2
jako	jako	k9
jsou	být	k5eAaImIp3nP
Elektroencefalografie	Elektroencefalografie	k1gFnPc1
(	(	kIx(
<g/>
EEG	EEG	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
funkční	funkční	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
rezonance	rezonance	k1gFnSc1
(	(	kIx(
<g/>
fMRI	fMRI	k?
<g/>
)	)	kIx)
či	či	k8xC
transkraniální	transkraniální	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
stimulace	stimulace	k1gFnSc1
(	(	kIx(
<g/>
TMS	TMS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologie	psychologie	k1gFnSc1
využívající	využívající	k2eAgFnSc2d1
tyto	tento	k3xDgFnPc1
metody	metoda	k1gFnPc4
se	se	k3xPyFc4
někdy	někdy	k6eAd1
chápe	chápat	k5eAaImIp3nS
jako	jako	k9
samostatná	samostatný	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
neurokognitivní	urokognitivní	k2eNgFnSc2d1
psychologie	psychologie	k1gFnSc2
či	či	k8xC
kognitivní	kognitivní	k2eAgFnSc1d1
neurověda	neurověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kognitivní	kognitivní	k2eAgFnSc1d1
neuropsychologie	neuropsychologie	k1gFnSc1
je	být	k5eAaImIp3nS
odvětví	odvětví	k1gNnSc4
kognitivní	kognitivní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
zkoumá	zkoumat	k5eAaImIp3nS
kognitivní	kognitivní	k2eAgInPc4d1
deficity	deficit	k1gInPc4
pacientů	pacient	k1gMnPc2
s	s	k7c7
neurologickým	urologický	k2eNgNnSc7d1,k2eAgNnSc7d1
poškozením	poškození	k1gNnSc7
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
vyvozuje	vyvozovat	k5eAaImIp3nS
závěry	závěr	k1gInPc7
o	o	k7c6
lidské	lidský	k2eAgFnSc6d1
kognitivní	kognitivní	k2eAgFnSc6d1
architektuře	architektura	k1gFnSc6
<g/>
,	,	kIx,
především	především	k6eAd1
jejích	její	k3xOp3gFnPc6
komponentách	komponenta	k1gFnPc6
a	a	k8xC
spojeních	spojení	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
i	i	k8xC
poznatky	poznatek	k1gInPc1
vývojové	vývojový	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
v	v	k7c6
průběhu	průběh	k1gInSc6
ontogeneze	ontogeneze	k1gFnSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
různých	různý	k2eAgFnPc2d1
kognitivních	kognitivní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Evoluční	evoluční	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
se	se	k3xPyFc4
navíc	navíc	k6eAd1
snaží	snažit	k5eAaImIp3nP
vysvětlit	vysvětlit	k5eAaPmF
vývoj	vývoj	k1gInSc4
a	a	k8xC
adaptivní	adaptivní	k2eAgFnSc4d1
roli	role	k1gFnSc4
kognitivních	kognitivní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
fylogeneze	fylogeneze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hraniční	hraniční	k2eAgFnSc7d1
disciplínou	disciplína	k1gFnSc7
mezi	mezi	k7c7
psychologií	psychologie	k1gFnSc7
a	a	k8xC
ekonomií	ekonomie	k1gFnSc7
je	být	k5eAaImIp3nS
behaviorální	behaviorální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zkoumá	zkoumat	k5eAaImIp3nS
vlivy	vliv	k1gInPc4
sociálních	sociální	k2eAgInPc2d1
<g/>
,	,	kIx,
kognitivních	kognitivní	k2eAgInPc2d1
a	a	k8xC
emočních	emoční	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
na	na	k7c4
ekonomické	ekonomický	k2eAgNnSc4d1
chování	chování	k1gNnSc4
jedinců	jedinec	k1gMnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
pomezí	pomezí	k1gNnSc6
kognitivní	kognitivní	k2eAgFnSc2d1
a	a	k8xC
evoluční	evoluční	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
kognitivní	kognitivní	k2eAgFnSc2d1
antropologie	antropologie	k1gFnSc2
<g/>
,	,	kIx,
kognitivní	kognitivní	k2eAgFnSc2d1
neurovědy	neurověda	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
polí	pole	k1gFnPc2
stojí	stát	k5eAaImIp3nS
kognitivní	kognitivní	k2eAgFnSc1d1
religionistika	religionistika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkoumá	zkoumat	k5eAaImIp3nS
náboženské	náboženský	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
a	a	k8xC
chování	chování	k1gNnSc4
z	z	k7c2
pohledu	pohled	k1gInSc2
kognitivních	kognitivní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
teorie	teorie	k1gFnSc2
evoluce	evoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Informatika	informatika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Informatika	informatika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Robot	robot	k1gInSc1
Kismet	kismet	k1gInSc1
se	s	k7c7
základními	základní	k2eAgFnPc7d1
sociálními	sociální	k2eAgFnPc7d1
dovednostmi	dovednost	k1gFnPc7
</s>
<s>
Zmíněnou	zmíněný	k2eAgFnSc7d1
aplikací	aplikace	k1gFnSc7
informatiky	informatika	k1gFnSc2
je	být	k5eAaImIp3nS
tvorba	tvorba	k1gFnSc1
výpočetních	výpočetní	k2eAgInPc2d1
modelů	model	k1gInPc2
simulujících	simulující	k2eAgInPc2d1
operace	operace	k1gFnPc4
lidského	lidský	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
modelování	modelování	k1gNnSc2
přirozeného	přirozený	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
se	se	k3xPyFc4
subdisciplína	subdisciplína	k1gFnSc1
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
snaží	snažit	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
umělou	umělý	k2eAgFnSc4d1
mysl	mysl	k1gFnSc4
–	–	k?
stroje	stroj	k1gInSc2
vykazující	vykazující	k2eAgFnSc2d1
známky	známka	k1gFnSc2
inteligentního	inteligentní	k2eAgNnSc2d1
chování	chování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hraniční	hraniční	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
výpočetní	výpočetní	k2eAgFnSc1d1
neurověda	neurověda	k1gFnSc1
aplikuje	aplikovat	k5eAaBmIp3nS
metody	metoda	k1gFnPc4
informatiky	informatika	k1gFnSc2
také	také	k9
na	na	k7c4
modelování	modelování	k1gNnSc4
a	a	k8xC
simulaci	simulace	k1gFnSc4
procesů	proces	k1gInPc2
různé	různý	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
v	v	k7c6
lidském	lidský	k2eAgInSc6d1
mozku	mozek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Lingvistika	lingvistika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Syntaktický	syntaktický	k2eAgInSc1d1
strom	strom	k1gInSc1
slavné	slavný	k2eAgFnSc2d1
Chomského	Chomský	k2eAgInSc2d1
věty	věta	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
gramaticky	gramaticky	k6eAd1
korektní	korektní	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
sémanticky	sémanticky	k6eAd1
nesmyslná	smyslný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Lingvisté	lingvista	k1gMnPc1
hledají	hledat	k5eAaImIp3nP
základní	základní	k2eAgNnPc4d1
gramatická	gramatický	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
jazyka	jazyk	k1gInSc2
pomocí	pomocí	k7c2
analýzy	analýza	k1gFnSc2
rozdílů	rozdíl	k1gInPc2
mezi	mezi	k7c7
gramatickými	gramatický	k2eAgFnPc7d1
a	a	k8xC
agramatickými	agramatický	k2eAgFnPc7d1
větami	věta	k1gFnPc7
<g/>
,	,	kIx,
provádí	provádět	k5eAaImIp3nS
psychologické	psychologický	k2eAgInPc4d1
experimenty	experiment	k1gInPc4
a	a	k8xC
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
výpočetní	výpočetní	k2eAgInPc4d1
modely	model	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
pomezí	pomezí	k1gNnSc6
lingvistiky	lingvistika	k1gFnSc2
a	a	k8xC
psychologie	psychologie	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
psycholingvistika	psycholingvistika	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
jazykovými	jazykový	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
člověka	člověk	k1gMnSc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc7
předpoklady	předpoklad	k1gInPc7
z	z	k7c2
psychologického	psychologický	k2eAgNnSc2d1
a	a	k8xC
neurobiologického	urobiologický	k2eNgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
hranici	hranice	k1gFnSc6
lingvistiky	lingvistika	k1gFnSc2
<g/>
,	,	kIx,
neurovědy	neurověda	k1gFnSc2
a	a	k8xC
neuropsychologie	neuropsychologie	k1gFnSc2
leží	ležet	k5eAaImIp3nS
neurolingvistika	neurolingvistika	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zkoumá	zkoumat	k5eAaImIp3nS
neurální	neurální	k2eAgInPc4d1
mechanismy	mechanismus	k1gInPc4
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Neurověda	Neurověda	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Neurověda	Neurověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Výzkumník	výzkumník	k1gMnSc1
prohlíží	prohlížet	k5eAaImIp3nS
snímky	snímek	k1gInPc4
mozku	mozek	k1gInSc2
pořízené	pořízený	k2eAgNnSc4d1
metodou	metoda	k1gFnSc7
funkční	funkční	k2eAgFnSc2d1
magnetické	magnetický	k2eAgFnSc2d1
rezonance	rezonance	k1gFnSc2
</s>
<s>
Neurověda	Neurověda	k1gFnSc1
je	být	k5eAaImIp3nS
vědecká	vědecký	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
zkoumající	zkoumající	k2eAgFnSc2d1
nervový	nervový	k2eAgInSc4d1
systém	systém	k1gInSc4
na	na	k7c6
mnoha	mnoho	k4c6
úrovních	úroveň	k1gFnPc6
<g/>
,	,	kIx,
od	od	k7c2
genetiky	genetika	k1gFnSc2
po	po	k7c6
chování	chování	k1gNnSc6
<g/>
,	,	kIx,
proto	proto	k8xC
ji	on	k3xPp3gFnSc4
lze	lze	k6eAd1
různě	různě	k6eAd1
dělit	dělit	k5eAaImF
na	na	k7c4
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
specializovaných	specializovaný	k2eAgNnPc2d1
odvětví	odvětví	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Neurovědy	Neurověda	k1gFnPc1
nižších	nízký	k2eAgFnPc2d2
úrovní	úroveň	k1gFnPc2
popisu	popis	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
molekulární	molekulární	k2eAgFnPc1d1
a	a	k8xC
buněčná	buněčný	k2eAgFnSc1d1
neurověda	neurověda	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
např.	např.	kA
mechanismem	mechanismus	k1gInSc7
přenosu	přenos	k1gInSc3
signálů	signál	k1gInPc2
<g/>
,	,	kIx,
tvorbou	tvorba	k1gFnSc7
synapsí	synapse	k1gFnSc7
a	a	k8xC
neurotransmitery	neurotransmiter	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systémová	systémový	k2eAgFnSc1d1
neurověda	neurověda	k1gFnSc1
pak	pak	k6eAd1
studuje	studovat	k5eAaImIp3nS
neurální	neurální	k2eAgInPc4d1
systémy	systém	k1gInPc4
a	a	k8xC
okruhy	okruh	k1gInPc4
složené	složený	k2eAgFnSc2d1
z	z	k7c2
mnoha	mnoho	k4c2
buněk	buňka	k1gFnPc2
a	a	k8xC
synapsí	synapse	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
zjišťuje	zjišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
tyto	tento	k3xDgInPc1
systémy	systém	k1gInPc1
vykonávají	vykonávat	k5eAaImIp3nP
funkce	funkce	k1gFnPc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
pohyb	pohyb	k1gInSc1
<g/>
,	,	kIx,
vnímání	vnímání	k1gNnSc1
či	či	k8xC
rozhodování	rozhodování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
například	například	k6eAd1
snímat	snímat	k5eAaImF
elektrickou	elektrický	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
skupin	skupina	k1gFnPc2
neuronů	neuron	k1gInPc2
během	během	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
invazivnosti	invazivnost	k1gFnSc3
metody	metoda	k1gFnSc2
častěji	často	k6eAd2
zvíře	zvíře	k1gNnSc1
<g/>
,	,	kIx,
vykonává	vykonávat	k5eAaImIp3nS
nějakou	nějaký	k3yIgFnSc4
úlohu	úloha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
je	být	k5eAaImIp3nS
nejvýznamnější	významný	k2eAgFnSc1d3
výše	výše	k1gFnSc1
zmíněná	zmíněný	k2eAgFnSc1d1
kognitivní	kognitivní	k2eAgFnSc1d1
a	a	k8xC
behaviorální	behaviorální	k2eAgFnSc1d1
neurověda	neurověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkoumá	zkoumat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
jsou	být	k5eAaImIp3nP
psychologické	psychologický	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
implementovány	implementován	k2eAgFnPc4d1
v	v	k7c6
mozku	mozek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
běžných	běžný	k2eAgInPc2d1
psychologických	psychologický	k2eAgInPc2d1
experimentů	experiment	k1gInPc2
je	být	k5eAaImIp3nS
snímána	snímán	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
mozku	mozek	k1gInSc2
pomocí	pomocí	k7c2
zobrazovacích	zobrazovací	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
fMRI	fMRI	k?
či	či	k8xC
pozitronová	pozitronový	k2eAgFnSc1d1
emisní	emisní	k2eAgFnSc1d1
tomografie	tomografie	k1gFnSc1
(	(	kIx(
<g/>
PET	PET	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
se	se	k3xPyFc4
usuzuje	usuzovat	k5eAaImIp3nS
na	na	k7c6
relativní	relativní	k2eAgFnSc6d1
lokalizaci	lokalizace	k1gFnSc6
kognitivních	kognitivní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
–	–	k?
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
tzv.	tzv.	kA
neurální	neurální	k2eAgInPc4d1
koreláty	korelát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
EEG	EEG	kA
či	či	k8xC
magnetoencefalografie	magnetoencefalografie	k1gFnSc1
(	(	kIx(
<g/>
MEG	MEG	kA
<g/>
)	)	kIx)
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
umožňuje	umožňovat	k5eAaImIp3nS
lépe	dobře	k6eAd2
sledovat	sledovat	k5eAaImF
časový	časový	k2eAgInSc4d1
průběh	průběh	k1gInSc4
těchto	tento	k3xDgFnPc2
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
pomocí	pomocí	k7c2
metody	metoda	k1gFnSc2
evokovaných	evokovaný	k2eAgInPc2d1
potenciálů	potenciál	k1gInPc2
(	(	kIx(
<g/>
ERP	ERP	kA
<g/>
)	)	kIx)
lze	lze	k6eAd1
zkoumat	zkoumat	k5eAaImF
jakou	jaký	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
část	část	k1gFnSc4
daného	daný	k2eAgInSc2d1
procesu	proces	k1gInSc2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
jaká	jaký	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
experimentální	experimentální	k2eAgFnSc1d1
manipulace	manipulace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
fMRI	fMRI	k?
má	mít	k5eAaImIp3nS
dobré	dobrý	k2eAgNnSc4d1
prostorové	prostorový	k2eAgNnSc4d1
a	a	k8xC
EEG	EEG	kA
dobré	dobrý	k2eAgNnSc4d1
časové	časový	k2eAgNnSc4d1
rozlišení	rozlišení	k1gNnSc4
<g/>
,	,	kIx,
o	o	k7c6
TMS	TMS	kA
lze	lze	k6eAd1
říct	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
funkční	funkční	k2eAgNnSc4d1
rozlišení	rozlišení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
TMS	TMS	kA
ovlivňuje	ovlivňovat	k5eAaImIp3nS
pomocí	pomocí	k7c2
magnetického	magnetický	k2eAgNnSc2d1
pole	pole	k1gNnSc2
aktivitu	aktivita	k1gFnSc4
v	v	k7c6
určité	určitý	k2eAgFnSc6d1
malé	malý	k2eAgFnSc6d1
části	část	k1gFnSc6
mozkové	mozkový	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ovlivní	ovlivnit	k5eAaPmIp3nS
i	i	k9
výkon	výkon	k1gInSc4
v	v	k7c6
psychologických	psychologický	k2eAgInPc6d1
experimentech	experiment	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
tak	tak	k6eAd1
usuzovat	usuzovat	k5eAaImF
nikoli	nikoli	k9
pouze	pouze	k6eAd1
o	o	k7c6
korelacích	korelace	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c6
kauzálních	kauzální	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
mezi	mezi	k7c7
neurální	neurální	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
a	a	k8xC
chováním	chování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Blíže	blízce	k6eAd2
klinické	klinický	k2eAgFnSc3d1
praxi	praxe	k1gFnSc3
je	být	k5eAaImIp3nS
neuropsychologie	neuropsychologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
zkoumá	zkoumat	k5eAaImIp3nS
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
mentálními	mentální	k2eAgFnPc7d1
dovednostmi	dovednost	k1gFnPc7
a	a	k8xC
činností	činnost	k1gFnSc7
mozku	mozek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
i	i	k8xC
metodami	metoda	k1gFnPc7
se	se	k3xPyFc4
překrývá	překrývat	k5eAaImIp3nS
s	s	k7c7
kognitivní	kognitivní	k2eAgFnSc7d1
neurovědou	neurověda	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ní	on	k3xPp3gFnSc2
zkoumá	zkoumat	k5eAaImIp3nS
především	především	k9
pacienty	pacient	k1gMnPc4
s	s	k7c7
kognitivními	kognitivní	k2eAgInPc7d1
deficity	deficit	k1gInPc7
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
vymezuje	vymezovat	k5eAaImIp3nS
přesnější	přesný	k2eAgInSc1d2
termín	termín	k1gInSc1
klinická	klinický	k2eAgFnSc1d1
neuropsychologie	neuropsychologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
lokalizací	lokalizace	k1gFnSc7
mentálních	mentální	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
v	v	k7c6
mozku	mozek	k1gInSc6
<g/>
,	,	kIx,
diagnostikou	diagnostika	k1gFnSc7
a	a	k8xC
léčbou	léčba	k1gFnSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
od	od	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
zmíněné	zmíněný	k2eAgFnSc2d1
kognitivní	kognitivní	k2eAgFnSc2d1
neuropsychologie	neuropsychologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
pomezí	pomezí	k1gNnSc6
neurovědy	neurověda	k1gFnSc2
<g/>
,	,	kIx,
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
fyziky	fyzika	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
disciplín	disciplína	k1gFnPc2
leží	ležet	k5eAaImIp3nS
výpočetní	výpočetní	k2eAgFnSc1d1
neurověda	neurověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
modelovat	modelovat	k5eAaImF
funkci	funkce	k1gFnSc4
nervové	nervový	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
od	od	k7c2
jednotlivých	jednotlivý	k2eAgInPc2d1
neuronů	neuron	k1gInPc2
až	až	k9
po	po	k7c4
kognitivní	kognitivní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
učení	učení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Antropologie	antropologie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Antropologie	antropologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
poznatků	poznatek	k1gInPc2
o	o	k7c4
funkci	funkce	k1gFnSc4
lidské	lidský	k2eAgFnSc2d1
mysli	mysl	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
experimentů	experiment	k1gInPc2
prováděných	prováděný	k2eAgInPc2d1
na	na	k7c6
vysokoškolských	vysokoškolský	k2eAgMnPc6d1
studentech	student	k1gMnPc6
rozvinutých	rozvinutý	k2eAgFnPc2d1
západních	západní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
se	se	k3xPyFc4
proto	proto	k8xC
začínají	začínat	k5eAaImIp3nP
stále	stále	k6eAd1
více	hodně	k6eAd2
zajímat	zajímat	k5eAaImF
o	o	k7c4
vlivy	vliv	k1gInPc4
různých	různý	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
<g/>
,	,	kIx,
jazyků	jazyk	k1gInPc2
a	a	k8xC
prostředí	prostředí	k1gNnSc2
na	na	k7c4
myšlení	myšlení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
metodou	metoda	k1gFnSc7
kulturní	kulturní	k2eAgFnSc2d1
antropologie	antropologie	k1gFnSc2
je	být	k5eAaImIp3nS
etnografie	etnografie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumníci	výzkumník	k1gMnPc1
žijí	žít	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
určité	určitý	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
po	po	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
porozuměli	porozumět	k5eAaPmAgMnP
sociálním	sociální	k2eAgMnPc3d1
a	a	k8xC
kognitivním	kognitivní	k2eAgInPc3d1
rozdílům	rozdíl	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kognitivní	kognitivní	k2eAgFnSc4d1
antropologii	antropologie	k1gFnSc4
lze	lze	k6eAd1
brát	brát	k5eAaImF
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
kulturní	kulturní	k2eAgFnSc2d1
antropologie	antropologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
metod	metoda	k1gFnPc2
a	a	k8xC
teorií	teorie	k1gFnPc2
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
především	především	k6eAd1
experimentální	experimentální	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
a	a	k8xC
evoluční	evoluční	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
zkoumá	zkoumat	k5eAaImIp3nS
například	například	k6eAd1
kulturní	kulturní	k2eAgFnSc1d1
inovace	inovace	k1gFnSc1
<g/>
,	,	kIx,
šíření	šíření	k1gNnSc1
sdílené	sdílený	k2eAgFnSc2d1
znalosti	znalost	k1gFnSc2
<g/>
,	,	kIx,
vliv	vliv	k1gInSc4
jazyka	jazyk	k1gInSc2
na	na	k7c4
chování	chování	k1gNnSc4
a	a	k8xC
myšlení	myšlení	k1gNnSc4
nebo	nebo	k8xC
podobnosti	podobnost	k1gFnPc4
a	a	k8xC
rozdíly	rozdíl	k1gInPc4
v	v	k7c6
jménech	jméno	k1gNnPc6
barev	barva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Filosofie	filosofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
filosofické	filosofický	k2eAgFnPc4d1
disciplíny	disciplína	k1gFnPc4
relevantní	relevantní	k2eAgFnSc1d1
pro	pro	k7c4
kognitivní	kognitivní	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
lze	lze	k6eAd1
počítat	počítat	k5eAaImF
především	především	k9
filosofii	filosofie	k1gFnSc3
mysli	mysl	k1gFnSc2
<g/>
,	,	kIx,
filosofii	filosofie	k1gFnSc3
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
filosofii	filosofie	k1gFnSc3
vědy	věda	k1gFnSc2
a	a	k8xC
etiku	etika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Filosofie	filosofie	k1gFnSc1
mysli	mysl	k1gFnSc2
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
převážně	převážně	k6eAd1
konceptuálními	konceptuální	k2eAgFnPc7d1
otázkami	otázka	k1gFnPc7
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
vztah	vztah	k1gInSc4
těla	tělo	k1gNnSc2
a	a	k8xC
mysli	mysl	k1gFnSc2
<g/>
,	,	kIx,
vědomí	vědomí	k1gNnSc2
<g/>
,	,	kIx,
svobodná	svobodný	k2eAgFnSc1d1
vůle	vůle	k1gFnSc1
<g/>
,	,	kIx,
poznání	poznání	k1gNnSc1
a	a	k8xC
povaha	povaha	k1gFnSc1
mentálních	mentální	k2eAgInPc2d1
obsahů	obsah	k1gInPc2
a	a	k8xC
procesů	proces	k1gInPc2
obecně	obecně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominantní	dominantní	k2eAgInSc4d1
postavení	postavení	k1gNnSc1
zde	zde	k6eAd1
zastává	zastávat	k5eAaImIp3nS
anglo-americká	anglo-americký	k2eAgFnSc1d1
analytická	analytický	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
i	i	k9
kvůli	kvůli	k7c3
jejímu	její	k3xOp3gInSc3
převážně	převážně	k6eAd1
naturalistickému	naturalistický	k2eAgInSc3d1
přístupu	přístup	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
zažívá	zažívat	k5eAaImIp3nS
určitou	určitý	k2eAgFnSc4d1
rehabilitaci	rehabilitace	k1gFnSc4
i	i	k9
dříve	dříve	k6eAd2
spíše	spíše	k9
anti-naturalistická	anti-naturalistický	k2eAgFnSc1d1
kontinentální	kontinentální	k2eAgFnSc1d1
fenomenologická	fenomenologický	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
nahlížena	nahlížen	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
varianta	varianta	k1gFnSc1
nedůvěryhodné	důvěryhodný	k2eNgFnSc2d1
introspekce	introspekce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
se	se	k3xPyFc4
však	však	k9
snaží	snažit	k5eAaImIp3nS
nacházet	nacházet	k5eAaImF
styčné	styčný	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
s	s	k7c7
poznatky	poznatek	k1gInPc7
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
jako	jako	k8xS,k8xC
je	on	k3xPp3gNnSc4
např.	např.	kA
teze	teze	k1gFnPc4
vtělené	vtělený	k2eAgFnSc2d1
kognice	kognice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
přispívá	přispívat	k5eAaImIp3nS
ke	k	k7c3
konceptualizaci	konceptualizace	k1gFnSc3
témat	téma	k1gNnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
vědomí	vědomí	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dle	dle	k7c2
zastánců	zastánce	k1gMnPc2
fenomenologie	fenomenologie	k1gFnSc2
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
lepšímu	dobrý	k2eAgInSc3d2
návrhu	návrh	k1gInSc3
a	a	k8xC
interpretaci	interpretace	k1gFnSc3
experimentů	experiment	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Filosofie	filosofie	k1gFnSc1
neurovědy	neurověda	k1gFnSc2
a	a	k8xC
filosofie	filosofie	k1gFnSc2
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
jsou	být	k5eAaImIp3nP
aplikacemi	aplikace	k1gFnPc7
filosofie	filosofie	k1gFnSc2
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabývá	zabývat	k5eAaImIp3nS
se	se	k3xPyFc4
metodologií	metodologie	k1gFnSc7
<g/>
,	,	kIx,
konceptuálními	konceptuální	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
,	,	kIx,
relevancí	relevance	k1gFnSc7
neurovědních	urovědní	k2eNgInPc2d1
poznatků	poznatek	k1gInPc2
pro	pro	k7c4
psychologické	psychologický	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
<g/>
,	,	kIx,
nároky	nárok	k1gInPc4
na	na	k7c4
adekvátní	adekvátní	k2eAgNnSc4d1
vysvětlení	vysvětlení	k1gNnSc4
v	v	k7c6
neurovědách	neurověda	k1gFnPc6
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Neurofilosofie	Neurofilosofie	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
ztotožňována	ztotožňován	k2eAgFnSc1d1
s	s	k7c7
filosofií	filosofie	k1gFnSc7
neurovědy	neurověda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
ji	on	k3xPp3gFnSc4
však	však	k9
vymezit	vymezit	k5eAaPmF
jako	jako	k9
snahu	snaha	k1gFnSc4
o	o	k7c4
řešení	řešení	k1gNnSc4
konkrétních	konkrétní	k2eAgInPc2d1
problémů	problém	k1gInPc2
filosofie	filosofie	k1gFnSc2
pomocí	pomocí	k7c2
poznatků	poznatek	k1gInPc2
neurověd	neurověda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
zkoumání	zkoumání	k1gNnSc4
neurálních	neurální	k2eAgInPc2d1
systémů	systém	k1gInPc2
etického	etický	k2eAgNnSc2d1
hodnocení	hodnocení	k1gNnSc2
nebo	nebo	k8xC
volního	volní	k2eAgNnSc2d1
rozhodování	rozhodování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
i	i	k9
neuroetika	neuroetika	k1gFnSc1
je	být	k5eAaImIp3nS
víceznačný	víceznačný	k2eAgInSc4d1
termín	termín	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
smyslu	smysl	k1gInSc6
"	"	kIx"
<g/>
neurověda	neurověda	k1gFnSc1
etiky	etika	k1gFnSc2
<g/>
"	"	kIx"
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
pod	pod	k7c4
výše	vysoce	k6eAd2
uvedenou	uvedený	k2eAgFnSc4d1
neurofilosofii	neurofilosofie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
smyslu	smysl	k1gInSc6
"	"	kIx"
<g/>
etika	etika	k1gFnSc1
neurovědy	neurověda	k1gFnSc2
<g/>
"	"	kIx"
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
etickými	etický	k2eAgInPc7d1
<g/>
,	,	kIx,
právními	právní	k2eAgInPc7d1
a	a	k8xC
společenskými	společenský	k2eAgInPc7d1
dopady	dopad	k1gInPc7
neurověd	neurověda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
například	například	k6eAd1
o	o	k7c4
problémy	problém	k1gInPc4
předvídání	předvídání	k1gNnSc2
lidského	lidský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
či	či	k8xC
vylepšování	vylepšování	k1gNnSc2
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Koncepty	koncept	k1gInPc1
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
</s>
<s>
Kognitivismus	Kognitivismus	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Kognitivismus	Kognitivismus	k1gInSc1
(	(	kIx(
<g/>
psychologie	psychologie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
kognitivismus	kognitivismus	k1gInSc4
lze	lze	k6eAd1
označit	označit	k5eAaPmF
přístup	přístup	k1gInSc4
v	v	k7c6
psychologii	psychologie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
reagoval	reagovat	k5eAaBmAgMnS
na	na	k7c4
behaviorismus	behaviorismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kognitivismus	Kognitivismus	k1gInSc1
rozšiřuje	rozšiřovat	k5eAaImIp3nS
behaviorismus	behaviorismus	k1gInSc4
v	v	k7c6
tom	ten	k3xDgInSc6
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
nezkoumá	zkoumat	k5eNaImIp3nS
mysl	mysl	k1gFnSc4
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
černou	černý	k2eAgFnSc4d1
skříňku	skříňka	k1gFnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
ale	ale	k8xC
akceptuje	akceptovat	k5eAaBmIp3nS
existenci	existence	k1gFnSc4
a	a	k8xC
smysluplnost	smysluplnost	k1gFnSc4
zkoumání	zkoumání	k1gNnSc2
mentálních	mentální	k2eAgInPc2d1
konceptů	koncept	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kognitivismus	Kognitivismus	k1gInSc1
bývá	bývat	k5eAaImIp3nS
považován	považován	k2eAgInSc1d1
za	za	k7c4
synonymní	synonymní	k2eAgNnSc4d1
ke	k	k7c3
komputacionalismu	komputacionalismus	k1gInSc3
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
že	že	k8xS
na	na	k7c6
určité	určitý	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
abstrakce	abstrakce	k1gFnSc2
lidská	lidský	k2eAgFnSc1d1
mysl	mysl	k1gFnSc1
je	být	k5eAaImIp3nS
počítač	počítač	k1gInSc1
–	–	k?
výpočetní	výpočetní	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravdivost	pravdivost	k1gFnSc1
tohoto	tento	k3xDgNnSc2
tvrzení	tvrzení	k1gNnSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c4
definici	definice	k1gFnSc4
výpočtu	výpočet	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
pojata	pojmout	k5eAaPmNgFnS
velmi	velmi	k6eAd1
široce	široko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možný	k2eAgFnSc1d1
odpověď	odpověď	k1gFnSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
sdílejí	sdílet	k5eAaImIp3nP
s	s	k7c7
počítači	počítač	k1gInSc6
určitou	určitý	k2eAgFnSc4d1
esenciální	esenciální	k2eAgFnSc4d1
či	či	k8xC
konstitutivní	konstitutivní	k2eAgFnSc4d1
vlastnost	vlastnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vymezuje	vymezovat	k5eAaImIp3nS
počítače	počítač	k1gInPc4
jako	jako	k8xC,k8xS
koherentní	koherentní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
je	být	k5eAaImIp3nS
touto	tento	k3xDgFnSc7
vlastností	vlastnost	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
empirická	empirický	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
je	být	k5eAaImIp3nS
však	však	k9
počítač	počítač	k1gInSc1
chápán	chápán	k2eAgInSc1d1
jakožto	jakožto	k8xS
formální	formální	k2eAgInSc1d1
systém	systém	k1gInSc1
manipulující	manipulující	k2eAgInSc1d1
se	s	k7c7
symboly	symbol	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reprezentace	reprezentace	k1gFnPc1
a	a	k8xC
výpočty	výpočet	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Výpočetní	výpočetní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
mysli	mysl	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1
neuronová	neuronový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
je	být	k5eAaImIp3nS
příkladem	příklad	k1gInSc7
konekcionistického	konekcionistický	k2eAgInSc2d1
modelu	model	k1gInSc2
</s>
<s>
Centrální	centrální	k2eAgFnSc7d1
hypotézou	hypotéza	k1gFnSc7
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
je	být	k5eAaImIp3nS
komputačně-reprezentační	komputačně-reprezentační	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
mysli	mysl	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
této	tento	k3xDgFnSc2
velmi	velmi	k6eAd1
obecné	obecný	k2eAgFnSc2d1
hypotézy	hypotéza	k1gFnSc2
lze	lze	k6eAd1
mysl	mysl	k1gFnSc4
nejlépe	dobře	k6eAd3
chápat	chápat	k5eAaImF
jakožto	jakožto	k8xS
obsahující	obsahující	k2eAgNnSc4d1
množství	množství	k1gNnSc4
reprezentací	reprezentace	k1gFnPc2
<g/>
,	,	kIx,
mentálních	mentální	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
analogických	analogický	k2eAgInPc2d1
k	k	k7c3
počítačovým	počítačový	k2eAgFnPc3d1
datovým	datový	k2eAgFnPc3d1
strukturám	struktura	k1gFnPc3
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgNnPc6,k3yRgNnPc6,k3yIgNnPc6
operují	operovat	k5eAaImIp3nP
výpočetní	výpočetní	k2eAgFnPc4d1
procedury	procedura	k1gFnPc4
<g/>
,	,	kIx,
analogy	analog	k1gInPc4
počítačových	počítačový	k2eAgInPc2d1
algoritmů	algoritmus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povaha	povaha	k1gFnSc1
těchto	tento	k3xDgFnPc2
reprezentací	reprezentace	k1gFnPc2
a	a	k8xC
procedur	procedura	k1gFnPc2
je	být	k5eAaImIp3nS
předmětem	předmět	k1gInSc7
zkoumání	zkoumání	k1gNnSc2
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
navržených	navržený	k2eAgFnPc2d1
reprezentací	reprezentace	k1gFnPc2
má	mít	k5eAaImIp3nS
symbolický	symbolický	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
subsymbolického	subsymbolický	k2eAgInSc2d1
konekcionismu	konekcionismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
inspirován	inspirován	k2eAgInSc1d1
paralelním	paralelní	k2eAgNnSc7d1
distribuovaným	distribuovaný	k2eAgNnSc7d1
zpracováním	zpracování	k1gNnSc7
v	v	k7c6
biologických	biologický	k2eAgFnPc6d1
neuronových	neuronový	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
tak	tak	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
s	s	k7c7
trojitou	trojitý	k2eAgFnSc7d1
analogií	analogie	k1gFnSc7
mezi	mezi	k7c7
myslí	mysl	k1gFnSc7
<g/>
,	,	kIx,
mozkem	mozek	k1gInSc7
a	a	k8xC
počítačem	počítač	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
principu	princip	k1gInSc6
reprezentací	reprezentace	k1gFnPc2
a	a	k8xC
procedur	procedura	k1gFnPc2
jsou	být	k5eAaImIp3nP
založené	založený	k2eAgFnPc1d1
kognitivní	kognitivní	k2eAgFnPc1d1
architektury	architektura	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
modelovat	modelovat	k5eAaImF
různé	různý	k2eAgFnPc4d1
kognitivní	kognitivní	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
inteligentních	inteligentní	k2eAgInPc2d1
agentů	agens	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
snaha	snaha	k1gFnSc1
reflektuje	reflektovat	k5eAaImIp3nS
předpoklad	předpoklad	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
skutečné	skutečný	k2eAgFnPc1d1
porozumění	porozumění	k1gNnSc2
kognitivním	kognitivní	k2eAgInPc3d1
procesům	proces	k1gInPc3
předpokládá	předpokládat	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
je	být	k5eAaImIp3nS
implementovat	implementovat	k5eAaImF
v	v	k7c6
umělém	umělý	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
přístupy	přístup	k1gInPc4
kombinují	kombinovat	k5eAaImIp3nP
více	hodně	k6eAd2
typů	typ	k1gInPc2
reprezentací	reprezentace	k1gFnSc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
symbolických	symbolický	k2eAgInPc2d1
a	a	k8xC
subsymbolických	subsymbolický	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známý	k2eAgInPc1d1
příklady	příklad	k1gInPc1
kognitivních	kognitivní	k2eAgFnPc2d1
architektur	architektura	k1gFnPc2
jsou	být	k5eAaImIp3nP
ACT-R	ACT-R	k1gFnSc4
nebo	nebo	k8xC
Soar	Soar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
ohledně	ohledně	k7c2
povahy	povaha	k1gFnSc2
reprezentací	reprezentace	k1gFnPc2
a	a	k8xC
výpočtů	výpočet	k1gInPc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Formální	formální	k2eAgFnSc1d1
logika	logika	k1gFnSc1
–	–	k?
Výroková	výrokový	k2eAgFnSc1d1
a	a	k8xC
predikátová	predikátový	k2eAgFnSc1d1
logika	logika	k1gFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
vyjádřit	vyjádřit	k5eAaPmF
komplexní	komplexní	k2eAgFnPc4d1
formy	forma	k1gFnPc4
znalostí	znalost	k1gFnPc2
a	a	k8xC
operací	operace	k1gFnPc2
nad	nad	k7c7
nimi	on	k3xPp3gMnPc7
je	být	k5eAaImIp3nS
například	například	k6eAd1
logická	logický	k2eAgFnSc1d1
dedukce	dedukce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
však	však	k9
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
logika	logika	k1gFnSc1
stála	stát	k5eAaImAgFnS
v	v	k7c6
jádru	jádro	k1gNnSc6
lidského	lidský	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1
–	–	k?
Mnoho	mnoho	k4c1
lidských	lidský	k2eAgFnPc2d1
znalostí	znalost	k1gFnPc2
je	být	k5eAaImIp3nS
ve	v	k7c6
formě	forma	k1gFnSc6
jestliže-pak	jestliže-pak	k1gInSc1
(	(	kIx(
<g/>
If-Then	If-Then	k1gInSc1
<g/>
)	)	kIx)
pravidel	pravidlo	k1gNnPc2
a	a	k8xC
různé	různý	k2eAgFnPc4d1
formy	forma	k1gFnPc4
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
plánování	plánování	k1gNnSc1
<g/>
,	,	kIx,
lze	lze	k6eAd1
modelovat	modelovat	k5eAaImF
jako	jako	k8xS,k8xC
systém	systém	k1gInSc4
založený	založený	k2eAgInSc4d1
na	na	k7c6
pravidlech	pravidlo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Koncepty	koncept	k1gInPc1
(	(	kIx(
<g/>
pojmy	pojem	k1gInPc1
<g/>
)	)	kIx)
–	–	k?
Koncepty	koncept	k1gInPc7
jsou	být	k5eAaImIp3nP
mentální	mentální	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
částečně	částečně	k6eAd1
korepondující	korepondující	k2eAgFnSc1d1
se	s	k7c7
slovy	slovo	k1gNnPc7
a	a	k8xC
výrazy	výraz	k1gInPc7
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
více	hodně	k6eAd2
teorií	teorie	k1gFnSc7
konceptů	koncept	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
jednoho	jeden	k4xCgInSc2
pohledu	pohled	k1gInSc2
je	být	k5eAaImIp3nS
koncept	koncept	k1gInSc4
množina	množina	k1gFnSc1
typických	typický	k2eAgFnPc2d1
vlastností	vlastnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
konceptům	koncept	k1gInPc3
pak	pak	k6eAd1
můžeme	moct	k5eAaImIp1nP
organizovat	organizovat	k5eAaBmF
vnímaný	vnímaný	k2eAgInSc4d1
svět	svět	k1gInSc4
a	a	k8xC
v	v	k7c6
mysli	mysl	k1gFnSc6
můžeme	moct	k5eAaImIp1nP
s	s	k7c7
koncepty	koncept	k1gInPc7
operovat	operovat	k5eAaImF
pomocí	pomocí	k7c2
k	k	k7c3
nim	on	k3xPp3gNnPc3
přiřazeným	přiřazený	k2eAgNnPc3d1
slovům	slovo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s>
Analogie	analogie	k1gFnSc1
–	–	k?
Analogie	analogie	k1gFnSc2
jsou	být	k5eAaImIp3nP
užitečným	užitečný	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
myšlení	myšlení	k1gNnSc2
např.	např.	kA
při	při	k7c6
řešení	řešení	k1gNnSc6
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
rozhodování	rozhodování	k1gNnSc2
<g/>
,	,	kIx,
vysvětlování	vysvětlování	k1gNnSc2
a	a	k8xC
komunikaci	komunikace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mentální	mentální	k2eAgInPc1d1
procesy	proces	k1gInPc1
umožňují	umožňovat	k5eAaImIp3nP
vybavit	vybavit	k5eAaPmF
zdrojový	zdrojový	k2eAgInSc4d1
analog	analog	k1gInSc4
<g/>
,	,	kIx,
upravit	upravit	k5eAaPmF
ho	on	k3xPp3gMnSc4
a	a	k8xC
aplikovat	aplikovat	k5eAaBmF
na	na	k7c4
cílovou	cílový	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Představy	představ	k1gInPc1
–	–	k?
Vizuální	vizuální	k2eAgInPc1d1
a	a	k8xC
jiné	jiný	k2eAgInPc1d1
druhy	druh	k1gInPc1
představ	představa	k1gFnPc2
často	často	k6eAd1
zachycují	zachycovat	k5eAaImIp3nP
informace	informace	k1gFnPc4
mnohem	mnohem	k6eAd1
lépe	dobře	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
slovní	slovní	k2eAgInPc1d1
popisy	popis	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
představami	představa	k1gFnPc7
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
v	v	k7c6
mysli	mysl	k1gFnSc6
manipulovat	manipulovat	k5eAaImF
<g/>
,	,	kIx,
přibližovat	přibližovat	k5eAaImF
je	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
rotovat	rotovat	k5eAaImF
<g/>
,	,	kIx,
transformovat	transformovat	k5eAaBmF
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Konekcionismus	Konekcionismus	k1gInSc4
–	–	k?
Konekcionistické	Konekcionistický	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
uzlů	uzel	k1gInPc2
a	a	k8xC
spojů	spoj	k1gInPc2
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
užitečnými	užitečný	k2eAgInPc7d1
modely	model	k1gInPc7
psychologických	psychologický	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
vyžadují	vyžadovat	k5eAaImIp3nP
paralelní	paralelní	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
např.	např.	kA
zrakové	zrakový	k2eAgNnSc1d1
vnímání	vnímání	k1gNnSc1
a	a	k8xC
rozhodování	rozhodování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
šíření	šíření	k1gNnSc2
aktivace	aktivace	k1gFnSc2
a	a	k8xC
učení	učení	k1gNnSc2
sítě	síť	k1gFnSc2
může	moct	k5eAaImIp3nS
vést	vést	k5eAaImF
k	k	k7c3
emergenci	emergence	k1gFnSc3
složitého	složitý	k2eAgNnSc2d1
chování	chování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konekcionistické	Konekcionistický	k2eAgInPc1d1
modely	model	k1gInPc1
jako	jako	k9
jsou	být	k5eAaImIp3nP
umělé	umělý	k2eAgFnPc1d1
neuronové	neuronový	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
hrubou	hrubý	k2eAgFnSc7d1
aproximací	aproximace	k1gFnSc7
biologických	biologický	k2eAgFnPc2d1
neuronových	neuronový	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
,	,	kIx,
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
však	však	k9
i	i	k9
více	hodně	k6eAd2
biologicky	biologicky	k6eAd1
realistické	realistický	k2eAgInPc4d1
modely	model	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
hlavních	hlavní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
kritiky	kritika	k1gFnSc2
tohoto	tento	k3xDgInSc2
přístupu	přístup	k1gInSc2
k	k	k7c3
mysli	mysl	k1gFnSc3
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zanedbávání	zanedbávání	k1gNnSc1
role	role	k1gFnSc2
emocí	emoce	k1gFnPc2
v	v	k7c6
lidském	lidský	k2eAgNnSc6d1
myšlení	myšlení	k1gNnSc6
</s>
<s>
Zanedbávání	zanedbávání	k1gNnSc1
problému	problém	k1gInSc2
vědomí	vědomí	k1gNnSc2
</s>
<s>
Zanedbávání	zanedbávání	k1gNnSc1
vlivu	vliv	k1gInSc2
fyzického	fyzický	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
na	na	k7c4
lidské	lidský	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
</s>
<s>
Zanedbávání	zanedbávání	k1gNnSc1
vlivu	vliv	k1gInSc2
vtělenosti	vtělenost	k1gFnSc2
lidské	lidský	k2eAgFnSc2d1
mysli	mysl	k1gFnSc2
</s>
<s>
Zanedbávání	zanedbávání	k1gNnSc1
sociálního	sociální	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
myšlení	myšlení	k1gNnSc2
</s>
<s>
Kritika	kritika	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
mysl	mysl	k1gFnSc1
je	být	k5eAaImIp3nS
dynamický	dynamický	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
nikoli	nikoli	k9
výpočetní	výpočetní	k2eAgInSc4d1
systém	systém	k1gInSc4
</s>
<s>
Kritika	kritika	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
mysl	mysl	k1gFnSc1
nemá	mít	k5eNaImIp3nS
výpočetní	výpočetní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
matematickém	matematický	k2eAgInSc6d1
významu	význam	k1gInSc6
</s>
<s>
Paul	Paul	k1gMnSc1
Thagard	Thagard	k1gMnSc1
argumentuje	argumentovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
komputačně-reprezentační	komputačně-reprezentační	k2eAgInSc1d1
přístup	přístup	k1gInSc1
díky	díky	k7c3
těmto	tento	k3xDgFnPc3
kritikám	kritika	k1gFnPc3
rozšířit	rozšířit	k5eAaPmF
a	a	k8xC
zdokonalit	zdokonalit	k5eAaPmF
<g/>
,	,	kIx,
spíše	spíše	k9
než	než	k8xS
ho	on	k3xPp3gMnSc4
zcela	zcela	k6eAd1
zavrhnout	zavrhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
THAGARD	THAGARD	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
kognitivní	kognitivní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
:	:	kIx,
mysl	mysl	k1gFnSc1
a	a	k8xC
myšlení	myšlení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7178	#num#	k4
<g/>
-	-	kIx~
<g/>
445	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Thagard	Thagarda	k1gFnPc2
<g/>
,	,	kIx,
Paul	Paula	k1gFnPc2
<g/>
,	,	kIx,
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
The	The	k1gMnPc1
Stanford	Stanforda	k1gFnPc2
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosopha	k1gMnSc2
(	(	kIx(
<g/>
Fall	Fall	k1gMnSc1
2012	#num#	k4
Edition	Edition	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
N.	N.	kA
Zalta	Zalta	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Miller	Miller	k1gMnSc1
<g/>
,	,	kIx,
G.	G.	kA
A.	A.	kA
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
cognitive	cognitiv	k1gInSc5
revolution	revolution	k1gInSc1
<g/>
:	:	kIx,
a	a	k8xC
historical	historicat	k5eAaPmAgMnS
perspective	perspectiv	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trends	Trends	k1gInSc1
in	in	k?
Cognitive	Cognitiv	k1gInSc5
Sciences	Sciences	k1gMnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
,	,	kIx,
141	#num#	k4
<g/>
-	-	kIx~
<g/>
144	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wundt	Wundt	k1gInSc1
<g/>
,	,	kIx,
W.	W.	kA
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Völkerpsychologie	Völkerpsychologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eine	Eine	k1gNnSc1
Untersuchung	Untersuchunga	k1gFnPc2
der	drát	k5eAaImRp2nS
Entwicklungsgesetze	Entwicklungsgesetze	k1gFnPc4
von	von	k1gInSc4
Sprache	Sprache	k1gNnSc1
<g/>
,	,	kIx,
Mythus	mythus	k1gInSc1
und	und	k?
Sitte	Sitt	k1gInSc5
<g/>
.	.	kIx.
<g/>
Leipzig	Leipzig	k1gMnSc1
<g/>
:	:	kIx,
Kröner	Kröner	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Watson	Watson	k1gInSc1
<g/>
,	,	kIx,
J.	J.	kA
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychology	psycholog	k1gMnPc7
as	as	k1gNnSc1
the	the	k?
behaviorist	behaviorist	k1gMnSc1
views	views	k6eAd1
it.	it.	k?
Psychological	Psychological	k1gFnSc1
Review	Review	k1gFnSc1
20	#num#	k4
<g/>
,	,	kIx,
158	#num#	k4
<g/>
–	–	k?
<g/>
177	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Newell	Newell	k1gInSc1
<g/>
,	,	kIx,
A.	A.	kA
and	and	k?
Simon	Simon	k1gMnSc1
<g/>
,	,	kIx,
H.A.	H.A.	k1gMnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Human	Humana	k1gFnPc2
Problem	Probl	k1gInSc7
Solving	Solving	k1gInSc1
<g/>
,	,	kIx,
Prentice-Hall	Prentice-Hall	k1gInSc1
<g/>
↑	↑	k?
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
Society	societa	k1gFnSc2
<g/>
↑	↑	k?
Walsh	Walsh	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
<g/>
;	;	kIx,
Rushworth	Rushworth	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	A	kA
primer	primera	k1gFnPc2
of	of	k?
magnetic	magnetice	k1gFnPc2
stimulation	stimulation	k1gInSc4
as	as	k1gNnPc2
a	a	k8xC
tool	toolum	k1gNnPc2
for	forum	k1gNnPc2
neuropsychology	neuropsycholog	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neuropsychologia	Neuropsychologia	k1gFnSc1
37	#num#	k4
<g/>
,	,	kIx,
125	#num#	k4
<g/>
-	-	kIx~
<g/>
135	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GALLAGHER	GALLAGHER	kA
<g/>
,	,	kIx,
Shaun	Shaun	k1gMnSc1
<g/>
;	;	kIx,
ZAHAVI	ZAHAVI	kA
<g/>
,	,	kIx,
Dan	Dan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Phenomenological	Phenomenological	k1gMnSc1
Mind	Mind	k1gMnSc1
:	:	kIx,
an	an	k?
introduction	introduction	k1gInSc1
to	ten	k3xDgNnSc1
philosophy	philosopha	k1gFnPc1
of	of	k?
mind	mind	k1gMnSc1
and	and	k?
cognitive	cognitiv	k1gInSc5
science	scienec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
and	and	k?
New	New	k1gFnSc1
York	York	k1gInSc1
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
257	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
203	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8659	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wilson	Wilson	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
A.	A.	kA
<g/>
;	;	kIx,
Frank	Frank	k1gMnSc1
C.	C.	kA
Keil	Keil	k1gMnSc1
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
The	The	k1gFnSc1
MIT	MIT	kA
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
the	the	k?
Cognitive	Cognitiv	k1gInSc5
Sciences	Sciences	k1gMnSc1
(	(	kIx(
<g/>
MITECS	MITECS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Mass	Mass	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
262	#num#	k4
<g/>
-	-	kIx~
<g/>
73144	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
↑	↑	k?
Thagard	Thagard	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mind	Mind	k1gInSc1
<g/>
:	:	kIx,
Introduction	Introduction	k1gInSc1
to	ten	k3xDgNnSc1
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
second	second	k1gInSc1
edition	edition	k1gInSc1
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
MA	MA	kA
<g/>
:	:	kIx,
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Seznam	seznam	k1gInSc1
univerzitních	univerzitní	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
kognitivní	kognitivní	k2eAgFnSc7d1
vědou	věda	k1gFnSc7
</s>
<s>
Cognitive	Cognitiv	k1gInSc5
Science	Science	k1gFnSc1
in	in	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
</s>
<s>
Společnost	společnost	k1gFnSc1
pro	pro	k7c4
kognitivní	kognitivní	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
a	a	k8xC
filosofii	filosofie	k1gFnSc4
</s>
<s>
Obor	obor	k1gInSc1
Kognitivní	kognitivní	k2eAgFnSc1d1
informatika	informatika	k1gFnSc1
na	na	k7c4
VŠE	všechen	k3xTgNnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Program	program	k1gInSc1
Kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Komenského	Komenský	k1gMnSc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
</s>
<s>
Kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Co	co	k8xS
že	že	k8xS
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
Stručný	stručný	k2eAgInSc1d1
přehledový	přehledový	k2eAgInSc1d1
článek	článek	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4193780-6	4193780-6	k4
</s>
