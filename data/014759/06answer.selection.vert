<s>
Kognitivní	kognitivní	k2eAgFnSc1d1
věda	věda	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
cognoscere	cognoscrat	k5eAaPmIp3nS
=	=	kIx~
poznávat	poznávat	k5eAaImF
<g/>
)	)	kIx)
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
interdisciplinárním	interdisciplinární	k2eAgInSc7d1
výzkumem	výzkum	k1gInSc7
mysli	mysl	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gInPc2
procesů	proces	k1gInPc2
<g/>
.	.	kIx.
</s>