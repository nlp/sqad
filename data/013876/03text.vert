<s>
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgInSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgInSc1d1
</s>
<s>
zweibrückenský	zweibrückenský	k2eAgMnSc1d1
falckrabě	falckrabě	k1gMnSc1
</s>
<s>
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgInSc1d1
<g/>
,	,	kIx,
Hyacinthe	Hyacinthe	k1gNnSc1
Rigaud	Rigauda	k1gFnPc2
<g/>
,	,	kIx,
1706	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1674	#num#	k4
</s>
<s>
Štrasburk	Štrasburk	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1735	#num#	k4
(	(	kIx(
<g/>
60	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Zweibrücken	Zweibrücken	k1gInSc1
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Nasavsko-Saarbrückenská	Nasavsko-Saarbrückenský	k2eAgFnSc1d1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Karolína	Karolína	k1gFnSc1
Falcko-ZweibrückenskáKristián	Falcko-ZweibrückenskáKristián	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-ZweibrückenskýFridrich	Falcko-ZweibrückenskýFridrich	k1gMnSc1
Michal	Michal	k1gMnSc1
Falcko-ZweibrückenskýKristýna	Falcko-ZweibrückenskýKristýna	k1gFnSc1
Henrieta	Henrieta	k1gFnSc1
Falcko-Zweibrückenská	Falcko-Zweibrückenský	k2eAgFnSc1d1
</s>
<s>
Rod	rod	k1gInSc1
</s>
<s>
Wittelsbachové	Wittelsbachové	k2eAgFnSc1d1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Kristián	Kristián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückensko-Birkenfeldský	Falcko-Zweibrückensko-Birkenfeldský	k2eAgInSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Agáta	Agáta	k1gFnSc1
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1674	#num#	k4
<g/>
,	,	kIx,
Štrasburk	Štrasburk	k1gInSc1
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1735	#num#	k4
<g/>
,	,	kIx,
Zweibrücken	Zweibrücken	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
šlechtic	šlechtic	k1gMnSc1
<g/>
,	,	kIx,
člen	člen	k1gMnSc1
falcko-zweibrückensko-birkenfeldského	falcko-zweibrückensko-birkenfeldský	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
vedlejší	vedlejší	k2eAgFnPc1d1
větve	větev	k1gFnPc1
rodu	rod	k1gInSc2
Wittelsbachů	Wittelsbach	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
syn	syn	k1gMnSc1
Kristiána	Kristián	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückensko-Birkenfeldského	Falcko-Zweibrückensko-Birkenfeldský	k2eAgNnSc2d1
a	a	k8xC
Kateřiny	Kateřina	k1gFnSc2
Agáty	Agáta	k1gFnSc2
<g/>
,	,	kIx,
hraběnky	hraběnka	k1gFnSc2
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1717	#num#	k4
až	až	k9
1731	#num#	k4
vládl	vládnout	k5eAaImAgInS
jako	jako	k9
zweibrückensko-birkenfeldský	zweibrückensko-birkenfeldský	k2eAgMnSc1d1
falckrabě	falckrabě	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1731	#num#	k4
zdědil	zdědit	k5eAaPmAgInS
samostatné	samostatný	k2eAgNnSc4d1
falcko-zweibrückenské	falcko-zweibrückenský	k2eAgNnSc4d1
vévodství	vévodství	k1gNnSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
zweibrückenským	zweibrückenský	k2eAgMnSc7d1
falckrabětem	falckrabě	k1gMnSc7
a	a	k8xC
vévodou	vévoda	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1699	#num#	k4
byl	být	k5eAaImAgInS
také	také	k9
hrabětem	hrabě	k1gMnSc7
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Kristián	Kristián	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1674	#num#	k4
ve	v	k7c6
Štrasburku	Štrasburk	k1gInSc6
jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
falckraběte	falckrabě	k1gMnSc2
Kristiána	Kristián	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zweibrückensko-Birkenfeldského	Zweibrückensko-Birkenfeldský	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
dožil	dožít	k5eAaPmAgInS
dospělosti	dospělost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
kariéra	kariéra	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
alsaském	alsaský	k2eAgInSc6d1
regimentu	regiment	k1gInSc6
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1697	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1699	#num#	k4
zdědil	zdědit	k5eAaPmAgInS
po	po	k7c6
své	svůj	k3xOyFgFnSc6
matce	matka	k1gFnSc6
hrabství	hrabství	k1gNnSc2
Rappoltstein	Rappoltsteina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1702	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
polním	polní	k2eAgMnSc7d1
maršálem	maršál	k1gMnSc7
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1704	#num#	k4
byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
generálporučíka	generálporučík	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojensky	vojensky	k6eAd1
exceloval	excelovat	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1708	#num#	k4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Oudenaarde	Oudenaard	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1717	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
Kristián	Kristián	k1gMnSc1
opustil	opustit	k5eAaPmAgMnS
armádu	armáda	k1gFnSc4
a	a	k8xC
převzal	převzít	k5eAaPmAgMnS
správu	správa	k1gFnSc4
nad	nad	k7c7
Zweibrückenem-Birkenfeldem	Zweibrückenem-Birkenfeldo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
malou	malý	k2eAgFnSc7d1
částí	část	k1gFnSc7
Sponheimského	Sponheimský	k2eAgNnSc2d1
hrabství	hrabství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1731	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
bezdětný	bezdětný	k2eAgMnSc1d1
falckrabě	falckrabě	k1gMnSc1
a	a	k8xC
vévoda	vévoda	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Zweibrückenský	Zweibrückenský	k2eAgMnSc1d1
a	a	k8xC
Kristián	Kristián	k1gMnSc1
zdědil	zdědit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc4
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
příbuzní	příbuzný	k1gMnPc1
protestovali	protestovat	k5eAaBmAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
smlouva	smlouva	k1gFnSc1
s	s	k7c7
falckým	falcký	k2eAgMnSc7d1
kurfiřtem	kurfiřt	k1gMnSc7
Karlem	Karel	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filipem	Filip	k1gMnSc7
<g/>
,	,	kIx,
uzavřená	uzavřený	k2eAgFnSc1d1
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1733	#num#	k4
v	v	k7c6
Mannheimu	Mannheim	k1gInSc6
<g/>
,	,	kIx,
dovolila	dovolit	k5eAaPmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Kristián	Kristián	k1gMnSc1
obdržel	obdržet	k5eAaPmAgMnS
Falc-Zweibrücken	Falc-Zweibrücken	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kristián	Kristián	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
necelé	celý	k2eNgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
podepsání	podepsání	k1gNnSc6
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1735	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
60	#num#	k4
let	léto	k1gNnPc2
ve	v	k7c6
Zweibrückenu	Zweibrücken	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřben	pohřben	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
Alexandrově	Alexandrův	k2eAgInSc6d1
kostele	kostel	k1gInSc6
tamtéž	tamtéž	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
potomci	potomek	k1gMnPc1
</s>
<s>
Kristián	Kristián	k1gMnSc1
se	s	k7c7
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1719	#num#	k4
v	v	k7c6
téměř	téměř	k6eAd1
pětačtyřiceti	pětačtyřicet	k4xCc6
letech	rok	k1gInPc6
na	na	k7c6
zámku	zámek	k1gInSc6
Lorentzen	Lorentzen	k1gInSc1
oženil	oženit	k5eAaPmAgInS
s	s	k7c7
o	o	k7c4
třicet	třicet	k4xCc4
let	rok	k1gInPc2
mladší	mladý	k2eAgFnSc7d2
Karolínou	Karolína	k1gFnSc7
Nasavsko-Saarbrückenskou	Nasavsko-Saarbrückenský	k1gFnSc7xF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželům	manžel	k1gMnPc3
se	se	k3xPyFc4
za	za	k7c4
šestnáct	šestnáct	k4xCc4
let	léto	k1gNnPc2
manželství	manželství	k1gNnPc2
narodily	narodit	k5eAaPmAgFnP
čtyři	čtyři	k4xCgInPc1
dětiː	dětiː	k?
</s>
<s>
Karolína	Karolína	k1gFnSc1
Falcko-Zweibrückenská	Falcko-Zweibrückenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1721	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1774	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kristián	Kristián	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1722	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1775	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Michal	Michal	k1gMnSc1
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgMnSc1d1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1724	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1767	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
Henrieta	Henrieta	k1gFnSc1
Falcko-Zweibrückenská	Falcko-Zweibrückenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1725	#num#	k4
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1816	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Wolfgang	Wolfgang	k1gMnSc1
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
Falcko-Zweibrückensko-Birkenfeldský	Falcko-Zweibrückensko-Birkenfeldský	k2eAgMnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Hesenská	hesenský	k2eAgFnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
I.	I.	kA
Falcko-Zweibrückensko-Bischweilerský	Falcko-Zweibrückensko-Bischweilerský	k2eAgMnSc5d1
</s>
<s>
Vilém	Vilém	k1gMnSc1
Brunšvicko-Lüneburský	Brunšvicko-Lüneburský	k2eAgMnSc1d1
</s>
<s>
Dorotea	Dorote	k2eAgFnSc1d1
Brunšvicko-Lüneburská	Brunšvicko-Lüneburský	k2eAgFnSc1d1
</s>
<s>
Dorothea	Dorothe	k2eAgFnSc1d1
Dánská	dánský	k2eAgFnSc1d1
</s>
<s>
Kristián	Kristián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückensko-Birkenfeldský	Falcko-Zweibrückensko-Birkenfeldský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
I.	I.	kA
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgMnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgInSc1d1
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
z	z	k7c2
Jülichu-Cleves-Bergu	Jülichu-Cleves-Berg	k1gInSc2
</s>
<s>
Magdalena	Magdalena	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Falcko-Zweibrückenská	Falcko-Zweibrückenský	k2eAgFnSc1d1
</s>
<s>
René	René	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
z	z	k7c2
Rohanu	Rohan	k1gMnSc6
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
de	de	k?
Rohan	Rohan	k1gMnSc1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
de	de	k?
Parthenay	Parthenaa	k1gFnSc2
</s>
<s>
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgInSc1d1
</s>
<s>
Egenolf	Egenolf	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
</s>
<s>
Eberhard	Eberhard	k1gInSc1
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Erbachu	Erbach	k1gInSc2
</s>
<s>
Johan	Johan	k1gMnSc1
Jakub	Jakub	k1gMnSc1
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
</s>
<s>
Ota	Ota	k1gMnSc1
I.	I.	kA
ze	z	k7c2
Salm-Kyrburgu	Salm-Kyrburg	k1gInSc2
</s>
<s>
Anna	Anna	k1gFnSc1
ze	z	k7c2
Salm-Kyrburgu	Salm-Kyrburg	k1gInSc2
</s>
<s>
Otýlie	Otýlie	k1gFnSc1
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Agáta	Agáta	k1gFnSc1
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
</s>
<s>
Ota	Ota	k1gMnSc1
I.	I.	kA
ze	z	k7c2
Salm-Kyrburgu	Salm-Kyrburg	k1gInSc2
</s>
<s>
Johan	Johan	k1gMnSc1
Kazimír	Kazimír	k1gMnSc1
ze	z	k7c2
Salm-Kyrburgu	Salm-Kyrburg	k1gInSc2
</s>
<s>
Otýlie	Otýlie	k1gFnSc1
Nasavsko-Weilburská	Nasavsko-Weilburský	k2eAgFnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Klaudie	Klaudie	k1gFnSc1
ze	z	k7c2
Salm-Kyrburgu	Salm-Kyrburg	k1gInSc2
</s>
<s>
Johan	Johan	k1gMnSc1
Jiří	Jiří	k1gMnSc1
I.	I.	kA
ze	z	k7c2
Solms-Laubachu	Solms-Laubach	k1gInSc2
</s>
<s>
Dorotea	Dorotea	k6eAd1
ze	z	k7c2
Solms-Laubachu	Solms-Laubach	k1gInSc2
</s>
<s>
Markéta	Markéta	k1gFnSc1
ze	z	k7c2
Schonbourg-Glauchau	Schonbourg-Glauchaus	k1gInSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Christian	Christian	k1gMnSc1
III	III	kA
<g/>
,	,	kIx,
Count	Count	k1gMnSc1
Palatine	Palatin	k1gInSc5
of	of	k?
Zweibrücken	Zweibrücken	k2eAgMnSc1d1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kristián	Kristián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falcko-Zweibrückenský	Falcko-Zweibrückenský	k2eAgInSc4d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Kateřina	Kateřina	k1gFnSc1
Agáta	Agáta	k1gFnSc1
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
</s>
<s>
Hrabě	Hrabě	k1gMnSc1
z	z	k7c2
Rappoltsteinu	Rappoltstein	k1gInSc2
1699	#num#	k4
–	–	k?
1735	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Kristián	Kristián	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Kristián	Kristián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1
ze	z	k7c2
Sponheimu	Sponheim	k1gInSc2
1717	#num#	k4
–	–	k?
1735	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Kristián	Kristián	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Gustav	Gustav	k1gMnSc1
</s>
<s>
Zweibrückenský	Zweibrückenský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
1731	#num#	k4
–	–	k?
1735	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Kristián	Kristián	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
120208164	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
67291414	#num#	k4
</s>
