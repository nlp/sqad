<s>
Maieutika	maieutika	k1gFnSc1
</s>
<s>
Maieutika	maieutika	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
umění	umění	k1gNnSc1
porodní	porodní	k2eAgFnSc2d1
báby	bába	k1gFnSc2
<g/>
;	;	kIx,
porodnické	porodnický	k2eAgNnSc1d1
umění	umění	k1gNnSc1
–	–	k?
narážka	narážka	k1gFnSc1
na	na	k7c4
povolání	povolání	k1gNnSc4
Sokratovy	Sokratův	k2eAgFnSc2d1
matky	matka	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
termín	termín	k1gInSc4
používaný	používaný	k2eAgInSc4d1
samotným	samotný	k2eAgInSc7d1
Sókratem	Sókrat	k1gInSc7
pro	pro	k7c4
umění	umění	k1gNnSc4
vést	vést	k5eAaImF
dialog	dialog	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maieutika	maieutika	k1gFnSc1
je	být	k5eAaImIp3nS
součást	součást	k1gFnSc4
sokratovské	sokratovský	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gNnSc7
jádrem	jádro	k1gNnSc7
je	být	k5eAaImIp3nS
myšlenka	myšlenka	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
pravda	pravda	k1gFnSc1
je	být	k5eAaImIp3nS
latentně	latentně	k6eAd1
přítomna	přítomen	k2eAgFnSc1d1
v	v	k7c6
rozumu	rozum	k1gInSc6
každého	každý	k3xTgMnSc2
člověka	člověk	k1gMnSc2
už	už	k9
od	od	k7c2
narození	narození	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
„	„	k?
<g/>
přivést	přivést	k5eAaPmF
na	na	k7c4
svět	svět	k1gInSc4
<g/>
“	“	k?
skrz	skrz	k7c4
správně	správně	k6eAd1
položené	položený	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
učitele	učitel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Maieutika	maieutika	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
kroků	krok	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Elenktika	Elenktika	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
„	„	k?
<g/>
umění	umění	k1gNnSc1
převedení	převedení	k1gNnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
Sokrates	Sokrates	k1gMnSc1
otřese	otřást	k5eAaPmIp3nS
pozicí	pozice	k1gFnSc7
svého	svůj	k3xOyFgMnSc2
žáka	žák	k1gMnSc2
a	a	k8xC
nebo	nebo	k8xC
partnera	partner	k1gMnSc2
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
a	a	k8xC
převede	převést	k5eAaPmIp3nS
jeho	jeho	k3xOp3gNnSc4
stanovisko	stanovisko	k1gNnSc4
do	do	k7c2
neřešitelného	řešitelný	k2eNgInSc2d1
stavu	stav	k1gInSc2
(	(	kIx(
<g/>
aporie	aporie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
něm	on	k3xPp3gMnSc6
takto	takto	k6eAd1
probudí	probudit	k5eAaPmIp3nS
touhu	touha	k1gFnSc4
po	po	k7c6
poznání	poznání	k1gNnSc6
a	a	k8xC
hledání	hledání	k1gNnSc6
pravdy	pravda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Protreptika	Protreptika	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
„	„	k?
<g/>
umění	umění	k1gNnSc1
obrácení	obrácení	k1gNnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
Sokrates	Sokrates	k1gMnSc1
přivede	přivést	k5eAaPmIp3nS
žáka	žák	k1gMnSc4
vhodně	vhodně	k6eAd1
položenými	položený	k2eAgFnPc7d1
otázkami	otázka	k1gFnPc7
ke	k	k7c3
správnému	správný	k2eAgInSc3d1
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
k	k	k7c3
poznání	poznání	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
dosažitelné	dosažitelný	k2eAgNnSc1d1
pouze	pouze	k6eAd1
prostřednictvím	prostřednictvím	k7c2
nazření	nazření	k1gNnSc2
idejí	idea	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Cílem	cíl	k1gInSc7
maieutiky	maieutika	k1gFnSc2
u	u	k7c2
Sokrata	Sokrates	k1gMnSc2
a	a	k8xC
Platóna	Platón	k1gMnSc2
je	být	k5eAaImIp3nS
ε	ε	k?
ζ	ζ	k?
(	(	kIx(
<g/>
eu	eu	k?
zén	zén	k?
<g/>
)	)	kIx)
–	–	k?
„	„	k?
<g/>
správně	správně	k6eAd1
<g/>
/	/	kIx~
<g/>
dobře	dobře	k6eAd1
<g/>
/	/	kIx~
<g/>
spravedlivě	spravedlivě	k6eAd1
žít	žít	k5eAaImF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Filosofický	filosofický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
FIN	Fin	k1gMnSc1
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
maieutika	maieutika	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
254	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Maieutika	maieutika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
16	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
633	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4074431-0	4074431-0	k4
</s>
