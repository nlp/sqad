<s>
Policie	policie	k1gFnSc1	policie
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
politia	politium	k1gNnSc2	politium
=	=	kIx~	=
"	"	kIx"	"
<g/>
veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
π	π	k?	π
polis	polis	k1gInSc1	polis
=	=	kIx~	=
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
represivní	represivní	k2eAgFnSc1d1	represivní
složka	složka	k1gFnSc1	složka
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
pořádku	pořádek	k1gInSc2	pořádek
uvnitř	uvnitř	k7c2	uvnitř
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
vnějším	vnější	k2eAgMnSc7d1	vnější
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
ke	k	k7c3	k
stíhání	stíhání	k1gNnSc3	stíhání
pachatelů	pachatel	k1gMnPc2	pachatel
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedemokratických	demokratický	k2eNgFnPc6d1	nedemokratická
zemích	zem	k1gFnPc6	zem
bývá	bývat	k5eAaImIp3nS	bývat
zneužívána	zneužívat	k5eAaImNgFnS	zneužívat
k	k	k7c3	k
potlačování	potlačování	k1gNnSc3	potlačování
a	a	k8xC	a
kriminalizování	kriminalizování	k1gNnSc4	kriminalizování
politických	politický	k2eAgMnPc2d1	politický
odpůrců	odpůrce	k1gMnPc2	odpůrce
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
zrovna	zrovna	k6eAd1	zrovna
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
různých	různý	k2eAgInPc2d1	různý
států	stát	k1gInPc2	stát
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
na	na	k7c4	na
dopadení	dopadení	k1gNnSc4	dopadení
nebezpečných	bezpečný	k2eNgInPc2d1	nebezpečný
kriminálních	kriminální	k2eAgInPc2d1	kriminální
živlů	živel	k1gInPc2	živel
<g/>
,	,	kIx,	,
operujících	operující	k2eAgNnPc2d1	operující
na	na	k7c4	na
území	území	k1gNnSc4	území
více	hodně	k6eAd2	hodně
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Příslušník	příslušník	k1gMnSc1	příslušník
policejního	policejní	k2eAgInSc2d1	policejní
sboru	sbor	k1gInSc2	sbor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
policista	policista	k1gMnSc1	policista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
plní	plnit	k5eAaImIp3nS	plnit
státní	státní	k2eAgFnSc1d1	státní
Policie	policie	k1gFnSc1	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
obecní	obecní	k2eAgFnSc2d1	obecní
(	(	kIx(	(
<g/>
městské	městský	k2eAgFnSc2d1	městská
<g/>
)	)	kIx)	)
policie	policie	k1gFnSc2	policie
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgFnPc2	tento
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
existovalo	existovat	k5eAaImAgNnS	existovat
více	hodně	k6eAd2	hodně
sborů	sbor	k1gInPc2	sbor
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
policejními	policejní	k2eAgFnPc7d1	policejní
pravomocemi	pravomoc	k1gFnPc7	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
policejní	policejní	k2eAgFnSc4d1	policejní
úlohu	úloha	k1gFnSc4	úloha
plnilo	plnit	k5eAaImAgNnS	plnit
četnictvo	četnictvo	k1gNnSc1	četnictvo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
v	v	k7c6	v
obcích	obec	k1gFnPc6	obec
též	tenž	k3xDgFnSc2	tenž
obecní	obecní	k2eAgFnSc2d1	obecní
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
plnila	plnit	k5eAaImAgFnS	plnit
více	hodně	k6eAd2	hodně
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
než	než	k8xS	než
kolik	kolik	k4yIc4	kolik
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byly	být	k5eAaImAgFnP	být
policie	policie	k1gFnPc1	policie
<g/>
,	,	kIx,	,
četnictvo	četnictvo	k1gNnSc1	četnictvo
a	a	k8xC	a
obecní	obecní	k2eAgFnSc2d1	obecní
policie	policie	k1gFnSc2	policie
sloučeny	sloučen	k2eAgMnPc4d1	sloučen
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Sbor	sbor	k1gInSc1	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
dvěma	dva	k4xCgFnPc7	dva
základními	základní	k2eAgFnPc7d1	základní
součástmi	součást	k1gFnPc7	součást
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Veřejnou	veřejný	k2eAgFnSc4d1	veřejná
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
a	a	k8xC	a
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mj.	mj.	kA	mj.
plnila	plnit	k5eAaImAgFnS	plnit
rozvědné	rozvědný	k2eAgInPc4d1	rozvědný
a	a	k8xC	a
kontrarozvědné	kontrarozvědný	k2eAgInPc4d1	kontrarozvědný
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
Sbor	sbor	k1gInSc1	sbor
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
zrušen	zrušen	k2eAgMnSc1d1	zrušen
a	a	k8xC	a
úkoly	úkol	k1gInPc1	úkol
Veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
byly	být	k5eAaImAgFnP	být
převzaty	převzít	k5eAaPmNgFnP	převzít
nově	nově	k6eAd1	nově
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
Policií	policie	k1gFnSc7	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Policejním	policejní	k2eAgInSc7d1	policejní
sborem	sbor	k1gInSc7	sbor
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
nejdříve	dříve	k6eAd3	dříve
živelně	živelně	k6eAd1	živelně
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákonů	zákon	k1gInPc2	zákon
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
obecní	obecní	k2eAgFnPc1d1	obecní
policie	policie	k1gFnPc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
nich	on	k3xPp3gFnPc2	on
působila	působit	k5eAaImAgFnS	působit
Ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
ostraha	ostraha	k1gFnSc1	ostraha
železnic	železnice	k1gFnPc2	železnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Federální	federální	k2eAgFnSc4d1	federální
Železniční	železniční	k2eAgFnSc4d1	železniční
policii	policie	k1gFnSc4	policie
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
na	na	k7c6	na
Železniční	železniční	k2eAgFnSc6d1	železniční
policii	policie	k1gFnSc6	policie
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
Ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
ostraha	ostraha	k1gFnSc1	ostraha
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
úkoly	úkol	k1gInPc4	úkol
převzala	převzít	k5eAaPmAgFnS	převzít
zpět	zpět	k6eAd1	zpět
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
policejní	policejní	k2eAgFnPc4d1	policejní
pravomoci	pravomoc	k1gFnPc4	pravomoc
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
i	i	k8xC	i
závodní	závodní	k2eAgFnSc1d1	závodní
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
