<s>
Policie	policie	k1gFnSc1	policie
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
politia	politium	k1gNnSc2	politium
=	=	kIx~	=
"	"	kIx"	"
<g/>
veřejná	veřejný	k2eAgFnSc1d1	veřejná
správa	správa	k1gFnSc1	správa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
π	π	k?	π
polis	polis	k1gInSc1	polis
=	=	kIx~	=
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
represivní	represivní	k2eAgFnSc1d1	represivní
složka	složka	k1gFnSc1	složka
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
pořádku	pořádek	k1gInSc2	pořádek
uvnitř	uvnitř	k7c2	uvnitř
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
vnějším	vnější	k2eAgMnSc7d1	vnější
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
ke	k	k7c3	k
stíhání	stíhání	k1gNnSc3	stíhání
pachatelů	pachatel	k1gMnPc2	pachatel
trestné	trestný	k2eAgFnSc2d1	trestná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
