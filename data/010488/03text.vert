<p>
<s>
Kahanismus	Kahanismus	k1gInSc1	Kahanismus
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
krajně	krajně	k6eAd1	krajně
pravicová	pravicový	k2eAgFnSc1d1	pravicová
ideologie	ideologie	k1gFnSc1	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
příjmení	příjmení	k1gNnSc2	příjmení
Meira	Meir	k1gMnSc2	Meir
Kahaneho	Kahane	k1gMnSc2	Kahane
<g/>
,	,	kIx,	,
rabína	rabín	k1gMnSc2	rabín
<g/>
,	,	kIx,	,
politika	politik	k1gMnSc2	politik
a	a	k8xC	a
zakladatele	zakladatel	k1gMnSc2	zakladatel
Jewish	Jewisha	k1gFnPc2	Jewisha
Defense	defense	k1gFnSc2	defense
League	Leagu	k1gFnSc2	Leagu
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
strany	strana	k1gFnSc2	strana
Kach	Kacha	k1gFnPc2	Kacha
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
USA	USA	kA	USA
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
potírání	potírání	k1gNnSc2	potírání
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
a	a	k8xC	a
bojem	boj	k1gInSc7	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
pro	pro	k7c4	pro
sovětské	sovětský	k2eAgMnPc4d1	sovětský
Židy	Žid	k1gMnPc4	Žid
založena	založen	k2eAgFnSc1d1	založena
organizace	organizace	k1gFnSc1	organizace
Jewish	Jewisha	k1gFnPc2	Jewisha
Defense	defense	k1gFnSc1	defense
League	League	k1gFnSc1	League
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
Kahanovi	Kahanův	k2eAgMnPc1d1	Kahanův
stoupenci	stoupenec	k1gMnPc1	stoupenec
byli	být	k5eAaImAgMnP	být
sionisté	sionista	k1gMnPc1	sionista
<g/>
,	,	kIx,	,
či	či	k8xC	či
aspoň	aspoň	k9	aspoň
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
termín	termín	k1gInSc1	termín
Kahanismus	Kahanismus	k1gInSc1	Kahanismus
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
označovaly	označovat	k5eAaImAgFnP	označovat
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
názory	názor	k1gInPc1	názor
Meira	Meira	k1gFnSc1	Meira
Kahan	kahan	k1gInSc1	kahan
<g/>
\	\	kIx~	\
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
izraelským	izraelský	k2eAgMnSc7d1	izraelský
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
myšlenky	myšlenka	k1gFnPc1	myšlenka
byly	být	k5eAaImAgFnP	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
Izrael	Izrael	k1gInSc1	Izrael
bránit	bránit	k5eAaImF	bránit
proti	proti	k7c3	proti
arabským	arabský	k2eAgMnPc3d1	arabský
a	a	k8xC	a
nacistickým	nacistický	k2eAgMnPc3d1	nacistický
nepřátelům	nepřítel	k1gMnPc3	nepřítel
<g/>
,	,	kIx,	,
plné	plný	k2eAgNnSc4d1	plné
občanství	občanství	k1gNnSc4	občanství
by	by	kYmCp3nP	by
dostávali	dostávat	k5eAaImAgMnP	dostávat
pouze	pouze	k6eAd1	pouze
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nežidé	nežid	k1gMnPc1	nežid
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
stejná	stejný	k2eAgNnPc4d1	stejné
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
práva	právo	k1gNnSc2	právo
volit	volit	k5eAaImF	volit
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
podřídit	podřídit	k5eAaPmF	podřídit
židovskému	židovský	k2eAgNnSc3d1	Židovské
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
právu	právo	k1gNnSc3	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Kahanism	Kahanisma	k1gFnPc2	Kahanisma
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
