<s>
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1896	[number]	k4	1896
Heroltice	Heroltice	k1gFnSc1	Heroltice
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
komunistický	komunistický	k2eAgMnSc1d1	komunistický
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
poslance	poslanec	k1gMnSc2	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
nejdříve	dříve	k6eAd3	dříve
funkci	funkce	k1gFnSc4	funkce
premiéra	premiéra	k1gFnSc1	premiéra
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnSc2	komunista
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
mnoha	mnoho	k4c2	mnoho
desítek	desítka	k1gFnPc2	desítka
svých	svůj	k3xOyFgMnPc2	svůj
odpůrců	odpůrce	k1gMnPc2	odpůrce
i	i	k8xC	i
nevinných	vinný	k2eNgMnPc2d1	nevinný
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Herolticích	Heroltice	k1gFnPc6	Heroltice
u	u	k7c2	u
Vyškova	Vyškov	k1gInSc2	Vyškov
jako	jako	k8xS	jako
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
chudé	chudý	k2eAgFnSc2d1	chudá
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
dělnice	dělnice	k1gFnSc2	dělnice
Marie	Maria	k1gFnSc2	Maria
Gottwaldové	Gottwaldová	k1gFnSc2	Gottwaldová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
narození	narození	k1gNnSc1	narození
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
matrice	matrika	k1gFnSc6	matrika
pro	pro	k7c4	pro
Dědice	dědic	k1gMnPc4	dědic
a	a	k8xC	a
jako	jako	k9	jako
adresa	adresa	k1gFnSc1	adresa
narození	narození	k1gNnSc2	narození
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
Dědice	Dědice	k1gFnPc1	Dědice
102	[number]	k4	102
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgInS	vyučit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
truhlářem	truhlář	k1gMnSc7	truhlář
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
činnosti	činnost	k1gFnPc4	činnost
mezi	mezi	k7c7	mezi
sociálnědemokratickou	sociálnědemokratický	k2eAgFnSc7d1	sociálnědemokratická
mládeží	mládež	k1gFnSc7	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Martou	Marta	k1gFnSc7	Marta
Gottwaldovou	Gottwaldová	k1gFnSc7	Gottwaldová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
on	on	k3xPp3gMnSc1	on
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
chudé	chudý	k2eAgFnSc2d1	chudá
rodiny	rodina	k1gFnSc2	rodina
sedláka	sedlák	k1gMnSc2	sedlák
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
nemanželským	manželský	k2eNgNnSc7d1	nemanželské
dítětem	dítě	k1gNnSc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
při	při	k7c6	při
něm	on	k3xPp3gNnSc6	on
stála	stát	k5eAaImAgFnS	stát
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc4	jeho
věrnou	věrný	k2eAgFnSc4d1	věrná
společnicí	společnice	k1gFnSc7	společnice
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nevstoupila	vstoupit	k5eNaPmAgFnS	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
dceru	dcera	k1gFnSc4	dcera
Martu	Marta	k1gFnSc4	Marta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
byl	být	k5eAaImAgMnS	být
vojákem	voják	k1gMnSc7	voják
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bojoval	bojovat	k5eAaImAgMnS	bojovat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
stál	stát	k5eAaImAgInS	stát
i	i	k9	i
proti	proti	k7c3	proti
budoucímu	budoucí	k2eAgMnSc3d1	budoucí
generálu	generál	k1gMnSc3	generál
a	a	k8xC	a
prezidentu	prezident	k1gMnSc3	prezident
Ludvíku	Ludvík	k1gMnSc3	Ludvík
Svobodovi	Svoboda	k1gMnSc3	Svoboda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bojoval	bojovat	k5eAaImAgMnS	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
čs	čs	kA	čs
<g/>
.	.	kIx.	.
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vojenského	vojenský	k2eAgMnSc2d1	vojenský
historika	historik	k1gMnSc2	historik
Aleše	Aleš	k1gMnSc2	Aleš
Knížka	knížka	k1gFnSc1	knížka
to	ten	k3xDgNnSc1	ten
sice	sice	k8xC	sice
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jakl	Jakl	k1gMnSc1	Jakl
z	z	k7c2	z
Vojenského	vojenský	k2eAgInSc2d1	vojenský
historického	historický	k2eAgInSc2d1	historický
ústavu	ústav	k1gInSc2	ústav
označil	označit	k5eAaPmAgMnS	označit
Gottwaldovu	Gottwaldův	k2eAgFnSc4d1	Gottwaldova
účast	účast	k1gFnSc4	účast
u	u	k7c2	u
Zborova	Zborov	k1gInSc2	Zborov
za	za	k7c4	za
legendu	legenda	k1gFnSc4	legenda
-	-	kIx~	-
Gottwald	Gottwald	k1gMnSc1	Gottwald
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
bitvy	bitva	k1gFnSc2	bitva
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
Gottwald	Gottwald	k1gMnSc1	Gottwald
z	z	k7c2	z
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
dezertoval	dezertovat	k5eAaBmAgMnS	dezertovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
sloužil	sloužit	k5eAaImAgInS	sloužit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Rousínově	Rousínův	k2eAgFnSc6d1	Rousínova
jako	jako	k8xS	jako
stolařský	stolařský	k2eAgMnSc1d1	stolařský
dělník	dělník	k1gMnSc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roztržce	roztržka	k1gFnSc6	roztržka
ve	v	k7c6	v
Svazu	svaz	k1gInSc6	svaz
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
tělovýchovných	tělovýchovný	k2eAgFnPc2d1	Tělovýchovná
jednot	jednota	k1gFnPc2	jednota
(	(	kIx(	(
<g/>
SDTJ	SDTJ	kA	SDTJ
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
komunisticky	komunisticky	k6eAd1	komunisticky
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
část	část	k1gFnSc1	část
jednot	jednota	k1gFnPc2	jednota
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
novou	nový	k2eAgFnSc4d1	nová
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
Federaci	federace	k1gFnSc4	federace
dělnických	dělnický	k2eAgFnPc2d1	Dělnická
tělovýchovných	tělovýchovný	k2eAgFnPc2d1	Tělovýchovná
jednot	jednota	k1gFnPc2	jednota
(	(	kIx(	(
<g/>
FDTJ	FDTJ	kA	FDTJ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gottwald	Gottwald	k1gMnSc1	Gottwald
dokázal	dokázat	k5eAaPmAgMnS	dokázat
získat	získat	k5eAaPmF	získat
všechny	všechen	k3xTgFnPc4	všechen
jednoty	jednota	k1gFnPc4	jednota
svého	svůj	k3xOyFgInSc2	svůj
okresu	okres	k1gInSc2	okres
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
starostou	starosta	k1gMnSc7	starosta
20	[number]	k4	20
<g/>
.	.	kIx.	.
okresu	okres	k1gInSc2	okres
FDTJ	FDTJ	kA	FDTJ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1921	[number]	k4	1921
své	svůj	k3xOyFgFnSc2	svůj
jednoty	jednota	k1gFnSc2	jednota
přivedl	přivést	k5eAaPmAgInS	přivést
na	na	k7c4	na
I.	I.	kA	I.
dělnickou	dělnický	k2eAgFnSc4d1	Dělnická
spartakiádu	spartakiáda	k1gFnSc4	spartakiáda
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Maninách	manina	k1gFnPc6	manina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1921	[number]	k4	1921
z	z	k7c2	z
Rousínova	Rousínův	k2eAgMnSc2d1	Rousínův
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
komunistického	komunistický	k2eAgInSc2d1	komunistický
časopisu	časopis	k1gInSc2	časopis
Hlas	hlas	k1gInSc1	hlas
ľudu	ľudus	k1gInSc2	ľudus
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
začal	začít	k5eAaPmAgInS	začít
se	s	k7c7	s
zakládáním	zakládání	k1gNnSc7	zakládání
jednot	jednota	k1gFnPc2	jednota
na	na	k7c6	na
okrese	okres	k1gInSc6	okres
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
jednotě	jednota	k1gFnSc6	jednota
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
místostarostou	místostarosta	k1gMnSc7	místostarosta
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
jednatelem	jednatel	k1gMnSc7	jednatel
47	[number]	k4	47
<g/>
.	.	kIx.	.
okresu	okres	k1gInSc2	okres
FDTJ	FDTJ	kA	FDTJ
<g/>
,	,	kIx,	,
na	na	k7c6	na
listopadové	listopadový	k2eAgFnSc6d1	listopadová
konferenci	konference	k1gFnSc6	konference
slovenské	slovenský	k2eAgFnSc6d1	slovenská
FDTJ	FDTJ	kA	FDTJ
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jednatelem	jednatel	k1gMnSc7	jednatel
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
župy	župa	k1gFnSc2	župa
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Žilině	Žilina	k1gFnSc6	Žilina
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
redaktorem	redaktor	k1gMnSc7	redaktor
časopisu	časopis	k1gInSc2	časopis
Spartakus	Spartakus	k1gMnSc1	Spartakus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Vrútek	Vrútka	k1gFnPc2	Vrútka
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
redakce	redakce	k1gFnSc1	redakce
několika	několik	k4yIc2	několik
komunistických	komunistický	k2eAgInPc2d1	komunistický
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vrútkách	Vrútko	k1gNnPc6	Vrútko
sídlila	sídlit	k5eAaImAgFnS	sídlit
i	i	k9	i
župa	župa	k1gFnSc1	župa
FDTJ	FDTJ	kA	FDTJ
pro	pro	k7c4	pro
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
Podkarpatskou	podkarpatský	k2eAgFnSc4d1	Podkarpatská
Rus	Rus	k1gFnSc4	Rus
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
byl	být	k5eAaImAgMnS	být
jednatel	jednatel	k1gMnSc1	jednatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byly	být	k5eAaImAgFnP	být
redakce	redakce	k1gFnPc1	redakce
přestěhovány	přestěhovat	k5eAaPmNgFnP	přestěhovat
do	do	k7c2	do
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
tam	tam	k6eAd1	tam
i	i	k9	i
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
byl	být	k5eAaImAgMnS	být
funkcionářem	funkcionář	k1gMnSc7	funkcionář
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
redaktorem	redaktor	k1gMnSc7	redaktor
komunistického	komunistický	k2eAgInSc2d1	komunistický
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
sekretariátu	sekretariát	k1gInSc6	sekretariát
KSČ	KSČ	kA	KSČ
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zformovat	zformovat	k5eAaPmF	zformovat
promoskevskou	promoskevský	k2eAgFnSc4d1	promoskevská
opozici	opozice	k1gFnSc4	opozice
proti	proti	k7c3	proti
jejímu	její	k3xOp3gNnSc3	její
tehdejšímu	tehdejší	k2eAgNnSc3d1	tehdejší
vedení	vedení	k1gNnSc3	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
orgánu	orgán	k1gInSc2	orgán
Kominterny	Kominterna	k1gFnSc2	Kominterna
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc2	její
exekutivy	exekutiva	k1gFnSc2	exekutiva
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
její	její	k3xOp3gFnSc7	její
výkonné	výkonný	k2eAgFnPc4d1	výkonná
složky	složka	k1gFnPc4	složka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
V.	V.	kA	V.
sjezdu	sjezd	k1gInSc2	sjezd
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
Guttmannem	Guttmann	k1gMnSc7	Guttmann
<g/>
,	,	kIx,	,
Švermou	Šverma	k1gMnSc7	Šverma
<g/>
,	,	kIx,	,
Slánským	Slánský	k1gMnSc7	Slánský
<g/>
,	,	kIx,	,
Kopeckým	Kopecký	k1gMnSc7	Kopecký
a	a	k8xC	a
Reimanem	Reiman	k1gMnSc7	Reiman
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
karlínští	karlínský	k2eAgMnPc1d1	karlínský
kluci	kluk	k1gMnPc1	kluk
<g/>
)	)	kIx)	)
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
vedení	vedení	k1gNnSc2	vedení
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
KSČ	KSČ	kA	KSČ
řadu	řada	k1gFnSc4	řada
změn	změna	k1gFnPc2	změna
podle	podle	k7c2	podle
změn	změna	k1gFnPc2	změna
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
politiky	politika	k1gFnSc2	politika
lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
a	a	k8xC	a
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
určené	určený	k2eAgNnSc1d1	určené
na	na	k7c6	na
VII	VII	kA	VII
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc6	kongres
Kominterny	Kominterna	k1gFnSc2	Kominterna
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
a	a	k8xC	a
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
hlavním	hlavní	k2eAgMnPc3d1	hlavní
představitelům	představitel	k1gMnPc3	představitel
opozice	opozice	k1gFnSc2	opozice
proti	proti	k7c3	proti
přijetí	přijetí	k1gNnSc3	přijetí
Mnichovského	mnichovský	k2eAgInSc2d1	mnichovský
diktátu	diktát	k1gInSc2	diktát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zákazu	zákaz	k1gInSc6	zákaz
KSČ	KSČ	kA	KSČ
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
zastával	zastávat	k5eAaImAgMnS	zastávat
politiku	politika	k1gFnSc4	politika
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
sovětsko-německému	sovětskoěmecký	k2eAgInSc3d1	sovětsko-německý
paktu	pakt	k1gInSc3	pakt
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
bral	brát	k5eAaImAgMnS	brát
vytváření	vytváření	k1gNnSc4	vytváření
protihitlerovské	protihitlerovský	k2eAgFnSc2d1	protihitlerovská
koalice	koalice	k1gFnSc2	koalice
jako	jako	k8xS	jako
svoji	svůj	k3xOyFgFnSc4	svůj
velkou	velký	k2eAgFnSc4d1	velká
příležitost	příležitost	k1gFnSc4	příležitost
prosadit	prosadit	k5eAaPmF	prosadit
se	se	k3xPyFc4	se
a	a	k8xC	a
začínal	začínat	k5eAaImAgInS	začínat
promýšlet	promýšlet	k5eAaImF	promýšlet
pozdější	pozdní	k2eAgNnSc4d2	pozdější
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
s	s	k7c7	s
představitelem	představitel	k1gMnSc7	představitel
londýnské	londýnský	k2eAgFnSc2d1	londýnská
emigrace	emigrace	k1gFnSc2	emigrace
prezidentem	prezident	k1gMnSc7	prezident
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
na	na	k7c6	na
sjednocení	sjednocení	k1gNnSc6	sjednocení
domácího	domácí	k2eAgInSc2d1	domácí
a	a	k8xC	a
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
podařilo	podařit	k5eAaPmAgNnS	podařit
zajistit	zajistit	k5eAaPmF	zajistit
komunistům	komunista	k1gMnPc3	komunista
zásadní	zásadní	k2eAgInSc4d1	zásadní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
poválečné	poválečný	k2eAgNnSc4d1	poválečné
uspořádání	uspořádání	k1gNnSc4	uspořádání
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Třetí	třetí	k4xOgFnSc1	třetí
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Únor	únor	k1gInSc1	únor
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
jako	jako	k8xS	jako
místopředseda	místopředseda	k1gMnSc1	místopředseda
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
zároveň	zároveň	k6eAd1	zároveň
předsedou	předseda	k1gMnSc7	předseda
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
KSČ	KSČ	kA	KSČ
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
poválečných	poválečný	k2eAgFnPc6d1	poválečná
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
sestavením	sestavení	k1gNnSc7	sestavení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
komunisti	komunisti	k?	komunisti
jsou	být	k5eAaImIp3nP	být
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yInSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nastala	nastat	k5eAaPmAgFnS	nastat
vládní	vládní	k2eAgFnSc1d1	vládní
krize	krize	k1gFnSc1	krize
kolem	kolem	k7c2	kolem
usnesení	usnesení	k1gNnSc2	usnesení
o	o	k7c6	o
Bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sérii	série	k1gFnSc3	série
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
převzetí	převzetí	k1gNnSc3	převzetí
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
byly	být	k5eAaImAgFnP	být
završeny	završen	k2eAgInPc4d1	završen
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prezident	prezident	k1gMnSc1	prezident
přijal	přijmout	k5eAaPmAgMnS	přijmout
demisi	demise	k1gFnSc4	demise
nekomunistických	komunistický	k2eNgMnPc2d1	nekomunistický
ministrů	ministr	k1gMnPc2	ministr
a	a	k8xC	a
doplnil	doplnit	k5eAaPmAgMnS	doplnit
vládu	vláda	k1gFnSc4	vláda
kandidáty	kandidát	k1gMnPc7	kandidát
navrženými	navržený	k2eAgMnPc7d1	navržený
komunisty	komunista	k1gMnPc7	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
nestranického	stranický	k2eNgMnSc2d1	nestranický
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Jana	Jan	k1gMnSc2	Jan
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
nová	nový	k2eAgFnSc1d1	nová
komunistická	komunistický	k2eAgFnSc1d1	komunistická
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přijata	přijmout	k5eAaPmNgFnS	přijmout
nová	nový	k2eAgFnSc1d1	nová
komunistická	komunistický	k2eAgFnSc1d1	komunistická
ústava	ústava	k1gFnSc1	ústava
vyhlašující	vyhlašující	k2eAgFnSc1d1	vyhlašující
lidově	lidově	k6eAd1	lidově
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prezident	prezident	k1gMnSc1	prezident
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
volby	volba	k1gFnPc1	volba
s	s	k7c7	s
vynucenou	vynucený	k2eAgFnSc7d1	vynucená
účastí	účast	k1gFnSc7	účast
a	a	k8xC	a
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
volit	volit	k5eAaImF	volit
jen	jen	k9	jen
z	z	k7c2	z
jednotného	jednotný	k2eAgInSc2d1	jednotný
seznamu	seznam	k1gInSc2	seznam
kandidátů	kandidát	k1gMnPc2	kandidát
komunisty	komunista	k1gMnSc2	komunista
ovládané	ovládaný	k2eAgFnSc2d1	ovládaná
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
prezidenta	prezident	k1gMnSc2	prezident
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
Ústavu	ústava	k1gFnSc4	ústava
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
československého	československý	k2eAgMnSc4d1	československý
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
pak	pak	k6eAd1	pak
začali	začít	k5eAaPmAgMnP	začít
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
vládnout	vládnout	k5eAaImF	vládnout
sovětští	sovětský	k2eAgMnPc1d1	sovětský
poradci	poradce	k1gMnPc1	poradce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
byla	být	k5eAaImAgFnS	být
prosazena	prosazen	k2eAgFnSc1d1	prosazena
koncepce	koncepce	k1gFnSc1	koncepce
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
násilná	násilný	k2eAgFnSc1d1	násilná
kolektivizace	kolektivizace	k1gFnSc1	kolektivizace
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
monopol	monopol	k1gInSc1	monopol
komunistické	komunistický	k2eAgFnSc2d1	komunistická
moci	moc	k1gFnSc2	moc
na	na	k7c4	na
školství	školství	k1gNnSc4	školství
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
zájem	zájem	k1gInSc1	zájem
poradců	poradce	k1gMnPc2	poradce
byl	být	k5eAaImAgInS	být
soustředěn	soustředěn	k2eAgInSc1d1	soustředěn
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
prokuraturu	prokuratura	k1gFnSc4	prokuratura
a	a	k8xC	a
soudy	soud	k1gInPc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
k	k	k7c3	k
mnoha	mnoho	k4c2	mnoho
politickým	politický	k2eAgInPc3d1	politický
vykonstruovaným	vykonstruovaný	k2eAgInPc3d1	vykonstruovaný
procesům	proces	k1gInPc3	proces
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
rozpoután	rozpoután	k2eAgInSc4d1	rozpoután
politický	politický	k2eAgInSc4d1	politický
teror	teror	k1gInSc4	teror
<g/>
:	:	kIx,	:
bylo	být	k5eAaImAgNnS	být
vyneseno	vynést	k5eAaPmNgNnS	vynést
přes	přes	k7c4	přes
230	[number]	k4	230
rozsudků	rozsudek	k1gInPc2	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInSc4	tisíc
občanů	občan	k1gMnPc2	občan
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
k	k	k7c3	k
doživotí	doživotí	k1gNnSc3	doživotí
či	či	k8xC	či
mnohaletým	mnohaletý	k2eAgNnSc7d1	mnohaleté
vězením	vězení	k1gNnSc7	vězení
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kárnými	kárný	k2eAgFnPc7d1	kárná
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
jednotkami	jednotka	k1gFnPc7	jednotka
PTP	PTP	kA	PTP
(	(	kIx(	(
<g/>
Pomocné	pomocný	k2eAgInPc1d1	pomocný
technické	technický	k2eAgInPc1d1	technický
prapory	prapor	k1gInPc1	prapor
<g/>
)	)	kIx)	)
prošly	projít	k5eAaPmAgFnP	projít
desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
"	"	kIx"	"
<g/>
protistátních	protistátní	k2eAgInPc2d1	protistátní
živlů	živel	k1gInPc2	živel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
šibenici	šibenice	k1gFnSc4	šibenice
i	i	k8xC	i
jedenáct	jedenáct	k4xCc1	jedenáct
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
předních	přední	k2eAgMnPc2d1	přední
komunistických	komunistický	k2eAgMnPc2d1	komunistický
funkcionářů	funkcionář	k1gMnPc2	funkcionář
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Slánským	Slánský	k1gMnSc7	Slánský
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
se	se	k3xPyFc4	se
první	první	k4xOgMnSc1	první
dělnický	dělnický	k2eAgMnSc1d1	dělnický
prezident	prezident	k1gMnSc1	prezident
vrátil	vrátit	k5eAaPmAgInS	vrátit
leteckým	letecký	k2eAgInSc7d1	letecký
speciálem	speciál	k1gInSc7	speciál
ze	z	k7c2	z
Stalinova	Stalinův	k2eAgInSc2d1	Stalinův
pohřbu	pohřeb	k1gInSc2	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
přistání	přistání	k1gNnSc4	přistání
si	se	k3xPyFc3	se
postěžoval	postěžovat	k5eAaPmAgInS	postěžovat
předsedovi	předseda	k1gMnSc3	předseda
vlády	vláda	k1gFnSc2	vláda
Antonínu	Antonín	k1gMnSc3	Antonín
Zápotockému	Zápotocký	k1gMnSc3	Zápotocký
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gInSc3	on
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Myslel	myslet	k5eAaImAgInS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
chřipku	chřipka	k1gFnSc4	chřipka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečnost	skutečnost	k1gFnSc1	skutečnost
byla	být	k5eAaImAgFnS	být
vážnější	vážní	k2eAgFnSc1d2	vážnější
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
přísně	přísně	k6eAd1	přísně
tajeno	tajit	k5eAaImNgNnS	tajit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
a	a	k8xC	a
trpěl	trpět	k5eAaImAgMnS	trpět
syfilidou	syfilida	k1gFnSc7	syfilida
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
problémy	problém	k1gInPc1	problém
byly	být	k5eAaImAgInP	být
zřejmě	zřejmě	k6eAd1	zřejmě
příčinou	příčina	k1gFnSc7	příčina
aneurysmatu	aneurysma	k1gNnSc2	aneurysma
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
výdutě	výduť	k1gFnPc1	výduť
srdeční	srdeční	k2eAgFnSc2d1	srdeční
aorty	aorta	k1gFnSc2	aorta
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInPc4	jejíž
následky	následek	k1gInPc4	následek
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
mauzoleu	mauzoleum	k1gNnSc6	mauzoleum
původně	původně	k6eAd1	původně
předpokládaném	předpokládaný	k2eAgNnSc6d1	předpokládané
pro	pro	k7c4	pro
prezidenta	prezident	k1gMnSc4	prezident
Tomáše	Tomáš	k1gMnSc4	Tomáš
Garrigua	Garriguus	k1gMnSc4	Garriguus
Masaryka	Masaryk	k1gMnSc4	Masaryk
v	v	k7c6	v
uzpůsobeném	uzpůsobený	k2eAgInSc6d1	uzpůsobený
Národním	národní	k2eAgInSc6d1	národní
památníku	památník	k1gInSc6	památník
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
vrchu	vrch	k1gInSc6	vrch
Vítkově	Vítkův	k2eAgInSc6d1	Vítkův
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953	[number]	k4	1953
až	až	k9	až
1962	[number]	k4	1962
vystavováno	vystavovat	k5eAaImNgNnS	vystavovat
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Leninovo	Leninův	k2eAgNnSc1d1	Leninovo
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
široce	široko	k6eAd1	široko
rozšířenému	rozšířený	k2eAgNnSc3d1	rozšířené
mýtu	mýto	k1gNnSc3	mýto
o	o	k7c6	o
špatně	špatně	k6eAd1	špatně
provedené	provedený	k2eAgFnSc6d1	provedená
balzamaci	balzamace	k1gFnSc6	balzamace
<g/>
,	,	kIx,	,
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
odstranění	odstranění	k1gNnSc2	odstranění
z	z	k7c2	z
památníku	památník	k1gInSc2	památník
snad	snad	k9	snad
v	v	k7c6	v
perfektním	perfektní	k2eAgInSc6d1	perfektní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
odtajněné	odtajněný	k2eAgFnSc2d1	odtajněná
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
doplňující	doplňující	k2eAgFnSc6d1	doplňující
balzamaci	balzamace	k1gFnSc6	balzamace
provedené	provedený	k2eAgFnSc6d1	provedená
sovětskými	sovětský	k2eAgInPc7d1	sovětský
odborníky	odborník	k1gMnPc7	odborník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zpopelnění	zpopelnění	k1gNnSc3	zpopelnění
těla	tělo	k1gNnSc2	tělo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
došlo	dojít	k5eAaPmAgNnS	dojít
čistě	čistě	k6eAd1	čistě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
politického	politický	k2eAgNnSc2d1	politické
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
souvisejícího	související	k2eAgNnSc2d1	související
s	s	k7c7	s
nákladností	nákladnost	k1gFnSc7	nákladnost
provozu	provoz	k1gInSc2	provoz
mauzolea	mauzoleum	k1gNnSc2	mauzoleum
a	a	k8xC	a
kritikou	kritika	k1gFnSc7	kritika
kultu	kult	k1gInSc2	kult
osobnosti	osobnost	k1gFnSc2	osobnost
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
zvěsti	zvěst	k1gFnPc4	zvěst
o	o	k7c6	o
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
však	však	k9	však
hodily	hodit	k5eAaPmAgFnP	hodit
jako	jako	k9	jako
zdůvodnění	zdůvodnění	k1gNnSc4	zdůvodnění
před	před	k7c7	před
stalinisticky	stalinisticky	k6eAd1	stalinisticky
smýšlející	smýšlející	k2eAgFnSc7d1	smýšlející
frakcí	frakce	k1gFnSc7	frakce
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
(	(	kIx(	(
<g/>
urna	urna	k1gFnSc1	urna
<g/>
)	)	kIx)	)
uloženy	uložen	k2eAgFnPc4d1	uložena
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
posmrtně	posmrtně	k6eAd1	posmrtně
zbaven	zbaven	k2eAgMnSc1d1	zbaven
čestného	čestný	k2eAgMnSc2d1	čestný
občanství	občanství	k1gNnSc3	občanství
města	město	k1gNnSc2	město
Vyškova	Vyškov	k1gInSc2	Vyškov
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odhlasovalo	odhlasovat	k5eAaPmAgNnS	odhlasovat
27	[number]	k4	27
<g/>
členné	členný	k2eAgNnSc1d1	členné
městské	městský	k2eAgNnSc1d1	Městské
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c4	o
odebrání	odebrání	k1gNnSc4	odebrání
čestného	čestný	k2eAgNnSc2d1	čestné
občanství	občanství	k1gNnSc2	občanství
město	město	k1gNnSc1	město
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
.	.	kIx.	.
</s>
<s>
Klement	Klement	k1gMnSc1	Klement
Gottwald	Gottwald	k1gMnSc1	Gottwald
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
možná	možná	k9	možná
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Hošticích	Hošticí	k2eAgFnPc6d1	Hošticí
-	-	kIx~	-
Herolticích	Heroltice	k1gFnPc6	Heroltice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rodný	rodný	k2eAgInSc1d1	rodný
domek	domek	k1gInSc1	domek
v	v	k7c6	v
Dědicích	Dědice	k1gFnPc6	Dědice
u	u	k7c2	u
Vyškova	Vyškov	k1gInSc2	Vyškov
byla	být	k5eAaImAgFnS	být
naaranžovaná	naaranžovaný	k2eAgFnSc1d1	naaranžovaná
komunistická	komunistický	k2eAgFnSc1d1	komunistická
smyšlenka	smyšlenka	k1gFnSc1	smyšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
několik	několik	k4yIc1	několik
jednotek	jednotka	k1gFnPc2	jednotka
československých	československý	k2eAgFnPc2d1	Československá
interbrigád	interbrigáda	k1gFnPc2	interbrigáda
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
četa	četa	k1gFnSc1	četa
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
skutečně	skutečně	k6eAd1	skutečně
československá	československý	k2eAgFnSc1d1	Československá
jednotka	jednotka	k1gFnSc1	jednotka
v	v	k7c6	v
interbrigádách	interbrigáda	k1gFnPc6	interbrigáda
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
banka	banka	k1gFnSc1	banka
československá	československý	k2eAgFnSc1d1	Československá
vydala	vydat	k5eAaPmAgFnS	vydat
dnem	dnem	k7c2	dnem
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
bankovky	bankovka	k1gFnPc4	bankovka
po	po	k7c4	po
100	[number]	k4	100
Kčs	Kčs	kA	Kčs
vzoru	vzor	k1gInSc2	vzor
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
líci	líce	k1gNnSc6	líce
provedeném	provedený	k2eAgNnSc6d1	provedené
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
zelené	zelený	k2eAgFnSc2d1	zelená
barvy	barva	k1gFnSc2	barva
byla	být	k5eAaImAgFnS	být
podobizna	podobizna	k1gFnSc1	podobizna
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Občany	občan	k1gMnPc4	občan
byla	být	k5eAaImAgFnS	být
odmítnuta	odmítnut	k2eAgFnSc1d1	odmítnuta
a	a	k8xC	a
zavdala	zavdat	k5eAaPmAgFnS	zavdat
příčinu	příčina	k1gFnSc4	příčina
k	k	k7c3	k
listopadovým	listopadový	k2eAgNnPc3d1	listopadové
heslům	heslo	k1gNnPc3	heslo
<g/>
:	:	kIx,	:
Masaryka	Masaryk	k1gMnSc4	Masaryk
na	na	k7c4	na
stovku	stovka	k1gFnSc4	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Nechceme	chtít	k5eNaImIp1nP	chtít
stovku	stovka	k1gFnSc4	stovka
s	s	k7c7	s
vrahem	vrah	k1gMnSc7	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
novou	nový	k2eAgFnSc4d1	nová
stovku	stovka	k1gFnSc4	stovka
<g/>
,	,	kIx,	,
sundejte	sundat	k5eAaPmRp2nP	sundat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vraha	vrah	k1gMnSc4	vrah
apod.	apod.	kA	apod.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	jeho	k3xOp3gFnPc1	jeho
sochy	socha	k1gFnPc1	socha
byly	být	k5eAaImAgFnP	být
rozmístěné	rozmístěný	k2eAgInPc1d1	rozmístěný
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
1989	[number]	k4	1989
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
terčem	terč	k1gInSc7	terč
nenásilných	násilný	k2eNgInPc2d1	nenásilný
negativních	negativní	k2eAgInPc2d1	negativní
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Ruce	ruka	k1gFnPc1	ruka
soch	socha	k1gFnPc2	socha
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
potřeny	potřít	k5eAaPmNgFnP	potřít
červenou	červený	k2eAgFnSc7d1	červená
barvou	barva	k1gFnSc7	barva
na	na	k7c4	na
připomínku	připomínka	k1gFnSc4	připomínka
krve	krev	k1gFnSc2	krev
nevinných	vinný	k2eNgFnPc2d1	nevinná
obětí	oběť	k1gFnPc2	oběť
z	z	k7c2	z
procesů	proces	k1gInPc2	proces
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
měla	mít	k5eAaImAgFnS	mít
socha	socha	k1gFnSc1	socha
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
velký	velký	k2eAgInSc4d1	velký
batoh	batoh	k1gInSc4	batoh
a	a	k8xC	a
na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
přibyl	přibýt	k5eAaPmAgInS	přibýt
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
máš	mít	k5eAaImIp2nS	mít
už	už	k6eAd1	už
sbaleno	sbalen	k2eAgNnSc1d1	sbaleno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gottwaldovy	Gottwaldův	k2eAgFnPc1d1	Gottwaldova
sochy	socha	k1gFnPc1	socha
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
KSČM	KSČM	kA	KSČM
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
odkazu	odkaz	k1gInSc3	odkaz
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
a	a	k8xC	a
při	při	k7c6	při
shromáždění	shromáždění	k1gNnSc6	shromáždění
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
vedoucí	vedoucí	k1gMnSc1	vedoucí
představitelé	představitel	k1gMnPc1	představitel
kladou	klást	k5eAaImIp3nP	klást
kytice	kytice	k1gFnPc4	kytice
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
hrob	hrob	k1gInSc4	hrob
<g/>
.	.	kIx.	.
</s>
