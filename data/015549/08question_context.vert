<s>
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Rheinbund	Rheinbund	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgInSc4d1
název	název	k1gInSc4
však	však	k9
byl	být	k5eAaImAgMnS
Konfederované	konfederovaný	k2eAgInPc4d1
státy	stát	k1gInPc4
rýnské	rýnský	k2eAgNnSc1d1
z	z	k7c2
doslovné	doslovný	k2eAgFnSc2d1
francouzštiny	francouzština	k1gFnSc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
États	États	k1gInSc1
confédérés	confédérés	k1gInSc1
du	du	k?
Rhin	Rhin	k1gInSc1
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
také	také	k9
francouzsky	francouzsky	k6eAd1
Confédération	Confédération	k1gInSc1
du	du	k?
Rhin	Rhin	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
bývalé	bývalý	k2eAgFnSc2d1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
po	po	k7c6
844	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
roce	rok	k1gInSc6
1806	#num#	k4
<g/>
.	.	kIx.
</s>