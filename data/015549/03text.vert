<s>
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
Konfederované	konfederovaný	k2eAgInPc1d1
státy	stát	k1gInPc1
rýnskéRheinbund	rýnskéRheinbunda	k1gFnPc2
(	(	kIx(
<g/>
de	de	k?
<g/>
)	)	kIx)
<g/>
États	États	k1gInSc1
confédérés	confédérés	k1gInSc1
du	du	k?
Rhin	Rhin	k1gInSc1
(	(	kIx(
<g/>
fr	fr	k0
<g/>
)	)	kIx)
</s>
<s>
←	←	k?
←	←	k?
</s>
<s>
1806	#num#	k4
<g/>
–	–	k?
<g/>
1813	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1812	#num#	k4
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1
</s>
<s>
rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
325	#num#	k4
752	#num#	k4
km²	km²	k?
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
14	#num#	k4
608	#num#	k4
877	#num#	k4
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
němčina	němčina	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
římskokatolické	římskokatolický	k2eAgNnSc1d1
<g/>
,	,	kIx,
protestantské	protestantský	k2eAgNnSc1d1
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
konfederace	konfederace	k1gFnSc1
klientských	klientský	k2eAgInPc2d1
států	stát	k1gInPc2
Francie	Francie	k1gFnSc2
</s>
<s>
mateřskázemě	mateřskázemě	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
</s>
<s>
Německé	německý	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Německý	německý	k2eAgInSc1d1
spolek	spolek	k1gInSc1
</s>
<s>
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Rheinbund	Rheinbund	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgInSc4d1
název	název	k1gInSc4
však	však	k9
byl	být	k5eAaImAgMnS
Konfederované	konfederovaný	k2eAgInPc4d1
státy	stát	k1gInPc4
rýnské	rýnský	k2eAgNnSc1d1
z	z	k7c2
doslovné	doslovný	k2eAgFnSc2d1
francouzštiny	francouzština	k1gFnSc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
États	États	k1gInSc1
confédérés	confédérés	k1gInSc1
du	du	k?
Rhin	Rhin	k1gInSc1
<g/>
,	,	kIx,
běžně	běžně	k6eAd1
také	také	k9
francouzsky	francouzsky	k6eAd1
Confédération	Confédération	k1gInSc1
du	du	k?
Rhin	Rhin	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
bývalé	bývalý	k2eAgFnSc2d1
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
po	po	k7c6
844	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
roce	rok	k1gInSc6
1806	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozpad	rozpad	k1gInSc1
říše	říš	k1gFnSc2
a	a	k8xC
vytvoření	vytvoření	k1gNnSc2
Rýnského	rýnský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
bylo	být	k5eAaImAgNnS
zaviněno	zaviněn	k2eAgNnSc1d1
porážkou	porážka	k1gFnSc7
říšských	říšský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
Napoleonem	napoleon	k1gInSc7
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
1805	#num#	k4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
nazývanou	nazývaný	k2eAgFnSc4d1
také	také	k6eAd1
bitvou	bitva	k1gFnSc7
tří	tři	k4xCgInPc2
císařů	císař	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
bitvě	bitva	k1gFnSc6
byl	být	k5eAaImAgInS
uzavřen	uzavřít	k5eAaPmNgInS
bratislavský	bratislavský	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
právě	právě	k9
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
zřekl	zřeknout	k5eAaPmAgMnS
titulu	titul	k1gInSc3
římského	římský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
a	a	k8xC
ponechal	ponechat	k5eAaPmAgInS
si	se	k3xPyFc3
pouze	pouze	k6eAd1
titul	titul	k1gInSc1
rakouského	rakouský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
(	(	kIx(
<g/>
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tedy	tedy	k8xC
Františkem	František	k1gMnSc7
I.	I.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
1806	#num#	k4
po	po	k7c6
francouzském	francouzský	k2eAgNnSc6d1
pokoření	pokoření	k1gNnSc6
Pruska	Prusko	k1gNnSc2
přistoupily	přistoupit	k5eAaPmAgInP
do	do	k7c2
Rýnského	rýnský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
i	i	k8xC
státy	stát	k1gInPc1
středního	střední	k2eAgNnSc2d1
a	a	k8xC
severního	severní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1808	#num#	k4
přistoupilo	přistoupit	k5eAaPmAgNnS
k	k	k7c3
Rýnskému	rýnské	k1gNnSc3
spolku	spolek	k1gInSc2
dalších	další	k2eAgInPc2d1
dvacet	dvacet	k4xCc4
států	stát	k1gInPc2
a	a	k8xC
státečků	státeček	k1gInPc2
a	a	k8xC
dosáhl	dosáhnout	k5eAaPmAgMnS
svého	svůj	k3xOyFgInSc2
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pouze	pouze	k6eAd1
státy	stát	k1gInPc1
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Prusko	Prusko	k1gNnSc1
<g/>
,	,	kIx,
knížectví	knížectví	k1gNnSc1
Holštýnsko	Holštýnsko	k1gNnSc1
náležející	náležející	k2eAgNnSc1d1
k	k	k7c3
Dánsku	Dánsko	k1gNnSc3
a	a	k8xC
švédské	švédský	k2eAgNnSc1d1
Pomořansko	Pomořansko	k1gNnSc1
zůstaly	zůstat	k5eAaPmAgFnP
mimo	mimo	k7c4
spolek	spolek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knížectví	knížectví	k1gNnSc1
Erfurt	Erfurta	k1gFnPc2
bylo	být	k5eAaImAgNnS
anektováno	anektovat	k5eAaBmNgNnS
přímo	přímo	k6eAd1
Francií	Francie	k1gFnSc7
a	a	k8xC
tvořilo	tvořit	k5eAaImAgNnS
tak	tak	k6eAd1
francouzskou	francouzský	k2eAgFnSc4d1
enklávu	enkláva	k1gFnSc4
uvnitř	uvnitř	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
byla	být	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
severozápadního	severozápadní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
s	s	k7c7
územím	území	k1gNnSc7
mezi	mezi	k7c7
řekami	řeka	k1gFnPc7
Emže	Emže	k1gFnSc1
<g/>
,	,	kIx,
Vezera	Vezera	k1gFnSc1
a	a	k8xC
Labe	Labe	k1gNnPc4
přičleněna	přičleněn	k2eAgNnPc4d1
k	k	k7c3
Francii	Francie	k1gFnSc3
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
utužení	utužení	k1gNnSc3
kontinentální	kontinentální	k2eAgFnSc2d1
blokády	blokáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
rýnskému	rýnské	k1gNnSc3
spolku	spolek	k1gInSc2
náleželo	náležet	k5eAaImAgNnS
skrz	skrz	k7c4
personální	personální	k2eAgFnSc4d1
unii	unie	k1gFnSc4
se	s	k7c7
Saskem	Sasko	k1gNnSc7
také	také	k6eAd1
Varšavské	varšavský	k2eAgNnSc4d1
velkoknížectví	velkoknížectví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
zahrnoval	zahrnovat	k5eAaImAgInS
spolek	spolek	k1gInSc1
325	#num#	k4
752	#num#	k4
čtverečních	čtvereční	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
s	s	k7c7
14	#num#	k4
608	#num#	k4
877	#num#	k4
obyvateli	obyvatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
zanikl	zaniknout	k5eAaPmAgInS
po	po	k7c6
porážce	porážka	k1gFnSc6
francouzských	francouzský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1813	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
většina	většina	k1gFnSc1
jeho	jeho	k3xOp3gInPc2
členů	člen	k1gInPc2
pod	pod	k7c7
tlakem	tlak	k1gInSc7
spojeneckého	spojenecký	k2eAgInSc2d1
postupu	postup	k1gInSc2
na	na	k7c4
západ	západ	k1gInSc4
přidala	přidat	k5eAaPmAgFnS
na	na	k7c4
stranu	strana	k1gFnSc4
spojenců	spojenec	k1gMnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
byla	být	k5eAaImAgFnS
vojensky	vojensky	k6eAd1
okupována	okupovat	k5eAaBmNgFnS
spojeneckými	spojenecký	k2eAgInPc7d1
vojsky	vojsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1815	#num#	k4
po	po	k7c6
vídeňském	vídeňský	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
nahrazen	nahradit	k5eAaPmNgInS
Německým	německý	k2eAgInSc7d1
spolkem	spolek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Mapa	mapa	k1gFnSc1
rýnského	rýnský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
se	se	k3xPyFc4
státy	stát	k1gInPc1
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
ho	on	k3xPp3gMnSc4
tvořily	tvořit	k5eAaImAgInP
</s>
<s>
4	#num#	k4
království	království	k1gNnSc1
</s>
<s>
5	#num#	k4
velkovévodství	velkovévodství	k1gNnSc1
(	(	kIx(
<g/>
velkoknížectví	velkoknížectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
13	#num#	k4
vévodství	vévodství	k1gNnSc1
</s>
<s>
17	#num#	k4
knížectví	knížectví	k1gNnSc1
</s>
<s>
Hansovní	hansovní	k2eAgNnPc1d1
města	město	k1gNnPc1
Hamburg	Hamburg	k1gInSc1
<g/>
,	,	kIx,
Lübeck	Lübeck	k1gInSc1
a	a	k8xC
Brémy	Brémy	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Království	království	k1gNnSc1
</s>
<s>
Bavorské	bavorský	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Württemberské	Württemberský	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Saské	saský	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
11	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Vestfálské	vestfálský	k2eAgNnSc1d1
království	království	k1gNnSc1
–	–	k?
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
15	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1807	#num#	k4
<g/>
,	,	kIx,
vytvořeno	vytvořit	k5eAaPmNgNnS
Napoleonem	napoleon	k1gInSc7
</s>
<s>
Velkovévodství	velkovévodství	k1gNnSc1
</s>
<s>
Bádenské	bádenský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Velkovévodství	velkovévodství	k1gNnSc1
Berg	Berg	k1gMnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Velkovévodství	velkovévodství	k1gNnSc4
Hesensko-Darmstadtské	Hesensko-Darmstadtský	k2eAgFnSc2d1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Velkovévodství	velkovévodství	k1gNnSc4
würzburské	würzburský	k2eAgFnSc2d1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Velkovévodství	velkovévodství	k1gNnSc1
frankfurtské	frankfurtský	k2eAgNnSc1d1
–	–	k?
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1810	#num#	k4
</s>
<s>
Vévodství	vévodství	k1gNnSc1
</s>
<s>
Nasavské	Nasavský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Nasavsko-Usingen	Nasavsko-Usingen	k1gInSc1
–	–	k?
spojeno	spojen	k2eAgNnSc1d1
30	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Nasavsko-Weilburg	Nasavsko-Weilburg	k1gMnSc1
–	–	k?
spojeno	spojen	k2eAgNnSc1d1
30	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Meklenbursko-Střelické	Meklenbursko-Střelický	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Meklenbursko-Zvěřínské	Meklenbursko-Zvěřínský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Oldenburské	oldenburský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
–	–	k?
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
14	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1808	#num#	k4
<g/>
,	,	kIx,
anektováno	anektovat	k5eAaBmNgNnS
Francií	Francie	k1gFnSc7
13	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1810	#num#	k4
</s>
<s>
Knížectví	knížectví	k1gNnSc1
</s>
<s>
Řezenské	řezenský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
knížectví	knížectví	k1gNnSc1
Leyen	Leyna	k1gFnPc2
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
Lichtenštejnské	lichtenštejnský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
knížectví	knížectví	k1gNnSc1
Lippe	Lipp	k1gInSc5
–	–	k?
spoluzakladatel	spoluzakladatel	k1gMnSc1
<g/>
,	,	kIx,
připojilo	připojit	k5eAaPmAgNnS
se	s	k7c7
12	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1806	#num#	k4
</s>
<s>
knížectví	knížectví	k1gNnSc1
Reuss	Reuss	k1gInSc1
–	–	k?
připojila	připojit	k5eAaPmAgFnS
se	s	k7c7
11	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1807	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rýnský	rýnský	k2eAgInSc4d1
spolek	spolek	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
Rýnského	rýnský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
Království	království	k1gNnSc1
</s>
<s>
Bavorské	bavorský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
**	**	k?
<g/>
,	,	kIx,
Württemberské	Württemberský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
**	**	k?
<g/>
,	,	kIx,
Saské	saský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
**	**	k?
<g/>
,	,	kIx,
Vestfálské	vestfálský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
*	*	kIx~
Rýnský	rýnský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
roku	rok	k1gInSc2
1808	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkovévodství	velkovévodství	k1gNnSc1
</s>
<s>
Bádenské	bádenský	k2eAgInPc4d1
<g/>
**	**	k?
<g/>
,	,	kIx,
velkovévodství	velkovévodství	k1gNnSc1
Berg	Berga	k1gFnPc2
<g/>
*	*	kIx~
<g/>
,	,	kIx,
Hesensko-Darmstadtské	Hesensko-Darmstadtský	k2eAgFnPc1d1
<g/>
**	**	k?
<g/>
,	,	kIx,
Würzburské	Würzburský	k2eAgFnPc1d1
<g/>
*	*	kIx~
<g/>
,	,	kIx,
Frankfurtské	frankfurtský	k2eAgFnSc2d1
<g/>
*	*	kIx~
<g/>
1	#num#	k4
Vévodství	vévodství	k1gNnPc2
</s>
<s>
Anhaltsko	Anhaltsko	k1gNnSc1
(	(	kIx(
<g/>
Bernburg	Bernburg	k1gInSc1
<g/>
,	,	kIx,
Dessau	Dessaa	k1gFnSc4
<g/>
,	,	kIx,
Köthen	Köthen	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lauenbursko	Lauenbursko	k1gNnSc1
<g/>
,	,	kIx,
Meklenbursko	Meklenbursko	k1gNnSc1
(	(	kIx(
<g/>
Střelicko	Střelicko	k1gNnSc1
<g/>
,	,	kIx,
Zvěřínsko	Zvěřínsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nasavské	Nasavský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
(	(	kIx(
<g/>
Nasavsko-Usigen	Nasavsko-Usigen	k1gInSc1
<g/>
,	,	kIx,
Nasavsko-Weilburg	Nasavsko-Weilburg	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Oldenburské	oldenburský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
<g/>
,	,	kIx,
Sasko	Sasko	k1gNnSc1
(	(	kIx(
<g/>
Gothajsko-Altenbursko	Gothajsko-Altenbursko	k1gNnSc1
<g/>
,	,	kIx,
Kobursko-Saalfeldsko	Kobursko-Saalfeldsko	k1gNnSc1
<g/>
,	,	kIx,
Meiningensko	Meiningensko	k1gNnSc1
<g/>
,	,	kIx,
Eisenašsko	Eisenašsko	k1gNnSc1
<g/>
,	,	kIx,
Výmarsko	Výmarsko	k1gNnSc1
,	,	kIx,
Výmarsko-eisenašsko	Výmarsko-eisenašsko	k1gNnSc1
<g/>
)	)	kIx)
Knížectví	knížectví	k1gNnSc1
</s>
<s>
Aschaffenburské	Aschaffenburský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
<g/>
*	*	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Řezenské	řezenský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
<g/>
*	*	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Hohenzollernsko	Hohenzollernsko	k1gNnSc1
(	(	kIx(
<g/>
Hechingen	Hechingen	k1gInSc1
<g/>
,	,	kIx,
Sigmaringen	Sigmaringen	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Isenburg-Birstein	Isenburg-Birstein	k1gInSc1
<g/>
,	,	kIx,
Leyen	Leyen	k1gInSc1
<g/>
*	*	kIx~
<g/>
,	,	kIx,
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
<g/>
,	,	kIx,
Lippe	Lipp	k1gMnSc5
<g/>
,	,	kIx,
Reuss	Reuss	k1gInSc1
(	(	kIx(
<g/>
Ebersdorf	Ebersdorf	k1gMnSc1
<g/>
,	,	kIx,
Greiz	Greiz	k1gMnSc1
<g/>
,	,	kIx,
Lobenstein	Lobenstein	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Schleiz	Schleiz	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Salm	Salm	k1gInSc1
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Schaumburg-Lippe	Schaumburg-Lipp	k1gMnSc5
<g/>
,	,	kIx,
Schwarzbursko	Schwarzburska	k1gMnSc5
(	(	kIx(
<g/>
Rudolstadt	Rudolstadt	k1gInSc1
<g/>
,	,	kIx,
Sondershausen	Sondershausen	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Waldeck	Waldeck	k1gInSc1
1	#num#	k4
od	od	k7c2
roku	rok	k1gInSc2
1810	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1810	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1809	#num#	k4
(	(	kIx(
<g/>
spojeno	spojit	k5eAaPmNgNnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
4	#num#	k4
od	od	k7c2
roku	rok	k1gInSc2
1809	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1811	#num#	k4
<g/>
,	,	kIx,
*	*	kIx~
vytvořeno	vytvořit	k5eAaPmNgNnS
Napoleonem	napoleon	k1gInSc7
<g/>
,	,	kIx,
**	**	k?
povýšeno	povýšen	k2eAgNnSc4d1
Napoleonem	napoleon	k1gInSc7
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
2033949-5	2033949-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2192	#num#	k4
3937	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83050541	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
156590657	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83050541	#num#	k4
</s>
