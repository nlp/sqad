<p>
<s>
Žebrák	Žebrák	k1gInSc1	Žebrák
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Nečín	Nečína	k1gFnPc2	Nečína
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Příbram	Příbram	k1gFnSc1	Příbram
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
od	od	k7c2	od
Nečína	Nečín	k1gInSc2	Nečín
<g/>
.	.	kIx.	.
</s>
<s>
Vesnicí	vesnice	k1gFnSc7	vesnice
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
102	[number]	k4	102
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
evidováno	evidovat	k5eAaImNgNnS	evidovat
48	[number]	k4	48
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
48	[number]	k4	48
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
Žebrák	žebrák	k1gMnSc1	žebrák
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Žebrák	Žebrák	k1gInSc4	Žebrák
u	u	k7c2	u
Nečína	Nečín	k1gInSc2	Nečín
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
3,23	[number]	k4	3,23
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Žebrák	Žebrák	k1gInSc4	Žebrák
u	u	k7c2	u
Nečína	Nečín	k1gInSc2	Nečín
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
Vaječník	vaječník	k1gInSc1	vaječník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vesnici	vesnice	k1gFnSc6	vesnice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1653	[number]	k4	1653
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
fotografie	fotografia	k1gFnPc1	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Žebrák	Žebrák	k1gInSc1	Žebrák
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
katastr	katastr	k1gInSc1	katastr
<g/>
:	:	kIx,	:
<g/>
701939	[number]	k4	701939
<g/>
)	)	kIx)	)
</s>
</p>
