<p>
<s>
Ray-Ban	Ray-Ban	k1gMnSc1	Ray-Ban
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
výrobce	výrobce	k1gMnSc1	výrobce
slunečních	sluneční	k2eAgFnPc2d1	sluneční
a	a	k8xC	a
dioptrických	dioptrický	k2eAgFnPc2d1	dioptrická
brýlí	brýle	k1gFnPc2	brýle
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byly	být	k5eAaImAgInP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
pro	pro	k7c4	pro
americké	americký	k2eAgMnPc4d1	americký
letce	letec	k1gMnPc4	letec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
hollywodských	hollywodský	k2eAgInPc6d1	hollywodský
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
popularita	popularita	k1gFnSc1	popularita
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značka	značka	k1gFnSc1	značka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
Bausch	Bausch	k1gMnSc1	Bausch
<g/>
&	&	k?	&
<g/>
Lomb	Lomb	k1gMnSc1	Lomb
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
díky	díky	k7c3	díky
filmům	film	k1gInPc3	film
Jamese	Jamese	k1gFnSc2	Jamese
Deana	Deano	k1gNnSc2	Deano
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
díky	díky	k7c3	díky
filmu	film	k1gInSc3	film
Top	topit	k5eAaImRp2nS	topit
Gun	Gun	k1gMnPc4	Gun
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc3d3	veliký
popularitě	popularita	k1gFnSc3	popularita
se	se	k3xPyFc4	se
brýle	brýle	k1gFnPc1	brýle
těšily	těšit	k5eAaImAgFnP	těšit
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
popularita	popularita	k1gFnSc1	popularita
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
nápadně	nápadně	k6eAd1	nápadně
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
zachránil	zachránit	k5eAaPmAgInS	zachránit
až	až	k9	až
film	film	k1gInSc1	film
Matrix	Matrix	k1gInSc1	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
Bausch	Bausch	k1gInSc1	Bausch
<g/>
&	&	k?	&
<g/>
Lomb	Lomb	k1gInSc4	Lomb
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
Ray-Ban	Ray-Ban	k1gInSc4	Ray-Ban
prodat	prodat	k5eAaPmF	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
vlastní	vlastní	k2eAgFnSc4d1	vlastní
značku	značka	k1gFnSc4	značka
Ray-Ban	Ray-Bany	k1gInPc2	Ray-Bany
italská	italský	k2eAgFnSc1d1	italská
firma	firma	k1gFnSc1	firma
Luxottica	Luxottica	k1gFnSc1	Luxottica
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ray-Ban	Ray-Bana	k1gFnPc2	Ray-Bana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.ray-ban.com	www.rayan.com	k1gInSc1	www.ray-ban.com
</s>
</p>
