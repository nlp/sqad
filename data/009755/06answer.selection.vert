<s>
Sémantika	sémantika	k1gFnSc1	sémantika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
významem	význam	k1gInSc7	význam
jazykových	jazykový	k2eAgInPc2d1	jazykový
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
(	(	kIx(	(
<g/>
morfémů	morfém	k1gInPc2	morfém
a	a	k8xC	a
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
složitých	složitý	k2eAgFnPc2d1	složitá
(	(	kIx(	(
<g/>
frází	fráze	k1gFnPc2	fráze
<g/>
,	,	kIx,	,
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
souvětí	souvětí	k1gNnPc2	souvětí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
