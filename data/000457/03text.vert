<s>
Pád	Pád	k1gInSc1	Pád
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Po	Po	kA	Po
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Padus	Padus	k1gInSc1	Padus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Benátsko	Benátsko	k1gNnSc1	Benátsko
<g/>
,	,	kIx,	,
Emilia-Romagna	Emilia-Romagna	k1gFnSc1	Emilia-Romagna
<g/>
,	,	kIx,	,
Piemont	Piemont	k1gInSc1	Piemont
<g/>
,	,	kIx,	,
Lombardie	Lombardie	k1gFnSc1	Lombardie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
652	[number]	k4	652
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
75	[number]	k4	75
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nP	pramenit
v	v	k7c6	v
Kottických	Kottický	k2eAgFnPc6d1	Kottický
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Teče	téct	k5eAaImIp3nS	téct
převážně	převážně	k6eAd1	převážně
Pádskou	pádský	k2eAgFnSc7d1	Pádská
rovinou	rovina	k1gFnSc7	rovina
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ústím	ústí	k1gNnSc7	ústí
Tanara	Tanara	k1gFnSc1	Tanara
je	být	k5eAaImIp3nS	být
koryto	koryto	k1gNnSc4	koryto
Pádu	Pád	k1gInSc2	Pád
široké	široký	k2eAgNnSc4d1	široké
300	[number]	k4	300
až	až	k9	až
350	[number]	k4	350
m	m	kA	m
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
hrázemi	hráz	k1gFnPc7	hráz
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jsou	být	k5eAaImIp3nP	být
chráněna	chráněn	k2eAgNnPc1d1	chráněno
i	i	k8xC	i
koryta	koryto	k1gNnPc1	koryto
řady	řada	k1gFnSc2	řada
přítoků	přítok	k1gInPc2	přítok
v	v	k7c6	v
Pádské	pádský	k2eAgFnSc6d1	Pádská
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
do	do	k7c2	do
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
bažinatou	bažinatý	k2eAgFnSc4d1	bažinatá
deltu	delta	k1gFnSc4	delta
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1500	[number]	k4	1500
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
60	[number]	k4	60
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
a	a	k8xC	a
nejvodnější	vodný	k2eAgInPc1d3	vodný
jsou	být	k5eAaImIp3nP	být
levé	levý	k2eAgInPc1d1	levý
přítoky	přítok	k1gInPc1	přítok
Dora	Dora	k1gFnSc1	Dora
Riparia	Riparium	k1gNnPc1	Riparium
<g/>
,	,	kIx,	,
Dora	Dora	k1gFnSc1	Dora
Baltea	Baltea	k1gFnSc1	Baltea
<g/>
,	,	kIx,	,
Ticino	Ticino	k1gNnSc1	Ticino
<g/>
,	,	kIx,	,
Adda	Adda	k1gFnSc1	Adda
<g/>
,	,	kIx,	,
Oglio	Oglio	k1gNnSc1	Oglio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgInPc1d1	pravý
přítoky	přítok	k1gInPc1	přítok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pramení	pramenit	k5eAaImIp3nP	pramenit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
severních	severní	k2eAgInPc6d1	severní
svazích	svah	k1gInPc6	svah
Toskánsko-Emiliánských	Toskánsko-Emiliánský	k2eAgFnPc2d1	Toskánsko-Emiliánský
Apenin	Apeniny	k1gFnPc2	Apeniny
a	a	k8xC	a
také	také	k9	také
Přímořských	přímořský	k2eAgFnPc2d1	přímořská
Alp	Alpy	k1gFnPc2	Alpy
mají	mít	k5eAaImIp3nP	mít
obyčejně	obyčejně	k6eAd1	obyčejně
vody	voda	k1gFnSc2	voda
málo	málo	k1gNnSc1	málo
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
je	být	k5eAaImIp3nS	být
Tanaro	Tanara	k1gFnSc5	Tanara
<g/>
)	)	kIx)	)
a	a	k8xC	a
unášejí	unášet	k5eAaImIp3nP	unášet
mnoho	mnoho	k4c4	mnoho
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
veškerá	veškerý	k3xTgNnPc4	veškerý
ochranná	ochranný	k2eAgNnPc4d1	ochranné
opatření	opatření	k1gNnPc4	opatření
dochází	docházet	k5eAaImIp3nS	docházet
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
silných	silný	k2eAgInPc6d1	silný
deštích	dešť	k1gInPc6	dešť
k	k	k7c3	k
mnohým	mnohý	k2eAgFnPc3d1	mnohá
povodním	povodeň	k1gFnPc3	povodeň
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
stoupá	stoupat	k5eAaImIp3nS	stoupat
úroveň	úroveň	k1gFnSc1	úroveň
hladiny	hladina	k1gFnSc2	hladina
řeky	řeka	k1gFnSc2	řeka
i	i	k8xC	i
jejích	její	k3xOp3gInPc2	její
přítoků	přítok	k1gInPc2	přítok
o	o	k7c4	o
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
m.	m.	k?	m.
Ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
povodním	povodeň	k1gFnPc3	povodeň
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
a	a	k8xC	a
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
povodně	povodeň	k1gFnPc1	povodeň
způsobily	způsobit	k5eAaPmAgFnP	způsobit
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
a	a	k8xC	a
vynutily	vynutit	k5eAaPmAgFnP	vynutit
si	se	k3xPyFc3	se
evakuaci	evakuace	k1gFnSc4	evakuace
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
činí	činit	k5eAaImIp3nS	činit
1460	[number]	k4	1460
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Levé	levý	k2eAgInPc1d1	levý
přítoky	přítok	k1gInPc1	přítok
mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgFnSc4d3	veliký
vodnost	vodnost	k1gFnSc4	vodnost
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
taje	tát	k5eAaImIp3nS	tát
sníh	sníh	k1gInSc1	sníh
a	a	k8xC	a
ledovce	ledovec	k1gInPc1	ledovec
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
a	a	k8xC	a
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
má	mít	k5eAaImIp3nS	mít
Pád	Pád	k1gInSc1	Pád
alpský	alpský	k2eAgInSc1d1	alpský
vodní	vodní	k2eAgInSc4d1	vodní
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgInPc1d1	pravý
přítoky	přítok	k1gInPc1	přítok
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
vodnost	vodnost	k1gFnSc4	vodnost
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vznik	vznik	k1gInSc1	vznik
dvou	dva	k4xCgNnPc2	dva
povodňových	povodňový	k2eAgNnPc2d1	povodňové
období	období	k1gNnPc2	období
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
a	a	k8xC	a
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Pádu	Pád	k1gInSc2	Pád
(	(	kIx(	(
<g/>
květen-červen	květen-červen	k2eAgMnSc1d1	květen-červen
a	a	k8xC	a
říjen-listopad	říjenistopad	k1gInSc1	říjen-listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
obvykle	obvykle	k6eAd1	obvykle
málo	málo	k1gNnSc4	málo
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
unášených	unášený	k2eAgFnPc2d1	unášená
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
13	[number]	k4	13
až	až	k9	až
15	[number]	k4	15
Mt	Mt	k1gFnPc2	Mt
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
na	na	k7c6	na
zavlažování	zavlažování	k1gNnSc6	zavlažování
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
do	do	k7c2	do
města	město	k1gNnSc2	město
Piacenza	Piacenz	k1gMnSc2	Piacenz
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
kanály	kanál	k1gInPc4	kanál
s	s	k7c7	s
Milánem	Milán	k1gInSc7	Milán
(	(	kIx(	(
<g/>
pomohl	pomoct	k5eAaPmAgMnS	pomoct
jej	on	k3xPp3gMnSc4	on
budovat	budovat	k5eAaImF	budovat
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnPc7	Vinca
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
Benátskou	benátský	k2eAgFnSc7d1	Benátská
lagunou	laguna	k1gFnSc7	laguna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přítocích	přítok	k1gInPc6	přítok
(	(	kIx(	(
<g/>
Adda	Adda	k1gMnSc1	Adda
<g/>
,	,	kIx,	,
Ticino	Ticino	k1gNnSc1	Ticino
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k6eAd1	také
splavných	splavný	k2eAgInPc6d1	splavný
kanálech	kanál	k1gInPc6	kanál
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
velkými	velký	k2eAgNnPc7d1	velké
jezery	jezero	k1gNnPc7	jezero
(	(	kIx(	(
<g/>
Komské	Komský	k2eAgNnSc1d1	Komský
<g/>
,	,	kIx,	,
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
,	,	kIx,	,
Gardské	Gardský	k2eAgNnSc4d1	Gardské
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
jejich	jejich	k3xOp3gInPc2	jejich
levých	levý	k2eAgInPc2d1	levý
přítoků	přítok	k1gInPc2	přítok
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc1d1	postavena
kaskády	kaskáda	k1gFnPc1	kaskáda
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
města	město	k1gNnSc2	město
Turín	Turín	k1gInSc1	Turín
<g/>
,	,	kIx,	,
Piacenza	Piacenza	k1gFnSc1	Piacenza
<g/>
,	,	kIx,	,
Cremona	Cremona	k1gFnSc1	Cremona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
П	П	k?	П
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Pád	Pád	k1gInSc1	Pád
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pád	Pád	k1gInSc1	Pád
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
povodí	povodí	k1gNnSc4	povodí
řeky	řeka	k1gFnSc2	řeka
Pád	Pád	k1gInSc1	Pád
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
Meziregionální	Meziregionální	k2eAgFnSc3d1	Meziregionální
(	(	kIx(	(
<g/>
Benátsko	Benátsko	k1gNnSc1	Benátsko
<g/>
,	,	kIx,	,
Emilia-Romagna	Emilia-Romagna	k1gFnSc1	Emilia-Romagna
<g/>
,	,	kIx,	,
Piemont	Piemont	k1gInSc1	Piemont
<g/>
,	,	kIx,	,
Lombardie	Lombardie	k1gFnSc1	Lombardie
<g/>
)	)	kIx)	)
kancelář	kancelář	k1gFnSc1	kancelář
pro	pro	k7c4	pro
řeku	řeka	k1gFnSc4	řeka
Pád	Pád	k1gInSc1	Pád
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
Prameny	pramen	k1gInPc1	pramen
Pádu	Pád	k1gInSc2	Pád
</s>
