<p>
<s>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
je	být	k5eAaImIp3nS	být
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
dánský	dánský	k2eAgMnSc1d1	dánský
fyzik	fyzik	k1gMnSc1	fyzik
Niels	Nielsa	k1gFnPc2	Nielsa
Bohr	Bohr	k1gMnSc1	Bohr
<g/>
.	.	kIx.	.
</s>
<s>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
následníkem	následník	k1gMnSc7	následník
Rutherfordova	Rutherfordův	k2eAgInSc2d1	Rutherfordův
modelu	model	k1gInSc2	model
atomu	atom	k1gInSc2	atom
a	a	k8xC	a
předchůdcem	předchůdce	k1gMnSc7	předchůdce
kvantověmechanického	kvantověmechanický	k2eAgInSc2d1	kvantověmechanický
modelu	model	k1gInSc2	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
přínosem	přínos	k1gInSc7	přínos
Bohrova	Bohrův	k2eAgInSc2d1	Bohrův
modelu	model	k1gInSc2	model
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
popisuje	popisovat	k5eAaImIp3nS	popisovat
stabilní	stabilní	k2eAgInSc4d1	stabilní
atom	atom	k1gInSc4	atom
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jeho	jeho	k3xOp3gFnPc4	jeho
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
kvantování	kvantování	k1gNnSc2	kvantování
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
.	.	kIx.	.
</s>
<s>
Bohr	Bohr	k1gInSc1	Bohr
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Motivace	motivace	k1gFnSc1	motivace
k	k	k7c3	k
modelu	model	k1gInSc3	model
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
tzv.	tzv.	kA	tzv.
Rutherfordův	Rutherfordův	k2eAgInSc4d1	Rutherfordův
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
základní	základní	k2eAgFnSc7d1	základní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
atom	atom	k1gInSc1	atom
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
hmotného	hmotný	k2eAgNnSc2d1	hmotné
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
lehkých	lehký	k2eAgInPc2d1	lehký
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Rutherfordův	Rutherfordův	k2eAgInSc1d1	Rutherfordův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
sice	sice	k8xC	sice
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
výsledky	výsledek	k1gInPc4	výsledek
experimentů	experiment	k1gInPc2	experiment
prováděných	prováděný	k2eAgInPc2d1	prováděný
při	při	k7c6	při
ostřelování	ostřelování	k1gNnSc6	ostřelování
velmi	velmi	k6eAd1	velmi
tenkých	tenký	k2eAgFnPc2d1	tenká
fólií	fólie	k1gFnPc2	fólie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nic	nic	k3yNnSc1	nic
neříkal	říkat	k5eNaImAgMnS	říkat
o	o	k7c6	o
spektroskopických	spektroskopický	k2eAgFnPc6d1	spektroskopická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
ukázáno	ukázat	k5eAaPmNgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomy	atom	k1gInPc1	atom
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Rutherfordova	Rutherfordův	k2eAgInSc2d1	Rutherfordův
modelu	model	k1gInSc2	model
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
nestabilní	stabilní	k2eNgInPc1d1	nestabilní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
příkrém	příkrý	k2eAgInSc6d1	příkrý
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
naší	náš	k3xOp1gFnSc7	náš
zkušeností	zkušenost	k1gFnSc7	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Bohr	Bohr	k1gMnSc1	Bohr
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
pokusil	pokusit	k5eAaPmAgMnS	pokusit
nalézt	nalézt	k5eAaPmF	nalézt
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
stabilní	stabilní	k2eAgMnSc1d1	stabilní
a	a	k8xC	a
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
spektrum	spektrum	k1gNnSc4	spektrum
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postuláty	postulát	k1gInPc4	postulát
==	==	k?	==
</s>
</p>
<p>
<s>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
postulátech	postulát	k1gInPc6	postulát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Elektrony	elektron	k1gInPc1	elektron
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
kružnicových	kružnicový	k2eAgFnPc6d1	kružnicová
trajektoriích	trajektorie	k1gFnPc6	trajektorie
(	(	kIx(	(
<g/>
hladinách	hladina	k1gFnPc6	hladina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
nevyzařují	vyzařovat	k5eNaImIp3nP	vyzařovat
žádné	žádný	k3yNgNnSc4	žádný
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
hladiny	hladina	k1gFnSc2	hladina
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
elektron	elektron	k1gInSc1	elektron
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
(	(	kIx(	(
<g/>
pohltí	pohltit	k5eAaPmIp3nS	pohltit
<g/>
)	)	kIx)	)
právě	právě	k9	právě
1	[number]	k4	1
foton	foton	k1gInSc1	foton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
dovoleny	dovolit	k5eAaPmNgFnP	dovolit
ty	ten	k3xDgFnPc1	ten
trajektorie	trajektorie	k1gFnPc1	trajektorie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
L	L	kA	L
je	být	k5eAaImIp3nS	být
nħ	nħ	k?	nħ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
;	;	kIx,	;
a	a	k8xC	a
ħ	ħ	k?	ħ
je	být	k5eAaImIp3nS	být
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důsledky	důsledek	k1gInPc1	důsledek
Bohrova	Bohrov	k1gInSc2	Bohrov
modelu	model	k1gInSc2	model
==	==	k?	==
</s>
</p>
<p>
<s>
Veškeré	veškerý	k3xTgInPc4	veškerý
stavy	stav	k1gInPc4	stav
elektronu	elektron	k1gInSc2	elektron
v	v	k7c6	v
Bohrově	Bohrův	k2eAgInSc6d1	Bohrův
atomu	atom	k1gInSc6	atom
vodíku	vodík	k1gInSc2	vodík
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
jediným	jediný	k2eAgNnSc7d1	jediné
kvantovým	kvantový	k2eAgNnSc7d1	kvantové
číslem	číslo	k1gNnSc7	číslo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
které	který	k3yQgInPc1	který
můžeme	moct	k5eAaImIp1nP	moct
interpretovat	interpretovat	k5eAaBmF	interpretovat
jako	jako	k9	jako
číslo	číslo	k1gNnSc4	číslo
hladiny	hladina	k1gFnSc2	hladina
(	(	kIx(	(
<g/>
počítáno	počítat	k5eAaImNgNnS	počítat
vzestupně	vzestupně	k6eAd1	vzestupně
od	od	k7c2	od
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Poloměr	poloměr	k1gInSc1	poloměr
kružnicové	kružnicový	k2eAgFnSc2d1	kružnicová
dráhy	dráha	k1gFnSc2	dráha
n-té	ný	k2eAgFnSc2d1	n-tý
hladiny	hladina	k1gFnSc2	hladina
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
elektron	elektron	k1gInSc1	elektron
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
me	me	k?	me
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
e	e	k0	e
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
náboj	náboj	k1gInSc4	náboj
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
permitivita	permitivita	k1gFnSc1	permitivita
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc4	hbar
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
r	r	kA	r
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
=	=	kIx~	=
5,29	[number]	k4	5,29
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
11	[number]	k4	11
m	m	kA	m
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
poloměr	poloměr	k1gInSc1	poloměr
vodíkového	vodíkový	k2eAgInSc2d1	vodíkový
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Energie	energie	k1gFnSc1	energie
elektronu	elektron	k1gInSc2	elektron
vázaného	vázané	k1gNnSc2	vázané
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
vodíku	vodík	k1gInSc2	vodík
na	na	k7c6	na
n-té	ný	k2eAgFnSc6d1	n-tý
hladině	hladina	k1gFnSc6	hladina
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
me	me	k?	me
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
elektronu	elektron	k1gInSc2	elektron
na	na	k7c6	na
n-té	ný	k2eAgFnSc6d1	n-tý
hladině	hladina	k1gFnSc6	hladina
jest	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ħ	ħ	k?	ħ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Experimentální	experimentální	k2eAgNnSc4d1	experimentální
ověření	ověření	k1gNnSc4	ověření
Bohrova	Bohrův	k2eAgInSc2d1	Bohrův
modelu	model	k1gInSc2	model
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
provedli	provést	k5eAaPmAgMnP	provést
James	James	k1gMnSc1	James
Franck	Franck	k1gMnSc1	Franck
a	a	k8xC	a
Gustav	Gustav	k1gMnSc1	Gustav
Ludwig	Ludwig	k1gMnSc1	Ludwig
Hertz	hertz	k1gInSc4	hertz
tzv.	tzv.	kA	tzv.
Franck-Hertzův	Franck-Hertzův	k2eAgInSc4d1	Franck-Hertzův
experiment	experiment	k1gInSc4	experiment
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
nespektroskopicky	spektroskopicky	k6eNd1	spektroskopicky
prokázali	prokázat	k5eAaPmAgMnP	prokázat
existenci	existence	k1gFnSc4	existence
Bohrových	Bohrův	k2eAgFnPc2d1	Bohrova
energetických	energetický	k2eAgFnPc2d1	energetická
hladin	hladina	k1gFnPc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
experiment	experiment	k1gInSc1	experiment
byl	být	k5eAaImAgInS	být
vykonán	vykonat	k5eAaPmNgInS	vykonat
a	a	k8xC	a
publikován	publikovat	k5eAaBmNgInS	publikovat
již	již	k9	již
rok	rok	k1gInSc4	rok
po	po	k7c6	po
uveřejnění	uveřejnění	k1gNnSc6	uveřejnění
Bohrova	Bohrův	k2eAgInSc2d1	Bohrův
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
rychlému	rychlý	k2eAgNnSc3d1	rychlé
přijetí	přijetí	k1gNnSc3	přijetí
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Omezení	omezení	k1gNnSc1	omezení
Bohrova	Bohrův	k2eAgInSc2d1	Bohrův
modelu	model	k1gInSc2	model
==	==	k?	==
</s>
</p>
<p>
<s>
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
popisuje	popisovat	k5eAaImIp3nS	popisovat
atom	atom	k1gInSc4	atom
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
iontů	ion	k1gInPc2	ion
mající	mající	k2eAgInSc1d1	mající
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
elektron	elektron	k1gInSc1	elektron
(	(	kIx(	(
<g/>
He	he	k0	he
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Li	li	k8xS	li
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
Be	Be	k1gFnSc1	Be
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
a	a	k8xC	a
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
atomů	atom	k1gInPc2	atom
mající	mající	k2eAgMnPc4d1	mající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc1	jeden
elektron	elektron	k1gInSc1	elektron
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
spektrální	spektrální	k2eAgFnSc2d1	spektrální
čáry	čára	k1gFnSc2	čára
odporující	odporující	k2eAgInSc4d1	odporující
experimentu	experiment	k1gInSc3	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
často	často	k6eAd1	často
hovoří	hovořit	k5eAaImIp3nS	hovořit
spíše	spíše	k9	spíše
o	o	k7c6	o
Bohrově	Bohrův	k2eAgInSc6d1	Bohrův
modelu	model	k1gInSc6	model
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
než	než	k8xS	než
obecněji	obecně	k6eAd2	obecně
o	o	k7c6	o
Bohrově	Bohrův	k2eAgInSc6d1	Bohrův
modelu	model	k1gInSc6	model
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
spektrum	spektrum	k1gNnSc1	spektrum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
naměřených	naměřený	k2eAgFnPc2d1	naměřená
hodnot	hodnota	k1gFnPc2	hodnota
liší	lišit	k5eAaImIp3nS	lišit
o	o	k7c4	o
0,05	[number]	k4	0,05
procenta	procento	k1gNnSc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
považuje	považovat	k5eAaImIp3nS	považovat
jádro	jádro	k1gNnSc1	jádro
za	za	k7c4	za
nehybné	hybný	k2eNgNnSc4d1	nehybné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
tiše	tiš	k1gFnPc4	tiš
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
má	mít	k5eAaImIp3nS	mít
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
ale	ale	k8xC	ale
hmotnost	hmotnost	k1gFnSc1	hmotnost
jádra	jádro	k1gNnSc2	jádro
není	být	k5eNaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
a	a	k8xC	a
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
elektron	elektron	k1gInSc1	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
rotuje	rotovat	k5eAaImIp3nS	rotovat
kolem	kolem	k7c2	kolem
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
jeho	jeho	k3xOp3gFnSc2	jeho
rotace	rotace	k1gFnSc2	rotace
přibližně	přibližně	k6eAd1	přibližně
dvoutisíckrát	dvoutisíckrát	k6eAd1	dvoutisíckrát
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bohrův	Bohrův	k2eAgInSc1d1	Bohrův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
přesnějším	přesný	k2eAgInSc7d2	přesnější
kvantově	kvantově	k6eAd1	kvantově
mechanickým	mechanický	k2eAgInSc7d1	mechanický
modelem	model	k1gInSc7	model
atomu	atom	k1gInSc2	atom
již	již	k6eAd1	již
za	za	k7c4	za
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
mezikrokem	mezikrok	k1gInSc7	mezikrok
k	k	k7c3	k
moderní	moderní	k2eAgFnSc3d1	moderní
kvantové	kvantový	k2eAgFnSc3d1	kvantová
mechanice	mechanika	k1gFnSc3	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Planckově	Planckův	k2eAgInSc6d1	Planckův
vyzařovacím	vyzařovací	k2eAgInSc6d1	vyzařovací
zákonu	zákon	k1gInSc6	zákon
a	a	k8xC	a
Einsteinově	Einsteinův	k2eAgNnSc6d1	Einsteinovo
vysvětlení	vysvětlení	k1gNnSc6	vysvětlení
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc1	třetí
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ukázala	ukázat	k5eAaPmAgFnS	ukázat
nutnost	nutnost	k1gFnSc4	nutnost
kvantování	kvantování	k1gNnSc3	kvantování
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k6eAd1	pak
momentu	moment	k1gInSc3	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
kvantování	kvantování	k1gNnSc1	kvantování
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
právě	právě	k9	právě
i	i	k9	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odvození	odvození	k1gNnSc3	odvození
postulátů	postulát	k1gInPc2	postulát
v	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
kontextu	kontext	k1gInSc6	kontext
==	==	k?	==
</s>
</p>
<p>
<s>
Bohr	Bohr	k1gInSc1	Bohr
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
nalézt	nalézt	k5eAaPmF	nalézt
stabilní	stabilní	k2eAgInSc4d1	stabilní
model	model	k1gInSc4	model
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
<g/>
,	,	kIx,	,
ponechal	ponechat	k5eAaPmAgInS	ponechat
poznatek	poznatek	k1gInSc1	poznatek
Rutherfordova	Rutherfordův	k2eAgInSc2d1	Rutherfordův
modelu	model	k1gInSc2	model
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
hmotném	hmotný	k2eAgNnSc6d1	hmotné
jádře	jádro	k1gNnSc6	jádro
a	a	k8xC	a
soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
atomový	atomový	k2eAgInSc4d1	atomový
obal	obal	k1gInSc4	obal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
vyzbrojen	vyzbrojit	k5eAaPmNgInS	vyzbrojit
tehdejšími	tehdejší	k2eAgInPc7d1	tehdejší
poznatky	poznatek	k1gInPc7	poznatek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
fotoelektrického	fotoelektrický	k2eAgInSc2d1	fotoelektrický
jevu	jev	k1gInSc2	jev
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
zavedl	zavést	k5eAaPmAgMnS	zavést
pojem	pojem	k1gInSc4	pojem
fotonu	foton	k1gInSc2	foton
a	a	k8xC	a
určil	určit	k5eAaPmAgInS	určit
jeho	jeho	k3xOp3gFnSc4	jeho
energii	energie	k1gFnSc4	energie
vzorcem	vzorec	k1gInSc7	vzorec
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
foton	foton	k1gInSc1	foton
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
h	h	k?	h
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
frekvence	frekvence	k1gFnSc1	frekvence
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rydbergovou	Rydbergův	k2eAgFnSc7d1	Rydbergova
formulí	formule	k1gFnSc7	formule
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
beze	beze	k7c2	beze
zbytku	zbytek	k1gInSc2	zbytek
popsat	popsat	k5eAaPmF	popsat
celé	celý	k2eAgNnSc4d1	celé
spektrum	spektrum	k1gNnSc4	spektrum
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
Z	z	k7c2	z
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnPc6	R_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
Rydbergova	Rydbergův	k2eAgFnSc1d1	Rydbergova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
postulát	postulát	k1gInSc1	postulát
(	(	kIx(	(
<g/>
O	o	k7c6	o
vyzařování	vyzařování	k1gNnSc6	vyzařování
a	a	k8xC	a
pohlcování	pohlcování	k1gNnSc6	pohlcování
fotonu	foton	k1gInSc2	foton
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
fotony	foton	k1gInPc4	foton
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
ve	v	k7c6	v
spektroskopických	spektroskopický	k2eAgInPc6d1	spektroskopický
experimentech	experiment	k1gInPc6	experiment
<g/>
,	,	kIx,	,
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
elektron	elektron	k1gInSc1	elektron
(	(	kIx(	(
<g/>
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
bude	být	k5eAaImBp3nS	být
foton	foton	k1gInSc1	foton
mít	mít	k5eAaImF	mít
energii	energie	k1gFnSc4	energie
rovnu	roven	k2eAgFnSc4d1	rovna
rozdílu	rozdíl	k1gInSc3	rozdíl
energie	energie	k1gFnSc2	energie
elektronu	elektron	k1gInSc2	elektron
před	před	k7c7	před
vyzářením	vyzáření	k1gNnSc7	vyzáření
fotonu	foton	k1gInSc2	foton
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnPc6	E_
<g/>
{	{	kIx(	{
<g/>
foton	foton	k1gInSc1	foton
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-E_	-E_	k?	-E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Použitím	použití	k1gNnSc7	použití
Rydbergovy	Rydbergův	k2eAgFnSc2d1	Rydbergova
formule	formule	k1gFnSc2	formule
a	a	k8xC	a
Einsteinova	Einsteinův	k2eAgInSc2d1	Einsteinův
vzorce	vzorec	k1gInSc2	vzorec
pro	pro	k7c4	pro
energii	energie	k1gFnSc4	energie
fotonu	foton	k1gInSc2	foton
dostáváme	dostávat	k5eAaImIp1nP	dostávat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
f	f	k?	f
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-E_	-E_	k?	-E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
t_	t_	k?	t_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
foton	foton	k1gInSc1	foton
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
h	h	k?	h
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
=	=	kIx~	=
<g/>
hc	hc	k?	hc
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
hcR_	hcR_	k?	hcR_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
hcR_	hcR_	k?	hcR_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
-hcR_	cR_	k?	-hcR_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n_	n_	k?	n_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc1	rozdíl
dvou	dva	k4xCgFnPc2	dva
energií	energie	k1gFnPc2	energie
a	a	k8xC	a
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc4	rozdíl
dvou	dva	k4xCgInPc2	dva
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
mající	mající	k2eAgFnSc4d1	mající
jednotku	jednotka	k1gFnSc4	jednotka
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
energii	energie	k1gFnSc4	energie
elektronu	elektron	k1gInSc2	elektron
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
h	h	k?	h
</s>
</p>
<p>
<s>
c	c	k0	c
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
hcR_	hcR_	k?	hcR_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
energie	energie	k1gFnSc1	energie
elektronu	elektron	k1gInSc2	elektron
v	v	k7c6	v
atomovém	atomový	k2eAgInSc6d1	atomový
obalu	obal	k1gInSc6	obal
vodíku	vodík	k1gInSc2	vodík
nemůže	moct	k5eNaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
libovolných	libovolný	k2eAgFnPc2d1	libovolná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
diskrétních	diskrétní	k2eAgFnPc2d1	diskrétní
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
postulát	postulát	k1gInSc1	postulát
(	(	kIx(	(
<g/>
O	o	k7c6	o
kružnicových	kružnicový	k2eAgFnPc6d1	kružnicová
drahách	draha	k1gFnPc6	draha
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Bohr	Bohr	k1gInSc1	Bohr
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
Rutherford	Rutherford	k1gInSc1	Rutherford
<g/>
,	,	kIx,	,
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
interakce	interakce	k1gFnSc1	interakce
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
elektrostatická	elektrostatický	k2eAgFnSc1d1	elektrostatická
a	a	k8xC	a
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
síla	síla	k1gFnSc1	síla
bude	být	k5eAaImBp3nS	být
klesat	klesat	k5eAaImF	klesat
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
mocninou	mocnina	k1gFnSc7	mocnina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
Isaac	Isaac	k1gInSc1	Isaac
Newton	Newton	k1gMnSc1	Newton
v	v	k7c6	v
případě	případ	k1gInSc6	případ
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
síla	síla	k1gFnSc1	síla
také	také	k9	také
klesá	klesat	k5eAaImIp3nS	klesat
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
mocninou	mocnina	k1gFnSc7	mocnina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediné	jediný	k2eAgFnPc1d1	jediná
možné	možný	k2eAgFnPc1d1	možná
stabilní	stabilní	k2eAgFnPc1d1	stabilní
dráhy	dráha	k1gFnPc1	dráha
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kružnice	kružnice	k1gFnSc2	kružnice
nebo	nebo	k8xC	nebo
elipsy	elipsa	k1gFnSc2	elipsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stabilita	stabilita	k1gFnSc1	stabilita
v	v	k7c6	v
atomu	atom	k1gInSc6	atom
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
jako	jako	k8xC	jako
rovnováhu	rovnováha	k1gFnSc4	rovnováha
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přitažlivá	přitažlivý	k2eAgFnSc1d1	přitažlivá
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
mezi	mezi	k7c7	mezi
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
elektronem	elektron	k1gInSc7	elektron
vykompenzovala	vykompenzovat	k5eAaPmAgFnS	vykompenzovat
s	s	k7c7	s
odstředivou	odstředivý	k2eAgFnSc7d1	odstředivá
silou	síla	k1gFnSc7	síla
působící	působící	k2eAgNnSc4d1	působící
na	na	k7c4	na
elektron	elektron	k1gInSc4	elektron
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
obíhá	obíhat	k5eAaImIp3nS	obíhat
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejjednodušším	jednoduchý	k2eAgInSc6d3	nejjednodušší
případě	případ	k1gInSc6	případ
kružnicové	kružnicový	k2eAgFnSc2d1	kružnicová
dráhy	dráha	k1gFnSc2	dráha
to	ten	k3xDgNnSc4	ten
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Energie	energie	k1gFnSc1	energie
elektronu	elektron	k1gInSc2	elektron
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
součtem	součet	k1gInSc7	součet
kinetické	kinetický	k2eAgFnSc2d1	kinetická
a	a	k8xC	a
elektrostatické	elektrostatický	k2eAgFnSc2d1	elektrostatická
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
k	k	k7c3	k
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
t	t	k?	t
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
kin	kino	k1gNnPc2	kino
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
E_	E_	k1gFnSc1	E_
<g/>
{	{	kIx(	{
<g/>
pot	pot	k1gInSc1	pot
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
mv	mv	k?	mv
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
r	r	kA	r
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Použitím	použití	k1gNnSc7	použití
předchozí	předchozí	k2eAgFnSc2d1	předchozí
rovnice	rovnice	k1gFnSc2	rovnice
dostáváme	dostávat	k5eAaImIp1nP	dostávat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
mv	mv	k?	mv
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
r	r	kA	r
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
mv	mv	k?	mv
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-mv	v	k?	-mv
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
mv	mv	k?	mv
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Vyjádřením	vyjádření	k1gNnSc7	vyjádření
rychlosti	rychlost	k1gFnSc2	rychlost
dostáváme	dostávat	k5eAaImIp1nP	dostávat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
E_	E_	k1gFnPc2	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
získáme	získat	k5eAaPmIp1nP	získat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
E_	E_	k1gFnPc2	E_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
E_	E_	k1gFnPc2	E_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
mn	mn	k?	mn
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
E_	E_	k1gFnPc2	E_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
vztah	vztah	k1gInSc4	vztah
pro	pro	k7c4	pro
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
elektronu	elektron	k1gInSc2	elektron
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rovnosti	rovnost	k1gFnSc2	rovnost
elektrické	elektrický	k2eAgFnSc2d1	elektrická
a	a	k8xC	a
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Dosazením	dosazení	k1gNnSc7	dosazení
vztahu	vztah	k1gInSc2	vztah
pro	pro	k7c4	pro
rychlost	rychlost	k1gFnSc4	rychlost
dostáváme	dostávat	k5eAaImIp1nP	dostávat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
e	e	k0	e
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
mv	mv	k?	mv
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
mv_	mv_	k?	mv_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}	}	kIx)	}
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Dostáváme	dostávat	k5eAaImIp1nP	dostávat
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektron	elektron	k1gInSc1	elektron
může	moct	k5eAaImIp3nS	moct
obíhat	obíhat	k5eAaImF	obíhat
po	po	k7c6	po
kružnicových	kružnicový	k2eAgFnPc6d1	kružnicová
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
elektronu	elektron	k1gInSc2	elektron
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
libovolná	libovolný	k2eAgFnSc1d1	libovolná
<g/>
.	.	kIx.	.
</s>
<s>
Vyzáření	vyzáření	k1gNnSc1	vyzáření
nebo	nebo	k8xC	nebo
pohlcení	pohlcení	k1gNnSc1	pohlcení
fotonu	foton	k1gInSc2	foton
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektron	elektron	k1gInSc1	elektron
přeskočí	přeskočit	k5eAaPmIp3nS	přeskočit
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kružnici	kružnice	k1gFnSc4	kružnice
(	(	kIx(	(
<g/>
hladinu	hladina	k1gFnSc4	hladina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
postulát	postulát	k1gInSc1	postulát
(	(	kIx(	(
<g/>
O	o	k7c6	o
kvantování	kvantování	k1gNnSc6	kvantování
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
i	i	k8xC	i
poloměr	poloměr	k1gInSc1	poloměr
dráhy	dráha	k1gFnSc2	dráha
elektronu	elektron	k1gInSc2	elektron
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
atomu	atom	k1gInSc2	atom
nemohou	moct	k5eNaImIp3nP	moct
nabývat	nabývat	k5eAaImF	nabývat
libovolné	libovolný	k2eAgFnPc4d1	libovolná
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
diskrétní	diskrétní	k2eAgFnPc1d1	diskrétní
<g/>
.	.	kIx.	.
</s>
<s>
Odvozené	odvozený	k2eAgInPc1d1	odvozený
vztahy	vztah	k1gInPc1	vztah
jsou	být	k5eAaImIp3nP	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
E_	E_	k1gMnSc1	E_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
\	\	kIx~	\
v	v	k7c6	v
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
\	\	kIx~	\
r	r	kA	r
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
<g/>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tedy	tedy	k9	tedy
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jakou	jaký	k3yIgFnSc4	jaký
veličinu	veličina	k1gFnSc4	veličina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
rozložení	rozložení	k1gNnSc1	rozložení
hodnot	hodnota	k1gFnPc2	hodnota
rovnoměrné	rovnoměrný	k2eAgNnSc1d1	rovnoměrné
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n.	n.	k?	n.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Veličina	veličina	k1gFnSc1	veličina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
r	r	kA	r
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgMnSc1d1	cdot
v	v	k7c6	v
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
max	max	kA	max
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
a-b	a	k?	a-b
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
a-b	a	k?	a-b
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
rozměr	rozměr	k1gInSc1	rozměr
této	tento	k3xDgFnSc2	tento
veličiny	veličina	k1gFnSc2	veličina
bude	být	k5eAaImBp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-b	-b	k?	-b
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Požadujeme	požadovat	k5eAaImIp1nP	požadovat
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
pak	pak	k6eAd1	pak
požadujeme	požadovat	k5eAaImIp1nP	požadovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
a-b	a	k?	a-b
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Tehdy	tehdy	k6eAd1	tehdy
lze	lze	k6eAd1	lze
rozměr	rozměr	k1gInSc4	rozměr
veličiny	veličina	k1gFnSc2	veličina
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-b	-b	k?	-b
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
a-	a-	k?	a-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
a-	a-	k?	a-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
<g/>
Postupným	postupný	k2eAgNnSc7d1	postupné
dosazováním	dosazování	k1gNnSc7	dosazování
hodnot	hodnota	k1gFnPc2	hodnota
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
do	do	k7c2	do
předchozího	předchozí	k2eAgInSc2d1	předchozí
vzorce	vzorec	k1gInSc2	vzorec
dostáváme	dostávat	k5eAaImIp1nP	dostávat
jednotky	jednotka	k1gFnPc1	jednotka
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
q_	q_	k?	q_
<g/>
{	{	kIx(	{
<g/>
min	mina	k1gFnPc2	mina
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n.	n.	k?	n.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
m-	m-	k?	m-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
s	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
m-	m-	k?	m-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
s-	s-	k?	s-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
s-	s-	k?	s-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Protože	protože	k8xS	protože
pro	pro	k7c4	pro
veličiny	veličina	k1gFnPc4	veličina
se	s	k7c7	s
čtvrtou	čtvrtá	k1gFnSc7	čtvrtá
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc7d2	vyšší
mocninou	mocnina	k1gFnSc7	mocnina
délky	délka	k1gFnSc2	délka
nejsou	být	k5eNaImIp3nP	být
názorné	názorný	k2eAgFnPc4d1	názorná
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
veličiny	veličina	k1gFnPc4	veličina
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
studovat	studovat	k5eAaImF	studovat
pouze	pouze	k6eAd1	pouze
m-	m-	k?	m-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
s	s	k7c7	s
a	a	k8xC	a
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
s-	s-	k?	s-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
jednotkou	jednotka	k1gFnSc7	jednotka
převrácené	převrácený	k2eAgFnSc2d1	převrácená
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
podílem	podíl	k1gInSc7	podíl
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
hmotnost	hmotnost	k1gFnSc1	hmotnost
není	být	k5eNaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
parametru	parametr	k1gInSc6	parametr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
moment	moment	k1gInSc1	moment
hybnosti	hybnost	k1gFnSc2	hybnost
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
n	n	k0	n
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
min	min	kA	min
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
<g/>
Třetí	třetí	k4xOgInSc1	třetí
postulát	postulát	k1gInSc1	postulát
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
z	z	k7c2	z
historického	historický	k2eAgInSc2d1	historický
pohledu	pohled	k1gInSc2	pohled
důsledkem	důsledek	k1gInSc7	důsledek
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nový	nový	k2eAgInSc1d1	nový
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
vodíku	vodík	k1gInSc2	vodík
byl	být	k5eAaImAgMnS	být
stabilní	stabilní	k2eAgMnSc1d1	stabilní
a	a	k8xC	a
uměl	umět	k5eAaImAgInS	umět
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
čárové	čárový	k2eAgNnSc4d1	čárové
spektrum	spektrum	k1gNnSc4	spektrum
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Thomsonův	Thomsonův	k2eAgInSc1d1	Thomsonův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
</s>
</p>
<p>
<s>
Rutherfordův	Rutherfordův	k2eAgInSc1d1	Rutherfordův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
</s>
</p>
<p>
<s>
Kvantově-mechanický	Kvantověechanický	k2eAgInSc1d1	Kvantově-mechanický
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
</s>
</p>
