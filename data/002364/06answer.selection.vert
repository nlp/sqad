<s>
Česká	český	k2eAgNnPc1d1	české
slovesa	sloveso	k1gNnPc1	sloveso
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
3	[number]	k4	3
časy	čas	k1gInPc4	čas
<g/>
:	:	kIx,	:
minulý	minulý	k2eAgInSc1d1	minulý
(	(	kIx(	(
<g/>
préteritum	préteritum	k1gNnSc1	préteritum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přítomný	přítomný	k2eAgInSc1d1	přítomný
(	(	kIx(	(
<g/>
prézens	prézens	k1gInSc1	prézens
<g/>
)	)	kIx)	)
a	a	k8xC	a
budoucí	budoucí	k2eAgMnSc1d1	budoucí
(	(	kIx(	(
<g/>
futurum	futurum	k1gNnSc1	futurum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
