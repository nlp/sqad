<s>
Sinus	sinus	k1gInSc1	sinus
je	být	k5eAaImIp3nS	být
goniometrická	goniometrický	k2eAgFnSc1d1	goniometrická
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
značka	značka	k1gFnSc1	značka
sin	sin	kA	sin
doplněná	doplněný	k2eAgFnSc1d1	doplněná
značkou	značka	k1gFnSc7	značka
nezávisle	závisle	k6eNd1	závisle
proměnné	proměnná	k1gFnSc2	proměnná
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
úhlu	úhel	k1gInSc2	úhel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravoúhlém	pravoúhlý	k2eAgInSc6d1	pravoúhlý
trojúhelníku	trojúhelník	k1gInSc6	trojúhelník
bývá	bývat	k5eAaImIp3nS	bývat
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xC	jako
poměr	poměr	k1gInSc1	poměr
protilehlé	protilehlý	k2eAgFnSc2d1	protilehlá
odvěsny	odvěsna	k1gFnSc2	odvěsna
a	a	k8xC	a
přepony	přepona	k1gFnSc2	přepona
<g/>
.	.	kIx.	.
</s>
<s>
Definici	definice	k1gFnSc4	definice
lze	lze	k6eAd1	lze
konzistentně	konzistentně	k6eAd1	konzistentně
rozšířit	rozšířit	k5eAaPmF	rozšířit
jak	jak	k6eAd1	jak
na	na	k7c4	na
celá	celý	k2eAgNnPc4d1	celé
reálná	reálný	k2eAgNnPc4d1	reálné
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
do	do	k7c2	do
oboru	obor	k1gInSc2	obor
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Grafem	graf	k1gInSc7	graf
sinu	sinus	k1gInSc2	sinus
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
oboru	obor	k1gInSc6	obor
je	být	k5eAaImIp3nS	být
sinusoida	sinusoida	k1gFnSc1	sinusoida
<g/>
.	.	kIx.	.
</s>
<s>
Sinus	sinus	k1gInSc1	sinus
se	se	k3xPyFc4	se
jednoduše	jednoduše	k6eAd1	jednoduše
definuje	definovat	k5eAaBmIp3nS	definovat
na	na	k7c6	na
jednotkové	jednotkový	k2eAgFnSc6d1	jednotková
kružnici	kružnice	k1gFnSc6	kružnice
(	(	kIx(	(
<g/>
kružnici	kružnice	k1gFnSc4	kružnice
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
a	a	k8xC	a
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
α	α	k?	α
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
počáteční	počáteční	k2eAgNnSc4d1	počáteční
rameno	rameno	k1gNnSc4	rameno
v	v	k7c6	v
kladné	kladný	k2eAgFnSc6d1	kladná
poloose	poloosa	k1gFnSc6	poloosa
x	x	k?	x
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
od	od	k7c2	od
kladné	kladný	k2eAgFnSc2d1	kladná
poloosy	poloosa	k1gFnSc2	poloosa
x	x	k?	x
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sin	sin	kA	sin
α	α	k?	α
roven	roven	k2eAgInSc1d1	roven
y-ové	yvý	k2eAgFnSc3d1	y-ová
souřadnici	souřadnice	k1gFnSc3	souřadnice
průsečíku	průsečík	k1gInSc2	průsečík
této	tento	k3xDgFnSc2	tento
kružnice	kružnice	k1gFnSc2	kružnice
s	s	k7c7	s
koncovým	koncový	k2eAgNnSc7d1	koncové
ramenem	rameno	k1gNnSc7	rameno
úhlu	úhel	k1gInSc2	úhel
α	α	k?	α
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
v	v	k7c6	v
absolutní	absolutní	k2eAgFnSc6d1	absolutní
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
)	)	kIx)	)
délce	délka	k1gFnSc6	délka
kolmice	kolmice	k1gFnPc4	kolmice
spuštěné	spuštěný	k2eAgFnPc4d1	spuštěná
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
bodu	bod	k1gInSc2	bod
na	na	k7c4	na
osu	osa	k1gFnSc4	osa
x.	x.	k?	x.
Délce	délka	k1gFnSc3	délka
úsečky	úsečka	k1gFnSc2	úsečka
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
k	k	k7c3	k
patě	pata	k1gFnSc3	pata
této	tento	k3xDgFnSc2	tento
kolmice	kolmice	k1gFnSc2	kolmice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
přesněji	přesně	k6eAd2	přesně
(	(	kIx(	(
<g/>
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
znaménko	znaménko	k1gNnSc4	znaménko
<g/>
)	)	kIx)	)
x-ové	xvý	k2eAgFnSc3d1	x-ová
souřadnici	souřadnice	k1gFnSc3	souřadnice
průsečíku	průsečík	k1gInSc2	průsečík
jednotkové	jednotkový	k2eAgFnSc2d1	jednotková
kružnice	kružnice	k1gFnSc2	kružnice
s	s	k7c7	s
koncovým	koncový	k2eAgNnSc7d1	koncové
ramenem	rameno	k1gNnSc7	rameno
úhlu	úhel	k1gInSc2	úhel
α	α	k?	α
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
roven	roven	k2eAgInSc1d1	roven
cos	cos	kA	cos
α	α	k?	α
Poloměr	poloměr	k1gInSc1	poloměr
<g/>
,	,	kIx,	,
kolmice	kolmice	k1gFnSc1	kolmice
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
úsečka	úsečka	k1gFnSc1	úsečka
tvoří	tvořit	k5eAaImIp3nS	tvořit
pravoúhlý	pravoúhlý	k2eAgInSc4d1	pravoúhlý
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nějž	jenž	k3xRgInSc4	jenž
platí	platit	k5eAaImIp3nS	platit
Pythagorova	Pythagorův	k2eAgFnSc1d1	Pythagorova
věta	věta	k1gFnSc1	věta
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
také	také	k9	také
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
cos	cos	kA	cos
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednotkové	jednotkový	k2eAgFnSc6d1	jednotková
kružnici	kružnice	k1gFnSc6	kružnice
je	být	k5eAaImIp3nS	být
také	také	k9	také
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
sinus	sinus	k1gInSc1	sinus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
a	a	k8xC	a
druhém	druhý	k4xOgInSc6	druhý
kvadrantu	kvadrant	k1gInSc6	kvadrant
nezáporný	záporný	k2eNgInSc4d1	nezáporný
(	(	kIx(	(
<g/>
≥	≥	k?	≥
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ve	v	k7c6	v
třetím	třetí	k4xOgMnSc6	třetí
a	a	k8xC	a
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
nekladný	kladný	k2eNgInSc4d1	kladný
(	(	kIx(	(
<g/>
≤	≤	k?	≤
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kvadrantu	kvadrant	k1gInSc6	kvadrant
je	být	k5eAaImIp3nS	být
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
klesající	klesající	k2eAgFnSc7d1	klesající
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zřejmě	zřejmě	k6eAd1	zřejmě
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
=	=	kIx~	=
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
k	k	k7c3	k
⋅	⋅	k?	⋅
2	[number]	k4	2
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc2	alph
+	+	kIx~	+
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
k	k	k7c3	k
⋅	⋅	k?	⋅
:	:	kIx,	:
360	[number]	k4	360
:	:	kIx,	:
∘	∘	k?	∘
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc2	alph
+	+	kIx~	+
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
360	[number]	k4	360
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
circ	circ	k6eAd1	circ
}}	}}	k?	}}
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
libovolné	libovolný	k2eAgNnSc4d1	libovolné
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
funkci	funkce	k1gFnSc4	funkce
sinus	sinus	k1gInSc1	sinus
rozšířit	rozšířit	k5eAaPmF	rozšířit
i	i	k9	i
na	na	k7c4	na
záporné	záporný	k2eAgInPc4d1	záporný
úhly	úhel	k1gInPc4	úhel
a	a	k8xC	a
konzistentně	konzistentně	k6eAd1	konzistentně
definovat	definovat	k5eAaBmF	definovat
jako	jako	k8xS	jako
funkci	funkce	k1gFnSc4	funkce
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
množině	množina	k1gFnSc6	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Sinusoida	sinusoida	k1gFnSc1	sinusoida
pak	pak	k6eAd1	pak
zhruba	zhruba	k6eAd1	zhruba
(	(	kIx(	(
<g/>
při	při	k7c6	při
nekonečně	konečně	k6eNd1	konečně
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
ojnici	ojnice	k1gFnSc6	ojnice
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
například	například	k6eAd1	například
pohyb	pohyb	k1gInSc1	pohyb
pístu	píst	k1gInSc2	píst
ve	v	k7c6	v
válci	válec	k1gInSc6	válec
spalovacího	spalovací	k2eAgInSc2d1	spalovací
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Reálná	reálný	k2eAgFnSc1d1	reálná
funkce	funkce	k1gFnSc1	funkce
reálné	reálný	k2eAgFnSc2d1	reálná
proměnné	proměnná	k1gFnSc2	proměnná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgFnPc4d1	následující
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
libovolné	libovolný	k2eAgFnPc4d1	libovolná
<g />
.	.	kIx.	.
</s>
<s hack="1">
celé	celá	k1gFnSc6	celá
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Definiční	definiční	k2eAgInSc1d1	definiční
obor	obor	k1gInSc1	obor
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
(	(	kIx(	(
<g/>
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
<g/>
)	)	kIx)	)
Obor	obor	k1gInSc1	obor
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
−	−	k?	−
1	[number]	k4	1
;	;	kIx,	;
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
-1	-1	k4	-1
<g/>
;	;	kIx,	;
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
:	:	kIx,	:
Rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
<g/>
:	:	kIx,	:
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
+	+	kIx~	+
2	[number]	k4	2
k	k	k7c3	k
π	π	k?	π
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
+	+	kIx~	+
2	[number]	k4	2
k	k	k7c3	k
π	π	k?	π
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
textstyle	textstyl	k1gInSc5	textstyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
+2	+2	k4	+2
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
+2	+2	k4	+2
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Klesající	klesající	k2eAgInSc1d1	klesající
<g/>
:	:	kIx,	:
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
+	+	kIx~	+
2	[number]	k4	2
k	k	k7c3	k
π	π	k?	π
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
+	+	kIx~	+
2	[number]	k4	2
k	k	k7c3	k
π	π	k?	π
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
textstyle	textstyl	k1gInSc5	textstyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
+2	+2	k4	+2
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
+2	+2	k4	+2
<g/>
k	k	k7c3	k
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Maximum	maximum	k1gNnSc1	maximum
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
v	v	k7c6	v
bodech	bod	k1gInPc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
+	+	kIx~	+
2	[number]	k4	2
k	k	k7c3	k
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
textstyle	textstyl	k1gInSc5	textstyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
+2	+2	k4	+2
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
)	)	kIx)	)
Minimum	minimum	k1gNnSc1	minimum
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
-1	-1	k4	-1
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
v	v	k7c6	v
bodech	bod	k1gInPc6	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
+	+	kIx~	+
2	[number]	k4	2
k	k	k7c3	k
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
textstyle	textstyl	k1gInSc5	textstyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
+2	+2	k4	+2
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
)	)	kIx)	)
Derivace	derivace	k1gFnSc1	derivace
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
'	'	kIx"	'
:	:	kIx,	:
=	=	kIx~	=
cos	cos	kA	cos
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
'	'	kIx"	'
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Primitivní	primitivní	k2eAgFnPc1d1	primitivní
funkce	funkce	k1gFnPc1	funkce
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
−	−	k?	−
cos	cos	kA	cos
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
c	c	k0	c
,	,	kIx,	,
c	c	k0	c
∈	∈	k?	∈
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Taylorova	Taylorův	k2eAgFnSc1d1	Taylorova
řada	řada	k1gFnSc1	řada
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
x	x	k?	x
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
2	[number]	k4	2
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
)	)	kIx)	)
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
x-	x-	k?	x-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Inverzní	inverzní	k2eAgFnSc1d1	inverzní
funkce	funkce	k1gFnSc1	funkce
(	(	kIx(	(
<g/>
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
−	−	k?	−
1	[number]	k4	1
;	;	kIx,	;
1	[number]	k4	1
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
-1	-1	k4	-1
<g/>
;	;	kIx,	;
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
a	a	k8xC	a
oborem	obor	k1gInSc7	obor
hodnot	hodnota	k1gFnPc2	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⟨	⟨	k?	⟨
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
π	π	k?	π
;	;	kIx,	;
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
π	π	k?	π
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
langle	langla	k1gFnSc6	langla
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
;	;	kIx,	;
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
rangle	rangla	k1gFnSc6	rangla
}	}	kIx)	}
)	)	kIx)	)
<g/>
:	:	kIx,	:
arkus	arkus	k1gInSc1	arkus
sinus	sinus	k1gInSc1	sinus
(	(	kIx(	(
<g/>
arcsin	arcsin	k1gInSc1	arcsin
<g/>
)	)	kIx)	)
Sinus	sinus	k1gInSc1	sinus
dvojnásobného	dvojnásobný	k2eAgInSc2d1	dvojnásobný
argumentu	argument	k1gInSc2	argument
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
(	(	kIx(	(
2	[number]	k4	2
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
2	[number]	k4	2
sin	sin	kA	sin
:	:	kIx,	:
x	x	k?	x
cos	cos	kA	cos
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
x	x	k?	x
<g/>
}	}	kIx)	}
:	:	kIx,	:
Sinus	sinus	k1gInSc1	sinus
je	být	k5eAaImIp3nS	být
funkce	funkce	k1gFnSc1	funkce
<g/>
:	:	kIx,	:
lichá	lichý	k2eAgFnSc1d1	lichá
omezená	omezený	k2eAgFnSc1d1	omezená
shora	shora	k6eAd1	shora
i	i	k8xC	i
zdola	zdola	k6eAd1	zdola
periodická	periodický	k2eAgFnSc1d1	periodická
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
k	k	k7c3	k
π	π	k?	π
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
:	:	kIx,	:
Funkce	funkce	k1gFnSc1	funkce
sinus	sinus	k1gInSc1	sinus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
komplexních	komplexní	k2eAgNnPc6d1	komplexní
číslech	číslo	k1gNnPc6	číslo
definována	definovat	k5eAaBmNgFnS	definovat
součtem	součet	k1gInSc7	součet
řady	řada	k1gFnSc2	řada
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
z	z	k7c2	z
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
5	[number]	k4	5
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
7	[number]	k4	7
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
7	[number]	k4	7
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
2	[number]	k4	2
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
)	)	kIx)	)
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
z-	z-	k?	z-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
5	[number]	k4	5
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
z	z	k7c2	z
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
která	který	k3yRgFnSc1	který
konverguje	konvergovat	k5eAaImIp3nS	konvergovat
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každá	každý	k3xTgNnPc4	každý
komplexní	komplexní	k2eAgNnPc4d1	komplexní
čísla	číslo	k1gNnPc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
}	}	kIx)	}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
i	i	k9	i
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
−	−	k?	−
i	i	k9	i
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
i	i	k8xC	i
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
iz	iz	k?	iz
<g/>
}	}	kIx)	}
<g/>
-e	-e	k?	-e
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-iz	z	k?	-iz
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
i	i	k8xC	i
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
=	=	kIx~	=
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k1gInSc1	right
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
i	i	k9	i
z	z	k7c2	z
=	=	kIx~	=
i	i	k9	i
sinh	sinh	k1gInSc1	sinh
:	:	kIx,	:
z	z	k7c2	z
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sin	kA	sin
iz	iz	k?	iz
<g/>
=	=	kIx~	=
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
sinh	sinh	k1gInSc1	sinh
z.	z.	k?	z.
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tyto	tento	k3xDgInPc1	tento
vzorce	vzorec	k1gInPc1	vzorec
plynou	plynout	k5eAaImIp3nP	plynout
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
příslušných	příslušný	k2eAgFnPc2d1	příslušná
definičních	definiční	k2eAgFnPc2d1	definiční
mocninných	mocninný	k2eAgFnPc2d1	mocninná
řad	řada	k1gFnPc2	řada
daných	daný	k2eAgFnPc2d1	daná
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Sinus	sinus	k1gInSc1	sinus
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
komplexní	komplexní	k2eAgFnSc6d1	komplexní
rovině	rovina	k1gFnSc6	rovina
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
holomorfní	holomorfní	k2eAgFnSc1d1	holomorfní
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
