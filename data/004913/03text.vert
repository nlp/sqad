<s>
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
havárie	havárie	k1gFnSc1	havárie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
v	v	k7c6	v
Černobylské	černobylský	k2eAgFnSc6d1	Černobylská
jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
část	část	k1gFnSc1	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
jadernou	jaderný	k2eAgFnSc4d1	jaderná
havárii	havárie	k1gFnSc4	havárie
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
další	další	k2eAgFnSc7d1	další
jadernou	jaderný	k2eAgFnSc7d1	jaderná
havárií	havárie	k1gFnSc7	havárie
označenou	označený	k2eAgFnSc7d1	označená
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
stupnicí	stupnice	k1gFnSc7	stupnice
INES	INES	kA	INES
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
stupněm	stupeň	k1gInSc7	stupeň
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
havárie	havárie	k1gFnSc1	havárie
elektrárny	elektrárna	k1gFnSc2	elektrárna
Fukušima	Fukušimum	k1gNnSc2	Fukušimum
I	i	k9	i
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
reaktoru	reaktor	k1gInSc2	reaktor
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
přehřátí	přehřátí	k1gNnSc3	přehřátí
<g/>
,	,	kIx,	,
protavení	protavení	k1gNnSc3	protavení
a	a	k8xC	a
výbuchu	výbuch	k1gInSc3	výbuch
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
uvolnily	uvolnit	k5eAaPmAgFnP	uvolnit
radioaktivní	radioaktivní	k2eAgFnPc1d1	radioaktivní
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k9	tak
ke	k	k7c3	k
kontaminaci	kontaminace	k1gFnSc3	kontaminace
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
spad	spad	k1gInSc1	spad
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Uváděné	uváděný	k2eAgInPc1d1	uváděný
počty	počet	k1gInPc1	počet
obětí	oběť	k1gFnPc2	oběť
sahají	sahat	k5eAaImIp3nP	sahat
od	od	k7c2	od
31	[number]	k4	31
oficiálně	oficiálně	k6eAd1	oficiálně
zemřelých	zemřelý	k1gMnPc2	zemřelý
bezprostředně	bezprostředně	k6eAd1	bezprostředně
při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
až	až	k9	až
po	po	k7c4	po
odhady	odhad	k1gInPc4	odhad
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
úmrtí	úmrť	k1gFnPc2	úmrť
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
záření	záření	k1gNnSc2	záření
podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
bylo	být	k5eAaImAgNnS	být
okolí	okolí	k1gNnSc1	okolí
včetně	včetně	k7c2	včetně
města	město	k1gNnSc2	město
Prypjať	Prypjať	k1gFnSc2	Prypjať
evakuováno	evakuován	k2eAgNnSc4d1	evakuováno
a	a	k8xC	a
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
se	se	k3xPyFc4	se
v	v	k7c4	v
zakázanou	zakázaný	k2eAgFnSc4d1	zakázaná
zónu	zóna	k1gFnSc4	zóna
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
reaktoru	reaktor	k1gInSc2	reaktor
byla	být	k5eAaImAgFnS	být
provizorně	provizorně	k6eAd1	provizorně
obestavěna	obestavět	k5eAaPmNgFnS	obestavět
betonovým	betonový	k2eAgInSc7d1	betonový
sarkofágem	sarkofág	k1gInSc7	sarkofág
pro	pro	k7c4	pro
zamezení	zamezení	k1gNnSc4	zamezení
další	další	k2eAgFnSc2d1	další
kontaminace	kontaminace	k1gFnSc2	kontaminace
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Zajištění	zajištění	k1gNnSc1	zajištění
a	a	k8xC	a
případná	případný	k2eAgFnSc1d1	případná
likvidace	likvidace	k1gFnSc1	likvidace
stále	stále	k6eAd1	stále
nebezpečného	bezpečný	k2eNgInSc2d1	nebezpečný
reaktoru	reaktor	k1gInSc2	reaktor
je	být	k5eAaImIp3nS	být
časově	časově	k6eAd1	časově
<g/>
,	,	kIx,	,
technicky	technicky	k6eAd1	technicky
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
velice	velice	k6eAd1	velice
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Příčin	příčina	k1gFnPc2	příčina
havárie	havárie	k1gFnSc2	havárie
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
použitého	použitý	k2eAgInSc2d1	použitý
reaktoru	reaktor	k1gInSc2	reaktor
RBMK	RBMK	kA	RBMK
<g/>
,	,	kIx,	,
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
pokus	pokus	k1gInSc1	pokus
a	a	k8xC	a
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
kompetentnost	kompetentnost	k1gFnSc1	kompetentnost
personálu	personál	k1gInSc2	personál
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozsahu	rozsah	k1gInSc3	rozsah
následků	následek	k1gInPc2	následek
přispěly	přispět	k5eAaPmAgFnP	přispět
nepřipravenost	nepřipravenost	k1gFnSc4	nepřipravenost
a	a	k8xC	a
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
kroky	krok	k1gInPc4	krok
v	v	k7c6	v
krizovém	krizový	k2eAgInSc6d1	krizový
postupu	postup	k1gInSc6	postup
řešení	řešení	k1gNnSc2	řešení
následků	následek	k1gInPc2	následek
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
elektrárna	elektrárna	k1gFnSc1	elektrárna
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
Pripjať	Pripjať	k1gFnSc2	Pripjať
<g/>
,	,	kIx,	,
18	[number]	k4	18
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
Černobyl	Černobyl	k1gInSc1	Černobyl
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
a	a	k8xC	a
110	[number]	k4	110
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
950	[number]	k4	950
MW	MW	kA	MW
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
GW	GW	kA	GW
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
havárie	havárie	k1gFnSc2	havárie
dohromady	dohromady	k6eAd1	dohromady
produkovaly	produkovat	k5eAaImAgFnP	produkovat
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
reaktor	reaktor	k1gInSc1	reaktor
č.	č.	k?	č.
1	[number]	k4	1
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
následován	následován	k2eAgInSc1d1	následován
č.	č.	k?	č.
2	[number]	k4	2
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
č.	č.	k?	č.
3	[number]	k4	3
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
a	a	k8xC	a
č.	č.	k?	č.
4	[number]	k4	4
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
reaktory	reaktor	k1gInPc1	reaktor
(	(	kIx(	(
<g/>
č.	č.	k?	č.
5	[number]	k4	5
a	a	k8xC	a
č.	č.	k?	č.
6	[number]	k4	6
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
také	také	k9	také
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
950	[number]	k4	950
MW	MW	kA	MW
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
havárie	havárie	k1gFnSc2	havárie
rozestavěny	rozestavět	k5eAaPmNgFnP	rozestavět
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
čtyři	čtyři	k4xCgInPc1	čtyři
reaktory	reaktor	k1gInPc1	reaktor
byly	být	k5eAaImAgInP	být
typu	typa	k1gFnSc4	typa
RBMK-	RBMK-	k1gFnSc2	RBMK-
<g/>
1000	[number]	k4	1000
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
chlazené	chlazený	k2eAgNnSc1d1	chlazené
obyčejnou	obyčejný	k2eAgFnSc7d1	obyčejná
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
moderované	moderovaný	k2eAgInPc1d1	moderovaný
grafitem	grafit	k1gInSc7	grafit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
během	během	k7c2	během
riskantního	riskantní	k2eAgInSc2d1	riskantní
pokusu	pokus	k1gInSc2	pokus
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
reaktoru	reaktor	k1gInSc6	reaktor
Černobylu	Černobyl	k1gInSc2	Černobyl
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přehřátí	přehřátí	k1gNnSc3	přehřátí
a	a	k8xC	a
následné	následný	k2eAgFnSc3d1	následná
explozi	exploze	k1gFnSc3	exploze
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Exploze	exploze	k1gFnSc1	exploze
v	v	k7c6	v
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
58	[number]	k4	58
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odtrhla	odtrhnout	k5eAaPmAgFnS	odtrhnout
víko	víko	k1gNnSc4	víko
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
požár	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
sérii	série	k1gFnSc4	série
dalších	další	k2eAgFnPc2d1	další
explozí	exploze	k1gFnPc2	exploze
a	a	k8xC	a
roztavení	roztavení	k1gNnSc2	roztavení
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
bezpečnostní	bezpečnostní	k2eAgInPc1d1	bezpečnostní
systémy	systém	k1gInPc1	systém
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
konstrukcí	konstrukce	k1gFnPc2	konstrukce
nesoucích	nesoucí	k2eAgFnPc2d1	nesoucí
jádro	jádro	k1gNnSc4	jádro
reaktoru	reaktor	k1gInSc2	reaktor
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
nebo	nebo	k8xC	nebo
těžce	těžce	k6eAd1	těžce
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
reaktoru	reaktor	k1gInSc2	reaktor
bylo	být	k5eAaImAgNnS	být
kontaminováno	kontaminovat	k5eAaBmNgNnS	kontaminovat
aktivními	aktivní	k2eAgInPc7d1	aktivní
úlomky	úlomek	k1gInPc7	úlomek
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
kusy	kus	k1gInPc4	kus
palivových	palivový	k2eAgFnPc2d1	palivová
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Naměřené	naměřený	k2eAgFnPc1d1	naměřená
dávky	dávka	k1gFnPc1	dávka
záření	záření	k1gNnSc2	záření
gama	gama	k1gNnSc2	gama
byly	být	k5eAaImAgInP	být
odstrašující	odstrašující	k2eAgInPc1d1	odstrašující
–	–	k?	–
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
překročily	překročit	k5eAaPmAgInP	překročit
hodnotu	hodnota	k1gFnSc4	hodnota
5	[number]	k4	5
000	[number]	k4	000
R	R	kA	R
(	(	kIx(	(
<g/>
rentgenů	rentgen	k1gInPc2	rentgen
<g/>
)	)	kIx)	)
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
bloku	blok	k1gInSc2	blok
číslo	číslo	k1gNnSc4	číslo
4	[number]	k4	4
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
hodnot	hodnota	k1gFnPc2	hodnota
2	[number]	k4	2
000	[number]	k4	000
R	R	kA	R
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
mrak	mrak	k1gInSc1	mrak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
postupoval	postupovat	k5eAaImAgInS	postupovat
západní	západní	k2eAgFnSc7d1	západní
částí	část	k1gFnSc7	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnSc7d1	východní
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
Skandinávií	Skandinávie	k1gFnSc7	Skandinávie
<g/>
,	,	kIx,	,
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
těžce	těžce	k6eAd1	těžce
kontaminovány	kontaminovat	k5eAaBmNgFnP	kontaminovat
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
oblasti	oblast	k1gFnPc1	oblast
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
evakuaci	evakuace	k1gFnSc4	evakuace
a	a	k8xC	a
přesídlení	přesídlení	k1gNnSc4	přesídlení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
350	[number]	k4	350
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
spadu	spad	k1gInSc2	spad
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Nehoda	nehoda	k1gFnSc1	nehoda
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
obavy	obava	k1gFnPc4	obava
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
sovětského	sovětský	k2eAgInSc2d1	sovětský
jaderného	jaderný	k2eAgInSc2d1	jaderný
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zpomalila	zpomalit	k5eAaPmAgFnS	zpomalit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
jeho	jeho	k3xOp3gFnSc3	jeho
expanzi	expanze	k1gFnSc3	expanze
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nutila	nutit	k5eAaImAgFnS	nutit
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
vládu	vláda	k1gFnSc4	vláda
přehodnotit	přehodnotit	k5eAaPmF	přehodnotit
míru	míra	k1gFnSc4	míra
utajování	utajování	k1gNnSc2	utajování
<g/>
.	.	kIx.	.
</s>
<s>
Nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
státy	stát	k1gInPc1	stát
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
–	–	k?	–
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
dodnes	dodnes	k6eAd1	dodnes
nesou	nést	k5eAaImIp3nP	nést
břímě	břímě	k1gNnSc4	břímě
pokračujících	pokračující	k2eAgInPc2d1	pokračující
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
dekontaminaci	dekontaminace	k1gFnSc4	dekontaminace
a	a	k8xC	a
léčení	léčení	k1gNnSc4	léčení
nemocí	nemoc	k1gFnPc2	nemoc
způsobených	způsobený	k2eAgFnPc2d1	způsobená
černobylskou	černobylský	k2eAgFnSc7d1	Černobylská
havárií	havárie	k1gFnSc7	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
přesně	přesně	k6eAd1	přesně
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
počet	počet	k1gInSc4	počet
úmrtí	úmrtí	k1gNnPc2	úmrtí
způsobených	způsobený	k2eAgNnPc2d1	způsobené
událostmi	událost	k1gFnPc7	událost
v	v	k7c6	v
Černobylu	Černobyl	k1gInSc6	Černobyl
–	–	k?	–
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
sto	sto	k4xCgNnSc4	sto
až	až	k8xS	až
sto	sto	k4xCgNnSc4	sto
padesáti	padesát	k4xCc2	padesát
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
po	po	k7c6	po
bezmála	bezmála	k6eAd1	bezmála
milion	milion	k4xCgInSc1	milion
<g/>
.	.	kIx.	.
</s>
<s>
Havárie	havárie	k1gFnSc1	havárie
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
dopady	dopad	k1gInPc1	dopad
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Katastrofa	katastrofa	k1gFnSc1	katastrofa
je	být	k5eAaImIp3nS	být
přisuzována	přisuzovat	k5eAaImNgFnS	přisuzovat
špatné	špatný	k2eAgFnSc3d1	špatná
konstrukci	konstrukce	k1gFnSc3	konstrukce
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc3	jeho
kontraintuitivním	kontraintuitivní	k2eAgFnPc3d1	kontraintuitivní
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
,	,	kIx,	,
nedodržení	nedodržení	k1gNnSc4	nedodržení
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
byl	být	k5eAaImAgInS	být
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
pokus	pokus	k1gInSc1	pokus
připraven	připravit	k5eAaPmNgInS	připravit
<g/>
,	,	kIx,	,
a	a	k8xC	a
obecnému	obecný	k2eAgInSc3d1	obecný
nedostatku	nedostatek	k1gInSc3	nedostatek
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
Three	Three	k1gFnPc6	Three
Mile	mile	k6eAd1	mile
Island	Island	k1gInSc1	Island
byl	být	k5eAaImAgInS	být
druhotným	druhotný	k2eAgInSc7d1	druhotný
faktorem	faktor	k1gInSc7	faktor
přispívajícím	přispívající	k2eAgInSc7d1	přispívající
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektrárenští	elektrárenský	k2eAgMnPc1d1	elektrárenský
operátoři	operátor	k1gMnPc1	operátor
nebyli	být	k5eNaImAgMnP	být
dostatečně	dostatečně	k6eAd1	dostatečně
vyškoleni	vyškolen	k2eAgMnPc1d1	vyškolen
a	a	k8xC	a
obeznámeni	obeznámen	k2eAgMnPc1d1	obeznámen
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
charakteristikami	charakteristika	k1gFnPc7	charakteristika
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
problémů	problém	k1gInPc2	problém
byla	být	k5eAaImAgFnS	být
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
komunikace	komunikace	k1gFnSc1	komunikace
mezi	mezi	k7c7	mezi
vedoucími	vedoucí	k2eAgMnPc7d1	vedoucí
bezpečnostními	bezpečnostní	k2eAgMnPc7d1	bezpečnostní
pracovníky	pracovník	k1gMnPc7	pracovník
a	a	k8xC	a
operátory	operátor	k1gMnPc7	operátor
ohledně	ohledně	k7c2	ohledně
příkazu	příkaz	k1gInSc2	příkaz
vykonat	vykonat	k5eAaPmF	vykonat
noční	noční	k2eAgInSc4d1	noční
experiment	experiment	k1gInSc4	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
kvůli	kvůli	k7c3	kvůli
nedostatečnému	dostatečný	k2eNgNnSc3d1	nedostatečné
proškolení	proškolení	k1gNnSc3	proškolení
operátoři	operátor	k1gMnPc1	operátor
dostatečně	dostatečně	k6eAd1	dostatečně
nechápali	chápat	k5eNaImAgMnP	chápat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
reaktor	reaktor	k1gInSc1	reaktor
pracuje	pracovat	k5eAaImIp3nS	pracovat
při	při	k7c6	při
nízkém	nízký	k2eAgInSc6d1	nízký
stupni	stupeň	k1gInSc6	stupeň
reaktivity	reaktivita	k1gFnSc2	reaktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
podmínkami	podmínka	k1gFnPc7	podmínka
experimentu	experiment	k1gInSc2	experiment
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
systémů	systém	k1gInPc2	systém
vyřazeno	vyřazen	k2eAgNnSc1d1	vyřazeno
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Experiment	experiment	k1gInSc1	experiment
totiž	totiž	k9	totiž
měl	mít	k5eAaImAgInS	mít
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
bude	být	k5eAaImBp3nS	být
elektrický	elektrický	k2eAgInSc1d1	elektrický
generátor	generátor	k1gInSc1	generátor
(	(	kIx(	(
<g/>
poháněný	poháněný	k2eAgInSc1d1	poháněný
parní	parní	k2eAgInSc1d1	parní
turbínou	turbína	k1gFnSc7	turbína
<g/>
)	)	kIx)	)
po	po	k7c6	po
rychlém	rychlý	k2eAgNnSc6d1	rychlé
uzavření	uzavření	k1gNnSc6	uzavření
přívodu	přívod	k1gInSc6	přívod
páry	pára	k1gFnPc1	pára
do	do	k7c2	do
turbíny	turbína	k1gFnSc2	turbína
schopen	schopen	k2eAgInSc1d1	schopen
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
setrvačném	setrvačný	k2eAgInSc6d1	setrvačný
doběhu	doběh	k1gInSc6	doběh
ještě	ještě	k6eAd1	ještě
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
sekund	sekunda	k1gFnPc2	sekunda
napájet	napájet	k5eAaImF	napájet
čerpadla	čerpadlo	k1gNnPc4	čerpadlo
havarijního	havarijní	k2eAgNnSc2d1	havarijní
chlazení	chlazení	k1gNnSc2	chlazení
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
technických	technický	k2eAgInPc2d1	technický
rysů	rys	k1gInPc2	rys
reaktoru	reaktor	k1gInSc2	reaktor
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vojenská	vojenský	k2eAgNnPc4d1	vojenské
tajemství	tajemství	k1gNnSc4	tajemství
a	a	k8xC	a
operátoři	operátor	k1gMnPc1	operátor
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
neměli	mít	k5eNaImAgMnP	mít
ponětí	ponětí	k1gNnSc3	ponětí
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
měl	mít	k5eAaImAgInS	mít
především	především	k9	především
nebezpečně	bezpečně	k6eNd1	bezpečně
velký	velký	k2eAgInSc1d1	velký
kladný	kladný	k2eAgInSc1d1	kladný
dutinový	dutinový	k2eAgInSc1d1	dutinový
koeficient	koeficient	k1gInSc1	koeficient
reaktivity	reaktivita	k1gFnSc2	reaktivita
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc7d1	významná
vadou	vada	k1gFnSc7	vada
reaktoru	reaktor	k1gInSc2	reaktor
byla	být	k5eAaImAgFnS	být
také	také	k9	také
konstrukce	konstrukce	k1gFnSc1	konstrukce
jeho	jeho	k3xOp3gFnPc2	jeho
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Regulační	regulační	k2eAgFnPc1d1	regulační
tyče	tyč	k1gFnPc1	tyč
nebyly	být	k5eNaImAgFnP	být
zcela	zcela	k6eAd1	zcela
naplněné	naplněný	k2eAgFnPc1d1	naplněná
<g/>
;	;	kIx,	;
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zasouvaly	zasouvat	k5eAaImAgInP	zasouvat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
na	na	k7c4	na
prvních	první	k4xOgInPc2	první
pár	pár	k4xCyI	pár
sekund	sekunda	k1gFnPc2	sekunda
chladicí	chladicí	k2eAgFnSc1d1	chladicí
kapalina	kapalina	k1gFnSc1	kapalina
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
dutými	dutý	k2eAgFnPc7d1	dutá
částmi	část	k1gFnPc7	část
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
chladicí	chladicí	k2eAgFnSc1d1	chladicí
kapalina	kapalina	k1gFnSc1	kapalina
(	(	kIx(	(
<g/>
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pohlcovač	pohlcovač	k1gInSc1	pohlcovač
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
reaktoru	reaktor	k1gInSc2	reaktor
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
neintuitivní	intuitivní	k2eNgNnSc1d1	neintuitivní
chování	chování	k1gNnSc1	chování
reaktoru	reaktor	k1gInSc2	reaktor
při	při	k7c6	při
zasouvání	zasouvání	k1gNnSc6	zasouvání
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
nebylo	být	k5eNaImAgNnS	být
operátorům	operátor	k1gInPc3	operátor
vůbec	vůbec	k9	vůbec
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
chyba	chyba	k1gFnSc1	chyba
se	se	k3xPyFc4	se
přece	přece	k9	přece
jen	jen	k9	jen
obecně	obecně	k6eAd1	obecně
připisuje	připisovat	k5eAaImIp3nS	připisovat
zástupci	zástupce	k1gMnPc1	zástupce
hlavního	hlavní	k2eAgMnSc2d1	hlavní
inženýra	inženýr	k1gMnSc2	inženýr
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Černobylu	Černobyl	k1gInSc6	Černobyl
Anatoliji	Anatoliji	k1gMnSc3	Anatoliji
Stěpaňoviči	Stěpaňovič	k1gMnSc3	Stěpaňovič
Ďatlovovi	Ďatlova	k1gMnSc3	Ďatlova
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nadřízeným	nadřízený	k1gMnSc7	nadřízený
operátorů	operátor	k1gMnPc2	operátor
v	v	k7c6	v
řídícím	řídící	k2eAgInSc6d1	řídící
centru	centr	k1gInSc6	centr
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Operátoři	operátor	k1gMnPc1	operátor
mu	on	k3xPp3gInSc3	on
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
sami	sám	k3xTgMnPc1	sám
odporovali	odporovat	k5eAaImAgMnP	odporovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
dával	dávat	k5eAaImAgInS	dávat
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
mnohým	mnohý	k2eAgNnPc3d1	mnohé
rizikům	riziko	k1gNnPc3	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3	nejznámější
Ďatlovovy	Ďatlovův	k2eAgInPc1d1	Ďatlovův
příkazy	příkaz	k1gInPc1	příkaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
<g/>
:	:	kIx,	:
Přikázal	přikázat	k5eAaPmAgMnS	přikázat
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
zkoušku	zkouška	k1gFnSc4	zkouška
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
nízkém	nízký	k2eAgInSc6d1	nízký
výkonu	výkon	k1gInSc6	výkon
<g/>
,	,	kIx,	,
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
limitem	limit	k1gInSc7	limit
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
700	[number]	k4	700
MW	MW	kA	MW
<g/>
)	)	kIx)	)
jen	jen	k9	jen
na	na	k7c4	na
200	[number]	k4	200
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velmi	velmi	k6eAd1	velmi
nestabilním	stabilní	k2eNgNnSc7d1	nestabilní
<g/>
.	.	kIx.	.
</s>
<s>
Operátoři	operátor	k1gMnPc1	operátor
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
a	a	k8xC	a
odporovali	odporovat	k5eAaImAgMnP	odporovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proti	proti	k7c3	proti
předpisům	předpis	k1gInPc3	předpis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedal	dát	k5eNaPmAgMnS	dát
si	se	k3xPyFc3	se
říct	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
snižování	snižování	k1gNnSc6	snižování
výkonu	výkon	k1gInSc2	výkon
reaktoru	reaktor	k1gInSc2	reaktor
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stalo	stát	k5eAaPmAgNnS	stát
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výkon	výkon	k1gInSc1	výkon
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
0	[number]	k4	0
<g/>
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
byl	být	k5eAaImAgInS	být
zastaven	zastavit	k5eAaPmNgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Ďatlov	Ďatlov	k1gInSc1	Ďatlov
přikázal	přikázat	k5eAaPmAgInS	přikázat
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
regulační	regulační	k2eAgFnPc4d1	regulační
tyče	tyč	k1gFnPc4	tyč
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
regulovaly	regulovat	k5eAaImAgFnP	regulovat
štěpnou	štěpný	k2eAgFnSc4d1	štěpná
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
přece	přece	k9	přece
zkoušku	zkouška	k1gFnSc4	zkouška
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
na	na	k7c4	na
200	[number]	k4	200
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
úplném	úplný	k2eAgNnSc6d1	úplné
vytažení	vytažení	k1gNnSc6	vytažení
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
výkon	výkon	k1gInSc1	výkon
reaktoru	reaktor	k1gInSc2	reaktor
stoupal	stoupat	k5eAaImAgInS	stoupat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyly	být	k5eNaImAgFnP	být
vytaženy	vytažen	k2eAgFnPc1d1	vytažena
úplně	úplně	k6eAd1	úplně
všechny	všechen	k3xTgFnPc4	všechen
tyče	tyč	k1gFnPc4	tyč
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
částečně	částečně	k6eAd1	částečně
vysunutých	vysunutý	k2eAgFnPc6d1	vysunutá
regulačních	regulační	k2eAgFnPc6d1	regulační
tyčích	tyč	k1gFnPc6	tyč
vznikala	vznikat	k5eAaImAgFnS	vznikat
tzv.	tzv.	kA	tzv.
žhavá	žhavý	k2eAgNnPc4d1	žhavé
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Tyčí	tyč	k1gFnPc2	tyč
bylo	být	k5eAaImAgNnS	být
méně	málo	k6eAd2	málo
a	a	k8xC	a
proto	proto	k8xC	proto
nestačily	stačit	k5eNaBmAgInP	stačit
regulovat	regulovat	k5eAaImF	regulovat
štěpnou	štěpný	k2eAgFnSc4d1	štěpná
reakci	reakce	k1gFnSc4	reakce
dostatečně	dostatečně	k6eAd1	dostatečně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
palivových	palivový	k2eAgFnPc2d1	palivová
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhala	probíhat	k5eAaImAgFnS	probíhat
štěpná	štěpný	k2eAgFnSc1d1	štěpná
reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
šířilo	šířit	k5eAaImAgNnS	šířit
teplo	teplo	k1gNnSc1	teplo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tyto	tento	k3xDgFnPc4	tento
částečně	částečně	k6eAd1	částečně
vysunuté	vysunutý	k2eAgFnSc2d1	vysunutá
tyče	tyč	k1gFnSc2	tyč
ohřívalo	ohřívat	k5eAaImAgNnS	ohřívat
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
byly	být	k5eAaImAgFnP	být
aktivovány	aktivován	k2eAgFnPc4d1	aktivována
turbíny	turbína	k1gFnPc4	turbína
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dávaly	dávat	k5eAaImAgFnP	dávat
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
čerpala	čerpat	k5eAaImAgFnS	čerpat
vodu	voda	k1gFnSc4	voda
na	na	k7c4	na
chlazení	chlazení	k1gNnSc4	chlazení
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Dostali	dostat	k5eAaPmAgMnP	dostat
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
výrazné	výrazný	k2eAgFnSc3d1	výrazná
chybě	chyba	k1gFnSc3	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
zkoušky	zkouška	k1gFnSc2	zkouška
bylo	být	k5eAaImAgNnS	být
přece	přece	k9	přece
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
za	za	k7c4	za
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
při	při	k7c6	při
vypnutí	vypnutí	k1gNnSc6	vypnutí
turbín	turbína	k1gFnPc2	turbína
pohánějících	pohánějící	k2eAgFnPc2d1	pohánějící
vodní	vodní	k2eAgMnPc1d1	vodní
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
ještě	ještě	k6eAd1	ještě
pracovala	pracovat	k5eAaImAgFnS	pracovat
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
<g/>
,	,	kIx,	,
zapnuly	zapnout	k5eAaPmAgInP	zapnout
záložní	záložní	k2eAgInPc1d1	záložní
zdroje	zdroj	k1gInPc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
poháněly	pohánět	k5eAaImAgFnP	pohánět
turbíny	turbína	k1gFnPc4	turbína
<g/>
,	,	kIx,	,
a	a	k8xC	a
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
účinné	účinný	k2eAgNnSc1d1	účinné
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
sled	sled	k1gInSc1	sled
hrozných	hrozný	k2eAgFnPc2d1	hrozná
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
turbíny	turbína	k1gFnPc1	turbína
byly	být	k5eAaImAgFnP	být
vypnuty	vypnout	k5eAaPmNgFnP	vypnout
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
setrvačnost	setrvačnost	k1gFnSc1	setrvačnost
byla	být	k5eAaImAgFnS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
čerpána	čerpán	k2eAgFnSc1d1	čerpána
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
reaktor	reaktor	k1gInSc1	reaktor
nebyl	být	k5eNaImAgInS	být
dostatečně	dostatečně	k6eAd1	dostatečně
chlazen	chladit	k5eAaImNgInS	chladit
<g/>
.	.	kIx.	.
</s>
<s>
Záložní	záložní	k2eAgInPc1d1	záložní
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
daly	dát	k5eAaPmAgInP	dát
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
čerpadla	čerpadlo	k1gNnSc2	čerpadlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nespustily	spustit	k5eNaPmAgFnP	spustit
hned	hned	k6eAd1	hned
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
toto	tento	k3xDgNnSc1	tento
riziko	riziko	k1gNnSc1	riziko
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Ďatlova	Ďatlův	k2eAgMnSc4d1	Ďatlův
jen	jen	k6eAd1	jen
teoretické	teoretický	k2eAgInPc1d1	teoretický
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
stoupaly	stoupat	k5eAaImAgFnP	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
žhavých	žhavý	k2eAgNnPc6d1	žhavé
místech	místo	k1gNnPc6	místo
vznikalo	vznikat	k5eAaImAgNnS	vznikat
mnoho	mnoho	k6eAd1	mnoho
páry	pára	k1gFnSc2	pára
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
tlak	tlak	k1gInSc1	tlak
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
současně	současně	k6eAd1	současně
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
výkonem	výkon	k1gInSc7	výkon
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
páry	pára	k1gFnSc2	pára
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
víku	víko	k1gNnSc6	víko
reaktoru	reaktor	k1gInSc2	reaktor
nadzvedl	nadzvednout	k5eAaPmAgMnS	nadzvednout
350	[number]	k4	350
<g/>
kilové	kilový	k2eAgFnSc2d1	kilová
uzávěry	uzávěra	k1gFnSc2	uzávěra
palivových	palivový	k2eAgFnPc2d1	palivová
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Operátoři	operátor	k1gMnPc1	operátor
si	se	k3xPyFc3	se
prudkého	prudký	k2eAgInSc2d1	prudký
růstu	růst	k1gInSc2	růst
výkonu	výkon	k1gInSc2	výkon
samozřejmě	samozřejmě	k6eAd1	samozřejmě
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
.	.	kIx.	.
</s>
<s>
Podnikli	podniknout	k5eAaPmAgMnP	podniknout
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
celou	celý	k2eAgFnSc4d1	celá
katastrofu	katastrofa	k1gFnSc4	katastrofa
jen	jen	k9	jen
urychlilo	urychlit	k5eAaPmAgNnS	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Dali	dát	k5eAaPmAgMnP	dát
zcela	zcela	k6eAd1	zcela
spustit	spustit	k5eAaPmF	spustit
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
reaktoru	reaktor	k1gInSc2	reaktor
borové	borový	k2eAgFnSc2d1	Borová
regulační	regulační	k2eAgFnSc2d1	regulační
tyče	tyč	k1gFnSc2	tyč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
omezily	omezit	k5eAaPmAgFnP	omezit
štěpnou	štěpný	k2eAgFnSc4d1	štěpná
reakci	reakce	k1gFnSc4	reakce
a	a	k8xC	a
snížily	snížit	k5eAaPmAgFnP	snížit
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Operátoři	operátor	k1gMnPc1	operátor
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
mohli	moct	k5eAaImAgMnP	moct
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
Ďatlov	Ďatlov	k1gInSc4	Ďatlov
nebyli	být	k5eNaImAgMnP	být
informováni	informovat	k5eAaBmNgMnP	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
regulační	regulační	k2eAgFnPc1d1	regulační
tyče	tyč	k1gFnPc1	tyč
měly	mít	k5eAaImAgFnP	mít
špičku	špička	k1gFnSc4	špička
z	z	k7c2	z
grafitu	grafit	k1gInSc2	grafit
<g/>
.	.	kIx.	.
</s>
<s>
Grafit	grafit	k1gInSc1	grafit
způsobil	způsobit	k5eAaPmAgInS	způsobit
nikoli	nikoli	k9	nikoli
snížení	snížení	k1gNnSc4	snížení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
prudké	prudký	k2eAgNnSc4d1	prudké
zvýšení	zvýšení	k1gNnSc4	zvýšení
výkonu	výkon	k1gInSc2	výkon
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
tyče	tyč	k1gFnPc1	tyč
nebyly	být	k5eNaImAgFnP	být
vytaženy	vytáhnout	k5eAaPmNgFnP	vytáhnout
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
nevzrostl	vzrůst	k5eNaPmAgMnS	vzrůst
a	a	k8xC	a
šance	šance	k1gFnSc1	šance
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
regulování	regulování	k1gNnSc6	regulování
by	by	kYmCp3nS	by
tu	tu	k6eAd1	tu
stále	stále	k6eAd1	stále
byla	být	k5eAaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
páry	pára	k1gFnSc2	pára
a	a	k8xC	a
teplotu	teplota	k1gFnSc4	teplota
ve	v	k7c6	v
žhavých	žhavý	k2eAgNnPc6d1	žhavé
místech	místo	k1gNnPc6	místo
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zvládnout	zvládnout	k5eAaPmF	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Operátoři	operátor	k1gMnPc1	operátor
mohli	moct	k5eAaImAgMnP	moct
na	na	k7c6	na
řídícím	řídící	k2eAgInSc6d1	řídící
panelu	panel	k1gInSc6	panel
jen	jen	k9	jen
bezmocně	bezmocně	k6eAd1	bezmocně
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
výkon	výkon	k1gInSc1	výkon
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
celé	celý	k2eAgNnSc1d1	celé
víko	víko	k1gNnSc1	víko
reaktoru	reaktor	k1gInSc2	reaktor
odletělo	odletět	k5eAaPmAgNnS	odletět
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
vysoký	vysoký	k2eAgInSc4d1	vysoký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
způsobil	způsobit	k5eAaPmAgInS	způsobit
jeho	jeho	k3xOp3gInSc1	jeho
výbuch	výbuch	k1gInSc1	výbuch
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podrobné	podrobný	k2eAgFnPc1d1	podrobná
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
před	před	k7c7	před
havárií	havárie	k1gFnSc7	havárie
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Nadpis-Průběh	Nadpis-Průběh	k1gInSc1	Nadpis-Průběh
událostí	událost	k1gFnPc2	událost
<g/>
)	)	kIx)	)
Výbuch	výbuch	k1gInSc1	výbuch
operátory	operátor	k1gMnPc7	operátor
přímo	přímo	k6eAd1	přímo
nezasáhl	zasáhnout	k5eNaPmAgMnS	zasáhnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ho	on	k3xPp3gMnSc4	on
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tlak	tlak	k1gInSc4	tlak
výbuchu	výbuch	k1gInSc2	výbuch
odtrhl	odtrhnout	k5eAaPmAgMnS	odtrhnout
dveře	dveře	k1gFnPc4	dveře
do	do	k7c2	do
řídicího	řídicí	k2eAgNnSc2d1	řídicí
střediska	středisko	k1gNnSc2	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
se	se	k3xPyFc4	se
šířilo	šířit	k5eAaImAgNnS	šířit
radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Ďatlov	Ďatlov	k1gInSc1	Ďatlov
nebyl	být	k5eNaImAgInS	být
nezkušený	zkušený	k2eNgMnSc1d1	nezkušený
<g/>
.	.	kIx.	.
</s>
<s>
Myslel	myslet	k5eAaImAgMnS	myslet
si	se	k3xPyFc3	se
jen	jen	k6eAd1	jen
<g/>
,	,	kIx,	,
že	že	k8xS	že
technologii	technologie	k1gFnSc4	technologie
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
reaktoru	reaktor	k1gInSc2	reaktor
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
podmanit	podmanit	k5eAaPmF	podmanit
<g/>
.	.	kIx.	.
</s>
<s>
Myslel	myslet	k5eAaImAgInS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc4	něco
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
z	z	k7c2	z
operátorů	operátor	k1gMnPc2	operátor
přežil	přežít	k5eAaPmAgMnS	přežít
nejdéle	dlouho	k6eAd3	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
ale	ale	k9	ale
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
nemůže	moct	k5eNaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Prý	prý	k9	prý
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
mohla	moct	k5eAaImAgFnS	moct
chybná	chybný	k2eAgFnSc1d1	chybná
konstrukce	konstrukce	k1gFnSc1	konstrukce
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc4	on
neměli	mít	k5eNaImAgMnP	mít
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
reaktor	reaktor	k1gInSc4	reaktor
dát	dát	k5eAaPmF	dát
vůbec	vůbec	k9	vůbec
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
odstavení	odstavení	k1gNnSc1	odstavení
reaktoru	reaktor	k1gInSc2	reaktor
číslo	číslo	k1gNnSc1	číslo
4	[number]	k4	4
pro	pro	k7c4	pro
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
údržbu	údržba	k1gFnSc4	údržba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
využít	využít	k5eAaPmF	využít
této	tento	k3xDgFnSc3	tento
příležitosti	příležitost	k1gFnSc3	příležitost
k	k	k7c3	k
otestování	otestování	k1gNnSc3	otestování
schopnosti	schopnost	k1gFnSc2	schopnost
turbínového	turbínový	k2eAgInSc2d1	turbínový
generátoru	generátor	k1gInSc2	generátor
reaktoru	reaktor	k1gInSc2	reaktor
vyrábět	vyrábět	k5eAaImF	vyrábět
patřičné	patřičný	k2eAgNnSc4d1	patřičné
množství	množství	k1gNnSc4	množství
elektřiny	elektřina	k1gFnSc2	elektřina
k	k	k7c3	k
napájení	napájení	k1gNnSc3	napájení
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
systémů	systém	k1gInPc2	systém
reaktoru	reaktor	k1gInSc2	reaktor
(	(	kIx(	(
<g/>
především	především	k9	především
vodních	vodní	k2eAgFnPc2d1	vodní
pump	pumpa	k1gFnPc2	pumpa
<g/>
)	)	kIx)	)
v	v	k7c6	v
případě	případ	k1gInSc6	případ
současného	současný	k2eAgInSc2d1	současný
výpadku	výpadek	k1gInSc2	výpadek
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
i	i	k8xC	i
vnějších	vnější	k2eAgInPc2d1	vnější
zdrojů	zdroj	k1gInPc2	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktér	konstruktér	k1gMnSc1	konstruktér
elektrárny	elektrárna	k1gFnSc2	elektrárna
počítal	počítat	k5eAaImAgMnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
roztočená	roztočený	k2eAgFnSc1d1	roztočená
turbína	turbína	k1gFnSc1	turbína
poskytnout	poskytnout	k5eAaPmF	poskytnout
dostatek	dostatek	k1gInSc4	dostatek
energie	energie	k1gFnSc2	energie
nutné	nutný	k2eAgFnPc1d1	nutná
pro	pro	k7c4	pro
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
odstavení	odstavení	k1gNnSc4	odstavení
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyzkoušení	vyzkoušení	k1gNnSc3	vyzkoušení
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
skutečně	skutečně	k6eAd1	skutečně
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
původně	původně	k6eAd1	původně
dojít	dojít	k5eAaPmF	dojít
ještě	ještě	k9	ještě
před	před	k7c7	před
spuštěním	spuštění	k1gNnSc7	spuštění
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
politický	politický	k2eAgInSc1d1	politický
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
rychlé	rychlý	k2eAgNnSc4d1	rychlé
uvedení	uvedení	k1gNnSc4	uvedení
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c4	v
činnost	činnost	k1gFnSc4	činnost
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
řada	řada	k1gFnSc1	řada
provozních	provozní	k2eAgInPc2d1	provozní
testů	test	k1gInPc2	test
odložena	odložen	k2eAgFnSc1d1	odložena
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
experimentu	experiment	k1gInSc2	experiment
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
reaktor	reaktor	k1gInSc1	reaktor
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
roztočení	roztočení	k1gNnSc3	roztočení
turbíny	turbína	k1gFnSc2	turbína
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
turbína	turbína	k1gFnSc1	turbína
od	od	k7c2	od
reaktoru	reaktor	k1gInSc2	reaktor
odpojena	odpojen	k2eAgFnSc1d1	odpojena
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
točit	točit	k5eAaImF	točit
jen	jen	k9	jen
vlastní	vlastní	k2eAgFnSc7d1	vlastní
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
<g/>
.	.	kIx.	.
</s>
<s>
Výstupní	výstupní	k2eAgInSc1d1	výstupní
výkon	výkon	k1gInSc1	výkon
reaktoru	reaktor	k1gInSc2	reaktor
byl	být	k5eAaImAgInS	být
snížen	snížit	k5eAaPmNgInS	snížit
z	z	k7c2	z
normálního	normální	k2eAgInSc2d1	normální
výkonu	výkon	k1gInSc2	výkon
3,2	[number]	k4	3,2
GW	GW	kA	GW
na	na	k7c4	na
700	[number]	k4	700
MW	MW	kA	MW
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
test	test	k1gInSc1	test
probíhal	probíhat	k5eAaImAgInS	probíhat
při	při	k7c6	při
bezpečnějším	bezpečný	k2eAgMnSc6d2	bezpečnější
<g/>
,	,	kIx,	,
nízkém	nízký	k2eAgInSc6d1	nízký
výkonu	výkon	k1gInSc6	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Snižování	snižování	k1gNnSc1	snižování
výkonu	výkon	k1gInSc2	výkon
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
v	v	k7c4	v
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
výkon	výkon	k1gInSc1	výkon
reaktoru	reaktor	k1gInSc2	reaktor
snížen	snížit	k5eAaPmNgInS	snížit
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odstavení	odstavení	k1gNnSc3	odstavení
prvního	první	k4xOgInSc2	první
turbogenerátoru	turbogenerátor	k1gInSc2	turbogenerátor
<g/>
,	,	kIx,	,
nato	nato	k6eAd1	nato
byl	být	k5eAaImAgInS	být
odpojen	odpojit	k5eAaPmNgInS	odpojit
i	i	k9	i
systém	systém	k1gInSc1	systém
havarijního	havarijní	k2eAgNnSc2d1	havarijní
chlazení	chlazení	k1gNnSc2	chlazení
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
však	však	k9	však
dispečink	dispečink	k1gInSc1	dispečink
energetických	energetický	k2eAgInPc2d1	energetický
závodů	závod	k1gInPc2	závod
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
udržení	udržení	k1gNnSc6	udržení
stávajícího	stávající	k2eAgInSc2d1	stávající
výkonu	výkon	k1gInSc2	výkon
bez	bez	k7c2	bez
dalšího	další	k2eAgNnSc2d1	další
snižování	snižování	k1gNnSc2	snižování
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
továrnách	továrna	k1gFnPc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Experiment	experiment	k1gInSc1	experiment
byl	být	k5eAaImAgInS	být
tudíž	tudíž	k8xC	tudíž
pozastaven	pozastavit	k5eAaPmNgInS	pozastavit
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
nová	nový	k2eAgFnSc1d1	nová
směna	směna	k1gFnSc1	směna
operátorů	operátor	k1gMnPc2	operátor
<g/>
,	,	kIx,	,
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
probíhajícím	probíhající	k2eAgNnSc6d1	probíhající
dění	dění	k1gNnSc6	dění
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
další	další	k2eAgNnSc1d1	další
snižování	snižování	k1gNnSc1	snižování
výkonu	výkon	k1gInSc2	výkon
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
výstupní	výstupní	k2eAgInSc1d1	výstupní
výkon	výkon	k1gInSc1	výkon
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
klesl	klesnout	k5eAaPmAgInS	klesnout
až	až	k9	až
k	k	k7c3	k
30	[number]	k4	30
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
koncentrace	koncentrace	k1gFnSc1	koncentrace
neutrony	neutron	k1gInPc4	neutron
pohlcujícího	pohlcující	k2eAgInSc2d1	pohlcující
produktu	produkt	k1gInSc2	produkt
štěpení	štěpení	k1gNnSc4	štěpení
–	–	k?	–
xenonu	xenon	k1gInSc2	xenon
135	[number]	k4	135
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
produkt	produkt	k1gInSc1	produkt
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
normálně	normálně	k6eAd1	normálně
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
hodnotách	hodnota	k1gFnPc6	hodnota
výkonu	výkon	k1gInSc2	výkon
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
ihned	ihned	k6eAd1	ihned
přeměňoval	přeměňovat	k5eAaImAgMnS	přeměňovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
spojenému	spojený	k2eAgInSc3d1	spojený
s	s	k7c7	s
přechodným	přechodný	k2eAgInSc7d1	přechodný
poklesem	pokles	k1gInSc7	pokles
reaktivity	reaktivita	k1gFnSc2	reaktivita
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
xenonová	xenonový	k2eAgFnSc1d1	xenonová
otrava	otrava	k1gFnSc1	otrava
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
související	související	k2eAgInSc1d1	související
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsluze	obsluha	k1gFnSc3	obsluha
hrozil	hrozit	k5eAaImAgInS	hrozit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pád	pád	k1gInSc1	pád
reaktoru	reaktor	k1gInSc2	reaktor
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
jódové	jódový	k2eAgFnSc2d1	jódová
jámy	jáma	k1gFnSc2	jáma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
prudkého	prudký	k2eAgNnSc2d1	prudké
snížení	snížení	k1gNnSc2	snížení
výkonu	výkon	k1gInSc2	výkon
nebo	nebo	k8xC	nebo
náhlého	náhlý	k2eAgNnSc2d1	náhlé
odstavení	odstavení	k1gNnSc2	odstavení
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
uplatní	uplatnit	k5eAaPmIp3nP	uplatnit
parazitní	parazitní	k2eAgInPc4d1	parazitní
radionuklidy	radionuklid	k1gInPc4	radionuklid
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
izotop	izotop	k1gInSc1	izotop
jódu	jód	k1gInSc2	jód
vznikající	vznikající	k2eAgInPc1d1	vznikající
při	při	k7c6	při
štěpné	štěpný	k2eAgFnSc6d1	štěpná
reakci	reakce	k1gFnSc6	reakce
v	v	k7c6	v
palivových	palivový	k2eAgInPc6d1	palivový
článcích	článek	k1gInPc6	článek
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
výraznou	výrazný	k2eAgFnSc4d1	výrazná
absorpci	absorpce	k1gFnSc4	absorpce
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
xenon	xenon	k1gInSc4	xenon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
mnoha	mnoho	k4c2	mnoho
hodin	hodina	k1gFnPc2	hodina
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
obnovit	obnovit	k5eAaPmF	obnovit
činnost	činnost	k1gFnSc4	činnost
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
samovolnému	samovolný	k2eAgInSc3d1	samovolný
rozpadu	rozpad	k1gInSc3	rozpad
izotopu	izotop	k1gInSc2	izotop
s	s	k7c7	s
následnou	následný	k2eAgFnSc7d1	následná
možností	možnost	k1gFnSc7	možnost
opětovného	opětovný	k2eAgNnSc2d1	opětovné
zahájení	zahájení	k1gNnSc2	zahájení
štěpné	štěpný	k2eAgFnSc2d1	štěpná
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
reaktivity	reaktivita	k1gFnSc2	reaktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
experimentu	experiment	k1gInSc6	experiment
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
pokračovat	pokračovat	k5eAaImF	pokračovat
a	a	k8xC	a
reaktor	reaktor	k1gInSc1	reaktor
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgInS	muset
zůstat	zůstat	k5eAaPmF	zůstat
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
mimo	mimo	k7c4	mimo
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Obsluha	obsluha	k1gFnSc1	obsluha
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
snažila	snažit	k5eAaImAgFnS	snažit
zvýšit	zvýšit	k5eAaPmF	zvýšit
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
reaktor	reaktor	k1gInSc4	reaktor
v	v	k7c6	v
chodu	chod	k1gInSc6	chod
vysunováním	vysunování	k1gNnSc7	vysunování
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Operátoři	operátor	k1gMnPc1	operátor
přitom	přitom	k6eAd1	přitom
měli	mít	k5eAaImAgMnP	mít
udržovat	udržovat	k5eAaImF	udržovat
tzv.	tzv.	kA	tzv.
operativní	operativní	k2eAgFnSc4d1	operativní
zásobu	zásoba	k1gFnSc4	zásoba
reaktivity	reaktivita	k1gFnSc2	reaktivita
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
předpis	předpis	k1gInSc4	předpis
nedodrželi	dodržet	k5eNaPmAgMnP	dodržet
<g/>
.	.	kIx.	.
</s>
<s>
Nebyli	být	k5eNaImAgMnP	být
informováni	informován	k2eAgMnPc1d1	informován
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
neměli	mít	k5eNaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
aktuální	aktuální	k2eAgFnSc4d1	aktuální
zásobu	zásoba	k1gFnSc4	zásoba
sledovat	sledovat	k5eAaImF	sledovat
během	během	k7c2	během
experimentu	experiment	k1gInSc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
dostatečně	dostatečně	k6eAd1	dostatečně
zvýšit	zvýšit	k5eAaPmF	zvýšit
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
osádka	osádka	k1gFnSc1	osádka
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nezastavit	zastavit	k5eNaPmF	zastavit
reaktor	reaktor	k1gInSc4	reaktor
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
experimentu	experiment	k1gInSc6	experiment
při	při	k7c6	při
200	[number]	k4	200
MW	MW	kA	MW
namísto	namísto	k7c2	namísto
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
700	[number]	k4	700
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
přemíře	přemíra	k1gFnSc3	přemíra
neutrony	neutron	k1gInPc1	neutron
pohlcujícího	pohlcující	k2eAgInSc2d1	pohlcující
xenonu	xenon	k1gInSc2	xenon
135	[number]	k4	135
byly	být	k5eAaImAgFnP	být
regulační	regulační	k2eAgFnPc1d1	regulační
tyče	tyč	k1gFnPc1	tyč
vysunuty	vysunut	k2eAgFnPc1d1	vysunuta
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
o	o	k7c4	o
něco	něco	k3yInSc4	něco
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
normálním	normální	k2eAgNnSc6d1	normální
bezpečném	bezpečný	k2eAgNnSc6d1	bezpečné
řízení	řízení	k1gNnSc6	řízení
přípustné	přípustný	k2eAgInPc1d1	přípustný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
experimentu	experiment	k1gInSc2	experiment
byly	být	k5eAaImAgFnP	být
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
spuštěny	spuštěn	k2eAgFnPc1d1	spuštěna
vodní	vodní	k2eAgFnPc1d1	vodní
pumpy	pumpa	k1gFnPc1	pumpa
poháněné	poháněný	k2eAgFnPc1d1	poháněná
turbínovým	turbínový	k2eAgInSc7d1	turbínový
generátorem	generátor	k1gInSc7	generátor
<g/>
;	;	kIx,	;
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
takto	takto	k6eAd1	takto
generovaný	generovaný	k2eAgMnSc1d1	generovaný
překročil	překročit	k5eAaPmAgInS	překročit
meze	mez	k1gFnPc4	mez
stanovené	stanovený	k2eAgFnPc4d1	stanovená
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
regulací	regulace	k1gFnSc7	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
v	v	k7c6	v
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
<g/>
;	;	kIx,	;
a	a	k8xC	a
protože	protože	k8xS	protože
voda	voda	k1gFnSc1	voda
také	také	k9	také
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
další	další	k2eAgNnSc1d1	další
zvýšení	zvýšení	k1gNnSc1	zvýšení
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
si	se	k3xPyFc3	se
vynutilo	vynutit	k5eAaPmAgNnS	vynutit
dokonce	dokonce	k9	dokonce
odstranění	odstranění	k1gNnSc1	odstranění
i	i	k8xC	i
manuálně	manuálně	k6eAd1	manuálně
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
vysoce	vysoce	k6eAd1	vysoce
nestabilní	stabilní	k2eNgMnSc1d1	nestabilní
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
provozní	provozní	k2eAgFnPc1d1	provozní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
žádný	žádný	k3yNgInSc4	žádný
předpis	předpis	k1gInSc4	předpis
nezakazoval	zakazovat	k5eNaImAgMnS	zakazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všechny	všechen	k3xTgFnPc1	všechen
pumpy	pumpa	k1gFnPc1	pumpa
pracovaly	pracovat	k5eAaImAgFnP	pracovat
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
začal	začít	k5eAaPmAgInS	začít
experiment	experiment	k1gInSc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Nestabilní	stabilní	k2eNgInSc1d1	nestabilní
stav	stav	k1gInSc1	stav
reaktoru	reaktor	k1gInSc2	reaktor
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
neprojevil	projevit	k5eNaPmAgMnS	projevit
na	na	k7c6	na
kontrolním	kontrolní	k2eAgInSc6d1	kontrolní
panelu	panel	k1gInSc6	panel
a	a	k8xC	a
nezdálo	zdát	k5eNaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
z	z	k7c2	z
posádky	posádka	k1gFnSc2	posádka
reaktoru	reaktor	k1gInSc2	reaktor
obával	obávat	k5eAaImAgMnS	obávat
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Přívod	přívod	k1gInSc1	přívod
elektřiny	elektřina	k1gFnSc2	elektřina
do	do	k7c2	do
vodních	vodní	k2eAgFnPc2d1	vodní
pump	pumpa	k1gFnPc2	pumpa
byl	být	k5eAaImAgInS	být
vypnut	vypnout	k5eAaPmNgInS	vypnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
je	on	k3xPp3gNnSc4	on
poháněl	pohánět	k5eAaImAgInS	pohánět
turbínový	turbínový	k2eAgInSc1d1	turbínový
generátor	generátor	k1gInSc1	generátor
jen	jen	k9	jen
setrvačností	setrvačnost	k1gFnSc7	setrvačnost
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
se	se	k3xPyFc4	se
zmenšoval	zmenšovat	k5eAaImAgInS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Turbína	turbína	k1gFnSc1	turbína
byla	být	k5eAaImAgFnS	být
odpojena	odpojit	k5eAaPmNgFnS	odpojit
od	od	k7c2	od
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
páry	pára	k1gFnSc2	pára
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
reaktoru	reaktor	k1gInSc2	reaktor
se	se	k3xPyFc4	se
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
chladicí	chladicí	k2eAgFnSc1d1	chladicí
kapalina	kapalina	k1gFnSc1	kapalina
zahřívala	zahřívat	k5eAaImAgFnS	zahřívat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
potrubí	potrubí	k1gNnSc6	potrubí
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
kapsy	kapsa	k1gFnPc4	kapsa
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
RBMK	RBMK	kA	RBMK
grafitem	grafit	k1gInSc7	grafit
moderovaného	moderovaný	k2eAgInSc2d1	moderovaný
reaktoru	reaktor	k1gInSc2	reaktor
v	v	k7c6	v
Černobylu	Černobyl	k1gInSc6	Černobyl
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velkým	velký	k2eAgInSc7d1	velký
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
dutinovým	dutinový	k2eAgInSc7d1	dutinový
koeficientem	koeficient	k1gInSc7	koeficient
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
absenci	absence	k1gFnSc6	absence
neutrony	neutron	k1gInPc1	neutron
pohlcujícího	pohlcující	k2eAgInSc2d1	pohlcující
efektu	efekt	k1gInSc2	efekt
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
výkon	výkon	k1gInSc1	výkon
reaktoru	reaktor	k1gInSc2	reaktor
prudce	prudko	k6eAd1	prudko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
a	a	k8xC	a
reaktor	reaktor	k1gInSc1	reaktor
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stává	stávat	k5eAaImIp3nS	stávat
stále	stále	k6eAd1	stále
nestabilnějším	stabilní	k2eNgInSc7d2	nestabilnější
a	a	k8xC	a
nebezpečnějším	bezpečný	k2eNgInSc7d2	nebezpečnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
zmáčkli	zmáčknout	k5eAaPmAgMnP	zmáčknout
operátoři	operátor	k1gMnPc1	operátor
tlačítko	tlačítko	k1gNnSc1	tlačítko
"	"	kIx"	"
<g/>
AZ	AZ	kA	AZ
<g/>
5	[number]	k4	5
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
а	а	k?	а
з	з	k?	з
–	–	k?	–
havarijní	havarijní	k2eAgFnSc1d1	havarijní
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
rychlé	rychlý	k2eAgNnSc4d1	rychlé
odstavení	odstavení	k1gNnSc4	odstavení
reaktoru	reaktor	k1gInSc2	reaktor
–	–	k?	–
kompletní	kompletní	k2eAgNnSc1d1	kompletní
zasunutí	zasunutí	k1gNnSc1	zasunutí
všech	všecek	k3xTgFnPc2	všecek
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
manuálně	manuálně	k6eAd1	manuálně
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
vytaženy	vytáhnout	k5eAaPmNgFnP	vytáhnout
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nouzové	nouzový	k2eAgNnSc4d1	nouzové
opatření	opatření	k1gNnSc4	opatření
<g/>
,	,	kIx,	,
či	či	k8xC	či
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
rutinní	rutinní	k2eAgInSc1d1	rutinní
krok	krok	k1gInSc1	krok
zastavení	zastavení	k1gNnSc2	zastavení
reaktoru	reaktor	k1gInSc2	reaktor
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
experimentu	experiment	k1gInSc2	experiment
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
zastavení	zastavení	k1gNnSc1	zastavení
reaktoru	reaktor	k1gInSc2	reaktor
pro	pro	k7c4	pro
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
údržbu	údržba	k1gFnSc4	údržba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlé	rychlý	k2eAgNnSc1d1	rychlé
odstavení	odstavení	k1gNnSc1	odstavení
bylo	být	k5eAaImAgNnS	být
spuštěno	spustit	k5eAaPmNgNnS	spustit
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
neočekávané	očekávaný	k2eNgNnSc4d1	neočekávané
prudké	prudký	k2eAgNnSc4d1	prudké
zvýšení	zvýšení	k1gNnSc4	zvýšení
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Anatolij	Anatolij	k1gMnSc1	Anatolij
Stepanovič	Stepanovič	k1gMnSc1	Stepanovič
Djatlov	Djatlov	k1gInSc4	Djatlov
<g/>
,	,	kIx,	,
provozní	provozní	k2eAgMnSc1d1	provozní
zástupce	zástupce	k1gMnSc1	zástupce
hlavního	hlavní	k2eAgMnSc2d1	hlavní
inženýra	inženýr	k1gMnSc2	inženýr
Černobylské	černobylský	k2eAgFnSc2d1	Černobylská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c4	za
havárii	havárie	k1gFnSc4	havárie
nepřímo	přímo	k6eNd1	přímo
mohl	moct	k5eAaImAgMnS	moct
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaBmAgMnS	napsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
<g/>
Před	před	k7c7	před
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
systém	systém	k1gInSc1	systém
centrální	centrální	k2eAgFnSc2d1	centrální
kontroly	kontrola	k1gFnSc2	kontrola
nezaregistroval	zaregistrovat	k5eNaPmAgMnS	zaregistrovat
žádné	žádný	k3yNgFnPc4	žádný
změny	změna	k1gFnPc4	změna
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohly	moct	k5eAaImAgFnP	moct
ospravedlnit	ospravedlnit	k5eAaPmF	ospravedlnit
rychlé	rychlý	k2eAgFnPc4d1	rychlá
odstavení	odstavení	k1gNnPc4	odstavení
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
nashromáždila	nashromáždit	k5eAaPmAgFnS	nashromáždit
a	a	k8xC	a
analyzovala	analyzovat	k5eAaImAgFnS	analyzovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zprávě	zpráva	k1gFnSc6	zpráva
<g/>
,	,	kIx,	,
nemohla	moct	k5eNaImAgFnS	moct
najít	najít	k5eAaPmF	najít
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
bylo	být	k5eAaImAgNnS	být
rychlé	rychlý	k2eAgNnSc1d1	rychlé
odstavení	odstavení	k1gNnSc1	odstavení
spuštěno	spuštěn	k2eAgNnSc1d1	spuštěno
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
hledat	hledat	k5eAaImF	hledat
důvod	důvod	k1gInSc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
experimentu	experiment	k1gInSc2	experiment
jednoduše	jednoduše	k6eAd1	jednoduše
odstaven	odstavit	k5eAaPmNgInS	odstavit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kvůli	kvůli	k7c3	kvůli
pomalému	pomalý	k2eAgInSc3d1	pomalý
mechanismu	mechanismus	k1gInSc3	mechanismus
spouštění	spouštění	k1gNnSc2	spouštění
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
sekund	sekunda	k1gFnPc2	sekunda
do	do	k7c2	do
ukončení	ukončení	k1gNnSc2	ukončení
operace	operace	k1gFnSc2	operace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc3	jejich
dutým	dutý	k2eAgInPc3d1	dutý
koncům	konec	k1gInPc3	konec
a	a	k8xC	a
dočasnému	dočasný	k2eAgNnSc3d1	dočasné
odpojení	odpojení	k1gNnSc3	odpojení
chladicího	chladicí	k2eAgInSc2d1	chladicí
okruhu	okruh	k1gInSc2	okruh
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
vlivem	vliv	k1gInSc7	vliv
rychlého	rychlý	k2eAgNnSc2d1	rychlé
odstavení	odstavení	k1gNnSc2	odstavení
reaktoru	reaktor	k1gInSc2	reaktor
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
rychlosti	rychlost	k1gFnSc2	rychlost
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
produkce	produkce	k1gFnSc1	produkce
tepla	teplo	k1gNnSc2	teplo
způsobila	způsobit	k5eAaPmAgFnS	způsobit
deformaci	deformace	k1gFnSc4	deformace
vedení	vedení	k1gNnSc3	vedení
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
zasekly	zaseknout	k5eAaPmAgFnP	zaseknout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgFnP	být
zasunuty	zasunout	k5eAaPmNgInP	zasunout
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
a	a	k8xC	a
nebyly	být	k5eNaImAgFnP	být
proto	proto	k6eAd1	proto
schopné	schopný	k2eAgFnPc1d1	schopná
zastavit	zastavit	k5eAaPmF	zastavit
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
špička	špička	k1gFnSc1	špička
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
grafitu	grafit	k1gInSc2	grafit
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
po	po	k7c6	po
zasunutí	zasunutí	k1gNnSc6	zasunutí
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
havárii	havárie	k1gFnSc4	havárie
jen	jen	k9	jen
urychlil	urychlit	k5eAaPmAgInS	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
nebyly	být	k5eNaImAgFnP	být
předtím	předtím	k6eAd1	předtím
vytaženy	vytažen	k2eAgFnPc1d1	vytažena
úplně	úplně	k6eAd1	úplně
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
nevzrostl	vzrůst	k5eNaPmAgMnS	vzrůst
a	a	k8xC	a
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
regulování	regulování	k1gNnSc4	regulování
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
výkon	výkon	k1gInSc4	výkon
reaktoru	reaktor	k1gInSc2	reaktor
na	na	k7c4	na
asi	asi	k9	asi
30	[number]	k4	30
GW	GW	kA	GW
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
<g/>
:	:	kIx,	:
desetkrát	desetkrát	k6eAd1	desetkrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
normální	normální	k2eAgInSc4d1	normální
operační	operační	k2eAgInSc4d1	operační
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Palivové	palivový	k2eAgFnPc1d1	palivová
tyče	tyč	k1gFnPc1	tyč
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
tavit	tavit	k5eAaImF	tavit
a	a	k8xC	a
prudce	prudko	k6eAd1	prudko
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
tlak	tlak	k1gInSc1	tlak
páry	pára	k1gFnSc2	pára
způsobil	způsobit	k5eAaPmAgInS	způsobit
velkou	velký	k2eAgFnSc4d1	velká
parní	parní	k2eAgFnSc4d1	parní
expanzi	expanze	k1gFnSc4	expanze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odhodila	odhodit	k5eAaPmAgFnS	odhodit
a	a	k8xC	a
zničila	zničit	k5eAaPmAgFnS	zničit
kryt	kryt	k1gInSc4	kryt
reaktoru	reaktor	k1gInSc2	reaktor
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1	[number]	k4	1
000	[number]	k4	000
t	t	k?	t
a	a	k8xC	a
potrhala	potrhat	k5eAaPmAgFnS	potrhat
chladicí	chladicí	k2eAgNnSc4d1	chladicí
potrubí	potrubí	k1gNnSc4	potrubí
<g/>
.	.	kIx.	.
</s>
<s>
Asfalt	asfalt	k1gInSc1	asfalt
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
chránit	chránit	k5eAaImF	chránit
okolí	okolí	k1gNnSc4	okolí
před	před	k7c7	před
únikem	únik	k1gInSc7	únik
radiace	radiace	k1gFnSc2	radiace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vznítil	vznítit	k5eAaPmAgInS	vznítit
od	od	k7c2	od
žhavých	žhavý	k2eAgFnPc2d1	žhavá
trosek	troska	k1gFnPc2	troska
vyletujících	vyletující	k2eAgFnPc2d1	vyletující
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
střecha	střecha	k1gFnSc1	střecha
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
snižování	snižování	k1gNnSc2	snižování
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nebyla	být	k5eNaImAgFnS	být
konstrukce	konstrukce	k1gFnSc1	konstrukce
jeho	on	k3xPp3gInSc2	on
protihavarijního	protihavarijní	k2eAgInSc2d1	protihavarijní
pláště	plášť	k1gInSc2	plášť
–	–	k?	–
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
reaktoru	reaktor	k1gInSc2	reaktor
–	–	k?	–
dostatečně	dostatečně	k6eAd1	dostatečně
dimenzována	dimenzován	k2eAgFnSc1d1	dimenzována
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
úsporná	úsporný	k2eAgNnPc1d1	úsporné
opatření	opatření	k1gNnPc1	opatření
umožnila	umožnit	k5eAaPmAgNnP	umožnit
únik	únik	k1gInSc4	únik
radiací	radiace	k1gFnPc2	radiace
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
parní	parní	k2eAgFnSc1d1	parní
expanze	expanze	k1gFnSc1	expanze
způsobila	způsobit	k5eAaPmAgFnS	způsobit
porušení	porušení	k1gNnSc4	porušení
integrity	integrita	k1gFnSc2	integrita
primární	primární	k2eAgFnSc2d1	primární
tlakové	tlakový	k2eAgFnSc2d1	tlaková
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
byla	být	k5eAaImAgFnS	být
odhozena	odhozen	k2eAgFnSc1d1	odhozena
část	část	k1gFnSc1	část
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgInS	zahájit
příval	příval	k1gInSc1	příval
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
s	s	k7c7	s
extrémně	extrémně	k6eAd1	extrémně
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
grafitového	grafitový	k2eAgInSc2d1	grafitový
moderátoru	moderátor	k1gInSc2	moderátor
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
hoření	hoření	k1gNnSc4	hoření
grafitu	grafit	k1gInSc2	grafit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
požár	požár	k1gInSc4	požár
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
rozptýlení	rozptýlení	k1gNnSc3	rozptýlení
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
celkové	celkový	k2eAgFnSc3d1	celková
kontaminaci	kontaminace	k1gFnSc3	kontaminace
vnějších	vnější	k2eAgFnPc2d1	vnější
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
spory	spor	k1gInPc7	spor
ohledně	ohledně	k7c2	ohledně
přesného	přesný	k2eAgNnSc2d1	přesné
pořadí	pořadí	k1gNnSc2	pořadí
událostí	událost	k1gFnPc2	událost
po	po	k7c6	po
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
díky	díky	k7c3	díky
nesrovnalostem	nesrovnalost	k1gFnPc3	nesrovnalost
mezi	mezi	k7c7	mezi
zprávami	zpráva	k1gFnPc7	zpráva
očitých	očitý	k2eAgMnPc2d1	očitý
svědků	svědek	k1gMnPc2	svědek
a	a	k8xC	a
záznamy	záznam	k1gInPc1	záznam
z	z	k7c2	z
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřijímanější	přijímaný	k2eAgFnSc1d3	přijímaný
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
popsána	popsat	k5eAaPmNgFnS	popsat
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
nastala	nastat	k5eAaPmAgFnS	nastat
první	první	k4xOgFnSc1	první
exploze	exploze	k1gFnSc1	exploze
asi	asi	k9	asi
v	v	k7c6	v
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
47	[number]	k4	47
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc4	sedm
sekund	sekunda	k1gFnPc2	sekunda
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
operátoři	operátor	k1gMnPc1	operátor
spustili	spustit	k5eAaPmAgMnP	spustit
"	"	kIx"	"
<g/>
rychlé	rychlý	k2eAgNnSc4d1	rychlé
odstavení	odstavení	k1gNnSc4	odstavení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
exploze	exploze	k1gFnSc1	exploze
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
'	'	kIx"	'
<g/>
dříve	dříve	k6eAd2	dříve
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
že	že	k8xS	že
následovala	následovat	k5eAaImAgFnS	následovat
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
aktivaci	aktivace	k1gFnSc6	aktivace
havarijní	havarijní	k2eAgFnSc2d1	havarijní
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
pracovní	pracovní	k2eAgFnSc1d1	pracovní
verze	verze	k1gFnSc1	verze
sovětské	sovětský	k2eAgFnSc2d1	sovětská
komise	komise	k1gFnSc2	komise
studující	studující	k2eAgFnSc4d1	studující
havárii	havárie	k1gFnSc4	havárie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
reaktor	reaktor	k1gInSc1	reaktor
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
kritického	kritický	k2eAgInSc2d1	kritický
stavu	stav	k1gInSc2	stav
několik	několik	k4yIc1	několik
sekund	sekunda	k1gFnPc2	sekunda
po	po	k7c6	po
spuštění	spuštění	k1gNnSc6	spuštění
rychlého	rychlý	k2eAgNnSc2d1	rychlé
odstavení	odstavení	k1gNnSc2	odstavení
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
selhání	selhání	k1gNnSc1	selhání
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
připsat	připsat	k5eAaPmF	připsat
špatné	špatný	k2eAgFnSc3d1	špatná
konstrukci	konstrukce	k1gFnSc3	konstrukce
regulačních	regulační	k2eAgFnPc2d1	regulační
tyčí	tyč	k1gFnPc2	tyč
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
exploze	exploze	k1gFnSc1	exploze
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
spuštění	spuštění	k1gNnSc2	spuštění
rychlého	rychlý	k2eAgNnSc2d1	rychlé
odstavení	odstavení	k1gNnSc2	odstavení
by	by	kYmCp3nS	by
spíše	spíše	k9	spíše
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
na	na	k7c4	na
chybu	chyba	k1gFnSc4	chyba
operátorů	operátor	k1gMnPc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	slabý	k2eAgFnSc1d1	slabá
seismická	seismický	k2eAgFnSc1d1	seismická
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgNnPc1d1	podobné
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
o	o	k7c4	o
magnitudě	magnitudě	k6eAd1	magnitudě
2,5	[number]	k4	2,5
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
čase	čas	k1gInSc6	čas
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Černobylu	Černobyl	k1gInSc2	Černobyl
skutečně	skutečně	k6eAd1	skutečně
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
způsobena	způsoben	k2eAgFnSc1d1	způsobena
explozí	exploze	k1gFnSc7	exploze
nebo	nebo	k8xC	nebo
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
jen	jen	k9	jen
o	o	k7c4	o
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
shodu	shoda	k1gFnSc4	shoda
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
tlačítko	tlačítko	k1gNnSc1	tlačítko
AZ-5	AZ-5	k1gFnSc2	AZ-5
(	(	kIx(	(
<g/>
spuštění	spuštění	k1gNnSc1	spuštění
mechanizmu	mechanizmus	k1gInSc2	mechanizmus
rychlé	rychlý	k2eAgFnSc2d1	rychlá
odstávky	odstávka	k1gFnSc2	odstávka
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
stisknuto	stisknout	k5eAaPmNgNnS	stisknout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednou	jednou	k6eAd1	jednou
a	a	k8xC	a
Alexander	Alexandra	k1gFnPc2	Alexandra
Akimov	Akimovo	k1gNnPc2	Akimovo
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
směny	směna	k1gFnSc2	směna
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tlačítko	tlačítko	k1gNnSc4	tlačítko
AZ-5	AZ-5	k1gMnSc2	AZ-5
stiskl	stisknout	k5eAaPmAgMnS	stisknout
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaImF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc4	všechen
jsem	být	k5eAaImIp1nS	být
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Rozsah	rozsah	k1gInSc1	rozsah
tragédie	tragédie	k1gFnSc2	tragédie
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
zhoršen	zhoršit	k5eAaPmNgInS	zhoršit
nekompetencí	nekompetence	k1gFnSc7	nekompetence
místního	místní	k2eAgNnSc2d1	místní
vedení	vedení	k1gNnSc2	vedení
a	a	k8xC	a
nedostatkem	nedostatek	k1gInSc7	nedostatek
náležitého	náležitý	k2eAgNnSc2d1	náležité
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
dozimetry	dozimetr	k1gInPc1	dozimetr
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
reaktoru	reaktor	k1gInSc2	reaktor
kromě	kromě	k7c2	kromě
dvou	dva	k4xCgNnPc2	dva
měly	mít	k5eAaImAgFnP	mít
limit	limit	k1gInSc4	limit
1	[number]	k4	1
mR	mR	k?	mR
<g/>
/	/	kIx~	/
<g/>
sec	sec	kA	sec
(	(	kIx(	(
<g/>
milirentgen	milirentgen	k1gInSc1	milirentgen
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
3,6	[number]	k4	3,6
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
dva	dva	k4xCgInPc1	dva
měly	mít	k5eAaImAgFnP	mít
limit	limit	k1gInSc4	limit
1	[number]	k4	1
000	[number]	k4	000
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
sec	sec	kA	sec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
zablokován	zablokovat	k5eAaPmNgInS	zablokovat
explozí	exploze	k1gFnSc7	exploze
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
selhal	selhat	k5eAaPmAgMnS	selhat
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zapnutí	zapnutí	k1gNnSc6	zapnutí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
směna	směna	k1gFnSc1	směna
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
jistá	jistý	k2eAgFnSc1d1	jistá
pouze	pouze	k6eAd1	pouze
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnoty	hodnota	k1gFnPc1	hodnota
radiace	radiace	k1gFnSc2	radiace
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
budov	budova	k1gFnPc2	budova
reaktoru	reaktor	k1gInSc2	reaktor
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
hodnoty	hodnota	k1gFnPc1	hodnota
4	[number]	k4	4
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
(	(	kIx(	(
<g/>
skutečná	skutečný	k2eAgFnSc1d1	skutečná
úroveň	úroveň	k1gFnSc1	úroveň
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
;	;	kIx,	;
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
500	[number]	k4	500
rentgenů	rentgen	k1gInPc2	rentgen
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dovolilo	dovolit	k5eAaPmAgNnS	dovolit
náčelníku	náčelník	k1gMnSc3	náčelník
směny	směna	k1gFnSc2	směna
Alexandru	Alexandr	k1gMnSc3	Alexandr
Akimovovi	Akimova	k1gMnSc3	Akimova
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
reaktor	reaktor	k1gInSc1	reaktor
zůstal	zůstat	k5eAaPmAgInS	zůstat
nedotčen	dotknout	k5eNaPmNgInS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Důkazy	důkaz	k1gInPc1	důkaz
opaku	opak	k1gInSc2	opak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
kousky	kousek	k1gInPc4	kousek
grafitu	grafit	k1gInSc2	grafit
a	a	k8xC	a
paliva	palivo	k1gNnSc2	palivo
reaktoru	reaktor	k1gInSc2	reaktor
ležící	ležící	k2eAgInSc4d1	ležící
kolem	kolem	k7c2	kolem
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
ignorovány	ignorovat	k5eAaImNgInP	ignorovat
a	a	k8xC	a
údaje	údaj	k1gInPc1	údaj
jiného	jiný	k2eAgInSc2d1	jiný
dozimetru	dozimetr	k1gInSc2	dozimetr
přineseného	přinesený	k2eAgInSc2d1	přinesený
v	v	k7c4	v
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
byly	být	k5eAaImAgFnP	být
odmítnuty	odmítnout	k5eAaPmNgInP	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přístroj	přístroj	k1gInSc1	přístroj
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vadný	vadný	k2eAgInSc1d1	vadný
<g/>
.	.	kIx.	.
</s>
<s>
Akimov	Akimov	k1gInSc1	Akimov
zůstal	zůstat	k5eAaPmAgInS	zůstat
se	s	k7c7	s
směnou	směna	k1gFnSc7	směna
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
reaktoru	reaktor	k1gInSc2	reaktor
až	až	k6eAd1	až
do	do	k7c2	do
rána	ráno	k1gNnSc2	ráno
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
se	se	k3xPyFc4	se
do	do	k7c2	do
reaktoru	reaktor	k1gInSc2	reaktor
pumpovat	pumpovat	k5eAaImF	pumpovat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nenosil	nosit	k5eNaImAgMnS	nosit
ochranný	ochranný	k2eAgInSc4d1	ochranný
oblek	oblek	k1gInSc4	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Akimova	Akimův	k2eAgInSc2d1	Akimův
samotného	samotný	k2eAgInSc2d1	samotný
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
ozáření	ozáření	k1gNnSc4	ozáření
během	během	k7c2	během
tří	tři	k4xCgInPc2	tři
týdnů	týden	k1gInPc2	týden
následujících	následující	k2eAgInPc2d1	následující
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
přijeli	přijet	k5eAaPmAgMnP	přijet
hasiči	hasič	k1gMnPc1	hasič
uhasit	uhasit	k5eAaPmF	uhasit
ohně	oheň	k1gInPc4	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
jim	on	k3xPp3gMnPc3	on
neřekl	říct	k5eNaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
sutiny	sutina	k1gFnPc4	sutina
a	a	k8xC	a
kouř	kouř	k1gInSc4	kouř
jsou	být	k5eAaImIp3nP	být
nebezpečně	bezpečně	k6eNd1	bezpečně
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
<g/>
.	.	kIx.	.
</s>
<s>
Příčinu	příčina	k1gFnSc4	příčina
požáru	požár	k1gInSc2	požár
neznali	znát	k5eNaImAgMnP	znát
a	a	k8xC	a
proto	proto	k8xC	proto
hasili	hasit	k5eAaImAgMnP	hasit
vodou	voda	k1gFnSc7	voda
i	i	k8xC	i
reaktor	reaktor	k1gInSc1	reaktor
samotný	samotný	k2eAgInSc1d1	samotný
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
teplota	teplota	k1gFnSc1	teplota
asi	asi	k9	asi
2	[number]	k4	2
000	[number]	k4	000
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
teplotě	teplota	k1gFnSc6	teplota
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
na	na	k7c4	na
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
opětné	opětný	k2eAgNnSc4d1	opětné
slučování	slučování	k1gNnSc4	slučování
těchto	tento	k3xDgFnPc2	tento
látek	látka	k1gFnPc2	látka
provázely	provázet	k5eAaImAgInP	provázet
výbuchy	výbuch	k1gInPc1	výbuch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dále	daleko	k6eAd2	daleko
přispěly	přispět	k5eAaPmAgInP	přispět
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Otevřené	otevřený	k2eAgInPc1d1	otevřený
ohně	oheň	k1gInPc1	oheň
byly	být	k5eAaImAgInP	být
uhašeny	uhasit	k5eAaPmNgInP	uhasit
v	v	k7c6	v
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
hasičů	hasič	k1gMnPc2	hasič
však	však	k9	však
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
ozáření	ozáření	k1gNnSc1	ozáření
vysokými	vysoký	k2eAgFnPc7d1	vysoká
dávkami	dávka	k1gFnPc7	dávka
radiace	radiace	k1gFnSc2	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgMnSc1d1	vládní
komisař	komisař	k1gMnSc1	komisař
určený	určený	k2eAgInSc4d1	určený
k	k	k7c3	k
vyšetření	vyšetření	k1gNnSc3	vyšetření
havárie	havárie	k1gFnSc2	havárie
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Černobylu	Černobyl	k1gInSc2	Černobyl
ráno	ráno	k6eAd1	ráno
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
byli	být	k5eAaImAgMnP	být
již	již	k9	již
dva	dva	k4xCgMnPc1	dva
lidé	člověk	k1gMnPc1	člověk
mrtví	mrtvit	k5eAaImIp3nP	mrtvit
a	a	k8xC	a
52	[number]	k4	52
bylo	být	k5eAaImAgNnS	být
hospitalizováno	hospitalizovat	k5eAaBmNgNnS	hospitalizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
na	na	k7c4	na
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
více	hodně	k6eAd2	hodně
než	než	k8xS	než
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c4	po
explozi	exploze	k1gFnSc4	exploze
–	–	k?	–
komisař	komisař	k1gMnSc1	komisař
<g/>
,	,	kIx,	,
konfrontovaný	konfrontovaný	k2eAgMnSc1d1	konfrontovaný
s	s	k7c7	s
dostatečnými	dostatečný	k2eAgInPc7d1	dostatečný
důkazy	důkaz	k1gInPc7	důkaz
o	o	k7c6	o
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
radiace	radiace	k1gFnSc2	radiace
a	a	k8xC	a
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
případů	případ	k1gInPc2	případ
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
připustit	připustit	k5eAaPmF	připustit
zničení	zničení	k1gNnSc4	zničení
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
přikázat	přikázat	k5eAaPmF	přikázat
evakuaci	evakuace	k1gFnSc4	evakuace
blízkého	blízký	k2eAgNnSc2d1	blízké
města	město	k1gNnSc2	město
Pripjať	Pripjať	k1gFnSc2	Pripjať
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
omezila	omezit	k5eAaPmAgFnS	omezit
rozsah	rozsah	k1gInSc4	rozsah
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
,	,	kIx,	,
poslala	poslat	k5eAaPmAgFnS	poslat
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
na	na	k7c4	na
místo	místo	k6eAd1	místo
pracovníky	pracovník	k1gMnPc4	pracovník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gInPc4	on
vyčistili	vyčistit	k5eAaPmAgMnP	vyčistit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
"	"	kIx"	"
<g/>
likvidátorů	likvidátor	k1gMnPc2	likvidátor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
členů	člen	k1gInPc2	člen
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
)	)	kIx)	)
tam	tam	k6eAd1	tam
bylo	být	k5eAaImAgNnS	být
posláno	poslat	k5eAaPmNgNnS	poslat
jako	jako	k9	jako
do	do	k7c2	do
normálního	normální	k2eAgNnSc2d1	normální
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
;	;	kIx,	;
většině	většina	k1gFnSc6	většina
se	se	k3xPyFc4	se
nikdo	nikdo	k3yNnSc1	nikdo
nezmínil	zmínit	k5eNaPmAgMnS	zmínit
o	o	k7c6	o
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Neměli	mít	k5eNaImAgMnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ochranné	ochranný	k2eAgInPc4d1	ochranný
obleky	oblek	k1gInPc4	oblek
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgFnPc1d3	nejhorší
radioaktivní	radioaktivní	k2eAgFnPc1d1	radioaktivní
trosky	troska	k1gFnPc1	troska
vyvržené	vyvržený	k2eAgFnPc1d1	vyvržená
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
byly	být	k5eAaImAgInP	být
posbírány	posbírat	k5eAaPmNgInP	posbírat
a	a	k8xC	a
umístěny	umístit	k5eAaPmNgInP	umístit
do	do	k7c2	do
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc4	reaktor
sám	sám	k3xTgInSc4	sám
byl	být	k5eAaImAgInS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
pytli	pytel	k1gInSc6	pytel
s	s	k7c7	s
pískem	písek	k1gInSc7	písek
shazovanými	shazovaný	k2eAgFnPc7d1	shazovaná
z	z	k7c2	z
vrtulníků	vrtulník	k1gInPc2	vrtulník
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
5	[number]	k4	5
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
během	během	k7c2	během
týdne	týden	k1gInSc2	týden
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poškozená	poškozený	k2eAgFnSc1d1	poškozená
střecha	střecha	k1gFnSc1	střecha
nevydržela	vydržet	k5eNaPmAgFnS	vydržet
takové	takový	k3xDgNnSc4	takový
zatížení	zatížení	k1gNnSc4	zatížení
a	a	k8xC	a
část	část	k1gFnSc4	část
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
další	další	k2eAgFnSc1d1	další
radiace	radiace	k1gFnSc1	radiace
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
vojáků	voják	k1gMnPc2	voják
bylo	být	k5eAaImAgNnS	být
těžce	těžce	k6eAd1	těžce
ozářeno	ozářen	k2eAgNnSc1d1	ozářeno
<g/>
.	.	kIx.	.
</s>
<s>
Únikům	únik	k1gInPc3	únik
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
materiálu	materiál	k1gInSc2	materiál
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zamezit	zamezit	k5eAaPmF	zamezit
až	až	k9	až
po	po	k7c6	po
devíti	devět	k4xCc6	devět
dnech	den	k1gInPc6	den
od	od	k7c2	od
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zapečetěn	zapečetit	k5eAaPmNgInS	zapečetit
reaktor	reaktor	k1gInSc1	reaktor
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gInSc2	on
rychle	rychle	k6eAd1	rychle
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
velký	velký	k2eAgInSc1d1	velký
betonový	betonový	k2eAgInSc1d1	betonový
sarkofág	sarkofág	k1gInSc1	sarkofág
<g/>
.	.	kIx.	.
203	[number]	k4	203
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
okamžitě	okamžitě	k6eAd1	okamžitě
hospitalizováno	hospitalizovat	k5eAaBmNgNnS	hospitalizovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
31	[number]	k4	31
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
(	(	kIx(	(
<g/>
28	[number]	k4	28
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
na	na	k7c4	na
akutní	akutní	k2eAgFnSc4d1	akutní
nemoc	nemoc	k1gFnSc4	nemoc
z	z	k7c2	z
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byli	být	k5eAaImAgMnP	být
hasiči	hasič	k1gMnPc1	hasič
a	a	k8xC	a
záchranáři	záchranář	k1gMnPc1	záchranář
snažící	snažící	k2eAgMnPc1d1	snažící
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
havárii	havárie	k1gFnSc3	havárie
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
plně	plně	k6eAd1	plně
informováni	informovat	k5eAaBmNgMnP	informovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
je	být	k5eAaImIp3nS	být
ozáření	ozáření	k1gNnSc1	ozáření
(	(	kIx(	(
<g/>
z	z	k7c2	z
kouře	kouř	k1gInSc2	kouř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
135	[number]	k4	135
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
evakuováno	evakuovat	k5eAaBmNgNnS	evakuovat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
50	[number]	k4	50
000	[number]	k4	000
z	z	k7c2	z
blízkého	blízký	k2eAgNnSc2d1	blízké
města	město	k1gNnSc2	město
Pripjať	Pripjať	k1gFnSc2	Pripjať
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
po	po	k7c4	po
následujících	následující	k2eAgNnPc2d1	následující
70	[number]	k4	70
let	léto	k1gNnPc2	léto
2	[number]	k4	2
<g/>
%	%	kIx~	%
zvýšení	zvýšení	k1gNnSc3	zvýšení
úrovně	úroveň	k1gFnSc2	úroveň
rakoviny	rakovina	k1gFnSc2	rakovina
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
(	(	kIx(	(
<g/>
informační	informační	k2eAgInPc1d1	informační
zdroje	zdroj	k1gInPc1	zdroj
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
EBq	EBq	k1gFnSc1	EBq
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
×	×	k?	×
<g/>
1018	[number]	k4	1018
Bq	Bq	k1gFnPc2	Bq
<g/>
)	)	kIx)	)
radioaktivní	radioaktivní	k2eAgFnSc2d1	radioaktivní
kontaminace	kontaminace	k1gFnSc2	kontaminace
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
z	z	k7c2	z
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgMnPc2d1	další
10	[number]	k4	10
jedinců	jedinec	k1gMnPc2	jedinec
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
havárie	havárie	k1gFnSc2	havárie
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
IAEA	IAEA	kA	IAEA
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
havárie	havárie	k1gFnSc2	havárie
akce	akce	k1gFnSc2	akce
operátorů	operátor	k1gMnPc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1993	[number]	k4	1993
ale	ale	k8xC	ale
vydala	vydat	k5eAaPmAgFnS	vydat
IAEA	IAEA	kA	IAEA
revidovanou	revidovaný	k2eAgFnSc4d1	revidovaná
analýzu	analýza	k1gFnSc4	analýza
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
hlavní	hlavní	k2eAgFnSc6d1	hlavní
vinu	vinout	k5eAaImIp1nS	vinout
přisoudila	přisoudit	k5eAaPmAgFnS	přisoudit
konstrukci	konstrukce	k1gFnSc4	konstrukce
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
chybě	chyba	k1gFnSc3	chyba
operátorů	operátor	k1gMnPc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Černobylský	černobylský	k2eAgMnSc1d1	černobylský
4	[number]	k4	4
<g/>
.	.	kIx.	.
reaktor	reaktor	k1gInSc1	reaktor
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
asi	asi	k9	asi
190	[number]	k4	190
tun	tuna	k1gFnPc2	tuna
oxidu	oxid	k1gInSc2	oxid
uraničitého	uraničitý	k2eAgInSc2d1	uraničitý
a	a	k8xC	a
produktů	produkt	k1gInPc2	produkt
štěpení	štěpení	k1gNnSc2	štěpení
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
množství	množství	k1gNnSc1	množství
uniklého	uniklý	k2eAgInSc2d1	uniklý
materiálu	materiál	k1gInSc2	materiál
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
13	[number]	k4	13
a	a	k8xC	a
30	[number]	k4	30
procenty	procento	k1gNnPc7	procento
<g/>
.	.	kIx.	.
</s>
<s>
Kontaminovaný	kontaminovaný	k2eAgInSc1d1	kontaminovaný
materiál	materiál	k1gInSc1	materiál
z	z	k7c2	z
černobylské	černobylský	k2eAgFnSc2d1	Černobylská
havárie	havárie	k1gFnSc2	havárie
nebyl	být	k5eNaImAgInS	být
jednoduše	jednoduše	k6eAd1	jednoduše
rozprášen	rozprášit	k5eAaPmNgInS	rozprášit
po	po	k7c6	po
okolní	okolní	k2eAgFnSc6d1	okolní
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roztrousil	roztrousit	k5eAaPmAgMnS	roztrousit
se	se	k3xPyFc4	se
nepravidelně	pravidelně	k6eNd1	pravidelně
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
sovětských	sovětský	k2eAgMnPc2d1	sovětský
a	a	k8xC	a
západních	západní	k2eAgMnPc2d1	západní
vědců	vědec	k1gMnPc2	vědec
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
z	z	k7c2	z
kontaminace	kontaminace	k1gFnSc2	kontaminace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
území	území	k1gNnSc4	území
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
60	[number]	k4	60
%	%	kIx~	%
na	na	k7c4	na
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
byla	být	k5eAaImAgFnS	být
kontaminována	kontaminovat	k5eAaBmNgFnS	kontaminovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
oblast	oblast	k1gFnSc1	oblast
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Brjansku	Brjansko	k1gNnSc3	Brjansko
a	a	k8xC	a
části	část	k1gFnSc3	část
severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
byl	být	k5eAaImAgMnS	být
Černobyl	Černobyl	k1gInSc4	Černobyl
utajovanou	utajovaný	k2eAgFnSc7d1	utajovaná
katastrofou	katastrofa	k1gFnSc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velká	velký	k2eAgFnSc1d1	velká
jaderná	jaderný	k2eAgFnSc1d1	jaderná
havárie	havárie	k1gFnSc1	havárie
<g/>
,	,	kIx,	,
nepřinesly	přinést	k5eNaPmAgInP	přinést
sovětské	sovětský	k2eAgInPc1d1	sovětský
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
pracovníci	pracovník	k1gMnPc1	pracovník
Forsmarkské	Forsmarkský	k2eAgFnSc2d1	Forsmarkský
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
100	[number]	k4	100
km	km	kA	km
od	od	k7c2	od
Černobylu	Černobyl	k1gInSc2	Černobyl
<g/>
)	)	kIx)	)
nalezli	nalézt	k5eAaBmAgMnP	nalézt
radioaktivní	radioaktivní	k2eAgFnPc4d1	radioaktivní
částice	částice	k1gFnPc4	částice
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
oblečení	oblečení	k1gNnSc6	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Pátrání	pátrání	k1gNnSc1	pátrání
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
problém	problém	k1gInSc1	problém
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
švédských	švédský	k2eAgFnPc6d1	švédská
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
na	na	k7c4	na
vážný	vážný	k2eAgInSc4d1	vážný
jaderný	jaderný	k2eAgInSc4d1	jaderný
problém	problém	k1gInSc4	problém
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
konaly	konat	k5eAaImAgFnP	konat
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
prvomájové	prvomájový	k2eAgFnPc1d1	Prvomájová
manifestace	manifestace	k1gFnPc1	manifestace
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
nebylo	být	k5eNaImAgNnS	být
o	o	k7c6	o
katastrofě	katastrofa	k1gFnSc6	katastrofa
informováno	informován	k2eAgNnSc1d1	informováno
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
závod	závod	k1gInSc1	závod
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
–	–	k?	–
<g/>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
–	–	k?	–
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dnech	den	k1gInPc6	den
přemístěn	přemístěn	k2eAgMnSc1d1	přemístěn
z	z	k7c2	z
ČSSR	ČSSR	kA	ČSSR
do	do	k7c2	do
Kyjeva	Kyjev	k1gInSc2	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
nebylo	být	k5eNaImAgNnS	být
informováno	informovat	k5eAaBmNgNnS	informovat
o	o	k7c4	o
zamoření	zamoření	k1gNnSc4	zamoření
ani	ani	k8xC	ani
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
pracovníci	pracovník	k1gMnPc1	pracovník
elektrárny	elektrárna	k1gFnSc2	elektrárna
Dukovany	Dukovany	k1gInPc1	Dukovany
již	již	k6eAd1	již
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1986	[number]	k4	1986
zachytili	zachytit	k5eAaPmAgMnP	zachytit
stopy	stop	k1gInPc4	stop
kontaminace	kontaminace	k1gFnSc2	kontaminace
ovzduší	ovzduší	k1gNnSc2	ovzduší
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
běžně	běžně	k6eAd1	běžně
prováděných	prováděný	k2eAgNnPc2d1	prováděné
měření	měření	k1gNnPc2	měření
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
následně	následně	k6eAd1	následně
potvrdila	potvrdit	k5eAaPmAgNnP	potvrdit
data	datum	k1gNnPc1	datum
z	z	k7c2	z
Krajských	krajský	k2eAgFnPc2d1	krajská
hygienických	hygienický	k2eAgFnPc2d1	hygienická
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Odhadované	odhadovaný	k2eAgFnPc1d1	odhadovaná
dávky	dávka	k1gFnPc1	dávka
ozáření	ozáření	k1gNnSc2	ozáření
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
však	však	k9	však
byly	být	k5eAaImAgFnP	být
poměrně	poměrně	k6eAd1	poměrně
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nebyla	být	k5eNaImAgNnP	být
zahájena	zahájit	k5eAaPmNgNnP	zahájit
žádná	žádný	k3yNgNnPc1	žádný
zvláštní	zvláštní	k2eAgNnPc1d1	zvláštní
protiopatření	protiopatření	k1gNnPc1	protiopatření
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1986	[number]	k4	1986
ovšem	ovšem	k9	ovšem
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
pravidelně	pravidelně	k6eAd1	pravidelně
kontrolován	kontrolovat	k5eAaImNgInS	kontrolovat
obsah	obsah	k1gInSc1	obsah
radionuklidů	radionuklid	k1gInPc2	radionuklid
v	v	k7c6	v
mléce	mléko	k1gNnSc6	mléko
a	a	k8xC	a
mléčných	mléčný	k2eAgInPc6d1	mléčný
výrobcích	výrobek	k1gInPc6	výrobek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
pitné	pitný	k2eAgFnSc6d1	pitná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
zelenině	zelenina	k1gFnSc6	zelenina
<g/>
,	,	kIx,	,
obilninách	obilnina	k1gFnPc6	obilnina
<g/>
,	,	kIx,	,
mase	maso	k1gNnSc6	maso
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
krmivu	krmivo	k1gNnSc6	krmivo
pro	pro	k7c4	pro
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
v	v	k7c6	v
houbách	houba	k1gFnPc6	houba
a	a	k8xC	a
lesních	lesní	k2eAgInPc6d1	lesní
plodech	plod	k1gInPc6	plod
<g/>
.	.	kIx.	.
</s>
<s>
Pracovníky	pracovník	k1gMnPc7	pracovník
účastnící	účastnící	k2eAgFnSc2d1	účastnící
se	se	k3xPyFc4	se
obnovy	obnova	k1gFnSc2	obnova
a	a	k8xC	a
vyčištění	vyčištění	k1gNnSc2	vyčištění
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
vysoké	vysoký	k2eAgFnPc1d1	vysoká
dávky	dávka	k1gFnPc1	dávka
radiace	radiace	k1gFnSc2	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
nebyli	být	k5eNaImAgMnP	být
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
osobními	osobní	k2eAgInPc7d1	osobní
dozimetry	dozimetr	k1gInPc7	dozimetr
měřícími	měřící	k2eAgInPc7d1	měřící
množství	množství	k1gNnSc1	množství
obdržené	obdržený	k2eAgFnSc2d1	obdržená
radiace	radiace	k1gFnSc2	radiace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
velikost	velikost	k1gFnSc1	velikost
těchto	tento	k3xDgFnPc2	tento
dávek	dávka	k1gFnPc2	dávka
mohli	moct	k5eAaImAgMnP	moct
odborníci	odborník	k1gMnPc1	odborník
jen	jen	k9	jen
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dozimetry	dozimetr	k1gInPc7	dozimetr
používaly	používat	k5eAaImAgInP	používat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dozimetrické	dozimetrický	k2eAgFnPc1d1	dozimetrická
procedury	procedura	k1gFnPc1	procedura
lišily	lišit	k5eAaImAgFnP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
některých	některý	k3yIgMnPc6	některý
pracovnících	pracovník	k1gMnPc6	pracovník
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odhady	odhad	k1gInPc1	odhad
dávek	dávka	k1gFnPc2	dávka
radiace	radiace	k1gFnSc2	radiace
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
případě	případ	k1gInSc6	případ
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
přesnější	přesný	k2eAgInPc1d2	přesnější
než	než	k8xS	než
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sovětských	sovětský	k2eAgInPc2d1	sovětský
odhadů	odhad	k1gInPc2	odhad
se	s	k7c7	s
300	[number]	k4	300
000	[number]	k4	000
až	až	k9	až
600	[number]	k4	600
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
účastnilo	účastnit	k5eAaImAgNnS	účastnit
vyčištění	vyčištění	k1gNnSc1	vyčištění
30	[number]	k4	30
km	km	kA	km
evakuační	evakuační	k2eAgFnSc2d1	evakuační
zóny	zóna	k1gFnSc2	zóna
kolem	kolem	k7c2	kolem
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
zóny	zóna	k1gFnSc2	zóna
až	až	k9	až
2	[number]	k4	2
roky	rok	k1gInPc4	rok
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Odhady	odhad	k1gInPc1	odhad
množství	množství	k1gNnSc1	množství
"	"	kIx"	"
<g/>
likvidátorů	likvidátor	k1gMnPc2	likvidátor
<g/>
"	"	kIx"	"
–	–	k?	–
pracovníků	pracovník	k1gMnPc2	pracovník
přivezených	přivezený	k2eAgMnPc2d1	přivezený
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
obnově	obnova	k1gFnSc6	obnova
–	–	k?	–
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
;	;	kIx,	;
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
například	například	k6eAd1	například
uvádí	uvádět	k5eAaImIp3nS	uvádět
sumu	suma	k1gFnSc4	suma
800	[number]	k4	800
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Rusko	Rusko	k1gNnSc1	Rusko
počítá	počítat	k5eAaImIp3nS	počítat
mezi	mezi	k7c7	mezi
likvidátory	likvidátor	k1gMnPc7	likvidátor
některé	některý	k3yIgMnPc4	některý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
kontaminovaných	kontaminovaný	k2eAgFnPc6d1	kontaminovaná
oblastech	oblast	k1gFnPc6	oblast
nepracovali	pracovat	k5eNaImAgMnP	pracovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
se	se	k3xPyFc4	se
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
pracujících	pracující	k2eAgMnPc2d1	pracující
na	na	k7c6	na
vyčištění	vyčištění	k1gNnSc6	vyčištění
zóny	zóna	k1gFnSc2	zóna
odhadovalo	odhadovat	k5eAaImAgNnS	odhadovat
na	na	k7c4	na
211	[number]	k4	211
000	[number]	k4	000
a	a	k8xC	a
tito	tento	k3xDgMnPc1	tento
pracovníci	pracovník	k1gMnPc1	pracovník
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
odhadovanou	odhadovaný	k2eAgFnSc4d1	odhadovaná
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
dávku	dávka	k1gFnSc4	dávka
165	[number]	k4	165
mSv	mSv	k?	mSv
(	(	kIx(	(
<g/>
16,5	[number]	k4	16,5
Rem	Rem	k1gFnPc2	Rem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
děti	dítě	k1gFnPc1	dítě
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
kontaminovaných	kontaminovaný	k2eAgFnPc6d1	kontaminovaná
oblastech	oblast	k1gFnPc6	oblast
vystaveny	vystavit	k5eAaPmNgInP	vystavit
vysokým	vysoký	k2eAgFnPc3d1	vysoká
dávkám	dávka	k1gFnPc3	dávka
až	až	k9	až
50	[number]	k4	50
Gy	Gy	k1gFnPc2	Gy
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
výskyt	výskyt	k1gInSc4	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přijímaly	přijímat	k5eAaImAgFnP	přijímat
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
jód	jód	k1gInSc4	jód
<g/>
,	,	kIx,	,
izotop	izotop	k1gInSc4	izotop
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
,	,	kIx,	,
z	z	k7c2	z
místního	místní	k2eAgNnSc2d1	místní
kontaminovaného	kontaminovaný	k2eAgNnSc2d1	kontaminované
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
studií	studio	k1gNnPc2	studio
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
výskyt	výskyt	k1gInSc1	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
prudce	prudko	k6eAd1	prudko
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
IAEA	IAEA	kA	IAEA
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
1	[number]	k4	1
800	[number]	k4	800
dokumentovaných	dokumentovaný	k2eAgInPc2d1	dokumentovaný
případů	případ	k1gInPc2	případ
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
bylo	být	k5eAaImAgNnS	být
14	[number]	k4	14
a	a	k8xC	a
méně	málo	k6eAd2	málo
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
havárie	havárie	k1gFnSc1	havárie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hodnota	hodnota	k1gFnSc1	hodnota
než	než	k8xS	než
normálně	normálně	k6eAd1	normálně
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
ale	ale	k8xC	ale
neuvádí	uvádět	k5eNaImIp3nS	uvádět
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
běžnou	běžný	k2eAgFnSc4d1	běžná
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
typy	typ	k1gInPc7	typ
dětské	dětský	k2eAgFnSc2d1	dětská
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
a	a	k8xC	a
agresivní	agresivní	k2eAgInPc1d1	agresivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
včas	včas	k6eAd1	včas
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
vyléčit	vyléčit	k5eAaPmF	vyléčit
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
operaci	operace	k1gFnSc6	operace
následované	následovaný	k2eAgFnSc2d1	následovaná
aplikací	aplikace	k1gFnSc7	aplikace
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
jódu	jód	k1gInSc2	jód
131	[number]	k4	131
na	na	k7c6	na
potlačení	potlačení	k1gNnSc6	potlačení
metastáz	metastáza	k1gFnPc2	metastáza
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
léčba	léčba	k1gFnSc1	léčba
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
jeví	jevit	k5eAaImIp3nS	jevit
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
diagnostikovaných	diagnostikovaný	k2eAgInPc2d1	diagnostikovaný
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
spojila	spojit	k5eAaPmAgFnS	spojit
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
téměř	téměř	k6eAd1	téměř
700	[number]	k4	700
případů	případ	k1gInPc2	případ
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
adolescentů	adolescent	k1gMnPc2	adolescent
s	s	k7c7	s
černobylskou	černobylský	k2eAgFnSc7d1	Černobylská
havárií	havárie	k1gFnSc7	havárie
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
asi	asi	k9	asi
10	[number]	k4	10
úmrtí	úmrtí	k1gNnSc4	úmrtí
připsala	připsat	k5eAaPmAgFnS	připsat
radiaci	radiace	k1gFnSc4	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zaznamenaného	zaznamenaný	k2eAgInSc2d1	zaznamenaný
výrazného	výrazný	k2eAgInSc2d1	výrazný
nárůstu	nárůst	k1gInSc2	nárůst
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
důsledkem	důsledek	k1gInSc7	důsledek
rentgenování	rentgenování	k1gNnSc2	rentgenování
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
čekací	čekací	k2eAgFnSc1d1	čekací
doba	doba	k1gFnSc1	doba
radiací	radiace	k1gFnSc7	radiace
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
ale	ale	k8xC	ale
zvýšení	zvýšení	k1gNnSc1	zvýšení
dětské	dětský	k2eAgFnSc2d1	dětská
rakoviny	rakovina	k1gFnSc2	rakovina
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
je	být	k5eAaImIp3nS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
zvýšení	zvýšení	k1gNnSc1	zvýšení
buďto	buďto	k8xC	buďto
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsme	být	k5eAaImIp1nP	být
dosud	dosud	k6eAd1	dosud
mechanismu	mechanismus	k1gInSc2	mechanismus
stojícímu	stojící	k2eAgMnSc3d1	stojící
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
správně	správně	k6eAd1	správně
neporozuměli	porozumět	k5eNaPmAgMnP	porozumět
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nelze	lze	k6eNd1	lze
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
žádné	žádný	k3yNgNnSc4	žádný
zvýšení	zvýšení	k1gNnSc4	zvýšení
leukémie	leukémie	k1gFnSc2	leukémie
<g/>
,	,	kIx,	,
očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
jasně	jasně	k6eAd1	jasně
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
společně	společně	k6eAd1	společně
s	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
výskytu	výskyt	k1gInSc2	výskyt
jiných	jiný	k2eAgFnPc2d1	jiná
rakovin	rakovina	k1gFnPc2	rakovina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
statisticky	statisticky	k6eAd1	statisticky
nerozpoznatelným	rozpoznatelný	k2eNgInSc7d1	nerozpoznatelný
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgNnSc1	žádný
zvýšení	zvýšení	k1gNnSc1	zvýšení
připsatelné	připsatelný	k2eAgNnSc1d1	připsatelný
Černobylu	Černobyl	k1gInSc6	Černobyl
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
u	u	k7c2	u
vrozených	vrozený	k2eAgFnPc2d1	vrozená
vad	vada	k1gFnPc2	vada
<g/>
,	,	kIx,	,
nepříznivých	příznivý	k2eNgInPc2d1	nepříznivý
výsledků	výsledek	k1gInPc2	výsledek
těhotenství	těhotenství	k1gNnSc2	těhotenství
ani	ani	k8xC	ani
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
nemocí	nemoc	k1gFnPc2	nemoc
způsobených	způsobený	k2eAgFnPc2d1	způsobená
radiací	radiace	k1gFnPc2	radiace
u	u	k7c2	u
obecné	obecný	k2eAgFnSc2d1	obecná
populace	populace	k1gFnSc2	populace
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
kontaminovaných	kontaminovaný	k2eAgFnPc6d1	kontaminovaná
oblastech	oblast	k1gFnPc6	oblast
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
shazovaly	shazovat	k5eAaImAgInP	shazovat
vrtulníky	vrtulník	k1gInPc1	vrtulník
tuny	tuna	k1gFnSc2	tuna
písku	písek	k1gInSc2	písek
na	na	k7c4	na
rozžhavený	rozžhavený	k2eAgInSc4d1	rozžhavený
reaktor	reaktor	k1gInSc4	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
takovém	takový	k3xDgInSc6	takový
manévru	manévr	k1gInSc6	manévr
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1986	[number]	k4	1986
zřítil	zřítit	k5eAaPmAgInS	zřítit
vrtulník	vrtulník	k1gInSc1	vrtulník
Mi	já	k3xPp1nSc3	já
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yQnSc2	což
zahynula	zahynout	k5eAaPmAgFnS	zahynout
celá	celý	k2eAgFnSc1d1	celá
čtyřčlenná	čtyřčlenný	k2eAgFnSc1d1	čtyřčlenná
posádka	posádka	k1gFnSc1	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
v	v	k7c6	v
nízké	nízký	k2eAgFnSc6d1	nízká
výšce	výška	k1gFnSc6	výška
zavadil	zavadit	k5eAaPmAgMnS	zavadit
vrtulí	vrtule	k1gFnSc7	vrtule
o	o	k7c4	o
lana	lano	k1gNnPc4	lano
jeřábu	jeřáb	k1gInSc2	jeřáb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stál	stát	k5eAaImAgInS	stát
poblíž	poblíž	k7c2	poblíž
zničeného	zničený	k2eAgInSc2d1	zničený
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
zřítil	zřítit	k5eAaPmAgInS	zřítit
se	se	k3xPyFc4	se
jen	jen	k9	jen
pár	pár	k4xCyI	pár
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
byl	být	k5eAaImAgMnS	být
pilot	pilot	k1gMnSc1	pilot
oslněn	oslnit	k5eAaPmNgMnS	oslnit
sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
jeřáb	jeřáb	k1gMnSc1	jeřáb
neviděl	vidět	k5eNaImAgMnS	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
tvořili	tvořit	k5eAaImAgMnP	tvořit
<g/>
:	:	kIx,	:
Leonid	Leonid	k1gInSc1	Leonid
Christič	Christič	k1gInSc1	Christič
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
1953	[number]	k4	1953
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
byl	být	k5eAaImAgInS	být
největším	veliký	k2eAgNnSc7d3	veliký
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
rizikem	riziko	k1gNnSc7	riziko
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
jód	jód	k1gInSc4	jód
131I	[number]	k4	131I
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
8	[number]	k4	8
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
budí	budit	k5eAaImIp3nP	budit
největší	veliký	k2eAgFnPc1d3	veliký
obavy	obava	k1gFnPc1	obava
kontaminace	kontaminace	k1gFnSc2	kontaminace
půdy	půda	k1gFnSc2	půda
izotopy	izotop	k1gInPc1	izotop
stroncia	stroncium	k1gNnSc2	stroncium
90	[number]	k4	90
<g/>
Sr	Sr	k1gFnSc2	Sr
a	a	k8xC	a
cesia	cesium	k1gNnSc2	cesium
137	[number]	k4	137
<g/>
Cs	Cs	k1gFnPc2	Cs
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
poločas	poločas	k1gInSc4	poločas
rozpadu	rozpad	k1gInSc2	rozpad
kolem	kolem	k7c2	kolem
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
137	[number]	k4	137
<g/>
Cs	Cs	k1gFnSc6	Cs
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
v	v	k7c6	v
povrchových	povrchový	k2eAgFnPc6d1	povrchová
vrstvách	vrstva	k1gFnPc6	vrstva
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
absorbovány	absorbovat	k5eAaBmNgFnP	absorbovat
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
houbami	houba	k1gFnPc7	houba
a	a	k8xC	a
dostávají	dostávat	k5eAaImIp3nP	dostávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
místního	místní	k2eAgInSc2d1	místní
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInPc1d1	dřívější
testy	test	k1gInPc1	test
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
kontaminovaných	kontaminovaný	k2eAgFnPc6d1	kontaminovaná
oblastech	oblast	k1gFnPc6	oblast
množství	množství	k1gNnSc1	množství
137	[number]	k4	137
<g/>
Cs	Cs	k1gMnPc2	Cs
ve	v	k7c6	v
stromech	strom	k1gInPc6	strom
stále	stále	k6eAd1	stále
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kontaminace	kontaminace	k1gFnSc1	kontaminace
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
do	do	k7c2	do
podzemních	podzemní	k2eAgInPc2d1	podzemní
zvodní	zvodnit	k5eAaPmIp3nS	zvodnit
a	a	k8xC	a
uzavřených	uzavřený	k2eAgInPc2d1	uzavřený
vodních	vodní	k2eAgInPc2d1	vodní
rezervoárů	rezervoár	k1gInPc2	rezervoár
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
jezera	jezero	k1gNnPc1	jezero
a	a	k8xC	a
rybníky	rybník	k1gInPc1	rybník
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Germenchuk	Germenchuk	k1gInSc1	Germenchuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
způsobem	způsob	k1gInSc7	způsob
odstranění	odstranění	k1gNnSc1	odstranění
kontaminace	kontaminace	k1gFnSc2	kontaminace
bude	být	k5eAaImBp3nS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
rozpad	rozpad	k1gInSc1	rozpad
137	[number]	k4	137
<g/>
Cs	Cs	k1gFnPc2	Cs
na	na	k7c4	na
stabilní	stabilní	k2eAgInSc4d1	stabilní
izotop	izotop	k1gInSc4	izotop
barya	baryum	k1gNnSc2	baryum
137	[number]	k4	137
<g/>
Ba	ba	k9	ba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vymývání	vymývání	k1gNnSc4	vymývání
deštěm	dešť	k1gInSc7	dešť
a	a	k8xC	a
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
zanedbatelné	zanedbatelný	k2eAgNnSc1d1	zanedbatelné
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
poznámky	poznámka	k1gFnPc1	poznámka
IAEA	IAEA	kA	IAEA
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
černobylská	černobylský	k2eAgFnSc1d1	Černobylská
havárie	havárie	k1gFnSc1	havárie
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
tolik	tolik	k6eAd1	tolik
radioaktivní	radioaktivní	k2eAgFnSc1d1	radioaktivní
kontaminace	kontaminace	k1gFnSc1	kontaminace
jako	jako	k9	jako
400	[number]	k4	400
bomb	bomba	k1gFnPc2	bomba
z	z	k7c2	z
Hirošimy	Hirošima	k1gFnSc2	Hirošima
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
velikost	velikost	k1gFnSc1	velikost
asi	asi	k9	asi
100	[number]	k4	100
<g/>
×	×	k?	×
až	až	k9	až
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
kontaminace	kontaminace	k1gFnSc1	kontaminace
způsobená	způsobený	k2eAgFnSc1d1	způsobená
atmosférickými	atmosférický	k2eAgMnPc7d1	atmosférický
testy	test	k1gMnPc7	test
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
proto	proto	k8xC	proto
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgFnS	být
černobylská	černobylský	k2eAgFnSc1d1	Černobylská
havárie	havárie	k1gFnSc1	havárie
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
lokální	lokální	k2eAgFnSc4d1	lokální
katastrofou	katastrofa	k1gFnSc7	katastrofa
<g/>
,	,	kIx,	,
nepřerostla	přerůst	k5eNaPmAgFnS	přerůst
v	v	k7c4	v
katastrofu	katastrofa	k1gFnSc4	katastrofa
globální	globální	k2eAgFnSc4d1	globální
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vědců	vědec	k1gMnPc2	vědec
na	na	k7c6	na
první	první	k4xOgFnSc6	první
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
konferenci	konference	k1gFnSc6	konference
o	o	k7c6	o
biologických	biologický	k2eAgInPc6d1	biologický
a	a	k8xC	a
radiologických	radiologický	k2eAgInPc6d1	radiologický
aspektech	aspekt	k1gInPc6	aspekt
černobylské	černobylský	k2eAgFnSc2d1	Černobylská
havárie	havárie	k1gFnSc2	havárie
(	(	kIx(	(
<g/>
konané	konaný	k2eAgFnSc2d1	konaná
v	v	k7c6	v
září	září	k1gNnSc6	září
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
úroveň	úroveň	k1gFnSc1	úroveň
spadu	spad	k1gInSc2	spad
v	v	k7c4	v
10	[number]	k4	10
km	km	kA	km
zóně	zóna	k1gFnSc6	zóna
kolem	kolem	k7c2	kolem
elektrárny	elektrárna	k1gFnSc2	elektrárna
až	až	k9	až
4,81	[number]	k4	4,81
GBq	GBq	k1gFnPc2	GBq
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
Rudý	rudý	k2eAgInSc1d1	rudý
les	les	k1gInSc1	les
<g/>
"	"	kIx"	"
z	z	k7c2	z
borovic	borovice	k1gFnPc2	borovice
zničený	zničený	k2eAgInSc4d1	zničený
silným	silný	k2eAgInSc7d1	silný
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
spadem	spad	k1gInSc7	spad
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
10	[number]	k4	10
km	km	kA	km
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
hned	hned	k6eAd1	hned
za	za	k7c7	za
komplexem	komplex	k1gInSc7	komplex
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
lesa	les	k1gInSc2	les
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dnů	den	k1gInPc2	den
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stromy	strom	k1gInPc1	strom
jevily	jevit	k5eAaImAgInP	jevit
temně	temně	k6eAd1	temně
rudé	rudý	k2eAgInPc1d1	rudý
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hynuly	hynout	k5eAaImAgFnP	hynout
na	na	k7c4	na
následky	následek	k1gInPc4	následek
ozáření	ozáření	k1gNnSc2	ozáření
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vyčišťovacích	vyčišťovací	k2eAgFnPc2d1	vyčišťovací
operací	operace	k1gFnPc2	operace
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
4	[number]	k4	4
km2	km2	k4	km2
lesa	les	k1gInSc2	les
srovnána	srovnán	k2eAgFnSc1d1	srovnána
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
spálena	spálen	k2eAgFnSc1d1	spálena
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Rudého	rudý	k2eAgInSc2d1	rudý
lesa	les	k1gInSc2	les
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
lokalitu	lokalita	k1gFnSc4	lokalita
bohatou	bohatý	k2eAgFnSc4d1	bohatá
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
mnoha	mnoho	k4c2	mnoho
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
půda	půda	k1gFnSc1	půda
a	a	k8xC	a
lesy	les	k1gInPc1	les
ze	z	k7c2	z
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
byly	být	k5eAaImAgFnP	být
zasaženy	zasažen	k2eAgMnPc4d1	zasažen
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
spadem	spad	k1gInSc7	spad
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
hrozba	hrozba	k1gFnSc1	hrozba
lesního	lesní	k2eAgInSc2d1	lesní
požáru	požár	k1gInSc2	požár
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nP	by
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
nad	nad	k7c4	nad
Evropu	Evropa	k1gFnSc4	Evropa
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
radiaoaktivní	radiaoaktivní	k2eAgInPc4d1	radiaoaktivní
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Sovětští	sovětský	k2eAgMnPc1d1	sovětský
odpovědní	odpovědný	k2eAgMnPc1d1	odpovědný
činitelé	činitel	k1gMnPc1	činitel
zahájili	zahájit	k5eAaPmAgMnP	zahájit
evakuaci	evakuace	k1gFnSc4	evakuace
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Černobylu	Černobyl	k1gInSc2	Černobyl
36	[number]	k4	36
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
přemístěni	přemístěn	k2eAgMnPc1d1	přemístěn
všichni	všechen	k3xTgMnPc1	všechen
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
30	[number]	k4	30
km	km	kA	km
kolem	kolem	k7c2	kolem
elektrárny	elektrárna	k1gFnSc2	elektrárna
(	(	kIx(	(
<g/>
asi	asi	k9	asi
116	[number]	k4	116
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vědců	vědec	k1gMnPc2	vědec
bylo	být	k5eAaImAgNnS	být
28	[number]	k4	28
000	[number]	k4	000
km2	km2	k4	km2
kontaminováno	kontaminovat	k5eAaBmNgNnS	kontaminovat
137	[number]	k4	137
<g/>
Cs	Cs	k1gMnPc2	Cs
o	o	k7c6	o
úrovni	úroveň	k1gFnSc6	úroveň
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
185	[number]	k4	185
kBq	kBq	k?	kBq
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
žilo	žít	k5eAaImAgNnS	žít
přibližně	přibližně	k6eAd1	přibližně
830	[number]	k4	830
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
10	[number]	k4	10
500	[number]	k4	500
km2	km2	k4	km2
bylo	být	k5eAaImAgNnS	být
kontaminováno	kontaminovat	k5eAaBmNgNnS	kontaminovat
137	[number]	k4	137
<g/>
Cs	Cs	k1gMnPc2	Cs
o	o	k7c6	o
úrovni	úroveň	k1gFnSc6	úroveň
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
555	[number]	k4	555
kBq	kBq	k?	kBq
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
plochy	plocha	k1gFnSc2	plocha
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
000	[number]	k4	000
km2	km2	k4	km2
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
,	,	kIx,	,
2	[number]	k4	2
000	[number]	k4	000
km2	km2	k4	km2
v	v	k7c6	v
Ruské	ruský	k2eAgFnSc6d1	ruská
federaci	federace	k1gFnSc6	federace
a	a	k8xC	a
1	[number]	k4	1
500	[number]	k4	500
km2	km2	k4	km2
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
250	[number]	k4	250
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc4	jejich
zprávy	zpráva	k1gFnPc4	zpráva
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
International	International	k1gMnSc1	International
Chernobyl	Chernobyl	k1gInSc4	Chernobyl
Project	Project	k2eAgInSc4d1	Project
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
agentury	agentura	k1gFnPc4	agentura
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
dříve	dříve	k6eAd2	dříve
zcela	zcela	k6eAd1	zcela
evakuovaná	evakuovaný	k2eAgFnSc1d1	evakuovaná
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
elektrárny	elektrárna	k1gFnSc2	elektrárna
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
zóny	zóna	k1gFnPc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
první	první	k4xOgFnSc6	první
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
600	[number]	k4	600
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
dobrovolně	dobrovolně	k6eAd1	dobrovolně
vrátili	vrátit	k5eAaPmAgMnP	vrátit
a	a	k8xC	a
dostávají	dostávat	k5eAaImIp3nP	dostávat
peněžní	peněžní	k2eAgInSc4d1	peněžní
příspěvek	příspěvek	k1gInSc4	příspěvek
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
také	také	k9	také
dovoz	dovoz	k1gInSc1	dovoz
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
nezamořených	zamořený	k2eNgFnPc2d1	zamořený
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
zóny	zóna	k1gFnSc2	zóna
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
jen	jen	k9	jen
vědci	vědec	k1gMnPc1	vědec
a	a	k8xC	a
exkurze	exkurze	k1gFnPc1	exkurze
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
udávané	udávaný	k2eAgInPc4d1	udávaný
počty	počet	k1gInPc4	počet
postižených	postižený	k1gMnPc2	postižený
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
organizace	organizace	k1gFnSc1	organizace
je	být	k5eAaImIp3nS	být
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejoptimističtějším	optimistický	k2eAgInSc6d3	nejoptimističtější
kraji	kraj	k1gInSc6	kraj
spektra	spektrum	k1gNnSc2	spektrum
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zprávy	zpráva	k1gFnPc1	zpráva
vydávané	vydávaný	k2eAgFnSc2d1	vydávaná
IAEA	IAEA	kA	IAEA
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mírným	mírný	k2eAgInSc7d1	mírný
odstupem	odstup	k1gInSc7	odstup
následuje	následovat	k5eAaImIp3nS	následovat
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
UNSCEAR	UNSCEAR	kA	UNSCEAR
(	(	kIx(	(
<g/>
komise	komise	k1gFnSc2	komise
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
efektů	efekt	k1gInPc2	efekt
radiace	radiace	k1gFnSc2	radiace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
uvádějí	uvádět	k5eAaImIp3nP	uvádět
větší	veliký	k2eAgInPc1d2	veliký
počty	počet	k1gInPc1	počet
postižených	postižený	k1gMnPc2	postižený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
řádově	řádově	k6eAd1	řádově
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
řádově	řádově	k6eAd1	řádově
vyšší	vysoký	k2eAgInPc1d2	vyšší
počty	počet	k1gInPc1	počet
postižených	postižený	k1gMnPc2	postižený
uvádějí	uvádět	k5eAaImIp3nP	uvádět
studie	studie	k1gFnPc1	studie
postsovětských	postsovětský	k2eAgMnPc2d1	postsovětský
výzkumníků	výzkumník	k1gMnPc2	výzkumník
<g/>
,	,	kIx,	,
studie	studie	k1gFnPc1	studie
německé	německý	k2eAgFnSc2d1	německá
sekce	sekce	k1gFnSc2	sekce
organizace	organizace	k1gFnSc2	organizace
lékařů	lékař	k1gMnPc2	lékař
pro	pro	k7c4	pro
zamezení	zamezení	k1gNnSc4	zamezení
jaderné	jaderný	k2eAgInPc4d1	jaderný
válce	válec	k1gInPc4	válec
(	(	kIx(	(
<g/>
IPPNW	IPPNW	kA	IPPNW
<g/>
)	)	kIx)	)
a	a	k8xC	a
německé	německý	k2eAgFnSc2d1	německá
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
radiační	radiační	k2eAgFnSc4d1	radiační
ochranu	ochrana	k1gFnSc4	ochrana
(	(	kIx(	(
<g/>
GfS	GfS	k1gFnSc4	GfS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
studie	studie	k1gFnPc4	studie
vypracované	vypracovaný	k2eAgFnPc4d1	vypracovaná
pro	pro	k7c4	pro
stranu	strana	k1gFnSc4	strana
Zelených	Zelená	k1gFnPc2	Zelená
<g/>
,	,	kIx,	,
či	či	k8xC	či
Greenpeace	Greenpeace	k1gFnSc1	Greenpeace
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
Legasov	Legasov	k1gInSc1	Legasov
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
vypracovaná	vypracovaný	k2eAgFnSc1d1	vypracovaná
pro	pro	k7c4	pro
Vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
konferenci	konference	k1gFnSc4	konference
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1986	[number]	k4	1986
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zemřou	zemřít	k5eAaPmIp3nP	zemřít
rakovinou	rakovina	k1gFnSc7	rakovina
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
izotopy	izotop	k1gInPc1	izotop
radiocesia	radiocesium	k1gNnSc2	radiocesium
na	na	k7c4	na
30	[number]	k4	30
000	[number]	k4	000
až	až	k9	až
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
IAEA	IAEA	kA	IAEA
označila	označit	k5eAaPmAgFnS	označit
předpověď	předpověď	k1gFnSc1	předpověď
za	za	k7c4	za
extrémně	extrémně	k6eAd1	extrémně
nadhodnocenou	nadhodnocený	k2eAgFnSc4d1	nadhodnocená
a	a	k8xC	a
stanovila	stanovit	k5eAaPmAgFnS	stanovit
max	max	kA	max
<g/>
.	.	kIx.	.
počet	počet	k1gInSc1	počet
na	na	k7c4	na
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
a	a	k8xC	a
5	[number]	k4	5
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
studie	studie	k1gFnSc2	studie
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Valerij	Valerij	k1gMnSc1	Valerij
Legasov	Legasov	k1gInSc4	Legasov
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
oběšený	oběšený	k2eAgMnSc1d1	oběšený
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
komise	komise	k1gFnSc1	komise
IAEA	IAEA	kA	IAEA
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
předložil	předložit	k5eAaPmAgInS	předložit
psychologický	psychologický	k2eAgInSc1d1	psychologický
dopad	dopad	k1gInSc1	dopad
havárie	havárie	k1gFnSc2	havárie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
eseji	esej	k1gInSc6	esej
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
adolescentů	adolescent	k1gMnPc2	adolescent
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
dospělých	dospělí	k1gMnPc2	dospělí
ze	z	k7c2	z
zasažených	zasažený	k2eAgFnPc2d1	zasažená
oblastí	oblast	k1gFnPc2	oblast
trpí	trpět	k5eAaImIp3nP	trpět
stresovými	stresový	k2eAgInPc7d1	stresový
symptomy	symptom	k1gInPc7	symptom
<g/>
,	,	kIx,	,
depresemi	deprese	k1gFnPc7	deprese
<g/>
,	,	kIx,	,
strachem	strach	k1gInSc7	strach
<g/>
,	,	kIx,	,
pocity	pocit	k1gInPc4	pocit
bezmoci	bezmoc	k1gFnSc2	bezmoc
<g/>
,	,	kIx,	,
slabosti	slabost	k1gFnSc2	slabost
a	a	k8xC	a
ztrátou	ztráta	k1gFnSc7	ztráta
životních	životní	k2eAgFnPc2d1	životní
vyhlídek	vyhlídka	k1gFnPc2	vyhlídka
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
referuje	referovat	k5eAaBmIp3nS	referovat
jako	jako	k9	jako
o	o	k7c6	o
obětech	oběť	k1gFnPc6	oběť
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
jako	jako	k9	jako
o	o	k7c6	o
přeživších	přeživší	k2eAgInPc6d1	přeživší
<g/>
,	,	kIx,	,
ústí	ústit	k5eAaImIp3nS	ústit
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
k	k	k7c3	k
přehnaným	přehnaný	k2eAgFnPc3d1	přehnaná
obavám	obava	k1gFnPc3	obava
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
zdraví	zdraví	k1gNnSc6	zdraví
a	a	k8xC	a
sebepozorování	sebepozorování	k1gNnSc6	sebepozorování
<g/>
,	,	kIx,	,
či	či	k8xC	či
k	k	k7c3	k
zcela	zcela	k6eAd1	zcela
bezohlednému	bezohledný	k2eAgNnSc3d1	bezohledné
chování	chování	k1gNnSc3	chování
vůči	vůči	k7c3	vůči
nim	on	k3xPp3gInPc3	on
samým	samý	k3xTgMnSc7	samý
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
konzumace	konzumace	k1gFnSc1	konzumace
lesních	lesní	k2eAgInPc2d1	lesní
plodů	plod	k1gInPc2	plod
z	z	k7c2	z
vysoce	vysoce	k6eAd1	vysoce
zamořených	zamořený	k2eAgFnPc2d1	zamořená
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
nadužívání	nadužívání	k1gNnSc1	nadužívání
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
tabáku	tabák	k1gInSc2	tabák
<g/>
,	,	kIx,	,
nechráněná	chráněný	k2eNgFnSc1d1	nechráněná
promiskuitní	promiskuitní	k2eAgFnSc1d1	promiskuitní
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Mettler	Mettler	k1gMnSc1	Mettler
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
tým	tým	k1gInSc1	tým
profesora	profesor	k1gMnSc2	profesor
Juryje	Juryje	k1gMnSc2	Juryje
Bandažeuského	Bandažeuský	k2eAgMnSc2d1	Bandažeuský
<g/>
,	,	kIx,	,
patologa	patolog	k1gMnSc2	patolog
a	a	k8xC	a
ředitele	ředitel	k1gMnSc2	ředitel
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
běloruském	běloruský	k2eAgInSc6d1	běloruský
kraji	kraj	k1gInSc6	kraj
Homel	Homela	k1gFnPc2	Homela
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
aktivitu	aktivita	k1gFnSc4	aktivita
radiocesia	radiocesium	k1gNnSc2	radiocesium
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
zemřelých	zemřelý	k1gMnPc2	zemřelý
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
potravě	potrava	k1gFnSc3	potrava
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
poznatky	poznatek	k1gInPc1	poznatek
ze	z	k7c2	z
studie	studie	k1gFnSc2	studie
<g/>
:	:	kIx,	:
Koncentrace	koncentrace	k1gFnSc1	koncentrace
radiocesia	radiocesia	k1gFnSc1	radiocesia
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
zelenině	zelenina	k1gFnSc6	zelenina
<g/>
,	,	kIx,	,
mléku	mléko	k1gNnSc6	mléko
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k6eAd1	zejména
lesních	lesní	k2eAgInPc6d1	lesní
plodech	plod	k1gInPc6	plod
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
konzumenti	konzument	k1gMnPc1	konzument
jsou	být	k5eAaImIp3nP	být
těžce	těžce	k6eAd1	těžce
zamoření	zamořený	k2eAgMnPc1d1	zamořený
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
do	do	k7c2	do
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
jsou	být	k5eAaImIp3nP	být
nejpostiženější	postižený	k2eAgFnPc1d3	nejpostiženější
<g/>
,	,	kIx,	,
aktivita	aktivita	k1gFnSc1	aktivita
radiocesia	radiocesia	k1gFnSc1	radiocesia
je	být	k5eAaImIp3nS	být
až	až	k9	až
11	[number]	k4	11
000	[number]	k4	000
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
ve	v	k7c6	v
slinivce	slinivka	k1gFnSc6	slinivka
<g/>
,	,	kIx,	,
6	[number]	k4	6
250	[number]	k4	250
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
ve	v	k7c6	v
štítné	štítný	k2eAgFnSc6d1	štítná
žláze	žláza	k1gFnSc6	žláza
<g/>
,	,	kIx,	,
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
5	[number]	k4	5
333	[number]	k4	333
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Účinným	účinný	k2eAgInSc7d1	účinný
transportním	transportní	k2eAgInSc7d1	transportní
prostředkem	prostředek	k1gInSc7	prostředek
pro	pro	k7c4	pro
radiocesium	radiocesium	k1gNnSc4	radiocesium
je	být	k5eAaImIp3nS	být
mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
zemřely	zemřít	k5eAaPmAgFnP	zemřít
na	na	k7c4	na
<g/>
:	:	kIx,	:
sepsi	sepse	k1gFnSc4	sepse
<g/>
,	,	kIx,	,
degeneraci	degenerace	k1gFnSc4	degenerace
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
hnisavé	hnisavý	k2eAgNnSc4d1	hnisavé
krvácení	krvácení	k1gNnSc4	krvácení
<g/>
,	,	kIx,	,
poruchu	porucha	k1gFnSc4	porucha
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
51	[number]	k4	51
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
10	[number]	k4	10
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
nalezli	nalézt	k5eAaBmAgMnP	nalézt
2	[number]	k4	2
až	až	k8xS	až
3	[number]	k4	3
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnPc4d2	veliký
koncentrace	koncentrace	k1gFnPc4	koncentrace
radiocesia	radiocesium	k1gNnSc2	radiocesium
než	než	k8xS	než
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
úrovně	úroveň	k1gFnPc1	úroveň
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
štítné	štítný	k2eAgFnSc6d1	štítná
žláze	žláza	k1gFnSc6	žláza
(	(	kIx(	(
<g/>
2	[number]	k4	2
054	[number]	k4	054
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nadledvinách	nadledvina	k1gFnPc6	nadledvina
(	(	kIx(	(
<g/>
1	[number]	k4	1
576	[number]	k4	576
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slinivce	slinivka	k1gFnSc6	slinivka
(	(	kIx(	(
<g/>
1	[number]	k4	1
359	[number]	k4	359
Bq	Bq	k1gFnPc2	Bq
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzorky	vzorek	k1gInPc1	vzorek
byly	být	k5eAaImAgInP	být
měřeny	měřit	k5eAaImNgInP	měřit
ukrajinskými	ukrajinský	k2eAgInPc7d1	ukrajinský
a	a	k8xC	a
německými	německý	k2eAgInPc7d1	německý
přístroji	přístroj	k1gInPc7	přístroj
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
byly	být	k5eAaImAgInP	být
vzorky	vzorek	k1gInPc1	vzorek
dvakrát	dvakrát	k6eAd1	dvakrát
přeměřeny	přeměřit	k5eAaPmNgInP	přeměřit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
školní	školní	k2eAgFnPc1d1	školní
děti	dítě	k1gFnPc1	dítě
posílány	posílat	k5eAaImNgFnP	posílat
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
na	na	k7c4	na
měsíční	měsíční	k2eAgInSc4d1	měsíční
ozdravný	ozdravný	k2eAgInSc4d1	ozdravný
pobyt	pobyt	k1gInSc4	pobyt
ze	z	k7c2	z
zasažené	zasažený	k2eAgFnSc2d1	zasažená
zóny	zóna	k1gFnSc2	zóna
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
jídelně	jídelna	k1gFnSc6	jídelna
dostávaly	dostávat	k5eAaImAgInP	dostávat
zdarma	zdarma	k6eAd1	zdarma
nekontaminované	kontaminovaný	k2eNgNnSc1d1	nekontaminované
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úsporných	úsporný	k2eAgInPc2d1	úsporný
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
sanatoriu	sanatorium	k1gNnSc6	sanatorium
zkrácen	zkrácen	k2eAgMnSc1d1	zkrácen
a	a	k8xC	a
některé	některý	k3yIgFnSc3	některý
kontaminované	kontaminovaný	k2eAgFnSc3d1	kontaminovaná
oblasti	oblast	k1gFnSc3	oblast
prohlášeny	prohlásit	k5eAaPmNgInP	prohlásit
za	za	k7c4	za
"	"	kIx"	"
<g/>
čisté	čistý	k2eAgInPc4d1	čistý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
dodávku	dodávka	k1gFnSc4	dodávka
dekontaminované	dekontaminovaný	k2eAgFnSc2d1	dekontaminovaná
školní	školní	k2eAgFnSc2d1	školní
stravy	strava	k1gFnSc2	strava
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
požadovala	požadovat	k5eAaImAgFnS	požadovat
pokračování	pokračování	k1gNnSc3	pokračování
jelikož	jelikož	k8xS	jelikož
kontaminovaná	kontaminovaný	k2eAgFnSc1d1	kontaminovaná
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
obdělávána	obděláván	k2eAgFnSc1d1	obdělávána
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
radiocesium	radiocesium	k1gNnSc1	radiocesium
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
době	době	k6eAd1	době
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
profesor	profesor	k1gMnSc1	profesor
Bandaževský	Bandaževský	k2eAgMnSc1d1	Bandaževský
ve	v	k7c6	v
výkonu	výkon	k1gInSc6	výkon
osmiletého	osmiletý	k2eAgInSc2d1	osmiletý
trestu	trest	k1gInSc2	trest
za	za	k7c4	za
údajné	údajný	k2eAgNnSc4d1	údajné
přijetí	přijetí	k1gNnSc4	přijetí
úplatku	úplatek	k1gInSc2	úplatek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
letech	let	k1gInPc6	let
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
protestů	protest	k1gInPc2	protest
a	a	k8xC	a
petic	petice	k1gFnPc2	petice
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
studii	studie	k1gFnSc6	studie
IAEA	IAEA	kA	IAEA
množství	množství	k1gNnSc6	množství
lidí	člověk	k1gMnPc2	člověk
kteří	který	k3yQgMnPc1	který
zemřou	zemřít	k5eAaPmIp3nP	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
katastrofou	katastrofa	k1gFnSc7	katastrofa
odhadováno	odhadován	k2eAgNnSc4d1	odhadováno
na	na	k7c4	na
9	[number]	k4	9
000	[number]	k4	000
až	až	k9	až
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jiná	jiný	k2eAgFnSc1d1	jiná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
Černobylu	Černobyl	k1gInSc6	Černobyl
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Fairlie	Fairlie	k1gFnSc1	Fairlie
<g/>
,	,	kIx,	,
Sumner	Sumner	k1gInSc1	Sumner
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
kteří	který	k3yIgMnPc1	který
zemřou	zemřít	k5eAaPmIp3nP	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
katastrofou	katastrofa	k1gFnSc7	katastrofa
na	na	k7c4	na
30	[number]	k4	30
000	[number]	k4	000
až	až	k9	až
60	[number]	k4	60
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
IAEA	IAEA	kA	IAEA
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pouze	pouze	k6eAd1	pouze
populací	populace	k1gFnSc7	populace
tří	tři	k4xCgInPc2	tři
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
zprávy	zpráva	k1gFnSc2	zpráva
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
spadu	spad	k1gInSc2	spad
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
nesovětských	sovětský	k2eNgNnPc6d1	sovětský
územích	území	k1gNnPc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
zpráv	zpráva	k1gFnPc2	zpráva
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
spadu	spad	k1gInSc2	spad
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinské	ukrajinský	k2eAgInPc1d1	ukrajinský
úřady	úřad	k1gInPc1	úřad
například	například	k6eAd1	například
uváděly	uvádět	k5eAaImAgInP	uvádět
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
výbuchem	výbuch	k1gInSc7	výbuch
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
přes	přes	k7c4	přes
125	[number]	k4	125
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
zase	zase	k9	zase
spočítalo	spočítat	k5eAaPmAgNnS	spočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
následky	následek	k1gInPc4	následek
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
výbuchem	výbuch	k1gInSc7	výbuch
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Černobylu	Černobyl	k1gInSc6	Černobyl
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
nebo	nebo	k8xC	nebo
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
zemře	zemřít	k5eAaPmIp3nS	zemřít
na	na	k7c4	na
200	[number]	k4	200
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
;	;	kIx,	;
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
anglický	anglický	k2eAgInSc1d1	anglický
překlad	překlad	k1gInSc1	překlad
velmi	velmi	k6eAd1	velmi
obsáhlé	obsáhlý	k2eAgFnSc2d1	obsáhlá
studie	studie	k1gFnSc2	studie
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
zásadní	zásadní	k2eAgFnSc7d1	zásadní
výpovědí	výpověď	k1gFnSc7	výpověď
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
na	na	k7c4	na
následky	následek	k1gInPc4	následek
havárie	havárie	k1gFnSc2	havárie
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
již	již	k6eAd1	již
985	[number]	k4	985
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
170	[number]	k4	170
000	[number]	k4	000
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1990	[number]	k4	1990
a	a	k8xC	a
2004	[number]	k4	2004
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
celková	celkový	k2eAgFnSc1d1	celková
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
o	o	k7c4	o
43	[number]	k4	43
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
i	i	k9	i
důsledkem	důsledek	k1gInSc7	důsledek
prudkého	prudký	k2eAgNnSc2d1	prudké
zhoršení	zhoršení	k1gNnSc2	zhoršení
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
a	a	k8xC	a
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
věci	věc	k1gFnPc1	věc
–	–	k?	–
vliv	vliv	k1gInSc1	vliv
radiace	radiace	k1gFnSc1	radiace
a	a	k8xC	a
pokles	pokles	k1gInSc1	pokles
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
–	–	k?	–
však	však	k9	však
žádná	žádný	k3yNgFnSc1	žádný
studie	studie	k1gFnSc1	studie
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
běžné	běžný	k2eAgFnPc1d1	běžná
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
i	i	k8xC	i
předtím	předtím	k6eAd1	předtím
spíše	spíše	k9	spíše
výjimečné	výjimečný	k2eAgInPc1d1	výjimečný
projevy	projev	k1gInPc1	projev
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
výživy	výživa	k1gFnSc2	výživa
a	a	k8xC	a
podvýživy	podvýživa	k1gFnSc2	podvýživa
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
V.	V.	kA	V.
Jablokov	Jablokov	k1gInSc1	Jablokov
(	(	kIx(	(
<g/>
ruská	ruský	k2eAgFnSc1d1	ruská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Nesterenko	Nesterenka	k1gFnSc5	Nesterenka
(	(	kIx(	(
<g/>
běloruský	běloruský	k2eAgInSc1d1	běloruský
institut	institut	k1gInSc1	institut
radiační	radiační	k2eAgFnSc2d1	radiační
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prof.	prof.	kA	prof.
V.	V.	kA	V.
Nesterenko	Nesterenka	k1gFnSc5	Nesterenka
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
běloruského	běloruský	k2eAgNnSc2d1	běloruské
jaderného	jaderný	k2eAgNnSc2d1	jaderné
střediska	středisko	k1gNnSc2	středisko
<g/>
)	)	kIx)	)
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
podklad	podklad	k1gInSc4	podklad
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
vědeckých	vědecký	k2eAgInPc2d1	vědecký
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
5	[number]	k4	5
000	[number]	k4	000
jiných	jiný	k2eAgFnPc2d1	jiná
publikací	publikace	k1gFnPc2	publikace
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
havárie	havárie	k1gFnSc2	havárie
a	a	k8xC	a
lékařských	lékařský	k2eAgInPc2d1	lékařský
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
upozornili	upozornit	k5eAaPmAgMnP	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc4	tento
vědecké	vědecký	k2eAgInPc4d1	vědecký
zdroje	zdroj	k1gInPc4	zdroj
byly	být	k5eAaImAgFnP	být
IAEA	IAEA	kA	IAEA
a	a	k8xC	a
UNSCEAR	UNSCEAR	kA	UNSCEAR
ignorovány	ignorovat	k5eAaImNgInP	ignorovat
či	či	k8xC	či
zlehčovány	zlehčovat	k5eAaImNgInP	zlehčovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analyzovaných	analyzovaný	k2eAgInPc6d1	analyzovaný
materiálech	materiál	k1gInPc6	materiál
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
spadu	spad	k1gInSc2	spad
zjištěn	zjistit	k5eAaPmNgInS	zjistit
nárůst	nárůst	k1gInSc1	nárůst
výskytu	výskyt	k1gInSc2	výskyt
<g/>
:	:	kIx,	:
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
:	:	kIx,	:
štítné	štítný	k2eAgFnSc2d1	štítná
žlázy	žláza	k1gFnSc2	žláza
leukémie	leukémie	k1gFnSc2	leukémie
ostatní	ostatní	k2eAgFnSc2d1	ostatní
rakoviny	rakovina	k1gFnSc2	rakovina
nerakovinných	rakovinný	k2eNgNnPc2d1	rakovinný
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
:	:	kIx,	:
nemoci	nemoc	k1gFnSc2	nemoc
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
ústrojí	ústrojí	k1gNnSc2	ústrojí
zažívacího	zažívací	k2eAgInSc2d1	zažívací
traktu	trakt	k1gInSc2	trakt
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
a	a	k8xC	a
kůže	kůže	k1gFnSc1	kůže
reprodukčních	reprodukční	k2eAgInPc2d1	reprodukční
orgánů	orgán	k1gInPc2	orgán
nervové	nervový	k2eAgFnSc2d1	nervová
a	a	k8xC	a
psychologické	psychologický	k2eAgFnSc2d1	psychologická
hormonální	hormonální	k2eAgFnSc2d1	hormonální
–	–	k?	–
způsobující	způsobující	k2eAgInSc4d1	způsobující
<g />
.	.	kIx.	.
</s>
<s>
aterosklerózu	ateroskleróza	k1gFnSc4	ateroskleróza
<g/>
,	,	kIx,	,
cukrovku	cukrovka	k1gFnSc4	cukrovka
<g/>
,	,	kIx,	,
obezitu	obezita	k1gFnSc4	obezita
abnormality	abnormalita	k1gFnSc2	abnormalita
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
–	–	k?	–
způsobující	způsobující	k2eAgFnSc1d1	způsobující
alergie	alergie	k1gFnSc1	alergie
<g/>
,	,	kIx,	,
infekce	infekce	k1gFnPc1	infekce
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
lymfocytech	lymfocyt	k1gInPc6	lymfocyt
genetické	genetický	k2eAgFnSc2d1	genetická
a	a	k8xC	a
chromozomální	chromozomální	k2eAgFnSc2d1	chromozomální
odchylky	odchylka	k1gFnSc2	odchylka
předčasné	předčasný	k2eAgNnSc4d1	předčasné
stárnutí	stárnutí	k1gNnSc4	stárnutí
postižení	postižení	k1gNnSc2	postižení
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
připomenout	připomenout	k5eAaPmF	připomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
výskyt	výskyt	k1gInSc1	výskyt
zhoubných	zhoubný	k2eAgFnPc2d1	zhoubná
i	i	k8xC	i
nezhoubných	zhoubný	k2eNgFnPc2d1	nezhoubná
forem	forma	k1gFnPc2	forma
rakoviny	rakovina	k1gFnSc2	rakovina
je	být	k5eAaImIp3nS	být
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
důsledkem	důsledek	k1gInSc7	důsledek
jejich	jejich	k3xOp3gNnSc2	jejich
cíleného	cílený	k2eAgNnSc2d1	cílené
vyhledávání	vyhledávání	k1gNnSc2	vyhledávání
v	v	k7c6	v
zasažené	zasažený	k2eAgFnSc6d1	zasažená
populaci	populace	k1gFnSc6	populace
–	–	k?	–
před	před	k7c7	před
havárií	havárie	k1gFnSc7	havárie
prostě	prostě	k9	prostě
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
značné	značný	k2eAgNnSc1d1	značné
procento	procento	k1gNnSc1	procento
nemocných	nemocný	k1gMnPc2	nemocný
bez	bez	k7c2	bez
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
potíží	potíž	k1gFnPc2	potíž
neodhaleno	odhalit	k5eNaPmNgNnS	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlubokém	hluboký	k2eAgInSc6d1	hluboký
kontrastu	kontrast	k1gInSc6	kontrast
k	k	k7c3	k
dopadům	dopad	k1gInPc3	dopad
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
populaci	populace	k1gFnSc4	populace
lze	lze	k6eAd1	lze
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
evakuace	evakuace	k1gFnSc2	evakuace
oblasti	oblast	k1gFnSc2	oblast
obklopující	obklopující	k2eAgFnSc4d1	obklopující
elektrárnu	elektrárna	k1gFnSc4	elektrárna
umožnila	umožnit	k5eAaPmAgFnS	umožnit
vytvoření	vytvoření	k1gNnSc3	vytvoření
bohaté	bohatý	k2eAgFnSc2d1	bohatá
a	a	k8xC	a
jedinečné	jedinečný	k2eAgFnSc2d1	jedinečná
rezervace	rezervace	k1gFnSc2	rezervace
divoké	divoký	k2eAgFnSc2d1	divoká
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
kontaminace	kontaminace	k1gFnSc1	kontaminace
spadem	spad	k1gInSc7	spad
nějaký	nějaký	k3yIgInSc4	nějaký
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
flóru	flóra	k1gFnSc4	flóra
a	a	k8xC	a
faunu	fauna	k1gFnSc4	fauna
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
zvířata	zvíře	k1gNnPc1	zvíře
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
významně	významně	k6eAd1	významně
liší	lišit	k5eAaImIp3nS	lišit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
radiační	radiační	k2eAgFnSc1d1	radiační
tolerance	tolerance	k1gFnSc1	tolerance
je	být	k5eAaImIp3nS	být
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
lidská	lidský	k2eAgFnSc1d1	lidská
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
kontaminované	kontaminovaný	k2eAgFnSc6d1	kontaminovaná
oblasti	oblast	k1gFnSc6	oblast
díky	díky	k7c3	díky
odstranění	odstranění	k1gNnSc3	odstranění
lidského	lidský	k2eAgInSc2d1	lidský
vlivu	vliv	k1gInSc2	vliv
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
samotné	samotný	k2eAgFnSc2d1	samotná
elektrárny	elektrárna	k1gFnSc2	elektrárna
katastrofou	katastrofa	k1gFnSc7	katastrofa
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
reaktoru	reaktor	k1gInSc2	reaktor
neskončily	skončit	k5eNaPmAgFnP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
vláda	vláda	k1gFnSc1	vláda
ponechala	ponechat	k5eAaPmAgFnS	ponechat
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
elektřiny	elektřina	k1gFnSc2	elektřina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
tři	tři	k4xCgInPc4	tři
zbývající	zbývající	k2eAgInPc4d1	zbývající
reaktory	reaktor	k1gInPc4	reaktor
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
poškodil	poškodit	k5eAaPmAgInS	poškodit
požár	požár	k1gInSc4	požár
kabelové	kabelový	k2eAgNnSc4d1	kabelové
vedení	vedení	k1gNnSc4	vedení
reaktoru	reaktor	k1gInSc2	reaktor
číslo	číslo	k1gNnSc4	číslo
2	[number]	k4	2
a	a	k8xC	a
odpovědní	odpovědný	k2eAgMnPc1d1	odpovědný
činitelé	činitel	k1gMnPc1	činitel
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
neopravitelně	opravitelně	k6eNd1	opravitelně
poškozen	poškozen	k2eAgInSc1d1	poškozen
a	a	k8xC	a
odpojili	odpojit	k5eAaPmAgMnP	odpojit
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Reaktor	reaktor	k1gInSc1	reaktor
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
byl	být	k5eAaImAgMnS	být
odstaven	odstavit	k5eAaPmNgMnS	odstavit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1996	[number]	k4	1996
jako	jako	k8xC	jako
část	část	k1gFnSc4	část
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
organizacemi	organizace	k1gFnPc7	organizace
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
IAEA	IAEA	kA	IAEA
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
činnosti	činnost	k1gFnSc2	činnost
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2000	[number]	k4	2000
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
prezident	prezident	k1gMnSc1	prezident
Leonid	Leonida	k1gFnPc2	Leonida
Kučma	kučma	k1gFnSc1	kučma
během	během	k7c2	během
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
zakončení	zakončení	k1gNnSc2	zakončení
provozu	provoz	k1gInSc2	provoz
osobně	osobně	k6eAd1	osobně
zmáčkl	zmáčknout	k5eAaPmAgInS	zmáčknout
vypínač	vypínač	k1gInSc1	vypínač
3	[number]	k4	3
<g/>
.	.	kIx.	.
reaktoru	reaktor	k1gInSc2	reaktor
a	a	k8xC	a
odstavil	odstavit	k5eAaPmAgMnS	odstavit
tím	ten	k3xDgNnSc7	ten
definitivně	definitivně	k6eAd1	definitivně
celou	celý	k2eAgFnSc4d1	celá
elektrárnu	elektrárna	k1gFnSc4	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Sarkofág	sarkofág	k1gInSc1	sarkofág
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
trvale	trvale	k6eAd1	trvale
účinně	účinně	k6eAd1	účinně
uzavřít	uzavřít	k5eAaPmF	uzavřít
zničený	zničený	k2eAgInSc4d1	zničený
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
reaktor	reaktor	k1gInSc4	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
chvatná	chvatný	k2eAgFnSc1d1	chvatná
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
prováděna	provádět	k5eAaImNgFnS	provádět
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
roboty	robot	k1gInPc7	robot
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	jeho	k3xOp3gFnSc2	jeho
rychlé	rychlý	k2eAgFnSc2d1	rychlá
stárnutí	stárnutí	k1gNnSc3	stárnutí
a	a	k8xC	a
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
uvolnit	uvolnit	k5eAaPmF	uvolnit
další	další	k2eAgInSc4d1	další
mrak	mrak	k1gInSc4	mrak
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
diskutováno	diskutovat	k5eAaImNgNnS	diskutovat
mnoho	mnoho	k4c1	mnoho
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
trvalejšího	trvalý	k2eAgNnSc2d2	trvalejší
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
realizaci	realizace	k1gFnSc4	realizace
však	však	k9	však
dosud	dosud	k6eAd1	dosud
brzdila	brzdit	k5eAaImAgFnS	brzdit
korupce	korupce	k1gFnSc1	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
peněz	peníze	k1gInPc2	peníze
věnovaných	věnovaný	k2eAgFnPc2d1	věnovaná
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
zeměmi	zem	k1gFnPc7	zem
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
byla	být	k5eAaImAgFnS	být
vyplýtvána	vyplýtvat	k5eAaPmNgFnS	vyplýtvat
neefektivním	efektivní	k2eNgNnSc7d1	neefektivní
rozvržením	rozvržení	k1gNnSc7	rozvržení
stavebních	stavební	k2eAgFnPc2d1	stavební
smluv	smlouva	k1gFnPc2	smlouva
a	a	k8xC	a
celkovým	celkový	k2eAgNnSc7d1	celkové
řízením	řízení	k1gNnSc7	řízení
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
jednoduše	jednoduše	k6eAd1	jednoduše
ukradena	ukraden	k2eAgFnSc1d1	ukradena
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
sarkofágem	sarkofág	k1gMnSc7	sarkofág
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
asi	asi	k9	asi
95	[number]	k4	95
%	%	kIx~	%
paliva	palivo	k1gNnSc2	palivo
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
asi	asi	k9	asi
18	[number]	k4	18
MCi	MCi	k1gFnPc2	MCi
=	=	kIx~	=
0,67	[number]	k4	0,67
EBq	EBq	k1gFnPc2	EBq
<g/>
.	.	kIx.	.
</s>
<s>
Radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
jádra	jádro	k1gNnSc2	jádro
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
lávě	láva	k1gFnSc6	láva
podobných	podobný	k2eAgMnPc2d1	podobný
"	"	kIx"	"
<g/>
palivo	palivo	k1gNnSc4	palivo
obsahujících	obsahující	k2eAgInPc2d1	obsahující
materiálů	materiál	k1gInPc2	materiál
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
FCM	FCM	kA	FCM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tekly	téct	k5eAaImAgFnP	téct
vrakem	vrak	k1gInSc7	vrak
budovy	budova	k1gFnSc2	budova
reaktoru	reaktor	k1gInSc2	reaktor
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
neztuhly	ztuhnout	k5eNaPmAgFnP	ztuhnout
do	do	k7c2	do
keramické	keramický	k2eAgFnSc2d1	keramická
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
střízlivých	střízlivý	k2eAgInPc2d1	střízlivý
odhadů	odhad	k1gInPc2	odhad
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
železobetonovým	železobetonový	k2eAgInSc7d1	železobetonový
obalem	obal	k1gInSc7	obal
nachází	nacházet	k5eAaImIp3nS	nacházet
nejméně	málo	k6eAd3	málo
4	[number]	k4	4
tuny	tuna	k1gFnPc4	tuna
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
betonu	beton	k1gInSc2	beton
pokrývajícího	pokrývající	k2eAgInSc2d1	pokrývající
reaktor	reaktor	k1gInSc1	reaktor
prosakuje	prosakovat	k5eAaImIp3nS	prosakovat
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
vyplavuje	vyplavovat	k5eAaImIp3nS	vyplavovat
radioaktivní	radioaktivní	k2eAgInPc4d1	radioaktivní
materiály	materiál	k1gInPc4	materiál
do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
vlhkost	vlhkost	k1gFnSc1	vlhkost
uvnitř	uvnitř	k7c2	uvnitř
krytu	kryt	k1gInSc2	kryt
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
erozi	eroze	k1gFnSc3	eroze
jeho	jeho	k3xOp3gFnSc2	jeho
ocelové	ocelový	k2eAgFnSc2d1	ocelová
konstrukce	konstrukce	k1gFnSc2	konstrukce
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
úniku	únik	k1gInSc3	únik
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
se	se	k3xPyFc4	se
vyskytl	vyskytnout	k5eAaPmAgInS	vyskytnout
nový	nový	k2eAgInSc1d1	nový
nerost	nerost	k1gInSc1	nerost
–	–	k?	–
černobylit	černobylit	k1gInSc1	černobylit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
došlo	dojít	k5eAaPmAgNnS	dojít
pod	pod	k7c7	pod
tíhou	tíha	k1gFnSc7	tíha
sněhu	sníh	k1gInSc2	sníh
k	k	k7c3	k
propadnutí	propadnutí	k1gNnSc3	propadnutí
střechy	střecha	k1gFnSc2	střecha
turbínové	turbínový	k2eAgFnSc2d1	turbínová
haly	hala	k1gFnSc2	hala
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
dělníci	dělník	k1gMnPc1	dělník
byli	být	k5eAaImAgMnP	být
evakuováni	evakuovat	k5eAaBmNgMnP	evakuovat
kvůli	kvůli	k7c3	kvůli
potenciálnímu	potenciální	k2eAgNnSc3d1	potenciální
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
úniku	únik	k1gInSc2	únik
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
však	však	k9	však
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
úniku	únik	k1gInSc3	únik
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
časem	čas	k1gInSc7	čas
Evropská	evropský	k2eAgFnSc1d1	Evropská
banka	banka	k1gFnSc1	banka
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
zahájila	zahájit	k5eAaPmAgFnS	zahájit
projekt	projekt	k1gInSc4	projekt
"	"	kIx"	"
<g/>
Úkryt	úkryt	k1gInSc4	úkryt
<g/>
"	"	kIx"	"
–	–	k?	–
stavbu	stavba	k1gFnSc4	stavba
nového	nový	k2eAgInSc2d1	nový
sarkofágu	sarkofág	k1gInSc2	sarkofág
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
přispívá	přispívat	k5eAaImIp3nS	přispívat
28	[number]	k4	28
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
nového	nový	k2eAgInSc2d1	nový
dvouplášťového	dvouplášťový	k2eAgInSc2d1	dvouplášťový
krytu	kryt	k1gInSc2	kryt
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zamezení	zamezení	k1gNnSc1	zamezení
úniku	únik	k1gInSc2	únik
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
starého	starý	k2eAgInSc2d1	starý
sarkofágu	sarkofág	k1gInSc2	sarkofág
před	před	k7c7	před
povětrnostními	povětrnostní	k2eAgInPc7d1	povětrnostní
vlivy	vliv	k1gInPc7	vliv
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
jeho	jeho	k3xOp3gNnSc2	jeho
budoucího	budoucí	k2eAgNnSc2d1	budoucí
robotického	robotický	k2eAgNnSc2d1	robotické
rozebrání	rozebrání	k1gNnSc2	rozebrání
<g/>
;	;	kIx,	;
konstrukce	konstrukce	k1gFnSc1	konstrukce
však	však	k9	však
plně	plně	k6eAd1	plně
nenahrazuje	nahrazovat	k5eNaImIp3nS	nahrazovat
funkci	funkce	k1gFnSc4	funkce
železobetonového	železobetonový	k2eAgInSc2d1	železobetonový
kontejnmentu	kontejnment	k1gInSc2	kontejnment
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvek	příspěvek	k1gInSc1	příspěvek
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
byl	být	k5eAaImAgInS	být
zpočátku	zpočátku	k6eAd1	zpočátku
paradoxně	paradoxně	k6eAd1	paradoxně
tak	tak	k6eAd1	tak
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměla	mít	k5eNaImAgFnS	mít
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgFnPc7d1	ostatní
ani	ani	k8xC	ani
hlasovací	hlasovací	k2eAgNnPc4d1	hlasovací
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
navýšila	navýšit	k5eAaPmAgFnS	navýšit
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
plnoprávným	plnoprávný	k2eAgMnSc7d1	plnoprávný
účastníkem	účastník	k1gMnSc7	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
má	mít	k5eAaImIp3nS	mít
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
1,55	[number]	k4	1,55
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současného	současný	k2eAgInSc2d1	současný
modelu	model	k1gInSc2	model
bude	být	k5eAaImBp3nS	být
projekt	projekt	k1gInSc1	projekt
financovat	financovat	k5eAaBmF	financovat
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
právě	právě	k6eAd1	právě
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
dobrovolní	dobrovolný	k2eAgMnPc1d1	dobrovolný
přispěvatelé	přispěvatel	k1gMnPc1	přispěvatel
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
<g/>
)	)	kIx)	)
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Evropské	evropský	k2eAgFnSc2d1	Evropská
banky	banka	k1gFnSc2	banka
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
v	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nebyla	být	k5eNaImAgFnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
celá	celý	k2eAgFnSc1d1	celá
částka	částka	k1gFnSc1	částka
<g/>
,	,	kIx,	,
pracovalo	pracovat	k5eAaImAgNnS	pracovat
se	se	k3xPyFc4	se
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2011	[number]	k4	2011
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
základů	základ	k1gInPc2	základ
nového	nový	k2eAgInSc2d1	nový
sarkofágu	sarkofág	k1gInSc2	sarkofág
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
kryt	kryt	k1gInSc1	kryt
je	být	k5eAaImIp3nS	být
oblouková	obloukový	k2eAgFnSc1d1	oblouková
konstrukce	konstrukce	k1gFnSc1	konstrukce
profilu	profil	k1gInSc2	profil
obrácené	obrácený	k2eAgFnSc2d1	obrácená
řetězovky	řetězovka	k1gFnSc2	řetězovka
vysoká	vysoká	k1gFnSc1	vysoká
105	[number]	k4	105
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
široká	široký	k2eAgFnSc1d1	široká
260	[number]	k4	260
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
mimo	mimo	k7c4	mimo
areál	areál	k1gInSc4	areál
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
nad	nad	k7c4	nad
stávající	stávající	k2eAgInSc4d1	stávající
sarkofág	sarkofág	k1gInSc4	sarkofág
<g/>
.	.	kIx.	.
</s>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
uzavření	uzavření	k1gNnSc2	uzavření
sarkofágu	sarkofág	k1gInSc2	sarkofág
novým	nový	k2eAgInSc7d1	nový
krytem	kryt	k1gInSc7	kryt
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
listopad	listopad	k1gInSc4	listopad
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Černobylská	černobylský	k2eAgFnSc1d1	Černobylská
havárie	havárie	k1gFnSc1	havárie
byla	být	k5eAaImAgFnS	být
ojedinělou	ojedinělý	k2eAgFnSc7d1	ojedinělá
událostí	událost	k1gFnSc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
komerční	komerční	k2eAgFnSc2d1	komerční
výroby	výroba	k1gFnSc2	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
nastala	nastat	k5eAaPmAgFnS	nastat
při	při	k7c6	při
havárii	havárie	k1gFnSc6	havárie
úmrtí	úmrtí	k1gNnSc2	úmrtí
přímo	přímo	k6eAd1	přímo
způsobená	způsobený	k2eAgFnSc1d1	způsobená
radiací	radiace	k1gFnSc7	radiace
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
havárie	havárie	k1gFnSc1	havárie
v	v	k7c6	v
přepracovacím	přepracovací	k2eAgInSc6d1	přepracovací
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
Tokaimuře	Tokaimura	k1gFnSc6	Tokaimura
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1999	[number]	k4	1999
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c4	v
smrt	smrt	k1gFnSc4	smrt
jednoho	jeden	k4xCgMnSc2	jeden
pracovníka	pracovník	k1gMnSc2	pracovník
na	na	k7c6	na
ozáření	ozáření	k1gNnSc6	ozáření
až	až	k9	až
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Havárie	havárie	k1gFnSc1	havárie
elektrárny	elektrárna	k1gFnSc2	elektrárna
A1	A1	k1gFnSc2	A1
v	v	k7c6	v
Jaslovských	Jaslovský	k2eAgFnPc6d1	Jaslovský
Bohunicích	Bohunice	k1gFnPc6	Bohunice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
měla	mít	k5eAaImAgFnS	mít
sice	sice	k8xC	sice
dvě	dva	k4xCgFnPc4	dva
oběti	oběť	k1gFnPc4	oběť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgFnP	být
udušeny	udušen	k2eAgMnPc4d1	udušen
uniklým	uniklý	k2eAgInSc7d1	uniklý
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
radioaktivitou	radioaktivita	k1gFnSc7	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhé	druhý	k4xOgFnSc6	druhý
havárii	havárie	k1gFnSc6	havárie
však	však	k9	však
k	k	k7c3	k
určitému	určitý	k2eAgInSc3d1	určitý
úniku	únik	k1gInSc3	únik
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
vrata	vrata	k1gNnPc4	vrata
hlavního	hlavní	k2eAgInSc2d1	hlavní
bloku	blok	k1gInSc2	blok
elektrárny	elektrárna	k1gFnSc2	elektrárna
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
22	[number]	k4	22
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1977	[number]	k4	1977
a	a	k8xC	a
již	již	k6eAd1	již
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neotevřela	otevřít	k5eNaPmAgFnS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Likvidace	likvidace	k1gFnSc1	likvidace
elektrárny	elektrárna	k1gFnSc2	elektrárna
A1	A1	k1gFnSc2	A1
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Počtem	počet	k1gInSc7	počet
zabitých	zabitý	k1gMnPc2	zabitý
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
havárie	havárie	k1gFnSc1	havárie
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
haváriemi	havárie	k1gFnPc7	havárie
přehrad	přehrada	k1gFnPc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
bylo	být	k5eAaImAgNnS	být
největší	veliký	k2eAgFnSc7d3	veliký
havárií	havárie	k1gFnSc7	havárie
přehrady	přehrada	k1gFnSc2	přehrada
zabito	zabít	k5eAaPmNgNnS	zabít
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
vlnou	vlna	k1gFnSc7	vlna
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
po	po	k7c6	po
sesuvu	sesuv	k1gInSc6	sesuv
půdy	půda	k1gFnSc2	půda
do	do	k7c2	do
přehrady	přehrada	k1gFnSc2	přehrada
Vajont	Vajonta	k1gFnPc2	Vajonta
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
neštěstí	neštěstí	k1gNnSc1	neštěstí
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
protržení	protržení	k1gNnSc6	protržení
několika	několik	k4yIc2	několik
hrází	hráz	k1gFnPc2	hráz
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
80	[number]	k4	80
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
přesná	přesný	k2eAgNnPc1d1	přesné
čísla	číslo	k1gNnPc1	číslo
byla	být	k5eAaImAgNnP	být
čínskými	čínský	k2eAgInPc7d1	čínský
úřady	úřad	k1gInPc7	úřad
utajena	utajit	k5eAaPmNgFnS	utajit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgFnPc1d1	čínská
přehrady	přehrada	k1gFnPc1	přehrada
mohou	moct	k5eAaImIp3nP	moct
posloužit	posloužit	k5eAaPmF	posloužit
také	také	k9	také
pro	pro	k7c4	pro
porovnání	porovnání	k1gNnSc4	porovnání
počtu	počet	k1gInSc2	počet
evakuovaných	evakuovaný	k2eAgInPc2d1	evakuovaný
–	–	k?	–
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
stavbě	stavba	k1gFnSc3	stavba
přehrady	přehrada	k1gFnSc2	přehrada
Tři	tři	k4xCgInPc4	tři
soutěsky	soutěsk	k1gInPc4	soutěsk
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k9	třeba
přesídlit	přesídlit	k5eAaPmF	přesídlit
asi	asi	k9	asi
700	[number]	k4	700
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Srovnání	srovnání	k1gNnSc1	srovnání
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
i	i	k9	i
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
z	z	k7c2	z
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
:	:	kIx,	:
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
zahynou	zahynout	k5eAaPmIp3nP	zahynout
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
při	při	k7c6	při
důlních	důlní	k2eAgFnPc6d1	důlní
haváriích	havárie	k1gFnPc6	havárie
desítky	desítka	k1gFnSc2	desítka
až	až	k9	až
stovky	stovka	k1gFnPc1	stovka
horníků	horník	k1gMnPc2	horník
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zavedením	zavedení	k1gNnSc7	zavedení
různých	různý	k2eAgFnPc2d1	různá
technologií	technologie	k1gFnPc2	technologie
na	na	k7c6	na
čištění	čištění	k1gNnSc6	čištění
dýmu	dým	k1gInSc2	dým
kyselé	kyselý	k2eAgInPc4d1	kyselý
deště	dešť	k1gInPc4	dešť
a	a	k8xC	a
spad	spad	k1gInSc4	spad
mírně	mírně	k6eAd1	mírně
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
popílku	popílek	k1gInSc2	popílek
jen	jen	k9	jen
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
zničily	zničit	k5eAaPmAgFnP	zničit
tisíce	tisíc	k4xCgInPc1	tisíc
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
lesů	les	k1gInPc2	les
a	a	k8xC	a
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
s	s	k7c7	s
účinkem	účinek	k1gInSc7	účinek
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
dřívější	dřívější	k2eAgFnSc3d1	dřívější
smrti	smrt	k1gFnSc3	smrt
statisíce	statisíce	k1gInPc4	statisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
