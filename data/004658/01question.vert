<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
sitcomu	sitcom	k1gInSc2	sitcom
Teorie	teorie	k1gFnSc2	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
herec	herec	k1gMnSc1	herec
Jim	on	k3xPp3gMnPc3	on
Parsons	Parsonsa	k1gFnPc2	Parsonsa
<g/>
?	?	kIx.	?
</s>
