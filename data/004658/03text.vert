<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Sheldon	Sheldon	k1gInSc4	Sheldon
Lee	Lea	k1gFnSc3	Lea
Cooper	Cooper	k1gInSc1	Cooper
<g/>
,	,	kIx,	,
B.	B.	kA	B.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
Sc	Sc	k1gFnSc1	Sc
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
sitcomu	sitcom	k1gInSc2	sitcom
Teorie	teorie	k1gFnSc2	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
herec	herec	k1gMnSc1	herec
Jim	on	k3xPp3gMnPc3	on
Parsons	Parsons	k1gInSc1	Parsons
<g/>
.	.	kIx.	.
</s>
<s>
Sheldon	Sheldon	k1gMnSc1	Sheldon
Cooper	Cooper	k1gMnSc1	Cooper
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nadaný	nadaný	k2eAgMnSc1d1	nadaný
a	a	k8xC	a
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
IQ	iq	kA	iq
187	[number]	k4	187
a	a	k8xC	a
2	[number]	k4	2
doktoráty	doktorát	k1gInPc7	doktorát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
kolegou	kolega	k1gMnSc7	kolega
a	a	k8xC	a
kamarádem	kamarád	k1gMnSc7	kamarád
Leonardem	Leonardo	k1gMnSc7	Leonardo
Hofstadterem	Hofstadter	k1gMnSc7	Hofstadter
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gMnPc4	on
často	často	k6eAd1	často
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
přátelé	přítel	k1gMnPc1	přítel
Howard	Howard	k1gMnSc1	Howard
Wolowitz	Wolowitz	k1gMnSc1	Wolowitz
a	a	k8xC	a
Rajesh	Rajesh	k1gMnSc1	Rajesh
Koothrappali	Koothrappali	k1gMnSc1	Koothrappali
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
posedlý	posedlý	k2eAgMnSc1d1	posedlý
pořádkem	pořádek	k1gInSc7	pořádek
<g/>
,	,	kIx,	,
miluje	milovat	k5eAaImIp3nS	milovat
komiksy	komiks	k1gInPc4	komiks
(	(	kIx(	(
<g/>
Superman	superman	k1gMnSc1	superman
<g/>
,	,	kIx,	,
Flash	Flash	k1gMnSc1	Flash
<g/>
,	,	kIx,	,
Aquaman	Aquaman	k1gMnSc1	Aquaman
a	a	k8xC	a
Green	Green	k2eAgMnSc1d1	Green
Lantern	Lantern	k1gMnSc1	Lantern
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sci-fi	scii	k1gFnSc1	sci-fi
seriály	seriál	k1gInPc4	seriál
(	(	kIx(	(
<g/>
Pán	pán	k1gMnSc1	pán
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
Firefly	Firefly	k1gFnSc4	Firefly
<g/>
,	,	kIx,	,
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
a	a	k8xC	a
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
však	však	k9	však
Babylon	Babylon	k1gInSc1	Babylon
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hraní	hraní	k1gNnSc4	hraní
konzolových	konzolový	k2eAgFnPc2d1	konzolová
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
Halo	halo	k1gNnSc1	halo
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
počítačových	počítačový	k2eAgInPc2d1	počítačový
(	(	kIx(	(
<g/>
Age	Age	k1gFnPc1	Age
of	of	k?	of
Conan	Conan	k1gMnSc1	Conan
<g/>
,	,	kIx,	,
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
)	)	kIx)	)
a	a	k8xC	a
paintballu	paintball	k1gInSc2	paintball
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
imunní	imunní	k2eAgMnSc1d1	imunní
vůči	vůči	k7c3	vůči
ironii	ironie	k1gFnSc3	ironie
a	a	k8xC	a
sarkasmu	sarkasmus	k1gInSc3	sarkasmus
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
Ryb	Ryby	k1gFnPc2	Ryby
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
ho	on	k3xPp3gMnSc4	on
Penny	penny	k1gFnSc1	penny
tipovala	tipovat	k5eAaImAgFnS	tipovat
na	na	k7c4	na
Býka	býk	k1gMnSc4	býk
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
města	město	k1gNnSc2	město
Galveston	Galveston	k1gInSc1	Galveston
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Texasu	Texas	k1gInSc6	Texas
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
malého	malý	k2eAgNnSc2d1	malé
ho	on	k3xPp3gInSc4	on
matka	matka	k1gFnSc1	matka
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
K-Mart	K-Marta	k1gFnPc2	K-Marta
upustila	upustit	k5eAaPmAgFnS	upustit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
vědcem	vědec	k1gMnSc7	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k8xS	jako
malý	malý	k2eAgInSc1d1	malý
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
nadaný	nadaný	k2eAgMnSc1d1	nadaný
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgInS	postavit
si	se	k3xPyFc3	se
například	například	k6eAd1	například
CT	CT	kA	CT
<g/>
,	,	kIx,	,
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
"	"	kIx"	"
<g/>
paprsek	paprsek	k1gInSc1	paprsek
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
a	a	k8xC	a
ve	v	k7c6	v
12	[number]	k4	12
letech	léto	k1gNnPc6	léto
chtěl	chtít	k5eAaImAgMnS	chtít
získat	získat	k5eAaPmF	získat
štěpný	štěpný	k2eAgInSc4d1	štěpný
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
postavit	postavit	k5eAaPmF	postavit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
jaderný	jaderný	k2eAgInSc4d1	jaderný
reaktor	reaktor	k1gInSc4	reaktor
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterého	který	k3yQgInSc2	který
chtěl	chtít	k5eAaImAgInS	chtít
zásobovat	zásobovat	k5eAaImF	zásobovat
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
vysoké	vysoký	k2eAgFnSc3d1	vysoká
inteligenci	inteligence	k1gFnSc3	inteligence
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
sociálních	sociální	k2eAgFnPc6d1	sociální
věcech	věc	k1gFnPc6	věc
naprosto	naprosto	k6eAd1	naprosto
"	"	kIx"	"
<g/>
nepoužitelný	použitelný	k2eNgMnSc1d1	nepoužitelný
<g/>
"	"	kIx"	"
–	–	k?	–
nechápe	chápat	k5eNaImIp3nS	chápat
ironii	ironie	k1gFnSc4	ironie
a	a	k8xC	a
sarkasmus	sarkasmus	k1gInSc4	sarkasmus
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vůbec	vůbec	k9	vůbec
důležité	důležitý	k2eAgNnSc1d1	důležité
kamarádit	kamarádit	k5eAaImF	kamarádit
se	se	k3xPyFc4	se
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
(	(	kIx(	(
<g/>
i	i	k8xC	i
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
dost	dost	k6eAd1	dost
svých	svůj	k3xOyFgMnPc2	svůj
tří	tři	k4xCgMnPc2	tři
přátel	přítel	k1gMnPc2	přítel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
straní	stranit	k5eAaImIp3nS	stranit
se	se	k3xPyFc4	se
sociálních	sociální	k2eAgFnPc2d1	sociální
interakcí	interakce	k1gFnPc2	interakce
a	a	k8xC	a
nechápe	chápat	k5eNaImIp3nS	chápat
mezilidské	mezilidský	k2eAgInPc4d1	mezilidský
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
se	se	k3xPyFc4	se
nezajímá	zajímat	k5eNaImIp3nS	zajímat
o	o	k7c4	o
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
upřímný	upřímný	k2eAgMnSc1d1	upřímný
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
veliké	veliký	k2eAgInPc4d1	veliký
problémy	problém	k1gInPc4	problém
udržet	udržet	k5eAaPmF	udržet
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
mu	on	k3xPp3gMnSc3	on
nedělá	dělat	k5eNaImIp3nS	dělat
problém	problém	k1gInSc1	problém
vytvářet	vytvářet	k5eAaImF	vytvářet
"	"	kIx"	"
<g/>
neprůstřelné	průstřelný	k2eNgFnPc4d1	neprůstřelná
<g/>
"	"	kIx"	"
lži	lež	k1gFnPc4	lež
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
až	až	k9	až
do	do	k7c2	do
extrémů	extrém	k1gInPc2	extrém
propracované	propracovaný	k2eAgFnSc2d1	propracovaná
zásady	zásada	k1gFnSc2	zásada
<g/>
,	,	kIx,	,
např.	např.	kA	např.
cereálie	cereálie	k1gFnPc1	cereálie
má	mít	k5eAaImIp3nS	mít
seřazené	seřazený	k2eAgNnSc1d1	seřazené
podle	podle	k7c2	podle
obsahu	obsah	k1gInSc2	obsah
vlákniny	vláknina	k1gFnSc2	vláknina
apod.	apod.	kA	apod.
Opravdu	opravdu	k6eAd1	opravdu
zbožňuje	zbožňovat	k5eAaImIp3nS	zbožňovat
asijskou	asijský	k2eAgFnSc4d1	asijská
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
,	,	kIx,	,
nejraději	rád	k6eAd3	rád
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
dává	dávat	k5eAaImIp3nS	dávat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
nejraději	rád	k6eAd3	rád
má	mít	k5eAaImIp3nS	mít
kuře	kuře	k1gNnSc4	kuře
na	na	k7c6	na
mandarinkách	mandarinka	k1gFnPc6	mandarinka
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
začal	začít	k5eAaPmAgInS	začít
restauraci	restaurace	k1gFnSc4	restaurace
podezřívat	podezřívat	k5eAaImF	podezřívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vydává	vydávat	k5eAaPmIp3nS	vydávat
za	za	k7c4	za
kuře	kuře	k1gNnSc4	kuře
na	na	k7c6	na
pomerančích	pomeranč	k1gInPc6	pomeranč
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgInSc3	ten
učit	učit	k5eAaImF	učit
čínštinu	čínština	k1gFnSc4	čínština
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povede	vést	k5eAaImIp3nS	vést
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
"	"	kIx"	"
<g/>
klasických	klasický	k2eAgInPc2d1	klasický
trefných	trefný	k2eAgInPc2d1	trefný
žertů	žert	k1gInPc2	žert
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
slovo	slovo	k1gNnSc1	slovo
bazinga	bazinga	k1gFnSc1	bazinga
(	(	kIx(	(
<g/>
Baryum	baryum	k1gNnSc1	baryum
<g/>
,	,	kIx,	,
Zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
Galium	galium	k1gNnSc1	galium
-BaZinGa	-BaZinG	k1gInSc2	-BaZinG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sheldonův	Sheldonův	k2eAgInSc1d1	Sheldonův
psychologický	psychologický	k2eAgInSc1d1	psychologický
profil	profil	k1gInSc1	profil
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
osobě	osoba	k1gFnSc3	osoba
postižené	postižený	k2eAgMnPc4d1	postižený
Aspergerovým	Aspergerův	k2eAgInSc7d1	Aspergerův
syndromem	syndrom	k1gInSc7	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Úzkostlivě	úzkostlivě	k6eAd1	úzkostlivě
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
přenosu	přenos	k1gInSc2	přenos
chorob	choroba	k1gFnPc2	choroba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc4	on
nesmí	smět	k5eNaImIp3nS	smět
nikdo	nikdo	k3yNnSc1	nikdo
dotýkat	dotýkat	k5eAaImF	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
to	ten	k3xDgNnSc1	ten
dovolí	dovolit	k5eAaPmIp3nS	dovolit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
jeho	jeho	k3xOp3gMnPc7	jeho
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
Amy	Amy	k1gFnSc1	Amy
Farrah	Farraha	k1gFnPc2	Farraha
Fowlerová	Fowlerová	k1gFnSc1	Fowlerová
<g/>
.	.	kIx.	.
</s>
<s>
Trpí	trpět	k5eAaImIp3nP	trpět
také	také	k9	také
mnoha	mnoho	k4c7	mnoho
fobiemi	fobie	k1gFnPc7	fobie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ornitofobií	ornitofobie	k1gFnPc2	ornitofobie
–	–	k?	–
strachem	strach	k1gInSc7	strach
z	z	k7c2	z
ptactva	ptactvo	k1gNnSc2	ptactvo
a	a	k8xC	a
výšek	výška	k1gFnPc2	výška
<g/>
,	,	kIx,	,
fobií	fobie	k1gFnSc7	fobie
z	z	k7c2	z
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
popsat	popsat	k5eAaPmF	popsat
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
situaci	situace	k1gFnSc4	situace
přesnou	přesný	k2eAgFnSc7d1	přesná
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
terminologií	terminologie	k1gFnSc7	terminologie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyznívá	vyznívat	k5eAaImIp3nS	vyznívat
komicky	komicky	k6eAd1	komicky
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
absenci	absence	k1gFnSc3	absence
sociálního	sociální	k2eAgNnSc2d1	sociální
cítění	cítění	k1gNnSc2	cítění
mu	on	k3xPp3gMnSc3	on
nepřijde	přijít	k5eNaPmIp3nS	přijít
divné	divný	k2eAgNnSc1d1	divné
dávat	dávat	k5eAaImF	dávat
často	často	k6eAd1	často
najevo	najevo	k6eAd1	najevo
své	svůj	k3xOyFgFnPc4	svůj
rozvinuté	rozvinutý	k2eAgFnPc4d1	rozvinutá
intelektuální	intelektuální	k2eAgFnPc4d1	intelektuální
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
své	svůj	k3xOyFgFnSc2	svůj
eidetické	eidetický	k2eAgFnSc2d1	eidetická
paměti	paměť	k1gFnSc2	paměť
si	se	k3xPyFc3	se
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
vše	všechen	k3xTgNnSc4	všechen
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Neumí	umět	k5eNaImIp3nS	umět
řídit	řídit	k5eAaImF	řídit
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
řízení	řízení	k1gNnSc1	řízení
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
pouze	pouze	k6eAd1	pouze
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
antropologové	antropolog	k1gMnPc1	antropolog
uznají	uznat	k5eAaPmIp3nP	uznat
<g/>
,	,	kIx,	,
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
nový	nový	k2eAgInSc4d1	nový
vývojový	vývojový	k2eAgInSc4d1	vývojový
stupeň	stupeň	k1gInSc4	stupeň
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
Homo	Homo	k6eAd1	Homo
Novus	Novus	k1gMnSc1	Novus
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nejhorší	zlý	k2eAgNnSc1d3	nejhorší
<g/>
,	,	kIx,	,
co	co	k8xS	co
mu	on	k3xPp3gMnSc3	on
můžete	moct	k5eAaImIp2nP	moct
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenávidíte	nenávidět	k5eAaImIp2nP	nenávidět
vlaky	vlak	k1gInPc1	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
Sheldonovu	Sheldonův	k2eAgFnSc4d1	Sheldonova
roli	role	k1gFnSc4	role
nominován	nominovat	k5eAaBmNgMnS	nominovat
jeho	jeho	k3xOp3gMnSc1	jeho
představitel	představitel	k1gMnSc1	představitel
Jim	on	k3xPp3gFnPc3	on
Parsons	Parsons	k1gInSc1	Parsons
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
následně	následně	k6eAd1	následně
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k6eAd1	půl
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Jim	on	k3xPp3gFnPc3	on
Parsons	Parsons	k1gInSc1	Parsons
za	za	k7c4	za
tutéž	týž	k3xTgFnSc4	týž
roli	role	k1gFnSc4	role
cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
–	–	k?	–
komediálním	komediální	k2eAgInPc3d1	komediální
nebo	nebo	k8xC	nebo
hudebním	hudební	k2eAgInPc3d1	hudební
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
