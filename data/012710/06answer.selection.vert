<s>
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
esperanta	esperanto	k1gNnSc2	esperanto
pochází	pocházet	k5eAaImIp3nS	pocházet
především	především	k9	především
ze	z	k7c2	z
západoevropských	západoevropský	k2eAgMnPc2d1	západoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
skladba	skladba	k1gFnSc1	skladba
a	a	k8xC	a
tvarosloví	tvarosloví	k1gNnSc4	tvarosloví
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
silný	silný	k2eAgInSc4d1	silný
slovanský	slovanský	k2eAgInSc4d1	slovanský
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
