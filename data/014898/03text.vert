<s>
Alexander	Alexandra	k1gFnPc2
Skene	Sken	k1gInSc5
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Skene	Sken	k1gInSc5
Alexander	Alexandra	k1gFnPc2
SkeneNarození	SkeneNarození	k1gNnPc5
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1837	#num#	k4
Fyvie	Fyvie	k1gFnSc1
<g/>
,	,	kIx,
Skotsko	Skotsko	k1gNnSc1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1900	#num#	k4
(	(	kIx(
<g/>
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Catskills	Catskills	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Torontská	torontský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
<g/>
Michiganská	michiganský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Obor	obor	k1gInSc1
</s>
<s>
gynekologie	gynekologie	k1gFnSc1
Známý	známý	k2eAgMnSc1d1
díky	díky	k7c3
</s>
<s>
Skeneho	Skeneze	k6eAd1
žlázy	žláza	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Johnston	Johnston	k1gInSc1
Chalmers	Chalmers	k1gInSc1
Skene	Sken	k1gInSc5
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1837	#num#	k4
Fyvie	Fyvie	k1gFnSc1
<g/>
,	,	kIx,
Skotsko	Skotsko	k1gNnSc1
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1900	#num#	k4
Catskills	Catskills	k1gInSc1
<g/>
,	,	kIx,
stát	stát	k1gInSc1
New	New	k1gFnPc2
York	York	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
skotský	skotský	k2eAgMnSc1d1
gynekolog	gynekolog	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
popsal	popsat	k5eAaPmAgMnS
část	část	k1gFnSc4
ženského	ženský	k2eAgNnSc2d1
pohlavního	pohlavní	k2eAgNnSc2d1
ústrojí	ústrojí	k1gNnSc2
<g/>
,	,	kIx,
známou	známý	k2eAgFnSc4d1
jako	jako	k8xS,k8xC
Skeneho	Skene	k1gMnSc4
žlázy	žláza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
Fyvie	Fyvie	k1gFnSc2
na	na	k7c6
severu	sever	k1gInSc6
Skotska	Skotsko	k1gNnSc2
a	a	k8xC
v	v	k7c6
devatenácti	devatenáct	k4xCc6
letech	léto	k1gNnPc6
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
medicínu	medicína	k1gFnSc4
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
v	v	k7c6
Torontu	Toronto	k1gNnSc6
a	a	k8xC
Michinganu	Michingan	k1gInSc6
a	a	k8xC
nakonec	nakonec	k6eAd1
v	v	k7c4
Long	Long	k1gInSc4
Island	Island	k1gInSc4
College	College	k1gFnPc2
Hospital	Hospital	k1gMnSc1
v	v	k7c6
Brooklynu	Brooklyn	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1863	#num#	k4
promoval	promovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vojenské	vojenský	k2eAgFnSc6d1
službě	služba	k1gFnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
soukromé	soukromý	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
v	v	k7c6
Brooklynu	Brooklyn	k1gInSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
ženských	ženský	k2eAgFnPc2d1
chorob	choroba	k1gFnPc2
na	na	k7c4
Long	Long	k1gInSc4
Island	Island	k1gInSc4
College	College	k1gFnPc2
Hospital	Hospital	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
přes	přes	k7c4
sto	sto	k4xCgNnSc4
lékařských	lékařský	k2eAgMnPc2d1
článků	článek	k1gInPc2
a	a	k8xC
několik	několik	k4yIc4
učebnic	učebnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílel	podílet	k5eAaImAgMnS
se	se	k3xPyFc4
na	na	k7c6
vývoji	vývoj	k1gInSc6
mnoha	mnoho	k4c2
chirurgických	chirurgický	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
a	a	k8xC
vylepšil	vylepšit	k5eAaPmAgInS
stávající	stávající	k2eAgInPc4d1
chirurgické	chirurgický	k2eAgInPc4d1
postupy	postup	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známý	známý	k1gMnSc1
je	být	k5eAaImIp3nS
především	především	k9
svým	svůj	k3xOyFgInSc7
popisem	popis	k1gInSc7
Skeneho	Skene	k1gMnSc2
žláz	žláza	k1gFnPc2
poblíž	poblíž	k7c2
močové	močový	k2eAgFnSc2d1
trubice	trubice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popsal	popsat	k5eAaPmAgMnS
také	také	k9
jejich	jejich	k3xOp3gFnSc4
infekci	infekce	k1gFnSc4
–	–	k?
skenitidu	skenitida	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
lékařství	lékařství	k1gNnSc4
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
rovněž	rovněž	k9
sochařství	sochařství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
sochař	sochař	k1gMnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
bustu	busta	k1gFnSc4
zakladatele	zakladatel	k1gMnSc2
americké	americký	k2eAgFnSc2d1
gynekologie	gynekologie	k1gFnSc2
J.	J.	kA
Mariona	Mariona	k1gFnSc1
Simse	Simse	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
hale	hala	k1gFnSc6
Kings	Kingsa	k1gFnPc2
County	Counta	k1gFnSc2
Medical	Medical	k1gFnSc2
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skeneho	Skeneha	k1gFnSc5
busta	busta	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c4
Prospect	Prospect	k2eAgInSc4d1
Park	park	k1gInSc4
Plaza	plaz	k1gMnSc2
(	(	kIx(
<g/>
také	také	k9
známé	známý	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
Grand	grand	k1gMnSc1
Army	Arma	k1gMnSc2
Plaza	plaz	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Skeneho	Skeneze	k6eAd1
žlázy	žláza	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Alexander	Alexandra	k1gFnPc2
Skene	Sken	k1gInSc5
v	v	k7c6
databázi	databáze	k1gFnSc6
Who	Who	k1gMnSc1
Named	Named	k1gMnSc1
It	It	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
114628831X	114628831X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6370	#num#	k4
8365	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
86839666	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
8883580	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
86839666	#num#	k4
</s>
