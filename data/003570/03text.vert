<s>
iOS	iOS	k?	iOS
je	být	k5eAaImIp3nS	být
mobilní	mobilní	k2eAgInSc4d1	mobilní
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
společností	společnost	k1gFnSc7	společnost
Apple	Apple	kA	Apple
Inc	Inc	k1gFnSc7	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
mobilních	mobilní	k2eAgNnPc6d1	mobilní
zařízeních	zařízení	k1gNnPc6	zařízení
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
<g/>
,	,	kIx,	,
iPad	iPad	k1gMnSc1	iPad
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
Apple	Apple	kA	Apple
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
iOS	iOS	k?	iOS
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
až	až	k9	až
od	od	k7c2	od
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
verze	verze	k1gFnSc2	verze
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
nazýván	nazývat	k5eAaImNgInS	nazývat
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
název	název	k1gInSc1	název
iOS	iOS	k?	iOS
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
politikou	politika	k1gFnSc7	politika
pojmenovávání	pojmenovávání	k1gNnSc2	pojmenovávání
produktů	produkt	k1gInPc2	produkt
(	(	kIx(	(
<g/>
iPod	iPod	k1gMnSc1	iPod
<g/>
,	,	kIx,	,
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
,	,	kIx,	,
iPad	iPad	k6eAd1	iPad
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
nového	nový	k2eAgInSc2d1	nový
názvu	název	k1gInSc2	název
iOS	iOS	k?	iOS
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Apple	Apple	kA	Apple
podána	podán	k2eAgFnSc1d1	podána
žaloba	žaloba	k1gFnSc1	žaloba
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Cisco	Cisco	k6eAd1	Cisco
Systems	Systems	k1gInSc4	Systems
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
název	název	k1gInSc1	název
IOS	IOS	kA	IOS
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
softwaru	software	k1gInSc2	software
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
routerech	router	k1gInPc6	router
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zabránila	zabránit	k5eAaPmAgFnS	zabránit
žalobě	žaloba	k1gFnSc3	žaloba
<g/>
,	,	kIx,	,
licencovala	licencovat	k5eAaBmAgFnS	licencovat
si	se	k3xPyFc3	se
společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
použití	použití	k1gNnSc2	použití
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
dotykového	dotykový	k2eAgInSc2d1	dotykový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
počátečním	počáteční	k2eAgNnSc6d1	počáteční
vydání	vydání	k1gNnSc6	vydání
nebylo	být	k5eNaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
žádné	žádný	k3yNgNnSc1	žádný
oficiální	oficiální	k2eAgNnSc1d1	oficiální
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
iPhone	iPhon	k1gInSc5	iPhon
software	software	k1gInSc1	software
development	development	k1gInSc1	development
kit	kit	k1gInSc4	kit
(	(	kIx(	(
<g/>
iPhone	iPhon	k1gMnSc5	iPhon
SDK	SDK	kA	SDK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Apple	Apple	kA	Apple
oficiálně	oficiálně	k6eAd1	oficiálně
systém	systém	k1gInSc4	systém
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jako	jako	k8xS	jako
iPhone	iPhon	k1gInSc5	iPhon
OS	OS	kA	OS
(	(	kIx(	(
<g/>
Systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
iOS	iOS	k?	iOS
<g/>
"	"	kIx"	"
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
hlavní	hlavní	k2eAgFnSc1d1	hlavní
verze	verze	k1gFnSc1	verze
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
iPhonu	iPhon	k1gInSc2	iPhon
3	[number]	k4	3
<g/>
G.	G.	kA	G.
Zařízení	zařízení	k1gNnSc1	zařízení
běžící	běžící	k2eAgFnSc7d1	běžící
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
lze	lze	k6eAd1	lze
upgradovat	upgradovat	k5eAaImF	upgradovat
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
systému	systém	k1gInSc2	systém
přináší	přinášet	k5eAaImIp3nS	přinášet
App	App	k1gMnSc5	App
Store	Stor	k1gMnSc5	Stor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
aplikace	aplikace	k1gFnPc1	aplikace
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
<g/>
.	.	kIx.	.
3.0	[number]	k4	3.0
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
verze	verze	k1gFnSc1	verze
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
iOS	iOS	k?	iOS
vyšla	vyjít	k5eAaPmAgFnS	vyjít
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
přidává	přidávat	k5eAaImIp3nS	přidávat
funkce	funkce	k1gFnPc4	funkce
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
kopírování	kopírování	k1gNnSc1	kopírování
a	a	k8xC	a
vkládání	vkládání	k1gNnSc1	vkládání
a	a	k8xC	a
MMS	MMS	kA	MMS
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
nové	nový	k2eAgFnPc1d1	nová
vlastnosti	vlastnost	k1gFnPc1	vlastnost
nejsou	být	k5eNaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
první	první	k4xOgFnPc1	první
generace	generace	k1gFnSc1	generace
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
běžící	běžící	k2eAgNnSc1d1	běžící
na	na	k7c6	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
lze	lze	k6eAd1	lze
upgradovat	upgradovat	k5eAaImF	upgradovat
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
4	[number]	k4	4
byl	být	k5eAaImAgInS	být
zpřístupněn	zpřístupněn	k2eAgInSc1d1	zpřístupněn
veřejnosti	veřejnost	k1gFnSc3	veřejnost
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
jednoduše	jednoduše	k6eAd1	jednoduše
na	na	k7c4	na
"	"	kIx"	"
<g/>
iOS	iOS	k?	iOS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
podporu	podpora	k1gFnSc4	podpora
některým	některý	k3yIgNnPc3	některý
zařízením	zařízení	k1gNnPc3	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
uživatelé	uživatel	k1gMnPc1	uživatel
iPodů	iPod	k1gMnPc2	iPod
Touch	Touch	k1gInSc1	Touch
nemusí	muset	k5eNaImIp3nS	muset
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gMnSc5	iPhon
3G	[number]	k4	3G
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
omezené	omezený	k2eAgFnPc1d1	omezená
funkce	funkce	k1gFnPc1	funkce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
možnosti	možnost	k1gFnSc2	možnost
multitaskingu	multitasking	k1gInSc2	multitasking
a	a	k8xC	a
možnosti	možnost	k1gFnSc2	možnost
nastavit	nastavit	k5eAaPmF	nastavit
tapetu	tapeta	k1gFnSc4	tapeta
na	na	k7c6	na
domovské	domovský	k2eAgFnSc6d1	domovská
obrazovce	obrazovka	k1gFnSc6	obrazovka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
iPhone	iPhon	k1gMnSc5	iPhon
4	[number]	k4	4
<g/>
,	,	kIx,	,
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
GS	GS	kA	GS
<g/>
,	,	kIx,	,
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
funkce	funkce	k1gFnPc1	funkce
aktivovány	aktivován	k2eAgFnPc1d1	aktivována
<g/>
.	.	kIx.	.
iPhone	iPhon	k1gInSc5	iPhon
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
nepodporují	podporovat	k5eNaImIp3nP	podporovat
iOS	iOS	k?	iOS
4.0	[number]	k4	4.0
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
4.2	[number]	k4	4.2
<g/>
.1	.1	k4	.1
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc4d1	vydaný
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
podporu	podpora	k1gFnSc4	podpora
iPadu	iPad	k1gInSc2	iPad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
poslední	poslední	k2eAgFnSc1d1	poslední
verze	verze	k1gFnSc1	verze
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
iPhonem	iPhon	k1gInSc7	iPhon
3G	[number]	k4	3G
a	a	k8xC	a
iPodem	iPod	k1gMnSc7	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
5	[number]	k4	5
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
veřejnosti	veřejnost	k1gFnSc2	veřejnost
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
a	a	k8xC	a
finálně	finálně	k6eAd1	finálně
vyšel	vyjít	k5eAaPmAgInS	vyjít
pro	pro	k7c4	pro
iPhone	iPhon	k1gInSc5	iPhon
3	[number]	k4	3
<g/>
GS	GS	kA	GS
<g/>
,	,	kIx,	,
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
(	(	kIx(	(
<g/>
GSM	GSM	kA	GSM
a	a	k8xC	a
CDMA	CDMA	kA	CDMA
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
&	&	k?	&
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
<g/>
,	,	kIx,	,
a	a	k8xC	a
iPad	iPad	k6eAd1	iPad
2	[number]	k4	2
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
5.1	[number]	k4	5.1
<g/>
.1	.1	k4	.1
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
verze	verze	k1gFnSc1	verze
iOS	iOS	k?	iOS
podporujcí	podporujcí	k1gMnSc1	podporujcí
iPad	iPad	k1gMnSc1	iPad
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
6	[number]	k4	6
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
při	při	k7c6	při
WWDC	WWDC	kA	WWDC
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
předchozích	předchozí	k2eAgFnPc2d1	předchozí
verzí	verze	k1gFnPc2	verze
iOS	iOS	k?	iOS
<g/>
,	,	kIx,	,
přestaly	přestat	k5eAaPmAgInP	přestat
být	být	k5eAaImF	být
některé	některý	k3yIgInPc1	některý
starší	starý	k2eAgInPc1d2	starší
přístroje	přístroj	k1gInPc1	přístroj
podporovány	podporovat	k5eAaImNgInP	podporovat
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
a	a	k8xC	a
iPad	iPad	k6eAd1	iPad
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podporované	podporovaný	k2eAgNnSc1d1	podporované
zařízení	zařízení	k1gNnSc1	zařízení
byly	být	k5eAaImAgInP	být
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
<g/>
;	;	kIx,	;
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
<g/>
;	;	kIx,	;
a	a	k8xC	a
iPad	iPad	k1gInSc1	iPad
2	[number]	k4	2
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
San	San	k1gMnSc6	San
Franciském	Franciský	k1gMnSc6	Franciský
Yerba	Yerba	k1gFnSc1	Yerba
Buena	Buena	k1gFnSc1	Buena
Center	centrum	k1gNnPc2	centrum
pro	pro	k7c4	pro
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
odhaleny	odhalen	k2eAgInPc4d1	odhalen
tři	tři	k4xCgInPc4	tři
<g />
.	.	kIx.	.
</s>
<s>
věci	věc	k1gFnPc4	věc
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
iOS	iOS	k?	iOS
<g/>
:	:	kIx,	:
další	další	k2eAgFnPc1d1	další
generace	generace	k1gFnPc1	generace
iPhone	iPhon	k1gInSc5	iPhon
5	[number]	k4	5
<g/>
,	,	kIx,	,
nový	nový	k2eAgMnSc1d1	nový
předělaný	předělaný	k2eAgMnSc1d1	předělaný
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
vydání	vydání	k1gNnSc4	vydání
iOS	iOS	k?	iOS
6	[number]	k4	6
příští	příští	k2eAgInSc4d1	příští
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
6	[number]	k4	6
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
vydán	vydán	k2eAgInSc1d1	vydán
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
iTunes	iTunes	k1gInSc4	iTunes
a	a	k8xC	a
přes	přes	k7c4	přes
OTA	Oto	k1gMnSc4	Oto
(	(	kIx(	(
<g/>
over-the-air	overheir	k1gMnSc1	over-the-air
<g/>
)	)	kIx)	)
aktualizace	aktualizace	k1gFnSc1	aktualizace
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
6.1	[number]	k4	6.1
<g/>
.6	.6	k4	.6
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
verze	verze	k1gFnSc1	verze
iOS	iOS	k?	iOS
podporující	podporující	k2eAgFnPc4d1	podporující
iPhone	iPhon	k1gInSc5	iPhon
3GS	[number]	k4	3GS
a	a	k8xC	a
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Apple	Apple	kA	Apple
představil	představit	k5eAaPmAgInS	představit
iOS	iOS	k?	iOS
7	[number]	k4	7
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
výroční	výroční	k2eAgFnSc6d1	výroční
konferenci	konference	k1gFnSc6	konference
Apple	Apple	kA	Apple
WWDC	WWDC	kA	WWDC
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydání	vydání	k1gNnSc1	vydání
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
oznámil	oznámit	k5eAaPmAgMnS	oznámit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgFnSc1d1	finální
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
společně	společně	k6eAd1	společně
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
iPhone	iPhon	k1gMnSc5	iPhon
5S	[number]	k4	5S
a	a	k8xC	a
iPhone	iPhon	k1gInSc5	iPhon
5C	[number]	k4	5C
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
iOS	iOS	k?	iOS
není	být	k5eNaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
iPhonem	iPhon	k1gInSc7	iPhon
3GS	[number]	k4	3GS
a	a	k8xC	a
iPodem	iPod	k1gMnSc7	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
podporované	podporovaný	k2eAgNnSc4d1	podporované
zařízení	zařízení	k1gNnSc4	zařízení
patří	patřit	k5eAaImIp3nS	patřit
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
a	a	k8xC	a
novější	nový	k2eAgFnSc2d2	novější
<g/>
,	,	kIx,	,
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
2	[number]	k4	2
a	a	k8xC	a
novější	nový	k2eAgInSc4d2	novější
a	a	k8xC	a
iPad	iPad	k6eAd1	iPad
mini	mini	k2eAgFnSc1d1	mini
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
a	a	k8xC	a
novější	nový	k2eAgInSc4d2	novější
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
7.1	[number]	k4	7.1
<g/>
.2	.2	k4	.2
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc7d1	poslední
verzí	verze	k1gFnSc7	verze
podporující	podporující	k2eAgFnSc4d1	podporující
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Apple	Apple	kA	Apple
představil	představit	k5eAaPmAgInS	představit
iOS	iOS	k?	iOS
8	[number]	k4	8
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
výroční	výroční	k2eAgFnSc6d1	výroční
konferenci	konference	k1gFnSc6	konference
Apple	Apple	kA	Apple
WWDC	WWDC	kA	WWDC
2014	[number]	k4	2014
a	a	k8xC	a
finální	finální	k2eAgNnSc4d1	finální
vydání	vydání	k1gNnPc4	vydání
oznámil	oznámit	k5eAaPmAgInS	oznámit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgFnSc1d1	finální
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
společně	společně	k6eAd1	společně
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
iPhone	iPhon	k1gMnSc5	iPhon
6	[number]	k4	6
a	a	k8xC	a
iPhone	iPhon	k1gInSc5	iPhon
6	[number]	k4	6
Plus	plus	k1gInSc1	plus
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
iOS	iOS	k?	iOS
8.1	[number]	k4	8.1
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vydána	vydán	k2eAgFnSc1d1	vydána
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
verzí	verze	k1gFnSc7	verze
Apple	Apple	kA	Apple
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
ukončování	ukončování	k1gNnSc2	ukončování
podpory	podpora	k1gFnSc2	podpora
nejstarších	starý	k2eAgFnPc2d3	nejstarší
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tentokrát	tentokrát	k6eAd1	tentokrát
byla	být	k5eAaImAgFnS	být
ukončena	ukončen	k2eAgFnSc1d1	ukončena
podpora	podpora	k1gFnSc1	podpora
jenom	jenom	k9	jenom
jednomu	jeden	k4xCgNnSc3	jeden
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
iPhonu	iPhona	k1gFnSc4	iPhona
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Podporovaná	podporovaný	k2eAgNnPc1d1	podporované
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
iPhone	iPhon	k1gInSc5	iPhon
4S	[number]	k4	4S
a	a	k8xC	a
novější	nový	k2eAgFnSc2d2	novější
<g/>
,	,	kIx,	,
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
2	[number]	k4	2
a	a	k8xC	a
novější	nový	k2eAgInSc4d2	novější
a	a	k8xC	a
iPad	iPad	k6eAd1	iPad
mini	mini	k2eAgFnSc1d1	mini
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
a	a	k8xC	a
novější	nový	k2eAgInSc4d2	novější
<g/>
.	.	kIx.	.
iPad	iPad	k1gInSc4	iPad
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
nejdéle	dlouho	k6eAd3	dlouho
podporované	podporovaný	k2eAgNnSc1d1	podporované
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
5	[number]	k4	5
verzí	verze	k1gFnPc2	verze
iOS	iOS	k?	iOS
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
iOS	iOS	k?	iOS
4	[number]	k4	4
<g/>
,	,	kIx,	,
iOS	iOS	k?	iOS
5	[number]	k4	5
<g/>
,	,	kIx,	,
iOS	iOS	k?	iOS
6	[number]	k4	6
<g/>
,	,	kIx,	,
iOS	iOS	k?	iOS
7	[number]	k4	7
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
iOS	iOS	k?	iOS
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Apple	Apple	kA	Apple
představil	představit	k5eAaPmAgInS	představit
iOS	iOS	k?	iOS
9	[number]	k4	9
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
výroční	výroční	k2eAgFnSc6d1	výroční
konferenci	konference	k1gFnSc6	konference
Apple	Apple	kA	Apple
WWDC	WWDC	kA	WWDC
2015	[number]	k4	2015
a	a	k8xC	a
systém	systém	k1gInSc1	systém
vydal	vydat	k5eAaPmAgInS	vydat
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
podporuje	podporovat	k5eAaImIp3nS	podporovat
všechna	všechen	k3xTgNnPc4	všechen
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
podporoval	podporovat	k5eAaImAgMnS	podporovat
iOS	iOS	k?	iOS
8	[number]	k4	8
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
iPhone	iPhon	k1gInSc5	iPhon
4S	[number]	k4	4S
a	a	k8xC	a
novější	nový	k2eAgFnSc2d2	novější
<g/>
,	,	kIx,	,
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
2	[number]	k4	2
a	a	k8xC	a
novější	nový	k2eAgInSc4d2	novější
a	a	k8xC	a
iPad	iPad	k6eAd1	iPad
mini	mini	k2eAgFnSc1d1	mini
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
a	a	k8xC	a
novější	nový	k2eAgInSc4d2	novější
<g/>
.	.	kIx.	.
</s>
<s>
Veze	vézt	k5eAaImIp3nS	vézt
9.3	[number]	k4	9.3
<g/>
.5	.5	k4	.5
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
aktualizace	aktualizace	k1gFnSc1	aktualizace
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnSc4	zařízení
iPhone	iPhon	k1gInSc5	iPhon
4	[number]	k4	4
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
2	[number]	k4	2
<g/>
,	,	kIx,	,
iPad	iPad	k1gInSc1	iPad
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc2	generace
<g/>
)	)	kIx)	)
a	a	k8xC	a
iPad	iPad	k6eAd1	iPad
mini	mini	k2eAgFnSc1d1	mini
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
iOS	iOS	k?	iOS
je	být	k5eAaImIp3nS	být
odlehčenou	odlehčený	k2eAgFnSc7d1	odlehčená
verzí	verze	k1gFnSc7	verze
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
macOS	macOS	k?	macOS
<g/>
,	,	kIx,	,
používaného	používaný	k2eAgNnSc2d1	používané
v	v	k7c6	v
počítačích	počítač	k1gInPc6	počítač
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
systém	systém	k1gInSc4	systém
UNIXového	unixový	k2eAgInSc2d1	unixový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
veškerou	veškerý	k3xTgFnSc4	veškerý
funkcionalitu	funkcionalita	k1gFnSc4	funkcionalita
OS	OS	kA	OS
X	X	kA	X
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ale	ale	k8xC	ale
přidává	přidávat	k5eAaImIp3nS	přidávat
podporu	podpora	k1gFnSc4	podpora
dotykového	dotykový	k2eAgNnSc2d1	dotykové
ovládání	ovládání	k1gNnSc2	ovládání
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
základní	základní	k2eAgFnPc4d1	základní
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
základní	základní	k2eAgFnSc4d1	základní
funkčnost	funkčnost	k1gFnSc4	funkčnost
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
vývojářům	vývojář	k1gMnPc3	vývojář
API	API	kA	API
a	a	k8xC	a
frameworky	frameworka	k1gFnSc2	frameworka
potřebné	potřebný	k2eAgFnSc2d1	potřebná
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
frameworky	frameworek	k1gInPc4	frameworek
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnPc1	technologie
dostupné	dostupný	k2eAgFnPc1d1	dostupná
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
pro	pro	k7c4	pro
implementaci	implementace	k1gFnSc4	implementace
grafického	grafický	k2eAgNnSc2d1	grafické
rozhraní	rozhraní	k1gNnSc2	rozhraní
aplikace	aplikace	k1gFnSc2	aplikace
a	a	k8xC	a
interakci	interakce	k1gFnSc4	interakce
s	s	k7c7	s
uživatelem	uživatel	k1gMnSc7	uživatel
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vysokoúrovňové	vysokoúrovňový	k2eAgFnPc4d1	vysokoúrovňová
systémové	systémový	k2eAgFnPc4d1	systémová
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
začínat	začínat	k5eAaImF	začínat
právě	právě	k9	právě
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
vrstvou	vrstva	k1gFnSc7	vrstva
a	a	k8xC	a
nižší	nízký	k2eAgMnSc1d2	nižší
používat	používat	k5eAaImF	používat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
verze	verze	k1gFnSc2	verze
iOS	iOS	k?	iOS
4.0	[number]	k4	4.0
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
psát	psát	k5eAaImF	psát
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
schopné	schopný	k2eAgFnPc1d1	schopná
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Stisk	stisk	k1gInSc1	stisk
tlačítka	tlačítko	k1gNnSc2	tlačítko
Home	Hom	k1gFnSc2	Hom
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
ukončení	ukončení	k1gNnSc4	ukončení
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
4.0	[number]	k4	4.0
sice	sice	k8xC	sice
nepřinesl	přinést	k5eNaPmAgInS	přinést
možnost	možnost	k1gFnSc4	možnost
plnohodnotného	plnohodnotný	k2eAgInSc2d1	plnohodnotný
běhu	běh	k1gInSc2	běh
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
provádění	provádění	k1gNnSc4	provádění
některých	některý	k3yIgFnPc2	některý
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
aplikace	aplikace	k1gFnSc1	aplikace
neběží	běžet	k5eNaImIp3nS	běžet
na	na	k7c6	na
popředí	popředí	k1gNnSc6	popředí
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnPc1	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ukládají	ukládat	k5eAaImIp3nP	ukládat
citlivá	citlivý	k2eAgNnPc4d1	citlivé
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
využívat	využívat	k5eAaImF	využívat
vestavěné	vestavěný	k2eAgFnPc4d1	vestavěná
podpory	podpora	k1gFnPc4	podpora
šifrování	šifrování	k1gNnSc2	šifrování
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
aplikace	aplikace	k1gFnSc1	aplikace
označí	označit	k5eAaPmIp3nS	označit
soubor	soubor	k1gInSc4	soubor
jako	jako	k8xC	jako
chráněný	chráněný	k2eAgInSc4d1	chráněný
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
ho	on	k3xPp3gInSc4	on
automaticky	automaticky	k6eAd1	automaticky
ukládá	ukládat	k5eAaImIp3nS	ukládat
na	na	k7c4	na
disk	disk	k1gInSc4	disk
v	v	k7c6	v
zašifrované	zašifrovaný	k2eAgFnSc6d1	zašifrovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
uzamčené	uzamčený	k2eAgNnSc1d1	uzamčené
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
souboru	soubor	k1gInSc2	soubor
je	být	k5eAaImIp3nS	být
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
jak	jak	k8xC	jak
aplikaci	aplikace	k1gFnSc6	aplikace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
případnému	případný	k2eAgMnSc3d1	případný
útočníkovi	útočník	k1gMnSc3	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
uživatel	uživatel	k1gMnSc1	uživatel
zařízení	zařízení	k1gNnSc4	zařízení
odemkne	odemknout	k5eAaPmIp3nS	odemknout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vygenerován	vygenerován	k2eAgInSc4d1	vygenerován
dešifrovací	dešifrovací	k2eAgInSc4d1	dešifrovací
klíč	klíč	k1gInSc4	klíč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
aplikaci	aplikace	k1gFnSc4	aplikace
umožní	umožnit	k5eAaPmIp3nS	umožnit
soubor	soubor	k1gInSc1	soubor
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
však	však	k9	však
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
iOS	iOS	k?	iOS
6.1	[number]	k4	6.1
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
odemknout	odemknout	k5eAaPmF	odemknout
a	a	k8xC	a
volat	volat	k5eAaImF	volat
i	i	k9	i
bez	bez	k7c2	bez
znalosti	znalost	k1gFnSc2	znalost
hesla	heslo	k1gNnSc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
3.0	[number]	k4	3.0
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
iOS	iOS	k?	iOS
posílání	posílání	k1gNnSc1	posílání
tzv.	tzv.	kA	tzv.
push	pusha	k1gFnPc2	pusha
notifikací	notifikace	k1gFnPc2	notifikace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
upozorňovat	upozorňovat	k5eAaImF	upozorňovat
uživatele	uživatel	k1gMnSc4	uživatel
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
daná	daný	k2eAgFnSc1d1	daná
aplikace	aplikace	k1gFnSc1	aplikace
právě	právě	k6eAd1	právě
spuštěna	spustit	k5eAaPmNgFnS	spustit
<g/>
.	.	kIx.	.
</s>
<s>
Uživateli	uživatel	k1gMnSc3	uživatel
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zobrazit	zobrazit	k5eAaPmF	zobrazit
krátkou	krátký	k2eAgFnSc4d1	krátká
textovou	textový	k2eAgFnSc4d1	textová
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
přehrát	přehrát	k5eAaPmF	přehrát
zvuk	zvuk	k1gInSc4	zvuk
či	či	k8xC	či
aktualizovat	aktualizovat	k5eAaBmF	aktualizovat
číselnou	číselný	k2eAgFnSc4d1	číselná
značku	značka	k1gFnSc4	značka
(	(	kIx(	(
<g/>
badge	badge	k1gFnSc1	badge
<g/>
,	,	kIx,	,
odznak	odznak	k1gInSc1	odznak
<g/>
)	)	kIx)	)
na	na	k7c6	na
ikoně	ikona	k1gFnSc6	ikona
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Iniciovat	iniciovat	k5eAaBmF	iniciovat
odeslání	odeslání	k1gNnPc4	odeslání
push	pusha	k1gFnPc2	pusha
notifikace	notifikace	k1gFnSc1	notifikace
musí	muset	k5eAaImIp3nS	muset
server	server	k1gInSc4	server
výrobce	výrobce	k1gMnSc2	výrobce
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
následně	následně	k6eAd1	následně
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
se	s	k7c7	s
servery	server	k1gInPc7	server
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nP	pokusit
o	o	k7c4	o
doručení	doručení	k1gNnSc4	doručení
na	na	k7c4	na
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
doručení	doručení	k1gNnSc4	doručení
není	být	k5eNaImIp3nS	být
garantováno	garantován	k2eAgNnSc1d1	garantováno
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
jeho	on	k3xPp3gInSc2	on
unikátního	unikátní	k2eAgInSc2d1	unikátní
identifikátoru	identifikátor	k1gInSc2	identifikátor
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
iOS	iOS	k?	iOS
4	[number]	k4	4
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc1d2	vyšší
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
mechanismus	mechanismus	k1gInSc4	mechanismus
push	push	k1gInSc1	push
notifikací	notifikace	k1gFnPc2	notifikace
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
lokální	lokální	k2eAgFnPc4d1	lokální
notifikace	notifikace	k1gFnPc4	notifikace
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
žádné	žádný	k3yNgNnSc4	žádný
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
serveru	server	k1gInSc2	server
a	a	k8xC	a
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
notifikacích	notifikace	k1gFnPc6	notifikace
ukládají	ukládat	k5eAaImIp3nP	ukládat
lokálně	lokálně	k6eAd1	lokálně
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
aktuálně	aktuálně	k6eAd1	aktuálně
běžící	běžící	k2eAgFnSc1d1	běžící
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
autonavigace	autonavigace	k1gFnSc2	autonavigace
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
upozornit	upozornit	k5eAaPmF	upozornit
uživatele	uživatel	k1gMnSc4	uživatel
na	na	k7c4	na
důležité	důležitý	k2eAgFnPc4d1	důležitá
události	událost	k1gFnPc4	událost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
o	o	k7c6	o
blížící	blížící	k2eAgFnSc6d1	blížící
se	se	k3xPyFc4	se
zatáčce	zatáčka	k1gFnSc6	zatáčka
<g/>
)	)	kIx)	)
ihned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
naplánovat	naplánovat	k5eAaBmF	naplánovat
notifikaci	notifikace	k1gFnSc4	notifikace
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
datum	datum	k1gInSc4	datum
a	a	k8xC	a
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
notifikace	notifikace	k1gFnSc1	notifikace
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
systému	systém	k1gInSc6	systém
a	a	k8xC	a
aplikace	aplikace	k1gFnSc2	aplikace
v	v	k7c4	v
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
čas	čas	k1gInSc4	čas
nemusí	muset	k5eNaImIp3nS	muset
běžet	běžet	k5eAaImF	běžet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
uživateli	uživatel	k1gMnSc3	uživatel
dané	daný	k2eAgNnSc1d1	dané
upozornění	upozornění	k1gNnSc1	upozornění
zobrazilo	zobrazit	k5eAaPmAgNnS	zobrazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
iOS	iOS	k?	iOS
před	před	k7c7	před
verzí	verze	k1gFnSc7	verze
3.2	[number]	k4	3.2
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ručně	ručně	k6eAd1	ručně
zachytávat	zachytávat	k5eAaImF	zachytávat
a	a	k8xC	a
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
dotykové	dotykový	k2eAgFnPc4d1	dotyková
události	událost	k1gFnPc4	událost
a	a	k8xC	a
komplikovaně	komplikovaně	k6eAd1	komplikovaně
rozpoznávat	rozpoznávat	k5eAaImF	rozpoznávat
gesta	gesto	k1gNnPc4	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
3.2	[number]	k4	3.2
však	však	k9	však
existuje	existovat	k5eAaImIp3nS	existovat
možnost	možnost	k1gFnSc4	možnost
systémového	systémový	k2eAgNnSc2d1	systémové
rozpoznávání	rozpoznávání	k1gNnSc2	rozpoznávání
definovaných	definovaný	k2eAgNnPc2d1	definované
gest	gesto	k1gNnPc2	gesto
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
předávání	předávání	k1gNnSc2	předávání
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
gesta	gesto	k1gNnPc1	gesto
jsou	být	k5eAaImIp3nP	být
ťuknutí	ťuknutí	k1gNnPc4	ťuknutí
(	(	kIx(	(
<g/>
možno	možno	k6eAd1	možno
i	i	k9	i
vícenásobné	vícenásobný	k2eAgFnPc1d1	vícenásobná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sevření	sevření	k1gNnSc1	sevření
a	a	k8xC	a
rozevření	rozevření	k1gNnSc1	rozevření
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
přetahování	přetahování	k1gNnSc2	přetahování
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
švihnutí	švihnutí	k1gNnSc1	švihnutí
(	(	kIx(	(
<g/>
swipe	swipat	k5eAaPmIp3nS	swipat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rotace	rotace	k1gFnSc1	rotace
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
stisk	stisk	k1gInSc1	stisk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nadefinovat	nadefinovat	k5eAaPmF	nadefinovat
rozpoznávání	rozpoznávání	k1gNnSc4	rozpoznávání
vlastních	vlastní	k2eAgNnPc2d1	vlastní
gest	gesto	k1gNnPc2	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
může	moct	k5eAaImIp3nS	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
svým	svůj	k3xOyFgNnPc3	svůj
datům	datum	k1gNnPc3	datum
pomocí	pomocí	k7c2	pomocí
programu	program	k1gInSc2	program
iTunes	iTunesa	k1gFnPc2	iTunesa
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc4	ten
aplikace	aplikace	k1gFnSc1	aplikace
umožní	umožnit	k5eAaPmIp3nS	umožnit
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
přes	přes	k7c4	přes
iTunes	iTunes	k1gInSc4	iTunes
nahrávat	nahrávat	k5eAaImF	nahrávat
soubory	soubor	k1gInPc4	soubor
do	do	k7c2	do
definované	definovaný	k2eAgFnSc2d1	definovaná
složky	složka	k1gFnSc2	složka
v	v	k7c4	v
aplikaci	aplikace	k1gFnSc4	aplikace
a	a	k8xC	a
soubory	soubor	k1gInPc4	soubor
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
naopak	naopak	k6eAd1	naopak
kopírovat	kopírovat	k5eAaImF	kopírovat
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
sdílení	sdílení	k1gNnSc4	sdílení
dokumentů	dokument	k1gInPc2	dokument
mezi	mezi	k7c7	mezi
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
3.0	[number]	k4	3.0
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
peer-to-peer	peeroeer	k1gInSc4	peer-to-peer
konektivitu	konektivita	k1gFnSc4	konektivita
mezi	mezi	k7c7	mezi
více	hodně	k6eAd2	hodně
zařízeními	zařízení	k1gNnPc7	zařízení
pomocí	pomocí	k7c2	pomocí
technologie	technologie	k1gFnSc2	technologie
Bluetooth	Bluetootha	k1gFnPc2	Bluetootha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkcionalita	funkcionalita	k1gFnSc1	funkcionalita
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
her	hra	k1gFnPc2	hra
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ji	on	k3xPp3gFnSc4	on
využít	využít	k5eAaPmF	využít
i	i	k9	i
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
frameworků	frameworek	k1gInPc2	frameworek
použitých	použitý	k2eAgInPc2d1	použitý
v	v	k7c6	v
systému	systém	k1gInSc6	systém
používá	používat	k5eAaImIp3nS	používat
standardizované	standardizovaný	k2eAgInPc4d1	standardizovaný
komponenty	komponent	k1gInPc4	komponent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zachování	zachování	k1gNnSc2	zachování
konzistentního	konzistentní	k2eAgInSc2d1	konzistentní
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
zážitku	zážitek	k1gInSc2	zážitek
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vhodné	vhodný	k2eAgNnSc1d1	vhodné
používat	používat	k5eAaImF	používat
tyto	tento	k3xDgInPc4	tento
standardizované	standardizovaný	k2eAgInPc4d1	standardizovaný
komponenty	komponent	k1gInPc4	komponent
i	i	k9	i
v	v	k7c6	v
aplikacích	aplikace	k1gFnPc6	aplikace
třetích	třetí	k4xOgInPc2	třetí
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInSc4d1	hlavní
controller	controller	k1gInSc4	controller
patří	patřit	k5eAaImIp3nS	patřit
Adresář	adresář	k1gInSc1	adresář
(	(	kIx(	(
<g/>
zobrazení	zobrazení	k1gNnSc1	zobrazení
kontaktních	kontaktní	k2eAgFnPc2d1	kontaktní
informací	informace	k1gFnPc2	informace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kalendář	kalendář	k1gInSc1	kalendář
<g/>
,	,	kIx,	,
Psaní	psaní	k1gNnSc1	psaní
e-mailu	eail	k1gInSc2	e-mail
<g/>
/	/	kIx~	/
<g/>
SMS	SMS	kA	SMS
<g/>
,	,	kIx,	,
Otevření	otevření	k1gNnSc1	otevření
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
Výběr	výběr	k1gInSc1	výběr
obrázku	obrázek	k1gInSc2	obrázek
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
/	/	kIx~	/
<g/>
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
apod.	apod.	kA	apod.
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
3.2	[number]	k4	3.2
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
připojit	připojit	k5eAaPmF	připojit
pomocí	pomocí	k7c2	pomocí
speciálního	speciální	k2eAgNnSc2d1	speciální
příslušenství	příslušenství	k1gNnSc2	příslušenství
externí	externí	k2eAgNnSc4d1	externí
zobrazovací	zobrazovací	k2eAgNnSc4d1	zobrazovací
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
externí	externí	k2eAgNnSc1d1	externí
zařízení	zařízení	k1gNnSc1	zařízení
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
druhé	druhý	k4xOgNnSc4	druhý
okno	okno	k1gNnSc4	okno
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
aplikace	aplikace	k1gFnSc1	aplikace
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
obsah	obsah	k1gInSc4	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
používat	používat	k5eAaImF	používat
režim	režim	k1gInSc4	režim
zrcadlení	zrcadlení	k1gNnSc2	zrcadlení
(	(	kIx(	(
<g/>
mirror	mirror	k1gInSc1	mirror
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
stejný	stejný	k2eAgInSc1d1	stejný
obsah	obsah	k1gInSc1	obsah
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
oken	okno	k1gNnPc2	okno
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytváření	vytváření	k1gNnSc4	vytváření
graficky	graficky	k6eAd1	graficky
a	a	k8xC	a
zvukově	zvukově	k6eAd1	zvukově
propracovaných	propracovaný	k2eAgFnPc2d1	propracovaná
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
technologie	technologie	k1gFnPc1	technologie
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
plynulé	plynulý	k2eAgNnSc4d1	plynulé
přehrávání	přehrávání	k1gNnSc4	přehrávání
animací	animace	k1gFnPc2	animace
<g/>
,	,	kIx,	,
videí	video	k1gNnPc2	video
a	a	k8xC	a
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
grafika	grafika	k1gFnSc1	grafika
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
systému	systém	k1gInSc2	systém
iOS	iOS	k?	iOS
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednoduššího	jednoduchý	k2eAgMnSc2d3	nejjednodušší
a	a	k8xC	a
nejefektivnějšího	efektivní	k2eAgNnSc2d3	nejefektivnější
vytváření	vytváření	k1gNnSc2	vytváření
aplikací	aplikace	k1gFnPc2	aplikace
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
používáním	používání	k1gNnSc7	používání
standardních	standardní	k2eAgInPc2d1	standardní
předrenderovaných	předrenderovaný	k2eAgInPc2d1	předrenderovaný
obrázků	obrázek	k1gInPc2	obrázek
a	a	k8xC	a
komponent	komponenta	k1gFnPc2	komponenta
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vše	všechen	k3xTgNnSc4	všechen
obstaral	obstarat	k5eAaPmAgMnS	obstarat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
situacích	situace	k1gFnPc6	situace
není	být	k5eNaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
situacích	situace	k1gFnPc6	situace
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
následující	následující	k2eAgFnPc4d1	následující
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
technologie	technologie	k1gFnPc1	technologie
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přehrávat	přehrávat	k5eAaImF	přehrávat
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
audiozáznamy	audiozáznam	k1gInPc4	audiozáznam
a	a	k8xC	a
používat	používat	k5eAaImF	používat
vibrace	vibrace	k1gFnPc4	vibrace
(	(	kIx(	(
<g/>
na	na	k7c6	na
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
to	ten	k3xDgNnSc1	ten
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
nabízí	nabízet	k5eAaImIp3nS	nabízet
několik	několik	k4yIc4	několik
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
přehrávat	přehrávat	k5eAaImF	přehrávat
či	či	k8xC	či
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Vysokoúrovňové	vysokoúrovňový	k2eAgFnPc1d1	vysokoúrovňová
frameworky	frameworka	k1gFnPc1	frameworka
velice	velice	k6eAd1	velice
zjednodušují	zjednodušovat	k5eAaImIp3nP	zjednodušovat
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
takovou	takový	k3xDgFnSc4	takový
míru	míra	k1gFnSc4	míra
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
frameworky	frameworek	k1gInPc1	frameworek
jsou	být	k5eAaImIp3nP	být
seřazeny	seřadit	k5eAaPmNgInP	seřadit
a	a	k8xC	a
od	od	k7c2	od
vysokoúrovňových	vysokoúrovňový	k2eAgInPc2d1	vysokoúrovňový
po	po	k7c6	po
nízkoúrovňové	nízkoúrovňová	k1gFnSc6	nízkoúrovňová
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Media	medium	k1gNnPc1	medium
Player	Player	k1gInSc1	Player
framework	framework	k1gInSc1	framework
–	–	k?	–
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
iTunes	iTunes	k1gMnSc1	iTunes
knihovně	knihovně	k6eAd1	knihovně
a	a	k8xC	a
přehrávaní	přehrávaný	k2eAgMnPc1d1	přehrávaný
skladeb	skladba	k1gFnPc2	skladba
AV	AV	kA	AV
Foundation	Foundation	k1gInSc1	Foundation
–	–	k?	–
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
sadu	sada	k1gFnSc4	sada
Objective-C	Objective-C	k1gFnSc2	Objective-C
rozhraní	rozhraní	k1gNnSc2	rozhraní
pro	pro	k7c4	pro
správu	správa	k1gFnSc4	správa
přehrávání	přehrávání	k1gNnSc2	přehrávání
a	a	k8xC	a
záznamu	záznam	k1gInSc2	záznam
zvuku	zvuk	k1gInSc2	zvuk
OpenAL	OpenAL	k1gFnSc1	OpenAL
–	–	k?	–
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
sadu	sada	k1gFnSc4	sada
multiplatformních	multiplatformní	k2eAgNnPc2d1	multiplatformní
rozhraní	rozhraní	k1gNnPc2	rozhraní
pro	pro	k7c4	pro
pozicovaný	pozicovaný	k2eAgInSc4d1	pozicovaný
zvuk	zvuk	k1gInSc4	zvuk
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
Core	Cor	k1gMnSc2	Cor
Audio	audio	k2eAgInSc1d1	audio
framework	framework	k1gInSc1	framework
–	–	k?	–
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rozhraní	rozhraní	k1gNnSc4	rozhraní
pro	pro	k7c4	pro
přehrávání	přehrávání	k1gNnSc4	přehrávání
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
záznam	záznam	k1gInSc1	záznam
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přehrávat	přehrávat	k5eAaImF	přehrávat
systémové	systémový	k2eAgInPc4d1	systémový
zvuky	zvuk	k1gInPc4	zvuk
a	a	k8xC	a
upozornění	upozornění	k1gNnPc4	upozornění
<g/>
,	,	kIx,	,
vibrovat	vibrovat	k5eAaImF	vibrovat
a	a	k8xC	a
přehrávat	přehrávat	k5eAaImF	přehrávat
vícekanálový	vícekanálový	k2eAgInSc4d1	vícekanálový
či	či	k8xC	či
streamovaný	streamovaný	k2eAgInSc4d1	streamovaný
zvuk	zvuk	k1gInSc4	zvuk
iOS	iOS	k?	iOS
podporuje	podporovat	k5eAaImIp3nS	podporovat
tyto	tento	k3xDgInPc4	tento
zvukové	zvukový	k2eAgInPc4d1	zvukový
formáty	formát	k1gInPc4	formát
<g/>
:	:	kIx,	:
AAC	AAC	kA	AAC
<g/>
,	,	kIx,	,
ALAC	ALAC	kA	ALAC
<g/>
,	,	kIx,	,
A-law	Aaw	k1gFnSc1	A-law
<g/>
,	,	kIx,	,
IMA	IMA	kA	IMA
<g/>
/	/	kIx~	/
<g/>
ADPCM	ADPCM	kA	ADPCM
(	(	kIx(	(
<g/>
IMA	IMA	kA	IMA
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Linear	Linear	k1gInSc1	Linear
PCM	PCM	kA	PCM
<g/>
,	,	kIx,	,
μ	μ	k?	μ
<g/>
,	,	kIx,	,
DVI	DVI	kA	DVI
<g/>
/	/	kIx~	/
<g/>
Intel	Intel	kA	Intel
IMA	IMA	kA	IMA
ADPCM	ADPCM	kA	ADPCM
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
GSM	GSM	kA	GSM
6.10	[number]	k4	6.10
<g/>
,	,	kIx,	,
AES3-2003	AES3-2003	k1gFnSc6	AES3-2003
Tyto	tento	k3xDgFnPc1	tento
technologie	technologie	k1gFnPc1	technologie
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přehrávat	přehrávat	k5eAaImF	přehrávat
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
video	video	k1gNnSc4	video
záznam	záznam	k1gInSc1	záznam
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
pořizovat	pořizovat	k5eAaImF	pořizovat
(	(	kIx(	(
<g/>
na	na	k7c6	na
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
to	ten	k3xDgNnSc1	ten
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c4	v
aplikaci	aplikace	k1gFnSc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
nabízí	nabízet	k5eAaImIp3nS	nabízet
několik	několik	k4yIc4	několik
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
přehrávat	přehrávat	k5eAaImF	přehrávat
či	či	k8xC	či
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
video	video	k1gNnSc4	video
<g/>
.	.	kIx.	.
</s>
<s>
Vysokoúrovňové	vysokoúrovňový	k2eAgFnPc1d1	vysokoúrovňová
frameworky	frameworka	k1gFnPc1	frameworka
velice	velice	k6eAd1	velice
zjednodušují	zjednodušovat	k5eAaImIp3nP	zjednodušovat
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
takovou	takový	k3xDgFnSc4	takový
míru	míra	k1gFnSc4	míra
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
frameworky	frameworka	k1gFnPc1	frameworka
jsou	být	k5eAaImIp3nP	být
seřazeny	seřadit	k5eAaPmNgFnP	seřadit
od	od	k7c2	od
vysokoúrovňových	vysokoúrovňový	k2eAgInPc2d1	vysokoúrovňový
po	po	k7c6	po
nízkoúrovňové	nízkoúrovňová	k1gFnSc6	nízkoúrovňová
<g/>
.	.	kIx.	.
</s>
<s>
Media	medium	k1gNnPc1	medium
Player	Playero	k1gNnPc2	Playero
framework	framework	k1gInSc1	framework
–	–	k?	–
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přehrávání	přehrávání	k1gNnSc4	přehrávání
videí	video	k1gNnPc2	video
(	(	kIx(	(
<g/>
buď	buď	k8xC	buď
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
částečně	částečně	k6eAd1	částečně
<g/>
)	)	kIx)	)
AV	AV	kA	AV
Foundation	Foundation	k1gInSc1	Foundation
–	–	k?	–
sada	sada	k1gFnSc1	sada
Objective-C	Objective-C	k1gFnSc2	Objective-C
rozhraní	rozhraní	k1gNnSc2	rozhraní
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
a	a	k8xC	a
přehrávání	přehrávání	k1gNnSc4	přehrávání
videa	video	k1gNnSc2	video
Core	Cor	k1gMnSc2	Cor
Media	medium	k1gNnSc2	medium
–	–	k?	–
popisuje	popisovat	k5eAaImIp3nS	popisovat
nízkoúrovňové	nízkoúrovňový	k2eAgInPc4d1	nízkoúrovňový
typy	typ	k1gInPc4	typ
a	a	k8xC	a
rozhraní	rozhraní	k1gNnSc4	rozhraní
používané	používaný	k2eAgNnSc4d1	používané
ve	v	k7c6	v
vysoko	vysoko	k6eAd1	vysoko
úrovňových	úrovňový	k2eAgInPc6d1	úrovňový
frameworcích	frameworek	k1gInPc6	frameworek
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
4.0	[number]	k4	4.0
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
objekty	objekt	k1gInPc4	objekt
typu	typ	k1gInSc2	typ
Block	Blocko	k1gNnPc2	Blocko
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jazykový	jazykový	k2eAgInSc4d1	jazykový
konstrukt	konstrukt	k1gInSc4	konstrukt
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc4d1	možný
používat	používat	k5eAaImF	používat
ve	v	k7c6	v
stávajícím	stávající	k2eAgInSc6d1	stávající
C	C	kA	C
nebo	nebo	k8xC	nebo
Objective-C	Objective-C	k1gFnSc2	Objective-C
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Block	Block	k6eAd1	Block
objekt	objekt	k1gInSc1	objekt
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
anonymní	anonymní	k2eAgFnSc4d1	anonymní
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
související	související	k2eAgNnPc4d1	související
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
konstrukt	konstrukt	k1gInSc1	konstrukt
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
často	často	k6eAd1	často
nazýván	nazýván	k2eAgMnSc1d1	nazýván
closure	closur	k1gMnSc5	closur
nebo	nebo	k8xC	nebo
lambda	lambda	k1gNnPc6	lambda
<g/>
.	.	kIx.	.
</s>
<s>
Block	Block	k6eAd1	Block
objekty	objekt	k1gInPc1	objekt
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nP	hodit
jako	jako	k9	jako
callback	callback	k6eAd1	callback
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
4.0	[number]	k4	4.0
byla	být	k5eAaImAgFnS	být
přidána	přidán	k2eAgFnSc1d1	přidána
technologie	technologie	k1gFnSc1	technologie
Grand	grand	k1gMnSc1	grand
Central	Central	k1gMnSc1	Central
Dispatch	Dispatcha	k1gFnPc2	Dispatcha
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c4	na
BSD	BSD	kA	BSD
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
správu	správa	k1gFnSc4	správa
úloh	úloha	k1gFnPc2	úloha
v	v	k7c6	v
aplikaci	aplikace	k1gFnSc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
GCD	GCD	kA	GCD
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
asynchronní	asynchronní	k2eAgInSc1d1	asynchronní
model	model	k1gInSc1	model
programování	programování	k1gNnSc2	programování
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
optimalizovaným	optimalizovaný	k2eAgNnSc7d1	optimalizované
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
tak	tak	k6eAd1	tak
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
efektivní	efektivní	k2eAgFnSc4d1	efektivní
alternativu	alternativa	k1gFnSc4	alternativa
k	k	k7c3	k
vláknovému	vláknový	k2eAgNnSc3d1	vláknové
programování	programování	k1gNnSc3	programování
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
verze	verze	k1gFnSc2	verze
3.0	[number]	k4	3.0
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
provádět	provádět	k5eAaImF	provádět
platby	platba	k1gFnPc4	platba
uvnitř	uvnitř	k7c2	uvnitř
aplikace	aplikace	k1gFnSc2	aplikace
za	za	k7c4	za
dodatečný	dodatečný	k2eAgInSc4d1	dodatečný
obsah	obsah	k1gInSc4	obsah
či	či	k8xC	či
zrušení	zrušení	k1gNnSc4	zrušení
reklamy	reklama	k1gFnSc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP	umožňovat
sledovat	sledovat	k5eAaImF	sledovat
aktuální	aktuální	k2eAgFnSc4d1	aktuální
polohu	poloha	k1gFnSc4	poloha
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
polohy	poloha	k1gFnSc2	poloha
veškerý	veškerý	k3xTgInSc4	veškerý
dostupný	dostupný	k2eAgInSc4d1	dostupný
hardware	hardware	k1gInSc4	hardware
(	(	kIx(	(
<g/>
Wi-Fi	Wi-Fe	k1gFnSc4	Wi-Fe
<g/>
,	,	kIx,	,
telefonní	telefonní	k2eAgFnSc4d1	telefonní
síť	síť	k1gFnSc4	síť
<g/>
,	,	kIx,	,
GPS	GPS	kA	GPS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnPc1	aplikace
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
uživateli	uživatel	k1gMnSc3	uživatel
nabídnout	nabídnout	k5eAaPmF	nabídnout
data	datum	k1gNnPc4	datum
relevantní	relevantní	k2eAgNnPc4d1	relevantní
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
poloze	poloha	k1gFnSc3	poloha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
restaurace	restaurace	k1gFnSc1	restaurace
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odlehčená	odlehčený	k2eAgFnSc1d1	odlehčená
SQL	SQL	kA	SQL
databáze	databáze	k1gFnSc1	databáze
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ukládání	ukládání	k1gNnSc1	ukládání
uživatelských	uživatelský	k2eAgNnPc2d1	Uživatelské
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
XML	XML	kA	XML
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
Core	Cor	k1gFnSc2	Cor
OS	OS	kA	OS
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
nízkoúrovňové	nízkoúrovňový	k2eAgFnPc4d1	nízkoúrovňová
funkce	funkce	k1gFnPc4	funkce
ostatním	ostatní	k2eAgFnPc3d1	ostatní
technologiím	technologie	k1gFnPc3	technologie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
postaveny	postavit	k5eAaPmNgInP	postavit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
nejsou	být	k5eNaImIp3nP	být
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
aplikacích	aplikace	k1gFnPc6	aplikace
využívány	využíván	k2eAgMnPc4d1	využíván
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
je	on	k3xPp3gFnPc4	on
využívají	využívat	k5eAaImIp3nP	využívat
vysokoúrovňové	vysokoúrovňový	k2eAgFnPc4d1	vysokoúrovňová
komponenty	komponenta	k1gFnPc4	komponenta
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rozhraní	rozhraní	k1gNnSc1	rozhraní
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
matematickými	matematický	k2eAgFnPc7d1	matematická
funkcemi	funkce	k1gFnPc7	funkce
(	(	kIx(	(
<g/>
obdoba	obdoba	k1gFnSc1	obdoba
java	java	k1gMnSc1	java
<g/>
.	.	kIx.	.
<g/>
math	math	k1gMnSc1	math
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velkými	velký	k2eAgNnPc7d1	velké
čísly	číslo	k1gNnPc7	číslo
<g/>
,	,	kIx,	,
výpočty	výpočet	k1gInPc7	výpočet
DSP	DSP	kA	DSP
apod.	apod.	kA	apod.
Výhodou	výhoda	k1gFnSc7	výhoda
tohoto	tento	k3xDgInSc2	tento
frameworku	frameworek	k1gInSc2	frameworek
oproti	oproti	k7c3	oproti
vlastní	vlastní	k2eAgFnSc3d1	vlastní
implementaci	implementace	k1gFnSc3	implementace
těchto	tento	k3xDgFnPc2	tento
funkcí	funkce	k1gFnPc2	funkce
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
verzích	verze	k1gFnPc6	verze
iOS	iOS	k?	iOS
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
různá	různý	k2eAgNnPc4d1	různé
zařízení	zařízení	k1gNnPc4	zařízení
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
framework	framework	k1gInSc1	framework
optimalizován	optimalizován	k2eAgInSc1d1	optimalizován
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
hardware	hardware	k1gInSc4	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
framework	framework	k1gInSc1	framework
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
externími	externí	k2eAgNnPc7d1	externí
zařízeními	zařízení	k1gNnPc7	zařízení
připojenými	připojený	k2eAgFnPc7d1	připojená
přes	přes	k7c4	přes
Bluetooth	Bluetooth	k1gInSc4	Bluetooth
nebo	nebo	k8xC	nebo
třicetipinový	třicetipinový	k2eAgInSc4d1	třicetipinový
konektor	konektor	k1gInSc4	konektor
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Framework	Framework	k1gInSc1	Framework
také	také	k9	také
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
získávat	získávat	k5eAaImF	získávat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
dostupném	dostupný	k2eAgNnSc6d1	dostupné
příslušenství	příslušenství	k1gNnSc6	příslušenství
a	a	k8xC	a
navázat	navázat	k5eAaPmF	navázat
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vestavěných	vestavěný	k2eAgFnPc2d1	vestavěná
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
vlastností	vlastnost	k1gFnPc2	vlastnost
iOS	iOS	k?	iOS
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
Security	Securita	k1gFnPc4	Securita
framework	framework	k1gInSc4	framework
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zaručit	zaručit	k5eAaPmF	zaručit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
citlivých	citlivý	k2eAgNnPc2d1	citlivé
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
rozhraní	rozhraní	k1gNnSc4	rozhraní
pro	pro	k7c4	pro
certifikáty	certifikát	k1gInPc4	certifikát
<g/>
,	,	kIx,	,
soukromé	soukromý	k2eAgInPc4d1	soukromý
a	a	k8xC	a
veřejné	veřejný	k2eAgInPc4d1	veřejný
klíče	klíč	k1gInPc4	klíč
<g/>
,	,	kIx,	,
generování	generování	k1gNnSc1	generování
kryptografických	kryptografický	k2eAgNnPc2d1	kryptografické
pseudonáhodných	pseudonáhodný	k2eAgNnPc2d1	pseudonáhodné
čísel	číslo	k1gNnPc2	číslo
apod.	apod.	kA	apod.
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ukládat	ukládat	k5eAaImF	ukládat
data	datum	k1gNnPc4	datum
do	do	k7c2	do
zašifrovaného	zašifrovaný	k2eAgNnSc2d1	zašifrované
centrálního	centrální	k2eAgNnSc2d1	centrální
úložiště	úložiště	k1gNnSc2	úložiště
svazku	svazek	k1gInSc2	svazek
klíčů	klíč	k1gInPc2	klíč
(	(	kIx(	(
<g/>
keychain	keychain	k1gInSc1	keychain
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úložišti	úložiště	k1gNnSc6	úložiště
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
možné	možný	k2eAgInPc4d1	možný
údaje	údaj	k1gInPc4	údaj
sdílet	sdílet	k5eAaImF	sdílet
mezi	mezi	k7c7	mezi
aplikacemi	aplikace	k1gFnPc7	aplikace
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
aplikace	aplikace	k1gFnSc1	aplikace
zkompilována	zkompilován	k2eAgFnSc1d1	zkompilován
s	s	k7c7	s
příslušným	příslušný	k2eAgNnSc7d1	příslušné
nastavením	nastavení	k1gNnSc7	nastavení
oprávnění	oprávnění	k1gNnSc2	oprávnění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
iOS	iOS	k?	iOS
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
spouště	spoušť	k1gFnPc4	spoušť
aplikace	aplikace	k1gFnSc2	aplikace
napsané	napsaný	k2eAgFnSc2d1	napsaná
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
C	C	kA	C
nebo	nebo	k8xC	nebo
pokročilejším	pokročilý	k2eAgMnPc3d2	pokročilejší
Objective-C	Objective-C	k1gMnPc3	Objective-C
<g/>
/	/	kIx~	/
<g/>
Swiftu	Swift	k1gInSc3	Swift
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
aplikaci	aplikace	k1gFnSc6	aplikace
XCode	XCod	k1gMnSc5	XCod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
vývojové	vývojový	k2eAgNnSc4d1	vývojové
prostředí	prostředí	k1gNnSc4	prostředí
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Apple	Apple	kA	Apple
(	(	kIx(	(
<g/>
nabízené	nabízený	k2eAgNnSc1d1	nabízené
zdarma	zdarma	k6eAd1	zdarma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
však	však	k9	však
dostupné	dostupný	k2eAgNnSc1d1	dostupné
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vývoj	vývoj	k1gInSc1	vývoj
např.	např.	kA	např.
ve	v	k7c6	v
Windows	Windows	kA	Windows
či	či	k8xC	či
Linuxu	linux	k1gInSc2	linux
není	být	k5eNaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
řešit	řešit	k5eAaImF	řešit
několik	několik	k4yIc1	několik
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgInP	snažit
kompilovat	kompilovat	k5eAaImF	kompilovat
programy	program	k1gInPc1	program
napsané	napsaný	k2eAgInPc1d1	napsaný
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
do	do	k7c2	do
nativního	nativní	k2eAgInSc2d1	nativní
kódu	kód	k1gInSc2	kód
Objective-	Objective-	k1gMnPc1	Objective-
<g/>
C.	C.	kA	C.
Asi	asi	k9	asi
největším	veliký	k2eAgInSc7d3	veliký
počinem	počin	k1gInSc7	počin
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
krok	krok	k1gInSc1	krok
společnosti	společnost	k1gFnSc2	společnost
Adobe	Adobe	kA	Adobe
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
svého	svůj	k3xOyFgInSc2	svůj
nástroje	nástroj	k1gInSc2	nástroj
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
Flash	Flash	k1gMnSc1	Flash
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kompilovat	kompilovat	k5eAaImF	kompilovat
právě	právě	k9	právě
do	do	k7c2	do
programu	program	k1gInSc2	program
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
iOS	iOS	k?	iOS
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
(	(	kIx(	(
<g/>
a	a	k8xC	a
podobné	podobný	k2eAgInPc4d1	podobný
<g/>
)	)	kIx)	)
nástroje	nástroj	k1gInPc4	nástroj
však	však	k9	však
byly	být	k5eAaImAgFnP	být
zakázány	zakázat	k5eAaPmNgFnP	zakázat
v	v	k7c6	v
licenčním	licenční	k2eAgNnSc6d1	licenční
ujednání	ujednání	k1gNnSc6	ujednání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
nevoli	nevole	k1gFnSc6	nevole
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vývojářů	vývojář	k1gMnPc2	vývojář
byly	být	k5eAaImAgFnP	být
opět	opět	k6eAd1	opět
povoleny	povolen	k2eAgFnPc1d1	povolena
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jailbreak	Jailbreak	k1gInSc1	Jailbreak
<g/>
.	.	kIx.	.
iOS	iOS	k?	iOS
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
systému	systém	k1gInSc2	systém
a	a	k8xC	a
také	také	k9	také
omezuje	omezovat	k5eAaImIp3nS	omezovat
možnost	možnost	k1gFnSc4	možnost
instalace	instalace	k1gFnSc1	instalace
aplikací	aplikace	k1gFnPc2	aplikace
–	–	k?	–
jediná	jediný	k2eAgFnSc1d1	jediná
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
oficiální	oficiální	k2eAgFnSc4d1	oficiální
App	App	k1gFnSc4	App
Store	Stor	k1gMnSc5	Stor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
aplikace	aplikace	k1gFnPc1	aplikace
procházejí	procházet	k5eAaImIp3nP	procházet
schvalovacím	schvalovací	k2eAgInSc7d1	schvalovací
procesem	proces	k1gInSc7	proces
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
nedostupnost	nedostupnost	k1gFnSc4	nedostupnost
některých	některý	k3yIgFnPc2	některý
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
nemožnost	nemožnost	k1gFnSc4	nemožnost
přístupu	přístup	k1gInSc2	přístup
do	do	k7c2	do
systému	systém	k1gInSc2	systém
bývají	bývat	k5eAaImIp3nP	bývat
motivem	motiv	k1gInSc7	motiv
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
jailbreaku	jailbreak	k1gInSc2	jailbreak
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
systém	systém	k1gInSc4	systém
a	a	k8xC	a
umožní	umožnit	k5eAaPmIp3nS	umožnit
nahrávat	nahrávat	k5eAaImF	nahrávat
neautorizované	autorizovaný	k2eNgFnPc4d1	neautorizovaná
aplikace	aplikace	k1gFnPc4	aplikace
a	a	k8xC	a
přistupovat	přistupovat	k5eAaImF	přistupovat
ke	k	k7c3	k
chráněným	chráněný	k2eAgInPc3d1	chráněný
souborům	soubor	k1gInPc3	soubor
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
výhod	výhoda	k1gFnPc2	výhoda
ale	ale	k8xC	ale
přináší	přinášet	k5eAaImIp3nS	přinášet
rizika	riziko	k1gNnPc4	riziko
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
snížení	snížení	k1gNnSc2	snížení
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc4	zvýšení
spotřeby	spotřeba	k1gFnSc2	spotřeba
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
zvýšeného	zvýšený	k2eAgNnSc2d1	zvýšené
rizika	riziko	k1gNnSc2	riziko
napadení	napadení	k1gNnSc2	napadení
telefonu	telefon	k1gInSc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
uživatelů	uživatel	k1gMnPc2	uživatel
také	také	k9	také
využívá	využívat	k5eAaPmIp3nS	využívat
možnosti	možnost	k1gFnPc4	možnost
nahrávat	nahrávat	k5eAaImF	nahrávat
do	do	k7c2	do
zařízení	zařízení	k1gNnSc2	zařízení
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
'	'	kIx"	'
<g/>
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
<g/>
'	'	kIx"	'
z	z	k7c2	z
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Apple	Apple	kA	Apple
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
bránit	bránit	k5eAaImF	bránit
tomuto	tento	k3xDgInSc3	tento
procesu	proces	k1gInSc3	proces
u	u	k7c2	u
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejnovější	nový	k2eAgInSc1d3	nejnovější
verdikt	verdikt	k1gInSc1	verdikt
zní	znět	k5eAaImIp3nS	znět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
legální	legální	k2eAgInSc1d1	legální
zásah	zásah	k1gInSc1	zásah
a	a	k8xC	a
nepoškozuje	poškozovat	k5eNaImIp3nS	poškozovat
copyright	copyright	k1gInSc1	copyright
společnosti	společnost	k1gFnSc2	společnost
Apple	Apple	kA	Apple
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
iOS	iOS	k?	iOS
8.1	[number]	k4	8.1
<g/>
.3	.3	k4	.3
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
spousta	spousta	k1gFnSc1	spousta
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
doposud	doposud	k6eAd1	doposud
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
nainstalovat	nainstalovat	k5eAaPmF	nainstalovat
Jailbreak	Jailbreak	k1gInSc4	Jailbreak
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgFnPc1d1	kompletní
opravy	oprava	k1gFnPc1	oprava
těchto	tento	k3xDgFnPc2	tento
děr	děra	k1gFnPc2	děra
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
objevit	objevit	k5eAaPmF	objevit
až	až	k9	až
v	v	k7c6	v
přicházející	přicházející	k2eAgFnSc6d1	přicházející
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
<g/>
,	,	kIx,	,
iOS	iOS	k?	iOS
9	[number]	k4	9
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
definitivně	definitivně	k6eAd1	definitivně
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
nemožné	možný	k2eNgNnSc1d1	nemožné
si	se	k3xPyFc3	se
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
nainstalovat	nainstalovat	k5eAaPmF	nainstalovat
jailbreak	jailbreak	k6eAd1	jailbreak
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumník	výzkumník	k1gMnSc1	výzkumník
cyber-bezpečnosti	cyberezpečnost	k1gFnSc2	cyber-bezpečnost
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
Soghoian	Soghoiana	k1gFnPc2	Soghoiana
<g/>
,	,	kIx,	,
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
indické	indický	k2eAgFnSc2d1	indická
vojenské	vojenský	k2eAgFnSc2d1	vojenská
rozvědky	rozvědka	k1gFnSc2	rozvědka
rozeslán	rozeslán	k2eAgInSc1d1	rozeslán
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
oběžník	oběžník	k1gInSc1	oběžník
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
plukovníkem	plukovník	k1gMnSc7	plukovník
Išwarem	Išwar	k1gMnSc7	Išwar
Singhem	Singh	k1gInSc7	Singh
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
získán	získat	k5eAaPmNgInS	získat
hackery	hacker	k1gMnPc7	hacker
a	a	k8xC	a
zveřejněn	zveřejnit	k5eAaPmNgInS	zveřejnit
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oběžníku	oběžník	k1gInSc6	oběžník
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
přítomnost	přítomnost	k1gFnSc4	přítomnost
na	na	k7c6	na
indickém	indický	k2eAgMnSc6d1	indický
(	(	kIx(	(
<g/>
mobilním	mobilní	k2eAgInSc6d1	mobilní
<g/>
)	)	kIx)	)
trhu	trh	k1gInSc6	trh
<g/>
"	"	kIx"	"
výrobci	výrobce	k1gMnPc1	výrobce
mobilních	mobilní	k2eAgNnPc2d1	mobilní
zařízení	zařízení	k1gNnPc2	zařízení
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
korporace	korporace	k1gFnSc1	korporace
RIM	RIM	kA	RIM
<g/>
,	,	kIx,	,
Nokia	Nokia	kA	Nokia
a	a	k8xC	a
Apple	Apple	kA	Apple
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
s	s	k7c7	s
umožněním	umožnění	k1gNnSc7	umožnění
a	a	k8xC	a
poskytnutím	poskytnutí	k1gNnSc7	poskytnutí
přístupu	přístup	k1gInSc2	přístup
"	"	kIx"	"
<g/>
zadními	zadní	k2eAgNnPc7d1	zadní
vrátky	vrátka	k1gNnPc7	vrátka
<g/>
"	"	kIx"	"
na	na	k7c6	na
jimi	on	k3xPp3gMnPc7	on
vyráběných	vyráběný	k2eAgNnPc2d1	vyráběné
zařízeních	zařízení	k1gNnPc6	zařízení
indické	indický	k2eAgFnSc3d1	indická
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
následně	následně	k6eAd1	následně
tento	tento	k3xDgInSc4	tento
přístup	přístup	k1gInSc4	přístup
informací	informace	k1gFnPc2	informace
utilizovala	utilizovat	k5eAaBmAgFnS	utilizovat
pro	pro	k7c4	pro
interní	interní	k2eAgInPc4d1	interní
e-maily	eail	k1gInPc4	e-mail
americko-čínské	americko-čínský	k2eAgFnSc2d1	americko-čínská
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
a	a	k8xC	a
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
komise	komise	k1gFnSc2	komise
a	a	k8xC	a
orgánům	orgán	k1gMnPc3	orgán
vlády	vláda	k1gFnSc2	vláda
USA	USA	kA	USA
s	s	k7c7	s
mandátem	mandát	k1gInSc7	mandát
monitorovat	monitorovat	k5eAaImF	monitorovat
<g/>
,	,	kIx,	,
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
a	a	k8xC	a
podávat	podávat	k5eAaImF	podávat
hlášení	hlášení	k1gNnPc4	hlášení
Kongresu	kongres	k1gInSc2	kongres
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
implikací	implikace	k1gFnPc2	implikace
národní	národní	k2eAgFnSc2d1	národní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
o	o	k7c4	o
ekonomického	ekonomický	k2eAgMnSc4d1	ekonomický
vztahu	vztah	k1gInSc6	vztah
a	a	k8xC	a
oboustranném	oboustranný	k2eAgInSc6d1	oboustranný
obchodu	obchod	k1gInSc6	obchod
mezi	mezi	k7c7	mezi
USA	USA	kA	USA
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
