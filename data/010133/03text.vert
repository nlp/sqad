<p>
<s>
Niels	Niels	k6eAd1	Niels
Henrik	Henrik	k1gMnSc1	Henrik
Abel	Abel	k1gMnSc1	Abel
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1802	[number]	k4	1802
<g/>
,	,	kIx,	,
Nedstrand	Nedstrand	k1gInSc1	Nedstrand
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1829	[number]	k4	1829
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Arendalu	Arendal	k1gInSc2	Arendal
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
norský	norský	k2eAgMnSc1d1	norský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
funkcionální	funkcionální	k2eAgFnSc4d1	funkcionální
analýzu	analýza	k1gFnSc4	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
důkazem	důkaz	k1gInSc7	důkaz
nemožnosti	nemožnost	k1gFnSc3	nemožnost
obecného	obecný	k2eAgNnSc2d1	obecné
řešení	řešení	k1gNnSc2	řešení
rovnic	rovnice	k1gFnPc2	rovnice
pátého	pátý	k4xOgInSc2	pátý
stupně	stupeň	k1gInSc2	stupeň
pomocí	pomocí	k7c2	pomocí
vzorců	vzorec	k1gInPc2	vzorec
s	s	k7c7	s
odmocninami	odmocnina	k1gFnPc7	odmocnina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Abelovi	Abel	k1gMnSc6	Abel
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
řada	řada	k1gFnSc1	řada
matematických	matematický	k2eAgInPc2d1	matematický
pojmů	pojem	k1gInPc2	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
Abelova	Abelův	k2eAgFnSc1d1	Abelova
grupa	grupa	k1gFnSc1	grupa
<g/>
,	,	kIx,	,
Abelova	Abelův	k2eAgFnSc1d1	Abelova
sumace	sumace	k1gFnSc1	sumace
<g/>
,	,	kIx,	,
Abelovo	Abelův	k2eAgNnSc1d1	Abelův
kritérium	kritérium	k1gNnSc1	kritérium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Abelova	Abelův	k2eAgFnSc1d1	Abelova
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Raná	raný	k2eAgNnPc4d1	rané
léta	léto	k1gNnPc4	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Abel	Abel	k1gMnSc1	Abel
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
norského	norský	k2eAgMnSc2d1	norský
luterského	luterský	k2eAgMnSc2d1	luterský
pastora	pastor	k1gMnSc2	pastor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
měl	mít	k5eAaImAgMnS	mít
taktéž	taktéž	k?	taktéž
doktorát	doktorát	k1gInSc4	doktorát
z	z	k7c2	z
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Aktivním	aktivní	k2eAgMnSc7d1	aktivní
pastorem	pastor	k1gMnSc7	pastor
byl	být	k5eAaImAgMnS	být
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k6eAd1	rovněž
politikem	politik	k1gMnSc7	politik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
poslancem	poslanec	k1gMnSc7	poslanec
parlamentu	parlament	k1gInSc2	parlament
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Napoleona	Napoleon	k1gMnSc2	Napoleon
Norsko	Norsko	k1gNnSc1	Norsko
vymanilo	vymanit	k5eAaPmAgNnS	vymanit
z	z	k7c2	z
područí	područí	k1gNnSc2	područí
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
alkoholismu	alkoholismus	k1gInSc2	alkoholismus
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
začal	začít	k5eAaPmAgInS	začít
Abel	Abel	k1gInSc1	Abel
studovat	studovat	k5eAaImF	studovat
katedrální	katedrální	k2eAgFnSc4d1	katedrální
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
talent	talent	k1gInSc1	talent
brilantním	brilantní	k2eAgNnSc7d1	brilantní
řešením	řešení	k1gNnSc7	řešení
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
předložil	předložit	k5eAaPmAgMnS	předložit
matematik	matematik	k1gMnSc1	matematik
Bernt	Bernt	k1gMnSc1	Bernt
Holmboe	Holmboe	k1gFnSc1	Holmboe
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
jeho	jeho	k3xOp3gNnSc2	jeho
velkého	velký	k2eAgNnSc2d1	velké
nadání	nadání	k1gNnSc2	nadání
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
do	do	k7c2	do
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Dával	dávat	k5eAaImAgMnS	dávat
mu	on	k3xPp3gMnSc3	on
různé	různý	k2eAgInPc1d1	různý
úkoly	úkol	k1gInPc1	úkol
a	a	k8xC	a
půjčoval	půjčovat	k5eAaImAgInS	půjčovat
mu	on	k3xPp3gMnSc3	on
spisy	spis	k1gInPc4	spis
Eulera	Euler	k1gMnSc4	Euler
a	a	k8xC	a
Lagrangea	Lagrangeus	k1gMnSc4	Lagrangeus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gFnSc6	jeho
otce	otka	k1gFnSc6	otka
rodina	rodina	k1gFnSc1	rodina
ztratila	ztratit	k5eAaPmAgFnS	ztratit
finanční	finanční	k2eAgInSc4d1	finanční
příjem	příjem	k1gInSc4	příjem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malá	malý	k2eAgFnSc1d1	malá
státní	státní	k2eAgFnSc1d1	státní
penze	penze	k1gFnSc1	penze
a	a	k8xC	a
finanční	finanční	k2eAgFnSc1d1	finanční
podpora	podpora	k1gFnSc1	podpora
Holmboea	Holmboeus	k1gMnSc2	Holmboeus
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Oslo	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Profesoři	profesor	k1gMnPc1	profesor
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
tíživé	tíživý	k2eAgFnSc3d1	tíživá
finanční	finanční	k2eAgFnSc3d1	finanční
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
pořádali	pořádat	k5eAaImAgMnP	pořádat
sbírky	sbírka	k1gFnPc4	sbírka
a	a	k8xC	a
poskytovali	poskytovat	k5eAaImAgMnP	poskytovat
mu	on	k3xPp3gMnSc3	on
střídavě	střídavě	k6eAd1	střídavě
zaopatření	zaopatření	k1gNnPc4	zaopatření
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rodinách	rodina	k1gFnPc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Abel	Abel	k1gMnSc1	Abel
školu	škola	k1gFnSc4	škola
dokončil	dokončit	k5eAaPmAgMnS	dokončit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
profesorů	profesor	k1gMnPc2	profesor
mu	on	k3xPp3gMnSc3	on
dokonce	dokonce	k9	dokonce
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
větší	veliký	k2eAgFnSc4d2	veliký
sumu	suma	k1gFnSc4	suma
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
např.	např.	kA	např.
s	s	k7c7	s
dánských	dánský	k2eAgFnPc2d1	dánská
matematikem	matematik	k1gMnSc7	matematik
Degenem	Degeno	k1gNnSc7	Degeno
a	a	k8xC	a
dalšími	další	k1gNnPc7	další
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
výrazně	výrazně	k6eAd1	výrazně
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Matematická	matematický	k2eAgFnSc1d1	matematická
práce	práce	k1gFnSc1	práce
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Dánska	Dánsko	k1gNnSc2	Dánsko
začal	začít	k5eAaPmAgInS	začít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
seznámit	seznámit	k5eAaPmF	seznámit
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
předními	přední	k2eAgMnPc7d1	přední
matematiky	matematik	k1gMnPc7	matematik
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
zatím	zatím	k6eAd1	zatím
získal	získat	k5eAaPmAgMnS	získat
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
po	po	k7c4	po
následující	následující	k2eAgInPc4d1	následující
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
učil	učit	k5eAaImAgMnS	učit
německy	německy	k6eAd1	německy
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Abel	Abel	k1gMnSc1	Abel
vydal	vydat	k5eAaPmAgMnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
významnější	významný	k2eAgFnSc4d2	významnější
práci	práce	k1gFnSc4	práce
–	–	k?	–
Mémoire	Mémoir	k1gInSc5	Mémoir
sur	sur	k?	sur
les	les	k1gInSc1	les
équations	équationsa	k1gFnPc2	équationsa
algébriques	algébriquesa	k1gFnPc2	algébriquesa
ou	ou	k0	ou
on	on	k3xPp3gMnSc1	on
démontre	démontr	k1gInSc5	démontr
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
impossibilité	impossibilitý	k2eAgNnSc1d1	impossibilitý
de	de	k?	de
la	la	k1gNnSc1	la
résolution	résolution	k1gInSc1	résolution
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
équation	équation	k1gInSc1	équation
générale	générale	k6eAd1	générale
du	du	k?	du
cinquiè	cinquiè	k?	cinquiè
degré	degrý	k2eAgNnSc1d1	degrý
(	(	kIx(	(
<g/>
Rozprava	rozprava	k1gFnSc1	rozprava
o	o	k7c6	o
rovnicích	rovnice	k1gFnPc6	rovnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
dokázána	dokázán	k2eAgFnSc1d1	dokázána
nemožnost	nemožnost	k1gFnSc1	nemožnost
obecného	obecný	k2eAgNnSc2d1	obecné
řešení	řešení	k1gNnSc2	řešení
rovnic	rovnice	k1gFnPc2	rovnice
pátého	pátý	k4xOgInSc2	pátý
stupně	stupeň	k1gInSc2	stupeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
obtížně	obtížně	k6eAd1	obtížně
srozumitelné	srozumitelný	k2eAgFnSc6d1	srozumitelná
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Abel	Abel	k1gInSc1	Abel
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
co	co	k3yInSc4	co
nejvíce	nejvíce	k6eAd1	nejvíce
šetřit	šetřit	k5eAaImF	šetřit
papírem	papír	k1gInSc7	papír
pro	pro	k7c4	pro
nákladnost	nákladnost	k1gFnSc4	nákladnost
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Rovnicemi	rovnice	k1gFnPc7	rovnice
pátého	pátý	k4xOgInSc2	pátý
stupně	stupeň	k1gInSc2	stupeň
se	se	k3xPyFc4	se
Abel	Abel	k1gInSc1	Abel
zabýval	zabývat	k5eAaImAgInS	zabývat
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
jeden	jeden	k4xCgInSc4	jeden
čas	čas	k1gInSc4	čas
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
vzorec	vzorec	k1gInSc4	vzorec
pro	pro	k7c4	pro
obecné	obecný	k2eAgNnSc4d1	obecné
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konzultacích	konzultace	k1gFnPc6	konzultace
s	s	k7c7	s
Degenem	Degen	k1gInSc7	Degen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
doporučil	doporučit	k5eAaPmAgInS	doporučit
výsledky	výsledek	k1gInPc4	výsledek
důkladněji	důkladně	k6eAd2	důkladně
prověřit	prověřit	k5eAaPmF	prověřit
<g/>
,	,	kIx,	,
však	však	k9	však
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
úvahy	úvaha	k1gFnPc1	úvaha
byly	být	k5eAaImAgFnP	být
chybné	chybný	k2eAgFnPc1d1	chybná
<g/>
.	.	kIx.	.
</s>
<s>
Degen	Degen	k1gInSc1	Degen
Abela	Abelo	k1gNnSc2	Abelo
taktéž	taktéž	k?	taktéž
nasměroval	nasměrovat	k5eAaPmAgInS	nasměrovat
k	k	k7c3	k
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
eliptické	eliptický	k2eAgFnPc4d1	eliptická
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
vládního	vládní	k2eAgNnSc2d1	vládní
dvouletého	dvouletý	k2eAgNnSc2d1	dvouleté
stipendia	stipendium	k1gNnSc2	stipendium
mohl	moct	k5eAaImAgInS	moct
Abel	Abel	k1gInSc1	Abel
konečně	konečně	k6eAd1	konečně
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
stavebním	stavební	k2eAgMnSc7d1	stavební
inženýrem	inženýr	k1gMnSc7	inženýr
Augustem	August	k1gMnSc7	August
Leopoldem	Leopold	k1gMnSc7	Leopold
Crellem	Crell	k1gMnSc7	Crell
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaPmF	vydávat
svůj	svůj	k3xOyFgInSc4	svůj
vědecký	vědecký	k2eAgInSc4d1	vědecký
časopis	časopis	k1gInSc4	časopis
<g/>
,	,	kIx,	,
v	v	k7c4	v
jehož	jehož	k3xOyRp3gNnPc2	jehož
1	[number]	k4	1
<g/>
.	.	kIx.	.
svazku	svazek	k1gInSc2	svazek
(	(	kIx(	(
<g/>
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
otiskl	otisknout	k5eAaPmAgInS	otisknout
Abelovy	Abelův	k2eAgFnPc4d1	Abelova
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
šesti	šest	k4xCc6	šest
měsících	měsíc	k1gInPc6	měsíc
strávených	strávený	k2eAgInPc2d1	strávený
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
se	se	k3xPyFc4	se
Abel	Abel	k1gMnSc1	Abel
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Freiburgu	Freiburg	k1gInSc2	Freiburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
svých	svůj	k3xOyFgInPc2	svůj
skvělých	skvělý	k2eAgInPc2d1	skvělý
výsledků	výsledek	k1gInPc2	výsledek
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
teorie	teorie	k1gFnSc2	teorie
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dál	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
Prahu	Praha	k1gFnSc4	Praha
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc4	Rakousko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
strávil	strávit	k5eAaPmAgMnS	strávit
deset	deset	k4xCc4	deset
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgMnPc2	jenž
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
předními	přední	k2eAgMnPc7d1	přední
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
matematiky	matematik	k1gMnPc7	matematik
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byla	být	k5eAaImAgFnS	být
sotva	sotva	k6eAd1	sotva
známa	znám	k2eAgFnSc1d1	známa
a	a	k8xC	a
Abel	Abel	k1gInSc1	Abel
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
skromnost	skromnost	k1gFnSc4	skromnost
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
nepropagoval	propagovat	k5eNaImAgMnS	propagovat
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgInPc4d1	finanční
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yQgFnPc2	který
nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgInS	mít
pokoj	pokoj	k1gInSc1	pokoj
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
donutily	donutit	k5eAaPmAgFnP	donutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smrt	smrt	k1gFnSc1	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
Abel	Abel	k1gMnSc1	Abel
nakazil	nakazit	k5eAaPmAgMnS	nakazit
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
jel	jet	k5eAaImAgMnS	jet
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
navštívit	navštívit	k5eAaPmF	navštívit
svoji	svůj	k3xOyFgFnSc4	svůj
snoubenku	snoubenka	k1gFnSc4	snoubenka
do	do	k7c2	do
Frolandu	Froland	k1gInSc2	Froland
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
vážně	vážně	k6eAd1	vážně
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
dočasnému	dočasný	k2eAgNnSc3d1	dočasné
zlepšení	zlepšení	k1gNnSc3	zlepšení
mohl	moct	k5eAaImAgInS	moct
pár	pár	k1gInSc1	pár
strávit	strávit	k5eAaPmF	strávit
společné	společný	k2eAgFnPc4d1	společná
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
L.	L.	kA	L.
Crelle	Crelle	k1gInSc1	Crelle
se	se	k3xPyFc4	se
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
zatím	zatím	k6eAd1	zatím
snažil	snažit	k5eAaImAgInS	snažit
Abelovi	Abel	k1gMnSc3	Abel
najít	najít	k5eAaPmF	najít
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
–	–	k?	–
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zařídit	zařídit	k5eAaPmF	zařídit
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
</s>
<s>
Abel	Abel	k1gMnSc1	Abel
nikdy	nikdy	k6eAd1	nikdy
v	v	k7c6	v
životě	život	k1gInSc6	život
neměl	mít	k5eNaImAgMnS	mít
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Crelle	Crelle	k1gInSc1	Crelle
odesílal	odesílat	k5eAaImAgInS	odesílat
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
profesury	profesura	k1gFnSc2	profesura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Abel	Abel	k1gMnSc1	Abel
již	již	k9	již
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Niels	Nielsa	k1gFnPc2	Nielsa
Henrik	Henrik	k1gMnSc1	Henrik
Abel	Abel	k1gMnSc1	Abel
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Encyklopedická	encyklopedický	k2eAgFnSc1d1	encyklopedická
edice	edice	k1gFnSc1	edice
Listy	lista	k1gFnSc2	lista
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Encyklopedický	encyklopedický	k2eAgInSc4d1	encyklopedický
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
;	;	kIx,	;
soubor	soubor	k1gInSc1	soubor
Matematici	matematik	k1gMnPc1	matematik
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Abel	Abel	k1gInSc1	Abel
<g/>
,	,	kIx,	,
Niels	Niels	k1gInSc1	Niels
Henrik	Henrik	k1gMnSc1	Henrik
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
matematiků	matematik	k1gMnPc2	matematik
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Niels	Niels	k1gInSc1	Niels
Henrik	Henrik	k1gMnSc1	Henrik
Abel	Abela	k1gFnPc2	Abela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Benediktová	Benediktový	k2eAgFnSc1d1	Benediktová
Větrovcová	Větrovcová	k1gFnSc1	Větrovcová
<g/>
:	:	kIx,	:
Niels	Niels	k1gInSc1	Niels
H.	H.	kA	H.
Abel	Abel	k1gInSc1	Abel
<g/>
:	:	kIx,	:
O	o	k7c6	o
algebraických	algebraický	k2eAgFnPc6d1	algebraická
rovnicích	rovnice	k1gFnPc6	rovnice
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
s	s	k7c7	s
překladem	překlad	k1gInSc7	překlad
Abelových	Abelův	k2eAgFnPc2d1	Abelova
prací	práce	k1gFnPc2	práce
Petrem	Petr	k1gMnSc7	Petr
Němcem	Němec	k1gMnSc7	Němec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
Kanina	Kanina	k1gFnSc1	Kanina
<g/>
:	:	kIx,	:
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
a	a	k8xC	a
OPS	OPS	kA	OPS
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Stránky	stránka	k1gFnPc4	stránka
Abelovy	Abelův	k2eAgFnSc2d1	Abelova
ceny	cena	k1gFnSc2	cena
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
něm.	něm.	k?	něm.
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Niels	Niels	k1gInSc1	Niels
Henrik	Henrik	k1gMnSc1	Henrik
Abel	Abel	k1gMnSc1	Abel
</s>
</p>
