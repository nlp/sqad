<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
norský	norský	k2eAgMnSc1d1	norský
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
významně	významně	k6eAd1	významně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
funkcionální	funkcionální	k2eAgFnSc4d1	funkcionální
analýzu	analýza	k1gFnSc4	analýza
<g/>
?	?	kIx.	?
</s>
