<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
(	(	kIx(	(
<g/>
Eudyptes	Eudyptes	k1gMnSc1	Eudyptes
chrysolophus	chrysolophus	k1gMnSc1	chrysolophus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
zvaný	zvaný	k2eAgInSc4d1	zvaný
makarony	makaron	k1gInPc4	makaron
(	(	kIx(	(
<g/>
či	či	k8xC	či
macaroni	macaron	k1gMnPc1	macaron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
tučňáka	tučňák	k1gMnSc2	tučňák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
6	[number]	k4	6
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
Eudyptes	Eudyptesa	k1gFnPc2	Eudyptesa
obývá	obývat	k5eAaImIp3nS	obývat
nejjižnější	jižní	k2eAgNnSc1d3	nejjižnější
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nejstudenější	studený	k2eAgFnPc1d3	nejstudenější
končiny	končina	k1gFnPc1	končina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
tučňákovi	tučňák	k1gMnSc3	tučňák
královskému	královský	k2eAgMnSc3d1	královský
(	(	kIx(	(
<g/>
hnízdícímu	hnízdící	k2eAgInSc3d1	hnízdící
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Macquarie	Macquarie	k1gFnSc2	Macquarie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
řada	řada	k1gFnSc1	řada
odborníků	odborník	k1gMnPc2	odborník
považuje	považovat	k5eAaImIp3nS	považovat
oba	dva	k4xCgMnPc4	dva
tučňáky	tučňák	k1gMnPc4	tučňák
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
vzezřením	vzezření	k1gNnSc7	vzezření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
výrazné	výrazný	k2eAgFnPc4d1	výrazná
zlatavě-žluté	zlatavě-žlutý	k2eAgFnPc4d1	zlatavě-žlutý
chocholky	chocholka	k1gFnPc4	chocholka
<g/>
.	.	kIx.	.
</s>
<s>
Záda	záda	k1gNnPc1	záda
a	a	k8xC	a
tváře	tvář	k1gFnPc1	tvář
má	mít	k5eAaImIp3nS	mít
černé	černý	k2eAgNnSc1d1	černé
<g/>
,	,	kIx,	,
a	a	k8xC	a
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
těla	tělo	k1gNnSc2	tělo
pak	pak	k6eAd1	pak
kontrastně	kontrastně	k6eAd1	kontrastně
bílou	bílý	k2eAgFnSc7d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
průměrné	průměrný	k2eAgFnPc4d1	průměrná
hmotnosti	hmotnost	k1gFnPc4	hmotnost
5	[number]	k4	5
kg	kg	kA	kg
<g/>
,	,	kIx,	,
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
70	[number]	k4	70
cm	cm	kA	cm
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
samici	samice	k1gFnSc4	samice
vzhledově	vzhledově	k6eAd1	vzhledově
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bývá	bývat	k5eAaImIp3nS	bývat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgFnSc1d2	vyšší
a	a	k8xC	a
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
zobák	zobák	k1gInSc1	zobák
má	mít	k5eAaImIp3nS	mít
podstatně	podstatně	k6eAd1	podstatně
robustnější	robustní	k2eAgFnSc1d2	robustnější
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
tučňáci	tučňák	k1gMnPc1	tučňák
není	být	k5eNaImIp3nS	být
schopen	schopit	k5eAaPmNgInS	schopit
letu	let	k1gInSc3	let
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
své	svůj	k3xOyFgNnSc4	svůj
vakovité	vakovitý	k2eAgNnSc4d1	vakovité
tělo	tělo	k1gNnSc4	tělo
a	a	k8xC	a
tuhá	tuhý	k2eAgNnPc4d1	tuhé
zploštělá	zploštělý	k2eAgNnPc4d1	zploštělé
křídla	křídlo	k1gNnPc4	křídlo
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
využije	využít	k5eAaPmIp3nS	využít
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
stráví	strávit	k5eAaPmIp3nS	strávit
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
polovinu	polovina	k1gFnSc4	polovina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
potravou	potrava	k1gFnSc7	potrava
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
strava	strava	k1gFnSc1	strava
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
různých	různý	k2eAgMnPc2d1	různý
korýšů	korýš	k1gMnPc2	korýš
(	(	kIx(	(
<g/>
především	především	k9	především
krilu	krilat	k5eAaPmIp1nS	krilat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malých	malý	k2eAgFnPc2d1	malá
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
případně	případně	k6eAd1	případně
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
každý	každý	k3xTgMnSc1	každý
tučňák	tučňák	k1gMnSc1	tučňák
obměnuje	obměnovat	k5eAaPmIp3nS	obměnovat
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
své	svůj	k3xOyFgFnSc3	svůj
peří	peřit	k5eAaImIp3nS	peřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
týdny	týden	k1gInPc7	týden
odkázán	odkázán	k2eAgInSc4d1	odkázán
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
a	a	k8xC	a
hladovění	hladovění	k1gNnSc6	hladovění
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
obrovských	obrovský	k2eAgFnPc6d1	obrovská
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolu	spolu	k6eAd1	spolu
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
období	období	k1gNnSc4	období
hnízdění	hnízdění	k1gNnSc2	hnízdění
pak	pak	k6eAd1	pak
migruje	migrovat	k5eAaImIp3nS	migrovat
i	i	k9	i
do	do	k7c2	do
tisíce	tisíc	k4xCgInPc1	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hojný	hojný	k2eAgInSc1d1	hojný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
milionů	milion	k4xCgInPc2	milion
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
jako	jako	k9	jako
zranitelný	zranitelný	k2eAgMnSc1d1	zranitelný
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
zaznamenáván	zaznamenávat	k5eAaImNgInS	zaznamenávat
pokles	pokles	k1gInSc1	pokles
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
tučňáků	tučňák	k1gMnPc2	tučňák
žlutorohém	žlutorohý	k2eAgInSc6d1	žlutorohý
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
zmínil	zmínit	k5eAaPmAgMnS	zmínit
německý	německý	k2eAgMnSc1d1	německý
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Johann	Johann	k1gMnSc1	Johann
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Brandt	Brandt	k2eAgInSc4d1	Brandt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
spatřil	spatřit	k5eAaPmAgMnS	spatřit
na	na	k7c6	na
Falklandských	Falklandský	k2eAgInPc6d1	Falklandský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
Eudyptes	Eudyptesa	k1gFnPc2	Eudyptesa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgMnSc1d1	známý
chocholatými	chocholatý	k2eAgFnPc7d1	chocholatá
tučňáky	tučňák	k1gMnPc4	tučňák
žijícími	žijící	k2eAgFnPc7d1	žijící
na	na	k7c6	na
skalnatých	skalnatý	k2eAgInPc6d1	skalnatý
útesech	útes	k1gInPc6	útes
<g/>
.	.	kIx.	.
</s>
<s>
Rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
Eudyptes	Eudyptesa	k1gFnPc2	Eudyptesa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
ze	z	k7c2	z
starořeckých	starořecký	k2eAgNnPc2d1	starořecké
slov	slovo	k1gNnPc2	slovo
eu	eu	k?	eu
–	–	k?	–
"	"	kIx"	"
<g/>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
dyptes	dyptes	k1gInSc1	dyptes
–	–	k?	–
"	"	kIx"	"
<g/>
potápěč	potápěč	k1gMnSc1	potápěč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druhový	k2eAgNnSc1d1	druhové
jméno	jméno	k1gNnSc1	jméno
chrysolophus	chrysolophus	k1gInSc1	chrysolophus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
rovněž	rovněž	k9	rovněž
ze	z	k7c2	z
starořečtiny	starořečtina	k1gFnSc2	starořečtina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ze	z	k7c2	z
samotných	samotný	k2eAgNnPc2d1	samotné
slov	slovo	k1gNnPc2	slovo
chryse	chrys	k1gMnSc2	chrys
–	–	k?	–
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
lophos	lophos	k1gInSc1	lophos
–	–	k?	–
"	"	kIx"	"
<g/>
hřeben	hřeben	k1gInSc1	hřeben
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
specifické	specifický	k2eAgFnPc4d1	specifická
chocholky	chocholka	k1gFnPc4	chocholka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
cizích	cizí	k2eAgInPc6d1	cizí
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
nazývaný	nazývaný	k2eAgInSc4d1	nazývaný
makarony	makaron	k1gInPc4	makaron
(	(	kIx(	(
<g/>
či	či	k8xC	či
macaroni	macaron	k1gMnPc1	macaron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
maccaronism	maccaronism	k6eAd1	maccaronism
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc4	termín
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
asi	asi	k9	asi
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
honosně	honosně	k6eAd1	honosně
zdobený	zdobený	k2eAgInSc1d1	zdobený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgNnSc2	tento
pojmenování	pojmenování	k1gNnSc2	pojmenování
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
od	od	k7c2	od
námořníků	námořník	k1gMnPc2	námořník
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zalíbil	zalíbit	k5eAaPmAgInS	zalíbit
jeho	jeho	k3xOp3gInSc1	jeho
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
hřebínek	hřebínek	k1gInSc1	hřebínek
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
spatřili	spatřit	k5eAaPmAgMnP	spatřit
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
Falklandských	Falklandský	k2eAgInPc2d1	Falklandský
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobné	podobný	k2eAgFnPc4d1	podobná
skladby	skladba	k1gFnPc4	skladba
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tomuto	tento	k3xDgInSc3	tento
druhu	druh	k1gInSc3	druh
asi	asi	k9	asi
nejblíže	blízce	k6eAd3	blízce
příbuzný	příbuzný	k2eAgMnSc1d1	příbuzný
tučňák	tučňák	k1gMnSc1	tučňák
královský	královský	k2eAgMnSc1d1	královský
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
eudyptes	eudyptes	k1gInSc4	eudyptes
schlegeli	schleget	k5eAaBmAgMnP	schleget
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
někteří	některý	k3yIgMnPc1	některý
ornitologové	ornitolog	k1gMnPc1	ornitolog
označit	označit	k5eAaPmF	označit
tučňáka	tučňák	k1gMnSc4	tučňák
královského	královský	k2eAgMnSc4d1	královský
za	za	k7c4	za
poddruh	poddruh	k1gInSc4	poddruh
tučňáka	tučňák	k1gMnSc2	tučňák
žlutorohého	žlutorohý	k2eAgMnSc2d1	žlutorohý
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
Heardově	Heardův	k2eAgInSc6d1	Heardův
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
ostrově	ostrov	k1gInSc6	ostrov
Marion	Marion	k1gInSc4	Marion
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
křížil	křížit	k5eAaImAgMnS	křížit
s	s	k7c7	s
poddruhem	poddruh	k1gInSc7	poddruh
tučňáka	tučňák	k1gMnSc2	tučňák
skalního	skalní	k2eAgMnSc2d1	skalní
(	(	kIx(	(
<g/>
eudyptes	eudyptes	k1gInSc1	eudyptes
chrysochome	chrysochom	k1gInSc5	chrysochom
filholi	filhole	k1gFnSc4	filhole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
zjistila	zjistit	k5eAaPmAgFnS	zjistit
antarktická	antarktický	k2eAgFnSc1d1	antarktická
expedice	expedice	k1gFnSc1	expedice
Australian	Australian	k1gMnSc1	Australian
National	National	k1gMnSc1	National
Research	Research	k1gMnSc1	Research
v	v	k7c6	v
letech	let	k1gInPc6	let
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
několik	několik	k4yIc4	několik
hybridů	hybrid	k1gInPc2	hybrid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejhojnější	hojný	k2eAgInSc4d3	nejhojnější
druh	druh	k1gInSc4	druh
své	svůj	k3xOyFgFnSc2	svůj
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
jeho	jeho	k3xOp3gInSc2	jeho
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
velmi	velmi	k6eAd1	velmi
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
hnízdiště	hnízdiště	k1gNnPc1	hnízdiště
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
polárním	polární	k2eAgInSc6d1	polární
klimatickém	klimatický	k2eAgInSc6d1	klimatický
pásu	pás	k1gInSc6	pás
na	na	k7c6	na
skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
plochách	plocha	k1gFnPc6	plocha
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgFnSc1d1	jižní
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
,	,	kIx,	,
Crozetovy	Crozetův	k2eAgInPc4d1	Crozetův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Falklandské	Falklandský	k2eAgInPc4d1	Falklandský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Kergueleny	Kerguelen	k2eAgInPc4d1	Kerguelen
<g/>
,	,	kIx,	,
Heardův	Heardův	k2eAgInSc4d1	Heardův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
McDonaldovy	McDonaldův	k2eAgInPc4d1	McDonaldův
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc4	ostrov
prince	princ	k1gMnSc2	princ
Edwarda	Edward	k1gMnSc2	Edward
a	a	k8xC	a
Bouvetův	Bouvetův	k2eAgInSc4d1	Bouvetův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
větší	veliký	k2eAgFnPc1d2	veliký
kolonie	kolonie	k1gFnPc1	kolonie
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Jižní	jižní	k2eAgFnSc2d1	jižní
Shetlandy	Shetlanda	k1gFnSc2	Shetlanda
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnPc4d1	jižní
Orkneje	Orkneje	k1gFnPc4	Orkneje
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
ostrovech	ostrov	k1gInPc6	ostrov
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
jižního	jižní	k2eAgNnSc2d1	jižní
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
také	také	k9	také
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
až	až	k9	až
na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
kontinentě	kontinent	k1gInSc6	kontinent
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
období	období	k1gNnSc6	období
mimo	mimo	k6eAd1	mimo
hnízdní	hnízdní	k2eAgMnSc1d1	hnízdní
není	být	k5eNaImIp3nS	být
přesné	přesný	k2eAgNnSc1d1	přesné
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
době	doba	k1gFnSc6	doba
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neexistují	existovat	k5eNaImIp3nP	existovat
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
údaje	údaj	k1gInPc1	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
migrovat	migrovat	k5eAaImF	migrovat
až	až	k9	až
na	na	k7c4	na
břehy	břeh	k1gInPc4	břeh
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
ostatním	ostatní	k2eAgInPc3d1	ostatní
druhům	druh	k1gInPc3	druh
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Eudyptes	Eudyptesa	k1gFnPc2	Eudyptesa
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
vysoký	vysoký	k2eAgInSc1d1	vysoký
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
pohlaví	pohlaví	k1gNnSc4	pohlaví
–	–	k?	–
samice	samice	k1gFnSc1	samice
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgNnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hmotnost	hmotnost	k1gFnSc4	hmotnost
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
kg	kg	kA	kg
<g/>
,	,	kIx,	,
po	po	k7c4	po
inkubaci	inkubace	k1gFnSc4	inkubace
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přepeření	přepeření	k1gNnSc1	přepeření
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hmotnosti	hmotnost	k1gFnSc2	hmotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
před	před	k7c7	před
pelicháním	pelichání	k1gNnSc7	pelichání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
6,5	[number]	k4	6,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
mívá	mívat	k5eAaImIp3nS	mívat
vždy	vždy	k6eAd1	vždy
asi	asi	k9	asi
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
kilogram	kilogram	k1gInSc4	kilogram
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Zobák	zobák	k1gInSc1	zobák
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
8	[number]	k4	8
cm	cm	kA	cm
tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
nepatrně	nepatrně	k6eAd1	nepatrně
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
ramenou	rameno	k1gNnPc6	rameno
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
špičce	špička	k1gFnSc3	špička
asi	asi	k9	asi
20	[number]	k4	20
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
a	a	k8xC	a
ocásek	ocásek	k1gInSc1	ocásek
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
asi	asi	k9	asi
jen	jen	k9	jen
10	[number]	k4	10
cm	cm	kA	cm
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
<g/>
Hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
bradu	brada	k1gFnSc4	brada
a	a	k8xC	a
krk	krk	k1gInSc4	krk
má	mít	k5eAaImIp3nS	mít
černé	černý	k2eAgNnSc1d1	černé
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
ohraničené	ohraničený	k2eAgFnPc1d1	ohraničená
kontrastním	kontrastní	k2eAgFnPc3d1	kontrastní
bílím	bílit	k5eAaImIp1nS	bílit
peřím	peří	k1gNnSc7	peří
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
přepeření	přepeření	k1gNnSc6	přepeření
získá	získat	k5eAaPmIp3nS	získat
černé	černý	k2eAgNnSc1d1	černé
peří	peřit	k5eAaImIp3nS	peřit
modravý	modravý	k2eAgInSc4d1	modravý
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dlouho	dlouho	k6eAd1	dlouho
neobměněný	obměněný	k2eNgInSc1d1	obměněný
šat	šat	k1gInSc1	šat
je	být	k5eAaImIp3nS	být
viditelně	viditelně	k6eAd1	viditelně
opotřebený	opotřebený	k2eAgMnSc1d1	opotřebený
<g/>
,	,	kIx,	,
a	a	k8xC	a
místy	místy	k6eAd1	místy
až	až	k9	až
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
rysem	rys	k1gInSc7	rys
je	být	k5eAaImIp3nS	být
zlatožlutý	zlatožlutý	k2eAgInSc1d1	zlatožlutý
hřebínek	hřebínek	k1gInSc1	hřebínek
na	na	k7c4	na
středu	středa	k1gFnSc4	středa
čela	čelo	k1gNnSc2	čelo
sahající	sahající	k2eAgInSc1d1	sahající
po	po	k7c4	po
temeno	temeno	k1gNnSc4	temeno
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
visící	visící	k2eAgMnSc1d1	visící
dolů	dolů	k6eAd1	dolů
za	za	k7c4	za
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Ploutvovitá	ploutvovitý	k2eAgNnPc4d1	ploutvovitý
křídla	křídlo	k1gNnPc4	křídlo
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
hřbetní	hřbetní	k2eAgFnSc6d1	hřbetní
straně	strana	k1gFnSc6	strana
modročerná	modročerný	k2eAgFnSc1d1	modročerná
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
zadní	zadní	k2eAgFnSc7d1	zadní
hranou	hrana	k1gFnSc7	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Zespodu	zespodu	k6eAd1	zespodu
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
celá	celý	k2eAgNnPc4d1	celé
bílá	bílé	k1gNnPc4	bílé
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
červenohnědý	červenohnědý	k2eAgInSc1d1	červenohnědý
zobák	zobák	k1gInSc1	zobák
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
růžovou	růžový	k2eAgFnSc4d1	růžová
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
červené	červený	k2eAgFnPc1d1	červená
a	a	k8xC	a
nohy	noha	k1gFnPc1	noha
včetně	včetně	k7c2	včetně
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
plovacích	plovací	k2eAgFnPc2d1	plovací
blan	blána	k1gFnPc2	blána
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
krátkozrací	krátkozraký	k2eAgMnPc1d1	krátkozraký
<g/>
,	,	kIx,	,
zrak	zrak	k1gInSc4	zrak
mají	mít	k5eAaImIp3nP	mít
přizpůsobený	přizpůsobený	k2eAgInSc1d1	přizpůsobený
k	k	k7c3	k
výbornému	výborný	k2eAgNnSc3d1	výborné
vidění	vidění	k1gNnSc3	vidění
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
<g/>
Mládí	mládí	k1gNnSc6	mládí
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
dospělých	dospělí	k1gMnPc2	dospělí
snadno	snadno	k6eAd1	snadno
rozeznatelní	rozeznatelný	k2eAgMnPc1d1	rozeznatelný
<g/>
.	.	kIx.	.
</s>
<s>
Opeřeni	opeřen	k2eAgMnPc1d1	opeřen
jsou	být	k5eAaImIp3nP	být
zprvu	zprvu	k6eAd1	zprvu
šedě	šedě	k6eAd1	šedě
a	a	k8xC	a
následně	následně	k6eAd1	následně
čokoládově	čokoládově	k6eAd1	čokoládově
hnědě	hnědě	k6eAd1	hnědě
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
nedefinovaný	definovaný	k2eNgInSc4d1	nedefinovaný
vzor	vzor	k1gInSc4	vzor
s	s	k7c7	s
nerozvinutými	rozvinutý	k2eNgFnPc7d1	nerozvinutá
chocholky	chocholka	k1gFnPc1	chocholka
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zobák	zobák	k1gInSc4	zobák
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgNnPc1d2	menší
a	a	k8xC	a
užší	úzký	k2eAgNnPc1d2	užší
(	(	kIx(	(
<g/>
nedostatečně	dostatečně	k6eNd1	dostatečně
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prostě	prostě	k6eAd1	prostě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
převážně	převážně	k6eAd1	převážně
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zcela	zcela	k6eAd1	zcela
dospělého	dospělý	k2eAgNnSc2d1	dospělé
vzezření	vzezření	k1gNnSc2	vzezření
se	se	k3xPyFc4	se
vybarví	vybarvit	k5eAaPmIp3nS	vybarvit
asi	asi	k9	asi
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
tří	tři	k4xCgNnPc2	tři
až	až	k9	až
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
na	na	k7c6	na
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
<g/>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
obměňuje	obměňovat	k5eAaImIp3nS	obměňovat
své	svůj	k3xOyFgNnSc1	svůj
peří	peří	k1gNnSc1	peří
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
zdlouhavý	zdlouhavý	k2eAgInSc1d1	zdlouhavý
(	(	kIx(	(
<g/>
až	až	k8xS	až
čtyř	čtyři	k4xCgMnPc2	čtyři
týdenní	týdenní	k2eAgFnSc3d1	týdenní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
odkázán	odkázat	k5eAaPmNgInS	odkázat
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
pobytu	pobyt	k1gInSc3	pobyt
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
a	a	k8xC	a
hladoví	hladovět	k5eAaImIp3nS	hladovět
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
než	než	k8xS	než
takové	takový	k3xDgNnSc1	takový
období	období	k1gNnSc1	období
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
vykrmuje	vykrmovat	k5eAaImIp3nS	vykrmovat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
nashromáždí	nashromáždit	k5eAaPmIp3nP	nashromáždit
si	se	k3xPyFc3	se
tukové	tukový	k2eAgFnPc1d1	tuková
zásoby	zásoba	k1gFnPc1	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přepeření	přepeření	k1gNnSc6	přepeření
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
nakrmit	nakrmit	k5eAaPmF	nakrmit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
hnízdišti	hnízdiště	k1gNnSc6	hnízdiště
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
až	až	k9	až
za	za	k7c4	za
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
nastávajícím	nastávající	k2eAgNnSc6d1	nastávající
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
==	==	k?	==
</s>
</p>
<p>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
mořskou	mořský	k2eAgFnSc7d1	mořská
potravou	potrava	k1gFnSc7	potrava
(	(	kIx(	(
<g/>
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
různých	různý	k2eAgMnPc2d1	různý
korýšů	korýš	k1gMnPc2	korýš
<g/>
,	,	kIx,	,
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
podíl	podíl	k1gInSc4	podíl
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
lokalita	lokalita	k1gFnSc1	lokalita
nebo	nebo	k8xC	nebo
roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
tah	tah	k1gInSc4	tah
může	moct	k5eAaImIp3nS	moct
ulovit	ulovit	k5eAaPmF	ulovit
200	[number]	k4	200
až	až	k9	až
700	[number]	k4	700
gramů	gram	k1gInPc2	gram
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
živí	živit	k5eAaImIp3nS	živit
krilem	kril	k1gInSc7	kril
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
krunýřovkou	krunýřovka	k1gFnSc7	krunýřovka
krillovou	krillová	k1gFnSc7	krillová
(	(	kIx(	(
<g/>
Ephausia	Ephausia	k1gFnSc1	Ephausia
superba	superba	k1gMnSc1	superba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
toho	ten	k3xDgMnSc4	ten
také	také	k9	také
již	již	k6eAd1	již
zmíněnými	zmíněný	k2eAgMnPc7d1	zmíněný
hlavonožci	hlavonožec	k1gMnPc7	hlavonožec
a	a	k8xC	a
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
druhy	druh	k1gInPc1	druh
jakož	jakož	k8xC	jakož
je	být	k5eAaImIp3nS	být
<g/>
;	;	kIx,	;
Notothenia	Notothenium	k1gNnSc2	Notothenium
rossi	rosse	k1gFnSc4	rosse
<g/>
,	,	kIx,	,
Lepidonotothen	Lepidonotothna	k1gFnPc2	Lepidonotothna
larseni	larsit	k5eAaPmNgMnP	larsit
<g/>
,	,	kIx,	,
Champsocephalus	Champsocephalus	k1gInSc4	Champsocephalus
gunneri	gunner	k1gFnSc2	gunner
<g/>
,	,	kIx,	,
lampovník	lampovník	k1gInSc4	lampovník
Anderssonův	Anderssonův	k2eAgInSc4d1	Anderssonův
(	(	kIx(	(
<g/>
Krefftichthys	Krefftichthys	k1gInSc4	Krefftichthys
anderssoni	andersson	k1gMnPc1	andersson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lampovníček	lampovníček	k1gInSc1	lampovníček
Tenisonův	Tenisonův	k2eAgInSc1d1	Tenisonův
(	(	kIx(	(
<g/>
Protomyctophum	Protomyctophum	k1gInSc1	Protomyctophum
tenisoni	tenisoň	k1gFnSc3	tenisoň
<g/>
)	)	kIx)	)
či	či	k8xC	či
lampovníček	lampovníček	k1gInSc4	lampovníček
Normanův	Normanův	k2eAgInSc4d1	Normanův
(	(	kIx(	(
<g/>
Protomyctophum	Protomyctophum	k1gInSc4	Protomyctophum
normani	norman	k1gMnPc1	norman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
jedinci	jedinec	k1gMnPc1	jedinec
polykají	polykat	k5eAaImIp3nP	polykat
menší	malý	k2eAgInPc4d2	menší
kamínky	kamínek	k1gInPc4	kamínek
(	(	kIx(	(
<g/>
asi	asi	k9	asi
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
10	[number]	k4	10
až	až	k9	až
30	[number]	k4	30
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jim	on	k3xPp3gMnPc3	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
trávení	trávení	k1gNnSc6	trávení
a	a	k8xC	a
rozmělňování	rozmělňování	k1gNnSc6	rozmělňování
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
tak	tak	k6eAd1	tak
dostávají	dostávat	k5eAaImIp3nP	dostávat
i	i	k9	i
parazity	parazit	k1gMnPc4	parazit
z	z	k7c2	z
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Potravu	potrava	k1gFnSc4	potrava
loví	lovit	k5eAaImIp3nP	lovit
zpravidla	zpravidla	k6eAd1	zpravidla
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
tedy	tedy	k9	tedy
za	za	k7c2	za
rozbřesku	rozbřesk	k1gInSc2	rozbřesk
a	a	k8xC	a
do	do	k7c2	do
setmění	setmění	k1gNnSc2	setmění
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
mláďata	mládě	k1gNnPc4	mládě
nakrmit	nakrmit	k5eAaPmF	nakrmit
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
potomci	potomek	k1gMnPc1	potomek
rostou	růst	k5eAaImIp3nP	růst
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
se	se	k3xPyFc4	se
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
krmení	krmení	k1gNnSc4	krmení
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
tak	tak	k9	tak
přicházejí	přicházet	k5eAaImIp3nP	přicházet
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
i	i	k9	i
za	za	k7c4	za
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
dny	den	k1gInPc4	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
podnikají	podnikat	k5eAaImIp3nP	podnikat
delší	dlouhý	k2eAgFnPc4d2	delší
trasy	trasa	k1gFnPc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
chovu	chov	k1gInSc2	chov
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
časová	časový	k2eAgFnSc1d1	časová
prodleva	prodleva	k1gFnSc1	prodleva
nejpatrnější	patrný	k2eAgFnSc1d3	nejpatrnější
<g/>
.	.	kIx.	.
</s>
<s>
Tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
ročně	ročně	k6eAd1	ročně
údajně	údajně	k6eAd1	údajně
nejvíce	nejvíce	k6eAd1	nejvíce
potravy	potrava	k1gFnSc2	potrava
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
mořských	mořský	k2eAgInPc2d1	mořský
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
asi	asi	k9	asi
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
krilu	kril	k1gInSc2	kril
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
na	na	k7c4	na
50	[number]	k4	50
až	až	k9	až
300	[number]	k4	300
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
15	[number]	k4	15
až	až	k9	až
70	[number]	k4	70
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokáže	dokázat	k5eAaPmIp3nS	dokázat
se	se	k3xPyFc4	se
potopit	potopit	k5eAaPmF	potopit
až	až	k9	až
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc1	kořist
loví	lovit	k5eAaImIp3nS	lovit
způsobem	způsob	k1gInSc7	způsob
podobnému	podobný	k2eAgNnSc3d1	podobné
písmenu	písmeno	k1gNnSc3	písmeno
"	"	kIx"	"
<g/>
V	V	kA	V
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
značí	značit	k5eAaImIp3nS	značit
ponoření	ponoření	k1gNnSc4	ponoření
<g/>
,	,	kIx,	,
uchopení	uchopení	k1gNnSc4	uchopení
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
opětovný	opětovný	k2eAgInSc4d1	opětovný
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
průměrně	průměrně	k6eAd1	průměrně
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
2	[number]	k4	2
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
a	a	k8xC	a
plavě	plavě	k6eAd1	plavě
obvykle	obvykle	k6eAd1	obvykle
rychlostí	rychlost	k1gFnSc7	rychlost
7,5	[number]	k4	7,5
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Mimo	mimo	k7c4	mimo
období	období	k1gNnSc4	období
hnízdění	hnízdění	k1gNnSc2	hnízdění
loví	lovit	k5eAaImIp3nP	lovit
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
hlubších	hluboký	k2eAgFnPc6d2	hlubší
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
než	než	k8xS	než
<g/>
-li	i	k?	-li
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
efektivnější	efektivní	k2eAgNnSc1d2	efektivnější
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíš	nejspíš	k9	nejspíš
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
období	období	k1gNnSc6	období
migrace	migrace	k1gFnSc2	migrace
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hnízdění	hnízdění	k1gNnSc2	hnízdění
a	a	k8xC	a
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přirození	přirozený	k2eAgMnPc1d1	přirozený
predátoři	predátor	k1gMnPc1	predátor
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
moři	moře	k1gNnSc6	moře
se	se	k3xPyFc4	se
tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
obává	obávat	k5eAaImIp3nS	obávat
převážně	převážně	k6eAd1	převážně
mořských	mořský	k2eAgMnPc2d1	mořský
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
je	být	k5eAaImIp3nS	být
tuleň	tuleň	k1gMnSc1	tuleň
leopardí	leopardí	k2eAgMnSc1d1	leopardí
(	(	kIx(	(
<g/>
Hydrurga	Hydrurga	k1gFnSc1	Hydrurga
leptonyx	leptonyx	k1gInSc1	leptonyx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lachtan	lachtan	k1gMnSc1	lachtan
antarktický	antarktický	k2eAgMnSc1d1	antarktický
(	(	kIx(	(
<g/>
Arctocephalus	Arctocephalus	k1gMnSc1	Arctocephalus
gazella	gazella	k1gMnSc1	gazella
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lachtan	lachtan	k1gMnSc1	lachtan
jižní	jižní	k2eAgMnSc1d1	jižní
(	(	kIx(	(
<g/>
Arctocephalus	Arctocephalus	k1gInSc1	Arctocephalus
tropicalis	tropicalis	k1gFnSc2	tropicalis
<g/>
)	)	kIx)	)
či	či	k8xC	či
kosatka	kosatka	k1gFnSc1	kosatka
dravá	dravý	k2eAgFnSc1d1	dravá
(	(	kIx(	(
<g/>
Orcinus	Orcinus	k1gMnSc1	Orcinus
orca	orca	k1gMnSc1	orca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
pak	pak	k6eAd1	pak
nemá	mít	k5eNaImIp3nS	mít
praktický	praktický	k2eAgInSc4d1	praktický
žádné	žádný	k3yNgNnSc1	žádný
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
vejce	vejce	k1gNnSc1	vejce
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
hledáčku	hledáček	k1gInSc2	hledáček
místních	místní	k2eAgMnPc2d1	místní
dravých	dravý	k2eAgMnPc2d1	dravý
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
štítonos	štítonos	k1gInSc1	štítonos
světlezobý	světlezobý	k2eAgInSc1d1	světlezobý
(	(	kIx(	(
<g/>
Chionis	Chionis	k1gInSc1	Chionis
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chaluha	chaluha	k1gFnSc1	chaluha
subantarktická	subantarktický	k2eAgFnSc1d1	subantarktický
(	(	kIx(	(
<g/>
Stercorarius	Stercorarius	k1gMnSc1	Stercorarius
antarcticus	antarcticus	k1gMnSc1	antarcticus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buřňák	buřňák	k1gMnSc1	buřňák
obrovský	obrovský	k2eAgMnSc1d1	obrovský
(	(	kIx(	(
<g/>
Macronectes	Macronectes	k1gMnSc1	Macronectes
giganteus	giganteus	k1gMnSc1	giganteus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
racek	racek	k1gMnSc1	racek
jižní	jižní	k2eAgMnSc1d1	jižní
(	(	kIx(	(
<g/>
Larus	Larus	k1gMnSc1	Larus
dominicanus	dominicanus	k1gMnSc1	dominicanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Komunikace	komunikace	k1gFnSc1	komunikace
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
===	===	k?	===
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
chocholatých	chocholatý	k2eAgMnPc2d1	chocholatý
tučňáků	tučňák	k1gMnPc2	tučňák
i	i	k9	i
tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
velkých	velká	k1gFnPc6	velká
ale	ale	k8xC	ale
především	především	k6eAd1	především
těsných	těsný	k2eAgFnPc6d1	těsná
koloniích	kolonie	k1gFnPc6	kolonie
(	(	kIx(	(
<g/>
v	v	k7c4	v
jedno	jeden	k4xCgNnSc4	jeden
metru	metr	k1gInSc6	metr
čtverečním	čtvereční	k2eAgInSc6d1	čtvereční
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nacházet	nacházet	k5eAaImF	nacházet
až	až	k9	až
tři	tři	k4xCgInPc4	tři
hnízdící	hnízdící	k2eAgInPc4d1	hnízdící
páry	pár	k1gInPc4	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
široký	široký	k2eAgInSc1d1	široký
repertoár	repertoár	k1gInSc1	repertoár
vizuální	vizuální	k2eAgFnSc2d1	vizuální
a	a	k8xC	a
vokální	vokální	k2eAgFnSc2d1	vokální
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
<g/>
Své	svůj	k3xOyFgNnSc1	svůj
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
,	,	kIx,	,
snesená	snesený	k2eAgNnPc4d1	snesené
vejce	vejce	k1gNnPc4	vejce
či	či	k8xC	či
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
,	,	kIx,	,
hájí	hájit	k5eAaImIp3nP	hájit
svým	svůj	k3xOyFgInSc7	svůj
mohutným	mohutný	k2eAgInSc7d1	mohutný
zobákem	zobák	k1gInSc7	zobák
tomu	ten	k3xDgNnSc3	ten
náležitě	náležitě	k6eAd1	náležitě
vyvinutým	vyvinutý	k2eAgMnPc3d1	vyvinutý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvílích	chvíle	k1gFnPc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jedinec	jedinec	k1gMnSc1	jedinec
sedí	sedit	k5eAaImIp3nS	sedit
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
nebo	nebo	k8xC	nebo
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
již	již	k6eAd1	již
vylíhlá	vylíhlý	k2eAgNnPc4d1	vylíhlé
ptáčata	ptáče	k1gNnPc4	ptáče
<g/>
,	,	kIx,	,
brání	bránit	k5eAaImIp3nS	bránit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
zobákem	zobák	k1gInSc7	zobák
ohání	ohánět	k5eAaImIp3nS	ohánět
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
hnul	hnout	k5eAaPmAgMnS	hnout
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ale	ale	k8xC	ale
situace	situace	k1gFnSc1	situace
dovolí	dovolit	k5eAaPmIp3nS	dovolit
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
si	se	k3xPyFc3	se
taktéž	taktéž	k?	taktéž
křídly	křídlo	k1gNnPc7	křídlo
nebo	nebo	k8xC	nebo
celým	celý	k2eAgNnSc7d1	celé
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
tučňáky	tučňák	k1gMnPc7	tučňák
nebo	nebo	k8xC	nebo
predátory	predátor	k1gMnPc7	predátor
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
agresivního	agresivní	k2eAgNnSc2d1	agresivní
jednání	jednání	k1gNnSc2	jednání
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dopouští	dopouštět	k5eAaImIp3nS	dopouštět
celkem	celkem	k6eAd1	celkem
běžně	běžně	k6eAd1	běžně
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
samotná	samotný	k2eAgFnSc1d1	samotná
hrozba	hrozba	k1gFnSc1	hrozba
soupeři	soupeř	k1gMnSc3	soupeř
okamžitě	okamžitě	k6eAd1	okamžitě
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vyběhnutím	vyběhnutí	k1gNnSc7	vyběhnutí
na	na	k7c4	na
soka	sok	k1gMnSc4	sok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
klovnutím	klovnutí	k1gNnSc7	klovnutí
zobáku	zobák	k1gInSc2	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Ozývají	ozývat	k5eAaImIp3nP	ozývat
se	se	k3xPyFc4	se
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
hlasitými	hlasitý	k2eAgInPc7d1	hlasitý
výkřiky	výkřik	k1gInPc7	výkřik
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
se	se	k3xPyFc4	se
pták	pták	k1gMnSc1	pták
otáčí	otáčet	k5eAaImIp3nS	otáčet
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
soka	sok	k1gMnSc4	sok
dobře	dobře	k6eAd1	dobře
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
bližších	blízký	k2eAgNnPc2d2	bližší
křídel	křídlo	k1gNnPc2	křídlo
natahuje	natahovat	k5eAaImIp3nS	natahovat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
případně	případně	k6eAd1	případně
ztíží	ztížet	k5eAaPmIp3nS	ztížet
soupeřův	soupeřův	k2eAgInSc4d1	soupeřův
výpad	výpad	k1gInSc4	výpad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
projevují	projevovat	k5eAaImIp3nP	projevovat
vůči	vůči	k7c3	vůči
sousedním	sousední	k2eAgMnPc3d1	sousední
ptákům	pták	k1gMnPc3	pták
agresivním	agresivní	k2eAgNnSc7d1	agresivní
chováním	chování	k1gNnSc7	chování
<g/>
,	,	kIx,	,
při	pře	k1gFnSc4	pře
po	po	k7c6	po
bytu	byt	k1gInSc6	byt
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
pak	pak	k9	pak
svá	svůj	k3xOyFgNnPc4	svůj
interakční	interakční	k2eAgNnPc4d1	interakční
chování	chování	k1gNnPc4	chování
otupí	otupit	k5eAaPmIp3nP	otupit
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
spolu	spolu	k6eAd1	spolu
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
potravy	potrava	k1gFnSc2	potrava
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
loví	lovit	k5eAaImIp3nS	lovit
každý	každý	k3xTgMnSc1	každý
sám	sám	k3xTgMnSc1	sám
za	za	k7c2	za
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hnízdění	hnízdění	k1gNnPc1	hnízdění
a	a	k8xC	a
chov	chov	k1gInSc1	chov
mláďat	mládě	k1gNnPc2	mládě
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnSc4	hnízdiště
připlouvají	připlouvat	k5eAaImIp3nP	připlouvat
nejdříve	dříve	k6eAd3	dříve
samci	samec	k1gMnPc1	samec
koncem	koncem	k7c2	koncem
října	říjen	k1gInSc2	říjen
nebo	nebo	k8xC	nebo
počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
půl	půl	k6eAd1	půl
roku	rok	k1gInSc2	rok
oddělené	oddělený	k2eAgFnSc2d1	oddělená
páry	pára	k1gFnSc2	pára
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
s	s	k7c7	s
hnízděním	hnízdění	k1gNnSc7	hnízdění
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
hnízda	hnízdo	k1gNnSc2	hnízdo
jsou	být	k5eAaImIp3nP	být
rychle	rychle	k6eAd1	rychle
hotovi	hotov	k2eAgMnPc1d1	hotov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
mělký	mělký	k2eAgInSc1d1	mělký
důlek	důlek	k1gInSc1	důlek
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
kruhovými	kruhový	k2eAgInPc7d1	kruhový
pohyby	pohyb	k1gInPc7	pohyb
břicha	břicho	k1gNnSc2	břicho
a	a	k8xC	a
hrabáním	hrabání	k1gNnSc7	hrabání
noh	noha	k1gFnPc2	noha
ohraničený	ohraničený	k2eAgInSc1d1	ohraničený
několika	několik	k4yIc7	několik
kamínky	kamínka	k1gNnPc7	kamínka
a	a	k8xC	a
dřívky	dřívko	k1gNnPc7	dřívko
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vystlaný	vystlaný	k2eAgInSc4d1	vystlaný
blátem	bláto	k1gNnSc7	bláto
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdiště	hnízdiště	k1gNnPc1	hnízdiště
bývají	bývat	k5eAaImIp3nP	bývat
i	i	k9	i
na	na	k7c6	na
strmém	strmý	k2eAgInSc6d1	strmý
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
stovky	stovka	k1gFnPc1	stovka
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
kamenité	kamenitý	k2eAgFnSc6d1	kamenitá
suti	suť	k1gFnSc6	suť
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
přeplněná	přeplněný	k2eAgNnPc1d1	přeplněné
<g/>
,	,	kIx,	,
2	[number]	k4	2
až	až	k8xS	až
3	[number]	k4	3
hnízda	hnízdo	k1gNnSc2	hnízdo
připadají	připadat	k5eAaImIp3nP	připadat
na	na	k7c4	na
1	[number]	k4	1
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
sousedským	sousedský	k2eAgInPc3d1	sousedský
sporům	spor	k1gInPc3	spor
<g/>
.	.	kIx.	.
<g/>
Námluvy	námluva	k1gFnSc2	námluva
<g/>
,	,	kIx,	,
tokání	tokání	k1gNnSc1	tokání
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
hnízda	hnízdo	k1gNnSc2	hnízdo
a	a	k8xC	a
páření	páření	k1gNnSc2	páření
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
10	[number]	k4	10
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
kolonii	kolonie	k1gFnSc6	kolonie
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
činnosti	činnost	k1gFnPc1	činnost
synchronizovány	synchronizovat	k5eAaBmNgFnP	synchronizovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
samice	samice	k1gFnPc1	samice
začnou	začít	k5eAaPmIp3nP	začít
počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
snášet	snášet	k5eAaImF	snášet
prvá	prvý	k4xOgNnPc1	prvý
vejce	vejce	k1gNnPc1	vejce
současně	současně	k6eAd1	současně
a	a	k8xC	a
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
druhé	druhý	k4xOgFnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvá	prvý	k4xOgNnPc1	prvý
vejce	vejce	k1gNnPc1	vejce
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
přibližně	přibližně	k6eAd1	přibližně
jen	jen	k9	jen
60	[number]	k4	60
%	%	kIx~	%
velikosti	velikost	k1gFnSc3	velikost
vajec	vejce	k1gNnPc2	vejce
druhých	druhý	k4xOgInPc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Prvé	prvý	k4xOgInPc1	prvý
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc1d2	menší
vejce	vejce	k1gNnSc1	vejce
nebývá	bývat	k5eNaImIp3nS	bývat
oplodněno	oplodněn	k2eAgNnSc1d1	oplodněno
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
během	během	k7c2	během
hnízdění	hnízdění	k1gNnSc2	hnízdění
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
vysezená	vysezený	k2eAgNnPc4d1	vysezené
obě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
jedno	jeden	k4xCgNnSc4	jeden
mládě	mládě	k1gNnSc4	mládě
menší	malý	k2eAgMnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
má	mít	k5eAaImIp3nS	mít
pár	pár	k4xCyI	pár
jedno	jeden	k4xCgNnSc1	jeden
mládě	mládě	k1gNnSc1	mládě
<g/>
.	.	kIx.	.
<g/>
Inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
během	během	k7c2	během
prvé	prvý	k4xOgFnSc2	prvý
třetiny	třetina	k1gFnSc2	třetina
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
vejce	vejce	k1gNnPc4	vejce
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
samec	samec	k1gMnSc1	samec
odejde	odejít	k5eAaPmIp3nS	odejít
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
třetinu	třetina	k1gFnSc4	třetina
odejde	odejít	k5eAaPmIp3nS	odejít
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
samice	samice	k1gFnSc2	samice
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
s	s	k7c7	s
potravou	potrava	k1gFnSc7	potrava
až	až	k6eAd1	až
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc1	mládě
vylíhla	vylíhnout	k5eAaPmAgNnP	vylíhnout
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gInSc4	samec
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
mládě	mládě	k1gNnSc4	mládě
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
ráno	ráno	k6eAd1	ráno
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
s	s	k7c7	s
potravou	potrava	k1gFnSc7	potrava
pro	pro	k7c4	pro
mládě	mládě	k1gNnSc4	mládě
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
denně	denně	k6eAd1	denně
asi	asi	k9	asi
200	[number]	k4	200
g.	g.	k?	g.
Po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
týdnech	týden	k1gInPc6	týden
mláďatům	mládě	k1gNnPc3	mládě
narůstá	narůstat	k5eAaImIp3nS	narůstat
již	již	k6eAd1	již
hřejivější	hřejivý	k2eAgNnSc1d2	hřejivější
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízda	hnízdo	k1gNnPc1	hnízdo
a	a	k8xC	a
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
se	se	k3xPyFc4	se
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
školek	školka	k1gFnPc2	školka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
odcházejí	odcházet	k5eAaImIp3nP	odcházet
a	a	k8xC	a
krmí	krmit	k5eAaImIp3nP	krmit
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
až	až	k9	až
1	[number]	k4	1
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
11	[number]	k4	11
týdnech	týden	k1gInPc6	týden
věku	věk	k1gInSc2	věk
jim	on	k3xPp3gMnPc3	on
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
definitivní	definitivní	k2eAgNnSc4d1	definitivní
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
zlatožlutý	zlatožlutý	k2eAgInSc4d1	zlatožlutý
hřebínek	hřebínek	k1gInSc4	hřebínek
<g/>
,	,	kIx,	,
a	a	k8xC	a
odcházejí	odcházet	k5eAaImIp3nP	odcházet
sami	sám	k3xTgMnPc1	sám
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgFnSc2d1	sexuální
zralosti	zralost	k1gFnSc2	zralost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mladí	mladý	k2eAgMnPc1d1	mladý
samci	samec	k1gMnPc1	samec
v	v	k7c4	v
6	[number]	k4	6
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
v	v	k7c6	v
5	[number]	k4	5
létech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
<g/>
Rodiče	rodič	k1gMnPc1	rodič
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
třítýdenní	třítýdenní	k2eAgFnSc4d1	třítýdenní
výkrmnou	výkrmný	k2eAgFnSc4d1	výkrmná
kúru	kúra	k1gFnSc4	kúra
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
a	a	k8xC	a
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
během	během	k7c2	během
25	[number]	k4	25
dní	den	k1gInPc2	den
přepeří	přepeřit	k5eAaPmIp3nS	přepeřit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
odcházejí	odcházet	k5eAaImIp3nP	odcházet
odděleně	odděleně	k6eAd1	odděleně
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
cca	cca	kA	cca
za	za	k7c4	za
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
červeného	červený	k2eAgInSc2d1	červený
seznamu	seznam	k1gInSc2	seznam
IUCN	IUCN	kA	IUCN
byla	být	k5eAaImAgFnS	být
populace	populace	k1gFnSc1	populace
tučňáka	tučňák	k1gMnSc2	tučňák
žlutorohého	žlutorohý	k2eAgInSc2d1	žlutorohý
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
zranitelný	zranitelný	k2eAgInSc4d1	zranitelný
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gFnSc1	jeho
současná	současný	k2eAgFnSc1d1	současná
populace	populace	k1gFnSc1	populace
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
počtu	počet	k1gInSc2	počet
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
párů	pár	k1gInPc2	pár
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
největší	veliký	k2eAgFnSc7d3	veliký
<g/>
,	,	kIx,	,
za	za	k7c4	za
posledních	poslední	k2eAgInPc2d1	poslední
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
stavy	stav	k1gInPc1	stav
značně	značně	k6eAd1	značně
snížily	snížit	k5eAaPmAgInP	snížit
–	–	k?	–
v	v	k7c4	v
Jižní	jižní	k2eAgFnSc4d1	jižní
Georgii	Georgie	k1gFnSc4	Georgie
klesla	klesnout	k5eAaPmAgFnS	klesnout
populace	populace	k1gFnSc1	populace
až	až	k9	až
o	o	k7c4	o
50	[number]	k4	50
<g/>
%	%	kIx~	%
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
až	až	k9	až
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
byla	být	k5eAaImAgFnS	být
i	i	k8xC	i
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgNnSc2d1	jižní
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odborníků	odborník	k1gMnPc2	odborník
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
kolísavou	kolísavý	k2eAgFnSc7d1	kolísavá
potravní	potravní	k2eAgFnSc7d1	potravní
dostupností	dostupnost	k1gFnSc7	dostupnost
<g/>
,	,	kIx,	,
vlivem	vliv	k1gInSc7	vliv
komerčních	komerční	k2eAgInPc2d1	komerční
rybolovů	rybolov	k1gInPc2	rybolov
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
–	–	k?	–
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgInPc1	takový
zásahy	zásah	k1gInPc1	zásah
narušují	narušovat	k5eAaImIp3nP	narušovat
reprodukční	reprodukční	k2eAgFnPc4d1	reprodukční
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
plodnost	plodnost	k1gFnSc4	plodnost
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Macaroni	Macaroň	k1gFnSc3	Macaroň
penguin	penguin	k1gInSc4	penguin
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
VESELOVSKÝ	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Tučňáci	tučňák	k1gMnPc1	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zvířata	zvíře	k1gNnPc1	zvíře
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WILLIAMS	WILLIAMS	kA	WILLIAMS
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
D.	D.	kA	D.
<g/>
;	;	kIx,	;
WILSON	WILSON	kA	WILSON
<g/>
,	,	kIx,	,
Rory	Rory	k1gInPc7	Rory
P.	P.	kA	P.
The	The	k1gMnPc2	The
penguins	penguinsa	k1gFnPc2	penguinsa
:	:	kIx,	:
Spheniscidae	Spheniscidae	k1gFnPc2	Spheniscidae
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bird	Bird	k1gMnSc1	Bird
Families	Families	k1gMnSc1	Families
of	of	k?	of
the	the	k?	the
World	World	k1gMnSc1	World
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
19854667	[number]	k4	19854667
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mořský	mořský	k2eAgInSc1d1	mořský
život	život	k1gInSc1	život
<g/>
:	:	kIx,	:
Tučňák	tučňák	k1gMnSc1	tučňák
žlutorohý	žlutorohý	k2eAgMnSc1d1	žlutorohý
</s>
</p>
