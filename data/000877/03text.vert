<s>
Ponoj	Ponoj	k1gInSc1	Ponoj
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
П	П	k?	П
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
na	na	k7c6	na
Kolském	Kolský	k2eAgInSc6d1	Kolský
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
Murmanské	murmanský	k2eAgFnSc6d1	Murmanská
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
426	[number]	k4	426
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
15	[number]	k4	15
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
vysočiny	vysočina	k1gFnSc2	vysočina
Kejvy	Kejva	k1gFnSc2	Kejva
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Bílého	bílý	k2eAgNnSc2d1	bílé
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
Terském	Terský	k2eAgInSc6d1	Terský
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
teče	téct	k5eAaImIp3nS	téct
v	v	k7c6	v
nízkých	nízký	k2eAgInPc6d1	nízký
březích	břeh	k1gInPc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
protéká	protékat	k5eAaImIp3nS	protékat
mezi	mezi	k7c7	mezi
výběžky	výběžek	k1gInPc7	výběžek
Kejvy	Kejva	k1gFnSc2	Kejva
kaňonovým	kaňonův	k2eAgNnSc7d1	kaňonův
údolím	údolí	k1gNnSc7	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Koryto	koryto	k1gNnSc1	koryto
je	být	k5eAaImIp3nS	být
plné	plný	k2eAgNnSc1d1	plné
peřejí	peřej	k1gFnSc7	peřej
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Kolmacká	Kolmacký	k2eAgFnSc1d1	Kolmacký
peřej	peřej	k1gFnSc1	peřej
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
sněhový	sněhový	k2eAgInSc1d1	sněhový
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
až	až	k8xS	až
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
voda	voda	k1gFnSc1	voda
opadá	opadat	k5eAaBmIp3nS	opadat
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
je	být	k5eAaImIp3nS	být
175	[number]	k4	175
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgNnSc1d1	maximální
až	až	k6eAd1	až
3500	[number]	k4	3500
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
až	až	k8xS	až
začátku	začátek	k1gInSc2	začátek
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
splavná	splavný	k2eAgFnSc1d1	splavná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
П	П	k?	П
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ponoj	Ponoj	k1gInSc1	Ponoj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
