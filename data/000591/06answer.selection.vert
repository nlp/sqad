<s>
Protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
též	též	k9	též
atomové	atomový	k2eAgNnSc4d1	atomové
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
daného	daný	k2eAgInSc2d1	daný
atomu	atom	k1gInSc2	atom
či	či	k8xC	či
obecně	obecně	k6eAd1	obecně
atomů	atom	k1gInPc2	atom
daného	daný	k2eAgInSc2d1	daný
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
