<s>
Protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
též	též	k9	též
atomové	atomový	k2eAgNnSc4d1	atomové
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
daného	daný	k2eAgInSc2d1	daný
atomu	atom	k1gInSc2	atom
či	či	k8xC	či
obecně	obecně	k6eAd1	obecně
atomů	atom	k1gInPc2	atom
daného	daný	k2eAgInSc2d1	daný
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
souvisí	souviset	k5eAaImIp3nS	souviset
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
též	též	k9	též
hmotové	hmotový	k2eAgNnSc1d1	hmotové
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
všech	všecek	k3xTgInPc2	všecek
nukleonů	nukleon	k1gInPc2	nukleon
<g/>
)	)	kIx)	)
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektricky	elektricky	k6eAd1	elektricky
neutrálním	neutrální	k2eAgInSc6d1	neutrální
atomu	atom	k1gInSc6	atom
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
rovná	rovnat	k5eAaImIp3nS	rovnat
počtu	počet	k1gInSc2	počet
elektronů	elektron	k1gInPc2	elektron
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
základní	základní	k2eAgInSc1d1	základní
počet	počet	k1gInSc1	počet
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
atomech	atom	k1gInPc6	atom
daného	daný	k2eAgInSc2d1	daný
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Protonové	protonový	k2eAgNnSc4d1	protonové
číslo	číslo	k1gNnSc4	číslo
obvykle	obvykle	k6eAd1	obvykle
označujeme	označovat	k5eAaImIp1nP	označovat
symbolem	symbol	k1gInSc7	symbol
Z.	Z.	kA	Z.
Např.	např.	kA	např.
pro	pro	k7c4	pro
uran	uran	k1gInSc4	uran
platí	platit	k5eAaImIp3nS	platit
Z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
92	[number]	k4	92
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zápisu	zápis	k1gInSc6	zápis
daného	daný	k2eAgInSc2d1	daný
prvku	prvek	k1gInSc2	prvek
se	se	k3xPyFc4	se
protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
vlevo	vlevo	k6eAd1	vlevo
dolů	dolů	k6eAd1	dolů
před	před	k7c4	před
symbol	symbol	k1gInSc4	symbol
(	(	kIx(	(
<g/>
značku	značka	k1gFnSc4	značka
<g/>
)	)	kIx)	)
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
92U	[number]	k4	92U
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
uran	uran	k1gInSc4	uran
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
označovalo	označovat	k5eAaImAgNnS	označovat
hlavně	hlavně	k9	hlavně
polohu	poloha	k1gFnSc4	poloha
prvku	prvek	k1gInSc2	prvek
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
periodická	periodický	k2eAgFnSc1d1	periodická
tabulka	tabulka	k1gFnSc1	tabulka
seřadila	seřadit	k5eAaPmAgFnS	seřadit
podle	podle	k7c2	podle
atomové	atomový	k2eAgFnSc2d1	atomová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
na	na	k7c4	na
špatné	špatný	k2eAgNnSc4d1	špatné
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jód	jód	k1gInSc4	jód
a	a	k8xC	a
tellur	tellur	k1gInSc4	tellur
by	by	kYmCp3nS	by
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
tabulce	tabulka	k1gFnSc6	tabulka
měly	mít	k5eAaImAgFnP	mít
prohozená	prohozený	k2eAgNnPc4d1	prohozené
místa	místo	k1gNnPc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
udávající	udávající	k2eAgNnPc1d1	udávající
pořadí	pořadí	k1gNnSc4	pořadí
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
chemickým	chemický	k2eAgFnPc3d1	chemická
vlastnostem	vlastnost	k1gFnPc3	vlastnost
jsou	být	k5eAaImIp3nP	být
právě	právě	k6eAd1	právě
atomová	atomový	k2eAgNnPc1d1	atomové
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
pouze	pouze	k6eAd1	pouze
zhruba	zhruba	k6eAd1	zhruba
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
atomové	atomový	k2eAgFnSc2d1	atomová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nesoulad	nesoulad	k1gInSc1	nesoulad
se	se	k3xPyFc4	se
vysvětlil	vysvětlit	k5eAaPmAgInS	vysvětlit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Henry	henry	k1gInSc1	henry
Moseley	Moselea	k1gFnSc2	Moselea
objevil	objevit	k5eAaPmAgInS	objevit
závislost	závislost	k1gFnSc4	závislost
umístění	umístění	k1gNnSc2	umístění
prvku	prvek	k1gInSc2	prvek
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
rentgenovém	rentgenový	k2eAgNnSc6d1	rentgenové
difrakčním	difrakční	k2eAgNnSc6d1	difrakční
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
elektrickém	elektrický	k2eAgInSc6d1	elektrický
náboji	náboj	k1gInSc6	náboj
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
počtu	počet	k1gInSc2	počet
(	(	kIx(	(
<g/>
kladně	kladně	k6eAd1	kladně
nabitých	nabitý	k2eAgInPc2d1	nabitý
<g/>
)	)	kIx)	)
protonů	proton	k1gInPc2	proton
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
tento	tento	k3xDgInSc4	tento
náboj	náboj	k1gInSc4	náboj
(	(	kIx(	(
<g/>
potažmo	potažmo	k6eAd1	potažmo
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
atomová	atomový	k2eAgFnSc1d1	atomová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
určuje	určovat	k5eAaImIp3nS	určovat
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
