<s>
Pak	pak	k6eAd1	pak
Čong-hui	Čongui	k1gNnSc1	Čong-hui
(	(	kIx(	(
<g/>
박	박	k?	박
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1917	[number]	k4	1917
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jihokorejský	jihokorejský	k2eAgMnSc1d1	jihokorejský
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961-1979	[number]	k4	1961-1979
byl	být	k5eAaImAgInS	být
diktátorem	diktátor	k1gMnSc7	diktátor
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963-1979	[number]	k4	1963-1979
oficiálně	oficiálně	k6eAd1	oficiálně
prezidentem	prezident	k1gMnSc7	prezident
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
vojenským	vojenský	k2eAgInSc7d1	vojenský
převratem	převrat	k1gInSc7	převrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
pokusila	pokusit	k5eAaPmAgFnS	pokusit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Modrý	modrý	k2eAgInSc4d1	modrý
dům	dům	k1gInSc4	dům
zavraždit	zavraždit	k5eAaPmF	zavraždit
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
diktaturu	diktatura	k1gFnSc4	diktatura
utužil	utužit	k5eAaPmAgMnS	utužit
zejména	zejména	k9	zejména
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
učinil	učinit	k5eAaPmAgInS	učinit
jihokorejskou	jihokorejský	k2eAgFnSc4d1	jihokorejská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejsilnějších	silný	k2eAgInPc2d3	nejsilnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
ho	on	k3xPp3gNnSc2	on
časopis	časopis	k1gInSc1	časopis
Time	Time	k1gNnSc2	Time
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
10	[number]	k4	10
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
Asiatů	Asiat	k1gMnPc2	Asiat
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
prvního	první	k4xOgMnSc2	první
jihokorejského	jihokorejský	k2eAgMnSc2d1	jihokorejský
prezidenta	prezident	k1gMnSc2	prezident
I	i	k8xC	i
Sung-mana	Sungan	k1gMnSc2	Sung-man
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
zmítala	zmítat	k5eAaImAgFnS	zmítat
v	v	k7c6	v
chaosu	chaos	k1gInSc6	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
novému	nový	k2eAgMnSc3d1	nový
prezidentu	prezident	k1gMnSc3	prezident
ani	ani	k9	ani
premiérovi	premiér	k1gMnSc3	premiér
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
sestavit	sestavit	k5eAaPmF	sestavit
funkční	funkční	k2eAgFnSc4d1	funkční
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
o	o	k7c4	o
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
I	i	k9	i
Sung-man	Sungan	k1gMnSc1	Sung-man
opíral	opírat	k5eAaImAgMnS	opírat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
demoralizovaná	demoralizovaný	k2eAgFnSc1d1	demoralizovaná
a	a	k8xC	a
studentské	studentský	k2eAgNnSc1d1	studentské
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
I	i	k8xC	i
Sung-mana	Sungan	k1gMnSc4	Sung-man
rezignovat	rezignovat	k5eAaBmF	rezignovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
revolucionalizovalo	revolucionalizovat	k5eAaBmAgNnS	revolucionalizovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
atmosféře	atmosféra	k1gFnSc6	atmosféra
provedl	provést	k5eAaPmAgMnS	provést
Pak	pak	k6eAd1	pak
Čong-hui	Čongu	k1gFnSc3	Čong-hu
vojenský	vojenský	k2eAgInSc4d1	vojenský
puč	puč	k1gInSc4	puč
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nově	nově	k6eAd1	nově
zřízená	zřízený	k2eAgFnSc1d1	zřízená
tajná	tajný	k2eAgFnSc1d1	tajná
policie	policie	k1gFnSc1	policie
(	(	kIx(	(
<g/>
KCIA	KCIA	kA	KCIA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
pravomoc	pravomoc	k1gFnSc4	pravomoc
zatýkat	zatýkat	k5eAaImF	zatýkat
nepřátele	nepřítel	k1gMnPc4	nepřítel
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgFnSc2d1	běžná
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
mučení	mučení	k1gNnSc1	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
přání	přání	k1gNnSc4	přání
americké	americký	k2eAgFnSc2d1	americká
administrativy	administrativa	k1gFnSc2	administrativa
pak	pak	k6eAd1	pak
nicméně	nicméně	k8xC	nicméně
formálně	formálně	k6eAd1	formálně
zcivilnil	zcivilnit	k5eAaPmAgMnS	zcivilnit
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odejde	odejít	k5eAaPmIp3nS	odejít
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slib	slib	k1gInSc4	slib
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
poté	poté	k6eAd1	poté
režim	režim	k1gInSc4	režim
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
oddemokratičtěl	oddemokratičtět	k5eAaPmAgMnS	oddemokratičtět
<g/>
:	:	kIx,	:
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
stanné	stanný	k2eAgNnSc4d1	stanné
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
stanovil	stanovit	k5eAaPmAgInS	stanovit
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
Yushin	Yushina	k1gFnPc2	Yushina
a	a	k8xC	a
zesílil	zesílit	k5eAaPmAgMnS	zesílit
cenzuru	cenzura	k1gFnSc4	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
předtím	předtím	k6eAd1	předtím
však	však	k9	však
ústavně	ústavně	k6eAd1	ústavně
zaručené	zaručený	k2eAgFnSc2d1	zaručená
svobody	svoboda	k1gFnSc2	svoboda
byly	být	k5eAaImAgFnP	být
spíše	spíše	k9	spíše
formálními	formální	k2eAgFnPc7d1	formální
proklamacemi	proklamace	k1gFnPc7	proklamace
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
třikrát	třikrát	k6eAd1	třikrát
při	při	k7c6	při
přímých	přímý	k2eAgFnPc6d1	přímá
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
pokaždé	pokaždé	k6eAd1	pokaždé
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
<g/>
%	%	kIx~	%
hlasy	hlas	k1gInPc1	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
další	další	k2eAgFnSc1d1	další
přímá	přímý	k2eAgFnSc1d1	přímá
volba	volba	k1gFnSc1	volba
však	však	k9	však
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
volby	volba	k1gFnPc4	volba
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
nezměnila	změnit	k5eNaPmAgFnS	změnit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
vlády	vláda	k1gFnSc2	vláda
měl	mít	k5eAaImAgInS	mít
Pak	pak	k6eAd1	pak
ve	v	k7c6	v
veřejnosti	veřejnost	k1gFnSc6	veřejnost
spíše	spíše	k9	spíše
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
ji	on	k3xPp3gFnSc4	on
rychle	rychle	k6eAd1	rychle
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
demonstracemi	demonstrace	k1gFnPc7	demonstrace
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
organizovaly	organizovat	k5eAaBmAgInP	organizovat
především	především	k6eAd1	především
studenti	student	k1gMnPc1	student
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Pu-Ma	Pu-Ma	k1gFnSc1	Pu-Ma
nepokoje	nepokoj	k1gInSc2	nepokoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
400	[number]	k4	400
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
<g/>
,	,	kIx,	,
nepokoje	nepokoj	k1gInPc1	nepokoj
však	však	k9	však
neustávaly	ustávat	k5eNaImAgInP	ustávat
<g/>
,	,	kIx,	,
demonstranti	demonstrant	k1gMnPc1	demonstrant
začali	začít	k5eAaPmAgMnP	začít
útočit	útočit	k5eAaImF	útočit
na	na	k7c4	na
policejní	policejní	k2eAgFnPc4d1	policejní
stanice	stanice	k1gFnPc4	stanice
i	i	k8xC	i
sídla	sídlo	k1gNnPc4	sídlo
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vrcholných	vrcholný	k2eAgInPc2d1	vrcholný
nepokojů	nepokoj	k1gInPc2	nepokoj
byl	být	k5eAaImAgMnS	být
Pak	pak	k6eAd1	pak
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
šéfem	šéf	k1gMnSc7	šéf
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
Kim	Kim	k1gMnSc7	Kim
Čä-gjuem	Čäju	k1gMnSc7	Čä-gju
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
hájil	hájit	k5eAaImAgMnS	hájit
vlastenectvím	vlastenectví	k1gNnSc7	vlastenectví
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
překážkou	překážka	k1gFnSc7	překážka
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
atentátu	atentát	k1gInSc2	atentát
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
motivy	motiv	k1gInPc7	motiv
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
dodnes	dodnes	k6eAd1	dodnes
nejasné	jasný	k2eNgInPc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Pakova	Pakův	k2eAgFnSc1d1	Pakův
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
stála	stát	k5eAaImAgFnS	stát
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
jeho	jeho	k3xOp3gMnPc2	jeho
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
na	na	k7c6	na
vztazích	vztah	k1gInPc6	vztah
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souviselo	souviset	k5eAaImAgNnS	souviset
o	o	k7c6	o
jihokorejské	jihokorejský	k2eAgNnSc4d1	jihokorejské
angažmá	angažmá	k1gNnSc4	angažmá
ve	v	k7c6	v
vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
Pak	pak	k6eAd1	pak
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
Vietnamu	Vietnam	k1gInSc2	Vietnam
320	[number]	k4	320
tisíc	tisíc	k4xCgInPc2	tisíc
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Pak	pak	k6eAd1	pak
Kun-hje	Kunj	k1gMnPc4	Kun-hj
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zvolena	zvolit	k5eAaPmNgFnS	zvolit
prezidentkou	prezidentka	k1gFnSc7	prezidentka
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
úřad	úřad	k1gInSc1	úřad
převzala	převzít	k5eAaPmAgFnS	převzít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Pakův	Pakův	k2eAgInSc1d1	Pakův
hlavní	hlavní	k2eAgInSc1d1	hlavní
přínos	přínos	k1gInSc1	přínos
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vybudovat	vybudovat	k5eAaPmF	vybudovat
silně	silně	k6eAd1	silně
exportní	exportní	k2eAgFnSc1d1	exportní
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
přísně	přísně	k6eAd1	přísně
řízenou	řízený	k2eAgFnSc4d1	řízená
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
financoval	financovat	k5eAaBmAgInS	financovat
především	především	k6eAd1	především
z	z	k7c2	z
japonských	japonský	k2eAgFnPc2d1	japonská
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
normalizace	normalizace	k1gFnSc1	normalizace
korejsko-japonských	korejskoaponský	k2eAgInPc2d1	korejsko-japonský
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
smlouvou	smlouva	k1gFnSc7	smlouva
o	o	k7c6	o
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
vztazích	vztah	k1gInPc6	vztah
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
budilo	budit	k5eAaImAgNnS	budit
nechuť	nechuť	k1gFnSc4	nechuť
v	v	k7c6	v
korejské	korejský	k2eAgFnSc6d1	Korejská
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
japonské	japonský	k2eAgFnSc6d1	japonská
kolonizaci	kolonizace	k1gFnSc6	kolonizace
silně	silně	k6eAd1	silně
protijaponsky	protijaponsky	k6eAd1	protijaponsky
naladěná	naladěný	k2eAgFnSc1d1	naladěná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Korejské	korejský	k2eAgFnSc6d1	Korejská
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
nacházela	nacházet	k5eAaImAgFnS	nacházet
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
těžký	těžký	k2eAgInSc4d1	těžký
a	a	k8xC	a
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
silná	silný	k2eAgFnSc1d1	silná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Korejský	korejský	k2eAgInSc1d1	korejský
lid	lid	k1gInSc1	lid
byl	být	k5eAaImAgInS	být
odhodlaný	odhodlaný	k2eAgInSc1d1	odhodlaný
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
vidinou	vidina	k1gFnSc7	vidina
lepšího	dobrý	k2eAgInSc2d2	lepší
života	život	k1gInSc2	život
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
otázkou	otázka	k1gFnSc7	otázka
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
Čong-hui	Čongu	k1gFnSc2	Čong-hu
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
plánem	plán	k1gInSc7	plán
obrody	obroda	k1gFnSc2	obroda
ekonomiky	ekonomika	k1gFnSc2	ekonomika
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
zlehčí	zlehčit	k5eAaPmIp3nS	zlehčit
jeho	jeho	k3xOp3gInPc4	jeho
prohřešky	prohřešek	k1gInPc4	prohřešek
proti	proti	k7c3	proti
svobodě	svoboda	k1gFnSc3	svoboda
<g/>
,	,	kIx,	,
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
lidským	lidský	k2eAgNnPc3d1	lidské
právům	právo	k1gNnPc3	právo
<g/>
.	.	kIx.	.
</s>
<s>
Clifford	Clifford	k1gMnSc1	Clifford
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
L.	L.	kA	L.
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Troubled	Troubled	k1gMnSc1	Troubled
Tiger	Tiger	k1gMnSc1	Tiger
<g/>
:	:	kIx,	:
Businessmen	Businessmen	k1gInSc1	Businessmen
<g/>
,	,	kIx,	,
Bureaucrats	Bureaucrats	k1gInSc1	Bureaucrats
and	and	k?	and
Generals	Generals	k1gInSc1	Generals
in	in	k?	in
South	South	k1gInSc1	South
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Armonk	Armonk	k1gMnSc1	Armonk
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
M.	M.	kA	M.
<g/>
E.	E.	kA	E.
Sharpe	sharp	k1gInSc5	sharp
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7656	[number]	k4	7656
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
141	[number]	k4	141
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
<g/>
,	,	kIx,	,
Byung-kook	Byungook	k1gInSc1	Byung-kook
and	and	k?	and
Ezra	Ezr	k1gInSc2	Ezr
F.	F.	kA	F.
Vogel	Vogel	k1gMnSc1	Vogel
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Park	park	k1gInSc1	park
Chung	Chung	k1gMnSc1	Chung
Hee	Hee	k1gMnSc1	Hee
Era	Era	k1gMnSc1	Era
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Transformation	Transformation	k1gInSc1	Transformation
of	of	k?	of
South	South	k1gInSc1	South
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
674	[number]	k4	674
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5820	[number]	k4	5820
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
<g/>
,	,	kIx,	,
Hyung-A	Hyung-A	k1gMnSc1	Hyung-A
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korea	Korea	k1gFnSc1	Korea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Development	Development	k1gInSc1	Development
Under	Undra	k1gFnPc2	Undra
Park	park	k1gInSc1	park
Chung	Chung	k1gMnSc1	Chung
Hee	Hee	k1gMnSc1	Hee
<g/>
.	.	kIx.	.
</s>
<s>
Routledge	Routledge	k6eAd1	Routledge
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
32329	[number]	k4	32329
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kim	Kim	k?	Kim
<g/>
,	,	kIx,	,
Hyung-A	Hyung-A	k1gMnSc1	Hyung-A
and	and	k?	and
Clark	Clark	k1gInSc1	Clark
W.	W.	kA	W.
Sorensen	Sorensen	k1gInSc1	Sorensen
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Reassessing	Reassessing	k1gInSc1	Reassessing
the	the	k?	the
Park	park	k1gInSc1	park
Chung	Chung	k1gMnSc1	Chung
Hee	Hee	k1gMnSc1	Hee
Era	Era	k1gMnSc1	Era
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Korea	Korea	k1gFnSc1	Korea
Studies	Studies	k1gInSc1	Studies
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
295	[number]	k4	295
<g/>
-	-	kIx~	-
<g/>
99140	[number]	k4	99140
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
Byeong-cheon	Byeongheon	k1gInSc1	Byeong-cheon
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Developmental	Developmental	k1gMnSc1	Developmental
Dictatorship	Dictatorship	k1gMnSc1	Dictatorship
and	and	k?	and
The	The	k1gFnSc1	The
Park	park	k1gInSc1	park
Chung-Hee	Chung-Hee	k1gFnSc1	Chung-Hee
Era	Era	k1gMnSc1	Era
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Shaping	Shaping	k1gInSc1	Shaping
of	of	k?	of
Modernity	modernita	k1gFnSc2	modernita
in	in	k?	in
the	the	k?	the
Republic	Republice	k1gFnPc2	Republice
of	of	k?	of
Korea	Korea	k1gFnSc1	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Paramus	Paramus	k1gMnSc1	Paramus
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
:	:	kIx,	:
Homa	Homa	k1gFnSc1	Homa
&	&	k?	&
Sekey	Sekea	k1gFnSc2	Sekea
Books	Booksa	k1gFnPc2	Booksa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
931907	[number]	k4	931907
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pak	pak	k6eAd1	pak
Čong-hui	Čongue	k1gFnSc4	Čong-hue
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Heslo	heslo	k1gNnSc4	heslo
v	v	k7c6	v
Encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Britannica	Britannic	k1gInSc2	Britannic
</s>
