<s>
Mary	Mary	k1gFnSc1
McElroyová	McElroyová	k1gFnSc1
</s>
<s>
Mary	Mary	k1gFnSc1
McElroyová	McElroyová	k1gFnSc1
</s>
<s>
Neoficiální	neoficiální	k2eAgFnSc1d1,k2eNgFnSc1d1
první	první	k4xOgFnSc1
dáma	dáma	k1gFnSc1
USA	USA	kA
</s>
<s>
V	v	k7c6
roli	role	k1gFnSc6
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1881	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1885	#num#	k4
Předchůdkyně	předchůdkyně	k1gFnSc2
</s>
<s>
Ellen	Ellen	k1gInSc1
Arthurová	Arthurový	k2eAgFnSc1d1
Nástupkyně	nástupkyně	k1gFnSc1
</s>
<s>
Rose	Rose	k1gMnSc1
Clevelandová	Clevelandový	k2eAgFnSc1d1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1841	#num#	k4
Greenwich	Greenwich	k1gInSc4
New	New	k1gFnSc2
York	York	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1917	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Albany	Albana	k1gFnPc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Albany	Albana	k1gFnPc1
Rural	Rural	k1gInSc1
Cemetery	Cemeter	k1gInPc1
Choť	choť	k1gFnSc4
</s>
<s>
John	John	k1gMnSc1
McElroy	McElroa	k1gFnSc2
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Chester	Chester	k1gMnSc1
A.	A.	kA
Arthur	Arthur	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Albany	Albana	k1gFnPc1
Zaměstnání	zaměstnání	k1gNnSc2
</s>
<s>
První	první	k4xOgFnSc1
dáma	dáma	k1gFnSc1
USA	USA	kA
Profese	profese	k1gFnSc1
</s>
<s>
politička	politička	k1gFnSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Mary	Mary	k1gFnSc1
Arthur	Arthura	k1gFnPc2
McElroy	McElroa	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mary	Mary	k1gFnSc1
Arthurová	Arthurová	k1gFnSc1
McElroyová	McElroyová	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1841	#num#	k4
<g/>
,	,	kIx,
Greenwich	Greenwich	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
–	–	k?
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
Albany	Albana	k1gFnPc1
<g/>
,	,	kIx,
New	New	k1gFnPc1
York	York	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
sestrou	sestra	k1gFnSc7
21	#num#	k4
<g/>
.	.	kIx.
prezidenta	prezident	k1gMnSc4
USA	USA	kA
Chestera	Chester	k1gMnSc4
A.	A.	kA
Arthura	Arthur	k1gMnSc4
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1881	#num#	k4
až	až	k9
1885	#num#	k4
vykonávala	vykonávat	k5eAaImAgFnS
funkci	funkce	k1gFnSc4
první	první	k4xOgFnSc2
dámy	dáma	k1gFnSc2
USA	USA	kA
<g/>
,	,	kIx,
protože	protože	k8xS
Chester	Chester	k1gMnSc1
A.	A.	kA
Arthur	Arthur	k1gMnSc1
byl	být	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1880	#num#	k4
vdovec	vdovec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
GASSERT	GASSERT	kA
<g/>
,	,	kIx,
Philipp	Philipp	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnPc1
dámy	dáma	k1gFnPc1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
BRÁNA	brána	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7243	#num#	k4
<g/>
-	-	kIx~
<g/>
133	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
První	první	k4xOgFnSc2
dámy	dáma	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
s.	s.	k?
246	#num#	k4
<g/>
-	-	kIx~
<g/>
248	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
První	první	k4xOgFnPc1
dámy	dáma	k1gFnPc1
USA	USA	kA
</s>
<s>
Martha	Martha	k1gFnSc1
Washingtonová	Washingtonový	k2eAgFnSc1d1
•	•	k?
Abigail	Abigaila	k1gFnPc2
Adamsová	Adamsový	k2eAgFnSc1d1
•	•	k?
Martha	Martha	k1gFnSc1
Jeffersonová	Jeffersonová	k1gFnSc1
Randolphová	Randolphová	k1gFnSc1
•	•	k?
Dolley	Dollea	k1gFnSc2
Madisonová	Madisonová	k1gFnSc1
•	•	k?
Elizabeth	Elizabeth	k1gFnSc1
Monroeová	Monroeová	k1gFnSc1
•	•	k?
Louisa	Louisa	k?
Adamsová	Adamsová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Emily	Emil	k1gMnPc4
Donelsonová	Donelsonová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
Sarah	Sarah	k1gFnSc1
Yorke	Yorke	k1gFnSc1
Jacksonová	Jacksonová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rachel	Rachel	k1gInSc1
Jacksonová	Jacksonová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Angelica	Angelica	k1gFnSc1
Van	van	k1gInSc1
Burenová	Burenová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Hannah	Hannah	k1gInSc1
Van	van	k1gInSc1
Burenová	Burenová	k1gFnSc1
•	•	k?
Anna	Anna	k1gFnSc1
Harrisonová	Harrisonová	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
(	(	kIx(
<g/>
Jane	Jan	k1gMnSc5
Irwin	Irwin	k1gMnSc1
Harrisonová	Harrisonová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Letitia	Letitia	k1gFnSc1
Tylerová	Tylerová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Priscilla	Priscilla	k1gFnSc1
Tylerová	Tylerová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Julia	Julius	k1gMnSc2
Tylerová	Tylerový	k2eAgFnSc1d1
•	•	k?
Sarah	Sarah	k1gFnSc1
Polková	polkový	k2eAgFnSc1d1
•	•	k?
Margaret	Margareta	k1gFnPc2
Taylorová	Taylorová	k1gFnSc1
•	•	k?
Abigail	Abigail	k1gInSc1
Fillmoreová	Fillmoreová	k1gFnSc1
•	•	k?
Jane	Jan	k1gMnSc5
Pierceová	Pierceová	k1gFnSc1
•	•	k?
Harriet	Harrieta	k1gFnPc2
Laneová	Laneový	k2eAgFnSc1d1
•	•	k?
Mary	Mary	k1gFnSc1
Toddová	Toddová	k1gFnSc1
Lincolnová	Lincolnová	k1gFnSc1
•	•	k?
Eliza	Eliza	k1gFnSc1
McCardle	McCardle	k1gFnSc1
Johnsonová	Johnsonová	k1gFnSc1
•	•	k?
Julia	Julius	k1gMnSc2
Grantová	grantový	k2eAgFnSc1d1
•	•	k?
Lucy	Luca	k1gMnSc2
Hayesová	Hayesový	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Lucretia	Lucretia	k1gFnSc1
Garfieldová	Garfieldová	k1gFnSc1
•	•	k?
Ellen	Ellna	k1gFnPc2
Arthurová	Arthurový	k2eAgFnSc1d1
•	•	k?
(	(	kIx(
<g/>
Mary	Mary	k1gFnSc1
McElroyová	McElroyová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
(	(	kIx(
<g/>
Rose	Ros	k1gMnSc2
Clevelandová	Clevelandový	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Frances	Francesa	k1gFnPc2
Clevelandová	Clevelandový	k2eAgFnSc1d1
•	•	k?
Caroline	Carolin	k1gInSc5
Harrisonová	Harrisonová	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Mary	Mary	k1gFnSc1
Harrisonová	Harrisonová	k1gFnSc1
McKeeová	McKeeová	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Frances	Francesa	k1gFnPc2
Clevelandová	Clevelandový	k2eAgFnSc1d1
•	•	k?
Ida	Ida	k1gFnSc1
McKinleyová	McKinleyová	k1gFnSc1
•	•	k?
Edith	Edith	k1gInSc1
Rooseveltová	Rooseveltová	k1gFnSc1
•	•	k?
Helen	Helena	k1gFnPc2
Taftová	taftový	k2eAgFnSc1d1
•	•	k?
Ellen	Ellen	k1gInSc1
Wilsonová	Wilsonová	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Edith	Editha	k1gFnPc2
Wilsonová	Wilsonová	k1gFnSc1
•	•	k?
Florence	Florenc	k1gFnSc2
Hardingová	Hardingový	k2eAgFnSc1d1
•	•	k?
Grace	Grace	k1gFnSc1
Coolidgeová	Coolidgeový	k2eAgFnSc1d1
•	•	k?
Lou	Lou	k1gFnSc1
Hooverová	Hooverová	k1gFnSc1
•	•	k?
Eleanor	Eleanor	k1gInSc1
Rooseveltová	Rooseveltová	k1gFnSc1
•	•	k?
Bess	Bessa	k1gFnPc2
Trumanová	Trumanový	k2eAgFnSc1d1
•	•	k?
Mamie	Mamie	k1gFnSc1
Eisenhowerová	Eisenhowerová	k1gFnSc1
•	•	k?
Jacqueline	Jacquelin	k1gInSc5
Kennedyová	Kennedyový	k2eAgFnSc1d1
•	•	k?
Lady	lady	k1gFnSc1
Bird	Bird	k1gInSc1
Johnsonová	Johnsonová	k1gFnSc1
•	•	k?
Pat	pata	k1gFnPc2
Nixonová	Nixonový	k2eAgFnSc1d1
•	•	k?
Betty	Betty	k1gFnSc1
Fordová	Fordová	k1gFnSc1
•	•	k?
Rosalynn	Rosalynna	k1gFnPc2
Carterová	Carterový	k2eAgFnSc1d1
•	•	k?
Nancy	Nancy	k1gFnSc1
Reaganová	Reaganová	k1gFnSc1
•	•	k?
Barbara	Barbara	k1gFnSc1
Bushová	Bushová	k1gFnSc1
•	•	k?
Hillary	Hillara	k1gFnSc2
Clintonová	Clintonová	k1gFnSc1
•	•	k?
Laura	Laura	k1gFnSc1
Bushová	Bushová	k1gFnSc1
•	•	k?
Michelle	Michelle	k1gInSc1
Obamová	Obamová	k1gFnSc1
•	•	k?
Melania	Melanium	k1gNnSc2
Trumpová	Trumpová	k1gFnSc1
•	•	k?
Jill	Jill	k1gInSc1
Bidenová	Bidenová	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
