<s>
Prof.	prof.	kA	prof.
JUDr.	JUDr.	kA	JUDr.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
příležitostný	příležitostný	k2eAgMnSc1d1	příležitostný
publicista	publicista	k1gMnSc1	publicista
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
skládá	skládat	k5eAaImIp3nS	skládat
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
získal	získat	k5eAaPmAgMnS	získat
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc3	ocenění
<g/>
,	,	kIx,	,
věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
volné	volný	k2eAgFnSc3d1	volná
hudební	hudební	k2eAgFnSc3d1	hudební
tvorbě	tvorba	k1gFnSc3	tvorba
a	a	k8xC	a
kompozici	kompozice	k1gFnSc3	kompozice
hudby	hudba	k1gFnSc2	hudba
filmové	filmový	k2eAgFnSc2d1	filmová
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc2d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
<g/>
;	;	kIx,	;
druhou	druhý	k4xOgFnSc4	druhý
základní	základní	k2eAgFnSc4d1	základní
oblast	oblast	k1gFnSc4	oblast
jeho	jeho	k3xOp3gFnSc2	jeho
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
představuje	představovat	k5eAaImIp3nS	představovat
tvorba	tvorba	k1gFnSc1	tvorba
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
pedagog	pedagog	k1gMnSc1	pedagog
na	na	k7c6	na
Divadelní	divadelní	k2eAgFnSc6d1	divadelní
fakultě	fakulta	k1gFnSc6	fakulta
AMU	AMU	kA	AMU
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
J.	J.	kA	J.
E.	E.	kA	E.
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
kandidátem	kandidát	k1gMnSc7	kandidát
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
jej	on	k3xPp3gMnSc4	on
hnutí	hnutí	k1gNnPc2	hnutí
ANO	ano	k9	ano
nominovalo	nominovat	k5eAaBmAgNnS	nominovat
jako	jako	k9	jako
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
post	post	k1gInSc4	post
náměstka	náměstek	k1gMnSc2	náměstek
ministra	ministr	k1gMnSc2	ministr
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ministrovým	ministrův	k2eAgMnSc7d1	ministrův
poradcem	poradce	k1gMnSc7	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
českých	český	k2eAgNnPc2d1	české
i	i	k8xC	i
světových	světový	k2eAgNnPc2d1	světové
médií	médium	k1gNnPc2	médium
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
především	především	k6eAd1	především
svým	svůj	k3xOyFgNnSc7	svůj
nezvykle	zvykle	k6eNd1	zvykle
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
tetováním	tetování	k1gNnSc7	tetování
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xC	jako
jediné	jediný	k2eAgNnSc1d1	jediné
dítě	dítě	k1gNnSc1	dítě
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
elektroinženýra	elektroinženýr	k1gMnSc2	elektroinženýr
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k1gMnSc2	vedoucí
projektanta	projektant	k1gMnSc2	projektant
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
TESLA	Tesla	k1gFnSc1	Tesla
<g/>
,	,	kIx,	,
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Předci	předek	k1gMnPc1	předek
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Franze	Franze	k1gFnSc2	Franze
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
větví	větev	k1gFnPc2	větev
-	-	kIx~	-
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
a	a	k8xC	a
druhé	druhý	k4xOgInPc1	druhý
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
větev	větev	k1gFnSc4	větev
moravskou	moravský	k2eAgFnSc4d1	Moravská
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
pradědečků	pradědeček	k1gMnPc2	pradědeček
byl	být	k5eAaImAgInS	být
advokátem	advokát	k1gMnSc7	advokát
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
zakládal	zakládat	k5eAaImAgInS	zakládat
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
německém	německý	k2eAgInSc6d1	německý
Novém	nový	k2eAgInSc6d1	nový
Jičíně	Jičín	k1gInSc6	Jičín
první	první	k4xOgFnSc4	první
českou	český	k2eAgFnSc4d1	Česká
reálku	reálka	k1gFnSc4	reálka
a	a	k8xC	a
první	první	k4xOgNnSc1	první
české	český	k2eAgNnSc1d1	české
gymnázium	gymnázium	k1gNnSc1	gymnázium
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
Valašsko	Valašsko	k1gNnSc4	Valašsko
jako	jako	k8xC	jako
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
pro	pro	k7c4	pro
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
při	při	k7c6	při
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
pradědeček	pradědeček	k1gMnSc1	pradědeček
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
rodinné	rodinný	k2eAgFnSc2d1	rodinná
větve	větev	k1gFnSc2	větev
byl	být	k5eAaImAgInS	být
správcem	správce	k1gMnSc7	správce
rosického	rosický	k2eAgNnSc2d1	rosické
panství	panství	k1gNnSc2	panství
údajně	údajně	k6eAd1	údajně
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
z	z	k7c2	z
Ostrovačic	Ostrovačice	k1gFnPc2	Ostrovačice
Vilém	Vilém	k1gMnSc1	Vilém
Mrštík	Mrštík	k1gMnSc1	Mrštík
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
Franzových	Franzův	k2eAgFnPc2d1	Franzova
pratetiček	pratetička	k1gFnPc2	pratetička
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
slečnu	slečna	k1gFnSc4	slečna
z	z	k7c2	z
pošty	pošta	k1gFnSc2	pošta
v	v	k7c6	v
Mrštíkově	Mrštíkův	k2eAgFnSc6d1	Mrštíkova
knize	kniha	k1gFnSc6	kniha
Pohádka	pohádka	k1gFnSc1	pohádka
máje	máj	k1gInSc2	máj
<g/>
.	.	kIx.	.
</s>
<s>
Prastrýc	prastrýc	k1gMnSc1	prastrýc
Karel	Karel	k1gMnSc1	Karel
Franz	Franz	k1gMnSc1	Franz
byl	být	k5eAaImAgMnS	být
sanitní	sanitní	k2eAgMnSc1d1	sanitní
generál	generál	k1gMnSc1	generál
rakouské	rakouský	k2eAgFnSc2d1	rakouská
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
Dvořákovy	Dvořákův	k2eAgFnSc2d1	Dvořákova
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
příležitostně	příležitostně	k6eAd1	příležitostně
hrával	hrávat	k5eAaImAgMnS	hrávat
s	s	k7c7	s
Českým	český	k2eAgInSc7d1	český
kvartetem	kvartet	k1gInSc7	kvartet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
stal	stát	k5eAaPmAgInS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
</s>
<s>
Franzův	Franzův	k2eAgMnSc1d1	Franzův
dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
větve	větev	k1gFnSc2	větev
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
českým	český	k2eAgMnSc7d1	český
inženýrem	inženýr	k1gMnSc7	inženýr
ve	v	k7c6	v
Vítkovicích	Vítkovice	k1gInPc6	Vítkovice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
větev	větev	k1gFnSc4	větev
českou	český	k2eAgFnSc4d1	Česká
<g/>
,	,	kIx,	,
Franzův	Franzův	k2eAgMnSc1d1	Franzův
pradědeček	pradědeček	k1gMnSc1	pradědeček
byl	být	k5eAaImAgMnS	být
strojvůdce	strojvůdce	k1gMnSc1	strojvůdce
<g/>
,	,	kIx,	,
Němec	Němec	k1gMnSc1	Němec
z	z	k7c2	z
Českého	český	k2eAgInSc2d1	český
Krumlova	Krumlov	k1gInSc2	Krumlov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Březí	březí	k1gNnSc2	březí
u	u	k7c2	u
Žinkov	Žinkov	k1gInSc4	Žinkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
výroby	výroba	k1gFnSc2	výroba
barvených	barvený	k2eAgFnPc2d1	barvená
hraček	hračka	k1gFnPc2	hračka
v	v	k7c6	v
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
Skašově	Skašův	k2eAgNnSc6d1	Skašův
<g/>
;	;	kIx,	;
jejím	její	k3xOp3gMnSc7	její
vnukem	vnuk	k1gMnSc7	vnuk
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
(	(	kIx(	(
<g/>
Trnkova	Trnkův	k2eAgFnSc1d1	Trnkova
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
sestrou	sestra	k1gFnSc7	sestra
Franzova	Franzův	k2eAgMnSc2d1	Franzův
dědečka	dědeček	k1gMnSc2	dědeček
Otty	Otta	k1gMnSc2	Otta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
pradědeček	pradědeček	k1gMnSc1	pradědeček
byl	být	k5eAaImAgMnS	být
kotlářem	kotlář	k1gMnSc7	kotlář
v	v	k7c6	v
plzeňské	plzeňský	k2eAgFnSc6d1	Plzeňská
Škodovce	škodovka	k1gFnSc6	škodovka
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
byl	být	k5eAaImAgMnS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
president	president	k1gMnSc1	president
ČR	ČR	kA	ČR
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dětství	dětství	k1gNnSc6	dětství
u	u	k7c2	u
prarodičů	prarodič	k1gMnPc2	prarodič
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
Franz	Franz	k1gInSc4	Franz
jako	jako	k8xC	jako
na	na	k7c6	na
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
patricijském	patricijský	k2eAgInSc6d1	patricijský
bytě	byt	k1gInSc6	byt
obklopen	obklopit	k5eAaPmNgMnS	obklopit
obrazy	obraz	k1gInPc7	obraz
Václava	Václav	k1gMnSc2	Václav
Jansy	Jansa	k1gMnSc2	Jansa
<g/>
,	,	kIx,	,
Alfonse	Alfons	k1gMnSc2	Alfons
Muchy	Mucha	k1gMnSc2	Mucha
či	či	k8xC	či
Vlastimila	Vlastimil	k1gMnSc2	Vlastimil
Hofmana	Hofman	k1gMnSc2	Hofman
<g/>
,	,	kIx,	,
biedermeierovskými	biedermeierovský	k2eAgMnPc7d1	biedermeierovský
hracími	hrací	k2eAgMnPc7d1	hrací
hodiny	hodina	k1gFnPc4	hodina
s	s	k7c7	s
pastýřem	pastýř	k1gMnSc7	pastýř
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
hrály	hrát	k5eAaImAgFnP	hrát
árie	árie	k1gFnPc1	árie
z	z	k7c2	z
Normy	Norma	k1gFnSc2	Norma
či	či	k8xC	či
z	z	k7c2	z
Traviaty	Traviata	k1gFnSc2	Traviata
nebo	nebo	k8xC	nebo
bronzovými	bronzový	k2eAgFnPc7d1	bronzová
hodinami	hodina	k1gFnPc7	hodina
s	s	k7c7	s
Napoleonem	napoleon	k1gInSc7	napoleon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
po	po	k7c6	po
obědě	oběd	k1gInSc6	oběd
<g/>
,	,	kIx,	,
servírovaném	servírovaný	k2eAgInSc6d1	servírovaný
na	na	k7c6	na
rosenthalu	rosenthal	k1gInSc6	rosenthal
<g/>
,	,	kIx,	,
poslouchala	poslouchat	k5eAaImAgFnS	poslouchat
na	na	k7c6	na
klikovém	klikový	k2eAgInSc6d1	klikový
gramofonu	gramofon	k1gInSc6	gramofon
vážná	vážný	k2eAgFnSc1d1	vážná
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Schubert	Schubert	k1gMnSc1	Schubert
nebo	nebo	k8xC	nebo
Mahler	Mahler	k1gMnSc1	Mahler
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
jeho	on	k3xPp3gInSc4	on
dědeček	dědeček	k1gMnSc1	dědeček
přehrával	přehrávat	k5eAaImAgMnS	přehrávat
na	na	k7c4	na
cello	cello	k1gNnSc4	cello
Bachovu	Bachův	k2eAgFnSc4d1	Bachova
suitu	suita	k1gFnSc4	suita
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Franze	Franze	k1gFnSc2	Franze
nebyl	být	k5eNaImAgInS	být
podobný	podobný	k2eAgInSc1d1	podobný
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
"	"	kIx"	"
<g/>
žádná	žádný	k3yNgFnSc1	žádný
snobárna	snobárna	k1gFnSc1	snobárna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
organická	organický	k2eAgFnSc1d1	organická
forma	forma	k1gFnSc1	forma
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nenávratně	návratně	k6eNd1	návratně
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dotyčná	dotyčný	k2eAgFnSc1d1	dotyčná
generace	generace	k1gFnSc1	generace
"	"	kIx"	"
<g/>
ztratila	ztratit	k5eAaPmAgFnS	ztratit
potřebu	potřeba	k1gFnSc4	potřeba
předávat	předávat	k5eAaImF	předávat
ji	on	k3xPp3gFnSc4	on
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
naše	náš	k3xOp1gFnPc1	náš
generace	generace	k1gFnPc1	generace
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
generace	generace	k1gFnPc4	generace
našich	náš	k3xOp1gMnPc2	náš
rodičů	rodič	k1gMnPc2	rodič
schopnost	schopnost	k1gFnSc4	schopnost
ji	on	k3xPp3gFnSc4	on
přijímat	přijímat	k5eAaImF	přijímat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
věku	věk	k1gInSc2	věk
bydlel	bydlet	k5eAaImAgMnS	bydlet
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
patře	patro	k1gNnSc6	patro
rohového	rohový	k2eAgInSc2d1	rohový
domu	dům	k1gInSc2	dům
v	v	k7c6	v
Mostecké	mostecký	k2eAgFnSc6d1	Mostecká
ulici	ulice	k1gFnSc6	ulice
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
Malostranské	malostranský	k2eAgNnSc4d1	Malostranské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
bytu	byt	k1gInSc2	byt
na	na	k7c6	na
pražském	pražský	k2eAgInSc6d1	pražský
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
žije	žít	k5eAaImIp3nS	žít
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svých	svůj	k3xOyFgNnPc2	svůj
vysokoškolských	vysokoškolský	k2eAgNnPc2d1	vysokoškolské
studií	studio	k1gNnPc2	studio
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
Vysočanech	Vysočany	k1gInPc6	Vysočany
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Praze-Košířích	Praze-Košíř	k1gInPc6	Praze-Košíř
s	s	k7c7	s
přírodovědným	přírodovědný	k2eAgNnSc7d1	Přírodovědné
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
jeho	jeho	k3xOp3gInSc1	jeho
hluboký	hluboký	k2eAgInSc1d1	hluboký
a	a	k8xC	a
soustavný	soustavný	k2eAgInSc1d1	soustavný
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
botaniku	botanika	k1gFnSc4	botanika
a	a	k8xC	a
mineralogii	mineralogie	k1gFnSc4	mineralogie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
také	také	k9	také
intenzivně	intenzivně	k6eAd1	intenzivně
věnoval	věnovat	k5eAaImAgMnS	věnovat
lehké	lehký	k2eAgFnSc3d1	lehká
atletice	atletika	k1gFnSc3	atletika
a	a	k8xC	a
gymnastice	gymnastika	k1gFnSc3	gymnastika
(	(	kIx(	(
<g/>
cvičení	cvičení	k1gNnSc4	cvičení
na	na	k7c6	na
bradlech	bradla	k1gNnPc6	bradla
a	a	k8xC	a
hrazdě	hrazda	k1gFnSc6	hrazda
<g/>
)	)	kIx)	)
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
několik	několik	k4yIc4	několik
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
blíže	blízce	k6eAd2	blízce
nespecifikovaných	specifikovaný	k2eNgInPc2d1	nespecifikovaný
"	"	kIx"	"
<g/>
přeborů	přebor	k1gInPc2	přebor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
hodlal	hodlat	k5eAaImAgMnS	hodlat
studovat	studovat	k5eAaImF	studovat
hudební	hudební	k2eAgFnSc4d1	hudební
a	a	k8xC	a
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
kunsthistorii	kunsthistorie	k1gFnSc4	kunsthistorie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
studenti	student	k1gMnPc1	student
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
tohoto	tento	k3xDgInSc2	tento
oboru	obor	k1gInSc2	obor
nepřijímali	přijímat	k5eNaImAgMnP	přijímat
<g/>
,	,	kIx,	,
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
škol	škola	k1gFnPc2	škola
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
dostat	dostat	k5eAaPmF	dostat
až	až	k9	až
po	po	k7c6	po
opakovaných	opakovaný	k2eAgInPc6d1	opakovaný
pokusech	pokus	k1gInPc6	pokus
a	a	k8xC	a
Franz	Franz	k1gMnSc1	Franz
navíc	navíc	k6eAd1	navíc
neměl	mít	k5eNaImAgMnS	mít
bezchybné	bezchybný	k2eAgNnSc4d1	bezchybné
"	"	kIx"	"
<g/>
kádrové	kádrový	k2eAgNnSc4d1	kádrové
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
volil	volit	k5eAaImAgInS	volit
mezi	mezi	k7c7	mezi
právnickou	právnický	k2eAgFnSc7d1	právnická
fakultou	fakulta	k1gFnSc7	fakulta
(	(	kIx(	(
<g/>
a	a	k8xC	a
volným	volný	k2eAgInSc7d1	volný
časem	čas	k1gInSc7	čas
mezi	mezi	k7c7	mezi
zkouškovými	zkouškový	k2eAgNnPc7d1	zkouškové
obdobími	období	k1gNnPc7	období
<g/>
)	)	kIx)	)
a	a	k8xC	a
studiem	studio	k1gNnSc7	studio
korejštiny	korejština	k1gFnSc2	korejština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1982	[number]	k4	1982
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
University	universita	k1gFnSc2	universita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc1	doktorát
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
studií	studio	k1gNnPc2	studio
práv	právo	k1gNnPc2	právo
studoval	studovat	k5eAaImAgMnS	studovat
soukromě	soukromě	k6eAd1	soukromě
malbu	malba	k1gFnSc4	malba
u	u	k7c2	u
Karla	Karel	k1gMnSc2	Karel
Součka	Souček	k1gMnSc2	Souček
a	a	k8xC	a
Andreje	Andrej	k1gMnSc2	Andrej
Bělocvětova	Bělocvětův	k2eAgMnSc2d1	Bělocvětův
(	(	kIx(	(
<g/>
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
<g/>
,	,	kIx,	,
když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
uklízeč	uklízeč	k1gMnSc1	uklízeč
v	v	k7c6	v
Dětském	dětský	k2eAgInSc6d1	dětský
domě	dům	k1gInSc6	dům
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladbu	skladba	k1gFnSc4	skladba
u	u	k7c2	u
Miroslava	Miroslav	k1gMnSc2	Miroslav
Raichla	Raichla	k1gMnSc2	Raichla
a	a	k8xC	a
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Sommera	Sommer	k1gMnSc2	Sommer
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc4	dějiny
umění	umění	k1gNnSc2	umění
u	u	k7c2	u
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Homolky	Homolka	k1gMnSc2	Homolka
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
hudby	hudba	k1gFnSc2	hudba
u	u	k7c2	u
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Kincla	Kincl	k1gMnSc2	Kincl
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
právnické	právnický	k2eAgFnSc3d1	právnická
profesi	profes	k1gFnSc3	profes
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jej	on	k3xPp3gMnSc4	on
údajně	údajně	k6eAd1	údajně
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
totalitního	totalitní	k2eAgInSc2d1	totalitní
práva	právo	k1gNnPc1	právo
nelákala	lákat	k5eNaImAgNnP	lákat
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaImF	věnovat
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
pozdějších	pozdní	k2eAgNnPc2d2	pozdější
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
vzdělání	vzdělání	k1gNnSc2	vzdělání
a	a	k8xC	a
stranické	stranický	k2eAgFnSc2d1	stranická
příslušnosti	příslušnost	k1gFnSc2	příslušnost
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tehdejších	tehdejší	k2eAgFnPc6d1	tehdejší
dobách	doba	k1gFnPc6	doba
složité	složitý	k2eAgNnSc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
volil	volit	k5eAaImAgInS	volit
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
nezatížila	zatížit	k5eNaPmAgFnS	zatížit
mozek	mozek	k1gInSc4	mozek
nesmysly	nesmysl	k1gInPc7	nesmysl
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
mu	on	k3xPp3gMnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
být	být	k5eAaImF	být
na	na	k7c6	na
zdravém	zdravý	k2eAgInSc6d1	zdravý
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jej	on	k3xPp3gMnSc4	on
zajímalo	zajímat	k5eAaImAgNnS	zajímat
nejvíc	nejvíc	k6eAd1	nejvíc
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kontinuálně	kontinuálně	k6eAd1	kontinuálně
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
malováním	malování	k1gNnSc7	malování
-	-	kIx~	-
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
o	o	k7c4	o
dělnické	dělnický	k2eAgFnPc4d1	Dělnická
profese	profes	k1gFnPc4	profes
<g/>
,	,	kIx,	,
a	a	k8xC	a
podle	podle	k7c2	podle
Franze	Franze	k1gFnSc2	Franze
by	by	kYmCp3nS	by
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
seznamu	seznam	k1gInSc2	seznam
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
docela	docela	k6eAd1	docela
dobrý	dobrý	k2eAgInSc1d1	dobrý
kádrový	kádrový	k2eAgInSc1d1	kádrový
posudek	posudek	k1gInSc1	posudek
pro	pro	k7c4	pro
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
disidentském	disidentský	k2eAgNnSc6d1	disidentské
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
jezdil	jezdit	k5eAaImAgInS	jezdit
například	například	k6eAd1	například
se	s	k7c7	s
sypačem	sypač	k1gInSc7	sypač
<g/>
,	,	kIx,	,
se	s	k7c7	s
sanitkou	sanitka	k1gFnSc7	sanitka
<g/>
,	,	kIx,	,
rouboval	roubovat	k5eAaImAgMnS	roubovat
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
vykládal	vykládat	k5eAaImAgMnS	vykládat
vagony	vagon	k1gInPc4	vagon
<g/>
,	,	kIx,	,
uklízel	uklízet	k5eAaImAgMnS	uklízet
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
domě	dům	k1gInSc6	dům
Dětský	dětský	k2eAgInSc4d1	dětský
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
dělal	dělat	k5eAaImAgMnS	dělat
referenta	referent	k1gMnSc4	referent
v	v	k7c6	v
knižním	knižní	k2eAgInSc6d1	knižní
Avicenu	Avicen	k2eAgFnSc4d1	Avicena
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
kvazikulturních	kvazikulturní	k2eAgFnPc6d1	kvazikulturní
institucích	instituce	k1gFnPc6	instituce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
žádnou	žádný	k3yNgFnSc4	žádný
kulturu	kultura	k1gFnSc4	kultura
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
institucích	instituce	k1gFnPc6	instituce
dělali	dělat	k5eAaImAgMnP	dělat
jen	jen	k6eAd1	jen
legraci	legrace	k1gFnSc4	legrace
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
pracné	pracný	k2eAgNnSc4d1	pracné
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
-	-	kIx~	-
bez	bez	k7c2	bez
razítka	razítko	k1gNnSc2	razítko
v	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
průkazu	průkaz	k1gInSc6	průkaz
-	-	kIx~	-
neustále	neustále	k6eAd1	neustále
jednou	jeden	k4xCgFnSc7	jeden
nohou	noha	k1gFnSc7	noha
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
na	na	k7c6	na
středním	střední	k2eAgNnSc6d1	střední
učilišti	učiliště	k1gNnSc6	učiliště
spojů	spoj	k1gInPc2	spoj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vedl	vést	k5eAaImAgMnS	vést
zájmovou	zájmový	k2eAgFnSc4d1	zájmová
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
později	pozdě	k6eAd2	pozdě
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
snažil	snažit	k5eAaImAgMnS	snažit
pozitivně	pozitivně	k6eAd1	pozitivně
působit	působit	k5eAaImF	působit
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
problémovou	problémový	k2eAgFnSc4d1	problémová
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
neukotvenou	ukotvený	k2eNgFnSc4d1	neukotvená
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
budoucí	budoucí	k2eAgFnSc7d1	budoucí
ženou	žena	k1gFnSc7	žena
Markétou	Markéta	k1gFnSc7	Markéta
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
spolužačkou	spolužačka	k1gFnSc7	spolužačka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
členkou	členka	k1gFnSc7	členka
jím	jíst	k5eAaImIp1nS	jíst
spoluzaloženého	spoluzaložený	k2eAgInSc2d1	spoluzaložený
divadelního	divadelní	k2eAgInSc2d1	divadelní
souboru	soubor	k1gInSc2	soubor
Kytka	kytka	k1gFnSc1	kytka
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dceru	dcera	k1gFnSc4	dcera
Kristýnu	Kristýna	k1gFnSc4	Kristýna
<g/>
.	.	kIx.	.
</s>
<s>
Rozvedli	rozvést	k5eAaPmAgMnP	rozvést
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
oboustranného	oboustranný	k2eAgNnSc2d1	oboustranné
pracovního	pracovní	k2eAgNnSc2d1	pracovní
vytížení	vytížení	k1gNnSc2	vytížení
<g/>
..	..	k?	..
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
je	být	k5eAaImIp3nS	být
Franzovou	Franzový	k2eAgFnSc7d1	Franzový
partnerkou	partnerka	k1gFnSc7	partnerka
fotografka	fotografka	k1gFnSc1	fotografka
Ida	Ida	k1gFnSc1	Ida
Saudková	Saudkový	k2eAgFnSc1d1	Saudková
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
manželka	manželka	k1gFnSc1	manželka
Samuela	Samuel	k1gMnSc2	Samuel
Saudka	Saudek	k1gMnSc2	Saudek
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
X.	X.	kA	X.
Doležala	Doležal	k1gMnSc2	Doležal
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
třípokojový	třípokojový	k2eAgInSc4d1	třípokojový
byt	byt	k1gInSc4	byt
v	v	k7c6	v
Praze-Smíchově	Praze-Smíchův	k2eAgInSc6d1	Praze-Smíchův
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
porozvodového	porozvodový	k2eAgNnSc2d1	porozvodové
majetkoprávního	majetkoprávní	k2eAgNnSc2d1	majetkoprávní
vypořádání	vypořádání	k1gNnSc2	vypořádání
<g/>
;	;	kIx,	;
bývalý	bývalý	k2eAgInSc1d1	bývalý
hostinec	hostinec	k1gInSc1	hostinec
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
ateliér	ateliér	k1gInSc1	ateliér
<g/>
,	,	kIx,	,
připadl	připadnout	k5eAaPmAgInS	připadnout
jeho	jeho	k3xOp3gFnSc3	jeho
bývalé	bývalý	k2eAgFnSc3d1	bývalá
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
zchátralou	zchátralý	k2eAgFnSc7d1	zchátralá
bývalou	bývalý	k2eAgFnSc7d1	bývalá
hospodu	hospodu	k?	hospodu
ve	v	k7c6	v
vesničce	vesnička	k1gFnSc6	vesnička
Dožice	Dožic	k1gMnSc2	Dožic
u	u	k7c2	u
Nepomuku	Nepomuk	k1gInSc2	Nepomuk
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
letní	letní	k2eAgInSc4d1	letní
ateliér	ateliér	k1gInSc4	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
partnerkou	partnerka	k1gFnSc7	partnerka
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
dva	dva	k4xCgMnPc4	dva
psy	pes	k1gMnPc4	pes
<g/>
,	,	kIx,	,
křížence	kříženec	k1gMnPc4	kříženec
Milina	Milin	k1gMnSc4	Milin
a	a	k8xC	a
Tomáše	Tomáš	k1gMnSc4	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
žádné	žádný	k3yNgFnSc2	žádný
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981-84	[number]	k4	1981-84
byl	být	k5eAaImAgInS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
<g/>
,	,	kIx,	,
scénografem	scénograf	k1gMnSc7	scénograf
<g/>
,	,	kIx,	,
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
studentského	studentský	k2eAgNnSc2d1	studentské
autorského	autorský	k2eAgNnSc2d1	autorské
divadla	divadlo	k1gNnSc2	divadlo
Kytka	kytka	k1gFnSc1	kytka
<g/>
,	,	kIx,	,
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
UK	UK	kA	UK
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
divadla	divadlo	k1gNnSc2	divadlo
Kytka	kytka	k1gFnSc1	kytka
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
celkem	celkem	k6eAd1	celkem
šest	šest	k4xCc1	šest
her	hra	k1gFnPc2	hra
-	-	kIx~	-
Opera	opera	k1gFnSc1	opera
ještě	ještě	k6eAd1	ještě
nezemřela	zemřít	k5eNaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
Jídelní	jídelní	k2eAgInSc4d1	jídelní
lístek	lístek	k1gInSc4	lístek
<g/>
,	,	kIx,	,
Tajemný	tajemný	k2eAgMnSc1d1	tajemný
listonoš	listonoš	k1gMnSc1	listonoš
<g/>
,	,	kIx,	,
Jeníček	Jeníček	k1gMnSc1	Jeníček
a	a	k8xC	a
Mařenka	Mařenka	k1gFnSc1	Mařenka
<g/>
,	,	kIx,	,
Harudel	Harudlo	k1gNnPc2	Harudlo
a	a	k8xC	a
Žába	žába	k1gFnSc1	žába
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
ansámblu	ansámbl	k1gInSc2	ansámbl
tohoto	tento	k3xDgNnSc2	tento
divadla	divadlo	k1gNnSc2	divadlo
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Franzova	Franzův	k2eAgFnSc1d1	Franzova
budoucí	budoucí	k2eAgFnSc1d1	budoucí
žena	žena	k1gFnSc1	žena
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
Katedře	katedra	k1gFnSc6	katedra
politologie	politologie	k1gFnSc2	politologie
a	a	k8xC	a
sociologie	sociologie	k1gFnSc2	sociologie
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Jan	Jan	k1gMnSc1	Jan
Kosek	Kosek	k1gMnSc1	Kosek
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
ředitel	ředitel	k1gMnSc1	ředitel
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
také	také	k9	také
ředitel	ředitel	k1gMnSc1	ředitel
agentury	agentura	k1gFnSc2	agentura
Dilia	Dilius	k1gMnSc2	Dilius
Jiří	Jiří	k1gMnSc2	Jiří
Srstka	srstka	k1gFnSc1	srstka
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
bývalí	bývalý	k2eAgMnPc1d1	bývalý
studenti	student	k1gMnPc1	student
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
UK	UK	kA	UK
Petr	Petr	k1gMnSc1	Petr
Borka	borka	k1gFnSc1	borka
<g/>
,	,	kIx,	,
Otokar	Otokar	k1gMnSc1	Otokar
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Eliška	Eliška	k1gFnSc1	Eliška
Lachoutová	Lachoutový	k2eAgFnSc1d1	Lachoutová
či	či	k8xC	či
Jana	Jana	k1gFnSc1	Jana
Švenková	Švenková	k1gFnSc1	Švenková
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
třiceti	třicet	k4xCc6	třicet
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
9	[number]	k4	9
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
festivalu	festival	k1gInSc2	festival
Opera	opera	k1gFnSc1	opera
2009	[number]	k4	2009
provedeny	provést	k5eAaPmNgInP	provést
v	v	k7c6	v
obnovené	obnovený	k2eAgFnSc6d1	obnovená
premiéře	premiéra	k1gFnSc6	premiéra
a	a	k8xC	a
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
obsazení	obsazení	k1gNnSc6	obsazení
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Petra	Petra	k1gFnSc1	Petra
Chmely	chmel	k1gInPc4	chmel
<g/>
)	)	kIx)	)
dvě	dva	k4xCgFnPc1	dva
hry	hra	k1gFnPc1	hra
divadla	divadlo	k1gNnSc2	divadlo
Kytka	kytka	k1gFnSc1	kytka
<g/>
,	,	kIx,	,
Opera	opera	k1gFnSc1	opera
ještě	ještě	k6eAd1	ještě
nezemřela	zemřít	k5eNaPmAgFnS	zemřít
a	a	k8xC	a
Tajemný	tajemný	k2eAgMnSc1d1	tajemný
listonoš	listonoš	k1gMnSc1	listonoš
<g/>
.	.	kIx.	.
</s>
<s>
Franzovým	Franzův	k2eAgInSc7d1	Franzův
prvním	první	k4xOgMnSc6	první
profesionálním	profesionální	k2eAgNnSc7d1	profesionální
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
angažmá	angažmá	k1gNnSc7	angažmá
bylo	být	k5eAaImAgNnS	být
zaměstnaní	zaměstnaný	k2eAgMnPc1d1	zaměstnaný
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
pracujících	pracující	k1gFnPc2	pracující
v	v	k7c6	v
Mostě	most	k1gInSc6	most
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
korepetitor	korepetitor	k1gMnSc1	korepetitor
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
hudby	hudba	k1gFnSc2	hudba
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
absolventů	absolvent	k1gMnPc2	absolvent
DAMU	DAMU	kA	DAMU
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Zbyňkem	Zbyněk	k1gMnSc7	Zbyněk
Srbou	Srba	k1gMnSc7	Srba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
pedagog	pedagog	k1gMnSc1	pedagog
Divadelní	divadelní	k2eAgFnSc2d1	divadelní
fakulty	fakulta	k1gFnSc2	fakulta
AMU	AMU	kA	AMU
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
rok	rok	k1gInSc1	rok
zde	zde	k6eAd1	zde
učil	učit	k5eAaImAgInS	učit
externě	externě	k6eAd1	externě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Kabinet	kabinet	k1gInSc1	kabinet
scénické	scénický	k2eAgFnSc2d1	scénická
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
hudebně-výtvarné	hudebněýtvarný	k2eAgFnSc2d1	hudebně-výtvarný
interakce	interakce	k1gFnSc2	interakce
<g/>
;	;	kIx,	;
přednáší	přednášet	k5eAaImIp3nS	přednášet
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
Filmové	filmový	k2eAgFnSc6d1	filmová
fakultě	fakulta	k1gFnSc6	fakulta
AMU	AMU	kA	AMU
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
kultury	kultura	k1gFnSc2	kultura
Univerzity	univerzita	k1gFnSc2	univerzita
J.	J.	kA	J.
E.	E.	kA	E.
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
stál	stát	k5eAaImAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
AMU	AMU	kA	AMU
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
docentem	docent	k1gMnSc7	docent
AMU	AMU	kA	AMU
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
dramaturgie	dramaturgie	k1gFnSc2	dramaturgie
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
habilitační	habilitační	k2eAgFnSc4d1	habilitační
práci	práce	k1gFnSc4	práce
O	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
scénické	scénický	k2eAgFnSc2d1	scénická
a	a	k8xC	a
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
AMU	AMU	kA	AMU
pro	pro	k7c4	pro
obor	obor	k1gInSc4	obor
dramatická	dramatický	k2eAgNnPc4d1	dramatické
umění	umění	k1gNnPc4	umění
-	-	kIx~	-
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
činoherního	činoherní	k2eAgNnSc2d1	činoherní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
pedagog	pedagog	k1gMnSc1	pedagog
působil	působit	k5eAaImAgMnS	působit
i	i	k9	i
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
FAVU	FAVU	kA	FAVU
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
dílem	díl	k1gInSc7	díl
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
scénické	scénický	k2eAgFnSc2d1	scénická
hudby	hudba	k1gFnSc2	hudba
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
tvorbu	tvorba	k1gFnSc4	tvorba
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
představitelů	představitel	k1gMnPc2	představitel
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
české	český	k2eAgFnSc2d1	Česká
divadelní	divadelní	k2eAgFnSc2d1	divadelní
režie	režie	k1gFnSc2	režie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
scénických	scénický	k2eAgFnPc6d1	scénická
skladbách	skladba	k1gFnPc6	skladba
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
principy	princip	k1gInPc4	princip
vlastní	vlastní	k2eAgFnSc2d1	vlastní
teoretické	teoretický	k2eAgFnSc2d1	teoretická
koncepce	koncepce	k1gFnSc2	koncepce
autonomní	autonomní	k2eAgFnSc2d1	autonomní
role	role	k1gFnSc2	role
hudby	hudba	k1gFnSc2	hudba
jakožto	jakožto	k8xS	jakožto
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
určujících	určující	k2eAgFnPc2d1	určující
složek	složka	k1gFnPc2	složka
dramatu	drama	k1gNnSc2	drama
<g/>
;	;	kIx,	;
základy	základ	k1gInPc1	základ
této	tento	k3xDgFnSc2	tento
koncepce	koncepce	k1gFnSc2	koncepce
popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
docentské	docentský	k2eAgFnSc6d1	docentská
práci	práce	k1gFnSc6	práce
"	"	kIx"	"
<g/>
O	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
scénické	scénický	k2eAgFnSc6d1	scénická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
přednášce	přednáška	k1gFnSc6	přednáška
"	"	kIx"	"
<g/>
Incidental	Incidental	k1gMnSc1	Incidental
Music	Music	k1gMnSc1	Music
as	as	k1gNnPc2	as
a	a	k8xC	a
Dramatic	Dramatice	k1gFnPc2	Dramatice
Character	Charactra	k1gFnPc2	Charactra
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
(	(	kIx(	(
<g/>
otištěno	otisknout	k5eAaPmNgNnS	otisknout
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Theatre	Theatr	k1gInSc5	Theatr
Design	design	k1gInSc1	design
<g/>
&	&	k?	&
<g/>
Technology	technolog	k1gMnPc7	technolog
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
přednášce	přednáška	k1gFnSc6	přednáška
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
way	way	k?	way
of	of	k?	of
incidental	incidentat	k5eAaPmAgMnS	incidentat
music	music	k1gMnSc1	music
from	froma	k1gFnPc2	froma
the	the	k?	the
semantic	semantice	k1gFnPc2	semantice
sign	signum	k1gNnPc2	signum
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
dramatic	dramatice	k1gFnPc2	dramatice
character	character	k1gMnSc1	character
<g/>
:	:	kIx,	:
Incidental	Incidental	k1gMnSc1	Incidental
music	music	k1gMnSc1	music
as	as	k1gNnSc4	as
a	a	k8xC	a
simultaneous	simultaneous	k1gInSc4	simultaneous
dramaturgy	dramaturg	k1gMnPc7	dramaturg
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
přednesené	přednesený	k2eAgNnSc1d1	přednesené
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
kolokviu	kolokvium	k1gNnSc3	kolokvium
OISTAT	OISTAT	kA	OISTAT
"	"	kIx"	"
<g/>
Theatre	Theatr	k1gInSc5	Theatr
<g/>
&	&	k?	&
<g/>
Sound	Sound	k1gInSc1	Sound
Colloquium	Colloquium	k1gNnSc1	Colloquium
<g/>
"	"	kIx"	"
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
přednášce	přednáška	k1gFnSc6	přednáška
"	"	kIx"	"
<g/>
Hercules	Hercules	k1gInSc1	Hercules
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
concept	concepta	k1gFnPc2	concepta
of	of	k?	of
a	a	k8xC	a
stage	stage	k6eAd1	stage
oratorio	oratorio	k6eAd1	oratorio
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
prezentované	prezentovaný	k2eAgNnSc1d1	prezentované
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Prague	Pragu	k1gFnSc2	Pragu
Quadrennial	Quadrennial	k1gMnSc1	Quadrennial
OISTAT	OISTAT	kA	OISTAT
Scenofest	Scenofest	k1gMnSc1	Scenofest
Sound	Sound	k1gMnSc1	Sound
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
profesorské	profesorský	k2eAgFnSc6d1	profesorská
přednášce	přednáška	k1gFnSc6	přednáška
"	"	kIx"	"
<g/>
Hudební	hudební	k2eAgFnSc1d1	hudební
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Texty	text	k1gInPc7	text
všech	všecek	k3xTgFnPc2	všecek
uvedených	uvedený	k2eAgFnPc2d1	uvedená
prací	práce	k1gFnPc2	práce
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
dosud	dosud	k6eAd1	dosud
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
divadelním	divadelní	k2eAgNnSc7d1	divadelní
představením	představení	k1gNnSc7	představení
(	(	kIx(	(
<g/>
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
scénických	scénický	k2eAgNnPc2d1	scénické
<g />
.	.	kIx.	.
</s>
<s>
kompozic	kompozice	k1gFnPc2	kompozice
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
vydání	vydání	k1gNnPc1	vydání
na	na	k7c4	na
CD	CD	kA	CD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scénická	scénický	k2eAgNnPc1d1	scénické
oratoria	oratorium	k1gNnPc1	oratorium
Júdit	Júdita	k1gFnPc2	Júdita
(	(	kIx(	(
<g/>
r.	r.	kA	r.
<g/>
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rút	Rút	k1gFnSc1	Rút
(	(	kIx(	(
<g/>
r.	r.	kA	r.
<g/>
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Západočeské	západočeský	k2eAgNnSc1d1	Západočeské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Hérakles	Hérakles	k1gMnSc1	Hérakles
(	(	kIx(	(
<g/>
r.	r.	kA	r.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Bambušek	Bambušek	k1gInSc1	Bambušek
<g/>
,	,	kIx,	,
Multiprostor	Multiprostor	k1gMnSc1	Multiprostor
Louny	Louny	k1gInPc4	Louny
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
rozhlasovým	rozhlasový	k2eAgFnPc3d1	rozhlasová
hrám	hra	k1gFnPc3	hra
Hamlet	Hamlet	k1gMnSc1	Hamlet
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
:	:	kIx,	:
Markéta	Markéta	k1gFnSc1	Markéta
Jahodová	Jahodová	k1gFnSc1	Jahodová
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bouře	bouře	k1gFnSc1	bouře
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
:	:	kIx,	:
Markéta	Markéta	k1gFnSc1	Markéta
<g />
.	.	kIx.	.
</s>
<s>
Jahodová	Jahodová	k1gFnSc1	Jahodová
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
rozhlasovému	rozhlasový	k2eAgInSc3d1	rozhlasový
mytologickému	mytologický	k2eAgInSc3d1	mytologický
seriálu	seriál	k1gInSc3	seriál
Jana	Jan	k1gMnSc2	Jan
Vedrala	Vedral	k1gMnSc2	Vedral
Achájové	Achájový	k2eAgFnPc1d1	Achájový
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
:	:	kIx,	:
Aleš	Aleš	k1gMnSc1	Aleš
Vrzák	Vrzák	k1gMnSc1	Vrzák
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
150	[number]	k4	150
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
písniček	písnička	k1gFnPc2	písnička
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgFnPc1d1	lidová
suity	suita	k1gFnPc1	suita
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
Praze	Praha	k1gFnSc3	Praha
v	v	k7c6	v
r.	r.	kA	r.
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
a	a	k8xC	a
Masopust	masopust	k1gInSc4	masopust
(	(	kIx(	(
<g/>
premiéra	premiér	k1gMnSc2	premiér
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
r.	r.	kA	r.
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
performancím	performance	k1gFnPc3	performance
Stavba	stavba	k1gFnSc1	stavba
Babylónské	babylónský	k2eAgFnPc1d1	Babylónská
věže	věž	k1gFnPc1	věž
(	(	kIx(	(
<g/>
Ústí	ústí	k1gNnSc1	ústí
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
a	a	k8xC	a
Republika	republika	k1gFnSc1	republika
slaví	slavit	k5eAaImIp3nS	slavit
narozeniny	narozeniny	k1gFnPc4	narozeniny
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
multimediálnímu	multimediální	k2eAgInSc3d1	multimediální
projektu	projekt	k1gInSc3	projekt
Ikarův	Ikarův	k2eAgInSc1d1	Ikarův
vzestup	vzestup	k1gInSc1	vzestup
(	(	kIx(	(
<g/>
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komorní	komorní	k2eAgFnSc4d1	komorní
kantátu	kantáta	k1gFnSc4	kantáta
Křik	křik	k1gInSc1	křik
strašidel	strašidlo	k1gNnPc2	strašidlo
(	(	kIx(	(
<g/>
na	na	k7c4	na
text	text	k1gInSc4	text
básně	báseň	k1gFnSc2	báseň
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Seiferta	Seifert	k1gMnSc2	Seifert
<g/>
)	)	kIx)	)
a	a	k8xC	a
kantátu	kantáta	k1gFnSc4	kantáta
pro	pro	k7c4	pro
smíšený	smíšený	k2eAgInSc4d1	smíšený
sbor	sbor	k1gInSc4	sbor
Tractatus	Tractatus	k1gInSc1	Tractatus
Pacis	Pacis	k1gInSc1	Pacis
(	(	kIx(	(
<g/>
na	na	k7c4	na
texty	text	k1gInPc4	text
návrhu	návrh	k1gInSc2	návrh
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
novozákonní	novozákonní	k2eAgFnPc1d1	novozákonní
Apokalypsy	apokalypsa	k1gFnPc1	apokalypsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
televizní	televizní	k2eAgFnSc3d1	televizní
inscenaci	inscenace	k1gFnSc3	inscenace
Dějinné	dějinný	k2eAgFnSc2d1	dějinná
události	událost	k1gFnSc2	událost
(	(	kIx(	(
<g/>
r.	r.	kA	r.
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklu	cyklus	k1gInSc2	cyklus
televizních	televizní	k2eAgFnPc2d1	televizní
povídek	povídka	k1gFnPc2	povídka
3	[number]	k4	3
<g/>
×	×	k?	×
s	s	k7c7	s
<g/>
...	...	k?	...
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Donutilem	Donutil	k1gMnSc7	Donutil
(	(	kIx(	(
<g/>
O	o	k7c6	o
návratech	návrat	k1gInPc6	návrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
filmům	film	k1gInPc3	film
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Šambhaly	Šambhal	k1gMnPc7	Šambhal
a	a	k8xC	a
Varga	Varga	k1gMnSc1	Varga
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
r.	r.	kA	r.
<g/>
:	:	kIx,	:
Z.	Z.	kA	Z.
<g/>
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
celovečernímu	celovečerní	k2eAgInSc3d1	celovečerní
hranému	hraný	k2eAgInSc3d1	hraný
filmu	film	k1gInSc3	film
Tomáše	Tomáš	k1gMnSc2	Tomáš
Vorla	Vorel	k1gMnSc2	Vorel
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c4	na
CD	CD	kA	CD
v	v	k7c6	v
r.	r.	kA	r.
1996	[number]	k4	1996
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
filmu	film	k1gInSc2	film
Víta	Vít	k1gMnSc2	Vít
Olmera	Olmer	k1gMnSc2	Olmer
Waterloo	Waterloo	k1gNnSc2	Waterloo
po	po	k7c6	po
česku	česko	k1gNnSc6	česko
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
středometrážnímu	středometrážní	k2eAgInSc3d1	středometrážní
hranému	hraný	k2eAgInSc3d1	hraný
filmu	film	k1gInSc3	film
Jakuba	Jakub	k1gMnSc2	Jakub
Hussara	Hussar	k1gMnSc2	Hussar
Skeletoni	Skeleton	k1gMnPc1	Skeleton
(	(	kIx(	(
<g/>
prem	prem	k1gMnSc1	prem
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvohru-muzikál	zpěvohruuzikál	k1gMnSc1	zpěvohru-muzikál
Pokušení	pokušení	k1gNnSc2	pokušení
sv.	sv.	kA	sv.
<g/>
Antonína	Antonín	k1gMnSc4	Antonín
(	(	kIx(	(
<g/>
r.	r.	kA	r.
<g/>
Petr	Petr	k1gMnSc1	Petr
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
předehru	předehra	k1gFnSc4	předehra
Měšťák	měšťák	k1gMnSc1	měšťák
šlechticem	šlechtic	k1gMnSc7	šlechtic
(	(	kIx(	(
<g/>
prem	prem	k1gMnSc1	prem
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgNnSc1d1	Pražské
divertimento	divertimento	k1gNnSc1	divertimento
(	(	kIx(	(
<g/>
prem	prem	k1gInSc1	prem
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapesní	kapesní	k2eAgNnSc1d1	kapesní
oratorium	oratorium	k1gNnSc1	oratorium
Mládenci	mládenec	k1gMnSc3	mládenec
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
ohnivé	ohnivý	k2eAgNnSc1d1	ohnivé
(	(	kIx(	(
<g/>
prem	prem	k6eAd1	prem
<g/>
.	.	kIx.	.
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hudebního	hudební	k2eAgInSc2d1	hudební
festivalu	festival	k1gInSc2	festival
Bohemia	bohemia	k1gFnSc1	bohemia
Cantat	Cantat	k1gFnSc1	Cantat
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kapesní	kapesní	k2eAgNnSc4d1	kapesní
oratorium	oratorium	k1gNnSc4	oratorium
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
volba	volba	k1gFnSc1	volba
(	(	kIx(	(
<g/>
prem	prem	k1gInSc1	prem
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hudebního	hudební	k2eAgInSc2d1	hudební
festivalu	festival	k1gInSc2	festival
Bohemia	bohemia	k1gFnSc1	bohemia
Cantat	Cantat	k1gFnSc1	Cantat
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symfonické	symfonický	k2eAgFnPc1d1	symfonická
básně	báseň	k1gFnPc1	báseň
Radobÿ	Radobÿ	k1gFnSc1	Radobÿ
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
symfonii	symfonie	k1gFnSc4	symfonie
pro	pro	k7c4	pro
baryton	baryton	k1gInSc4	baryton
<g/>
,	,	kIx,	,
dětský	dětský	k2eAgInSc4d1	dětský
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
sólo	sólo	k1gNnSc4	sólo
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
,	,	kIx,	,
dětský	dětský	k2eAgInSc1d1	dětský
a	a	k8xC	a
smíšený	smíšený	k2eAgInSc1d1	smíšený
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
varhany	varhany	k1gInPc1	varhany
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
orchestr	orchestr	k1gInSc1	orchestr
Písně	píseň	k1gFnSc2	píseň
o	o	k7c6	o
Samotách	samota	k1gFnPc6	samota
<g/>
,	,	kIx,	,
oratorium	oratorium	k1gNnSc1	oratorium
Ludus	Ludus	k1gInSc1	Ludus
Danielis	Danielis	k1gFnSc1	Danielis
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
operu	opera	k1gFnSc4	opera
Ludus	Ludus	k1gInSc4	Ludus
Danielis	Danielis	k1gFnSc2	Danielis
<g/>
,	,	kIx,	,
operu	oprat	k5eAaPmIp1nS	oprat
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
(	(	kIx(	(
<g/>
Státní	státní	k2eAgFnSc1d1	státní
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operu-oratorium	operuratorium	k1gNnSc1	operu-oratorium
Údolí	údolí	k1gNnPc2	údolí
suchých	suchý	k2eAgFnPc2d1	suchá
kostí	kost	k1gFnPc2	kost
(	(	kIx(	(
<g/>
prem	prem	k1gInSc1	prem
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
baletu	balet	k1gInSc3	balet
Zlatovláska	zlatovláska	k1gFnSc1	zlatovláska
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
varhanních	varhanní	k2eAgFnPc2d1	varhanní
Variací	variace	k1gFnPc2	variace
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Jana	Jan	k1gMnSc2	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
z	z	k7c2	z
Dražic	Dražice	k1gFnPc2	Dražice
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
provedena	proveden	k2eAgFnSc1d1	provedena
autorem	autor	k1gMnSc7	autor
samým	samý	k3xTgMnSc7	samý
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
odhalení	odhalení	k1gNnSc1	odhalení
pomníku	pomník	k1gInSc2	pomník
-	-	kIx~	-
kašny	kašna	k1gFnPc1	kašna
Janovi	Jan	k1gMnSc3	Jan
z	z	k7c2	z
Dražic	Dražice	k1gFnPc2	Dražice
ak.	ak.	k?	ak.
<g/>
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Stanislava	Stanislav	k1gMnSc2	Stanislav
Hanzíka	Hanzík	k1gMnSc2	Hanzík
v	v	k7c6	v
r.	r.	kA	r.
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varhanních	varhanní	k2eAgFnPc2d1	varhanní
Variací	variace	k1gFnPc2	variace
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
sv.	sv.	kA	sv.
<g/>
Jiří	Jiří	k1gMnPc2	Jiří
(	(	kIx(	(
<g/>
provedeno	proveden	k2eAgNnSc1d1	provedeno
při	při	k7c6	při
odhalení	odhalení	k1gNnSc6	odhalení
krucifixu	krucifix	k1gInSc2	krucifix
ak.	ak.	k?	ak.
<g/>
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
<g/>
St.	st.	kA	st.
Hanzíka	Hanzík	k1gMnSc2	Hanzík
na	na	k7c6	na
Řípu	Říp	k1gInSc6	Říp
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varhanní	varhanní	k2eAgFnSc2d1	varhanní
Pocty	pocta	k1gFnSc2	pocta
hoře	hoře	k1gNnSc1	hoře
Vítkov	Vítkov	k1gInSc1	Vítkov
(	(	kIx(	(
<g/>
provedeno	provést	k5eAaPmNgNnS	provést
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Act	Act	k1gFnSc4	Act
on	on	k3xPp3gInSc1	on
Vítkov	Vítkov	k1gInSc1	Vítkov
<g/>
"	"	kIx"	"
zrealizovaného	zrealizovaný	k2eAgInSc2d1	zrealizovaný
Teatrem	teatrum	k1gNnSc7	teatrum
Novogo	Novogo	k1gNnSc4	Novogo
Fronta	fronta	k1gFnSc1	fronta
dne	den	k1gInSc2	den
11.4	[number]	k4	11.4
<g/>
.2004	.2004	k4	.2004
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
památníku	památník	k1gInSc6	památník
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
varhanních	varhanní	k2eAgFnPc2d1	varhanní
Variací	variace	k1gFnPc2	variace
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
provedena	proveden	k2eAgFnSc1d1	provedena
autorem	autor	k1gMnSc7	autor
samým	samý	k3xTgMnSc7	samý
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
vernisáže	vernisáž	k1gFnSc2	vernisáž
jeho	jeho	k3xOp3gFnSc2	jeho
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Lokti	loket	k1gInSc6	loket
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Variací	variace	k1gFnSc7	variace
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
provedena	proveden	k2eAgFnSc1d1	provedena
autorem	autor	k1gMnSc7	autor
samým	samý	k3xTgMnSc7	samý
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
vernisáže	vernisáž	k1gFnSc2	vernisáž
jeho	jeho	k3xOp3gFnSc2	jeho
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
Nepomuku	Nepomuk	k1gInSc6	Nepomuk
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Variací	variace	k1gFnSc7	variace
k	k	k7c3	k
putování	putování	k1gNnSc3	putování
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
(	(	kIx(	(
<g/>
skladba	skladba	k1gFnSc1	skladba
provedena	proveden	k2eAgFnSc1d1	provedena
autorem	autor	k1gMnSc7	autor
samým	samý	k3xTgMnSc7	samý
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
vernisáže	vernisáž	k1gFnSc2	vernisáž
jeho	jeho	k3xOp3gFnSc2	jeho
výstavy	výstava	k1gFnSc2	výstava
ve	v	k7c6	v
Vrčni	Vrčeň	k1gFnSc6	Vrčeň
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
spoluautorem	spoluautor	k1gMnSc7	spoluautor
skladby	skladba	k1gFnSc2	skladba
Dialogy	dialog	k1gInPc1	dialog
-	-	kIx~	-
variace	variace	k1gFnPc1	variace
pro	pro	k7c4	pro
varhany	varhany	k1gFnPc4	varhany
a	a	k8xC	a
bicí	bicí	k2eAgNnSc4d1	bicí
(	(	kIx(	(
<g/>
druhým	druhý	k4xOgNnSc7	druhý
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
Radek	Radek	k1gMnSc1	Radek
Němejc	Němejc	k1gInSc1	Němejc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provedenou	provedený	k2eAgFnSc7d1	provedená
již	již	k6eAd1	již
mnohokrát	mnohokrát	k6eAd1	mnohokrát
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
obsazení	obsazení	k1gNnSc6	obsazení
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
i	i	k8xC	i
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
autorem	autor	k1gMnSc7	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
projektům	projekt	k1gInPc3	projekt
Tryptych	Tryptycha	k1gFnPc2	Tryptycha
-	-	kIx~	-
Poesie	poesie	k1gFnSc1	poesie
Emila	Emil	k1gMnSc2	Emil
Juliše	Juliš	k1gMnSc2	Juliš
(	(	kIx(	(
<g/>
provedeno	provést	k5eAaPmNgNnS	provést
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
festivalu	festival	k1gInSc2	festival
Next	Next	k1gMnSc1	Next
Wave	Wav	k1gFnSc2	Wav
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
a	a	k8xC	a
Srdce	srdce	k1gNnSc1	srdce
svého	svůj	k3xOyFgInSc2	svůj
nejez	jíst	k5eNaImRp2nS	jíst
<g/>
/	/	kIx~	/
<g/>
Tanec	tanec	k1gInSc1	tanec
smrti	smrt	k1gFnSc2	smrt
-	-	kIx~	-
Dialogy	dialog	k1gInPc1	dialog
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Šiktancem	Šiktanec	k1gInSc7	Šiktanec
(	(	kIx(	(
<g/>
poesii	poesie	k1gFnSc4	poesie
EJ	ej	k0	ej
a	a	k8xC	a
KŠ	kš	k0	kš
recitoval	recitovat	k5eAaImAgMnS	recitovat
Miloš	Miloš	k1gMnSc1	Miloš
Mejzlík	Mejzlík	k1gMnSc1	Mejzlík
<g/>
)	)	kIx)	)
a	a	k8xC	a
autorem	autor	k1gMnSc7	autor
cyklů	cyklus	k1gInPc2	cyklus
Sbory	sbor	k1gInPc4	sbor
z	z	k7c2	z
Krále	Král	k1gMnSc2	Král
Oidipa	Oidipus	k1gMnSc2	Oidipus
<g/>
,	,	kIx,	,
Divadelní	divadelní	k2eAgInPc1d1	divadelní
madrigaly	madrigal	k1gInPc1	madrigal
a	a	k8xC	a
Zářící	zářící	k2eAgFnSc1d1	zářící
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Jiřího	Jiří	k1gMnSc4	Jiří
Suchého	Suchý	k1gMnSc4	Suchý
a	a	k8xC	a
Jitku	Jitka	k1gFnSc4	Jitka
Molavcovou	Molavcův	k2eAgFnSc7d1	Molavcův
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
symfonické	symfonický	k2eAgFnPc4d1	symfonická
variace	variace	k1gFnPc4	variace
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
písně	píseň	k1gFnSc2	píseň
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitrum	k1gNnSc2	Šlitrum
Co	co	k3yRnSc4	co
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgInS	mít
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
obědu	oběd	k1gInSc3	oběd
<g/>
.	.	kIx.	.
</s>
<s>
Složil	složit	k5eAaPmAgMnS	složit
také	také	k9	také
Slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
festivalovou	festivalový	k2eAgFnSc4d1	festivalová
introdukci	introdukce	k1gFnSc4	introdukce
pro	pro	k7c4	pro
Hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Znojmo	Znojmo	k1gNnSc1	Znojmo
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
r.	r.	kA	r.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
ještě	ještě	k6eAd1	ještě
nezemřela	zemřít	k5eNaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jan	Jan	k1gMnSc1	Jan
Kosek	Kosek	k1gMnSc1	Kosek
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
Kytka	kytka	k1gFnSc1	kytka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Jídelní	jídelní	k2eAgInSc1d1	jídelní
lístek	lístek	k1gInSc1	lístek
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jan	Jan	k1gMnSc1	Jan
Kosek	Kosek	k1gMnSc1	Kosek
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
Kytka	kytka	k1gFnSc1	kytka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Tajemný	tajemný	k2eAgMnSc1d1	tajemný
listonoš	listonoš	k1gMnSc1	listonoš
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jan	Jan	k1gMnSc1	Jan
Kosek	Kosek	k1gMnSc1	Kosek
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
Kytka	kytka	k1gFnSc1	kytka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Král	Král	k1gMnSc1	Král
jelenem	jelen	k1gMnSc7	jelen
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
Disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
Filosofská	Filosofská	k?	Filosofská
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
Cirkus	cirkus	k1gInSc1	cirkus
Humberto	Humberta	k1gFnSc5	Humberta
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Most	most	k1gInSc1	most
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1987	[number]	k4	1987
Nová	Nová	k1gFnSc1	Nová
komedyje	komedýt	k5eAaPmIp3nS	komedýt
o	o	k7c4	o
Libuši	Libuše	k1gFnSc4	Libuše
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Josef	Josef	k1gMnSc1	Josef
Henke	Henk	k1gMnSc2	Henk
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
P.	P.	kA	P.
<g/>
Pecháček	Pecháček	k1gMnSc1	Pecháček
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
P.	P.	kA	P.
<g/>
Pecháček	Pecháček	k1gMnSc1	Pecháček
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Hodina	hodina	k1gFnSc1	hodina
<g />
.	.	kIx.	.
</s>
<s>
mezi	mezi	k7c7	mezi
psem	pes	k1gMnSc7	pes
a	a	k8xC	a
vlkem	vlk	k1gMnSc7	vlk
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jakub	Jakub	k1gMnSc1	Jakub
Špalek	špalek	k1gInSc1	špalek
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Řeznické	řeznická	k1gFnSc6	řeznická
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Spolek	spolek	k1gInSc1	spolek
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
hlava	hlava	k1gFnSc1	hlava
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Josef	Josef	k1gMnSc1	Josef
Henke	Henk	k1gMnSc2	Henk
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
Lidé	člověk	k1gMnPc1	člověk
<g />
.	.	kIx.	.
</s>
<s>
z	z	k7c2	z
maringotek	maringotka	k1gFnPc2	maringotka
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Její	její	k3xOp3gFnSc4	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
Disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Král	Král	k1gMnSc1	Král
jelenem	jelen	k1gMnSc7	jelen
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g />
.	.	kIx.	.
</s>
<s>
Cyrano	Cyrano	k1gMnSc1	Cyrano
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jakub	Jakub	k1gMnSc1	Jakub
Špalek	špalek	k1gInSc4	špalek
<g/>
,	,	kIx,	,
Spolek	spolek	k1gInSc4	spolek
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
Idiot	idiot	k1gMnSc1	idiot
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
O.	O.	kA	O.
<g/>
Zajíc	Zajíc	k1gMnSc1	Zajíc
a	a	k8xC	a
L.	L.	kA	L.
<g/>
Vymětal	Vymětal	k1gMnSc1	Vymětal
<g/>
,	,	kIx,	,
Disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Procitnutí	procitnutí	k1gNnSc2	procitnutí
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
Činoherní	činoherní	k2eAgNnSc1d1	činoherní
studio	studio	k1gNnSc1	studio
<g />
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
L	L	kA	L
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Osm	osm	k4xCc1	osm
a	a	k8xC	a
půl	půl	k1xP	půl
(	(	kIx(	(
<g/>
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Poslední	poslední	k2eAgInSc1d1	poslední
šantán	šantán	k1gInSc1	šantán
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
svatba	svatba	k1gFnSc1	svatba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Z.	Z.	kA	Z.
<g/>
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Její	její	k3xOp3gFnSc4	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Cymbelín	Cymbelína	k1gFnPc2	Cymbelína
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
Balaďa	Balaďa	k1gMnSc1	Balaďa
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
r.	r.	kA	r.
<g/>
V.	V.	kA	V.
<g/>
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
MD	MD	kA	MD
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
Faust	Faust	k1gInSc1	Faust
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc1	Julie
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Karel	Karel	k1gMnSc1	Karel
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Baladyna	Baladyno	k1gNnSc2	Baladyno
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
<g/>
Pecháček	Pecháček	k1gMnSc1	Pecháček
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Kočičí	kočičí	k2eAgFnSc1d1	kočičí
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
J.	J.	kA	J.
<g/>
Deák	Deák	k1gMnSc1	Deák
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Zkrocení	zkrocení	k1gNnSc2	zkrocení
zlé	zlý	k2eAgFnSc2d1	zlá
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Michal	Michal	k1gMnSc1	Michal
Lang	Lang	k1gMnSc1	Lang
<g/>
,	,	kIx,	,
Činoherní	činoherní	k2eAgInSc1d1	činoherní
klub	klub	k1gInSc1	klub
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Scény	scéna	k1gFnSc2	scéna
z	z	k7c2	z
manželského	manželský	k2eAgInSc2d1	manželský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Karel	Karel	k1gMnSc1	Karel
<g />
.	.	kIx.	.
</s>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Vinohradské	vinohradský	k2eAgNnSc1d1	Vinohradské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Bloudění	bloudění	k1gNnSc2	bloudění
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Pašije	pašije	k1gFnSc2	pašije
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Radůz	Radůza	k1gFnPc2	Radůza
a	a	k8xC	a
Mahulena	Mahulena	k1gFnSc1	Mahulena
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Michael	Michael	k1gMnSc1	Michael
<g />
.	.	kIx.	.
</s>
<s>
Tarant	Tarant	k1gInSc1	Tarant
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Mistr	mistr	k1gMnSc1	mistr
a	a	k8xC	a
Markétka	Markétka	k1gFnSc1	Markétka
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Sergej	Sergej	k1gMnSc1	Sergej
Fedotov	Fedotov	k1gInSc1	Fedotov
<g/>
,	,	kIx,	,
Klicperovo	Klicperův	k2eAgNnSc1d1	Klicperovo
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Strakonický	strakonický	k2eAgMnSc1d1	strakonický
dudák	dudák	k1gMnSc1	dudák
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hruška	Hruška	k1gMnSc1	Hruška
<g/>
,	,	kIx,	,
Disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Evžen	Evžen	k1gMnSc1	Evžen
Oněgin	Oněgin	k1gMnSc1	Oněgin
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
MD	MD	kA	MD
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Královský	královský	k2eAgInSc1d1	královský
hon	hon	k1gInSc1	hon
na	na	k7c4	na
slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Juraj	Juraj	k1gMnSc1	Juraj
Deák	Deák	k1gMnSc1	Deák
<g/>
,	,	kIx,	,
Vinohradské	vinohradský	k2eAgNnSc1d1	Vinohradské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Lidumor	Lidumora	k1gFnPc2	Lidumora
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Dušan	Dušan	k1gMnSc1	Dušan
Pařízek	Pařízek	k1gMnSc1	Pařízek
<g/>
,	,	kIx,	,
Vinohradské	vinohradský	k2eAgNnSc1d1	Vinohradské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dostává	dostávat	k5eAaImIp3nS	dostávat
políčky	políček	k1gInPc4	políček
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Michael	Michael	k1gMnSc1	Michael
Tarant	Tarant	k1gMnSc1	Tarant
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Michal	Michal	k1gMnSc1	Michal
Lang	Lang	k1gMnSc1	Lang
<g/>
,	,	kIx,	,
Letní	letní	k2eAgFnPc1d1	letní
Shakespearovské	shakespearovský	k2eAgFnPc1d1	shakespearovská
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
,	,	kIx,	,
Pražský	pražský	k2eAgInSc1d1	pražský
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Maryša	Maryša	k1gFnSc1	Maryša
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1999	[number]	k4	1999
Hráči	hráč	k1gMnPc7	hráč
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Sergej	Sergej	k1gMnSc1	Sergej
Fedotov	Fedotov	k1gInSc1	Fedotov
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
bez	bez	k7c2	bez
zábradlí	zábradlí	k1gNnSc2	zábradlí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
Gazdina	gazdina	k1gFnSc1	gazdina
roba	roba	k1gFnSc1	roba
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Slovácké	slovácký	k2eAgNnSc1d1	Slovácké
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
,	,	kIx,	,
Klicperovo	Klicperův	k2eAgNnSc1d1	Klicperovo
divadlo	divadlo	k1gNnSc1	divadlo
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Othello	Othello	k1gMnSc1	Othello
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
Balaďa	Balaďa	k1gMnSc1	Balaďa
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Nápadníci	nápadník	k1gMnPc1	nápadník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Michael	Michael	k1gMnSc1	Michael
Tarant	Tarant	k1gMnSc1	Tarant
<g/>
,	,	kIx,	,
Východočeské	východočeský	k2eAgNnSc1d1	Východočeské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Úklady	úklad	k1gInPc1	úklad
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Ondina	Ondino	k1gNnSc2	Ondino
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Petr	Petr	k1gMnSc1	Petr
Svojtka	Svojtka	k1gMnSc1	Svojtka
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
F.	F.	kA	F.
<g/>
X.	X.	kA	X.
<g/>
Šaldy	Šalda	k1gMnSc2	Šalda
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
Tramvaj	tramvaj	k1gFnSc1	tramvaj
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Touha	touha	k1gFnSc1	touha
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
moravsko-slezské	moravskolezský	k2eAgNnSc1d1	moravsko-slezské
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2001	[number]	k4	2001
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Markéta	Markéta	k1gFnSc1	Markéta
Lazarová	Lazarová	k1gFnSc1	Lazarová
<g/>
,	,	kIx,	,
r.	r.	kA	r.
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Běsi	běs	k1gMnPc1	běs
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g />
.	.	kIx.	.
</s>
<s>
Vichřice	vichřice	k1gFnSc1	vichřice
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Zapadočeské	Zapadočeský	k2eAgNnSc1d1	Zapadočeský
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Strýček	strýček	k1gMnSc1	strýček
Solený	solený	k2eAgMnSc1d1	solený
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
,	,	kIx,	,
Klicperovo	Klicperův	k2eAgNnSc1d1	Klicperovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
líbí	líbit	k5eAaImIp3nS	líbit
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Karel	Karel	k1gMnSc1	Karel
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Středočeské	středočeský	k2eAgNnSc1d1	Středočeské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Kladno	Kladno	k1gNnSc1	Kladno
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Trojanky	Trojanka	k1gFnSc2	Trojanka
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Západočeské	západočeský	k2eAgNnSc1d1	Západočeské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
Don	Don	k1gMnSc1	Don
Carlos	Carlos	k1gMnSc1	Carlos
<g/>
,	,	kIx,	,
infant	infant	k1gMnSc1	infant
španělský	španělský	k2eAgMnSc1d1	španělský
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
a	a	k8xC	a
Faust	Faust	k1gMnSc1	Faust
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
<g />
.	.	kIx.	.
</s>
<s>
divadlo	divadlo	k1gNnSc1	divadlo
moravskoslezské	moravskoslezský	k2eAgNnSc1d1	moravskoslezské
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
Povídky	povídka	k1gFnSc2	povídka
z	z	k7c2	z
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Arnošt	Arnošt	k1gMnSc1	Arnošt
Goldflam	Goldflam	k1gInSc1	Goldflam
<g/>
,	,	kIx,	,
Klicperovo	Klicperův	k2eAgNnSc1d1	Klicperovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Equus	Equus	k1gInSc1	Equus
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Karel	Karel	k1gMnSc1	Karel
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Horácké	horácký	k2eAgNnSc1d1	Horácké
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Macbeth	Macbetha	k1gFnPc2	Macbetha
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Vinohradské	vinohradský	k2eAgNnSc1d1	Vinohradské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Bouře	bouř	k1gFnSc2	bouř
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Peter	Peter	k1gMnSc1	Peter
Gábor	Gábor	k1gMnSc1	Gábor
<g/>
,	,	kIx,	,
Západočeské	západočeský	k2eAgNnSc1d1	Západočeské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Západočeské	západočeský	k2eAgNnSc1d1	Západočeské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Znamení	znamení	k1gNnSc2	znamení
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Porta	porto	k1gNnSc2	porto
Apostolorum	Apostolorum	k1gInSc1	Apostolorum
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Bambušek	Bambušek	k1gInSc1	Bambušek
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Fabrica	Fabrica	k1gFnSc1	Fabrica
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Terorismus	terorismus	k1gInSc1	terorismus
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Peter	Peter	k1gMnSc1	Peter
Chmela	Chmel	k1gMnSc2	Chmel
<g/>
,	,	kIx,	,
Disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Zločin	zločin	k1gInSc4	zločin
a	a	k8xC	a
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
moravskoslezské	moravskoslezský	k2eAgNnSc1d1	moravskoslezské
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Jiří	Jiří	k1gMnSc1	Jiří
Šesták	Šesták	k1gMnSc1	Šesták
<g/>
,	,	kIx,	,
Jihočeské	jihočeský	k2eAgNnSc1d1	Jihočeské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
otáčivé	otáčivý	k2eAgNnSc1d1	otáčivé
hlediště	hlediště	k1gNnSc1	hlediště
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Manon	Manon	k1gInSc1	Manon
Lescaut	Lescaut	k2eAgInSc1d1	Lescaut
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Moliere	Molier	k1gInSc5	Molier
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Sergej	Sergej	k1gMnSc1	Sergej
Fedotov	Fedotov	k1gInSc1	Fedotov
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Amfitryon	Amfitryona	k1gFnPc2	Amfitryona
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Marie	Marie	k1gFnSc1	Marie
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g />
.	.	kIx.	.
</s>
<s>
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Šumperk	Šumperk	k1gInSc1	Šumperk
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Médeia	Médeium	k1gNnSc2	Médeium
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
F.	F.	kA	F.
<g/>
X.	X.	kA	X.
<g/>
Šaldy	Šalda	k1gMnSc2	Šalda
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Švanda	Švanda	k1gMnSc1	Švanda
Dudák	dudák	k1gMnSc1	dudák
<g/>
,	,	kIx,	,
r.	r.	kA	r.
<g/>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
Bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
z	z	k7c2	z
Chaillot	Chaillota	k1gFnPc2	Chaillota
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Karel	Karel	k1gMnSc1	Karel
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Vinohradské	vinohradský	k2eAgNnSc1d1	Vinohradské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Manon	Manon	k1gInSc1	Manon
Lescaut	Lescaut	k2eAgInSc1d1	Lescaut
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Michael	Michael	k1gMnSc1	Michael
Tarant	Tarant	k1gMnSc1	Tarant
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
F.	F.	kA	F.
<g/>
X.	X.	kA	X.
<g/>
Šaldy	Šalda	k1gMnSc2	Šalda
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Smrt	smrt	k1gFnSc1	smrt
Pavla	Pavla	k1gFnSc1	Pavla
I.	I.	kA	I.
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
r.	r.	kA	r.
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarova	Lazarův	k2eAgFnSc1d1	Lazarova
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Horácké	horácký	k2eAgNnSc1d1	Horácké
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Paní	paní	k1gFnSc1	paní
Urbanová	Urbanová	k1gFnSc1	Urbanová
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Martin	Martin	k1gMnSc1	Martin
Františák	Františák	k1gMnSc1	Františák
<g/>
,	,	kIx,	,
Klicperovo	Klicperův	k2eAgNnSc1d1	Klicperovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Radůz	Radůza	k1gFnPc2	Radůza
a	a	k8xC	a
Mahulena	Mahulena	k1gFnSc1	Mahulena
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Michael	Michael	k1gMnSc1	Michael
Tarant	Tarant	k1gMnSc1	Tarant
<g/>
,	,	kIx,	,
Východočeské	východočeský	k2eAgNnSc4d1	Východočeské
divadlo	divadlo	k1gNnSc4	divadlo
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Věc	věc	k1gFnSc1	věc
Makropulos	Makropulosa	k1gFnPc2	Makropulosa
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Klicperovo	Klicperův	k2eAgNnSc1d1	Klicperovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Lékař	lékař	k1gMnSc1	lékař
své	svůj	k3xOyFgFnSc2	svůj
cti	čest	k1gFnSc2	čest
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Mariša	Marišum	k1gNnSc2	Marišum
<g/>
,	,	kIx,	,
r.	r.	kA	r.
J.	J.	kA	J.
A.	A.	kA	A.
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Zdař	zdařit	k5eAaPmRp2nS	zdařit
Bůh	bůh	k1gMnSc1	bůh
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Ewan	Ewan	k1gNnSc1	Ewan
McLaren	McLarno	k1gNnPc2	McLarno
<g/>
,	,	kIx,	,
Důl	důl	k1gInSc1	důl
Michal	Michala	k1gFnPc2	Michala
<g/>
,	,	kIx,	,
Ostrava-Michálkovice	Ostrava-Michálkovice	k1gFnSc2	Ostrava-Michálkovice
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Věra	Věra	k1gFnSc1	Věra
Herajtová	Herajtová	k1gFnSc1	Herajtová
<g/>
,	,	kIx,	,
Klicperovo	Klicperův	k2eAgNnSc1d1	Klicperovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Radúz	Radúz	k1gMnSc1	Radúz
a	a	k8xC	a
Mahuliena	Mahuliena	k1gFnSc1	Mahuliena
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Peter	Peter	k1gMnSc1	Peter
Gábor	Gábor	k1gMnSc1	Gábor
<g/>
,	,	kIx,	,
Komorné	komorný	k2eAgNnSc1d1	komorné
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Martin	Martin	k1gMnSc1	Martin
Františák	Františák	k1gMnSc1	Františák
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Poslední	poslední	k2eAgFnPc1d1	poslední
chvíle	chvíle	k1gFnPc1	chvíle
lidstva	lidstvo	k1gNnSc2	lidstvo
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Thomas	Thomas	k1gMnSc1	Thomas
Zelinski	Zelinsk	k1gFnSc2	Zelinsk
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Konec	konec	k1gInSc1	konec
masupustu	masupust	k1gInSc2	masupust
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
r.	r.	kA	r.
J.A.	J.A.	k1gMnSc1	J.A.
<g/>
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Gábor	Gábor	k1gMnSc1	Gábor
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Stavitel	stavitel	k1gMnSc1	stavitel
Solness	Solness	k1gInSc1	Solness
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Tarant	Tarant	k1gMnSc1	Tarant
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgNnSc1d1	Moravské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Vyučování	vyučování	k1gNnSc2	vyučování
<g />
.	.	kIx.	.
</s>
<s>
Dony	dona	k1gFnPc1	dona
Margaridy	Margarid	k1gInPc4	Margarid
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Pidivadlo	Pidivadlo	k1gNnSc1	Pidivadlo
VOŠH	VOŠH	kA	VOŠH
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Buzní	Buzní	k2eAgInSc1d1	Buzní
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Bambušek	Bambušek	k1gInSc1	Bambušek
<g/>
,	,	kIx,	,
MeetFactory	MeetFactor	k1gInPc1	MeetFactor
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Slečna	slečna	k1gFnSc1	slečna
Jairová	Jairová	k1gFnSc1	Jairová
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
Smrtihlav	smrtihlav	k1gMnSc1	smrtihlav
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
r.	r.	kA	r.
Peter	Peter	k1gMnSc1	Peter
Gábor	Gábor	k1gMnSc1	Gábor
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Moravskoslezské	moravskoslezský	k2eAgNnSc1d1	moravskoslezské
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
Sen	sen	k1gInSc1	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgFnSc2d1	Svatojánská
<g/>
,	,	kIx,	,
r.	r.	kA	r.
Peter	Peter	k1gMnSc1	Peter
Gábor	Gábor	k1gMnSc1	Gábor
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Moravskoslezské	moravskoslezský	k2eAgNnSc1d1	moravskoslezské
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
Podle	podle	k7c2	podle
Svatavy	Svatava	k1gFnSc2	Svatava
Barančicové	Barančicová	k1gFnSc2	Barančicová
(	(	kIx(	(
<g/>
Opera	opera	k1gFnSc1	opera
Plus	plus	k1gInSc1	plus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Franzova	Franzův	k2eAgFnSc1d1	Franzova
partitura	partitura	k1gFnSc1	partitura
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
přehledná	přehledný	k2eAgFnSc1d1	přehledná
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejpřesvědčivěji	přesvědčivě	k6eAd3	přesvědčivě
působí	působit	k5eAaImIp3nS	působit
popově	popův	k2eAgFnSc3d1	Popova
líbivé	líbivý	k2eAgFnPc4d1	líbivá
hudební	hudební	k2eAgFnPc4d1	hudební
vsuvky	vsuvka	k1gFnPc4	vsuvka
a	a	k8xC	a
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Franz	Franz	k1gMnSc1	Franz
mohl	moct	k5eAaImAgMnS	moct
dobře	dobře	k6eAd1	dobře
živit	živit	k5eAaImF	živit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
skládal	skládat	k5eAaImAgMnS	skládat
šlágry	šlágr	k1gInPc4	šlágr
pro	pro	k7c4	pro
Kotvalda	Kotvaldo	k1gNnPc4	Kotvaldo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Franka	Frank	k1gMnSc2	Frank
Kuznika	Kuznik	k1gMnSc2	Kuznik
(	(	kIx(	(
<g/>
iDNES	iDNES	k?	iDNES
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
Franzovu	Franzův	k2eAgFnSc4d1	Franzova
hudbu	hudba	k1gFnSc4	hudba
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
opeře	opera	k1gFnSc6	opera
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
postmoderní	postmoderní	k2eAgMnPc4d1	postmoderní
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
údajně	údajně	k6eAd1	údajně
guláš	guláš	k1gInSc4	guláš
klasických	klasický	k2eAgInPc2d1	klasický
<g/>
,	,	kIx,	,
tradičních	tradiční	k2eAgInPc2d1	tradiční
i	i	k8xC	i
popových	popový	k2eAgInPc2d1	popový
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
divoce	divoce	k6eAd1	divoce
přeřazuje	přeřazovat	k5eAaImIp3nS	přeřazovat
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
rychlosti	rychlost	k1gFnSc6	rychlost
jako	jako	k9	jako
závodník	závodník	k1gMnSc1	závodník
vozů	vůz	k1gInPc2	vůz
formule	formule	k1gFnSc2	formule
a	a	k8xC	a
romantické	romantický	k2eAgFnPc4d1	romantická
melodie	melodie	k1gFnPc4	melodie
co	co	k8xS	co
chvíli	chvíle	k1gFnSc4	chvíle
prokládá	prokládat	k5eAaImIp3nS	prokládat
bezmyšlenkovitým	bezmyšlenkovitý	k2eAgInSc7d1	bezmyšlenkovitý
popem	pop	k1gInSc7	pop
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
heavymetalem	heavymetal	k1gMnSc7	heavymetal
či	či	k8xC	či
nespecifikovatelným	specifikovatelný	k2eNgInSc7d1	nespecifikovatelný
<g/>
,	,	kIx,	,
zuřícím	zuřící	k2eAgInSc7d1	zuřící
hudebním	hudební	k2eAgInSc7d1	hudební
vichrem	vichr	k1gInSc7	vichr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
sice	sice	k8xC	sice
originální	originální	k2eAgFnPc1d1	originální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bohužel	bohužel	k9	bohužel
už	už	k6eAd1	už
ne	ne	k9	ne
moc	moc	k6eAd1	moc
divácky	divácky	k6eAd1	divácky
uspokojivé	uspokojivý	k2eAgFnPc1d1	uspokojivá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
divákovi	divák	k1gMnSc3	divák
se	se	k3xPyFc4	se
nedopřeje	dopřát	k5eNaPmIp3nS	dopřát
ani	ani	k8xC	ani
jediného	jediný	k2eAgNnSc2d1	jediné
vydechnutí	vydechnutí	k1gNnSc2	vydechnutí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
tu	ten	k3xDgFnSc4	ten
nepřetržitou	přetržitý	k2eNgFnSc4d1	nepřetržitá
a	a	k8xC	a
neforemnou	foremný	k2eNgFnSc4d1	neforemná
hudební	hudební	k2eAgFnSc4d1	hudební
látku	látka	k1gFnSc4	látka
mohl	moct	k5eAaImAgInS	moct
nějak	nějak	k6eAd1	nějak
přebrat	přebrat	k5eAaPmF	přebrat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Radmily	Radmila	k1gFnSc2	Radmila
Hrdinové	Hrdinová	k1gFnSc2	Hrdinová
(	(	kIx(	(
<g/>
Právo	právo	k1gNnSc1	právo
<g/>
)	)	kIx)	)
Franzova	Franzův	k2eAgFnSc1d1	Franzova
opera	opera	k1gFnSc1	opera
za	za	k7c4	za
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
uplynuly	uplynout	k5eAaPmAgInP	uplynout
mezi	mezi	k7c7	mezi
jejím	její	k3xOp3gNnSc7	její
dokončením	dokončení	k1gNnSc7	dokončení
a	a	k8xC	a
scénickým	scénický	k2eAgNnSc7d1	scénické
provedením	provedení	k1gNnSc7	provedení
nezestárla	zestárnout	k5eNaPmAgFnS	zestárnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
nikterak	nikterak	k6eAd1	nikterak
novátorská	novátorský	k2eAgFnSc1d1	novátorská
ani	ani	k8xC	ani
módní	módní	k2eAgFnSc1d1	módní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
formě	forma	k1gFnSc6	forma
i	i	k8xC	i
jazyku	jazyk	k1gInSc6	jazyk
záměrně	záměrně	k6eAd1	záměrně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
tradiční	tradiční	k2eAgFnSc4d1	tradiční
operu	opera	k1gFnSc4	opera
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Franzova	Franzův	k2eAgFnSc1d1	Franzova
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Hrdinové	Hrdinová	k1gFnSc2	Hrdinová
<g/>
,	,	kIx,	,
eklektická	eklektický	k2eAgFnSc1d1	eklektická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
motivicky	motivicky	k6eAd1	motivicky
důmyslně	důmyslně	k6eAd1	důmyslně
propracovaná	propracovaný	k2eAgFnSc1d1	propracovaná
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
umí	umět	k5eAaImIp3nS	umět
napsat	napsat	k5eAaPmF	napsat
velké	velký	k2eAgInPc4d1	velký
sbory	sbor	k1gInPc4	sbor
i	i	k9	i
záměrně	záměrně	k6eAd1	záměrně
primitivní	primitivní	k2eAgFnSc4d1	primitivní
písničku	písnička	k1gFnSc4	písnička
<g/>
,	,	kIx,	,
parodii	parodie	k1gFnSc4	parodie
muzikálu	muzikál	k1gInSc2	muzikál
i	i	k9	i
symfonicky	symfonicky	k6eAd1	symfonicky
hutnou	hutný	k2eAgFnSc4d1	hutná
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
celku	celek	k1gInSc2	celek
při	při	k7c6	při
vší	všecek	k3xTgFnSc6	všecek
formální	formální	k2eAgFnSc6d1	formální
diverzitě	diverzita	k1gFnSc6	diverzita
nelze	lze	k6eNd1	lze
upřít	upřít	k5eAaPmF	upřít
emotivní	emotivní	k2eAgFnSc4d1	emotivní
působivost	působivost	k1gFnSc4	působivost
a	a	k8xC	a
posluchačskou	posluchačský	k2eAgFnSc4d1	posluchačská
přitažlivost	přitažlivost	k1gFnSc4	přitažlivost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Tomáše	Tomáš	k1gMnSc2	Tomáš
Foltýnka	Foltýnka	k1gFnSc1	Foltýnka
(	(	kIx(	(
<g/>
Metro	metro	k1gNnSc1	metro
<g/>
)	)	kIx)	)
navazuje	navazovat	k5eAaImIp3nS	navazovat
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
stránce	stránka	k1gFnSc6	stránka
na	na	k7c4	na
první	první	k4xOgFnSc4	první
polovinu	polovina	k1gFnSc4	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
přidává	přidávat	k5eAaImIp3nS	přidávat
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
zazní	zaznít	k5eAaPmIp3nP	zaznít
prvky	prvek	k1gInPc1	prvek
šansonu	šanson	k1gInSc2	šanson
či	či	k8xC	či
popové	popový	k2eAgFnSc2d1	popová
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
evokující	evokující	k2eAgFnSc4d1	evokující
primitivitu	primitivita	k1gFnSc4	primitivita
většiny	většina	k1gFnSc2	většina
současných	současný	k2eAgInPc2d1	současný
českých	český	k2eAgInPc2d1	český
"	"	kIx"	"
<g/>
muzikálů	muzikál	k1gInPc2	muzikál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
struktura	struktura	k1gFnSc1	struktura
údajně	údajně	k6eAd1	údajně
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
,	,	kIx,	,
posluchačsky	posluchačsky	k6eAd1	posluchačsky
stravitelná	stravitelný	k2eAgFnSc1d1	stravitelná
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
tonality	tonalita	k1gFnSc2	tonalita
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
leitmotivy	leitmotiv	k1gInPc4	leitmotiv
nebo	nebo	k8xC	nebo
místy	místy	k6eAd1	místy
nechává	nechávat	k5eAaImIp3nS	nechávat
zaznít	zaznít	k5eAaPmF	zaznít
aleatoriku	aleatorika	k1gFnSc4	aleatorika
<g/>
.	.	kIx.	.
</s>
<s>
Franzova	Franzův	k2eAgFnSc1d1	Franzova
partitura	partitura	k1gFnSc1	partitura
klade	klást	k5eAaImIp3nS	klást
podle	podle	k7c2	podle
Foltýnka	Foltýnka	k1gFnSc1	Foltýnka
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
orchestrální	orchestrální	k2eAgFnSc4d1	orchestrální
sazbu	sazba	k1gFnSc4	sazba
a	a	k8xC	a
sborové	sborový	k2eAgInPc4d1	sborový
výstupy	výstup	k1gInPc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Věry	Věra	k1gFnSc2	Věra
Drápelové	Drápelová	k1gFnSc2	Drápelová
(	(	kIx(	(
<g/>
iDNES	iDNES	k?	iDNES
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
představení	představení	k1gNnSc2	představení
nejvíce	nejvíce	k6eAd1	nejvíce
sil	síla	k1gFnPc2	síla
samotná	samotný	k2eAgFnSc1d1	samotná
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
coby	coby	k?	coby
zkušený	zkušený	k2eAgMnSc1d1	zkušený
skladatel	skladatel	k1gMnSc1	skladatel
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
zkomponoval	zkomponovat	k5eAaPmAgInS	zkomponovat
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
čísla	číslo	k1gNnPc4	číslo
s	s	k7c7	s
dovedností	dovednost	k1gFnSc7	dovednost
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
umí	umět	k5eAaImIp3nS	umět
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
posluchačem	posluchač	k1gMnSc7	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
od	od	k7c2	od
prostých	prostý	k2eAgInPc2d1	prostý
popěvků	popěvek	k1gInPc2	popěvek
přes	přes	k7c4	přes
árie	árie	k1gFnPc4	árie
<g/>
,	,	kIx,	,
milostný	milostný	k2eAgInSc1d1	milostný
duet	duet	k1gInSc1	duet
až	až	k9	až
po	po	k7c4	po
muzikálový	muzikálový	k2eAgInSc4d1	muzikálový
hit	hit	k1gInSc4	hit
a	a	k8xC	a
sbory	sbor	k1gInPc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Drápelová	Drápelová	k1gFnSc1	Drápelová
však	však	k9	však
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
efektní	efektní	k2eAgFnSc1d1	efektní
scénická	scénický	k2eAgFnSc1d1	scénická
muzika	muzika	k1gFnSc1	muzika
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
špatné	špatný	k2eAgFnSc3d1	špatná
hře	hra	k1gFnSc3	hra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Petra	Petr	k1gMnSc2	Petr
Vebera	Veber	k1gMnSc2	Veber
(	(	kIx(	(
<g/>
iHNED	ihned	k6eAd1	ihned
<g/>
)	)	kIx)	)
napsal	napsat	k5eAaPmAgMnS	napsat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
pro	pro	k7c4	pro
operu	opera	k1gFnSc4	opera
hudbu	hudba	k1gFnSc4	hudba
zvukově	zvukově	k6eAd1	zvukově
moderní	moderní	k2eAgFnSc4d1	moderní
<g/>
,	,	kIx,	,
různorodou	různorodý	k2eAgFnSc4d1	různorodá
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
temnou	temný	k2eAgFnSc4d1	temná
a	a	k8xC	a
nepřívětivou	přívětivý	k2eNgFnSc4d1	nepřívětivá
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
tradiční	tradiční	k2eAgNnSc1d1	tradiční
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
experimentující	experimentující	k2eAgFnPc1d1	experimentující
<g/>
;	;	kIx,	;
současně	současně	k6eAd1	současně
však	však	k9	však
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hudebně	hudebně	k6eAd1	hudebně
nepříliš	příliš	k6eNd1	příliš
zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
<g/>
.	.	kIx.	.
</s>
<s>
Postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Vebera	Vebero	k1gNnSc2	Vebero
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
uspokojivě	uspokojivě	k6eAd1	uspokojivě
propracovány	propracován	k2eAgFnPc1d1	propracována
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	on	k3xPp3gInPc4	on
provází	provázet	k5eAaImIp3nS	provázet
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
prý	prý	k9	prý
vyprofilována	vyprofilovat	k5eAaPmNgFnS	vyprofilovat
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
.	.	kIx.	.
</s>
<s>
Franzova	Franzův	k2eAgFnSc1d1	Franzova
opera	opera	k1gFnSc1	opera
údajně	údajně	k6eAd1	údajně
neoplývá	oplývat	k5eNaImIp3nS	oplývat
tak	tak	k6eAd1	tak
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zanechala	zanechat	k5eAaPmAgFnS	zanechat
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
na	na	k7c4	na
jedinečný	jedinečný	k2eAgInSc4d1	jedinečný
nebo	nebo	k8xC	nebo
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
motiv	motiv	k1gInSc4	motiv
<g/>
,	,	kIx,	,
sound	sound	k1gInSc4	sound
<g/>
,	,	kIx,	,
styl	styl	k1gInSc4	styl
či	či	k8xC	či
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Stanislava	Stanislav	k1gMnSc2	Stanislav
Vaňka	Vaněk	k1gMnSc2	Vaněk
(	(	kIx(	(
<g/>
Deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Opera	opera	k1gFnSc1	opera
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
kulturní	kulturní	k2eAgFnSc2d1	kulturní
událostí	událost	k1gFnSc7	událost
nadprůměrné	nadprůměrný	k2eAgFnSc2d1	nadprůměrná
kvality	kvalita	k1gFnSc2	kvalita
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
autoři	autor	k1gMnPc1	autor
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nP	zasloužit
respekt	respekt	k1gInSc4	respekt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Petra	Petr	k1gMnSc2	Petr
Fischera	Fischer	k1gMnSc2	Fischer
(	(	kIx(	(
<g/>
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
skladatel	skladatel	k1gMnSc1	skladatel
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
smůlu	smůla	k1gFnSc4	smůla
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
premiéra	premiéra	k1gFnSc1	premiéra
jeho	jeho	k3xOp3gFnSc2	jeho
opery	opera	k1gFnSc2	opera
sice	sice	k8xC	sice
stala	stát	k5eAaPmAgFnS	stát
významnou	významný	k2eAgFnSc7d1	významná
mediální	mediální	k2eAgFnSc7d1	mediální
událostí	událost	k1gFnSc7	událost
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
kulturní	kulturní	k2eAgInPc4d1	kulturní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
napsal	napsat	k5eAaBmAgMnS	napsat
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Nebýt	být	k5eNaImF	být
této	tento	k3xDgFnSc3	tento
souvislosti	souvislost	k1gFnSc3	souvislost
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
nedostaly	dostat	k5eNaPmAgInP	dostat
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
recenze	recenze	k1gFnPc1	recenze
jeho	jeho	k3xOp3gFnSc2	jeho
opery	opera	k1gFnSc2	opera
nikdy	nikdy	k6eAd1	nikdy
tak	tak	k6eAd1	tak
vysoko	vysoko	k6eAd1	vysoko
a	a	k8xC	a
některá	některý	k3yIgNnPc1	některý
média	médium	k1gNnPc1	médium
by	by	k9	by
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
"	"	kIx"	"
<g/>
postmoderním	postmoderní	k2eAgInSc6d1	postmoderní
guláši	guláš	k1gInSc6	guláš
<g/>
"	"	kIx"	"
ani	ani	k8xC	ani
nepsala	psát	k5eNaImAgFnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
hudebním	hudební	k2eAgNnSc6d1	hudební
pojetí	pojetí	k1gNnSc6	pojetí
opery	opera	k1gFnSc2	opera
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
vést	vést	k5eAaImF	vést
estetické	estetický	k2eAgInPc1d1	estetický
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Někomu	někdo	k3yInSc3	někdo
prý	prý	k9	prý
může	moct	k5eAaImIp3nS	moct
Franzova	Franzův	k2eAgFnSc1d1	Franzova
hudba	hudba	k1gFnSc1	hudba
připadat	připadat	k5eAaPmF	připadat
povrchní	povrchní	k2eAgFnSc4d1	povrchní
<g/>
,	,	kIx,	,
někomu	někdo	k3yInSc3	někdo
málo	málo	k6eAd1	málo
vynalézavá	vynalézavý	k2eAgFnSc1d1	vynalézavá
a	a	k8xC	a
stará	starý	k2eAgFnSc1d1	stará
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
klasická	klasický	k2eAgFnSc1d1	klasická
<g/>
;	;	kIx,	;
jinému	jiný	k1gMnSc3	jiný
zase	zase	k9	zase
prvoplánová	prvoplánový	k2eAgNnPc4d1	prvoplánové
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
očekávatelná	očekávatelný	k2eAgFnSc1d1	očekávatelná
<g/>
,	,	kIx,	,
stereotypní	stereotypní	k2eAgFnSc1d1	stereotypní
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
či	či	k8xC	či
chce	chtít	k5eAaImIp3nS	chtít
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
Gesamtkunstwerk	Gesamtkunstwerk	k1gInSc4	Gesamtkunstwerk
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
všemi	všecek	k3xTgFnPc7	všecek
složkami	složka	k1gFnPc7	složka
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
spíš	spíš	k9	spíš
něco	něco	k3yInSc1	něco
jako	jako	k9	jako
lepidlo	lepidlo	k1gNnSc4	lepidlo
<g/>
,	,	kIx,	,
spojovací	spojovací	k2eAgNnSc4d1	spojovací
médium	médium	k1gNnSc4	médium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
drží	držet	k5eAaImIp3nS	držet
všechny	všechen	k3xTgFnPc4	všechen
věci	věc	k1gFnPc4	věc
pospolu	pospolu	k6eAd1	pospolu
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednotě	jednota	k1gFnSc6	jednota
uměleckého	umělecký	k2eAgInSc2d1	umělecký
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
co	co	k9	co
největšího	veliký	k2eAgInSc2d3	veliký
účinku	účinek	k1gInSc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Fischera	Fischer	k1gMnSc2	Fischer
však	však	k9	však
o	o	k7c4	o
pevnosti	pevnost	k1gFnPc4	pevnost
a	a	k8xC	a
přilnavosti	přilnavost	k1gFnPc4	přilnavost
Franzova	Franzův	k2eAgNnSc2d1	Franzovo
lepidla	lepidlo	k1gNnSc2	lepidlo
lze	lze	k6eAd1	lze
pochybovat	pochybovat	k5eAaImF	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všeho	všecek	k3xTgInSc2	všecek
nejvíc	nejvíc	k6eAd1	nejvíc
prý	prý	k9	prý
připomíná	připomínat	k5eAaImIp3nS	připomínat
jeho	jeho	k3xOp3gInPc4	jeho
(	(	kIx(	(
<g/>
Franzovy	Franzův	k2eAgInPc4d1	Franzův
<g/>
)	)	kIx)	)
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nejsou	být	k5eNaImIp3nP	být
surrealistické	surrealistický	k2eAgInPc1d1	surrealistický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
setkat	setkat	k5eAaPmF	setkat
kdekdo	kdekdo	k3yInSc1	kdekdo
s	s	k7c7	s
kdekým	kdekdo	k3yInSc7	kdekdo
a	a	k8xC	a
kdečím	kdečí	k3xOyIgNnSc6	kdečí
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
setkání	setkání	k1gNnSc1	setkání
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
napříč	napříč	k7c7	napříč
časem	čas	k1gInSc7	čas
a	a	k8xC	a
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obrazy	obraz	k1gInPc1	obraz
leží	ležet	k5eAaImIp3nP	ležet
spíše	spíše	k9	spíše
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
Franzovi	Franz	k1gMnSc3	Franz
upřít	upřít	k5eAaPmF	upřít
objevitelský	objevitelský	k2eAgInSc4d1	objevitelský
vhled	vhled	k1gInSc4	vhled
<g/>
:	:	kIx,	:
hudební	hudební	k2eAgNnSc4d1	hudební
napětí	napětí	k1gNnSc4	napětí
či	či	k8xC	či
rozpor	rozpor	k1gInSc4	rozpor
mezi	mezi	k7c7	mezi
první	první	k4xOgFnSc7	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
částí	část	k1gFnSc7	část
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
konzumním	konzumní	k2eAgNnSc7d1	konzumní
"	"	kIx"	"
<g/>
štěstím	štěstí	k1gNnSc7	štěstí
<g/>
"	"	kIx"	"
a	a	k8xC	a
dobou	doba	k1gFnSc7	doba
po	po	k7c6	po
Apokalypse	apokalypsa	k1gFnSc6	apokalypsa
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
se	se	k3xPyFc4	se
však	však	k9	však
skladatel	skladatel	k1gMnSc1	skladatel
jakkoliv	jakkoliv	k6eAd1	jakkoliv
snaží	snažit	k5eAaImIp3nS	snažit
měnit	měnit	k5eAaImF	měnit
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
styly	styl	k1gInPc4	styl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzájemně	vzájemně	k6eAd1	vzájemně
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
vrůstaly	vrůstat	k5eAaImAgFnP	vrůstat
a	a	k8xC	a
v	v	k7c6	v
sobě	se	k3xPyFc3	se
vyprchávaly	vyprchávat	k5eAaImAgFnP	vyprchávat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Fischera	Fischer	k1gMnSc2	Fischer
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedaří	dařit	k5eNaImIp3nS	dařit
udržet	udržet	k5eAaPmF	udržet
dojem	dojem	k1gInSc4	dojem
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Petra	Petr	k1gMnSc2	Petr
Kadlece	Kadlec	k1gMnSc2	Kadlec
(	(	kIx(	(
<g/>
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
Franzova	Franzův	k2eAgFnSc1d1	Franzova
opera	opera	k1gFnSc1	opera
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
nudí	nudit	k5eAaImIp3nS	nudit
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
údajně	údajně	k6eAd1	údajně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
hudba	hudba	k1gFnSc1	hudba
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
stylové	stylový	k2eAgFnSc6d1	stylová
rozrůzněnosti	rozrůzněnost	k1gFnSc6	rozrůzněnost
bezradně	bezradně	k6eAd1	bezradně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kadlece	Kadlec	k1gMnSc2	Kadlec
Franz	Franza	k1gFnPc2	Franza
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
mnoho	mnoho	k6eAd1	mnoho
hudebních	hudební	k2eAgFnPc2d1	hudební
řečí	řeč	k1gFnPc2	řeč
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vybírá	vybírat	k5eAaImIp3nS	vybírat
zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
posledního	poslední	k2eAgNnSc2d1	poslední
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
vážnohudební	vážnohudební	k2eAgFnSc1d1	vážnohudební
disonance	disonance	k1gFnSc1	disonance
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
střídá	střídat	k5eAaImIp3nS	střídat
podbízivý	podbízivý	k2eAgInSc4d1	podbízivý
muzikál	muzikál	k1gInSc4	muzikál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
heavy-metal	heavyetat	k5eAaImAgMnS	heavy-metat
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
předepsané	předepsaný	k2eAgFnPc1d1	předepsaná
pasáže	pasáž	k1gFnPc1	pasáž
ústí	ústit	k5eAaImIp3nP	ústit
do	do	k7c2	do
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hudebníci	hudebník	k1gMnPc1	hudebník
mají	mít	k5eAaImIp3nP	mít
pokoušet	pokoušet	k5eAaImF	pokoušet
o	o	k7c4	o
"	"	kIx"	"
<g/>
Totální	totální	k2eAgInSc4d1	totální
agresivní	agresivní	k2eAgInSc4d1	agresivní
chaos	chaos	k1gInSc4	chaos
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
Čapka	Čapek	k1gMnSc2	Čapek
se	se	k3xPyFc4	se
prý	prý	k9	prý
různé	různý	k2eAgInPc1d1	různý
žánry	žánr	k1gInPc1	žánr
propojují	propojovat	k5eAaImIp3nP	propojovat
a	a	k8xC	a
vykreslují	vykreslovat	k5eAaImIp3nP	vykreslovat
jeden	jeden	k4xCgInSc4	jeden
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Franzově	Franzův	k2eAgFnSc6d1	Franzova
opeře	opera	k1gFnSc6	opera
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
nestmelené	stmelený	k2eNgInPc1d1	stmelený
hudební	hudební	k2eAgInPc1d1	hudební
světy	svět	k1gInPc1	svět
poskládané	poskládaný	k2eAgInPc1d1	poskládaný
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Kadlec	Kadlec	k1gMnSc1	Kadlec
za	za	k7c4	za
paradoxně	paradoxně	k6eAd1	paradoxně
nejzdařilejší	zdařilý	k2eAgNnSc4d3	nejzdařilejší
považuje	považovat	k5eAaImIp3nS	považovat
muzikálové	muzikálový	k2eAgFnPc4d1	muzikálová
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
ukazovat	ukazovat	k5eAaImF	ukazovat
mediální	mediální	k2eAgFnSc1d1	mediální
masáž	masáž	k1gFnSc1	masáž
lidí	člověk	k1gMnPc2	člověk
v	v	k7c4	v
Morgan	morgan	k1gInSc4	morgan
Bay	Bay	k1gFnPc2	Bay
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
hudební	hudební	k2eAgFnSc2d1	hudební
zábavy	zábava	k1gFnSc2	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Kadlece	Kadlec	k1gMnSc2	Kadlec
<g/>
,	,	kIx,	,
převládá	převládat	k5eAaImIp3nS	převládat
hudba	hudba	k1gFnSc1	hudba
bez	bez	k7c2	bez
výrazných	výrazný	k2eAgFnPc2d1	výrazná
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
tísnivě	tísnivě	k6eAd1	tísnivě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
vlastně	vlastně	k9	vlastně
bezradně	bezradně	k6eAd1	bezradně
<g/>
;	;	kIx,	;
opera	opera	k1gFnSc1	opera
prý	prý	k9	prý
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nepojily	pojit	k5eNaImAgInP	pojit
ani	ani	k8xC	ani
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
předepsané	předepsaný	k2eAgFnPc1d1	předepsaná
v	v	k7c6	v
partituře	partitura	k1gFnSc6	partitura
<g/>
,	,	kIx,	,
k	k	k7c3	k
výraznější	výrazný	k2eAgFnSc3d2	výraznější
hudební	hudební	k2eAgFnSc3d1	hudební
řeči	řeč	k1gFnSc3	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kadlece	Kadlec	k1gMnSc2	Kadlec
hudba	hudba	k1gFnSc1	hudba
ve	v	k7c6	v
Válce	válka	k1gFnSc6	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
nemá	mít	k5eNaImIp3nS	mít
tvář	tvář	k1gFnSc1	tvář
a	a	k8xC	a
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
Franz	Franz	k1gInSc1	Franz
nemá	mít	k5eNaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
osobitý	osobitý	k2eAgInSc4d1	osobitý
rukopis	rukopis	k1gInSc4	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Šárky	Šárka	k1gFnSc2	Šárka
Lebedové	Lebedová	k1gFnSc2	Lebedová
(	(	kIx(	(
<g/>
e-kultura	eultura	k1gFnSc1	e-kultura
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opera	opera	k1gFnSc1	opera
hudebně	hudebně	k6eAd1	hudebně
mdlá	mdlý	k2eAgFnSc1d1	mdlá
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
scénická	scénický	k2eAgFnSc1d1	scénická
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
přítomen	přítomen	k2eAgInSc1d1	přítomen
žádný	žádný	k3yNgInSc1	žádný
zapamatovatelný	zapamatovatelný	k2eAgInSc1d1	zapamatovatelný
leitmotiv	leitmotiv	k1gInSc1	leitmotiv
<g/>
;	;	kIx,	;
opera	opera	k1gFnSc1	opera
podle	podle	k7c2	podle
Kadlecové	Kadlecová	k1gFnSc2	Kadlecová
postrádá	postrádat	k5eAaImIp3nS	postrádat
dramatický	dramatický	k2eAgInSc4d1	dramatický
náboj	náboj	k1gInSc4	náboj
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
ní	on	k3xPp3gFnSc2	on
zábavné	zábavný	k2eAgFnSc2d1	zábavná
vsuvky	vsuvka	k1gFnSc2	vsuvka
<g/>
.	.	kIx.	.
</s>
<s>
Šárka	Šárka	k1gFnSc1	Šárka
Lebedová	Lebedová	k1gFnSc1	Lebedová
<g/>
:	:	kIx,	:
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
e-kultura	eultura	k1gFnSc1	e-kultura
<g/>
,	,	kIx,	,
leden	leden	k1gInSc1	leden
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Táni	Táňa	k1gFnSc2	Táňa
Švehlové	Švehlové	k2eAgFnSc2d1	Švehlové
(	(	kIx(	(
<g/>
Literární	literární	k2eAgFnSc2d1	literární
noviny	novina	k1gFnSc2	novina
<g/>
)	)	kIx)	)
Franz	Franz	k1gMnSc1	Franz
<g/>
,	,	kIx,	,
Drábek	Drábek	k1gMnSc1	Drábek
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
Čapek	Čapek	k1gMnSc1	Čapek
sdílejí	sdílet	k5eAaImIp3nP	sdílet
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
eklektičnosti	eklektičnost	k1gFnSc6	eklektičnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
radostném	radostný	k2eAgNnSc6d1	radostné
mísení	mísení	k1gNnSc6	mísení
žánrů	žánr	k1gInPc2	žánr
vysokých	vysoký	k2eAgInPc2d1	vysoký
a	a	k8xC	a
nízkých	nízký	k2eAgFnPc2d1	nízká
<g/>
,	,	kIx,	,
operních	operní	k2eAgFnPc2d1	operní
árií	árie	k1gFnPc2	árie
<g/>
,	,	kIx,	,
muzikálových	muzikálový	k2eAgInPc2d1	muzikálový
vstupů	vstup	k1gInPc2	vstup
s	s	k7c7	s
tančícími	tančící	k2eAgInPc7d1	tančící
sbory	sbor	k1gInPc7	sbor
humrů	humr	k1gMnPc2	humr
a	a	k8xC	a
mořských	mořský	k2eAgMnPc2d1	mořský
koníčků	koníček	k1gMnPc2	koníček
a	a	k8xC	a
rozjívených	rozjívený	k2eAgFnPc2d1	rozjívená
reklam	reklama	k1gFnPc2	reklama
v	v	k7c6	v
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
katastrofě	katastrofa	k1gFnSc6	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Potenciální	potenciální	k2eAgNnSc1d1	potenciální
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
je	být	k5eAaImIp3nS	být
však	však	k9	však
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Švehlové	Švehlová	k1gFnSc2	Švehlová
<g/>
,	,	kIx,	,
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
už	už	k6eAd1	už
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
podstatě	podstata	k1gFnSc6	podstata
této	tento	k3xDgFnSc2	tento
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
metody	metoda	k1gFnSc2	metoda
a	a	k8xC	a
ve	v	k7c6	v
Válce	válka	k1gFnSc6	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
se	se	k3xPyFc4	se
vyjeví	vyjevit	k5eAaPmIp3nS	vyjevit
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nabudeme	nabýt	k5eAaPmIp1nP	nabýt
dojmu	dojem	k1gInSc3	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
namísto	namísto	k7c2	namísto
jediného	jediné	k1gNnSc2	jediné
sledujeme	sledovat	k5eAaImIp1nP	sledovat
tři	tři	k4xCgNnPc4	tři
odlišná	odlišný	k2eAgNnPc4d1	odlišné
vyprávění	vyprávění	k1gNnPc4	vyprávění
podobného	podobný	k2eAgInSc2d1	podobný
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spolu	spolu	k6eAd1	spolu
však	však	k9	však
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Drábkova	Drábkův	k2eAgFnSc1d1	Drábkova
rozjívená	rozjívený	k2eAgFnSc1d1	rozjívená
režie	režie	k1gFnSc1	režie
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
kostýmy	kostým	k1gInPc7	kostým
výtvarnice	výtvarnice	k1gFnSc2	výtvarnice
Simony	Simona	k1gFnSc2	Simona
Rybákové	Rybáková	k1gFnSc2	Rybáková
pojímají	pojímat	k5eAaImIp3nP	pojímat
Válku	válka	k1gFnSc4	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
ironicky	ironicky	k6eAd1	ironicky
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
jako	jako	k9	jako
pohádku	pohádka	k1gFnSc4	pohádka
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
komické	komický	k2eAgFnPc1d1	komická
figurky	figurka	k1gFnPc1	figurka
v	v	k7c6	v
barevných	barevný	k2eAgInPc6d1	barevný
oblečcích	obleček	k1gInPc6	obleček
a	a	k8xC	a
mloci	mlok	k1gMnPc1	mlok
jsou	být	k5eAaImIp3nP	být
roztomilými	roztomilý	k2eAgMnPc7d1	roztomilý
měkkými	měkký	k2eAgMnPc7d1	měkký
tvory	tvor	k1gMnPc7	tvor
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
<g/>
,	,	kIx,	,
libreto	libreto	k1gNnSc4	libreto
Rostislava	Rostislav	k1gMnSc2	Rostislav
Křivánka	Křivánek	k1gMnSc2	Křivánek
nabízí	nabízet	k5eAaImIp3nP	nabízet
civilní	civilní	k2eAgFnPc4d1	civilní
promluvy	promluva	k1gFnPc4	promluva
jako	jako	k8xC	jako
z	z	k7c2	z
komiksových	komiksový	k2eAgFnPc2d1	komiksová
bublin	bublina	k1gFnPc2	bublina
<g/>
,	,	kIx,	,
prázdné	prázdný	k2eAgNnSc1d1	prázdné
tlachání	tlachání	k1gNnSc1	tlachání
nebo	nebo	k8xC	nebo
ohrané	ohraný	k2eAgFnPc1d1	ohraná
metafory	metafora	k1gFnPc1	metafora
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
bohatá	bohatý	k2eAgFnSc1d1	bohatá
Franzova	Franzův	k2eAgFnSc1d1	Franzova
hudba	hudba	k1gFnSc1	hudba
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
blížící	blížící	k2eAgFnSc2d1	blížící
se	se	k3xPyFc4	se
katastrofy	katastrofa	k1gFnSc2	katastrofa
a	a	k8xC	a
nemožnosti	nemožnost	k1gFnSc2	nemožnost
společnosti	společnost	k1gFnSc2	společnost
ji	on	k3xPp3gFnSc4	on
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
uvědomit	uvědomit	k5eAaPmF	uvědomit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
jakkoliv	jakkoliv	k6eAd1	jakkoliv
jí	on	k3xPp3gFnSc3	on
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
hudba	hudba	k1gFnSc1	hudba
si	se	k3xPyFc3	se
udrží	udržet	k5eAaPmIp3nS	udržet
nadhled	nadhled	k1gInSc4	nadhled
nad	nad	k7c7	nad
Čapkovým	Čapkův	k2eAgInSc7d1	Čapkův
románem	román	k1gInSc7	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
lehkosti	lehkost	k1gFnSc6	lehkost
chronologicky	chronologicky	k6eAd1	chronologicky
převypráví	převyprávět	k5eAaPmIp3nS	převyprávět
v	v	k7c6	v
předehře	předehra	k1gFnSc6	předehra
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
začíná	začínat	k5eAaImIp3nS	začínat
jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
zvukem	zvuk	k1gInSc7	zvuk
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c4	v
dynamicky	dynamicky	k6eAd1	dynamicky
burácivou	burácivý	k2eAgFnSc4d1	burácivá
melodii	melodie	k1gFnSc4	melodie
s	s	k7c7	s
přechodem	přechod	k1gInSc7	přechod
v	v	k7c4	v
disharmonicky	disharmonicky	k6eAd1	disharmonicky
nelibý	libý	k2eNgInSc4d1	nelibý
skřípot	skřípot	k1gInSc4	skřípot
a	a	k8xC	a
skončí	skončit	k5eAaPmIp3nS	skončit
důrazným	důrazný	k2eAgInSc7d1	důrazný
úderem	úder	k1gInSc7	úder
na	na	k7c4	na
buben	buben	k1gInSc4	buben
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
východiskem	východisko	k1gNnSc7	východisko
dalšího	další	k2eAgNnSc2d1	další
dění	dění	k1gNnSc2	dění
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
uvědomujeme	uvědomovat	k5eAaImIp1nP	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
nejdůležitější	důležitý	k2eAgFnSc4d3	nejdůležitější
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
Švehlová	Švehlová	k1gFnSc1	Švehlová
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Varyše	Varyš	k1gMnSc2	Varyš
(	(	kIx(	(
<g/>
Týden	týden	k1gInSc1	týden
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Franzova	Franzův	k2eAgFnSc1d1	Franzova
opera	opera	k1gFnSc1	opera
solidní	solidní	k2eAgNnSc4d1	solidní
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
jejího	její	k3xOp3gMnSc4	její
autora	autor	k1gMnSc4	autor
mrzí	mrzet	k5eAaImIp3nS	mrzet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
konvenční	konvenční	k2eAgMnSc1d1	konvenční
<g/>
,	,	kIx,	,
tradicionalistický	tradicionalistický	k2eAgMnSc1d1	tradicionalistický
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spíš	spíš	k9	spíš
poučeně	poučeně	k6eAd1	poučeně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
kánon	kánon	k1gInSc4	kánon
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
by	by	kYmCp3nP	by
přicházel	přicházet	k5eAaImAgMnS	přicházet
s	s	k7c7	s
něčím	něco	k3yInSc7	něco
novým	nový	k2eAgFnPc3d1	nová
<g/>
;	;	kIx,	;
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
prý	prý	k9	prý
představuje	představovat	k5eAaImIp3nS	představovat
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
světě	svět	k1gInSc6	svět
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
spíše	spíše	k9	spíše
anachronismus	anachronismus	k1gInSc1	anachronismus
<g/>
.	.	kIx.	.
</s>
<s>
Franzova	Franzův	k2eAgFnSc1d1	Franzova
údajně	údajně	k6eAd1	údajně
eklektická	eklektický	k2eAgFnSc1d1	eklektická
<g/>
,	,	kIx,	,
poučená	poučený	k2eAgFnSc1d1	poučená
hudba	hudba	k1gFnSc1	hudba
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
Varyš	Varyš	k1gInSc4	Varyš
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc4d1	silný
základ	základ	k1gInSc4	základ
v	v	k7c6	v
melodii	melodie	k1gFnSc6	melodie
a	a	k8xC	a
ohlíží	ohlížet	k5eAaImIp3nS	ohlížet
se	se	k3xPyFc4	se
především	především	k9	především
po	po	k7c6	po
tradicích	tradice	k1gFnPc6	tradice
symfonické	symfonický	k2eAgFnSc2d1	symfonická
a	a	k8xC	a
sborové	sborový	k2eAgFnSc2d1	sborová
hudby	hudba	k1gFnSc2	hudba
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Varyše	Varyš	k1gMnSc2	Varyš
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Franz	Franz	k1gMnSc1	Franz
patrně	patrně	k6eAd1	patrně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
jako	jako	k9	jako
tvůrce	tvůrce	k1gMnSc1	tvůrce
hudby	hudba	k1gFnSc2	hudba
pro	pro	k7c4	pro
velké	velký	k2eAgInPc4d1	velký
hollywoodské	hollywoodský	k2eAgInPc4d1	hollywoodský
trháky	trhák	k1gInPc4	trhák
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Heleny	Helena	k1gFnSc2	Helena
Havlíkové	Havlíkové	k2eAgFnSc2d1	Havlíkové
(	(	kIx(	(
<g/>
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
)	)	kIx)	)
proměnil	proměnit	k5eAaPmAgMnS	proměnit
Franz	Franz	k1gMnSc1	Franz
Čapkovo	Čapkův	k2eAgNnSc4d1	Čapkovo
poselství	poselství	k1gNnSc4	poselství
v	v	k7c4	v
kaleidoskop	kaleidoskop	k1gInSc4	kaleidoskop
zvukových	zvukový	k2eAgMnPc2d1	zvukový
cákanců	cákanců	k?	cákanců
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
dojde	dojít	k5eAaPmIp3nS	dojít
tu	tu	k6eAd1	tu
na	na	k7c4	na
Janáčka	Janáček	k1gMnSc4	Janáček
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
na	na	k7c4	na
Martinů	Martinů	k1gFnSc4	Martinů
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Dvořáka	Dvořák	k1gMnSc4	Dvořák
<g/>
,	,	kIx,	,
Petra	Petr	k1gMnSc4	Petr
Kotvalda	Kotvald	k1gMnSc4	Kotvald
<g/>
,	,	kIx,	,
romantickou	romantický	k2eAgFnSc4d1	romantická
árii	árie	k1gFnSc4	árie
<g/>
,	,	kIx,	,
milostný	milostný	k2eAgInSc4d1	milostný
duet	duet	k1gInSc4	duet
<g/>
,	,	kIx,	,
odrhovačku	odrhovačka	k1gFnSc4	odrhovačka
či	či	k8xC	či
šlágr	šlágr	k1gInSc4	šlágr
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
<g/>
,	,	kIx,	,
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
Havlíková	Havlíková	k1gFnSc1	Havlíková
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nezdá	zdát	k5eNaImIp3nS	zdát
být	být	k5eAaImF	být
podstatné	podstatný	k2eAgNnSc1d1	podstatné
kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
proč	proč	k6eAd1	proč
tónové	tónový	k2eAgFnSc2d1	tónová
mazanice	mazanice	k1gFnSc2	mazanice
a	a	k8xC	a
hromadění	hromadění	k1gNnSc2	hromadění
not	nota	k1gFnPc2	nota
začnou	začít	k5eAaPmIp3nP	začít
a	a	k8xC	a
skončí	skončit	k5eAaPmIp3nP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Havlíkové	Havlíková	k1gFnSc2	Havlíková
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
operní	operní	k2eAgInSc4d1	operní
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
zcela	zcela	k6eAd1	zcela
jiných	jiný	k2eAgInPc6d1	jiný
základech	základ	k1gInPc6	základ
než	než	k8xS	než
scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
za	za	k7c7	za
níž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
Havlíková	Havlíková	k1gFnSc1	Havlíková
<g/>
,	,	kIx,	,
ověnčen	ověnčen	k2eAgInSc1d1	ověnčen
mnoha	mnoho	k4c7	mnoho
cenami	cena	k1gFnPc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Havlíková	Havlíková	k1gFnSc1	Havlíková
rovněž	rovněž	k9	rovněž
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
dirigenta	dirigent	k1gMnSc4	dirigent
těžké	těžký	k2eAgNnSc1d1	těžké
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
z	z	k7c2	z
hudebníků	hudebník	k1gMnPc2	hudebník
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
hraje	hrát	k5eAaImIp3nS	hrát
falešně	falešně	k6eAd1	falešně
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
partitura	partitura	k1gFnSc1	partitura
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zbytečně	zbytečně	k6eAd1	zbytečně
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Franzova	Franzův	k2eAgFnSc1d1	Franzova
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kandidatura	kandidatura	k1gFnSc1	kandidatura
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
Havlíková	Havlíková	k1gFnSc1	Havlíková
<g/>
,	,	kIx,	,
dodala	dodat	k5eAaPmAgFnS	dodat
opeře	opera	k1gFnSc3	opera
obskurní	obskurní	k2eAgInSc1d1	obskurní
nádech	nádech	k1gInSc1	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Havlíkové	Havlíkové	k2eAgNnSc2d1	Havlíkové
vedení	vedení	k1gNnSc2	vedení
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
nepoznala	poznat	k5eNaPmAgFnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Franzova	Franzův	k2eAgFnSc1d1	Franzova
opera	opera	k1gFnSc1	opera
nemá	mít	k5eNaImIp3nS	mít
parametry	parametr	k1gInPc4	parametr
pro	pro	k7c4	pro
uvedení	uvedení	k1gNnSc4	uvedení
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
první	první	k4xOgFnSc6	první
scéně	scéna	k1gFnSc6	scéna
a	a	k8xC	a
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
její	její	k3xOp3gFnSc1	její
premiéra	premiéra	k1gFnSc1	premiéra
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgInSc4d2	menší
zájem	zájem	k1gInSc4	zájem
bez	bez	k7c2	bez
prezidentského	prezidentský	k2eAgMnSc2d1	prezidentský
balábile	balábile	k6eAd1	balábile
<g/>
.	.	kIx.	.
</s>
<s>
Havlíková	Havlíková	k1gFnSc1	Havlíková
dále	daleko	k6eAd2	daleko
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Franz	Franz	k1gMnSc1	Franz
určitě	určitě	k6eAd1	určitě
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgMnSc1	žádný
dnešní	dnešní	k2eAgMnSc1d1	dnešní
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Verdi	Verd	k1gMnPc1	Verd
nebo	nebo	k8xC	nebo
v	v	k7c6	v
našich	náš	k3xOp1gInPc6	náš
poměrech	poměr	k1gInPc6	poměr
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
Dvořák	Dvořák	k1gMnSc1	Dvořák
a	a	k8xC	a
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Olgy	Olga	k1gFnSc2	Olga
Janáčkové	Janáčková	k1gFnSc2	Janáčková
(	(	kIx(	(
<g/>
Opera	opera	k1gFnSc1	opera
Plus	plus	k1gInSc1	plus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
zkušený	zkušený	k2eAgMnSc1d1	zkušený
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dovede	dovést	k5eAaPmIp3nS	dovést
užít	užít	k5eAaPmF	užít
takových	takový	k3xDgInPc2	takový
hudebních	hudební	k2eAgInPc2d1	hudební
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
sdělí	sdělit	k5eAaPmIp3nS	sdělit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
si	se	k3xPyFc3	se
předsevzal	předsevzít	k5eAaPmAgMnS	předsevzít
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
veleúspěšným	veleúspěšný	k2eAgMnSc7d1	veleúspěšný
skladatelem	skladatel	k1gMnSc7	skladatel
scénické	scénický	k2eAgFnSc2d1	scénická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
prý	prý	k9	prý
dokáže	dokázat	k5eAaPmIp3nS	dokázat
napsat	napsat	k5eAaPmF	napsat
stylově	stylově	k6eAd1	stylově
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
žádoucí	žádoucí	k2eAgInSc4d1	žádoucí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
obratně	obratně	k6eAd1	obratně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
souhrnného	souhrnný	k2eAgNnSc2d1	souhrnné
divadelního	divadelní	k2eAgNnSc2d1	divadelní
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Válce	válka	k1gFnSc6	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Janáčkové	Janáčková	k1gFnSc2	Janáčková
<g/>
,	,	kIx,	,
záměrně	záměrně	k6eAd1	záměrně
střídal	střídat	k5eAaImAgMnS	střídat
různé	různý	k2eAgFnPc4d1	různá
stylově	stylově	k6eAd1	stylově
kontrastující	kontrastující	k2eAgFnPc4d1	kontrastující
plochy	plocha	k1gFnPc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Drábkova	Drábkův	k2eAgFnSc1d1	Drábkova
inscenace	inscenace	k1gFnSc1	inscenace
se	se	k3xPyFc4	se
však	však	k9	však
prý	prý	k9	prý
s	s	k7c7	s
Franzovou	Franzový	k2eAgFnSc7d1	Franzový
operou	opera	k1gFnSc7	opera
minula	minulo	k1gNnSc2	minulo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povrchní	povrchní	k2eAgInSc1d1	povrchní
<g/>
,	,	kIx,	,
nezprostředkovává	zprostředkovávat	k5eNaImIp3nS	zprostředkovávat
hudbu	hudba	k1gFnSc4	hudba
divákům	divák	k1gMnPc3	divák
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ji	on	k3xPp3gFnSc4	on
bez	bez	k7c2	bez
hierarchizace	hierarchizace	k1gFnSc2	hierarchizace
inscenačních	inscenační	k2eAgInPc2d1	inscenační
prostředků	prostředek	k1gInPc2	prostředek
předehraje	předehrát	k5eAaPmIp3nS	předehrát
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
chtěla	chtít	k5eAaImAgFnS	chtít
pouze	pouze	k6eAd1	pouze
předvést	předvést	k5eAaPmF	předvést
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
všechno	všechen	k3xTgNnSc4	všechen
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Josefa	Josef	k1gMnSc2	Josef
Hermana	Herman	k1gMnSc2	Herman
(	(	kIx(	(
<g/>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
noviny	novina	k1gFnPc1	novina
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
nemůže	moct	k5eNaImIp3nS	moct
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgNnSc6d1	uzavřené
operním	operní	k2eAgNnSc6d1	operní
prostředí	prostředí	k1gNnSc6	prostředí
neprovokovat	provokovat	k5eNaImF	provokovat
<g/>
.	.	kIx.	.
</s>
<s>
Herman	Herman	k1gMnSc1	Herman
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Franz	Franz	k1gMnSc1	Franz
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
postmodernista	postmodernista	k1gMnSc1	postmodernista
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
ani	ani	k9	ani
nenapadne	napadnout	k5eNaPmIp3nS	napadnout
pachtit	pachtit	k5eAaImF	pachtit
se	se	k3xPyFc4	se
za	za	k7c7	za
originalitou	originalita	k1gFnSc7	originalita
<g/>
,	,	kIx,	,
za	za	k7c7	za
vytříbenou	vytříbený	k2eAgFnSc7d1	vytříbená
prověřenou	prověřený	k2eAgFnSc7d1	prověřená
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
běžná	běžný	k2eAgFnSc1d1	běžná
operní	operní	k2eAgFnSc1d1	operní
veřejnost	veřejnost	k1gFnSc1	veřejnost
neodpouští	odpouštět	k5eNaImIp3nS	odpouštět
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
považuje	považovat	k5eAaImIp3nS	považovat
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
autorovu	autorův	k2eAgFnSc4d1	autorova
slabinu	slabina	k1gFnSc4	slabina
a	a	k8xC	a
doklad	doklad	k1gInSc4	doklad
jeho	jeho	k3xOp3gFnSc2	jeho
nemohoucnosti	nemohoucnost	k1gFnSc2	nemohoucnost
komponovat	komponovat	k5eAaImF	komponovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hermana	Herman	k1gMnSc2	Herman
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
Helena	Helena	k1gFnSc1	Helena
Havlíková	Havlíková	k1gFnSc1	Havlíková
(	(	kIx(	(
<g/>
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
)	)	kIx)	)
svojí	svůj	k3xOyFgFnSc7	svůj
recenzí	recenze	k1gFnSc7	recenze
na	na	k7c6	na
Válku	válek	k1gInSc6	válek
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
zpronevěřila	zpronevěřit	k5eAaPmAgFnS	zpronevěřit
své	svůj	k3xOyFgFnSc3	svůj
profesi	profes	k1gFnSc3	profes
<g/>
,	,	kIx,	,
když	když	k8xS	když
politikařila	politikařit	k5eAaImAgFnS	politikařit
místo	místo	k1gNnSc4	místo
aby	aby	kYmCp3nS	aby
posuzovala	posuzovat	k5eAaImAgFnS	posuzovat
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Herman	Herman	k1gMnSc1	Herman
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
partitura	partitura	k1gFnSc1	partitura
Války	válka	k1gFnSc2	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
je	být	k5eAaImIp3nS	být
neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
a	a	k8xC	a
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
hudebnímu	hudební	k2eAgNnSc3d1	hudební
provedení	provedení	k1gNnSc3	provedení
co	co	k9	co
vytýkat	vytýkat	k5eAaImF	vytýkat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hudby	hudba	k1gFnSc2	hudba
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
patrná	patrný	k2eAgFnSc1d1	patrná
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
nadsázku	nadsázka	k1gFnSc4	nadsázka
<g/>
,	,	kIx,	,
lomenou	lomený	k2eAgFnSc4d1	lomená
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
v	v	k7c6	v
libretu	libreto	k1gNnSc6	libreto
akčními	akční	k2eAgFnPc7d1	akční
bitkami	bitka	k1gFnPc7	bitka
a	a	k8xC	a
civilistně	civilistně	k6eAd1	civilistně
zhudebněnými	zhudebněný	k2eAgInPc7d1	zhudebněný
dialogy	dialog	k1gInPc7	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hermana	Herman	k1gMnSc2	Herman
napsal	napsat	k5eAaBmAgMnS	napsat
Franz	Franz	k1gInSc4	Franz
posluchačsky	posluchačsky	k6eAd1	posluchačsky
velmi	velmi	k6eAd1	velmi
vstřícnou	vstřícný	k2eAgFnSc4d1	vstřícná
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
hudbu	hudba	k1gFnSc4	hudba
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
tonality	tonalita	k1gFnSc2	tonalita
<g/>
,	,	kIx,	,
s	s	k7c7	s
chytlavými	chytlavý	k2eAgInPc7d1	chytlavý
melodickými	melodický	k2eAgInPc7d1	melodický
motivy	motiv	k1gInPc7	motiv
a	a	k8xC	a
prokomponoval	prokomponovat	k5eAaPmAgMnS	prokomponovat
ji	on	k3xPp3gFnSc4	on
leitmotivy	leitmotiv	k1gInPc1	leitmotiv
<g/>
;	;	kIx,	;
ryze	ryze	k6eAd1	ryze
hudebně	hudebně	k6eAd1	hudebně
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
pokračování	pokračování	k1gNnSc2	pokračování
Franzovy	Franzův	k2eAgFnPc4d1	Franzova
1	[number]	k4	1
<g/>
.	.	kIx.	.
symfonie	symfonie	k1gFnPc1	symfonie
<g/>
,	,	kIx,	,
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
použil	použít	k5eAaPmAgMnS	použít
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
modely	model	k1gInPc4	model
a	a	k8xC	a
vzory	vzor	k1gInPc4	vzor
<g/>
,	,	kIx,	,
od	od	k7c2	od
tradičních	tradiční	k2eAgFnPc2d1	tradiční
oper	opera	k1gFnPc2	opera
přes	přes	k7c4	přes
Janáčka	Janáček	k1gMnSc4	Janáček
<g/>
,	,	kIx,	,
Stravinského	Stravinský	k2eAgMnSc4d1	Stravinský
<g/>
,	,	kIx,	,
Martinů	Martinů	k2eAgMnSc4d1	Martinů
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
,	,	kIx,	,
filmovou	filmový	k2eAgFnSc4d1	filmová
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
pochody	pochod	k1gInPc1	pochod
a	a	k8xC	a
písně	píseň	k1gFnPc1	píseň
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
popový	popový	k2eAgInSc4d1	popový
mainstream	mainstream	k1gInSc4	mainstream
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
výrazné	výrazný	k2eAgNnSc4d1	výrazné
pásmo	pásmo	k1gNnSc4	pásmo
slaboduchého	slaboduchý	k2eAgInSc2d1	slaboduchý
kýčovitého	kýčovitý	k2eAgInSc2d1	kýčovitý
muzikálu	muzikál	k1gInSc2	muzikál
prokládaného	prokládaný	k2eAgInSc2d1	prokládaný
reklamami	reklama	k1gFnPc7	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hermana	Herman	k1gMnSc2	Herman
leží	ležet	k5eAaImIp3nS	ležet
problém	problém	k1gInSc1	problém
provedení	provedení	k1gNnSc2	provedení
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
režisér	režisér	k1gMnSc1	režisér
Drábek	Drábek	k1gMnSc1	Drábek
režíroval	režírovat	k5eAaImAgMnS	režírovat
pouze	pouze	k6eAd1	pouze
její	její	k3xOp3gNnSc4	její
libreto	libreto	k1gNnSc4	libreto
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Franz	Franz	k1gMnSc1	Franz
svou	svůj	k3xOyFgFnSc7	svůj
hudební	hudební	k2eAgFnSc7d1	hudební
poetikou	poetika	k1gFnSc7	poetika
vyprávěl	vyprávět	k5eAaImAgInS	vyprávět
příběh	příběh	k1gInSc1	příběh
podstatně	podstatně	k6eAd1	podstatně
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
most	most	k1gInSc1	most
-	-	kIx~	-
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
1997	[number]	k4	1997
-	-	kIx~	-
hudební	hudební	k2eAgInSc1d1	hudební
komentář	komentář	k1gInSc1	komentář
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgInSc3d1	stejnojmenný
celovečernímu	celovečerní	k2eAgInSc3d1	celovečerní
filmu	film	k1gInSc3	film
Tomáše	Tomáš	k1gMnSc2	Tomáš
Vorla	Vorel	k1gMnSc2	Vorel
Faust	Faust	k1gMnSc1	Faust
-	-	kIx~	-
1998	[number]	k4	1998
-	-	kIx~	-
suita	suita	k1gFnSc1	suita
z	z	k7c2	z
hudby	hudba	k1gFnSc2	hudba
ke	k	k7c3	k
stejnojmenné	stejnojmenný	k2eAgFnSc3d1	stejnojmenná
inscenaci	inscenace	k1gFnSc3	inscenace
Zbyňka	Zbyněk	k1gMnSc2	Zbyněk
Srby	Srba	k1gMnSc2	Srba
Cirkus	cirkus	k1gInSc1	cirkus
Humberto	Humberta	k1gFnSc5	Humberta
-	-	kIx~	-
1998	[number]	k4	1998
-	-	kIx~	-
scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
adaptacím	adaptace	k1gFnPc3	adaptace
české	český	k2eAgFnSc2d1	Česká
klasiky	klasika	k1gFnSc2	klasika
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Zbyňka	Zbyněk	k1gMnSc2	Zbyněk
Srby	Srba	k1gMnSc2	Srba
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rokem	rok	k1gInSc7	rok
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
vsi	ves	k1gFnSc6	ves
Vlasty	Vlasta	k1gMnSc2	Vlasta
Redla	Redl	k1gMnSc2	Redl
a	a	k8xC	a
Lucernou	lucerna	k1gFnSc7	lucerna
Martina	Martin	k1gMnSc2	Martin
Dohnala	Dohnal	k1gMnSc2	Dohnal
<g/>
)	)	kIx)	)
Júdit	Júdit	k1gMnSc1	Júdit
<g/>
,	,	kIx,	,
Tractatus	Tractatus	k1gMnSc1	Tractatus
Pacis	Pacis	k1gFnSc2	Pacis
-	-	kIx~	-
1999	[number]	k4	1999
-	-	kIx~	-
suita	suita	k1gFnSc1	suita
z	z	k7c2	z
hudby	hudba	k1gFnSc2	hudba
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgNnSc3d1	stejnojmenné
scénickému	scénický	k2eAgNnSc3d1	scénické
oratoriu	oratorium	k1gNnSc3	oratorium
Rostislava	Rostislav	k1gMnSc2	Rostislav
Křivánka	Křivánek	k1gMnSc2	Křivánek
(	(	kIx(	(
<g/>
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Věry	Věra	k1gFnSc2	Věra
Herajtové	Herajtová	k1gFnSc2	Herajtová
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
pro	pro	k7c4	pro
Národní	národní	k2eAgNnSc4d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
-	-	kIx~	-
1999	[number]	k4	1999
-	-	kIx~	-
suity	suita	k1gFnPc1	suita
ze	z	k7c2	z
scénických	scénický	k2eAgFnPc2d1	scénická
hudeb	hudba	k1gFnPc2	hudba
k	k	k7c3	k
představením	představení	k1gNnSc7	představení
Bloudění	bloudění	k1gNnPc2	bloudění
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Pašije	pašije	k1gFnSc1	pašije
a	a	k8xC	a
Maryša	Maryša	k1gFnSc1	Maryša
Gazdina	gazdina	k1gFnSc1	gazdina
roba	roba	k1gFnSc1	roba
-	-	kIx~	-
2000	[number]	k4	2000
sestřih	sestřih	k1gInSc1	sestřih
studiového	studiový	k2eAgInSc2d1	studiový
záznamu	záznam	k1gInSc2	záznam
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
inscenace	inscenace	k1gFnSc2	inscenace
J.	J.	kA	J.
A.	A.	kA	A.
Pitínského	Pitínský	k2eAgMnSc4d1	Pitínský
Rút	Rút	k1gFnSc7	Rút
-	-	kIx~	-
2001	[number]	k4	2001
-	-	kIx~	-
suita	suita	k1gFnSc1	suita
z	z	k7c2	z
hudby	hudba	k1gFnSc2	hudba
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgNnSc3d1	stejnojmenné
scénickému	scénický	k2eAgNnSc3d1	scénické
oratoriu	oratorium	k1gNnSc3	oratorium
Rostislava	Rostislav	k1gMnSc2	Rostislav
Křivánka	Křivánek	k1gMnSc2	Křivánek
(	(	kIx(	(
<g/>
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Věry	Věra	k1gFnSc2	Věra
Herajtové	Herajtový	k2eAgInPc4d1	Herajtový
<g/>
)	)	kIx)	)
Kruhy	kruh	k1gInPc4	kruh
do	do	k7c2	do
ledu	led	k1gInSc2	led
-	-	kIx~	-
2002	[number]	k4	2002
-	-	kIx~	-
suita-kantáta	suitaantáta	k1gFnSc1	suita-kantáta
z	z	k7c2	z
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
inscenaci	inscenace	k1gFnSc3	inscenace
pražského	pražský	k2eAgNnSc2d1	Pražské
<g />
.	.	kIx.	.
</s>
<s>
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Markéta	Markéta	k1gFnSc1	Markéta
Lazarová	Lazarová	k1gFnSc1	Lazarová
Ha-Hamlet	Ha-Hamlet	k1gInSc1	Ha-Hamlet
!!!	!!!	k?	!!!
(	(	kIx(	(
<g/>
Shakespearovská	shakespearovský	k2eAgFnSc1d1	shakespearovská
svita	svita	k1gFnSc1	svita
<g/>
)	)	kIx)	)
-	-	kIx~	-
2005	[number]	k4	2005
-	-	kIx~	-
suita	suita	k1gFnSc1	suita
ze	z	k7c2	z
scénických	scénický	k2eAgFnPc2d1	scénická
skladeb	skladba	k1gFnPc2	skladba
k	k	k7c3	k
shakespearovským	shakespearovský	k2eAgNnPc3d1	shakespearovské
představením	představení	k1gNnPc3	představení
Triptych	triptych	k1gInSc4	triptych
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
Pavla	Pavla	k1gFnSc1	Pavla
I	i	k9	i
<g/>
,	,	kIx,	,
Amfitryon	Amfitryon	k1gNnSc1	Amfitryon
<g/>
,	,	kIx,	,
Znamení	znamení	k1gNnSc1	znamení
kříže	kříž	k1gInSc2	kříž
-	-	kIx~	-
2007	[number]	k4	2007
-	-	kIx~	-
tři	tři	k4xCgFnPc1	tři
suity	suita	k1gFnPc1	suita
ze	z	k7c2	z
scénických	scénický	k2eAgFnPc2d1	scénická
skladeb	skladba	k1gFnPc2	skladba
k	k	k7c3	k
inscenacím	inscenace	k1gFnPc3	inscenace
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Skeletoni	Skeleton	k1gMnPc1	Skeleton
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
2007	[number]	k4	2007
-	-	kIx~	-
hudba	hudba	k1gFnSc1	hudba
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
středometrážního	středometrážní	k2eAgInSc2d1	středometrážní
filmu	film	k1gInSc2	film
filmu	film	k1gInSc2	film
Jakuba	Jakub	k1gMnSc2	Jakub
Hussara	Hussar	k1gMnSc2	Hussar
Cenu	cena	k1gFnSc4	cena
Nadace	nadace	k1gFnSc2	nadace
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hudba	hudba	k1gFnSc1	hudba
<g/>
"	"	kIx"	"
získal	získat	k5eAaPmAgMnS	získat
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
:	:	kIx,	:
1998	[number]	k4	1998
-	-	kIx~	-
za	za	k7c4	za
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
Bloudění	bloudění	k1gNnSc2	bloudění
(	(	kIx(	(
<g/>
Durych	Durych	k1gInSc1	Durych
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g />
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
A.	A.	kA	A.
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
2000	[number]	k4	2000
-	-	kIx~	-
za	za	k7c4	za
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
Hamlet	Hamlet	k1gMnSc1	Hamlet
(	(	kIx(	(
<g/>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
V.	V.	kA	V.
Morávek	Morávek	k1gMnSc1	Morávek
<g/>
,	,	kIx,	,
Klicperovo	Klicperův	k2eAgNnSc1d1	Klicperovo
divadlo	divadlo	k1gNnSc1	divadlo
Hradec	Hradec	k1gInSc4	Hradec
Králové	Králová	k1gFnSc2	Králová
2002	[number]	k4	2002
-	-	kIx~	-
za	za	k7c4	za
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
Markéta	Markéta	k1gFnSc1	Markéta
Lazarová	Lazarová	k1gFnSc1	Lazarová
(	(	kIx(	(
<g/>
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g />
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
A.	A.	kA	A.
Pitínský	Pitínský	k2eAgMnSc1d1	Pitínský
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
2005	[number]	k4	2005
-	-	kIx~	-
za	za	k7c4	za
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
Znamení	znamení	k1gNnSc2	znamení
kříže	kříž	k1gInSc2	kříž
(	(	kIx(	(
<g/>
Calderón	Calderón	k1gInSc1	Calderón
de	de	k?	de
la	la	k1gNnSc1	la
Barca	Barca	k1gMnSc1	Barca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
2006	[number]	k4	2006
-	-	kIx~	-
za	za	k7c4	za
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
Amfitryon	Amfitryona	k1gFnPc2	Amfitryona
(	(	kIx(	(
<g/>
Moliére	Moliér	k1gMnSc5	Moliér
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Hana	Hana	k1gFnSc1	Hana
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
2007	[number]	k4	2007
-	-	kIx~	-
za	za	k7c4	za
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
divadelní	divadelní	k2eAgFnSc3d1	divadelní
hře	hra	k1gFnSc3	hra
Smrt	smrt	k1gFnSc4	smrt
Pavla	Pavel	k1gMnSc2	Pavel
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
sedmkrát	sedmkrát	k6eAd1	sedmkrát
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
<g/>
"	"	kIx"	"
ještě	ještě	k6eAd1	ještě
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
nominován	nominovat	k5eAaBmNgMnS	nominovat
byl	být	k5eAaImAgMnS	být
proto	proto	k8xC	proto
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
představení	představení	k1gNnSc3	představení
Faust	Faust	k1gInSc4	Faust
v	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
jiný	jiný	k2eAgInSc4d1	jiný
umělecký	umělecký	k2eAgInSc4d1	umělecký
výkon	výkon	k1gInSc4	výkon
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
představení	představení	k1gNnSc3	představení
Júdit	Júdita	k1gFnPc2	Júdita
v	v	k7c6	v
Městském	městský	k2eAgNnSc6d1	Městské
divadle	divadlo	k1gNnSc6	divadlo
Zlín	Zlín	k1gInSc4	Zlín
a	a	k8xC	a
Maryša	Maryša	k1gFnSc1	Maryša
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
za	za	k7c4	za
zpěvohru	zpěvohra	k1gFnSc4	zpěvohra
Pokušení	pokušení	k1gNnSc1	pokušení
sv.	sv.	kA	sv.
Antonína	Antonín	k1gMnSc2	Antonín
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Semafor	Semafor	k1gInSc1	Semafor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
představení	představení	k1gNnSc3	představení
Běsi	běs	k1gMnPc1	běs
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Dlouhé	Dlouhá	k1gFnSc6	Dlouhá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
představení	představení	k1gNnSc3	představení
Macbeth	Macbetha	k1gFnPc2	Macbetha
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Divadle	divadlo	k1gNnSc6	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
za	za	k7c4	za
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Válka	Válka	k1gMnSc1	Válka
s	s	k7c7	s
mloky	mlok	k1gMnPc7	mlok
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
Divadelních	divadelní	k2eAgFnPc2d1	divadelní
novin	novina	k1gFnPc2	novina
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
hudební	hudební	k2eAgNnSc4d1	hudební
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
"	"	kIx"	"
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Suchým	Suchý	k1gMnSc7	Suchý
za	za	k7c4	za
zpěvohru	zpěvohra	k1gFnSc4	zpěvohra
Pokušení	pokušení	k1gNnSc1	pokušení
sv.	sv.	kA	sv.
Antonína	Antonín	k1gMnSc2	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
nominován	nominovat	k5eAaBmNgMnS	nominovat
za	za	k7c4	za
operu-oratorium	operuratorium	k1gNnSc4	operu-oratorium
Údolí	údolí	k1gNnSc2	údolí
suchých	suchý	k2eAgFnPc2d1	suchá
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
získal	získat	k5eAaPmAgMnS	získat
výroční	výroční	k2eAgFnSc4d1	výroční
cenu	cena	k1gFnSc4	cena
Ochranného	ochranný	k2eAgInSc2d1	ochranný
svazu	svaz	k1gInSc2	svaz
autorského	autorský	k2eAgInSc2d1	autorský
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Vážná	vážný	k2eAgFnSc1d1	vážná
skladba	skladba	k1gFnSc1	skladba
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
za	za	k7c4	za
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
báseň	báseň	k1gFnSc4	báseň
Radobÿ	Radobÿ	k1gFnSc2	Radobÿ
<g/>
.	.	kIx.	.
</s>
<s>
Cíleně	cíleně	k6eAd1	cíleně
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
malovat	malovat	k5eAaImF	malovat
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
deseti	deset	k4xCc6	deset
až	až	k8xS	až
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
prvním	první	k4xOgMnSc6	první
rádcem	rádce	k1gMnSc7	rádce
a	a	k8xC	a
učitelem	učitel	k1gMnSc7	učitel
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
dědeček	dědeček	k1gMnSc1	dědeček
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
milovník	milovník	k1gMnSc1	milovník
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
amatérský	amatérský	k2eAgMnSc1d1	amatérský
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
vlastním	vlastní	k2eAgInSc7d1	vlastní
výtvarným	výtvarný	k2eAgInSc7d1	výtvarný
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Franz	Franz	k1gMnSc1	Franz
osobně	osobně	k6eAd1	osobně
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
červenočerný	červenočerný	k2eAgInSc4d1	červenočerný
obraz	obraz	k1gInSc4	obraz
ukřižovaného	ukřižovaný	k2eAgMnSc2d1	ukřižovaný
šaška	šašek	k1gMnSc2	šašek
bez	bez	k7c2	bez
tváře	tvář	k1gFnSc2	tvář
s	s	k7c7	s
názvem	název	k1gInSc7	název
Zlá	zlý	k2eAgFnSc1d1	zlá
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
husákovskou	husákovský	k2eAgFnSc4d1	husákovská
realitu	realita	k1gFnSc4	realita
a	a	k8xC	a
který	který	k3yIgMnSc1	který
namaloval	namalovat	k5eAaPmAgMnS	namalovat
ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
či	či	k8xC	či
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
šestnácti	šestnáct	k4xCc6	šestnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
první	první	k4xOgFnSc4	první
výstavu	výstava	k1gFnSc4	výstava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Galerii	galerie	k1gFnSc6	galerie
Futurum	futurum	k1gNnSc1	futurum
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgNnPc7d1	základní
tématy	téma	k1gNnPc7	téma
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
díla	dílo	k1gNnSc2	dílo
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Franze	Franze	k1gFnSc2	Franze
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
ambivalentnost	ambivalentnost	k1gFnSc1	ambivalentnost
vztahu	vztah	k1gInSc2	vztah
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
<g/>
,	,	kIx,	,
pomíjivost	pomíjivost	k1gFnSc4	pomíjivost
lidských	lidský	k2eAgInPc2d1	lidský
zásahů	zásah	k1gInPc2	zásah
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
postupné	postupný	k2eAgNnSc4d1	postupné
mizení	mizení	k1gNnSc4	mizení
jeho	jeho	k3xOp3gFnPc2	jeho
stop	stopa	k1gFnPc2	stopa
v	v	k7c6	v
nemilosrdně	milosrdně	k6eNd1	milosrdně
obrodivém	obrodivý	k2eAgInSc6d1	obrodivý
kontextu	kontext	k1gInSc6	kontext
elementární	elementární	k2eAgFnSc2d1	elementární
přírodní	přírodní	k2eAgFnSc2d1	přírodní
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
reflexe	reflexe	k1gFnSc1	reflexe
mnohých	mnohý	k2eAgInPc2d1	mnohý
modelů	model	k1gInPc2	model
dnešního	dnešní	k2eAgNnSc2d1	dnešní
sociálního	sociální	k2eAgNnSc2d1	sociální
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
usvědčování	usvědčování	k1gNnSc4	usvědčování
ze	z	k7c2	z
směšnosti	směšnost	k1gFnSc2	směšnost
a	a	k8xC	a
pokrytectví	pokrytectví	k1gNnSc2	pokrytectví
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
letním	letní	k2eAgInSc6d1	letní
šumavském	šumavský	k2eAgInSc6d1	šumavský
ateliéru	ateliér	k1gInSc6	ateliér
tvořil	tvořit	k5eAaImAgInS	tvořit
básnivé	básnivý	k2eAgInPc1d1	básnivý
expresivně	expresivně	k6eAd1	expresivně
lyrické	lyrický	k2eAgInPc1d1	lyrický
obrazy	obraz	k1gInPc1	obraz
plné	plný	k2eAgInPc1d1	plný
archetypálních	archetypální	k2eAgMnPc2d1	archetypální
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
splynutí	splynutí	k1gNnSc1	splynutí
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
pamětí	paměť	k1gFnSc7	paměť
<g/>
,	,	kIx,	,
reflektující	reflektující	k2eAgFnSc7d1	reflektující
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
okolní	okolní	k2eAgFnSc4d1	okolní
přírodu	příroda	k1gFnSc4	příroda
(	(	kIx(	(
<g/>
obrazové	obrazový	k2eAgInPc1d1	obrazový
cykly	cyklus	k1gInPc1	cyklus
Studně	studně	k1gFnSc2	studně
<g/>
,	,	kIx,	,
Slunečnice	slunečnice	k1gFnSc2	slunečnice
<g/>
,	,	kIx,	,
Znamení	znamení	k1gNnSc2	znamení
zóny	zóna	k1gFnSc2	zóna
či	či	k8xC	či
Variace	variace	k1gFnSc2	variace
o	o	k7c6	o
portrétu	portrét	k1gInSc6	portrét
Antona	Anton	k1gMnSc2	Anton
Brucknera	Bruckner	k1gMnSc2	Bruckner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
výtvarné	výtvarný	k2eAgFnSc6d1	výtvarná
tvorbě	tvorba	k1gFnSc6	tvorba
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
věcí	věc	k1gFnPc2	věc
a	a	k8xC	a
jevů	jev	k1gInPc2	jev
společenských	společenský	k2eAgInPc2d1	společenský
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
lyriky	lyrika	k1gFnSc2	lyrika
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
obrazech	obraz	k1gInPc6	obraz
objevuje	objevovat	k5eAaImIp3nS	objevovat
přímé	přímý	k2eAgNnSc4d1	přímé
nazývání	nazývání	k1gNnSc4	nazývání
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
jasná	jasný	k2eAgFnSc1d1	jasná
předmětnost	předmětnost	k1gFnSc1	předmětnost
<g/>
,	,	kIx,	,
silná	silný	k2eAgFnSc1d1	silná
angažovanost	angažovanost	k1gFnSc1	angažovanost
a	a	k8xC	a
nelhostejnost	nelhostejnost	k1gFnSc1	nelhostejnost
k	k	k7c3	k
negativním	negativní	k2eAgInPc3d1	negativní
jevům	jev	k1gInPc3	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nás	my	k3xPp1nPc4	my
všechny	všechen	k3xTgFnPc4	všechen
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Komentuje	komentovat	k5eAaBmIp3nS	komentovat
kýče	kýč	k1gInSc2	kýč
a	a	k8xC	a
šmé	šmé	k1gNnSc2	šmé
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
ironizuje	ironizovat	k5eAaImIp3nS	ironizovat
<g/>
,	,	kIx,	,
satirizuje	satirizovat	k5eAaImIp3nS	satirizovat
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
vlastně	vlastně	k9	vlastně
křičí	křičet	k5eAaImIp3nS	křičet
a	a	k8xC	a
řve	řvát	k5eAaImIp3nS	řvát
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
vzteky	vzteky	k6eAd1	vzteky
bez	bez	k7c2	bez
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
ironie	ironie	k1gFnSc1	ironie
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
vždycky	vždycky	k6eAd1	vždycky
i	i	k9	i
sebeironií	sebeironie	k1gFnSc7	sebeironie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
schopnost	schopnost	k1gFnSc1	schopnost
a	a	k8xC	a
ochota	ochota	k1gFnSc1	ochota
udělat	udělat	k5eAaPmF	udělat
si	se	k3xPyFc3	se
legraci	legrace	k1gFnSc4	legrace
sám	sám	k3xTgInSc1	sám
ze	z	k7c2	z
sebe	se	k3xPyFc2	se
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
výsostně	výsostně	k6eAd1	výsostně
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kunsthistorika	kunsthistorik	k1gMnSc2	kunsthistorik
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Drápala	Drápal	k1gMnSc2	Drápal
se	se	k3xPyFc4	se
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
nekompromisnosti	nekompromisnost	k1gFnSc3	nekompromisnost
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc3	schopnost
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
zábran	zábrana	k1gFnPc2	zábrana
"	"	kIx"	"
<g/>
stává	stávat	k5eAaImIp3nS	stávat
burcujícím	burcující	k2eAgMnSc7d1	burcující
glosátorem	glosátor	k1gMnSc7	glosátor
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
nemilosrdným	milosrdný	k2eNgMnSc7d1	nemilosrdný
kritikem	kritik	k1gMnSc7	kritik
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
zbavené	zbavený	k2eAgNnSc1d1	zbavené
vší	veš	k1gFnSc7	veš
naivity	naivita	k1gFnSc2	naivita
a	a	k8xC	a
vyumělkovaných	vyumělkovaný	k2eAgFnPc2d1	vyumělkovaná
příkras	příkrasa	k1gFnPc2	příkrasa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c6	na
akcelerující	akcelerující	k2eAgFnSc3d1	akcelerující
civilizační	civilizační	k2eAgFnSc3d1	civilizační
autodestrukci	autodestrukce	k1gFnSc3	autodestrukce
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
nás	my	k3xPp1nPc4	my
prizmatem	prizma	k1gNnSc7	prizma
apokalypsy	apokalypsa	k1gFnSc2	apokalypsa
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
se	se	k3xPyFc4	se
chtě	chtě	k6eAd1	chtě
nechtě	chtě	k6eNd1	chtě
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
stal	stát	k5eAaPmAgMnS	stát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
svá	svůj	k3xOyFgNnPc4	svůj
současná	současný	k2eAgNnPc4d1	současné
plátna	plátno	k1gNnPc4	plátno
zaplňuje	zaplňovat	k5eAaImIp3nS	zaplňovat
podle	podle	k7c2	podle
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Drápala	Drápal	k1gMnSc2	Drápal
zjeveními	zjevení	k1gNnPc7	zjevení
jakoby	jakoby	k8xS	jakoby
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
kosmické	kosmický	k2eAgFnSc2d1	kosmická
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
najdeme	najít	k5eAaPmIp1nP	najít
zasněžené	zasněžený	k2eAgFnPc1d1	zasněžená
muchomůrky	muchomůrky	k?	muchomůrky
a	a	k8xC	a
vedle	vedle	k7c2	vedle
nich	on	k3xPp3gInPc2	on
Ježíška	ježíšek	k1gInSc2	ježíšek
<g/>
,	,	kIx,	,
pomalu	pomalu	k6eAd1	pomalu
umrzajícího	umrzající	k2eAgNnSc2d1	umrzající
ve	v	k7c6	v
fialově	fialově	k6eAd1	fialově
nevlídném	vlídný	k2eNgInSc6d1	nevlídný
lednovém	lednový	k2eAgInSc6d1	lednový
lese	les	k1gInSc6	les
-	-	kIx~	-
co	co	k3yQnSc1	co
ostatně	ostatně	k6eAd1	ostatně
také	také	k9	také
s	s	k7c7	s
Ježíškem	ježíšek	k1gInSc7	ježíšek
po	po	k7c6	po
Vánocích	Vánoce	k1gFnPc6	Vánoce
<g/>
?	?	kIx.	?
</s>
<s>
Modré	modrý	k2eAgMnPc4d1	modrý
králíky	králík	k1gMnPc4	králík
a	a	k8xC	a
nebe	nebe	k1gNnSc4	nebe
plné	plný	k2eAgNnSc4d1	plné
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
kočku	kočka	k1gFnSc4	kočka
sledující	sledující	k2eAgFnSc4d1	sledující
televizní	televizní	k2eAgFnSc4d1	televizní
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
,	,	kIx,	,
umístěnou	umístěný	k2eAgFnSc4d1	umístěná
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
totemu	totem	k1gInSc2	totem
z	z	k7c2	z
prasečích	prasečí	k2eAgFnPc2d1	prasečí
hlav	hlava	k1gFnPc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Obřího	obří	k2eAgMnSc2d1	obří
Disneyho	Disney	k1gMnSc2	Disney
myšáka	myšák	k1gMnSc2	myšák
<g/>
,	,	kIx,	,
večeřícího	večeřící	k2eAgMnSc2d1	večeřící
rozteklý	rozteklý	k2eAgInSc4d1	rozteklý
dort	dort	k1gInSc4	dort
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
dvanácti	dvanáct	k4xCc7	dvanáct
ženskými	ženský	k2eAgNnPc7d1	ženské
přirozeními	přirození	k1gNnPc7	přirození
<g/>
,	,	kIx,	,
symbolizujícími	symbolizující	k2eAgNnPc7d1	symbolizující
dvanáct	dvanáct	k4xCc4	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
a	a	k8xC	a
navozujícími	navozující	k2eAgNnPc7d1	navozující
téma	téma	k1gNnPc7	téma
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
příměr	příměr	k1gInSc1	příměr
falše	faleš	k1gFnSc2	faleš
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kosočtverec	kosočtverec	k1gInSc1	kosočtverec
Jidáš	jidáš	k1gInSc1	jidáš
pronáší	pronášet	k5eAaImIp3nS	pronášet
slovo	slovo	k1gNnSc4	slovo
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgFnPc1d1	ostatní
vagíny	vagína	k1gFnPc1	vagína
se	se	k3xPyFc4	se
překřikují	překřikovat	k5eAaImIp3nP	překřikovat
slovy	slovo	k1gNnPc7	slovo
jako	jako	k8xS	jako
Láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
Svědomí	svědomí	k1gNnSc1	svědomí
<g/>
,	,	kIx,	,
Řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
Tolerance	tolerance	k1gFnSc1	tolerance
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
už	už	k9	už
zcela	zcela	k6eAd1	zcela
vyprázdněnými	vyprázdněný	k2eAgInPc7d1	vyprázdněný
symboly	symbol	k1gInPc7	symbol
<g/>
,	,	kIx,	,
sakrální	sakrální	k2eAgNnPc1d1	sakrální
témata	téma	k1gNnPc1	téma
jsou	být	k5eAaImIp3nP	být
destabilizována	destabilizován	k2eAgNnPc1d1	destabilizováno
prvky	prvek	k1gInPc7	prvek
Matějské	matějský	k2eAgFnSc2d1	Matějská
poutě	pouť	k1gFnSc2	pouť
a	a	k8xC	a
Coca-Coly	cocaola	k1gFnSc2	coca-cola
<g/>
.	.	kIx.	.
</s>
<s>
Razantní	razantní	k2eAgFnSc7d1	razantní
<g/>
,	,	kIx,	,
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
malbou	malba	k1gFnSc7	malba
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
solitérem	solitér	k1gMnSc7	solitér
v	v	k7c6	v
zobrazování	zobrazování	k1gNnSc6	zobrazování
klipovitosti	klipovitost	k1gFnSc2	klipovitost
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
zvrácenosti	zvrácenost	k1gFnSc2	zvrácenost
lidské	lidský	k2eAgFnSc2d1	lidská
říše	říš	k1gFnSc2	říš
<g/>
;	;	kIx,	;
i	i	k9	i
proto	proto	k8xC	proto
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
obrazech	obraz	k1gInPc6	obraz
hovoří	hovořit	k5eAaImIp3nS	hovořit
zásadně	zásadně	k6eAd1	zásadně
zvířata	zvíře	k1gNnPc4	zvíře
či	či	k8xC	či
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
hlas	hlas	k1gInSc1	hlas
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
naléhavější	naléhavý	k2eAgInSc1d2	naléhavější
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Franze	Franze	k1gFnSc2	Franze
hudebník	hudebník	k1gMnSc1	hudebník
nebo	nebo	k8xC	nebo
malíř	malíř	k1gMnSc1	malíř
nazývat	nazývat	k5eAaImF	nazývat
umělcem	umělec	k1gMnSc7	umělec
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
především	především	k9	především
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
komiks	komiks	k1gInSc1	komiks
i	i	k8xC	i
kýčovité	kýčovitý	k2eAgFnPc1d1	kýčovitá
koláže	koláž	k1gFnPc1	koláž
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
obrazů	obraz	k1gInPc2	obraz
přitom	přitom	k6eAd1	přitom
podle	podle	k7c2	podle
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
ještě	ještě	k9	ještě
potřebovaly	potřebovat	k5eAaImAgInP	potřebovat
domalovat	domalovat	k5eAaPmF	domalovat
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
to	ten	k3xDgNnSc4	ten
zdůvodnil	zdůvodnit	k5eAaPmAgInS	zdůvodnit
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
výrok	výrok	k1gInSc4	výrok
Pabla	Pabl	k1gMnSc2	Pabl
Picassa	Picass	k1gMnSc2	Picass
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dokončený	dokončený	k2eAgInSc1d1	dokončený
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Nedokončený	dokončený	k2eNgMnSc1d1	nedokončený
je	být	k5eAaImIp3nS	být
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Významný	významný	k2eAgInSc1d1	významný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
Franzovu	Franzův	k2eAgFnSc4d1	Franzova
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
tvorbu	tvorba	k1gFnSc4	tvorba
měl	mít	k5eAaImAgInS	mít
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
úmrtí	úmrtí	k1gNnSc2	úmrtí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
prakticky	prakticky	k6eAd1	prakticky
ignorovaný	ignorovaný	k2eAgMnSc1d1	ignorovaný
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
Andrej	Andrej	k1gMnSc1	Andrej
Bělocvětov	Bělocvětov	k1gInSc1	Bělocvětov
<g/>
;	;	kIx,	;
Franz	Franz	k1gMnSc1	Franz
věnoval	věnovat	k5eAaImAgMnS	věnovat
propagaci	propagace	k1gFnSc4	propagace
a	a	k8xC	a
kunsthistorickému	kunsthistorický	k2eAgNnSc3d1	Kunsthistorické
zhodnocení	zhodnocení	k1gNnSc3	zhodnocení
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
mnoho	mnoho	k6eAd1	mnoho
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
seznámení	seznámení	k1gNnSc3	seznámení
s	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
významným	významný	k2eAgMnSc7d1	významný
českým	český	k2eAgMnSc7d1	český
výtvarníkem	výtvarník	k1gMnSc7	výtvarník
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
dílem	díl	k1gInSc7	díl
Franz	Franz	k1gMnSc1	Franz
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
...	...	k?	...
Třeba	třeba	k8wRxS	třeba
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
před	před	k7c7	před
dvaceti	dvacet	k4xCc7	dvacet
lety	let	k1gInPc7	let
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
spoustu	spoustu	k6eAd1	spoustu
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zkomplikoval	zkomplikovat	k5eAaPmAgMnS	zkomplikovat
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
za	za	k7c4	za
dílo	dílo	k1gNnSc4	dílo
malíře	malíř	k1gMnSc2	malíř
Andreje	Andrej	k1gMnSc2	Andrej
Bělocvětova	Bělocvětův	k2eAgMnSc2d1	Bělocvětův
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Jeho	jeho	k3xOp3gFnSc1	jeho
paní	paní	k1gFnSc1	paní
dělala	dělat	k5eAaImAgFnS	dělat
provozní	provozní	k2eAgNnPc1d1	provozní
v	v	k7c6	v
Dětském	dětský	k2eAgInSc6d1	dětský
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsem	být	k5eAaImIp1nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
dělnickém	dělnický	k2eAgNnSc6d1	dělnické
období	období	k1gNnSc6	období
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
dělal	dělat	k5eAaImAgMnS	dělat
uklízeče	uklízeč	k1gMnSc4	uklízeč
<g/>
.	.	kIx.	.
</s>
<s>
Mně	já	k3xPp1nSc3	já
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
dílo	dílo	k1gNnSc1	dílo
nejdřív	dříve	k6eAd3	dříve
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
čase	čas	k1gInSc6	čas
jsem	být	k5eAaImIp1nS	být
pocítil	pocítit	k5eAaPmAgMnS	pocítit
potřebu	potřeba	k1gFnSc4	potřeba
ho	on	k3xPp3gMnSc4	on
zase	zase	k9	zase
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsme	být	k5eAaImIp1nP	být
byli	být	k5eAaImAgMnP	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
intenzivním	intenzivní	k2eAgInSc6d1	intenzivní
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normalizace	normalizace	k1gFnSc2	normalizace
žil	žít	k5eAaImAgMnS	žít
téměř	téměř	k6eAd1	téměř
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
žižkovském	žižkovský	k2eAgInSc6d1	žižkovský
bytě	byt	k1gInSc6	byt
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
nevycházel	vycházet	k5eNaImAgMnS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k6eAd1	až
zlým	zlý	k2eAgInSc7d1	zlý
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
mnohé	mnohý	k2eAgInPc4d1	mnohý
jeho	jeho	k3xOp3gInPc4	jeho
obrazy	obraz	k1gInPc4	obraz
vysoce	vysoce	k6eAd1	vysoce
poetické	poetický	k2eAgInPc4d1	poetický
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
poetika	poetika	k1gFnSc1	poetika
smířlivá	smířlivý	k2eAgFnSc1d1	smířlivá
či	či	k8xC	či
sentimentální	sentimentální	k2eAgFnSc1d1	sentimentální
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
vymykal	vymykat	k5eAaImAgMnS	vymykat
kontextu	kontext	k1gInSc2	kontext
jistého	jistý	k2eAgInSc2d1	jistý
intimismu	intimismus	k1gInSc2	intimismus
českého	český	k2eAgNnSc2d1	české
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
solitérství	solitérství	k1gNnPc1	solitérství
mu	on	k3xPp3gMnSc3	on
znemožňovalo	znemožňovat	k5eAaImAgNnS	znemožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
konfrontoval	konfrontovat	k5eAaBmAgInS	konfrontovat
s	s	k7c7	s
historiky	historik	k1gMnPc7	historik
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
blízké	blízký	k2eAgNnSc1d1	blízké
mému	můj	k3xOp1gInSc3	můj
způsobu	způsob	k1gInSc3	způsob
nazírání	nazírání	k1gNnSc2	nazírání
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mi	já	k3xPp1nSc3	já
okamžitě	okamžitě	k6eAd1	okamžitě
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
to	ten	k3xDgNnSc4	ten
i	i	k9	i
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
taky	taky	k6eAd1	taky
hned	hned	k6eAd1	hned
zkraje	zkraje	k7c2	zkraje
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
obrazy	obraz	k1gInPc1	obraz
Andreje	Andrej	k1gMnSc2	Andrej
Bělocvětova	Bělocvětův	k2eAgMnSc2d1	Bělocvětův
vystavil	vystavit	k5eAaPmAgMnS	vystavit
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
nějaká	nějaký	k3yIgNnPc4	nějaký
díla	dílo	k1gNnPc4	dílo
dokoupit	dokoupit	k5eAaPmF	dokoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
to	ten	k3xDgNnSc1	ten
takovou	takový	k3xDgFnSc4	takový
nevůli	nevůle	k1gFnSc4	nevůle
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
NG	NG	kA	NG
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
musel	muset	k5eAaImAgMnS	muset
couvnout	couvnout	k5eAaPmF	couvnout
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
dosud	dosud	k6eAd1	dosud
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
75	[number]	k4	75
samostatných	samostatný	k2eAgFnPc6d1	samostatná
výstavách	výstava	k1gFnPc6	výstava
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
55	[number]	k4	55
kolektivních	kolektivní	k2eAgFnPc2d1	kolektivní
přehlídek	přehlídka	k1gFnPc2	přehlídka
<g/>
,	,	kIx,	,
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
soutěží	soutěž	k1gFnPc2	soutěž
(	(	kIx(	(
<g/>
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zpravidla	zpravidla	k6eAd1	zpravidla
neoficiálních	oficiální	k2eNgInPc2d1	neoficiální
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
Sušici	Sušice	k1gFnSc6	Sušice
<g/>
,	,	kIx,	,
Nymburku	Nymburk	k1gInSc6	Nymburk
<g/>
,	,	kIx,	,
Dobříši	Dobříš	k1gFnSc6	Dobříš
<g/>
,	,	kIx,	,
Kolíně	Kolín	k1gInSc6	Kolín
<g/>
,	,	kIx,	,
Valticích	Valtice	k1gFnPc6	Valtice
atd.	atd.	kA	atd.
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
zastoupena	zastoupen	k2eAgNnPc1d1	zastoupeno
v	v	k7c6	v
galeriích	galerie	k1gFnPc6	galerie
a	a	k8xC	a
soukromých	soukromý	k2eAgFnPc6d1	soukromá
sbírkách	sbírka	k1gFnPc6	sbírka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
samizdatovou	samizdatový	k2eAgFnSc4d1	samizdatová
sbírku	sbírka	k1gFnSc4	sbírka
Jaroslavy	Jaroslava	k1gFnSc2	Jaroslava
Šálkové	Šálkové	k2eAgNnSc4d1	Šálkové
zvané	zvaný	k2eAgNnSc4d1	zvané
Kavče	kavče	k1gNnSc4	kavče
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
Dobře	dobře	k6eAd1	dobře
domluvený	domluvený	k2eAgInSc4d1	domluvený
vandr	vandr	k1gInSc4	vandr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ilustroval	ilustrovat	k5eAaBmAgInS	ilustrovat
i	i	k9	i
román	román	k1gInSc1	román
Rostislava	Rostislav	k1gMnSc2	Rostislav
Křivánka	Křivánek	k1gMnSc2	Křivánek
Země	zem	k1gFnSc2	zem
strašidel	strašidlo	k1gNnPc2	strašidlo
(	(	kIx(	(
<g/>
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Barrister	Barristra	k1gFnPc2	Barristra
&	&	k?	&
Principal	Principal	k1gFnPc2	Principal
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc1	kresba
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Futurum	futurum	k1gNnSc1	futurum
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
Obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc1	kresba
<g/>
;	;	kIx,	;
Klatovy	Klatovy	k1gInPc1	Klatovy
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Klatovy	Klatovy	k1gInPc4	Klatovy
<g/>
,1985	,1985	k4	,1985
Obrazy	obraz	k1gInPc4	obraz
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Stuart	Stuart	k1gInSc1	Stuart
Lewy	Lew	k2eAgInPc1d1	Lew
Gallery	Galler	k1gInPc1	Galler
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Obrazy	obraz	k1gInPc7	obraz
<g/>
;	;	kIx,	;
Lulea	Luleus	k1gMnSc2	Luleus
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
Přípravné	přípravný	k2eAgNnSc4d1	přípravné
období	období	k1gNnSc4	období
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
ÚLUV	ÚLUV	kA	ÚLUV
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
Obrazy	obraz	k1gInPc7	obraz
<g/>
;	;	kIx,	;
Most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnSc1d1	státní
galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Obrazy	obraz	k1gInPc4	obraz
<g/>
;	;	kIx,	;
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Emila	Emil	k1gMnSc2	Emil
Filly	Filla	k1gMnSc2	Filla
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Znamení	znamení	k1gNnSc2	znamení
zóny	zóna	k1gFnSc2	zóna
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Lichtenštejnský	lichtenštejnský	k2eAgInSc1d1	lichtenštejnský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
Znamení	znamení	k1gNnSc2	znamení
zóny	zóna	k1gFnSc2	zóna
<g/>
;	;	kIx,	;
Rychnov	Rychnov	k1gInSc1	Rychnov
n	n	k0	n
<g/>
/	/	kIx~	/
<g/>
Kn	Kn	k1gFnSc1	Kn
<g/>
,	,	kIx,	,
Orlická	orlický	k2eAgFnSc1d1	Orlická
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Variace	variace	k1gFnSc1	variace
o	o	k7c6	o
portrétu	portrét	k1gInSc6	portrét
Antona	Anton	k1gMnSc2	Anton
Brucknera	Bruckner	k1gMnSc2	Bruckner
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Atrium	atrium	k1gNnSc1	atrium
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Obrazy	obraz	k1gInPc7	obraz
<g/>
;	;	kIx,	;
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2000	[number]	k4	2000
Znamení	znamení	k1gNnSc2	znamení
zóny	zóna	k1gFnSc2	zóna
(	(	kIx(	(
<g/>
Im	Im	k1gFnSc1	Im
Zeichen	Zeichna	k1gFnPc2	Zeichna
der	drát	k5eAaImRp2nS	drát
Zone	Zone	k1gNnSc4	Zone
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Berlin	berlina	k1gFnPc2	berlina
<g/>
,	,	kIx,	,
Pussy	Pussa	k1gFnSc2	Pussa
Galore	Galor	k1gInSc5	Galor
Art	Art	k1gMnSc2	Art
Gallery	Galler	k1gInPc4	Galler
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
K	k	k7c3	k
horám	hora	k1gFnPc3	hora
<g/>
,	,	kIx,	,
k	k	k7c3	k
lesům	les	k1gInPc3	les
<g/>
;	;	kIx,	;
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
Suvenýr	suvenýr	k1gInSc1	suvenýr
(	(	kIx(	(
<g/>
akvarely	akvarel	k1gInPc1	akvarel
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Primavesi	Primavese	k1gFnSc4	Primavese
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Líbánky	líbánky	k1gInPc7	líbánky
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
;	;	kIx,	;
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
Kroměřížska	Kroměřížsk	k1gInSc2	Kroměřížsk
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Podzim	podzim	k1gInSc1	podzim
v	v	k7c4	v
Louvre	Louvre	k1gInSc4	Louvre
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Louvre	Louvre	k1gInSc1	Louvre
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
Grotesky	groteska	k1gFnPc1	groteska
<g/>
;	;	kIx,	;
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
Sokolská	sokolská	k1gFnSc1	sokolská
26	[number]	k4	26
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
Doteky	dotek	k1gInPc1	dotek
krajiny	krajina	k1gFnSc2	krajina
<g/>
;	;	kIx,	;
Zámek	zámek	k1gInSc1	zámek
<g />
.	.	kIx.	.
</s>
<s>
Chropyně	Chropyně	k1gFnSc1	Chropyně
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
Kroměřížska	Kroměřížsk	k1gInSc2	Kroměřížsk
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
V	v	k7c4	v
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Art	Art	k1gFnSc1	Art
Factory	Factor	k1gInPc4	Factor
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s.	s.	k?	s.
I.	I.	kA	I.
<g/>
Saudkovou	Saudkový	k2eAgFnSc4d1	Saudková
<g/>
)	)	kIx)	)
Cestovní	cestovní	k2eAgInPc1d1	cestovní
deníky	deník	k1gInPc1	deník
<g/>
;	;	kIx,	;
Osek	Osek	k1gInSc1	Osek
<g/>
,	,	kIx,	,
Osecký	osecký	k2eAgInSc1d1	osecký
klášter	klášter	k1gInSc1	klášter
-	-	kIx~	-
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
Madona	Madona	k1gFnSc1	Madona
nesená	nesený	k2eAgFnSc1d1	nesená
mravenci	mravenec	k1gMnPc7	mravenec
<g/>
;	;	kIx,	;
Škrle	Škrle	k1gNnPc4	Škrle
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Písně	píseň	k1gFnPc1	píseň
potulného	potulný	k2eAgMnSc2d1	potulný
tovaryše	tovaryš	k1gMnSc2	tovaryš
(	(	kIx(	(
<g/>
akvarely	akvarel	k1gInPc1	akvarel
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
Gustava	Gustav	k1gMnSc2	Gustav
Mahlera	Mahler	k1gMnSc2	Mahler
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Vánoce	Vánoce	k1gFnPc1	Vánoce
na	na	k7c6	na
Psí	psí	k2eAgFnSc6d1	psí
řece	řeka	k1gFnSc6	řeka
aneb	aneb	k?	aneb
Něco	něco	k6eAd1	něco
k	k	k7c3	k
snědku	snědek	k1gInSc3	snědek
<g/>
;	;	kIx,	;
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
Špejchar	špejchar	k1gInSc1	špejchar
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Caprichos	Caprichos	k1gInSc1	Caprichos
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s>
9	[number]	k4	9
<g/>
;	;	kIx,	;
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Karlovarské	karlovarský	k2eAgNnSc1d1	Karlovarské
krajské	krajský	k2eAgNnSc1d1	krajské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgFnSc1d1	oficiální
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
akce	akce	k1gFnSc1	akce
44	[number]	k4	44
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
filmového	filmový	k2eAgInSc2d1	filmový
festivalu	festival	k1gInSc2	festival
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
<g/>
)	)	kIx)	)
Caprichos	Caprichos	k1gInSc1	Caprichos
009	[number]	k4	009
(	(	kIx(	(
<g/>
Obrazy	obraz	k1gInPc7	obraz
2004	[number]	k4	2004
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Vrčeň	Vrčeň	k1gFnSc1	Vrčeň
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Stodola	stodola	k1gFnSc1	stodola
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
Caprichos	Caprichos	k1gInSc1	Caprichos
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Nostický	nostický	k2eAgInSc1d1	nostický
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Doba	doba	k1gFnSc1	doba
osvitu	osvit	k1gInSc2	osvit
<g/>
;	;	kIx,	;
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Doba	doba	k1gFnSc1	doba
osvitu	osvit	k1gInSc2	osvit
<g/>
;	;	kIx,	;
Příbram	Příbram	k1gFnSc1	Příbram
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Františka	Františka	k1gFnSc1	Františka
Drtikola	Drtikola	k1gFnSc1	Drtikola
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Obrazy	obraz	k1gInPc7	obraz
2013	[number]	k4	2013
<g/>
;	;	kIx,	;
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
,	,	kIx,	,
Dusíkovo	Dusíkův	k2eAgNnSc1d1	Dusíkovo
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
Můj	můj	k3xOp1gInSc1	můj
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
můj	můj	k3xOp1gInSc1	můj
hrad	hrad	k1gInSc1	hrad
<g/>
;	;	kIx,	;
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgNnSc1d1	jižní
křídlo	křídlo	k1gNnSc1	křídlo
hradu	hrad	k1gInSc2	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
a	a	k8xC	a
barokní	barokní	k2eAgFnSc1d1	barokní
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
I.	I.	kA	I.
Saudkovou	Saudkový	k2eAgFnSc4d1	Saudková
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
má	mít	k5eAaImIp3nS	mít
potetovanou	potetovaný	k2eAgFnSc4d1	potetovaná
přibližně	přibližně	k6eAd1	přibližně
polovinu	polovina	k1gFnSc4	polovina
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
ušních	ušní	k2eAgInPc2d1	ušní
boltců	boltec	k1gInPc2	boltec
<g/>
,	,	kIx,	,
očních	oční	k2eAgNnPc2d1	oční
víček	víčko	k1gNnPc2	víčko
či	či	k8xC	či
rtů	ret	k1gInPc2	ret
<g/>
;	;	kIx,	;
barevnými	barevný	k2eAgInPc7d1	barevný
pigmenty	pigment	k1gInPc7	pigment
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
tetovanou	tetovaný	k2eAgFnSc4d1	tetovaná
pouze	pouze	k6eAd1	pouze
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
paže	paže	k1gFnPc4	paže
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c4	po
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
také	také	k6eAd1	také
hojným	hojný	k2eAgInSc7d1	hojný
piercingem	piercing	k1gInSc7	piercing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2004	[number]	k4	2004
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Orpheus	Orpheus	k1gInSc1	Orpheus
knihu	kniha	k1gFnSc4	kniha
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
<g/>
:	:	kIx,	:
Rozhovory	rozhovor	k1gInPc1	rozhovor
překladatele	překladatel	k1gMnSc2	překladatel
<g/>
,	,	kIx,	,
filosofa	filosof	k1gMnSc2	filosof
a	a	k8xC	a
kunsthistorika	kunsthistorik	k1gMnSc2	kunsthistorik
Petra	Petr	k1gMnSc2	Petr
Kurky	Kurka	k1gMnSc2	Kurka
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
členěna	členit	k5eAaImNgFnS	členit
na	na	k7c4	na
kapitoly	kapitola	k1gFnPc4	kapitola
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
a	a	k8xC	a
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
barevnou	barevný	k2eAgFnSc4d1	barevná
přílohu	příloha	k1gFnSc4	příloha
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
kompletní	kompletní	k2eAgInSc4d1	kompletní
soupis	soupis	k1gInSc4	soupis
Franzova	Franzův	k2eAgNnSc2d1	Franzovo
díla	dílo	k1gNnSc2	dílo
<g/>
;	;	kIx,	;
součástí	součást	k1gFnSc7	součást
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
i	i	k9	i
podrobný	podrobný	k2eAgInSc1d1	podrobný
rejstřík	rejstřík	k1gInSc1	rejstřík
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Kniha	kniha	k1gFnSc1	kniha
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
první	první	k4xOgFnSc4	první
z	z	k7c2	z
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
edice	edice	k1gFnSc2	edice
rozhovorů	rozhovor	k1gInPc2	rozhovor
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Orfeus	Orfeus	k1gMnSc1	Orfeus
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Petr	Petr	k1gMnSc1	Petr
Kurka	Kurka	k1gMnSc1	Kurka
zemřel	zemřít	k5eAaPmAgMnS	zemřít
uprostřed	uprostřed	k7c2	uprostřed
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
knize	kniha	k1gFnSc6	kniha
rozhovorů	rozhovor	k1gInPc2	rozhovor
se	s	k7c7	s
sochařem	sochař	k1gMnSc7	sochař
Stanislavem	Stanislav	k1gMnSc7	Stanislav
Hanzíkem	Hanzík	k1gMnSc7	Hanzík
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
a	a	k8xC	a
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gInSc2	jeho
programu	program	k1gInSc2	program
převzalo	převzít	k5eAaPmAgNnS	převzít
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Casablanca	Casablanca	k1gFnSc1	Casablanca
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
některých	některý	k3yIgNnPc2	některý
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
dokumentů	dokument	k1gInPc2	dokument
účastnil	účastnit	k5eAaImAgMnS	účastnit
vznikající	vznikající	k2eAgFnSc2d1	vznikající
skinheadské	skinheadský	k2eAgFnSc2d1	skinheadská
subkultury	subkultura	k1gFnSc2	subkultura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
snažil	snažit	k5eAaImAgMnS	snažit
dodat	dodat	k5eAaPmF	dodat
určitý	určitý	k2eAgInSc4d1	určitý
filozofický	filozofický	k2eAgInSc4d1	filozofický
a	a	k8xC	a
morální	morální	k2eAgInSc4d1	morální
rozměr	rozměr	k1gInSc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
záměr	záměr	k1gInSc1	záměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
realizovat	realizovat	k5eAaBmF	realizovat
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
začínajícím	začínající	k2eAgMnSc7d1	začínající
novinářem	novinář	k1gMnSc7	novinář
<g/>
,	,	kIx,	,
psychologem	psycholog	k1gMnSc7	psycholog
Jiřím	Jiří	k1gMnSc7	Jiří
X.	X.	kA	X.
Doležalem	Doležal	k1gMnSc7	Doležal
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nezdařil	zdařit	k5eNaPmAgInS	zdařit
a	a	k8xC	a
nad	nad	k7c7	nad
následky	následek	k1gInPc7	následek
svého	svůj	k3xOyFgNnSc2	svůj
počínání	počínání	k1gNnSc2	počínání
údajně	údajně	k6eAd1	údajně
ztratil	ztratit	k5eAaPmAgInS	ztratit
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
samotného	samotný	k2eAgMnSc2d1	samotný
J.	J.	kA	J.
X.	X.	kA	X.
Doležala	Doležal	k1gMnSc2	Doležal
byli	být	k5eAaImAgMnP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Franzem	Franz	k1gInSc7	Franz
opakovaně	opakovaně	k6eAd1	opakovaně
od	od	k7c2	od
skinheadů	skinhead	k1gMnPc2	skinhead
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
období	období	k1gNnSc6	období
zbiti	zbít	k5eAaPmNgMnP	zbít
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
J.	J.	kA	J.
X.	X.	kA	X.
Doležala	Doležal	k1gMnSc2	Doležal
se	se	k3xPyFc4	se
doktor	doktor	k1gMnSc1	doktor
Franz	Franz	k1gMnSc1	Franz
společně	společně	k6eAd1	společně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
stát	stát	k5eAaImF	stát
ideovým	ideový	k2eAgMnSc7d1	ideový
vůdcem	vůdce	k1gMnSc7	vůdce
českých	český	k2eAgMnPc2d1	český
skinů	skin	k1gMnPc2	skin
a	a	k8xC	a
naučit	naučit	k5eAaPmF	naučit
je	být	k5eAaImIp3nS	být
Dobru	dobro	k1gNnSc3	dobro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Aspiroval	aspirovat	k5eAaImAgMnS	aspirovat
na	na	k7c4	na
ideologa	ideolog	k1gMnSc4	ideolog
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgInS	chtít
skiny	skin	k1gMnPc4	skin
zklidnit	zklidnit	k5eAaPmF	zklidnit
a	a	k8xC	a
deproblematizovat	deproblematizovat	k5eAaBmF	deproblematizovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neměl	mít	k5eNaImAgMnS	mít
podle	podle	k7c2	podle
Doležala	Doležal	k1gMnSc2	Doležal
ani	ani	k8xC	ani
nejmenší	malý	k2eAgFnPc4d3	nejmenší
sympatie	sympatie	k1gFnPc4	sympatie
k	k	k7c3	k
nacismu	nacismus	k1gInSc3	nacismus
a	a	k8xC	a
neonacismu	neonacismus	k1gInSc3	neonacismus
<g/>
.	.	kIx.	.
</s>
<s>
Doležal	Doležal	k1gMnSc1	Doležal
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
společně	společně	k6eAd1	společně
s	s	k7c7	s
Franzem	Franz	k1gInSc7	Franz
vypracovali	vypracovat	k5eAaPmAgMnP	vypracovat
koncept	koncept	k1gInSc4	koncept
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgMnSc4	který
skinheadi	skinhead	k1gMnPc1	skinhead
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
přijímáni	přijímat	k5eAaImNgMnP	přijímat
do	do	k7c2	do
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
řízených	řízený	k2eAgInPc2d1	řízený
oddílů	oddíl	k1gInPc2	oddíl
domobrany	domobrana	k1gFnSc2	domobrana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
naučili	naučit	k5eAaPmAgMnP	naučit
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
správným	správný	k2eAgMnPc3d1	správný
stravovacím	stravovací	k2eAgMnPc3d1	stravovací
návykům	návyk	k1gInPc3	návyk
<g/>
,	,	kIx,	,
úctě	úcta	k1gFnSc6	úcta
k	k	k7c3	k
právu	právo	k1gNnSc3	právo
<g/>
,	,	kIx,	,
jenomže	jenomže	k8xC	jenomže
po	po	k7c6	po
vystřízlivění	vystřízlivění	k1gNnSc6	vystřízlivění
oba	dva	k4xCgMnPc4	dva
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
skini	skin	k1gMnPc1	skin
jsou	být	k5eAaImIp3nP	být
nesocializovatelní	socializovatelný	k2eNgMnPc1d1	socializovatelný
sociopati	sociopat	k5eAaPmF	sociopat
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgFnPc7	který
"	"	kIx"	"
<g/>
nejde	jít	k5eNaImIp3nS	jít
dělat	dělat	k5eAaImF	dělat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
pozavírat	pozavírat	k5eAaPmF	pozavírat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
Franz	Franz	k1gMnSc1	Franz
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
Orlík	Orlík	k1gInSc1	Orlík
a	a	k8xC	a
Braník	Braník	k1gInSc1	Braník
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Tichý	Tichý	k1gMnSc1	Tichý
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
hotel	hotel	k1gInSc1	hotel
Ariston	ariston	k1gInSc1	ariston
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
s	s	k7c7	s
projevem	projev	k1gInSc7	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
oznámil	oznámit	k5eAaPmAgInS	oznámit
dřívější	dřívější	k2eAgInSc1d1	dřívější
vznik	vznik	k1gInSc1	vznik
názvu	název	k1gInSc2	název
Nová	nový	k2eAgFnSc1d1	nová
česká	český	k2eAgFnSc1d1	Česká
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Na	na	k7c6	na
Chmelnici	chmelnice	k1gFnSc6	chmelnice
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
přítomné	přítomný	k1gMnPc4	přítomný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
vybrali	vybrat	k5eAaPmAgMnP	vybrat
organizačně	organizačně	k6eAd1	organizačně
zdatné	zdatný	k2eAgMnPc4d1	zdatný
jedince	jedinec	k1gMnPc4	jedinec
a	a	k8xC	a
svolal	svolat	k5eAaPmAgMnS	svolat
první	první	k4xOgInSc4	první
sraz	sraz	k1gInSc4	sraz
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zakončil	zakončit	k5eAaPmAgInS	zakončit
zvoláním	zvolání	k1gNnSc7	zvolání
Oi	Oi	k1gFnSc2	Oi
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
první	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
skinheadského	skinheadský	k2eAgInSc2d1	skinheadský
časopisu	časopis	k1gInSc2	časopis
(	(	kIx(	(
<g/>
zinu	zinus	k1gInSc2	zinus
<g/>
)	)	kIx)	)
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Organizátor	organizátor	k1gInSc1	organizátor
koncertu	koncert	k1gInSc2	koncert
však	však	k9	však
vzápětí	vzápětí	k6eAd1	vzápětí
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Franze	Franze	k1gFnSc2	Franze
osočil	osočit	k5eAaPmAgMnS	osočit
za	za	k7c4	za
údajné	údajný	k2eAgNnSc4d1	údajné
"	"	kIx"	"
<g/>
rozeštvávání	rozeštvávání	k1gNnSc4	rozeštvávání
<g/>
"	"	kIx"	"
a	a	k8xC	a
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sraz	sraz	k1gInSc1	sraz
skinheadů	skinhead	k1gMnPc2	skinhead
měl	mít	k5eAaImAgInS	mít
oznámit	oznámit	k5eAaPmF	oznámit
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
zinu	zinu	k5eAaPmIp1nS	zinu
Čech	Čech	k1gMnSc1	Čech
následně	následně	k6eAd1	následně
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Franzem	Franz	k1gMnSc7	Franz
svolaný	svolaný	k2eAgInSc4d1	svolaný
sraz	sraz	k1gInSc4	sraz
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
projevu	projev	k1gInSc2	projev
i	i	k8xC	i
celého	celý	k2eAgInSc2d1	celý
koncertu	koncert	k1gInSc2	koncert
koluje	kolovat	k5eAaImIp3nS	kolovat
po	po	k7c6	po
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
JUDr.	JUDr.	kA	JUDr.
Franze	Franze	k1gFnSc1	Franze
byla	být	k5eAaImAgFnS	být
motivem	motiv	k1gInSc7	motiv
titulní	titulní	k2eAgFnPc4d1	titulní
strany	strana	k1gFnPc4	strana
prvního	první	k4xOgNnSc2	první
čísla	číslo	k1gNnSc2	číslo
časopisu	časopis	k1gInSc2	časopis
(	(	kIx(	(
<g/>
zinu	zinus	k1gInSc2	zinus
<g/>
)	)	kIx)	)
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
zin	zin	k?	zin
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
údajně	údajně	k6eAd1	údajně
sepsal	sepsat	k5eAaPmAgMnS	sepsat
několik	několik	k4yIc4	několik
programových	programový	k2eAgInPc2d1	programový
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
například	například	k6eAd1	například
vytyčil	vytyčit	k5eAaPmAgMnS	vytyčit
ideu	idea	k1gFnSc4	idea
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
drogám	droga	k1gFnPc3	droga
<g/>
,	,	kIx,	,
posilování	posilování	k1gNnSc6	posilování
fyzické	fyzický	k2eAgFnSc2d1	fyzická
kondice	kondice	k1gFnSc2	kondice
nebo	nebo	k8xC	nebo
pozitivního	pozitivní	k2eAgNnSc2d1	pozitivní
rozvíjení	rozvíjení	k1gNnSc2	rozvíjení
vlastenectví	vlastenectví	k1gNnSc2	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
od	od	k7c2	od
výsledné	výsledný	k2eAgFnSc2d1	výsledná
podoby	podoba	k1gFnSc2	podoba
zinu	zinu	k6eAd1	zinu
i	i	k9	i
jeho	jeho	k3xOp3gMnPc2	jeho
autorů	autor	k1gMnPc2	autor
opakovaně	opakovaně	k6eAd1	opakovaně
veřejně	veřejně	k6eAd1	veřejně
distancoval	distancovat	k5eAaBmAgMnS	distancovat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
několik	několik	k4yIc1	několik
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgFnP	zabývat
vztahem	vztah	k1gInSc7	vztah
JUDr.	JUDr.	kA	JUDr.
Franze	Franza	k1gFnSc3	Franza
ke	k	k7c3	k
skinheadské	skinheadský	k2eAgFnSc3d1	skinheadská
subkultuře	subkultura	k1gFnSc3	subkultura
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dehonestačního	dehonestační	k2eAgInSc2d1	dehonestační
textu	text	k1gInSc2	text
Davida	David	k1gMnSc2	David
Vondráčka	Vondráček	k1gMnSc2	Vondráček
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Mladý	mladý	k2eAgInSc1d1	mladý
svět	svět	k1gInSc1	svět
se	se	k3xPyFc4	se
jistý	jistý	k2eAgMnSc1d1	jistý
mladý	mladý	k2eAgMnSc1d1	mladý
skinhead	skinhead	k1gMnSc1	skinhead
Dan	Dan	k1gMnSc1	Dan
Reilich	Reilich	k1gMnSc1	Reilich
distancoval	distancovat	k5eAaBmAgInS	distancovat
od	od	k7c2	od
"	"	kIx"	"
<g/>
mladých	mladý	k2eAgMnPc2d1	mladý
vypatlanců	vypatlanec	k1gMnPc2	vypatlanec
a	a	k8xC	a
JUDr.	JUDr.	kA	JUDr.
France	Franc	k1gMnSc2	Franc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
údajně	údajně	k6eAd1	údajně
honí	honit	k5eAaImIp3nP	honit
rákosníky	rákosník	k1gInPc1	rákosník
po	po	k7c6	po
Václaváku	Václavák	k1gInSc6	Václavák
a	a	k8xC	a
"	"	kIx"	"
<g/>
mají	mít	k5eAaImIp3nP	mít
ukrutánskou	ukrutánský	k2eAgFnSc4d1	ukrutánská
srandu	sranda	k1gFnSc4	sranda
<g/>
,	,	kIx,	,
když	když	k8xS	když
nějakému	nějaký	k3yIgInSc3	nějaký
rákosu	rákos	k1gInSc3	rákos
zpřelámou	zpřelámat	k5eAaPmIp3nP	zpřelámat
žebra	žebro	k1gNnPc4	žebro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
citace	citace	k1gFnSc2	citace
tohoto	tento	k3xDgMnSc2	tento
Dana	Dan	k1gMnSc2	Dan
v	v	k7c6	v
Mladém	mladý	k2eAgInSc6d1	mladý
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Franc	Franc	k1gMnSc1	Franc
rozumnej	rozumnej	k?	rozumnej
chlap	chlap	k1gMnSc1	chlap
a	a	k8xC	a
inteligent	inteligent	k1gMnSc1	inteligent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
ujetý	ujetý	k2eAgInSc1d1	ujetý
potěšení	potěšení	k1gNnSc2	potěšení
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
někdo	někdo	k3yInSc1	někdo
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
mlaďochů	mlaďoch	k1gMnPc2	mlaďoch
bere	brát	k5eAaImIp3nS	brát
a	a	k8xC	a
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
může	moct	k5eAaImIp3nS	moct
ovládat	ovládat	k5eAaImF	ovládat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
Dan	Dan	k1gMnSc1	Dan
ztotožňoval	ztotožňovat	k5eAaImAgMnS	ztotožňovat
s	s	k7c7	s
údajným	údajný	k2eAgInSc7d1	údajný
Franzovým	Franzův	k2eAgInSc7d1	Franzův
názorem	názor	k1gInSc7	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
čeští	český	k2eAgMnPc1d1	český
skini	skin	k1gMnPc1	skin
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
něco	něco	k6eAd1	něco
jako	jako	k8xC	jako
policajti	policajti	k?	policajti
pochůzkáři	pochůzkář	k1gMnPc1	pochůzkář
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
ti	ten	k3xDgMnPc1	ten
kriminálníci	kriminálník	k1gMnPc1	kriminálník
a	a	k8xC	a
šmelináři	šmelinář	k1gMnPc1	šmelinář
bát	bát	k5eAaImF	bát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
článek	článek	k1gInSc4	článek
v	v	k7c6	v
Mladém	mladý	k2eAgInSc6d1	mladý
světě	svět	k1gInSc6	svět
zareagoval	zareagovat	k5eAaPmAgInS	zareagovat
dopisem	dopis	k1gInSc7	dopis
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgNnSc2	jenž
tento	tento	k3xDgInSc4	tento
týdeník	týdeník	k1gInSc4	týdeník
publikoval	publikovat	k5eAaBmAgMnS	publikovat
několik	několik	k4yIc4	několik
upravených	upravený	k2eAgInPc2d1	upravený
výňatků	výňatek	k1gInPc2	výňatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
stát	stát	k5eAaImF	stát
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
problematiky	problematika	k1gFnSc2	problematika
problémové	problémový	k2eAgFnSc2d1	problémová
mládeže	mládež	k1gFnSc2	mládež
<g/>
,	,	kIx,	,
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
své	svůj	k3xOyFgFnPc4	svůj
pohnutky	pohnutka	k1gFnPc4	pohnutka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jej	on	k3xPp3gMnSc4	on
vedly	vést	k5eAaImAgFnP	vést
do	do	k7c2	do
daného	daný	k2eAgNnSc2d1	dané
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
distancoval	distancovat	k5eAaBmAgInS	distancovat
se	se	k3xPyFc4	se
od	od	k7c2	od
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
násilných	násilný	k2eAgFnPc2d1	násilná
akcí	akce	k1gFnPc2	akce
i	i	k8xC	i
materiálů	materiál	k1gInPc2	materiál
publikovaných	publikovaný	k2eAgInPc2d1	publikovaný
zinem	zinem	k6eAd1	zinem
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc7	jeho
podobou	podoba	k1gFnSc7	podoba
zděšen	zděsit	k5eAaPmNgMnS	zděsit
a	a	k8xC	a
že	že	k9	že
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gFnPc1	jeho
myšlenky	myšlenka	k1gFnPc1	myšlenka
redaktorem	redaktor	k1gMnSc7	redaktor
zinu	zinus	k1gInSc6	zinus
zneužity	zneužit	k2eAgInPc1d1	zneužit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
zveřejněných	zveřejněný	k2eAgFnPc2d1	zveřejněná
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
na	na	k7c6	na
facebookovém	facebookový	k2eAgInSc6d1	facebookový
profilu	profil	k1gInSc6	profil
iniciativy	iniciativa	k1gFnSc2	iniciativa
"	"	kIx"	"
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
prezidentem	prezident	k1gMnSc7	prezident
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
David	David	k1gMnSc1	David
Vondráček	Vondráček	k1gMnSc1	Vondráček
po	po	k7c6	po
22	[number]	k4	22
letech	léto	k1gNnPc6	léto
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
text	text	k1gInSc4	text
v	v	k7c6	v
Mladém	mladý	k2eAgInSc6d1	mladý
světě	svět	k1gInSc6	svět
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
redaktorkou	redaktorka	k1gFnSc7	redaktorka
časopisu	časopis	k1gInSc2	časopis
Vlasty	Vlasta	k1gFnSc2	Vlasta
Marií	Maria	k1gFnPc2	Maria
Homolkovou	Homolková	k1gFnSc7	Homolková
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Franz	Franza	k1gFnPc2	Franza
sice	sice	k8xC	sice
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
typický	typický	k2eAgMnSc1d1	typický
skinhead	skinhead	k1gMnSc1	skinhead
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostavil	dostavit	k5eAaPmAgMnS	dostavit
se	se	k3xPyFc4	se
s	s	k7c7	s
vlasy	vlas	k1gInPc7	vlas
ostříhanými	ostříhaný	k2eAgInPc7d1	ostříhaný
na	na	k7c4	na
milimetr	milimetr	k1gInSc4	milimetr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
botách	bota	k1gFnPc6	bota
a	a	k8xC	a
v	v	k7c6	v
maskáčích	maskáč	k1gInPc6	maskáč
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
skinheadské	skinheadský	k2eAgFnSc2d1	skinheadská
subkultury	subkultura	k1gFnSc2	subkultura
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
jako	jako	k9	jako
sociálně	sociálně	k6eAd1	sociálně
logickou	logický	k2eAgFnSc4d1	logická
reakci	reakce	k1gFnSc4	reakce
civilizované	civilizovaný	k2eAgFnSc2d1	civilizovaná
společnost	společnost	k1gFnSc1	společnost
proti	proti	k7c3	proti
agresivnímu	agresivní	k2eAgNnSc3d1	agresivní
pronikání	pronikání	k1gNnSc3	pronikání
promitivnější	promitivný	k2eAgFnSc2d2	promitivný
kultury	kultura	k1gFnSc2	kultura
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mentalitou	mentalita	k1gFnSc7	mentalita
a	a	k8xC	a
temperamentem	temperament	k1gInSc7	temperament
nejsou	být	k5eNaImIp3nP	být
schopní	schopný	k2eAgMnPc1d1	schopný
přijmout	přijmout	k5eAaPmF	přijmout
místní	místní	k2eAgFnPc4d1	místní
pravidla	pravidlo	k1gNnPc4	pravidlo
soužití	soužití	k1gNnSc2	soužití
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
svépomoc	svépomoc	k1gFnSc1	svépomoc
lidí	člověk	k1gMnPc2	člověk
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stát	stát	k1gInSc1	stát
nestačí	stačit	k5eNaBmIp3nS	stačit
zajistit	zajistit	k5eAaPmF	zajistit
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
situaci	situace	k1gFnSc3	situace
kolem	kolem	k7c2	kolem
undergroundové	undergroundový	k2eAgFnSc2d1	undergroundová
mládeže	mládež	k1gFnSc2	mládež
prý	prý	k9	prý
tehdy	tehdy	k6eAd1	tehdy
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Považoval	považovat	k5eAaImAgMnS	považovat
bych	by	kYmCp1nS	by
za	za	k7c4	za
prozíravé	prozíravý	k2eAgNnSc4d1	prozíravé
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
prezident	prezident	k1gMnSc1	prezident
Havel	Havel	k1gMnSc1	Havel
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
rozsáhlému	rozsáhlý	k2eAgInSc3d1	rozsáhlý
poradenskému	poradenský	k2eAgInSc3d1	poradenský
aparátu	aparát	k1gInSc3	aparát
přibral	přibrat	k5eAaPmAgMnS	přibrat
poradce	poradce	k1gMnSc1	poradce
pro	pro	k7c4	pro
undergroundovou	undergroundový	k2eAgFnSc4d1	undergroundová
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
dole	dole	k6eAd1	dole
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
tam	tam	k6eAd1	tam
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
a	a	k8xC	a
kam	kam	k6eAd1	kam
odvést	odvést	k5eAaPmF	odvést
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dneska	dneska	k?	dneska
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
nudě	nuda	k1gFnSc6	nuda
a	a	k8xC	a
rvačkách	rvačka	k1gFnPc6	rvačka
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vážná	vážný	k2eAgFnSc1d1	vážná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc7	on
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
zákazy	zákaz	k1gInPc1	zákaz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Od	od	k7c2	od
vlny	vlna	k1gFnSc2	vlna
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
Romy	Rom	k1gMnPc4	Rom
a	a	k8xC	a
Vietnamce	Vietnamka	k1gFnSc3	Vietnamka
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Vlasta	Vlasta	k1gMnSc1	Vlasta
distancoval	distancovat	k5eAaBmAgMnS	distancovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nejsme	být	k5eNaImIp1nP	být
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
tyhle	tenhle	k3xDgFnPc4	tenhle
bitky	bitka	k1gFnPc1	bitka
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
<g/>
,	,	kIx,	,
aspoň	aspoň	k9	aspoň
ne	ne	k9	ne
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Nechceme	chtít	k5eNaImIp1nP	chtít
násilí	násilí	k1gNnSc4	násilí
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
by	by	kYmCp3nS	by
jen	jen	k9	jen
měl	mít	k5eAaImAgInS	mít
umět	umět	k5eAaImF	umět
zdravě	zdravě	k6eAd1	zdravě
si	se	k3xPyFc3	se
prosadit	prosadit	k5eAaPmF	prosadit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
chce	chtít	k5eAaImIp3nS	chtít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
září	září	k1gNnSc6	září
1991	[number]	k4	1991
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Franz	Franz	k1gMnSc1	Franz
pro	pro	k7c4	pro
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
článek	článek	k1gInSc4	článek
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Reflex	reflex	k1gInSc1	reflex
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Každý	každý	k3xTgMnSc1	každý
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
prodělat	prodělat	k5eAaPmF	prodělat
podobné	podobný	k2eAgNnSc4d1	podobné
období	období	k1gNnSc4	období
určité	určitý	k2eAgFnSc2d1	určitá
manifestačnosti	manifestačnost	k1gFnSc2	manifestačnost
<g/>
,	,	kIx,	,
chtění	chtění	k1gNnSc1	chtění
-	-	kIx~	-
ten	ten	k3xDgInSc4	ten
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
někým	někdo	k3yInSc7	někdo
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
někam	někam	k6eAd1	někam
patřím	patřit	k5eAaImIp1nS	patřit
<g/>
,	,	kIx,	,
na	na	k7c4	na
něco	něco	k3yInSc4	něco
navazuju	navazovat	k5eAaImIp1nS	navazovat
<g/>
...	...	k?	...
Bohužel	bohužel	k9	bohužel
u	u	k7c2	u
skinheadů	skinhead	k1gMnPc2	skinhead
se	se	k3xPyFc4	se
vyjádření	vyjádření	k1gNnSc2	vyjádření
oné	onen	k3xDgFnSc2	onen
manifestačnosti	manifestačnost	k1gFnSc2	manifestačnost
většinou	většinou	k6eAd1	většinou
zploštilo	zploštit	k5eAaPmAgNnS	zploštit
na	na	k7c4	na
řev	řev	k1gInSc4	řev
Jsem	být	k5eAaImIp1nS	být
Čech	Čech	k1gMnSc1	Čech
<g/>
!	!	kIx.	!
</s>
<s>
Přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
měli	mít	k5eAaImAgMnP	mít
zmatek	zmatek	k1gInSc4	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsem	být	k5eAaImIp1nS	být
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
nějak	nějak	k6eAd1	nějak
je	být	k5eAaImIp3nS	být
zkultivovat	zkultivovat	k5eAaPmF	zkultivovat
<g/>
.	.	kIx.	.
</s>
<s>
Identifikovat	identifikovat	k5eAaBmF	identifikovat
ten	ten	k3xDgInSc4	ten
dav	dav	k1gInSc4	dav
s	s	k7c7	s
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Zajímalo	zajímat	k5eAaImAgNnS	zajímat
mne	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nS	by
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
určitého	určitý	k2eAgInSc2d1	určitý
výchovného	výchovný	k2eAgInSc2d1	výchovný
cyklu	cyklus	k1gInSc2	cyklus
-	-	kIx~	-
sebeobrana	sebeobrana	k1gFnSc1	sebeobrana
<g/>
,	,	kIx,	,
kázeň	kázeň	k1gFnSc1	kázeň
<g/>
,	,	kIx,	,
morálka	morálka	k1gFnSc1	morálka
<g/>
...	...	k?	...
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
své	svůj	k3xOyFgFnPc4	svůj
schopnosti	schopnost	k1gFnPc4	schopnost
nasměrovat	nasměrovat	k5eAaPmF	nasměrovat
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
by	by	kYmCp3nS	by
mohli	moct	k5eAaImAgMnP	moct
učinit	učinit	k5eAaImF	učinit
zásadní	zásadní	k2eAgInSc4d1	zásadní
obrat	obrat	k1gInSc4	obrat
sami	sám	k3xTgMnPc1	sám
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
-	-	kIx~	-
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
pocit	pocit	k1gInSc4	pocit
plnosti	plnost	k1gFnSc2	plnost
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
"	"	kIx"	"
<g/>
problematiku	problematika	k1gFnSc4	problematika
městské	městský	k2eAgFnSc2d1	městská
mládeže	mládež	k1gFnSc2	mládež
<g/>
"	"	kIx"	"
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
z	z	k7c2	z
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tam	tam	k6eAd1	tam
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
myšlenka	myšlenka	k1gFnSc1	myšlenka
výcviku	výcvik	k1gInSc2	výcvik
českých	český	k2eAgMnPc2d1	český
skinů	skin	k1gMnPc2	skin
<g/>
.	.	kIx.	.
</s>
<s>
Viděl	vidět	k5eAaImAgMnS	vidět
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Třeba	třeba	k9	třeba
když	když	k8xS	když
má	můj	k3xOp1gFnSc1	můj
ženská	ženská	k1gFnSc1	ženská
noční	noční	k2eAgFnSc4d1	noční
službu	služba	k1gFnSc4	služba
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
telefonem	telefon	k1gInSc7	telefon
si	se	k3xPyFc3	se
najme	najmout	k5eAaPmIp3nS	najmout
dva	dva	k4xCgMnPc4	dva
kluky	kluk	k1gMnPc4	kluk
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
ji	on	k3xPp3gFnSc4	on
doprovodí	doprovodit	k5eAaPmIp3nP	doprovodit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Dotáhnout	dotáhnout	k5eAaPmF	dotáhnout
naše	náš	k3xOp1gMnPc4	náš
skiny	skin	k1gMnPc4	skin
k	k	k7c3	k
pozitivní	pozitivní	k2eAgFnSc3d1	pozitivní
činnosti	činnost	k1gFnSc3	činnost
mohlo	moct	k5eAaImAgNnS	moct
nahradit	nahradit	k5eAaPmF	nahradit
jejich	jejich	k3xOp3gFnSc4	jejich
tradiční	tradiční	k2eAgFnSc4d1	tradiční
zamindrákovanost	zamindrákovanost	k1gFnSc4	zamindrákovanost
<g/>
,	,	kIx,	,
planý	planý	k2eAgMnSc1d1	planý
pindání	pindání	k1gNnSc4	pindání
<g/>
,	,	kIx,	,
poflakování	poflakování	k1gNnSc4	poflakování
po	po	k7c6	po
hospodách	hospodách	k?	hospodách
pocitem	pocit	k1gInSc7	pocit
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
plnosti	plnost	k1gFnSc2	plnost
<g/>
.	.	kIx.	.
</s>
<s>
Třeba	třeba	k6eAd1	třeba
karatisti	karatisti	k?	karatisti
si	se	k3xPyFc3	se
dovedli	dovést	k5eAaPmAgMnP	dovést
najít	najít	k5eAaPmF	najít
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
ochrance	ochranka	k1gFnSc6	ochranka
pana	pan	k1gMnSc2	pan
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
společnost	společnost	k1gFnSc4	společnost
je	on	k3xPp3gMnPc4	on
bere	brát	k5eAaImIp3nS	brát
jako	jako	k8xS	jako
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
faktor	faktor	k1gInSc1	faktor
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
A	a	k9	a
mimochodem	mimochodem	k9	mimochodem
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zase	zase	k9	zase
tak	tak	k6eAd1	tak
dávno	dávno	k6eAd1	dávno
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
plese	pleso	k1gNnSc6	pleso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc3	jehož
se	se	k3xPyFc4	se
pan	pan	k1gMnSc1	pan
prezident	prezident	k1gMnSc1	prezident
účastnil	účastnit	k5eAaImAgMnS	účastnit
<g/>
,	,	kIx,	,
svěřena	svěřen	k2eAgFnSc1d1	svěřena
pořadatelská	pořadatelský	k2eAgFnSc1d1	pořadatelská
služba	služba	k1gFnSc1	služba
právě	právě	k9	právě
skinům	skin	k1gMnPc3	skin
<g/>
.	.	kIx.	.
</s>
<s>
Nedostala	dostat	k5eNaPmAgFnS	dostat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
ani	ani	k8xC	ani
noha	noha	k1gFnSc1	noha
bez	bez	k7c2	bez
lístku	lístek	k1gInSc2	lístek
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
tam	tam	k6eAd1	tam
nevpustili	vpustit	k5eNaPmAgMnP	vpustit
ani	ani	k8xC	ani
svého	svůj	k3xOyFgMnSc4	svůj
kolegu	kolega	k1gMnSc4	kolega
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
kvádru	kvádr	k1gInSc6	kvádr
<g/>
.	.	kIx.	.
</s>
<s>
Škoda	škoda	k1gFnSc1	škoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
opačných	opačný	k2eAgFnPc2d1	opačná
zkušeností	zkušenost	k1gFnPc2	zkušenost
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zin	zin	k?	zin
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Franz	Franz	k1gInSc1	Franz
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
genezi	geneze	k1gFnSc3	geneze
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Napadlo	napadnout	k5eAaPmAgNnS	napadnout
mě	já	k3xPp1nSc4	já
tedy	tedy	k9	tedy
ověřit	ověřit	k5eAaPmF	ověřit
si	se	k3xPyFc3	se
možnost	možnost	k1gFnSc4	možnost
kultivace	kultivace	k1gFnSc2	kultivace
na	na	k7c4	na
tak	tak	k6eAd1	tak
banální	banální	k2eAgFnPc4d1	banální
věci	věc	k1gFnPc4	věc
jako	jako	k8xS	jako
nějaká	nějaký	k3yIgFnSc1	nějaký
tiskovina	tiskovina	k1gFnSc1	tiskovina
<g/>
.	.	kIx.	.
</s>
<s>
Totiž	totiž	k9	totiž
zejména	zejména	k9	zejména
skinheadi	skinhead	k1gMnPc1	skinhead
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
furt	furt	k?	furt
sténali	sténat	k5eAaImAgMnP	sténat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
o	o	k7c6	o
nikom	nikdo	k3yNnSc6	nikdo
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
má	mít	k5eAaImIp3nS	mít
někdo	někdo	k3yInSc1	někdo
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
bombra	bombra	k6eAd1	bombra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
jaká	jaký	k3yQgFnSc1	jaký
kapela	kapela	k1gFnSc1	kapela
<g/>
...	...	k?	...
Vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
jsem	být	k5eAaImIp1nS	být
určité	určitý	k2eAgNnSc4d1	určité
schéma	schéma	k1gNnSc4	schéma
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgMnSc4	který
mohli	moct	k5eAaImAgMnP	moct
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
servisu	servis	k1gInSc2	servis
tam	tam	k6eAd1	tam
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
články	článek	k1gInPc1	článek
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc1d1	vlastní
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
názory	názor	k1gInPc1	názor
<g/>
...	...	k?	...
Prostě	prostě	k6eAd1	prostě
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
trochu	trochu	k6eAd1	trochu
rozhejbali	rozhejbat	k5eAaImAgMnP	rozhejbat
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k6eAd1	bohužel
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
kdosi	kdosi	k3yInSc1	kdosi
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nechutný	chutný	k2eNgInSc4d1	nechutný
pamflet	pamflet	k1gInSc4	pamflet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
rozsahu	rozsah	k1gInSc2	rozsah
čtrnácti	čtrnáct	k4xCc7	čtrnáct
patnácti	patnáct	k4xCc7	patnáct
stránek	stránka	k1gFnPc2	stránka
zbyl	zbýt	k5eAaPmAgInS	zbýt
dvoustránkový	dvoustránkový	k2eAgInSc1d1	dvoustránkový
rasistický	rasistický	k2eAgInSc1d1	rasistický
letáček	letáček	k1gInSc1	letáček
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
informativní	informativní	k2eAgInSc1d1	informativní
a	a	k8xC	a
stmelující	stmelující	k2eAgInSc1d1	stmelující
smysl	smysl	k1gInSc1	smysl
byl	být	k5eAaImAgInS	být
absolutně	absolutně	k6eAd1	absolutně
popřený	popřený	k2eAgInSc1d1	popřený
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jmenovat	jmenovat	k5eAaBmF	jmenovat
Krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
oni	onen	k3xDgMnPc1	onen
zvolili	zvolit	k5eAaPmAgMnP	zvolit
název	název	k1gInSc4	název
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
titulní	titulní	k2eAgFnSc4d1	titulní
stránku	stránka	k1gFnSc4	stránka
ovšem	ovšem	k9	ovšem
použili	použít	k5eAaPmAgMnP	použít
mou	můj	k3xOp1gFnSc4	můj
fotku	fotka	k1gFnSc4	fotka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
mě	já	k3xPp1nSc4	já
uvedli	uvést	k5eAaPmAgMnP	uvést
do	do	k7c2	do
přímé	přímý	k2eAgFnSc2d1	přímá
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
věcí	věc	k1gFnSc7	věc
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
jsem	být	k5eAaImIp1nS	být
naprosto	naprosto	k6eAd1	naprosto
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Oleje	olej	k1gInPc4	olej
přilil	přilít	k5eAaPmAgInS	přilít
Mladý	mladý	k2eAgInSc1d1	mladý
svět	svět	k1gInSc1	svět
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
diletantským	diletantský	k2eAgInSc7d1	diletantský
článkem	článek	k1gInSc7	článek
pana	pan	k1gMnSc2	pan
Vondráčka	Vondráček	k1gMnSc2	Vondráček
a	a	k8xC	a
své	své	k1gNnSc4	své
si	se	k3xPyFc3	se
přisadili	přisadit	k5eAaPmAgMnP	přisadit
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
kolem	kolem	k7c2	kolem
skupiny	skupina	k1gFnSc2	skupina
Orlík	Orlík	k1gInSc1	Orlík
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oni	onen	k3xDgMnPc1	onen
si	se	k3xPyFc3	se
neobyčejně	obyčejně	k6eNd1	obyčejně
zakládali	zakládat	k5eAaImAgMnP	zakládat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
slušný	slušný	k2eAgMnSc1d1	slušný
-	-	kIx~	-
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
můžou	můžou	k?	můžou
ty	ten	k3xDgMnPc4	ten
mládenečky	mládeneček	k1gMnPc4	mládeneček
divočit	divočit	k5eAaImF	divočit
písněmi	píseň	k1gFnPc7	píseň
typu	typ	k1gInSc2	typ
́	́	k?	́
<g/>
Rozbij	rozbít	k5eAaPmRp2nS	rozbít
mu	on	k3xPp3gMnSc3	on
držku	držka	k1gFnSc4	držka
<g/>
,	,	kIx,	,
dobij	dobít	k5eAaPmRp2nS	dobít
ho	on	k3xPp3gMnSc4	on
pěstí	pěstit	k5eAaImIp3nS	pěstit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
́	́	k?	́
a	a	k8xC	a
potom	potom	k6eAd1	potom
okamžitě	okamžitě	k6eAd1	okamžitě
poskytovat	poskytovat	k5eAaImF	poskytovat
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
mírumilovní	mírumilovný	k2eAgMnPc1d1	mírumilovný
a	a	k8xC	a
hodní	hodný	k2eAgMnPc1d1	hodný
<g/>
...	...	k?	...
Tehdy	tehdy	k6eAd1	tehdy
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
úplně	úplně	k6eAd1	úplně
stáhnul	stáhnout	k5eAaPmAgMnS	stáhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
fašizující	fašizující	k2eAgFnSc1d1	fašizující
tendence	tendence	k1gFnSc1	tendence
mi	já	k3xPp1nSc3	já
připomínaly	připomínat	k5eAaImAgInP	připomínat
bolševismus	bolševismus	k1gInSc4	bolševismus
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
hávu	háv	k1gInSc6	háv
<g/>
.	.	kIx.	.
</s>
<s>
Nejsem	být	k5eNaImIp1nS	být
žádným	žádný	k3yNgMnSc7	žádný
duchovním	duchovní	k2eAgMnSc7d1	duchovní
otcem	otec	k1gMnSc7	otec
české	český	k2eAgFnSc2d1	Česká
mutace	mutace	k1gFnSc2	mutace
skinheads	skinheadsa	k1gFnPc2	skinheadsa
-	-	kIx~	-
onoho	onen	k3xDgNnSc2	onen
hnutí	hnutí	k1gNnSc2	hnutí
rozhněvaných	rozhněvaný	k2eAgMnPc2d1	rozhněvaný
mladých	mladý	k2eAgMnPc2d1	mladý
učňů	učeň	k1gMnPc2	učeň
<g/>
.	.	kIx.	.
</s>
<s>
Připadá	připadat	k5eAaPmIp3nS	připadat
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
kolem	kolem	k6eAd1	kolem
jela	jet	k5eAaImAgFnS	jet
kára	kára	k1gFnSc1	kára
s	s	k7c7	s
píchlou	píchlý	k2eAgFnSc7d1	píchlá
gumou	guma	k1gFnSc7	guma
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
půjčil	půjčit	k5eAaPmAgMnS	půjčit
lepidlo	lepidlo	k1gNnSc4	lepidlo
a	a	k8xC	a
někdo	někdo	k3yInSc1	někdo
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
svaloval	svalovat	k5eAaImAgInS	svalovat
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůz	vůz	k1gInSc4	vůz
opravili	opravit	k5eAaPmAgMnP	opravit
špatně	špatně	k6eAd1	špatně
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
S	s	k7c7	s
významným	významný	k2eAgNnSc7d1	významné
osobním	osobní	k2eAgNnSc7d1	osobní
svědectvím	svědectví	k1gNnSc7	svědectví
ohledně	ohledně	k7c2	ohledně
názorového	názorový	k2eAgInSc2d1	názorový
světa	svět	k1gInSc2	svět
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Franze	Franze	k1gFnSc2	Franze
a	a	k8xC	a
motivace	motivace	k1gFnSc2	motivace
jeho	jeho	k3xOp3gFnSc2	jeho
přítomnosti	přítomnost	k1gFnSc2	přítomnost
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
problémové	problémový	k2eAgFnSc2d1	problémová
<g />
.	.	kIx.	.
</s>
<s>
mládeže	mládež	k1gFnPc1	mládež
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
počátkem	počátkem	k7c2	počátkem
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
sociolog	sociolog	k1gMnSc1	sociolog
Jiří	Jiří	k1gMnSc1	Jiří
X.	X.	kA	X.
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
kategoricky	kategoricky	k6eAd1	kategoricky
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Franz	Franz	k1gMnSc1	Franz
projevoval	projevovat	k5eAaImAgMnS	projevovat
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
sympatie	sympatie	k1gFnPc4	sympatie
k	k	k7c3	k
nacismu	nacismus	k1gInSc3	nacismus
a	a	k8xC	a
neonacismu	neonacismus	k1gInSc3	neonacismus
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
aktivity	aktivita	k1gFnPc1	aktivita
mezi	mezi	k7c7	mezi
tehdy	tehdy	k6eAd1	tehdy
se	s	k7c7	s
profilujícími	profilující	k2eAgMnPc7d1	profilující
skinheads	skinheads	k6eAd1	skinheads
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
snahou	snaha	k1gFnSc7	snaha
tyto	tento	k3xDgFnPc4	tento
zklidnit	zklidnit	k5eAaPmF	zklidnit
a	a	k8xC	a
odproblematizovat	odproblematizovat	k5eAaImF	odproblematizovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Počátek	počátek	k1gInSc1	počátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
oba	dva	k4xCgMnPc1	dva
mladí	mladý	k2eAgMnPc1d1	mladý
intelektuálové	intelektuál	k1gMnPc1	intelektuál
<g/>
,	,	kIx,	,
neznalí	znalý	k2eNgMnPc1d1	neznalý
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
vyrostlí	vyrostlý	k2eAgMnPc1d1	vyrostlý
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
středostavovských	středostavovský	k2eAgFnPc6d1	středostavovská
rodinách	rodina	k1gFnPc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
večerů	večer	k1gInPc2	večer
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
kavárně	kavárna	k1gFnSc6	kavárna
Slavia	Slavia	k1gFnSc1	Slavia
prodiskutovali	prodiskutovat	k5eAaPmAgMnP	prodiskutovat
o	o	k7c6	o
subkulturách	subkultura	k1gFnPc6	subkultura
<g/>
,	,	kIx,	,
o	o	k7c6	o
přínosu	přínos	k1gInSc6	přínos
pankáčů	pankáč	k1gMnPc2	pankáč
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
rastamanů	rastaman	k1gInPc2	rastaman
pro	pro	k7c4	pro
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
eliminovat	eliminovat	k5eAaBmF	eliminovat
negativní	negativní	k2eAgInPc4d1	negativní
patologické	patologický	k2eAgInPc4d1	patologický
jevy	jev	k1gInPc4	jev
mezi	mezi	k7c7	mezi
subkulturami	subkultura	k1gFnPc7	subkultura
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
vybrali	vybrat	k5eAaPmAgMnP	vybrat
tu	tu	k6eAd1	tu
nejproblematičtější	problematický	k2eAgInPc4d3	nejproblematičtější
-	-	kIx~	-
skinheady	skinhead	k1gMnPc4	skinhead
(	(	kIx(	(
<g/>
neonacisté	neonacista	k1gMnPc1	neonacista
jako	jako	k8xC	jako
takoví	takový	k3xDgMnPc1	takový
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
neexistovali	existovat	k5eNaImAgMnP	existovat
<g/>
)	)	kIx)	)
-	-	kIx~	-
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
zreformujeme	zreformovat	k5eAaPmIp1nP	zreformovat
v	v	k7c4	v
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
sociální	sociální	k2eAgInSc4d1	sociální
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Koupili	koupit	k5eAaPmAgMnP	koupit
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
bombery	bombera	k1gFnPc4	bombera
a	a	k8xC	a
martensky	martensky	k6eAd1	martensky
<g/>
,	,	kIx,	,
vyholili	vyholit	k5eAaPmAgMnP	vyholit
hlavy	hlava	k1gFnPc4	hlava
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
skinheadům	skinhead	k1gMnPc3	skinhead
vysvětlíme	vysvětlit	k5eAaPmIp1nP	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mají	mít	k5eAaImIp3nP	mít
správně	správně	k6eAd1	správně
skinheadit	skinheadit	k5eAaPmF	skinheadit
<g/>
.	.	kIx.	.
</s>
<s>
Vypracovali	vypracovat	k5eAaPmAgMnP	vypracovat
jsme	být	k5eAaImIp1nP	být
koncept	koncept	k1gInSc4	koncept
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
skinheadských	skinheadský	k2eAgFnPc2d1	skinheadská
tlup	tlupa	k1gFnPc2	tlupa
ujmout	ujmout	k5eAaPmF	ujmout
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Skinheadi	skinhead	k1gMnPc1	skinhead
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
přijímáni	přijímat	k5eAaImNgMnP	přijímat
do	do	k7c2	do
vnitrem	vnitro	k1gNnSc7	vnitro
řízených	řízený	k2eAgInPc2d1	řízený
oddílů	oddíl	k1gInPc2	oddíl
domobrany	domobrana	k1gFnSc2	domobrana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
naučili	naučit	k5eAaPmAgMnP	naučit
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
správným	správný	k2eAgMnPc3d1	správný
stravovacím	stravovací	k2eAgMnPc3d1	stravovací
návykům	návyk	k1gInPc3	návyk
<g/>
,	,	kIx,	,
úctě	úcta	k1gFnSc6	úcta
k	k	k7c3	k
právu	právo	k1gNnSc3	právo
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jsme	být	k5eAaImIp1nP	být
s	s	k7c7	s
Láďou	Láďa	k1gMnSc7	Láďa
vyrostli	vyrůst	k5eAaPmAgMnP	vyrůst
v	v	k7c6	v
dětinském	dětinský	k2eAgNnSc6d1	dětinské
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidi	člověk	k1gMnPc4	člověk
lze	lze	k6eAd1	lze
měnit	měnit	k5eAaImF	měnit
a	a	k8xC	a
že	že	k8xS	že
klíčovým	klíčový	k2eAgInSc7d1	klíčový
určujícím	určující	k2eAgInSc7d1	určující
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
prostředí	prostředí	k1gNnSc1	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
tehdy	tehdy	k6eAd1	tehdy
nenapadlo	napadnout	k5eNaPmAgNnS	napadnout
-	-	kIx~	-
protože	protože	k8xS	protože
jsme	být	k5eAaImIp1nP	být
neměli	mít	k5eNaImAgMnP	mít
žádnou	žádný	k3yNgFnSc4	žádný
osobní	osobní	k2eAgFnSc4d1	osobní
zkušenost	zkušenost	k1gFnSc4	zkušenost
s	s	k7c7	s
nejnižšími	nízký	k2eAgFnPc7d3	nejnižší
vrstvami	vrstva	k1gFnPc7	vrstva
-	-	kIx~	-
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
skinů	skin	k1gMnPc2	skin
jsou	být	k5eAaImIp3nP	být
nesocializovatelní	socializovatelný	k2eNgMnPc1d1	socializovatelný
sociopati	sociopat	k5eAaPmF	sociopat
a	a	k8xC	a
nejde	jít	k5eNaImIp3nS	jít
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
dělat	dělat	k5eAaImF	dělat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
je	on	k3xPp3gFnPc4	on
pozavírat	pozavírat	k5eAaPmF	pozavírat
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnPc1	náš
teorie	teorie	k1gFnPc1	teorie
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
Slavii	slavie	k1gFnSc6	slavie
velmi	velmi	k6eAd1	velmi
pěkné	pěkný	k2eAgNnSc1d1	pěkné
a	a	k8xC	a
přesvědčivé	přesvědčivý	k2eAgNnSc1d1	přesvědčivé
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
ale	ale	k9	ale
nastal	nastat	k5eAaPmAgInS	nastat
ostrý	ostrý	k2eAgInSc1d1	ostrý
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
<g/>
.	.	kIx.	.
</s>
<s>
Vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
jsme	být	k5eAaImIp1nP	být
do	do	k7c2	do
skinheadské	skinheadský	k2eAgFnSc2d1	skinheadská
hospody	hospody	k?	hospody
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
jako	jako	k8xS	jako
klukům	kluk	k1gMnPc3	kluk
vysvětlíme	vysvětlit	k5eAaPmIp1nP	vysvětlit
<g/>
,	,	kIx,	,
a	a	k8xC	a
dostali	dostat	k5eAaPmAgMnP	dostat
jsme	být	k5eAaImIp1nP	být
přes	přes	k7c4	přes
držku	držka	k1gFnSc4	držka
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jsme	být	k5eAaImIp1nP	být
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
šli	jít	k5eAaImAgMnP	jít
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
rezultátem	rezultát	k1gInSc7	rezultát
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jsme	být	k5eAaImIp1nP	být
mezi	mezi	k7c7	mezi
skiny	skin	k1gMnPc7	skin
zavedli	zavést	k5eAaPmAgMnP	zavést
štáb	štáb	k1gInSc4	štáb
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
jsme	být	k5eAaImIp1nP	být
dostali	dostat	k5eAaPmAgMnP	dostat
přes	přes	k7c4	přes
hubu	huba	k1gFnSc4	huba
i	i	k9	i
se	s	k7c7	s
štábem	štáb	k1gInSc7	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k8xS	takže
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
Slavie	slavie	k1gFnSc2	slavie
<g/>
,	,	kIx,	,
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
ještě	ještě	k9	ještě
rozebírali	rozebírat	k5eAaImAgMnP	rozebírat
naše	náš	k3xOp1gFnPc4	náš
sociálně-	sociálně-	k?	sociálně-
inženýrské	inženýrský	k2eAgFnPc4d1	inženýrská
teorie	teorie	k1gFnPc4	teorie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c7	mezi
realskiny	realskin	k1gInPc7	realskin
jsme	být	k5eAaImIp1nP	být
už	už	k6eAd1	už
nešli	jít	k5eNaImAgMnP	jít
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
závěrem	závěr	k1gInSc7	závěr
<g/>
:	:	kIx,	:
doktor	doktor	k1gMnSc1	doktor
Franz	Franz	k1gMnSc1	Franz
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
ideovým	ideový	k2eAgMnSc7d1	ideový
vůdcem	vůdce	k1gMnSc7	vůdce
českých	český	k2eAgMnPc2d1	český
skinů	skin	k1gMnPc2	skin
a	a	k8xC	a
naučit	naučit	k5eAaPmF	naučit
je	být	k5eAaImIp3nS	být
Dobru	dobro	k1gNnSc3	dobro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepovedlo	povést	k5eNaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Aspiroval	aspirovat	k5eAaImAgMnS	aspirovat
na	na	k7c4	na
ideologa	ideolog	k1gMnSc4	ideolog
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgInS	chtít
skiny	skin	k1gMnPc4	skin
zklidnit	zklidnit	k5eAaPmF	zklidnit
a	a	k8xC	a
odproblematizovat	odproblematizovat	k5eAaBmF	odproblematizovat
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
ani	ani	k9	ani
nejmenší	malý	k2eAgFnPc4d3	nejmenší
sympatie	sympatie	k1gFnPc4	sympatie
k	k	k7c3	k
nacismu	nacismus	k1gInSc3	nacismus
a	a	k8xC	a
neonacismu	neonacismus	k1gInSc3	neonacismus
<g/>
.	.	kIx.	.
</s>
<s>
Tečka	tečka	k1gFnSc1	tečka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
Franz	Franz	k1gInSc4	Franz
příležitostně	příležitostně	k6eAd1	příležitostně
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
působení	působení	k1gNnSc4	působení
mezi	mezi	k7c4	mezi
skinheads	skinheads	k1gInSc4	skinheads
médii	médium	k1gNnPc7	médium
dotazován	dotazován	k2eAgInSc4d1	dotazován
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Reflex	reflex	k1gInSc4	reflex
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
Petra	Petr	k1gMnSc2	Petr
Holce	holec	k1gMnSc2	holec
"	"	kIx"	"
<g/>
Počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
jste	být	k5eAaImIp2nP	být
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
osvětou	osvěta	k1gFnSc7	osvěta
působit	působit	k5eAaImF	působit
na	na	k7c4	na
skiny	skin	k1gMnPc4	skin
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
jste	být	k5eAaImIp2nP	být
však	však	k9	však
skončil	skončit	k5eAaPmAgMnS	skončit
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
</s>
<s>
Jaká	jaký	k3yRgFnSc1	jaký
byla	být	k5eAaImAgFnS	být
vaše	váš	k3xOp2gNnSc4	váš
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
vize	vize	k1gFnSc1	vize
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
podchytit	podchytit	k5eAaPmF	podchytit
problémovou	problémový	k2eAgFnSc4d1	problémová
městskou	městský	k2eAgFnSc4d1	městská
mládež	mládež	k1gFnSc4	mládež
a	a	k8xC	a
naznačit	naznačit	k5eAaPmF	naznačit
jí	on	k3xPp3gFnSc7	on
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
ultrapravicové	ultrapravicový	k2eAgNnSc4d1	ultrapravicové
smýšlení	smýšlení	k1gNnSc4	smýšlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
učilišti	učiliště	k1gNnSc6	učiliště
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
nad	nad	k7c7	nad
nikým	nikdo	k3yNnSc7	nikdo
-	-	kIx~	-
byť	byť	k8xS	byť
sebezanedbanějším	sebezanedbaný	k2eAgMnSc6d2	sebezanedbaný
-	-	kIx~	-
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
neměla	mít	k5eNaImAgNnP	mít
a	a	k8xC	a
priori	priori	k6eAd1	priori
lámat	lámat	k5eAaImF	lámat
hůl	hůl	k1gFnSc4	hůl
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
základali	základat	k5eAaImAgMnP	základat
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
zajímali	zajímat	k5eAaImAgMnP	zajímat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
společnosti	společnost	k1gFnSc2	společnost
pomoci	pomoct	k5eAaPmF	pomoct
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
jsem	být	k5eAaImIp1nS	být
něco	něco	k6eAd1	něco
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
problematická	problematický	k2eAgFnSc1d1	problematická
městská	městský	k2eAgFnSc1d1	městská
mládež	mládež	k1gFnSc1	mládež
jemně	jemně	k6eAd1	jemně
podchytila	podchytit	k5eAaPmAgFnS	podchytit
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc4	společnost
celý	celý	k2eAgInSc4d1	celý
problém	problém	k1gInSc4	problém
zbytečně	zbytečně	k6eAd1	zbytečně
skandalizovala	skandalizovat	k5eAaBmAgFnS	skandalizovat
a	a	k8xC	a
medializovala	medializovat	k5eAaImAgFnS	medializovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dnešní	dnešní	k2eAgInSc4d1	dnešní
stav	stav	k1gInSc4	stav
ultrapravicové	ultrapravicový	k2eAgFnSc2d1	ultrapravicová
devalvace	devalvace	k1gFnSc2	devalvace
části	část	k1gFnSc2	část
mládeže	mládež	k1gFnSc2	mládež
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
společnost	společnost	k1gFnSc1	společnost
může	moct	k5eAaImIp3nS	moct
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
jevila	jevit	k5eAaImAgFnS	jevit
růžově	růžově	k6eAd1	růžově
a	a	k8xC	a
my	my	k3xPp1nPc1	my
jsme	být	k5eAaImIp1nP	být
byli	být	k5eAaImAgMnP	být
idealisté	idealista	k1gMnPc1	idealista
<g/>
.	.	kIx.	.
</s>
<s>
Domnívali	domnívat	k5eAaImAgMnP	domnívat
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
jako	jako	k9	jako
ve	v	k7c6	v
foglarovce	foglarovka	k1gFnSc6	foglarovka
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
nad	nad	k7c7	nad
lží	lež	k1gFnSc7	lež
a	a	k8xC	a
nenávistí	nenávist	k1gFnSc7	nenávist
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c4	na
chatu	chata	k1gFnSc4	chata
serveru	server	k1gInSc2	server
Tiscalli	Tiscall	k1gMnPc1	Tiscall
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2006	[number]	k4	2006
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
Počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
jste	být	k5eAaImIp2nP	být
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
médiích	médium	k1gNnPc6	médium
(	(	kIx(	(
<g/>
Reflex	reflex	k1gInSc1	reflex
<g/>
)	)	kIx)	)
propírán	propírán	k2eAgMnSc1d1	propírán
coby	coby	k?	coby
extrémista	extrémista	k1gMnSc1	extrémista
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
skinheadů	skinhead	k1gMnPc2	skinhead
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Čím	co	k3yQnSc7	co
jste	být	k5eAaImIp2nP	být
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
jen	jen	k6eAd1	jen
pitomost	pitomost	k1gFnSc1	pitomost
redaktorů	redaktor	k1gMnPc2	redaktor
nebo	nebo	k8xC	nebo
nějaká	nějaký	k3yIgFnSc1	nějaký
osobní	osobní	k2eAgFnSc1d1	osobní
animozita	animozita	k1gFnSc1	animozita
<g/>
?	?	kIx.	?
</s>
<s>
Jak	jak	k8xS	jak
moc	moc	k6eAd1	moc
Vám	vy	k3xPp2nPc3	vy
to	ten	k3xDgNnSc1	ten
uškodilo	uškodit	k5eAaPmAgNnS	uškodit
<g/>
?	?	kIx.	?
</s>
<s>
Setkáte	setkat	k5eAaPmIp2nP	setkat
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
náhledem	náhled	k1gInSc7	náhled
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
osobu	osoba	k1gFnSc4	osoba
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
reagoval	reagovat	k5eAaBmAgInS	reagovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
mediální	mediální	k2eAgFnSc1d1	mediální
mystifikace	mystifikace	k1gFnSc1	mystifikace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyplynula	vyplynout	k5eAaPmAgFnS	vyplynout
z	z	k7c2	z
banální	banální	k2eAgFnSc2d1	banální
osobní	osobní	k2eAgFnSc2d1	osobní
animozity	animozita	k1gFnSc2	animozita
<g/>
.	.	kIx.	.
</s>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
jenom	jenom	k9	jenom
trochu	trochu	k6eAd1	trochu
něco	něco	k3yInSc4	něco
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
podnikal	podnikat	k5eAaImAgMnS	podnikat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
publicita	publicita	k1gFnSc1	publicita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mi	já	k3xPp1nSc3	já
byla	být	k5eAaImAgFnS	být
věnována	věnován	k2eAgFnSc1d1	věnována
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mi	já	k3xPp1nSc3	já
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
zajistila	zajistit	k5eAaPmAgFnS	zajistit
minimálně	minimálně	k6eAd1	minimálně
místo	místo	k7c2	místo
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Uškodilo	uškodit	k5eAaPmAgNnS	uškodit
mi	já	k3xPp1nSc3	já
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
<g/>
.	.	kIx.	.
</s>
<s>
Minimálně	minimálně	k6eAd1	minimálně
už	už	k9	už
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
ne	ne	k9	ne
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
krátké	krátký	k2eAgNnSc1d1	krátké
video	video	k1gNnSc1	video
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
"	"	kIx"	"
<g/>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
<g/>
:	:	kIx,	:
O	o	k7c6	o
korupci	korupce	k1gFnSc6	korupce
a	a	k8xC	a
extremismu	extremismus	k1gInSc6	extremismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
až	až	k6eAd1	až
úsměvné	úsměvný	k2eAgNnSc1d1	úsměvné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
reminiscence	reminiscence	k1gFnPc1	reminiscence
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
já	já	k3xPp1nSc1	já
nějak	nějak	k6eAd1	nějak
souvisím	souviset	k5eAaImIp1nS	souviset
s	s	k7c7	s
extrémismem	extrémismus	k1gInSc7	extrémismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
před	před	k7c7	před
dvaceti	dvacet	k4xCc7	dvacet
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
někdy	někdy	k6eAd1	někdy
nějaká	nějaký	k3yIgFnSc1	nějaký
vlaštovka	vlaštovka	k1gFnSc1	vlaštovka
jako	jako	k9	jako
by	by	kYmCp3nS	by
přiletěla	přiletět	k5eAaPmAgFnS	přiletět
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
v	v	k7c6	v
učňovském	učňovský	k2eAgNnSc6d1	učňovské
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
problémovou	problémový	k2eAgFnSc7d1	problémová
městskou	městský	k2eAgFnSc7d1	městská
mládeží	mládež	k1gFnSc7	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
nejsou	být	k5eNaImIp3nP	být
pouze	pouze	k6eAd1	pouze
ztracená	ztracený	k2eAgFnSc1d1	ztracená
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
neustále	neustále	k6eAd1	neustále
líčeno	líčit	k5eAaImNgNnS	líčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
bezradní	bezradný	k2eAgMnPc1d1	bezradný
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
najít	najít	k5eAaPmF	najít
nějaké	nějaký	k3yIgNnSc4	nějaký
řešení	řešení	k1gNnSc4	řešení
nebo	nebo	k8xC	nebo
zkusit	zkusit	k5eAaPmF	zkusit
nebo	nebo	k8xC	nebo
naznačit	naznačit	k5eAaPmF	naznačit
i	i	k8xC	i
státu	stát	k1gInSc2	stát
jaké	jaký	k3yQgFnPc4	jaký
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
potenciální	potenciální	k2eAgNnSc1d1	potenciální
řešení	řešení	k1gNnSc1	řešení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
energie	energie	k1gFnSc1	energie
těchto	tento	k3xDgMnPc2	tento
lidí	člověk	k1gMnPc2	člověk
nezvrhla	zvrhnout	k5eNaPmAgFnS	zvrhnout
do	do	k7c2	do
nějakých	nějaký	k3yIgInPc2	nějaký
opravdu	opravdu	k6eAd1	opravdu
negativních	negativní	k2eAgInPc2d1	negativní
konců	konec	k1gInPc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
celé	celý	k2eAgNnSc1d1	celé
ne	ne	k9	ne
medializováno	medializovat	k5eAaImNgNnS	medializovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přímo	přímo	k6eAd1	přímo
skandalizováno	skandalizován	k2eAgNnSc1d1	skandalizováno
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
se	se	k3xPyFc4	se
z	z	k7c2	z
lékaře	lékař	k1gMnSc2	lékař
stal	stát	k5eAaPmAgMnS	stát
pacient	pacient	k1gMnSc1	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nS	kdyby
profesor	profesor	k1gMnSc1	profesor
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hledal	hledat	k5eAaImAgMnS	hledat
vakcínu	vakcína	k1gFnSc4	vakcína
proti	proti	k7c3	proti
vzteklině	vzteklina	k1gFnSc3	vzteklina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
najednou	najednou	k6eAd1	najednou
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzteklinu	vzteklina	k1gFnSc4	vzteklina
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Čili	čili	k8xC	čili
jsem	být	k5eAaImIp1nS	být
vlastně	vlastně	k9	vlastně
přišel	přijít	k5eAaPmAgMnS	přijít
k	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
nezasloužené	zasloužený	k2eNgFnSc3d1	nezasloužená
<g/>
,	,	kIx,	,
v	v	k7c6	v
uvozovkách	uvozovka	k1gFnPc6	uvozovka
<g/>
,	,	kIx,	,
popularitě	popularita	k1gFnSc3	popularita
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
jsem	být	k5eAaImIp1nS	být
opravdu	opravdu	k6eAd1	opravdu
nestál	stát	k5eNaImAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
říkám	říkat	k5eAaImIp1nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
extrémismus	extrémismus	k1gInSc1	extrémismus
je	být	k5eAaImIp3nS	být
výrazem	výraz	k1gInSc7	výraz
nějaké	nějaký	k3yIgFnSc2	nějaký
společenské	společenský	k2eAgFnSc2d1	společenská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
slabších	slabý	k2eAgFnPc2d2	slabší
nátur	nátura	k1gFnPc2	nátura
<g/>
,	,	kIx,	,
a	a	k8xC	a
takto	takto	k6eAd1	takto
by	by	k9	by
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
přistupováno	přistupován	k2eAgNnSc1d1	přistupováno
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Začátkem	začátkem	k7c2	začátkem
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
Franz	Franza	k1gFnPc2	Franza
pro	pro	k7c4	pro
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
jako	jako	k9	jako
skinhead	skinhead	k1gMnSc1	skinhead
necítil	cítit	k5eNaImAgMnS	cítit
a	a	k8xC	a
necítím	cítit	k5eNaImIp1nS	cítit
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
být	být	k5eAaImF	být
ani	ani	k8xC	ani
dnes	dnes	k6eAd1	dnes
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
pro	pro	k7c4	pro
magazín	magazín	k1gInSc4	magazín
MF	MF	kA	MF
Dnes	dnes	k6eAd1	dnes
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
někdy	někdy	k6eAd1	někdy
sympatizoval	sympatizovat	k5eAaImAgMnS	sympatizovat
se	s	k7c7	s
skinheadským	skinheadský	k2eAgNnSc7d1	skinheadské
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
příslušnosti	příslušnost	k1gFnSc6	příslušnost
ke	k	k7c3	k
skinheadskému	skinheadský	k2eAgNnSc3d1	skinheadské
hnutí	hnutí	k1gNnSc3	hnutí
byla	být	k5eAaImAgFnS	být
údajně	údajně	k6eAd1	údajně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
ze	z	k7c2	z
msty	msta	k1gFnSc2	msta
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
grafik	grafik	k1gMnSc1	grafik
nedodal	dodat	k5eNaPmAgMnS	dodat
včas	včas	k6eAd1	včas
návrh	návrh	k1gInSc4	návrh
plakátu	plakát	k1gInSc2	plakát
pro	pro	k7c4	pro
koncert	koncert	k1gInSc4	koncert
a	a	k8xC	a
Franz	Franz	k1gMnSc1	Franz
zadal	zadat	k5eAaPmAgMnS	zadat
práci	práce	k1gFnSc4	práce
jinému	jiný	k1gMnSc3	jiný
grafikovi	grafik	k1gMnSc3	grafik
<g/>
,	,	kIx,	,
pomlouvačný	pomlouvačný	k2eAgInSc1d1	pomlouvačný
článek	článek	k1gInSc1	článek
v	v	k7c6	v
Mladém	mladý	k2eAgInSc6d1	mladý
světě	svět	k1gInSc6	svět
vyšel	vyjít	k5eAaPmAgInS	vyjít
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prý	prý	k9	prý
založil	založit	k5eAaPmAgMnS	založit
organizaci	organizace	k1gFnSc4	organizace
Nová	nový	k2eAgFnSc1d1	nová
skinheadská	skinheadský	k2eAgFnSc1d1	skinheadská
jednota	jednota	k1gFnSc1	jednota
a	a	k8xC	a
časopis	časopis	k1gInSc1	časopis
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
měl	mít	k5eAaImAgMnS	mít
vymezit	vymezit	k5eAaPmF	vymezit
základní	základní	k2eAgNnPc4d1	základní
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
skinheady	skinhead	k1gMnPc4	skinhead
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Franz	Franz	k1gMnSc1	Franz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
se	s	k7c7	s
smíchem	smích	k1gInSc7	smích
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
dočetl	dočíst	k5eAaPmAgMnS	dočíst
i	i	k9	i
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
knížce	knížka	k1gFnSc6	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Strašně	strašně	k6eAd1	strašně
rád	rád	k6eAd1	rád
bych	by	kYmCp1nS	by
tu	ten	k3xDgFnSc4	ten
organizaci	organizace	k1gFnSc4	organizace
i	i	k8xC	i
časopis	časopis	k1gInSc4	časopis
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Franz	Franz	k1gMnSc1	Franz
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
tato	tento	k3xDgFnSc1	tento
tendence	tendence	k1gFnSc1	tendence
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
mládeží	mládež	k1gFnSc7	mládež
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
naprosto	naprosto	k6eAd1	naprosto
nesouvisela	souviset	k5eNaImAgNnP	souviset
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
nepříjemným	příjemný	k2eNgInSc7d1	nepříjemný
<g/>
,	,	kIx,	,
s	s	k7c7	s
čím	co	k3yInSc7	co
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
scéna	scéna	k1gFnSc1	scéna
spojována	spojovat	k5eAaImNgFnS	spojovat
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
nacisticky	nacisticky	k6eAd1	nacisticky
orientováno	orientován	k2eAgNnSc1d1	orientováno
<g/>
.	.	kIx.	.
</s>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Mareš	Mareš	k1gMnSc1	Mareš
oponuje	oponovat	k5eAaImIp3nS	oponovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
scéna	scéna	k1gFnSc1	scéna
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
kriminálního	kriminální	k2eAgInSc2d1	kriminální
pohledu	pohled	k1gInSc2	pohled
naopak	naopak	k6eAd1	naopak
nebezpečnější	bezpečný	k2eNgFnSc1d2	nebezpečnější
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
dnešní	dnešní	k2eAgFnSc1d1	dnešní
a	a	k8xC	a
počet	počet	k1gInSc1	počet
jimi	on	k3xPp3gMnPc7	on
spáchaných	spáchaný	k2eAgFnPc2d1	spáchaná
rasových	rasový	k2eAgFnPc2d1	rasová
vražd	vražda	k1gFnPc2	vražda
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nebyli	být	k5eNaImAgMnP	být
tak	tak	k9	tak
ideově	ideově	k6eAd1	ideově
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
někteří	některý	k3yIgMnPc1	některý
neonacisté	neonacista	k1gMnPc1	neonacista
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Docházení	docházení	k1gNnSc1	docházení
na	na	k7c4	na
koncerty	koncert	k1gInPc4	koncert
Orlíku	Orlík	k1gInSc2	Orlík
Franz	Franz	k1gMnSc1	Franz
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvedl	uvést	k5eAaPmAgMnS	uvést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Daniel	Daniel	k1gMnSc1	Daniel
Landa	Landa	k1gMnSc1	Landa
ho	on	k3xPp3gMnSc4	on
zajímal	zajímat	k5eAaImAgMnS	zajímat
nikoliv	nikoliv	k9	nikoliv
jako	jako	k8xC	jako
zpěvák	zpěvák	k1gMnSc1	zpěvák
své	svůj	k3xOyFgFnSc2	svůj
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xC	jako
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
možnost	možnost	k1gFnSc4	možnost
vypracovat	vypracovat	k5eAaPmF	vypracovat
se	se	k3xPyFc4	se
na	na	k7c4	na
poměrně	poměrně	k6eAd1	poměrně
slušného	slušný	k2eAgMnSc2d1	slušný
šansoniéra	šansoniér	k1gMnSc2	šansoniér
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
texty	text	k1gInPc1	text
Orlíku	Orlík	k1gInSc2	Orlík
prý	prý	k9	prý
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
slabomyslné	slabomyslný	k2eAgNnSc4d1	slabomyslné
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
navštívil	navštívit	k5eAaPmAgMnS	navštívit
asi	asi	k9	asi
dvě	dva	k4xCgFnPc4	dva
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k8xC	i
David	David	k1gMnSc1	David
Matásek	Matásek	k1gMnSc1	Matásek
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgMnSc4	jenž
Franz	Franz	k1gInSc4	Franz
na	na	k7c6	na
zkouškách	zkouška	k1gFnPc6	zkouška
seděl	sedět	k5eAaImAgMnS	sedět
"	"	kIx"	"
<g/>
takový	takový	k3xDgMnSc1	takový
tichý	tichý	k2eAgMnSc1d1	tichý
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
neurotický	neurotický	k2eAgMnSc1d1	neurotický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
obrovským	obrovský	k2eAgInSc7d1	obrovský
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
"	"	kIx"	"
<g/>
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
zpětně	zpětně	k6eAd1	zpětně
díváte	dívat	k5eAaImIp2nP	dívat
na	na	k7c4	na
vaši	váš	k3xOp2gFnSc4	váš
aktivitu	aktivita	k1gFnSc4	aktivita
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsme	být	k5eAaImIp1nP	být
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
dělnických	dělnický	k2eAgFnPc6d1	Dělnická
profesích	profes	k1gFnPc6	profes
<g/>
,	,	kIx,	,
mě	já	k3xPp1nSc4	já
zajímala	zajímat	k5eAaImAgFnS	zajímat
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
jsem	být	k5eAaImIp1nS	být
tu	ten	k3xDgFnSc4	ten
možnost	možnost	k1gFnSc4	možnost
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jsou	být	k5eAaImIp3nP	být
nejrůznější	různý	k2eAgFnPc1d3	nejrůznější
společenské	společenský	k2eAgFnPc1d1	společenská
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jelikož	jelikož	k8xS	jelikož
zastávám	zastávat	k5eAaImIp1nS	zastávat
názor	názor	k1gInSc1	názor
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
nesáhnu	sáhnout	k5eNaPmIp1nS	sáhnout
<g/>
,	,	kIx,	,
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nic	nic	k6eAd1	nic
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
zajímalo	zajímat	k5eAaImAgNnS	zajímat
mě	já	k3xPp1nSc4	já
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
jsem	být	k5eAaImIp1nS	být
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
vezmou	vzít	k5eAaPmIp3nP	vzít
bundu	bund	k1gInSc3	bund
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
metalisti	metalist	k1gMnPc1	metalist
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
vezmou	vzít	k5eAaPmIp3nP	vzít
čelenku	čelenka	k1gFnSc4	čelenka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
indiáni	indián	k1gMnPc1	indián
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
načešou	načesat	k5eAaPmIp3nP	načesat
číro	číro	k6eAd1	číro
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pankáči	pankáč	k1gMnPc1	pankáč
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
tyto	tento	k3xDgFnPc4	tento
sociologické	sociologický	k2eAgFnPc4d1	sociologická
sondy	sonda	k1gFnPc4	sonda
stran	strana	k1gFnPc2	strana
společnosti	společnost	k1gFnPc1	společnost
nesouvisely	souviset	k5eNaImAgFnP	souviset
s	s	k7c7	s
mým	můj	k3xOp1gInSc7	můj
názorem	názor	k1gInSc7	názor
nebo	nebo	k8xC	nebo
s	s	k7c7	s
mým	můj	k3xOp1gInSc7	můj
názorovým	názorový	k2eAgInSc7d1	názorový
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
mě	já	k3xPp1nSc4	já
to	ten	k3xDgNnSc1	ten
zajímalo	zajímat	k5eAaImAgNnS	zajímat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
sociologická	sociologický	k2eAgFnSc1d1	sociologická
sonda	sonda	k1gFnSc1	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
amnestie	amnestie	k1gFnSc1	amnestie
<g/>
,	,	kIx,	,
strávil	strávit	k5eAaPmAgMnS	strávit
týden	týden	k1gInSc4	týden
mezi	mezi	k7c7	mezi
kriminálníky	kriminálník	k1gMnPc7	kriminálník
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
amnestovanými	amnestovaný	k2eAgMnPc7d1	amnestovaný
<g/>
,	,	kIx,	,
na	na	k7c6	na
Hlavním	hlavní	k2eAgNnSc6d1	hlavní
nádraží	nádraží	k1gNnSc6	nádraží
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mě	já	k3xPp1nSc4	já
zajímalo	zajímat	k5eAaImAgNnS	zajímat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
sociologickou	sociologický	k2eAgFnSc4d1	sociologická
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
zajímal	zajímat	k5eAaImAgMnS	zajímat
mě	já	k3xPp1nSc4	já
třeba	třeba	k6eAd1	třeba
běh	běh	k1gInSc1	běh
non-stopu	nontop	k1gInSc2	non-stop
na	na	k7c6	na
Václavském	václavský	k2eAgNnSc6d1	Václavské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
funguje	fungovat	k5eAaImIp3nS	fungovat
organizace	organizace	k1gFnSc1	organizace
<g />
.	.	kIx.	.
</s>
<s>
<g/>
...	...	k?	...
nebo	nebo	k8xC	nebo
<g/>
...	...	k?	...
jak	jak	k6eAd1	jak
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
organizována	organizován	k2eAgFnSc1d1	organizována
noc	noc	k1gFnSc4	noc
pražskou	pražský	k2eAgFnSc7d1	Pražská
galérkou	galérka	k1gFnSc7	galérka
<g/>
,	,	kIx,	,
zajímaly	zajímat	k5eAaImAgInP	zajímat
mě	já	k3xPp1nSc2	já
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
sociologické	sociologický	k2eAgInPc4d1	sociologický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
by	by	kYmCp3nS	by
sociolog	sociolog	k1gMnSc1	sociolog
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sedí	sedit	k5eAaImIp3nS	sedit
v	v	k7c6	v
kanceláři	kancelář	k1gFnSc6	kancelář
<g/>
,	,	kIx,	,
a	a	k8xC	a
čte	číst	k5eAaImIp3nS	číst
jenom	jenom	k9	jenom
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyděšen	vyděšen	k2eAgMnSc1d1	vyděšen
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
ulicích	ulice	k1gFnPc6	ulice
kdokoli	kdokoli	k3yInSc1	kdokoli
<g />
.	.	kIx.	.
</s>
<s>
chodí	chodit	k5eAaImIp3nP	chodit
<g/>
...	...	k?	...
samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
byl	být	k5eAaImAgMnS	být
dobrodružný	dobrodružný	k2eAgMnSc1d1	dobrodružný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mě	já	k3xPp1nSc4	já
zajímaly	zajímat	k5eAaImAgInP	zajímat
tyhlecty	tyhlect	k1gInPc1	tyhlect
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
<g/>
,	,	kIx,	,
trošku	trošku	k6eAd1	trošku
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
normální	normální	k2eAgInPc1d1	normální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
normální	normální	k2eAgMnPc4d1	normální
lidi	člověk	k1gMnPc4	člověk
člověk	člověk	k1gMnSc1	člověk
může	moct	k5eAaImIp3nS	moct
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
tramvaji	tramvaj	k1gFnSc6	tramvaj
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Respekt	respekt	k1gInSc4	respekt
na	na	k7c6	na
konci	konec	k1gInSc6	konec
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
<g />
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gInSc1	Franz
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
údajné	údajný	k2eAgFnSc3d1	údajná
skinheadské	skinheadský	k2eAgFnSc3d1	skinheadská
historii	historie	k1gFnSc3	historie
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zajímal	zajímat	k5eAaImAgMnS	zajímat
mě	já	k3xPp1nSc4	já
dav	dav	k1gInSc1	dav
v	v	k7c6	v
mé	můj	k3xOp1gFnSc6	můj
energii	energie	k1gFnSc6	energie
<g/>
,	,	kIx,	,
dělal	dělat	k5eAaImAgMnS	dělat
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
pozorování	pozorování	k1gNnSc4	pozorování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
násilí	násilí	k1gNnSc4	násilí
ošklivil	ošklivit	k5eAaImAgMnS	ošklivit
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
energii	energie	k1gFnSc4	energie
těch	ten	k3xDgMnPc2	ten
kluků	kluk	k1gMnPc2	kluk
napřít	napřít	k5eAaPmF	napřít
dobrým	dobrý	k2eAgInSc7d1	dobrý
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
aby	aby	kYmCp3nP	aby
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
Časopis	časopis	k1gInSc1	časopis
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gNnSc1	jeho
tvrzení	tvrzení	k1gNnSc1	tvrzení
o	o	k7c6	o
pouze	pouze	k6eAd1	pouze
studijních	studijní	k2eAgInPc6d1	studijní
a	a	k8xC	a
bohulibých	bohulibý	k2eAgInPc6d1	bohulibý
účelech	účel	k1gInPc6	účel
skinheadského	skinheadský	k2eAgNnSc2d1	skinheadské
angažmá	angažmá	k1gNnSc2	angažmá
se	se	k3xPyFc4	se
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
nedají	dát	k5eNaPmIp3nP	dát
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
na	na	k7c4	na
Franzovo	Franzův	k2eAgNnSc4d1	Franzovo
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
subkultuře	subkultura	k1gFnSc6	subkultura
dívá	dívat	k5eAaImIp3nS	dívat
i	i	k9	i
politolog	politolog	k1gMnSc1	politolog
zaměřený	zaměřený	k2eAgMnSc1d1	zaměřený
na	na	k7c4	na
extremismus	extremismus	k1gInSc4	extremismus
Miroslav	Miroslav	k1gMnSc1	Miroslav
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
jeho	on	k3xPp3gNnSc2	on
pojednání	pojednání	k1gNnSc2	pojednání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
chtěl	chtít	k5eAaImAgMnS	chtít
potetovaný	potetovaný	k2eAgMnSc1d1	potetovaný
vychovatel	vychovatel	k1gMnSc1	vychovatel
vytvořit	vytvořit	k5eAaPmF	vytvořit
ze	z	k7c2	z
skinů	skin	k1gMnPc2	skin
"	"	kIx"	"
<g/>
apolitické	apolitický	k2eAgFnSc2d1	apolitická
a	a	k8xC	a
společnosti	společnost	k1gFnSc2	společnost
prospěšné	prospěšný	k2eAgFnSc2d1	prospěšná
disciplinované	disciplinovaný	k2eAgFnSc2d1	disciplinovaná
jednotky	jednotka	k1gFnSc2	jednotka
pro	pro	k7c4	pro
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
pořádkem	pořádek	k1gInSc7	pořádek
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
kriminalitou	kriminalita	k1gFnSc7	kriminalita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
svědci	svědek	k1gMnPc1	svědek
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
Franzovo	Franzův	k2eAgNnSc4d1	Franzovo
nadšení	nadšení	k1gNnSc4	nadšení
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
:	:	kIx,	:
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
prý	prý	k9	prý
pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
Mostě	most	k1gInSc6	most
v	v	k7c6	v
romské	romský	k2eAgFnSc6d1	romská
rodině	rodina	k1gFnSc6	rodina
a	a	k8xC	a
jezdil	jezdit	k5eAaImAgMnS	jezdit
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
každé	každý	k3xTgFnPc4	každý
ráno	ráno	k1gNnSc1	ráno
na	na	k7c4	na
šichtu	šichta	k1gFnSc4	šichta
do	do	k7c2	do
dolu	dol	k1gInSc2	dol
Vrbenský	Vrbenský	k2eAgMnSc1d1	Vrbenský
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
prostě	prostě	k9	prostě
mě	já	k3xPp1nSc4	já
to	ten	k3xDgNnSc1	ten
zajímalo	zajímat	k5eAaImAgNnS	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
stejně	stejně	k6eAd1	stejně
jsem	být	k5eAaImIp1nS	být
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
lidí	člověk	k1gMnPc2	člověk
propuštěných	propuštěný	k2eAgMnPc2d1	propuštěný
Václavem	Václav	k1gMnSc7	Václav
Havlem	Havel	k1gMnSc7	Havel
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
amnestii	amnestie	k1gFnSc6	amnestie
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
budou	být	k5eAaImBp3nP	být
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
citovaný	citovaný	k2eAgInSc4d1	citovaný
článek	článek	k1gInSc4	článek
ve	v	k7c6	v
Vlastě	Vlasta	k1gFnSc6	Vlasta
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
prý	prý	k9	prý
tehdy	tehdy	k6eAd1	tehdy
jeho	jeho	k3xOp3gNnPc4	jeho
slova	slovo	k1gNnPc4	slovo
překroutili	překroutit	k5eAaPmAgMnP	překroutit
<g/>
,	,	kIx,	,
a	a	k8xC	a
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
subkultura	subkultura	k1gFnSc1	subkultura
skinheads	skinheads	k6eAd1	skinheads
cítí	cítit	k5eAaImIp3nS	cítit
ke	k	k7c3	k
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
vydávala	vydávat	k5eAaImAgFnS	vydávat
redaktorka	redaktorka	k1gFnSc1	redaktorka
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
vlastní	vlastní	k2eAgFnPc4d1	vlastní
úvahy	úvaha	k1gFnPc4	úvaha
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
kandidovat	kandidovat	k5eAaImF	kandidovat
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
volbě	volba	k1gFnSc6	volba
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
facebookovou	facebookový	k2eAgFnSc4d1	facebooková
iniciativu	iniciativa	k1gFnSc4	iniciativa
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
bez	bez	k7c2	bez
Franzova	Franzův	k2eAgNnSc2d1	Franzovo
vědomí	vědomí	k1gNnSc2	vědomí
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
vypadala	vypadat	k5eAaPmAgFnS	vypadat
jako	jako	k9	jako
recese	recese	k1gFnSc1	recese
<g/>
.	.	kIx.	.
</s>
<s>
Výzvu	výzva	k1gFnSc4	výzva
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
sbírat	sbírat	k5eAaImF	sbírat
podpisy	podpis	k1gInPc4	podpis
potřebné	potřebný	k2eAgInPc4d1	potřebný
ke	k	k7c3	k
kandidatuře	kandidatura	k1gFnSc3	kandidatura
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ochotný	ochotný	k2eAgMnSc1d1	ochotný
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
věnovat	věnovat	k5eAaImF	věnovat
naplno	naplno	k6eAd1	naplno
a	a	k8xC	a
se	s	k7c7	s
vší	všecek	k3xTgFnSc7	všecek
vážností	vážnost	k1gFnSc7	vážnost
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
by	by	kYmCp3nS	by
přijal	přijmout	k5eAaPmAgMnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
kandidovat	kandidovat	k5eAaImF	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
na	na	k7c4	na
dotaz	dotaz	k1gInSc4	dotaz
z	z	k7c2	z
legrace	legrace	k1gFnSc2	legrace
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Kandidaturu	kandidatura	k1gFnSc4	kandidatura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
prohlašoval	prohlašovat	k5eAaImAgInS	prohlašovat
za	za	k7c4	za
vážně	vážně	k6eAd1	vážně
míněnou	míněný	k2eAgFnSc4d1	míněná
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc4	kampaň
vedl	vést	k5eAaImAgInS	vést
výhradně	výhradně	k6eAd1	výhradně
přes	přes	k7c4	přes
internetové	internetový	k2eAgFnPc4d1	internetová
sociální	sociální	k2eAgFnPc4d1	sociální
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
spustila	spustit	k5eAaPmAgFnS	spustit
iniciativa	iniciativa	k1gFnSc1	iniciativa
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
prezidentem	prezident	k1gMnSc7	prezident
(	(	kIx(	(
<g/>
VFP	VFP	kA	VFP
<g/>
)	)	kIx)	)
předvolební	předvolební	k2eAgInSc4d1	předvolební
web	web	k1gInSc4	web
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
byly	být	k5eAaImAgInP	být
zároveň	zároveň	k6eAd1	zároveň
zveřejněny	zveřejnit	k5eAaPmNgInP	zveřejnit
základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
Franzovi	Franz	k1gMnSc6	Franz
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
životopis	životopis	k1gInSc4	životopis
a	a	k8xC	a
názory	názor	k1gInPc4	názor
a	a	k8xC	a
text	text	k1gInSc4	text
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
Franz	Franz	k1gInSc1	Franz
nakládal	nakládat	k5eAaImAgInS	nakládat
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
pravomocemi	pravomoc	k1gFnPc7	pravomoc
a	a	k8xC	a
jak	jak	k6eAd1	jak
vidí	vidět	k5eAaImIp3nS	vidět
úlohu	úloha	k1gFnSc4	úloha
prezidenta	prezident	k1gMnSc2	prezident
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
koncem	koncem	k7c2	koncem
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
podepsal	podepsat	k5eAaPmAgMnS	podepsat
petice	petice	k1gFnSc2	petice
podporující	podporující	k2eAgFnSc2d1	podporující
kandidatury	kandidatura	k1gFnSc2	kandidatura
několika	několik	k4yIc2	několik
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
demokratické	demokratický	k2eAgNnSc4d1	demokratické
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
napomohl	napomoct	k5eAaPmAgMnS	napomoct
rovným	rovný	k2eAgMnSc7d1	rovný
šancím	šance	k1gFnPc3	šance
v	v	k7c6	v
prezidentské	prezidentský	k2eAgFnSc6d1	prezidentská
volbě	volba	k1gFnSc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Podpořil	podpořit	k5eAaPmAgInS	podpořit
podle	podle	k7c2	podle
Novinek	novinka	k1gFnPc2	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgMnPc4	všechen
uchazeče	uchazeč	k1gMnPc4	uchazeč
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
například	například	k6eAd1	například
Slávka	Slávek	k1gMnSc4	Slávek
Popelku	Popelka	k1gMnSc4	Popelka
<g/>
,	,	kIx,	,
Ladislava	Ladislav	k1gMnSc4	Ladislav
Jakla	Jakl	k1gMnSc4	Jakl
<g/>
,	,	kIx,	,
Janu	Jana	k1gFnSc4	Jana
Bobošíkovou	Bobošíková	k1gFnSc4	Bobošíková
či	či	k8xC	či
Kláru	Klára	k1gFnSc4	Klára
Samkovou	Samková	k1gFnSc4	Samková
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podpořit	podpořit	k5eAaPmF	podpořit
kandidaturu	kandidatura	k1gFnSc4	kandidatura
antikomunisty	antikomunista	k1gMnSc2	antikomunista
Petra	Petr	k1gMnSc2	Petr
Cibulky	Cibulka	k1gMnSc2	Cibulka
<g/>
,	,	kIx,	,
předsedy	předseda	k1gMnSc2	předseda
DSSS	DSSS	kA	DSSS
Tomáše	Tomáš	k1gMnSc2	Tomáš
Vandase	Vandas	k1gInSc6	Vandas
a	a	k8xC	a
vědmy	vědma	k1gFnSc2	vědma
<g/>
,	,	kIx,	,
kartářky	kartářka	k1gFnSc2	kartářka
a	a	k8xC	a
léčitelky	léčitelka	k1gFnSc2	léčitelka
Jany	Jana	k1gFnSc2	Jana
Lysoňkové	Lysoňková	k1gFnSc2	Lysoňková
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgFnPc6	který
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
nechce	chtít	k5eNaImIp3nS	chtít
mít	mít	k5eAaImF	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Gesto	gesto	k1gNnSc1	gesto
podpory	podpora	k1gFnSc2	podpora
více	hodně	k6eAd2	hodně
kandidátů	kandidát	k1gMnPc2	kandidát
ocenil	ocenit	k5eAaPmAgMnS	ocenit
například	například	k6eAd1	například
sociolog	sociolog	k1gMnSc1	sociolog
Jan	Jan	k1gMnSc1	Jan
Hartl	Hartl	k1gMnSc1	Hartl
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
STEM	sto	k4xCgNnSc7	sto
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgInSc2	jenž
Franzova	Franzův	k2eAgFnSc1d1	Franzova
podpora	podpora	k1gFnSc1	podpora
ostatních	ostatní	k2eAgMnPc2d1	ostatní
kandidátů	kandidát	k1gMnPc2	kandidát
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
smyslu	smysl	k1gInSc3	smysl
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
výrazem	výraz	k1gInSc7	výraz
demokratického	demokratický	k2eAgNnSc2d1	demokratické
smýšlení	smýšlení	k1gNnSc2	smýšlení
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
politici	politik	k1gMnPc1	politik
prý	prý	k9	prý
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
hlouposti	hloupost	k1gFnSc3	hloupost
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
podpis	podpis	k1gInSc1	podpis
na	na	k7c6	na
petiční	petiční	k2eAgFnSc6d1	petiční
listině	listina	k1gFnSc6	listina
ještě	ještě	k9	ještě
neznamená	znamenat	k5eNaImIp3nS	znamenat
automaticky	automaticky	k6eAd1	automaticky
volbu	volba	k1gFnSc4	volba
daného	daný	k2eAgMnSc4d1	daný
kandidáta	kandidát	k1gMnSc4	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Herzmann	Herzmann	k1gMnSc1	Herzmann
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
PPM	PPM	kA	PPM
Factum	Factum	k1gNnSc1	Factum
označil	označit	k5eAaPmAgInS	označit
Franzův	Franzův	k2eAgInSc1d1	Franzův
postup	postup	k1gInSc1	postup
za	za	k7c4	za
chytrý	chytrý	k2eAgInSc4d1	chytrý
marketingový	marketingový	k2eAgInSc4d1	marketingový
tah	tah	k1gInSc4	tah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
určen	určit	k5eAaPmNgInS	určit
především	především	k9	především
politicky	politicky	k6eAd1	politicky
zainteresovaným	zainteresovaný	k2eAgMnPc3d1	zainteresovaný
intelektuálům	intelektuál	k1gMnPc3	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
Organizátoři	organizátor	k1gMnPc1	organizátor
kampaně	kampaň	k1gFnSc2	kampaň
vydávali	vydávat	k5eAaImAgMnP	vydávat
magazín	magazín	k1gInSc4	magazín
Franzin	Franzina	k1gFnPc2	Franzina
a	a	k8xC	a
objížděli	objíždět	k5eAaImAgMnP	objíždět
republiku	republika	k1gFnSc4	republika
v	v	k7c6	v
limuzíně	limuzína	k1gFnSc6	limuzína
Air	Air	k1gMnSc1	Air
Franz	Franz	k1gMnSc1	Franz
One	One	k1gMnSc1	One
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
název	název	k1gInSc1	název
parafrázuje	parafrázovat	k5eAaBmIp3nS	parafrázovat
volací	volací	k2eAgInSc1d1	volací
znak	znak	k1gInSc1	znak
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
One	One	k1gFnSc2	One
letounu	letoun	k1gInSc2	letoun
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
i	i	k8xC	i
název	název	k1gInSc1	název
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Slovně	slovně	k6eAd1	slovně
jej	on	k3xPp3gMnSc4	on
podpořili	podpořit	k5eAaPmAgMnP	podpořit
například	například	k6eAd1	například
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
tanečník	tanečník	k1gMnSc1	tanečník
Ondřej	Ondřej	k1gMnSc1	Ondřej
Havelka	Havelka	k1gMnSc1	Havelka
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Miloš	Miloš	k1gMnSc1	Miloš
Urban	Urban	k1gMnSc1	Urban
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Jan	Jan	k1gMnSc1	Jan
Hřebejk	Hřebejk	k1gMnSc1	Hřebejk
<g/>
.	.	kIx.	.
</s>
<s>
Finančně	finančně	k6eAd1	finančně
jej	on	k3xPp3gMnSc4	on
podpořili	podpořit	k5eAaPmAgMnP	podpořit
zejména	zejména	k9	zejména
drobní	drobný	k2eAgMnPc1d1	drobný
dárci	dárce	k1gMnPc1	dárce
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
(	(	kIx(	(
<g/>
20	[number]	k4	20
tisíc	tisíc	k4xCgInSc4	tisíc
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
dal	dát	k5eAaPmAgMnS	dát
Václav	Václav	k1gMnSc1	Václav
Dejcmar	Dejcmar	k1gMnSc1	Dejcmar
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Franz	Franz	k1gMnSc1	Franz
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
přispěl	přispět	k5eAaPmAgMnS	přispět
10	[number]	k4	10
tisíci	tisíc	k4xCgInPc7	tisíc
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
volební	volební	k2eAgInSc4d1	volební
účet	účet	k1gInSc4	účet
asi	asi	k9	asi
285	[number]	k4	285
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgNnPc1d1	zahraniční
média	médium	k1gNnPc1	médium
se	se	k3xPyFc4	se
pozastavují	pozastavovat	k5eAaImIp3nP	pozastavovat
zejména	zejména	k9	zejména
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInSc7	jeho
netradičním	tradiční	k2eNgInSc7d1	netradiční
zjevem	zjev	k1gInSc7	zjev
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
považují	považovat	k5eAaImIp3nP	považovat
u	u	k7c2	u
studovaného	studovaný	k2eAgMnSc2d1	studovaný
člověka	člověk	k1gMnSc2	člověk
za	za	k7c4	za
kuriozitu	kuriozita	k1gFnSc4	kuriozita
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgInSc1d1	polský
server	server	k1gInSc1	server
tokfm	tokfma	k1gFnPc2	tokfma
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
si	se	k3xPyFc3	se
položil	položit	k5eAaPmAgMnS	položit
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
kandidatura	kandidatura	k1gFnSc1	kandidatura
není	být	k5eNaImIp3nS	být
provokací	provokace	k1gFnSc7	provokace
nebo	nebo	k8xC	nebo
marketingovým	marketingový	k2eAgInSc7d1	marketingový
podvodem	podvod	k1gInSc7	podvod
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Český	český	k2eAgInSc1d1	český
sen	sen	k1gInSc1	sen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Josefa	Josef	k1gMnSc2	Josef
Mlejnka	Mlejnek	k1gMnSc2	Mlejnek
jr	jr	k?	jr
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
nejzajímavějším	zajímavý	k2eAgMnSc7d3	nejzajímavější
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
inspirativním	inspirativní	k2eAgMnSc7d1	inspirativní
kandidátem	kandidát	k1gMnSc7	kandidát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
již	již	k6eAd1	již
jen	jen	k9	jen
svou	svůj	k3xOyFgFnSc7	svůj
vizáží	vizáž	k1gFnSc7	vizáž
nastoluje	nastolovat	k5eAaImIp3nS	nastolovat
otázku	otázka	k1gFnSc4	otázka
vztahu	vztah	k1gInSc2	vztah
vnějšku	vnějšek	k1gInSc2	vnějšek
a	a	k8xC	a
vnitřku	vnitřek	k1gInSc2	vnitřek
<g/>
,	,	kIx,	,
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
připomíná	připomínat	k5eAaImIp3nS	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
úřad	úřad	k1gInSc1	úřad
prezidenta	prezident	k1gMnSc2	prezident
je	být	k5eAaImIp3nS	být
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
virtuální	virtuální	k2eAgFnSc7d1	virtuální
realitou	realita	k1gFnSc7	realita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
lidé	člověk	k1gMnPc1	člověk
především	především	k9	především
promítají	promítat	k5eAaImIp3nP	promítat
různé	různý	k2eAgFnPc1d1	různá
emoce	emoce	k1gFnPc1	emoce
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
tedy	tedy	k8xC	tedy
panem	pan	k1gMnSc7	pan
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
institucí	instituce	k1gFnSc7	instituce
zčásti	zčásti	k6eAd1	zčásti
pohádkovou	pohádkový	k2eAgFnSc7d1	pohádková
<g/>
,	,	kIx,	,
ba	ba	k9	ba
skoro	skoro	k6eAd1	skoro
nadpřirozenou	nadpřirozený	k2eAgFnSc4d1	nadpřirozená
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentní	parlamentní	k2eAgInPc1d1	parlamentní
listy	list	k1gInPc1	list
oslovily	oslovit	k5eAaPmAgInP	oslovit
mediálního	mediální	k2eAgMnSc2d1	mediální
experta	expert	k1gMnSc2	expert
Karla	Karel	k1gMnSc2	Karel
Hvížďalu	Hvížďal	k1gInSc2	Hvížďal
<g/>
,	,	kIx,	,
politologa	politolog	k1gMnSc2	politolog
Jiřího	Jiří	k1gMnSc2	Jiří
Koubka	Koubek	k1gMnSc2	Koubek
a	a	k8xC	a
psychiatra	psychiatr	k1gMnSc2	psychiatr
Jana	Jan	k1gMnSc2	Jan
Cimického	Cimický	k2eAgMnSc2d1	Cimický
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zhodnotili	zhodnotit	k5eAaPmAgMnP	zhodnotit
jeho	jeho	k3xOp3gFnPc4	jeho
šance	šance	k1gFnPc4	šance
proti	proti	k7c3	proti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
kandidátům	kandidát	k1gMnPc3	kandidát
a	a	k8xC	a
pokusili	pokusit	k5eAaPmAgMnP	pokusit
se	se	k3xPyFc4	se
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jeho	jeho	k3xOp3gFnSc4	jeho
osobu	osoba	k1gFnSc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Politolog	politolog	k1gMnSc1	politolog
Jiří	Jiří	k1gMnSc1	Jiří
Koubek	Koubek	k1gMnSc1	Koubek
ho	on	k3xPp3gNnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
bezpochyby	bezpochyby	k6eAd1	bezpochyby
za	za	k7c4	za
excentrika	excentrik	k1gMnSc4	excentrik
a	a	k8xC	a
nedal	dát	k5eNaPmAgMnS	dát
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
žádné	žádný	k3yNgFnSc2	žádný
naděje	naděje	k1gFnSc2	naděje
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
vzezření	vzezření	k1gNnSc4	vzezření
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hvížďaly	Hvížďala	k1gFnSc2	Hvížďala
není	být	k5eNaImIp3nS	být
Franzova	Franzův	k2eAgFnSc1d1	Franzova
kandidatura	kandidatura	k1gFnSc1	kandidatura
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgMnSc4d1	jiný
než	než	k8xS	než
pouhá	pouhý	k2eAgFnSc1d1	pouhá
recese	recese	k1gFnSc1	recese
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hudebního	hudební	k2eAgMnSc2d1	hudební
publicisty	publicista	k1gMnSc2	publicista
Petra	Petr	k1gMnSc2	Petr
Žantovského	Žantovský	k1gMnSc2	Žantovský
byl	být	k5eAaImAgInS	být
Franz	Franz	k1gInSc1	Franz
vždy	vždy	k6eAd1	vždy
napřažen	napřáhnout	k5eAaPmNgInS	napřáhnout
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
kariéře	kariéra	k1gFnSc3	kariéra
a	a	k8xC	a
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
kandidatura	kandidatura	k1gFnSc1	kandidatura
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
levný	levný	k2eAgInSc4d1	levný
způsob	způsob	k1gInSc4	způsob
zviditelnit	zviditelnit	k5eAaPmF	zviditelnit
se	se	k3xPyFc4	se
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
jako	jako	k8xC	jako
umělec	umělec	k1gMnSc1	umělec
"	"	kIx"	"
<g/>
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
prodat	prodat	k5eAaPmF	prodat
více	hodně	k6eAd2	hodně
svých	svůj	k3xOyFgFnPc2	svůj
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
získat	získat	k5eAaPmF	získat
další	další	k2eAgNnSc4d1	další
angažmá	angažmá	k1gNnSc4	angažmá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
důsledku	důsledek	k1gInSc6	důsledek
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
přestal	přestat	k5eAaPmAgInS	přestat
Magazín	magazín	k1gInSc1	magazín
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
publikovat	publikovat	k5eAaBmF	publikovat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
Franzovy	Franzův	k2eAgFnPc4d1	Franzova
glosy	glosa	k1gFnPc4	glosa
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
počet	počet	k1gInSc1	počet
Franzových	Franzův	k2eAgFnPc2d1	Franzova
zakázek	zakázka	k1gFnPc2	zakázka
pro	pro	k7c4	pro
divadla	divadlo	k1gNnPc4	divadlo
-	-	kIx~	-
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
až	až	k9	až
15	[number]	k4	15
na	na	k7c4	na
nulu	nula	k4xCgFnSc4	nula
-	-	kIx~	-
a	a	k8xC	a
od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
nebyl	být	k5eNaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
jediný	jediný	k2eAgInSc1d1	jediný
nosič	nosič	k1gInSc1	nosič
s	s	k7c7	s
Franzovou	Franzový	k2eAgFnSc7d1	Franzový
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Psychiatr	psychiatr	k1gMnSc1	psychiatr
Jan	Jan	k1gMnSc1	Jan
Cimický	Cimický	k2eAgInSc1d1	Cimický
k	k	k7c3	k
Franzovu	Franzův	k2eAgNnSc3d1	Franzovo
tetování	tetování	k1gNnSc3	tetování
těžko	těžko	k6eAd1	těžko
hledal	hledat	k5eAaImAgInS	hledat
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nehledal	hledat	k5eNaImAgInS	hledat
by	by	kYmCp3nS	by
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
žádnou	žádný	k3yNgFnSc4	žádný
diagnózu	diagnóza	k1gFnSc4	diagnóza
a	a	k8xC	a
Franze	Franze	k1gFnSc1	Franze
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
velice	velice	k6eAd1	velice
vzdělaného	vzdělaný	k2eAgMnSc4d1	vzdělaný
a	a	k8xC	a
kultivovaného	kultivovaný	k2eAgMnSc4d1	kultivovaný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
a	a	k8xC	a
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
srpna	srpen	k1gInSc2	srpen
objevují	objevovat	k5eAaImIp3nP	objevovat
zčásti	zčásti	k6eAd1	zčásti
recesistické	recesistický	k2eAgInPc1d1	recesistický
soupisy	soupis	k1gInPc1	soupis
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
Franz	Franz	k1gInSc1	Franz
měl	mít	k5eAaImAgInS	mít
či	či	k8xC	či
neměl	mít	k5eNaImAgInS	mít
být	být	k5eAaImF	být
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Blesk	blesk	k1gInSc1	blesk
sestavil	sestavit	k5eAaPmAgInS	sestavit
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
podle	podle	k7c2	podle
reakcí	reakce	k1gFnPc2	reakce
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
,	,	kIx,	,
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
pětici	pětice	k1gFnSc4	pětice
důvodů	důvod	k1gInPc2	důvod
pro	pro	k7c4	pro
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
modřejší	modrý	k2eAgMnSc1d2	modřejší
než	než	k8xS	než
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
,	,	kIx,	,
nekrade	krást	k5eNaImIp3nS	krást
tužky	tužka	k1gFnPc4	tužka
<g/>
,	,	kIx,	,
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
holinky	holinky	k?	holinky
<g/>
,	,	kIx,	,
konečně	konečně	k6eAd1	konečně
jeden	jeden	k4xCgInSc1	jeden
slušnej	slušnej	k?	slušnej
kandidát	kandidát	k1gMnSc1	kandidát
hodící	hodící	k2eAgMnSc1d1	hodící
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
republice	republika	k1gFnSc3	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
a	a	k8xC	a
proti	proti	k7c3	proti
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
napojen	napojit	k5eAaPmNgInS	napojit
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
Orlík	Orlík	k1gInSc1	Orlík
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
bychom	by	kYmCp1nP	by
pro	pro	k7c4	pro
smích	smích	k1gInSc1	smích
celému	celý	k2eAgInSc3d1	celý
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
premiérem	premiér	k1gMnSc7	premiér
<g/>
,	,	kIx,	,
copak	copak	k6eAd1	copak
jsme	být	k5eAaImIp1nP	být
národ	národ	k1gInSc4	národ
Indiánů	Indián	k1gMnPc2	Indián
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jak	jak	k8xS	jak
mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
snad	snad	k9	snad
raději	rád	k6eAd2	rád
Bechera	Becher	k1gMnSc4	Becher
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
body	bod	k1gInPc4	bod
Franz	Franza	k1gFnPc2	Franza
reagoval	reagovat	k5eAaBmAgInS	reagovat
v	v	k7c4	v
propagační	propagační	k2eAgInSc4d1	propagační
talk	talk	k1gInSc4	talk
show	show	k1gFnSc1	show
Honzy	Honza	k1gMnSc2	Honza
Dědka	Dědek	k1gMnSc2	Dědek
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2012	[number]	k4	2012
v	v	k7c6	v
Paláci	palác	k1gInSc6	palác
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizi	televize	k1gFnSc6	televize
Prima	prima	k6eAd1	prima
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
výčet	výčet	k1gInSc4	výčet
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
narazil	narazit	k5eAaPmAgMnS	narazit
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
talk	talk	k6eAd1	talk
show	show	k1gNnPc1	show
s	s	k7c7	s
Franzem	Franz	k1gMnSc7	Franz
jako	jako	k8xS	jako
hostem	host	k1gMnSc7	host
<g/>
.	.	kIx.	.
</s>
<s>
Blog	Blog	k1gMnSc1	Blog
Automotive	Automotiv	k1gInSc5	Automotiv
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
10	[number]	k4	10
důvodů	důvod	k1gInPc2	důvod
pro	pro	k7c4	pro
zvolení	zvolení	k1gNnSc4	zvolení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
spontánnost	spontánnost	k1gFnSc1	spontánnost
iniciativy	iniciativa	k1gFnSc2	iniciativa
za	za	k7c4	za
zvolení	zvolení	k1gNnSc4	zvolení
<g/>
,	,	kIx,	,
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
postoje	postoj	k1gInPc4	postoj
demonstrující	demonstrující	k2eAgFnSc4d1	demonstrující
toleranci	tolerance	k1gFnSc4	tolerance
k	k	k7c3	k
odlišnosti	odlišnost	k1gFnSc3	odlišnost
<g/>
,	,	kIx,	,
právnické	právnický	k2eAgNnSc1d1	právnické
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
odstup	odstup	k1gInSc1	odstup
od	od	k7c2	od
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
silových	silový	k2eAgNnPc6d1	silové
uskupeních	uskupení	k1gNnPc6	uskupení
a	a	k8xC	a
politických	politický	k2eAgFnPc6d1	politická
lobby	lobby	k1gFnPc6	lobby
<g/>
,	,	kIx,	,
zkušenosti	zkušenost	k1gFnPc1	zkušenost
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
bulvárem	bulvár	k1gInSc7	bulvár
a	a	k8xC	a
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
pochopení	pochopení	k1gNnSc1	pochopení
pro	pro	k7c4	pro
situaci	situace	k1gFnSc4	situace
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
nabídka	nabídka	k1gFnSc1	nabídka
alternativních	alternativní	k2eAgNnPc2d1	alternativní
řešení	řešení	k1gNnPc2	řešení
a	a	k8xC	a
odmítání	odmítání	k1gNnSc1	odmítání
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
extremismu	extremismus	k1gInSc2	extremismus
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgNnSc1d1	renesanční
propojení	propojení	k1gNnSc1	propojení
sfér	sféra	k1gFnPc2	sféra
společnosti	společnost	k1gFnSc2	společnost
-	-	kIx~	-
zvládnutí	zvládnutí	k1gNnSc3	zvládnutí
umělecké	umělecký	k2eAgFnSc2d1	umělecká
formy	forma	k1gFnSc2	forma
i	i	k8xC	i
právnické	právnický	k2eAgFnSc2d1	právnická
hantýrky	hantýrka	k1gFnSc2	hantýrka
<g/>
,	,	kIx,	,
životní	životní	k2eAgFnSc4d1	životní
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
hmotná	hmotný	k2eAgFnSc1d1	hmotná
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
větru	vítr	k1gInSc2	vítr
<g/>
"	"	kIx"	"
k	k	k7c3	k
vymetení	vymetení	k1gNnSc3	vymetení
politických	politický	k2eAgInPc2d1	politický
chlévů	chlév	k1gInPc2	chlév
od	od	k7c2	od
přetvářky	přetvářka	k1gFnSc2	přetvářka
a	a	k8xC	a
klamu	klam	k1gInSc2	klam
<g/>
.	.	kIx.	.
13	[number]	k4	13
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Franz	Franz	k1gInSc1	Franz
bude	být	k5eAaImBp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
i	i	k8xC	i
internetový	internetový	k2eAgInSc1d1	internetový
magazín	magazín	k1gInSc1	magazín
iHub	iHuba	k1gFnPc2	iHuba
-	-	kIx~	-
mezi	mezi	k7c7	mezi
novými	nový	k2eAgInPc7d1	nový
důvody	důvod	k1gInPc7	důvod
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládne	vládnout	k5eAaImIp3nS	vládnout
internetu	internet	k1gInSc2	internet
a	a	k8xC	a
Facebooku	Facebook	k1gInSc2	Facebook
či	či	k8xC	či
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
a	a	k8xC	a
že	že	k8xS	že
za	za	k7c2	za
komunistů	komunista	k1gMnPc2	komunista
pracoval	pracovat	k5eAaImAgMnS	pracovat
raději	rád	k6eAd2	rád
i	i	k9	i
manuálně	manuálně	k6eAd1	manuálně
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
podporoval	podporovat	k5eAaImAgMnS	podporovat
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nepovažoval	považovat	k5eNaImAgMnS	považovat
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
nápadná	nápadný	k2eAgFnSc1d1	nápadná
odlišnost	odlišnost	k1gFnSc1	odlišnost
přispěje	přispět	k5eAaPmIp3nS	přispět
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
X.	X.	kA	X.
Doležal	Doležal	k1gMnSc1	Doležal
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
v	v	k7c6	v
Reflexu	reflex	k1gInSc6	reflex
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgMnSc4	jenž
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
získal	získat	k5eAaPmAgMnS	získat
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
možnost	možnost	k1gFnSc4	možnost
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
zúčastněného	zúčastněný	k2eAgMnSc2d1	zúčastněný
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
nastavit	nastavit	k5eAaPmF	nastavit
celé	celý	k2eAgFnSc3d1	celá
prezidentské	prezidentský	k2eAgFnSc3d1	prezidentská
kampani	kampaň	k1gFnSc3	kampaň
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
ukázat	ukázat	k5eAaPmF	ukázat
prázdnotu	prázdnota	k1gFnSc4	prázdnota
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
dutost	dutost	k1gFnSc1	dutost
slibů	slib	k1gInPc2	slib
a	a	k8xC	a
trapnost	trapnost	k1gFnSc4	trapnost
sebepodbízení	sebepodbízení	k1gNnSc2	sebepodbízení
ostatních	ostatní	k2eAgInPc2d1	ostatní
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
registraci	registrace	k1gFnSc6	registrace
vzal	vzít	k5eAaPmAgInS	vzít
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
vážně	vážně	k6eAd1	vážně
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
se	se	k3xPyFc4	se
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
Hyde	Hyde	k1gFnSc1	Hyde
Park	park	k1gInSc1	park
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
jako	jako	k9	jako
karikatura	karikatura	k1gFnSc1	karikatura
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
až	až	k9	až
groteskní	groteskní	k2eAgNnSc4d1	groteskní
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
politického	politický	k2eAgInSc2d1	politický
diletantismu	diletantismus	k1gInSc2	diletantismus
<g/>
,	,	kIx,	,
amatérismu	amatérismus	k1gInSc2	amatérismus
<g/>
,	,	kIx,	,
neinformovanosti	neinformovanost	k1gFnSc2	neinformovanost
a	a	k8xC	a
nepochopení	nepochopení	k1gNnSc2	nepochopení
<g/>
,	,	kIx,	,
ničí	ničit	k5eAaImIp3nS	ničit
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
desetiletí	desetiletí	k1gNnPc4	desetiletí
budovaný	budovaný	k2eAgInSc4d1	budovaný
kredit	kredit	k1gInSc4	kredit
solidního	solidní	k2eAgMnSc2d1	solidní
skladatele	skladatel	k1gMnSc2	skladatel
a	a	k8xC	a
profesora	profesor	k1gMnSc2	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklady	příklad	k1gInPc1	příklad
Doležal	Doležal	k1gMnSc1	Doležal
uváděl	uvádět	k5eAaImAgInS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Franz	Franz	k1gMnSc1	Franz
až	až	k9	až
po	po	k7c4	po
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
moderátorky	moderátorka	k1gFnSc2	moderátorka
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
progresivní	progresivní	k2eAgNnSc4d1	progresivní
zdanění	zdanění	k1gNnSc4	zdanění
<g/>
,	,	kIx,	,
či	či	k8xC	či
že	že	k8xS	že
zároveň	zároveň	k6eAd1	zároveň
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
odlukou	odluka	k1gFnSc7	odluka
církve	církev	k1gFnSc2	církev
od	od	k7c2	od
státu	stát	k1gInSc2	stát
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
dohled	dohled	k1gInSc4	dohled
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
církve	církev	k1gFnPc1	církev
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
majetkem	majetek	k1gInSc7	majetek
nakládají	nakládat	k5eAaImIp3nP	nakládat
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
X.	X.	kA	X.
Doležal	Doležal	k1gMnSc1	Doležal
má	mít	k5eAaImIp3nS	mít
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Franze	Franze	k1gFnSc2	Franze
velmi	velmi	k6eAd1	velmi
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opravdu	opravdu	k6eAd1	opravdu
si	se	k3xPyFc3	se
nepřeje	přát	k5eNaImIp3nS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
čestný	čestný	k2eAgMnSc1d1	čestný
<g/>
,	,	kIx,	,
nezkompromitovaný	zkompromitovaný	k2eNgMnSc1d1	nezkompromitovaný
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
tvůrčí	tvůrčí	k2eAgMnSc1d1	tvůrčí
politický	politický	k2eAgMnSc1d1	politický
diletant	diletant	k1gMnSc1	diletant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předvolebních	předvolební	k2eAgInPc6d1	předvolební
anketních	anketní	k2eAgInPc6d1	anketní
průzkumech	průzkum	k1gInPc6	průzkum
<g/>
,	,	kIx,	,
uvádějících	uvádějící	k2eAgInPc2d1	uvádějící
většinou	většinou	k6eAd1	většinou
kolem	kolem	k7c2	kolem
deseti	deset	k4xCc2	deset
nejnadějnějších	nadějný	k2eAgMnPc2d3	nejnadějnější
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
září	září	k1gNnSc4	září
2012	[number]	k4	2012
vůbec	vůbec	k9	vůbec
neobjevoval	objevovat	k5eNaImAgMnS	objevovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
PPM	PPM	kA	PPM
Factum	Factum	k1gNnSc1	Factum
za	za	k7c7	za
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
s	s	k7c7	s
6,6	[number]	k4	6,6
%	%	kIx~	%
rovnou	rovnou	k6eAd1	rovnou
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
měsících	měsíc	k1gInPc6	měsíc
získal	získat	k5eAaPmAgInS	získat
4,5	[number]	k4	4,5
%	%	kIx~	%
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
5,6	[number]	k4	5,6
%	%	kIx~	%
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumech	průzkum	k1gInPc6	průzkum
agentury	agentura	k1gFnSc2	agentura
Median	Median	k1gInSc1	Median
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
i	i	k8xC	i
v	v	k7c6	v
září	září	k1gNnSc6	září
1,5	[number]	k4	1,5
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
agentury	agentura	k1gFnSc2	agentura
Sanep	Sanep	k1gMnSc1	Sanep
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
2,6	[number]	k4	2,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
deník	deník	k1gInSc1	deník
iHNed	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
facebookových	facebookový	k2eAgMnPc2d1	facebookový
příznivců	příznivec	k1gMnPc2	příznivec
je	být	k5eAaImIp3nS	být
Franz	Franz	k1gInSc4	Franz
druhý	druhý	k4xOgInSc4	druhý
z	z	k7c2	z
deseti	deset	k4xCc2	deset
kandidátů	kandidát	k1gMnPc2	kandidát
a	a	k8xC	a
prvního	první	k4xOgMnSc2	první
Karla	Karel	k1gMnSc2	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
rychle	rychle	k6eAd1	rychle
dohání	dohánět	k5eAaImIp3nS	dohánět
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
referoval	referovat	k5eAaBmAgInS	referovat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
i	i	k9	i
deník	deník	k1gInSc4	deník
Blesk	blesk	k1gInSc1	blesk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
webu	web	k1gInSc2	web
Blesk	blesk	k1gInSc1	blesk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
000	[number]	k4	000
hlasujících	hlasující	k1gMnPc2	hlasující
<g/>
,	,	kIx,	,
se	s	k7c7	s
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
s	s	k7c7	s
13	[number]	k4	13
%	%	kIx~	%
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
Janem	Jan	k1gMnSc7	Jan
Fischerem	Fischer	k1gMnSc7	Fischer
a	a	k8xC	a
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dvoukolové	dvoukolový	k2eAgFnSc6d1	dvoukolová
anketě	anketa	k1gFnSc6	anketa
internetové	internetový	k2eAgFnSc2d1	internetová
verze	verze	k1gFnSc2	verze
týdeníku	týdeník	k1gInSc2	týdeník
Reflex	reflex	k1gInSc1	reflex
ve	v	k7c6	v
dnech	den	k1gInPc6	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
35	[number]	k4	35
%	%	kIx~	%
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
7416	[number]	k4	7416
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgMnS	následovat
jej	on	k3xPp3gMnSc4	on
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
s	s	k7c7	s
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
získal	získat	k5eAaPmAgMnS	získat
7720	[number]	k4	7720
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
68	[number]	k4	68
%	%	kIx~	%
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
11	[number]	k4	11
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
portálu	portál	k1gInSc2	portál
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
16725	[number]	k4	16725
(	(	kIx(	(
<g/>
26,1	[number]	k4	26,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
z	z	k7c2	z
64031	[number]	k4	64031
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgMnS	následovat
ho	on	k3xPp3gMnSc4	on
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
s	s	k7c7	s
16,3	[number]	k4	16,3
%	%	kIx~	%
a	a	k8xC	a
společně	společně	k6eAd1	společně
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
ankety	anketa	k1gFnSc2	anketa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
ankety	anketa	k1gFnSc2	anketa
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
s	s	k7c7	s
55,7	[number]	k4	55,7
%	%	kIx~	%
(	(	kIx(	(
<g/>
18	[number]	k4	18
895	[number]	k4	895
<g/>
)	)	kIx)	)
hlasů	hlas	k1gInPc2	hlas
nad	nad	k7c7	nad
Karlem	Karel	k1gMnSc7	Karel
Schwarzenbergem	Schwarzenberg	k1gMnSc7	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takzvaných	takzvaný	k2eAgFnPc6d1	takzvaná
studentských	studentský	k2eAgFnPc6d1	studentská
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
dnech	den	k1gInPc6	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výchovy	výchova	k1gFnSc2	výchova
k	k	k7c3	k
občanství	občanství	k1gNnSc3	občanství
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
Jeden	jeden	k4xCgInSc1	jeden
svět	svět	k1gInSc1	svět
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
na	na	k7c6	na
400	[number]	k4	400
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
připojily	připojit	k5eAaPmAgInP	připojit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
61	[number]	k4	61
499	[number]	k4	499
studentů	student	k1gMnPc2	student
starších	starý	k2eAgMnPc2d2	starší
15	[number]	k4	15
let	let	k1gInSc4	let
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
s	s	k7c7	s
40,7	[number]	k4	40,7
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
jej	on	k3xPp3gInSc2	on
následovali	následovat	k5eAaImAgMnP	následovat
Jan	Jan	k1gMnSc1	Jan
Fischer	Fischer	k1gMnSc1	Fischer
(	(	kIx(	(
<g/>
19,4	[number]	k4	19,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
(	(	kIx(	(
<g/>
14,6	[number]	k4	14,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prohlášení	prohlášení	k1gNnSc6	prohlášení
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
své	svůj	k3xOyFgFnSc2	svůj
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
prezident	prezident	k1gMnSc1	prezident
jednotícím	jednotící	k2eAgInSc7d1	jednotící
prvkem	prvek	k1gInSc7	prvek
české	český	k2eAgFnSc2d1	Česká
politiky	politika	k1gFnSc2	politika
i	i	k8xC	i
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
ani	ani	k8xC	ani
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
autorem	autor	k1gMnSc7	autor
a	a	k8xC	a
nositelem	nositel	k1gMnSc7	nositel
nějaké	nějaký	k3yIgFnSc2	nějaký
vlastní	vlastní	k2eAgFnSc2d1	vlastní
a	a	k8xC	a
na	na	k7c6	na
výkonné	výkonný	k2eAgFnSc6d1	výkonná
moci	moc	k1gFnSc6	moc
zcela	zcela	k6eAd1	zcela
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
<g/>
,	,	kIx,	,
environmentální	environmentální	k2eAgFnSc2d1	environmentální
<g/>
,	,	kIx,	,
natož	natož	k6eAd1	natož
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podporovat	podporovat	k5eAaImF	podporovat
iniciativy	iniciativa	k1gFnSc2	iniciativa
směřující	směřující	k2eAgFnSc2d1	směřující
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
života	život	k1gInSc2	život
a	a	k8xC	a
odmítat	odmítat	k5eAaImF	odmítat
negativní	negativní	k2eAgInPc4d1	negativní
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
vzdělanost	vzdělanost	k1gFnSc4	vzdělanost
<g/>
,	,	kIx,	,
kulturnost	kulturnost	k1gFnSc4	kulturnost
a	a	k8xC	a
toleranci	tolerance	k1gFnSc4	tolerance
<g/>
,	,	kIx,	,
zodpovědnost	zodpovědnost	k1gFnSc1	zodpovědnost
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
lidský	lidský	k2eAgInSc4d1	lidský
rozměr	rozměr	k1gInSc4	rozměr
funkce	funkce	k1gFnSc1	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc4d1	dobrá
vůli	vůle	k1gFnSc4	vůle
a	a	k8xC	a
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kampaně	kampaň	k1gFnSc2	kampaň
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pravicového	pravicový	k2eAgInSc2d1	pravicový
extrémismu	extrémismus	k1gInSc2	extrémismus
a	a	k8xC	a
populismu	populismus	k1gInSc2	populismus
děsí	děsit	k5eAaImIp3nS	děsit
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
současné	současný	k2eAgFnSc2d1	současná
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ohlášení	ohlášení	k1gNnSc6	ohlášení
své	svůj	k3xOyFgFnSc2	svůj
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kandidatury	kandidatura	k1gFnSc2	kandidatura
na	na	k7c4	na
chatu	chata	k1gFnSc4	chata
TN	TN	kA	TN
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
televize	televize	k1gFnSc1	televize
Nova	nova	k1gFnSc1	nova
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
cesta	cesta	k1gFnSc1	cesta
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zvolení	zvolení	k1gNnSc2	zvolení
by	by	kYmCp3nS	by
asi	asi	k9	asi
byla	být	k5eAaImAgFnS	být
tradičně	tradičně	k6eAd1	tradičně
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgMnPc1d1	domácí
k	k	k7c3	k
hrobům	hrob	k1gInPc3	hrob
Tomáše	Tomáš	k1gMnSc4	Tomáš
Garrigue	Garrigu	k1gMnSc2	Garrigu
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
státní	státní	k2eAgFnSc3d1	státní
hymně	hymna	k1gFnSc3	hymna
by	by	kYmCp3nS	by
rád	rád	k6eAd1	rád
připojoval	připojovat	k5eAaImAgInS	připojovat
Svatováclavský	svatováclavský	k2eAgInSc1d1	svatováclavský
chorál	chorál	k1gInSc1	chorál
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
angažovaností	angažovanost	k1gFnSc7	angažovanost
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
jako	jako	k9	jako
demokrat	demokrat	k1gMnSc1	demokrat
a	a	k8xC	a
humanista	humanista	k1gMnSc1	humanista
velký	velký	k2eAgInSc4d1	velký
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
prý	prý	k9	prý
bojí	bát	k5eAaImIp3nS	bát
pravicového	pravicový	k2eAgInSc2d1	pravicový
extremismu	extremismus	k1gInSc2	extremismus
a	a	k8xC	a
populismu	populismus	k1gInSc2	populismus
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
KSČM	KSČM	kA	KSČM
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
<g/>
)	)	kIx)	)
při	při	k7c6	při
debatě	debata	k1gFnSc6	debata
prezidentských	prezidentský	k2eAgMnPc2d1	prezidentský
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
Mladí	mladý	k2eAgMnPc1d1	mladý
konzervativci	konzervativec	k1gMnPc1	konzervativec
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
marasmu	marasmus	k1gInSc2	marasmus
a	a	k8xC	a
blbé	blbý	k2eAgFnSc2d1	blbá
nálady	nálada	k1gFnSc2	nálada
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
jasně	jasně	k6eAd1	jasně
odsoudit	odsoudit	k5eAaPmF	odsoudit
komunismus	komunismus	k1gInSc4	komunismus
a	a	k8xC	a
nevypořádala	vypořádat	k5eNaPmAgFnS	vypořádat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Norimberský	norimberský	k2eAgInSc1d1	norimberský
proces	proces	k1gInSc1	proces
s	s	k7c7	s
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
Pětka	pětka	k1gFnSc1	pětka
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ideologie	ideologie	k1gFnSc1	ideologie
komunistů	komunista	k1gMnPc2	komunista
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
nikde	nikde	k6eAd1	nikde
neosvědčila	osvědčit	k5eNaPmAgFnS	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
s	s	k7c7	s
komunisty	komunista	k1gMnPc7	komunista
by	by	kYmCp3nS	by
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
kvůli	kvůli	k7c3	kvůli
respektu	respekt	k1gInSc3	respekt
k	k	k7c3	k
výsledku	výsledek	k1gInSc3	výsledek
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
sice	sice	k8xC	sice
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
zaťatými	zaťatý	k2eAgInPc7d1	zaťatý
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
politické	politický	k2eAgFnPc4d1	politická
otázky	otázka	k1gFnPc4	otázka
často	často	k6eAd1	často
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
zeptal	zeptat	k5eAaPmAgMnS	zeptat
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
sporných	sporný	k2eAgFnPc2d1	sporná
otázek	otázka	k1gFnPc2	otázka
nabízí	nabízet	k5eAaImIp3nS	nabízet
nekonkrétní	konkrétní	k2eNgFnPc4d1	nekonkrétní
odpovědi	odpověď	k1gFnPc4	odpověď
<g/>
,	,	kIx,	,
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
poradu	porada	k1gFnSc4	porada
s	s	k7c7	s
odborníky	odborník	k1gMnPc7	odborník
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgMnPc7d1	jiný
státními	státní	k2eAgMnPc7d1	státní
činiteli	činitel	k1gMnPc7	činitel
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ministrem	ministr	k1gMnSc7	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
či	či	k8xC	či
financí	finance	k1gFnPc2	finance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
dialog	dialog	k1gInSc4	dialog
(	(	kIx(	(
<g/>
většinové	většinový	k2eAgFnPc1d1	většinová
společnosti	společnost	k1gFnPc1	společnost
s	s	k7c7	s
Romy	Rom	k1gMnPc7	Rom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
spíše	spíše	k9	spíše
proti	proti	k7c3	proti
kriminalizaci	kriminalizace	k1gFnSc3	kriminalizace
užívání	užívání	k1gNnSc2	užívání
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
(	(	kIx(	(
<g/>
za	za	k7c2	za
přisvědčování	přisvědčování	k1gNnSc2	přisvědčování
moderátora	moderátor	k1gMnSc2	moderátor
<g/>
)	)	kIx)	)
opakoval	opakovat	k5eAaImAgInS	opakovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
marihuana	marihuana	k1gFnSc1	marihuana
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
"	"	kIx"	"
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
kriminalizovat	kriminalizovat	k5eAaImF	kriminalizovat
přírodu	příroda	k1gFnSc4	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Mediální	mediální	k2eAgInSc4d1	mediální
ohlas	ohlas	k1gInSc4	ohlas
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
jeho	jeho	k3xOp3gFnSc1	jeho
podpora	podpora	k1gFnSc1	podpora
akci	akce	k1gFnSc4	akce
Prague	Praguus	k1gMnSc5	Praguus
Pride	Prid	k1gMnSc5	Prid
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
prý	prý	k9	prý
"	"	kIx"	"
<g/>
ani	ani	k8xC	ani
v	v	k7c6	v
nejmenším	malý	k2eAgMnSc6d3	nejmenší
nevadí	vadit	k5eNaImIp3nS	vadit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
homosexualita	homosexualita	k1gFnSc1	homosexualita
není	být	k5eNaImIp3nS	být
nemoc	nemoc	k1gFnSc4	nemoc
ani	ani	k8xC	ani
politický	politický	k2eAgInSc4d1	politický
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
danost	danost	k1gFnSc4	danost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c4	v
chatu	chata	k1gFnSc4	chata
na	na	k7c6	na
tn	tn	k?	tn
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
facebookovém	facebookový	k2eAgInSc6d1	facebookový
profilu	profil	k1gInSc6	profil
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
prohlášení	prohlášení	k1gNnSc4	prohlášení
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Prague	Pragu	k1gFnSc2	Pragu
Pride	Prid	k1gInSc5	Prid
a	a	k8xC	a
proti	proti	k7c3	proti
útokům	útok	k1gInPc3	útok
na	na	k7c6	na
konání	konání	k1gNnSc6	konání
pochodu	pochod	k1gInSc2	pochod
Prague	Praguus	k1gMnSc5	Praguus
Pride	Prid	k1gMnSc5	Prid
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
hůře	zle	k6eAd2	zle
či	či	k8xC	či
šikovněji	šikovně	k6eAd2	šikovně
skrývané	skrývaný	k2eAgInPc1d1	skrývaný
projevy	projev	k1gInPc1	projev
homofobie	homofobie	k1gFnSc2	homofobie
a	a	k8xC	a
vyvolávání	vyvolávání	k1gNnSc4	vyvolávání
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
jinakosti	jinakost	k1gFnSc2	jinakost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
iDnes	iDnesa	k1gFnPc2	iDnesa
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2012	[number]	k4	2012
Franz	Franza	k1gFnPc2	Franza
dosud	dosud	k6eAd1	dosud
ani	ani	k8xC	ani
nenaznačil	naznačit	k5eNaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgMnPc7	jaký
lidmi	člověk	k1gMnPc7	člověk
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
obklopil	obklopit	k5eAaPmAgMnS	obklopit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
filosofa	filosof	k1gMnSc2	filosof
a	a	k8xC	a
překladatele	překladatel	k1gMnSc2	překladatel
Petra	Petr	k1gMnSc2	Petr
Kurky	Kurka	k1gMnSc2	Kurka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
lidskou	lidský	k2eAgFnSc4d1	lidská
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
hudební	hudební	k2eAgNnSc4d1	hudební
a	a	k8xC	a
malířské	malířský	k2eAgNnSc4d1	malířské
dílo	dílo	k1gNnSc4	dílo
představuje	představovat	k5eAaImIp3nS	představovat
jednotu	jednota	k1gFnSc4	jednota
žitého	žitý	k2eAgInSc2d1	žitý
existenciálního	existenciální	k2eAgInSc2d1	existenciální
Gesamtkunstwerku	Gesamtkunstwerk	k1gInSc2	Gesamtkunstwerk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
vědomím	vědomí	k1gNnSc7	vědomí
závazku	závazek	k1gInSc2	závazek
<g/>
,	,	kIx,	,
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
a	a	k8xC	a
pokory	pokora	k1gFnSc2	pokora
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc2	druhý
však	však	k8xC	však
i	i	k9	i
výrazem	výraz	k1gInSc7	výraz
nevázaně	vázaně	k6eNd1	vázaně
extatického	extatický	k2eAgNnSc2d1	extatické
veselí	veselí	k1gNnSc2	veselí
<g/>
,	,	kIx,	,
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
radosti	radost	k1gFnSc2	radost
z	z	k7c2	z
formování	formování	k1gNnSc2	formování
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
jako	jako	k8xS	jako
svrchovaného	svrchovaný	k2eAgNnSc2d1	svrchované
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Kurky	Kurka	k1gMnSc2	Kurka
představuje	představovat	k5eAaImIp3nS	představovat
Franz	Franz	k1gMnSc1	Franz
nezkrotný	zkrotný	k2eNgInSc4d1	nezkrotný
živel	živel	k1gInSc4	živel
imaginace	imaginace	k1gFnSc2	imaginace
a	a	k8xC	a
touhy	touha	k1gFnSc2	touha
dát	dát	k5eAaPmF	dát
věcem	věc	k1gFnPc3	věc
přesný	přesný	k2eAgInSc4d1	přesný
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
kouzlo	kouzlo	k1gNnSc4	kouzlo
ozvláštnění	ozvláštnění	k1gNnPc2	ozvláštnění
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
reprezentantem	reprezentant	k1gInSc7	reprezentant
skutečně	skutečně	k6eAd1	skutečně
praktikované	praktikovaný	k2eAgFnSc2d1	praktikovaná
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
přirozeně	přirozeně	k6eAd1	přirozeně
zakořeněná	zakořeněný	k2eAgFnSc1d1	zakořeněná
v	v	k7c6	v
niterné	niterný	k2eAgFnSc6d1	niterná
potřebě	potřeba	k1gFnSc6	potřeba
jednoznačného	jednoznačný	k2eAgInSc2d1	jednoznačný
etického	etický	k2eAgInSc2d1	etický
postoje	postoj	k1gInSc2	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
překladatele	překladatel	k1gMnSc2	překladatel
Petra	Petr	k1gMnSc2	Petr
Onufera	Onufer	k1gMnSc2	Onufer
<g/>
,	,	kIx,	,
zveřejněného	zveřejněný	k2eAgInSc2d1	zveřejněný
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
na	na	k7c6	na
Bubínku	bubínek	k1gInSc6	bubínek
Revolveru	revolver	k1gInSc2	revolver
a	a	k8xC	a
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
Franzových	Franzův	k2eAgNnPc6d1	Franzovo
veřejných	veřejný	k2eAgNnPc6d1	veřejné
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
,	,	kIx,	,
někdejších	někdejší	k2eAgInPc2d1	někdejší
i	i	k8xC	i
dnešních	dnešní	k2eAgInPc2d1	dnešní
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnSc1	jeho
výzvách	výzva	k1gFnPc6	výzva
týkajících	týkající	k2eAgInPc6d1	týkající
se	se	k3xPyFc4	se
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
,	,	kIx,	,
kulturnosti	kulturnost	k1gFnSc2	kulturnost
a	a	k8xC	a
humanity	humanita	k1gFnSc2	humanita
vyjevuje	vyjevovat	k5eAaImIp3nS	vyjevovat
hystericky	hystericky	k6eAd1	hystericky
přehrávaná	přehrávaný	k2eAgFnSc1d1	přehrávaná
<g/>
,	,	kIx,	,
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
odiv	odiv	k1gInSc1	odiv
stavěná	stavěný	k2eAgFnSc1d1	stavěná
"	"	kIx"	"
<g/>
kulturnost	kulturnost	k1gFnSc1	kulturnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obskurní	obskurní	k2eAgInSc1d1	obskurní
národovecký	národovecký	k2eAgInSc1d1	národovecký
mysticismus	mysticismus	k1gInSc1	mysticismus
a	a	k8xC	a
jakési	jakýsi	k3yIgNnSc1	jakýsi
new	new	k?	new
age	age	k?	age
třeštění	třeštění	k1gNnSc3	třeštění
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
okořeněné	okořeněný	k2eAgNnSc1d1	okořeněné
latentní	latentní	k2eAgFnSc7d1	latentní
fascinací	fascinace	k1gFnSc7	fascinace
násilím	násilí	k1gNnSc7	násilí
a	a	k8xC	a
mocí	moc	k1gFnSc7	moc
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
Hassliebe	Hasslieb	k1gInSc5	Hasslieb
(	(	kIx(	(
<g/>
silným	silný	k2eAgInSc7d1	silný
ambivalentním	ambivalentní	k2eAgInSc7d1	ambivalentní
citovým	citový	k2eAgInSc7d1	citový
vztahem	vztah	k1gInSc7	vztah
<g/>
)	)	kIx)	)
pociťovanou	pociťovaný	k2eAgFnSc4d1	pociťovaná
vůči	vůči	k7c3	vůči
stádu	stádo	k1gNnSc3	stádo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
připomíná	připomínat	k5eAaImIp3nS	připomínat
světonázor	světonázor	k1gInSc1	světonázor
Daniela	Daniel	k1gMnSc2	Daniel
Landy	Landa	k1gMnSc2	Landa
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
i	i	k8xC	i
Landa	Landa	k1gMnSc1	Landa
svému	svůj	k3xOyFgNnSc3	svůj
publiku	publikum	k1gNnSc3	publikum
podle	podle	k7c2	podle
Onufera	Onufero	k1gNnSc2	Onufero
servírují	servírovat	k5eAaBmIp3nP	servírovat
vizi	vize	k1gFnSc4	vize
víceméně	víceméně	k9	víceméně
totožného	totožný	k2eAgInSc2d1	totožný
pseudořádu	pseudořád	k1gInSc2	pseudořád
a	a	k8xC	a
pseudomystiky	pseudomystika	k1gFnSc2	pseudomystika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zatímco	zatímco	k8xS	zatímco
Landa	Landa	k1gMnSc1	Landa
ji	on	k3xPp3gFnSc4	on
prodává	prodávat	k5eAaImIp3nS	prodávat
úplným	úplný	k2eAgMnPc3d1	úplný
prosťáčkům	prosťáček	k1gMnPc3	prosťáček
<g/>
,	,	kIx,	,
Franz	Franz	k1gInSc1	Franz
oslovuje	oslovovat	k5eAaImIp3nS	oslovovat
přece	přece	k9	přece
jen	jen	k9	jen
náročnější	náročný	k2eAgNnSc1d2	náročnější
publikum	publikum	k1gNnSc1	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Daniela	Daniel	k1gMnSc2	Daniel
Novotného	Novotný	k1gMnSc2	Novotný
je	být	k5eAaImIp3nS	být
Onufer	Onufer	k1gInSc1	Onufer
naprostým	naprostý	k2eAgMnSc7d1	naprostý
ignorantem	ignorant	k1gMnSc7	ignorant
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Franzovu	Franzův	k2eAgFnSc4d1	Franzova
starší	starý	k2eAgFnSc4d2	starší
i	i	k8xC	i
aktuální	aktuální	k2eAgInPc4d1	aktuální
výroky	výrok	k1gInPc4	výrok
a	a	k8xC	a
mnoholetou	mnoholetý	k2eAgFnSc4d1	mnoholetá
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
tvrzení	tvrzení	k1gNnSc1	tvrzení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Franz	Franz	k1gMnSc1	Franz
údajně	údajně	k6eAd1	údajně
"	"	kIx"	"
<g/>
servíruje	servírovat	k5eAaBmIp3nS	servírovat
<g/>
"	"	kIx"	"
svému	svůj	k3xOyFgNnSc3	svůj
publiku	publikum	k1gNnSc3	publikum
<g/>
,	,	kIx,	,
pouhým	pouhý	k2eAgMnSc7d1	pouhý
"	"	kIx"	"
<g/>
tláskáním	tláskání	k1gNnSc7	tláskání
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Onufer	Onufer	k1gMnSc1	Onufer
navíc	navíc	k6eAd1	navíc
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
podle	podle	k7c2	podle
Novotného	Novotný	k1gMnSc2	Novotný
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nezareagoval	zareagovat	k5eNaPmAgMnS	zareagovat
na	na	k7c4	na
jedinou	jediný	k2eAgFnSc4d1	jediná
z	z	k7c2	z
výzev	výzva	k1gFnPc2	výzva
ke	k	k7c3	k
konkretizaci	konkretizace	k1gFnSc3	konkretizace
svých	svůj	k3xOyFgInPc2	svůj
výroků	výrok	k1gInPc2	výrok
ohledně	ohledně	k7c2	ohledně
Franze	Franze	k1gFnSc2	Franze
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
článek	článek	k1gInSc4	článek
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
periodiku	periodikum	k1gNnSc6	periodikum
jednoznačně	jednoznačně	k6eAd1	jednoznačně
podporujícím	podporující	k2eAgNnSc6d1	podporující
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kandidaturu	kandidatura	k1gFnSc4	kandidatura
Karla	Karel	k1gMnSc2	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
Franzem	Franz	k1gMnSc7	Franz
bojoval	bojovat	k5eAaImAgMnS	bojovat
o	o	k7c4	o
totožnou	totožný	k2eAgFnSc4d1	totožná
vrstvu	vrstva	k1gFnSc4	vrstva
elektorátu	elektorát	k1gInSc2	elektorát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
text	text	k1gInSc4	text
pouhou	pouhý	k2eAgFnSc7d1	pouhá
součástí	součást	k1gFnSc7	součást
defamační	defamační	k2eAgFnSc2d1	defamační
útočné	útočný	k2eAgFnSc2d1	útočná
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
mnozí	mnohý	k2eAgMnPc1d1	mnohý
Schwarzenbergovi	Schwarzenbergův	k2eAgMnPc1d1	Schwarzenbergův
podporovatelé	podporovatel	k1gMnPc1	podporovatel
během	během	k7c2	během
předvolební	předvolební	k2eAgFnSc2d1	předvolební
kampaně	kampaň	k1gFnSc2	kampaň
snížili	snížit	k5eAaPmAgMnP	snížit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
levicový	levicový	k2eAgInSc1d1	levicový
portál	portál	k1gInSc1	portál
Deník	deník	k1gInSc1	deník
Referendum	referendum	k1gNnSc1	referendum
článek	článek	k1gInSc1	článek
punkera	punker	k1gMnSc2	punker
Petra	Petr	k1gMnSc2	Petr
Bergmanna	Bergmann	k1gMnSc2	Bergmann
alias	alias	k9	alias
Bergáma	Bergáma	k1gFnSc1	Bergáma
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Květy	Květa	k1gFnSc2	Květa
punkovým	punkový	k2eAgInSc7d1	punkový
protějškem	protějšek	k1gInSc7	protějšek
Franze	Franze	k1gFnSc2	Franze
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
představoval	představovat	k5eAaImAgMnS	představovat
skinheadské	skinheadský	k2eAgNnSc4d1	skinheadské
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bergmanna	Bergmann	k1gInSc2	Bergmann
není	být	k5eNaImIp3nS	být
Franz	Franz	k1gMnSc1	Franz
žádný	žádný	k1gMnSc1	žádný
alternativec	alternativec	k1gMnSc1	alternativec
<g/>
,	,	kIx,	,
bohém	bohém	k1gMnSc1	bohém
ani	ani	k8xC	ani
filosofující	filosofující	k2eAgMnSc1d1	filosofující
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
podstaty	podstata	k1gFnSc2	podstata
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
<g/>
,	,	kIx,	,
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
xenofob	xenofob	k1gMnSc1	xenofob
a	a	k8xC	a
možná	možná	k9	možná
latentní	latentní	k2eAgNnSc1d1	latentní
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
životními	životní	k2eAgFnPc7d1	životní
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
otupený	otupený	k2eAgMnSc1d1	otupený
rasista	rasista	k1gMnSc1	rasista
<g/>
,	,	kIx,	,
s	s	k7c7	s
despotickými	despotický	k2eAgInPc7d1	despotický
sklony	sklon	k1gInPc7	sklon
a	a	k8xC	a
bláhovými	bláhový	k2eAgInPc7d1	bláhový
recepty	recept	k1gInPc7	recept
na	na	k7c6	na
řízení	řízení	k1gNnSc6	řízení
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bergmanna	Bergmanno	k1gNnSc2	Bergmanno
byly	být	k5eAaImAgInP	být
Franzovy	Franzův	k2eAgInPc1d1	Franzův
xenofobní	xenofobní	k2eAgInPc1d1	xenofobní
a	a	k8xC	a
rasistické	rasistický	k2eAgInPc1d1	rasistický
názory	názor	k1gInPc1	názor
fundamentální	fundamentální	k2eAgInPc1d1	fundamentální
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
tendenční	tendenční	k2eAgFnSc1d1	tendenční
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
Bergmann	Bergmann	k1gMnSc1	Bergmann
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Franz	Franz	k1gInSc1	Franz
nyní	nyní	k6eAd1	nyní
prost	prost	k2eAgInSc1d1	prost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bergmanna	Bergmann	k1gInSc2	Bergmann
Franz	Franz	k1gMnSc1	Franz
své	svůj	k3xOyFgFnSc3	svůj
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
působení	působení	k1gNnSc1	působení
nyní	nyní	k6eAd1	nyní
bagatelizuje	bagatelizovat	k5eAaImIp3nS	bagatelizovat
a	a	k8xC	a
lže	lhát	k5eAaImIp3nS	lhát
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
motivaci	motivace	k1gFnSc6	motivace
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
ho	on	k3xPp3gInSc4	on
podle	podle	k7c2	podle
Bergmanna	Bergmanno	k1gNnSc2	Bergmanno
tehdy	tehdy	k6eAd1	tehdy
skinheadské	skinheadský	k2eAgNnSc4d1	skinheadské
prostředí	prostředí	k1gNnSc4	prostředí
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnPc3	jeho
vůdcovským	vůdcovský	k2eAgFnPc3d1	vůdcovská
tendencím	tendence	k1gFnPc3	tendence
a	a	k8xC	a
scestným	scestný	k2eAgInPc3d1	scestný
konceptům	koncept	k1gInPc3	koncept
vyvrhlo	vyvrhnout	k5eAaPmAgNnS	vyvrhnout
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
nynější	nynější	k2eAgNnSc1d1	nynější
působení	působení	k1gNnSc1	působení
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
extrémně	extrémně	k6eAd1	extrémně
pravicové	pravicový	k2eAgFnPc4d1	pravicová
tendence	tendence	k1gFnPc4	tendence
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
rehabilitovaly	rehabilitovat	k5eAaBmAgFnP	rehabilitovat
a	a	k8xC	a
aktivizovaly	aktivizovat	k5eAaImAgFnP	aktivizovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
už	už	k6eAd1	už
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
údajně	údajně	k6eAd1	údajně
hlásí	hlásit	k5eAaImIp3nS	hlásit
"	"	kIx"	"
<g/>
stará	starý	k2eAgFnSc1d1	stará
garda	garda	k1gFnSc1	garda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Danielem	Daniel	k1gMnSc7	Daniel
Landou	Landa	k1gMnSc7	Landa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Alarmující	alarmující	k2eAgNnSc1d1	alarmující
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Bergmanna	Bergmann	k1gMnSc2	Bergmann
působení	působení	k1gNnSc2	působení
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
bez	bez	k7c2	bez
hlubší	hluboký	k2eAgFnSc2d2	hlubší
znalosti	znalost	k1gFnSc2	znalost
Franze	Franze	k1gFnSc2	Franze
podporují	podporovat	k5eAaImIp3nP	podporovat
jen	jen	k9	jen
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	on	k3xPp3gNnSc2	on
vzezření	vzezření	k1gNnSc2	vzezření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
"	"	kIx"	"
<g/>
super	super	k2eAgFnSc1d1	super
týpka	týpka	k1gFnSc1	týpka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
muzikanta	muzikant	k1gMnSc2	muzikant
<g/>
,	,	kIx,	,
umělce	umělec	k1gMnSc2	umělec
a	a	k8xC	a
vzdělance	vzdělanec	k1gMnSc2	vzdělanec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nebojí	bát	k5eNaImIp3nS	bát
mluvit	mluvit	k5eAaImF	mluvit
ostře	ostro	k6eAd1	ostro
o	o	k7c6	o
politicích	politik	k1gMnPc6	politik
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
jeho	jeho	k3xOp3gInPc1	jeho
výpady	výpad	k1gInPc1	výpad
jsou	být	k5eAaImIp3nP	být
prázdné	prázdný	k2eAgInPc1d1	prázdný
<g/>
,	,	kIx,	,
populistické	populistický	k2eAgInPc1d1	populistický
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Bergmanna	Bergmann	k1gInSc2	Bergmann
jakoby	jakoby	k8xS	jakoby
Franzovo	Franzův	k2eAgNnSc1d1	Franzovo
tetování	tetování	k1gNnSc1	tetování
překrylo	překrýt	k5eAaPmAgNnS	překrýt
nejen	nejen	k6eAd1	nejen
Franzovu	Franzův	k2eAgFnSc4d1	Franzova
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
názorovou	názorový	k2eAgFnSc4d1	názorová
podstatu	podstata	k1gFnSc4	podstata
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zakrylo	zakrýt	k5eAaPmAgNnS	zakrýt
i	i	k9	i
schopnost	schopnost	k1gFnSc4	schopnost
kritického	kritický	k2eAgNnSc2d1	kritické
myšlení	myšlení	k1gNnSc2	myšlení
jindy	jindy	k6eAd1	jindy
příčetných	příčetný	k2eAgFnPc2d1	příčetná
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
moudrých	moudrý	k2eAgMnPc2d1	moudrý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
některé	některý	k3yIgMnPc4	některý
z	z	k7c2	z
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
výroků	výrok	k1gInPc2	výrok
bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
Bergmanna	Bergmann	k1gMnSc4	Bergmann
podáno	podat	k5eAaPmNgNnS	podat
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
trestní	trestní	k2eAgFnSc4d1	trestní
oznámení	oznámení	k1gNnSc4	oznámení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
levicového	levicový	k2eAgMnSc2d1	levicový
extrémisty	extrémista	k1gMnSc2	extrémista
Tomáše	Tomáš	k1gMnSc2	Tomáš
Schejbala	Schejbal	k1gMnSc2	Schejbal
je	být	k5eAaImIp3nS	být
Franz	Franz	k1gMnSc1	Franz
"	"	kIx"	"
<g/>
nacionální	nacionální	k2eAgMnSc1d1	nacionální
socialista	socialista	k1gMnSc1	socialista
strasserovského	strasserovský	k2eAgInSc2d1	strasserovský
směru	směr	k1gInSc2	směr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
"	"	kIx"	"
<g/>
prázdná	prázdný	k2eAgFnSc1d1	prázdná
postmoderní	postmoderní	k2eAgFnSc1d1	postmoderní
bublina	bublina	k1gFnSc1	bublina
jeho	jeho	k3xOp3gFnSc2	jeho
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
vizuální	vizuální	k2eAgFnSc1d1	vizuální
image	image	k1gFnSc1	image
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
"	"	kIx"	"
<g/>
praskla	prasknout	k5eAaPmAgFnS	prasknout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
otevřeně	otevřeně	k6eAd1	otevřeně
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
k	k	k7c3	k
fašismu	fašismus	k1gInSc3	fašismus
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
prezident	prezident	k1gMnSc1	prezident
by	by	kYmCp3nS	by
prý	prý	k9	prý
-	-	kIx~	-
podle	podle	k7c2	podle
Schejbala	Schejbal	k1gMnSc2	Schejbal
-	-	kIx~	-
chtěl	chtít	k5eAaImAgMnS	chtít
Franz	Franz	k1gMnSc1	Franz
"	"	kIx"	"
<g/>
vládnout	vládnout	k5eAaImF	vládnout
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
rukou	ruka	k1gFnSc7	ruka
a	a	k8xC	a
prosazovat	prosazovat	k5eAaImF	prosazovat
potlačení	potlačení	k1gNnSc3	potlačení
odborů	odbor	k1gInPc2	odbor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
po	po	k7c6	po
publikaci	publikace	k1gFnSc6	publikace
poslal	poslat	k5eAaPmAgMnS	poslat
Schejbal	Schejbal	k1gMnSc1	Schejbal
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
článek	článek	k1gInSc4	článek
na	na	k7c4	na
facebookový	facebookový	k2eAgInSc4d1	facebookový
profil	profil	k1gInSc4	profil
iniciativy	iniciativa	k1gFnSc2	iniciativa
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
Prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
doplnil	doplnit	k5eAaPmAgMnS	doplnit
jej	on	k3xPp3gInSc2	on
o	o	k7c4	o
několik	několik	k4yIc4	několik
nadávek	nadávka	k1gFnPc2	nadávka
určených	určený	k2eAgFnPc2d1	určená
Vladimíru	Vladimíra	k1gFnSc4	Vladimíra
Franzovi	Franz	k1gMnSc3	Franz
a	a	k8xC	a
Jakubu	Jakub	k1gMnSc3	Jakub
Hussarovi	Hussar	k1gMnSc3	Hussar
<g/>
,	,	kIx,	,
vedoucímu	vedoucí	k1gMnSc3	vedoucí
iniciativy	iniciativa	k1gFnSc2	iniciativa
VFP	VFP	kA	VFP
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Novák	Novák	k1gMnSc1	Novák
na	na	k7c6	na
oblastním	oblastní	k2eAgInSc6d1	oblastní
webu	web	k1gInSc6	web
Dělnické	dělnický	k2eAgFnSc2d1	Dělnická
strany	strana	k1gFnSc2	strana
sociální	sociální	k2eAgFnSc2d1	sociální
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
publikoval	publikovat	k5eAaBmAgMnS	publikovat
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Franze	Franz	k1gInSc6	Franz
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
plná	plný	k2eAgNnPc4d1	plné
ústa	ústa	k1gNnPc4	ústa
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
tolerance	tolerance	k1gFnSc2	tolerance
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Petra	Petr	k1gMnSc4	Petr
Cibulku	Cibulka	k1gMnSc4	Cibulka
a	a	k8xC	a
předsedu	předseda	k1gMnSc4	předseda
DSSS	DSSS	kA	DSSS
Tomáše	Tomáš	k1gMnSc4	Tomáš
Vandase	Vandas	k1gInSc6	Vandas
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
nedemokratické	demokratický	k2eNgMnPc4d1	nedemokratický
kandidáty	kandidát	k1gMnPc4	kandidát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgMnS	zařadit
po	po	k7c4	po
bok	bok	k1gInSc4	bok
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
potlačují	potlačovat	k5eAaImIp3nP	potlačovat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
smýšlení	smýšlení	k1gNnSc2	smýšlení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Franzovy	Franzův	k2eAgFnSc2d1	Franzova
mnohaleté	mnohaletý	k2eAgFnSc2d1	mnohaletá
partnerky	partnerka	k1gFnSc2	partnerka
Idy	Ida	k1gFnSc2	Ida
Saudkové	Saudkový	k2eAgFnSc2d1	Saudková
sice	sice	k8xC	sice
</s>
<s>
možná	možná	k9	možná
Franzovo	Franzův	k2eAgNnSc1d1	Franzovo
tetování	tetování	k1gNnSc1	tetování
bylo	být	k5eAaImAgNnS	být
projevem	projev	k1gInSc7	projev
jeho	jeho	k3xOp3gFnSc2	jeho
sebestřednosti	sebestřednost	k1gFnSc2	sebestřednost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možná	možná	k9	možná
už	už	k6eAd1	už
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
stačí	stačit	k5eAaBmIp3nS	stačit
a	a	k8xC	a
teď	teď	k6eAd1	teď
už	už	k6eAd1	už
nemá	mít	k5eNaImIp3nS	mít
zájem	zájem	k1gInSc4	zájem
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
dál	daleko	k6eAd2	daleko
upozorňovat	upozorňovat	k5eAaImF	upozorňovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
stejné	stejný	k2eAgInPc4d1	stejný
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgInPc4	svůj
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
stejné	stejná	k1gFnSc2	stejná
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
rád	rád	k6eAd1	rád
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
zahrádce	zahrádka	k1gFnSc6	zahrádka
<g/>
.	.	kIx.	.
</s>
<s>
Chalupu	chalupa	k1gFnSc4	chalupa
koupil	koupit	k5eAaPmAgMnS	koupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
a	a	k8xC	a
původně	původně	k6eAd1	původně
zpustlou	zpustlý	k2eAgFnSc4d1	zpustlá
zahradu	zahrada	k1gFnSc4	zahrada
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zkultivoval	zkultivovat	k5eAaPmAgInS	zkultivovat
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
ji	on	k3xPp3gFnSc4	on
zná	znát	k5eAaImIp3nS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Saudkové	Saudkový	k2eAgFnSc2d1	Saudková
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k6eAd1	rád
věci	věc	k1gFnSc3	věc
a	a	k8xC	a
k	k	k7c3	k
předmětům	předmět	k1gInPc3	předmět
nemá	mít	k5eNaImIp3nS	mít
vztah	vztah	k1gInSc1	vztah
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
oblečení	oblečení	k1gNnSc3	oblečení
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
pragmatický	pragmatický	k2eAgMnSc1d1	pragmatický
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mu	on	k3xPp3gNnSc3	on
oblečení	oblečení	k1gNnSc3	oblečení
vybírá	vybírat	k5eAaImIp3nS	vybírat
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sama	sám	k3xTgFnSc1	sám
nemá	mít	k5eNaImIp3nS	mít
vkus	vkus	k1gInSc4	vkus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
partnerky	partnerka	k1gFnSc2	partnerka
Idy	Ida	k1gFnSc2	Ida
Saudkové	Saudkový	k2eAgFnSc2d1	Saudková
poslouchá	poslouchat	k5eAaImIp3nS	poslouchat
jen	jen	k9	jen
vážnou	vážný	k2eAgFnSc4d1	vážná
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
pop	pop	k1gInSc1	pop
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
bytostně	bytostně	k6eAd1	bytostně
nesnáší	snášet	k5eNaImIp3nS	snášet
a	a	k8xC	a
posílá	posílat	k5eAaImIp3nS	posílat
"	"	kIx"	"
<g/>
klidně	klidně	k6eAd1	klidně
k	k	k7c3	k
čertu	čert	k1gMnSc6	čert
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vadí	vadit	k5eAaImIp3nS	vadit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
východiska	východisko	k1gNnSc2	východisko
téhle	tenhle	k3xDgFnSc2	tenhle
hudby	hudba	k1gFnSc2	hudba
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
betonových	betonový	k2eAgFnPc6d1	betonová
džunglích	džungle	k1gFnPc6	džungle
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
squatech	squat	k1gInPc6	squat
někde	někde	k6eAd1	někde
v	v	k7c6	v
Harlemu	Harlem	k1gInSc6	Harlem
či	či	k8xC	či
na	na	k7c6	na
smeťáku	smeťák	k1gInSc6	smeťák
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zdroje	zdroj	k1gInPc1	zdroj
českých	český	k2eAgMnPc2d1	český
klasiků	klasik	k1gMnPc2	klasik
-	-	kIx~	-
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
Janáčka	Janáček	k1gMnSc2	Janáček
či	či	k8xC	či
Mahlera	Mahler	k1gMnSc2	Mahler
-	-	kIx~	-
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
věci	věc	k1gFnSc6	věc
tu	tu	k6eAd1	tu
mohou	moct	k5eAaImIp3nP	moct
vyrůstat	vyrůstat	k5eAaImF	vyrůstat
z	z	k7c2	z
něčeho	něco	k3yInSc2	něco
jiného	jiný	k2eAgMnSc2d1	jiný
než	než	k8xS	než
z	z	k7c2	z
hromady	hromada	k1gFnSc2	hromada
plechu	plech	k1gInSc2	plech
a	a	k8xC	a
betonu	beton	k1gInSc2	beton
<g/>
.	.	kIx.	.
</s>
<s>
Připouští	připouštět	k5eAaImIp3nS	připouštět
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
i	i	k9	i
v	v	k7c6	v
rocku	rock	k1gInSc6	rock
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
umělec	umělec	k1gMnSc1	umělec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Franze	Franze	k1gFnPc4	Franze
"	"	kIx"	"
<g/>
spíš	spíš	k9	spíš
forma	forma	k1gFnSc1	forma
divadla	divadlo	k1gNnSc2	divadlo
než	než	k8xS	než
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
tolik	tolik	k6eAd1	tolik
koncentrovat	koncentrovat	k5eAaBmF	koncentrovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
jako	jako	k8xS	jako
koncentrovat	koncentrovat	k5eAaBmF	koncentrovat
energii	energie	k1gFnSc4	energie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
rock	rock	k1gInSc1	rock
nelíbí	líbit	k5eNaImIp3nS	líbit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
míjí	míjet	k5eAaImIp3nS	míjet
účinkem	účinek	k1gInSc7	účinek
<g/>
"	"	kIx"	"
tím	ten	k3xDgInSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
buší	bušit	k5eAaImIp3nS	bušit
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
za	za	k7c2	za
níž	jenž	k3xRgFnSc2	jenž
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gNnSc3	on
osobně	osobně	k6eAd1	osobně
"	"	kIx"	"
<g/>
punk	punk	k1gInSc1	punk
i	i	k8xC	i
<g />
.	.	kIx.	.
</s>
<s>
určitý	určitý	k2eAgInSc1d1	určitý
metal	metal	k1gInSc1	metal
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
energetické	energetický	k2eAgFnSc6d1	energetická
výpovědi	výpověď	k1gFnSc6	výpověď
sympatičtější	sympatický	k2eAgInSc4d2	sympatičtější
než	než	k8xS	než
nějaké	nějaký	k3yIgInPc4	nějaký
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
umělecké	umělecký	k2eAgInPc4d1	umělecký
útvary	útvar	k1gInPc4	útvar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
takové	takový	k3xDgFnPc1	takový
skupiny	skupina	k1gFnPc1	skupina
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
́	́	k?	́
<g/>
drajv	drajv	k1gInSc4	drajv
<g/>
́	́	k?	́
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
čím	čí	k3xOyQgNnSc7	čí
to	ten	k3xDgNnSc1	ten
jde	jít	k5eAaImIp3nS	jít
zaštítit	zaštítit	k5eAaPmF	zaštítit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
Franz	Franz	k1gMnSc1	Franz
rád	rád	k2eAgMnSc1d1	rád
"	"	kIx"	"
<g/>
country	country	k2eAgInPc4d1	country
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
nehraje	hrát	k5eNaImIp3nS	hrát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zpěváky	zpěvák	k1gMnPc4	zpěvák
a	a	k8xC	a
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
či	či	k8xC	či
poslouchá	poslouchat	k5eAaImIp3nS	poslouchat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Sex	sex	k1gInSc1	sex
Pistols	Pistolsa	k1gFnPc2	Pistolsa
<g/>
,	,	kIx,	,
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
,	,	kIx,	,
Iggy	Iggy	k1gInPc1	Iggy
Pop	pop	k1gInSc1	pop
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
píseň	píseň	k1gFnSc1	píseň
No	no	k9	no
Shit	Shit	k1gInSc4	Shit
a	a	k8xC	a
část	část	k1gFnSc4	část
písně	píseň	k1gFnSc2	píseň
Nazi	Naz	k1gFnSc2	Naz
Girfriend	Girfrienda	k1gFnPc2	Girfrienda
z	z	k7c2	z
alba	album	k1gNnSc2	album
Avenue	avenue	k1gFnSc2	avenue
B	B	kA	B
využil	využít	k5eAaPmAgMnS	využít
ve	v	k7c6	v
scénické	scénický	k2eAgFnSc6d1	scénická
hudbě	hudba	k1gFnSc6	hudba
k	k	k7c3	k
představení	představení	k1gNnSc3	představení
Gazdina	gazdina	k1gFnSc1	gazdina
roba	roba	k1gFnSc1	roba
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
křtil	křtít	k5eAaImAgMnS	křtít
CD	CD	kA	CD
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Tudor	tudor	k1gInSc1	tudor
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
obdiv	obdiv	k1gInSc4	obdiv
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
také	také	k6eAd1	také
Jiřímu	Jiří	k1gMnSc3	Jiří
Pavlicovi	Pavlic	k1gMnSc3	Pavlic
a	a	k8xC	a
"	"	kIx"	"
<g/>
jeho	jeho	k3xOp3gFnSc3	jeho
cestě	cesta	k1gFnSc3	cesta
od	od	k7c2	od
folklóru	folklór	k1gInSc2	folklór
k	k	k7c3	k
vážné	vážný	k2eAgFnSc3d1	vážná
hudbě	hudba	k1gFnSc3	hudba
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
folkové	folkový	k2eAgFnSc6d1	folková
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
"	"	kIx"	"
<g/>
velikou	veliký	k2eAgFnSc7d1	veliká
personou	persona	k1gFnSc7	persona
Karel	Karel	k1gMnSc1	Karel
Plíhal	Plíhal	k1gMnSc1	Plíhal
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
melodickou	melodický	k2eAgFnSc7d1	melodická
invencí	invence	k1gFnSc7	invence
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
skladatelské	skladatelský	k2eAgInPc4d1	skladatelský
vzory	vzor	k1gInPc4	vzor
pak	pak	k6eAd1	pak
Franz	Franz	k1gMnSc1	Franz
popsal	popsat	k5eAaPmAgMnS	popsat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Dmitrij	Dmitrij	k1gFnSc1	Dmitrij
Šostakovič	Šostakovič	k1gMnSc1	Šostakovič
(	(	kIx(	(
<g/>
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
paradoxem	paradoxon	k1gNnSc7	paradoxon
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francis	Francis	k1gFnSc1	Francis
Poulenc	Poulenc	k1gFnSc1	Poulenc
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
bodu	bod	k1gInSc2	bod
A	a	k8xC	a
do	do	k7c2	do
bodu	bod	k1gInSc2	bod
B	B	kA	B
nejkratší	krátký	k2eAgFnSc7d3	nejkratší
cestou	cesta	k1gFnSc7	cesta
a	a	k8xC	a
lehce	lehko	k6eAd1	lehko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
hlubinu	hlubina	k1gFnSc4	hlubina
letního	letní	k2eAgInSc2d1	letní
<g />
.	.	kIx.	.
</s>
<s>
bezpečí	bezpečí	k1gNnSc1	bezpečí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
skýtá	skýtat	k5eAaImIp3nS	skýtat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Liška	Liška	k1gMnSc1	Liška
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
maximální	maximální	k2eAgFnSc4d1	maximální
ekonomii	ekonomie	k1gFnSc4	ekonomie
ve	v	k7c6	v
vytěžení	vytěžení	k1gNnSc6	vytěžení
tématu	téma	k1gNnSc2	téma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Giacomo	Giacoma	k1gFnSc5	Giacoma
Puccini	Puccin	k1gMnPc1	Puccin
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
nakládání	nakládání	k1gNnSc4	nakládání
s	s	k7c7	s
temporytmem	temporytm	k1gInSc7	temporytm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
naprostý	naprostý	k2eAgInSc4d1	naprostý
srůst	srůst	k1gInSc4	srůst
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k1gFnSc2	Martinů
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
zvuku	zvuk	k1gInSc3	zvuk
sypajících	sypající	k2eAgInPc2d1	sypající
se	se	k3xPyFc4	se
křišťálových	křišťálový	k2eAgInPc2d1	křišťálový
lustrů	lustr	k1gInPc2	lustr
<g/>
)	)	kIx)	)
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
kterého	který	k3yIgInSc2	který
mohu	moct	k5eAaImIp1nS	moct
kdykoliv	kdykoliv	k6eAd1	kdykoliv
potkat	potkat	k5eAaPmF	potkat
mezi	mezi	k7c4	mezi
poli	pole	k1gFnSc4	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
autorky	autorka	k1gFnSc2	autorka
rozhovoru	rozhovor	k1gInSc2	rozhovor
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Instinkt	instinkt	k1gInSc4	instinkt
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
"	"	kIx"	"
<g/>
všechno	všechen	k3xTgNnSc1	všechen
to	ten	k3xDgNnSc1	ten
tetování	tetování	k1gNnSc1	tetování
a	a	k8xC	a
vojenské	vojenský	k2eAgFnPc1d1	vojenská
boty	bota	k1gFnPc1	bota
působí	působit	k5eAaImIp3nP	působit
jen	jen	k9	jen
jako	jako	k9	jako
drsná	drsný	k2eAgFnSc1d1	drsná
fasáda	fasáda	k1gFnSc1	fasáda
člověka	člověk	k1gMnSc2	člověk
obdařeného	obdařený	k2eAgInSc2d1	obdařený
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
vnímavostí	vnímavost	k1gFnSc7	vnímavost
a	a	k8xC	a
věčně	věčně	k6eAd1	věčně
rozjitřenou	rozjitřený	k2eAgFnSc7d1	rozjitřená
duší	duše	k1gFnSc7	duše
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
rád	rád	k6eAd1	rád
akční	akční	k2eAgInPc4d1	akční
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
dobro	dobro	k1gNnSc1	dobro
a	a	k8xC	a
zlo	zlo	k1gNnSc1	zlo
jasně	jasně	k6eAd1	jasně
definované	definovaný	k2eAgNnSc1d1	definované
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
Kubricka	Kubricko	k1gNnSc2	Kubricko
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
asi	asi	k9	asi
zbláznil	zbláznit	k5eAaPmAgMnS	zbláznit
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
renesanční	renesanční	k2eAgMnSc1d1	renesanční
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
nazval	nazvat	k5eAaPmAgMnS	nazvat
ho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
například	například	k6eAd1	například
Martin	Martin	k1gMnSc1	Martin
Plešinger	Plešinger	k1gMnSc1	Plešinger
jako	jako	k8xS	jako
shrnutí	shrnutí	k1gNnSc1	shrnutí
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
nadhledem	nadhled	k1gInSc7	nadhled
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
technokrat	technokrat	k1gMnSc1	technokrat
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Rychlík	Rychlík	k1gMnSc1	Rychlík
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
pozici	pozice	k1gFnSc6	pozice
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
předvolebním	předvolební	k2eAgNnSc6d1	předvolební
období	období	k1gNnSc6	období
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
o	o	k7c6	o
Franzovi	Franz	k1gMnSc6	Franz
jako	jako	k8xC	jako
o	o	k7c6	o
renesanční	renesanční	k2eAgFnSc3d1	renesanční
osobnosti	osobnost	k1gFnSc3	osobnost
rozkročené	rozkročený	k2eAgFnSc3d1	rozkročená
od	od	k7c2	od
umění	umění	k1gNnSc2	umění
po	po	k7c4	po
juristickou	juristický	k2eAgFnSc4d1	juristická
erudici	erudice	k1gFnSc4	erudice
i	i	k8xC	i
geologii	geologie	k1gFnSc4	geologie
<g/>
,	,	kIx,	,
vzdělaném	vzdělaný	k2eAgMnSc6d1	vzdělaný
člověku	člověk	k1gMnSc6	člověk
s	s	k7c7	s
mravní	mravní	k2eAgFnSc7d1	mravní
autoritou	autorita	k1gFnSc7	autorita
i	i	k8xC	i
úspěchy	úspěch	k1gInPc7	úspěch
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
prý	prý	k9	prý
nálepku	nálepka	k1gFnSc4	nálepka
"	"	kIx"	"
<g/>
renesanční	renesanční	k2eAgMnSc1d1	renesanční
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
nesnáší	snášet	k5eNaImIp3nS	snášet
-	-	kIx~	-
takovým	takový	k3xDgInPc3	takový
"	"	kIx"	"
<g/>
Broukem	brouk	k1gMnSc7	brouk
Pytlíkem	pytlík	k1gMnSc7	pytlík
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
kušuje	kušovat	k5eAaImIp3nS	kušovat
do	do	k7c2	do
všeho	všecek	k3xTgInSc2	všecek
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
do	do	k7c2	do
ničeho	nic	k3yNnSc2	nic
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
nerad	nerad	k2eAgMnSc1d1	nerad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
však	však	k8xC	však
studentům	student	k1gMnPc3	student
ekonomie	ekonomie	k1gFnSc2	ekonomie
na	na	k7c6	na
besedě	beseda	k1gFnSc6	beseda
v	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
dnešní	dnešní	k2eAgInSc1d1	dnešní
problém	problém	k1gInSc1	problém
není	být	k5eNaImIp3nS	být
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mravní	mravní	k2eAgInPc1d1	mravní
<g/>
,	,	kIx,	,
doporučil	doporučit	k5eAaPmAgMnS	doporučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
znát	znát	k5eAaImF	znát
staré	starý	k2eAgFnPc4d1	stará
básničky	básnička	k1gFnPc4	básnička
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
"	"	kIx"	"
<g/>
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
Le	Le	k1gFnSc4	Le
Cigare	Cigar	k1gMnSc5	Cigar
and	and	k?	and
Vin	vina	k1gFnPc2	vina
Style	styl	k1gInSc5	styl
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
&	&	k?	&
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Počítače	počítač	k1gInPc1	počítač
a	a	k8xC	a
internet	internet	k1gInSc1	internet
jsou	být	k5eAaImIp3nP	být
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgInPc2d3	Nejhorší
zločinů	zločin	k1gInPc2	zločin
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
na	na	k7c6	na
lidstvu	lidstvo	k1gNnSc6	lidstvo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnSc7	ruka
prostému	prostý	k2eAgInSc3d1	prostý
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Internet	Internet	k1gInSc1	Internet
měl	mít	k5eAaImAgInS	mít
zůstat	zůstat	k5eAaPmF	zůstat
vědcům	vědec	k1gMnPc3	vědec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Za	za	k7c4	za
tragický	tragický	k2eAgInSc1d1	tragický
považoval	považovat	k5eAaImAgInS	považovat
posun	posun	k1gInSc1	posun
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
všeho	všecek	k3xTgMnSc2	všecek
do	do	k7c2	do
virtuální	virtuální	k2eAgFnSc2d1	virtuální
reality	realita	k1gFnSc2	realita
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
zůstane	zůstat	k5eAaPmIp3nS	zůstat
úplně	úplně	k6eAd1	úplně
shnilej	shnilej	k?	shnilej
sedět	sedět	k5eAaImF	sedět
s	s	k7c7	s
nějakýma	nějaký	k3yIgFnPc7	nějaký
sluchátkama	sluchátkama	k?	sluchátkama
<g/>
,	,	kIx,	,
hmatátkama	hmatátkama	k1gFnSc1	hmatátkama
a	a	k8xC	a
brýlema	brýlema	k?	brýlema
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
píše	psát	k5eAaImIp3nS	psát
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
v	v	k7c6	v
editoru	editor	k1gInSc6	editor
vypadá	vypadat	k5eAaPmIp3nS	vypadat
pěkně	pěkně	k6eAd1	pěkně
každá	každý	k3xTgFnSc1	každý
pitomost	pitomost	k1gFnSc1	pitomost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ručně	ručně	k6eAd1	ručně
musí	muset	k5eAaImIp3nS	muset
člověk	člověk	k1gMnSc1	člověk
psát	psát	k5eAaImF	psát
s	s	k7c7	s
rozmyslem	rozmysl	k1gInSc7	rozmysl
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Franz	Franz	k1gMnSc1	Franz
nebyl	být	k5eNaImAgMnS	být
nikdy	nikdy	k6eAd1	nikdy
součástí	součást	k1gFnPc2	součást
politického	politický	k2eAgInSc2d1	politický
disentu	disent	k1gInSc2	disent
či	či	k8xC	či
kulturního	kulturní	k2eAgInSc2d1	kulturní
undergroundu	underground	k1gInSc2	underground
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
-	-	kIx~	-
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
-	-	kIx~	-
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
při	při	k7c6	při
vší	všecek	k3xTgFnSc6	všecek
úctě	úcta	k1gFnSc6	úcta
k	k	k7c3	k
postojům	postoj	k1gInPc3	postoj
představitelů	představitel	k1gMnPc2	představitel
těchto	tento	k3xDgInPc2	tento
kruhů	kruh	k1gInPc2	kruh
značně	značně	k6eAd1	značně
vadila	vadit	k5eAaImAgFnS	vadit
estetika	estetika	k1gFnSc1	estetika
jejich	jejich	k3xOp3gInSc2	jejich
"	"	kIx"	"
<g/>
divného	divný	k2eAgInSc2d1	divný
máničkovského	máničkovský	k2eAgInSc2d1	máničkovský
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
"	"	kIx"	"
<g/>
odpuzovala	odpuzovat	k5eAaImAgFnS	odpuzovat
<g/>
"	"	kIx"	"
hudba	hudba	k1gFnSc1	hudba
Plastic	Plastice	k1gFnPc2	Plastice
People	People	k1gFnSc1	People
<g/>
,	,	kIx,	,
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc1d1	divadelní
činnost	činnost	k1gFnSc1	činnost
Tvrdohlavých	tvrdohlavý	k2eAgInPc2d1	tvrdohlavý
i	i	k8xC	i
divadlo	divadlo	k1gNnSc1	divadlo
Sklep	sklep	k1gInSc4	sklep
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
měl	mít	k5eAaImAgInS	mít
prý	prý	k9	prý
mnohé	mnohý	k2eAgMnPc4d1	mnohý
členy	člen	k1gMnPc4	člen
těchto	tento	k3xDgNnPc2	tento
uskupení	uskupení	k1gNnPc2	uskupení
měl	mít	k5eAaImAgInS	mít
"	"	kIx"	"
<g/>
lidsky	lidsky	k6eAd1	lidsky
rád	rád	k6eAd1	rád
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
