<s>
Féničané	Féničan	k1gMnPc1	Féničan
byli	být	k5eAaImAgMnP	být
semitský	semitský	k2eAgInSc4d1	semitský
kmen	kmen	k1gInSc4	kmen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
osídlil	osídlit	k5eAaPmAgInS	osídlit
krajinu	krajina	k1gFnSc4	krajina
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Syropalestinské	Syropalestinský	k2eAgFnSc2d1	Syropalestinský
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
fénických	fénický	k2eAgNnPc2d1	fénické
měst	město	k1gNnPc2	město
byl	být	k5eAaImAgMnS	být
Týros	Týros	k1gInSc4	Týros
<g/>
,	,	kIx,	,
také	také	k9	také
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Syr	Syr	k1gMnSc1	Syr
<g/>
.	.	kIx.	.
</s>
<s>
Féničané	Féničan	k1gMnPc1	Féničan
byli	být	k5eAaImAgMnP	být
zdatnými	zdatný	k2eAgMnPc7d1	zdatný
obchodníky	obchodník	k1gMnPc7	obchodník
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
obchodu	obchod	k1gInSc3	obchod
podnikali	podnikat	k5eAaImAgMnP	podnikat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
plavby	plavba	k1gFnPc4	plavba
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
rovněž	rovněž	k9	rovněž
vynikajícími	vynikající	k2eAgMnPc7d1	vynikající
řemeslníky	řemeslník	k1gMnPc7	řemeslník
<g/>
,	,	kIx,	,
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
s	s	k7c7	s
výrobky	výrobek	k1gInPc7	výrobek
z	z	k7c2	z
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
bavlněné	bavlněný	k2eAgInPc4d1	bavlněný
oděvy	oděv	k1gInPc4	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc4	látka
barvili	barvit	k5eAaImAgMnP	barvit
nachovým	nachový	k2eAgNnSc7d1	nachové
barvivem	barvivo	k1gNnSc7	barvivo
(	(	kIx(	(
<g/>
foinix	foinix	k1gInSc1	foinix
=	=	kIx~	=
purpurová	purpurový	k2eAgFnSc1d1	purpurová
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
započala	započnout	k5eAaPmAgFnS	započnout
fénická	fénický	k2eAgFnSc1d1	fénická
kolonizace	kolonizace	k1gFnSc1	kolonizace
západního	západní	k2eAgNnSc2d1	západní
Středomoří	středomoří	k1gNnSc2	středomoří
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgFnSc1d1	obchodní
dostupnost	dostupnost	k1gFnSc1	dostupnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
814	[number]	k4	814
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Féničané	Féničan	k1gMnPc1	Féničan
založili	založit	k5eAaPmAgMnP	založit
město	město	k1gNnSc4	město
a	a	k8xC	a
přístav	přístav	k1gInSc4	přístav
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
významnou	významný	k2eAgFnSc7d1	významná
námořní	námořní	k2eAgFnSc7d1	námořní
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Féničané	Féničan	k1gMnPc1	Féničan
dostávají	dostávat	k5eAaImIp3nP	dostávat
i	i	k9	i
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hérodota	Hérodot	k1gMnSc2	Hérodot
údajně	údajně	k6eAd1	údajně
obepluli	obeplout	k5eAaPmAgMnP	obeplout
i	i	k9	i
Afriku	Afrika	k1gFnSc4	Afrika
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
kolonie	kolonie	k1gFnSc1	kolonie
Féničanů	Féničan	k1gMnPc2	Féničan
bylo	být	k5eAaImAgNnS	být
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nakonec	nakonec	k6eAd1	nakonec
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
západ	západ	k1gInSc4	západ
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
mocnost	mocnost	k1gFnSc1	mocnost
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
sérií	série	k1gFnSc7	série
punských	punský	k2eAgFnPc2d1	punská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
zničena	zničit	k5eAaPmNgFnS	zničit
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
122	[number]	k4	122
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
města	město	k1gNnSc2	město
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
římské	římský	k2eAgNnSc1d1	římské
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kartágo	Kartágo	k1gNnSc1	Kartágo
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Féničanům	Féničan	k1gMnPc3	Féničan
je	být	k5eAaImIp3nS	být
připisován	připisován	k2eAgInSc1d1	připisován
vynález	vynález	k1gInSc1	vynález
hláskového	hláskový	k2eAgNnSc2d1	hláskové
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
22	[number]	k4	22
hlásek	hláska	k1gFnPc2	hláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
vynález	vynález	k1gInSc4	vynález
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
-	-	kIx~	-
šlo	jít	k5eAaImAgNnS	jít
spíše	spíše	k9	spíše
o	o	k7c6	o
završení	završení	k1gNnSc3	završení
nejméně	málo	k6eAd3	málo
několikasetletého	několikasetletý	k2eAgInSc2d1	několikasetletý
vývoje	vývoj	k1gInSc2	vývoj
písma	písmo	k1gNnSc2	písmo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
semitské	semitský	k2eAgFnSc6d1	semitská
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokládají	dokládat	k5eAaImIp3nP	dokládat
nálezy	nález	k1gInPc1	nález
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
i	i	k8xC	i
na	na	k7c6	na
Sinajském	sinajský	k2eAgInSc6d1	sinajský
poloostrově	poloostrov	k1gInSc6	poloostrov
(	(	kIx(	(
<g/>
protosinajské	protosinajský	k2eAgNnSc1d1	protosinajský
písmo	písmo	k1gNnSc1	písmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fénická	fénický	k2eAgFnSc1d1	fénická
kultura	kultura	k1gFnSc1	kultura
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
svébytná	svébytný	k2eAgFnSc1d1	svébytná
<g/>
,	,	kIx,	,
jen	jen	k9	jen
nepatrně	nepatrně	k6eAd1	nepatrně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
mezopotámskými	mezopotámský	k2eAgInPc7d1	mezopotámský
a	a	k8xC	a
egyptskými	egyptský	k2eAgInPc7d1	egyptský
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tu	tu	k6eAd1	tu
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
kvetlo	kvést	k5eAaImAgNnS	kvést
tu	tu	k6eAd1	tu
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
především	především	k9	především
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
dovednost	dovednost	k1gFnSc1	dovednost
<g/>
:	:	kIx,	:
výrobky	výrobek	k1gInPc4	výrobek
jejich	jejich	k3xOp3gNnPc2	jejich
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
nacházely	nacházet	k5eAaImAgFnP	nacházet
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
se	se	k3xPyFc4	se
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
ledek	ledek	k1gInSc1	ledek
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
mramor	mramor	k1gInSc1	mramor
<g/>
,	,	kIx,	,
purpur	purpur	k1gInSc1	purpur
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
tkaniny	tkanina	k1gFnPc1	tkanina
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
lodích	loď	k1gFnPc6	loď
křižovali	křižovat	k5eAaImAgMnP	křižovat
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
prostorem	prostor	k1gInSc7	prostor
mezi	mezi	k7c7	mezi
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Levantou	Levanta	k1gFnSc7	Levanta
<g/>
,	,	kIx,	,
zboží	zboží	k1gNnSc4	zboží
převáželi	převážet	k5eAaImAgMnP	převážet
mezi	mezi	k7c7	mezi
Baleárami	Baleáry	k1gFnPc7	Baleáry
<g/>
,	,	kIx,	,
Sicílií	Sicílie	k1gFnSc7	Sicílie
a	a	k8xC	a
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Féničané	Féničan	k1gMnPc1	Féničan
vynikali	vynikat	k5eAaImAgMnP	vynikat
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
v	v	k7c6	v
tkalcovství	tkalcovství	k1gNnSc6	tkalcovství
<g/>
,	,	kIx,	,
v	v	k7c6	v
barvení	barvení	k1gNnSc6	barvení
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
nachovcem	nachovec	k1gMnSc7	nachovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
leštění	leštění	k1gNnSc6	leštění
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
.	.	kIx.	.
</s>
<s>
Stopy	stopa	k1gFnPc1	stopa
fénických	fénický	k2eAgFnPc2d1	fénická
znalostí	znalost	k1gFnPc2	znalost
lze	lze	k6eAd1	lze
údajně	údajně	k6eAd1	údajně
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
zvané	zvaný	k2eAgNnSc4d1	zvané
Sanchoniathon	Sanchoniathon	k1gNnSc4	Sanchoniathon
<g/>
.	.	kIx.	.
</s>
<s>
Svérázné	svérázný	k2eAgNnSc1d1	svérázné
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
jejich	jejich	k3xOp3gNnPc1	jejich
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
uctívání	uctívání	k1gNnSc6	uctívání
mnoha	mnoho	k4c2	mnoho
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
bohem	bůh	k1gMnSc7	bůh
jejich	jejich	k3xOp3gInSc2	jejich
panteonu	panteon	k1gInSc2	panteon
byl	být	k5eAaImAgInS	být
strašný	strašný	k2eAgInSc1d1	strašný
a	a	k8xC	a
obávaný	obávaný	k2eAgInSc1d1	obávaný
El	Ela	k1gFnPc2	Ela
<g/>
,	,	kIx,	,
stvořitel	stvořitel	k1gMnSc1	stvořitel
<g/>
,	,	kIx,	,
uctívaný	uctívaný	k2eAgMnSc1d1	uctívaný
někdy	někdy	k6eAd1	někdy
též	též	k6eAd1	též
jako	jako	k8xS	jako
Dagon	Dagon	k1gInSc4	Dagon
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
tu	tu	k6eAd1	tu
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jaw	jawa	k1gFnPc2	jawa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
někdy	někdy	k6eAd1	někdy
ztotožňován	ztotožňován	k2eAgMnSc1d1	ztotožňován
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
úrody	úroda	k1gFnSc2	úroda
Baalem	Baal	k1gMnSc7	Baal
<g/>
,	,	kIx,	,
Baalova	Baalův	k2eAgFnSc1d1	Baalova
dcera	dcera	k1gFnSc1	dcera
bohyně	bohyně	k1gFnSc2	bohyně
deště	dešť	k1gInSc2	dešť
Tapie	Tapie	k1gFnSc1	Tapie
<g/>
,	,	kIx,	,
Baalova	Baalův	k2eAgFnSc1d1	Baalova
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
bohyně	bohyně	k1gFnSc1	bohyně
lásky	láska	k1gFnSc2	láska
Astarté	Astarta	k1gMnPc1	Astarta
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
nalezena	nalezen	k2eAgFnSc1d1	nalezena
spousta	spousta	k1gFnSc1	spousta
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
dochovaly	dochovat	k5eAaPmAgInP	dochovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hliněných	hliněný	k2eAgFnPc2d1	hliněná
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Fénické	fénický	k2eAgNnSc1d1	fénické
náboženství	náboženství	k1gNnSc1	náboženství
bylo	být	k5eAaImAgNnS	být
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
častým	častý	k2eAgFnPc3d1	častá
oslavám	oslava	k1gFnPc3	oslava
a	a	k8xC	a
tolerantnosti	tolerantnost	k1gFnSc6	tolerantnost
velkým	velký	k2eAgNnSc7d1	velké
lákadlem	lákadlo	k1gNnSc7	lákadlo
pro	pro	k7c4	pro
Izraelity	izraelita	k1gMnPc4	izraelita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
soudců	soudce	k1gMnPc2	soudce
(	(	kIx(	(
<g/>
10,6	[number]	k4	10,6
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
dočíst	dočíst	k5eAaPmF	dočíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
Izraelité	izraelita	k1gMnPc1	izraelita
sloužili	sloužit	k5eAaImAgMnP	sloužit
Bálům	bál	k1gInPc3	bál
a	a	k8xC	a
Astarté	Astarta	k1gMnPc1	Astarta
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
bohům	bůh	k1gMnPc3	bůh
syrským	syrský	k2eAgNnSc7d1	syrské
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
opustili	opustit	k5eAaPmAgMnP	opustit
Hospodina	Hospodin	k1gMnSc4	Hospodin
a	a	k8xC	a
nesloužili	sloužit	k5eNaImAgMnP	sloužit
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Šalomounovy	Šalomounův	k2eAgFnSc2d1	Šalomounova
vlády	vláda	k1gFnSc2	vláda
měli	mít	k5eAaImAgMnP	mít
Baal	Baal	k1gMnSc1	Baal
i	i	k8xC	i
Astarté	Astarta	k1gMnPc1	Astarta
svůj	svůj	k3xOyFgInSc4	svůj
oltář	oltář	k1gInSc4	oltář
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Šalomounově	Šalomounův	k2eAgInSc6d1	Šalomounův
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
rivalita	rivalita	k1gFnSc1	rivalita
těchto	tento	k3xDgNnPc2	tento
náboženství	náboženství	k1gNnPc2	náboženství
způsobila	způsobit	k5eAaPmAgFnS	způsobit
nevraživost	nevraživost	k1gFnSc1	nevraživost
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
nenávist	nenávist	k1gFnSc1	nenávist
hebrejských	hebrejský	k2eAgMnPc2d1	hebrejský
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
bohyně	bohyně	k1gFnSc1	bohyně
Astarté	Astartý	k2eAgNnSc1d1	Astarté
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
Bibli	bible	k1gFnSc4	bible
zostuzována	zostuzován	k2eAgFnSc1d1	zostuzován
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
kultu	kult	k1gInSc6	kult
hrál	hrát	k5eAaImAgInS	hrát
hlavní	hlavní	k2eAgFnSc3d1	hlavní
roli	role	k1gFnSc3	role
sex	sex	k1gInSc4	sex
<g/>
.	.	kIx.	.
</s>
<s>
Prostý	prostý	k2eAgInSc1d1	prostý
lid	lid	k1gInSc1	lid
však	však	k9	však
nepovažoval	považovat	k5eNaImAgInS	považovat
sex	sex	k1gInSc1	sex
za	za	k7c4	za
něco	něco	k3yInSc4	něco
pobuřujícího	pobuřující	k2eAgNnSc2d1	pobuřující
a	a	k8xC	a
učení	učení	k1gNnSc1	učení
Astarté	Astartý	k2eAgNnSc1d1	Astarté
tedy	tedy	k9	tedy
nebudilo	budit	k5eNaImAgNnS	budit
jeho	on	k3xPp3gInSc4	on
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Zavedli	zavést	k5eAaPmAgMnP	zavést
také	také	k9	také
řadu	řada	k1gFnSc4	řada
novinek	novinka	k1gFnPc2	novinka
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
lodí	loď	k1gFnPc2	loď
<g/>
:	:	kIx,	:
jejich	jejich	k3xOp3gFnPc4	jejich
první	první	k4xOgFnPc4	první
lodi	loď	k1gFnPc4	loď
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
podobaly	podobat	k5eAaImAgInP	podobat
egyptským	egyptský	k2eAgNnSc7d1	egyptské
se	s	k7c7	s
zvednutou	zvednutý	k2eAgFnSc7d1	zvednutá
přídí	příď	k1gFnSc7	příď
i	i	k8xC	i
zádí	záď	k1gFnSc7	záď
a	a	k8xC	a
kolmými	kolmý	k2eAgInPc7d1	kolmý
boky	bok	k1gInPc7	bok
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
však	však	k9	však
důmyslnější	důmyslný	k2eAgFnSc4d2	důmyslnější
vazbu	vazba	k1gFnSc4	vazba
lanoví	lanoví	k1gNnSc2	lanoví
mezi	mezi	k7c7	mezi
přídí	příď	k1gFnSc7	příď
a	a	k8xC	a
zádí	záď	k1gFnSc7	záď
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
fénická	fénický	k2eAgNnPc1d1	fénické
plavidla	plavidlo	k1gNnPc1	plavidlo
kombinovala	kombinovat	k5eAaImAgNnP	kombinovat
pohon	pohon	k1gInSc4	pohon
vesly	veslo	k1gNnPc7	veslo
a	a	k8xC	a
čtvercovou	čtvercový	k2eAgFnSc7d1	čtvercová
příčnou	příčný	k2eAgFnSc7d1	příčná
plachtou	plachta	k1gFnSc7	plachta
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
300	[number]	k4	300
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Výtlak	výtlak	k1gInSc1	výtlak
takových	takový	k3xDgNnPc2	takový
plavidel	plavidlo	k1gNnPc2	plavidlo
při	při	k7c6	při
délce	délka	k1gFnSc6	délka
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
ponorem	ponor	k1gInSc7	ponor
2,5	[number]	k4	2,5
metru	metr	k1gInSc2	metr
činil	činit	k5eAaImAgMnS	činit
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
400	[number]	k4	400
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Posádku	posádka	k1gFnSc4	posádka
tvořilo	tvořit	k5eAaImAgNnS	tvořit
asi	asi	k9	asi
30	[number]	k4	30
mužů	muž	k1gMnPc2	muž
–	–	k?	–
veslařů	veslař	k1gMnPc2	veslař
<g/>
,	,	kIx,	,
námořníků	námořník	k1gMnPc2	námořník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Féničané	Féničan	k1gMnPc1	Féničan
však	však	k9	však
disponovali	disponovat	k5eAaBmAgMnP	disponovat
i	i	k9	i
velmi	velmi	k6eAd1	velmi
rychlými	rychlý	k2eAgInPc7d1	rychlý
a	a	k8xC	a
obratnými	obratný	k2eAgInPc7d1	obratný
válečnými	válečný	k2eAgNnPc7d1	válečné
plavidly	plavidlo	k1gNnPc7	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
plavidla	plavidlo	k1gNnPc1	plavidlo
byla	být	k5eAaImAgNnP	být
delší	dlouhý	k2eAgInSc4d2	delší
a	a	k8xC	a
užší	úzký	k2eAgFnPc4d2	užší
než	než	k8xS	než
obchodní	obchodní	k2eAgFnPc4d1	obchodní
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
zesílenou	zesílený	k2eAgFnSc4d1	zesílená
příď	příď	k1gFnSc4	příď
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
orientací	orientace	k1gFnSc7	orientace
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
byly	být	k5eAaImAgFnP	být
potíže	potíž	k1gFnPc1	potíž
a	a	k8xC	a
proto	proto	k8xC	proto
pluly	plout	k5eAaImAgFnP	plout
lodě	loď	k1gFnPc1	loď
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
vidět	vidět	k5eAaImF	vidět
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
Féničané	Féničan	k1gMnPc1	Féničan
řídili	řídit	k5eAaImAgMnP	řídit
podle	podle	k7c2	podle
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
lodi	loď	k1gFnSc2	loď
byla	být	k5eAaImAgFnS	být
8	[number]	k4	8
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
a	a	k8xC	a
válečné	válečný	k2eAgInPc1d1	válečný
15	[number]	k4	15
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
loděnice	loděnice	k1gFnPc1	loděnice
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
v	v	k7c6	v
Sidonu	Sidon	k1gInSc6	Sidon
<g/>
,	,	kIx,	,
Ugaritu	Ugarit	k1gInSc6	Ugarit
<g/>
,	,	kIx,	,
Auvadu	Auvad	k1gInSc6	Auvad
<g/>
,	,	kIx,	,
Byblu	Bybl	k1gInSc6	Bybl
<g/>
,	,	kIx,	,
Týru	Týrus	k1gInSc2	Týrus
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
další	další	k2eAgInPc1d1	další
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1500	[number]	k4	1500
–	–	k?	–
1209	[number]	k4	1209
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
foinických	foinický	k2eAgNnPc2d1	foinický
měst	město	k1gNnPc2	město
propracoval	propracovat	k5eAaPmAgMnS	propracovat
Sídon	Sídon	k1gMnSc1	Sídon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Fénicie	Fénicie	k1gFnSc2	Fénicie
jako	jako	k8xS	jako
na	na	k7c6	na
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
Předního	přední	k2eAgInSc2d1	přední
východu	východ	k1gInSc2	východ
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
četným	četný	k2eAgInPc3d1	četný
velmocenským	velmocenský	k2eAgInPc3d1	velmocenský
bojům	boj	k1gInPc3	boj
mezi	mezi	k7c7	mezi
Asyřany	Asyřan	k1gMnPc7	Asyřan
<g/>
,	,	kIx,	,
Chetity	Chetit	k1gMnPc7	Chetit
a	a	k8xC	a
Egypťany	Egypťan	k1gMnPc7	Egypťan
<g/>
,	,	kIx,	,
Féničané	Féničan	k1gMnPc1	Féničan
však	však	k9	však
kupodivu	kupodivu	k6eAd1	kupodivu
tohoto	tento	k3xDgInSc2	tento
pohybu	pohyb	k1gInSc2	pohyb
dovedli	dovést	k5eAaPmAgMnP	dovést
využít	využít	k5eAaPmF	využít
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
lodě	loď	k1gFnPc1	loď
brázdily	brázdit	k5eAaImAgFnP	brázdit
Egejské	egejský	k2eAgNnSc4d1	Egejské
moře	moře	k1gNnSc4	moře
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
celé	celý	k2eAgNnSc4d1	celé
východní	východní	k2eAgNnSc4d1	východní
Středomoří	středomoří	k1gNnSc4	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
je	on	k3xPp3gInPc4	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Kypru	Kypr	k1gInSc2	Kypr
<g/>
,	,	kIx,	,
Rhodu	Rhodos	k1gInSc2	Rhodos
<g/>
,	,	kIx,	,
Krétě	Kréta	k1gFnSc6	Kréta
<g/>
,	,	kIx,	,
Théře	Théra	k1gFnSc6	Théra
<g/>
,	,	kIx,	,
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
Korsice	Korsika	k1gFnSc6	Korsika
<g/>
,	,	kIx,	,
Sardínii	Sardínie	k1gFnSc6	Sardínie
<g/>
,	,	kIx,	,
Elbě	Elba	k1gFnSc6	Elba
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
vzhlíželi	vzhlížet	k5eAaImAgMnP	vzhlížet
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
mateřské	mateřský	k2eAgFnSc3d1	mateřská
zemi	zem	k1gFnSc3	zem
s	s	k7c7	s
pietou	pieta	k1gFnSc7	pieta
<g/>
,	,	kIx,	,
dodržovali	dodržovat	k5eAaImAgMnP	dodržovat
její	její	k3xOp3gInPc4	její
zvyky	zvyk	k1gInPc4	zvyk
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc4	náboženství
a	a	k8xC	a
způsoby	způsob	k1gInPc4	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
osady	osada	k1gFnPc1	osada
však	však	k9	však
byly	být	k5eAaImAgFnP	být
drženy	držet	k5eAaImNgFnP	držet
v	v	k7c6	v
přísné	přísný	k2eAgFnSc6d1	přísná
podřízenosti	podřízenost	k1gFnSc6	podřízenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1209	[number]	k4	1209
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
vznešených	vznešený	k2eAgFnPc2d1	vznešená
rodin	rodina	k1gFnPc2	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Týru	Týrus	k1gInSc2	Týrus
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
nejsilnějším	silný	k2eAgNnSc7d3	nejsilnější
městem	město	k1gNnSc7	město
Fénicie	Fénicie	k1gFnSc2	Fénicie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1050	[number]	k4	1050
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyl	dobýt	k5eAaPmAgInS	dobýt
celou	celý	k2eAgFnSc4d1	celá
Fénicii	Fénicie	k1gFnSc4	Fénicie
král	král	k1gMnSc1	král
Abibaal	Abibaal	k1gMnSc1	Abibaal
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
králem	král	k1gMnSc7	král
Fénicie	Fénicie	k1gFnSc2	Fénicie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Chíram	Chíram	k1gInSc4	Chíram
byl	být	k5eAaImAgMnS	být
současníkem	současník	k1gMnSc7	současník
Šalamounovým	Šalamounův	k2eAgMnSc7d1	Šalamounův
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozkvětu	rozkvět	k1gInSc3	rozkvět
Fénicie	Fénicie	k1gFnSc2	Fénicie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Chíramově	Chíramově	k1gFnSc6	Chíramově
smrti	smrt	k1gFnSc2	smrt
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c4	v
Fénicii	Fénicie	k1gFnSc4	Fénicie
k	k	k7c3	k
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Týru	Týrus	k1gInSc2	Týrus
se	se	k3xPyFc4	se
vystěhovali	vystěhovat	k5eAaPmAgMnP	vystěhovat
do	do	k7c2	do
zámořských	zámořský	k2eAgFnPc2d1	zámořská
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
Ithobal	Ithobal	k1gFnSc2	Ithobal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zmatky	zmatek	k1gInPc1	zmatek
ve	v	k7c6	v
Fénicii	Fénicie	k1gFnSc6	Fénicie
ukončil	ukončit	k5eAaPmAgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
zmatkům	zmatek	k1gInPc3	zmatek
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
králů	král	k1gMnPc2	král
bylo	být	k5eAaImAgNnS	být
svrženo	svrhnout	k5eAaPmNgNnS	svrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
zmatků	zmatek	k1gInPc2	zmatek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
Pygmalion	Pygmalion	k1gInSc4	Pygmalion
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
Féničané	Féničan	k1gMnPc1	Féničan
stali	stát	k5eAaPmAgMnP	stát
největší	veliký	k2eAgFnSc7d3	veliký
obchodní	obchodní	k2eAgFnSc7d1	obchodní
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
panování	panování	k1gNnSc2	panování
založila	založit	k5eAaPmAgFnS	založit
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Elisser	Elissero	k1gNnPc2	Elissero
město	město	k1gNnSc1	město
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Fénické	fénický	k2eAgFnPc1d1	fénická
obchodní	obchodní	k2eAgFnPc1d1	obchodní
stanice	stanice	k1gFnPc1	stanice
<g/>
,	,	kIx,	,
roztroušené	roztroušený	k2eAgFnPc1d1	roztroušená
až	až	k9	až
po	po	k7c6	po
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
základnami	základna	k1gFnPc7	základna
mořeplavby	mořeplavba	k1gFnSc2	mořeplavba
a	a	k8xC	a
překladišti	překladiště	k1gNnSc3	překladiště
zboží	zboží	k1gNnSc2	zboží
pro	pro	k7c4	pro
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
svou	svůj	k3xOyFgFnSc4	svůj
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
chrámy	chrám	k1gInPc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Féničané	Féničan	k1gMnPc1	Féničan
ale	ale	k9	ale
přitom	přitom	k6eAd1	přitom
neusilovali	usilovat	k5eNaImAgMnP	usilovat
o	o	k7c4	o
osídlování	osídlování	k1gNnSc4	osídlování
(	(	kIx(	(
<g/>
kolonizaci	kolonizace	k1gFnSc4	kolonizace
<g/>
)	)	kIx)	)
cizích	cizí	k2eAgNnPc2d1	cizí
území	území	k1gNnPc2	území
ani	ani	k8xC	ani
o	o	k7c6	o
soužití	soužití	k1gNnSc6	soužití
s	s	k7c7	s
místním	místní	k2eAgNnSc7d1	místní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
:	:	kIx,	:
žili	žít	k5eAaImAgMnP	žít
jen	jen	k9	jen
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
nepřijímali	přijímat	k5eNaImAgMnP	přijímat
cizí	cizí	k2eAgInPc4d1	cizí
zvyky	zvyk	k1gInPc4	zvyk
nebo	nebo	k8xC	nebo
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
exportním	exportní	k2eAgInSc7d1	exportní
artiklem	artikl	k1gInSc7	artikl
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
kovové	kovový	k2eAgInPc1d1	kovový
předměty	předmět	k1gInPc1	předmět
včetně	včetně	k7c2	včetně
klenotů	klenot	k1gInPc2	klenot
a	a	k8xC	a
purpurové	purpurový	k2eAgFnSc2d1	purpurová
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
dovážela	dovážet	k5eAaImAgFnS	dovážet
až	až	k9	až
z	z	k7c2	z
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
byl	být	k5eAaImAgInS	být
i	i	k9	i
trh	trh	k1gInSc1	trh
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
:	:	kIx,	:
féničtí	fénický	k2eAgMnPc1d1	fénický
kupci	kupec	k1gMnPc1	kupec
chodili	chodit	k5eAaImAgMnP	chodit
za	za	k7c7	za
armádami	armáda	k1gFnPc7	armáda
a	a	k8xC	a
skupovali	skupovat	k5eAaImAgMnP	skupovat
zajatce	zajatec	k1gMnSc4	zajatec
jako	jako	k8xC	jako
otroky	otrok	k1gMnPc4	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nebezpečným	bezpečný	k2eNgMnPc3d1	nebezpečný
konkurentům	konkurent	k1gMnPc3	konkurent
neváhali	váhat	k5eNaImAgMnP	váhat
použít	použít	k5eAaPmF	použít
zbraně	zbraň	k1gFnPc4	zbraň
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hispánského	hispánský	k2eAgInSc2d1	hispánský
Tartessonu	Tartesson	k1gInSc2	Tartesson
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zničili	zničit	k5eAaPmAgMnP	zničit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepřekážel	překážet	k5eNaImAgMnS	překážet
rozvoji	rozvoj	k1gInSc3	rozvoj
jejich	jejich	k3xOp3gNnSc2	jejich
vlastního	vlastní	k2eAgNnSc2d1	vlastní
střediska	středisko	k1gNnSc2	středisko
Gades	Gades	k1gInSc1	Gades
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Cádiz	Cádiz	k1gInSc1	Cádiz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
850	[number]	k4	850
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
moc	moc	k6eAd1	moc
v	v	k7c6	v
Týru	Týrus	k1gInSc6	Týrus
oslabena	oslabit	k5eAaPmNgFnS	oslabit
jednak	jednak	k8xC	jednak
založením	založení	k1gNnSc7	založení
Kartága	Kartágo	k1gNnSc2	Kartágo
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
odlivem	odliv	k1gInSc7	odliv
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Fénicie	Fénicie	k1gFnSc1	Fénicie
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
obsazována	obsazovat	k5eAaImNgFnS	obsazovat
jinými	jiný	k2eAgFnPc7d1	jiná
říšemi	říš	k1gFnPc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
605	[number]	k4	605
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Karchemiše	Karchemiše	k1gFnSc2	Karchemiše
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Fénicie	Fénicie	k1gFnSc1	Fénicie
babylonskou	babylonský	k2eAgFnSc4d1	Babylonská
provincii	provincie	k1gFnSc4	provincie
-	-	kIx~	-
až	až	k9	až
na	na	k7c4	na
Týros	Týros	k1gInSc4	Týros
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
573	[number]	k4	573
př	př	kA	př
<g/>
.	.	kIx.	.
n	n	k0	n
l.	l.	k?	l.
po	po	k7c6	po
třináctiletém	třináctiletý	k2eAgNnSc6d1	třináctileté
obléhání	obléhání	k1gNnSc6	obléhání
<g/>
!	!	kIx.	!
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
538	[number]	k4	538
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Fénicie	Fénicie	k1gFnSc1	Fénicie
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Perské	perský	k2eAgFnSc2d1	perská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
perské	perský	k2eAgFnSc2d1	perská
námořní	námořní	k2eAgFnSc2d1	námořní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
332	[number]	k4	332
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Fénicii	Fénicie	k1gFnSc4	Fénicie
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
Fénicie	Fénicie	k1gFnSc1	Fénicie
tak	tak	k6eAd1	tak
natrvalo	natrvalo	k6eAd1	natrvalo
ztratila	ztratit	k5eAaPmAgFnS	ztratit
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgInSc4d1	dnešní
Libanon	Libanon	k1gInSc4	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Fénicie	Fénicie	k1gFnSc1	Fénicie
Féničtina	Féničtina	k1gFnSc1	Féničtina
Kartágo	Kartágo	k1gNnSc1	Kartágo
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Féničané	Féničan	k1gMnPc1	Féničan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Staré	Staré	k2eAgInPc1d1	Staré
národy	národ	k1gInPc1	národ
|	|	kIx~	|
civilizace	civilizace	k1gFnSc1	civilizace
-	-	kIx~	-
Féničané	Féničan	k1gMnPc1	Féničan
</s>
