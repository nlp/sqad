měkký	měkký	k2eAgInSc1d1	měkký
hmyz	hmyz	k1gInSc1	hmyz
a	a	k8xC	a
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
postupně	postupně	k6eAd1	postupně
podíl	podíl	k1gInSc4	podíl
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
klesá	klesat	k5eAaImIp3nS	klesat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
složky	složka	k1gFnSc2	složka
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k9	především
semena	semeno	k1gNnPc4	semeno
trav	tráva	k1gFnPc2	tráva
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
listy	list	k1gInPc4	list
a	a	k8xC	a
květy	květ	k1gInPc4	květ
