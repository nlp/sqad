<p>
<s>
Křepelka	křepelka	k1gFnSc1	křepelka
polní	polní	k2eAgFnSc1d1	polní
(	(	kIx(	(
<g/>
Coturnix	Coturnix	k1gInSc1	Coturnix
coturnix	coturnix	k1gInSc1	coturnix
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgMnSc1d3	nejmenší
evropský	evropský	k2eAgMnSc1d1	evropský
zástupce	zástupce	k1gMnSc1	zástupce
řádu	řád	k1gInSc2	řád
hrabavých	hrabavý	k2eAgNnPc2d1	hrabavé
a	a	k8xC	a
čeledi	čeleď	k1gFnSc2	čeleď
bažantovitých	bažantovitý	k2eAgMnPc2d1	bažantovitý
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
vysloveně	vysloveně	k6eAd1	vysloveně
stěhovavým	stěhovavý	k2eAgMnSc7d1	stěhovavý
hrabavým	hrabavý	k2eAgMnSc7d1	hrabavý
ptákem	pták	k1gMnSc7	pták
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Křepelka	křepelka	k1gFnSc1	křepelka
má	mít	k5eAaImIp3nS	mít
zavalité	zavalitý	k2eAgNnSc4d1	zavalité
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
krátkýma	krátký	k2eAgFnPc7d1	krátká
nohama	noha	k1gFnPc7	noha
i	i	k8xC	i
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
zakulacenými	zakulacený	k2eAgInPc7d1	zakulacený
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
má	mít	k5eAaImIp3nS	mít
výrazné	výrazný	k2eAgNnSc1d1	výrazné
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgNnSc4d1	malé
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
silný	silný	k2eAgInSc4d1	silný
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
zahnutý	zahnutý	k2eAgInSc4d1	zahnutý
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnPc4	délka
těla	tělo	k1gNnSc2	tělo
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
cm	cm	kA	cm
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
91	[number]	k4	91
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
g.	g.	k?	g.
Je	být	k5eAaImIp3nS	být
nenápadně	nápadně	k6eNd1	nápadně
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
–	–	k?	–
světlehnědá	světlehnědý	k2eAgFnSc1d1	světlehnědá
se	s	k7c7	s
světlejším	světlý	k2eAgNnSc7d2	světlejší
břichem	břicho	k1gNnSc7	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peřit	k5eAaImIp3nS	peřit
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
je	být	k5eAaImIp3nS	být
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
tmavohnědými	tmavohnědý	k2eAgFnPc7d1	tmavohnědá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
má	mít	k5eAaImIp3nS	mít
bělavé	bělavý	k2eAgInPc4d1	bělavý
proužky	proužek	k1gInPc4	proužek
<g/>
,	,	kIx,	,
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
bývá	bývat	k5eAaImIp3nS	bývat
vyvinuta	vyvinut	k2eAgFnSc1d1	vyvinuta
tmavá	tmavý	k2eAgFnSc1d1	tmavá
"	"	kIx"	"
<g/>
čepička	čepička	k1gFnSc1	čepička
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
černý	černý	k2eAgInSc4d1	černý
pruh	pruh	k1gInSc4	pruh
uprostřed	uprostřed	k7c2	uprostřed
hrdla	hrdlo	k1gNnSc2	hrdlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Křepelka	křepelka	k1gFnSc1	křepelka
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
krajině	krajina	k1gFnSc6	krajina
s	s	k7c7	s
pastvinami	pastvina	k1gFnPc7	pastvina
a	a	k8xC	a
obilnými	obilný	k2eAgNnPc7d1	obilné
poli	pole	k1gNnPc7	pole
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
v	v	k7c6	v
lesostepích	lesostep	k1gFnPc6	lesostep
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
především	především	k9	především
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
však	však	k9	však
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
i	i	k9	i
na	na	k7c6	na
hřebeni	hřeben	k1gInSc6	hřeben
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
a	a	k8xC	a
Hrubého	Hrubého	k2eAgInSc2d1	Hrubého
Jeseníku	Jeseník	k1gInSc2	Jeseník
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
5000-10	[number]	k4	5000-10
000	[number]	k4	000
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Křepelka	křepelka	k1gFnSc1	křepelka
polní	polní	k2eAgFnSc1d1	polní
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
roztroušeně	roztroušeně	k6eAd1	roztroušeně
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
od	od	k7c2	od
jižní	jižní	k2eAgFnSc2d1	jižní
čísti	číst	k5eAaImF	číst
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
po	po	k7c4	po
Řecko	Řecko	k1gNnSc4	Řecko
a	a	k8xC	a
Ural	Ural	k1gInSc4	Ural
<g/>
,	,	kIx,	,
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
se	se	k3xPyFc4	se
vysokým	vysoký	k2eAgFnPc3d1	vysoká
horám	hora	k1gFnPc3	hora
(	(	kIx(	(
<g/>
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Karpaty	Karpaty	k1gInPc1	Karpaty
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
)	)	kIx)	)
a	a	k8xC	a
souvislým	souvislý	k2eAgInPc3d1	souvislý
lesním	lesní	k2eAgInPc3d1	lesní
porostům	porost	k1gInPc3	porost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc6	Írán
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
<g/>
,	,	kIx,	,
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc6d1	východní
hranici	hranice	k1gFnSc6	hranice
jejího	její	k3xOp3gNnSc2	její
rozšíření	rozšíření	k1gNnSc2	rozšíření
tvoří	tvořit	k5eAaImIp3nS	tvořit
Bajkal	Bajkal	k1gInSc1	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
se	se	k3xPyFc4	se
vysokým	vysoký	k2eAgFnPc3d1	vysoká
horám	hora	k1gFnPc3	hora
a	a	k8xC	a
pouštím	poušť	k1gFnPc3	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
vyčleňovaná	vyčleňovaný	k2eAgFnSc1d1	vyčleňovaný
jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
poddruh	poddruh	k1gInSc1	poddruh
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
savanách	savana	k1gFnPc6	savana
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Africké	africký	k2eAgFnPc1d1	africká
křepelky	křepelka	k1gFnPc1	křepelka
nejsou	být	k5eNaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
evropských	evropský	k2eAgMnPc2d1	evropský
a	a	k8xC	a
asijských	asijský	k2eAgMnPc2d1	asijský
ptáků	pták	k1gMnPc2	pták
stěhovavé	stěhovavý	k2eAgInPc1d1	stěhovavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Migrace	migrace	k1gFnSc1	migrace
==	==	k?	==
</s>
</p>
<p>
<s>
Křepelka	křepelka	k1gFnSc1	křepelka
polní	polní	k2eAgFnSc1d1	polní
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
typicky	typicky	k6eAd1	typicky
stěhovavých	stěhovavý	k2eAgInPc2d1	stěhovavý
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
hrabavých	hrabavý	k2eAgFnPc2d1	hrabavá
<g/>
.	.	kIx.	.
</s>
<s>
Křepelky	křepelka	k1gFnPc1	křepelka
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
odlétají	odlétat	k5eAaPmIp3nP	odlétat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
vracejí	vracet	k5eAaImIp3nP	vracet
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgFnPc1d1	Evropská
křepelky	křepelka	k1gFnPc1	křepelka
zimují	zimovat	k5eAaImIp3nP	zimovat
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Maroku	Maroko	k1gNnSc6	Maroko
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sahelu	Sahel	k1gInSc2	Sahel
<g/>
.	.	kIx.	.
</s>
<s>
Křepelky	křepelka	k1gFnPc1	křepelka
z	z	k7c2	z
asijských	asijský	k2eAgFnPc2d1	asijská
populací	populace	k1gFnPc2	populace
zimují	zimovat	k5eAaImIp3nP	zimovat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odletem	odlet	k1gInSc7	odlet
křepelky	křepelka	k1gFnSc2	křepelka
zkonzumují	zkonzumovat	k5eAaPmIp3nP	zkonzumovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zvýšením	zvýšení	k1gNnSc7	zvýšení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
tukové	tukový	k2eAgFnSc2d1	tuková
vrstvy	vrstva	k1gFnSc2	vrstva
pod	pod	k7c7	pod
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pro	pro	k7c4	pro
migující	migující	k2eAgMnPc4d1	migující
ptáky	pták	k1gMnPc4	pták
představuje	představovat	k5eAaImIp3nS	představovat
energetickou	energetický	k2eAgFnSc4d1	energetická
rezervu	rezerva	k1gFnSc4	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Migrující	migrující	k2eAgFnPc1d1	migrující
křepelky	křepelka	k1gFnPc1	křepelka
jsou	být	k5eAaImIp3nP	být
místy	místy	k6eAd1	místy
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
či	či	k8xC	či
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
chytány	chytán	k2eAgFnPc1d1	chytána
do	do	k7c2	do
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
konzumovány	konzumován	k2eAgFnPc1d1	konzumována
<g/>
,	,	kIx,	,
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dokonce	dokonce	k9	dokonce
i	i	k9	i
masové	masový	k2eAgFnPc4d1	masová
konzervy	konzerva	k1gFnPc4	konzerva
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
odedávna	odedávna	k6eAd1	odedávna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
(	(	kIx(	(
<g/>
Ex	ex	k6eAd1	ex
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
zázračném	zázračný	k2eAgInSc6d1	zázračný
příletu	přílet	k1gInSc6	přílet
křepelek	křepelka	k1gFnPc2	křepelka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zachránil	zachránit	k5eAaPmAgInS	zachránit
hladovějící	hladovějící	k2eAgMnPc4d1	hladovějící
Izraelity	izraelita	k1gMnPc4	izraelita
v	v	k7c6	v
Sinajské	sinajský	k2eAgFnSc6d1	Sinajská
poušti	poušť	k1gFnSc6	poušť
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
staré	starý	k2eAgFnSc2d1	stará
pověry	pověra	k1gFnSc2	pověra
je	být	k5eAaImIp3nS	být
každé	každý	k3xTgNnSc1	každý
hejno	hejno	k1gNnSc1	hejno
táhnoucích	táhnoucí	k2eAgFnPc2d1	táhnoucí
křepelek	křepelka	k1gFnPc2	křepelka
vedeno	vést	k5eAaImNgNnS	vést
jedním	jeden	k4xCgInSc7	jeden
chřástalem	chřástal	k1gMnSc7	chřástal
polním	polní	k2eAgMnSc7d1	polní
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
chřástal	chřástal	k1gMnSc1	chřástal
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
německy	německy	k6eAd1	německy
der	drát	k5eAaImRp2nS	drát
Wachtelkönig	Wachtelkönig	k1gMnSc1	Wachtelkönig
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
král	král	k1gMnSc1	král
křepelek	křepelka	k1gFnPc2	křepelka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Migrace	migrace	k1gFnSc1	migrace
chřástalů	chřástal	k1gMnPc2	chřástal
a	a	k8xC	a
křepelek	křepelka	k1gFnPc2	křepelka
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
oba	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
táhnou	táhnout	k5eAaImIp3nP	táhnout
především	především	k9	především
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc4d1	společný
tah	tah	k1gInSc4	tah
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
hejnu	hejno	k1gNnSc6	hejno
však	však	k8xC	však
pozorován	pozorován	k2eAgMnSc1d1	pozorován
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
příbuzné	příbuzný	k2eAgFnSc2d1	příbuzná
koroptve	koroptev	k1gFnSc2	koroptev
je	být	k5eAaImIp3nS	být
křepelka	křepelka	k1gFnSc1	křepelka
polní	polní	k2eAgFnSc2d1	polní
polygamní	polygamní	k2eAgInSc4d1	polygamní
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Kohoutcí	Kohoutcí	k1gMnPc1	Kohoutcí
začínají	začínat	k5eAaImIp3nP	začínat
tokat	tokat	k5eAaImF	tokat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
příletu	přílet	k1gInSc6	přílet
na	na	k7c4	na
hnízdiště	hnízdiště	k1gNnPc4	hnízdiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
toku	tok	k1gInSc2	tok
se	se	k3xPyFc4	se
sameček	sameček	k1gMnSc1	sameček
křepelky	křepelka	k1gFnSc2	křepelka
ozývá	ozývat	k5eAaImIp3nS	ozývat
charakteristkcým	charakteristkcé	k1gNnSc7	charakteristkcé
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
slyšitelným	slyšitelný	k2eAgNnPc3d1	slyšitelné
trojslabičným	trojslabičný	k2eAgNnPc3d1	trojslabičné
voláním	volání	k1gNnPc3	volání
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
pik	pik	k1gInSc1	pik
per	pero	k1gNnPc2	pero
vik	vika	k1gFnPc2	vika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
křepelky	křepelka	k1gFnPc1	křepelka
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
jejich	jejich	k3xOp3gInSc1	jejich
hlas	hlas	k1gInSc1	hlas
převáděn	převádět	k5eAaImNgInS	převádět
do	do	k7c2	do
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
křepelka	křepelka	k1gFnSc1	křepelka
volá	volat	k5eAaImIp3nS	volat
"	"	kIx"	"
<g/>
Pět	pět	k4xCc1	pět
peněz	peníze	k1gInPc2	peníze
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Pojď	jít	k5eAaImRp2nS	jít
pod	pod	k7c4	pod
mez	mez	k1gFnSc4	mez
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
"	"	kIx"	"
<g/>
Bück	Bück	k1gInSc1	Bück
den	den	k1gInSc1	den
Rück	Rück	k1gInSc1	Rück
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Bitt	Bitt	k1gInSc1	Bitt
für	für	k?	für
uns	uns	k?	uns
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
"	"	kIx"	"
<g/>
Whet	Whet	k1gInSc1	Whet
my	my	k3xPp1nPc1	my
feet	feetum	k1gNnPc2	feetum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
srbochorvatštině	srbochorvatština	k1gFnSc6	srbochorvatština
"	"	kIx"	"
<g/>
Puć	Puć	k1gFnSc1	Puć
puruć	puruć	k?	puruć
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
"	"	kIx"	"
<g/>
Pity	pit	k2eAgFnPc1d1	pita
palaty	palata	k1gFnPc1	palata
<g/>
"	"	kIx"	"
a	a	k8xC	a
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Alberta	Albert	k1gMnSc2	Albert
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dic	Dic	k1gMnSc1	Dic
<g/>
,	,	kIx,	,
cur	cur	k?	cur
hic	hic	k?	hic
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Napodobené	napodobený	k2eAgNnSc1d1	napodobené
křepelčí	křepelčí	k2eAgNnSc1d1	křepelčí
volání	volání	k1gNnSc1	volání
se	se	k3xPyFc4	se
jako	jako	k9	jako
motiv	motiv	k1gInSc1	motiv
objevuje	objevovat	k5eAaImIp3nS	objevovat
často	často	k6eAd1	často
i	i	k9	i
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
využili	využít	k5eAaPmAgMnP	využít
jej	on	k3xPp3gMnSc4	on
např.	např.	kA	např.
český	český	k2eAgMnSc1d1	český
skladatel	skladatel	k1gMnSc1	skladatel
Leopold	Leopold	k1gMnSc1	Leopold
Koželuh	koželuh	k1gMnSc1	koželuh
nebo	nebo	k8xC	nebo
Rakušan	Rakušan	k1gMnSc1	Rakušan
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Symfonie-	Symfonie-	k?	Symfonie-
Pastorální	pastorální	k2eAgFnPc4d1	pastorální
<g/>
)	)	kIx)	)
<g/>
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Haydn	Haydn	k1gMnSc1	Haydn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kohoutci	Kohoutek	k1gMnPc1	Kohoutek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
toku	tok	k1gInSc6	tok
velmi	velmi	k6eAd1	velmi
agresívní	agresívní	k2eAgFnPc1d1	agresívní
a	a	k8xC	a
bojují	bojovat	k5eAaImIp3nP	bojovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
využívají	využívat	k5eAaImIp3nP	využívat
lidé	člověk	k1gMnPc1	člověk
např.	např.	kA	např.
v	v	k7c6	v
Ázerbajdžánu	Ázerbajdžán	k1gInSc6	Ázerbajdžán
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
ke	k	k7c3	k
křepelčím	křepelčí	k2eAgInPc3d1	křepelčí
zápasům	zápas	k1gInPc3	zápas
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
krvavou	krvavý	k2eAgFnSc7d1	krvavá
obdobou	obdoba	k1gFnSc7	obdoba
kohoutích	kohoutí	k2eAgInPc2d1	kohoutí
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
června	červen	k1gInSc2	červen
klade	klást	k5eAaImIp3nS	klást
slepička	slepička	k1gFnSc1	slepička
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
žlutohnědých	žlutohnědý	k2eAgFnPc2d1	žlutohnědá
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
kropenatých	kropenatý	k2eAgNnPc2d1	kropenaté
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
důlek	důlek	k1gInSc4	důlek
vyslaný	vyslaný	k2eAgInSc4d1	vyslaný
suchými	suchý	k2eAgNnPc7d1	suché
stébly	stéblo	k1gNnPc7	stéblo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
trochou	trocha	k1gFnSc7	trocha
peří	peří	k1gNnSc2	peří
<g/>
.	.	kIx.	.
</s>
<s>
Inkubace	inkubace	k1gFnSc1	inkubace
trvá	trvat	k5eAaImIp3nS	trvat
17	[number]	k4	17
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
líhnout	líhnout	k5eAaImF	líhnout
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
prachový	prachový	k2eAgInSc1d1	prachový
šat	šat	k1gInSc1	šat
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
kropenaté	kropenatý	k2eAgNnSc1d1	kropenaté
zbarvení	zbarvení	k1gNnSc1	zbarvení
jako	jako	k8xS	jako
skořápka	skořápka	k1gFnSc1	skořápka
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
hrabavých	hrabavý	k2eAgMnPc2d1	hrabavý
<g/>
,	,	kIx,	,
nekrmivá	krmivý	k2eNgFnSc1d1	nekrmivá
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
vodí	vodit	k5eAaImIp3nS	vodit
asi	asi	k9	asi
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kohoutek	kohoutek	k1gInSc1	kohoutek
se	se	k3xPyFc4	se
nepodílí	podílet	k5eNaImIp3nS	podílet
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Potravu	potrava	k1gFnSc4	potrava
mláďat	mládě	k1gNnPc2	mládě
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k6eAd1	především
měkký	měkký	k2eAgInSc1d1	měkký
hmyz	hmyz	k1gInSc1	hmyz
a	a	k8xC	a
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
postupně	postupně	k6eAd1	postupně
podíl	podíl	k1gInSc4	podíl
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
klesá	klesat	k5eAaImIp3nS	klesat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
složky	složka	k1gFnSc2	složka
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k9	především
semena	semeno	k1gNnPc4	semeno
trav	tráva	k1gFnPc2	tráva
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
listy	list	k1gInPc4	list
a	a	k8xC	a
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Křepelka	křepelka	k1gFnSc1	křepelka
polní	polní	k2eAgFnSc1d1	polní
je	být	k5eAaImIp3nS	být
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k8xS	jako
ubývající	ubývající	k2eAgInSc4d1	ubývající
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
ji	on	k3xPp3gFnSc4	on
zejména	zejména	k9	zejména
lov	lov	k1gInSc4	lov
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tahu	tah	k1gInSc2	tah
ve	v	k7c6	v
středomořské	středomořský	k2eAgFnSc6d1	středomořská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
křepelka	křepelka	k1gFnSc1	křepelka
není	být	k5eNaImIp3nS	být
lovným	lovný	k2eAgInSc7d1	lovný
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hnízdní	hnízdní	k2eAgFnSc6d1	hnízdní
době	doba	k1gFnSc6	doba
ji	on	k3xPp3gFnSc4	on
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
chřástala	chřástal	k1gMnSc4	chřástal
polního	polní	k2eAgMnSc4d1	polní
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
necitlivé	citlivý	k2eNgNnSc4d1	necitlivé
vyžínání	vyžínání	k1gNnSc4	vyžínání
lučních	luční	k2eAgInPc2d1	luční
porostů	porost	k1gInPc2	porost
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
(	(	kIx(	(
<g/>
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
provádět	provádět	k5eAaImF	provádět
až	až	k9	až
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
nebo	nebo	k8xC	nebo
počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
egyptských	egyptský	k2eAgInPc6d1	egyptský
hieroglyfech	hieroglyf	k1gInPc6	hieroglyf
představuje	představovat	k5eAaImIp3nS	představovat
obrázek	obrázek	k1gInSc1	obrázek
křepelky	křepelka	k1gFnSc2	křepelka
hlásku	hlásek	k1gInSc2	hlásek
w	w	k?	w
nebo	nebo	k8xC	nebo
u	u	k7c2	u
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
však	však	k9	však
tímto	tento	k3xDgMnSc7	tento
ptákem	pták	k1gMnSc7	pták
není	být	k5eNaImIp3nS	být
křepelka	křepelka	k1gFnSc1	křepelka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
kulík	kulík	k1gMnSc1	kulík
nilský	nilský	k2eAgMnSc1d1	nilský
(	(	kIx(	(
<g/>
Pluvianus	Pluvianus	k1gMnSc1	Pluvianus
aegyptius	aegyptius	k1gMnSc1	aegyptius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
staré	starý	k2eAgMnPc4d1	starý
Egypťany	Egypťan	k1gMnPc4	Egypťan
měla	mít	k5eAaImAgFnS	mít
nicméně	nicméně	k8xC	nicméně
křepelka	křepelka	k1gFnSc1	křepelka
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc4	význam
jako	jako	k8xC	jako
lovný	lovný	k2eAgMnSc1d1	lovný
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
křepelky	křepelka	k1gFnPc1	křepelka
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
tahu	tah	k1gInSc6	tah
chytány	chytán	k2eAgInPc4d1	chytán
do	do	k7c2	do
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
buďto	buďto	k8xC	buďto
přímo	přímo	k6eAd1	přímo
konzumovány	konzumován	k2eAgInPc1d1	konzumován
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vykrmovány	vykrmován	k2eAgFnPc4d1	vykrmována
v	v	k7c6	v
klecích	klek	k1gInPc6	klek
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
přeraženými	přeražený	k2eAgNnPc7d1	přeražené
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
křepelka	křepelka	k1gFnSc1	křepelka
polní	polní	k2eAgFnSc1d1	polní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
křepelka	křepelka	k1gFnSc1	křepelka
polní	polní	k2eAgFnSc1d1	polní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Coturnix	Coturnix	k1gInSc1	Coturnix
coturnix	coturnix	k1gInSc1	coturnix
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
