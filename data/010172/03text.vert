<p>
<s>
Hewlett-Packard	Hewlett-Packard	k1gInSc1	Hewlett-Packard
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
HP	HP	kA	HP
(	(	kIx(	(
<g/>
NYSE	NYSE	kA	NYSE
<g/>
:	:	kIx,	:
HPQ	HPQ	kA	HPQ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nadnárodní	nadnárodní	k2eAgFnSc4d1	nadnárodní
společnost	společnost	k1gFnSc4	společnost
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	se	k3xPyFc4	se
informačními	informační	k2eAgFnPc7d1	informační
technologiemi	technologie	k1gFnPc7	technologie
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Palo	Pala	k1gMnSc5	Pala
Alto	Alto	k1gNnSc1	Alto
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Hewlett	Hewlett	kA	Hewlett
-	-	kIx~	-
Packard	Packard	kA	Packard
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
společností	společnost	k1gFnPc2	společnost
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
na	na	k7c6	na
světě	svět	k1gInSc6	svět
působící	působící	k2eAgNnSc1d1	působící
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
<g/>
,	,	kIx,	,
paměťového	paměťový	k2eAgInSc2d1	paměťový
a	a	k8xC	a
síťového	síťový	k2eAgInSc2d1	síťový
hardwaru	hardware	k1gInSc2	hardware
<g/>
,	,	kIx,	,
softwaru	software	k1gInSc2	software
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
svými	svůj	k3xOyFgFnPc7	svůj
tiskárnami	tiskárna	k1gFnPc7	tiskárna
<g/>
,	,	kIx,	,
osobními	osobní	k2eAgInPc7d1	osobní
počítači	počítač	k1gInPc7	počítač
<g/>
,	,	kIx,	,
scannery	scanner	k1gInPc7	scanner
<g/>
,	,	kIx,	,
kapesními	kapesní	k2eAgInPc7d1	kapesní
počítači	počítač	k1gInPc7	počítač
a	a	k8xC	a
servery	server	k1gInPc7	server
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HP	HP	kA	HP
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
domácnosti	domácnost	k1gFnPc4	domácnost
a	a	k8xC	a
domácí	domácí	k2eAgFnPc4d1	domácí
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc4d1	malá
a	a	k8xC	a
střední	střední	k2eAgFnPc4d1	střední
firmy	firma	k1gFnPc4	firma
<g/>
,	,	kIx,	,
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
správu	správa	k1gFnSc4	správa
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
grafiku	grafika	k1gFnSc4	grafika
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
rok	rok	k1gInSc4	rok
2006	[number]	k4	2006
měla	mít	k5eAaImAgFnS	mít
HP	HP	kA	HP
tržby	tržba	k1gFnSc2	tržba
91,7	[number]	k4	91,7
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
firmou	firma	k1gFnSc7	firma
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
příjmy	příjem	k1gInPc7	příjem
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
příjmů	příjem	k1gInPc2	příjem
až	až	k9	až
104	[number]	k4	104
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc6	první
IT	IT	kA	IT
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
příjmy	příjem	k1gInPc7	příjem
přes	přes	k7c4	přes
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HP	HP	kA	HP
je	být	k5eAaImIp3nS	být
také	také	k9	také
největším	veliký	k2eAgMnSc7d3	veliký
prodejcem	prodejce	k1gMnSc7	prodejce
osobních	osobní	k2eAgInPc2d1	osobní
počítačů	počítač	k1gInPc2	počítač
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
překonal	překonat	k5eAaPmAgMnS	překonat
i	i	k9	i
společnost	společnost	k1gFnSc4	společnost
Dell	Dell	kA	Dell
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
z	z	k7c2	z
průzkumu	průzkum	k1gInSc2	průzkum
firmy	firma	k1gFnSc2	firma
Gartner	Gartner	k1gInSc1	Gartner
a	a	k8xC	a
IDC	IDC	kA	IDC
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
HP	HP	kA	HP
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
softwarovými	softwarový	k2eAgFnPc7d1	softwarová
společnostmi	společnost	k1gFnPc7	společnost
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
na	na	k7c6	na
Stanfordově	Stanfordův	k2eAgFnSc6d1	Stanfordova
univerzitě	univerzita	k1gFnSc6	univerzita
dostudovali	dostudovat	k5eAaPmAgMnP	dostudovat
Bill	Bill	k1gMnSc1	Bill
Hewlett	Hewlett	kA	Hewlett
a	a	k8xC	a
David	David	k1gMnSc1	David
Packard	Packard	kA	Packard
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
garáži	garáž	k1gFnSc6	garáž
nedaleko	nedaleko	k7c2	nedaleko
Palo	Pala	k1gMnSc5	Pala
Alto	Alto	k1gNnSc4	Alto
během	během	k7c2	během
Světové	světový	k2eAgFnSc2d1	světová
krize	krize	k1gFnSc2	krize
společnost	společnost	k1gFnSc1	společnost
Hewlett-Packard	Hewlett-Packard	k1gInSc1	Hewlett-Packard
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Formálně	formálně	k6eAd1	formálně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
společnost	společnost	k1gFnSc1	společnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1939	[number]	k4	1939
díky	díky	k7c3	díky
investici	investice	k1gFnSc3	investice
538	[number]	k4	538
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Hewlett	Hewlett	kA	Hewlett
a	a	k8xC	a
Packard	packard	k1gInSc4	packard
si	se	k3xPyFc3	se
tehdy	tehdy	k6eAd1	tehdy
"	"	kIx"	"
<g/>
hodili	hodit	k5eAaPmAgMnP	hodit
korunou	koruna	k1gFnSc7	koruna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaImF	jmenovat
Hewlett-Packard	Hewlett-Packard	k1gInSc4	Hewlett-Packard
nebo	nebo	k8xC	nebo
Packard-Hewlett	Packard-Hewlett	k1gInSc4	Packard-Hewlett
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Packard	Packard	kA	Packard
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přesto	přesto	k8xC	přesto
jejich	jejich	k3xOp3gFnSc4	jejich
manufakturu	manufaktura	k1gFnSc4	manufaktura
s	s	k7c7	s
elektronikou	elektronika	k1gFnSc7	elektronika
nazval	nazvat	k5eAaPmAgInS	nazvat
"	"	kIx"	"
<g/>
Hewlett-Packard	Hewlett-Packard	k1gInSc1	Hewlett-Packard
Company	Compana	k1gFnSc2	Compana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akciovou	akciový	k2eAgFnSc7d1	akciová
společností	společnost	k1gFnSc7	společnost
se	s	k7c7	s
HP	HP	kA	HP
stala	stát	k5eAaPmAgFnS	stát
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnPc1	její
akcie	akcie	k1gFnPc1	akcie
veřejně	veřejně	k6eAd1	veřejně
obchodovatelné	obchodovatelný	k2eAgFnPc1d1	obchodovatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
vynálezy	vynález	k1gInPc4	vynález
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
finančně	finančně	k6eAd1	finančně
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
produktem	produkt	k1gInSc7	produkt
byl	být	k5eAaImAgInS	být
přesný	přesný	k2eAgInSc1d1	přesný
audiooscilátor	audiooscilátor	k1gInSc1	audiooscilátor
<g/>
,	,	kIx,	,
Model	model	k1gInSc1	model
200	[number]	k4	200
<g/>
A.	A.	kA	A.
Inovací	inovace	k1gFnPc2	inovace
bylo	být	k5eAaImAgNnS	být
použití	použití	k1gNnSc4	použití
malé	malý	k2eAgFnSc2d1	malá
žárovky	žárovka	k1gFnSc2	žárovka
jako	jako	k8xS	jako
teplotně	teplotně	k6eAd1	teplotně
závislého	závislý	k2eAgInSc2d1	závislý
rezistoru	rezistor	k1gInSc2	rezistor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
prodávat	prodávat	k5eAaImF	prodávat
Model	model	k1gInSc4	model
200A	[number]	k4	200A
za	za	k7c4	za
54.4	[number]	k4	54.4
USD	USD	kA	USD
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
konkurence	konkurence	k1gFnSc1	konkurence
nabízela	nabízet	k5eAaImAgFnS	nabízet
méně	málo	k6eAd2	málo
stabilní	stabilní	k2eAgInPc4d1	stabilní
produkty	produkt	k1gInPc4	produkt
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgMnPc7	první
zákazníky	zákazník	k1gMnPc7	zákazník
nové	nový	k2eAgFnSc2d1	nová
firmy	firma	k1gFnSc2	firma
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
animační	animační	k2eAgNnSc1d1	animační
studio	studio	k1gNnSc1	studio
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
8	[number]	k4	8
oscilátorů	oscilátor	k1gInPc2	oscilátor
po	po	k7c4	po
71,5	[number]	k4	71,5
USD	USD	kA	USD
pro	pro	k7c4	pro
prostorový	prostorový	k2eAgInSc4d1	prostorový
zvuk	zvuk	k1gInSc4	zvuk
k	k	k7c3	k
filmu	film	k1gInSc3	film
Fantasia	Fantasium	k1gNnSc2	Fantasium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInPc4	první
roky	rok	k1gInPc4	rok
===	===	k?	===
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
nebyla	být	k5eNaImAgFnS	být
zprvu	zprvu	k6eAd1	zprvu
nijak	nijak	k6eAd1	nijak
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
a	a	k8xC	a
zabývala	zabývat	k5eAaImAgFnS	zabývat
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
široké	široký	k2eAgFnSc2d1	široká
palety	paleta	k1gFnSc2	paleta
elektronických	elektronický	k2eAgInPc2d1	elektronický
produktů	produkt	k1gInPc2	produkt
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
i	i	k8xC	i
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zvolila	zvolit	k5eAaPmAgFnS	zvolit
zaměření	zaměření	k1gNnSc4	zaměření
na	na	k7c4	na
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
elektronické	elektronický	k2eAgNnSc4d1	elektronické
testovací	testovací	k2eAgNnSc4d1	testovací
a	a	k8xC	a
měřící	měřící	k2eAgNnSc4d1	měřící
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
40	[number]	k4	40
<g/>
.	.	kIx.	.
až	až	k6eAd1	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
zaměřovala	zaměřovat	k5eAaImAgFnS	zaměřovat
především	především	k9	především
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
přesných	přesný	k2eAgInPc2d1	přesný
signálních	signální	k2eAgInPc2d1	signální
generátorů	generátor	k1gInPc2	generátor
<g/>
,	,	kIx,	,
voltmetrů	voltmetr	k1gInPc2	voltmetr
<g/>
,	,	kIx,	,
osciloskopů	osciloskop	k1gInPc2	osciloskop
<g/>
,	,	kIx,	,
kalkulaček	kalkulačka	k1gFnPc2	kalkulačka
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
testovacích	testovací	k2eAgInPc2d1	testovací
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
prvního	první	k4xOgInSc2	první
produktu	produkt	k1gInSc2	produkt
Model	model	k1gInSc4	model
200A	[number]	k4	200A
byly	být	k5eAaImAgFnP	být
nadále	nadále	k6eAd1	nadále
součástky	součástka	k1gFnPc1	součástka
v	v	k7c6	v
testovacích	testovací	k2eAgNnPc6d1	testovací
zařízeních	zařízení	k1gNnPc6	zařízení
označovány	označovat	k5eAaImNgInP	označovat
třemi	tři	k4xCgNnPc7	tři
až	až	k9	až
pěti	pět	k4xCc7	pět
číslicemi	číslice	k1gFnPc7	číslice
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterými	který	k3yIgMnPc7	který
následovalo	následovat	k5eAaImAgNnS	následovat
písmeno	písmeno	k1gNnSc1	písmeno
A.	A.	kA	A.
Vylepšené	vylepšený	k2eAgFnSc2d1	vylepšená
verze	verze	k1gFnSc2	verze
pak	pak	k6eAd1	pak
dostávaly	dostávat	k5eAaImAgInP	dostávat
písmena	písmeno	k1gNnPc4	písmeno
B	B	kA	B
až	až	k8xS	až
E.	E.	kA	E.
Jak	jak	k8xS	jak
společnost	společnost	k1gFnSc1	společnost
stále	stále	k6eAd1	stále
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
označení	označení	k1gNnSc4	označení
produktů	produkt	k1gInPc2	produkt
s	s	k7c7	s
písmeny	písmeno	k1gNnPc7	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
pro	pro	k7c4	pro
příslušenství	příslušenství	k1gNnSc4	příslušenství
<g/>
,	,	kIx,	,
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
software	software	k1gInSc4	software
a	a	k8xC	a
komponenty	komponenta	k1gFnPc4	komponenta
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
předešla	předejít	k5eAaPmAgFnS	předejít
vyčerpání	vyčerpání	k1gNnSc4	vyčerpání
číslic	číslice	k1gFnPc2	číslice
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgInPc4d1	hlavní
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1960	[number]	k4	1960
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
začal	začít	k5eAaPmAgMnS	začít
HP	HP	kA	HP
vyvážet	vyvážet	k5eAaImF	vyvážet
své	svůj	k3xOyFgInPc4	svůj
výrobky	výrobek	k1gInPc4	výrobek
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
centrála	centrála	k1gFnSc1	centrála
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HP	HP	kA	HP
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
stal	stát	k5eAaPmAgMnS	stát
partnerem	partner	k1gMnSc7	partner
japonských	japonský	k2eAgFnPc6d1	japonská
společnostmi	společnost	k1gFnPc7	společnost
Sony	Sony	kA	Sony
a	a	k8xC	a
Yokogawa	Yokogawa	k1gMnSc1	Yokogawa
Electric	Electric	k1gMnSc1	Electric
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
několik	několik	k4yIc4	několik
vysoce-kvalitních	vysocevalitní	k2eAgInPc2d1	vysoce-kvalitní
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Produkty	produkt	k1gInPc1	produkt
však	však	k9	však
nesklidily	sklidit	k5eNaPmAgInP	sklidit
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
na	na	k7c4	na
budování	budování	k1gNnSc4	budování
HP	HP	kA	HP
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
HP	HP	kA	HP
a	a	k8xC	a
Yokogawa	Yokogawa	k1gFnSc1	Yokogawa
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
společnou	společný	k2eAgFnSc4d1	společná
firmu	firma	k1gFnSc4	firma
(	(	kIx(	(
<g/>
Yokogawa-Hewlett-Packard	Yokogawa-Hewlett-Packard	k1gInSc4	Yokogawa-Hewlett-Packard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
trh	trh	k1gInSc4	trh
HP	HP	kA	HP
produktů	produkt	k1gInPc2	produkt
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
HP	HP	kA	HP
koupil	koupit	k5eAaPmAgMnS	koupit
podíl	podíl	k1gInSc4	podíl
v	v	k7c4	v
Yokogawa	Yokogawum	k1gNnPc4	Yokogawum
Electric	Electrice	k1gFnPc2	Electrice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
HP	HP	kA	HP
pochází	pocházet	k5eAaImIp3nS	pocházet
malá	malý	k2eAgFnSc1d1	malá
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
Dynec	Dynec	k1gInSc1	Dynec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
digitální	digitální	k2eAgNnSc4d1	digitální
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
převrácené	převrácený	k2eAgNnSc1d1	převrácené
logo	logo	k1gNnSc1	logo
"	"	kIx"	"
<g/>
hp	hp	k?	hp
<g/>
"	"	kIx"	"
představovalo	představovat	k5eAaImAgNnS	představovat
logo	logo	k1gNnSc1	logo
Dynecu	Dynecus	k1gInSc2	Dynecus
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
dy	dy	k?	dy
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
Dynec	Dynec	k1gInSc1	Dynec
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
Dymec	Dymec	k1gInSc4	Dymec
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
přeložen	přeložen	k2eAgInSc1d1	přeložen
pod	pod	k7c7	pod
HP	HP	kA	HP
<g/>
.	.	kIx.	.
</s>
<s>
HP	HP	kA	HP
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
minipočítačů	minipočítač	k1gInPc2	minipočítač
Digital	Digital	kA	Digital
Equipment	Equipment	k1gInSc1	Equipment
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
koupit	koupit	k5eAaPmF	koupit
jiný	jiný	k2eAgInSc4d1	jiný
malý	malý	k2eAgInSc4d1	malý
tým	tým	k1gInSc4	tým
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
DEC	DEC	kA	DEC
<g/>
,	,	kIx,	,
a	a	k8xC	a
HP	HP	kA	HP
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
na	na	k7c4	na
trh	trh	k1gInSc4	trh
s	s	k7c7	s
minipočítačí	minipočítačit	k5eAaPmIp3nS	minipočítačit
HP	HP	kA	HP
2100	[number]	k4	2100
/	/	kIx~	/
HP	HP	kA	HP
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
počítače	počítač	k1gInPc1	počítač
měly	mít	k5eAaImAgInP	mít
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
ALU	ala	k1gFnSc4	ala
s	s	k7c7	s
registry	registr	k1gInPc7	registr
uspořádanými	uspořádaný	k2eAgInPc7d1	uspořádaný
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Intel	Intel	kA	Intel
x	x	k?	x
<g/>
86	[number]	k4	86
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
architektura	architektura	k1gFnSc1	architektura
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
vyráběna	vyráběn	k2eAgFnSc1d1	vyráběna
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
pokusů	pokus	k1gInPc2	pokus
nahradit	nahradit	k5eAaPmF	nahradit
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
předchůdcem	předchůdce	k1gMnSc7	předchůdce
HP	HP	kA	HP
9800	[number]	k4	9800
a	a	k8xC	a
HP	HP	kA	HP
250	[number]	k4	250
série	série	k1gFnSc1	série
domácích	domácí	k2eAgMnPc2d1	domácí
i	i	k8xC	i
firemních	firemní	k2eAgMnPc2d1	firemní
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1970	[number]	k4	1970
===	===	k?	===
</s>
</p>
<p>
<s>
HP	HP	kA	HP
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Wired	Wired	k1gInSc1	Wired
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
začal	začít	k5eAaPmAgInS	začít
hromadně	hromadně	k6eAd1	hromadně
vyrábět	vyrábět	k5eAaImF	vyrábět
osobní	osobní	k2eAgInSc4d1	osobní
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nesl	nést	k5eAaImAgInS	nést
název	název	k1gInSc1	název
Hewlett-Packard	Hewlett-Packard	k1gInSc1	Hewlett-Packard
9100A	[number]	k4	9100A
a	a	k8xC	a
na	na	k7c4	na
trh	trh	k1gInSc4	trh
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
HP	HP	kA	HP
ho	on	k3xPp3gNnSc4	on
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
jako	jako	k9	jako
stolní	stolní	k2eAgInSc4d1	stolní
kalkulátor	kalkulátor	k1gInSc4	kalkulátor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
Billa	Bill	k1gMnSc2	Bill
Hewletta	Hewlett	k1gMnSc2	Hewlett
se	se	k3xPyFc4	se
nepodobali	podobat	k5eNaImAgMnP	podobat
počítačům	počítač	k1gMnPc3	počítač
firmy	firma	k1gFnSc2	firma
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
inženýrský	inženýrský	k2eAgInSc4d1	inženýrský
triumf	triumf	k1gInSc4	triumf
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
logické	logický	k2eAgInPc1d1	logický
obvody	obvod	k1gInPc1	obvod
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
montáže	montáž	k1gFnSc2	montáž
procesorů	procesor	k1gInPc2	procesor
byly	být	k5eAaImAgInP	být
zcela	zcela	k6eAd1	zcela
provedeny	provést	k5eAaPmNgInP	provést
z	z	k7c2	z
diskrétních	diskrétní	k2eAgFnPc2d1	diskrétní
součástek	součástka	k1gFnPc2	součástka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
CRT	CRT	kA	CRT
displejem	displej	k1gInSc7	displej
<g/>
,	,	kIx,	,
magnetickou	magnetický	k2eAgFnSc7d1	magnetická
pamětí	paměť	k1gFnSc7	paměť
a	a	k8xC	a
tiskárnou	tiskárna	k1gFnSc7	tiskárna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
5000	[number]	k4	5000
<g/>
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Klávesnice	klávesnice	k1gFnSc1	klávesnice
byla	být	k5eAaImAgFnS	být
křížencem	kříženec	k1gMnSc7	kříženec
vědeckého	vědecký	k2eAgInSc2d1	vědecký
kalkulátoru	kalkulátor	k1gInSc2	kalkulátor
a	a	k8xC	a
přídavným	přídavný	k2eAgNnSc7d1	přídavné
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
zde	zde	k6eAd1	zde
abecední	abecední	k2eAgFnSc1d1	abecední
klávesnice	klávesnice	k1gFnSc1	klávesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
respekt	respekt	k1gInSc4	respekt
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
představil	představit	k5eAaPmAgInS	představit
světově	světově	k6eAd1	světově
první	první	k4xOgInSc1	první
kapesní	kapesní	k2eAgInSc1d1	kapesní
vědecký	vědecký	k2eAgInSc1d1	vědecký
elektronický	elektronický	k2eAgInSc1d1	elektronický
kalkulátor	kalkulátor	k1gInSc1	kalkulátor
(	(	kIx(	(
<g/>
HP-	HP-	k1gFnSc1	HP-
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
první	první	k4xOgInSc4	první
kapesní	kapesní	k2eAgInSc4d1	kapesní
počítač	počítač	k1gInSc4	počítač
(	(	kIx(	(
<g/>
HP-	HP-	k1gFnSc1	HP-
<g/>
65	[number]	k4	65
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
alfanumerický	alfanumerický	k2eAgInSc4d1	alfanumerický
<g/>
,	,	kIx,	,
programovatelný	programovatelný	k2eAgInSc4d1	programovatelný
(	(	kIx(	(
<g/>
HP-	HP-	k1gFnSc1	HP-
<g/>
41	[number]	k4	41
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgInSc4	první
symbolický	symbolický	k2eAgInSc4d1	symbolický
a	a	k8xC	a
grafický	grafický	k2eAgInSc4d1	grafický
kalkulátor	kalkulátor	k1gInSc4	kalkulátor
(	(	kIx(	(
<g/>
HP-	HP-	k1gFnSc1	HP-
<g/>
28	[number]	k4	28
<g/>
c	c	k0	c
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jejich	jejich	k3xOp3gFnPc4	jejich
vědecké	vědecký	k2eAgFnPc4d1	vědecká
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc4d1	obchodní
kalkulačky	kalkulačka	k1gFnPc4	kalkulačka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
osciloskopy	osciloskop	k1gInPc4	osciloskop
<g/>
,	,	kIx,	,
logické	logický	k2eAgInPc4d1	logický
analyzátory	analyzátor	k1gInPc4	analyzátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
měřicí	měřicí	k2eAgInPc1d1	měřicí
přístroje	přístroj	k1gInPc1	přístroj
mají	mít	k5eAaImIp3nP	mít
pověst	pověst	k1gFnSc4	pověst
odolnosti	odolnost	k1gFnSc2	odolnost
a	a	k8xC	a
využitelnost	využitelnost	k1gFnSc1	využitelnost
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc1	tento
produkty	produkt	k1gInPc1	produkt
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
součástí	součást	k1gFnSc7	součást
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
produktové	produktový	k2eAgInPc4d1	produktový
řady	řad	k1gInPc4	řad
Agilent	Agilent	k1gInSc1	Agilent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
98	[number]	k4	98
<g/>
x	x	k?	x
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
technických	technický	k2eAgMnPc2d1	technický
stolních	stolní	k2eAgMnPc2d1	stolní
počítačů	počítač	k1gMnPc2	počítač
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
s	s	k7c7	s
9815	[number]	k4	9815
<g/>
,	,	kIx,	,
a	a	k8xC	a
levnější	levný	k2eAgFnSc1d2	levnější
série	série	k1gFnSc1	série
80	[number]	k4	80
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
technických	technický	k2eAgInPc2d1	technický
počítačů	počítač	k1gInPc2	počítač
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
s	s	k7c7	s
85	[number]	k4	85
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
stroje	stroj	k1gInPc1	stroj
používaly	používat	k5eAaImAgInP	používat
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
BASIC	Basic	kA	Basic
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
ihned	ihned	k6eAd1	ihned
po	po	k7c4	po
jejich	jejich	k3xOp3gNnSc4	jejich
zapnutí	zapnutí	k1gNnSc4	zapnutí
a	a	k8xC	a
používal	používat	k5eAaImAgInS	používat
proprietární	proprietární	k2eAgInPc4d1	proprietární
magnetické	magnetický	k2eAgInPc4d1	magnetický
pásky	pásek	k1gInPc4	pásek
pro	pro	k7c4	pro
uchovávání	uchovávání	k1gNnSc4	uchovávání
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
HP	HP	kA	HP
počítače	počítač	k1gInPc1	počítač
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
velmi	velmi	k6eAd1	velmi
podobaly	podobat	k5eAaImAgInP	podobat
svými	svůj	k3xOyFgMnPc7	svůj
schopnostmi	schopnost	k1gFnPc7	schopnost
počítačům	počítač	k1gInPc3	počítač
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
omezeně	omezeně	k6eAd1	omezeně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
technologie	technologie	k1gFnPc1	technologie
zvyšovaly	zvyšovat	k5eAaImAgFnP	zvyšovat
jejich	jejich	k3xOp3gFnPc1	jejich
ceny	cena	k1gFnPc1	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1980	[number]	k4	1980
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
HP	HP	kA	HP
zavedlo	zavést	k5eAaPmAgNnS	zavést
i	i	k8xC	i
inkoustové	inkoustový	k2eAgFnPc1d1	inkoustová
a	a	k8xC	a
laserové	laserový	k2eAgFnPc1d1	laserová
tiskárny	tiskárna	k1gFnPc1	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
skenerů	skener	k1gInPc2	skener
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
tiskáren	tiskárna	k1gFnPc2	tiskárna
později	pozdě	k6eAd2	pozdě
vyvinuty	vyvinut	k2eAgFnPc4d1	vyvinuta
úspěšné	úspěšný	k2eAgFnPc4d1	úspěšná
multifunkční	multifunkční	k2eAgNnPc4d1	multifunkční
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
byly	být	k5eAaImAgFnP	být
samostatné	samostatný	k2eAgFnPc4d1	samostatná
jednotky	jednotka	k1gFnPc4	jednotka
tiskárna	tiskárna	k1gFnSc1	tiskárna
/	/	kIx~	/
skener	skener	k1gInSc1	skener
/	/	kIx~	/
kopírka	kopírka	k1gFnSc1	kopírka
/	/	kIx~	/
fax	fax	k1gInSc1	fax
<g/>
.	.	kIx.	.
</s>
<s>
Tiskové	tiskový	k2eAgInPc4d1	tiskový
mechanismy	mechanismus	k1gInPc4	mechanismus
HP	HP	kA	HP
LaserJet	LaserJet	k1gInSc4	LaserJet
<g/>
,	,	kIx,	,
nesmírně	smírně	k6eNd1	smírně
populární	populární	k2eAgFnPc1d1	populární
řady	řada	k1gFnPc1	řada
laserových	laserový	k2eAgFnPc2d1	laserová
tiskáren	tiskárna	k1gFnPc2	tiskárna
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
závisí	záviset	k5eAaImIp3nS	záviset
komponentách	komponenta	k1gFnPc6	komponenta
Canon	Canon	kA	Canon
<g/>
(	(	kIx(	(
<g/>
tiskové	tiskový	k2eAgInPc4d1	tiskový
motory	motor	k1gInPc4	motor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zase	zase	k9	zase
využívají	využívat	k5eAaPmIp3nP	využívat
technologií	technologie	k1gFnSc7	technologie
vyvinutých	vyvinutý	k2eAgFnPc2d1	vyvinutá
společností	společnost	k1gFnPc2	společnost
Xerox	Xerox	kA	Xerox
<g/>
.	.	kIx.	.
</s>
<s>
HP	HP	kA	HP
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
hardware	hardware	k1gInSc4	hardware
<g/>
,	,	kIx,	,
firmware	firmware	k1gInSc1	firmware
a	a	k8xC	a
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
převáděl	převádět	k5eAaImAgInS	převádět
data	datum	k1gNnPc4	datum
do	do	k7c2	do
teček	tečka	k1gFnPc2	tečka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
mechanismus	mechanismus	k1gInSc4	mechanismus
pro	pro	k7c4	pro
tisk	tisk	k1gInSc4	tisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
garáž	garáž	k1gFnSc1	garáž
v	v	k7c6	v
Palo	Pala	k1gMnSc5	Pala
Alto	Alta	k1gMnSc5	Alta
<g/>
,	,	kIx,	,
kde	kde	k9	kde
Hewlett	Hewlett	kA	Hewlett
a	a	k8xC	a
Packard	Packard	kA	Packard
začali	začít	k5eAaPmAgMnP	začít
své	své	k1gNnSc4	své
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xC	jako
státní	státní	k2eAgFnSc1d1	státní
památka	památka	k1gFnSc1	památka
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roku	rok	k1gInSc6	rok
1990	[number]	k4	1990
HP	HP	kA	HP
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
produktovou	produktový	k2eAgFnSc4d1	produktová
řadu	řada	k1gFnSc4	řada
o	o	k7c4	o
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
výzkumy	výzkum	k1gInPc1	výzkum
a	a	k8xC	a
firmy	firma	k1gFnPc1	firma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HP	HP	kA	HP
také	také	k9	také
rostlo	růst	k5eAaImAgNnS	růst
díky	díky	k7c3	díky
akvizicím	akvizice	k1gFnPc3	akvizice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
koupilo	koupit	k5eAaPmAgNnS	koupit
Apollo	Apollo	k1gNnSc1	Apollo
Computer	computer	k1gInSc1	computer
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
Convex	Convex	k1gInSc1	Convex
Computer	computer	k1gInSc4	computer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
HP	HP	kA	HP
otevřel	otevřít	k5eAaPmAgInS	otevřít
eshop	eshop	k1gInSc1	eshop
hpshopping	hpshopping	k1gInSc1	hpshopping
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
jako	jako	k8xS	jako
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
prodávající	prodávající	k2eAgMnSc1d1	prodávající
on-line	onin	k1gInSc5	on-lin
přímo	přímo	k6eAd1	přímo
spotřebitelům	spotřebitel	k1gMnPc3	spotřebitel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
obchod	obchod	k1gInSc1	obchod
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
HP	HP	kA	HP
Home	Home	k1gNnPc2	Home
&	&	k?	&
Home	Hom	k1gFnSc2	Hom
Office	Office	kA	Office
Store	Stor	k1gInSc5	Stor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
všechny	všechen	k3xTgInPc4	všechen
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
počítači	počítač	k1gInPc7	počítač
<g/>
,	,	kIx,	,
ukládáním	ukládání	k1gNnSc7	ukládání
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
obrazu	obraz	k1gInSc2	obraz
byly	být	k5eAaImAgInP	být
převedeny	převést	k5eAaPmNgInP	převést
pod	pod	k7c4	pod
firmu	firma	k1gFnSc4	firma
Agilent	Agilent	k1gMnSc1	Agilent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
odnož	odnož	k1gFnSc1	odnož
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
8	[number]	k4	8
miliardovou	miliardový	k2eAgFnSc4d1	miliardová
USD	USD	kA	USD
společnost	společnost	k1gFnSc4	společnost
s	s	k7c7	s
asi	asi	k9	asi
30.000	[number]	k4	30.000
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
polovodičů	polovodič	k1gInPc2	polovodič
<g/>
,	,	kIx,	,
optických	optický	k2eAgFnPc2d1	optická
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
elektronických	elektronický	k2eAgNnPc2d1	elektronické
zkušebních	zkušební	k2eAgNnPc2d1	zkušební
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
telekomunikaci	telekomunikace	k1gFnSc4	telekomunikace
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
<g/>
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1999	[number]	k4	1999
HP	HP	kA	HP
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
výkonné	výkonný	k2eAgFnSc2d1	výkonná
ředitelky	ředitelka	k1gFnSc2	ředitelka
podnikatelku	podnikatelka	k1gFnSc4	podnikatelka
Carly	Carla	k1gFnSc2	Carla
Fiorinovou	Fiorinový	k2eAgFnSc4d1	Fiorinový
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
indexu	index	k1gInSc6	index
Dow	Dow	k1gMnSc1	Dow
Jones	Jones	k1gMnSc1	Jones
Industrial	Industrial	k1gMnSc1	Industrial
Average	Average	k1gFnSc1	Average
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fiorinová	Fiorinová	k1gFnSc1	Fiorinová
působila	působit	k5eAaImAgFnS	působit
jako	jako	k9	jako
generální	generální	k2eAgFnSc1d1	generální
ředitelka	ředitelka	k1gFnSc1	ředitelka
technického	technický	k2eAgNnSc2d1	technické
centra	centrum	k1gNnSc2	centrum
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
jejího	její	k3xOp3gNnSc2	její
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
HP	HP	kA	HP
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
ztratila	ztratit	k5eAaPmAgFnS	ztratit
polovinu	polovina	k1gFnSc4	polovina
své	svůj	k3xOyFgFnSc2	svůj
hodnoty	hodnota	k1gFnSc2	hodnota
oproti	oproti	k7c3	oproti
srovnatelným	srovnatelný	k2eAgFnPc3d1	srovnatelná
společnostem	společnost	k1gFnPc3	společnost
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
velké	velký	k2eAgFnPc1d1	velká
ztráty	ztráta	k1gFnPc1	ztráta
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Představenstvo	představenstvo	k1gNnSc1	představenstvo
HP	HP	kA	HP
Fiorinovou	Fiorinová	k1gFnSc4	Fiorinová
požádalo	požádat	k5eAaPmAgNnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rezignovala	rezignovat	k5eAaBmAgFnS	rezignovat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
společnost	společnost	k1gFnSc1	společnost
HP	HP	kA	HP
sfúzovala	sfúzovat	k5eAaBmAgFnS	sfúzovat
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Compaq	Compaq	kA	Compaq
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
skoupila	skoupit	k5eAaPmAgFnS	skoupit
společnost	společnost	k1gFnSc1	společnost
Tandem	tandem	k1gInSc1	tandem
Computers	Computers	k1gInSc1	Computers
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc4d1	založená
bývalými	bývalý	k2eAgMnPc7d1	bývalý
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
HP	HP	kA	HP
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
skoupila	skoupit	k5eAaImAgFnS	skoupit
firmu	firma	k1gFnSc4	firma
Digital	Digital	kA	Digital
Equipment	Equipment	k1gInSc1	Equipment
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
fúzi	fúze	k1gFnSc3	fúze
se	se	k3xPyFc4	se
značka	značka	k1gFnSc1	značka
HP	HP	kA	HP
(	(	kIx(	(
<g/>
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
sloučená	sloučený	k2eAgFnSc1d1	sloučená
společnost	společnost	k1gFnSc1	společnost
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
jedinou	jediný	k2eAgFnSc4d1	jediná
<g/>
)	)	kIx)	)
stala	stát	k5eAaPmAgFnS	stát
majoritním	majoritní	k2eAgMnSc7d1	majoritní
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
Notebooky	notebook	k1gInPc7	notebook
<g/>
,	,	kIx,	,
stolními	stolní	k2eAgInPc7d1	stolní
PC	PC	kA	PC
a	a	k8xC	a
servery	server	k1gInPc1	server
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgInPc2d1	různý
trhů	trh	k1gInPc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgInPc4d3	nejúspěšnější
notebooky	notebook	k1gInPc4	notebook
firmy	firma	k1gFnSc2	firma
HP	HP	kA	HP
patří	patřit	k5eAaImIp3nS	patřit
HP	HP	kA	HP
Omnibook	Omnibook	k1gInSc1	Omnibook
a	a	k8xC	a
HP	HP	kA	HP
Pavilion	Pavilion	k1gInSc1	Pavilion
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Compaq	Compaq	kA	Compaq
začala	začít	k5eAaPmAgFnS	začít
HP	HP	kA	HP
používat	používat	k5eAaImF	používat
nové	nový	k2eAgNnSc4d1	nové
značení	značení	k1gNnSc4	značení
výrobků	výrobek	k1gInPc2	výrobek
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
HPQ	HPQ	kA	HPQ
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
kombinací	kombinace	k1gFnSc7	kombinace
původních	původní	k2eAgFnPc2d1	původní
značek	značka	k1gFnPc2	značka
HWP	HWP	kA	HWP
a	a	k8xC	a
CPQ	CPQ	kA	CPQ
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
HP	HP	kA	HP
zřídila	zřídit	k5eAaPmAgFnS	zřídit
své	svůj	k3xOyFgInPc4	svůj
podpůrné	podpůrný	k2eAgInPc4d1	podpůrný
podniky	podnik	k1gInPc4	podnik
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
levnou	levný	k2eAgFnSc7d1	levná
pracovní	pracovní	k2eAgFnSc7d1	pracovní
silou	síla	k1gFnSc7	síla
<g/>
:	:	kIx,	:
španělský	španělský	k2eAgInSc1d1	španělský
podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
Německý	německý	k2eAgMnSc1d1	německý
do	do	k7c2	do
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2011	[number]	k4	2011
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
CEO	CEO	kA	CEO
HP	HP	kA	HP
Leo	Leo	k1gMnSc1	Leo
Apotheker	Apotheker	k1gMnSc1	Apotheker
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
oddělení	oddělení	k1gNnSc1	oddělení
či	či	k8xC	či
prodej	prodej	k1gInSc1	prodej
divize	divize	k1gFnSc2	divize
PSG	PSG	kA	PSG
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
PC	PC	kA	PC
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
ovšem	ovšem	k9	ovšem
sklidil	sklidit	k5eAaPmAgMnS	sklidit
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
kritiku	kritika	k1gFnSc4	kritika
od	od	k7c2	od
analytiků	analytik	k1gMnPc2	analytik
i	i	k9	i
od	od	k7c2	od
zákazníků	zákazník	k1gMnPc2	zákazník
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
post	post	k1gInSc4	post
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
CEO	CEO	kA	CEO
Meg	Meg	k1gFnSc1	Meg
Whitmanová	Whitmanová	k1gFnSc1	Whitmanová
pak	pak	k6eAd1	pak
na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
HP	HP	kA	HP
revidovala	revidovat	k5eAaImAgFnS	revidovat
původní	původní	k2eAgNnSc4d1	původní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
a	a	k8xC	a
divizi	divize	k1gFnSc4	divize
vyrábějící	vyrábějící	k2eAgFnSc2d1	vyrábějící
PC	PC	kA	PC
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
ponechá	ponechat	k5eAaPmIp3nS	ponechat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
produkty	produkt	k1gInPc4	produkt
==	==	k?	==
</s>
</p>
<p>
<s>
HP	HP	kA	HP
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
úspěšné	úspěšný	k2eAgFnPc4d1	úspěšná
řady	řada	k1gFnPc4	řada
tiskáren	tiskárna	k1gFnPc2	tiskárna
<g/>
,	,	kIx,	,
skenerů	skener	k1gInPc2	skener
<g/>
,	,	kIx,	,
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
,	,	kIx,	,
kalkulátorů	kalkulátor	k1gInPc2	kalkulátor
<g/>
,	,	kIx,	,
PDA	PDA	kA	PDA
<g/>
,	,	kIx,	,
serverů	server	k1gInPc2	server
<g/>
,	,	kIx,	,
pracovních	pracovní	k2eAgFnPc2d1	pracovní
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
a	a	k8xC	a
malých	malý	k2eAgMnPc2d1	malý
domácích	domácí	k2eAgMnPc2d1	domácí
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Compaq	Compaq	kA	Compaq
se	se	k3xPyFc4	se
ale	ale	k9	ale
tyto	tento	k3xDgFnPc1	tento
produktové	produktový	k2eAgFnPc1d1	produktová
řady	řada	k1gFnPc1	řada
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
HP	HP	kA	HP
také	také	k9	také
začala	začít	k5eAaPmAgFnS	začít
nabízet	nabízet	k5eAaImF	nabízet
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
služeb	služba	k1gFnPc2	služba
pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
<g/>
,	,	kIx,	,
realizaci	realizace	k1gFnSc4	realizace
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
IT	IT	kA	IT
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
IPG	IPG	kA	IPG
-	-	kIx~	-
Imaging	Imaging	k1gInSc1	Imaging
and	and	k?	and
Printing	Printing	k1gInSc1	Printing
Group	Group	k1gInSc1	Group
-	-	kIx~	-
Skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
tisk	tisk	k1gInSc4	tisk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
HP	HP	kA	HP
2005	[number]	k4	2005
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
<g/>
SEC	sec	kA	sec
10-K	[number]	k4	10-K
registrací	registrace	k1gFnSc7	registrace
je	být	k5eAaImIp3nS	být
IPG	IPG	kA	IPG
od	od	k7c2	od
HP	HP	kA	HP
vedoucím	vedoucí	k1gMnSc7	vedoucí
poskytovatelem	poskytovatel	k1gMnSc7	poskytovatel
zobrazovacích	zobrazovací	k2eAgInPc2d1	zobrazovací
a	a	k8xC	a
tiskařských	tiskařský	k2eAgInPc2d1	tiskařský
systémů	systém	k1gInPc2	systém
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
tiskárenskému	tiskárenský	k2eAgInSc3d1	tiskárenský
hardwaru	hardware	k1gInSc3	hardware
<g/>
,	,	kIx,	,
doplňkům	doplněk	k1gInPc3	doplněk
a	a	k8xC	a
skenovacím	skenovací	k2eAgNnPc3d1	skenovací
zařízením	zařízení	k1gNnPc3	zařízení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
jsou	být	k5eAaImIp3nP	být
nabízeny	nabízet	k5eAaImNgFnP	nabízet
všem	všecek	k3xTgFnPc3	všecek
segmentům	segment	k1gInPc3	segment
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
individuálním	individuální	k2eAgMnPc3d1	individuální
spotřebitelům	spotřebitel	k1gMnPc3	spotřebitel
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k8xC	i
malým	malý	k2eAgFnPc3d1	malá
až	až	k8xS	až
velkým	velký	k2eAgFnPc3d1	velká
obchodním	obchodní	k2eAgFnPc3d1	obchodní
společnostem	společnost	k1gFnPc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
divize	divize	k1gFnSc1	divize
je	být	k5eAaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
vedena	vést	k5eAaImNgFnS	vést
Vyomeshem	Vyomesh	k1gInSc7	Vyomesh
Joshim	Joshima	k1gFnPc2	Joshima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Produkty	produkt	k1gInPc4	produkt
a	a	k8xC	a
technologie	technologie	k1gFnPc4	technologie
spadající	spadající	k2eAgFnPc4d1	spadající
pod	pod	k7c7	pod
IPG	IPG	kA	IPG
====	====	k?	====
</s>
</p>
<p>
<s>
inkoustové	inkoustový	k2eAgFnPc1d1	inkoustová
a	a	k8xC	a
laserové	laserový	k2eAgFnPc1d1	laserová
tiskárny	tiskárna	k1gFnPc1	tiskárna
<g/>
,	,	kIx,	,
spotřební	spotřební	k2eAgInPc1d1	spotřební
materiály	materiál	k1gInPc1	materiál
a	a	k8xC	a
příbuzné	příbuzný	k2eAgNnSc4d1	příbuzné
zboží	zboží	k1gNnSc4	zboží
</s>
</p>
<p>
<s>
multifunkční	multifunkční	k2eAgNnSc1d1	multifunkční
zařízení	zařízení	k1gNnSc1	zařízení
OfficeJet	OfficeJet	k1gInSc1	OfficeJet
(	(	kIx(	(
<g/>
tisk	tisk	k1gInSc1	tisk
<g/>
/	/	kIx~	/
<g/>
scan	scan	k1gInSc1	scan
<g/>
/	/	kIx~	/
<g/>
fax	fax	k1gInSc1	fax
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
velkoformátové	velkoformátový	k2eAgFnPc1d1	velkoformátová
tiskárny	tiskárna	k1gFnPc1	tiskárna
</s>
</p>
<p>
<s>
Indigo	indigo	k1gNnSc1	indigo
digitální	digitální	k2eAgFnSc2d1	digitální
tisk	tisk	k1gInSc1	tisk
</s>
</p>
<p>
<s>
HP	HP	kA	HP
Web	web	k1gInSc1	web
Jetadmin	Jetadmin	k1gInSc1	Jetadmin
</s>
</p>
<p>
<s>
HP	HP	kA	HP
Output	Output	k2eAgInSc1d1	Output
Management	management	k1gInSc1	management
sada	sada	k1gFnSc1	sada
softwaru	software	k1gInSc2	software
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
HP	HP	kA	HP
Output	Output	k2eAgInSc1d1	Output
Server	server	k1gInSc1	server
</s>
</p>
<p>
<s>
LightScribe	LightScribat	k5eAaPmIp3nS	LightScribat
optická	optický	k2eAgFnSc1d1	optická
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
technologie	technologie	k1gFnSc1	technologie
umožňující	umožňující	k2eAgFnSc2d1	umožňující
laserový	laserový	k2eAgInSc4d1	laserový
popis	popis	k1gInSc4	popis
disků	disk	k1gInPc2	disk
</s>
</p>
<p>
<s>
HP	HP	kA	HP
Photosmart	Photosmart	k1gInSc1	Photosmart
digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
a	a	k8xC	a
foto	foto	k1gNnSc1	foto
tiskárny	tiskárna	k1gFnSc2	tiskárna
</s>
</p>
<p>
<s>
===	===	k?	===
PSG	PSG	kA	PSG
-	-	kIx~	-
Personal	Personal	k1gMnSc1	Personal
Systems	Systemsa	k1gFnPc2	Systemsa
Group	Group	k1gMnSc1	Group
-	-	kIx~	-
Skupina	skupina	k1gFnSc1	skupina
osobních	osobní	k2eAgInPc2d1	osobní
systémů	systém	k1gInPc2	systém
===	===	k?	===
</s>
</p>
<p>
<s>
PSG	PSG	kA	PSG
od	od	k7c2	od
HP	HP	kA	HP
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vůdčích	vůdčí	k2eAgMnPc2d1	vůdčí
prodejců	prodejce	k1gMnPc2	prodejce
osobních	osobní	k2eAgMnPc2d1	osobní
počítačů	počítač	k1gMnPc2	počítač
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pod	pod	k7c4	pod
skupinu	skupina	k1gFnSc4	skupina
PSG	PSG	kA	PSG
můžeme	moct	k5eAaImIp1nP	moct
zařadit	zařadit	k5eAaPmF	zařadit
====	====	k?	====
</s>
</p>
<p>
<s>
osobní	osobní	k2eAgInPc4d1	osobní
počítače	počítač	k1gInPc4	počítač
včetně	včetně	k7c2	včetně
HP	HP	kA	HP
Pavilion	Pavilion	k1gInSc1	Pavilion
<g/>
,	,	kIx,	,
Compaq	Compaq	kA	Compaq
Presario	Presario	k1gNnSc1	Presario
a	a	k8xC	a
VoodooPC	VoodooPC	k1gFnSc1	VoodooPC
série	série	k1gFnSc2	série
</s>
</p>
<p>
<s>
pracovní	pracovní	k2eAgFnSc1d1	pracovní
stanice	stanice	k1gFnSc1	stanice
pro	pro	k7c4	pro
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
Unix	Unix	k1gInSc1	Unix
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
a	a	k8xC	a
Linux	Linux	kA	Linux
</s>
</p>
<p>
<s>
kapesní	kapesní	k2eAgInPc1d1	kapesní
počítače	počítač	k1gInPc1	počítač
jako	jako	k8xS	jako
PocketPC	PocketPC	k1gFnPc1	PocketPC
(	(	kIx(	(
<g/>
iPAQ	iPAQ	k?	iPAQ
<g/>
)	)	kIx)	)
–	–	k?	–
kapesní	kapesní	k2eAgNnSc1d1	kapesní
počítačové	počítačový	k2eAgNnSc1d1	počítačové
zařízení	zařízení	k1gNnSc1	zařízení
(	(	kIx(	(
<g/>
od	od	k7c2	od
Compaqu	Compaqus	k1gInSc2	Compaqus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
digitální	digitální	k2eAgFnSc1d1	digitální
zábava	zábava	k1gFnSc1	zábava
jako	jako	k8xS	jako
např.	např.	kA	např.
DVD	DVD	kA	DVD
<g/>
+	+	kIx~	+
<g/>
RW	RW	kA	RW
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
HP	HP	kA	HP
Movie	Movie	k1gFnSc1	Movie
Writer	Writer	k1gMnSc1	Writer
a	a	k8xC	a
HP	HP	kA	HP
Digital	Digital	kA	Digital
Entertainment	Entertainment	k1gMnSc1	Entertainment
Center	centrum	k1gNnPc2	centrum
</s>
</p>
<p>
<s>
===	===	k?	===
HP	HP	kA	HP
Laboratoře	laboratoř	k1gFnSc2	laboratoř
===	===	k?	===
</s>
</p>
<p>
<s>
HP	HP	kA	HP
Laboratoře	laboratoř	k1gFnPc1	laboratoř
jsou	být	k5eAaImIp3nP	být
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
HP	HP	kA	HP
<g/>
.	.	kIx.	.
</s>
<s>
Založeny	založen	k2eAgFnPc1d1	založena
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
a	a	k8xC	a
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
dodavatel	dodavatel	k1gMnSc1	dodavatel
průlomových	průlomový	k2eAgFnPc2d1	průlomová
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
obchodní	obchodní	k2eAgFnPc4d1	obchodní
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jdou	jít	k5eAaImIp3nP	jít
mimo	mimo	k7c4	mimo
hlavní	hlavní	k2eAgFnSc4d1	hlavní
strategii	strategie	k1gFnSc4	strategie
HP	HP	kA	HP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spolupráce	spolupráce	k1gFnSc1	spolupráce
===	===	k?	===
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
HP	HP	kA	HP
plně	plně	k6eAd1	plně
podporuje	podporovat	k5eAaImIp3nS	podporovat
FOSS	FOSS	kA	FOSS
a	a	k8xC	a
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
<s>
Hewlett-Packard	Hewlett-Packard	k1gMnSc1	Hewlett-Packard
také	také	k9	také
navázal	navázat	k5eAaPmAgMnS	navázat
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
Compaqu	Compaqus	k1gInSc2	Compaqus
s	s	k7c7	s
Microsoftem	Microsoft	k1gInSc7	Microsoft
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
technologie	technologie	k1gFnSc1	technologie
od	od	k7c2	od
největšího	veliký	k2eAgMnSc2d3	veliký
prodejce	prodejce	k1gMnSc2	prodejce
HW	HW	kA	HW
a	a	k8xC	a
SW	SW	kA	SW
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
společnost	společnost	k1gFnSc1	společnost
HP	HP	kA	HP
nabízela	nabízet	k5eAaImAgFnS	nabízet
i	i	k8xC	i
přeoznačený	přeoznačený	k2eAgInSc1d1	přeoznačený
Apple	Apple	kA	Apple
iPod	iPod	k1gInSc1	iPod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
požadoval	požadovat	k5eAaImAgInS	požadovat
úřad	úřad	k1gInSc1	úřad
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
USA	USA	kA	USA
po	po	k7c6	po
HP	HP	kA	HP
sankci	sankce	k1gFnSc6	sankce
2,5	[number]	k4	2,5
<g/>
milionu	milion	k4xCgInSc2	milion
USD	USD	kA	USD
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
nepodala	podat	k5eNaPmAgFnS	podat
před	před	k7c4	před
-	-	kIx~	-
výrobní	výrobní	k2eAgNnSc4d1	výrobní
oznámení	oznámení	k1gNnSc4	oznámení
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
a	a	k8xC	a
vyvážet	vyvážet	k5eAaImF	vyvážet
chemické	chemický	k2eAgFnPc4d1	chemická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
tohoto	tento	k3xDgNnSc2	tento
oznámení	oznámení	k1gNnSc2	oznámení
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
úřad	úřad	k1gInSc1	úřad
provést	provést	k5eAaPmF	provést
analýzu	analýza	k1gFnSc4	analýza
rizika	riziko	k1gNnSc2	riziko
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HP	HP	kA	HP
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
na	na	k7c4	na
Scorecard	Scorecard	k1gMnSc1	Scorecard
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
v	v	k7c6	v
top	topit	k5eAaImRp2nS	topit
10-20	[number]	k4	10-20
pro	pro	k7c4	pro
celkové	celkový	k2eAgInPc4d1	celkový
úniky	únik	k1gInPc4	únik
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
v	v	k7c6	v
top	topit	k5eAaImRp2nS	topit
30-40	[number]	k4	30-40
pro	pro	k7c4	pro
úniky	únik	k1gInPc4	únik
toxických	toxický	k2eAgFnPc2d1	toxická
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
společnost	společnost	k1gFnSc1	společnost
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
splnila	splnit	k5eAaPmAgFnS	splnit
svůj	svůj	k3xOyFgInSc4	svůj
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
si	se	k3xPyFc3	se
stanovila	stanovit	k5eAaPmAgFnS	stanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
recyklovat	recyklovat	k5eAaBmF	recyklovat
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
lb	lb	k?	lb
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
,	,	kIx,	,
tonerů	toner	k1gInPc2	toner
a	a	k8xC	a
inkoustových	inkoustový	k2eAgFnPc2d1	inkoustová
kazet	kazeta	k1gFnPc2	kazeta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
má	mít	k5eAaImIp3nS	mít
dle	dle	k7c2	dle
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
recyklovat	recyklovat	k5eAaBmF	recyklovat
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
lb	lb	k?	lb
hardwaru	hardware	k1gInSc2	hardware
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sponzorství	sponzorství	k1gNnSc2	sponzorství
===	===	k?	===
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
HP	HP	kA	HP
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
světovým	světový	k2eAgMnSc7d1	světový
sponzorem	sponzor	k1gMnSc7	sponzor
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
sponzorských	sponzorský	k2eAgFnPc2d1	sponzorská
akcí	akce	k1gFnPc2	akce
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gMnSc2	Disnea
World	Worlda	k1gFnPc2	Worlda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
EPCOT	EPCOT	kA	EPCOT
Park	park	k1gInSc1	park
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
<g/>
Space	Space	k1gFnSc1	Space
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
další	další	k2eAgInPc1d1	další
můžete	moct	k5eAaImIp2nP	moct
najít	najít	k5eAaPmF	najít
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
HP	HP	kA	HP
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
HP	HP	kA	HP
též	též	k9	též
sponzorem	sponzor	k1gMnSc7	sponzor
klubu	klub	k1gInSc2	klub
Tottenham	Tottenham	k1gInSc1	Tottenham
Hotspur	Hotspur	k1gMnSc1	Hotspur
anglické	anglický	k2eAgInPc1d1	anglický
Premier	Premier	k1gInSc4	Premier
League	Leagu	k1gInSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
HP	HP	kA	HP
sponzoruje	sponzorovat	k5eAaImIp3nS	sponzorovat
také	také	k9	také
tým	tým	k1gInSc1	tým
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
-	-	kIx~	-
BMW	BMW	kA	BMW
a	a	k8xC	a
hokejový	hokejový	k2eAgInSc1d1	hokejový
tým	tým	k1gInSc1	tým
San	San	k1gFnSc2	San
Jose	Jos	k1gFnSc2	Jos
Sharks	Sharksa	k1gFnPc2	Sharksa
z	z	k7c2	z
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
arénu	aréna	k1gFnSc4	aréna
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hewlett-Packard	Hewlett-Packard	k1gMnSc1	Hewlett-Packard
koupil	koupit	k5eAaPmAgMnS	koupit
Compaq	Compaq	kA	Compaq
<g/>
,	,	kIx,	,
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
z	z	k7c2	z
Compaq	Compaq	kA	Compaq
Center	centrum	k1gNnPc2	centrum
na	na	k7c4	na
HP	HP	kA	HP
Pavilion	Pavilion	k1gInSc4	Pavilion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hewlett-Packard	Hewlett-Packarda	k1gFnPc2	Hewlett-Packarda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hewlett-Packard	Hewlett-Packarda	k1gFnPc2	Hewlett-Packarda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Hewlett-Packard	Hewlett-Packard	k1gInSc1	Hewlett-Packard
home	homat	k5eAaPmIp3nS	homat
</s>
</p>
<p>
<s>
HP	HP	kA	HP
Printing	Printing	k1gInSc1	Printing
and	and	k?	and
The	The	k1gFnSc1	The
Science	Science	k1gFnSc2	Science
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Minnesota	Minnesota	k1gFnSc1	Minnesota
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
The	The	k?	The
Museum	museum	k1gNnSc1	museum
of	of	k?	of
HP	HP	kA	HP
Calculators	Calculators	k1gInSc1	Calculators
</s>
</p>
<p>
<s>
HP	HP	kA	HP
History	Histor	k1gInPc4	Histor
Links	Links	k1gInSc4	Links
</s>
</p>
<p>
<s>
Notebooky	notebook	k1gInPc1	notebook
firmy	firma	k1gFnSc2	firma
HP	HP	kA	HP
</s>
</p>
