<s>
Venuše	Venuše	k1gFnSc1	Venuše
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Země	zem	k1gFnSc2	zem
pevný	pevný	k2eAgInSc4d1	pevný
kamenitý	kamenitý	k2eAgInSc4d1	kamenitý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
