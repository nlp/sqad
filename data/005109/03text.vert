<s>
NTFS	NTFS	kA	NTFS
(	(	kIx(	(
<g/>
New	New	k1gMnSc1	New
Technology	technolog	k1gMnPc7	technolog
File	File	k1gFnSc4	File
System	Syst	k1gInSc7	Syst
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
souborový	souborový	k2eAgInSc4d1	souborový
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
firma	firma	k1gFnSc1	firma
Microsoft	Microsoft	kA	Microsoft
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgInPc4	svůj
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
řady	řada	k1gFnSc2	řada
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
.	.	kIx.	.
</s>
<s>
Souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
NTFS	NTFS	kA	NTFS
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xS	jako
rozšiřitelný	rozšiřitelný	k2eAgInSc1d1	rozšiřitelný
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
novým	nový	k2eAgInPc3d1	nový
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Microsoft	Microsoft	kA	Microsoft
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
NTFS	NTFS	kA	NTFS
využil	využít	k5eAaPmAgInS	využít
poznatky	poznatek	k1gInPc7	poznatek
z	z	k7c2	z
vývoje	vývoj	k1gInSc2	vývoj
HPFS	HPFS	kA	HPFS
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
IBM	IBM	kA	IBM
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
firmy	firma	k1gFnPc1	firma
Microsoft	Microsoft	kA	Microsoft
a	a	k8xC	a
IBM	IBM	kA	IBM
společný	společný	k2eAgInSc4d1	společný
projekt	projekt	k1gInSc4	projekt
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
grafického	grafický	k2eAgInSc2d1	grafický
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
projektu	projekt	k1gInSc2	projekt
bylo	být	k5eAaImAgNnS	být
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Firmy	firma	k1gFnPc1	firma
Microsoft	Microsoft	kA	Microsoft
a	a	k8xC	a
IBM	IBM	kA	IBM
ale	ale	k8xC	ale
měly	mít	k5eAaImAgFnP	mít
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
důležitých	důležitý	k2eAgFnPc2d1	důležitá
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozešly	rozejít	k5eAaPmAgFnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
zůstal	zůstat	k5eAaPmAgInS	zůstat
projektem	projekt	k1gInSc7	projekt
firmy	firma	k1gFnSc2	firma
IBM	IBM	kA	IBM
a	a	k8xC	a
Microsoft	Microsoft	kA	Microsoft
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
.	.	kIx.	.
</s>
<s>
Souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
HPFS	HPFS	kA	HPFS
ze	z	k7c2	z
systému	systém	k1gInSc2	systém
OS	OS	kA	OS
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
několik	několik	k4yIc4	několik
důležitých	důležitý	k2eAgInPc2d1	důležitý
nových	nový	k2eAgInPc2d1	nový
konceptů	koncept	k1gInPc2	koncept
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Microsoft	Microsoft	kA	Microsoft
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
konceptů	koncept	k1gInPc2	koncept
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
souborový	souborový	k2eAgInSc4d1	souborový
systém	systém	k1gInSc4	systém
NTFS	NTFS	kA	NTFS
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tohoto	tento	k3xDgInSc2	tento
společného	společný	k2eAgInSc2d1	společný
původu	původ	k1gInSc2	původ
používá	používat	k5eAaImIp3nS	používat
HPFS	HPFS	kA	HPFS
a	a	k8xC	a
NTFS	NTFS	kA	NTFS
stejné	stejné	k1gNnSc1	stejné
identifikačních	identifikační	k2eAgInPc2d1	identifikační
kódy	kód	k1gInPc7	kód
diskových	diskový	k2eAgInPc2d1	diskový
oddílů	oddíl	k1gInPc2	oddíl
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
stejného	stejné	k1gNnSc2	stejné
ID	Ida	k1gFnPc2	Ida
čísla	číslo	k1gNnSc2	číslo
oddílu	oddíl	k1gInSc2	oddíl
bylo	být	k5eAaImAgNnS	být
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
desítky	desítka	k1gFnSc2	desítka
volných	volný	k2eAgInPc2d1	volný
identifikačních	identifikační	k2eAgInPc2d1	identifikační
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
jinak	jinak	k6eAd1	jinak
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
významné	významný	k2eAgInPc4d1	významný
souborové	souborový	k2eAgInPc4d1	souborový
systémy	systém	k1gInPc4	systém
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
samotný	samotný	k2eAgInSc1d1	samotný
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
FAT	fatum	k1gNnPc2	fatum
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
devět	devět	k4xCc4	devět
různých	různý	k2eAgInPc2d1	různý
kódů	kód	k1gInPc2	kód
(	(	kIx(	(
<g/>
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
pro	pro	k7c4	pro
varianty	varianta	k1gFnPc4	varianta
FAT	fatum	k1gNnPc2	fatum
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
FAT	fatum	k1gNnPc2	fatum
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
FAT32	FAT32	k1gFnSc1	FAT32
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
,	,	kIx,	,
identifikující	identifikující	k2eAgInSc1d1	identifikující
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
typu	typ	k1gInSc2	typ
diskového	diskový	k2eAgInSc2d1	diskový
oddílu	oddíl	k1gInSc2	oddíl
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
provést	provést	k5eAaPmF	provést
další	další	k2eAgInPc4d1	další
testy	test	k1gInPc4	test
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
diskovém	diskový	k2eAgInSc6d1	diskový
oddíly	oddíl	k1gInPc7	oddíl
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Vývojáři	vývojář	k1gMnPc1	vývojář
NTFS	NTFS	kA	NTFS
<g/>
:	:	kIx,	:
Tom	Tom	k1gMnSc1	Tom
Miller	Miller	k1gMnSc1	Miller
Gary	Gara	k1gMnSc2	Gara
Kimura	Kimur	k1gMnSc2	Kimur
Brian	Brian	k1gMnSc1	Brian
Andrew	Andrew	k1gMnSc1	Andrew
David	David	k1gMnSc1	David
Goebel	Goebel	k1gMnSc1	Goebel
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
pět	pět	k4xCc1	pět
verzí	verze	k1gFnPc2	verze
NTFS	NTFS	kA	NTFS
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každá	každý	k3xTgFnSc1	každý
verze	verze	k1gFnSc1	verze
přinesla	přinést	k5eAaPmAgFnS	přinést
nějaké	nějaký	k3yIgFnPc4	nějaký
novinky	novinka	k1gFnPc4	novinka
<g/>
:	:	kIx,	:
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
chcete	chtít	k5eAaImIp2nP	chtít
používat	používat	k5eAaImF	používat
NTFS	NTFS	kA	NTFS
v	v	k7c6	v
<g/>
3.0	[number]	k4	3.0
ve	v	k7c6	v
starším	starý	k2eAgInSc6d2	starší
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
4.0	[number]	k4	4.0
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
být	být	k5eAaImF	být
nainstalován	nainstalován	k2eAgInSc1d1	nainstalován
Service	Service	k1gFnSc1	Service
Pack	Packo	k1gNnPc2	Packo
4	[number]	k4	4
(	(	kIx(	(
<g/>
servisní	servisní	k2eAgInSc1d1	servisní
balíček	balíček	k1gInSc1	balíček
číslo	číslo	k1gNnSc1	číslo
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
aktualizovanou	aktualizovaný	k2eAgFnSc4d1	aktualizovaná
verzi	verze	k1gFnSc4	verze
ovladače	ovladač	k1gInSc2	ovladač
NTFS	NTFS	kA	NTFS
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
Vista	vista	k2eAgFnSc2d1	vista
přineslo	přinést	k5eAaPmAgNnS	přinést
spíše	spíše	k9	spíše
vylepšení	vylepšení	k1gNnSc4	vylepšení
jádra	jádro	k1gNnSc2	jádro
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
než	než	k8xS	než
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
NTFS	NTFS	kA	NTFS
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
verze	verze	k1gFnSc1	verze
NTFS	NTFS	kA	NTFS
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
NTFS	NTFS	kA	NTFS
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k8xS	jako
nativní	nativní	k2eAgInSc1d1	nativní
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
NT	NT	kA	NT
a	a	k8xC	a
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
oproti	oproti	k7c3	oproti
zastaralému	zastaralý	k2eAgNnSc3d1	zastaralé
filesystému	filesystý	k2eAgNnSc3d1	filesystý
FAT	fatum	k1gNnPc2	fatum
<g/>
)	)	kIx)	)
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
spoustu	spousta	k1gFnSc4	spousta
novinek	novinka	k1gFnPc2	novinka
<g/>
:	:	kIx,	:
žurnálování	žurnálování	k1gNnSc1	žurnálování
–	–	k?	–
všechny	všechen	k3xTgInPc4	všechen
zápisy	zápis	k1gInPc4	zápis
na	na	k7c4	na
disk	disk	k1gInSc4	disk
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
do	do	k7c2	do
speciálního	speciální	k2eAgInSc2d1	speciální
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
žurnálu	žurnál	k1gInSc2	žurnál
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
uprostřed	uprostřed	k7c2	uprostřed
zápisu	zápis	k1gInSc2	zápis
systém	systém	k1gInSc1	systém
havaruje	havarovat	k5eAaPmIp3nS	havarovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
možné	možný	k2eAgNnSc1d1	možné
podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
všechny	všechen	k3xTgFnPc4	všechen
rozpracované	rozpracovaný	k2eAgFnPc4d1	rozpracovaná
operace	operace	k1gFnPc4	operace
dokončit	dokončit	k5eAaPmF	dokončit
nebo	nebo	k8xC	nebo
anulovat	anulovat	k5eAaBmF	anulovat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
systém	systém	k1gInSc1	systém
souborů	soubor	k1gInPc2	soubor
opět	opět	k6eAd1	opět
uvést	uvést	k5eAaPmF	uvést
do	do	k7c2	do
konzistentního	konzistentní	k2eAgInSc2d1	konzistentní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
access	access	k1gInSc1	access
control	control	k1gInSc1	control
list	list	k1gInSc1	list
–	–	k?	–
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
přidělování	přidělování	k1gNnSc4	přidělování
práv	právo	k1gNnPc2	právo
k	k	k7c3	k
souborům	soubor	k1gInPc3	soubor
komprese	komprese	k1gFnSc2	komprese
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
šifrování	šifrování	k1gNnSc2	šifrování
(	(	kIx(	(
<g/>
EFS	EFS	kA	EFS
-	-	kIx~	-
Encrypting	Encrypting	k1gInSc1	Encrypting
File	Fil	k1gInSc2	Fil
System	Systo	k1gNnSc7	Systo
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
chránit	chránit	k5eAaImF	chránit
data	datum	k1gNnSc2	datum
uživatele	uživatel	k1gMnSc2	uživatel
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
transparentní	transparentní	k2eAgMnSc1d1	transparentní
<g/>
.	.	kIx.	.
diskové	diskový	k2eAgFnPc1d1	disková
kvóty	kvóta	k1gFnPc1	kvóta
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
nastavit	nastavit	k5eAaPmF	nastavit
maximálně	maximálně	k6eAd1	maximálně
využitelné	využitelný	k2eAgNnSc4d1	využitelné
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
diskovém	diskový	k2eAgInSc6d1	diskový
oddíle	oddíl	k1gInSc6	oddíl
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
uživatele	uživatel	k1gMnSc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
diskové	diskový	k2eAgFnSc2d1	disková
kvóty	kvóta	k1gFnSc2	kvóta
se	se	k3xPyFc4	se
nezapočítávají	započítávat	k5eNaImIp3nP	započítávat
komprimované	komprimovaný	k2eAgInPc1d1	komprimovaný
soubory	soubor	k1gInPc1	soubor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
reálná	reálný	k2eAgFnSc1d1	reálná
velikost	velikost	k1gFnSc1	velikost
<g/>
.	.	kIx.	.
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
jména	jméno	k1gNnPc4	jméno
souborů	soubor	k1gInPc2	soubor
(	(	kIx(	(
<g/>
ve	v	k7c6	v
FAT	fatum	k1gNnPc2	fatum
původně	původně	k6eAd1	původně
nebyla	být	k5eNaImAgFnS	být
a	a	k8xC	a
ve	v	k7c6	v
Windows	Windows	kA	Windows
95	[number]	k4	95
je	on	k3xPp3gInPc4	on
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
doplňovat	doplňovat	k5eAaImF	doplňovat
značně	značně	k6eAd1	značně
komplikovaným	komplikovaný	k2eAgInSc7d1	komplikovaný
způsobem	způsob	k1gInSc7	způsob
<g/>
)	)	kIx)	)
pevné	pevný	k2eAgFnPc4d1	pevná
a	a	k8xC	a
symbolické	symbolický	k2eAgFnPc4d1	symbolická
linky	linka	k1gFnPc4	linka
–	–	k?	–
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
soubory	soubor	k1gInPc4	soubor
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
filesystémů	filesystém	k1gInPc2	filesystém
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
z	z	k7c2	z
operačních	operační	k2eAgInPc2d1	operační
systému	systém	k1gInSc2	systém
UNIX	UNIX	kA	UNIX
<g/>
.	.	kIx.	.
</s>
<s>
Windows	Windows	kA	Windows
pro	pro	k7c4	pro
editaci	editace	k1gFnSc4	editace
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
odkazů	odkaz	k1gInPc2	odkaz
nemají	mít	k5eNaImIp3nP	mít
standardní	standardní	k2eAgNnSc4d1	standardní
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umí	umět	k5eAaImIp3nS	umět
je	on	k3xPp3gInPc4	on
interpretovat	interpretovat	k5eAaBmF	interpretovat
a	a	k8xC	a
také	také	k9	také
je	on	k3xPp3gInPc4	on
používají	používat	k5eAaImIp3nP	používat
(	(	kIx(	(
<g/>
Distribuovaný	distribuovaný	k2eAgInSc4d1	distribuovaný
systém	systém	k1gInSc4	systém
souborů	soubor	k1gInPc2	soubor
na	na	k7c6	na
Windows	Windows	kA	Windows
server	server	k1gInSc4	server
2003	[number]	k4	2003
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NTFS	NTFS	kA	NTFS
používá	používat	k5eAaImIp3nS	používat
64	[number]	k4	64
<g/>
bitové	bitový	k2eAgFnSc2d1	bitová
adresy	adresa	k1gFnSc2	adresa
clusterů	cluster	k1gInPc2	cluster
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
diskový	diskový	k2eAgInSc1d1	diskový
oddíl	oddíl	k1gInSc1	oddíl
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
FAT	fatum	k1gNnPc2	fatum
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
verzi	verze	k1gFnSc6	verze
používala	používat	k5eAaImAgFnS	používat
efektivně	efektivně	k6eAd1	efektivně
28	[number]	k4	28
<g/>
bitové	bitový	k2eAgInPc1d1	bitový
adresování	adresování	k1gNnPc2	adresování
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
konkrétně	konkrétně	k6eAd1	konkrétně
až	až	k9	až
16	[number]	k4	16
EB	EB	kA	EB
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
17	[number]	k4	17
×	×	k?	×
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
6	[number]	k4	6
TB	TB	kA	TB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
řešen	řešit	k5eAaImNgInS	řešit
jako	jako	k8xS	jako
obří	obří	k2eAgFnSc1d1	obří
databáze	databáze	k1gFnSc1	databáze
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
jeden	jeden	k4xCgInSc1	jeden
záznam	záznam	k1gInSc1	záznam
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
souboru	soubor	k1gInSc3	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
tvoří	tvořit	k5eAaImIp3nS	tvořit
11	[number]	k4	11
systémových	systémový	k2eAgInPc2d1	systémový
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
metadat	metadat	k5eAaImF	metadat
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
naformátování	naformátování	k1gNnSc6	naformátování
svazku	svazek	k1gInSc2	svazek
<g/>
.	.	kIx.	.
$	$	kIx~	$
<g/>
Logfile	Logfila	k1gFnSc3	Logfila
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
žurnálování	žurnálování	k1gNnSc4	žurnálování
<g/>
;	;	kIx,	;
$	$	kIx~	$
<g/>
MFT	MFT	kA	MFT
(	(	kIx(	(
<g/>
Master	master	k1gMnSc1	master
File	File	k1gNnSc6	File
Table	tablo	k1gNnSc6	tablo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tabulka	tabulka	k1gFnSc1	tabulka
obsahující	obsahující	k2eAgInPc1d1	obsahující
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
všech	všecek	k3xTgInPc6	všecek
souborech	soubor	k1gInPc6	soubor
<g/>
,	,	kIx,	,
adresářích	adresář	k1gInPc6	adresář
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
metadatech	metadat	k1gMnPc6	metadat
(	(	kIx(	(
<g/>
jelikož	jelikož	k8xS	jelikož
$	$	kIx~	$
<g/>
MFT	MFT	kA	MFT
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
i	i	k9	i
informace	informace	k1gFnSc1	informace
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
za	za	k7c4	za
boot	boot	k1gInSc4	boot
sektorem	sektor	k1gInSc7	sektor
<g/>
;	;	kIx,	;
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
teoreticky	teoreticky	k6eAd1	teoreticky
fragmentovat	fragmentovat	k5eAaPmF	fragmentovat
(	(	kIx(	(
<g/>
prakticky	prakticky	k6eAd1	prakticky
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
zamezeno	zamezit	k5eAaPmNgNnS	zamezit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
předešlo	předejít	k5eAaPmAgNnS	předejít
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
udržuje	udržovat	k5eAaImIp3nS	udržovat
zónu	zóna	k1gFnSc4	zóna
volného	volný	k2eAgNnSc2d1	volné
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Uložení	uložení	k1gNnSc1	uložení
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
záznamech	záznam	k1gInPc6	záznam
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
MFT	MFT	kA	MFT
mohla	moct	k5eAaImAgFnS	moct
růst	růst	k1gInSc4	růst
nebo	nebo	k8xC	nebo
zmenšovat	zmenšovat	k5eAaImF	zmenšovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
NTFS	NTFS	kA	NTFS
vnitřně	vnitřně	k6eAd1	vnitřně
určuje	určovat	k5eAaImIp3nS	určovat
soubory	soubor	k1gInPc1	soubor
a	a	k8xC	a
adresáře	adresář	k1gInPc1	adresář
podle	podle	k7c2	podle
pozice	pozice	k1gFnSc2	pozice
jejich	jejich	k3xOp3gInPc2	jejich
záznamů	záznam	k1gInPc2	záznam
v	v	k7c6	v
MFT	MFT	kA	MFT
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
označují	označovat	k5eAaImIp3nP	označovat
začátek	začátek	k1gInSc4	začátek
jejich	jejich	k3xOp3gInSc4	jejich
metadat	metadat	k5eAaPmF	metadat
<g/>
.	.	kIx.	.
</s>
<s>
Soubory	soubor	k1gInPc1	soubor
metadat	metadat	k5eAaImF	metadat
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
2	[number]	k4	2
mají	mít	k5eAaImIp3nP	mít
určené	určený	k2eAgInPc1d1	určený
první	první	k4xOgInPc1	první
záznamy	záznam	k1gInPc1	záznam
v	v	k7c6	v
MFT	MFT	kA	MFT
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
záznamu	záznam	k1gInSc2	záznam
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
1	[number]	k4	1
kB	kb	kA	kb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
větší	veliký	k2eAgFnPc4d2	veliký
<g/>
.	.	kIx.	.
$	$	kIx~	$
<g/>
MFTMirr	MFTMirr	k1gInSc1	MFTMirr
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
dat	datum	k1gNnPc2	datum
<g/>
;	;	kIx,	;
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
uprostřed	uprostřed	k7c2	uprostřed
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvních	první	k4xOgInPc2	první
16	[number]	k4	16
záznamů	záznam	k1gInPc2	záznam
$	$	kIx~	$
<g/>
MFT	MFT	kA	MFT
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
$	$	kIx~	$
<g/>
MFT	MFT	kA	MFT
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
poškozená	poškozený	k2eAgFnSc1d1	poškozená
<g/>
,	,	kIx,	,
použije	použít	k5eAaPmIp3nS	použít
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
kopie	kopie	k1gFnSc1	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Bootovací	Bootovací	k2eAgInSc1d1	Bootovací
záznam	záznam	k1gInSc1	záznam
NTFS	NTFS	kA	NTFS
disku	disk	k1gInSc2	disk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pozici	pozice	k1gFnSc4	pozice
MFT	MFT	kA	MFT
i	i	k9	i
její	její	k3xOp3gFnSc1	její
kopie	kopie	k1gFnSc1	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
přístupu	přístup	k1gInSc2	přístup
do	do	k7c2	do
MFT	MFT	kA	MFT
hraje	hrát	k5eAaImIp3nS	hrát
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
roli	role	k1gFnSc4	role
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
výkonu	výkon	k1gInSc6	výkon
NTFS	NTFS	kA	NTFS
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
NTFS	NTFS	kA	NTFS
snaží	snažit	k5eAaImIp3nP	snažit
tento	tento	k3xDgInSc4	tento
přístup	přístup	k1gInSc4	přístup
maximálně	maximálně	k6eAd1	maximálně
zrychlit	zrychlit	k5eAaPmF	zrychlit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
MFT	MFT	kA	MFT
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
růst	růst	k1gInSc4	růst
a	a	k8xC	a
zmenšovat	zmenšovat	k5eAaImF	zmenšovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
fragmentovaný	fragmentovaný	k2eAgMnSc1d1	fragmentovaný
(	(	kIx(	(
<g/>
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
do	do	k7c2	do
více	hodně	k6eAd2	hodně
částí	část	k1gFnPc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
fragmentace	fragmentace	k1gFnSc1	fragmentace
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
NTFS	NTFS	kA	NTFS
nemůže	moct	k5eNaImIp3nS	moct
přidělit	přidělit	k5eAaPmF	přidělit
souvislý	souvislý	k2eAgInSc4d1	souvislý
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
MFT	MFT	kA	MFT
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
velikost	velikost	k1gFnSc1	velikost
MFT	MFT	kA	MFT
nelze	lze	k6eNd1	lze
dopředu	dopředu	k6eAd1	dopředu
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
$	$	kIx~	$
<g/>
Badclus	Badclus	k1gInSc1	Badclus
drží	držet	k5eAaImIp3nS	držet
seznam	seznam	k1gInSc4	seznam
známých	známý	k2eAgInPc2d1	známý
vadných	vadný	k2eAgInPc2d1	vadný
clusterů	cluster	k1gInPc2	cluster
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
znovu	znovu	k6eAd1	znovu
nebudou	být	k5eNaImBp3nP	být
použity	použít	k5eAaPmNgInP	použít
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
nastane	nastat	k5eAaPmIp3nS	nastat
chyba	chyba	k1gFnSc1	chyba
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
systém	systém	k1gInSc1	systém
označí	označit	k5eAaPmIp3nS	označit
clustery	cluster	k1gInPc4	cluster
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
špatné	špatný	k2eAgFnPc4d1	špatná
a	a	k8xC	a
$	$	kIx~	$
<g/>
Badclus	Badclus	k1gInSc1	Badclus
se	se	k3xPyFc4	se
aktualizuje	aktualizovat	k5eAaBmIp3nS	aktualizovat
<g/>
;	;	kIx,	;
$	$	kIx~	$
<g/>
Bitmap	bitmapa	k1gFnPc2	bitmapa
je	být	k5eAaImIp3nS	být
jednorozměrné	jednorozměrný	k2eAgNnSc4d1	jednorozměrné
pole	pole	k1gNnSc4	pole
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
volného	volný	k2eAgNnSc2d1	volné
místa	místo	k1gNnSc2	místo
<g/>
;	;	kIx,	;
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
bit	bit	k1gInSc1	bit
0	[number]	k4	0
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
volný	volný	k2eAgMnSc1d1	volný
a	a	k8xC	a
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
použitý	použitý	k2eAgInSc4d1	použitý
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
patří	patřit	k5eAaImIp3nS	patřit
$	$	kIx~	$
<g/>
Boot	Boot	k1gInSc1	Boot
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
$	$	kIx~	$
<g/>
Volume	volum	k1gInSc5	volum
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Attef	Attef	k1gInSc1	Attef
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Quota	Quota	k1gFnSc1	Quota
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Upcase	Upcasa	k1gFnSc3	Upcasa
a	a	k8xC	a
.	.	kIx.	.
(	(	kIx(	(
<g/>
kořenový	kořenový	k2eAgInSc1d1	kořenový
adresář	adresář	k1gInSc1	adresář
disku	disk	k1gInSc2	disk
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
NTFS	NTFS	kA	NTFS
je	být	k5eAaImIp3nS	být
flexibilní	flexibilní	k2eAgFnSc1d1	flexibilní
–	–	k?	–
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
soubory	soubor	k1gInPc4	soubor
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
speciálních	speciální	k2eAgInPc2d1	speciální
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
boot	boota	k1gFnPc2	boota
sektoru	sektor	k1gInSc2	sektor
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
přesunout	přesunout	k5eAaPmF	přesunout
<g/>
.	.	kIx.	.
</s>
<s>
Soubory	soubor	k1gInPc1	soubor
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
mnoho	mnoho	k4c4	mnoho
atributů	atribut	k1gInPc2	atribut
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnSc2	jejich
definice	definice	k1gFnSc2	definice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
soubor	soubor	k1gInSc1	soubor
$	$	kIx~	$
<g/>
Attef	Attef	k1gInSc1	Attef
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
verzích	verze	k1gFnPc6	verze
se	se	k3xPyFc4	se
atributy	atribut	k1gInPc7	atribut
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
teoreticky	teoreticky	k6eAd1	teoreticky
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
možné	možný	k2eAgNnSc1d1	možné
přidávat	přidávat	k5eAaImF	přidávat
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgInSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgInSc1d1	klasický
soubor	soubor	k1gInSc1	soubor
má	mít	k5eAaImIp3nS	mít
mj.	mj.	kA	mj.
tyto	tento	k3xDgInPc4	tento
atributy	atribut	k1gInPc4	atribut
<g/>
:	:	kIx,	:
$	$	kIx~	$
<g/>
FILE_NAME	FILE_NAME	k1gFnSc1	FILE_NAME
–	–	k?	–
struktura	struktura	k1gFnSc1	struktura
pro	pro	k7c4	pro
jméno	jméno	k1gNnSc4	jméno
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
něj	on	k3xPp3gMnSc2	on
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
reference	reference	k1gFnSc1	reference
na	na	k7c4	na
nadřízený	nadřízený	k2eAgInSc4d1	nadřízený
adresář	adresář	k1gInSc4	adresář
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
příznaky	příznak	k1gInPc4	příznak
<g/>
.	.	kIx.	.
$	$	kIx~	$
<g/>
SECURITY_DESCRIPTOR	SECURITY_DESCRIPTOR	k1gFnSc1	SECURITY_DESCRIPTOR
–	–	k?	–
přístupová	přístupový	k2eAgNnPc4d1	přístupové
práva	právo	k1gNnPc4	právo
k	k	k7c3	k
souboru	soubor	k1gInSc3	soubor
$	$	kIx~	$
<g/>
DATA	datum	k1gNnPc1	datum
–	–	k?	–
vlastní	vlastní	k2eAgInSc4d1	vlastní
obsah	obsah	k1gInSc4	obsah
souboru	soubor	k1gInSc2	soubor
Z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
lze	lze	k6eAd1	lze
u	u	k7c2	u
souboru	soubor	k1gInSc2	soubor
číst	číst	k5eAaImF	číst
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
v	v	k7c6	v
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
datum	datum	k1gNnSc4	datum
a	a	k8xC	a
čas	čas	k1gInSc4	čas
vytvoření	vytvoření	k1gNnSc2	vytvoření
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgInSc2d1	poslední
zápisu	zápis	k1gInSc2	zápis
a	a	k8xC	a
posledního	poslední	k2eAgNnSc2d1	poslední
čtení	čtení	k1gNnSc2	čtení
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
atributy	atribut	k1gInPc1	atribut
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Skrytý	skrytý	k2eAgInSc4d1	skrytý
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jen	jen	k9	jen
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
pokročilé	pokročilý	k1gMnPc4	pokročilý
patří	patřit	k5eAaImIp3nS	patřit
možnost	možnost	k1gFnSc1	možnost
archivace	archivace	k1gFnSc1	archivace
<g/>
,	,	kIx,	,
šifrování	šifrování	k1gNnSc1	šifrování
(	(	kIx(	(
<g/>
per	prát	k5eAaImRp2nS	prát
uživatel	uživatel	k1gMnSc1	uživatel
OS	OS	kA	OS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komprimace	komprimace	k1gFnSc1	komprimace
<g/>
,	,	kIx,	,
indexace	indexace	k1gFnSc1	indexace
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Adresáře	adresář	k1gInPc1	adresář
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
NTFS	NTFS	kA	NTFS
pojaty	pojmout	k5eAaPmNgInP	pojmout
jako	jako	k8xC	jako
speciální	speciální	k2eAgInSc1d1	speciální
druh	druh	k1gInSc1	druh
souborů	soubor	k1gInPc2	soubor
<g/>
;	;	kIx,	;
používají	používat	k5eAaImIp3nP	používat
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
atributů	atribut	k1gInPc2	atribut
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
disk	disk	k1gInSc4	disk
jsou	být	k5eAaImIp3nP	být
vkládány	vkládán	k2eAgInPc1d1	vkládán
jako	jako	k8xS	jako
B-stromy	Btrom	k1gInPc1	B-strom
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jmény	jméno	k1gNnPc7	jméno
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
jejich	jejich	k3xOp3gInPc4	jejich
záznamy	záznam	k1gInPc4	záznam
v	v	k7c6	v
MFT	MFT	kA	MFT
<g/>
.	.	kIx.	.
</s>
<s>
NTFS	NTFS	kA	NTFS
používá	používat	k5eAaImIp3nS	používat
tento	tento	k3xDgInSc4	tento
atribut	atribut	k1gInSc4	atribut
k	k	k7c3	k
uložení	uložení	k1gNnSc3	uložení
jmen	jméno	k1gNnPc2	jméno
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
kopií	kopie	k1gFnSc7	kopie
atributů	atribut	k1gInPc2	atribut
standardních	standardní	k2eAgFnPc2d1	standardní
informací	informace	k1gFnPc2	informace
pro	pro	k7c4	pro
soubory	soubor	k1gInPc4	soubor
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
adresáři	adresář	k1gInSc6	adresář
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
procházení	procházení	k1gNnSc4	procházení
adresářů	adresář	k1gInPc2	adresář
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
číst	číst	k5eAaImF	číst
MFT	MFT	kA	MFT
záznamy	záznam	k1gInPc4	záznam
souborů	soubor	k1gInPc2	soubor
v	v	k7c6	v
adresáři	adresář	k1gInSc6	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
malou	malý	k2eAgFnSc4d1	malá
hlavičku	hlavička	k1gFnSc4	hlavička
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgInPc4d1	základní
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
záznamu	záznam	k1gInSc6	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
hlavičkou	hlavička	k1gFnSc7	hlavička
následuje	následovat	k5eAaImIp3nS	následovat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
atributů	atribut	k1gInPc2	atribut
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
data	datum	k1gNnPc4	datum
nebo	nebo	k8xC	nebo
typ	typ	k1gInSc4	typ
souboru	soubor	k1gInSc2	soubor
či	či	k8xC	či
adresáře	adresář	k1gInSc2	adresář
odpovídajícímu	odpovídající	k2eAgInSc3d1	odpovídající
záznamu	záznam	k1gInSc3	záznam
<g/>
.	.	kIx.	.
</s>
<s>
Hlavička	hlavička	k1gFnSc1	hlavička
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
NTFS	NTFS	kA	NTFS
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
ověření	ověření	k1gNnSc4	ověření
integrity	integrita	k1gFnSc2	integrita
<g/>
,	,	kIx,	,
ukazatel	ukazatel	k1gInSc1	ukazatel
na	na	k7c4	na
první	první	k4xOgInSc4	první
atribut	atribut	k1gInSc4	atribut
v	v	k7c6	v
záznamu	záznam	k1gInSc6	záznam
<g/>
,	,	kIx,	,
ukazatel	ukazatel	k1gInSc1	ukazatel
na	na	k7c4	na
první	první	k4xOgInSc4	první
volný	volný	k2eAgInSc4d1	volný
bajt	bajt	k1gInSc4	bajt
v	v	k7c6	v
záznamu	záznam	k1gInSc6	záznam
a	a	k8xC	a
číslo	číslo	k1gNnSc1	číslo
prvního	první	k4xOgInSc2	první
(	(	kIx(	(
<g/>
hlavního	hlavní	k2eAgInSc2d1	hlavní
<g/>
)	)	kIx)	)
záznamu	záznam	k1gInSc2	záznam
v	v	k7c6	v
MFT	MFT	kA	MFT
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
záznam	záznam	k1gInSc1	záznam
není	být	k5eNaImIp3nS	být
první	první	k4xOgFnSc4	první
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
adresářů	adresář	k1gInPc2	adresář
a	a	k8xC	a
souborů	soubor	k1gInPc2	soubor
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
255	[number]	k4	255
znaků	znak	k1gInPc2	znak
v	v	k7c4	v
kódování	kódování	k1gNnSc4	kódování
Unicode	Unicod	k1gInSc5	Unicod
(	(	kIx(	(
<g/>
UTF-	UTF-	k1gFnPc2	UTF-
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Posixovém	Posixový	k2eAgInSc6d1	Posixový
namespace	namespace	k1gFnPc4	namespace
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
UTF-16	UTF-16	k1gFnPc3	UTF-16
znak	znak	k1gInSc4	znak
(	(	kIx(	(
<g/>
case	casus	k1gMnSc5	casus
sensitive	sensitiv	k1gMnSc5	sensitiv
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
znaku	znak	k1gInSc2	znak
NULL	NULL	kA	NULL
a	a	k8xC	a
lomítko	lomítko	k1gNnSc4	lomítko
(	(	kIx(	(
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Win	Win	k1gFnSc6	Win
<g/>
32	[number]	k4	32
namespace	namespace	k1gFnSc2	namespace
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
UTF-16	UTF-16	k1gMnSc1	UTF-16
znak	znak	k1gInSc1	znak
(	(	kIx(	(
<g/>
case	case	k1gInSc1	case
insensitive	insensitiv	k1gInSc5	insensitiv
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
znaku	znak	k1gInSc2	znak
NULL	NULL	kA	NULL
<g/>
,	,	kIx,	,
lomítko	lomítko	k1gNnSc1	lomítko
(	(	kIx(	(
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpětné	zpětný	k2eAgNnSc1d1	zpětné
lomítko	lomítko	k1gNnSc1	lomítko
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
(	(	kIx(	(
<g/>
:	:	kIx,	:
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hvězdička	hvězdička	k1gFnSc1	hvězdička
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otazník	otazník	k1gInSc1	otazník
(	(	kIx(	(
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uvozovky	uvozovka	k1gFnPc1	uvozovka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
(	(	kIx(	(
<g/>
<	<	kIx(	<
<g/>
)	)	kIx)	)
větší	veliký	k2eAgFnPc4d2	veliký
než	než	k8xS	než
(	(	kIx(	(
<g/>
>	>	kIx)	>
<g/>
)	)	kIx)	)
a	a	k8xC	a
svislá	svislý	k2eAgFnSc1d1	svislá
čára	čára	k1gFnSc1	čára
(	(	kIx(	(
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
jména	jméno	k1gNnPc4	jméno
speciálních	speciální	k2eAgInPc2d1	speciální
souborů	soubor	k1gInPc2	soubor
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
svazku	svazek	k1gInSc6	svazek
<g/>
:	:	kIx,	:
$	$	kIx~	$
<g/>
MFT	MFT	kA	MFT
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
MFTMirr	MFTMirr	k1gInSc1	MFTMirr
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
LogFile	LogFila	k1gFnSc3	LogFila
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Volume	volum	k1gInSc5	volum
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Attef	Attef	k1gInSc1	Attef
<g/>
,	,	kIx,	,
.	.	kIx.	.
(	(	kIx(	(
<g/>
tečka	tečka	k1gFnSc1	tečka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Bitmap	bitmapa	k1gFnPc2	bitmapa
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Boot	Boot	k1gInSc1	Boot
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
BadClus	BadClus	k1gInSc1	BadClus
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Secure	Secur	k1gMnSc5	Secur
<g/>
,	,	kIx,	,
$	$	kIx~	$
<g/>
Upcase	Upcas	k1gMnSc5	Upcas
<g/>
,	,	kIx,	,
a	a	k8xC	a
$	$	kIx~	$
<g/>
Extend	Extend	k1gInSc1	Extend
(	(	kIx(	(
<g/>
tečka	tečka	k1gFnSc1	tečka
a	a	k8xC	a
$	$	kIx~	$
<g/>
Extend	Extend	k1gInSc1	Extend
jsou	být	k5eAaImIp3nP	být
adresáře	adresář	k1gInPc1	adresář
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
soubory	soubor	k1gInPc1	soubor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
velikost	velikost	k1gFnSc1	velikost
svazku	svazek	k1gInSc2	svazek
Teoreticky	teoreticky	k6eAd1	teoreticky
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgFnSc1d1	maximální
velikost	velikost	k1gFnSc1	velikost
NTFS	NTFS	kA	NTFS
svazku	svazek	k1gInSc2	svazek
264-1	[number]	k4	264-1
alokačních	alokační	k2eAgFnPc2d1	alokační
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
implementace	implementace	k1gFnSc2	implementace
ve	v	k7c6	v
Windows	Windows	kA	Windows
XP	XP	kA	XP
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
232-1	[number]	k4	232-1
alokačních	alokační	k2eAgFnPc2d1	alokační
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
64	[number]	k4	64
KiB	KiB	k1gFnPc2	KiB
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
256	[number]	k4	256
TiB	tiba	k1gFnPc2	tiba
mínus	mínus	k6eAd1	mínus
64	[number]	k4	64
KiB	KiB	k1gFnPc2	KiB
<g/>
,	,	kIx,	,
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
implicitní	implicitní	k2eAgFnSc4d1	implicitní
velikost	velikost	k1gFnSc4	velikost
4	[number]	k4	4
KiB	KiB	k1gFnPc2	KiB
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
TiB	tiba	k1gFnPc2	tiba
mínus	mínus	k6eAd1	mínus
4	[number]	k4	4
KiB	KiB	k1gFnPc2	KiB
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc4	velikost
klasického	klasický	k2eAgInSc2d1	klasický
oddílu	oddíl	k1gInSc2	oddíl
(	(	kIx(	(
<g/>
MBR	MBR	kA	MBR
<g/>
)	)	kIx)	)
limitována	limitovat	k5eAaBmNgFnS	limitovat
velikostí	velikost	k1gFnSc7	velikost
2	[number]	k4	2
TiB	tiba	k1gFnPc2	tiba
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
větší	veliký	k2eAgInPc4d2	veliký
oddíly	oddíl	k1gInPc4	oddíl
použit	použit	k2eAgInSc4d1	použit
dynamický	dynamický	k2eAgInSc4d1	dynamický
disk	disk	k1gInSc4	disk
nebo	nebo	k8xC	nebo
GPT	GPT	kA	GPT
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
velikost	velikost	k1gFnSc1	velikost
souboru	soubor	k1gInSc2	soubor
Teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
:	:	kIx,	:
16	[number]	k4	16
EiB	EiB	k1gFnPc2	EiB
mínus	mínus	k6eAd1	mínus
1	[number]	k4	1
KiB	KiB	k1gFnPc2	KiB
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
64	[number]	k4	64
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
10	[number]	k4	10
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
64	[number]	k4	64
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
10	[number]	k4	10
<g/>
}}	}}	k?	}}
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Implementováno	implementován	k2eAgNnSc1d1	implementováno
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
TiB	tiba	k1gFnPc2	tiba
mínus	mínus	k6eAd1	mínus
64	[number]	k4	64
KiB	KiB	k1gFnPc2	KiB
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
44	[number]	k4	44
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
16	[number]	k4	16
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
44	[number]	k4	44
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
16	[number]	k4	16
<g/>
}}	}}	k?	}}
bajtů	bajt	k1gInPc2	bajt
<g/>
)	)	kIx)	)
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
cesty	cesta	k1gFnSc2	cesta
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
délka	délka	k1gFnSc1	délka
cesty	cesta	k1gFnSc2	cesta
až	až	k9	až
32767	[number]	k4	32767
znaků	znak	k1gInPc2	znak
<g/>
;	;	kIx,	;
relativní	relativní	k2eAgFnSc1d1	relativní
délka	délka	k1gFnSc1	délka
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
limitována	limitovat	k5eAaBmNgFnS	limitovat
na	na	k7c4	na
255	[number]	k4	255
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
času	čas	k1gInSc2	čas
NTFS	NTFS	kA	NTFS
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
času	čas	k1gInSc2	čas
stejnou	stejný	k2eAgFnSc4d1	stejná
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Windows	Windows	kA	Windows
NT	NT	kA	NT
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
64	[number]	k4	64
<g/>
bitové	bitový	k2eAgNnSc1d1	bitové
číslo	číslo	k1gNnSc1	číslo
s	s	k7c7	s
rozsahem	rozsah	k1gInSc7	rozsah
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1601	[number]	k4	1601
do	do	k7c2	do
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
60056	[number]	k4	60056
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
tiků	tik	k1gInPc2	tik
na	na	k7c4	na
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
desetina	desetina	k1gFnSc1	desetina
mikrosekundy	mikrosekunda	k1gFnSc2	mikrosekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1	alternativní
datové	datový	k2eAgInPc1d1	datový
proudy	proud	k1gInPc1	proud
Systémová	systémový	k2eAgFnSc1d1	systémová
volání	volání	k1gNnSc1	volání
Windows	Windows	kA	Windows
mohou	moct	k5eAaImIp3nP	moct
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nemusí	muset	k5eNaImIp3nP	muset
podporovat	podporovat	k5eAaImF	podporovat
datové	datový	k2eAgInPc1d1	datový
proudy	proud	k1gInPc1	proud
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
nástrojích	nástroj	k1gInPc6	nástroj
a	a	k8xC	a
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
systému	systém	k1gInSc6	systém
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
souboru	soubor	k1gInSc2	soubor
může	moct	k5eAaImIp3nS	moct
tiše	tiš	k1gFnPc4	tiš
přerušit	přerušit	k5eAaPmF	přerušit
datový	datový	k2eAgInSc1d1	datový
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
způsob	způsob	k1gInSc4	způsob
pro	pro	k7c4	pro
kopírování	kopírování	k1gNnSc4	kopírování
a	a	k8xC	a
přesun	přesun	k1gInSc1	přesun
souborů	soubor	k1gInPc2	soubor
pouze	pouze	k6eAd1	pouze
použití	použití	k1gNnSc1	použití
systémových	systémový	k2eAgNnPc2d1	systémové
volání	volání	k1gNnPc2	volání
BackupRead	BackupRead	k1gInSc1	BackupRead
a	a	k8xC	a
BackupWrite	BackupWrit	k1gInSc5	BackupWrit
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
sledovat	sledovat	k5eAaImF	sledovat
datový	datový	k2eAgInSc4d1	datový
proud	proud	k1gInSc4	proud
a	a	k8xC	a
zjistit	zjistit	k5eAaPmF	zjistit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
přenos	přenos	k1gInSc1	přenos
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
a	a	k8xC	a
eventuálně	eventuálně	k6eAd1	eventuálně
přeskočit	přeskočit	k5eAaPmF	přeskočit
přerušené	přerušený	k2eAgInPc4d1	přerušený
proudy	proud	k1gInPc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Fragmentace	fragmentace	k1gFnPc4	fragmentace
Souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
NTFS	NTFS	kA	NTFS
již	již	k9	již
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
počátku	počátek	k1gInSc2	počátek
velice	velice	k6eAd1	velice
trpí	trpět	k5eAaImIp3nS	trpět
svou	svůj	k3xOyFgFnSc7	svůj
nepříjemnou	příjemný	k2eNgFnSc7d1	nepříjemná
vlastností	vlastnost	k1gFnSc7	vlastnost
-	-	kIx~	-
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
fragmentuje	fragmentovat	k5eAaBmIp3nS	fragmentovat
soubory	soubor	k1gInPc7	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Čtení	čtení	k1gNnSc1	čtení
takto	takto	k6eAd1	takto
fragmentovaných	fragmentovaný	k2eAgInPc2d1	fragmentovaný
souborů	soubor	k1gInPc2	soubor
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zbytečně	zbytečně	k6eAd1	zbytečně
pomalé	pomalý	k2eAgNnSc1d1	pomalé
a	a	k8xC	a
čím	co	k3yQnSc7	co
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
takovýto	takovýto	k3xDgInSc1	takovýto
soubor	soubor	k1gInSc1	soubor
fragmentován	fragmentován	k2eAgInSc1d1	fragmentován
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
čtení	čtení	k1gNnSc1	čtení
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Windows	Windows	kA	Windows
7	[number]	k4	7
implicitně	implicitně	k6eAd1	implicitně
nastavena	nastaven	k2eAgFnSc1d1	nastavena
defragmentace	defragmentace	k1gFnSc1	defragmentace
automaticky	automaticky	k6eAd1	automaticky
jednou	jeden	k4xCgFnSc7	jeden
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
podporu	podpora	k1gFnSc4	podpora
NTFS	NTFS	kA	NTFS
v	v	k7c6	v
Linuxu	linux	k1gInSc6	linux
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ovladač	ovladač	k1gInSc4	ovladač
NTFS-	NTFS-	k1gFnSc1	NTFS-
<g/>
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
běží	běžet	k5eAaImIp3nS	běžet
v	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
prostoru	prostor	k1gInSc6	prostor
(	(	kIx(	(
<g/>
využívá	využívat	k5eAaPmIp3nS	využívat
modul	modul	k1gInSc1	modul
FUSE	fuse	k1gFnSc2	fuse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ovladač	ovladač	k1gInSc1	ovladač
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
omezeně	omezeně	k6eAd1	omezeně
i	i	k9	i
zápis	zápis	k1gInSc1	zápis
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Linux-NTFS	Linux-NTFS	k1gFnSc2	Linux-NTFS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
ovladače	ovladač	k1gInPc1	ovladač
poskytující	poskytující	k2eAgFnSc4d1	poskytující
plnou	plný	k2eAgFnSc4d1	plná
podporu	podpora	k1gFnSc4	podpora
čtení	čtení	k1gNnSc2	čtení
i	i	k8xC	i
zápisu	zápis	k1gInSc2	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
možné	možný	k2eAgNnSc1d1	možné
spustit	spustit	k5eAaPmF	spustit
v	v	k7c6	v
emulátoru	emulátor	k1gInSc6	emulátor
Captive-NTFS	Captive-NTFS	k1gFnSc2	Captive-NTFS
ovladače	ovladač	k1gInSc2	ovladač
z	z	k7c2	z
originální	originální	k2eAgFnSc2d1	originální
instalace	instalace	k1gFnSc2	instalace
MS	MS	kA	MS
Windows	Windows	kA	Windows
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
soubory	soubor	k1gInPc4	soubor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
získat	získat	k5eAaPmF	získat
buď	buď	k8xC	buď
ze	z	k7c2	z
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
XP	XP	kA	XP
SP	SP	kA	SP
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
instalačního	instalační	k2eAgInSc2d1	instalační
balíčku	balíček	k1gInSc2	balíček
SP1	SP1	k1gFnSc2	SP1
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
XP	XP	kA	XP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
je	být	k5eAaImIp3nS	být
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
různé	různý	k2eAgFnPc1d1	různá
verze	verze	k1gFnPc1	verze
NTFS	NTFS	kA	NTFS
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
zpětně	zpětně	k6eAd1	zpětně
kompatibilní	kompatibilní	k2eAgInPc1d1	kompatibilní
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
technické	technický	k2eAgInPc4d1	technický
důvody	důvod	k1gInPc4	důvod
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
novějších	nový	k2eAgFnPc2d2	novější
NTFS	NTFS	kA	NTFS
ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
verzích	verze	k1gFnPc6	verze
systému	systém	k1gInSc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
duální	duální	k2eAgNnPc4d1	duální
zavádění	zavádění	k1gNnPc4	zavádění
a	a	k8xC	a
externí	externí	k2eAgInPc4d1	externí
přenosné	přenosný	k2eAgInPc4d1	přenosný
pevné	pevný	k2eAgInPc4d1	pevný
disky	disk	k1gInPc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
použít	použít	k5eAaPmF	použít
NTFS	NTFS	kA	NTFS
s	s	k7c7	s
vlastností	vlastnost	k1gFnSc7	vlastnost
"	"	kIx"	"
<g/>
Volume	volum	k1gInSc5	volum
Shadow	Shadow	k1gFnSc1	Shadow
Copy	cop	k1gInPc4	cop
<g/>
"	"	kIx"	"
na	na	k7c6	na
starším	starý	k2eAgInSc6d2	starší
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
<g/>
,	,	kIx,	,
povede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
příkaz	příkaz	k1gInSc1	příkaz
convert	convert	k1gInSc4	convert
<g/>
.	.	kIx.	.
<g/>
exe	exe	k?	exe
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
převést	převést	k5eAaPmF	převést
některé	některý	k3yIgInPc4	některý
podporované	podporovaný	k2eAgInPc4d1	podporovaný
souborové	souborový	k2eAgInPc4d1	souborový
systémy	systém	k1gInPc4	systém
na	na	k7c6	na
NTFS	NTFS	kA	NTFS
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
HPFS	HPFS	kA	HPFS
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
NT	NT	kA	NT
3.1	[number]	k4	3.1
<g/>
,	,	kIx,	,
3.5	[number]	k4	3.5
a	a	k8xC	a
3.51	[number]	k4	3.51
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
FAT16	FAT16	k1gMnSc1	FAT16
a	a	k8xC	a
FAT32	FAT32	k1gMnSc1	FAT32
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Windows	Windows	kA	Windows
2000	[number]	k4	2000
a	a	k8xC	a
novější	nový	k2eAgInSc4d2	novější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
10.3	[number]	k4	10.3
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc1d2	vyšší
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
podporu	podpora	k1gFnSc4	podpora
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
pro	pro	k7c4	pro
oddíly	oddíl	k1gInPc4	oddíl
formátované	formátovaný	k2eAgFnPc1d1	formátovaná
systémem	systém	k1gInSc7	systém
NTFS	NTFS	kA	NTFS
<g/>
.	.	kIx.	.
</s>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
NTFS-	NTFS-	k1gFnSc1	NTFS-
<g/>
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
šířen	šířen	k2eAgInSc1d1	šířen
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GPL	GPL	kA	GPL
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
systémech	systém	k1gInPc6	systém
OS	osa	k1gFnPc2	osa
X	X	kA	X
a	a	k8xC	a
Linux	Linux	kA	Linux
pomocí	pomoc	k1gFnSc7	pomoc
FUSE	fuse	k1gFnSc2	fuse
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Filesystem	Filesyst	k1gInSc7	Filesyst
in	in	k?	in
Userspace	Userspace	k1gFnSc1	Userspace
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
čtení	čtení	k1gNnSc1	čtení
i	i	k8xC	i
zápis	zápis	k1gInSc1	zápis
na	na	k7c4	na
NTFS	NTFS	kA	NTFS
oddíly	oddíl	k1gInPc1	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vývojářů	vývojář	k1gMnPc2	vývojář
NTFS-3G	NTFS-3G	k1gFnSc2	NTFS-3G
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
výkonnější	výkonný	k2eAgFnPc1d2	výkonnější
komerční	komerční	k2eAgFnPc1d1	komerční
verze	verze	k1gFnPc1	verze
s	s	k7c7	s
názvem	název	k1gInSc7	název
Tuxera	Tuxero	k1gNnSc2	Tuxero
"	"	kIx"	"
<g/>
NTFS	NTFS	kA	NTFS
for	forum	k1gNnPc2	forum
Mac	Mac	kA	Mac
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Paragon	paragon	k1gInSc1	paragon
Software	software	k1gInSc1	software
Group	Group	k1gInSc1	Group
prodává	prodávat	k5eAaImIp3nS	prodávat
ovladač	ovladač	k1gInSc4	ovladač
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
i	i	k9	i
zápis	zápis	k1gInSc1	zápis
s	s	k7c7	s
názvem	název	k1gInSc7	název
NTFS	NTFS	kA	NTFS
pro	pro	k7c4	pro
OS	OS	kA	OS
X	X	kA	X
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgInPc2	některý
modelů	model	k1gInPc2	model
pevných	pevný	k2eAgInPc2d1	pevný
disků	disk	k1gInPc2	disk
Seagate	Seagate	kA	Seagate
<g/>
.	.	kIx.	.
</s>
<s>
Nativní	nativní	k2eAgFnSc1d1	nativní
podpora	podpora	k1gFnSc1	podpora
zápisu	zápis	k1gInSc2	zápis
na	na	k7c6	na
NTFS	NTFS	kA	NTFS
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
systému	systém	k1gInSc6	systém
OS	OS	kA	OS
X	X	kA	X
10.6	[number]	k4	10.6
a	a	k8xC	a
novějších	nový	k2eAgFnPc2d2	novější
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
výchozím	výchozí	k2eAgNnSc6d1	výchozí
nastavení	nastavení	k1gNnSc6	nastavení
aktivována	aktivovat	k5eAaBmNgFnS	aktivovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
povolit	povolit	k5eAaPmF	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zkušenosti	zkušenost	k1gFnPc1	zkušenost
uživatelů	uživatel	k1gMnPc2	uživatel
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podpora	podpora	k1gFnSc1	podpora
není	být	k5eNaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
způsobit	způsobit	k5eAaPmF	způsobit
pád	pád	k1gInSc4	pád
jádra	jádro	k1gNnSc2	jádro
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nebyla	být	k5eNaImAgFnS	být
podpora	podpora	k1gFnSc1	podpora
zápisu	zápis	k1gInSc2	zápis
ve	v	k7c6	v
výchozím	výchozí	k2eAgNnSc6d1	výchozí
nastavení	nastavení	k1gNnSc6	nastavení
povolena	povolit	k5eAaPmNgFnS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Linuxová	linuxový	k2eAgFnSc1d1	linuxová
verze	verze	k1gFnSc1	verze
jádra	jádro	k1gNnSc2	jádro
2.2	[number]	k4	2.2
<g/>
.0	.0	k4	.0
a	a	k8xC	a
novější	nový	k2eAgInPc1d2	novější
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
schopnost	schopnost	k1gFnSc4	schopnost
číst	číst	k5eAaImF	číst
oddíly	oddíl	k1gInPc4	oddíl
NTFS	NTFS	kA	NTFS
<g/>
;	;	kIx,	;
verze	verze	k1gFnSc1	verze
jádra	jádro	k1gNnSc2	jádro
2.6	[number]	k4	2.6
<g/>
.0	.0	k4	.0
a	a	k8xC	a
novější	nový	k2eAgInPc1d2	novější
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ovladač	ovladač	k1gInSc1	ovladač
od	od	k7c2	od
Antona	Anton	k1gMnSc2	Anton
Altaparmakova	Altaparmakův	k2eAgMnSc2d1	Altaparmakův
(	(	kIx(	(
<g/>
University	universita	k1gFnSc2	universita
of	of	k?	of
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
)	)	kIx)	)
a	a	k8xC	a
Richarda	Richard	k1gMnSc2	Richard
Russona	Russon	k1gMnSc2	Russon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
čtení	čtení	k1gNnSc4	čtení
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
přepis	přepis	k1gInSc4	přepis
a	a	k8xC	a
změnu	změna	k1gFnSc4	změna
jeho	jeho	k3xOp3gFnSc2	jeho
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
uživatelské	uživatelský	k2eAgInPc1d1	uživatelský
prostory	prostor	k1gInPc1	prostor
ovladače	ovladač	k1gInSc2	ovladač
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
NTFS	NTFS	kA	NTFS
-	-	kIx~	-
NTFSMount	NTFSMount	k1gMnSc1	NTFSMount
<g/>
,	,	kIx,	,
NTFS-3G	NTFS-3G	k1gMnSc1	NTFS-3G
a	a	k8xC	a
Captive	Captiv	k1gInSc5	Captiv
NTFS	NTFS	kA	NTFS
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
obalující	obalující	k2eAgInSc1d1	obalující
<g/>
"	"	kIx"	"
ovladač	ovladač	k1gInSc1	ovladač
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
používá	používat	k5eAaImIp3nS	používat
vlastní	vlastní	k2eAgInSc4d1	vlastní
ovladač	ovladač	k1gInSc4	ovladač
systému	systém	k1gInSc2	systém
Windows	Windows	kA	Windows
ntfs	ntfs	k6eAd1	ntfs
<g/>
.	.	kIx.	.
<g/>
sys	sys	k?	sys
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c6	na
souborovém	souborový	k2eAgInSc6d1	souborový
systému	systém	k1gInSc6	systém
v	v	k7c6	v
uživatelském	uživatelský	k2eAgInSc6d1	uživatelský
prostoru	prostor	k1gInSc6	prostor
(	(	kIx(	(
<g/>
FUSE	fuse	k1gFnSc1	fuse
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modul	modul	k1gInSc1	modul
Linux	Linux	kA	Linux
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
přemostění	přemostění	k1gNnSc1	přemostění
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
kernel	kernel	k1gInSc1	kernel
kódu	kód	k1gInSc2	kód
k	k	k7c3	k
uložení	uložení	k1gNnSc3	uložení
a	a	k8xC	a
získání	získání	k1gNnSc4	získání
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
jsou	být	k5eAaImIp3nP	být
licencovány	licencovat	k5eAaBmNgInP	licencovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podmínek	podmínka	k1gFnPc2	podmínka
uvedených	uvedený	k2eAgFnPc2d1	uvedená
v	v	k7c6	v
licenci	licence	k1gFnSc6	licence
GNU	gnu	k1gNnPc2	gnu
General	General	k1gMnPc2	General
Public	publicum	k1gNnPc2	publicum
License	License	k1gFnSc1	License
(	(	kIx(	(
<g/>
GPL	GPL	kA	GPL
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
složitosti	složitost	k1gFnSc3	složitost
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
struktur	struktura	k1gFnPc2	struktura
NTFS	NTFS	kA	NTFS
zakazují	zakazovat	k5eAaImIp3nP	zakazovat
vestavěný	vestavěný	k2eAgInSc4d1	vestavěný
ovladač	ovladač	k1gInSc4	ovladač
jádra	jádro	k1gNnSc2	jádro
2.6	[number]	k4	2.6
<g/>
.14	.14	k4	.14
i	i	k8xC	i
ovladače	ovladač	k1gInPc1	ovladač
FUSE	fuse	k1gFnSc2	fuse
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
dvě	dva	k4xCgNnPc1	dva
proprietární	proprietární	k2eAgNnPc1d1	proprietární
řešení	řešení	k1gNnPc1	řešení
<g/>
:	:	kIx,	:
Tuxera	Tuxera	k1gFnSc1	Tuxera
NTFS	NTFS	kA	NTFS
-	-	kIx~	-
Vysoce	vysoce	k6eAd1	vysoce
výkonný	výkonný	k2eAgInSc1d1	výkonný
čtecí	čtecí	k2eAgInSc1d1	čtecí
/	/	kIx~	/
zapisovací	zapisovací	k2eAgInSc1d1	zapisovací
komerční	komerční	k2eAgInSc1d1	komerční
kernel	kernel	k1gInSc1	kernel
ovladač	ovladač	k1gInSc1	ovladač
<g/>
,	,	kIx,	,
cílený	cílený	k2eAgInSc1d1	cílený
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
vestavené	vestavený	k2eAgNnSc4d1	vestavený
zařízení	zařízení	k1gNnSc4	zařízení
od	od	k7c2	od
Tuxera	Tuxero	k1gNnSc2	Tuxero
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
NTFS-	NTFS-	k1gFnSc4	NTFS-
<g/>
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
;	;	kIx,	;
NTFS	NTFS	kA	NTFS
pro	pro	k7c4	pro
Linux	linux	k1gInSc4	linux
-	-	kIx~	-
Komerční	komerční	k2eAgInSc4d1	komerční
ovladač	ovladač	k1gInSc4	ovladač
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
podporou	podpora	k1gFnSc7	podpora
čtení	čtení	k1gNnSc4	čtení
/	/	kIx~	/
zápisu	zápis	k1gInSc2	zápis
z	z	k7c2	z
Paragon	paragon	k1gInSc4	paragon
Software	software	k1gInSc1	software
Group	Group	k1gInSc1	Group
<g/>
.	.	kIx.	.
eComStation	eComStation	k1gInSc1	eComStation
a	a	k8xC	a
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
nabízí	nabízet	k5eAaImIp3nS	nabízet
jen	jen	k9	jen
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
podporu	podpor	k1gInSc2	podpor
NTFS	NTFS	kA	NTFS
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
beta	beta	k1gNnSc1	beta
ovladač	ovladač	k1gInSc1	ovladač
NTFS	NTFS	kA	NTFS
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
psát	psát	k5eAaImF	psát
/	/	kIx~	/
vymazat	vymazat	k5eAaPmF	vymazat
pro	pro	k7c4	pro
eComStation	eComStation	k1gInSc4	eComStation
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bezplatný	bezplatný	k2eAgInSc4d1	bezplatný
nástroj	nástroj	k1gInSc4	nástroj
třetí	třetí	k4xOgFnSc2	třetí
strany	strana	k1gFnSc2	strana
pro	pro	k7c4	pro
BeOS	BeOS	k1gMnSc4	BeOS
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
NTFS-	NTFS-	k1gFnSc6	NTFS-
<g/>
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
plné	plný	k2eAgFnSc3d1	plná
NTFS	NTFS	kA	NTFS
čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
zápis	zápis	k1gInSc4	zápis
<g/>
.	.	kIx.	.
</s>
<s>
NTFS-3G	NTFS-3G	k4	NTFS-3G
pracuje	pracovat	k5eAaImIp3nS	pracovat
také	také	k9	také
na	na	k7c4	na
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
<g/>
,	,	kIx,	,
FreeBSD	FreeBSD	k1gFnSc1	FreeBSD
<g/>
,	,	kIx,	,
NetBSD	NetBSD	k1gFnSc1	NetBSD
<g/>
,	,	kIx,	,
Solaris	Solaris	k1gFnSc1	Solaris
<g/>
,	,	kIx,	,
QNX	QNX	kA	QNX
a	a	k8xC	a
Haiku	Haika	k1gFnSc4	Haika
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
přes	přes	k7c4	přes
FUSE	fuse	k1gFnPc4	fuse
v	v	k7c6	v
Linuxu	linux	k1gInSc6	linux
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
ovladač	ovladač	k1gInSc4	ovladač
ke	k	k7c3	k
čtení	čtení	k1gNnSc3	čtení
/	/	kIx~	/
zápis	zápis	k1gInSc4	zápis
pro	pro	k7c4	pro
osobní	osobní	k2eAgNnSc4d1	osobní
použití	použití	k1gNnSc4	použití
pro	pro	k7c4	pro
MS-DOS	MS-DOS	k1gFnSc4	MS-DOS
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
NTFS	NTFS	kA	NTFS
<g/>
4	[number]	k4	4
<g/>
DOS	DOS	kA	DOS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ahead	Ahead	k6eAd1	Ahead
Software	software	k1gInSc4	software
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ovladač	ovladač	k1gInSc4	ovladač
"	"	kIx"	"
<g/>
NTFSREAD	NTFSREAD	kA	NTFSREAD
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
verze	verze	k1gFnSc1	verze
1.200	[number]	k4	1.200
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
DR-DOS	DR-DOS	k1gFnSc4	DR-DOS
7.0	[number]	k4	7.0
<g/>
x	x	k?	x
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2002	[number]	k4	2002
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
část	část	k1gFnSc1	část
jejich	jejich	k3xOp3gFnSc2	jejich
Nero	Nero	k1gMnSc1	Nero
Burning	Burning	k1gInSc1	Burning
ROM	ROM	kA	ROM
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
OpenBSD	OpenBSD	k?	OpenBSD
nabízí	nabízet	k5eAaImIp3nS	nabízet
nativní	nativní	k2eAgFnSc4d1	nativní
podporu	podpora	k1gFnSc4	podpora
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
NTFS	NTFS	kA	NTFS
ve	v	k7c6	v
výchozím	výchozí	k2eAgNnSc6d1	výchozí
nastavení	nastavení	k1gNnSc6	nastavení
na	na	k7c4	na
i	i	k9	i
<g/>
386	[number]	k4	386
a	a	k8xC	a
amd	amd	k?	amd
<g/>
64	[number]	k4	64
platformy	platforma	k1gFnSc2	platforma
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
4.9	[number]	k4	4.9
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
/	/	kIx~	/
zápis	zápis	k1gInSc1	zápis
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
NTFS-3G	NTFS-3G	k1gFnSc2	NTFS-3G
v	v	k7c6	v
OpenBSD	OpenBSD	k1gFnSc6	OpenBSD
-	-	kIx~	-
aktuálně	aktuálně	k6eAd1	aktuálně
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
první	první	k4xOgNnPc4	první
vydání	vydání	k1gNnPc4	vydání
je	být	k5eAaImIp3nS	být
OpenBSD	OpenBSD	k1gFnSc1	OpenBSD
5.5	[number]	k4	5.5
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
OpenBSD	OpenBSD	k?	OpenBSD
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
implementaci	implementace	k1gFnSc4	implementace
FUSE	fuse	k1gFnSc2	fuse
<g/>
;	;	kIx,	;
NTFS-3G	NTFS-3G	k1gMnSc1	NTFS-3G
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
z	z	k7c2	z
portů	port	k1gInPc2	port
<g/>
.	.	kIx.	.
</s>
<s>
ReFS	ReFS	k?	ReFS
–	–	k?	–
nový	nový	k2eAgMnSc1d1	nový
nástupce	nástupce	k1gMnSc1	nástupce
NTFS	NTFS	kA	NTFS
představený	představený	k1gMnSc1	představený
ve	v	k7c6	v
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2012	[number]	k4	2012
WinFS	WinFS	k1gMnSc1	WinFS
–	–	k?	–
již	již	k6eAd1	již
ukončený	ukončený	k2eAgInSc4d1	ukončený
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
nástupce	nástupce	k1gMnSc4	nástupce
NTFS	NTFS	kA	NTFS
http://www.linux-ntfs.org	[url]	k1gInSc1	http://www.linux-ntfs.org
–	–	k?	–
projekt	projekt	k1gInSc1	projekt
Linux-NTFS	Linux-NTFS	k1gFnPc2	Linux-NTFS
<g/>
,	,	kIx,	,
vyvíjející	vyvíjející	k2eAgInPc1d1	vyvíjející
ovladače	ovladač	k1gInPc1	ovladač
pro	pro	k7c4	pro
NTFS	NTFS	kA	NTFS
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Linux	Linux	kA	Linux
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mnoho	mnoho	k4c4	mnoho
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
struktuře	struktura	k1gFnSc6	struktura
NTFS	NTFS	kA	NTFS
<g/>
.	.	kIx.	.
http://www.ntfs-3g.org	[url]	k1gMnSc1	http://www.ntfs-3g.org
–	–	k?	–
NTFS-	NTFS-	k1gMnSc1	NTFS-
<g/>
3	[number]	k4	3
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
zápis	zápis	k1gInSc4	zápis
i	i	k8xC	i
čtení	čtení	k1gNnSc4	čtení
NTFS	NTFS	kA	NTFS
z	z	k7c2	z
Linuxu	linux	k1gInSc2	linux
http://www.milannemec.com/ntfs.html	[url]	k5eAaPmAgMnS	http://www.milannemec.com/ntfs.html
–	–	k?	–
podrobný	podrobný	k2eAgInSc4d1	podrobný
český	český	k2eAgInSc4d1	český
popis	popis	k1gInSc4	popis
NTFS	NTFS	kA	NTFS
http://www.osnews.com/story/24076/NTFS_A_File_System_with_Integrity_and_Complexity	[url]	k1gMnSc2	http://www.osnews.com/story/24076/NTFS_A_File_System_with_Integrity_and_Complexity
–	–	k?	–
popis	popis	k1gInSc1	popis
vlastností	vlastnost	k1gFnPc2	vlastnost
NTFS	NTFS	kA	NTFS
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
