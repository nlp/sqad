<p>
<s>
Orkut	Orkut	k1gInSc1	Orkut
byl	být	k5eAaImAgInS	být
komunitní	komunitní	k2eAgInSc1d1	komunitní
server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
Fungoval	fungovat	k5eAaImAgMnS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
socioware	sociowar	k1gMnSc5	sociowar
<g/>
.	.	kIx.	.
</s>
<s>
Sdružoval	sdružovat	k5eAaImAgMnS	sdružovat
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
společné	společný	k2eAgInPc4d1	společný
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
bydliště	bydliště	k1gNnPc4	bydliště
<g/>
,	,	kIx,	,
národnost	národnost	k1gFnSc4	národnost
<g/>
,	,	kIx,	,
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
anebo	anebo	k8xC	anebo
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
znají	znát	k5eAaImIp3nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Orkut	Orkut	k1gInSc1	Orkut
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Orkut	Orkut	k2eAgMnSc1d1	Orkut
Büyükkökten	Büyükkökten	k2eAgMnSc1d1	Büyükkökten
<g/>
,	,	kIx,	,
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gFnSc2	Google
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
provozovala	provozovat	k5eAaImAgFnS	provozovat
<g/>
.	.	kIx.	.
59	[number]	k4	59
%	%	kIx~	%
uživatelů	uživatel	k1gMnPc2	uživatel
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síť	síť	k1gFnSc1	síť
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zrušena	zrušen	k2eAgFnSc1d1	zrušena
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Google	Google	k1gFnSc1	Google
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Orkut	Orkut	k1gInSc4	Orkut
provozovala	provozovat	k5eAaImAgFnS	provozovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
věnovala	věnovat	k5eAaPmAgFnS	věnovat
provozu	provoz	k1gInSc3	provoz
komplexnější	komplexní	k2eAgFnSc2d2	komplexnější
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
Google	Google	k1gFnSc2	Google
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nejlepším	dobrý	k2eAgNnSc6d3	nejlepší
období	období	k1gNnSc6	období
měl	mít	k5eAaImAgInS	mít
Orkut	Orkut	k1gInSc1	Orkut
přes	přes	k7c4	přes
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
aktivních	aktivní	k2eAgMnPc2d1	aktivní
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
sociální	sociální	k2eAgFnSc7d1	sociální
sítí	síť	k1gFnSc7	síť
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
ho	on	k3xPp3gInSc4	on
ale	ale	k9	ale
i	i	k9	i
zde	zde	k6eAd1	zde
překonal	překonat	k5eAaPmAgMnS	překonat
Facebook	Facebook	k1gInSc4	Facebook
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sociální	sociální	k2eAgFnSc1d1	sociální
síť	síť	k1gFnSc1	síť
==	==	k?	==
</s>
</p>
<p>
<s>
Orkut	Orkut	k1gInSc1	Orkut
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
budování	budování	k1gNnSc4	budování
tzv.	tzv.	kA	tzv.
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
mohou	moct	k5eAaImIp3nP	moct
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
a	a	k8xC	a
prohlížet	prohlížet	k5eAaImF	prohlížet
si	se	k3xPyFc3	se
stránky	stránka	k1gFnPc4	stránka
jejich	jejich	k3xOp3gInPc2	jejich
známých	známý	k2eAgInPc2d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Připojením	připojení	k1gNnSc7	připojení
ke	k	k7c3	k
komunitě	komunita	k1gFnSc3	komunita
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
další	další	k2eAgMnPc4d1	další
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgInPc4d1	stejný
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
koníčky	koníček	k1gInPc4	koníček
<g/>
,	,	kIx,	,
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
názory	názor	k1gInPc1	názor
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Slabiny	slabina	k1gFnSc2	slabina
Orkutu	Orkut	k1gInSc2	Orkut
==	==	k?	==
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vstup	vstup	k1gInSc4	vstup
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
pozvánku	pozvánka	k1gFnSc4	pozvánka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Orkut	Orkut	k1gMnSc1	Orkut
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
technickými	technický	k2eAgInPc7d1	technický
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
trpěl	trpět	k5eAaImAgMnS	trpět
častými	častý	k2eAgInPc7d1	častý
výpadky	výpadek	k1gInPc7	výpadek
a	a	k8xC	a
nedostupností	nedostupnost	k1gFnSc7	nedostupnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgMnPc4	některý
uživatele	uživatel	k1gMnPc4	uživatel
takové	takový	k3xDgNnSc4	takový
problémy	problém	k1gInPc1	problém
odradily	odradit	k5eAaPmAgInP	odradit
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
registraci	registrace	k1gFnSc4	registrace
zrušili	zrušit	k5eAaPmAgMnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Čechů	Čech	k1gMnPc2	Čech
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
na	na	k7c6	na
Orkutu	Orkut	k1gInSc6	Orkut
pouze	pouze	k6eAd1	pouze
pasivně	pasivně	k6eAd1	pasivně
a	a	k8xC	a
nijak	nijak	k6eAd1	nijak
zvláště	zvláště	k6eAd1	zvláště
se	se	k3xPyFc4	se
neprojevovalo	projevovat	k5eNaImAgNnS	projevovat
<g/>
.	.	kIx.	.
</s>
<s>
Zaznívaly	zaznívat	k5eAaImAgFnP	zaznívat
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
bublina	bublina	k1gFnSc1	bublina
Orkutu	Orkut	k1gInSc2	Orkut
<g/>
"	"	kIx"	"
splaskla	splasknout	k5eAaPmAgFnS	splasknout
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Orkut	Orkut	k1gInSc1	Orkut
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Orkut	Orkut	k1gInSc1	Orkut
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Orkut	Orkut	k1gInSc1	Orkut
<g/>
,	,	kIx,	,
pozvánky	pozvánka	k1gFnPc1	pozvánka
a	a	k8xC	a
budoucnost	budoucnost	k1gFnSc1	budoucnost
SocioWare	SocioWar	k1gMnSc5	SocioWar
(	(	kIx(	(
<g/>
článek	článek	k1gInSc1	článek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Orkut	Orkut	k1gMnSc1	Orkut
se	se	k3xPyFc4	se
známe	znát	k5eAaImIp1nP	znát
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
článek	článek	k1gInSc1	článek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Orálkut	Orálkut	k1gInSc1	Orálkut
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
–	–	k?	–
nová	nový	k2eAgFnSc1d1	nová
internetová	internetový	k2eAgFnSc1d1	internetová
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
vtipná	vtipný	k2eAgFnSc1d1	vtipná
parodie	parodie	k1gFnSc1	parodie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Orkut	Orkut	k1gInSc1	Orkut
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
aneb	aneb	k?	aneb
něco	něco	k3yInSc1	něco
o	o	k7c6	o
SocioWare	SocioWar	k1gMnSc5	SocioWar
(	(	kIx(	(
<g/>
článek	článek	k1gInSc4	článek
v	v	k7c6	v
blogu	blog	k1gInSc6	blog
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Indie	Indie	k1gFnSc1	Indie
se	se	k3xPyFc4	se
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
s	s	k7c7	s
Googlem	Googl	k1gInSc7	Googl
na	na	k7c6	na
cenzuře	cenzura	k1gFnSc6	cenzura
Orkutu	Orkut	k1gInSc2	Orkut
</s>
</p>
