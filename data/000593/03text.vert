<s>
Tallahassee	Tallahasseat	k5eAaPmIp3nS	Tallahasseat
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
[	[	kIx(	[
<g/>
telehesí	telehese	k1gFnPc2	telehese
<g/>
]	]	kIx)	]
<g/>
;	;	kIx,	;
[	[	kIx(	[
<g/>
l	l	kA	l
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Floridy	Florida	k1gFnSc2	Florida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
171	[number]	k4	171
922	[number]	k4	922
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
aglomerace	aglomerace	k1gFnSc2	aglomerace
přibližně	přibližně	k6eAd1	přibližně
357	[number]	k4	357
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
Leon	Leona	k1gFnPc2	Leona
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tallahassee	Tallahassee	k1gFnSc6	Tallahassee
sídlí	sídlet	k5eAaImIp3nS	sídlet
Univerzita	univerzita	k1gFnSc1	univerzita
státu	stát	k1gInSc2	stát
Florida	Florida	k1gFnSc1	Florida
a	a	k8xC	a
několik	několik	k4yIc4	několik
méně	málo	k6eAd2	málo
významných	významný	k2eAgFnPc2d1	významná
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
regionální	regionální	k2eAgNnSc4d1	regionální
obchodní	obchodní	k2eAgNnSc4d1	obchodní
a	a	k8xC	a
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
provozuje	provozovat	k5eAaImIp3nS	provozovat
Tallahassee	Tallahassee	k1gFnPc7	Tallahassee
Regional	Regional	k1gFnSc1	Regional
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
Tallahasseeské	Tallahasseeský	k2eAgNnSc1d1	Tallahasseeský
Regionální	regionální	k2eAgNnSc1d1	regionální
letiště	letiště	k1gNnSc1	letiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Tallahassee	Tallahasse	k1gFnSc2	Tallahasse
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
Muskogeanského	Muskogeanský	k2eAgNnSc2d1	Muskogeanský
indiánského	indiánský	k2eAgNnSc2d1	indiánské
slova	slovo	k1gNnSc2	slovo
často	často	k6eAd1	často
překládaného	překládaný	k2eAgNnSc2d1	překládané
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
stará	starý	k2eAgNnPc1d1	staré
pole	pole	k1gNnPc1	pole
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
staré	starý	k2eAgNnSc1d1	staré
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
to	ten	k3xDgNnSc1	ten
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Kríků	Krík	k1gInPc2	Krík
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Seminolové	Seminolový	k2eAgNnSc1d1	Seminolový
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
Indiánů	Indián	k1gMnPc2	Indián
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
přicestovali	přicestovat	k5eAaPmAgMnP	přicestovat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tallahassee	Tallahassee	k1gFnSc1	Tallahassee
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
nejrychleji	rychle	k6eAd3	rychle
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
metropole	metropol	k1gFnSc2	metropol
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
12,4	[number]	k4	12,4
<g/>
%	%	kIx~	%
přírůstkem	přírůstek	k1gInSc7	přírůstek
za	za	k7c4	za
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
Miami	Miami	k1gNnSc1	Miami
i	i	k8xC	i
Tampa	Tampa	k1gFnSc1	Tampa
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
přívrženci	přívrženec	k1gMnPc1	přívrženec
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
tradičně	tradičně	k6eAd1	tradičně
konzervativním	konzervativní	k2eAgInSc6d1	konzervativní
severu	sever	k1gInSc6	sever
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
181	[number]	k4	181
376	[number]	k4	376
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
57,4	[number]	k4	57,4
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
35,0	[number]	k4	35,0
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,7	[number]	k4	3,7
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,3	[number]	k4	1,3
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,3	[number]	k4	2,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
6,3	[number]	k4	6,3
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Krasnodar	Krasnodar	k1gInSc1	Krasnodar
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc4	Rusko
Konongo-Odumase	Konongo-Odumasa	k1gFnSc3	Konongo-Odumasa
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
Sint	Sint	k1gMnSc1	Sint
Maarten	Maarten	k2eAgMnSc1d1	Maarten
<g/>
,	,	kIx,	,
Nizozemské	nizozemský	k2eAgFnPc1d1	nizozemská
Antily	Antily	k1gFnPc1	Antily
Sligo	Sligo	k1gNnSc1	Sligo
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc1	hrabství
Sligo	Sligo	k1gNnSc1	Sligo
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
Ramat	Ramat	k2eAgMnSc1d1	Ramat
ha-Šaron	ha-Šaron	k1gMnSc1	ha-Šaron
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gMnSc1	Izrael
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tallahassee	Tallahasse	k1gFnSc2	Tallahasse
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
