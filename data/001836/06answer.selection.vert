<s>
Ashmolean	Ashmolean	k1gMnSc1	Ashmolean
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
and	and	k?	and
Archaeology	Archaeolog	k1gMnPc7	Archaeolog
<g/>
,	,	kIx,	,
Beaumont	Beaumont	k1gMnSc1	Beaumont
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Ashmoleovo	Ashmoleův	k2eAgNnSc1d1	Ashmoleův
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
prvním	první	k4xOgMnSc7	první
univerzitním	univerzitní	k2eAgMnSc7d1	univerzitní
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
.	.	kIx.	.
</s>
