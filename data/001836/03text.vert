<s>
Ashmolean	Ashmolean	k1gMnSc1	Ashmolean
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
and	and	k?	and
Archaeology	Archaeolog	k1gMnPc7	Archaeolog
<g/>
,	,	kIx,	,
Beaumont	Beaumont	k1gMnSc1	Beaumont
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Ashmoleovo	Ashmoleův	k2eAgNnSc1d1	Ashmoleův
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
prvním	první	k4xOgMnSc7	první
univerzitním	univerzitní	k2eAgMnSc7d1	univerzitní
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
Oxfordu	Oxford	k1gInSc6	Oxford
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1678	[number]	k4	1678
až	až	k9	až
1683	[number]	k4	1683
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vznik	vznik	k1gInSc1	vznik
bývá	bývat	k5eAaImIp3nS	bývat
dáván	dávat	k5eAaImNgInS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
sira	sir	k1gMnSc2	sir
Christophera	Christopher	k1gMnSc2	Christopher
Wrena	Wren	k1gMnSc2	Wren
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
-	-	kIx~	-
<g/>
1723	[number]	k4	1723
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britského	britský	k2eAgMnSc2d1	britský
matematika	matematik	k1gMnSc2	matematik
<g/>
,	,	kIx,	,
fyzika	fyzik	k1gMnSc2	fyzik
<g/>
,	,	kIx,	,
geometra	geometr	k1gMnSc2	geometr
a	a	k8xC	a
astronoma	astronom	k1gMnSc2	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
od	od	k7c2	od
dalšího	další	k2eAgMnSc2d1	další
britského	britský	k2eAgMnSc2d1	britský
vědce	vědec	k1gMnSc2	vědec
<g/>
,	,	kIx,	,
astrologa	astrolog	k1gMnSc2	astrolog
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
Eliase	Eliasa	k1gFnSc3	Eliasa
Ashmolea	Ashmole	k1gInSc2	Ashmole
(	(	kIx(	(
<g/>
1617	[number]	k4	1617
<g/>
-	-	kIx~	-
<g/>
1692	[number]	k4	1692
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
původnímu	původní	k2eAgNnSc3d1	původní
ústavu	ústav	k1gInSc2	ústav
věnoval	věnovat	k5eAaPmAgMnS	věnovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1677	[number]	k4	1677
své	svůj	k3xOyFgFnSc2	svůj
sbírky	sbírka	k1gFnSc2	sbírka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
kabinetu	kabinet	k1gInSc2	kabinet
kuriozit	kuriozita	k1gFnPc2	kuriozita
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006-2009	[number]	k4	2006-2009
celkovou	celkový	k2eAgFnSc7d1	celková
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
také	také	k6eAd1	také
jako	jako	k8xS	jako
první	první	k4xOgFnPc4	první
instituce	instituce	k1gFnPc4	instituce
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
sbírkách	sbírka	k1gFnPc6	sbírka
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
prokázaná	prokázaný	k2eAgFnSc1d1	prokázaná
fosílie	fosílie	k1gFnSc1	fosílie
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
(	(	kIx(	(
<g/>
kus	kus	k1gInSc1	kus
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
megalosaura	megalosaura	k1gFnSc1	megalosaura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zobrazil	zobrazit	k5eAaPmAgMnS	zobrazit
první	první	k4xOgMnSc1	první
představený	představený	k1gMnSc1	představený
muzea	muzeum	k1gNnSc2	muzeum
Robert	Robert	k1gMnSc1	Robert
Plot	plot	k1gInSc1	plot
(	(	kIx(	(
<g/>
1640	[number]	k4	1640
<g/>
-	-	kIx~	-
<g/>
1696	[number]	k4	1696
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgFnPc1d1	archeologická
sbírky	sbírka	k1gFnPc1	sbírka
mapují	mapovat	k5eAaImIp3nP	mapovat
historii	historie	k1gFnSc4	historie
lidského	lidský	k2eAgNnSc2d1	lidské
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Nilu	Nil	k1gInSc2	Nil
(	(	kIx(	(
<g/>
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Núbie	Núbie	k1gFnSc2	Núbie
<g/>
)	)	kIx)	)
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
5	[number]	k4	5
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
šesti	šest	k4xCc6	šest
sálech	sál	k1gInPc6	sál
druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
staroegyptského	staroegyptský	k2eAgNnSc2d1	staroegyptské
umění	umění	k1gNnSc2	umění
po	po	k7c4	po
British	British	k1gInSc4	British
Museum	museum	k1gNnSc1	museum
<g/>
.	.	kIx.	.
</s>
<s>
Archeolog	archeolog	k1gMnSc1	archeolog
Arthur	Arthur	k1gMnSc1	Arthur
Evans	Evans	k1gInSc4	Evans
věnoval	věnovat	k5eAaImAgMnS	věnovat
muzeu	muzeum	k1gNnSc6	muzeum
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
sbírku	sbírka	k1gFnSc4	sbírka
mínojského	mínojský	k2eAgNnSc2d1	mínojské
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pokladům	poklad	k1gInPc3	poklad
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
patří	patřit	k5eAaImIp3nS	patřit
olejomalba	olejomalba	k1gFnSc1	olejomalba
Paola	Paola	k1gFnSc1	Paola
Uccella	Uccella	k1gFnSc1	Uccella
Lov	lov	k1gInSc1	lov
v	v	k7c6	v
lese	les	k1gInSc6	les
(	(	kIx(	(
<g/>
1465	[number]	k4	1465
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnSc2	kresba
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
,	,	kIx,	,
Raffaela	Raffael	k1gMnSc2	Raffael
<g/>
,	,	kIx,	,
Michelangela	Michelangel	k1gMnSc2	Michelangel
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
Williama	William	k1gMnSc2	William
Turnera	turner	k1gMnSc2	turner
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc2	John
Constablea	Constableus	k1gMnSc2	Constableus
<g/>
,	,	kIx,	,
Pabla	Pabl	k1gMnSc2	Pabl
Picassa	Picass	k1gMnSc2	Picass
nebo	nebo	k8xC	nebo
plastiky	plastika	k1gFnPc1	plastika
Barbary	Barbara	k1gFnSc2	Barbara
Hepworth	Hepwortha	k1gFnPc2	Hepwortha
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgFnPc1d1	britská
galerie	galerie	k1gFnPc1	galerie
a	a	k8xC	a
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Beránek	Beránek	k1gMnSc1	Beránek
<g/>
,	,	kIx,	,
Radioservis	Radioservis	k1gFnSc1	Radioservis
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87530-09-2	[number]	k4	978-80-87530-09-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ashmolean	Ashmoleana	k1gFnPc2	Ashmoleana
Museum	museum	k1gNnSc4	museum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Web	web	k1gInSc1	web
muzea	muzeum	k1gNnSc2	muzeum
Ashmolean	Ashmoleany	k1gInPc2	Ashmoleany
videos	videosa	k1gFnPc2	videosa
</s>
