<s>
Sokratovská	sokratovský	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
</s>
<s>
Sokratovská	sokratovský	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
je	být	k5eAaImIp3nS
výchovný	výchovný	k2eAgInSc4d1
a	a	k8xC
vzdělávací	vzdělávací	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
učitel	učitel	k1gMnSc1
snaží	snažit	k5eAaImIp3nS
své	svůj	k3xOyFgMnPc4
studenty	student	k1gMnPc4
otázkami	otázka	k1gFnPc7
přivést	přivést	k5eAaPmF
k	k	k7c3
novému	nový	k2eAgNnSc3d1
poznání	poznání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odvolává	odvolávat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
antického	antický	k2eAgMnSc4d1
filosofa	filosof	k1gMnSc4
Sókrata	Sókrat	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
v	v	k7c6
některých	některý	k3yIgNnPc6
svých	svůj	k3xOyFgInPc6
dialozích	dialog	k1gInPc6
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
poznání	poznání	k1gNnSc1
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
rozpomínání	rozpomínání	k1gNnSc1
a	a	k8xC
svoji	svůj	k3xOyFgFnSc4
úlohu	úloha	k1gFnSc4
při	při	k7c6
tom	ten	k3xDgNnSc6
přirovnává	přirovnávat	k5eAaImIp3nS
k	k	k7c3
činnosti	činnost	k1gFnSc3
porodní	porodní	k2eAgFnSc2d1
báby	bába	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Užívá	užívat	k5eAaImIp3nS
se	se	k3xPyFc4
jak	jak	k6eAd1
na	na	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
také	také	k9
v	v	k7c6
profesním	profesní	k2eAgInSc6d1
a	a	k8xC
firemním	firemní	k2eAgInSc6d1
tréninku	trénink	k1gInSc6
kritického	kritický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
a	a	k8xC
logické	logický	k2eAgFnSc2d1
argumentace	argumentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
hlavní	hlavní	k2eAgFnSc4d1
přednost	přednost	k1gFnSc4
metody	metoda	k1gFnSc2
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
forma	forma	k1gFnSc1
zapojení	zapojení	k1gNnSc2
studentů	student	k1gMnPc2
/	/	kIx~
účastníků	účastník	k1gMnPc2
do	do	k7c2
procesu	proces	k1gInSc2
poznávání	poznávání	k1gNnSc2
a	a	k8xC
zvýšená	zvýšený	k2eAgFnSc1d1
motivace	motivace	k1gFnSc1
přijít	přijít	k5eAaPmF
věci	věc	k1gFnSc3
„	„	kIx"
<g/>
na	na	k7c4
kloub	kloub	k1gInSc4
<g/>
“	“	kIx"
oproti	oproti	k7c3
pouhému	pouhý	k2eAgInSc3d1
výkladu	výklad	k1gInSc3
učitele	učitel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Sókratovy	Sókratův	k2eAgInPc1d1
dialogy	dialog	k1gInPc1
</s>
<s>
Modrý	modrý	k2eAgInSc1d1
čtverec	čtverec	k1gInSc1
má	mít	k5eAaImIp3nS
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
než	než	k8xS
původní	původní	k2eAgInSc4d1
žlutý	žlutý	k2eAgInSc4d1
</s>
<s>
V	v	k7c6
řadě	řada	k1gFnSc6
raných	raný	k2eAgInPc2d1
dialogů	dialog	k1gInPc2
hovoří	hovořit	k5eAaImIp3nS
Sókratés	Sókratés	k1gInSc1
s	s	k7c7
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
pevně	pevně	k6eAd1
přesvědčeni	přesvědčit	k5eAaPmNgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
odborníci	odborník	k1gMnPc1
a	a	k8xC
že	že	k8xS
něco	něco	k3yInSc4
spolehlivě	spolehlivě	k6eAd1
vědí	vědět	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sókratés	Sókratés	k1gInSc1
svými	svůj	k3xOyFgFnPc7
otázkami	otázka	k1gFnPc7
nejprve	nejprve	k6eAd1
zpochybní	zpochybnit	k5eAaPmIp3nP
jejich	jejich	k3xOp3gNnSc4
domnělé	domnělý	k2eAgNnSc4d1
vědění	vědění	k1gNnSc4
a	a	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
dovede	dovést	k5eAaPmIp3nS
k	k	k7c3
hlubšímu	hluboký	k2eAgNnSc3d2
<g/>
,	,	kIx,
pravdivějšímu	pravdivý	k2eAgNnSc3d2
porozumění	porozumění	k1gNnSc3
a	a	k8xC
náhledu	náhled	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
Platónovy	Platónův	k2eAgInPc1d1
dialogy	dialog	k1gInPc1
Euthydémos	Euthydémosa	k1gFnPc2
<g/>
,	,	kIx,
Ion	ion	k1gInSc1
a	a	k8xC
zejména	zejména	k9
Menón	Menón	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
dialogu	dialog	k1gInSc6
pozve	pozvat	k5eAaPmIp3nS
Menón	Menón	k1gInSc1
k	k	k7c3
rozhovoru	rozhovor	k1gInSc3
prvního	první	k4xOgMnSc2
otroka	otrok	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
jde	jít	k5eAaImIp3nS
kolem	kolem	k6eAd1
a	a	k8xC
u	u	k7c2
něhož	jenž	k3xRgNnSc2
je	být	k5eAaImIp3nS
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c6
geometrii	geometrie	k1gFnSc6
nic	nic	k6eAd1
neslyšel	slyšet	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sókratés	Sókratés	k1gInSc1
mu	on	k3xPp3gMnSc3
předloží	předložit	k5eAaPmIp3nS
obtížnou	obtížný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
<g/>
:	:	kIx,
ke	k	k7c3
čtverci	čtverec	k1gInSc3
o	o	k7c6
straně	strana	k1gFnSc6
dvě	dva	k4xCgFnPc4
stopy	stopa	k1gFnPc4
má	mít	k5eAaImIp3nS
zkonstruovat	zkonstruovat	k5eAaPmF
čtverec	čtverec	k1gInSc4
s	s	k7c7
dvojnásobnou	dvojnásobný	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otrok	otrok	k1gMnSc1
nejprve	nejprve	k6eAd1
myslí	myslet	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
bude	být	k5eAaImBp3nS
čtverec	čtverec	k1gInSc1
s	s	k7c7
dvojnásobně	dvojnásobně	k6eAd1
dlouhou	dlouhý	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
když	když	k8xS
jej	on	k3xPp3gMnSc4
zkonstruuje	zkonstruovat	k5eAaPmIp3nS
<g/>
,	,	kIx,
zjistí	zjistit	k5eAaPmIp3nS
že	že	k8xS
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sókratés	Sókratés	k1gInSc1
ho	on	k3xPp3gInSc4
pak	pak	k6eAd1
otázkami	otázka	k1gFnPc7
dovede	dovést	k5eAaPmIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
čtyři	čtyři	k4xCgFnPc1
části	část	k1gFnPc1
čtverce	čtverec	k1gInSc2
úhlopříčkami	úhlopříčka	k1gFnPc7
rozpůlil	rozpůlit	k5eAaPmAgInS
<g/>
,	,	kIx,
takže	takže	k8xS
vznikne	vzniknout	k5eAaPmIp3nS
skutečně	skutečně	k6eAd1
čtverec	čtverec	k1gInSc1
o	o	k7c6
dvojnásobné	dvojnásobný	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
<g/>
,	,	kIx,
vůči	vůči	k7c3
původnímu	původní	k2eAgNnSc3d1
postavený	postavený	k2eAgInSc4d1
na	na	k7c4
roh	roh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sókratés	Sókratés	k1gInSc1
v	v	k7c6
tom	ten	k3xDgNnSc6
vidí	vidět	k5eAaImIp3nS
zřejmé	zřejmý	k2eAgNnSc1d1
potvrzení	potvrzení	k1gNnSc1
své	svůj	k3xOyFgFnSc2
domněnky	domněnka	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
otrok	otrok	k1gMnSc1
„	„	k?
<g/>
rozpomenul	rozpomenout	k5eAaPmAgMnS
<g/>
“	“	k?
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
jeho	jeho	k3xOp3gFnSc1
nesmrtelná	smrtelný	k2eNgFnSc1d1
duše	duše	k1gFnSc1
už	už	k6eAd1
věděla	vědět	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Protože	protože	k8xS
totiž	totiž	k9
je	být	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
příroda	příroda	k1gFnSc1
stejnorodá	stejnorodý	k2eAgFnSc1d1
a	a	k8xC
duše	duše	k1gFnSc1
poznala	poznat	k5eAaPmAgFnS
všechny	všechen	k3xTgFnPc4
věci	věc	k1gFnPc4
<g/>
,	,	kIx,
stačí	stačit	k5eAaBmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
si	se	k3xPyFc3
člověk	člověk	k1gMnSc1
vzpomene	vzpomenout	k5eAaPmIp3nS
toliko	toliko	k6eAd1
na	na	k7c6
jednu	jeden	k4xCgFnSc4
–	–	k?
což	což	k3yQnSc4,k3yRnSc4
lidé	člověk	k1gMnPc1
nazývají	nazývat	k5eAaImIp3nP
učením	učení	k1gNnSc7
–	–	k?
aby	aby	kYmCp3nP
všechny	všechen	k3xTgMnPc4
ostatní	ostatní	k2eAgMnPc4d1
nalezl	nalézt	k5eAaBmAgMnS,k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
statečný	statečný	k2eAgMnSc1d1
a	a	k8xC
neúnavný	únavný	k2eNgMnSc1d1
v	v	k7c6
hledání	hledání	k1gNnSc6
<g/>
;	;	kIx,
neboť	neboť	k8xC
hledání	hledání	k1gNnSc3
a	a	k8xC
poznávání	poznávání	k1gNnSc3
je	být	k5eAaImIp3nS
vůbec	vůbec	k9
vzpomínání	vzpomínání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Platón	Platón	k1gMnSc1
<g/>
,	,	kIx,
Menón	Menón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
81	#num#	k4
<g/>
d.	d.	k?
</s>
<s>
V	v	k7c6
jiných	jiný	k2eAgInPc6d1
dialozích	dialog	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c6
geometrii	geometrie	k1gFnSc6
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
Sókratova	Sókratův	k2eAgFnSc1d1
argumentace	argumentace	k1gFnSc1
tak	tak	k6eAd1
přesvědčivá	přesvědčivý	k2eAgFnSc1d1
a	a	k8xC
moderní	moderní	k2eAgMnSc1d1
čtenář	čtenář	k1gMnSc1
má	mít	k5eAaImIp3nS
dojem	dojem	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
nutí	nutit	k5eAaImIp3nP
své	svůj	k3xOyFgMnPc4
partnery	partner	k1gMnPc4
volit	volit	k5eAaImF
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
alternativami	alternativa	k1gFnPc7
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
více	hodně	k6eAd2
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
alternativy	alternativa	k1gFnPc1
nevylučují	vylučovat	k5eNaImIp3nP
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Moderní	moderní	k2eAgInPc1d1
sokratovské	sokratovský	k2eAgInPc1d1
rozhovory	rozhovor	k1gInPc1
</s>
<s>
Metoda	metoda	k1gFnSc1
v	v	k7c6
tradici	tradice	k1gFnSc6
Nelson	Nelson	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Heckmann	Heckmann	k1gMnSc1
</s>
<s>
Historie	historie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základy	základ	k1gInPc1
moderní	moderní	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
</s>
<s>
sokratovského	sokratovský	k2eAgInSc2d1
rozhovoru	rozhovor	k1gInSc2
položil	položit	k5eAaPmAgMnS
göttingenský	göttingenský	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
filosofie	filosofie	k1gFnSc2
Leonard	Leonard	k1gMnSc1
Nelson	Nelson	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1882	#num#	k4
<g/>
-	-	kIx~
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Minnou	Minna	k1gFnSc7
Specht	Spechta	k1gFnPc2
založil	založit	k5eAaPmAgMnS
školu	škola	k1gFnSc4
ve	v	k7c6
Walkenmühle	Walkenmühle	k1gFnSc6
u	u	k7c2
</s>
<s>
města	město	k1gNnSc2
Melsungen	Melsungen	k1gInSc1
v	v	k7c6
Hesensku	Hesensko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byli	být	k5eAaImAgMnP
v	v	k7c6
duchu	duch	k1gMnSc6
této	tento	k3xDgFnSc6
metody	metoda	k1gFnPc4
vyučováni	vyučován	k2eAgMnPc1d1
žáci	žák	k1gMnPc1
ve	v	k7c6
věku	věk	k1gInSc6
</s>
<s>
12-20	12-20	k4
let	léto	k1gNnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
politického	politický	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
Nelsonovi	Nelson	k1gMnSc3
žáky	žák	k1gMnPc7
a	a	k8xC
</s>
<s>
spolupracovníky	spolupracovník	k1gMnPc4
patřil	patřit	k5eAaImAgMnS
také	také	k6eAd1
vystudovaný	vystudovaný	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
a	a	k8xC
matematik	matematik	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Heckmann	Heckmann	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
1898	#num#	k4
<g/>
-	-	kIx~
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
věnovat	věnovat	k5eAaPmF,k5eAaImF
oproti	oproti	k7c3
očekávané	očekávaný	k2eAgFnSc3d1
profesi	profes	k1gFnSc3
technické	technický	k2eAgFnSc2d1
</s>
<s>
dráze	dráha	k1gFnSc3
pedagogické	pedagogický	k2eAgFnSc3d1
a	a	k8xC
začal	začít	k5eAaPmAgInS
vyučovat	vyučovat	k5eAaImF
na	na	k7c6
Walkenmühle	Walkenmühle	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
rozvíjení	rozvíjení	k1gNnSc4
metody	metoda	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
</s>
<s>
výuce	výuka	k1gFnSc3
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Škola	škola	k1gFnSc1
ve	v	k7c6
Walkenmühle	Walkenmühle	k1gFnSc6
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
</s>
<s>
a	a	k8xC
Heckmann	Heckmann	k1gMnSc1
spolu	spolu	k6eAd1
s	s	k7c7
Minnou	Minna	k1gFnSc7
Specht	Specht	k1gInSc4
imigrovali	imigrovat	k5eAaBmAgMnP
do	do	k7c2
Dánska	Dánsko	k1gNnSc2
a	a	k8xC
posléze	posléze	k6eAd1
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
</s>
<s>
tomu	ten	k3xDgMnSc3
se	se	k3xPyFc4
sokratovský	sokratovský	k2eAgInSc4d1
rozhovor	rozhovor	k1gInSc4
uchytil	uchytit	k5eAaPmAgMnS
také	také	k9
v	v	k7c6
těchto	tento	k3xDgFnPc6
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
ztratila	ztratit	k5eAaPmAgFnS
sokratovská	sokratovský	k2eAgFnSc1d1
</s>
<s>
metoda	metoda	k1gFnSc1
svůj	svůj	k3xOyFgInSc4
silně	silně	k6eAd1
politicky	politicky	k6eAd1
zaměřený	zaměřený	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
prosazoval	prosazovat	k5eAaImAgMnS
Nelson	Nelson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byla	být	k5eAaImAgFnS
Philosophish-Politische	Philosophish-Politische	k1gFnSc1
Akademie	akademie	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
PPA	PPA	kA
<g/>
)	)	kIx)
obnovena	obnoven	k2eAgFnSc1d1
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
tomu	ten	k3xDgNnSc3
již	již	k6eAd1
ve	v	k7c6
spojitosti	spojitost	k1gFnSc6
z	z	k7c2
žádnou	žádný	k3yNgFnSc7
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heckman	Heckman	k1gMnSc1
</s>
<s>
působil	působit	k5eAaImAgMnS
po	po	k7c6
svém	svůj	k3xOyFgInSc6
návratu	návrat	k1gInSc6
do	do	k7c2
vlasti	vlast	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
jako	jako	k8xS,k8xC
pedagog	pedagog	k1gMnSc1
v	v	k7c6
Hannoveru	Hannover	k1gInSc6
<g/>
,	,	kIx,
</s>
<s>
reformoval	reformovat	k5eAaBmAgInS
tradici	tradice	k1gFnSc4
sokratovských	sokratovský	k2eAgMnPc2d1
</s>
<s>
rozhovorů	rozhovor	k1gInPc2
a	a	k8xC
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
spolu	spolu	k6eAd1
s	s	k7c7
Ernou	Erna	k1gFnSc7
Benckle	Benckl	k1gInSc5
a	a	k8xC
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
Charlotte	Charlott	k1gMnSc5
Heckmann	Heckmanno	k1gNnPc2
</s>
<s>
obnovili	obnovit	k5eAaPmAgMnP
sokratovské	sokratovský	k2eAgInPc4d1
semináře	seminář	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
se	se	k3xPyFc4
tentokrát	tentokrát	k6eAd1
konaly	konat	k5eAaImAgFnP
především	především	k9
na	na	k7c6
zámku	zámek	k1gInSc6
Schwöbber	Schwöbbra	k1gFnPc2
u	u	k7c2
Hameln	Hamelna	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
byli	být	k5eAaImAgMnP
v	v	k7c6
praxi	praxe	k1gFnSc6
této	tento	k3xDgFnSc2
metody	metoda	k1gFnSc2
vzdělávání	vzdělávání	k1gNnSc6
další	další	k2eAgMnPc1d1
lektoři	lektor	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
</s>
<s>
lektoři	lektor	k1gMnPc1
sdružují	sdružovat	k5eAaImIp3nP
v	v	k7c6
organizaci	organizace	k1gFnSc6
Gesellschaft	Gesellschafta	k1gFnPc2
</s>
<s>
für	für	k?
Sokratisches	Sokratisches	k1gInSc1
Philosophieren	Philosophierna	k1gFnPc2
<g/>
,	,	kIx,
pořádají	pořádat	k5eAaImIp3nP
</s>
<s>
odborná	odborný	k2eAgNnPc4d1
symposia	symposion	k1gNnPc4
a	a	k8xC
podporují	podporovat	k5eAaImIp3nP
vydávání	vydávání	k1gNnSc4
sokratovské	sokratovský	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
GSP	GSP	kA
má	mít	k5eAaImIp3nS
sídlo	sídlo	k1gNnSc4
v	v	k7c6
</s>
<s>
Hannoveru	Hannover	k1gInSc2
a	a	k8xC
úzce	úzko	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
PPA	PPA	kA
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Bonnu	Bonn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
organizace	organizace	k1gFnSc2
</s>
<s>
podporuje	podporovat	k5eAaImIp3nS
tradici	tradice	k1gFnSc4
kritického	kritický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
vázanou	vázaný	k2eAgFnSc4d1
na	na	k7c4
filosofy	filosof	k1gMnPc4
Kanta	Kant	k1gMnSc2
<g/>
,	,	kIx,
Friese	Friese	k1gFnSc2
a	a	k8xC
</s>
<s>
Nelsona	Nelson	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
</s>
<s>
úrovni	úroveň	k1gFnSc3
spolupracuje	spolupracovat	k5eAaImIp3nS
PPA	PPA	kA
také	také	k6eAd1
s	s	k7c7
anglickou	anglický	k2eAgFnSc7d1
"	"	kIx"
<g/>
The	The	k1gFnSc7
</s>
<s>
Society	societa	k1gFnPc1
for	forum	k1gNnPc2
the	the	k?
Furtherance	Furtherance	k1gFnSc2
of	of	k?
the	the	k?
Critical	Critical	k1gMnSc2
Philosophy	Philosopha	k1gMnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
SFCP	SFCP	kA
<g/>
)	)	kIx)
a	a	k8xC
společně	společně	k6eAd1
pořádají	pořádat	k5eAaImIp3nP
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sokratovský	sokratovský	k2eAgInSc1d1
</s>
<s>
rozhovor	rozhovor	k1gInSc1
není	být	k5eNaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
spojován	spojován	k2eAgMnSc1d1
jen	jen	k6eAd1
s	s	k7c7
filosofií	filosofie	k1gFnSc7
<g/>
,	,	kIx,
účastní	účastnit	k5eAaImIp3nS
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
také	také	k9
</s>
<s>
například	například	k6eAd1
pedagogové	pedagog	k1gMnPc1
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
profese	profese	k1gFnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
týmovou	týmový	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
psychologové	psycholog	k1gMnPc1
atd.	atd.	kA
Proto	proto	k8xC
lze	lze	k6eAd1
zmínit	zmínit	k5eAaPmF
také	také	k9
například	například	k6eAd1
německou	německý	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
Pro	pro	k7c4
Argumentis	Argumentis	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
uplatnění	uplatnění	k1gNnSc1
sokratovských	sokratovský	k2eAgInPc2d1
principů	princip	k1gInPc2
a	a	k8xC
filosofického	filosofický	k2eAgInSc2d1
</s>
<s>
přístupu	přístup	k1gInSc3
v	v	k7c6
současném	současný	k2eAgInSc6d1
managementu	management	k1gInSc6
a	a	k8xC
řízení	řízení	k1gNnSc6
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Sokratici	sokratik	k1gMnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
</s>
<s>
Sokratovské	sokratovský	k2eAgInPc1d1
rozhovory	rozhovor	k1gInPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
dle	dle	k7c2
školy	škola	k1gFnSc2
Nelson	Nelson	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Heckmann	Heckmann	k1gMnSc1
tradici	tradice	k1gFnSc4
zhruba	zhruba	k6eAd1
od	od	k7c2
konce	konec	k1gInSc2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
občanské	občanský	k2eAgNnSc4d1
sdružení	sdružení	k1gNnSc4
Sokratovské	sokratovský	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
pořádá	pořádat	k5eAaImIp3nS
Fakulta	fakulta	k1gFnSc1
humanitních	humanitní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
UK	UK	kA
spolu	spolu	k6eAd1
s	s	k7c7
německou	německý	k2eAgFnSc7d1
lektorskou	lektorský	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
GSP	GSP	kA
pravidelné	pravidelný	k2eAgInPc4d1
třídenní	třídenní	k2eAgInPc4d1
semináře	seminář	k1gInPc4
pro	pro	k7c4
studenty	student	k1gMnPc4
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
,	,	kIx,
angličtině	angličtina	k1gFnSc6
a	a	k8xC
němčině	němčina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
německou	německý	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
se	se	k3xPyFc4
během	během	k7c2
let	léto	k1gNnPc2
prohloubila	prohloubit	k5eAaPmAgFnS
a	a	k8xC
sdružení	sdružení	k1gNnPc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
podílí	podílet	k5eAaImIp3nS
na	na	k7c4
organizaci	organizace	k1gFnSc4
vícedenních	vícedenní	k2eAgInPc2d1
seminářů	seminář	k1gInPc2
několikrát	několikrát	k6eAd1
ročně	ročně	k6eAd1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
i	i	k8xC
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dialektika	dialektika	k1gFnSc1
</s>
<s>
Dialog	dialog	k1gInSc1
</s>
<s>
Platón	Platón	k1gMnSc1
</s>
<s>
Sókratés	Sókratés	k6eAd1
</s>
<s>
Leonard	Leonard	k1gMnSc1
Nelson	Nelson	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Podrobný	podrobný	k2eAgInSc4d1
záznam	záznam	k1gInSc4
„	„	k?
<g/>
sókratovské	sókratovský	k2eAgFnSc2d1
<g/>
“	“	k?
diskuse	diskuse	k1gFnSc2
ve	v	k7c6
třídě	třída	k1gFnSc6
</s>
<s>
Robinson	Robinson	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
,	,	kIx,
Plato	Plato	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Earlier	Earlier	k1gInSc1
Dialectic	Dialectice	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
nd	nd	k?
edition	edition	k1gInSc1
(	(	kIx(
<g/>
Clarendon	Clarendon	k1gInSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Ch	Ch	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
:	:	kIx,
Elenchus	Elenchus	k1gInSc1
<g/>
;	;	kIx,
</s>
<s>
Ch	Ch	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
:	:	kIx,
Elenchus	Elenchus	k1gMnSc1
<g/>
:	:	kIx,
Direct	Direct	k1gMnSc1
and	and	k?
Indirect	Indirect	k1gMnSc1
</s>
<s>
Socratic	Socratice	k1gFnPc2
Method	Methoda	k1gFnPc2
Research	Research	k1gInSc1
Portal	Portal	k1gMnSc1
</s>
<s>
Sokratovské	sokratovský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Web	web	k1gInSc1
</s>
<s>
T.	T.	kA
Maranhao	Maranhao	k6eAd1
<g/>
,	,	kIx,
Therapeutic	Therapeutice	k1gFnPc2
discourse	discourse	k1gFnSc2
and	and	k?
Socratic	Socratice	k1gFnPc2
dialogue	dialogue	k1gFnSc1
(	(	kIx(
<g/>
Google	Google	k1gNnSc1
Books	Booksa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Elenchus	Elenchus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
8	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
515	#num#	k4
</s>
<s>
Platón	Platón	k1gMnSc1
<g/>
,	,	kIx,
Euthydémos	Euthydémos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menón	Menón	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Oikúmené	Oikúmený	k2eAgNnSc1d1
1994	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Platón	Platón	k1gMnSc1
<g/>
,	,	kIx,
Menón	Menón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
.	.	kIx.
86	#num#	k4
<g/>
b	b	k?
↑	↑	k?
https://www.socraticdialogue.org/o-metode/	https://www.socraticdialogue.org/o-metode/	k?
Web	web	k1gInSc1
sokratovského	sokratovský	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://drive.google.com/file/d/1gR96oLIA0hjqQEJhJQ8oil-iDZcwOP09/view.	https://drive.google.com/file/d/1gR96oLIA0hjqQEJhJQ8oil-iDZcwOP09/view.	k4
Stanovy	stanova	k1gFnSc2
občanského	občanský	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
Sokratovské	sokratovský	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4470314-4	4470314-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85109823	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85109823	#num#	k4
</s>
