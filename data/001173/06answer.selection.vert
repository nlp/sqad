<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1346	[number]	k4	1346
<g/>
,	,	kIx,	,
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Kresčak	Kresčak	k1gInSc4	Kresčak
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Crécy-en-Ponthieu	Crécyn-Ponthie	k2eAgFnSc4d1	Crécy-en-Ponthie
<g/>
)	)	kIx)	)
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
