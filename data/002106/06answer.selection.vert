<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
23	[number]	k4	23
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
tradičně	tradičně	k6eAd1	tradičně
označovaných	označovaný	k2eAgFnPc2d1	označovaná
jako	jako	k8xS	jako
vídeňské	vídeňský	k2eAgInPc1d1	vídeňský
městské	městský	k2eAgInPc1d1	městský
okresy	okres	k1gInPc1	okres
(	(	kIx(	(
<g/>
Wiener	Wiener	k1gInSc1	Wiener
Stadtbezirke	Stadtbezirk	k1gInSc2	Stadtbezirk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
