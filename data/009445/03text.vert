<p>
<s>
Malíř	malíř	k1gMnSc1	malíř
pokojů	pokoj	k1gInPc2	pokoj
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnPc4d1	tradiční
stavební	stavební	k2eAgFnPc4d1	stavební
profese	profes	k1gFnPc4	profes
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
samotného	samotný	k2eAgNnSc2d1	samotné
malování	malování	k1gNnSc2	malování
pokojů	pokoj	k1gInPc2	pokoj
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgFnPc2d1	různá
technik	technika	k1gFnPc2	technika
ovládá	ovládat	k5eAaImIp3nS	ovládat
také	také	k9	také
drobné	drobný	k2eAgFnPc4d1	drobná
opravy	oprava	k1gFnPc4	oprava
omítky	omítka	k1gFnSc2	omítka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
stěrkování	stěrkování	k1gNnSc1	stěrkování
nebo	nebo	k8xC	nebo
štukování	štukování	k1gNnSc1	štukování
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lepení	lepení	k1gNnSc1	lepení
tapet	tapeta	k1gFnPc2	tapeta
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
případě	případ	k1gInSc6	případ
specializace	specializace	k1gFnSc2	specializace
na	na	k7c4	na
tapetování	tapetování	k1gNnSc4	tapetování
se	se	k3xPyFc4	se
profese	profes	k1gFnSc2	profes
nazývá	nazývat	k5eAaImIp3nS	nazývat
tapetář	tapetář	k1gMnSc1	tapetář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
malíře	malíř	k1gMnSc4	malíř
pokojů	pokoj	k1gInPc2	pokoj
Cech	cech	k1gInSc4	cech
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
lakýrníků	lakýrník	k1gMnPc2	lakýrník
a	a	k8xC	a
tapetářů	tapetář	k1gMnPc2	tapetář
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
zvyšování	zvyšování	k1gNnSc1	zvyšování
kvality	kvalita	k1gFnSc2	kvalita
vlastních	vlastní	k2eAgInPc2d1	vlastní
členů	člen	k1gInPc2	člen
i	i	k9	i
přípravě	příprava	k1gFnSc3	příprava
učňů	učeň	k1gMnPc2	učeň
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
školení	školení	k1gNnPc4	školení
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc4	výstava
i	i	k8xC	i
zájezdy	zájezd	k1gInPc4	zájezd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c6	o
profesi	profes	k1gFnSc6	profes
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
ubývá	ubývat	k5eAaImIp3nS	ubývat
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
nedostatek	nedostatek	k1gInSc4	nedostatek
učňů	učeň	k1gMnPc2	učeň
i	i	k8xC	i
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
malíř	malíř	k1gMnSc1	malíř
pokojů	pokoj	k1gInPc2	pokoj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
BRTKOVÁ	BRTKOVÁ	kA	BRTKOVÁ
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
malíři	malíř	k1gMnPc1	malíř
pokojů	pokoj	k1gInPc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
iDnes	iDnes	k1gMnSc1	iDnes
<g/>
,	,	kIx,	,
2015-10-24	[number]	k4	2015-10-24
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
