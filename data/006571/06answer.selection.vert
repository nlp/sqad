<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
hladina	hladina	k1gFnSc1	hladina
celkového	celkový	k2eAgInSc2d1	celkový
cholesterolu	cholesterol	k1gInSc2	cholesterol
(	(	kIx(	(
<g/>
cholesterolemie	cholesterolemie	k1gFnSc1	cholesterolemie
<g/>
)	)	kIx)	)
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
5,00	[number]	k4	5,00
mmol	mmola	k1gFnPc2	mmola
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
(	(	kIx(	(
<g/>
milimolů	milimol	k1gInPc2	milimol
na	na	k7c4	na
litr	litr	k1gInSc4	litr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
