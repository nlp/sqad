<p>
<s>
Silicon	Silicon	kA	Silicon
Valley	Valley	k1gInPc1	Valley
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Křemíkové	křemíkový	k2eAgNnSc4d1	křemíkové
údolí	údolí	k1gNnSc4	údolí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přezdívka	přezdívka	k1gFnSc1	přezdívka
nejjižnější	jižní	k2eAgFnSc2d3	nejjižnější
části	část	k1gFnSc2	část
aglomerace	aglomerace	k1gFnSc2	aglomerace
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
Bay	Bay	k1gMnSc1	Bay
Area	area	k1gFnSc1	area
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Santa	Santa	k1gFnSc1	Santa
Clara	Clara	k1gFnSc1	Clara
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
na	na	k7c6	na
severu	sever	k1gInSc6	sever
hraničí	hraničit	k5eAaImIp3nP	hraničit
se	s	k7c7	s
Sanfranciským	sanfranciský	k2eAgInSc7d1	sanfranciský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
časopise	časopis	k1gInSc6	časopis
Electronic	Electronice	k1gFnPc2	Electronice
News	News	k1gInSc4	News
začala	začít	k5eAaPmAgFnS	začít
vycházet	vycházet	k5eAaImF	vycházet
týdenní	týdenní	k2eAgFnSc1d1	týdenní
rubrika	rubrika	k1gFnSc1	rubrika
"	"	kIx"	"
<g/>
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gFnSc2	Vallea
USA	USA	kA	USA
<g/>
"	"	kIx"	"
od	od	k7c2	od
Dona	Don	k1gMnSc2	Don
Hoeflera	Hoefler	k1gMnSc2	Hoefler
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
koncentraci	koncentrace	k1gFnSc6	koncentrace
společností	společnost	k1gFnPc2	společnost
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	s	k7c7	s
křemíkovými	křemíkový	k2eAgInPc7d1	křemíkový
mikročipy	mikročip	k1gInPc7	mikročip
a	a	k8xC	a
počítači	počítač	k1gInPc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
společnosti	společnost	k1gFnPc1	společnost
postupně	postupně	k6eAd1	postupně
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
ovocné	ovocný	k2eAgFnPc1d1	ovocná
sady	sada	k1gFnPc1	sada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
daly	dát	k5eAaPmAgFnP	dát
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
její	její	k3xOp3gNnSc4	její
původní	původní	k2eAgNnSc4d1	původní
jméno	jméno	k1gNnSc4	jméno
–	–	k?	–
Údolí	údolí	k1gNnSc2	údolí
potěchy	potěcha	k1gFnSc2	potěcha
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
Silicon	Silicon	kA	Silicon
Valley	Valley	k1gInPc1	Valley
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
centrum	centrum	k1gNnSc4	centrum
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
San	San	k1gFnSc4	San
José	José	k1gNnSc2	José
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
dvěma	dva	k4xCgInPc7	dva
miliony	milion	k4xCgInPc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Sanfranciské	sanfranciský	k2eAgNnSc1d1	sanfranciské
pobřeží	pobřeží	k1gNnSc1	pobřeží
bylo	být	k5eAaImAgNnS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
využíváno	využívat	k5eAaImNgNnS	využívat
americkým	americký	k2eAgNnSc7d1	americké
námořnictvem	námořnictvo	k1gNnSc7	námořnictvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zde	zde	k6eAd1	zde
provádělo	provádět	k5eAaImAgNnS	provádět
výzkumy	výzkum	k1gInPc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
firem	firma	k1gFnPc2	firma
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
vývojem	vývoj	k1gInSc7	vývoj
technologií	technologie	k1gFnPc2	technologie
se	se	k3xPyFc4	se
usadilo	usadit	k5eAaPmAgNnS	usadit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pracovaly	pracovat	k5eAaImAgFnP	pracovat
pro	pro	k7c4	pro
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
"	"	kIx"	"
<g/>
mariňáci	mariňáci	k?	mariňáci
<g/>
"	"	kIx"	"
opustili	opustit	k5eAaPmAgMnP	opustit
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
jejich	jejich	k3xOp3gNnSc2	jejich
vybavení	vybavení	k1gNnSc2	vybavení
převzalo	převzít	k5eAaPmAgNnS	převzít
NASA	NASA	kA	NASA
a	a	k8xC	a
zaměřilo	zaměřit	k5eAaPmAgNnS	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
letecký	letecký	k2eAgInSc4d1	letecký
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
firem	firma	k1gFnPc2	firma
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zůstala	zůstat	k5eAaPmAgFnS	zůstat
a	a	k8xC	a
přibývaly	přibývat	k5eAaImAgFnP	přibývat
nové	nový	k2eAgFnPc1d1	nová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gFnSc2	Vallea
světovým	světový	k2eAgInSc7d1	světový
centrem	centr	k1gInSc7	centr
počítačového	počítačový	k2eAgInSc2d1	počítačový
a	a	k8xC	a
technologického	technologický	k2eAgInSc2d1	technologický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemka	k1gFnPc1	pozemka
na	na	k7c6	na
nejlukrativnějších	lukrativní	k2eAgNnPc6d3	nejlukrativnější
místech	místo	k1gNnPc6	místo
stojí	stát	k5eAaImIp3nS	stát
tolik	tolik	k6eAd1	tolik
co	co	k9	co
na	na	k7c6	na
newyorském	newyorský	k2eAgInSc6d1	newyorský
Manhattanu	Manhattan	k1gInSc6	Manhattan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
firmy	firma	k1gFnPc1	firma
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gFnSc2	Vallea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pirates	Pirates	k1gMnSc1	Pirates
of	of	k?	of
silicon	silicon	k1gMnSc1	silicon
valley	vallea	k1gFnSc2	vallea
</s>
</p>
<p>
<s>
The	The	k?	The
Rise	Rise	k1gInSc1	Rise
of	of	k?	of
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gFnSc2	Vallea
</s>
</p>
