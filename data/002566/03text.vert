<s>
Jukio	Jukio	k1gNnSc1	Jukio
Mišima	Mišimum	k1gNnSc2	Mišimum
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
三	三	k?	三
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Kimitake	Kimitak	k1gFnSc2	Kimitak
Hiraoka	Hiraoek	k1gInSc2	Hiraoek
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
平	平	k?	平
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1925	[number]	k4	1925
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
japonský	japonský	k2eAgMnSc1d1	japonský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svou	svůj	k3xOyFgFnSc7	svůj
rituální	rituální	k2eAgFnSc7d1	rituální
sebevraždou	sebevražda	k1gFnSc7	sebevražda
seppuku	seppuk	k1gInSc2	seppuk
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
otec	otec	k1gMnSc1	otec
Azuma	Azum	k1gMnSc2	Azum
Hiraoka	Hiraoko	k1gNnSc2	Hiraoko
byl	být	k5eAaImAgMnS	být
vládní	vládní	k2eAgMnSc1d1	vládní
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
ho	on	k3xPp3gMnSc4	on
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
babička	babička	k1gFnSc1	babička
Nacu	Naca	k1gFnSc4	Naca
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
uzurpátorské	uzurpátorský	k2eAgInPc4d1	uzurpátorský
sklony	sklon	k1gInPc4	sklon
a	a	k8xC	a
malému	malý	k2eAgMnSc3d1	malý
chlapci	chlapec	k1gMnSc3	chlapec
neustále	neustále	k6eAd1	neustále
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
jeho	jeho	k3xOp3gInSc4	jeho
"	"	kIx"	"
<g/>
samurajský	samurajský	k2eAgInSc4d1	samurajský
<g/>
"	"	kIx"	"
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
životopisců	životopisec	k1gMnPc2	životopisec
právě	právě	k9	právě
jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
fascinaci	fascinace	k1gFnSc4	fascinace
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
dílech	díl	k1gInPc6	díl
<g/>
;	;	kIx,	;
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
musel	muset	k5eAaImAgMnS	muset
obvykle	obvykle	k6eAd1	obvykle
trávit	trávit	k5eAaImF	trávit
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
pokoji	pokoj	k1gInSc6	pokoj
a	a	k8xC	a
zřídka	zřídka	k6eAd1	zřídka
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
dovolila	dovolit	k5eAaPmAgFnS	dovolit
hrát	hrát	k5eAaImF	hrát
si	se	k3xPyFc3	se
venku	venku	k6eAd1	venku
(	(	kIx(	(
<g/>
odůvodňovala	odůvodňovat	k5eAaImAgFnS	odůvodňovat
to	ten	k3xDgNnSc1	ten
chlapcovo	chlapcův	k2eAgNnSc1d1	chlapcovo
chatrným	chatrný	k2eAgNnSc7d1	chatrné
zdravím	zdraví	k1gNnSc7	zdraví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
si	se	k3xPyFc3	se
směl	smět	k5eAaImAgMnS	smět
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
pečlivě	pečlivě	k6eAd1	pečlivě
vybranými	vybraný	k2eAgFnPc7d1	vybraná
dívkami	dívka	k1gFnPc7	dívka
ze	z	k7c2	z
sousedství	sousedství	k1gNnSc2	sousedství
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
dětství	dětství	k1gNnSc6	dětství
osvojil	osvojit	k5eAaPmAgInS	osvojit
ženskou	ženský	k2eAgFnSc4d1	ženská
řeč	řeč	k1gFnSc4	řeč
a	a	k8xC	a
dívčí	dívčí	k2eAgNnSc4d1	dívčí
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
domácnosti	domácnost	k1gFnSc2	domácnost
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
snažil	snažit	k5eAaImAgMnS	snažit
přísně	přísně	k6eAd1	přísně
vychovávat	vychovávat	k5eAaImF	vychovávat
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
naplánoval	naplánovat	k5eAaBmAgMnS	naplánovat
kariéru	kariéra	k1gFnSc4	kariéra
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
právníka	právník	k1gMnSc2	právník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
zaujímal	zaujímat	k5eAaImAgInS	zaujímat
nepřátelský	přátelský	k2eNgInSc1d1	nepřátelský
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
Mišimovým	Mišimův	k2eAgInPc3d1	Mišimův
literárním	literární	k2eAgInPc3d1	literární
pokusům	pokus	k1gInPc3	pokus
<g/>
,	,	kIx,	,
chlapec	chlapec	k1gMnSc1	chlapec
proto	proto	k8xC	proto
psal	psát	k5eAaImAgMnS	psát
tajně	tajně	k6eAd1	tajně
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
elitního	elitní	k2eAgNnSc2d1	elitní
vzdělávacího	vzdělávací	k2eAgNnSc2d1	vzdělávací
zařízení	zařízení	k1gNnSc2	zařízení
Gakušúin	Gakušúina	k1gFnPc2	Gakušúina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Šlechtická	šlechtický	k2eAgFnSc1d1	šlechtická
škola	škola	k1gFnSc1	škola
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
letech	let	k1gInPc6	let
postoupil	postoupit	k5eAaPmAgMnS	postoupit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Gakušúinu	Gakušúin	k1gInSc2	Gakušúin
na	na	k7c4	na
nižší	nízký	k2eAgFnSc4d2	nižší
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
uveřejňuje	uveřejňovat	k5eAaImIp3nS	uveřejňovat
své	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
básně	báseň	k1gFnSc2	báseň
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
i	i	k9	i
první	první	k4xOgFnSc2	první
povídky	povídka	k1gFnSc2	povídka
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
časopise	časopis	k1gInSc6	časopis
Hodžinkai	Hodžinka	k1gFnSc2	Hodžinka
zašši	zašsat	k5eAaPmIp1nSwK	zašsat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
příspěvky	příspěvek	k1gInPc1	příspěvek
ihned	ihned	k6eAd1	ihned
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
v	v	k7c6	v
Gakušúinu	Gakušúin	k1gInSc6	Gakušúin
značnou	značný	k2eAgFnSc4d1	značná
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Mladého	mladý	k1gMnSc4	mladý
autora	autor	k1gMnSc4	autor
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
např.	např.	kA	např.
Rainer	Rainra	k1gFnPc2	Rainra
Maria	Maria	k1gFnSc1	Maria
Rilke	Rilke	k1gFnSc1	Rilke
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gInSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
a	a	k8xC	a
japonští	japonský	k2eAgMnPc1d1	japonský
klasici	klasik	k1gMnPc1	klasik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
významná	významný	k2eAgFnSc1d1	významná
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
metaforický	metaforický	k2eAgInSc1d1	metaforický
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
sepětí	sepětí	k1gNnSc6	sepětí
mezi	mezi	k7c7	mezi
zemřelými	zemřelý	k2eAgMnPc7d1	zemřelý
předky	předek	k1gMnPc7	předek
a	a	k8xC	a
žijícími	žijící	k2eAgMnPc7d1	žijící
lidmi	člověk	k1gMnPc7	člověk
Hanazakari	Hanazakar	k1gFnSc2	Hanazakar
no	no	k9	no
mori	mori	k6eAd1	mori
(	(	kIx(	(
<g/>
Rozkvetlý	rozkvetlý	k2eAgInSc1d1	rozkvetlý
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
jeho	jeho	k3xOp3gMnSc2	jeho
učitele	učitel	k1gMnSc2	učitel
literatury	literatura	k1gFnSc2	literatura
Fumia	Fumius	k1gMnSc2	Fumius
Šimizua	Šimizuus	k1gMnSc2	Šimizuus
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
v	v	k7c6	v
prestižním	prestižní	k2eAgInSc6d1	prestižní
literárním	literární	k2eAgInSc6d1	literární
magazínu	magazín	k1gInSc6	magazín
Bungei-Bunka	Bungei-Bunka	k1gFnSc1	Bungei-Bunka
(	(	kIx(	(
<g/>
Literární	literární	k2eAgFnSc1d1	literární
kultura	kultura	k1gFnSc1	kultura
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
titulním	titulní	k2eAgInSc7d1	titulní
příběhem	příběh	k1gInSc7	příběh
sbírky	sbírka	k1gFnSc2	sbírka
kratších	krátký	k2eAgFnPc2d2	kratší
próz	próza	k1gFnPc2	próza
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
v	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
Šimizua	Šimizuus	k1gMnSc2	Šimizuus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
chlapcovu	chlapcův	k2eAgNnSc3d1	chlapcovo
mládí	mládí	k1gNnSc3	mládí
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
skrýt	skrýt	k5eAaPmF	skrýt
jeho	jeho	k3xOp3gFnSc4	jeho
totožnost	totožnost	k1gFnSc4	totožnost
<g/>
,	,	kIx,	,
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
Rozkvetlý	rozkvetlý	k2eAgInSc1d1	rozkvetlý
les	les	k1gInSc1	les
pod	pod	k7c7	pod
pečlivě	pečlivě	k6eAd1	pečlivě
zvoleným	zvolený	k2eAgInSc7d1	zvolený
literárním	literární	k2eAgInSc7d1	literární
pseudonymem	pseudonym	k1gInSc7	pseudonym
Jukio	Jukio	k1gMnSc1	Jukio
Mišima	Mišim	k1gMnSc2	Mišim
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
používal	používat	k5eAaImAgInS	používat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jasunariho	Jasunari	k1gMnSc2	Jasunari
Kawabaty	Kawabata	k1gFnSc2	Kawabata
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
literárním	literární	k2eAgInSc6d1	literární
časopise	časopis	k1gInSc6	časopis
Ningen	Ningen	k1gInSc1	Ningen
(	(	kIx(	(
<g/>
Člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
Mišimova	Mišimův	k2eAgFnSc1d1	Mišimova
povídka	povídka	k1gFnSc1	povídka
Cigareta	cigareta	k1gFnSc1	cigareta
(	(	kIx(	(
<g/>
Tabako	Tabako	k1gNnSc1	Tabako
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
sportovně	sportovně	k6eAd1	sportovně
založení	založení	k1gNnSc4	založení
spolužáci	spolužák	k1gMnPc1	spolužák
elitní	elitní	k2eAgFnSc2d1	elitní
školy	škola	k1gFnSc2	škola
pohrdají	pohrdat	k5eAaImIp3nP	pohrdat
chlapcem	chlapec	k1gMnSc7	chlapec
s	s	k7c7	s
literárními	literární	k2eAgInPc7d1	literární
zájmy	zájem	k1gInPc7	zájem
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
i	i	k9	i
významnou	významný	k2eAgFnSc4d1	významná
povídku	povídka	k1gFnSc4	povídka
Ši	Ši	k1gFnSc2	Ši
wo	wo	k?	wo
kaku	kaku	k5eAaPmIp1nS	kaku
šónen	šónen	k2eAgMnSc1d1	šónen
(	(	kIx(	(
<g/>
Chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
píše	psát	k5eAaImIp3nS	psát
verše	verš	k1gInPc4	verš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
povolán	povolat	k5eAaPmNgMnS	povolat
do	do	k7c2	do
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
armádní	armádní	k2eAgMnSc1d1	armádní
lékař	lékař	k1gMnSc1	lékař
ale	ale	k8xC	ale
zaměnil	zaměnit	k5eAaPmAgMnS	zaměnit
jeho	jeho	k3xOp3gNnSc4	jeho
akutní	akutní	k2eAgNnSc4d1	akutní
respirační	respirační	k2eAgNnSc4d1	respirační
onemocnění	onemocnění	k1gNnSc4	onemocnění
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
za	za	k7c4	za
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
neschopného	schopný	k2eNgMnSc4d1	neschopný
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnPc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
na	na	k7c4	na
právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
Tokijské	tokijský	k2eAgFnSc2d1	Tokijská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
promoval	promovat	k5eAaBmAgMnS	promovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
rok	rok	k1gInSc1	rok
pak	pak	k6eAd1	pak
pracoval	pracovat	k5eAaImAgInS	pracovat
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
bude	být	k5eAaImBp3nS	být
věnovat	věnovat	k5eAaImF	věnovat
výlučně	výlučně	k6eAd1	výlučně
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
časopisecky	časopisecky	k6eAd1	časopisecky
publikovaných	publikovaný	k2eAgFnPc6d1	publikovaná
povídkách	povídka	k1gFnPc6	povídka
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
román	román	k1gInSc4	román
Zloději	zloděj	k1gMnPc1	zloděj
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
盗	盗	k?	盗
<g/>
,	,	kIx,	,
Tózoku	Tózok	k1gInSc2	Tózok
<g/>
)	)	kIx)	)
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
dvou	dva	k4xCgMnPc2	dva
mladých	mladý	k2eAgMnPc2d1	mladý
aristokratů	aristokrat	k1gMnPc2	aristokrat
<g/>
.	.	kIx.	.
</s>
<s>
Slávu	Sláva	k1gFnSc4	Sláva
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
získal	získat	k5eAaPmAgInS	získat
až	až	k9	až
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
román	román	k1gInSc4	román
s	s	k7c7	s
autobiografickými	autobiografický	k2eAgInPc7d1	autobiografický
prvky	prvek	k1gInPc7	prvek
Zpověď	zpověď	k1gFnSc4	zpověď
masky	maska	k1gFnSc2	maska
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
仮	仮	k?	仮
<g/>
,	,	kIx,	,
Kamen	kamna	k1gNnPc2	kamna
no	no	k9	no
kokuhaku	kokuhak	k1gInSc2	kokuhak
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
líčí	líčit	k5eAaImIp3nS	líčit
dětství	dětství	k1gNnSc1	dětství
a	a	k8xC	a
dospívání	dospívání	k1gNnSc1	dospívání
chlapce	chlapec	k1gMnSc2	chlapec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
odlišnosti	odlišnost	k1gFnSc3	odlišnost
od	od	k7c2	od
většinové	většinový	k2eAgFnSc2d1	většinová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
psal	psát	k5eAaImAgInS	psát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc2d1	populární
eseje	esej	k1gFnSc2	esej
<g/>
,	,	kIx,	,
novely	novela	k1gFnSc2	novela
a	a	k8xC	a
povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
také	také	k9	také
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
her	hra	k1gFnPc2	hra
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgNnPc4d1	tradiční
divadla	divadlo	k1gNnPc4	divadlo
kabuki	kabuki	k6eAd1	kabuki
a	a	k8xC	a
nó	nó	k0	nó
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgMnSc1d1	populární
i	i	k8xC	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dílech	dílo	k1gNnPc6	dílo
využíval	využívat	k5eAaPmAgMnS	využívat
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
třikrát	třikrát	k6eAd1	třikrát
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
ji	on	k3xPp3gFnSc4	on
neobdržel	obdržet	k5eNaPmAgMnS	obdržet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
posilování	posilování	k1gNnPc4	posilování
a	a	k8xC	a
kendó	kendó	k?	kendó
<g/>
,	,	kIx,	,
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
totiž	totiž	k9	totiž
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
intelektuálové	intelektuál	k1gMnPc1	intelektuál
kladou	klást	k5eAaImIp3nP	klást
tak	tak	k6eAd1	tak
malý	malý	k2eAgInSc4d1	malý
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
cvičení	cvičení	k1gNnSc4	cvičení
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
vojenský	vojenský	k2eAgInSc1d1	vojenský
výcvik	výcvik	k1gInSc1	výcvik
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
svou	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
soukromou	soukromý	k2eAgFnSc4d1	soukromá
armádu	armáda	k1gFnSc4	armáda
<g/>
"	"	kIx"	"
Tatenokai	Tatenokae	k1gFnSc4	Tatenokae
(	(	kIx(	(
<g/>
Společnost	společnost	k1gFnSc1	společnost
štítu	štít	k1gInSc2	štít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
střežit	střežit	k5eAaImF	střežit
japonské	japonský	k2eAgFnPc4d1	japonská
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
ženu	žena	k1gFnSc4	žena
jménem	jméno	k1gNnSc7	jméno
Jóko	Jóko	k1gNnSc4	Jóko
Sugijama	Sugijamum	k1gNnSc2	Sugijamum
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Noriko	Norika	k1gFnSc5	Norika
(	(	kIx(	(
<g/>
narozena	narozen	k2eAgFnSc1d1	narozena
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Iičiróa	Iičiróus	k1gMnSc4	Iičiróus
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
režiséra	režisér	k1gMnSc2	režisér
Jasuzo	Jasuza	k1gFnSc5	Jasuza
Masamury	Masamura	k1gFnSc2	Masamura
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Afraid	Afraida	k1gFnPc2	Afraida
to	ten	k3xDgNnSc1	ten
Die	Die	k1gFnPc4	Die
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Lizard	Lizard	k1gMnSc1	Lizard
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hitokiri	Hitokire	k1gFnSc4	Hitokire
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgInSc2d1	vlastní
scénáře	scénář	k1gInSc2	scénář
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
povídce	povídka	k1gFnSc6	povídka
natočil	natočit	k5eAaBmAgInS	natočit
krátký	krátký	k2eAgInSc1d1	krátký
černobílý	černobílý	k2eAgInSc1d1	černobílý
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Láska	láska	k1gFnSc1	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Júkoku	Júkok	k1gInSc2	Júkok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
roli	role	k1gFnSc4	role
poručíka	poručík	k1gMnSc2	poručík
Císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spáchá	spáchat	k5eAaPmIp3nS	spáchat
seppuku	seppuk	k1gInSc3	seppuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
se	s	k7c7	s
čtyřmi	čtyři	k4xCgMnPc7	čtyři
členy	člen	k1gMnPc7	člen
Tatenokai	Tatenoka	k1gFnSc2	Tatenoka
zabarikádoval	zabarikádovat	k5eAaPmAgInS	zabarikádovat
na	na	k7c6	na
velitelství	velitelství	k1gNnSc6	velitelství
japonských	japonský	k2eAgFnPc2d1	japonská
Sil	síla	k1gFnPc2	síla
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
(	(	kIx(	(
<g/>
Džietai	Džieta	k1gFnSc2	Džieta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přivázali	přivázat	k5eAaPmAgMnP	přivázat
tamního	tamní	k2eAgMnSc4d1	tamní
velitele	velitel	k1gMnSc4	velitel
k	k	k7c3	k
židli	židle	k1gFnSc3	židle
a	a	k8xC	a
Mišima	Mišima	k1gFnSc1	Mišima
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
proslovu	proslov	k1gInSc6	proslov
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
požadavek	požadavek	k1gInSc1	požadavek
návratu	návrat	k1gInSc2	návrat
moci	moc	k1gFnSc2	moc
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
proslovu	proslov	k1gInSc6	proslov
spáchal	spáchat	k5eAaPmAgInS	spáchat
rituální	rituální	k2eAgFnSc4d1	rituální
sebevraždu	sebevražda	k1gFnSc4	sebevražda
seppuku	seppuk	k1gInSc2	seppuk
<g/>
.	.	kIx.	.
</s>
<s>
Mišima	Mišim	k1gMnSc4	Mišim
napsal	napsat	k5eAaBmAgMnS	napsat
čtyřicet	čtyřicet	k4xCc4	čtyřicet
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
šedesát	šedesát	k4xCc4	šedesát
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
moderního	moderní	k2eAgNnSc2d1	moderní
divadla	divadlo	k1gNnSc2	divadlo
psal	psát	k5eAaImAgMnS	psát
i	i	k9	i
hry	hra	k1gFnPc4	hra
pro	pro	k7c4	pro
tradiční	tradiční	k2eAgNnPc4d1	tradiční
divadla	divadlo	k1gNnPc4	divadlo
nó	nó	k0	nó
a	a	k8xC	a
kabuki	kabukit	k5eAaImRp2nS	kabukit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvacet	dvacet	k4xCc4	dvacet
svazků	svazek	k1gInPc2	svazek
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
dvacet	dvacet	k4xCc4	dvacet
svazků	svazek	k1gInPc2	svazek
esejí	esej	k1gFnPc2	esej
i	i	k8xC	i
filmový	filmový	k2eAgInSc1d1	filmový
scénář	scénář	k1gInSc1	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
poválečné	poválečný	k2eAgFnSc2d1	poválečná
japonské	japonský	k2eAgFnSc2d1	japonská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
hlavním	hlavní	k2eAgNnPc3d1	hlavní
tématům	téma	k1gNnPc3	téma
patří	patřit	k5eAaImIp3nS	patřit
fascinace	fascinace	k1gFnSc1	fascinace
smrtí	smrt	k1gFnPc2	smrt
a	a	k8xC	a
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
tradiční	tradiční	k2eAgFnPc4d1	tradiční
japonské	japonský	k2eAgFnPc4d1	japonská
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
vynikají	vynikat	k5eAaImIp3nP	vynikat
bohatým	bohatý	k2eAgInSc7d1	bohatý
metaforickým	metaforický	k2eAgInSc7d1	metaforický
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
vyšel	vyjít	k5eAaPmAgInS	vyjít
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
Mišimův	Mišimův	k2eAgInSc1d1	Mišimův
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc1d1	zlatý
pavilon	pavilon	k1gInSc1	pavilon
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
金	金	k?	金
<g/>
,	,	kIx,	,
Kinkakudži	Kinkakudž	k1gFnSc6	Kinkakudž
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Kláry	Klára	k1gFnSc2	Klára
Macúchové	Macúchová	k1gFnSc2	Macúchová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Brody	Brod	k1gInPc1	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
literárních	literární	k2eAgFnPc2d1	literární
a	a	k8xC	a
náboženských	náboženský	k2eAgFnPc2d1	náboženská
narážek	narážka	k1gFnPc2	narážka
novelizuje	novelizovat	k5eAaBmIp3nS	novelizovat
reálné	reálný	k2eAgFnSc3d1	reálná
události	událost	k1gFnSc3	událost
zapálení	zapálení	k1gNnPc2	zapálení
chrámu	chrámat	k5eAaImIp1nS	chrámat
Kinkakudži	Kinkakudž	k1gFnSc3	Kinkakudž
psychicky	psychicky	k6eAd1	psychicky
nemocným	nemocný	k1gMnSc7	nemocný
mnichem	mnich	k1gMnSc7	mnich
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
zajistil	zajistit	k5eAaPmAgInS	zajistit
autorovi	autor	k1gMnSc3	autor
díky	díky	k7c3	díky
anglickému	anglický	k2eAgInSc3d1	anglický
překladu	překlad	k1gInSc3	překlad
Ivana	Ivana	k1gFnSc1	Ivana
Morrise	Morrise	k1gFnSc2	Morrise
světovou	světový	k2eAgFnSc4d1	světová
slávu	sláva	k1gFnSc4	sláva
a	a	k8xC	a
počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
k	k	k7c3	k
jeho	jeho	k3xOp3gNnPc3	jeho
nejlepším	dobrý	k2eAgNnPc3d3	nejlepší
dílům	dílo	k1gNnPc3	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
ve	v	k7c6	v
Světové	světový	k2eAgFnSc6d1	světová
literatuře	literatura	k1gFnSc6	literatura
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Kláry	Klára	k1gFnSc2	Klára
Macúchové	Macúchová	k1gFnSc2	Macúchová
novela	novela	k1gFnSc1	novela
Láska	láska	k1gFnSc1	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
憂	憂	k?	憂
<g/>
,	,	kIx,	,
Júkoku	Júkok	k1gInSc2	Júkok
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Yukio	Yukio	k1gNnSc1	Yukio
Mishima	Mishimum	k1gNnSc2	Mishimum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
