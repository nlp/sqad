<s>
Poušť	poušť	k1gFnSc1	poušť
je	být	k5eAaImIp3nS	být
neúrodná	úrodný	k2eNgFnSc1d1	neúrodná
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
za	za	k7c4	za
horní	horní	k2eAgFnSc4d1	horní
hranici	hranice	k1gFnSc4	hranice
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
250	[number]	k4	250
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
