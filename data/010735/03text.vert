<p>
<s>
Petr	Petr	k1gMnSc1	Petr
"	"	kIx"	"
<g/>
Doldy	Dold	k1gInPc1	Dold
<g/>
"	"	kIx"	"
Dolének	Dolének	k1gInSc1	Dolének
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
Přerov	Přerov	k1gInSc1	Přerov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
skupiny	skupina	k1gFnSc2	skupina
Kreyson	Kreyson	k1gMnSc1	Kreyson
Memorial	Memorial	k1gMnSc1	Memorial
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
aranžér	aranžér	k1gMnSc1	aranžér
a	a	k8xC	a
multiinstrumentalista	multiinstrumentalista	k1gMnSc1	multiinstrumentalista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
16	[number]	k4	16
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Doldy	Dolda	k1gFnSc2	Dolda
Dolének	Dolének	k1gInSc1	Dolének
nahrál	nahrát	k5eAaBmAgMnS	nahrát
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
demosnímky	demosnímek	k1gInPc4	demosnímek
v	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Bechyni	Bechyně	k1gFnSc6	Bechyně
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
koncertovat	koncertovat	k5eAaImF	koncertovat
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
baskytaru	baskytara	k1gFnSc4	baskytara
a	a	k8xC	a
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Sezimově	Sezimův	k2eAgNnSc6d1	Sezimovo
Ústí	ústí	k1gNnSc6	ústí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
hudební	hudební	k2eAgNnSc4d1	hudební
studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
Michaela	Michael	k1gMnSc4	Michael
a	a	k8xC	a
Petra	Petr	k1gMnSc4	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hudebním	hudební	k2eAgInSc7d1	hudební
vzorem	vzor	k1gInSc7	vzor
jsou	být	k5eAaImIp3nP	být
Dream	Dream	k1gInSc4	Dream
Theatre	Theatr	k1gInSc5	Theatr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Wolfram	wolfram	k1gInSc1	wolfram
Liars	Liars	k1gInSc4	Liars
In	In	k1gFnSc2	In
A	a	k9	a
Second	Second	k1gMnSc1	Second
Skin	skin	k1gMnSc1	skin
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Řemen	řemen	k1gInSc4	řemen
a	a	k8xC	a
hosté	host	k1gMnPc1	host
Živě	živě	k6eAd1	živě
...	...	k?	...
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Nokturno	nokturno	k1gNnSc1	nokturno
20	[number]	k4	20
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Wolfram	wolfram	k1gInSc4	wolfram
No	no	k9	no
Redemption	Redemption	k1gInSc4	Redemption
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Sali	Sali	k1gNnSc7	Sali
Silent	Silent	k1gInSc1	Silent
Scream	Scream	k1gInSc1	Scream
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
William	William	k1gInSc1	William
Demo	demo	k2eAgInSc2d1	demo
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Sali	Sali	k1gNnSc1	Sali
Unbreakable	Unbreakable	k1gFnPc2	Unbreakable
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Dirty	Dirta	k1gMnSc2	Dirta
Game	game	k1gInSc4	game
Demo	demo	k2eAgFnSc2d1	demo
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Blackout	Blackout	k1gMnSc1	Blackout
Band	banda	k1gFnPc2	banda
Máš	mít	k5eAaImIp2nS	mít
mat	mat	k6eAd1	mat
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Sali	Sali	k1gNnSc1	Sali
Eternal	Eternal	k1gFnPc2	Eternal
Steel	Steela	k1gFnPc2	Steela
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Dirty	Dirta	k1gMnSc2	Dirta
Game	game	k1gInSc4	game
Flame	Flam	k1gInSc5	Flam
Of	Of	k1gFnSc7	Of
Loneliness	Loneliness	k1gInSc4	Loneliness
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Morava	Morava	k1gFnSc1	Morava
Brána	brána	k1gFnSc1	brána
času	čas	k1gInSc2	čas
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Rentgen	rentgen	k1gInSc1	rentgen
Svět	svět	k1gInSc4	svět
zatracených	zatracený	k2eAgMnPc2d1	zatracený
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
Dubínek	Dubínka	k1gFnPc2	Dubínka
Jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
vánocích	vánoce	k1gFnPc6	vánoce
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
Autobus	autobus	k1gInSc4	autobus
Poslové	posel	k1gMnPc5	posel
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
Wolfram	wolfram	k1gInSc1	wolfram
Covers	Covers	k1gInSc4	Covers
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2019	[number]	k4	2019
Kreyson	Kreyson	k1gMnSc1	Kreyson
Memorial	Memorial	k1gMnSc1	Memorial
Strážci	strážce	k1gMnSc3	strážce
plamenů	plamen	k1gInPc2	plamen
</s>
</p>
<p>
<s>
==	==	k?	==
Společné	společný	k2eAgInPc4d1	společný
koncerty	koncert	k1gInPc4	koncert
==	==	k?	==
</s>
</p>
<p>
<s>
Společné	společný	k2eAgInPc1d1	společný
tour	tour	k1gInSc1	tour
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Arakain	Arakaina	k1gFnPc2	Arakaina
</s>
</p>
<p>
<s>
Společné	společný	k2eAgInPc1d1	společný
tour	tour	k1gInSc1	tour
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Škwor	Škwora	k1gFnPc2	Škwora
</s>
</p>
<p>
<s>
Společné	společný	k2eAgInPc1d1	společný
tour	tour	k1gInSc1	tour
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Jerem	jer	k1gInSc7	jer
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
</s>
</p>
<p>
<s>
Společné	společný	k2eAgInPc1d1	společný
tour	tour	k1gInSc1	tour
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
K2	K2	k1gFnSc2	K2
</s>
</p>
<p>
<s>
==	==	k?	==
Spolupráce	spolupráce	k1gFnPc4	spolupráce
a	a	k8xC	a
hostování	hostování	k1gNnPc4	hostování
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
==	==	k?	==
</s>
</p>
<p>
<s>
Sammael	Sammael	k1gMnSc1	Sammael
</s>
</p>
<p>
<s>
Morava	Morava	k1gFnSc1	Morava
</s>
</p>
<p>
<s>
Autobus	autobus	k1gInSc4	autobus
</s>
</p>
<p>
<s>
Krokus	krokus	k1gInSc1	krokus
</s>
</p>
<p>
<s>
Kern	Kern	k1gMnSc1	Kern
</s>
</p>
<p>
<s>
In	In	k?	In
Siders	Siders	k1gInSc1	Siders
</s>
</p>
<p>
<s>
Mylions	Mylions	k6eAd1	Mylions
</s>
</p>
<p>
<s>
Bohemica	Bohemica	k6eAd1	Bohemica
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Krob	Krob	k1gMnSc1	Krob
Band	banda	k1gFnPc2	banda
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
https://www.rockpalace.cz/archiv/rockpalace-archiv-2016/item/747-doldy-dolenek-zpiva-andela-na-uteku	[url]	k5eAaPmIp1nSwK	https://www.rockpalace.cz/archiv/rockpalace-archiv-2016/item/747-doldy-dolenek-zpiva-andela-na-uteku
</s>
</p>
<p>
<s>
http://danielkrob.cz/petr-doldy-dolenek-zpev/	[url]	k?	http://danielkrob.cz/petr-doldy-dolenek-zpev/
</s>
</p>
<p>
<s>
http://kreyson.cz/petr-doldy-dolenek/	[url]	k?	http://kreyson.cz/petr-doldy-dolenek/
</s>
</p>
<p>
<s>
https://metalforever.info/article.php?id=94	[url]	k4	https://metalforever.info/article.php?id=94
</s>
</p>
<p>
<s>
http://hippo.feld.cvut.cz/metal/wolfram/doldy.html	[url]	k1gMnSc1	http://hippo.feld.cvut.cz/metal/wolfram/doldy.html
</s>
</p>
<p>
<s>
http://hippo.feld.cvut.cz/metal/wolfram/roman_krokus_kriz/fotogalerie_nataceni_wolfram_covers2_inspirace.html	[url]	k1gMnSc1	http://hippo.feld.cvut.cz/metal/wolfram/roman_krokus_kriz/fotogalerie_nataceni_wolfram_covers2_inspirace.html
</s>
</p>
<p>
<s>
http://www.remen.cz/diskografie/	[url]	k?	http://www.remen.cz/diskografie/
</s>
</p>
<p>
<s>
http://regiolist.cz/kreyson-memorial-novy-koncertni-program/	[url]	k?	http://regiolist.cz/kreyson-memorial-novy-koncertni-program/
</s>
</p>
<p>
<s>
http://www.rockpalace.cz/component/k2/item/708-kreyson-andel-na-uteku-po-25-letecopet-zivy	[url]	k1gFnPc1	http://www.rockpalace.cz/component/k2/item/708-kreyson-andel-na-uteku-po-25-letecopet-zivy
</s>
</p>
<p>
<s>
http://www.danielkrob.com/2015/11/rozhovor-dk-pro-metalmania-magazin-eu/	[url]	k4	http://www.danielkrob.com/2015/11/rozhovor-dk-pro-metalmania-magazin-eu/
</s>
</p>
