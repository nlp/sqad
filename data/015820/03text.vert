<s>
Grande	grand	k1gMnSc5
Peur	Peur	k1gMnSc1
</s>
<s>
Grande	grand	k1gMnSc5
Peur	Peur	k1gMnSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
velký	velký	k2eAgInSc1d1
strach	strach	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ve	v	k7c6
francouzských	francouzský	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
označení	označení	k1gNnPc2
pro	pro	k7c4
sérii	série	k1gFnSc4
událostí	událost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgFnP
na	na	k7c6
francouzském	francouzský	k2eAgInSc6d1
venkově	venkov	k1gInSc6
mezi	mezi	k7c7
20	#num#	k4
<g/>
.	.	kIx.
červencem	červenec	k1gInSc7
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
srpnem	srpen	k1gInSc7
1789	#num#	k4
krátce	krátce	k6eAd1
po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
Velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c6
zvěsti	zvěst	k1gFnSc6
o	o	k7c4
spiknutí	spiknutí	k1gNnSc4
aristokracie	aristokracie	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
rolníci	rolník	k1gMnPc1
chopili	chopit	k5eAaPmAgMnP
zbraní	zbraň	k1gFnSc7
a	a	k8xC
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
propukaly	propukat	k5eAaImAgFnP
násilné	násilný	k2eAgFnPc1d1
rolnické	rolnický	k2eAgFnPc1d1
vzpoury	vzpoura	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
povstání	povstání	k1gNnSc3
vedla	vést	k5eAaImAgFnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
Ústavodárné	ústavodárný	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
pod	pod	k7c7
tlakem	tlak	k1gInSc7
těchto	tento	k3xDgFnPc2
událostí	událost	k1gFnPc2
v	v	k7c6
noci	noc	k1gFnSc6
ze	z	k7c2
4	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1789	#num#	k4
zrušilo	zrušit	k5eAaPmAgNnS
mnoho	mnoho	k4c1
výsad	výsada	k1gFnPc2
privilegovaných	privilegovaný	k2eAgInPc2d1
stavů	stav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
1789	#num#	k4
mnoho	mnoho	k4c1
sedláků	sedlák	k1gMnPc2
odmítlo	odmítnout	k5eAaPmAgNnS
zaplatit	zaplatit	k5eAaPmF
daně	daň	k1gFnPc4
nebo	nebo	k8xC
poplatky	poplatek	k1gInPc1
svým	svůj	k3xOyFgMnPc3
lením	lenit	k5eAaImIp1nS
pánům	pan	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
byla	být	k5eAaImAgFnS
venkovská	venkovský	k2eAgFnSc1d1
populace	populace	k1gFnSc1
pod	pod	k7c7
vlivem	vliv	k1gInSc7
událostí	událost	k1gFnPc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
(	(	kIx(
<g/>
útok	útok	k1gInSc1
na	na	k7c4
Bastilu	Bastila	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obávala	obávat	k5eAaImAgFnS
se	se	k3xPyFc4
pomsty	pomsta	k1gFnPc1
ze	z	k7c2
strany	strana	k1gFnSc2
šlechty	šlechta	k1gFnSc2
a	a	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
šířily	šířit	k5eAaImAgFnP
zvěsti	zvěst	k1gFnPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
celou	celý	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
táhnou	táhnout	k5eAaImIp3nP
hordy	horda	k1gFnPc1
žebráků	žebrák	k1gMnPc2
<g/>
,	,	kIx,
lupičů	lupič	k1gMnPc2
a	a	k8xC
intrikánů	intrikán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venkované	venkovan	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
panice	panika	k1gFnSc6
kvůli	kvůli	k7c3
této	tento	k3xDgFnSc3
údajné	údajný	k2eAgFnSc3d1
hrozbě	hrozba	k1gFnSc3
ozbrojili	ozbrojit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
si	se	k3xPyFc3
uvědomili	uvědomit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
zemí	zem	k1gFnSc7
žádné	žádný	k3yNgInPc4
loupežné	loupežný	k2eAgInPc4d1
bandy	band	k1gInPc4
netáhnou	táhnout	k5eNaImIp3nP
<g/>
,	,	kIx,
namířili	namířit	k5eAaPmAgMnP
nahromaděnou	nahromaděný	k2eAgFnSc4d1
agresi	agrese	k1gFnSc4
proti	proti	k7c3
šlechtě	šlechta	k1gFnSc3
a	a	k8xC
jejímu	její	k3xOp3gInSc3
majetku	majetek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
útokům	útok	k1gInPc3
na	na	k7c4
zámky	zámek	k1gInPc4
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
podpalování	podpalování	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
ničili	ničit	k5eAaImAgMnP
písemnosti	písemnost	k1gFnPc4
obsahující	obsahující	k2eAgFnSc2d1
daňové	daňový	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
poddaných	poddaný	k1gMnPc2
a	a	k8xC
výsady	výsada	k1gFnSc2
šlechty	šlechta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
série	série	k1gFnSc1
spontánních	spontánní	k2eAgFnPc2d1
násilností	násilnost	k1gFnPc2
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
dějin	dějiny	k1gFnPc2
pod	pod	k7c7
označením	označení	k1gNnSc7
Grande	grand	k1gMnSc5
Peur	Peur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
zasedající	zasedající	k2eAgNnSc1d1
ve	v	k7c6
Versailles	Versailles	k1gFnSc6
reagovalo	reagovat	k5eAaBmAgNnS
na	na	k7c4
venkovské	venkovský	k2eAgNnSc4d1
rolnické	rolnický	k2eAgNnSc4d1
povstání	povstání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
prokázalo	prokázat	k5eAaPmAgNnS
svoji	svůj	k3xOyFgFnSc4
způsobilost	způsobilost	k1gFnSc4
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
na	na	k7c6
zasedání	zasedání	k1gNnSc6
v	v	k7c6
noci	noc	k1gFnSc6
ze	z	k7c2
4	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1789	#num#	k4
zrušeny	zrušit	k5eAaPmNgFnP
některé	některý	k3yIgFnPc1
výsady	výsada	k1gFnPc1
pro	pro	k7c4
první	první	k4xOgInSc4
a	a	k8xC
druhý	druhý	k4xOgInSc4
stav	stav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
např.	např.	kA
daňové	daňový	k2eAgFnPc4d1
výsady	výsada	k1gFnPc4
šlechty	šlechta	k1gFnSc2
a	a	k8xC
nevolnictví	nevolnictví	k1gNnSc4
byly	být	k5eAaImAgFnP
zrušeny	zrušit	k5eAaPmNgFnP
<g/>
,	,	kIx,
práva	právo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
vázána	vázat	k5eAaImNgFnS
na	na	k7c6
vlastnictví	vlastnictví	k1gNnSc6
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
však	však	k9
zůstala	zůstat	k5eAaPmAgFnS
v	v	k7c6
platnosti	platnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedlákům	Sedlák	k1gMnPc3
zbývala	zbývat	k5eAaImAgNnP
jako	jako	k8xC,k8xS
jediná	jediný	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
se	se	k3xPyFc4
z	z	k7c2
placení	placení	k1gNnSc2
poplatků	poplatek	k1gInPc2
vykoupit	vykoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
tento	tento	k3xDgInSc4
kompromis	kompromis	k1gInSc4
byly	být	k5eAaImAgFnP
rolnické	rolnický	k2eAgFnPc1d1
vzpoury	vzpoura	k1gFnPc1
důležitým	důležitý	k2eAgInSc7d1
krokem	krok	k1gInSc7
při	při	k7c6
odstraňování	odstraňování	k1gNnSc6
feudální	feudální	k2eAgInSc1d1
systém	systém	k1gInSc1
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
koncem	konec	k1gInSc7
konstituční	konstituční	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
feudální	feudální	k2eAgInSc1d1
systém	systém	k1gInSc1
zcela	zcela	k6eAd1
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Grande	grand	k1gMnSc5
Peur	Peura	k1gFnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
