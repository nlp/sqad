<s>
Listopad	listopad	k1gInSc1	listopad
je	být	k5eAaImIp3nS	být
jedenáctý	jedenáctý	k4xOgInSc4	jedenáctý
měsíc	měsíc	k1gInSc4	měsíc
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
padání	padání	k1gNnSc2	padání
listí	listí	k1gNnSc2	listí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
roční	roční	k2eAgFnSc4d1	roční
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
středoevropských	středoevropský	k2eAgFnPc6d1	středoevropská
přírodních	přírodní	k2eAgFnPc6d1	přírodní
podmínkách	podmínka	k1gFnPc6	podmínka
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
kalendáři	kalendář	k1gInSc6	kalendář
byl	být	k5eAaImAgInS	být
listopad	listopad	k1gInSc1	listopad
devátým	devátý	k4xOgInSc7	devátý
měsícem	měsíc	k1gInSc7	měsíc
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
november	novembra	k1gFnPc2	novembra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
novem	nov	k1gInSc7	nov
=	=	kIx~	=
devět	devět	k4xCc1	devět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
153	[number]	k4	153
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgInP	být
přidány	přidat	k5eAaPmNgInP	přidat
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
měsíce	měsíc	k1gInPc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
církevním	církevní	k2eAgInSc6d1	církevní
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
den	den	k1gInSc4	den
připadá	připadat	k5eAaImIp3nS	připadat
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
advent	advent	k1gInSc1	advent
začíná	začínat	k5eAaImIp3nS	začínat
poslední	poslední	k2eAgFnSc4d1	poslední
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
nebo	nebo	k8xC	nebo
první	první	k4xOgFnSc3	první
neděli	neděle	k1gFnSc3	neděle
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
státní	státní	k2eAgInSc1d1	státní
svátek	svátek	k1gInSc1	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Den	den	k1gInSc4	den
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
dle	dle	k7c2	dle
§	§	k?	§
1	[number]	k4	1
zákona	zákon	k1gInSc2	zákon
číslo	číslo	k1gNnSc4	číslo
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
listopad	listopad	k1gInSc1	listopad
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
