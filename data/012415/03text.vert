<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
John	John	k1gMnSc1	John
Cassidy	Cassida	k1gFnSc2	Cassida
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
Salem	Salem	k1gInSc1	Salem
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Navy	Navy	k?	Navy
SEALs	SEALs	k1gInSc1	SEALs
týmech	tým	k1gInPc6	tým
amerického	americký	k2eAgNnSc2d1	americké
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
od	od	k7c2	od
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
astronautem	astronaut	k1gMnSc7	astronaut
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
posádky	posádka	k1gFnSc2	posádka
raketoplánu	raketoplán	k1gInSc2	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
při	při	k7c6	při
letu	let	k1gInSc6	let
STS-127	STS-127	k1gFnSc2	STS-127
na	na	k7c4	na
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
stanici	stanice	k1gFnSc4	stanice
(	(	kIx(	(
<g/>
ISS	ISS	kA	ISS
<g/>
)	)	kIx)	)
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
letu	let	k1gInSc3	let
<g/>
,	,	kIx,	,
půlroční	půlroční	k2eAgFnSc6d1	půlroční
misi	mise	k1gFnSc6	mise
na	na	k7c4	na
ISS	ISS	kA	ISS
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Expedice	expedice	k1gFnSc2	expedice
35	[number]	k4	35
a	a	k8xC	a
36	[number]	k4	36
<g/>
,	,	kIx,	,
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
===	===	k?	===
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Cassidy	Cassida	k1gFnSc2	Cassida
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Salemu	Salem	k1gInSc6	Salem
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
rodné	rodný	k2eAgNnSc4d1	rodné
město	město	k1gNnSc4	město
považuje	považovat	k5eAaImIp3nS	považovat
York	York	k1gInSc1	York
v	v	k7c6	v
Maine	Main	k1gInSc5	Main
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
námořní	námořní	k2eAgFnSc6d1	námořní
přípravce	přípravka	k1gFnSc6	přípravka
v	v	k7c6	v
Newportu	Newport	k1gInSc6	Newport
a	a	k8xC	a
Námořní	námořní	k2eAgFnSc3d1	námořní
akademii	akademie	k1gFnSc3	akademie
(	(	kIx(	(
<g/>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Naval	navalit	k5eAaPmRp2nS	navalit
Academy	Academa	k1gFnSc2	Academa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
bakaláře	bakalář	k1gMnSc2	bakalář
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
akademii	akademie	k1gFnSc6	akademie
sloužil	sloužit	k5eAaImAgMnS	sloužit
ve	v	k7c6	v
vojenském	vojenský	k2eAgNnSc6d1	vojenské
námořnictvu	námořnictvo	k1gNnSc6	námořnictvo
v	v	k7c6	v
elitních	elitní	k2eAgFnPc6d1	elitní
jednotkách	jednotka	k1gFnPc6	jednotka
Navy	Navy	k?	Navy
SEALs	SEALs	k1gInSc1	SEALs
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
nasazen	nasadit	k5eAaPmNgInS	nasadit
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
a	a	k8xC	a
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
pokaždé	pokaždé	k6eAd1	pokaždé
na	na	k7c4	na
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Astronaut	astronaut	k1gMnSc1	astronaut
===	===	k?	===
</s>
</p>
<p>
<s>
Přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
se	se	k3xPyFc4	se
k	k	k7c3	k
19	[number]	k4	19
<g/>
.	.	kIx.	.
náboru	nábor	k1gInSc2	nábor
astronautů	astronaut	k1gMnPc2	astronaut
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
prošel	projít	k5eAaPmAgInS	projít
všemi	všecek	k3xTgInPc7	všecek
koly	kolo	k1gNnPc7	kolo
výběru	výběr	k1gInSc2	výběr
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
americké	americký	k2eAgMnPc4d1	americký
astronauty	astronaut	k1gMnPc4	astronaut
<g/>
.	.	kIx.	.
</s>
<s>
Zahájil	zahájit	k5eAaPmAgInS	zahájit
základní	základní	k2eAgInSc1d1	základní
kosmonautický	kosmonautický	k2eAgInSc1d1	kosmonautický
výcvik	výcvik	k1gInSc1	výcvik
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2006	[number]	k4	2006
získal	získat	k5eAaPmAgMnS	získat
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
"	"	kIx"	"
<g/>
letový	letový	k2eAgMnSc1d1	letový
specialista	specialista	k1gMnSc1	specialista
<g/>
"	"	kIx"	"
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
NASA	NASA	kA	NASA
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
Cassidyho	Cassidy	k1gMnSc2	Cassidy
jmenování	jmenování	k1gNnSc1	jmenování
do	do	k7c2	do
posádky	posádka	k1gFnSc2	posádka
letu	let	k1gInSc2	let
STS-127	STS-127	k1gMnSc2	STS-127
plánovaného	plánovaný	k2eAgMnSc2d1	plánovaný
na	na	k7c4	na
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
raketoplánu	raketoplán	k1gInSc2	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
mise	mise	k1gFnSc2	mise
byla	být	k5eAaImAgFnS	být
dokončení	dokončení	k1gNnSc4	dokončení
montáže	montáž	k1gFnSc2	montáž
modulu	modul	k1gInSc2	modul
Kibó	Kibó	k1gFnSc1	Kibó
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
zásob	zásoba	k1gFnPc2	zásoba
a	a	k8xC	a
vybavení	vybavení	k1gNnSc2	vybavení
na	na	k7c4	na
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
stanici	stanice	k1gFnSc4	stanice
(	(	kIx(	(
<g/>
ISS	ISS	kA	ISS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
trval	trvat	k5eAaImAgInS	trvat
15	[number]	k4	15
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letu	let	k1gInSc2	let
třikrát	třikrát	k6eAd1	třikrát
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
stanice	stanice	k1gFnSc2	stanice
k	k	k7c3	k
montážním	montážní	k2eAgFnPc3d1	montážní
pracím	práce	k1gFnPc3	práce
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
vycházky	vycházka	k1gFnPc1	vycházka
trvaly	trvat	k5eAaImAgFnP	trvat
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2010	[number]	k4	2010
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
posádky	posádka	k1gFnSc2	posádka
Expedic	expedice	k1gFnPc2	expedice
35	[number]	k4	35
a	a	k8xC	a
36	[number]	k4	36
na	na	k7c4	na
ISS	ISS	kA	ISS
s	s	k7c7	s
plánovaným	plánovaný	k2eAgInSc7d1	plánovaný
startem	start	k1gInSc7	start
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
posádka	posádka	k1gFnSc1	posádka
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
záložní	záložní	k2eAgInSc1d1	záložní
pro	pro	k7c4	pro
Expedici	expedice	k1gFnSc4	expedice
33	[number]	k4	33
<g/>
/	/	kIx~	/
<g/>
34	[number]	k4	34
startující	startující	k2eAgFnSc2d1	startující
v	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
jako	jako	k8xS	jako
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
lodi	loď	k1gFnSc2	loď
Sojuz	Sojuz	k1gInSc1	Sojuz
TMA-08M	TMA-08M	k1gFnSc2	TMA-08M
společně	společně	k6eAd1	společně
s	s	k7c7	s
Pavlem	Pavel	k1gMnSc7	Pavel
Vinogradovem	Vinogradovo	k1gNnSc7	Vinogradovo
a	a	k8xC	a
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Misurkinem	Misurkin	k1gMnSc7	Misurkin
<g/>
,	,	kIx,	,
po	po	k7c6	po
šestihodinovém	šestihodinový	k2eAgInSc6d1	šestihodinový
letu	let	k1gInSc6	let
se	se	k3xPyFc4	se
připojili	připojit	k5eAaPmAgMnP	připojit
k	k	k7c3	k
ISS	ISS	kA	ISS
a	a	k8xC	a
zahájili	zahájit	k5eAaPmAgMnP	zahájit
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
Expedici	expedice	k1gFnSc6	expedice
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
Expedic	expedice	k1gFnPc2	expedice
35	[number]	k4	35
a	a	k8xC	a
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
do	do	k7c2	do
otevřeného	otevřený	k2eAgInSc2d1	otevřený
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
výstupy	výstup	k1gInPc1	výstup
trvaly	trvat	k5eAaImAgInP	trvat
5	[number]	k4	5
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
32	[number]	k4	32
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgInSc1	třetí
výstup	výstup	k1gInSc1	výstup
byl	být	k5eAaImAgInS	být
zkrácen	zkrátit	k5eAaPmNgInS	zkrátit
kvůli	kvůli	k7c3	kvůli
vodě	voda	k1gFnSc3	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
skafandru	skafandr	k1gInSc6	skafandr
Luca	Lucus	k1gMnSc2	Lucus
Parmitana	Parmitan	k1gMnSc2	Parmitan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
posádka	posádka	k1gFnSc1	posádka
Sojuzu	Sojuz	k1gInSc2	Sojuz
TMA-08M	TMA-08M	k1gFnSc1	TMA-08M
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
<g/>
Christopher	Christophra	k1gFnPc2	Christophra
Cassidy	Cassida	k1gFnSc2	Cassida
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Christopher	Christophra	k1gFnPc2	Christophra
Cassidy	Cassida	k1gFnSc2	Cassida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Biographical	Biographicat	k5eAaPmAgMnS	Biographicat
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
J.	J.	kA	J.
Cassidy	Cassida	k1gFnSc2	Cassida
(	(	kIx(	(
<g/>
Commander	Commander	k1gMnSc1	Commander
<g/>
,	,	kIx,	,
USN	USN	kA	USN
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2013-03	[number]	k4	2013-03
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
