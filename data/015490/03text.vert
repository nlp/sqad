<s>
Alojz	Alojz	k1gInSc1
Rebula	Rebulum	k1gNnSc2
</s>
<s>
Alojz	Alojz	k1gMnSc1
Rebula	Rebul	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1924	#num#	k4
San	San	k1gMnSc7
Pelagio	Pelagio	k6eAd1
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
94	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Topolšica	Topolšic	k2eAgFnSc1d1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
infarkt	infarkt	k1gInSc4
myokardu	myokard	k1gInSc2
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Loka	Lok	k2eAgFnSc1d1
pri	pri	k?
Zidanem	Zidan	k1gInSc7
Mostu	most	k1gInSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
jazykovědec	jazykovědec	k1gMnSc1
<g/>
,	,	kIx,
překladatel	překladatel	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
dramatik	dramatik	k1gMnSc1
<g/>
,	,	kIx,
esejista	esejista	k1gMnSc1
<g/>
,	,	kIx,
dramaturg	dramaturg	k1gMnSc1
<g/>
,	,	kIx,
filolog	filolog	k1gMnSc1
<g/>
,	,	kIx,
biblical	biblicat	k5eAaPmAgMnS
scholar	scholar	k1gMnSc1
a	a	k8xC
básník	básník	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
slovinská	slovinský	k2eAgFnSc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Lublani	Lublaň	k1gFnSc6
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Zora	Zora	k1gFnSc1
Tavčarová	Tavčarová	k1gFnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Alenka	Alenka	k1gFnSc1
Rebula	Rebula	k1gFnSc1
Tuta	Tuta	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alojz	Alojz	k1gInSc1
Rebula	Rebulum	k1gNnSc2
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1924	#num#	k4
San	San	k1gMnSc1
Pelagio	Pelagio	k1gMnSc1
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
slovinský	slovinský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
dramaturg	dramaturg	k1gMnSc1
<g/>
,	,	kIx,
esejista	esejista	k1gMnSc1
a	a	k8xC
překladatel	překladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Alojz	Alojz	k1gInSc1
Rebula	Rebulum	k1gNnSc2
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
ve	v	k7c6
Slovinci	Slovinec	k1gMnSc6
obydlené	obydlený	k2eAgFnSc2d1
vesničce	vesnička	k1gFnSc3
San	San	k1gMnPc7
Pelagio	Pelagio	k6eAd1
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
<g/>
:	:	kIx,
Šempolaj	Šempolaj	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
zde	zde	k6eAd1
sídlila	sídlit	k5eAaImAgFnS
antislavistická	antislavistický	k2eAgFnSc1d1
italská	italský	k2eAgFnSc1d1
policie	policie	k1gFnSc1
fašistického	fašistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
nemohl	moct	k5eNaImAgMnS
studovat	studovat	k5eAaImF
v	v	k7c6
rodném	rodný	k2eAgInSc6d1
jazyce	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštěvoval	navštěvovat	k5eAaImAgMnS
italsky	italsky	k6eAd1
mluvící	mluvící	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
italskou	italský	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
a	a	k8xC
literaturou	literatura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
navštěvoval	navštěvovat	k5eAaImAgMnS
klasické	klasický	k2eAgNnSc4d1
gymnázium	gymnázium	k1gNnSc4
v	v	k7c6
Gorici	Gorice	k1gFnSc6
mezi	mezi	k7c7
lety	let	k1gInPc7
1936	#num#	k4
až	až	k9
1940	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1940	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
<g/>
,	,	kIx,
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
tehdejším	tehdejší	k2eAgMnSc6d1
Vidmu	Vidmu	k?
(	(	kIx(
<g/>
moderně	moderna	k1gFnSc3
Udine	Udin	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
také	také	k9
maturoval	maturovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
konci	konec	k1gInSc6
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc6d1
válka	válka	k1gFnSc1
se	se	k3xPyFc4
odstěhoval	odstěhovat	k5eAaPmAgMnS
do	do	k7c2
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1949	#num#	k4
vystudoval	vystudovat	k5eAaPmAgInS
klasickou	klasický	k2eAgFnSc4d1
filologii	filologie	k1gFnSc4
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Lublani	Lublaň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
se	se	k3xPyFc4
odstěhoval	odstěhovat	k5eAaPmAgInS
zpět	zpět	k6eAd1
do	do	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
tlakům	tlak	k1gInPc3
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
měl	mít	k5eAaImAgInS
zakázán	zakázán	k2eAgInSc1d1
vstup	vstup	k1gInSc1
do	do	k7c2
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
důvodu	důvod	k1gInSc2
nesouhlasu	nesouhlas	k1gInSc2
ke	k	k7c3
komunistickému	komunistický	k2eAgInSc3d1
režimu	režim	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1960	#num#	k4
v	v	k7c6
Římě	Řím	k1gInSc6
dokončil	dokončit	k5eAaPmAgMnS
doktorandské	doktorandský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc1
téma	téma	k1gNnSc1
bylo	být	k5eAaImAgNnS
Dante	Dante	k1gMnSc1
ve	v	k7c6
slovinských	slovinský	k2eAgInPc6d1
překladech	překlad	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentýž	týž	k3xTgInSc4
rok	rok	k1gInSc4
mu	on	k3xPp3gMnSc3
podruhé	podruhé	k6eAd1
zakázali	zakázat	k5eAaPmAgMnP
vstup	vstup	k1gInSc4
do	do	k7c2
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
veřejně	veřejně	k6eAd1
protestoval	protestovat	k5eAaBmAgMnS
v	v	k7c6
Terstských	terstský	k2eAgFnPc6d1
novinách	novina	k1gFnPc6
Listina	listina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
žil	žít	k5eAaImAgMnS
Rebula	Rebulum	k1gNnSc2
v	v	k7c6
Terstu	Terst	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
učitel	učitel	k1gMnSc1
latinského	latinský	k2eAgMnSc2d1
a	a	k8xC
řeckého	řecký	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
na	na	k7c6
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
s	s	k7c7
vyučováním	vyučování	k1gNnSc7
slovinského	slovinský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
kulturní	kulturní	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
s	s	k7c7
místní	místní	k2eAgFnSc7d1
slovinskou	slovinský	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
Borisem	Boris	k1gMnSc7
Pahorem	Pahorem	k?
<g/>
,	,	kIx,
editovali	editovat	k5eAaImAgMnP
časopis	časopis	k1gInSc4
Zaliv	zalít	k5eAaPmDgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
koeditorem	koeditor	k1gMnSc7
literárního	literární	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
Sidro	Sidro	k1gNnSc1
<g/>
,	,	kIx,
Tokovi	Toka	k1gMnSc3
<g/>
,	,	kIx,
Most	most	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
<g/>
,	,	kIx,
Pahor	Pahor	k?
a	a	k8xC
Rebula	Rebulum	k1gNnSc2
zveřejnili	zveřejnit	k5eAaPmAgMnP
knihu	kniha	k1gFnSc4
rozhovorů	rozhovor	k1gInPc2
napsanou	napsaný	k2eAgFnSc4d1
Edvardem	Edvard	k1gMnSc7
Kocbekem	Kocbek	k1gInSc7
<g/>
:	:	kIx,
Pričevalec	Pričevalec	k1gMnSc1
naseta	naset	k2eAgNnPc4d1
časa	časum	k1gNnPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
Rebula	Rebula	k1gFnSc1
odsuzuje	odsuzovat	k5eAaImIp3nS
zabití	zabití	k1gNnSc4
dvanáctitisícové	dvanáctitisícový	k2eAgFnSc2d1
slovinské	slovinský	k2eAgFnSc2d1
antikomunistické	antikomunistický	k2eAgFnSc2d1
milice	milice	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
květnu	květen	k1gInSc6
a	a	k8xC
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
1945	#num#	k4
<g/>
,	,	kIx,
komunistickými	komunistický	k2eAgInPc7d1
úřady	úřad	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
vyvolala	vyvolat	k5eAaPmAgFnS
v	v	k7c6
Jugoslávii	Jugoslávie	k1gFnSc6
skandál	skandál	k1gInSc1
a	a	k8xC
jak	jak	k8xC,k8xS
Pahor	Pahor	k?
<g/>
,	,	kIx,
tak	tak	k6eAd1
Rebula	Rebula	k1gFnSc1
nemohli	moct	k5eNaImAgMnP
několik	několik	k4yIc1
let	léto	k1gNnPc2
do	do	k7c2
Jugoslávie	Jugoslávie	k1gFnSc2
vstoupit	vstoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
demokratizace	demokratizace	k1gFnSc2
a	a	k8xC
osamostatnění	osamostatnění	k1gNnSc4
Slovinska	Slovinsko	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
letech	léto	k1gNnPc6
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
<g/>
,	,	kIx,
Rebula	Rebulum	k1gNnSc2
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
fejetonista	fejetonista	k1gMnSc1
v	v	k7c6
několika	několik	k4yIc6
katolických	katolický	k2eAgInPc6d1
časopisech	časopis	k1gInPc6
ve	v	k7c6
Slovinsku	Slovinsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žil	žíla	k1gFnPc2
a	a	k8xC
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
rodné	rodný	k2eAgFnSc6d1
vesničce	vesnička	k1gFnSc6
v	v	k7c6
italské	italský	k2eAgFnSc6d1
části	část	k1gFnSc6
regionu	region	k1gInSc2
Kras	Kras	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Literární	literární	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
Alojz	Alojz	k1gInSc1
Rebula	Rebulum	k1gNnSc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejdůležitější	důležitý	k2eAgMnPc4d3
slovinské	slovinský	k2eAgMnPc4d1
autory	autor	k1gMnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
především	především	k6eAd1
důležitým	důležitý	k2eAgMnSc7d1
terstským	terstský	k2eAgMnSc7d1
spisovatelem	spisovatel	k1gMnSc7
<g/>
,	,	kIx,
stylem	styl	k1gInSc7
realistický	realistický	k2eAgMnSc1d1
filozofický	filozofický	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
prózy	próza	k1gFnSc2
inspirovaný	inspirovaný	k2eAgInSc1d1
symbolickými	symbolický	k2eAgInPc7d1
a	a	k8xC
expresionistickými	expresionistický	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgNnSc1
vypravování	vypravování	k1gNnSc1
proplétá	proplétat	k5eAaImIp3nS
s	s	k7c7
esejovým	esejový	k2eAgNnSc7d1
diskutováním	diskutování	k1gNnSc7
a	a	k8xC
rozjímáním	rozjímání	k1gNnSc7
o	o	k7c6
národních	národní	k2eAgFnPc6d1
<g/>
,	,	kIx,
morálních	morální	k2eAgInPc2d1
<g/>
,	,	kIx,
zamilovaných	zamilovaný	k2eAgInPc2d1
<g/>
,	,	kIx,
uměleckých	umělecký	k2eAgInPc2d1
a	a	k8xC
politických	politický	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
zahrnul	zahrnout	k5eAaPmAgInS
zvláště	zvláště	k6eAd1
poválečnou	poválečný	k2eAgFnSc4d1
problematiku	problematika	k1gFnSc4
slovinských	slovinský	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
v	v	k7c6
Terstu	Terst	k1gInSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
strastiplného	strastiplný	k2eAgInSc2d1
a	a	k8xC
trpkého	trpký	k2eAgInSc2d1
života	život	k1gInSc2
venkovského	venkovský	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
vzdělanců	vzdělanec	k1gMnPc2
a	a	k8xC
mládeže	mládež	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
starší	starý	k2eAgFnPc4d2
generace	generace	k1gFnPc4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgInS
jako	jako	k9
dostatečně	dostatečně	k6eAd1
sofistikovaný	sofistikovaný	k2eAgMnSc1d1
pozorovatel	pozorovatel	k1gMnSc1
a	a	k8xC
poutavý	poutavý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
trpké	trpký	k2eAgFnSc2d1
krásy	krása	k1gFnSc2
pastvin	pastvina	k1gFnPc2
a	a	k8xC
lesů	les	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známý	známý	k2eAgMnSc1d1
svými	svůj	k3xOyFgInPc7
historickými	historický	k2eAgInPc7d1
romány	román	k1gInPc7
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgMnPc6,k3yRgMnPc6,k3yQgMnPc6
v	v	k7c6
dalekém	daleký	k2eAgInSc6d1
čase	čas	k1gInSc6
ukázal	ukázat	k5eAaPmAgMnS
soudobý	soudobý	k2eAgInSc4d1
svět	svět	k1gInSc4
a	a	k8xC
rozměr	rozměr	k1gInSc4
<g/>
,	,	kIx,
také	také	k9
s	s	k7c7
pomocí	pomoc	k1gFnSc7
hlubokého	hluboký	k2eAgInSc2d1
psychologického	psychologický	k2eAgInSc2d1
portrétu	portrét	k1gInSc2
svých	svůj	k3xOyFgMnPc2
hrdinů	hrdina	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svých	svůj	k3xOyFgInPc6
dílech	díl	k1gInPc6
spojuje	spojovat	k5eAaImIp3nS
témata	téma	k1gNnPc1
antická	antický	k2eAgFnSc1d1
<g/>
,	,	kIx,
křesťanská	křesťanský	k2eAgFnSc1d1
a	a	k8xC
slovinská	slovinský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Uveřejnil	uveřejnit	k5eAaPmAgMnS
mnoho	mnoho	k4c4
sbírek	sbírka	k1gFnPc2
esejí	esej	k1gFnPc2
<g/>
,	,	kIx,
deníků	deník	k1gInPc2
<g/>
,	,	kIx,
románů	román	k1gInPc2
<g/>
,	,	kIx,
dramat	drama	k1gNnPc2
<g/>
,	,	kIx,
krátké	krátký	k2eAgFnSc2d1
prózy	próza	k1gFnSc2
a	a	k8xC
jiných	jiný	k2eAgNnPc2d1
inspirovaných	inspirovaný	k2eAgNnPc2d1
publicistických	publicistický	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
přeložena	přeložit	k5eAaPmNgFnS
do	do	k7c2
několika	několik	k4yIc2
cizích	cizí	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
své	svůj	k3xOyFgInPc4
úspěchy	úspěch	k1gInPc4
byl	být	k5eAaImAgInS
odměněn	odměnit	k5eAaPmNgInS
mnoha	mnoho	k4c7
cenami	cena	k1gFnPc7
<g/>
;	;	kIx,
roku	rok	k1gInSc2
1995	#num#	k4
–	–	k?
Prešernova	Prešernův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
za	za	k7c4
literární	literární	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1997	#num#	k4
–	–	k?
mezinárodní	mezinárodní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Acerbi	Acerb	k1gFnSc2
za	za	k7c4
italské	italský	k2eAgInPc4d1
překlady	překlad	k1gInPc4
jeho	jeho	k3xOp3gInPc2
románů	román	k1gInPc2
V	v	k7c6
sibilinem	sibilin	k1gInSc7
vetru	vetru	k?
<g/>
,	,	kIx,
a	a	k8xC
roku	rok	k1gInSc2
2005	#num#	k4
–	–	k?
cena	cena	k1gFnSc1
kresnik	kresnik	k1gInSc4
za	za	k7c4
román	román	k1gInSc4
Nokturno	nokturno	k1gNnSc4
za	za	k7c4
Primorsko	Primorsko	k1gNnSc4
jako	jako	k8xC,k8xS
nejúspěšnější	úspěšný	k2eAgInSc4d3
román	román	k1gInSc4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vybraná	vybraný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Romány	román	k1gInPc1
</s>
<s>
Zvonovi	Zvonův	k2eAgMnPc1d1
Nilandije	Nilandije	k1gMnPc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Nokturno	nokturno	k1gNnSc1
za	za	k7c4
Primorsko	Primorsko	k1gNnSc4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jutranjice	Jutranjice	k1gFnSc1
za	za	k7c4
Slovenijo	Slovenijo	k1gNnSc4
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Cesta	cesta	k1gFnSc1
s	s	k7c7
cipreso	cipresa	k1gFnSc5
in	in	k?
zvezdo	zvezdo	k1gNnSc4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Duh	duh	k1gInSc1
velikih	velikih	k1gInSc1
jezer	jezero	k1gNnPc2
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Maranathà	Maranathà	k?
ali	ali	k?
leto	leto	k1gNnSc1
999	#num#	k4
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
přeloženo	přeložen	k2eAgNnSc1d1
do	do	k7c2
němčiny	němčina	k1gFnSc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kačja	Kačj	k2eAgFnSc1d1
roža	roža	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
přeloženo	přeložen	k2eAgNnSc1d1
do	do	k7c2
italštiny	italština	k1gFnSc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jutri	Jutri	k6eAd1
čez	čez	k?
Jordan	Jordan	k1gMnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
přeloženo	přeložen	k2eAgNnSc1d1
do	do	k7c2
francouzštiny	francouzština	k1gFnSc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zeleno	zeleno	k1gNnSc1
izgnanstvo	izgnanstvo	k1gNnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Snegovi	Snegův	k2eAgMnPc1d1
Edena	Edeno	k1gNnPc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Divji	Divt	k5eAaPmIp1nS
golob	goloba	k1gFnPc2
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
Sibilinem	Sibilin	k1gInSc7
vetru	vetru	k?
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
přeloženo	přeložen	k2eAgNnSc1d1
do	do	k7c2
italštiny	italština	k1gFnSc2
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Senčni	Senčnit	k5eAaPmRp2nS
ples	ples	k1gInSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
přeloženo	přeložen	k2eAgNnSc1d1
do	do	k7c2
srbochorvatštiny	srbochorvatština	k1gFnSc2
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Klic	Klic	k6eAd1
v	v	k7c6
Sredozemlje	Sredozemlje	k1gFnSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vinograd	Vinograd	k1gInSc1
rimske	rimskat	k5eAaPmIp3nS
cesarice	cesarika	k1gFnSc3
(	(	kIx(
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Devinski	Devinski	k1gNnSc1
sholar	sholara	k1gFnPc2
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Drama	drama	k1gNnSc1
</s>
<s>
Operacija	Operacij	k2eAgFnSc1d1
Timava	Timava	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Savlov	Savlov	k1gInSc1
demon	demon	k1gInSc1
–	–	k?
šest	šest	k4xCc4
her	hra	k1gFnPc2
s	s	k7c7
religiozní	religiozní	k2eAgFnSc7d1
tematikou	tematika	k1gFnSc7
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hribi	Hribi	k6eAd1
<g/>
,	,	kIx,
pokrijte	pokrít	k5eAaPmRp2nP
nas	nas	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eseje	esej	k1gFnPc1
</s>
<s>
Smer	Smer	k1gMnSc1
Nova	novum	k1gNnSc2
zemlja	zemlja	k1gMnSc1
</s>
<s>
Ob	Ob	k1gInSc1
slovenskem	slovensko	k1gNnSc7
poldnevniku	poldnevnik	k1gInSc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Deníky	deník	k1gInPc1
</s>
<s>
Iz	Iz	k?
partiture	partitur	k1gMnSc5
življenja	življenj	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnevnik	Dnevnik	k1gInSc1
1977	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gorje	Gorje	k1gFnSc1
zelenemu	zelenem	k1gInSc2
drevesu	drevést	k5eAaPmIp1nS
</s>
<s>
Oblaki	Oblaki	k6eAd1
Michigana	Michigana	k1gFnSc1
</s>
<s>
Vrt	vrt	k1gInSc1
bogov	bogov	k1gInSc1
</s>
<s>
Koraki	Koraki	k6eAd1
apostolskih	apostolskih	k1gMnSc1
sandal	sandat	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Previsna	Previsen	k2eAgFnSc1d1
leta	leta	k1gFnSc1
</s>
<s>
Ko	Ko	k?
proti	proti	k7c3
jutru	jutro	k1gNnSc3
gre	gre	k?
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1
literární	literární	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Arhipel	Arhipel	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Intervjuji	Intervjovat	k5eAaPmIp1nS,k5eAaImIp1nS,k5eAaBmIp1nS
o	o	k7c6
prihajajočem	prihajajoč	k1gInSc7
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pastir	Pastir	k1gInSc1
prihodnosti	prihodnost	k1gFnSc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pričevalci	Pričevalec	k1gMnPc1
vstajenja	vstajenja	k6eAd1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
přeloženo	přeložen	k2eAgNnSc1d1
do	do	k7c2
angličtiny	angličtina	k1gFnSc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Severni	Severnit	k5eAaPmRp2nS
križ	križ	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Alojz	Alojz	k1gInSc4
Rebula	Rebulum	k1gNnSc2
na	na	k7c6
slovinské	slovinský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alojz	Alojza	k1gFnPc2
Rebula	Rebula	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
slovinsky	slovinsky	k6eAd1
<g/>
)	)	kIx)
Pomembne	Pomembne	k1gFnSc1
osebnosti	osebnost	k1gFnSc2
<g/>
,	,	kIx,
Alojz	Alojz	k1gMnSc1
Rebula	Rebula	k1gFnSc1
<g/>
,	,	kIx,
Kras	Kras	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
20020925020	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
120166798	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0918	#num#	k4
6735	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85830001	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
79044094	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85830001	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
|	|	kIx~
Slovinsko	Slovinsko	k1gNnSc1
</s>
