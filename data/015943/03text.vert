<s>
Kohoutí	kohoutí	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Kohoutí	kohoutí	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
Wojna	wojna	k1gFnSc1
kokosza	kokosz	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
vzpouru	vzpoura	k1gFnSc4
polské	polský	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
proti	proti	k7c3
králi	král	k1gMnSc3
Zikmundovi	Zikmund	k1gMnSc3
v	v	k7c6
době	doba	k1gFnSc6
tažení	tažení	k1gNnSc2
do	do	k7c2
Multánska	Multánsko	k1gNnSc2
roku	rok	k1gInSc2
1537	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
kralování	kralování	k1gNnSc4
Zikmunda	Zikmund	k1gMnSc2
I.	I.	kA
Starého	Starý	k1gMnSc2
začaly	začít	k5eAaPmAgInP
v	v	k7c6
Polsku	Polsko	k1gNnSc6
první	první	k4xOgFnSc2
snahy	snaha	k1gFnSc2
o	o	k7c4
centralistickou	centralistický	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
projevující	projevující	k2eAgFnSc4d1
se	se	k3xPyFc4
jak	jak	k6eAd1
reformou	reforma	k1gFnSc7
armády	armáda	k1gFnSc2
(	(	kIx(
<g/>
stálé	stálý	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
reformou	reforma	k1gFnSc7
fiskální	fiskální	k2eAgInSc4d1
(	(	kIx(
<g/>
pevná	pevný	k2eAgFnSc1d1
berní	berní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
nelíbilo	líbit	k5eNaImAgNnS
šlechtě	šlechta	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
roku	rok	k1gInSc2
1537	#num#	k4
využila	využít	k5eAaPmAgFnS
Zikmundovy	Zikmundův	k2eAgFnPc4d1
snahy	snaha	k1gFnPc4
dobýt	dobýt	k5eAaPmF
Multánsko	Multánsko	k1gNnSc4
a	a	k8xC
ve	v	k7c6
vojenském	vojenský	k2eAgNnSc6d1
ležení	ležení	k1gNnSc6
poblíž	poblíž	k7c2
Lvova	Lvov	k1gInSc2
se	se	k3xPyFc4
namísto	namísto	k7c2
proti	proti	k7c3
nepřátelskému	přátelský	k2eNgNnSc3d1
území	území	k1gNnSc3
obrátila	obrátit	k5eAaPmAgFnS
proti	proti	k7c3
svému	svůj	k3xOyFgMnSc3
králi	král	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
(	(	kIx(
<g/>
či	či	k8xC
spíše	spíše	k9
vzpoura	vzpoura	k1gFnSc1
<g/>
)	)	kIx)
nepřinesla	přinést	k5eNaPmAgFnS
pro	pro	k7c4
šlechtu	šlechta	k1gFnSc4
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
základem	základ	k1gInSc7
politických	politický	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
Polsko	Polsko	k1gNnSc1
přivedly	přivést	k5eAaPmAgFnP
až	až	k6eAd1
k	k	k7c3
ustanovení	ustanovení	k1gNnSc3
šlechtického	šlechtický	k2eAgNnSc2d1
voleného	volený	k2eAgNnSc2d1
království	království	k1gNnSc2
(	(	kIx(
<g/>
Rzeczpospolita	Rzeczpospolita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Kohoutí	kohoutí	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
CoJeCo	CoJeCo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
