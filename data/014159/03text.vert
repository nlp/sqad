<s>
Transgas	Transgas	k1gInSc1
(	(	kIx(
<g/>
budova	budova	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Transgas	Transgas	k1gInSc1
Administrativní	administrativní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Transgasu	Transgas	k1gInSc2
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
Brutalismus	brutalismus	k1gInSc4
Architekti	architekt	k1gMnPc1
</s>
<s>
Václav	Václav	k1gMnSc1
Aulický	Aulický	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Eisenreich	Eisenreich	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
Loos	Loos	k1gInSc1
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
Malátek	Malátek	k1gInSc1
<g/>
,	,	kIx,
spolupráce	spolupráce	k1gFnSc1
Jan	Jan	k1gMnSc1
Fišer	Fišer	k1gMnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
Zánik	zánik	k1gInSc1
</s>
<s>
počátek	počátek	k1gInSc1
demolice	demolice	k1gFnSc2
2019	#num#	k4
(	(	kIx(
<g/>
dokončena	dokončen	k2eAgFnSc1d1
2020	#num#	k4
<g/>
)	)	kIx)
Materiály	materiál	k1gInPc4
</s>
<s>
beton	beton	k1gInSc1
<g/>
,	,	kIx,
ocel	ocel	k1gFnSc4
<g/>
,	,	kIx,
sklo	sklo	k1gNnSc4
<g/>
,	,	kIx,
žulové	žulový	k2eAgFnPc4d1
dlažební	dlažební	k2eAgFnPc4d1
kostky	kostka	k1gFnPc4
Současný	současný	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
</s>
<s>
Phibell	Phibell	k1gInSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
(	(	kIx(
<g/>
HB	HB	kA
Reavis	Reavis	k1gFnSc1
<g/>
)	)	kIx)
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Vinohradská	vinohradský	k2eAgFnSc1d1
8	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
-Vinohrady	-Vinohrad	k1gInPc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Vinohradská	vinohradský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Římská	římský	k2eAgFnSc1d1
a	a	k8xC
Rubešova	Rubešův	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
43,23	43,23	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
0,71	0,71	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
Transgas	Transgasa	k1gFnPc2
(	(	kIx(
<g/>
Centrální	centrální	k2eAgInSc1d1
dispečink	dispečink	k1gInSc1
Transgas	Transgasa	k1gFnPc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
Transgaz	Transgaz	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Plynárenské	plynárenský	k2eAgNnSc1d1
řídící	řídící	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
dříve	dříve	k6eAd2
též	též	k9
Ústřední	ústřední	k2eAgInSc1d1
dispečink	dispečink	k1gInSc1
tranzitního	tranzitní	k2eAgInSc2d1
plynovodu	plynovod	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
Federální	federální	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
paliv	palivo	k1gNnPc2
a	a	k8xC
energetiky	energetika	k1gFnSc2
byl	být	k5eAaImAgInS
komplex	komplex	k1gInSc1
tří	tři	k4xCgFnPc2
budov	budova	k1gFnPc2
postavených	postavený	k2eAgFnPc2d1
v	v	k7c6
letech	léto	k1gNnPc6
1972	#num#	k4
až	až	k9
1978	#num#	k4
v	v	k7c6
brutalistickém	brutalistický	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
demolice	demolice	k1gFnSc1
započala	započnout	k5eAaPmAgFnS
v	v	k7c6
březnu	březen	k1gInSc6
2019	#num#	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplex	komplex	k1gInSc1
stál	stát	k5eAaImAgInS
na	na	k7c6
Vinohradské	vinohradský	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
sídla	sídlo	k1gNnSc2
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
2	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
adresa	adresa	k1gFnSc1
byla	být	k5eAaImAgFnS
Římská	římský	k2eAgFnSc1d1
325	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Vinohradská	vinohradský	k2eAgFnSc1d1
325	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadní	zadní	k2eAgFnPc1d1
části	část	k1gFnPc1
objektu	objekt	k1gInSc2
byly	být	k5eAaImAgFnP
ohraničeny	ohraničit	k5eAaPmNgInP
ulicemi	ulice	k1gFnPc7
Rubešova	Rubešův	k2eAgNnPc1d1
a	a	k8xC
Římská	římský	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budovách	budova	k1gFnPc6
sídlily	sídlit	k5eAaImAgFnP
také	také	k6eAd1
další	další	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
(	(	kIx(
<g/>
Světová	světový	k2eAgFnSc1d1
odborová	odborový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
a	a	k8xC
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
období	období	k1gNnSc6
také	také	k9
centrála	centrála	k1gFnSc1
a	a	k8xC
klientské	klientský	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Všeobecné	všeobecný	k2eAgFnSc2d1
zdravotní	zdravotní	k2eAgFnSc2d1
pojišťovny	pojišťovna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgNnSc4
pojmenování	pojmenování	k1gNnSc4
získala	získat	k5eAaPmAgFnS
budova	budova	k1gFnSc1
podle	podle	k7c2
plynovodu	plynovod	k1gInSc2
Transgas	Transgasa	k1gFnPc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
kterému	který	k3yRgInSc3,k3yIgInSc3,k3yQgInSc3
tyto	tento	k3xDgFnPc1
stavby	stavba	k1gFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autory	autor	k1gMnPc7
tohoto	tento	k3xDgInSc2
komplexu	komplex	k1gInSc2
byli	být	k5eAaImAgMnP
Václav	Václav	k1gMnSc1
Aulický	Aulický	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Eisenreich	Eisenreich	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
Loos	Loos	k1gInSc1
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
Malátek	Malátek	k1gInSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Janem	Jan	k1gMnSc7
Fišerem	Fišer	k1gMnSc7
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgMnSc7d1
konstruktérem	konstruktér	k1gMnSc7
byl	být	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
Kozák	Kozák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Transgas	Transgasa	k1gFnPc2
</s>
<s>
Celý	celý	k2eAgInSc1d1
areál	areál	k1gInSc1
byl	být	k5eAaImAgInS
kompozicí	kompozice	k1gFnSc7
různých	různý	k2eAgFnPc2d1
stavebních	stavební	k2eAgFnPc2d1
forem	forma	k1gFnPc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
sledují	sledovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gInSc4
různý	různý	k2eAgInSc4d1
účel	účel	k1gInSc4
využití	využití	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplex	komplex	k1gInSc1
<g/>
,	,	kIx,
sjednocující	sjednocující	k2eAgNnSc1d1
brutalistní	brutalistní	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
hmot	hmota	k1gFnPc2
i	i	k8xC
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
doplněné	doplněný	k2eAgInPc1d1
výrazně	výrazně	k6eAd1
technicistními	technicistní	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejradikálnějších	radikální	k2eAgInPc2d3
a	a	k8xC
nejkvalitnějších	kvalitní	k2eAgInPc2d3
projevů	projev	k1gInPc2
brutalismu	brutalismus	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
s	s	k7c7
rysy	rys	k1gInPc7
rané	raný	k2eAgFnSc2d1
postmoderny	postmoderna	k1gFnSc2
a	a	k8xC
industriálního	industriální	k2eAgInSc2d1
a	a	k8xC
high-tech	high-to	k1gNnPc6
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
architektonického	architektonický	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
bylo	být	k5eAaImAgNnS
použití	použití	k1gNnSc1
samorezivějící	samorezivějící	k2eAgFnSc2d1
oceli	ocel	k1gFnSc2
ATMOFIX	ATMOFIX	kA
na	na	k7c6
předsazené	předsazený	k2eAgFnSc6d1
hlavní	hlavní	k2eAgFnSc6d1
konstrukci	konstrukce	k1gFnSc6
a	a	k8xC
ukončující	ukončující	k2eAgNnSc1d1
dvoupatrové	dvoupatrový	k2eAgNnSc1d1
"	"	kIx"
<g/>
hlavice	hlavice	k1gFnSc1
<g/>
"	"	kIx"
obou	dva	k4xCgFnPc6
věžových	věžový	k2eAgFnPc2d1
budov	budova	k1gFnPc2
a	a	k8xC
v	v	k7c6
partii	partie	k1gFnSc6
obchodů	obchod	k1gInPc2
a	a	k8xC
uplatnění	uplatnění	k1gNnSc4
pohledového	pohledový	k2eAgInSc2d1
betonu	beton	k1gInSc2
v	v	k7c6
exteriéru	exteriér	k1gInSc6
i	i	k8xC
interiéru	interiér	k1gInSc6
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristické	charakteristický	k2eAgFnPc1d1
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
stavbu	stavba	k1gFnSc4
bylo	být	k5eAaImAgNnS
také	také	k9
například	například	k6eAd1
zábradlí	zábradlí	k1gNnSc4
z	z	k7c2
plynovodných	plynovodný	k2eAgFnPc2d1
trubek	trubka	k1gFnPc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
spojovací	spojovací	k2eAgInPc1d1
tunely	tunel	k1gInPc1
všech	všecek	k3xTgFnPc2
tří	tři	k4xCgFnPc2
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
spojením	spojení	k1gNnSc7
s	s	k7c7
dobovým	dobový	k2eAgNnSc7d1
architektonickým	architektonický	k2eAgNnSc7d1
děním	dění	k1gNnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
dispozici	dispozice	k1gFnSc3
měl	mít	k5eAaImAgMnS
70	#num#	k4
parkovacích	parkovací	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dispečink	dispečink	k1gInSc1
</s>
<s>
Budova	budova	k1gFnSc1
dispečinku	dispečink	k1gInSc2
s	s	k7c7
izolačním	izolační	k2eAgNnSc7d1
opláštěním	opláštění	k1gNnSc7
ze	z	k7c2
žulových	žulový	k2eAgFnPc2d1
dlažebních	dlažební	k2eAgFnPc2d1
kostek	kostka	k1gFnPc2
</s>
<s>
Dispečink	dispečink	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
nejdůležitější	důležitý	k2eAgFnSc1d3
budova	budova	k1gFnSc1
komplexu	komplex	k1gInSc2
<g/>
,	,	kIx,
dokončena	dokončen	k2eAgFnSc1d1
byl	být	k5eAaImAgInS
jako	jako	k9
první	první	k4xOgFnSc1
z	z	k7c2
celého	celý	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k6eAd1
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgInP
dva	dva	k4xCgInPc1
sálové	sálový	k2eAgInPc1d1
počítače	počítač	k1gInPc1
poháněné	poháněný	k2eAgInPc1d1
procesory	procesor	k1gInPc4
General	General	k1gMnPc2
Electric	Electric	k1gMnSc1
PAC	pac	k1gFnSc2
4010	#num#	k4
<g/>
,	,	kIx,
ty	ten	k3xDgInPc1
musely	muset	k5eAaImAgInP
být	být	k5eAaImF
komplikovaně	komplikovaně	k6eAd1
odděleny	oddělit	k5eAaPmNgInP
od	od	k7c2
vibrací	vibrace	k1gFnPc2
z	z	k7c2
Vinohradských	vinohradský	k2eAgInPc2d1
železničních	železniční	k2eAgInPc2d1
tunelů	tunel	k1gInPc2
a	a	k8xC
okolního	okolní	k2eAgInSc2d1
hluku	hluk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvádr	kvádr	k1gInSc1
má	mít	k5eAaImIp3nS
výšku	výška	k1gFnSc4
7,5	7,5	k4
<g/>
,	,	kIx,
délku	délka	k1gFnSc4
48	#num#	k4
a	a	k8xC
šířku	šířka	k1gFnSc4
24	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
těžkou	těžký	k2eAgFnSc4d1
izolační	izolační	k2eAgFnSc4d1
fasádu	fasáda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
sestavena	sestavit	k5eAaPmNgFnS
z	z	k7c2
18	#num#	k4
tisíc	tisíc	k4xCgInPc2
žulových	žulový	k2eAgFnPc2d1
dlažebních	dlažební	k2eAgFnPc2d1
kostek	kostka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Eisenreich	Eisenreich	k1gMnSc1
o	o	k7c6
fasádě	fasáda	k1gFnSc6
z	z	k7c2
dlažebních	dlažební	k2eAgFnPc2d1
kostek	kostka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
umožnila	umožnit	k5eAaPmAgFnS
dosáhnout	dosáhnout	k5eAaPmF
dojmu	dojem	k1gInSc3
letu	let	k1gInSc2
a	a	k8xC
vznosnosti	vznosnost	k1gFnSc2
<g/>
,	,	kIx,
pocitu	pocit	k1gInSc3
adekvátnímu	adekvátní	k2eAgInSc3d1
širokému	široký	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
technického	technický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
a	a	k8xC
stupni	stupeň	k1gInPc7
jeho	jeho	k3xOp3gFnSc2
schopnosti	schopnost	k1gFnSc2
ovládnout	ovládnout	k5eAaPmF
hmotu	hmota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Věžové	věžový	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
</s>
<s>
U	u	k7c2
ulic	ulice	k1gFnPc2
Římská	římský	k2eAgFnSc1d1
a	a	k8xC
Rubešova	Rubešův	k2eAgFnSc1d1
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
dvě	dva	k4xCgFnPc1
administrativní	administrativní	k2eAgFnPc1d1
devítipatrové	devítipatrový	k2eAgFnPc1d1
věžové	věžový	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
původních	původní	k2eAgInPc2d1
návrhů	návrh	k1gInPc2
Loose	Loose	k1gFnSc2
a	a	k8xC
Malátka	Malátko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
výška	výška	k1gFnSc1
je	být	k5eAaImIp3nS
33,75	33,75	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
regulována	regulovat	k5eAaImNgFnS
kvůli	kvůli	k7c3
okolní	okolní	k2eAgFnSc3d1
zástavbě	zástavba	k1gFnSc3
a	a	k8xC
především	především	k9
kvůli	kvůli	k7c3
blízkému	blízký	k2eAgNnSc3d1
Národnímu	národní	k2eAgNnSc3d1
muzeu	muzeum	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
nimi	on	k3xPp3gInPc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
rohu	roh	k1gInSc6
kašna	kašna	k1gFnSc1
navrhnutá	navrhnutý	k2eAgFnSc1d1
Loosem	Looso	k1gNnSc7
a	a	k8xC
Janem	Jan	k1gMnSc7
Fišerem	Fišer	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Obchodní	obchodní	k2eAgInSc1d1
parter	parter	k1gInSc1
</s>
<s>
Areál	areál	k1gInSc1
sjednocoval	sjednocovat	k5eAaImAgInS
prosklený	prosklený	k2eAgInSc1d1
dvoupatrový	dvoupatrový	k2eAgInSc1d1
obchodní	obchodní	k2eAgInSc1d1
parter	parter	k1gInSc1
nacházející	nacházející	k2eAgFnSc2d1
se	se	k3xPyFc4
pod	pod	k7c7
věžovými	věžový	k2eAgFnPc7d1
budovami	budova	k1gFnPc7
i	i	k8xC
dispečinkem	dispečink	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
koncipován	koncipovat	k5eAaBmNgInS
jako	jako	k8xS,k8xC
společenský	společenský	k2eAgInSc1d1
prostor	prostor	k1gInSc1
a	a	k8xC
pěší	pěší	k2eAgFnSc1d1
křižovatka	křižovatka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Původní	původní	k2eAgFnSc1d1
zástavba	zástavba	k1gFnSc1
</s>
<s>
Na	na	k7c6
místě	místo	k1gNnSc6
pozdější	pozdní	k2eAgFnSc2d2
stavby	stavba	k1gFnSc2
u	u	k7c2
Vinohradské	vinohradský	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
stály	stát	k5eAaImAgInP
rozsáhlé	rozsáhlý	k2eAgInPc1d1
činžovní	činžovní	k2eAgInPc1d1
domy	dům	k1gInPc1
ze	z	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
čp.	čp.	k?
365	#num#	k4
<g/>
,	,	kIx,
325	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
nechalo	nechat	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
zbořit	zbořit	k5eAaPmF
Ministerstvo	ministerstvo	k1gNnSc1
pošt	pošta	k1gFnPc2
a	a	k8xC
telegrafů	telegraf	k1gInPc2
<g/>
,	,	kIx,
vznikla	vzniknout	k5eAaPmAgFnS
zde	zde	k6eAd1
proluka	proluka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
čtyři	čtyři	k4xCgInPc1
domy	dům	k1gInPc1
na	na	k7c6
ulicích	ulice	k1gFnPc6
Římská	římský	k2eAgNnPc1d1
a	a	k8xC
Rubešova	Rubešův	k2eAgNnPc1d1
byly	být	k5eAaImAgFnP
zbořeny	zbořit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
záměrem	záměr	k1gInSc7
postavit	postavit	k5eAaPmF
zde	zde	k6eAd1
administrativní	administrativní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
Pražských	pražský	k2eAgFnPc2d1
plynáren	plynárna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Původní	původní	k2eAgFnSc1d1
architektonická	architektonický	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
vypsaly	vypsat	k5eAaPmAgInP
Plynárenské	plynárenský	k2eAgInPc1d1
podniky	podnik	k1gInPc1
urbanistickou	urbanistický	k2eAgFnSc4d1
a	a	k8xC
architektonickou	architektonický	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
na	na	k7c4
řešení	řešení	k1gNnSc4
svého	svůj	k3xOyFgNnSc2
budoucího	budoucí	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceny	cena	k1gFnSc2
získali	získat	k5eAaPmAgMnP
Ivo	Ivo	k1gMnSc1
Loos	Loos	k1gInSc1
a	a	k8xC
Jindřich	Jindřich	k1gMnSc1
Malátek	Malátka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přišli	přijít	k5eAaPmAgMnP
s	s	k7c7
uzavřením	uzavření	k1gNnSc7
bloku	blok	k1gInSc2
v	v	k7c6
ulici	ulice	k1gFnSc6
Římské	římský	k2eAgFnSc6d1
a	a	k8xC
postavením	postavení	k1gNnSc7
tří	tři	k4xCgInPc2
věžových	věžový	k2eAgInPc2d1
domů	dům	k1gInPc2
do	do	k7c2
Rubešovy	Rubešův	k2eAgFnSc2d1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
již	již	k6eAd1
reagovali	reagovat	k5eAaBmAgMnP
na	na	k7c4
plány	plán	k1gInPc4
druhé	druhý	k4xOgFnSc2
etapy	etapa	k1gFnSc2
stavby	stavba	k1gFnSc2
Severojižní	severojižní	k2eAgFnSc2d1
magistrály	magistrála	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gInPc1
plány	plán	k1gInPc1
se	se	k3xPyFc4
ale	ale	k9
neuskutečnily	uskutečnit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Nový	nový	k2eAgInSc1d1
záměr	záměr	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
se	se	k3xPyFc4
Československo	Československo	k1gNnSc1
zavázalo	zavázat	k5eAaPmAgNnS
ke	k	k7c3
stavbě	stavba	k1gFnSc3
1030	#num#	k4
km	km	kA
dlouhého	dlouhý	k2eAgInSc2d1
plynovodu	plynovod	k1gInSc2
ze	z	k7c2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
do	do	k7c2
zemí	zem	k1gFnPc2
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
přes	přes	k7c4
své	svůj	k3xOyFgNnSc4
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
finančně	finančně	k6eAd1
a	a	k8xC
technicky	technicky	k6eAd1
náročná	náročný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
vyžadovala	vyžadovat	k5eAaImAgFnS
také	také	k9
telemetrickou	telemetrický	k2eAgFnSc4d1
řídící	řídící	k2eAgFnSc4d1
ústřednu	ústředna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Tranzitní	tranzitní	k2eAgInSc4d1
plynovod	plynovod	k1gInSc4
se	se	k3xPyFc4
tedy	tedy	k9
připravil	připravit	k5eAaPmAgMnS
na	na	k7c4
realizaci	realizace	k1gFnSc4
nového	nový	k2eAgNnSc2d1
sídla	sídlo	k1gNnSc2
a	a	k8xC
přizval	přizvat	k5eAaPmAgMnS
ateliér	ateliér	k1gInSc4
Vojenského	vojenský	k2eAgInSc2d1
projektového	projektový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
vedl	vést	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
Eisenreich	Eisenreich	k1gMnSc1
a	a	k8xC
jednoroční	jednoroční	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
v	v	k7c6
něm	on	k3xPp3gInSc6
tehdy	tehdy	k6eAd1
vykonával	vykonávat	k5eAaImAgMnS
Václav	Václav	k1gMnSc1
Aulický	Aulický	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
tedy	tedy	k9
sestaven	sestavit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
tvůrčí	tvůrčí	k2eAgInSc1d1
tým	tým	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
skládal	skládat	k5eAaImAgInS
i	i	k9
z	z	k7c2
architektů	architekt	k1gMnPc2
původního	původní	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostal	dostat	k5eAaPmAgMnS
také	také	k9
nové	nový	k2eAgNnSc4d1
zadání	zadání	k1gNnSc4
<g/>
:	:	kIx,
dispečink	dispečink	k1gInSc1
transitního	transitní	k2eAgInSc2d1
plynovodu	plynovod	k1gInSc2
<g/>
,	,	kIx,
administrativní	administrativní	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
Federálního	federální	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
paliv	palivo	k1gNnPc2
a	a	k8xC
energetiky	energetika	k1gFnSc2
<g/>
,	,	kIx,
obchody	obchod	k1gInPc1
a	a	k8xC
provozovny	provozovna	k1gFnPc1
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Reprezentativní	reprezentativní	k2eAgInPc4d1
vnitřní	vnitřní	k2eAgInPc4d1
prostory	prostor	k1gInPc4
a	a	k8xC
venkovní	venkovní	k2eAgFnSc4d1
kašnu	kašna	k1gFnSc4
v	v	k7c6
zahloubeném	zahloubený	k2eAgInSc6d1
rohu	roh	k1gInSc6
u	u	k7c2
ulice	ulice	k1gFnSc2
Římská	římský	k2eAgNnPc1d1
řešil	řešit	k5eAaImAgInS
architekt	architekt	k1gMnSc1
Ivo	Ivo	k1gMnSc1
Loos	Loos	k1gInSc4
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Janem	Jan	k1gMnSc7
Fišeremem	Fišerem	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocelové	ocelový	k2eAgInPc1d1
skelety	skelet	k1gInPc1
budov	budova	k1gFnPc2
řešil	řešit	k5eAaImAgMnS
konstruktér	konstruktér	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Kozák	Kozák	k1gMnSc1
se	s	k7c7
spolupracovníkem	spolupracovník	k1gMnSc7
Josefem	Josef	k1gMnSc7
Tomáškem	Tomášek	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výstavba	výstavba	k1gFnSc1
</s>
<s>
Výstavba	výstavba	k1gFnSc1
objektu	objekt	k1gInSc2
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
letech	léto	k1gNnPc6
1972	#num#	k4
až	až	k9
1978	#num#	k4
<g/>
,	,	kIx,
dodavatelem	dodavatel	k1gMnSc7
byl	být	k5eAaImAgInS
národní	národní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Průmyslové	průmyslový	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
Gottwaldov	Gottwaldov	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
první	první	k4xOgFnSc7
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1974	#num#	k4
dokončena	dokončen	k2eAgFnSc1d1
hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
–	–	k?
samotný	samotný	k2eAgInSc1d1
dispečink	dispečink	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
výškou	výška	k1gFnSc7
nejnižší	nízký	k2eAgFnSc1d3
<g/>
,	,	kIx,
avšak	avšak	k8xC
nejdůležitější	důležitý	k2eAgFnSc1d3
budova	budova	k1gFnSc1
komplexu	komplex	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
byly	být	k5eAaImAgFnP
dokončeny	dokončit	k5eAaPmNgFnP
obě	dva	k4xCgFnPc1
kancelářské	kancelářský	k2eAgFnPc1d1
věžové	věžový	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
odkloněné	odkloněný	k2eAgFnPc1d1
od	od	k7c2
Vinohradské	vinohradský	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Záměr	záměr	k1gInSc1
demolice	demolice	k1gFnSc2
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2014	#num#	k4
projevila	projevit	k5eAaPmAgFnS
zájem	zájem	k1gInSc4
o	o	k7c4
pozemek	pozemek	k1gInSc4
budovy	budova	k1gFnSc2
Transgasu	Transgas	k1gInSc2
investorská	investorský	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
HB	HB	kA
Reavis	Reavis	k1gFnSc1
slovenského	slovenský	k2eAgMnSc2d1
miliardáře	miliardář	k1gMnSc2
Ivana	Ivan	k1gMnSc2
Chrenka	Chrenka	k1gFnSc1
se	s	k7c7
záměrem	záměr	k1gInSc7
celou	celý	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
zdemolovat	zdemolovat	k5eAaPmF
a	a	k8xC
vytvořit	vytvořit	k5eAaPmF
zde	zde	k6eAd1
moderní	moderní	k2eAgNnSc4d1
kancelářské	kancelářský	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
šest	šest	k4xCc1
až	až	k8xS
osm	osm	k4xCc1
nadzemních	nadzemní	k2eAgNnPc2d1
podlaží	podlaží	k1gNnPc2
a	a	k8xC
zelený	zelený	k2eAgInSc1d1
vnitroblok	vnitroblok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
se	se	k3xPyFc4
na	na	k7c4
podnět	podnět	k1gInSc4
Klubu	klub	k1gInSc2
za	za	k7c4
starou	starý	k2eAgFnSc4d1
Prahu	Praha	k1gFnSc4
kauzou	kauza	k1gFnSc7
zabýval	zabývat	k5eAaImAgInS
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
byl	být	k5eAaImAgInS
objekt	objekt	k1gInSc4
v	v	k7c6
dokumentu	dokument	k1gInSc6
„	„	k?
<g/>
Územně	územně	k6eAd1
analytické	analytický	k2eAgInPc1d1
podklady	podklad	k1gInPc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
<g/>
“	“	k?
ÚRM	ÚRM	kA
z	z	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
mezi	mezi	k7c4
architektonicky	architektonicky	k6eAd1
významnými	významný	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
kategorie	kategorie	k1gFnSc2
A	A	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
si	se	k3xPyFc3
při	pře	k1gFnSc4
navrhování	navrhování	k1gNnSc2
změn	změna	k1gFnPc2
územního	územní	k2eAgInSc2d1
plánu	plán	k1gInSc2
zasluhují	zasluhovat	k5eAaImIp3nP
obzvláštní	obzvláštní	k2eAgInSc4d1
respekt	respekt	k1gInSc4
<g/>
,	,	kIx,
NPÚ	NPÚ	kA
návrh	návrh	k1gInSc1
na	na	k7c4
zařazení	zařazení	k1gNnSc4
mezi	mezi	k7c4
kulturní	kulturní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
nedoporučil	doporučit	k5eNaPmAgMnS
a	a	k8xC
Ministerstvo	ministerstvo	k1gNnSc1
Kultury	kultura	k1gFnSc2
ČR	ČR	kA
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
návrh	návrh	k1gInSc1
zcela	zcela	k6eAd1
zamítlo	zamítnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oficiálním	oficiální	k2eAgInSc6d1
zápisu	zápis	k1gInSc6
bylo	být	k5eAaImAgNnS
uvedeno	uvést	k5eAaPmNgNnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Architektonické	architektonický	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
stávající	stávající	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
nevyvažují	vyvažovat	k5eNaImIp3nP
jeho	jeho	k3xOp3gInPc4
závažné	závažný	k2eAgInPc4d1
urbanistické	urbanistický	k2eAgInPc4d1
nedostatky	nedostatek	k1gInPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Demolice	demolice	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
budova	budova	k1gFnSc1
začala	začít	k5eAaPmAgFnS
vyklízet	vyklízet	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
začaly	začít	k5eAaPmAgInP
přípravy	příprava	k1gFnPc4
na	na	k7c6
demolici	demolice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Majitel	majitel	k1gMnSc1
HB	HB	kA
Reavis	Reavis	k1gFnSc1
v	v	k7c6
červenci	červenec	k1gInSc6
2018	#num#	k4
požádal	požádat	k5eAaPmAgInS
Stavební	stavební	k2eAgInSc1d1
úřad	úřad	k1gInSc1
Prahy	Praha	k1gFnSc2
2	#num#	k4
o	o	k7c4
povolení	povolení	k1gNnSc4
k	k	k7c3
demolici	demolice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2019	#num#	k4
stavební	stavební	k2eAgInSc1d1
úřad	úřad	k1gInSc1
povolení	povolení	k1gNnSc2
k	k	k7c3
demolici	demolice	k1gFnSc3
dal	dát	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Proti	proti	k7c3
povolení	povolení	k1gNnSc3
demolice	demolice	k1gFnSc2
se	se	k3xPyFc4
nikdo	nikdo	k3yNnSc1
neodvolal	odvolat	k5eNaPmAgMnS
<g/>
,	,	kIx,
takže	takže	k8xS
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
vstoupilo	vstoupit	k5eAaPmAgNnS
v	v	k7c4
platnost	platnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Nové	Nové	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
Prahy	Praha	k1gFnSc2
od	od	k7c2
podzimu	podzim	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
snažilo	snažit	k5eAaImAgNnS
<g />
.	.	kIx.
</s>
<s hack="1">
budovu	budova	k1gFnSc4
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
od	od	k7c2
developera	developer	k1gMnSc2
odkoupit	odkoupit	k5eAaPmF
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
však	však	k9
odkup	odkup	k1gInSc4
nabídl	nabídnout	k5eAaPmAgMnS
za	za	k7c4
více	hodně	k6eAd2
než	než	k8xS
1,5	1,5	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
vedení	vedení	k1gNnSc1
odmítlo	odmítnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Kolem	kolem	k7c2
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
začala	začít	k5eAaPmAgFnS
demolice	demolice	k1gFnSc1
<g/>
,	,	kIx,
nejdříve	dříve	k6eAd3
byla	být	k5eAaImAgFnS
odstraněna	odstraněn	k2eAgFnSc1d1
kašna	kašna	k1gFnSc1
<g/>
,	,	kIx,
vchodový	vchodový	k2eAgInSc1d1
přístřešek	přístřešek	k1gInSc1
a	a	k8xC
několik	několik	k4yIc1
prvků	prvek	k1gInPc2
na	na	k7c6
fasádě	fasáda	k1gFnSc6
budovy	budova	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
Později	pozdě	k6eAd2
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
celá	celý	k2eAgFnSc1d1
budova	budova	k1gFnSc1
rozebírána	rozebírán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
HB	HB	kA
Reavis	Reavis	k1gFnSc1
pak	pak	k6eAd1
od	od	k7c2
projektu	projekt	k1gInSc2
Vinohradská	vinohradský	k2eAgFnSc1d1
8	#num#	k4
upustil	upustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
že	že	k8xS
opouští	opouštět	k5eAaImIp3nP
svoje	svůj	k3xOyFgFnPc4
pražské	pražský	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
a	a	k8xC
nabídl	nabídnout	k5eAaPmAgMnS
svoje	svůj	k3xOyFgFnPc4
budovy	budova	k1gFnPc4
Transgasu	Transgas	k1gInSc2
na	na	k7c4
prodej	prodej	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
září	září	k1gNnSc6
2019	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
prodeje	prodej	k1gInSc2
sešlo	sejít	k5eAaPmAgNnS
a	a	k8xC
že	že	k8xS
hodlá	hodlat	k5eAaImIp3nS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
původním	původní	k2eAgInSc6d1
záměru	záměr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
února	únor	k1gInSc2
2020	#num#	k4
byl	být	k5eAaImAgInS
pak	pak	k6eAd1
objekt	objekt	k1gInSc1
zbourán	zbourat	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2021	#num#	k4
prodal	prodat	k5eAaPmAgMnS
HB	HB	kA
Reavis	Reavis	k1gInSc1
pozemek	pozemek	k1gInSc1
po	po	k7c6
bývalém	bývalý	k2eAgInSc6d1
Transgasu	Transgas	k1gInSc6
společnosti	společnost	k1gFnSc2
PSN	PSN	kA
za	za	k7c4
870	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PSN	PSN	kA
plánuje	plánovat	k5eAaImIp3nS
na	na	k7c6
místě	místo	k1gNnSc6
vystavět	vystavět	k5eAaPmF
novou	nový	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
budou	být	k5eAaImBp3nP
byty	byt	k1gInPc4
<g/>
,	,	kIx,
administrativní	administrativní	k2eAgInPc4d1
prostory	prostor	k1gInPc4
<g/>
,	,	kIx,
obchody	obchod	k1gInPc4
a	a	k8xC
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
Filip	Filip	k1gMnSc1
Buryán	Buryán	k1gMnSc1
<g/>
:	:	kIx,
Transgas	Transgas	k1gInSc1
<g/>
,	,	kIx,
akvarel	akvarel	k1gInSc1
a	a	k8xC
tuš	tuš	k1gFnSc1
na	na	k7c6
papíře	papír	k1gInSc6
<g/>
,	,	kIx,
42	#num#	k4
x	x	k?
29,7	29,7	k4
cm	cm	kA
<g/>
,	,	kIx,
2018	#num#	k4
</s>
<s>
Budova	budova	k1gFnSc1
Transgasu	Transgas	k1gInSc2
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
i	i	k9
v	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
nejoblíbenějších	oblíbený	k2eAgFnPc2d3
českých	český	k2eAgFnPc2d1
televizních	televizní	k2eAgFnPc2d1
komedií	komedie	k1gFnPc2
Vesničko	vesnička	k1gFnSc5
má	mít	k5eAaImIp3nS
<g/>
,	,	kIx,
středisková	střediskový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmu	film	k1gInSc6
je	být	k5eAaImIp3nS
scéna	scéna	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
jde	jít	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
hrdinů	hrdina	k1gMnPc2
filmu	film	k1gInSc2
<g/>
,	,	kIx,
závozník	závozník	k1gMnSc1
Otík	Otík	k1gMnSc1
Rákosník	rákosník	k1gMnSc1
<g/>
,	,	kIx,
dohodnout	dohodnout	k5eAaPmF
výměnu	výměna	k1gFnSc4
panelového	panelový	k2eAgInSc2d1
bytu	byt	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c6
Jižním	jižní	k2eAgNnSc6d1
Městě	město	k1gNnSc6
za	za	k7c4
svoji	svůj	k3xOyFgFnSc4
rodnou	rodný	k2eAgFnSc4d1
chalupu	chalupa	k1gFnSc4
v	v	k7c6
Křečovicích	Křečovice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
scéně	scéna	k1gFnSc6
jsou	být	k5eAaImIp3nP
dobře	dobře	k6eAd1
patrné	patrný	k2eAgInPc1d1
i	i	k8xC
některé	některý	k3yIgInPc1
interiéry	interiér	k1gInPc1
v	v	k7c6
původním	původní	k2eAgInSc6d1
stavu	stav	k1gInSc6
z	z	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Transgas	Transgas	k1gMnSc1
se	se	k3xPyFc4
též	též	k9
objevil	objevit	k5eAaPmAgMnS
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
dílu	díl	k1gInSc2
televizního	televizní	k2eAgInSc2d1
detektivního	detektivní	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
Vraždy	vražda	k1gFnSc2
v	v	k7c6
kruhu	kruh	k1gInSc6
nazvaném	nazvaný	k2eAgInSc6d1
Vítěz	vítěz	k1gMnSc1
nejde	jít	k5eNaImIp3nS
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Major	major	k1gMnSc1
Holina	holina	k1gFnSc1
(	(	kIx(
<g/>
Ivan	Ivan	k1gMnSc1
Trojan	Trojan	k1gMnSc1
<g/>
)	)	kIx)
přichází	přicházet	k5eAaImIp3nS
vyšetřovat	vyšetřovat	k5eAaImF
vraždu	vražda	k1gFnSc4
do	do	k7c2
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
záběrech	záběr	k1gInPc6
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
původní	původní	k2eAgNnSc4d1
schodiště	schodiště	k1gNnSc4
v	v	k7c6
budově	budova	k1gFnSc6
dispečinku	dispečink	k1gInSc2
a	a	k8xC
vnitřek	vnitřek	k1gInSc4
spojovacích	spojovací	k2eAgInPc2d1
tubusů	tubus	k1gInPc2
mezi	mezi	k7c7
dispečinkem	dispečink	k1gInSc7
a	a	k8xC
věžovými	věžový	k2eAgFnPc7d1
budovami	budova	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Různé	různý	k2eAgFnPc1d1
části	část	k1gFnPc1
komplexu	komplex	k1gInSc2
budov	budova	k1gFnPc2
Transgas	Transgasa	k1gFnPc2
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
i	i	k9
na	na	k7c6
začátku	začátek	k1gInSc6
pohádkového	pohádkový	k2eAgInSc2d1
filmu	film	k1gInSc2
Kaňka	kaňka	k1gFnSc1
do	do	k7c2
pohádky	pohádka	k1gFnSc2
od	od	k7c2
režiséra	režisér	k1gMnSc2
Oty	Ota	k1gMnSc2
Kovala	kovat	k5eAaImAgFnS
z	z	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
děti	dítě	k1gFnPc1
si	se	k3xPyFc3
ve	v	k7c6
škole	škola	k1gFnSc6
vyzvednou	vyzvednout	k5eAaPmIp3nP
rodiče	rodič	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
osmiletou	osmiletý	k2eAgFnSc4d1
Vendulku	Vendulka	k1gFnSc4
(	(	kIx(
<g/>
Žaneta	Žaneta	k1gFnSc1
Fuchsová	Fuchsová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
nikdo	nikdo	k3yNnSc1
nepřijde	přijít	k5eNaPmIp3nS
a	a	k8xC
tak	tak	k6eAd1
musí	muset	k5eAaImIp3nS
sama	sám	k3xTgMnSc4
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistane	přistat	k5eAaPmIp3nS
jí	on	k3xPp3gFnSc3
u	u	k7c2
nohou	noha	k1gFnPc2
vlaštovka	vlaštovka	k1gFnSc1
se	se	k3xPyFc4
vzkazem	vzkaz	k1gInSc7
Miluju	milovat	k5eAaImIp1nS
tě	ty	k3xPp2nSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vendulka	Vendulka	k1gFnSc1
ji	on	k3xPp3gFnSc4
hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
dál	daleko	k6eAd2
<g/>
,	,	kIx,
další	další	k2eAgMnPc1d1
zase	zase	k9
dál	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
počátečních	počáteční	k2eAgFnPc6d1
scénách	scéna	k1gFnPc6
jsou	být	k5eAaImIp3nP
vidět	vidět	k5eAaImF
některé	některý	k3yIgFnPc4
části	část	k1gFnPc4
Transgasu	Transgas	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jak	jak	k6eAd1
vypadal	vypadat	k5eAaPmAgInS,k5eAaImAgInS
na	na	k7c6
počátku	počátek	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
se	se	k3xPyFc4
budova	budova	k1gFnSc1
Transgas	Transgasa	k1gFnPc2
objevila	objevit	k5eAaPmAgFnS
v	v	k7c6
reklamě	reklama	k1gFnSc6
americké	americký	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Apple	Apple	kA
na	na	k7c6
iPhone	iPhon	k1gInSc5
XR	XR	kA
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
brutalistními	brutalistní	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Již	již	k6eAd1
nefunkční	funkční	k2eNgFnSc1d1
fontána	fontána	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Schodiště	schodiště	k1gNnSc1
v	v	k7c6
podchodu	podchod	k1gInSc6
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Architekt	architekt	k1gMnSc1
Václav	Václav	k1gMnSc1
Aulický	Aulický	k2eAgMnSc1d1
při	při	k7c6
poslední	poslední	k2eAgFnSc6d1
komentované	komentovaný	k2eAgFnSc6d1
prohlídce	prohlídka	k1gFnSc6
Transgasu	Transgas	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
1945	#num#	k4
<g/>
-	-	kIx~
<g/>
,	,	kIx,
Ševčík	Ševčík	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architektura	architektura	k1gFnSc1
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
:	:	kIx,
"	"	kIx"
<g/>
zlatá	zlatý	k2eAgNnPc4d1
šedesátá	šedesátý	k4xOgNnPc4
léta	léto	k1gNnPc4
<g/>
"	"	kIx"
v	v	k7c6
české	český	k2eAgFnSc6d1
architektuře	architektura	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
502	#num#	k4
s.	s.	k?
s.	s.	k?
ISBN	ISBN	kA
8024713721	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
316675732	#num#	k4
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
BERAN	Beran	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transgas	Transgas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
archiweb	archiwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-08-15	2015-08-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ostuda	ostuda	k1gFnSc1
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
perla	perla	k1gFnSc1
brutalismu	brutalismus	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Legendární	legendární	k2eAgInSc1d1
Transgas	Transgas	k1gInSc1
budí	budit	k5eAaImIp3nS
vášně	vášeň	k1gFnPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-04-01	2016-04-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HB	HB	kA
Reavis	Reavis	k1gFnPc2
chce	chtít	k5eAaImIp3nS
zbourat	zbourat	k5eAaPmF
budovu	budova	k1gFnSc4
na	na	k7c6
začátku	začátek	k1gInSc6
Vinohradské	vinohradský	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
archiweb	archiwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-08-03	2015-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Pražské	pražský	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
Transgas	Transgasa	k1gFnPc2
mají	mít	k5eAaImIp3nP
na	na	k7c6
kahánku	kahánek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měly	mít	k5eAaImAgInP
by	by	kYmCp3nP
ustoupit	ustoupit	k5eAaPmF
moderně	moderně	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2016-04-26	2016-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Transgas	Transgas	k1gMnSc1
na	na	k7c6
Vinohradské	vinohradský	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
není	být	k5eNaImIp3nS
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-13	2016-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyklízení	vyklízení	k1gNnSc1
Transgasu	Transgas	k1gInSc2
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
,	,	kIx,
blíží	blížit	k5eAaImIp3nS
se	se	k3xPyFc4
demolice	demolice	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
RTVplus	RTVplus	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-03-10	2017-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Majitel	majitel	k1gMnSc1
Transgasu	Transgas	k1gInSc2
požádal	požádat	k5eAaPmAgMnS
o	o	k7c6
demolici	demolice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
brutalistního	brutalistní	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
má	mít	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
administrativní	administrativní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stavební	stavební	k2eAgInSc1d1
úřad	úřad	k1gInSc1
Prahy	Praha	k1gFnSc2
2	#num#	k4
povolil	povolit	k5eAaPmAgMnS
demolici	demolice	k1gFnSc4
budov	budova	k1gFnPc2
Transgas	Transgasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
20190201	#num#	k4
|	|	kIx~
Majitel	majitel	k1gMnSc1
souboru	soubor	k1gInSc2
pražského	pražský	k2eAgInSc2d1
Transgasu	Transgas	k1gInSc2
může	moct	k5eAaImIp3nS
budovy	budova	k1gFnPc4
zbourat	zbourat	k5eAaPmF
(	(	kIx(
<g/>
ceskenoviny	ceskenovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vykoupení	vykoupení	k1gNnSc1
Transgasu	Transgas	k1gInSc2
zkrachovalo	zkrachovat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
se	se	k3xPyFc4
s	s	k7c7
developerem	developer	k1gMnSc7
nedohodla	dohodnout	k5eNaPmAgFnS
na	na	k7c6
ceně	cena	k1gFnSc6
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Transgas	Transgas	k1gInSc1
zmizí	zmizet	k5eAaPmIp3nS
tiše	tiš	k1gFnPc4
<g/>
,	,	kIx,
budoucí	budoucí	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
může	moct	k5eAaImIp3nS
ohrozit	ohrozit	k5eAaPmF
tunel	tunel	k1gInSc4
pod	pod	k7c7
komplexem	komplex	k1gInSc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-19	2019-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Začala	začít	k5eAaPmAgFnS
demolice	demolice	k1gFnSc1
Transgasu	Transgas	k1gInSc2
na	na	k7c6
Vinohradech	Vinohrady	k1gInPc6
<g/>
,	,	kIx,
budova	budova	k1gFnSc1
je	být	k5eAaImIp3nS
ale	ale	k9
na	na	k7c4
prodej	prodej	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-15	2019-02-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V	v	k7c6
developerském	developerský	k2eAgInSc6d1
projektu	projekt	k1gInSc6
po	po	k7c6
Transgasu	Transgas	k1gInSc6
nastal	nastat	k5eAaPmAgMnS
nečekaný	čekaný	k2eNgInSc4d1
zvrat	zvrat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
OBRAZEM	obraz	k1gInSc7
<g/>
:	:	kIx,
Demolice	demolice	k1gFnSc1
Transgasu	Transgas	k1gInSc2
je	být	k5eAaImIp3nS
dokončená	dokončený	k2eAgFnSc1d1
<g/>
,	,	kIx,
vyroste	vyrůst	k5eAaPmIp3nS
tam	tam	k6eAd1
moderní	moderní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-14	2020-02-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Koupili	koupit	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
pozemek	pozemek	k1gInSc4
Transgasu	Transgas	k1gInSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Odvážná	odvážný	k2eAgFnSc1d1
akvizice	akvizice	k1gFnSc1
PSN	PSN	kA
oživí	oživit	k5eAaPmIp3nS
okolí	okolí	k1gNnSc4
Václavského	václavský	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PSN	PSN	kA
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vesničko	vesnička	k1gFnSc5
má	mít	k5eAaImIp3nS
středisková	střediskový	k2eAgFnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Youtube	Youtub	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2016-08	2016-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vraždy	vražda	k1gFnSc2
v	v	k7c6
kruhu	kruh	k1gInSc6
<g/>
:	:	kIx,
Vítěz	vítěz	k1gMnSc1
nejde	jít	k5eNaImIp3nS
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kaňka	Kaňka	k1gMnSc1
do	do	k7c2
pohádky	pohádka	k1gFnSc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BEST	BEST	kA
FILMS	FILMS	kA
EN	EN	kA
<g/>
/	/	kIx~
<g/>
CZ	CZ	kA
<g/>
/	/	kIx~
<g/>
SK	Sk	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaňka	kaňka	k1gFnSc1
do	do	k7c2
pohádky	pohádka	k1gFnSc2
-	-	kIx~
CZ	CZ	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
REFRESHER	REFRESHER	kA
<g/>
.	.	kIx.
<g/>
SK	Sk	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnovější	nový	k2eAgInSc1d3
reklamu	reklama	k1gFnSc4
na	na	k7c4
iPhone	iPhon	k1gInSc5
XR	XR	kA
natočili	natočit	k5eAaBmAgMnP
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
refresher	refreshra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
TRANSGAS	TRANSGAS	kA
-	-	kIx~
Areál	areál	k1gInSc1
řídící	řídící	k2eAgFnSc2d1
ústředny	ústředna	k1gFnSc2
Tranzitního	tranzitní	k2eAgInSc2d1
plynovodu	plynovod	k1gInSc2
a	a	k8xC
budova	budova	k1gFnSc1
FMPE	FMPE	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Naďa	Naďa	k1gFnSc1
Gorytzková	Gorytzkový	k2eAgFnSc1d1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
255	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7480	#num#	k4
<g/>
-	-	kIx~
<g/>
138	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Telefonní	telefonní	k2eAgFnSc1d1
ústředna	ústředna	k1gFnSc1
–	–	k?
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Dejvice	Dejvice	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Transgas	Transgasa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Zpráva	zpráva	k1gFnSc1
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
přišli	přijít	k5eAaPmAgMnP
rozloučit	rozloučit	k5eAaPmF
s	s	k7c7
Transgasem	Transgas	k1gInSc7
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Transgas	Transgas	k1gInSc1
na	na	k7c6
archiweb	archiwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Transgas	Transgas	k1gInSc1
v	v	k7c6
Památkovém	památkový	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
</s>
<s>
Transgaz	Transgaz	k1gInSc1
z	z	k7c2
ptačí	ptačí	k2eAgFnSc2d1
perspektivy	perspektiva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaImRp2nP,k5eAaPmRp2nP
se	se	k3xPyFc4
na	na	k7c4
budovu	budova	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
součástí	součást	k1gFnPc2
Prahy	Praha	k1gFnSc2
–	–	k?
video	video	k1gNnSc1
na	na	k7c4
iRozhlas	iRozhlas	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Grafika	grafika	k1gFnSc1
<g/>
:	:	kIx,
Je	být	k5eAaImIp3nS
Transgas	Transgas	k1gMnSc1
jizva	jizva	k1gFnSc1
na	na	k7c6
tváři	tvář	k1gFnSc6
Prahy	Praha	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Brutalismus	brutalismus	k1gInSc1
není	být	k5eNaImIp3nS
normalizační	normalizační	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
–	–	k?
přehledový	přehledový	k2eAgInSc4d1
článek	článek	k1gInSc4
na	na	k7c6
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
</s>
