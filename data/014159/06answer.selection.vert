<s>
Budova	budova	k1gFnSc1
Transgas	Transgasa	k1gFnPc2
(	(	kIx(
<g/>
Centrální	centrální	k2eAgInSc1d1
dispečink	dispečink	k1gInSc1
Transgas	Transgasa	k1gFnPc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
Transgaz	Transgaz	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
Plynárenské	plynárenský	k2eAgNnSc1d1
řídící	řídící	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
dříve	dříve	k6eAd2
též	též	k9
Ústřední	ústřední	k2eAgInSc1d1
dispečink	dispečink	k1gInSc1
tranzitního	tranzitní	k2eAgInSc2d1
plynovodu	plynovod	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
Federální	federální	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
paliv	palivo	k1gNnPc2
a	a	k8xC
energetiky	energetika	k1gFnSc2
byl	být	k5eAaImAgInS
komplex	komplex	k1gInSc1
tří	tři	k4xCgFnPc2
budov	budova	k1gFnPc2
postavených	postavený	k2eAgFnPc2d1
v	v	k7c6
letech	léto	k1gNnPc6
1972	#num#	k4
až	až	k9
1978	#num#	k4
v	v	k7c6
brutalistickém	brutalistický	k2eAgInSc6d1
stylu	styl	k1gInSc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
demolice	demolice	k1gFnSc1
započala	započnout	k5eAaPmAgFnS
v	v	k7c6
březnu	březen	k1gInSc6
2019	#num#	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
.	.	kIx.
</s>