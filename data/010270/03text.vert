<p>
<s>
Tadeusz	Tadeusz	k1gMnSc1	Tadeusz
Kutrzeba	Kutrzeba	k1gMnSc1	Kutrzeba
[	[	kIx(	[
<g/>
tadeuš	tadeuš	k1gMnSc1	tadeuš
kutřeba	kutřeba	k1gMnSc1	kutřeba
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1885	[number]	k4	1885
nebo	nebo	k8xC	nebo
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Krakov	Krakov	k1gInSc1	Krakov
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
divizní	divizní	k2eAgMnSc1d1	divizní
generál	generál	k1gMnSc1	generál
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
polských	polský	k2eAgMnPc2d1	polský
velitelů	velitel	k1gMnPc2	velitel
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
ukončil	ukončit	k5eAaPmAgInS	ukončit
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
technickou	technický	k2eAgFnSc4d1	technická
akademii	akademie	k1gFnSc4	akademie
v	v	k7c6	v
Mödlingenu	Mödlingen	k1gInSc6	Mödlingen
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
podporučíkem	podporučík	k1gMnSc7	podporučík
ženijního	ženijní	k2eAgNnSc2d1	ženijní
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
jako	jako	k8xC	jako
generál	generál	k1gMnSc1	generál
<g/>
)	)	kIx)	)
vedl	vést	k5eAaImAgMnS	vést
Armádu	armáda	k1gFnSc4	armáda
Poznaň	Poznaň	k1gFnSc4	Poznaň
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
4	[number]	k4	4
pěších	pěší	k2eAgFnPc2d1	pěší
divizí	divize	k1gFnPc2	divize
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
jízdních	jízdní	k2eAgFnPc2d1	jízdní
brigád	brigáda	k1gFnPc2	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
ulevil	ulevit	k5eAaPmAgMnS	ulevit
tlaku	tlak	k1gInSc3	tlak
na	na	k7c4	na
Varšavu	Varšava	k1gFnSc4	Varšava
a	a	k8xC	a
poté	poté	k6eAd1	poté
posílil	posílit	k5eAaPmAgMnS	posílit
její	její	k3xOp3gMnSc1	její
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
naplánoval	naplánovat	k5eAaBmAgMnS	naplánovat
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
zoufalý	zoufalý	k2eAgInSc4d1	zoufalý
protiútok	protiútok	k1gInSc4	protiútok
trosek	troska	k1gFnPc2	troska
armád	armáda	k1gFnPc2	armáda
Poznaň	Poznaň	k1gFnSc4	Poznaň
a	a	k8xC	a
Pomoří	pomořit	k5eAaPmIp3nS	pomořit
na	na	k7c4	na
křídlo	křídlo	k1gNnSc4	křídlo
a	a	k8xC	a
týl	týl	k1gInSc4	týl
skupiny	skupina	k1gFnSc2	skupina
armád	armáda	k1gFnPc2	armáda
Jih	jih	k1gInSc1	jih
generála	generál	k1gMnSc2	generál
Gerda	Gerda	k1gFnSc1	Gerda
von	von	k1gInSc1	von
Rundstedta	Rundstedta	k1gFnSc1	Rundstedta
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Bzuře	Bzura	k1gFnSc6	Bzura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
počátečních	počáteční	k2eAgInPc6d1	počáteční
úspěších	úspěch	k1gInPc6	úspěch
rozdrcen	rozdrcen	k2eAgInSc1d1	rozdrcen
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
přesilou	přesila	k1gFnSc7	přesila
německých	německý	k2eAgFnPc2d1	německá
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
metodickými	metodický	k2eAgInPc7d1	metodický
údery	úder	k1gInPc7	úder
Luftwaffe	Luftwaff	k1gMnSc5	Luftwaff
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Varšavy	Varšava	k1gFnSc2	Varšava
a	a	k8xC	a
jako	jako	k9	jako
válečný	válečný	k2eAgMnSc1d1	válečný
zajatec	zajatec	k1gMnSc1	zajatec
vězněn	věznit	k5eAaImNgMnS	věznit
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
Hohenstein	Hohensteina	k1gFnPc2	Hohensteina
<g/>
,	,	kIx,	,
Königstein	Königsteina	k1gFnPc2	Königsteina
a	a	k8xC	a
Murnau	Murnaus	k1gInSc2	Murnaus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
Komise	komise	k1gFnSc2	komise
historické	historický	k2eAgFnSc2d1	historická
zářijové	zářijový	k2eAgFnSc2d1	zářijová
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zasedala	zasedat	k5eAaImAgFnS	zasedat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
'	'	kIx"	'
<g/>
Cmentarzu	Cmentarz	k1gInSc6	Cmentarz
Wojskowym	Wojskowym	k1gInSc4	Wojskowym
na	na	k7c4	na
Powązkach	Powązkach	k1gInSc4	Powązkach
<g/>
'	'	kIx"	'
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
===	===	k?	===
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
řádem	řád	k1gInSc7	řád
Virtuti	Virtuť	k1gFnSc2	Virtuť
Militari	Militar	k1gFnSc2	Militar
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Řádem	řád	k1gInSc7	řád
Odrodzenia	Odrodzenium	k1gNnSc2	Odrodzenium
Polski	Polsk	k1gFnSc2	Polsk
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
řádem	řád	k1gInSc7	řád
Krzyż	Krzyż	k1gFnPc2	Krzyż
Walecznych	Walecznycha	k1gFnPc2	Walecznycha
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řádem	řád	k1gInSc7	řád
Złoty	Złota	k1gFnSc2	Złota
Krzyż	Krzyż	k1gFnSc2	Krzyż
Zasługi	Zasług	k1gFnSc2	Zasług
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgNnPc2d1	další
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tadeusz	Tadeusza	k1gFnPc2	Tadeusza
Kutrzeba	Kutrzeba	k1gFnSc1	Kutrzeba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
