<s>
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Lawrence	Lawrence	k1gFnSc1	Lawrence
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
popularizátor	popularizátor	k1gMnSc1	popularizátor
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Louis	Louis	k1gMnSc1	Louis
Bernstein	Bernstein	k1gMnSc1	Bernstein
v	v	k7c6	v
Lawrence	Lawrenka	k1gFnSc6	Lawrenka
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc2	Massachusetts
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
Židů	Žid	k1gMnPc2	Žid
původem	původ	k1gInSc7	původ
z	z	k7c2	z
města	město	k1gNnSc2	město
Rovno	roven	k2eAgNnSc1d1	rovno
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
nenalezeno	nalezen	k2eNgNnSc1d1	nenalezeno
v	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
zdroji	zdroj	k1gInSc6	zdroj
<g/>
]	]	kIx)	]
Nebyl	být	k5eNaImAgMnS	být
příbuzný	příbuzný	k1gMnSc1	příbuzný
filmového	filmový	k2eAgMnSc2d1	filmový
skladatele	skladatel	k1gMnSc2	skladatel
Elmera	Elmer	k1gMnSc2	Elmer
Bernsteina	Bernstein	k1gMnSc2	Bernstein
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
sdíleli	sdílet	k5eAaImAgMnP	sdílet
určitou	určitý	k2eAgFnSc4d1	určitá
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
podobnost	podobnost	k1gFnSc4	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
malý	malý	k2eAgMnSc1d1	malý
chlapec	chlapec	k1gMnSc1	chlapec
bral	brát	k5eAaImAgMnS	brát
lekce	lekce	k1gFnPc4	lekce
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
a	a	k8xC	a
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Posádkovou	posádkový	k2eAgFnSc4d1	Posádková
a	a	k8xC	a
Bostonskou	bostonský	k2eAgFnSc4d1	Bostonská
latinskou	latinský	k2eAgFnSc4d1	Latinská
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
studoval	studovat	k5eAaImAgMnS	studovat
s	s	k7c7	s
Walterem	Walter	k1gMnSc7	Walter
Pistonem	piston	k1gInSc7	piston
<g/>
,	,	kIx,	,
Edwardem	Edward	k1gMnSc7	Edward
Burlingame-Hillem	Burlingame-Hill	k1gMnSc7	Burlingame-Hill
a	a	k8xC	a
A.	A.	kA	A.
Tillmanem	Tillman	k1gMnSc7	Tillman
Merrittem	Merritt	k1gMnSc7	Merritt
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgFnPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
studium	studium	k1gNnSc4	studium
hudebních	hudební	k2eAgInPc2d1	hudební
oborů	obor	k1gInPc2	obor
na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
studoval	studovat	k5eAaImAgMnS	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
dirigování	dirigování	k1gNnSc4	dirigování
a	a	k8xC	a
orchestraci	orchestrace	k1gFnSc4	orchestrace
na	na	k7c6	na
Curtisově	Curtisův	k2eAgInSc6d1	Curtisův
institutu	institut	k1gInSc6	institut
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
nejnadanějším	nadaný	k2eAgMnSc7d3	nejnadanější
žákem	žák	k1gMnSc7	žák
Fritze	Fritze	k1gFnSc2	Fritze
Reinera	Reiner	k1gMnSc2	Reiner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
studoval	studovat	k5eAaImAgInS	studovat
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
dirigenta	dirigent	k1gMnSc2	dirigent
Sergeje	Sergej	k1gMnSc2	Sergej
Kusevického	Kusevický	k2eAgMnSc2d1	Kusevický
v	v	k7c6	v
Tanglewoodu	Tanglewood	k1gInSc6	Tanglewood
<g/>
,	,	kIx,	,
berkshisrském	berkshisrský	k2eAgNnSc6d1	berkshisrský
letním	letní	k2eAgNnSc6d1	letní
středisku	středisko	k1gNnSc6	středisko
Bostonského	bostonský	k2eAgInSc2d1	bostonský
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Kusevického	Kusevický	k2eAgInSc2d1	Kusevický
asistentem	asistent	k1gMnSc7	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
fenomenálně	fenomenálně	k6eAd1	fenomenálně
"	"	kIx"	"
<g/>
zaskočil	zaskočit	k5eAaPmAgInS	zaskočit
<g/>
"	"	kIx"	"
za	za	k7c4	za
nemocného	mocný	k2eNgMnSc4d1	nemocný
Bruna	Bruno	k1gMnSc4	Bruno
Waltera	Walter	k1gMnSc4	Walter
u	u	k7c2	u
Newyorské	newyorský	k2eAgFnSc2d1	newyorská
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
od	od	k7c2	od
září	září	k1gNnSc2	září
1943	[number]	k4	1943
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
záskok	záskok	k1gInSc1	záskok
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
jeho	jeho	k3xOp3gFnSc4	jeho
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945-1947	[number]	k4	1945-1947
byl	být	k5eAaImAgInS	být
hudebním	hudební	k2eAgMnSc7d1	hudební
ředitelem	ředitel	k1gMnSc7	ředitel
Newyorského	newyorský	k2eAgInSc2d1	newyorský
Symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Sergeje	Sergej	k1gMnSc2	Sergej
Kusevického	Kusevický	k2eAgMnSc2d1	Kusevický
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
vedoucím	vedoucí	k1gMnSc7	vedoucí
třídy	třída	k1gFnSc2	třída
dirigování	dirigování	k1gNnPc2	dirigování
v	v	k7c6	v
Tanglewoodu	Tanglewood	k1gInSc6	Tanglewood
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
Bernstein	Bernstein	k1gMnSc1	Bernstein
přijal	přijmout	k5eAaPmAgMnS	přijmout
pozvání	pozvání	k1gNnSc4	pozvání
Palestinského	palestinský	k2eAgInSc2d1	palestinský
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
britské	britský	k2eAgFnSc6d1	britská
mandátní	mandátní	k2eAgFnSc6d1	mandátní
Palestině	Palestina	k1gFnSc6	Palestina
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
zážitcích	zážitek	k1gInPc6	zážitek
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
napsal	napsat	k5eAaBmAgInS	napsat
Bernstein	Bernstein	k1gInSc4	Bernstein
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
<g/>
:	:	kIx,	:
–	–	k?	–
<g/>
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
<g/>
,	,	kIx,	,
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
svému	svůj	k1gMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
V	v	k7c6	v
letech	let	k1gInPc6	let
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
postu	post	k1gInSc2	post
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
hudebním	hudební	k2eAgMnSc7d1	hudební
ředitelem	ředitel	k1gMnSc7	ředitel
Newyorské	newyorský	k2eAgFnSc2d1	newyorská
filharmonie	filharmonie	k1gFnSc2	filharmonie
(	(	kIx(	(
<g/>
831	[number]	k4	831
koncertů	koncert	k1gInPc2	koncert
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
hostoval	hostovat	k5eAaImAgInS	hostovat
u	u	k7c2	u
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
orchestrů	orchestr	k1gInPc2	orchestr
(	(	kIx(	(
<g/>
Bostonský	bostonský	k2eAgInSc1d1	bostonský
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
,	,	kIx,	,
Londýnští	londýnský	k2eAgMnPc1d1	londýnský
symfonikové	symfonik	k1gMnPc1	symfonik
<g/>
,	,	kIx,	,
Vídeňští	vídeňský	k2eAgMnPc1d1	vídeňský
filharmonikové	filharmonik	k1gMnPc1	filharmonik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operních	operní	k2eAgInPc2d1	operní
domů	dům	k1gInPc2	dům
a	a	k8xC	a
na	na	k7c6	na
hudebních	hudební	k2eAgInPc6d1	hudební
festivalech	festival	k1gInPc6	festival
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejzářivějších	zářivý	k2eAgFnPc2d3	nejzářivější
dirigentských	dirigentský	k2eAgFnPc2d1	dirigentská
hvězd	hvězda	k1gFnPc2	hvězda
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
osobní	osobní	k2eAgFnSc7d1	osobní
horlivostí	horlivost	k1gFnSc7	horlivost
oživil	oživit	k5eAaPmAgInS	oživit
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
Gustava	Gustav	k1gMnSc2	Gustav
Mahlera	Mahler	k1gMnSc2	Mahler
a	a	k8xC	a
Charlese	Charles	k1gMnSc2	Charles
Ivese	Ives	k1gMnSc2	Ives
Na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svými	svůj	k3xOyFgInPc7	svůj
televizními	televizní	k2eAgInPc7d1	televizní
popularizačními	popularizační	k2eAgInPc7d1	popularizační
pořady	pořad	k1gInPc7	pořad
o	o	k7c6	o
(	(	kIx(	(
<g/>
vážné	vážný	k2eAgFnSc3d1	vážná
<g/>
)	)	kIx)	)
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pořady	pořad	k1gInPc1	pořad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
získaly	získat	k5eAaPmAgInP	získat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
televizních	televizní	k2eAgNnPc2d1	televizní
ocenění	ocenění	k1gNnPc2	ocenění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
11	[number]	k4	11
Emmy	Emma	k1gFnPc1	Emma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
udělaly	udělat	k5eAaPmAgFnP	udělat
televizní	televizní	k2eAgFnSc4d1	televizní
hvězdu	hvězda	k1gFnSc4	hvězda
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgFnPc2d3	nejvýraznější
kulturních	kulturní	k2eAgFnPc2d1	kulturní
osobností	osobnost	k1gFnPc2	osobnost
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
právě	právě	k6eAd1	právě
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
přivedl	přivést	k5eAaPmAgMnS	přivést
mnoho	mnoho	k4c4	mnoho
Američanů	Američan	k1gMnPc2	Američan
k	k	k7c3	k
vážné	vážný	k2eAgFnSc3d1	vážná
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
začalo	začít	k5eAaPmAgNnS	začít
pořadem	pořad	k1gInSc7	pořad
"	"	kIx"	"
<g/>
Omnibus	omnibus	k1gInSc1	omnibus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
letech	let	k1gInPc6	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
deset	deset	k4xCc4	deset
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
Bernstein	Bernstein	k1gMnSc1	Bernstein
stal	stát	k5eAaPmAgMnS	stát
šéfdirigentem	šéfdirigent	k1gMnSc7	šéfdirigent
Newyorské	newyorský	k2eAgFnSc2d1	newyorská
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
,	,	kIx,	,
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
15	[number]	k4	15
programů	program	k1gInPc2	program
pro	pro	k7c4	pro
dospělé	dospělý	k2eAgInPc4d1	dospělý
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
a	a	k8xC	a
především	především	k9	především
53	[number]	k4	53
programů	program	k1gInPc2	program
pro	pro	k7c4	pro
mladé	mladý	k1gMnPc4	mladý
s	s	k7c7	s
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Young	Young	k1gMnSc1	Young
People	People	k1gMnSc1	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Concerts	Concerts	k1gInSc1	Concerts
with	witha	k1gFnPc2	witha
the	the	k?	the
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
Bernsteinových	Bernsteinův	k2eAgInPc2d1	Bernsteinův
pořadů	pořad	k1gInPc2	pořad
nejpopulárnější	populární	k2eAgFnSc1d3	nejpopulárnější
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
těchto	tento	k3xDgInPc6	tento
programech	program	k1gInPc6	program
Bernstein	Bernsteina	k1gFnPc2	Bernsteina
na	na	k7c6	na
televizní	televizní	k2eAgFnSc6d1	televizní
obrazovce	obrazovka	k1gFnSc6	obrazovka
zasvěcoval	zasvěcovat	k5eAaImAgInS	zasvěcovat
velké	velký	k2eAgMnPc4d1	velký
i	i	k8xC	i
malé	malý	k2eAgMnPc4d1	malý
diváky	divák	k1gMnPc4	divák
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
tajů	taj	k1gInPc2	taj
a	a	k8xC	a
"	"	kIx"	"
<g/>
záhad	záhada	k1gFnPc2	záhada
<g/>
"	"	kIx"	"
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
představoval	představovat	k5eAaImAgMnS	představovat
významné	významný	k2eAgFnPc4d1	významná
skladby	skladba	k1gFnPc4	skladba
a	a	k8xC	a
hudební	hudební	k2eAgMnPc4d1	hudební
skladatele	skladatel	k1gMnPc4	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Vybrané	vybraný	k2eAgInPc4d1	vybraný
scénáře	scénář	k1gInPc4	scénář
Koncertů	koncert	k1gInPc2	koncert
pro	pro	k7c4	pro
mladé	mladý	k2eAgNnSc4d1	mladé
publikum	publikum	k1gNnSc4	publikum
poté	poté	k6eAd1	poté
vyšly	vyjít	k5eAaPmAgFnP	vyjít
knižně	knižně	k6eAd1	knižně
–	–	k?	–
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
bestsellerem	bestseller	k1gInSc7	bestseller
-	-	kIx~	-
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
LB	LB	kA	LB
<g/>
:	:	kIx,	:
O	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
-	-	kIx~	-
Koncerty	koncert	k1gInPc1	koncert
pro	pro	k7c4	pro
mladé	mladý	k2eAgNnSc4d1	mladé
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NLN	NLN	kA	NLN
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
Bernsteinova	Bernsteinův	k2eAgInSc2d1	Bernsteinův
životopisu	životopis	k1gInSc2	životopis
Meryle	Meryl	k1gInSc5	Meryl
Secrest	Secrest	k1gMnSc1	Secrest
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
bisexualitě	bisexualita	k1gFnSc6	bisexualita
a	a	k8xC	a
cituje	citovat	k5eAaBmIp3nS	citovat
též	též	k9	též
jeho	jeho	k3xOp3gFnSc4	jeho
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Shirley	Shirlea	k1gFnSc2	Shirlea
Rhoades	Rhoades	k1gInSc1	Rhoades
Perle	perla	k1gFnSc3	perla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
muže	muž	k1gMnSc4	muž
sexuálně	sexuálně	k6eAd1	sexuálně
a	a	k8xC	a
ženy	žena	k1gFnPc4	žena
emocionálně	emocionálně	k6eAd1	emocionálně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
života	život	k1gInSc2	život
nahrál	nahrát	k5eAaBmAgMnS	nahrát
s	s	k7c7	s
Vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
filharmonií	filharmonie	k1gFnSc7	filharmonie
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Krystianem	Krystian	k1gMnSc7	Krystian
Zimermanem	Zimerman	k1gMnSc7	Zimerman
oba	dva	k4xCgInPc4	dva
Brahmsovy	Brahmsův	k2eAgInPc4d1	Brahmsův
klavírní	klavírní	k2eAgInPc4d1	klavírní
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
poslední	poslední	k2eAgInPc4d1	poslední
tři	tři	k4xCgInPc4	tři
klavírní	klavírní	k2eAgInPc4d1	klavírní
koncerty	koncert	k1gInPc4	koncert
od	od	k7c2	od
Ludwiga	Ludwig	k1gMnSc2	Ludwig
van	vana	k1gFnPc2	vana
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
odjel	odjet	k5eAaPmAgMnS	odjet
dirigovat	dirigovat	k5eAaImF	dirigovat
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
Beethovenovu	Beethovenův	k2eAgFnSc4d1	Beethovenova
Devátou	devátý	k4xOgFnSc4	devátý
<g/>
,	,	kIx,	,
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
pádu	pád	k1gInSc2	pád
Berlínské	berlínský	k2eAgFnSc3d1	Berlínská
zdi	zeď	k1gFnSc3	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Symfonie	symfonie	k1gFnSc1	symfonie
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
kvůli	kvůli	k7c3	kvůli
symbolice	symbolika	k1gFnSc3	symbolika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
sólisté	sólista	k1gMnPc1	sólista
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
zpívají	zpívat	k5eAaImIp3nP	zpívat
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Freiheit	Freiheit	k2eAgMnSc1d1	Freiheit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
Freude	Freud	k1gMnSc5	Freud
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
radost	radost	k1gFnSc4	radost
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
Bernstein	Bernsteina	k1gFnPc2	Bernsteina
schválně	schválně	k6eAd1	schválně
zaměnil	zaměnit	k5eAaPmAgMnS	zaměnit
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
oslavě	oslava	k1gFnSc3	oslava
bratrství	bratrství	k1gNnSc2	bratrství
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
svobody	svoboda	k1gFnSc2	svoboda
po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
se	se	k3xPyFc4	se
necítil	cítit	k5eNaImAgMnS	cítit
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
i	i	k9	i
na	na	k7c6	na
nahrávkách	nahrávka	k1gFnPc6	nahrávka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydržel	vydržet	k5eAaPmAgMnS	vydržet
a	a	k8xC	a
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
úplně	úplně	k6eAd1	úplně
vyčerpaný	vyčerpaný	k2eAgMnSc1d1	vyčerpaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
náchylný	náchylný	k2eAgInSc1d1	náchylný
na	na	k7c4	na
bolest	bolest	k1gFnSc4	bolest
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
i	i	k9	i
po	po	k7c6	po
dávkách	dávka	k1gFnPc6	dávka
lécích	lék	k1gInPc6	lék
nyní	nyní	k6eAd1	nyní
neustupovaly	ustupovat	k5eNaImAgFnP	ustupovat
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Vyhledal	vyhledat	k5eAaPmAgMnS	vyhledat
proto	proto	k8xC	proto
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
podceňoval	podceňovat	k5eAaImAgInS	podceňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bernstein	Bernstein	k1gInSc1	Bernstein
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
rentgenovém	rentgenový	k2eAgInSc6d1	rentgenový
snímku	snímek	k1gInSc6	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Zjistilo	zjistit	k5eAaPmAgNnS	zjistit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
maligní	maligní	k2eAgInSc4d1	maligní
tumor	tumor	k1gInSc4	tumor
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
pohrudnice	pohrudnice	k1gFnSc2	pohrudnice
u	u	k7c2	u
levé	levý	k2eAgFnSc2d1	levá
plíce	plíce	k1gFnSc2	plíce
<g/>
,	,	kIx,	,
mezoteliom	mezoteliom	k1gInSc4	mezoteliom
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zhruba	zhruba	k6eAd1	zhruba
měsíční	měsíční	k2eAgFnSc6d1	měsíční
léčbě	léčba	k1gFnSc6	léčba
rakoviny	rakovina	k1gFnSc2	rakovina
se	se	k3xPyFc4	se
zdál	zdát	k5eAaImAgInS	zdát
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
slabší	slabý	k2eAgInPc1d2	slabší
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
prodělal	prodělat	k5eAaPmAgMnS	prodělat
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
a	a	k8xC	a
pásový	pásový	k2eAgInSc4d1	pásový
opar	opar	k1gInSc4	opar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
poslední	poslední	k2eAgInSc1d1	poslední
koncert	koncert	k1gInSc1	koncert
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
Tanglewoodu	Tanglewood	k1gInSc6	Tanglewood
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
se	se	k3xPyFc4	se
Beethovenova	Beethovenův	k2eAgFnSc1d1	Beethovenova
Sedmá	sedmý	k4xOgFnSc1	sedmý
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
,	,	kIx,	,
tempa	tempo	k1gNnSc2	tempo
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
pomalá	pomalý	k2eAgFnSc1d1	pomalá
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
už	už	k6eAd1	už
upozorňovala	upozorňovat	k5eAaImAgFnS	upozorňovat
na	na	k7c4	na
neodvratitelné	odvratitelný	k2eNgNnSc4d1	neodvratitelné
<g/>
.	.	kIx.	.
</s>
<s>
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rezidenci	rezidence	k1gFnSc6	rezidence
Dakota	Dakota	k1gFnSc1	Dakota
House	house	k1gNnSc4	house
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
půl	půl	k1xP	půl
sedmé	sedmý	k4xOgFnSc2	sedmý
večer	večer	k6eAd1	večer
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
záchvat	záchvat	k1gInSc4	záchvat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
bral	brát	k5eAaImAgMnS	brát
různé	různý	k2eAgInPc4d1	různý
medikamenty	medikament	k1gInPc4	medikament
a	a	k8xC	a
povzbuzující	povzbuzující	k2eAgInPc4d1	povzbuzující
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
hektický	hektický	k2eAgInSc4d1	hektický
dirigentský	dirigentský	k2eAgInSc4d1	dirigentský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
skladatele	skladatel	k1gMnSc4	skladatel
ho	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
postmodernistu	postmodernista	k1gMnSc4	postmodernista
už	už	k6eAd1	už
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nerozlišoval	rozlišovat	k5eNaImAgMnS	rozlišovat
mezi	mezi	k7c7	mezi
hudbou	hudba	k1gFnSc7	hudba
"	"	kIx"	"
<g/>
vážnou	vážný	k2eAgFnSc7d1	vážná
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
populární	populární	k2eAgMnSc1d1	populární
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
dobrou	dobrý	k2eAgFnSc4d1	dobrá
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
špatnou	špatný	k2eAgFnSc7d1	špatná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bernstein	Bernstein	k1gMnSc1	Bernstein
byl	být	k5eAaImAgMnS	být
stylový	stylový	k2eAgMnSc1d1	stylový
syntetik	syntetik	k1gMnSc1	syntetik
a	a	k8xC	a
"	"	kIx"	"
<g/>
eklektik	eklektik	k1gMnSc1	eklektik
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
nějaké	nějaký	k3yIgInPc1	nějaký
umělecké	umělecký	k2eAgInPc1d1	umělecký
škatulkování	škatulkování	k?	škatulkování
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
neplatilo	platit	k5eNaImAgNnS	platit
<g/>
:	:	kIx,	:
V	v	k7c6	v
artificiálních	artificiální	k2eAgFnPc6d1	artificiální
skladbách	skladba	k1gFnPc6	skladba
<g/>
,	,	kIx,	,
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
tonálních	tonální	k2eAgInPc2d1	tonální
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgInS	používat
latinskoamerické	latinskoamerický	k2eAgInPc4d1	latinskoamerický
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
výrazný	výrazný	k2eAgInSc1d1	výrazný
rytmus	rytmus	k1gInSc1	rytmus
(	(	kIx(	(
<g/>
synkopování	synkopování	k1gNnSc1	synkopování
<g/>
,	,	kIx,	,
střídavé	střídavý	k2eAgInPc1d1	střídavý
rytmy	rytmus	k1gInPc1	rytmus
<g/>
,	,	kIx,	,
nesouměrné	souměrný	k2eNgNnSc1d1	nesouměrné
metrum	metrum	k1gNnSc1	metrum
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
barvitou	barvitý	k2eAgFnSc4d1	barvitá
instrumentaci	instrumentace	k1gFnSc4	instrumentace
(	(	kIx(	(
<g/>
časté	častý	k2eAgNnSc1d1	časté
využívání	využívání	k1gNnSc1	využívání
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgFnSc2d1	vysoká
polohy	poloha	k1gFnSc2	poloha
žesťů	žestě	k1gInPc2	žestě
<g/>
,	,	kIx,	,
početných	početný	k2eAgFnPc2d1	početná
<g/>
,	,	kIx,	,
virtuózních	virtuózní	k2eAgFnPc2d1	virtuózní
perkusí	perkuse	k1gFnPc2	perkuse
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
muzikálovou	muzikálový	k2eAgFnSc4d1	muzikálová
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
formu	forma	k1gFnSc4	forma
pozvedl	pozvednout	k5eAaPmAgMnS	pozvednout
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jazzová	jazzový	k2eAgFnSc1d1	jazzová
fuga	fuga	k1gFnSc1	fuga
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Cool	Coola	k1gFnPc2	Coola
<g/>
)	)	kIx)	)
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
díla	dílo	k1gNnPc4	dílo
populární	populární	k2eAgFnSc2d1	populární
kultury	kultura	k1gFnSc2	kultura
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
velmi	velmi	k6eAd1	velmi
hodnotná	hodnotný	k2eAgFnSc1d1	hodnotná
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
nejde	jít	k5eNaImIp3nS	jít
vůbec	vůbec	k9	vůbec
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
předem	předem	k6eAd1	předem
vymezených	vymezený	k2eAgFnPc2d1	vymezená
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
polystylová	polystylový	k2eAgFnSc1d1	polystylový
a	a	k8xC	a
polyžánrová	polyžánrový	k2eAgFnSc1d1	polyžánrový
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
"	"	kIx"	"
<g/>
od	od	k7c2	od
všeho	všecek	k3xTgNnSc2	všecek
něco	něco	k6eAd1	něco
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Náměty	námět	k1gInPc1	námět
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
duchovní	duchovní	k2eAgMnPc1d1	duchovní
i	i	k9	i
čistě	čistě	k6eAd1	čistě
světské	světský	k2eAgFnPc4d1	světská
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neobracejí	obracet	k5eNaImIp3nP	obracet
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přímo	přímo	k6eAd1	přímo
reflektují	reflektovat	k5eAaImIp3nP	reflektovat
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
jejich	jejich	k3xOp3gMnSc1	jejich
autor	autor	k1gMnSc1	autor
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
tvořil	tvořit	k5eAaImAgInS	tvořit
<g/>
:	:	kIx,	:
hledají	hledat	k5eAaImIp3nP	hledat
víru	víra	k1gFnSc4	víra
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
opěvují	opěvovat	k5eAaImIp3nP	opěvovat
radovánky	radovánka	k1gFnPc4	radovánka
a	a	k8xC	a
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
problémy	problém	k1gInPc1	problém
soudobého	soudobý	k2eAgMnSc2d1	soudobý
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
kompozic	kompozice	k1gFnPc2	kompozice
se	s	k7c7	s
zpěvem	zpěv	k1gInSc7	zpěv
je	být	k5eAaImIp3nS	být
vícejazyčná	vícejazyčný	k2eAgFnSc1d1	vícejazyčná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mísí	mísit	k5eAaImIp3nP	mísit
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
další	další	k2eAgInPc4d1	další
jazyky	jazyk	k1gInPc4	jazyk
včetně	včetně	k7c2	včetně
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Fancy	Fanc	k2eAgInPc1d1	Fanc
Free	Fre	k1gInPc1	Fre
–	–	k?	–
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
On	on	k3xPp3gInSc1	on
the	the	k?	the
Town	Town	k1gInSc1	Town
–	–	k?	–
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
Facsimile	Facsimila	k1gFnSc6	Facsimila
–	–	k?	–
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
Peter	Peter	k1gMnSc1	Peter
Pan	Pan	k1gMnSc1	Pan
–	–	k?	–
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
Trouble	Trouble	k1gFnSc1	Trouble
in	in	k?	in
Tahiti	Tahiti	k1gNnSc1	Tahiti
–	–	k?	–
opera	opera	k1gFnSc1	opera
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
aktu	akt	k1gInSc6	akt
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
Wonderful	Wonderful	k1gInSc1	Wonderful
Town	Town	k1gInSc4	Town
–	–	k?	–
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g />
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gInSc1	on
the	the	k?	the
Waterfront	Waterfront	k1gInSc1	Waterfront
–	–	k?	–
filmová	filmový	k2eAgFnSc1d1	filmová
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
Candide	Candid	k1gInSc5	Candid
–	–	k?	–
muzikál-opera	muzikálper	k1gMnSc2	muzikál-oper
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
Story	story	k1gFnSc2	story
–	–	k?	–
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
Mass	Mass	k1gInSc1	Mass
–	–	k?	–
muzikálové	muzikálový	k2eAgNnSc1d1	muzikálové
oratorium	oratorium	k1gNnSc1	oratorium
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
Dybbuk	Dybbuk	k1gInSc1	Dybbuk
–	–	k?	–
balet	balet	k1gInSc1	balet
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
1600	[number]	k4	1600
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
Avenue	avenue	k1gFnSc2	avenue
–	–	k?	–
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
A	a	k8xC	a
Quiet	Quiet	k1gInSc1	Quiet
Place	plac	k1gInSc6	plac
–	–	k?	–
opera	opera	k1gFnSc1	opera
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
dvou	dva	k4xCgNnPc6	dva
aktech	akta	k1gNnPc6	akta
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
/	/	kIx~	/
<g/>
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Trouble	Trouble	k1gFnSc1	Trouble
in	in	k?	in
Tahiti	Tahiti	k1gNnSc1	Tahiti
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Race	Rac	k1gFnSc2	Rac
to	ten	k3xDgNnSc4	ten
Urga	Urga	k1gFnSc1	Urga
–	–	k?	–
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
Jeremiah	Jeremiah	k1gInSc1	Jeremiah
–	–	k?	–
Symfonie	symfonie	k1gFnSc2	symfonie
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
Three	Three	k1gNnSc2	Three
Dance	Danka	k1gFnSc6	Danka
Episodes	Episodes	k1gMnSc1	Episodes
from	from	k1gMnSc1	from
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Town	Town	k1gNnSc1	Town
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
Three	Thre	k1gInSc2	Thre
Dance	Danka	k1gFnSc3	Danka
<g />
.	.	kIx.	.
</s>
<s>
Variations	Variations	k6eAd1	Variations
from	from	k6eAd1	from
"	"	kIx"	"
<g/>
Fancy	Fanc	k2eAgInPc1d1	Fanc
Free	Fre	k1gInPc1	Fre
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
The	The	k1gMnPc2	The
Age	Age	k1gFnSc2	Age
of	of	k?	of
Anxiety	Anxieta	k1gFnSc2	Anxieta
–	–	k?	–
symfonie	symfonie	k1gFnSc2	symfonie
č.	č.	k?	č.
2	[number]	k4	2
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
W.	W.	kA	W.
H.	H.	kA	H.
Audena	Audena	k1gFnSc1	Audena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
Serenade	Serenad	k1gInSc5	Serenad
–	–	k?	–
pro	pro	k7c4	pro
sólové	sólový	k2eAgFnPc4d1	sólová
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
smyčcové	smyčcový	k2eAgInPc4d1	smyčcový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
harfu	harfa	k1gFnSc4	harfa
a	a	k8xC	a
bicí	bicí	k2eAgNnSc4d1	bicí
<g />
.	.	kIx.	.
</s>
<s>
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
Prelude	Prelud	k1gInSc5	Prelud
<g/>
,	,	kIx,	,
Fugue	Fugue	k1gNnSc3	Fugue
and	and	k?	and
Riffs	Riffs	k1gInSc1	Riffs
–	–	k?	–
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klarinet	klarinet	k1gInSc4	klarinet
a	a	k8xC	a
jazzový	jazzový	k2eAgInSc1d1	jazzový
ansámbl	ansámbl	k1gInSc1	ansámbl
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
Symphonic	Symphonice	k1gFnPc2	Symphonice
Suite	Suit	k1gInSc5	Suit
from	from	k1gMnSc1	from
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Waterfront	Waterfronto	k1gNnPc2	Waterfronto
<g/>
"	"	kIx"	"
–	–	k?	–
symfonická	symfonický	k2eAgFnSc1d1	symfonická
suita	suita	k1gFnSc1	suita
z	z	k7c2	z
On	on	k3xPp3gInSc1	on
the	the	k?	the
Waterfront	Waterfront	k1gInSc1	Waterfront
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
Symphonic	Symphonice	k1gFnPc2	Symphonice
Dances	Dancesa	k1gFnPc2	Dancesa
from	from	k1gInSc1	from
"	"	kIx"	"
<g/>
West	West	k2eAgInSc1d1	West
Side	Side	k1gInSc1	Side
Story	story	k1gFnSc1	story
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
symfonické	symfonický	k2eAgInPc4d1	symfonický
tance	tanec	k1gInPc4	tanec
z	z	k7c2	z
West	Westa	k1gFnPc2	Westa
Side	Sid	k1gFnSc2	Sid
Story	story	k1gFnSc2	story
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
Kaddish	Kaddish	k1gInSc1	Kaddish
–	–	k?	–
Symfonie	symfonie	k1gFnSc2	symfonie
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
smíšený	smíšený	k2eAgInSc4d1	smíšený
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
chlapecký	chlapecký	k2eAgInSc4d1	chlapecký
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
recitátora	recitátor	k1gMnSc4	recitátor
a	a	k8xC	a
sopránové	sopránový	k2eAgNnSc1d1	sopránové
sólo	sólo	k1gNnSc1	sólo
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
Dybbuk	Dybbuk	k1gInSc1	Dybbuk
–	–	k?	–
suity	suita	k1gFnSc2	suita
č.	č.	k?	č.
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g />
.	.	kIx.	.
</s>
<s>
Songfest	Songfest	k1gFnSc1	Songfest
<g/>
:	:	kIx,	:
A	a	k9	a
Cycle	Cyclo	k1gNnSc6	Cyclo
of	of	k?	of
American	American	k1gInSc1	American
Poems	Poemsa	k1gFnPc2	Poemsa
for	forum	k1gNnPc2	forum
Six	Six	k1gMnSc1	Six
Singers	Singers	k1gInSc1	Singers
and	and	k?	and
Orchestra	orchestra	k1gFnSc1	orchestra
–	–	k?	–
Slavnost	slavnost	k1gFnSc1	slavnost
písní	píseň	k1gFnPc2	píseň
<g/>
:	:	kIx,	:
Cyklus	cyklus	k1gInSc1	cyklus
amerických	americký	k2eAgFnPc2d1	americká
básní	báseň	k1gFnPc2	báseň
pro	pro	k7c4	pro
šest	šest	k4xCc4	šest
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
Three	Thre	k1gInSc2	Thre
Meditations	Meditations	k1gInSc1	Meditations
from	from	k1gInSc1	from
"	"	kIx"	"
<g/>
Mass	Mass	k1gInSc1	Mass
<g/>
"	"	kIx"	"
–	–	k?	–
tři	tři	k4xCgFnPc4	tři
meditace	meditace	k1gFnPc1	meditace
z	z	k7c2	z
"	"	kIx"	"
<g/>
Mass	Massa	k1gFnPc2	Massa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
for	forum	k1gNnPc2	forum
<g />
.	.	kIx.	.
</s>
<s>
violoncello	violoncello	k1gNnSc1	violoncello
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
Divertimento	divertimento	k1gNnSc1	divertimento
for	forum	k1gNnPc2	forum
Orchestra	orchestra	k1gFnSc1	orchestra
–	–	k?	–
1980	[number]	k4	1980
Halil	halit	k5eAaImAgInS	halit
–	–	k?	–
nokturné	nokturný	k2eAgFnSc2d1	nokturný
pro	pro	k7c4	pro
sólovou	sólový	k2eAgFnSc4d1	sólová
flétnu	flétna	k1gFnSc4	flétna
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
harfu	harfa	k1gFnSc4	harfa
a	a	k8xC	a
smyčce	smyčec	k1gInPc4	smyčec
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Concerto	Concerta	k1gFnSc5	Concerta
for	forum	k1gNnPc2	forum
Orchestra	orchestra	k1gFnSc1	orchestra
–	–	k?	–
1989	[number]	k4	1989
Hashkiveinu	Hashkivein	k2eAgFnSc4d1	Hashkivein
–	–	k?	–
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
tenor	tenor	k1gInSc4	tenor
<g/>
,	,	kIx,	,
smíšený	smíšený	k2eAgInSc4d1	smíšený
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
varhany	varhany	k1gInPc4	varhany
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
Chichester	Chichestrum	k1gNnPc2	Chichestrum
Psalms	Psalmsa	k1gFnPc2	Psalmsa
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
1965	[number]	k4	1965
Missa	Missa	k1gFnSc1	Missa
Brevis	Brevis	k1gFnSc1	Brevis
–	–	k?	–
pro	pro	k7c4	pro
smíšený	smíšený	k2eAgInSc4d1	smíšený
sbor	sbor	k1gInSc4	sbor
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc1d1	vysoký
tenor	tenor	k1gInSc1	tenor
(	(	kIx(	(
<g/>
mužský	mužský	k2eAgInSc1d1	mužský
alt	alt	k1gInSc1	alt
<g/>
)	)	kIx)	)
s	s	k7c7	s
bicími	bicí	k2eAgInPc7d1	bicí
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Sonáta	sonáta	k1gFnSc1	sonáta
-	-	kIx~	-
pro	pro	k7c4	pro
klarinet	klarinet	k1gInSc4	klarinet
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
Touches	Touchesa	k1gFnPc2	Touchesa
-	-	kIx~	-
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
sólo	sólo	k1gNnSc4	sólo
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
Thirteen	Thirteen	k2eAgInSc1d1	Thirteen
Anniversaries	Anniversaries	k1gInSc1	Anniversaries
-	-	kIx~	-
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
sólo	sólo	k1gNnSc4	sólo
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
Dance	Danka	k1gFnSc6	Danka
<g />
.	.	kIx.	.
</s>
<s>
Suite	Suite	k5eAaPmIp2nP	Suite
-	-	kIx~	-
pro	pro	k7c4	pro
žesťový	žesťový	k2eAgInSc4d1	žesťový
kvintet	kvintet	k1gInSc4	kvintet
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
I	I	kA	I
Hate	Hate	k1gFnPc2	Hate
Music	Musice	k1gFnPc2	Musice
<g/>
:	:	kIx,	:
A	a	k9	a
cycle	cyclo	k1gNnSc6	cyclo
of	of	k?	of
Five	Five	k1gInSc1	Five
Kids	Kids	k1gInSc1	Kids
Songs	Songsa	k1gFnPc2	Songsa
for	forum	k1gNnPc2	forum
Soprano	Soprana	k1gFnSc5	Soprana
and	and	k?	and
Piano	piano	k1gNnSc1	piano
–	–	k?	–
Nenávidím	nenávidět	k5eAaImIp1nS	nenávidět
hudbu	hudba	k1gFnSc4	hudba
<g/>
:	:	kIx,	:
Cyklus	cyklus	k1gInSc1	cyklus
pěti	pět	k4xCc2	pět
dětských	dětský	k2eAgFnPc2d1	dětská
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
soprán	soprán	k1gInSc4	soprán
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
La	la	k1gNnSc2	la
Bonne	Bonn	k1gInSc5	Bonn
Cuisine	Cuisin	k1gInSc5	Cuisin
<g/>
:	:	kIx,	:
Four	Four	k1gInSc4	Four
Recipes	Recipes	k1gInSc4	Recipes
for	forum	k1gNnPc2	forum
Voice	Voice	k1gMnSc2	Voice
and	and	k?	and
Piano	piano	k6eAd1	piano
–	–	k?	–
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
:	:	kIx,	:
Pět	pět	k4xCc1	pět
receptů	recept	k1gInPc2	recept
pro	pro	k7c4	pro
vokál	vokál	k1gInSc4	vokál
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
Arias	Arias	k1gInSc1	Arias
and	and	k?	and
Barcarolles	Barcarolles	k1gInSc1	Barcarolles
–	–	k?	–
Árie	árie	k1gFnSc2	árie
a	a	k8xC	a
barkaroly	barkarola	k1gFnSc2	barkarola
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
mezzosoprán	mezzosoprán	k1gInSc4	mezzosoprán
<g/>
,	,	kIx,	,
baryton	baryton	k1gInSc4	baryton
a	a	k8xC	a
čtyřruční	čtyřruční	k2eAgInSc4d1	čtyřruční
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
(	(	kIx(	(
<g/>
orchestrováno	orchestrován	k2eAgNnSc1d1	orchestrován
<g/>
)	)	kIx)	)
</s>
