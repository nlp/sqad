<s>
Leonard	Leonard	k1gMnSc1	Leonard
Bernstein	Bernstein	k1gMnSc1	Bernstein
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Lawrence	Lawrence	k1gFnSc1	Lawrence
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
popularizátor	popularizátor	k1gMnSc1	popularizátor
a	a	k8xC	a
televizní	televizní	k2eAgFnSc1d1	televizní
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
