<s>
Hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgFnSc2d1	veveří
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západě	západ	k1gInSc6	západ
brněnské	brněnský	k2eAgFnSc2d1	brněnská
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
12	[number]	k4	12
kilometrů	kilometr	k1gInPc2	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
ostrohu	ostroh	k1gInSc6	ostroh
nad	nad	k7c7	nad
Brněnskou	brněnský	k2eAgFnSc7d1	brněnská
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
napájí	napájet	k5eAaImIp3nS	napájet
řeka	řeka	k1gFnSc1	řeka
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejrozsáhlejších	rozsáhlý	k2eAgInPc2d3	nejrozsáhlejší
a	a	k8xC	a
nejstarších	starý	k2eAgInPc2d3	nejstarší
hradních	hradní	k2eAgInPc2d1	hradní
areálů	areál	k1gInPc2	areál
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Stranou	stranou	k6eAd1	stranou
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
při	při	k7c6	při
příjezdové	příjezdový	k2eAgFnSc6d1	příjezdová
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
západní	západní	k2eAgFnSc3d1	západní
vstupní	vstupní	k2eAgFnSc3d1	vstupní
bráně	brána	k1gFnSc3	brána
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
kaple	kaple	k1gFnSc1	kaple
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
z	z	k7c2	z
konce	konec	k1gInSc2	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
veverského	veverský	k2eAgNnSc2d1	veverské
panství	panství	k1gNnSc2	panství
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
veversko-říčanského	veversko-říčanský	k2eAgNnSc2d1	veversko-říčanský
panství	panství	k1gNnSc2	panství
<g/>
)	)	kIx)	)
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
patřil	patřit	k5eAaImAgMnS	patřit
do	do	k7c2	do
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
obce	obec	k1gFnSc2	obec
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1957	[number]	k4	1957
však	však	k9	však
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
krajského	krajský	k2eAgInSc2d1	krajský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
připojen	připojit	k5eAaPmNgInS	připojit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
územím	území	k1gNnSc7	území
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
a	a	k8xC	a
začleněn	začlenit	k5eAaPmNgMnS	začlenit
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
obvodu	obvod	k1gInSc2	obvod
Brno	Brno	k1gNnSc4	Brno
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
zprvu	zprvu	k6eAd1	zprvu
tvořil	tvořit	k5eAaImAgInS	tvořit
samostatné	samostatný	k2eAgNnSc4d1	samostatné
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
I	i	k9	i
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
po	po	k7c4	po
připojení	připojení	k1gNnSc4	připojení
Bystrce	Bystrc	k1gFnSc2	Bystrc
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
administrativně	administrativně	k6eAd1	administrativně
podřízeno	podřízen	k2eAgNnSc1d1	podřízeno
bystrckému	bystrcký	k2eAgInSc3d1	bystrcký
MNV	MNV	kA	MNV
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
radikální	radikální	k2eAgFnSc3d1	radikální
katastrální	katastrální	k2eAgFnSc3d1	katastrální
reformě	reforma	k1gFnSc3	reforma
Brna	Brno	k1gNnSc2	Brno
provedené	provedený	k2eAgFnPc1d1	provedená
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
celým	celý	k2eAgInSc7d1	celý
katastrem	katastr	k1gInSc7	katastr
Veverská	Veverský	k2eAgFnSc1d1	Veverská
Bítýška	Bítýška	k1gFnSc1	Bítýška
I	i	k8xC	i
přičleněn	přičleněn	k2eAgInSc1d1	přičleněn
ke	k	k7c3	k
katastrálnímu	katastrální	k2eAgNnSc3d1	katastrální
území	území	k1gNnSc3	území
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
správu	správa	k1gFnSc4	správa
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgInSc1d1	veveří
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
založen	založit	k5eAaPmNgInS	založit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1059	[number]	k4	1059
moravským	moravský	k2eAgMnSc7d1	moravský
údělným	údělný	k2eAgMnSc7d1	údělný
knížetem	kníže	k1gMnSc7	kníže
Konrádem	Konrád	k1gMnSc7	Konrád
I.	I.	kA	I.
Brněnským	brněnský	k2eAgFnPc3d1	brněnská
<g/>
;	;	kIx,	;
první	první	k4xOgInSc4	první
písemný	písemný	k2eAgInSc4d1	písemný
doklad	doklad	k1gInSc4	doklad
o	o	k7c4	o
Veveří	veveří	k2eAgNnSc4d1	veveří
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1213	[number]	k4	1213
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
správcem	správce	k1gMnSc7	správce
Štěpán	Štěpána	k1gFnPc2	Štěpána
<g/>
,	,	kIx,	,
pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
předek	předek	k1gInSc1	předek
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
pozdně	pozdně	k6eAd1	pozdně
románský	románský	k2eAgInSc1d1	románský
a	a	k8xC	a
raně	raně	k6eAd1	raně
gotický	gotický	k2eAgInSc1d1	gotický
hrad	hrad	k1gInSc1	hrad
zabral	zabrat	k5eAaPmAgInS	zabrat
úzkou	úzký	k2eAgFnSc4d1	úzká
část	část	k1gFnSc4	část
ostrohu	ostroh	k1gInSc2	ostroh
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
zde	zde	k6eAd1	zde
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
umisťoval	umisťovat	k5eAaImAgMnS	umisťovat
své	svůj	k3xOyFgMnPc4	svůj
politické	politický	k2eAgMnPc4d1	politický
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejznámější	známý	k2eAgMnPc4d3	nejznámější
byl	být	k5eAaImAgInS	být
komorník	komorník	k1gMnSc1	komorník
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Miloty	milota	k1gFnSc2	milota
z	z	k7c2	z
Dědic	Dědice	k1gFnPc2	Dědice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1311	[number]	k4	1311
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
zastaven	zastavit	k5eAaPmNgInS	zastavit
Janovi	Jan	k1gMnSc6	Jan
z	z	k7c2	z
Vartenberka	Vartenberka	k1gFnSc1	Vartenberka
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
roku	rok	k1gInSc2	rok
1334	[number]	k4	1334
usiloval	usilovat	k5eAaImAgMnS	usilovat
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
navrácení	navrácení	k1gNnSc4	navrácení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
markrabě	markrabě	k1gMnSc1	markrabě
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
hrad	hrad	k1gInSc4	hrad
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
opevňoval	opevňovat	k5eAaImAgMnS	opevňovat
a	a	k8xC	a
užíval	užívat	k5eAaImAgMnS	užívat
ho	on	k3xPp3gNnSc4	on
jako	jako	k9	jako
zemskou	zemský	k2eAgFnSc4d1	zemská
pevnost	pevnost	k1gFnSc4	pevnost
i	i	k9	i
jako	jako	k8xC	jako
pokladnici	pokladnice	k1gFnSc4	pokladnice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
posádka	posádka	k1gFnSc1	posádka
vévody	vévoda	k1gMnSc2	vévoda
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
a	a	k8xC	a
Veveří	veveří	k2eAgNnSc1d1	veveří
bylo	být	k5eAaImAgNnS	být
proto	proto	k6eAd1	proto
roku	rok	k1gInSc2	rok
1424	[number]	k4	1424
obléháno	obléhán	k2eAgNnSc4d1	obléháno
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
česko-uherských	českoherský	k2eAgFnPc2d1	česko-uherská
válek	válka	k1gFnPc2	válka
hrad	hrad	k1gInSc1	hrad
držel	držet	k5eAaImAgMnS	držet
těšínský	těšínský	k2eAgMnSc1d1	těšínský
kníže	kníže	k1gMnSc1	kníže
Přemek	Přemek	k1gMnSc1	Přemek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
příznivcem	příznivec	k1gMnSc7	příznivec
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1468	[number]	k4	1468
dobyt	dobýt	k5eAaPmNgInS	dobýt
vojskem	vojsko	k1gNnSc7	vojsko
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gMnSc2	Korvín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
hrad	hrad	k1gInSc4	hrad
získal	získat	k5eAaPmAgMnS	získat
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Ludanic	Ludanice	k1gFnPc2	Ludanice
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
střídali	střídat	k5eAaImAgMnP	střídat
další	další	k2eAgMnPc1d1	další
majitelé	majitel	k1gMnPc1	majitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obléhání	obléhání	k1gNnSc3	obléhání
Brna	Brno	k1gNnSc2	Brno
švédskými	švédský	k2eAgNnPc7d1	švédské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obležen	obležen	k2eAgInSc4d1	obležen
i	i	k8xC	i
nedaleký	daleký	k2eNgInSc4d1	nedaleký
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
náporu	nápor	k1gInSc6	nápor
útočníků	útočník	k1gMnPc2	útočník
odolal	odolat	k5eAaPmAgMnS	odolat
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
panství	panství	k1gNnSc1	panství
bylo	být	k5eAaImAgNnS	být
vojsky	vojsky	k6eAd1	vojsky
generála	generál	k1gMnSc2	generál
Torstenssona	Torstensson	k1gMnSc2	Torstensson
vydrancováno	vydrancován	k2eAgNnSc4d1	vydrancováno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1707	[number]	k4	1707
získali	získat	k5eAaPmAgMnP	získat
veverské	veverský	k2eAgNnSc4d1	veverské
panství	panství	k1gNnSc4	panství
Sinzendorfové	Sinzendorf	k1gMnPc1	Sinzendorf
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hrad	hrad	k1gInSc4	hrad
upravili	upravit	k5eAaPmAgMnP	upravit
barokně	barokně	k6eAd1	barokně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
úprav	úprava	k1gFnPc2	úprava
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
fresková	freskový	k2eAgFnSc1d1	fresková
výzdoba	výzdoba	k1gFnSc1	výzdoba
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
,	,	kIx,	,
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
okolí	okolí	k1gNnSc4	okolí
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc4	hrad
dobyt	dobyt	k2eAgInSc4d1	dobyt
a	a	k8xC	a
vypleněn	vypleněn	k2eAgInSc4d1	vypleněn
pruskými	pruský	k2eAgNnPc7d1	pruské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
prodal	prodat	k5eAaPmAgInS	prodat
hrad	hrad	k1gInSc4	hrad
i	i	k9	i
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
kníže	kníže	k1gMnSc1	kníže
Prosper	Prosper	k1gMnSc1	Prosper
ze	z	k7c2	z
Sinzendorfu	Sinzendorf	k1gInSc2	Sinzendorf
průmyslníkovi	průmyslník	k1gMnSc3	průmyslník
Vilému	Vilém	k1gMnSc3	Vilém
Mundymu	Mundym	k1gInSc2	Mundym
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
spojil	spojit	k5eAaPmAgMnS	spojit
se	s	k7c7	s
sousedním	sousední	k2eAgNnSc7d1	sousední
tišnovským	tišnovský	k2eAgNnSc7d1	tišnovské
panstvím	panství	k1gNnSc7	panství
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tak	tak	k6eAd1	tak
v	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
zázemí	zázemí	k1gNnSc6	zázemí
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
dominium	dominium	k1gNnSc4	dominium
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
koupil	koupit	k5eAaPmAgInS	koupit
hrad	hrad	k1gInSc1	hrad
od	od	k7c2	od
Mundyho	Mundy	k1gMnSc2	Mundy
dědice	dědic	k1gMnSc2	dědic
Jana	Jan	k1gMnSc2	Jan
syn	syn	k1gMnSc1	syn
sesazeného	sesazený	k2eAgMnSc2d1	sesazený
švédského	švédský	k2eAgMnSc2d1	švédský
krále	král	k1gMnSc2	král
Gustava	Gustav	k1gMnSc2	Gustav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc2	Adolf
Gustav	Gustav	k1gMnSc1	Gustav
Vasa	Vasa	k1gMnSc1	Vasa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zamýšlel	zamýšlet	k5eAaImAgMnS	zamýšlet
na	na	k7c6	na
předhradí	předhradí	k1gNnSc6	předhradí
vystavět	vystavět	k5eAaPmF	vystavět
rodovou	rodový	k2eAgFnSc4d1	rodová
hrobku	hrobka	k1gFnSc4	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
se	se	k3xPyFc4	se
panství	panství	k1gNnSc2	panství
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
podnikatele	podnikatel	k1gMnSc2	podnikatel
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
Jiřího	Jiří	k1gMnSc2	Jiří
Šimona	Šimon	k1gMnSc2	Šimon
Siny	sinus	k1gInPc1	sinus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1856-81	[number]	k4	1856-81
patřilo	patřit	k5eAaImAgNnS	patřit
panství	panství	k1gNnSc1	panství
jeho	jeho	k3xOp3gInSc2	jeho
vnučce	vnučka	k1gFnSc6	vnučka
Heleně	Helena	k1gFnSc6	Helena
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
iniciativy	iniciativa	k1gFnSc2	iniciativa
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
k	k	k7c3	k
výrazným	výrazný	k2eAgFnPc3d1	výrazná
úpravám	úprava	k1gFnPc3	úprava
hradního	hradní	k2eAgNnSc2d1	hradní
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
stráni	stráň	k1gFnSc6	stráň
hradního	hradní	k2eAgInSc2d1	hradní
ochozu	ochoz	k1gInSc2	ochoz
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
anglický	anglický	k2eAgInSc1d1	anglický
park	park	k1gInSc1	park
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
zdevastovanými	zdevastovaný	k2eAgInPc7d1	zdevastovaný
<g/>
)	)	kIx)	)
skleníky	skleník	k1gInPc7	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
majitelem	majitel	k1gMnSc7	majitel
Mořic	Mořic	k1gMnSc1	Mořic
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
Hirsch-Gereuth	Hirsch-Gereuth	k1gMnSc1	Hirsch-Gereuth
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgMnSc1d1	židovský
filantrop	filantrop	k1gMnSc1	filantrop
a	a	k8xC	a
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Baron	baron	k1gMnSc1	baron
Hisch	Hisch	k1gMnSc1	Hisch
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
pobýval	pobývat	k5eAaImAgInS	pobývat
na	na	k7c4	na
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
velkým	velký	k2eAgInSc7d1	velký
nákladem	náklad	k1gInSc7	náklad
opravil	opravit	k5eAaPmAgMnS	opravit
střechy	střecha	k1gFnPc4	střecha
většiny	většina	k1gFnSc2	většina
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
telefon	telefon	k1gInSc4	telefon
a	a	k8xC	a
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
dřevěný	dřevěný	k2eAgInSc4d1	dřevěný
vodovod	vodovod	k1gInSc4	vodovod
za	za	k7c4	za
kovový	kovový	k2eAgInSc4d1	kovový
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
však	však	k9	však
zbourat	zbourat	k5eAaPmF	zbourat
kapli	kaple	k1gFnSc4	kaple
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
zdědil	zdědit	k5eAaPmAgMnS	zdědit
Veveří	veveří	k2eAgMnSc1d1	veveří
příbuzný	příbuzný	k1gMnSc1	příbuzný
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
baron	baron	k1gMnSc1	baron
Mořic	Mořic	k1gMnSc1	Mořic
Arnold	Arnold	k1gMnSc1	Arnold
de	de	k?	de
Forest	Forest	k1gMnSc1	Forest
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
zde	zde	k6eAd1	zde
strávili	strávit	k5eAaPmAgMnP	strávit
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
svatební	svatební	k2eAgFnSc2d1	svatební
cesty	cesta	k1gFnSc2	cesta
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
britského	britský	k2eAgNnSc2d1	Britské
království	království	k1gNnSc2	království
Winston	Winston	k1gInSc1	Winston
Churchill	Churchillum	k1gNnPc2	Churchillum
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Clementine	Clementin	k1gInSc5	Clementin
<g/>
.	.	kIx.	.
</s>
<s>
Churchill	Churchill	k1gMnSc1	Churchill
sám	sám	k3xTgMnSc1	sám
navštívil	navštívit	k5eAaPmAgMnS	navštívit
barona	baron	k1gMnSc4	baron
de	de	k?	de
Forest	Forest	k1gMnSc1	Forest
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Veveří	veveří	k2eAgNnSc4d1	veveří
již	již	k9	již
dvakrát	dvakrát	k6eAd1	dvakrát
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
první	první	k4xOgFnSc7	první
československou	československý	k2eAgFnSc7d1	Československá
pozemkovou	pozemkový	k2eAgFnSc7d1	pozemková
reformou	reforma	k1gFnSc7	reforma
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
většina	většina	k1gFnSc1	většina
pozemků	pozemek	k1gInPc2	pozemek
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
panství	panství	k1gNnSc2	panství
zestátněna	zestátněn	k2eAgFnSc1d1	zestátněna
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
však	však	k9	však
postavil	postavit	k5eAaPmAgMnS	postavit
poslední	poslední	k2eAgMnSc1d1	poslední
šlechtický	šlechtický	k2eAgMnSc1d1	šlechtický
majitel	majitel	k1gMnSc1	majitel
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
panství	panství	k1gNnSc4	panství
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
,	,	kIx,	,
Mořic	Mořic	k1gMnSc1	Mořic
Arnold	Arnold	k1gMnSc1	Arnold
de	de	k?	de
Forest-Bischofsheim	Forest-Bischofsheim	k1gMnSc1	Forest-Bischofsheim
<g/>
,	,	kIx,	,
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
vedl	vést	k5eAaImAgMnS	vést
spor	spor	k1gInSc4	spor
s	s	k7c7	s
Československou	československý	k2eAgFnSc7d1	Československá
republikou	republika	k1gFnSc7	republika
o	o	k7c4	o
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
nakonec	nakonec	k6eAd1	nakonec
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
kupní	kupní	k2eAgFnSc7d1	kupní
cenou	cena	k1gFnSc7	cena
a	a	k8xC	a
hrad	hrad	k1gInSc1	hrad
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
celým	celý	k2eAgNnSc7d1	celé
panstvím	panství	k1gNnSc7	panství
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
i	i	k9	i
s	s	k7c7	s
pozemky	pozemek	k1gInPc7	pozemek
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
státním	státní	k2eAgInSc7d1	státní
velkostatkem	velkostatek	k1gInSc7	velkostatek
v	v	k7c6	v
Rosicích	Rosice	k1gFnPc6	Rosice
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
transformoval	transformovat	k5eAaBmAgInS	transformovat
ve	v	k7c4	v
Správu	správa	k1gFnSc4	správa
státních	státní	k2eAgInPc2d1	státní
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
turistickým	turistický	k2eAgInSc7d1	turistický
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1928	[number]	k4	1928
navštívil	navštívit	k5eAaPmAgMnS	navštívit
hrad	hrad	k1gInSc4	hrad
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
plány	plán	k1gInPc7	plán
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
hradu	hrad	k1gInSc2	hrad
jakožto	jakožto	k8xS	jakožto
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
rezidence	rezidence	k1gFnSc2	rezidence
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nakonec	nakonec	k6eAd1	nakonec
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
součinnosti	součinnost	k1gFnSc6	součinnost
s	s	k7c7	s
památkovým	památkový	k2eAgInSc7d1	památkový
úřadem	úřad	k1gInSc7	úřad
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
rekonstrukční	rekonstrukční	k2eAgFnSc2d1	rekonstrukční
a	a	k8xC	a
památkové	památkový	k2eAgFnSc2d1	památková
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
i	i	k9	i
za	za	k7c4	za
nacistické	nacistický	k2eAgFnPc4d1	nacistická
okupace	okupace	k1gFnPc4	okupace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
hrad	hrad	k1gInSc1	hrad
zabaven	zabavit	k5eAaPmNgInS	zabavit
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
a	a	k8xC	a
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hradu	hrad	k1gInSc2	hrad
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
výcvikový	výcvikový	k2eAgInSc4d1	výcvikový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnPc1d1	světová
války	válka	k1gFnPc1	válka
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
hrad	hrad	k1gInSc4	hrad
boje	boj	k1gInSc2	boj
při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgFnP	být
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
střechy	střecha	k1gFnPc1	střecha
<g/>
,	,	kIx,	,
fasády	fasáda	k1gFnPc1	fasáda
i	i	k8xC	i
nedaleký	daleký	k2eNgInSc1d1	nedaleký
kostelík	kostelík	k1gInSc1	kostelík
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
Předhradí	předhradí	k1gNnSc6	předhradí
<g/>
.	.	kIx.	.
</s>
<s>
Průchod	průchod	k1gInSc1	průchod
armád	armáda	k1gFnPc2	armáda
vedl	vést	k5eAaImAgInS	vést
také	také	k9	také
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
či	či	k8xC	či
ztrátě	ztráta	k1gFnSc3	ztráta
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
hradního	hradní	k2eAgInSc2d1	hradní
mobiliáře	mobiliář	k1gInSc2	mobiliář
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
a	a	k8xC	a
1948	[number]	k4	1948
byly	být	k5eAaImAgFnP	být
opraveny	opravit	k5eAaPmNgFnP	opravit
alespoň	alespoň	k9	alespoň
střechy	střecha	k1gFnPc1	střecha
a	a	k8xC	a
ztracený	ztracený	k2eAgInSc1d1	ztracený
mobiliář	mobiliář	k1gInSc1	mobiliář
doplněn	doplnit	k5eAaPmNgInS	doplnit
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
státního	státní	k2eAgInSc2d1	státní
zámku	zámek	k1gInSc2	zámek
Konopiště	Konopiště	k1gNnSc2	Konopiště
a	a	k8xC	a
hradu	hrad	k1gInSc2	hrad
Křivoklátu	Křivoklát	k1gInSc2	Křivoklát
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
nakrátko	nakrátko	k6eAd1	nakrátko
opět	opět	k6eAd1	opět
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počátkem	počátkem	k7c2	počátkem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
necitlivě	citlivě	k6eNd1	citlivě
upraven	upravit	k5eAaPmNgInS	upravit
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
Lesnického	lesnický	k2eAgNnSc2d1	lesnické
učiliště	učiliště	k1gNnSc2	učiliště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zde	zde	k6eAd1	zde
sídlilo	sídlit	k5eAaImAgNnS	sídlit
přes	přes	k7c4	přes
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1953-1954	[number]	k4	1953-1954
přesídlila	přesídlit	k5eAaPmAgFnS	přesídlit
do	do	k7c2	do
objektu	objekt	k1gInSc2	objekt
Správa	správa	k1gFnSc1	správa
lesního	lesní	k2eAgNnSc2d1	lesní
hospodářství	hospodářství	k1gNnSc2	hospodářství
v	v	k7c6	v
Rosicích	Rosice	k1gFnPc6	Rosice
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
závodní	závodní	k2eAgFnSc1d1	závodní
škola	škola	k1gFnSc1	škola
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
potřeby	potřeba	k1gFnPc4	potřeba
byly	být	k5eAaImAgFnP	být
opět	opět	k6eAd1	opět
zcela	zcela	k6eAd1	zcela
necitlivě	citlivě	k6eNd1	citlivě
upraveny	upravit	k5eAaPmNgInP	upravit
interiéry	interiér	k1gInPc1	interiér
na	na	k7c4	na
obytné	obytný	k2eAgFnPc4d1	obytná
místnosti	místnost	k1gFnPc4	místnost
a	a	k8xC	a
učebny	učebna	k1gFnPc4	učebna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1972-1999	[number]	k4	1972-1999
byl	být	k5eAaImAgInS	být
objekt	objekt	k1gInSc4	objekt
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
nevedlo	vést	k5eNaImAgNnS	vést
k	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
jeho	jeho	k3xOp3gInSc2	jeho
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
projekt	projekt	k1gInSc1	projekt
přestavby	přestavba	k1gFnSc2	přestavba
hradu	hrad	k1gInSc2	hrad
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
studentské	studentská	k1gFnSc6	studentská
a	a	k8xC	a
kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přestavby	přestavba	k1gFnSc2	přestavba
byly	být	k5eAaImAgInP	být
nakonec	nakonec	k6eAd1	nakonec
realizovány	realizovat	k5eAaBmNgInP	realizovat
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnPc4	první
etapy	etapa	k1gFnPc4	etapa
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
položeny	položen	k2eAgFnPc1d1	položena
nový	nový	k2eAgInSc1d1	nový
vodovod	vodovod	k1gInSc1	vodovod
a	a	k8xC	a
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k9	také
neodborně	odborně	k6eNd1	odborně
provedeno	proveden	k2eAgNnSc1d1	provedeno
tzv.	tzv.	kA	tzv.
statické	statický	k2eAgNnSc1d1	statické
zajištění	zajištění	k1gNnSc1	zajištění
pomocí	pomocí	k7c2	pomocí
betonových	betonový	k2eAgFnPc2d1	betonová
injektáží	injektáž	k1gFnPc2	injektáž
a	a	k8xC	a
nástřiků	nástřik	k1gInPc2	nástřik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
těžkému	těžký	k2eAgNnSc3d1	těžké
poškození	poškození	k1gNnSc3	poškození
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
necitlivé	citlivý	k2eNgFnPc4d1	necitlivá
úpravy	úprava	k1gFnPc4	úprava
přerušila	přerušit	k5eAaPmAgFnS	přerušit
až	až	k9	až
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
ponechán	ponechat	k5eAaPmNgInS	ponechat
svému	svůj	k3xOyFgInSc3	svůj
osudu	osud	k1gInSc3	osud
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
provádí	provádět	k5eAaImIp3nS	provádět
postupnou	postupný	k2eAgFnSc4d1	postupná
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
celého	celý	k2eAgInSc2d1	celý
značně	značně	k6eAd1	značně
zdevastovaného	zdevastovaný	k2eAgInSc2d1	zdevastovaný
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
znovu	znovu	k6eAd1	znovu
trvale	trvale	k6eAd1	trvale
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Kastelánem	kastelán	k1gMnSc7	kastelán
hradu	hrad	k1gInSc2	hrad
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
Petr	Petr	k1gMnSc1	Petr
Fedor	Fedor	k1gMnSc1	Fedor
<g/>
.	.	kIx.	.
</s>
<s>
Veveří	veveří	k2eAgNnSc1d1	veveří
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
majitele	majitel	k1gMnSc2	majitel
barona	baron	k1gMnSc2	baron
Mořice	Mořic	k1gMnSc2	Mořic
Arnolda	Arnold	k1gMnSc2	Arnold
De	De	k?	De
Forest-Bischoffsheim	Forest-Bischoffsheim	k1gMnSc1	Forest-Bischoffsheim
třikrát	třikrát	k6eAd1	třikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
britský	britský	k2eAgMnSc1d1	britský
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gInSc1	Churchill
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Clementine	Clementin	k1gInSc5	Clementin
zde	zde	k6eAd1	zde
strávili	strávit	k5eAaPmAgMnP	strávit
také	také	k9	také
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
svatební	svatební	k2eAgFnSc2d1	svatební
cesty	cesta	k1gFnSc2	cesta
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
tradována	tradován	k2eAgFnSc1d1	tradována
pověst	pověst	k1gFnSc1	pověst
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgInSc1d1	veveří
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
několika	několik	k4yIc2	několik
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
ukrývat	ukrývat	k5eAaImF	ukrývat
poklad	poklad	k1gInSc1	poklad
12	[number]	k4	12
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
soch	socha	k1gFnPc2	socha
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnPc1d1	pocházející
původně	původně	k6eAd1	původně
z	z	k7c2	z
kořisti	kořist	k1gFnSc2	kořist
přinesené	přinesený	k2eAgFnSc2d1	přinesená
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
knížetem	kníže	k1gMnSc7	kníže
Břetislavem	Břetislav	k1gMnSc7	Břetislav
I.	I.	kA	I.
z	z	k7c2	z
tažení	tažení	k1gNnSc2	tažení
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
přenos	přenos	k1gInSc1	přenos
ostatků	ostatek	k1gInPc2	ostatek
sv.	sv.	kA	sv.
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1039	[number]	k4	1039
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
legendy	legenda	k1gFnSc2	legenda
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ukryté	ukrytý	k2eAgFnSc2d1	ukrytá
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Porta	porta	k1gFnSc1	porta
Coeli	Coele	k1gFnSc4	Coele
v	v	k7c6	v
Předklášteří	Předklášteří	k1gNnSc6	Předklášteří
u	u	k7c2	u
Tišnova	Tišnov	k1gInSc2	Tišnov
<g/>
,	,	kIx,	,
podvodně	podvodně	k6eAd1	podvodně
na	na	k7c6	na
tamní	tamní	k2eAgFnSc6d1	tamní
abatyši	abatyše	k1gFnSc6	abatyše
vylákány	vylákat	k5eAaPmNgFnP	vylákat
templářskými	templářský	k2eAgMnPc7d1	templářský
rytíři	rytíř	k1gMnPc7	rytíř
a	a	k8xC	a
převezeny	převézt	k5eAaPmNgFnP	převézt
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
vzápětí	vzápětí	k6eAd1	vzápětí
po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
posledního	poslední	k2eAgNnSc2d1	poslední
z	z	k7c2	z
přemyslovských	přemyslovský	k2eAgMnPc2d1	přemyslovský
králů	král	k1gMnPc2	král
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Romantická	romantický	k2eAgFnSc1d1	romantická
legenda	legenda	k1gFnSc1	legenda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zjevně	zjevně	k6eAd1	zjevně
jako	jako	k9	jako
snaha	snaha	k1gFnSc1	snaha
ozvláštnit	ozvláštnit	k5eAaPmF	ozvláštnit
panství	panství	k1gNnSc4	panství
módní	módní	k2eAgFnSc7d1	módní
historkou	historka	k1gFnSc7	historka
o	o	k7c6	o
pokladu	poklad	k1gInSc6	poklad
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
nejspíše	nejspíše	k9	nejspíše
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
hradních	hradní	k2eAgMnPc2d1	hradní
úředníků	úředník	k1gMnPc2	úředník
<g/>
.	.	kIx.	.
</s>
