<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obléhání	obléhání	k1gNnSc3	obléhání
Brna	Brno	k1gNnSc2	Brno
švédskými	švédský	k2eAgNnPc7d1	švédské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obležen	obležen	k2eAgInSc4d1	obležen
i	i	k8xC	i
nedaleký	daleký	k2eNgInSc4d1	nedaleký
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
náporu	nápor	k1gInSc6	nápor
útočníků	útočník	k1gMnPc2	útočník
odolal	odolat	k5eAaPmAgMnS	odolat
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
panství	panství	k1gNnSc1	panství
bylo	být	k5eAaImAgNnS	být
vojsky	vojsky	k6eAd1	vojsky
generála	generál	k1gMnSc2	generál
Torstenssona	Torstensson	k1gMnSc2	Torstensson
vydrancováno	vydrancován	k2eAgNnSc4d1	vydrancováno
<g/>
.	.	kIx.	.
</s>
