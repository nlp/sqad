<s>
Jablečný	jablečný	k2eAgInSc1d1	jablečný
závin	závin	k1gInSc1	závin
neboli	neboli	k8xC	neboli
štrúdl	štrúdl	k?	štrúdl
<g/>
,	,	kIx,	,
také	také	k9	také
štrudle	štrudle	k?	štrudle
či	či	k8xC	či
štrúdle	štrúdle	k?	štrúdle
[	[	kIx(	[
<g/>
f.	f.	k?	f.
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Apfelstrudel	Apfelstrudlo	k1gNnPc2	Apfelstrudlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
moučníku	moučník	k1gInSc2	moučník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
