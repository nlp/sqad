<s>
René	René	k1gMnSc1	René
Descartes	Descartes	k1gMnSc1	Descartes
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Renatus	Renatus	k1gMnSc1	Renatus
Cartesius	Cartesius	k1gMnSc1	Cartesius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1596	[number]	k4	1596
La	la	k1gNnPc2	la
Haye	Haye	k1gFnPc2	Haye
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Descartes	Descartes	k1gInSc4	Descartes
(	(	kIx(	(
<g/>
Indre-et-Loire	Indret-Loir	k1gMnSc5	Indre-et-Loir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Tours	Toursa	k1gFnPc2	Toursa
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1650	[number]	k4	1650
<g/>
,	,	kIx,	,
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
.	.	kIx.	.
</s>
