<s>
Logický	logický	k2eAgInSc1d1	logický
člen	člen	k1gInSc1	člen
neboli	neboli	k8xC	neboli
hradlo	hradlo	k1gNnSc1	hradlo
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
stavební	stavební	k2eAgInSc4d1	stavební
prvek	prvek	k1gInSc4	prvek
logických	logický	k2eAgInPc2d1	logický
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyčísluje	vyčíslovat	k5eAaPmIp3nS	vyčíslovat
logickou	logický	k2eAgFnSc4d1	logická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc4	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
vstupů	vstup	k1gInPc2	vstup
a	a	k8xC	a
jediný	jediný	k2eAgInSc4d1	jediný
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
na	na	k7c6	na
výstupu	výstup	k1gInSc6	výstup
logického	logický	k2eAgInSc2d1	logický
členu	člen	k1gInSc2	člen
je	být	k5eAaImIp3nS	být
funkcí	funkce	k1gFnSc7	funkce
hodnot	hodnota	k1gFnPc2	hodnota
vstupních	vstupní	k2eAgFnPc2d1	vstupní
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
f	f	k?	f
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
pojmy	pojem	k1gInPc1	pojem
logický	logický	k2eAgInSc1d1	logický
člen	člen	k1gInSc4	člen
a	a	k8xC	a
hradlo	hradlo	k1gNnSc4	hradlo
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
hradlo	hradlo	k1gNnSc1	hradlo
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
součástku	součástka	k1gFnSc4	součástka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
logický	logický	k2eAgInSc1d1	logický
člen	člen	k1gInSc1	člen
je	být	k5eAaImIp3nS	být
myšlen	myšlen	k2eAgInSc4d1	myšlen
prvek	prvek	k1gInSc4	prvek
realizující	realizující	k2eAgInSc4d1	realizující
logickou	logický	k2eAgFnSc4d1	logická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
způsoby	způsob	k1gInPc4	způsob
značení	značení	k1gNnSc2	značení
logických	logický	k2eAgMnPc2d1	logický
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
definované	definovaný	k2eAgInPc1d1	definovaný
ANSI	ANSI	kA	ANSI
<g/>
/	/	kIx~	/
<g/>
IEEE	IEEE	kA	IEEE
Std	Std	k1gFnSc1	Std
91-1984	[number]	k4	91-1984
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
dodatkem	dodatek	k1gInSc7	dodatek
ANSI	ANSI	kA	ANSI
<g/>
/	/	kIx~	/
<g/>
IEEE	IEEE	kA	IEEE
Std	Std	k1gFnSc1	Std
91	[number]	k4	91
<g/>
a-	a-	k?	a-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
jsou	být	k5eAaImIp3nP	být
obdélníkové	obdélníkový	k2eAgFnPc1d1	obdélníková
(	(	kIx(	(
<g/>
čtvercové	čtvercový	k2eAgFnPc1d1	čtvercová
<g/>
)	)	kIx)	)
značky	značka	k1gFnPc1	značka
(	(	kIx(	(
<g/>
IEC	IEC	kA	IEC
<g/>
,	,	kIx,	,
DIN	din	k1gInSc1	din
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
způsobem	způsob	k1gInSc7	způsob
jsou	být	k5eAaImIp3nP	být
značky	značka	k1gFnPc1	značka
složené	složený	k2eAgFnPc1d1	složená
z	z	k7c2	z
křivek	křivka	k1gFnPc2	křivka
(	(	kIx(	(
<g/>
ANSI	ANSI	kA	ANSI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
v	v	k7c6	v
profesionálních	profesionální	k2eAgInPc6d1	profesionální
systémech	systém	k1gInPc6	systém
pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
logických	logický	k2eAgInPc2d1	logický
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obou	dva	k4xCgInPc2	dva
způsobů	způsob	k1gInPc2	způsob
značení	značení	k1gNnSc2	značení
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
drobné	drobný	k2eAgFnSc2d1	drobná
varianty	varianta	k1gFnSc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Negovaný	negovaný	k2eAgInSc1d1	negovaný
výstup	výstup	k1gInSc1	výstup
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označen	označit	k5eAaPmNgInS	označit
kolečkem	kolečko	k1gNnSc7	kolečko
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
logických	logický	k2eAgInPc2d1	logický
členů	člen	k1gInPc2	člen
AND	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
OR	OR	kA	OR
a	a	k8xC	a
NOT	nota	k1gFnPc2	nota
lze	lze	k6eAd1	lze
realizovat	realizovat	k5eAaBmF	realizovat
libovolný	libovolný	k2eAgInSc4d1	libovolný
logický	logický	k2eAgInSc4d1	logický
obvod	obvod	k1gInSc4	obvod
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
číslicový	číslicový	k2eAgInSc1d1	číslicový
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
AND	Anda	k1gFnPc2	Anda
a	a	k8xC	a
OR	OR	kA	OR
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
členu	člen	k1gInSc2	člen
NOT	nota	k1gFnPc2	nota
komplementární	komplementární	k2eAgFnPc1d1	komplementární
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
způsobem	způsob	k1gInSc7	způsob
vzájemně	vzájemně	k6eAd1	vzájemně
nahradit	nahradit	k5eAaPmF	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
implementovat	implementovat	k5eAaImF	implementovat
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
číslicový	číslicový	k2eAgInSc4d1	číslicový
systém	systém	k1gInSc4	systém
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
logických	logický	k2eAgInPc2d1	logický
členů	člen	k1gInPc2	člen
NAND	Nanda	k1gFnPc2	Nanda
nebo	nebo	k8xC	nebo
NOR	Nora	k1gFnPc2	Nora
nebo	nebo	k8xC	nebo
AND	Anda	k1gFnPc2	Anda
a	a	k8xC	a
NOT	nota	k1gFnPc2	nota
a	a	k8xC	a
nebo	nebo	k8xC	nebo
OR	OR	kA	OR
a	a	k8xC	a
NOT	nota	k1gFnPc2	nota
(	(	kIx(	(
<g/>
vždy	vždy	k6eAd1	vždy
stačí	stačit	k5eAaBmIp3nS	stačit
členy	člen	k1gInPc4	člen
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vstupy	vstup	k1gInPc7	vstup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
členu	člen	k1gInSc2	člen
XOR	XOR	kA	XOR
<g/>
.	.	kIx.	.
</s>
<s>
NAND	Nanda	k1gFnPc2	Nanda
a	a	k8xC	a
NOR	Nora	k1gFnPc2	Nora
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
univerzální	univerzální	k2eAgInPc1d1	univerzální
logické	logický	k2eAgInPc1d1	logický
členy	člen	k1gInPc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
seznam	seznam	k1gInSc4	seznam
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
logických	logický	k2eAgInPc2d1	logický
členů	člen	k1gInPc2	člen
včetně	včetně	k7c2	včetně
rovnice	rovnice	k1gFnSc2	rovnice
v	v	k7c6	v
Booleově	Booleův	k2eAgFnSc6d1	Booleova
algebře	algebra	k1gFnSc6	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
logickým	logický	k2eAgInSc7d1	logický
členem	člen	k1gInSc7	člen
je	být	k5eAaImIp3nS	být
opakovač	opakovač	k1gInSc1	opakovač
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
realizuje	realizovat	k5eAaBmIp3nS	realizovat
funkci	funkce	k1gFnSc4	funkce
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
i	i	k9	i
jako	jako	k9	jako
buffer	buffer	k1gInSc1	buffer
-	-	kIx~	-
zpožďovací	zpožďovací	k2eAgInSc1d1	zpožďovací
člen	člen	k1gInSc1	člen
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
krátkým	krátký	k2eAgNnSc7d1	krátké
zpožděním	zpoždění	k1gNnSc7	zpoždění
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
ns	ns	k?	ns
(	(	kIx(	(
<g/>
nanosekundy	nanosekunda	k1gFnSc2	nanosekunda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oddělovací	oddělovací	k2eAgMnSc1d1	oddělovací
člen	člen	k1gMnSc1	člen
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
kolektorem	kolektor	k1gInSc7	kolektor
<g/>
,	,	kIx,	,
výkonový	výkonový	k2eAgMnSc1d1	výkonový
budič	budič	k1gMnSc1	budič
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
sběrnice	sběrnice	k1gFnSc2	sběrnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
logickým	logický	k2eAgInSc7d1	logický
členem	člen	k1gInSc7	člen
je	být	k5eAaImIp3nS	být
invertor	invertor	k1gInSc1	invertor
<g/>
.	.	kIx.	.
</s>
<s>
Realizuje	realizovat	k5eAaBmIp3nS	realizovat
funkci	funkce	k1gFnSc4	funkce
tzv.	tzv.	kA	tzv.
logické	logický	k2eAgFnSc2d1	logická
negace	negace	k1gFnSc2	negace
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
používá	používat	k5eAaImIp3nS	používat
negovaného	negovaný	k2eAgInSc2d1	negovaný
logického	logický	k2eAgInSc2d1	logický
součtu	součet	k1gInSc2	součet
s	s	k7c7	s
přivedením	přivedení	k1gNnSc7	přivedení
hodnoty	hodnota	k1gFnSc2	hodnota
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
jediný	jediný	k2eAgInSc4d1	jediný
vstup	vstup	k1gInSc4	vstup
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
"	"	kIx"	"
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
zbylém	zbylý	k2eAgInSc6d1	zbylý
a	a	k8xC	a
nebo	nebo	k8xC	nebo
zbylých	zbylý	k2eAgInPc6d1	zbylý
vstupech	vstup	k1gInPc6	vstup
bude	být	k5eAaImBp3nS	být
logická	logický	k2eAgFnSc1d1	logická
0	[number]	k4	0
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
mít	mít	k5eAaImF	mít
tento	tento	k3xDgInSc4	tento
vstup	vstup	k1gInSc4	vstup
již	již	k6eAd1	již
na	na	k7c6	na
provedení	provedení	k1gNnSc6	provedení
operace	operace	k1gFnSc2	operace
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
negovaného	negovaný	k2eAgInSc2d1	negovaný
logického	logický	k2eAgInSc2d1	logický
součinu	součin	k1gInSc2	součin
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
vstupy	vstup	k1gInPc1	vstup
propojí	propojit	k5eAaPmIp3nP	propojit
paralelně	paralelně	k6eAd1	paralelně
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
člen	člen	k1gInSc1	člen
provádí	provádět	k5eAaImIp3nS	provádět
funkci	funkce	k1gFnSc4	funkce
tzv.	tzv.	kA	tzv.
logického	logický	k2eAgInSc2d1	logický
součinu	součin	k1gInSc2	součin
(	(	kIx(	(
<g/>
konjunkce	konjunkce	k1gFnSc1	konjunkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
člen	člen	k1gInSc1	člen
provádí	provádět	k5eAaImIp3nS	provádět
funkci	funkce	k1gFnSc4	funkce
tzv.	tzv.	kA	tzv.
logického	logický	k2eAgInSc2d1	logický
součtu	součet	k1gInSc2	součet
(	(	kIx(	(
<g/>
disjunkce	disjunkce	k1gFnSc1	disjunkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
člen	člen	k1gInSc1	člen
provádí	provádět	k5eAaImIp3nS	provádět
funkci	funkce	k1gFnSc4	funkce
tzv.	tzv.	kA	tzv.
negovaného	negovaný	k2eAgInSc2d1	negovaný
logického	logický	k2eAgInSc2d1	logický
součinu	součin	k1gInSc2	součin
(	(	kIx(	(
<g/>
Shefferovu	Shefferův	k2eAgFnSc4d1	Shefferův
funkci	funkce	k1gFnSc4	funkce
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
součet	součet	k1gInSc4	součet
negací	negace	k1gFnPc2	negace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejběžněji	běžně	k6eAd3	běžně
používané	používaný	k2eAgNnSc1d1	používané
hradlo	hradlo	k1gNnSc1	hradlo
.	.	kIx.	.
</s>
<s>
Propojením	propojení	k1gNnSc7	propojení
vstupů	vstup	k1gInPc2	vstup
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
invertor	invertor	k1gInSc4	invertor
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
něho	on	k3xPp3gMnSc2	on
realizovat	realizovat	k5eAaBmF	realizovat
většinu	většina	k1gFnSc4	většina
klopných	klopný	k2eAgInPc2d1	klopný
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
člen	člen	k1gInSc1	člen
provádí	provádět	k5eAaImIp3nS	provádět
funkci	funkce	k1gFnSc4	funkce
tzv.	tzv.	kA	tzv.
negovaného	negovaný	k2eAgInSc2d1	negovaný
logického	logický	k2eAgInSc2d1	logický
součtu	součet	k1gInSc2	součet
(	(	kIx(	(
<g/>
Peirceovu	Peirceův	k2eAgFnSc4d1	Peirceova
funkci	funkce	k1gFnSc4	funkce
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
součin	součin	k1gInSc1	součin
negací	negace	k1gFnPc2	negace
<g/>
.	.	kIx.	.
</s>
<s>
Propojením	propojení	k1gNnSc7	propojení
vstupů	vstup	k1gInPc2	vstup
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
invertor	invertor	k1gInSc4	invertor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
logický	logický	k2eAgInSc1d1	logický
člen	člen	k1gInSc1	člen
vyčísluje	vyčíslovat	k5eAaImIp3nS	vyčíslovat
exkluzivní	exkluzivní	k2eAgInSc1d1	exkluzivní
logický	logický	k2eAgInSc1d1	logický
součet	součet	k1gInSc1	součet
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
negaci	negace	k1gFnSc4	negace
exkluzivního	exkluzivní	k2eAgInSc2d1	exkluzivní
logického	logický	k2eAgInSc2d1	logický
součtu	součet	k1gInSc2	součet
<g/>
.	.	kIx.	.
</s>
<s>
Logický	logický	k2eAgMnSc1d1	logický
člen	člen	k1gMnSc1	člen
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
realizovat	realizovat	k5eAaBmF	realizovat
vhodným	vhodný	k2eAgNnSc7d1	vhodné
zapojením	zapojení	k1gNnSc7	zapojení
aktivních	aktivní	k2eAgFnPc2d1	aktivní
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
diod	dioda	k1gFnPc2	dioda
<g/>
,	,	kIx,	,
rezistorů	rezistor	k1gInPc2	rezistor
či	či	k8xC	či
dalších	další	k2eAgFnPc2d1	další
pasivních	pasivní	k2eAgFnPc2d1	pasivní
součástek	součástka	k1gFnPc2	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
logickými	logický	k2eAgMnPc7d1	logický
členy	člen	k1gMnPc7	člen
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
řady	řada	k1gFnPc1	řada
74	[number]	k4	74
<g/>
xx	xx	k?	xx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
jsou	být	k5eAaImIp3nP	být
hradla	hradlo	k1gNnSc2	hradlo
sestavena	sestavit	k5eAaPmNgFnS	sestavit
z	z	k7c2	z
několika	několik	k4yIc2	několik
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Logické	logický	k2eAgInPc1d1	logický
integrované	integrovaný	k2eAgInPc1d1	integrovaný
obvody	obvod	k1gInPc1	obvod
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
TTL	TTL	kA	TTL
<g/>
,	,	kIx,	,
SCHOTTKY	SCHOTTKY	kA	SCHOTTKY
STTL	STTL	kA	STTL
<g/>
,	,	kIx,	,
SCHOTTKY	SCHOTTKY	kA	SCHOTTKY
ALS	ALS	kA	ALS
<g/>
,	,	kIx,	,
HTL	HTL	kA	HTL
<g/>
,	,	kIx,	,
DTL	DTL	kA	DTL
<g/>
,	,	kIx,	,
LS	LS	kA	LS
<g/>
,	,	kIx,	,
CMOS	CMOS	kA	CMOS
<g/>
,	,	kIx,	,
NMOS	NMOS	kA	NMOS
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
podle	podle	k7c2	podle
technologie	technologie	k1gFnSc2	technologie
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
samostatné	samostatný	k2eAgInPc1d1	samostatný
logické	logický	k2eAgInPc1d1	logický
členy	člen	k1gInPc1	člen
používají	používat	k5eAaImIp3nP	používat
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
a	a	k8xC	a
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
je	on	k3xPp3gInPc4	on
logické	logický	k2eAgInPc4d1	logický
obvody	obvod	k1gInPc4	obvod
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
integrací	integrace	k1gFnSc7	integrace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
provádějí	provádět	k5eAaImIp3nP	provádět
složitější	složitý	k2eAgFnPc1d2	složitější
logické	logický	k2eAgFnPc1d1	logická
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
funkce	funkce	k1gFnPc1	funkce
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
stále	stále	k6eAd1	stále
realizovány	realizovat	k5eAaBmNgFnP	realizovat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
jednodušších	jednoduchý	k2eAgInPc2d2	jednodušší
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
možnostmi	možnost	k1gFnPc7	možnost
realizace	realizace	k1gFnPc4	realizace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
relé	relé	k1gNnSc7	relé
<g/>
,	,	kIx,	,
hydraulické	hydraulický	k2eAgInPc1d1	hydraulický
ventily	ventil	k1gInPc1	ventil
či	či	k8xC	či
elektronky	elektronka	k1gFnPc1	elektronka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
řízení	řízení	k1gNnSc2	řízení
se	se	k3xPyFc4	se
logické	logický	k2eAgInPc1d1	logický
členy	člen	k1gInPc1	člen
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
logických	logický	k2eAgFnPc2d1	logická
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
do	do	k7c2	do
programovatelných	programovatelný	k2eAgInPc2d1	programovatelný
logických	logický	k2eAgInPc2d1	logický
automatů	automat	k1gInPc2	automat
<g/>
.	.	kIx.	.
</s>
<s>
Logické	logický	k2eAgMnPc4d1	logický
členy	člen	k1gMnPc4	člen
jsou	být	k5eAaImIp3nP	být
potom	potom	k6eAd1	potom
pouze	pouze	k6eAd1	pouze
virtuální	virtuální	k2eAgFnSc4d1	virtuální
a	a	k8xC	a
realizaci	realizace	k1gFnSc4	realizace
zvolené	zvolený	k2eAgFnSc2d1	zvolená
logické	logický	k2eAgFnSc2d1	logická
funkce	funkce	k1gFnSc2	funkce
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
programový	programový	k2eAgInSc4d1	programový
algoritmus	algoritmus	k1gInSc4	algoritmus
<g/>
.	.	kIx.	.
</s>
