<p>
<s>
Parlamentář	parlamentář	k1gMnSc1	parlamentář
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
svým	svůj	k1gMnSc7	svůj
velitelem	velitel	k1gMnSc7	velitel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyjednávala	vyjednávat	k5eAaImAgFnS	vyjednávat
s	s	k7c7	s
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ohledně	ohledně	k7c2	ohledně
dočasného	dočasný	k2eAgNnSc2d1	dočasné
zastavení	zastavení	k1gNnSc2	zastavení
palby	palba	k1gFnSc2	palba
nebo	nebo	k8xC	nebo
výměny	výměna	k1gFnSc2	výměna
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
bílou	bílý	k2eAgFnSc7d1	bílá
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentář	parlamentář	k1gMnSc1	parlamentář
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
jeho	on	k3xPp3gInSc2	on
doprovodu	doprovod	k1gInSc2	doprovod
jsou	být	k5eAaImIp3nP	být
chráněnými	chráněný	k2eAgFnPc7d1	chráněná
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
obecného	obecný	k2eAgNnSc2d1	obecné
válečného	válečný	k2eAgNnSc2d1	válečné
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
32	[number]	k4	32
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
haagské	haagský	k2eAgFnSc2d1	Haagská
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
zákonech	zákon	k1gInPc6	zákon
a	a	k8xC	a
obyčejích	obyčej	k1gInPc6	obyčej
války	válka	k1gFnSc2	válka
pozemní	pozemní	k2eAgFnSc2d1	pozemní
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
požívají	požívat	k5eAaImIp3nP	požívat
výsady	výsada	k1gFnPc4	výsada
nedotknutelnosti	nedotknutelnost	k1gFnSc2	nedotknutelnost
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
napadnout	napadnout	k5eAaPmF	napadnout
ani	ani	k8xC	ani
jinak	jinak	k6eAd1	jinak
urazit	urazit	k5eAaPmF	urazit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
zadržet	zadržet	k5eAaPmF	zadržet
a	a	k8xC	a
učinit	učinit	k5eAaImF	učinit
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
např.	např.	kA	např.
válečné	válečný	k2eAgMnPc4d1	válečný
zajatce	zajatec	k1gMnPc4	zajatec
<g/>
,	,	kIx,	,
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
mise	mise	k1gFnSc2	mise
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
jim	on	k3xPp3gInPc3	on
poskytnout	poskytnout	k5eAaPmF	poskytnout
svobodný	svobodný	k2eAgInSc4d1	svobodný
a	a	k8xC	a
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
návrat	návrat	k1gInSc4	návrat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Výsadu	výsada	k1gFnSc4	výsada
nedotknutelnosti	nedotknutelnost	k1gFnSc2	nedotknutelnost
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
možné	možný	k2eAgNnSc1d1	možné
upřít	upřít	k5eAaPmF	upřít
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
prokázaného	prokázaný	k2eAgNnSc2d1	prokázané
vyzvědačství	vyzvědačství	k1gNnSc2	vyzvědačství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
právu	právo	k1gNnSc6	právo
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
ochrana	ochrana	k1gFnSc1	ochrana
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
válečného	válečný	k2eAgInSc2d1	válečný
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
Ublížení	ublížený	k2eAgMnPc1d1	ublížený
parlamentáři	parlamentář	k1gMnPc1	parlamentář
(	(	kIx(	(
<g/>
§	§	k?	§
417	[number]	k4	417
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Parlamentář	parlamentář	k1gMnSc1	parlamentář
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
