<p>
<s>
Státy	stát	k1gInPc1	stát
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
členové	člen	k1gMnPc1	člen
americké	americký	k2eAgFnSc2d1	americká
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
mírou	míra	k1gFnSc7	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
tvoří	tvořit	k5eAaImIp3nS	tvořit
celkem	celkem	k6eAd1	celkem
50	[number]	k4	50
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
a	a	k8xC	a
1	[number]	k4	1
federální	federální	k2eAgInSc4d1	federální
distrikt	distrikt	k1gInSc4	distrikt
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc1	D.C.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
dohromady	dohromady	k6eAd1	dohromady
51	[number]	k4	51
celků	celek	k1gInPc2	celek
vznikalo	vznikat	k5eAaImAgNnS	vznikat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
suverenitu	suverenita	k1gFnSc4	suverenita
USA	USA	kA	USA
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Portoriko	Portoriko	k1gNnSc1	Portoriko
<g/>
,	,	kIx,	,
Americké	americký	k2eAgInPc1d1	americký
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Guam	Guam	k1gInSc1	Guam
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnPc1d1	severní
Mariany	Mariana	k1gFnPc1	Mariana
<g/>
,	,	kIx,	,
Americká	americký	k2eAgFnSc1d1	americká
Samoa	Samoa	k1gFnSc1	Samoa
a	a	k8xC	a
Menší	malý	k2eAgInPc1d2	menší
odlehlé	odlehlý	k2eAgInPc1d1	odlehlý
ostrovy	ostrov	k1gInPc1	ostrov
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc4	původ
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
a	a	k8xC	a
federálního	federální	k2eAgInSc2d1	federální
distriktu	distrikt	k1gInSc2	distrikt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
samostatnosti	samostatnost	k1gFnSc2	samostatnost
USA	USA	kA	USA
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
existovalo	existovat	k5eAaImAgNnS	existovat
pouze	pouze	k6eAd1	pouze
13	[number]	k4	13
(	(	kIx(	(
<g/>
Connecticut	Connecticut	k1gInSc1	Connecticut
<g/>
,	,	kIx,	,
Delaware	Delawar	k1gMnSc5	Delawar
<g/>
,	,	kIx,	,
Georgie	Georgie	k1gFnSc1	Georgie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
,	,	kIx,	,
Maryland	Maryland	k1gInSc1	Maryland
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
,	,	kIx,	,
New	New	k1gFnSc3	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
,	,	kIx,	,
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc1	Virginie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
USA	USA	kA	USA
britskými	britský	k2eAgFnPc7d1	britská
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
dodatečně	dodatečně	k6eAd1	dodatečně
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
věnovaném	věnovaný	k2eAgNnSc6d1	věnované
státy	stát	k1gInPc1	stát
Marylandem	Marylando	k1gNnSc7	Marylando
a	a	k8xC	a
Virginií	Virginie	k1gFnSc7	Virginie
<g/>
,	,	kIx,	,
Federální	federální	k2eAgInSc1d1	federální
distrikt	distrikt	k1gInSc1	distrikt
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
území	území	k1gNnSc6	území
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavěn	k2eAgNnSc1d1	vystavěno
nové	nový	k2eAgNnSc1d1	nové
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.	D.	kA	D.
C.	C.	kA	C.
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Vermontu	Vermont	k1gInSc2	Vermont
<g/>
,	,	kIx,	,
vzniklého	vzniklý	k2eAgMnSc2d1	vzniklý
teprve	teprve	k9	teprve
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
plně	plně	k6eAd1	plně
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Vermontská	Vermontský	k2eAgFnSc1d1	Vermontská
republika	republika	k1gFnSc1	republika
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yRgFnSc6	který
vedly	vést	k5eAaImAgInP	vést
spory	spor	k1gInPc1	spor
původní	původní	k2eAgFnSc2d1	původní
kolonie	kolonie	k1gFnSc2	kolonie
Provincie	provincie	k1gFnSc2	provincie
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
a	a	k8xC	a
Provincie	provincie	k1gFnSc1	provincie
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
,	,	kIx,	,
vznikaly	vznikat	k5eAaImAgInP	vznikat
další	další	k2eAgInPc1d1	další
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
dodatečně	dodatečně	k6eAd1	dodatečně
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
USA	USA	kA	USA
získaly	získat	k5eAaPmAgFnP	získat
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
expanzí	expanze	k1gFnSc7	expanze
<g/>
,	,	kIx,	,
koupěmi	koupě	k1gFnPc7	koupě
<g/>
,	,	kIx,	,
či	či	k8xC	či
dohodami	dohoda	k1gFnPc7	dohoda
o	o	k7c6	o
hranicích	hranice	k1gFnPc6	hranice
se	s	k7c7	s
sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
či	či	k8xC	či
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
nových	nový	k2eAgInPc2d1	nový
států	stát	k1gInPc2	stát
postupným	postupný	k2eAgNnSc7d1	postupné
přerozdělováním	přerozdělování	k1gNnSc7	přerozdělování
větších	veliký	k2eAgInPc2d2	veliký
<g/>
,	,	kIx,	,
federální	federální	k2eAgFnSc7d1	federální
vládou	vláda	k1gFnSc7	vláda
spravovaných	spravovaný	k2eAgInPc2d1	spravovaný
<g/>
,	,	kIx,	,
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
,	,	kIx,	,
na	na	k7c6	na
teritoria	teritorium	k1gNnSc2	teritorium
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
uděleno	udělen	k2eAgNnSc1d1	uděleno
postavení	postavení	k1gNnSc1	postavení
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Nebrasky	Nebraska	k1gFnSc2	Nebraska
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc1	všechen
dvoukomorový	dvoukomorový	k2eAgInSc4d1	dvoukomorový
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
stát	stát	k5eAaImF	stát
od	od	k7c2	od
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
24	[number]	k4	24
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
Legislativa	legislativa	k1gFnSc1	legislativa
(	(	kIx(	(
<g/>
Legislature	Legislatur	k1gMnSc5	Legislatur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
19	[number]	k4	19
jako	jako	k8xS	jako
Generální	generální	k2eAgNnSc1d1	generální
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
General	General	k1gMnSc5	General
Assembly	Assembly	k1gMnSc5	Assembly
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnPc6	Massachusetts
a	a	k8xC	a
New	New	k1gFnPc6	New
Hampshiru	Hampshir	k1gInSc2	Hampshir
jako	jako	k8xC	jako
Generální	generální	k2eAgInSc1d1	generální
dvůr	dvůr	k1gInSc1	dvůr
(	(	kIx(	(
<g/>
General	General	k1gFnSc1	General
Court	Courta	k1gFnPc2	Courta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Dakotě	Dakota	k1gFnSc6	Dakota
a	a	k8xC	a
Oregonu	Oregon	k1gInSc6	Oregon
jako	jako	k8xS	jako
Zákonodárné	zákonodárný	k2eAgNnSc1d1	zákonodárné
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
Legislative	Legislativ	k1gInSc5	Legislativ
Assembly	Assembly	k1gFnSc3	Assembly
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
uvedenou	uvedený	k2eAgFnSc7d1	uvedená
výjimkou	výjimka	k1gFnSc7	výjimka
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
v	v	k7c6	v
pěti	pět	k4xCc6	pět
státech	stát	k1gInPc6	stát
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
Shromáždění	shromáždění	k1gNnSc2	shromáždění
-	-	kIx~	-
Assembly	Assembly	k1gFnSc1	Assembly
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
pak	pak	k6eAd1	pak
Sněmovna	sněmovna	k1gFnSc1	sněmovna
delegátů	delegát	k1gMnPc2	delegát
-	-	kIx~	-
House	house	k1gNnSc1	house
of	of	k?	of
Delegates	Delegatesa	k1gFnPc2	Delegatesa
<g/>
)	)	kIx)	)
a	a	k8xC	a
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
každého	každý	k3xTgInSc2	každý
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
guvernér	guvernér	k1gMnSc1	guvernér
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
předsedou	předseda	k1gMnSc7	předseda
zdejší	zdejší	k2eAgFnSc2d1	zdejší
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc4d1	soudní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
policii	policie	k1gFnSc4	policie
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
také	také	k9	také
deleguje	delegovat	k5eAaBmIp3nS	delegovat
dva	dva	k4xCgMnPc4	dva
senátory	senátor	k1gMnPc4	senátor
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
stát	stát	k1gInSc1	stát
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vlajku	vlajka	k1gFnSc4	vlajka
a	a	k8xC	a
pečeť	pečeť	k1gFnSc4	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
jiných	jiný	k2eAgFnPc2d1	jiná
federací	federace	k1gFnPc2	federace
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
užívání	užívání	k1gNnSc1	užívání
vlastního	vlastní	k2eAgInSc2d1	vlastní
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
většinou	většinou	k6eAd1	většinou
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
pečeť	pečeť	k1gFnSc4	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
uzákonění	uzákonění	k1gNnSc1	uzákonění
dalších	další	k2eAgInPc2d1	další
neobvyklých	obvyklý	k2eNgInPc2d1	neobvyklý
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
květina	květina	k1gFnSc1	květina
<g/>
,	,	kIx,	,
státní	státní	k2eAgNnSc1d1	státní
zvíře	zvíře	k1gNnSc1	zvíře
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
USA	USA	kA	USA
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gFnPc2	jejich
tradičních	tradiční	k2eAgFnPc2d1	tradiční
zkratek	zkratka	k1gFnPc2	zkratka
<g/>
,	,	kIx,	,
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
a	a	k8xC	a
měst	město	k1gNnPc2	město
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
mot	moto	k1gNnPc2	moto
států	stát	k1gInPc2	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Přezdívky	přezdívka	k1gFnPc1	přezdívka
států	stát	k1gInPc2	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
pečetí	pečeť	k1gFnSc7	pečeť
států	stát	k1gInPc2	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vlajek	vlajka	k1gFnPc2	vlajka
států	stát	k1gInPc2	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
stát	stát	k1gInSc1	stát
USA	USA	kA	USA
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
StateMaster	StateMaster	k1gInSc1	StateMaster
–	–	k?	–
statistický	statistický	k2eAgInSc1d1	statistický
server	server	k1gInSc1	server
specializovaný	specializovaný	k2eAgInSc1d1	specializovaný
na	na	k7c4	na
státy	stát	k1gInPc4	stát
USA	USA	kA	USA
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
stovkou	stovka	k1gFnSc7	stovka
různých	různý	k2eAgInPc2d1	různý
žebříčků	žebříček	k1gInPc2	žebříček
</s>
</p>
