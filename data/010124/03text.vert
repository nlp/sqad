<p>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Langentriebe	Langentrieb	k1gMnSc5	Langentrieb
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
v	v	k7c6	v
Pardubickém	pardubický	k2eAgInSc6d1	pardubický
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Třebovky	Třebovka	k1gFnSc2	Třebovka
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
Třebovou	Třebová	k1gFnSc7	Třebová
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
s	s	k7c7	s
Ústím	ústí	k1gNnSc7	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
přirozeným	přirozený	k2eAgInSc7d1	přirozený
středem	střed	k1gInSc7	střed
konurbace	konurbace	k1gFnSc2	konurbace
(	(	kIx(	(
<g/>
souměstí	souměstí	k1gNnSc1	souměstí
<g/>
)	)	kIx)	)
Třebová	Třebová	k1gFnSc1	Třebová
-	-	kIx~	-
Ústí	ústí	k1gNnSc1	ústí
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
300	[number]	k4	300
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1304	[number]	k4	1304
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
novogotický	novogotický	k2eAgInSc1d1	novogotický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
dominantou	dominanta	k1gFnSc7	dominanta
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
Třebové	Třebová	k1gFnSc2	Třebová
a	a	k8xC	a
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
věži	věž	k1gFnSc6	věž
je	být	k5eAaImIp3nS	být
zavěšena	zavěšen	k2eAgFnSc1d1	zavěšena
nejstarší	starý	k2eAgFnSc1d3	nejstarší
památka	památka	k1gFnSc1	památka
obce	obec	k1gFnSc2	obec
–	–	k?	–
zvon	zvon	k1gInSc1	zvon
Prokop	Prokop	k1gMnSc1	Prokop
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1753	[number]	k4	1753
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
museli	muset	k5eAaImAgMnP	muset
první	první	k4xOgMnPc1	první
obyvatelé	obyvatel	k1gMnPc1	obyvatel
místo	místo	k1gNnSc4	místo
pokryté	pokrytý	k2eAgInPc4d1	pokrytý
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
vykácet	vykácet	k5eAaPmF	vykácet
<g/>
,	,	kIx,	,
vymýtit	vymýtit	k5eAaPmF	vymýtit
–	–	k?	–
staročesky	staročesky	k6eAd1	staročesky
tříbiti	tříbit	k5eAaImF	tříbit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
významu	význam	k1gInSc6	význam
užívaného	užívaný	k2eAgNnSc2d1	užívané
slovesa	sloveso	k1gNnSc2	sloveso
je	být	k5eAaImIp3nS	být
odvozováno	odvozován	k2eAgNnSc4d1	odvozováno
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
Třebová	Třebová	k1gFnSc1	Třebová
i	i	k8xC	i
název	název	k1gInSc1	název
řeky	řeka	k1gFnSc2	řeka
Třebovka	Třebovka	k1gFnSc1	Třebovka
<g/>
.	.	kIx.	.
</s>
<s>
Blízkost	blízkost	k1gFnSc1	blízkost
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
byla	být	k5eAaImAgFnS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
sídla	sídlo	k1gNnSc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
protáhlý	protáhlý	k2eAgInSc1d1	protáhlý
tvar	tvar	k1gInSc1	tvar
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
osady	osada	k1gFnSc2	osada
dal	dát	k5eAaPmAgInS	dát
rozlišující	rozlišující	k2eAgInSc1d1	rozlišující
název	název	k1gInSc1	název
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
pojmenování	pojmenování	k1gNnSc2	pojmenování
sousedního	sousední	k2eAgNnSc2d1	sousední
města	město	k1gNnSc2	město
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
před	před	k7c7	před
vytyčováním	vytyčování	k1gNnSc7	vytyčování
katastrů	katastr	k1gInPc2	katastr
obcí	obec	k1gFnPc2	obec
<g/>
)	)	kIx)	)
sahala	sahat	k5eAaImAgFnS	sahat
obec	obec	k1gFnSc1	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
až	až	k9	až
do	do	k7c2	do
Hylvát	Hylvát	k1gMnSc1	Hylvát
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Krátká	krátký	k2eAgFnSc1d1	krátká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
k	k	k7c3	k
Čáslavkovu	Čáslavkův	k2eAgInSc3d1	Čáslavkův
statku	statek	k1gInSc3	statek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Profous	Profous	k1gMnSc1	Profous
–	–	k?	–
Místní	místní	k2eAgNnSc1d1	místní
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
šest	šest	k4xCc4	šest
verzí	verze	k1gFnPc2	verze
možného	možný	k2eAgInSc2d1	možný
vzniku	vznik	k1gInSc2	vznik
jména	jméno	k1gNnSc2	jméno
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vpředu	vpředu	k6eAd1	vpředu
nastíněná	nastíněný	k2eAgFnSc1d1	nastíněná
varianta	varianta	k1gFnSc1	varianta
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
nejvěrohodnější	věrohodný	k2eAgNnSc1d3	nejvěrohodnější
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
však	však	k9	však
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
neměnil	měnit	k5eNaImAgInS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
záznamech	záznam	k1gInPc6	záznam
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
názvy	název	k1gInPc7	název
naší	náš	k3xOp1gFnSc2	náš
obce	obec	k1gFnSc2	obec
<g/>
:	:	kIx,	:
Longa	Long	k1gMnSc2	Long
Trebovia	Trebovius	k1gMnSc2	Trebovius
villa	vill	k1gMnSc2	vill
(	(	kIx(	(
<g/>
1292	[number]	k4	1292
i	i	k9	i
1304	[number]	k4	1304
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dlauha	Dlauha	k1gMnSc1	Dlauha
Tržebowa	Tržebowa	k1gMnSc1	Tržebowa
(	(	kIx(	(
<g/>
1654	[number]	k4	1654
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dlauha	Dlauha	k1gMnSc1	Dlauha
Třebowa	Třebowa	k1gMnSc1	Třebowa
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
abecedních	abecední	k2eAgInPc6d1	abecední
seznamech	seznam	k1gInPc6	seznam
obcí	obec	k1gFnPc2	obec
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
i	i	k9	i
obráceného	obrácený	k2eAgInSc2d1	obrácený
slovosledu	slovosled	k1gInSc2	slovosled
Třebová	Třebová	k1gFnSc1	Třebová
Dlouhá	Dlouhá	k1gFnSc1	Dlouhá
<g/>
,	,	kIx,	,
Třebová	Třebová	k1gFnSc1	Třebová
Česká	český	k2eAgFnSc1d1	Česká
atd.	atd.	kA	atd.
Překlad	překlad	k1gInSc4	překlad
názvu	název	k1gInSc2	název
obce	obec	k1gFnSc2	obec
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
nebyl	být	k5eNaImAgInS	být
jednotný	jednotný	k2eAgInSc1d1	jednotný
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
se	se	k3xPyFc4	se
názvy	název	k1gInPc7	název
<g/>
:	:	kIx,	:
Lange	Langus	k1gMnSc5	Langus
Triebe	Trieb	k1gMnSc5	Trieb
<g/>
,	,	kIx,	,
Lange	Lang	k1gMnSc4	Lang
Trübau	Trübaa	k1gMnSc4	Trübaa
<g/>
,	,	kIx,	,
Langetriebe	Langetrieb	k1gMnSc5	Langetrieb
<g/>
,	,	kIx,	,
Langentriebe	Langentrieb	k1gMnSc5	Langentrieb
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lexikonech	lexikon	k1gInPc6	lexikon
obcí	obec	k1gFnPc2	obec
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
pojmenováním	pojmenování	k1gNnSc7	pojmenování
obce	obec	k1gFnSc2	obec
Dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
Třebová	Třebová	k1gFnSc1	Třebová
nesetkáváme	setkávat	k5eNaImIp1nP	setkávat
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
vyslovit	vyslovit	k5eAaPmF	vyslovit
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc1	název
této	tento	k3xDgFnSc2	tento
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
i	i	k8xC	i
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nadační	nadační	k2eAgFnSc6d1	nadační
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
1304	[number]	k4	1304
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
přidělil	přidělit	k5eAaPmAgInS	přidělit
správu	správa	k1gFnSc4	správa
nad	nad	k7c7	nad
panstvím	panství	k1gNnSc7	panství
lanšperským	lanšperský	k2eAgNnSc7d1	lanšperské
a	a	k8xC	a
lanškrounským	lanškrounský	k2eAgNnSc7d1	lanškrounské
Zbraslavskému	zbraslavský	k2eAgInSc3d1	zbraslavský
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
výčtem	výčet	k1gInSc7	výčet
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
klášteru	klášter	k1gInSc3	klášter
připadly	připadnout	k5eAaPmAgInP	připadnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
i	i	k9	i
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1358	[number]	k4	1358
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
biskupství	biskupství	k1gNnSc3	biskupství
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stávala	stávat	k5eAaImAgFnS	stávat
zemanská	zemanský	k2eAgFnSc1d1	zemanská
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhaly	probíhat	k5eAaImAgInP	probíhat
soudy	soud	k1gInPc1	soud
a	a	k8xC	a
několikráte	několikráte	k6eAd1	několikráte
zde	zde	k6eAd1	zde
pobývali	pobývat	k5eAaImAgMnP	pobývat
i	i	k9	i
litomyšlští	litomyšlský	k2eAgMnPc1d1	litomyšlský
biskupové	biskup	k1gMnPc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
častému	častý	k2eAgNnSc3d1	časté
střídání	střídání	k1gNnSc3	střídání
vlastníků	vlastník	k1gMnPc2	vlastník
panství	panství	k1gNnSc4	panství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1621	[number]	k4	1621
připadlo	připadnout	k5eAaPmAgNnS	připadnout
panství	panství	k1gNnSc1	panství
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
rodu	rod	k1gInSc2	rod
Lichtenštejnů	Lichtenštejn	k1gMnPc2	Lichtenštejn
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
majetek	majetek	k1gInSc1	majetek
patřil	patřit	k5eAaImAgInS	patřit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
rušena	rušen	k2eAgMnSc4d1	rušen
robota	robot	k1gMnSc4	robot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
úřadu	úřad	k1gInSc2	úřad
rychtáře	rychtář	k1gMnSc2	rychtář
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
poslední	poslední	k2eAgMnSc1d1	poslední
rychtář	rychtář	k1gMnSc1	rychtář
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
Ignác	Ignác	k1gMnSc1	Ignác
Fajt	Fajt	k1gMnSc1	Fajt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1848	[number]	k4	1848
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
rychtáře	rychtář	k1gMnSc2	rychtář
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
obecním	obecní	k2eAgMnSc7d1	obecní
starostou	starosta	k1gMnSc7	starosta
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
zvolen	zvolen	k2eAgMnSc1d1	zvolen
František	František	k1gMnSc1	František
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
vykonával	vykonávat	k5eAaImAgInS	vykonávat
úřad	úřad	k1gInSc1	úřad
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Od	od	k7c2	od
první	první	k4xOgFnSc2	první
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
na	na	k7c4	na
čtyřicet	čtyřicet	k4xCc4	čtyřicet
padlých	padlý	k1gMnPc2	padlý
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
padli	padnout	k5eAaImAgMnP	padnout
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
za	za	k7c4	za
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
zřízena	zřízen	k2eAgFnSc1d1	zřízena
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
prodejnu	prodejna	k1gFnSc4	prodejna
jízdenek	jízdenka	k1gFnPc2	jízdenka
a	a	k8xC	a
čekárnu	čekárna	k1gFnSc4	čekárna
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Plošná	plošný	k2eAgFnSc1d1	plošná
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
obce	obec	k1gFnSc2	obec
skončila	skončit	k5eAaPmAgFnS	skončit
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
obcí	obec	k1gFnPc2	obec
projížděli	projíždět	k5eAaImAgMnP	projíždět
prezidenti	prezident	k1gMnPc1	prezident
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gNnSc1	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
a	a	k8xC	a
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
obec	obec	k1gFnSc1	obec
nebyla	být	k5eNaImAgFnS	být
cílem	cíl	k1gInSc7	cíl
jejich	jejich	k3xOp3gFnSc2	jejich
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
složité	složitý	k2eAgFnSc3d1	složitá
situaci	situace	k1gFnSc3	situace
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
Třebové	Třebová	k1gFnSc6	Třebová
k	k	k7c3	k
nahromadění	nahromadění	k1gNnSc3	nahromadění
uprchlíků	uprchlík	k1gMnPc2	uprchlík
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
Sudet	Sudety	k1gInPc2	Sudety
<g/>
.	.	kIx.	.
</s>
<s>
Sousední	sousední	k2eAgFnSc1d1	sousední
obec	obec	k1gFnSc1	obec
Hylváty	Hylváta	k1gFnSc2	Hylváta
ale	ale	k9	ale
měla	mít	k5eAaImAgFnS	mít
výraznou	výrazný	k2eAgFnSc4d1	výrazná
německou	německý	k2eAgFnSc4d1	německá
menšinu	menšina	k1gFnSc4	menšina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
její	její	k3xOp3gMnPc1	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vyžádali	vyžádat	k5eAaPmAgMnP	vyžádat
ochranu	ochrana	k1gFnSc4	ochrana
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
ve	v	k7c4	v
stavbu	stavba	k1gFnSc4	stavba
celnice	celnice	k1gFnSc2	celnice
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
prostoru	prostor	k1gInSc6	prostor
"	"	kIx"	"
<g/>
U	u	k7c2	u
kovárny	kovárna	k1gFnSc2	kovárna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Česka	Česko	k1gNnSc2	Česko
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
objevily	objevit	k5eAaPmAgFnP	objevit
mimořádné	mimořádný	k2eAgInPc4d1	mimořádný
lidové	lidový	k2eAgInPc4d1	lidový
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
začaly	začít	k5eAaPmAgFnP	začít
odsuzovat	odsuzovat	k5eAaImF	odsuzovat
kolaboranty	kolaborant	k1gMnPc7	kolaborant
a	a	k8xC	a
konfiskovat	konfiskovat	k5eAaBmF	konfiskovat
jejich	jejich	k3xOp3gInSc4	jejich
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
padlo	padnout	k5eAaPmAgNnS	padnout
17	[number]	k4	17
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
období	období	k1gNnSc1	období
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
odhalen	odhalen	k2eAgInSc4d1	odhalen
pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
také	také	k9	také
nalézá	nalézat	k5eAaImIp3nS	nalézat
masový	masový	k2eAgInSc1d1	masový
hrob	hrob	k1gInSc1	hrob
26	[number]	k4	26
bezejmenných	bezejmenný	k2eAgFnPc2d1	bezejmenná
obětí	oběť	k1gFnPc2	oběť
z	z	k7c2	z
transportu	transport	k1gInSc2	transport
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obcí	obec	k1gFnSc7	obec
projel	projet	k5eAaPmAgInS	projet
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
snaze	snaha	k1gFnSc3	snaha
občanů	občan	k1gMnPc2	občan
obce	obec	k1gFnSc2	obec
byly	být	k5eAaImAgInP	být
nakonec	nakonec	k6eAd1	nakonec
oběti	oběť	k1gFnPc4	oběť
pohřbeny	pohřben	k2eAgFnPc4d1	pohřbena
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
působily	působit	k5eAaImAgFnP	působit
organizace	organizace	k1gFnPc1	organizace
jako	jako	k8xS	jako
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Orel	Orel	k1gMnSc1	Orel
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
,	,	kIx,	,
Spolek	spolek	k1gInSc1	spolek
divadelních	divadelní	k2eAgMnPc2d1	divadelní
ochotníků	ochotník	k1gMnPc2	ochotník
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
,	,	kIx,	,
kapely	kapela	k1gFnSc2	kapela
J.	J.	kA	J.
Rybky	rybka	k1gFnSc2	rybka
a	a	k8xC	a
J.	J.	kA	J.
Pirkla	Pirkla	k1gFnSc7	Pirkla
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
kulturních	kulturní	k2eAgFnPc2d1	kulturní
tradic	tradice	k1gFnPc2	tradice
a	a	k8xC	a
odkazů	odkaz	k1gInPc2	odkaz
a	a	k8xC	a
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
zvyklosti	zvyklost	k1gFnSc3	zvyklost
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
celky	celek	k1gInPc1	celek
nové	nový	k2eAgInPc1d1	nový
jako	jako	k8xS	jako
Svaz	svaz	k1gInSc1	svaz
československo-sovětského	československoovětský	k2eAgNnSc2d1	československo-sovětský
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
,	,	kIx,	,
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
ČSM	ČSM	kA	ČSM
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
Osvětová	osvětový	k2eAgFnSc1d1	osvětová
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
Svaz	svaz	k1gInSc1	svaz
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
či	či	k8xC	či
apolitický	apolitický	k2eAgInSc1d1	apolitický
Československý	československý	k2eAgInSc1d1	československý
červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
i	i	k8xC	i
místní	místní	k2eAgInSc1d1	místní
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
místo	místo	k7c2	místo
obecního	obecní	k2eAgInSc2d1	obecní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
na	na	k7c4	na
rok	rok	k1gInSc4	rok
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kovář	Kovář	k1gMnSc1	Kovář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
byl	být	k5eAaImAgInS	být
předsedou	předseda	k1gMnSc7	předseda
MNV	MNV	kA	MNV
Antonín	Antonín	k1gMnSc1	Antonín
Sychra	Sychra	k1gMnSc1	Sychra
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
129	[number]	k4	129
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1976	[number]	k4	1976
až	až	k8xS	až
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
k	k	k7c3	k
městu	město	k1gNnSc3	město
Ústí	ústit	k5eAaImIp3nP	ústit
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
pátá	pátý	k4xOgFnSc1	pátý
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
však	však	k9	však
obec	obec	k1gFnSc1	obec
získala	získat	k5eAaPmAgFnS	získat
opět	opět	k6eAd1	opět
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
prvního	první	k4xOgMnSc2	první
obecního	obecní	k2eAgMnSc2d1	obecní
starosty	starosta	k1gMnSc2	starosta
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
postihla	postihnout	k5eAaPmAgFnS	postihnout
obec	obec	k1gFnSc1	obec
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
řeka	řeka	k1gFnSc1	řeka
Třebovka	Třebovka	k1gFnSc1	Třebovka
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
škody	škoda	k1gFnPc1	škoda
byly	být	k5eAaImAgFnP	být
příčinou	příčina	k1gFnSc7	příčina
návštěvy	návštěva	k1gFnSc2	návštěva
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
s	s	k7c7	s
chotí	choť	k1gFnSc7	choť
Dagmar	Dagmar	k1gFnSc7	Dagmar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
získala	získat	k5eAaPmAgFnS	získat
obec	obec	k1gFnSc1	obec
od	od	k7c2	od
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
používání	používání	k1gNnSc4	používání
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
znaku	znak	k1gInSc2	znak
jako	jako	k8xS	jako
nových	nový	k2eAgInPc2d1	nový
symbolů	symbol	k1gInPc2	symbol
obce	obec	k1gFnSc2	obec
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
propagaci	propagace	k1gFnSc4	propagace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Starostové	Starosta	k1gMnPc1	Starosta
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
===	===	k?	===
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
obecním	obecní	k2eAgMnSc7d1	obecní
starostou	starosta	k1gMnSc7	starosta
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
úřadu	úřad	k1gInSc2	úřad
rychtáře	rychtář	k1gMnSc2	rychtář
<g/>
.	.	kIx.	.
<g/>
Pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Štancl	Štancl	k1gMnSc1	Štancl
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
109	[number]	k4	109
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Rybka	Rybka	k1gMnSc1	Rybka
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Sršeň	sršeň	k1gMnSc1	sršeň
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
potomkem	potomek	k1gMnSc7	potomek
ze	z	k7c2	z
zdejší	zdejší	k2eAgFnSc2d1	zdejší
učitelské	učitelský	k2eAgFnSc2d1	učitelská
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
<g/>
105	[number]	k4	105
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Rybka	Rybka	k1gMnSc1	Rybka
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
–	–	k?	–
<g/>
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
–	–	k?	–
<g/>
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Šár	šár	k1gInSc1	šár
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
183	[number]	k4	183
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
zasloužilým	zasloužilý	k2eAgMnSc7d1	zasloužilý
veřejným	veřejný	k2eAgMnSc7d1	veřejný
činitelem	činitel	k1gMnSc7	činitel
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
32	[number]	k4	32
a	a	k8xC	a
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
působení	působení	k1gNnSc4	působení
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
vysvěcen	vysvěcen	k2eAgInSc1d1	vysvěcen
chrám	chrám	k1gInSc1	chrám
Páně	páně	k2eAgMnSc2d1	páně
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Hác	Hác	k1gMnSc1	Hác
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
9	[number]	k4	9
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
byl	být	k5eAaImAgInS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
činitelem	činitel	k1gMnSc7	činitel
ve	v	k7c6	v
zdejších	zdejší	k2eAgInPc6d1	zdejší
spolcích	spolek	k1gInPc6	spolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Engelbert	Engelbert	k1gMnSc1	Engelbert
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
starostou	starosta	k1gMnSc7	starosta
po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
57	[number]	k4	57
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
76	[number]	k4	76
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
od	od	k7c2	od
spoluobčanů	spoluobčan	k1gMnPc2	spoluobčan
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
také	také	k9	také
velitelem	velitel	k1gMnSc7	velitel
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
i	i	k9	i
starostou	starosta	k1gMnSc7	starosta
místního	místní	k2eAgInSc2d1	místní
Sboru	sbor	k1gInSc2	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
172	[number]	k4	172
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šimek	Šimek	k1gMnSc1	Šimek
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
44	[number]	k4	44
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
starosty	starosta	k1gMnSc2	starosta
zvolen	zvolit	k5eAaPmNgInS	zvolit
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
27	[number]	k4	27
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
své	své	k1gNnSc4	své
funkcí	funkce	k1gFnPc2	funkce
zůstal	zůstat	k5eAaPmAgInS	zůstat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
postupně	postupně	k6eAd1	postupně
opustil	opustit	k5eAaPmAgMnS	opustit
všechny	všechen	k3xTgFnPc4	všechen
veřejné	veřejný	k2eAgFnPc4d1	veřejná
funkce	funkce	k1gFnPc4	funkce
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
civilnímu	civilní	k2eAgNnSc3d1	civilní
povolání	povolání	k1gNnSc3	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předsedové	předseda	k1gMnPc1	předseda
Místního	místní	k2eAgInSc2d1	místní
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Vavřín	Vavřín	k1gMnSc1	Vavřín
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Skála	Skála	k1gMnSc1	Skála
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Morkes	Morkes	k1gMnSc1	Morkes
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Sychra	Sychra	k1gMnSc1	Sychra
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Starostové	Starosta	k1gMnPc1	Starosta
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
obnoven	obnoven	k2eAgInSc1d1	obnoven
po	po	k7c6	po
společenských	společenský	k2eAgFnPc6d1	společenská
změnách	změna	k1gFnPc6	změna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Filip	Filip	k1gMnSc1	Filip
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
Zedník	Zedník	k1gMnSc1	Zedník
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kovář	Kovář	k1gMnSc1	Kovář
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Halva	halv	k1gMnSc2	halv
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kašpar	Kašpar	k1gMnSc1	Kašpar
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
Slezáková	Slezáková	k1gFnSc1	Slezáková
2018	[number]	k4	2018
<g/>
–	–	k?	–
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zaniklé	zaniklý	k2eAgMnPc4d1	zaniklý
===	===	k?	===
</s>
</p>
<p>
<s>
Zemanská	zemanský	k2eAgFnSc1d1	zemanská
tvrz	tvrz	k1gFnSc1	tvrz
–	–	k?	–
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
památkou	památka	k1gFnSc7	památka
osídlení	osídlení	k1gNnSc4	osídlení
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
Třebové	Třebová	k1gFnSc6	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
Tvrz	tvrz	k1gFnSc1	tvrz
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c4	na
ostrohu	ostroha	k1gFnSc4	ostroha
nad	nad	k7c7	nad
vsí	ves	k1gFnSc7	ves
v	v	k7c6	v
části	část	k1gFnSc6	část
zvané	zvaný	k2eAgFnSc2d1	zvaná
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
okolí	okolí	k1gNnSc2	okolí
vzdálenějšího	vzdálený	k2eAgInSc2d2	vzdálenější
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
Lanšperk	Lanšperk	k1gInSc1	Lanšperk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
existenci	existence	k1gFnSc6	existence
máme	mít	k5eAaImIp1nP	mít
z	z	k7c2	z
biskupské	biskupský	k2eAgFnSc2d1	biskupská
listiny	listina	k1gFnSc2	listina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
střídání	střídání	k1gNnSc6	střídání
mnoha	mnoho	k4c2	mnoho
majitelů	majitel	k1gMnPc2	majitel
tvrz	tvrz	k1gFnSc1	tvrz
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
osiřela	osiřet	k5eAaPmAgFnS	osiřet
a	a	k8xC	a
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k8xS	jako
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
k	k	k7c3	k
opodál	opodál	k6eAd1	opodál
zřizovanému	zřizovaný	k2eAgInSc3d1	zřizovaný
vrchnostenskému	vrchnostenský	k2eAgInSc3d1	vrchnostenský
dvoru	dvůr	k1gInSc3	dvůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
zvonice	zvonice	k1gFnSc1	zvonice
–	–	k?	–
stála	stát	k5eAaImAgFnS	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1753	[number]	k4	1753
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
zemanské	zemanský	k2eAgFnSc2d1	zemanská
tvrze	tvrz	k1gFnSc2	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
opatřena	opatřit	k5eAaPmNgFnS	opatřit
zvonem	zvon	k1gInSc7	zvon
Prokop	Prokop	k1gMnSc1	Prokop
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
dostavbě	dostavba	k1gFnSc6	dostavba
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
přenesen	přenesen	k2eAgMnSc1d1	přenesen
<g/>
,	,	kIx,	,
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
zvonice	zvonice	k1gFnSc2	zvonice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
10	[number]	k4	10
metrová	metrový	k2eAgFnSc1d1	metrová
stavba	stavba	k1gFnSc1	stavba
dokumentovala	dokumentovat	k5eAaBmAgFnS	dokumentovat
řemeslnou	řemeslný	k2eAgFnSc4d1	řemeslná
zručnost	zručnost	k1gFnSc4	zručnost
vesnických	vesnický	k2eAgMnPc2d1	vesnický
tesařů	tesař	k1gMnPc2	tesař
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
představitelkou	představitelka	k1gFnSc7	představitelka
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
lidové	lidový	k2eAgFnSc2d1	lidová
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dochované	dochovaný	k2eAgMnPc4d1	dochovaný
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolický	římskokatolický	k2eAgInSc1d1	římskokatolický
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Prokopa	Prokop	k1gMnSc2	Prokop
v	v	k7c6	v
pseudogotickém	pseudogotický	k2eAgInSc6d1	pseudogotický
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
postavený	postavený	k2eAgInSc4d1	postavený
v	v	k7c6	v
letech	let	k1gInPc6	let
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
však	však	k9	však
našetřené	našetřený	k2eAgInPc1d1	našetřený
peníze	peníz	k1gInPc1	peníz
brzy	brzy	k6eAd1	brzy
došly	dojít	k5eAaPmAgInP	dojít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byly	být	k5eAaImAgInP	být
vystavěny	vystavět	k5eAaPmNgInP	vystavět
pouze	pouze	k6eAd1	pouze
základy	základ	k1gInPc1	základ
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
posměšně	posměšně	k6eAd1	posměšně
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
kostel	kostel	k1gInSc4	kostel
postavený	postavený	k2eAgInSc4d1	postavený
grunty	grunt	k1gInPc7	grunt
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
sv.	sv.	kA	sv.
Prokopovi	Prokopův	k2eAgMnPc1d1	Prokopův
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
svatoprokopská	svatoprokopský	k2eAgFnSc1d1	Svatoprokopská
tradice	tradice	k1gFnSc1	tradice
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
obci	obec	k1gFnSc6	obec
dávné	dávný	k2eAgInPc4d1	dávný
kořeny	kořen	k1gInPc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
také	také	k9	také
jméno	jméno	k1gNnSc1	jméno
zvonu	zvon	k1gInSc2	zvon
Prokop	Prokop	k1gMnSc1	Prokop
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
poslední	poslední	k2eAgFnSc3d1	poslední
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
získal	získat	k5eAaPmAgInS	získat
zářivě	zářivě	k6eAd1	zářivě
bílou	bílý	k2eAgFnSc4d1	bílá
omítku	omítka	k1gFnSc4	omítka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
z	z	k7c2	z
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc2	Josef
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
kaple	kaple	k1gFnSc1	kaple
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
sv.	sv.	kA	sv.
Josefovi	Josef	k1gMnSc3	Josef
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
pseudogotickém	pseudogotický	k2eAgInSc6d1	pseudogotický
slohu	sloh	k1gInSc6	sloh
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
připravil	připravit	k5eAaPmAgMnS	připravit
stavitel	stavitel	k1gMnSc1	stavitel
Josef	Josef	k1gMnSc1	Josef
Shüler	Shüler	k1gMnSc1	Shüler
z	z	k7c2	z
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
místního	místní	k2eAgInSc2d1	místní
hřbitova	hřbitov	k1gInSc2	hřbitov
ležícího	ležící	k2eAgMnSc4d1	ležící
nad	nad	k7c7	nad
vesnicí	vesnice	k1gFnSc7	vesnice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kapli	kaple	k1gFnSc6	kaple
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
konala	konat	k5eAaImAgFnS	konat
první	první	k4xOgFnSc1	první
mše	mše	k1gFnSc1	mše
na	na	k7c6	na
území	území	k1gNnSc6	území
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
až	až	k9	až
o	o	k7c4	o
8	[number]	k4	8
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
–	–	k?	–
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
socha	socha	k1gFnSc1	socha
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
Třebové	Třebová	k1gFnSc6	Třebová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyobrazuje	vyobrazovat	k5eAaImIp3nS	vyobrazovat
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
–	–	k?	–
zemského	zemský	k2eAgMnSc2d1	zemský
patrona	patron	k1gMnSc2	patron
a	a	k8xC	a
ochránce	ochránce	k1gMnSc2	ochránce
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
Třebové	Třebová	k1gFnSc2	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
socha	socha	k1gFnSc1	socha
zhotovená	zhotovený	k2eAgFnSc1d1	zhotovená
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
je	být	k5eAaImIp3nS	být
však	však	k9	však
již	již	k6eAd1	již
třetí	třetí	k4xOgFnSc7	třetí
verzí	verze	k1gFnSc7	verze
sochy	socha	k1gFnSc2	socha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
na	na	k7c6	na
původním	původní	k2eAgInSc6d1	původní
podstavci	podstavec	k1gInSc6	podstavec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tzv.	tzv.	kA	tzv.
Prokopova	Prokopův	k2eAgFnSc1d1	Prokopova
studánka	studánka	k1gFnSc1	studánka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
svobody	svoboda	k1gFnSc2	svoboda
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
kamenná	kamenný	k2eAgFnSc1d1	kamenná
mohyla	mohyla	k1gFnSc1	mohyla
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
a	a	k8xC	a
odhalena	odhalit	k5eAaPmNgFnS	odhalit
při	při	k7c6	při
výročí	výročí	k1gNnSc6	výročí
smrti	smrt	k1gFnSc2	smrt
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
a	a	k8xC	a
výročí	výročí	k1gNnPc1	výročí
samostatnosti	samostatnost	k1gFnSc2	samostatnost
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pomníku	pomník	k1gInSc2	pomník
jsou	být	k5eAaImIp3nP	být
zasazeny	zasazen	k2eAgInPc1d1	zasazen
tři	tři	k4xCgInPc1	tři
bronzové	bronzový	k2eAgInPc1d1	bronzový
emblémy	emblém	k1gInPc1	emblém
–	–	k?	–
Jana	Jan	k1gMnSc4	Jan
Husa	Hus	k1gMnSc4	Hus
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
a	a	k8xC	a
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbor	sbor	k1gInSc1	sbor
Československé	československý	k2eAgFnSc2d1	Československá
církve	církev	k1gFnSc2	církev
husitské	husitský	k2eAgFnSc2d1	husitská
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
novodobá	novodobý	k2eAgFnSc1d1	novodobá
budova	budova	k1gFnSc1	budova
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
místní	místní	k2eAgFnSc3d1	místní
železniční	železniční	k2eAgFnSc3d1	železniční
zastávce	zastávka	k1gFnSc3	zastávka
byla	být	k5eAaImAgFnS	být
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	let	k1gInPc6	let
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
místními	místní	k2eAgMnPc7d1	místní
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
nově	nova	k1gFnSc3	nova
vznikající	vznikající	k2eAgFnSc3d1	vznikající
Církvi	církev	k1gFnSc3	církev
československé	československý	k2eAgFnSc3d1	Československá
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Církev	církev	k1gFnSc1	církev
československá	československý	k2eAgFnSc1d1	Československá
husitská	husitský	k2eAgFnSc1d1	husitská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
k	k	k7c3	k
náboženským	náboženský	k2eAgInPc3d1	náboženský
účelům	účel	k1gInPc3	účel
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
již	již	k6eAd1	již
neslouží	sloužit	k5eNaImIp3nP	sloužit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stále	stále	k6eAd1	stále
stojí	stát	k5eAaImIp3nS	stát
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
pískovcová	pískovcový	k2eAgFnSc1d1	pískovcová
socha	socha	k1gFnSc1	socha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
vyobrazuje	vyobrazovat	k5eAaImIp3nS	vyobrazovat
svatého	svatý	k2eAgMnSc4d1	svatý
Jana	Jan	k1gMnSc4	Jan
Nepomuckého	Nepomucký	k2eAgMnSc4d1	Nepomucký
<g/>
,	,	kIx,	,
českého	český	k2eAgMnSc4d1	český
patrona	patron	k1gMnSc4	patron
a	a	k8xC	a
také	také	k9	také
ochránce	ochránce	k1gMnSc1	ochránce
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
drobné	drobný	k2eAgFnPc1d1	drobná
sakrální	sakrální	k2eAgFnPc1d1	sakrální
stavby	stavba	k1gFnPc1	stavba
(	(	kIx(	(
<g/>
kříže	kříž	k1gInPc1	kříž
<g/>
,	,	kIx,	,
boží	boží	k2eAgFnSc1d1	boží
muka	muka	k1gFnSc1	muka
a	a	k8xC	a
obrázky	obrázek	k1gInPc1	obrázek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
tradice	tradice	k1gFnSc1	tradice
==	==	k?	==
</s>
</p>
<p>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
spolků	spolek	k1gInPc2	spolek
–	–	k?	–
Sbor	sbor	k1gInSc1	sbor
dobrovolných	dobrovolný	k2eAgMnPc2d1	dobrovolný
hasičů	hasič	k1gMnPc2	hasič
<g/>
,	,	kIx,	,
Myslivecké	myslivecký	k2eAgNnSc4d1	Myslivecké
sdružení	sdružení	k1gNnSc4	sdružení
Třebovská	třebovský	k2eAgFnSc1d1	Třebovská
obora	obora	k1gFnSc1	obora
<g/>
,	,	kIx,	,
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1	Tělovýchovná
jednota	jednota	k1gFnSc1	jednota
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
Divadelní	divadelní	k2eAgInSc1d1	divadelní
spolek	spolek	k1gInSc1	spolek
Garáž	garáž	k1gFnSc4	garáž
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tradiční	tradiční	k2eAgInPc4d1	tradiční
kulturní	kulturní	k2eAgInPc4d1	kulturní
počiny	počin	k1gInPc4	počin
patří	patřit	k5eAaImIp3nP	patřit
plesy	ples	k1gInPc4	ples
–	–	k?	–
Hasičský	hasičský	k2eAgInSc1d1	hasičský
<g/>
,	,	kIx,	,
Myslivecký	myslivecký	k2eAgInSc1d1	myslivecký
a	a	k8xC	a
Dětský	dětský	k2eAgInSc1d1	dětský
karneval	karneval	k1gInSc1	karneval
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
konají	konat	k5eAaImIp3nP	konat
Výroční	výroční	k2eAgFnPc1d1	výroční
ceny	cena	k1gFnPc1	cena
obce	obec	k1gFnSc2	obec
Dlouhá	Dlouhá	k1gFnSc1	Dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
oceňováni	oceňován	k2eAgMnPc1d1	oceňován
nejzasloužilejší	zasloužilý	k2eAgMnPc1d3	nejzasloužilejší
občané	občan	k1gMnPc1	občan
na	na	k7c6	na
základě	základ	k1gInSc6	základ
lidové	lidový	k2eAgFnSc2d1	lidová
ankety	anketa	k1gFnSc2	anketa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
svátku	svátek	k1gInSc2	svátek
patrona	patron	k1gMnSc2	patron
zdejší	zdejší	k2eAgFnSc2d1	zdejší
obce	obec	k1gFnSc2	obec
sv.	sv.	kA	sv.
Prokopa	Prokop	k1gMnSc2	Prokop
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
koná	konat	k5eAaImIp3nS	konat
pouť	pouť	k1gFnSc1	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kulturnímu	kulturní	k2eAgNnSc3d1	kulturní
dědictví	dědictví	k1gNnSc3	dědictví
obce	obec	k1gFnSc2	obec
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k6eAd1	mimo
památek	památka	k1gFnPc2	památka
různého	různý	k2eAgInSc2d1	různý
charakteru	charakter	k1gInSc2	charakter
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
i	i	k8xC	i
pamětní	pamětní	k2eAgFnSc1d1	pamětní
kniha	kniha	k1gFnSc1	kniha
–	–	k?	–
kronika	kronika	k1gFnSc1	kronika
obce	obec	k1gFnSc2	obec
založená	založený	k2eAgFnSc1d1	založená
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
kronikářem	kronikář	k1gMnSc7	kronikář
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
Martin	Martin	k1gMnSc1	Martin
Lamplot	Lamplot	k1gMnSc1	Lamplot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kronikáři	kronikář	k1gMnPc1	kronikář
obce	obec	k1gFnSc2	obec
===	===	k?	===
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hýbl	hýbnout	k5eAaPmAgMnS	hýbnout
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Kolomý	Kolomý	k2eAgMnSc1d1	Kolomý
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Rybka	Rybka	k1gMnSc1	Rybka
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Broulík	Broulík	k1gMnSc1	Broulík
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlasta	Vlasta	k1gFnSc1	Vlasta
Renčínová	Renčínová	k1gFnSc1	Renčínová
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Martin	Martin	k2eAgInSc1d1	Martin
Lamplot	Lamplot	k1gInSc1	Lamplot
od	od	k7c2	od
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
mírnému	mírný	k2eAgInSc3d1	mírný
úbytku	úbytek	k1gInSc3	úbytek
počtu	počet	k1gInSc2	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
až	až	k6eAd1	až
do	do	k7c2	do
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
kolísal	kolísat	k5eAaImAgMnS	kolísat
kolem	kolem	k7c2	kolem
1400	[number]	k4	1400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nahlášeno	nahlásit	k5eAaPmNgNnS	nahlásit
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
1261	[number]	k4	1261
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
je	být	k5eAaImIp3nS	být
641	[number]	k4	641
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
38,5	[number]	k4	38,5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
je	být	k5eAaImIp3nS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
ročníkem	ročník	k1gInSc7	ročník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
Třebové	Třebová	k1gFnSc6	Třebová
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
meandrující	meandrující	k2eAgFnSc2d1	meandrující
řeky	řeka	k1gFnSc2	řeka
Třebovky	Třebovka	k1gFnSc2	Třebovka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
strany	strana	k1gFnSc2	strana
ohraničeno	ohraničen	k2eAgNnSc1d1	ohraničeno
zalesněným	zalesněný	k2eAgNnPc3d1	zalesněné
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
masivním	masivní	k2eAgInSc7d1	masivní
Kozlovským	kozlovský	k2eAgInSc7d1	kozlovský
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
zemědělsky	zemědělsky	k6eAd1	zemědělsky
obdělávaná	obdělávaný	k2eAgFnSc1d1	obdělávaná
plochá	plochý	k2eAgFnSc1d1	plochá
krajina	krajina	k1gFnSc1	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Nejjižnější	jižní	k2eAgNnSc1d3	nejjižnější
a	a	k8xC	a
nejsevernější	severní	k2eAgNnSc1d3	nejsevernější
místo	místo	k1gNnSc1	místo
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
cca	cca	kA	cca
3	[number]	k4	3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
350	[number]	k4	350
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
okrajovém	okrajový	k2eAgInSc6d1	okrajový
<g/>
,	,	kIx,	,
tektonicky	tektonicky	k6eAd1	tektonicky
zprohýbaném	zprohýbaný	k2eAgMnSc6d1	zprohýbaný
a	a	k8xC	a
rozlámaném	rozlámaný	k2eAgNnSc6d1	rozlámané
pásmu	pásmo	k1gNnSc6	pásmo
české	český	k2eAgFnSc2d1	Česká
křídové	křídový	k2eAgFnSc2d1	křídová
tabule	tabule	k1gFnSc2	tabule
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
usazováním	usazování	k1gNnSc7	usazování
mořských	mořský	k2eAgInPc2d1	mořský
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Horninově	horninově	k6eAd1	horninově
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
zejména	zejména	k9	zejména
o	o	k7c4	o
opuky	opuka	k1gFnPc4	opuka
(	(	kIx(	(
<g/>
písčité	písčitý	k2eAgInPc4d1	písčitý
slínovce	slínovec	k1gInPc4	slínovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
fragmenty	fragment	k1gInPc1	fragment
spraší	spraš	k1gFnPc2	spraš
<g/>
,	,	kIx,	,
deluvií	deluvie	k1gFnPc2	deluvie
a	a	k8xC	a
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
dně	dno	k1gNnSc6	dno
také	také	k9	také
řekou	řeka	k1gFnSc7	řeka
přinesené	přinesený	k2eAgInPc1d1	přinesený
písčitohlinité	písčitohlinitý	k2eAgInPc1d1	písčitohlinitý
sedimenty	sediment	k1gInPc1	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
půd	půda	k1gFnPc2	půda
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
substrátu	substrát	k1gInSc6	substrát
nalézají	nalézat	k5eAaImIp3nP	nalézat
ve	v	k7c6	v
svazích	svah	k1gInPc6	svah
kambizemě	kambizemě	k6eAd1	kambizemě
nebo	nebo	k8xC	nebo
pseudogleje	pseudoglít	k5eAaPmIp3nS	pseudoglít
a	a	k8xC	a
na	na	k7c6	na
údolním	údolní	k2eAgNnSc6d1	údolní
dně	dno	k1gNnSc6	dno
zejména	zejména	k9	zejména
gleje	glej	k1gMnSc4	glej
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologicky	geomorfologicky	k6eAd1	geomorfologicky
se	se	k3xPyFc4	se
obec	obec	k1gFnSc1	obec
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
celku	celek	k1gInSc6	celek
Svitavská	svitavský	k2eAgFnSc1d1	Svitavská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
(	(	kIx(	(
<g/>
VIC-	VIC-	k1gFnSc1	VIC-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
konkrétně	konkrétně	k6eAd1	konkrétně
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
protáhlé	protáhlý	k2eAgFnSc6d1	protáhlá
sníženině	sníženina	k1gFnSc6	sníženina
zvané	zvaný	k2eAgFnSc6d1	zvaná
Ústecká	ústecký	k2eAgFnSc1d1	ústecká
brázda	brázda	k1gFnSc1	brázda
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západu	západ	k1gInSc2	západ
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
Kozlovským	kozlovský	k2eAgInSc7d1	kozlovský
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
do	do	k7c2	do
obce	obec	k1gFnSc2	obec
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
malým	malý	k2eAgInSc7d1	malý
výběžkem	výběžek	k1gInSc7	výběžek
známým	známý	k2eAgInSc7d1	známý
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Hůra	hůra	k1gFnSc1	hůra
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
obce	obec	k1gFnSc2	obec
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Třebovky	Třebovka	k1gFnSc2	Třebovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
levostranným	levostranný	k2eAgInSc7d1	levostranný
přítokem	přítok	k1gInSc7	přítok
Tiché	Tiché	k2eAgFnSc2d1	Tiché
Orlice	Orlice	k1gFnSc2	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
Třebovka	Třebovka	k1gFnSc1	Třebovka
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
zregulována	zregulován	k2eAgFnSc1d1	zregulována
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c4	na
ničivé	ničivý	k2eAgFnPc4d1	ničivá
povodně	povodeň	k1gFnPc4	povodeň
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
především	především	k9	především
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
a	a	k8xC	a
zpevnění	zpevnění	k1gNnSc3	zpevnění
koryta	koryto	k1gNnSc2	koryto
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
několika	několik	k4yIc2	několik
nových	nový	k2eAgFnPc2d1	nová
lávek	lávka	k1gFnPc2	lávka
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
biogeografického	biogeografický	k2eAgNnSc2d1	Biogeografické
začlenění	začlenění	k1gNnSc2	začlenění
<g/>
,	,	kIx,	,
řadíme	řadit	k5eAaImIp1nP	řadit
obec	obec	k1gFnSc4	obec
do	do	k7c2	do
Svitavského	svitavský	k2eAgInSc2d1	svitavský
bioregionu	bioregion	k1gInSc2	bioregion
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
údolí	údolí	k1gNnSc1	údolí
náleží	náležet	k5eAaImIp3nS	náležet
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubobukovému	dubobukový	k2eAgInSc3d1	dubobukový
vegetačnímu	vegetační	k2eAgInSc3d1	vegetační
stupni	stupeň	k1gInSc3	stupeň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prozrazuje	prozrazovat	k5eAaImIp3nS	prozrazovat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
skladbu	skladba	k1gFnSc4	skladba
vegetace	vegetace	k1gFnSc2	vegetace
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
bez	bez	k7c2	bez
zásahu	zásah	k1gInSc2	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
společenstva	společenstvo	k1gNnPc4	společenstvo
lužní	lužní	k2eAgNnPc4d1	lužní
a	a	k8xC	a
také	také	k9	také
olšiny	olšina	k1gFnSc2	olšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
není	být	k5eNaImIp3nS	být
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
však	však	k9	však
na	na	k7c4	na
spojnici	spojnice	k1gFnSc4	spojnice
měst	město	k1gNnPc2	město
Ústí	ústit	k5eAaImIp3nP	ústit
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
významnými	významný	k2eAgInPc7d1	významný
dopravními	dopravní	k2eAgInPc7d1	dopravní
uzly	uzel	k1gInPc7	uzel
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
železnici	železnice	k1gFnSc3	železnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
dopravních	dopravní	k2eAgInPc2d1	dopravní
koridorů	koridor	k1gInPc2	koridor
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c7	mezi
Moravou	Morava	k1gFnSc7	Morava
a	a	k8xC	a
Čechami	Čechy	k1gFnPc7	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
koridoru	koridor	k1gInSc2	koridor
využívali	využívat	k5eAaImAgMnP	využívat
již	již	k6eAd1	již
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
zmíněnou	zmíněný	k2eAgFnSc7d1	zmíněná
tratí	trať	k1gFnSc7	trať
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přímo	přímo	k6eAd1	přímo
I.	I.	kA	I.
železničním	železniční	k2eAgInSc7d1	železniční
koridorem	koridor	k1gInSc7	koridor
spojující	spojující	k2eAgInSc1d1	spojující
Děčín	Děčín	k1gInSc1	Děčín
a	a	k8xC	a
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Trať	trať	k1gFnSc4	trať
0	[number]	k4	0
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
dvoukolejné	dvoukolejný	k2eAgFnSc6d1	dvoukolejná
trati	trať	k1gFnSc6	trať
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
při	při	k7c6	při
modernizaci	modernizace	k1gFnSc6	modernizace
trati	trať	k1gFnSc2	trať
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
též	též	k9	též
nachází	nacházet	k5eAaImIp3nS	nacházet
nový	nový	k2eAgInSc1d1	nový
železniční	železniční	k2eAgInSc1d1	železniční
viadukt	viadukt	k1gInSc1	viadukt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napřimoval	napřimovat	k5eAaImAgInS	napřimovat
trať	trať	k1gFnSc4	trať
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
Třebovou	Třebová	k1gFnSc4	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
optimalizaci	optimalizace	k1gFnSc4	optimalizace
dopravy	doprava	k1gFnSc2	doprava
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
Třebové	Třebová	k1gFnSc6	Třebová
nezastavovaly	zastavovat	k5eNaImAgInP	zastavovat
osobní	osobní	k2eAgInPc1d1	osobní
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
ušetřily	ušetřit	k5eAaPmAgFnP	ušetřit
asi	asi	k9	asi
2	[number]	k4	2
minuty	minuta	k1gFnPc4	minuta
jízdní	jízdní	k2eAgFnSc2d1	jízdní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
petici	petice	k1gFnSc6	petice
místních	místní	k2eAgFnPc2d1	místní
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
záměru	záměr	k1gInSc2	záměr
kraj	kraj	k1gInSc1	kraj
částečně	částečně	k6eAd1	částečně
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
<g/>
.	.	kIx.	.
<g/>
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
komunikací	komunikace	k1gFnSc7	komunikace
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
zároveň	zároveň	k6eAd1	zároveň
obchvatem	obchvat	k1gInSc7	obchvat
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
dobrým	dobrý	k2eAgNnSc7d1	dobré
dopravním	dopravní	k2eAgNnSc7d1	dopravní
napojením	napojení	k1gNnSc7	napojení
na	na	k7c6	na
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Skrze	Skrze	k?	Skrze
obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
linková	linkový	k2eAgFnSc1d1	Linková
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
společností	společnost	k1gFnPc2	společnost
ČSAD	ČSAD	kA	ČSAD
BUS	bus	k1gInSc1	bus
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
ICOM	ICOM	kA	ICOM
transport	transport	k1gInSc4	transport
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
spojení	spojení	k1gNnSc1	spojení
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
Třebové	Třebová	k1gFnSc2	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
Trendem	trend	k1gInSc7	trend
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
stavba	stavba	k1gFnSc1	stavba
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
byla	být	k5eAaImAgFnS	být
také	také	k9	také
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
vede	vést	k5eAaImIp3nS	vést
cyklotrasa	cyklotrasa	k1gFnSc1	cyklotrasa
č.	č.	k?	č.
4061	[number]	k4	4061
mezi	mezi	k7c7	mezi
Ústím	ústí	k1gNnSc7	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
a	a	k8xC	a
Českou	český	k2eAgFnSc7d1	Česká
Třebovou	Třebová	k1gFnSc7	Třebová
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
cyklotrasou	cyklotrasa	k1gFnSc7	cyklotrasa
je	být	k5eAaImIp3nS	být
č.	č.	k?	č.
4051	[number]	k4	4051
spojující	spojující	k2eAgInSc1d1	spojující
Knapovec	Knapovec	k1gInSc1	Knapovec
a	a	k8xC	a
Přívrat	přívrat	k1gInSc1	přívrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
Třebová	Třebová	k1gFnSc1	Třebová
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přehledná	přehledný	k2eAgFnSc1d1	přehledná
historie	historie	k1gFnSc1	historie
obce	obec	k1gFnSc2	obec
</s>
</p>
