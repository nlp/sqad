<p>
<s>
dir	dir	k?	dir
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
název	název	k1gInSc1	název
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
příkazů	příkaz	k1gInPc2	příkaz
systémů	systém	k1gInPc2	systém
MS	MS	kA	MS
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
MS-DOS	MS-DOS	k1gFnSc1	MS-DOS
a	a	k8xC	a
CP	CP	kA	CP
<g/>
/	/	kIx~	/
<g/>
M.	M.	kA	M.
V	v	k7c6	v
příkazovém	příkazový	k2eAgInSc6d1	příkazový
řádku	řádek	k1gInSc6	řádek
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vypsání	vypsání	k1gNnSc3	vypsání
obsahu	obsah	k1gInSc2	obsah
adresáře	adresář	k1gInSc2	adresář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc4	jeho
obdobou	obdoba	k1gFnSc7	obdoba
příkaz	příkaz	k1gInSc4	příkaz
ls	ls	k?	ls
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
na	na	k7c6	na
platformách	platforma	k1gFnPc6	platforma
MS	MS	kA	MS
DOS	DOS	kA	DOS
a	a	k8xC	a
MS	MS	kA	MS
Windows	Windows	kA	Windows
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
příkaz	příkaz	k1gInSc4	příkaz
dir	dir	k?	dir
volán	volán	k1gInSc1	volán
bez	bez	k7c2	bez
argumentů	argument	k1gInPc2	argument
<g/>
,	,	kIx,	,
vypíše	vypsat	k5eAaPmIp3nS	vypsat
soubory	soubor	k1gInPc4	soubor
a	a	k8xC	a
složky	složka	k1gFnPc4	složka
v	v	k7c6	v
aktuálním	aktuální	k2eAgInSc6d1	aktuální
pracovním	pracovní	k2eAgInSc6d1	pracovní
adresáři	adresář	k1gInSc6	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
parametr	parametr	k1gInSc1	parametr
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
specifikován	specifikovat	k5eAaBmNgInS	specifikovat
i	i	k9	i
jiný	jiný	k2eAgInSc1d1	jiný
adresář	adresář	k1gInSc1	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Skryté	skrytý	k2eAgInPc1d1	skrytý
a	a	k8xC	a
systémové	systémový	k2eAgInPc1d1	systémový
soubory	soubor	k1gInPc1	soubor
nejsou	být	k5eNaImIp3nP	být
vypisovány	vypisován	k2eAgInPc1d1	vypisován
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
explicitně	explicitně	k6eAd1	explicitně
požadováno	požadován	k2eAgNnSc1d1	požadováno
pomocí	pomocí	k7c2	pomocí
parametru	parametr	k1gInSc2	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
parametrů	parametr	k1gInPc2	parametr
vypisuje	vypisovat	k5eAaImIp3nS	vypisovat
příkaz	příkaz	k1gInSc4	příkaz
soubory	soubor	k1gInPc7	soubor
a	a	k8xC	a
adresáře	adresář	k1gInPc1	adresář
v	v	k7c6	v
holém	holý	k2eAgInSc6d1	holý
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
vypisován	vypisován	k2eAgInSc4d1	vypisován
datum	datum	k1gInSc4	datum
vytvoření	vytvoření	k1gNnSc4	vytvoření
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
text	text	k1gInSc1	text
<DIR>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
adresář	adresář	k1gInSc4	adresář
<g/>
,	,	kIx,	,
a	a	k8xC	a
název	název	k1gInSc1	název
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
adresáře	adresář	k1gInPc1	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vlastního	vlastní	k2eAgInSc2d1	vlastní
seznamu	seznam	k1gInSc2	seznam
vypíše	vypsat	k5eAaPmIp3nS	vypsat
ještě	ještě	k9	ještě
záhlaví	záhlaví	k1gNnSc1	záhlaví
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
disku	disk	k1gInSc2	disk
a	a	k8xC	a
adresáře	adresář	k1gInSc2	adresář
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výpis	výpis	k1gInSc1	výpis
je	být	k5eAaImIp3nS	být
prováděn	provádět	k5eAaImNgInS	provádět
<g/>
,	,	kIx,	,
a	a	k8xC	a
zápatí	zápatí	k1gNnSc4	zápatí
se	s	k7c7	s
shrnutím	shrnutí	k1gNnSc7	shrnutí
počtu	počet	k1gInSc2	počet
souborů	soubor	k1gInPc2	soubor
a	a	k8xC	a
adresářů	adresář	k1gInPc2	adresář
a	a	k8xC	a
součtu	součet	k1gInSc6	součet
velikosti	velikost	k1gFnSc2	velikost
všech	všecek	k3xTgInPc2	všecek
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Parametry	parametr	k1gInPc1	parametr
příkazu	příkaz	k1gInSc2	příkaz
===	===	k?	===
</s>
</p>
<p>
<s>
disk	disk	k1gInSc1	disk
<g/>
:	:	kIx,	:
–	–	k?	–
výpis	výpis	k1gInSc4	výpis
obsahu	obsah	k1gInSc2	obsah
adresáře	adresář	k1gInSc2	adresář
na	na	k7c6	na
určeném	určený	k2eAgInSc6d1	určený
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
cesta	cesta	k1gFnSc1	cesta
–	–	k?	–
výpis	výpis	k1gInSc4	výpis
obsahu	obsah	k1gInSc2	obsah
zvoleného	zvolený	k2eAgInSc2d1	zvolený
adresáře	adresář	k1gInSc2	adresář
</s>
</p>
<p>
<s>
soubor	soubor	k1gInSc1	soubor
–	–	k?	–
výpis	výpis	k1gInSc1	výpis
detailů	detail	k1gInPc2	detail
o	o	k7c6	o
zvoleném	zvolený	k2eAgInSc6d1	zvolený
souboru	soubor	k1gInSc6	soubor
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
použití	použití	k1gNnSc2	použití
hvězdičkové	hvězdičkový	k2eAgFnSc2d1	hvězdičková
konvence	konvence	k1gFnSc2	konvence
výpis	výpis	k1gInSc4	výpis
souborů	soubor	k1gInPc2	soubor
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
název	název	k1gInSc1	název
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
požadované	požadovaný	k2eAgInPc4d1	požadovaný
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
A	a	k9	a
<g/>
:	:	kIx,	:
<g/>
atributy	atribut	k1gInPc4	atribut
–	–	k?	–
výpis	výpis	k1gInSc4	výpis
souborů	soubor	k1gInPc2	soubor
se	s	k7c7	s
zvolenými	zvolený	k2eAgInPc7d1	zvolený
atributy	atribut	k1gInPc7	atribut
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
použití	použití	k1gNnSc2	použití
znaku	znak	k1gInSc2	znak
pomlčka	pomlčka	k1gFnSc1	pomlčka
před	před	k7c7	před
písmenem	písmeno	k1gNnSc7	písmeno
atributu	atribut	k1gInSc2	atribut
výpis	výpis	k1gInSc4	výpis
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tento	tento	k3xDgInSc1	tento
atribut	atribut	k1gInSc1	atribut
nastaven	nastaven	k2eAgInSc1d1	nastaven
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
B	B	kA	B
–	–	k?	–
potlačí	potlačit	k5eAaPmIp3nS	potlačit
výpis	výpis	k1gInSc4	výpis
záhlaví	záhlaví	k1gNnSc2	záhlaví
a	a	k8xC	a
zápatí	zápatí	k1gNnSc2	zápatí
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
C	C	kA	C
–	–	k?	–
vypíše	vypsat	k5eAaPmIp3nS	vypsat
oddělovače	oddělovač	k1gInPc4	oddělovač
tisíců	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgNnSc1d1	standardní
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,	,
oddělovače	oddělovač	k1gInPc1	oddělovač
tisíců	tisíc	k4xCgInPc2	tisíc
lze	lze	k6eAd1	lze
potlačit	potlačit	k5eAaPmF	potlačit
volbou	volba	k1gFnSc7	volba
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
C.	C.	kA	C.
Ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
MS-DOS	MS-DOS	k1gMnSc2	MS-DOS
6.22	[number]	k4	6.22
tento	tento	k3xDgInSc4	tento
parametr	parametr	k1gInSc1	parametr
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výpisu	výpis	k1gInSc3	výpis
kompresního	kompresní	k2eAgInSc2d1	kompresní
poměru	poměr	k1gInSc2	poměr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ke	k	k7c3	k
komprimaci	komprimace	k1gFnSc3	komprimace
souboru	soubor	k1gInSc2	soubor
použit	použít	k5eAaPmNgInS	použít
program	program	k1gInSc1	program
DoubleSpace	DoubleSpace	k1gFnSc2	DoubleSpace
nebo	nebo	k8xC	nebo
DriveSpace	DriveSpace	k1gFnSc2	DriveSpace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
také	také	k9	také
varianta	varianta	k1gFnSc1	varianta
/	/	kIx~	/
<g/>
CH	Ch	kA	Ch
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
D	D	kA	D
–	–	k?	–
setříděný	setříděný	k2eAgInSc4d1	setříděný
výpis	výpis	k1gInSc4	výpis
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
L	L	kA	L
–	–	k?	–
výpis	výpis	k1gInSc1	výpis
pouze	pouze	k6eAd1	pouze
malými	malý	k2eAgFnPc7d1	malá
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
D	D	kA	D
–	–	k?	–
výpis	výpis	k1gInSc1	výpis
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jména	jméno	k1gNnPc1	jméno
souborů	soubor	k1gInPc2	soubor
jsou	být	k5eAaImIp3nP	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
úplně	úplně	k6eAd1	úplně
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
O	o	k7c4	o
<g/>
:	:	kIx,	:
<g/>
způsob	způsob	k1gInSc4	způsob
řazení	řazení	k1gNnSc2	řazení
–	–	k?	–
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
souboru	soubor	k1gInSc6	soubor
bude	být	k5eAaImBp3nS	být
výpis	výpis	k1gInSc1	výpis
seřazen	seřadit	k5eAaPmNgInS	seřadit
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
přípona	přípona	k1gFnSc1	přípona
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc4	velikost
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc1	datum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
P	P	kA	P
–	–	k?	–
u	u	k7c2	u
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
výpisů	výpis	k1gInPc2	výpis
po	po	k7c6	po
zaplnění	zaplnění	k1gNnSc6	zaplnění
obrazovky	obrazovka	k1gFnSc2	obrazovka
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
stisk	stisk	k1gInSc4	stisk
klávesy	klávesa	k1gFnSc2	klávesa
pro	pro	k7c4	pro
pokračování	pokračování	k1gNnSc4	pokračování
výpisu	výpis	k1gInSc2	výpis
(	(	kIx(	(
<g/>
alternativně	alternativně	k6eAd1	alternativně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stejného	stejný	k2eAgInSc2d1	stejný
výsledku	výsledek	k1gInSc2	výsledek
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
pomocí	pomocí	k7c2	pomocí
konstrukce	konstrukce	k1gFnSc2	konstrukce
dir	dir	k?	dir
|	|	kIx~	|
more	mor	k1gInSc5	mor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
výstup	výstup	k1gInSc1	výstup
příkazu	příkaz	k1gInSc2	příkaz
dir	dir	k?	dir
je	být	k5eAaImIp3nS	být
posílán	posílat	k5eAaImNgInS	posílat
rourou	roura	k1gFnSc7	roura
příkazu	příkaz	k1gInSc2	příkaz
more	mor	k1gInSc5	mor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
Q	Q	kA	Q
–	–	k?	–
vypíše	vypsat	k5eAaPmIp3nS	vypsat
i	i	k9	i
majitele	majitel	k1gMnPc4	majitel
souboru	soubor	k1gInSc2	soubor
nebo	nebo	k8xC	nebo
adresáře	adresář	k1gInSc2	adresář
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
R	R	kA	R
–	–	k?	–
vypíše	vypsat	k5eAaPmIp3nS	vypsat
alternativní	alternativní	k2eAgInPc4d1	alternativní
streamy	stream	k1gInPc4	stream
souboru	soubor	k1gInSc2	soubor
(	(	kIx(	(
<g/>
týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
NTFS	NTFS	kA	NTFS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
S	s	k7c7	s
–	–	k?	–
vypíše	vypsat	k5eAaPmIp3nS	vypsat
i	i	k8xC	i
obsah	obsah	k1gInSc1	obsah
všech	všecek	k3xTgInPc2	všecek
podadresářů	podadresář	k1gInPc2	podadresář
zvoleného	zvolený	k2eAgInSc2d1	zvolený
adresáře	adresář	k1gInSc2	adresář
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
T	T	kA	T
<g/>
:	:	kIx,	:
<g/>
časový	časový	k2eAgInSc1d1	časový
údaj	údaj	k1gInSc1	údaj
–	–	k?	–
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
časový	časový	k2eAgInSc1d1	časový
a	a	k8xC	a
datový	datový	k2eAgInSc1d1	datový
údaj	údaj	k1gInSc1	údaj
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
vypisovat	vypisovat	k5eAaImF	vypisovat
a	a	k8xC	a
podle	podle	k7c2	podle
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
výpis	výpis	k1gInSc1	výpis
řadit	řadit	k5eAaImF	řadit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
vytvoření	vytvoření	k1gNnSc2	vytvoření
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc2d1	poslední
změny	změna	k1gFnSc2	změna
nebo	nebo	k8xC	nebo
posledního	poslední	k2eAgInSc2d1	poslední
přístupu	přístup	k1gInSc2	přístup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
W	W	kA	W
–	–	k?	–
výpis	výpis	k1gInSc1	výpis
obsahu	obsah	k1gInSc2	obsah
adresáře	adresář	k1gInSc2	adresář
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
X	X	kA	X
–	–	k?	–
kromě	kromě	k7c2	kromě
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
jmen	jméno	k1gNnPc2	jméno
souborů	soubor	k1gInPc2	soubor
vypisuje	vypisovat	k5eAaImIp3nS	vypisovat
i	i	k8xC	i
krátká	krátký	k2eAgNnPc1d1	krátké
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
8.3	[number]	k4	8.3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
4	[number]	k4	4
–	–	k?	–
vypisuje	vypisovat	k5eAaImIp3nS	vypisovat
rok	rok	k1gInSc4	rok
jako	jako	k8xS	jako
čtyřciferné	čtyřciferný	k2eAgNnSc4d1	čtyřciferné
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
/	/	kIx~	/
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
místo	místo	k7c2	místo
obsahu	obsah	k1gInSc2	obsah
adresáře	adresář	k1gInSc2	adresář
vypíše	vypsat	k5eAaPmIp3nS	vypsat
nápovědu	nápověda	k1gFnSc4	nápověda
k	k	k7c3	k
příkazu	příkaz	k1gInSc3	příkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přidružené	přidružený	k2eAgInPc1d1	přidružený
příkazy	příkaz	k1gInPc1	příkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
příkazem	příkaz	k1gInSc7	příkaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výpisu	výpis	k1gInSc3	výpis
obsahu	obsah	k1gInSc2	obsah
adresáře	adresář	k1gInSc2	adresář
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
příkaz	příkaz	k1gInSc4	příkaz
tree	tre	k1gInSc2	tre
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
celou	celý	k2eAgFnSc4d1	celá
stromovou	stromový	k2eAgFnSc4d1	stromová
strukturu	struktura	k1gFnSc4	struktura
zvoleného	zvolený	k2eAgInSc2d1	zvolený
adresáře	adresář	k1gInSc2	adresář
včetně	včetně	k7c2	včetně
grafického	grafický	k2eAgNnSc2d1	grafické
znázornění	znázornění	k1gNnSc2	znázornění
stromové	stromový	k2eAgFnSc2d1	stromová
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BARÁNEK	BARÁNEK	kA	BARÁNEK
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
MS-DOS	MS-DOS	k?	MS-DOS
6.2	[number]	k4	6.2
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
208	[number]	k4	208
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ČERNÝ	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
MS-DOS	MS-DOS	k?	MS-DOS
6.2	[number]	k4	6.2
Prakticky	prakticky	k6eAd1	prakticky
6.22	[number]	k4	6.22
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85828	[number]	k4	85828
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
