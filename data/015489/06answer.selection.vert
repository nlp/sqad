<s>
Městská	městský	k2eAgFnSc1d1
hromadná	hromadný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
(	(	kIx(
<g/>
MHD	MHD	kA
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
označovaná	označovaný	k2eAgFnSc1d1
zkráceně	zkráceně	k6eAd1
jen	jen	k9
městská	městský	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
systém	systém	k1gInSc1
linek	linka	k1gFnPc2
osobní	osobní	k2eAgFnSc2d1
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
určených	určený	k2eAgInPc2d1
k	k	k7c3
zajišťování	zajišťování	k1gNnSc3
dopravní	dopravní	k2eAgFnSc2d1
obsluhy	obsluha	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
hromadnými	hromadný	k2eAgInPc7d1
dopravními	dopravní	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
<g/>
.	.	kIx.
</s>