<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
používání	používání	k1gNnSc2	používání
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
(	(	kIx(	(
<g/>
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
až	až	k9	až
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
vlajka	vlajka	k1gFnSc1	vlajka
používána	používat	k5eAaImNgFnS	používat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
československé	československý	k2eAgFnSc2d1	Československá
státnosti	státnost	k1gFnSc2	státnost
včetně	včetně	k7c2	včetně
období	období	k1gNnSc2	období
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1938	[number]	k4	1938
až	až	k9	až
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
exilovou	exilový	k2eAgFnSc7d1	exilová
reprezentací	reprezentace	k1gFnSc7	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
uzákonění	uzákonění	k1gNnSc2	uzákonění
vlajky	vlajka	k1gFnPc4	vlajka
protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
byla	být	k5eAaImAgFnS	být
používána	používat	k5eAaImNgFnS	používat
okupačními	okupační	k2eAgInPc7d1	okupační
a	a	k8xC	a
protektorátními	protektorátní	k2eAgInPc7d1	protektorátní
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
měla	mít	k5eAaImAgFnS	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
dvojbarevnou	dvojbarevný	k2eAgFnSc4d1	dvojbarevná
bíločervenou	bíločervený	k2eAgFnSc4d1	bíločervená
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
užívána	užívat	k5eAaImNgFnS	užívat
jen	jen	k6eAd1	jen
velmi	velmi	k6eAd1	velmi
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Bikolóra	Bikolóra	k1gFnSc1	Bikolóra
a	a	k8xC	a
trikolóra	trikolóra	k1gFnSc1	trikolóra
===	===	k?	===
</s>
</p>
<p>
<s>
Historickou	historický	k2eAgFnSc7d1	historická
vlajkou	vlajka	k1gFnSc7	vlajka
Čech	Čechy	k1gFnPc2	Čechy
byla	být	k5eAaImAgFnS	být
bíločervená	bíločervený	k2eAgFnSc1d1	bíločervená
bikolóra	bikolóra	k1gFnSc1	bikolóra
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
sousedního	sousední	k2eAgNnSc2d1	sousední
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
bikolóra	bikolóra	k1gFnSc1	bikolóra
měla	mít	k5eAaImAgFnS	mít
svou	svůj	k3xOyFgFnSc4	svůj
historickou	historický	k2eAgFnSc4d1	historická
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
(	(	kIx(	(
<g/>
státním	státní	k2eAgInSc6d1	státní
<g/>
)	)	kIx)	)
vojenském	vojenský	k2eAgInSc6d1	vojenský
praporu	prapor	k1gInSc6	prapor
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgFnPc4d1	využívající
barvy	barva	k1gFnPc4	barva
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tinktury	tinktura	k1gFnSc2	tinktura
<g/>
)	)	kIx)	)
erbu	erb	k1gInSc3	erb
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
heraldických	heraldický	k2eAgNnPc2d1	heraldické
pravidel	pravidlo	k1gNnPc2	pravidlo
se	se	k3xPyFc4	se
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
praporu	prapor	k1gInSc6	prapor
objevovala	objevovat	k5eAaImAgFnS	objevovat
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
polovině	polovina	k1gFnSc6	polovina
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
symbolizující	symbolizující	k2eAgFnSc1d1	symbolizující
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
českého	český	k2eAgInSc2d1	český
lva	lev	k1gInSc2	lev
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
dolní	dolní	k2eAgFnSc6d1	dolní
polovině	polovina	k1gFnSc6	polovina
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
symbolizující	symbolizující	k2eAgNnSc1d1	symbolizující
červené	červený	k2eAgNnSc1d1	červené
pole	pole	k1gNnSc1	pole
štítu	štít	k1gInSc2	štít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
tento	tento	k3xDgInSc1	tento
český	český	k2eAgInSc1d1	český
prapor	prapor	k1gInSc1	prapor
nelišil	lišit	k5eNaImAgInS	lišit
od	od	k7c2	od
polského	polský	k2eAgInSc2d1	polský
státního	státní	k2eAgInSc2d1	státní
praporu	prapor	k1gInSc2	prapor
(	(	kIx(	(
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
orlice	orlice	k1gFnSc1	orlice
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
historická	historický	k2eAgFnSc1d1	historická
bikolóra	bikolóra	k1gFnSc1	bikolóra
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
razit	razit	k5eAaImF	razit
panslovanské	panslovanský	k2eAgFnPc4d1	panslovanská
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
pomalu	pomalu	k6eAd1	pomalu
ustupovat	ustupovat	k5eAaImF	ustupovat
slovanské	slovanský	k2eAgInPc4d1	slovanský
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
ruské	ruský	k2eAgFnSc6d1	ruská
<g/>
)	)	kIx)	)
trikolóře	trikolóra	k1gFnSc6	trikolóra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
)	)	kIx)	)
rovněž	rovněž	k9	rovněž
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odlišení	odlišení	k1gNnSc4	odlišení
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
trikolór	trikolóra	k1gFnPc2	trikolóra
<g/>
,	,	kIx,	,
dělící	dělící	k2eAgNnSc4d1	dělící
své	své	k1gNnSc4	své
národní	národní	k2eAgFnSc2d1	národní
barvy	barva	k1gFnSc2	barva
ve	v	k7c6	v
vlajce	vlajka	k1gFnSc6	vlajka
do	do	k7c2	do
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
r.	r.	kA	r.
1920	[number]	k4	1920
zákonem	zákon	k1gInSc7	zákon
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
<g/>
,	,	kIx,	,
že	že	k8xS	že
pole	pole	k1gNnSc1	pole
přidané	přidaný	k2eAgFnSc2d1	přidaná
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
tvar	tvar	k1gInSc4	tvar
klínu	klín	k1gInSc2	klín
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgNnSc1d1	směřující
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
tohoto	tento	k3xDgInSc2	tento
modrého	modrý	k2eAgInSc2d1	modrý
klínu	klín	k1gInSc2	klín
byl	být	k5eAaImAgInS	být
obvykle	obvykle	k6eAd1	obvykle
vykládán	vykládat	k5eAaImNgInS	vykládat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Československo	Československo	k1gNnSc1	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
a	a	k8xC	a
unitární	unitární	k2eAgFnSc2d1	unitární
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
vyvstal	vyvstat	k5eAaPmAgInS	vyvstat
problém	problém	k1gInSc1	problém
mít	mít	k5eAaImF	mít
jako	jako	k9	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
stát	stát	k5eAaImF	stát
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
stát	stát	k5eAaImF	stát
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
dvojbarevná	dvojbarevný	k2eAgFnSc1d1	dvojbarevná
bíločervená	bíločervený	k2eAgFnSc1d1	bíločervená
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Speciálně	speciálně	k6eAd1	speciálně
určená	určený	k2eAgFnSc1d1	určená
komise	komise	k1gFnSc1	komise
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
popud	popud	k1gInSc4	popud
univerzitního	univerzitní	k2eAgMnSc2d1	univerzitní
profesora	profesor	k1gMnSc2	profesor
G.	G.	kA	G.
Friedricha	Friedrich	k1gMnSc2	Friedrich
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
úkol	úkol	k1gInSc1	úkol
vytvořit	vytvořit	k5eAaPmF	vytvořit
návrh	návrh	k1gInSc4	návrh
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
několik	několik	k4yIc1	několik
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgMnPc2	který
nejvíce	nejvíce	k6eAd1	nejvíce
kladných	kladný	k2eAgMnPc2d1	kladný
ohlasů	ohlas	k1gInPc2	ohlas
sklidil	sklidit	k5eAaPmAgInS	sklidit
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
horizontální	horizontální	k2eAgFnSc4d1	horizontální
bikolóru	bikolóra	k1gFnSc4	bikolóra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
modrým	modrý	k2eAgInSc7d1	modrý
klínem	klín	k1gInSc7	klín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sahal	sahat	k5eAaImAgInS	sahat
do	do	k7c2	do
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
tohoto	tento	k3xDgInSc2	tento
návrhu	návrh	k1gInSc2	návrh
byl	být	k5eAaImAgMnS	být
státní	státní	k2eAgMnSc1d1	státní
úředník	úředník	k1gMnSc1	úředník
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kursa	Kursa	k1gFnSc1	Kursa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úpravách	úprava	k1gFnPc6	úprava
byl	být	k5eAaImAgInS	být
klín	klín	k1gInSc1	klín
prodloužen	prodloužit	k5eAaPmNgInS	prodloužit
a	a	k8xC	a
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
vlajky	vlajka	k1gFnSc2	vlajka
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
poslanci	poslanec	k1gMnPc1	poslanec
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
v	v	k7c4	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
a	a	k8xC	a
uznána	uznán	k2eAgFnSc1d1	uznána
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
oficiální	oficiální	k2eAgFnSc7d1	oficiální
vlajkou	vlajka	k1gFnSc7	vlajka
Československa	Československo	k1gNnSc2	Československo
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gNnSc2	jeho
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
vlajek	vlajka	k1gFnPc2	vlajka
používajících	používající	k2eAgFnPc2d1	používající
klín	klín	k1gInSc4	klín
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sporná	sporný	k2eAgNnPc1d1	sporné
autorství	autorství	k1gNnPc1	autorství
===	===	k?	===
</s>
</p>
<p>
<s>
Kursa	Kurs	k1gMnSc4	Kurs
nebyl	být	k5eNaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
připisovalo	připisovat	k5eAaImAgNnS	připisovat
autorství	autorství	k1gNnSc1	autorství
české	český	k2eAgFnSc2d1	Česká
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
například	například	k6eAd1	například
všeobecně	všeobecně	k6eAd1	všeobecně
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Jareš	Jareš	k1gMnSc1	Jareš
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
skonu	skon	k1gInSc6	skon
<g/>
,	,	kIx,	,
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
časopis	časopis	k1gInSc1	časopis
Reportér	reportér	k1gMnSc1	reportér
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
domnělém	domnělý	k2eAgMnSc6d1	domnělý
autorovi	autor	k1gMnSc6	autor
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
o	o	k7c4	o
autorství	autorství	k1gNnSc4	autorství
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
údajně	údajně	k6eAd1	údajně
náhodou	náhodou	k6eAd1	náhodou
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
využila	využít	k5eAaPmAgFnS	využít
odchodu	odchod	k1gInSc3	odchod
svého	svůj	k3xOyFgMnSc4	svůj
muže	muž	k1gMnSc4	muž
do	do	k7c2	do
kavárny	kavárna	k1gFnSc2	kavárna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
jako	jako	k9	jako
obvykle	obvykle	k6eAd1	obvykle
udělala	udělat	k5eAaPmAgFnS	udělat
pořádek	pořádek	k1gInSc4	pořádek
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mezi	mezi	k7c7	mezi
starými	starý	k2eAgFnPc7d1	stará
kresbami	kresba	k1gFnPc7	kresba
nalezla	nalézt	k5eAaBmAgFnS	nalézt
i	i	k9	i
několik	několik	k4yIc4	několik
návrhů	návrh	k1gInPc2	návrh
na	na	k7c4	na
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
žádala	žádat	k5eAaImAgFnS	žádat
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
muži	muž	k1gMnPc1	muž
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
k	k	k7c3	k
autorství	autorství	k1gNnSc3	autorství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
doposud	doposud	k6eAd1	doposud
důsledně	důsledně	k6eAd1	důsledně
tajil	tajit	k5eAaImAgMnS	tajit
<g/>
,	,	kIx,	,
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
Jarešovými	Jarešův	k2eAgInPc7d1	Jarešův
návrhy	návrh	k1gInPc7	návrh
figuroval	figurovat	k5eAaImAgInS	figurovat
např.	např.	kA	např.
husitský	husitský	k2eAgInSc4d1	husitský
motiv	motiv	k1gInSc4	motiv
s	s	k7c7	s
černým	černý	k2eAgInSc7d1	černý
klínem	klín	k1gInSc7	klín
a	a	k8xC	a
emblémem	emblém	k1gInSc7	emblém
kalicha	kalich	k1gInSc2	kalich
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgInS	mít
dle	dle	k7c2	dle
autorových	autorův	k2eAgFnPc2d1	autorova
představ	představa	k1gFnPc2	představa
propojovat	propojovat	k5eAaImF	propojovat
bojové	bojový	k2eAgFnPc4d1	bojová
tradice	tradice	k1gFnPc4	tradice
husitské	husitský	k2eAgFnPc4d1	husitská
s	s	k7c7	s
legionářskými	legionářský	k2eAgInPc7d1	legionářský
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
–	–	k?	–
a	a	k8xC	a
"	"	kIx"	"
<g/>
vítězný	vítězný	k2eAgInSc4d1	vítězný
<g/>
"	"	kIx"	"
–	–	k?	–
modrý	modrý	k2eAgInSc4d1	modrý
klín	klín	k1gInSc4	klín
pak	pak	k6eAd1	pak
údajně	údajně	k6eAd1	údajně
znázorňoval	znázorňovat	k5eAaImAgInS	znázorňovat
barvu	barva	k1gFnSc4	barva
slovenských	slovenský	k2eAgFnPc2d1	slovenská
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
si	se	k3xPyFc3	se
Jareš	Jareš	k1gFnSc1	Jareš
jako	jako	k8xC	jako
krajinář	krajinář	k1gMnSc1	krajinář
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
České	český	k2eAgFnSc2d1	Česká
vexilologické	vexilologický	k2eAgFnSc2d1	vexilologická
společnosti	společnost	k1gFnSc2	společnost
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
autorství	autorství	k1gNnSc2	autorství
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
nicméně	nicméně	k8xC	nicméně
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tiskovém	tiskový	k2eAgNnSc6d1	tiskové
sdělení	sdělení	k1gNnSc6	sdělení
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
není	být	k5eNaImIp3nS	být
o	o	k7c6	o
Jarešovi	Jareš	k1gMnSc6	Jareš
žádná	žádný	k3yNgFnSc1	žádný
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
.	.	kIx.	.
</s>
<s>
Byť	byť	k8xS	byť
se	se	k3xPyFc4	se
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
prací	práce	k1gFnPc2	práce
tohoto	tento	k3xDgMnSc2	tento
malíře	malíř	k1gMnSc2	malíř
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
objevila	objevit	k5eAaPmAgFnS	objevit
kresba	kresba	k1gFnSc1	kresba
vlajky	vlajka	k1gFnSc2	vlajka
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
vítězným	vítězný	k2eAgInSc7d1	vítězný
návrhem	návrh	k1gInSc7	návrh
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
skutečně	skutečně	k6eAd1	skutečně
před	před	k7c7	před
přijetím	přijetí	k1gNnSc7	přijetí
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozhodně	rozhodně	k6eAd1	rozhodně
neopustila	opustit	k5eNaPmAgFnS	opustit
malířův	malířův	k2eAgInSc4d1	malířův
ateliér	ateliér	k1gInSc4	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vexilologa	vexilolog	k1gMnSc2	vexilolog
Aleše	Aleš	k1gMnSc2	Aleš
Brožka	Brožek	k1gMnSc2	Brožek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
historii	historie	k1gFnSc3	historie
české	český	k2eAgFnPc4d1	Česká
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
,	,	kIx,	,
zas	zas	k6eAd1	zas
Jareš	Jareš	k1gMnSc1	Jareš
sice	sice	k8xC	sice
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
došel	dojít	k5eAaPmAgInS	dojít
ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
závěru	závěr	k1gInSc3	závěr
a	a	k8xC	a
stejné	stejný	k2eAgFnSc3d1	stejná
myšlence	myšlenka	k1gFnSc3	myšlenka
jako	jako	k9	jako
členové	člen	k1gMnPc1	člen
kolektivu	kolektiv	k1gInSc2	kolektiv
znakové	znakový	k2eAgFnSc2d1	znaková
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
však	však	k9	však
jejím	její	k3xOp3gInSc7	její
členem	člen	k1gInSc7	člen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jej	on	k3xPp3gMnSc4	on
nelze	lze	k6eNd1	lze
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
pravého	pravý	k2eAgMnSc4d1	pravý
autora	autor	k1gMnSc4	autor
<g/>
.	.	kIx.	.
<g/>
Jiným	jiný	k2eAgMnSc7d1	jiný
pretendentem	pretendent	k1gMnSc7	pretendent
<g />
.	.	kIx.	.
</s>
<s>
autorství	autorství	k1gNnSc4	autorství
vlajky	vlajka	k1gFnSc2	vlajka
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
Čech	Čech	k1gMnSc1	Čech
Josef	Josef	k1gMnSc1	Josef
Knedlhans	Knedlhans	k1gInSc4	Knedlhans
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
vystěhoval	vystěhovat	k5eAaPmAgMnS	vystěhovat
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
aktivním	aktivní	k2eAgMnSc7d1	aktivní
Sokolem	Sokol	k1gMnSc7	Sokol
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
vítal	vítat	k5eAaImAgMnS	vítat
Masaryka	Masaryk	k1gMnSc4	Masaryk
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
vlajkou	vlajka	k1gFnSc7	vlajka
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
klínem	klín	k1gInSc7	klín
<g/>
,	,	kIx,	,
podobnou	podobný	k2eAgFnSc7d1	podobná
té	ten	k3xDgFnSc2	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
osobního	osobní	k2eAgNnSc2d1	osobní
tvrzení	tvrzení	k1gNnSc2	tvrzení
se	se	k3xPyFc4	se
však	však	k9	však
nedochoval	dochovat	k5eNaPmAgInS	dochovat
žádný	žádný	k3yNgInSc4	žádný
důkaz	důkaz	k1gInSc4	důkaz
o	o	k7c6	o
pravosti	pravost	k1gFnSc6	pravost
autorova	autorův	k2eAgInSc2d1	autorův
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odborníků	odborník	k1gMnPc2	odborník
z	z	k7c2	z
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
vlajky	vlajka	k1gFnSc2	vlajka
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
klínem	klín	k1gInSc7	klín
nelze	lze	k6eNd1	lze
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedeném	uvedený	k2eAgInSc6d1	uvedený
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byla	být	k5eAaImAgNnP	být
vyrobena	vyrobit	k5eAaPmNgNnP	vyrobit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
až	až	k9	až
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
exemplář	exemplář	k1gInSc1	exemplář
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
muzejní	muzejní	k2eAgFnSc2d1	muzejní
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
předán	předat	k5eAaPmNgInS	předat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
a	a	k8xC	a
rozměry	rozměr	k1gInPc1	rozměr
jejího	její	k3xOp3gNnSc2	její
modrého	modré	k1gNnSc2	modré
klínu	klín	k1gInSc2	klín
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
této	tento	k3xDgFnSc2	tento
vlajky	vlajka	k1gFnSc2	vlajka
navíc	navíc	k6eAd1	navíc
neodpovídají	odpovídat	k5eNaImIp3nP	odpovídat
podobě	podoba	k1gFnSc3	podoba
přijaté	přijatý	k2eAgFnSc3d1	přijatá
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federace	federace	k1gFnSc2	federace
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federalizace	federalizace	k1gFnSc1	federalizace
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
neměla	mít	k5eNaImAgFnS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
znak	znak	k1gInSc4	znak
ani	ani	k8xC	ani
vlajku	vlajka	k1gFnSc4	vlajka
–	–	k?	–
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
přiřazována	přiřazovat	k5eAaImNgFnS	přiřazovat
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
dvojdílné	dvojdílný	k2eAgFnSc2d1	dvojdílná
federální	federální	k2eAgFnSc2d1	federální
hymny	hymna	k1gFnSc2	hymna
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
federálních	federální	k2eAgInPc2d1	federální
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
komise	komise	k1gFnSc1	komise
expertů	expert	k1gMnPc2	expert
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Šebánkem	Šebánek	k1gMnSc7	Šebánek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
stanovit	stanovit	k5eAaPmF	stanovit
znak	znak	k1gInSc4	znak
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
České	český	k2eAgFnSc2d1	Česká
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
zavést	zavést	k5eAaPmF	zavést
bíločervenou	bíločervený	k2eAgFnSc4d1	bíločervená
bikolóru	bikolóra	k1gFnSc4	bikolóra
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
znaku	znak	k1gInSc3	znak
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
byly	být	k5eAaImAgInP	být
připraveny	připravit	k5eAaPmNgInP	připravit
až	až	k6eAd1	až
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Stranické	stranický	k2eAgInPc1d1	stranický
i	i	k8xC	i
státní	státní	k2eAgInPc1d1	státní
orgány	orgán	k1gInPc1	orgán
tehdy	tehdy	k6eAd1	tehdy
návrhy	návrh	k1gInPc1	návrh
odložily	odložit	k5eAaPmAgInP	odložit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
nikdy	nikdy	k6eAd1	nikdy
vlastní	vlastní	k2eAgInSc4d1	vlastní
znak	znak	k1gInSc4	znak
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
vydání	vydání	k1gNnSc4	vydání
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
a	a	k8xC	a
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
předložil	předložit	k5eAaPmAgMnS	předložit
až	až	k6eAd1	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
prezident	prezident	k1gMnSc1	prezident
ČSSR	ČSSR	kA	ČSSR
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1990	[number]	k4	1990
předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
přikázalo	přikázat	k5eAaPmAgNnS	přikázat
návrh	návrh	k1gInSc4	návrh
projednat	projednat	k5eAaPmF	projednat
ve	v	k7c6	v
výborech	výbor	k1gInPc6	výbor
ČNR	ČNR	kA	ČNR
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
komise	komise	k1gFnSc1	komise
předložila	předložit	k5eAaPmAgFnS	předložit
dva	dva	k4xCgInPc4	dva
návrhy	návrh	k1gInPc4	návrh
znaku	znak	k1gInSc2	znak
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgInS	být
J.	J.	kA	J.
Dolejš	Dolejš	k1gMnSc1	Dolejš
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
návrh	návrh	k1gInSc4	návrh
popisoval	popisovat	k5eAaImAgMnS	popisovat
i	i	k9	i
vlajku	vlajka	k1gFnSc4	vlajka
ČR	ČR	kA	ČR
jako	jako	k8xS	jako
bíločervenou	bíločervený	k2eAgFnSc4d1	bíločervená
bikolóru	bikolóra	k1gFnSc4	bikolóra
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
návrzích	návrh	k1gInPc6	návrh
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevovaly	objevovat	k5eAaImAgInP	objevovat
souběžně	souběžně	k6eAd1	souběžně
dva	dva	k4xCgInPc4	dva
znaky	znak	k1gInPc4	znak
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
malý	malý	k2eAgInSc1d1	malý
znak	znak	k1gInSc1	znak
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
i	i	k8xC	i
česká	český	k2eAgFnSc1d1	Česká
hymna	hymna	k1gFnSc1	hymna
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
první	první	k4xOgInSc1	první
částí	část	k1gFnPc2	část
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
československé	československý	k2eAgFnSc2d1	Československá
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
navázaly	navázat	k5eAaPmAgInP	navázat
na	na	k7c4	na
kontinuitu	kontinuita	k1gFnSc4	kontinuita
původních	původní	k2eAgInPc2d1	původní
zemských	zemský	k2eAgInPc2d1	zemský
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
tak	tak	k6eAd1	tak
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
zavedla	zavést	k5eAaPmAgFnS	zavést
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
stejně	stejně	k6eAd1	stejně
širokých	široký	k2eAgMnPc2d1	široký
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
spodního	spodní	k2eAgNnSc2d1	spodní
červeného	červené	k1gNnSc2	červené
a	a	k8xC	a
vrchního	vrchní	k2eAgMnSc2d1	vrchní
bílého	bílý	k1gMnSc2	bílý
<g/>
,	,	kIx,	,
při	při	k7c6	při
poměru	poměr	k1gInSc6	poměr
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
stanovil	stanovit	k5eAaPmAgInS	stanovit
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
vlajky	vlajka	k1gFnSc2	vlajka
lišila	lišit	k5eAaImAgFnS	lišit
pouze	pouze	k6eAd1	pouze
poměrem	poměr	k1gInSc7	poměr
stran	strana	k1gFnPc2	strana
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
sice	sice	k8xC	sice
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
volebních	volební	k2eAgFnPc2d1	volební
místností	místnost	k1gFnPc2	místnost
vyvěšena	vyvěsit	k5eAaPmNgFnS	vyvěsit
jak	jak	k6eAd1	jak
československá	československý	k2eAgFnSc1d1	Československá
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
česká	český	k2eAgFnSc1d1	Česká
respektive	respektive	k9	respektive
slovenská	slovenský	k2eAgFnSc1d1	slovenská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
použití	použití	k1gNnSc1	použití
české	český	k2eAgFnSc2d1	Česká
vlajky	vlajka	k1gFnSc2	vlajka
není	být	k5eNaImIp3nS	být
dokumentováno	dokumentován	k2eAgNnSc1d1	dokumentováno
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
slovenské	slovenský	k2eAgFnSc2d1	slovenská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
dvojbarevná	dvojbarevný	k2eAgFnSc1d1	dvojbarevná
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
a	a	k8xC	a
bílým	bílý	k2eAgInSc7d1	bílý
pruhem	pruh	k1gInSc7	pruh
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
podobná	podobný	k2eAgFnSc1d1	podobná
polské	polský	k2eAgFnPc1d1	polská
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
platnosti	platnost	k1gFnSc2	platnost
vůbec	vůbec	k9	vůbec
nevžila	vžít	k5eNaPmAgFnS	vžít
a	a	k8xC	a
mnohde	mnohde	k6eAd1	mnohde
nebyla	být	k5eNaImAgFnS	být
používána	používán	k2eAgFnSc1d1	používána
ani	ani	k8xC	ani
v	v	k7c6	v
povinných	povinný	k2eAgInPc6d1	povinný
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
Podobnost	podobnost	k1gFnSc1	podobnost
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
vlajkou	vlajka	k1gFnSc7	vlajka
budila	budit	k5eAaImAgFnS	budit
i	i	k9	i
jisté	jistý	k2eAgFnPc4d1	jistá
kontroverze	kontroverze	k1gFnPc4	kontroverze
mezi	mezi	k7c7	mezi
polskou	polský	k2eAgFnSc7d1	polská
menšinou	menšina	k1gFnSc7	menšina
v	v	k7c6	v
Zaolší	Zaolší	k1gNnSc6	Zaolší
<g/>
.	.	kIx.	.
<g/>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
50	[number]	k4	50
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
zavedla	zavést	k5eAaPmAgFnS	zavést
vlajku	vlajka	k1gFnSc4	vlajka
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
třemi	tři	k4xCgNnPc7	tři
stejně	stejně	k6eAd1	stejně
širokými	široký	k2eAgInPc7d1	široký
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
svrchní	svrchní	k2eAgMnSc1d1	svrchní
bílý	bílý	k1gMnSc1	bílý
<g/>
,	,	kIx,	,
prostřední	prostřední	k2eAgMnSc1d1	prostřední
modrý	modrý	k2eAgMnSc1d1	modrý
a	a	k8xC	a
spodní	spodní	k2eAgMnSc1d1	spodní
červený	červený	k2eAgMnSc1d1	červený
<g/>
,	,	kIx,	,
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	strana	k1gFnPc2	strana
rovněž	rovněž	k9	rovněž
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samostatná	samostatný	k2eAgFnSc1d1	samostatná
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
federativního	federativní	k2eAgNnSc2d1	federativní
Československa	Československo	k1gNnSc2	Československo
původní	původní	k2eAgFnSc4d1	původní
československou	československý	k2eAgFnSc4d1	Československá
vlajku	vlajka	k1gFnSc4	vlajka
převzala	převzít	k5eAaPmAgFnS	převzít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nástupnických	nástupnický	k2eAgInPc2d1	nástupnický
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
však	však	k9	však
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
dohodami	dohoda	k1gFnPc7	dohoda
mezi	mezi	k7c7	mezi
českou	český	k2eAgFnSc7d1	Česká
a	a	k8xC	a
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
politickou	politický	k2eAgFnSc7d1	politická
reprezentací	reprezentace	k1gFnSc7	reprezentace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyústily	vyústit	k5eAaPmAgFnP	vyústit
ve	v	k7c6	v
schválení	schválení	k1gNnSc6	schválení
znění	znění	k1gNnPc2	znění
čl	čl	kA	čl
<g/>
.	.	kIx.	.
3	[number]	k4	3
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
542	[number]	k4	542
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
ČSFR	ČSFR	kA	ČSFR
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
nesmějí	smát	k5eNaImIp3nP	smát
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
Republiky	republika	k1gFnSc2	republika
užívat	užívat	k5eAaImF	užívat
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
ustanovení	ustanovení	k1gNnSc2	ustanovení
posléze	posléze	k6eAd1	posléze
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
slovenský	slovenský	k2eAgMnSc1d1	slovenský
premiér	premiér	k1gMnSc1	premiér
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
nespecifikované	specifikovaný	k2eNgFnPc4d1	nespecifikovaná
kompenzace	kompenzace	k1gFnPc4	kompenzace
pro	pro	k7c4	pro
Slovensko	Slovensko	k1gNnSc4	Slovensko
za	za	k7c4	za
užívání	užívání	k1gNnSc4	užívání
československé	československý	k2eAgFnSc2d1	Československá
vlajky	vlajka	k1gFnSc2	vlajka
Českem	Česko	k1gNnSc7	Česko
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
čl	čl	kA	čl
<g/>
.	.	kIx.	.
112	[number]	k4	112
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ústava	ústava	k1gFnSc1	ústava
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
na	na	k7c4	na
výslovné	výslovný	k2eAgFnPc4d1	výslovná
výjimky	výjimka	k1gFnPc4	výjimka
se	s	k7c7	s
z	z	k7c2	z
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
ústavních	ústavní	k2eAgInPc2d1	ústavní
zákonů	zákon	k1gInPc2	zákon
stávají	stávat	k5eAaImIp3nP	stávat
běžné	běžný	k2eAgInPc1d1	běžný
zákony	zákon	k1gInPc1	zákon
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
ústavní	ústavní	k2eAgInPc1d1	ústavní
zákony	zákon	k1gInPc1	zákon
platné	platný	k2eAgInPc1d1	platný
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ke	k	k7c3	k
dni	den	k1gInSc3	den
účinnosti	účinnost	k1gFnSc2	účinnost
této	tento	k3xDgFnSc2	tento
Ústavy	ústava	k1gFnSc2	ústava
mají	mít	k5eAaImIp3nP	mít
sílu	síla	k1gFnSc4	síla
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
ČR	ČR	kA	ČR
následně	následně	k6eAd1	následně
byly	být	k5eAaImAgInP	být
upraveny	upravit	k5eAaPmNgInP	upravit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kolizních	kolizní	k2eAgNnPc2d1	kolizní
pravidel	pravidlo	k1gNnPc2	pravidlo
(	(	kIx(	(
<g/>
principu	princip	k1gInSc6	princip
"	"	kIx"	"
<g/>
lex	lex	k?	lex
posterior	posterior	k1gInSc1	posterior
derogat	derogat	k2eAgInSc1d1	derogat
priori	priori	k6eAd1	priori
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
uplatnil	uplatnit	k5eAaPmAgInS	uplatnit
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
legitimizaci	legitimizace	k1gFnSc4	legitimizace
barev	barva	k1gFnPc2	barva
byla	být	k5eAaImAgFnS	být
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
interpretována	interpretován	k2eAgFnSc1d1	interpretována
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
jako	jako	k9	jako
barva	barva	k1gFnSc1	barva
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pod	pod	k7c7	pod
modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
a	a	k8xC	a
výklad	výklad	k1gInSc1	výklad
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc4d1	původní
vlajku	vlajka	k1gFnSc4	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
dvoubarevnou	dvoubarevný	k2eAgFnSc4d1	dvoubarevná
<g/>
)	)	kIx)	)
zavedla	zavést	k5eAaPmAgFnS	zavést
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
67	[number]	k4	67
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
stejně	stejně	k6eAd1	stejně
širokých	široký	k2eAgInPc2d1	široký
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
spodního	spodní	k2eAgNnSc2d1	spodní
červeného	červené	k1gNnSc2	červené
a	a	k8xC	a
vrchního	vrchní	k2eAgMnSc2d1	vrchní
bílého	bílý	k1gMnSc2	bílý
<g/>
,	,	kIx,	,
při	při	k7c6	při
poměru	poměr	k1gInSc6	poměr
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
podobná	podobný	k2eAgFnSc1d1	podobná
polská	polský	k2eAgFnSc1d1	polská
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobu	podoba	k1gFnSc4	podoba
nynější	nynější	k2eAgFnSc2d1	nynější
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
č.	č.	k?	č.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
horního	horní	k2eAgInSc2d1	horní
pruhu	pruh	k1gInSc2	pruh
bílého	bílý	k2eAgInSc2d1	bílý
a	a	k8xC	a
dolního	dolní	k2eAgInSc2d1	dolní
pruhu	pruh	k1gInSc2	pruh
červeného	červené	k1gNnSc2	červené
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
je	být	k5eAaImIp3nS	být
vsunut	vsunut	k2eAgInSc1d1	vsunut
žerďový	žerďový	k2eAgInSc1d1	žerďový
modrý	modrý	k2eAgInSc1d1	modrý
klín	klín	k1gInSc1	klín
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
délky	délka	k1gFnSc2	délka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
šířky	šířka	k1gFnSc2	šířka
vlajky	vlajka	k1gFnSc2	vlajka
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
délce	délka	k1gFnSc3	délka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
však	však	k9	však
výslovně	výslovně	k6eAd1	výslovně
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
namísto	namísto	k7c2	namísto
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
použít	použít	k5eAaPmF	použít
její	její	k3xOp3gFnSc4	její
napodobeninu	napodobenina	k1gFnSc4	napodobenina
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
vzájemným	vzájemný	k2eAgInSc7d1	vzájemný
poměrem	poměr	k1gInSc7	poměr
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Odstíny	odstín	k1gInPc1	odstín
barev	barva	k1gFnPc2	barva
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgInP	dát
pouze	pouze	k6eAd1	pouze
otištěním	otištění	k1gNnSc7	otištění
barevného	barevný	k2eAgInSc2d1	barevný
vzoru	vzor	k1gInSc2	vzor
jako	jako	k8xC	jako
přílohy	příloha	k1gFnSc2	příloha
číslo	číslo	k1gNnSc1	číslo
3	[number]	k4	3
výše	vysoce	k6eAd2	vysoce
zmíněného	zmíněný	k2eAgInSc2d1	zmíněný
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
slovně	slovně	k6eAd1	slovně
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
specifikovány	specifikovat	k5eAaBmNgInP	specifikovat
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
polygrafickém	polygrafický	k2eAgInSc6d1	polygrafický
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
přímé	přímý	k2eAgFnPc1d1	přímá
barvy	barva	k1gFnPc1	barva
škály	škála	k1gFnSc2	škála
Pantone	Panton	k1gInSc5	Panton
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
modrá	modrat	k5eAaImIp3nS	modrat
293	[number]	k4	293
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
485	[number]	k4	485
<g/>
.	.	kIx.	.
<g/>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
výklad	výklad	k1gInSc1	výklad
významu	význam	k1gInSc2	význam
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
podoby	podoba	k1gFnSc2	podoba
vlajky	vlajka	k1gFnSc2	vlajka
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
zákonech	zákon	k1gInPc6	zákon
obsažen	obsažen	k2eAgInSc1d1	obsažen
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jakožto	jakožto	k8xS	jakožto
státní	státní	k2eAgFnPc1d1	státní
barvy	barva	k1gFnPc1	barva
také	také	k9	také
státním	státní	k2eAgInSc7d1	státní
symbolem	symbol	k1gInSc7	symbol
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
zákon	zákon	k1gInSc1	zákon
nestanoví	stanovit	k5eNaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgInPc7	jaký
způsoby	způsob	k1gInPc7	způsob
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Červenobílá	červenobílý	k2eAgFnSc1d1	červenobílá
bikolóra	bikolóra	k1gFnSc1	bikolóra
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgInSc7d1	tradiční
symbolem	symbol	k1gInSc7	symbol
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
klín	klín	k1gInSc1	klín
býval	bývat	k5eAaImAgInS	bývat
někdy	někdy	k6eAd1	někdy
vykládán	vykládat	k5eAaImNgInS	vykládat
jako	jako	k8xC	jako
symbol	symbol	k1gInSc1	symbol
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
po	po	k7c6	po
přisvojení	přisvojení	k1gNnSc6	přisvojení
vlajky	vlajka	k1gFnSc2	vlajka
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Používání	používání	k1gNnSc1	používání
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stanovil	stanovit	k5eAaPmAgInS	stanovit
pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
zmocňoval	zmocňovat	k5eAaImAgInS	zmocňovat
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
prováděcí	prováděcí	k2eAgFnSc2d1	prováděcí
vyhlášky	vyhláška	k1gFnSc2	vyhláška
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
kontinuálně	kontinuálně	k6eAd1	kontinuálně
platil	platit	k5eAaImAgInS	platit
jak	jak	k8xC	jak
pro	pro	k7c4	pro
původní	původní	k2eAgFnSc4d1	původní
vlajku	vlajka	k1gFnSc4	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
ji	on	k3xPp3gFnSc4	on
novelizoval	novelizovat	k5eAaBmAgInS	novelizovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
68	[number]	k4	68
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
článkem	článek	k1gInSc7	článek
V.	V.	kA	V.
</s>
</p>
<p>
<s>
Zásady	zásada	k1gFnPc1	zásada
a	a	k8xC	a
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
vlajky	vlajka	k1gFnSc2	vlajka
stanoví	stanovit	k5eAaPmIp3nS	stanovit
§	§	k?	§
7	[number]	k4	7
až	až	k9	až
9	[number]	k4	9
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
352	[number]	k4	352
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vodorovném	vodorovný	k2eAgNnSc6d1	vodorovné
věšení	věšení	k1gNnSc6	věšení
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
a	a	k8xC	a
modrý	modrý	k2eAgInSc1d1	modrý
klín	klín	k1gInSc1	klín
směřuje	směřovat	k5eAaImIp3nS	směřovat
vpravo	vpravo	k6eAd1	vpravo
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
modrý	modrý	k2eAgInSc1d1	modrý
klín	klín	k1gInSc1	klín
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
a	a	k8xC	a
špička	špička	k1gFnSc1	špička
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
věší	věšet	k5eAaImIp3nS	věšet
svisle	svisle	k6eAd1	svisle
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
vlevo	vlevo	k6eAd1	vlevo
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
a	a	k8xC	a
klín	klín	k1gInSc4	klín
směřuje	směřovat	k5eAaImIp3nS	směřovat
vždy	vždy	k6eAd1	vždy
dolů	dol	k1gInPc2	dol
(	(	kIx(	(
<g/>
t.	t.	k?	t.
j.	j.	k?	j.
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
definuje	definovat	k5eAaBmIp3nS	definovat
"	"	kIx"	"
<g/>
nejčestnější	čestný	k2eAgNnSc4d3	nejčestnější
místo	místo	k1gNnSc4	místo
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
vyvěšena	vyvěsit	k5eAaPmNgFnS	vyvěsit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
státními	státní	k2eAgFnPc7d1	státní
vlajkami	vlajka	k1gFnPc7	vlajka
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
anebo	anebo	k8xC	anebo
jinými	jiný	k2eAgFnPc7d1	jiná
vlajkami	vlajka	k1gFnPc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
užívat	užívat	k5eAaImF	užívat
jen	jen	k9	jen
vhodným	vhodný	k2eAgInSc7d1	vhodný
a	a	k8xC	a
důstojným	důstojný	k2eAgInSc7d1	důstojný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
stanoví	stanovit	k5eAaPmIp3nS	stanovit
i	i	k9	i
další	další	k2eAgFnPc4d1	další
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
používání	používání	k1gNnSc1	používání
vlajky	vlajka	k1gFnSc2	vlajka
k	k	k7c3	k
zahalování	zahalování	k1gNnSc3	zahalování
desek	deska	k1gFnPc2	deska
či	či	k8xC	či
pomníků	pomník	k1gInPc2	pomník
<g/>
,	,	kIx,	,
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
zdobení	zdobení	k1gNnSc1	zdobení
žerdi	žerď	k1gFnSc2	žerď
<g/>
,	,	kIx,	,
stanoví	stanovit	k5eAaPmIp3nS	stanovit
postup	postup	k1gInSc4	postup
vztyčování	vztyčování	k1gNnSc2	vztyčování
a	a	k8xC	a
snímání	snímání	k1gNnSc2	snímání
<g/>
,	,	kIx,	,
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
umisťování	umisťování	k1gNnSc1	umisťování
obrázků	obrázek	k1gInPc2	obrázek
či	či	k8xC	či
symbolů	symbol	k1gInPc2	symbol
do	do	k7c2	do
vlajky	vlajka	k1gFnSc2	vlajka
či	či	k8xC	či
svazování	svazování	k1gNnSc2	svazování
vlajky	vlajka	k1gFnSc2	vlajka
do	do	k7c2	do
růžice	růžice	k1gFnSc2	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
užití	užití	k1gNnSc6	užití
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
smutku	smutek	k1gInSc2	smutek
se	se	k3xPyFc4	se
vlajka	vlajka	k1gFnSc1	vlajka
vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
žerdě	žerď	k1gFnSc2	žerď
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
spustí	spustit	k5eAaPmIp3nS	spustit
na	na	k7c4	na
půl	půl	k1xP	půl
žerdi	žerď	k1gFnSc2	žerď
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
však	však	k9	však
dotýkat	dotýkat	k5eAaImF	dotýkat
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
územně-samosprávné	územněamosprávný	k2eAgInPc1d1	územně-samosprávný
celky	celek	k1gInPc1	celek
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
subjekty	subjekt	k1gInPc1	subjekt
vyjmenované	vyjmenovaný	k2eAgInPc1d1	vyjmenovaný
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
jsou	být	k5eAaImIp3nP	být
povinny	povinen	k2eAgFnPc1d1	povinna
vyvěšovat	vyvěšovat	k5eAaImF	vyvěšovat
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
"	"	kIx"	"
<g/>
vyvěšují	vyvěšovat	k5eAaImIp3nP	vyvěšovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnSc4	její
napodobeninu	napodobenina	k1gFnSc4	napodobenina
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
poměrem	poměr	k1gInSc7	poměr
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
sídlí	sídlet	k5eAaImIp3nS	sídlet
<g/>
,	,	kIx,	,
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
státních	státní	k2eAgInPc2d1	státní
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
při	při	k7c6	při
příležitostech	příležitost	k1gFnPc6	příležitost
celostátního	celostátní	k2eAgInSc2d1	celostátní
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c4	v
den	den	k1gInSc4	den
smutku	smutek	k1gInSc2	smutek
nebo	nebo	k8xC	nebo
v	v	k7c4	v
den	den	k1gInSc4	den
státního	státní	k2eAgInSc2d1	státní
smutku	smutek	k1gInSc2	smutek
<g/>
,	,	kIx,	,
vyhlášených	vyhlášený	k2eAgFnPc2d1	vyhlášená
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
novely	novela	k1gFnSc2	novela
č.	č.	k?	č.
213	[number]	k4	213
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
příležitosti	příležitost	k1gFnPc4	příležitost
k	k	k7c3	k
vyvěšování	vyvěšování	k1gNnSc3	vyvěšování
vlajky	vlajka	k1gFnSc2	vlajka
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
původního	původní	k2eAgNnSc2d1	původní
znění	znění	k1gNnSc2	znění
zákona	zákon	k1gInSc2	zákon
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zároveň	zároveň	k6eAd1	zároveň
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
ustanovení	ustanovení	k1gNnSc2	ustanovení
vypuštěny	vypuštěn	k2eAgFnPc4d1	vypuštěna
příležitosti	příležitost	k1gFnPc4	příležitost
místního	místní	k2eAgInSc2d1	místní
významu	význam	k1gInSc2	význam
vyhlášené	vyhlášený	k2eAgFnPc1d1	vyhlášená
krajem	kraj	k1gInSc7	kraj
nebo	nebo	k8xC	nebo
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
výzdoba	výzdoba	k1gFnSc1	výzdoba
zpravidla	zpravidla	k6eAd1	zpravidla
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c4	v
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
dne	den	k1gInSc2	den
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c4	v
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
sídel	sídlo	k1gNnPc2	sídlo
vyjmenovaných	vyjmenovaný	k2eAgInPc2d1	vyjmenovaný
státních	státní	k2eAgInPc2d1	státní
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
územně-samosprávných	územněamosprávný	k2eAgInPc2d1	územně-samosprávný
celků	celek	k1gInPc2	celek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
vyvěšena	vyvěsit	k5eAaPmNgFnS	vyvěsit
trvale	trvale	k6eAd1	trvale
(	(	kIx(	(
<g/>
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
oprávnění	oprávnění	k1gNnSc2	oprávnění
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
státní	státní	k2eAgInPc1d1	státní
fondy	fond	k1gInPc1	fond
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnPc1d1	státní
kulturní	kulturní	k2eAgFnPc1d1	kulturní
a	a	k8xC	a
vědecké	vědecký	k2eAgFnPc1d1	vědecká
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
povinnost	povinnost	k1gFnSc1	povinnost
příležitostného	příležitostný	k2eAgNnSc2d1	příležitostné
vyvěšování	vyvěšování	k1gNnSc2	vyvěšování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
vymezení	vymezení	k1gNnSc4	vymezení
oprávněných	oprávněný	k2eAgFnPc2d1	oprávněná
osob	osoba	k1gFnPc2	osoba
však	však	k9	však
pozbývá	pozbývat	k5eAaImIp3nS	pozbývat
smyslu	smysl	k1gInSc2	smysl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
konec	konec	k1gInSc4	konec
je	být	k5eAaImIp3nS	být
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
fyzické	fyzický	k2eAgFnPc1d1	fyzická
a	a	k8xC	a
právnické	právnický	k2eAgFnPc1d1	právnická
osoby	osoba	k1gFnPc1	osoba
a	a	k8xC	a
organizační	organizační	k2eAgFnPc1d1	organizační
složky	složka	k1gFnPc1	složka
státu	stát	k1gInSc2	stát
mohou	moct	k5eAaImIp3nP	moct
užít	užít	k5eAaPmF	užít
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
vhodným	vhodný	k2eAgInSc7d1	vhodný
a	a	k8xC	a
důstojným	důstojný	k2eAgInSc7d1	důstojný
způsobem	způsob	k1gInSc7	způsob
kdykoliv	kdykoliv	k6eAd1	kdykoliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
symboly	symbol	k1gInPc7	symbol
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Vexilologické	Vexilologický	k2eAgNnSc1d1	Vexilologický
názvosloví	názvosloví	k1gNnSc1	názvosloví
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vlajka	vlajka	k1gFnSc1	vlajka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Flags	Flags	k1gInSc1	Flags
of	of	k?	of
the	the	k?	the
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
National	National	k1gFnSc1	National
flag	flag	k1gInSc1	flag
of	of	k?	of
the	the	k?	the
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Pravidla	pravidlo	k1gNnPc4	pravidlo
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
a	a	k8xC	a
vyvěšování	vyvěšování	k1gNnSc4	vyvěšování
české	český	k2eAgFnSc2d1	Česká
vlajky	vlajka	k1gFnSc2	vlajka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
historie	historie	k1gFnSc1	historie
vlajky	vlajka	k1gFnSc2	vlajka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
</s>
</p>
