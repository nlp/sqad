<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1990	[number]	k4	1990
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
měla	mít	k5eAaImAgFnS	mít
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
dvojbarevnou	dvojbarevný	k2eAgFnSc4d1	dvojbarevná
bíločervenou	bíločervený	k2eAgFnSc4d1	bíločervená
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
užívána	užívat	k5eAaImNgFnS	užívat
jen	jen	k6eAd1	jen
velmi	velmi	k6eAd1	velmi
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
