<s>
Wilhelm	Wilhelm	k6eAd1
Weitling	Weitling	k1gInSc1
</s>
<s>
Wilhelm	Wilhelm	k6eAd1
Weitling	Weitling	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1808	#num#	k4
<g/>
Magdeburg	Magdeburg	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1871	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
62	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
New	New	k1gMnPc2
York	York	k1gInSc1
Povolání	povolání	k1gNnSc4
</s>
<s>
publicista	publicista	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Wilhelm	Wilhelm	k1gInSc1
Weitling	Weitling	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1808	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1871	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
raný	raný	k2eAgMnSc1d1
socialista	socialista	k1gMnSc1
německého	německý	k2eAgInSc2d1
původu	původ	k1gInSc2
a	a	k8xC
teoretik	teoretik	k1gMnSc1
komunismu	komunismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povoláním	povolání	k1gNnSc7
krejčí	krejčí	k1gMnSc1
emigroval	emigrovat	k5eAaBmAgMnS
nejdříve	dříve	k6eAd3
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgInS
do	do	k7c2
tajných	tajný	k2eAgInPc2d1
spolků	spolek	k1gInPc2
revolucionářů	revolucionář	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
zakládajícím	zakládající	k2eAgInSc7d1
členem	člen	k1gInSc7
Svazu	svaz	k1gInSc2
spravedlivých	spravedlivý	k2eAgMnPc2d1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
měl	mít	k5eAaImAgInS
změnit	změnit	k5eAaPmF
v	v	k7c6
první	první	k4xOgFnSc7
marxistickou	marxistický	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
Svaz	svaz	k1gInSc1
komunistů	komunista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Weitling	Weitling	k1gInSc1
nesouhlasil	souhlasit	k5eNaImAgInS
s	s	k7c7
humanistickým	humanistický	k2eAgInSc7d1
socialismem	socialismus	k1gInSc7
Saint-Simona	Saint-Simon	k1gMnSc2
či	či	k8xC
Fouriera	Fourier	k1gMnSc2
a	a	k8xC
propagoval	propagovat	k5eAaImAgMnS
revoluci	revoluce	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
by	by	kYmCp3nS
nastolila	nastolit	k5eAaPmAgFnS
vládu	vláda	k1gFnSc4
dělnické	dělnický	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Marxem	Marx	k1gMnSc7
se	se	k3xPyFc4
však	však	k9
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
do	do	k7c2
pojetí	pojetí	k1gNnSc2
revoluce	revoluce	k1gFnSc2
neshodl	shodnout	k5eNaPmAgInS
a	a	k8xC
byl	být	k5eAaImAgInS
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
přívrženci	přívrženec	k1gMnPc7
ze	z	k7c2
Svazu	svaz	k1gInSc2
komunistů	komunista	k1gMnPc2
vyloučen	vyloučit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
Weitling	Weitling	k1gInSc4
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
a	a	k8xC
zůstal	zůstat	k5eAaPmAgMnS
až	až	k6eAd1
do	do	k7c2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
několik	několik	k4yIc1
patentů	patent	k1gInPc2
za	za	k7c4
své	svůj	k3xOyFgInPc4
vynálezy	vynález	k1gInPc4
<g/>
,	,	kIx,
například	například	k6eAd1
za	za	k7c4
stroj	stroj	k1gInSc4
na	na	k7c4
šití	šití	k1gNnSc4
knoflíkových	knoflíkový	k2eAgFnPc2d1
dírek	dírka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Die	Die	k?
Menschheit	Menschheit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wie	Wie	k1gMnSc1
Sie	Sie	k1gMnSc1
ist	ist	k?
und	und	k?
wie	wie	k?
sie	sie	k?
sein	sein	k1gNnSc4
sollte	sollet	k5eAaPmRp2nP
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1838	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
<g/>
)	)	kIx)
Text	text	k1gInSc1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Das	Das	k?
Evangelium	evangelium	k1gNnSc1
eines	eines	k1gMnSc1
armen	armen	k2eAgInSc4d1
Sünders	Sünders	k1gInSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Garantien	Garantien	k2eAgInSc4d1
der	drát	k5eAaImRp2nS
Harmonie	harmonie	k1gFnSc2
und	und	k?
Freiheit	Freiheit	k1gMnSc1
<g/>
,	,	kIx,
1849	#num#	k4
</s>
<s>
Weitling	Weitling	k1gInSc1
připomíná	připomínat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zavedené	zavedený	k2eAgInPc4d1
stroje	stroj	k1gInPc4
nejenže	nejenže	k6eAd1
nezľahčili	zľahčit	k5eNaImAgMnP,k5eNaPmAgMnP
práci	práce	k1gFnSc4
dělníkům	dělník	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
zvýšily	zvýšit	k5eAaPmAgInP
zisky	zisk	k1gInPc1
bohatých	bohatý	k2eAgFnPc2d1
a	a	k8xC
připravili	připravit	k5eAaPmAgMnP
dělníků	dělník	k1gMnPc2
o	o	k7c4
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
nové	nový	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
by	by	kYmCp3nP
stroje	stroj	k1gInPc1
sloužily	sloužit	k5eAaImAgInP
skutečně	skutečně	k6eAd1
všem	všecek	k3xTgMnPc3
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc1
skutečný	skutečný	k2eAgInSc1d1
posláním	poslání	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Weitling	Weitling	k1gInSc1
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
pracují	pracovat	k5eAaImIp3nP
v	v	k7c6
potu	pot	k1gInSc6
tváře	tvář	k1gFnSc2
od	od	k7c2
rána	ráno	k1gNnSc2
do	do	k7c2
večera	večer	k1gInSc2
a	a	k8xC
hoc	hoc	k?
jsou	být	k5eAaImIp3nP
sýpky	sýpka	k1gFnPc1
bohatých	bohatý	k2eAgMnPc2d1
přeplněné	přeplněný	k2eAgFnSc6d1
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
planety	planeta	k1gFnSc2
postrádá	postrádat	k5eAaImIp3nS
i	i	k9
věci	věc	k1gFnPc1
nutné	nutný	k2eAgFnPc1d1
k	k	k7c3
obživě	obživa	k1gFnSc3
<g/>
,	,	kIx,
bydlení	bydlení	k1gNnSc3
a	a	k8xC
odívání	odívání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinu	příčina	k1gFnSc4
tohoto	tento	k3xDgInSc2
stavu	stav	k1gInSc2
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nerovném	rovný	k2eNgNnSc6d1
rozdělení	rozdělení	k1gNnSc6
práce	práce	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
plodů	plod	k1gInPc2
(	(	kIx(
<g/>
nerovném	rovný	k2eNgNnSc6d1
rozdělení	rozdělení	k1gNnSc6
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
statků	statek	k1gInPc2
a	a	k8xC
spotřeby	spotřeba	k1gFnSc2
statků	statek	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
příčinou	příčina	k1gFnSc7
chudoby	chudoba	k1gFnSc2
a	a	k8xC
bohatství	bohatství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
prostředkem	prostředek	k1gInSc7
tohoto	tento	k3xDgNnSc2
zla	zlo	k1gNnSc2
jsou	být	k5eAaImIp3nP
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Weitling	Weitling	k1gInSc1
volá	volat	k5eAaImIp3nS
po	po	k7c4
zdanění	zdanění	k1gNnSc4
bohatých	bohatý	k2eAgInPc2d1
<g/>
,	,	kIx,
kritizuje	kritizovat	k5eAaImIp3nS
lichvu	lichva	k1gFnSc4
<g/>
,	,	kIx,
nerovné	rovný	k2eNgNnSc4d1
rozdělení	rozdělení	k1gNnSc4
a	a	k8xC
spotřebu	spotřeba	k1gFnSc4
statků	statek	k1gInPc2
<g/>
,	,	kIx,
nerovnost	nerovnost	k1gFnSc1
<g/>
,	,	kIx,
zneužití	zneužití	k1gNnSc1
peněz	peníze	k1gInPc2
na	na	k7c6
obohacování	obohacování	k1gNnSc6
se	se	k3xPyFc4
na	na	k7c4
úkor	úkor	k1gInSc4
druhých	druhý	k4xOgFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projektuje	projektovat	k5eAaBmIp3nS
zřízení	zřízení	k1gNnSc4
národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
a	a	k8xC
národních	národní	k2eAgFnPc2d1
dílen	dílna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpovídá	předpovídat	k5eAaImIp3nS
sjednocení	sjednocení	k1gNnSc2
lidstva	lidstvo	k1gNnSc2
<g/>
,	,	kIx,
rovné	rovný	k2eAgNnSc4d1
rozdělování	rozdělování	k1gNnSc4
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
přístupnou	přístupný	k2eAgFnSc4d1
výchovu	výchova	k1gFnSc4
<g/>
,	,	kIx,
rovné	rovný	k2eAgFnPc4d1
práva	práv	k2eAgNnPc4d1
pohlaví	pohlaví	k1gNnPc4
<g/>
,	,	kIx,
odstranění	odstranění	k1gNnSc4
dědičných	dědičný	k2eAgFnPc2d1
výsad	výsada	k1gFnPc2
<g/>
,	,	kIx,
zavedení	zavedení	k1gNnSc1
všeobecných	všeobecný	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
<g/>
,	,	kIx,
stejné	stejný	k2eAgFnPc4d1
povinnosti	povinnost	k1gFnPc4
bez	bez	k7c2
privilegií	privilegium	k1gNnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Weitling	Weitling	k1gInSc1
připomíná	připomínat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
stroje	stroj	k1gInPc4
nejenže	nejenže	k6eAd1
nezľahčili	zľahčit	k5eNaImAgMnP,k5eNaPmAgMnP
práci	práce	k1gFnSc4
dělníkům	dělník	k1gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
zvýšily	zvýšit	k5eAaPmAgInP
zisky	zisk	k1gInPc1
bohatých	bohatý	k2eAgFnPc2d1
a	a	k8xC
připravili	připravit	k5eAaPmAgMnP
dělníků	dělník	k1gMnPc2
o	o	k7c4
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
nové	nový	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
by	by	kYmCp3nP
stroje	stroj	k1gInPc1
sloužily	sloužit	k5eAaImAgInP
skutečně	skutečně	k6eAd1
všem	všecek	k3xTgMnPc3
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc1
skutečný	skutečný	k2eAgInSc1d1
posláním	poslání	k1gNnSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
nejvyšší	vysoký	k2eAgFnSc4d3
vědu	věda	k1gFnSc4
považuje	považovat	k5eAaImIp3nS
filozofii	filozofie	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
zavést	zavést	k5eAaPmF
mezi	mezi	k7c4
všechny	všechen	k3xTgFnPc4
vědy	věda	k1gFnPc4
řád	řád	k1gInSc4
a	a	k8xC
harmonii	harmonie	k1gFnSc4
celku	celek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědy	věda	k1gFnPc1
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
filozofii	filozofie	k1gFnSc4
lékařství	lékařství	k1gNnSc2
<g/>
,	,	kIx,
fyziku	fyzika	k1gFnSc4
a	a	k8xC
mechaniku	mechanika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znalost	znalost	k1gFnSc4
těchto	tento	k3xDgFnPc2
tří	tři	k4xCgFnPc2
věd	věda	k1gFnPc2
a	a	k8xC
nadměrný	nadměrný	k2eAgInSc1d1
talent	talent	k1gInSc1
je	být	k5eAaImIp3nS
podmínkou	podmínka	k1gFnSc7
způsobilost	způsobilost	k1gFnSc1
vládnout	vládnout	k5eAaImF
v	v	k7c6
tzv.	tzv.	kA
triu	trio	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trio	trio	k1gNnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
řada	řada	k1gFnSc1
tří	tři	k4xCgFnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
stojí	stát	k5eAaImIp3nS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Weitlingovej	Weitlingovej	k1gFnSc2
ideální	ideální	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
géniů	génius	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
dosazování	dosazování	k1gNnSc4
na	na	k7c6
základě	základ	k1gInSc6
znalostí	znalost	k1gFnPc2
a	a	k8xC
talentu	talent	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
prostřednictvím	prostřednictvím	k7c2
konkurzů	konkurz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
řízení	řízení	k1gNnSc6
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
velkého	velký	k2eAgInSc2d1
rodinného	rodinný	k2eAgInSc2d1
celku	celek	k1gInSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
dále	daleko	k6eAd2
podílejí	podílet	k5eAaImIp3nP
<g/>
:	:	kIx,
ústřední	ústřední	k2eAgNnSc1d1
kolegium	kolegium	k1gNnSc1
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
akademie	akademie	k1gFnSc1
<g/>
,	,	kIx,
akademická	akademický	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
zdravotní	zdravotní	k2eAgFnSc1d1
komise	komise	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Weitling	Weitling	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
přes	přes	k7c4
100	#num#	k4
citátů	citát	k1gInPc2
z	z	k7c2
Bible	bible	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
vysvětluje	vysvětlovat	k5eAaImIp3nS
a	a	k8xC
argumentuje	argumentovat	k5eAaImIp3nS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
sociálního	sociální	k2eAgNnSc2d1
vnímání	vnímání	k1gNnSc2
křesťanství	křesťanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opírá	opírat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
o	o	k7c6
křesťanských	křesťanský	k2eAgMnPc2d1
utopistů	utopista	k1gMnPc2
jak	jak	k8xC,k8xS
byly	být	k5eAaImAgFnP
Lamennais	Lamennais	k1gFnSc1
či	či	k8xC
Münzer	Münzer	k1gInSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
dokázali	dokázat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
demokratické	demokratický	k2eAgInPc1d1
názory	názor	k1gInPc1
vyplynuly	vyplynout	k5eAaPmAgInP
z	z	k7c2
křesťanství	křesťanství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Citace	citace	k1gFnPc1
z	z	k7c2
díla	dílo	k1gNnSc2
</s>
<s>
Nehľaďme	Nehľadit	k5eAaPmRp1nP
stále	stále	k6eAd1
nahoru	nahoru	k6eAd1
<g/>
,	,	kIx,
na	na	k7c4
modrou	modrý	k2eAgFnSc4d1
oblohu	obloha	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
řeč	řeč	k1gFnSc1
o	o	k7c4
říši	říše	k1gFnSc4
Boží	boží	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tady	tady	k6eAd1
na	na	k7c6
zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vybudovat	vybudovat	k5eAaPmF
království	království	k1gNnSc4
nebeské	nebeský	k2eAgFnSc2d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
W.	W.	kA
171	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Každá	každý	k3xTgFnSc1
generace	generace	k1gFnSc1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
představu	představa	k1gFnSc4
dokonalosti	dokonalost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
může	moct	k5eAaImIp3nS
přiblížit	přiblížit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
ji	on	k3xPp3gFnSc4
nikdy	nikdy	k6eAd1
zcela	zcela	k6eAd1
nedosáhne	dosáhnout	k5eNaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
W.	W.	kA
24	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
pojetím	pojetí	k1gNnSc7
vlastnictví	vlastnictví	k1gNnSc2
podobá	podobat	k5eAaImIp3nS
trosečníkům	trosečník	k1gMnPc3
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
každý	každý	k3xTgInSc1
se	se	k3xPyFc4
cítí	cítit	k5eAaImIp3nS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podaří	podařit	k5eAaPmIp3nS
zachytit	zachytit	k5eAaPmF
ve	v	k7c6
víru	vír	k1gInSc6
o	o	k7c4
břevno	břevno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvlášť	zvlášť	k6eAd1
pokud	pokud	k8xS
vidí	vidět	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnPc1
sousedé	soused	k1gMnPc1
zoufale	zoufale	k6eAd1
brání	bránit	k5eAaImIp3nP
vlnám	vlna	k1gFnPc3
bídy	bída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
zdráhali	zdráhat	k5eAaImAgMnP
by	by	kYmCp3nP
se	se	k3xPyFc4
snad	snad	k9
všichni	všechen	k3xTgMnPc1
nastoupit	nastoupit	k5eAaPmF
na	na	k7c4
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
je	on	k3xPp3gMnPc4
přišla	přijít	k5eAaPmAgFnS
zachránit	zachránit	k5eAaPmF
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
W.	W.	kA
167	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
…	…	k?
<g/>
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
žádá	žádat	k5eAaImIp3nS
stejnou	stejný	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
křesťan	křesťan	k1gMnSc1
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
komunista	komunista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byl	být	k5eAaImAgMnS
také	také	k9
Ježíš	Ježíš	k1gMnSc1
komunista	komunista	k1gMnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
hlásal	hlásat	k5eAaImAgInS
princip	princip	k1gInSc1
společenství	společenství	k1gNnSc2
a	a	k8xC
nutnost	nutnost	k1gFnSc4
tohoto	tento	k3xDgInSc2
principu	princip	k1gInSc2
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gNnSc4
uskutečnění	uskutečnění	k1gNnSc4
a	a	k8xC
podobu	podoba	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
bude	být	k5eAaImBp3nS
uskutečněn	uskutečněn	k2eAgMnSc1d1
<g/>
,	,	kIx,
ponechal	ponechat	k5eAaPmAgMnS
budoucnosti	budoucnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možný	k2eAgFnSc1d1
s	s	k7c7
tím	ten	k3xDgInSc7
byly	být	k5eAaImAgInP
v	v	k7c4
esejskom	esejskom	k1gInSc4
spolku	spolek	k1gInSc2
srozuměni	srozuměn	k2eAgMnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
plán	plán	k1gInSc1
křesťanské	křesťanský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
tehdy	tehdy	k6eAd1
neexistoval	existovat	k5eNaImAgInS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
W.	W.	kA
171	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dokud	dokud	k8xS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
stát	stát	k5eAaImF,k5eAaPmF
vlastníkem	vlastník	k1gMnSc7
každý	každý	k3xTgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
vlastnictví	vlastnictví	k1gNnSc1
pro	pro	k7c4
společnost	společnost	k1gFnSc4
škodlivé	škodlivý	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
žilo	žít	k5eAaImAgNnS
tak	tak	k9
málo	málo	k4c1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
že	že	k8xS
neměli	mít	k5eNaImAgMnP
ani	ani	k8xC
představu	představa	k1gFnSc4
o	o	k7c6
rozloze	rozloha	k1gFnSc6
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
značně	značně	k6eAd1
rozmnožili	rozmnožit	k5eAaPmAgMnP
...	...	k?
půdy	půda	k1gFnPc4
ale	ale	k8xC
zůstává	zůstávat	k5eAaImIp3nS
stále	stále	k6eAd1
stejně	stejně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodí	hodit	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
rozdělení	rozdělení	k1gNnSc1
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
došlo	dojít	k5eAaPmAgNnS
před	před	k7c7
tisíci	tisíc	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
,	,	kIx,
i	i	k9
na	na	k7c4
dnešní	dnešní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Ne	Ne	kA
<g/>
,	,	kIx,
protože	protože	k8xS
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
miliony	milion	k4xCgInPc1
lidí	člověk	k1gMnPc2
nevlastní	vlastnit	k5eNaImIp3nP
nic	nic	k6eAd1
<g/>
,	,	kIx,
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
vlastnictví	vlastnictví	k1gNnSc1
majetku	majetek	k1gInSc2
bezprávím	bezpráví	k1gNnSc7
spáchaným	spáchaný	k2eAgNnSc7d1
na	na	k7c6
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
neodpustitelnou	odpustitelný	k2eNgFnSc4d1
a	a	k8xC
stydlivě	stydlivě	k6eAd1
krádeží	krádež	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
W.	W.	kA
55-6	55-6	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obchodníkovi	obchodník	k1gMnSc3
nic	nic	k3yNnSc1
nebrání	bránit	k5eNaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
svým	svůj	k3xOyFgMnPc3
zákazníkům	zákazník	k1gMnPc3
předložil	předložit	k5eAaPmAgInS
přehnaný	přehnaný	k2eAgInSc4d1
účet	účet	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
ať	ať	k9
se	se	k3xPyFc4
dělník	dělník	k1gMnSc1
nikdy	nikdy	k6eAd1
neodváží	odvážit	k5eNaPmIp3nS,k5eNaImIp3nS
žádat	žádat	k5eAaImF
zvýšení	zvýšení	k1gNnSc2
své	svůj	k3xOyFgFnSc2
mzdy	mzda	k1gFnSc2
...	...	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
"	"	kIx"
<g/>
W.	W.	kA
124	#num#	k4
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
PERNÝ	perný	k2eAgInSc1d1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evanjelium	Evanjelium	k?
podľa	podľus	k1gMnSc4
Wilhelma	Wilhelm	k1gMnSc4
Weitlinga	Weitling	k1gMnSc4
<g/>
,	,	kIx,
buriča	buričus	k1gMnSc4
<g/>
,	,	kIx,
proroka	prorok	k1gMnSc4
a	a	k8xC
vizionára	vizionár	k1gMnSc4
vedecky	vedecky	k6eAd1
riadenej	riadenat	k5eAaImRp2nS,k5eAaPmRp2nS
spoločnosti	spoločnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
DAV	Dav	k1gInSc1
DVA	dva	k4xCgInPc4
-	-	kIx~
kultúrno-politický	kultúrno-politický	k2eAgInSc1d1
magazín	magazín	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-11-22	2019-11-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
WEITLING	WEITLING	kA
<g/>
,	,	kIx,
W.	W.	kA
<g/>
:	:	kIx,
Lidstvo	lidstvo	k1gNnSc1
jaké	jaký	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1987	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Weitling	Weitling	k1gInSc1
<g/>
,	,	kIx,
W.	W.	kA
Lidstvo	lidstvo	k1gNnSc4
jaké	jaký	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Eduard	Eduard	k1gMnSc1
Fuchs	Fuchs	k1gMnSc1
(	(	kIx(
<g/>
Vorwort	Vorwort	k1gInSc1
<g/>
)	)	kIx)
in	in	k?
Sammlung	Sammlung	k1gInSc1
gesellschaftswissenschaftlicher	gesellschaftswissenschaftlichra	k1gFnPc2
Aufsätze	Aufsätze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrsg	Hrsg	k1gInSc1
<g/>
.	.	kIx.
von	von	k1gInSc1
Eduard	Eduard	k1gMnSc1
Fuchs	Fuchs	k1gMnSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
u.	u.	k?
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heft	Heft	k?
<g/>
:	:	kIx,
Das	Das	k1gMnSc1
Evangelium	evangelium	k1gNnSc4
eines	eines	k1gInSc4
armen	armen	k2eAgInSc4d1
Sünders	Sünders	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Von	von	k1gInSc1
Wilhelm	Wilhelm	k1gMnSc1
Weitling	Weitling	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zweiter	Zweiter	k1gMnSc1
Neudruck	Neudruck	k1gMnSc1
<g/>
,	,	kIx,
München	München	k2eAgMnSc1d1
1897	#num#	k4
</s>
<s>
Helmut	Helmut	k1gMnSc1
Asmus	Asmus	k1gMnSc1
<g/>
,	,	kIx,
in	in	k?
Magdeburger	Magdeburger	k1gInSc1
Biographisches	Biographisches	k1gMnSc1
Lexikon	lexikon	k1gInSc1
<g/>
,	,	kIx,
Magdeburg	Magdeburg	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3-933046-49-1	3-933046-49-1	k4
</s>
<s>
Schäfer	Schäfer	k1gMnSc1
<g/>
,	,	kIx,
Wolf	Wolf	k1gMnSc1
<g/>
:	:	kIx,
Die	Die	k1gMnSc5
unvertraute	unvertraut	k1gMnSc5
Moderne	Modern	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historische	Historische	k1gNnSc7
Umrisse	Umrisse	k1gFnSc2
einer	einer	k1gMnSc1
anderen	anderna	k1gFnPc2
Natur	Natur	k1gMnSc1
und	und	k?
Sozialgeschichte	Sozialgeschicht	k1gMnSc5
<g/>
,	,	kIx,
Frankfurt	Frankfurt	k1gInSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3-596-27356-0	3-596-27356-0	k4
</s>
<s>
Martin	Martin	k1gMnSc1
Hüttner	Hüttner	k1gMnSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Wilhelm	Wilhelm	k1gInSc1
Weitling	Weitling	k1gInSc1
als	als	k?
Frühsozialist	Frühsozialist	k1gInSc1
:	:	kIx,
Essay	Essaa	k1gFnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haag	Haag	k1gInSc1
und	und	k?
Herchen	Herchen	k1gInSc1
<g/>
,	,	kIx,
Frankfurt	Frankfurt	k1gInSc1
am	am	k?
Main	Main	k1gInSc1
1985	#num#	k4
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
94	#num#	k4
S.	S.	kA
ISBN	ISBN	kA
3-86137-127-8	3-86137-127-8	k4
</s>
<s>
Jürg	Jürg	k1gMnSc1
Haefelin	Haefelin	k2eAgMnSc1d1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Wilhelm	Wilhelm	k1gInSc1
Weitling	Weitling	k1gInSc1
:	:	kIx,
Biographie	Biographie	k1gFnSc1
u.	u.	k?
Theorie	theorie	k1gFnSc1
;	;	kIx,
d.	d.	k?
Zürcher	Zürchra	k1gFnPc2
Kommunistenprozess	Kommunistenprozess	k1gInSc4
von	von	k1gInSc1
1843	#num#	k4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bern	Bern	k1gInSc1
;	;	kIx,
Frankfurt	Frankfurt	k1gInSc1
am	am	k?
Main	Main	k1gInSc1
;	;	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
:	:	kIx,
Lang	Lang	k1gMnSc1
1986	#num#	k4
<g/>
.	.	kIx.
277	#num#	k4
S.	S.	kA
(	(	kIx(
<g/>
Zugl	Zugl	k1gMnSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Zürich	Zürich	k1gMnSc1
<g/>
,	,	kIx,
Univ	Univ	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Dissertation	Dissertation	k1gInSc1
1985	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
3-261-03583-8	3-261-03583-8	k4
</s>
<s>
Ellen	Ellen	k2eAgInSc1d1
Drünert	Drünert	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Die	Die	k1gFnSc1
religiös-ethische	religiös-ethische	k1gInSc4
Motivation	Motivation	k1gInSc1
des	des	k1gNnSc1
Kommunismus	Kommunismus	k1gInSc1
bei	bei	k?
Wilhelm	Wilhelm	k1gInSc1
Weitling	Weitling	k1gInSc1
:	:	kIx,
Versuch	Versuch	k1gInSc1
einer	einer	k1gInSc1
Analyse	analysa	k1gFnSc3
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bonn	Bonn	k1gInSc1
1979	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diss	Dissa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erziehungswiss	Erziehungswissa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PHR	PHR	kA
Bonn	Bonn	k1gInSc1
</s>
<s>
Wolfram	wolfram	k1gInSc1
von	von	k1gInSc1
Moritz	moritz	k1gInSc1
<g/>
:	:	kIx,
Wilhelm	Wilhelm	k1gInSc1
Weitling	Weitling	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Religiöse	Religiöse	k1gFnSc1
Problematik	problematika	k1gFnPc2
und	und	k?
literarische	literarischat	k5eAaPmIp3nS
Form.	Form.	k1gFnSc1
-	-	kIx~
Bern	Bern	k1gInSc1
<g/>
,	,	kIx,
<g/>
Frankfurt	Frankfurt	k1gInSc1
am	am	k?
Main	Main	k1gInSc1
<g/>
,	,	kIx,
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Dissertation	Dissertation	k1gInSc1
Germ.	Germ.	k1gFnSc2
<g/>
/	/	kIx~
<g/>
Theol	Theol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uni	Uni	k1gMnSc1
Wuppertal	Wuppertal	k1gMnSc1
<g/>
)	)	kIx)
ISBN	ISBN	kA
3-8204-7067-0	3-8204-7067-0	k4
</s>
<s>
Waltraud	Waltraud	k1gMnSc1
Seidel-Höppner	Seidel-Höppner	k1gMnSc1
<g/>
:	:	kIx,
Wilhelm	Wilhelm	k1gMnSc1
Weitling	Weitling	k1gInSc4
<g/>
,	,	kIx,
der	drát	k5eAaImRp2nS
erste	erste	k5eAaPmIp2nP
deutsche	deutsche	k1gNnSc7
Theoretiker	Theoretiker	k1gInSc1
und	und	k?
Agitator	Agitator	k1gInSc1
des	des	k1gNnSc1
Kommunismus	Kommunismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
1961	#num#	k4
(	(	kIx(
<g/>
Inst	Insta	k1gFnPc2
<g/>
.	.	kIx.
f.	f.	k?
Gesellschaftswiss	Gesellschaftswiss	k1gInSc1
<g/>
.	.	kIx.
beim	beim	k1gInSc1
ZK	ZK	kA
d.	d.	k?
SED	sed	k1gInSc1
<g/>
,	,	kIx,
Diss	Diss	k1gInSc1
<g/>
.	.	kIx.
v.	v.	k?
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
März	März	k1gInSc1
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Perný	perný	k2eAgMnSc1d1
<g/>
:	:	kIx,
Evanjelium	Evanjelium	k?
podľa	podľus	k1gMnSc4
Wilhelma	Wilhelm	k1gMnSc4
Weitlinga	Weitling	k1gMnSc4
<g/>
,	,	kIx,
buriča	buričus	k1gMnSc4
<g/>
,	,	kIx,
proroka	prorok	k1gMnSc4
a	a	k8xC
vizionára	vizionár	k1gMnSc4
vedecky	vedecky	k6eAd1
riadenej	riadenat	k5eAaPmRp2nS,k5eAaImRp2nS
spoločnosti	spoločnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
DAV	Dav	k1gInSc1
DVA	dva	k4xCgInPc4
-	-	kIx~
kultúrno-politický	kultúrno-politický	k2eAgInSc1d1
magazín	magazín	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000701950	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118630652	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0857	#num#	k4
1375	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85339455	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
45095616	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85339455	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
<s>
Autor	autor	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Weitling	Weitling	k1gInSc4
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Osoba	osoba	k1gFnSc1
Wilhelm	Wilhelm	k1gMnSc1
Weitling	Weitling	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
