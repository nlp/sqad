<s>
Hron	Hron	k1gMnSc1
</s>
<s>
Hron	Hron	k1gMnSc1
Řeka	Řek	k1gMnSc2
v	v	k7c6
Banské	banský	k2eAgFnSc6d1
BystriciZákladní	BystriciZákladný	k2eAgMnPc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
298	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
5454,56	5454,56	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
53,7	53,7	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Pramen	pramen	k1gInSc1
</s>
<s>
Nízké	nízký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
31,13	31,13	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
55,82	55,82	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
do	do	k7c2
Dunaje	Dunaj	k1gInSc2
47	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
4,44	4,44	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
30,12	30,12	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
Banskobystrický	banskobystrický	k2eAgInSc1d1
<g/>
,	,	kIx,
Nitranský	nitranský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Černé	Černé	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Dunaj	Dunaj	k1gInSc1
</s>
<s>
Povodí	povodí	k1gNnSc1
Hronu	Hron	k1gInSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
slovenské	slovenský	k2eAgFnSc6d1
řece	řeka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Hron	Hron	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hron	Hron	k1gMnSc1
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
označení	označení	k1gNnSc2
Granus	Granus	k1gMnSc1
–	–	k?
keltský	keltský	k2eAgMnSc1d1
bůh	bůh	k1gMnSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
Gran	Gran	k1gInSc1
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Garam	Garam	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
v	v	k7c6
Banskobystrickém	banskobystrický	k2eAgInSc6d1
a	a	k8xC
Nitranském	nitranský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
nejdelší	dlouhý	k2eAgFnSc1d3
v	v	k7c6
zemi	zem	k1gFnSc6
a	a	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
levostranný	levostranný	k2eAgInSc4d1
přítok	přítok	k1gInSc4
Dunaje	Dunaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
délka	délka	k1gFnSc1
činí	činit	k5eAaImIp3nS
298	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
5454,56	5454,56	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Tok	tok	k1gInSc1
řeky	řeka	k1gFnSc2
</s>
<s>
Pramen	pramen	k1gInSc1
</s>
<s>
Hron	Hron	k1gMnSc1
pramení	pramenit	k5eAaImIp3nS
v	v	k7c6
geomorfologickém	geomorfologický	k2eAgInSc6d1
celku	celek	k1gInSc6
Horehronské	horehronský	k2eAgFnSc2d1
podolí	podolí	k?
na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
Nízkých	nízký	k2eAgFnPc2d1
Tater	Tatra	k1gFnPc2
a	a	k8xC
Spišsko-gemerského	Spišsko-gemerský	k2eAgInSc2d1
krasu	kras	k1gInSc2
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Telgárt	Telgárta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pramen	pramen	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
jihovýchodním	jihovýchodní	k2eAgNnSc6d1
úpatí	úpatí	k1gNnSc6
Kráľovy	Kráľův	k2eAgFnSc2d1
hole	hole	k1gFnSc2
<g/>
,	,	kIx,
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
sedla	sedlo	k1gNnSc2
Besník	Besník	k1gInSc1
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
zhruba	zhruba	k6eAd1
980	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Tok	tok	k1gInSc1
</s>
<s>
Na	na	k7c6
horním	horní	k2eAgInSc6d1
a	a	k8xC
středním	střední	k2eAgInSc6d1
toku	tok	k1gInSc6
protéká	protékat	k5eAaImIp3nS
soutěskami	soutěska	k1gFnPc7
a	a	k8xC
mezihorskými	mezihorský	k2eAgFnPc7d1
kotlinami	kotlina	k1gFnPc7
Západních	západní	k2eAgInPc2d1
Karpat	Karpaty	k1gInPc2
a	a	k8xC
na	na	k7c6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
Podunajskou	podunajský	k2eAgFnSc7d1
nížinou	nížina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgInPc7d1
přítoky	přítok	k1gInPc7
jsou	být	k5eAaImIp3nP
Čierny	Čierna	k1gFnSc2
Hron	Hron	k1gInSc1
<g/>
,	,	kIx,
Slatina	slatina	k1gFnSc1
<g/>
,	,	kIx,
Sikenica	Sikenica	k1gFnSc1
a	a	k8xC
Paríž	Paríž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
činí	činit	k5eAaImIp3nS
53,7	53,7	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s.	s.	k?
Nejvyšší	vysoký	k2eAgInSc1d3
je	být	k5eAaImIp3nS
na	na	k7c4
jaře	jař	k1gFnPc4
a	a	k8xC
nejmenší	malý	k2eAgFnPc4d3
v	v	k7c6
létě	léto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
k	k	k7c3
vodáctví	vodáctví	k1gNnSc3
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
výrobě	výroba	k1gFnSc3
vodní	vodní	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodní	vodní	k2eAgFnSc1d1
turistika	turistika	k1gFnSc1
je	být	k5eAaImIp3nS
po	po	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
rozvinutá	rozvinutý	k2eAgFnSc1d1
především	především	k9
v	v	k7c6
úseku	úsek	k1gInSc6
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
-	-	kIx~
Zvolen	Zvolen	k1gInSc1
a	a	k8xC
část	část	k1gFnSc1
posádek	posádka	k1gFnPc2
pokračuje	pokračovat	k5eAaImIp3nS
až	až	k9
k	k	k7c3
soutoku	soutok	k1gInSc3
s	s	k7c7
Dunajem	Dunaj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodou	voda	k1gFnSc7
zásobuje	zásobovat	k5eAaImIp3nS
jadernou	jaderný	k2eAgFnSc4d1
elektrárnu	elektrárna	k1gFnSc4
Mochovce	Mochovka	k1gFnSc3
</s>
<s>
Osídlení	osídlení	k1gNnSc1
</s>
<s>
Protéká	protékat	k5eAaImIp3nS
těmito	tento	k3xDgFnPc7
městy	město	k1gNnPc7
<g/>
:	:	kIx,
Brezno	Brezna	k1gFnSc5
<g/>
,	,	kIx,
Podbrezová	Podbrezová	k1gFnSc5
<g/>
,	,	kIx,
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
<g/>
,	,	kIx,
Zvolen	Zvolen	k1gInSc1
<g/>
,	,	kIx,
Žiar	Žiar	k1gInSc1
nad	nad	k7c4
Hronom	Hronom	k1gInSc4
<g/>
,	,	kIx,
Žarnovica	Žarnovica	k1gFnSc1
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
Baňa	baňa	k1gFnSc1
<g/>
,	,	kIx,
Želiezovce	Želiezovec	k1gInPc1
<g/>
,	,	kIx,
Čata	Čata	k1gFnSc1
<g/>
,	,	kIx,
Štúrovo	Štúrův	k2eAgNnSc1d1
(	(	kIx(
<g/>
zde	zde	k6eAd1
se	se	k3xPyFc4
vlévá	vlévat	k5eAaImIp3nS
do	do	k7c2
Dunaje	Dunaj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Návod	návod	k1gInSc1
na	na	k7c4
splutí	splutí	k1gNnSc4
Hronu	Hron	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hron	Hron	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Hron	Hron	k1gInSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
vodních	vodní	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
na	na	k7c6
Hronu	Hron	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Čejka	čejka	k1gFnSc1
T.	T.	kA
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
Mäkkýše	Mäkkýš	k1gInSc2
hlavných	hlavný	k2eAgNnPc2d1
typov	typovo	k1gNnPc2
vôd	vôd	k?
dolného	dolný	k1gMnSc2
Hrona	Hron	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malacologica	Malacologica	k1gFnSc1
Bohemoslovaca	Bohemoslovaca	k1gFnSc1
5	#num#	k4
<g/>
:	:	kIx,
33	#num#	k4
<g/>
-	-	kIx~
<g/>
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
501765	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4213892-9	4213892-9	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
236424256	#num#	k4
</s>
