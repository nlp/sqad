<s>
Monero	Monero	k1gNnSc1
</s>
<s>
Monero	Monero	k1gNnSc1
Logo	logo	k1gNnSc1
MoneraZemě	MoneraZema	k1gFnSc6
</s>
<s>
mezinárodní	mezinárodní	k2eAgFnSc1d1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
XMR	XMR	kA
(	(	kIx(
<g/>
neoficiální	neoficiální	k2eAgFnSc1d1,k2eNgFnSc1d1
<g/>
)	)	kIx)
Inflace	inflace	k1gFnSc1
</s>
<s>
5	#num#	k4
XMR	XMR	kA
~	~	kIx~
2	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
ɱ	ɱ	k?
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
XMR	XMR	kA
<g/>
.	.	kIx.
</s>
<s>
Monero	Monero	k1gNnSc1
(	(	kIx(
<g/>
XMR	XMR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
open-source	open-sourka	k1gFnSc3
kryptoměna	kryptoměn	k2eAgFnSc1d1
vytvořená	vytvořený	k2eAgFnSc1d1
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
devizou	deviza	k1gFnSc7
je	být	k5eAaImIp3nS
anonymita	anonymita	k1gFnSc1
<g/>
,	,	kIx,
decentralizace	decentralizace	k1gFnSc1
<g/>
,	,	kIx,
škálovatelnost	škálovatelnost	k1gFnSc1
a	a	k8xC
nízké	nízký	k2eAgInPc1d1
poplatky	poplatek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
mnoha	mnoho	k4c2
kryptoměn	kryptoměn	k2eAgInSc1d1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
deriváty	derivát	k1gInPc4
Bitcoinu	Bitcoin	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Monero	Monero	k1gNnSc1
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
protokolu	protokol	k1gInSc6
CryptoNote	CryptoNot	k1gInSc5
a	a	k8xC
má	mít	k5eAaImIp3nS
významné	významný	k2eAgInPc4d1
algoritmické	algoritmický	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
k	k	k7c3
blockchain	blockchain	k1gMnSc1
mlžení	mlžený	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Monero	Monero	k1gNnSc1
má	mít	k5eAaImIp3nS
trvalou	trvalý	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
od	od	k7c2
komunity	komunita	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
modulární	modulární	k2eAgInSc4d1
kód	kód	k1gInSc4
architektury	architektura	k1gFnSc2
byl	být	k5eAaImAgMnS
vychvalován	vychvalován	k2eAgMnSc1d1
Vladimir	Vladimir	k1gMnSc1
J.	J.	kA
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Laanem	Laan	k1gMnSc7
<g/>
,	,	kIx,
Bitcoin	Bitcoin	k1gMnSc1
Core	Core	k1gNnSc2
správcem	správce	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zpočátku	zpočátku	k6eAd1
se	se	k3xPyFc4
setkalo	setkat	k5eAaPmAgNnS
s	s	k7c7
malou	malý	k2eAgFnSc7d1
oblíbeností	oblíbenost	k1gFnSc7
u	u	k7c2
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
ale	ale	k8xC
Monero	Monero	k1gNnSc4
potkal	potkat	k5eAaPmAgInS
raketový	raketový	k2eAgInSc1d1
vzestup	vzestup	k1gInSc1
v	v	k7c6
tržní	tržní	k2eAgFnSc6d1
kapitalizaci	kapitalizace	k1gFnSc6
(	(	kIx(
<g/>
z	z	k7c2
5M	5M	k4
USD	USD	kA
na	na	k7c4
185M	185M	k4
USD	USD	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
objemu	objem	k1gInSc2
transakcí	transakce	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
částečně	částečně	k6eAd1
vzhledem	vzhledem	k7c3
k	k	k7c3
adoptování	adoptování	k1gNnSc3
měny	měna	k1gFnSc2
na	na	k7c4
darknet	darknet	k1gInSc4
trhu	trh	k1gInSc2
AlphaBay	AlphaBaa	k1gFnSc2
na	na	k7c6
konci	konec	k1gInSc6
léta	léto	k1gNnSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
Bitcoinu	Bitcoin	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
vstoupila	vstoupit	k5eAaPmAgFnS
na	na	k7c4
trh	trh	k1gInSc4
konkurenční	konkurenční	k2eAgInSc4d1
kryptoměna	kryptoměn	k2eAgFnSc1d1
Bytecoin	Bytecoin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bytecoin	Bytecoin	k1gInSc1
představoval	představovat	k5eAaImAgInS
první	první	k4xOgFnSc4
skutečnou	skutečný	k2eAgFnSc4d1
implementaci	implementace	k1gFnSc4
CryptoNote	CryptoNot	k1gInSc5
<g/>
,	,	kIx,
protokolu	protokol	k1gInSc3
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
postavena	postaven	k2eAgFnSc1d1
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
decentralizovaných	decentralizovaný	k2eAgInPc2d1
kryptoměn	kryptoměn	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problém	problém	k1gInSc4
této	tento	k3xDgFnSc2
kryptoměny	kryptoměn	k2eAgFnPc1d1
byl	být	k5eAaImAgMnS
však	však	k9
ten	ten	k3xDgMnSc1
<g/>
,	,	kIx,
že	že	k8xS
80	#num#	k4
%	%	kIx~
všech	všecek	k3xTgFnPc2
mincí	mince	k1gFnPc2
bylo	být	k5eAaImAgNnS
vytěženo	vytěžit	k5eAaPmNgNnS
předem	předem	k6eAd1
jejími	její	k3xOp3gMnPc7
autory	autor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šance	šance	k1gFnSc1
k	k	k7c3
těžbě	těžba	k1gFnSc3
tedy	tedy	k9
nebyly	být	k5eNaImAgFnP
vyrovnané	vyrovnaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
této	tento	k3xDgFnSc2
kryptoměny	kryptoměn	k2eAgFnPc4d1
vznikne	vzniknout	k5eAaPmIp3nS
nová	nový	k2eAgNnPc1d1
<g/>
,	,	kIx,
nepředtěžená	předtěžený	k2eNgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
spatřila	spatřit	k5eAaPmAgFnS
světlo	světlo	k1gNnSc4
světa	svět	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2014	#num#	k4
a	a	k8xC
jmenovala	jmenovat	k5eAaBmAgFnS,k5eAaImAgFnS
se	se	k3xPyFc4
BitMonero	BitMonero	k1gNnSc4
<g/>
,	,	kIx,
složenina	složenina	k1gFnSc1
ze	z	k7c2
slova	slovo	k1gNnSc2
mince	mince	k1gFnSc2
v	v	k7c6
esperantu	esperanto	k1gNnSc6
(	(	kIx(
<g/>
Monero	Monero	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
Bit	bit	k1gInSc1
podle	podle	k7c2
Bitcoinu	Bitcoin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pěti	pět	k4xCc6
dnech	den	k1gInPc6
ale	ale	k8xC
komunita	komunita	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
používat	používat	k5eAaImF
jen	jen	k9
zkráceně	zkráceně	k6eAd1
Monero	Monero	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
fork	fork	k1gInSc1
digitální	digitální	k2eAgFnSc2d1
měny	měna	k1gFnSc2
založené	založený	k2eAgFnSc2d1
na	na	k7c6
protokolu	protokol	k1gInSc6
CryptoNote	CryptoNot	k1gInSc5
-	-	kIx~
Bytecoin	Bytecoin	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
s	s	k7c7
dvěma	dva	k4xCgFnPc7
změnami	změna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Block	Block	k1gInSc1
time	time	k6eAd1
byl	být	k5eAaImAgInS
snížen	snížit	k5eAaPmNgInS
ze	z	k7c2
120	#num#	k4
sekund	sekunda	k1gFnPc2
na	na	k7c4
60	#num#	k4
a	a	k8xC
emise	emise	k1gFnSc1
byla	být	k5eAaImAgFnS
zpomalena	zpomalit	k5eAaPmNgFnS
o	o	k7c4
50	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Monero	Monero	k1gNnSc1
vrátilo	vrátit	k5eAaPmAgNnS
ke	k	k7c3
120	#num#	k4
sekundovému	sekundový	k2eAgInSc3d1
bloku	blok	k1gInSc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
x	x	k?
rychlejší	rychlý	k2eAgMnSc1d2
než	než	k8xS
Bitcoin	Bitcoin	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
se	s	k7c7
zachováním	zachování	k1gNnSc7
původní	původní	k2eAgFnSc2d1
snížené	snížený	k2eAgFnSc2d1
emise	emise	k1gFnSc2
zdvojnásobením	zdvojnásobení	k1gNnSc7
odměny	odměna	k1gFnSc2
za	za	k7c4
blok	blok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Několik	několik	k4yIc1
týdnů	týden	k1gInPc2
po	po	k7c6
spuštění	spuštění	k1gNnSc6
byl	být	k5eAaImAgMnS
vyvinutý	vyvinutý	k2eAgMnSc1d1
optimalizovaný	optimalizovaný	k2eAgMnSc1d1
GNU	gnu	k1gMnSc1
miner	miner	k1gMnSc1
pro	pro	k7c4
CryptoNight	CryptoNight	k1gInSc4
PoW	PoW	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
kryptoměnou	kryptoměný	k2eAgFnSc4d1
Monero	Monero	k1gNnSc4
stojí	stát	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
sedmi	sedm	k4xCc2
vývojářů	vývojář	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
v	v	k7c6
anonymitě	anonymita	k1gFnSc6
není	být	k5eNaImIp3nS
pouze	pouze	k6eAd1
David	David	k1gMnSc1
Latapie	Latapie	k1gFnSc2
a	a	k8xC
Riccarda	Riccard	k1gMnSc2
Spagni	Spageň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
uvedení	uvedení	k1gNnSc6
na	na	k7c4
trh	trh	k1gInSc4
dosáhla	dosáhnout	k5eAaPmAgFnS
měna	měna	k1gFnSc1
hodnoty	hodnota	k1gFnSc2
až	až	k9
několika	několik	k4yIc2
USD	USD	kA
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
rychlém	rychlý	k2eAgInSc6d1
úpadku	úpadek	k1gInSc6
se	se	k3xPyFc4
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
a	a	k8xC
půl	půl	k6eAd1
její	její	k3xOp3gFnSc1
hodnota	hodnota	k1gFnSc1
potácela	potácet	k5eAaImAgFnS
mezi	mezi	k7c7
0,2	0,2	k4
a	a	k8xC
1	#num#	k4
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s>
Zlom	zlom	k1gInSc1
nastal	nastat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
začal	začít	k5eAaPmAgInS
kurz	kurz	k1gInSc1
výrazně	výrazně	k6eAd1
růst	růst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohl	moct	k5eAaImAgMnS
za	za	k7c4
to	ten	k3xDgNnSc4
server	server	k1gInSc1
AlphaBay	AlphaBaa	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
obchoduje	obchodovat	k5eAaImIp3nS
s	s	k7c7
kradeným	kradený	k2eAgNnSc7d1
a	a	k8xC
nelegálním	legální	k2eNgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
server	server	k1gInSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
začal	začít	k5eAaPmAgInS
Monero	Monero	k1gNnSc4
přijímat	přijímat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
se	se	k3xPyFc4
hodnota	hodnota	k1gFnSc1
Monero	Monero	k1gNnSc4
znásobila	znásobit	k5eAaPmAgFnS
téměř	téměř	k6eAd1
30	#num#	k4
<g/>
x	x	k?
(	(	kIx(
<g/>
o	o	k7c4
2760	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Růst	růst	k1gInSc1
měny	měna	k1gFnSc2
pokračoval	pokračovat	k5eAaImAgInS
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
nastal	nastat	k5eAaPmAgInS
další	další	k2eAgInSc1d1
zlom	zlom	k1gInSc1
<g/>
,	,	kIx,
opačný	opačný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
se	se	k3xPyFc4
přiznala	přiznat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
systému	systém	k1gInSc6
byla	být	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
chyba	chyba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
umožnila	umožnit	k5eAaPmAgFnS
vytvářet	vytvářet	k5eAaImF
neomezený	omezený	k2eNgInSc4d1
počet	počet	k1gInSc4
mincí	mince	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trh	trh	k1gInSc1
reagoval	reagovat	k5eAaBmAgInS
poklesem	pokles	k1gInSc7
z	z	k7c2
60	#num#	k4
USD	USD	kA
na	na	k7c4
30	#num#	k4
USD	USD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kurz	Kurz	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
vrátil	vrátit	k5eAaPmAgInS
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
na	na	k7c4
původní	původní	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
a	a	k8xC
nadále	nadále	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
v	v	k7c6
růstu	růst	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
byla	být	k5eAaImAgFnS
vylepšena	vylepšen	k2eAgFnSc1d1
anonymita	anonymita	k1gFnSc1
transakcí	transakce	k1gFnPc2
dobrovolným	dobrovolný	k2eAgNnSc7d1
využitím	využití	k1gNnSc7
funkce	funkce	k1gFnSc2
Ring	ring	k1gInSc1
Confidential	Confidential	k1gMnSc1
Transactions	Transactions	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnPc4
pomáhá	pomáhat	k5eAaImIp3nS
zamaskovat	zamaskovat	k5eAaPmF
počet	počet	k1gInSc4
převedených	převedený	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
před	před	k7c7
kýmkoliv	kdokoliv	k3yInSc7
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
na	na	k7c6
převodu	převod	k1gInSc6
přímo	přímo	k6eAd1
nepodílel	podílet	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
problém	problém	k1gInSc1
s	s	k7c7
nelegálním	legální	k2eNgNnSc7d1
těžením	těžení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
administrátoři	administrátor	k1gMnPc1
stránek	stránka	k1gFnPc2
si	se	k3xPyFc3
na	na	k7c4
web	web	k1gInSc4
vložili	vložit	k5eAaPmAgMnP
skript	skript	k1gInSc4
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
neprávem	neprávo	k1gNnSc7
využívali	využívat	k5eAaPmAgMnP,k5eAaImAgMnP
počítače	počítač	k1gInPc4
svých	svůj	k3xOyFgMnPc2
návštěvníků	návštěvník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
jim	on	k3xPp3gMnPc3
nevědomky	nevědomky	k6eAd1
těžili	těžit	k5eAaImAgMnP
Monero	Monero	k1gNnSc4
bez	bez	k7c2
nároku	nárok	k1gInSc2
na	na	k7c4
jakoukoliv	jakýkoliv	k3yIgFnSc4
odměnu	odměna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
Monero	Monero	k1gNnSc1
</s>
<s>
Monero	Monero	k1gNnSc1
(	(	kIx(
<g/>
XMR	XMR	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kryptoměna	kryptomět	k5eAaPmNgFnS
s	s	k7c7
veřejně	veřejně	k6eAd1
dostupným	dostupný	k2eAgInSc7d1
open	open	k1gInSc4
source	sourec	k1gInSc2
zdrojovým	zdrojový	k2eAgInSc7d1
kódem	kód	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc1
centrální	centrální	k2eAgInSc1d1
server	server	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
by	by	kYmCp3nS
zaznamenával	zaznamenávat	k5eAaImAgInS
všechny	všechen	k3xTgFnPc4
platby	platba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monero	Monero	k1gNnSc1
je	být	k5eAaImIp3nS
vybudované	vybudovaný	k2eAgNnSc1d1
na	na	k7c6
vlastním	vlastní	k2eAgInSc6d1
protokolu	protokol	k1gInSc6
CryptoNote	CryptoNot	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
nedostatečnou	dostatečný	k2eNgFnSc4d1
anonymitu	anonymita	k1gFnSc4
Bitcoinu	Bitcoin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Bitcoinem	Bitcoino	k1gNnSc7
má	mít	k5eAaImIp3nS
ale	ale	k9
Monero	Monero	k1gNnSc4
společný	společný	k2eAgInSc1d1
systém	systém	k1gInSc1
těžby	těžba	k1gFnSc2
Proof	Proof	k1gMnSc1
of	of	k?
Work	Work	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatel	uživatel	k1gMnSc1
v	v	k7c6
něm	on	k3xPp3gInSc6
těží	těžet	k5eAaImIp3nS
Monero	Monero	k1gNnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
potvrzuje	potvrzovat	k5eAaImIp3nS
probíhající	probíhající	k2eAgFnSc1d1
transakce	transakce	k1gFnSc1
v	v	k7c6
síti	síť	k1gFnSc6
s	s	k7c7
pomocí	pomoc	k1gFnSc7
výpočetního	výpočetní	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
svého	svůj	k3xOyFgInSc2
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monero	Monero	k1gNnSc1
dále	daleko	k6eAd2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
algoritmus	algoritmus	k1gInSc1
CryptoNight	CryptoNight	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
náročný	náročný	k2eAgInSc1d1
na	na	k7c4
paměť	paměť	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
nedá	dát	k5eNaPmIp3nS
dobře	dobře	k6eAd1
těžit	těžit	k5eAaImF
na	na	k7c4
ASIC	ASIC	kA
minerech	minero	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnPc1
výrobci	výrobce	k1gMnPc1
ale	ale	k8xC
chtěli	chtít	k5eAaImAgMnP
tyto	tento	k3xDgInPc4
stroje	stroj	k1gInPc4
vylepšit	vylepšit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zvládaly	zvládat	k5eAaImAgInP
i	i	k9
těžbu	těžba	k1gFnSc4
Monera	Moner	k1gMnSc2
<g/>
,	,	kIx,
tomu	ten	k3xDgMnSc3
však	však	k9
vzdorovala	vzdorovat	k5eAaImAgFnS
komunita	komunita	k1gFnSc1
jeho	jeho	k3xOp3gMnPc2
vývojářů	vývojář	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Monero	Monero	k1gNnSc4
tak	tak	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
2018	#num#	k4
prošlo	projít	k5eAaPmAgNnS
tzv.	tzv.	kA
hard-forkem	hard-forek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
má	mít	k5eAaImIp3nS
zajistit	zajistit	k5eAaPmF
další	další	k2eAgFnSc1d1
výhodnost	výhodnost	k1gFnSc1
těžby	těžba	k1gFnSc2
na	na	k7c6
počítačích	počítač	k1gInPc6
s	s	k7c7
pomocí	pomoc	k1gFnSc7
CPU	CPU	kA
a	a	k8xC
GPU	GPU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Maximální	maximální	k2eAgInSc1d1
počet	počet	k1gInSc1
mincí	mince	k1gFnPc2
Monera	Monero	k1gNnSc2
není	být	k5eNaImIp3nS
omezen	omezit	k5eAaPmNgInS
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
regulována	regulován	k2eAgFnSc1d1
jejich	jejich	k3xOp3gFnSc1
emitace	emitace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
blok	blok	k1gInSc1
je	být	k5eAaImIp3nS
vytěžený	vytěžený	k2eAgInSc1d1
každé	každý	k3xTgFnSc6
2	#num#	k4
minuty	minuta	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
odměna	odměna	k1gFnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
mincí	mince	k1gFnPc2
se	se	k3xPyFc4
za	za	k7c4
něj	on	k3xPp3gMnSc4
bude	být	k5eAaImBp3nS
snižovat	snižovat	k5eAaImF
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
května	květen	k1gInSc2
2022	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jich	on	k3xPp3gMnPc2
bude	být	k5eAaImBp3nS
v	v	k7c6
oběhu	oběh	k1gInSc6
18	#num#	k4
132	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
odměna	odměna	k1gFnSc1
zastaví	zastavit	k5eAaPmIp3nS
na	na	k7c4
0,6	0,6	k4
XMR	XMR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velkou	velký	k2eAgFnSc7d1
výhodou	výhoda	k1gFnSc7
Monera	Monero	k1gNnSc2
je	být	k5eAaImIp3nS
dynamická	dynamický	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
vytěžených	vytěžený	k2eAgInPc2d1
bloků	blok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitcoin	Bitcoin	k1gInSc1
má	mít	k5eAaImIp3nS
velikost	velikost	k1gFnSc4
jednoho	jeden	k4xCgInSc2
bloku	blok	k1gInSc2
omezenou	omezený	k2eAgFnSc7d1
na	na	k7c4
1	#num#	k4
MB	MB	kA
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
Monero	Monero	k1gNnSc4
velikost	velikost	k1gFnSc1
bloků	blok	k1gInPc2
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS
vytíženosti	vytíženost	k1gFnSc3
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
počet	počet	k1gInSc1
transakcí	transakce	k1gFnPc2
zvýší	zvýšit	k5eAaPmIp3nS
<g/>
,	,	kIx,
zvětší	zvětšit	k5eAaPmIp3nS
se	se	k3xPyFc4
i	i	k9
velikost	velikost	k1gFnSc1
bloku	blok	k1gInSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	být	k5eAaImIp3nS
všechny	všechen	k3xTgMnPc4
pojal	pojmout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Bitcoinu	Bitcoin	k1gInSc2
tedy	tedy	k8xC
čím	co	k3yQnSc7,k3yInSc7,k3yRnSc7
víc	hodně	k6eAd2
transakcí	transakce	k1gFnPc2
uživatelé	uživatel	k1gMnPc1
provedou	provést	k5eAaPmIp3nP
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
nižší	nízký	k2eAgFnSc1d2
budou	být	k5eAaImBp3nP
poplatky	poplatek	k1gInPc1
na	na	k7c4
jednu	jeden	k4xCgFnSc4
transakci	transakce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
benefitem	benefit	k1gInSc7
Monera	Monero	k1gNnSc2
je	být	k5eAaImIp3nS
však	však	k9
jeho	jeho	k3xOp3gFnSc1
plná	plný	k2eAgFnSc1d1
anonymita	anonymita	k1gFnSc1
a	a	k8xC
zaměnitelnost	zaměnitelnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monero	Monero	k1gNnSc1
skrývá	skrývat	k5eAaImIp3nS
adresy	adresa	k1gFnPc4
příjemců	příjemce	k1gMnPc2
a	a	k8xC
odesílatelů	odesílatel	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
nově	nově	k6eAd1
i	i	k9
hodnotu	hodnota	k1gFnSc4
transakce	transakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracuje	pracovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
na	na	k7c6
implementaci	implementace	k1gFnSc6
funkce	funkce	k1gFnSc2
Kovri	Kovr	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
nad	nad	k7c7
Monerem	Moner	k1gInSc7
vytvořila	vytvořit	k5eAaPmAgFnS
síť	síť	k1gFnSc1
I2P	I2P	k1gFnSc1
a	a	k8xC
skryla	skrýt	k5eAaPmAgFnS
IP	IP	kA
adresy	adresa	k1gFnPc1
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
2	#num#	k4
klíčů	klíč	k1gInPc2
Bitcoinu	Bitcoin	k1gInSc2
má	mít	k5eAaImIp3nS
Monero	Monero	k1gNnSc4
hned	hned	k6eAd1
čtyři	čtyři	k4xCgInPc1
klíče	klíč	k1gInPc1
<g/>
:	:	kIx,
soukromý	soukromý	k2eAgInSc1d1
view	view	k?
key	key	k?
<g/>
,	,	kIx,
veřejný	veřejný	k2eAgInSc4d1
view	view	k?
key	key	k?
<g/>
,	,	kIx,
soukromý	soukromý	k2eAgMnSc1d1
spend	spend	k1gMnSc1
key	key	k?
a	a	k8xC
veřejný	veřejný	k2eAgInSc4d1
spend	spend	k1gInSc4
key	key	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
hrají	hrát	k5eAaImIp3nP
v	v	k7c6
anonymizaci	anonymizace	k1gFnSc6
zásadní	zásadní	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
pošle	poslat	k5eAaPmIp3nS
uživatel	uživatel	k1gMnSc1
A	a	k9
uživateli	uživatel	k1gMnSc3
B	B	kA
nějaké	nějaký	k3yIgNnSc4
Monero	Monero	k1gNnSc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
k	k	k7c3
tomu	ten	k3xDgNnSc3
nejprve	nejprve	k6eAd1
uživatel	uživatel	k1gMnSc1
B	B	kA
poskytnout	poskytnout	k5eAaPmF
adresu	adresa	k1gFnSc4
své	svůj	k3xOyFgFnSc2
peněženky	peněženka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
veřejného	veřejný	k2eAgInSc2d1
spend	spenda	k1gFnPc2
key	key	k?
a	a	k8xC
view	view	k?
key	key	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
této	tento	k3xDgFnSc2
adresy	adresa	k1gFnSc2
a	a	k8xC
náhodně	náhodně	k6eAd1
vygenerovaných	vygenerovaný	k2eAgNnPc2d1
dat	datum	k1gNnPc2
se	se	k3xPyFc4
vytvoří	vytvořit	k5eAaPmIp3nS
na	na	k7c4
blockchainu	blockchaina	k1gFnSc4
stealth	stealtha	k1gFnPc2
(	(	kIx(
<g/>
skrytá	skrytý	k2eAgFnSc1d1
<g/>
)	)	kIx)
adresa	adresa	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
bude	být	k5eAaImBp3nS
Monero	Monero	k1gNnSc1
odesláno	odeslán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživateli	uživatel	k1gMnSc3
B	B	kA
se	se	k3xPyFc4
připíší	připsat	k5eAaPmIp3nP
peníze	peníz	k1gInPc1
do	do	k7c2
peněženky	peněženka	k1gFnSc2
až	až	k9
po	po	k7c6
prohledání	prohledání	k1gNnSc6
blockchainu	blockchainout	k5eAaPmIp1nS
jeho	jeho	k3xOp3gInSc7
privátním	privátní	k2eAgInSc7d1
view	view	k?
key	key	k?
a	a	k8xC
spend	spend	k1gMnSc1
key	key	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stealth	Stealtha	k1gFnPc2
adresa	adresa	k1gFnSc1
je	být	k5eAaImIp3nS
sice	sice	k8xC
pro	pro	k7c4
každého	každý	k3xTgMnSc4
viditelná	viditelný	k2eAgNnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
uživatelé	uživatel	k1gMnPc1
A	A	kA
a	a	k8xC
B	B	kA
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
komu	kdo	k3yInSc3,k3yRnSc3,k3yQnSc3
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
skrytí	skrytí	k1gNnSc3
identity	identita	k1gFnSc2
odesílatele	odesílatel	k1gMnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
kruhových	kruhový	k2eAgInPc2d1
podpisů	podpis	k1gInPc2
(	(	kIx(
<g/>
ring	ring	k1gInSc1
signatures	signatures	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transakci	transakce	k1gFnSc3
nepodepisuje	podepisovat	k5eNaImIp3nS
jen	jen	k9
pravý	pravý	k2eAgMnSc1d1
odesílatel	odesílatel	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
další	další	k2eAgInPc4d1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
podpisy	podpis	k1gInPc1
jsou	být	k5eAaImIp3nP
vytaženy	vytáhnout	k5eAaPmNgInP
z	z	k7c2
již	již	k6eAd1
použitých	použitý	k2eAgInPc2d1
výstupů	výstup	k1gInPc2
blockchainu	blockchainout	k5eAaPmIp1nS
jako	jako	k9
zmatení	zmatení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příjemce	příjemka	k1gFnSc6
proto	proto	k8xC
nepozná	poznat	k5eNaPmIp3nS
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
transakci	transakce	k1gFnSc4
opravdu	opravdu	k6eAd1
podepsal	podepsat	k5eAaPmAgMnS
a	a	k8xC
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
výstup	výstup	k1gInSc4
opravdu	opravdu	k6eAd1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
úhradě	úhrada	k1gFnSc3
transakce	transakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2017	#num#	k4
byly	být	k5eAaImAgInP
kruhové	kruhový	k2eAgInPc1d1
podpisy	podpis	k1gInPc1
vylepšeny	vylepšen	k2eAgInPc1d1
ještě	ještě	k6eAd1
o	o	k7c6
tzv.	tzv.	kA
důvěrné	důvěrný	k2eAgFnSc2d1
kruhové	kruhový	k2eAgFnSc2d1
transakce	transakce	k1gFnSc2
(	(	kIx(
<g/>
Ring	ring	k1gInSc1
Confidential	Confidential	k1gMnSc1
Transaction	Transaction	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
kromě	kromě	k7c2
odesílatele	odesílatel	k1gMnSc2
dokáží	dokázat	k5eAaPmIp3nP
skrýt	skrýt	k5eAaPmF
i	i	k9
samotnou	samotný	k2eAgFnSc4d1
odesílanou	odesílaný	k2eAgFnSc4d1
sumu	suma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
u	u	k7c2
Monera	Monero	k1gNnSc2
není	být	k5eNaImIp3nS
určitelný	určitelný	k2eAgInSc1d1
původ	původ	k1gInSc1
<g/>
,	,	kIx,
cíl	cíl	k1gInSc1
a	a	k8xC
suma	suma	k1gFnSc1
transakce	transakce	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jako	jako	k9
obyčejná	obyčejný	k2eAgFnSc1d1
mince	mince	k1gFnSc1
zaměnitelné	zaměnitelný	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Omezení	omezení	k1gNnSc1
</s>
<s>
Protože	protože	k8xS
Monero	Monero	k1gNnSc1
není	být	k5eNaImIp3nS
pouze	pouze	k6eAd1
dalším	další	k2eAgInSc7d1
derivátem	derivát	k1gInSc7
Bitcoinu	Bitcoin	k1gInSc2
<g/>
,	,	kIx,
nemůže	moct	k5eNaImIp3nS
využít	využít	k5eAaPmF
možnosti	možnost	k1gFnPc4
nabízené	nabízený	k2eAgFnPc4d1
technologickým	technologický	k2eAgInSc7d1
ekosystémem	ekosystém	k1gInSc7
Bitcoinu	Bitcoin	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
platební	platební	k2eAgInPc4d1
procesory	procesor	k1gInPc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
většina	většina	k1gFnSc1
mandatorních	mandatorní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
momentálně	momentálně	k6eAd1
komunitě	komunita	k1gFnSc3
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
již	již	k6eAd1
na	na	k7c6
cestě	cesta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
vývojářů	vývojář	k1gMnPc2
<g/>
,	,	kIx,
Riccardo	Riccardo	k1gNnSc1
Spagni	Spagň	k1gMnSc6
již	již	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
platebním	platební	k2eAgInSc6d1
procesoru	procesor	k1gInSc6
PayBee	PayBe	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
a	a	k8xC
kde	kde	k6eAd1
Monero	Monero	k1gNnSc4
koupit	koupit	k5eAaPmF
<g/>
?	?	kIx.
</s>
<s>
Kromě	kromě	k7c2
těžení	těžení	k1gNnSc2
lze	lze	k6eAd1
měnu	měna	k1gFnSc4
Monero	Monero	k1gNnSc4
získat	získat	k5eAaPmF
nákupem	nákup	k1gInSc7
na	na	k7c6
burzách	burza	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
Bitfinex	Bitfinex	k1gInSc1
či	či	k8xC
Binance	Binance	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jejímu	její	k3xOp3gNnSc3
uschování	uschování	k1gNnSc3
existuje	existovat	k5eAaImIp3nS
peněženka	peněženka	k1gFnSc1
pro	pro	k7c4
PC	PC	kA
<g/>
,	,	kIx,
chytré	chytrý	k2eAgInPc1d1
mobily	mobil	k1gInPc1
i	i	k8xC
webové	webový	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
(	(	kIx(
<g/>
My	my	k3xPp1nPc1
Monero	Monero	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vytvoření	vytvoření	k1gNnSc6
peněženky	peněženka	k1gFnSc2
získáte	získat	k5eAaPmIp2nP
privátní	privátní	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
nesmíte	smět	k5eNaImIp2nP
ztratit	ztratit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgMnSc1d1
klient	klient	k1gMnSc1
(	(	kIx(
<g/>
peněženka	peněženka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Monero	Monero	k1gNnSc1
(	(	kIx(
<g/>
cryptocurrency	cryptocurrenca	k1gFnPc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Nope	nop	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
You	You	k1gFnSc1
are	ar	k1gInSc5
confused	confused	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
You	You	k1gMnSc1
should	should	k1gMnSc1
consider	consider	k1gMnSc1
this	this	k6eAd1
great	great	k2eAgInSc4d1
news	news	k1gInSc4
because	because	k1gFnSc2
you	you	k?
are	ar	k1gInSc5
abou	aboum	k1gNnSc3
<g/>
...	...	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Monero	Monero	k1gNnSc4
(	(	kIx(
<g/>
XMR	XMR	kA
<g/>
)	)	kIx)
CoinGecko	CoinGecko	k1gNnSc1
Community	Communita	k1gFnSc2
Statistics	Statisticsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Wladimir	Wladimir	k1gInSc1
J.	J.	kA
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Laan	Laan	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Monero	Monero	k1gNnSc4
(	(	kIx(
<g/>
XMR	XMR	kA
<g/>
)	)	kIx)
Market	market	k1gInSc1
Capitalization	Capitalization	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
monerostats	monerostats	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ALIENS	ALIENS	kA
<g/>
,	,	kIx,
C.	C.	kA
AlphaBay	AlphaBaa	k1gFnPc1
and	and	k?
Oasis	Oasis	k1gFnSc2
Markets	Marketsa	k1gFnPc2
to	ten	k3xDgNnSc1
Begin	Begin	k2eAgInSc1d1
Accepting	Accepting	k1gInSc1
Monero	Monero	k1gNnSc4
for	forum	k1gNnPc2
Payments	Paymentsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
23	#num#	k4
August	August	k1gMnSc1
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Recenze	recenze	k1gFnSc1
Monero	Monero	k1gNnSc1
–	–	k?
Kurz	Kurz	k1gMnSc1
<g/>
,	,	kIx,
mining	mining	k1gInSc1
<g/>
,	,	kIx,
graf	graf	k1gInSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finex	Finex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Monero	Monero	k1gNnSc1
<g/>
:	:	kIx,
Anonymní	anonymní	k2eAgFnSc1d1
kryptoměna	kryptoměn	k2eAgFnSc1d1
brojící	brojící	k2eAgFnSc1d1
proti	proti	k7c3
Asic	Asic	k1gFnSc4
minerům	miner	k1gInPc3
-	-	kIx~
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
MIKSA	MIKSA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Monero	Monero	k1gNnSc1
<g/>
:	:	kIx,
Toto	tento	k3xDgNnSc1
byste	by	kYmCp2nP
měli	mít	k5eAaImAgMnP
vědět	vědět	k5eAaImF
o	o	k7c6
zcela	zcela	k6eAd1
anonymní	anonymní	k2eAgFnSc6d1
kryptoměně	kryptoměna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Connect	Connect	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
</s>
<s>
Zcash	Zcash	k1gMnSc1
</s>
<s>
Dash	Dash	k1gInSc1
(	(	kIx(
<g/>
kryptoměna	kryptoměn	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Monero	Monero	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Monero	Monero	k1gNnSc1
-	-	kIx~
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
graf	graf	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
koupit	koupit	k5eAaPmF
a	a	k8xC
další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kryptoměny	Kryptoměn	k2eAgInPc1d1
založené	založený	k2eAgInPc1d1
na	na	k7c4
SHA-256	SHA-256	k1gFnSc4
</s>
<s>
Bitcoin	Bitcoin	k1gMnSc1
•	•	k?
Bitcoin	Bitcoin	k1gMnSc1
cash	cash	k1gFnSc2
•	•	k?
Peercoin	Peercoin	k1gInSc1
•	•	k?
Namecoin	Namecoina	k1gFnPc2
založené	založený	k2eAgFnPc4d1
na	na	k7c6
Scryptu	Scrypt	k1gInSc6
</s>
<s>
Auroracoin	Auroracoin	k1gMnSc1
•	•	k?
Dogecoin	Dogecoin	k1gMnSc1
•	•	k?
Litecoin	Litecoin	k1gMnSc1
•	•	k?
PotCoin	PotCoin	k1gMnSc1
založené	založený	k2eAgInPc4d1
na	na	k7c4
Zerocoinu	Zerocoina	k1gFnSc4
</s>
<s>
Zcoin	Zcoin	k1gMnSc1
•	•	k?
ZeroVert	ZeroVert	k1gMnSc1
•	•	k?
Anoncoin	Anoncoin	k1gMnSc1
•	•	k?
SmartCash	SmartCash	k1gMnSc1
•	•	k?
PIVX	PIVX	kA
•	•	k?
Zcash	Zcash	k1gInSc1
•	•	k?
Komodo	komoda	k1gFnSc5
založené	založený	k2eAgInPc1d1
na	na	k7c4
CryptoNote	CryptoNot	k1gMnSc5
</s>
<s>
Bytecoin	Bytecoin	k1gMnSc1
•	•	k?
Monero	Monero	k1gNnSc4
•	•	k?
DigitalNote	DigitalNot	k1gInSc5
•	•	k?
Boolberry	Boolberr	k1gInPc1
založené	založený	k2eAgInPc1d1
na	na	k7c4
Ethash	Ethash	k1gInSc4
</s>
<s>
Ethereum	Ethereum	k1gInSc1
•	•	k?
Ethereum	Ethereum	k1gInSc1
Classic	Classic	k1gMnSc1
•	•	k?
Ubiq	Ubiq	k1gMnSc1
Jiné	jiná	k1gFnSc2
proof-of-work	proof-of-work	k1gInSc1
skripty	skript	k1gInPc4
</s>
<s>
Dash	Dash	k1gMnSc1
•	•	k?
Primecoin	Primecoin	k1gMnSc1
•	•	k?
Digibyte	Digibyt	k1gInSc5
Bez	bez	k1gInSc4
proof-of-work	proof-of-work	k1gInSc1
skriptu	skript	k1gInSc2
</s>
<s>
BlackCoin	BlackCoin	k1gMnSc1
•	•	k?
Burstcoin	Burstcoin	k1gMnSc1
•	•	k?
Counterparty	Counterparta	k1gFnSc2
•	•	k?
Enigma	enigma	k1gFnSc1
•	•	k?
FunFair	FunFair	k1gMnSc1
•	•	k?
Gridcoin	Gridcoin	k1gMnSc1
•	•	k?
Lisk	Lisk	k1gMnSc1
•	•	k?
Melonport	Melonport	k1gInSc1
•	•	k?
NEM	NEM	kA
•	•	k?
NEO	NEO	kA
•	•	k?
Nxt	Nxt	k1gMnSc1
•	•	k?
OmiseGO	OmiseGO	k1gMnSc1
•	•	k?
Polkadot	Polkadot	k1gMnSc1
•	•	k?
Qtum	Qtum	k1gMnSc1
•	•	k?
RChain	RChain	k1gMnSc1
•	•	k?
Ripple	Ripple	k1gMnSc1
•	•	k?
Simple	Simple	k1gMnSc1
Token	Token	k2eAgMnSc1d1
•	•	k?
Stellar	Stellar	k1gMnSc1
•	•	k?
Shadow	Shadow	k1gMnSc1
Technologie	technologie	k1gFnSc2
</s>
<s>
Blockchain	Blockchain	k2eAgInSc1d1
•	•	k?
Proof-of-stake	Proof-of-stake	k1gInSc1
•	•	k?
Proof-of-work	Proof-of-work	k1gInSc1
system	syst	k1gInSc7
•	•	k?
Zerocash	Zerocash	k1gInSc1
•	•	k?
Zerocoin	Zerocoin	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Anonymní	anonymní	k2eAgFnSc1d1
internetové	internetový	k2eAgNnSc4d1
bankovnictví	bankovnictví	k1gNnSc4
•	•	k?
Kryptoanarchismus	Kryptoanarchismus	k1gInSc1
•	•	k?
Digitální	digitální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
•	•	k?
Digital	Digital	kA
currency	currenca	k1gFnPc1
exchanger	exchanger	k1gMnSc1
•	•	k?
Double-spending	Double-spending	k1gInSc1
•	•	k?
Virtuální	virtuální	k2eAgFnSc1d1
měna	měna	k1gFnSc1
</s>
