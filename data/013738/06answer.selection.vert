<s>
Goldfinger	Goldfinger	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
pořadí	pořadí	k1gNnSc6
třetí	třetí	k4xOgFnSc6
film	film	k1gInSc1
o	o	k7c4
Jamesi	James	k1gMnSc6
Bondovi	Bond	k1gMnSc6
z	z	k7c2
roku	rok	k1gInSc2
1964	#num#	k4
<g/>
,	,	kIx,
adaptace	adaptace	k1gFnSc1
sedmého	sedmý	k4xOgInSc2
románu	román	k1gInSc2
spisovatele	spisovatel	k1gMnSc2
Iana	Ian	k1gMnSc2
Fleminga	Fleming	k1gMnSc2
o	o	k7c6
této	tento	k3xDgFnSc6
postavě	postava	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
</s>