<s>
Goldfinger	Goldfinger	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
filmu	film	k1gInSc6
o	o	k7c4
Jamesi	Jamese	k1gFnSc4
Bondovi	Bonda	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
hudební	hudební	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Goldfinger	Goldfinger	k1gInSc1
(	(	kIx(
<g/>
hudební	hudební	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Goldfinger	Goldfinger	k1gInSc1
Základní	základní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Goldfinger	Goldfinger	k1gInSc1
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgInPc1d1
královstvíSpojené	královstvíSpojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
109	#num#	k4
min	mina	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
akční	akční	k2eAgInSc1d1
filmšpionážní	filmšpionážní	k2eAgInSc1d1
filmfilm	filmfilm	k1gInSc1
založený	založený	k2eAgInSc1d1
na	na	k7c6
románu	román	k1gInSc6
Předloha	předloha	k1gFnSc1
</s>
<s>
Goldfinger	Goldfinger	k1gInSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Richard	Richard	k1gMnSc1
MaibaumPaul	MaibaumPaula	k1gFnPc2
DehnIan	DehnIan	k1gMnSc1
Fleming	Fleming	k1gInSc4
Režie	režie	k1gFnSc2
</s>
<s>
Guy	Guy	k?
Hamilton	Hamilton	k1gInSc1
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Sean	Sean	k1gMnSc1
ConneryHonor	ConneryHonor	k1gMnSc1
BlackmanováGert	BlackmanováGert	k1gMnSc1
FröbeHarold	FröbeHarold	k1gMnSc1
SakataBernard	SakataBernard	k1gMnSc1
Lee	Lea	k1gFnSc3
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Produkce	produkce	k1gFnSc1
</s>
<s>
Harry	Harra	k1gMnSc2
SaltzmanAlbert	SaltzmanAlbert	k1gMnSc1
R.	R.	kA
Broccoli	Broccole	k1gFnSc4
Hudba	hudba	k1gFnSc1
</s>
<s>
John	John	k1gMnSc1
Barry	Barra	k1gFnSc2
Kamera	kamera	k1gFnSc1
</s>
<s>
Ted	Ted	k1gMnSc1
Moore	Moor	k1gInSc5
Střih	střih	k1gInSc4
</s>
<s>
Peter	Peter	k1gMnSc1
R.	R.	kA
Hunt	hunt	k1gInSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1964	#num#	k4
(	(	kIx(
<g/>
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
<g/>
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1965	#num#	k4
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
Produkční	produkční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Eon	Eon	k?
Productions	Productions	k1gInSc1
Distribuce	distribuce	k1gFnSc1
</s>
<s>
United	United	k1gInSc1
ArtistsNetflix	ArtistsNetflix	k1gInSc1
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
3	#num#	k4
500	#num#	k4
000	#num#	k4
USD	USD	kA
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Oscar	Oscar	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
střih	střih	k1gInSc4
zvuku	zvuk	k1gInSc2
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
Bondovky	Bondovky	k?
</s>
<s>
Srdečné	srdečný	k2eAgInPc1d1
pozdravy	pozdrav	k1gInPc1
z	z	k7c2
Ruska	Rusko	k1gNnSc2
</s>
<s>
Thunderball	Thunderball	k1gMnSc1
</s>
<s>
Goldfinger	Goldfinger	k1gInSc1
na	na	k7c4
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Goldfinger	Goldfinger	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
pořadí	pořadí	k1gNnSc6
třetí	třetí	k4xOgFnSc6
film	film	k1gInSc1
o	o	k7c4
Jamesi	James	k1gMnSc6
Bondovi	Bond	k1gMnSc6
z	z	k7c2
roku	rok	k1gInSc2
1964	#num#	k4
<g/>
,	,	kIx,
adaptace	adaptace	k1gFnSc1
sedmého	sedmý	k4xOgInSc2
románu	román	k1gInSc2
spisovatele	spisovatel	k1gMnSc2
Iana	Ian	k1gMnSc2
Fleminga	Fleming	k1gMnSc2
o	o	k7c6
této	tento	k3xDgFnSc6
postavě	postava	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
Auric	Auric	k1gMnSc1
Goldfinger	Goldfinger	k1gMnSc1
je	být	k5eAaImIp3nS
obézní	obézní	k2eAgMnSc1d1
boháč	boháč	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
kontakty	kontakt	k1gInPc4
na	na	k7c4
zločinecké	zločinecký	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
a	a	k8xC
zálibu	záliba	k1gFnSc4
ve	v	k7c6
shromažďování	shromažďování	k1gNnSc6
zlata	zlato	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
James	James	k1gInSc1
Bond	bond	k1gInSc4
dostane	dostat	k5eAaPmIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
jakým	jaký	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
způsobem	způsob	k1gInSc7
se	se	k3xPyFc4
Goldfingerovi	Goldfingerův	k2eAgMnPc1d1
daří	dařit	k5eAaImIp3nP
ilegálně	ilegálně	k6eAd1
obchodovat	obchodovat	k5eAaImF
se	s	k7c7
zlatem	zlato	k1gNnSc7
<g/>
,	,	kIx,
převážet	převážet	k5eAaImF
jej	on	k3xPp3gInSc4
tajně	tajně	k6eAd1
mezi	mezi	k7c7
státy	stát	k1gInPc7
a	a	k8xC
vydělávat	vydělávat	k5eAaImF
na	na	k7c6
různé	různý	k2eAgFnSc6d1
prodejní	prodejní	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souboji	souboj	k1gInSc6
s	s	k7c7
agentem	agens	k1gInSc7
007	#num#	k4
má	mít	k5eAaImIp3nS
většinu	většina	k1gFnSc4
času	čas	k1gInSc2
Goldfinger	Goldfinger	k1gInSc1
navrch	navrch	k7c2
<g/>
,	,	kIx,
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
korejský	korejský	k2eAgMnSc1d1
nemluvící	mluvící	k2eNgMnSc1d1
sluha	sluha	k1gMnSc1
Oddjob	Oddjoba	k1gFnPc2
Bonda	Bonda	k1gMnSc1
přepadne	přepadnout	k5eAaPmIp3nS
a	a	k8xC
později	pozdě	k6eAd2
jej	on	k3xPp3gMnSc4
uvězní	uvěznit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Goldfinger	Goldfinger	k1gMnSc1
plánuje	plánovat	k5eAaImIp3nS
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
pevnost	pevnost	k1gFnSc4
Fort	Fort	k?
Knox	Knox	k1gInSc1
a	a	k8xC
na	na	k7c6
desetiletí	desetiletí	k1gNnSc6
znehodnotit	znehodnotit	k5eAaPmF
americké	americký	k2eAgFnPc4d1
zásoby	zásoba	k1gFnPc4
zlata	zlato	k1gNnSc2
radioaktivitou	radioaktivita	k1gFnSc7
ze	z	k7c2
špinavé	špinavý	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
by	by	kYmCp3nP
jeho	jeho	k3xOp3gFnPc1
vlastní	vlastní	k2eAgFnPc1d1
zásoby	zásoba	k1gFnPc1
zlata	zlato	k1gNnSc2
obratem	obratem	k6eAd1
stouply	stoupnout	k5eAaPmAgFnP
na	na	k7c6
ceně	cena	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
James	James	k1gInSc1
Bond	bond	k1gInSc4
v	v	k7c6
zajetí	zajetí	k1gNnSc6
na	na	k7c6
Goldfingrově	Goldfingrův	k2eAgFnSc6d1
farmě	farma	k1gFnSc6
svede	svést	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc4
pilotku	pilotka	k1gFnSc4
Pussy	Pussa	k1gFnSc2
Galorovou	Galorová	k1gFnSc7
a	a	k8xC
přemluví	přemluvit	k5eAaPmIp3nP
ji	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dala	dát	k5eAaPmAgFnS
vládní	vládní	k2eAgFnSc1d1
agentuře	agentura	k1gFnSc3
echo	echo	k1gNnSc1
o	o	k7c6
plánovaném	plánovaný	k2eAgNnSc6d1
přepadení	přepadení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galorová	Galorový	k2eAgFnSc1d1
také	také	k6eAd1
při	při	k7c6
útoku	útok	k1gInSc6
z	z	k7c2
letadla	letadlo	k1gNnSc2
vypustí	vypustit	k5eAaPmIp3nS
místo	místo	k1gNnSc4
nervového	nervový	k2eAgInSc2d1
plynu	plyn	k1gInSc2
neškodnou	škodný	k2eNgFnSc4d1
látku	látka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
zdánlivě	zdánlivě	k6eAd1
podle	podle	k7c2
plánu	plán	k1gInSc2
až	až	k6eAd1
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
„	„	k?
<g/>
otrávení	otrávení	k1gNnSc2
<g/>
“	“	k?
vojáci	voják	k1gMnPc1
zvednou	zvednout	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
bitvy	bitva	k1gFnSc2
vytáhne	vytáhnout	k5eAaPmIp3nS
a	a	k8xC
zprovozní	zprovoznit	k5eAaPmIp3nS
Goldfinger	Goldfinger	k1gInSc4
svou	svůj	k3xOyFgFnSc4
bombu	bomba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bond	bond	k1gInSc1
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
snaží	snažit	k5eAaImIp3nS
zneškodnit	zneškodnit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
uspěje	uspět	k5eAaPmIp3nS
až	až	k9
přivolaný	přivolaný	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
sedm	sedm	k4xCc4
sekund	sekunda	k1gFnPc2
před	před	k7c7
výbuchem	výbuch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goldfingerovi	Goldfinger	k1gMnSc6
se	se	k3xPyFc4
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c4
přestrojení	přestrojení	k1gNnSc4
za	za	k7c4
amerického	americký	k2eAgMnSc4d1
generála	generál	k1gMnSc4
uniknout	uniknout	k5eAaPmF
a	a	k8xC
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
letadla	letadlo	k1gNnSc2
s	s	k7c7
Bondem	bond	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
výstřelu	výstřel	k1gInSc6
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
zlaté	zlatý	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
dojde	dojít	k5eAaPmIp3nS
v	v	k7c6
letadle	letadlo	k1gNnSc6
k	k	k7c3
dekompresi	dekomprese	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
jej	on	k3xPp3gMnSc4
vysaje	vysát	k5eAaPmIp3nS
z	z	k7c2
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bond	bond	k1gInSc1
se	se	k3xPyFc4
společně	společně	k6eAd1
s	s	k7c7
Galorovou	Galorová	k1gFnSc7
zachrání	zachránit	k5eAaPmIp3nP
a	a	k8xC
zůstávají	zůstávat	k5eAaImIp3nP
v	v	k7c6
objetí	objetí	k1gNnSc6
pod	pod	k7c7
padákem	padák	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Goldfingera	Goldfingero	k1gNnPc1
kritici	kritik	k1gMnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
obrat	obrat	k1gInSc4
v	v	k7c6
bondovských	bondovský	k2eAgInPc6d1
filmech	film	k1gInPc6
<g/>
,	,	kIx,
umělecky	umělecky	k6eAd1
i	i	k9
dopadem	dopad	k1gInSc7
na	na	k7c4
pop-kulturu	pop-kultura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyspělá	vyspělý	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
série	série	k1gFnSc1
<g/>
,	,	kIx,
Bond	bond	k1gInSc1
poprvé	poprvé	k6eAd1
řídí	řídit	k5eAaImIp3nS
upravený	upravený	k2eAgInSc4d1
vůz	vůz	k1gInSc4
Aston	Aston	k1gMnSc1
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
byl	být	k5eAaImAgInS
rozpočet	rozpočet	k1gInSc1
3,5	3,5	k4
milionu	milion	k4xCgInSc2
dolarů	dolar	k1gInPc2
dost	dost	k6eAd1
vysoký	vysoký	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
výdělky	výdělek	k1gInPc1
se	se	k3xPyFc4
pohybovaly	pohybovat	k5eAaImAgInP
kolem	kolem	k7c2
130	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goldfinger	Goldfinger	k1gMnSc1
se	se	k3xPyFc4
tak	tak	k9
jako	jako	k9
první	první	k4xOgFnSc1
bondovka	bondovka	k?
stal	stát	k5eAaPmAgInS
trhákem	trhák	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikde	nikde	k6eAd1
jinde	jinde	k6eAd1
James	James	k1gInSc1
Bond	bond	k1gInSc1
neschytal	schytat	k5eNaPmAgInS
tolik	tolik	k4yIc4,k4xDc4
ran	rána	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
tento	tento	k3xDgInSc1
díl	díl	k1gInSc1
mu	on	k3xPp3gMnSc3
přesto	přesto	k6eAd1
zvedl	zvednout	k5eAaPmAgInS
neuvěřitelným	uvěřitelný	k2eNgInSc7d1
způsobem	způsob	k1gInSc7
popularitu	popularita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Bond	bond	k1gInSc1
car	car	k1gMnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
filmu	film	k1gInSc6
James	James	k1gMnSc1
jezdí	jezdit	k5eAaImIp3nS
vozem	vůz	k1gInSc7
Aston	Aston	k1gMnSc1
Martin	Martin	k1gMnSc1
DB	db	kA
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozidlo	vozidlo	k1gNnSc1
je	být	k5eAaImIp3nS
vybaveno	vybavit	k5eAaPmNgNnS
bodci	bodec	k1gInPc7
ve	v	k7c6
středu	střed	k1gInSc6
kola	kolo	k1gNnSc2
na	na	k7c4
roztrhání	roztrhání	k1gNnSc4
protivníkových	protivníkův	k2eAgFnPc2d1
pneumatik	pneumatika	k1gFnPc2
<g/>
,	,	kIx,
kulomety	kulomet	k1gInPc7
a	a	k8xC
katapultovým	katapultový	k2eAgNnSc7d1
sedadlem	sedadlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
použito	použít	k5eAaPmNgNnS
ze	z	k7c2
staré	starý	k2eAgFnSc2d1
stíhačky	stíhačka	k1gFnSc2
a	a	k8xC
doopravdy	doopravdy	k6eAd1
fungovalo	fungovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
filmu	film	k1gInSc6
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
automobilové	automobilový	k2eAgFnPc4d1
honičky	honička	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Tilly	Tilla	k1gFnSc2
Mastersonovou	Mastersonová	k1gFnSc4
a	a	k8xC
jejím	její	k3xOp3gInSc7
kabrioletem	kabriolet	k1gInSc7
Ford	ford	k1gInSc1
Mustang	mustang	k1gMnSc1
a	a	k8xC
s	s	k7c7
Goldfingerovým	Goldfingerův	k2eAgInSc7d1
zabijákem	zabiják	k1gInSc7
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
voze	vůz	k1gInSc6
Mercedes-Benz	Mercedes-Benza	k1gFnPc2
190	#num#	k4
S.	S.	kA
</s>
<s>
Osoby	osoba	k1gFnPc1
a	a	k8xC
obsazení	obsazení	k1gNnSc1
</s>
<s>
Aston	Aston	k1gMnSc1
Martin	Martin	k1gMnSc1
DB5	DB5	k1gMnSc1
</s>
<s>
James	James	k1gMnSc1
Bond	bond	k1gInSc1
-	-	kIx~
Sean	Sean	k1gMnSc1
Connery	Conner	k1gMnPc7
</s>
<s>
M	M	kA
-	-	kIx~
Bernard	Bernard	k1gMnSc1
Lee	Lea	k1gFnSc3
</s>
<s>
Felix	Felix	k1gMnSc1
Leiter	Leitra	k1gFnPc2
-	-	kIx~
Cec	cecat	k5eAaImRp2nS
Linder	Linder	k1gInSc1
</s>
<s>
Moneypenny	Moneypenna	k1gFnPc1
-	-	kIx~
Lois	Lois	k1gInSc1
Maxwellová	Maxwellový	k2eAgFnSc1d1
</s>
<s>
Q	Q	kA
-	-	kIx~
Desmond	Desmond	k1gMnSc1
Llewelyn	Llewelyn	k1gMnSc1
</s>
<s>
Auric	Auric	k1gMnSc1
Goldfinger	Goldfinger	k1gMnSc1
-	-	kIx~
Gert	Gert	k1gMnSc1
Fröbe	Fröb	k1gMnSc5
</s>
<s>
Oddjob	Oddjobit	k5eAaPmRp2nS
-	-	kIx~
Harold	Harold	k1gMnSc1
Sakata	Sakat	k1gMnSc2
</s>
<s>
Pussy	Pussa	k1gFnPc1
Galore	Galor	k1gInSc5
-	-	kIx~
Honor	Honor	k1gFnSc1
Blackmanová	Blackmanová	k1gFnSc1
</s>
<s>
Jill	Jill	k1gMnSc1
Masterson	Masterson	k1gMnSc1
-	-	kIx~
Shirley	Shirley	k1gInPc1
Eatonová	Eatonová	k1gFnSc1
</s>
<s>
Tilly	Till	k1gInPc1
Masterson	Mastersona	k1gFnPc2
-	-	kIx~
Tania	Tania	k1gFnSc1
Malletová	Malletový	k2eAgFnSc1d1
</s>
<s>
Solo	Solo	k1gMnSc1
-	-	kIx~
Martin	Martin	k1gMnSc1
Benson	Benson	k1gMnSc1
</s>
<s>
Kisch	Kisch	k1gMnSc1
-	-	kIx~
Michael	Michael	k1gMnSc1
Mellinger	Mellinger	k1gMnSc1
</s>
<s>
Mr	Mr	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ling	Ling	k1gMnSc1
-	-	kIx~
Burt	Burt	k1gMnSc1
Kwouk	Kwouk	k1gMnSc1
</s>
<s>
Soundtrack	soundtrack	k1gInSc1
</s>
<s>
První	první	k4xOgMnSc1
bondovka	bondovka	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
titulkovém	titulkový	k2eAgInSc6d1
úvodu	úvod	k1gInSc6
zpívá	zpívat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
o	o	k7c4
první	první	k4xOgInSc4
ze	z	k7c2
tří	tři	k4xCgInPc2
bondovek	bondovek	k?
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
nazpívala	nazpívat	k5eAaBmAgFnS,k5eAaPmAgFnS
zpěvačka	zpěvačka	k1gFnSc1
Shirley	Shirlea	k1gMnSc2
Bassey	Bassea	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Goldfinger	Goldfingra	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Goldfinger	Goldfinger	k1gInSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Goldfinger	Goldfinger	k1gInSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Filmy	film	k1gInPc1
o	o	k7c4
Jamesi	Jamese	k1gFnSc4
Bondovi	Bondův	k2eAgMnPc1d1
Sean	Seana	k1gFnPc2
Connery	Conner	k1gMnPc7
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
No	no	k9
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Srdečné	srdečný	k2eAgInPc4d1
pozdravy	pozdrav	k1gInPc4
z	z	k7c2
Ruska	Rusko	k1gNnSc2
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Goldfinger	Goldfinger	k1gInSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Thunderball	Thunderball	k1gInSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Žiješ	žít	k5eAaImIp2nS
jenom	jenom	k9
dvakrát	dvakrát	k6eAd1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Diamanty	diamant	k1gInPc1
jsou	být	k5eAaImIp3nP
věčné	věčné	k1gNnSc4
(	(	kIx(
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
George	Georg	k1gInSc2
Lazenby	Lazenba	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
tajné	tajný	k2eAgFnSc6d1
službě	služba	k1gFnSc6
Jejího	její	k3xOp3gNnSc2
Veličenstva	veličenstvo	k1gNnSc2
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
Roger	Roger	k1gInSc1
Moore	Moor	k1gInSc5
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Žít	žít	k5eAaImF
a	a	k8xC
nechat	nechat	k5eAaPmF
zemřít	zemřít	k5eAaPmF
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Muž	muž	k1gMnSc1
se	s	k7c7
zlatou	zlatý	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Špion	špion	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
mě	já	k3xPp1nSc4
miloval	milovat	k5eAaImAgMnS
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Moonraker	Moonraker	k1gInSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jen	jen	k9
pro	pro	k7c4
tvé	tvůj	k3xOp2gNnSc4
oči	oko	k1gNnPc4
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chobotnička	Chobotnička	k1gFnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vyhlídka	vyhlídka	k1gFnSc1
na	na	k7c4
vraždu	vražda	k1gFnSc4
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
Timothy	Timotha	k1gFnSc2
Dalton	Dalton	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dech	dech	k1gInSc1
života	život	k1gInSc2
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Povolení	povolení	k1gNnSc2
zabíjet	zabíjet	k5eAaImF
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
Pierce	Pierec	k1gInSc2
Brosnan	Brosnan	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zlaté	zlatý	k2eAgNnSc1d1
oko	oko	k1gNnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zítřek	zítřek	k1gInSc1
nikdy	nikdy	k6eAd1
neumírá	umírat	k5eNaImIp3nS
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jeden	jeden	k4xCgInSc1
svět	svět	k1gInSc1
nestačí	stačit	k5eNaBmIp3nS
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dnes	dnes	k6eAd1
neumírej	umírat	k5eNaImRp2nS
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Daniel	Daniel	k1gMnSc1
Craig	Craig	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Casino	Casina	k1gMnSc5
Royale	Royal	k1gMnSc5
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Quantum	Quantum	k1gNnSc4
of	of	k?
Solace	Solace	k1gFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Skyfall	Skyfall	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Spectre	Spectr	k1gInSc5
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Není	být	k5eNaImIp3nS
čas	čas	k1gInSc4
zemřít	zemřít	k5eAaPmF
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
Neoficiální	neoficiální	k2eAgFnSc1d1,k2eNgFnSc1d1
bondovky	bondovky	k?
</s>
<s>
Casino	Casina	k1gMnSc5
Royale	Royal	k1gMnSc5
(	(	kIx(
<g/>
TV	TV	kA
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
;	;	kIx,
Barry	Barra	k1gFnSc2
Nelson	Nelson	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Casino	Casina	k1gMnSc5
Royale	Royal	k1gMnSc5
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
;	;	kIx,
David	David	k1gMnSc1
Niven	Nivna	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Nikdy	nikdy	k6eAd1
neříkej	říkat	k5eNaImRp2nS
nikdy	nikdy	k6eAd1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
;	;	kIx,
Sean	Sean	k1gMnSc1
Connery	Conner	k1gMnPc7
<g/>
)	)	kIx)
Související	související	k2eAgInPc4d1
články	článek	k1gInPc4
</s>
<s>
bondovka	bondovka	k?
•	•	k?
Bond	bond	k1gInSc1
girl	girl	k1gFnSc1
•	•	k?
Ian	Ian	k1gFnSc1
Fleming	Fleming	k1gInSc1
•	•	k?
hudba	hudba	k1gFnSc1
ve	v	k7c6
filmech	film	k1gInPc6
o	o	k7c4
Jamesi	Jamese	k1gFnSc4
Bondovi	Bondův	k2eAgMnPc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4683567-2	4683567-2	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
316753744	#num#	k4
</s>
