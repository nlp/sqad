<s>
Rybník	rybník	k1gInSc1
Rebeka	Rebeka	k1gFnSc1
</s>
<s>
Rybník	rybník	k1gInSc1
Rebeka	Rebeka	k1gFnSc1
Rybník	rybník	k1gInSc1
Rebeka	Rebeka	k1gFnSc1
v	v	k7c4
BystrémPoloha	BystrémPoloh	k1gMnSc4
Světadíl	světadíl	k1gInSc4
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
</s>
<s>
Rybník	rybník	k1gInSc1
Rebeka	Rebeka	k1gFnSc1
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
16	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozměry	rozměra	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
</s>
<s>
0,8	0,8	k4
ha	ha	kA
Ostatní	ostatní	k2eAgInSc1d1
Typ	typ	k1gInSc1
</s>
<s>
rybník	rybník	k1gInSc1
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
586	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Přítok	přítok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
Bysterský	Bysterský	k2eAgInSc1d1
potok	potok	k1gInSc1
Sídla	sídlo	k1gNnSc2
</s>
<s>
Bystré	bystrý	k2eAgNnSc1d1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rybník	rybník	k1gInSc1
Rebeka	Rebeka	k1gFnSc1
je	být	k5eAaImIp3nS
rybník	rybník	k1gInSc4
o	o	k7c6
rozloze	rozloha	k1gFnSc6
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
0,8	0,8	k4
ha	ha	kA
vybudovaný	vybudovaný	k2eAgInSc4d1
ke	k	k7c3
konci	konec	k1gInSc3
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rybník	rybník	k1gInSc1
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
asi	asi	k9
150	#num#	k4
m	m	kA
východně	východně	k6eAd1
od	od	k7c2
zámku	zámek	k1gInSc2
Bystré	bystrý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rybník	rybník	k1gInSc1
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
po	po	k7c6
hraběnce	hraběnka	k1gFnSc6
Marii	Maria	k1gFnSc3
Rebece	Rebeka	k1gFnSc3
z	z	k7c2
Harrachu	Harrach	k1gInSc2
-	-	kIx~
manželce	manželka	k1gFnSc3
majitele	majitel	k1gMnSc2
zdejšího	zdejší	k2eAgNnSc2d1
panství	panství	k1gNnSc2
hraběte	hrabě	k1gMnSc2
Františka	František	k1gMnSc2
Xavera	Xaver	k1gMnSc2
z	z	k7c2
Harrachu	Harrach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rybník	rybník	k1gInSc1
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
pro	pro	k7c4
chov	chov	k1gInSc4
ryb	ryba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Rybník	rybník	k1gInSc1
Rebeka	Rebeka	k1gFnSc1
v	v	k7c6
Bystrém	bystrý	k2eAgMnSc6d1
-	-	kIx~
infotabule	infotabule	k1gFnSc1
NS	NS	kA
</s>
<s>
Rybník	rybník	k1gInSc1
Rebeka	Rebeka	k1gFnSc1
v	v	k7c6
Bystrém	bystrý	k2eAgInSc6d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rybník	rybník	k1gInSc4
Rebeka	Rebeka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
