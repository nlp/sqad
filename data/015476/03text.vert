<s>
Eyolfek	Eyolfka	k1gFnPc2
</s>
<s>
Eyolfek	Eyolfka	k1gFnPc2
Autor	autor	k1gMnSc1
</s>
<s>
Henrik	Henrik	k1gMnSc1
Ibsen	Ibsen	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Lille	Lille	k6eAd1
Eyolf	Eyolf	k1gMnSc1
Jazyk	jazyk	k1gMnSc1
</s>
<s>
norština	norština	k1gFnSc1
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc6
</s>
<s>
1894	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Eyolfek	Eyolfek	k1gMnSc1
<g/>
,	,	kIx,
ve	v	k7c6
starších	starý	k2eAgInPc6d2
překladech	překlad	k1gInPc6
Malý	malý	k2eAgInSc1d1
Eyolf	Eyolf	k1gInSc1
(	(	kIx(
<g/>
Lille	Lille	k1gFnSc1
Eyolf	Eyolf	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
drama	drama	k1gNnSc1
Henrika	Henrik	k1gMnSc2
Ibsena	Ibsen	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
posledního	poslední	k2eAgNnSc2d1
tvůrčího	tvůrčí	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Norska	Norsko	k1gNnSc2
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drama	drama	k1gNnSc1
bylo	být	k5eAaImAgNnS
napsáno	napsat	k5eAaBmNgNnS,k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
Ibsenových	Ibsenových	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
prolíná	prolínat	k5eAaImIp3nS
realismus	realismus	k1gInSc1
se	s	k7c7
symbolismem	symbolismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
téma	téma	k1gNnSc1
poslání	poslání	k1gNnSc2
versus	versus	k7c1
lidské	lidský	k2eAgNnSc4d1
štěstí	štěstí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Konkrétně	konkrétně	k6eAd1
u	u	k7c2
Eyolfka	Eyolfka	k1gFnSc1
se	se	k3xPyFc4
autor	autor	k1gMnSc1
zaměřil	zaměřit	k5eAaPmAgMnS
na	na	k7c4
téma	téma	k1gNnSc4
sobectví	sobectví	k1gNnSc2
v	v	k7c6
kontrastu	kontrast	k1gInSc6
se	s	k7c7
sebeobětováním	sebeobětování	k1gNnSc7
<g/>
,	,	kIx,
tedy	tedy	k9
na	na	k7c4
otázku	otázka	k1gFnSc4
mravní	mravní	k2eAgFnSc2d1
odpovědnosti	odpovědnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
se	se	k3xPyFc4
zatím	zatím	k6eAd1
dočkala	dočkat	k5eAaPmAgFnS
pouze	pouze	k6eAd1
třech	tři	k4xCgNnPc2
uvedení	uvedení	k1gNnPc2
na	na	k7c6
českých	český	k2eAgNnPc6d1
jevištích	jeviště	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
uvedení	uvedení	k1gNnPc2
se	se	k3xPyFc4
u	u	k7c2
nás	my	k3xPp1nPc2
uskutečnilo	uskutečnit	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
v	v	k7c6
Divadle	divadlo	k1gNnSc6
v	v	k7c6
Řeznické	řeznická	k1gFnSc6
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Postavy	postava	k1gFnPc1
</s>
<s>
V	v	k7c6
Eyolfkovi	Eyolfek	k1gMnSc6
vystupuje	vystupovat	k5eAaImIp3nS
šest	šest	k4xCc4
postav	postava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgInPc4d1
postavy	postav	k1gInPc4
patří	patřit	k5eAaImIp3nS
statkář	statkář	k1gMnSc1
Alfred	Alfred	k1gMnSc1
Allmers	Allmersa	k1gFnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc4
žena	žena	k1gFnSc1
Rita	Rita	k1gFnSc1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
mladší	mladý	k2eAgFnSc1d2
sestra	sestra	k1gFnSc1
Asta	Asta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
zde	zde	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
devítiletý	devítiletý	k2eAgMnSc1d1
syn	syn	k1gMnSc1
manželů	manžel	k1gMnPc2
Allmersových	Allmersův	k2eAgFnPc2d1
Eyolfek	Eyolfka	k1gFnPc2
<g/>
,	,	kIx,
inženýr	inženýr	k1gMnSc1
Borgheim	Borgheim	k1gMnSc1
a	a	k8xC
Krysařka	Krysařka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělená	rozdělený	k2eAgFnSc1d1
do	do	k7c2
třech	tři	k4xCgInPc2
dějství	dějství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
dějství	dějství	k1gNnSc6
se	se	k3xPyFc4
nacházíme	nacházet	k5eAaImIp1nP
na	na	k7c6
Allmersově	Allmersův	k2eAgInSc6d1
statku	statek	k1gInSc6
u	u	k7c2
fjordu	fjord	k1gInSc2
(	(	kIx(
<g/>
několik	několik	k4yIc4
mil	míle	k1gFnPc2
od	od	k7c2
města	město	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
bohatě	bohatě	k6eAd1
zařízeném	zařízený	k2eAgInSc6d1
pokoji	pokoj	k1gInSc6
orientovaném	orientovaný	k2eAgInSc6d1
do	do	k7c2
zahrady	zahrada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgNnSc1
dějství	dějství	k1gNnSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
nevelkém	velký	k2eNgNnSc6d1
těsném	těsný	k2eAgNnSc6d1
údolí	údolí	k1gNnSc6
u	u	k7c2
břehu	břeh	k1gInSc2
a	a	k8xC
ve	v	k7c6
třetím	třetí	k4xOgNnSc6
dějství	dějství	k1gNnSc6
se	se	k3xPyFc4
nacházíme	nacházet	k5eAaImIp1nP
v	v	k7c6
Allmersově	Allmersův	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děj	děj	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
létě	léto	k1gNnSc6
během	během	k7c2
24	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
prvním	první	k4xOgNnSc6
dějství	dějství	k1gNnSc6
se	se	k3xPyFc4
dozvídáme	dozvídat	k5eAaImIp1nP
o	o	k7c6
nefunkčním	funkční	k2eNgNnSc6d1
manželství	manželství	k1gNnSc6
Allmersových	Allmersových	k2eAgNnSc6d1
a	a	k8xC
o	o	k7c6
nedávném	dávný	k2eNgInSc6d1
duševním	duševní	k2eAgInSc6d1
obratu	obrat	k1gInSc6
Alfreda	Alfred	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
právě	právě	k6eAd1
vrací	vracet	k5eAaImIp3nS
z	z	k7c2
léčebného	léčebný	k2eAgInSc2d1
pobytu	pobyt	k1gInSc2
v	v	k7c6
horách	hora	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rita	Rita	k1gFnSc1
projevuje	projevovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
žárlivou	žárlivý	k2eAgFnSc4d1
a	a	k8xC
sobeckou	sobecký	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
domu	dům	k1gInSc2
Allmersových	Allmersových	k2eAgFnPc4d1
přichází	přicházet	k5eAaImIp3nS
Krysařka	Krysařka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
jim	on	k3xPp3gMnPc3
nabízí	nabízet	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
spočívající	spočívající	k2eAgInPc4d1
ve	v	k7c6
vyhnání	vyhnání	k1gNnSc6
krys	krysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
prvního	první	k4xOgNnSc2
dějství	dějství	k1gNnSc2
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
katastrofě	katastrofa	k1gFnSc3
–	–	k?
Eyolfek	Eyolfka	k1gFnPc2
se	se	k3xPyFc4
utopí	utopit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinu	vinout	k5eAaImIp1nS
za	za	k7c4
smrt	smrt	k1gFnSc4
Eyolfka	Eyolfka	k1gFnSc1
přičítají	přičítat	k5eAaImIp3nP
rodiče	rodič	k1gMnPc4
právě	právě	k9
Krysařce	Krysařka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
dvou	dva	k4xCgNnPc6
dějstvích	dějství	k1gNnPc6
se	se	k3xPyFc4
Alfred	Alfred	k1gMnSc1
<g/>
,	,	kIx,
Rita	Rita	k1gFnSc1
<g/>
,	,	kIx,
Asta	Asta	k1gFnSc1
a	a	k8xC
Borgheim	Borgheim	k1gInSc1
snaží	snažit	k5eAaImIp3nS
vyrovnat	vyrovnat	k5eAaBmF,k5eAaPmF
se	s	k7c7
smrtí	smrt	k1gFnSc7
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
sledujeme	sledovat	k5eAaImIp1nP
Alfredovy	Alfredův	k2eAgInPc4d1
duševní	duševní	k2eAgInPc4d1
pochody	pochod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfred	Alfred	k1gMnSc1
se	se	k3xPyFc4
také	také	k9
dozvídá	dozvídat	k5eAaImIp3nS
pravdu	pravda	k1gFnSc4
o	o	k7c6
své	svůj	k3xOyFgFnSc6
sestře	sestra	k1gFnSc6
Astě	Astě	k1gMnSc2
–	–	k?
není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
jeho	jeho	k3xOp3gFnSc1
sestra	sestra	k1gFnSc1
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
slova	slovo	k1gNnSc2
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
pokrevně	pokrevně	k6eAd1
příbuzní	příbuzný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zjištění	zjištění	k1gNnSc1
je	být	k5eAaImIp3nS
zásadní	zásadní	k2eAgNnSc1d1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
Asta	Asta	k1gFnSc1
svého	svůj	k3xOyFgMnSc4
nevlastního	vlastní	k2eNgMnSc4d1
bratra	bratr	k1gMnSc4
miluje	milovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rita	rito	k1gNnPc1
i	i	k8xC
Alfred	Alfred	k1gMnSc1
mají	mít	k5eAaImIp3nP
výčitky	výčitek	k1gInPc7
svědomí	svědomí	k1gNnSc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
smrti	smrt	k1gFnSc3
Eyolfka	Eyolfka	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
obviňují	obviňovat	k5eAaImIp3nP
se	se	k3xPyFc4
navzájem	navzájem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychází	vycházet	k5eAaImIp3nS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
rodičů	rodič	k1gMnPc2
neprojevoval	projevovat	k5eNaImAgMnS
Eyolfkovi	Eyolfek	k1gMnSc3
dostatek	dostatek	k1gInSc4
lásky	láska	k1gFnSc2
<g/>
,	,	kIx,
ani	ani	k8xC
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
příliš	příliš	k6eAd1
nevěnoval	věnovat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
roli	role	k1gFnSc4
zastoupila	zastoupit	k5eAaPmAgFnS
v	v	k7c6
tomto	tento	k3xDgInSc6
ohledu	ohled	k1gInSc6
Asta	Ast	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
dějství	dějství	k1gNnSc6
se	se	k3xPyFc4
atmosféra	atmosféra	k1gFnSc1
uklidňuje	uklidňovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inženýr	inženýr	k1gMnSc1
Borgheim	Borgheim	k1gMnSc1
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
být	být	k5eAaImF
jedinou	jediný	k2eAgFnSc7d1
kladnou	kladný	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rád	rád	k6eAd1
Astu	Ast	k2eAgFnSc4d1
a	a	k8xC
celou	celý	k2eAgFnSc4d1
hru	hra	k1gFnSc4
o	o	k7c4
ni	on	k3xPp3gFnSc4
usiluje	usilovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
dějství	dějství	k1gNnSc6
odjíždí	odjíždět	k5eAaImIp3nS
pryč	pryč	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
nakonec	nakonec	k6eAd1
i	i	k9
Asta	Asta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfred	Alfred	k1gMnSc1
a	a	k8xC
Rita	rito	k1gNnPc4
zůstanou	zůstat	k5eAaPmIp3nP
sami	sám	k3xTgMnPc1
a	a	k8xC
dochází	docházet	k5eAaImIp3nP
u	u	k7c2
nich	on	k3xPp3gMnPc2
k	k	k7c3
vnitřnímu	vnitřní	k2eAgInSc3d1
obratu	obrat	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rita	Rita	k1gFnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
začít	začít	k5eAaPmF
starat	starat	k5eAaImF
o	o	k7c4
chudé	chudý	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfred	Alfred	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
chtěl	chtít	k5eAaImAgMnS
původně	původně	k6eAd1
odejít	odejít	k5eAaPmF
do	do	k7c2
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozhodne	rozhodnout	k5eAaPmIp3nS
zůstat	zůstat	k5eAaPmF
a	a	k8xC
pomoct	pomoct	k5eAaPmF
Ritě	Rita	k1gFnSc3
realizovat	realizovat	k5eAaBmF
její	její	k3xOp3gInSc4
záměr	záměr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
usmíření	usmíření	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
pocit	pocit	k1gInSc4
viny	vina	k1gFnSc2
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
snažit	snažit	k5eAaImF
odčinit	odčinit	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
obětují	obětovat	k5eAaBmIp3nP
ve	v	k7c4
prospěch	prospěch	k1gInSc4
druhých	druhý	k4xOgFnPc2
<g/>
.	.	kIx.
</s>
<s>
Drama	drama	k1gNnSc1
se	se	k3xPyFc4
odvíjí	odvíjet	k5eAaImIp3nS
chronologicky	chronologicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
hlavní	hlavní	k2eAgInPc4d1
postavy	postav	k1gInPc4
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
vztahy	vztah	k1gInPc1
prochází	procházet	k5eAaImIp3nS
vývojem	vývoj	k1gInSc7
<g/>
;	;	kIx,
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
rozpadem	rozpad	k1gInSc7
jejich	jejich	k3xOp3gFnPc2
dosavadních	dosavadní	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
České	český	k2eAgInPc1d1
překlady	překlad	k1gInPc1
</s>
<s>
České	český	k2eAgInPc1d1
překlady	překlad	k1gInPc1
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hru	hra	k1gFnSc4
poprvé	poprvé	k6eAd1
do	do	k7c2
češtiny	čeština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgMnS
Konrád	Konrád	k1gMnSc1
Pešek	Pešek	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novější	nový	k2eAgInPc1d2
překlady	překlad	k1gInPc1
pořídili	pořídit	k5eAaPmAgMnP
<g/>
:	:	kIx,
Viktor	Viktor	k1gMnSc1
Šuman	Šuman	k1gMnSc1
(	(	kIx(
<g/>
strojopisný	strojopisný	k2eAgInSc1d1
rukopis	rukopis	k1gInSc1
<g/>
,	,	kIx,
rok	rok	k1gInSc1
neznámý	známý	k2eNgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Rak	rak	k1gMnSc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
František	František	k1gMnSc1
Fröhlich	Fröhlich	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgNnPc1d1
uvedení	uvedení	k1gNnPc1
</s>
<s>
Česká	český	k2eAgNnPc1d1
uvedení	uvedení	k1gNnPc1
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Divadlo	divadlo	k1gNnSc1
v	v	k7c6
Řeznické	řeznická	k1gFnSc6
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ondřej	Ondřej	k1gMnSc1
Zajíc	Zajíc	k1gMnSc1
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1995	#num#	k4
</s>
<s>
Divadlo	divadlo	k1gNnSc1
v	v	k7c6
Dlouhé	Dlouhá	k1gFnSc6
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Jan	Jan	k1gMnSc1
Nebeský	nebeský	k2eAgMnSc1d1
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
</s>
<s>
HaDivadlo	HaDivadlo	k1gNnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Ivan	Ivan	k1gMnSc1
Buraj	Buraj	k1gMnSc1
<g/>
,	,	kIx,
premiéra	premiéra	k1gFnSc1
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HUMPÁL	HUMPÁL	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
KADEČKOVÁ	KADEČKOVÁ	kA
a	a	k8xC
Viola	Viola	k1gFnSc1
PARENTE-ČAPKOVÁ	PARENTE-ČAPKOVÁ	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnPc1d1
skandinávské	skandinávský	k2eAgFnPc1d1
literatury	literatura	k1gFnPc1
1870	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
246	#num#	k4
<g/>
-	-	kIx~
<g/>
1174	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
IBSEN	Ibsen	k1gMnSc1
<g/>
,	,	kIx,
Henrik	Henrik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hry	hra	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Divadelní	divadelní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7008-194-5	80-7008-194-5	k4
</s>
<s>
IBSEN	Ibsen	k1gMnSc1
<g/>
,	,	kIx,
Henrik	Henrik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hry	hra	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeložil	přeložit	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
RAK	rak	k1gMnSc1
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Břetislav	Břetislav	k1gMnSc1
MENCÁK	MENCÁK	kA
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Milada	Milada	k1gFnSc1
KRAUSOVÁ-LESNÁ	KRAUSOVÁ-LESNÁ	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
krásné	krásný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
hudby	hudba	k1gFnSc2
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knihovna	knihovna	k1gFnSc1
klasiků	klasik	k1gMnPc2
(	(	kIx(
<g/>
SNKLHU	SNKLHU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
–	–	k?
Eyolfek	Eyolfka	k1gFnPc2
<g/>
,	,	kIx,
překlad	překlad	k1gInSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Rak	rak	k1gMnSc1
<g/>
↑	↑	k?
http://vis.idu.cz/ProductionPetail.aspx?id=42383,2404&	http://vis.idu.cz/ProductionPetail.aspx?id=42383,2404&	k?
</s>
<s>
ibsen	ibsen	k1gInSc1
<g/>
.	.	kIx.
<g/>
nb	nb	k?
<g/>
.	.	kIx.
<g/>
no	no	k9
</s>
<s>
nkp	nkp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
nkp	nkp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
http://vis.idu.cz/ProductionPetail.aspx?id=42383,2404&	http://vis.idu.cz/ProductionPetail.aspx?id=42383,2404&	k?
</s>
<s>
ibsen	ibsen	k1gInSc1
<g/>
.	.	kIx.
<g/>
nb	nb	k?
<g/>
.	.	kIx.
<g/>
no	no	k9
<g/>
↑	↑	k?
Eyolf	Eyolf	k1gInSc1
|	|	kIx~
HaDivadlo	HaDivadlo	k1gNnSc1
<g/>
.	.	kIx.
www.hadivadlo.cz	www.hadivadlo.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
199742968	#num#	k4
</s>
