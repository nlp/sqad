<s>
Eyolfek	Eyolfek	k1gMnSc1
<g/>
,	,	kIx,
ve	v	k7c6
starších	starý	k2eAgInPc6d2
překladech	překlad	k1gInPc6
Malý	malý	k2eAgInSc1d1
Eyolf	Eyolf	k1gInSc1
(	(	kIx(
<g/>
Lille	Lille	k1gFnSc1
Eyolf	Eyolf	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
drama	drama	k1gNnSc1
Henrika	Henrik	k1gMnSc2
Ibsena	Ibsen	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
posledního	poslední	k2eAgNnSc2d1
tvůrčího	tvůrčí	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Norska	Norsko	k1gNnSc2
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>