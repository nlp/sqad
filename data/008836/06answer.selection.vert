<s>
Irština	irština	k1gFnSc1	irština
(	(	kIx(	(
<g/>
také	také	k9	také
irská	irský	k2eAgFnSc1d1	irská
gaelština	gaelština	k1gFnSc1	gaelština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
goidelský	goidelský	k2eAgMnSc1d1	goidelský
(	(	kIx(	(
<g/>
gaelský	gaelský	k2eAgInSc1d1	gaelský
<g/>
)	)	kIx)	)
jazyk	jazyk	k1gInSc1	jazyk
keltské	keltský	k2eAgFnSc2d1	keltská
větve	větev	k1gFnSc2	větev
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k1gMnSc1	příbuzný
skotské	skotský	k2eAgFnSc3d1	skotská
gaelštině	gaelština	k1gFnSc3	gaelština
<g/>
.	.	kIx.	.
</s>
