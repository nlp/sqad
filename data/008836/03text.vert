<p>
<s>
Irština	irština	k1gFnSc1	irština
(	(	kIx(	(
<g/>
také	také	k9	také
irská	irský	k2eAgFnSc1d1	irská
gaelština	gaelština	k1gFnSc1	gaelština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
goidelský	goidelský	k2eAgMnSc1d1	goidelský
(	(	kIx(	(
<g/>
gaelský	gaelský	k2eAgInSc1d1	gaelský
<g/>
)	)	kIx)	)
jazyk	jazyk	k1gInSc1	jazyk
keltské	keltský	k2eAgFnSc2d1	keltská
větve	větev	k1gFnSc2	větev
indoevropské	indoevropský	k2eAgFnSc2d1	indoevropská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
blízce	blízce	k6eAd1	blízce
příbuzný	příbuzný	k1gMnSc1	příbuzný
skotské	skotský	k2eAgFnSc3d1	skotská
gaelštině	gaelština	k1gFnSc3	gaelština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irštinou	irština	k1gFnSc7	irština
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
Irské	irský	k2eAgFnSc6d1	irská
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
jako	jako	k8xC	jako
menšinovým	menšinový	k2eAgInSc7d1	menšinový
jazykem	jazyk	k1gInSc7	jazyk
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
však	však	k9	však
status	status	k1gInSc4	status
národního	národní	k2eAgInSc2d1	národní
a	a	k8xC	a
úředního	úřední	k2eAgInSc2d1	úřední
jazyka	jazyk	k1gInSc2	jazyk
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Irsky	irsky	k6eAd1	irsky
mluvící	mluvící	k2eAgMnPc1d1	mluvící
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
především	především	k9	především
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
zvaných	zvaný	k2eAgFnPc2d1	zvaná
Gaeltacht	Gaeltachta	k1gFnPc2	Gaeltachta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
situovány	situován	k2eAgInPc4d1	situován
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
západně	západně	k6eAd1	západně
od	od	k7c2	od
Galway	Galwaa	k1gFnSc2	Galwaa
s	s	k7c7	s
přilehlými	přilehlý	k2eAgInPc7d1	přilehlý
Aranskými	Aranský	k2eAgInPc7d1	Aranský
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Bangor	Bangora	k1gFnPc2	Bangora
a	a	k8xC	a
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
ostrova	ostrov	k1gInSc2	ostrov
mluví	mluvit	k5eAaImIp3nS	mluvit
irštinou	irština	k1gFnSc7	irština
jako	jako	k8xC	jako
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
asi	asi	k9	asi
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
není	být	k5eNaImIp3nS	být
irština	irština	k1gFnSc1	irština
úředním	úřední	k2eAgMnSc7d1	úřední
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
úředně	úředně	k6eAd1	úředně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
menšinový	menšinový	k2eAgInSc4d1	menšinový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
irská	irský	k2eAgFnSc1d1	irská
vláda	vláda	k1gFnSc1	vláda
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
jakési	jakýsi	k3yIgNnSc4	jakýsi
"	"	kIx"	"
<g/>
znovuvzkříšení	znovuvzkříšení	k1gNnSc4	znovuvzkříšení
<g/>
"	"	kIx"	"
irštiny	irština	k1gFnSc2	irština
na	na	k7c6	na
základě	základ	k1gInSc6	základ
různých	různý	k2eAgInPc2d1	různý
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zavedení	zavedení	k1gNnSc4	zavedení
irštiny	irština	k1gFnSc2	irština
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
<g/>
,	,	kIx,	,
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
turistu	turista	k1gMnSc4	turista
vyvstávají	vyvstávat	k5eAaImIp3nP	vyvstávat
jisté	jistý	k2eAgFnPc4d1	jistá
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
Gaeltacht	Gaeltachta	k1gFnPc2	Gaeltachta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
ukazateli	ukazatel	k1gInPc7	ukazatel
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
mapy	mapa	k1gFnPc1	mapa
často	často	k6eAd1	často
nemyslí	myslet	k5eNaImIp3nP	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
nápisů	nápis	k1gInPc2	nápis
dvojjazyčných	dvojjazyčný	k2eAgInPc2d1	dvojjazyčný
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
anglicko-irské	anglickorský	k2eAgFnSc6d1	anglicko-irský
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
umí	umět	k5eAaImIp3nS	umět
jazykem	jazyk	k1gInSc7	jazyk
předků	předek	k1gInPc2	předek
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
pasivně	pasivně	k6eAd1	pasivně
rozumět	rozumět	k5eAaImF	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
30	[number]	k4	30
000	[number]	k4	000
žáků	žák	k1gMnPc2	žák
mimo	mimo	k7c4	mimo
Gaeltacht	Gaeltacht	k1gInSc4	Gaeltacht
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
základní	základní	k2eAgFnSc3d1	základní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
s	s	k7c7	s
výukou	výuka	k1gFnSc7	výuka
vedenou	vedený	k2eAgFnSc7d1	vedená
v	v	k7c6	v
irském	irský	k2eAgInSc6d1	irský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
přitom	přitom	k6eAd1	přitom
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
chod	chod	k1gInSc4	chod
těchto	tento	k3xDgFnPc2	tento
škol	škola	k1gFnPc2	škola
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
nevládní	vládní	k2eNgFnSc1d1	nevládní
organizace	organizace	k1gFnSc1	organizace
Gaelscoileanna	Gaelscoileanna	k1gFnSc1	Gaelscoileanna
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
obory	obor	k1gInPc1	obor
lze	lze	k6eAd1	lze
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
Národní	národní	k2eAgFnSc6d1	národní
universitě	universita	k1gFnSc6	universita
v	v	k7c4	v
Galway	Galway	k1gInPc4	Galway
(	(	kIx(	(
<g/>
Ollscoil	Ollscoil	k1gInSc4	Ollscoil
na	na	k7c4	na
hÉireann	hÉireann	k1gInSc4	hÉireann
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
státní	státní	k2eAgFnSc1d1	státní
televize	televize	k1gFnSc1	televize
TG4	TG4	k1gFnSc2	TG4
vysílaná	vysílaný	k2eAgFnSc1d1	vysílaná
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
i	i	k9	i
rádiová	rádiový	k2eAgFnSc1d1	rádiová
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
vysílání	vysílání	k1gNnSc2	vysílání
BBC	BBC	kA	BBC
vedena	vést	k5eAaImNgNnP	vést
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
s	s	k7c7	s
výukovými	výukový	k2eAgInPc7d1	výukový
programy	program	k1gInPc7	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Irština	irština	k1gFnSc1	irština
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
psala	psát	k5eAaImAgFnS	psát
irskou	irský	k2eAgFnSc7d1	irská
unciálou	unciála	k1gFnSc7	unciála
<g/>
,	,	kIx,	,
písmem	písmo	k1gNnSc7	písmo
užívaným	užívaný	k2eAgInSc7d1	užívaný
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Opuštění	opuštění	k1gNnSc4	opuštění
unciály	unciála	k1gFnSc2	unciála
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
moderních	moderní	k2eAgInPc2d1	moderní
typů	typ	k1gInPc2	typ
písma	písmo	k1gNnSc2	písmo
bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
opuštění	opuštění	k1gNnSc2	opuštění
fraktury	fraktura	k1gFnSc2	fraktura
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
křesťanství	křesťanství	k1gNnSc2	křesťanství
se	se	k3xPyFc4	se
irština	irština	k1gFnSc1	irština
psala	psát	k5eAaImAgFnS	psát
keltským	keltský	k2eAgInSc7d1	keltský
oghamem	ogham	k1gInSc7	ogham
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
se	se	k3xPyFc4	se
irština	irština	k1gFnSc1	irština
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
úředních	úřední	k2eAgInPc2d1	úřední
jazyků	jazyk	k1gInPc2	jazyk
EU	EU	kA	EU
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třech	tři	k4xCgNnPc6	tři
desetiletích	desetiletí	k1gNnPc6	desetiletí
od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
Irska	Irsko	k1gNnSc2	Irsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnPc1d1	následující
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
irštinu	irština	k1gFnSc4	irština
význačné	význačný	k2eAgInPc1d1	význačný
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
větná	větný	k2eAgFnSc1d1	větná
skladba	skladba	k1gFnSc1	skladba
typu	typ	k1gInSc2	typ
VSO	VSO	kA	VSO
(	(	kIx(	(
<g/>
Verb-Subject-Object	Verb-Subject-Object	k1gMnSc1	Verb-Subject-Object
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
stojí	stát	k5eAaImIp3nS	stát
přísudek	přísudek	k1gInSc4	přísudek
<g/>
,	,	kIx,	,
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
podmět	podmět	k1gInSc1	podmět
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
předmět	předmět	k1gInSc1	předmět
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
gramatické	gramatický	k2eAgFnPc1d1	gramatická
kategorie	kategorie	k1gFnPc1	kategorie
jmen	jméno	k1gNnPc2	jméno
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
dvě	dva	k4xCgNnPc1	dva
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
a	a	k8xC	a
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
pády	pád	k1gInPc1	pád
(	(	kIx(	(
<g/>
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
genitiv	genitiv	k1gInSc1	genitiv
a	a	k8xC	a
vokativ	vokativ	k1gInSc1	vokativ
+	+	kIx~	+
někde	někde	k6eAd1	někde
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
dativu	dativ	k1gInSc2	dativ
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
rody	rod	k1gInPc1	rod
(	(	kIx(	(
<g/>
maskulinum	maskulinum	k1gNnSc1	maskulinum
a	a	k8xC	a
femininum	femininum	k1gNnSc1	femininum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
psaná	psaný	k2eAgFnSc1d1	psaná
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
mluvené	mluvený	k2eAgFnSc2d1	mluvená
<g/>
,	,	kIx,	,
časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
spřežky	spřežka	k1gFnPc1	spřežka
a	a	k8xC	a
nevyslovované	vyslovovaný	k2eNgInPc1d1	vyslovovaný
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
slovo	slovo	k1gNnSc1	slovo
mívá	mívat	k5eAaImIp3nS	mívat
často	často	k6eAd1	často
znatelně	znatelně	k6eAd1	znatelně
méně	málo	k6eAd2	málo
hlásek	hláska	k1gFnPc2	hláska
než	než	k8xS	než
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c6	v
pravopise	pravopis	k1gInSc6	pravopis
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
pravidlo	pravidlo	k1gNnSc4	pravidlo
tzv.	tzv.	kA	tzv.
úzkých	úzké	k1gInPc2	úzké
a	a	k8xC	a
širokých	široký	k2eAgFnPc2d1	široká
hlásek	hláska	k1gFnPc2	hláska
-	-	kIx~	-
výslovnost	výslovnost	k1gFnSc1	výslovnost
všech	všecek	k3xTgFnPc2	všecek
souhlásek	souhláska	k1gFnPc2	souhláska
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
charakteru	charakter	k1gInSc6	charakter
sousedních	sousední	k2eAgFnPc2d1	sousední
samohlásek	samohláska	k1gFnPc2	samohláska
(	(	kIx(	(
<g/>
/	/	kIx~	/
<g/>
e	e	k0	e
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
/	/	kIx~	/
<g/>
i	i	k8xC	i
<g/>
/	/	kIx~	/
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
úzké	úzký	k2eAgFnPc1d1	úzká
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgNnSc1d1	ostatní
široké	široký	k2eAgNnSc1d1	široké
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
postavení	postavení	k1gNnSc6	postavení
slov	slovo	k1gNnPc2	slovo
jejich	jejich	k3xOp3gFnSc2	jejich
počáteční	počáteční	k2eAgFnSc2d1	počáteční
souhlásky	souhláska	k1gFnSc2	souhláska
podléhají	podléhat	k5eAaImIp3nP	podléhat
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mutacím	mutace	k1gFnPc3	mutace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
neexistuje	existovat	k5eNaImIp3nS	existovat
samostatný	samostatný	k2eAgInSc4d1	samostatný
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
ano	ano	k9	ano
<g/>
"	"	kIx"	"
a	a	k8xC	a
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
ne	ne	k9	ne
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
opakováním	opakování	k1gNnSc7	opakování
dotyčného	dotyčný	k2eAgNnSc2d1	dotyčné
slovesa	sloveso	k1gNnSc2	sloveso
v	v	k7c6	v
požadovaném	požadovaný	k2eAgInSc6d1	požadovaný
tvaru	tvar	k1gInSc6	tvar
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
předložky	předložka	k1gFnPc1	předložka
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
osobními	osobní	k2eAgNnPc7d1	osobní
zájmeny	zájmeno	k1gNnPc7	zájmeno
v	v	k7c4	v
jednotná	jednotný	k2eAgNnPc4d1	jednotné
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
le	le	k?	le
+	+	kIx~	+
tú	tú	k0	tú
>	>	kIx)	>
leat	leata	k1gFnPc2	leata
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
s	s	k7c7	s
tebou	ty	k3xPp2nSc7	ty
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
předložkové	předložkový	k2eAgFnPc1d1	předložková
vazby	vazba	k1gFnPc1	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Níl	Níl	k1gMnSc1	Níl
a	a	k8xC	a
fhios	fhios	k1gMnSc1	fhios
ag	ag	k?	ag
m	m	kA	m
<g/>
'	'	kIx"	'
<g/>
athair	athair	k1gMnSc1	athair
go	go	k?	go
bhfuil	bhfuil	k1gMnSc1	bhfuil
tú	tú	k0	tú
anseo	anseo	k6eAd1	anseo
<g/>
.	.	kIx.	.
=	=	kIx~	=
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gMnSc1	můj
otec	otec	k1gMnSc1	otec
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tu	tu	k6eAd1	tu
jsi	být	k5eAaImIp2nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
toho	ten	k3xDgMnSc4	ten
znalost	znalost	k1gFnSc1	znalost
u	u	k7c2	u
mého	můj	k3xOp1gMnSc2	můj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsi	být	k5eAaImIp2nS	být
ty	ten	k3xDgFnPc4	ten
tady	tady	k6eAd1	tady
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgNnSc4d1	samostatné
sloveso	sloveso	k1gNnSc4	sloveso
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
<g/>
"	"	kIx"	"
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
opisuje	opisovat	k5eAaImIp3nS	opisovat
se	se	k3xPyFc4	se
<g/>
:	:	kIx,	:
níl	níl	k?	níl
ticéad	ticéad	k1gInSc1	ticéad
agam	agam	k1gInSc1	agam
=	=	kIx~	=
"	"	kIx"	"
<g/>
nemám	mít	k5eNaImIp1nS	mít
jízdenku	jízdenka	k1gFnSc4	jízdenka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
není	být	k5eNaImIp3nS	být
jízdenka	jízdenka	k1gFnSc1	jízdenka
u	u	k7c2	u
mě	já	k3xPp1nSc2	já
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skotskou	skotský	k2eAgFnSc7d1	skotská
gaelštinou	gaelština	k1gFnSc7	gaelština
a	a	k8xC	a
manštinou	manština	k1gFnSc7	manština
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
takzvaným	takzvaný	k2eAgInPc3d1	takzvaný
q-keltským	qeltský	k2eAgInPc3d1	q-keltský
jazykům	jazyk	k1gInPc3	jazyk
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
p-keltské	peltský	k2eAgFnSc2d1	p-keltský
velštiny	velština	k1gFnSc2	velština
<g/>
,	,	kIx,	,
bretonštiny	bretonština	k1gFnSc2	bretonština
a	a	k8xC	a
kornštiny	kornština	k1gFnSc2	kornština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
proto	proto	k8xC	proto
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
velštině	velština	k1gFnSc6	velština
hláska	hláska	k1gFnSc1	hláska
p	p	k?	p
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
k.	k.	k?	k.
Hláska	hláska	k1gFnSc1	hláska
p	p	k?	p
se	se	k3xPyFc4	se
v	v	k7c6	v
původně	původně	k6eAd1	původně
irských	irský	k2eAgNnPc6d1	irské
slovech	slovo	k1gNnPc6	slovo
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
častá	častý	k2eAgFnSc1d1	častá
v	v	k7c4	v
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
přejatých	přejatý	k2eAgNnPc6d1	přejaté
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
téměř	téměř	k6eAd1	téměř
za	za	k7c4	za
domácí	domácí	k1gMnPc4	domácí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
písmeno	písmeno	k1gNnSc1	písmeno
p	p	k?	p
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
irské	irský	k2eAgFnSc2d1	irská
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pravopis	pravopis	k1gInSc4	pravopis
==	==	k?	==
</s>
</p>
<p>
<s>
Irština	irština	k1gFnSc1	irština
používá	používat	k5eAaImIp3nS	používat
písmen	písmeno	k1gNnPc2	písmeno
a	a	k8xC	a
<g/>
,	,	kIx,	,
á	á	k0	á
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
é	é	k0	é
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
h	h	k?	h
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
n	n	k0	n
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
ó	ó	k0	ó
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
r	r	kA	r
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
t	t	k?	t
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
ú.	ú.	k?	ú.
Písmena	písmeno	k1gNnSc2	písmeno
j	j	k?	j
<g/>
,	,	kIx,	,
v	v	k7c6	v
a	a	k8xC	a
z	z	k7c2	z
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vzácně	vzácně	k6eAd1	vzácně
v	v	k7c6	v
přejatých	přejatý	k2eAgNnPc6d1	přejaté
slovech	slovo	k1gNnPc6	slovo
<g/>
,	,	kIx,	,
k	k	k7c3	k
<g/>
,	,	kIx,	,
q	q	k?	q
<g/>
,	,	kIx,	,
w	w	k?	w
a	a	k8xC	a
y	y	k?	y
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
psanou	psaný	k2eAgFnSc7d1	psaná
a	a	k8xC	a
mluvenou	mluvený	k2eAgFnSc7d1	mluvená
podobou	podoba	k1gFnSc7	podoba
je	být	k5eAaImIp3nS	být
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reforma	reforma	k1gFnSc1	reforma
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odstranila	odstranit	k5eAaPmAgFnS	odstranit
některé	některý	k3yIgFnPc4	některý
tiché	tichý	k2eAgFnPc4d1	tichá
souhláskové	souhláskový	k2eAgFnPc4d1	souhlásková
spřežky	spřežka	k1gFnPc4	spřežka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pův	pův	k?	pův
<g/>
.	.	kIx.	.
aedhea	aedhea	k1gMnSc1	aedhea
dnes	dnes	k6eAd1	dnes
psáno	psán	k2eAgNnSc4d1	psáno
ae	ae	k?	ae
<g/>
,	,	kIx,	,
pův	pův	k?	pův
<g/>
.	.	kIx.	.
idhe	idh	k1gMnSc2	idh
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
píše	psát	k5eAaImIp3nS	psát
í	í	k0	í
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravopis	pravopis	k1gInSc1	pravopis
zejména	zejména	k9	zejména
souhlásek	souhláska	k1gFnPc2	souhláska
stále	stále	k6eAd1	stále
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
středověké	středověký	k2eAgFnPc4d1	středověká
výslovnosti	výslovnost	k1gFnPc4	výslovnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samohlásky	samohláska	k1gFnSc2	samohláska
===	===	k?	===
</s>
</p>
<p>
<s>
Samohlásky	samohláska	k1gFnPc1	samohláska
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
nabývají	nabývat	k5eAaImIp3nP	nabývat
pěti	pět	k4xCc2	pět
hodnot	hodnota	k1gFnPc2	hodnota
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
a	a	k8xC	a
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
nepřízvučných	přízvučný	k2eNgFnPc6d1	nepřízvučná
slabikách	slabika	k1gFnPc6	slabika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
neurčitá	určitý	k2eNgFnSc1d1	neurčitá
samohláska	samohláska	k1gFnSc1	samohláska
ə	ə	k?	ə
</s>
</p>
<p>
<s>
Pravopis	pravopis	k1gInSc1	pravopis
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
zásady	zásada	k1gFnSc2	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
psaná	psaný	k2eAgFnSc1d1	psaná
souhláska	souhláska	k1gFnSc1	souhláska
má	mít	k5eAaImIp3nS	mít
měkkou	měkký	k2eAgFnSc4d1	měkká
a	a	k8xC	a
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
výslovnost	výslovnost	k1gFnSc4	výslovnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
caol	caola	k1gFnPc2	caola
a	a	k8xC	a
leathan	leathana	k1gFnPc2	leathana
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
úzký	úzký	k2eAgInSc1d1	úzký
resp.	resp.	kA	resp.
široký	široký	k2eAgInSc1d1	široký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
shluk	shluk	k1gInSc1	shluk
souhlásek	souhláska	k1gFnPc2	souhláska
má	mít	k5eAaImIp3nS	mít
výslovnost	výslovnost	k1gFnSc1	výslovnost
buď	buď	k8xC	buď
měkkou	měkký	k2eAgFnSc7d1	měkká
<g/>
,	,	kIx,	,
či	či	k8xC	či
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
odlišení	odlišení	k1gNnSc2	odlišení
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
volbou	volba	k1gFnSc7	volba
samohlásek	samohláska	k1gFnPc2	samohláska
tento	tento	k3xDgInSc4	tento
shluk	shluk	k1gInSc4	shluk
obklopujících	obklopující	k2eAgNnPc2d1	obklopující
<g/>
:	:	kIx,	:
písmena	písmeno	k1gNnPc4	písmeno
e	e	k0	e
a	a	k8xC	a
i	i	k9	i
značí	značit	k5eAaImIp3nS	značit
měkkou	měkký	k2eAgFnSc4d1	měkká
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dvojice	dvojice	k1gFnPc4	dvojice
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
trojice	trojice	k1gFnSc1	trojice
<g/>
)	)	kIx)	)
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jedna	jeden	k4xCgFnSc1	jeden
značí	značit	k5eAaImIp3nS	značit
výslovnost	výslovnost	k1gFnSc1	výslovnost
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
a	a	k8xC	a
příp	příp	kA	příp
<g/>
.	.	kIx.	.
třetí	třetí	k4xOgFnSc1	třetí
<g/>
)	)	kIx)	)
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
/	/	kIx~	/
<g/>
měkkost	měkkost	k1gFnSc1	měkkost
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
pravopise	pravopis	k1gInSc6	pravopis
ae	ae	k?	ae
považované	považovaný	k2eAgFnPc4d1	považovaná
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidlem	pravidlem	k6eAd1	pravidlem
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
označená	označený	k2eAgFnSc1d1	označená
čárkou	čárka	k1gFnSc7	čárka
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
dlouze	dlouho	k6eAd1	dlouho
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
a	a	k8xC	a
případně	případně	k6eAd1	případně
třetí	třetí	k4xOgMnSc1	třetí
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nečte	číst	k5eNaImIp3nS	číst
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
označena	označen	k2eAgFnSc1d1	označena
čárkou	čárka	k1gFnSc7	čárka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
neznalého	znalý	k2eNgMnSc4d1	neznalý
irštiny	irština	k1gFnPc4	irština
těžké	těžký	k2eAgNnSc1d1	těžké
poznat	poznat	k5eAaPmF	poznat
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
samohlásku	samohláska	k1gFnSc4	samohláska
má	mít	k5eAaImIp3nS	mít
číst	číst	k5eAaImF	číst
<g/>
.	.	kIx.	.
</s>
<s>
Orientační	orientační	k2eAgFnSc1d1	orientační
výslovnost	výslovnost	k1gFnSc1	výslovnost
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Souhlásky	souhláska	k1gFnSc2	souhláska
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
přibližně	přibližně	k6eAd1	přibližně
výslovnost	výslovnost	k1gFnSc1	výslovnost
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Spřežky	spřežka	k1gFnPc1	spřežka
mb	mb	k?	mb
<g/>
,	,	kIx,	,
nd	nd	k?	nd
<g/>
,	,	kIx,	,
ng	ng	k?	ng
<g/>
,	,	kIx,	,
gc	gc	k?	gc
<g/>
,	,	kIx,	,
ts	ts	k0	ts
a	a	k8xC	a
bhf	bhf	k?	bhf
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
indikaci	indikace	k1gFnSc3	indikace
počáteční	počáteční	k2eAgFnSc2d1	počáteční
mutace	mutace	k1gFnSc2	mutace
-	-	kIx~	-
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
první	první	k4xOgNnSc1	první
písmeno	písmeno	k1gNnSc1	písmeno
resp.	resp.	kA	resp.
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
případě	případ	k1gInSc6	případ
bh	bh	k?	bh
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
písmeno	písmeno	k1gNnSc1	písmeno
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
určit	určit	k5eAaPmF	určit
základní	základní	k2eAgInSc4d1	základní
tvar	tvar	k1gInSc4	tvar
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
tvaru	tvar	k1gInSc2	tvar
gcat	gcat	k5eAaPmF	gcat
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
základní	základní	k2eAgInSc4d1	základní
tvar	tvar	k1gInSc4	tvar
je	být	k5eAaImIp3nS	být
cat	cat	k?	cat
=	=	kIx~	=
kočka	kočka	k1gFnSc1	kočka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
spřežky	spřežka	k1gFnPc1	spřežka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
ng	ng	k?	ng
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spřežky	spřežka	k1gFnPc1	spřežka
bh	bh	k?	bh
<g/>
,	,	kIx,	,
ch	ch	k0	ch
<g/>
,	,	kIx,	,
dh	dh	k?	dh
<g/>
,	,	kIx,	,
fh	fh	k?	fh
<g/>
,	,	kIx,	,
gh	gh	k?	gh
<g/>
,	,	kIx,	,
mh	mh	k?	mh
<g/>
,	,	kIx,	,
ph	ph	kA	ph
<g/>
,	,	kIx,	,
sh	sh	k?	sh
a	a	k8xC	a
th	th	k?	th
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
v	v	k7c6	v
počátečních	počáteční	k2eAgFnPc6d1	počáteční
mutacích	mutace	k1gFnPc6	mutace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
cat	cat	k?	cat
→	→	k?	→
chat	chata	k1gFnPc2	chata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
irština	irština	k1gFnSc1	irština
psána	psát	k5eAaImNgFnS	psát
unciálou	unciála	k1gFnSc7	unciála
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
časté	častý	k2eAgFnPc4d1	častá
např.	např.	kA	např.
na	na	k7c6	na
vývěsních	vývěsní	k2eAgInPc6d1	vývěsní
štítech	štít	k1gInPc6	štít
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
písmene	písmeno	k1gNnSc2	písmeno
h	h	k?	h
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tečka	tečka	k1gFnSc1	tečka
nad	nad	k7c7	nad
předchozím	předchozí	k2eAgNnSc7d1	předchozí
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
spřežek	spřežka	k1gFnPc2	spřežka
th	th	k?	th
<g/>
,	,	kIx,	,
gh	gh	k?	gh
<g/>
,	,	kIx,	,
dh	dh	k?	dh
<g/>
,	,	kIx,	,
bh	bh	k?	bh
<g/>
,	,	kIx,	,
mh	mh	k?	mh
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
po	po	k7c6	po
samohlásce	samohláska	k1gFnSc6	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
třeba	třeba	k6eAd1	třeba
th	th	k?	th
a	a	k8xC	a
často	často	k6eAd1	často
ani	ani	k8xC	ani
gh	gh	k?	gh
a	a	k8xC	a
dh	dh	k?	dh
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
nevyslovují	vyslovovat	k5eNaImIp3nP	vyslovovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
áth	áth	k?	áth
(	(	kIx(	(
<g/>
=	=	kIx~	=
brod	brod	k1gInSc1	brod
<g/>
)	)	kIx)	)
čteme	číst	k5eAaImIp1nP	číst
á	á	k0	á
<g/>
;	;	kIx,	;
adh	adh	k?	adh
<g/>
,	,	kIx,	,
agh	agh	k?	agh
vyslovujeme	vyslovovat	k5eAaImIp1nP	vyslovovat
aj	aj	kA	aj
<g/>
,	,	kIx,	,
ogh	ogh	k?	ogh
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
ou	ou	k0	ou
<g/>
,	,	kIx,	,
abh	abh	k?	abh
či	či	k8xC	či
amh	amh	k?	amh
vyslovujeme	vyslovovat	k5eAaImIp1nP	vyslovovat
au	au	k0	au
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Samotné	samotný	k2eAgFnPc1d1	samotná
h	h	k?	h
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
vlastní	vlastní	k2eAgNnSc4d1	vlastní
slovo	slovo	k1gNnSc4	slovo
začínáme	začínat	k5eAaImIp1nP	začínat
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Poblacht	Poblacht	k2eAgInSc1d1	Poblacht
na	na	k7c4	na
hÉireann	hÉireann	k1gInSc4	hÉireann
=	=	kIx~	=
Irská	irský	k2eAgFnSc1d1	irská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
může	moct	k5eAaImIp3nS	moct
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
vlivem	vlivem	k7c2	vlivem
mutací	mutace	k1gFnPc2	mutace
vzniknout	vzniknout	k5eAaPmF	vzniknout
t	t	k?	t
nebo	nebo	k8xC	nebo
n	n	k0	n
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tato	tento	k3xDgNnPc1	tento
písmena	písmeno	k1gNnPc1	písmeno
mohou	moct	k5eAaImIp3nP	moct
fungovat	fungovat	k5eAaImF	fungovat
i	i	k9	i
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
snadnou	snadný	k2eAgFnSc4d1	snadná
rozlišitelnost	rozlišitelnost	k1gFnSc4	rozlišitelnost
mutací	mutace	k1gFnPc2	mutace
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
pomlčkou	pomlčka	k1gFnSc7	pomlčka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
an	an	k?	an
t-úll	t-úll	k1gInSc1	t-úll
=	=	kIx~	=
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
čteme	číst	k5eAaImIp1nP	číst
dohromady	dohromady	k6eAd1	dohromady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
slovo	slovo	k1gNnSc1	slovo
psáno	psán	k2eAgNnSc1d1	psáno
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
pomlčku	pomlčka	k1gFnSc4	pomlčka
možno	možno	k6eAd1	možno
vynechat	vynechat	k5eAaPmF	vynechat
(	(	kIx(	(
<g/>
An	An	k1gMnSc1	An
tAontas	tAontas	k1gMnSc1	tAontas
Eorpach	Eorpach	k1gMnSc1	Eorpach
=	=	kIx~	=
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
prefixy	prefix	k1gInPc4	prefix
počátečních	počáteční	k2eAgFnPc2d1	počáteční
mutací	mutace	k1gFnPc2	mutace
se	se	k3xPyFc4	se
píšou	psát	k5eAaImIp3nP	psát
malým	malý	k2eAgNnSc7d1	malé
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jinak	jinak	k6eAd1	jinak
píšeme	psát	k5eAaImIp1nP	psát
verzálkami	verzálka	k1gFnPc7	verzálka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
mutace	mutace	k1gFnSc1	mutace
===	===	k?	===
</s>
</p>
<p>
<s>
Irština	irština	k1gFnSc1	irština
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
počátečních	počáteční	k2eAgFnPc2d1	počáteční
mutací	mutace	k1gFnPc2	mutace
(	(	kIx(	(
<g/>
ve	v	k7c6	v
velštině	velština	k1gFnSc6	velština
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgInPc1	tři
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oslabení	oslabení	k1gNnSc1	oslabení
(	(	kIx(	(
<g/>
ir	ir	k?	ir
<g/>
.	.	kIx.	.
séimhiú	séimhiú	k?	séimhiú
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
lenition	lenition	k1gInSc1	lenition
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spočívalo	spočívat	k5eAaImAgNnS	spočívat
původně	původně	k6eAd1	původně
ve	v	k7c6	v
změně	změna	k1gFnSc6	změna
plozívní	plozívní	k2eAgFnPc1d1	plozívní
(	(	kIx(	(
<g/>
závěrové	závěrový	k2eAgFnPc1d1	závěrová
<g/>
)	)	kIx)	)
hlásky	hláska	k1gFnPc1	hláska
na	na	k7c4	na
frikativní	frikativní	k2eAgMnPc4d1	frikativní
(	(	kIx(	(
<g/>
úžinovou	úžinový	k2eAgFnSc7d1	úžinová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
při	při	k7c6	při
mutaci	mutace	k1gFnSc6	mutace
b	b	k?	b
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdého	tvrdé	k1gNnSc2	tvrdé
k	k	k7c3	k
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
na	na	k7c4	na
v	v	k7c6	v
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
ch	ch	k0	ch
<g/>
,	,	kIx,	,
γ	γ	k?	γ
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
výslovnost	výslovnost	k1gFnSc1	výslovnost
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
původní	původní	k2eAgMnPc1d1	původní
θ	θ	k?	θ
a	a	k8xC	a
ð	ð	k?	ð
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
oslabených	oslabený	k2eAgInPc2d1	oslabený
t	t	k?	t
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
d	d	k?	d
vystřídaly	vystřídat	k5eAaPmAgInP	vystřídat
hlásky	hlásek	k1gInPc1	hlásek
h	h	k?	h
a	a	k8xC	a
γ	γ	k?	γ
V	v	k7c6	v
pravopise	pravopis	k1gInSc6	pravopis
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
mutace	mutace	k1gFnSc1	mutace
značí	značit	k5eAaImIp3nS	značit
přidáním	přidání	k1gNnSc7	přidání
h	h	k?	h
za	za	k7c4	za
původní	původní	k2eAgNnSc4d1	původní
písmeno	písmeno	k1gNnSc4	písmeno
a	a	k8xC	a
podléhají	podléhat	k5eAaImIp3nP	podléhat
jí	on	k3xPp3gFnSc3	on
slova	slovo	k1gNnSc2	slovo
začínající	začínající	k2eAgFnSc1d1	začínající
na	na	k7c6	na
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
m	m	kA	m
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
s	s	k7c7	s
<g/>
,	,	kIx,	,
t.	t.	k?	t.
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
také	také	k9	také
(	(	kIx(	(
<g/>
nesprávně	správně	k6eNd1	správně
<g/>
)	)	kIx)	)
tato	tento	k3xDgFnSc1	tento
mutace	mutace	k1gFnSc1	mutace
označovala	označovat	k5eAaImAgFnS	označovat
jako	jako	k9	jako
přídech	přídech	k1gInSc4	přídech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákryt	zákryt	k1gInSc1	zákryt
(	(	kIx(	(
<g/>
budu	být	k5eAaImBp1nS	být
používat	používat	k5eAaImF	používat
toto	tento	k3xDgNnSc4	tento
jako	jako	k8xC	jako
překlad	překlad	k1gInSc1	překlad
anglického	anglický	k2eAgInSc2d1	anglický
eclipsis	eclipsis	k1gFnSc1	eclipsis
=	=	kIx~	=
zatmění	zatmění	k1gNnSc1	zatmění
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
reprezentace	reprezentace	k1gFnSc2	reprezentace
této	tento	k3xDgFnSc2	tento
mutace	mutace	k1gFnSc2	mutace
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Irsky	irsky	k6eAd1	irsky
urú	urú	k?	urú
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
mění	měnit	k5eAaImIp3nS	měnit
neznělou	znělý	k2eNgFnSc4d1	neznělá
souhlásku	souhláska	k1gFnSc4	souhláska
na	na	k7c4	na
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
znělou	znělý	k2eAgFnSc4d1	znělá
a	a	k8xC	a
znělou	znělý	k2eAgFnSc4d1	znělá
na	na	k7c4	na
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
nosovku	nosovka	k1gFnSc4	nosovka
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
v	v	k7c6	v
psaném	psaný	k2eAgInSc6d1	psaný
jazyce	jazyk	k1gInSc6	jazyk
připsáním	připsání	k1gNnSc7	připsání
nové	nový	k2eAgFnSc2d1	nová
hlásky	hláska	k1gFnSc2	hláska
před	před	k7c4	před
původní	původní	k2eAgFnSc4d1	původní
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
g	g	kA	g
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
ng	ng	k?	ng
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podléhají	podléhat	k5eAaImIp3nP	podléhat
mu	on	k3xPp3gInSc3	on
slova	slovo	k1gNnSc2	slovo
začínající	začínající	k2eAgFnSc1d1	začínající
na	na	k7c6	na
b	b	k?	b
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
,	,	kIx,	,
d	d	k?	d
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
,	,	kIx,	,
g	g	kA	g
<g/>
,	,	kIx,	,
p	p	k?	p
<g/>
,	,	kIx,	,
t.	t.	k?	t.
<g/>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
mutací	mutace	k1gFnPc2	mutace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Změna	změna	k1gFnSc1	změna
s	s	k7c7	s
→	→	k?	→
t	t	k?	t
(	(	kIx(	(
<g/>
v	v	k7c6	v
pravopise	pravopis	k1gInSc6	pravopis
značeno	značen	k2eAgNnSc1d1	značeno
ts	ts	k0	ts
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
U	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
začínajících	začínající	k2eAgNnPc2d1	začínající
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
</s>
</p>
<p>
<s>
prefix	prefix	k1gInSc1	prefix
h	h	k?	h
</s>
</p>
<p>
<s>
prefix	prefix	k1gInSc1	prefix
n-	n-	k?	n-
</s>
</p>
<p>
<s>
prefix	prefix	k1gInSc1	prefix
t-U	t-U	k?	t-U
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c4	na
hlásky	hláska	k1gFnPc4	hláska
neumožňující	umožňující	k2eNgFnSc3d1	neumožňující
počáteční	počáteční	k2eAgFnSc3d1	počáteční
mutaci	mutace	k1gFnSc3	mutace
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
a	a	k8xC	a
počátek	počátek	k1gInSc4	počátek
slova	slovo	k1gNnSc2	slovo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
počátečních	počáteční	k2eAgFnPc2d1	počáteční
mutací	mutace	k1gFnPc2	mutace
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
fonetická	fonetický	k2eAgFnSc1d1	fonetická
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nesou	nést	k5eAaImIp3nP	nést
i	i	k9	i
gramatický	gramatický	k2eAgInSc4d1	gramatický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Detailněji	detailně	k6eAd2	detailně
viz	vidět	k5eAaImRp2nS	vidět
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
gramatické	gramatický	k2eAgFnPc4d1	gramatická
kategorie	kategorie	k1gFnPc4	kategorie
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
===	===	k?	===
</s>
</p>
<p>
<s>
Podstatná	podstatný	k2eAgNnPc1d1	podstatné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
členěna	členit	k5eAaImNgFnS	členit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
tříd	třída	k1gFnPc2	třída
(	(	kIx(	(
<g/>
deklinací	deklinace	k1gFnPc2	deklinace
-	-	kIx~	-
díochlaonadh	díochlaonadh	k1gInSc1	díochlaonadh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
třídu	třída	k1gFnSc4	třída
je	být	k5eAaImIp3nS	být
nejsnazší	snadný	k2eAgNnSc1d3	nejsnazší
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
genitivu	genitiv	k1gInSc2	genitiv
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
historickému	historický	k2eAgInSc3d1	historický
vývoji	vývoj	k1gInSc3	vývoj
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
tvarů	tvar	k1gInPc2	tvar
plurálu	plurál	k1gInSc2	plurál
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
genitivu	genitiv	k1gInSc2	genitiv
u	u	k7c2	u
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
znalost	znalost	k1gFnSc1	znalost
zařazení	zařazení	k1gNnSc2	zařazení
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
pod	pod	k7c4	pod
určitou	určitý	k2eAgFnSc4d1	určitá
deklinaci	deklinace	k1gFnSc4	deklinace
ještě	ještě	k6eAd1	ještě
negarantuje	garantovat	k5eNaBmIp3nS	garantovat
schopnost	schopnost	k1gFnSc4	schopnost
správně	správně	k6eAd1	správně
skloňovat	skloňovat	k5eAaImF	skloňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc4	první
třídu	třída	k1gFnSc4	třída
tvoří	tvořit	k5eAaImIp3nS	tvořit
jména	jméno	k1gNnPc1	jméno
mužského	mužský	k2eAgInSc2d1	mužský
rodu	rod	k1gInSc2	rod
končící	končící	k2eAgNnSc1d1	končící
na	na	k7c4	na
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
souhlásku	souhláska	k1gFnSc4	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Genitiv	genitiv	k1gInSc1	genitiv
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
měkčením	měkčení	k1gNnSc7	měkčení
koncové	koncový	k2eAgFnSc2d1	koncová
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
nominativům	nominativ	k1gInPc3	nominativ
bád	bád	k?	bád
(	(	kIx(	(
<g/>
=	=	kIx~	=
člun	člun	k1gInSc1	člun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fear	fear	k1gMnSc1	fear
(	(	kIx(	(
<g/>
=	=	kIx~	=
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
genitivy	genitiv	k1gInPc1	genitiv
báid	báid	k1gInSc1	báid
a	a	k8xC	a
fir	fir	k?	fir
<g/>
.	.	kIx.	.
</s>
<s>
Vokativ	vokativ	k1gInSc1	vokativ
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
genitiv	genitiv	k1gInSc1	genitiv
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
měkčení	měkčení	k1gNnSc6	měkčení
se	se	k3xPyFc4	se
koncové	koncový	k2eAgFnSc6d1	koncová
-ch	h	k?	-ch
mění	měnit	k5eAaImIp3nP	měnit
na	na	k7c6	na
nevyslovované	vyslovovaný	k2eNgFnSc6d1	nevyslovovaná
-gh	h	k?	-gh
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
třída	třída	k1gFnSc1	třída
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
až	až	k9	až
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
výjimky	výjimka	k1gFnPc4	výjimka
jmény	jméno	k1gNnPc7	jméno
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Genitiv	genitiv	k1gInSc1	genitiv
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
koncovkou	koncovka	k1gFnSc7	koncovka
-e	-e	k?	-e
+	+	kIx~	+
příp	příp	kA	příp
<g/>
.	.	kIx.	.
měkčením	měkčení	k1gNnSc7	měkčení
poslední	poslední	k2eAgFnSc2d1	poslední
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
long	long	k1gMnSc1	long
(	(	kIx(	(
<g/>
=	=	kIx~	=
loď	loď	k1gFnSc1	loď
<g/>
)	)	kIx)	)
→	→	k?	→
loinge	loinge	k1gInSc1	loinge
<g/>
,	,	kIx,	,
coill	coill	k1gInSc1	coill
(	(	kIx(	(
<g/>
=	=	kIx~	=
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
→	→	k?	→
coille	coille	k1gInSc1	coille
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
původní	původní	k2eAgInSc1d1	původní
e	e	k0	e
není	být	k5eNaImIp3nS	být
díky	díky	k7c3	díky
změně	změna	k1gFnSc3	změna
pravopisu	pravopis	k1gInSc2	pravopis
a	a	k8xC	a
výslovnosti	výslovnost	k1gFnSc2	výslovnost
vidět	vidět	k5eAaImF	vidět
-	-	kIx~	-
teach	teach	k1gInSc4	teach
(	(	kIx(	(
<g/>
=	=	kIx~	=
dům	dům	k1gInSc1	dům
<g/>
)	)	kIx)	)
→	→	k?	→
tí	tí	k?	tí
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
<g/>
,	,	kIx,	,
tighe	tighat	k5eAaPmIp3nS	tighat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokativ	vokativ	k1gInSc1	vokativ
singuláru	singulár	k1gInSc2	singulár
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
druhé	druhý	k4xOgFnSc2	druhý
až	až	k8xS	až
páté	pátý	k4xOgFnSc2	pátý
třídy	třída	k1gFnSc2	třída
stejný	stejný	k2eAgInSc4d1	stejný
jako	jako	k8xS	jako
nominativ	nominativ	k1gInSc4	nominativ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
třída	třída	k1gFnSc1	třída
sestává	sestávat	k5eAaImIp3nS	sestávat
jak	jak	k8xC	jak
z	z	k7c2	z
mužských	mužský	k2eAgNnPc2d1	mužské
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
ženských	ženský	k2eAgNnPc2d1	ženské
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Genitiv	genitiv	k1gInSc1	genitiv
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
koncovkou	koncovka	k1gFnSc7	koncovka
-a	-a	k?	-a
a	a	k8xC	a
případně	případně	k6eAd1	případně
tvrzením	tvrzení	k1gNnSc7	tvrzení
poslední	poslední	k2eAgFnSc2d1	poslední
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
loch	loch	k1gInSc1	loch
(	(	kIx(	(
<g/>
=	=	kIx~	=
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
→	→	k?	→
locha	locha	k1gMnSc1	locha
<g/>
,	,	kIx,	,
feoil	feoil	k1gMnSc1	feoil
(	(	kIx(	(
<g/>
=	=	kIx~	=
maso	maso	k1gNnSc1	maso
<g/>
)	)	kIx)	)
→	→	k?	→
feola	feola	k1gFnSc1	feola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
třídu	třída	k1gFnSc4	třída
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinou	většinou	k6eAd1	většinou
mužská	mužský	k2eAgNnPc1d1	mužské
jména	jméno	k1gNnPc1	jméno
končící	končící	k2eAgNnPc1d1	končící
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
nebo	nebo	k8xC	nebo
zdrobňující	zdrobňující	k2eAgFnSc4d1	zdrobňující
příponu	přípona	k1gFnSc4	přípona
-ín	-ín	k?	-ín
(	(	kIx(	(
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
rí	rí	k?	rí
=	=	kIx~	=
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Genitiv	genitiv	k1gInSc1	genitiv
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
nominativ	nominativ	k1gInSc1	nominativ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pátá	pátý	k4xOgFnSc1	pátý
třída	třída	k1gFnSc1	třída
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
převážně	převážně	k6eAd1	převážně
ženská	ženský	k2eAgNnPc4d1	ženské
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Genitiv	genitiv	k1gInSc1	genitiv
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
tvrzením	tvrzení	k1gNnSc7	tvrzení
poslední	poslední	k2eAgFnSc2d1	poslední
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
,	,	kIx,	,
koncovkou	koncovka	k1gFnSc7	koncovka
-ach	cha	k1gFnPc2	-acha
<g/>
,	,	kIx,	,
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
končících	končící	k2eAgNnPc2d1	končící
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
koncovkani	koncovkan	k1gMnPc1	koncovkan
-d	-d	k?	-d
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
n.	n.	k?	n.
Př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
máthair	máthair	k1gInSc1	máthair
(	(	kIx(	(
<g/>
=	=	kIx~	=
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
→	→	k?	→
máthar	máthar	k1gMnSc1	máthar
<g/>
,	,	kIx,	,
beoir	beoir	k1gMnSc1	beoir
(	(	kIx(	(
<g/>
=	=	kIx~	=
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
→	→	k?	→
beorach	beorach	k1gMnSc1	beorach
<g/>
,	,	kIx,	,
cara	car	k1gMnSc2	car
(	(	kIx(	(
<g/>
=	=	kIx~	=
přítel	přítel	k1gMnSc1	přítel
<g/>
)	)	kIx)	)
→	→	k?	→
carad	carad	k1gInSc1	carad
<g/>
.	.	kIx.	.
<g/>
Plurál	plurál	k1gInSc1	plurál
existuje	existovat	k5eAaImIp3nS	existovat
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
slabý	slabý	k2eAgInSc1d1	slabý
<g/>
.	.	kIx.	.
</s>
<s>
Slabý	slabý	k2eAgInSc1d1	slabý
plurál	plurál	k1gInSc1	plurál
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
měkčením	měkčení	k1gNnSc7	měkčení
končící	končící	k2eAgFnSc2d1	končící
souhlásky	souhláska	k1gFnSc2	souhláska
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
převažuje	převažovat	k5eAaImIp3nS	převažovat
u	u	k7c2	u
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nominativ	nominativ	k1gInSc1	nominativ
plurálu	plurál	k1gInSc2	plurál
stejný	stejný	k2eAgMnSc1d1	stejný
jako	jako	k8xS	jako
genitiv	genitiv	k1gInSc1	genitiv
singuláru	singulár	k1gInSc2	singulár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
koncovkami	koncovka	k1gFnPc7	koncovka
-a	-a	k?	-a
<g/>
,	,	kIx,	,
-e	-e	k?	-e
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tvrdosti	tvrdost	k1gFnSc6	tvrdost
či	či	k8xC	či
měkkosti	měkkost	k1gFnSc6	měkkost
koncové	koncový	k2eAgFnSc2d1	koncová
samohlásky	samohláska	k1gFnSc2	samohláska
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
převažuje	převažovat	k5eAaImIp3nS	převažovat
u	u	k7c2	u
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
třetí	třetí	k4xOgFnSc2	třetí
až	až	k8xS	až
páté	pátý	k4xOgFnSc2	pátý
třídy	třída	k1gFnSc2	třída
převažuje	převažovat	k5eAaImIp3nS	převažovat
silný	silný	k2eAgInSc4d1	silný
plurál	plurál	k1gInSc4	plurál
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
nejčastěji	často	k6eAd3	často
příponami	přípona	k1gFnPc7	přípona
-anna	nno	k1gNnSc2	-anno
<g/>
,	,	kIx,	,
-acha	cha	k1gMnSc1	-acha
<g/>
,	,	kIx,	,
-í	-í	k?	-í
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
-ta	a	k?	-ta
<g/>
,	,	kIx,	,
-tha	ha	k6eAd1	-tha
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Genitiv	genitiv	k1gInSc1	genitiv
slov	slovo	k1gNnPc2	slovo
majících	mající	k2eAgInPc2d1	mající
slabý	slabý	k2eAgInSc4d1	slabý
plurál	plurál	k1gInSc4	plurál
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xC	jako
nominativ	nominativ	k1gInSc1	nominativ
singuláru	singulár	k1gInSc2	singulár
<g/>
,	,	kIx,	,
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
plurálem	plurál	k1gInSc7	plurál
jako	jako	k8xC	jako
nominativ	nominativ	k1gInSc4	nominativ
plurálu	plurál	k1gInSc2	plurál
<g/>
.	.	kIx.	.
</s>
<s>
Vokativ	vokativ	k1gInSc1	vokativ
plurálu	plurál	k1gInSc2	plurál
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
se	s	k7c7	s
slabým	slabý	k2eAgInSc7d1	slabý
plurálem	plurál	k1gInSc7	plurál
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
nominativu	nominativ	k1gInSc2	nominativ
singuláru	singulár	k1gInSc2	singulár
příponou	přípona	k1gFnSc7	přípona
-a	-a	k?	-a
<g/>
,	,	kIx,	,
u	u	k7c2	u
slov	slovo	k1gNnPc2	slovo
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
plurálem	plurál	k1gInSc7	plurál
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
vokativ	vokativ	k1gInSc4	vokativ
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xC	jako
nominativ	nominativ	k1gInSc1	nominativ
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc4	příklad
z	z	k7c2	z
první	první	k4xOgFnSc2	první
deklinace	deklinace	k1gFnSc2	deklinace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
oslovení	oslovení	k1gNnSc6	oslovení
předchází	předcházet	k5eAaImIp3nS	předcházet
vokativu	vokativ	k1gInSc2	vokativ
vždy	vždy	k6eAd1	vždy
částice	částice	k1gFnSc1	částice
a	a	k8xC	a
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
oslabení	oslabení	k1gNnSc4	oslabení
první	první	k4xOgFnSc2	první
souhlásky	souhláska	k1gFnSc2	souhláska
následujícího	následující	k2eAgNnSc2d1	následující
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
správný	správný	k2eAgInSc1d1	správný
tvar	tvar	k1gInSc1	tvar
oslovení	oslovení	k1gNnSc2	oslovení
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
a	a	k8xC	a
fhir	fhir	k1gInSc1	fhir
<g/>
!	!	kIx.	!
</s>
<s>
=	=	kIx~	=
muži	muž	k1gMnSc6	muž
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
</p>
<p>
<s>
===	===	k?	===
Člen	člen	k1gMnSc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
irštině	irština	k1gFnSc6	irština
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
an	an	k?	an
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gMnSc1	člen
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
počáteční	počáteční	k2eAgFnSc1d1	počáteční
mutace	mutace	k1gFnSc1	mutace
následujícího	následující	k2eAgNnSc2d1	následující
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nominativu	nominativ	k1gInSc6	nominativ
tvar	tvar	k1gInSc4	tvar
an	an	k?	an
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dochází	docházet	k5eAaImIp3nS	docházet
</s>
</p>
<p>
<s>
u	u	k7c2	u
ženských	ženský	k2eAgNnPc2d1	ženské
jmen	jméno	k1gNnPc2	jméno
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
an	an	k?	an
mháthair	mháthair	k1gInSc1	mháthair
=	=	kIx~	=
ta	ten	k3xDgFnSc1	ten
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jmen	jméno	k1gNnPc2	jméno
začínajících	začínající	k2eAgFnPc2d1	začínající
na	na	k7c4	na
d	d	k?	d
<g/>
,	,	kIx,	,
t.	t.	k?	t.
</s>
</p>
<p>
<s>
u	u	k7c2	u
ženských	ženský	k2eAgNnPc2d1	ženské
jmen	jméno	k1gNnPc2	jméno
začínajících	začínající	k2eAgNnPc2d1	začínající
na	na	k7c4	na
s	s	k7c7	s
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
na	na	k7c6	na
t	t	k?	t
(	(	kIx(	(
<g/>
an	an	k?	an
tsúil	tsúil	k1gInSc1	tsúil
=	=	kIx~	=
to	ten	k3xDgNnSc1	ten
oko	oko	k1gNnSc1	oko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
u	u	k7c2	u
mužských	mužský	k2eAgNnPc2d1	mužské
jmen	jméno	k1gNnPc2	jméno
začínajících	začínající	k2eAgNnPc2d1	začínající
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
k	k	k7c3	k
přidání	přidání	k1gNnSc3	přidání
předpony	předpona	k1gFnSc2	předpona
t-	t-	k?	t-
(	(	kIx(	(
<g/>
an	an	k?	an
t-úll	t-úll	k1gInSc1	t-úll
=	=	kIx~	=
to	ten	k3xDgNnSc4	ten
jablko	jablko	k1gNnSc4	jablko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
pro	pro	k7c4	pro
mužská	mužský	k2eAgNnPc4d1	mužské
jména	jméno	k1gNnPc4	jméno
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc4	tvar
také	také	k9	také
an	an	k?	an
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dochází	docházet	k5eAaImIp3nS	docházet
</s>
</p>
<p>
<s>
oslabení	oslabení	k1gNnSc4	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
an	an	k?	an
fhir	fhir	k1gInSc1	fhir
=	=	kIx~	=
toho	ten	k3xDgMnSc4	ten
muže	muž	k1gMnSc4	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jmen	jméno	k1gNnPc2	jméno
začínajících	začínající	k2eAgFnPc2d1	začínající
na	na	k7c4	na
d	d	k?	d
<g/>
,	,	kIx,	,
t.	t.	k?	t.
</s>
</p>
<p>
<s>
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
začínajících	začínající	k2eAgNnPc2d1	začínající
na	na	k7c4	na
s	s	k7c7	s
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
na	na	k7c6	na
t	t	k?	t
(	(	kIx(	(
<g/>
an	an	k?	an
tsolas	tsolas	k1gInSc1	tsolas
=	=	kIx~	=
toho	ten	k3xDgNnSc2	ten
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
jednotného	jednotný	k2eAgNnSc2d1	jednotné
čísla	číslo	k1gNnSc2	číslo
pro	pro	k7c4	pro
ženská	ženský	k2eAgNnPc4d1	ženské
jména	jméno	k1gNnPc4	jméno
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc4	tvar
členu	člen	k1gInSc2	člen
na	na	k7c4	na
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
</s>
</p>
<p>
<s>
u	u	k7c2	u
jmen	jméno	k1gNnPc2	jméno
začínajících	začínající	k2eAgNnPc2d1	začínající
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
prefix	prefix	k1gInSc1	prefix
h	h	k?	h
(	(	kIx(	(
<g/>
na	na	k7c6	na
hoibre	hoibr	k1gInSc5	hoibr
=	=	kIx~	=
té	ten	k3xDgFnSc2	ten
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
množném	množný	k2eAgNnSc6d1	množné
čísle	číslo	k1gNnSc6	číslo
je	být	k5eAaImIp3nS	být
tvar	tvar	k1gInSc1	tvar
členu	člen	k1gInSc2	člen
na	na	k7c4	na
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
nominativu	nominativ	k1gInSc6	nominativ
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
prefix	prefix	k1gInSc1	prefix
h	h	k?	h
k	k	k7c3	k
ženským	ženský	k2eAgNnPc3d1	ženské
jménům	jméno	k1gNnPc3	jméno
(	(	kIx(	(
<g/>
na	na	k7c4	na
hoibreacha	hoibreach	k1gMnSc4	hoibreach
=	=	kIx~	=
ty	ten	k3xDgFnPc4	ten
práce	práce	k1gFnPc4	práce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
v	v	k7c6	v
množném	množný	k2eAgInSc6d1	množný
genitivu	genitiv	k1gInSc6	genitiv
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
</s>
</p>
<p>
<s>
k	k	k7c3	k
zákrytu	zákryt	k1gInSc2	zákryt
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
(	(	kIx(	(
<g/>
na	na	k7c4	na
bhfear	bhfear	k1gInSc4	bhfear
=	=	kIx~	=
těch	ten	k3xDgMnPc2	ten
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
k	k	k7c3	k
dodání	dodání	k1gNnSc3	dodání
prefixu	prefix	k1gInSc2	prefix
n-	n-	k?	n-
před	před	k7c4	před
slova	slovo	k1gNnPc4	slovo
začínající	začínající	k2eAgNnPc4d1	začínající
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
(	(	kIx(	(
<g/>
na	na	k7c4	na
n-éan	n-éan	k1gInSc4	n-éan
=	=	kIx~	=
těch	ten	k3xDgMnPc2	ten
ptáků	pták	k1gMnPc2	pták
<g/>
)	)	kIx)	)
<g/>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
určeno	určit	k5eAaPmNgNnS	určit
přívlastkem	přívlastek	k1gInSc7	přívlastek
tvořeným	tvořený	k2eAgNnSc7d1	tvořené
určitým	určitý	k2eAgNnSc7d1	určité
jménem	jméno	k1gNnSc7	jméno
v	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
a	a	k8xC	a
člen	člen	k1gMnSc1	člen
se	se	k3xPyFc4	se
před	před	k7c4	před
něj	on	k3xPp3gMnSc4	on
už	už	k6eAd1	už
neklade	klást	k5eNaImIp3nS	klást
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Baile	Baile	k1gFnSc1	Baile
Átha	Átha	k1gFnSc1	Átha
an	an	k?	an
Rí	Rí	k1gMnSc2	Rí
(	(	kIx(	(
<g/>
Athenry	Athenra	k1gMnSc2	Athenra
-	-	kIx~	-
město	město	k1gNnSc1	město
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
doslova	doslova	k6eAd1	doslova
město	město	k1gNnSc1	město
toho	ten	k3xDgInSc2	ten
brodu	brod	k1gInSc2	brod
toho	ten	k3xDgMnSc2	ten
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
a	a	k8xC	a
příslovce	příslovce	k1gNnPc1	příslovce
===	===	k?	===
</s>
</p>
<p>
<s>
Přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
podstatné	podstatný	k2eAgNnSc4d1	podstatné
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
rodě	rod	k1gInSc6	rod
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
počátečního	počáteční	k2eAgNnSc2d1	počáteční
písmena	písmeno	k1gNnSc2	písmeno
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
např.	např.	kA	např.
velká	velký	k2eAgFnSc1d1	velká
žena	žena	k1gFnSc1	žena
=	=	kIx~	=
bean	bean	k1gMnSc1	bean
mhór	mhór	k1gMnSc1	mhór
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velký	velký	k2eAgMnSc1d1	velký
muž	muž	k1gMnSc1	muž
=	=	kIx~	=
fear	fear	k1gInSc1	fear
mór	móra	k1gFnPc2	móra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
opačně	opačně	k6eAd1	opačně
<g/>
,	,	kIx,	,
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
adjektiva	adjektivum	k1gNnSc2	adjektivum
jen	jen	k9	jen
v	v	k7c6	v
nominativu	nominativ	k1gInSc6	nominativ
a	a	k8xC	a
jen	jen	k6eAd1	jen
když	když	k8xS	když
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
má	mít	k5eAaImIp3nS	mít
koncovku	koncovka	k1gFnSc4	koncovka
silného	silný	k2eAgInSc2d1	silný
plurálu	plurál	k1gInSc2	plurál
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
užívána	užíván	k2eAgNnPc1d1	užíváno
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
předpony	předpona	k1gFnSc2	předpona
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
složenin	složenina	k1gFnPc2	složenina
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
poč	poč	k?	poč
<g/>
.	.	kIx.	.
písmena	písmeno	k1gNnSc2	písmeno
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
složeniny	složenina	k1gFnSc2	složenina
(	(	kIx(	(
<g/>
seanfhear	seanfhear	k1gMnSc1	seanfhear
=	=	kIx~	=
starý	starý	k2eAgMnSc1d1	starý
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
umístěná	umístěný	k2eAgNnPc1d1	umístěné
za	za	k7c4	za
jménem	jméno	k1gNnSc7	jméno
podstatným	podstatný	k2eAgFnPc3d1	podstatná
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
skloňují	skloňovat	k5eAaImIp3nP	skloňovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
měkčení	měkčení	k1gNnSc2	měkčení
poslední	poslední	k2eAgFnSc2d1	poslední
souhlásky	souhláska	k1gFnSc2	souhláska
v	v	k7c6	v
mužském	mužský	k2eAgInSc6d1	mužský
rodě	rod	k1gInSc6	rod
a	a	k8xC	a
příponu	přípona	k1gFnSc4	přípona
-e	-e	k?	-e
v	v	k7c6	v
ženském	ženský	k2eAgNnSc6d1	ženské
<g/>
,	,	kIx,	,
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
pak	pak	k6eAd1	pak
přípony	přípona	k1gFnPc1	přípona
-e	-e	k?	-e
nebo	nebo	k8xC	nebo
-	-	kIx~	-
<g/>
a.	a.	k?	a.
Adjektiv	adjektivum	k1gNnPc2	adjektivum
končící	končící	k2eAgFnSc2d1	končící
v	v	k7c6	v
nominativu	nominativ	k1gInSc6	nominativ
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
si	se	k3xPyFc3	se
udržují	udržovat	k5eAaImIp3nP	udržovat
tento	tento	k3xDgInSc4	tento
tvar	tvar	k1gInSc4	tvar
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
pádech	pád	k1gInPc6	pád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stupňování	stupňování	k1gNnSc1	stupňování
<g/>
:	:	kIx,	:
komparativ	komparativ	k1gInSc4	komparativ
získáme	získat	k5eAaPmIp1nP	získat
většinou	většinou	k6eAd1	většinou
pomocí	pomocí	k7c2	pomocí
měkčení	měkčení	k1gNnSc2	měkčení
poslední	poslední	k2eAgFnSc2d1	poslední
souhlásky	souhláska	k1gFnSc2	souhláska
a	a	k8xC	a
přípony	přípona	k1gFnSc2	přípona
-	-	kIx~	-
<g/>
e.	e.	k?	e.
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
stupňování	stupňování	k1gNnSc1	stupňování
nepravidelné	pravidelný	k2eNgNnSc1d1	nepravidelné
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
maith	maith	k1gInSc1	maith
=	=	kIx~	=
dobrý	dobrý	k2eAgInSc1d1	dobrý
→	→	k?	→
fearr	fearr	k1gInSc1	fearr
<g/>
,	,	kIx,	,
mór	mór	k1gInSc1	mór
=	=	kIx~	=
velký	velký	k2eAgInSc1d1	velký
→	→	k?	→
mó	mó	k?	mó
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnávající	porovnávající	k2eAgFnSc6d1	porovnávající
větě	věta	k1gFnSc6	věta
se	se	k3xPyFc4	se
před	před	k7c4	před
komparativ	komparativ	k1gInSc4	komparativ
klade	klást	k5eAaImIp3nS	klást
částice	částice	k1gFnSc1	částice
níos	níosa	k1gFnPc2	níosa
(	(	kIx(	(
<g/>
is	is	k?	is
í	í	k0	í
an	an	k?	an
Rúis	Rúis	k1gInSc1	Rúis
níos	níos	k1gInSc1	níos
mó	mó	k?	mó
ná	ná	k?	ná
Éirinn	Éirinn	k1gInSc1	Éirinn
=	=	kIx~	=
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgNnSc1d2	veliký
než	než	k8xS	než
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Superlativ	superlativ	k1gInSc1	superlativ
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
stejným	stejný	k2eAgInSc7d1	stejný
tvarem	tvar	k1gInSc7	tvar
jako	jako	k8xS	jako
komparativ	komparativ	k1gInSc4	komparativ
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
podle	podle	k7c2	podle
kontextu	kontext	k1gInSc2	kontext
(	(	kIx(	(
<g/>
is	is	k?	is
í	í	k0	í
an	an	k?	an
Rúis	Rúis	k1gInSc1	Rúis
an	an	k?	an
tír	tír	k?	tír
is	is	k?	is
mó	mó	k?	mó
ar	ar	k1gInSc1	ar
an	an	k?	an
domhan	domhan	k1gInSc1	domhan
=	=	kIx~	=
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnPc4d3	veliký
země	zem	k1gFnPc4	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozlišení	rozlišení	k1gNnSc1	rozlišení
komparativ	komparativ	k1gInSc1	komparativ
vs	vs	k?	vs
<g/>
.	.	kIx.	.
superlativ	superlativ	k1gInSc1	superlativ
je	být	k5eAaImIp3nS	být
ekvivalentní	ekvivalentní	k2eAgNnSc4d1	ekvivalentní
použití	použití	k1gNnSc4	použití
níos	níos	k6eAd1	níos
vs	vs	k?	vs
<g/>
.	.	kIx.	.
is	is	k?	is
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příslovce	příslovka	k1gFnSc3	příslovka
tvoříme	tvořit	k5eAaImIp1nP	tvořit
z	z	k7c2	z
přídavných	přídavný	k2eAgNnPc2d1	přídavné
jmen	jméno	k1gNnPc2	jméno
pomocí	pomocí	k7c2	pomocí
částice	částice	k1gFnSc2	částice
go	go	k?	go
(	(	kIx(	(
<g/>
maith	maith	k1gInSc4	maith
=	=	kIx~	=
dobrý	dobrý	k2eAgInSc4d1	dobrý
<g/>
,	,	kIx,	,
go	go	k?	go
maith	maith	k1gInSc4	maith
=	=	kIx~	=
dobře	dobře	k6eAd1	dobře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zájmena	zájmeno	k1gNnSc2	zájmeno
===	===	k?	===
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základní	základní	k2eAgFnSc2d1	základní
formy	forma	k1gFnSc2	forma
existuje	existovat	k5eAaImIp3nS	existovat
forma	forma	k1gFnSc1	forma
emfatická	emfatický	k2eAgFnSc1d1	emfatická
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
zájmena	zájmeno	k1gNnSc2	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
mají	mít	k5eAaImIp3nP	mít
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
tvar	tvar	k1gInSc4	tvar
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podmětu	podmět	k1gInSc6	podmět
nebo	nebo	k8xC	nebo
v	v	k7c6	v
předmětu	předmět	k1gInSc6	předmět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Navíc	navíc	k6eAd1	navíc
existuje	existovat	k5eAaImIp3nS	existovat
zájmeno	zájmeno	k1gNnSc4	zájmeno
středního	střední	k2eAgInSc2d1	střední
rodu	rod	k1gInSc2	rod
ea	ea	k?	ea
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
používáno	používán	k2eAgNnSc1d1	používáno
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
omezeně	omezeně	k6eAd1	omezeně
ve	v	k7c6	v
větách	věta	k1gFnPc6	věta
typu	typ	k1gInSc2	typ
Is	Is	k1gFnSc1	Is
ea	ea	k?	ea
<g/>
.	.	kIx.	.
=	=	kIx~	=
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přivlastňovací	přivlastňovací	k2eAgNnPc1d1	přivlastňovací
zájmena	zájmeno	k1gNnPc1	zájmeno
jsou	být	k5eAaImIp3nP	být
</s>
</p>
<p>
<s>
mo	mo	k?	mo
=	=	kIx~	=
můj	můj	k1gMnSc1	můj
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
oslabení	oslabení	k1gNnSc4	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
následujícího	následující	k2eAgNnSc2d1	následující
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
následující	následující	k2eAgNnSc4d1	následující
slovo	slovo	k1gNnSc4	slovo
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
redukuje	redukovat	k5eAaBmIp3nS	redukovat
se	se	k3xPyFc4	se
na	na	k7c4	na
m	m	kA	m
<g/>
'	'	kIx"	'
.	.	kIx.	.
</s>
</p>
<p>
<s>
do	do	k7c2	do
=	=	kIx~	=
tvůj	tvůj	k1gMnSc1	tvůj
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
oslabení	oslabení	k1gNnSc4	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
následujícího	následující	k2eAgNnSc2d1	následující
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
následující	následující	k2eAgNnSc4d1	následující
slovo	slovo	k1gNnSc4	slovo
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
redukuje	redukovat	k5eAaBmIp3nS	redukovat
se	se	k3xPyFc4	se
na	na	k7c6	na
d	d	k?	d
<g/>
'	'	kIx"	'
.	.	kIx.	.
</s>
</p>
<p>
<s>
a	a	k8xC	a
=	=	kIx~	=
jeho	jeho	k3xOp3gMnSc1	jeho
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
oslabení	oslabení	k1gNnSc4	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
následujícího	následující	k2eAgNnSc2d1	následující
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
a	a	k8xC	a
=	=	kIx~	=
její	její	k3xOp3gMnPc1	její
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
prefix	prefix	k1gInSc1	prefix
h	h	k?	h
ke	k	k7c3	k
slovům	slovo	k1gNnPc3	slovo
začínajícím	začínající	k2eAgNnPc3d1	začínající
na	na	k7c6	na
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ár	ár	k?	ár
=	=	kIx~	=
náš	náš	k3xOp1gMnSc1	náš
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zákryt	zákryt	k1gInSc4	zákryt
první	první	k4xOgFnSc2	první
souhlásky	souhláska	k1gFnSc2	souhláska
u	u	k7c2	u
následujícího	následující	k2eAgNnSc2d1	následující
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
následující	následující	k2eAgNnSc4d1	následující
slovo	slovo	k1gNnSc4	slovo
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
prefix	prefix	k1gInSc1	prefix
n-	n-	k?	n-
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
bhur	bhur	k1gMnSc1	bhur
=	=	kIx~	=
váš	váš	k3xOp2gMnSc1	váš
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zákryt	zákryt	k1gInSc4	zákryt
první	první	k4xOgFnSc2	první
souhlásky	souhláska	k1gFnSc2	souhláska
u	u	k7c2	u
následujícího	následující	k2eAgNnSc2d1	následující
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
následující	následující	k2eAgNnSc4d1	následující
slovo	slovo	k1gNnSc4	slovo
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
prefix	prefix	k1gInSc1	prefix
n-	n-	k?	n-
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
a	a	k8xC	a
=	=	kIx~	=
jejich	jejich	k3xOp3gMnPc1	jejich
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zákryt	zákryt	k1gInSc4	zákryt
první	první	k4xOgFnSc2	první
souhlásky	souhláska	k1gFnSc2	souhláska
u	u	k7c2	u
následujícího	následující	k2eAgNnSc2d1	následující
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
následující	následující	k2eAgNnSc4d1	následující
slovo	slovo	k1gNnSc4	slovo
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
,	,	kIx,	,
přidává	přidávat	k5eAaImIp3nS	přidávat
prefix	prefix	k1gInSc1	prefix
n-	n-	k?	n-
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
irštině	irština	k1gFnSc6	irština
neexistuje	existovat	k5eNaImIp3nS	existovat
obdoba	obdoba	k1gFnSc1	obdoba
vykání	vykání	k1gNnSc2	vykání
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
osobu	osoba	k1gFnSc4	osoba
singuláru	singulár	k1gInSc2	singulár
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
použito	použít	k5eAaPmNgNnS	použít
zájmeno	zájmeno	k1gNnSc4	zájmeno
tú	tú	k0	tú
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tázací	tázací	k2eAgNnPc1d1	tázací
zájmena	zájmeno	k1gNnPc1	zájmeno
jsou	být	k5eAaImIp3nP	být
cé	cé	k?	cé
<g/>
,	,	kIx,	,
cá	cá	k?	cá
<g/>
,	,	kIx,	,
cad	cad	k?	cad
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
odvozená	odvozený	k2eAgFnSc1d1	odvozená
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
složené	složený	k2eAgFnPc1d1	složená
konstrukce	konstrukce	k1gFnPc1	konstrukce
-	-	kIx~	-
cé	cé	k?	cé
mhéad	mhéad	k1gInSc1	mhéad
=	=	kIx~	=
kolik	kolik	k9	kolik
<g/>
,	,	kIx,	,
cén	cén	k?	cén
áit	áit	k?	áit
=	=	kIx~	=
kde	kde	k6eAd1	kde
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
jaké	jaký	k3yRgNnSc4	jaký
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Číslovky	číslovka	k1gFnSc2	číslovka
===	===	k?	===
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
číslovky	číslovka	k1gFnPc1	číslovka
mají	mít	k5eAaImIp3nP	mít
dvojí	dvojí	k4xRgInSc4	dvojí
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
<g/>
,	,	kIx,	,
když	když	k8xS	když
číslovka	číslovka	k1gFnSc1	číslovka
stojí	stát	k5eAaImIp3nS	stát
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předcházen	předcházet	k5eAaImNgInS	předcházet
částicí	částice	k1gFnSc7	částice
a	a	k8xC	a
(	(	kIx(	(
<g/>
např.	např.	kA	např.
autobus	autobus	k1gInSc1	autobus
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
čtyři	čtyři	k4xCgInPc1	čtyři
=	=	kIx~	=
bus	bus	k1gInSc1	bus
a	a	k8xC	a
ceathair	ceathair	k1gInSc1	ceathair
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
tvar	tvar	k1gInSc1	tvar
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
když	když	k8xS	když
číslovka	číslovka	k1gFnSc1	číslovka
určuje	určovat	k5eAaImIp3nS	určovat
množství	množství	k1gNnSc1	množství
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgInPc1	čtyři
autobusy	autobus	k1gInPc1	autobus
=	=	kIx~	=
ceithre	ceithr	k1gInSc5	ceithr
bhus	bhusa	k1gFnPc2	bhusa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
počítané	počítaný	k2eAgNnSc1d1	počítané
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
singuláru	singulár	k1gInSc6	singulár
a	a	k8xC	a
u	u	k7c2	u
číslovek	číslovka	k1gFnPc2	číslovka
1	[number]	k4	1
-	-	kIx~	-
6	[number]	k4	6
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
první	první	k4xOgFnSc2	první
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
,	,	kIx,	,
u	u	k7c2	u
číslovek	číslovka	k1gFnPc2	číslovka
7	[number]	k4	7
-	-	kIx~	-
10	[number]	k4	10
k	k	k7c3	k
zákrytu	zákryt	k1gInSc3	zákryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslovky	číslovka	k1gFnPc1	číslovka
11-19	[number]	k4	11-19
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgInPc1d1	stejný
jako	jako	k9	jako
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
na	na	k7c4	na
konec	konec	k1gInSc4	konec
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
déag	déag	k1gInSc1	déag
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
15	[number]	k4	15
=	=	kIx~	=
a	a	k8xC	a
cúig	cúig	k1gMnSc1	cúig
déag	déag	k1gMnSc1	déag
<g/>
,	,	kIx,	,
15	[number]	k4	15
autobusů	autobus	k1gInPc2	autobus
=	=	kIx~	=
cúig	cúig	k1gMnSc1	cúig
bhus	bhusa	k1gFnPc2	bhusa
déag	déag	k1gMnSc1	déag
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
autobus	autobus	k1gInSc1	autobus
=	=	kIx~	=
an	an	k?	an
cúigiú	cúigiú	k?	cúigiú
bus	bus	k1gInSc1	bus
déag	déag	k1gInSc1	déag
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvacet	dvacet	k4xCc4	dvacet
je	být	k5eAaImIp3nS	být
fiche	fiche	k1gFnSc1	fiche
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvojím	dvojí	k4xRgInSc7	dvojí
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
první	první	k4xOgInSc1	první
typ	typ	k1gInSc1	typ
základních	základní	k2eAgFnPc2d1	základní
číslovek	číslovka	k1gFnPc2	číslovka
má	mít	k5eAaImIp3nS	mít
slovo	slovo	k1gNnSc1	slovo
fiche	fiche	k1gFnPc2	fiche
před	před	k7c7	před
sebou	se	k3xPyFc7	se
<g/>
:	:	kIx,	:
25	[number]	k4	25
=	=	kIx~	=
fiche	fiche	k1gInSc1	fiche
a	a	k8xC	a
cúig	cúig	k1gInSc1	cúig
</s>
</p>
<p>
<s>
druhý	druhý	k4xOgInSc1	druhý
typ	typ	k1gInSc1	typ
základních	základní	k2eAgFnPc2d1	základní
číslovek	číslovka	k1gFnPc2	číslovka
a	a	k8xC	a
číslovky	číslovka	k1gFnSc2	číslovka
řadové	řadový	k2eAgFnSc2d1	řadová
dostanou	dostat	k5eAaPmIp3nP	dostat
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
is	is	k?	is
fiche	fichat	k5eAaPmIp3nS	fichat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
25	[number]	k4	25
autobusů	autobus	k1gInPc2	autobus
=	=	kIx~	=
cúig	cúig	k1gInSc1	cúig
bus	bus	k1gInSc1	bus
is	is	k?	is
fiche	fiche	k1gInSc1	fiche
<g/>
.	.	kIx.	.
</s>
<s>
Pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
číslovky	číslovka	k1gFnPc4	číslovka
20	[number]	k4	20
a	a	k8xC	a
21	[number]	k4	21
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
autobus	autobus	k1gInSc1	autobus
=	=	kIx~	=
fichiú	fichiú	k?	fichiú
bus	bus	k1gInSc1	bus
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
autobus	autobus	k1gInSc1	autobus
=	=	kIx~	=
an	an	k?	an
t-aonú	tonú	k?	t-aonú
bus	bus	k1gInSc1	bus
is	is	k?	is
fiche	fichat	k5eAaPmIp3nS	fichat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
21	[number]	k4	21
autobusů	autobus	k1gInPc2	autobus
=	=	kIx~	=
bus	bus	k1gInSc1	bus
is	is	k?	is
fiche	fiche	k1gInSc1	fiche
(	(	kIx(	(
<g/>
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
se	se	k3xPyFc4	se
číslovka	číslovka	k1gFnSc1	číslovka
aon	aon	k?	aon
<g/>
)	)	kIx)	)
<g/>
Další	další	k2eAgFnPc1d1	další
desítkové	desítkový	k2eAgFnPc1d1	desítková
číslice	číslice	k1gFnPc1	číslice
jsou	být	k5eAaImIp3nP	být
30	[number]	k4	30
=	=	kIx~	=
tríocha	tríoch	k1gMnSc2	tríoch
<g/>
,	,	kIx,	,
40	[number]	k4	40
=	=	kIx~	=
daichead	daichead	k1gInSc1	daichead
<g/>
,	,	kIx,	,
50	[number]	k4	50
=	=	kIx~	=
caoga	caoga	k1gFnSc1	caoga
<g/>
,	,	kIx,	,
60	[number]	k4	60
=	=	kIx~	=
seasca	seasc	k1gInSc2	seasc
<g/>
,	,	kIx,	,
70	[number]	k4	70
=	=	kIx~	=
seachtó	seachtó	k?	seachtó
<g/>
,	,	kIx,	,
80	[number]	k4	80
=	=	kIx~	=
ochtó	ochtó	k?	ochtó
<g/>
,	,	kIx,	,
90	[number]	k4	90
=	=	kIx~	=
nócha	nócha	k1gFnSc1	nócha
a	a	k8xC	a
fungují	fungovat	k5eAaImIp3nP	fungovat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dvacítka	dvacítka	k1gFnSc1	dvacítka
<g/>
.	.	kIx.	.
</s>
<s>
Sto	sto	k4xCgNnSc1	sto
je	být	k5eAaImIp3nS	být
céad	céada	k1gFnPc2	céada
a	a	k8xC	a
čísla	číslo	k1gNnSc2	číslo
nad	nad	k7c4	nad
sto	sto	k4xCgNnSc4	sto
se	se	k3xPyFc4	se
konstruují	konstruovat	k5eAaImIp3nP	konstruovat
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
např.	např.	kA	např.
263	[number]	k4	263
=	=	kIx~	=
dhá	dhá	k?	dhá
chéad	chéad	k1gInSc1	chéad
seasca	seascus	k1gMnSc2	seascus
a	a	k8xC	a
trí	trí	k?	trí
<g/>
.	.	kIx.	.
</s>
<s>
Tisíc	tisíc	k4xCgInSc4	tisíc
a	a	k8xC	a
milion	milion	k4xCgInSc4	milion
jsou	být	k5eAaImIp3nP	být
míle	míle	k1gFnSc2	míle
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
milliún	milliún	k1gInSc1	milliún
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvar	tvar	k1gInSc1	tvar
daichead	daichead	k1gInSc1	daichead
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
dhá	dhá	k?	dhá
fhichead	fhichead	k1gInSc1	fhichead
<g/>
)	)	kIx)	)
u	u	k7c2	u
čtyřicítky	čtyřicítka	k1gFnSc2	čtyřicítka
je	být	k5eAaImIp3nS	být
upomínkou	upomínka	k1gFnSc7	upomínka
na	na	k7c4	na
starý	starý	k2eAgInSc4d1	starý
keltský	keltský	k2eAgInSc4d1	keltský
dvacítkový	dvacítkový	k2eAgInSc4d1	dvacítkový
způsob	způsob	k1gInSc4	způsob
počítání	počítání	k1gNnSc2	počítání
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
částečně	částečně	k6eAd1	částečně
přežívá	přežívat	k5eAaImIp3nS	přežívat
např.	např.	kA	např.
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
šedesát	šedesát	k4xCc1	šedesát
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
trí	trí	k?	trí
fichid	fichida	k1gFnPc2	fichida
<g/>
,	,	kIx,	,
98	[number]	k4	98
ceithre	ceithr	k1gInSc5	ceithr
fichid	fichid	k1gInSc1	fichid
a	a	k8xC	a
hocht	hocht	k2eAgInSc1d1	hocht
déag	déag	k1gInSc1	déag
</s>
</p>
<p>
<s>
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
srv	srv	k?	srv
<g/>
.	.	kIx.	.
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
quatre-vingt-dix-huit	quatreingtixuit	k1gInSc1	quatre-vingt-dix-huit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zlomky	zlomek	k1gInPc1	zlomek
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
a	a	k8xC	a
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
leath	leath	k1gMnSc1	leath
a	a	k8xC	a
trian	trian	k1gMnSc1	trian
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
příponou	přípona	k1gFnSc7	přípona
-ú	-ú	k?	-ú
(	(	kIx(	(
<g/>
fichiú	fichiú	k?	fichiú
=	=	kIx~	=
dvacetina	dvacetina	k1gFnSc1	dvacetina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
u	u	k7c2	u
složitějších	složitý	k2eAgInPc2d2	složitější
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
a	a	k8xC	a
dó	dó	k?	dó
ar	ar	k1gInSc1	ar
trí	trí	k?	trí
déag	déag	k1gInSc1	déag
=	=	kIx~	=
dvě	dva	k4xCgFnPc1	dva
třináctiny	třináctina	k1gFnPc1	třináctina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovesa	sloveso	k1gNnSc2	sloveso
===	===	k?	===
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
časování	časování	k1gNnSc2	časování
sloves	sloveso	k1gNnPc2	sloveso
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgMnSc1d2	složitější
než	než	k8xS	než
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
tvarem	tvar	k1gInSc7	tvar
slovesa	sloveso	k1gNnSc2	sloveso
je	být	k5eAaImIp3nS	být
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
způsob	způsob	k1gInSc1	způsob
(	(	kIx(	(
<g/>
imperativ	imperativ	k1gInSc1	imperativ
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
formami	forma	k1gFnPc7	forma
jsou	být	k5eAaImIp3nP	být
přítomný	přítomný	k2eAgInSc1d1	přítomný
a	a	k8xC	a
budoucí	budoucí	k2eAgInSc1d1	budoucí
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
préteritum	préteritum	k1gNnSc1	préteritum
<g/>
,	,	kIx,	,
imperfektum	imperfektum	k1gNnSc1	imperfektum
<g/>
,	,	kIx,	,
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
způsob	způsob	k1gInSc1	způsob
a	a	k8xC	a
přítomný	přítomný	k2eAgInSc1d1	přítomný
a	a	k8xC	a
minulý	minulý	k2eAgInSc1d1	minulý
subjunktiv	subjunktiv	k1gInSc1	subjunktiv
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
jsou	být	k5eAaImIp3nP	být
užívány	užívat	k5eAaImNgInP	užívat
zřídka	zřídka	k6eAd1	zřídka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
principu	princip	k1gInSc6	princip
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
kategorií	kategorie	k1gFnPc2	kategorie
existují	existovat	k5eAaImIp3nP	existovat
tvary	tvar	k1gInPc1	tvar
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
vyjádřena	vyjádřen	k2eAgFnSc1d1	vyjádřena
vyjádřena	vyjádřen	k2eAgFnSc1d1	vyjádřena
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
irštině	irština	k1gFnSc6	irština
často	často	k6eAd1	často
analyticky	analyticky	k6eAd1	analyticky
pomocí	pomocí	k7c2	pomocí
zájmena	zájmeno	k1gNnSc2	zájmeno
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
původní	původní	k2eAgFnPc4d1	původní
syntetické	syntetický	k2eAgFnPc4d1	syntetická
formy	forma	k1gFnPc4	forma
zejména	zejména	k9	zejména
druhé	druhý	k4xOgFnPc1	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnPc1	třetí
osoby	osoba	k1gFnPc1	osoba
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
navíc	navíc	k6eAd1	navíc
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
jazykům	jazyk	k1gInPc3	jazyk
existuje	existovat	k5eAaImIp3nS	existovat
tvar	tvar	k1gInSc1	tvar
tzv.	tzv.	kA	tzv.
volného	volný	k2eAgNnSc2d1	volné
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
saorbhriathar	saorbhriathar	k1gInSc1	saorbhriathar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
podmět	podmět	k1gInSc1	podmět
(	(	kIx(	(
<g/>
díoltar	díoltar	k1gInSc1	díoltar
é	é	k0	é
ansin	ansin	k2eAgMnSc1d1	ansin
=	=	kIx~	=
prodávají	prodávat	k5eAaImIp3nP	prodávat
to	ten	k3xDgNnSc1	ten
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
tam	tam	k6eAd1	tam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
odvozená	odvozený	k2eAgFnSc1d1	odvozená
podstatná	podstatný	k2eAgFnSc1d1	podstatná
a	a	k8xC	a
přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
slovesné	slovesný	k2eAgNnSc1d1	slovesné
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
neexistujícího	existující	k2eNgInSc2d1	neexistující
infinitivu	infinitiv	k1gInSc2	infinitiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovesa	sloveso	k1gNnPc1	sloveso
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
tříd	třída	k1gFnPc2	třída
(	(	kIx(	(
<g/>
konjugací	konjugace	k1gFnPc2	konjugace
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
třída	třída	k1gFnSc1	třída
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
především	především	k9	především
jednoslabičnými	jednoslabičný	k2eAgNnPc7d1	jednoslabičné
slovesy	sloveso	k1gNnPc7	sloveso
a	a	k8xC	a
slovesy	sloveso	k1gNnPc7	sloveso
končícími	končící	k2eAgNnPc7d1	končící
na	na	k7c4	na
-áil	-áil	k1gInSc4	-áil
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhou	druhý	k4xOgFnSc4	druhý
tvoří	tvořit	k5eAaImIp3nS	tvořit
slovesa	sloveso	k1gNnPc1	sloveso
víceslabičná	víceslabičný	k2eAgFnSc1d1	víceslabičná
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
podstatný	podstatný	k2eAgInSc1d1	podstatný
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
formování	formování	k1gNnSc6	formování
budoucího	budoucí	k2eAgInSc2d1	budoucí
času	čas	k1gInSc2	čas
a	a	k8xC	a
podmiňovacího	podmiňovací	k2eAgInSc2d1	podmiňovací
způsobu	způsob	k1gInSc2	způsob
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
u	u	k7c2	u
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
stojí	stát	k5eAaImIp3nP	stát
koncovky	koncovka	k1gFnPc1	koncovka
-faidh	aidha	k1gFnPc2	-faidha
<g/>
,	,	kIx,	,
-far	ara	k1gFnPc2	-fara
<g/>
,	,	kIx,	,
-fainn	ainna	k1gFnPc2	-fainna
<g/>
,	,	kIx,	,
-feá	eat	k5eAaBmIp3nS	-feat
apod.	apod.	kA	apod.
místo	místo	k7c2	místo
koncovek	koncovka	k1gFnPc2	koncovka
-óidh	-óidha	k1gFnPc2	-óidha
<g/>
,	,	kIx,	,
-ófar	-ófara	k1gFnPc2	-ófara
<g/>
,	,	kIx,	,
-óinn	-óinna	k1gFnPc2	-óinna
<g/>
,	,	kIx,	,
-ófá	-ófat	k5eAaBmIp3nS	-ófat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvary	tvar	k1gInPc1	tvar
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
de	de	k?	de
facto	facto	k1gNnSc4	facto
z	z	k7c2	z
imperativu	imperativ	k1gInSc2	imperativ
2	[number]	k4	2
<g/>
.	.	kIx.	.
osoby	osoba	k1gFnPc4	osoba
<g/>
)	)	kIx)	)
tvořeny	tvořen	k2eAgFnPc4d1	tvořena
koncovkou	koncovka	k1gFnSc7	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
ann	ann	k?	ann
+	+	kIx~	+
příslušným	příslušný	k2eAgNnSc7d1	příslušné
zájmenem	zájmeno	k1gNnSc7	zájmeno
či	či	k8xC	či
podmětem	podmět	k1gInSc7	podmět
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
osoba	osoba	k1gFnSc1	osoba
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
koncovka	koncovka	k1gFnSc1	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
imid	imid	k1gInSc1	imid
<g/>
,	,	kIx,	,
zájmeno	zájmeno	k1gNnSc1	zájmeno
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
za	za	k7c4	za
sloveso	sloveso	k1gNnSc4	sloveso
neklade	klást	k5eNaImIp3nS	klást
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotném	jednotný	k2eAgNnSc6d1	jednotné
čísle	číslo	k1gNnSc6	číslo
první	první	k4xOgFnSc2	první
osoby	osoba	k1gFnSc2	osoba
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
jak	jak	k8xC	jak
analytická	analytický	k2eAgFnSc1d1	analytická
forma	forma	k1gFnSc1	forma
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
ann	ann	k?	ann
mé	můj	k3xOp1gNnSc1	můj
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
syntetická	syntetický	k2eAgFnSc1d1	syntetická
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
ím	ím	k?	ím
<g/>
.	.	kIx.	.
</s>
<s>
Volná	volný	k2eAgFnSc1d1	volná
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
příponou	přípona	k1gFnSc7	přípona
-t	-t	k?	-t
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
ar	ar	k1gInSc1	ar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
díol	díolit	k5eAaPmRp2nS	díolit
=	=	kIx~	=
prodávej	prodávat	k5eAaImRp2nS	prodávat
<g/>
,	,	kIx,	,
díolaim	díolaim	k6eAd1	díolaim
=	=	kIx~	=
prodávám	prodávat	k5eAaImIp1nS	prodávat
<g/>
,	,	kIx,	,
díolann	díolann	k1gInSc1	díolann
tú	tú	k0	tú
=	=	kIx~	=
prodáváš	prodávat	k5eAaImIp2nS	prodávat
<g/>
,	,	kIx,	,
díolann	díolann	k1gMnSc1	díolann
siad	siad	k1gMnSc1	siad
=	=	kIx~	=
prodávají	prodávat	k5eAaImIp3nP	prodávat
<g/>
,	,	kIx,	,
díoltear	díoltear	k1gInSc1	díoltear
=	=	kIx~	=
prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přítomný	přítomný	k2eAgInSc1d1	přítomný
čas	čas	k1gInSc1	čas
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
především	především	k9	především
děj	děj	k1gInSc4	děj
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
opakující	opakující	k2eAgNnPc1d1	opakující
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
přítomný	přítomný	k2eAgInSc1d1	přítomný
prostý	prostý	k2eAgInSc1d1	prostý
čas	čas	k1gInSc1	čas
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
probíhajícího	probíhající	k2eAgInSc2d1	probíhající
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
konstrukce	konstrukce	k1gFnSc1	konstrukce
s	s	k7c7	s
užitím	užití	k1gNnSc7	užití
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
tá	tá	k0	tá
<g/>
)	)	kIx)	)
a	a	k8xC	a
slovesného	slovesný	k2eAgNnSc2d1	slovesné
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
táim	táim	k6eAd1	táim
ag	ag	k?	ag
díoladh	díoladh	k1gInSc1	díoladh
=	=	kIx~	=
(	(	kIx(	(
<g/>
právě	právě	k9	právě
teď	teď	k6eAd1	teď
<g/>
)	)	kIx)	)
prodávám	prodávat	k5eAaImIp1nS	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Slovesné	slovesný	k2eAgNnSc1d1	slovesné
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
typicky	typicky	k6eAd1	typicky
příponou	přípona	k1gFnSc7	přípona
-adh	dha	k1gFnPc2	-adha
u	u	k7c2	u
první	první	k4xOgFnSc2	první
a	a	k8xC	a
-ú	-ú	k?	-ú
u	u	k7c2	u
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepravidelné	pravidelný	k2eNgNnSc1d1	nepravidelné
tvoření	tvoření	k1gNnSc1	tvoření
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgNnSc1d1	časté
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
slovesného	slovesný	k2eAgNnSc2d1	slovesné
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
skončený	skončený	k2eAgInSc1d1	skončený
děj	děj	k1gInSc1	děj
(	(	kIx(	(
<g/>
perfektum	perfektum	k1gNnSc1	perfektum
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
tá	tá	k0	tá
díolta	díolta	k1gMnSc1	díolta
agam	agam	k1gInSc1	agam
=	=	kIx~	=
prodal	prodat	k5eAaPmAgInS	prodat
jsem	být	k5eAaImIp1nS	být
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
u	u	k7c2	u
mě	já	k3xPp1nSc2	já
je	být	k5eAaImIp3nS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
doslova	doslova	k6eAd1	doslova
mám	mít	k5eAaImIp1nS	mít
prodáno	prodat	k5eAaPmNgNnS	prodat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovesné	slovesný	k2eAgNnSc1d1	slovesné
přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
většinou	většinou	k6eAd1	většinou
příponami	přípona	k1gFnPc7	přípona
-ta	a	k?	-ta
<g/>
,	,	kIx,	,
-tha	ha	k1gMnSc1	-tha
<g/>
,	,	kIx,	,
-te	e	k?	-te
nebo	nebo	k8xC	nebo
-the	h	k1gFnSc2	-th
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
tvoříme	tvořit	k5eAaImIp1nP	tvořit
pomocí	pomocí	k7c2	pomocí
přípony	přípona	k1gFnSc2	přípona
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
imid	imid	k1gInSc1	imid
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
os	osa	k1gFnPc2	osa
<g/>
.	.	kIx.	.
plurálu	plurál	k1gInSc2	plurál
<g/>
,	,	kIx,	,
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
ar	ar	k1gInSc1	ar
u	u	k7c2	u
volného	volný	k2eAgInSc2d1	volný
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
idh	idh	k?	idh
+	+	kIx~	+
podmět	podmět	k1gInSc1	podmět
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
písmene	písmeno	k1gNnSc2	písmeno
f	f	k?	f
ve	v	k7c6	v
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
imid	imid	k1gInSc1	imid
a	a	k8xC	a
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
idh	idh	k?	idh
je	být	k5eAaImIp3nS	být
nepravidelná	pravidelný	k2eNgFnSc1d1	nepravidelná
<g/>
,	,	kIx,	,
čteme	číst	k5eAaImIp1nP	číst
[	[	kIx(	[
<g/>
-himiď	imidit	k5eAaPmRp2nS	-himidit
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
-hí	í	k?	-hí
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
ar	ar	k1gInSc4	ar
čteme	číst	k5eAaImIp1nP	číst
[	[	kIx(	[
<g/>
-fə	-fə	k?	-fə
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Př	př	kA	př
<g/>
.	.	kIx.	.
díolfaidh	díolfaidh	k1gMnSc1	díolfaidh
mé	můj	k3xOp1gNnSc4	můj
=	=	kIx~	=
budu	být	k5eAaImBp1nS	být
prodávat	prodávat	k5eAaImF	prodávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
události	událost	k1gFnPc1	událost
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
popisuje	popisovat	k5eAaImIp3nS	popisovat
préteritum	préteritum	k1gNnSc1	préteritum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
sloveso	sloveso	k1gNnSc4	sloveso
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
tvaru	tvar	k1gInSc6	tvar
následováno	následován	k2eAgNnSc1d1	následováno
podmětem	podmět	k1gInSc7	podmět
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
1	[number]	k4	1
<g/>
.	.	kIx.	.
osoby	osoba	k1gFnPc1	osoba
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
amar	amara	k1gFnPc2	amara
a	a	k8xC	a
volného	volný	k2eAgInSc2d1	volný
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
koncovkou	koncovka	k1gFnSc7	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
adh	adh	k?	adh
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
<g/>
,	,	kIx,	,
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
(	(	kIx(	(
<g/>
dhíol	dhíol	k1gInSc1	dhíol
mé	můj	k3xOp1gFnSc2	můj
=	=	kIx~	=
prodal	prodat	k5eAaPmAgInS	prodat
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
dhíolamar	dhíolamar	k1gInSc1	dhíolamar
=	=	kIx~	=
prodali	prodat	k5eAaPmAgMnP	prodat
jsme	být	k5eAaImIp1nP	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
začínajících	začínající	k2eAgNnPc2d1	začínající
na	na	k7c4	na
samohlásku	samohláska	k1gFnSc4	samohláska
se	se	k3xPyFc4	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
částice	částice	k1gFnSc1	částice
do	do	k7c2	do
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
d	d	k?	d
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
ól	ól	k?	ól
sé	sé	k?	sé
=	=	kIx~	=
pil	pila	k1gFnPc2	pila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
minulý	minulý	k2eAgInSc1d1	minulý
děl	dít	k5eAaBmAgInS	dít
častěji	často	k6eAd2	často
opakoval	opakovat	k5eAaImAgInS	opakovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
použití	použití	k1gNnSc2	použití
imperfekta	imperfektum	k1gNnSc2	imperfektum
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgFnPc1d1	počáteční
mutace	mutace	k1gFnPc1	mutace
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xC	jako
v	v	k7c6	v
préteritu	préteritum	k1gNnSc6	préteritum
<g/>
.	.	kIx.	.
</s>
<s>
Koncovky	koncovka	k1gFnPc1	koncovka
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
řádce	řádka	k1gFnSc6	řádka
podoba	podoba	k1gFnSc1	podoba
pro	pro	k7c4	pro
sloveso	sloveso	k1gNnSc4	sloveso
díol	díola	k1gFnPc2	díola
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tvary	tvar	k1gInPc1	tvar
podmiňovacího	podmiňovací	k2eAgInSc2d1	podmiňovací
způsobu	způsob	k1gInSc2	způsob
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgInPc1d1	stejný
jako	jako	k8xC	jako
tvary	tvar	k1gInPc1	tvar
imperfekta	imperfektum	k1gNnSc2	imperfektum
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
-f-	-f-	k?	-f-
(	(	kIx(	(
<g/>
vyslovované	vyslovovaný	k2eAgFnSc2d1	vyslovovaná
[	[	kIx(	[
<g/>
h	h	k?	h
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
před	před	k7c7	před
koncovkou	koncovka	k1gFnSc7	koncovka
(	(	kIx(	(
<g/>
u	u	k7c2	u
sloves	sloveso	k1gNnPc2	sloveso
2	[number]	k4	2
<g/>
.	.	kIx.	.
konjugace	konjugace	k1gFnSc2	konjugace
místo	místo	k7c2	místo
f	f	k?	f
stojí	stát	k5eAaImIp3nS	stát
ó	ó	k0	ó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
-t	-t	k?	-t
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
á	á	k0	á
a	a	k8xC	a
-t	-t	k?	-t
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
í	í	k0	í
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
e	e	k0	e
<g/>
)	)	kIx)	)
<g/>
á	á	k0	á
a	a	k8xC	a
-f	-f	k?	-f
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
í	í	k0	í
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
však	však	k9	však
už	už	k6eAd1	už
čtené	čtený	k2eAgNnSc1d1	čtené
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
například	například	k6eAd1	například
dhíolainn	dhíolainn	k1gNnSc1	dhíolainn
=	=	kIx~	=
prodá	prodat	k5eAaPmIp3nS	prodat
<g/>
(	(	kIx(	(
<g/>
vá	vá	k?	vá
<g/>
)	)	kIx)	)
<g/>
val	val	k1gInSc1	val
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
dhíolfainn	dhíolfainn	k1gInSc1	dhíolfainn
=	=	kIx~	=
prodával	prodávat	k5eAaImAgInS	prodávat
bych	by	kYmCp1nS	by
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Imperativ	imperativ	k1gInSc1	imperativ
2	[number]	k4	2
<g/>
.	.	kIx.	.
osoby	osoba	k1gFnPc1	osoba
množ	množit	k5eAaImRp2nS	množit
<g/>
.	.	kIx.	.
čísla	číslo	k1gNnSc2	číslo
má	mít	k5eAaImIp3nS	mít
koncovku	koncovka	k1gFnSc4	koncovka
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
igí	igí	k?	igí
(	(	kIx(	(
<g/>
prodejte	prodat	k5eAaPmRp2nP	prodat
=	=	kIx~	=
díolaigí	díolaigí	k1gNnSc3	díolaigí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
dvě	dva	k4xCgFnPc4	dva
třídy	třída	k1gFnPc4	třída
pravidelných	pravidelný	k2eAgNnPc2d1	pravidelné
sloves	sloveso	k1gNnPc2	sloveso
existuje	existovat	k5eAaImIp3nS	existovat
jedenáct	jedenáct	k4xCc1	jedenáct
sloves	sloveso	k1gNnPc2	sloveso
nepravidelných	pravidelný	k2eNgInPc2d1	nepravidelný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bí	bí	k?	bí
=	=	kIx~	=
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
déan	déan	k1gInSc1	déan
=	=	kIx~	=
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
clois	clois	k1gInSc1	clois
=	=	kIx~	=
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
feic	feic	k6eAd1	feic
=	=	kIx~	=
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
beir	beir	k1gInSc1	beir
=	=	kIx~	=
držet	držet	k5eAaImF	držet
<g/>
,	,	kIx,	,
tar	tar	k?	tar
=	=	kIx~	=
přijít	přijít	k5eAaPmF	přijít
<g/>
,	,	kIx,	,
ith	ith	k?	ith
=	=	kIx~	=
jíst	jíst	k5eAaImF	jíst
<g/>
,	,	kIx,	,
tabhair	tabhair	k1gInSc1	tabhair
=	=	kIx~	=
dát	dát	k5eAaPmF	dát
<g/>
,	,	kIx,	,
faigh	faigh	k1gInSc1	faigh
=	=	kIx~	=
vzít	vzít	k5eAaPmF	vzít
<g/>
,	,	kIx,	,
téigh	téigh	k1gInSc1	téigh
=	=	kIx~	=
jít	jít	k5eAaImF	jít
<g/>
,	,	kIx,	,
abair	abair	k1gInSc1	abair
=	=	kIx~	=
říct	říct	k5eAaPmF	říct
<g/>
.	.	kIx.	.
</s>
<s>
Nepravidelnost	nepravidelnost	k1gFnSc1	nepravidelnost
se	se	k3xPyFc4	se
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
u	u	k7c2	u
určování	určování	k1gNnSc2	určování
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
sloveso	sloveso	k1gNnSc1	sloveso
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
bí	bí	k?	bí
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
tvary	tvar	k1gInPc4	tvar
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
odvozené	odvozený	k2eAgNnSc1d1	odvozené
pravidelně	pravidelně	k6eAd1	pravidelně
od	od	k7c2	od
tvaru	tvar	k1gInSc2	tvar
tá	tá	k0	tá
(	(	kIx(	(
<g/>
tudíž	tudíž	k8xC	tudíž
táimid	táimid	k1gInSc4	táimid
=	=	kIx~	=
jsme	být	k5eAaImIp1nP	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
minulý	minulý	k2eAgInSc4d1	minulý
čas	čas	k1gInSc4	čas
a	a	k8xC	a
imperfektum	imperfektum	k1gNnSc4	imperfektum
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgInP	odvodit
od	od	k7c2	od
tvaru	tvar	k1gInSc2	tvar
bí	bí	k?	bí
(	(	kIx(	(
<g/>
bhí	bhí	k?	bhí
muid	muid	k1gInSc1	muid
=	=	kIx~	=
byli	být	k5eAaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgInPc1d1	budoucí
a	a	k8xC	a
podmiňovací	podmiňovací	k2eAgInPc1d1	podmiňovací
tvary	tvar	k1gInPc1	tvar
začínají	začínat	k5eAaImIp3nP	začínat
na	na	k7c6	na
bei-	bei-	k?	bei-
<g/>
:	:	kIx,	:
beidh	beidh	k1gMnSc1	beidh
mé	můj	k3xOp1gNnSc4	můj
=	=	kIx~	=
budu	být	k5eAaImBp1nS	být
<g/>
,	,	kIx,	,
bheinn	bheinn	k1gInSc1	bheinn
=	=	kIx~	=
byl	být	k5eAaImAgInS	být
bych	by	kYmCp1nS	by
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
otázce	otázka	k1gFnSc6	otázka
se	se	k3xPyFc4	se
před	před	k7c4	před
sloveso	sloveso	k1gNnSc4	sloveso
klade	klást	k5eAaImIp3nS	klást
částice	částice	k1gFnSc1	částice
an	an	k?	an
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
způsobí	způsobit	k5eAaPmIp3nS	způsobit
zákryt	zákryt	k1gInSc4	zákryt
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
an	an	k?	an
ndíolann	ndíolann	k1gInSc1	ndíolann
tú	tú	k0	tú
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
prodáváš	prodávat	k5eAaImIp2nS	prodávat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
otázky	otázka	k1gFnSc2	otázka
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
stojí	stát	k5eAaImIp3nS	stát
částice	částice	k1gFnSc1	částice
ar	ar	k1gInSc4	ar
a	a	k8xC	a
oslabení	oslabení	k1gNnSc4	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
(	(	kIx(	(
<g/>
ar	ar	k1gInSc1	ar
dhíol	dhíola	k1gFnPc2	dhíola
tú	tú	k0	tú
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
prodals	prodalsa	k1gFnPc2	prodalsa
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
V	v	k7c6	v
případě	případ	k1gInSc6	případ
negace	negace	k1gFnSc2	negace
jsou	být	k5eAaImIp3nP	být
příslušné	příslušný	k2eAgFnPc1d1	příslušná
částice	částice	k1gFnPc1	částice
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
níor	níora	k1gFnPc2	níora
s	s	k7c7	s
oslabením	oslabení	k1gNnSc7	oslabení
první	první	k4xOgFnSc2	první
hlásky	hláska	k1gFnSc2	hláska
(	(	kIx(	(
<g/>
níor	níor	k1gMnSc1	níor
dhíolamar	dhíolamar	k1gMnSc1	dhíolamar
=	=	kIx~	=
neprodali	prodat	k5eNaPmAgMnP	prodat
jsme	být	k5eAaImIp1nP	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
negativní	negativní	k2eAgFnSc2d1	negativní
otázky	otázka	k1gFnSc2	otázka
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
před	před	k7c4	před
sloveso	sloveso	k1gNnSc4	sloveso
nach	nach	k1gInSc1	nach
nebo	nebo	k8xC	nebo
nár	nár	k?	nár
(	(	kIx(	(
<g/>
nach	nach	k1gInSc1	nach
ndíolann	ndíolann	k1gInSc1	ndíolann
sé	sé	k?	sé
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
neprodává	prodávat	k5eNaImIp3nS	prodávat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
nár	nár	k?	nár
dhíol	dhíol	k1gInSc1	dhíol
sé	sé	k?	sé
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
neprodal	prodat	k5eNaPmAgInS	prodat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
nepravidelná	pravidelný	k2eNgNnPc1d1	nepravidelné
slovesa	sloveso	k1gNnPc1	sloveso
mají	mít	k5eAaImIp3nP	mít
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
závislou	závislý	k2eAgFnSc4d1	závislá
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
užita	užít	k5eAaPmNgFnS	užít
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
a	a	k8xC	a
negacích	negace	k1gFnPc6	negace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgNnPc2	tento
sloves	sloveso	k1gNnPc2	sloveso
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
použity	použít	k5eAaPmNgFnP	použít
přítomné	přítomný	k2eAgFnPc1d1	přítomná
tázací	tázací	k2eAgFnPc1d1	tázací
a	a	k8xC	a
negační	negační	k2eAgFnPc1d1	negační
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
slovesa	sloveso	k1gNnSc2	sloveso
být	být	k5eAaImF	být
je	on	k3xPp3gInPc4	on
tvar	tvar	k1gInSc1	tvar
fuil	fuinout	k5eAaPmAgInS	fuinout
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
a	a	k8xC	a
raibh	raibh	k1gInSc4	raibh
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
an	an	k?	an
bhfuil	bhfuil	k1gInSc1	bhfuil
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
je	být	k5eAaImIp3nS	být
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
an	an	k?	an
raibh	raibh	k1gInSc1	raibh
<g/>
?	?	kIx.	?
</s>
<s>
=	=	kIx~	=
byl	být	k5eAaImAgInS	být
<g/>
?	?	kIx.	?
</s>
<s>
Ní	on	k3xPp3gFnSc7	on
fhuil	fhuit	k5eAaBmAgMnS	fhuit
(	(	kIx(	(
<g/>
=	=	kIx~	=
není	být	k5eNaImIp3nS	být
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zkráceno	zkrátit	k5eAaPmNgNnS	zkrátit
na	na	k7c6	na
níl	níl	k?	níl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
irštině	irština	k1gFnSc6	irština
neexistuje	existovat	k5eNaImIp3nS	existovat
sloveso	sloveso	k1gNnSc4	sloveso
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
se	s	k7c7	s
slovesem	sloveso	k1gNnSc7	sloveso
být	být	k5eAaImF	být
a	a	k8xC	a
předložkovou	předložkový	k2eAgFnSc7d1	předložková
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
tá	tá	k0	tá
carr	carr	k1gMnSc1	carr
agam	agam	k1gInSc1	agam
=	=	kIx~	=
mám	mít	k5eAaImIp1nS	mít
auto	auto	k1gNnSc4	auto
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
auto	auto	k1gNnSc1	auto
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mě	já	k3xPp1nSc2	já
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spona	spona	k1gFnSc1	spona
===	===	k?	===
</s>
</p>
<p>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
bí	bí	k?	bí
není	být	k5eNaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
jako	jako	k9	jako
spona	spona	k1gFnSc1	spona
ve	v	k7c6	v
jmenném	jmenný	k2eAgInSc6d1	jmenný
přísudku	přísudek	k1gInSc6	přísudek
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
spona	spona	k1gFnSc1	spona
(	(	kIx(	(
<g/>
kopula	kopula	k1gFnSc1	kopula
<g/>
)	)	kIx)	)
is	is	k?	is
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
případ	případ	k1gInSc1	případ
hlavně	hlavně	k6eAd1	hlavně
přiřazovacích	přiřazovací	k2eAgFnPc2d1	přiřazovací
vět	věta	k1gFnPc2	věta
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
is	is	k?	is
mamach	mamach	k1gInSc1	mamach
an	an	k?	an
muc	muc	k?	muc
=	=	kIx~	=
prase	prase	k1gNnSc1	prase
je	být	k5eAaImIp3nS	být
savec	savec	k1gMnSc1	savec
<g/>
.	.	kIx.	.
</s>
<s>
Přísudek	přísudek	k1gInSc1	přísudek
se	se	k3xPyFc4	se
sponou	spona	k1gFnSc7	spona
užijeme	užít	k5eAaPmIp1nP	užít
i	i	k9	i
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
příslušnosti	příslušnost	k1gFnSc2	příslušnost
ke	k	k7c3	k
kategorii	kategorie	k1gFnSc3	kategorie
(	(	kIx(	(
<g/>
is	is	k?	is
múinteoir	múinteoir	k1gInSc1	múinteoir
é	é	k0	é
=	=	kIx~	=
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
učitel	učitel	k1gMnSc1	učitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulém	minulý	k2eAgInSc6d1	minulý
čase	čas	k1gInSc6	čas
má	mít	k5eAaImIp3nS	mít
spona	spona	k1gFnSc1	spona
tvar	tvar	k1gInSc1	tvar
ba	ba	k9	ba
(	(	kIx(	(
<g/>
ba	ba	k9	ba
múinteoir	múinteoir	k1gInSc1	múinteoir
é	é	k0	é
=	=	kIx~	=
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
učitel	učitel	k1gMnSc1	učitel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozor	pozor	k1gInSc1	pozor
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dát	dát	k5eAaPmF	dát
na	na	k7c4	na
pořadí	pořadí	k1gNnSc4	pořadí
slov	slovo	k1gNnPc2	slovo
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
používáme	používat	k5eAaImIp1nP	používat
sloveso	sloveso	k1gNnSc4	sloveso
bí	bí	k?	bí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
toto	tento	k3xDgNnSc4	tento
sloveso	sloveso	k1gNnSc4	sloveso
přísudkem	přísudek	k1gInSc7	přísudek
<g/>
,	,	kIx,	,
a	a	k8xC	a
podmět	podmět	k1gInSc1	podmět
je	být	k5eAaImIp3nS	být
hned	hned	k6eAd1	hned
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
následován	následován	k2eAgInSc1d1	následován
dalšími	další	k2eAgInPc7d1	další
větnými	větný	k2eAgInPc7d1	větný
členy	člen	k1gInPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
při	při	k7c6	při
užití	užití	k1gNnSc6	užití
spony	spona	k1gFnSc2	spona
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
sponou	spona	k1gFnSc7	spona
součástí	součást	k1gFnPc2	součást
přísudku	přísudek	k1gInSc2	přísudek
a	a	k8xC	a
podmět	podmět	k1gInSc1	podmět
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc3d2	vyšší
pozici	pozice	k1gFnSc3	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sponě	spona	k1gFnSc6	spona
nesmí	smět	k5eNaImIp3nS	smět
následovat	následovat	k5eAaImF	následovat
člen	člen	k1gMnSc1	člen
nebo	nebo	k8xC	nebo
určité	určitý	k2eAgNnSc4d1	určité
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
i	i	k8xC	i
vlastní	vlastní	k2eAgNnSc1d1	vlastní
jméno	jméno	k1gNnSc1	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
přiřadit	přiřadit	k5eAaPmF	přiřadit
něčemu	něco	k3yInSc3	něco
určité	určitý	k2eAgNnSc4d1	určité
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
mezi	mezi	k7c4	mezi
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
sponu	spona	k1gFnSc4	spona
vložit	vložit	k5eAaPmF	vložit
ještě	ještě	k9	ještě
osobní	osobní	k2eAgNnSc4d1	osobní
zájmeno	zájmeno	k1gNnSc4	zájmeno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
is	is	k?	is
é	é	k0	é
an	an	k?	an
t-uachtarán	tachtarán	k2eAgInSc1d1	t-uachtarán
é	é	k0	é
=	=	kIx~	=
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
)	)	kIx)	)
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předložky	předložka	k1gFnSc2	předložka
===	===	k?	===
</s>
</p>
<p>
<s>
Role	role	k1gFnSc1	role
předložek	předložka	k1gFnPc2	předložka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
důležitější	důležitý	k2eAgMnSc1d2	důležitější
než	než	k8xS	než
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
především	především	k9	především
díky	díky	k7c3	díky
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
předložkových	předložkový	k2eAgFnPc2d1	předložková
vazeb	vazba	k1gFnPc2	vazba
nahrazujících	nahrazující	k2eAgFnPc2d1	nahrazující
plnohodnotná	plnohodnotný	k2eAgNnPc4d1	plnohodnotné
slovesa	sloveso	k1gNnSc2	sloveso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bí	bí	k?	bí
ag	ag	k?	ag
=	=	kIx~	=
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
bí	bí	k?	bí
ó	ó	k0	ó
=	=	kIx~	=
chtít	chtít	k5eAaImF	chtít
<g/>
,	,	kIx,	,
bí	bí	k?	bí
ar	ar	k1gInSc1	ar
=	=	kIx~	=
muset	muset	k5eAaImF	muset
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předložky	předložka	k1gFnPc1	předložka
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nP	pojit
většinou	většina	k1gFnSc7	většina
se	s	k7c7	s
zaniklým	zaniklý	k2eAgInSc7d1	zaniklý
akuzativem	akuzativ	k1gInSc7	akuzativ
či	či	k8xC	či
dativem	dativ	k1gInSc7	dativ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
následující	následující	k2eAgNnSc1d1	následující
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
tvaru	tvar	k1gInSc6	tvar
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
počátečním	počáteční	k2eAgFnPc3d1	počáteční
mutacím	mutace	k1gFnPc3	mutace
<g/>
.	.	kIx.	.
</s>
<s>
Složené	složený	k2eAgFnPc4d1	složená
předložky	předložka	k1gFnPc4	předložka
a	a	k8xC	a
předložky	předložka	k1gFnPc4	předložka
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
z	z	k7c2	z
podstatných	podstatný	k2eAgFnPc2d1	podstatná
jmen	jméno	k1gNnPc2	jméno
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
pojí	pojit	k5eAaImIp3nS	pojit
s	s	k7c7	s
genitivem	genitiv	k1gInSc7	genitiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
předložky	předložka	k1gFnPc1	předložka
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
přibližný	přibližný	k2eAgInSc1d1	přibližný
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
kontextu	kontext	k1gInSc6	kontext
<g/>
)	)	kIx)	)
ag	ag	k?	ag
=	=	kIx~	=
u	u	k7c2	u
<g/>
;	;	kIx,	;
ar	ar	k1gInSc1	ar
=	=	kIx~	=
na	na	k7c6	na
<g/>
;	;	kIx,	;
i	i	k9	i
<g/>
,	,	kIx,	,
in	in	k?	in
=	=	kIx~	=
v	v	k7c6	v
<g/>
;	;	kIx,	;
ó	ó	k0	ó
=	=	kIx~	=
od	od	k7c2	od
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
;	;	kIx,	;
de	de	k?	de
=	=	kIx~	=
od	od	k7c2	od
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
;	;	kIx,	;
do	do	k7c2	do
=	=	kIx~	=
k	k	k7c3	k
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
<g/>
;	;	kIx,	;
idir	idir	k1gInSc4	idir
=	=	kIx~	=
mezi	mezi	k7c7	mezi
<g/>
;	;	kIx,	;
roimh	roimh	k1gMnSc1	roimh
=	=	kIx~	=
před	před	k7c7	před
<g/>
;	;	kIx,	;
le	le	k?	le
=	=	kIx~	=
s	s	k7c7	s
<g/>
;	;	kIx,	;
thar	thar	k1gMnSc1	thar
=	=	kIx~	=
přes	přes	k7c4	přes
<g/>
;	;	kIx,	;
go	go	k?	go
=	=	kIx~	=
do	do	k7c2	do
<g/>
;	;	kIx,	;
chuig	chuiga	k1gFnPc2	chuiga
=	=	kIx~	=
k	k	k7c3	k
<g/>
;	;	kIx,	;
faoi	fao	k1gFnSc3	fao
=	=	kIx~	=
o	o	k0	o
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
základní	základní	k2eAgFnPc1d1	základní
předložky	předložka	k1gFnPc1	předložka
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
skloňovatelné	skloňovatelný	k2eAgFnPc1d1	skloňovatelný
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pojí	pojit	k5eAaImIp3nP	pojit
se	se	k3xPyFc4	se
s	s	k7c7	s
osobními	osobní	k2eAgNnPc7d1	osobní
zájmeny	zájmeno	k1gNnPc7	zájmeno
do	do	k7c2	do
předložkových	předložkový	k2eAgNnPc2d1	předložkové
zájmen	zájmeno	k1gNnPc2	zájmeno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
předložku	předložka	k1gFnSc4	předložka
ag	ag	k?	ag
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
agam	agam	k6eAd1	agam
=	=	kIx~	=
u	u	k7c2	u
mě	já	k3xPp1nSc2	já
<g/>
,	,	kIx,	,
agat	agata	k1gFnPc2	agata
=	=	kIx~	=
u	u	k7c2	u
tebe	ty	k3xPp2nSc2	ty
<g/>
,	,	kIx,	,
aige	aige	k1gFnPc6	aige
=	=	kIx~	=
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
aici	aici	k6eAd1	aici
=	=	kIx~	=
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
againn	againn	k1gNnSc1	againn
=	=	kIx~	=
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
agaibh	agaibha	k1gFnPc2	agaibha
=	=	kIx~	=
u	u	k7c2	u
vás	vy	k3xPp2nPc2	vy
<g/>
,	,	kIx,	,
acu	acu	k?	acu
=	=	kIx~	=
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
taky	taky	k6eAd1	taky
předložky	předložka	k1gFnPc1	předložka
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
členem	člen	k1gMnSc7	člen
<g/>
,	,	kIx,	,
např.	např.	kA	např.
i	i	k9	i
+	+	kIx~	+
an	an	k?	an
→	→	k?	→
sa	sa	k?	sa
<g/>
,	,	kIx,	,
le	le	k?	le
+	+	kIx~	+
na	na	k7c6	na
→	→	k?	→
lena	lena	k1gMnSc1	lena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spojky	spojka	k1gFnPc1	spojka
a	a	k8xC	a
částice	částice	k1gFnPc1	částice
===	===	k?	===
</s>
</p>
<p>
<s>
Nejběžnější	běžný	k2eAgFnPc1d3	nejběžnější
spojky	spojka	k1gFnPc1	spojka
jsou	být	k5eAaImIp3nP	být
agus	agus	k6eAd1	agus
=	=	kIx~	=
a	a	k8xC	a
<g/>
;	;	kIx,	;
nó	nó	k0	nó
=	=	kIx~	=
nebo	nebo	k8xC	nebo
<g/>
;	;	kIx,	;
mar	mar	k?	mar
=	=	kIx~	=
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
=	=	kIx~	=
když	když	k8xS	když
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
<g/>
;	;	kIx,	;
dá	dát	k5eAaPmIp3nS	dát
=	=	kIx~	=
pokud	pokud	k8xS	pokud
<g/>
;	;	kIx,	;
nuair	nuair	k1gInSc1	nuair
=	=	kIx~	=
když	když	k8xS	když
<g/>
,	,	kIx,	,
až	až	k8xS	až
<g/>
;	;	kIx,	;
ach	ach	k0	ach
=	=	kIx~	=
ale	ale	k8xC	ale
<g/>
;	;	kIx,	;
freisin	freisin	k1gInSc1	freisin
=	=	kIx~	=
také	také	k9	také
<g/>
;	;	kIx,	;
go	go	k?	go
=	=	kIx~	=
že	že	k8xS	že
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
částic	částice	k1gFnPc2	částice
nemajících	mající	k2eNgFnPc2d1	nemající
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Homonyma	homonymum	k1gNnSc2	homonymum
===	===	k?	===
</s>
</p>
<p>
<s>
Pozor	pozor	k1gInSc1	pozor
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dát	dát	k5eAaPmF	dát
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k6eAd1	mnoho
stejně	stejně	k6eAd1	stejně
psaných	psaný	k2eAgNnPc2d1	psané
slov	slovo	k1gNnPc2	slovo
má	mít	k5eAaImIp3nS	mít
různé	různý	k2eAgNnSc1d1	různé
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
výrazně	výrazně	k6eAd1	výrazně
odlišné	odlišný	k2eAgNnSc4d1	odlišné
<g/>
,	,	kIx,	,
významy	význam	k1gInPc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
</s>
</p>
<p>
<s>
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
částice	částice	k1gFnSc1	částice
uvozující	uvozující	k2eAgNnSc1d1	uvozující
oslovení	oslovení	k1gNnSc1	oslovení
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
zájmeno	zájmeno	k1gNnSc4	zájmeno
s	s	k7c7	s
významem	význam	k1gInSc7	význam
jeho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
či	či	k8xC	či
jejich	jejich	k3xOp3gFnSc1	jejich
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
mutaci	mutace	k1gFnSc4	mutace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
také	také	k9	také
vztažná	vztažný	k2eAgFnSc1d1	vztažná
částice	částice	k1gFnSc1	částice
hrající	hrající	k2eAgFnSc4d1	hrající
roli	role	k1gFnSc4	role
přibližně	přibližně	k6eAd1	přibližně
českého	český	k2eAgInSc2d1	český
jenž	jenž	k3xRgMnSc1	jenž
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
někdy	někdy	k6eAd1	někdy
částice	částice	k1gFnSc1	částice
stojící	stojící	k2eAgFnSc1d1	stojící
před	před	k7c7	před
slovesným	slovesný	k2eAgNnSc7d1	slovesné
podstatným	podstatný	k2eAgNnSc7d1	podstatné
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
obdobu	obdoba	k1gFnSc4	obdoba
infinitivu	infinitiv	k1gInSc2	infinitiv
</s>
</p>
<p>
<s>
an	an	k?	an
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
určitý	určitý	k2eAgInSc1d1	určitý
člen	člen	k1gInSc1	člen
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
částice	částice	k1gFnSc2	částice
uvozující	uvozující	k2eAgFnSc4d1	uvozující
otázku	otázka	k1gFnSc4	otázka
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
také	také	k9	také
předpona	předpona	k1gFnSc1	předpona
s	s	k7c7	s
významem	význam	k1gInSc7	význam
velmi	velmi	k6eAd1	velmi
</s>
</p>
<p>
<s>
ar	ar	k1gInSc1	ar
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předložka	předložka	k1gFnSc1	předložka
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
také	také	k9	také
částice	částice	k1gFnSc1	částice
uvozující	uvozující	k2eAgFnSc4d1	uvozující
otázku	otázka	k1gFnSc4	otázka
v	v	k7c6	v
préteritu	préteritum	k1gNnSc6	préteritum
</s>
</p>
<p>
<s>
is	is	k?	is
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spona	spona	k1gFnSc1	spona
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
spojka	spojka	k1gFnSc1	spojka
s	s	k7c7	s
významem	význam	k1gInSc7	význam
a	a	k8xC	a
nebo	nebo	k8xC	nebo
i	i	k9	i
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
označovat	označovat	k5eAaImF	označovat
superlativ	superlativ	k1gInSc4	superlativ
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
go	go	k?	go
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předložka	předložka	k1gFnSc1	předložka
s	s	k7c7	s
významem	význam	k1gInSc7	význam
k	k	k7c3	k
<g/>
;	;	kIx,	;
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
částice	částice	k1gFnSc1	částice
tvořící	tvořící	k2eAgFnSc1d1	tvořící
z	z	k7c2	z
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
spojka	spojka	k1gFnSc1	spojka
že	že	k8xS	že
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
částice	částice	k1gFnSc2	částice
uvozující	uvozující	k2eAgInSc1d1	uvozující
subjunktiv	subjunktiv	k1gInSc1	subjunktiv
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ó	ó	k0	ó
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
předložka	předložka	k1gFnSc1	předložka
od	od	k7c2	od
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
také	také	k9	také
znamenat	znamenat	k5eAaImF	znamenat
vnuk	vnuk	k1gMnSc1	vnuk
či	či	k8xC	či
potomek	potomek	k1gMnSc1	potomek
(	(	kIx(	(
<g/>
časté	častý	k2eAgNnSc1d1	časté
ve	v	k7c6	v
jménech	jméno	k1gNnPc6	jméno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Různé	různý	k2eAgInPc1d1	různý
==	==	k?	==
</s>
</p>
<p>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
pozdravem	pozdrav	k1gInSc7	pozdrav
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
je	být	k5eAaImIp3nS	být
Dia	Dia	k1gMnSc1	Dia
dhuit	dhuit	k1gMnSc1	dhuit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zkrácenina	zkrácenina	k1gFnSc1	zkrácenina
z	z	k7c2	z
go	go	k?	go
mbeannaí	mbeannaí	k2eAgMnSc1d1	mbeannaí
Dia	Dia	k1gMnSc1	Dia
dhuit	dhuit	k1gMnSc1	dhuit
s	s	k7c7	s
významem	význam	k1gInSc7	význam
ať	ať	k8xC	ať
ti	ty	k3xPp2nSc3	ty
Bůh	bůh	k1gMnSc1	bůh
žehná	žehnat	k5eAaImIp3nS	žehnat
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
tohoto	tento	k3xDgInSc2	tento
pozdravu	pozdrav	k1gInSc2	pozdrav
nemá	mít	k5eNaImIp3nS	mít
jen	jen	k9	jen
náboženský	náboženský	k2eAgInSc1d1	náboženský
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
např.	např.	kA	např.
i	i	k8xC	i
světštější	světský	k2eAgInSc1d2	světský
maidin	maidin	k2eAgInSc1d1	maidin
mhaith	mhaith	k1gInSc1	mhaith
=	=	kIx~	=
dobré	dobrý	k2eAgNnSc1d1	dobré
ráno	ráno	k1gNnSc1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
loučení	loučení	k1gNnSc6	loučení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
slán	slán	k?	slán
agat	agat	k1gInSc1	agat
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
odcházíte	odcházet	k5eAaImIp2nP	odcházet
a	a	k8xC	a
protějšek	protějšek	k1gInSc1	protějšek
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
slán	slán	k?	slán
leat	leat	k1gInSc1	leat
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
protějšek	protějšek	k1gInSc1	protějšek
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
lze	lze	k6eAd1	lze
zkrátit	zkrátit	k5eAaPmF	zkrátit
na	na	k7c4	na
slán	slán	k?	slán
(	(	kIx(	(
<g/>
=	=	kIx~	=
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dny	den	k1gInPc1	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
jsou	být	k5eAaImIp3nP	být
počínaje	počínaje	k7c7	počínaje
pondělkem	pondělek	k1gInSc7	pondělek
Luan	Luana	k1gFnPc2	Luana
<g/>
,	,	kIx,	,
Máirt	Máirta	k1gFnPc2	Máirta
<g/>
,	,	kIx,	,
Céadaoin	Céadaoina	k1gFnPc2	Céadaoina
<g/>
,	,	kIx,	,
Déardaoin	Déardaoina	k1gFnPc2	Déardaoina
<g/>
,	,	kIx,	,
Aoine	Aoin	k1gMnSc5	Aoin
<g/>
,	,	kIx,	,
Satharn	Satharn	k1gMnSc1	Satharn
<g/>
,	,	kIx,	,
Domhnach	Domhnach	k1gMnSc1	Domhnach
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tu	tu	k6eAd1	tu
rozeznat	rozeznat	k5eAaPmF	rozeznat
vliv	vliv	k1gInSc4	vliv
latinských	latinský	k2eAgNnPc2d1	latinské
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
srv	srv	k?	srv
<g/>
.	.	kIx.	.
např.	např.	kA	např.
ital	ital	k1gInSc1	ital
<g/>
.	.	kIx.	.
</s>
<s>
Lunedì	Lunedì	k?	Lunedì
=	=	kIx~	=
pondělí	pondělí	k1gNnSc4	pondělí
=	=	kIx~	=
původně	původně	k6eAd1	původně
den	den	k1gInSc4	den
měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
Luny	luna	k1gFnSc2	luna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martedì	Martedì	k1gMnPc5	Martedì
=	=	kIx~	=
úterý	úterý	k1gNnSc4	úterý
=	=	kIx~	=
pův	pův	k?	pův
<g/>
.	.	kIx.	.
den	den	k1gInSc1	den
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
Domenica	Domenic	k1gInSc2	Domenic
=	=	kIx~	=
neděle	neděle	k1gFnSc2	neděle
=	=	kIx~	=
den	den	k1gInSc4	den
Páně	páně	k2eAgNnSc2d1	páně
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
angl.	angl.	k?	angl.
Saturday	Saturdaa	k1gFnSc2	Saturdaa
=	=	kIx~	=
sobota	sobota	k1gFnSc1	sobota
=	=	kIx~	=
původně	původně	k6eAd1	původně
den	den	k1gInSc4	den
Saturna	Saturn	k1gMnSc2	Saturn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Irské	irský	k2eAgInPc1d1	irský
názvy	název	k1gInPc1	název
pro	pro	k7c4	pro
středu	středa	k1gFnSc4	středa
<g/>
,	,	kIx,	,
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
a	a	k8xC	a
pátek	pátek	k1gInSc4	pátek
se	se	k3xPyFc4	se
odvíjejí	odvíjet	k5eAaImIp3nP	odvíjet
od	od	k7c2	od
slavností	slavnost	k1gFnPc2	slavnost
(	(	kIx(	(
<g/>
aoine	aoinout	k5eAaImIp3nS	aoinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Céadaoin	Céadaoin	k1gInSc1	Céadaoin
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc4	první
slavnost	slavnost	k1gFnSc4	slavnost
(	(	kIx(	(
<g/>
chéad	chéad	k6eAd1	chéad
aoine	aoinout	k5eAaPmIp3nS	aoinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Déardaoin	Déardaoin	k1gInSc1	Déardaoin
značilo	značit	k5eAaImAgNnS	značit
původně	původně	k6eAd1	původně
den	den	k1gInSc4	den
mezi	mezi	k7c7	mezi
slavnostmi	slavnost	k1gFnPc7	slavnost
(	(	kIx(	(
<g/>
dé	dé	k?	dé
idir	idir	k1gInSc1	idir
dhá	dhá	k?	dhá
aoine	aoinout	k5eAaImIp3nS	aoinout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
užíváme	užívat	k5eAaImIp1nP	užívat
jména	jméno	k1gNnPc1	jméno
dní	den	k1gInPc2	den
jako	jako	k8xC	jako
příslovce	příslovce	k1gNnSc2	příslovce
<g/>
,	,	kIx,	,
dáváme	dávat	k5eAaImIp1nP	dávat
před	před	k7c4	před
ně	on	k3xPp3gNnSc4	on
slovo	slovo	k1gNnSc4	slovo
dé	dé	k?	dé
a	a	k8xC	a
jméno	jméno	k1gNnSc1	jméno
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
uvede	uvést	k5eAaPmIp3nS	uvést
v	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
(	(	kIx(	(
<g/>
dé	dé	k?	dé
Luain	Luain	k1gInSc1	Luain
=	=	kIx~	=
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
<g/>
,	,	kIx,	,
dé	dé	k?	dé
hAoine	hAoinout	k5eAaPmIp3nS	hAoinout
=	=	kIx~	=
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dé	Dé	k?	Dé
značilo	značit	k5eAaImAgNnS	značit
původně	původně	k6eAd1	původně
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
běžným	běžný	k2eAgNnSc7d1	běžné
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
den	den	k1gInSc4	den
slovo	slovo	k1gNnSc4	slovo
lá	lá	k0	lá
(	(	kIx(	(
<g/>
dé	dé	k?	dé
je	být	k5eAaImIp3nS	být
také	také	k9	také
genitiv	genitiv	k1gInSc1	genitiv
slova	slovo	k1gNnSc2	slovo
dia	dia	k?	dia
=	=	kIx~	=
bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jména	jméno	k1gNnPc1	jméno
měsíců	měsíc	k1gInPc2	měsíc
jsou	být	k5eAaImIp3nP	být
Eanáir	Eanáir	k1gInSc4	Eanáir
<g/>
,	,	kIx,	,
Feabhra	Feabhra	k1gFnSc1	Feabhra
<g/>
,	,	kIx,	,
Márta	Márta	k1gFnSc1	Márta
<g/>
,	,	kIx,	,
Aibreán	Aibreán	k1gInSc1	Aibreán
<g/>
,	,	kIx,	,
Bealtaine	Bealtain	k1gMnSc5	Bealtain
<g/>
,	,	kIx,	,
Meitheamh	Meitheamh	k1gMnSc1	Meitheamh
<g/>
,	,	kIx,	,
Iúil	Iúil	k1gMnSc1	Iúil
<g/>
,	,	kIx,	,
Lúnasa	Lúnasa	k1gFnSc1	Lúnasa
<g/>
,	,	kIx,	,
Meán	Meán	k1gMnSc1	Meán
Fómhair	Fómhair	k1gMnSc1	Fómhair
<g/>
,	,	kIx,	,
Deireadh	Deireadh	k1gMnSc1	Deireadh
Fómhair	Fómhair	k1gMnSc1	Fómhair
<g/>
,	,	kIx,	,
Samhain	Samhain	k1gMnSc1	Samhain
<g/>
,	,	kIx,	,
Nollaig	Nollaig	k1gMnSc1	Nollaig
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
irština	irština	k1gFnSc1	irština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
irština	irština	k1gFnSc1	irština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Velmi	velmi	k6eAd1	velmi
podrobný	podrobný	k2eAgInSc4d1	podrobný
přehled	přehled	k1gInSc4	přehled
gramatiky	gramatika	k1gFnSc2	gramatika
</s>
</p>
<p>
<s>
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
irském	irský	k2eAgInSc6d1	irský
jazyce	jazyk	k1gInSc6	jazyk
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
</s>
</p>
<p>
<s>
Irský	irský	k2eAgInSc1d1	irský
internetový	internetový	k2eAgInSc1d1	internetový
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
organizace	organizace	k1gFnSc2	organizace
škol	škola	k1gFnPc2	škola
vyučujících	vyučující	k2eAgFnPc2d1	vyučující
v	v	k7c6	v
irštině	irština	k1gFnSc6	irština
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
regulátora	regulátor	k1gMnSc2	regulátor
irského	irský	k2eAgInSc2d1	irský
jazyka	jazyk	k1gInSc2	jazyk
Foras	Forasa	k1gFnPc2	Forasa
na	na	k7c4	na
Gaeilge	Gaeilge	k1gFnSc4	Gaeilge
</s>
</p>
<p>
<s>
Organizace	organizace	k1gFnSc2	organizace
podporující	podporující	k2eAgInSc1d1	podporující
irský	irský	k2eAgInSc1d1	irský
jazyk	jazyk	k1gInSc1	jazyk
</s>
</p>
