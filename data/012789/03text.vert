<p>
<s>
Mickey	Mickey	k1gInPc4	Mickey
Petralia	Petralium	k1gNnSc2	Petralium
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
zvukový	zvukový	k2eAgMnSc1d1	zvukový
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
hudebníky	hudebník	k1gMnPc7	hudebník
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nS	patřit
John	John	k1gMnSc1	John
Cale	Cal	k1gInSc2	Cal
<g/>
,	,	kIx,	,
Kate	kat	k1gMnSc5	kat
Miller-Heidke	Miller-Heidkus	k1gMnSc5	Miller-Heidkus
<g/>
,	,	kIx,	,
Beck	Becka	k1gFnPc2	Becka
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnSc2	skupina
Linkin	Linkina	k1gFnPc2	Linkina
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Dandy	dandy	k1gMnSc1	dandy
Warhols	Warhols	k1gInSc1	Warhols
a	a	k8xC	a
Rage	Rage	k1gFnSc1	Rage
Against	Against	k1gFnSc1	Against
the	the	k?	the
Machine	Machin	k1gMnSc5	Machin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
životopisném	životopisný	k2eAgInSc6d1	životopisný
filmu	film	k1gInSc6	film
o	o	k7c4	o
dívčí	dívčí	k2eAgFnPc4d1	dívčí
rockové	rockový	k2eAgFnPc4d1	rocková
skupiny	skupina	k1gFnPc4	skupina
The	The	k1gFnSc2	The
Runaways	Runawaysa	k1gFnPc2	Runawaysa
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
The	The	k1gFnPc3	The
Runaways	Runaways	k1gInSc4	Runaways
<g/>
;	;	kIx,	;
představil	představit	k5eAaPmAgInS	představit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
cameo	cameo	k1gNnSc1	cameo
roli	role	k1gFnSc6	role
výkonného	výkonný	k2eAgMnSc2d1	výkonný
producenta	producent	k1gMnSc2	producent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mickey	Mickey	k1gInPc1	Mickey
Petralia	Petralium	k1gNnSc2	Petralium
na	na	k7c4	na
Allmusic	Allmusic	k1gMnSc1	Allmusic
</s>
</p>
<p>
<s>
Mickey	Mickey	k1gInPc1	Mickey
Petralia	Petralium	k1gNnSc2	Petralium
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
