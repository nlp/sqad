<s>
Van	van	k1gInSc1	van
Halen	halena	k1gFnPc2	halena
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
hard	hard	k6eAd1	hard
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Pasadeně	Pasadena	k1gFnSc6	Pasadena
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vydaní	vydaný	k2eAgMnPc1d1	vydaný
svého	svůj	k3xOyFgNnSc2	svůj
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Van	van	k1gInSc1	van
Halen	halena	k1gFnPc2	halena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
slavila	slavit	k5eAaImAgFnS	slavit
úspěchy	úspěch	k1gInPc4	úspěch
až	až	k9	až
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
postihlo	postihnout	k5eAaPmAgNnS	postihnout
vícero	vícero	k1gNnSc1	vícero
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
skupině	skupina	k1gFnSc3	skupina
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
podařilo	podařit	k5eAaPmAgNnS	podařit
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světe	svět	k1gInSc5	svět
udat	udat	k5eAaPmF	udat
víc	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
80	[number]	k4	80
miliónů	milión	k4xCgInPc2	milión
hudebních	hudební	k2eAgInPc2d1	hudební
nosičů	nosič	k1gInPc2	nosič
a	a	k8xC	a
vícero	vícero	k1gNnSc4	vícero
její	její	k3xOp3gFnSc2	její
singlů	singl	k1gInPc2	singl
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Mainstream	Mainstream	k1gInSc1	Mainstream
Rock	rock	k1gInSc1	rock
Tracks	Tracksa	k1gFnPc2	Tracksa
časopisu	časopis	k1gInSc2	časopis
Billboard	billboard	k1gInSc1	billboard
<g/>
..	..	k?	..
Prodejem	prodej	k1gInSc7	prodej
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
56	[number]	k4	56
miliónů	milión	k4xCgInPc2	milión
alb	alba	k1gFnPc2	alba
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
hodnocení	hodnocení	k1gNnSc2	hodnocení
RIAA	RIAA	kA	RIAA
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
mezi	mezi	k7c7	mezi
nejlíp	dobře	k6eAd3	dobře
prodávajícími	prodávající	k2eAgFnPc7d1	prodávající
kapelami	kapela	k1gFnPc7	kapela
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
rockových	rockový	k2eAgFnPc2d1	rocková
formaci	formace	k1gFnSc4	formace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnPc4	jejíž
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
prodeje	prodej	k1gInPc4	prodej
v	v	k7c6	v
USA	USA	kA	USA
vyšší	vysoký	k2eAgFnPc4d2	vyšší
než	než	k8xS	než
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
n	n	k0	n
<g/>
'	'	kIx"	'
rollové	rollový	k2eAgFnSc2d1	rollová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
po	po	k7c6	po
různých	různý	k2eAgFnPc6d1	různá
spekulacích	spekulace	k1gFnPc6	spekulace
skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
nahrála	nahrát	k5eAaBmAgFnS	nahrát
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
A	a	k9	a
Different	Different	k1gMnSc1	Different
Kind	Kind	k1gMnSc1	Kind
Of	Of	k1gMnSc1	Of
Truth	Truth	k1gMnSc1	Truth
a	a	k8xC	a
rozjela	rozjet	k5eAaPmAgFnS	rozjet
se	se	k3xPyFc4	se
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
americké	americký	k2eAgNnSc4d1	americké
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
nahrála	nahrát	k5eAaBmAgFnS	nahrát
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
krátkou	krátký	k2eAgFnSc4d1	krátká
skladbu	skladba	k1gFnSc4	skladba
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Eruption	Eruption	k1gInSc1	Eruption
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
kytarové	kytarový	k2eAgNnSc1d1	kytarové
sólo	sólo	k1gNnSc1	sólo
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
v	v	k7c6	v
rockové	rockový	k2eAgFnSc6d1	rocková
hudební	hudební	k2eAgFnSc6d1	hudební
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
seznamech	seznam	k1gInPc6	seznam
výběrů	výběr	k1gInPc2	výběr
kytarových	kytarový	k2eAgNnPc2d1	kytarové
sól	sólo	k1gNnPc2	sólo
včetně	včetně	k7c2	včetně
aktuálního	aktuální	k2eAgInSc2d1	aktuální
žebříčku	žebříček	k1gInSc2	žebříček
časopisu	časopis	k1gInSc2	časopis
Guitar	Guitar	k1gMnSc1	Guitar
World	World	k1gMnSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
Alex	Alex	k1gMnSc1	Alex
s	s	k7c7	s
Eddiem	Eddius	k1gMnSc7	Eddius
měli	mít	k5eAaImAgMnP	mít
několik	několik	k4yIc4	několik
skupin	skupina	k1gFnPc2	skupina
než	než	k8xS	než
založili	založit	k5eAaPmAgMnP	založit
Van	van	k1gInSc4	van
Halen	halena	k1gFnPc2	halena
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc4	jméno
jejich	jejich	k3xOp3gFnPc2	jejich
předchozích	předchozí	k2eAgFnPc2d1	předchozí
kapel	kapela	k1gFnPc2	kapela
byla	být	k5eAaImAgFnS	být
The	The	k1gFnSc1	The
Broken	Brokna	k1gFnPc2	Brokna
Off	Off	k1gFnPc2	Off
Combs	Combs	k1gInSc1	Combs
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Space	Space	k1gMnSc1	Space
Brothers	Brothers	k1gInSc1	Brothers
a	a	k8xC	a
Mammoth	Mammoth	k1gMnSc1	Mammoth
Chammoth	Chammoth	k1gMnSc1	Chammoth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
Alex	Alex	k1gMnSc1	Alex
a	a	k8xC	a
Eddie	Eddie	k1gFnPc4	Eddie
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
Mammoth	Mammotha	k1gFnPc2	Mammotha
s	s	k7c7	s
Markem	Marek	k1gMnSc7	Marek
Stonem	ston	k1gInSc7	ston
jako	jako	k9	jako
baskytaristou	baskytarista	k1gMnSc7	baskytarista
a	a	k8xC	a
Eddiem	Eddius	k1gMnSc7	Eddius
jako	jako	k8xC	jako
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
si	se	k3xPyFc3	se
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
Davidův	Davidův	k2eAgInSc4d1	Davidův
Leeův	Leeův	k2eAgInSc4d1	Leeův
Rothův	Rothův	k2eAgInSc4d1	Rothův
PA	Pa	kA	Pa
systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
Eddie	Eddie	k1gFnSc2	Eddie
unavil	unavit	k5eAaPmAgInS	unavit
zpíváním	zpívání	k1gNnSc7	zpívání
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozhodi	rozhod	k1gMnPc1	rozhod
aby	aby	kYmCp3nS	aby
Roth	Roth	k1gMnSc1	Roth
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc1	jméno
Mammoth	Mammoth	k1gInSc1	Mammoth
zabráno	zabrat	k5eAaPmNgNnS	zabrat
jinou	jiný	k2eAgFnSc7d1	jiná
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jméno	jméno	k1gNnSc1	jméno
Mammoth	Mammoth	k1gMnSc1	Mammoth
nahradili	nahradit	k5eAaPmAgMnP	nahradit
jménem	jméno	k1gNnSc7	jméno
Van	vana	k1gFnPc2	vana
Halen	halen	k2eAgMnSc1d1	halen
a	a	k8xC	a
Stone	ston	k1gInSc5	ston
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Michaelem	Michael	k1gMnSc7	Michael
Anthonym	Anthonym	k1gInSc4	Anthonym
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Lee	Lea	k1gFnSc3	Lea
Roth	Roth	k1gMnSc1	Roth
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
Eddie	Eddie	k1gFnSc1	Eddie
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
-	-	kIx~	-
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
<g />
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
Halen	halena	k1gFnPc2	halena
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
Alex	Alex	k1gMnSc1	Alex
Van	vana	k1gFnPc2	vana
Halen	halena	k1gFnPc2	halena
-	-	kIx~	-
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
při	při	k7c6	při
nahrávaní	nahrávaný	k2eAgMnPc1d1	nahrávaný
alb	alba	k1gFnPc2	alba
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Anthony	Anthona	k1gFnSc2	Anthona
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Sammy	Samma	k1gFnSc2	Samma
Hagar	Hagar	k1gInSc1	Hagar
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytary	kytara	k1gFnPc1	kytara
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Gary	Gara	k1gFnSc2	Gara
Cherone	Cheron	k1gInSc5	Cheron
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Mark	Mark	k1gMnSc1	Mark
<g />
.	.	kIx.	.
</s>
<s>
Stone	ston	k1gInSc5	ston
-	-	kIx~	-
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Mitch	Mitch	k1gInSc1	Mitch
Malloy	Malloa	k1gFnSc2	Malloa
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
-	-	kIx~	-
Van	van	k1gInSc1	van
Halen	halen	k2eAgInSc1d1	halen
1979	[number]	k4	1979
-	-	kIx~	-
Van	van	k1gInSc4	van
Halen	halen	k2eAgInSc4d1	halen
II	II	kA	II
1980	[number]	k4	1980
-	-	kIx~	-
Women	Womna	k1gFnPc2	Womna
and	and	k?	and
Children	Childrna	k1gFnPc2	Childrna
First	First	k1gFnSc4	First
1981	[number]	k4	1981
-	-	kIx~	-
Fair	fair	k2eAgInSc1d1	fair
Warning	Warning	k1gInSc1	Warning
1982	[number]	k4	1982
-	-	kIx~	-
Diver	Diver	k1gInSc1	Diver
Down	Down	k1gNnSc1	Down
1984	[number]	k4	1984
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
1984	[number]	k4	1984
1986	[number]	k4	1986
-	-	kIx~	-
5150	[number]	k4	5150
1988	[number]	k4	1988
-	-	kIx~	-
OU812	OU812	k1gFnSc1	OU812
1991	[number]	k4	1991
-	-	kIx~	-
For	forum	k1gNnPc2	forum
Unlawful	Unlawfula	k1gFnPc2	Unlawfula
Carnal	Carnal	k1gFnSc1	Carnal
Knowledge	Knowledge	k1gFnSc1	Knowledge
1993	[number]	k4	1993
-	-	kIx~	-
Live	Liv	k1gInSc2	Liv
<g/>
:	:	kIx,	:
Right	Right	k2eAgInSc1d1	Right
Here	Here	k1gInSc1	Here
<g/>
,	,	kIx,	,
Right	Right	k1gInSc1	Right
Now	Now	k1gFnSc1	Now
1995	[number]	k4	1995
-	-	kIx~	-
Balance	balanc	k1gInSc2	balanc
1998	[number]	k4	1998
-	-	kIx~	-
Van	van	k1gInSc4	van
Halen	halen	k2eAgInSc4d1	halen
III	III	kA	III
2012	[number]	k4	2012
-	-	kIx~	-
A	a	k8xC	a
Different	Different	k1gMnSc1	Different
Kind	Kind	k1gMnSc1	Kind
Of	Of	k1gMnSc1	Of
Truth	Truth	k1gMnSc1	Truth
1996	[number]	k4	1996
-	-	kIx~	-
Best	Best	k1gInSc1	Best
of	of	k?	of
Volume	volum	k1gInSc5	volum
I	i	k9	i
2004	[number]	k4	2004
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Both	Both	k1gMnSc1	Both
Worlds	Worlds	k1gInSc4	Worlds
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Van	vana	k1gFnPc2	vana
Halen	halen	k2eAgMnSc1d1	halen
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
www.van-halen.com	www.vanalen.com	k1gInSc4	www.van-halen.com
</s>
