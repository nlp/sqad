<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
či	či	k8xC
losos	losos	k1gMnSc1
atlantský	atlantský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Salmo	Salma	k1gFnSc5
salar	salar	k1gInSc4
Linné	Linné	k1gNnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dravá	dravý	k2eAgFnSc1d1
tažná	tažný	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
lososovitých	lososovití	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
žije	žít	k5eAaImIp3nS
většinu	většina	k1gFnSc4
života	život	k1gInSc2
v	v	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>