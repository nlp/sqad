<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
</s>
<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Salmo	Salma	k1gFnSc5
salar	salar	k1gInSc1
<g/>
)	)	kIx)
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
paprskoploutví	paprskoploutvit	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
Actinopterygii	Actinopterygie	k1gFnSc3
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
lososotvární	lososotvárný	k2eAgMnPc1d1
(	(	kIx(
<g/>
Salmoniformes	Salmoniformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
lososovití	lososovití	k1gMnPc1
(	(	kIx(
<g/>
Salmonidae	Salmonidae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
losos	losos	k1gMnSc1
(	(	kIx(
<g/>
Salmo	Salma	k1gFnSc5
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Salmo	Salmo	k6eAd1
salarLinné	salarLinný	k2eAgInPc1d1
<g/>
,	,	kIx,
1758	#num#	k4
</s>
<s>
Výskyt	výskyt	k1gInSc1
lososa	losos	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
k	k	k7c3
roku	rok	k1gInSc3
2006	#num#	k4
</s>
<s>
Výskyt	výskyt	k1gInSc1
lososa	losos	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
k	k	k7c3
roku	rok	k1gInSc3
2006	#num#	k4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
či	či	k8xC
losos	losos	k1gMnSc1
atlantský	atlantský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Salmo	Salma	k1gFnSc5
salar	salar	k1gInSc4
Linné	Linné	k1gNnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dravá	dravý	k2eAgFnSc1d1
tažná	tažný	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
lososovitých	lososovití	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
žije	žít	k5eAaImIp3nS
většinu	většina	k1gFnSc4
života	život	k1gInSc2
v	v	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospělí	dospělý	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
během	během	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
migrují	migrovat	k5eAaImIp3nP
proti	proti	k7c3
proudu	proud	k1gInSc3
řek	řeka	k1gFnPc2
hluboko	hluboko	k6eAd1
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
kontinentů	kontinent	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
v	v	k7c6
rodných	rodný	k2eAgFnPc6d1
řekách	řeka	k1gFnPc6
vyvedli	vyvést	k5eAaPmAgMnP
další	další	k2eAgFnSc4d1
generaci	generace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladí	mladý	k2eAgMnPc1d1
lososi	losos	k1gMnPc1
pak	pak	k6eAd1
v	v	k7c6
horních	horní	k2eAgInPc6d1
tocích	tok	k1gInPc6
řek	řeka	k1gFnPc2
tráví	trávit	k5eAaImIp3nP
začátek	začátek	k1gInSc4
života	život	k1gInSc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
vydají	vydat	k5eAaPmIp3nP
zpět	zpět	k6eAd1
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zde	zde	k6eAd1
rychle	rychle	k6eAd1
dospěli	dochvít	k5eAaPmAgMnP
a	a	k8xC
vydali	vydat	k5eAaPmAgMnP
se	se	k3xPyFc4
zpět	zpět	k6eAd1
do	do	k7c2
rodných	rodný	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
když	když	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
rybu	ryba	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
většinu	většina	k1gFnSc4
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
tráví	trávit	k5eAaImIp3nS
ve	v	k7c6
slané	slaný	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
losos	losos	k1gMnSc1
schopný	schopný	k2eAgMnSc1d1
dlouhodobě	dlouhodobě	k6eAd1
či	či	k8xC
celoživotně	celoživotně	k6eAd1
žít	žít	k5eAaImF
ve	v	k7c6
sladké	sladký	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
výraznému	výrazný	k2eAgNnSc3d1
poškození	poškození	k1gNnSc3
či	či	k8xC
strádání	strádání	k1gNnSc3
jedince	jedinko	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
živého	živý	k2eAgMnSc4d1
lososa	losos	k1gMnSc4
</s>
<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
obvykle	obvykle	k6eAd1
dorůstá	dorůstat	k5eAaImIp3nS
70	#num#	k4
až	až	k9
90	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
ale	ale	k8xC
byli	být	k5eAaImAgMnP
pozorování	pozorování	k1gNnSc4
i	i	k8xC
jedinci	jedinec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
dosahovali	dosahovat	k5eAaImAgMnP
délky	délka	k1gFnPc4
150	#num#	k4
cm	cm	kA
u	u	k7c2
samce	samec	k1gInSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
120	#num#	k4
cm	cm	kA
u	u	k7c2
samice	samice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Většinou	většina	k1gFnSc7
se	se	k3xPyFc4
dožívá	dožívat	k5eAaImIp3nS
13	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
prvním	první	k4xOgInSc6
roce	rok	k1gInSc6
života	život	k1gInSc2
losos	losos	k1gMnSc1
dosahuje	dosahovat	k5eAaImIp3nS
délky	délka	k1gFnSc2
přibližně	přibližně	k6eAd1
50	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
70	#num#	k4
až	až	k9
90	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
ve	v	k7c6
třetím	třetí	k4xOgInSc6
roce	rok	k1gInSc6
života	život	k1gInSc2
již	již	k6eAd1
90	#num#	k4
až	až	k9
105	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
ve	v	k7c6
třetím	třetí	k4xOgInSc6
roce	rok	k1gInSc6
života	život	k1gInSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
8	#num#	k4
až	až	k9
13	#num#	k4
kg	kg	kA
s	s	k7c7
maximální	maximální	k2eAgFnSc7d1
zaznamenanou	zaznamenaný	k2eAgFnSc7d1
hmotností	hmotnost	k1gFnSc7
samce	samec	k1gInSc2
46,8	46,8	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Velikost	velikost	k1gFnSc1
lososa	losos	k1gMnSc2
je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
populaci	populace	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
a	a	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
značně	značně	k6eAd1
rozmanitá	rozmanitý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každé	každý	k3xTgFnSc6
populaci	populace	k1gFnSc6
se	se	k3xPyFc4
současně	současně	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
i	i	k9
morfologicky	morfologicky	k6eAd1
odlišní	odlišný	k2eAgMnPc1d1
zakrnělí	zakrnělý	k2eAgMnPc1d1
samci	samec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nepodnikají	podnikat	k5eNaImIp3nP
migrační	migrační	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
jedinci	jedinec	k1gMnPc1
byli	být	k5eAaImAgMnP
dříve	dříve	k6eAd2
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
samostatný	samostatný	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
moderní	moderní	k2eAgInPc1d1
genetické	genetický	k2eAgInPc1d1
testy	test	k1gInPc1
tyto	tento	k3xDgFnPc1
domněnky	domněnka	k1gFnPc1
vyvrátily	vyvrátit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s>
Losos	losos	k1gMnSc1
má	mít	k5eAaImIp3nS
dlouhé	dlouhý	k2eAgNnSc4d1
<g/>
,	,	kIx,
protáhlé	protáhlý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
se	s	k7c7
štíhlým	štíhlý	k2eAgInSc7d1
ocasním	ocasní	k2eAgInSc7d1
násadcem	násadec	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
se	se	k3xPyFc4
ve	v	k7c6
přední	přední	k2eAgFnSc6d1
části	část	k1gFnSc6
nachází	nacházet	k5eAaImIp3nS
relativně	relativně	k6eAd1
malá	malý	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
začínající	začínající	k2eAgMnPc4d1
malým	malý	k2eAgInSc7d1
špičatým	špičatý	k2eAgInSc7d1
rypcem	rypec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Široká	široký	k2eAgFnSc1d1
tlama	tlama	k1gFnSc1
zasahuje	zasahovat	k5eAaImIp3nS
až	až	k9
za	za	k7c4
oči	oko	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
stranách	strana	k1gFnPc6
stříbro-modře	stříbro-modro	k6eAd1
zbarveno	zbarvit	k5eAaPmNgNnS
při	při	k7c6
pobytu	pobyt	k1gInSc6
v	v	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
návratu	návrat	k1gInSc2
do	do	k7c2
třecích	třecí	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
tělo	tělo	k1gNnSc1
postupně	postupně	k6eAd1
tmavne	tmavnout	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hřbet	hřbet	k1gInSc1
samců	samec	k1gMnPc2
začíná	začínat	k5eAaImIp3nS
být	být	k5eAaImF
postupně	postupně	k6eAd1
hnědý	hnědý	k2eAgInSc1d1
<g/>
,	,	kIx,
bok	bok	k1gInSc1
přechází	přecházet	k5eAaImIp3nS
do	do	k7c2
namodralé	namodralý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
a	a	k8xC
zespodu	zespodu	k6eAd1
je	být	k5eAaImIp3nS
červený	červený	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
pokryto	pokrýt	k5eAaPmNgNnS
drobnými	drobný	k2eAgFnPc7d1
šupinami	šupina	k1gFnPc7
<g/>
,	,	kIx,
kterých	který	k3yRgNnPc2,k3yIgNnPc2,k3yQgNnPc2
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
120	#num#	k4
až	až	k9
150	#num#	k4
v	v	k7c6
oblasti	oblast	k1gFnSc6
postranní	postranní	k2eAgFnSc2d1
čáry	čára	k1gFnSc2
a	a	k8xC
11	#num#	k4
až	až	k6eAd1
15	#num#	k4
mezi	mezi	k7c7
tukovou	tukový	k2eAgFnSc7d1
ploutvičkou	ploutvička	k1gFnSc7
a	a	k8xC
postranní	postranní	k2eAgFnSc7d1
čárou	čára	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Losos	losos	k1gMnSc1
v	v	k7c6
moři	moře	k1gNnSc6
</s>
<s>
Moře	moře	k1gNnSc1
</s>
<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
od	od	k7c2
příbřežních	příbřežní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
až	až	k9
po	po	k7c4
oblast	oblast	k1gFnSc4
Bílého	bílý	k2eAgInSc2d1
moře	moře	k1gNnSc1
vymezenou	vymezený	k2eAgFnSc4d1
37	#num#	k4
<g/>
°	°	k?
až	až	k9
72	#num#	k4
<g/>
°	°	k?
severní	severní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
a	a	k8xC
77	#num#	k4
<g/>
°	°	k?
západní	západní	k2eAgFnSc2d1
délky	délka	k1gFnSc2
až	až	k9
61	#num#	k4
<g/>
°	°	k?
východní	východní	k2eAgFnSc2d1
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhledává	vyhledávat	k5eAaImIp3nS
chladnější	chladný	k2eAgFnSc2d2
vody	voda	k1gFnSc2
o	o	k7c6
teplotě	teplota	k1gFnSc6
od	od	k7c2
2	#num#	k4
do	do	k7c2
9	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
hloubce	hloubka	k1gFnSc6
0	#num#	k4
<g/>
–	–	k?
<g/>
210	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
mořskou	mořský	k2eAgFnSc7d1
hladinou	hladina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
Atlantiku	Atlantik	k1gInSc2
migruje	migrovat	k5eAaImIp3nS
do	do	k7c2
oblastí	oblast	k1gFnPc2
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
od	od	k7c2
Islandu	Island	k1gInSc2
<g/>
,	,	kIx,
Norska	Norsko	k1gNnSc2
<g/>
,	,	kIx,
Švédska	Švédsko	k1gNnSc2
<g/>
,	,	kIx,
Finska	Finsko	k1gNnSc2
<g/>
,	,	kIx,
Dánska	Dánsko	k1gNnSc2
<g/>
,	,	kIx,
Pobaltí	Pobaltí	k1gNnSc2
<g/>
,	,	kIx,
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
severní	severní	k2eAgNnSc4d1
Portugalsko	Portugalsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
tato	tento	k3xDgNnPc4
evropská	evropský	k2eAgNnPc4d1
území	území	k1gNnPc4
obývají	obývat	k5eAaImIp3nP
lososi	losos	k1gMnPc1
také	také	k9
oblasti	oblast	k1gFnSc2
západního	západní	k2eAgInSc2d1
Atlantiku	Atlantik	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
se	se	k3xPyFc4
šíří	šířit	k5eAaImIp3nS
do	do	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
od	od	k7c2
Quebecu	Quebecus	k1gInSc2
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlivem	vlivem	k7c2
člověka	člověk	k1gMnSc2
se	se	k3xPyFc4
losos	losos	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
i	i	k9
do	do	k7c2
oblasti	oblast	k1gFnSc2
Velkých	velký	k2eAgNnPc2d1
jezer	jezero	k1gNnPc2
na	na	k7c6
hranici	hranice	k1gFnSc6
USA	USA	kA
a	a	k8xC
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
populace	populace	k1gFnSc1
postupně	postupně	k6eAd1
ustálila	ustálit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
vyskytoval	vyskytovat	k5eAaImAgMnS
v	v	k7c6
jezeře	jezero	k1gNnSc6
Ontario	Ontario	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
vlivem	vliv	k1gInSc7
nadměrného	nadměrný	k2eAgInSc2d1
rybolovu	rybolov	k1gInSc2
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
poškození	poškození	k1gNnSc2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
přirozeného	přirozený	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
populace	populace	k1gFnSc2
zdecimována	zdecimovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1
populace	populace	k1gFnPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
současně	současně	k6eAd1
i	i	k9
v	v	k7c6
Argentině	Argentina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Proniká	pronikat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
části	část	k1gFnSc2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řeky	Řek	k1gMnPc4
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1
lososi	losos	k1gMnPc1
se	se	k3xPyFc4
vždy	vždy	k6eAd1
vracejí	vracet	k5eAaImIp3nP
do	do	k7c2
řeky	řeka	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
narodili	narodit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
jev	jev	k1gInSc1
je	být	k5eAaImIp3nS
vysvětlován	vysvětlovat	k5eAaImNgInS
schopností	schopnost	k1gFnSc7
lososa	losos	k1gMnSc2
si	se	k3xPyFc3
zapamatovat	zapamatovat	k5eAaPmF
typické	typický	k2eAgNnSc4d1
složení	složení	k1gNnSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
vůni	vůně	k1gFnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
chemismus	chemismus	k1gInSc1
<g/>
)	)	kIx)
řeky	řeka	k1gFnPc1
a	a	k8xC
na	na	k7c6
jejím	její	k3xOp3gInSc6
základě	základ	k1gInSc6
ji	on	k3xPp3gFnSc4
pak	pak	k6eAd1
objevit	objevit	k5eAaPmF
a	a	k8xC
vydat	vydat	k5eAaPmF
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickým	typický	k2eAgNnSc7d1
trdlištěm	trdliště	k1gNnSc7
lososů	losos	k1gMnPc2
jsou	být	k5eAaImIp3nP
prudce	prudko	k6eAd1
tekoucí	tekoucí	k2eAgFnPc4d1
bystřiny	bystřina	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
se	se	k3xPyFc4
vytírají	vytírat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
Ulovený	ulovený	k2eAgMnSc1d1
losos	losos	k1gMnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
byl	být	k5eAaImAgMnS
losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
dočasně	dočasně	k6eAd1
vyhuben	vyhuben	k2eAgMnSc1d1
vlivem	vlivem	k7c2
přehrazování	přehrazování	k1gNnSc2
řek	řeka	k1gFnPc2
(	(	kIx(
<g/>
přehrady	přehrada	k1gFnPc4
<g/>
,	,	kIx,
nevhodné	vhodný	k2eNgInPc4d1
jezy	jez	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
znečištění	znečištění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
se	se	k3xPyFc4
losos	losos	k1gMnSc1
vyskytoval	vyskytovat	k5eAaImAgMnS
v	v	k7c6
říční	říční	k2eAgFnSc6d1
síti	síť	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pokrývala	pokrývat	k5eAaImAgFnS
přibližně	přibližně	k6eAd1
řeky	řeka	k1gFnPc4
Labe	Labe	k1gNnSc2
<g/>
,	,	kIx,
Ohři	Ohře	k1gFnSc4
<g/>
,	,	kIx,
Orlici	Orlice	k1gFnSc4
<g/>
,	,	kIx,
Otavu	Otava	k1gFnSc4
a	a	k8xC
Vltavu	Vltava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Rozhodujícím	rozhodující	k2eAgInSc7d1
faktorem	faktor	k1gInSc7
pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
dočasné	dočasný	k2eAgNnSc4d1
vyhubení	vyhubení	k1gNnSc4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
stavba	stavba	k1gFnSc1
zdymadla	zdymadlo	k1gNnSc2
na	na	k7c6
Labi	Labe	k1gNnSc6
ve	v	k7c6
Střekově	Střekův	k2eAgNnSc6d1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
pro	pro	k7c4
zlepšení	zlepšení	k1gNnSc4
splavnosti	splavnost	k1gFnSc2
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
stavba	stavba	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
překážkou	překážka	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
nemohli	moct	k5eNaImAgMnP
migrující	migrující	k2eAgMnPc1d1
lososi	losos	k1gMnPc1
překonat	překonat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
roku	rok	k1gInSc3
2008	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
celoročně	celoročně	k6eAd1
hájenou	hájený	k2eAgFnSc4d1
rybu	ryba	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
zakázáno	zakázán	k2eAgNnSc1d1
lovit	lovit	k5eAaImF
za	za	k7c7
účelem	účel	k1gInSc7
konzumace	konzumace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezónách	sezóna	k1gFnPc6
2002	#num#	k4
a	a	k8xC
2003	#num#	k4
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
po	po	k7c6
75	#num#	k4
letech	léto	k1gNnPc6
potvrdit	potvrdit	k5eAaPmF
úlovek	úlovek	k1gInSc4
lososa	losos	k1gMnSc4
na	na	k7c6
území	území	k1gNnSc6
státu	stát	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Existují	existovat	k5eAaImIp3nP
i	i	k9
nepodložené	podložený	k2eNgFnPc1d1
zprávy	zpráva	k1gFnPc1
o	o	k7c6
úlovku	úlovek	k1gInSc6
lososa	losos	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
v	v	k7c6
Lovosicích	Lovosice	k1gInPc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
na	na	k7c6
Otavě	Otava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
vzácně	vzácně	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
severních	severní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
existovaly	existovat	k5eAaImAgInP
dva	dva	k4xCgInPc1
hlavní	hlavní	k2eAgInPc1d1
tahy	tah	k1gInPc1
lososů	losos	k1gMnPc2
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
probíhal	probíhat	k5eAaImAgInS
od	od	k7c2
března	březen	k1gInSc2
do	do	k7c2
června	červen	k1gInSc2
a	a	k8xC
další	další	k2eAgNnPc1d1
pak	pak	k6eAd1
od	od	k7c2
srpna	srpen	k1gInSc2
do	do	k7c2
března	březen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
druhém	druhý	k4xOgInSc6
tahu	tah	k1gInSc6
se	se	k3xPyFc4
nacházeli	nacházet	k5eAaImAgMnP
větší	veliký	k2eAgMnPc1d2
jedinci	jedinec	k1gMnPc1
než	než	k8xS
v	v	k7c6
tahu	tah	k1gInSc6
předchozím	předchozí	k2eAgInSc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obnova	obnova	k1gFnSc1
populace	populace	k1gFnSc2
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
probíhají	probíhat	k5eAaImIp3nP
projekty	projekt	k1gInPc4
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
návrat	návrat	k1gInSc4
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
za	za	k7c2
pomoci	pomoc	k1gFnSc2
vysazování	vysazování	k1gNnSc2
mladých	mladý	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
do	do	k7c2
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
o	o	k7c4
projekt	projekt	k1gInSc4
Losos	losos	k1gMnSc1
2000	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
za	za	k7c2
spolupráce	spolupráce	k1gFnSc2
Českého	český	k2eAgInSc2d1
rybářského	rybářský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
Německého	německý	k2eAgInSc2d1
rybářského	rybářský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
přípravou	příprava	k1gFnSc7
programu	program	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
německou	německý	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledal	hledat	k5eAaImAgMnS
se	se	k3xPyFc4
losos	losos	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
by	by	kYmCp3nS
odpovídal	odpovídat	k5eAaImAgMnS
původní	původní	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
žijící	žijící	k2eAgFnSc1d1
na	na	k7c6
Labi	Labe	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
vybrán	vybrán	k2eAgMnSc1d1
losos	losos	k1gMnSc1
žijící	žijící	k2eAgMnSc1d1
na	na	k7c6
území	území	k1gNnSc6
Švédska	Švédsko	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
počtu	počet	k1gInSc6
1,4	1,4	k4
miliónů	milión	k4xCgInPc2
kusů	kus	k1gInPc2
plůdku	plůdek	k1gInSc2
vysazen	vysazen	k2eAgInSc4d1
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
to	ten	k3xDgNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
přistoupila	přistoupit	k5eAaPmAgFnS
k	k	k7c3
projektu	projekt	k1gInSc3
česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
využity	využit	k2eAgFnPc1d1
zkušenosti	zkušenost	k1gFnPc1
z	z	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
-	-	kIx~
kresba	kresba	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
populace	populace	k1gFnSc1
lososa	losos	k1gMnSc2
byla	být	k5eAaImAgFnS
vysazena	vysadit	k5eAaPmNgFnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Hřenské	Hřenský	k2eAgFnSc2d1
Kamenice	Kamenice	k1gFnSc2
<g/>
,	,	kIx,
Chřibské	chřibský	k2eAgFnSc2d1
Kamenice	Kamenice	k1gFnSc2
<g/>
,	,	kIx,
Ploučnice	Ploučnice	k1gFnSc2
<g/>
,	,	kIx,
Ještědského	ještědský	k2eAgInSc2d1
potoku	potok	k1gInSc6
<g/>
,	,	kIx,
Zdislavského	Zdislavský	k2eAgMnSc2d1
potoku	potok	k1gInSc3
<g/>
,	,	kIx,
Ohře	Ohře	k1gFnSc1
a	a	k8xC
Libovického	Libovický	k2eAgMnSc2d1
potoku	potok	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
k	k	k7c3
roku	rok	k1gInSc3
2006	#num#	k4
bylo	být	k5eAaImAgNnS
celkově	celkově	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
vysazeno	vysazen	k2eAgNnSc4d1
přes	přes	k7c4
1	#num#	k4
841	#num#	k4
500	#num#	k4
jedinců	jedinec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
probíhá	probíhat	k5eAaImIp3nS
projekt	projekt	k1gInSc1
Návrat	návrat	k1gInSc1
lososů	losos	k1gMnPc2
pod	pod	k7c7
patronací	patronace	k1gFnSc7
Správy	správa	k1gFnSc2
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
,	,	kIx,
zaměřený	zaměřený	k2eAgInSc1d1
na	na	k7c4
podzimní	podzimní	k2eAgNnSc4d1
vypouštění	vypouštění	k1gNnSc4
odrostlejších	odrostlý	k2eAgMnPc2d2
<g/>
,	,	kIx,
8	#num#	k4
až	až	k9
10	#num#	k4
centimetrů	centimetr	k1gInPc2
velkých	velký	k2eAgFnPc2d1
rybiček	rybička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgInSc2
projektu	projekt	k1gInSc2
může	moct	k5eAaImIp3nS
zájemce	zájemce	k1gMnSc1
zaslat	zaslat	k5eAaPmF
dárcovskou	dárcovský	k2eAgFnSc4d1
SMS	SMS	kA
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
„	„	k?
<g/>
adoptuje	adoptovat	k5eAaPmIp3nS
<g/>
“	“	k?
lososa	losos	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
vysazen	vysadit	k5eAaPmNgMnS
do	do	k7c2
řeky	řeka	k1gFnSc2
Kamenice	Kamenice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Inkubace	inkubace	k1gFnSc1
jiker	jikra	k1gFnPc2
v	v	k7c6
plovoucích	plovoucí	k2eAgFnPc6d1
schránkách	schránka	k1gFnPc6
na	na	k7c6
Jetřichovické	Jetřichovický	k2eAgFnSc6d1
Bělé	Bělá	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Plovoucí	plovoucí	k2eAgFnSc1d1
modifikace	modifikace	k1gFnSc1
inkubační	inkubační	k2eAgFnSc2d1
schránky	schránka	k1gFnSc2
LOSOSA	losos	k1gMnSc2
Gama	gama	k1gNnSc2
umístěná	umístěný	k2eAgFnSc1d1
na	na	k7c6
řece	řeka	k1gFnSc6
Jetřichovické	Jetřichovický	k2eAgFnSc2d1
Bělé	Bělá	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
schránce	schránka	k1gFnSc6
se	se	k3xPyFc4
inkubují	inkubovat	k5eAaImIp3nP
jikry	jikra	k1gFnPc1
lososa	losos	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Inkubační	inkubační	k2eAgFnPc4d1
schránky	schránka	k1gFnPc4
LOSOSA	losos	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnová	Dnová	k1gFnSc1
modifikace	modifikace	k1gFnSc1
DELTA	delta	k1gFnSc1
a	a	k8xC
plovoucí	plovoucí	k2eAgFnSc1d1
modifikace	modifikace	k1gFnSc1
GAMA	gama	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schránky	schránka	k1gFnPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgFnP
na	na	k7c6
řece	řeka	k1gFnSc6
Kamenici	Kamenice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
schránkách	schránka	k1gFnPc6
se	se	k3xPyFc4
inkubují	inkubovat	k5eAaImIp3nP
jikry	jikra	k1gFnPc1
lososa	losos	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Inkubace	inkubace	k1gFnSc1
jiker	jikra	k1gFnPc2
lososa	losos	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
na	na	k7c6
Jetřichovické	Jetřichovický	k2eAgFnSc6d1
Bělé	Bělá	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Plovoucí	plovoucí	k2eAgFnSc1d1
modifikace	modifikace	k1gFnSc1
inkubační	inkubační	k2eAgFnSc2d1
schránky	schránka	k1gFnSc2
LOSOSA	losos	k1gMnSc2
Gama	gama	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schránka	schránka	k1gFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
pro	pro	k7c4
inkubaci	inkubace	k1gFnSc4
jiker	jikra	k1gFnPc2
lososovitých	lososovitý	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
průmyslový	průmyslový	k2eAgInSc4d1
vzor	vzor	k1gInSc4
vytvořený	vytvořený	k2eAgInSc4d1
českým	český	k2eAgMnSc7d1
ichtyologem	ichtyolog	k1gMnSc7
RNDr.	RNDr.	kA
Jiřím	Jiří	k1gMnSc7
Křesinou	Křesin	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
repatriace	repatriace	k1gFnSc1
losos	losos	k1gMnSc1
obecného	obecný	k2eAgNnSc2d1
formou	forma	k1gFnSc7
inkubace	inkubace	k1gFnSc2
jiker	jikra	k1gFnPc2
v	v	k7c6
mateřském	mateřský	k2eAgInSc6d1
toku	tok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
této	tento	k3xDgFnSc3
metodě	metoda	k1gFnSc3
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
u	u	k7c2
lososů	losos	k1gMnPc2
k	k	k7c3
lepšímu	lepší	k1gNnSc3
vývinu	vývin	k1gInSc2
tzv.	tzv.	kA
homingu	homing	k1gInSc2
<g/>
,	,	kIx,
vlastnosti	vlastnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
napomáhá	napomáhat	k5eAaBmIp3nS,k5eAaImIp3nS
rybám	ryba	k1gFnPc3
navrátit	navrátit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
narodily	narodit	k5eAaPmAgFnP
<g/>
,	,	kIx,
za	za	k7c7
účelem	účel	k1gInSc7
rozmnožování	rozmnožování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
uplatnění	uplatnění	k1gNnSc4
metody	metoda	k1gFnSc2
bylo	být	k5eAaImAgNnS
vybráno	vybrat	k5eAaPmNgNnS
povodí	povodí	k1gNnSc1
řeky	řeka	k1gFnSc2
Kamenice	Kamenice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
z	z	k7c2
lokalit	lokalita	k1gFnPc2
v	v	k7c6
povodí	povodí	k1gNnSc6
je	být	k5eAaImIp3nS
Jetřichovická	Jetřichovický	k2eAgFnSc1d1
Bělá	bělat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koordinátorem	koordinátor	k1gMnSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
ichtyolog	ichtyolog	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Křesina	Křesina	k1gFnSc1
z	z	k7c2
organizace	organizace	k1gFnSc2
Beleco	Beleco	k6eAd1
zabývající	zabývající	k2eAgFnSc4d1
se	se	k3xPyFc4
aplikovanou	aplikovaný	k2eAgFnSc7d1
ekologií	ekologie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
povodí	povodí	k1gNnSc6
řeky	řeka	k1gFnSc2
Kamenice	Kamenice	k1gFnSc2
inkubováno	inkubovat	k5eAaImNgNnS
celkem	celkem	k6eAd1
100.000	100.000	k4
kusů	kus	k1gInPc2
jiker	jikra	k1gFnPc2
lososa	losos	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jikry	jikra	k1gFnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
stádiu	stádium	k1gNnSc6
viditelných	viditelný	k2eAgInPc2d1
očních	oční	k2eAgInPc2d1
bodů	bod	k1gInPc2
umístěny	umístit	k5eAaPmNgFnP
do	do	k7c2
speciálních	speciální	k2eAgFnPc2d1
plovoucích	plovoucí	k2eAgFnPc2d1
inkubačních	inkubační	k2eAgFnPc2d1
schránek	schránka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inkubovaný	inkubovaný	k2eAgInSc1d1
plůdek	plůdek	k1gInSc1
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
vypouštěn	vypouštět	k5eAaImNgInS
do	do	k7c2
toku	tok	k1gInSc2
v	v	k7c6
místě	místo	k1gNnSc6
inkubace	inkubace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inkubace	inkubace	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
termínu	termín	k1gInSc6
od	od	k7c2
ledna	leden	k1gInSc2
do	do	k7c2
května	květen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
bude	být	k5eAaImBp3nS
probíhat	probíhat	k5eAaImF
do	do	k7c2
roku	rok	k1gInSc2
2023	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
financován	financovat	k5eAaBmNgInS
z	z	k7c2
Operačního	operační	k2eAgInSc2d1
programu	program	k1gInSc2
Životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
můžou	můžou	k?
zájemci	zájemce	k1gMnPc7
podpořit	podpořit	k5eAaPmF
aktivitu	aktivita	k1gFnSc4
dárcovskou	dárcovský	k2eAgFnSc7d1
výzvou	výzva	k1gFnSc7
Losos	losos	k1gMnSc1
na	na	k7c6
divoko	divoko	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
2019	#num#	k4
-	-	kIx~
2021	#num#	k4
proběhl	proběhnout	k5eAaPmAgInS
projekt	projekt	k1gInSc1
na	na	k7c4
inovaci	inovace	k1gFnSc4
inkubačních	inkubační	k2eAgFnPc2d1
schránek	schránka	k1gFnPc2
a	a	k8xC
vývoj	vývoj	k1gInSc1
metodiky	metodika	k1gFnSc2
inkubace	inkubace	k1gFnSc2
jiker	jikra	k1gFnPc2
lososovitých	lososovitý	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
v	v	k7c6
mateřském	mateřský	k2eAgInSc6d1
toku	tok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zájmovými	zájmový	k2eAgInPc7d1
druhy	druh	k1gInPc7
mimo	mimo	k7c4
lososa	losos	k1gMnSc4
obecného	obecný	k2eAgNnSc2d1
byli	být	k5eAaImAgMnP
také	také	k9
pstruh	pstruh	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
a	a	k8xC
lipan	lipan	k1gMnSc1
podhorní	podhorní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
projekt	projekt	k1gInSc1
byl	být	k5eAaImAgInS
realizován	realizovat	k5eAaBmNgInS
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
organizace	organizace	k1gFnSc2
Beleco	Beleco	k6eAd1
<g/>
,	,	kIx,
z.	z.	k?
s.	s.	k?
s	s	k7c7
Ostravskou	ostravský	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Projekt	projekt	k1gInSc1
je	být	k5eAaImIp3nS
realizován	realizovat	k5eAaBmNgInS
za	za	k7c4
podpory	podpora	k1gFnPc4
Technologické	technologický	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstupem	výstup	k1gInSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
metodika	metodika	k1gFnSc1
na	na	k7c4
inkubaci	inkubace	k1gFnSc4
jiker	jikra	k1gFnPc2
lososovitých	lososovitý	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
v	v	k7c6
mateřském	mateřský	k2eAgInSc6d1
toku	tok	k1gInSc6
certifikovaná	certifikovaný	k2eAgFnSc1d1
Ministerstvem	ministerstvo	k1gNnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
výstupem	výstup	k1gInSc7
je	být	k5eAaImIp3nS
inovovaná	inovovaný	k2eAgFnSc1d1
inkubační	inkubační	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
LOSOSA	losos	k1gMnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnSc1
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
ichtyolog	ichtyolog	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Křesina	Křesina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
dravec	dravec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nS
ve	v	k7c6
sladké	sladký	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
larvami	larva	k1gFnPc7
hmyzu	hmyz	k1gInSc2
<g/>
,	,	kIx,
korýši	korýš	k1gMnPc7
a	a	k8xC
drobnými	drobný	k2eAgFnPc7d1
rybkami	rybka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
života	život	k1gInSc2
v	v	k7c6
moři	moře	k1gNnSc6
loví	lovit	k5eAaImIp3nP
výhradně	výhradně	k6eAd1
menší	malý	k2eAgFnPc1d2
ryby	ryba	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
sledi	sleď	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Čerstvě	čerstvě	k6eAd1
narozený	narozený	k2eAgInSc1d1
plůdek	plůdek	k1gInSc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nS
larvami	larva	k1gFnPc7
jepic	jepice	k1gFnPc2
<g/>
,	,	kIx,
pošvatek	pošvatka	k1gFnPc2
<g/>
,	,	kIx,
chrostíků	chrostík	k1gMnPc2
či	či	k8xC
muchniček	muchnička	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Losos	losos	k1gMnSc1
je	být	k5eAaImIp3nS
schopný	schopný	k2eAgMnSc1d1
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
přibývat	přibývat	k5eAaImF
na	na	k7c6
hmotnosti	hmotnost	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k6eAd1
rychlostí	rychlost	k1gFnSc7
1	#num#	k4
kg	kg	kA
<g/>
/	/	kIx~
<g/>
měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
díky	díky	k7c3
hromadění	hromadění	k1gNnSc3
značného	značný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
tuku	tuk	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
zbarvení	zbarvení	k1gNnSc2
lososí	lososí	k2eAgFnSc2d1
svaloviny	svalovina	k1gFnSc2
do	do	k7c2
oranžovočervené	oranžovočervený	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tuto	tento	k3xDgFnSc4
barvu	barva	k1gFnSc4
způsobuje	způsobovat	k5eAaImIp3nS
přítomnost	přítomnost	k1gFnSc1
xanthofylového	xanthofylový	k2eAgNnSc2d1
barviva	barvivo	k1gNnSc2
astaxanthinu	astaxanthin	k1gInSc2
a	a	k8xC
karotenu	karoten	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
do	do	k7c2
těla	tělo	k1gNnSc2
lososa	losos	k1gMnSc2
dostává	dostávat	k5eAaImIp3nS
potravinovým	potravinový	k2eAgInSc7d1
řetězcem	řetězec	k1gInSc7
<g/>
,	,	kIx,
například	například	k6eAd1
přes	přes	k7c4
korýše	korýš	k1gMnPc4
<g/>
,	,	kIx,
díky	díky	k7c3
některým	některý	k3yIgInPc3
druhům	druh	k1gInPc3
planktonu	plankton	k1gInSc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgNnPc7
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc4
barvivo	barvivo	k1gNnSc4
produkováno	produkován	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tichomořské	tichomořský	k2eAgInPc1d1
druhy	druh	k1gInPc1
lososa	losos	k1gMnSc2
obsahují	obsahovat	k5eAaImIp3nP
v	v	k7c6
malé	malý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
také	také	k9
kanthaxanthin	kanthaxanthin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
prvních	první	k4xOgNnPc2
let	léto	k1gNnPc2
života	život	k1gInSc2
jsou	být	k5eAaImIp3nP
lososi	losos	k1gMnPc1
často	často	k6eAd1
potravou	potrava	k1gFnSc7
pro	pro	k7c4
jiné	jiný	k2eAgMnPc4d1
říční	říční	k2eAgMnPc4d1
predátory	predátor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
převážně	převážně	k6eAd1
o	o	k7c4
pstruhy	pstruh	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
zkonzumují	zkonzumovat	k5eAaPmIp3nP
až	až	k9
40	#num#	k4
%	%	kIx~
mladých	mladý	k2eAgMnPc2d1
lososů	losos	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgMnPc7d1
predátory	predátor	k1gMnPc7
<g/>
,	,	kIx,
pojídajícími	pojídající	k2eAgMnPc7d1
mladé	mladý	k2eAgFnPc4d1
ryby	ryba	k1gFnPc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
ptáci	pták	k1gMnPc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
dravé	dravý	k2eAgFnPc1d1
ryby	ryba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospívající	dospívající	k2eAgMnPc1d1
lososi	losos	k1gMnPc1
v	v	k7c6
moři	moře	k1gNnSc6
jsou	být	k5eAaImIp3nP
dále	daleko	k6eAd2
konzumováni	konzumován	k2eAgMnPc1d1
žralokem	žralok	k1gMnSc7
grónským	grónský	k2eAgMnSc7d1
<g/>
,	,	kIx,
rejnoky	rejnok	k1gMnPc7
<g/>
,	,	kIx,
treskou	treska	k1gFnSc7
a	a	k8xC
platýsovitými	platýsovitý	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Plůdek	plůdek	k1gInSc1
se	s	k7c7
žloutkovým	žloutkový	k2eAgInSc7d1
vakem	vak	k1gInSc7
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
pohlavní	pohlavní	k2eAgFnSc2d1
dospělosti	dospělost	k1gFnSc2
začnou	začít	k5eAaPmIp3nP
lososi	losos	k1gMnPc1
táhnout	táhnout	k5eAaImF
do	do	k7c2
oblastí	oblast	k1gFnPc2
rodných	rodný	k2eAgFnPc2d1
vod	voda	k1gFnPc2
proti	proti	k7c3
proudu	proud	k1gInSc3
řeky	řeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tahu	tah	k1gInSc2
nepřijímají	přijímat	k5eNaImIp3nP
téměř	téměř	k6eAd1
žádnou	žádný	k3yNgFnSc4
potravu	potrava	k1gFnSc4
a	a	k8xC
žijí	žít	k5eAaImIp3nP
z	z	k7c2
nashromážděných	nashromážděný	k2eAgFnPc2d1
zásob	zásoba	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
na	na	k7c6
šupinách	šupina	k1gFnPc6
–	–	k?
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
vzniku	vznik	k1gInSc3
tzv.	tzv.	kA
„	„	k?
<g/>
třecích	třecí	k2eAgFnPc2d1
značek	značka	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jejich	jejich	k3xOp3gInSc6
základě	základ	k1gInSc6
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
určit	určit	k5eAaPmF
počet	počet	k1gInSc4
tahů	tah	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
jedinec	jedinec	k1gMnSc1
podnikl	podniknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
lososi	losos	k1gMnPc1
dostanou	dostat	k5eAaPmIp3nP
do	do	k7c2
horních	horní	k2eAgInPc2d1
toků	tok	k1gInPc2
řek	řeka	k1gFnPc2
do	do	k7c2
míst	místo	k1gNnPc2
tření	tření	k1gNnSc2
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
v	v	k7c6
době	doba	k1gFnSc6
výtěru	výtěr	k1gInSc2
k	k	k7c3
fyziologickým	fyziologický	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
ve	v	k7c6
stavbě	stavba	k1gFnSc6
těla	tělo	k1gNnSc2
samců	samec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přední	přední	k2eAgFnSc1d1
čelist	čelist	k1gFnSc1
se	se	k3xPyFc4
začne	začít	k5eAaPmIp3nS
protahovat	protahovat	k5eAaImF
v	v	k7c4
hákovitý	hákovitý	k2eAgInSc4d1
útvar	útvar	k1gInSc4
a	a	k8xC
zvětší	zvětšit	k5eAaPmIp3nP
se	se	k3xPyFc4
přední	přední	k2eAgInPc1d1
zuby	zub	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
hák	hák	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
hormonální	hormonální	k2eAgFnSc6d1
činnosti	činnost	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
vazivem	vazivo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zavřených	zavřený	k2eAgNnPc6d1
ústech	ústa	k1gNnPc6
se	se	k3xPyFc4
vtlačí	vtlačit	k5eAaPmIp3nP
do	do	k7c2
prohlubně	prohlubeň	k1gFnSc2
mezi	mezi	k7c4
pohyblivé	pohyblivý	k2eAgFnPc4d1
přední	přední	k2eAgFnPc4d1
kosti	kost	k1gFnPc4
a	a	k8xC
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
nedochází	docházet	k5eNaImIp3nS
k	k	k7c3
poranění	poranění	k1gNnSc3
patra	patro	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doba	doba	k1gFnSc1
rozmnožování	rozmnožování	k1gNnSc2
nastává	nastávat	k5eAaImIp3nS
mezi	mezi	k7c7
říjnem	říjen	k1gInSc7
až	až	k9
prosincem	prosinec	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
začátku	začátek	k1gInSc6
samice	samice	k1gFnSc2
vytvoří	vytvořit	k5eAaPmIp3nS
viditelně	viditelně	k6eAd1
očištěnou	očištěný	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
na	na	k7c6
dně	dno	k1gNnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
klade	klást	k5eAaImIp3nS
jikry	jikra	k1gFnPc4
o	o	k7c6
velikosti	velikost	k1gFnSc6
5	#num#	k4
až	až	k9
7	#num#	k4
mm	mm	kA
do	do	k7c2
rýhovitých	rýhovitý	k2eAgFnPc2d1
jam	jáma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
samice	samice	k1gFnSc1
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
vyprodukovat	vyprodukovat	k5eAaPmF
10	#num#	k4
000	#num#	k4
až	až	k9
40	#num#	k4
000	#num#	k4
jiker	jikra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohybem	pohyb	k1gInSc7
jejího	její	k3xOp3gNnSc2
těla	tělo	k1gNnSc2
vzniká	vznikat	k5eAaImIp3nS
několik	několik	k4yIc1
jam	jáma	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
následně	následně	k6eAd1
zaplní	zaplnit	k5eAaPmIp3nP
vajíčky	vajíčko	k1gNnPc7
a	a	k8xC
zahrabe	zahrabat	k5eAaPmIp3nS
štěrkem	štěrk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
třít	třít	k5eAaImF
s	s	k7c7
několika	několik	k4yIc7
samci	samec	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
vytření	vytření	k1gNnSc6
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
samců	samec	k1gMnPc2
hyne	hynout	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
oplodněných	oplodněný	k2eAgNnPc2d1
vajíček	vajíčko	k1gNnPc2
se	se	k3xPyFc4
vylíhnou	vylíhnout	k5eAaPmIp3nP
mladí	mladý	k2eAgMnPc1d1
lososi	losos	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
v	v	k7c6
místě	místo	k1gNnSc6
narození	narození	k1gNnSc2
žijí	žít	k5eAaImIp3nP
1	#num#	k4
až	až	k9
2	#num#	k4
roky	rok	k1gInPc4
(	(	kIx(
<g/>
pro	pro	k7c4
jižně	jižně	k6eAd1
umístěné	umístěný	k2eAgFnPc4d1
řeky	řeka	k1gFnPc4
a	a	k8xC
oblast	oblast	k1gFnSc4
La	la	k1gNnSc2
Manche	Manch	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
oblastech	oblast	k1gFnPc6
severní	severní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
až	až	k9
5	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Těmto	tento	k3xDgMnPc3
mladým	mladý	k2eAgMnPc3d1
lososům	losos	k1gMnPc3
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
říká	říkat	k5eAaImIp3nS
„	„	k?
<g/>
strdlice	strdlice	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Mláďata	mládě	k1gNnPc1
mají	mít	k5eAaImIp3nP
do	do	k7c2
velikosti	velikost	k1gFnSc2
přibližně	přibližně	k6eAd1
15	#num#	k4
cm	cm	kA
na	na	k7c6
bocích	bok	k1gInPc6
velké	velký	k2eAgFnSc2d1
tmavé	tmavý	k2eAgFnSc2d1
skvrny	skvrna	k1gFnSc2
s	s	k7c7
malými	malý	k2eAgFnPc7d1
červenými	červený	k2eAgFnPc7d1
tečkami	tečka	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
mladí	mladý	k2eAgMnPc1d1
lososi	losos	k1gMnPc1
připraveni	připravit	k5eAaPmNgMnP
na	na	k7c4
cestu	cesta	k1gFnSc4
do	do	k7c2
moře	moře	k1gNnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
kde	kde	k6eAd1
stráví	strávit	k5eAaPmIp3nP
1	#num#	k4
až	až	k9
3	#num#	k4
roky	rok	k1gInPc4
života	život	k1gInSc2
(	(	kIx(
<g/>
vzácněji	vzácně	k6eAd2
i	i	k9
déle	dlouho	k6eAd2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zase	zase	k9
mohli	moct	k5eAaImAgMnP
v	v	k7c6
době	doba	k1gFnSc6
pohlavní	pohlavní	k2eAgFnSc2d1
dospělosti	dospělost	k1gFnSc2
vrátit	vrátit	k5eAaPmF
k	k	k7c3
vytření	vytření	k1gNnSc3
do	do	k7c2
rodných	rodný	k2eAgFnPc2d1
vod	voda	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
uzavřít	uzavřít	k5eAaPmF
stále	stále	k6eAd1
se	se	k3xPyFc4
opakující	opakující	k2eAgInSc4d1
koloběh	koloběh	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Migrace	migrace	k1gFnSc1
</s>
<s>
Tah	tah	k1gInSc1
lososů	losos	k1gMnPc2
začíná	začínat	k5eAaImIp3nS
nejednotně	jednotně	k6eNd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nejspíše	nejspíše	k9
způsobeno	způsobit	k5eAaPmNgNnS
rozdílnými	rozdílný	k2eAgFnPc7d1
vzdálenostmi	vzdálenost	k1gFnPc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
od	od	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgMnS
losos	losos	k1gMnSc1
přirozeně	přirozeně	k6eAd1
migrovat	migrovat	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byly	být	k5eAaImAgFnP
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
řeky	řeka	k1gFnSc2
splavitelné	splavitelný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
umožnění	umožnění	k1gNnSc4
migrace	migrace	k1gFnSc2
se	se	k3xPyFc4
budují	budovat	k5eAaImIp3nP
často	často	k6eAd1
speciální	speciální	k2eAgFnPc1d1
propusti	propust	k1gFnPc1
na	na	k7c6
přehradách	přehrada	k1gFnPc6
či	či	k8xC
schody	schod	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
umožňují	umožňovat	k5eAaImIp3nP
rybě	ryba	k1gFnSc6
se	se	k3xPyFc4
přes	přes	k7c4
umělou	umělý	k2eAgFnSc4d1
bariéru	bariéra	k1gFnSc4
dostat	dostat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lososi	losos	k1gMnPc1
jsou	být	k5eAaImIp3nP
známí	známý	k1gMnPc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
vyskočit	vyskočit	k5eAaPmF
do	do	k7c2
značné	značný	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
přes	přes	k7c4
překážku	překážka	k1gFnSc4
(	(	kIx(
<g/>
např.	např.	kA
vodopád	vodopád	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
migrace	migrace	k1gFnSc2
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
urazit	urazit	k5eAaPmF
vzdálenost	vzdálenost	k1gFnSc4
až	až	k9
4000	#num#	k4
km	km	kA
a	a	k8xC
překonat	překonat	k5eAaPmF
bariéry	bariéra	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
až	až	k9
3	#num#	k4
metry	metr	k1gInPc1
vysoké	vysoký	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Parazité	parazit	k1gMnPc1
a	a	k8xC
nemoci	nemoc	k1gFnPc1
</s>
<s>
Argulus	Argulus	k1gMnSc1
coregoni	coregoň	k1gFnSc3
je	být	k5eAaImIp3nS
parazit	parazit	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
žije	žít	k5eAaImIp3nS
na	na	k7c4
kůži	kůže	k1gFnSc4
lososa	losos	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Lernaeopoda	Lernaeopoda	k1gMnSc1
salmonea	salmonea	k1gMnSc1
je	být	k5eAaImIp3nS
parazit	parazit	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
žije	žít	k5eAaImIp3nS
na	na	k7c6
žábrách	žábry	k1gFnPc6
lososa	losos	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
endoparazity	endoparazit	k1gMnPc7
patří	patřit	k5eAaImIp3nS
Bothriocephalus	Bothriocephalus	k1gInSc1
infundibuliformis	infundibuliformis	k1gFnSc2
<g/>
,	,	kIx,
larva	larva	k1gFnSc1
Scolex	Scolex	k1gInSc1
polymorphus	polymorphus	k1gInSc1
<g/>
,	,	kIx,
Ascaris	Ascaris	k1gFnSc1
clavata	clavata	k1gFnSc1
<g/>
,	,	kIx,
Agamonema	Agamonema	k1gFnSc1
capsularia	capsularium	k1gNnSc2
<g/>
,	,	kIx,
Echinorhynchus	Echinorhynchus	k1gMnSc1
pachysomus	pachysomus	k1gMnSc1
a	a	k8xC
tasemnice	tasemnice	k1gFnSc1
Tetrarhynchus	Tetrarhynchus	k1gMnSc1
macrobothrius	macrobothrius	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Gyrodactylus	Gyrodactylus	k1gInSc1
salaris	salaris	k1gFnSc2
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
se	se	k3xPyFc4
v	v	k7c6
Norsku	Norsko	k1gNnSc6
u	u	k7c2
lososa	losos	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
objevuje	objevovat	k5eAaImIp3nS
častý	častý	k2eAgMnSc1d1
parazit	parazit	k1gMnSc1
Gyrodactylus	Gyrodactylus	k1gMnSc1
salaris	salaris	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
způsobuje	způsobovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
úmrtnost	úmrtnost	k1gFnSc4
v	v	k7c6
populacích	populace	k1gFnPc6
této	tento	k3xDgFnSc2
ryby	ryba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgMnS
do	do	k7c2
Norska	Norsko	k1gNnSc2
dovezen	dovézt	k5eAaPmNgInS
spolu	spolu	k6eAd1
s	s	k7c7
lososím	lososí	k2eAgInSc7d1
potěrem	potěr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
značné	značný	k2eAgNnSc1d1
decimování	decimování	k1gNnSc1
sádek	sádka	k1gFnPc2
a	a	k8xC
výskytu	výskyt	k1gInSc2
lososa	losos	k1gMnSc2
v	v	k7c6
oblastech	oblast	k1gFnPc6
okolí	okolí	k1gNnSc2
řeky	řeka	k1gFnSc2
Rauma	Raumum	k1gNnSc2
<g/>
,	,	kIx,
Istra	Istro	k1gNnSc2
<g/>
,	,	kIx,
Driva	Drivo	k1gNnSc2
<g/>
,	,	kIx,
Usma	Usmum	k1gNnSc2
<g/>
,	,	kIx,
Litledelselva	Litledelselvo	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
šíření	šíření	k1gNnSc4
tohoto	tento	k3xDgMnSc2
parazita	parazit	k1gMnSc2
v	v	k7c6
Norsku	Norsko	k1gNnSc6
jsou	být	k5eAaImIp3nP
návštěvníci	návštěvník	k1gMnPc1
zasažených	zasažený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
žádáni	žádat	k5eAaImNgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
důkladně	důkladně	k6eAd1
očistili	očistit	k5eAaPmAgMnP
a	a	k8xC
dezinfikovali	dezinfikovat	k5eAaBmAgMnP
rybářské	rybářský	k2eAgNnSc4d1
náčiní	náčiní	k1gNnSc4
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
vydají	vydat	k5eAaPmIp3nP
rybařit	rybařit	k5eAaImF
do	do	k7c2
další	další	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hospodářské	hospodářský	k2eAgNnSc1d1
využití	využití	k1gNnSc1
</s>
<s>
Maso	maso	k1gNnSc1
lososa	losos	k1gMnSc2
nakrájené	nakrájený	k2eAgNnSc1d1
na	na	k7c4
plátky	plátek	k1gInPc4
</s>
<s>
Luis	Luisa	k1gFnPc2
Meléndez	Meléndeza	k1gFnPc2
<g/>
,	,	kIx,
Zátiší	zátiší	k1gNnSc1
s	s	k7c7
lososem	losos	k1gMnSc7
<g/>
,	,	kIx,
citronem	citron	k1gInSc7
a	a	k8xC
třemi	tři	k4xCgFnPc7
nádobami	nádoba	k1gFnPc7
(	(	kIx(
<g/>
1772	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Losos	losos	k1gMnSc1
je	být	k5eAaImIp3nS
hojně	hojně	k6eAd1
loven	loven	k2eAgMnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
průmyslově	průmyslově	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
sportovními	sportovní	k2eAgMnPc7d1
rybáři	rybář	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
maso	maso	k1gNnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
ceněno	cenit	k5eAaImNgNnS
a	a	k8xC
má	mít	k5eAaImIp3nS
typickou	typický	k2eAgFnSc4d1
oranžovočervenou	oranžovočervený	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
za	za	k7c4
syrova	syrovo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
se	se	k3xPyFc4
po	po	k7c6
uvaření	uvaření	k1gNnSc6
mění	měnit	k5eAaImIp3nS
dorůžova	dorůžova	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
rybolovu	rybolov	k1gInSc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
je	být	k5eAaImIp3nS
značná	značný	k2eAgFnSc1d1
část	část	k1gFnSc1
lososího	lososí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
získávána	získáván	k2eAgFnSc1d1
na	na	k7c6
lososích	lososí	k2eAgFnPc6d1
farmách	farma	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
jedinci	jedinec	k1gMnPc1
chováni	chovat	k5eAaImNgMnP
pro	pro	k7c4
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
farmách	farma	k1gFnPc6
se	se	k3xPyFc4
někdy	někdy	k6eAd1
k	k	k7c3
přibarvení	přibarvení	k1gNnSc3
masa	maso	k1gNnSc2
přidávají	přidávat	k5eAaImIp3nP
do	do	k7c2
potravy	potrava	k1gFnSc2
barviva	barvivo	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
kantaxantin	kantaxantin	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
rybolovu	rybolov	k1gInSc2
na	na	k7c6
Labi	Labe	k1gNnSc6
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
na	na	k7c6
území	území	k1gNnSc6
Labe	Labe	k1gNnSc2
o	o	k7c4
významnou	významný	k2eAgFnSc4d1
rybu	ryba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
hlavním	hlavní	k2eAgInSc7d1
úlovkem	úlovek	k1gInSc7
rybolovu	rybolov	k1gInSc2
na	na	k7c6
tomto	tento	k3xDgInSc6
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výlov	výlov	k1gInSc1
lososů	losos	k1gMnPc2
probíhal	probíhat	k5eAaImAgInS
2	#num#	k4
krát	krát	k6eAd1
až	až	k9
3	#num#	k4
krát	krát	k6eAd1
do	do	k7c2
roka	rok	k1gInSc2
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
období	období	k1gNnSc6
února	únor	k1gInSc2
až	až	k8xS
března	březen	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
května	květen	k1gInSc2
do	do	k7c2
června	červen	k1gInSc2
a	a	k8xC
dále	daleko	k6eAd2
pak	pak	k6eAd1
na	na	k7c4
podzim	podzim	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadměrným	nadměrný	k2eAgInSc7d1
rybolovem	rybolov	k1gInSc7
a	a	k8xC
přehrazováním	přehrazování	k1gNnSc7
řeky	řeka	k1gFnSc2
ale	ale	k8xC
začala	začít	k5eAaPmAgFnS
populace	populace	k1gFnSc1
lososů	losos	k1gMnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
postupně	postupně	k6eAd1
slábnout	slábnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1883	#num#	k4
až	až	k8xS
1908	#num#	k4
byly	být	k5eAaImAgFnP
provedeny	provést	k5eAaPmNgInP
pokusy	pokus	k1gInPc1
o	o	k7c4
stabilizaci	stabilizace	k1gFnSc4
a	a	k8xC
udržení	udržení	k1gNnSc4
populace	populace	k1gFnSc2
umělým	umělý	k2eAgNnSc7d1
vysazováním	vysazování	k1gNnSc7
plůdku	plůdek	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšnost	úspěšnost	k1gFnSc1
tohoto	tento	k3xDgNnSc2
konání	konání	k1gNnSc2
potvrdily	potvrdit	k5eAaPmAgInP
zvýšené	zvýšený	k2eAgInPc1d1
úlovky	úlovek	k1gInPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
se	se	k3xPyFc4
upustilo	upustit	k5eAaPmAgNnS
od	od	k7c2
budování	budování	k1gNnSc2
umělých	umělý	k2eAgInPc2d1
rybích	rybí	k2eAgInPc2d1
přechodů	přechod	k1gInPc2
na	na	k7c6
vodních	vodní	k2eAgInPc6d1
dílech	díl	k1gInPc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mělo	mít	k5eAaImAgNnS
značný	značný	k2eAgInSc4d1
negativní	negativní	k2eAgInSc4d1
dopad	dopad	k1gInSc4
na	na	k7c4
migrující	migrující	k2eAgFnPc4d1
populace	populace	k1gFnPc4
lososa	losos	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úprava	úprava	k1gFnSc1
</s>
<s>
Losos	losos	k1gMnSc1
se	se	k3xPyFc4
servíruje	servírovat	k5eAaBmIp3nS
vařený	vařený	k2eAgMnSc1d1
nebo	nebo	k8xC
pečený	pečený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
oblibu	obliba	k1gFnSc4
má	mít	k5eAaImIp3nS
také	také	k9
uzený	uzený	k2eAgMnSc1d1
losos	losos	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
tepelně	tepelně	k6eAd1
neupravený	upravený	k2eNgMnSc1d1
jako	jako	k8xC,k8xS
suši	suš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nutriční	nutriční	k2eAgInPc1d1
parametry	parametr	k1gInPc1
</s>
<s>
Tabulka	tabulka	k1gFnSc1
udává	udávat	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
živin	živina	k1gFnPc2
<g/>
,	,	kIx,
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
vitamínů	vitamín	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
nutričních	nutriční	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
zjištěných	zjištěný	k2eAgInPc2d1
v	v	k7c6
mase	maso	k1gNnSc6
lososa	losos	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Složka	složka	k1gFnSc1
</s>
<s>
Jednotka	jednotka	k1gFnSc1
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
</s>
<s>
Prvek	prvek	k1gInSc1
(	(	kIx(
<g/>
mg	mg	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
)	)	kIx)
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
</s>
<s>
Složka	složka	k1gFnSc1
(	(	kIx(
<g/>
mg	mg	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
)	)	kIx)
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
</s>
<s>
vodag	vodag	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
67,2	67,2	k4
<g/>
Na	na	k7c4
<g/>
45	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
Cstopy	Cstopa	k1gFnSc2
</s>
<s>
bílkovinyg	bílkovinyg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
20,2	20,2	k4
<g/>
K	k	k7c3
<g/>
360	#num#	k4
<g/>
vitamin	vitamin	k1gInSc1
D	D	kA
<g/>
5	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
</s>
<s>
tukyg	tukyg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
23	#num#	k4
<g/>
Ca	ca	kA
<g/>
21	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
E	E	kA
<g/>
1,91	1,91	k4
</s>
<s>
cukryg	cukryg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
0	#num#	k4
<g/>
Mg	mg	kA
<g/>
27	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
B	B	kA
<g/>
60,75	60,75	k4
</s>
<s>
celkový	celkový	k2eAgInSc1d1
dusíkg	dusíkg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
3,23	3,23	k4
<g/>
P	P	kA
<g/>
250	#num#	k4
<g/>
vitamin	vitamin	k1gInSc4
B	B	kA
<g/>
120,004	120,004	k4
</s>
<s>
vlákninag	vlákninag	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
0	#num#	k4
<g/>
Fe	Fe	k1gFnSc2
<g/>
0,4	0,4	k4
<g/>
karotenstopy	karotenstopa	k1gFnSc2
</s>
<s>
mastné	mastné	k1gNnSc1
kyselinyg	kyselinyga	k1gFnPc2
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
9,5	9,5	k4
<g/>
Cu	Cu	k1gFnSc2
<g/>
0,03	0,03	k4
<g/>
thiamin	thiamin	k1gInSc1
<g/>
0,23	0,23	k4
</s>
<s>
cholesterolmg	cholesterolmg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
50	#num#	k4
<g/>
Zn	zn	kA
<g/>
0,6	0,6	k4
<g/>
riboflavin	riboflavin	k1gInSc1
<g/>
0,13	0,13	k4
</s>
<s>
Semg	Semg	k1gInSc1
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
0,026	0,026	k4
<g/>
I	i	k9
<g/>
0,037	0,037	k4
<g/>
niacin	niacina	k1gFnPc2
<g/>
7,2	7,2	k4
</s>
<s>
energiekJ	energiekJ	k?
<g/>
/	/	kIx~
<g/>
100	#num#	k4
g	g	kA
<g/>
750	#num#	k4
<g/>
Mn	Mn	k1gFnSc2
<g/>
0,02	0,02	k4
<g/>
Cl	Cl	k1gFnSc1
<g/>
58	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Atlantic	Atlantice	k1gFnPc2
salmon	salmona	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gFnPc4
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
DOBROVSKÝ	Dobrovský	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severskelisty	Severskelista	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Salmo	Salma	k1gFnSc5
Salar	salar	k1gInSc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MRK	mrk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Stále	stále	k6eAd1
na	na	k7c6
rybách	ryba	k1gFnPc6
-	-	kIx~
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Salmo	Salma	k1gFnSc5
salar	salar	k1gInSc1
<g/>
,	,	kIx,
Atlantic	Atlantice	k1gFnPc2
salmon	salmona	k1gFnPc2
:	:	kIx,
fisheries	fisheries	k1gInSc1
<g/>
,	,	kIx,
aquaculture	aquacultur	k1gMnSc5
<g/>
,	,	kIx,
gamefish	gamefish	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Rainer	Rainra	k1gFnPc2
Froese	Froese	k1gFnSc2
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Pauly	Paula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
FishBase	FishBas	k1gInSc6
<g/>
,	,	kIx,
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
18	#num#	k4
19	#num#	k4
20	#num#	k4
Obnova	obnova	k1gFnSc1
reprodukce	reprodukce	k1gFnSc2
lososa	losos	k1gMnSc2
atlantského	atlantský	k2eAgMnSc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
STEPAN	STEPAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hununpa	Hununpa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
databáze	databáze	k1gFnSc2
ryb	ryba	k1gFnPc2
:	:	kIx,
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Atlas	Atlas	k1gInSc1
ryb	ryba	k1gFnPc2
-	-	kIx~
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.navratlososu.cz/jak-adoptovat-lososa.html	http://www.navratlososu.cz/jak-adoptovat-lososa.html	k1gMnSc1
<g/>
↑	↑	k?
Beleco	Beleco	k1gMnSc1
|	|	kIx~
Odborná	odborný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
působící	působící	k2eAgFnSc1d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
beleco	beleco	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Beleco	Beleco	k1gNnSc1
|	|	kIx~
Podpora	podpora	k1gFnSc1
kriticky	kriticky	k6eAd1
ohroženého	ohrožený	k2eAgInSc2d1
druhu	druh	k1gInSc2
lososa	losos	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
na	na	k7c4
<g/>
.	.	kIx.
beleco	beleco	k1gNnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Repatriace	repatriace	k1gFnSc2
lososa	losos	k1gMnSc2
<g/>
.	.	kIx.
www.repatriacelososa.cz	www.repatriacelososa.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RESDB	RESDB	kA
<g/>
.	.	kIx.
isdv	isdv	k1gMnSc1
<g/>
.	.	kIx.
<g/>
upv	upv	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VAROVÁNÍ	varování	k1gNnSc1
–	–	k?
Nakažlivý	nakažlivý	k2eAgMnSc1d1
cizopasník	cizopasník	k1gMnSc1
útočí	útočit	k5eAaImIp3nS
na	na	k7c4
lososy	losos	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Opinion	Opinion	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
use	usus	k1gInSc5
of	of	k?
canthaxanthin	canthaxanthin	k2eAgInSc4d1
in	in	k?
feedingstuffs	feedingstuffs	k1gInSc4
for	forum	k1gNnPc2
salmon	salmon	k1gInSc1
<g/>
.	.	kIx.
ec	ec	k?
<g/>
.	.	kIx.
<g/>
europa	europa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
eu	eu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2008	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
16	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
McCance	McCance	k1gFnSc1
a	a	k8xC
Widdowson	Widdowson	k1gInSc1
<g/>
´	´	k?
<g/>
s	s	k7c7
<g/>
:	:	kIx,
<g/>
The	The	k1gMnSc2
Composition	Composition	k1gInSc1
of	of	k?
Foods	Foods	k1gInSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Summary	Summara	k1gFnPc1
edition	edition	k1gInSc1
<g/>
,	,	kIx,
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
Chemistry	Chemistr	k1gMnPc4
Cambridge	Cambridge	k1gFnSc2
a	a	k8xC
Food	Food	k1gInSc4
Standard	standard	k1gInSc1
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-0-85404-428-3	978-0-85404-428-3	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Taxon	taxon	k1gInSc1
Salmo	Salma	k1gFnSc5
salar	salar	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Losos	losos	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
-	-	kIx~
atlas	atlas	k1gInSc1
ryb	ryba	k1gFnPc2
on-line	on-lin	k1gInSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4033961-0	4033961-0	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Živočichové	živočich	k1gMnPc1
</s>
