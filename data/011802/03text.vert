<p>
<s>
Oxid	oxid	k1gInSc1	oxid
technecistý	technecistý	k2eAgInSc1d1	technecistý
(	(	kIx(	(
<g/>
Tc	tc	k0	tc
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
oxidů	oxid	k1gInPc2	oxid
technecia	technecium	k1gNnSc2	technecium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
má	mít	k5eAaImIp3nS	mít
oxidační	oxidační	k2eAgNnSc1d1	oxidační
číslo	číslo	k1gNnSc1	číslo
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
oxidací	oxidace	k1gFnSc7	oxidace
technecia	technecium	k1gNnSc2	technecium
za	za	k7c2	za
teploty	teplota	k1gFnSc2	teplota
450	[number]	k4	450
<g/>
–	–	k?	–
<g/>
500	[number]	k4	500
°	°	k?	°
<g/>
C	C	kA	C
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
4	[number]	k4	4
Tc	tc	k0	tc
+	+	kIx~	+
7	[number]	k4	7
O2	O2	k1gFnSc2	O2
→	→	k?	→
2	[number]	k4	2
Tc	tc	k0	tc
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
</s>
</p>
