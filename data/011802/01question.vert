<s>
Jaký	jaký	k3yIgInSc1	jaký
oxid	oxid	k1gInSc1	oxid
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
oxidů	oxid	k1gInPc2	oxid
technecia	technecium	k1gNnSc2	technecium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
má	mít	k5eAaImIp3nS	mít
oxidační	oxidační	k2eAgNnSc1d1	oxidační
číslo	číslo	k1gNnSc1	číslo
VII	VII	kA	VII
<g/>
?	?	kIx.	?
</s>
