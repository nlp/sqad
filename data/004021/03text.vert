<s>
Glykogen	glykogen	k1gInSc1	glykogen
neboli	neboli	k8xC	neboli
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
živočišný	živočišný	k2eAgInSc4d1	živočišný
škrob	škrob	k1gInSc4	škrob
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
zásobní	zásobní	k2eAgInSc1d1	zásobní
polysacharid	polysacharid	k1gInSc1	polysacharid
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Glykogen	glykogen	k1gInSc1	glykogen
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
větvený	větvený	k2eAgInSc1d1	větvený
polymer	polymer	k1gInSc1	polymer
tvořený	tvořený	k2eAgInSc1d1	tvořený
glukózami	glukóza	k1gFnPc7	glukóza
(	(	kIx(	(
<g/>
polyglukan	polyglukan	k1gInSc1	polyglukan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
pospojované	pospojovaný	k2eAgFnPc1d1	pospojovaná
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
molekula	molekula	k1gFnSc1	molekula
glykogenu	glykogen	k1gInSc2	glykogen
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
molekul	molekula	k1gFnPc2	molekula
glukózy	glukóza	k1gFnSc2	glukóza
(	(	kIx(	(
<g/>
až	až	k9	až
120	[number]	k4	120
000	[number]	k4	000
molekul	molekula	k1gFnPc2	molekula
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgFnPc1d1	spojená
navzájem	navzájem	k6eAd1	navzájem
α	α	k?	α
<g/>
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
<g/>
)	)	kIx)	)
<g/>
–	–	k?	–
<g/>
glykosidovými	glykosidový	k2eAgFnPc7d1	glykosidový
vazbami	vazba	k1gFnPc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
8-12	[number]	k4	8-12
jednotkách	jednotka	k1gFnPc6	jednotka
se	se	k3xPyFc4	se
však	však	k9	však
ještě	ještě	k6eAd1	ještě
nachází	nacházet	k5eAaImIp3nS	nacházet
větvení	větvení	k1gNnPc4	větvení
pomocí	pomocí	k7c2	pomocí
vazby	vazba	k1gFnSc2	vazba
α	α	k?	α
<g/>
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
<g/>
)	)	kIx)	)
<g/>
–	–	k?	–
<g/>
glykosidické	glykosidický	k2eAgFnSc2d1	glykosidická
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
větvení	větvení	k1gNnSc1	větvení
molekul	molekula	k1gFnPc2	molekula
glykogenu	glykogen	k1gInSc2	glykogen
<g/>
.	.	kIx.	.
</s>
<s>
Glykogen	glykogen	k1gInSc1	glykogen
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
uložen	uložit	k5eAaPmNgInS	uložit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
granulí	granule	k1gFnPc2	granule
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
některých	některý	k3yIgFnPc2	některý
buněk	buňka	k1gFnPc2	buňka
vyšších	vysoký	k2eAgMnPc2d2	vyšší
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
jater	játra	k1gNnPc2	játra
(	(	kIx(	(
<g/>
lidské	lidský	k2eAgFnPc1d1	lidská
jaterní	jaterní	k2eAgFnPc1d1	jaterní
buňky	buňka	k1gFnPc1	buňka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
sušině	sušina	k1gFnSc6	sušina
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
glykogenu	glykogen	k1gInSc2	glykogen
<g/>
)	)	kIx)	)
a	a	k8xC	a
svalů	sval	k1gInPc2	sval
(	(	kIx(	(
<g/>
svalové	svalový	k2eAgFnPc1d1	svalová
buňky	buňka	k1gFnPc1	buňka
asi	asi	k9	asi
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
u	u	k7c2	u
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgMnSc1d1	průměrný
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
zásobě	zásoba	k1gFnSc6	zásoba
cca	cca	kA	cca
250-400	[number]	k4	250-400
g	g	kA	g
glykogenu	glykogen	k1gInSc2	glykogen
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
ve	v	k7c6	v
svalech	sval	k1gInPc6	sval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sportovci	sportovec	k1gMnPc1	sportovec
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zásoby	zásoba	k1gFnPc4	zásoba
glykogenu	glykogen	k1gInSc2	glykogen
až	až	k9	až
800	[number]	k4	800
g.	g.	k?	g.
Velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
glykogenových	glykogenův	k2eAgFnPc2d1	glykogenův
zásob	zásoba	k1gFnPc2	zásoba
má	mít	k5eAaImIp3nS	mít
strava	strava	k1gFnSc1	strava
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
strava	strava	k1gFnSc1	strava
obsahující	obsahující	k2eAgInPc1d1	obsahující
sacharidy	sacharid	k1gInPc7	sacharid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zásoba	zásoba	k1gFnSc1	zásoba
je	být	k5eAaImIp3nS	být
vyčerpána	vyčerpat	k5eAaPmNgFnS	vyčerpat
po	po	k7c6	po
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
minutách	minuta	k1gFnPc6	minuta
cvičení	cvičení	k1gNnSc2	cvičení
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
intenzitě	intenzita	k1gFnSc6	intenzita
cvičení	cvičení	k1gNnSc2	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Jaterní	jaterní	k2eAgInSc1d1	jaterní
glykogen	glykogen	k1gInSc1	glykogen
udržuje	udržovat	k5eAaImIp3nS	udržovat
stabilní	stabilní	k2eAgFnSc4d1	stabilní
hladinu	hladina	k1gFnSc4	hladina
krevního	krevní	k2eAgInSc2d1	krevní
cukru	cukr	k1gInSc2	cukr
zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
hladovění	hladovění	k1gNnSc6	hladovění
<g/>
,	,	kIx,	,
svalový	svalový	k2eAgInSc1d1	svalový
glykogen	glykogen	k1gInSc1	glykogen
je	být	k5eAaImIp3nS	být
okamžitě	okamžitě	k6eAd1	okamžitě
využitelný	využitelný	k2eAgMnSc1d1	využitelný
ke	k	k7c3	k
svalové	svalový	k2eAgFnSc3d1	svalová
práci	práce	k1gFnSc3	práce
jako	jako	k8xC	jako
bezprostřední	bezprostřední	k2eAgInSc4d1	bezprostřední
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
jsou	být	k5eAaImIp3nP	být
zásoby	zásoba	k1gFnPc4	zásoba
glykogenu	glykogen	k1gInSc2	glykogen
nízké	nízký	k2eAgInPc4d1	nízký
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
vyčerpané	vyčerpaný	k2eAgFnPc1d1	vyčerpaná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
nový	nový	k2eAgInSc4d1	nový
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
použity	použit	k2eAgFnPc1d1	použita
bílkoviny	bílkovina	k1gFnPc1	bílkovina
(	(	kIx(	(
<g/>
proteiny	protein	k1gInPc1	protein
<g/>
)	)	kIx)	)
a	a	k8xC	a
lipidy	lipid	k1gInPc1	lipid
(	(	kIx(	(
<g/>
tuky	tuk	k1gInPc1	tuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
koncentrace	koncentrace	k1gFnSc2	koncentrace
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
se	se	k3xPyFc4	se
jaterní	jaterní	k2eAgInSc1d1	jaterní
glykogen	glykogen	k1gInSc1	glykogen
štěpí	štěpit	k5eAaImIp3nS	štěpit
na	na	k7c4	na
glukózu	glukóza	k1gFnSc4	glukóza
(	(	kIx(	(
<g/>
glukóza-	glukóza-	k?	glukóza-
<g/>
1	[number]	k4	1
<g/>
-fosfát	osfát	k1gInSc1	-fosfát
→	→	k?	→
glukóza-	glukóza-	k?	glukóza-
<g/>
6	[number]	k4	6
<g/>
-fosfát	osfát	k1gInSc1	-fosfát
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
procesu	proces	k1gInSc2	proces
glykogenolýzy	glykogenolýza	k1gFnSc2	glykogenolýza
-	-	kIx~	-
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
glukóza	glukóza	k1gFnSc1	glukóza
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
uvolňována	uvolňovat	k5eAaImNgFnS	uvolňovat
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Svalový	svalový	k2eAgInSc1d1	svalový
glykogen	glykogen	k1gInSc1	glykogen
se	se	k3xPyFc4	se
štěpí	štěpit	k5eAaImIp3nS	štěpit
jen	jen	k9	jen
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
daného	daný	k2eAgInSc2d1	daný
svalu	sval	k1gInSc2	sval
a	a	k8xC	a
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
glukóza	glukóza	k1gFnSc1	glukóza
se	se	k3xPyFc4	se
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
nedostává	dostávat	k5eNaImIp3nS	dostávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
metabolizována	metabolizován	k2eAgFnSc1d1	metabolizována
jako	jako	k8xS	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
svalovou	svalový	k2eAgFnSc4d1	svalová
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
schopné	schopný	k2eAgInPc1d1	schopný
naopak	naopak	k6eAd1	naopak
glykogen	glykogen	k1gInSc4	glykogen
vyrábět	vyrábět	k5eAaImF	vyrábět
(	(	kIx(	(
<g/>
z	z	k7c2	z
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kroku	krok	k1gInSc6	krok
aktivací	aktivace	k1gFnPc2	aktivace
pomocí	pomoc	k1gFnPc2	pomoc
UDP-glukóza	UDPlukóza	k1gFnSc1	UDP-glukóza
difosforylázy	difosforyláza	k1gFnSc2	difosforyláza
na	na	k7c4	na
UDP-glukózu	UDPlukóza	k1gFnSc4	UDP-glukóza
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
glykogensyntáza	glykogensyntáza	k1gFnSc1	glykogensyntáza
katalyzuje	katalyzovat	k5eAaPmIp3nS	katalyzovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kondenzaci	kondenzace	k1gFnSc4	kondenzace
těchto	tento	k3xDgInPc2	tento
aktivovaných	aktivovaný	k2eAgInPc2d1	aktivovaný
monomerů	monomer	k1gInPc2	monomer
na	na	k7c4	na
glykogen	glykogen	k1gInSc4	glykogen
<g/>
.	.	kIx.	.
</s>
<s>
Rychlosti	rychlost	k1gFnPc1	rychlost
syntézy	syntéza	k1gFnSc2	syntéza
glykogenu	glykogen	k1gInSc2	glykogen
(	(	kIx(	(
<g/>
glykogeneze	glykogeneze	k1gFnSc1	glykogeneze
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
odbourávání	odbourávání	k1gNnSc1	odbourávání
(	(	kIx(	(
<g/>
glykogenolýza	glykogenolýza	k1gFnSc1	glykogenolýza
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
řízeny	řízen	k2eAgInPc4d1	řízen
hormony	hormon	k1gInPc4	hormon
inzulínem	inzulín	k1gInSc7	inzulín
<g/>
,	,	kIx,	,
adrenalinem	adrenalin	k1gInSc7	adrenalin
a	a	k8xC	a
glukagonem	glukagon	k1gInSc7	glukagon
<g/>
.	.	kIx.	.
</s>
