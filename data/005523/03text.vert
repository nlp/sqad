<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
je	být	k5eAaImIp3nS	být
náhlý	náhlý	k2eAgInSc4d1	náhlý
pohyb	pohyb	k1gInSc4	pohyb
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
vyvolaný	vyvolaný	k2eAgMnSc1d1	vyvolaný
uvolněním	uvolnění	k1gNnSc7	uvolnění
napětí	napětí	k1gNnSc2	napětí
–	–	k?	–
např.	např.	kA	např.
z	z	k7c2	z
neustálých	neustálý	k2eAgInPc2d1	neustálý
pohybů	pohyb	k1gInPc2	pohyb
zemských	zemský	k2eAgFnPc2d1	zemská
desek	deska	k1gFnPc2	deska
–	–	k?	–
podél	podél	k7c2	podél
zlomů	zlom	k1gInPc2	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnSc1d2	veliký
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
obvykle	obvykle	k6eAd1	obvykle
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
významné	významný	k2eAgInPc1d1	významný
zlomy	zlom	k1gInPc1	zlom
procházejí	procházet	k5eAaImIp3nP	procházet
(	(	kIx(	(
<g/>
západní	západní	k2eAgNnSc1d1	západní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
ostrovy	ostrov	k1gInPc4	ostrov
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
,	,	kIx,	,
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
Středomoří	středomoří	k1gNnSc1	středomoří
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Studiu	studio	k1gNnSc6	studio
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
geofyzika	geofyzika	k1gFnSc1	geofyzika
konkrétně	konkrétně	k6eAd1	konkrétně
její	její	k3xOp3gFnSc4	její
součást	součást	k1gFnSc4	součást
seismologie	seismologie	k1gFnSc2	seismologie
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
vzniku	vznik	k1gInSc2	vznik
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ohnisko	ohnisko	k1gNnSc1	ohnisko
neboli	neboli	k8xC	neboli
hypocentrum	hypocentrum	k1gNnSc1	hypocentrum
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
kolmý	kolmý	k2eAgInSc1d1	kolmý
průmět	průmět	k1gInSc1	průmět
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
epicentrum	epicentrum	k1gNnSc1	epicentrum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
síly	síla	k1gFnSc2	síla
otřesů	otřes	k1gInPc2	otřes
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dvě	dva	k4xCgFnPc1	dva
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
veličiny	veličina	k1gFnPc1	veličina
<g/>
,	,	kIx,	,
magnitudo	magnitudo	k1gNnSc1	magnitudo
a	a	k8xC	a
makroseismická	makroseismický	k2eAgFnSc1d1	makroseismická
intenzita	intenzita	k1gFnSc1	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Magnitudo	Magnitudo	k1gNnSc1	Magnitudo
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
z	z	k7c2	z
maximální	maximální	k2eAgFnSc2d1	maximální
výchylky	výchylka	k1gFnSc2	výchylka
seismometru	seismometr	k1gInSc2	seismometr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
makroseismická	makroseismický	k2eAgFnSc1d1	makroseismická
intenzita	intenzita	k1gFnSc1	intenzita
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
ze	z	k7c2	z
statistického	statistický	k2eAgNnSc2d1	statistické
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
účinků	účinek	k1gInPc2	účinek
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
na	na	k7c4	na
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
krajinu	krajina	k1gFnSc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
jednou	jednou	k6eAd1	jednou
hodnotou	hodnota	k1gFnSc7	hodnota
magnituda	magnitudo	k1gNnSc2	magnitudo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
hodnotami	hodnota	k1gFnPc7	hodnota
makroseismické	makroseismický	k2eAgFnSc2d1	makroseismická
intenzity	intenzita	k1gFnSc2	intenzita
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
poloze	poloha	k1gFnSc6	poloha
vůči	vůči	k7c3	vůči
hypocentru	hypocentrum	k1gNnSc3	hypocentrum
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Ebreichsdorfu	Ebreichsdorf	k1gInSc6	Ebreichsdorf
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2000	[number]	k4	2000
mělo	mít	k5eAaImAgNnS	mít
magnitudo	magnitudo	k1gNnSc1	magnitudo
4,8	[number]	k4	4,8
a	a	k8xC	a
makroseismickou	makroseismický	k2eAgFnSc4d1	makroseismická
intenzitu	intenzita	k1gFnSc4	intenzita
v	v	k7c6	v
epicentru	epicentrum	k1gNnSc6	epicentrum
VI	VI	kA	VI
stupňů	stupeň	k1gInPc2	stupeň
EMS-	EMS-	k1gFnSc2	EMS-
<g/>
98	[number]	k4	98
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
bylo	být	k5eAaImAgNnS	být
pocítěno	pocítit	k5eAaPmNgNnS	pocítit
jako	jako	k9	jako
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
stupeň	stupeň	k1gInSc1	stupeň
EMS-	EMS-	k1gFnSc2	EMS-
<g/>
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pohyb	pohyb	k1gInSc4	pohyb
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Slabá	slabý	k2eAgNnPc1d1	slabé
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
člověk	člověk	k1gMnSc1	člověk
buď	buď	k8xC	buď
vůbec	vůbec	k9	vůbec
nepocítí	pocítit	k5eNaPmIp3nS	pocítit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
na	na	k7c6	na
nestabilních	stabilní	k2eNgInPc6d1	nestabilní
předmětech	předmět	k1gInPc6	předmět
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
(	(	kIx(	(
<g/>
skleničky	sklenička	k1gFnPc1	sklenička
na	na	k7c6	na
policích	police	k1gFnPc6	police
<g/>
,	,	kIx,	,
lustr	lustr	k1gInSc1	lustr
<g/>
,	,	kIx,	,
hodiny	hodina	k1gFnPc1	hodina
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
častá	častý	k2eAgNnPc4d1	časté
i	i	k8xC	i
v	v	k7c6	v
seismicky	seismicky	k6eAd1	seismicky
klidnějších	klidný	k2eAgFnPc6d2	klidnější
oblastech	oblast	k1gFnPc6	oblast
–	–	k?	–
např.	např.	kA	např.
seismické	seismický	k2eAgInPc1d1	seismický
roje	roj	k1gInPc1	roj
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
Čechách	Čechy	k1gFnPc6	Čechy
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
lehkému	lehký	k2eAgNnSc3d1	lehké
popraskání	popraskání	k1gNnSc3	popraskání
zdí	zeď	k1gFnPc2	zeď
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
k	k	k7c3	k
slabým	slabý	k2eAgMnPc3d1	slabý
otřesům	otřes	k1gInPc3	otřes
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
důlní	důlní	k2eAgFnSc7d1	důlní
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
mohou	moct	k5eAaImIp3nP	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
i	i	k9	i
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
a	a	k8xC	a
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velká	k1gFnSc1	velká
a	a	k8xC	a
nízkofrekvenční	nízkofrekvenční	k2eAgNnSc1d1	nízkofrekvenční
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
(	(	kIx(	(
<g/>
LFE	LFE	kA	LFE
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
spouštět	spouštět	k5eAaImF	spouštět
slapové	slapový	k2eAgFnPc4d1	slapová
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Silnější	silný	k2eAgNnPc1d2	silnější
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
vázána	vázat	k5eAaImNgFnS	vázat
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
aktivní	aktivní	k2eAgFnPc4d1	aktivní
tektonické	tektonický	k2eAgFnPc4d1	tektonická
oblasti	oblast	k1gFnPc4	oblast
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
častý	častý	k2eAgInSc1d1	častý
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
celosvětově	celosvětově	k6eAd1	celosvětově
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
úkaz	úkaz	k1gInSc4	úkaz
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
však	však	k9	však
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
silnému	silný	k2eAgNnSc3d1	silné
zemětřesení	zemětřesení	k1gNnSc3	zemětřesení
v	v	k7c6	v
nejchudších	chudý	k2eAgInPc6d3	nejchudší
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
připravit	připravit	k5eAaPmF	připravit
o	o	k7c4	o
život	život	k1gInSc4	život
stovky	stovka	k1gFnSc2	stovka
či	či	k8xC	či
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
mnohamiliardové	mnohamiliardový	k2eAgFnPc4d1	mnohamiliardová
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Podmořská	podmořský	k2eAgNnPc1d1	podmořské
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
také	také	k9	také
velmi	velmi	k6eAd1	velmi
ničivé	ničivý	k2eAgFnSc2d1	ničivá
vlny	vlna	k1gFnSc2	vlna
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
také	také	k9	také
někdy	někdy	k6eAd1	někdy
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
světelné	světelný	k2eAgInPc1d1	světelný
efekty	efekt	k1gInPc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
efekty	efekt	k1gInPc1	efekt
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
podobné	podobný	k2eAgFnPc4d1	podobná
charakteristiky	charakteristika	k1gFnPc4	charakteristika
jako	jako	k8xS	jako
lámání	lámání	k1gNnPc4	lámání
bambusové	bambusový	k2eAgFnPc4d1	bambusová
tyčky	tyčka	k1gFnPc4	tyčka
<g/>
.	.	kIx.	.
</s>
<s>
Předvídat	předvídat	k5eAaImF	předvídat
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
<g/>
:	:	kIx,	:
řítivá	řítivý	k2eAgFnSc1d1	řítivý
–	–	k?	–
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
-	-	kIx~	-
vznikají	vznikat	k5eAaImIp3nP	vznikat
např.	např.	kA	např.
zřícením	zřícení	k1gNnSc7	zřícení
stropů	strop	k1gInPc2	strop
podzemních	podzemní	k2eAgFnPc2d1	podzemní
dutin	dutina	k1gFnPc2	dutina
v	v	k7c6	v
krasových	krasový	k2eAgFnPc6d1	krasová
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
dolovaných	dolovaný	k2eAgFnPc6d1	dolovaný
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
mělké	mělký	k2eAgNnSc4d1	mělké
hypocentrum	hypocentrum	k1gNnSc4	hypocentrum
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
lokálního	lokální	k2eAgInSc2d1	lokální
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
však	však	k9	však
způsobit	způsobit	k5eAaPmF	způsobit
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
sopečná	sopečný	k2eAgFnSc1d1	sopečná
(	(	kIx(	(
<g/>
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
<g/>
)	)	kIx)	)
–	–	k?	–
7	[number]	k4	7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
průvodním	průvodní	k2eAgInSc7d1	průvodní
jevem	jev	k1gInSc7	jev
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Hypocentra	hypocentrum	k1gNnPc1	hypocentrum
mají	mít	k5eAaImIp3nP	mít
vázaná	vázané	k1gNnPc1	vázané
na	na	k7c4	na
přívodní	přívodní	k2eAgFnPc4d1	přívodní
dráhy	dráha	k1gFnPc4	dráha
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
do	do	k7c2	do
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
mívají	mívat	k5eAaImIp3nP	mívat
lokální	lokální	k2eAgInSc4d1	lokální
význam	význam	k1gInSc4	význam
a	a	k8xC	a
malou	malý	k2eAgFnSc4d1	malá
intenzitu	intenzita	k1gFnSc4	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
rojích	roj	k1gInPc6	roj
<g/>
.	.	kIx.	.
tektonická	tektonický	k2eAgFnSc1d1	tektonická
(	(	kIx(	(
<g/>
dislokační	dislokační	k2eAgFnSc1d1	dislokační
<g/>
)	)	kIx)	)
–	–	k?	–
nejčastější	častý	k2eAgFnSc7d3	nejčastější
a	a	k8xC	a
nejzhoubnější	zhoubný	k2eAgFnSc7d3	nejzhoubnější
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
náhlým	náhlý	k2eAgNnSc7d1	náhlé
uvolněním	uvolnění	k1gNnSc7	uvolnění
nahromaděné	nahromaděný	k2eAgFnSc2d1	nahromaděná
elastické	elastický	k2eAgFnSc2d1	elastická
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
tektonicky	tektonicky	k6eAd1	tektonicky
aktivních	aktivní	k2eAgFnPc6d1	aktivní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
smykovému	smykový	k2eAgInSc3d1	smykový
pohybu	pohyb	k1gInSc2	pohyb
ker	kra	k1gFnPc2	kra
podél	podél	k7c2	podél
zlomových	zlomový	k2eAgFnPc2d1	zlomová
spár	spára	k1gFnPc2	spára
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInPc1d1	maximální
pohyby	pohyb	k1gInPc1	pohyb
v	v	k7c6	v
horizontálním	horizontální	k2eAgInSc6d1	horizontální
i	i	k8xC	i
vertikálním	vertikální	k2eAgInSc6d1	vertikální
směru	směr	k1gInSc6	směr
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
mnohametrových	mnohametrový	k2eAgFnPc2d1	mnohametrová
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Horizontální	horizontální	k2eAgInSc1d1	horizontální
rozměr	rozměr	k1gInSc1	rozměr
ohniska	ohnisko	k1gNnSc2	ohnisko
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
i	i	k9	i
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
:	:	kIx,	:
mělká	mělký	k2eAgFnSc1d1	mělká
–	–	k?	–
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
do	do	k7c2	do
70	[number]	k4	70
km	km	kA	km
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
85	[number]	k4	85
%	%	kIx~	%
všech	všecek	k3xTgNnPc2	všecek
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
středně	středně	k6eAd1	středně
hluboká	hluboký	k2eAgFnSc1d1	hluboká
–	–	k?	–
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
70	[number]	k4	70
až	až	k9	až
300	[number]	k4	300
km	km	kA	km
<g/>
,	,	kIx,	,
12	[number]	k4	12
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
hluboká	hluboký	k2eAgFnSc1d1	hluboká
–	–	k?	–
hlouběji	hluboko	k6eAd2	hluboko
než	než	k8xS	než
300	[number]	k4	300
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
do	do	k7c2	do
700	[number]	k4	700
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
tektonického	tektonický	k2eAgInSc2d1	tektonický
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
poruchami	porucha	k1gFnPc7	porucha
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
také	také	k9	také
jako	jako	k8xS	jako
zlomy	zlom	k1gInPc1	zlom
anebo	anebo	k8xC	anebo
dislokace	dislokace	k1gFnPc1	dislokace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
horninových	horninový	k2eAgInPc2d1	horninový
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
takovýchto	takovýto	k3xDgInPc2	takovýto
zlomů	zlom	k1gInPc2	zlom
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
při	při	k7c6	při
okrajích	okraj	k1gInPc6	okraj
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
interakci	interakce	k1gFnSc3	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
75	[number]	k4	75
%	%	kIx~	%
tektonických	tektonický	k2eAgNnPc2d1	tektonické
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
ohraničující	ohraničující	k2eAgInSc1d1	ohraničující
Pacifik	Pacifik	k1gInSc1	Pacifik
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
v	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
od	od	k7c2	od
Azor	Azory	k1gFnPc2	Azory
přes	přes	k7c4	přes
Severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
,	,	kIx,	,
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Apeninský	apeninský	k2eAgInSc4d1	apeninský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Dinárské	dinárský	k2eAgFnPc1d1	Dinárská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
až	až	k9	až
po	po	k7c4	po
Himálaj	Himálaj	k1gFnSc4	Himálaj
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
tektonická	tektonický	k2eAgNnPc1d1	tektonické
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
připadají	připadat	k5eAaImIp3nP	připadat
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
středooceánských	středooceánský	k2eAgInPc2d1	středooceánský
hřbetů	hřbet	k1gInPc2	hřbet
a	a	k8xC	a
v	v	k7c6	v
minimálním	minimální	k2eAgNnSc6d1	minimální
množství	množství	k1gNnSc6	množství
ještě	ještě	k9	ještě
na	na	k7c4	na
vnitrodesková	vnitrodeskový	k2eAgNnPc4d1	vnitrodeskový
zemětřesení	zemětřesení	k1gNnPc4	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgInSc1d1	masivní
výskyt	výskyt	k1gInSc1	výskyt
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
v	v	k7c6	v
konvergentních	konvergentní	k2eAgInPc6d1	konvergentní
<g/>
,	,	kIx,	,
divergentních	divergentní	k2eAgInPc6d1	divergentní
a	a	k8xC	a
transformních	transformní	k2eAgInPc6d1	transformní
okrajích	okraj	k1gInPc6	okraj
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
faktorů	faktor	k1gInPc2	faktor
podporující	podporující	k2eAgFnSc4d1	podporující
teorii	teorie	k1gFnSc4	teorie
deskové	deskový	k2eAgFnSc2d1	desková
tektoniky	tektonika	k1gFnSc2	tektonika
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
typy	typ	k1gInPc1	typ
okrajů	okraj	k1gInPc2	okraj
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
interakce	interakce	k1gFnSc2	interakce
produkují	produkovat	k5eAaImIp3nP	produkovat
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
typ	typ	k1gInSc4	typ
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
transformních	transformní	k2eAgInPc2d1	transformní
okrajů	okraj	k1gInPc2	okraj
mají	mít	k5eAaImIp3nP	mít
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
hloubku	hloubka	k1gFnSc4	hloubka
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
km	km	kA	km
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
konvergentních	konvergentní	k2eAgInPc2d1	konvergentní
okrajů	okraj	k1gInPc2	okraj
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
výskyt	výskyt	k1gInSc1	výskyt
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
hloubkách	hloubka	k1gFnPc6	hloubka
lemující	lemující	k2eAgFnSc4d1	lemující
poklesávající	poklesávající	k2eAgFnSc4d1	poklesávající
desku	deska	k1gFnSc4	deska
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
benioffova	benioffův	k2eAgFnSc1d1	benioffův
zóna	zóna	k1gFnSc1	zóna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zlom	zlom	k1gInSc1	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
aktivních	aktivní	k2eAgInPc6d1	aktivní
zlomech	zlom	k1gInPc6	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
představuje	představovat	k5eAaImIp3nS	představovat
oslabenou	oslabený	k2eAgFnSc4d1	oslabená
zónu	zóna	k1gFnSc4	zóna
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
bloky	blok	k1gInPc7	blok
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
horninami	hornina	k1gFnPc7	hornina
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
vysokotlakou	vysokotlaký	k2eAgFnSc7d1	vysokotlaká
metamorfózou	metamorfóza	k1gFnSc7	metamorfóza
(	(	kIx(	(
<g/>
mylonity	mylonita	k1gFnSc2	mylonita
<g/>
,	,	kIx,	,
tektonity	tektonita	k1gFnSc2	tektonita
až	až	k8xS	až
pseudotachylity	pseudotachylita	k1gFnSc2	pseudotachylita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
zlomu	zlom	k1gInSc2	zlom
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
jen	jen	k9	jen
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
stovky	stovka	k1gFnPc1	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Zlomy	zlom	k1gInPc1	zlom
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
litosférických	litosférický	k2eAgFnPc2d1	litosférická
desek	deska	k1gFnPc2	deska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
uvnitř	uvnitř	k7c2	uvnitř
těchto	tento	k3xDgFnPc2	tento
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pod	pod	k7c7	pod
jejím	její	k3xOp3gInSc7	její
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
horninové	horninový	k2eAgInPc1d1	horninový
bloky	blok	k1gInPc1	blok
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
podél	podél	k7c2	podél
zlomu	zlom	k1gInSc2	zlom
různými	různý	k2eAgFnPc7d1	různá
rychlostmi	rychlost	k1gFnPc7	rychlost
(	(	kIx(	(
<g/>
od	od	k7c2	od
několika	několik	k4yIc2	několik
mm	mm	kA	mm
až	až	k9	až
po	po	k7c6	po
cm	cm	kA	cm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
směr	směr	k1gInSc4	směr
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
protisměrný	protisměrný	k2eAgInSc1d1	protisměrný
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rovnoběžný	rovnoběžný	k2eAgInSc1d1	rovnoběžný
s	s	k7c7	s
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
rychlostmi	rychlost	k1gFnPc7	rychlost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
bloků	blok	k1gInPc2	blok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zaklínění	zaklínění	k1gNnSc3	zaklínění
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energii	energie	k1gFnSc4	energie
pohybu	pohyb	k1gInSc2	pohyb
akumulovat	akumulovat	k5eAaBmF	akumulovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
deformaci	deformace	k1gFnSc3	deformace
zaklíněných	zaklíněný	k2eAgFnPc2d1	zaklíněná
částí	část	k1gFnPc2	část
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Třecí	třecí	k2eAgFnSc2d1	třecí
síly	síla	k1gFnSc2	síla
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
oslabované	oslabovaný	k2eAgNnSc4d1	oslabované
silikagelem	silikagel	k1gInSc7	silikagel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgMnSc4d1	křemičitý
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
současně	současně	k6eAd1	současně
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
mazivo	mazivo	k1gNnSc1	mazivo
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
zvyšujícím	zvyšující	k2eAgMnSc7d1	zvyšující
se	se	k3xPyFc4	se
obsahem	obsah	k1gInSc7	obsah
SiO	SiO	k1gFnSc2	SiO
<g/>
2	[number]	k4	2
v	v	k7c6	v
hornině	hornina	k1gFnSc6	hornina
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
granit	granit	k1gInSc4	granit
či	či	k8xC	či
gabro	gabro	k1gNnSc4	gabro
<g/>
)	)	kIx)	)
klesá	klesat	k5eAaImIp3nS	klesat
tření	tření	k1gNnSc4	tření
v	v	k7c6	v
horninových	horninový	k2eAgInPc6d1	horninový
blocích	blok	k1gInPc6	blok
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
kumulace	kumulace	k1gFnSc2	kumulace
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
fyzikálním	fyzikální	k2eAgFnPc3d1	fyzikální
a	a	k8xC	a
chemickým	chemický	k2eAgFnPc3d1	chemická
změnám	změna	k1gFnPc3	změna
na	na	k7c6	na
zlomové	zlomový	k2eAgFnSc6d1	zlomová
ploše	plocha	k1gFnSc6	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
spouštění	spouštění	k1gNnSc6	spouštění
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
uvolňování	uvolňování	k1gNnSc1	uvolňování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dehydratací	dehydratace	k1gFnPc2	dehydratace
serpentinitu	serpentinit	k1gInSc2	serpentinit
<g/>
)	)	kIx)	)
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
tlakovým	tlakový	k2eAgNnSc7d1	tlakové
nasycením	nasycení	k1gNnSc7	nasycení
zaklesnutých	zaklesnutý	k2eAgFnPc2d1	zaklesnutá
částí	část	k1gFnPc2	část
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
mikrotrhlin	mikrotrhlina	k1gFnPc2	mikrotrhlina
<g/>
.	.	kIx.	.
</s>
<s>
Narůst	narůst	k5eAaPmF	narůst
počtu	počet	k1gInSc6	počet
mikrotrhlin	mikrotrhlina	k1gFnPc2	mikrotrhlina
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
překročení	překročení	k1gNnSc2	překročení
mezi	mezi	k7c4	mezi
pevnosti	pevnost	k1gFnPc4	pevnost
horniny	hornina	k1gFnSc2	hornina
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
trhliny	trhlina	k1gFnSc2	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
napětí	napětí	k1gNnSc1	napětí
naroste	narůst	k5eAaPmIp3nS	narůst
příliš	příliš	k6eAd1	příliš
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zaklesnuté	zaklesnutý	k2eAgInPc1d1	zaklesnutý
bloky	blok	k1gInPc1	blok
nenávratně	návratně	k6eNd1	návratně
posunou	posunout	k5eAaPmIp3nP	posunout
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pohybu	pohyb	k1gInSc2	pohyb
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
emisi	emise	k1gFnSc3	emise
seismických	seismický	k2eAgFnPc2d1	seismická
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
porušené	porušený	k2eAgFnSc2d1	porušená
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
ohnisko	ohnisko	k1gNnSc4	ohnisko
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
výskytu	výskyt	k1gInSc2	výskyt
trhliny	trhlina	k1gFnSc2	trhlina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hypocentrum	hypocentrum	k1gNnSc1	hypocentrum
a	a	k8xC	a
kolmý	kolmý	k2eAgInSc1d1	kolmý
průmět	průmět	k1gInSc1	průmět
hypocentra	hypocentrum	k1gNnSc2	hypocentrum
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
epicentrum	epicentrum	k1gNnSc4	epicentrum
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
horninové	horninový	k2eAgInPc1d1	horninový
bloky	blok	k1gInPc1	blok
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
uvolnění	uvolnění	k1gNnSc6	uvolnění
napětí	napětí	k1gNnSc2	napětí
nedostanou	dostat	k5eNaPmIp3nP	dostat
do	do	k7c2	do
poloh	poloha	k1gFnPc2	poloha
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
zaklesnutí	zaklesnutí	k1gNnSc3	zaklesnutí
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezí	mezit	k5eAaImIp3nS	mezit
původní	původní	k2eAgFnSc7d1	původní
a	a	k8xC	a
novou	nový	k2eAgFnSc7d1	nová
polohou	poloha	k1gFnSc7	poloha
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
diskontinuita	diskontinuita	k1gFnSc1	diskontinuita
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
řádově	řádově	k6eAd1	řádově
v	v	k7c6	v
několika	několik	k4yIc6	několik
metrech	metr	k1gInPc6	metr
<g/>
,	,	kIx,	,
při	pře	k1gFnSc4	pře
největších	veliký	k2eAgNnPc2d3	veliký
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
posunu	posun	k1gInSc2	posun
bloků	blok	k1gInPc2	blok
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rychlost	rychlost	k1gFnSc1	rychlost
posunutí	posunutí	k1gNnSc1	posunutí
a	a	k8xC	a
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yIgMnSc4	který
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
posunutí	posunutí	k1gNnSc3	posunutí
došlo	dojít	k5eAaPmAgNnS	dojít
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
náběhový	náběhový	k2eAgInSc1d1	náběhový
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Z	z	k7c2	z
hypocentra	hypocentrum	k1gNnSc2	hypocentrum
se	se	k3xPyFc4	se
trhlina	trhlina	k1gFnSc1	trhlina
šíří	šířit	k5eAaImIp3nS	šířit
po	po	k7c6	po
deformované	deformovaný	k2eAgFnSc6d1	deformovaná
zlomové	zlomový	k2eAgFnSc6d1	zlomová
ploše	plocha	k1gFnSc6	plocha
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
<g/>
,	,	kIx,	,
jediným	jediný	k2eAgNnSc7d1	jediné
omezením	omezení	k1gNnSc7	omezení
je	být	k5eAaImIp3nS	být
geometrie	geometrie	k1gFnSc1	geometrie
zlomu	zlom	k1gInSc2	zlom
a	a	k8xC	a
změna	změna	k1gFnSc1	změna
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pokud	pokud	k8xS	pokud
nenarazí	narazit	k5eNaPmIp3nS	narazit
na	na	k7c4	na
volný	volný	k2eAgInSc4d1	volný
povrch	povrch	k1gInSc4	povrch
anebo	anebo	k8xC	anebo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
deformací	deformace	k1gFnSc7	deformace
nepostačující	postačující	k2eNgFnSc7d1	nepostačující
na	na	k7c4	na
šíření	šíření	k1gNnSc4	šíření
trhliny	trhlina	k1gFnSc2	trhlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
šíření	šíření	k1gNnSc1	šíření
trhliny	trhlina	k1gFnSc2	trhlina
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
energii	energie	k1gFnSc4	energie
akumulovat	akumulovat	k5eAaBmF	akumulovat
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
místem	místem	k6eAd1	místem
menších	malý	k2eAgNnPc2d2	menší
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
dotřesů	dotřes	k1gInPc2	dotřes
<g/>
.	.	kIx.	.
</s>
<s>
Předtřesy	Předtřesa	k1gFnPc1	Předtřesa
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
menší	malý	k2eAgNnPc1d2	menší
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
předcházejí	předcházet	k5eAaImIp3nP	předcházet
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
otřesu	otřes	k1gInSc3	otřes
a	a	k8xC	a
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
příchod	příchod	k1gInSc4	příchod
hlavního	hlavní	k2eAgInSc2d1	hlavní
úderu	úder	k1gInSc2	úder
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
je	být	k5eAaImIp3nS	být
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
míst	místo	k1gNnPc2	místo
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
velkým	velký	k2eAgNnSc7d1	velké
napětím	napětí	k1gNnSc7	napětí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
uvolnění	uvolnění	k1gNnSc2	uvolnění
nerozšíří	rozšířit	k5eNaPmIp3nS	rozšířit
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
zlomovou	zlomový	k2eAgFnSc4d1	zlomová
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dojde	dojít	k5eAaPmIp3nS	dojít
tak	tak	k6eAd1	tak
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
lokálně	lokálně	k6eAd1	lokálně
omezenému	omezený	k2eAgNnSc3d1	omezené
uvolnění	uvolnění	k1gNnSc3	uvolnění
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
zlom	zlom	k1gInSc1	zlom
nehomogenní	homogenní	k2eNgInSc1d1	nehomogenní
a	a	k8xC	a
neudrží	udržet	k5eNaPmIp3nS	udržet
kumulaci	kumulace	k1gFnSc4	kumulace
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zemětřesným	zemětřesný	k2eAgInPc3d1	zemětřesný
rojům	roj	k1gInPc3	roj
-	-	kIx~	-
sérii	série	k1gFnSc4	série
slabších	slabý	k2eAgNnPc2d2	slabší
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
týdny	týden	k1gInPc4	týden
až	až	k8xS	až
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vícenásobné	vícenásobný	k2eAgNnSc1d1	vícenásobné
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
nastává	nastávat	k5eAaImIp3nS	nastávat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
krátkém	krátký	k2eAgInSc6d1	krátký
časovém	časový	k2eAgInSc6d1	časový
sledu	sled	k1gInSc6	sled
(	(	kIx(	(
<g/>
sekundy	sekunda	k1gFnSc2	sekunda
až	až	k8xS	až
minuty	minuta	k1gFnSc2	minuta
<g/>
)	)	kIx)	)
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
energie	energie	k1gFnSc1	energie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
sérií	série	k1gFnPc2	série
izolovaných	izolovaný	k2eAgFnPc2d1	izolovaná
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
není	být	k5eNaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
na	na	k7c6	na
zlomě	zloma	k1gFnSc6	zloma
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
krátkou	krátký	k2eAgFnSc7d1	krátká
epizodou	epizoda	k1gFnSc7	epizoda
tektonického	tektonický	k2eAgInSc2d1	tektonický
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uvolnění	uvolnění	k1gNnSc6	uvolnění
nahromaděné	nahromaděný	k2eAgFnSc2d1	nahromaděná
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Hypocentrum	hypocentrum	k1gNnSc1	hypocentrum
a	a	k8xC	a
porušená	porušený	k2eAgFnSc1d1	porušená
část	část	k1gFnSc1	část
zlomové	zlomový	k2eAgFnSc2d1	zlomová
plochy	plocha	k1gFnSc2	plocha
však	však	k9	však
zpravidla	zpravidla	k6eAd1	zpravidla
nebývá	bývat	k5eNaImIp3nS	bývat
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
předcházející	předcházející	k2eAgFnSc7d1	předcházející
oblastí	oblast	k1gFnSc7	oblast
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seismická	seismický	k2eAgFnSc1d1	seismická
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Seismické	seismický	k2eAgFnPc1d1	seismická
vlny	vlna	k1gFnPc1	vlna
jsou	být	k5eAaImIp3nP	být
elastické	elastický	k2eAgFnPc1d1	elastická
vlny	vlna	k1gFnPc1	vlna
šířící	šířící	k2eAgFnPc1d1	šířící
se	se	k3xPyFc4	se
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
tělese	těleso	k1gNnSc6	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vyzařované	vyzařovaný	k2eAgFnPc1d1	vyzařovaná
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
šíření	šíření	k1gNnSc2	šíření
trhliny	trhlina	k1gFnSc2	trhlina
ve	v	k7c6	v
zlomě	zloma	k1gFnSc6	zloma
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
projevů	projev	k1gInPc2	projev
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
a	a	k8xC	a
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
asi	asi	k9	asi
30	[number]	k4	30
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
zbytek	zbytek	k1gInSc1	zbytek
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
uvolnění	uvolnění	k1gNnSc4	uvolnění
zaklesnutých	zaklesnutý	k2eAgInPc2d1	zaklesnutý
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
posun	posun	k1gInSc1	posun
a	a	k8xC	a
přeměna	přeměna	k1gFnSc1	přeměna
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vytvářeny	vytvářit	k5eAaPmNgFnP	vytvářit
i	i	k9	i
fázovými	fázový	k2eAgInPc7d1	fázový
přechody	přechod	k1gInPc7	přechod
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
nitru	nitro	k1gNnSc6	nitro
<g/>
,	,	kIx,	,
dopady	dopad	k1gInPc4	dopad
meteoritů	meteorit	k1gInPc2	meteorit
<g/>
,	,	kIx,	,
svahovými	svahový	k2eAgInPc7d1	svahový
sesuvy	sesuv	k1gInPc7	sesuv
anebo	anebo	k8xC	anebo
projevy	projev	k1gInPc1	projev
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
(	(	kIx(	(
<g/>
výbuchy	výbuch	k1gInPc1	výbuch
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
rakety	raketa	k1gFnSc2	raketa
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
šíření	šíření	k1gNnSc2	šíření
zemětřesných	zemětřesný	k2eAgFnPc2d1	zemětřesná
vln	vlna	k1gFnPc2	vlna
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stavbě	stavba	k1gFnSc6	stavba
Země	zem	k1gFnSc2	zem
pomocí	pomocí	k7c2	pomocí
vzniku	vznik	k1gInSc2	vznik
seismických	seismický	k2eAgInPc2d1	seismický
modelů	model	k1gInPc2	model
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Seismické	seismický	k2eAgFnSc2d1	seismická
vlny	vlna	k1gFnSc2	vlna
se	se	k3xPyFc4	se
rozděluji	rozdělovat	k5eAaImIp1nS	rozdělovat
na	na	k7c4	na
povrchové	povrchový	k2eAgFnPc4d1	povrchová
a	a	k8xC	a
prostorové	prostorový	k2eAgFnPc4d1	prostorová
<g/>
.	.	kIx.	.
</s>
<s>
Povrchové	povrchový	k2eAgFnPc1d1	povrchová
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
jen	jen	k9	jen
po	po	k7c6	po
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
hloubky	hloubka	k1gFnSc2	hloubka
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
hloubka	hloubka	k1gFnSc1	hloubka
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
periodě	perioda	k1gFnSc6	perioda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
dlouhoperiodické	dlouhoperiodický	k2eAgFnSc2d1	dlouhoperiodická
povrchové	povrchový	k2eAgFnSc2d1	povrchová
seismické	seismický	k2eAgFnSc2d1	seismická
vlny	vlna	k1gFnSc2	vlna
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
okolo	okolo	k7c2	okolo
200	[number]	k4	200
s	s	k7c7	s
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
svrchního	svrchní	k2eAgInSc2d1	svrchní
pláště	plášť	k1gInSc2	plášť
<g/>
)	)	kIx)	)
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
Rayleighovy	Rayleighova	k1gFnPc4	Rayleighova
vlny	vlna	k1gFnSc2	vlna
a	a	k8xC	a
Loveovy	Loveův	k2eAgFnSc2d1	Loveův
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Prostorové	prostorový	k2eAgFnPc1d1	prostorová
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
můžou	můžou	k?	můžou
šířit	šířit	k5eAaImF	šířit
i	i	k9	i
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
zemského	zemský	k2eAgNnSc2d1	zemské
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
se	se	k3xPyFc4	se
na	na	k7c4	na
primární	primární	k2eAgFnSc4d1	primární
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
P-vlny	Plna	k1gFnSc2	P-vlna
<g/>
,	,	kIx,	,
či	či	k8xC	či
podélné	podélný	k2eAgFnPc1d1	podélná
vlny	vlna	k1gFnPc1	vlna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
celou	celý	k2eAgFnSc7d1	celá
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
na	na	k7c4	na
sekundární	sekundární	k2eAgFnPc4d1	sekundární
(	(	kIx(	(
<g/>
S-vlny	Slna	k1gFnPc4	S-vlna
<g/>
,	,	kIx,	,
či	či	k8xC	či
příčné	příčný	k2eAgFnPc1d1	příčná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemohou	moct	k5eNaImIp3nP	moct
procházet	procházet	k5eAaImF	procházet
kapalinami	kapalina	k1gFnPc7	kapalina
a	a	k8xC	a
tedy	tedy	k9	tedy
vnějším	vnější	k2eAgNnSc7d1	vnější
jádrem	jádro	k1gNnSc7	jádro
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
seismické	seismický	k2eAgFnSc2d1	seismická
vlny	vlna	k1gFnSc2	vlna
šíří	šířit	k5eAaImIp3nP	šířit
tělesem	těleso	k1gNnSc7	těleso
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
postupnému	postupný	k2eAgInSc3d1	postupný
útlumu	útlum	k1gInSc3	útlum
vlivem	vlivem	k7c2	vlivem
zmenšování	zmenšování	k1gNnSc2	zmenšování
jejich	jejich	k3xOp3gFnSc2	jejich
amplitudy	amplituda	k1gFnSc2	amplituda
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
horniny	hornina	k1gFnPc1	hornina
se	se	k3xPyFc4	se
nechovají	chovat	k5eNaImIp3nP	chovat
zcela	zcela	k6eAd1	zcela
elasticky	elasticky	k6eAd1	elasticky
a	a	k8xC	a
část	část	k1gFnSc4	část
mechanické	mechanický	k2eAgFnSc2d1	mechanická
energie	energie	k1gFnSc2	energie
vlnění	vlnění	k1gNnSc2	vlnění
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Energii	energie	k1gFnSc4	energie
uvolněnou	uvolněný	k2eAgFnSc4d1	uvolněná
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
část	část	k1gFnSc1	část
vyzářená	vyzářený	k2eAgFnSc1d1	vyzářená
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
seismických	seismický	k2eAgFnPc2d1	seismická
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
seismometry	seismometr	k1gInPc4	seismometr
a	a	k8xC	a
seismografy	seismograf	k1gInPc4	seismograf
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
stavby	stavba	k1gFnPc4	stavba
měří	měřit	k5eAaImIp3nS	měřit
makroseismická	makroseismický	k2eAgFnSc1d1	makroseismická
stupnice	stupnice	k1gFnSc1	stupnice
intenzity	intenzita	k1gFnSc2	intenzita
a	a	k8xC	a
na	na	k7c4	na
odhad	odhad	k1gInSc4	odhad
velikosti	velikost	k1gFnSc2	velikost
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
magnitudo	magnitudo	k1gNnSc1	magnitudo
a	a	k8xC	a
seismické	seismický	k2eAgInPc1d1	seismický
momenty	moment	k1gInPc1	moment
<g/>
.	.	kIx.	.
</s>
<s>
Makroseismické	makroseismický	k2eAgInPc1d1	makroseismický
účinky	účinek	k1gInPc1	účinek
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
jsou	být	k5eAaImIp3nP	být
účinky	účinek	k1gInPc1	účinek
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
a	a	k8xC	a
lidech	člověk	k1gMnPc6	člověk
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
definované	definovaný	k2eAgInPc1d1	definovaný
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
tzv.	tzv.	kA	tzv.
makroseismické	makroseismický	k2eAgFnSc2d1	makroseismická
intenzity	intenzita	k1gFnSc2	intenzita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
určována	určovat	k5eAaImNgFnS	určovat
škálou	škála	k1gFnSc7	škála
makrosiesmické	makrosiesmický	k2eAgFnSc2d1	makrosiesmický
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc4	jeden
stupeň	stupeň	k1gInSc4	stupeň
této	tento	k3xDgFnSc2	tento
stupnice	stupnice	k1gFnSc2	stupnice
je	být	k5eAaImIp3nS	být
charakterizovaný	charakterizovaný	k2eAgInSc1d1	charakterizovaný
souborem	soubor	k1gInSc7	soubor
pozorovatelných	pozorovatelný	k2eAgInPc2d1	pozorovatelný
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
třetí	třetí	k4xOgInSc4	třetí
stupeň	stupeň	k1gInSc4	stupeň
Mercalliho	Mercalli	k1gMnSc2	Mercalli
stupnice	stupnice	k1gFnSc2	stupnice
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
slabými	slabý	k2eAgInPc7d1	slabý
otřesy	otřes	k1gInPc7	otřes
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
může	moct	k5eAaImIp3nS	moct
pocítit	pocítit	k5eAaPmF	pocítit
člověk	člověk	k1gMnSc1	člověk
na	na	k7c6	na
vyšších	vysoký	k2eAgNnPc6d2	vyšší
poschodích	poschodí	k1gNnPc6	poschodí
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k8xS	jako
projev	projev	k1gInSc4	projev
rozkývání	rozkývání	k1gNnSc2	rozkývání
zavěšených	zavěšený	k2eAgInPc2d1	zavěšený
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
stupeň	stupeň	k1gInSc4	stupeň
této	tento	k3xDgFnSc2	tento
stupnice	stupnice	k1gFnSc2	stupnice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
projevuje	projevovat	k5eAaImIp3nS	projevovat
destrukcemi	destrukce	k1gFnPc7	destrukce
velkých	velký	k2eAgFnPc2d1	velká
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
zdvíháním	zdvíhání	k1gNnSc7	zdvíhání
či	či	k8xC	či
poklesáváním	poklesávání	k1gNnSc7	poklesávání
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
trhlin	trhlina	k1gFnPc2	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
dvanácti	dvanáct	k4xCc2	dvanáct
stupňové	stupňový	k2eAgFnPc4d1	stupňová
stupnice	stupnice	k1gFnSc2	stupnice
(	(	kIx(	(
<g/>
MCS	MCS	kA	MCS
<g/>
,	,	kIx,	,
MM	mm	kA	mm
<g/>
,	,	kIx,	,
EMS-	EMS-	k1gFnSc1	EMS-
<g/>
98	[number]	k4	98
<g/>
,	,	kIx,	,
MKS	MKS	kA	MKS
<g/>
)	)	kIx)	)
a	a	k8xC	a
sedmistupňová	sedmistupňový	k2eAgFnSc1d1	sedmistupňová
stupnice	stupnice	k1gFnSc1	stupnice
JMA	JMA	kA	JMA
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
stupnice	stupnice	k1gFnPc1	stupnice
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgFnP	využívat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
JMA	JMA	kA	JMA
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
pro	pro	k7c4	pro
evropské	evropský	k2eAgInPc4d1	evropský
státy	stát	k1gInPc4	stát
EMS-	EMS-	k1gFnSc2	EMS-
<g/>
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Definování	definování	k1gNnSc1	definování
síly	síla	k1gFnSc2	síla
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
pomocí	pomocí	k7c2	pomocí
stupnic	stupnice	k1gFnPc2	stupnice
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
subjektivní	subjektivní	k2eAgNnSc1d1	subjektivní
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
odhadu	odhad	k1gInSc2	odhad
rozsahu	rozsah	k1gInSc2	rozsah
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
objektivnější	objektivní	k2eAgInSc1d2	objektivnější
popis	popis	k1gInSc1	popis
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
magnituda	magnitudo	k1gNnSc2	magnitudo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
funkcí	funkce	k1gFnSc7	funkce
dekadického	dekadický	k2eAgInSc2d1	dekadický
logaritmu	logaritmus	k1gInSc2	logaritmus
amplitudy	amplituda	k1gFnSc2	amplituda
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
síly	síla	k1gFnSc2	síla
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
pomocí	pomocí	k7c2	pomocí
magnituda	magnitudo	k1gNnSc2	magnitudo
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
poprvé	poprvé	k6eAd1	poprvé
japonský	japonský	k2eAgMnSc1d1	japonský
seismolog	seismolog	k1gMnSc1	seismolog
Kijoo	Kijoo	k6eAd1	Kijoo
Wadati	Wadat	k1gMnPc1	Wadat
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
uvedli	uvést	k5eAaPmAgMnP	uvést
Charles	Charles	k1gMnSc1	Charles
Richter	Richter	k1gMnSc1	Richter
a	a	k8xC	a
Beno	Beno	k1gMnSc1	Beno
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gFnSc2	jejich
práce	práce	k1gFnSc2	práce
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
analyzovat	analyzovat	k5eAaImF	analyzovat
seismogramy	seismogram	k1gInPc4	seismogram
pro	pro	k7c4	pro
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
měří	měřit	k5eAaImIp3nS	měřit
tzv.	tzv.	kA	tzv.
lokální	lokální	k2eAgNnSc1d1	lokální
magnitudo	magnitudo	k1gNnSc1	magnitudo
(	(	kIx(	(
<g/>
ML	ml	kA	ml
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
dekadický	dekadický	k2eAgInSc1d1	dekadický
logaritmus	logaritmus	k1gInSc1	logaritmus
poměru	poměr	k1gInSc2	poměr
amplitudy	amplituda	k1gFnSc2	amplituda
a	a	k8xC	a
periody	perioda	k1gFnSc2	perioda
seismické	seismický	k2eAgFnSc2d1	seismická
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
veřejnosti	veřejnost	k1gFnSc2	veřejnost
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
Richterova	Richterův	k2eAgFnSc1d1	Richterova
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Richter	Richter	k1gMnSc1	Richter
se	se	k3xPyFc4	se
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
pojmenování	pojmenování	k1gNnSc6	pojmenování
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
magnitudou	magnitudý	k2eAgFnSc4d1	magnitudý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Richterovou	Richterův	k2eAgFnSc7d1	Richterova
stupnicí	stupnice	k1gFnSc7	stupnice
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
častá	častý	k2eAgFnSc1d1	častá
chyba	chyba	k1gFnSc1	chyba
mezi	mezi	k7c7	mezi
veřejností	veřejnost	k1gFnSc7	veřejnost
využívat	využívat	k5eAaPmF	využívat
tuto	tento	k3xDgFnSc4	tento
stupnici	stupnice	k1gFnSc4	stupnice
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
stupnice	stupnice	k1gFnSc1	stupnice
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
vymyšlena	vymyslet	k5eAaPmNgFnS	vymyslet
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
síly	síla	k1gFnSc2	síla
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
velká	velký	k2eAgNnPc1d1	velké
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
označujeme	označovat	k5eAaImIp1nP	označovat
ta	ten	k3xDgNnPc1	ten
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgFnSc1d1	schopna
vážně	vážně	k6eAd1	vážně
poškodit	poškodit	k5eAaPmF	poškodit
budovy	budova	k1gFnPc4	budova
blízko	blízko	k7c2	blízko
epicentra	epicentrum	k1gNnSc2	epicentrum
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
6	[number]	k4	6
<g/>
.	.	kIx.	.
stupni	stupeň	k1gInPc7	stupeň
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ničivých	ničivý	k2eAgNnPc2d1	ničivé
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hustě	hustě	k6eAd1	hustě
osídlených	osídlený	k2eAgFnPc6d1	osídlená
oblastech	oblast	k1gFnPc6	oblast
většinou	většinou	k6eAd1	většinou
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
v	v	k7c6	v
řidčeji	řídce	k6eAd2	řídce
osídlených	osídlený	k2eAgFnPc6d1	osídlená
oblastech	oblast	k1gFnPc6	oblast
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
100	[number]	k4	100
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
velká	velký	k2eAgNnPc1d1	velké
ničivá	ničivý	k2eAgNnPc1d1	ničivé
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
označují	označovat	k5eAaImIp3nP	označovat
různí	různý	k2eAgMnPc1d1	různý
autoři	autor	k1gMnPc1	autor
ta	ten	k3xDgNnPc4	ten
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
<g/>
,	,	kIx,	,
20	[number]	k4	20
nebo	nebo	k8xC	nebo
50	[number]	k4	50
tisíci	tisíc	k4xCgInPc7	tisíc
oběťmi	oběť	k1gFnPc7	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nejméně	málo	k6eAd3	málo
126	[number]	k4	126
ničivých	ničivý	k2eAgNnPc2d1	ničivé
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
–	–	k?	–
podle	podle	k7c2	podle
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
11	[number]	k4	11
až	až	k9	až
25	[number]	k4	25
velkých	velký	k2eAgNnPc2d1	velké
ničivých	ničivý	k2eAgNnPc2d1	ničivé
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
ničivé	ničivý	k2eAgNnSc1d1	ničivé
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
období	období	k1gNnSc6	období
průměrný	průměrný	k2eAgInSc4d1	průměrný
počet	počet	k1gInSc4	počet
obětí	oběť	k1gFnPc2	oběť
60	[number]	k4	60
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
61	[number]	k4	61
<g/>
)	)	kIx)	)
tisíc	tisíc	k4xCgInSc1	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
jedno	jeden	k4xCgNnSc4	jeden
velké	velký	k2eAgNnSc4d1	velké
ničivé	ničivý	k2eAgNnSc4d1	ničivé
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
průměrně	průměrně	k6eAd1	průměrně
za	za	k7c4	za
4,5	[number]	k4	4,5
až	až	k9	až
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
nejméně	málo	k6eAd3	málo
1	[number]	k4	1
ničivé	ničivý	k2eAgNnSc4d1	ničivé
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
ročně	ročně	k6eAd1	ročně
<g/>
;	;	kIx,	;
1	[number]	k4	1
velké	velká	k1gFnSc2	velká
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
několikrát	několikrát	k6eAd1	několikrát
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ČT	ČT	kA	ČT
<g/>
1	[number]	k4	1
<g/>
;	;	kIx,	;
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
bývají	bývat	k5eAaImIp3nP	bývat
citelná	citelný	k2eAgNnPc1d1	citelné
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
zaznamenána	zaznamenat	k5eAaPmNgNnP	zaznamenat
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otřesy	otřes	k1gInPc1	otřes
bývají	bývat	k5eAaImIp3nP	bývat
jen	jen	k9	jen
slabé	slabý	k2eAgFnPc1d1	slabá
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
škály	škála	k1gFnSc2	škála
<g/>
.	.	kIx.	.
</s>
<s>
Nejaktivnějšími	aktivní	k2eAgFnPc7d3	nejaktivnější
oblastmi	oblast	k1gFnPc7	oblast
jsou	být	k5eAaImIp3nP	být
mariánskolázeňský	mariánskolázeňský	k2eAgInSc4d1	mariánskolázeňský
zlom	zlom	k1gInSc4	zlom
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Kraslicko	Kraslicko	k1gNnSc4	Kraslicko
v	v	k7c6	v
Karlovarském	karlovarský	k2eAgInSc6d1	karlovarský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
hronovsko-poříčský	hronovskooříčský	k2eAgInSc1d1	hronovsko-poříčský
zlom	zlom	k1gInSc1	zlom
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
zemětřesných	zemětřesný	k2eAgInPc6d1	zemětřesný
rojích	roj	k1gInPc6	roj
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnějším	silný	k2eAgInSc7d3	nejsilnější
zaznamenaným	zaznamenaný	k2eAgInSc7d1	zaznamenaný
rojem	roj	k1gInSc7	roj
na	na	k7c6	na
Kraslicku	Kraslick	k1gInSc6	Kraslick
byl	být	k5eAaImAgInS	být
roj	roj	k1gInSc1	roj
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
s	s	k7c7	s
epicentrem	epicentrum	k1gNnSc7	epicentrum
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Nový	nový	k2eAgInSc1d1	nový
Kostel	kostel	k1gInSc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgInSc4d3	nejsilnější
otřes	otřes	k1gInSc4	otřes
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
stupnice	stupnice	k1gFnSc2	stupnice
magnitudo	magnitudo	k1gNnSc1	magnitudo
4,8	[number]	k4	4,8
až	až	k8xS	až
5,0	[number]	k4	5,0
Nejsilnější	silný	k2eAgInSc4d3	nejsilnější
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
na	na	k7c6	na
Hronovsku	Hronovsko	k1gNnSc6	Hronovsko
bylo	být	k5eAaImAgNnS	být
naměřeno	naměřit	k5eAaBmNgNnS	naměřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
sílu	síla	k1gFnSc4	síla
4,7	[number]	k4	4,7
Richterovy	Richterův	k2eAgFnSc2d1	Richterova
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starších	starší	k1gMnPc2	starší
pak	pak	k6eAd1	pak
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1616	[number]	k4	1616
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
oblastmi	oblast	k1gFnPc7	oblast
s	s	k7c7	s
občasnou	občasný	k2eAgFnSc7d1	občasná
aktivitou	aktivita	k1gFnSc7	aktivita
jsou	být	k5eAaImIp3nP	být
Český	český	k2eAgInSc4d1	český
les	les	k1gInSc4	les
<g/>
,	,	kIx,	,
Opavsko	Opavsko	k1gNnSc4	Opavsko
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
Krušných	krušný	k2eAgInPc2d1	krušný
hor.	hor.	k?	hor.
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
na	na	k7c6	na
Ostravsku	Ostravsko	k1gNnSc6	Ostravsko
<g/>
,	,	kIx,	,
Kladensku	Kladensko	k1gNnSc6	Kladensko
a	a	k8xC	a
v	v	k7c6	v
Podkrušnohorské	podkrušnohorský	k2eAgFnSc6d1	Podkrušnohorská
pánvi	pánev	k1gFnSc6	pánev
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
otřesům	otřes	k1gInPc3	otřes
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
intenzivní	intenzivní	k2eAgFnSc7d1	intenzivní
důlní	důlní	k2eAgFnSc7d1	důlní
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
zaznamenávány	zaznamenáván	k2eAgInPc1d1	zaznamenáván
dozvuky	dozvuk	k1gInPc1	dozvuk
alpských	alpský	k2eAgNnPc2d1	alpské
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Ohnisko	ohnisko	k1gNnSc1	ohnisko
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc1	prostor
konečných	konečný	k2eAgInPc2d1	konečný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
délkové	délkový	k2eAgInPc1d1	délkový
rozměry	rozměr	k1gInPc1	rozměr
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Hypocentrum	hypocentrum	k1gNnSc1	hypocentrum
je	být	k5eAaImIp3nS	být
bodem	bod	k1gInSc7	bod
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
nahrazováno	nahrazován	k2eAgNnSc1d1	nahrazováno
ohnisko	ohnisko	k1gNnSc1	ohnisko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kladeno	klást	k5eAaImNgNnS	klást
do	do	k7c2	do
těžiště	těžiště	k1gNnSc2	těžiště
ohniska	ohnisko	k1gNnSc2	ohnisko
<g/>
.	.	kIx.	.
</s>
<s>
Epicentrum	epicentrum	k1gNnSc1	epicentrum
je	být	k5eAaImIp3nS	být
kolmý	kolmý	k2eAgInSc4d1	kolmý
průmět	průmět	k1gInSc4	průmět
hypocentra	hypocentrum	k1gNnSc2	hypocentrum
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
ohniska	ohnisko	k1gNnSc2	ohnisko
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
hypocentrem	hypocentrum	k1gNnSc7	hypocentrum
a	a	k8xC	a
epicentrem	epicentrum	k1gNnSc7	epicentrum
<g/>
.	.	kIx.	.
</s>
<s>
Epicentrální	Epicentrální	k2eAgFnSc1d1	Epicentrální
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
epicentra	epicentrum	k1gNnSc2	epicentrum
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
uvést	uvést	k5eAaPmF	uvést
buď	buď	k8xC	buď
jako	jako	k9	jako
délku	délka	k1gFnSc4	délka
ortodromy	ortodroma	k1gFnSc2	ortodroma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
velikost	velikost	k1gFnSc4	velikost
úhlu	úhel	k1gInSc2	úhel
vzhedem	vzhed	k1gInSc7	vzhed
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Epicentrální	Epicentrální	k2eAgInSc1d1	Epicentrální
čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
projeví	projevit	k5eAaPmIp3nS	projevit
v	v	k7c6	v
epicentru	epicentrum	k1gNnSc6	epicentrum
<g/>
.	.	kIx.	.
</s>
<s>
Pleistoseistní	Pleistoseistní	k2eAgFnSc1d1	Pleistoseistní
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
okolí	okolí	k1gNnSc4	okolí
epicentra	epicentrum	k1gNnSc2	epicentrum
nejvíce	nejvíce	k6eAd1	nejvíce
postižené	postižený	k2eAgNnSc1d1	postižené
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Intenzita	intenzita	k1gFnSc1	intenzita
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
je	být	k5eAaImIp3nS	být
veličina	veličina	k1gFnSc1	veličina
charakterizující	charakterizující	k2eAgFnSc1d1	charakterizující
velikost	velikost	k1gFnSc1	velikost
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnPc2	pozorování
makroseismických	makroseismický	k2eAgInPc2d1	makroseismický
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
od	od	k7c2	od
pleistoseistní	pleistoseistní	k2eAgFnSc2d1	pleistoseistní
oblasti	oblast	k1gFnSc2	oblast
klesá	klesat	k5eAaImIp3nS	klesat
intenzita	intenzita	k1gFnSc1	intenzita
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesné	zemětřesný	k2eAgInPc4d1	zemětřesný
roje	roj	k1gInPc4	roj
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
následujících	následující	k2eAgInPc2d1	následující
otřesů	otřes	k1gInPc2	otřes
podobné	podobný	k2eAgFnSc2d1	podobná
intenzity	intenzita	k1gFnSc2	intenzita
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
není	být	k5eNaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
určit	určit	k5eAaPmF	určit
hlavní	hlavní	k2eAgInSc4d1	hlavní
otřes	otřes	k1gInSc4	otřes
<g/>
.	.	kIx.	.
</s>
