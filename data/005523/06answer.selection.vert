<s>
Místo	místo	k7c2	místo
vzniku	vznik	k1gInSc2	vznik
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ohnisko	ohnisko	k1gNnSc1	ohnisko
neboli	neboli	k8xC	neboli
hypocentrum	hypocentrum	k1gNnSc1	hypocentrum
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
kolmý	kolmý	k2eAgInSc1d1	kolmý
průmět	průmět	k1gInSc1	průmět
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
epicentrum	epicentrum	k1gNnSc1	epicentrum
<g/>
.	.	kIx.	.
</s>
