<s>
Hokej	hokej	k1gInSc1	hokej
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
dva	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc4	tým
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
puk	puk	k1gInSc1	puk
nebo	nebo	k8xC	nebo
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
míček	míček	k1gInSc1	míček
do	do	k7c2	do
soupeřovy	soupeřův	k2eAgFnSc2d1	soupeřova
branky	branka	k1gFnSc2	branka
pomocí	pomocí	k7c2	pomocí
hokejek	hokejka	k1gFnPc2	hokejka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
formy	forma	k1gFnPc1	forma
hokeje	hokej	k1gInSc2	hokej
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
pozemní	pozemní	k2eAgInSc4d1	pozemní
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
s	s	k7c7	s
míčkem	míček	k1gInSc7	míček
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
s	s	k7c7	s
umělou	umělý	k2eAgFnSc7d1	umělá
trávou	tráva	k1gFnSc7	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
hrával	hrávat	k5eAaImAgMnS	hrávat
na	na	k7c6	na
přírodním	přírodní	k2eAgInSc6d1	přírodní
travnatém	travnatý	k2eAgInSc6d1	travnatý
povrchu	povrch	k1gInSc6	povrch
(	(	kIx(	(
<g/>
olympijský	olympijský	k2eAgInSc4d1	olympijský
sport	sport	k1gInSc4	sport
<g/>
)	)	kIx)	)
lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
ledě	led	k1gInSc6	led
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
černým	černý	k2eAgNnSc7d1	černé
gumovým	gumový	k2eAgNnSc7d1	gumové
diskem	disco	k1gNnSc7	disco
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
pukem	puk	k1gInSc7	puk
(	(	kIx(	(
<g/>
olympijský	olympijský	k2eAgInSc1d1	olympijský
sport	sport	k1gInSc1	sport
<g/>
)	)	kIx)	)
sledge	sledgat	k5eAaPmIp3nS	sledgat
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
varianta	varianta	k1gFnSc1	varianta
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
s	s	k7c7	s
těžkým	těžký	k2eAgNnSc7d1	těžké
<g />
.	.	kIx.	.
</s>
<s>
postižením	postižení	k1gNnSc7	postižení
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
powerchair	powerchaira	k1gFnPc2	powerchaira
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sport	sport	k1gInSc1	sport
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
na	na	k7c6	na
elektrickém	elektrický	k2eAgInSc6d1	elektrický
vozíku	vozík	k1gInSc6	vozík
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
s	s	k7c7	s
florbalovým	florbalův	k2eAgInSc7d1	florbalův
míčkem	míček	k1gInSc7	míček
<g/>
,	,	kIx,	,
hokejkami	hokejka	k1gFnPc7	hokejka
a	a	k8xC	a
hokejkami	hokejka	k1gFnPc7	hokejka
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
písmene	písmeno	k1gNnSc2	písmeno
T	T	kA	T
připevněnými	připevněný	k2eAgFnPc7d1	připevněná
k	k	k7c3	k
vozíkům	vozík	k1gInPc3	vozík
<g/>
,	,	kIx,	,
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
týmu	tým	k1gInSc6	tým
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
3	[number]	k4	3
hráči	hráč	k1gMnPc7	hráč
s	s	k7c7	s
obyčejnou	obyčejný	k2eAgFnSc7d1	obyčejná
florbalovou	florbalův	k2eAgFnSc7d1	florbalův
holí	hole	k1gFnSc7	hole
a	a	k8xC	a
2	[number]	k4	2
hráči	hráč	k1gMnPc1	hráč
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
T	T	kA	T
hokejkou	hokejka	k1gFnSc7	hokejka
tzv.	tzv.	kA	tzv.
téčkem	téčko	k1gNnSc7	téčko
hokejbal	hokejbal	k1gInSc1	hokejbal
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
s	s	k7c7	s
míčkem	míček	k1gInSc7	míček
na	na	k7c6	na
betonovém	betonový	k2eAgNnSc6d1	betonové
či	či	k8xC	či
asfaltovém	asfaltový	k2eAgNnSc6d1	asfaltové
hřišti	hřiště	k1gNnSc6	hřiště
bandy	banda	k1gFnSc2	banda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hokej	hokej	k1gInSc1	hokej
hraný	hraný	k2eAgInSc1d1	hraný
na	na	k7c6	na
ledě	led	k1gInSc6	led
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
míčkem	míček	k1gInSc7	míček
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
ruskou	ruský	k2eAgFnSc4d1	ruská
národní	národní	k2eAgFnSc4d1	národní
hru	hra	k1gFnSc4	hra
-	-	kIx~	-
hokej	hokej	k1gInSc4	hokej
s	s	k7c7	s
míčkem	míček	k1gInSc7	míček
na	na	k7c6	na
ledě	led	k1gInSc6	led
inline	inlinout	k5eAaPmIp3nS	inlinout
hokej	hokej	k1gInSc1	hokej
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
kolečkových	kolečkový	k2eAgInPc6d1	kolečkový
in-line	inin	k1gInSc5	in-lin
bruslích	brusle	k1gFnPc6	brusle
rink	rink	k6eAd1	rink
hokej	hokej	k1gInSc1	hokej
skater	skatra	k1gFnPc2	skatra
hokej	hokej	k1gInSc1	hokej
podvodní	podvodní	k2eAgInSc4d1	podvodní
hokej	hokej	k1gInSc4	hokej
Velice	velice	k6eAd1	velice
podobný	podobný	k2eAgInSc4d1	podobný
hokeji	hokej	k1gInPc7	hokej
je	být	k5eAaImIp3nS	být
také	také	k9	také
florbal	florbal	k1gInSc1	florbal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
s	s	k7c7	s
plastovými	plastový	k2eAgFnPc7d1	plastová
hokejkami	hokejka	k1gFnPc7	hokejka
a	a	k8xC	a
lehkým	lehký	k2eAgInSc7d1	lehký
děravým	děravý	k2eAgInSc7d1	děravý
míčkem	míček	k1gInSc7	míček
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tzv.	tzv.	kA	tzv.
podvodní	podvodní	k2eAgInSc4d1	podvodní
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hokeje	hokej	k1gInSc2	hokej
se	se	k3xPyFc4	se
také	také	k6eAd1	také
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
některé	některý	k3yIgFnPc4	některý
stolní	stolní	k2eAgFnPc4d1	stolní
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
např.	např.	kA	např.
stolní	stolní	k2eAgInSc1d1	stolní
hokej	hokej	k1gInSc1	hokej
nebo	nebo	k8xC	nebo
šprtec	šprtec	k1gInSc1	šprtec
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hokej	hokej	k1gInSc4	hokej
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
www.pozemnihokej.cz	www.pozemnihokej.cza	k1gFnPc2	www.pozemnihokej.cza
Hokej	hokej	k1gInSc1	hokej
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
Hokejportal	Hokejportal	k1gFnSc2	Hokejportal
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Kompletní	kompletní	k2eAgNnSc1d1	kompletní
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
hokeje	hokej	k1gInSc2	hokej
</s>
