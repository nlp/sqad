<s>
Hokej	hokej	k1gInSc1	hokej
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
dva	dva	k4xCgInPc4	dva
týmy	tým	k1gInPc4	tým
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
puk	puk	k1gInSc1	puk
nebo	nebo	k8xC	nebo
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
míček	míček	k1gInSc1	míček
do	do	k7c2	do
soupeřovy	soupeřův	k2eAgFnSc2d1	soupeřova
branky	branka	k1gFnSc2	branka
pomocí	pomocí	k7c2	pomocí
hokejek	hokejka	k1gFnPc2	hokejka
<g/>
.	.	kIx.	.
</s>
