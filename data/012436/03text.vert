<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
epidemie	epidemie	k1gFnSc1	epidemie
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postihla	postihnout	k5eAaPmAgFnS	postihnout
Eurasii	Eurasie	k1gFnSc4	Eurasie
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
smrtelnou	smrtelný	k2eAgFnSc4d1	smrtelná
nemoc	nemoc	k1gFnSc4	nemoc
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
bakterií	bakterie	k1gFnSc7	bakterie
Yersinia	Yersinium	k1gNnSc2	Yersinium
pestis	pestis	k1gFnSc2	pestis
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc7	jaký
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
propukaly	propukat	k5eAaImAgFnP	propukat
epidemie	epidemie	k1gFnPc1	epidemie
jak	jak	k8xS	jak
u	u	k7c2	u
místních	místní	k2eAgMnPc2d1	místní
kočovníků	kočovník	k1gMnPc2	kočovník
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgFnPc2d1	další
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
jejího	její	k3xOp3gNnSc2	její
rozšíření	rozšíření	k1gNnSc2	rozšíření
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
výboji	výboj	k1gInPc7	výboj
Mongolů	mongol	k1gInPc2	mongol
a	a	k8xC	a
pohyby	pohyb	k1gInPc4	pohyb
jejich	jejich	k3xOp3gNnPc2	jejich
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
šíření	šíření	k1gNnSc1	šíření
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
i	i	k8xC	i
Evropě	Evropa	k1gFnSc6	Evropa
ale	ale	k8xC	ale
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
z	z	k7c2	z
několika	několik	k4yIc2	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouhé	Dlouhá	k1gFnPc1	Dlouhá
(	(	kIx(	(
<g/>
1205	[number]	k4	1205
<g/>
-	-	kIx~	-
<g/>
1353	[number]	k4	1353
<g/>
)	)	kIx)	)
období	období	k1gNnSc6	období
podmaňování	podmaňování	k1gNnSc2	podmaňování
Číny	Čína	k1gFnSc2	Čína
Mongoly	Mongol	k1gMnPc4	Mongol
přivedlo	přivést	k5eAaPmAgNnS	přivést
dříve	dříve	k6eAd2	dříve
silné	silný	k2eAgNnSc1d1	silné
čínské	čínský	k2eAgNnSc1d1	čínské
zemědělství	zemědělství	k1gNnSc1	zemědělství
na	na	k7c4	na
mizinu	mizina	k1gFnSc4	mizina
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgInSc3	ten
zkrachovaly	zkrachovat	k5eAaPmAgFnP	zkrachovat
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
i	i	k9	i
obchodní	obchodní	k2eAgFnPc1d1	obchodní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
společně	společně	k6eAd1	společně
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
opakovaným	opakovaný	k2eAgNnPc3d1	opakované
obdobím	období	k1gNnPc3	období
hladu	hlad	k1gInSc3	hlad
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
účinky	účinek	k1gInPc1	účinek
navíc	navíc	k6eAd1	navíc
velmi	velmi	k6eAd1	velmi
zesílilo	zesílit	k5eAaPmAgNnS	zesílit
zhoršení	zhoršení	k1gNnSc3	zhoršení
podnebí	podnebí	k1gNnSc2	podnebí
na	na	k7c6	na
konci	konec	k1gInSc6	konec
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
doba	doba	k1gFnSc1	doba
ledová	ledový	k2eAgFnSc1d1	ledová
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Evropa	Evropa	k1gFnSc1	Evropa
procházela	procházet	k5eAaImAgFnS	procházet
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obdobím	období	k1gNnSc7	období
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Vlámsko	Vlámsko	k1gNnSc1	Vlámsko
<g/>
)	)	kIx)	)
končícím	končící	k2eAgNnSc6d1	končící
až	až	k6eAd1	až
hladomorem	hladomor	k1gMnSc7	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
a	a	k8xC	a
nemoci	nemoc	k1gFnPc1	nemoc
oslabily	oslabit	k5eAaPmAgFnP	oslabit
zdraví	zdraví	k1gNnSc4	zdraví
značného	značný	k2eAgInSc2d1	značný
počtu	počet	k1gInSc2	počet
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
usnadnily	usnadnit	k5eAaPmAgFnP	usnadnit
šíření	šíření	k1gNnSc4	šíření
nakažlivých	nakažlivý	k2eAgFnPc2d1	nakažlivá
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
moru	mor	k1gInSc2	mor
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
1330	[number]	k4	1330
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
provincii	provincie	k1gFnSc4	provincie
Chu-pej	Chuej	k1gFnSc2	Chu-pej
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
jižních	jižní	k2eAgFnPc2d1	jižní
i	i	k8xC	i
severních	severní	k2eAgFnPc2d1	severní
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vojenskými	vojenský	k2eAgInPc7d1	vojenský
přesuny	přesun	k1gInPc7	přesun
a	a	k8xC	a
cestami	cesta	k1gFnPc7	cesta
kupců	kupec	k1gMnPc2	kupec
po	po	k7c6	po
karavanních	karavanní	k2eAgFnPc6d1	karavanní
stezkách	stezka	k1gFnPc6	stezka
šířil	šířit	k5eAaImAgInS	šířit
mor	mor	k1gInSc1	mor
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
západních	západní	k2eAgFnPc2d1	západní
asijských	asijský	k2eAgFnPc2d1	asijská
stepí	step	k1gFnPc2	step
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
jednak	jednak	k8xC	jednak
do	do	k7c2	do
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šíření	šíření	k1gNnSc4	šíření
moru	mor	k1gInSc2	mor
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1347	[number]	k4	1347
byl	být	k5eAaImAgInS	být
mor	mor	k1gInSc4	mor
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgInSc4d1	zvaný
též	též	k9	též
černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
v	v	k7c6	v
Cařihradě	Cařihrad	k1gInSc6	Cařihrad
a	a	k8xC	a
Trapezuntu	Trapezunt	k1gInSc6	Trapezunt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
čilý	čilý	k2eAgInSc4d1	čilý
obchodní	obchodní	k2eAgInSc4d1	obchodní
styk	styk	k1gInSc4	styk
s	s	k7c7	s
asijským	asijský	k2eAgInSc7d1	asijský
východem	východ	k1gInSc7	východ
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
mor	mor	k1gInSc1	mor
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
také	také	k9	také
vojsko	vojsko	k1gNnSc4	vojsko
krymských	krymský	k2eAgInPc2d1	krymský
Tatarů	tatar	k1gInPc2	tatar
podporovaných	podporovaný	k2eAgInPc2d1	podporovaný
Benátčany	Benátčan	k1gMnPc7	Benátčan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
obléhali	obléhat	k5eAaImAgMnP	obléhat
janovský	janovský	k2eAgInSc4d1	janovský
přístav	přístav	k1gInSc4	přístav
Kaffa	Kaff	k1gMnSc4	Kaff
na	na	k7c6	na
krymském	krymský	k2eAgNnSc6d1	krymské
pobřeží	pobřeží	k1gNnSc6	pobřeží
(	(	kIx(	(
<g/>
obléhající	obléhající	k2eAgMnPc1d1	obléhající
dobyli	dobýt	k5eAaPmAgMnP	dobýt
město	město	k1gNnSc4	město
i	i	k8xC	i
házením	házení	k1gNnSc7	házení
nakažených	nakažený	k2eAgFnPc2d1	nakažená
mrtvol	mrtvola	k1gFnPc2	mrtvola
svých	svůj	k3xOyFgMnPc2	svůj
dřívějších	dřívější	k2eAgMnPc2d1	dřívější
spolubojovníků	spolubojovník	k1gMnPc2	spolubojovník
přes	přes	k7c4	přes
městské	městský	k2eAgFnPc4d1	městská
hradby	hradba	k1gFnPc4	hradba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Janovská	janovský	k2eAgFnSc1d1	janovská
obchodní	obchodní	k2eAgFnSc1d1	obchodní
flotila	flotila	k1gFnSc1	flotila
s	s	k7c7	s
nakaženými	nakažený	k2eAgMnPc7d1	nakažený
přistála	přistát	k5eAaImAgFnS	přistát
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
sicilské	sicilský	k2eAgFnSc6d1	sicilská
Messině	Messina	k1gFnSc6	Messina
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
během	během	k7c2	během
plavby	plavba	k1gFnSc2	plavba
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
nemoc	nemoc	k1gFnSc4	nemoc
spousta	spousta	k1gFnSc1	spousta
námořníků	námořník	k1gMnPc2	námořník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
lodě	loď	k1gFnPc1	loď
převážely	převážet	k5eAaImAgFnP	převážet
i	i	k9	i
nakažené	nakažený	k2eAgMnPc4d1	nakažený
hlodavce	hlodavec	k1gMnPc4	hlodavec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stali	stát	k5eAaPmAgMnP	stát
přenašeči	přenašeč	k1gMnPc1	přenašeč
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Přenašeči	přenašeč	k1gMnPc1	přenašeč
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
blechy	blecha	k1gFnPc1	blecha
a	a	k8xC	a
vši	veš	k1gFnPc1	veš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stykem	styk	k1gInSc7	styk
s	s	k7c7	s
janovskými	janovský	k2eAgMnPc7d1	janovský
námořníky	námořník	k1gMnPc7	námořník
(	(	kIx(	(
<g/>
Messiňané	Messiňan	k1gMnPc1	Messiňan
údajně	údajně	k6eAd1	údajně
některé	některý	k3yIgFnPc4	některý
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
lodi	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
posádky	posádka	k1gFnPc1	posádka
zemřely	zemřít	k5eAaPmAgFnP	zemřít
na	na	k7c4	na
mor	mor	k1gInSc4	mor
<g/>
,	,	kIx,	,
chodili	chodit	k5eAaImAgMnP	chodit
bezostyšně	bezostyšně	k6eAd1	bezostyšně
rabovat	rabovat	k5eAaImF	rabovat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mor	mor	k1gInSc1	mor
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
mor	mor	k1gInSc1	mor
dostal	dostat	k5eAaPmAgInS	dostat
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Janovu	Janov	k1gInSc3	Janov
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
západně	západně	k6eAd1	západně
od	od	k7c2	od
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
na	na	k7c4	na
ligurské	ligurský	k2eAgNnSc4d1	Ligurské
a	a	k8xC	a
provensálské	provensálský	k2eAgNnSc4d1	provensálské
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Janově	Janov	k1gInSc6	Janov
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
se	se	k3xPyFc4	se
mor	mor	k1gInSc1	mor
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1347	[number]	k4	1347
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
centry	centr	k1gInPc4	centr
námořního	námořní	k2eAgInSc2d1	námořní
obchodu	obchod	k1gInSc2	obchod
přijato	přijat	k2eAgNnSc4d1	přijato
opatření	opatření	k1gNnSc4	opatření
<g/>
,	,	kIx,	,
že	že	k8xS	že
lodě	loď	k1gFnPc1	loď
s	s	k7c7	s
nákladem	náklad	k1gInSc7	náklad
musely	muset	k5eAaImAgFnP	muset
40	[number]	k4	40
<g/>
dní	den	k1gInPc2	den
čekat	čekat	k5eAaImF	čekat
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
než	než	k8xS	než
směli	smět	k5eAaImAgMnP	smět
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
hojně	hojně	k6eAd1	hojně
porušováno	porušovat	k5eAaImNgNnS	porušovat
pašeráky	pašerák	k1gMnPc7	pašerák
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
výrazu	výraz	k1gInSc2	výraz
quarante	quarant	k1gMnSc5	quarant
(	(	kIx(	(
<g/>
čtyřicet	čtyřicet	k4xCc1	čtyřicet
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
slovo	slovo	k1gNnSc1	slovo
karanténa	karanténa	k1gFnSc1	karanténa
<g/>
.	.	kIx.	.
</s>
<s>
Itálie	Itálie	k1gFnSc1	Itálie
se	se	k3xPyFc4	se
šířil	šířit	k5eAaImAgMnS	šířit
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1348	[number]	k4	1348
a	a	k8xC	a
1349	[number]	k4	1349
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
na	na	k7c4	na
sever	sever	k1gInSc4	sever
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
na	na	k7c4	na
Britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
do	do	k7c2	do
jižního	jižní	k2eAgNnSc2d1	jižní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
středního	střední	k2eAgNnSc2d1	střední
Podunají	Podunají	k1gNnSc2	Podunají
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
se	se	k3xPyFc4	se
souběžně	souběžně	k6eAd1	souběžně
šířila	šířit	k5eAaImAgFnS	šířit
nákaza	nákaza	k1gFnSc1	nákaza
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
již	již	k9	již
byla	být	k5eAaImAgFnS	být
morem	mor	k1gInSc7	mor
nakažena	nakažen	k2eAgFnSc1d1	nakažena
polovina	polovina	k1gFnSc1	polovina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1352	[number]	k4	1352
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostala	dostat	k5eAaPmAgFnS	dostat
nákaza	nákaza	k1gFnSc1	nákaza
i	i	k9	i
do	do	k7c2	do
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
východních	východní	k2eAgFnPc2d1	východní
částí	část	k1gFnPc2	část
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
nemnohé	mnohý	k2eNgFnPc1d1	nemnohá
oblasti	oblast	k1gFnPc1	oblast
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
moru	mora	k1gFnSc4	mora
ušetřeny	ušetřen	k2eAgFnPc4d1	ušetřena
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
řídkému	řídký	k2eAgNnSc3d1	řídké
osídlení	osídlení	k1gNnSc3	osídlení
nebo	nebo	k8xC	nebo
izolaci	izolace	k1gFnSc3	izolace
od	od	k7c2	od
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
první	první	k4xOgFnSc1	první
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
jen	jen	k9	jen
okrajově	okrajově	k6eAd1	okrajově
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
soudobého	soudobý	k2eAgMnSc2d1	soudobý
kronikáře	kronikář	k1gMnSc2	kronikář
Františka	František	k1gMnSc2	František
Pražského	pražský	k2eAgInSc2d1	pražský
první	první	k4xOgFnSc4	první
vlnu	vlna	k1gFnSc4	vlna
moru	mor	k1gInSc2	mor
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
"	"	kIx"	"
<g/>
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
a	a	k8xC	a
chladné	chladný	k2eAgNnSc1d1	chladné
povětří	povětří	k1gNnSc1	povětří
<g/>
''	''	k?	''
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
vlně	vlna	k1gFnSc6	vlna
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
klid	klid	k1gInSc1	klid
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
suchým	suchý	k2eAgInPc3d1	suchý
rokům	rok	k1gInPc3	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1356	[number]	k4	1356
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
z	z	k7c2	z
nevelkého	velký	k2eNgNnSc2d1	nevelké
ohniska	ohnisko	k1gNnSc2	ohnisko
v	v	k7c6	v
Hessensku	Hessensko	k1gNnSc6	Hessensko
šířit	šířit	k5eAaImF	šířit
nová	nový	k2eAgFnSc1d1	nová
pandemie	pandemie	k1gFnSc1	pandemie
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kulminace	kulminace	k1gFnSc1	kulminace
v	v	k7c6	v
letech	let	k1gInPc6	let
1357	[number]	k4	1357
<g/>
–	–	k?	–
<g/>
1360	[number]	k4	1360
a	a	k8xC	a
1363	[number]	k4	1363
<g/>
–	–	k?	–
<g/>
1366	[number]	k4	1366
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
známý	známý	k2eAgInSc4d1	známý
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
druhá	druhý	k4xOgFnSc1	druhý
vlna	vlna	k1gFnSc1	vlna
moru	mor	k1gInSc2	mor
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
Čechám	Čechy	k1gFnPc3	Čechy
nevyhnula	vyhnout	k5eNaPmAgFnS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
ji	on	k3xPp3gFnSc4	on
máme	mít	k5eAaImIp1nP	mít
bezpečně	bezpečně	k6eAd1	bezpečně
doloženou	doložená	k1gFnSc4	doložená
v	v	k7c6	v
letech	let	k1gInPc6	let
1357	[number]	k4	1357
<g/>
–	–	k?	–
<g/>
1363	[number]	k4	1363
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
morová	morový	k2eAgFnSc1d1	morová
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
rozhodně	rozhodně	k6eAd1	rozhodně
vlnou	vlna	k1gFnSc7	vlna
poslední	poslední	k2eAgFnSc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vracela	vracet	k5eAaImAgFnS	vracet
ve	v	k7c6	v
zhruba	zhruba	k6eAd1	zhruba
dvacetiletých	dvacetiletý	k2eAgInPc6d1	dvacetiletý
intervalech	interval	k1gInPc6	interval
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
už	už	k6eAd1	už
ale	ale	k8xC	ale
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
útoky	útok	k1gInPc7	útok
moru	mor	k1gInSc2	mor
na	na	k7c4	na
Evropu	Evropa	k1gFnSc4	Evropa
zpomalily	zpomalit	k5eAaPmAgFnP	zpomalit
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
větší	veliký	k2eAgFnPc4d2	veliký
epidemie	epidemie	k1gFnPc4	epidemie
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Velký	velký	k2eAgInSc1d1	velký
londýnský	londýnský	k2eAgInSc1d1	londýnský
mor	mor	k1gInSc1	mor
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1665	[number]	k4	1665
až	až	k9	až
1666	[number]	k4	1666
<g/>
,	,	kIx,	,
mor	mor	k1gInSc1	mor
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
1629-1631	[number]	k4	1629-1631
nebo	nebo	k8xC	nebo
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1679	[number]	k4	1679
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1720	[number]	k4	1720
až	až	k8xS	až
1721	[number]	k4	1721
se	se	k3xPyFc4	se
mor	mor	k1gInSc1	mor
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
objevil	objevit	k5eAaPmAgMnS	objevit
naposledy	naposledy	k6eAd1	naposledy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důsledky	důsledek	k1gInPc1	důsledek
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
celoevropská	celoevropský	k2eAgFnSc1d1	celoevropská
epidemie	epidemie	k1gFnSc1	epidemie
moru	mor	k1gInSc2	mor
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
25	[number]	k4	25
miliónů	milión	k4xCgInPc2	milión
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
obyvatel	obyvatel	k1gMnPc2	obyvatel
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
infekce	infekce	k1gFnSc2	infekce
existovalo	existovat	k5eAaImAgNnS	existovat
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lidé	člověk	k1gMnPc1	člověk
přicházeli	přicházet	k5eAaImAgMnP	přicházet
vzájemně	vzájemně	k6eAd1	vzájemně
do	do	k7c2	do
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ulehčovalo	ulehčovat	k5eAaImAgNnS	ulehčovat
situaci	situace	k1gFnSc4	situace
venkovu	venkov	k1gInSc3	venkov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgNnP	být
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
osídlení	osídlení	k1gNnPc1	osídlení
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zranitelná	zranitelný	k2eAgFnSc1d1	zranitelná
byla	být	k5eAaImAgFnS	být
naopak	naopak	k6eAd1	naopak
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
soustřeďovala	soustřeďovat	k5eAaImAgNnP	soustřeďovat
velká	velký	k2eAgNnPc1d1	velké
množství	množství	k1gNnPc1	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
neustále	neustále	k6eAd1	neustále
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
nesoběstačnosti	nesoběstačnost	k1gFnSc3	nesoběstačnost
stále	stále	k6eAd1	stále
přicházeli	přicházet	k5eAaImAgMnP	přicházet
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vládci	vládce	k1gMnPc1	vládce
evropských	evropský	k2eAgInPc2d1	evropský
států	stát	k1gInPc2	stát
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
učinit	učinit	k5eAaPmF	učinit
žádné	žádný	k3yNgNnSc4	žádný
preventivní	preventivní	k2eAgNnSc4d1	preventivní
opatření	opatření	k1gNnSc4	opatření
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nikdo	nikdo	k3yNnSc1	nikdo
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
mor	mor	k1gInSc1	mor
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
a	a	k8xC	a
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
vyděšeni	vyděsit	k5eAaPmNgMnP	vyděsit
a	a	k8xC	a
pokládali	pokládat	k5eAaImAgMnP	pokládat
mor	mor	k1gInSc4	mor
za	za	k7c4	za
předzvěst	předzvěst	k1gFnSc4	předzvěst
apokalypsy	apokalypsa	k1gFnSc2	apokalypsa
a	a	k8xC	a
Boží	boží	k2eAgInSc1d1	boží
trest	trest	k1gInSc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgInPc1d1	hospodářský
následky	následek	k1gInPc1	následek
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
alespoň	alespoň	k9	alespoň
zmírnit	zmírnit	k5eAaPmF	zmírnit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nijak	nijak	k6eAd1	nijak
efektivně	efektivně	k6eAd1	efektivně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
lidí	člověk	k1gMnPc2	člověk
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
následně	následně	k6eAd1	následně
poklesu	pokles	k1gInSc2	pokles
zásobování	zásobování	k1gNnPc2	zásobování
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
objevovaly	objevovat	k5eAaImAgInP	objevovat
zákazy	zákaz	k1gInPc1	zákaz
vývozu	vývoz	k1gInSc2	vývoz
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
nařízení	nařízení	k1gNnSc1	nařízení
potírající	potírající	k2eAgInPc1d1	potírající
podvody	podvod	k1gInPc1	podvod
prodejců	prodejce	k1gMnPc2	prodejce
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
trh	trh	k1gInSc1	trh
a	a	k8xC	a
regulace	regulace	k1gFnSc1	regulace
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Úbytek	úbytek	k1gInSc1	úbytek
rolníků	rolník	k1gMnPc2	rolník
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
přineslo	přinést	k5eAaPmAgNnS	přinést
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
užitek	užitek	k1gInSc1	užitek
i	i	k8xC	i
jisté	jistý	k2eAgNnSc1d1	jisté
zlepšení	zlepšení	k1gNnSc1	zlepšení
společenského	společenský	k2eAgNnSc2d1	společenské
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
nutnosti	nutnost	k1gFnSc2	nutnost
třeba	třeba	k6eAd1	třeba
zavádět	zavádět	k5eAaImF	zavádět
inovace	inovace	k1gFnPc4	inovace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
stávaly	stávat	k5eAaImAgFnP	stávat
zárodky	zárodek	k1gInPc4	zárodek
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
vypadala	vypadat	k5eAaImAgFnS	vypadat
situace	situace	k1gFnSc1	situace
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
řidšímu	řídký	k2eAgNnSc3d2	řidší
osídlení	osídlení	k1gNnSc3	osídlení
nebyl	být	k5eNaImAgInS	být
úbytek	úbytek	k1gInSc1	úbytek
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
mor	mor	k1gInSc1	mor
tedy	tedy	k9	tedy
okamžitě	okamžitě	k6eAd1	okamžitě
nevedl	vést	k5eNaImAgMnS	vést
k	k	k7c3	k
velkým	velký	k2eAgFnPc3d1	velká
společenským	společenský	k2eAgFnPc3d1	společenská
změnám	změna	k1gFnPc3	změna
a	a	k8xC	a
rozporům	rozpor	k1gInPc3	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
ovšem	ovšem	k9	ovšem
morová	morový	k2eAgFnSc1d1	morová
epidemie	epidemie	k1gFnSc1	epidemie
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Evropu	Evropa	k1gFnSc4	Evropa
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
stagnace	stagnace	k1gFnSc2	stagnace
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
ji	on	k3xPp3gFnSc4	on
ještě	ještě	k6eAd1	ještě
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
urychlila	urychlit	k5eAaPmAgFnS	urychlit
společenské	společenský	k2eAgFnPc4d1	společenská
změny	změna	k1gFnPc4	změna
ve	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Morová	morový	k2eAgFnSc1d1	morová
rána	rána	k1gFnSc1	rána
zanechala	zanechat	k5eAaPmAgFnS	zanechat
stopy	stopa	k1gFnPc4	stopa
také	také	k6eAd1	také
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gNnPc6	jejich
srdcích	srdce	k1gNnPc6	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
umírání	umírání	k1gNnSc1	umírání
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
pesimistických	pesimistický	k2eAgFnPc2d1	pesimistická
nálad	nálada	k1gFnPc2	nálada
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
více	hodně	k6eAd2	hodně
zabývat	zabývat	k5eAaImF	zabývat
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
podlomena	podlomen	k2eAgFnSc1d1	podlomena
víra	víra	k1gFnSc1	víra
ve	v	k7c6	v
schopnosti	schopnost	k1gFnSc6	schopnost
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
mor	mor	k1gInSc4	mor
léčit	léčit	k5eAaImF	léčit
ani	ani	k8xC	ani
zabránit	zabránit	k5eAaPmF	zabránit
jeho	jeho	k3xOp3gNnSc4	jeho
šíření	šíření	k1gNnSc4	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
lék	lék	k1gInSc1	lék
na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
smrt	smrt	k1gFnSc4	smrt
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
líh	líh	k1gInSc1	líh
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
spotřeby	spotřeba	k1gFnSc2	spotřeba
destilovaného	destilovaný	k2eAgInSc2d1	destilovaný
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
stoletích	století	k1gNnPc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
kněží	kněz	k1gMnPc1	kněz
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
odpovědět	odpovědět	k5eAaPmF	odpovědět
na	na	k7c4	na
mor	mor	k1gInSc4	mor
podle	podle	k7c2	podle
požadavků	požadavek	k1gInPc2	požadavek
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
částečnému	částečný	k2eAgInSc3d1	částečný
odklonu	odklon	k1gInSc3	odklon
i	i	k8xC	i
od	od	k7c2	od
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
měla	mít	k5eAaImAgFnS	mít
pošramocenou	pošramocený	k2eAgFnSc4d1	pošramocená
pověst	pověst	k1gFnSc4	pověst
již	již	k6eAd1	již
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
papež	papež	k1gMnSc1	papež
sídlil	sídlit	k5eAaImAgMnS	sídlit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Avignonu	Avignon	k1gInSc6	Avignon
a	a	k8xC	a
podle	podle	k7c2	podle
mínění	mínění	k1gNnSc2	mínění
mnohých	mnohý	k2eAgMnPc2d1	mnohý
byl	být	k5eAaImAgInS	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
loutkou	loutka	k1gFnSc7	loutka
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odcizení	odcizení	k1gNnSc4	odcizení
k	k	k7c3	k
církvi	církev	k1gFnSc3	církev
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
se	s	k7c7	s
zvýšenou	zvýšený	k2eAgFnSc7d1	zvýšená
morbiditou	morbidita	k1gFnSc7	morbidita
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
rozšíření	rozšíření	k1gNnSc3	rozšíření
flagelantství	flagelantství	k1gNnSc2	flagelantství
<g/>
,	,	kIx,	,
snaze	snaha	k1gFnSc6	snaha
usmířit	usmířit	k5eAaPmF	usmířit
rozhněvaného	rozhněvaný	k2eAgMnSc4d1	rozhněvaný
Boha	bůh	k1gMnSc4	bůh
pomocí	pomocí	k7c2	pomocí
sebemrskačství	sebemrskačství	k1gNnSc2	sebemrskačství
<g/>
,	,	kIx,	,
trestáním	trestání	k1gNnPc3	trestání
sebe	sebe	k3xPyFc4	sebe
samých	samý	k3xTgNnPc2	samý
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
důvodem	důvod	k1gInSc7	důvod
morové	morový	k2eAgFnSc2d1	morová
epidemie	epidemie	k1gFnSc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
kláštery	klášter	k1gInPc1	klášter
jakožto	jakožto	k8xS	jakožto
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
míst	místo	k1gNnPc2	místo
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
léčbu	léčba	k1gFnSc4	léčba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
stávaly	stávat	k5eAaImAgFnP	stávat
oběťmi	oběť	k1gFnPc7	oběť
moru	mor	k1gInSc2	mor
<g/>
.	.	kIx.	.
</s>
<s>
Zemřelé	zemřelý	k2eAgInPc4d1	zemřelý
mnichy	mnich	k1gInPc4	mnich
a	a	k8xC	a
řeholnice	řeholnice	k1gFnPc4	řeholnice
museli	muset	k5eAaImAgMnP	muset
vzápětí	vzápětí	k6eAd1	vzápětí
nahradit	nahradit	k5eAaPmF	nahradit
nově	nově	k6eAd1	nově
příchozí	příchozí	k1gMnPc4	příchozí
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
však	však	k9	však
mnohdy	mnohdy	k6eAd1	mnohdy
nedosahovali	dosahovat	k5eNaImAgMnP	dosahovat
potřebných	potřebný	k2eAgFnPc2d1	potřebná
morálních	morální	k2eAgFnPc2d1	morální
kvalit	kvalita	k1gFnPc2	kvalita
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
obdobích	období	k1gNnPc6	období
zvětšovali	zvětšovat	k5eAaImAgMnP	zvětšovat
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
lidu	lid	k1gInSc2	lid
vůči	vůči	k7c3	vůči
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
v	v	k7c6	v
období	období	k1gNnSc6	období
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
,	,	kIx,	,
mor	mor	k1gInSc1	mor
roznítil	roznítit	k5eAaPmAgInS	roznítit
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
horlivost	horlivost	k1gFnSc4	horlivost
a	a	k8xC	a
také	také	k9	také
náboženský	náboženský	k2eAgInSc1d1	náboženský
fanatismus	fanatismus	k1gInSc1	fanatismus
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
hledali	hledat	k5eAaImAgMnP	hledat
původ	původ	k1gInSc4	původ
moru	mor	k1gInSc2	mor
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
ho	on	k3xPp3gMnSc4	on
nalezli	nalézt	k5eAaBmAgMnP	nalézt
v	v	k7c6	v
menšinách	menšina	k1gFnPc6	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Týkalo	týkat	k5eAaImAgNnS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
malomocných	malomocný	k2eAgMnPc2d1	malomocný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
opatřováni	opatřovat	k5eAaImNgMnP	opatřovat
viditelnými	viditelný	k2eAgNnPc7d1	viditelné
znameními	znamení	k1gNnPc7	znamení
<g/>
,	,	kIx,	,
odsouváni	odsouvat	k5eAaImNgMnP	odsouvat
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
dokonce	dokonce	k9	dokonce
vybíjeni	vybíjen	k2eAgMnPc1d1	vybíjen
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
malomocného	malomocný	k1gMnSc2	malomocný
byl	být	k5eAaImAgMnS	být
označen	označit	k5eAaPmNgMnS	označit
i	i	k9	i
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
měl	mít	k5eAaImAgMnS	mít
trudovitost	trudovitost	k1gFnSc4	trudovitost
nebo	nebo	k8xC	nebo
lupénku	lupénka	k1gFnSc4	lupénka
<g/>
.	.	kIx.	.
</s>
<s>
Jakékoli	jakýkoli	k3yIgNnSc1	jakýkoli
poškození	poškození	k1gNnSc1	poškození
kůže	kůže	k1gFnSc2	kůže
bylo	být	k5eAaImAgNnS	být
bráno	brát	k5eAaImNgNnS	brát
jako	jako	k8xS	jako
vnější	vnější	k2eAgInSc1d1	vnější
příznak	příznak	k1gInSc1	příznak
nečisté	čistý	k2eNgFnSc2d1	nečistá
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
viníkem	viník	k1gMnSc7	viník
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
morem	mor	k1gInSc7	mor
sužovaných	sužovaný	k2eAgMnPc2d1	sužovaný
lidí	člověk	k1gMnPc2	člověk
stali	stát	k5eAaPmAgMnP	stát
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
se	se	k3xPyFc4	se
stávali	stávat	k5eAaImAgMnP	stávat
snadným	snadný	k2eAgInSc7d1	snadný
terčem	terč	k1gInSc7	terč
nenávisti	nenávist	k1gFnSc2	nenávist
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyznávali	vyznávat	k5eAaImAgMnP	vyznávat
jiné	jiný	k2eAgNnSc4d1	jiné
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
zvyky	zvyk	k1gInPc4	zvyk
málokdo	málokdo	k3yInSc1	málokdo
znal	znát	k5eAaImAgMnS	znát
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
živili	živit	k5eAaImAgMnP	živit
jinou	jiný	k2eAgFnSc7d1	jiná
činností	činnost	k1gFnSc7	činnost
než	než	k8xS	než
jejich	jejich	k3xOp3gNnSc4	jejich
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
okolí	okolí	k1gNnSc4	okolí
(	(	kIx(	(
<g/>
z	z	k7c2	z
nábožensko-společenských	náboženskopolečenský	k2eAgInPc2d1	nábožensko-společenský
důvodů	důvod	k1gInPc2	důvod
byli	být	k5eAaImAgMnP	být
vytlačováni	vytlačovat	k5eAaImNgMnP	vytlačovat
z	z	k7c2	z
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
oblasti	oblast	k1gFnSc2	oblast
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nemohli	moct	k5eNaImAgMnP	moct
konkurovat	konkurovat	k5eAaImF	konkurovat
svým	svůj	k3xOyFgMnPc3	svůj
křesťanským	křesťanský	k2eAgMnPc3d1	křesťanský
spoluobčanům	spoluobčan	k1gMnPc3	spoluobčan
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
trpěna	trpěn	k2eAgFnSc1d1	trpěna
jim	on	k3xPp3gMnPc3	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
oblast	oblast	k1gFnSc1	oblast
půjčování	půjčování	k1gNnSc2	půjčování
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
provádění	provádění	k1gNnSc4	provádění
různých	různý	k2eAgInPc2d1	různý
finančních	finanční	k2eAgInPc2d1	finanční
převodů	převod	k1gInPc2	převod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
církví	církev	k1gFnPc2	církev
i	i	k8xC	i
šlechtou	šlechta	k1gFnSc7	šlechta
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c7	za
–	–	k?	–
pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
–	–	k?	–
nedůstojnou	důstojný	k2eNgFnSc4d1	nedůstojná
<g/>
,	,	kIx,	,
neetickou	etický	k2eNgFnSc4d1	neetická
činnost	činnost	k1gFnSc4	činnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
doléhala	doléhat	k5eAaImAgFnS	doléhat
různá	různý	k2eAgNnPc4d1	různé
omezení	omezení	k1gNnPc4	omezení
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nimž	jenž	k3xRgMnPc3	jenž
museli	muset	k5eAaImAgMnP	muset
například	například	k6eAd1	například
žít	žít	k5eAaImF	žít
v	v	k7c6	v
oddělených	oddělený	k2eAgFnPc6d1	oddělená
a	a	k8xC	a
uzavřených	uzavřený	k2eAgFnPc6d1	uzavřená
prostorách	prostora	k1gFnPc6	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc1	podezření
budilo	budit	k5eAaImAgNnS	budit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
moru	mor	k1gInSc2	mor
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
méně	málo	k6eAd2	málo
Židů	Žid	k1gMnPc2	Žid
než	než	k8xS	než
křesťanů	křesťan	k1gMnPc2	křesťan
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
dáno	dát	k5eAaPmNgNnS	dát
jednak	jednak	k8xC	jednak
lepšími	dobrý	k2eAgFnPc7d2	lepší
<g/>
,	,	kIx,	,
nábožensky	nábožensky	k6eAd1	nábožensky
motivovanými	motivovaný	k2eAgInPc7d1	motivovaný
hygienickými	hygienický	k2eAgInPc7d1	hygienický
návyky	návyk	k1gInPc7	návyk
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
izolací	izolace	k1gFnSc7	izolace
židovských	židovský	k2eAgNnPc2d1	Židovské
ghett	ghetto	k1gNnPc2	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
uvěřili	uvěřit	k5eAaPmAgMnP	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Židé	Žid	k1gMnPc1	Žid
způsobili	způsobit	k5eAaPmAgMnP	způsobit
roznesení	roznesení	k1gNnSc4	roznesení
moru	mor	k1gInSc2	mor
otravováním	otravování	k1gNnSc7	otravování
studen	studeno	k1gNnPc2	studeno
<g/>
,	,	kIx,	,
v	v	k7c6	v
Savojsku	Savojsko	k1gNnSc6	Savojsko
na	na	k7c6	na
mučidlech	mučidlo	k1gNnPc6	mučidlo
vynutili	vynutit	k5eAaPmAgMnP	vynutit
od	od	k7c2	od
několika	několik	k4yIc2	několik
jedinců	jedinec	k1gMnPc2	jedinec
přiznání	přiznání	k1gNnSc4	přiznání
ke	k	k7c3	k
světovému	světový	k2eAgNnSc3d1	světové
židovskému	židovský	k2eAgNnSc3d1	Židovské
spiknutí	spiknutí	k1gNnSc3	spiknutí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
židovských	židovský	k2eAgNnPc2d1	Židovské
osídlení	osídlení	k1gNnPc2	osídlení
pocítila	pocítit	k5eAaPmAgFnS	pocítit
hněv	hněv	k1gInSc4	hněv
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1351	[number]	k4	1351
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
350	[number]	k4	350
ozbrojenými	ozbrojený	k2eAgInPc7d1	ozbrojený
útoky	útok	k1gInPc7	útok
na	na	k7c4	na
Židy	Žid	k1gMnPc4	Žid
a	a	k8xC	a
210	[number]	k4	210
zcela	zcela	k6eAd1	zcela
zničenými	zničený	k2eAgFnPc7d1	zničená
židovskými	židovská	k1gFnPc7	židovská
sídly	sídlo	k1gNnPc7	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řádění	řádění	k1gNnSc1	řádění
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nikdo	nikdo	k3yNnSc1	nikdo
nesnažil	snažit	k5eNaImAgMnS	snažit
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
vládci	vládce	k1gMnPc1	vládce
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
snažili	snažit	k5eAaImAgMnP	snažit
vydělat	vydělat	k5eAaPmF	vydělat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
basilejská	basilejský	k2eAgFnSc1d1	Basilejská
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
rýnském	rýnský	k2eAgInSc6d1	rýnský
ostrově	ostrov	k1gInSc6	ostrov
dokonce	dokonce	k9	dokonce
postavit	postavit	k5eAaPmF	postavit
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
domy	dům	k1gInPc4	dům
k	k	k7c3	k
upalování	upalování	k1gNnSc3	upalování
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evropských	evropský	k2eAgMnPc2d1	evropský
panovníků	panovník	k1gMnPc2	panovník
se	se	k3xPyFc4	se
jedině	jedině	k6eAd1	jedině
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
snažil	snažit	k5eAaImAgMnS	snažit
vraždění	vraždění	k1gNnSc4	vraždění
Židů	Žid	k1gMnPc2	Žid
zabránit	zabránit	k5eAaPmF	zabránit
a	a	k8xC	a
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
nevinní	vinní	k2eNgMnPc1d1	nevinní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgInS	mít
příliš	příliš	k6eAd1	příliš
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
s	s	k7c7	s
podobnými	podobný	k2eAgNnPc7d1	podobné
prohlášeními	prohlášení	k1gNnPc7	prohlášení
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Protižidovské	protižidovský	k2eAgInPc1d1	protižidovský
pogromy	pogrom	k1gInPc1	pogrom
postupně	postupně	k6eAd1	postupně
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Židé	Žid	k1gMnPc1	Žid
hromadně	hromadně	k6eAd1	hromadně
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
největší	veliký	k2eAgFnPc4d3	veliký
evropské	evropský	k2eAgFnPc4d1	Evropská
židovské	židovská	k1gFnPc4	židovská
osídlení	osídlení	k1gNnSc2	osídlení
trvající	trvající	k2eAgFnPc4d1	trvající
až	až	k9	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
se	se	k3xPyFc4	se
tak	tak	k9	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
malomocnými	malomocný	k1gMnPc7	malomocný
stali	stát	k5eAaPmAgMnP	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
obětním	obětní	k2eAgInSc7d1	obětní
beránkem	beránek	k1gInSc7	beránek
morové	morový	k2eAgFnSc2d1	morová
pohromy	pohroma	k1gFnSc2	pohroma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dobové	dobový	k2eAgNnSc4d1	dobové
svědectví	svědectví	k1gNnSc4	svědectví
==	==	k?	==
</s>
</p>
<p>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejživějších	živý	k2eAgNnPc2d3	nejživější
svědectví	svědectví	k1gNnSc2	svědectví
následků	následek	k1gInPc2	následek
moru	mor	k1gInSc2	mor
nám	my	k3xPp1nPc3	my
zanechal	zanechat	k5eAaPmAgMnS	zanechat
Ital	Ital	k1gMnSc1	Ital
Agnolo	Agnola	k1gFnSc5	Agnola
di	di	k?	di
Tura	tur	k1gMnSc4	tur
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Sienské	sienský	k2eAgFnSc2d1	Sienská
kroniky	kronika	k1gFnSc2	kronika
zaznamenávající	zaznamenávající	k2eAgFnSc2d1	zaznamenávající
události	událost	k1gFnSc2	událost
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1300	[number]	k4	1300
<g/>
-	-	kIx~	-
<g/>
1351	[number]	k4	1351
<g/>
.	.	kIx.	.
</s>
<s>
Řádění	řádění	k1gNnSc1	řádění
moru	mor	k1gInSc2	mor
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Siena	Siena	k1gFnSc1	Siena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1348	[number]	k4	1348
popisuje	popisovat	k5eAaImIp3nS	popisovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
<g/>
Umírání	umírání	k1gNnSc1	umírání
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
Sieně	Siena	k1gFnSc6	Siena
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
ukrutné	ukrutný	k2eAgNnSc1d1	ukrutné
a	a	k8xC	a
strašné	strašný	k2eAgNnSc1d1	strašné
<g/>
;	;	kIx,	;
a	a	k8xC	a
nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mám	mít	k5eAaImIp1nS	mít
začít	začít	k5eAaPmF	začít
vyprávět	vyprávět	k5eAaImF	vyprávět
o	o	k7c6	o
ukrutnosti	ukrutnost	k1gFnSc6	ukrutnost
a	a	k8xC	a
způsobech	způsob	k1gInPc6	způsob
nemilosrdnosti	nemilosrdnost	k1gFnSc2	nemilosrdnost
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
každému	každý	k3xTgInSc3	každý
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
oněmět	oněmět	k5eAaPmF	oněmět
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
bolest	bolest	k1gFnSc4	bolest
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
lidskému	lidský	k2eAgInSc3d1	lidský
jazyku	jazyk	k1gInSc3	jazyk
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
vylíčit	vylíčit	k5eAaPmF	vylíčit
tu	ten	k3xDgFnSc4	ten
strašnou	strašný	k2eAgFnSc4d1	strašná
věc	věc	k1gFnSc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Vskutku	vskutku	k9	vskutku
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
nespatří	spatřit	k5eNaPmIp3nS	spatřit
tu	ten	k3xDgFnSc4	ten
hrůzu	hrůza	k1gFnSc4	hrůza
<g/>
,	,	kIx,	,
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
zván	zván	k2eAgInSc1d1	zván
blažený	blažený	k2eAgInSc1d1	blažený
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
oběti	oběť	k1gFnPc1	oběť
umíraly	umírat	k5eAaImAgFnP	umírat
téměř	téměř	k6eAd1	téměř
ihned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
</s>
<s>
Otékaly	otékat	k5eAaImAgInP	otékat
pod	pod	k7c7	pod
pažemi	paže	k1gFnPc7	paže
a	a	k8xC	a
ve	v	k7c6	v
slabinách	slabina	k1gFnPc6	slabina
a	a	k8xC	a
padaly	padat	k5eAaImAgFnP	padat
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
mrtvy	mrtev	k2eAgFnPc4d1	mrtva
během	během	k7c2	během
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
opouštěl	opouštět	k5eAaImAgMnS	opouštět
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
bratr	bratr	k1gMnSc1	bratr
druhého	druhý	k4xOgMnSc4	druhý
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zachvacovala	zachvacovat	k5eAaImAgFnS	zachvacovat
dechem	dech	k1gInSc7	dech
a	a	k8xC	a
pohledem	pohled	k1gInSc7	pohled
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k8xC	a
nedal	dát	k5eNaPmAgMnS	dát
se	se	k3xPyFc4	se
nalézt	nalézt	k5eAaPmF	nalézt
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
zakopal	zakopat	k5eAaPmAgMnS	zakopat
mrtvého	mrtvý	k1gMnSc4	mrtvý
za	za	k7c4	za
peníze	peníz	k1gInPc4	peníz
nebo	nebo	k8xC	nebo
kvůli	kvůli	k7c3	kvůli
přátelství	přátelství	k1gNnSc3	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
domácnosti	domácnost	k1gFnSc2	domácnost
nosili	nosit	k5eAaImAgMnP	nosit
své	svůj	k3xOyFgMnPc4	svůj
mrtvé	mrtvý	k1gMnPc4	mrtvý
do	do	k7c2	do
příkopu	příkop	k1gInSc2	příkop
jak	jak	k8xS	jak
jen	jen	k9	jen
mohli	moct	k5eAaImAgMnP	moct
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
zvony	zvon	k1gInPc1	zvon
za	za	k7c4	za
mrtvé	mrtvý	k1gMnPc4	mrtvý
nezněly	znět	k5eNaImAgFnP	znět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Sieně	Siena	k1gFnSc6	Siena
se	se	k3xPyFc4	se
vykopávaly	vykopávat	k5eAaImAgFnP	vykopávat
velké	velký	k2eAgFnPc1d1	velká
jámy	jáma	k1gFnPc1	jáma
a	a	k8xC	a
plnily	plnit	k5eAaImAgFnP	plnit
množstvím	množství	k1gNnSc7	množství
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
umírali	umírat	k5eAaImAgMnP	umírat
po	po	k7c6	po
stovkách	stovka	k1gFnPc6	stovka
ve	v	k7c6	v
dne	den	k1gInSc2	den
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
vhazováni	vhazovat	k5eAaImNgMnP	vhazovat
do	do	k7c2	do
těch	ten	k3xDgFnPc2	ten
jam	jáma	k1gFnPc2	jáma
a	a	k8xC	a
zahazováni	zahazován	k2eAgMnPc1d1	zahazován
hlínou	hlína	k1gFnSc7	hlína
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
jámy	jáma	k1gFnPc1	jáma
naplnily	naplnit	k5eAaPmAgFnP	naplnit
<g/>
,	,	kIx,	,
vykopávaly	vykopávat	k5eAaImAgFnP	vykopávat
se	se	k3xPyFc4	se
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Agnolo	Agnola	k1gFnSc5	Agnola
z	z	k7c2	z
Tury	tur	k1gMnPc4	tur
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Tlustý	tlustý	k2eAgInSc1d1	tlustý
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
vlastníma	vlastní	k2eAgFnPc7d1	vlastní
rukama	ruka	k1gFnPc7	ruka
zakopal	zakopat	k5eAaPmAgMnS	zakopat
svých	svůj	k3xOyFgFnPc2	svůj
pět	pět	k4xCc4	pět
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
někteří	některý	k3yIgMnPc1	některý
byli	být	k5eAaImAgMnP	být
jen	jen	k9	jen
tak	tak	k6eAd1	tak
málo	málo	k6eAd1	málo
pokryti	pokrýt	k5eAaPmNgMnP	pokrýt
hlínou	hlína	k1gFnSc7	hlína
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
psi	pes	k1gMnPc1	pes
vytahovali	vytahovat	k5eAaImAgMnP	vytahovat
ven	ven	k6eAd1	ven
a	a	k8xC	a
po	po	k7c6	po
městě	město	k1gNnSc6	město
požírali	požírat	k5eAaImAgMnP	požírat
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgMnS	být
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
by	by	kYmCp3nS	by
plakal	plakat	k5eAaImAgMnS	plakat
pro	pro	k7c4	pro
něčí	něčí	k3xOyIgFnSc4	něčí
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
ty	ten	k3xDgMnPc4	ten
očekávané	očekávaný	k2eAgMnPc4d1	očekávaný
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
umíralo	umírat	k5eAaImAgNnS	umírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
žádný	žádný	k3yNgInSc1	žádný
lék	lék	k1gInSc1	lék
či	či	k8xC	či
jiná	jiný	k2eAgFnSc1d1	jiná
ochrana	ochrana	k1gFnSc1	ochrana
nebyly	být	k5eNaImAgFnP	být
k	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
páni	pan	k1gMnPc1	pan
vybrali	vybrat	k5eAaPmAgMnP	vybrat
tři	tři	k4xCgMnPc4	tři
občany	občan	k1gMnPc4	občan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
od	od	k7c2	od
sienské	sienský	k2eAgFnSc2d1	Sienská
obce	obec	k1gFnSc2	obec
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
sto	sto	k4xCgNnSc4	sto
zlatých	zlatý	k2eAgInPc2d1	zlatý
florénů	florén	k1gInPc2	florén
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
měli	mít	k5eAaImAgMnP	mít
dát	dát	k5eAaPmF	dát
na	na	k7c4	na
ubohé	ubohý	k2eAgMnPc4d1	ubohý
nemocné	nemocný	k1gMnPc4	nemocný
a	a	k8xC	a
na	na	k7c4	na
zakopání	zakopání	k1gNnSc4	zakopání
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
všechno	všechen	k3xTgNnSc1	všechen
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
strašné	strašný	k2eAgNnSc1d1	strašné
<g/>
,	,	kIx,	,
že	že	k8xS	že
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
pisatel	pisatel	k1gMnSc1	pisatel
<g/>
,	,	kIx,	,
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
nemohu	moct	k5eNaImIp1nS	moct
pomyslet	pomyslet	k5eAaPmF	pomyslet
a	a	k8xC	a
proto	proto	k8xC	proto
nebudu	být	k5eNaImBp1nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
poměry	poměr	k1gInPc1	poměr
panovaly	panovat	k5eAaImAgInP	panovat
do	do	k7c2	do
září	září	k1gNnSc2	září
a	a	k8xC	a
zabralo	zabrat	k5eAaPmAgNnS	zabrat
by	by	kYmCp3nS	by
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
místa	místo	k1gNnSc2	místo
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
rozepisovat	rozepisovat	k5eAaImF	rozepisovat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
shledalo	shledat	k5eAaPmAgNnS	shledat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
ten	ten	k3xDgInSc4	ten
čas	čas	k1gInSc4	čas
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
Sieně	Siena	k1gFnSc6	Siena
36	[number]	k4	36
000	[number]	k4	000
dvacetiletých	dvacetiletý	k2eAgMnPc2d1	dvacetiletý
nebo	nebo	k8xC	nebo
mladších	mladý	k2eAgMnPc2d2	mladší
<g/>
,	,	kIx,	,
a	a	k8xC	a
umírali	umírat	k5eAaImAgMnP	umírat
staří	starý	k2eAgMnPc1d1	starý
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
52	[number]	k4	52
000	[number]	k4	000
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Sieně	Siena	k1gFnSc6	Siena
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Sieny	Siena	k1gFnSc2	Siena
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
28	[number]	k4	28
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
;	;	kIx,	;
takže	takže	k8xS	takže
celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
shledalo	shledat	k5eAaPmAgNnS	shledat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
předměstích	předměstí	k1gNnPc6	předměstí
80	[number]	k4	80
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
má	mít	k5eAaImIp3nS	mít
Siena	Siena	k1gFnSc1	Siena
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
předměstími	předměstí	k1gNnPc7	předměstí
přes	přes	k7c4	přes
30	[number]	k4	30
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
Sieně	Siena	k1gFnSc6	Siena
samé	samý	k3xTgNnSc1	samý
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
méně	málo	k6eAd2	málo
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
jakoby	jakoby	k8xS	jakoby
zmatení	zmatený	k2eAgMnPc1d1	zmatený
a	a	k8xC	a
bez	bez	k7c2	bez
citů	cit	k1gInPc2	cit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
hradby	hradba	k1gFnPc1	hradba
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
věci	věc	k1gFnPc1	věc
byly	být	k5eAaImAgFnP	být
opuštěny	opuštěn	k2eAgFnPc1d1	opuštěna
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
stříbrné	stříbrná	k1gFnPc1	stříbrná
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgFnPc1d1	zlatá
a	a	k8xC	a
měděné	měděný	k2eAgInPc1d1	měděný
doly	dol	k1gInPc1	dol
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
na	na	k7c6	na
sienském	sienský	k2eAgNnSc6d1	Sienské
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
opuštěny	opustit	k5eAaPmNgInP	opustit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
spatřuje	spatřovat	k5eAaImIp3nS	spatřovat
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
...	...	k?	...
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
vsí	ves	k1gFnPc2	ves
bylo	být	k5eAaImAgNnS	být
opuštěno	opustit	k5eAaPmNgNnS	opustit
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
tam	tam	k6eAd1	tam
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Nebudu	být	k5eNaImBp1nS	být
psát	psát	k5eAaImF	psát
o	o	k7c6	o
ukrutnosti	ukrutnost	k1gFnSc6	ukrutnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
o	o	k7c6	o
vlcích	vlk	k1gMnPc6	vlk
a	a	k8xC	a
divokých	divoký	k2eAgNnPc6d1	divoké
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žrala	žrát	k5eAaImAgFnS	žrát
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
spálená	spálený	k2eAgNnPc1d1	spálené
těla	tělo	k1gNnPc1	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
dalších	další	k2eAgFnPc6d1	další
krutostech	krutost	k1gFnPc6	krutost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
bolestné	bolestný	k2eAgFnPc1d1	bolestná
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
čtou	číst	k5eAaImIp3nP	číst
<g/>
...	...	k?	...
Město	město	k1gNnSc1	město
Siena	Siena	k1gFnSc1	Siena
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
skoro	skoro	k6eAd1	skoro
neobydlené	obydlený	k2eNgNnSc1d1	neobydlené
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nikdo	nikdo	k3yNnSc1	nikdo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nenalézal	nalézat	k5eNaImAgMnS	nalézat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mor	mor	k1gInSc1	mor
zmírnil	zmírnit	k5eAaPmAgInS	zmírnit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
přeživší	přeživší	k2eAgMnPc1d1	přeživší
oddali	oddat	k5eAaPmAgMnP	oddat
radostem	radost	k1gFnPc3	radost
<g/>
:	:	kIx,	:
mniši	mnich	k1gMnPc1	mnich
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
jeptišky	jeptiška	k1gFnPc1	jeptiška
<g/>
,	,	kIx,	,
laici	laik	k1gMnPc1	laik
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
radovali	radovat	k5eAaImAgMnP	radovat
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
si	se	k3xPyFc3	se
nedělal	dělat	k5eNaImAgMnS	dělat
starosti	starost	k1gFnPc4	starost
s	s	k7c7	s
výdaji	výdaj	k1gInPc7	výdaj
a	a	k8xC	a
hrami	hra	k1gFnPc7	hra
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
každý	každý	k3xTgMnSc1	každý
se	se	k3xPyFc4	se
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c2	za
bohatého	bohatý	k2eAgInSc2d1	bohatý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
unikl	uniknout	k5eAaPmAgMnS	uniknout
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
získal	získat	k5eAaPmAgMnS	získat
svět	svět	k1gInSc4	svět
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
přimět	přimět	k5eAaPmF	přimět
k	k	k7c3	k
nicnedělání	nicnedělání	k1gNnSc3	nicnedělání
<g/>
...	...	k?	...
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
čase	čas	k1gInSc6	čas
v	v	k7c6	v
Sieně	Siena	k1gFnSc6	Siena
velký	velký	k2eAgInSc4d1	velký
a	a	k8xC	a
vznešený	vznešený	k2eAgInSc4d1	vznešený
plán	plán	k1gInSc4	plán
zvětšení	zvětšení	k1gNnSc2	zvětšení
sienské	sienský	k2eAgFnSc2d1	Sienská
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
započat	započnout	k5eAaPmNgInS	započnout
před	před	k7c7	před
několika	několik	k4yIc7	několik
roky	rok	k1gInPc7	rok
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
nemoci	nemoc	k1gFnSc2	nemoc
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
nemoc	nemoc	k1gFnSc1	nemoc
léčitelná	léčitelný	k2eAgFnSc1d1	léčitelná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
včasném	včasný	k2eAgNnSc6d1	včasné
zahájení	zahájení	k1gNnSc6	zahájení
léčby	léčba	k1gFnSc2	léčba
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
hodin	hodina	k1gFnPc2	hodina
od	od	k7c2	od
nakažení	nakažení	k1gNnSc2	nakažení
<g/>
)	)	kIx)	)
nemoc	nemoc	k1gFnSc1	nemoc
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
širokospektrá	širokospektrat	k5eAaPmIp3nS	širokospektrat
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
:	:	kIx,	:
streptomycin	streptomycin	k1gInSc1	streptomycin
<g/>
,	,	kIx,	,
tetracyklin	tetracyklin	k1gInSc1	tetracyklin
a	a	k8xC	a
chloramfenikol	chloramfenikol	k1gInSc1	chloramfenikol
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
vakcíny	vakcína	k1gFnPc4	vakcína
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
takřka	takřka	k6eAd1	takřka
nulového	nulový	k2eAgInSc2d1	nulový
výskytu	výskyt	k1gInSc2	výskyt
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
a	a	k8xC	a
slabé	slabý	k2eAgFnSc6d1	slabá
imunitě	imunita	k1gFnSc6	imunita
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
nemoci	nemoc	k1gFnSc3	nemoc
neočkuje	očkovat	k5eNaImIp3nS	očkovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DELUMEAU	DELUMEAU	kA	DELUMEAU
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
ve	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Obležená	obležený	k2eAgFnSc1d1	obležená
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Strach	strach	k1gInSc1	strach
doléhající	doléhající	k2eAgInSc1d1	doléhající
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
288	[number]	k4	288
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
156	[number]	k4	156
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BERGDOLT	BERGDOLT	kA	BERGDOLT
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
:	:	kIx,	:
velký	velký	k2eAgInSc1d1	velký
mor	mor	k1gInSc1	mor
a	a	k8xC	a
konec	konec	k1gInSc1	konec
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
236	[number]	k4	236
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
541	[number]	k4	541
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
GRAUS	GRAUS	kA	GRAUS
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Pest	Pest	k2eAgInSc4d1	Pest
-	-	kIx~	-
Geissler	Geissler	k1gInSc4	Geissler
-	-	kIx~	-
Judenmorde	Judenmord	k1gMnSc5	Judenmord
<g/>
.	.	kIx.	.
</s>
<s>
Das	Das	k?	Das
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Jahrhundert	Jahrhundert	k1gMnSc1	Jahrhundert
als	als	k?	als
Krisenzeit	Krisenzeit	k1gMnSc1	Krisenzeit
<g/>
.	.	kIx.	.
</s>
<s>
Göttingen	Göttingen	k1gInSc1	Göttingen
<g/>
:	:	kIx,	:
Vandenhoeck	Vandenhoeck	k1gInSc1	Vandenhoeck
&	&	k?	&
Ruprecht	Ruprecht	k1gInSc1	Ruprecht
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
608	[number]	k4	608
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ger	ger	k?	ger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MEZNÍK	mezník	k1gInSc1	mezník
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Mory	mora	k1gFnPc1	mora
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mediaevalia	Mediaevalia	k1gFnSc1	Mediaevalia
Historica	Historica	k1gFnSc1	Historica
Bohemica	Bohemica	k1gFnSc1	Bohemica
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
225	[number]	k4	225
<g/>
-	-	kIx~	-
<g/>
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Zhou	Zhoa	k1gMnSc4	Zhoa
D	D	kA	D
<g/>
,	,	kIx,	,
Tong	Tong	k1gInSc1	Tong
Z	Z	kA	Z
<g/>
,	,	kIx,	,
Song	song	k1gInSc1	song
Y	Y	kA	Y
<g/>
,	,	kIx,	,
Han	Hana	k1gFnPc2	Hana
Y	Y	kA	Y
<g/>
,	,	kIx,	,
Pei	Pei	k1gFnSc1	Pei
D	D	kA	D
<g/>
,	,	kIx,	,
Pang	Pang	k1gInSc1	Pang
X	X	kA	X
<g/>
,	,	kIx,	,
Zhai	Zhai	k1gNnSc1	Zhai
J	J	kA	J
<g/>
,	,	kIx,	,
Li	li	k8xS	li
M	M	kA	M
<g/>
,	,	kIx,	,
Cui	Cui	k1gFnSc1	Cui
B	B	kA	B
<g/>
,	,	kIx,	,
Qi	Qi	k1gFnSc1	Qi
Z	Z	kA	Z
<g/>
,	,	kIx,	,
Jin	Jin	k1gFnSc1	Jin
L	L	kA	L
<g/>
,	,	kIx,	,
Dai	Dai	k1gMnSc1	Dai
R	R	kA	R
<g/>
,	,	kIx,	,
Du	Du	k?	Du
Z	Z	kA	Z
<g/>
,	,	kIx,	,
Wang	Wang	k1gInSc1	Wang
J	J	kA	J
<g/>
,	,	kIx,	,
Guo	Guo	k1gFnSc1	Guo
Z	Z	kA	Z
<g/>
,	,	kIx,	,
Wang	Wang	k1gInSc1	Wang
J	J	kA	J
<g/>
,	,	kIx,	,
Huang	Huang	k1gInSc1	Huang
P	P	kA	P
<g/>
,	,	kIx,	,
Yang	Yang	k1gInSc1	Yang
R.	R.	kA	R.
Genetics	Genetics	k1gInSc1	Genetics
of	of	k?	of
metabolic	metabolice	k1gFnPc2	metabolice
variations	variations	k6eAd1	variations
between	between	k1gInSc4	between
Yersinia	Yersinium	k1gNnSc2	Yersinium
pestis	pestis	k1gFnSc2	pestis
biovars	biovars	k1gInSc1	biovars
and	and	k?	and
the	the	k?	the
proposal	proposat	k5eAaPmAgMnS	proposat
of	of	k?	of
a	a	k8xC	a
new	new	k?	new
biovar	biovar	k1gInSc1	biovar
<g/>
,	,	kIx,	,
microtus	microtus	k1gInSc1	microtus
<g/>
.	.	kIx.	.
</s>
<s>
J	J	kA	J
Bacteriol	Bacteriol	k1gInSc1	Bacteriol
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
186	[number]	k4	186
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
,	,	kIx,	,
s.	s.	k?	s.
5147	[number]	k4	5147
<g/>
–	–	k?	–
<g/>
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
DOI	DOI	kA	DOI
<g/>
:	:	kIx,	:
<g/>
10.112	[number]	k4	10.112
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
JB	JB	kA	JB
<g/>
.186	.186	k4	.186
<g/>
.15	.15	k4	.15
<g/>
.5147	.5147	k4	.5147
<g/>
-	-	kIx~	-
<g/>
5152.2004	[number]	k4	5152.2004
<g/>
.	.	kIx.	.
</s>
<s>
PMID	PMID	kA	PMID
15262951	[number]	k4	15262951
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Drancourt	Drancourt	k1gInSc1	Drancourt
M	M	kA	M
<g/>
,	,	kIx,	,
Aboudharam	Aboudharam	k1gInSc1	Aboudharam
G	G	kA	G
<g/>
,	,	kIx,	,
Signolidagger	Signolidagger	k1gInSc1	Signolidagger
M	M	kA	M
<g/>
,	,	kIx,	,
Dutourdagger	Dutourdagger	k1gInSc1	Dutourdagger
O	O	kA	O
<g/>
,	,	kIx,	,
Raoult	Raoult	k2eAgInSc1d1	Raoult
D.	D.	kA	D.
Detection	Detection	k1gInSc1	Detection
of	of	k?	of
400	[number]	k4	400
<g/>
-year-old	earld	k1gInSc1	-year-old
Yersinia	Yersinium	k1gNnSc2	Yersinium
pestis	pestis	k1gFnPc2	pestis
DNA	DNA	kA	DNA
in	in	k?	in
human	human	k1gInSc1	human
dental	dental	k1gMnSc1	dental
pulp	pulpa	k1gFnPc2	pulpa
<g/>
:	:	kIx,	:
An	An	k1gMnSc1	An
approach	approach	k1gMnSc1	approach
to	ten	k3xDgNnSc1	ten
the	the	k?	the
diagnosis	diagnosis	k1gInSc1	diagnosis
of	of	k?	of
ancient	ancient	k1gInSc1	ancient
septicemia	septicemia	k1gFnSc1	septicemia
<g/>
.	.	kIx.	.
</s>
<s>
PNAS	PNAS	kA	PNAS
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
95	[number]	k4	95
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
,	,	kIx,	,
s.	s.	k?	s.
12637	[number]	k4	12637
<g/>
–	–	k?	–
<g/>
12640	[number]	k4	12640
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
PMID	PMID	kA	PMID
9770538	[number]	k4	9770538
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
septický	septický	k2eAgInSc4d1	septický
mor	mor	k1gInSc4	mor
</s>
</p>
<p>
<s>
dýmějový	dýmějový	k2eAgInSc1d1	dýmějový
mor	mor	k1gInSc1	mor
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Černá	černý	k2eAgFnSc1d1	černá
smrt	smrt	k1gFnSc1	smrt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Black	Black	k1gMnSc1	Black
Death	Death	k1gMnSc1	Death
<g/>
,	,	kIx,	,
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
bbc	bbc	k?	bbc
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
</s>
</p>
<p>
<s>
Mor	mor	k1gInSc1	mor
(	(	kIx(	(
<g/>
1346	[number]	k4	1346
<g/>
-	-	kIx~	-
<g/>
1352	[number]	k4	1352
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
e-stredovek	etredovka	k1gFnPc2	e-stredovka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
