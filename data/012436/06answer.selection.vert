<s>
Nemoc	nemoc	k1gFnSc1	nemoc
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
propukaly	propukat	k5eAaImAgFnP	propukat
epidemie	epidemie	k1gFnPc1	epidemie
jak	jak	k8xC	jak
u	u	k7c2	u
místních	místní	k2eAgMnPc2d1	místní
kočovníků	kočovník	k1gMnPc2	kočovník
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgFnPc2d1	další
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
.	.	kIx.	.
</s>
