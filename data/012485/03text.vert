<p>
<s>
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
,	,	kIx,	,
též	též	k9	též
označovaná	označovaný	k2eAgFnSc1d1	označovaná
La	la	k1gNnSc3	la
Gioconda	Gioconda	k1gFnSc1	Gioconda
(	(	kIx(	(
<g/>
portrétovaná	portrétovaný	k2eAgFnSc1d1	portrétovaná
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Lisa	Lisa	k1gFnSc1	Lisa
del	del	k?	del
Giocondo	Giocondo	k1gNnSc1	Giocondo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
portrét	portrét	k1gInSc1	portrét
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
letech	let	k1gInPc6	let
1503	[number]	k4	1503
<g/>
–	–	k?	–
<g/>
1506	[number]	k4	1506
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
1517	[number]	k4	1517
<g/>
)	)	kIx)	)
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
florentského	florentský	k2eAgInSc2d1	florentský
pobytu	pobyt	k1gInSc2	pobyt
namaloval	namalovat	k5eAaPmAgMnS	namalovat
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
malbu	malba	k1gFnSc4	malba
olejem	olej	k1gInSc7	olej
na	na	k7c4	na
dřevo	dřevo	k1gNnSc4	dřevo
topolu	topol	k1gInSc2	topol
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
techniky	technika	k1gFnSc2	technika
sfumato	sfumato	k1gNnSc1	sfumato
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
77	[number]	k4	77
×	×	k?	×
53	[number]	k4	53
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
sleduje	sledovat	k5eAaImIp3nS	sledovat
diváka	divák	k1gMnSc4	divák
s	s	k7c7	s
nenapodobitelným	napodobitelný	k2eNgInSc7d1	nenapodobitelný
<g/>
,	,	kIx,	,
tajuplným	tajuplný	k2eAgInSc7d1	tajuplný
úsměvem	úsměv	k1gInSc7	úsměv
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
pověstí	pověst	k1gFnPc2	pověst
a	a	k8xC	a
spekulací	spekulace	k1gFnPc2	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
obraz	obraz	k1gInSc1	obraz
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
Louvru	Louvre	k1gInSc6	Louvre
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
Monu	Mon	k2eAgFnSc4d1	Mona
Lisu	Lisa	k1gFnSc4	Lisa
dodělával	dodělávat	k5eAaImAgMnS	dodělávat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc4	tento
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
přibalil	přibalit	k5eAaPmAgMnS	přibalit
mezi	mezi	k7c4	mezi
svá	svůj	k3xOyFgNnPc4	svůj
zavazadla	zavazadlo	k1gNnPc4	zavazadlo
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
nuceně	nuceně	k6eAd1	nuceně
opouštěl	opouštět	k5eAaImAgInS	opouštět
Řím	Řím	k1gInSc1	Řím
po	po	k7c4	po
udání	udání	k1gNnSc4	udání
svých	svůj	k3xOyFgMnPc2	svůj
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Leonardo	Leonardo	k1gMnSc1	Leonardo
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
prodal	prodat	k5eAaPmAgMnS	prodat
Monu	Mon	k2eAgFnSc4d1	Mona
Lisu	Lisa	k1gFnSc4	Lisa
francouzskému	francouzský	k2eAgInSc3d1	francouzský
králi	král	k1gMnPc7	král
Františku	františek	k1gInSc2	františek
I.	I.	kA	I.
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jenž	k3xRgNnSc4	jenž
pozvání	pozvání	k1gNnSc4	pozvání
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
přijel	přijet	k5eAaPmAgMnS	přijet
<g/>
,	,	kIx,	,
za	za	k7c4	za
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
částku	částka	k1gFnSc4	částka
4000	[number]	k4	4000
zlatých	zlatý	k2eAgInPc2d1	zlatý
dukátů	dukát	k1gInPc2	dukát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
pro	pro	k7c4	pro
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
známost	známost	k1gFnSc4	známost
obrazu	obraz	k1gInSc2	obraz
sehrála	sehrát	k5eAaPmAgFnS	sehrát
až	až	k9	až
krádež	krádež	k1gFnSc4	krádež
obrazu	obraz	k1gInSc2	obraz
z	z	k7c2	z
Louvru	Louvre	k1gInSc2	Louvre
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byli	být	k5eAaImAgMnP	být
podezíráni	podezírán	k2eAgMnPc1d1	podezírán
stoupenci	stoupenec	k1gMnPc1	stoupenec
umělecké	umělecký	k2eAgFnSc2d1	umělecká
moderny	moderna	k1gFnSc2	moderna
–	–	k?	–
např.	např.	kA	např.
básník	básník	k1gMnSc1	básník
Guillaume	Guillaum	k1gInSc5	Guillaum
Apollinaire	Apollinair	k1gInSc5	Apollinair
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
zatčen	zatknout	k5eAaPmNgMnS	zatknout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
musel	muset	k5eAaImAgMnS	muset
k	k	k7c3	k
výslechu	výslech	k1gInSc3	výslech
<g/>
.	.	kIx.	.
</s>
<s>
Skutečným	skutečný	k2eAgMnSc7d1	skutečný
zlodějem	zloděj	k1gMnSc7	zloděj
byl	být	k5eAaImAgMnS	být
dělník-zasklívač	dělníkasklívač	k1gMnSc1	dělník-zasklívač
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
Louvru	Louvre	k1gInSc2	Louvre
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Perugia	Perugius	k1gMnSc4	Perugius
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
Monu	Mon	k2eAgFnSc4d1	Mona
Lisu	Lisa	k1gFnSc4	Lisa
až	až	k9	až
do	do	k7c2	do
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
ke	k	k7c3	k
koupi	koupě	k1gFnSc4	koupě
antikváři	antikvář	k1gMnSc3	antikvář
Alfrédu	Alfréd	k1gMnSc3	Alfréd
Gerimu	Gerim	k1gMnSc3	Gerim
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
naivním	naivní	k2eAgInSc6d1	naivní
prodeji	prodej	k1gInSc6	prodej
zatčen	zatknout	k5eAaPmNgMnS	zatknout
(	(	kIx(	(
<g/>
přenechal	přenechat	k5eAaPmAgMnS	přenechat
obraz	obraz	k1gInSc4	obraz
antikváři	antikvář	k1gMnPc1	antikvář
k	k	k7c3	k
expertíze	expertíza	k1gFnSc3	expertíza
a	a	k8xC	a
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
peníze	peníz	k1gInPc4	peníz
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
slavnostně	slavnostně	k6eAd1	slavnostně
vrácen	vrátit	k5eAaPmNgInS	vrátit
do	do	k7c2	do
Louvru	Louvre	k1gInSc2	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
krádež	krádež	k1gFnSc4	krádež
dostal	dostat	k5eAaPmAgMnS	dostat
Perugia	Perugium	k1gNnSc2	Perugium
trest	trest	k1gInSc1	trest
pouhých	pouhý	k2eAgInPc2d1	pouhý
7	[number]	k4	7
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
hájil	hájit	k5eAaImAgMnS	hájit
se	se	k3xPyFc4	se
patriotstvím	patriotství	k1gNnSc7	patriotství
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
vrátit	vrátit	k5eAaPmF	vrátit
obraz	obraz	k1gInSc4	obraz
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Zjištění	zjištění	k1gNnPc1	zjištění
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nápad	nápad	k1gInSc1	nápad
na	na	k7c4	na
odcizení	odcizení	k1gNnSc4	odcizení
nepocházel	pocházet	k5eNaImAgInS	pocházet
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
více	hodně	k6eAd2	hodně
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Dnes	dnes	k6eAd1	dnes
obraz	obraz	k1gInSc1	obraz
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
byl	být	k5eAaImAgInS	být
však	však	k9	však
obraz	obraz	k1gInSc1	obraz
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Fontainebleau	Fontainebleaus	k1gInSc2	Fontainebleaus
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Velké	velký	k2eAgFnSc6d1	velká
francouzské	francouzský	k2eAgFnSc3d1	francouzská
revoluci	revoluce	k1gFnSc3	revoluce
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
krom	krom	k7c2	krom
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
pověšen	pověsit	k5eAaPmNgInS	pověsit
v	v	k7c6	v
ložnici	ložnice	k1gFnSc6	ložnice
samotného	samotný	k2eAgMnSc2d1	samotný
Napoleona	Napoleon	k1gMnSc2	Napoleon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Model	model	k1gInSc1	model
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
stál	stát	k5eAaImAgMnS	stát
Leonardovi	Leonardo	k1gMnSc3	Leonardo
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
obraz	obraz	k1gInSc4	obraz
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
předmětem	předmět	k1gInSc7	předmět
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
Lisa	Lisa	k1gFnSc1	Lisa
del	del	k?	del
Giocondo	Giocondo	k1gNnSc1	Giocondo
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
florentského	florentský	k2eAgMnSc2d1	florentský
obchodníka	obchodník	k1gMnSc2	obchodník
Francesca	Francescus	k1gMnSc2	Francescus
del	del	k?	del
Giocondo	Giocondo	k6eAd1	Giocondo
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
první	první	k4xOgMnSc1	první
Leonardův	Leonardův	k2eAgMnSc1d1	Leonardův
životopisec	životopisec	k1gMnSc1	životopisec
Giorgio	Giorgio	k1gMnSc1	Giorgio
Vasari	Vasari	k1gNnPc2	Vasari
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
zastává	zastávat	k5eAaImIp3nS	zastávat
i	i	k9	i
odborník	odborník	k1gMnSc1	odborník
z	z	k7c2	z
knihovny	knihovna	k1gFnSc2	knihovna
německé	německý	k2eAgFnSc2d1	německá
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
Armin	Armin	k2eAgInSc4d1	Armin
Schlechter	Schlechter	k1gInSc4	Schlechter
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nálezu	nález	k1gInSc2	nález
rukopisné	rukopisný	k2eAgFnSc2d1	rukopisná
poznámky	poznámka	k1gFnSc2	poznámka
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
knihy	kniha	k1gFnSc2	kniha
florentského	florentský	k2eAgMnSc4d1	florentský
úředníka	úředník	k1gMnSc4	úředník
Agostina	Agostin	k1gMnSc4	Agostin
Vespucciho	Vespucci	k1gMnSc4	Vespucci
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
1503	[number]	k4	1503
<g/>
.	.	kIx.	.
</s>
<s>
Ital	Ital	k1gMnSc1	Ital
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
známé	známý	k2eAgNnSc4d1	známé
da	da	k?	da
Vinciho	Vinciha	k1gMnSc5	Vinciha
<g/>
,	,	kIx,	,
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
zápiscích	zápisek	k1gInPc6	zápisek
renesančního	renesanční	k2eAgMnSc4d1	renesanční
velikána	velikán	k1gMnSc4	velikán
ke	k	k7c3	k
starověkému	starověký	k2eAgMnSc3d1	starověký
řeckému	řecký	k2eAgMnSc3d1	řecký
malíři	malíř	k1gMnSc3	malíř
Apelléovi	Apelléa	k1gMnSc3	Apelléa
a	a	k8xC	a
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
pracuje	pracovat	k5eAaImIp3nS	pracovat
souběžně	souběžně	k6eAd1	souběžně
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
malbách	malba	k1gFnPc6	malba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
portrét	portrét	k1gInSc1	portrét
Lisy	Lisa	k1gFnSc2	Lisa
del	del	k?	del
Giocondo	Giocondo	k6eAd1	Giocondo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
portrétovanou	portrétovaný	k2eAgFnSc7d1	portrétovaná
ženou	žena	k1gFnSc7	žena
je	být	k5eAaImIp3nS	být
Leonardova	Leonardův	k2eAgFnSc1d1	Leonardova
matka	matka	k1gFnSc1	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rysy	rys	k1gInPc1	rys
díla	dílo	k1gNnSc2	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Da	Da	k?	Da
Vinci	Vinca	k1gMnSc2	Vinca
na	na	k7c6	na
portrétu	portrét	k1gInSc6	portrét
Mony	Mona	k1gFnSc2	Mona
Lisy	Lisa	k1gFnSc2	Lisa
představil	představit	k5eAaPmAgInS	představit
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
převratných	převratný	k2eAgInPc2d1	převratný
postupů	postup	k1gInPc2	postup
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Šířka	šířka	k1gFnSc1	šířka
záběru	záběr	k1gInSc2	záběr
<g/>
"	"	kIx"	"
použitá	použitý	k2eAgFnSc1d1	použitá
v	v	k7c6	v
obrazu	obraz	k1gInSc6	obraz
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
zobrazení	zobrazení	k1gNnSc1	zobrazení
přibližně	přibližně	k6eAd1	přibližně
horní	horní	k2eAgFnSc2d1	horní
poloviny	polovina	k1gFnSc2	polovina
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nepoužívala	používat	k5eNaImAgFnS	používat
–	–	k?	–
osoby	osoba	k1gFnPc1	osoba
byly	být	k5eAaImAgFnP	být
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
buď	buď	k8xC	buď
celé	celý	k2eAgFnPc1d1	celá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
jejich	jejich	k3xOp3gFnSc2	jejich
busty	busta	k1gFnSc2	busta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
středověcí	středověký	k2eAgMnPc1d1	středověký
portrétisté	portrétista	k1gMnPc1	portrétista
malovali	malovat	k5eAaImAgMnP	malovat
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
<g/>
;	;	kIx,	;
malba	malba	k1gFnSc1	malba
z	z	k7c2	z
poloprofilu	poloprofil	k1gInSc2	poloprofil
(	(	kIx(	(
<g/>
částečně	částečně	k6eAd1	částečně
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
en	en	k?	en
face	face	k1gInSc1	face
<g/>
)	)	kIx)	)
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
používaná	používaný	k2eAgFnSc1d1	používaná
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Nezvyklé	zvyklý	k2eNgNnSc1d1	nezvyklé
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
též	též	k9	též
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
postava	postava	k1gFnSc1	postava
na	na	k7c6	na
portrétu	portrét	k1gInSc6	portrét
udržovala	udržovat	k5eAaImAgFnS	udržovat
s	s	k7c7	s
divákem	divák	k1gMnSc7	divák
"	"	kIx"	"
<g/>
oční	oční	k2eAgInSc1d1	oční
kontakt	kontakt	k1gInSc1	kontakt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Mony	Mona	k1gFnSc2	Mona
Lisy	Lisa	k1gFnSc2	Lisa
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozadí	pozadí	k1gNnSc1	pozadí
za	za	k7c7	za
Monou	Moný	k2eAgFnSc7d1	Moný
Lisou	Lisa	k1gFnSc7	Lisa
je	být	k5eAaImIp3nS	být
rozostřené	rozostřený	k2eAgNnSc1d1	rozostřené
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soudobých	soudobý	k2eAgFnPc6d1	soudobá
malbách	malba	k1gFnPc6	malba
bylo	být	k5eAaImAgNnS	být
naopak	naopak	k6eAd1	naopak
vše	všechen	k3xTgNnSc1	všechen
stejně	stejně	k6eAd1	stejně
ostré	ostrý	k2eAgInPc1d1	ostrý
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
čím	čí	k3xOyQgNnSc7	čí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
pozadí	pozadí	k1gNnSc2	pozadí
vzdálenější	vzdálený	k2eAgNnSc1d2	vzdálenější
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
rozostřenější	rozostřený	k2eAgMnSc1d2	rozostřený
a	a	k8xC	a
mdlejší	mdlý	k2eAgMnSc1d2	mdlejší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
něco	něco	k3yInSc4	něco
jako	jako	k9	jako
armagedon	armagedon	k1gInSc4	armagedon
neboli	neboli	k8xC	neboli
konec	konec	k1gInSc4	konec
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
několik	několik	k4yIc1	několik
mostů	most	k1gInPc2	most
a	a	k8xC	a
větší	veliký	k2eAgFnSc2d2	veliký
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
několik	několik	k4yIc4	několik
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimi	on	k3xPp3gNnPc7	on
jsou	být	k5eAaImIp3nP	být
útesy	útes	k1gInPc1	útes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zdaleka	zdaleka	k6eAd1	zdaleka
nejprokreslenější	prokreslený	k2eAgNnSc1d3	prokreslený
stínování	stínování	k1gNnSc1	stínování
a	a	k8xC	a
tónování	tónování	k1gNnSc1	tónování
<g/>
,	,	kIx,	,
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
technikou	technika	k1gFnSc7	technika
sfumato	sfumato	k1gNnSc4	sfumato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
anatomická	anatomický	k2eAgFnSc1d1	anatomická
věrnost	věrnost	k1gFnSc1	věrnost
(	(	kIx(	(
<g/>
ruce	ruka	k1gFnPc1	ruka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neviděné	viděný	k2eNgInPc1d1	neviděný
detaily	detail	k1gInPc1	detail
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
tkanině	tkanina	k1gFnSc6	tkanina
šatů	šat	k1gInPc2	šat
<g/>
.	.	kIx.	.
<g/>
Další	další	k2eAgInPc4d1	další
rysy	rys	k1gInPc4	rys
díla	dílo	k1gNnSc2	dílo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
za	za	k7c7	za
Monou	Moný	k2eAgFnSc7d1	Moný
Lisou	Lisa	k1gFnSc7	Lisa
je	být	k5eAaImIp3nS	být
fantaskní	fantaskní	k2eAgInPc4d1	fantaskní
<g/>
;	;	kIx,	;
horizonty	horizont	k1gInPc4	horizont
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
a	a	k8xC	a
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
výrazně	výrazně	k6eAd1	výrazně
rozdílné	rozdílný	k2eAgFnSc6d1	rozdílná
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
fyzicky	fyzicky	k6eAd1	fyzicky
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
Mony	Mona	k1gFnSc2	Mona
Lisy	Lisa	k1gFnSc2	Lisa
mohly	moct	k5eAaImAgInP	moct
spojit	spojit	k5eAaPmF	spojit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
z	z	k7c2	z
topolového	topolový	k2eAgNnSc2d1	topolové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
)	)	kIx)	)
praskla	prasknout	k5eAaPmAgFnS	prasknout
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
zelený	zelený	k2eAgInSc1d1	zelený
vertikální	vertikální	k2eAgInSc1d1	vertikální
proužek	proužek	k1gInSc1	proužek
<g/>
.	.	kIx.	.
</s>
<s>
Podklad	podklad	k1gInSc1	podklad
i	i	k8xC	i
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
vydrolily	vydrolit	k5eAaPmAgInP	vydrolit
<g/>
;	;	kIx,	;
neexistuje	existovat	k5eNaImIp3nS	existovat
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
původní	původní	k2eAgInSc4d1	původní
barevný	barevný	k2eAgInSc4d1	barevný
charakter	charakter	k1gInSc4	charakter
díla	dílo	k1gNnSc2	dílo
nahradila	nahradit	k5eAaPmAgFnS	nahradit
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
pojidla	pojidlo	k1gNnPc1	pojidlo
<g/>
,	,	kIx,	,
či	či	k8xC	či
techniky	technika	k1gFnPc1	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
další	další	k2eAgFnSc1d1	další
z	z	k7c2	z
teorií	teorie	k1gFnPc2	teorie
objasňující	objasňující	k2eAgInSc4d1	objasňující
"	"	kIx"	"
<g/>
tajemný	tajemný	k2eAgInSc4d1	tajemný
úsměv	úsměv	k1gInSc4	úsměv
<g/>
"	"	kIx"	"
Mony	Mon	k2eAgFnPc4d1	Mona
Lisy	Lisa	k1gFnPc4	Lisa
<g/>
:	:	kIx,	:
kanadští	kanadský	k2eAgMnPc1d1	kanadský
vědci	vědec	k1gMnPc1	vědec
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
úsměv	úsměv	k1gInSc1	úsměv
patří	patřit	k5eAaImIp3nS	patřit
ženě	žena	k1gFnSc3	žena
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
nalezení	nalezení	k1gNnSc1	nalezení
stopy	stopa	k1gFnSc2	stopa
původní	původní	k2eAgFnSc2d1	původní
malby	malba	k1gFnSc2	malba
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
přes	přes	k7c4	přes
tvář	tvář	k1gFnSc4	tvář
tenký	tenký	k2eAgInSc4d1	tenký
šátek	šátek	k1gInSc4	šátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nosívaly	nosívat	k5eAaImAgFnP	nosívat
florentské	florentský	k2eAgFnPc1d1	florentská
ženy	žena	k1gFnPc1	žena
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
něm.	něm.	k?	něm.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
obrazu	obraz	k1gInSc2	obraz
údajně	údajně	k6eAd1	údajně
byla	být	k5eAaImAgFnS	být
kdysi	kdysi	k6eAd1	kdysi
odříznuta	odříznut	k2eAgFnSc1d1	odříznuta
dvojice	dvojice	k1gFnSc1	dvojice
sloupů	sloup	k1gInPc2	sloup
terasy	terasa	k1gFnSc2	terasa
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
původně	původně	k6eAd1	původně
postavu	postava	k1gFnSc4	postava
rámovaly	rámovat	k5eAaImAgInP	rámovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kopie	kopie	k1gFnPc1	kopie
z	z	k7c2	z
Museo	Museo	k1gMnSc1	Museo
del	del	k?	del
Prado	Prado	k1gNnSc4	Prado
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
Museo	Museo	k6eAd1	Museo
del	del	k?	del
Prado	Prado	k1gNnSc1	Prado
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
téměř	téměř	k6eAd1	téměř
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
restauraci	restaurace	k1gFnSc4	restaurace
kopie	kopie	k1gFnSc2	kopie
malby	malba	k1gFnSc2	malba
od	od	k7c2	od
žáka	žák	k1gMnSc2	žák
Leonarda	Leonardo	k1gMnSc2	Leonardo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kopie	kopie	k1gFnSc1	kopie
je	být	k5eAaImIp3nS	být
dlouho	dlouho	k6eAd1	dlouho
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
černého	černý	k2eAgInSc2d1	černý
nánosu	nános	k1gInSc2	nános
barvy	barva	k1gFnSc2	barva
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc1	obraz
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
obyčejnou	obyčejný	k2eAgFnSc7d1	obyčejná
kopií	kopie	k1gFnSc7	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
provedeny	provést	k5eAaPmNgFnP	provést
i	i	k8xC	i
stejné	stejný	k2eAgFnPc1d1	stejná
změny	změna	k1gFnPc1	změna
jako	jako	k9	jako
u	u	k7c2	u
originálu	originál	k1gInSc2	originál
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
malba	malba	k1gFnSc1	malba
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
namalována	namalován	k2eAgFnSc1d1	namalována
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
samotného	samotný	k2eAgMnSc2d1	samotný
mistra	mistr	k1gMnSc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Kopie	kopie	k1gFnSc1	kopie
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
detailů	detail	k1gInPc2	detail
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
modelem	model	k1gInSc7	model
byla	být	k5eAaImAgFnS	být
krásná	krásný	k2eAgFnSc1d1	krásná
mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
originálu	originál	k1gInSc6	originál
vypadá	vypadat	k5eAaPmIp3nS	vypadat
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
starší	starý	k2eAgFnSc1d2	starší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
poškozen	poškodit	k5eAaPmNgInS	poškodit
a	a	k8xC	a
vrstva	vrstva	k1gFnSc1	vrstva
laku	lak	k1gInSc2	lak
ztmavla	ztmavnout	k5eAaPmAgFnS	ztmavnout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kopie	kopie	k1gFnSc1	kopie
lépe	dobře	k6eAd2	dobře
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
portrét	portrét	k1gInSc1	portrét
původně	původně	k6eAd1	původně
vypadal	vypadat	k5eAaPmAgInS	vypadat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
lak	lak	k1gInSc4	lak
na	na	k7c6	na
malbě	malba	k1gFnSc6	malba
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
zežloutl	zežloutnout	k5eAaPmAgInS	zežloutnout
a	a	k8xC	a
popraskal	popraskat	k5eAaPmAgInS	popraskat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Dmitrij	Dmitrít	k5eAaPmRp2nS	Dmitrít
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Merežkovskij	Merežkovskij	k1gMnSc1	Merežkovskij
<g/>
:	:	kIx,	:
Leonardo	Leonardo	k1gMnSc1	Leonardo
–	–	k?	–
Mona	Mona	k1gFnSc1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Pelcl	Pelcl	k1gInSc1	Pelcl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
</s>
</p>
<p>
<s>
Donald	Donald	k1gMnSc1	Donald
Sassoon	Sassoon	k1gMnSc1	Sassoon
<g/>
:	:	kIx,	:
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
–	–	k?	–
historie	historie	k1gFnSc2	historie
nejslavnějšího	slavný	k2eAgInSc2d3	nejslavnější
obrazu	obraz	k1gInSc2	obraz
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7341-273-X	[number]	k4	80-7341-273-X
</s>
</p>
<p>
<s>
Dieter	Dieter	k1gMnSc1	Dieter
Sinn	Sinn	k1gMnSc1	Sinn
<g/>
:	:	kIx,	:
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
–	–	k?	–
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
Gioconda	Gioconda	k1gFnSc1	Gioconda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Terence	Terence	k1gFnSc1	Terence
Harold	Harold	k1gMnSc1	Harold
Robsjohn-Gibbings	Robsjohn-Gibbings	k1gInSc1	Robsjohn-Gibbings
<g/>
:	:	kIx,	:
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
mustache	mustache	k1gFnSc7	mustache
–	–	k?	–
a	a	k8xC	a
dissection	dissection	k1gInSc1	dissection
of	of	k?	of
modern	modern	k1gInSc1	modern
art	art	k?	art
<g/>
,	,	kIx,	,
Alfred	Alfred	k1gMnSc1	Alfred
A.	A.	kA	A.
Knopf	Knopf	k1gMnSc1	Knopf
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
1947	[number]	k4	1947
</s>
</p>
<p>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2004	[number]	k4	2004
–	–	k?	–
příloha	příloha	k1gFnSc1	příloha
Víkend	víkend	k1gInSc1	víkend
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Obrazová	obrazový	k2eAgFnSc1d1	obrazová
kompozice	kompozice	k1gFnSc1	kompozice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Mona	Mona	k1gFnSc1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
