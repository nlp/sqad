<s>
Krajina	Krajina	k1gFnSc1	Krajina
za	za	k7c7	za
Monou	Moný	k2eAgFnSc7d1	Moný
Lisou	Lisa	k1gFnSc7	Lisa
je	být	k5eAaImIp3nS	být
fantaskní	fantaskní	k2eAgInPc4d1	fantaskní
<g/>
;	;	kIx,	;
horizonty	horizont	k1gInPc4	horizont
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
a	a	k8xC	a
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
výrazně	výrazně	k6eAd1	výrazně
rozdílné	rozdílný	k2eAgFnSc6d1	rozdílná
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
fyzicky	fyzicky	k6eAd1	fyzicky
téměř	téměř	k6eAd1	téměř
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
Mony	Mona	k1gFnSc2	Mona
Lisy	Lisa	k1gFnSc2	Lisa
mohly	moct	k5eAaImAgInP	moct
spojit	spojit	k5eAaPmF	spojit
<g/>
.	.	kIx.	.
</s>
