<s>
Šlomo	Šlomo	k6eAd1
Amar	Amar	k1gInSc1
</s>
<s>
Šlomo	Šlomo	k6eAd1
Amar	Amar	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
1948	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
nisanu	nisan	k1gInSc2
5708	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
72	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Casablanca	Casablanca	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
rabín	rabín	k1gMnSc1
Denominace	denominace	k1gFnSc2
</s>
<s>
ortodoxní	ortodoxní	k2eAgInSc1d1
judaismus	judaismus	k1gInSc1
Etnický	etnický	k2eAgInSc4d1
původ	původ	k1gInSc4
</s>
<s>
Sefardi	Sefard	k1gMnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
izraelském	izraelský	k2eAgMnSc6d1
rabínovi	rabín	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
politikovi	politik	k1gMnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Šlomo	Šloma	k1gFnSc5
Amar	Amar	k1gMnSc1
(	(	kIx(
<g/>
politik	politik	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Šlomo	Šlomo	k1gMnSc1
Moše	Moše	k1gMnSc1
Amar	Amar	k1gMnSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ש	ש	k?
מ	מ	k?
ע	ע	k?
<g/>
;	;	kIx,
narozen	narozen	k2eAgInSc1d1
1948	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
sefardský	sefardský	k2eAgMnSc1d1
vrchní	vrchní	k2eAgMnSc1d1
rabín	rabín	k1gMnSc1
Izraele	Izrael	k1gInSc2
v	v	k7c6
letech	let	k1gInPc6
2003	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
aškenázským	aškenázský	k2eAgInSc7d1
protějškem	protějšek	k1gInSc7
byl	být	k5eAaImAgMnS
rabi	rabi	k1gMnSc1
Jona	Jona	k1gMnSc1
Metzger	Metzger	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Rabi	rabi	k1gMnSc1
Amar	Amar	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
Casablance	Casablanca	k1gFnSc6
v	v	k7c6
Maroku	Maroko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc4d1
vzdělání	vzdělání	k1gNnSc1
obdržel	obdržet	k5eAaPmAgInS
na	na	k7c6
židovských	židovský	k2eAgFnPc6d1
školách	škola	k1gFnPc6
Ocar	Ocar	k1gMnSc1
ha-Tora	ha-Tora	k1gFnSc1
a	a	k8xC
Neve	Neve	k1gFnSc1
šalom	šalom	k0
a	a	k8xC
na	na	k7c6
chabadnické	chabadnický	k2eAgFnSc6d1
ješivě	ješiva	k1gFnSc6
Tomchej	Tomchej	k1gMnSc1
temimim	temimim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
odešel	odejít	k5eAaPmAgMnS
spolu	spolu	k6eAd1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
rodiči	rodič	k1gMnPc7
do	do	k7c2
Izraele	Izrael	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
usadili	usadit	k5eAaPmAgMnP
v	v	k7c6
obci	obec	k1gFnSc6
Pardes	Pardesa	k1gFnPc2
Chana	Chana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1963	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
ješivě	ješiva	k1gFnSc6
Tif	Tif	k1gFnSc2
<g/>
'	'	kIx"
<g/>
eret	eret	k2eAgInSc1d1
Cijon	Cijon	k1gInSc1
v	v	k7c4
Benej	Benej	k1gInSc4
Brak	braka	k1gFnPc2
a	a	k8xC
na	na	k7c6
ješivě	ješiva	k1gFnSc6
Ša	Ša	k1gFnSc2
<g/>
'	'	kIx"
<g/>
arit	arit	k1gInSc1
Josef	Josef	k1gMnSc1
u	u	k7c2
rabi	rabi	k1gMnSc1
Nisima	Nisim	k1gMnSc2
Toledana	Toledan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1967	#num#	k4
přešel	přejít	k5eAaPmAgInS
na	na	k7c4
ješivu	ješiva	k1gFnSc4
ve	v	k7c6
Šlomi	Šlom	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
požádán	požádat	k5eAaPmNgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dlouhodobě	dlouhodobě	k6eAd1
zastupoval	zastupovat	k5eAaImAgInS
na	na	k7c4
pozici	pozice	k1gFnSc4
rabína	rabín	k1gMnSc2
a	a	k8xC
vedoucího	vedoucí	k1gMnSc2
ješivy	ješiva	k1gFnSc2
rabi	rabi	k1gMnSc1
Mas	masa	k1gFnPc2
<g/>
'	'	kIx"
<g/>
uda	uda	k?
Revacha	Revach	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Současně	současně	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
studiích	studio	k1gNnPc6
rabínského	rabínský	k2eAgNnSc2d1
soudnictví	soudnictví	k1gNnSc2
u	u	k7c2
rabi	rabi	k1gMnSc1
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akova	akova	k1gFnSc1
Nisen	Nisna	k1gFnPc2
Rosentala	Rosental	k1gMnSc2
z	z	k7c2
Haify	Haifa	k1gFnSc2
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
hlavního	hlavní	k2eAgMnSc4d1
učitele	učitel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
byl	být	k5eAaImAgInS
jmenován	jmenovat	k5eAaImNgInS,k5eAaBmNgInS
rabínem	rabín	k1gMnSc7
a	a	k8xC
vedoucím	vedoucí	k1gMnSc7
oddělení	oddělení	k1gNnSc2
kašrutu	kašrut	k2eAgFnSc4d1
v	v	k7c6
Náboženské	náboženský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
v	v	k7c6
Neharji	Neharj	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
rabínem	rabín	k1gMnSc7
mošavu	mošavat	k5eAaPmIp1nS
Megadim	Megadim	k1gMnSc1
v	v	k7c6
oblastní	oblastní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
Chof	Chof	k1gMnSc1
ha-Karmel	ha-Karmel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byl	být	k5eAaImAgInS
mašgiachem	mašgiach	k1gMnSc7
v	v	k7c6
ješivě	ješiva	k1gFnSc6
Tora	Tor	k1gMnSc2
ve-hora	ve-hor	k1gMnSc2
<g/>
'	'	kIx"
<g/>
a	a	k8xC
v	v	k7c6
Tel	tel	kA
Avivu	Aviv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
obdržel	obdržet	k5eAaPmAgMnS
osvědčení	osvědčení	k1gNnSc4
(	(	kIx(
<g/>
smichu	smicha	k1gFnSc4
<g/>
)	)	kIx)
pro	pro	k7c4
rituální	rituální	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
(	(	kIx(
<g/>
šochet	šochet	k1gInSc1
u-bodek	u-bodek	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pro	pro	k7c4
funkci	funkce	k1gFnSc4
rabína	rabín	k1gMnSc2
obce	obec	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
funkci	funkce	k1gFnSc4
rabína	rabín	k1gMnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povinnou	povinný	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
v	v	k7c6
izraelské	izraelský	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
následnou	následný	k2eAgFnSc4d1
záložní	záložní	k2eAgFnSc4d1
službu	služba	k1gFnSc4
vykonal	vykonat	k5eAaPmAgMnS
rabi	rabi	k1gMnSc1
Amar	Amar	k1gMnSc1
v	v	k7c6
jednotce	jednotka	k1gFnSc6
pro	pro	k7c4
identifikaci	identifikace	k1gFnSc4
padlých	padlý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
sérii	série	k1gFnSc6
cvičení	cvičení	k1gNnPc2
v	v	k7c6
záchranářství	záchranářství	k1gNnSc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
zabývat	zabývat	k5eAaImF
výukou	výuka	k1gFnSc7
halachických	halachický	k2eAgNnPc2d1
témat	téma	k1gNnPc2
souvisejících	související	k2eAgInPc2d1
s	s	k7c7
činností	činnost	k1gFnSc7
oddílu	oddíl	k1gInSc2
<g/>
;	;	kIx,
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
přednáškách	přednáška	k1gFnPc6
se	se	k3xPyFc4
soustředil	soustředit	k5eAaPmAgInS
na	na	k7c4
význam	význam	k1gInSc4
hodnot	hodnota	k1gFnPc2
úcty	úcta	k1gFnSc2
k	k	k7c3
mrtvým	mrtvý	k2eAgInPc3d1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
halachickou	halachický	k2eAgFnSc4d1
aplikaci	aplikace	k1gFnSc4
v	v	k7c6
praxi	praxe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgFnPc4d1
přednášky	přednáška	k1gFnPc4
měl	mít	k5eAaImAgMnS
také	také	k9
pro	pro	k7c4
členy	člen	k1gInPc4
organizace	organizace	k1gFnSc2
ZAKA	ZAKA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praktické	praktický	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
s	s	k7c7
tematikou	tematika	k1gFnSc7
padlých	padlý	k1gMnPc2
a	a	k8xC
nezvěstných	zvěstný	k2eNgMnPc2d1
se	se	k3xPyFc4
podepsaly	podepsat	k5eAaPmAgFnP
také	také	k9
na	na	k7c6
jeho	jeho	k3xOp3gNnPc6
halachických	halachický	k2eAgNnPc6d1
rozhodnutích	rozhodnutí	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
pronikla	proniknout	k5eAaPmAgFnS
ve	v	k7c4
známost	známost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Úzce	úzko	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
dřívějším	dřívější	k2eAgMnSc7d1
sefardským	sefardský	k2eAgMnSc7d1
vrchním	vrchní	k2eAgMnSc7d1
rabínem	rabín	k1gMnSc7
a	a	k8xC
duchovním	duchovní	k2eAgMnSc7d1
vůdcem	vůdce	k1gMnSc7
strany	strana	k1gFnSc2
Šas	Šas	k1gMnSc7
<g/>
,	,	kIx,
rabi	rabi	k1gMnSc1
Ovadjou	Ovadja	k1gMnSc7
Josefem	Josef	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
svým	svůj	k3xOyFgNnSc7
zvolením	zvolení	k1gNnSc7
do	do	k7c2
funkce	funkce	k1gFnSc2
vrchního	vrchní	k2eAgMnSc2d1
rabína	rabín	k1gMnSc2
Izraele	Izrael	k1gInSc2
rabi	rabi	k1gMnSc1
Amar	Amar	k1gMnSc1
působil	působit	k5eAaImAgMnS
jako	jako	k9
předseda	předseda	k1gMnSc1
rabínského	rabínský	k2eAgInSc2d1
soudu	soud	k1gInSc2
v	v	k7c6
Petach	Peta	k1gFnPc6
Tikvě	Tikva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
vrchním	vrchní	k2eAgMnSc7d1
rabínem	rabín	k1gMnSc7
Tel	tel	kA
Avivu	Aviv	k1gInSc2
jakožto	jakožto	k8xS
první	první	k4xOgMnSc1
jediný	jediný	k2eAgMnSc1d1
vrchní	vrchní	k2eAgMnSc1d1
rabín	rabín	k1gMnSc1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
ho	on	k3xPp3gInSc4
seznámil	seznámit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
rabín	rabín	k1gMnSc1
Ja	Ja	k?
<g/>
'	'	kIx"
<g/>
akov	akov	k1gMnSc1
Rosental	Rosental	k1gMnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
12	#num#	k4
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgMnSc7
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
bratrů	bratr	k1gMnPc2
je	být	k5eAaImIp3nS
hudebník	hudebník	k1gMnSc1
Albert	Albert	k1gMnSc1
Amar	Amar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Šema	Šema	k6eAd1
Šlomo	Šloma	k1gFnSc5
<g/>
7	#num#	k4
svazků	svazek	k1gInPc2
respons	responsa	k1gFnPc2
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
rabi	rabi	k1gMnSc1
Amara	Amara	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
vyšlo	vyjít	k5eAaPmAgNnS
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
a	a	k8xC
bylo	být	k5eAaImAgNnS
dále	daleko	k6eAd2
rozšířeno	rozšířit	k5eAaPmNgNnS
v	v	k7c6
době	doba	k1gFnSc6
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
působení	působení	k1gNnSc2
jako	jako	k8xC,k8xS
rabínského	rabínský	k2eAgMnSc2d1
soudce	soudce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaobírá	zaobírat	k5eAaImIp3nS
se	s	k7c7
především	především	k6eAd1
halachickými	halachický	k2eAgFnPc7d1
tématy	téma	k1gNnPc7
pojednanými	pojednaná	k1gFnPc7
v	v	k7c6
části	část	k1gFnSc6
Šulchan	Šulchany	k1gInPc2
aruchu	aruch	k1gInSc2
nazvané	nazvaný	k2eAgFnSc2d1
Chošen	Chošen	k2eAgInSc1d1
mišpat	mišpat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Kerem	Kerem	k6eAd1
Šlomodvoudílná	Šlomodvoudílný	k2eAgFnSc1d1
responsa	responsa	k1gFnSc1
pojednávající	pojednávající	k2eAgFnSc1d1
o	o	k7c6
smíšených	smíšený	k2eAgInPc6d1
druzích	druh	k1gInPc6
a	a	k8xC
přikázáních	přikázání	k1gNnPc6
souvisejících	související	k2eAgFnPc2d1
se	se	k3xPyFc4
zemí	zem	k1gFnPc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Be	Be	k?
<g/>
'	'	kIx"
<g/>
erah	erah	k1gMnSc1
šel	jít	k5eAaImAgMnS
Mirjamresponsa	Mirjamresponsa	k1gFnSc1
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
na	na	k7c4
památku	památka	k1gFnSc4
matky	matka	k1gFnSc2
rabi	rabi	k1gMnSc1
Amara	Amara	k1gMnSc1
<g/>
,	,	kIx,
pojednávají	pojednávat	k5eAaImIp3nP
o	o	k7c6
otázkách	otázka	k1gFnPc6
kašrutu	kašrut	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Birkat	Birkat	k5eAaImF,k5eAaBmF,k5eAaPmF
Elijahukomentáře	Elijahukomentář	k1gMnPc4
k	k	k7c3
týdenní	týdenní	k2eAgFnSc3d1
paraše	paracha	k1gFnSc3
pojmenované	pojmenovaný	k2eAgFnSc2d1
na	na	k7c4
památku	památka	k1gFnSc4
otce	otec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
vyšly	vyjít	k5eAaPmAgInP
části	část	k1gFnPc4
Berešit	Berešit	k1gFnPc2
a	a	k8xC
Šemot	Šemota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mac	Mac	kA
<g/>
'	'	kIx"
<g/>
halot	halot	k1gInSc1
chatanimčlánky	chatanimčlánka	k1gFnSc2
o	o	k7c6
halachických	halachický	k2eAgNnPc6d1
tématech	téma	k1gNnPc6
souvisejících	související	k2eAgInPc2d1
s	s	k7c7
židovskou	židovský	k2eAgFnSc7d1
svatbou	svatba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
ש	ש	k?
ע	ע	k?
na	na	k7c6
hebrejské	hebrejský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Shlomo	Shloma	k1gFnSc5
Amar	Amara	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šlomo	Šloma	k1gFnSc5
Amar	Amar	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Záznam	záznam	k1gInSc1
a	a	k8xC
stručný	stručný	k2eAgInSc4d1
životopis	životopis	k1gInSc4
na	na	k7c4
Jewish	Jewish	k1gInSc4
Virtual	Virtual	k1gInSc4
Library	Librara	k1gFnSc2
</s>
<s>
Vrchní	vrchní	k2eAgMnSc1d1
sefardský	sefardský	k2eAgMnSc1d1
rabín	rabín	k1gMnSc1
Izraele	Izrael	k1gInSc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Elijahu	Elijaha	k1gFnSc4
Bakši	Bakše	k1gFnSc4
Doron	Dorona	k1gFnPc2
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
Šlomo	Šloma	k1gFnSc5
Amar	Amara	k1gFnPc2
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Jicchak	Jicchak	k1gMnSc1
Josef	Josef	k1gMnSc1
</s>
