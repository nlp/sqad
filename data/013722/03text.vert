<s>
Reflektor	reflektor	k1gInSc1
(	(	kIx(
<g/>
fotografie	fotografia	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1
reflektor	reflektor	k1gInSc1
s	s	k7c7
deštníkem	deštník	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
rozptýlení	rozptýlení	k1gNnSc3
světla	světlo	k1gNnSc2
z	z	k7c2
připevněné	připevněný	k2eAgFnSc2d1
fotografické	fotografický	k2eAgFnSc2d1
lampy	lampa	k1gFnSc2
</s>
<s>
Hans	Hans	k1gMnSc1
Blohm	Blohm	k1gMnSc1
fotografuje	fotografovat	k5eAaImIp3nS
na	na	k7c4
velký	velký	k2eAgInSc4d1
formát	formát	k1gInSc4
v	v	k7c6
Kanadské	kanadský	k2eAgFnSc6d1
Dolní	dolní	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Světlo	světlo	k1gNnSc4
ve	v	k7c6
fotografii	fotografia	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
fotografii	fotografia	k1gFnSc6
a	a	k8xC
kinematografii	kinematografie	k1gFnSc6
slouží	sloužit	k5eAaImIp3nS
reflektor	reflektor	k1gInSc1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
speciální	speciální	k2eAgFnSc2d1
odrazné	odrazný	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
ke	k	k7c3
směrování	směrování	k1gNnSc3
světla	světlo	k1gNnSc2
na	na	k7c4
daný	daný	k2eAgInSc4d1
předmět	předmět	k1gInSc4
nebo	nebo	k8xC
scénu	scéna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
reflecto	reflecta	k1gMnSc5
=	=	kIx~
ohýbám	ohýbat	k5eAaImIp1nS
nazpět	nazpět	k6eAd1
<g/>
,	,	kIx,
odrážím	odrážet	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
ve	v	k7c6
studiovém	studiový	k2eAgNnSc6d1
osvětlení	osvětlení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
</s>
<s>
Kromě	kromě	k7c2
některých	některý	k3yIgInPc2
vysoce	vysoce	k6eAd1
specializovaných	specializovaný	k2eAgInPc2d1
komponentů	komponent	k1gInPc2
ve	v	k7c6
zvětšovacích	zvětšovací	k2eAgInPc6d1
přístrojích	přístroj	k1gInPc6
<g/>
,	,	kIx,
projektorech	projektor	k1gInPc6
<g/>
,	,	kIx,
scannerech	scanner	k1gInPc6
spadají	spadat	k5eAaImIp3nP,k5eAaPmIp3nP
fotografické	fotografický	k2eAgInPc1d1
reflektory	reflektor	k1gInPc1
do	do	k7c2
dvou	dva	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Lampové	lampový	k2eAgInPc4d1
reflektory	reflektor	k1gInPc4
</s>
<s>
Princip	princip	k1gInSc1
reflektoru	reflektor	k1gInSc2
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
cestu	cesta	k1gFnSc4
světla	světlo	k1gNnSc2
od	od	k7c2
žárovky	žárovka	k1gFnSc2
přes	přes	k7c4
odraznou	odrazný	k2eAgFnSc4d1
desku	deska	k1gFnSc4
na	na	k7c4
osvětlenou	osvětlený	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
</s>
<s>
V	v	k7c6
dnešních	dnešní	k2eAgInPc6d1
světlometech	světlomet	k1gInPc6
bývá	bývat	k5eAaImIp3nS
zdrojem	zdroj	k1gInSc7
světla	světlo	k1gNnSc2
elektrická	elektrický	k2eAgFnSc1d1
žárovka	žárovka	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
halogenová	halogenový	k2eAgFnSc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
výbojka	výbojka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
světlometu	světlomet	k1gInSc2
je	být	k5eAaImIp3nS
reflektor	reflektor	k1gInSc1
(	(	kIx(
<g/>
odrazná	odrazný	k2eAgFnSc1d1
deska	deska	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
koncentraci	koncentrace	k1gFnSc4
světla	světlo	k1gNnSc2
v	v	k7c6
požadovaném	požadovaný	k2eAgInSc6d1
směru	směr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
duté	dutý	k2eAgNnSc4d1
zrcadlo	zrcadlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
umístěné	umístěný	k2eAgNnSc4d1
za	za	k7c7
světelným	světelný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
a	a	k8xC
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
odráží	odrážet	k5eAaImIp3nP
světelné	světelný	k2eAgInPc1d1
paprsky	paprsek	k1gInPc1
šířící	šířící	k2eAgInPc1d1
se	se	k3xPyFc4
jiným	jiný	k2eAgInSc7d1
než	než	k8xS
požadovaným	požadovaný	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
rovnoběžných	rovnoběžný	k2eAgInPc2d1
paprsků	paprsek	k1gInPc2
<g/>
,	,	kIx,
zrcadlo	zrcadlo	k1gNnSc1
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
paraboly	parabola	k1gFnSc2
a	a	k8xC
světelný	světelný	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgMnS
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
ohnisku	ohnisko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Faktor	faktor	k1gInSc1
reflektoru	reflektor	k1gInSc2
je	být	k5eAaImIp3nS
poměr	poměr	k1gInSc1
osvětlení	osvětlení	k1gNnSc2
světlometu	světlomet	k1gInSc2
s	s	k7c7
reflektorem	reflektor	k1gInSc7
k	k	k7c3
osvětlení	osvětlení	k1gNnSc3
bez	bez	k7c2
namontovaného	namontovaný	k2eAgInSc2d1
reflektoru	reflektor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matný	matný	k2eAgInSc1d1
reflektor	reflektor	k1gInSc1
zpravidla	zpravidla	k6eAd1
má	mít	k5eAaImIp3nS
faktor	faktor	k1gInSc1
kolem	kolem	k7c2
hodnoty	hodnota	k1gFnSc2
2	#num#	k4
<g/>
,	,	kIx,
vzhledem	vzhled	k1gInSc7
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
většímu	veliký	k2eAgInSc3d2
rozptylovému	rozptylový	k2eAgInSc3d1
účinku	účinek	k1gInSc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
lesklé	lesklý	k2eAgFnPc1d1
nebo	nebo	k8xC
kovové	kovový	k2eAgFnPc1d1
odrazky	odrazka	k1gFnPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
koeficient	koeficient	k1gInSc4
až	až	k9
6	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Velmi	velmi	k6eAd1
častým	častý	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
této	tento	k3xDgFnSc2
techniky	technika	k1gFnSc2
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc1d1
deštníkový	deštníkový	k2eAgInSc1d1
reflektor	reflektor	k1gInSc1
<g/>
,	,	kIx,
zpravidla	zpravidla	k6eAd1
se	s	k7c7
zlatým	zlatý	k2eAgInSc7d1
<g/>
,	,	kIx,
stříbrným	stříbrný	k2eAgInSc7d1
nebo	nebo	k8xC
matným	matný	k2eAgInSc7d1
bílým	bílý	k2eAgInSc7d1
interiérem	interiér	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
je	být	k5eAaImIp3nS
namířena	namířen	k2eAgFnSc1d1
svítilna	svítilna	k1gFnSc1
s	s	k7c7
kruhovým	kruhový	k2eAgInSc7d1
reflektorem	reflektor	k1gInSc7
a	a	k8xC
ve	v	k7c6
výsledku	výsledek	k1gInSc6
poskytuje	poskytovat	k5eAaImIp3nS
široké	široký	k2eAgNnSc1d1
a	a	k8xC
měkké	měkký	k2eAgNnSc1d1
osvětlení	osvětlení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Deskové	deskový	k2eAgInPc4d1
reflektory	reflektor	k1gInPc4
</s>
<s>
Přenosný	přenosný	k2eAgInSc1d1
skládací	skládací	k2eAgInSc1d1
reflektor	reflektor	k1gInSc1
odráží	odrážet	k5eAaImIp3nS
sluneční	sluneční	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
na	na	k7c4
modelku	modelka	k1gFnSc4
</s>
<s>
Známé	známý	k2eAgInPc1d1
jako	jako	k8xC,k8xS
rovinné	rovinný	k2eAgInPc1d1
reflektory	reflektor	k1gInPc1
<g/>
,	,	kIx,
„	„	k?
<g/>
lívance	lívanec	k1gInPc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
“	“	k?
<g/>
„	„	k?
<g/>
flats	flats	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
odrazné	odrazný	k2eAgFnPc4d1
desky	deska	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
reflektoru	reflektor	k1gInSc3
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
nezávisle	závisle	k6eNd1
mimo	mimo	k7c4
zdroj	zdroj	k1gInSc4
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
od	od	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
povrchu	povrch	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
buď	buď	k8xC
pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
širšího	široký	k2eAgInSc2d2
světelného	světelný	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
<g/>
,	,	kIx,
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
vržených	vržený	k2eAgInPc2d1
stínů	stín	k1gInPc2
nebo	nebo	k8xC
obojí	oboj	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
reflektoru	reflektor	k1gInSc2
má	mít	k5eAaImIp3nS
obecně	obecně	k6eAd1
velmi	velmi	k6eAd1
nízký	nízký	k2eAgInSc1d1
faktor	faktor	k1gInSc1
odrazu	odraz	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
značně	značně	k6eAd1
liší	lišit	k5eAaImIp3nP
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
povrchu	povrch	k1gInSc6
a	a	k8xC
barvě	barva	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejvíce	nejvíce	k6eAd1,k6eAd3
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
pro	pro	k7c4
kontrolu	kontrola	k1gFnSc4
kontrastu	kontrast	k1gInSc2
při	při	k7c6
umělém	umělý	k2eAgNnSc6d1
i	i	k8xC
přírodním	přírodní	k2eAgNnSc6d1
osvětlení	osvětlení	k1gNnSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
doplňkové	doplňkový	k2eAgNnSc4d1
(	(	kIx(
<g/>
podpůrné	podpůrný	k2eAgNnSc4d1
<g/>
)	)	kIx)
světlo	světlo	k1gNnSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
„	„	k?
<g/>
fill	fill	k1gMnSc1
light	light	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vykrývat	vykrývat	k5eAaImF
stinné	stinný	k2eAgFnPc4d1
partie	partie	k1gFnPc4
snímku	snímek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
světlo	světlo	k1gNnSc1
se	se	k3xPyFc4
z	z	k7c2
hlavního	hlavní	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
odráží	odrážet	k5eAaImIp3nS
zpět	zpět	k6eAd1
do	do	k7c2
scény	scéna	k1gFnSc2
s	s	k7c7
různým	různý	k2eAgInSc7d1
stupněm	stupeň	k1gInSc7
přesnosti	přesnost	k1gFnSc2
a	a	k8xC
intenzity	intenzita	k1gFnSc2
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
zvolené	zvolený	k2eAgFnSc6d1
odrazné	odrazný	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
a	a	k8xC
jejím	její	k3xOp3gNnSc6
umístění	umístění	k1gNnSc6
ve	v	k7c6
vztahu	vztah	k1gInSc6
ke	k	k7c3
scéně	scéna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Reflektory	reflektor	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
také	také	k6eAd1
použity	použít	k5eAaPmNgInP
ke	k	k7c3
zvětšení	zvětšení	k1gNnSc3
velikosti	velikost	k1gFnSc2
hlavního	hlavní	k2eAgInSc2d1
světelného	světelný	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
<g/>
,	,	kIx,
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
může	moct	k5eAaImIp3nS
(	(	kIx(
<g/>
nebo	nebo	k8xC
nemusí	muset	k5eNaImIp3nS
<g/>
)	)	kIx)
ponechat	ponechat	k5eAaPmF
přímou	přímý	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
na	na	k7c4
scénu	scéna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změnou	změna	k1gFnSc7
vzdálenosti	vzdálenost	k1gFnSc2
desky	deska	k1gFnSc2
reflektoru	reflektor	k1gInSc2
od	od	k7c2
zdroje	zdroj	k1gInSc2
světla	světlo	k1gNnSc2
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
efektivní	efektivní	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Softbox	Softbox	k1gInSc1
</s>
<s>
Striplight	Striplight	k1gMnSc1
</s>
<s>
Fotografický	fotografický	k2eAgInSc1d1
deštník	deštník	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Focal	Focal	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Photography	Photograph	k1gInPc1
<g/>
,	,	kIx,
Leslie	Leslie	k1gFnPc1
Stroebel	Stroebel	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
D.	D.	kA
Zakia	Zakia	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Focal	Focal	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
rd	rd	k?
edn	edn	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
str	str	kA
<g/>
.	.	kIx.
689	#num#	k4
<g/>
↑	↑	k?
Focal	Focal	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Photography	Photograph	k1gInPc1
<g/>
,	,	kIx,
Leslie	Leslie	k1gFnPc1
Stroebel	Stroebel	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
D.	D.	kA
Zakia	Zakia	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Focal	Focal	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
rd	rd	k?
edn	edn	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
str	str	kA
<g/>
.	.	kIx.
60	#num#	k4
<g/>
↑	↑	k?
Basic	Basic	kA
Photography	Photographa	k1gFnSc2
Course	Course	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Reflektor	reflektor	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
