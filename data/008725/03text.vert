<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Velvarský	Velvarský	k2eAgMnSc1d1	Velvarský
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1894	[number]	k4	1894
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
projektant	projektant	k1gMnSc1	projektant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
Velvarského	Velvarský	k2eAgMnSc4d1	Velvarský
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
domy	dům	k1gInPc1	dům
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hořice	Hořice	k1gFnPc1	Hořice
<g/>
,	,	kIx,	,
vojenská	vojenský	k2eAgFnSc1d1	vojenská
invalidovna	invalidovna	k1gFnSc1	invalidovna
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Milady	Milada	k1gFnSc2	Milada
Horákové	Horáková	k1gFnSc2	Horáková
123	[number]	k4	123
<g/>
/	/	kIx~	/
<g/>
298	[number]	k4	298
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Valech	val	k1gInPc6	val
32	[number]	k4	32
<g/>
/	/	kIx~	/
<g/>
45	[number]	k4	45
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Valech	val	k1gInPc6	val
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
46	[number]	k4	46
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Truhlářce	truhlářka	k1gFnSc6	truhlářka
1459	[number]	k4	1459
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Opletalova	Opletalův	k2eAgFnSc1d1	Opletalova
919	[number]	k4	919
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
–	–	k?	–
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1930	[number]	k4	1930
až	až	k9	až
1992	[number]	k4	1992
sídlo	sídlo	k1gNnSc1	sídlo
České	český	k2eAgFnSc2d1	Česká
tiskové	tiskový	k2eAgFnSc2d1	tisková
kanceláře	kancelář	k1gFnSc2	kancelář
(	(	kIx(	(
<g/>
ČTK	ČTK	kA	ČTK
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
