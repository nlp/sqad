<p>
<s>
Okres	okres	k1gInSc1	okres
Tamsweg	Tamsweg	k1gInSc1	Tamsweg
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
územně	územně	k6eAd1	územně
identicky	identicky	k6eAd1	identicky
s	s	k7c7	s
regionem	region	k1gInSc7	region
Lungau	Lungaus	k1gInSc2	Lungaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
okres	okres	k1gInSc1	okres
v	v	k7c6	v
rakouské	rakouský	k2eAgFnSc6d1	rakouská
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Salcbursko	Salcbursko	k1gNnSc1	Salcbursko
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
1	[number]	k4	1
0	[number]	k4	0
<g/>
19,69	[number]	k4	19,69
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
20	[number]	k4	20
902	[number]	k4	902
<g/>
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
okresu	okres	k1gInSc2	okres
je	být	k5eAaImIp3nS	být
městys	městys	k1gInSc1	městys
Tamsweg	Tamswega	k1gFnPc2	Tamswega
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
15	[number]	k4	15
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
3	[number]	k4	3
městysy	městys	k1gInPc4	městys
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnPc1	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Městysy	městys	k1gInPc1	městys
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mauterndorf	Mauterndorf	k1gMnSc1	Mauterndorf
</s>
</p>
<p>
<s>
Sankt	Sankt	k1gInSc1	Sankt
Michael	Michael	k1gMnSc1	Michael
im	im	k?	im
Lungau	Lungaa	k1gMnSc4	Lungaa
</s>
</p>
<p>
<s>
TamswegObce	TamswegObce	k1gMnSc1	TamswegObce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Göriach	Göriach	k1gMnSc1	Göriach
</s>
</p>
<p>
<s>
Lessach	Lessach	k1gMnSc1	Lessach
</s>
</p>
<p>
<s>
Mariapfarr	Mariapfarr	k1gMnSc1	Mariapfarr
</s>
</p>
<p>
<s>
Muhr	Muhr	k1gMnSc1	Muhr
</s>
</p>
<p>
<s>
Ramingstein	Ramingstein	k1gMnSc1	Ramingstein
</s>
</p>
<p>
<s>
Sankt	Sankt	k1gInSc1	Sankt
Andrä	Andrä	k1gFnSc2	Andrä
im	im	k?	im
Lungau	Lungaus	k1gInSc2	Lungaus
</s>
</p>
<p>
<s>
Sankt	Sankt	k1gInSc1	Sankt
Margarethen	Margarethen	k1gInSc1	Margarethen
im	im	k?	im
Lungau	Lungaus	k1gInSc2	Lungaus
</s>
</p>
<p>
<s>
Thomatal	Thomatal	k1gMnSc1	Thomatal
</s>
</p>
<p>
<s>
Tweng	Tweng	k1gMnSc1	Tweng
</s>
</p>
<p>
<s>
Unternberg	Unternberg	k1gMnSc1	Unternberg
</s>
</p>
<p>
<s>
Weißpriach	Weißpriach	k1gMnSc1	Weißpriach
</s>
</p>
<p>
<s>
Zederhaus	Zederhaus	k1gMnSc1	Zederhaus
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Tamsweg	Tamsweg	k1gMnSc1	Tamsweg
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Okres	okres	k1gInSc1	okres
Tamsweg	Tamsweg	k1gInSc4	Tamsweg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
