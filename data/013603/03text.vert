<s>
Keplerovy	Keplerův	k2eAgInPc4d1
zákony	zákon	k1gInPc4
</s>
<s>
Keplerovy	Keplerův	k2eAgInPc1d1
zákony	zákon	k1gInPc1
jsou	být	k5eAaImIp3nP
tři	tři	k4xCgInPc4
fyzikální	fyzikální	k2eAgInPc4d1
zákony	zákon	k1gInPc4
popisující	popisující	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
planet	planeta	k1gFnPc2
kolem	kolem	k7c2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platí	platit	k5eAaImIp3nS
však	však	k9
obecněji	obecně	k6eAd2
pro	pro	k7c4
pohyb	pohyb	k1gInSc4
libovolného	libovolný	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
v	v	k7c6
centrálním	centrální	k2eAgNnSc6d1
silovém	silový	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
působení	působení	k1gNnSc2
nějaké	nějaký	k3yIgFnSc2
dostředivé	dostředivý	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
přitažlivost	přitažlivost	k1gFnSc1
klesá	klesat	k5eAaImIp3nS
s	s	k7c7
druhou	druhý	k4xOgFnSc7
mocninou	mocnina	k1gFnSc7
vzdálenosti	vzdálenost	k1gFnSc2
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
gravitace	gravitace	k1gFnPc1
výrazně	výrazně	k6eAd1
hmotnějšího	hmotný	k2eAgNnSc2d2
tělesa	těleso	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
je	být	k5eAaImIp3nS
tedy	tedy	k9
použít	použít	k5eAaPmF
například	například	k6eAd1
i	i	k9
na	na	k7c4
pohyb	pohyb	k1gInSc4
Měsíce	měsíc	k1gInSc2
či	či	k8xC
umělé	umělý	k2eAgFnSc2d1
družice	družice	k1gFnSc2
kolem	kolem	k7c2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
s	s	k7c7
menší	malý	k2eAgFnSc7d2
přesností	přesnost	k1gFnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
vliv	vliv	k1gInSc1
Slunce	slunce	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
nezanedbatelný	zanedbatelný	k2eNgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Johannes	Johannes	k1gMnSc1
Kepler	Kepler	k1gMnSc1
při	při	k7c6
odvození	odvození	k1gNnSc6
těchto	tento	k3xDgInPc2
zákonů	zákon	k1gInPc2
využil	využít	k5eAaPmAgInS
systematická	systematický	k2eAgFnSc1d1
a	a	k8xC
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
nejpřesnější	přesný	k2eAgNnPc4d3
astronomická	astronomický	k2eAgNnPc4d1
měření	měření	k1gNnPc4
Tychona	Tychon	k1gMnSc2
Brahe	Brah	k1gMnSc2
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
byl	být	k5eAaImAgInS
Kepler	Kepler	k1gMnSc1
asistentem	asistent	k1gMnSc7
v	v	k7c6
letech	léto	k1gNnPc6
1600	#num#	k4
až	až	k9
1601	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
dva	dva	k4xCgInPc4
zákony	zákon	k1gInPc4
vydal	vydat	k5eAaPmAgInS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
Astronomia	Astronomia	k1gFnSc1
nova	nova	k1gFnSc1
(	(	kIx(
<g/>
1609	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
třetí	třetí	k4xOgMnSc1
vyšel	vyjít	k5eAaPmAgInS
roku	rok	k1gInSc2
1618	#num#	k4
v	v	k7c6
Harmonices	Harmonices	k1gMnSc1
mundi	mundit	k5eAaPmRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
(	(	kIx(
<g/>
1687	#num#	k4
<g/>
)	)	kIx)
Isaac	Isaac	k1gInSc1
Newton	Newton	k1gMnSc1
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Keplerovy	Keplerův	k2eAgInPc1d1
zákony	zákon	k1gInPc1
jsou	být	k5eAaImIp3nP
důsledkem	důsledek	k1gInSc7
jeho	jeho	k3xOp3gFnSc2
obecnější	obecní	k2eAgFnSc2d2
fyzikální	fyzikální	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
mechaniky	mechanika	k1gFnSc2
a	a	k8xC
gravitace	gravitace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Formulace	formulace	k1gFnSc1
zákonů	zákon	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerův	Keplerův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Keplerův	Keplerův	k2eAgInSc4d1
první	první	k4xOgInSc4
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Planety	planeta	k1gFnPc1
obíhají	obíhat	k5eAaImIp3nP
kolem	kolem	k7c2
Slunce	slunce	k1gNnSc2
po	po	k7c6
eliptických	eliptický	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
trajektoriích	trajektorie	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jejichž	jejichž	k3xOyRp3gInSc6
jednom	jeden	k4xCgInSc6
společném	společný	k2eAgNnSc6d1
ohnisku	ohnisko	k1gNnSc6
je	být	k5eAaImIp3nS
Slunce	slunce	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerova	Keplerův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
popisuje	popisovat	k5eAaImIp3nS
tvar	tvar	k1gInSc4
trajektorií	trajektorie	k1gFnPc2
planet	planeta	k1gFnPc2
pohybujících	pohybující	k2eAgFnPc2d1
se	se	k3xPyFc4
v	v	k7c6
gravitačním	gravitační	k2eAgNnSc6d1
poli	pole	k1gNnSc6
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
planety	planeta	k1gFnPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
po	po	k7c6
rovinných	rovinný	k2eAgFnPc6d1
křivkách	křivka	k1gFnPc6
(	(	kIx(
<g/>
elipsách	elipsa	k1gFnPc6
či	či	k8xC
kružnicích	kružnice	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kolem	kolem	k7c2
stálého	stálý	k2eAgInSc2d1
středu	střed	k1gInSc2
(	(	kIx(
<g/>
centra	centrum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vektor	vektor	k1gInSc1
zrychlení	zrychlení	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
i	i	k9
síla	síla	k1gFnSc1
způsobující	způsobující	k2eAgFnPc1d1
tento	tento	k3xDgInSc4
pohyb	pohyb	k1gInSc4
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
v	v	k7c6
rovině	rovina	k1gFnSc6
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Planety	planeta	k1gFnPc1
se	se	k3xPyFc4
periodicky	periodicky	k6eAd1
vzdalují	vzdalovat	k5eAaImIp3nP
a	a	k8xC
přibližují	přibližovat	k5eAaImIp3nP
ke	k	k7c3
Slunci	slunce	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Planety	planeta	k1gFnPc1
obíhají	obíhat	k5eAaImIp3nP
kolem	kolem	k7c2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
geocentrický	geocentrický	k2eAgInSc1d1
popis	popis	k1gInSc1
nebeské	nebeský	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
již	již	k6eAd1
není	být	k5eNaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Planety	planeta	k1gFnPc1
ale	ale	k9
nemají	mít	k5eNaImIp3nP
příliš	příliš	k6eAd1
výstřednou	výstředný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
prvním	první	k4xOgInSc6
přiblížení	přiblížení	k1gNnSc6
lze	lze	k6eAd1
uvažovat	uvažovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
po	po	k7c6
kružnici	kružnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
však	však	k9
platí	platit	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
komety	kometa	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
po	po	k7c6
značně	značně	k6eAd1
výstředných	výstředný	k2eAgFnPc6d1
drahách	draha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
nějaké	nějaký	k3yIgNnSc1
těleso	těleso	k1gNnSc1
(	(	kIx(
<g/>
dlouhodobě	dlouhodobě	k6eAd1
<g/>
)	)	kIx)
pohybovalo	pohybovat	k5eAaImAgNnS
okolo	okolo	k7c2
Slunce	slunce	k1gNnSc2
přesně	přesně	k6eAd1
po	po	k7c6
kružnici	kružnice	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nulová	nulový	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
kružnice	kružnice	k1gFnSc1
je	být	k5eAaImIp3nS
ideální	ideální	k2eAgInSc4d1
případ	případ	k1gInSc4
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yIgInSc3,k3yQgInSc3,k3yRgInSc3
se	se	k3xPyFc4
lze	lze	k6eAd1
v	v	k7c6
praxi	praxe	k1gFnSc6
pouze	pouze	k6eAd1
přiblížit	přiblížit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
nelze	lze	k6eNd1
ho	on	k3xPp3gMnSc4
dosáhnout	dosáhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Roviny	rovina	k1gFnPc1
drah	draha	k1gFnPc2
všech	všecek	k3xTgFnPc2
planet	planeta	k1gFnPc2
procházejí	procházet	k5eAaImIp3nP
středem	středem	k7c2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
přibližně	přibližně	k6eAd1
totožné	totožný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slunce	slunce	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
ohnisku	ohnisko	k1gNnSc6
dráhy	dráha	k1gFnSc2
každé	každý	k3xTgFnSc2
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
elipsy	elipsa	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
je	být	k5eAaImIp3nS
planeta	planeta	k1gFnSc1
nejblíže	blízce	k6eAd3
Slunci	slunce	k1gNnSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
přísluní	přísluní	k1gNnSc1
(	(	kIx(
<g/>
perihélium	perihélium	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
hlavní	hlavní	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
je	být	k5eAaImIp3nS
planeta	planeta	k1gFnSc1
nejdále	daleko	k6eAd3
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
odsluní	odsluní	k1gNnSc1
(	(	kIx(
<g/>
afélium	afélium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerův	Keplerův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Plocha	plocha	k1gFnSc1
opsaná	opsaný	k2eAgFnSc1d1
průvodičem	průvodič	k1gInSc7
planety	planeta	k1gFnSc2
za	za	k7c4
stejný	stejný	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Keplerův	Keplerův	k2eAgInSc4d1
druhý	druhý	k4xOgInSc4
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Obsahy	obsah	k1gInPc1
ploch	plocha	k1gFnPc2
opsaných	opsaný	k2eAgFnPc2d1
průvodičem	průvodič	k1gInSc7
planety	planeta	k1gFnSc2
(	(	kIx(
<g/>
spojnice	spojnice	k1gFnSc1
planety	planeta	k1gFnSc2
a	a	k8xC
Slunce	slunce	k1gNnSc2
<g/>
)	)	kIx)
za	za	k7c4
stejný	stejný	k2eAgInSc4d1
čas	čas	k1gInSc4
jsou	být	k5eAaImIp3nP
stejně	stejně	k6eAd1
velké	velký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Průvodič	průvodič	k1gInSc1
planety	planeta	k1gFnSc2
je	být	k5eAaImIp3nS
spojnice	spojnice	k1gFnSc1
hmotného	hmotný	k2eAgInSc2d1
středu	střed	k1gInSc2
planety	planeta	k1gFnSc2
s	s	k7c7
hmotným	hmotný	k2eAgInSc7d1
středem	střed	k1gInSc7
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
i	i	k8xC
směr	směr	k1gInSc1
průvodiče	průvodič	k1gInSc2
se	se	k3xPyFc4
při	při	k7c6
pohybu	pohyb	k1gInSc6
planety	planeta	k1gFnSc2
kolem	kolem	k7c2
Slunce	slunce	k1gNnSc2
neustále	neustále	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průvodič	průvodič	k1gInSc4
však	však	k9
vždy	vždy	k6eAd1
za	za	k7c4
stejnou	stejný	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
opíše	opsat	k5eAaPmIp3nS
plochu	plocha	k1gFnSc4
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
důvodem	důvod	k1gInSc7
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
někdy	někdy	k6eAd1
nazývá	nazývat	k5eAaImIp3nS
zákon	zákon	k1gInSc1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerova	Keplerův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
</s>
<s>
Planety	planeta	k1gFnPc1
se	se	k3xPyFc4
v	v	k7c6
přísluní	přísluní	k1gNnSc6
pohybují	pohybovat	k5eAaImIp3nP
nejrychleji	rychle	k6eAd3
<g/>
,	,	kIx,
v	v	k7c6
odsluní	odsluní	k1gNnSc6
zase	zase	k9
nejpomaleji	pomale	k6eAd3
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
výpočtech	výpočet	k1gInPc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
plocha	plocha	k1gFnSc1
opsaná	opsaný	k2eAgFnSc1d1
průvodičem	průvodič	k1gInSc7
za	za	k7c7
infinitezimálně	infinitezimálně	k6eAd1
(	(	kIx(
<g/>
nekonečně	konečně	k6eNd1
<g/>
)	)	kIx)
krátký	krátký	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
zanedbat	zanedbat	k5eAaPmF
zakřivení	zakřivení	k1gNnSc4
trajektorie	trajektorie	k1gFnSc2
planety	planeta	k1gFnSc2
a	a	k8xC
celý	celý	k2eAgInSc4d1
výpočet	výpočet	k1gInSc4
se	se	k3xPyFc4
redukuje	redukovat	k5eAaBmIp3nS
na	na	k7c4
vyjádření	vyjádření	k1gNnSc4
obsahu	obsah	k1gInSc2
trojúhelníka	trojúhelník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
Keplerův	Keplerův	k2eAgInSc4d1
zákon	zákon	k1gInSc4
je	být	k5eAaImIp3nS
jiné	jiný	k2eAgNnSc4d1
vyjádření	vyjádření	k1gNnSc4
zákona	zákon	k1gInSc2
zachování	zachování	k1gNnSc2
momentu	moment	k1gInSc2
hybnosti	hybnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plyne	plynout	k5eAaImIp3nS
z	z	k7c2
něj	on	k3xPp3gMnSc2
(	(	kIx(
<g/>
netriviálně	triviálně	k6eNd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
oběžná	oběžný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
planet	planeta	k1gFnPc2
se	se	k3xPyFc4
zmenšuje	zmenšovat	k5eAaImIp3nS
se	s	k7c7
vzrůstající	vzrůstající	k2eAgFnSc7d1
vzdáleností	vzdálenost	k1gFnSc7
od	od	k7c2
Slunce	slunce	k1gNnSc2
(	(	kIx(
<g/>
těles	těleso	k1gNnPc2
od	od	k7c2
centrálního	centrální	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
zřejmé	zřejmý	k2eAgNnSc1d1
ze	z	k7c2
zákona	zákon	k1gInSc2
zachování	zachování	k1gNnSc2
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Plošná	plošný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
</s>
<s>
Sledujeme	sledovat	k5eAaImIp1nP
<g/>
-li	-li	k?
pohyb	pohyb	k1gInSc4
tělesa	těleso	k1gNnSc2
s	s	k7c7
polohovým	polohový	k2eAgInSc7d1
vektorem	vektor	k1gInSc7
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
v	v	k7c6
gravitačním	gravitační	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
za	za	k7c4
čas	čas	k1gInSc4
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g/>
}	}	kIx)
</s>
<s>
dojde	dojít	k5eAaPmIp3nS
ke	k	k7c3
změně	změna	k1gFnSc3
průvodiče	průvodič	k1gInSc2
na	na	k7c4
</s>
<s>
r	r	kA
</s>
<s>
+	+	kIx~
</s>
<s>
d	d	k?
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
+	+	kIx~
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
elementární	elementární	k2eAgInSc4d1
přírůstek	přírůstek	k1gInSc4
</s>
<s>
d	d	k?
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
směru	směr	k1gInSc2
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
elementární	elementární	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
opsané	opsaný	k2eAgFnSc2d1
tímto	tento	k3xDgInSc7
průvodičem	průvodič	k1gInSc7
lze	lze	k6eAd1
vyjádřit	vyjádřit	k5eAaPmF
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
d	d	k?
</s>
<s>
S	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
d	d	k?
</s>
<s>
r	r	kA
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
S	s	k7c7
<g/>
}	}	kIx)
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gInSc1
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
)	)	kIx)
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Pro	pro	k7c4
plošnou	plošný	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
pak	pak	k6eAd1
s	s	k7c7
pomocí	pomoc	k1gFnSc7
tohoto	tento	k3xDgInSc2
vztahu	vztah	k1gInSc2
získáme	získat	k5eAaPmIp1nP
výraz	výraz	k1gInSc4
</s>
<s>
w	w	k?
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
S	s	k7c7
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
d	d	k?
</s>
<s>
r	r	kA
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
v	v	k7c6
</s>
<s>
)	)	kIx)
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
w	w	k?
<g/>
}	}	kIx)
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
S	s	k7c7
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g/>
}}	}}	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gMnSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
)	)	kIx)
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Vektor	vektor	k1gInSc1
plošné	plošný	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
</s>
<s>
w	w	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
w	w	k?
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
kolmý	kolmý	k2eAgMnSc1d1
k	k	k7c3
rovině	rovina	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
leží	ležet	k5eAaImIp3nS
trajektorie	trajektorie	k1gFnSc1
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
Keplerův	Keplerův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
plošnou	plošný	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
platí	platit	k5eAaImIp3nS
</s>
<s>
w	w	k?
</s>
<s>
=	=	kIx~
</s>
<s>
konst	konst	k1gFnSc1
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
w	w	k?
<g/>
}	}	kIx)
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mbox	mbox	k1gInSc1
<g/>
{	{	kIx(
<g/>
konst	konst	k1gFnSc1
<g/>
}}	}}	k?
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Ze	z	k7c2
znalosti	znalost	k1gFnSc2
vztahu	vztah	k1gInSc2
pro	pro	k7c4
moment	moment	k1gInSc4
hybnosti	hybnost	k1gFnSc2
</s>
<s>
L	L	kA
</s>
<s>
=	=	kIx~
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
p	p	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
L	L	kA
<g/>
}	}	kIx)
=	=	kIx~
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gMnSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
p	p	k?
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
p	p	k?
</s>
<s>
=	=	kIx~
</s>
<s>
m	m	kA
</s>
<s>
v	v	k7c6
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
p	p	k?
<g/>
}	}	kIx)
=	=	kIx~
<g/>
m	m	kA
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
hybnost	hybnost	k1gFnSc4
planety	planeta	k1gFnSc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
psát	psát	k5eAaImF
</s>
<s>
L	L	kA
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
w	w	k?
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
L	L	kA
<g/>
}	}	kIx)
=	=	kIx~
<g/>
2	#num#	k4
<g/>
m	m	kA
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
w	w	k?
<g/>
}	}	kIx)
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
tedy	tedy	k8xC
konstantní	konstantní	k2eAgFnSc1d1
plošná	plošný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
konstantní	konstantní	k2eAgFnSc1d1
také	také	k9
moment	moment	k1gInSc4
hybnosti	hybnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obráceně	obráceně	k6eAd1
lze	lze	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
ze	z	k7c2
zákona	zákon	k1gInSc2
zachování	zachování	k1gNnSc2
momentu	moment	k1gInSc2
hybnosti	hybnost	k1gFnSc2
vyplývá	vyplývat	k5eAaImIp3nS
konstantní	konstantní	k2eAgFnSc1d1
plošná	plošný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
pohybu	pohyb	k1gInSc2
planety	planeta	k1gFnSc2
v	v	k7c6
radiálním	radiální	k2eAgNnSc6d1
gravitačním	gravitační	k2eAgNnSc6d1
poli	pole	k1gNnSc6
(	(	kIx(
<g/>
a	a	k8xC
tedy	tedy	k9
také	také	k9
druhý	druhý	k4xOgInSc1
Keplerův	Keplerův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Plošné	plošný	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
</s>
<s>
Derivací	derivace	k1gFnSc7
plošné	plošný	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
podle	podle	k7c2
času	čas	k1gInSc2
dostaneme	dostat	k5eAaPmIp1nP
plošné	plošný	k2eAgNnSc4d1
zrychlení	zrychlení	k1gNnSc4
</s>
<s>
q	q	k?
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
w	w	k?
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
d	d	k?
</s>
<s>
v	v	k7c6
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
d	d	k?
</s>
<s>
r	r	kA
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
×	×	k?
</s>
<s>
v	v	k7c6
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
d	d	k?
</s>
<s>
v	v	k7c6
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
a	a	k8xC
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
q	q	k?
<g/>
}	}	kIx)
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
w	w	k?
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
times	times	k1gInSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
times	times	k1gInSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gInSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
využito	využít	k5eAaPmNgNnS
toho	ten	k3xDgInSc2
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
d	d	k?
</s>
<s>
r	r	kA
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
×	×	k?
</s>
<s>
v	v	k7c6
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
times	times	k1gMnSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
\	\	kIx~
<g/>
right	right	k5eAaPmF
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Při	při	k7c6
planetárním	planetární	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
je	být	k5eAaImIp3nS
plošná	plošný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
stálá	stálý	k2eAgFnSc1d1
a	a	k8xC
plošné	plošný	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
tedy	tedy	k9
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
nulové	nulový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gInSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
Vektorový	vektorový	k2eAgInSc1d1
součin	součin	k1gInSc1
dvou	dva	k4xCgInPc2
vektorů	vektor	k1gInPc2
je	být	k5eAaImIp3nS
nulový	nulový	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
nulový	nulový	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
pokud	pokud	k8xS
leží	ležet	k5eAaImIp3nS
v	v	k7c6
jedné	jeden	k4xCgFnSc6
přímce	přímka	k1gFnSc6
(	(	kIx(
<g/>
tzn.	tzn.	kA
mají	mít	k5eAaImIp3nP
shodný	shodný	k2eAgInSc1d1
nebo	nebo	k8xC
přesně	přesně	k6eAd1
opačný	opačný	k2eAgInSc1d1
směr	směr	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
ani	ani	k8xC
</s>
<s>
a	a	k8xC
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
nulové	nulový	k2eAgNnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
pohyb	pohyb	k1gInSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
určité	určitý	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
od	od	k7c2
středu	střed	k1gInSc2
(	(	kIx(
<g/>
tedy	tedy	k9
</s>
<s>
r	r	kA
</s>
<s>
≠	≠	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
neq	neq	k?
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
)	)	kIx)
a	a	k8xC
při	při	k7c6
každém	každý	k3xTgInSc6
křivočarém	křivočarý	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
nějaké	nějaký	k3yIgNnSc1
zrychlení	zrychlení	k1gNnSc1
(	(	kIx(
<g/>
tedy	tedy	k9
</s>
<s>
a	a	k8xC
</s>
<s>
≠	≠	k?
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
\	\	kIx~
<g/>
neq	neq	k?
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znamená	znamenat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tedy	tedy	k9
<g/>
,	,	kIx,
že	že	k8xS
zrychlení	zrychlení	k1gNnSc1
</s>
<s>
a	a	k8xC
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
(	(	kIx(
<g/>
tedy	tedy	k9
i	i	k9
odpovídající	odpovídající	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
)	)	kIx)
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
směru	směr	k1gInSc6
průvodiče	průvodič	k1gInSc2
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
</s>
<s>
Trajektorie	trajektorie	k1gFnSc1
dráhy	dráha	k1gFnSc2
má	mít	k5eAaImIp3nS
vždy	vždy	k6eAd1
takový	takový	k3xDgInSc4
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
vzhledem	vzhledem	k7c3
k	k	k7c3
tečnému	tečný	k2eAgInSc3d1
vektoru	vektor	k1gInSc3
se	se	k3xPyFc4
vždy	vždy	k6eAd1
zakřivuje	zakřivovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
k	k	k7c3
centru	centrum	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zrychlení	zrychlení	k1gNnSc1
směřuje	směřovat	k5eAaImIp3nS
dovnitř	dovnitř	k6eAd1
uzavřené	uzavřený	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
(	(	kIx(
<g/>
elipsy	elipsa	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
opačném	opačný	k2eAgInSc6d1
případě	případ	k1gInSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
dráha	dráha	k1gFnSc1
zakřivovala	zakřivovat	k5eAaImAgFnS
ven	ven	k6eAd1
od	od	k7c2
tečného	tečný	k2eAgInSc2d1
vektoru	vektor	k1gInSc2
a	a	k8xC
dráha	dráha	k1gFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
neuzavřela	uzavřít	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vektor	vektor	k1gInSc1
zrychlení	zrychlení	k1gNnSc2
směřuje	směřovat	k5eAaImIp3nS
vždy	vždy	k6eAd1
do	do	k7c2
centra	centrum	k1gNnSc2
silového	silový	k2eAgNnSc2d1
působení	působení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
silové	silový	k2eAgNnSc1d1
působení	působení	k1gNnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
centrální	centrální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
pohyb	pohyb	k1gInSc1
způsobený	způsobený	k2eAgInSc1d1
těmito	tento	k3xDgFnPc7
silami	síla	k1gFnPc7
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
centrální	centrální	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerův	Keplerův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Poměr	poměr	k1gInSc1
druhých	druhý	k4xOgFnPc2
mocnin	mocnina	k1gFnPc2
oběžných	oběžný	k2eAgFnPc2d1
dob	doba	k1gFnPc2
dvou	dva	k4xCgFnPc2
planet	planeta	k1gFnPc2
je	být	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
poměr	poměr	k1gInSc1
třetích	třetí	k4xOgFnPc2
mocnin	mocnina	k1gFnPc2
délek	délka	k1gFnPc2
jejich	jejich	k3xOp3gFnPc2
hlavních	hlavní	k2eAgFnPc2d1
poloos	poloosa	k1gFnPc2
(	(	kIx(
<g/>
středních	střední	k2eAgFnPc2d1
vzdáleností	vzdálenost	k1gFnPc2
těchto	tento	k3xDgFnPc2
planet	planeta	k1gFnPc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
označíme	označit	k5eAaPmIp1nP
</s>
<s>
T	T	kA
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
T_	T_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
T	T	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
T_	T_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
oběžné	oběžný	k2eAgFnPc4d1
doby	doba	k1gFnPc4
dvou	dva	k4xCgFnPc2
planet	planeta	k1gFnPc2
a	a	k8xC
</s>
<s>
a	a	k8xC
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a_	a_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a_	a_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
délky	délka	k1gFnPc1
jejich	jejich	k3xOp3gFnPc2
hlavních	hlavní	k2eAgFnPc2d1
poloos	poloosa	k1gFnPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
lze	lze	k6eAd1
tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
vyjádřit	vyjádřit	k5eAaPmF
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
T	T	kA
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
T	T	kA
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
T_	T_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
T_	T_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
1	#num#	k4
</s>
<s>
T	T	kA
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
T	T	kA
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a_	a_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
a_	a_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k5eAaPmF
<g/>
[	[	kIx(
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
]	]	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
T_	T_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
T_	T_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}}	}}}}}	k?
</s>
<s>
Tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
platí	platit	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
tvaru	tvar	k1gInSc6
jen	jen	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
hmotnosti	hmotnost	k1gFnPc1
planet	planeta	k1gFnPc2
zanedbatelně	zanedbatelně	k6eAd1
malé	malý	k2eAgFnPc4d1
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
hmotností	hmotnost	k1gFnSc7
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
u	u	k7c2
planet	planeta	k1gFnPc2
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
splněno	splněn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerova	Keplerův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
</s>
<s>
Planety	planeta	k1gFnPc1
blízko	blízko	k7c2
Slunce	slunce	k1gNnSc2
jej	on	k3xPp3gMnSc4
oběhnou	oběhnout	k5eAaPmIp3nP
za	za	k7c4
kratší	krátký	k2eAgInSc4d2
čas	čas	k1gInSc4
než	než	k8xS
planety	planeta	k1gFnPc1
vzdálené	vzdálený	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
však	však	k9
roste	růst	k5eAaImIp3nS
se	s	k7c7
vzdáleností	vzdálenost	k1gFnSc7
od	od	k7c2
Slunce	slunce	k1gNnSc2
rychleji	rychle	k6eAd2
než	než	k8xS
tato	tento	k3xDgFnSc1
vzdálenost	vzdálenost	k1gFnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
průměrná	průměrný	k2eAgFnSc1d1
úhlová	úhlový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
planet	planeta	k1gFnPc2
klesá	klesat	k5eAaImIp3nS
se	s	k7c7
vzdáleností	vzdálenost	k1gFnSc7
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Saturn	Saturn	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
Slunce	slunce	k1gNnSc2
vzdálen	vzdálit	k5eAaPmNgInS
přibližně	přibližně	k6eAd1
10	#num#	k4
<g/>
x	x	k?
více	hodně	k6eAd2
než	než	k8xS
Země	země	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnSc1
doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Saturnův	Saturnův	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
již	již	k6eAd1
skoro	skoro	k6eAd1
30	#num#	k4
<g/>
x	x	k?
delší	dlouhý	k2eAgFnPc4d2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
tabulku	tabulka	k1gFnSc4
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odvození	odvození	k1gNnSc1
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP
<g/>
,	,	kIx,
že	že	k8xS
soustava	soustava	k1gFnSc1
spojená	spojený	k2eAgFnSc1d1
se	s	k7c7
Sluncem	slunce	k1gNnSc7
je	být	k5eAaImIp3nS
inerciální	inerciální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Excentricity	excentricita	k1gFnSc2
drah	draha	k1gFnPc2
planet	planeta	k1gFnPc2
jsou	být	k5eAaImIp3nP
malé	malý	k2eAgFnPc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
je	on	k3xPp3gInPc4
můžeme	moct	k5eAaImIp1nP
považovat	považovat	k5eAaImF
za	za	k7c4
přibližně	přibližně	k6eAd1
kruhové	kruhový	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bližší	blízký	k2eAgFnPc1d2
planety	planeta	k1gFnPc1
mají	mít	k5eAaImIp3nP
větší	veliký	k2eAgFnSc4d2
oběžnou	oběžný	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
na	na	k7c4
ně	on	k3xPp3gNnSc4
Slunce	slunce	k1gNnSc4
působí	působit	k5eAaImIp3nS
větší	veliký	k2eAgFnSc7d2
silou	síla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběžná	oběžný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
jde	jít	k5eAaImIp3nS
vyjádřit	vyjádřit	k5eAaPmF
z	z	k7c2
gravitační	gravitační	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
silou	síla	k1gFnSc7
dostředivou	dostředivý	k2eAgFnSc7d1
<g/>
:	:	kIx,
</s>
<s>
F	F	kA
</s>
<s>
g	g	kA
</s>
<s>
=	=	kIx~
</s>
<s>
F	F	kA
</s>
<s>
d	d	k?
</s>
<s>
⟹	⟹	k?
</s>
<s>
κ	κ	k?
</s>
<s>
m	m	kA
</s>
<s>
S	s	k7c7
</s>
<s>
⋅	⋅	k?
</s>
<s>
m	m	kA
</s>
<s>
p	p	k?
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
m	m	kA
</s>
<s>
p	p	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
v	v	k7c6
</s>
<s>
2	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
⟹	⟹	k?
</s>
<s>
v	v	k7c6
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
κ	κ	k?
</s>
<s>
m	m	kA
</s>
<s>
S	s	k7c7
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F_	F_	k1gMnPc6
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
rm	rm	k?
{	{	kIx(
<g/>
g	g	kA
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
F_	F_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
rm	rm	k?
{	{	kIx(
<g/>
d	d	k?
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
Longrightarrow	Longrightarrow	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gInSc1
\	\	kIx~
<g/>
kappa	kappa	k1gNnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
m_	m_	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
rm	rm	k?
{	{	kIx(
<g/>
S	s	k7c7
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
m_	m_	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
rm	rm	k?
{	{	kIx(
<g/>
p	p	k?
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
m_	m_	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
rm	rm	k?
{	{	kIx(
<g/>
p	p	k?
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k2eAgMnSc1d1
v	v	k7c6
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
}}	}}	k?
<g/>
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
\	\	kIx~
<g/>
Longrightarrow	Longrightarrow	k1gMnSc1
\	\	kIx~
<g/>
quad	quad	k1gMnSc1
v	v	k7c6
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
kappa	kappa	k1gNnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
m_	m_	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
rm	rm	k?
{	{	kIx(
<g/>
S	s	k7c7
<g/>
}}}	}}}	k?
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
}}}	}}}	k?
</s>
<s>
</s>
<s>
Vidíme	vidět	k5eAaImIp1nP
tedy	tedy	k9
<g/>
,	,	kIx,
že	že	k8xS
čím	čí	k3xOyQgNnSc7,k3xOyRgNnSc7
je	být	k5eAaImIp3nS
planeta	planeta	k1gFnSc1
blíže	blízce	k6eAd2
Slunci	slunce	k1gNnSc3
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
rychleji	rychle	k6eAd2
obíhá	obíhat	k5eAaImIp3nS
kolem	kolem	k7c2
něho	on	k3xPp3gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
</s>
<s>
v	v	k7c6
</s>
<s>
⋅	⋅	k?
</s>
<s>
T	T	kA
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v	v	k7c4
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc4
T	T	kA
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
r	r	kA
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
dostaneme	dostat	k5eAaPmIp1nP
dosazením	dosazení	k1gNnSc7
</s>
<s>
T	T	kA
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
2	#num#	k4
</s>
<s>
κ	κ	k?
</s>
<s>
m	m	kA
</s>
<s>
S	s	k7c7
</s>
<s>
)	)	kIx)
</s>
<s>
r	r	kA
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
T	T	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
left	left	k5eAaPmF
<g/>
(	(	kIx(
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
kappa	kappa	k1gNnSc1
m_	m_	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
rm	rm	k?
{	{	kIx(
<g/>
S	s	k7c7
<g/>
}}}}	}}}}	k?
<g/>
\	\	kIx~
<g/>
right	right	k1gMnSc1
<g/>
)	)	kIx)
<g/>
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
obecnější	obecní	k2eAgMnSc1d2
<g/>
)	)	kIx)
vyjádření	vyjádření	k1gNnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerova	Keplerův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
vztah	vztah	k1gInSc1
lze	lze	k6eAd1
elementárně	elementárně	k6eAd1
uhodnout	uhodnout	k5eAaPmF
i	i	k9
rozměrovou	rozměrový	k2eAgFnSc7d1
úvahou	úvaha	k1gFnSc7
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
bezrozměrnou	bezrozměrný	k2eAgFnSc4d1
konstantu	konstanta	k1gFnSc4
</s>
<s>
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
4	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
pro	pro	k7c4
původní	původní	k2eAgFnSc4d1
formulaci	formulace	k1gFnSc4
nevadí	vadit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Tabulka	tabulka	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerova	Keplerův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
</s>
<s>
PlanetaČas	PlanetaČas	k1gInSc1
oběhuStřední	oběhuStřednit	k5eAaPmIp3nS
vzdálenost	vzdálenost	k1gFnSc4
od	od	k7c2
SlunceStř	SlunceStř	k1gFnSc1
<g/>
.	.	kIx.
vz.	vz.	k?
od	od	k7c2
Slunce	slunce	k1gNnSc2
podle	podle	k7c2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Keplerova	Keplerův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
</s>
<s>
Merkur	Merkur	k1gInSc1
<g/>
0,240	0,240	k4
847	#num#	k4
let	léto	k1gNnPc2
<g/>
0,387	0,387	k4
098-9	098-9	k4
AU	au	k0
<g/>
0,387	0,387	k4
104	#num#	k4
AU	au	k0
</s>
<s>
Venuše	Venuše	k1gFnSc1
<g/>
0,615	0,615	k4
198	#num#	k4
let	léto	k1gNnPc2
<g/>
0,723	0,723	k4
332	#num#	k4
AU	au	k0
<g/>
0,723	0,723	k4
341	#num#	k4
AU	au	k0
</s>
<s>
Země	země	k1gFnSc1
<g/>
1	#num#	k4
rok	rok	k1gInSc1
<g/>
1	#num#	k4
AU1	AU1	k1gFnPc2
AU	au	k0
</s>
<s>
Mars	Mars	k1gInSc1
<g/>
1,880	1,880	k4
834	#num#	k4
let	léto	k1gNnPc2
<g/>
1,523	1,523	k4
662	#num#	k4
AU	au	k0
<g/>
1,523	1,523	k4
703	#num#	k4
AU	au	k0
</s>
<s>
Jupiter	Jupiter	k1gMnSc1
<g/>
11,869	11,869	k4
807	#num#	k4
let	léto	k1gNnPc2
<g/>
5,203	5,203	k4
363	#num#	k4
AU	au	k0
<g/>
5,203	5,203	k4
503	#num#	k4
AU	au	k0
</s>
<s>
Saturn	Saturn	k1gInSc1
<g/>
29,453	29,453	k4
712	#num#	k4
let	léto	k1gNnPc2
<g/>
9,537	9,537	k4
070	#num#	k4
AU	au	k0
<g/>
9,537	9,537	k4
327	#num#	k4
AU	au	k0
</s>
<s>
Uran	Uran	k1gInSc1
<g/>
84,076	84,076	k4
157	#num#	k4
let	léto	k1gNnPc2
<g/>
19,191	19,191	k4
264	#num#	k4
AU	au	k0
<g/>
19,191	19,191	k4
779	#num#	k4
AU	au	k0
</s>
<s>
Neptun	Neptun	k1gInSc1
<g/>
164,794	164,794	k4
790	#num#	k4
let	léto	k1gNnPc2
<g/>
30,058	30,058	k4
963	#num#	k4
AU	au	k0
<g/>
30,058	30,058	k4
155	#num#	k4
AU	au	k0
</s>
<s>
Odvození	odvození	k1gNnSc1
Newtonova	Newtonův	k2eAgInSc2d1
gravitačního	gravitační	k2eAgInSc2d1
zákona	zákon	k1gInSc2
z	z	k7c2
Keplerových	Keplerův	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
</s>
<s>
Při	při	k7c6
planetárním	planetární	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
je	být	k5eAaImIp3nS
plošná	plošný	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
stálá	stálý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
plyne	plynout	k5eAaImIp3nS
z	z	k7c2
druhého	druhý	k4xOgInSc2
Keplerova	Keplerův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
konstantnosti	konstantnost	k1gFnSc2
plošné	plošný	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
plošné	plošný	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
je	být	k5eAaImIp3nS
nulové	nulový	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plošné	plošný	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
lze	lze	k6eAd1
zapsat	zapsat	k5eAaPmF
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
q	q	k?
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
w	w	k?
</s>
<s>
d	d	k?
</s>
<s>
t	t	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
a	a	k8xC
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc4
{	{	kIx(
<g/>
q	q	k?
<g/>
}	}	kIx)
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
w	w	k?
<g/>
}	}	kIx)
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
t	t	k?
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gInSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
Má	mít	k5eAaImIp3nS
<g/>
-li	-li	k?
tato	tento	k3xDgNnPc4
hodnota	hodnota	k1gFnSc1
být	být	k5eAaImF
nulová	nulový	k2eAgFnSc1d1
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
nulový	nulový	k2eAgInSc1d1
vektorový	vektorový	k2eAgInSc1d1
součin	součin	k1gInSc1
</s>
<s>
r	r	kA
</s>
<s>
×	×	k?
</s>
<s>
a	a	k8xC
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathbf	mathbf	k1gMnSc1
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
\	\	kIx~
<g/>
times	times	k1gInSc1
\	\	kIx~
<g/>
mathbf	mathbf	k1gInSc1
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
Toho	ten	k3xDgMnSc4
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
z	z	k7c2
vektorů	vektor	k1gInPc2
nulový	nulový	k2eAgMnSc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
pokud	pokud	k8xS
mají	mít	k5eAaImIp3nP
oba	dva	k4xCgInPc1
vektory	vektor	k1gInPc1
stejný	stejný	k2eAgInSc4d1
nebo	nebo	k8xC
opačný	opačný	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Poněvadž	poněvadž	k8xS
při	při	k7c6
křivočarém	křivočarý	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
je	být	k5eAaImIp3nS
zrychlení	zrychlení	k1gNnSc1
nenulové	nulový	k2eNgNnSc1d1
a	a	k8xC
polohový	polohový	k2eAgInSc1d1
vektor	vektor	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
nenulový	nulový	k2eNgInSc1d1
<g/>
,	,	kIx,
přichází	přicházet	k5eAaImIp3nS
do	do	k7c2
úvahy	úvaha	k1gFnSc2
pouze	pouze	k6eAd1
druhá	druhý	k4xOgFnSc1
možnost	možnost	k1gFnSc1
<g/>
,	,	kIx,
tzn.	tzn.	kA
zrychlení	zrychlení	k1gNnSc1
i	i	k8xC
průvodič	průvodič	k1gInSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
jedné	jeden	k4xCgFnSc6
přímce	přímka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znamená	znamenat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tedy	tedy	k9
<g/>
,	,	kIx,
že	že	k8xS
pole	pole	k1gNnSc1
bodového	bodový	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
je	být	k5eAaImIp3nS
centrálním	centrální	k2eAgNnSc7d1
polem	pole	k1gNnSc7
a	a	k8xC
tedy	tedy	k9
<g/>
,	,	kIx,
že	že	k8xS
hledaná	hledaný	k2eAgFnSc1d1
gravitační	gravitační	k2eAgFnSc1d1
síla	síla	k1gFnSc1
je	být	k5eAaImIp3nS
funkcí	funkce	k1gFnSc7
vzdálenosti	vzdálenost	k1gFnSc2
od	od	k7c2
tohoto	tento	k3xDgNnSc2
centra	centrum	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nezávisí	záviset	k5eNaImIp3nS
např.	např.	kA
na	na	k7c6
zeměpisné	zeměpisný	k2eAgFnSc6d1
šířce	šířka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
odvození	odvození	k1gNnSc4
velikosti	velikost	k1gFnSc2
radiálního	radiální	k2eAgNnSc2d1
zrychlení	zrychlení	k1gNnSc2
můžeme	moct	k5eAaImIp1nP
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
těleso	těleso	k1gNnSc1
se	se	k3xPyFc4
kolem	kolem	k7c2
centra	centrum	k1gNnSc2
sil	síla	k1gFnPc2
pohybuje	pohybovat	k5eAaImIp3nS
po	po	k7c6
kružnici	kružnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rovnoměrném	rovnoměrný	k2eAgInSc6d1
kruhovém	kruhový	k2eAgInSc6d1
pohybu	pohyb	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
pozorujeme	pozorovat	k5eAaImIp1nP
v	v	k7c6
důsledku	důsledek	k1gInSc6
konstantnosti	konstantnost	k1gFnSc2
plošné	plošný	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
centrum	centrum	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
středu	střed	k1gInSc6
křivosti	křivost	k1gFnSc2
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radiální	radiální	k2eAgInSc4d1
zrychlení	zrychlení	k1gNnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
totožné	totožný	k2eAgInPc1d1
s	s	k7c7
dostředivým	dostředivý	k2eAgNnSc7d1
zrychlením	zrychlení	k1gNnSc7
a	a	k8xC
má	mít	k5eAaImIp3nS
velikost	velikost	k1gFnSc1
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
v	v	k7c6
</s>
<s>
2	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
=	=	kIx~
</s>
<s>
r	r	kA
</s>
<s>
ω	ω	k?
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
2	#num#	k4
</s>
<s>
T	T	kA
</s>
<s>
2	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
v	v	k7c6
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
r	r	kA
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
T	T	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
r	r	kA
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
T	T	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
T	T	kA
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
třetího	třetí	k4xOgInSc2
Keplerova	Keplerův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
platí	platit	k5eAaImIp3nS
</s>
<s>
T	T	kA
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
C	C	kA
</s>
<s>
r	r	kA
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
T	T	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
Cr	cr	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
C	C	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
C	C	kA
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
konstanta	konstanta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrychlení	zrychlení	k1gNnSc1
lze	lze	k6eAd1
pak	pak	k6eAd1
zapsat	zapsat	k5eAaPmF
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
2	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
C	C	kA
</s>
<s>
r	r	kA
</s>
<s>
3	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
1	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
4	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
r	r	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Cr	cr	k0
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}}}	}}}	k?
<g/>
=	=	kIx~
<g/>
k	k	k7c3
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
k	k	k7c3
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
k	k	k7c3
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
konstanta	konstanta	k1gFnSc1
platná	platný	k2eAgFnSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
planety	planeta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Síla	síla	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
působí	působit	k5eAaImIp3nS
Slunce	slunce	k1gNnSc1
na	na	k7c4
planetu	planeta	k1gFnSc4
<g/>
,	,	kIx,
má	můj	k3xOp1gFnSc1
velikost	velikost	k1gFnSc1
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
m	m	kA
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
m	m	kA
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
ma	ma	k?
<g/>
=	=	kIx~
<g/>
k	k	k7c3
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
m	m	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
hmotnost	hmotnost	k1gFnSc4
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Planeta	planeta	k1gFnSc1
však	však	k9
zároveň	zároveň	k6eAd1
podle	podle	k7c2
třetího	třetí	k4xOgInSc2
Newtonova	Newtonův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
působí	působit	k5eAaImIp3nS
na	na	k7c4
Slunce	slunce	k1gNnSc4
stejně	stejně	k6eAd1
velkou	velký	k2eAgFnSc7d1
silou	síla	k1gFnSc7
</s>
<s>
F	F	kA
</s>
<s>
′	′	k?
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
′	′	k?
</s>
<s>
M	M	kA
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}	}	kIx)
<g/>
=	=	kIx~
<g/>
k	k	k7c3
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
M	M	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
M	M	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
M	M	kA
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
hmotnost	hmotnost	k1gFnSc4
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
rovnosti	rovnost	k1gFnSc2
</s>
<s>
|	|	kIx~
</s>
<s>
F	F	kA
</s>
<s>
|	|	kIx~
</s>
<s>
=	=	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
F	F	kA
</s>
<s>
′	′	k?
</s>
<s>
|	|	kIx~
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
|	|	kIx~
<g/>
F	F	kA
<g/>
|	|	kIx~
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
F	F	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}	}	kIx)
<g/>
|	|	kIx~
<g/>
}	}	kIx)
</s>
<s>
dostaneme	dostat	k5eAaPmIp1nP
</s>
<s>
k	k	k7c3
</s>
<s>
m	m	kA
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
′	′	k?
</s>
<s>
M	M	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
km	km	kA
<g/>
=	=	kIx~
<g/>
k	k	k7c3
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}	}	kIx)
<g/>
M	M	kA
<g/>
}	}	kIx)
</s>
<s>
Položíme	položit	k5eAaPmIp1nP
<g/>
-li	-li	k?
</s>
<s>
G	G	kA
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
M	M	kA
</s>
<s>
=	=	kIx~
</s>
<s>
k	k	k7c3
</s>
<s>
′	′	k?
</s>
<s>
m	m	kA
</s>
<s>
=	=	kIx~
</s>
<s>
konst	konst	k1gFnSc1
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
G	G	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
k	k	k7c3
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
M	M	kA
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
k	k	k7c3
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
prime	prim	k1gInSc5
}}	}}	k?
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
mbox	mbox	k1gInSc1
<g/>
{	{	kIx(
<g/>
konst	konst	k1gInSc1
<g/>
}}}	}}}	k?
</s>
<s>
,	,	kIx,
dostáváme	dostávat	k5eAaImIp1nP
Newtonův	Newtonův	k2eAgInSc4d1
gravitační	gravitační	k2eAgInSc4d1
zákon	zákon	k1gInSc4
ve	v	k7c6
známém	známý	k2eAgInSc6d1
tvaru	tvar	k1gInSc6
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
G	G	kA
</s>
<s>
M	M	kA
</s>
<s>
m	m	kA
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
G	G	kA
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
Mm	mm	kA
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mechanika	mechanika	k1gFnSc1
</s>
<s>
Gravitace	gravitace	k1gFnSc1
</s>
<s>
Gravitační	gravitační	k2eAgNnPc1d1
pole	pole	k1gNnPc1
</s>
<s>
Newtonův	Newtonův	k2eAgInSc1d1
gravitační	gravitační	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Keplerova	Keplerův	k2eAgFnSc1d1
úloha	úloha	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Johannes	Johannes	k1gMnSc1
Kepler	Kepler	k1gMnSc1
Objevy	objev	k1gInPc7
</s>
<s>
Keplerova	Keplerův	k2eAgFnSc1d1
domněnka	domněnka	k1gFnSc1
•	•	k?
Keplerovy	Keplerův	k2eAgInPc4d1
zákony	zákon	k1gInPc4
•	•	k?
Keplerův	Keplerův	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
•	•	k?
Keplerova	Keplerův	k2eAgFnSc1d1
rovnice	rovnice	k1gFnSc1
•	•	k?
Kepler-Poinsotova	Kepler-Poinsotův	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
•	•	k?
Keplerova	Keplerův	k2eAgFnSc1d1
supernova	supernova	k1gFnSc1
•	•	k?
Keplerův	Keplerův	k2eAgInSc1d1
dalekohled	dalekohled	k1gInSc1
•	•	k?
Keplerova	Keplerův	k2eAgFnSc1d1
úloha	úloha	k1gFnSc1
Díla	dílo	k1gNnSc2
</s>
<s>
Mysterium	mysterium	k1gNnSc1
Cosmographicum	Cosmographicum	k1gNnSc1
(	(	kIx(
<g/>
1596	#num#	k4
<g/>
)	)	kIx)
•	•	k?
De	De	k?
Fundamentis	Fundamentis	k1gInSc1
Astrologiae	Astrologiae	k1gFnSc1
Certioribus	Certioribus	k1gMnSc1
(	(	kIx(
<g/>
1601	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Astronomiae	Astronomiae	k1gInSc1
Pars	Pars	k1gInSc1
Optica	Optica	k1gFnSc1
(	(	kIx(
<g/>
1604	#num#	k4
<g/>
)	)	kIx)
•	•	k?
De	De	k?
Stella	Stella	k1gFnSc1
Nova	nova	k1gFnSc1
(	(	kIx(
<g/>
1604	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Astronomia	Astronomia	k1gFnSc1
nova	nova	k1gFnSc1
(	(	kIx(
<g/>
1609	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tertius	Tertius	k1gInSc1
Interveniens	Interveniens	k1gInSc1
(	(	kIx(
<g/>
1610	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Dissertatio	Dissertatio	k1gMnSc1
cum	cum	k?
Nuncio	Nuncio	k1gMnSc1
Sidereo	Sidereo	k1gMnSc1
(	(	kIx(
<g/>
1610	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dioptrice	Dioptrika	k1gFnSc6
(	(	kIx(
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
•	•	k?
De	De	k?
nive	nivat	k5eAaPmIp3nS
sextangula	sextangula	k1gFnSc1
<g/>
…	…	k?
(	(	kIx(
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
•	•	k?
De	De	k?
vero	vero	k6eAd1
Anno	Anna	k1gFnSc5
(	(	kIx(
<g/>
1614	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Eclogae	Eclogae	k1gInSc1
Chronicae	Chronicae	k1gFnSc1
(	(	kIx(
<g/>
1615	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nova	nova	k1gFnSc1
stereometria	stereometrium	k1gNnSc2
doliorum	doliorum	k1gNnSc1
vinariorum	vinariorum	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1615	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ephemerides	Ephemerides	k1gInSc1
nouae	nouaat	k5eAaPmIp3nS
motuum	motuum	k1gInSc1
coelestium	coelestium	k1gNnSc1
(	(	kIx(
<g/>
1617	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Epitome	Epitom	k1gInSc5
astronomiae	astronomiae	k1gNnSc3
Copernicanae	Copernicanae	k1gNnSc3
(	(	kIx(
<g/>
1618	#num#	k4
<g/>
–	–	k?
<g/>
1621	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Harmonices	Harmonicesa	k1gFnPc2
mundi	mund	k1gMnPc1
(	(	kIx(
<g/>
1619	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tabulae	Tabulae	k1gInSc1
Rudolphinae	Rudolphinae	k1gFnSc1
(	(	kIx(
<g/>
1627	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Somnium	Somnium	k1gNnSc4
seu	seu	k?
Opus	opus	k1gInSc1
posthumum	posthumum	k1gNnSc1
de	de	k?
astronomia	astronomia	k1gFnSc1
Lunari	Lunar	k1gFnSc2
(	(	kIx(
<g/>
1634	#num#	k4
<g/>
)	)	kIx)
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Katharina	Katharin	k2eAgFnSc1d1
Keplerová	Keplerová	k1gFnSc1
•	•	k?
Jakob	Jakob	k1gMnSc1
Bartsch	Bartsch	k1gMnSc1
Související	související	k2eAgMnSc1d1
</s>
<s>
Kepler	Kepler	k1gMnSc1
(	(	kIx(
<g/>
opera	opera	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kepler	Kepler	k1gMnSc1
(	(	kIx(
<g/>
sonda	sonda	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Johannes	Johannes	k1gMnSc1
Kepler	Kepler	k1gMnSc1
ATV	ATV	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4365820-9	4365820-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
397	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
94003544	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
94003544	#num#	k4
</s>
