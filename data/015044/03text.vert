<s>
Povrchový	povrchový	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
</s>
<s>
Povrchový	povrchový	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
</s>
<s>
Povrchový	povrchový	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
vyznačen	vyznačit	k5eAaPmNgInS
modře	modř	k1gFnSc2
</s>
<s>
Latinsky	latinsky	k6eAd1
</s>
<s>
Musculus	Musculus	k1gInSc1
flexor	flexor	k1gInSc1
digitorum	digitorum	k1gInSc4
superficialis	superficialis	k1gFnSc2
</s>
<s>
Odstup	odstup	k1gInSc1
</s>
<s>
kost	kost	k1gFnSc1
loketní	loketní	k2eAgFnSc1d1
a	a	k8xC
kost	kost	k1gFnSc1
vřetenní	vřetenní	k2eAgFnSc1d1
</s>
<s>
Úpon	úpon	k1gInSc1
</s>
<s>
Báze	báze	k1gFnSc1
středních	střední	k2eAgInPc2d1
článků	článek	k1gInPc2
prstů	prst	k1gInPc2
</s>
<s>
Inervace	inervace	k1gFnSc1
</s>
<s>
středový	středový	k2eAgInSc1d1
nerv	nerv	k1gInSc1
</s>
<s>
Funkce	funkce	k1gFnSc1
</s>
<s>
flexe	flexe	k1gFnSc1
prstů	prst	k1gInPc2
ruky	ruka	k1gFnSc2
</s>
<s>
Povrchový	povrchový	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
:	:	kIx,
musculus	musculus	k1gInSc1
flexor	flexor	k1gInSc1
digitorum	digitorum	k1gInSc1
superficialis	superficialis	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sval	sval	k1gInSc4
druhé	druhý	k4xOgFnSc2
vrstvy	vrstva	k1gFnSc2
svalů	sval	k1gInPc2
předloktí	předloktí	k1gNnSc2
zodpovědný	zodpovědný	k2eAgInSc1d1
za	za	k7c4
flexi	flexe	k1gFnSc4
druhého	druhý	k4xOgInSc2
až	až	k6eAd1
pátého	pátý	k4xOgInSc2
prstu	prst	k1gInSc2
(	(	kIx(
<g/>
kromě	kromě	k7c2
posledního	poslední	k2eAgInSc2d1
článku	článek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ohýbá	ohýbat	k5eAaImIp3nS
hluboký	hluboký	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
(	(	kIx(
<g/>
m.	m.	k?
flexor	flexor	k1gInSc1
digitorum	digitorum	k1gInSc1
profundus	profundus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
pomocnou	pomocný	k2eAgFnSc4d1
flexi	flexe	k1gFnSc4
předloktí	předloktí	k1gNnSc2
a	a	k8xC
ruky	ruka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
hlavy	hlava	k1gFnPc4
–	–	k?
pažněloketní	pažněloketní	k2eAgFnSc4d1
hlavu	hlava	k1gFnSc4
(	(	kIx(
<g/>
caput	caput	k2eAgInSc1d1
humeroulnare	humeroulnar	k1gMnSc5
<g/>
)	)	kIx)
a	a	k8xC
vřetenní	vřetenní	k2eAgFnSc4d1
hlavu	hlava	k1gFnSc4
(	(	kIx(
<g/>
caput	caput	k1gInSc1
radiale	radiale	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pažněloketní	Pažněloketní	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
na	na	k7c6
přístředním	přístřednit	k5eAaPmIp1nS
nadkůstku	nadkůstka	k1gFnSc4
(	(	kIx(
<g/>
epicondylus	epicondylus	k1gInSc4
medialis	medialis	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
kosti	kost	k1gFnPc1
loketní	loketní	k2eAgFnPc1d1
(	(	kIx(
<g/>
ulna	ulna	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vřetenní	vřetenní	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
na	na	k7c4
kosti	kost	k1gFnPc4
vřetenní	vřetenní	k2eAgFnPc4d1
(	(	kIx(
<g/>
radius	radius	k1gInSc4
<g/>
)	)	kIx)
podél	podél	k7c2
přivracečové	přivracečový	k2eAgFnSc2d1
drsnatiny	drsnatina	k1gFnSc2
(	(	kIx(
<g/>
tuberositas	tuberositas	k1gInSc1
pronatoria	pronatorium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sval	sval	k1gInSc1
se	se	k3xPyFc4
upíná	upínat	k5eAaImIp3nS
pomocí	pomocí	k7c2
čtyř	čtyři	k4xCgFnPc2
šlach	šlacha	k1gFnPc2
na	na	k7c6
bázi	báze	k1gFnSc6
středního	střední	k2eAgInSc2d1
článku	článek	k1gInSc2
druhého	druhý	k4xOgInSc2
až	až	k6eAd1
pátého	pátý	k4xOgInSc2
prstu	prst	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sval	sval	k1gInSc1
je	být	k5eAaImIp3nS
inervován	inervovat	k5eAaImNgInS
středovým	středový	k2eAgInSc7d1
nervem	nerv	k1gInSc7
(	(	kIx(
<g/>
nervus	nervus	k1gMnSc1
medianus	medianus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Svaly	sval	k1gInPc1
horní	horní	k2eAgFnSc2d1
končetiny	končetina	k1gFnSc2
člověka	člověk	k1gMnSc2
Svaly	sval	k1gInPc7
ramene	rameno	k1gNnSc2
a	a	k8xC
lopatky	lopatka	k1gFnSc2
</s>
<s>
Svaly	sval	k1gInPc1
rotátorové	rotátorový	k2eAgFnSc2d1
manžety	manžeta	k1gFnSc2
</s>
<s>
Nadhřebenový	Nadhřebenový	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
supraspinatus	supraspinatus	k1gInSc1
</s>
<s>
Podhřebenový	Podhřebenový	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
infraspinatus	infraspinatus	k1gInSc1
</s>
<s>
Malý	malý	k2eAgInSc1d1
oblý	oblý	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
teres	teres	k1gInSc1
minor	minor	k2eAgInSc1d1
</s>
<s>
Podlopatkový	podlopatkový	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
subscapularis	subscapularis	k1gInSc1
Další	další	k2eAgInPc1d1
svaly	sval	k1gInPc1
ramene	rameno	k1gNnSc2
a	a	k8xC
lopatky	lopatka	k1gFnSc2
</s>
<s>
Deltový	deltový	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
deltoideus	deltoideus	k1gInSc1
</s>
<s>
Velký	velký	k2eAgInSc1d1
oblý	oblý	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
teres	teres	k1gMnSc1
major	major	k1gMnSc1
</s>
<s>
Svaly	sval	k1gInPc1
paže	paže	k1gFnSc2
</s>
<s>
Přední	přední	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Dvojhlavý	dvojhlavý	k2eAgInSc4d1
sval	sval	k1gInSc4
pažní	pažní	k2eAgInSc4d1
m.	m.	k?
biceps	biceps	k1gInSc4
brachii	brachie	k1gFnSc4
•	•	k?
Nadpažní	Nadpažní	k2eAgInSc4d1
sval	sval	k1gInSc4
m.	m.	k?
coracobrachialis	coracobrachialis	k1gInSc1
•	•	k?
Pažní	pažní	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
brachialis	brachialis	k1gFnSc2
Zadní	zadní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Trojhlavý	trojhlavý	k2eAgInSc4d1
sval	sval	k1gInSc4
pažní	pažní	k2eAgInSc4d1
m.	m.	k?
triceps	triceps	k1gInSc4
brachii	brachie	k1gFnSc4
•	•	k?
Loketní	loketní	k2eAgInSc4d1
sval	sval	k1gInSc4
m.	m.	k?
anconeus	anconeus	k1gInSc1
</s>
<s>
Svaly	sval	k1gInPc1
předloktí	předloktí	k1gNnSc2
</s>
<s>
Přední	přední	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
vrstva	vrstva	k1gFnSc1
</s>
<s>
Oblý	oblý	k2eAgInSc1d1
přitahovač	přitahovač	k1gInSc1
m.	m.	k?
pronator	pronator	k1gInSc1
teres	teres	k1gMnSc1
•	•	k?
Vřetenní	vřetenní	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
zápěstí	zápěstí	k1gNnSc2
m.	m.	k?
flexor	flexor	k1gInSc1
carpi	carp	k1gFnSc2
radialis	radialis	k1gFnSc3
•	•	k?
Loketní	loketní	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
zápěstí	zápěstí	k1gNnSc2
m.	m.	k?
flexor	flexor	k1gInSc4
carpi	carp	k1gFnSc2
ulnaris	ulnaris	k1gFnSc2
•	•	k?
Dlouhý	dlouhý	k2eAgInSc1d1
dlaňový	dlaňový	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
palmaris	palmaris	k1gInSc1
longus	longus	k1gInSc1
Druhá	druhý	k4xOgFnSc1
vrstva	vrstva	k1gFnSc1
</s>
<s>
Povrchový	povrchový	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
m.	m.	k?
flexor	flexor	k1gInSc1
digitorum	digitorum	k1gInSc1
superficialis	superficialis	k1gFnSc2
Třetí	třetí	k4xOgFnSc1
vrstva	vrstva	k1gFnSc1
</s>
<s>
Hluboký	hluboký	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
m.	m.	k?
flexor	flexor	k1gInSc1
digitorum	digitorum	k1gInSc1
profundus	profundus	k1gInSc4
•	•	k?
Dlouhý	dlouhý	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
palce	palec	k1gInSc2
m.	m.	k?
flexor	flexor	k1gInSc1
pollicis	pollicis	k1gInSc1
longus	longus	k1gInSc1
Čtvrtá	čtvrtá	k1gFnSc1
vrstva	vrstva	k1gFnSc1
</s>
<s>
Čtvercový	čtvercový	k2eAgMnSc1d1
přivraceč	přivraceč	k1gMnSc1
m.	m.	k?
pronator	pronator	k1gMnSc1
quadratus	quadratus	k1gMnSc1
</s>
<s>
Boční	boční	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
</s>
<s>
Vřetenopažní	Vřetenopažní	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
brachioradialis	brachioradialis	k1gInSc1
•	•	k?
Dlouhý	dlouhý	k2eAgInSc1d1
vřetenní	vřetenní	k2eAgInSc1d1
natahovač	natahovač	k1gInSc1
zápěstí	zápěstí	k1gNnSc2
m.	m.	k?
extensor	extensor	k1gInSc4
carpi	carp	k1gFnSc2
radialis	radialis	k1gFnSc2
longus	longus	k1gInSc1
<g/>
•	•	k?
Krátký	krátký	k2eAgInSc1d1
vřetenní	vřetenní	k2eAgInSc1d1
natahovač	natahovač	k1gInSc1
zápěstí	zápěstí	k1gNnSc2
m.	m.	k?
extensor	extensor	k1gInSc4
carpi	carp	k1gFnSc2
radialis	radialis	k1gFnSc1
brevis	brevis	k1gFnSc1
Hluboká	Hluboká	k1gFnSc1
vrstva	vrstva	k1gFnSc1
</s>
<s>
Odvraceč	Odvraceč	k1gMnSc1
m.	m.	k?
supinator	supinator	k1gMnSc1
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
</s>
<s>
Natahovač	natahovač	k1gInSc1
prstů	prst	k1gInPc2
m.	m.	k?
extensor	extensor	k1gInSc1
digitorum	digitorum	k1gInSc1
•	•	k?
Natahovač	natahovač	k1gInSc1
malíku	malík	k1gInSc2
m.	m.	k?
extensor	extensor	k1gInSc4
digiti	digit	k5eAaPmF,k5eAaBmF,k5eAaImF
minimi	mini	k1gFnPc7
•	•	k?
Loketní	loketní	k2eAgInSc4d1
natahovač	natahovač	k1gInSc4
zápěstí	zápěstí	k1gNnSc2
m.	m.	k?
extensor	extensor	k1gInSc4
carpi	carp	k1gFnSc2
ulnaris	ulnaris	k1gFnSc1
Hluboká	Hluboká	k1gFnSc1
vrstva	vrstva	k1gFnSc1
</s>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1
odtahovač	odtahovač	k1gInSc1
palce	palec	k1gInPc1
m.	m.	k?
abductor	abductor	k1gInSc1
pollicis	pollicis	k1gInSc1
longus	longus	k1gInSc1
•	•	k?
Krátký	krátký	k2eAgInSc1d1
natahovač	natahovač	k1gInSc1
palce	palec	k1gInPc1
m.	m.	k?
extensor	extensor	k1gInSc1
pollicis	pollicis	k1gInSc1
brevis	brevis	k1gInSc1
•	•	k?
Dlouhý	dlouhý	k2eAgInSc1d1
natahovač	natahovač	k1gInSc1
palce	palec	k1gInPc1
m.	m.	k?
extensor	extensor	k1gInSc1
pollicis	pollicis	k1gInSc1
longus	longus	k1gInSc1
•	•	k?
Natahovač	natahovač	k1gInSc1
ukazováku	ukazovák	k1gInSc2
m.	m.	k?
extensor	extensor	k1gInSc4
digiti	digit	k5eAaPmF,k5eAaBmF,k5eAaImF
minimi	mini	k1gFnPc7
</s>
<s>
Svaly	sval	k1gInPc1
ruky	ruka	k1gFnSc2
</s>
<s>
Svaly	sval	k1gInPc1
tenaru	tenar	k1gInSc2
</s>
<s>
Krátký	krátký	k2eAgInSc1d1
odtahovač	odtahovač	k1gInSc1
palce	palec	k1gInPc1
m.	m.	k?
abductor	abductor	k1gInSc1
pollicis	pollicis	k1gFnPc2
brevis	brevis	k1gFnSc2
•	•	k?
Protistavěč	Protistavěč	k1gMnSc1
palce	palec	k1gInSc2
m.	m.	k?
opponens	opponens	k1gInSc1
policis	policis	k1gFnSc2
•	•	k?
Krátký	krátký	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
palce	palec	k1gInSc2
m.	m.	k?
flexor	flexor	k1gInSc1
pollicis	pollicis	k1gInSc1
brevis	brevis	k1gInSc1
•	•	k?
Přitahovač	přitahovač	k1gInSc1
palce	palec	k1gInPc1
m.	m.	k?
adductor	adductor	k1gInSc4
pollicis	pollicis	k1gFnSc2
Svaly	sval	k1gInPc1
hypotenaru	hypotenara	k1gFnSc4
</s>
<s>
Krátký	krátký	k2eAgInSc1d1
dlaňový	dlaňový	k2eAgInSc1d1
sval	sval	k1gInSc1
m.	m.	k?
palmaris	palmaris	k1gInSc1
brevis	brevis	k1gInSc1
•	•	k?
Odtahovač	odtahovač	k1gInSc1
malíku	malík	k1gInSc2
m.	m.	k?
abductor	abductor	k1gMnSc1
digiti	digit	k5eAaBmF,k5eAaImF,k5eAaPmF
minimi	mini	k1gFnPc7
•	•	k?
krátký	krátký	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
malíku	malík	k1gInSc2
m.	m.	k?
flexor	flexor	k1gInSc4
digiti	digit	k5eAaPmF,k5eAaImF,k5eAaBmF
minimi	mini	k1gFnPc7
•	•	k?
protistavěč	protistavěč	k1gMnSc1
malíku	malík	k1gInSc2
m.	m.	k?
opponens	opponens	k6eAd1
digiti	digit	k5eAaImF,k5eAaBmF,k5eAaPmF
minimi	mini	k1gFnPc7
Meziprstní	meziprstní	k2eAgFnSc2d1
svaly	sval	k1gInPc4
</s>
<s>
Dlaňové	dlaňový	k2eAgInPc1d1
meziprstní	meziprstní	k2eAgInPc1d1
svaly	sval	k1gInPc1
mm	mm	kA
<g/>
.	.	kIx.
interossei	interosse	k1gInSc6
palmares	palmares	k1gInSc4
<g/>
•	•	k?
Hřbetní	hřbetní	k2eAgInPc4d1
meziprstní	meziprstní	k2eAgInPc4d1
svaly	sval	k1gInPc4
mm	mm	kA
<g/>
.	.	kIx.
interossei	interosseit	k5eAaImRp2nS,k5eAaPmRp2nS
dorsales	dorsales	k1gInSc4
Červovité	červovitý	k2eAgInPc1d1
svaly	sval	k1gInPc1
</s>
<s>
Červovité	červovitý	k2eAgInPc1d1
svaly	sval	k1gInPc1
mm	mm	kA
<g/>
.	.	kIx.
lumbricales	lumbricales	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Medicína	medicína	k1gFnSc1
</s>
<s>
↑	↑	k?
HUDÁK	HUDÁK	kA
<g/>
,	,	kIx,
RADOVAN	Radovan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Memorix	Memorix	k1gInSc1
anatomy	anatom	k1gMnPc4
:	:	kIx,
entire	entir	k1gInSc5
human	human	k1gMnSc1
anatomy	anatom	k1gMnPc4
in	in	k?
English	English	k1gMnSc1
and	and	k?
Latin	Latin	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7553	#num#	k4
<g/>
-	-	kIx~
<g/>
415	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7553	#num#	k4
<g/>
-	-	kIx~
<g/>
415	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
1005716209	#num#	k4
</s>
