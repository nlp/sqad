<s>
Povrchový	povrchový	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
:	:	kIx,
musculus	musculus	k1gInSc1
flexor	flexor	k1gInSc1
digitorum	digitorum	k1gInSc1
superficialis	superficialis	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sval	sval	k1gInSc4
druhé	druhý	k4xOgFnSc2
vrstvy	vrstva	k1gFnSc2
svalů	sval	k1gInPc2
předloktí	předloktí	k1gNnSc2
zodpovědný	zodpovědný	k2eAgInSc1d1
za	za	k7c4
flexi	flexe	k1gFnSc4
druhého	druhý	k4xOgInSc2
až	až	k6eAd1
pátého	pátý	k4xOgInSc2
prstu	prst	k1gInSc2
(	(	kIx(
<g/>
kromě	kromě	k7c2
posledního	poslední	k2eAgInSc2d1
článku	článek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ohýbá	ohýbat	k5eAaImIp3nS
hluboký	hluboký	k2eAgMnSc1d1
ohýbač	ohýbač	k1gMnSc1
prstů	prst	k1gInPc2
(	(	kIx(
<g/>
m.	m.	k?
flexor	flexor	k1gInSc1
digitorum	digitorum	k1gInSc1
profundus	profundus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
pomocnou	pomocný	k2eAgFnSc4d1
flexi	flexe	k1gFnSc4
předloktí	předloktí	k1gNnSc2
a	a	k8xC
ruky	ruka	k1gFnSc2
<g/>
.	.	kIx.
</s>