<p>
<s>
Serena	Seren	k2eAgFnSc1d1	Serena
Jameka	Jameka	k1gFnSc1	Jameka
Williamsová	Williamsová	k1gFnSc1	Williamsová
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1981	[number]	k4	1981
Saginaw	Saginaw	k1gFnPc2	Saginaw
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
Některými	některý	k3yIgMnPc7	některý
komentátory	komentátor	k1gMnPc7	komentátor
<g/>
,	,	kIx,	,
odborníky	odborník	k1gMnPc7	odborník
a	a	k8xC	a
tenisty	tenista	k1gMnPc7	tenista
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
hráčku	hráčka	k1gFnSc4	hráčka
v	v	k7c6	v
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
historii	historie	k1gFnSc6	historie
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
byla	být	k5eAaImAgFnS	být
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
dvouhře	dvouhra	k1gFnSc6	dvouhra
i	i	k8xC	i
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
získala	získat	k5eAaPmAgFnS	získat
singlový	singlový	k2eAgMnSc1d1	singlový
i	i	k8xC	i
deblový	deblový	k2eAgMnSc1d1	deblový
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
Grand	grand	k1gMnSc1	grand
Slam	sláma	k1gFnPc2	sláma
výhrou	výhra	k1gFnSc7	výhra
všech	všecek	k3xTgInPc2	všecek
čtyř	čtyři	k4xCgInPc2	čtyři
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
a	a	k8xC	a
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
tenistka	tenistka	k1gFnSc1	tenistka
na	na	k7c6	na
odměnách	odměna	k1gFnPc6	odměna
vydělala	vydělat	k5eAaPmAgFnS	vydělat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
prodloužení	prodloužení	k1gNnSc4	prodloužení
herního	herní	k2eAgInSc2d1	herní
výpadku	výpadek	k1gInSc2	výpadek
trvajícího	trvající	k2eAgInSc2d1	trvající
od	od	k7c2	od
února	únor	k1gInSc2	únor
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
únorovým	únorový	k2eAgNnSc7d1	únorové
čtvrtfinále	čtvrtfinále	k1gNnSc7	čtvrtfinále
světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
2018	[number]	k4	2018
proti	proti	k7c3	proti
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
odehrála	odehrát	k5eAaPmAgFnS	odehrát
se	se	k3xPyFc4	se
sestrou	sestra	k1gFnSc7	sestra
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
.	.	kIx.	.
<g/>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
tenistkou	tenistka	k1gFnSc7	tenistka
dvouhry	dvouhra	k1gFnSc2	dvouhra
stala	stát	k5eAaPmAgFnS	stát
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Sedmkrát	sedmkrát	k6eAd1	sedmkrát
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
pozici	pozice	k1gFnSc4	pozice
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
nejstarší	starý	k2eAgFnSc1d3	nejstarší
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
29	[number]	k4	29
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
pak	pak	k9	pak
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
rekord	rekord	k1gInSc4	rekord
Grafové	Grafová	k1gFnSc2	Grafová
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
186	[number]	k4	186
týdnů	týden	k1gInPc2	týden
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
klasifikace	klasifikace	k1gFnSc2	klasifikace
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
počtem	počet	k1gInSc7	počet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
319	[number]	k4	319
týdnů	týden	k1gInPc2	týden
jí	on	k3xPp3gFnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
získala	získat	k5eAaPmAgFnS	získat
23	[number]	k4	23
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
před	před	k7c7	před
Steffi	Steffi	k1gFnSc7	Steffi
Grafovou	Grafová	k1gFnSc7	Grafová
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
ve	v	k7c6	v
statistikách	statistika	k1gFnPc6	statistika
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
za	za	k7c2	za
Margaret	Margareta	k1gFnPc2	Margareta
Courtovou	Courtový	k2eAgFnSc4d1	Courtový
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
tenistek	tenistka	k1gFnPc2	tenistka
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zkompletovat	zkompletovat	k5eAaPmF	zkompletovat
Grand	grand	k1gMnSc1	grand
Slam	slam	k1gInSc4	slam
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
tenistou	tenista	k1gMnSc7	tenista
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
tři	tři	k4xCgFnPc4	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
grandslamů	grandslam	k1gInPc2	grandslam
alespoň	alespoň	k9	alespoň
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc6	první
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
trofejemi	trofej	k1gFnPc7	trofej
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
majorů	major	k1gMnPc2	major
i	i	k8xC	i
jediným	jediné	k1gNnSc7	jediné
s	s	k7c7	s
10	[number]	k4	10
takovými	takový	k3xDgNnPc7	takový
tituly	titul	k1gInPc4	titul
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
dekádách	dekáda	k1gFnPc6	dekáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
londýnských	londýnský	k2eAgFnPc6d1	londýnská
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
po	po	k7c6	po
Grafové	Grafová	k1gFnSc6	Grafová
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
Grand	grand	k1gMnSc1	grand
Slam	slam	k1gInSc1	slam
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
39	[number]	k4	39
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
23	[number]	k4	23
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
,	,	kIx,	,
14	[number]	k4	14
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
2	[number]	k4	2
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
za	za	k7c4	za
Martinu	Martin	k2eAgFnSc4d1	Martina
Navrátilovou	Navrátilová	k1gFnSc4	Navrátilová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Toura	k1gFnPc2	Toura
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
72	[number]	k4	72
singlových	singlový	k2eAgNnPc2d1	singlové
a	a	k8xC	a
23	[number]	k4	23
deblových	deblový	k2eAgInPc2d1	deblový
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
trojnásobnou	trojnásobný	k2eAgFnSc7d1	trojnásobná
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
vítězkou	vítězka	k1gFnSc7	vítězka
z	z	k7c2	z
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2000	[number]	k4	2000
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
a	a	k8xC	a
2012	[number]	k4	2012
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
finančních	finanční	k2eAgFnPc6d1	finanční
odměnách	odměna	k1gFnPc6	odměna
vydělala	vydělat	k5eAaPmAgFnS	vydělat
největší	veliký	k2eAgFnSc4d3	veliký
částku	částka	k1gFnSc4	částka
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
tenistek	tenistka	k1gFnPc2	tenistka
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jí	on	k3xPp3gFnSc2	on
také	také	k6eAd1	také
patří	patřit	k5eAaImIp3nS	patřit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
sportovkyněmi	sportovkyně	k1gFnPc7	sportovkyně
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
starší	starý	k2eAgFnSc1d2	starší
sestra	sestra	k1gFnSc1	sestra
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
profesionální	profesionální	k2eAgFnSc1d1	profesionální
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
,	,	kIx,	,
bývalá	bývalý	k2eAgFnSc1d1	bývalá
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
i	i	k8xC	i
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
Venus	Venus	k1gMnSc1	Venus
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
odehrály	odehrát	k5eAaPmAgInP	odehrát
29	[number]	k4	29
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
sedmnáct	sedmnáct	k4xCc4	sedmnáct
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Serena	Serena	k1gFnSc1	Serena
<g/>
.	.	kIx.	.
</s>
<s>
Devětkrát	devětkrát	k6eAd1	devětkrát
se	se	k3xPyFc4	se
utkaly	utkat	k5eAaPmAgInP	utkat
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
sedmkrát	sedmkrát	k6eAd1	sedmkrát
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
Serena	Serena	k1gFnSc1	Serena
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
French	French	k1gInSc4	French
Open	Open	k1gInSc4	Open
2002	[number]	k4	2002
a	a	k8xC	a
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
2003	[number]	k4	2003
vzájemně	vzájemně	k6eAd1	vzájemně
odehrály	odehrát	k5eAaPmAgFnP	odehrát
všechny	všechen	k3xTgFnPc1	všechen
čtyři	čtyři	k4xCgFnPc1	čtyři
grandslamová	grandslamový	k2eAgNnPc4d1	grandslamové
finále	finále	k1gNnSc4	finále
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
tenisu	tenis	k1gInSc2	tenis
probojovali	probojovat	k5eAaPmAgMnP	probojovat
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
za	za	k7c7	za
sebou	se	k3xPyFc7	se
jdoucích	jdoucí	k2eAgFnPc2d1	jdoucí
finále	finále	k1gNnSc6	finále
grandslamu	grandslam	k1gInSc6	grandslam
dva	dva	k4xCgMnPc1	dva
stejní	stejný	k2eAgMnPc1d1	stejný
tenisté	tenista	k1gMnPc1	tenista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příchod	příchod	k1gInSc4	příchod
sester	sestra	k1gFnPc2	sestra
Williamsových	Williamsův	k2eAgMnPc2d1	Williamsův
je	být	k5eAaImIp3nS	být
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
nové	nový	k2eAgFnSc2d1	nová
éry	éra	k1gFnSc2	éra
ženského	ženský	k2eAgInSc2d1	ženský
tenisu	tenis	k1gInSc2	tenis
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nastolila	nastolit	k5eAaPmAgFnS	nastolit
větší	veliký	k2eAgFnSc4d2	veliký
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
usilovnost	usilovnost	k1gFnSc4	usilovnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2016	[number]	k4	2016
se	se	k3xPyFc4	se
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
stala	stát	k5eAaPmAgFnS	stát
nejlépe	dobře	k6eAd3	dobře
vydělávající	vydělávající	k2eAgFnSc7d1	vydělávající
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
její	její	k3xOp3gInPc1	její
příjmy	příjem	k1gInPc1	příjem
časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc1	forbes
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
na	na	k7c4	na
28,9	[number]	k4	28,9
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
reklamních	reklamní	k2eAgInPc2d1	reklamní
kontraktů	kontrakt	k1gInPc2	kontrakt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
michiganském	michiganský	k2eAgNnSc6d1	Michiganské
městě	město	k1gNnSc6	město
Saginaw	Saginaw	k1gFnSc2	Saginaw
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
Richarda	Richard	k1gMnSc2	Richard
Williamse	Williams	k1gMnSc2	Williams
a	a	k8xC	a
Oracene	Oracen	k1gInSc5	Oracen
Priceové	Priceové	k2eAgInSc2d1	Priceové
afroamerického	afroamerický	k2eAgInSc2d1	afroamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
z	z	k7c2	z
pěti	pět	k4xCc2	pět
Priceových	Priceův	k2eAgFnPc2d1	Priceova
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Polosestrami	polosestra	k1gFnPc7	polosestra
jsou	být	k5eAaImIp3nP	být
Yetunde	Yetund	k1gInSc5	Yetund
Priceová	Priceový	k2eAgFnSc1d1	Priceová
(	(	kIx(	(
<g/>
zemřela	zemřít	k5eAaPmAgFnS	zemřít
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lyndrea	Lyndre	k2eAgFnSc1d1	Lyndre
a	a	k8xC	a
Isha	Ish	k2eAgFnSc1d1	Ish
Price	Price	k1gFnSc1	Price
a	a	k8xC	a
starší	starý	k2eAgFnSc7d2	starší
sestrou	sestra	k1gFnSc7	sestra
je	být	k5eAaImIp3nS	být
Venus	Venus	k1gInSc4	Venus
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Tenis	tenis	k1gInSc4	tenis
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
na	na	k7c4	na
losangeleské	losangeleský	k2eAgNnSc4d1	losangeleské
předměstí	předměstí	k1gNnSc4	předměstí
Compton	Compton	k1gInSc4	Compton
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
ji	on	k3xPp3gFnSc4	on
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gMnSc1	Venus
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
také	také	k9	také
se	se	k3xPyFc4	se
ženou	žena	k1gFnSc7	žena
trénoval	trénovat	k5eAaImAgInS	trénovat
<g/>
.	.	kIx.	.
<g/>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
z	z	k7c2	z
Comptonu	Compton	k1gInSc2	Compton
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
West	Westa	k1gFnPc2	Westa
Palm	Palm	k1gMnSc1	Palm
Beach	Beach	k1gMnSc1	Beach
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
tenisovou	tenisový	k2eAgFnSc4d1	tenisová
akademii	akademie	k1gFnSc4	akademie
Ricka	Ricek	k1gMnSc2	Ricek
Macciho	Macci	k1gMnSc2	Macci
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
talent	talent	k1gInSc4	talent
obou	dva	k4xCgFnPc2	dva
začínajících	začínající	k2eAgFnPc2d1	začínající
hráček	hráčka	k1gFnPc2	hráčka
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
jí	jíst	k5eAaImIp3nS	jíst
po	po	k7c6	po
roce	rok	k1gInSc6	rok
přestal	přestat	k5eAaPmAgInS	přestat
schvalovat	schvalovat	k5eAaImF	schvalovat
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
požadoval	požadovat	k5eAaImAgMnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
začala	začít	k5eAaPmAgFnS	začít
více	hodně	k6eAd2	hodně
zaměřovat	zaměřovat	k5eAaImF	zaměřovat
na	na	k7c4	na
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroku	krok	k1gInSc3	krok
byla	být	k5eAaImAgFnS	být
rasová	rasový	k2eAgFnSc1d1	rasová
otázka	otázka	k1gFnSc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
bílé	bílý	k2eAgFnSc2d1	bílá
pleti	pleť	k1gFnSc2	pleť
opakovaně	opakovaně	k6eAd1	opakovaně
slyšel	slyšet	k5eAaImAgMnS	slyšet
hanlivé	hanlivý	k2eAgFnPc4d1	hanlivá
urážky	urážka	k1gFnPc4	urážka
na	na	k7c4	na
mladé	mladý	k2eAgFnPc4d1	mladá
sestry	sestra	k1gFnPc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
žákyně	žákyně	k1gFnSc1	žákyně
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
vyhraných	vyhraná	k1gFnPc2	vyhraná
<g/>
–	–	k?	–
<g/>
prohraných	prohraná	k1gFnPc2	prohraná
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
46	[number]	k4	46
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Americké	americký	k2eAgFnSc2d1	americká
tenisové	tenisový	k2eAgFnSc2d1	tenisová
asociace	asociace	k1gFnSc2	asociace
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
žákyň	žákyně	k1gFnPc2	žákyně
Floridy	Florida	k1gFnSc2	Florida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
otec	otec	k1gMnSc1	otec
ukončil	ukončit	k5eAaPmAgMnS	ukončit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
tenisovou	tenisový	k2eAgFnSc7d1	tenisová
akademií	akademie	k1gFnSc7	akademie
a	a	k8xC	a
plně	plně	k6eAd1	plně
převzal	převzít	k5eAaPmAgInS	převzít
trénování	trénování	k1gNnSc4	trénování
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herní	herní	k2eAgInSc4d1	herní
styl	styl	k1gInSc4	styl
==	==	k?	==
</s>
</p>
<p>
<s>
Podstatou	podstata	k1gFnSc7	podstata
její	její	k3xOp3gFnSc2	její
hry	hra	k1gFnSc2	hra
jsou	být	k5eAaImIp3nP	být
údery	úder	k1gInPc4	úder
od	od	k7c2	od
základní	základní	k2eAgFnSc2d1	základní
čáry	čára	k1gFnSc2	čára
dvorce	dvorec	k1gInSc2	dvorec
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
výměnu	výměna	k1gFnSc4	výměna
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgNnSc1d1	silné
podání	podání	k1gNnSc1	podání
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
servis	servis	k1gInSc4	servis
hraje	hrát	k5eAaImIp3nS	hrát
stabilně	stabilně	k6eAd1	stabilně
s	s	k7c7	s
dobrou	dobrý	k2eAgFnSc7d1	dobrá
úspěšností	úspěšnost	k1gFnSc7	úspěšnost
(	(	kIx(	(
<g/>
některými	některý	k3yIgFnPc7	některý
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
úder	úder	k1gInSc1	úder
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vůbec	vůbec	k9	vůbec
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
tenise	tenis	k1gInSc6	tenis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
rychlost	rychlost	k1gFnSc1	rychlost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
206,5	[number]	k4	206,5
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
podání	podání	k1gNnSc4	podání
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ženského	ženský	k2eAgInSc2d1	ženský
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
parametru	parametr	k1gInSc6	parametr
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
pouze	pouze	k6eAd1	pouze
sestra	sestra	k1gFnSc1	sestra
Venus	Venus	k1gInSc1	Venus
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
silné	silný	k2eAgInPc4d1	silný
údery	úder	k1gInPc4	úder
patří	patřit	k5eAaImIp3nP	patřit
základní	základní	k2eAgInPc1d1	základní
údery	úder	k1gInPc1	úder
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
–	–	k?	–
forhend	forhend	k1gInSc1	forhend
i	i	k8xC	i
bekhend	bekhend	k1gInSc1	bekhend
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
také	také	k9	také
zvládá	zvládat	k5eAaImIp3nS	zvládat
voleje	volej	k1gInPc4	volej
<g/>
,	,	kIx,	,
hru	hra	k1gFnSc4	hra
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
smečů	smeč	k1gInPc2	smeč
a	a	k8xC	a
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
jí	on	k3xPp3gFnSc2	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
i	i	k9	i
výška	výška	k1gFnSc1	výška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
její	její	k3xOp3gInSc1	její
forhend	forhend	k1gInSc1	forhend
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejtvrdší	tvrdý	k2eAgInPc4d3	nejtvrdší
údery	úder	k1gInPc4	úder
na	na	k7c6	na
ženském	ženský	k2eAgInSc6d1	ženský
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
obouručný	obouručný	k2eAgInSc1d1	obouručný
bekhend	bekhend	k1gInSc1	bekhend
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
účinnosti	účinnost	k1gFnSc6	účinnost
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
ještě	ještě	k6eAd1	ještě
lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
,	,	kIx,	,
řadící	řadící	k2eAgFnSc1d1	řadící
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
nejlepším	dobrý	k2eAgFnPc3d3	nejlepší
představitelkám	představitelka	k1gFnPc3	představitelka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
charakteristice	charakteristika	k1gFnSc6	charakteristika
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Justine	Justin	k1gMnSc5	Justin
Heninové	Heninové	k2eAgMnSc7d1	Heninové
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
bekhendové	bekhendový	k2eAgFnSc2d1	bekhendová
strany	strana	k1gFnSc2	strana
umí	umět	k5eAaImIp3nS	umět
zakončit	zakončit	k5eAaPmF	zakončit
výměnu	výměna	k1gFnSc4	výměna
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
pozice	pozice	k1gFnSc2	pozice
kurtu	kurt	k1gInSc2	kurt
<g/>
,	,	kIx,	,
po	po	k7c6	po
lajně	lajna	k1gFnSc6	lajna
či	či	k8xC	či
krosovým	krosový	k2eAgInSc7d1	krosový
úderem	úder	k1gInSc7	úder
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
z	z	k7c2	z
obranného	obranný	k2eAgNnSc2d1	obranné
postavení	postavení	k1gNnSc2	postavení
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
údery	úder	k1gInPc1	úder
jsou	být	k5eAaImIp3nP	být
hrány	hrát	k5eAaImNgInP	hrát
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
otevřeného	otevřený	k2eAgInSc2d1	otevřený
postoje	postoj	k1gInSc2	postoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charakteristickým	charakteristický	k2eAgMnSc7d1	charakteristický
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
agresivní	agresivní	k2eAgInSc1d1	agresivní
způsob	způsob	k1gInSc1	způsob
hry	hra	k1gFnSc2	hra
doprovázený	doprovázený	k2eAgInSc1d1	doprovázený
relativně	relativně	k6eAd1	relativně
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
nevynucených	vynucený	k2eNgFnPc2d1	nevynucená
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
koriguje	korigovat	k5eAaBmIp3nS	korigovat
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
umisťovaný	umisťovaný	k2eAgInSc1d1	umisťovaný
servis	servis	k1gInSc1	servis
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
umí	umět	k5eAaImIp3nS	umět
úspěšně	úspěšně	k6eAd1	úspěšně
použít	použít	k5eAaPmF	použít
zejména	zejména	k9	zejména
v	v	k7c6	v
krizových	krizový	k2eAgInPc6d1	krizový
okamžicích	okamžik	k1gInPc6	okamžik
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
velkých	velký	k2eAgInPc6d1	velký
míčích	míč	k1gInPc6	míč
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
důležité	důležitý	k2eAgFnPc4d1	důležitá
hry	hra	k1gFnPc4	hra
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
<g/>
Ačkoli	ačkoli	k8xS	ačkoli
ji	on	k3xPp3gFnSc4	on
řada	řada	k1gFnSc1	řada
odborníků	odborník	k1gMnPc2	odborník
a	a	k8xC	a
novinářů	novinář	k1gMnPc2	novinář
řadí	řadit	k5eAaImIp3nS	řadit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
útočně	útočně	k6eAd1	útočně
laděným	laděný	k2eAgFnPc3d1	laděná
tenistkám	tenistka	k1gFnPc3	tenistka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
zvládá	zvládat	k5eAaImIp3nS	zvládat
i	i	k9	i
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
obrannou	obranný	k2eAgFnSc4d1	obranná
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jí	on	k3xPp3gFnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
k	k	k7c3	k
vynikajícím	vynikající	k2eAgInPc3d1	vynikající
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
<g/>
:	:	kIx,	:
Raná	raný	k2eAgFnSc1d1	raná
fáze	fáze	k1gFnSc1	fáze
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
profesionální	profesionální	k2eAgInSc4d1	profesionální
turnaj	turnaj	k1gInSc4	turnaj
tenistka	tenistka	k1gFnSc1	tenistka
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
září	září	k1gNnSc6	září
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
Bell	bell	k1gInSc1	bell
Challenge	Challenge	k1gFnSc4	Challenge
v	v	k7c6	v
Québecu	Québecus	k1gInSc6	Québecus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
tehdy	tehdy	k6eAd1	tehdy
149	[number]	k4	149
<g/>
.	.	kIx.	.
hráčce	hráčka	k1gFnSc6	hráčka
světa	svět	k1gInSc2	svět
Annie	Annie	k1gFnSc2	Annie
Millerové	Millerové	k2eAgFnSc2d1	Millerové
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
na	na	k7c6	na
odměnách	odměna	k1gFnPc6	odměna
vydělala	vydělat	k5eAaPmAgFnS	vydělat
240	[number]	k4	240
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
žádného	žádný	k3yNgInSc2	žádný
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
postupně	postupně	k6eAd1	postupně
neprošla	projít	k5eNaPmAgFnS	projít
přes	přes	k7c4	přes
kvalifikační	kvalifikační	k2eAgNnSc4d1	kvalifikační
kola	kolo	k1gNnSc2	kolo
tří	tři	k4xCgInPc2	tři
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
než	než	k8xS	než
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
první	první	k4xOgInSc4	první
vítězný	vítězný	k2eAgInSc4d1	vítězný
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
soutěži	soutěž	k1gFnSc6	soutěž
ženské	ženský	k2eAgFnSc2d1	ženská
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1997	[number]	k4	1997
na	na	k7c6	na
Ameritech	Amerit	k1gInPc6	Amerit
Cup	cup	k1gInSc4	cup
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
jako	jako	k8xS	jako
304	[number]	k4	304
<g/>
.	.	kIx.	.
hráčka	hráčka	k1gFnSc1	hráčka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
dokázala	dokázat	k5eAaPmAgFnS	dokázat
porazit	porazit	k5eAaPmF	porazit
světovou	světový	k2eAgFnSc4d1	světová
sedmičku	sedmička	k1gFnSc4	sedmička
Mary	Mary	k1gFnPc2	Mary
Piercovou	Piercový	k2eAgFnSc4d1	Piercový
i	i	k9	i
čtyřku	čtyřka	k1gFnSc4	čtyřka
Moniku	Monika	k1gFnSc4	Monika
Selešovou	Selešová	k1gFnSc4	Selešová
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
tak	tak	k6eAd1	tak
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
hráčkami	hráčka	k1gFnPc7	hráčka
první	první	k4xOgFnPc4	první
světové	světový	k2eAgFnPc4d1	světová
desítky	desítka	k1gFnPc4	desítka
jako	jako	k8xC	jako
nejníže	nízce	k6eAd3	nízce
postavená	postavený	k2eAgFnSc1d1	postavená
tenistka	tenistka	k1gFnSc1	tenistka
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
světového	světový	k2eAgInSc2d1	světový
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
pátou	pátý	k4xOgFnSc7	pátý
hráčkou	hráčka	k1gFnSc7	hráčka
světa	svět	k1gInSc2	svět
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
Davenportovou	Davenportová	k1gFnSc7	Davenportová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
již	již	k9	již
uzavírala	uzavírat	k5eAaImAgFnS	uzavírat
první	první	k4xOgFnSc4	první
stovku	stovka	k1gFnSc4	stovka
hráček	hráčka	k1gFnPc2	hráčka
<g/>
,	,	kIx,	,
když	když	k8xS	když
figurovala	figurovat	k5eAaImAgFnS	figurovat
na	na	k7c4	na
99	[number]	k4	99
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
1998	[number]	k4	1998
zahájila	zahájit	k5eAaPmAgFnS	zahájit
startem	start	k1gInSc7	start
na	na	k7c4	na
Medibank	Medibank	k1gInSc4	Medibank
International	International	k1gFnSc2	International
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
kvalifikantka	kvalifikantka	k1gFnSc1	kvalifikantka
a	a	k8xC	a
96	[number]	k4	96
<g/>
.	.	kIx.	.
hráčka	hráčka	k1gFnSc1	hráčka
porazila	porazit	k5eAaPmAgFnS	porazit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
světovou	světový	k2eAgFnSc4d1	světová
trojku	trojka	k1gFnSc4	trojka
Davenportovou	Davenportová	k1gFnSc7	Davenportová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
následně	následně	k6eAd1	následně
v	v	k7c6	v
semifinálovém	semifinálový	k2eAgInSc6d1	semifinálový
duelu	duel	k1gInSc6	duel
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Španělce	Španělka	k1gFnSc6	Španělka
Arantxe	Arantxe	k1gFnSc1	Arantxe
Sánchezové	Sánchezová	k1gFnSc2	Sánchezová
Vicariové	Vicariový	k2eAgFnSc2d1	Vicariová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
dvouhře	dvouhra	k1gFnSc6	dvouhra
triumfoval	triumfovat	k5eAaBmAgMnS	triumfovat
český	český	k2eAgMnSc1d1	český
tenista	tenista	k1gMnSc1	tenista
Petr	Petr	k1gMnSc1	Petr
Korda	Korda	k1gMnSc1	Korda
<g/>
,	,	kIx,	,
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
zdolala	zdolat	k5eAaPmAgFnS	zdolat
šestou	šestý	k4xOgFnSc4	šestý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Irinu	Irien	k2eAgFnSc4d1	Irina
Spîrleaovou	Spîrleaová	k1gFnSc4	Spîrleaová
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
kolo	kolo	k1gNnSc4	kolo
odehrála	odehrát	k5eAaPmAgFnS	odehrát
proti	proti	k7c3	proti
sestře	sestra	k1gFnSc3	sestra
Venus	Venus	k1gInSc4	Venus
Williamsové	Williamsová	k1gFnSc2	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
první	první	k4xOgInSc4	první
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
zápas	zápas	k1gInSc4	zápas
na	na	k7c6	na
profesionálním	profesionální	k2eAgInSc6d1	profesionální
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
starší	starý	k2eAgFnSc1d2	starší
sestra	sestra	k1gFnSc1	sestra
Venus	Venus	k1gInSc4	Venus
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sezóny	sezóna	k1gFnSc2	sezóna
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
šest	šest	k4xCc4	šest
čtvrtfinálových	čtvrtfinálový	k2eAgInPc2d1	čtvrtfinálový
turnajových	turnajový	k2eAgInPc2d1	turnajový
účastí	účast	k1gFnSc7	účast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovšem	ovšem	k9	ovšem
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
a	a	k8xC	a
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
odešla	odejít	k5eAaPmAgFnS	odejít
poražená	poražený	k2eAgFnSc1d1	poražená
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
premiérový	premiérový	k2eAgInSc4d1	premiérový
zápas	zápas	k1gInSc4	zápas
proti	proti	k7c3	proti
světové	světový	k2eAgFnSc3d1	světová
jedničce	jednička	k1gFnSc3	jednička
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tehdy	tehdy	k6eAd1	tehdy
představovala	představovat	k5eAaImAgFnS	představovat
Martina	Martina	k1gFnSc1	Martina
Hingisová	Hingisový	k2eAgFnSc1d1	Hingisová
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Lipton	Lipton	k1gInSc1	Lipton
International	International	k1gFnSc2	International
Players	Playersa	k1gFnPc2	Playersa
Championships	Championshipsa	k1gFnPc2	Championshipsa
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc4	druhý
střetnutí	střetnutí	k1gNnSc4	střetnutí
proti	proti	k7c3	proti
starší	starý	k2eAgFnSc3d2	starší
sestře	sestra	k1gFnSc3	sestra
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c4	na
Italian	Italian	k1gInSc4	Italian
Open	Opena	k1gFnPc2	Opena
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
grandslamech	grandslam	k1gInPc6	grandslam
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
probojovat	probojovat	k5eAaPmF	probojovat
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Sánchezové	Sánchezové	k2eAgFnSc1d1	Sánchezové
Vicariové	Vicariový	k2eAgNnSc4d1	Vicariový
<g/>
,	,	kIx,	,
do	do	k7c2	do
třetích	třetí	k4xOgNnPc2	třetí
kol	kolo	k1gNnPc2	kolo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostala	dostat	k5eAaPmAgFnS	dostat
ve	v	k7c4	v
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
a	a	k8xC	a
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Virginii	Virginie	k1gFnSc4	Virginie
Ruanovou	Ruanový	k2eAgFnSc4d1	Ruanový
Pascualovou	Pascualová	k1gFnSc4	Pascualová
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgMnSc6	druhý
pak	pak	k6eAd1	pak
na	na	k7c4	na
Spîrleaovou	Spîrleaová	k1gFnSc4	Spîrleaová
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgInPc4	první
grandslamové	grandslamový	k2eAgInPc4d1	grandslamový
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
nejdříve	dříve	k6eAd3	dříve
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
prestižních	prestižní	k2eAgFnPc6d1	prestižní
událostech	událost	k1gFnPc6	událost
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
Bělorus	Bělorus	k1gMnSc1	Bělorus
Max	Max	k1gMnSc1	Max
Mirnyj	Mirnyj	k1gMnSc1	Mirnyj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhre	čtyřhr	k1gInSc5	čtyřhr
sestry	sestra	k1gFnPc4	sestra
Williamsovy	Williamsův	k2eAgFnPc1d1	Williamsova
poprvé	poprvé	k6eAd1	poprvé
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
turnaj	turnaj	k1gInSc4	turnaj
na	na	k7c4	na
IGA	IGA	kA	IGA
Superthrift	Superthrift	k1gMnSc1	Superthrift
Classic	Classic	k1gMnSc1	Classic
v	v	k7c4	v
Oklahoma	Oklahoma	k1gNnSc4	Oklahoma
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
třetí	třetí	k4xOgFnSc7	třetí
sesterskou	sesterský	k2eAgFnSc7d1	sesterská
dvojicí	dvojice	k1gFnSc7	dvojice
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
tenisu	tenis	k1gInSc2	tenis
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
titul	titul	k1gInSc4	titul
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
získat	získat	k5eAaPmF	získat
další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
vítězství	vítězství	k1gNnPc4	vítězství
a	a	k8xC	a
Serena	Serena	k1gFnSc1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
zakončila	zakončit	k5eAaPmAgFnS	zakončit
sezónu	sezóna	k1gFnSc4	sezóna
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
pro	pro	k7c4	pro
dvouhru	dvouhra	k1gFnSc4	dvouhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Vstup	vstup	k1gInSc1	vstup
mezi	mezi	k7c4	mezi
10	[number]	k4	10
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
hráček	hráčka	k1gFnPc2	hráčka
světa	svět	k1gInSc2	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
úvodním	úvodní	k2eAgInSc6d1	úvodní
grandslamu	grandslam	k1gInSc6	grandslam
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
Sandrine	Sandrin	k1gInSc5	Sandrin
Testudovou	Testudový	k2eAgFnSc7d1	Testudový
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
měsíc	měsíc	k1gInSc4	měsíc
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
svůj	svůj	k3xOyFgInSc4	svůj
premiérový	premiérový	k2eAgInSc4d1	premiérový
titul	titul	k1gInSc4	titul
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pařížského	pařížský	k2eAgInSc2d1	pařížský
Open	Openo	k1gNnPc2	Openo
Gaz	Gaz	k1gMnSc2	Gaz
de	de	k?	de
France	Franc	k1gMnSc2	Franc
zdolala	zdolat	k5eAaPmAgFnS	zdolat
domácí	domácí	k1gFnSc4	domácí
Amélii	Amélie	k1gFnSc4	Amélie
Mauresmovou	Mauresmová	k1gFnSc4	Mauresmová
<g/>
,	,	kIx,	,
finalistku	finalistka	k1gFnSc4	finalistka
právě	právě	k9	právě
skončeného	skončený	k2eAgInSc2d1	skončený
prvního	první	k4xOgInSc2	první
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
Oběma	dva	k4xCgFnPc3	dva
mladým	mladý	k2eAgFnPc3d1	mladá
sestrám	sestra	k1gFnPc3	sestra
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
nový	nový	k2eAgInSc1d1	nový
tenisový	tenisový	k2eAgInSc4d1	tenisový
historický	historický	k2eAgInSc4d1	historický
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
pařížském	pařížský	k2eAgNnSc6d1	pařížské
vítězství	vítězství	k1gNnSc6	vítězství
Sereny	Serena	k1gFnSc2	Serena
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
titul	titul	k1gInSc4	titul
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
5	[number]	k4	5
000	[number]	k4	000
mil	míle	k1gFnPc2	míle
IGA	IGA	kA	IGA
Superthrift	Superthrift	k1gMnSc1	Superthrift
Classic	Classic	k1gMnSc1	Classic
v	v	k7c4	v
Oklahoma	Oklahoma	k1gNnSc4	Oklahoma
City	City	k1gFnSc2	City
také	také	k9	také
její	její	k3xOp3gFnSc1	její
osmnáctiletá	osmnáctiletý	k2eAgFnSc1d1	osmnáctiletá
sestra	sestra	k1gFnSc1	sestra
Venus	Venus	k1gInSc4	Venus
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
staly	stát	k5eAaPmAgFnP	stát
prvními	první	k4xOgFnPc7	první
sestrami	sestra	k1gFnPc7	sestra
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
kdy	kdy	k6eAd1	kdy
triumfovaly	triumfovat	k5eAaBmAgInP	triumfovat
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
týdnu	týden	k1gInSc6	týden
i	i	k8xC	i
dnu	den	k1gInSc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
měsíc	měsíc	k1gInSc4	měsíc
pak	pak	k6eAd1	pak
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
turnaji	turnaj	k1gInSc6	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
WTA	WTA	kA	WTA
Tier	Tier	k1gInSc1	Tier
I	i	k8xC	i
Evert	Evert	k1gInSc1	Evert
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
zdolala	zdolat	k5eAaPmAgFnS	zdolat
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
světovou	světový	k2eAgFnSc4d1	světová
sedmičku	sedmička	k1gFnSc4	sedmička
Steffi	Steffi	k1gFnSc1	Steffi
Grafovou	Grafová	k1gFnSc7	Grafová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
události	událost	k1gFnPc4	událost
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
Lipton	Lipton	k1gInSc1	Lipton
International	International	k1gFnSc2	International
Players	Playersa	k1gFnPc2	Playersa
Championships	Championshipsa	k1gFnPc2	Championshipsa
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
porazila	porazit	k5eAaPmAgFnS	porazit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Martina	Martin	k1gInSc2	Martin
Hingisovou	Hingisový	k2eAgFnSc7d1	Hingisová
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
ukončila	ukončit	k5eAaPmAgFnS	ukončit
její	její	k3xOp3gFnSc4	její
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
šňůru	šňůra	k1gFnSc4	šňůra
šestnácti	šestnáct	k4xCc2	šestnáct
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Venus	Venus	k1gMnSc1	Venus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dominovala	dominovat	k5eAaImAgFnS	dominovat
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
turnajovém	turnajový	k2eAgNnSc6d1	turnajové
finále	finále	k1gNnSc6	finále
historie	historie	k1gFnSc2	historie
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
mezi	mezi	k7c7	mezi
nejlepšími	dobrý	k2eAgInPc7d3	nejlepší
deseti	deset	k4xCc7	deset
tenistkami	tenistka	k1gFnPc7	tenistka
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
figurovala	figurovat	k5eAaImAgFnS	figurovat
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jarní	jarní	k2eAgFnSc6d1	jarní
antukové	antukový	k2eAgFnSc6d1	antuková
části	část	k1gFnSc6	část
1999	[number]	k4	1999
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
odehrála	odehrát	k5eAaPmAgFnS	odehrát
tři	tři	k4xCgInPc4	tři
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
Tier	Tier	k1gMnSc1	Tier
I	I	kA	I
Italian	Italian	k1gMnSc1	Italian
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Hingisovou	Hingisový	k2eAgFnSc4d1	Hingisová
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
události	událost	k1gFnSc6	událost
Tier	Tier	k1gMnSc1	Tier
I	i	k8xC	i
German	German	k1gMnSc1	German
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
sedmé	sedmý	k4xOgFnSc2	sedmý
tenistky	tenistka	k1gFnSc2	tenistka
světa	svět	k1gInSc2	svět
Arantxy	Arantx	k1gInPc4	Arantx
Sánchezové	Sánchezová	k1gFnPc1	Sánchezová
Vicariové	Vicariový	k2eAgFnPc1d1	Vicariová
<g/>
.	.	kIx.	.
</s>
<s>
Sestry	sestra	k1gFnPc1	sestra
Williamsovy	Williamsův	k2eAgFnPc1d1	Williamsova
pak	pak	k6eAd1	pak
získaly	získat	k5eAaPmAgFnP	získat
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
z	z	k7c2	z
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Serena	Serena	k1gFnSc1	Serena
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
již	již	k6eAd1	již
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
krajankou	krajanka	k1gFnSc7	krajanka
Mary	Mary	k1gFnSc7	Mary
Joe	Joe	k1gFnSc7	Joe
Fernandezovou	Fernandezová	k1gFnSc7	Fernandezová
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
poté	poté	k6eAd1	poté
odřekla	odřeknout	k5eAaPmAgFnS	odřeknout
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
nejslavnějším	slavný	k2eAgInSc6d3	nejslavnější
turnaji	turnaj	k1gInSc6	turnaj
světa	svět	k1gInSc2	svět
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
<g/>
,	,	kIx,	,
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
Fed	Fed	k1gFnSc4	Fed
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
odehrála	odehrát	k5eAaPmAgFnS	odehrát
dvě	dva	k4xCgFnPc4	dva
události	událost	k1gFnPc4	událost
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
letní	letní	k2eAgFnSc2d1	letní
části	část	k1gFnSc2	část
1999	[number]	k4	1999
konané	konaný	k2eAgFnSc6d1	konaná
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
JPMorgan	JPMorgany	k1gInPc2	JPMorgany
Chase	chasa	k1gFnSc3	chasa
Open	Open	k1gInSc4	Open
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přemohla	přemoct	k5eAaPmAgFnS	přemoct
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Švýcarku	Švýcarka	k1gFnSc4	Švýcarka
Hingisovou	Hingisový	k2eAgFnSc4d1	Hingisová
a	a	k8xC	a
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Julií	Julie	k1gFnSc7	Julie
Halardovou-Decugisovou	Halardovou-Decugisový	k2eAgFnSc7d1	Halardovou-Decugisový
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
byla	být	k5eAaImAgFnS	být
nasazená	nasazený	k2eAgFnSc1d1	nasazená
jako	jako	k8xC	jako
hráčka	hráčka	k1gFnSc1	hráčka
číslo	číslo	k1gNnSc4	číslo
sedm	sedm	k4xCc1	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výborná	výborný	k2eAgFnSc1d1	výborná
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
<g/>
,	,	kIx,	,
když	když	k8xS	když
postupně	postupně	k6eAd1	postupně
porazila	porazit	k5eAaPmAgFnS	porazit
světovou	světový	k2eAgFnSc4d1	světová
čtyřku	čtyřka	k1gFnSc4	čtyřka
Selešovou	Selešová	k1gFnSc4	Selešová
<g/>
,	,	kIx,	,
dvojku	dvojka	k1gFnSc4	dvojka
Davenportovou	Davenportová	k1gFnSc4	Davenportová
a	a	k8xC	a
jedničku	jednička	k1gFnSc4	jednička
Hingisovou	Hingisový	k2eAgFnSc4d1	Hingisová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
historicky	historicky	k6eAd1	historicky
druhou	druhý	k4xOgFnSc7	druhý
Afroameričankou	Afroameričanka	k1gFnSc7	Afroameričanka
(	(	kIx(	(
<g/>
po	po	k7c4	po
Althee	Althee	k1gInSc4	Althee
Gibsonové	Gibsonové	k2eAgInSc4d1	Gibsonové
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
na	na	k7c6	na
French	Fren	k1gFnPc6	Fren
Open	Open	k1gNnSc1	Open
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kdy	kdy	k6eAd1	kdy
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
singlový	singlový	k2eAgInSc4d1	singlový
titul	titul	k1gInSc4	titul
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
turnaj	turnaj	k1gInSc1	turnaj
dovršila	dovršit	k5eAaPmAgFnS	dovršit
společně	společně	k6eAd1	společně
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvítězily	zvítězit	k5eAaPmAgInP	zvítězit
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
druhý	druhý	k4xOgInSc4	druhý
společný	společný	k2eAgInSc4d1	společný
grandslam	grandslam	k1gInSc4	grandslam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
obě	dva	k4xCgFnPc1	dva
dopomohly	dopomoct	k5eAaPmAgInP	dopomoct
k	k	k7c3	k
finálové	finálový	k2eAgFnSc3d1	finálová
výhře	výhra	k1gFnSc3	výhra
amerického	americký	k2eAgNnSc2d1	americké
družstva	družstvo	k1gNnSc2	družstvo
ve	v	k7c6	v
Fed	Fed	k1gFnSc6	Fed
Cupu	cup	k1gInSc2	cup
nad	nad	k7c7	nad
Ruskem	Rusko	k1gNnSc7	Rusko
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Grand	grand	k1gMnSc1	grand
Slam	slam	k1gInSc4	slam
Cup	cup	k1gInSc4	cup
hraného	hraný	k2eAgInSc2d1	hraný
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Serena	Serena	k1gFnSc1	Serena
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
ve	v	k7c6	v
Filderstadtu	Filderstadto	k1gNnSc6	Filderstadto
ovšem	ovšem	k9	ovšem
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
odehrála	odehrát	k5eAaPmAgFnS	odehrát
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc4	rok
2000	[number]	k4	2000
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
prohrou	prohra	k1gFnSc7	prohra
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
osmifinále	osmifinále	k1gNnSc6	osmifinále
<g/>
)	)	kIx)	)
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
šestnáctou	šestnáctý	k4xOgFnSc4	šestnáctý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Rusku	Ruska	k1gFnSc4	Ruska
Jelenu	Jelena	k1gFnSc4	Jelena
Lichovcevovou	Lichovcevová	k1gFnSc4	Lichovcevová
<g/>
.	.	kIx.	.
</s>
<s>
Neuspěla	uspět	k5eNaPmAgFnS	uspět
při	při	k7c6	při
obhajobách	obhajoba	k1gFnPc6	obhajoba
titulů	titul	k1gInPc2	titul
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
a	a	k8xC	a
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
díky	díky	k7c3	díky
zranění	zranění	k1gNnSc3	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vybojovat	vybojovat	k5eAaPmF	vybojovat
titul	titul	k1gInSc4	titul
na	na	k7c4	na
Faber	Faber	k1gInSc4	Faber
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
v	v	k7c6	v
Hannoveru	Hannover	k1gInSc6	Hannover
<g/>
.	.	kIx.	.
</s>
<s>
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
vynechala	vynechat	k5eAaPmAgFnS	vynechat
kvůli	kvůli	k7c3	kvůli
poranění	poranění	k1gNnSc3	poranění
<g/>
.	.	kIx.	.
</s>
<s>
Vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
grandslamu	grandslam	k1gInSc6	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
semifinále	semifinále	k1gNnPc6	semifinále
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
příští	příští	k2eAgInSc4d1	příští
vítězce	vítězka	k1gFnSc3	vítězka
Venus	Venus	k1gInSc4	Venus
Williamsové	Williamsové	k2eAgInSc4d1	Williamsové
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
ztratila	ztratit	k5eAaPmAgFnS	ztratit
třináct	třináct	k4xCc4	třináct
her	hra	k1gFnPc2	hra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
sestry	sestra	k1gFnSc2	sestra
dominovaly	dominovat	k5eAaImAgFnP	dominovat
a	a	k8xC	a
získaly	získat	k5eAaPmAgFnP	získat
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
úspěšně	úspěšně	k6eAd1	úspěšně
obhájila	obhájit	k5eAaPmAgFnS	obhájit
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
semifinálovém	semifinálový	k2eAgNnSc6d1	semifinálové
utkání	utkání	k1gNnSc6	utkání
porazila	porazit	k5eAaPmAgFnS	porazit
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Hingisovou	Hingisový	k2eAgFnSc7d1	Hingisová
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
dvojku	dvojka	k1gFnSc4	dvojka
Davenportovou	Davenportový	k2eAgFnSc4d1	Davenportová
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
finálové	finálový	k2eAgFnSc2d1	finálová
účasti	účast	k1gFnSc2	účast
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c6	na
Du	Du	k?	Du
Maurier	Maurier	k1gMnSc1	Maurier
Open	Open	k1gMnSc1	Open
hraném	hraný	k2eAgNnSc6d1	hrané
daný	daný	k2eAgInSc4d1	daný
rok	rok	k1gInSc4	rok
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
<g/>
.	.	kIx.	.
</s>
<s>
Soupeřkou	soupeřka	k1gFnSc7	soupeřka
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zápase	zápas	k1gInSc6	zápas
byla	být	k5eAaImAgFnS	být
Hingisová	Hingisový	k2eAgFnSc1d1	Hingisová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
pro	pro	k7c4	pro
Švýcarku	Švýcarka	k1gFnSc4	Švýcarka
musela	muset	k5eAaImAgFnS	muset
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ji	on	k3xPp3gFnSc4	on
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
utkání	utkání	k1gNnSc4	utkání
skrečovat	skrečovat	k5eAaPmF	skrečovat
<g/>
.	.	kIx.	.
</s>
<s>
Obhajoba	obhajoba	k1gFnSc1	obhajoba
titulu	titul	k1gInSc2	titul
z	z	k7c2	z
Mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
mistrovství	mistrovství	k1gNnSc2	mistrovství
USA	USA	kA	USA
ve	v	k7c4	v
Flushing	Flushing	k1gInSc4	Flushing
Meadows	Meadowsa	k1gFnPc2	Meadowsa
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
druhé	druhý	k4xOgFnSc2	druhý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
Davenportové	Davenportová	k1gFnSc2	Davenportová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zářijových	zářijový	k2eAgFnPc6d1	zářijová
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2000	[number]	k4	2000
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
získaly	získat	k5eAaPmAgFnP	získat
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
poté	poté	k6eAd1	poté
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
třetí	třetí	k4xOgInSc4	třetí
singlový	singlový	k2eAgInSc4d1	singlový
titul	titul	k1gInSc4	titul
sezóny	sezóna	k1gFnSc2	sezóna
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Toyota	toyota	k1gFnSc1	toyota
Princess	Princess	k1gInSc1	Princess
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
nové	nový	k2eAgFnSc2d1	nová
sezóny	sezóna	k1gFnSc2	sezóna
2001	[number]	k4	2001
odehrála	odehrát	k5eAaPmAgFnS	odehrát
dvě	dva	k4xCgFnPc4	dva
události	událost	k1gFnPc4	událost
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
turnajích	turnaj	k1gInPc6	turnaj
Medibank	Medibank	k1gInSc4	Medibank
International	International	k1gMnPc2	International
Sydney	Sydney	k1gNnSc4	Sydney
a	a	k8xC	a
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
odešla	odejít	k5eAaPmAgFnS	odejít
poražená	poražený	k2eAgFnSc1d1	poražená
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
první	první	k4xOgFnSc7	první
hráčkou	hráčka	k1gFnSc7	hráčka
světa	svět	k1gInSc2	svět
Hingisovou	Hingisový	k2eAgFnSc4d1	Hingisová
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc1	Venus
triumfovaly	triumfovat	k5eAaBmAgInP	triumfovat
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
grandslamu	grandslam	k1gInSc6	grandslam
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
teprve	teprve	k6eAd1	teprve
pátou	pátá	k1gFnSc4	pátá
dvojicí	dvojice	k1gFnSc7	dvojice
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
všechny	všechen	k3xTgInPc4	všechen
čtyři	čtyři	k4xCgInPc4	čtyři
tituly	titul	k1gInPc4	titul
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Bojkot	bojkot	k1gInSc1	bojkot
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Masters	Masters	k1gInSc1	Masters
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
lednovém	lednový	k2eAgNnSc6d1	lednové
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gInSc4	Open
2001	[number]	k4	2001
nehrála	hrát	k5eNaImAgFnS	hrát
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
turnaje	turnaj	k1gInPc4	turnaj
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Masters	Mastersa	k1gFnPc2	Mastersa
kategorie	kategorie	k1gFnSc2	kategorie
Tier	Tier	k1gMnSc1	Tier
I.	I.	kA	I.
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Belgičanku	Belgičanka	k1gFnSc4	Belgičanka
Kim	Kim	k1gMnSc4	Kim
Clijstersovou	Clijstersová	k1gFnSc4	Clijstersová
poměrem	poměr	k1gInSc7	poměr
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
finále	finále	k1gNnSc2	finále
prošla	projít	k5eAaPmAgFnS	projít
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
čtyři	čtyři	k4xCgFnPc1	čtyři
minuty	minuta	k1gFnPc1	minuta
před	před	k7c7	před
semifinálovým	semifinálový	k2eAgNnSc7d1	semifinálové
utkáním	utkání	k1gNnSc7	utkání
sestra	sestra	k1gFnSc1	sestra
Venus	Venus	k1gInSc4	Venus
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
níž	jenž	k3xRgFnSc3	jenž
měla	mít	k5eAaImAgFnS	mít
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
sourozenkyně	sourozenkyně	k1gFnSc1	sourozenkyně
jako	jako	k8xC	jako
důvod	důvod	k1gInSc1	důvod
uvedla	uvést	k5eAaPmAgFnS	uvést
zranění	zranění	k1gNnSc4	zranění
limitující	limitující	k2eAgNnSc1d1	limitující
ji	on	k3xPp3gFnSc4	on
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
zdůvodnění	zdůvodnění	k1gNnSc1	zdůvodnění
podezřelé	podezřelý	k2eAgNnSc1d1	podezřelé
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
finále	finále	k1gNnSc2	finále
diváci	divák	k1gMnPc1	divák
"	"	kIx"	"
<g/>
bučeli	bučet	k5eAaImAgMnP	bučet
<g/>
"	"	kIx"	"
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
otce	otka	k1gFnSc6	otka
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
Venus	Venus	k1gInSc4	Venus
Williamsová	Williamsový	k2eAgFnSc1d1	Williamsová
přítomná	přítomný	k2eAgFnSc1d1	přítomná
v	v	k7c6	v
hledišti	hlediště	k1gNnSc6	hlediště
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
nesouhlasné	souhlasný	k2eNgFnPc1d1	nesouhlasná
reakce	reakce	k1gFnPc1	reakce
zazněly	zaznět	k5eAaImAgFnP	zaznět
na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
a	a	k8xC	a
tenisový	tenisový	k2eAgMnSc1d1	tenisový
trenér	trenér	k1gMnSc1	trenér
obou	dva	k4xCgFnPc2	dva
dcer	dcera	k1gFnPc2	dcera
Richard	Richarda	k1gFnPc2	Richarda
Williams	Williams	k1gInSc1	Williams
to	ten	k3xDgNnSc4	ten
později	pozdě	k6eAd2	pozdě
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
projev	projev	k1gInSc4	projev
rasismu	rasismus	k1gInSc2	rasismus
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
ze	z	k7c2	z
sester	sestra	k1gFnPc2	sestra
se	se	k3xPyFc4	se
dalších	další	k2eAgInPc2d1	další
ročníků	ročník	k1gInPc2	ročník
turnaje	turnaj	k1gInSc2	turnaj
neúčastnila	účastnit	k5eNaImAgNnP	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholetý	dlouholetý	k2eAgInSc4d1	dlouholetý
bojkot	bojkot	k1gInSc4	bojkot
ukončila	ukončit	k5eAaPmAgFnS	ukončit
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
když	když	k8xS	když
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
dvouhry	dvouhra	k1gFnSc2	dvouhra
miamského	miamský	k2eAgInSc2d1	miamský
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Venus	Venus	k1gInSc1	Venus
Williamsová	Williamsová	k1gFnSc1	Williamsová
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vrátila	vrátit	k5eAaPmAgFnS	vrátit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
BNP	BNP	kA	BNP
Paribas	Paribas	k1gMnSc1	Paribas
Open	Open	k1gMnSc1	Open
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
<g/>
Následující	následující	k2eAgInSc1d1	následující
týden	týden	k1gInSc1	týden
na	na	k7c4	na
události	událost	k1gFnPc4	událost
Tier	Tiera	k1gFnPc2	Tiera
I	i	k8xC	i
Miami	Miami	k1gNnSc4	Miami
Nasters	Nastersa	k1gFnPc2	Nastersa
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
krajankou	krajanka	k1gFnSc7	krajanka
Jennifer	Jennifra	k1gFnPc2	Jennifra
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgInSc1d1	další
průběh	průběh	k1gInSc1	průběh
sezóny	sezóna	k1gFnSc2	sezóna
2001	[number]	k4	2001
====	====	k?	====
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
sezóny	sezóna	k1gFnSc2	sezóna
neodehrála	odehrát	k5eNaPmAgFnS	odehrát
až	až	k9	až
do	do	k7c2	do
French	Frencha	k1gFnPc2	Frencha
Open	Opena	k1gFnPc2	Opena
žádný	žádný	k3yNgInSc4	žádný
turnaj	turnaj	k1gInSc4	turnaj
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
grandslamu	grandslam	k1gInSc6	grandslam
pak	pak	k6eAd1	pak
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
opět	opět	k6eAd1	opět
s	s	k7c7	s
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
před	před	k7c7	před
Wimbledonem	Wimbledon	k1gInSc7	Wimbledon
nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
ani	ani	k8xC	ani
jednoho	jeden	k4xCgInSc2	jeden
přípravného	přípravný	k2eAgInSc2d1	přípravný
turnaje	turnaj	k1gInSc2	turnaj
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
grandslam	grandslam	k1gInSc1	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
grandslamem	grandslam	k1gInSc7	grandslam
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
mezi	mezi	k7c7	mezi
posledními	poslední	k2eAgInPc7d1	poslední
osmi	osm	k4xCc7	osm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
severoamerické	severoamerický	k2eAgFnSc2d1	severoamerická
části	část	k1gFnSc2	část
2001	[number]	k4	2001
na	na	k7c6	na
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
dvorcích	dvorec	k1gInPc6	dvorec
odehrála	odehrát	k5eAaPmAgFnS	odehrát
tři	tři	k4xCgFnPc4	tři
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čtvrtfinálovém	čtvrtfinálový	k2eAgNnSc6d1	čtvrtfinálové
vyřazení	vyřazení	k1gNnSc6	vyřazení
na	na	k7c6	na
losangeleském	losangeleský	k2eAgInSc6d1	losangeleský
turnaji	turnaj	k1gInSc6	turnaj
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
druhou	druhý	k4xOgFnSc4	druhý
trofej	trofej	k1gFnSc4	trofej
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Rogers	Rogersa	k1gFnPc2	Rogersa
Cup	cup	k1gInSc4	cup
konaném	konaný	k2eAgMnSc6d1	konaný
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Selešovou	Selešová	k1gFnSc4	Selešová
a	a	k8xC	a
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
zdolala	zdolat	k5eAaPmAgFnS	zdolat
světovou	světový	k2eAgFnSc4d1	světová
trojku	trojka	k1gFnSc4	trojka
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
byla	být	k5eAaImAgFnS	být
desátou	desátá	k1gFnSc4	desátá
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
zde	zde	k6eAd1	zde
porazila	porazit	k5eAaPmAgFnS	porazit
šestou	šestý	k4xOgFnSc4	šestý
hráčku	hráčka	k1gFnSc4	hráčka
světa	svět	k1gInSc2	svět
a	a	k8xC	a
wimbledonskou	wimbledonský	k2eAgFnSc4d1	wimbledonská
finalistku	finalistka	k1gFnSc4	finalistka
Justine	Justin	k1gMnSc5	Justin
Heninovou	Heninová	k1gFnSc7	Heninová
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
trojku	trojka	k1gFnSc4	trojka
Davenportovou	Davenportová	k1gFnSc7	Davenportová
a	a	k8xC	a
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Hingisovou	Hingisový	k2eAgFnSc4d1	Hingisová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ze	z	k7c2	z
sesterského	sesterský	k2eAgNnSc2d1	sesterské
finále	finále	k1gNnSc2	finále
s	s	k7c7	s
Venus	Venus	k1gMnSc1	Venus
odešla	odejít	k5eAaPmAgFnS	odejít
poražena	porazit	k5eAaPmNgFnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
grandslam	grandslam	k1gInSc4	grandslam
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
nastoupily	nastoupit	k5eAaPmAgFnP	nastoupit
sestry	sestra	k1gFnPc1	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
události	událost	k1gFnSc6	událost
sezóny	sezóna	k1gFnSc2	sezóna
Turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
Silvii	Silvia	k1gFnSc4	Silvia
Farinaovou	Farinaový	k2eAgFnSc4d1	Farinaový
<g/>
,	,	kIx,	,
Heninovou	Heninový	k2eAgFnSc4d1	Heninová
a	a	k8xC	a
Testudovou	Testudový	k2eAgFnSc4d1	Testudový
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
turnaj	turnaj	k1gInSc1	turnaj
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
finále	finále	k1gNnSc1	finále
bylo	být	k5eAaImAgNnS	být
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
zahájením	zahájení	k1gNnSc7	zahájení
Davenportová	Davenportová	k1gFnSc1	Davenportová
skrečovala	skrečovat	k5eAaPmAgFnS	skrečovat
zápas	zápas	k1gInSc4	zápas
pro	pro	k7c4	pro
poranění	poranění	k1gNnSc4	poranění
kolene	kolen	k1gInSc5	kolen
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2001	[number]	k4	2001
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Čtyři	čtyři	k4xCgFnPc1	čtyři
grandslamové	grandslamový	k2eAgFnPc1d1	grandslamová
tituly	titul	k1gInPc4	titul
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
===	===	k?	===
</s>
</p>
<p>
<s>
Zranění	zranění	k1gNnSc1	zranění
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
ji	on	k3xPp3gFnSc4	on
donutilo	donutit	k5eAaPmAgNnS	donutit
nenastoupit	nastoupit	k5eNaPmF	nastoupit
k	k	k7c3	k
semifinálovému	semifinálový	k2eAgInSc3d1	semifinálový
zápasu	zápas	k1gInSc3	zápas
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
a	a	k8xC	a
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
grandslamu	grandslam	k1gInSc2	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
sezóny	sezóna	k1gFnSc2	sezóna
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c6	na
americké	americký	k2eAgFnSc6d1	americká
půdě	půda	k1gFnSc6	půda
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
State	status	k1gInSc5	status
Farm	Farm	k1gFnPc3	Farm
Women	Women	k1gInSc4	Women
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tennis	Tennis	k1gFnSc7	Tennis
Classic	Classice	k1gFnPc2	Classice
ve	v	k7c6	v
Scottsdale	Scottsdala	k1gFnSc6	Scottsdala
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
zdolala	zdolat	k5eAaPmAgFnS	zdolat
světovou	světový	k2eAgFnSc4d1	světová
dvojku	dvojka	k1gFnSc4	dvojka
Jennifer	Jennifra	k1gFnPc2	Jennifra
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
přidala	přidat	k5eAaPmAgFnS	přidat
další	další	k2eAgFnSc4d1	další
trofej	trofej	k1gFnSc4	trofej
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
WTA	WTA	kA	WTA
Tier	Tier	k1gInSc1	Tier
I	i	k8xC	i
Miami	Miami	k1gNnSc1	Miami
Masters	Mastersa	k1gFnPc2	Mastersa
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
podniku	podnik	k1gInSc6	podnik
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
teprve	teprve	k6eAd1	teprve
třetí	třetí	k4xOgFnSc7	třetí
tenistkou	tenistka	k1gFnSc7	tenistka
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
turnaji	turnaj	k1gInSc6	turnaj
porazila	porazit	k5eAaPmAgFnS	porazit
první	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
hráčky	hráčka	k1gFnPc1	hráčka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
když	když	k8xS	když
postupně	postupně	k6eAd1	postupně
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
trojku	trojka	k1gFnSc4	trojka
Martinu	Martina	k1gFnSc4	Martina
Hingisovou	Hingisový	k2eAgFnSc4d1	Hingisová
ve	v	k7c4	v
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
<g/>
,	,	kIx,	,
dvojku	dvojka	k1gFnSc4	dvojka
svou	svůj	k3xOyFgFnSc4	svůj
sestru	sestra	k1gFnSc4	sestra
Venus	Venus	k1gInSc4	Venus
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
první	první	k4xOgFnSc4	první
hráčku	hráčka	k1gFnSc4	hráčka
světa	svět	k1gInSc2	svět
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
<g/>
.	.	kIx.	.
</s>
<s>
Výhra	výhra	k1gFnSc1	výhra
nad	nad	k7c7	nad
sestrou	sestra	k1gFnSc7	sestra
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
byla	být	k5eAaImAgFnS	být
jejím	její	k3xOp3gNnSc7	její
teprve	teprve	k9	teprve
druhým	druhý	k4xOgNnSc7	druhý
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
druhým	druhý	k4xOgInSc7	druhý
grandslamem	grandslam	k1gInSc7	grandslam
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
tři	tři	k4xCgInPc4	tři
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
finále	finále	k1gNnSc1	finále
na	na	k7c6	na
antukovém	antukový	k2eAgInSc6d1	antukový
povrchu	povrch	k1gInSc6	povrch
vůbec	vůbec	k9	vůbec
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
na	na	k7c6	na
berlínském	berlínský	k2eAgInSc6d1	berlínský
German	German	k1gMnSc1	German
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Belgičanku	Belgičanka	k1gFnSc4	Belgičanka
Justine	Justin	k1gMnSc5	Justin
Heninovou	Heninový	k2eAgFnSc7d1	Heninová
<g/>
,	,	kIx,	,
když	když	k8xS	když
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
až	až	k9	až
tiebreak	tiebreak	k1gInSc1	tiebreak
třetího	třetí	k4xOgInSc2	třetí
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
týden	týden	k1gInSc4	týden
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
premiérový	premiérový	k2eAgInSc4d1	premiérový
antukový	antukový	k2eAgInSc4d1	antukový
titul	titul	k1gInSc4	titul
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
římského	římský	k2eAgMnSc2d1	římský
Italian	Italian	k1gInSc4	Italian
Open	Open	k1gInSc4	Open
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Capriatiovou	Capriatiový	k2eAgFnSc7d1	Capriatiová
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
oplatila	oplatit	k5eAaPmAgFnS	oplatit
porážku	porážka	k1gFnSc4	porážka
Heninové	Heninová	k1gFnSc2	Heninová
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
výsledky	výsledek	k1gInPc1	výsledek
ji	on	k3xPp3gFnSc4	on
vynesly	vynést	k5eAaPmAgInP	vynést
na	na	k7c4	na
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
žebříčkové	žebříčkový	k2eAgNnSc4d1	žebříčkové
maximum	maximum	k1gNnSc4	maximum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
třetí	třetí	k4xOgFnSc1	třetí
nasazená	nasazený	k2eAgFnSc1d1	nasazená
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gInSc4	Open
prošla	projít	k5eAaPmAgFnS	projít
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
jen	jen	k9	jen
dvou	dva	k4xCgInPc2	dva
setů	set	k1gInPc2	set
<g/>
,	,	kIx,	,
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
obhájkyni	obhájkyně	k1gFnSc4	obhájkyně
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
porazila	porazit	k5eAaPmAgFnS	porazit
Venus	Venus	k1gInSc4	Venus
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
zajistila	zajistit	k5eAaPmAgFnS	zajistit
zisk	zisk	k1gInSc4	zisk
druhého	druhý	k4xOgInSc2	druhý
grandslamu	grandslam	k1gInSc2	grandslam
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
žebříčku	žebříček	k1gInSc2	žebříček
právě	právě	k9	právě
za	za	k7c4	za
starší	starý	k2eAgFnSc4d2	starší
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
hladce	hladko	k6eAd1	hladko
přehrála	přehrát	k5eAaPmAgFnS	přehrát
v	v	k7c6	v
semifinálovém	semifinálový	k2eAgInSc6d1	semifinálový
duelu	duel	k1gInSc6	duel
Amélii	Amélie	k1gFnSc4	Amélie
Mauresmovou	Mauresmová	k1gFnSc4	Mauresmová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
setkala	setkat	k5eAaPmAgFnS	setkat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
obhájkyní	obhájkyně	k1gFnSc7	obhájkyně
titulu	titul	k1gInSc2	titul
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zdolala	zdolat	k5eAaPmAgFnS	zdolat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
svůj	svůj	k3xOyFgInSc4	svůj
třetí	třetí	k4xOgInSc4	třetí
grandslam	grandslam	k1gInSc4	grandslam
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
jediného	jediný	k2eAgInSc2d1	jediný
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
turnaji	turnaj	k1gInSc6	turnaj
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
stala	stát	k5eAaPmAgFnS	stát
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
hráčkou	hráčka	k1gFnSc7	hráčka
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
klasifikována	klasifikovat	k5eAaImNgFnS	klasifikovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
a	a	k8xC	a
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
pozici	pozice	k1gFnSc4	pozice
odsunula	odsunout	k5eAaPmAgFnS	odsunout
sestru	sestra	k1gFnSc4	sestra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
tenisu	tenis	k1gInSc2	tenis
představovala	představovat	k5eAaImAgFnS	představovat
teprve	teprve	k6eAd1	teprve
druhou	druhý	k4xOgFnSc4	druhý
Afroameričanku	Afroameričanka	k1gFnSc4	Afroameričanka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tohoto	tento	k3xDgNnSc2	tento
umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
WTA	WTA	kA	WTA
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Sestry	sestra	k1gFnPc1	sestra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
získaly	získat	k5eAaPmAgFnP	získat
další	další	k2eAgFnSc4d1	další
trofej	trofej	k1gFnSc4	trofej
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
svou	svůj	k3xOyFgFnSc4	svůj
pátou	pátá	k1gFnSc4	pátá
grandslamovou	grandslamový	k2eAgFnSc4d1	grandslamová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
Wimbledonem	Wimbledon	k1gInSc7	Wimbledon
a	a	k8xC	a
US	US	kA	US
Open	Open	k1gInSc1	Open
odehrála	odehrát	k5eAaPmAgFnS	odehrát
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc4d1	jediný
turnaj	turnaj	k1gInSc4	turnaj
JPMorgan	JPMorgana	k1gFnPc2	JPMorgana
Chase	chasa	k1gFnSc3	chasa
Open	Open	k1gInSc4	Open
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
podlehla	podlehnout	k5eAaPmAgNnP	podlehnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
krajance	krajanka	k1gFnSc6	krajanka
Chandě	Chanda	k1gFnSc3	Chanda
Rubinové	Rubinová	k1gFnSc3	Rubinová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přerušila	přerušit	k5eAaPmAgFnS	přerušit
její	její	k3xOp3gFnSc4	její
sérii	série	k1gFnSc4	série
21	[number]	k4	21
výher	výhra	k1gFnPc2	výhra
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nejvýše	nejvýše	k6eAd1	nejvýše
nasazená	nasazený	k2eAgFnSc1d1	nasazená
hráčka	hráčka	k1gFnSc1	hráčka
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
přešla	přejít	k5eAaPmAgFnS	přejít
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přes	přes	k7c4	přes
obhájkyni	obhájkyně	k1gFnSc4	obhájkyně
titulu	titul	k1gInSc2	titul
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
Davenportovou	Davenportová	k1gFnSc7	Davenportová
a	a	k8xC	a
probojovala	probojovat	k5eAaPmAgFnS	probojovat
se	se	k3xPyFc4	se
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
opět	opět	k6eAd1	opět
stejné	stejný	k2eAgNnSc1d1	stejné
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
za	za	k7c4	za
soupeřku	soupeřka	k1gFnSc4	soupeřka
byla	být	k5eAaImAgFnS	být
sestra	sestra	k1gFnSc1	sestra
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zdolala	zdolat	k5eAaPmAgFnS	zdolat
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
celkově	celkově	k6eAd1	celkově
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
dva	dva	k4xCgInPc4	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucí	jdoucí	k2eAgInPc4d1	jdoucí
turnaje	turnaj	k1gInPc4	turnaj
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
tokijský	tokijský	k2eAgInSc4d1	tokijský
Toyota	toyota	k1gFnSc1	toyota
Princess	Princess	k1gInSc1	Princess
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
dominovala	dominovat	k5eAaImAgFnS	dominovat
nad	nad	k7c4	nad
Kim	Kim	k1gFnSc4	Kim
Clijstersovou	Clijstersový	k2eAgFnSc4d1	Clijstersová
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
Sparkassen	Sparkassen	k2eAgInSc4d1	Sparkassen
Cup	cup	k1gInSc4	cup
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Ruskou	Ruska	k1gFnSc7	Ruska
Anastasií	Anastasie	k1gFnSc7	Anastasie
Myskinovou	Myskinová	k1gFnSc7	Myskinová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
události	událost	k1gFnSc6	událost
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
tenistky	tenistka	k1gFnPc4	tenistka
světa	svět	k1gInSc2	svět
Home	Home	k1gInSc1	Home
Depot	depot	k1gInSc1	depot
Championships	Championships	k1gInSc4	Championships
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
páté	pátý	k4xOgFnSc2	pátý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
Clijstersové	Clijstersová	k1gFnSc2	Clijstersová
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tak	tak	k6eAd1	tak
přerušila	přerušit	k5eAaPmAgFnS	přerušit
její	její	k3xOp3gFnSc4	její
další	další	k2eAgFnSc4d1	další
sérii	série	k1gFnSc4	série
18	[number]	k4	18
výher	výhra	k1gFnPc2	výhra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
skóre	skóre	k1gNnSc1	skóre
výher-proher	výherrohra	k1gFnPc2	výher-prohra
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
2002	[number]	k4	2002
činilo	činit	k5eAaImAgNnS	činit
56	[number]	k4	56
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
osm	osm	k4xCc1	osm
turnajových	turnajový	k2eAgInPc2d1	turnajový
titulů	titul	k1gInPc2	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
zakončení	zakončení	k1gNnSc6	zakončení
roku	rok	k1gInSc2	rok
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Althea	Althe	k2eAgFnSc1d1	Althea
Gibsonová	Gibsonová	k1gFnSc1	Gibsonová
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
klasifikována	klasifikovat	k5eAaImNgFnS	klasifikovat
jako	jako	k8xC	jako
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgMnSc7	první
Afroameričanem	Afroameričan	k1gMnSc7	Afroameričan
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tento	tento	k3xDgInSc1	tento
výkon	výkon	k1gInSc1	výkon
(	(	kIx(	(
<g/>
i	i	k9	i
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
<g/>
)	)	kIx)	)
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
.	.	kIx.	.
daný	daný	k2eAgInSc4d1	daný
rok	rok	k1gInSc4	rok
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
grandslamech	grandslam	k1gInPc6	grandslam
ze	z	k7c2	z
čtyř	čtyři	k4xCgNnPc2	čtyři
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
Hingisové	Hingisový	k2eAgFnSc2d1	Hingisová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
grandslamu	grandslam	k1gInSc6	grandslam
nové	nový	k2eAgFnSc2d1	nová
sezóny	sezóna	k1gFnSc2	sezóna
2003	[number]	k4	2003
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
míče	míč	k1gInPc4	míč
od	od	k7c2	od
vyřazení	vyřazení	k1gNnSc2	vyřazení
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Émilií	Émilie	k1gFnSc7	Émilie
Loitovou	Loitová	k1gFnSc7	Loitová
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ji	on	k3xPp3gFnSc4	on
hrozilo	hrozit	k5eAaImAgNnS	hrozit
opět	opět	k6eAd1	opět
vypadnutí	vypadnutí	k1gNnSc1	vypadnutí
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
setu	set	k1gInSc6	set
již	již	k6eAd1	již
s	s	k7c7	s
Clijstersovou	Clijstersová	k1gFnSc7	Clijstersová
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
a	a	k8xC	a
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
dva	dva	k4xCgInPc4	dva
mečboly	mečbol	k1gInPc4	mečbol
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zápas	zápas	k1gInSc1	zápas
nakonec	nakonec	k6eAd1	nakonec
získala	získat	k5eAaPmAgFnS	získat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
grandslamu	grandslam	k1gInSc2	grandslam
střetla	střetnout	k5eAaPmAgFnS	střetnout
se	s	k7c7	s
starší	starý	k2eAgFnSc7d2	starší
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc1	Venus
a	a	k8xC	a
počtvrté	počtvrté	k4xO	počtvrté
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
odešla	odejít	k5eAaPmAgFnS	odejít
vítězně	vítězně	k6eAd1	vítězně
po	po	k7c6	po
setech	set	k1gInPc6	set
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Dosažením	dosažení	k1gNnSc7	dosažení
titulu	titul	k1gInSc2	titul
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
teprve	teprve	k6eAd1	teprve
šestou	šestý	k4xOgFnSc7	šestý
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
tenisu	tenis	k1gInSc2	tenis
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Grand	grand	k1gMnSc1	grand
Slam	sláma	k1gFnPc2	sláma
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
čtyři	čtyři	k4xCgInPc4	čtyři
nejprestižnější	prestižní	k2eAgInPc4d3	nejprestižnější
tenisové	tenisový	k2eAgInPc4d1	tenisový
turnaje	turnaj	k1gInPc4	turnaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
čistý	čistý	k2eAgInSc4d1	čistý
grandslam	grandslam	k1gInSc4	grandslam
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
získán	získat	k5eAaPmNgInS	získat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jediné	jediný	k2eAgFnSc2d1	jediná
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Připojila	připojit	k5eAaPmAgFnS	připojit
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
tenisových	tenisový	k2eAgFnPc2d1	tenisová
legend	legenda	k1gFnPc2	legenda
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Billie	Billie	k1gFnSc2	Billie
Jean	Jean	k1gMnSc1	Jean
Kingové	Kingový	k2eAgFnSc2d1	Kingová
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc2	Chris
Evertové	Evertová	k1gFnSc2	Evertová
<g/>
,	,	kIx,	,
Martiny	Martina	k1gFnSc2	Martina
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
<g/>
,	,	kIx,	,
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
a	a	k8xC	a
Margaret	Margareta	k1gFnPc2	Margareta
Courtové	Courtová	k1gFnSc2	Courtová
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
také	také	k6eAd1	také
pátou	pátá	k1gFnSc7	pátá
tenistkou	tenistka	k1gFnSc7	tenistka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
grandslamové	grandslamový	k2eAgInPc4d1	grandslamový
turnaje	turnaj	k1gInPc4	turnaj
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
výkon	výkon	k1gInSc4	výkon
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
podařil	podařit	k5eAaPmAgInS	podařit
Maureen	Maureen	k1gInSc1	Maureen
Connollyové	Connollyová	k1gFnSc2	Connollyová
<g/>
,	,	kIx,	,
Courtové	Courtová	k1gFnSc2	Courtová
<g/>
,	,	kIx,	,
Grafové	Grafová	k1gFnSc2	Grafová
a	a	k8xC	a
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
<g/>
.	.	kIx.	.
</s>
<s>
Sestry	sestra	k1gFnPc1	sestra
Williamsovy	Williamsův	k2eAgFnPc1d1	Williamsova
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
na	na	k7c6	na
daném	daný	k2eAgInSc6d1	daný
Australian	Australian	k1gInSc1	Australian
Open	Opena	k1gFnPc2	Opena
šestý	šestý	k4xOgInSc4	šestý
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následně	následně	k6eAd1	následně
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
další	další	k2eAgInPc4d1	další
turnajové	turnajový	k2eAgInPc4d1	turnajový
tituly	titul	k1gInPc4	titul
z	z	k7c2	z
pařížského	pařížský	k2eAgInSc2d1	pařížský
Open	Openo	k1gNnPc2	Openo
Gaz	Gaz	k1gMnSc2	Gaz
de	de	k?	de
France	Franc	k1gMnSc2	Franc
a	a	k8xC	a
Miami	Miami	k1gNnSc1	Miami
Nasters	Nastersa	k1gFnPc2	Nastersa
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
porazila	porazit	k5eAaPmAgFnS	porazit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Clijstersovou	Clijstersová	k1gFnSc4	Clijstersová
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
prohrála	prohrát	k5eAaPmAgFnS	prohrát
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
antukové	antukový	k2eAgFnSc2d1	antuková
události	událost	k1gFnSc2	událost
Family	Famila	k1gFnSc2	Famila
Circle	Circle	k1gFnSc2	Circle
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
s	s	k7c7	s
Heninovou	Heninová	k1gFnSc7	Heninová
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
první	první	k4xOgFnSc4	první
porážku	porážka	k1gFnSc4	porážka
sezóny	sezóna	k1gFnSc2	sezóna
po	po	k7c6	po
21	[number]	k4	21
vítězstvích	vítězství	k1gNnPc6	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
prohru	prohra	k1gFnSc4	prohra
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
s	s	k7c7	s
Mauresmovou	Mauresmová	k1gFnSc7	Mauresmová
na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Internazionali	Internazionali	k1gMnSc3	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
grandslam	grandslam	k1gInSc4	grandslam
roku	rok	k1gInSc2	rok
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
jako	jako	k9	jako
nejvýše	nejvýše	k6eAd1	nejvýše
nasazená	nasazený	k2eAgFnSc1d1	nasazená
hráčka	hráčka	k1gFnSc1	hráčka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
pátou	pátá	k1gFnSc4	pátá
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Mauresmovou	Mauresmová	k1gFnSc4	Mauresmová
<g/>
,	,	kIx,	,
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
budoucí	budoucí	k2eAgFnSc4d1	budoucí
vítězku	vítězka	k1gFnSc4	vítězka
Heninovou	Heninový	k2eAgFnSc4d1	Heninová
<g/>
,	,	kIx,	,
když	když	k8xS	když
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
první	první	k4xOgFnSc4	první
grandslamovou	grandslamový	k2eAgFnSc4d1	grandslamová
porážku	porážka	k1gFnSc4	porážka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Williamsové	Williamsová	k1gFnSc2	Williamsová
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nelíbilo	líbit	k5eNaImAgNnS	líbit
Heninové	Heninový	k2eAgNnSc4d1	Heninový
sportovní	sportovní	k2eAgNnSc4d1	sportovní
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
diváci	divák	k1gMnPc1	divák
navíc	navíc	k6eAd1	navíc
aplaudovali	aplaudovat	k5eAaImAgMnP	aplaudovat
při	při	k7c6	při
jejích	její	k3xOp3gFnPc6	její
chybách	chyba	k1gFnPc6	chyba
<g/>
.	.	kIx.	.
<g/>
Zlepšení	zlepšení	k1gNnSc1	zlepšení
přišlo	přijít	k5eAaPmAgNnS	přijít
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
oplatila	oplatit	k5eAaPmAgFnS	oplatit
porážku	porážka	k1gFnSc4	porážka
Heninové	Heninová	k1gFnSc2	Heninová
a	a	k8xC	a
obhájila	obhájit	k5eAaPmAgFnS	obhájit
titul	titul	k1gInSc4	titul
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gMnSc1	Venus
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
tak	tak	k9	tak
druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
celkově	celkově	k6eAd1	celkově
šestý	šestý	k4xOgInSc4	šestý
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
<g/>
.	.	kIx.	.
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gInSc7	její
posledním	poslední	k2eAgInSc7d1	poslední
turnajem	turnaj	k1gInSc7	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
roku	rok	k1gInSc2	rok
musela	muset	k5eAaImAgFnS	muset
vynechat	vynechat	k5eAaPmF	vynechat
pro	pro	k7c4	pro
poranění	poranění	k1gNnSc4	poranění
kolene	kolen	k1gMnSc5	kolen
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
podrobila	podrobit	k5eAaPmAgFnS	podrobit
chirurgickému	chirurgický	k2eAgInSc3d1	chirurgický
výkonu	výkon	k1gInSc3	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
absence	absence	k1gFnSc1	absence
ji	on	k3xPp3gFnSc4	on
stála	stát	k5eAaImAgFnS	stát
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
nepřetržitě	přetržitě	k6eNd1	přetržitě
vládla	vládnout	k5eAaImAgFnS	vládnout
po	po	k7c4	po
57	[number]	k4	57
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
ji	on	k3xPp3gFnSc4	on
Kim	Kim	k1gFnSc1	Kim
Clijstersová	Clijstersová	k1gFnSc1	Clijstersová
<g/>
.	.	kIx.	.
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
figurovala	figurovat	k5eAaImAgNnP	figurovat
na	na	k7c6	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc3	příčka
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
tituly	titul	k1gInPc7	titul
z	z	k7c2	z
posledního	poslední	k2eAgInSc2d1	poslední
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
z	z	k7c2	z
kolenní	kolenní	k2eAgFnSc2d1	kolenní
operace	operace	k1gFnSc2	operace
byla	být	k5eAaImAgFnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2003	[number]	k4	2003
zavražděna	zavražděn	k2eAgFnSc1d1	zavražděna
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Yetunde	Yetund	k1gInSc5	Yetund
Priceová	Priceový	k2eAgFnSc5d1	Priceová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Zranění	zranění	k1gNnSc1	zranění
a	a	k8xC	a
nevyrovnané	vyrovnaný	k2eNgInPc1d1	nevyrovnaný
výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
sezóny	sezóna	k1gFnSc2	sezóna
2004	[number]	k4	2004
nestihla	stihnout	k5eNaPmAgFnS	stihnout
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zotavovala	zotavovat	k5eAaImAgFnS	zotavovat
ze	z	k7c2	z
zranění	zranění	k1gNnSc2	zranění
levého	levý	k2eAgInSc2d1	levý
kolene	kolen	k1gInSc5	kolen
<g/>
.	.	kIx.	.
</s>
<s>
Odřekla	odřeknout	k5eAaPmAgFnS	odřeknout
i	i	k9	i
další	další	k2eAgInPc4d1	další
turnaje	turnaj	k1gInPc4	turnaj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
spekulace	spekulace	k1gFnPc4	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
ztratila	ztratit	k5eAaPmAgFnS	ztratit
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
motivaci	motivace	k1gFnSc4	motivace
o	o	k7c4	o
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kurty	kurt	k1gInPc4	kurt
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
po	po	k7c6	po
osmi	osm	k4xCc6	osm
měsících	měsíc	k1gInPc6	měsíc
turnajem	turnaj	k1gInSc7	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
WTA	WTA	kA	WTA
Tier	Tier	k1gMnSc1	Tier
I	i	k8xC	i
NASDAQ-100	NASDAQ-100	k1gMnSc1	NASDAQ-100
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazila	porazit	k5eAaPmAgFnS	porazit
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
šestnáctiletou	šestnáctiletý	k2eAgFnSc4d1	šestnáctiletá
ruskou	ruský	k2eAgFnSc4d1	ruská
juniorku	juniorka	k1gFnSc4	juniorka
Marii	Maria	k1gFnSc4	Maria
Šarapovovou	Šarapovová	k1gFnSc4	Šarapovová
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
osmou	osmý	k4xOgFnSc4	osmý
hráčku	hráčka	k1gFnSc4	hráčka
světa	svět	k1gInSc2	svět
Jelenu	Jelena	k1gFnSc4	Jelena
Dementěvovou	Dementěvová	k1gFnSc4	Dementěvová
hladce	hladko	k6eAd1	hladko
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
řade	řad	k1gInSc5	řad
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
tento	tento	k3xDgInSc4	tento
turnaj	turnaj	k1gInSc4	turnaj
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
se	se	k3xPyFc4	se
navrátila	navrátit	k5eAaPmAgFnS	navrátit
na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
grandslam	grandslam	k1gInSc4	grandslam
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
odehrála	odehrát	k5eAaPmAgFnS	odehrát
tři	tři	k4xCgInPc4	tři
antukové	antukový	k2eAgInPc4d1	antukový
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Poražena	poražen	k2eAgFnSc1d1	poražena
odešla	odejít	k5eAaPmAgFnS	odejít
ze	z	k7c2	z
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
Bausch	Bausch	k1gMnSc1	Bausch
&	&	k?	&
Lomb	Lomb	k1gInSc1	Lomb
Championships	Championships	k1gInSc1	Championships
ve	v	k7c6	v
floridském	floridský	k2eAgInSc6d1	floridský
Amelia	Amelium	k1gNnSc2	Amelium
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
následující	následující	k2eAgInSc4d1	následující
týden	týden	k1gInSc4	týden
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
před	před	k7c7	před
třetím	třetí	k4xOgNnSc7	třetí
kolem	kolo	k1gNnSc7	kolo
pro	pro	k7c4	pro
opětovné	opětovný	k2eAgNnSc4d1	opětovné
zranění	zranění	k1gNnSc4	zranění
kolene	kolen	k1gInSc5	kolen
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
Tier	Tiera	k1gFnPc2	Tiera
I	i	k9	i
Family	Famil	k1gInPc1	Famil
Circle	Circle	k1gFnSc1	Circle
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
čtyři	čtyři	k4xCgInPc1	čtyři
týdny	týden	k1gInPc1	týden
odpočívala	odpočívat	k5eAaImAgFnS	odpočívat
než	než	k8xS	než
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
antukovou	antukový	k2eAgFnSc7d1	antuková
událostí	událost	k1gFnSc7	událost
kategorie	kategorie	k1gFnSc2	kategorie
Tier	Tiera	k1gFnPc2	Tiera
I	i	k9	i
Rome	Rom	k1gMnSc5	Rom
Masters	Masters	k1gInSc4	Masters
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
světové	světový	k2eAgFnPc4d1	světová
devítce	devítka	k1gFnSc6	devítka
Jennifer	Jennifero	k1gNnPc2	Jennifero
Capriatiové	Capriatiový	k2eAgFnSc2d1	Capriatiová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
až	až	k9	až
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
pořadatelé	pořadatel	k1gMnPc1	pořadatel
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
ji	on	k3xPp3gFnSc4	on
nasadili	nasadit	k5eAaPmAgMnP	nasadit
do	do	k7c2	do
ženské	ženský	k2eAgFnSc2d1	ženská
dvouhry	dvouhra	k1gFnSc2	dvouhra
jako	jako	k8xC	jako
dvojku	dvojka	k1gFnSc4	dvojka
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgNnPc7	první
čtyřmi	čtyři	k4xCgNnPc7	čtyři
koly	kolo	k1gNnPc7	kolo
prošla	projít	k5eAaPmAgFnS	projít
přes	přes	k7c4	přes
hráčky	hráčka	k1gFnPc4	hráčka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
klasifikované	klasifikovaný	k2eAgInPc1d1	klasifikovaný
mimo	mimo	k7c4	mimo
TOP	topit	k5eAaImRp2nS	topit
50	[number]	k4	50
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
pak	pak	k6eAd1	pak
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
Capriatiovou	Capriatiový	k2eAgFnSc4d1	Capriatiová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jí	on	k3xPp3gFnSc3	on
přehrála	přehrát	k5eAaPmAgFnS	přehrát
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
vyřazení	vyřazení	k1gNnSc4	vyřazení
před	před	k7c7	před
semifinálem	semifinále	k1gNnSc7	semifinále
grandslamu	grandslam	k1gInSc2	grandslam
od	od	k7c2	od
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
klesla	klesnout	k5eAaPmAgFnS	klesnout
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
pozice	pozice	k1gFnPc4	pozice
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
pořadatelé	pořadatel	k1gMnPc1	pořadatel
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgFnPc2d1	vlastní
zvyklostí	zvyklost	k1gFnPc2	zvyklost
nasadili	nasadit	k5eAaPmAgMnP	nasadit
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
hráčku	hráčka	k1gFnSc4	hráčka
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
Capriatiovou	Capriatiový	k2eAgFnSc7d1	Capriatiová
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tentokrát	tentokrát	k6eAd1	tentokrát
přehrála	přehrát	k5eAaPmAgFnS	přehrát
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
,	,	kIx,	,
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Amélii	Amélie	k1gFnSc4	Amélie
Mauresmovou	Mauresmový	k2eAgFnSc4d1	Mauresmový
po	po	k7c6	po
těžkém	těžký	k2eAgInSc6d1	těžký
boji	boj	k1gInSc6	boj
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sadě	sada	k1gFnSc6	sada
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
již	již	k9	již
soupeřka	soupeřka	k1gFnSc1	soupeřka
měla	mít	k5eAaImAgFnS	mít
break	break	k1gInSc4	break
a	a	k8xC	a
podržením	podržení	k1gNnSc7	podržení
servisu	servis	k1gInSc2	servis
mohla	moct	k5eAaImAgFnS	moct
dovést	dovést	k5eAaPmF	dovést
zápas	zápas	k1gInSc4	zápas
do	do	k7c2	do
vítězného	vítězný	k2eAgInSc2d1	vítězný
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejpřekvapivějších	překvapivý	k2eAgMnPc2d3	nejpřekvapivější
utkání	utkání	k1gNnSc6	utkání
celé	celý	k2eAgFnSc2d1	celá
wimbledonské	wimbledonský	k2eAgFnSc2d1	wimbledonská
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
když	když	k8xS	když
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
třinácté	třináctý	k4xOgFnSc6	třináctý
nasazené	nasazený	k2eAgFnSc6d1	nasazená
mladé	mladý	k2eAgFnSc6d1	mladá
Rusce	Ruska	k1gFnSc6	Ruska
Šarapovové	Šarapovové	k2eAgFnSc1d1	Šarapovové
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
porážka	porážka	k1gFnSc1	porážka
ji	on	k3xPp3gFnSc4	on
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
desítky	desítka	k1gFnSc2	desítka
tenistek	tenistka	k1gFnPc2	tenistka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
finále	finále	k1gNnSc1	finále
sezóny	sezóna	k1gFnSc2	sezóna
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
na	na	k7c6	na
losangeleských	losangeleský	k2eAgInPc6d1	losangeleský
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
dvorcích	dvorec	k1gInPc6	dvorec
turnaje	turnaj	k1gInSc2	turnaj
JPMorgan	JPMorgan	k1gInSc4	JPMorgan
Chase	chasa	k1gFnSc3	chasa
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Lindsay	Lindsaa	k1gFnPc4	Lindsaa
Davenportovou	Davenportová	k1gFnSc4	Davenportová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
porážka	porážka	k1gFnSc1	porážka
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
krajankou	krajanka	k1gFnSc7	krajanka
od	od	k7c2	od
US	US	kA	US
Open	Open	k1gInSc4	Open
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
události	událost	k1gFnPc4	událost
Acura	Acuro	k1gNnSc2	Acuro
Classic	Classice	k1gInPc2	Classice
v	v	k7c6	v
San	San	k1gFnSc6	San
Diegu	Dieg	k1gInSc2	Dieg
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
před	před	k7c4	před
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
kvůli	kvůli	k7c3	kvůli
poranění	poranění	k1gNnSc3	poranění
levého	levý	k2eAgInSc2d1	levý
kolene	kolen	k1gMnSc5	kolen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ji	on	k3xPp3gFnSc4	on
neumožnilo	umožnit	k5eNaPmAgNnS	umožnit
start	start	k1gInSc4	start
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
turnajích	turnaj	k1gInPc6	turnaj
kategorií	kategorie	k1gFnPc2	kategorie
Tier	Tier	k1gInSc1	Tier
I	i	k8xC	i
Rogers	Rogers	k1gInSc1	Rogers
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
a	a	k8xC	a
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2004	[number]	k4	2004
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
.	.	kIx.	.
</s>
<s>
Vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
až	až	k9	až
třetím	třetí	k4xOgInSc7	třetí
grandslamem	grandslam	k1gInSc7	grandslam
US	US	kA	US
Open	Open	k1gNnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nasazená	nasazený	k2eAgFnSc1d1	nasazená
jako	jako	k8xS	jako
hráčka	hráčka	k1gFnSc1	hráčka
číslo	číslo	k1gNnSc1	číslo
tři	tři	k4xCgNnPc1	tři
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
figurovala	figurovat	k5eAaImAgFnS	figurovat
až	až	k6eAd1	až
na	na	k7c6	na
jedenáctém	jedenáctý	k4xOgInSc6	jedenáctý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
potkala	potkat	k5eAaPmAgFnS	potkat
s	s	k7c7	s
osmou	osmý	k4xOgFnSc7	osmý
hráčkou	hráčka	k1gFnSc7	hráčka
světa	svět	k1gInSc2	svět
Capriatiovou	Capriatiový	k2eAgFnSc7d1	Capriatiová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
prohrála	prohrát	k5eAaPmAgFnS	prohrát
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
zápas	zápas	k1gInSc4	zápas
provázelo	provázet	k5eAaImAgNnS	provázet
několik	několik	k4yIc1	několik
chybných	chybný	k2eAgInPc2d1	chybný
výroků	výrok	k1gInPc2	výrok
čárových	čárový	k2eAgMnPc2d1	čárový
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jednoho	jeden	k4xCgInSc2	jeden
sporného	sporný	k2eAgInSc2d1	sporný
výroku	výrok	k1gInSc2	výrok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
zákazu	zákaz	k1gInSc3	zákaz
rozhodování	rozhodování	k1gNnSc2	rozhodování
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
turnaje	turnaj	k1gInSc2	turnaj
pro	pro	k7c4	pro
hlavního	hlavní	k2eAgMnSc4d1	hlavní
rozhodčího	rozhodčí	k1gMnSc4	rozhodčí
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
popudů	popud	k1gInPc2	popud
pro	pro	k7c4	pro
zavedení	zavedení	k1gNnSc4	zavedení
jestřábího	jestřábí	k2eAgNnSc2d1	jestřábí
oka	oko	k1gNnSc2	oko
do	do	k7c2	do
profesionálního	profesionální	k2eAgInSc2d1	profesionální
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
odehrála	odehrát	k5eAaPmAgFnS	odehrát
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
titul	titul	k1gInSc4	titul
sezóny	sezóna	k1gFnSc2	sezóna
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
na	na	k7c6	na
China	China	k1gFnSc1	China
Open	Open	k1gNnSc4	Open
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
přehrála	přehrát	k5eAaPmAgFnS	přehrát
aktuální	aktuální	k2eAgFnSc4d1	aktuální
vítězku	vítězka	k1gFnSc4	vítězka
US	US	kA	US
Open	Open	k1gMnSc1	Open
Světlanu	Světlana	k1gFnSc4	Světlana
Kuzněcovovou	Kuzněcovová	k1gFnSc4	Kuzněcovová
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
poté	poté	k6eAd1	poté
odehrála	odehrát	k5eAaPmAgFnS	odehrát
linecký	linecký	k2eAgInSc4d1	linecký
turnaj	turnaj	k1gInSc4	turnaj
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
již	již	k9	již
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
sedmdesátou	sedmdesátý	k4xOgFnSc4	sedmdesátý
třetí	třetí	k4xOgFnSc7	třetí
tenistkou	tenistka	k1gFnSc7	tenistka
žebříčku	žebříček	k1gInSc2	žebříček
Alinou	Aliný	k2eAgFnSc7d1	Aliný
Židkovovou	Židkovová	k1gFnSc7	Židkovová
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
na	na	k7c4	na
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
událost	událost	k1gFnSc4	událost
sezóny	sezóna	k1gFnSc2	sezóna
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
Championships	Championships	k1gInSc1	Championships
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Robinově	Robinův	k2eAgFnSc6d1	Robinova
skupině	skupina	k1gFnSc6	skupina
Turnaje	turnaj	k1gInSc2	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
přehrála	přehrát	k5eAaPmAgFnS	přehrát
světovou	světový	k2eAgFnSc4d1	světová
pětku	pětka	k1gFnSc4	pětka
Dementěvovou	Dementěvový	k2eAgFnSc4d1	Dementěvový
<g/>
,	,	kIx,	,
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
jedničce	jednička	k1gFnSc3	jednička
Davenportové	Davenportová	k1gFnSc3	Davenportová
a	a	k8xC	a
porazila	porazit	k5eAaPmAgFnS	porazit
trojku	trojka	k1gFnSc4	trojka
Anastasii	Anastasie	k1gFnSc4	Anastasie
Myskinovou	Myskinová	k1gFnSc4	Myskinová
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
nad	nad	k7c7	nad
dvojkou	dvojka	k1gFnSc7	dvojka
Mauresmovou	Mauresmová	k1gFnSc7	Mauresmová
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
setjně	setjně	k6eAd1	setjně
jako	jako	k9	jako
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc7	její
finálovou	finálový	k2eAgFnSc7d1	finálová
přemožitelkou	přemožitelka	k1gFnSc7	přemožitelka
stala	stát	k5eAaPmAgFnS	stát
šestka	šestka	k1gFnSc1	šestka
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
stavu	stav	k1gInSc2	stav
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sadě	sada	k1gFnSc6	sada
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
přestávku	přestávka	k1gFnSc4	přestávka
na	na	k7c4	na
ošetření	ošetření	k1gNnSc4	ošetření
poranění	poranění	k1gNnSc2	poranění
břišního	břišní	k2eAgNnSc2d1	břišní
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ji	on	k3xPp3gFnSc4	on
omezovalo	omezovat	k5eAaImAgNnS	omezovat
zejména	zejména	k9	zejména
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
prvního	první	k4xOgInSc2	první
servisu	servis	k1gInSc2	servis
činila	činit	k5eAaImAgFnS	činit
pouze	pouze	k6eAd1	pouze
něco	něco	k3yInSc1	něco
okolo	okolo	k7c2	okolo
65	[number]	k4	65
mph	mph	k?	mph
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
setu	set	k1gInSc6	set
nad	nad	k7c7	nad
soupeřkou	soupeřka	k1gFnSc7	soupeřka
přesto	přesto	k8xC	přesto
vedla	vést	k5eAaImAgFnS	vést
již	již	k6eAd1	již
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
na	na	k7c4	na
gamy	game	k1gInPc4	game
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
ovšem	ovšem	k9	ovšem
následoval	následovat	k5eAaImAgInS	následovat
zvrat	zvrat	k1gInSc1	zvrat
<g/>
,	,	kIx,	,
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
získala	získat	k5eAaPmAgFnS	získat
šest	šest	k4xCc4	šest
her	hra	k1gFnPc2	hra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
utkání	utkání	k1gNnSc4	utkání
roku	rok	k1gInSc2	rok
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
byla	být	k5eAaImAgFnS	být
klasifikována	klasifikovat	k5eAaImNgFnS	klasifikovat
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
a	a	k8xC	a
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
žádný	žádný	k3yNgInSc4	žádný
grandslam	grandslam	k1gInSc4	grandslam
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Venus	Venus	k1gInSc1	Venus
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
vyvrátila	vyvrátit	k5eAaPmAgFnS	vyvrátit
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
sestry	sestra	k1gFnPc1	sestra
Williamsovy	Williamsův	k2eAgFnPc1d1	Williamsova
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
na	na	k7c6	na
ústupu	ústup	k1gInSc2	ústup
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Mauresmovou	Mauresmová	k1gFnSc4	Mauresmová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
se	s	k7c7	s
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Šarapovovou	Šarapovová	k1gFnSc7	Šarapovová
po	po	k7c6	po
boji	boj	k1gInSc6	boj
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
když	když	k8xS	když
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
tři	tři	k4xCgInPc4	tři
mečboly	mečbol	k1gInPc4	mečbol
Rusky	Ruska	k1gFnSc2	Ruska
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
ji	on	k3xPp3gFnSc4	on
čekala	čekat	k5eAaImAgFnS	čekat
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
Davenportová	Davenportová	k1gFnSc1	Davenportová
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
kterou	který	k3yRgFnSc4	který
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
a	a	k8xC	a
připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
dvouhře	dvouhra	k1gFnSc6	dvouhra
druhý	druhý	k4xOgInSc1	druhý
titul	titul	k1gInSc1	titul
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
grandslamu	grandslam	k1gInSc2	grandslam
a	a	k8xC	a
sedmý	sedmý	k4xOgInSc4	sedmý
celkově	celkově	k6eAd1	celkově
<g/>
.	.	kIx.	.
</s>
<s>
Výhra	výhra	k1gFnSc1	výhra
ji	on	k3xPp3gFnSc4	on
katapultovala	katapultovat	k5eAaBmAgFnS	katapultovat
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc4	pozice
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
znovu	znovu	k6eAd1	znovu
usednut	usednut	k2eAgInSc1d1	usednut
na	na	k7c4	na
světový	světový	k2eAgInSc4d1	světový
tenisový	tenisový	k2eAgInSc4d1	tenisový
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
pěti	pět	k4xCc6	pět
turnajích	turnaj	k1gInPc6	turnaj
se	se	k3xPyFc4	se
neprobojovala	probojovat	k5eNaPmAgFnS	probojovat
ani	ani	k9	ani
jednou	jeden	k4xCgFnSc7	jeden
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížském	pařížský	k2eAgNnSc6d1	pařížské
Open	Open	k1gNnSc1	Open
Gaz	Gaz	k1gMnSc2	Gaz
de	de	k?	de
France	Franc	k1gMnSc2	Franc
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
před	před	k7c4	před
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
kvůli	kvůli	k7c3	kvůli
žaludečním	žaludeční	k2eAgFnPc3d1	žaludeční
potížím	potíž	k1gFnPc3	potíž
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
skrečovala	skrečovat	k5eAaPmAgFnS	skrečovat
semifinále	semifinále	k1gNnSc4	semifinále
s	s	k7c7	s
Jelenou	Jelena	k1gFnSc7	Jelena
Jankovićovou	Jankovićová	k1gFnSc7	Jankovićová
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Dubai	Duba	k1gFnSc2	Duba
Duty	Duty	k?	Duty
Free	Free	k1gInSc1	Free
Women	Women	k1gInSc1	Women
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Open	Open	k1gInSc1	Open
pro	pro	k7c4	pro
nataženou	natažený	k2eAgFnSc4d1	natažená
šlachu	šlacha	k1gFnSc4	šlacha
v	v	k7c6	v
pravém	pravý	k2eAgNnSc6d1	pravé
rameni	rameno	k1gNnSc6	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInSc4d1	další
měsíc	měsíc	k1gInSc4	měsíc
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
kategorie	kategorie	k1gFnSc2	kategorie
Tier	Tiera	k1gFnPc2	Tiera
I	i	k8xC	i
turnaje	turnaj	k1gInSc2	turnaj
NASDAQ-100	NASDAQ-100	k1gMnSc1	NASDAQ-100
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
starší	starý	k2eAgFnSc3d2	starší
sestře	sestra	k1gFnSc3	sestra
Venus	Venus	k1gInSc4	Venus
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
ní	on	k3xPp3gFnSc6	on
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
týden	týden	k1gInSc4	týden
ji	on	k3xPp3gFnSc4	on
zraněný	zraněný	k2eAgInSc4d1	zraněný
levý	levý	k2eAgInSc4d1	levý
kotník	kotník	k1gInSc4	kotník
přinutil	přinutit	k5eAaPmAgInS	přinutit
odstoupit	odstoupit	k5eAaPmF	odstoupit
ze	z	k7c2	z
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
antukové	antukový	k2eAgFnSc2d1	antuková
události	událost	k1gFnSc2	událost
Bausch	Bausch	k1gMnSc1	Bausch
&	&	k?	&
Lomb	Lomb	k1gInSc1	Lomb
Championships	Championshipsa	k1gFnPc2	Championshipsa
v	v	k7c4	v
Amelia	Amelius	k1gMnSc4	Amelius
Island	Island	k1gInSc4	Island
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
poté	poté	k6eAd1	poté
stále	stále	k6eAd1	stále
nezlepšila	zlepšit	k5eNaPmAgFnS	zlepšit
svůj	svůj	k3xOyFgInSc4	svůj
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
kategorie	kategorie	k1gFnSc2	kategorie
Tier	Tiera	k1gFnPc2	Tiera
I	i	k8xC	i
římského	římský	k2eAgMnSc2d1	římský
Internazionali	Internazionali	k1gMnSc2	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italia	k1gFnSc1	Italia
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
s	s	k7c7	s
domácí	domácí	k2eAgFnSc7d1	domácí
Francescou	Francesca	k1gFnSc7	Francesca
Schiavoneovou	Schiavoneův	k2eAgFnSc7d1	Schiavoneův
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zranění	zranění	k1gNnSc1	zranění
kotníku	kotník	k1gInSc2	kotník
ji	on	k3xPp3gFnSc4	on
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
grandslamu	grandslam	k1gInSc6	grandslam
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
<g/>
Vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
ani	ani	k8xC	ani
na	na	k7c6	na
nejslavnějším	slavný	k2eAgInSc6d3	nejslavnější
turnaji	turnaj	k1gInSc6	turnaj
světa	svět	k1gInSc2	svět
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
dvě	dva	k4xCgNnPc1	dva
kola	kolo	k1gNnPc1	kolo
vybojovala	vybojovat	k5eAaPmAgNnP	vybojovat
v	v	k7c6	v
třísetových	třísetový	k2eAgInPc6d1	třísetový
zápasech	zápas	k1gInPc6	zápas
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgMnSc6	třetí
již	již	k6eAd1	již
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
osmdesátou	osmdesátý	k4xOgFnSc4	osmdesátý
pátou	pátá	k1gFnSc4	pátá
tenistku	tenistka	k1gFnSc4	tenistka
světa	svět	k1gInSc2	svět
Jill	Jilla	k1gFnPc2	Jilla
Craybasovou	Craybasová	k1gFnSc4	Craybasová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
na	na	k7c4	na
Tier	Tier	k1gInSc4	Tier
I	i	k8xC	i
Rogers	Rogers	k1gInSc4	Rogers
Cupu	cup	k1gInSc2	cup
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ozvalo	ozvat	k5eAaPmAgNnS	ozvat
zranění	zranění	k1gNnSc4	zranění
levého	levý	k2eAgInSc2d1	levý
kolene	kolen	k1gMnSc5	kolen
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
její	její	k3xOp3gNnSc1	její
odhlášení	odhlášení	k1gNnSc1	odhlášení
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
grandslamu	grandslam	k1gInSc6	grandslam
roku	rok	k1gInSc2	rok
US	US	kA	US
Open	Open	k1gMnSc1	Open
nestačila	stačit	k5eNaBmAgFnS	stačit
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
na	na	k7c4	na
sestru	sestra	k1gFnSc4	sestra
Venus	Venus	k1gInSc4	Venus
a	a	k8xC	a
prohrála	prohrát	k5eAaPmAgFnS	prohrát
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
spolu	spolu	k6eAd1	spolu
sehrály	sehrát	k5eAaPmAgFnP	sehrát
v	v	k7c6	v
nejranější	raný	k2eAgFnSc6d3	nejranější
fázi	fáze	k1gFnSc6	fáze
(	(	kIx(	(
<g/>
osmifinále	osmifinále	k1gNnSc6	osmifinále
<g/>
)	)	kIx)	)
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
grandslamu	grandslam	k1gInSc2	grandslam
od	od	k7c2	od
jejich	jejich	k3xOp3gNnSc2	jejich
prvního	první	k4xOgNnSc2	první
utkání	utkání	k1gNnSc2	utkání
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
pak	pak	k6eAd1	pak
odehrála	odehrát	k5eAaPmAgFnS	odehrát
pouze	pouze	k6eAd1	pouze
jediné	jediný	k2eAgNnSc4d1	jediné
utkání	utkání	k1gNnSc4	utkání
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
pekingského	pekingský	k2eAgInSc2d1	pekingský
China	China	k1gFnSc1	China
Open	Open	k1gInSc1	Open
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
dvacátou	dvacátý	k4xOgFnSc4	dvacátý
sedmou	sedmý	k4xOgFnSc4	sedmý
tenistku	tenistka	k1gFnSc4	tenistka
žebříčku	žebříček	k1gInSc2	žebříček
Sun	Sun	kA	Sun
Tchien-tchien	Tchienchien	k1gInSc1	Tchien-tchien
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
nekvalifikovala	kvalifikovat	k5eNaBmAgFnS	kvalifikovat
na	na	k7c4	na
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
Turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
hráčky	hráčka	k1gFnPc4	hráčka
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
premiérově	premiérově	k6eAd1	premiérově
mimo	mimo	k7c4	mimo
elitní	elitní	k2eAgFnSc4d1	elitní
desítku	desítka	k1gFnSc4	desítka
žebříčku	žebříček	k1gInSc2	žebříček
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
nenastoupila	nastoupit	k5eNaPmAgFnS	nastoupit
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
přípravnému	přípravný	k2eAgInSc3d1	přípravný
turnaji	turnaj	k1gInSc3	turnaj
před	před	k7c4	před
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
grandslamu	grandslam	k1gInSc6	grandslam
hrála	hrát	k5eAaImAgFnS	hrát
jako	jako	k9	jako
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
sedmnáctou	sedmnáctý	k4xOgFnSc7	sedmnáctý
hráčkou	hráčka	k1gFnSc7	hráčka
Slovenkou	Slovenka	k1gFnSc7	Slovenka
Danielou	Daniela	k1gFnSc7	Daniela
Hantuchovou	Hantuchův	k2eAgFnSc7d1	Hantuchova
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
z	z	k7c2	z
tokijského	tokijský	k2eAgMnSc2d1	tokijský
Toray	Toraa	k1gMnSc2	Toraa
Pan	Pan	k1gMnSc1	Pan
Pacific	Pacific	k1gMnSc1	Pacific
Open	Open	k1gMnSc1	Open
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
důvod	důvod	k1gInSc4	důvod
uvedla	uvést	k5eAaPmAgFnS	uvést
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
kondici	kondice	k1gFnSc4	kondice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
dubajského	dubajský	k2eAgNnSc2d1	dubajské
Dubai	Dubai	k1gNnSc2	Dubai
Duty	Duty	k?	Duty
Free	Free	k1gNnSc2	Free
Women	Womna	k1gFnPc2	Womna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
Tier	Tier	k1gMnSc1	Tier
I	i	k8xC	i
NASDAQ-100	NASDAQ-100	k1gMnSc1	NASDAQ-100
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
z	z	k7c2	z
první	první	k4xOgFnSc2	první
stovky	stovka	k1gFnSc2	stovka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
(	(	kIx(	(
<g/>
TOP	topit	k5eAaImRp2nS	topit
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vydala	vydat	k5eAaPmAgFnS	vydat
prohlášení	prohlášení	k1gNnSc3	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
protrahovanému	protrahovaný	k2eAgNnSc3d1	protrahované
zranění	zranění	k1gNnSc3	zranění
kolene	kolen	k1gInSc5	kolen
vynechá	vynechat	k5eAaPmIp3nS	vynechat
dva	dva	k4xCgInPc4	dva
grandslamy	grandslam	k1gInPc4	grandslam
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
a	a	k8xC	a
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
lékařů	lékař	k1gMnPc2	lékař
bude	být	k5eAaImBp3nS	být
mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
až	až	k6eAd1	až
"	"	kIx"	"
<g/>
do	do	k7c2	do
konce	konec	k1gInSc2	konec
léta	léto	k1gNnSc2	léto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
vrátila	vrátit	k5eAaPmAgFnS	vrátit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
turnajem	turnaj	k1gInSc7	turnaj
Western	Western	kA	Western
&	&	k?	&
Southern	Southern	k1gMnSc1	Southern
Financial	Financial	k1gMnSc1	Financial
Group	Group	k1gMnSc1	Group
Women	Women	k2eAgMnSc1d1	Women
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Open	Opena	k1gFnPc2	Opena
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
herní	herní	k2eAgFnSc3d1	herní
neaktivitě	neaktivita	k1gFnSc3	neaktivita
klesla	klesnout	k5eAaPmAgFnS	klesnout
až	až	k6eAd1	až
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
třicátou	třicátý	k4xOgFnSc4	třicátý
devátou	devátý	k4xOgFnSc4	devátý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
hladce	hladko	k6eAd1	hladko
porazila	porazit	k5eAaPmAgFnS	porazit
světovou	světový	k2eAgFnSc4d1	světová
jedenáctku	jedenáctka	k1gFnSc4	jedenáctka
Myskinovou	Myskinový	k2eAgFnSc4d1	Myskinový
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Došla	dojít	k5eAaPmAgFnS	dojít
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
vítězku	vítězka	k1gFnSc4	vítězka
Věru	Věra	k1gFnSc4	Věra
Zvonarevovou	Zvonarevová	k1gFnSc4	Zvonarevová
<g/>
.	.	kIx.	.
</s>
<s>
Semifinálové	semifinálový	k2eAgFnSc2d1	semifinálová
účasti	účast	k1gFnSc2	účast
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
také	také	k9	také
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
dvacáté	dvacátý	k4xOgFnSc6	dvacátý
osmé	osmý	k4xOgFnSc6	osmý
hráčce	hráčka	k1gFnSc6	hráčka
světa	svět	k1gInSc2	svět
Jankovićové	Jankovićová	k1gFnSc2	Jankovićová
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
jako	jako	k9	jako
nenasazená	nasazený	k2eNgFnSc1d1	nenasazená
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
a	a	k8xC	a
pořadatelé	pořadatel	k1gMnPc1	pořadatel
jí	on	k3xPp3gFnSc7	on
museli	muset	k5eAaImAgMnP	muset
udělit	udělit	k5eAaPmF	udělit
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
žebříčkové	žebříčkový	k2eAgNnSc1d1	žebříčkové
umístění	umístění	k1gNnSc1	umístění
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
nízké	nízký	k2eAgInPc4d1	nízký
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
sedmnáctou	sedmnáctý	k4xOgFnSc4	sedmnáctý
hráčku	hráčka	k1gFnSc4	hráčka
Srbku	Srbka	k1gFnSc4	Srbka
Anu	Anu	k1gFnSc4	Anu
Ivanovićovou	Ivanovićová	k1gFnSc4	Ivanovićová
<g/>
,	,	kIx,	,
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
pak	pak	k6eAd1	pak
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
nejvýše	vysoce	k6eAd3	vysoce
nasazené	nasazený	k2eAgFnSc6d1	nasazená
Mauresmové	Mauresmová	k1gFnSc3	Mauresmová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gInSc1	její
poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
na	na	k7c4	na
95	[number]	k4	95
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc4	pozice
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
sezónu	sezóna	k1gFnSc4	sezóna
odehrála	odehrát	k5eAaPmAgFnS	odehrát
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
===	===	k?	===
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2007	[number]	k4	2007
zahájila	zahájit	k5eAaPmAgNnP	zahájit
s	s	k7c7	s
obnovenou	obnovený	k2eAgFnSc7d1	obnovená
důvěrou	důvěra	k1gFnSc7	důvěra
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
záměr	záměr	k1gInSc4	záměr
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
tenistek	tenistka	k1gFnPc2	tenistka
světa	svět	k1gInSc2	svět
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
wimbledonský	wimbledonský	k2eAgMnSc1d1	wimbledonský
vítěz	vítěz	k1gMnSc1	vítěz
a	a	k8xC	a
sportovní	sportovní	k2eAgMnSc1d1	sportovní
komentátor	komentátor	k1gMnSc1	komentátor
Pat	pat	k1gInSc4	pat
Cash	cash	k1gFnSc2	cash
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
klamnou	klamný	k2eAgFnSc4d1	klamná
představu	představa	k1gFnSc4	představa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Moorilla	Moorillo	k1gNnSc2	Moorillo
Hobart	Hobart	k1gInSc1	Hobart
International	International	k1gFnSc4	International
v	v	k7c6	v
Hobartu	Hobart	k1gInSc6	Hobart
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prvního	první	k4xOgInSc2	první
grandslamu	grandslam	k1gInSc2	grandslam
roku	rok	k1gInSc2	rok
Australian	Australiana	k1gFnPc2	Australiana
Open	Openo	k1gNnPc2	Openo
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
jako	jako	k9	jako
nenasazená	nasazený	k2eNgFnSc1d1	nenasazená
hráčka	hráčka	k1gFnSc1	hráčka
z	z	k7c2	z
osmdesáté	osmdesátý	k4xOgFnPc4	osmdesátý
první	první	k4xOgFnPc4	první
pozice	pozice	k1gFnPc4	pozice
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
bylo	být	k5eAaImAgNnS	být
předpovídáno	předpovídán	k2eAgNnSc1d1	předpovídáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
mimo	mimo	k7c4	mimo
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
pátou	pátá	k1gFnSc7	pátá
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Naděždou	Naděžda	k1gFnSc7	Naděžda
Petrovovou	Petrovový	k2eAgFnSc7d1	Petrovová
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
hráčkou	hráčka	k1gFnSc7	hráčka
elitní	elitní	k2eAgFnSc2d1	elitní
desítky	desítka	k1gFnSc2	desítka
od	od	k7c2	od
výhry	výhra	k1gFnSc2	výhra
s	s	k7c7	s
Lindsay	Lindsay	k1gInPc7	Lindsay
Davenportovou	Davenportová	k1gFnSc4	Davenportová
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Australian	Australiany	k1gInPc2	Australiany
Open	Open	k1gInSc1	Open
2005	[number]	k4	2005
před	před	k7c7	před
dvěma	dva	k4xCgInPc7	dva
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
byla	být	k5eAaImAgFnS	být
dva	dva	k4xCgInPc4	dva
míče	míč	k1gInPc4	míč
od	od	k7c2	od
vyřazení	vyřazení	k1gNnSc2	vyřazení
se	se	k3xPyFc4	se
Šachar	Šachar	k1gInSc1	Šachar
Pe	Pe	k1gFnSc2	Pe
<g/>
'	'	kIx"	'
<g/>
erovou	erovat	k5eAaPmIp3nP	erovat
<g/>
,	,	kIx,	,
než	než	k8xS	než
zápas	zápas	k1gInSc4	zápas
otočila	otočit	k5eAaPmAgFnS	otočit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
zdolala	zdolat	k5eAaPmAgFnS	zdolat
nejvýše	vysoce	k6eAd3	vysoce
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Marii	Maria	k1gFnSc4	Maria
Šarapovou	Šarapový	k2eAgFnSc4d1	Šarapový
hladce	hladko	k6eAd1	hladko
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyvrátila	vyvrátit	k5eAaPmAgFnS	vyvrátit
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
nemožnosti	nemožnost	k1gFnSc6	nemožnost
návratu	návrat	k1gInSc2	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
třetí	třetí	k4xOgInSc4	třetí
titul	titul	k1gInSc4	titul
z	z	k7c2	z
Melbourne	Melbourne	k1gNnSc2	Melbourne
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
osmý	osmý	k4xOgInSc4	osmý
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
v	v	k7c6	v
singlu	singl	k1gInSc6	singl
<g/>
.	.	kIx.	.
</s>
<s>
Věnovala	věnovat	k5eAaPmAgFnS	věnovat
jej	on	k3xPp3gInSc4	on
své	svůj	k3xOyFgFnSc6	svůj
zavražděné	zavražděný	k2eAgFnSc6d1	zavražděná
sestře	sestra	k1gFnSc6	sestra
Yetunde	Yetund	k1gMnSc5	Yetund
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
finálové	finálový	k2eAgNnSc4d1	finálové
vystoupení	vystoupení	k1gNnSc4	vystoupení
označil	označit	k5eAaPmAgInS	označit
magazín	magazín	k1gInSc1	magazín
TENNIS	TENNIS	kA	TENNIS
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
za	za	k7c4	za
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
výkonů	výkon	k1gInPc2	výkon
její	její	k3xOp3gFnSc2	její
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
"	"	kIx"	"
a	a	k8xC	a
BBC	BBC	kA	BBC
Sport	sport	k1gInSc1	sport
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejsilovější	silový	k2eAgNnSc4d3	silový
pojetí	pojetí	k1gNnSc4	pojetí
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
tenise	tenis	k1gInSc6	tenis
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Příští	příští	k2eAgFnSc7d1	příští
událostí	událost	k1gFnSc7	událost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
turnaj	turnaj	k1gInSc1	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
WTA	WTA	kA	WTA
Tier	Tier	k1gInSc1	Tier
I	i	k8xC	i
floridský	floridský	k2eAgInSc1d1	floridský
Miami	Miami	k1gNnSc7	Miami
Masters	Mastersa	k1gFnPc2	Mastersa
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
výhru	výhra	k1gFnSc4	výhra
nad	nad	k7c7	nad
světovou	světový	k2eAgFnSc7d1	světová
dvojkou	dvojka	k1gFnSc7	dvojka
Šarapovovou	Šarapovová	k1gFnSc7	Šarapovová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
jedničkou	jednička	k1gFnSc7	jednička
Justine	Justin	k1gMnSc5	Justin
Heninovou	Heninová	k1gFnSc4	Heninová
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
porazila	porazit	k5eAaPmAgFnS	porazit
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
musela	muset	k5eAaImAgFnS	muset
odvracet	odvracet	k5eAaImF	odvracet
mečbol	mečbol	k1gInSc4	mečbol
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
<g/>
..	..	k?	..
<g/>
Na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
turnaji	turnaj	k1gInSc6	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
Tier	Tiera	k1gFnPc2	Tiera
I	i	k9	i
Family	Famil	k1gInPc1	Famil
Circle	Circle	k1gFnSc1	Circle
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
hraném	hraný	k2eAgInSc6d1	hraný
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
skrečovala	skrečovat	k5eAaPmAgFnS	skrečovat
utkání	utkání	k1gNnSc2	utkání
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
pro	pro	k7c4	pro
natažené	natažený	k2eAgNnSc4d1	natažené
tříslo	tříslo	k1gNnSc4	tříslo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
svou	svůj	k3xOyFgFnSc4	svůj
premiérovou	premiérový	k2eAgFnSc4d1	premiérová
dvouhru	dvouhra	k1gFnSc4	dvouhra
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
belgiskému	belgiský	k2eAgNnSc3d1	belgiský
družstvu	družstvo	k1gNnSc3	družstvo
hraného	hraný	k2eAgNnSc2d1	hrané
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
singlu	singl	k1gInSc3	singl
již	již	k6eAd1	již
nenastoupila	nastoupit	k5eNaPmAgFnS	nastoupit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
příliš	příliš	k6eAd1	příliš
nenamáhala	namáhat	k5eNaImAgFnS	namáhat
koleno	koleno	k1gNnSc4	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhým	druhý	k4xOgInSc7	druhý
grandslamem	grandslam	k1gInSc7	grandslam
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
evropské	evropský	k2eAgFnSc2d1	Evropská
přípravy	příprava	k1gFnSc2	příprava
pouze	pouze	k6eAd1	pouze
jediný	jediný	k2eAgInSc1d1	jediný
antukový	antukový	k2eAgInSc1d1	antukový
turnaj	turnaj	k1gInSc1	turnaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
Tier	Tier	k1gInSc1	Tier
I	i	k8xC	i
Internazionali	Internazionali	k1gFnSc1	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došla	dojít	k5eAaPmAgFnS	dojít
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
prohrála	prohrát	k5eAaPmAgFnS	prohrát
se	s	k7c7	s
čtrnáctou	čtrnáctý	k4xOgFnSc7	čtrnáctý
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Švýcarkou	Švýcarka	k1gFnSc7	Švýcarka
Patty	Patta	k1gFnSc2	Patta
Schnyderovou	Schnyderová	k1gFnSc4	Schnyderová
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jí	jíst	k5eAaImIp3nS	jíst
tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
stačil	stačit	k5eAaBmAgInS	stačit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gInSc4	Open
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
jako	jako	k9	jako
osmá	osmý	k4xOgFnSc1	osmý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
však	však	k9	však
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
přejít	přejít	k5eAaPmF	přejít
přes	přes	k7c4	přes
příští	příští	k2eAgFnSc4d1	příští
vítězku	vítězka	k1gFnSc4	vítězka
Heninovou	Heninový	k2eAgFnSc4d1	Heninová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
výkon	výkon	k1gInSc4	výkon
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
odporný	odporný	k2eAgInSc4d1	odporný
a	a	k8xC	a
příšerný	příšerný	k2eAgInSc4d1	příšerný
<g/>
"	"	kIx"	"
a	a	k8xC	a
horší	zlý	k2eAgMnSc1d2	horší
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
zahanbená	zahanbený	k2eAgFnSc1d1	zahanbená
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
porážku	porážka	k1gFnSc4	porážka
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
favoritkám	favoritka	k1gFnPc3	favoritka
dalšího	další	k2eAgInSc2d1	další
grandslamu	grandslam	k1gInSc2	grandslam
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
kola	kolo	k1gNnSc2	kolo
proti	proti	k7c3	proti
Daniele	Daniela	k1gFnSc3	Daniela
Hantuchové	Hantuchový	k2eAgFnSc2d1	Hantuchová
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
upadla	upadnout	k5eAaPmAgFnS	upadnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
postihla	postihnout	k5eAaPmAgFnS	postihnout
svalová	svalový	k2eAgFnSc1d1	svalová
křeč	křeč	k1gFnSc1	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přestávce	přestávka	k1gFnSc6	přestávka
na	na	k7c4	na
ošetření	ošetření	k1gNnSc4	ošetření
si	se	k3xPyFc3	se
podržela	podržet	k5eAaPmAgFnS	podržet
svůj	svůj	k3xOyFgInSc4	svůj
servis	servis	k1gInSc4	servis
a	a	k8xC	a
sadu	sada	k1gFnSc4	sada
dovedla	dovést	k5eAaPmAgFnS	dovést
do	do	k7c2	do
tiebreaku	tiebreak	k1gInSc2	tiebreak
<g/>
.	.	kIx.	.
</s>
<s>
Déšť	déšť	k1gInSc4	déšť
však	však	k9	však
utkání	utkání	k1gNnSc4	utkání
přerušil	přerušit	k5eAaPmAgMnS	přerušit
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
hry	hra	k1gFnSc2	hra
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zvítězit	zvítězit	k5eAaPmF	zvítězit
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgNnSc6d1	následné
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
však	však	k9	však
opět	opět	k6eAd1	opět
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Heninovou	Heninový	k2eAgFnSc4d1	Heninová
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zápasu	zápas	k1gInSc3	zápas
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
bandáží	bandáž	k1gFnSc7	bandáž
lýtka	lýtko	k1gNnSc2	lýtko
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
donucena	donucen	k2eAgFnSc1d1	donucena
hrát	hrát	k5eAaImF	hrát
pomalý	pomalý	k2eAgInSc4d1	pomalý
slajsovaný	slajsovaný	k2eAgInSc4d1	slajsovaný
jednoručný	jednoručný	k2eAgInSc4d1	jednoručný
bekhend	bekhend	k1gInSc4	bekhend
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
limitována	limitovat	k5eAaBmNgFnS	limitovat
poraněním	poranění	k1gNnSc7	poranění
palce	palec	k1gInSc2	palec
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dostalo	dostat	k5eAaPmAgNnS	dostat
kritiky	kritika	k1gFnPc4	kritika
za	za	k7c4	za
nesportovní	sportovní	k2eNgNnSc4d1	nesportovní
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Heninovou	Heninová	k1gFnSc4	Heninová
porazila	porazit	k5eAaPmAgFnS	porazit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
zdravá	zdravá	k1gFnSc1	zdravá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
grandslamu	grandslam	k1gInSc6	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
posunula	posunout	k5eAaPmAgFnS	posunout
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
příčky	příčka	k1gFnPc4	příčka
výše	vysoce	k6eAd2	vysoce
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
umístění	umístění	k1gNnSc4	umístění
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
poranění	poranění	k1gNnSc3	poranění
levého	levý	k2eAgInSc2d1	levý
palce	palec	k1gInSc2	palec
odřekla	odřeknout	k5eAaPmAgFnS	odřeknout
turnaje	turnaj	k1gInPc4	turnaj
hrané	hraný	k2eAgInPc4d1	hraný
mezi	mezi	k7c7	mezi
Wimbledonem	Wimbledon	k1gInSc7	Wimbledon
a	a	k8xC	a
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
grandslamu	grandslam	k1gInSc6	grandslam
přehrála	přehrát	k5eAaPmAgFnS	přehrát
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
aktuální	aktuální	k2eAgFnSc4d1	aktuální
wimbledonskou	wimbledonský	k2eAgFnSc4d1	wimbledonská
finalistku	finalistka	k1gFnSc4	finalistka
Marion	Marion	k1gInSc1	Marion
Bartoliovou	Bartoliový	k2eAgFnSc7d1	Bartoliová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poté	poté	k6eAd1	poté
prohrála	prohrát	k5eAaPmAgFnS	prohrát
potřetí	potřetí	k4xO	potřetí
za	za	k7c4	za
sebou	se	k3xPyFc7	se
ve	v	k7c4	v
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
grandslamu	grandslam	k1gInSc2	grandslam
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
tenistkou	tenistka	k1gFnSc7	tenistka
<g/>
,	,	kIx,	,
Belgičankou	Belgičanka	k1gFnSc7	Belgičanka
Heninovou	Heninová	k1gFnSc7	Heninová
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
nepřešla	přejít	k5eNaPmAgFnS	přejít
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
stuttgartského	stuttgartský	k2eAgInSc2d1	stuttgartský
turnaje	turnaj	k1gInSc2	turnaj
Porsche	Porsche	k1gNnSc1	Porsche
Tennis	Tennis	k1gInSc1	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
světovou	světový	k2eAgFnSc4d1	světová
dvojku	dvojka	k1gFnSc4	dvojka
Světlanu	Světlana	k1gFnSc4	Světlana
Kuzněcovovou	Kuzněcovová	k1gFnSc4	Kuzněcovová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
události	událost	k1gFnSc6	událost
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
třetí	třetí	k4xOgFnSc2	třetí
finálové	finálový	k2eAgFnSc2d1	finálová
účasti	účast	k1gFnSc2	účast
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
turnaj	turnaj	k1gInSc4	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
Tier	Tiera	k1gFnPc2	Tiera
I	i	k9	i
moskevský	moskevský	k2eAgInSc4d1	moskevský
Kremlin	Kremlin	k2eAgInSc4d1	Kremlin
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
oplatila	oplatit	k5eAaPmAgFnS	oplatit
předchozí	předchozí	k2eAgFnSc4d1	předchozí
prohru	prohra	k1gFnSc4	prohra
Kuzněcovové	Kuzněcovová	k1gFnSc2	Kuzněcovová
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
si	se	k3xPyFc3	se
již	již	k6eAd1	již
neporadila	poradit	k5eNaPmAgFnS	poradit
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
Ruskou	ruský	k2eAgFnSc7d1	ruská
Jelenou	Jelena	k1gFnSc7	Jelena
Dementěvovou	Dementěvový	k2eAgFnSc7d1	Dementěvový
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
výsledky	výsledek	k1gInPc1	výsledek
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
zajistily	zajistit	k5eAaPmAgFnP	zajistit
postup	postup	k1gInSc4	postup
na	na	k7c4	na
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Championships	Championships	k1gInSc1	Championships
in	in	k?	in
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
přítomnost	přítomnost	k1gFnSc1	přítomnost
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
byla	být	k5eAaImAgFnS	být
krátká	krátký	k2eAgFnSc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prohraném	prohraný	k2eAgInSc6d1	prohraný
prvním	první	k4xOgInSc6	první
setu	set	k1gInSc6	set
premiérového	premiérový	k2eAgNnSc2d1	premiérové
utkání	utkání	k1gNnSc2	utkání
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Čakvetadzeovou	Čakvetadzeův	k2eAgFnSc7d1	Čakvetadzeův
musela	muset	k5eAaImAgFnS	muset
zápas	zápas	k1gInSc4	zápas
skrečovat	skrečovat	k5eAaPmF	skrečovat
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
a	a	k8xC	a
odstoupit	odstoupit	k5eAaPmF	odstoupit
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc4	pozice
žebříčku	žebříček	k1gInSc2	žebříček
jako	jako	k8xS	jako
nejvýše	vysoce	k6eAd3	vysoce
postavená	postavený	k2eAgFnSc1d1	postavená
Američanka	Američanka	k1gFnSc1	Američanka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
postavení	postavení	k1gNnSc6	postavení
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
<g/>
Sezónu	sezóna	k1gFnSc4	sezóna
2008	[number]	k4	2008
zahájila	zahájit	k5eAaPmAgFnS	zahájit
první	první	k4xOgFnSc7	první
účastí	účast	k1gFnSc7	účast
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
smíšených	smíšený	k2eAgNnPc2d1	smíšené
družstev	družstvo	k1gNnPc2	družstvo
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
v	v	k7c6	v
australském	australský	k2eAgInSc6d1	australský
Perthu	Perth	k1gInSc6	Perth
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Melbourne	Melbourne	k1gNnSc2	Melbourne
přijela	přijet	k5eAaPmAgFnS	přijet
jako	jako	k9	jako
sedmá	sedmý	k4xOgFnSc1	sedmý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
tenistka	tenistka	k1gFnSc1	tenistka
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
postavení	postavení	k1gNnSc1	postavení
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
dosaženému	dosažený	k2eAgInSc3d1	dosažený
výsledku	výsledek	k1gInSc3	výsledek
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
se	se	k3xPyFc4	se
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
hráčkou	hráčka	k1gFnSc7	hráčka
světa	svět	k1gInSc2	svět
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Jelenou	Jelena	k1gFnSc7	Jelena
Jankovićovou	Jankovićová	k1gFnSc7	Jankovićová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gFnSc1	její
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
čtvrtfinálová	čtvrtfinálový	k2eAgFnSc1d1	čtvrtfinálová
porážka	porážka	k1gFnSc1	porážka
na	na	k7c6	na
grandslamu	grandslam	k1gInSc6	grandslam
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gMnSc1	Venus
vypadly	vypadnout	k5eAaPmAgFnP	vypadnout
mezi	mezi	k7c7	mezi
posledními	poslední	k2eAgMnPc7d1	poslední
osmi	osm	k4xCc7	osm
páry	pár	k1gInPc7	pár
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačily	stačit	k5eNaBmAgFnP	stačit
na	na	k7c4	na
turnajové	turnajový	k2eAgFnPc4d1	turnajová
sedmnáctky	sedmnáctka	k1gFnPc4	sedmnáctka
Číňanky	Číňanka	k1gFnSc2	Číňanka
Čeng	Čenga	k1gFnPc2	Čenga
Ťie	Ťie	k1gFnSc2	Ťie
a	a	k8xC	a
Jen	jen	k9	jen
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
turnajů	turnaj	k1gInPc2	turnaj
pro	pro	k7c4	pro
nutný	nutný	k2eAgInSc4d1	nutný
stomatologický	stomatologický	k2eAgInSc4d1	stomatologický
operativní	operativní	k2eAgInSc4d1	operativní
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
získala	získat	k5eAaPmAgFnS	získat
tři	tři	k4xCgInPc4	tři
tituly	titul	k1gInPc4	titul
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
turnaji	turnaj	k1gInSc6	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
Tier	Tier	k1gMnSc1	Tier
II	II	kA	II
Bangalore	Bangalor	k1gInSc5	Bangalor
Open	Openo	k1gNnPc2	Openo
v	v	k7c6	v
Bengalúru	Bengalúr	k1gInSc6	Bengalúr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
porazila	porazit	k5eAaPmAgFnS	porazit
Venus	Venus	k1gInSc4	Venus
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
zachránila	zachránit	k5eAaPmAgFnS	zachránit
zápas	zápas	k1gInSc1	zápas
odvrácením	odvrácení	k1gNnSc7	odvrácení
mečbolu	mečbol	k1gInSc2	mečbol
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
setu	set	k1gInSc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
první	první	k4xOgNnSc4	první
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
utkání	utkání	k1gNnSc4	utkání
od	od	k7c2	od
osmifinále	osmifinále	k1gNnSc2	osmifinále
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
nad	nad	k7c4	nad
Patty	Patt	k1gMnPc4	Patt
Schnyderovou	Schnyderová	k1gFnSc4	Schnyderová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Tier	Tier	k1gInSc1	Tier
I	i	k8xC	i
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
získala	získat	k5eAaPmAgFnS	získat
celkově	celkově	k6eAd1	celkově
pátý	pátý	k4xOgInSc4	pátý
titul	titul	k1gInSc4	titul
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
podniku	podnik	k1gInSc2	podnik
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
rekord	rekord	k1gInSc4	rekord
Němky	Němka	k1gFnSc2	Němka
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
zde	zde	k6eAd1	zde
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Heninovou	Heninový	k2eAgFnSc7d1	Heninová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
předtím	předtím	k6eAd1	předtím
na	na	k7c6	na
grandslamech	grandslam	k1gInPc6	grandslam
třikrát	třikrát	k6eAd1	třikrát
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
prohrála	prohrát	k5eAaPmAgFnS	prohrát
<g/>
,	,	kIx,	,
v	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
dvojkou	dvojka	k1gFnSc7	dvojka
Kuzněcovovu	Kuzněcovův	k2eAgFnSc4d1	Kuzněcovův
a	a	k8xC	a
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
se	s	k7c7	s
čtyřkou	čtyřka	k1gFnSc7	čtyřka
Jankovićovou	Jankovićová	k1gFnSc7	Jankovićová
<g/>
.	.	kIx.	.
</s>
<s>
Triumf	triumf	k1gInSc1	triumf
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jubilejním	jubilejní	k2eAgInSc7d1	jubilejní
třicátým	třicátý	k4xOgInSc7	třicátý
titulem	titul	k1gInSc7	titul
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Tier	Tiera	k1gFnPc2	Tiera
I	i	k8xC	i
Family	Famila	k1gFnSc2	Famila
Circle	Circle	k1gFnSc2	Circle
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
<g/>
,	,	kIx,	,
zdolala	zdolat	k5eAaPmAgFnS	zdolat
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
druhou	druhý	k4xOgFnSc4	druhý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Šarapovovou	Šarapovová	k1gFnSc4	Šarapovová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
nad	nad	k7c7	nad
další	další	k2eAgFnSc7d1	další
Ruskou	Ruska	k1gFnSc7	Ruska
Věrou	Věra	k1gFnSc7	Věra
Zvonarevovou	Zvonarevová	k1gFnSc7	Zvonarevová
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
završila	završit	k5eAaPmAgFnS	završit
svůj	svůj	k3xOyFgInSc4	svůj
desáty	desáta	k1gFnSc2	desáta
vyhraný	vyhraný	k2eAgInSc4d1	vyhraný
turnaj	turnaj	k1gInSc4	turnaj
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
WTA	WTA	kA	WTA
Tier	Tier	k1gInSc1	Tier
I	i	k8xC	i
a	a	k8xC	a
první	první	k4xOgInSc1	první
hraný	hraný	k2eAgInSc1d1	hraný
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
od	od	k7c2	od
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
šňůra	šňůra	k1gFnSc1	šňůra
sedmnácti	sedmnáct	k4xCc2	sedmnáct
výher	výhra	k1gFnPc2	výhra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
byla	být	k5eAaImAgFnS	být
ukončena	ukončit	k5eAaPmNgFnS	ukončit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
berlínského	berlínský	k2eAgNnSc2d1	berlínské
Tier	Tiera	k1gFnPc2	Tiera
I	i	k8xC	i
Qatar	Qatara	k1gFnPc2	Qatara
Telecom	Telecom	k1gInSc1	Telecom
German	German	k1gMnSc1	German
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Dinaru	Dinara	k1gFnSc4	Dinara
Safinovou	Safinový	k2eAgFnSc4d1	Safinová
po	po	k7c6	po
setech	set	k1gInPc6	set
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
pátá	pátý	k4xOgFnSc1	pátý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Tier	Tier	k1gInSc1	Tier
I	i	k9	i
Internazionali	Internazionali	k1gMnSc1	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italia	k1gFnSc1	Italia
prošla	projít	k5eAaPmAgFnS	projít
také	také	k9	také
mezi	mezi	k7c4	mezi
posledních	poslední	k2eAgNnPc2d1	poslední
osm	osm	k4xCc4	osm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc3	on
čekala	čekat	k5eAaImAgFnS	čekat
Alizé	Alizí	k1gMnPc4	Alizí
Cornetová	Cornetový	k2eAgFnSc1d1	Cornetová
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Williamsová	Williamsová	k1gFnSc1	Williamsová
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
grandslam	grandslam	k1gInSc4	grandslam
French	French	k1gInSc4	French
Open	Opena	k1gFnPc2	Opena
přijela	přijet	k5eAaPmAgFnS	přijet
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
páté	pátý	k4xOgFnSc2	pátý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
tenistky	tenistka	k1gFnSc2	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgMnSc6d1	celý
pavouku	pavouk	k1gMnSc6	pavouk
ženské	ženský	k2eAgFnSc2d1	ženská
dvouhry	dvouhra	k1gFnSc2	dvouhra
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
vítězkou	vítězka	k1gFnSc7	vítězka
tohoto	tento	k3xDgInSc2	tento
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
šampiónka	šampiónka	k1gFnSc1	šampiónka
Heninová	Heninová	k1gFnSc1	Heninová
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
překvapivě	překvapivě	k6eAd1	překvapivě
ukončila	ukončit	k5eAaPmAgFnS	ukončit
profesionální	profesionální	k2eAgFnSc4d1	profesionální
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
ovšem	ovšem	k9	ovšem
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
dvacáté	dvacátý	k4xOgFnSc6	dvacátý
sedmé	sedmý	k4xOgFnSc3	sedmý
nasazené	nasazený	k2eAgFnSc3d1	nasazená
Slovince	Slovinka	k1gFnSc3	Slovinka
Katarině	Katarina	k1gFnSc3	Katarina
Srebotnikové	Srebotnikový	k2eAgFnSc2d1	Srebotniková
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c4	v
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
se	se	k3xPyFc4	se
jako	jako	k9	jako
šestá	šestý	k4xOgFnSc1	šestý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
poprvé	poprvé	k6eAd1	poprvé
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
porazila	porazit	k5eAaPmAgFnS	porazit
bývalou	bývalý	k2eAgFnSc4d1	bývalá
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
a	a	k8xC	a
wimbledonskou	wimbledonský	k2eAgFnSc4d1	wimbledonská
vítězku	vítězka	k1gFnSc4	vítězka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Amélii	Amélie	k1gFnSc6	Amélie
Mauresmovou	Mauresmová	k1gFnSc4	Mauresmová
<g/>
,	,	kIx,	,
v	v	k7c6	v
sesterském	sesterský	k2eAgNnSc6d1	sesterské
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Venus	Venus	k1gInSc4	Venus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgNnSc4	první
grandslamové	grandslamový	k2eAgNnSc4d1	grandslamové
finále	finále	k1gNnSc4	finále
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
sestry	sestra	k1gFnPc1	sestra
střetly	střetnout	k5eAaPmAgFnP	střetnout
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
triumfovaly	triumfovat	k5eAaBmAgFnP	triumfovat
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
připsaly	připsat	k5eAaPmAgFnP	připsat
si	se	k3xPyFc3	se
první	první	k4xOgNnSc4	první
grandslamové	grandslamový	k2eAgNnSc4d1	grandslamové
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
odehrála	odehrát	k5eAaPmAgFnS	odehrát
čtyři	čtyři	k4xCgNnPc4	čtyři
utkání	utkání	k1gNnPc4	utkání
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
profesionální	profesionální	k2eAgFnSc6d1	profesionální
tenisové	tenisový	k2eAgFnSc6d1	tenisová
lize	liga	k1gFnSc6	liga
World	Worlda	k1gFnPc2	Worlda
Team	team	k1gInSc4	team
Tennis	Tennis	k1gFnSc4	Tennis
za	za	k7c4	za
klub	klub	k1gInSc4	klub
Washington	Washington	k1gInSc1	Washington
Kastles	Kastles	k1gInSc1	Kastles
<g/>
,	,	kIx,	,
když	když	k8xS	když
družstvu	družstvo	k1gNnSc6	družstvo
přispěla	přispět	k5eAaPmAgFnS	přispět
49	[number]	k4	49
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kalifornském	kalifornský	k2eAgInSc6d1	kalifornský
turnaji	turnaj	k1gInSc6	turnaj
Bank	bank	k1gInSc1	bank
of	of	k?	of
the	the	k?	the
West	West	k1gMnSc1	West
Classic	Classic	k1gMnSc1	Classic
ve	v	k7c6	v
Stanfordu	Stanford	k1gInSc6	Stanford
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
hráčkou	hráčka	k1gFnSc7	hráčka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
semifinále	semifinále	k1gNnSc1	semifinále
proti	proti	k7c3	proti
kvalifikantce	kvalifikantka	k1gFnSc3	kvalifikantka
Aleksandře	Aleksandra	k1gFnSc3	Aleksandra
Wozniakové	Wozniakový	k2eAgFnSc2d1	Wozniaková
musela	muset	k5eAaImAgFnS	muset
skrečovat	skrečovat	k5eAaPmF	skrečovat
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
kvůli	kvůli	k7c3	kvůli
obnovenému	obnovený	k2eAgNnSc3d1	obnovené
zranění	zranění	k1gNnSc3	zranění
levého	levý	k2eAgInSc2d1	levý
kolene	kolen	k1gMnSc5	kolen
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
týden	týden	k1gInSc4	týden
vynechala	vynechat	k5eAaPmAgFnS	vynechat
další	další	k2eAgInSc4d1	další
turnaj	turnaj	k1gInSc4	turnaj
East	East	k2eAgInSc1d1	East
West	West	k2eAgInSc1d1	West
Bank	bank	k1gInSc1	bank
Classic	Classice	k1gFnPc2	Classice
hraný	hraný	k2eAgMnSc1d1	hraný
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
soutěže	soutěž	k1gFnSc2	soutěž
ženské	ženský	k2eAgFnSc2d1	ženská
dvouhry	dvouhra	k1gFnSc2	dvouhra
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
olympijského	olympijský	k2eAgInSc2d1	olympijský
turnaje	turnaj	k1gInSc2	turnaj
na	na	k7c6	na
pekingských	pekingský	k2eAgFnPc6d1	Pekingská
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
her	hra	k1gFnPc2	hra
2008	[number]	k4	2008
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
jako	jako	k9	jako
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
nasazená	nasazený	k2eAgFnSc1d1	nasazená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
čtvrtfinále	čtvrtfinále	k1gNnSc4	čtvrtfinále
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
podlehla	podlehnout	k5eAaPmAgNnP	podlehnout
páté	pátá	k1gFnSc2	pátá
nasazené	nasazený	k2eAgFnSc6d1	nasazená
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc6d2	pozdější
olympijské	olympijský	k2eAgFnSc6d1	olympijská
vítězce	vítězka	k1gFnSc6	vítězka
Jeleně	jeleně	k1gNnSc1	jeleně
Dementěvové	Dementěvová	k1gFnSc2	Dementěvová
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc1	Venus
si	se	k3xPyFc3	se
zopakovaly	zopakovat	k5eAaPmAgFnP	zopakovat
triumf	triumf	k1gInSc4	triumf
z	z	k7c2	z
LOH	LOH	kA	LOH
2000	[number]	k4	2000
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
přehrály	přehrát	k5eAaPmAgInP	přehrát
španělský	španělský	k2eAgInSc1d1	španělský
pár	pár	k4xCyI	pár
Anabel	Anablo	k1gNnPc2	Anablo
Medinaová	Medinaový	k2eAgFnSc1d1	Medinaová
Garriguesová	Garriguesový	k2eAgFnSc1d1	Garriguesová
a	a	k8xC	a
Virginia	Virginium	k1gNnPc4	Virginium
Ruanová	Ruanový	k2eAgFnSc1d1	Ruanový
Pascualová	Pascualová	k1gFnSc1	Pascualová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
nasazené	nasazený	k2eAgFnSc2d1	nasazená
tenistky	tenistka	k1gFnSc2	tenistka
na	na	k7c6	na
poolympijském	poolympijský	k2eAgNnSc6d1	poolympijské
US	US	kA	US
Open	Open	k1gMnSc1	Open
porazila	porazit	k5eAaPmAgFnS	porazit
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
sedmou	sedma	k1gFnSc7	sedma
hráčku	hráčka	k1gFnSc4	hráčka
turnaje	turnaj	k1gInPc4	turnaj
Venus	Venus	k1gInSc1	Venus
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
setu	set	k1gInSc6	set
již	již	k6eAd1	již
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
sadě	sada	k1gFnSc6	sada
navíc	navíc	k6eAd1	navíc
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
ve	v	k7c4	v
druhé	druhý	k4xOgNnSc4	druhý
osm	osm	k4xCc4	osm
setbolů	setbol	k1gInPc2	setbol
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Safinovou	Safinová	k1gFnSc4	Safinová
a	a	k8xC	a
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
druhou	druhý	k4xOgFnSc4	druhý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Jankovićovou	Jankovićová	k1gFnSc4	Jankovićová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tak	tak	k9	tak
nadále	nadále	k6eAd1	nadále
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
bez	bez	k7c2	bez
grandslamového	grandslamový	k2eAgInSc2d1	grandslamový
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
třetí	třetí	k4xOgFnSc4	třetí
výhru	výhra	k1gFnSc4	výhra
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gNnSc4	Open
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
devátý	devátý	k4xOgInSc4	devátý
singlový	singlový	k2eAgInSc4d1	singlový
grandslam	grandslam	k1gInSc4	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
triumf	triumf	k1gInSc1	triumf
ji	on	k3xPp3gFnSc4	on
zajistil	zajistit	k5eAaPmAgInS	zajistit
návrat	návrat	k1gInSc1	návrat
na	na	k7c4	na
světový	světový	k2eAgInSc4d1	světový
trůn	trůn	k1gInSc4	trůn
ženského	ženský	k2eAgInSc2d1	ženský
tenisu	tenis	k1gInSc2	tenis
–	–	k?	–
první	první	k4xOgNnPc1	první
místo	místo	k1gNnSc1	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
stuttgartském	stuttgartský	k2eAgInSc6d1	stuttgartský
podniku	podnik	k1gInSc6	podnik
Tier	Tier	k1gMnSc1	Tier
II	II	kA	II
Porsche	Porsche	k1gNnSc1	Porsche
Tennis	Tennis	k1gInSc1	Tennis
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
podlehla	podlehnout	k5eAaPmAgNnP	podlehnout
jako	jako	k8xC	jako
nejvýše	vysoce	k6eAd3	vysoce
nasazená	nasazený	k2eAgFnSc1d1	nasazená
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
třicáté	třicátý	k4xOgFnSc3	třicátý
tenistce	tenistka	k1gFnSc3	tenistka
Li	li	k8xS	li
Na	na	k7c6	na
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
si	se	k3xPyFc3	se
také	také	k6eAd1	také
zahrála	zahrát	k5eAaPmAgFnS	zahrát
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
postupu	postup	k1gInSc6	postup
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc1d1	americký
pár	pár	k1gInSc1	pár
nucen	nucen	k2eAgInSc1d1	nucen
odstoupit	odstoupit	k5eAaPmF	odstoupit
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
Serenina	Serenin	k2eAgInSc2d1	Serenin
levého	levý	k2eAgInSc2d1	levý
kotníku	kotník	k1gInSc2	kotník
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
z	z	k7c2	z
Tier	Tiera	k1gFnPc2	Tiera
I	i	k8xC	i
Kremlin	Kremlina	k1gFnPc2	Kremlina
Cupu	cupat	k5eAaImIp1nS	cupat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
pro	pro	k7c4	pro
přetrvávající	přetrvávající	k2eAgNnSc4d1	přetrvávající
zranění	zranění	k1gNnSc4	zranění
a	a	k8xC	a
únavu	únava	k1gFnSc4	únava
z	z	k7c2	z
náročného	náročný	k2eAgInSc2d1	náročný
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Odpočinek	odpočinek	k1gInSc1	odpočinek
ji	on	k3xPp3gFnSc4	on
stál	stát	k5eAaImAgInS	stát
první	první	k4xOgFnSc7	první
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
přenechala	přenechat	k5eAaPmAgFnS	přenechat
Srbce	Srbka	k1gFnSc3	Srbka
Jankovićové	Jankovićová	k1gFnSc3	Jankovićová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Championships	Championships	k1gInSc4	Championships
v	v	k7c6	v
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
nejdříve	dříve	k6eAd3	dříve
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
Robinově	Robinův	k2eAgFnSc6d1	Robinova
skupině	skupina	k1gFnSc6	skupina
nad	nad	k7c7	nad
Safinovou	Safinová	k1gFnSc7	Safinová
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
prohrála	prohrát	k5eAaPmAgFnS	prohrát
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gMnSc1	Venus
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
a	a	k8xC	a
do	do	k7c2	do
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
třetího	třetí	k4xOgInSc2	třetí
utkání	utkání	k1gNnPc2	utkání
proti	proti	k7c3	proti
Dementěvové	Dementěvová	k1gFnSc3	Dementěvová
vůbec	vůbec	k9	vůbec
nenastoupila	nastoupit	k5eNaPmAgFnS	nastoupit
pro	pro	k7c4	pro
poranění	poranění	k1gNnSc4	poranění
břišního	břišní	k2eAgNnSc2d1	břišní
svalstva	svalstvo	k1gNnSc2	svalstvo
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
završila	završit	k5eAaPmAgFnS	završit
2	[number]	k4	2
<g/>
.	.	kIx.	.
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
umístěním	umístění	k1gNnSc7	umístění
i	i	k8xC	i
celkovou	celkový	k2eAgFnSc7d1	celková
předvedenou	předvedený	k2eAgFnSc7d1	předvedená
hrou	hra	k1gFnSc7	hra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Opět	opět	k6eAd1	opět
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
tenistkou	tenistka	k1gFnSc7	tenistka
světa	svět	k1gInSc2	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Medibank	Medibanko	k1gNnPc2	Medibanko
International	International	k1gFnPc2	International
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
startovala	startovat	k5eAaBmAgFnS	startovat
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
nejvýše	vysoce	k6eAd3	vysoce
nasazené	nasazený	k2eAgFnPc1d1	nasazená
hráčky	hráčka	k1gFnPc1	hráčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
měla	mít	k5eAaImAgFnS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
Australankou	Australanka	k1gFnSc7	Australanka
Samanthou	Samantha	k1gFnSc7	Samantha
Stosurovou	stosurový	k2eAgFnSc7d1	stosurový
<g/>
,	,	kIx,	,
když	když	k8xS	když
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
čtyři	čtyři	k4xCgInPc4	čtyři
mečboly	mečbol	k1gInPc4	mečbol
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sadě	sada	k1gFnSc6	sada
pro	pro	k7c4	pro
podávající	podávající	k2eAgFnSc4d1	podávající
soupeřku	soupeřka	k1gFnSc4	soupeřka
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnPc1	utkání
vybojovala	vybojovat	k5eAaPmAgNnP	vybojovat
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
mladou	mladý	k2eAgFnSc4d1	mladá
Dánku	Dánka	k1gFnSc4	Dánka
Carolinu	Carolin	k2eAgFnSc4d1	Carolina
Wozniackou	Wozniacký	k2eAgFnSc4d1	Wozniacká
také	také	k9	také
s	s	k7c7	s
obtížemi	obtíž	k1gFnPc7	obtíž
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
zachránila	zachránit	k5eAaPmAgFnS	zachránit
tři	tři	k4xCgInPc4	tři
mečboly	mečbol	k1gInPc4	mečbol
protihráčky	protihráčka	k1gFnSc2	protihráčka
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
setu	set	k1gInSc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
zápasem	zápas	k1gInSc7	zápas
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
stalo	stát	k5eAaPmAgNnS	stát
semifinále	semifinále	k1gNnSc1	semifinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
potřetí	potřetí	k4xO	potřetí
za	za	k7c4	za
sebou	se	k3xPyFc7	se
Rusce	Ruska	k1gFnSc6	Ruska
Jeleně	Jelena	k1gFnSc3	Jelena
Dementěvové	Dementěvová	k1gFnSc3	Dementěvová
tentokrát	tentokrát	k6eAd1	tentokrát
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
grandslam	grandslam	k1gInSc4	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
Australian	Australiana	k1gFnPc2	Australiana
Open	Open	k1gMnSc1	Open
přijela	přijet	k5eAaPmAgFnS	přijet
v	v	k7c6	v
roli	role	k1gFnSc6	role
druhé	druhý	k4xOgFnSc2	druhý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
hráčky	hráčka	k1gFnSc2	hráčka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
utkáních	utkání	k1gNnPc6	utkání
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
tři	tři	k4xCgInPc4	tři
míče	míč	k1gInPc4	míč
od	od	k7c2	od
vyřazení	vyřazení	k1gNnSc2	vyřazení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
pak	pak	k6eAd1	pak
porazila	porazit	k5eAaPmAgFnS	porazit
osmou	osma	k1gFnSc7	osma
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Světlanu	Světlana	k1gFnSc4	Světlana
Kuzněcovovou	Kuzněcovová	k1gFnSc4	Kuzněcovová
a	a	k8xC	a
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
čtyřku	čtyřka	k1gFnSc4	čtyřka
Dementěvovou	Dementěvový	k2eAgFnSc4d1	Dementěvový
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
desátý	desátý	k4xOgInSc4	desátý
grnadslamový	grnadslamový	k2eAgInSc4d1	grnadslamový
titul	titul	k1gInSc4	titul
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
získala	získat	k5eAaPmAgFnS	získat
po	po	k7c6	po
hladké	hladký	k2eAgFnSc6d1	hladká
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
trvající	trvající	k2eAgFnSc1d1	trvající
59	[number]	k4	59
minut	minuta	k1gFnPc2	minuta
nad	nad	k7c7	nad
Ruskou	ruský	k2eAgFnSc7d1	ruská
Dinarou	Dinarý	k2eAgFnSc7d1	Dinarý
Safinovou	Safinová	k1gFnSc7	Safinová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc1	vítězství
ji	on	k3xPp3gFnSc4	on
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
a	a	k8xC	a
získaná	získaný	k2eAgFnSc1d1	získaná
finanční	finanční	k2eAgFnSc1d1	finanční
odměna	odměna	k1gFnSc1	odměna
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
učinila	učinit	k5eAaImAgFnS	učinit
nejlépe	dobře	k6eAd3	dobře
vydělávající	vydělávající	k2eAgFnSc4d1	vydělávající
sportovkyni	sportovkyně	k1gFnSc4	sportovkyně
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
na	na	k7c6	na
prémiích	prémie	k1gFnPc6	prémie
připsala	připsat	k5eAaPmAgFnS	připsat
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
než	než	k8xS	než
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
<g/>
,	,	kIx,	,
golfistka	golfistka	k1gFnSc1	golfistka
Annika	Annika	k1gFnSc1	Annika
Sörenstamová	Sörenstamový	k2eAgFnSc1d1	Sörenstamová
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
také	také	k6eAd1	také
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gMnSc1	Venus
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
Open	Open	k1gInSc1	Open
GDF	GDF	kA	GDF
SUEZ	Suez	k1gInSc4	Suez
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
před	před	k7c7	před
semifinále	semifinále	k1gNnSc7	semifinále
s	s	k7c7	s
Dementěvovou	Dementěvová	k1gFnSc7	Dementěvová
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
kolena	koleno	k1gNnSc2	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
turnaje	turnaj	k1gInSc2	turnaj
Barclays	Barclays	k1gInSc4	Barclays
Dubai	Dubai	k1gNnSc2	Dubai
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premier	k1gInSc1	Premier
5	[number]	k4	5
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
2009	[number]	k4	2009
nastupovala	nastupovat	k5eAaImAgFnS	nastupovat
jako	jako	k9	jako
nejvýše	vysoce	k6eAd3	vysoce
nasazená	nasazený	k2eAgFnSc1d1	nasazená
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
bývalou	bývalý	k2eAgFnSc4d1	bývalá
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Srbku	Srbka	k1gFnSc4	Srbka
Anu	Anu	k1gFnSc4	Anu
Ivanovićovou	Ivanovićová	k1gFnSc4	Ivanovićová
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
ovšem	ovšem	k9	ovšem
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
sestry	sestra	k1gFnSc2	sestra
Venus	Venus	k1gInSc1	Venus
po	po	k7c6	po
semifinálové	semifinálový	k2eAgFnSc6d1	semifinálová
prohře	prohra	k1gFnSc6	prohra
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
<g/>
,	,	kIx,	,
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premiero	k1gNnPc2	Premiero
Mandatory	Mandator	k1gInPc7	Mandator
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
zdolala	zdolat	k5eAaPmAgFnS	zdolat
tři	tři	k4xCgFnPc4	tři
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
čínské	čínský	k2eAgFnPc4d1	čínská
hráčky	hráčka	k1gFnPc4	hráčka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
světovou	světový	k2eAgFnSc4d1	světová
třicet	třicet	k4xCc1	třicet
čtyřku	čtyřka	k1gFnSc4	čtyřka
Pcheng	Pcheng	k1gMnSc1	Pcheng
Šuaj	Šuaj	k1gMnSc1	Šuaj
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
sedmnáctou	sedmnáctý	k4xOgFnSc4	sedmnáctý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
tenistku	tenistka	k1gFnSc4	tenistka
světa	svět	k1gInSc2	svět
Čeng	Čeng	k1gMnSc1	Čeng
Ťie	Ťie	k1gMnSc1	Ťie
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
čtyřicítku	čtyřicítka	k1gFnSc4	čtyřicítka
Li	li	k9	li
Na	na	k7c4	na
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
oplatila	oplatit	k5eAaPmAgFnS	oplatit
poslední	poslední	k2eAgFnSc4d1	poslední
porážku	porážka	k1gFnSc4	porážka
sestře	sestra	k1gFnSc3	sestra
Venus	Venus	k1gInSc4	Venus
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
hrála	hrát	k5eAaImAgFnS	hrát
se	s	k7c7	s
zraněním	zranění	k1gNnSc7	zranění
stehna	stehno	k1gNnSc2	stehno
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
jedenáctou	jedenáctý	k4xOgFnSc4	jedenáctý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Bělorusku	Běloruska	k1gFnSc4	Běloruska
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Finálová	finálový	k2eAgFnSc1d1	finálová
prohra	prohra	k1gFnSc1	prohra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc1	první
ze	z	k7c2	z
šňůry	šňůra	k1gFnSc2	šňůra
čtyř	čtyři	k4xCgInPc2	čtyři
nevyhraných	vyhraný	k2eNgInPc2d1	nevyhraný
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
její	její	k3xOp3gInSc4	její
osobní	osobní	k2eAgInSc4d1	osobní
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
odehraných	odehraný	k2eAgInPc6d1	odehraný
antukových	antukový	k2eAgInPc6d1	antukový
turnajích	turnaj	k1gInPc6	turnaj
odešla	odejít	k5eAaPmAgFnS	odejít
poražena	poražen	k2eAgFnSc1d1	poražena
hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
událostí	událost	k1gFnPc2	událost
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premier	k1gInSc1	Premier
5	[number]	k4	5
Internazionali	Internazionali	k1gFnSc2	Internazionali
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
Premier	Premier	k1gInSc4	Premier
Mandatory	Mandator	k1gMnPc7	Mandator
Mutua	Mutu	k2eAgFnSc1d1	Mutua
Madrilena	Madrilena	k1gFnSc1	Madrilena
Madrid	Madrid	k1gInSc1	Madrid
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
musela	muset	k5eAaImAgFnS	muset
po	po	k7c6	po
nevyrovnaných	vyrovnaný	k2eNgInPc6d1	nevyrovnaný
výkonech	výkon	k1gInPc6	výkon
přenechat	přenechat	k5eAaPmF	přenechat
post	post	k1gInSc4	post
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
Rusce	Ruska	k1gFnSc3	Ruska
Safinové	Safinová	k1gFnSc3	Safinová
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
před	před	k7c4	před
French	French	k1gInSc4	French
Open	Open	k1gInSc4	Open
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
zápas	zápas	k1gInSc4	zápas
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
grandslamu	grandslam	k1gInSc6	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
došla	dojít	k5eAaPmAgFnS	dojít
až	až	k9	až
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
podlehla	podlehnout	k5eAaPmAgNnP	podlehnout
budoucí	budoucí	k2eAgNnPc1d1	budoucí
vítězce	vítězka	k1gFnSc3	vítězka
Světlaně	Světlana	k1gFnSc3	Světlana
Kuzněcovové	Kuzněcovová	k1gFnSc2	Kuzněcovová
po	po	k7c6	po
vyrovnaném	vyrovnaný	k2eAgInSc6d1	vyrovnaný
boji	boj	k1gInSc6	boj
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
prohra	prohra	k1gFnSc1	prohra
přerušila	přerušit	k5eAaPmAgFnS	přerušit
její	její	k3xOp3gFnSc4	její
sérii	série	k1gFnSc4	série
osmnácti	osmnáct	k4xCc2	osmnáct
grandslamových	grandslamový	k2eAgFnPc2d1	grandslamová
výher	výhra	k1gFnPc2	výhra
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
formu	forma	k1gFnSc4	forma
opět	opět	k6eAd1	opět
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
musela	muset	k5eAaImAgFnS	muset
proti	proti	k7c3	proti
čtvrté	čtvrtá	k1gFnSc3	čtvrtá
nasazené	nasazený	k2eAgFnSc3d1	nasazená
Rusce	Ruska	k1gFnSc3	Ruska
Dementěvové	Dementěvová	k1gFnSc2	Dementěvová
odvracet	odvracet	k5eAaImF	odvracet
mečbol	mečbol	k1gInSc4	mečbol
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
utkání	utkání	k1gNnPc4	utkání
dovedla	dovést	k5eAaPmAgFnS	dovést
do	do	k7c2	do
vítězného	vítězný	k2eAgInSc2d1	vítězný
konce	konec	k1gInSc2	konec
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
se	s	k7c7	s
starší	starý	k2eAgFnSc7d2	starší
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přehrála	přehrát	k5eAaPmAgFnS	přehrát
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
třetí	třetí	k4xOgInSc4	třetí
titul	titul	k1gInSc4	titul
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
nejslavnějšího	slavný	k2eAgInSc2d3	nejslavnější
turnaje	turnaj	k1gInSc2	turnaj
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
jedenáctý	jedenáctý	k4xOgInSc4	jedenáctý
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
aktuální	aktuální	k2eAgFnSc7d1	aktuální
vítězkou	vítězka	k1gFnSc7	vítězka
tří	tři	k4xCgFnPc2	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
grandslamů	grandslam	k1gInPc2	grandslam
<g/>
,	,	kIx,	,
nestačilo	stačit	k5eNaBmAgNnS	stačit
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nadále	nadále	k6eAd1	nadále
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Safinová	Safinová	k1gFnSc1	Safinová
<g/>
.	.	kIx.	.
</s>
<s>
Jízlivými	jízlivý	k2eAgFnPc7d1	jízlivá
poznámkami	poznámka	k1gFnPc7	poznámka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
veřejně	veřejně	k6eAd1	veřejně
zmínila	zmínit	k5eAaPmAgFnS	zmínit
o	o	k7c6	o
systému	systém	k1gInSc6	systém
přidělování	přidělování	k1gNnSc2	přidělování
bodů	bod	k1gInPc2	bod
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Venus	Venus	k1gInSc1	Venus
obhájily	obhájit	k5eAaPmAgInP	obhájit
titul	titul	k1gInSc4	titul
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
připsaly	připsat	k5eAaPmAgFnP	připsat
si	se	k3xPyFc3	se
celkově	celkově	k6eAd1	celkově
devátý	devátý	k4xOgInSc4	devátý
grandslam	grandslam	k1gInSc4	grandslam
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
odehrála	odehrát	k5eAaPmAgFnS	odehrát
dva	dva	k4xCgInPc4	dva
turnaje	turnaj	k1gInPc4	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premira	k1gFnPc2	Premira
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgMnS	být
Western	Western	kA	Western
&	&	k?	&
Southern	Southern	k1gMnSc1	Southern
Financial	Financial	k1gMnSc1	Financial
Group	Group	k1gMnSc1	Group
Women	Women	k2eAgMnSc1d1	Women
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Open	Opena	k1gFnPc2	Opena
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgNnSc7	druhý
pak	pak	k6eAd1	pak
Rogers	Rogers	k1gInSc1	Rogers
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
světové	světový	k2eAgFnPc4d1	světová
pětce	pětka	k1gFnSc3	pětka
Dementěvové	Dementěvové	k2eAgMnPc1d1	Dementěvové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
tenistka	tenistka	k1gFnSc1	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
Grandslam	Grandslam	k1gInSc1	Grandslam
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ji	on	k3xPp3gFnSc4	on
zastavila	zastavit	k5eAaPmAgFnS	zastavit
příští	příští	k2eAgFnSc1d1	příští
vítězka	vítězka	k1gFnSc1	vítězka
Belgičanka	Belgičanka	k1gFnSc1	Belgičanka
Kim	Kim	k1gFnSc1	Kim
Clijstersová	Clijstersová	k1gFnSc1	Clijstersová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
dvorce	dvorec	k1gInPc4	dvorec
vrátila	vrátit	k5eAaPmAgFnS	vrátit
po	po	k7c6	po
osmnácti	osmnáct	k4xCc6	osmnáct
měsíční	měsíční	k2eAgInSc1d1	měsíční
mateřské	mateřský	k2eAgFnSc3d1	mateřská
dvolené	dvolená	k1gFnSc3	dvolená
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc4	zápas
provázely	provázet	k5eAaImAgInP	provázet
vyhrocené	vyhrocený	k2eAgInPc1d1	vyhrocený
momenty	moment	k1gInPc1	moment
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Williamsová	Williamsová	k1gFnSc1	Williamsová
podávala	podávat	k5eAaImAgFnS	podávat
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
při	při	k7c6	při
poměru	poměr	k1gInSc6	poměr
gamů	game	k1gInPc2	game
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
čárová	čárový	k2eAgFnSc1d1	čárová
rozhodčí	rozhodčí	k1gMnPc4	rozhodčí
zahlásila	zahlásit	k5eAaPmAgFnS	zahlásit
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
druhém	druhý	k4xOgNnSc6	druhý
podání	podání	k1gNnSc6	podání
chybu	chyba	k1gFnSc4	chyba
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
automaticky	automaticky	k6eAd1	automaticky
znamenalo	znamenat	k5eAaImAgNnS	znamenat
dva	dva	k4xCgInPc1	dva
mečboly	mečbol	k1gInPc1	mečbol
pro	pro	k7c4	pro
belgickou	belgický	k2eAgFnSc4d1	belgická
soupeřku	soupeřka	k1gFnSc4	soupeřka
<g/>
.	.	kIx.	.
</s>
<s>
Raketou	raketa	k1gFnSc7	raketa
začala	začít	k5eAaPmAgFnS	začít
rozrušeně	rozrušeně	k6eAd1	rozrušeně
gestikulovat	gestikulovat	k5eAaImF	gestikulovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
lajnové	lajnová	k1gFnSc3	lajnová
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
a	a	k8xC	a
sprostě	sprostě	k6eAd1	sprostě
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
ohradila	ohradit	k5eAaPmAgFnS	ohradit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Přísahám	přísahat	k5eAaImIp1nS	přísahat
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
že	že	k8xS	že
vezmu	vzít	k5eAaPmIp1nS	vzít
ten	ten	k3xDgInSc4	ten
zasranej	zasranej	k?	zasranej
míček	míček	k1gInSc1	míček
a	a	k8xC	a
narvu	narvat	k5eAaPmIp1nS	narvat
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
tvýho	tvýho	k?	tvýho
zasranýho	zasranýho	k?	zasranýho
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc1	tento
nesportovní	sportovní	k2eNgNnSc1d1	nesportovní
chování	chování	k1gNnSc1	chování
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
penalizaci	penalizace	k1gFnSc6	penalizace
od	od	k7c2	od
hlavní	hlavní	k2eAgMnSc1d1	hlavní
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jí	on	k3xPp3gFnSc3	on
udělila	udělit	k5eAaPmAgFnS	udělit
trestný	trestný	k2eAgInSc4d1	trestný
míček	míček	k1gInSc4	míček
–	–	k?	–
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
prohřešek	prohřešek	k1gInSc4	prohřešek
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
obdržela	obdržet	k5eAaPmAgFnS	obdržet
varování	varování	k1gNnSc4	varování
za	za	k7c4	za
hození	hození	k1gNnSc4	hození
rakety	raketa	k1gFnSc2	raketa
–	–	k?	–
a	a	k8xC	a
zápas	zápas	k1gInSc1	zápas
tak	tak	k6eAd1	tak
skončil	skončit	k5eAaPmAgInS	skončit
bez	bez	k7c2	bez
aktivní	aktivní	k2eAgFnSc2d1	aktivní
dohry	dohra	k1gFnSc2	dohra
na	na	k7c6	na
dvorci	dvorec	k1gInSc6	dvorec
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
pro	pro	k7c4	pro
Clijstersovou	Clijstersová	k1gFnSc4	Clijstersová
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
obdržela	obdržet	k5eAaPmAgFnS	obdržet
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
možnou	možný	k2eAgFnSc4d1	možná
pokutu	pokuta	k1gFnSc4	pokuta
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
10	[number]	k4	10
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
plus	plus	k6eAd1	plus
500	[number]	k4	500
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
hození	hození	k1gNnSc4	hození
rakety	raketa	k1gFnSc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Incident	incident	k1gInSc1	incident
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
vyšetřován	vyšetřovat	k5eAaImNgInS	vyšetřovat
Grandslamovým	grandslamový	k2eAgInSc7d1	grandslamový
výborem	výbor	k1gInSc7	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgInS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
uhradit	uhradit	k5eAaPmF	uhradit
pokutu	pokuta	k1gFnSc4	pokuta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
175	[number]	k4	175
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
jí	on	k3xPp3gFnSc3	on
bude	být	k5eAaImBp3nS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
start	start	k1gInSc1	start
na	na	k7c6	na
příštím	příští	k2eAgInSc6d1	příští
US	US	kA	US
Open	Open	k1gInSc4	Open
nebo	nebo	k8xC	nebo
jiném	jiný	k2eAgInSc6d1	jiný
grandslamovém	grandslamový	k2eAgInSc6d1	grandslamový
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
běžet	běžet	k5eAaImF	běžet
dvouleté	dvouletý	k2eAgNnSc4d1	dvouleté
zkušební	zkušební	k2eAgNnSc4d1	zkušební
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
během	během	k7c2	během
grandslamu	grandslam	k1gInSc2	grandslam
dopustit	dopustit	k5eAaPmF	dopustit
žádného	žádný	k3yNgNnSc2	žádný
nezdvořilého	zdvořilý	k2eNgNnSc2d1	nezdvořilé
či	či	k8xC	či
agresivního	agresivní	k2eAgNnSc2d1	agresivní
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
hrozbou	hrozba	k1gFnSc7	hrozba
zákazu	zákaz	k1gInSc2	zákaz
startu	start	k1gInSc2	start
na	na	k7c6	na
nejbližším	blízký	k2eAgInSc6d3	Nejbližší
ročníku	ročník	k1gInSc6	ročník
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nebude	být	k5eNaImBp3nS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
žádný	žádný	k3yNgInSc4	žádný
takový	takový	k3xDgInSc4	takový
incident	incident	k1gInSc4	incident
ve	v	k7c6	v
stanovené	stanovený	k2eAgFnSc6d1	stanovená
lhůtě	lhůta	k1gFnSc6	lhůta
pokuta	pokuta	k1gFnSc1	pokuta
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
sníží	snížit	k5eAaPmIp3nS	snížit
na	na	k7c6	na
82	[number]	k4	82
500	[number]	k4	500
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Hráčka	hráčka	k1gFnSc1	hráčka
se	se	k3xPyFc4	se
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
omluvit	omluvit	k5eAaPmF	omluvit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
na	na	k7c6	na
pozápasové	pozápasový	k2eAgFnSc6d1	pozápasová
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
prohlášení	prohlášení	k1gNnSc6	prohlášení
vydaném	vydaný	k2eAgNnSc6d1	vydané
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Omluvu	omluva	k1gFnSc4	omluva
čárové	čárový	k2eAgFnSc2d1	čárová
rozhodčí	rozhodčí	k1gMnPc4	rozhodčí
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
až	až	k6eAd1	až
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
hrané	hraný	k2eAgInPc4d1	hraný
s	s	k7c7	s
Venus	Venus	k1gMnSc1	Venus
nebyla	být	k5eNaImAgFnS	být
diskvalifikována	diskvalifikovat	k5eAaBmNgFnS	diskvalifikovat
a	a	k8xC	a
sesterský	sesterský	k2eAgInSc4d1	sesterský
pár	pár	k1gInSc4	pár
grandslam	grandslam	k6eAd1	grandslam
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc4	třetí
ženskou	ženský	k2eAgFnSc4d1	ženská
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
možných	možný	k2eAgMnPc2d1	možný
v	v	k7c6	v
aktuální	aktuální	k2eAgFnSc6d1	aktuální
sezóně	sezóna	k1gFnSc6	sezóna
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
desátý	desátý	k4xOgInSc4	desátý
společný	společný	k2eAgInSc4d1	společný
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
US	US	kA	US
Open	Open	k1gNnSc1	Open
odehrála	odehrát	k5eAaPmAgFnS	odehrát
dva	dva	k4xCgInPc4	dva
turnaje	turnaj	k1gInPc4	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
událost	událost	k1gFnSc1	událost
z	z	k7c2	z
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premira	k1gFnPc2	Premira
Mandatory	Mandator	k1gInPc1	Mandator
China	China	k1gFnSc1	China
Open	Open	k1gInSc1	Open
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
Ruskou	Ruska	k1gFnSc7	Ruska
Naděždou	Naděžda	k1gFnSc7	Naděžda
Petrovovou	Petrovová	k1gFnSc7	Petrovová
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
turnaj	turnaj	k1gInSc1	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
Championships	Championships	k1gInSc1	Championships
v	v	k7c6	v
katarském	katarský	k2eAgInSc6d1	katarský
Dauhá	Dauhý	k2eAgFnSc1d1	Dauhá
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
v	v	k7c6	v
Robinově	Robinův	k2eAgFnSc6d1	Robinova
skupině	skupina	k1gFnSc6	skupina
postupně	postupně	k6eAd1	postupně
porazila	porazit	k5eAaPmAgFnS	porazit
světovou	světový	k2eAgFnSc4d1	světová
sedmičku	sedmička	k1gFnSc4	sedmička
Venus	Venus	k1gMnSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
,	,	kIx,	,
pětku	pětka	k1gFnSc4	pětka
Dementěvovou	Dementěvová	k1gFnSc4	Dementěvová
a	a	k8xC	a
trojku	trojka	k1gFnSc4	trojka
Kuzněcovovou	Kuzněcovový	k2eAgFnSc4d1	Kuzněcovová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
mečbol	mečbol	k1gInSc4	mečbol
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
třetího	třetí	k4xOgInSc2	třetí
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Semifinálový	semifinálový	k2eAgInSc1d1	semifinálový
zápas	zápas	k1gInSc1	zápas
zůstal	zůstat	k5eAaPmAgInS	zůstat
nedohrán	dohrát	k5eNaPmNgInS	dohrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
aktuální	aktuální	k2eAgFnSc1d1	aktuální
finalistka	finalistka	k1gFnSc1	finalistka
z	z	k7c2	z
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
Wozniacká	Wozniacký	k2eAgFnSc1d1	Wozniacká
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
utkání	utkání	k1gNnPc2	utkání
skrečovala	skrečovat	k5eAaPmAgFnS	skrečovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
střetla	střetnout	k5eAaPmAgNnP	střetnout
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
během	během	k7c2	během
čtyř	čtyři	k4xCgInPc2	čtyři
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
nad	nad	k7c7	nad
unavenou	unavený	k2eAgFnSc7d1	unavená
a	a	k8xC	a
chyby	chyba	k1gFnSc2	chyba
produkující	produkující	k2eAgFnSc7d1	produkující
sourozenkyní	sourozenkyně	k1gFnSc7	sourozenkyně
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
setů	set	k1gInPc2	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
druhé	druhý	k4xOgNnSc4	druhý
singlové	singlový	k2eAgNnSc4d1	singlové
vítězství	vítězství	k1gNnSc4	vítězství
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
zakončila	zakončit	k5eAaPmAgFnS	zakončit
jako	jako	k9	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
hráčka	hráčka	k1gFnSc1	hráčka
světa	svět	k1gInSc2	svět
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
celkem	celkem	k6eAd1	celkem
16	[number]	k4	16
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Překonala	překonat	k5eAaPmAgFnS	překonat
také	také	k9	také
absolutní	absolutní	k2eAgInSc4d1	absolutní
rekord	rekord	k1gInSc4	rekord
Justine	Justin	k1gMnSc5	Justin
Heninové	Heninové	k2eAgMnPc7d1	Heninové
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
výdělku	výdělek	k1gInSc2	výdělek
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
na	na	k7c6	na
odměnách	odměna	k1gFnPc6	odměna
okruhu	okruh	k1gInSc2	okruh
2009	[number]	k4	2009
připsala	připsat	k5eAaPmAgFnS	připsat
6	[number]	k4	6
545	[number]	k4	545
586	[number]	k4	586
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
sestry	sestra	k1gFnSc2	sestra
Williamsovy	Williamsův	k2eAgFnPc1d1	Williamsova
zakončily	zakončit	k5eAaPmAgFnP	zakončit
shodně	shodně	k6eAd1	shodně
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
odehrály	odehrát	k5eAaPmAgFnP	odehrát
pouhých	pouhý	k2eAgInPc2d1	pouhý
šest	šest	k4xCc4	šest
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
tenistky	tenistka	k1gFnPc1	tenistka
umístěné	umístěný	k2eAgFnPc1d1	umístěná
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
pozicích	pozice	k1gFnPc6	pozice
hrají	hrát	k5eAaImIp3nP	hrát
minimálně	minimálně	k6eAd1	minimálně
16	[number]	k4	16
a	a	k8xC	a
více	hodně	k6eAd2	hodně
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
pět	pět	k4xCc4	pět
grandslamových	grandslamový	k2eAgNnPc2d1	grandslamové
vítězství	vítězství	k1gNnPc2	vítězství
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
jich	on	k3xPp3gMnPc2	on
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
kontě	konto	k1gNnSc6	konto
měla	mít	k5eAaImAgFnS	mít
dvacet	dvacet	k4xCc4	dvacet
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Agentura	agentura	k1gFnSc1	agentura
Associated	Associated	k1gMnSc1	Associated
Press	Press	k1gInSc1	Press
jí	on	k3xPp3gFnSc3	on
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
volbě	volba	k1gFnSc6	volba
drtivě	drtivě	k6eAd1	drtivě
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
66	[number]	k4	66
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
158	[number]	k4	158
hlasů	hlas	k1gInPc2	hlas
–	–	k?	–
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
kandidát	kandidát	k1gMnSc1	kandidát
neobdržel	obdržet	k5eNaPmAgMnS	obdržet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
18	[number]	k4	18
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
mistryní	mistryně	k1gFnSc7	mistryně
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
i	i	k8xC	i
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Pokračující	pokračující	k2eAgFnSc1d1	pokračující
dominance	dominance	k1gFnSc1	dominance
===	===	k?	===
</s>
</p>
<p>
<s>
Premiérovou	premiérový	k2eAgFnSc7d1	premiérová
událostí	událost	k1gFnSc7	událost
sezóny	sezóna	k1gFnSc2	sezóna
2010	[number]	k4	2010
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
stal	stát	k5eAaPmAgInS	stát
turnaj	turnaj	k1gInSc1	turnaj
v	v	k7c4	v
Sydney	Sydney	k1gNnSc4	Sydney
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Francouzskou	francouzský	k2eAgFnSc7d1	francouzská
Aravane	Aravan	k1gMnSc5	Aravan
Rezaï	Rezaï	k1gMnPc6	Rezaï
po	po	k7c6	po
setech	set	k1gInPc6	set
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sadě	sada	k1gFnSc6	sada
již	již	k6eAd1	již
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dva	dva	k4xCgInPc4	dva
míče	míč	k1gInPc4	míč
od	od	k7c2	od
vyřazení	vyřazení	k1gNnSc2	vyřazení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finálovém	finálový	k2eAgNnSc6d1	finálové
utkání	utkání	k1gNnSc6	utkání
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
obhájkyni	obhájkyně	k1gFnSc4	obhájkyně
titulu	titul	k1gInSc2	titul
a	a	k8xC	a
světovou	světový	k2eAgFnSc4d1	světová
pětku	pětka	k1gFnSc4	pětka
Jelenu	Jelena	k1gFnSc4	Jelena
Dementěvovou	Dementěvová	k1gFnSc4	Dementěvová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
grandslamu	grandslam	k1gInSc6	grandslam
roku	rok	k1gInSc2	rok
Australian	Australiana	k1gFnPc2	Australiana
Open	Openo	k1gNnPc2	Openo
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
trofeje	trofej	k1gInPc4	trofej
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Venus	Venus	k1gInSc4	Venus
i	i	k9	i
ze	z	k7c2	z
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
prošla	projít	k5eAaPmAgFnS	projít
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
jediného	jediný	k2eAgInSc2d1	jediný
ztraceného	ztracený	k2eAgInSc2d1	ztracený
servisu	servis	k1gInSc2	servis
i	i	k8xC	i
setu	set	k1gInSc2	set
<g/>
,	,	kIx,	,
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
po	po	k7c6	po
náročném	náročný	k2eAgInSc6d1	náročný
boji	boj	k1gInSc6	boj
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
setu	set	k1gInSc6	set
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
blízko	blízko	k7c2	blízko
prohry	prohra	k1gFnSc2	prohra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
šestnáctou	šestnáctý	k4xOgFnSc7	šestnáctý
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Li	li	k8xS	li
Na	na	k7c6	na
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
proměnila	proměnit	k5eAaPmAgFnS	proměnit
až	až	k6eAd1	až
pátý	pátý	k4xOgInSc4	pátý
mečbol	mečbol	k1gInSc4	mečbol
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
znamenal	znamenat	k5eAaImAgInS	znamenat
postup	postup	k1gInSc4	postup
do	do	k7c2	do
pátého	pátý	k4xOgNnSc2	pátý
singlového	singlový	k2eAgNnSc2d1	singlové
finále	finále	k1gNnSc2	finále
tohoto	tento	k3xDgInSc2	tento
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
patnáctého	patnáctý	k4xOgNnSc2	patnáctý
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
porazila	porazit	k5eAaPmAgFnS	porazit
šampiónku	šampiónka	k1gFnSc4	šampiónka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
Belgičanku	Belgičanka	k1gFnSc4	Belgičanka
Justine	Justin	k1gMnSc5	Justin
Heninovou	Heninový	k2eAgFnSc4d1	Heninová
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
sadách	sada	k1gFnPc6	sada
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
a	a	k8xC	a
připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
grandslam	grandslam	k1gInSc4	grandslam
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
předčila	předčit	k5eAaBmAgFnS	předčit
Australanku	Australanka	k1gFnSc4	Australanka
Margaret	Margareta	k1gFnPc2	Margareta
Courtovou	Courtová	k1gFnSc4	Courtová
s	s	k7c7	s
11	[number]	k4	11
tituly	titul	k1gInPc7	titul
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
s	s	k7c7	s
Heninovou	Heninová	k1gFnSc7	Heninová
bylo	být	k5eAaImAgNnS	být
premiérovým	premiérový	k2eAgInSc7d1	premiérový
finálovým	finálový	k2eAgInSc7d1	finálový
zápasem	zápas	k1gInSc7	zápas
na	na	k7c6	na
grandslamu	grandslam	k1gInSc6	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
dablu	dabl	k1gInSc2	dabl
Jennifer	Jennifra	k1gFnPc2	Jennifra
Capriatiové	Capriatiový	k2eAgFnPc4d1	Capriatiová
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
Američanka	Američanka	k1gFnSc1	Američanka
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
obhájit	obhájit	k5eAaPmF	obhájit
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
úspěšně	úspěšně	k6eAd1	úspěšně
završily	završit	k5eAaPmAgFnP	završit
obhajobu	obhajoba	k1gFnSc4	obhajoba
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
když	když	k8xS	když
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
světové	světový	k2eAgFnPc1d1	světová
jedničky	jednička	k1gFnPc1	jednička
Caru	car	k1gMnSc3	car
Blackovou	Blacková	k1gFnSc4	Blacková
a	a	k8xC	a
Liezel	Liezel	k1gFnSc4	Liezel
Huberovou	Huberový	k2eAgFnSc4d1	Huberový
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zranění	zranění	k1gNnSc1	zranění
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
si	se	k3xPyFc3	se
vynutilo	vynutit	k5eAaPmAgNnS	vynutit
její	její	k3xOp3gFnSc4	její
odhlášení	odhlášení	k1gNnSc1	odhlášení
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
pěti	pět	k4xCc2	pět
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dvou	dva	k4xCgFnPc2	dva
událostí	událost	k1gFnPc2	událost
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premier	k1gInSc1	Premier
5	[number]	k4	5
Barclays	Barclaysa	k1gFnPc2	Barclaysa
Dubai	Dubae	k1gFnSc4	Dubae
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
a	a	k8xC	a
kategorie	kategorie	k1gFnSc2	kategorie
Premier	Premira	k1gFnPc2	Premira
Mandatory	Mandator	k1gInPc1	Mandator
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Open	Open	k1gNnSc4	Open
ve	v	k7c6	v
floridském	floridský	k2eAgMnSc6d1	floridský
Key	Key	k1gMnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
až	až	k6eAd1	až
římským	římský	k2eAgMnSc7d1	římský
Internazionali	Internazionali	k1gMnSc7	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Srbce	Srbka	k1gFnSc3	Srbka
Jeleně	Jelena	k1gFnSc3	Jelena
Jankovićové	Jankovićová	k1gFnSc2	Jankovićová
po	po	k7c6	po
setech	set	k1gInPc6	set
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sadě	sada	k1gFnSc6	sada
měla	mít	k5eAaImAgFnS	mít
mečbol	mečbol	k1gInSc4	mečbol
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ovšem	ovšem	k9	ovšem
neproměnila	proměnit	k5eNaPmAgFnS	proměnit
a	a	k8xC	a
následně	následně	k6eAd1	následně
nevyužila	využít	k5eNaPmAgFnS	využít
ani	ani	k8xC	ani
nadějný	nadějný	k2eAgInSc1d1	nadějný
stav	stav	k1gInSc1	stav
vedení	vedení	k1gNnSc1	vedení
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
tiebreaku	tiebreak	k1gInSc6	tiebreak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Mutua	Mutuum	k1gNnPc4	Mutuum
Madrileñ	Madrileñ	k1gMnPc2	Madrileñ
Madrid	Madrid	k1gInSc4	Madrid
Open	Opena	k1gFnPc2	Opena
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
volný	volný	k2eAgInSc4d1	volný
los	los	k1gInSc4	los
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
73	[number]	k4	73
nevynucených	vynucený	k2eNgFnPc2d1	nevynucená
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
hodinách	hodina	k1gFnPc6	hodina
a	a	k8xC	a
dvaceti	dvacet	k4xCc2	dvacet
šesti	šest	k4xCc6	šest
minutách	minuta	k1gFnPc6	minuta
udolala	udolat	k5eAaPmAgFnS	udolat
Rusku	Rusko	k1gNnSc6	Rusko
Věru	Věra	k1gFnSc4	Věra
Duševinovou	Duševinový	k2eAgFnSc4d1	Duševinový
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
setů	set	k1gInPc2	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sadě	sada	k1gFnSc6	sada
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
mečbol	mečbol	k1gInSc1	mečbol
soupeřky	soupeřka	k1gFnSc2	soupeřka
a	a	k8xC	a
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
třetího	třetí	k4xOgInSc2	třetí
setu	set	k1gInSc2	set
si	se	k3xPyFc3	se
poranila	poranit	k5eAaPmAgFnS	poranit
horní	horní	k2eAgFnSc4d1	horní
končetinu	končetina	k1gFnSc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
zápas	zápas	k1gInSc4	zápas
prohrála	prohrát	k5eAaPmAgFnS	prohrát
se	s	k7c7	s
šestnáctou	šestnáctý	k4xOgFnSc7	šestnáctý
nasazenou	nasazený	k2eAgFnSc7d1	nasazená
Naděždou	Naděžda	k1gFnSc7	Naděžda
Petrovovou	Petrovová	k1gFnSc7	Petrovová
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Proměnila	proměnit	k5eAaPmAgFnS	proměnit
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
z	z	k7c2	z
osmi	osm	k4xCc2	osm
nabízených	nabízený	k2eAgInPc2d1	nabízený
brejkbolů	brejkbol	k1gInPc2	brejkbol
Rusky	Ruska	k1gFnSc2	Ruska
<g/>
.	.	kIx.	.
</s>
<s>
Ženskou	ženský	k2eAgFnSc4d1	ženská
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
společně	společně	k6eAd1	společně
s	s	k7c7	s
Venus	Venus	k1gInSc4	Venus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
grandslamu	grandslam	k1gInSc6	grandslam
roku	rok	k1gInSc2	rok
French	Frencha	k1gFnPc2	Frencha
Open	Openo	k1gNnPc2	Openo
prošla	projít	k5eAaPmAgFnS	projít
snadno	snadno	k6eAd1	snadno
přes	přes	k7c4	přes
první	první	k4xOgNnPc4	první
tři	tři	k4xCgNnPc4	tři
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Šachar	Šachar	k1gInSc4	Šachar
Pe	Pe	k1gFnSc2	Pe
<g/>
'	'	kIx"	'
<g/>
erovou	erovat	k5eAaPmIp3nP	erovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Australance	Australanka	k1gFnSc3	Australanka
Samantě	Samanta	k1gFnSc3	Samanta
Stosurové	stosurový	k2eAgFnSc2d1	stosurový
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zápasu	zápas	k1gInSc2	zápas
zahrála	zahrát	k5eAaPmAgFnS	zahrát
46	[number]	k4	46
nevynucených	vynucený	k2eNgFnPc2d1	nevynucená
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
promarnila	promarnit	k5eAaPmAgFnS	promarnit
možnost	možnost	k1gFnSc4	možnost
ukončit	ukončit	k5eAaPmF	ukončit
duel	duel	k1gInSc4	duel
<g/>
,	,	kIx,	,
když	když	k8xS	když
neproměnila	proměnit	k5eNaPmAgFnS	proměnit
mečbol	mečbol	k1gInSc4	mečbol
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
setu	set	k1gInSc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
grandslam	grandslam	k1gInSc4	grandslam
od	od	k7c2	od
French	Frencha	k1gFnPc2	Frencha
Open	Open	k1gNnSc1	Open
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
nebo	nebo	k8xC	nebo
nebyla	být	k5eNaImAgFnS	být
poražena	porazit	k5eAaPmNgFnS	porazit
budoucí	budoucí	k2eAgFnSc7d1	budoucí
vítězkou	vítězka	k1gFnSc7	vítězka
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
zde	zde	k6eAd1	zde
nepostoupila	postoupit	k5eNaPmAgFnS	postoupit
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
byly	být	k5eAaImAgFnP	být
s	s	k7c7	s
venus	venus	k1gInSc1	venus
prvním	první	k4xOgInSc7	první
nasazeným	nasazený	k2eAgInSc7d1	nasazený
párem	pár	k1gInSc7	pár
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
semifinálové	semifinálový	k2eAgFnSc6d1	semifinálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Huberovou	Huberová	k1gFnSc7	Huberová
a	a	k8xC	a
Anabel	Anabel	k1gMnSc1	Anabel
Medinaovouu	Medinaovou	k2eAgFnSc4d1	Medinaovou
Garriguesovou	Garriguesový	k2eAgFnSc4d1	Garriguesová
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
zajistily	zajistit	k5eAaPmAgFnP	zajistit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
pro	pro	k7c4	pro
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
významně	významně	k6eAd1	významně
nižší	nízký	k2eAgInSc4d2	nižší
počet	počet	k1gInSc4	počet
odehraných	odehraný	k2eAgInPc2d1	odehraný
turnajů	turnaj	k1gInPc2	turnaj
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc4d1	ostatní
tenistky	tenistka	k1gFnPc4	tenistka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
přehrály	přehrát	k5eAaPmAgInP	přehrát
dvanáctou	dvanáctý	k4xOgFnSc4	dvanáctý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
česko-slovinskou	českolovinský	k2eAgFnSc4d1	česko-slovinská
dvojici	dvojice	k1gFnSc4	dvojice
Květa	Květa	k1gFnSc1	Květa
Peschkeová	Peschkeová	k1gFnSc1	Peschkeová
a	a	k8xC	a
Katarina	Katarina	k1gFnSc1	Katarina
Srebotniková	Srebotnikový	k2eAgFnSc1d1	Srebotniková
po	po	k7c6	po
hladkém	hladký	k2eAgInSc6d1	hladký
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
a	a	k8xC	a
získaly	získat	k5eAaPmAgFnP	získat
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
grandslam	grandslam	k1gInSc4	grandslam
v	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
porazila	porazit	k5eAaPmAgFnS	porazit
mladou	mladý	k2eAgFnSc4d1	mladá
Portugalku	Portugalka	k1gFnSc4	Portugalka
Michelle	Michelle	k1gFnSc2	Michelle
Larcherovou	Larcherová	k1gFnSc4	Larcherová
de	de	k?	de
Britovou	Britův	k2eAgFnSc7d1	Britova
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
prezentovala	prezentovat	k5eAaBmAgFnS	prezentovat
vynikající	vynikající	k2eAgInSc4d1	vynikající
první	první	k4xOgInSc4	první
servis	servis	k1gInSc4	servis
s	s	k7c7	s
patnácti	patnáct	k4xCc7	patnáct
zahranými	zahraný	k2eAgNnPc7d1	zahrané
esy	eso	k1gNnPc7	eso
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
si	se	k3xPyFc3	se
hladce	hladko	k6eAd1	hladko
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Čakvetadzeovou	Čakvetadzeův	k2eAgFnSc7d1	Čakvetadzeův
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
a	a	k8xC	a
následovala	následovat	k5eAaImAgFnS	následovat
další	další	k2eAgFnSc1d1	další
výhra	výhra	k1gFnSc1	výhra
se	s	k7c7	s
Slovenkou	Slovenka	k1gFnSc7	Slovenka
Dominikou	Dominika	k1gFnSc7	Dominika
Cibulkovou	Cibulková	k1gFnSc7	Cibulková
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
doplněna	doplnit	k5eAaPmNgFnS	doplnit
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
servisem	servis	k1gInSc7	servis
s	s	k7c7	s
20	[number]	k4	20
zahranými	zahraný	k2eAgNnPc7d1	zahrané
esy	eso	k1gNnPc7	eso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
bývalou	bývalý	k2eAgFnSc4d1	bývalá
wimbledonskou	wimbledonský	k2eAgFnSc4d1	wimbledonská
vítězku	vítězka	k1gFnSc4	vítězka
Marii	Maria	k1gFnSc4	Maria
Šarapovovou	Šarapovová	k1gFnSc4	Šarapovová
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
první	první	k4xOgFnSc6	první
sadě	sada	k1gFnSc6	sada
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
tři	tři	k4xCgInPc4	tři
setboly	setbol	k1gInPc4	setbol
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
nastřílela	nastřílet	k5eAaPmAgFnS	nastřílet
19	[number]	k4	19
es	es	k1gNnPc2	es
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinálové	čtvrtfinálový	k2eAgNnSc4d1	čtvrtfinálové
utkání	utkání	k1gNnSc4	utkání
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
Číňankou	Číňanka	k1gFnSc7	Číňanka
Li	li	k8xS	li
Na	na	k7c6	na
po	po	k7c6	po
setech	set	k1gInPc6	set
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
překvapením	překvapení	k1gNnSc7	překvapení
turnaje	turnaj	k1gInSc2	turnaj
Češkou	Češka	k1gFnSc7	Češka
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitová	k1gFnSc7	Kvitová
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
porazila	porazit	k5eAaPmAgFnS	porazit
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
setu	set	k1gInSc6	set
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
měla	mít	k5eAaImAgFnS	mít
soupeřka	soupeřka	k1gFnSc1	soupeřka
break	break	k1gInSc1	break
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
s	s	k7c7	s
Venus	Venus	k1gInSc4	Venus
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
ročníky	ročník	k1gInPc1	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Hattrick	hattrick	k1gInSc1	hattrick
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ovšem	ovšem	k9	ovšem
nezdařil	zdařit	k5eNaPmAgInS	zdařit
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
nestačily	stačit	k5eNaBmAgFnP	stačit
na	na	k7c4	na
ruskou	ruský	k2eAgFnSc4d1	ruská
dvojici	dvojice	k1gFnSc4	dvojice
Jelena	Jelena	k1gFnSc1	Jelena
Vesninová	Vesninový	k2eAgFnSc1d1	Vesninová
a	a	k8xC	a
Věra	Věra	k1gFnSc1	Věra
Zvonarevová	Zvonarevová	k1gFnSc1	Zvonarevová
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
podlehly	podlehnout	k5eAaPmAgInP	podlehnout
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
sadách	sada	k1gFnPc6	sada
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
bez	bez	k7c2	bez
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
problémů	problém	k1gInPc2	problém
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
ženské	ženský	k2eAgFnSc2d1	ženská
dvouhry	dvouhra	k1gFnSc2	dvouhra
nad	nad	k7c7	nad
Věrou	Věra	k1gFnSc7	Věra
Zvonarevovou	Zvonarevový	k2eAgFnSc7d1	Zvonarevová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ruska	Ruska	k1gFnSc1	Ruska
si	se	k3xPyFc3	se
nevytvořila	vytvořit	k5eNaPmAgFnS	vytvořit
ani	ani	k8xC	ani
jedinou	jediný	k2eAgFnSc4d1	jediná
brejkbolovou	brejkbolový	k2eAgFnSc4d1	brejkbolový
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Američanka	Američanka	k1gFnSc1	Američanka
třikrát	třikrát	k6eAd1	třikrát
prolomila	prolomit	k5eAaPmAgFnS	prolomit
její	její	k3xOp3gNnSc4	její
podání	podání	k1gNnSc4	podání
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
odehraných	odehraný	k2eAgInPc6d1	odehraný
zápasech	zápas	k1gInPc6	zápas
neztratila	ztratit	k5eNaPmAgFnS	ztratit
jediný	jediný	k2eAgInSc4d1	jediný
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
13	[number]	k4	13
<g/>
.	.	kIx.	.
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
<s>
Devítinásobná	devítinásobný	k2eAgFnSc1d1	devítinásobná
vítězka	vítězka	k1gFnSc1	vítězka
wimbledonské	wimbledonský	k2eAgFnSc2d1	wimbledonská
dvouhry	dvouhra	k1gFnSc2	dvouhra
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
označila	označit	k5eAaPmAgFnS	označit
její	její	k3xOp3gInSc4	její
servis	servis	k1gInSc4	servis
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kdy	kdy	k6eAd1	kdy
od	od	k7c2	od
ženy	žena	k1gFnSc2	žena
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
jednoduše	jednoduše	k6eAd1	jednoduše
"	"	kIx"	"
<g/>
úžasný	úžasný	k2eAgInSc1d1	úžasný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
celkově	celkově	k6eAd1	celkově
zahranými	zahraný	k2eAgNnPc7d1	zahrané
89	[number]	k4	89
esy	eso	k1gNnPc7	eso
překonala	překonat	k5eAaPmAgFnS	překonat
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
počtu	počet	k1gInSc6	počet
zahraných	zahraný	k2eAgNnPc2d1	zahrané
es	es	k1gNnPc2	es
tenistkou	tenistka	k1gFnSc7	tenistka
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
grandslamovém	grandslamový	k2eAgInSc6d1	grandslamový
turnaji	turnaj	k1gInSc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Finalistka	finalistka	k1gFnSc1	finalistka
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Serena	Serena	k1gFnSc1	Serena
často	často	k6eAd1	často
mění	měnit	k5eAaImIp3nS	měnit
umístění	umístění	k1gNnSc4	umístění
<g/>
,	,	kIx,	,
rotaci	rotace	k1gFnSc4	rotace
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
<g/>
.	.	kIx.	.
</s>
<s>
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
dodala	dodat	k5eAaPmAgFnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
tenistek	tenistka	k1gFnPc2	tenistka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
velikost	velikost	k1gFnSc1	velikost
hráče	hráč	k1gMnSc2	hráč
nespočívá	spočívat	k5eNaImIp3nS	spočívat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
vyhraných	vyhraný	k2eAgInPc2d1	vyhraný
grandslamů	grandslam	k1gInPc2	grandslam
či	či	k8xC	či
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
komplexním	komplexní	k2eAgNnSc6d1	komplexní
pojetí	pojetí	k1gNnSc6	pojetí
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
těchto	tento	k3xDgFnPc6	tento
kvalitách	kvalita	k1gFnPc6	kvalita
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Serena	Serena	k1gFnSc1	Serena
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Po	po	k7c6	po
wimbledonské	wimbledonský	k2eAgFnSc6d1	wimbledonská
výhře	výhra	k1gFnSc6	výhra
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
časopisu	časopis	k1gInSc2	časopis
Sports	Sports	k1gInSc4	Sports
Illustrated	Illustrated	k1gInSc1	Illustrated
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
před	před	k7c7	před
US	US	kA	US
Open	Open	k1gInSc1	Open
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
lékařů	lékař	k1gMnPc2	lékař
nezúčastní	zúčastnit	k5eNaPmIp3nS	zúčastnit
posledního	poslední	k2eAgInSc2d1	poslední
grandslamu	grandslam	k1gInSc2	grandslam
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
herní	herní	k2eAgFnSc4d1	herní
absenci	absence	k1gFnSc4	absence
ji	on	k3xPp3gFnSc4	on
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
dvacetiletá	dvacetiletý	k2eAgFnSc1d1	dvacetiletá
Dánka	Dánka	k1gFnSc1	Dánka
Caroline	Carolin	k1gInSc5	Carolin
Wozniacká	Wozniacký	k2eAgNnPc1d1	Wozniacký
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
již	již	k6eAd1	již
neodehrála	odehrát	k5eNaPmAgFnS	odehrát
žádný	žádný	k3yNgInSc4	žádný
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
singlového	singlový	k2eAgInSc2d1	singlový
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
když	když	k8xS	když
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
odehrála	odehrát	k5eAaPmAgFnS	odehrát
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc4	šest
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deblovém	deblový	k2eAgNnSc6d1	deblové
umístění	umístění	k1gNnSc6	umístění
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
odehranými	odehraný	k2eAgFnPc7d1	odehraná
událostmi	událost	k1gFnPc7	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
po	po	k7c6	po
zranění	zranění	k1gNnSc6	zranění
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
pokračující	pokračující	k2eAgNnSc4d1	pokračující
zotavování	zotavování	k1gNnSc4	zotavování
po	po	k7c6	po
zranění	zranění	k1gNnSc6	zranění
nohy	noha	k1gFnSc2	noha
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
z	z	k7c2	z
Hopmanova	Hopmanův	k2eAgInSc2d1	Hopmanův
pojáru	pojár	k1gInSc2	pojár
i	i	k8xC	i
úvodního	úvodní	k2eAgInSc2d1	úvodní
grandslamu	grandslam	k1gInSc2	grandslam
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
hematomu	hematom	k1gInSc2	hematom
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc3	rozvoj
plicní	plicní	k2eAgFnSc2d1	plicní
embolie	embolie	k1gFnSc2	embolie
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
okruh	okruh	k1gInSc4	okruh
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
turnajem	turnaj	k1gInSc7	turnaj
v	v	k7c6	v
Eastbourne	Eastbourn	k1gInSc5	Eastbourn
AEGON	AEGON	kA	AEGON
International	International	k1gFnSc6	International
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
první	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
nad	nad	k7c7	nad
Cvetanou	Cvetaný	k2eAgFnSc7d1	Cvetaný
Pironkovovou	Pironkovová	k1gFnSc7	Pironkovová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
ji	on	k3xPp3gFnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
třetí	třetí	k4xOgFnSc1	třetí
nasazená	nasazený	k2eAgFnSc1d1	nasazená
Ruska	Ruska	k1gFnSc1	Ruska
Věra	Věra	k1gFnSc1	Věra
Zvonarevová	Zvonarevová	k1gFnSc1	Zvonarevová
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
hodinách	hodina	k1gFnPc6	hodina
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgMnSc2	který
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
jako	jako	k9	jako
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
figurovala	figurovat	k5eAaImAgNnP	figurovat
na	na	k7c6	na
26	[number]	k4	26
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
,	,	kIx,	,
pořadatelé	pořadatel	k1gMnPc1	pořadatel
ji	on	k3xPp3gFnSc4	on
nasadili	nasadit	k5eAaPmAgMnP	nasadit
jako	jako	k9	jako
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
sedmičku	sedmička	k1gFnSc4	sedmička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
zdolala	zdolat	k5eAaPmAgFnS	zdolat
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
dvojku	dvojka	k1gFnSc4	dvojka
Aravane	Aravan	k1gMnSc5	Aravan
Rezaï	Rezaï	k1gMnSc5	Rezaï
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Rumunkou	Rumunka	k1gFnSc7	Rumunka
Simonou	Simona	k1gFnSc7	Simona
Halepovou	Halepová	k1gFnSc7	Halepová
a	a	k8xC	a
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Kirilenkovou	Kirilenkový	k2eAgFnSc7d1	Kirilenková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
pak	pak	k6eAd1	pak
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
devítku	devítka	k1gFnSc4	devítka
Marion	Marion	k1gInSc1	Marion
Bartoliovou	Bartoliový	k2eAgFnSc4d1	Bartoliová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
části	část	k1gFnSc6	část
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
amerických	americký	k2eAgInPc6d1	americký
betonech	beton	k1gInPc6	beton
ve	v	k7c6	v
Stanfordu	Stanford	k1gInSc6	Stanford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
nenasazené	nasazený	k2eNgFnSc2d1	nenasazená
hráčky	hráčka	k1gFnSc2	hráčka
přešla	přejít	k5eAaPmAgFnS	přejít
v	v	k7c6	v
úvodních	úvodní	k2eAgNnPc6d1	úvodní
dvou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
přes	přes	k7c4	přes
Anastasii	Anastasie	k1gFnSc4	Anastasie
Rodionovovou	Rodionovový	k2eAgFnSc4d1	Rodionovový
a	a	k8xC	a
Kirilenkovou	Kirilenkový	k2eAgFnSc4d1	Kirilenková
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
dějství	dějství	k1gNnSc6	dějství
turnaje	turnaj	k1gInSc2	turnaj
ji	on	k3xPp3gFnSc4	on
čekala	čekat	k5eAaImAgFnS	čekat
aktuální	aktuální	k2eAgFnSc1d1	aktuální
wimbledonská	wimbledonský	k2eAgFnSc1d1	wimbledonská
finalistka	finalistka	k1gFnSc1	finalistka
Maria	Maria	k1gFnSc1	Maria
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
posledními	poslední	k2eAgMnPc7d1	poslední
čtyřmi	čtyři	k4xCgMnPc7	čtyři
zdolala	zdolat	k5eAaPmAgFnS	zdolat
wimbledonskou	wimbledonský	k2eAgFnSc4d1	wimbledonská
semifinalistku	semifinalistka	k1gFnSc4	semifinalistka
Sabine	Sabin	k1gInSc5	Sabin
Lisickou	Lisický	k2eAgFnSc4d1	Lisická
a	a	k8xC	a
první	první	k4xOgInSc4	první
titul	titul	k1gInSc4	titul
sezóny	sezóna	k1gFnSc2	sezóna
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
oplatila	oplatit	k5eAaPmAgFnS	oplatit
wimbledonskou	wimbledonský	k2eAgFnSc4d1	wimbledonská
porážku	porážka	k1gFnSc4	porážka
Bartoliové	Bartoliový	k2eAgFnSc2d1	Bartoliová
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
sadách	sada	k1gFnPc6	sada
<g/>
.	.	kIx.	.
</s>
<s>
Triumf	triumf	k1gInSc1	triumf
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
představoval	představovat	k5eAaImAgInS	představovat
38	[number]	k4	38
<g/>
.	.	kIx.	.
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
kanadského	kanadský	k2eAgNnSc2d1	kanadské
Rogers	Rogers	k1gInSc4	Rogers
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
Aljony	Aljona	k1gFnSc2	Aljona
Bondarenkové	Bondarenkový	k2eAgFnSc2d1	Bondarenková
<g/>
,	,	kIx,	,
postoupila	postoupit	k5eAaPmAgFnS	postoupit
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
také	také	k9	také
přes	přes	k7c4	přes
Němku	Němka	k1gFnSc4	Němka
Julii	Julie	k1gFnSc4	Julie
Görgesovou	Görgesový	k2eAgFnSc4d1	Görgesová
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
třísadové	třísadový	k2eAgFnPc1d1	třísadový
výhry	výhra	k1gFnPc1	výhra
nad	nad	k7c4	nad
Čeng	Čeng	k1gInSc4	Čeng
Ťie	Ťie	k1gFnSc2	Ťie
a	a	k8xC	a
Lucií	Lucie	k1gFnSc7	Lucie
Šafářovou	Šafářová	k1gFnSc7	Šafářová
<g/>
,	,	kIx,	,
po	po	k7c6	po
nichž	jenž	k3xRgMnPc6	jenž
v	v	k7c6	v
semifinále	semifinále	k1gNnPc6	semifinále
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c6	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
tenistek	tenistka	k1gFnPc2	tenistka
s	s	k7c7	s
nejstabilnějšími	stabilní	k2eAgInPc7d3	nejstabilnější
výkony	výkon	k1gInPc7	výkon
v	v	k7c6	v
probíhající	probíhající	k2eAgFnSc6d1	probíhající
sezóně	sezóna	k1gFnSc6	sezóna
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Běloruskou	Běloruska	k1gFnSc7	Běloruska
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
finále	finále	k1gNnSc2	finále
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
raketě	raketa	k1gFnSc6	raketa
skončila	skončit	k5eAaPmAgFnS	skončit
zlepšující	zlepšující	k2eAgFnSc1d1	zlepšující
se	se	k3xPyFc4	se
australská	australský	k2eAgFnSc1d1	australská
hráčka	hráčka	k1gFnSc1	hráčka
Samantha	Samantha	k1gFnSc1	Samantha
Stosurová	stosurový	k2eAgFnSc1d1	stosurový
a	a	k8xC	a
připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
39	[number]	k4	39
<g/>
.	.	kIx.	.
singlový	singlový	k2eAgInSc4d1	singlový
titul	titul	k1gInSc4	titul
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
cincinnatském	cincinnatský	k2eAgInSc6d1	cincinnatský
turnaji	turnaj	k1gInSc6	turnaj
nejdříve	dříve	k6eAd3	dříve
porazila	porazit	k5eAaPmAgFnS	porazit
Češku	Češka	k1gFnSc4	Češka
Lucii	Lucie	k1gFnSc4	Lucie
Hradeckou	Hradecká	k1gFnSc4	Hradecká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
již	již	k6eAd1	již
nenastoupila	nastoupit	k5eNaPmAgNnP	nastoupit
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
pravého	pravý	k2eAgInSc2d1	pravý
palce	palec	k1gInSc2	palec
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
newyorského	newyorský	k2eAgInSc2d1	newyorský
grandslamu	grandslam	k1gInSc2	grandslam
roku	rok	k1gInSc2	rok
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
vstupovala	vstupovat	k5eAaImAgNnP	vstupovat
jako	jako	k8xS	jako
turnajová	turnajový	k2eAgFnSc1d1	turnajová
dvacet	dvacet	k4xCc4	dvacet
osmička	osmička	k1gFnSc1	osmička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
prošla	projít	k5eAaPmAgFnS	projít
přes	přes	k7c4	přes
mladou	mladý	k2eAgFnSc4d1	mladá
Srbku	Srbka	k1gFnSc4	Srbka
Bojanu	Bojana	k1gFnSc4	Bojana
Jovanovskou	Jovanovský	k2eAgFnSc4d1	Jovanovská
<g/>
,	,	kIx,	,
Nizozemku	Nizozemka	k1gFnSc4	Nizozemka
Michaëllua	Michaëlluus	k1gMnSc4	Michaëlluus
Krajicekovou	Krajiceková	k1gFnSc4	Krajiceková
<g/>
,	,	kIx,	,
Bělorusku	Běloruska	k1gFnSc4	Běloruska
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
a	a	k8xC	a
po	po	k7c6	po
dalších	další	k2eAgFnPc6d1	další
dvousetových	dvousetový	k2eAgFnPc6d1	dvousetový
výhrách	výhra	k1gFnPc6	výhra
nad	nad	k7c4	nad
Anou	Ano	k2eAgFnSc4d1	Ano
Ivanovićovou	Ivanovićová	k1gFnSc4	Ivanovićová
<g/>
,	,	kIx,	,
Anastasií	Anastasie	k1gFnSc7	Anastasie
Pavljučenkovovou	Pavljučenkovová	k1gFnSc7	Pavljučenkovová
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
porazila	porazit	k5eAaPmAgFnS	porazit
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacký	k2eAgFnSc4d1	Wozniacká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
jí	jíst	k5eAaImIp3nS	jíst
pak	pak	k6eAd1	pak
předešlou	předešlý	k2eAgFnSc4d1	předešlá
finálovou	finálový	k2eAgFnSc4d1	finálová
prohru	prohra	k1gFnSc4	prohra
oplatila	oplatit	k5eAaPmAgFnS	oplatit
Samantha	Samantha	k1gFnSc1	Samantha
Stosurová	stosurový	k2eAgFnSc1d1	stosurový
po	po	k7c6	po
přesvědčivém	přesvědčivý	k2eAgInSc6d1	přesvědčivý
výsledku	výsledek	k1gInSc6	výsledek
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
incidentu	incident	k1gInSc3	incident
Williamsové	Williamsová	k1gFnSc2	Williamsová
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
řeckou	řecký	k2eAgFnSc7d1	řecká
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
zápasu	zápas	k1gInSc3	zápas
Evou	Eva	k1gFnSc7	Eva
Asderakiovou	Asderakiový	k2eAgFnSc7d1	Asderakiový
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
první	první	k4xOgFnSc6	první
hře	hra	k1gFnSc6	hra
druhé	druhý	k4xOgFnSc2	druhý
sady	sada	k1gFnSc2	sada
učinila	učinit	k5eAaImAgFnS	učinit
kontroverzní	kontroverzní	k2eAgNnSc4d1	kontroverzní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výměny	výměna	k1gFnSc2	výměna
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
znamenala	znamenat	k5eAaImAgFnS	znamenat
breakbol	breakbol	k1gInSc4	breakbol
pro	pro	k7c4	pro
Australanku	Australanka	k1gFnSc4	Australanka
<g/>
,	,	kIx,	,
Williamsová	Williamsová	k1gFnSc1	Williamsová
po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
míče	míč	k1gInSc2	míč
zakřičela	zakřičet	k5eAaPmAgFnS	zakřičet
s	s	k7c7	s
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
prodlevou	prodleva	k1gFnSc7	prodleva
<g/>
,	,	kIx,	,
v	v	k7c6	v
momentu	moment	k1gInSc6	moment
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Stosurová	stosurový	k2eAgFnSc1d1	stosurový
snažila	snažit	k5eAaImAgFnS	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
na	na	k7c4	na
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nezachytila	zachytit	k5eNaPmAgFnS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
tak	tak	k6eAd1	tak
následovat	následovat	k5eAaImF	následovat
shoda	shoda	k1gFnSc1	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
však	však	k9	však
využila	využít	k5eAaPmAgNnP	využít
pravidla	pravidlo	k1gNnPc1	pravidlo
o	o	k7c6	o
úmyslném	úmyslný	k2eAgNnSc6d1	úmyslné
vyrušení	vyrušení	k1gNnSc6	vyrušení
soupeřky	soupeřka	k1gFnSc2	soupeřka
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
míč	míč	k1gInSc4	míč
přidělila	přidělit	k5eAaPmAgFnS	přidělit
Australance	Australanka	k1gFnSc3	Australanka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
první	první	k4xOgFnSc4	první
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Američanka	Američanka	k1gFnSc1	Američanka
s	s	k7c7	s
protesty	protest	k1gInPc7	protest
neuspěla	uspět	k5eNaPmAgNnP	uspět
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
následně	následně	k6eAd1	následně
prohraného	prohraný	k2eAgInSc2d1	prohraný
zápasu	zápas	k1gInSc2	zápas
<g/>
,	,	kIx,	,
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
s	s	k7c7	s
narážkami	narážka	k1gFnPc7	narážka
vůči	vůči	k7c3	vůči
ní	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
utkání	utkání	k1gNnPc2	utkání
jí	jíst	k5eAaImIp3nS	jíst
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
podat	podat	k5eAaPmF	podat
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
při	při	k7c6	při
následném	následný	k2eAgInSc6d1	následný
komentáři	komentář	k1gInSc6	komentář
míč	míč	k1gInSc4	míč
označila	označit	k5eAaPmAgFnS	označit
"	"	kIx"	"
<g/>
za	za	k7c4	za
vítězný	vítězný	k2eAgInSc4d1	vítězný
(	(	kIx(	(
<g/>
winner	winner	k1gInSc4	winner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
započítán	započítán	k2eAgInSc1d1	započítán
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgMnPc1d1	vrchní
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
hlavní	hlavní	k2eAgMnSc1d1	hlavní
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
utkání	utkání	k1gNnSc2	utkání
zastal	zastat	k5eAaPmAgMnS	zastat
a	a	k8xC	a
inkriminované	inkriminovaný	k2eAgNnSc4d1	inkriminované
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
<g/>
.	.	kIx.	.
<g/>
Finále	finále	k1gNnSc4	finále
na	na	k7c6	na
US	US	kA	US
Open	Open	k1gNnSc1	Open
představovalo	představovat	k5eAaImAgNnS	představovat
její	její	k3xOp3gNnSc4	její
poslední	poslední	k2eAgNnSc4d1	poslední
utkání	utkání	k1gNnSc4	utkání
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
tituly	titul	k1gInPc7	titul
a	a	k8xC	a
bilancí	bilance	k1gFnSc7	bilance
dvacet	dvacet	k4xCc4	dvacet
dva	dva	k4xCgInPc1	dva
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
proher	prohra	k1gFnPc2	prohra
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
odehrála	odehrát	k5eAaPmAgFnS	odehrát
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc4	šest
turnajů	turnaj	k1gInPc2	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Singlový	singlový	k2eAgInSc1d1	singlový
Golden	Goldno	k1gNnPc2	Goldno
Slam	slam	k1gInSc4	slam
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
grandslamy	grandslam	k1gInPc4	grandslam
===	===	k?	===
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
zahájila	zahájit	k5eAaPmAgFnS	zahájit
premiérovou	premiérový	k2eAgFnSc7d1	premiérová
účastí	účast	k1gFnSc7	účast
na	na	k7c4	na
Brisbane	Brisban	k1gMnSc5	Brisban
International	International	k1gMnSc6	International
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zápasu	zápas	k1gInSc2	zápas
s	s	k7c7	s
Bojanou	Bojaný	k2eAgFnSc7d1	Bojaný
Jovanovskou	Jovanovský	k2eAgFnSc7d1	Jovanovská
si	se	k3xPyFc3	se
v	v	k7c6	v
gamu	game	k1gInSc6	game
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
podávala	podávat	k5eAaImAgFnS	podávat
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
<g/>
,	,	kIx,	,
zranila	zranit	k5eAaPmAgFnS	zranit
pravý	pravý	k2eAgInSc4d1	pravý
kotník	kotník	k1gInSc4	kotník
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
odstoupení	odstoupení	k1gNnSc1	odstoupení
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
melbournského	melbournský	k2eAgInSc2d1	melbournský
Australian	Australiany	k1gInPc2	Australiany
Open	Openo	k1gNnPc2	Openo
se	se	k3xPyFc4	se
stihla	stihnout	k5eAaPmAgFnS	stihnout
zotavit	zotavit	k5eAaPmF	zotavit
a	a	k8xC	a
jako	jako	k8xS	jako
dvanáctá	dvanáctý	k4xOgFnSc1	dvanáctý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
neztratila	ztratit	k5eNaPmAgFnS	ztratit
v	v	k7c6	v
úvodních	úvodní	k2eAgNnPc6d1	úvodní
třech	tři	k4xCgNnPc6	tři
kolech	kolo	k1gNnPc6	kolo
žádný	žádný	k3yNgInSc4	žádný
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
však	však	k9	však
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Rusce	Ruska	k1gFnSc3	Ruska
Jekatěrině	Jekatěrina	k1gFnSc3	Jekatěrina
Makarovové	Makarovová	k1gFnSc2	Makarovová
poměrem	poměr	k1gInSc7	poměr
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odehrála	odehrát	k5eAaPmAgFnS	odehrát
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
dvouhru	dvouhra	k1gFnSc4	dvouhra
za	za	k7c4	za
americký	americký	k2eAgInSc4d1	americký
tým	tým	k1gInSc4	tým
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
II	II	kA	II
Fed	Fed	k1gFnSc1	Fed
Cupu	cup	k1gInSc2	cup
nad	nad	k7c7	nad
Anastasií	Anastasie	k1gFnSc7	Anastasie
Jakimovovou	Jakimovový	k2eAgFnSc7d1	Jakimovový
po	po	k7c6	po
setech	set	k1gInPc6	set
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
pomohla	pomoct	k5eAaPmAgFnS	pomoct
Američankám	Američanka	k1gFnPc3	Američanka
k	k	k7c3	k
výhře	výhra	k1gFnSc3	výhra
nad	nad	k7c7	nad
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
postupu	postup	k1gInSc6	postup
do	do	k7c2	do
baráže	baráž	k1gFnSc2	baráž
o	o	k7c4	o
Světovou	světový	k2eAgFnSc4d1	světová
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
floridském	floridský	k2eAgNnSc6d1	floridské
Miami	Miami	k1gNnSc6	Miami
Masters	Mastersa	k1gFnPc2	Mastersa
jako	jako	k8xC	jako
desátá	desátá	k1gFnSc1	desátá
nasazená	nasazený	k2eAgFnSc1d1	nasazená
oplatila	oplatit	k5eAaPmAgFnS	oplatit
porážku	porážka	k1gFnSc4	porážka
z	z	k7c2	z
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
Australance	Australanka	k1gFnSc6	Australanka
Stosurové	stosurový	k2eAgNnSc1d1	stosurový
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vítězství	vítězství	k1gNnSc4	vítězství
jí	on	k3xPp3gFnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnSc2d1	světová
desítky	desítka	k1gFnSc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
však	však	k9	však
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Dánku	Dánka	k1gFnSc4	Dánka
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacka	k1gFnSc7	Wozniacka
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
poměrem	poměr	k1gInSc7	poměr
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
týden	týden	k1gInSc4	týden
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
premiérovou	premiérový	k2eAgFnSc4d1	premiérová
finálovou	finálový	k2eAgFnSc4d1	finálová
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
hraném	hraný	k2eAgInSc6d1	hraný
na	na	k7c6	na
zelené	zelený	k2eAgFnSc6d1	zelená
antuce	antuka	k1gFnSc6	antuka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přešla	přejít	k5eAaPmAgFnS	přejít
hladce	hladko	k6eAd1	hladko
přes	přes	k7c4	přes
Stosurovou	stosurový	k2eAgFnSc4d1	stosurový
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
a	a	k8xC	a
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
dalším	další	k2eAgInSc7d1	další
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
výsledkem	výsledek	k1gInSc7	výsledek
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
nad	nad	k7c4	nad
Luciíe	Luciíe	k1gFnSc4	Luciíe
Šafářovou	Šafářová	k1gFnSc4	Šafářová
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jubilejní	jubilejní	k2eAgNnSc4d1	jubilejní
40	[number]	k4	40
<g/>
.	.	kIx.	.
titul	titul	k1gInSc1	titul
její	její	k3xOp3gFnSc2	její
kariéry	kariéra	k1gFnSc2	kariéra
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
postoupila	postoupit	k5eAaPmAgFnS	postoupit
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropskou	evropský	k2eAgFnSc4d1	Evropská
sezónu	sezóna	k1gFnSc4	sezóna
zahájila	zahájit	k5eAaPmAgFnS	zahájit
na	na	k7c6	na
modré	modrý	k2eAgFnSc6d1	modrá
antuce	antuka	k1gFnSc6	antuka
turnaje	turnaj	k1gInSc2	turnaj
Mutua	Mutu	k1gInSc2	Mutu
Madrileñ	Madrileñ	k1gFnSc2	Madrileñ
Madrid	Madrid	k1gInSc1	Madrid
Open	Open	k1gInSc1	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
jako	jako	k8xS	jako
nasazená	nasazený	k2eAgFnSc1d1	nasazená
devítka	devítka	k1gFnSc1	devítka
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zahrála	zahrát	k5eAaPmAgFnS	zahrát
finále	finále	k1gNnSc4	finále
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
utkání	utkání	k1gNnSc4	utkání
s	s	k7c7	s
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacký	k2eAgFnSc4d1	Wozniacká
poměrem	poměr	k1gInSc7	poměr
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1	čtvrtfinále
znamenalo	znamenat	k5eAaImAgNnS	znamenat
vyřazení	vyřazení	k1gNnSc3	vyřazení
soupeřky	soupeřka	k1gFnSc2	soupeřka
a	a	k8xC	a
světové	světový	k2eAgFnSc2d1	světová
dvojky	dvojka	k1gFnSc2	dvojka
Marie	Maria	k1gFnSc2	Maria
Šarapovové	Šarapovová	k1gFnSc2	Šarapovová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
přes	přes	k7c4	přes
překvapení	překvapení	k1gNnSc4	překvapení
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
kvalifikantku	kvalifikantka	k1gFnSc4	kvalifikantka
Lucii	Lucie	k1gFnSc4	Lucie
Hradeckou	Hradecká	k1gFnSc4	Hradecká
<g/>
,	,	kIx,	,
poskočila	poskočit	k5eAaPmAgFnS	poskočit
na	na	k7c6	na
šesté	šestý	k4xOgFnSc6	šestý
místo	místo	k7c2	místo
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
madridský	madridský	k2eAgInSc4d1	madridský
titul	titul	k1gInSc4	titul
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
snadno	snadno	k6eAd1	snadno
poradila	poradit	k5eAaPmAgFnS	poradit
se	s	k7c7	s
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
Viktorií	Viktoria	k1gFnSc7	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc7d1	Azarenková
výsledkem	výsledek	k1gInSc7	výsledek
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získala	získat	k5eAaPmAgFnS	získat
41	[number]	k4	41
<g/>
.	.	kIx.	.
singlovou	singlový	k2eAgFnSc4d1	singlová
turnajovou	turnajový	k2eAgFnSc4d1	turnajová
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následně	následně	k6eAd1	následně
odletěla	odletět	k5eAaPmAgFnS	odletět
na	na	k7c4	na
Rome	Rom	k1gMnSc5	Rom
Masters	Masters	k1gInSc4	Masters
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jako	jako	k9	jako
devátá	devátý	k4xOgFnSc1	devátý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
došla	dojít	k5eAaPmAgFnS	dojít
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
začátkem	začátek	k1gInSc7	začátek
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
pro	pro	k7c4	pro
zádové	zádový	k2eAgNnSc4d1	zádové
zranění	zranění	k1gNnSc4	zranění
a	a	k8xC	a
Číňanka	Číňanka	k1gFnSc1	Číňanka
Li	li	k8xS	li
Na	na	k7c6	na
prošla	projít	k5eAaPmAgFnS	projít
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
znovu	znovu	k6eAd1	znovu
postoupila	postoupit	k5eAaPmAgFnS	postoupit
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
pátou	pátý	k4xOgFnSc4	pátý
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
grandslamu	grandslam	k1gInSc6	grandslam
French	French	k1gMnSc1	French
Open	Open	k1gNnSc1	Open
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
favoritek	favoritka	k1gFnPc2	favoritka
negativní	negativní	k2eAgInSc1d1	negativní
osobní	osobní	k2eAgInSc1d1	osobní
rekord	rekord	k1gInSc1	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
páté	pátý	k4xOgFnSc2	pátý
nasazené	nasazený	k2eAgFnSc2d1	nasazená
tenistky	tenistka	k1gFnSc2	tenistka
skončila	skončit	k5eAaPmAgFnS	skončit
již	již	k6eAd1	již
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
111	[number]	k4	111
<g/>
.	.	kIx.	.
hráčky	hráčka	k1gFnSc2	hráčka
světa	svět	k1gInSc2	svět
Francouzky	Francouzka	k1gFnSc2	Francouzka
Virginie	Virginie	k1gFnSc2	Virginie
Razzanové	Razzanová	k1gFnSc2	Razzanová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
první	první	k4xOgFnSc4	první
prohru	prohra	k1gFnSc4	prohra
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
dvouhry	dvouhra	k1gFnSc2	dvouhra
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Roland	Roland	k1gInSc1	Roland
Garros	Garrosa	k1gFnPc2	Garrosa
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
představoval	představovat	k5eAaImAgInS	představovat
47	[number]	k4	47
<g/>
.	.	kIx.	.
start	start	k1gInSc4	start
v	v	k7c6	v
singlové	singlový	k2eAgFnSc6d1	singlová
grandslamové	grandslamový	k2eAgFnSc6d1	grandslamová
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
smíšené	smíšený	k2eAgFnSc2d1	smíšená
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
pak	pak	k6eAd1	pak
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
Australian	Australiana	k1gFnPc2	Australiana
Open	Openo	k1gNnPc2	Openo
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
krajanem	krajan	k1gMnSc7	krajan
Bobem	Bob	k1gMnSc7	Bob
Bryanem	Bryan	k1gMnSc7	Bryan
však	však	k9	však
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
v	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
fázi	fáze	k1gFnSc6	fáze
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačili	stačit	k5eNaBmAgMnP	stačit
na	na	k7c4	na
argentinský	argentinský	k2eAgInSc4d1	argentinský
pár	pár	k1gInSc4	pár
Gisela	Gisela	k1gFnSc1	Gisela
Dulková	Dulková	k1gFnSc1	Dulková
a	a	k8xC	a
Eduardo	Eduardo	k1gNnSc1	Eduardo
Schwank	Schwanka	k1gFnPc2	Schwanka
po	po	k7c6	po
setech	set	k1gInPc6	set
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
a	a	k8xC	a
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
travnatého	travnatý	k2eAgInSc2d1	travnatý
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
jako	jako	k9	jako
turnajová	turnajový	k2eAgFnSc1d1	turnajová
šestka	šestka	k1gFnSc1	šestka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Barborou	Barbora	k1gFnSc7	Barbora
Záhlavovou-Strýcovou	Záhlavovou-Strýcův	k2eAgFnSc7d1	Záhlavovou-Strýcův
a	a	k8xC	a
následně	následně	k6eAd1	následně
také	také	k9	také
s	s	k7c7	s
Melindou	Melinda	k1gFnSc7	Melinda
Czinkovou	Czinková	k1gFnSc7	Czinková
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
dvacátou	dvacátý	k4xOgFnSc4	dvacátý
pátou	pátá	k1gFnSc4	pátá
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
čínskou	čínský	k2eAgFnSc4d1	čínská
hráčku	hráčka	k1gFnSc4	hráčka
Čeng	Čeng	k1gMnSc1	Čeng
Ťie	Ťie	k1gMnSc1	Ťie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vítězném	vítězný	k2eAgNnSc6d1	vítězné
třísetovém	třísetový	k2eAgNnSc6d1	třísetové
utkání	utkání	k1gNnSc6	utkání
nastřílela	nastřílet	k5eAaPmAgFnS	nastřílet
23	[number]	k4	23
es	es	k1gNnPc2	es
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nový	nový	k2eAgInSc4d1	nový
ženský	ženský	k2eAgInSc4d1	ženský
wimbledonský	wimbledonský	k2eAgInSc4d1	wimbledonský
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
Kazašku	Kazaška	k1gFnSc4	Kazaška
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
Švedovovou	Švedovová	k1gFnSc4	Švedovová
a	a	k8xC	a
mezi	mezi	k7c7	mezi
posledními	poslední	k2eAgMnPc7d1	poslední
osmi	osm	k4xCc7	osm
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgNnP	střetnout
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
obhájkyní	obhájkyně	k1gFnSc7	obhájkyně
titulu	titul	k1gInSc2	titul
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitová	k1gFnSc7	Kvitová
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
nepovolila	povolit	k5eNaPmAgFnS	povolit
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
semifinálové	semifinálový	k2eAgFnSc6d1	semifinálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
světovou	světový	k2eAgFnSc7d1	světová
dvojkou	dvojka	k1gFnSc7	dvojka
Viktorií	Viktoria	k1gFnSc7	Viktoria
Azarenkovouá	Azarenkovouá	k1gFnSc1	Azarenkovouá
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
a	a	k8xC	a
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
66	[number]	k4	66
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
posunula	posunout	k5eAaPmAgFnS	posunout
svůj	svůj	k3xOyFgInSc4	svůj
nedávný	dávný	k2eNgInSc4d1	nedávný
rekord	rekord	k1gInSc4	rekord
na	na	k7c4	na
24	[number]	k4	24
es	es	k1gNnPc2	es
zahraných	zahraný	k2eAgMnPc2d1	zahraný
v	v	k7c6	v
jediném	jediné	k1gNnSc6	jediné
utkání	utkání	k1gNnSc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
mísu	mísa	k1gFnSc4	mísa
Venus	Venus	k1gMnSc1	Venus
Rosewater	Rosewater	k1gMnSc1	Rosewater
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
třetí	třetí	k4xOgFnSc4	třetí
hráčku	hráčka	k1gFnSc4	hráčka
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
Polku	Polka	k1gFnSc4	Polka
Agnieszku	Agnieszka	k1gFnSc4	Agnieszka
Radwańskou	Radwańská	k1gFnSc4	Radwańská
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
debutová	debutový	k2eAgFnSc1d1	debutová
účast	účast	k1gFnSc1	účast
v	v	k7c6	v
grandslamovém	grandslamový	k2eAgNnSc6d1	grandslamové
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
Williamsová	Williamsová	k1gFnSc1	Williamsová
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
výsledkem	výsledek	k1gInSc7	výsledek
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
tak	tak	k9	tak
14	[number]	k4	14
<g/>
.	.	kIx.	.
singlový	singlový	k2eAgInSc1d1	singlový
grandslam	grandslam	k1gInSc1	grandslam
kariéry	kariéra	k1gFnSc2	kariéra
a	a	k8xC	a
pátý	pátý	k4xOgMnSc1	pátý
wimbledonský	wimbledonský	k2eAgMnSc1d1	wimbledonský
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
ve	v	k7c6	v
wimbledonské	wimbledonský	k2eAgFnSc6d1	wimbledonská
statistice	statistika	k1gFnSc6	statistika
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
starší	starý	k2eAgFnSc1d2	starší
sestře	sestra	k1gFnSc3	sestra
Venus	Venus	k1gInSc4	Venus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
wimbledonské	wimbledonský	k2eAgFnSc6d1	wimbledonská
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Venus	Venus	k1gInSc1	Venus
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gInSc4	jejich
první	první	k4xOgInSc4	první
deblový	deblový	k2eAgInSc4d1	deblový
turnaj	turnaj	k1gInSc4	turnaj
od	od	k7c2	od
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolaly	zdolat	k5eAaPmAgFnP	zdolat
šestý	šestý	k4xOgMnSc1	šestý
nasazený	nasazený	k2eAgInSc1d1	nasazený
pár	pár	k4xCyI	pár
Češek	Češka	k1gFnPc2	Češka
Andrea	Andrea	k1gFnSc1	Andrea
Hlaváčková	Hlaváčková	k1gFnSc1	Hlaváčková
a	a	k8xC	a
Lucie	Lucie	k1gFnSc1	Lucie
Hradecká	Hradecká	k1gFnSc1	Hradecká
výsledkem	výsledek	k1gInSc7	výsledek
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
získaly	získat	k5eAaPmAgFnP	získat
pátý	pátý	k4xOgInSc4	pátý
titul	titul	k1gInSc4	titul
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
soutěži	soutěž	k1gFnSc6	soutěž
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
třináctý	třináctý	k4xOgInSc4	třináctý
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
<g/>
.	.	kIx.	.
<g/>
Týden	týden	k1gInSc4	týden
po	po	k7c6	po
londýnské	londýnský	k2eAgFnSc6d1	londýnská
události	událost	k1gFnSc6	událost
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
Stanfordu	Stanford	k1gInSc6	Stanford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
titul	titul	k1gInSc4	titul
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
nasazené	nasazený	k2eAgFnSc2d1	nasazená
jedničky	jednička	k1gFnSc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
finále	finále	k1gNnSc2	finále
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
se	s	k7c7	s
ztrátou	ztráta	k1gFnSc7	ztráta
pouhých	pouhý	k2eAgInPc2d1	pouhý
deseti	deset	k4xCc2	deset
gamů	game	k1gInPc2	game
z	z	k7c2	z
předešlé	předešlý	k2eAgFnSc2d1	předešlá
části	část	k1gFnSc2	část
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
ženském	ženský	k2eAgInSc6d1	ženský
turnaji	turnaj	k1gInSc6	turnaj
utkaly	utkat	k5eAaPmAgInP	utkat
dvě	dva	k4xCgFnPc4	dva
Američanky	Američanka	k1gFnPc4	Američanka
<g/>
.	.	kIx.	.
</s>
<s>
Williamsová	Williamsová	k1gFnSc1	Williamsová
přehrála	přehrát	k5eAaPmAgFnS	přehrát
krajanku	krajanka	k1gFnSc4	krajanka
Coco	Coco	k6eAd1	Coco
Vandewegheovou	Vandewegheův	k2eAgFnSc7d1	Vandewegheův
<g/>
,	,	kIx,	,
startující	startující	k2eAgFnSc7d1	startující
jako	jako	k8xS	jako
šťastnou	šťastný	k2eAgFnSc7d1	šťastná
poraženou	poražený	k2eAgFnSc7d1	poražená
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Výhra	výhra	k1gFnSc1	výhra
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
znamenala	znamenat	k5eAaImAgFnS	znamenat
43	[number]	k4	43
<g/>
.	.	kIx.	.
titul	titul	k1gInSc4	titul
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
Letní	letní	k2eAgFnPc1d1	letní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
na	na	k7c6	na
trávě	tráva	k1gFnSc6	tráva
v	v	k7c6	v
All	All	k1gFnSc6	All
England	Englanda	k1gFnPc2	Englanda
Clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
Semifinále	semifinále	k1gNnSc1	semifinále
ženské	ženský	k2eAgFnSc2d1	ženská
dvouhry	dvouhra	k1gFnSc2	dvouhra
proti	proti	k7c3	proti
běloruské	běloruský	k2eAgFnSc3d1	Běloruská
světové	světový	k2eAgFnSc3d1	světová
jedničce	jednička	k1gFnSc3	jednička
Azarenkové	Azarenkový	k2eAgFnSc2d1	Azarenková
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
znamenalo	znamenat	k5eAaImAgNnS	znamenat
hladkou	hladký	k2eAgFnSc4d1	hladká
výhru	výhra	k1gFnSc4	výhra
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
v	v	k7c6	v
nejjednoznačnějším	jednoznačný	k2eAgInSc6d3	nejjednoznačnější
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Mariu	Mario	k1gMnSc3	Mario
Šarapovovou	Šarapovový	k2eAgFnSc4d1	Šarapovová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šesti	šest	k4xCc2	šest
zápasů	zápas	k1gInPc2	zápas
Williamsová	Williamsová	k1gFnSc1	Williamsová
ztratila	ztratit	k5eAaPmAgFnS	ztratit
pouze	pouze	k6eAd1	pouze
17	[number]	k4	17
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
poměr	poměr	k1gInSc4	poměr
81	[number]	k4	81
%	%	kIx~	%
vyhraných	vyhraný	k2eAgInPc2d1	vyhraný
gamů	game	k1gInPc2	game
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
turnaji	turnaj	k1gInSc6	turnaj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
,	,	kIx,	,
neprohrála	prohrát	k5eNaPmAgFnS	prohrát
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
Ziskem	zisk	k1gInSc7	zisk
zlaté	zlatý	k2eAgFnSc2d1	zlatá
medaile	medaile	k1gFnSc2	medaile
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
se	se	k3xPyFc4	se
po	po	k7c6	po
Steffi	Steffi	k1gFnSc6	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
stala	stát	k5eAaPmAgFnS	stát
druhou	druhý	k4xOgFnSc7	druhý
tenistkou	tenistka	k1gFnSc7	tenistka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
završila	završit	k5eAaPmAgFnS	završit
Golden	Goldna	k1gFnPc2	Goldna
Slam	slam	k1gInSc1	slam
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
čtyřech	čtyři	k4xCgInPc6	čtyři
grandslamech	grandslam	k1gInPc6	grandslam
a	a	k8xC	a
také	také	k9	také
soutěž	soutěž	k1gFnSc4	soutěž
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
ženské	ženský	k2eAgFnSc6d1	ženská
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc1	Venus
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
soutěž	soutěž	k1gFnSc4	soutěž
opět	opět	k6eAd1	opět
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
předtím	předtím	k6eAd1	předtím
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Finálový	finálový	k2eAgInSc1d1	finálový
zápas	zápas	k1gInSc1	zápas
měl	mít	k5eAaImAgInS	mít
dokonce	dokonce	k9	dokonce
stejné	stejný	k2eAgNnSc4d1	stejné
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
když	když	k8xS	když
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
nasazený	nasazený	k2eAgInSc1d1	nasazený
pár	pár	k1gInSc1	pár
Hlaváčková	Hlaváčková	k1gFnSc1	Hlaváčková
s	s	k7c7	s
Hradeckou	Hradecká	k1gFnSc7	Hradecká
<g/>
.	.	kIx.	.
</s>
<s>
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
získaly	získat	k5eAaPmAgFnP	získat
Američanky	Američanka	k1gFnPc1	Američanka
výhrou	výhra	k1gFnSc7	výhra
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
sadách	sada	k1gFnPc6	sada
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgInP	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
prvními	první	k4xOgFnPc7	první
tenistkami	tenistka	k1gFnPc7	tenistka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mužských	mužský	k2eAgFnPc2d1	mužská
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
soutěž	soutěž	k1gFnSc4	soutěž
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Předešlé	předešlý	k2eAgInPc1d1	předešlý
dva	dva	k4xCgInPc1	dva
tituly	titul	k1gInPc1	titul
získaly	získat	k5eAaPmAgInP	získat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
a	a	k8xC	a
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
obě	dva	k4xCgFnPc1	dva
sourozenkyně	sourozenkyně	k1gFnPc1	sourozenkyně
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
jako	jako	k9	jako
první	první	k4xOgFnPc1	první
tenistky	tenistka	k1gFnPc1	tenistka
či	či	k8xC	či
tenisté	tenista	k1gMnPc1	tenista
získat	získat	k5eAaPmF	získat
na	na	k7c6	na
olympijských	olympijský	k2eAgInPc6d1	olympijský
turnajích	turnaj	k1gInPc6	turnaj
čtyři	čtyři	k4xCgInPc4	čtyři
zlaté	zlatý	k2eAgInPc4d1	zlatý
kovy	kov	k1gInPc4	kov
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
jako	jako	k9	jako
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
nasazená	nasazený	k2eAgFnSc1d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc4d1	předchozí
formu	forma	k1gFnSc4	forma
z	z	k7c2	z
letní	letní	k2eAgFnSc2d1	letní
části	část	k1gFnSc2	část
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
sérií	série	k1gFnSc7	série
23	[number]	k4	23
vítězných	vítězný	k2eAgInPc2d1	vítězný
gamů	game	k1gInPc2	game
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zahájila	zahájit	k5eAaPmAgFnS	zahájit
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
setu	set	k1gInSc6	set
utkání	utkání	k1gNnSc2	utkání
třetího	třetí	k4xOgNnSc2	třetí
kola	kolo	k1gNnSc2	kolo
s	s	k7c7	s
Jekatěrinou	Jekatěrin	k2eAgFnSc7d1	Jekatěrina
Makarovovou	Makarovová	k1gFnSc7	Makarovová
a	a	k8xC	a
ukončila	ukončit	k5eAaPmAgFnS	ukončit
ji	on	k3xPp3gFnSc4	on
až	až	k9	až
při	při	k7c6	při
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
první	první	k4xOgFnSc2	první
sady	sada	k1gFnSc2	sada
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
proti	proti	k7c3	proti
Aně	Aně	k1gFnSc3	Aně
Ivanovićové	Ivanovićová	k1gFnSc2	Ivanovićová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
semifinále	semifinále	k1gNnSc4	semifinále
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Italkou	Italka	k1gFnSc7	Italka
Erraniovou	Erraniový	k2eAgFnSc7d1	Erraniová
a	a	k8xC	a
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
zdolala	zdolat	k5eAaPmAgFnS	zdolat
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgNnSc6d1	rozhodující
dějství	dějství	k1gNnSc6	dějství
již	již	k6eAd1	již
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
na	na	k7c4	na
gamy	game	k1gInPc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
však	však	k9	však
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
čtyři	čtyři	k4xCgFnPc4	čtyři
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
utkání	utkání	k1gNnPc4	utkání
dovedla	dovést	k5eAaPmAgFnS	dovést
do	do	k7c2	do
vítězného	vítězný	k2eAgInSc2d1	vítězný
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Zařadila	zařadit	k5eAaPmAgFnS	zařadit
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	grafový	k2eAgFnSc2d1	grafová
a	a	k8xC	a
starší	starý	k2eAgFnSc2d2	starší
sestry	sestra	k1gFnSc2	sestra
Venus	Venus	k1gMnSc1	Venus
Williamsové	Williamsové	k2eAgMnSc1d1	Williamsové
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třetí	třetí	k4xOgFnSc1	třetí
tenistka	tenistka	k1gFnSc1	tenistka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
sezóně	sezóna	k1gFnSc6	sezóna
vyhrát	vyhrát	k5eAaPmF	vyhrát
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
<g/>
,	,	kIx,	,
letní	letní	k2eAgNnSc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
a	a	k8xC	a
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
istanbulským	istanbulský	k2eAgInSc7d1	istanbulský
Turnajem	turnaj	k1gInSc7	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgMnSc2	jenž
vstupovala	vstupovat	k5eAaImAgFnS	vstupovat
jako	jako	k9	jako
třetí	třetí	k4xOgFnSc7	třetí
nasazená	nasazený	k2eAgFnSc1d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
porazila	porazit	k5eAaPmAgFnS	porazit
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
,	,	kIx,	,
Li	li	k8xS	li
i	i	k9	i
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
<g/>
,	,	kIx,	,
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
Agnieszku	Agnieszka	k1gFnSc4	Agnieszka
Radwańskou	Radwańský	k2eAgFnSc4d1	Radwańský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Šarapovovou	Šarapovová	k1gFnSc7	Šarapovová
po	po	k7c6	po
setech	set	k1gInPc6	set
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Připsala	připsat	k5eAaPmAgFnS	připsat
si	se	k3xPyFc3	se
tak	tak	k9	tak
třetí	třetí	k4xOgNnSc4	třetí
vítězství	vítězství	k1gNnSc4	vítězství
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Kralování	kralování	k1gNnSc2	kralování
ženskému	ženský	k2eAgInSc3d1	ženský
tenisu	tenis	k1gInSc3	tenis
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
Grand	grand	k1gMnSc1	grand
Slamy	slam	k1gInPc5	slam
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
sezóny	sezóna	k1gFnSc2	sezóna
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
jedenáct	jedenáct	k4xCc4	jedenáct
singlových	singlový	k2eAgInPc2d1	singlový
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
jediného	jediný	k2eAgInSc2d1	jediný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
již	již	k6eAd1	již
neopustila	opustit	k5eNaPmAgFnS	opustit
a	a	k8xC	a
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
neprohrála	prohrát	k5eNaPmAgFnS	prohrát
žádný	žádný	k3yNgInSc4	žádný
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc4	rok
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
titulem	titul	k1gInSc7	titul
na	na	k7c4	na
Brisbane	Brisban	k1gMnSc5	Brisban
International	International	k1gMnSc5	International
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Anastasii	Anastasie	k1gFnSc4	Anastasie
Pavljučenkovovou	Pavljučenkovový	k2eAgFnSc4d1	Pavljučenkovový
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
turnaji	turnaj	k1gInSc6	turnaj
ztratila	ztratit	k5eAaPmAgFnS	ztratit
pouze	pouze	k6eAd1	pouze
17	[number]	k4	17
her	hra	k1gFnPc2	hra
a	a	k8xC	a
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
favoritka	favoritka	k1gFnSc1	favoritka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
ženské	ženský	k2eAgFnSc2d1	ženská
dvouhry	dvouhra	k1gFnSc2	dvouhra
proti	proti	k7c3	proti
Gallovitsové-Hallové	Gallovitsové-Hallová	k1gFnSc3	Gallovitsové-Hallová
si	se	k3xPyFc3	se
však	však	k9	však
poranila	poranit	k5eAaPmAgFnS	poranit
kotník	kotník	k1gInSc4	kotník
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
prošla	projít	k5eAaPmAgFnS	projít
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
ji	on	k3xPp3gFnSc4	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Sloane	Sloan	k1gMnSc5	Sloan
Stephensová	Stephensový	k2eAgFnSc1d1	Stephensový
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
a	a	k8xC	a
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
Čtvrtfinálovou	čtvrtfinálový	k2eAgFnSc7d1	čtvrtfinálová
výhrou	výhra	k1gFnSc7	výhra
na	na	k7c6	na
únorovém	únorový	k2eAgInSc6d1	únorový
Qatar	Qatar	k1gInSc1	Qatar
Total	totat	k5eAaImAgInS	totat
Open	Open	k1gInSc4	Open
nad	nad	k7c7	nad
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitová	k1gFnSc7	Kvitová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pošesté	pošesté	k4xO	pošesté
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
klasifikace	klasifikace	k1gFnSc2	klasifikace
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
však	však	k9	však
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
šňůru	šňůra	k1gFnSc4	šňůra
34	[number]	k4	34
<g/>
zápasové	zápasový	k2eAgFnSc6d1	zápasová
neporazitelnosti	neporazitelnost	k1gFnSc6	neporazitelnost
na	na	k7c4	na
Sony	Sony	kA	Sony
Open	Open	k1gNnSc4	Open
Tennis	Tennis	k1gInSc1	Tennis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
zdolání	zdolání	k1gNnSc6	zdolání
Marii	Maria	k1gFnSc4	Maria
Šarapovové	Šarapovové	k2eAgFnSc4d1	Šarapovové
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
slavila	slavit	k5eAaImAgFnS	slavit
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
triumfem	triumf	k1gInSc7	triumf
na	na	k7c4	na
Family	Famil	k1gMnPc4	Famil
Circle	Circle	k1gFnSc2	Circle
Cupu	cup	k1gInSc2	cup
po	po	k7c6	po
finálovém	finálový	k2eAgNnSc6d1	finálové
vítězství	vítězství	k1gNnSc6	vítězství
nad	nad	k7c7	nad
Jelenou	Jelena	k1gFnSc7	Jelena
Jankovićovou	Jankovićová	k1gFnSc7	Jankovićová
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,,	,,	k?	,,
trofejí	trofej	k1gFnPc2	trofej
z	z	k7c2	z
Mutua	Mutuum	k1gNnSc2	Mutuum
Madrid	Madrid	k1gInSc1	Madrid
Open	Openo	k1gNnPc2	Openo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
nestačila	stačit	k5eNaBmAgFnS	stačit
opět	opět	k5eAaPmF	opět
Šarapovová	Šarapovová	k1gFnSc1	Šarapovová
6	[number]	k4	6
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
turnajovou	turnajový	k2eAgFnSc7d1	turnajová
výhrou	výhra	k1gFnSc7	výhra
na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Internazionali	Internazionali	k1gMnSc3	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Výbornou	výborný	k2eAgFnSc4d1	výborná
formu	forma	k1gFnSc4	forma
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
svým	svůj	k3xOyFgInSc7	svůj
druhým	druhý	k4xOgInSc7	druhý
titulem	titul	k1gInSc7	titul
z	z	k7c2	z
French	Frencha	k1gFnPc2	Frencha
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgNnSc6d1	rozhodující
střetnutí	střetnutí	k1gNnSc6	střetnutí
zdolala	zdolat	k5eAaPmAgFnS	zdolat
obhájkyni	obhájkyně	k1gFnSc4	obhájkyně
titulu	titul	k1gInSc2	titul
Šarapovovou	Šarapovový	k2eAgFnSc4d1	Šarapovová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Martině	Martina	k1gFnSc6	Martina
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc2	Chris
Evertové	Evertová	k1gFnSc2	Evertová
a	a	k8xC	a
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
turnajů	turnaj	k1gInPc2	turnaj
alespoň	alespoň	k9	alespoň
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
neporazitelnost	neporazitelnost	k1gFnSc1	neporazitelnost
ukončila	ukončit	k5eAaPmAgFnS	ukončit
až	až	k9	až
v	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
Němka	Němka	k1gFnSc1	Němka
Sabine	Sabin	k1gInSc5	Sabin
Lisická	Lisický	k2eAgFnSc1d1	Lisická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
premiérové	premiérový	k2eAgNnSc4d1	premiérové
vítězství	vítězství	k1gNnSc4	vítězství
kariéry	kariéra	k1gFnSc2	kariéra
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
International	International	k1gFnSc2	International
na	na	k7c4	na
Swedish	Swedish	k1gInSc4	Swedish
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
poradila	poradit	k5eAaPmAgFnS	poradit
se	s	k7c7	s
švédskou	švédský	k2eAgFnSc7d1	švédská
Johannou	Johanný	k2eAgFnSc7d1	Johanný
Larssonovou	Larssonová	k1gFnSc7	Larssonová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Trofej	trofej	k1gFnSc1	trofej
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2013	[number]	k4	2013
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
neporažena	poražen	k2eNgFnSc1d1	neporažena
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgMnPc1d1	letní
US	US	kA	US
Open	Open	k1gMnSc1	Open
Series	Series	k1gMnSc1	Series
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
na	na	k7c4	na
Rogers	Rogers	k1gInSc4	Rogers
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
získala	získat	k5eAaPmAgFnS	získat
osmý	osmý	k4xOgInSc4	osmý
triumf	triumf	k1gInSc4	triumf
roku	rok	k1gInSc2	rok
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c4	nad
Soraně	Soraň	k1gFnPc4	Soraň
Cîrsteaové	Cîrsteaová	k1gFnSc2	Cîrsteaová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
se	se	k3xPyFc4	se
probojovala	probojovat	k5eAaPmAgFnS	probojovat
další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
na	na	k7c4	na
Cincinnati	Cincinnati	k1gFnSc4	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
a	a	k8xC	a
finálová	finálový	k2eAgFnSc1d1	finálová
účast	účast	k1gFnSc1	účast
jí	on	k3xPp3gFnSc3	on
zajistila	zajistit	k5eAaPmAgFnS	zajistit
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sérii	série	k1gFnSc6	série
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc4	turnaj
však	však	k9	však
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zápase	zápas	k1gInSc6	zápas
byla	být	k5eAaImAgFnS	být
nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
Viktoria	Viktoria	k1gFnSc1	Viktoria
Azarenková	Azarenkový	k2eAgFnSc1d1	Azarenková
<g/>
.	.	kIx.	.
</s>
<s>
Sedmnáctou	sedmnáctý	k4xOgFnSc4	sedmnáctý
grandslamovou	grandslamový	k2eAgFnSc4d1	grandslamová
trofej	trofej	k1gFnSc4	trofej
kariéry	kariéra	k1gFnSc2	kariéra
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
oplatila	oplatit	k5eAaPmAgFnS	oplatit
Azarenkové	Azarenkový	k2eAgFnPc4d1	Azarenková
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čínském	čínský	k2eAgInSc6d1	čínský
China	China	k1gFnSc1	China
Open	Openo	k1gNnPc2	Openo
pak	pak	k6eAd1	pak
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
podesáté	podesáté	k4xO	podesáté
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
duelu	duel	k1gInSc6	duel
o	o	k7c4	o
titul	titul	k1gInSc4	titul
hladce	hladko	k6eAd1	hladko
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Jelenu	Jelena	k1gFnSc4	Jelena
Jankovićovou	Jankovićový	k2eAgFnSc4d1	Jankovićový
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
tak	tak	k9	tak
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
sezóny	sezóna	k1gFnSc2	sezóna
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
dvouciferný	dvouciferný	k2eAgInSc4d1	dvouciferný
počet	počet	k1gInSc4	počet
titulů	titul	k1gInPc2	titul
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
.	.	kIx.	.
<g/>
Podruhé	podruhé	k6eAd1	podruhé
za	za	k7c7	za
sebou	se	k3xPyFc7	se
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
všech	všecek	k3xTgNnPc2	všecek
pět	pět	k4xCc4	pět
utkání	utkání	k1gNnPc2	utkání
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
Turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
po	po	k7c6	po
semifinálové	semifinálový	k2eAgFnSc6d1	semifinálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Jankovićovou	Jankovićová	k1gFnSc7	Jankovićová
poradila	poradit	k5eAaPmAgFnS	poradit
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
s	s	k7c7	s
čínskou	čínský	k2eAgFnSc7d1	čínská
turnajovou	turnajový	k2eAgFnSc7d1	turnajová
čtyřkou	čtyřka	k1gFnSc7	čtyřka
Li	li	k8xS	li
Na	na	k7c6	na
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
tak	tak	k6eAd1	tak
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
zakončila	zakončit	k5eAaPmAgFnS	zakončit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
grandslam	grandslam	k6eAd1	grandslam
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
sezóny	sezóna	k1gFnSc2	sezóna
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
svou	svůj	k3xOyFgFnSc4	svůj
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
úspěšnou	úspěšný	k2eAgFnSc7d1	úspěšná
obhajobou	obhajoba	k1gFnSc7	obhajoba
Brisbane	Brisban	k1gMnSc5	Brisban
International	International	k1gFnPc6	International
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
dva	dva	k4xCgInPc4	dva
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
si	se	k3xPyFc3	se
počtrnácté	počtrnácta	k1gMnPc1	počtrnácta
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
poradila	poradit	k5eAaPmAgFnS	poradit
se	s	k7c7	s
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
hráčkou	hráčka	k1gFnSc7	hráčka
žebříčku	žebříček	k1gInSc2	žebříček
Marií	Maria	k1gFnSc7	Maria
Šarapovovou	Šarapovová	k1gFnSc7	Šarapovová
a	a	k8xC	a
ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
zdolala	zdolat	k5eAaPmAgFnS	zdolat
světovou	světový	k2eAgFnSc4d1	světová
dvojku	dvojka	k1gFnSc4	dvojka
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc7d1	Azarenková
po	po	k7c6	po
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Australian	Australiana	k1gFnPc2	Australiana
Open	Open	k1gMnSc1	Open
jí	on	k3xPp3gFnSc7	on
stěžovalo	stěžovat	k5eAaImAgNnS	stěžovat
hru	hra	k1gFnSc4	hra
zranění	zranění	k1gNnSc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
se	s	k7c7	s
Srbkou	Srbka	k1gFnSc7	Srbka
Anou	Ano	k2eAgFnSc4d1	Ano
Ivanovićou	Ivanovićá	k1gFnSc4	Ivanovićá
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c4	o
odstoupení	odstoupení	k1gNnSc4	odstoupení
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
před	před	k7c7	před
třetím	třetí	k4xOgNnSc7	třetí
kolem	kolo	k1gNnSc7	kolo
proti	proti	k7c3	proti
Hantuchové	Hantuchový	k2eAgNnSc1d1	Hantuchové
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
zádovým	zádový	k2eAgFnPc3d1	zádová
potížím	potíž	k1gFnPc3	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
poslední	poslední	k2eAgFnSc4d1	poslední
čtyřku	čtyřka	k1gFnSc4	čtyřka
tenistek	tenistka	k1gFnPc2	tenistka
došla	dojít	k5eAaPmAgFnS	dojít
na	na	k7c6	na
Dubai	Dubai	k1gNnSc6	Dubai
Tennis	Tennis	k1gFnSc2	Tennis
Championships	Championshipsa	k1gFnPc2	Championshipsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
Francouzka	Francouzka	k1gFnSc1	Francouzka
Alizé	Alizé	k1gNnSc2	Alizé
Cornetová	Cornetový	k2eAgFnSc1d1	Cornetová
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
srpnového	srpnový	k2eAgInSc2d1	srpnový
Western	Western	kA	Western
&	&	k?	&
Southern	Southern	k1gInSc1	Southern
Open	Open	k1gNnSc1	Open
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
nepočítaje	nepočítaje	k7c4	nepočítaje
grandslam	grandslam	k1gInSc4	grandslam
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
již	již	k6eAd1	již
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
dané	daný	k2eAgFnSc2d1	daná
události	událost	k1gFnSc2	událost
také	také	k9	také
prohrála	prohrát	k5eAaPmAgFnS	prohrát
utkání	utkání	k1gNnSc4	utkání
bez	bez	k7c2	bez
zisku	zisk	k1gInSc2	zisk
jediné	jediný	k2eAgFnSc2d1	jediná
sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Březnový	březnový	k2eAgInSc1d1	březnový
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
Masters	Masters	k1gInSc4	Masters
opět	opět	k6eAd1	opět
bojkotovala	bojkotovat	k5eAaImAgFnS	bojkotovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
daných	daný	k2eAgInPc6d1	daný
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
překonala	překonat	k5eAaPmAgFnS	překonat
šestou	šestý	k4xOgFnSc4	šestý
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
a	a	k8xC	a
dětský	dětský	k2eAgInSc4d1	dětský
idol	idol	k1gInSc4	idol
Moniku	Monika	k1gFnSc4	Monika
Selešovou	Selešová	k1gFnSc7	Selešová
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
178	[number]	k4	178
týdnů	týden	k1gInPc2	týden
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
světové	světový	k2eAgFnSc2d1	světová
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
na	na	k7c4	na
Miami	Miami	k1gNnSc4	Miami
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
semifinálovém	semifinálový	k2eAgNnSc6d1	semifinálové
vyřazení	vyřazení	k1gNnSc6	vyřazení
Šarapovové	Šarapovová	k1gFnSc2	Šarapovová
zdolala	zdolat	k5eAaPmAgFnS	zdolat
i	i	k9	i
Li	li	k9	li
Na	na	k7c4	na
a	a	k8xC	a
ziskem	zisk	k1gInSc7	zisk
rekordního	rekordní	k2eAgInSc2d1	rekordní
sedmého	sedmý	k4xOgInSc2	sedmý
titulu	titul	k1gInSc2	titul
z	z	k7c2	z
floridské	floridský	k2eAgFnSc2d1	floridská
události	událost	k1gFnSc2	událost
tak	tak	k6eAd1	tak
překonala	překonat	k5eAaPmAgFnS	překonat
Agassiho	Agassi	k1gMnSc4	Agassi
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zelené	zelený	k2eAgFnSc6d1	zelená
antuce	antuka	k1gFnSc6	antuka
Family	Famila	k1gFnSc2	Famila
Circle	Circle	k1gFnSc2	Circle
Cupu	cup	k1gInSc2	cup
překvapivě	překvapivě	k6eAd1	překvapivě
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
úvodním	úvodní	k2eAgInSc6d1	úvodní
duelu	duel	k1gInSc6	duel
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
se	s	k7c7	s
Slovenkou	Slovenka	k1gFnSc7	Slovenka
Janou	Jana	k1gFnSc7	Jana
Čepelovou	čepelový	k2eAgFnSc7d1	Čepelová
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
provázelo	provázet	k5eAaImAgNnS	provázet
poranění	poranění	k1gNnSc1	poranění
dolní	dolní	k2eAgFnSc2d1	dolní
končetiny	končetina	k1gFnSc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
být	být	k5eAaImF	být
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
a	a	k8xC	a
na	na	k7c4	na
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
si	se	k3xPyFc3	se
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
od	od	k7c2	od
tenisu	tenis	k1gInSc2	tenis
odpočinout	odpočinout	k5eAaPmF	odpočinout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dvorce	dvorec	k1gInPc4	dvorec
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
květnovým	květnový	k2eAgMnSc7d1	květnový
Madrid	Madrid	k1gInSc1	Madrid
Masters	Masters	k1gInSc4	Masters
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
před	před	k7c7	před
čtvrtfinále	čtvrtfinále	k1gNnSc7	čtvrtfinále
s	s	k7c7	s
Petrou	Petra	k1gFnSc7	Petra
Kvitovou	Kvitová	k1gFnSc4	Kvitová
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
levého	levý	k2eAgNnSc2d1	levé
stehna	stehno	k1gNnSc2	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
šesté	šestý	k4xOgFnSc2	šestý
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
Race	Race	k1gNnSc2	Race
pro	pro	k7c4	pro
turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
se	se	k3xPyFc4	se
posunula	posunout	k5eAaPmAgFnS	posunout
po	po	k7c6	po
třetím	třetí	k4xOgInSc6	třetí
úspěšně	úspěšně	k6eAd1	úspěšně
obhájeném	obhájený	k2eAgInSc6d1	obhájený
titulu	titul	k1gInSc6	titul
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
když	když	k8xS	když
opanovala	opanovat	k5eAaPmAgFnS	opanovat
Rome	Rom	k1gMnSc5	Rom
Masters	Masters	k1gInSc1	Masters
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
semifinálové	semifinálový	k2eAgFnSc6d1	semifinálová
odplatě	odplata	k1gFnSc6	odplata
z	z	k7c2	z
lednového	lednový	k2eAgInSc2d1	lednový
grandslamu	grandslam	k1gInSc2	grandslam
s	s	k7c7	s
Ivanovićovou	Ivanovićová	k1gFnSc7	Ivanovićová
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
i	i	k9	i
finálový	finálový	k2eAgInSc4d1	finálový
duel	duel	k1gInSc4	duel
proti	proti	k7c3	proti
světové	světový	k2eAgFnSc3d1	světová
jedenáctce	jedenáctka	k1gFnSc3	jedenáctka
Saře	Sař	k1gFnSc2	Sař
Erraniové	Erraniový	k2eAgFnSc2d1	Erraniová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
uhrála	uhrát	k5eAaPmAgFnS	uhrát
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
gamy	game	k1gInPc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
navazujícího	navazující	k2eAgInSc2d1	navazující
French	French	k1gMnSc1	French
Open	Openo	k1gNnPc2	Openo
však	však	k9	však
utržila	utržit	k5eAaPmAgFnS	utržit
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
grandslamovou	grandslamový	k2eAgFnSc4d1	grandslamová
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
když	když	k8xS	když
Španělce	Španělka	k1gFnSc3	Španělka
Garbiñ	Garbiñ	k1gFnSc2	Garbiñ
Muguruzaové	Muguruzaová	k1gFnSc2	Muguruzaová
dokázala	dokázat	k5eAaPmAgFnS	dokázat
odebrat	odebrat	k5eAaPmF	odebrat
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
setu	set	k1gInSc6	set
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
si	se	k3xPyFc3	se
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Tatišviliovou	Tatišviliový	k2eAgFnSc7d1	Tatišviliový
a	a	k8xC	a
Chanelle	Chanelle	k1gFnSc1	Chanelle
Scheepersovou	Scheepersová	k1gFnSc7	Scheepersová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
fázi	fáze	k1gFnSc6	fáze
však	však	k9	však
překvapivě	překvapivě	k6eAd1	překvapivě
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
s	s	k7c7	s
Francouzkou	Francouzka	k1gFnSc7	Francouzka
Alizé	Alizé	k1gNnSc1	Alizé
Cornetovou	Cornetový	k2eAgFnSc7d1	Cornetová
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
utržila	utržit	k5eAaPmAgFnS	utržit
druhou	druhý	k4xOgFnSc4	druhý
prohru	prohra	k1gFnSc4	prohra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
wimbledonské	wimbledonský	k2eAgNnSc4d1	wimbledonské
vyřazení	vyřazení	k1gNnSc4	vyřazení
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Venus	Venus	k1gInSc4	Venus
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
sesterský	sesterský	k2eAgInSc4d1	sesterský
pár	pár	k1gInSc4	pár
utkání	utkání	k1gNnSc2	utkání
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
skrečovat	skrečovat	k5eAaPmF	skrečovat
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
na	na	k7c4	na
gamy	game	k1gInPc4	game
<g/>
,	,	kIx,	,
když	když	k8xS	když
mladší	mladý	k2eAgMnSc1d2	mladší
ze	z	k7c2	z
sourozenkyň	sourozenkyně	k1gFnPc2	sourozenkyně
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
hry	hra	k1gFnPc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
prohlášením	prohlášení	k1gNnSc7	prohlášení
bylo	být	k5eAaImAgNnS	být
sděleno	sdělen	k2eAgNnSc1d1	sděleno
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
virové	virový	k2eAgNnSc1d1	virové
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
ze	z	k7c2	z
Swedish	Swedisha	k1gFnPc2	Swedisha
Open	Opena	k1gFnPc2	Opena
<g/>
.	.	kIx.	.
<g/>
Následovala	následovat	k5eAaImAgFnS	následovat
vítězná	vítězný	k2eAgFnSc1d1	vítězná
vlna	vlna	k1gFnSc1	vlna
na	na	k7c6	na
amerických	americký	k2eAgInPc6d1	americký
betonech	beton	k1gInPc6	beton
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
19	[number]	k4	19
z	z	k7c2	z
20	[number]	k4	20
dalších	další	k2eAgInPc2d1	další
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
sezónní	sezónní	k2eAgInSc4d1	sezónní
titul	titul	k1gInSc4	titul
dobyla	dobýt	k5eAaPmAgFnS	dobýt
na	na	k7c6	na
standfordském	standfordský	k2eAgInSc6d1	standfordský
Bank	bank	k1gInSc1	bank
of	of	k?	of
the	the	k?	the
West	West	k1gMnSc1	West
Classic	Classic	k1gMnSc1	Classic
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
pavoukem	pavouk	k1gMnSc7	pavouk
přehrála	přehrát	k5eAaPmAgFnS	přehrát
tři	tři	k4xCgFnPc4	tři
hráčky	hráčka	k1gFnPc4	hráčka
elitní	elitní	k2eAgFnSc2d1	elitní
dvacítky	dvacítka	k1gFnSc2	dvacítka
–	–	k?	–
Ivanovićovou	Ivanovićová	k1gFnSc7	Ivanovićová
<g/>
,	,	kIx,	,
Petkovicovou	Petkovicový	k2eAgFnSc7d1	Petkovicová
a	a	k8xC	a
Kerberovou	Kerberův	k2eAgFnSc7d1	Kerberova
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
žebříčku	žebříček	k1gInSc2	žebříček
z	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pátou	pátý	k4xOgFnSc7	pátý
hráčkou	hráčka	k1gFnSc7	hráčka
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
klasifikace	klasifikace	k1gFnSc2	klasifikace
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
strávila	strávit	k5eAaPmAgFnS	strávit
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
alespoň	alespoň	k9	alespoň
200	[number]	k4	200
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Canada	Canada	k1gFnSc1	Canada
Masters	Masters	k1gInSc1	Masters
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
raketě	raketa	k1gFnSc6	raketa
nejdříve	dříve	k6eAd3	dříve
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
Samantha	Samantha	k1gFnSc1	Samantha
Stosurová	stosurový	k2eAgFnSc1d1	stosurový
<g/>
,	,	kIx,	,
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
a	a	k8xC	a
Caroline	Carolin	k1gInSc5	Carolin
Wozniacká	Wozniacký	k2eAgNnPc4d1	Wozniacký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poté	poté	k6eAd1	poté
navzdory	navzdory	k7c3	navzdory
19	[number]	k4	19
zahraným	zahraný	k2eAgFnPc3d1	zahraná
esům	eso	k1gNnPc3	eso
nestačila	stačit	k5eNaBmAgFnS	stačit
na	na	k7c4	na
sestru	sestra	k1gFnSc4	sestra
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
po	po	k7c4	po
sérii	série	k1gFnSc4	série
pěti	pět	k4xCc2	pět
výher	výhra	k1gFnPc2	výhra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
šestý	šestý	k4xOgInSc4	šestý
pokus	pokus	k1gInSc4	pokus
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
poprvé	poprvé	k6eAd1	poprvé
Cincinnati	Cincinnati	k1gFnSc1	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
Anu	Anu	k1gFnSc4	Anu
Ivanovićovou	Ivanovićový	k2eAgFnSc4d1	Ivanovićový
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
soupeřky	soupeřka	k1gFnPc4	soupeřka
nepřipravily	připravit	k5eNaPmAgInP	připravit
o	o	k7c4	o
žádný	žádný	k3yNgInSc4	žádný
set	set	k1gInSc4	set
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
vítězku	vítězka	k1gFnSc4	vítězka
debla	deblo	k1gNnSc2	deblo
Jekatěrinu	Jekatěrin	k1gInSc2	Jekatěrin
Makarovovou	Makarovový	k2eAgFnSc7d1	Makarovový
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Dánku	Dánka	k1gFnSc4	Dánka
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacký	k2eAgFnSc7d1	Wozniacká
<g/>
.	.	kIx.	.
</s>
<s>
Osmnáctý	osmnáctý	k4xOgInSc4	osmnáctý
kariérní	kariérní	k2eAgInSc4d1	kariérní
grandslam	grandslam	k1gInSc4	grandslam
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
znamenal	znamenat	k5eAaImAgInS	znamenat
posun	posun	k1gInSc1	posun
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
statistik	statistika	k1gFnPc2	statistika
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Evertové	Evertová	k1gFnSc2	Evertová
a	a	k8xC	a
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
<g/>
.	.	kIx.	.
</s>
<s>
Vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
také	také	k9	také
rekord	rekord	k1gInSc1	rekord
šesti	šest	k4xCc2	šest
titulů	titul	k1gInPc2	titul
Evertové	Evertová	k1gFnSc2	Evertová
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výhru	výhra	k1gFnSc4	výhra
inkasovala	inkasovat	k5eAaBmAgFnS	inkasovat
3	[number]	k4	3
milióny	milión	k4xCgInPc4	milión
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
bonus	bonus	k1gInSc1	bonus
1	[number]	k4	1
miliónu	milión	k4xCgInSc2	milión
za	za	k7c4	za
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
US	US	kA	US
Open	Open	k1gMnSc1	Open
Series	Series	k1gMnSc1	Series
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
čtyřmiliónou	čtyřmilióný	k2eAgFnSc4d1	čtyřmilióný
odměnu	odměna	k1gFnSc4	odměna
–	–	k?	–
k	k	k7c3	k
danému	daný	k2eAgNnSc3d1	dané
datu	datum	k1gNnSc3	datum
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
výdělek	výdělek	k1gInSc1	výdělek
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
tenisu	tenis	k1gInSc2	tenis
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
touto	tento	k3xDgFnSc7	tento
částkou	částka	k1gFnSc7	částka
také	také	k6eAd1	také
překročila	překročit	k5eAaPmAgFnS	překročit
hranici	hranice	k1gFnSc4	hranice
60	[number]	k4	60
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
vydělala	vydělat	k5eAaPmAgFnS	vydělat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
singapurském	singapurský	k2eAgInSc6d1	singapurský
Turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
utržila	utržit	k5eAaPmAgFnS	utržit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
červené	červený	k2eAgFnSc2d1	červená
skupiny	skupina	k1gFnSc2	skupina
debakl	debakl	k1gInSc1	debakl
od	od	k7c2	od
Simony	Simona	k1gFnSc2	Simona
Halepové	Halepový	k2eAgFnSc2d1	Halepový
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
znamenal	znamenat	k5eAaImAgInS	znamenat
konec	konec	k1gInSc4	konec
16	[number]	k4	16
<g/>
zápasové	zápasový	k2eAgFnPc4d1	zápasová
neporazitelnosti	neporazitelnost	k1gFnPc4	neporazitelnost
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
Rumunka	Rumunka	k1gFnSc1	Rumunka
ji	on	k3xPp3gFnSc4	on
také	také	k6eAd1	také
přivodila	přivodit	k5eAaBmAgFnS	přivodit
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
porážku	porážka	k1gFnSc4	porážka
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
16	[number]	k4	16
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc4d1	zbylý
dva	dva	k4xCgInPc4	dva
duely	duel	k1gInPc4	duel
nad	nad	k7c7	nad
Bouchardovou	Bouchardův	k2eAgFnSc7d1	Bouchardův
a	a	k8xC	a
Ivanovićovou	Ivanovićová	k1gFnSc7	Ivanovićová
světová	světový	k2eAgFnSc1d1	světová
jednička	jednička	k1gFnSc1	jednička
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Caroline	Carolin	k1gInSc5	Carolin
Wozniackou	Wozniacka	k1gFnSc7	Wozniacka
až	až	k9	až
v	v	k7c6	v
tiebreaku	tiebreak	k1gInSc6	tiebreak
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
sady	sada	k1gFnSc2	sada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
oplatila	oplatit	k5eAaPmAgFnS	oplatit
čerstvou	čerstvý	k2eAgFnSc4d1	čerstvá
prohru	prohra	k1gFnSc4	prohra
Halepové	Halepové	k2eAgInSc7d1	Halepové
výsledkem	výsledek	k1gInSc7	výsledek
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
tak	tak	k9	tak
pátý	pátý	k4xOgInSc4	pátý
titul	titul	k1gInSc4	titul
z	z	k7c2	z
Turnaje	turnaj	k1gInSc2	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
a	a	k8xC	a
počtvrté	počtvrté	k4xO	počtvrté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
zakončila	zakončit	k5eAaPmAgFnS	zakončit
rok	rok	k1gInSc4	rok
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sezóny	sezóna	k1gFnSc2	sezóna
neopustila	opustit	k5eNaPmAgFnS	opustit
jeho	jeho	k3xOp3gNnSc4	jeho
čelo	čelo	k1gNnSc4	čelo
ani	ani	k8xC	ani
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
výkon	výkon	k1gInSc1	výkon
naposledy	naposledy	k6eAd1	naposledy
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Němka	Němka	k1gFnSc1	Němka
Steffi	Steffi	k1gFnSc1	Steffi
Grafová	Grafová	k1gFnSc1	Grafová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
tenistkou	tenistka	k1gFnSc7	tenistka
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Zisk	zisk	k1gInSc1	zisk
druhého	druhý	k4xOgMnSc2	druhý
"	"	kIx"	"
<g/>
Serena	Sereno	k1gNnSc2	Sereno
Slamu	slam	k1gInSc2	slam
<g/>
"	"	kIx"	"
===	===	k?	===
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
potřetí	potřetí	k4xO	potřetí
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
jako	jako	k9	jako
členka	členka	k1gFnSc1	členka
amerického	americký	k2eAgInSc2d1	americký
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
Hopmanově	Hopmanův	k2eAgInSc6d1	Hopmanův
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
skupinu	skupina	k1gFnSc4	skupina
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Isnerem	Isner	k1gMnSc7	Isner
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazila	porazit	k5eAaPmAgFnS	porazit
Pennettaovou	Pennettaová	k1gFnSc4	Pennettaová
<g/>
,	,	kIx,	,
Šafářovou	Šafářová	k1gFnSc4	Šafářová
a	a	k8xC	a
hladce	hladko	k6eAd1	hladko
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Bouchardové	Bouchardový	k2eAgFnPc4d1	Bouchardový
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
proti	proti	k7c3	proti
Polsku	Polsko	k1gNnSc3	Polsko
zajistila	zajistit	k5eAaPmAgFnS	zajistit
jediný	jediný	k2eAgInSc4d1	jediný
bod	bod	k1gInSc4	bod
vítězstvím	vítězství	k1gNnSc7	vítězství
nad	nad	k7c7	nad
Agnieszkou	Agnieszka	k1gFnSc7	Agnieszka
Radwańskou	Radwańský	k2eAgFnSc7d1	Radwańský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
mixu	mix	k1gInSc6	mix
Američané	Američan	k1gMnPc1	Američan
prohráli	prohrát	k5eAaPmAgMnP	prohrát
a	a	k8xC	a
obsadili	obsadit	k5eAaPmAgMnP	obsadit
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
startovala	startovat	k5eAaBmAgFnS	startovat
jako	jako	k9	jako
nejvýše	vysoce	k6eAd3	vysoce
nasazená	nasazený	k2eAgFnSc1d1	nasazená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodních	úvodní	k2eAgNnPc6d1	úvodní
třech	tři	k4xCgNnPc6	tři
kolech	kolo	k1gNnPc6	kolo
uštědřila	uštědřit	k5eAaPmAgFnS	uštědřit
každé	každý	k3xTgFnSc3	každý
soupeřce	soupeřka	k1gFnSc3	soupeřka
po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
"	"	kIx"	"
<g/>
kanáru	kanár	k1gMnSc6	kanár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
otáčela	otáčet	k5eAaImAgFnS	otáčet
duel	duel	k1gInSc4	duel
s	s	k7c7	s
Garbiñ	Garbiñ	k1gFnSc7	Garbiñ
Muguruzaovou	Muguruzaová	k1gFnSc7	Muguruzaová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
finalistku	finalistka	k1gFnSc4	finalistka
předchozího	předchozí	k2eAgInSc2d1	předchozí
ročníku	ročník	k1gInSc2	ročník
Dominiku	Dominik	k1gMnSc3	Dominik
Cibulkovou	Cibulková	k1gFnSc4	Cibulková
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
nezastavila	zastavit	k5eNaPmAgFnS	zastavit
ani	ani	k8xC	ani
překvapivá	překvapivý	k2eAgFnSc1d1	překvapivá
19	[number]	k4	19
<g/>
letá	letý	k2eAgFnSc1d1	letá
semifinalistka	semifinalistka	k1gFnSc1	semifinalistka
Madison	Madisona	k1gFnPc2	Madisona
Keysová	Keysová	k1gFnSc1	Keysová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
přehrála	přehrát	k5eAaPmAgFnS	přehrát
světovou	světový	k2eAgFnSc4d1	světová
dvojku	dvojka	k1gFnSc4	dvojka
Marii	Maria	k1gFnSc4	Maria
Šarapovovou	Šarapovová	k1gFnSc4	Šarapovová
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
a	a	k8xC	a
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
tak	tak	k9	tak
na	na	k7c4	na
šestnácté	šestnáctý	k4xOgNnSc4	šestnáctý
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
devatenáctý	devatenáctý	k4xOgInSc1	devatenáctý
grandslamový	grandslamový	k2eAgInSc1d1	grandslamový
titul	titul	k1gInSc1	titul
ze	z	k7c2	z
singlu	singl	k1gInSc2	singl
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
posun	posun	k1gInSc4	posun
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
za	za	k7c4	za
22	[number]	k4	22
titulů	titul	k1gInPc2	titul
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
33	[number]	k4	33
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
také	také	k9	také
stala	stát	k5eAaPmAgFnS	stát
nejstarší	starý	k2eAgFnSc7d3	nejstarší
vítězkou	vítězka	k1gFnSc7	vítězka
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
open	open	k1gMnSc1	open
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
šestou	šestý	k4xOgFnSc7	šestý
melbourneskou	melbourneský	k2eAgFnSc7d1	melbourneský
trofejí	trofej	k1gFnSc7	trofej
navýšila	navýšit	k5eAaPmAgFnS	navýšit
svůj	svůj	k3xOyFgInSc4	svůj
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Venus	Venus	k1gMnSc1	Venus
Williamsovou	Williamsová	k1gFnSc7	Williamsová
se	se	k3xPyFc4	se
odhlásily	odhlásit	k5eAaPmAgFnP	odhlásit
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
<g/>
Následující	následující	k2eAgInSc4d1	následující
víkend	víkend	k1gInSc4	víkend
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Aires	k1gInSc4	Aires
utkání	utkání	k1gNnSc4	utkání
amerického	americký	k2eAgInSc2d1	americký
týmu	tým	k1gInSc2	tým
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
skupině	skupina	k1gFnSc6	skupina
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
proti	proti	k7c3	proti
Argentině	Argentina	k1gFnSc3	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výhře	výhra	k1gFnSc3	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
přispěla	přispět	k5eAaPmAgFnS	přispět
vítězstvím	vítězství	k1gNnSc7	vítězství
nad	nad	k7c7	nad
Maríou	Maríý	k2eAgFnSc7d1	Maríý
Irigoyenovou	Irigoyenová	k1gFnSc7	Irigoyenová
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Masters	Masters	k1gInSc1	Masters
a	a	k8xC	a
ukončit	ukončit	k5eAaPmF	ukončit
tak	tak	k9	tak
14	[number]	k4	14
<g/>
letý	letý	k2eAgInSc4d1	letý
bojkot	bojkot	k1gInSc4	bojkot
tohoto	tento	k3xDgInSc2	tento
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
kalifornskou	kalifornský	k2eAgFnSc4d1	kalifornská
událost	událost	k1gFnSc4	událost
dostalo	dostat	k5eAaPmAgNnS	dostat
ovací	ovace	k1gFnPc2	ovace
ve	v	k7c6	v
stoje	stoje	k6eAd1	stoje
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
semifinále	semifinále	k1gNnSc7	semifinále
proti	proti	k7c3	proti
světové	světový	k2eAgFnSc3d1	světová
trojce	trojka	k1gFnSc3	trojka
Simoně	Simona	k1gFnSc3	Simona
Halepové	Halepová	k1gFnSc2	Halepová
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
pro	pro	k7c4	pro
poranění	poranění	k1gNnSc4	poranění
pravého	pravý	k2eAgNnSc2d1	pravé
kolena	koleno	k1gNnSc2	koleno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
navazujícím	navazující	k2eAgNnSc6d1	navazující
Miami	Miami	k1gNnSc6	Miami
Open	Opena	k1gFnPc2	Opena
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
čtvrtfinálovou	čtvrtfinálový	k2eAgFnSc7d1	čtvrtfinálová
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c7	nad
Sabine	Sabin	k1gInSc5	Sabin
Lisickou	Lisická	k1gFnSc7	Lisická
jubilejního	jubilejní	k2eAgNnSc2d1	jubilejní
700	[number]	k4	700
<g/>
.	.	kIx.	.
vítězného	vítězný	k2eAgInSc2d1	vítězný
zápasu	zápas	k1gInSc2	zápas
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
osmou	osmý	k4xOgFnSc7	osmý
hráčkou	hráčka	k1gFnSc7	hráčka
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
výkon	výkon	k1gInSc1	výkon
podařil	podařit	k5eAaPmAgInS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtveřicí	čtveřice	k1gFnSc7	čtveřice
tenistek	tenistka	k1gFnPc2	tenistka
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Halepovou	Halepová	k1gFnSc4	Halepová
po	po	k7c6	po
těsném	těsný	k2eAgInSc6d1	těsný
třísetovém	třísetový	k2eAgInSc6d1	třísetový
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
pak	pak	k6eAd1	pak
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
dvanáctou	dvanáctý	k4xOgFnSc4	dvanáctý
nasazenou	nasazený	k2eAgFnSc4d1	nasazená
Carlu	Carla	k1gFnSc4	Carla
Suárezovou	Suárezový	k2eAgFnSc7d1	Suárezový
Navarrovou	Navarrová	k1gFnSc7	Navarrová
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
tak	tak	k9	tak
sérii	série	k1gFnSc4	série
neporazitelnosti	neporazitelnost	k1gFnSc2	neporazitelnost
na	na	k7c4	na
21	[number]	k4	21
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Miami	Miami	k1gNnSc2	Miami
Masters	Mastersa	k1gFnPc2	Mastersa
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
rekordní	rekordní	k2eAgInSc4d1	rekordní
osmý	osmý	k4xOgInSc4	osmý
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
tenistkou	tenistka	k1gFnSc7	tenistka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
jeden	jeden	k4xCgInSc4	jeden
turnaj	turnaj	k1gInSc4	turnaj
alespoň	alespoň	k9	alespoň
osmkrát	osmkrát	k6eAd1	osmkrát
<g/>
.	.	kIx.	.
</s>
<s>
Zařadila	zařadit	k5eAaPmAgFnS	zařadit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
po	po	k7c4	po
bok	bok	k1gInSc4	bok
Chris	Chris	k1gFnSc2	Chris
Evertové	Evertová	k1gFnSc2	Evertová
<g/>
,	,	kIx,	,
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	grafový	k2eAgFnSc2d1	grafová
a	a	k8xC	a
Martiny	Martin	k2eAgFnSc2d1	Martina
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
týdnu	týden	k1gInSc6	týden
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
figurovala	figurovat	k5eAaImAgFnS	figurovat
114	[number]	k4	114
<g/>
.	.	kIx.	.
týden	týden	k1gInSc4	týden
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
posun	posun	k1gInSc4	posun
na	na	k7c4	na
na	na	k7c4	na
třetí	třetí	k4xOgFnSc4	třetí
příčku	příčka	k1gFnSc4	příčka
za	za	k7c4	za
186	[number]	k4	186
týdnů	týden	k1gInPc2	týden
Grafové	Grafová	k1gFnSc2	Grafová
a	a	k8xC	a
156	[number]	k4	156
týdnů	týden	k1gInPc2	týden
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
<g/>
.	.	kIx.	.
</s>
<s>
Premiérovou	premiérový	k2eAgFnSc4d1	premiérová
porážku	porážka	k1gFnSc4	porážka
sezóny	sezóna	k1gFnSc2	sezóna
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
antukového	antukový	k2eAgInSc2d1	antukový
Mutua	Mutu	k1gInSc2	Mutu
Madrid	Madrid	k1gInSc4	Madrid
Open	Openo	k1gNnPc2	Openo
od	od	k7c2	od
světové	světový	k2eAgFnSc2d1	světová
čtyřky	čtyřka	k1gFnSc2	čtyřka
Petry	Petra	k1gFnSc2	Petra
Kvitové	Kvitová	k1gFnSc2	Kvitová
<g/>
.	.	kIx.	.
</s>
<s>
Češka	Češka	k1gFnSc1	Češka
tak	tak	k6eAd1	tak
ukončila	ukončit	k5eAaPmAgFnS	ukončit
její	její	k3xOp3gFnSc1	její
27	[number]	k4	27
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc4d1	jediné
utkání	utkání	k1gNnSc4	utkání
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
římském	římský	k2eAgInSc6d1	římský
Internazionali	Internazionali	k1gMnSc3	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
než	než	k8xS	než
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
pro	pro	k7c4	pro
poranění	poranění	k1gNnSc4	poranění
lokte	loket	k1gInSc2	loket
<g/>
.	.	kIx.	.
<g/>
Postupem	postup	k1gInSc7	postup
do	do	k7c2	do
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
kola	kolo	k1gNnSc2	kolo
French	Frencha	k1gFnPc2	Frencha
Open	Opena	k1gFnPc2	Opena
přes	přes	k7c4	přes
Viktorii	Viktoria	k1gFnSc4	Viktoria
Azarenkovou	Azarenkový	k2eAgFnSc4d1	Azarenková
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
alespoň	alespoň	k9	alespoň
50	[number]	k4	50
zápasů	zápas	k1gInPc2	zápas
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
Grand	grand	k1gMnSc1	grand
Slamů	slam	k1gInPc2	slam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
krajanku	krajanka	k1gFnSc4	krajanka
Sloane	Sloan	k1gMnSc5	Sloan
Stephensovou	Stephensová	k1gFnSc7	Stephensová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	své	k1gNnSc6	své
40	[number]	k4	40
<g/>
.	.	kIx.	.
grandslamovém	grandslamový	k2eAgNnSc6d1	grandslamové
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
si	se	k3xPyFc3	se
hladce	hladko	k6eAd1	hladko
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
Erraniovou	Erraniový	k2eAgFnSc7d1	Erraniová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtyřkou	čtyřka	k1gFnSc7	čtyřka
dokázala	dokázat	k5eAaPmAgFnS	dokázat
otočit	otočit	k5eAaPmF	otočit
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
průběh	průběh	k1gInSc4	průběh
se	s	k7c7	s
Švýcarkou	Švýcarka	k1gFnSc7	Švýcarka
Timeou	Timea	k1gFnSc7	Timea
Bacsinszkou	Bacsinszka	k1gFnSc7	Bacsinszka
<g/>
,	,	kIx,	,
když	když	k8xS	když
ztratila	ztratit	k5eAaPmAgFnS	ztratit
první	první	k4xOgInSc4	první
set	set	k1gInSc4	set
i	i	k8xC	i
podání	podání	k1gNnSc4	podání
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
sadě	sada	k1gFnSc6	sada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
si	se	k3xPyFc3	se
však	však	k9	však
připsala	připsat	k5eAaPmAgFnS	připsat
deset	deset	k4xCc4	deset
gamů	game	k1gInPc2	game
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgInSc1	třetí
titul	titul	k1gInSc1	titul
z	z	k7c2	z
Roland	Rolanda	k1gFnPc2	Rolanda
Garros	Garrosa	k1gFnPc2	Garrosa
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
po	po	k7c6	po
třísetové	třísetový	k2eAgFnSc6d1	třísetová
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Lucií	Lucie	k1gFnSc7	Lucie
Šafářovovu	Šafářovův	k2eAgFnSc4d1	Šafářovův
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jí	jíst	k5eAaImIp3nS	jíst
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
zápasu	zápas	k1gInSc2	zápas
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
cestu	cesta	k1gFnSc4	cesta
za	za	k7c4	za
20	[number]	k4	20
<g/>
.	.	kIx.	.
grandslamem	grandslam	k1gInSc7	grandslam
<g/>
.	.	kIx.	.
</s>
<s>
Trofej	trofej	k1gFnSc1	trofej
znamenala	znamenat	k5eAaImAgFnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
třetí	třetí	k4xOgFnSc7	třetí
tenistkou	tenistka	k1gFnSc7	tenistka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
každou	každý	k3xTgFnSc4	každý
událost	událost	k1gFnSc4	událost
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
nejméně	málo	k6eAd3	málo
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
pouze	pouze	k6eAd1	pouze
Margaret	Margareta	k1gFnPc2	Margareta
Courtové	Courtový	k2eAgFnSc2d1	Courtový
a	a	k8xC	a
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
hráčka	hráčka	k1gFnSc1	hráčka
<g/>
,	,	kIx,	,
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
výkonu	výkon	k1gInSc2	výkon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
ve	v	k7c6	v
třech	tři	k4xCgMnPc6	tři
majorech	major	k1gMnPc6	major
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
i	i	k8xC	i
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
sezóně	sezóna	k1gFnSc6	sezóna
naposledy	naposledy	k6eAd1	naposledy
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
opanovala	opanovat	k5eAaPmAgFnS	opanovat
Jennifer	Jennifer	k1gInSc4	Jennifer
Capriatiová	Capriatiový	k2eAgFnSc1d1	Capriatiová
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
prahu	práh	k1gInSc6	práh
vyřazení	vyřazení	k1gNnSc2	vyřazení
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
proti	proti	k7c3	proti
Heather	Heathra	k1gFnPc2	Heathra
Watsonové	Watsonová	k1gFnPc4	Watsonová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhodující	rozhodující	k2eAgFnSc6d1	rozhodující
sadě	sada	k1gFnSc6	sada
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
dvě	dva	k4xCgNnPc4	dva
prohraná	prohraný	k2eAgNnPc4d1	prohrané
podání	podání	k1gNnPc4	podání
<g/>
.	.	kIx.	.
</s>
<s>
Britka	Britka	k1gFnSc1	Britka
na	na	k7c6	na
servisu	servis	k1gInSc6	servis
však	však	k8xC	však
nevyužila	využít	k5eNaPmAgFnS	využít
gamebol	gamebol	k1gInSc4	gamebol
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
ani	ani	k8xC	ani
vedení	vedení	k1gNnSc1	vedení
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
šla	jít	k5eAaImAgFnS	jít
podávat	podávat	k5eAaImF	podávat
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
a	a	k8xC	a
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
komentovala	komentovat	k5eAaBmAgFnS	komentovat
Američanka	Američanka	k1gFnSc1	Američanka
postup	postup	k1gInSc4	postup
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Budu	být	k5eAaImBp1nS	být
upřímná	upřímný	k2eAgFnSc1d1	upřímná
<g/>
.	.	kIx.	.
</s>
<s>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
povedlo	povést	k5eAaPmAgNnS	povést
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
sama	sám	k3xTgFnSc1	sám
jsem	být	k5eAaImIp1nS	být
v	v	k7c4	v
to	ten	k3xDgNnSc4	ten
rozhodně	rozhodně	k6eAd1	rozhodně
nedoufala	doufat	k5eNaImAgFnS	doufat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
přehrála	přehrát	k5eAaPmAgFnS	přehrát
sestru	sestra	k1gFnSc4	sestra
Venus	Venus	k1gInSc1	Venus
Williamsovou	Williamsová	k1gFnSc7	Williamsová
za	za	k7c2	za
67	[number]	k4	67
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
když	když	k8xS	když
využila	využít	k5eAaPmAgFnS	využít
čtyři	čtyři	k4xCgInPc4	čtyři
z	z	k7c2	z
devíti	devět	k4xCc2	devět
brejkových	brejkův	k2eAgFnPc2d1	brejkův
příležitostí	příležitost	k1gFnPc2	příležitost
a	a	k8xC	a
aktivní	aktivní	k2eAgInSc4d1	aktivní
poměr	poměr	k1gInSc4	poměr
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
duelů	duel	k1gInPc2	duel
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
na	na	k7c4	na
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
zdolala	zdolat	k5eAaPmAgFnS	zdolat
Marii	Maria	k1gFnSc4	Maria
Šarapovovou	Šarapovová	k1gFnSc4	Šarapovová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
nevypracovala	vypracovat	k5eNaPmAgFnS	vypracovat
žádnou	žádný	k3yNgFnSc4	žádný
brejkovou	brejkový	k2eAgFnSc4d1	brejková
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gNnSc4	její
sedmé	sedmý	k4xOgNnSc4	sedmý
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
vítězství	vítězství	k1gNnSc4	vítězství
za	za	k7c7	za
sebou	se	k3xPyFc7	se
a	a	k8xC	a
celkový	celkový	k2eAgInSc4d1	celkový
poměr	poměr	k1gInSc4	poměr
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
na	na	k7c4	na
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Pošesté	pošesté	k4xO	pošesté
zvedla	zvednout	k5eAaPmAgFnS	zvednout
mísu	mísa	k1gFnSc4	mísa
Venus	Venus	k1gMnSc1	Venus
Rosewater	Rosewater	k1gMnSc1	Rosewater
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
po	po	k7c6	po
finálové	finálový	k2eAgFnSc6d1	finálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
21	[number]	k4	21
<g/>
letou	letý	k2eAgFnSc7d1	letá
Španělkou	Španělka	k1gFnSc7	Španělka
Garbiñ	Garbiñ	k1gFnSc7	Garbiñ
Muguruzaovou	Muguruzaová	k1gFnSc7	Muguruzaová
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
po	po	k7c6	po
šesti	šest	k4xCc6	šest
titulech	titul	k1gInPc6	titul
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
grandslamech	grandslam	k1gInPc6	grandslam
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
znamenal	znamenat	k5eAaImAgMnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
jedné	jeden	k4xCgFnSc2	jeden
trofeje	trofej	k1gFnSc2	trofej
na	na	k7c4	na
Grafovou	Grafová	k1gFnSc4	Grafová
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
také	také	k9	také
dobyla	dobýt	k5eAaPmAgFnS	dobýt
nekalendářní	kalendářní	k2eNgInSc4d1	kalendářní
grandslam	grandslam	k1gInSc4	grandslam
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zisk	zisk	k1gInSc1	zisk
všech	všecek	k3xTgMnPc2	všecek
čtyř	čtyři	k4xCgMnPc2	čtyři
majorů	major	k1gMnPc2	major
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
médii	médium	k1gNnPc7	médium
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
podání	podání	k1gNnSc6	podání
označovaný	označovaný	k2eAgMnSc1d1	označovaný
za	za	k7c4	za
Serena	Sereno	k1gNnPc4	Sereno
Slam	sláma	k1gFnPc2	sláma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
33	[number]	k4	33
let	léto	k1gNnPc2	léto
a	a	k8xC	a
289	[number]	k4	289
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejstarší	starý	k2eAgFnSc7d3	nejstarší
šampionkou	šampionka	k1gFnSc7	šampionka
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
open	open	k1gMnSc1	open
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
když	když	k8xS	když
překonala	překonat	k5eAaPmAgFnS	překonat
věkový	věkový	k2eAgInSc4d1	věkový
rekord	rekord	k1gInSc4	rekord
Martiny	Martin	k2eAgFnSc2d1	Martina
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
1990	[number]	k4	1990
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
33	[number]	k4	33
let	léto	k1gNnPc2	léto
a	a	k8xC	a
263	[number]	k4	263
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Toura	k1gFnPc2	Toura
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
68	[number]	k4	68
<g/>
.	.	kIx.	.
trofej	trofej	k1gFnSc1	trofej
a	a	k8xC	a
dotáhla	dotáhnout	k5eAaPmAgFnS	dotáhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
Australanky	Australanka	k1gFnSc2	Australanka
Evonne	Evonn	k1gInSc5	Evonn
Goolagongové	Goolagongový	k2eAgMnPc4d1	Goolagongový
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
WTA	WTA	kA	WTA
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
tenistka	tenistka	k1gFnSc1	tenistka
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
zavedení	zavedení	k1gNnSc2	zavedení
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
posedmé	posedmé	k4xO	posedmé
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
anketu	anketa	k1gFnSc4	anketa
ESPY	ESPY	kA	ESPY
pro	pro	k7c4	pro
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
tenistku	tenistka	k1gFnSc4	tenistka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
utkáním	utkání	k1gNnSc7	utkání
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
na	na	k7c6	na
bastadském	bastadský	k2eAgInSc6d1	bastadský
Swedish	Swedish	k1gInSc1	Swedish
Open	Open	k1gInSc1	Open
s	s	k7c7	s
Klárou	Klára	k1gFnSc7	Klára
Koukalovou	Koukalová	k1gFnSc4	Koukalová
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
pro	pro	k7c4	pro
zranění	zranění	k1gNnSc4	zranění
lokte	loket	k1gInSc2	loket
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
nedoléčené	doléčený	k2eNgNnSc4d1	nedoléčené
zranění	zranění	k1gNnSc4	zranění
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
ze	z	k7c2	z
srpnového	srpnový	k2eAgInSc2d1	srpnový
Bank	bank	k1gInSc1	bank
of	of	k?	of
the	the	k?	the
West	West	k1gMnSc1	West
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
obhajovat	obhajovat	k5eAaImF	obhajovat
trofej	trofej	k1gFnSc1	trofej
Její	její	k3xOp3gFnSc4	její
19	[number]	k4	19
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
ukončila	ukončit	k5eAaPmAgFnS	ukončit
18	[number]	k4	18
<g/>
letá	letý	k2eAgFnSc1d1	letá
Švýcarka	Švýcarka	k1gFnSc1	Švýcarka
a	a	k8xC	a
světová	světový	k2eAgFnSc1d1	světová
dvacítka	dvacítka	k1gFnSc1	dvacítka
Belinda	Belinda	k1gFnSc1	Belinda
Bencicová	Bencicová	k1gFnSc1	Bencicová
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
Canada	Canada	k1gFnSc1	Canada
Masters	Masters	k1gInSc1	Masters
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
a	a	k8xC	a
první	první	k4xOgMnSc1	první
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
od	od	k7c2	od
WTA	WTA	kA	WTA
Finals	Finals	k1gInSc4	Finals
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
týden	týden	k1gInSc4	týden
obhájila	obhájit	k5eAaPmAgFnS	obhájit
trofej	trofej	k1gFnSc1	trofej
na	na	k7c4	na
Cincinnati	Cincinnati	k1gFnSc4	Cincinnati
Masters	Mastersa	k1gFnPc2	Mastersa
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
světovou	světový	k2eAgFnSc7d1	světová
trojkou	trojka	k1gFnSc7	trojka
Simonou	Simona	k1gFnSc7	Simona
Halepovou	Halepová	k1gFnSc7	Halepová
<g/>
.	.	kIx.	.
69	[number]	k4	69
<g/>
.	.	kIx.	.
trofej	trofej	k1gFnSc1	trofej
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
znamenala	znamenat	k5eAaImAgFnS	znamenat
odpoutání	odpoutání	k1gNnSc4	odpoutání
se	se	k3xPyFc4	se
od	od	k7c2	od
Goolagongové	Goolagongový	k2eAgFnSc2d1	Goolagongový
a	a	k8xC	a
setrvání	setrvání	k1gNnSc2	setrvání
na	na	k7c6	na
páté	pátý	k4xOgFnSc6	pátý
pozici	pozice	k1gFnSc6	pozice
open	opena	k1gFnPc2	opena
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc4	Open
Series	Seriesa	k1gFnPc2	Seriesa
skončila	skončit	k5eAaPmAgFnS	skončit
druhá	druhý	k4xOgFnSc1	druhý
o	o	k7c6	o
pět	pět	k4xCc4	pět
bodů	bod	k1gInPc2	bod
za	za	k7c7	za
Karolínou	Karolína	k1gFnSc7	Karolína
Plíškovou	plíškový	k2eAgFnSc7d1	Plíšková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gMnSc1	Open
přijížděla	přijíždět	k5eAaImAgFnS	přijíždět
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
završit	završit	k5eAaPmF	završit
kalendářní	kalendářní	k2eAgMnSc1d1	kalendářní
Grand	grand	k1gMnSc1	grand
Slam	sláma	k1gFnPc2	sláma
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
zařadit	zařadit	k5eAaPmF	zařadit
po	po	k7c4	po
bok	bok	k1gInSc4	bok
tří	tři	k4xCgFnPc2	tři
hráček	hráčka	k1gFnPc2	hráčka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vybojovaly	vybojovat	k5eAaPmAgFnP	vybojovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
pronikla	proniknout	k5eAaPmAgFnS	proniknout
po	po	k7c6	po
třísetové	třísetový	k2eAgFnSc6d1	třísetová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc1	Venus
Williamsovou	Williamsová	k1gFnSc4	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
poslední	poslední	k2eAgFnSc7d1	poslední
čtveřicí	čtveřice	k1gFnSc7	čtveřice
však	však	k8xC	však
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
zápas	zápas	k1gInSc4	zápas
se	s	k7c7	s
čtyřicátou	čtyřicátý	k4xOgFnSc7	čtyřicátý
třetí	třetí	k4xOgFnSc7	třetí
ženou	žena	k1gFnSc7	žena
žebříčku	žebříček	k1gInSc2	žebříček
Robertou	Roberta	k1gFnSc7	Roberta
Vinciovou	Vinciový	k2eAgFnSc7d1	Vinciová
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
úvodní	úvodní	k2eAgInSc4d1	úvodní
set	set	k1gInSc4	set
získala	získat	k5eAaPmAgFnS	získat
poměrem	poměr	k1gInSc7	poměr
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Italská	italský	k2eAgFnSc1d1	italská
soupeřka	soupeřka	k1gFnSc1	soupeřka
však	však	k9	však
dokázala	dokázat	k5eAaPmAgFnS	dokázat
otočit	otočit	k5eAaPmF	otočit
průběh	průběh	k1gInSc4	průběh
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
sety	set	k1gInPc4	set
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
shodně	shodně	k6eAd1	shodně
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgNnSc6d1	rozhodující
dějství	dějství	k1gNnSc6	dějství
doháněla	dohánět	k5eAaImAgFnS	dohánět
ztrátu	ztráta	k1gFnSc4	ztráta
gamů	game	k1gInPc2	game
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
ESPN	ESPN	kA	ESPN
to	ten	k3xDgNnSc4	ten
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
vítězství	vítězství	k1gNnPc2	vítězství
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
periodikum	periodikum	k1gNnSc4	periodikum
The	The	k1gFnSc2	The
Five	Fiv	k1gInSc2	Fiv
Thirty	Thirta	k1gFnSc2	Thirta
Eight	Eighta	k1gFnPc2	Eighta
za	za	k7c4	za
největší	veliký	k2eAgNnSc4d3	veliký
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
moderního	moderní	k2eAgInSc2d1	moderní
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Williamsová	Williamsová	k1gFnSc1	Williamsová
tak	tak	k9	tak
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
prohrála	prohrát	k5eAaPmAgFnS	prohrát
grandslamové	grandslamový	k2eAgNnSc4d1	grandslamové
utkání	utkání	k1gNnSc4	utkání
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
završila	završit	k5eAaPmAgFnS	završit
svou	svůj	k3xOyFgFnSc4	svůj
26	[number]	k4	26
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
33	[number]	k4	33
<g/>
zápasovou	zápasový	k2eAgFnSc4d1	zápasová
počítaje	počítat	k5eAaImSgMnS	počítat
i	i	k9	i
triumf	triumf	k1gInSc4	triumf
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Vinciová	Vinciový	k2eAgFnSc1d1	Vinciová
výhru	výhra	k1gFnSc4	výhra
komentovala	komentovat	k5eAaBmAgFnS	komentovat
jako	jako	k9	jako
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
moment	moment	k1gInSc4	moment
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
čtyřech	čtyři	k4xCgNnPc6	čtyři
vzájemných	vzájemný	k2eAgNnPc6d1	vzájemné
střetnutích	střetnutí	k1gNnPc6	střetnutí
neuhrála	uhrát	k5eNaPmAgFnS	uhrát
proti	proti	k7c3	proti
Američance	Američanka	k1gFnSc3	Američanka
žádný	žádný	k3yNgInSc1	žádný
set	set	k1gInSc1	set
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
si	se	k3xPyFc3	se
i	i	k9	i
přes	přes	k7c4	přes
porážku	porážka	k1gFnSc4	porážka
zabezpečila	zabezpečit	k5eAaPmAgFnS	zabezpečit
popáté	popáté	k4xO	popáté
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
pozici	pozice	k1gFnSc4	pozice
světové	světový	k2eAgFnSc2d1	světová
jedničky	jednička	k1gFnSc2	jednička
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
oznámila	oznámit	k5eAaPmAgFnS	oznámit
ukončení	ukončení	k1gNnSc4	ukončení
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
byl	být	k5eAaImAgInS	být
čas	čas	k1gInSc1	čas
na	na	k7c4	na
vyléčení	vyléčení	k1gNnSc4	vyléčení
různých	různý	k2eAgInPc2d1	různý
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
šrámů	šrám	k1gInPc2	šrám
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hrála	hrát	k5eAaImAgFnS	hrát
jsem	být	k5eAaImIp1nS	být
zraněná	zraněný	k2eAgFnSc1d1	zraněná
většinu	většina	k1gFnSc4	většina
sezony	sezona	k1gFnSc2	sezona
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
loket	loket	k1gInSc1	loket
<g/>
,	,	kIx,	,
koleno	koleno	k1gNnSc1	koleno
nebo	nebo	k8xC	nebo
–	–	k?	–
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jistém	jistý	k2eAgNnSc6d1	jisté
utkání	utkání	k1gNnSc6	utkání
ve	v	k7c4	v
Flushing	Flushing	k1gInSc4	Flushing
Meadows	Meadowsa	k1gFnPc2	Meadowsa
–	–	k?	–
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Jsem	být	k5eAaImIp1nS	být
bojovnice	bojovnice	k1gFnSc1	bojovnice
a	a	k8xC	a
chci	chtít	k5eAaImIp1nS	chtít
hrát	hrát	k5eAaImF	hrát
tak	tak	k6eAd1	tak
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokážu	dokázat	k5eAaPmIp1nS	dokázat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nejdéle	dlouho	k6eAd3	dlouho
to	ten	k3xDgNnSc4	ten
půjde	jít	k5eAaImIp3nS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udělala	udělat	k5eAaPmAgFnS	udělat
jsem	být	k5eAaImIp1nS	být
ho	on	k3xPp3gNnSc4	on
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Williamsová	Williamsová	k1gFnSc1	Williamsová
tak	tak	k6eAd1	tak
neobhajovala	obhajovat	k5eNaImAgFnS	obhajovat
na	na	k7c6	na
Turnaji	turnaj	k1gInSc6	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
titul	titul	k1gInSc4	titul
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
druhou	druhý	k4xOgFnSc7	druhý
nejlépe	dobře	k6eAd3	dobře
placenou	placený	k2eAgFnSc7d1	placená
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
turnajových	turnajový	k2eAgFnPc6d1	turnajová
odměnách	odměna	k1gFnPc6	odměna
vydělala	vydělat	k5eAaPmAgFnS	vydělat
11,6	[number]	k4	11,6
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
jí	on	k3xPp3gFnSc3	on
plynulo	plynout	k5eAaImAgNnS	plynout
z	z	k7c2	z
reklam	reklama	k1gFnPc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prsinci	prsinec	k1gInSc6	prsinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
roku	rok	k1gInSc2	rok
časopisem	časopis	k1gInSc7	časopis
Sports	Sportsa	k1gFnPc2	Sportsa
Illustrated	Illustrated	k1gMnSc1	Illustrated
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Sedmou	sedmý	k4xOgFnSc7	sedmý
wimbledonskou	wimbledonský	k2eAgFnSc7d1	wimbledonská
trofejí	trofej	k1gFnSc7	trofej
vyrovnán	vyrovnat	k5eAaPmNgInS	vyrovnat
rekord	rekord	k1gInSc1	rekord
22	[number]	k4	22
grandslamů	grandslam	k1gInPc2	grandslam
Grafové	Grafová	k1gFnSc2	Grafová
===	===	k?	===
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
otevřela	otevřít	k5eAaPmAgFnS	otevřít
na	na	k7c4	na
Hopman	Hopman	k1gMnSc1	Hopman
Cupu	cup	k1gInSc2	cup
v	v	k7c6	v
páru	pár	k1gInSc6	pár
s	s	k7c7	s
Jackem	Jacek	k1gMnSc7	Jacek
Sockem	Socek	k1gMnSc7	Socek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prvního	první	k4xOgInSc2	první
zápasu	zápas	k1gInSc2	zápas
ji	on	k3xPp3gFnSc4	on
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
nevpustil	vpustit	k5eNaPmAgMnS	vpustit
zánět	zánět	k1gInSc4	zánět
kolena	koleno	k1gNnSc2	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
tak	tak	k6eAd1	tak
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
656	[number]	k4	656
<g/>
.	.	kIx.	.
hráčka	hráčka	k1gFnSc1	hráčka
žebříčku	žebříček	k1gInSc2	žebříček
Victoria	Victorium	k1gNnSc2	Victorium
Duvalová	Duvalová	k1gFnSc1	Duvalová
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
proti	proti	k7c3	proti
Jarmile	Jarmila	k1gFnSc3	Jarmila
Wolfeové	Wolfeová	k1gFnSc2	Wolfeová
z	z	k7c2	z
týmu	tým	k1gInSc2	tým
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Austrálie	Austrálie	k1gFnSc2	Austrálie
skrečovala	skrečovat	k5eAaPmAgFnS	skrečovat
po	po	k7c6	po
prohraném	prohraný	k2eAgInSc6d1	prohraný
prvním	první	k4xOgInSc6	první
setu	set	k1gInSc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Mix	mix	k1gInSc1	mix
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
odehrála	odehrát	k5eAaPmAgFnS	odehrát
Duvalová	Duvalový	k2eAgFnSc1d1	Duvalový
a	a	k8xC	a
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
titulu	titul	k1gInSc2	titul
prošla	projít	k5eAaPmAgFnS	projít
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgInSc6	dvacátý
šestém	šestý	k4xOgInSc6	šestý
finálovém	finálový	k2eAgInSc6d1	finálový
duelu	duel	k1gInSc6	duel
na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
však	však	k9	však
utržila	utržit	k5eAaPmAgFnS	utržit
pátou	pátý	k4xOgFnSc4	pátý
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
její	její	k3xOp3gFnPc4	její
síly	síla	k1gFnPc4	síla
byla	být	k5eAaImAgFnS	být
německá	německý	k2eAgFnSc1d1	německá
debutantka	debutantka	k1gFnSc1	debutantka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
majoru	major	k1gMnSc6	major
Angelique	Angelique	k1gFnSc1	Angelique
Kerberová	Kerberová	k1gFnSc1	Kerberová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
přehrála	přehrát	k5eAaPmAgFnS	přehrát
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
první	první	k4xOgFnSc4	první
prohru	prohra	k1gFnSc4	prohra
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
finále	finále	k1gNnPc2	finále
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Opena	k1gFnPc2	Opena
i	i	k8xC	i
první	první	k4xOgFnSc4	první
třísetovou	třísetový	k2eAgFnSc4d1	třísetová
grandslamovou	grandslamový	k2eAgFnSc4d1	grandslamová
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
když	když	k8xS	když
všech	všecek	k3xTgFnPc2	všecek
osm	osm	k4xCc1	osm
předchozích	předchozí	k2eAgFnPc2d1	předchozí
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
až	až	k9	až
na	na	k7c6	na
březnovém	březnový	k2eAgNnSc6d1	březnové
BNP	BNP	kA	BNP
Paribas	Paribas	k1gMnSc1	Paribas
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
kalifornský	kalifornský	k2eAgInSc1d1	kalifornský
turnaj	turnaj	k1gInSc1	turnaj
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
bojkotovala	bojkotovat	k5eAaImAgFnS	bojkotovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
Simonu	Simona	k1gFnSc4	Simona
Halepovou	Halepový	k2eAgFnSc4d1	Halepový
a	a	k8xC	a
poté	poté	k6eAd1	poté
Agnieszku	Agnieszka	k1gFnSc4	Agnieszka
Radwańskou	Radwańský	k2eAgFnSc4d1	Radwańský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
však	však	k9	však
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
třinácté	třináctý	k4xOgFnSc6	třináctý
nasazené	nasazený	k2eAgFnSc6d1	nasazená
Viktorii	Viktoria	k1gFnSc6	Viktoria
Azarenkové	Azarenkový	k2eAgFnSc6d1	Azarenková
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
předtím	předtím	k6eAd1	předtím
pětkrát	pětkrát	k6eAd1	pětkrát
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
porazila	porazit	k5eAaPmAgFnS	porazit
bez	bez	k7c2	bez
ztráty	ztráta	k1gFnSc2	ztráta
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
tak	tak	k8xC	tak
prohrála	prohrát	k5eAaPmAgFnS	prohrát
dvě	dva	k4xCgFnPc4	dva
finále	finále	k1gNnSc6	finále
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
obhájkyně	obhájkyně	k1gFnSc1	obhájkyně
trofeje	trofej	k1gFnSc2	trofej
přijela	přijet	k5eAaPmAgFnS	přijet
na	na	k7c6	na
Miami	Miami	k1gNnSc6	Miami
Open	Opena	k1gFnPc2	Opena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
Světlany	Světlana	k1gFnSc2	Světlana
Kuzněcovové	Kuzněcovová	k1gFnSc2	Kuzněcovová
<g/>
.	.	kIx.	.
</s>
<s>
Znamenalo	znamenat	k5eAaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gFnSc4	její
první	první	k4xOgFnSc4	první
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
a	a	k8xC	a
konec	konec	k1gInSc1	konec
tamější	tamější	k2eAgInSc1d1	tamější
20	[number]	k4	20
<g/>
zápasové	zápasový	k2eAgFnSc2d1	zápasová
neporazitelnosti	neporazitelnost	k1gFnSc2	neporazitelnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tak	tak	k6eAd1	tak
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
na	na	k7c6	na
miamské	miamský	k2eAgFnSc6d1	Miamská
události	událost	k1gFnSc6	událost
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jubilejní	jubilejní	k2eAgInSc1d1	jubilejní
70	[number]	k4	70
<g/>
.	.	kIx.	.
singlový	singlový	k2eAgInSc4d1	singlový
titul	titul	k1gInSc4	titul
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
na	na	k7c4	na
Internazionali	Internazionali	k1gFnSc4	Internazionali
BNL	BNL	kA	BNL
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italia	Italius	k1gMnSc2	Italius
<g/>
,	,	kIx,	,
hraném	hraný	k2eAgInSc6d1	hraný
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
oplatila	oplatit	k5eAaPmAgFnS	oplatit
porážku	porážka	k1gFnSc4	porážka
Kuzněcovové	Kuzněcovová	k1gFnSc2	Kuzněcovová
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ruska	Ruska	k1gFnSc1	Ruska
získala	získat	k5eAaPmAgFnS	získat
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
gamy	game	k1gInPc4	game
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
semifinálové	semifinálový	k2eAgFnSc6d1	semifinálová
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Irinou-Camelií	Irinou-Camelie	k1gFnSc7	Irinou-Camelie
Beguovou	Beguový	k2eAgFnSc7d1	Beguový
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
i	i	k9	i
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
duel	duel	k1gInSc4	duel
proti	proti	k7c3	proti
světové	světový	k2eAgFnSc3d1	světová
čtyřiadvacítce	čtyřiadvacítka	k1gFnSc3	čtyřiadvacítka
Madison	Madison	k1gInSc4	Madison
Keysové	Keysová	k1gFnSc2	Keysová
a	a	k8xC	a
z	z	k7c2	z
Rome	Rom	k1gMnSc5	Rom
Masters	Masters	k1gInSc4	Masters
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
odvezla	odvézt	k5eAaPmAgFnS	odvézt
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trofej	trofej	k1gInSc4	trofej
čekala	čekat	k5eAaImAgFnS	čekat
265	[number]	k4	265
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgMnSc7	první
ryze	ryze	k6eAd1	ryze
americkým	americký	k2eAgNnSc7d1	americké
finále	finále	k1gNnSc7	finále
na	na	k7c6	na
antuce	antuka	k1gFnSc6	antuka
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gMnSc1	Tour
po	po	k7c6	po
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
naposledy	naposledy	k6eAd1	naposledy
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
antukového	antukový	k2eAgInSc2d1	antukový
turnaje	turnaj	k1gInSc2	turnaj
utkaly	utkat	k5eAaPmAgInP	utkat
Serena	Seren	k2eAgFnSc1d1	Serena
a	a	k8xC	a
Venus	Venus	k1gInSc1	Venus
Williamsovy	Williamsův	k2eAgFnSc2d1	Williamsova
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gNnSc4	Open
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
French	French	k1gInSc4	French
Open	Open	k1gInSc4	Open
ztratila	ztratit	k5eAaPmAgFnS	ztratit
jediný	jediný	k2eAgInSc4d1	jediný
set	set	k1gInSc4	set
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
proti	proti	k7c3	proti
kazašské	kazašský	k2eAgFnSc6d1	kazašská
šedesáté	šedesátý	k4xOgFnSc6	šedesátý
hráčce	hráčka	k1gFnSc6	hráčka
žebříčku	žebříček	k1gInSc2	žebříček
Julii	Julie	k1gFnSc4	Julie
Putincevové	Putincevové	k2eAgInSc1d1	Putincevové
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otočení	otočení	k1gNnSc6	otočení
</s>
<s>
průběhu	průběh	k1gInSc6	průběh
a	a	k8xC	a
zisku	zisk	k1gInSc6	zisk
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
dvou	dva	k4xCgFnPc2	dva
sad	sada	k1gFnPc2	sada
si	se	k3xPyFc3	se
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
poradila	poradit	k5eAaPmAgFnS	poradit
s	s	k7c7	s
překvapením	překvapení	k1gNnSc7	překvapení
turnaje	turnaj	k1gInSc2	turnaj
Kiki	Kik	k1gFnSc2	Kik
Bertensovou	Bertensová	k1gFnSc4	Bertensová
z	z	k7c2	z
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
repríze	repríza	k1gFnSc6	repríza
wimbledonského	wimbledonský	k2eAgNnSc2d1	wimbledonské
finále	finále	k1gNnSc2	finále
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
oplatila	oplatit	k5eAaPmAgFnS	oplatit
jí	on	k3xPp3gFnSc3	on
platila	platit	k5eAaImAgFnS	platit
porážku	porážka	k1gFnSc4	porážka
22	[number]	k4	22
<g/>
letá	letý	k2eAgFnSc1d1	letá
světová	světový	k2eAgFnSc1d1	světová
čtyřka	čtyřka	k1gFnSc1	čtyřka
Garbiñ	Garbiñ	k1gFnSc1	Garbiñ
Muguruzaová	Muguruzaová	k1gFnSc1	Muguruzaová
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
na	na	k7c6	na
druhém	druhý	k4xOgMnSc6	druhý
majoru	major	k1gMnSc6	major
sezóny	sezóna	k1gFnSc2	sezóna
tak	tak	k6eAd1	tak
odešla	odejít	k5eAaPmAgFnS	odejít
poražena	porazit	k5eAaPmNgFnS	porazit
z	z	k7c2	z
přímého	přímý	k2eAgInSc2d1	přímý
boje	boj	k1gInSc2	boj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
prohrála	prohrát	k5eAaPmAgFnS	prohrát
dvě	dva	k4xCgNnPc4	dva
grandslamová	grandslamový	k2eAgNnPc4d1	grandslamové
finále	finále	k1gNnPc4	finále
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zápasů	zápas	k1gInPc2	zápas
Španělka	Španělka	k1gFnSc1	Španělka
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c4	na
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
když	když	k8xS	když
všech	všecek	k3xTgNnPc2	všecek
pět	pět	k4xCc1	pět
utkání	utkání	k1gNnPc2	utkání
dvojice	dvojice	k1gFnSc2	dvojice
odehrála	odehrát	k5eAaPmAgFnS	odehrát
na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
ztratila	ztratit	k5eAaPmAgFnS	ztratit
jediný	jediný	k2eAgInSc4d1	jediný
set	set	k1gInSc4	set
<g/>
,	,	kIx,	,
když	když	k8xS	když
otočila	otočit	k5eAaPmAgFnS	otočit
duel	duel	k1gInSc4	duel
druhého	druhý	k4xOgMnSc2	druhý
kola	kolo	k1gNnPc4	kolo
proti	proti	k7c3	proti
krajance	krajanka	k1gFnSc3	krajanka
Christině	Christina	k1gFnSc3	Christina
McHaleové	McHaleová	k1gFnSc3	McHaleová
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
uštědřila	uštědřit	k5eAaPmAgFnS	uštědřit
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
"	"	kIx"	"
<g/>
kanáru	kanár	k1gMnSc6	kanár
<g/>
"	"	kIx"	"
Němce	Němec	k1gMnPc4	Němec
Annice	Annice	k1gFnSc1	Annice
Beckové	Becková	k1gFnSc2	Becková
a	a	k8xC	a
třinácté	třináctý	k4xOgFnSc3	třináctý
nasazené	nasazený	k2eAgFnSc3d1	nasazená
Světlaně	Světlana	k1gFnSc3	Světlana
Kuzněcovové	Kuzněcovová	k1gFnSc2	Kuzněcovová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvládnutém	zvládnutý	k2eAgNnSc6d1	zvládnuté
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
turnajovou	turnajový	k2eAgFnSc7d1	turnajová
jednadvacítkou	jednadvacítka	k1gFnSc7	jednadvacítka
Pavljučenkovovou	Pavljučenkovový	k2eAgFnSc7d1	Pavljučenkovový
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
uhrála	uhrát	k5eAaPmAgFnS	uhrát
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
gamy	game	k1gInPc4	game
třetí	třetí	k4xOgFnSc1	třetí
ruská	ruský	k2eAgFnSc1d1	ruská
soupeřka	soupeřka	k1gFnSc1	soupeřka
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
Jelena	Jelen	k1gMnSc4	Jelen
Vesninová	Vesninový	k2eAgFnSc1d1	Vesninová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finála	k1gFnSc6	finála
oplatila	oplatit	k5eAaPmAgFnS	oplatit
lednovou	lednový	k2eAgFnSc4d1	lednová
porážku	porážka	k1gFnSc4	porážka
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
nasazené	nasazený	k2eAgFnSc2d1	nasazená
Angelique	Angeliqu	k1gFnSc2	Angeliqu
Kerberové	Kerber	k1gMnPc1	Kerber
ve	v	k7c6	v
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
zápasu	zápas	k1gInSc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
pokus	pokus	k1gInSc4	pokus
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
dvacátý	dvacátý	k4xOgMnSc1	dvacátý
druhý	druhý	k4xOgInSc4	druhý
grandslamový	grandslamový	k2eAgInSc4d1	grandslamový
titul	titul	k1gInSc4	titul
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgMnSc3	jenž
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
rekord	rekord	k1gInSc4	rekord
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
trofejí	trofej	k1gFnPc2	trofej
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Němku	Němka	k1gFnSc4	Němka
a	a	k8xC	a
Dorotheu	Dorothe	k2eAgFnSc4d1	Dorothea
Lambertovou	Lambertový	k2eAgFnSc4d1	Lambertová
Chambersovou	Chambersová	k1gFnSc4	Chambersová
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
i	i	k9	i
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
wimbledonských	wimbledonský	k2eAgInPc2d1	wimbledonský
vavřínů	vavřín	k1gInPc2	vavřín
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
posedmé	posedmé	k4xO	posedmé
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
také	také	k9	také
rekordní	rekordní	k2eAgNnSc4d1	rekordní
rozpětí	rozpětí	k1gNnSc4	rozpětí
mezi	mezi	k7c7	mezi
premiérovou	premiérový	k2eAgFnSc7d1	premiérová
a	a	k8xC	a
poslední	poslední	k2eAgFnSc7d1	poslední
trofejí	trofej	k1gFnSc7	trofej
z	z	k7c2	z
majoru	major	k1gMnSc3	major
na	na	k7c4	na
16	[number]	k4	16
let	léto	k1gNnPc2	léto
a	a	k8xC	a
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Margaret	Margareta	k1gFnPc2	Margareta
Courtové	Court	k1gMnPc1	Court
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
držela	držet	k5eAaImAgFnS	držet
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
v	v	k7c6	v
grandslamových	grandslamový	k2eAgInPc6d1	grandslamový
finále	finále	k1gNnSc2	finále
open	open	k1gNnSc1	open
éry	éra	k1gFnSc2	éra
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
34	[number]	k4	34
let	léto	k1gNnPc2	léto
a	a	k8xC	a
288	[number]	k4	288
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejstarší	starý	k2eAgFnSc7d3	nejstarší
vítězkou	vítězka	k1gFnSc7	vítězka
majoru	major	k1gMnSc3	major
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
éře	éra	k1gFnSc6	éra
<g/>
.	.	kIx.	.
</s>
<s>
Zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
tak	tak	k9	tak
scénář	scénář	k1gInSc1	scénář
Martiny	Martina	k1gFnSc2	Martina
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
porážkách	porážka	k1gFnPc6	porážka
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
grandslamu	grandslam	k1gInSc2	grandslam
během	během	k7c2	během
jediné	jediný	k2eAgFnSc2d1	jediná
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
si	se	k3xPyFc3	se
odvezla	odvézt	k5eAaPmAgFnS	odvézt
double	double	k2eAgNnSc4d1	double
<g/>
,	,	kIx,	,
když	když	k8xS	když
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
letech	let	k1gInPc6	let
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
s	s	k7c7	s
Venus	Venus	k1gInSc1	Venus
Williamsovou	Williamsův	k2eAgFnSc7d1	Williamsova
šestou	šestý	k4xOgFnSc4	šestý
wimbledonskou	wimbledonský	k2eAgFnSc4d1	wimbledonská
trofej	trofej	k1gFnSc4	trofej
a	a	k8xC	a
čtrnáctou	čtrnáctý	k4xOgFnSc4	čtrnáctý
grandslamovou	grandslamový	k2eAgFnSc4d1	grandslamová
z	z	k7c2	z
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
finálové	finálový	k2eAgInPc4d1	finálový
zápasy	zápas	k1gInPc4	zápas
od	od	k7c2	od
debutového	debutový	k2eAgInSc2d1	debutový
vavřínu	vavřín	k1gInSc2	vavřín
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gInSc4	Open
1999	[number]	k4	1999
přitom	přitom	k6eAd1	přitom
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
statistikách	statistika	k1gFnPc6	statistika
jim	on	k3xPp3gMnPc3	on
patřilo	patřit	k5eAaImAgNnS	patřit
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
dvojicí	dvojice	k1gFnSc7	dvojice
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
a	a	k8xC	a
Pam	Pam	k1gFnSc1	Pam
Shriverová	Shriverová	k1gFnSc1	Shriverová
s	s	k7c7	s
dvaceti	dvacet	k4xCc7	dvacet
deblovými	deblový	k2eAgNnPc7d1	deblové
vítězstvími	vítězství	k1gNnPc7	vítězství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Rogers	Rogersa	k1gFnPc2	Rogersa
Cupu	cup	k1gInSc2	cup
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
zdůvodněním	zdůvodnění	k1gNnSc7	zdůvodnění
zánětu	zánět	k1gInSc2	zánět
ramene	rameno	k1gNnSc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
zlaté	zlatý	k2eAgFnPc1d1	zlatá
obhajovala	obhajovat	k5eAaImAgFnS	obhajovat
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
singlové	singlový	k2eAgFnSc6d1	singlová
soutěži	soutěž	k1gFnSc6	soutěž
však	však	k9	však
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
kole	kolo	k1gNnSc6	kolo
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Ukrajince	Ukrajinka	k1gFnSc3	Ukrajinka
Elině	Elina	k1gFnSc3	Elina
Svitolinové	Svitolinový	k2eAgFnPc1d1	Svitolinový
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Venus	Venus	k1gInSc1	Venus
Williamsovou	Williamsová	k1gFnSc7	Williamsová
pak	pak	k6eAd1	pak
prohrály	prohrát	k5eAaPmAgInP	prohrát
již	již	k6eAd1	již
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
,	,	kIx,	,
když	když	k8xS	když
nestačily	stačit	k5eNaBmAgFnP	stačit
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
pár	pár	k1gInSc4	pár
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
a	a	k8xC	a
Barbora	Barbora	k1gFnSc1	Barbora
Strýcová	Strýcová	k1gFnSc1	Strýcová
<g/>
.	.	kIx.	.
</s>
<s>
Přišly	přijít	k5eAaPmAgFnP	přijít
tak	tak	k6eAd1	tak
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
neporazitelnost	neporazitelnost	k1gFnSc4	neporazitelnost
v	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
pod	pod	k7c7	pod
pěti	pět	k4xCc7	pět
olympijskými	olympijský	k2eAgInPc7d1	olympijský
kruhy	kruh	k1gInPc7	kruh
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
čítala	čítat	k5eAaImAgFnS	čítat
šňůru	šňůra	k1gFnSc4	šňůra
patnácti	patnáct	k4xCc2	patnáct
výher	výhra	k1gFnPc2	výhra
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
obdržení	obdržení	k1gNnSc4	obdržení
divoké	divoký	k2eAgFnSc2d1	divoká
karty	karta	k1gFnSc2	karta
na	na	k7c6	na
Western	Western	kA	Western
&	&	k?	&
Southern	Southern	k1gMnSc1	Southern
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
turnaje	turnaj	k1gInSc2	turnaj
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
pro	pro	k7c4	pro
pokračující	pokračující	k2eAgFnPc4d1	pokračující
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
ramenem	rameno	k1gNnSc7	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
US	US	kA	US
Open	Open	k1gInSc1	Open
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
semifinálový	semifinálový	k2eAgInSc4d1	semifinálový
výsledek	výsledek	k1gInSc4	výsledek
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fázi	fáze	k1gFnSc6	fáze
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
desátá	desátý	k4xOgFnSc1	desátý
nasazená	nasazený	k2eAgFnSc1d1	nasazená
Karolína	Karolína	k1gFnSc1	Karolína
Plíšková	plíškový	k2eAgFnSc1d1	Plíšková
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
výhrou	výhra	k1gFnSc7	výhra
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
kole	kolo	k1gNnSc6	kolo
nad	nad	k7c7	nad
Švédkou	Švédka	k1gFnSc7	Švédka
Johannu	Johanen	k2eAgFnSc4d1	Johanna
Larssonovou	Larssonová	k1gFnSc4	Larssonová
stala	stát	k5eAaPmAgFnS	stát
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
ženou	žena	k1gFnSc7	žena
ve	v	k7c6	v
statistice	statistika	k1gFnSc6	statistika
vítězných	vítězný	k2eAgInPc2d1	vítězný
zápasů	zápas	k1gInPc2	zápas
na	na	k7c6	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
307	[number]	k4	307
<g/>
.	.	kIx.	.
vítězstvím	vítězství	k1gNnSc7	vítězství
pokořila	pokořit	k5eAaPmAgFnS	pokořit
rekord	rekord	k1gInSc4	rekord
Navrátilové	Navrátilová	k1gFnSc2	Navrátilová
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
pozápasová	pozápasový	k2eAgFnSc1d1	pozápasová
bilance	bilance	k1gFnSc1	bilance
činila	činit	k5eAaImAgFnS	činit
307	[number]	k4	307
<g/>
–	–	k?	–
<g/>
42	[number]	k4	42
<g/>
.	.	kIx.	.
</s>
<s>
Navazující	navazující	k2eAgInSc1d1	navazující
postup	postup	k1gInSc1	postup
do	do	k7c2	do
čtvrtfinále	čtvrtfinále	k1gNnSc2	čtvrtfinále
přes	přes	k7c4	přes
Kazašku	Kazaška	k1gFnSc4	Kazaška
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
Švedovovou	Švedovová	k1gFnSc4	Švedovová
znamenal	znamenat	k5eAaImAgMnS	znamenat
308	[number]	k4	308
<g/>
.	.	kIx.	.
vyhraný	vyhraný	k2eAgInSc1d1	vyhraný
zápas	zápas	k1gInSc1	zápas
a	a	k8xC	a
posun	posun	k1gInSc1	posun
na	na	k7c6	na
první	první	k4xOgFnSc6	první
místo	místo	k7c2	místo
historických	historický	k2eAgFnPc2d1	historická
statistik	statistika	k1gFnPc2	statistika
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
odpoutala	odpoutat	k5eAaPmAgFnS	odpoutat
od	od	k7c2	od
307	[number]	k4	307
výher	výhra	k1gFnPc2	výhra
Rogera	Rogera	k1gFnSc1	Rogera
Federera	Federera	k1gFnSc1	Federera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
vyrovnala	vyrovnat	k5eAaPmAgFnS	vyrovnat
rekord	rekord	k1gInSc4	rekord
Steffi	Steffi	k1gFnSc2	Steffi
Grafové	Grafová	k1gFnSc2	Grafová
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
186	[number]	k4	186
týdnů	týden	k1gInPc2	týden
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
klasifikace	klasifikace	k1gFnSc2	klasifikace
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
šampionka	šampionka	k1gFnSc1	šampionka
newyorského	newyorský	k2eAgNnSc2d1	newyorské
majoru	major	k1gMnSc6	major
Angelique	Angelique	k1gFnSc1	Angelique
Kerberová	Kerberová	k1gFnSc1	Kerberová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Rekord	rekord	k1gInSc1	rekord
open	open	k1gNnSc1	open
éry	éra	k1gFnSc2	éra
23	[number]	k4	23
<g/>
.	.	kIx.	.
grandslamem	grandslam	k1gInSc7	grandslam
a	a	k8xC	a
těhotenství	těhotenství	k1gNnSc1	těhotenství
===	===	k?	===
</s>
</p>
<p>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
rozehrála	rozehrát	k5eAaPmAgFnS	rozehrát
na	na	k7c6	na
aucklandském	aucklandský	k2eAgInSc6d1	aucklandský
ASB	ASB	kA	ASB
Classic	Classice	k1gFnPc2	Classice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
výhře	výhra	k1gFnSc6	výhra
nad	nad	k7c7	nad
Francouzkou	Francouzka	k1gFnSc7	Francouzka
Pauline	Paulin	k1gInSc5	Paulin
Parmentierovou	Parmentierová	k1gFnSc7	Parmentierová
dohrála	dohrát	k5eAaPmAgFnS	dohrát
na	na	k7c6	na
raketě	raketa	k1gFnSc6	raketa
krajanky	krajanka	k1gFnSc2	krajanka
Madison	Madison	k1gInSc4	Madison
Brengleové	Brengleová	k1gFnSc2	Brengleová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
zdolala	zdolat	k5eAaPmAgFnS	zdolat
cestou	cesta	k1gFnSc7	cesta
pavoukem	pavouk	k1gMnSc7	pavouk
bývalé	bývalý	k2eAgFnSc2d1	bývalá
členky	členka	k1gFnSc2	členka
elitní	elitní	k2eAgFnSc2d1	elitní
světové	světový	k2eAgFnPc4d1	světová
desítky	desítka	k1gFnPc4	desítka
Belindu	Belinda	k1gFnSc4	Belinda
Bencicovou	Bencicová	k1gFnSc4	Bencicová
<g/>
,	,	kIx,	,
Lucii	Lucie	k1gFnSc4	Lucie
Šafářovou	Šafářová	k1gFnSc4	Šafářová
i	i	k8xC	i
Johannu	Johanen	k2eAgFnSc4d1	Johanna
Kontaovou	Kontaová	k1gFnSc4	Kontaová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
jí	on	k3xPp3gFnSc7	on
odebrala	odebrat	k5eAaPmAgFnS	odebrat
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
gamy	game	k1gInPc4	game
Chorvatka	Chorvatka	k1gFnSc1	Chorvatka
Mirjana	Mirjana	k1gFnSc1	Mirjana
Lučićová	Lučićová	k1gFnSc1	Lučićová
Baroniová	Baroniový	k2eAgFnSc1d1	Baroniový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devátém	devátý	k4xOgNnSc6	devátý
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
finále	finále	k1gNnSc6	finále
grandslamu	grandslam	k1gInSc2	grandslam
se	se	k3xPyFc4	se
utkala	utkat	k5eAaPmAgFnS	utkat
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gInSc4	Venus
Williamsovou	Williamsová	k1gFnSc7	Williamsová
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
open	open	k1gNnSc1	open
éře	éra	k1gFnSc6	éra
se	se	k3xPyFc4	se
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
zápase	zápas	k1gInSc6	zápas
utkali	utkat	k5eAaPmAgMnP	utkat
dva	dva	k4xCgMnPc1	dva
tenisté	tenista	k1gMnPc1	tenista
starší	starý	k2eAgMnPc1d2	starší
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
1.21	[number]	k4	1.21
hodiny	hodina	k1gFnSc2	hodina
ji	on	k3xPp3gFnSc4	on
přehrála	přehrát	k5eAaPmAgFnS	přehrát
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
a	a	k8xC	a
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
rekordní	rekordní	k2eAgFnSc4d1	rekordní
dvacátou	dvacátý	k4xOgFnSc4	dvacátý
třetí	třetí	k4xOgFnSc4	třetí
grandslamovou	grandslamový	k2eAgFnSc4d1	grandslamová
trofej	trofej	k1gFnSc4	trofej
z	z	k7c2	z
dvouhry	dvouhra	k1gFnSc2	dvouhra
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
sedmou	sedmý	k4xOgFnSc7	sedmý
z	z	k7c2	z
Melbourne	Melbourne	k1gNnSc2	Melbourne
Parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
tenista	tenista	k1gMnSc1	tenista
historie	historie	k1gFnSc2	historie
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
na	na	k7c6	na
dvou	dva	k4xCgMnPc6	dva
majorech	major	k1gMnPc6	major
sedmkrát	sedmkrát	k6eAd1	sedmkrát
<g/>
.	.	kIx.	.
</s>
<s>
Bodový	bodový	k2eAgInSc1d1	bodový
zisk	zisk	k1gInSc1	zisk
ji	on	k3xPp3gFnSc4	on
následně	následně	k6eAd1	následně
posedmé	posedmé	k4xO	posedmé
posunul	posunout	k5eAaPmAgMnS	posunout
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
nejstarší	starý	k2eAgFnSc4d3	nejstarší
nastupující	nastupující	k2eAgFnSc4d1	nastupující
světovou	světový	k2eAgFnSc4d1	světová
jedničku	jednička	k1gFnSc4	jednička
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
březnových	březnový	k2eAgNnPc2d1	březnové
BNP	BNP	kA	BNP
Paribas	Paribas	k1gMnSc1	Paribas
Open	Open	k1gMnSc1	Open
a	a	k8xC	a
Masters	Masters	k1gInSc1	Masters
Open	Opena	k1gFnPc2	Opena
se	se	k3xPyFc4	se
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
pro	pro	k7c4	pro
levostranné	levostranný	k2eAgNnSc4d1	levostranné
kolenní	kolenní	k2eAgNnSc4d1	kolenní
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
prvního	první	k4xOgMnSc2	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	s	k7c7	s
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
klasifikace	klasifikace	k1gFnSc2	klasifikace
vrátila	vrátit	k5eAaPmAgFnS	vrátit
Kerberová	Kerberová	k1gFnSc1	Kerberová
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
těhotenství	těhotenství	k1gNnSc1	těhotenství
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
prodloužení	prodloužení	k1gNnSc4	prodloužení
herního	herní	k2eAgInSc2d1	herní
výpadku	výpadek	k1gInSc2	výpadek
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
na	na	k7c4	na
dvorce	dvorec	k1gInPc4	dvorec
naplánovala	naplánovat	k5eAaBmAgFnS	naplánovat
na	na	k7c4	na
sezónu	sezóna	k1gFnSc4	sezóna
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Lednový	lednový	k2eAgInSc1d1	lednový
grandslam	grandslam	k1gInSc1	grandslam
tak	tak	k9	tak
zřejmě	zřejmě	k6eAd1	zřejmě
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
v	v	k7c6	v
osmém	osmý	k4xOgInSc6	osmý
týdnu	týden	k1gInSc6	týden
gravidity	gravidita	k1gFnSc2	gravidita
<g/>
.	.	kIx.	.
<g/>
Návrat	návrat	k1gInSc1	návrat
na	na	k7c4	na
profesionální	profesionální	k2eAgInSc4d1	profesionální
okruh	okruh	k1gInSc4	okruh
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
v	v	k7c6	v
únorovém	únorový	k2eAgNnSc6d1	únorové
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
2018	[number]	k4	2018
proti	proti	k7c3	proti
Nizozemsku	Nizozemsko	k1gNnSc3	Nizozemsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sestrou	sestra	k1gFnSc7	sestra
odešly	odejít	k5eAaPmAgFnP	odejít
poraženy	porazit	k5eAaPmNgFnP	porazit
ze	z	k7c2	z
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
rozhodnutého	rozhodnutý	k2eAgInSc2d1	rozhodnutý
stavu	stav	k1gInSc2	stav
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Američanek	Američanka	k1gFnPc2	Američanka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
obvinila	obvinit	k5eAaPmAgFnS	obvinit
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
po	po	k7c6	po
prohraném	prohraný	k2eAgInSc6d1	prohraný
zápase	zápas	k1gInSc6	zápas
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
ze	z	k7c2	z
sexismu	sexismus	k1gInSc2	sexismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
vlnu	vlna	k1gFnSc4	vlna
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
června	červen	k1gInSc2	červen
2019	[number]	k4	2019
si	se	k3xPyFc3	se
po	po	k7c6	po
vyřazení	vyřazení	k1gNnSc6	vyřazení
z	z	k7c2	z
grandslamového	grandslamový	k2eAgInSc2d1	grandslamový
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
prosadila	prosadit	k5eAaPmAgFnS	prosadit
upřednostněný	upřednostněný	k2eAgInSc4d1	upřednostněný
přístup	přístup	k1gInSc4	přístup
do	do	k7c2	do
tiskového	tiskový	k2eAgNnSc2d1	tiskové
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soupeření	soupeření	k1gNnSc4	soupeření
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gMnSc1	Venus
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ženské	ženský	k2eAgFnSc2d1	ženská
dvouhry	dvouhra	k1gFnSc2	dvouhra
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
odehrála	odehrát	k5eAaPmAgFnS	odehrát
proti	proti	k7c3	proti
starší	starý	k2eAgFnSc3d2	starší
sestře	sestra	k1gFnSc3	sestra
Venus	Venus	k1gInSc4	Venus
Williamsové	Williamsové	k2eAgInPc2d1	Williamsové
patnáct	patnáct	k4xCc4	patnáct
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
devět	devět	k4xCc4	devět
finálových	finálový	k2eAgInPc2d1	finálový
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
turnajích	turnaj	k1gInPc6	turnaj
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
potkala	potkat	k5eAaPmAgFnS	potkat
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
drží	držet	k5eAaImIp3nS	držet
aktivní	aktivní	k2eAgFnSc4d1	aktivní
bilanci	bilance	k1gFnSc4	bilance
výher	výhra	k1gFnPc2	výhra
a	a	k8xC	a
proher	prohra	k1gFnPc2	prohra
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Sestry	sestra	k1gFnPc1	sestra
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgFnPc7d1	jediná
tenistkami	tenistka	k1gFnPc7	tenistka
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
éře	éra	k1gFnSc6	éra
světového	světový	k2eAgInSc2d1	světový
tenisu	tenis	k1gInSc2	tenis
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spolu	spolu	k6eAd1	spolu
sehrály	sehrát	k5eAaPmAgFnP	sehrát
čtyři	čtyři	k4xCgFnPc1	čtyři
po	po	k7c6	po
sobě	se	k3xPyFc3	se
jdoucí	jdoucí	k2eAgNnSc4d1	jdoucí
finále	finále	k1gNnSc4	finále
grandslamu	grandslam	k1gInSc2	grandslam
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
od	od	k7c2	od
French	Frencha	k1gFnPc2	Frencha
Open	Open	k1gNnSc1	Open
2002	[number]	k4	2002
po	po	k7c4	po
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vyšla	vyjít	k5eAaPmAgFnS	vyjít
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
vítězně	vítězně	k6eAd1	vítězně
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
utkaly	utkat	k5eAaPmAgInP	utkat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2018	[number]	k4	2018
na	na	k7c6	na
indianwellském	indianwellský	k2eAgNnSc6d1	indianwellský
BNP	BNP	kA	BNP
Paribas	Paribas	k1gMnSc1	Paribas
Open	Open	k1gMnSc1	Open
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
mladší	mladý	k2eAgMnSc1d2	mladší
ze	z	k7c2	z
sourozenkyň	sourozenkyně	k1gFnPc2	sourozenkyně
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
turnaje	turnaj	k1gInPc4	turnaj
po	po	k7c6	po
roční	roční	k2eAgFnSc6d1	roční
přestávce	přestávka	k1gFnSc6	přestávka
pro	pro	k7c4	pro
mateřské	mateřský	k2eAgFnPc4d1	mateřská
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Venus	Venus	k1gInSc4	Venus
Williamsová	Williamsová	k1gFnSc1	Williamsová
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
a	a	k8xC	a
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
aktivity	aktivita	k1gFnPc1	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Móda	móda	k1gFnSc1	móda
===	===	k?	===
</s>
</p>
<p>
<s>
Tenistka	tenistka	k1gFnSc1	tenistka
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgNnSc7	svůj
častým	častý	k2eAgNnSc7d1	časté
nekonformním	konformní	k2eNgNnSc7d1	nekonformní
a	a	k8xC	a
barevným	barevný	k2eAgNnSc7d1	barevné
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nosí	nosit	k5eAaImIp3nS	nosit
během	během	k7c2	během
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
na	na	k7c4	na
US	US	kA	US
Open	Open	k1gInSc4	Open
2002	[number]	k4	2002
svým	svůj	k3xOyFgMnSc7	svůj
černým	černý	k2eAgMnSc7d1	černý
elastanovým	elastanový	k2eAgMnSc7d1	elastanový
lycra	lycra	k1gFnSc1	lycra
kočičím	kočičí	k2eAgNnSc7d1	kočičí
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
grandslamu	grandslam	k1gInSc6	grandslam
představila	představit	k5eAaPmAgFnS	představit
bavlněnou	bavlněný	k2eAgFnSc4d1	bavlněná
denim	denim	k?	denim
sukni	sukně	k1gFnSc4	sukně
a	a	k8xC	a
botami	bota	k1gFnPc7	bota
až	až	k6eAd1	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
kolen	koleno	k1gNnPc2	koleno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
ovšem	ovšem	k9	ovšem
nesměla	smět	k5eNaImAgFnS	smět
nastoupit	nastoupit	k5eAaPmF	nastoupit
k	k	k7c3	k
zápasům	zápas	k1gInPc3	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
2008	[number]	k4	2008
se	se	k3xPyFc4	se
před	před	k7c7	před
utkáními	utkání	k1gNnPc7	utkání
rozehrávala	rozehrávat	k5eAaImAgFnS	rozehrávat
v	v	k7c6	v
bílém	bílý	k2eAgInSc6d1	bílý
nepromokavém	promokavý	k2eNgInSc6d1	nepromokavý
plášti	plášť	k1gInSc6	plášť
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
při	při	k7c6	při
slunečném	slunečný	k2eAgNnSc6d1	slunečné
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mimo	mimo	k7c4	mimo
dvorce	dvorec	k1gInPc4	dvorec
se	se	k3xPyFc4	se
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
novou	nový	k2eAgFnSc7d1	nová
módou	móda	k1gFnSc7	móda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
se	se	k3xPyFc4	se
na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
premiéře	premiéra	k1gFnSc6	premiéra
akční	akční	k2eAgFnSc1d1	akční
komedie	komedie	k1gFnSc1	komedie
Když	když	k8xS	když
se	se	k3xPyFc4	se
setmí	setmět	k5eAaPmIp3nS	setmět
ukázala	ukázat	k5eAaPmAgFnS	ukázat
v	v	k7c6	v
červených	červený	k2eAgInPc6d1	červený
šatech	šat	k1gInPc6	šat
s	s	k7c7	s
hlubokým	hluboký	k2eAgInSc7d1	hluboký
výstřihem	výstřih	k1gInSc7	výstřih
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
smluvního	smluvní	k2eAgNnSc2d1	smluvní
působení	působení	k1gNnSc2	působení
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Puma	puma	k1gFnSc1	puma
měla	mít	k5eAaImAgFnS	mít
speciální	speciální	k2eAgFnPc4d1	speciální
řady	řada	k1gFnPc4	řada
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
po	po	k7c6	po
přestupu	přestup	k1gInSc6	přestup
ke	k	k7c3	k
společnosti	společnost	k1gFnSc3	společnost
Nike	Nike	k1gFnSc2	Nike
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	nízce	k6eAd2	nízce
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
kontrakt	kontrakt	k1gInSc4	kontrakt
na	na	k7c4	na
40	[number]	k4	40
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
také	také	k9	také
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
módní	módní	k2eAgFnSc4d1	módní
řadu	řada	k1gFnSc4	řada
šatů	šat	k1gInPc2	šat
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Aneres	Aneres	k1gInSc1	Aneres
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
napsané	napsaný	k2eAgNnSc4d1	napsané
pozpátku	pozpátku	k6eAd1	pozpátku
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
prodávají	prodávat	k5eAaImIp3nP	prodávat
kolekce	kolekce	k1gFnPc1	kolekce
kabelek	kabelka	k1gFnPc2	kabelka
a	a	k8xC	a
klenotů	klenot	k1gInPc2	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Kolekce	kolekce	k1gFnSc1	kolekce
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Signature	Signatur	k1gMnSc5	Signatur
Statement	Statement	k1gInSc4	Statement
je	být	k5eAaImIp3nS	být
distribuována	distribuován	k2eAgFnSc1d1	distribuována
především	především	k9	především
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
Home	Hom	k1gInSc2	Hom
Shopping	shopping	k1gInSc4	shopping
Network	network	k1gInSc1	network
(	(	kIx(	(
<g/>
HSN	HSN	kA	HSN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
černé	černý	k2eAgFnSc2d1	černá
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
časopisu	časopis	k1gInSc2	časopis
Vogue	Vogu	k1gInSc2	Vogu
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
dubnovém	dubnový	k2eAgNnSc6d1	dubnové
čísle	číslo	k1gNnSc6	číslo
nasnímaná	nasnímaný	k2eAgFnSc1d1	nasnímaná
Annie	Annie	k1gFnSc1	Annie
Leibovitzovou	Leibovitzová	k1gFnSc7	Leibovitzová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
nadabovala	nadabovat	k5eAaPmAgFnS	nadabovat
v	v	k7c6	v
díle	díl	k1gInSc6	díl
animovaném	animovaný	k2eAgInSc6d1	animovaný
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
její	její	k3xOp3gFnSc1	její
postavička	postavička	k1gFnSc1	postavička
zahrála	zahrát	k5eAaPmAgFnS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
sestry	sestra	k1gFnSc2	sestra
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
Peta	Peta	k1gFnSc1	Peta
Samprase	Samprasa	k1gFnSc3	Samprasa
a	a	k8xC	a
Andreho	Andre	k1gMnSc4	Andre
Aggasiho	Aggasi	k1gMnSc4	Aggasi
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
opět	opět	k6eAd1	opět
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
postavám	postava	k1gFnPc3	postava
z	z	k7c2	z
dětského	dětský	k2eAgInSc2d1	dětský
animovaného	animovaný	k2eAgInSc2d1	animovaný
seriálu	seriál	k1gInSc2	seriál
Higglytown	Higglytown	k1gMnSc1	Higglytown
Heroes	Heroes	k1gMnSc1	Heroes
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
epizodě	epizoda	k1gFnSc6	epizoda
kresleného	kreslený	k2eAgInSc2d1	kreslený
Avatar	Avatar	k1gInSc1	Avatar
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Aangovi	Aang	k1gMnSc6	Aang
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
na	na	k7c6	na
kabelovém	kabelový	k2eAgInSc6d1	kabelový
kanálu	kanál	k1gInSc6	kanál
Nickelodeon	Nickelodeon	k1gInSc1	Nickelodeon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
<g/>
.	.	kIx.	.
<g/>
Sérii	série	k1gFnSc4	série
snímků	snímek	k1gInPc2	snímek
nafotila	nafotit	k5eAaPmAgFnS	nafotit
pro	pro	k7c4	pro
vydání	vydání	k1gNnSc4	vydání
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
časopisu	časopis	k1gInSc2	časopis
Sports	Sports	k1gInSc1	Sports
Illustrated	Illustrated	k1gInSc1	Illustrated
Swimsuit	Swimsuit	k1gInSc4	Swimsuit
Issue	Issu	k1gFnSc2	Issu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2005	[number]	k4	2005
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
hudební	hudební	k2eAgFnSc1d1	hudební
stanice	stanice	k1gFnSc1	stanice
MTV	MTV	kA	MTV
záměr	záměr	k1gInSc4	záměr
vysílat	vysílat	k5eAaImF	vysílat
reality	realita	k1gFnSc2	realita
show	show	k1gFnPc2	show
ze	z	k7c2	z
životů	život	k1gInPc2	život
Sereny	Serena	k1gFnSc2	Serena
a	a	k8xC	a
Venus	Venus	k1gInSc1	Venus
<g/>
,	,	kIx,	,
s	s	k7c7	s
variantou	varianta	k1gFnSc7	varianta
přenosu	přenos	k1gInSc2	přenos
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
ABC	ABC	kA	ABC
Family	Famil	k1gInPc7	Famil
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
skryté	skrytý	k2eAgFnSc6d1	skrytá
kameře	kamera	k1gFnSc6	kamera
Punk	punk	k1gInSc1	punk
<g/>
'	'	kIx"	'
<g/>
d	d	k?	d
stanice	stanice	k1gFnSc2	stanice
MTV	MTV	kA	MTV
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
také	také	k9	také
jako	jako	k9	jako
poslední	poslední	k2eAgFnSc1d1	poslední
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c4	v
reality	realita	k1gFnPc4	realita
seriálu	seriál	k1gInSc2	seriál
kanálu	kanál	k1gInSc2	kanál
ABC	ABC	kA	ABC
s	s	k7c7	s
názvem	název	k1gInSc7	název
Fast	Fast	k2eAgInSc1d1	Fast
Cars	Cars	k1gInSc1	Cars
and	and	k?	and
Superstars	Superstars	k1gInSc4	Superstars
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Gillette	Gillett	k1gMnSc5	Gillett
Young	Young	k1gMnSc1	Young
Guns	Guns	k1gInSc4	Guns
Celebrity	celebrita	k1gFnSc2	celebrita
Race	Rac	k1gInSc2	Rac
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
roli	role	k1gFnSc4	role
slečny	slečna	k1gFnSc2	slečna
Wigginsové	Wigginsový	k2eAgFnSc2d1	Wigginsový
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Crouching	Crouching	k1gInSc1	Crouching
Mother	Mothra	k1gFnPc2	Mothra
<g/>
,	,	kIx,	,
Hidden	Hiddna	k1gFnPc2	Hiddna
Father	Fathra	k1gFnPc2	Fathra
<g/>
"	"	kIx"	"
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
My	my	k3xPp1nPc1	my
Wife	Wife	k1gNnPc7	Wife
and	and	k?	and
Kids	Kidsa	k1gFnPc2	Kidsa
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
také	také	k9	také
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
postavy	postava	k1gFnPc4	postava
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
Pohotovosti	pohotovost	k1gFnPc1	pohotovost
a	a	k8xC	a
Zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
pořádek	pořádek	k1gInSc1	pořádek
<g/>
:	:	kIx,	:
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
I	i	k9	i
Want	Want	k2eAgMnSc1d1	Want
You	You	k1gMnSc1	You
amerického	americký	k2eAgMnSc2d1	americký
rapera	raper	k1gMnSc2	raper
Commona	Common	k1gMnSc2	Common
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Alicie	Alicie	k1gFnSc2	Alicie
Keysové	Keysová	k1gFnSc2	Keysová
a	a	k8xC	a
Kanya	Kanya	k1gMnSc1	Kanya
Westa	Westa	k1gMnSc1	Westa
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
aktivní	aktivní	k2eAgFnSc7d1	aktivní
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
reklamy	reklama	k1gFnPc4	reklama
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
produktů	produkt	k1gInPc2	produkt
ženské	ženský	k2eAgFnSc2d1	ženská
hygieny	hygiena	k1gFnSc2	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
sérii	série	k1gFnSc6	série
online	onlinout	k5eAaPmIp3nS	onlinout
videí	video	k1gNnPc2	video
a	a	k8xC	a
tištěných	tištěný	k2eAgFnPc2d1	tištěná
reklam	reklama	k1gFnPc2	reklama
na	na	k7c4	na
zaváděcí	zaváděcí	k2eAgInPc4d1	zaváděcí
tampony	tampon	k1gInPc4	tampon
Tampax	Tampax	k1gInSc1	Tampax
<g/>
.	.	kIx.	.
</s>
<s>
Manažer	manažer	k1gMnSc1	manažer
společnosti	společnost	k1gFnSc2	společnost
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hráčce	hráčka	k1gFnSc3	hráčka
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Protože	protože	k8xS	protože
s	s	k7c7	s
naším	náš	k3xOp1gNnSc7	náš
odvětvím	odvětví	k1gNnSc7	odvětví
nechce	chtít	k5eNaImIp3nS	chtít
mít	mít	k5eAaImF	mít
řada	řada	k1gFnSc1	řada
hvězd	hvězda	k1gFnPc2	hvězda
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
nadšení	nadšený	k2eAgMnPc1d1	nadšený
<g/>
,	,	kIx,	,
že	že	k8xS	že
Serena	Serena	k1gFnSc1	Serena
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gMnSc1	Venus
byly	být	k5eAaImAgInP	být
námětem	námět	k1gInSc7	námět
vzniku	vznik	k1gInSc2	vznik
několika	několik	k4yIc2	několik
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
Super	super	k2eAgFnSc2d1	super
Furry	Furra	k1gFnSc2	Furra
Animals	Animalsa	k1gFnPc2	Animalsa
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
Phantom	Phantom	k1gInSc4	Phantom
Power	Power	k1gInSc4	Power
natočila	natočit	k5eAaBmAgFnS	natočit
píseň	píseň	k1gFnSc1	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
Venus	Venus	k1gMnSc1	Venus
and	and	k?	and
Serena	Serena	k1gFnSc1	Serena
<g/>
.	.	kIx.	.
</s>
<s>
Sourozenkyň	sourozenkyně	k1gFnPc2	sourozenkyně
se	se	k3xPyFc4	se
také	také	k9	také
týkají	týkat	k5eAaImIp3nP	týkat
singly	singl	k1gInPc4	singl
Signs	Signsa	k1gFnPc2	Signsa
od	od	k7c2	od
Snoop	Snoop	k1gInSc4	Snoop
Dogga	Dogga	k1gFnSc1	Dogga
a	a	k8xC	a
My	my	k3xPp1nPc1	my
Chick	Chicka	k1gFnPc2	Chicka
Bad	Bad	k1gMnPc4	Bad
od	od	k7c2	od
rappera	rappero	k1gNnSc2	rappero
Ludacrise	Ludacrise	k1gFnSc2	Ludacrise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Herecká	herecký	k2eAgFnSc1d1	herecká
filmografie	filmografie	k1gFnSc1	filmografie
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Podnikání	podnikání	k1gNnSc1	podnikání
<g/>
:	:	kIx,	:
Miami	Miami	k1gNnSc1	Miami
Dolphins	Dolphinsa	k1gFnPc2	Dolphinsa
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2009	[number]	k4	2009
se	se	k3xPyFc4	se
sestry	sestra	k1gFnPc1	sestra
Williamsovy	Williamsův	k2eAgFnSc2d1	Williamsova
staly	stát	k5eAaPmAgFnP	stát
spoluvlastníky	spoluvlastník	k1gMnPc7	spoluvlastník
klubu	klub	k1gInSc2	klub
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
Miami	Miami	k1gNnSc2	Miami
Dolphins	Dolphinsa	k1gFnPc2	Dolphinsa
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
byla	být	k5eAaImAgFnS	být
informace	informace	k1gFnSc1	informace
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
po	po	k7c6	po
prohlídce	prohlídka	k1gFnSc6	prohlídka
tréninkového	tréninkový	k2eAgNnSc2d1	tréninkové
hřiště	hřiště	k1gNnSc2	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnPc4	první
Afroameričanky	Afroameričanka	k1gFnPc4	Afroameričanka
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
National	National	k1gFnPc3	National
Football	Football	k1gInSc4	Football
League	League	k1gInSc4	League
povolila	povolit	k5eAaPmAgFnS	povolit
franšízing	franšízing	k1gInSc4	franšízing
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
prominentními	prominentní	k2eAgMnPc7d1	prominentní
spoluvlastníky	spoluvlastník	k1gMnPc7	spoluvlastník
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
Jimmy	Jimm	k1gMnPc4	Jimm
Buffett	Buffetta	k1gFnPc2	Buffetta
<g/>
,	,	kIx,	,
Gloria	Gloria	k1gFnSc1	Gloria
a	a	k8xC	a
Emilio	Emilio	k1gMnSc1	Emilio
Estefanovi	Estefanův	k2eAgMnPc1d1	Estefanův
(	(	kIx(	(
<g/>
první	první	k4xOgMnPc1	první
kubánsko-američtí	kubánskomerický	k2eAgMnPc1d1	kubánsko-americký
vlastníci	vlastník	k1gMnPc1	vlastník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marc	Marc	k1gInSc1	Marc
Anthony	Anthona	k1gFnSc2	Anthona
či	či	k8xC	či
Jennifer	Jennifer	k1gMnSc1	Jennifer
Lopez	Lopez	k1gMnSc1	Lopez
<g/>
.	.	kIx.	.
</s>
<s>
Většinový	většinový	k2eAgMnSc1d1	většinový
vlastník	vlastník	k1gMnSc1	vlastník
klubu	klub	k1gInSc2	klub
Stephan	Stephan	k1gMnSc1	Stephan
Ross	Ross	k1gInSc4	Ross
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsme	být	k5eAaImIp1nP	být
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
minoritními	minoritní	k2eAgMnPc7d1	minoritní
společníky	společník	k1gMnPc7	společník
staly	stát	k5eAaPmAgInP	stát
Venus	Venus	k1gInSc4	Venus
a	a	k8xC	a
Serena	Serena	k1gFnSc1	Serena
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejrespektovanějším	respektovaný	k2eAgFnPc3d3	nejrespektovanější
světovým	světový	k2eAgFnPc3d1	světová
sportovkyním	sportovkyně	k1gFnPc3	sportovkyně
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vyslankyněmi	vyslankyně	k1gFnPc7	vyslankyně
tenisu	tenis	k1gInSc2	tenis
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vstup	vstup	k1gInSc1	vstup
také	také	k9	také
odráží	odrážet	k5eAaImIp3nS	odrážet
náš	náš	k3xOp1gInSc4	náš
závazek	závazek	k1gInSc4	závazek
mít	mít	k5eAaImF	mít
tah	tah	k1gInSc4	tah
na	na	k7c4	na
branku	branka	k1gFnSc4	branka
a	a	k8xC	a
dokládá	dokládat	k5eAaImIp3nS	dokládat
velkou	velký	k2eAgFnSc4d1	velká
různorodost	různorodost	k1gFnSc4	různorodost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Floridy	Florida	k1gFnSc2	Florida
činí	činit	k5eAaImIp3nS	činit
multikulturní	multikulturní	k2eAgInSc4d1	multikulturní
klenot	klenot	k1gInSc4	klenot
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Charita	charita	k1gFnSc1	charita
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
hráčka	hráčka	k1gFnSc1	hráčka
pomohla	pomoct	k5eAaPmAgFnS	pomoct
financovat	financovat	k5eAaBmF	financovat
výstavbu	výstavba	k1gFnSc4	výstavba
Střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
Sereny	Serena	k1gFnSc2	Serena
Williamsové	Williamsová	k1gFnSc2	Williamsová
v	v	k7c6	v
keňském	keňský	k2eAgInSc6d1	keňský
Matooni	Matooň	k1gFnSc3	Matooň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
ji	on	k3xPp3gFnSc4	on
Nadace	nadace	k1gFnSc1	nadace
Avon	Avon	k1gMnSc1	Avon
ocenila	ocenit	k5eAaPmAgFnS	ocenit
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
získání	získání	k1gNnSc4	získání
finančních	finanční	k2eAgInPc2d1	finanční
příspěvků	příspěvek	k1gInPc2	příspěvek
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
prevence	prevence	k1gFnSc2	prevence
rakoviny	rakovina	k1gFnSc2	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
řady	řada	k1gFnSc2	řada
besed	beseda	k1gFnPc2	beseda
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
komunitních	komunitní	k2eAgNnPc6d1	komunitní
centrech	centrum	k1gNnPc6	centrum
zaměřených	zaměřený	k2eAgNnPc6d1	zaměřené
na	na	k7c4	na
rizika	riziko	k1gNnPc4	riziko
u	u	k7c2	u
dospívajících	dospívající	k2eAgInPc2d1	dospívající
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
na	na	k7c6	na
Haiti	Haiti	k1gNnPc6	Haiti
2010	[number]	k4	2010
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
tenisty	tenista	k1gMnPc7	tenista
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vypustit	vypustit	k5eAaPmF	vypustit
poslední	poslední	k2eAgFnSc4d1	poslední
přípravu	příprava	k1gFnSc4	příprava
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc1	Open
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
charitativní	charitativní	k2eAgFnSc2d1	charitativní
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podpořila	podpořit	k5eAaPmAgFnS	podpořit
oběti	oběť	k1gFnPc4	oběť
přírodní	přírodní	k2eAgFnSc2d1	přírodní
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literární	literární	k2eAgFnSc1d1	literární
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
2009	[number]	k4	2009
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
píše	psát	k5eAaImIp3nS	psát
námět	námět	k1gInSc4	námět
k	k	k7c3	k
televiznímu	televizní	k2eAgInSc3d1	televizní
seriálu	seriál	k1gInSc3	seriál
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
odevzdat	odevzdat	k5eAaPmF	odevzdat
agentuře	agentura	k1gFnSc3	agentura
k	k	k7c3	k
převedení	převedení	k1gNnSc3	převedení
na	na	k7c4	na
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
měl	mít	k5eAaImAgInS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
prvky	prvek	k1gInPc4	prvek
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
amerických	americký	k2eAgInPc2d1	americký
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgInPc7	jaký
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
Zoufalé	zoufalý	k2eAgFnPc1d1	zoufalá
manželky	manželka	k1gFnPc1	manželka
<g/>
,	,	kIx,	,
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
Griffinovi	Griffin	k1gMnSc3	Griffin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
knihou	kniha	k1gFnSc7	kniha
tenistky	tenistka	k1gFnSc2	tenistka
je	být	k5eAaImIp3nS	být
autobiografie	autobiografie	k1gFnSc1	autobiografie
On	on	k3xPp3gInSc1	on
the	the	k?	the
Line	linout	k5eAaImIp3nS	linout
(	(	kIx(	(
<g/>
Na	na	k7c6	na
čáře	čára	k1gFnSc6	čára
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
US	US	kA	US
Open	Open	k1gInSc4	Open
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
Serena	Serena	k1gFnSc1	Serena
a	a	k8xC	a
Venus	Venus	k1gInSc1	Venus
Williamsovy	Williamsův	k2eAgFnPc1d1	Williamsova
byly	být	k5eAaImAgFnP	být
vychovány	vychován	k2eAgFnPc1d1	vychována
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
víry	víra	k1gFnSc2	víra
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgMnPc2d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Tenistka	tenistka	k1gFnSc1	tenistka
ovšem	ovšem	k9	ovšem
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
vyznání	vyznání	k1gNnSc4	vyznání
"	"	kIx"	"
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nikdy	nikdy	k6eAd1	nikdy
nepraktikovala	praktikovat	k5eNaImAgFnS	praktikovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězných	vítězný	k2eAgInPc6d1	vítězný
zápasech	zápas	k1gInPc6	zápas
přesto	přesto	k8xC	přesto
často	často	k6eAd1	často
děkuje	děkovat	k5eAaImIp3nS	děkovat
Jehovovi	Jehova	k1gMnSc3	Jehova
<g/>
.	.	kIx.	.
</s>
<s>
Sestry	sestra	k1gFnPc1	sestra
vyjádřily	vyjádřit	k5eAaPmAgFnP	vyjádřit
podporu	podpora	k1gFnSc4	podpora
demokratickému	demokratický	k2eAgNnSc3d1	demokratické
kandidátu	kandidát	k1gMnSc3	kandidát
Baracku	Barack	k1gInSc2	Barack
Obamovi	Obam	k1gMnSc3	Obam
před	před	k7c7	před
prezidentskými	prezidentský	k2eAgFnPc7d1	prezidentská
volbami	volba	k1gFnPc7	volba
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
<g/>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
pozápasové	pozápasový	k2eAgInPc1d1	pozápasový
rozhovory	rozhovor	k1gInPc1	rozhovor
na	na	k7c4	na
French	French	k1gInSc4	French
Open	Openo	k1gNnPc2	Openo
opakovaně	opakovaně	k6eAd1	opakovaně
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
polosestra	polosestra	k1gFnSc1	polosestra
Yetunde	Yetund	k1gInSc5	Yetund
Priceová	Priceová	k1gFnSc1	Priceová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
příležitostně	příležitostně	k6eAd1	příležitostně
pracovala	pracovat	k5eAaImAgFnS	pracovat
pro	pro	k7c4	pro
Venus	Venus	k1gInSc4	Venus
a	a	k8xC	a
Serenu	Serena	k1gFnSc4	Serena
Williamsovy	Williamsův	k2eAgFnSc2d1	Williamsova
jako	jako	k8xC	jako
asistentka	asistentka	k1gFnSc1	asistentka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2003	[number]	k4	2003
zastřelena	zastřelen	k2eAgFnSc1d1	zastřelena
<g/>
.	.	kIx.	.
</s>
<s>
Vrah	vrah	k1gMnSc1	vrah
byl	být	k5eAaImAgMnS	být
shledán	shledat	k5eAaPmNgMnS	shledat
vinným	vinný	k1gMnSc7	vinný
z	z	k7c2	z
úmyslného	úmyslný	k2eAgNnSc2d1	úmyslné
zabití	zabití	k1gNnSc2	zabití
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
patnácti	patnáct	k4xCc3	patnáct
letům	let	k1gInPc3	let
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
<g/>
Partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
udržovala	udržovat	k5eAaImAgFnS	udržovat
s	s	k7c7	s
raperem	raper	k1gMnSc7	raper
Commonem	Common	k1gMnSc7	Common
<g/>
,	,	kIx,	,
než	než	k8xS	než
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
k	k	k7c3	k
rozchodu	rozchod	k1gInSc3	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
zasnoubila	zasnoubit	k5eAaPmAgFnS	zasnoubit
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
podnikatelem	podnikatel	k1gMnSc7	podnikatel
Alexisem	Alexis	k1gInSc7	Alexis
Ohanianem	Ohanian	k1gMnSc7	Ohanian
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
sociální	sociální	k2eAgFnSc2d1	sociální
sítě	síť	k1gFnSc2	síť
Reddit	Reddit	k1gFnSc2	Reddit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2017	[number]	k4	2017
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ve	v	k7c6	v
středisku	středisko	k1gNnSc6	středisko
St.	st.	kA	st.
Mary	Mary	k1gFnSc1	Mary
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Medical	Medical	k1gFnSc7	Medical
Center	centrum	k1gNnPc2	centrum
ve	v	k7c6	v
floridském	floridský	k2eAgInSc6d1	floridský
West	West	k1gInSc4	West
Palm	Pal	k1gNnSc7	Pal
Beach	Beacha	k1gFnPc2	Beacha
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
2017	[number]	k4	2017
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Vogue	Vogue	k1gInSc4	Vogue
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Ohanianem	Ohanian	k1gInSc7	Ohanian
plánují	plánovat	k5eAaImIp3nP	plánovat
svatbu	svatba	k1gFnSc4	svatba
a	a	k8xC	a
poté	poté	k6eAd1	poté
přestěhování	přestěhování	k1gNnSc1	přestěhování
do	do	k7c2	do
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
rekordy	rekord	k1gInPc1	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
Lipton	Lipton	k1gInSc1	Lipton
International	International	k1gFnSc2	International
Players	Players	k1gInSc1	Players
Championships	Championships	k1gInSc1	Championships
1998	[number]	k4	1998
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
pátý	pátý	k4xOgInSc4	pátý
zápas	zápas	k1gInSc4	zápas
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
nad	nad	k7c7	nad
hráčkou	hráčka	k1gFnSc7	hráčka
elitní	elitní	k2eAgFnSc2d1	elitní
desítky	desítka	k1gFnSc2	desítka
žebříčku	žebříček	k1gInSc2	žebříček
WTA	WTA	kA	WTA
(	(	kIx(	(
<g/>
TOP	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tenistkou	tenistka	k1gFnSc7	tenistka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tohoto	tento	k3xDgInSc2	tento
výkonu	výkon	k1gInSc2	výkon
od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
na	na	k7c4	na
profesionální	profesionální	k2eAgInSc4d1	profesionální
okruh	okruh	k1gInSc4	okruh
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nejrychleji	rychle	k6eAd3	rychle
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
šestnácti	šestnáct	k4xCc2	šestnáct
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Evert	Evert	k1gInSc4	Evert
Cupu	cup	k1gInSc2	cup
1999	[number]	k4	1999
v	v	k7c6	v
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
nenasazená	nasazený	k2eNgFnSc1d1	nenasazená
hráčka	hráčka	k1gFnSc1	hráčka
historie	historie	k1gFnSc2	historie
turnaj	turnaj	k1gInSc1	turnaj
kategorie	kategorie	k1gFnSc2	kategorie
WTA	WTA	kA	WTA
Tier	Tier	k1gMnSc1	Tier
I.	I.	kA	I.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
tenistkou	tenistka	k1gFnSc7	tenistka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
Turnaj	turnaj	k1gInSc4	turnaj
mistryň	mistryně	k1gFnPc2	mistryně
na	na	k7c4	na
první	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
French	French	k1gInSc4	French
Open	Open	k1gInSc4	Open
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
mladší	mladý	k2eAgFnSc7d2	mladší
sestrou	sestra	k1gFnSc7	sestra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
grandslamu	grandslam	k1gInSc2	grandslam
porazit	porazit	k5eAaPmF	porazit
starší	starý	k2eAgFnSc4d2	starší
sourozenkyni	sourozenkyně	k1gFnSc4	sourozenkyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2002	[number]	k4	2002
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Venus	Venus	k1gMnSc1	Venus
staly	stát	k5eAaPmAgFnP	stát
prvním	první	k4xOgInSc7	první
sourozeneckým	sourozenecký	k2eAgInSc7d1	sourozenecký
párem	pár	k1gInSc7	pár
historie	historie	k1gFnSc2	historie
tenisu	tenis	k1gInSc2	tenis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
souběžně	souběžně	k6eAd1	souběžně
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Titulem	titul	k1gInSc7	titul
na	na	k7c4	na
Australian	Australian	k1gInSc4	Australian
Open	Open	k1gInSc4	Open
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
Afroameričankou	Afroameričanka	k1gFnSc7	Afroameričanka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
tento	tento	k3xDgInSc4	tento
turnaj	turnaj	k1gInSc4	turnaj
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Sony	Sony	kA	Sony
Ericsson	Ericsson	kA	Ericsson
Open	Open	k1gInSc4	Open
2007	[number]	k4	2007
v	v	k7c6	v
Key	Key	k1gFnSc6	Key
Biscayne	Biscayn	k1gInSc5	Biscayn
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejníže	nízce	k6eAd3	nízce
postavenou	postavený	k2eAgFnSc7d1	postavená
tenistkou	tenistka	k1gFnSc7	tenistka
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
č.	č.	k?	č.
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
porazila	porazit	k5eAaPmAgFnS	porazit
dvě	dva	k4xCgFnPc4	dva
nejvýše	nejvýše	k6eAd1	nejvýše
postavené	postavený	k2eAgFnPc1d1	postavená
hráčky	hráčka	k1gFnPc1	hráčka
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jediného	jediný	k2eAgInSc2d1	jediný
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2008	[number]	k4	2008
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
hráčky	hráčka	k1gFnPc4	hráčka
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
č.	č.	k?	č.
1	[number]	k4	1
na	na	k7c6	na
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
nejdelším	dlouhý	k2eAgNnSc6d3	nejdelší
časovém	časový	k2eAgNnSc6d1	časové
období	období	k1gNnSc6	období
vůbec	vůbec	k9	vůbec
–	–	k?	–
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
jednom	jeden	k4xCgInSc6	jeden
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časopis	časopis	k1gInSc1	časopis
Men	Men	k1gFnSc2	Men
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fitness	Fitness	k1gInSc1	Fitness
ji	on	k3xPp3gFnSc4	on
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejvíce	hodně	k6eAd3	hodně
pověrčivých	pověrčivý	k2eAgMnPc2d1	pověrčivý
sportovců	sportovec	k1gMnPc2	sportovec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčské	hráčský	k2eAgFnPc1d1	hráčská
statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
Statistiky	statistika	k1gFnPc1	statistika
a	a	k8xC	a
dosažené	dosažený	k2eAgInPc1d1	dosažený
výsledky	výsledek	k1gInPc1	výsledek
tenistky	tenistka	k1gFnSc2	tenistka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
článek	článek	k1gInSc1	článek
Hráčské	hráčský	k2eAgFnSc2d1	hráčská
statistiky	statistika	k1gFnSc2	statistika
Sereny	Serena	k1gFnSc2	Serena
Williamsové	Williamsová	k1gFnSc2	Williamsová
</s>
</p>
<p>
<s>
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
39	[number]	k4	39
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
23	[number]	k4	23
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
<g/>
,	,	kIx,	,
14	[number]	k4	14
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
a	a	k8xC	a
2	[number]	k4	2
ve	v	k7c6	v
smíšené	smíšený	k2eAgFnSc6d1	smíšená
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
otevřené	otevřený	k2eAgFnSc2d1	otevřená
éry	éra	k1gFnSc2	éra
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Toura	k1gFnPc2	Toura
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
72	[number]	k4	72
singlových	singlový	k2eAgNnPc2d1	singlové
a	a	k8xC	a
23	[number]	k4	23
deblových	deblový	k2eAgInPc2d1	deblový
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
na	na	k7c4	na
Grand	grand	k1gMnSc1	grand
Slamu	slam	k1gInSc2	slam
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Ženská	ženský	k2eAgFnSc1d1	ženská
dvouhra	dvouhra	k1gFnSc1	dvouhra
<g/>
:	:	kIx,	:
31	[number]	k4	31
finále	finále	k1gNnSc2	finále
(	(	kIx(	(
<g/>
23	[number]	k4	23
výher	výhra	k1gFnPc2	výhra
<g/>
,	,	kIx,	,
8	[number]	k4	8
proher	prohra	k1gFnPc2	prohra
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Ženská	ženský	k2eAgFnSc1d1	ženská
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
<g/>
:	:	kIx,	:
14	[number]	k4	14
finále	finále	k1gNnSc2	finále
(	(	kIx(	(
<g/>
14	[number]	k4	14
výher	výhra	k1gFnPc2	výhra
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Smíšená	smíšený	k2eAgFnSc1d1	smíšená
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
<g/>
:	:	kIx,	:
4	[number]	k4	4
finále	finále	k1gNnSc2	finále
(	(	kIx(	(
<g/>
2	[number]	k4	2
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
2	[number]	k4	2
prohry	prohra	k1gFnPc1	prohra
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Chronologie	chronologie	k1gFnPc4	chronologie
grandslamových	grandslamový	k2eAgInPc2d1	grandslamový
výsledků	výsledek	k1gInPc2	výsledek
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Ženská	ženský	k2eAgFnSc1d1	ženská
dvouhra	dvouhra	k1gFnSc1	dvouhra
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Ženská	ženský	k2eAgFnSc1d1	ženská
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
====	====	k?	====
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Serena	Sereno	k1gNnSc2	Sereno
Williams	Williamsa	k1gFnPc2	Williamsa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
–	–	k?	–
oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Ženské	ženský	k2eAgFnSc2d1	ženská
tenisové	tenisový	k2eAgFnSc2d1	tenisová
asociace	asociace	k1gFnSc2	asociace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
tenisové	tenisový	k2eAgFnSc2d1	tenisová
federace	federace	k1gFnSc2	federace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Serena	Seren	k2eAgFnSc1d1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
