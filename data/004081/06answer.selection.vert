<s>
Hrách	hrách	k1gInSc1	hrách
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
luštěnina	luštěnina	k1gFnSc1	luštěnina
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
chutným	chutný	k2eAgInPc3d1	chutný
plodům	plod	k1gInPc3	plod
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vitamíny	vitamín	k1gInPc4	vitamín
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
skupiny	skupina	k1gFnPc4	skupina
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
