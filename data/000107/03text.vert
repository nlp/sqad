<s>
Salina	salina	k1gFnSc1	salina
(	(	kIx(	(
<g/>
26,1	[number]	k4	26,1
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
z	z	k7c2	z
Liparských	Liparský	k2eAgInPc2d1	Liparský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
však	však	k9	však
pouze	pouze	k6eAd1	pouze
geograficky	geograficky	k6eAd1	geograficky
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
správě	správa	k1gFnSc6	správa
z	z	k7c2	z
Lipari	Lipar	k1gFnSc2	Lipar
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
je	být	k5eAaImIp3nS	být
i	i	k9	i
svou	svůj	k3xOyFgFnSc7	svůj
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholové	vrcholový	k2eAgFnPc1d1	vrcholová
partie	partie	k1gFnPc1	partie
jejích	její	k3xOp3gFnPc2	její
šesti	šest	k4xCc2	šest
vyhaslých	vyhaslý	k2eAgInPc2d1	vyhaslý
sopečných	sopečný	k2eAgInPc2d1	sopečný
kuželů	kužel	k1gInPc2	kužel
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
ostrovů	ostrov	k1gInPc2	ostrov
pokryty	pokryt	k2eAgMnPc4d1	pokryt
hustým	hustý	k2eAgInSc7d1	hustý
jehličnatým	jehličnatý	k2eAgInSc7d1	jehličnatý
lesem	les	k1gInSc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Monte	Mont	k1gInSc5	Mont
Fossa	Fossa	k1gFnSc1	Fossa
delle	delle	k1gInSc1	delle
Felci	Felce	k1gFnSc4	Felce
vysoký	vysoký	k2eAgInSc1d1	vysoký
962	[number]	k4	962
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
horské	horský	k2eAgInPc4d1	horský
masivy	masiv	k1gInPc4	masiv
<g/>
,	,	kIx,	,
rozdělené	rozdělený	k2eAgInPc4d1	rozdělený
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
průrvou	průrvou	k?	průrvou
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
ostrova	ostrov	k1gInSc2	ostrov
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
především	především	k9	především
na	na	k7c4	na
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
lesnictví	lesnictví	k1gNnSc4	lesnictví
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
kapary	kapary	k1gInPc4	kapary
a	a	k8xC	a
malvazové	malvazový	k2eAgNnSc4d1	malvazový
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
stoupá	stoupat	k5eAaImIp3nS	stoupat
i	i	k9	i
význam	význam	k1gInSc4	význam
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
města	město	k1gNnSc2	město
Santa	Santa	k1gFnSc1	Santa
Marina	Marina	k1gFnSc1	Marina
Salina	salina	k1gFnSc1	salina
<g/>
,	,	kIx,	,
Malfa	Malf	k1gMnSc2	Malf
a	a	k8xC	a
Leni	Leni	k?	Leni
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
zhruba	zhruba	k6eAd1	zhruba
2280	[number]	k4	2280
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
