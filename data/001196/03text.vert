<s>
Helium	helium	k1gNnSc1	helium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
He	he	k0	he
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Helium	helium	k1gNnSc4	helium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
tvořící	tvořící	k2eAgFnPc4d1	tvořící
druhou	druhý	k4xOgFnSc4	druhý
nejvíce	hodně	k6eAd3	hodně
zastoupenou	zastoupený	k2eAgFnSc4d1	zastoupená
složku	složka	k1gFnSc4	složka
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
izotop	izotop	k1gInSc1	izotop
4	[number]	k4	4
<g/>
He	he	k0	he
(	(	kIx(	(
<g/>
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
nukleony	nukleon	k1gInPc7	nukleon
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
stopovém	stopový	k2eAgNnSc6d1	stopové
množství	množství	k1gNnSc6	množství
i	i	k8xC	i
izotop	izotop	k1gInSc1	izotop
3	[number]	k4	3
<g/>
He	he	k0	he
(	(	kIx(	(
<g/>
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
nukleony	nukleon	k1gInPc7	nukleon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
zcela	zcela	k6eAd1	zcela
inertní	inertní	k2eAgNnPc1d1	inertní
-	-	kIx~	-
helium	helium	k1gNnSc1	helium
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
sloučeniny	sloučenina	k1gFnSc2	sloučenina
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
fullereny	fulleren	k1gInPc7	fulleren
a	a	k8xC	a
se	s	k7c7	s
rtutí	rtuť	k1gFnSc7	rtuť
(	(	kIx(	(
<g/>
helidy	helida	k1gFnSc2	helida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
8,8	[number]	k4	8,8
ml	ml	kA	ml
He	he	k0	he
v	v	k7c6	v
1000	[number]	k4	1000
ml	ml	kA	ml
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Helium	helium	k1gNnSc1	helium
a	a	k8xC	a
i	i	k9	i
ostatní	ostatní	k2eAgInPc1d1	ostatní
vzácné	vzácný	k2eAgInPc1d1	vzácný
plyny	plyn	k1gInPc1	plyn
mají	mít	k5eAaImIp3nP	mít
malé	malý	k2eAgNnSc4d1	malé
elektrické	elektrický	k2eAgNnSc4d1	elektrické
průrazné	průrazný	k2eAgNnSc4d1	průrazné
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
ionizují	ionizovat	k5eAaBmIp3nP	ionizovat
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
vedou	vést	k5eAaImIp3nP	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
výbojek	výbojka	k1gFnPc2	výbojka
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
září	září	k1gNnSc2	září
intenzivně	intenzivně	k6eAd1	intenzivně
žlutě	žlutě	k6eAd1	žlutě
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
a	a	k8xC	a
normálním	normální	k2eAgInSc6d1	normální
tlaku	tlak	k1gInSc6	tlak
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kapalná	kapalný	k2eAgFnSc1d1	kapalná
až	až	k9	až
k	k	k7c3	k
teplotě	teplota	k1gFnSc3	teplota
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgNnSc4d1	pevné
helium	helium	k1gNnSc4	helium
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
známých	známý	k2eAgFnPc2d1	známá
látek	látka	k1gFnPc2	látka
nejnižší	nízký	k2eAgInSc4d3	nejnižší
bod	bod	k1gInSc4	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Kapalné	kapalný	k2eAgNnSc1d1	kapalné
helium	helium	k1gNnSc1	helium
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyniká	vynikat	k5eAaImIp3nS	vynikat
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
2,176	[number]	k4	2,176
<g/>
8	[number]	k4	8
K	K	kA	K
je	být	k5eAaImIp3nS	být
supratekuté	supratekutý	k2eAgNnSc1d1	supratekuté
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
bez	bez	k7c2	bez
tření	tření	k1gNnSc2	tření
protékat	protékat	k5eAaImF	protékat
libovolnými	libovolný	k2eAgInPc7d1	libovolný
předměty	předmět	k1gInPc7	předmět
a	a	k8xC	a
téct	téct	k5eAaImF	téct
bez	bez	k7c2	bez
tření	tření	k1gNnSc2	tření
po	po	k7c6	po
libovolných	libovolný	k2eAgInPc6d1	libovolný
předmětech	předmět	k1gInPc6	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
vodivost	vodivost	k1gFnSc1	vodivost
tekutého	tekutý	k2eAgNnSc2d1	tekuté
helia	helium	k1gNnSc2	helium
je	být	k5eAaImIp3nS	být
tři	tři	k4xCgInPc1	tři
milionkrát	milionkrát	k6eAd1	milionkrát
větší	veliký	k2eAgInPc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
mědi	měď	k1gFnSc2	měď
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
objev	objev	k1gInSc1	objev
helia	helium	k1gNnSc2	helium
byl	být	k5eAaImAgInS	být
učiněn	učinit	k5eAaPmNgInS	učinit
zkoumáním	zkoumání	k1gNnSc7	zkoumání
spektra	spektrum	k1gNnSc2	spektrum
sluneční	sluneční	k2eAgFnSc2d1	sluneční
korony	korona	k1gFnSc2	korona
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
při	při	k7c6	při
zatmění	zatmění	k1gNnSc6	zatmění
Slunce	slunce	k1gNnSc2	slunce
francouzský	francouzský	k2eAgMnSc1d1	francouzský
astronom	astronom	k1gMnSc1	astronom
Pierre	Pierr	k1gInSc5	Pierr
Janssen	Janssen	k1gInSc4	Janssen
objevil	objevit	k5eAaPmAgInS	objevit
neznámé	známý	k2eNgFnPc4d1	neznámá
žluté	žlutý	k2eAgFnPc4d1	žlutá
spektrální	spektrální	k2eAgFnPc4d1	spektrální
linie	linie	k1gFnPc4	linie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
přiřazeny	přiřadit	k5eAaPmNgFnP	přiřadit
doposud	doposud	k6eAd1	doposud
neznámému	známý	k2eNgInSc3d1	neznámý
prvku	prvek	k1gInSc3	prvek
<g/>
,	,	kIx,	,
pojmenovaném	pojmenovaný	k2eAgMnSc6d1	pojmenovaný
po	po	k7c6	po
starořeckém	starořecký	k2eAgMnSc6d1	starořecký
bohu	bůh	k1gMnSc6	bůh
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
Héliovi	Héliův	k2eAgMnPc1d1	Héliův
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
se	se	k3xPyFc4	se
britskému	britský	k2eAgMnSc3d1	britský
chemikovi	chemik	k1gMnSc3	chemik
Williamu	William	k1gInSc2	William
Ramsayovi	Ramsaya	k1gMnSc3	Ramsaya
podařilo	podařit	k5eAaPmAgNnS	podařit
izolovat	izolovat	k5eAaBmF	izolovat
plynné	plynný	k2eAgNnSc4d1	plynné
helium	helium	k1gNnSc4	helium
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
astronomové	astronom	k1gMnPc1	astronom
<g/>
,	,	kIx,	,
francouz	francouz	k1gMnSc1	francouz
Pierre-Jules	Pierre-Jules	k1gMnSc1	Pierre-Jules
Janssen	Janssen	k2eAgMnSc1d1	Janssen
a	a	k8xC	a
angličan	angličan	k1gMnSc1	angličan
Joseph	Joseph	k1gMnSc1	Joseph
Norman	Norman	k1gMnSc1	Norman
Lockyer	Lockyer	k1gMnSc1	Lockyer
<g/>
,	,	kIx,	,
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	se	k3xPyFc3	se
ve	v	k7c6	v
slunečním	sluneční	k2eAgNnSc6d1	sluneční
spektru	spektrum	k1gNnSc6	spektrum
na	na	k7c6	na
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
587,49	[number]	k4	587,49
nm	nm	k?	nm
žlutou	žlutý	k2eAgFnSc4d1	žlutá
spektrální	spektrální	k2eAgFnSc4d1	spektrální
čáru	čára	k1gFnSc4	čára
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nepatřila	patřit	k5eNaImAgFnS	patřit
žádnému	žádný	k1gMnSc3	žádný
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
známému	známý	k2eAgInSc3d1	známý
prvku	prvek	k1gInSc3	prvek
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
blízkosti	blízkost	k1gFnSc3	blízkost
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
sodíku	sodík	k1gInSc2	sodík
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xS	jako
spekrální	spekrální	k2eAgFnSc1d1	spekrální
čára	čára	k1gFnSc1	čára
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Lockyer	Lockyer	k1gMnSc1	Lockyer
postuloval	postulovat	k5eAaImAgMnS	postulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
podle	podle	k7c2	podle
starořeckého	starořecký	k2eAgMnSc2d1	starořecký
boha	bůh	k1gMnSc2	bůh
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
Helios	Helios	k1gInSc1	Helios
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
nazval	nazvat	k5eAaPmAgMnS	nazvat
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
italský	italský	k2eAgMnSc1d1	italský
fyzik	fyzik	k1gMnSc1	fyzik
Luigi	Luig	k1gFnSc2	Luig
Palmieri	Palmier	k1gFnSc2	Palmier
spektrální	spektrální	k2eAgFnSc4d1	spektrální
čáru	čára	k1gFnSc4	čára
D3	D3	k1gFnSc2	D3
v	v	k7c6	v
plynu	plyn	k1gInSc6	plyn
unikajícím	unikající	k2eAgInSc6d1	unikající
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc3	zahřívání
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
sublimace	sublimace	k1gFnSc2	sublimace
z	z	k7c2	z
Vesuvu	Vesuv	k1gInSc2	Vesuv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1888	[number]	k4	1888
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
pracoval	pracovat	k5eAaImAgMnS	pracovat
americký	americký	k2eAgMnSc1d1	americký
mineralog	mineralog	k1gMnSc1	mineralog
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
William	William	k1gInSc4	William
F.	F.	kA	F.
<g/>
Hillebrand	Hillebrand	k1gInSc1	Hillebrand
s	s	k7c7	s
minerálem	minerál	k1gInSc7	minerál
uraninitem	uraninit	k1gInSc7	uraninit
a	a	k8xC	a
podrobil	podrobit	k5eAaPmAgMnS	podrobit
jej	on	k3xPp3gNnSc4	on
zahřívání	zahřívání	k1gNnSc4	zahřívání
s	s	k7c7	s
minerálními	minerální	k2eAgFnPc7d1	minerální
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
plyn	plyn	k1gInSc1	plyn
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k2eAgMnSc1d1	skotský
chemik	chemik	k1gMnSc1	chemik
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
opakoval	opakovat	k5eAaImAgMnS	opakovat
pokus	pokus	k1gInSc4	pokus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
minerálem	minerál	k1gInSc7	minerál
cleveitem	cleveit	k1gInSc7	cleveit
(	(	kIx(	(
<g/>
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
uranových	uranový	k2eAgFnPc2d1	uranová
rud	ruda	k1gFnPc2	ruda
<g/>
)	)	kIx)	)
a	a	k8xC	a
kromě	kromě	k7c2	kromě
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
argonu	argon	k1gInSc2	argon
isoloval	isolovat	k5eAaBmAgInS	isolovat
také	také	k9	také
plyn	plyn	k1gInSc1	plyn
s	s	k7c7	s
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
spektrální	spektrální	k2eAgFnSc7d1	spektrální
linií	linie	k1gFnSc7	linie
a	a	k8xC	a
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
mezitím	mezitím	k6eAd1	mezitím
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
minerálem	minerál	k1gInSc7	minerál
cleveitem	cleveit	k1gInSc7	cleveit
také	také	k9	také
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
Per	pero	k1gNnPc2	pero
Theodor	Theodor	k1gMnSc1	Theodor
Cleve	Cleev	k1gFnSc2	Cleev
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
Nils	Nilsa	k1gFnPc2	Nilsa
Abraham	Abraham	k1gMnSc1	Abraham
Langlet	Langlet	k1gInSc1	Langlet
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
helium	helium	k1gNnSc4	helium
v	v	k7c6	v
čistější	čistý	k2eAgFnSc6d2	čistší
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
stačilo	stačit	k5eAaBmAgNnS	stačit
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
atomové	atomový	k2eAgFnSc2d1	atomová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Ramsey	Ramsey	k1gInPc4	Ramsey
<g/>
,	,	kIx,	,
Cleve	Cleev	k1gFnPc4	Cleev
a	a	k8xC	a
Langlet	Langlet	k1gInSc4	Langlet
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
objevitele	objevitel	k1gMnSc4	objevitel
helia	helium	k1gNnSc2	helium
v	v	k7c6	v
pozemském	pozemský	k2eAgInSc6d1	pozemský
materiálu	materiál	k1gInSc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vrtných	vrtný	k2eAgFnPc2d1	vrtná
prací	práce	k1gFnPc2	práce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Dexteru	Dexter	k1gInSc6	Dexter
<g/>
,	,	kIx,	,
Kansas	Kansas	k1gInSc1	Kansas
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
zdroj	zdroj	k1gInSc1	zdroj
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
12	[number]	k4	12
objemových	objemový	k2eAgNnPc2d1	objemové
procent	procento	k1gNnPc2	procento
neznámého	známý	k2eNgInSc2d1	neznámý
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Američtí	americký	k2eAgMnPc1d1	americký
chemici	chemik	k1gMnPc1	chemik
Hamilton	Hamilton	k1gInSc1	Hamilton
Cady	Cada	k1gFnPc1	Cada
a	a	k8xC	a
David	David	k1gMnSc1	David
McFarland	McFarlanda	k1gFnPc2	McFarlanda
z	z	k7c2	z
Kansaské	kansaský	k2eAgFnSc2d1	Kansaská
univerzity	univerzita	k1gFnSc2	univerzita
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
velké	velký	k2eAgFnPc1d1	velká
zásoby	zásoba	k1gFnPc1	zásoba
helia	helium	k1gNnSc2	helium
v	v	k7c6	v
ložiscích	ložisko	k1gNnPc6	ložisko
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
v	v	k7c4	v
Great	Great	k2eAgInSc4d1	Great
Plains	Plains	k1gInSc4	Plains
a	a	k8xC	a
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
světovým	světový	k2eAgMnSc7d1	světový
dodavatelem	dodavatel	k1gMnSc7	dodavatel
tohoto	tento	k3xDgInSc2	tento
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
a	a	k8xC	a
Thomas	Thomas	k1gMnSc1	Thomas
Royds	Roydsa	k1gFnPc2	Roydsa
demonstrovali	demonstrovat	k5eAaBmAgMnP	demonstrovat
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
alfa	alfa	k1gNnSc2	alfa
jsou	být	k5eAaImIp3nP	být
jádra	jádro	k1gNnPc4	jádro
hélia	hélium	k1gNnSc2	hélium
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechali	nechat	k5eAaPmAgMnP	nechat
částice	částice	k1gFnSc2	částice
proniknout	proniknout	k5eAaPmF	proniknout
tenkou	tenký	k2eAgFnSc4d1	tenká
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
stěnu	stěna	k1gFnSc4	stěna
evakuované	evakuovaný	k2eAgFnSc2d1	evakuovaná
trubice	trubice	k1gFnSc2	trubice
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
výbojem	výboj	k1gInSc7	výboj
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
spektrální	spektrální	k2eAgFnSc4d1	spektrální
čáru	čára	k1gFnSc4	čára
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
poprvé	poprvé	k6eAd1	poprvé
zkapalnil	zkapalnit	k5eAaPmAgMnS	zkapalnit
helium	helium	k1gNnSc4	helium
holandský	holandský	k2eAgMnSc1d1	holandský
fyzik	fyzik	k1gMnSc1	fyzik
Heike	Heik	k1gFnSc2	Heik
Kamerlingh	Kamerlingh	k1gMnSc1	Kamerlingh
Onnes	Onnes	k1gInSc1	Onnes
ochlazením	ochlazení	k1gNnSc7	ochlazení
plynu	plyn	k1gInSc2	plyn
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
méně	málo	k6eAd2	málo
než	než	k8xS	než
jeden	jeden	k4xCgInSc1	jeden
Kelvin	kelvin	k1gInSc1	kelvin
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
K	k	k7c3	k
=	=	kIx~	=
-	-	kIx~	-
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěšně	úspěšně	k6eNd1	úspěšně
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
dalším	další	k2eAgNnSc7d1	další
snižováním	snižování	k1gNnSc7	snižování
teploty	teplota	k1gFnSc2	teplota
také	také	k9	také
o	o	k7c6	o
převedení	převedení	k1gNnSc6	převedení
helia	helium	k1gNnSc2	helium
do	do	k7c2	do
pevného	pevný	k2eAgInSc2d1	pevný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dokázal	dokázat	k5eAaPmAgInS	dokázat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
jeho	jeho	k3xOp3gFnSc4	jeho
žák	žák	k1gMnSc1	žák
Willem	Will	k1gMnSc7	Will
Hendrik	Hendrik	k1gMnSc1	Hendrik
Keesom	Keesom	k1gInSc4	Keesom
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
vyššího	vysoký	k2eAgInSc2d2	vyšší
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
objevil	objevit	k5eAaPmAgMnS	objevit
Petr	Petr	k1gMnSc1	Petr
Leonidovič	Leonidovič	k1gMnSc1	Leonidovič
Kapica	Kapica	k1gMnSc1	Kapica
supratekutost	supratekutost	k1gFnSc1	supratekutost
isotopu	isotop	k1gInSc6	isotop
4	[number]	k4	4
<g/>
He	he	k0	he
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
blízkých	blízký	k2eAgFnPc6d1	blízká
absolutní	absolutní	k2eAgFnSc4d1	absolutní
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
přítomno	přítomno	k1gNnSc1	přítomno
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
atmosféře	atmosféra	k1gFnSc6	atmosféra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
vrstvách	vrstva	k1gFnPc6	vrstva
a	a	k8xC	a
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
mimořádně	mimořádně	k6eAd1	mimořádně
nízké	nízký	k2eAgFnSc3d1	nízká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
vyprchává	vyprchávat	k5eAaImIp3nS	vyprchávat
do	do	k7c2	do
meziplanetárního	meziplanetární	k2eAgInSc2d1	meziplanetární
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
200	[number]	k4	200
km	km	kA	km
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
0,000524	[number]	k4	0,000524
objemových	objemový	k2eAgNnPc2d1	objemové
procent	procento	k1gNnPc2	procento
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
5,24	[number]	k4	5,24
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
helium	helium	k1gNnSc1	helium
izolováno	izolovat	k5eAaBmNgNnS	izolovat
z	z	k7c2	z
minerálu	minerál	k1gInSc2	minerál
smolince	smolinec	k1gInSc2	smolinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
až	až	k9	až
9	[number]	k4	9
%	%	kIx~	%
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
zemním	zemní	k2eAgInSc6d1	zemní
plynu	plyn	k1gInSc6	plyn
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
také	také	k9	také
získává	získávat	k5eAaImIp3nS	získávat
vymrazováním	vymrazování	k1gNnSc7	vymrazování
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
helium	helium	k1gNnSc4	helium
i	i	k8xC	i
trhlinami	trhlina	k1gFnPc7	trhlina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnPc4d3	nejznámější
oblasti	oblast	k1gFnPc4	oblast
těchto	tento	k3xDgInPc2	tento
vývěrů	vývěr	k1gInPc2	vývěr
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
Skalistých	skalistý	k2eAgFnPc6d1	skalistý
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškeré	veškerý	k3xTgNnSc4	veškerý
toto	tento	k3xDgNnSc4	tento
helium	helium	k1gNnSc4	helium
je	být	k5eAaImIp3nS	být
produktem	produkt	k1gInSc7	produkt
jaderného	jaderný	k2eAgInSc2d1	jaderný
rozpadu	rozpad	k1gInSc2	rozpad
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
(	(	kIx(	(
<g/>
částice	částice	k1gFnSc1	částice
alfa	alfa	k1gNnSc1	alfa
jsou	být	k5eAaImIp3nP	být
jádry	jádro	k1gNnPc7	jádro
atomů	atom	k1gInPc2	atom
helia	helium	k1gNnSc2	helium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmírném	vesmírný	k2eAgNnSc6d1	vesmírné
měřítku	měřítko	k1gNnSc6	měřítko
je	být	k5eAaImIp3nS	být
helium	helium	k1gNnSc4	helium
druhým	druhý	k4xOgNnSc7	druhý
nejvíce	hodně	k6eAd3	hodně
zastoupeným	zastoupený	k2eAgInSc7d1	zastoupený
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
především	především	k9	především
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
svítících	svítící	k2eAgFnPc6d1	svítící
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mezistupňů	mezistupeň	k1gInPc2	mezistupeň
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
syntézy	syntéza	k1gFnSc2	syntéza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
teorií	teorie	k1gFnPc2	teorie
základním	základní	k2eAgInSc7d1	základní
energetickým	energetický	k2eAgInSc7d1	energetický
zdrojem	zdroj	k1gInSc7	zdroj
ve	v	k7c6	v
Vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
%	%	kIx~	%
hmoty	hmota	k1gFnSc2	hmota
okolního	okolní	k2eAgInSc2d1	okolní
pozorovatelného	pozorovatelný	k2eAgInSc2d1	pozorovatelný
Vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
objemová	objemový	k2eAgFnSc1d1	objemová
koncentrace	koncentrace	k1gFnSc1	koncentrace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
3	[number]	k4	3
a	a	k8xC	a
19	[number]	k4	19
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
vodíku	vodík	k1gInSc6	vodík
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
prvek	prvek	k1gInSc1	prvek
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kamenných	kamenný	k2eAgFnPc2d1	kamenná
planet	planeta	k1gFnPc2	planeta
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
na	na	k7c6	na
Merkuru	Merkur	k1gInSc6	Merkur
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
získává	získávat	k5eAaImIp3nS	získávat
helium	helium	k1gNnSc4	helium
z	z	k7c2	z
ložisek	ložisko	k1gNnPc2	ložisko
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
methanu	methan	k1gInSc2	methan
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
plynů	plyn	k1gInPc2	plyn
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
frakční	frakční	k2eAgFnSc7d1	frakční
destilací	destilace	k1gFnSc7	destilace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
možnost	možnost	k1gFnSc1	možnost
je	být	k5eAaImIp3nS	být
zahřívat	zahřívat	k5eAaImF	zahřívat
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
helium	helium	k1gNnSc1	helium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
teplotou	teplota	k1gFnSc7	teplota
okolo	okolo	k7c2	okolo
1	[number]	k4	1
200	[number]	k4	200
°	°	k?	°
<g/>
C.	C.	kA	C.
K	k	k7c3	k
takovým	takový	k3xDgInPc3	takový
minerálům	minerál	k1gInPc3	minerál
patří	patřit	k5eAaImIp3nS	patřit
cleveit	cleveit	k1gInSc1	cleveit
<g/>
,	,	kIx,	,
monazit	monazit	k1gInSc1	monazit
a	a	k8xC	a
thorianit	thorianit	k1gInSc1	thorianit
<g/>
.	.	kIx.	.
</s>
<s>
Plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
získat	získat	k5eAaPmF	získat
čisté	čistý	k2eAgNnSc4d1	čisté
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
extrémně	extrémně	k6eAd1	extrémně
nízké	nízký	k2eAgFnSc3d1	nízká
hustotě	hustota	k1gFnSc3	hustota
a	a	k8xC	a
inertnímu	inertní	k2eAgNnSc3d1	inertní
chování	chování	k1gNnSc3	chování
se	se	k3xPyFc4	se
helium	helium	k1gNnSc1	helium
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
balónů	balón	k1gInPc2	balón
(	(	kIx(	(
<g/>
balónek	balónek	k1gInSc1	balónek
díky	díky	k7c3	díky
heliu	helium	k1gNnSc3	helium
vyletí	vyletět	k5eAaPmIp3nS	vyletět
ke	k	k7c3	k
stropu	strop	k1gInSc3	strop
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
hořlavého	hořlavý	k2eAgInSc2d1	hořlavý
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc7d1	značná
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
jeho	jeho	k3xOp3gFnSc1	jeho
poměrně	poměrně	k6eAd1	poměrně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
má	mít	k5eAaImIp3nS	mít
atom	atom	k1gInSc4	atom
helia	helium	k1gNnSc2	helium
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
průměr	průměr	k1gInSc1	průměr
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
difunduje	difundovat	k5eAaImIp3nS	difundovat
skrze	skrze	k?	skrze
pevné	pevný	k2eAgInPc4d1	pevný
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
ke	k	k7c3	k
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Směsí	směs	k1gFnPc2	směs
helia	helium	k1gNnSc2	helium
<g/>
,	,	kIx,	,
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
dusíku	dusík	k1gInSc2	dusík
se	se	k3xPyFc4	se
plní	plnit	k5eAaImIp3nP	plnit
tlakové	tlakový	k2eAgFnPc1d1	tlaková
láhve	láhev	k1gFnPc1	láhev
s	s	k7c7	s
dýchací	dýchací	k2eAgFnSc7d1	dýchací
směsí	směs	k1gFnSc7	směs
<g/>
,	,	kIx,	,
určenou	určený	k2eAgFnSc7d1	určená
pro	pro	k7c4	pro
potápění	potápění	k1gNnSc4	potápění
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dusíku	dusík	k1gInSc2	dusík
totiž	totiž	k9	totiž
ani	ani	k8xC	ani
pod	pod	k7c7	pod
velkým	velký	k2eAgInSc7d1	velký
tlakem	tlak	k1gInSc7	tlak
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
tzv.	tzv.	kA	tzv.
hloubkové	hloubkový	k2eAgNnSc1d1	hloubkové
opojení	opojení	k1gNnSc1	opojení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
potápěč	potápěč	k1gMnSc1	potápěč
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
hloubkách	hloubka	k1gFnPc6	hloubka
i	i	k9	i
přes	přes	k7c4	přes
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
omezuje	omezovat	k5eAaImIp3nS	omezovat
vznik	vznik	k1gInSc1	vznik
otravy	otrava	k1gFnSc2	otrava
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
současně	současně	k6eAd1	současně
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
riziko	riziko	k1gNnSc4	riziko
kesonové	kesonový	k2eAgFnSc2d1	kesonová
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
rychlém	rychlý	k2eAgInSc6d1	rychlý
výstupu	výstup	k1gInSc6	výstup
potápěče	potápěč	k1gInSc2	potápěč
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
uvolněním	uvolnění	k1gNnSc7	uvolnění
bublinek	bublinka	k1gFnPc2	bublinka
plynného	plynný	k2eAgInSc2d1	plynný
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
mechanického	mechanický	k2eAgNnSc2d1	mechanické
poškození	poškození	k1gNnSc2	poškození
různých	různý	k2eAgFnPc2d1	různá
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
zvuku	zvuk	k1gInSc2	zvuk
v	v	k7c6	v
heliu	helium	k1gNnSc6	helium
je	být	k5eAaImIp3nS	být
řádově	řádově	k6eAd1	řádově
3	[number]	k4	3
<g/>
×	×	k?	×
větší	veliký	k2eAgNnPc4d2	veliký
než	než	k8xS	než
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
nadechne	nadechnout	k5eAaPmIp3nS	nadechnout
helium	helium	k1gNnSc4	helium
<g/>
,	,	kIx,	,
rezonanční	rezonanční	k2eAgFnSc1d1	rezonanční
frekvence	frekvence	k1gFnSc1	frekvence
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
zabarvení	zabarvení	k1gNnSc4	zabarvení
hlasu	hlas	k1gInSc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
přítomnost	přítomnost	k1gFnSc1	přítomnost
hélia	hélium	k1gNnSc2	hélium
projevila	projevit	k5eAaPmAgFnS	projevit
při	při	k7c6	při
hvízdání	hvízdání	k1gNnSc6	hvízdání
nebo	nebo	k8xC	nebo
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
dechový	dechový	k2eAgInSc4d1	dechový
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
nosný	nosný	k2eAgInSc4d1	nosný
plyn	plyn	k1gInSc4	plyn
pro	pro	k7c4	pro
kapilární	kapilární	k2eAgFnSc4d1	kapilární
plynovou	plynový	k2eAgFnSc4d1	plynová
chromatografii	chromatografie	k1gFnSc4	chromatografie
s	s	k7c7	s
hmotově	hmotově	k6eAd1	hmotově
spektrometrickou	spektrometrický	k2eAgFnSc7d1	spektrometrická
detekcí	detekce	k1gFnSc7	detekce
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
aplikací	aplikace	k1gFnSc7	aplikace
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
je	být	k5eAaImIp3nS	být
rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
fluorescence	fluorescence	k1gFnSc1	fluorescence
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
atmosféru	atmosféra	k1gFnSc4	atmosféra
mezi	mezi	k7c7	mezi
zdrojem	zdroj	k1gInSc7	zdroj
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
vzorkem	vzorek	k1gInSc7	vzorek
a	a	k8xC	a
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
tak	tak	k9	tak
pohlcování	pohlcování	k1gNnSc1	pohlcování
fotonů	foton	k1gInPc2	foton
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
argonem	argon	k1gInSc7	argon
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
nízká	nízký	k2eAgFnSc1d1	nízká
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
kapalné	kapalný	k2eAgNnSc4d1	kapalné
helium	helium	k1gNnSc4	helium
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgNnPc2d1	základní
médií	médium	k1gNnPc2	médium
pro	pro	k7c4	pro
kryogenní	kryogenní	k2eAgFnPc4d1	kryogenní
techniky	technika	k1gFnPc4	technika
<g/>
,	,	kIx,	,
především	především	k9	především
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
i	i	k8xC	i
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
supravodivosti	supravodivost	k1gFnSc2	supravodivost
a	a	k8xC	a
supratekutosti	supratekutost	k1gFnSc2	supratekutost
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
se	se	k3xPyFc4	se
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
neonem	neon	k1gInSc7	neon
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
reklamních	reklamní	k2eAgMnPc2d1	reklamní
osvětlovačů	osvětlovač	k1gMnPc2	osvětlovač
<g/>
,	,	kIx,	,
obloukových	obloukový	k2eAgFnPc2d1	oblouková
lamp	lampa	k1gFnPc2	lampa
a	a	k8xC	a
doutnavek	doutnavka	k1gFnPc2	doutnavka
<g/>
.	.	kIx.	.
</s>
<s>
Výboj	výboj	k1gInSc1	výboj
v	v	k7c6	v
heliu	helium	k1gNnSc6	helium
má	mít	k5eAaImIp3nS	mít
intenzivně	intenzivně	k6eAd1	intenzivně
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
viskozita	viskozita	k1gFnSc1	viskozita
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
hélia	hélium	k1gNnSc2	hélium
ve	v	k7c6	v
Stirlingově	Stirlingově	k1gFnSc6	Stirlingově
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
má	mít	k5eAaImIp3nS	mít
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
hodinářském	hodinářský	k2eAgInSc6d1	hodinářský
průmyslu	průmysl	k1gInSc6	průmysl
v	v	k7c6	v
hodinkách	hodinka	k1gFnPc6	hodinka
pro	pro	k7c4	pro
hlubinné	hlubinný	k2eAgInPc4d1	hlubinný
ponory	ponor	k1gInPc4	ponor
<g/>
,	,	kIx,	,
vyrovnávajících	vyrovnávající	k2eAgMnPc2d1	vyrovnávající
tlak	tlak	k1gInSc4	tlak
pomocí	pomocí	k7c2	pomocí
heliového	heliový	k2eAgInSc2d1	heliový
ventilu	ventil	k1gInSc2	ventil
<g/>
.	.	kIx.	.
</s>
<s>
He	he	k0	he
<g/>
@	@	kIx~	@
<g/>
C	C	kA	C
<g/>
60	[number]	k4	60
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
doposud	doposud	k6eAd1	doposud
známých	známý	k2eAgFnPc2d1	známá
sloučenin	sloučenina	k1gFnPc2	sloučenina
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Sférická	sférický	k2eAgFnSc1d1	sférická
koule	koule	k1gFnSc1	koule
je	být	k5eAaImIp3nS	být
fulleren	fulleren	k1gInSc4	fulleren
a	a	k8xC	a
uvnitř	uvnitř	k6eAd1	uvnitř
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
tohoto	tento	k3xDgInSc2	tento
fullerenu	fulleren	k1gInSc2	fulleren
je	být	k5eAaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
jeden	jeden	k4xCgInSc1	jeden
atom	atom	k1gInSc1	atom
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Fullereny	fulleren	k1gInPc1	fulleren
vznikají	vznikat	k5eAaImIp3nP	vznikat
kondenzací	kondenzace	k1gFnSc7	kondenzace
grafitových	grafitový	k2eAgFnPc2d1	grafitová
par	para	k1gFnPc2	para
v	v	k7c6	v
heliu	helium	k1gNnSc6	helium
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kondenzaci	kondenzace	k1gFnSc6	kondenzace
par	para	k1gFnPc2	para
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
radikálovému	radikálový	k2eAgInSc3d1	radikálový
mechanismu	mechanismus	k1gInSc3	mechanismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
grafitu	grafit	k1gInSc2	grafit
nelze	lze	k6eNd1	lze
normální	normální	k2eAgFnSc7d1	normální
cestou	cesta	k1gFnSc7	cesta
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uhlík	uhlík	k1gInSc1	uhlík
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc3	tání
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3500	[number]	k4	3500
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
vyšší	vysoký	k2eAgFnSc4d2	vyšší
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
4800	[number]	k4	4800
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
par	para	k1gFnPc2	para
grafitu	grafit	k1gInSc2	grafit
využívá	využívat	k5eAaImIp3nS	využívat
laseru	laser	k1gInSc3	laser
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
heliem	helium	k1gNnSc7	helium
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgInSc1d1	spojen
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
úkaz	úkaz	k1gInSc1	úkaz
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
supratekutost	supratekutost	k1gFnSc4	supratekutost
<g/>
.	.	kIx.	.
</s>
<s>
Kapalné	kapalný	k2eAgNnSc1d1	kapalné
helium	helium	k1gNnSc1	helium
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
formách	forma	k1gFnPc6	forma
-	-	kIx~	-
helium	helium	k1gNnSc4	helium
I	i	k9	i
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
2,176	[number]	k4	2,176
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
4,21	[number]	k4	4,21
K	k	k7c3	k
a	a	k8xC	a
helium	helium	k1gNnSc4	helium
II	II	kA	II
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
nižších	nízký	k2eAgMnPc2d2	nižší
než	než	k8xS	než
2,176	[number]	k4	2,176
<g/>
8	[number]	k4	8
K	K	kA	K
(	(	kIx(	(
<g/>
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
lambda	lambda	k1gNnSc1	lambda
bod	bod	k1gInSc1	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
izotopu	izotop	k1gInSc2	izotop
4	[number]	k4	4
<g/>
He	he	k0	he
<g/>
.	.	kIx.	.
</s>
<s>
Izotop	izotop	k1gInSc1	izotop
3	[number]	k4	3
<g/>
He	he	k0	he
je	být	k5eAaImIp3nS	být
supratekutý	supratekutý	k2eAgInSc1d1	supratekutý
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c7	pod
přibližně	přibližně	k6eAd1	přibližně
0,002	[number]	k4	0,002
5	[number]	k4	5
K.	K.	kA	K.
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
izotopy	izotop	k1gInPc7	izotop
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
tím	ten	k3xDgInSc7	ten
že	že	k9	že
atom	atom	k1gInSc1	atom
4	[number]	k4	4
<g/>
He	he	k0	he
je	být	k5eAaImIp3nS	být
bosonem	boson	k1gInSc7	boson
(	(	kIx(	(
<g/>
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
2	[number]	k4	2
protonů	proton	k1gInPc2	proton
<g/>
,	,	kIx,	,
2	[number]	k4	2
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
2	[number]	k4	2
elektronů	elektron	k1gInPc2	elektron
<g/>
)	)	kIx)	)
zatímco	zatímco	k8xS	zatímco
atom	atom	k1gInSc4	atom
3	[number]	k4	3
<g/>
He	he	k0	he
je	být	k5eAaImIp3nS	být
fermionem	fermion	k1gInSc7	fermion
<g/>
.	.	kIx.	.
</s>
<s>
Izotop	izotop	k1gInSc1	izotop
4	[number]	k4	4
<g/>
He	he	k0	he
je	být	k5eAaImIp3nS	být
supratekutý	supratekutý	k2eAgInSc1d1	supratekutý
díky	díky	k7c3	díky
vzniku	vznik	k1gInSc3	vznik
Bose-Einsteinova	Bose-Einsteinův	k2eAgInSc2d1	Bose-Einsteinův
kondenzátu	kondenzát	k1gInSc2	kondenzát
zatímco	zatímco	k8xS	zatímco
3	[number]	k4	3
<g/>
He	he	k0	he
díky	díky	k7c3	díky
vzniku	vznik	k1gInSc3	vznik
Cooperových	Cooperův	k2eAgInPc2d1	Cooperův
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
heliem	helium	k1gNnSc7	helium
I	i	k9	i
a	a	k8xC	a
heliem	helium	k1gNnSc7	helium
II	II	kA	II
neexistuje	existovat	k5eNaImIp3nS	existovat
skupenské	skupenský	k2eAgNnSc4d1	skupenské
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
formy	forma	k1gFnPc1	forma
helia	helium	k1gNnSc2	helium
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
nádobě	nádoba	k1gFnSc6	nádoba
současně	současně	k6eAd1	současně
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
lambda	lambda	k1gNnSc7	lambda
teplotou	teplota	k1gFnSc7	teplota
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
pouze	pouze	k6eAd1	pouze
helium	helium	k1gNnSc4	helium
I	i	k8xC	i
a	a	k8xC	a
pod	pod	k7c7	pod
lambda	lambda	k1gNnSc7	lambda
teplotou	teplota	k1gFnSc7	teplota
pouze	pouze	k6eAd1	pouze
helium	helium	k1gNnSc1	helium
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
helium	helium	k1gNnSc1	helium
I	I	kA	I
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
běžné	běžný	k2eAgFnPc4d1	běžná
tekutiny	tekutina	k1gFnPc4	tekutina
<g/>
,	,	kIx,	,
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
helium	helium	k1gNnSc4	helium
II	II	kA	II
velmi	velmi	k6eAd1	velmi
neobvyklé	obvyklý	k2eNgFnPc4d1	neobvyklá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
nemá	mít	k5eNaImIp3nS	mít
tato	tento	k3xDgFnSc1	tento
kapalina	kapalina	k1gFnSc1	kapalina
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgNnSc4	žádný
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
tření	tření	k1gNnSc4	tření
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
teče	téct	k5eAaImIp3nS	téct
nesmírně	smírně	k6eNd1	smírně
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
díky	díky	k7c3	díky
kapilárnímu	kapilární	k2eAgInSc3d1	kapilární
jevu	jev	k1gInSc3	jev
přetéká	přetékat	k5eAaImIp3nS	přetékat
stěny	stěn	k1gInPc7	stěn
nádob	nádoba	k1gFnPc2	nádoba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
je	být	k5eAaImIp3nS	být
uchováno	uchován	k2eAgNnSc1d1	uchováno
a	a	k8xC	a
vytéká	vytékat	k5eAaImIp3nS	vytékat
horním	horní	k2eAgInSc7d1	horní
koncem	konec	k1gInSc7	konec
do	do	k7c2	do
něj	on	k3xPp3gMnSc4	on
ponořené	ponořený	k2eAgFnPc1d1	ponořená
kapiláry	kapilára	k1gFnPc1	kapilára
(	(	kIx(	(
<g/>
jev	jev	k1gInSc1	jev
zvaný	zvaný	k2eAgInSc1d1	zvaný
fontánový	fontánový	k2eAgInSc1d1	fontánový
efekt	efekt	k1gInSc1	efekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
budí	budit	k5eAaImIp3nS	budit
zdání	zdání	k1gNnSc1	zdání
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
helium	helium	k1gNnSc1	helium
II	II	kA	II
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
má	mít	k5eAaImIp3nS	mít
supratekuté	supratekutý	k2eAgNnSc4d1	supratekuté
helium	helium	k1gNnSc4	helium
největší	veliký	k2eAgFnSc4d3	veliký
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
vodivost	vodivost	k1gFnSc4	vodivost
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
doposud	doposud	k6eAd1	doposud
známých	známý	k2eAgFnPc2d1	známá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
ANDRONIKAŠVILI	ANDRONIKAŠVILI	kA	ANDRONIKAŠVILI
<g/>
,	,	kIx,	,
E.	E.	kA	E.
L.	L.	kA	L.
Vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
na	na	k7c4	na
kapalné	kapalný	k2eAgNnSc4d1	kapalné
hélium	hélium	k1gNnSc4	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
Fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
Supravodivost	supravodivost	k1gFnSc1	supravodivost
Supratekutost	supratekutost	k1gFnSc1	supratekutost
Inertní	inertní	k2eAgFnSc1d1	inertní
plyn	plyn	k1gInSc4	plyn
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
helium	helium	k1gNnSc4	helium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hélium	hélium	k1gNnSc4	hélium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaImAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Helium	helium	k1gNnSc1	helium
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Helium	helium	k1gNnSc1	helium
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
plynů	plyn	k1gInPc2	plyn
na	na	k7c4	na
encyclopedia	encyclopedium	k1gNnPc4	encyclopedium
<g/>
.	.	kIx.	.
<g/>
airliquide	airliquid	k1gInSc5	airliquid
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
com	com	k?	com
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
publikace	publikace	k1gFnSc1	publikace
o	o	k7c6	o
heliu	helium	k1gNnSc6	helium
na	na	k7c6	na
webu	web	k1gInSc6	web
České	český	k2eAgFnSc2d1	Česká
asociace	asociace	k1gFnSc2	asociace
technických	technický	k2eAgInPc2d1	technický
plynů	plyn	k1gInPc2	plyn
catp	catp	k1gMnSc1	catp
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
Alchymie	alchymie	k1gFnSc1	alchymie
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
vědci	vědec	k1gMnPc1	vědec
přinutili	přinutit	k5eAaPmAgMnP	přinutit
helium	helium	k1gNnSc4	helium
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
chovalo	chovat	k5eAaImAgNnS	chovat
jako	jako	k9	jako
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
na	na	k7c4	na
technet	technet	k1gInSc4	technet
<g/>
.	.	kIx.	.
<g/>
idnes	idnes	k1gInSc4	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Na	na	k7c6	na
Jupiteru	Jupiter	k1gInSc6	Jupiter
prší	pršet	k5eAaImIp3nS	pršet
hélium	hélium	k1gNnSc1	hélium
<g/>
,	,	kIx,	,
článek	článek	k1gInSc1	článek
na	na	k7c4	na
astro	astra	k1gFnSc5	astra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Government	Government	k1gMnSc1	Government
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bureau	Bureaum	k1gNnSc3	Bureaum
of	of	k?	of
Land	Land	k1gInSc1	Land
Management	management	k1gInSc1	management
<g/>
:	:	kIx,	:
Sources	Sources	k1gMnSc1	Sources
<g/>
,	,	kIx,	,
Refinement	Refinement	k1gMnSc1	Refinement
<g/>
,	,	kIx,	,
and	and	k?	and
Shortage	Shortage	k1gInSc1	Shortage
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Geological	Geological	k1gMnSc2	Geological
Survey	Survea	k1gMnSc2	Survea
publications	publications	k6eAd1	publications
on	on	k3xPp3gMnSc1	on
helium	helium	k1gNnSc4	helium
Helium	helium	k1gNnSc1	helium
Where	Wher	k1gInSc5	Wher
is	is	k?	is
all	all	k?	all
the	the	k?	the
helium	helium	k1gNnSc4	helium
<g/>
?	?	kIx.	?
</s>
<s>
*	*	kIx~	*
<g/>
It	It	k1gMnPc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Elemental	Elemental	k1gMnSc1	Elemental
-	-	kIx~	-
Helium	helium	k1gNnSc1	helium
Chemistry	Chemistr	k1gMnPc4	Chemistr
in	in	k?	in
its	its	k?	its
element	element	k1gInSc1	element
podcast	podcast	k1gFnSc1	podcast
(	(	kIx(	(
<g/>
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
from	from	k6eAd1	from
the	the	k?	the
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
of	of	k?	of
Chemistry	Chemistr	k1gMnPc7	Chemistr
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chemistry	Chemistr	k1gMnPc7	Chemistr
World	World	k1gInSc1	World
<g/>
:	:	kIx,	:
Helium	helium	k1gNnSc1	helium
</s>
