<s>
Angína	angína	k1gFnSc1	angína
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc4d1	běžné
onemocnění	onemocnění	k1gNnSc4	onemocnění
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
léčitelná	léčitelný	k2eAgFnSc1d1	léčitelná
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>

