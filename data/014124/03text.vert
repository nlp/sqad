<s>
Adam	Adam	k1gMnSc1
Václavík	Václavík	k1gMnSc1
</s>
<s>
Adam	Adam	k1gMnSc1
Václavík	Václavík	k1gMnSc1
Datum	datum	k1gInSc4
narození	narození	k1gNnSc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1994	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Sportovní	sportovní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Sport	sport	k1gInSc1
</s>
<s>
biatlon	biatlon	k1gInSc1
Klub	klub	k1gInSc1
</s>
<s>
KB	kb	kA
Jilemnice	Jilemnice	k1gFnSc1
<g/>
,	,	kIx,
SKP	SKP	kA
Kornspitz	Kornspitz	k1gInSc1
Lyže	lyže	k1gFnSc1
</s>
<s>
Fischer	Fischer	k1gMnSc1
Zbraň	zbraň	k1gFnSc1
</s>
<s>
Anschütz	Anschütz	k1gInSc1
Světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
Debut	debut	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
Nejlepší	dobrý	k2eAgInSc4d3
umístění	umístění	k1gNnSc1
</s>
<s>
68	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
výher	výhra	k1gFnPc2
</s>
<s>
0	#num#	k4
Stupně	stupeň	k1gInSc2
vítězů	vítěz	k1gMnPc2
</s>
<s>
0	#num#	k4
Medailový	medailový	k2eAgInSc1d1
zisk	zisk	k1gInSc1
Olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
0	#num#	k4
–	–	k?
0	#num#	k4
–	–	k?
0	#num#	k4
Mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
</s>
<s>
0	#num#	k4
–	–	k?
0	#num#	k4
–	–	k?
0	#num#	k4
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktualizovány	aktualizován	k2eAgInPc4d1
dne	den	k1gInSc2
20180421	#num#	k4
<g/>
a	a	k8xC
<g/>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
2020	#num#	k4
Raubiči	Raubič	k1gMnPc1
</s>
<s>
supersprint	supersprint	k1gMnSc1
</s>
<s>
Adam	Adam	k1gMnSc1
Václavík	Václavík	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1994	#num#	k4
<g/>
,	,	kIx,
Jilemnice	Jilemnice	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
v	v	k7c6
biatlonu	biatlon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reprezentuje	reprezentovat	k5eAaImIp3nS
klub	klub	k1gInSc1
SKP	SKP	kA
Kornspitz	Kornspitz	k1gInSc1
Jablonec	Jablonec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
obsadil	obsadit	k5eAaPmAgMnS
individuálně	individuálně	k6eAd1
nejlépe	dobře	k6eAd3
17	#num#	k4
<g/>
.	.	kIx.
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
největším	veliký	k2eAgInSc7d3
úspěchem	úspěch	k1gInSc7
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
stříbro	stříbro	k1gNnSc1
ze	z	k7c2
supersprintu	supersprint	k1gInSc2
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
biatlonu	biatlon	k1gInSc6
2017	#num#	k4
v	v	k7c4
Hochfilzenu	Hochfilzen	k2eAgFnSc4d1
a	a	k8xC
Zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
2018	#num#	k4
v	v	k7c6
Pchjongčchangu	Pchjongčchang	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
Olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
a	a	k8xC
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Akce	akce	k1gFnSc1
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
MS	MS	kA
Hochfilzen	Hochfilzna	k1gFnPc2
<g/>
34.54	34.54	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
22	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
ZOH	ZOH	kA
Pchjongčchang	Pchjongčchang	k1gInSc4
<g/>
73	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
23	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
MS	MS	kA
Antholz-Anterselva	Antholz-Anterselva	k1gFnSc1
<g/>
64	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
25	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
Výsledky	výsledek	k1gInPc1
z	z	k7c2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
se	se	k3xPyFc4
započítávají	započítávat	k5eAaImIp3nP
do	do	k7c2
celkového	celkový	k2eAgNnSc2d1
hodnocení	hodnocení	k1gNnSc2
světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
,	,	kIx,
výsledky	výsledek	k1gInPc4
z	z	k7c2
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
se	se	k3xPyFc4
dříve	dříve	k6eAd2
započítávaly	započítávat	k5eAaImAgFnP
<g/>
,	,	kIx,
od	od	k7c2
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Soči	Soči	k1gNnPc6
2014	#num#	k4
se	se	k3xPyFc4
nezapočítávají	započítávat	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
<g/>
[	[	kIx(
<g/>
p.	p.	k?
1	#num#	k4
<g/>
]	]	kIx)
<g/>
75	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
<g/>
–	–	k?
<g/>
41	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
80	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anterselva	Anterselva	k1gFnSc1
<g/>
55.49	55.49	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
73	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canmorenestartoval	Canmorenestartoval	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Presque	Presque	k1gFnSc1
Islenestartoval	Islenestartoval	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chanty-Mansijsk	Chanty-Mansijsk	k1gInSc1
<g/>
72	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
50	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
Celkové	celkový	k2eAgNnSc4d1
umístěnínkl	umístěnínknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
nkl	nkl	k?
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
nkl	nkl	k?
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
71	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
0	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
nkl	nkl	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersund	Östersund	k1gInSc1
<g/>
55.42	55.42	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
95	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokljuka	Pokljuk	k1gMnSc2
<g/>
50.50	50.50	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
–	–	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
<g/>
17.33	17.33	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
<g/>
39.40	39.40	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anterselva	Anterselva	k1gFnSc1
<g/>
–	–	k?
<g/>
70.13	70.13	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pchjongčchang	Pchjongčchang	k1gInSc1
<g/>
43.58	43.58	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnSc1
<g/>
[	[	kIx(
<g/>
p.	p.	k?
2	#num#	k4
<g/>
]	]	kIx)
<g/>
64	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollen	Holmenkollen	k1gInSc1
<g/>
56.45	56.45	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
<g/>
54.70	54.70	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
nkl	nkl	k?
<g/>
.7	.7	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
69	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
42	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
68	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersundnestartoval	Östersundnestartoval	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzen	k2eAgInSc4d1
<g/>
59.47	59.47	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annecy	Annec	k2eAgFnPc4d1
<g/>
84	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
<g/>
75	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpolding	Ruhpolding	k1gInSc1
<g/>
–	–	k?
<g/>
71	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anterselva	Anterselva	k1gFnSc1
<g/>
58.34	58.34	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnSc1
<g/>
44	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollen	Holmenkollen	k1gInSc1
<g/>
90	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ťumeňnezúčastnil	Ťumeňnezúčastnil	k1gMnSc1
se	se	k3xPyFc4
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
13.68	13.68	k4
%	%	kIx~
</s>
<s>
7	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
91	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokljuka	Pokljuk	k1gMnSc2
<g/>
69	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
85	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzen	k2eAgInSc4d1
<g/>
86	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nová	k1gFnSc6
Město	město	k1gNnSc1
<g/>
86	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhofnestartoval	Oberhofnestartoval	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpoldingnestartoval	Ruhpoldingnestartoval	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anterselva	Anterselva	k1gFnSc1
<g/>
79	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canmore	Canmor	k1gInSc5
<g/>
–	–	k?
<g/>
77.6	77.6	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soldier	Soldier	k1gMnSc1
Hollownestartoval	Hollownestartoval	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollen	Holmenkollen	k1gInSc1
<g/>
–	–	k?
</s>
<s>
Celkové	celkový	k2eAgNnSc4d1
umístěnínkl	umístěnínknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
nkl	nkl	k?
<g/>
.7	.7	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
66	#num#	k4
%	%	kIx~
</s>
<s>
0	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
nkl	nkl	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersund	Östersund	k1gInSc1
<g/>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
76	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzen	k2eAgInSc4d1
<g/>
94	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annecy	Annec	k2eAgFnPc4d1
<g/>
37.41	37.41	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhofnestartoval	Oberhofnestartoval	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruhpoldingnestartoval	Ruhpoldingnestartoval	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokljuka	Pokljuk	k1gMnSc4
<g/>
–	–	k?
<g/>
77	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
77	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnSc1
<g/>
52.30	52.30	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
zrušeno	zrušit	k5eAaPmNgNnS
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holmenkollenzrušeno	Holmenkollenzrušen	k2eAgNnSc4d1
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1
umístění	umístění	k1gNnSc1
<g/>
63	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
–	–	k?
</s>
<s>
(	(	kIx(
<g/>
nkl	nkl	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
–	–	k?
</s>
<s>
74	#num#	k4
%	%	kIx~
</s>
<s>
30	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
70	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
ČSP	ČSP	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
HZ	Hz	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
SŠT	SŠT	kA
</s>
<s>
SZD	SZD	kA
</s>
<s>
STŘ	STŘ	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnSc1
<g/>
82	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
45	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
73,5	73,5	k4
%	%	kIx~
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontiolahti	Kontiolahti	k1gNnPc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
62	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
80	#num#	k4
%	%	kIx~
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzen	k2eAgInSc4d1
<g/>
79	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
60	#num#	k4
%	%	kIx~
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hochfilzen	Hochfilzna	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
49.52	49.52	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
77	#num#	k4
%	%	kIx~
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhofnestartoval	Oberhofnestartoval	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oberhof	Oberhof	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
nestartoval	startovat	k5eNaBmAgMnS
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antholz-Anterselvanestartoval	Antholz-Anterselvanestartoval	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
80	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
69,5	69,5	k4
%	%	kIx~
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc1d1
Město	město	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
nestartoval	startovat	k5eNaBmAgMnS
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Östersund	Östersund	k1gInSc1
<g/>
80	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
70	#num#	k4
%	%	kIx~
</s>
<s>
Celkové	celkový	k2eAgNnSc4d1
umístěnínkl	umístěnínknout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
nkl	nkl	k?
<g/>
.	.	kIx.
</s>
<s>
–	–	k?
</s>
<s>
nkl	nkl	k?
<g/>
.10	.10	k4
<g/>
.	.	kIx.
</s>
<s>
–	–	k?
</s>
<s>
72	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
0	#num#	k4
bodů	bod	k1gInPc2
(	(	kIx(
<g/>
nkl	nkl	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Juniorská	juniorský	k2eAgNnPc1d1
mistrovství	mistrovství	k1gNnSc1
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
čtyř	čtyři	k4xCgInPc2
juniorských	juniorský	k2eAgInPc2d1
šampionátů	šampionát	k1gInPc2
v	v	k7c6
biatlonu	biatlon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepším	dobrý	k2eAgInSc7d3
individuálním	individuální	k2eAgInSc7d1
výsledkem	výsledek	k1gInSc7
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
dvě	dva	k4xCgFnPc4
7	#num#	k4
<g/>
.	.	kIx.
místa	místo	k1gNnSc2
ze	z	k7c2
šampionátu	šampionát	k1gInSc2
v	v	k7c6
běloruském	běloruský	k2eAgInSc6d1
Minsku	Minsk	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
obsadil	obsadit	k5eAaPmAgMnS
ve	v	k7c6
sprintu	sprint	k1gInSc6
a	a	k8xC
ve	v	k7c6
vytrvalostním	vytrvalostní	k2eAgInSc6d1
závodě	závod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
štafetou	štafeta	k1gFnSc7
obsadil	obsadit	k5eAaPmAgMnS
dvakrát	dvakrát	k6eAd1
šestou	šestý	k4xOgFnSc4
pozici	pozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
D	D	kA
</s>
<s>
Místo	místo	k7c2
</s>
<s>
SP	SP	kA
</s>
<s>
SZ	SZ	kA
</s>
<s>
IZ	IZ	kA
</s>
<s>
ŠT	ŠT	kA
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
MSJ	MSJ	kA
Kontiolahti	Kontiolahť	k1gFnSc2
<g/>
37.35	37.35	k4
<g/>
.48	.48	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
18	#num#	k4
let	léto	k1gNnPc2
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
MSJ	MSJ	kA
Obertilliach	Obertilliach	k1gInSc4
<g/>
29.12.17.6	29.12.17.6	k4
<g/>
.18	.18	k4
let	léto	k1gNnPc2
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
MSJ	MSJ	kA
Presque	Presque	k1gFnSc1
Isle	Isle	k1gFnSc1
<g/>
22.23.17.9	22.23.17.9	k4
<g/>
.20	.20	k4
let	léto	k1gNnPc2
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
MSJ	MSJ	kA
Minsk	Minsk	k1gInSc4
<g/>
7.10.7.6	7.10.7.6	k4
<g/>
.21	.21	k4
let	léto	k1gNnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Původně	původně	k6eAd1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
závod	závod	k1gInSc1
uskutečnit	uskutečnit	k5eAaPmF
v	v	k7c6
Oberhofu	Oberhof	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
sněhu	sníh	k1gInSc2
a	a	k8xC
velmi	velmi	k6eAd1
vysokých	vysoký	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
byl	být	k5eAaImAgInS
přesunut	přesunout	k5eAaPmNgInS
do	do	k7c2
Ruhpoldingu	Ruhpolding	k1gInSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Původně	původně	k6eAd1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
podnik	podnik	k1gInSc1
uskutečnit	uskutečnit	k5eAaPmF
v	v	k7c6
ruské	ruský	k2eAgFnSc6d1
Ťumeni	Ťumen	k2eAgMnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
dopingovém	dopingový	k2eAgInSc6d1
skandálu	skandál	k1gInSc6
ruských	ruský	k2eAgMnPc2d1
sportovců	sportovec	k1gMnPc2
se	se	k3xPyFc4
pořadatelé	pořadatel	k1gMnPc1
sami	sám	k3xTgMnPc1
konání	konání	k1gNnSc2
tohoto	tento	k3xDgInSc2
podniku	podnik	k1gInSc2
vzdali	vzdát	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://services.biathlonresults.com/athletes.aspx?IbuId=BTCZE11802199401,	http://services.biathlonresults.com/athletes.aspx?IbuId=BTCZE11802199401,	k4
přístup	přístup	k1gInSc1
<g/>
:	:	kIx,
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
<g/>
↑	↑	k?
Г	Г	k?
О	О	k?
б	б	k?
н	н	k?
и	и	k?
в	в	k?
з	з	k?
К	К	k?
м	м	k?
<g/>
.	.	kIx.
Т	Т	k?
Б	Б	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Adam	Adam	k1gMnSc1
Václavík	Václavík	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Mezinárodní	mezinárodní	k2eAgFnSc2d1
biatlonové	biatlonový	k2eAgFnSc2d1
unie	unie	k1gFnSc2
</s>
<s>
Profil	profil	k1gInSc1
Adama	Adam	k1gMnSc2
Václavíka	Václavík	k1gMnSc2
na	na	k7c6
webu	web	k1gInSc6
Českého	český	k2eAgInSc2d1
svazu	svaz	k1gInSc2
biatlonu	biatlon	k1gInSc2
</s>
<s>
Adam	Adam	k1gMnSc1
Václavík	Václavík	k1gMnSc1
na	na	k7c6
Twitteru	Twitter	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
