<s>
Homeostáze	Homeostáze	k1gFnSc1
nebo	nebo	k8xC
také	také	k9
homeostáza	homeostáza	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
řec.	řec.	k?
homoios	homoios	k1gInSc1
<g/>
,	,	kIx,
stejný	stejný	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
stasis	stasis	k1gFnSc1
<g/>
,	,	kIx,
trvání	trvání	k1gNnSc1
<g/>
,	,	kIx,
stání	stání	k1gNnSc1
<g/>
)	)	kIx)
znamená	znamenat	k5eAaImIp3nS
samočinné	samočinný	k2eAgNnSc1d1
udržování	udržování	k1gNnSc1
hodnoty	hodnota	k1gFnSc2
nějaké	nějaký	k3yIgFnPc4
veličiny	veličina	k1gFnPc4
na	na	k7c6
přibližně	přibližně	k6eAd1
stejné	stejný	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
<g/>
.	.	kIx.
</s>