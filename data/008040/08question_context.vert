<s>
U	u	k7c2
živých	živý	k2eAgInPc2d1
organismů	organismus	k1gInPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
schopnost	schopnost	k1gFnSc1
udržovat	udržovat	k5eAaImF
stabilní	stabilní	k2eAgNnSc4d1
vnitřní	vnitřní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1
je	být	k5eAaImIp3nS
nezbytnou	zbytný	k2eNgFnSc7d1
podmínkou	podmínka	k1gFnSc7
jejich	jejich	k3xOp3gNnSc2
fungování	fungování	k1gNnSc2
a	a	k8xC
existence	existence	k1gFnSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
se	se	k3xPyFc4
vnější	vnější	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
mění	měnit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>