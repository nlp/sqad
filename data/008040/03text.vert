<s>
Homeostáze	Homeostáze	k1gFnSc1	Homeostáze
nebo	nebo	k8xC	nebo
také	také	k9	také
homeostáza	homeostáza	k1gFnSc1	homeostáza
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
homoios	homoios	k1gInSc1	homoios
<g/>
,	,	kIx,	,
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
a	a	k8xC	a
stasis	stasis	k1gFnSc1	stasis
<g/>
,	,	kIx,	,
trvání	trvání	k1gNnSc1	trvání
<g/>
,	,	kIx,	,
stání	stání	k1gNnSc1	stání
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
samočinné	samočinný	k2eAgNnSc1d1	samočinné
udržování	udržování	k1gNnSc1	udržování
hodnoty	hodnota	k1gFnSc2	hodnota
nějaké	nějaký	k3yIgFnPc4	nějaký
veličiny	veličina	k1gFnPc4	veličina
na	na	k7c6	na
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgFnSc6d1	stejná
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
schopnost	schopnost	k1gFnSc1	schopnost
udržovat	udržovat	k5eAaImF	udržovat
stabilní	stabilní	k2eAgNnSc4d1	stabilní
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
podmínkou	podmínka	k1gFnSc7	podmínka
jejich	jejich	k3xOp3gNnSc2	jejich
fungování	fungování	k1gNnSc2	fungování
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
vnější	vnější	k2eAgFnPc1d1	vnější
podmínky	podmínka	k1gFnPc1	podmínka
mění	měnit	k5eAaImIp3nP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
organické	organický	k2eAgFnSc2d1	organická
homeostáze	homeostáze	k1gFnSc2	homeostáze
je	být	k5eAaImIp3nS	být
udržování	udržování	k1gNnSc1	udržování
acidobazické	acidobazický	k2eAgFnSc2d1	acidobazická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
nebo	nebo	k8xC	nebo
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
u	u	k7c2	u
homoiotermních	homoiotermní	k2eAgInPc2d1	homoiotermní
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Kyberneticky	kyberneticky	k6eAd1	kyberneticky
se	se	k3xPyFc4	se
homeostáze	homeostáze	k1gFnSc1	homeostáze
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jako	jako	k9	jako
záporná	záporný	k2eAgFnSc1d1	záporná
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
chybového	chybový	k2eAgInSc2d1	chybový
signálu	signál	k1gInSc2	signál
redukuje	redukovat	k5eAaBmIp3nS	redukovat
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
normativní	normativní	k2eAgFnSc2d1	normativní
<g/>
,	,	kIx,	,
správné	správný	k2eAgFnSc2d1	správná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
nebo	nebo	k8xC	nebo
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
takto	takto	k6eAd1	takto
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
homeostat	homeostat	k5eAaImF	homeostat
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
homeostáze	homeostáze	k1gFnSc2	homeostáze
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyziolog	fyziolog	k1gMnSc1	fyziolog
Claude	Claud	k1gInSc5	Claud
Bernard	Bernard	k1gMnSc1	Bernard
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
pojem	pojem	k1gInSc1	pojem
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
amerického	americký	k2eAgMnSc2d1	americký
fyziologa	fyziolog	k1gMnSc2	fyziolog
W.	W.	kA	W.
B.	B.	kA	B.
Cannona	Cannona	k1gFnSc1	Cannona
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Udržování	udržování	k1gNnSc1	udržování
řady	řada	k1gFnSc2	řada
veličin	veličina	k1gFnPc2	veličina
–	–	k?	–
například	například	k6eAd1	například
stupně	stupeň	k1gInPc1	stupeň
pH	ph	kA	ph
tělních	tělní	k2eAgFnPc2d1	tělní
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
,	,	kIx,	,
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
aj.	aj.	kA	aj.
–	–	k?	–
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
úzkých	úzký	k2eAgFnPc6d1	úzká
mezích	mez	k1gFnPc6	mez
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
organismů	organismus	k1gInPc2	organismus
nutná	nutný	k2eAgFnSc1d1	nutná
podmínka	podmínka	k1gFnSc1	podmínka
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
udržují	udržovat	k5eAaImIp3nP	udržovat
živé	živý	k2eAgFnPc1d1	živá
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
tkáně	tkáň	k1gFnPc1	tkáň
i	i	k8xC	i
organismy	organismus	k1gInPc1	organismus
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
stálou	stálý	k2eAgFnSc4d1	stálá
hladinu	hladina	k1gFnSc4	hladina
různých	různý	k2eAgFnPc2d1	různá
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
zbavují	zbavovat	k5eAaImIp3nP	zbavovat
se	se	k3xPyFc4	se
přebytečných	přebytečný	k2eAgFnPc2d1	přebytečná
a	a	k8xC	a
přijímají	přijímat	k5eAaImIp3nP	přijímat
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
nedostává	dostávat	k5eNaImIp3nS	dostávat
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
živočich	živočich	k1gMnSc1	živočich
reguluje	regulovat	k5eAaImIp3nS	regulovat
hladinu	hladina	k1gFnSc4	hladina
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
ledviny	ledvina	k1gFnPc1	ledvina
odvádějí	odvádět	k5eAaImIp3nP	odvádět
přebytečnou	přebytečný	k2eAgFnSc4d1	přebytečná
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Poruchy	porucha	k1gFnPc1	porucha
homeostatických	homeostatický	k2eAgInPc2d1	homeostatický
mechanismů	mechanismus	k1gInPc2	mechanismus
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
řady	řada	k1gFnSc2	řada
nemocí	nemoc	k1gFnPc2	nemoc
(	(	kIx(	(
<g/>
cukrovka	cukrovka	k1gFnSc1	cukrovka
<g/>
,	,	kIx,	,
hypoglykemie	hypoglykemie	k1gFnSc1	hypoglykemie
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organické	organický	k2eAgFnPc1d1	organická
homeostaty	homeostata	k1gFnPc1	homeostata
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
biolog	biolog	k1gMnSc1	biolog
Humberto	Humberta	k1gFnSc5	Humberta
Maturana	Maturan	k1gMnSc2	Maturan
proto	proto	k8xC	proto
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gNnPc4	on
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
homeostáze	homeostáze	k1gFnSc1	homeostáze
<g/>
"	"	kIx"	"
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
skutečnost	skutečnost	k1gFnSc1	skutečnost
příliš	příliš	k6eAd1	příliš
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
pouhé	pouhý	k2eAgNnSc4d1	pouhé
udržování	udržování	k1gNnSc4	udržování
či	či	k8xC	či
trvání	trvání	k1gNnSc4	trvání
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c4	o
dynamické	dynamický	k2eAgNnSc4d1	dynamické
vyvažování	vyvažování	k1gNnSc4	vyvažování
<g/>
;	;	kIx,	;
Maturana	Maturana	k1gFnSc1	Maturana
proto	proto	k8xC	proto
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pojem	pojem	k1gInSc4	pojem
homeodynamiky	homeodynamika	k1gFnSc2	homeodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Technický	technický	k2eAgInSc1d1	technický
homeostat	homeostat	k1gInSc1	homeostat
je	být	k5eAaImIp3nS	být
zpětnovazebný	zpětnovazebný	k2eAgInSc1d1	zpětnovazebný
regulátor	regulátor	k1gInSc1	regulátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
chybový	chybový	k2eAgInSc4d1	chybový
signál	signál	k1gInSc4	signál
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
odchylku	odchylka	k1gFnSc4	odchylka
od	od	k7c2	od
žádoucí	žádoucí	k2eAgFnSc2d1	žádoucí
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
a	a	k8xC	a
přivádí	přivádět	k5eAaImIp3nS	přivádět
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odchylka	odchylka	k1gFnSc1	odchylka
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
Wattův	Wattův	k2eAgInSc1d1	Wattův
regulátor	regulátor	k1gInSc1	regulátor
parního	parní	k2eAgInSc2d1	parní
stroje	stroj	k1gInSc2	stroj
<g/>
:	:	kIx,	:
když	když	k8xS	když
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
otáčky	otáčka	k1gFnSc2	otáčka
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
vynese	vynést	k5eAaPmIp3nS	vynést
závažíčka	závažíčko	k1gNnPc4	závažíčko
výš	vysoce	k6eAd2	vysoce
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
přiškrtí	přiškrtit	k5eAaPmIp3nS	přiškrtit
přívod	přívod	k1gInSc4	přívod
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
fungují	fungovat	k5eAaImIp3nP	fungovat
například	například	k6eAd1	například
termostaty	termostat	k1gInPc4	termostat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
teploty	teplota	k1gFnSc2	teplota
automaticky	automaticky	k6eAd1	automaticky
zapnou	zapnout	k5eAaPmIp3nP	zapnout
topení	topení	k1gNnSc4	topení
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
chlazení	chlazení	k1gNnSc1	chlazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgFnPc1d1	technická
homeostaty	homeostata	k1gFnPc1	homeostata
vždy	vždy	k6eAd1	vždy
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
chybovým	chybový	k2eAgInSc7d1	chybový
signálem	signál	k1gInSc7	signál
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
tedy	tedy	k9	tedy
působit	působit	k5eAaImF	působit
až	až	k6eAd1	až
když	když	k8xS	když
odchylka	odchylka	k1gFnSc1	odchylka
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
určitou	určitý	k2eAgFnSc4d1	určitá
mez	mez	k1gFnSc4	mez
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
homeostatu	homeostat	k1gInSc2	homeostat
postavil	postavit	k5eAaPmAgInS	postavit
Ross	Ross	k1gInSc1	Ross
Ashby	Ashba	k1gFnSc2	Ashba
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
-	-	kIx~	-
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
psychiatr	psychiatr	k1gMnSc1	psychiatr
<g/>
,	,	kIx,	,
pionýr	pionýr	k1gMnSc1	pionýr
kybernetiky	kybernetika	k1gFnSc2	kybernetika
a	a	k8xC	a
studia	studio	k1gNnSc2	studio
komplexních	komplexní	k2eAgInPc2d1	komplexní
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ashby	Ashb	k1gMnPc4	Ashb
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
Ratio	Ratio	k6eAd1	Ratio
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
neformálního	formální	k2eNgInSc2d1	neformální
diskuzního	diskuzní	k2eAgInSc2d1	diskuzní
kroužku	kroužek	k1gInSc2	kroužek
psychologů	psycholog	k1gMnPc2	psycholog
<g/>
,	,	kIx,	,
fyziologů	fyziolog	k1gMnPc2	fyziolog
<g/>
,	,	kIx,	,
matematiků	matematik	k1gMnPc2	matematik
a	a	k8xC	a
inženýrů	inženýr	k1gMnPc2	inženýr
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
během	během	k7c2	během
diskuzí	diskuze	k1gFnPc2	diskuze
o	o	k7c6	o
tehdy	tehdy	k6eAd1	tehdy
nové	nový	k2eAgFnSc6d1	nová
teorii	teorie	k1gFnSc6	teorie
kybernetiky	kybernetika	k1gFnSc2	kybernetika
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
neurolog	neurolog	k1gMnSc1	neurolog
John	John	k1gMnSc1	John
Bates	Bates	k1gMnSc1	Bates
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
aktivní	aktivní	k2eAgInSc4d1	aktivní
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Alan	alan	k1gInSc1	alan
Turing	Turing	k1gInSc1	Turing
napsal	napsat	k5eAaBmAgInS	napsat
Ashbymu	Ashbym	k1gInSc3	Ashbym
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
jej	on	k3xPp3gMnSc4	on
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
použil	použít	k5eAaPmAgMnS	použít
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
experimenty	experiment	k1gInPc1	experiment
jeho	jeho	k3xOp3gFnPc2	jeho
Automatic	Automatice	k1gFnPc2	Automatice
Computing	Computing	k1gInSc1	Computing
Engine	Engin	k1gInSc5	Engin
(	(	kIx(	(
<g/>
ACE	ACE	kA	ACE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
aby	aby	kYmCp3nS	aby
konstruoval	konstruovat	k5eAaImAgInS	konstruovat
nový	nový	k2eAgInSc4d1	nový
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
Ashby	Ashba	k1gFnSc2	Ashba
postavil	postavit	k5eAaPmAgMnS	postavit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
homeostat	homeostat	k1gInSc4	homeostat
<g/>
,	,	kIx,	,
sestrojený	sestrojený	k2eAgInSc4d1	sestrojený
ze	z	k7c2	z
4	[number]	k4	4
řídících	řídící	k2eAgFnPc2d1	řídící
jednotek	jednotka	k1gFnPc2	jednotka
z	z	k7c2	z
použitých	použitý	k2eAgInPc2d1	použitý
RAF	raf	k0	raf
bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
elektronek	elektronka	k1gFnPc2	elektronka
a	a	k8xC	a
magnetických	magnetický	k2eAgInPc2d1	magnetický
potenciometrů	potenciometr	k1gInPc2	potenciometr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
deníku	deník	k1gInSc2	deník
2433	[number]	k4	2433
Ross	Ross	k1gInSc1	Ross
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
poděkování	poděkování	k1gNnSc4	poděkování
Denisu	Denisa	k1gFnSc4	Denisa
a	a	k8xC	a
Grahamovi	Graham	k1gMnSc3	Graham
White	Whit	k1gMnSc5	Whit
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přešli	přejít	k5eAaPmAgMnP	přejít
z	z	k7c2	z
The	The	k1gFnSc2	The
Burden	Burdna	k1gFnPc2	Burdna
Institute	institut	k1gInSc5	institut
pomáhat	pomáhat	k5eAaImF	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Denis	Denisa	k1gFnPc2	Denisa
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
homeostat	homeostat	k1gInSc1	homeostat
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
těžký	těžký	k2eAgInSc1d1	těžký
a	a	k8xC	a
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
nosit	nosit	k5eAaImF	nosit
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Homeostáze	Homeostáze	k1gFnSc1	Homeostáze
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
pouze	pouze	k6eAd1	pouze
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
biologická	biologický	k2eAgFnSc1d1	biologická
homeostáze	homeostáze	k1gFnSc1	homeostáze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celých	celý	k2eAgNnPc2d1	celé
společenství	společenství	k1gNnPc2	společenství
(	(	kIx(	(
<g/>
ekologická	ekologický	k2eAgFnSc1d1	ekologická
homeostáze	homeostáze	k1gFnSc1	homeostáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
stabilní	stabilní	k2eAgInPc4d1	stabilní
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ekologické	ekologický	k2eAgFnPc1d1	ekologická
<g/>
,	,	kIx,	,
udržují	udržovat	k5eAaImIp3nP	udržovat
nějaké	nějaký	k3yIgFnPc4	nějaký
homeostáze	homeostáze	k1gFnPc4	homeostáze
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
dostupné	dostupný	k2eAgFnSc2d1	dostupná
potravě	potrava	k1gFnSc3	potrava
nebo	nebo	k8xC	nebo
mění	měnit	k5eAaImIp3nS	měnit
své	svůj	k3xOyFgFnPc4	svůj
strategie	strategie	k1gFnPc4	strategie
podle	podle	k7c2	podle
vnějších	vnější	k2eAgFnPc2d1	vnější
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
britský	britský	k2eAgMnSc1d1	britský
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
ekolog	ekolog	k1gMnSc1	ekolog
James	James	k1gMnSc1	James
Lovelock	Lovelock	k1gMnSc1	Lovelock
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
Gaia	Gai	k1gInSc2	Gai
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
planeta	planeta	k1gFnSc1	planeta
jakýsi	jakýsi	k3yIgInSc4	jakýsi
superorganismus	superorganismus	k1gInSc4	superorganismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
udržuje	udržovat	k5eAaImIp3nS	udržovat
stabilní	stabilní	k2eAgFnPc4d1	stabilní
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
život	život	k1gInSc4	život
díky	díky	k7c3	díky
celkovým	celkový	k2eAgFnPc3d1	celková
homeostázím	homeostáze	k1gFnPc3	homeostáze
<g/>
.	.	kIx.	.
</s>
<s>
Lovelock	Lovelock	k6eAd1	Lovelock
to	ten	k3xDgNnSc1	ten
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c6	na
hypotetickém	hypotetický	k2eAgInSc6d1	hypotetický
příkladě	příklad	k1gInSc6	příklad
oživené	oživený	k2eAgFnSc2d1	oživená
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udržuje	udržovat	k5eAaImIp3nS	udržovat
stálou	stálý	k2eAgFnSc4d1	stálá
teplotu	teplota	k1gFnSc4	teplota
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mění	měnit	k5eAaImIp3nS	měnit
své	svůj	k3xOyFgNnSc4	svůj
albedo	albedo	k1gNnSc4	albedo
<g/>
,	,	kIx,	,
míru	míra	k1gFnSc4	míra
absorpce	absorpce	k1gFnSc2	absorpce
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
