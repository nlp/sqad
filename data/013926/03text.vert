<s>
Spiritualismus	spiritualismus	k1gInSc1
</s>
<s>
Hypnotizér	hypnotizér	k1gMnSc1
navozuje	navozovat	k5eAaImIp3nS
stav	stav	k1gInSc4
tranzu	tranz	k1gInSc2
<g/>
,	,	kIx,
malba	malba	k1gFnSc1
<g/>
:	:	kIx,
Richard	Richard	k1gMnSc1
Bergh	Bergh	k1gMnSc1
<g/>
,	,	kIx,
1887	#num#	k4
</s>
<s>
Spiritualismus	spiritualismus	k1gInSc1
je	být	k5eAaImIp3nS
víra	víra	k1gFnSc1
že	že	k8xS
se	se	k3xPyFc4
živí	živý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
mohou	moct	k5eAaImIp3nP
stýkat	stýkat	k5eAaImF
s	s	k7c7
duchy	duch	k1gMnPc7
zemřelých	zemřelá	k1gFnPc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
prostřednictvím	prostřednictvím	k7c2
média	médium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
přesvědčení	přesvědčení	k1gNnSc1
je	být	k5eAaImIp3nS
široce	široko	k6eAd1
rozšířené	rozšířený	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
užším	úzký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
je	být	k5eAaImIp3nS
jako	jako	k9
spiritualismus	spiritualismus	k1gInSc1
označován	označován	k2eAgInSc1d1
fenomén	fenomén	k1gInSc1
či	či	k8xC
hnutí	hnutí	k1gNnSc1
vzniklé	vzniklý	k2eAgNnSc1d1
v	v	k7c6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
založené	založený	k2eAgInPc4d1
na	na	k7c6
tomto	tento	k3xDgInSc6
předpokladu	předpoklad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ovlivněn	ovlivnit	k5eAaPmNgInS
dílem	dílo	k1gNnSc7
švédského	švédský	k2eAgMnSc2d1
mystika	mystik	k1gMnSc2
Emanuela	Emanuel	k1gMnSc2
Swedenborga	Swedenborg	k1gMnSc2
a	a	k8xC
myšlenkou	myšlenka	k1gFnSc7
živočišného	živočišný	k2eAgInSc2d1
magnetismu	magnetismus	k1gInSc2
německého	německý	k2eAgMnSc2d1
lékaře	lékař	k1gMnSc2
Franze	Franze	k1gFnSc1
Mesmera	Mesmera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
spiritualismu	spiritualismus	k1gInSc3
bývá	bývat	k5eAaImIp3nS
též	též	k9
řazena	řazen	k2eAgFnSc1d1
„	„	k?
<g/>
harmonická	harmonický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Harmonial	Harmonial	k1gMnSc1
Philosophy	Philosopha	k1gMnSc2
<g/>
)	)	kIx)
Andrewa	Andrewus	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
Davise	Davise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
ze	z	k7c2
spiritualistů	spiritualista	k1gMnPc2
byli	být	k5eAaImAgMnP
významnými	významný	k2eAgFnPc7d1
postavami	postava	k1gFnPc7
hnutí	hnutí	k1gNnPc2
za	za	k7c4
ženská	ženský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
abolicionismus	abolicionismus	k1gInSc4
<g/>
,	,	kIx,
prohibici	prohibice	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
ekonomické	ekonomický	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc1
spiritualismu	spiritualismus	k1gInSc2
spadají	spadat	k5eAaPmIp3nP,k5eAaImIp3nP
do	do	k7c2
období	období	k1gNnSc2
takzvaného	takzvaný	k2eAgInSc2d1
třetího	třetí	k4xOgNnSc2
velkého	velký	k2eAgNnSc2d1
probuzení	probuzení	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
americké	americký	k2eAgFnSc6d1
náboženské	náboženský	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
objevila	objevit	k5eAaPmAgFnS
například	například	k6eAd1
Křesťanská	křesťanský	k2eAgFnSc1d1
věda	věda	k1gFnSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
<g/>
,	,	kIx,
Svědkové	svědek	k1gMnPc1
Jehovovi	Jehovův	k2eAgMnPc1d1
nebo	nebo	k8xC
Teosofická	Teosofický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgMnS
v	v	k7c6
protestantském	protestantský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
New	New	k1gFnSc2
Yorku	York	k1gInSc2
a	a	k8xC
obecně	obecně	k6eAd1
severozápadních	severozápadní	k2eAgInPc2d1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
jižanských	jižanský	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
mezi	mezi	k7c7
katolíky	katolík	k1gMnPc7
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
vliv	vliv	k1gInSc4
mizivý	mizivý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spiritualismus	spiritualismus	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
myšlenky	myšlenka	k1gFnSc2
křesťanského	křesťanský	k2eAgInSc2d1
universalismu	universalismus	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
myšlenky	myšlenka	k1gFnPc1
že	že	k8xS
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
budou	být	k5eAaImBp3nP
nakonec	nakonec	k6eAd1
spaseni	spasit	k5eAaPmNgMnP
<g/>
,	,	kIx,
a	a	k8xC
klade	klást	k5eAaImIp3nS
důraz	důraz	k1gInSc4
na	na	k7c4
osobní	osobní	k2eAgFnSc4d1
duchovní	duchovní	k2eAgFnSc4d1
zkušenost	zkušenost	k1gFnSc4
namísto	namísto	k7c2
následování	následování	k1gNnSc2
autorit	autorita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
samotný	samotný	k2eAgInSc4d1
počátek	počátek	k1gInSc4
hnutí	hnutí	k1gNnSc2
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
působení	působení	k1gNnSc1
sester	sestra	k1gFnPc2
Foxových	foxův	k2eAgFnPc2d1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
předváděly	předvádět	k5eAaImAgFnP
komunikaci	komunikace	k1gFnSc4
s	s	k7c7
duchy	duch	k1gMnPc7
od	od	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
;	;	kIx,
spiritualismus	spiritualismus	k1gInSc1
poté	poté	k6eAd1
rychle	rychle	k6eAd1
získával	získávat	k5eAaImAgMnS
na	na	k7c6
oblibě	obliba	k1gFnSc6
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1855	#num#	k4
byl	být	k5eAaImAgInS
počet	počet	k1gInSc1
jeho	jeho	k3xOp3gMnPc2
příznivců	příznivec	k1gMnPc2
odhadován	odhadován	k2eAgMnSc1d1
mezi	mezi	k7c7
jedním	jeden	k4xCgNnSc7
a	a	k8xC
jedenácti	jedenáct	k4xCc7
miliony	milion	k4xCgInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
jsou	být	k5eAaImIp3nP
takové	takový	k3xDgInPc4
odhady	odhad	k1gInPc4
nespolehlivé	spolehlivý	k2eNgInPc4d1
<g/>
,	,	kIx,
víra	víra	k1gFnSc1
v	v	k7c6
komunikaci	komunikace	k1gFnSc6
s	s	k7c7
duchy	duch	k1gMnPc7
a	a	k8xC
návštěvy	návštěva	k1gFnPc4
seancí	seance	k1gFnPc2
se	se	k3xPyFc4
týkaly	týkat	k5eAaImAgFnP
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
lidí	člověk	k1gMnPc2
–	–	k?
především	především	k6eAd1
po	po	k7c6
Americké	americký	k2eAgFnSc6d1
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
-	-	kIx~
<g/>
1865	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
lidé	člověk	k1gMnPc1
toužili	toužit	k5eAaImAgMnP
po	po	k7c6
kontaktu	kontakt	k1gInSc6
s	s	k7c7
blízkými	blízký	k2eAgMnPc7d1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
v	v	k7c6
tomto	tento	k3xDgInSc6
konfliktu	konflikt	k1gInSc6
zahynuli	zahynout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Postupně	postupně	k6eAd1
však	však	k9
seance	seance	k1gFnSc2
počali	počnout	k5eAaPmAgMnP
splývat	splývat	k5eAaImF
se	s	k7c7
salónním	salónní	k2eAgNnSc7d1
kouzelnictvím	kouzelnictví	k1gNnSc7
a	a	k8xC
média	médium	k1gNnPc4
byla	být	k5eAaImAgFnS
častěji	často	k6eAd2
obviňována	obviňován	k2eAgFnSc1d1
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
pouhými	pouhý	k2eAgMnPc7d1
podvodníky	podvodník	k1gMnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
na	na	k7c6
přelomu	přelom	k1gInSc6
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zbytek	zbytek	k1gInSc4
hnutí	hnutí	k1gNnSc2
zasáhla	zasáhnout	k5eAaPmAgFnS
kampaň	kampaň	k1gFnSc1
Harryho	Harry	k1gMnSc2
Houdiniho	Houdini	k1gMnSc2
ukazující	ukazující	k2eAgInPc1d1
triky	trik	k1gInPc1
médií	médium	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
rozplynulo	rozplynout	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Známým	známý	k2eAgMnSc7d1
spiritualistem	spiritualistem	k?
byl	být	k5eAaImAgMnS
Alfred	Alfred	k1gMnSc1
Russel	Russel	k1gMnSc1
Wallace	Wallace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
DEVENEY	DEVENEY	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Patrick	Patrick	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spiritualism	Spiritualisma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
HANEGRAAFF	HANEGRAAFF	kA
<g/>
,	,	kIx,
Wouter	Wouter	k1gMnSc1
Jacobus	Jacobus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dictionary	Dictionara	k1gFnSc2
of	of	k?
Gnosis	Gnosis	k1gFnSc2
&	&	k?
Western	Western	kA
Esotericism	Esotericism	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Brill	Brill	k1gMnSc1
Academic	Academic	k1gMnSc1
Pub	Pub	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
9004152311	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1074	#num#	k4
<g/>
-	-	kIx~
<g/>
1082	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BUDIL	Budil	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
T.	T.	kA
Spiritualismus	spiritualismus	k1gInSc1
a	a	k8xC
odvrácená	odvrácený	k2eAgFnSc1d1
strana	strana	k1gFnSc1
přírody	příroda	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Metropolitní	metropolitní	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
408	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
755	#num#	k4
<g/>
-	-	kIx~
<g/>
3519	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mystika	mystika	k1gFnSc1
</s>
<s>
Okultismus	okultismus	k1gInSc1
</s>
<s>
Reinkarnace	reinkarnace	k1gFnSc1
</s>
<s>
Seance	seance	k1gFnSc1
</s>
<s>
Fotografie	fotografia	k1gFnPc1
duchovního	duchovní	k2eAgInSc2d1
světa	svět	k1gInSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Spiritualismus	spiritualismus	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gFnSc3
Catholic	Catholice	k1gFnPc2
Encyclopedia	Encyclopedium	k1gNnSc2
<g/>
:	:	kIx,
Spiritism	Spiritism	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4180308-5	4180308-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85126775	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85126775	#num#	k4
</s>
