<s>
Oheň	oheň	k1gInSc1	oheň
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
hoření	hoření	k2eAgFnSc1d1	hoření
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
toto	tento	k3xDgNnSc1	tento
označuje	označovat	k5eAaImIp3nS	označovat
kombinaci	kombinace	k1gFnSc4	kombinace
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
hodně	hodně	k6eAd1	hodně
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
při	při	k7c6	při
rychlé	rychlý	k2eAgFnSc6d1	rychlá
a	a	k8xC	a
samoudržující	samoudržující	k2eAgFnSc6d1	samoudržující
se	se	k3xPyFc4	se
exotermické	exotermický	k2eAgFnSc3d1	exotermická
oxidaci	oxidace	k1gFnSc3	oxidace
hořlavých	hořlavý	k2eAgInPc2d1	hořlavý
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
z	z	k7c2	z
paliva	palivo	k1gNnSc2	palivo
(	(	kIx(	(
<g/>
fosilního	fosilní	k2eAgMnSc2d1	fosilní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obnovitelného	obnovitelný	k2eAgInSc2d1	obnovitelný
zdroje	zdroj	k1gInSc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
–	–	k?	–
hoření	hoření	k1gNnSc2	hoření
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
a	a	k8xC	a
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
vytvářeno	vytvářit	k5eAaPmNgNnS	vytvářit
plameny	plamen	k1gInPc1	plamen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
nad	nad	k7c7	nad
palivem	palivo	k1gNnSc7	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
se	se	k3xPyFc4	se
zažehne	zažehnout	k5eAaPmIp3nS	zažehnout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hořlavá	hořlavý	k2eAgFnSc1d1	hořlavá
látka	látka	k1gFnSc1	látka
vystavena	vystavit	k5eAaPmNgFnS	vystavit
teplu	teplo	k1gNnSc3	teplo
nebo	nebo	k8xC	nebo
jinému	jiný	k2eAgInSc3d1	jiný
zdroji	zdroj	k1gInSc3	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
ohně	oheň	k1gInSc2	oheň
větší	veliký	k2eAgFnSc1d2	veliký
dole	dole	k6eAd1	dole
než	než	k8xS	než
nahoře	nahoře	k6eAd1	nahoře
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
udržuje	udržovat	k5eAaImIp3nS	udržovat
díky	díky	k7c3	díky
teplu	teplo	k1gNnSc3	teplo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
produkuje	produkovat	k5eAaImIp3nS	produkovat
<g/>
.	.	kIx.	.
</s>
<s>
Uhasíná	uhasínat	k5eAaImIp3nS	uhasínat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vyhoří	vyhořet	k5eAaPmIp3nS	vyhořet
všechno	všechen	k3xTgNnSc4	všechen
palivo	palivo	k1gNnSc4	palivo
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
teplota	teplota	k1gFnSc1	teplota
paliva	palivo	k1gNnSc2	palivo
anebo	anebo	k8xC	anebo
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
ohni	oheň	k1gInSc3	oheň
již	již	k6eAd1	již
nedostane	dostat	k5eNaPmIp3nS	dostat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
požárem	požár	k1gInSc7	požár
je	být	k5eAaImIp3nS	být
oheň	oheň	k1gInSc1	oheň
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xS	jako
lidmi	člověk	k1gMnPc7	člověk
řízené	řízený	k2eAgFnSc2d1	řízená
<g/>
,	,	kIx,	,
předem	předem	k6eAd1	předem
plánované	plánovaný	k2eAgNnSc1d1	plánované
a	a	k8xC	a
kontrolované	kontrolovaný	k2eAgNnSc1d1	kontrolované
hoření	hoření	k1gNnSc1	hoření
<g/>
,	,	kIx,	,
ohraničené	ohraničený	k2eAgNnSc1d1	ohraničené
určitým	určitý	k2eAgInSc7d1	určitý
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Plamen	plamen	k1gInSc1	plamen
je	být	k5eAaImIp3nS	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
oblast	oblast	k1gFnSc1	oblast
hořících	hořící	k2eAgInPc2d1	hořící
plynů	plyn	k1gInPc2	plyn
nebo	nebo	k8xC	nebo
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
svítivosti	svítivost	k1gFnSc2	svítivost
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
na	na	k7c4	na
druh	druh	k1gInSc4	druh
spalované	spalovaný	k2eAgFnSc2d1	spalovaná
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
na	na	k7c4	na
dokonalost	dokonalost	k1gFnSc4	dokonalost
spalování	spalování	k1gNnSc1	spalování
<g/>
.	.	kIx.	.
temně	temně	k6eAd1	temně
žlutý	žlutý	k2eAgInSc1d1	žlutý
čadivý	čadivý	k2eAgInSc1d1	čadivý
plamen	plamen	k1gInSc1	plamen
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
nedokonalém	dokonalý	k2eNgNnSc6d1	nedokonalé
spalování	spalování	k1gNnSc6	spalování
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
(	(	kIx(	(
<g/>
např.	např.	kA	např.
volně	volně	k6eAd1	volně
rozlitá	rozlitý	k2eAgFnSc1d1	rozlitá
nafta	nafta	k1gFnSc1	nafta
<g/>
,	,	kIx,	,
guma	guma	k1gFnSc1	guma
<g/>
,	,	kIx,	,
acetylen	acetylen	k1gInSc1	acetylen
z	z	k7c2	z
autogenu	autogen	k1gInSc2	autogen
bez	bez	k7c2	bez
přidání	přidání	k1gNnSc2	přidání
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
se	se	k3xPyFc4	se
nestačí	stačit	k5eNaBmIp3nS	stačit
spalovat	spalovat	k5eAaImF	spalovat
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sazí	saze	k1gFnPc2	saze
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
plamene	plamen	k1gInSc2	plamen
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
dokonalého	dokonalý	k2eAgNnSc2d1	dokonalé
spalování	spalování	k1gNnSc2	spalování
<g/>
.	.	kIx.	.
žlutý	žlutý	k2eAgInSc1d1	žlutý
svítivý	svítivý	k2eAgInSc1d1	svítivý
plamen	plamen	k1gInSc1	plamen
–	–	k?	–
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
do	do	k7c2	do
sloupce	sloupec	k1gInSc2	sloupec
hořících	hořící	k2eAgInPc2d1	hořící
plynů	plyn	k1gInPc2	plyn
organické	organický	k2eAgFnSc2d1	organická
látky	látka	k1gFnSc2	látka
dostává	dostávat	k5eAaImIp3nS	dostávat
více	hodně	k6eAd2	hodně
kyslíku	kyslík	k1gInSc2	kyslík
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
než	než	k8xS	než
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
částečky	částečka	k1gFnPc1	částečka
uhlíku	uhlík	k1gInSc2	uhlík
se	se	k3xPyFc4	se
rozžhaví	rozžhavit	k5eAaPmIp3nS	rozžhavit
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
a	a	k8xC	a
svítí	svítit	k5eAaImIp3nS	svítit
(	(	kIx(	(
<g/>
plamen	plamen	k1gInSc1	plamen
svíčky	svíčka	k1gFnSc2	svíčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
částí	část	k1gFnSc7	část
pak	pak	k6eAd1	pak
v	v	k7c6	v
přebytku	přebytek	k1gInSc6	přebytek
vzduchu	vzduch	k1gInSc2	vzduch
shoří	shořet	k5eAaPmIp3nS	shořet
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
vývinu	vývin	k1gInSc3	vývin
sazí	saze	k1gFnPc2	saze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
neviditelné	viditelný	k2eNgFnSc6d1	neviditelná
části	část	k1gFnSc6	část
plamene	plamen	k1gInSc2	plamen
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
bílý	bílý	k2eAgInSc1d1	bílý
svítivý	svítivý	k2eAgInSc1d1	svítivý
plamen	plamen	k1gInSc1	plamen
-	-	kIx~	-
pokud	pokud	k8xS	pokud
technickými	technický	k2eAgNnPc7d1	technické
opatřeními	opatření	k1gNnPc7	opatření
zajistíme	zajistit	k5eAaPmIp1nP	zajistit
ještě	ještě	k6eAd1	ještě
dokonalejší	dokonalý	k2eAgInSc4d2	dokonalejší
přístup	přístup	k1gInSc4	přístup
vzduchu	vzduch	k1gInSc2	vzduch
k	k	k7c3	k
plameni	plamen	k1gInSc3	plamen
(	(	kIx(	(
<g/>
cylindr	cylindr	k1gInSc1	cylindr
petrolejové	petrolejový	k2eAgFnSc2d1	petrolejová
lampy	lampa	k1gFnSc2	lampa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
částečky	částečka	k1gFnPc1	částečka
uhlíku	uhlík	k1gInSc2	uhlík
se	se	k3xPyFc4	se
rozžhaví	rozžhavit	k5eAaPmIp3nS	rozžhavit
doběla	doběla	k6eAd1	doběla
a	a	k8xC	a
svítí	svítit	k5eAaImIp3nS	svítit
mnohem	mnohem	k6eAd1	mnohem
intenzivněji	intenzivně	k6eAd2	intenzivně
<g/>
.	.	kIx.	.
modrý	modrý	k2eAgInSc1d1	modrý
nesvítivý	svítivý	k2eNgInSc1d1	nesvítivý
plamen	plamen	k1gInSc1	plamen
–	–	k?	–
u	u	k7c2	u
uhlíkatých	uhlíkatý	k2eAgFnPc2d1	uhlíkatá
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
jej	on	k3xPp3gMnSc4	on
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
při	při	k7c6	při
dokonalém	dokonalý	k2eAgNnSc6d1	dokonalé
spalování	spalování	k1gNnSc6	spalování
v	v	k7c6	v
přebytku	přebytek	k1gInSc6	přebytek
vzduchu	vzduch	k1gInSc2	vzduch
nebo	nebo	k8xC	nebo
čistého	čistý	k2eAgInSc2d1	čistý
kyslíku	kyslík	k1gInSc2	kyslík
(	(	kIx(	(
<g/>
plamen	plamen	k1gInSc1	plamen
plynového	plynový	k2eAgInSc2d1	plynový
sporáku	sporák	k1gInSc2	sporák
<g/>
,	,	kIx,	,
benzinové	benzinový	k2eAgFnSc2d1	benzinová
letlampy	letlampa	k1gFnSc2	letlampa
<g/>
,	,	kIx,	,
acetylen	acetylen	k1gInSc1	acetylen
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
při	při	k7c6	při
autogenním	autogenní	k2eAgNnSc6d1	autogenní
svařování	svařování	k1gNnSc6	svařování
<g/>
.	.	kIx.	.
</s>
<s>
Částečky	částečka	k1gFnPc1	částečka
uhlíku	uhlík	k1gInSc2	uhlík
nesvítivě	svítivě	k6eNd1	svítivě
shoří	shořet	k5eAaPmIp3nS	shořet
ještě	ještě	k6eAd1	ještě
uvnitř	uvnitř	k7c2	uvnitř
plamene	plamen	k1gInSc2	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Nesvítivý	svítivý	k2eNgInSc1d1	nesvítivý
plamen	plamen	k1gInSc1	plamen
dokonalého	dokonalý	k2eAgNnSc2d1	dokonalé
spalování	spalování	k1gNnSc2	spalování
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
modrý	modrý	k2eAgInSc1d1	modrý
plamen	plamen	k1gInSc1	plamen
bezuhlíkatých	bezuhlíkatý	k2eAgFnPc2d1	bezuhlíkatý
látek	látka	k1gFnPc2	látka
nebo	nebo	k8xC	nebo
látek	látka	k1gFnPc2	látka
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
-	-	kIx~	-
např.	např.	kA	např.
hořící	hořící	k2eAgInSc1d1	hořící
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
,	,	kIx,	,
ethanol	ethanol	k1gInSc1	ethanol
apod.	apod.	kA	apod.
V	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
plameni	plamen	k1gInSc6	plamen
svíčky	svíčka	k1gFnSc2	svíčka
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
několik	několik	k4yIc4	několik
pásem	pásmo	k1gNnPc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
uvnitř	uvnitř	k7c2	uvnitř
plamene	plamen	k1gInSc2	plamen
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
knotu	knot	k1gInSc2	knot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pásmo	pásmo	k1gNnSc1	pásmo
zplynovací	zplynovací	k2eAgNnSc1d1	zplynovací
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
roztavený	roztavený	k2eAgInSc1d1	roztavený
parafin	parafin	k1gInSc1	parafin
teplem	teplo	k1gNnSc7	teplo
mění	měnit	k5eAaImIp3nP	měnit
v	v	k7c4	v
páry	pár	k1gInPc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
tedy	tedy	k9	tedy
ještě	ještě	k9	ještě
nic	nic	k3yNnSc1	nic
nehoří	hořet	k5eNaImIp3nS	hořet
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
tohoto	tento	k3xDgNnSc2	tento
pásma	pásmo	k1gNnSc2	pásmo
nastává	nastávat	k5eAaImIp3nS	nastávat
mísení	mísení	k1gNnSc1	mísení
par	para	k1gFnPc2	para
parafinu	parafin	k1gInSc2	parafin
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
zapálení	zapálení	k1gNnSc1	zapálení
hořlavé	hořlavý	k2eAgFnSc2d1	hořlavá
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
plamene	plamen	k1gInSc2	plamen
má	mít	k5eAaImIp3nS	mít
redukční	redukční	k2eAgInPc4d1	redukční
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
přebytek	přebytek	k1gInSc1	přebytek
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
rozžhavené	rozžhavený	k2eAgFnPc1d1	rozžhavená
částečky	částečka	k1gFnPc1	částečka
žlutě	žlutě	k6eAd1	žlutě
svítí	svítit	k5eAaImIp3nP	svítit
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
vnější	vnější	k2eAgNnSc1d1	vnější
pásmo	pásmo	k1gNnSc1	pásmo
plamene	plamen	k1gInSc2	plamen
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
neviditelné	viditelný	k2eNgNnSc1d1	neviditelné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
oxidační	oxidační	k2eAgNnSc1d1	oxidační
pásmo	pásmo	k1gNnSc1	pásmo
s	s	k7c7	s
přebytkekm	přebytkek	k1gNnSc7	přebytkek
kyslíku	kyslík	k1gInSc2	kyslík
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Uhlíkové	Uhlíkové	k2eAgFnPc1d1	Uhlíkové
částečky	částečka	k1gFnPc1	částečka
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
spalují	spalovat	k5eAaImIp3nP	spalovat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
fáze	fáze	k1gFnPc1	fáze
hoření	hoření	k2eAgFnPc1d1	hoření
lze	lze	k6eAd1	lze
pěkně	pěkně	k6eAd1	pěkně
pozorovat	pozorovat	k5eAaImF	pozorovat
na	na	k7c6	na
tábornickém	tábornický	k2eAgInSc6d1	tábornický
ohýnku	ohýnek	k1gInSc6	ohýnek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přiloženého	přiložený	k2eAgNnSc2d1	přiložené
nevysušeného	vysušený	k2eNgNnSc2d1	nevysušené
polínka	polínko	k1gNnSc2	polínko
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
vypařují	vypařovat	k5eAaImIp3nP	vypařovat
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dřevo	dřevo	k1gNnSc1	dřevo
vysuší	vysušit	k5eAaPmIp3nS	vysušit
a	a	k8xC	a
prohřeje	prohřát	k5eAaPmIp3nS	prohřát
<g/>
,	,	kIx,	,
vzplanou	vzplanout	k5eAaPmIp3nP	vzplanout
čadivým	čadivý	k2eAgInSc7d1	čadivý
plamenem	plamen	k1gInSc7	plamen
pryskyřičné	pryskyřičný	k2eAgFnSc2d1	pryskyřičná
části	část	k1gFnSc2	část
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
uhlíku	uhlík	k1gInSc2	uhlík
ve	v	k7c6	v
smole	smola	k1gFnSc6	smola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
z	z	k7c2	z
praskajícího	praskající	k2eAgNnSc2d1	praskající
polínka	polínko	k1gNnSc2	polínko
vystaveného	vystavený	k2eAgInSc2d1	vystavený
žáru	žár	k1gInSc2	žár
ohniště	ohniště	k1gNnSc1	ohniště
vyrážejí	vyrážet	k5eAaImIp3nP	vyrážet
dýmající	dýmající	k2eAgInPc1d1	dýmající
hořlavé	hořlavý	k2eAgInPc1d1	hořlavý
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
dřevní	dřevní	k2eAgFnSc2d1	dřevní
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plyny	plyn	k1gInPc1	plyn
se	se	k3xPyFc4	se
zapalují	zapalovat	k5eAaImIp3nP	zapalovat
a	a	k8xC	a
hoří	hořet	k5eAaImIp3nP	hořet
svítivým	svítivý	k2eAgInSc7d1	svítivý
žlutým	žlutý	k2eAgInSc7d1	žlutý
plamenem	plamen	k1gInSc7	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
dřevo	dřevo	k1gNnSc1	dřevo
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
rozloží	rozložit	k5eAaPmIp3nP	rozložit
a	a	k8xC	a
těkavé	těkavý	k2eAgInPc1d1	těkavý
plyny	plyn	k1gInPc1	plyn
vyhoří	vyhořet	k5eAaPmIp3nP	vyhořet
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
plamen	plamen	k1gInSc1	plamen
přecházet	přecházet	k5eAaImF	přecházet
do	do	k7c2	do
modrého	modré	k1gNnSc2	modré
zbarvení	zbarvení	k1gNnSc2	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
zbývajícím	zbývající	k2eAgNnSc6d1	zbývající
rozžhaveném	rozžhavený	k2eAgNnSc6d1	rozžhavené
dřevěném	dřevěný	k2eAgNnSc6d1	dřevěné
uhlí	uhlí	k1gNnSc6	uhlí
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
spalováním	spalování	k1gNnSc7	spalování
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhelnatý	uhelnatý	k2eAgInSc4d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
hoří	hořet	k5eAaImIp3nS	hořet
namodralým	namodralý	k2eAgInSc7d1	namodralý
plamenem	plamen	k1gInSc7	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
chemické	chemický	k2eAgInPc1d1	chemický
prvky	prvek	k1gInPc1	prvek
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
sloučeniny	sloučenina	k1gFnPc1	sloučenina
charakteristicky	charakteristicky	k6eAd1	charakteristicky
zbarvují	zbarvovat	k5eAaImIp3nP	zbarvovat
nesvítivý	svítivý	k2eNgInSc4d1	nesvítivý
plamen	plamen	k1gInSc4	plamen
laboratorního	laboratorní	k2eAgInSc2d1	laboratorní
kahanu	kahan	k1gInSc2	kahan
<g/>
:	:	kIx,	:
sodík	sodík	k1gInSc1	sodík
-	-	kIx~	-
sytě	sytě	k6eAd1	sytě
žlutě	žlutě	k6eAd1	žlutě
draslík	draslík	k1gInSc4	draslík
-	-	kIx~	-
fialově	fialově	k6eAd1	fialově
vápník	vápník	k1gInSc1	vápník
-	-	kIx~	-
cihlově	cihlově	k6eAd1	cihlově
červeně	červeň	k1gFnPc4	červeň
měď	měď	k1gFnSc4	měď
-	-	kIx~	-
trávově	trávově	k6eAd1	trávově
zeleně	zeleně	k6eAd1	zeleně
lithium	lithium	k1gNnSc1	lithium
-	-	kIx~	-
purpurově	purpurově	k6eAd1	purpurově
červeně	červeně	k6eAd1	červeně
stroncium	stroncium	k1gNnSc1	stroncium
-	-	kIx~	-
karmínově	karmínově	k6eAd1	karmínově
červeně	červeně	k6eAd1	červeně
baryum	baryum	k1gNnSc1	baryum
-	-	kIx~	-
žlutozeleně	žlutozeleně	k6eAd1	žlutozeleně
antimon	antimon	k1gInSc1	antimon
-	-	kIx~	-
modře	modro	k6eAd1	modro
Toho	ten	k3xDgMnSc4	ten
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jak	jak	k6eAd1	jak
při	při	k7c6	při
jednoduchých	jednoduchý	k2eAgFnPc6d1	jednoduchá
předběžných	předběžný	k2eAgFnPc6d1	předběžná
analýzách	analýza	k1gFnPc6	analýza
neznámé	známý	k2eNgFnSc2d1	neznámá
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
tak	tak	k9	tak
při	při	k7c6	při
náročnějších	náročný	k2eAgInPc6d2	náročnější
a	a	k8xC	a
přesnějších	přesný	k2eAgInPc6d2	přesnější
analytických	analytický	k2eAgInPc6d1	analytický
postupech	postup	k1gInPc6	postup
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
v	v	k7c6	v
ohněstrůjství	ohněstrůjství	k1gNnSc6	ohněstrůjství
(	(	kIx(	(
<g/>
barevné	barevný	k2eAgInPc4d1	barevný
bengálské	bengálský	k2eAgInPc4d1	bengálský
ohně	oheň	k1gInPc4	oheň
<g/>
,	,	kIx,	,
rakety	raketa	k1gFnPc4	raketa
<g/>
,	,	kIx,	,
světlice	světlice	k1gFnPc4	světlice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
se	se	k3xPyFc4	se
člověku	člověk	k1gMnSc6	člověk
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
první	první	k4xOgFnSc1	první
obrovská	obrovský	k2eAgFnSc1d1	obrovská
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
začal	začít	k5eAaPmAgMnS	začít
měnit	měnit	k5eAaImF	měnit
tvář	tvář	k1gFnSc4	tvář
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
je	být	k5eAaImIp3nS	být
předchůdci	předchůdce	k1gMnSc3	předchůdce
moderního	moderní	k2eAgNnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
využíván	využívat	k5eAaPmNgInS	využívat
snad	snad	k9	snad
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
přibližně	přibližně	k6eAd1	přibližně
1,5	[number]	k4	1,5
miliónem	milión	k4xCgInSc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
ale	ale	k8xC	ale
až	až	k9	až
před	před	k7c7	před
1	[number]	k4	1
miliónem	milión	k4xCgInSc7	milión
let	léto	k1gNnPc2	léto
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
jednoznačnější	jednoznačný	k2eAgInPc1d2	jednoznačnější
důkazy	důkaz	k1gInPc1	důkaz
se	se	k3xPyFc4	se
omezují	omezovat	k5eAaImIp3nP	omezovat
na	na	k7c4	na
statisíce	statisíce	k1gInPc4	statisíce
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
Už	už	k6eAd1	už
desetitisíce	desetitisíce	k1gInPc4	desetitisíce
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
člověkem	člověk	k1gMnSc7	člověk
využíván	využíván	k2eAgMnSc1d1	využíván
i	i	k8xC	i
k	k	k7c3	k
fire-stick	firetick	k6eAd1	fire-stick
farming	farming	k1gInSc4	farming
a	a	k8xC	a
také	také	k9	také
i	i	k9	i
ke	k	k7c3	k
žďáření	žďáření	k1gNnSc3	žďáření
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
člověku	člověk	k1gMnSc3	člověk
rozmanité	rozmanitý	k2eAgFnSc2d1	rozmanitá
služby	služba	k1gFnSc2	služba
<g/>
:	:	kIx,	:
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
chladem	chlad	k1gInSc7	chlad
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
v	v	k7c6	v
kamnech	kamna	k1gNnPc6	kamna
či	či	k8xC	či
v	v	k7c6	v
krbech	krb	k1gInPc6	krb
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svícení	svícení	k1gNnSc4	svícení
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
(	(	kIx(	(
<g/>
jednoduchá	jednoduchý	k2eAgNnPc4d1	jednoduché
osvětlovací	osvětlovací	k2eAgNnPc4d1	osvětlovací
tělesa	těleso	k1gNnPc4	těleso
používající	používající	k2eAgInSc4d1	používající
otevřený	otevřený	k2eAgInSc4d1	otevřený
oheň	oheň	k1gInSc4	oheň
<g/>
:	:	kIx,	:
pochodeň	pochodeň	k1gFnSc1	pochodeň
<g/>
,	,	kIx,	,
louče	louč	k1gFnPc1	louč
<g/>
,	,	kIx,	,
olejová	olejový	k2eAgFnSc1d1	olejová
lampa	lampa	k1gFnSc1	lampa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
svíčka	svíčka	k1gFnSc1	svíčka
<g/>
,	,	kIx,	,
petrolejová	petrolejový	k2eAgFnSc1d1	petrolejová
lampa	lampa	k1gFnSc1	lampa
<g/>
,	,	kIx,	,
plynová	plynový	k2eAgFnSc1d1	plynová
lampa	lampa	k1gFnSc1	lampa
<g/>
)	)	kIx)	)
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
šelmami	šelma	k1gFnPc7	šelma
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
připravit	připravit	k5eAaPmF	připravit
pokrm	pokrm	k1gInSc1	pokrm
(	(	kIx(	(
<g/>
vaření	vaření	k1gNnSc1	vaření
<g/>
,	,	kIx,	,
pečení	pečení	k1gNnSc1	pečení
<g/>
,	,	kIx,	,
smažení	smažení	k1gNnSc1	smažení
<g/>
,	,	kIx,	,
dušení	dušení	k1gNnSc1	dušení
<g/>
,	,	kIx,	,
zapékání	zapékání	k1gNnSc1	zapékání
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
kouřem	kouř	k1gInSc7	kouř
lze	lze	k6eAd1	lze
předávat	předávat	k5eAaImF	předávat
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
zprávy	zpráva	k1gFnPc4	zpráva
-	-	kIx~	-
kouřový	kouřový	k2eAgInSc4d1	kouřový
signál	signál	k1gInSc4	signál
oheň	oheň	k1gInSc1	oheň
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
vodní	vodní	k2eAgInSc1d1	vodní
<g />
.	.	kIx.	.
</s>
<s>
páru	pára	k1gFnSc4	pára
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
parní	parní	k2eAgInSc1d1	parní
stroj	stroj	k1gInSc1	stroj
či	či	k8xC	či
parní	parní	k2eAgFnSc1d1	parní
turbína	turbína	k1gFnSc1	turbína
<g/>
)	)	kIx)	)
vypalování	vypalování	k1gNnSc1	vypalování
porostů	porost	k1gInPc2	porost
přináší	přinášet	k5eAaImIp3nS	přinášet
rozmanitý	rozmanitý	k2eAgInSc4d1	rozmanitý
užitek	užitek	k1gInSc4	užitek
(	(	kIx(	(
<g/>
opečený	opečený	k2eAgInSc4d1	opečený
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
lovné	lovný	k2eAgInPc4d1	lovný
druhy	druh	k1gInPc4	druh
a	a	k8xC	a
plodonosné	plodonosný	k2eAgFnPc4d1	plodonosná
či	či	k8xC	či
léčivé	léčivý	k2eAgFnPc4d1	léčivá
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
zničení	zničení	k1gNnSc4	zničení
úkrytů	úkryt	k1gInPc2	úkryt
šelem	šelma	k1gFnPc2	šelma
a	a	k8xC	a
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
vytvoření	vytvoření	k1gNnSc1	vytvoření
cest	cesta	k1gFnPc2	cesta
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
ohně	oheň	k1gInSc2	oheň
lze	lze	k6eAd1	lze
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
a	a	k8xC	a
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
různé	různý	k2eAgInPc4d1	různý
materiály	materiál	k1gInPc4	materiál
(	(	kIx(	(
<g/>
tavení	tavení	k1gNnSc2	tavení
vosku	vosk	k1gInSc2	vosk
<g/>
,	,	kIx,	,
škvaření	škvaření	k1gNnSc2	škvaření
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
vypalování	vypalování	k1gNnSc2	vypalování
hlíny	hlína	k1gFnSc2	hlína
<g/>
,	,	kIx,	,
tavení	tavení	k1gNnSc6	tavení
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
vypalování	vypalování	k1gNnSc2	vypalování
dřeva	dřevo	k1gNnSc2	dřevo
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
oheň	oheň	k1gInSc1	oheň
(	(	kIx(	(
<g/>
totiž	totiž	k9	totiž
spalování	spalování	k1gNnSc1	spalování
organického	organický	k2eAgInSc2d1	organický
materiálu	materiál	k1gInSc2	materiál
<g/>
)	)	kIx)	)
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
různé	různý	k2eAgInPc4d1	různý
produkty	produkt	k1gInPc4	produkt
(	(	kIx(	(
<g/>
kouř	kouř	k1gInSc1	kouř
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
saze	saze	k1gFnSc1	saze
<g/>
,	,	kIx,	,
popel	popel	k1gInSc1	popel
<g/>
,	,	kIx,	,
dehet	dehet	k1gInSc1	dehet
<g/>
,	,	kIx,	,
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
uhlí	uhlí	k1gNnSc4	uhlí
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
oheň	oheň	k1gInSc1	oheň
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
lidem	člověk	k1gMnPc3	člověk
příjemný	příjemný	k2eAgInSc4d1	příjemný
vonný	vonný	k2eAgInSc4d1	vonný
kouř	kouř	k1gInSc4	kouř
(	(	kIx(	(
<g/>
dýmka	dýmka	k1gFnSc1	dýmka
<g/>
,	,	kIx,	,
doutník	doutník	k1gInSc1	doutník
<g/>
,	,	kIx,	,
cigareta	cigareta	k1gFnSc1	cigareta
<g/>
,	,	kIx,	,
vonné	vonný	k2eAgFnPc4d1	vonná
tyčinky	tyčinka	k1gFnPc4	tyčinka
<g/>
,	,	kIx,	,
vonné	vonný	k2eAgFnPc4d1	vonná
svíčky	svíčka	k1gFnPc4	svíčka
<g/>
)	)	kIx)	)
teplo	teplo	k1gNnSc4	teplo
ohně	oheň	k1gInSc2	oheň
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
produkty	produkt	k1gInPc7	produkt
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
<g />
.	.	kIx.	.
</s>
<s>
léčení	léčení	k1gNnSc1	léčení
(	(	kIx(	(
<g/>
vypalování	vypalování	k1gNnSc1	vypalování
ran	rána	k1gFnPc2	rána
a	a	k8xC	a
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
vyvařování	vyvařování	k1gNnSc1	vyvařování
obvazů	obvaz	k1gInPc2	obvaz
<g/>
,	,	kIx,	,
nahřívání	nahřívání	k1gNnSc2	nahřívání
a	a	k8xC	a
vykuřování	vykuřování	k1gNnSc2	vykuřování
<g/>
,	,	kIx,	,
horké	horký	k2eAgFnPc1d1	horká
lázně	lázeň	k1gFnPc1	lázeň
vodní	vodní	k2eAgFnPc1d1	vodní
i	i	k8xC	i
parní	parní	k2eAgFnPc1d1	parní
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
léků	lék	k1gInPc2	lék
<g/>
)	)	kIx)	)
spalování	spalování	k1gNnSc1	spalování
a	a	k8xC	a
vykuřování	vykuřování	k1gNnSc1	vykuřování
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
očištění	očištění	k1gNnSc3	očištění
oheň	oheň	k1gInSc1	oheň
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
při	při	k7c6	při
různých	různý	k2eAgInPc6d1	různý
obřadech	obřad	k1gInPc6	obřad
společenské	společenský	k2eAgFnSc2d1	společenská
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
narozeninové	narozeninový	k2eAgFnSc2d1	narozeninová
svíčky	svíčka	k1gFnSc2	svíčka
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
zemřelé	zemřelá	k1gFnPc4	zemřelá
<g/>
,	,	kIx,	,
táborové	táborový	k2eAgInPc4d1	táborový
a	a	k8xC	a
besední	besední	k2eAgInPc4d1	besední
ohně	oheň	k1gInPc4	oheň
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
oheň	oheň	k1gInSc1	oheň
lze	lze	k6eAd1	lze
ale	ale	k9	ale
použít	použít	k5eAaPmF	použít
i	i	k9	i
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
napalm	napalm	k1gInSc1	napalm
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
k	k	k7c3	k
mučení	mučení	k1gNnSc3	mučení
<g/>
,	,	kIx,	,
zabíjení	zabíjení	k1gNnSc3	zabíjení
a	a	k8xC	a
popravám	poprava	k1gFnPc3	poprava
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
upálení	upálení	k1gNnSc1	upálení
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
<g/>
)	)	kIx)	)
O	o	k7c6	o
ohni	oheň	k1gInSc6	oheň
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
mnoho	mnoho	k4c1	mnoho
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
přišel	přijít	k5eAaPmAgMnS	přijít
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
básník	básník	k1gMnSc1	básník
Hésiodos	Hésiodos	k1gMnSc1	Hésiodos
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
bohové	bůh	k1gMnPc1	bůh
lidem	člověk	k1gMnPc3	člověk
oheň	oheň	k1gInSc1	oheň
vzali	vzít	k5eAaPmAgMnP	vzít
za	za	k7c4	za
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
nemohli	moct	k5eNaImAgMnP	moct
vařit	vařit	k5eAaImF	vařit
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
přítel	přítel	k1gMnSc1	přítel
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
mazaný	mazaný	k2eAgMnSc1d1	mazaný
Prométheus	Prométheus	k1gMnSc1	Prométheus
<g/>
,	,	kIx,	,
bohům	bůh	k1gMnPc3	bůh
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
uhlík	uhlík	k1gInSc4	uhlík
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
krbu	krb	k1gInSc2	krb
a	a	k8xC	a
donesl	donést	k5eAaPmAgInS	donést
ho	on	k3xPp3gMnSc4	on
lidem	člověk	k1gMnPc3	člověk
ve	v	k7c6	v
fenyklovém	fenyklový	k2eAgInSc6d1	fenyklový
stonku	stonek	k1gInSc6	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Irokézové	Irokéz	k1gMnPc1	Irokéz
zase	zase	k9	zase
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
uhodil	uhodit	k5eAaPmAgInS	uhodit
blesk	blesk	k1gInSc1	blesk
do	do	k7c2	do
stromu	strom	k1gInSc2	strom
a	a	k8xC	a
všechna	všechen	k3xTgNnPc1	všechen
zvířata	zvíře	k1gNnPc1	zvíře
koukala	koukat	k5eAaImAgNnP	koukat
na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
div	div	k1gInSc4	div
a	a	k8xC	a
chtěla	chtít	k5eAaImAgFnS	chtít
oheň	oheň	k1gInSc4	oheň
přinést	přinést	k5eAaPmF	přinést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
mohla	moct	k5eAaImAgFnS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
obřady	obřad	k1gInPc4	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
zvířata	zvíře	k1gNnPc1	zvíře
silná	silný	k2eAgNnPc1d1	silné
i	i	k8xC	i
rychlá	rychlý	k2eAgFnSc1d1	rychlá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechna	všechen	k3xTgFnSc1	všechen
se	se	k3xPyFc4	se
jen	jen	k6eAd1	jen
popálila	popálit	k5eAaPmAgFnS	popálit
a	a	k8xC	a
umazala	umazat	k5eAaPmAgFnS	umazat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
nejchytřejší	chytrý	k2eAgMnPc1d3	nejchytřejší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
pavouk	pavouk	k1gMnSc1	pavouk
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
uhlík	uhlík	k1gInSc4	uhlík
v	v	k7c6	v
ranečku	raneček	k1gInSc6	raneček
z	z	k7c2	z
pavučiny	pavučina	k1gFnSc2	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
staré	starý	k2eAgFnPc4d1	stará
kultury	kultura	k1gFnPc4	kultura
je	být	k5eAaImIp3nS	být
oheň	oheň	k1gInSc1	oheň
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
prvků	prvek	k1gInPc2	prvek
lidského	lidský	k2eAgInSc2d1	lidský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potupy	potupa	k1gFnSc2	potupa
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
Spartě	Sparta	k1gFnSc6	Sparta
bylo	být	k5eAaImAgNnS	být
odepření	odepření	k1gNnSc1	odepření
ohně	oheň	k1gInSc2	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Provinilec	provinilec	k1gMnSc1	provinilec
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
vyloučený	vyloučený	k2eAgMnSc1d1	vyloučený
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
zimě	zima	k1gFnSc3	zima
<g/>
,	,	kIx,	,
noční	noční	k2eAgFnSc3d1	noční
tmě	tma	k1gFnSc3	tma
a	a	k8xC	a
syrovému	syrový	k2eAgNnSc3d1	syrové
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
<g/>
,	,	kIx,	,
Historie	historie	k1gFnSc1	historie
7.231	[number]	k4	7.231
<g/>
)	)	kIx)	)
Starověké	starověký	k2eAgInPc1d1	starověký
a	a	k8xC	a
středověké	středověký	k2eAgInPc1d1	středověký
národy	národ	k1gInPc1	národ
jej	on	k3xPp3gMnSc4	on
považovaly	považovat	k5eAaImAgInP	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
základních	základní	k2eAgInPc2d1	základní
živlů	živel	k1gInPc2	živel
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
ještě	ještě	k9	ještě
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
teorie	teorie	k1gFnSc2	teorie
živlů	živel	k1gInPc2	živel
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
jejího	její	k3xOp3gMnSc2	její
původce	původce	k1gMnSc2	původce
označován	označován	k2eAgMnSc1d1	označován
svérázný	svérázný	k2eAgMnSc1d1	svérázný
mudrc	mudrc	k1gMnSc1	mudrc
Empedoklés	Empedoklésa	k1gFnPc2	Empedoklésa
z	z	k7c2	z
Akragantu	Akragant	k1gInSc2	Akragant
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
písni	píseň	k1gFnSc6	píseň
zpívá	zpívat	k5eAaImIp3nS	zpívat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
božských	božská	k1gFnPc2	božská
"	"	kIx"	"
<g/>
kořenů	kořen	k1gInPc2	kořen
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
rhizómata	rhizóma	k1gNnPc1	rhizóma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
lze	lze	k6eAd1	lze
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
se	s	k7c7	s
živly	živel	k1gInPc7	živel
a	a	k8xC	a
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
mísí	mísit	k5eAaImIp3nP	mísit
vlivem	vlivem	k7c2	vlivem
Lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
Filié	Filiá	k1gFnSc2	Filiá
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozlučují	rozlučovat	k5eAaImIp3nP	rozlučovat
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
Sváru	svár	k1gInSc2	svár
(	(	kIx(	(
<g/>
Neikos	Neikos	k1gInSc1	Neikos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
živlů	živel	k1gInPc2	živel
z	z	k7c2	z
čiré	čirý	k2eAgFnSc2d1	čirá
nerozlišenosti	nerozlišenost	k1gFnSc2	nerozlišenost
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
chóra	chóra	k6eAd1	chóra
<g/>
)	)	kIx)	)
skládáním	skládání	k1gNnSc7	skládání
ideálních	ideální	k2eAgInPc2d1	ideální
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
z	z	k7c2	z
pěti	pět	k4xCc2	pět
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
dokonalých	dokonalý	k2eAgNnPc2d1	dokonalé
těles	těleso	k1gNnPc2	těleso
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čtyřstěn	čtyřstěn	k1gInSc1	čtyřstěn
-	-	kIx~	-
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
krychle	krychle	k1gFnPc1	krychle
-	-	kIx~	-
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
osmistěn	osmistěn	k1gInSc1	osmistěn
-	-	kIx~	-
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
dvacetistěn	dvacetistěn	k2eAgInSc1d1	dvacetistěn
-	-	kIx~	-
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
viditelného	viditelný	k2eAgInSc2d1	viditelný
světa	svět	k1gInSc2	svět
a	a	k8xC	a
bytostí	bytost	k1gFnPc2	bytost
z	z	k7c2	z
živlů	živel	k1gInPc2	živel
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
Platónův	Platónův	k2eAgInSc4d1	Platónův
dialog	dialog	k1gInSc4	dialog
Timaios	Timaiosa	k1gFnPc2	Timaiosa
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
určuje	určovat	k5eAaImIp3nS	určovat
každý	každý	k3xTgInSc1	každý
ze	z	k7c2	z
živlů	živel	k1gInPc2	živel
podle	podle	k7c2	podle
dvou	dva	k4xCgFnPc2	dva
taktilních	taktilní	k2eAgFnPc2d1	taktilní
kvalit	kvalita	k1gFnPc2	kvalita
(	(	kIx(	(
<g/>
hmatových	hmatový	k2eAgFnPc2d1	hmatová
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Oheň	oheň	k1gInSc1	oheň
(	(	kIx(	(
<g/>
PÝR	pýr	k1gInSc1	pýr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
teplý	teplý	k2eAgInSc1d1	teplý
a	a	k8xC	a
suchý	suchý	k2eAgInSc1d1	suchý
<g/>
,	,	kIx,	,
vzduch	vzduch	k1gInSc1	vzduch
(	(	kIx(	(
<g/>
AÉR	AÉR	kA	AÉR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
teplý	teplý	k2eAgMnSc1d1	teplý
a	a	k8xC	a
vlhký	vlhký	k2eAgMnSc1d1	vlhký
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
HYDÓR	HYDÓR	kA	HYDÓR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
studená	studený	k2eAgFnSc1d1	studená
a	a	k8xC	a
vlhká	vlhký	k2eAgFnSc1d1	vlhká
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
GÉ	GÉ	kA	GÉ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
studená	studený	k2eAgFnSc1d1	studená
a	a	k8xC	a
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
aristotelského	aristotelský	k2eAgInSc2d1	aristotelský
modelu	model	k1gInSc2	model
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
změny	změna	k1gFnSc2	změna
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
obou	dva	k4xCgFnPc2	dva
kvalit	kvalita	k1gFnPc2	kvalita
–	–	k?	–
tedy	tedy	k8xC	tedy
možnosti	možnost	k1gFnSc2	možnost
proměny	proměna	k1gFnSc2	proměna
každého	každý	k3xTgInSc2	každý
živlu	živel	k1gInSc2	živel
v	v	k7c4	v
ostatní	ostatní	k2eAgInPc4d1	ostatní
tři	tři	k4xCgInPc4	tři
bez	bez	k7c2	bez
mezistupně	mezistupeň	k1gInSc2	mezistupeň
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
např.	např.	kA	např.
sublimaci	sublimace	k1gFnSc3	sublimace
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelův	Aristotelův	k2eAgMnSc1d1	Aristotelův
žák	žák	k1gMnSc1	žák
<g/>
,	,	kIx,	,
Theofrastos	Theofrastos	k1gMnSc1	Theofrastos
napsal	napsat	k5eAaBmAgMnS	napsat
o	o	k7c6	o
ohni	oheň	k1gInSc6	oheň
celý	celý	k2eAgInSc4d1	celý
spis	spis	k1gInSc4	spis
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
zlomky	zlomek	k1gInPc1	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
například	například	k6eAd1	například
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakého	jaký	k3yIgNnSc2	jaký
dřeva	dřevo	k1gNnSc2	dřevo
ze	z	k7c2	z
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
zápalná	zápalný	k2eAgNnPc4d1	zápalné
dřívka	dřívko	k1gNnPc4	dřívko
(	(	kIx(	(
<g/>
vrtidlo	vrtidlo	k1gNnSc1	vrtidlo
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
z	z	k7c2	z
vavřínu	vavřín	k1gInSc2	vavřín
nebo	nebo	k8xC	nebo
břečťanu	břečťan	k1gInSc2	břečťan
a	a	k8xC	a
lůžko	lůžko	k1gNnSc1	lůžko
třeba	třeba	k6eAd1	třeba
z	z	k7c2	z
lípy	lípa	k1gFnSc2	lípa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
staří	starý	k2eAgMnPc1d1	starý
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
oheň	oheň	k1gInSc4	oheň
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
světlo	světlo	k1gNnSc1	světlo
nebo	nebo	k8xC	nebo
teplo	teplo	k1gNnSc1	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ohně	oheň	k1gInSc2	oheň
jsou	být	k5eAaImIp3nP	být
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc1	oheň
je	být	k5eAaImIp3nS	být
záhadně	záhadně	k6eAd1	záhadně
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
pazourku	pazourek	k1gInSc6	pazourek
a	a	k8xC	a
křemeni	křemen	k1gInSc6	křemen
<g/>
,	,	kIx,	,
blesk	blesk	k1gInSc1	blesk
je	být	k5eAaImIp3nS	být
úžasným	úžasný	k2eAgInSc7d1	úžasný
případem	případ	k1gInSc7	případ
ohně	oheň	k1gInSc2	oheň
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
činné	činný	k2eAgFnPc1d1	činná
sopky	sopka	k1gFnPc1	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Fascinace	fascinace	k1gFnSc1	fascinace
ohněm	oheň	k1gInSc7	oheň
přetrvala	přetrvat	k5eAaPmAgFnS	přetrvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
domorodých	domorodý	k2eAgInPc2d1	domorodý
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
nejen	nejen	k6eAd1	nejen
jeho	jeho	k3xOp3gNnSc7	jeho
praktickým	praktický	k2eAgNnSc7d1	praktické
využitím	využití	k1gNnSc7	využití
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jako	jako	k8xS	jako
formou	forma	k1gFnSc7	forma
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
vyjádření	vyjádření	k1gNnSc2	vyjádření
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
fenoménem	fenomén	k1gInSc7	fenomén
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
napříč	napříč	k7c7	napříč
kulturami	kultura	k1gFnPc7	kultura
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
často	často	k6eAd1	často
tančí	tančit	k5eAaImIp3nP	tančit
břišní	břišní	k2eAgFnPc1d1	břišní
tanečnice	tanečnice	k1gFnPc1	tanečnice
<g/>
,	,	kIx,	,
ohnivé	ohnivý	k2eAgInPc1d1	ohnivý
efekty	efekt	k1gInPc1	efekt
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
hudebních	hudební	k2eAgInPc6d1	hudební
koncertech	koncert	k1gInPc6	koncert
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
potěšíme	potěšit	k5eAaPmIp1nP	potěšit
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
vzdušné	vzdušný	k2eAgNnSc4d1	vzdušné
ohnivé	ohnivý	k2eAgNnSc4d1	ohnivé
představení	představení	k1gNnSc4	představení
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ohňostroje	ohňostroj	k1gInSc2	ohňostroj
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
nalézá	nalézat	k5eAaImIp3nS	nalézat
potěšení	potěšení	k1gNnSc4	potěšení
v	v	k7c6	v
hrátkách	hrátky	k1gFnPc6	hrátky
s	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
zrovna	zrovna	k6eAd1	zrovna
žháři	žhář	k1gMnPc1	žhář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
existují	existovat	k5eAaImIp3nP	existovat
amatérské	amatérský	k2eAgInPc1d1	amatérský
i	i	k8xC	i
profesionální	profesionální	k2eAgInPc1d1	profesionální
spolky	spolek	k1gInPc1	spolek
představující	představující	k2eAgFnSc1d1	představující
své	svůj	k3xOyFgNnSc4	svůj
umění	umění	k1gNnSc4	umění
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
Ohnivým	ohnivý	k2eAgNnSc7d1	ohnivé
představením	představení	k1gNnSc7	představení
(	(	kIx(	(
<g/>
fireshow	fireshow	k?	fireshow
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejinak	nejinak	k6eAd1	nejinak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vystoupení	vystoupení	k1gNnSc4	vystoupení
skupiny	skupina	k1gFnSc2	skupina
Pa	Pa	kA	Pa
<g/>
-li	i	k?	-li
<g/>
-Tchi	-Tch	k1gFnSc2	-Tch
nebo	nebo	k8xC	nebo
Pyroterra	Pyroterro	k1gNnSc2	Pyroterro
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
Vypalování	vypalování	k1gNnSc2	vypalování
porostů	porost	k1gInPc2	porost
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
oheň	oheň	k1gInSc4	oheň
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
oheň	oheň	k1gInSc4	oheň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
oheň	oheň	k1gInSc1	oheň
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Oheň	oheň	k1gInSc1	oheň
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
