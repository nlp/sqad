<s>
Alberto	Alberta	k1gFnSc5	Alberta
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Frič	Frič	k1gMnSc1	Frič
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1882	[number]	k4	1882
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
etnograf	etnograf	k1gMnSc1	etnograf
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Jihoameričtí	jihoamerický	k2eAgMnPc1d1	jihoamerický
indiáni	indián	k1gMnPc1	indián
jej	on	k3xPp3gMnSc4	on
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
Karaí	Karaí	k2eAgMnPc1d1	Karaí
Pukú	Pukú	k1gMnPc1	Pukú
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
lovec	lovec	k1gMnSc1	lovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
též	též	k9	též
jako	jako	k9	jako
Lovec	lovec	k1gMnSc1	lovec
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
.	.	kIx.	.
</s>
<s>
Frič	Frič	k1gMnSc1	Frič
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
právníka	právník	k1gMnSc2	právník
a	a	k8xC	a
náměstka	náměstek	k1gMnSc2	náměstek
pražského	pražský	k2eAgMnSc2d1	pražský
primátora	primátor	k1gMnSc2	primátor
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Friče	Frič	k1gMnSc2	Frič
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
nadání	nadání	k1gNnSc4	nadání
v	v	k7c6	v
přírodovědných	přírodovědný	k2eAgInPc6d1	přírodovědný
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ho	on	k3xPp3gNnSc4	on
zajímaly	zajímat	k5eAaImAgFnP	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
malý	malý	k2eAgMnSc1d1	malý
kluk	kluk	k1gMnSc1	kluk
si	se	k3xPyFc3	se
přinesl	přinést	k5eAaPmAgInS	přinést
domů	domů	k6eAd1	domů
kaktus	kaktus	k1gInSc1	kaktus
Echinopsis	Echinopsis	k1gFnSc2	Echinopsis
eyriesii	eyriesie	k1gFnSc4	eyriesie
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jen	jen	k9	jen
jako	jako	k9	jako
rekvizitu	rekvizita	k1gFnSc4	rekvizita
pro	pro	k7c4	pro
pomstu	pomsta	k1gFnSc4	pomsta
na	na	k7c6	na
strážníkovi	strážník	k1gMnSc6	strážník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
vždycky	vždycky	k6eAd1	vždycky
šacoval	šacovat	k5eAaImAgMnS	šacovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nepřenáší	přenášet	k5eNaImIp3nS	přenášet
vejce	vejce	k1gNnSc1	vejce
bez	bez	k7c2	bez
zaplacení	zaplacení	k1gNnSc2	zaplacení
patřičných	patřičný	k2eAgInPc2d1	patřičný
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Nalézt	nalézt	k5eAaPmF	nalézt
po	po	k7c6	po
hmatu	hmat	k1gInSc6	hmat
velmi	velmi	k6eAd1	velmi
pichlavý	pichlavý	k2eAgInSc1d1	pichlavý
kaktus	kaktus	k1gInSc1	kaktus
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
strážníka	strážník	k1gMnSc4	strážník
velmi	velmi	k6eAd1	velmi
bolestivá	bolestivý	k2eAgFnSc1d1	bolestivá
zkušenost	zkušenost	k1gFnSc1	zkušenost
a	a	k8xC	a
kaktus	kaktus	k1gInSc1	kaktus
tak	tak	k6eAd1	tak
splnil	splnit	k5eAaPmAgInS	splnit
svůj	svůj	k3xOyFgInSc4	svůj
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tedy	tedy	k8xC	tedy
dán	dát	k5eAaPmNgInS	dát
na	na	k7c4	na
okno	okno	k1gNnSc4	okno
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
ponecháván	ponechávat	k5eAaImNgMnS	ponechávat
bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
však	však	k9	však
vykvetl	vykvést	k5eAaPmAgInS	vykvést
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
upoutalo	upoutat	k5eAaPmAgNnS	upoutat
Fričovu	Fričův	k2eAgFnSc4d1	Fričova
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Z	z	k7c2	z
tak	tak	k6eAd1	tak
neforemné	foremný	k2eNgFnSc2d1	neforemná
<g/>
,	,	kIx,	,
nevzhledné	vzhledný	k2eNgFnSc2d1	nevzhledná
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
význam	význam	k1gInSc1	význam
jenom	jenom	k9	jenom
potud	potud	k6eAd1	potud
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
sloužila	sloužit	k5eAaImAgFnS	sloužit
mi	já	k3xPp1nSc3	já
za	za	k7c4	za
nástroj	nástroj	k1gInSc4	nástroj
odplaty	odplata	k1gFnSc2	odplata
<g/>
,	,	kIx,	,
vyvinovala	vyvinovat	k5eAaImAgFnS	vyvinovat
se	se	k3xPyFc4	se
krása	krása	k1gFnSc1	krása
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
jsem	být	k5eAaImIp1nS	být
dosud	dosud	k6eAd1	dosud
nepoznal	poznat	k5eNaPmAgMnS	poznat
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Nově	nově	k6eAd1	nově
objevený	objevený	k2eAgInSc4d1	objevený
svět	svět	k1gInSc4	svět
Friče	Frič	k1gMnSc2	Frič
nadchnul	nadchnout	k5eAaPmAgMnS	nadchnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
znalců	znalec	k1gMnPc2	znalec
kaktusů	kaktus	k1gInPc2	kaktus
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
odborníci	odborník	k1gMnPc1	odborník
evropského	evropský	k2eAgInSc2d1	evropský
formátu	formát	k1gInSc2	formát
mu	on	k3xPp3gInSc3	on
zasílali	zasílat	k5eAaImAgMnP	zasílat
žádosti	žádost	k1gFnPc4	žádost
o	o	k7c4	o
konzultace	konzultace	k1gFnPc4	konzultace
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
určování	určování	k1gNnSc6	určování
a	a	k8xC	a
zvali	zvát	k5eAaImAgMnP	zvát
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
vědecké	vědecký	k2eAgFnPc4d1	vědecká
konference	konference	k1gFnPc4	konference
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c6	o
kaktusech	kaktus	k1gInPc6	kaktus
samozřejmě	samozřejmě	k6eAd1	samozřejmě
sbíral	sbírat	k5eAaImAgInS	sbírat
také	také	k9	také
kaktusy	kaktus	k1gInPc1	kaktus
samotné	samotný	k2eAgInPc1d1	samotný
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
však	však	k9	však
celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
kolekce	kolekce	k1gFnSc1	kolekce
umrzla	umrznout	k5eAaPmAgFnS	umrznout
<g/>
.	.	kIx.	.
</s>
<s>
Frič	Frič	k1gMnSc1	Frič
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
založit	založit	k5eAaPmF	založit
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
z	z	k7c2	z
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
sesbírá	sesbírat	k5eAaPmIp3nS	sesbírat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1929	[number]	k4	1929
podnikl	podniknout	k5eAaPmAgMnS	podniknout
7	[number]	k4	7
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
soustředil	soustředit	k5eAaPmAgInS	soustředit
jednak	jednak	k8xC	jednak
na	na	k7c4	na
sběr	sběr	k1gInSc4	sběr
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
objevování	objevování	k1gNnSc1	objevování
nových	nový	k2eAgFnPc2d1	nová
končin	končina	k1gFnPc2	končina
a	a	k8xC	a
sběr	sběr	k1gInSc1	sběr
etnografických	etnografický	k2eAgInPc2d1	etnografický
materiálů	materiál	k1gInPc2	materiál
o	o	k7c6	o
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgInPc6d1	žijící
indiánských	indiánský	k2eAgInPc6d1	indiánský
kmenech	kmen	k1gInPc6	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
pořizoval	pořizovat	k5eAaImAgInS	pořizovat
botanické	botanický	k2eAgFnPc4d1	botanická
a	a	k8xC	a
etnografické	etnografický	k2eAgFnPc4d1	etnografická
fotografie	fotografia	k1gFnPc4	fotografia
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
průkopníka	průkopník	k1gMnSc4	průkopník
české	český	k2eAgFnSc2d1	Česká
krajinářské	krajinářský	k2eAgFnSc2d1	krajinářská
a	a	k8xC	a
cestopisné	cestopisný	k2eAgFnSc2d1	cestopisná
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
cestách	cesta	k1gFnPc6	cesta
strávil	strávit	k5eAaPmAgMnS	strávit
A.	A.	kA	A.
V.	V.	kA	V.
Frič	Frič	k1gMnSc1	Frič
téměř	téměř	k6eAd1	téměř
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Matto	Matta	k1gFnSc5	Matta
Grosso	Grossa	k1gFnSc5	Grossa
<g/>
,	,	kIx,	,
Paraná	Paraný	k2eAgFnSc1d1	Paraná
Cucura	Cucura	k1gFnSc1	Cucura
<g/>
,	,	kIx,	,
Šavantes	Šavantesa	k1gFnPc2	Šavantesa
Fričova	Fričův	k2eAgFnSc1d1	Fričova
první	první	k4xOgFnSc1	první
cesta	cesta	k1gFnSc1	cesta
vedla	vést	k5eAaImAgFnS	vést
do	do	k7c2	do
divokých	divoký	k2eAgFnPc2d1	divoká
končin	končina	k1gFnPc2	končina
Mato	Mato	k1gMnSc1	Mato
Grossa	Grossa	k?	Grossa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
indiány	indián	k1gMnPc7	indián
kmene	kmen	k1gInSc2	kmen
Šavantes	Šavantes	k1gMnSc1	Šavantes
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
předčasně	předčasně	k6eAd1	předčasně
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
utrpěl	utrpět	k5eAaPmAgInS	utrpět
strašlivá	strašlivý	k2eAgNnPc4d1	strašlivé
zranění	zranění	k1gNnPc4	zranění
při	při	k7c6	při
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
jaguárem	jaguár	k1gMnSc7	jaguár
<g/>
.	.	kIx.	.
</s>
<s>
Šelma	šelma	k1gFnSc1	šelma
Friče	Frič	k1gMnSc2	Frič
zaskočila	zaskočit	k5eAaPmAgFnS	zaskočit
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
ji	on	k3xPp3gFnSc4	on
stihl	stihnout	k5eAaPmAgMnS	stihnout
výstřelem	výstřel	k1gInSc7	výstřel
pouze	pouze	k6eAd1	pouze
zranit	zranit	k5eAaPmF	zranit
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
střetl	střetnout	k5eAaPmAgMnS	střetnout
tělo	tělo	k1gNnSc4	tělo
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
zvíře	zvíře	k1gNnSc4	zvíře
poté	poté	k6eAd1	poté
zabil	zabít	k5eAaPmAgMnS	zabít
nožem	nůž	k1gInSc7	nůž
nebo	nebo	k8xC	nebo
opožděně	opožděně	k6eAd1	opožděně
zaúčinkovalo	zaúčinkovat	k5eAaPmAgNnS	zaúčinkovat
předchozí	předchozí	k2eAgNnSc4d1	předchozí
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
následky	následek	k1gInPc1	následek
střetu	střet	k1gInSc2	střet
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
Friče	Frič	k1gMnPc4	Frič
velmi	velmi	k6eAd1	velmi
zlé	zlý	k2eAgNnSc1d1	zlé
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
ho	on	k3xPp3gNnSc4	on
celé	celý	k2eAgNnSc4d1	celé
týdny	týden	k1gInPc7	týden
léčili	léčit	k5eAaImAgMnP	léčit
indiáni	indián	k1gMnPc1	indián
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jakkoliv	jakkoliv	k8xS	jakkoliv
ale	ale	k8xC	ale
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
zážitek	zážitek	k1gInSc4	zážitek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
někdy	někdy	k6eAd1	někdy
přál	přát	k5eAaImAgMnS	přát
prožít	prožít	k5eAaPmF	prožít
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc2	on
znamenal	znamenat	k5eAaImAgInS	znamenat
požehnání	požehnání	k1gNnSc4	požehnání
<g/>
.	.	kIx.	.
</s>
<s>
Nestal	stát	k5eNaPmAgMnS	stát
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zabil	zabít	k5eAaPmAgMnS	zabít
jaguára	jaguár	k1gMnSc4	jaguár
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
už	už	k9	už
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
vyneslo	vynést	k5eAaPmAgNnS	vynést
velkou	velký	k2eAgFnSc4d1	velká
úctu	úcta	k1gFnSc4	úcta
a	a	k8xC	a
vážnost	vážnost	k1gFnSc4	vážnost
mezi	mezi	k7c4	mezi
indiány	indián	k1gMnPc4	indián
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
v	v	k7c6	v
objetí	objetí	k1gNnSc6	objetí
jaguára	jaguár	k1gMnSc2	jaguár
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgMnSc1	takový
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nepočítal	počítat	k5eNaImAgMnS	počítat
mezi	mezi	k7c4	mezi
obyčejné	obyčejný	k2eAgMnPc4d1	obyčejný
smrtelníky	smrtelník	k1gMnPc4	smrtelník
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
posvátná	posvátný	k2eAgFnSc1d1	posvátná
úcta	úcta	k1gFnSc1	úcta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
obestřen	obestřen	k2eAgInSc1d1	obestřen
<g/>
,	,	kIx,	,
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Fričovi	Fričův	k2eAgMnPc1d1	Fričův
později	pozdě	k6eAd2	pozdě
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
by	by	kYmCp3nS	by
jinak	jinak	k6eAd1	jinak
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nemohl	moct	k5eNaImAgMnS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Chaco	Chaco	k1gMnSc1	Chaco
<g/>
,	,	kIx,	,
Pilcomayo	Pilcomayo	k1gMnSc1	Pilcomayo
Anganité	Anganitý	k2eAgFnSc2d1	Anganitý
<g/>
,	,	kIx,	,
Bororó	Bororó	k1gMnSc1	Bororó
<g/>
,	,	kIx,	,
Karraim	Karraim	k1gMnSc1	Karraim
<g/>
,	,	kIx,	,
Pilagá	Pilagá	k1gFnSc1	Pilagá
<g/>
,	,	kIx,	,
Sanapaná	Sanapaný	k2eAgFnSc1d1	Sanapaný
<g/>
,	,	kIx,	,
Toba	Tob	k2eAgFnSc1d1	Toba
<g/>
,	,	kIx,	,
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
cestě	cesta	k1gFnSc6	cesta
Frič	Frič	k1gMnSc1	Frič
prozkoumal	prozkoumat	k5eAaPmAgMnS	prozkoumat
pro	pro	k7c4	pro
paraguayskou	paraguayský	k2eAgFnSc4d1	paraguayská
vládu	vláda	k1gFnSc4	vláda
tok	tok	k1gInSc4	tok
řeky	řeka	k1gFnSc2	řeka
Pilcomayo	Pilcomayo	k6eAd1	Pilcomayo
a	a	k8xC	a
proplul	proplout	k5eAaPmAgMnS	proplout
jím	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
žádné	žádný	k3yNgNnSc1	žádný
z	z	k7c2	z
předcházejících	předcházející	k2eAgNnPc2d1	předcházející
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
expedic	expedice	k1gFnPc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
též	též	k9	též
objevil	objevit	k5eAaPmAgInS	objevit
hrob	hrob	k1gInSc1	hrob
španělského	španělský	k2eAgMnSc2d1	španělský
geodeta	geodet	k1gMnSc2	geodet
Ibarrety	Ibarreta	k1gFnSc2	Ibarreta
(	(	kIx(	(
<g/>
†	†	k?	†
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
zabili	zabít	k5eAaPmAgMnP	zabít
indiáni	indián	k1gMnPc1	indián
v	v	k7c6	v
rozmíšce	rozmíška	k1gFnSc6	rozmíška
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
svým	svůj	k3xOyFgNnSc7	svůj
neuváženým	uvážený	k2eNgNnSc7d1	neuvážené
chováním	chování	k1gNnSc7	chování
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
výpravě	výprava	k1gFnSc6	výprava
strávil	strávit	k5eAaPmAgMnS	strávit
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
u	u	k7c2	u
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Čamakoko	Čamakoko	k1gNnSc1	Čamakoko
(	(	kIx(	(
<g/>
Chamacoco	Chamacoco	k1gNnSc1	Chamacoco
<g/>
)	)	kIx)	)
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Paraguay	Paraguay	k1gFnSc1	Paraguay
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
po	po	k7c6	po
indiánském	indiánský	k2eAgInSc6d1	indiánský
způsobu	způsob	k1gInSc6	způsob
s	s	k7c7	s
Indiánkou	Indiánka	k1gFnSc7	Indiánka
Lora-y	Lora	k1gFnSc2	Lora-a
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Černá	černý	k2eAgFnSc1d1	černá
kachna	kachna	k1gFnSc1	kachna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
svazku	svazek	k1gInSc2	svazek
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Herminia	Herminium	k1gNnSc2	Herminium
(	(	kIx(	(
<g/>
*	*	kIx~	*
asi	asi	k9	asi
v	v	k7c6	v
září	září	k1gNnSc6	září
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odjel	odjet	k5eAaPmAgMnS	odjet
domů	domů	k6eAd1	domů
(	(	kIx(	(
<g/>
léto	léto	k1gNnSc4	léto
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
už	už	k9	už
zřejmě	zřejmě	k6eAd1	zřejmě
nesetkal	setkat	k5eNaPmAgMnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Paraguay	Paraguay	k1gFnSc1	Paraguay
Čamakoko	Čamakoko	k1gNnSc1	Čamakoko
<g/>
,	,	kIx,	,
Kainganu	Kaingan	k1gInSc2	Kaingan
<g/>
,	,	kIx,	,
Ššeta-Kuruton	Ššeta-Kuruton	k1gInSc1	Ššeta-Kuruton
<g/>
,	,	kIx,	,
Pehunče	Pehunče	k1gNnSc1	Pehunče
<g/>
,	,	kIx,	,
Pikunče	Pikunče	k1gNnSc1	Pikunče
<g/>
,	,	kIx,	,
Pulche	Pulche	k1gNnSc1	Pulche
<g/>
,	,	kIx,	,
Rankelče	Rankelec	k1gMnSc5	Rankelec
<g/>
,	,	kIx,	,
Tehuelche	Tehuelche	k1gNnPc1	Tehuelche
Ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
třetí	třetí	k4xOgFnSc2	třetí
cesty	cesta	k1gFnSc2	cesta
si	se	k3xPyFc3	se
přivezl	přivézt	k5eAaPmAgMnS	přivézt
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
Čerwuiše	Čerwuiš	k1gInSc2	Čerwuiš
<g/>
,	,	kIx,	,
indiána	indián	k1gMnSc4	indián
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Čamakoko	Čamakoko	k1gNnSc1	Čamakoko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
záhadná	záhadný	k2eAgFnSc1d1	záhadná
nemoc	nemoc	k1gFnSc1	nemoc
decimující	decimující	k2eAgFnSc2d1	decimující
Čerwuiše	Čerwuiše	k1gFnSc2	Čerwuiše
i	i	k8xC	i
jeho	jeho	k3xOp3gInSc1	jeho
kmen	kmen	k1gInSc1	kmen
a	a	k8xC	a
Frič	Frič	k1gMnSc1	Frič
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gInSc4	on
prohlédli	prohlédnout	k5eAaPmAgMnP	prohlédnout
lékaři	lékař	k1gMnPc1	lékař
specialisté	specialista	k1gMnPc1	specialista
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
nemoc	nemoc	k1gFnSc4	nemoc
objasnil	objasnit	k5eAaPmAgMnS	objasnit
student	student	k1gMnSc1	student
mediciny	medicin	k2eAgFnSc2d1	medicina
oboru	obor	k1gInSc6	obor
parazitologie	parazitologie	k1gFnSc2	parazitologie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
přišel	přijít	k5eAaPmAgInS	přijít
uctivě	uctivě	k6eAd1	uctivě
vyprosit	vyprosit	k5eAaPmF	vyprosit
vzorek	vzorek	k1gInSc4	vzorek
fekálií	fekálie	k1gFnPc2	fekálie
od	od	k7c2	od
Fričova	Fričův	k2eAgMnSc2d1	Fričův
indiánského	indiánský	k2eAgMnSc2d1	indiánský
hosta	host	k1gMnSc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc4	nemoc
způsobili	způsobit	k5eAaPmAgMnP	způsobit
dosud	dosud	k6eAd1	dosud
u	u	k7c2	u
Indiánů	Indián	k1gMnPc2	Indián
neznámí	známit	k5eNaImIp3nP	známit
střevní	střevní	k2eAgMnPc1d1	střevní
parazité	parazit	k1gMnPc1	parazit
(	(	kIx(	(
<g/>
Ancylostoma	Ancylostoma	k1gFnSc1	Ancylostoma
duodenale	duodenale	k6eAd1	duodenale
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
stačilo	stačit	k5eAaBmAgNnS	stačit
použít	použít	k5eAaPmF	použít
silné	silný	k2eAgNnSc4d1	silné
projímadlo	projímadlo	k1gNnSc4	projímadlo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Čerwuiš	Čerwuiš	k1gInSc1	Čerwuiš
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třetí	třetí	k4xOgFnSc2	třetí
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
rovněž	rovněž	k9	rovněž
podařilo	podařit	k5eAaPmAgNnS	podařit
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
pozůstalost	pozůstalost	k1gFnSc4	pozůstalost
po	po	k7c6	po
italském	italský	k2eAgMnSc6d1	italský
fotografovi	fotograf	k1gMnSc6	fotograf
Guido	Guido	k1gNnSc4	Guido
Boggianim	Boggianim	k1gMnSc1	Boggianim
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
nejasných	jasný	k2eNgFnPc2d1	nejasná
okolností	okolnost	k1gFnPc2	okolnost
zabit	zabit	k2eAgInSc1d1	zabit
indiány	indián	k1gMnPc7	indián
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kaktusů	kaktus	k1gInPc2	kaktus
bylo	být	k5eAaImAgNnS	být
významným	významný	k2eAgInSc7d1	významný
objevem	objev	k1gInSc7	objev
Gymnocalycium	Gymnocalycium	k1gNnSc4	Gymnocalycium
mihanovichii	mihanovichie	k1gFnSc4	mihanovichie
<g/>
.	.	kIx.	.
</s>
<s>
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
Čamakoko	Čamakoko	k1gNnSc1	Čamakoko
<g/>
,	,	kIx,	,
Kaďuvej	Kaďuvej	k1gFnSc1	Kaďuvej
Začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
a	a	k8xC	a
Frič	Frič	k1gMnSc1	Frič
během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
dopravil	dopravit	k5eAaPmAgMnS	dopravit
Čerwuiše	Čerwuiše	k1gFnPc4	Čerwuiše
a	a	k8xC	a
patřičné	patřičný	k2eAgInPc4d1	patřičný
léky	lék	k1gInPc4	lék
ke	k	k7c3	k
kmenu	kmen	k1gInSc3	kmen
Čamakoko	Čamakoko	k1gNnSc4	Čamakoko
(	(	kIx(	(
<g/>
Chamacoco	Chamacoco	k6eAd1	Chamacoco
<g/>
)	)	kIx)	)
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Gran	Gran	k1gMnSc1	Gran
Chaca	Chaca	k1gMnSc1	Chaca
v	v	k7c6	v
Paraguayi	Paraguay	k1gFnSc6	Paraguay
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
datování	datování	k1gNnSc1	datování
cesty	cesta	k1gFnSc2	cesta
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
cesty	cesta	k1gFnSc2	cesta
Frič	Frič	k1gMnSc1	Frič
dopravil	dopravit	k5eAaPmAgMnS	dopravit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
3	[number]	k4	3
živé	živý	k2eAgInPc1d1	živý
exempláře	exemplář	k1gInPc1	exemplář
bahníků	bahník	k1gInPc2	bahník
<g/>
.	.	kIx.	.
</s>
<s>
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Tarahumara	Tarahumara	k1gFnSc1	Tarahumara
Jediná	jediný	k2eAgFnSc1d1	jediná
cesta	cesta	k1gFnSc1	cesta
po	po	k7c6	po
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
významem	význam	k1gInSc7	význam
však	však	k9	však
vydala	vydat	k5eAaPmAgFnS	vydat
za	za	k7c4	za
ostatní	ostatní	k2eAgFnPc4d1	ostatní
cesty	cesta	k1gFnPc4	cesta
po	po	k7c6	po
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
,	,	kIx,	,
Peru	prát	k5eAaImIp1nS	prát
Tato	tento	k3xDgFnSc1	tento
poslední	poslední	k2eAgFnSc1d1	poslední
cesta	cesta	k1gFnSc1	cesta
po	po	k7c6	po
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kaktusářství	kaktusářství	k1gNnPc2	kaktusářství
nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Frič	Frič	k1gMnSc1	Frič
objevil	objevit	k5eAaPmAgMnS	objevit
nová	nový	k2eAgNnPc4d1	nové
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neznámá	známý	k2eNgNnPc4d1	neznámé
naleziště	naleziště	k1gNnPc4	naleziště
vysokohorských	vysokohorský	k2eAgInPc2d1	vysokohorský
kaktusů	kaktus	k1gInPc2	kaktus
rodů	rod	k1gInPc2	rod
Parodia	Parodium	k1gNnSc2	Parodium
<g/>
,	,	kIx,	,
Lobivia	Lobivium	k1gNnSc2	Lobivium
<g/>
,	,	kIx,	,
Rebutia	Rebutium	k1gNnSc2	Rebutium
<g/>
,	,	kIx,	,
Neowerdermania	Neowerdermanium	k1gNnSc2	Neowerdermanium
<g/>
,	,	kIx,	,
Oreocereus	Oreocereus	k1gMnSc1	Oreocereus
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
a	a	k8xC	a
spustil	spustit	k5eAaPmAgInS	spustit
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
zájem	zájem	k1gInSc1	zájem
až	až	k9	až
horečku	horečka	k1gFnSc4	horečka
o	o	k7c4	o
jihoamerické	jihoamerický	k2eAgInPc4d1	jihoamerický
kaktusy	kaktus	k1gInPc4	kaktus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
5	[number]	k4	5
<g/>
.	.	kIx.	.
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
Frič	Frič	k1gMnSc1	Frič
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Dragou	Draga	k1gFnSc7	Draga
Janáčkovou	Janáčková	k1gFnSc7	Janáčková
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc4	syn
Ivana	Ivan	k1gMnSc4	Ivan
Vojtěcha	Vojtěch	k1gMnSc4	Vojtěch
(	(	kIx(	(
<g/>
*	*	kIx~	*
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
kameramana	kameraman	k1gMnSc2	kameraman
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Pavel	Pavel	k1gMnSc1	Pavel
Frič	Frič	k1gMnSc1	Frič
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Yvonnou	Yvonna	k1gFnSc7	Yvonna
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
zvýšit	zvýšit	k5eAaPmF	zvýšit
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c4	o
A.	A.	kA	A.
V.	V.	kA	V.
Fričovi	Fričův	k2eAgMnPc1d1	Fričův
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
dost	dost	k6eAd1	dost
malé	malý	k2eAgNnSc1d1	malé
<g/>
.	.	kIx.	.
</s>
<s>
Početné	početný	k2eAgNnSc4d1	početné
potomstvo	potomstvo	k1gNnSc4	potomstvo
(	(	kIx(	(
<g/>
8	[number]	k4	8
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
po	po	k7c6	po
sobě	se	k3xPyFc3	se
zanechala	zanechat	k5eAaPmAgFnS	zanechat
i	i	k8xC	i
Fričova	Fričův	k2eAgFnSc1d1	Fričova
jihoamerická	jihoamerický	k2eAgFnSc1d1	jihoamerická
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
odjezdu	odjezd	k1gInSc6	odjezd
předal	předat	k5eAaPmAgMnS	předat
do	do	k7c2	do
péče	péče	k1gFnSc2	péče
náčelníka	náčelník	k1gMnSc2	náčelník
Magpioty	Magpiota	k1gFnSc2	Magpiota
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
synové	syn	k1gMnPc1	syn
hrdě	hrdě	k6eAd1	hrdě
nosí	nosit	k5eAaImIp3nP	nosit
jméno	jméno	k1gNnSc4	jméno
Fric	Fric	k1gMnSc1	Fric
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
větve	větev	k1gFnPc1	větev
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
o	o	k7c6	o
sobě	se	k3xPyFc3	se
dozvěděly	dozvědět	k5eAaPmAgFnP	dozvědět
až	až	k8xS	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Hermínu	Hermína	k1gFnSc4	Hermína
objevili	objevit	k5eAaPmAgMnP	objevit
čeští	český	k2eAgMnPc1d1	český
filmaři	filmař	k1gMnPc1	filmař
Alice	Alice	k1gFnSc1	Alice
Růžičková	Růžičková	k1gFnSc1	Růžičková
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
Číhák	Číhák	k1gMnSc1	Číhák
putující	putující	k2eAgMnSc1d1	putující
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
Čerwuiše	Čerwuiše	k1gFnSc2	Čerwuiše
<g/>
.	.	kIx.	.
</s>
<s>
Frič	Frič	k1gMnSc1	Frič
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
na	na	k7c4	na
odborníka	odborník	k1gMnSc4	odborník
světového	světový	k2eAgInSc2d1	světový
formátu	formát	k1gInSc2	formát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
soudobého	soudobý	k2eAgMnSc4d1	soudobý
znalce	znalec	k1gMnSc4	znalec
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
desítky	desítka	k1gFnPc4	desítka
nových	nový	k2eAgMnPc2d1	nový
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
největší	veliký	k2eAgFnSc4d3	veliký
a	a	k8xC	a
nejcennější	cenný	k2eAgFnSc4d3	nejcennější
evropskou	evropský	k2eAgFnSc4d1	Evropská
sbírku	sbírka	k1gFnSc4	sbírka
kaktusů	kaktus	k1gInPc2	kaktus
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
přes	přes	k7c4	přes
všechnu	všechen	k3xTgFnSc4	všechen
snahu	snaha	k1gFnSc4	snaha
nepodařilo	podařit	k5eNaPmAgNnS	podařit
sehnat	sehnat	k5eAaPmF	sehnat
dost	dost	k6eAd1	dost
topiva	topivo	k1gNnPc4	topivo
a	a	k8xC	a
kaktusy	kaktus	k1gInPc4	kaktus
ve	v	k7c6	v
sklenících	skleník	k1gInPc6	skleník
umrzly	umrznout	k5eAaPmAgFnP	umrznout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
–	–	k?	–
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
mu	on	k3xPp3gMnSc3	on
čeští	český	k2eAgMnPc1d1	český
celníci	celník	k1gMnPc1	celník
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
nechali	nechat	k5eAaPmAgMnP	nechat
kaktusy	kaktus	k1gInPc4	kaktus
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
krabic	krabice	k1gFnPc2	krabice
zasypat	zasypat	k5eAaPmF	zasypat
sněhem	sníh	k1gInSc7	sníh
a	a	k8xC	a
umrznout	umrznout	k5eAaPmF	umrznout
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sbíral	sbírat	k5eAaImAgMnS	sbírat
je	být	k5eAaImIp3nS	být
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
od	od	k7c2	od
Mato	Mato	k1gMnSc1	Mato
Grossa	Grossa	k?	Grossa
přes	přes	k7c4	přes
Gran	Gran	k1gInSc4	Gran
Chaco	Chaco	k6eAd1	Chaco
až	až	k9	až
po	po	k7c4	po
strmé	strmý	k2eAgFnPc4d1	strmá
stráně	stráň	k1gFnPc4	stráň
And	Anda	k1gFnPc2	Anda
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
přes	přes	k7c4	přes
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Navštívil	navštívit	k5eAaPmAgInS	navštívit
kvůli	kvůli	k7c3	kvůli
nim	on	k3xPp3gMnPc3	on
i	i	k9	i
Střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
fotografií	fotografia	k1gFnPc2	fotografia
krajiny	krajina	k1gFnSc2	krajina
porostlé	porostlý	k2eAgInPc1d1	porostlý
kaktusy	kaktus	k1gInPc1	kaktus
a	a	k8xC	a
zpopularizoval	zpopularizovat	k5eAaPmAgInS	zpopularizovat
pěstování	pěstování	k1gNnSc4	pěstování
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Odborníky	odborník	k1gMnPc4	odborník
fascinoval	fascinovat	k5eAaBmAgMnS	fascinovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
botanické	botanický	k2eAgFnSc6d1	botanická
zahradě	zahrada	k1gFnSc6	zahrada
umístěné	umístěný	k2eAgFnSc2d1	umístěná
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Mexika	Mexiko	k1gNnSc2	Mexiko
objevil	objevit	k5eAaPmAgInS	objevit
druh	druh	k1gInSc1	druh
Astrophytum	Astrophytum	k1gNnSc1	Astrophytum
asterias	asterias	k1gInSc1	asterias
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
paradoxnější	paradoxní	k2eAgFnSc1d2	paradoxnější
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahradu	zahrada	k1gFnSc4	zahrada
odborníci	odborník	k1gMnPc1	odborník
na	na	k7c4	na
kaktusy	kaktus	k1gInPc4	kaktus
často	často	k6eAd1	často
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prováděl	provádět	k5eAaImAgInS	provádět
výzkum	výzkum	k1gInSc1	výzkum
kaktusů	kaktus	k1gInPc2	kaktus
s	s	k7c7	s
narkotizačními	narkotizační	k2eAgInPc7d1	narkotizační
účinky	účinek	k1gInPc7	účinek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
narkokaktusy	narkokaktus	k1gInPc1	narkokaktus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
potlačují	potlačovat	k5eAaImIp3nP	potlačovat
závrať	závrať	k1gFnSc4	závrať
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
využíval	využívat	k5eAaImAgInS	využívat
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
objevy	objev	k1gInPc1	objev
byly	být	k5eAaImAgInP	být
obrovské	obrovský	k2eAgInPc4d1	obrovský
<g/>
,	,	kIx,	,
horší	horšit	k5eAaImIp3nS	horšit
to	ten	k3xDgNnSc1	ten
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
publikací	publikace	k1gFnSc7	publikace
výsledků	výsledek	k1gInPc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Friče	Frič	k1gMnPc4	Frič
nijak	nijak	k6eAd1	nijak
nelákala	lákat	k5eNaImAgFnS	lákat
ani	ani	k8xC	ani
tvorba	tvorba	k1gFnSc1	tvorba
strohých	strohý	k2eAgFnPc2d1	strohá
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
taxonomie	taxonomie	k1gFnSc1	taxonomie
jako	jako	k8xS	jako
striktní	striktní	k2eAgFnSc1d1	striktní
věda	věda	k1gFnSc1	věda
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
názvy	název	k1gInPc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
spousta	spousta	k1gFnSc1	spousta
jím	jíst	k5eAaImIp1nS	jíst
objevených	objevený	k2eAgInPc2d1	objevený
druhů	druh	k1gInPc2	druh
byla	být	k5eAaImAgNnP	být
nakonec	nakonec	k6eAd1	nakonec
přisouzena	přisoudit	k5eAaPmNgNnP	přisoudit
jiným	jiné	k1gNnSc7	jiné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
nepřiměl	přimět	k5eNaPmAgMnS	přimět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
podrobně	podrobně	k6eAd1	podrobně
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
vědecky	vědecky	k6eAd1	vědecky
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediné	k1gNnSc1	jediné
větší	veliký	k2eAgFnSc2d2	veliký
botanické	botanický	k2eAgFnSc2d1	botanická
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
snesou	snést	k5eAaPmIp3nP	snést
nejpřísnější	přísný	k2eAgNnPc4d3	nejpřísnější
měřítka	měřítko	k1gNnPc4	měřítko
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
myšleno	myslet	k5eAaImNgNnS	myslet
po	po	k7c6	po
formální	formální	k2eAgFnSc6d1	formální
stránce	stránka	k1gFnSc6	stránka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInSc4	jeho
Index	index	k1gInSc4	index
kaktusů	kaktus	k1gInPc2	kaktus
a	a	k8xC	a
unikátní	unikátní	k2eAgInSc1d1	unikátní
herbář	herbář	k1gInSc1	herbář
o	o	k7c6	o
několika	několik	k4yIc6	několik
stech	sto	k4xCgNnPc6	sto
položkách	položka	k1gFnPc6	položka
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
svých	svůj	k3xOyFgFnPc2	svůj
výprav	výprava	k1gFnPc2	výprava
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
jednak	jednak	k8xC	jednak
pěstěním	pěstění	k1gNnSc7	pěstění
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
šlechtěním	šlechtění	k1gNnSc7	šlechtění
a	a	k8xC	a
křížením	křížení	k1gNnSc7	křížení
exotických	exotický	k2eAgFnPc2d1	exotická
plodin	plodina	k1gFnPc2	plodina
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
velkých	velký	k2eAgInPc2d1	velký
úspěchů	úspěch	k1gInPc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
u	u	k7c2	u
rajčat	rajče	k1gNnPc2	rajče
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Fričovi	Frič	k1gMnSc6	Frič
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
časopis	časopis	k1gInSc1	časopis
Friciana	Frician	k1gMnSc2	Frician
<g/>
.	.	kIx.	.
</s>
<s>
Frič	Frič	k1gMnSc1	Frič
během	během	k7c2	během
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
navštívil	navštívit	k5eAaPmAgInS	navštívit
desítky	desítka	k1gFnPc4	desítka
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
sestavil	sestavit	k5eAaPmAgInS	sestavit
slovníky	slovník	k1gInPc4	slovník
36	[number]	k4	36
indiánských	indiánský	k2eAgMnPc2d1	indiánský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pořídil	pořídit	k5eAaPmAgMnS	pořídit
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
stovky	stovka	k1gFnPc1	stovka
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
přivezl	přivézt	k5eAaPmAgMnS	přivézt
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
tisíce	tisíc	k4xCgInPc4	tisíc
upomínkových	upomínkový	k2eAgMnPc2d1	upomínkový
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jediné	jediný	k2eAgInPc4d1	jediný
podrobné	podrobný	k2eAgInPc4d1	podrobný
materiály	materiál	k1gInPc4	materiál
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tito	tento	k3xDgMnPc1	tento
indiáni	indián	k1gMnPc1	indián
žili	žít	k5eAaImAgMnP	žít
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
byli	být	k5eAaImAgMnP	být
zásadněji	zásadně	k6eAd2	zásadně
ovlivněni	ovlivněn	k2eAgMnPc1d1	ovlivněn
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vybiti	vybit	k2eAgMnPc1d1	vybit
<g/>
)	)	kIx)	)
bělochy	běloch	k1gMnPc7	běloch
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
prožil	prožít	k5eAaPmAgMnS	prožít
asi	asi	k9	asi
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
to	ten	k3xDgNnSc4	ten
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
nádherné	nádherný	k2eAgFnPc1d1	nádherná
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jaguára	jaguár	k1gMnSc4	jaguár
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
nehody	nehoda	k1gFnPc4	nehoda
a	a	k8xC	a
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ho	on	k3xPp3gNnSc4	on
potkaly	potkat	k5eAaPmAgFnP	potkat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
času	čas	k1gInSc2	čas
strávil	strávit	k5eAaPmAgMnS	strávit
u	u	k7c2	u
kmene	kmen	k1gInSc2	kmen
Čamakoko	Čamakoko	k1gNnSc1	Čamakoko
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
pocházela	pocházet	k5eAaImAgFnS	pocházet
jeho	jeho	k3xOp3gFnSc1	jeho
indiánská	indiánský	k2eAgFnSc1d1	indiánská
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Frič	Frič	k1gMnSc1	Frič
později	pozdě	k6eAd2	pozdě
velice	velice	k6eAd1	velice
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
pobyty	pobyt	k1gInPc4	pobyt
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
a	a	k8xC	a
dával	dávat	k5eAaImAgMnS	dávat
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
cítil	cítit	k5eAaImAgMnS	cítit
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
pokrytecké	pokrytecký	k2eAgFnSc3d1	pokrytecká
a	a	k8xC	a
maloměšťácké	maloměšťácký	k2eAgFnSc3d1	maloměšťácká
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
indiánem	indián	k1gMnSc7	indián
<g/>
,	,	kIx,	,
oblékal	oblékat	k5eAaImAgMnS	oblékat
se	se	k3xPyFc4	se
jako	jako	k9	jako
oni	onen	k3xDgMnPc1	onen
a	a	k8xC	a
choval	chovat	k5eAaImAgMnS	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
oni	onen	k3xDgMnPc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Šel	jít	k5eAaImAgMnS	jít
<g/>
-li	i	k?	-li
jsem	být	k5eAaImIp1nS	být
navštívit	navštívit	k5eAaPmF	navštívit
ministra	ministr	k1gMnSc4	ministr
nebo	nebo	k8xC	nebo
prezidenta	prezident	k1gMnSc2	prezident
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
oblékl	obléct	k5eAaPmAgMnS	obléct
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
frak	frak	k1gInSc4	frak
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
<g/>
-li	i	k?	-li
ale	ale	k8xC	ale
cestovatel	cestovatel	k1gMnSc1	cestovatel
pochopit	pochopit	k5eAaPmF	pochopit
člověka	člověk	k1gMnSc4	člověk
divočiny	divočina	k1gFnSc2	divočina
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
odložit	odložit	k5eAaPmF	odložit
šaty	šat	k1gInPc4	šat
<g/>
,	,	kIx,	,
oholit	oholit	k5eAaPmF	oholit
brvy	brva	k1gFnPc4	brva
a	a	k8xC	a
obočí	obočí	k1gNnSc4	obočí
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
jako	jako	k9	jako
kaktusář	kaktusář	k1gMnSc1	kaktusář
byl	být	k5eAaImAgMnS	být
Frič	Frič	k1gMnSc1	Frič
ctěn	ctěn	k2eAgMnSc1d1	ctěn
doma	doma	k6eAd1	doma
i	i	k8xC	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
amatérský	amatérský	k2eAgMnSc1d1	amatérský
etnograf	etnograf	k1gMnSc1	etnograf
a	a	k8xC	a
znalec	znalec	k1gMnSc1	znalec
Indiánů	Indián	k1gMnPc2	Indián
získal	získat	k5eAaPmAgMnS	získat
popularitu	popularita	k1gFnSc4	popularita
zejména	zejména	k9	zejména
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byl	být	k5eAaImAgInS	být
přehlížen	přehlížet	k5eAaImNgInS	přehlížet
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
sbírek	sbírka	k1gFnPc2	sbírka
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
dodavatelem	dodavatel	k1gMnSc7	dodavatel
tamního	tamní	k2eAgNnSc2d1	tamní
Muzea	muzeum	k1gNnSc2	muzeum
antropologie	antropologie	k1gFnSc2	antropologie
a	a	k8xC	a
etnografie	etnografie	k1gFnSc2	etnografie
a	a	k8xC	a
členem-korespondentem	členemorespondent	k1gMnSc7	členem-korespondent
Ruské	ruský	k2eAgFnSc2d1	ruská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
Náprstkově	náprstkově	k6eAd1	náprstkově
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
odbornějších	odborný	k2eAgFnPc2d2	odbornější
prací	práce	k1gFnPc2	práce
o	o	k7c6	o
indiánech	indián	k1gMnPc6	indián
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
(	(	kIx(	(
<g/>
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
či	či	k8xC	či
v	v	k7c6	v
jihoamerických	jihoamerický	k2eAgFnPc6d1	jihoamerická
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
odborné	odborný	k2eAgFnPc4d1	odborná
knihy	kniha	k1gFnPc4	kniha
i	i	k8xC	i
dobrodružné	dobrodružný	k2eAgFnPc4d1	dobrodružná
knihy	kniha	k1gFnPc4	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
čerpal	čerpat	k5eAaImAgInS	čerpat
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
zážitků	zážitek	k1gInPc2	zážitek
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
druhé	druhý	k4xOgFnPc1	druhý
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
a	a	k8xC	a
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
se	se	k3xPyFc4	se
několika	několik	k4yIc7	několik
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
kresbami	kresba	k1gFnPc7	kresba
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Buriana	Burian	k1gMnSc2	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Stěžejním	stěžejní	k2eAgNnSc7d1	stěžejní
dílem	dílo	k1gNnSc7	dílo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
etnografie	etnografie	k1gFnSc2	etnografie
jsou	být	k5eAaImIp3nP	být
Indiáni	Indián	k1gMnPc1	Indián
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
na	na	k7c6	na
podkladu	podklad	k1gInSc6	podklad
cestopisných	cestopisný	k2eAgFnPc2d1	cestopisná
črt	črta	k1gFnPc2	črta
popisuje	popisovat	k5eAaImIp3nS	popisovat
indiánské	indiánský	k2eAgFnPc4d1	indiánská
zvyklosti	zvyklost	k1gFnPc4	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
s	s	k7c7	s
indiánem	indián	k1gMnSc7	indián
kmene	kmen	k1gInSc2	kmen
Čamakoko	Čamakoko	k1gNnSc1	Čamakoko
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Pestrý	pestrý	k2eAgInSc4d1	pestrý
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
vydalo	vydat	k5eAaPmAgNnS	vydat
knižně	knižně	k6eAd1	knižně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Titanic	Titanic	k1gInSc1	Titanic
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Čerwuiš	Čerwuiš	k1gMnSc1	Čerwuiš
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
Z	z	k7c2	z
Pacheka	Pacheek	k1gInSc2	Pacheek
do	do	k7c2	do
Pacheka	Pacheko	k1gNnSc2	Pacheko
oklikou	oklika	k1gFnSc7	oklika
přes	přes	k7c4	přes
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
také	také	k9	také
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
komiksovém	komiksový	k2eAgNnSc6d1	komiksové
zpracování	zpracování	k1gNnSc6	zpracování
Lucie	Lucie	k1gFnSc2	Lucie
Lomové	lomový	k2eAgFnPc4d1	Lomová
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Divoši	divoch	k1gMnPc1	divoch
(	(	kIx(	(
<g/>
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Okupace	okupace	k1gFnSc1	okupace
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
Friče	Frič	k1gMnPc4	Frič
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
odmítal	odmítat	k5eAaImAgMnS	odmítat
opouštět	opouštět	k5eAaImF	opouštět
svůj	svůj	k3xOyFgInSc4	svůj
byt	byt	k1gInSc4	byt
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
kaktus	kaktus	k1gInSc1	kaktus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
přivezl	přivézt	k5eAaPmAgMnS	přivézt
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
údajně	údajně	k6eAd1	údajně
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
po	po	k7c6	po
Hitlerovi	Hitler	k1gMnSc6	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
jeden	jeden	k4xCgMnSc1	jeden
návštěvník	návštěvník	k1gMnSc1	návštěvník
pogratuloval	pogratulovat	k5eAaPmAgMnS	pogratulovat
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skvělé	skvělý	k2eAgFnSc3d1	skvělá
myšlence	myšlenka	k1gFnSc3	myšlenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
okupantům	okupant	k1gMnPc3	okupant
jistě	jistě	k6eAd1	jistě
líbit	líbit	k5eAaImF	líbit
<g/>
,	,	kIx,	,
Frič	Frič	k1gMnSc1	Frič
ho	on	k3xPp3gInSc4	on
uzemnil	uzemnit	k5eAaPmAgMnS	uzemnit
komentářem	komentář	k1gInSc7	komentář
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgInSc1	ten
kaktus	kaktus	k1gInSc1	kaktus
je	být	k5eAaImIp3nS	být
nedomrlý	nedomrlý	k2eAgMnSc1d1	nedomrlý
<g/>
,	,	kIx,	,
nemocný	mocný	k2eNgMnSc1d1	nemocný
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
chcípne	chcípnout	k5eAaPmIp3nS	chcípnout
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
nádherný	nádherný	k2eAgInSc4d1	nádherný
a	a	k8xC	a
zdravý	zdravý	k2eAgInSc4d1	zdravý
kaktus	kaktus	k1gInSc4	kaktus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
po	po	k7c6	po
Einsteinovi	Einstein	k1gMnSc6	Einstein
(	(	kIx(	(
<g/>
Rebutia	Rebutia	k1gFnSc1	Rebutia
einsteinii	einsteinium	k1gNnPc7	einsteinium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
sklenících	skleník	k1gInPc6	skleník
údajně	údajně	k6eAd1	údajně
ukrýval	ukrývat	k5eAaImAgInS	ukrývat
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
škrábl	škrábnout	k5eAaPmAgMnS	škrábnout
o	o	k7c4	o
hřebík	hřebík	k1gInSc4	hřebík
a	a	k8xC	a
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
tetanem	tetan	k1gInSc7	tetan
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
lékařům	lékař	k1gMnPc3	lékař
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
léčby	léčba	k1gFnSc2	léčba
aplikovali	aplikovat	k5eAaBmAgMnP	aplikovat
kurare	kurare	k1gNnSc4	kurare
(	(	kIx(	(
<g/>
indiáni	indián	k1gMnPc5	indián
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
tetanu	tetan	k1gInSc3	tetan
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
relativně	relativně	k6eAd1	relativně
úspěšně	úspěšně	k6eAd1	úspěšně
používali	používat	k5eAaImAgMnP	používat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lékaři	lékař	k1gMnPc1	lékař
raději	rád	k6eAd2	rád
zůstali	zůstat	k5eAaPmAgMnP	zůstat
u	u	k7c2	u
standardní	standardní	k2eAgFnSc2d1	standardní
léčby	léčba	k1gFnSc2	léčba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spočívala	spočívat	k5eAaImAgFnS	spočívat
ve	v	k7c6	v
zmírňování	zmírňování	k1gNnSc6	zmírňování
bolestí	bolest	k1gFnPc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
umírání	umírání	k1gNnSc1	umírání
lékařům	lékař	k1gMnPc3	lékař
pečlivě	pečlivě	k6eAd1	pečlivě
popisoval	popisovat	k5eAaImAgMnS	popisovat
své	svůj	k3xOyFgInPc4	svůj
pocity	pocit	k1gInPc4	pocit
a	a	k8xC	a
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
co	co	k3yQnSc4	co
nejvíce	nejvíce	k6eAd1	nejvíce
prospěl	prospět	k5eAaPmAgMnS	prospět
lékařské	lékařský	k2eAgFnSc3d1	lékařská
vědě	věda	k1gFnSc3	věda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
jeho	jeho	k3xOp3gFnPc2	jeho
poznámek	poznámka	k1gFnPc2	poznámka
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nepublikovaných	publikovaný	k2eNgFnPc2d1	nepublikovaná
prací	práce	k1gFnPc2	práce
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Frič	Frič	k1gMnSc1	Frič
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Motolského	motolský	k2eAgNnSc2d1	motolské
krematoria	krematorium	k1gNnSc2	krematorium
<g/>
.	.	kIx.	.
</s>
<s>
Fričovým	Fričův	k2eAgNnSc7d1	Fričovo
jménem	jméno	k1gNnSc7	jméno
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
téměř	téměř	k6eAd1	téměř
třicet	třicet	k4xCc4	třicet
druhů	druh	k1gInPc2	druh
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
:	:	kIx,	:
Gymnocalycium	Gymnocalycium	k1gNnSc1	Gymnocalycium
fricianum	fricianum	k1gNnSc1	fricianum
Parodia	Parodium	k1gNnSc2	Parodium
friciana	frician	k1gMnSc2	frician
Cephalocereus	Cephalocereus	k1gInSc4	Cephalocereus
fricii	fricie	k1gFnSc3	fricie
Cereus	Cereus	k1gInSc1	Cereus
fricii	fricie	k1gFnSc3	fricie
Echinocactus	Echinocactus	k1gMnSc1	Echinocactus
fricii	fricie	k1gFnSc4	fricie
Echinopsis	Echinopsis	k1gFnSc4	Echinopsis
fricii	fricie	k1gFnSc4	fricie
Griseocactus	Griseocactus	k1gInSc1	Griseocactus
fricii	fricie	k1gFnSc4	fricie
Lobivia	Lobivius	k1gMnSc4	Lobivius
fricii	fricie	k1gFnSc4	fricie
Lophophora	Lophophora	k1gFnSc1	Lophophora
fricii	fricie	k1gFnSc3	fricie
Malacocarpus	Malacocarpus	k1gInSc4	Malacocarpus
fricii	fricie	k1gFnSc3	fricie
Notocactus	Notocactus	k1gInSc1	Notocactus
fricii	fricie	k1gFnSc3	fricie
Pilocereus	Pilocereus	k1gInSc4	Pilocereus
fricii	fricie	k1gFnSc3	fricie
Stenocereus	Stenocereus	k1gInSc1	Stenocereus
fricii	fricie	k1gFnSc3	fricie
Subpilocereus	Subpilocereus	k1gInSc1	Subpilocereus
fricii	fricie	k1gFnSc4	fricie
Wigginsia	Wigginsius	k1gMnSc4	Wigginsius
fricii	fricie	k1gFnSc4	fricie
Cleiostocactus	Cleiostocactus	k1gInSc4	Cleiostocactus
straussii	straussie	k1gFnSc4	straussie
var.	var.	k?	var.
fricii	fricie	k1gFnSc4	fricie
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
názvů	název	k1gInPc2	název
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
neplatných	platný	k2eNgFnPc2d1	neplatná
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jen	jen	k6eAd1	jen
Lophophora	Lophophora	k1gFnSc1	Lophophora
fricii	fricie	k1gFnSc4	fricie
Stenocereus	Stenocereus	k1gInSc4	Stenocereus
fricii	fricie	k1gFnSc3	fricie
Cleiostocactus	Cleiostocactus	k1gInSc1	Cleiostocactus
straussii	straussie	k1gFnSc3	straussie
var.	var.	k?	var.
fricii	fricie	k1gFnSc3	fricie
Notocactus	Notocactus	k1gInSc4	Notocactus
fricii	fricie	k1gFnSc6	fricie
dřívější	dřívější	k2eAgInPc4d1	dřívější
názvy	název	k1gInPc4	název
Malacocarpus	Malacocarpus	k1gMnSc1	Malacocarpus
fricii	fricie	k1gFnSc4	fricie
<g/>
,	,	kIx,	,
Wigginsia	Wigginsia	k1gFnSc1	Wigginsia
fricii	fricie	k1gFnSc4	fricie
Naopak	naopak	k6eAd1	naopak
nejnověji	nově	k6eAd3	nově
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
druh	druh	k1gInSc4	druh
Lophophora	Lophophor	k1gMnSc2	Lophophor
alberto-vojtechii	albertoojtechie	k1gFnSc4	alberto-vojtechie
Bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
jest	být	k5eAaImIp3nS	být
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
Kalera	Kalero	k1gNnSc2	Kalero
Marsal	Marsal	k1gMnPc2	Marsal
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
D.	D.	kA	D.
Fričové	Fričová	k1gFnPc1	Fričová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g />
.	.	kIx.	.
</s>
<s>
Indiáni	Indián	k1gMnPc1	Indián
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Novina	novina	k1gFnSc1	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1943	[number]	k4	1943
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
a	a	k8xC	a
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
vydání	vydání	k1gNnSc1	vydání
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1977	[number]	k4	1977
Červíček	červíček	k1gMnSc1	červíček
aneb	aneb	k?	aneb
indiánský	indiánský	k2eAgMnSc1d1	indiánský
lovec	lovec	k1gMnSc1	lovec
objevuje	objevovat	k5eAaImIp3nS	objevovat
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Titanic	Titanic	k1gInSc1	Titanic
<g/>
–	–	k?	–
<g/>
Global	globat	k5eAaImAgInS	globat
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
;	;	kIx,	;
Čerwuiš	Čerwuiš	k1gInSc1	Čerwuiš
aneb	aneb	k?	aneb
z	z	k7c2	z
Pacheka	Pacheek	k1gInSc2	Pacheek
do	do	k7c2	do
Pacheka	Pacheko	k1gNnSc2	Pacheko
oklikou	oklika	k1gFnSc7	oklika
přes	přes	k7c4	přes
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
vydání	vydání	k1gNnSc1	vydání
<g/>
)	)	kIx)	)
Kaktusy	kaktus	k1gInPc1	kaktus
sukkulenty	sukkulenta	k1gFnSc2	sukkulenta
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
pěstění	pěstění	k1gNnSc2	pěstění
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
J.	J.	kA	J.
Seidlem	Seidel	k1gMnSc7	Seidel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
O	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s>
kaktech	kaktus	k1gInPc6	kaktus
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
narkotických	narkotický	k2eAgInPc6d1	narkotický
účincích	účinek	k1gInPc6	účinek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Sfinx	sfinx	k1gInSc1	sfinx
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
1	[number]	k4	1
<g/>
.	.	kIx.	.
doplněné	doplněný	k2eAgNnSc1d1	doplněné
a	a	k8xC	a
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
:	:	kIx,	:
Maťa	Maťa	k1gMnSc1	Maťa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-901915-3-3	[number]	k4	80-901915-3-3
2	[number]	k4	2
<g/>
.	.	kIx.	.
doplněné	doplněný	k2eAgNnSc1d1	doplněné
a	a	k8xC	a
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
DharmaGaia	DharmaGaia	k1gFnSc1	DharmaGaia
:	:	kIx,	:
Maťa	Maťa	k1gMnSc1	Maťa
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86013-70-7	[number]	k4	80-86013-70-7
3	[number]	k4	3
<g/>
.	.	kIx.	.
doplněné	doplněný	k2eAgNnSc1d1	doplněné
a	a	k8xC	a
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Titanic	Titanic	k1gInSc1	Titanic
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-8665-256-6	[number]	k4	978-80-8665-256-6
Kaktusová	kaktusový	k2eAgFnSc1d1	kaktusová
příloha	příloha	k1gFnSc1	příloha
Život	život	k1gInSc1	život
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
J.	J.	kA	J.
Seidlem	Seidel	k1gMnSc7	Seidel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
Abablehnung	Abablehnunga	k1gFnPc2	Abablehnunga
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kurtem	Kurt	k1gMnSc7	Kurt
Kreuzingerem	Kreuzinger	k1gMnSc7	Kreuzinger
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
Svět	svět	k1gInSc1	svět
kaktusů	kaktus	k1gInPc2	kaktus
podle	podle	k7c2	podle
Friče	Frič	k1gMnSc2	Frič
<g/>
,	,	kIx,	,
lexikon	lexikon	k1gNnSc1	lexikon
kaktusů	kaktus	k1gInPc2	kaktus
v	v	k7c6	v
zahradnickém	zahradnický	k2eAgInSc6d1	zahradnický
a	a	k8xC	a
ovocnicko-vinařském	ovocnickoinařský	k2eAgInSc6d1	ovocnicko-vinařský
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
reprint	reprint	k1gInSc1	reprint
sestavil	sestavit	k5eAaPmAgInS	sestavit
Matis	Matis	k1gFnSc4	Matis
<g/>
,	,	kIx,	,
Dráb	dráb	k1gMnSc1	dráb
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
Cihly	cihla	k1gFnPc4	cihla
<g/>
,	,	kIx,	,
Přerov	Přerov	k1gInSc4	Přerov
1918	[number]	k4	1918
Zákon	zákon	k1gInSc1	zákon
pralesa	prales	k1gInSc2	prales
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
Mezi	mezi	k7c4	mezi
indiány	indián	k1gMnPc4	indián
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
A.	A.	kA	A.
Koníček	koníček	k1gInSc1	koníček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
Strýček	strýček	k1gMnSc1	strýček
indián	indián	k1gMnSc1	indián
<g/>
,	,	kIx,	,
Toužimský	Toužimský	k2eAgMnSc1d1	Toužimský
a	a	k8xC	a
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
Titanic	Titanic	k1gInSc1	Titanic
<g/>
–	–	k?	–
<g/>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
lovec	lovec	k1gMnSc1	lovec
<g/>
,	,	kIx,	,
Toužimský	Toužimský	k2eAgMnSc1d1	Toužimský
a	a	k8xC	a
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
Titanic	Titanic	k1gInSc1	Titanic
<g/>
–	–	k?	–
<g/>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Hadí	hadí	k2eAgInSc1d1	hadí
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Toužimský	Toužimský	k2eAgMnSc1d1	Toužimský
a	a	k8xC	a
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
Titanic	Titanic	k1gInSc1	Titanic
<g/>
–	–	k?	–
<g/>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
Crkal	crkat	k5eAaImAgMnS	crkat
<g/>
:	:	kIx,	:
Lovec	lovec	k1gMnSc1	lovec
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Alberto	Alberta	k1gFnSc5	Alberta
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Frič	Frič	k1gMnSc1	Frič
<g/>
:	:	kIx,	:
Fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Josefa	Josef	k1gMnSc2	Josef
Sudka	sudka	k1gFnSc1	sudka
</s>
