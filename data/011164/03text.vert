<p>
<s>
Astrometrie	astrometrie	k1gFnSc1	astrometrie
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc4	odvětví
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
přesnými	přesný	k2eAgNnPc7d1	přesné
měřeními	měření	k1gNnPc7	měření
a	a	k8xC	a
vysvětlováním	vysvětlování	k1gNnSc7	vysvětlování
pozice	pozice	k1gFnSc2	pozice
a	a	k8xC	a
pohybů	pohyb	k1gInPc2	pohyb
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
ostatních	ostatní	k2eAgNnPc2d1	ostatní
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
omezené	omezený	k2eAgFnSc3d1	omezená
přesnosti	přesnost	k1gFnSc3	přesnost
přístrojů	přístroj	k1gInPc2	přístroj
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
astrometrie	astrometrie	k1gFnSc1	astrometrie
bude	být	k5eAaImBp3nS	být
stát	stát	k5eAaPmF	stát
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
vědeckého	vědecký	k2eAgInSc2d1	vědecký
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zlepšujícími	zlepšující	k2eAgMnPc7d1	zlepšující
se	se	k3xPyFc4	se
přístroji	přístroj	k1gInPc7	přístroj
a	a	k8xC	a
nástupem	nástup	k1gInSc7	nástup
automatizace	automatizace	k1gFnSc2	automatizace
se	se	k3xPyFc4	se
informace	informace	k1gFnPc1	informace
získávané	získávaný	k2eAgFnSc2d1	získávaná
astrometrií	astrometrie	k1gFnSc7	astrometrie
staly	stát	k5eAaPmAgFnP	stát
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgFnPc1d1	důležitá
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
výzkumu	výzkum	k1gInSc6	výzkum
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
původu	původ	k1gInSc2	původ
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hledání	hledání	k1gNnSc1	hledání
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
==	==	k?	==
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
využití	využití	k1gNnSc2	využití
astrometrie	astrometrie	k1gFnSc2	astrometrie
je	být	k5eAaImIp3nS	být
hledání	hledání	k1gNnSc1	hledání
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
nejstarší	starý	k2eAgFnSc1d3	nejstarší
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
používána	používán	k2eAgFnSc1d1	používána
asi	asi	k9	asi
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zjišťujeme	zjišťovat	k5eAaImIp1nP	zjišťovat
<g/>
-li	i	k?	-li
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
a	a	k8xC	a
precizně	precizně	k6eAd1	precizně
polohu	poloha	k1gFnSc4	poloha
určité	určitý	k2eAgFnSc2d1	určitá
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
pohybovat	pohybovat	k5eAaImF	pohybovat
ne	ne	k9	ne
po	po	k7c6	po
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vlnovce	vlnovka	k1gFnSc6	vlnovka
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
oběhem	oběh	k1gInSc7	oběh
Země	zem	k1gFnSc2	zem
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Odhlédneme	odhlédnout	k5eAaPmIp1nP	odhlédnout
<g/>
-li	i	k?	-li
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
zkoumaná	zkoumaný	k2eAgFnSc1d1	zkoumaná
hvězda	hvězda	k1gFnSc1	hvězda
pohybovat	pohybovat	k5eAaImF	pohybovat
po	po	k7c6	po
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ovšem	ovšem	k9	ovšem
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
hvězdy	hvězda	k1gFnSc2	hvězda
jinak	jinak	k6eAd1	jinak
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
hvězdu	hvězda	k1gFnSc4	hvězda
svou	svůj	k3xOyFgFnSc4	svůj
gravitaci	gravitace	k1gFnSc4	gravitace
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
pohyb	pohyb	k1gInSc1	pohyb
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
jevit	jevit	k5eAaImF	jevit
přímý	přímý	k2eAgInSc1d1	přímý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
po	po	k7c6	po
vlnovce	vlnovka	k1gFnSc6	vlnovka
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
oběžné	oběžný	k2eAgFnSc2d1	oběžná
doby	doba	k1gFnSc2	doba
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
s	s	k7c7	s
amplitudou	amplituda	k1gFnSc7	amplituda
úměrnou	úměrný	k2eAgFnSc4d1	úměrná
poměru	poměra	k1gFnSc4	poměra
hmotností	hmotnost	k1gFnSc7	hmotnost
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
hledání	hledání	k1gNnSc6	hledání
exoplanet	exoplaneta	k1gFnPc2	exoplaneta
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
astrometrie	astrometrie	k1gFnSc2	astrometrie
byl	být	k5eAaImAgMnS	být
nejznámější	známý	k2eAgMnSc1d3	nejznámější
astronom	astronom	k1gMnSc1	astronom
Peter	Peter	k1gMnSc1	Peter
van	vana	k1gFnPc2	vana
de	de	k?	de
Kamp	Kampa	k1gFnPc2	Kampa
svými	svůj	k3xOyFgMnPc7	svůj
"	"	kIx"	"
<g/>
objevy	objev	k1gInPc4	objev
<g/>
"	"	kIx"	"
dvou	dva	k4xCgInPc2	dva
objektů	objekt	k1gInPc2	objekt
u	u	k7c2	u
Barnardovy	Barnardův	k2eAgFnSc2d1	Barnardova
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Domnělé	domnělý	k2eAgInPc4d1	domnělý
objevy	objev	k1gInPc4	objev
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
výsledkem	výsledek	k1gInSc7	výsledek
chyby	chyba	k1gFnSc2	chyba
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ve	v	k7c6	v
fikcích	fikce	k1gFnPc6	fikce
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sci-fi	scii	k1gNnSc6	sci-fi
seriálu	seriál	k1gInSc2	seriál
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
<g/>
:	:	kIx,	:
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
loď	loď	k1gFnSc1	loď
Voyager	Voyagra	k1gFnPc2	Voyagra
je	být	k5eAaImIp3nS	být
astrometrická	astrometrický	k2eAgFnSc1d1	astrometrická
laboratoř	laboratoř	k1gFnSc1	laboratoř
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
často	často	k6eAd1	často
používaných	používaný	k2eAgFnPc2d1	používaná
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sci-fi	scii	k1gNnSc6	sci-fi
seriálu	seriál	k1gInSc2	seriál
Battlestar	Battlestar	k1gInSc1	Battlestar
Galactica	Galactic	k1gInSc2	Galactic
je	být	k5eAaImIp3nS	být
astrometrická	astrometrický	k2eAgFnSc1d1	astrometrická
laboratoř	laboratoř	k1gFnSc1	laboratoř
uváděna	uváděn	k2eAgFnSc1d1	uváděna
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dialozích	dialog	k1gInPc6	dialog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
astrometrie	astrometrie	k1gFnSc2	astrometrie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jean	Jean	k1gMnSc1	Jean
Kovalevsky	Kovalevsko	k1gNnPc7	Kovalevsko
a	a	k8xC	a
P.	P.	kA	P.
Kenneth	Kenneth	k1gMnSc1	Kenneth
Seidelman	Seidelman	k1gMnSc1	Seidelman
<g/>
,	,	kIx,	,
Fundamentals	Fundamentals	k1gInSc4	Fundamentals
of	of	k?	of
Astrometry	Astrometr	k1gInPc4	Astrometr
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnPc4	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
64216	[number]	k4	64216
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Precision	Precision	k1gInSc4	Precision
Astrometry	Astrometr	k1gInPc7	Astrometr
<g/>
,	,	kIx,	,
University	universita	k1gFnPc4	universita
of	of	k?	of
Virginia	Virginium	k1gNnPc1	Virginium
Department	department	k1gInSc1	department
of	of	k?	of
Astronomy	astronom	k1gMnPc7	astronom
<g/>
,	,	kIx,	,
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
2006-08-10	[number]	k4	2006-08-10
</s>
</p>
<p>
<s>
NASA	NASA	kA	NASA
<g/>
:	:	kIx,	:
informace	informace	k1gFnSc1	informace
</s>
</p>
<p>
<s>
Discovery	Discover	k1gInPc1	Discover
<g/>
:	:	kIx,	:
Largest	Largest	k1gMnSc1	Largest
Solar	Solar	k1gMnSc1	Solar
System	Syst	k1gMnSc7	Syst
Object	Object	k1gMnSc1	Object
Since	Since	k1gMnSc1	Since
Pluto	Pluto	k1gMnSc1	Pluto
</s>
</p>
<p>
<s>
Mike	Mike	k1gFnSc1	Mike
Brown	Browna	k1gFnPc2	Browna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
CalTech	CalTech	k1gInSc1	CalTech
Home	Home	k1gNnSc6	Home
Page	Page	k1gInSc1	Page
</s>
</p>
<p>
<s>
Scientific	Scientific	k1gMnSc1	Scientific
Paper	Paper	k1gMnSc1	Paper
describing	describing	k1gInSc4	describing
Sedna	Sedn	k1gInSc2	Sedn
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
discovery	discover	k1gMnPc7	discover
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
G.	G.	kA	G.
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Astrometry	Astrometr	k1gInPc1	Astrometr
of	of	k?	of
fundamental	fundamental	k1gMnSc1	fundamental
catalogues	catalogues	k1gMnSc1	catalogues
:	:	kIx,	:
the	the	k?	the
evolution	evolution	k1gInSc1	evolution
from	from	k6eAd1	from
optical	opticat	k5eAaPmAgInS	opticat
to	ten	k3xDgNnSc4	ten
radio	radio	k1gNnSc4	radio
reference	reference	k1gFnSc2	reference
frames	framesa	k1gFnPc2	framesa
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
:	:	kIx,	:
Springer	Springer	k1gInSc1	Springer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Hipparcos	Hipparcos	k1gMnSc1	Hipparcos
Space	Space	k1gMnSc1	Space
Astrometry	Astrometr	k1gInPc7	Astrometr
Mission	Mission	k1gInSc1	Mission
</s>
</p>
<p>
<s>
Kovalevsky	Kovalevsky	k6eAd1	Kovalevsky
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Modern	Modern	k1gInSc1	Modern
Astrometry	Astrometr	k1gInPc1	Astrometr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Springer	Springer	k1gInSc1	Springer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hipparcos	Hipparcos	k1gInSc1	Hipparcos
Space	Spaec	k1gInSc2	Spaec
Astrometry	Astrometr	k1gInPc4	Astrometr
Mission	Mission	k1gInSc1	Mission
(	(	kIx(	(
<g/>
ESA	eso	k1gNnSc2	eso
–	–	k?	–
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
