<s>
Astrometrie	astrometrie	k1gFnSc1	astrometrie
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc4	odvětví
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
přesnými	přesný	k2eAgNnPc7d1	přesné
měřeními	měření	k1gNnPc7	měření
a	a	k8xC	a
vysvětlováním	vysvětlování	k1gNnSc7	vysvětlování
pozice	pozice	k1gFnSc2	pozice
a	a	k8xC	a
pohybů	pohyb	k1gInPc2	pohyb
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
ostatních	ostatní	k2eAgNnPc2d1	ostatní
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
