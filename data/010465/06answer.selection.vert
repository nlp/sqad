<s>
Bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
použití	použití	k1gNnSc1	použití
trichlorfonu	trichlorfon	k1gInSc2	trichlorfon
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
doporučen	doporučen	k2eAgMnSc1d1	doporučen
<g/>
.	.	kIx.	.
</s>
