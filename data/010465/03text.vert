<p>
<s>
Trichlorfon	Trichlorfon	k1gInSc1	Trichlorfon
(	(	kIx(	(
<g/>
též	též	k9	též
metrifonát	metrifonát	k1gInSc1	metrifonát
<g/>
;	;	kIx,	;
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
(	(	kIx(	(
<g/>
RS	RS	kA	RS
<g/>
)	)	kIx)	)
<g/>
-dimethyl	imethyl	k1gInSc1	-dimethyl
(	(	kIx(	(
<g/>
2,2	[number]	k4	2,2
<g/>
,2	,2	k4	,2
<g/>
-trichlor-	richlor-	k?	-trichlor-
<g/>
1	[number]	k4	1
<g/>
-hydroxyethyl	ydroxyethyl	k1gInSc1	-hydroxyethyl
<g/>
)	)	kIx)	)
<g/>
fosfonát	fosfonát	k1gInSc1	fosfonát
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
orgnofosfát	orgnofosfát	k1gInSc1	orgnofosfát
používaný	používaný	k2eAgInSc1d1	používaný
především	především	k9	především
jako	jako	k9	jako
anthelmintikum	anthelmintikum	k1gNnSc1	anthelmintikum
a	a	k8xC	a
insekticid	insekticid	k1gInSc1	insekticid
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
prekurzor	prekurzor	k1gInSc4	prekurzor
prolátky	prolátko	k1gNnPc7	prolátko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
neenzymaticky	enzymaticky	k6eNd1	enzymaticky
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
na	na	k7c4	na
aktivní	aktivní	k2eAgInSc4d1	aktivní
metabolit	metabolit	k1gInSc4	metabolit
dichlorvos	dichlorvosa	k1gFnPc2	dichlorvosa
<g/>
.	.	kIx.	.
</s>
<s>
Trichlorfon	Trichlorfon	k1gInSc1	Trichlorfon
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
dichlorvos	dichlorvos	k1gInSc1	dichlorvos
<g/>
,	,	kIx,	,
ireverzibilně	ireverzibilně	k6eAd1	ireverzibilně
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
acetylcholinesterázu	acetylcholinesteráza	k1gFnSc4	acetylcholinesteráza
<g/>
.	.	kIx.	.
<g/>
Trichlorfon	Trichlorfon	k1gInSc4	Trichlorfon
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
schistosomózy	schistosomóza	k1gFnSc2	schistosomóza
způsobované	způsobovaný	k2eAgFnSc2d1	způsobovaná
červem	červ	k1gMnSc7	červ
Schistosoma	Schistosom	k1gMnSc2	Schistosom
haematobium	haematobium	k1gNnSc4	haematobium
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
přípravky	přípravek	k1gInPc1	přípravek
pro	pro	k7c4	pro
humánní	humánní	k2eAgFnSc4d1	humánní
medicínu	medicína	k1gFnSc4	medicína
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
však	však	k9	však
prodávají	prodávat	k5eAaImIp3nP	prodávat
anthelmintické	anthelmintický	k2eAgInPc1d1	anthelmintický
přípravky	přípravek	k1gInPc1	přípravek
veterinární	veterinární	k2eAgFnSc2d1	veterinární
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Neguvon	Neguvon	k1gInSc1	Neguvon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
použití	použití	k1gNnSc1	použití
trichlorfonu	trichlorfon	k1gInSc2	trichlorfon
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
doporučen	doporučen	k2eAgMnSc1d1	doporučen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Metrifonate	Metrifonat	k1gInSc5	Metrifonat
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
