<s>
Staré	Staré	k2eAgFnPc1d1	Staré
stupnice	stupnice	k1gFnPc1	stupnice
-	-	kIx~	-
mody	modus	k1gInPc1	modus
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
středověké	středověký	k2eAgFnSc3d1	středověká
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
gregoriánském	gregoriánský	k2eAgInSc6d1	gregoriánský
chorálu	chorál	k1gInSc6	chorál
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
církevní	církevní	k2eAgFnPc1d1	církevní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sloužila	sloužit	k5eAaImAgFnS	sloužit
středověká	středověký	k2eAgFnSc1d1	středověká
hudba	hudba	k1gFnSc1	hudba
převážně	převážně	k6eAd1	převážně
církevním	církevní	k2eAgInPc3d1	církevní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
