<p>
<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgMnSc1d1	slavný
britský	britský	k2eAgMnSc1d1	britský
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
především	především	k6eAd1	především
jako	jako	k8xC	jako
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
před	před	k7c7	před
newyorským	newyorský	k2eAgInSc7d1	newyorský
domem	dům	k1gInSc7	dům
The	The	k1gFnSc2	The
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
večerním	večerní	k2eAgInSc6d1	večerní
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
smrtelně	smrtelně	k6eAd1	smrtelně
postřelen	postřelit	k5eAaPmNgMnS	postřelit
svým	svůj	k3xOyFgMnSc7	svůj
fanouškem	fanoušek	k1gMnSc7	fanoušek
Markem	Marek	k1gMnSc7	Marek
Davidem	David	k1gMnSc7	David
Chapmanem	Chapman	k1gMnSc7	Chapman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
rychlému	rychlý	k2eAgInSc3d1	rychlý
převozu	převoz	k1gInSc3	převoz
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
se	se	k3xPyFc4	se
Lennonův	Lennonův	k2eAgInSc1d1	Lennonův
život	život	k1gInSc1	život
lékařům	lékař	k1gMnPc3	lékař
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
zpopelněno	zpopelnit	k5eAaPmNgNnS	zpopelnit
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
a	a	k8xC	a
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
předány	předat	k5eAaPmNgInP	předat
manželce	manželka	k1gFnSc3	manželka
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
nevypravit	vypravit	k5eNaPmF	vypravit
svému	svůj	k3xOyFgInSc3	svůj
manželovi	manželův	k2eAgMnPc1d1	manželův
žádný	žádný	k3yNgInSc4	žádný
pohřeb	pohřeb	k1gInSc4	pohřeb
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
shledal	shledat	k5eAaPmAgMnS	shledat
Chapmana	Chapman	k1gMnSc4	Chapman
vinným	vinný	k1gMnSc7	vinný
z	z	k7c2	z
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
doživotnímu	doživotní	k2eAgInSc3d1	doživotní
trestu	trest	k1gInSc3	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
podmínečného	podmínečný	k2eAgNnSc2d1	podmínečné
propuštění	propuštění	k1gNnSc2	propuštění
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vražda	vražda	k1gFnSc1	vražda
měla	mít	k5eAaImAgFnS	mít
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
ohlas	ohlas	k1gInSc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Osobě	osoba	k1gFnSc3	osoba
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
věnována	věnovat	k5eAaImNgFnS	věnovat
řada	řada	k1gFnSc1	řada
poct	pocta	k1gFnPc2	pocta
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
památník	památník	k1gInSc1	památník
Strawberry	Strawberra	k1gFnSc2	Strawberra
Fields	Fieldsa	k1gFnPc2	Fieldsa
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
nebo	nebo	k8xC	nebo
Imagine	Imagin	k1gInSc5	Imagin
Peace	Peace	k1gMnPc4	Peace
Tower	Tower	k1gInSc1	Tower
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
spontánně	spontánně	k6eAd1	spontánně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
památník	památník	k1gInSc1	památník
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Lennonova	Lennonův	k2eAgFnSc1d1	Lennonova
zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Den	den	k1gInSc4	den
vraždy	vražda	k1gFnSc2	vražda
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
hodinách	hodina	k1gFnPc6	hodina
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Johna	John	k1gMnSc4	John
Lennona	Lennon	k1gMnSc4	Lennon
a	a	k8xC	a
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
The	The	k1gFnPc2	The
Dakota	Dakota	k1gFnSc1	Dakota
na	na	k7c4	na
72	[number]	k4	72
<g/>
.	.	kIx.	.
ulici	ulice	k1gFnSc4	ulice
fotografka	fotografka	k1gFnSc1	fotografka
Annie	Annie	k1gFnSc1	Annie
Leibovitzová	Leibovitzová	k1gFnSc1	Leibovitzová
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgFnSc1d1	pracující
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Lennona	Lennona	k1gFnSc1	Lennona
vyfotografovala	vyfotografovat	k5eAaPmAgFnS	vyfotografovat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
obálku	obálka	k1gFnSc4	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gInSc1	Lennon
však	však	k9	však
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
časopisu	časopis	k1gInSc2	časopis
bude	být	k5eAaImBp3nS	být
on	on	k3xPp3gMnSc1	on
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
fotografka	fotografka	k1gFnSc1	fotografka
zmíněnou	zmíněný	k2eAgFnSc4d1	zmíněná
fotografii	fotografia	k1gFnSc4	fotografia
pořídila	pořídit	k5eAaPmAgFnS	pořídit
<g/>
,	,	kIx,	,
opustila	opustit	k5eAaPmAgFnS	opustit
jejich	jejich	k3xOp3gInSc4	jejich
apartmán	apartmán	k1gInSc4	apartmán
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
veřejné	veřejný	k2eAgNnSc4d1	veřejné
vystoupení	vystoupení	k1gNnSc4	vystoupení
Lennona	Lennon	k1gMnSc2	Lennon
bylo	být	k5eAaImAgNnS	být
odpolední	odpolední	k1gNnSc1	odpolední
interview	interview	k1gNnSc2	interview
poskytnuté	poskytnutý	k2eAgFnSc2d1	poskytnutá
moderátorovi	moderátor	k1gMnSc6	moderátor
Daveu	Dave	k1gMnSc6	Dave
Sholinovi	Sholin	k1gMnSc6	Sholin
z	z	k7c2	z
rádia	rádio	k1gNnSc2	rádio
RKO	RKO	kA	RKO
Radio	radio	k1gNnSc1	radio
Network	network	k1gInSc1	network
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
skončilo	skončit	k5eAaPmAgNnS	skončit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
sedmnácté	sedmnáctý	k4xOgFnSc6	sedmnáctý
hodině	hodina	k1gFnSc6	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vražda	vražda	k1gFnSc1	vražda
===	===	k?	===
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
17.00	[number]	k4	17.00
se	se	k3xPyFc4	se
Lennon	Lennon	k1gMnSc1	Lennon
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
vydali	vydat	k5eAaPmAgMnP	vydat
do	do	k7c2	do
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
Record	Recorda	k1gFnPc2	Recorda
Plant	planta	k1gFnPc2	planta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přepracovali	přepracovat	k5eAaPmAgMnP	přepracovat
příští	příští	k2eAgInSc4d1	příští
singl	singl	k1gInSc4	singl
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Walking	Walking	k1gInSc4	Walking
on	on	k3xPp3gMnSc1	on
Thin	Thin	k1gMnSc1	Thin
Ice	Ice	k1gMnSc1	Ice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Lennon	Lennon	k1gMnSc1	Lennon
a	a	k8xC	a
Yoko	Yoko	k1gMnSc1	Yoko
vycházeli	vycházet	k5eAaImAgMnP	vycházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
a	a	k8xC	a
směřovali	směřovat	k5eAaImAgMnP	směřovat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
limuzíně	limuzína	k1gFnSc3	limuzína
<g/>
,	,	kIx,	,
shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gMnPc2	on
několik	několik	k4yIc1	několik
fanoušků	fanoušek	k1gMnPc2	fanoušek
žádajících	žádající	k2eAgInPc2d1	žádající
autogram	autogram	k1gInSc4	autogram
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Lennonův	Lennonův	k2eAgMnSc1d1	Lennonův
budoucí	budoucí	k2eAgMnSc1d1	budoucí
vrah	vrah	k1gMnSc1	vrah
Mark	Mark	k1gMnSc1	Mark
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
.	.	kIx.	.
</s>
<s>
Chapman	Chapman	k1gMnSc1	Chapman
mu	on	k3xPp3gMnSc3	on
podal	podat	k5eAaPmAgMnS	podat
svou	svůj	k3xOyFgFnSc4	svůj
desku	deska	k1gFnSc4	deska
Double	double	k2eAgInPc1d1	double
Fantasy	fantas	k1gInPc1	fantas
a	a	k8xC	a
Lennon	Lennon	k1gMnSc1	Lennon
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Lennon	Lennon	k1gInSc1	Lennon
Chapmana	Chapman	k1gMnSc2	Chapman
zeptal	zeptat	k5eAaPmAgInS	zeptat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
souhlasně	souhlasně	k6eAd1	souhlasně
přikývl	přikývnout	k5eAaPmAgMnS	přikývnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lennon	Lennon	k1gMnSc1	Lennon
pak	pak	k6eAd1	pak
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
nahrávacího	nahrávací	k2eAgNnSc2d1	nahrávací
studia	studio	k1gNnSc2	studio
Record	Recorda	k1gFnPc2	Recorda
Plant	planta	k1gFnPc2	planta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
strávili	strávit	k5eAaPmAgMnP	strávit
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zpáteční	zpáteční	k2eAgFnSc2d1	zpáteční
cesty	cesta	k1gFnSc2	cesta
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
nají	najíst	k5eAaPmIp3nS	najíst
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
v	v	k7c6	v
autě	auto	k1gNnSc6	auto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
dříve	dříve	k6eAd2	dříve
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
domu	dům	k1gInSc3	dům
přijeli	přijet	k5eAaPmAgMnP	přijet
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
22.50	[number]	k4	22.50
a	a	k8xC	a
z	z	k7c2	z
limuzíny	limuzína	k1gFnSc2	limuzína
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
72	[number]	k4	72
<g/>
.	.	kIx.	.
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svědeckých	svědecký	k2eAgFnPc2d1	svědecká
výpovědí	výpověď	k1gFnPc2	výpověď
domovníka	domovník	k1gMnSc2	domovník
Jose	Jos	k1gMnSc2	Jos
Perdoma	Perdom	k1gMnSc2	Perdom
a	a	k8xC	a
řidiče	řidič	k1gMnSc2	řidič
taxislužby	taxislužba	k1gFnSc2	taxislužba
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Chapman	Chapman	k1gMnSc1	Chapman
ukrýval	ukrývat	k5eAaImAgMnS	ukrývat
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
klenutého	klenutý	k2eAgNnSc2d1	klenuté
podloubí	podloubí	k1gNnSc2	podloubí
domu	dům	k1gInSc2	dům
The	The	k1gFnSc1	The
Dakota	Dakota	k1gFnSc1	Dakota
a	a	k8xC	a
čekal	čekat	k5eAaImAgMnS	čekat
zde	zde	k6eAd1	zde
na	na	k7c4	na
Lennonův	Lennonův	k2eAgInSc4d1	Lennonův
příchod	příchod	k1gInSc4	příchod
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
šla	jít	k5eAaImAgFnS	jít
několik	několik	k4yIc4	několik
kroků	krok	k1gInPc2	krok
před	před	k7c7	před
manželem	manžel	k1gMnSc7	manžel
a	a	k8xC	a
do	do	k7c2	do
recepce	recepce	k1gFnSc2	recepce
vešla	vejít	k5eAaPmAgFnS	vejít
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
<g/>
.	.	kIx.	.
<g/>
Jakmile	jakmile	k8xS	jakmile
Lennon	Lennon	k1gMnSc1	Lennon
prošel	projít	k5eAaPmAgMnS	projít
kolem	kolem	k6eAd1	kolem
Chapmana	Chapman	k1gMnSc4	Chapman
<g/>
,	,	kIx,	,
Chapman	Chapman	k1gMnSc1	Chapman
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vypálil	vypálit	k5eAaPmAgMnS	vypálit
pět	pět	k4xCc4	pět
ran	rána	k1gFnPc2	rána
z	z	k7c2	z
revolveru	revolver	k1gInSc2	revolver
ráže	ráže	k1gFnSc2	ráže
.38	.38	k4	.38
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
z	z	k7c2	z
kulek	kulka	k1gFnPc2	kulka
Lennona	Lennon	k1gMnSc2	Lennon
minula	minulo	k1gNnSc2	minulo
a	a	k8xC	a
zasáhla	zasáhnout	k5eAaPmAgNnP	zasáhnout
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
oken	okno	k1gNnPc2	okno
budovy	budova	k1gFnSc2	budova
The	The	k1gFnSc1	The
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
kulky	kulka	k1gFnPc1	kulka
však	však	k9	však
pronikly	proniknout	k5eAaPmAgFnP	proniknout
do	do	k7c2	do
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
mu	on	k3xPp3gNnSc3	on
roztříštila	roztříštit	k5eAaPmAgFnS	roztříštit
rameno	rameno	k1gNnSc4	rameno
a	a	k8xC	a
poslední	poslední	k2eAgFnSc4d1	poslední
proťala	protít	k5eAaPmAgFnS	protít
aortu	aorta	k1gFnSc4	aorta
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gInSc1	Lennon
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgMnSc1d1	schopen
udělat	udělat	k5eAaPmF	udělat
pouze	pouze	k6eAd1	pouze
pět	pět	k4xCc4	pět
kroků	krok	k1gInPc2	krok
do	do	k7c2	do
recepce	recepce	k1gFnSc2	recepce
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Postřelili	postřelit	k5eAaPmAgMnP	postřelit
mě	já	k3xPp1nSc2	já
<g/>
...	...	k?	...
někdo	někdo	k3yInSc1	někdo
na	na	k7c4	na
mě	já	k3xPp1nSc4	já
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Posléze	posléze	k6eAd1	posléze
ztratil	ztratit	k5eAaPmAgMnS	ztratit
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Jay	Jay	k?	Jay
Hastings	Hastings	k1gInSc1	Hastings
<g/>
,	,	kIx,	,
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
recepce	recepce	k1gFnSc2	recepce
domu	dům	k1gInSc2	dům
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
Lennona	Lennona	k1gFnSc1	Lennona
přikryl	přikrýt	k5eAaPmAgInS	přikrýt
svojí	svůj	k3xOyFgFnSc7	svůj
uniformou	uniforma	k1gFnSc7	uniforma
<g/>
,	,	kIx,	,
sundal	sundat	k5eAaPmAgMnS	sundat
mu	on	k3xPp3gMnSc3	on
brýle	brýle	k1gFnPc4	brýle
a	a	k8xC	a
přivolal	přivolat	k5eAaPmAgInS	přivolat
policii	policie	k1gFnSc3	policie
<g/>
.	.	kIx.	.
</s>
<s>
Vrátný	vrátný	k1gMnSc1	vrátný
Perdomo	Perdoma	k1gFnSc5	Perdoma
mezitím	mezitím	k6eAd1	mezitím
vytrhl	vytrhnout	k5eAaPmAgMnS	vytrhnout
Chapmanovi	Chapman	k1gMnSc3	Chapman
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
revolver	revolver	k1gInSc1	revolver
a	a	k8xC	a
odkopl	odkopnout	k5eAaPmAgMnS	odkopnout
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
silnici	silnice	k1gFnSc4	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
revolver	revolver	k1gInSc1	revolver
zadržel	zadržet	k5eAaPmAgMnS	zadržet
přítomný	přítomný	k2eAgMnSc1d1	přítomný
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
obsluhy	obsluha	k1gFnSc2	obsluha
výtahu	výtah	k1gInSc2	výtah
z	z	k7c2	z
domu	dům	k1gInSc2	dům
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chapman	Chapman	k1gMnSc1	Chapman
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
sundal	sundat	k5eAaPmAgMnS	sundat
kabát	kabát	k1gInSc4	kabát
a	a	k8xC	a
klobouk	klobouk	k1gInSc4	klobouk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
již	již	k6eAd1	již
neskrývá	skrývat	k5eNaImIp3nS	skrývat
žádné	žádný	k3yNgFnPc4	žádný
další	další	k2eAgFnPc4d1	další
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Usadil	usadit	k5eAaPmAgMnS	usadit
se	se	k3xPyFc4	se
na	na	k7c4	na
obrubník	obrubník	k1gInSc4	obrubník
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
a	a	k8xC	a
klidně	klidně	k6eAd1	klidně
vyčkával	vyčkávat	k5eAaImAgMnS	vyčkávat
na	na	k7c4	na
příjezd	příjezd	k1gInSc4	příjezd
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Vrátný	vrátný	k1gMnSc1	vrátný
Perdomo	Perdoma	k1gFnSc5	Perdoma
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
zakřičel	zakřičet	k5eAaPmAgMnS	zakřičet
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Víš	vědět	k5eAaImIp2nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsi	být	k5eAaImIp2nS	být
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
Chapman	Chapman	k1gMnSc1	Chapman
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jo	jo	k9	jo
<g/>
,	,	kIx,	,
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
jsem	být	k5eAaImIp1nS	být
Johna	John	k1gMnSc4	John
Lennona	Lennon	k1gMnSc4	Lennon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgMnPc7	první
policisty	policista	k1gMnPc7	policista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Steve	Steve	k1gMnSc1	Steve
Spiro	Spiro	k1gNnSc4	Spiro
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Cullen	Cullna	k1gFnPc2	Cullna
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hlášení	hlášení	k1gNnSc2	hlášení
o	o	k7c6	o
střelbě	střelba	k1gFnSc6	střelba
nacházeli	nacházet	k5eAaImAgMnP	nacházet
na	na	k7c6	na
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
křižovatce	křižovatka	k1gFnSc6	křižovatka
Broadwaye	Broadway	k1gInSc2	Broadway
a	a	k8xC	a
72	[number]	k4	72
<g/>
.	.	kIx.	.
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Spatřili	spatřit	k5eAaPmAgMnP	spatřit
Chapmana	Chapman	k1gMnSc4	Chapman
klidně	klidně	k6eAd1	klidně
sedícího	sedící	k2eAgMnSc4d1	sedící
na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
s	s	k7c7	s
výtiskem	výtisk	k1gInSc7	výtisk
knihy	kniha	k1gFnSc2	kniha
od	od	k7c2	od
J.	J.	kA	J.
D.	D.	kA	D.
Sallingera	Sallingera	k1gFnSc1	Sallingera
Kdo	kdo	k3yInSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
zatýkání	zatýkání	k1gNnSc2	zatýkání
nekladl	klást	k5eNaImAgMnS	klást
naprosto	naprosto	k6eAd1	naprosto
žádný	žádný	k3yNgInSc4	žádný
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
knihy	kniha	k1gFnSc2	kniha
si	se	k3xPyFc3	se
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
před	před	k7c7	před
vraždou	vražda	k1gFnSc7	vražda
zapsal	zapsat	k5eAaPmAgMnS	zapsat
poznámku	poznámka	k1gFnSc4	poznámka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Holdenu	Holden	k1gMnSc3	Holden
Caulfieldovi	Caulfield	k1gMnSc3	Caulfield
od	od	k7c2	od
Holdena	Holdeno	k1gNnSc2	Holdeno
Caulfielda	Caulfield	k1gMnSc2	Caulfield
–	–	k?	–
To	to	k9	to
je	být	k5eAaImIp3nS	být
moje	můj	k3xOp1gNnSc1	můj
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Chapman	Chapman	k1gMnSc1	Chapman
byl	být	k5eAaImAgMnS	být
knihou	kniha	k1gFnSc7	kniha
posedlý	posedlý	k2eAgMnSc1d1	posedlý
a	a	k8xC	a
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
Caulfieldem	Caulfield	k1gMnSc7	Caulfield
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
ztotožňoval	ztotožňovat	k5eAaImAgMnS	ztotožňovat
<g/>
.	.	kIx.	.
<g/>
Několik	několik	k4yIc1	několik
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
střelbě	střelba	k1gFnSc6	střelba
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
také	také	k9	také
policisté	policista	k1gMnPc1	policista
Bill	Bill	k1gMnSc1	Bill
Gamble	Gamble	k1gMnSc1	Gamble
a	a	k8xC	a
James	James	k1gMnSc1	James
Moran	Morana	k1gFnPc2	Morana
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
Lennona	Lennon	k1gMnSc4	Lennon
položili	položit	k5eAaPmAgMnP	položit
na	na	k7c4	na
zadní	zadní	k2eAgNnSc4d1	zadní
sedadlo	sedadlo	k1gNnSc4	sedadlo
svého	svůj	k3xOyFgInSc2	svůj
služebního	služební	k2eAgInSc2d1	služební
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
odvezli	odvézt	k5eAaPmAgMnP	odvézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
St.	st.	kA	st.
Luke	Luk	k1gFnSc2	Luk
<g/>
'	'	kIx"	'
<g/>
s-Roosevelt	s-Roosevelt	k1gMnSc1	s-Roosevelt
Hospital	Hospital	k1gMnSc1	Hospital
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Policista	policista	k1gMnSc1	policista
Moran	Morana	k1gFnPc2	Morana
se	se	k3xPyFc4	se
Lennona	Lennona	k1gFnSc1	Lennona
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
opět	opět	k6eAd1	opět
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
nabyl	nabýt	k5eAaPmAgInS	nabýt
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
dotázal	dotázat	k5eAaPmAgMnS	dotázat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vy	vy	k3xPp2nPc1	vy
jste	být	k5eAaImIp2nP	být
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
sotva	sotva	k6eAd1	sotva
slyšitelným	slyšitelný	k2eAgInSc7d1	slyšitelný
a	a	k8xC	a
sípavým	sípavý	k2eAgInSc7d1	sípavý
hlasem	hlas	k1gInSc7	hlas
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
"	"	kIx"	"
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
ztratil	ztratit	k5eAaPmAgMnS	ztratit
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úmrtí	úmrtí	k1gNnSc1	úmrtí
Lennona	Lennon	k1gMnSc2	Lennon
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgMnS	být
dopraven	dopravit	k5eAaPmNgMnS	dopravit
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
asi	asi	k9	asi
15	[number]	k4	15
nebo	nebo	k8xC	nebo
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
bezúspěšně	bezúspěšně	k6eAd1	bezúspěšně
oživován	oživován	k2eAgInSc1d1	oživován
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
lékař	lékař	k1gMnSc1	lékař
Stephan	Stephan	k1gMnSc1	Stephan
Lynn	Lynn	k1gMnSc1	Lynn
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lennon	Lennon	k1gMnSc1	Lennon
byl	být	k5eAaImAgMnS	být
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
již	již	k9	již
při	při	k7c6	při
přijetí	přijetí	k1gNnSc6	přijetí
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
ve	v	k7c4	v
23.15	[number]	k4	23.15
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
byla	být	k5eAaImAgFnS	být
ztráta	ztráta	k1gFnSc1	ztráta
tří	tři	k4xCgInPc2	tři
až	až	k9	až
čtyř	čtyři	k4xCgInPc2	čtyři
litrů	litr	k1gInPc2	litr
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
objemu	objem	k1gInSc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgMnSc1d1	vrchní
lékař	lékař	k1gMnSc1	lékař
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Elliott	Elliott	k1gMnSc1	Elliott
M.	M.	kA	M.
Gross	Gross	k1gMnSc1	Gross
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
takto	takto	k6eAd1	takto
těžkým	těžký	k2eAgNnSc7d1	těžké
zraněním	zranění	k1gNnSc7	zranění
nemůže	moct	k5eNaImIp3nS	moct
nikdo	nikdo	k3yNnSc1	nikdo
přežít	přežít	k5eAaPmF	přežít
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Lennon	Lennon	k1gInSc1	Lennon
byl	být	k5eAaImAgInS	být
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
zasažen	zasáhnout	k5eAaPmNgInS	zasáhnout
tříštivými	tříštivý	k2eAgFnPc7d1	tříštivá
střelami	střela	k1gFnPc7	střela
ráže	ráže	k1gFnSc2	ráže
.38	.38	k4	.38
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
proniknutí	proniknutí	k1gNnSc6	proniknutí
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
deformují	deformovat	k5eAaImIp3nP	deformovat
a	a	k8xC	a
působí	působit	k5eAaImIp3nP	působit
tak	tak	k6eAd1	tak
devastující	devastující	k2eAgNnSc4d1	devastující
zranění	zranění	k1gNnSc4	zranění
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
se	se	k3xPyFc4	se
nechala	nechat	k5eAaPmAgFnS	nechat
dovézt	dovézt	k5eAaPmF	dovézt
k	k	k7c3	k
manželovi	manžel	k1gMnSc3	manžel
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k1gMnSc4	mrtvý
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
odjela	odjet	k5eAaPmAgFnS	odjet
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
informovat	informovat	k5eAaBmF	informovat
syna	syn	k1gMnSc4	syn
Seana	Sean	k1gMnSc4	Sean
<g/>
.	.	kIx.	.
</s>
<s>
Lennonovo	Lennonův	k2eAgNnSc1d1	Lennonovo
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
zpopelněno	zpopelnit	k5eAaPmNgNnS	zpopelnit
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Ferncliff	Ferncliff	k1gMnSc1	Ferncliff
Cemetery	Cemeter	k1gInPc4	Cemeter
<g/>
,	,	kIx,	,
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Hartsdale	Hartsdala	k1gFnSc6	Hartsdala
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc4	ostatek
poté	poté	k6eAd1	poté
převzala	převzít	k5eAaPmAgFnS	převzít
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Možné	možný	k2eAgInPc4d1	možný
motivy	motiv	k1gInPc4	motiv
činu	čin	k1gInSc2	čin
===	===	k?	===
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zatčení	zatčení	k1gNnSc6	zatčení
Chapman	Chapman	k1gMnSc1	Chapman
na	na	k7c6	na
policejní	policejní	k2eAgFnSc6d1	policejní
stanici	stanice	k1gFnSc6	stanice
stručně	stručně	k6eAd1	stručně
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
motivech	motiv	k1gInPc6	motiv
svého	své	k1gNnSc2	své
činu	čin	k1gInSc2	čin
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
proslavit	proslavit	k5eAaPmF	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
jsem	být	k5eAaImIp1nS	být
uvést	uvést	k5eAaPmF	uvést
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
Salingerovu	Salingerův	k2eAgFnSc4d1	Salingerova
knihu	kniha	k1gFnSc4	kniha
Kdo	kdo	k3yRnSc1	kdo
chytá	chytat	k5eAaImIp3nS	chytat
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tu	ten	k3xDgFnSc4	ten
knihu	kniha	k1gFnSc4	kniha
přečtete	přečíst	k5eAaPmIp2nP	přečíst
a	a	k8xC	a
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
zajímáte	zajímat	k5eAaImIp2nP	zajímat
o	o	k7c4	o
můj	můj	k3xOp1gInSc4	můj
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
porozumíte	porozumět	k5eAaPmIp2nP	porozumět
<g/>
...	...	k?	...
Nachytal	nachytat	k5eAaBmAgMnS	nachytat
jsem	být	k5eAaImIp1nS	být
v	v	k7c6	v
žitě	žito	k1gNnSc6	žito
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgMnS	být
Chapman	Chapman	k1gMnSc1	Chapman
knihou	kniha	k1gFnSc7	kniha
zcela	zcela	k6eAd1	zcela
posedlý	posedlý	k2eAgMnSc1d1	posedlý
a	a	k8xC	a
líbily	líbit	k5eAaImAgFnP	líbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
názory	názor	k1gInPc1	názor
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
Holdena	Holden	k1gMnSc2	Holden
Caulfielda	Caulfield	k1gMnSc2	Caulfield
<g/>
,	,	kIx,	,
poukazujícího	poukazující	k2eAgMnSc4d1	poukazující
na	na	k7c4	na
faleš	faleš	k1gFnSc4	faleš
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgMnPc1d1	soudní
psychologové	psycholog	k1gMnPc1	psycholog
konstatovali	konstatovat	k5eAaBmAgMnP	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Chapman	Chapman	k1gMnSc1	Chapman
"	"	kIx"	"
<g/>
...	...	k?	...
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
velice	velice	k6eAd1	velice
citlivý	citlivý	k2eAgInSc1d1	citlivý
a	a	k8xC	a
časté	častý	k2eAgFnPc1d1	častá
hádky	hádka	k1gFnPc1	hádka
rodičů	rodič	k1gMnPc2	rodič
silně	silně	k6eAd1	silně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
jeho	jeho	k3xOp3gInSc4	jeho
duševní	duševní	k2eAgInSc4d1	duševní
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
ve	v	k7c6	v
velice	velice	k6eAd1	velice
raném	raný	k2eAgInSc6d1	raný
věku	věk	k1gInSc6	věk
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
vlastního	vlastní	k2eAgInSc2d1	vlastní
světa	svět	k1gInSc2	svět
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc7	The
Beatles	beatles	k1gMnSc7	beatles
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
dosti	dosti	k6eAd1	dosti
znelíbil	znelíbit	k5eAaPmAgMnS	znelíbit
svou	svůj	k3xOyFgFnSc7	svůj
známou	známý	k2eAgFnSc7d1	známá
poznámkou	poznámka	k1gFnSc7	poznámka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Beatles	Beatles	k1gFnPc1	Beatles
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
Ježíš	ježit	k5eAaImIp2nS	ježit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chapmanova	Chapmanův	k2eAgFnSc1d1	Chapmanova
manželka	manželka	k1gFnSc1	manželka
vypověděla	vypovědět	k5eAaPmAgFnS	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
nelíbilo	líbit	k5eNaImAgNnS	líbit
"	"	kIx"	"
<g/>
Lennonovo	Lennonův	k2eAgNnSc4d1	Lennonovo
kázání	kázání	k1gNnSc4	kázání
o	o	k7c6	o
míru	mír	k1gInSc6	mír
a	a	k8xC	a
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
měl	mít	k5eAaImAgInS	mít
přitom	přitom	k6eAd1	přitom
na	na	k7c6	na
kontě	konto	k1gNnSc6	konto
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
jachty	jachta	k1gFnSc2	jachta
a	a	k8xC	a
vily	vila	k1gFnSc2	vila
a	a	k8xC	a
smál	smát	k5eAaImAgMnS	smát
se	se	k3xPyFc4	se
lidem	člověk	k1gMnPc3	člověk
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
jako	jako	k9	jako
Chapman	Chapman	k1gMnSc1	Chapman
mu	on	k3xPp3gMnSc3	on
ty	ty	k3xPp2nSc5	ty
řeči	řeč	k1gFnSc2	řeč
baštili	baštit	k5eAaImAgMnP	baštit
a	a	k8xC	a
kupovali	kupovat	k5eAaImAgMnP	kupovat
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gFnSc2	jeho
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Teorie	teorie	k1gFnSc1	teorie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	se	k3xPyFc4	se
motivy	motiv	k1gInPc7	motiv
a	a	k8xC	a
duševním	duševní	k2eAgInSc7d1	duševní
stavem	stav	k1gInSc7	stav
Marka	Marek	k1gMnSc2	Marek
Chapmana	Chapman	k1gMnSc2	Chapman
v	v	k7c6	v
době	doba	k1gFnSc6	doba
činu	čin	k1gInSc2	čin
ale	ale	k9	ale
nebyly	být	k5eNaImAgInP	být
jednotné	jednotný	k2eAgInPc1d1	jednotný
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgFnP	lišit
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
protiřečily	protiřečit	k5eAaImAgFnP	protiřečit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
psychiatričky	psychiatrička	k1gFnSc2	psychiatrička
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Naomi	Nao	k1gFnPc7	Nao
Goldsteinové	Goldsteinová	k1gFnSc2	Goldsteinová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
vyšetřila	vyšetřit	k5eAaPmAgFnS	vyšetřit
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
Lennona	Lennon	k1gMnSc2	Lennon
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
kvůli	kvůli	k7c3	kvůli
neodbytné	odbytný	k2eNgFnSc3d1	neodbytná
touze	touha	k1gFnSc3	touha
po	po	k7c6	po
pozornosti	pozornost	k1gFnSc6	pozornost
a	a	k8xC	a
uznání	uznání	k1gNnSc6	uznání
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
oboru	obor	k1gInSc2	obor
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
,	,	kIx,	,
přítomní	přítomný	k1gMnPc1	přítomný
při	při	k7c6	při
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
soudním	soudní	k2eAgNnSc6d1	soudní
líčení	líčení	k1gNnSc6	líčení
<g/>
,	,	kIx,	,
s	s	k7c7	s
Goldsteinovou	Goldsteinová	k1gFnSc7	Goldsteinová
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Dodali	dodat	k5eAaPmAgMnP	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Chapman	Chapman	k1gMnSc1	Chapman
silně	silně	k6eAd1	silně
neurotického	neurotický	k2eAgNnSc2d1	neurotické
ražení	ražení	k1gNnSc2	ražení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
i	i	k9	i
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
uvědomit	uvědomit	k5eAaPmF	uvědomit
si	se	k3xPyFc3	se
důsledky	důsledek	k1gInPc4	důsledek
svého	svůj	k3xOyFgNnSc2	svůj
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
psychiatři	psychiatr	k1gMnPc1	psychiatr
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
obhajoby	obhajoba	k1gFnSc2	obhajoba
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Chapman	Chapman	k1gMnSc1	Chapman
trpí	trpět	k5eAaImIp3nS	trpět
schizofrenií	schizofrenie	k1gFnSc7	schizofrenie
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vraždy	vražda	k1gFnSc2	vražda
Lennona	Lennon	k1gMnSc2	Lennon
zabil	zabít	k5eAaPmAgMnS	zabít
tu	tu	k6eAd1	tu
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
rozdvojené	rozdvojený	k2eAgFnSc2d1	rozdvojená
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
působila	působit	k5eAaImAgFnS	působit
bolest	bolest	k1gFnSc4	bolest
–	–	k?	–
tedy	tedy	k8xC	tedy
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Chapman	Chapman	k1gMnSc1	Chapman
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
dotázán	dotázán	k2eAgMnSc1d1	dotázán
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
chce	chtít	k5eAaImIp3nS	chtít
něco	něco	k3yInSc4	něco
říci	říct	k5eAaPmF	říct
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
případu	případ	k1gInSc3	případ
<g/>
,	,	kIx,	,
přečetl	přečíst	k5eAaPmAgInS	přečíst
několik	několik	k4yIc4	několik
pasáží	pasáž	k1gFnPc2	pasáž
z	z	k7c2	z
uvedené	uvedený	k2eAgFnSc2d1	uvedená
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
vybrané	vybraný	k2eAgFnPc4d1	vybraná
pasáže	pasáž	k1gFnPc4	pasáž
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
hrdinu	hrdina	k1gMnSc4	hrdina
Caulfielda	Caulfield	k1gMnSc4	Caulfield
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
útesu	útes	k1gInSc2	útes
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zachránit	zachránit	k5eAaPmF	zachránit
okolo	okolo	k6eAd1	okolo
stojící	stojící	k2eAgFnPc4d1	stojící
děti	dítě	k1gFnPc4	dítě
před	před	k7c7	před
pádem	pád	k1gInSc7	pád
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
psychiatra	psychiatr	k1gMnSc2	psychiatr
Daniela	Daniel	k1gMnSc2	Daniel
W.	W.	kA	W.
Schwartze	Schwartze	k1gFnSc2	Schwartze
chtěl	chtít	k5eAaImAgMnS	chtít
Chapman	Chapman	k1gMnSc1	Chapman
Lennona	Lennon	k1gMnSc2	Lennon
zabít	zabít	k5eAaPmF	zabít
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
představoval	představovat	k5eAaImAgInS	představovat
jako	jako	k8xS	jako
zosobnění	zosobnění	k1gNnSc1	zosobnění
falešnosti	falešnost	k1gFnSc2	falešnost
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Chapman	Chapman	k1gMnSc1	Chapman
se	se	k3xPyFc4	se
také	také	k9	také
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
"	"	kIx"	"
<g/>
...	...	k?	...
slýchával	slýchávat	k5eAaImAgMnS	slýchávat
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
přikazovaly	přikazovat	k5eAaImAgFnP	přikazovat
zabít	zabít	k5eAaPmF	zabít
Lennona	Lennona	k1gFnSc1	Lennona
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Soudce	soudce	k1gMnSc1	soudce
Edwards	Edwardsa	k1gFnPc2	Edwardsa
se	se	k3xPyFc4	se
především	především	k6eAd1	především
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Chapmanova	Chapmanův	k2eAgNnSc2d1	Chapmanovo
rozvážného	rozvážný	k2eAgNnSc2d1	rozvážné
a	a	k8xC	a
klidného	klidný	k2eAgNnSc2d1	klidné
chování	chování	k1gNnSc2	chování
během	během	k7c2	během
líčení	líčení	k1gNnSc2	líčení
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Chapman	Chapman	k1gMnSc1	Chapman
závažnost	závažnost	k1gFnSc4	závažnost
svého	svůj	k3xOyFgNnSc2	svůj
jednání	jednání	k1gNnSc2	jednání
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
a	a	k8xC	a
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
činu	čin	k1gInSc2	čin
příčetný	příčetný	k2eAgInSc1d1	příčetný
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
shledán	shledat	k5eAaPmNgInS	shledat
vinným	vinný	k1gMnSc7	vinný
z	z	k7c2	z
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
doživotnímu	doživotní	k2eAgInSc3d1	doživotní
trestu	trest	k1gInSc3	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
podmínečného	podmínečný	k2eAgNnSc2d1	podmínečné
propuštění	propuštění	k1gNnSc2	propuštění
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
trest	trest	k1gInSc4	trest
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
odpykává	odpykávat	k5eAaImIp3nS	odpykávat
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
Wende	Wend	k1gInSc5	Wend
Correctional	Correctional	k1gFnPc2	Correctional
Facility	Facilit	k2eAgFnPc4d1	Facilit
v	v	k7c6	v
newyorském	newyorský	k2eAgInSc6d1	newyorský
okrese	okres	k1gInSc6	okres
Erie	Eri	k1gFnSc2	Eri
County	Counta	k1gFnSc2	Counta
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
podmínečné	podmínečný	k2eAgNnSc4d1	podmínečné
propuštění	propuštění	k1gNnSc4	propuštění
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
desetkrát	desetkrát	k6eAd1	desetkrát
zamítnuty	zamítnut	k2eAgFnPc1d1	zamítnuta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
slyšení	slyšení	k1gNnSc2	slyšení
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
podmínečného	podmínečný	k2eAgNnSc2d1	podmínečné
propuštění	propuštění	k1gNnSc2	propuštění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
Chapman	Chapman	k1gMnSc1	Chapman
dotázán	dotázat	k5eAaPmNgMnS	dotázat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
spáchal	spáchat	k5eAaPmAgInS	spáchat
vraždu	vražda	k1gFnSc4	vražda
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
slavným	slavný	k2eAgInSc7d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
Odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
značnou	značný	k2eAgFnSc4d1	značná
publicitu	publicita	k1gFnSc4	publicita
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
očekával	očekávat	k5eAaImAgMnS	očekávat
a	a	k8xC	a
že	že	k8xS	že
svého	svůj	k3xOyFgInSc2	svůj
činu	čin	k1gInSc2	čin
lituje	litovat	k5eAaImIp3nS	litovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
událost	událost	k1gFnSc4	událost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reakce	reakce	k1gFnPc1	reakce
médií	médium	k1gNnPc2	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Bezprostřední	bezprostřední	k2eAgFnPc1d1	bezprostřední
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
události	událost	k1gFnSc6	událost
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
moderátor	moderátor	k1gMnSc1	moderátor
ABC	ABC	kA	ABC
Ted	Ted	k1gMnSc1	Ted
Koppel	Koppel	k1gMnSc1	Koppel
v	v	k7c6	v
nočním	noční	k2eAgNnSc6d1	noční
vysílacím	vysílací	k2eAgNnSc6d1	vysílací
pásmu	pásmo	k1gNnSc6	pásmo
Nightline	Nightlin	k1gInSc5	Nightlin
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
NBC	NBC	kA	NBC
kvůli	kvůli	k7c3	kvůli
hlášení	hlášení	k1gNnSc3	hlášení
o	o	k7c6	o
Lennonově	Lennonův	k2eAgFnSc6d1	Lennonova
smrti	smrt	k1gFnSc6	smrt
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
přerušila	přerušit	k5eAaPmAgFnS	přerušit
pořad	pořad	k1gInSc4	pořad
The	The	k1gMnPc2	The
Tonight	Tonight	k1gMnSc1	Tonight
Show	show	k1gFnSc2	show
Starring	Starring	k1gInSc1	Starring
Johnny	Johnna	k1gFnSc2	Johnna
Carson	Carsona	k1gFnPc2	Carsona
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgMnSc1d1	sportovní
komentátor	komentátor	k1gMnSc1	komentátor
Howard	Howard	k1gMnSc1	Howard
Cosell	Cosell	k1gMnSc1	Cosell
z	z	k7c2	z
ABC	ABC	kA	ABC
News	News	k1gInSc1	News
obdržel	obdržet	k5eAaPmAgInS	obdržet
pokyn	pokyn	k1gInSc4	pokyn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
Lennonově	Lennonův	k2eAgFnSc6d1	Lennonova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
když	když	k8xS	když
komentoval	komentovat	k5eAaBmAgMnS	komentovat
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
zápas	zápas	k1gInSc4	zápas
mezi	mezi	k7c7	mezi
New	New	k1gMnSc7	New
England	Englanda	k1gFnPc2	Englanda
Patriots	Patriots	k1gInSc1	Patriots
a	a	k8xC	a
Miami	Miami	k1gNnSc1	Miami
Dolphins	Dolphinsa	k1gFnPc2	Dolphinsa
<g/>
.	.	kIx.	.
<g/>
Události	událost	k1gFnSc2	událost
byla	být	k5eAaImAgFnS	být
věnována	věnován	k2eAgFnSc1d1	věnována
pozornost	pozornost	k1gFnSc1	pozornost
i	i	k9	i
za	za	k7c7	za
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
tzv.	tzv.	kA	tzv.
železnou	železný	k2eAgFnSc7d1	železná
oponou	opona	k1gFnSc7	opona
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
noviny	novina	k1gFnPc1	novina
Komsomolskaja	Komsomolskaja	k1gFnSc1	Komsomolskaja
pravda	pravda	k1gFnSc1	pravda
například	například	k6eAd1	například
uvedly	uvést	k5eAaPmAgFnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hořká	hořký	k2eAgFnSc1d1	hořká
ironie	ironie	k1gFnSc1	ironie
této	tento	k3xDgFnSc2	tento
tragédie	tragédie	k1gFnSc2	tragédie
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
svou	svůj	k3xOyFgFnSc4	svůj
hudbu	hudba	k1gFnSc4	hudba
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
násilí	násilí	k1gNnSc3	násilí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
obětí	oběť	k1gFnSc7	oběť
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
v	v	k7c6	v
československém	československý	k2eAgInSc6d1	československý
deníku	deník	k1gInSc6	deník
Rudé	rudý	k2eAgNnSc4d1	Rudé
právo	právo	k1gNnSc4	právo
objevila	objevit	k5eAaPmAgFnS	objevit
krátká	krátký	k2eAgFnSc1d1	krátká
zpráva	zpráva	k1gFnSc1	zpráva
s	s	k7c7	s
titulkem	titulek	k1gInSc7	titulek
"	"	kIx"	"
<g/>
Nedal	dát	k5eNaPmAgMnS	dát
autogram	autogram	k1gInSc4	autogram
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
kulku	kulka	k1gFnSc4	kulka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
motivech	motiv	k1gInPc6	motiv
vraždy	vražda	k1gFnSc2	vražda
Rudé	rudý	k2eAgNnSc1d1	Rudé
právo	právo	k1gNnSc1	právo
informovalo	informovat	k5eAaBmAgNnS	informovat
dosti	dosti	k6eAd1	dosti
nepřesně	přesně	k6eNd1	přesně
tvrzením	tvrzení	k1gNnSc7	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Chapman	Chapman	k1gMnSc1	Chapman
střílel	střílet	k5eAaImAgMnS	střílet
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
Lennon	Lennon	k1gMnSc1	Lennon
opakovaně	opakovaně	k6eAd1	opakovaně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
dát	dát	k5eAaPmF	dát
autogram	autogram	k1gInSc4	autogram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reakce	reakce	k1gFnPc1	reakce
veřejnosti	veřejnost	k1gFnSc2	veřejnost
===	===	k?	===
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
události	událost	k1gFnSc6	událost
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
ohlas	ohlas	k1gInSc4	ohlas
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
odvysílání	odvysílání	k1gNnSc6	odvysílání
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
například	například	k6eAd1	například
také	také	k9	také
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
shromáždily	shromáždit	k5eAaPmAgFnP	shromáždit
tisíce	tisíc	k4xCgInPc1	tisíc
jeho	jeho	k3xOp3gMnPc2	jeho
fanoušků	fanoušek	k1gMnPc2	fanoušek
ke	k	k7c3	k
společné	společný	k2eAgFnSc3d1	společná
tryzně	tryzna	k1gFnSc3	tryzna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
na	na	k7c4	na
předchozí	předchozí	k2eAgFnSc4d1	předchozí
výzvu	výzva	k1gFnSc4	výzva
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
sešly	sejít	k5eAaPmAgFnP	sejít
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
po	po	k7c6	po
světě	svět	k1gInSc6	svět
miliony	milion	k4xCgInPc4	milion
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
tiché	tichý	k2eAgFnSc3d1	tichá
modlitbě	modlitba	k1gFnSc3	modlitba
za	za	k7c2	za
Johna	John	k1gMnSc4	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
Liverpoolu	Liverpool	k1gInSc6	Liverpool
se	se	k3xPyFc4	se
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
třicetitisícový	třicetitisícový	k2eAgInSc1d1	třicetitisícový
dav	dav	k1gInSc1	dav
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
největší	veliký	k2eAgNnSc1d3	veliký
shromáždění	shromáždění	k1gNnSc1	shromáždění
příznivců	příznivec	k1gMnPc2	příznivec
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
v	v	k7c6	v
newyorském	newyorský	k2eAgInSc6d1	newyorský
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
novináři	novinář	k1gMnPc1	novinář
dotázali	dotázat	k5eAaPmAgMnP	dotázat
bývalého	bývalý	k2eAgMnSc4d1	bývalý
člena	člen	k1gMnSc4	člen
The	The	k1gFnSc2	The
Beatles	beatles	k1gMnSc4	beatles
Paula	Paul	k1gMnSc4	Paul
McCartneyho	McCartney	k1gMnSc4	McCartney
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
vraždě	vražda	k1gFnSc6	vražda
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
opruz	opruz	k1gMnSc1	opruz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
drag	draga	k1gFnPc2	draga
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
odpověď	odpověď	k1gFnSc4	odpověď
byl	být	k5eAaImAgInS	být
McCartney	McCartnea	k1gFnSc2	McCartnea
kritizován	kritizován	k2eAgInSc1d1	kritizován
<g/>
,	,	kIx,	,
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obhajobu	obhajoba	k1gFnSc4	obhajoba
ale	ale	k8xC	ale
v	v	k7c6	v
interview	interview	k1gNnSc6	interview
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Playboy	playboy	k1gMnSc1	playboy
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Víte	vědět	k5eAaImIp2nP	vědět
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
jsem	být	k5eAaImIp1nS	být
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
šoku	šok	k1gInSc6	šok
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsem	být	k5eAaImIp1nS	být
prostě	prostě	k9	prostě
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
opruz	opruz	k1gMnSc1	opruz
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
mínil	mínit	k5eAaImAgMnS	mínit
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
nejvážnějším	vážní	k2eAgInSc6d3	nejvážnější
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
víte	vědět	k5eAaImIp2nP	vědět
<g/>
?	?	kIx.	?
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
–	–	k?	–
HRŮZA	hrůza	k1gFnSc1	hrůza
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
Lennona	Lennon	k1gMnSc2	Lennon
později	pozdě	k6eAd2	pozdě
pronesl	pronést	k5eAaPmAgMnS	pronést
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c4	na
Johna	John	k1gMnSc4	John
bude	být	k5eAaImBp3nS	být
vzpomínáno	vzpomínat	k5eAaImNgNnS	vzpomínat
především	především	k9	především
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
unikátnímu	unikátní	k2eAgInSc3d1	unikátní
přínosu	přínos	k1gInSc3	přínos
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc6	umění
a	a	k8xC	a
světovém	světový	k2eAgInSc6d1	světový
míru	mír	k1gInSc6	mír
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Památníky	památník	k1gInPc4	památník
a	a	k8xC	a
pocty	pocta	k1gFnPc4	pocta
===	===	k?	===
</s>
</p>
<p>
<s>
Osobě	osoba	k1gFnSc3	osoba
a	a	k8xC	a
odkazu	odkaz	k1gInSc2	odkaz
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pocty	pocta	k1gFnSc2	pocta
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
rozličných	rozličný	k2eAgInPc2d1	rozličný
památníků	památník	k1gInPc2	památník
a	a	k8xC	a
vzpomínkových	vzpomínkový	k2eAgNnPc2d1	vzpomínkové
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
památník	památník	k1gInSc4	památník
Strawberry	Strawberra	k1gFnSc2	Strawberra
Fields	Fieldsa	k1gFnPc2	Fieldsa
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
provoz	provoz	k1gInSc4	provoz
Yoko	Yoko	k1gNnSc4	Yoko
Ono	onen	k3xDgNnSc1	onen
věnovala	věnovat	k5eAaImAgFnS	věnovat
jeden	jeden	k4xCgMnSc1	jeden
milion	milion	k4xCgInSc4	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
schází	scházet	k5eAaImIp3nS	scházet
jednak	jednak	k8xC	jednak
na	na	k7c4	na
výročí	výročí	k1gNnSc4	výročí
Lennonovy	Lennonův	k2eAgFnSc2d1	Lennonova
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
výročí	výročí	k1gNnSc4	výročí
útoků	útok	k1gInPc2	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
či	či	k8xC	či
smrti	smrt	k1gFnSc6	smrt
George	Georg	k1gMnSc2	Georg
Harrisona	Harrison	k1gMnSc2	Harrison
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
textař	textař	k1gMnSc1	textař
Bernie	Bernie	k1gFnSc2	Bernie
Taupin	Taupin	k1gMnSc1	Taupin
společně	společně	k6eAd1	společně
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
vzpomínkovou	vzpomínkový	k2eAgFnSc4d1	vzpomínková
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Empty	Empt	k1gInPc1	Empt
Garden	Gardna	k1gFnPc2	Gardna
(	(	kIx(	(
<g/>
Hey	Hey	k1gFnSc1	Hey
Hey	Hey	k1gMnSc2	Hey
Johnny	Johnna	k1gMnSc2	Johnna
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
na	na	k7c6	na
albu	album	k1gNnSc6	album
Eltona	Elton	k1gMnSc2	Elton
Johna	John	k1gMnSc2	John
Jump	Jump	k1gMnSc1	Jump
Up	Up	k1gMnSc1	Up
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
V	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
singlových	singlový	k2eAgFnPc2d1	singlová
písní	píseň	k1gFnPc2	píseň
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Harrison	Harrison	k1gMnSc1	Harrison
<g/>
,	,	kIx,	,
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
vzpomínkovou	vzpomínkový	k2eAgFnSc4d1	vzpomínková
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
All	All	k1gFnSc1	All
Those	Those	k1gFnSc2	Those
Years	Yearsa	k1gFnPc2	Yearsa
Ago	aga	k1gMnSc5	aga
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bubny	buben	k1gInPc4	buben
hrál	hrát	k5eAaImAgInS	hrát
Ringo	Ringo	k6eAd1	Ringo
Starr	Starr	k1gInSc1	Starr
a	a	k8xC	a
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
<s>
McCartney	McCartnea	k1gFnPc4	McCartnea
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
vydal	vydat	k5eAaPmAgInS	vydat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
píseň	píseň	k1gFnSc4	píseň
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
Lennonovi	Lennon	k1gMnSc6	Lennon
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
Tug	Tug	k1gMnPc4	Tug
of	of	k?	of
War	War	k1gFnSc1	War
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Here	Here	k1gInSc1	Here
Today	Todaa	k1gFnSc2	Todaa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
Lennon	Lennon	k1gInSc1	Lennon
posmrtně	posmrtně	k6eAd1	posmrtně
oceněn	ocenit	k5eAaPmNgInS	ocenit
cenou	cena	k1gFnSc7	cena
Grammy	Gramm	k1gInPc4	Gramm
za	za	k7c4	za
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
kubánský	kubánský	k2eAgMnSc1d1	kubánský
prezident	prezident	k1gMnSc1	prezident
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
slavnostně	slavnostně	k6eAd1	slavnostně
odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
sochu	socha	k1gFnSc4	socha
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
havanských	havanský	k2eAgInPc2d1	havanský
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
městě	město	k1gNnSc6	město
Saitama	Saitamum	k1gNnSc2	Saitamum
otevřeno	otevřen	k2eAgNnSc4d1	otevřeno
Muzeum	muzeum	k1gNnSc4	muzeum
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
Museum	museum	k1gNnSc4	museum
<g/>
)	)	kIx)	)
a	a	k8xC	a
město	město	k1gNnSc1	město
Liverpool	Liverpool	k1gInSc4	Liverpool
přejmenovalo	přejmenovat	k5eAaPmAgNnS	přejmenovat
své	svůj	k3xOyFgFnPc4	svůj
letiště	letiště	k1gNnSc1	letiště
na	na	k7c4	na
Letiště	letiště	k1gNnSc4	letiště
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
Liverpool	Liverpool	k1gInSc4	Liverpool
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
mottem	motto	k1gNnSc7	motto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
slogan	slogan	k1gInSc4	slogan
"	"	kIx"	"
<g/>
Above	Aboev	k1gFnSc2	Aboev
us	us	k?	us
only	onla	k1gFnSc2	onla
sky	sky	k?	sky
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Nád	Nád	k1gFnSc1	Nád
námi	my	k3xPp1nPc7	my
jen	jen	k9	jen
nebe	nebe	k1gNnSc2	nebe
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Lennonovi	Lennon	k1gMnSc6	Lennon
je	být	k5eAaImIp3nS	být
též	též	k6eAd1	též
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
(	(	kIx(	(
<g/>
4147	[number]	k4	4147
<g/>
)	)	kIx)	)
Lennon	Lennona	k1gFnPc2	Lennona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1983	[number]	k4	1983
B.	B.	kA	B.
A.	A.	kA	A.
Skiffem	Skiff	k1gInSc7	Skiff
na	na	k7c4	na
Anderson	Anderson	k1gMnSc1	Anderson
Mesa	Mes	k1gInSc2	Mes
Station	station	k1gInSc1	station
patřící	patřící	k2eAgInSc1d1	patřící
Lowellově	Lowellův	k2eAgFnSc3d1	Lowellova
observatoři	observatoř	k1gFnSc3	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc4	onen
darovala	darovat	k5eAaPmAgFnS	darovat
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
islandskému	islandský	k2eAgInSc3d1	islandský
ostrovu	ostrov	k1gInSc3	ostrov
Við	Við	k1gFnSc2	Við
pomník	pomník	k1gInSc1	pomník
Imagine	Imagin	k1gInSc5	Imagin
Peace	Peace	k1gMnPc4	Peace
Tower	Tower	k1gInSc4	Tower
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
mezi	mezi	k7c7	mezi
9	[number]	k4	9
<g/>
.	.	kIx.	.
říjnem	říjen	k1gInSc7	říjen
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
silný	silný	k2eAgInSc1d1	silný
paprsek	paprsek	k1gInSc1	paprsek
světla	světlo	k1gNnSc2	světlo
k	k	k7c3	k
nebi	nebe	k1gNnSc3	nebe
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
J.	J.	kA	J.
P.	P.	kA	P.
Schaefer	Schaefer	k1gInSc4	Schaefer
nezávislý	závislý	k2eNgInSc1d1	nezávislý
film	film	k1gInSc1	film
Chapter	Chapter	k1gInSc1	Chapter
27	[number]	k4	27
(	(	kIx(	(
<g/>
Zavraždění	zavraždění	k1gNnSc4	zavraždění
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pachatele	pachatel	k1gMnSc4	pachatel
Chapmana	Chapman	k1gMnSc4	Chapman
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Jared	Jared	k1gMnSc1	Jared
Leto	Leto	k1gMnSc1	Leto
<g/>
,	,	kIx,	,
postavu	postava	k1gFnSc4	postava
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
herec	herec	k1gMnSc1	herec
jménem	jméno	k1gNnSc7	jméno
Mark	Mark	k1gMnSc1	Mark
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Lennonova	Lennonův	k2eAgFnSc1d1	Lennonova
zeď	zeď	k1gFnSc1	zeď
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
se	se	k3xPyFc4	se
u	u	k7c2	u
zdi	zeď	k1gFnSc2	zeď
Maltézské	maltézský	k2eAgFnSc2d1	Maltézská
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
objevil	objevit	k5eAaPmAgMnS	objevit
jeho	jeho	k3xOp3gInSc4	jeho
symbolický	symbolický	k2eAgInSc4d1	symbolický
hrob	hrob	k1gInSc4	hrob
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgMnSc3	jenž
lidé	člověk	k1gMnPc1	člověk
nosili	nosit	k5eAaImAgMnP	nosit
různé	různý	k2eAgFnPc4d1	různá
květiny	květina	k1gFnPc4	květina
<g/>
,	,	kIx,	,
svíčky	svíčka	k1gFnPc4	svíčka
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
na	na	k7c6	na
okolních	okolní	k2eAgFnPc6d1	okolní
zdech	zeď	k1gFnPc6	zeď
začala	začít	k5eAaPmAgFnS	začít
objevovat	objevovat	k5eAaImF	objevovat
graffiti	graffiti	k1gNnSc4	graffiti
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
Lennonem	Lennon	k1gInSc7	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
objevovaly	objevovat	k5eAaImAgFnP	objevovat
útržky	útržka	k1gFnPc1	útržka
textů	text	k1gInPc2	text
písní	píseň	k1gFnPc2	píseň
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnPc1	Beatles
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
jiné	jiný	k2eAgInPc1d1	jiný
vzkazy	vzkaz	k1gInPc1	vzkaz
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
jako	jako	k8xC	jako
Lennonova	Lennonův	k2eAgFnSc1d1	Lennonova
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
několikrát	několikrát	k6eAd1	několikrát
na	na	k7c4	na
popud	popud	k1gInSc4	popud
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
přebarvena	přebarven	k2eAgFnSc1d1	přebarven
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
zcela	zcela	k6eAd1	zcela
lichá	lichý	k2eAgFnSc1d1	lichá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
však	však	k9	však
stala	stát	k5eAaPmAgFnS	stát
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
a	a	k8xC	a
původní	původní	k2eAgInSc1d1	původní
idealistický	idealistický	k2eAgInSc1d1	idealistický
význam	význam	k1gInSc1	význam
zdi	zeď	k1gFnSc2	zeď
se	se	k3xPyFc4	se
tak	tak	k9	tak
postupně	postupně	k6eAd1	postupně
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Death	Death	k1gMnSc1	Death
of	of	k?	of
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BRESLER	BRESLER	kA	BRESLER
<g/>
,	,	kIx,	,
Fenton	Fenton	k1gInSc1	Fenton
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
zabil	zabít	k5eAaPmAgMnS	zabít
Johna	John	k1gMnSc4	John
Lennona	Lennon	k1gMnSc4	Lennon
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Vimperk	Vimperk	k1gInSc1	Vimperk
<g/>
:	:	kIx,	:
Papyrus	papyrus	k1gInSc1	papyrus
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901273	[number]	k4	901273
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
MERLE	MERLE	kA	MERLE
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gInSc5	Pierr
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
490	[number]	k4	490
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEAMAN	SEAMAN	kA	SEAMAN
<g/>
,	,	kIx,	,
Frederic	Frederic	k1gMnSc1	Frederic
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dny	den	k1gInPc1	den
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROSEN	rosen	k2eAgMnSc1d1	rosen
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Nowhere	Nowhrat	k5eAaPmIp3nS	Nowhrat
Man	Man	k1gMnSc1	Man
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Final	Final	k1gMnSc1	Final
Days	Daysa	k1gFnPc2	Daysa
of	of	k?	of
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
.	.	kIx.	.
</s>
<s>
Oakland	Oakland	k1gInSc1	Oakland
<g/>
:	:	kIx,	:
Quick	Quick	k1gMnSc1	Quick
American	American	k1gMnSc1	American
Archives	Archives	k1gMnSc1	Archives
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
211	[number]	k4	211
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
932551	[number]	k4	932551
<g/>
-	-	kIx~	-
<g/>
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Jaromír	Jaromír	k1gMnSc1	Jaromír
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Maňourová	Maňourový	k2eAgFnSc1d1	Maňourová
Anna	Anna	k1gFnSc1	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
si	se	k3xPyFc3	se
připomíná	připomínat	k5eAaImIp3nS	připomínat
památku	památka	k1gFnSc4	památka
Johna	John	k1gMnSc2	John
Lennona	Lennon	k1gMnSc2	Lennon
<g/>
,	,	kIx,	,
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
33	[number]	k4	33
lety	let	k1gInPc7	let
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2013	[number]	k4	2013
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
Lennona	Lennon	k1gMnSc2	Lennon
–	–	k?	–
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	s	k7c7	s
mnohým	mnohé	k1gNnSc7	mnohé
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
svět	svět	k1gInSc1	svět
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2010	[number]	k4	2010
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
