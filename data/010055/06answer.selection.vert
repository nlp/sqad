<s>
John	John	k1gMnSc1	John
Lennon	Lennon	k1gMnSc1	Lennon
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgMnSc1d1	slavný
britský	britský	k2eAgMnSc1d1	britský
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
především	především	k6eAd1	především
jako	jako	k8xS	jako
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Beatles	beatles	k1gMnSc1	beatles
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1980	[number]	k4	1980
před	před	k7c7	před
newyorským	newyorský	k2eAgInSc7d1	newyorský
domem	dům	k1gInSc7	dům
The	The	k1gFnSc2	The
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
bydlel	bydlet	k5eAaImAgMnS	bydlet
<g/>
.	.	kIx.	.
</s>
