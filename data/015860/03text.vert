<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
ANO	ano	k9
2011	#num#	k4
</s>
<s>
Zkratka	zkratka	k1gFnSc1
ANO	ano	k9
</s>
<s>
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
Předseda	předseda	k1gMnSc1
</s>
<s>
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
</s>
<s>
neobsazeno	obsazen	k2eNgNnSc1d1
Tiskový	tiskový	k2eAgMnSc1d1
mluvčí	mluvčí	k1gMnSc1
</s>
<s>
Lucie	Lucie	k1gFnSc1
Kubovičová	Kubovičový	k2eAgFnSc1d1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Babická	Babická	k1gFnSc1
2329	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
149	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Ideologie	ideologie	k1gFnSc2
</s>
<s>
populismus	populismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
korporativismus	korporativismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Typ	typ	k1gInSc1
strany	strana	k1gFnSc2
</s>
<s>
všelidová	všelidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
catch-all	catch-all	k1gInSc1
party	parta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
strana	strana	k1gFnSc1
typu	typ	k1gInSc2
firmy	firma	k1gFnSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Stát	stát	k1gInSc1
<g/>
.	.	kIx.
příspěvek	příspěvek	k1gInSc1
(	(	kIx(
<g/>
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
)	)	kIx)
</s>
<s>
145,9	145,9	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Aliance	aliance	k1gFnSc1
liberálů	liberál	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
Politická	politický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
EP	EP	kA
</s>
<s>
Obnova	obnova	k1gFnSc1
Evropy	Evropa	k1gFnSc2
Mládežnická	mládežnický	k2eAgFnSc1d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Mladé	mladé	k1gNnSc1
ANO	ano	k9
Počet	počet	k1gInSc1
členů	člen	k1gMnPc2
</s>
<s>
3	#num#	k4
300	#num#	k4
(	(	kIx(
<g/>
únor	únor	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Slogan	slogan	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Ano	ano	k9
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
líp	dobře	k6eAd2
<g/>
“	“	k?
Barvy	barva	k1gFnSc2
</s>
<s>
tmavě	tmavě	k6eAd1
modrá	modrat	k5eAaImIp3nS
Volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
</s>
<s>
29,64	29,64	k4
%	%	kIx~
(	(	kIx(
<g/>
PSP	PSP	kA
ČR	ČR	kA
2017	#num#	k4
<g/>
)	)	kIx)
IČO	IČO	kA
</s>
<s>
71443339	#num#	k4
(	(	kIx(
<g/>
PSH	PSH	kA
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.anobudelip.cz	www.anobudelip.cz	k1gInSc1
Zisk	zisk	k1gInSc1
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Sněmovna	sněmovna	k1gFnSc1
<g/>
2017	#num#	k4
</s>
<s>
78	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
Senát	senát	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
2019	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
Zastupitelstva	zastupitelstvo	k1gNnPc1
krajů	kraj	k1gInPc2
<g/>
2020	#num#	k4
</s>
<s>
178	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Zastupitelstva	zastupitelstvo	k1gNnPc1
obcí	obec	k1gFnPc2
<g/>
2018	#num#	k4
</s>
<s>
1676	#num#	k4
<g/>
/	/	kIx~
<g/>
62300	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Zastupitelstvo	zastupitelstvo	k1gNnSc1
Prahy	Praha	k1gFnSc2
<g/>
2018	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
ANO	ano	k9
2011	#num#	k4
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ANO	ano	k9
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
české	český	k2eAgNnSc1d1
populistické	populistický	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
politické	politický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
(	(	kIx(
<g/>
často	často	k6eAd1
označované	označovaný	k2eAgFnPc4d1
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
hnutí	hnutí	k1gNnPc4
ANO	ano	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
založil	založit	k5eAaPmAgMnS
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc2
ANO	ano	k9
2011	#num#	k4
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
občanské	občanský	k2eAgNnSc4d1
sdružení	sdružení	k1gNnSc4
Akce	akce	k1gFnSc2
nespokojených	spokojený	k2eNgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
krátce	krátce	k6eAd1
předtím	předtím	k6eAd1
na	na	k7c4
podzim	podzim	k1gInSc4
2011	#num#	k4
po	po	k7c6
několika	několik	k4yIc2
veřejných	veřejný	k2eAgNnPc6d1
vystoupeních	vystoupení	k1gNnPc6
Andreje	Andrej	k1gMnSc2
Babiše	Babicha	k1gFnSc6
ve	v	k7c6
sdělovacích	sdělovací	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
velmi	velmi	k6eAd1
kriticky	kriticky	k6eAd1
hodnotil	hodnotit	k5eAaImAgMnS
stav	stav	k1gInSc4
společnosti	společnost	k1gFnSc2
a	a	k8xC
silně	silně	k6eAd1
kritizoval	kritizovat	k5eAaImAgMnS
korupci	korupce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
se	se	k3xPyFc4
zaměřilo	zaměřit	k5eAaPmAgNnS
na	na	k7c4
voliče	volič	k1gMnPc4
nespokojené	spokojený	k2eNgInPc1d1
s	s	k7c7
dosavadními	dosavadní	k2eAgInPc7d1
etablovanými	etablovaný	k2eAgInPc7d1
politickými	politický	k2eAgInPc7d1
subjekty	subjekt	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgInPc4d1
deklarované	deklarovaný	k2eAgInPc4d1
cíle	cíl	k1gInPc4
ANO	ano	k9
2011	#num#	k4
patří	patřit	k5eAaImIp3nP
prosazení	prosazení	k1gNnSc4
fungujícího	fungující	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
boj	boj	k1gInSc4
s	s	k7c7
korupcí	korupce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladatel	zakladatel	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
počátku	počátek	k1gInSc2
předsedou	předseda	k1gMnSc7
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
března	březen	k1gInSc2
2013	#num#	k4
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
první	první	k4xOgFnSc1
místopředsedkyní	místopředsedkyně	k1gFnPc2
Věra	Věra	k1gFnSc1
Jourová	Jourová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ale	ale	k9
v	v	k7c6
říjnu	říjen	k1gInSc6
2014	#num#	k4
v	v	k7c6
důsledku	důsledek	k1gInSc6
svého	svůj	k3xOyFgNnSc2
schválení	schválení	k1gNnSc2
do	do	k7c2
funkce	funkce	k1gFnSc2
evropské	evropský	k2eAgFnSc2d1
komisařky	komisařka	k1gFnSc2
z	z	k7c2
vedení	vedení	k1gNnSc2
hnutí	hnutí	k1gNnSc2
odešla	odejít	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Dalšími	další	k2eAgFnPc7d1
tvářemi	tvář	k1gFnPc7
strany	strana	k1gFnPc4
jsou	být	k5eAaImIp3nP
mj.	mj.	kA
předseda	předseda	k1gMnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Radek	Radek	k1gMnSc1
Vondráček	Vondráček	k1gMnSc1
a	a	k8xC
předseda	předseda	k1gMnSc1
poslanců	poslanec	k1gMnPc2
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
Jaroslav	Jaroslav	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Deklarovaným	deklarovaný	k2eAgInSc7d1
cílem	cíl	k1gInSc7
hnutí	hnutí	k1gNnSc2
je	být	k5eAaImIp3nS
zúčastňovat	zúčastňovat	k5eAaImF
se	se	k3xPyFc4
voleb	volba	k1gFnPc2
na	na	k7c6
všech	všecek	k3xTgFnPc6
úrovních	úroveň	k1gFnPc6
a	a	k8xC
prosazovat	prosazovat	k5eAaImF
více	hodně	k6eAd2
podpůrné	podpůrný	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
pro	pro	k7c4
podnikatele	podnikatel	k1gMnPc4
a	a	k8xC
živnostníky	živnostník	k1gMnPc4
<g/>
,	,	kIx,
odstranit	odstranit	k5eAaPmF
korupci	korupce	k1gFnSc4
a	a	k8xC
politikaření	politikařený	k2eAgMnPc1d1
a	a	k8xC
podpořit	podpořit	k5eAaPmF
růst	růst	k1gInSc4
zaměstnanosti	zaměstnanost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předseda	předseda	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
počátku	počátek	k1gInSc2
dominantní	dominantní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
je	on	k3xPp3gNnSc4
popsal	popsat	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
„	„	k?
<g/>
pravicovou	pravicový	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
se	s	k7c7
sociálním	sociální	k2eAgNnSc7d1
cítěním	cítění	k1gNnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
2014	#num#	k4
bylo	být	k5eAaImAgNnS
hnutí	hnutí	k1gNnSc1
ANO	ano	k9
na	na	k7c4
35	#num#	k4
<g/>
.	.	kIx.
kongresu	kongres	k1gInSc2
Aliance	aliance	k1gFnSc1
liberálů	liberál	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
(	(	kIx(
<g/>
ALDE	ALDE	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
působí	působit	k5eAaImIp3nS
jako	jako	k9
politický	politický	k2eAgInSc4d1
subjekt	subjekt	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
přijato	přijmout	k5eAaPmNgNnS
za	za	k7c4
jejího	její	k3xOp3gMnSc4
člena	člen	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
ANO	ano	k9
2011	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
únoru	únor	k1gInSc6
2019	#num#	k4
definoval	definovat	k5eAaBmAgMnS
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gInSc4
hnutí	hnutí	k1gNnSc2
jako	jako	k8xC,k8xS
„	„	k?
<g/>
catch	catch	k1gInSc1
all	all	k?
party	party	k1gFnSc1
<g/>
“	“	k?
–	–	k?
stranu	strana	k1gFnSc4
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
stranické	stranický	k2eAgFnSc2d1
typologie	typologie	k1gFnSc2
je	být	k5eAaImIp3nS
ANO	ano	k9
2011	#num#	k4
řazeno	řadit	k5eAaImNgNnS
mezi	mezi	k7c4
strany	strana	k1gFnPc4
typu	typ	k1gInSc2
firmy	firma	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
vzniku	vznik	k1gInSc6
hnutí	hnutí	k1gNnSc2
výrazně	výrazně	k6eAd1
uspělo	uspět	k5eAaPmAgNnS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
získalo	získat	k5eAaPmAgNnS
18,65	18,65	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
47	#num#	k4
mandátů	mandát	k1gInPc2
<g/>
,	,	kIx,
umístilo	umístit	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
za	za	k7c7
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
vytvořily	vytvořit	k5eAaPmAgFnP
vládní	vládní	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
společně	společně	k6eAd1
s	s	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
,	,	kIx,
několik	několik	k4yIc1
kandidátů	kandidát	k1gMnPc2
ANO	ano	k9
2011	#num#	k4
neuspělo	uspět	k5eNaPmAgNnS
v	v	k7c6
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
2017	#num#	k4
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
2011	#num#	k4
vyhrálo	vyhrát	k5eAaPmAgNnS
se	s	k7c7
ziskem	zisk	k1gInSc7
29,64	29,64	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
78	#num#	k4
mandátů	mandát	k1gInPc2
z	z	k7c2
200	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
vládne	vládnout	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
druhá	druhý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
Andreje	Andrej	k1gMnSc4
Babiše	Babiš	k1gMnSc4
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
jsou	být	k5eAaImIp3nP
zastoupeni	zastoupen	k2eAgMnPc1d1
politici	politik	k1gMnPc1
za	za	k7c4
ANO	ano	k9
2011	#num#	k4
a	a	k8xC
pět	pět	k4xCc1
ministrů	ministr	k1gMnPc2
za	za	k7c4
Českou	český	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
sociálně	sociálně	k6eAd1
demokratickou	demokratický	k2eAgFnSc7d1
(	(	kIx(
<g/>
ČSSD	ČSSD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládu	vláda	k1gFnSc4
podporovala	podporovat	k5eAaImAgFnS
od	od	k7c2
července	červenec	k1gInSc2
2018	#num#	k4
do	do	k7c2
dubna	duben	k1gInSc2
2021	#num#	k4
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
KSČM	KSČM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Programové	programový	k2eAgFnPc1d1
teze	teze	k1gFnPc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Sdružení	sdružení	k1gNnSc1
vystupovalo	vystupovat	k5eAaImAgNnS
původně	původně	k6eAd1
pod	pod	k7c7
následujícími	následující	k2eAgFnPc7d1
výzvami	výzva	k1gFnPc7
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chceme	chtít	k5eAaImIp1nP
funkční	funkční	k2eAgInSc1d1
a	a	k8xC
právní	právní	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Chceme	chtít	k5eAaImIp1nP
spravedlivou	spravedlivý	k2eAgFnSc4d1
a	a	k8xC
nezadluženou	zadlužený	k2eNgFnSc4d1
společnost	společnost	k1gFnSc4
</s>
<s>
Chceme	chtít	k5eAaImIp1nP
budoucnost	budoucnost	k1gFnSc4
bez	bez	k7c2
korupce	korupce	k1gFnSc2
</s>
<s>
Chceme	chtít	k5eAaImIp1nP
výkonnou	výkonný	k2eAgFnSc4d1
veřejnou	veřejný	k2eAgFnSc4d1
správu	správa	k1gFnSc4
</s>
<s>
Chceme	chtít	k5eAaImIp1nP
lepší	dobrý	k2eAgFnSc4d2
budoucnost	budoucnost	k1gFnSc4
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
děti	dítě	k1gFnPc4
</s>
<s>
Chcete	chtít	k5eAaImIp2nP
se	se	k3xPyFc4
podílet	podílet	k5eAaImF
na	na	k7c6
změnách	změna	k1gFnPc6
v	v	k7c6
politice	politika	k1gFnSc6
a	a	k8xC
společnosti	společnost	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s>
Předvolební	předvolební	k2eAgInSc1d1
míting	míting	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
Lípě	lípa	k1gFnSc6
<g/>
,	,	kIx,
září	zářit	k5eAaImIp3nS
2013	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
teze	teze	k1gFnSc1
k	k	k7c3
15	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
byly	být	k5eAaImAgInP
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aby	aby	k9
stát	stát	k1gInSc4
začali	začít	k5eAaPmAgMnP
řídit	řídit	k5eAaImF
zkušení	zkušený	k2eAgMnPc1d1
<g/>
,	,	kIx,
slušní	slušný	k2eAgMnPc1d1
a	a	k8xC
odpovědní	odpovědný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
prokázali	prokázat	k5eAaPmAgMnP
své	svůj	k3xOyFgFnPc4
schopnosti	schopnost	k1gFnPc4
</s>
<s>
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
dalo	dát	k5eAaPmAgNnS
slušně	slušně	k6eAd1
žít	žít	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nejen	nejen	k6eAd1
odborníci	odborník	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
naše	náš	k3xOp1gFnPc1
děti	dítě	k1gFnPc1
neodcházely	odcházet	k5eNaImAgFnP
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
</s>
<s>
Aby	aby	kYmCp3nS
skončilo	skončit	k5eAaPmAgNnS
systémové	systémový	k2eAgNnSc1d1
rozkrádání	rozkrádání	k1gNnSc1
státu	stát	k1gInSc2
pod	pod	k7c7
dohledem	dohled	k1gInSc7
stávajících	stávající	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
</s>
<s>
Aby	aby	k9
politici	politik	k1gMnPc1
na	na	k7c4
občany	občan	k1gMnPc4
tupě	tupě	k6eAd1
nepřenášeli	přenášet	k5eNaImAgMnP
náklady	náklad	k1gInPc4
za	za	k7c4
svoji	svůj	k3xOyFgFnSc4
neschopnost	neschopnost	k1gFnSc4
a	a	k8xC
lhostejnost	lhostejnost	k1gFnSc4
</s>
<s>
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
kvůli	kvůli	k7c3
reformním	reformní	k2eAgInPc3d1
„	„	k?
<g/>
pokusům-omylům	pokusům-omyl	k1gInPc3
<g/>
“	“	k?
vlády	vláda	k1gFnSc2
nezhroutil	zhroutit	k5eNaPmAgInS
důchodový	důchodový	k2eAgInSc1d1
<g/>
,	,	kIx,
zdravotnický	zdravotnický	k2eAgInSc1d1
či	či	k8xC
školský	školský	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Politické	politický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
ANO	ano	k9
2011	#num#	k4
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
sdružení	sdružení	k1gNnPc4
Akce	akce	k1gFnSc2
nespokojených	spokojený	k2eNgMnPc2d1
občanů	občan	k1gMnPc2
2011	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
současnosti	současnost	k1gFnSc6
pod	pod	k7c7
zkratkou	zkratka	k1gFnSc7
ANO	ano	k9
<g/>
)	)	kIx)
založené	založený	k2eAgFnPc4d1
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
podnikatelem	podnikatel	k1gMnSc7
Andrejem	Andrej	k1gMnSc7
Babišem	Babiš	k1gMnSc7
na	na	k7c4
podporu	podpora	k1gFnSc4
jeho	jeho	k3xOp3gFnSc2
kritiky	kritika	k1gFnSc2
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
společnosti	společnost	k1gFnSc2
a	a	k8xC
korupce	korupce	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
začal	začít	k5eAaPmAgMnS
vystupovat	vystupovat	k5eAaImF
ve	v	k7c6
sdělovacích	sdělovací	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
na	na	k7c4
podzim	podzim	k1gInSc4
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdružení	sdružení	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
rychle	rychle	k6eAd1
podporu	podpora	k1gFnSc4
několika	několik	k4yIc2
tisíc	tisíc	k4xCgInPc2
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
dni	den	k1gInSc3
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
19	#num#	k4
dní	den	k1gInPc2
po	po	k7c6
založení	založení	k1gNnSc6
sdružení	sdružení	k1gNnPc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
pod	pod	k7c7
výzvou	výzva	k1gFnSc7
podepsáno	podepsat	k5eAaPmNgNnS
10	#num#	k4
142	#num#	k4
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2011	#num#	k4
se	se	k3xPyFc4
podpora	podpora	k1gFnSc1
dále	daleko	k6eAd2
zvýšila	zvýšit	k5eAaPmAgFnS
na	na	k7c4
17	#num#	k4
161	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sdružení	sdružení	k1gNnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
během	během	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
transformovalo	transformovat	k5eAaBmAgNnS
na	na	k7c4
politické	politický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
ANO	ano	k9
2011	#num#	k4
registrované	registrovaný	k2eAgFnSc2d1
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2012	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
2013	#num#	k4
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2013	#num#	k4
hnutí	hnutí	k1gNnSc1
vypustilo	vypustit	k5eAaPmAgNnS
ze	z	k7c2
zkratky	zkratka	k1gFnSc2
rok	rok	k1gInSc4
založení	založení	k1gNnSc2
a	a	k8xC
prezentuje	prezentovat	k5eAaBmIp3nS
se	se	k3xPyFc4
dále	daleko	k6eAd2
jen	jen	k9
jako	jako	k9
ANO	ano	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Se	s	k7c7
samostatnou	samostatný	k2eAgFnSc7d1
kandidátní	kandidátní	k2eAgFnSc7d1
listinou	listina	k1gFnSc7
hnutí	hnutí	k1gNnSc1
poprvé	poprvé	k6eAd1
kandidovalo	kandidovat	k5eAaImAgNnS
v	v	k7c6
předčasných	předčasný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
konaných	konaný	k2eAgMnPc2d1
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nich	on	k3xPp3gInPc6
obdrželo	obdržet	k5eAaPmAgNnS
927	#num#	k4
240	#num#	k4
hlasů	hlas	k1gInPc2
(	(	kIx(
<g/>
18,65	18,65	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
se	se	k3xPyFc4
47	#num#	k4
mandáty	mandát	k1gInPc7
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
druhou	druhý	k4xOgFnSc7
nejsilnější	silný	k2eAgFnSc7d3
volební	volební	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnPc2
ANO	ano	k9
bylo	být	k5eAaImAgNnS
nejúspěšnější	úspěšný	k2eAgFnSc7d3
volební	volební	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
ve	v	k7c6
Středočeském	středočeský	k2eAgNnSc6d1
<g/>
,	,	kIx,
Libereckém	liberecký	k2eAgNnSc6d1
<g/>
,	,	kIx,
Královéhradeckém	královéhradecký	k2eAgMnSc6d1
a	a	k8xC
Ústeckém	ústecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
hnutí	hnutí	k1gNnSc2
byl	být	k5eAaImAgInS
nejúspěšnějším	úspěšný	k2eAgMnSc7d3
kandidátem	kandidát	k1gMnSc7
lídr	lídr	k1gMnSc1
pražské	pražský	k2eAgFnSc2d1
kandidátní	kandidátní	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
s	s	k7c7
18	#num#	k4
955	#num#	k4
preferenčními	preferenční	k2eAgInPc7d1
hlasy	hlas	k1gInPc7
(	(	kIx(
<g/>
19,62	19,62	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
za	za	k7c7
ním	on	k3xPp3gMnSc7
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgMnS
herec	herec	k1gMnSc1
Martin	Martin	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vedl	vést	k5eAaImAgMnS
jihomoravskou	jihomoravský	k2eAgFnSc4d1
kandidátní	kandidátní	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
a	a	k8xC
získal	získat	k5eAaPmAgMnS
10	#num#	k4
203	#num#	k4
(	(	kIx(
<g/>
10,25	10,25	k4
%	%	kIx~
<g/>
)	)	kIx)
preferenčních	preferenční	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Program	program	k1gInSc1
a	a	k8xC
veřejná	veřejný	k2eAgFnSc1d1
prezentace	prezentace	k1gFnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Během	během	k7c2
předvolební	předvolební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
2011	#num#	k4
zdůrazňovalo	zdůrazňovat	k5eAaImAgNnS
novost	novost	k1gFnSc4
a	a	k8xC
nepolitičnost	nepolitičnost	k1gFnSc4
svých	svůj	k3xOyFgMnPc2
kandidátů	kandidát	k1gMnPc2
v	v	k7c6
kontrastu	kontrast	k1gInSc6
se	s	k7c7
stávajícím	stávající	k2eAgInSc7d1
establishmentem	establishment	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Široce	široko	k6eAd1
používalo	používat	k5eAaImAgNnS
například	například	k6eAd1
heslo	heslo	k1gNnSc1
Nejsme	být	k5eNaImIp1nP
politici	politik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Makáme	makat	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
program	program	k1gInSc1
hnutí	hnutí	k1gNnSc2
byl	být	k5eAaImAgInS
však	však	k9
kritizován	kritizován	k2eAgInSc1d1
pro	pro	k7c4
bezobsažnost	bezobsažnost	k1gFnSc4
a	a	k8xC
nekonkrétnost	nekonkrétnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
hnutí	hnutí	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
krátce	krátce	k6eAd1
před	před	k7c7
parlamentními	parlamentní	k2eAgFnPc7d1
volbami	volba	k1gFnPc7
<g/>
,	,	kIx,
nebyl	být	k5eNaImAgInS
program	program	k1gInSc1
hnutí	hnutí	k1gNnSc2
do	do	k7c2
začátku	začátek	k1gInSc2
volební	volební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
kompletní	kompletní	k2eAgInSc4d1
a	a	k8xC
doplňoval	doplňovat	k5eAaImAgMnS
se	se	k3xPyFc4
a	a	k8xC
procházel	procházet	k5eAaImAgMnS
změnami	změna	k1gFnPc7
během	během	k7c2
předvolební	předvolební	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dle	dle	k7c2
slov	slovo	k1gNnPc2
Andreje	Andrej	k1gMnSc2
Babiše	Babiše	k1gFnSc2
stála	stát	k5eAaImAgFnS
předvolební	předvolební	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
okolo	okolo	k7c2
sedmdesáti	sedmdesát	k4xCc2
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
největší	veliký	k2eAgMnPc4d3
sponzory	sponzor	k1gMnPc4
patřili	patřit	k5eAaImAgMnP
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
sám	sám	k3xTgMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
(	(	kIx(
<g/>
29,5	29,5	k4
mil	míle	k1gFnPc2
Kč	Kč	kA
<g/>
)	)	kIx)
a	a	k8xC
společnosti	společnost	k1gFnSc2
DEZA	DEZA	kA
(	(	kIx(
<g/>
10	#num#	k4
mil	míle	k1gFnPc2
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lovochemie	Lovochemie	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Precheza	Precheza	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
mil	míle	k1gFnPc2
Kč	Kč	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Fatra	Fatra	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
)	)	kIx)
a	a	k8xC
Synthesia	Synthesia	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
všechny	všechen	k3xTgInPc1
z	z	k7c2
holdingu	holding	k1gInSc2
Agrofert	Agrofert	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
úspěchu	úspěch	k1gInSc6
hnutí	hnutí	k1gNnSc2
ve	v	k7c6
volbách	volba	k1gFnPc6
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
podílela	podílet	k5eAaImAgFnS
poradenská	poradenský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Penn	Penn	k1gInSc4
Schoen	Schoen	k2eAgInSc1d1
Berland	Berland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Služeb	služba	k1gFnPc2
této	tento	k3xDgFnSc2
společnosti	společnost	k1gFnSc2
v	v	k7c6
minulosti	minulost	k1gFnSc6
využívala	využívat	k5eAaPmAgFnS,k5eAaImAgFnS
i	i	k9
strana	strana	k1gFnSc1
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
zpracovávala	zpracovávat	k5eAaImAgFnS
kampaně	kampaň	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Těsně	těsně	k6eAd1
před	před	k7c7
termínem	termín	k1gInSc7
voleb	volba	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
médiích	médium	k1gNnPc6
objevilo	objevit	k5eAaPmAgNnS
prohlášení	prohlášení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
dvojka	dvojka	k1gFnSc1
<g/>
“	“	k?
kandidátní	kandidátní	k2eAgFnPc4d1
listiny	listina	k1gFnPc4
v	v	k7c6
kraji	kraj	k1gInSc6
Vysočina	vysočina	k1gFnSc1
Miloslav	Miloslav	k1gMnSc1
Bačiak	Bačiak	k1gMnSc1
zatajil	zatajit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
minulost	minulost	k1gFnSc4
důvěrníka	důvěrník	k1gMnSc2
u	u	k7c2
vojenské	vojenský	k2eAgFnSc2d1
kontrarozvědky	kontrarozvědka	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
„	„	k?
<g/>
politruk	politruk	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Sám	sám	k3xTgMnSc1
Bačiak	Bačiak	k1gMnSc1
slíbil	slíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
zvolení	zvolený	k2eAgMnPc1d1
svůj	svůj	k3xOyFgInSc4
mandát	mandát	k1gInSc4
složí	složit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miloslav	Miloslav	k1gMnSc1
Bačiak	Bačiak	k1gMnSc1
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
se	s	k7c7
ziskem	zisk	k1gInSc7
1	#num#	k4
207	#num#	k4
preferenčních	preferenční	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Svůj	svůj	k3xOyFgInSc4
poslanecký	poslanecký	k2eAgInSc4d1
mandát	mandát	k1gInSc4
složil	složit	k5eAaPmAgMnS
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Babiš	Babiš	k1gMnSc1
později	pozdě	k6eAd2
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Bačiaka	Bačiak	k1gMnSc4
ihned	ihned	k6eAd1
vyloučili	vyloučit	k5eAaPmAgMnP
z	z	k7c2
ANO	ano	k9
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnSc1
do	do	k7c2
voleb	volba	k1gFnPc2
vedli	vést	k5eAaImAgMnP
tři	tři	k4xCgMnPc4
krajští	krajský	k2eAgMnPc1d1
lídři	lídr	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
bývali	bývat	k5eAaImAgMnP
členy	člen	k1gMnPc7
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
jmenovitě	jmenovitě	k6eAd1
předseda	předseda	k1gMnSc1
hnutí	hnutí	k1gNnSc2
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Komárek	Komárek	k1gMnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Vozdecký	Vozdecký	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
volbách	volba	k1gFnPc6
2013	#num#	k4
byl	být	k5eAaImAgInS
vedením	vedení	k1gNnSc7
strany	strana	k1gFnSc2
schválen	schválen	k2eAgInSc1d1
tzv.	tzv.	kA
etický	etický	k2eAgInSc1d1
kodex	kodex	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
poslance	poslanec	k1gMnSc4
zavázal	zavázat	k5eAaPmAgMnS
ke	k	k7c3
čtyřem	čtyři	k4xCgInPc3
bodům	bod	k1gInPc3
<g/>
:	:	kIx,
zaprvé	zaprvé	k4xO
musí	muset	k5eAaImIp3nS
všechny	všechen	k3xTgInPc4
návrhy	návrh	k1gInPc4
zákona	zákon	k1gInSc2
konzultovat	konzultovat	k5eAaImF
se	s	k7c7
svým	svůj	k3xOyFgInSc7
poslaneckým	poslanecký	k2eAgInSc7d1
klubem	klub	k1gInSc7
<g/>
,	,	kIx,
zadruhé	zadruhé	k4xO
během	během	k7c2
výkonu	výkon	k1gInSc2
mandátu	mandát	k1gInSc2
nesmějí	smát	k5eNaImIp3nP
přestoupit	přestoupit	k5eAaPmF
k	k	k7c3
jinému	jiný	k2eAgInSc3d1
poslaneckému	poslanecký	k2eAgInSc3d1
klubu	klub	k1gInSc3
<g/>
,	,	kIx,
zatřetí	zatřetí	k4xO
při	při	k7c6
vážných	vážný	k2eAgNnPc6d1
tématech	téma	k1gNnPc6
nesmějí	smát	k5eNaImIp3nP
hlasovat	hlasovat	k5eAaImF
proti	proti	k7c3
usnesení	usnesení	k1gNnSc3
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
a	a	k8xC
konečně	konečně	k6eAd1
začtvrté	začtvrté	k4xO
<g/>
,	,	kIx,
každý	každý	k3xTgInSc4
měsíc	měsíc	k1gInSc4
musí	muset	k5eAaImIp3nS
věnovat	věnovat	k5eAaImF,k5eAaPmF
sedm	sedm	k4xCc4
tisíc	tisíc	k4xCgInPc2
korun	koruna	k1gFnPc2
z	z	k7c2
platu	plat	k1gInSc2
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
dodržování	dodržování	k1gNnSc4
kodexu	kodex	k1gInSc2
dohlíží	dohlížet	k5eAaImIp3nS
vedení	vedení	k1gNnSc1
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
a	a	k8xC
poslanci	poslanec	k1gMnPc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
tento	tento	k3xDgInSc1
kodex	kodex	k1gInSc1
poruší	porušit	k5eAaPmIp3nS
hrozí	hrozit	k5eAaImIp3nS
pokuta	pokuta	k1gFnSc1
až	až	k9
52	#num#	k4
000	#num#	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
však	však	k9
díky	díky	k7c3
ústavně	ústavně	k6eAd1
garantovanému	garantovaný	k2eAgInSc3d1
volnému	volný	k2eAgInSc3d1
mandátu	mandát	k1gInSc3
poslanců	poslanec	k1gMnPc2
právně	právně	k6eAd1
nevymahatelná	vymahatelný	k2eNgFnSc1d1
sankce	sankce	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
začali	začít	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
jednat	jednat	k5eAaImF
o	o	k7c6
společné	společný	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
s	s	k7c7
ČSSD	ČSSD	kA
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnSc2
ANO	ano	k9
především	především	k6eAd1
usilovalo	usilovat	k5eAaImAgNnS
o	o	k7c4
zisk	zisk	k1gInSc4
ministerstev	ministerstvo	k1gNnPc2
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
vnitra	vnitro	k1gNnSc2
a	a	k8xC
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Také	také	k6eAd1
trvalo	trvat	k5eAaImAgNnS
na	na	k7c4
postu	posta	k1gFnSc4
místopředsedy	místopředseda	k1gMnSc2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
vedení	vedení	k1gNnSc1
pěti	pět	k4xCc2
sněmovních	sněmovní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
a	a	k8xC
obsazení	obsazení	k1gNnSc2
funkcí	funkce	k1gFnPc2
prvních	první	k4xOgMnPc2
náměstků	náměstek	k1gMnPc2
na	na	k7c6
ministerstvech	ministerstvo	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
nepovedou	vést	k5eNaImIp3nP,k5eNaPmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
volbách	volba	k1gFnPc6
Babiš	Babiš	k1gMnSc1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
vést	vést	k5eAaImF
ministerstvo	ministerstvo	k1gNnSc4
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
prezident	prezident	k1gMnSc1
Zeman	Zeman	k1gMnSc1
po	po	k7c6
ministrech	ministr	k1gMnPc6
požadoval	požadovat	k5eAaImAgMnS
lustrační	lustrační	k2eAgNnSc4d1
osvědčení	osvědčení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
Babiš	Babiš	k1gMnSc1
neměl	mít	k5eNaImAgMnS
a	a	k8xC
kvůli	kvůli	k7c3
svému	svůj	k3xOyFgInSc3
slovenskému	slovenský	k2eAgInSc3d1
soudnímu	soudní	k2eAgInSc3d1
sporu	spor	k1gInSc3
ohledně	ohledně	k7c2
své	svůj	k3xOyFgFnSc2
údajné	údajný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
orgány	orgán	k1gInPc7
StB	StB	k1gFnSc2
ho	on	k3xPp3gMnSc4
zatím	zatím	k6eAd1
ani	ani	k8xC
získat	získat	k5eAaPmF
nemohl	moct	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
sám	sám	k3xTgMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
o	o	k7c4
osvědčení	osvědčení	k1gNnSc4
požádat	požádat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc4
proto	proto	k8xC
v	v	k7c6
listopadu	listopad	k1gInSc6
2013	#num#	k4
začal	začít	k5eAaPmAgMnS
preferovat	preferovat	k5eAaImF
vytvoření	vytvoření	k1gNnSc4
funkce	funkce	k1gFnSc2
vicepremiéra	vicepremiér	k1gMnSc2
pro	pro	k7c4
ekonomiku	ekonomika	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
by	by	kYmCp3nS
obsadil	obsadit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
po	po	k7c6
volbách	volba	k1gFnPc6
2013	#num#	k4
</s>
<s>
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
koaliční	koaliční	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
ANO	ano	k9
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
poslanecké	poslanecký	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
měla	mít	k5eAaImAgFnS
koalice	koalice	k1gFnSc1
většinu	většina	k1gFnSc4
111	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládu	vláda	k1gFnSc4
tvořilo	tvořit	k5eAaImAgNnS
sedmnáct	sedmnáct	k4xCc1
ministrů	ministr	k1gMnPc2
<g/>
,	,	kIx,
politické	politický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
ANO	ano	k9
získalo	získat	k5eAaPmAgNnS
šest	šest	k4xCc4
rezortů	rezort	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vicepremiérem	vicepremiér	k1gMnSc7
a	a	k8xC
ministrem	ministr	k1gMnSc7
financí	finance	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
ministrem	ministr	k1gMnSc7
obrany	obrana	k1gFnSc2
Martin	Martin	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
,	,	kIx,
ministryní	ministryně	k1gFnSc7
pro	pro	k7c4
místní	místní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
Věra	Věra	k1gFnSc1
Jourová	Jourová	k1gFnSc1
<g/>
,	,	kIx,
ministrem	ministr	k1gMnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Richard	Richard	k1gMnSc1
Brabec	Brabec	k1gMnSc1
<g/>
,	,	kIx,
ministryní	ministryně	k1gFnSc7
spravedlnosti	spravedlnost	k1gFnSc2
Helena	Helena	k1gFnSc1
Válková	Válková	k1gFnSc1
a	a	k8xC
ministrem	ministr	k1gMnSc7
dopravy	doprava	k1gFnSc2
Antonín	Antonín	k1gMnSc1
Prachař	Prachař	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
oznámení	oznámení	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
kandidátů	kandidát	k1gMnPc2
se	se	k3xPyFc4
z	z	k7c2
řad	řada	k1gFnPc2
ekologů	ekolog	k1gMnPc2
snesla	snést	k5eAaPmAgFnS
kritika	kritika	k1gFnSc1
na	na	k7c4
Richarda	Richard	k1gMnSc4
Brabce	Brabec	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
dříve	dříve	k6eAd2
působil	působit	k5eAaImAgMnS
v	v	k7c6
podniku	podnik	k1gInSc6
Lovochemie	Lovochemie	k1gFnSc2
(	(	kIx(
<g/>
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
vlastní	vlastní	k2eAgMnSc1d1
Agrofert	Agrofert	k1gMnSc1
a	a	k8xC
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
sponzoroval	sponzorovat	k5eAaImAgMnS
kampaň	kampaň	k1gFnSc4
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
prosazoval	prosazovat	k5eAaImAgMnS
zájmy	zájem	k1gInPc4
českého	český	k2eAgInSc2d1
chemického	chemický	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
29	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
devadesát	devadesát	k4xCc4
pět	pět	k4xCc4
dní	den	k1gInPc2
od	od	k7c2
konání	konání	k1gNnSc2
voleb	volba	k1gFnPc2
<g/>
,	,	kIx,
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
prezident	prezident	k1gMnSc1
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
vládu	vláda	k1gFnSc4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
ANO	ano	k9
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Učinil	učinit	k5eAaImAgInS,k5eAaPmAgInS
tak	tak	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
koalice	koalice	k1gFnSc1
zavázala	zavázat	k5eAaPmAgFnS
k	k	k7c3
prosazení	prosazení	k1gNnSc3
služebního	služební	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
umožnil	umožnit	k5eAaPmAgInS
Andreji	Andrej	k1gMnSc3
Babišovi	Babiš	k1gMnSc3
se	se	k3xPyFc4
stát	stát	k5eAaPmF,k5eAaImF
ministrem	ministr	k1gMnSc7
financí	finance	k1gFnPc2
i	i	k8xC
bez	bez	k7c2
dodání	dodání	k1gNnSc2
lustračního	lustrační	k2eAgNnSc2d1
osvědčení	osvědčení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2014	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
hnutí	hnutí	k1gNnSc1
kandidovalo	kandidovat	k5eAaImAgNnS
do	do	k7c2
voleb	volba	k1gFnPc2
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lídrem	lídr	k1gMnSc7
kandidátní	kandidátní	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
byl	být	k5eAaImAgMnS
zvolen	zvolen	k2eAgMnSc1d1
Pavel	Pavel	k1gMnSc1
Telička	telička	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Při	pře	k1gFnSc6
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
rekordně	rekordně	k6eAd1
nízké	nízký	k2eAgFnSc6d1
účasti	účast	k1gFnSc6
18,2	18,2	k4
%	%	kIx~
voličů	volič	k1gMnPc2
hnutí	hnutí	k1gNnSc2
získalo	získat	k5eAaPmAgNnS
244	#num#	k4
501	#num#	k4
hlasů	hlas	k1gInPc2
(	(	kIx(
<g/>
16,13	16,13	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
4	#num#	k4
mandáty	mandát	k1gInPc7
z	z	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
českých	český	k2eAgFnPc2d1
stran	strana	k1gFnPc2
tak	tak	k9
získalo	získat	k5eAaPmAgNnS
nejvíce	nejvíce	k6eAd1,k6eAd3
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Mandáty	mandát	k1gInPc1
obdrželi	obdržet	k5eAaPmAgMnP
Pavel	Pavel	k1gMnSc1
Telička	telička	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
50	#num#	k4
784	#num#	k4
přednostními	přednostní	k2eAgInPc7d1
hlasy	hlas	k1gInPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Petr	Petr	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
Dita	Dita	k1gFnSc1
Charanzová	Charanzová	k1gFnSc1
a	a	k8xC
Martina	Martina	k1gFnSc1
Dlabajová	Dlabajový	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Ihned	ihned	k6eAd1
po	po	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
vyhlášení	vyhlášení	k1gNnSc1
výsledků	výsledek	k1gInPc2
voleb	volba	k1gFnPc2
předseda	předseda	k1gMnSc1
hnutí	hnutí	k1gNnSc2
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
započnou	započnout	k5eAaPmIp3nP
vyjednávání	vyjednávání	k1gNnSc4
s	s	k7c7
evropskou	evropský	k2eAgFnSc7d1
frakcí	frakce	k1gFnSc7
Aliance	aliance	k1gFnSc2
liberálů	liberál	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
(	(	kIx(
<g/>
ALDE	ALDE	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
začátku	začátek	k1gInSc6
června	červen	k1gInSc2
2014	#num#	k4
hnutí	hnutí	k1gNnSc1
podalo	podat	k5eAaPmAgNnS
oficiální	oficiální	k2eAgFnSc4d1
žádost	žádost	k1gFnSc4
o	o	k7c6
přijetí	přijetí	k1gNnSc6
s	s	k7c7
podporou	podpora	k1gFnSc7
bývalého	bývalý	k2eAgMnSc2d1
evropského	evropský	k2eAgMnSc2d1
komisaře	komisař	k1gMnSc2
Pavla	Pavel	k1gMnSc2
Teličky	telička	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
říjnu	říjen	k1gInSc6
2017	#num#	k4
ukončil	ukončit	k5eAaPmAgInS
europoslanec	europoslanec	k1gInSc1
Telička	telička	k1gFnSc1
spolupráci	spolupráce	k1gFnSc3
s	s	k7c7
hnutím	hnutí	k1gNnSc7
ANO	ano	k9
pro	pro	k7c4
názorové	názorový	k2eAgFnPc4d1
neshody	neshoda	k1gFnPc4
s	s	k7c7
předsedou	předseda	k1gMnSc7
hnutí	hnutí	k1gNnSc2
Babišem	Babiš	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sněmy	sněm	k1gInPc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Sněm	sněm	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
</s>
<s>
Ve	v	k7c6
dnech	den	k1gInPc6
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Praze	Praha	k1gFnSc6
konal	konat	k5eAaImAgMnS
III	III	kA
<g/>
.	.	kIx.
sněm	sněm	k1gInSc1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
na	na	k7c6
něm	on	k3xPp3gInSc6
obhájil	obhájit	k5eAaPmAgInS
post	post	k1gInSc1
předsedy	předseda	k1gMnSc2
(	(	kIx(
<g/>
získal	získat	k5eAaPmAgInS
186	#num#	k4
hlasů	hlas	k1gInPc2
=	=	kIx~
100	#num#	k4
%	%	kIx~
z	z	k7c2
platných	platný	k2eAgInPc2d1
odevzdaných	odevzdaný	k2eAgInPc2d1
hlasovacích	hlasovací	k2eAgInPc2d1
lístků	lístek	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
místopředsedou	místopředseda	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Jaroslav	Jaroslav	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
(	(	kIx(
<g/>
získal	získat	k5eAaPmAgInS
153	#num#	k4
hlasů	hlas	k1gInPc2
ze	z	k7c2
188	#num#	k4
možných	možný	k2eAgMnPc2d1
<g/>
,	,	kIx,
tj.	tj.	kA
82	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadovými	řadový	k2eAgMnPc7d1
místopředsedy	místopředseda	k1gMnPc7
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
v	v	k7c6
prvním	první	k4xOgNnSc6
kole	kolo	k1gNnSc6
Petr	Petr	k1gMnSc1
Vokřál	Vokřál	k1gMnSc1
(	(	kIx(
<g/>
125	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
66	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
Jermanová	Jermanová	k1gFnSc1
(	(	kIx(
<g/>
124	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
66	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Radmila	Radmila	k1gFnSc1
Kleslová	Kleslový	k2eAgFnSc1d1
(	(	kIx(
<g/>
109	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
58	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
třetím	třetí	k4xOgNnSc6
kole	kolo	k1gNnSc6
Jan	Jan	k1gMnSc1
Volný	volný	k2eAgMnSc1d1
(	(	kIx(
<g/>
96	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
51	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sněm	sněm	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
</s>
<s>
Ve	v	k7c6
dnech	den	k1gInPc6
25	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
26	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Praze	Praha	k1gFnSc6
konal	konat	k5eAaImAgMnS
IV	IV	kA
<g/>
.	.	kIx.
sněm	sněm	k1gInSc1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
na	na	k7c6
něm	on	k3xPp3gInSc6
obhájil	obhájit	k5eAaPmAgInS
post	post	k1gInSc1
předsedy	předseda	k1gMnSc2
(	(	kIx(
<g/>
získal	získat	k5eAaPmAgInS
195	#num#	k4
hlasů	hlas	k1gInPc2
=	=	kIx~
95	#num#	k4
%	%	kIx~
z	z	k7c2
platných	platný	k2eAgInPc2d1
odevzdaných	odevzdaný	k2eAgInPc2d1
hlasovacích	hlasovací	k2eAgInPc2d1
lístků	lístek	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
pak	pak	k6eAd1
pozici	pozice	k1gFnSc4
1	#num#	k4
<g/>
.	.	kIx.
místopředsedy	místopředseda	k1gMnSc2
(	(	kIx(
<g/>
získal	získat	k5eAaPmAgInS
142	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
69	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadovými	řadový	k2eAgMnPc7d1
místopředsedy	místopředseda	k1gMnPc7
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
Martin	Martin	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
(	(	kIx(
<g/>
174	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Vokřál	Vokřál	k1gMnSc1
(	(	kIx(
<g/>
173	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Brabec	Brabec	k1gMnSc1
(	(	kIx(
<g/>
171	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
Jaroslava	Jaroslava	k1gFnSc1
Pokorná	Pokorná	k1gFnSc1
Jermanová	Jermanová	k1gFnSc1
(	(	kIx(
<g/>
169	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
2017	#num#	k4
</s>
<s>
Ve	v	k7c6
dnech	den	k1gInPc6
20	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2017	#num#	k4
proběhly	proběhnout	k5eAaPmAgFnP
volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc4
vyhrálo	vyhrát	k5eAaPmAgNnS
hnutí	hnutí	k1gNnSc1
ANO	ano	k9
2011	#num#	k4
s	s	k7c7
29,64	29,64	k4
%	%	kIx~
platných	platný	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
volebního	volební	k2eAgInSc2d1
systému	systém	k1gInSc2
získalo	získat	k5eAaPmAgNnS
78	#num#	k4
poslaneckých	poslanecký	k2eAgInPc2d1
mandátů	mandát	k1gInPc2
z	z	k7c2
200	#num#	k4
(	(	kIx(
<g/>
39	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
zpravodajského	zpravodajský	k2eAgInSc2d1
portálu	portál	k1gInSc2
Hlídací	hlídací	k2eAgMnSc1d1
pes	pes	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
výsledky	výsledek	k1gInPc1
byly	být	k5eAaImAgInP
uveřejněny	uveřejnit	k5eAaPmNgInP
i	i	k9
v	v	k7c6
německém	německý	k2eAgInSc6d1
tisku	tisk	k1gInSc6
<g/>
,	,	kIx,
přeběhla	přeběhnout	k5eAaPmAgFnS
při	při	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
říjnu	říjen	k1gInSc6
2017	#num#	k4
mj.	mj.	kA
jedna	jeden	k4xCgFnSc1
třetina	třetina	k1gFnSc1
dřívějších	dřívější	k2eAgMnPc2d1
přívrženců	přívrženec	k1gMnPc2
ČSSD	ČSSD	kA
a	a	k8xC
jedna	jeden	k4xCgFnSc1
čtvrtina	čtvrtina	k1gFnSc1
přívrženců	přívrženec	k1gMnPc2
KSČM	KSČM	kA
k	k	k7c3
Babišovu	Babišův	k2eAgNnSc3d1
hnutí	hnutí	k1gNnSc3
ANO	ano	k9
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Program	program	k1gInSc1
a	a	k8xC
veřejná	veřejný	k2eAgFnSc1d1
prezentace	prezentace	k1gFnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Programem	program	k1gInSc7
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
před	před	k7c7
volbami	volba	k1gFnPc7
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
2017	#num#	k4
byl	být	k5eAaImAgInS
růst	růst	k1gInSc1
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
omezení	omezení	k1gNnSc1
předražených	předražený	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
zakázek	zakázka	k1gFnPc2
<g/>
,	,	kIx,
rychlá	rychlý	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
dálnic	dálnice	k1gFnPc2
<g/>
,	,	kIx,
efektivní	efektivní	k2eAgInPc4d1
a	a	k8xC
transparentně	transparentně	k6eAd1
fungující	fungující	k2eAgNnSc4d1
zdravotnictví	zdravotnictví	k1gNnSc4
a	a	k8xC
funkční	funkční	k2eAgFnPc4d1
justice	justice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdůrazněny	zdůrazněn	k2eAgFnPc4d1
byly	být	k5eAaImAgInP
čtyři	čtyři	k4xCgInPc1
body	bod	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
<g/>
:	:	kIx,
navýšení	navýšení	k1gNnSc1
peněz	peníze	k1gInPc2
pro	pro	k7c4
zpravodajské	zpravodajský	k2eAgFnPc4d1
služby	služba	k1gFnPc4
a	a	k8xC
na	na	k7c4
armádu	armáda	k1gFnSc4
na	na	k7c4
2	#num#	k4
procenta	procento	k1gNnSc2
HDP	HDP	kA
s	s	k7c7
cílem	cíl	k1gInSc7
mít	mít	k5eAaImF
30	#num#	k4
až	až	k9
35	#num#	k4
tisíc	tisíc	k4xCgInSc4
vojáků	voják	k1gMnPc2
vybavených	vybavený	k2eAgFnPc2d1
špičkovou	špičkový	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
;	;	kIx,
řešení	řešení	k1gNnSc4
migrace	migrace	k1gFnSc2
mimo	mimo	k7c4
evropský	evropský	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
<g/>
;	;	kIx,
zajištění	zajištění	k1gNnSc4
potravinové	potravinový	k2eAgFnSc2d1
soběstačnosti	soběstačnost	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
energetické	energetický	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
výstavba	výstavba	k1gFnSc1
dalších	další	k2eAgInPc2d1
jaderných	jaderný	k2eAgInPc2d1
bloků	blok	k1gInPc2
a	a	k8xC
udržení	udržení	k1gNnSc4
kontroly	kontrola	k1gFnSc2
státu	stát	k1gInSc2
nad	nad	k7c7
zásobami	zásoba	k1gFnPc7
lithia	lithium	k1gNnSc2
<g/>
;	;	kIx,
nepřijímat	přijímat	k5eNaImF
euro	euro	k1gNnSc4
</s>
<s>
Efektivní	efektivní	k2eAgInSc1d1
a	a	k8xC
hospodárný	hospodárný	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
:	:	kIx,
snižovat	snižovat	k5eAaImF
dluh	dluh	k1gInSc4
a	a	k8xC
hospodařit	hospodařit	k5eAaImF
s	s	k7c7
přebytkem	přebytek	k1gInSc7
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
investičních	investiční	k2eAgFnPc6d1
možnostech	možnost	k1gFnPc6
<g/>
;	;	kIx,
nízké	nízký	k2eAgFnPc1d1
daně	daň	k1gFnPc1
a	a	k8xC
vyšší	vysoký	k2eAgInPc1d2
platy	plat	k1gInPc1
<g/>
;	;	kIx,
zákonem	zákon	k1gInSc7
o	o	k7c6
whistleblowingu	whistleblowing	k1gInSc6
ochránit	ochránit	k5eAaPmF
oznamovatele	oznamovatel	k1gInPc4
korupce	korupce	k1gFnSc2
<g/>
;	;	kIx,
digitalizace	digitalizace	k1gFnSc1
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
a	a	k8xC
centralizace	centralizace	k1gFnSc2
nákupů	nákup	k1gInPc2
<g/>
;	;	kIx,
zlepšení	zlepšení	k1gNnSc1
zákonu	zákon	k1gInSc3
o	o	k7c6
státní	státní	k2eAgFnSc6d1
službě	služba	k1gFnSc6
</s>
<s>
Investice	investice	k1gFnPc1
do	do	k7c2
naší	náš	k3xOp1gFnSc2
země	zem	k1gFnSc2
<g/>
:	:	kIx,
rychlovlaky	rychlovlak	k1gInPc4
<g/>
,	,	kIx,
nové	nový	k2eAgInPc4d1
kilometry	kilometr	k1gInPc4
silnic	silnice	k1gFnPc2
<g/>
,	,	kIx,
výstavba	výstavba	k1gFnSc1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
školek	školka	k1gFnPc2
<g/>
,	,	kIx,
jeslí	jesle	k1gFnPc2
<g/>
,	,	kIx,
domovů	domov	k1gInPc2
důchodců	důchodce	k1gMnPc2
<g/>
,	,	kIx,
sportovišť	sportoviště	k1gNnPc2
<g/>
,	,	kIx,
renovace	renovace	k1gFnSc1
vlakových	vlakový	k2eAgNnPc2d1
nádraží	nádraží	k1gNnPc2
<g/>
,	,	kIx,
obnova	obnova	k1gFnSc1
památek	památka	k1gFnPc2
<g/>
;	;	kIx,
do	do	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
postavit	postavit	k5eAaPmF
nové	nový	k2eAgInPc4d1
obchvaty	obchvat	k1gInPc4
a	a	k8xC
170	#num#	k4
kilometrů	kilometr	k1gInPc2
nových	nový	k2eAgFnPc2d1
dálnic	dálnice	k1gFnPc2
jako	jako	k9
D	D	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
D	D	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
D11	D11	k1gFnSc1
a	a	k8xC
D	D	kA
<g/>
35	#num#	k4
<g/>
;	;	kIx,
opravy	oprava	k1gFnPc4
hradů	hrad	k1gInPc2
<g/>
,	,	kIx,
zámků	zámek	k1gInPc2
a	a	k8xC
investice	investice	k1gFnPc4
do	do	k7c2
zařízení	zařízení	k1gNnSc2
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
<g/>
;	;	kIx,
výstavba	výstavba	k1gFnSc1
galerie	galerie	k1gFnSc2
pro	pro	k7c4
Muchovu	Muchův	k2eAgFnSc4d1
Slovanskou	slovanský	k2eAgFnSc4d1
epopej	epopej	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Investice	investice	k1gFnPc1
do	do	k7c2
našich	náš	k3xOp1gMnPc2
lidí	člověk	k1gMnPc2
<g/>
:	:	kIx,
učitelům	učitel	k1gMnPc3
během	během	k7c2
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
zvýšit	zvýšit	k5eAaPmF
mzdy	mzda	k1gFnPc4
o	o	k7c4
50	#num#	k4
procent	procento	k1gNnPc2
<g/>
;	;	kIx,
nezavádět	zavádět	k5eNaImF
školné	školné	k1gNnSc4
na	na	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
;	;	kIx,
zvyšování	zvyšování	k1gNnSc3
důchodů	důchod	k1gInPc2
<g/>
;	;	kIx,
neprivatizovat	privatizovat	k5eNaImF
zdravotnictví	zdravotnictví	k1gNnSc4
nebo	nebo	k8xC
potají	potají	k6eAd1
zvyšovat	zvyšovat	k5eAaImF
spoluúčast	spoluúčast	k1gFnSc4
pacientů	pacient	k1gMnPc2
<g/>
;	;	kIx,
zrychlení	zrychlení	k1gNnSc1
nekonečných	konečný	k2eNgNnPc2d1
soudních	soudní	k2eAgNnPc2d1
stání	stání	k1gNnPc2
a	a	k8xC
vymahatelnosti	vymahatelnost	k1gFnSc2
práva	právo	k1gNnSc2
<g/>
;	;	kIx,
dokončíme	dokončit	k5eAaPmIp1nP
ratifikační	ratifikační	k2eAgInSc4d1
proces	proces	k1gInSc4
Úmluvy	úmluva	k1gFnSc2
o	o	k7c6
právech	právo	k1gNnPc6
osob	osoba	k1gFnPc2
se	s	k7c7
zdravotním	zdravotní	k2eAgNnSc7d1
postižením	postižení	k1gNnSc7
podpisem	podpis	k1gInSc7
Opčního	opční	k2eAgInSc2d1
protokolu	protokol	k1gInSc2
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
po	po	k7c6
volbách	volba	k1gFnPc6
2017	#num#	k4
</s>
<s>
Prezident	prezident	k1gMnSc1
Miloš	Miloš	k1gMnSc1
Zeman	Zeman	k1gMnSc1
pověřil	pověřit	k5eAaPmAgMnS
lídra	lídr	k1gMnSc4
ANO	ano	k9
2011	#num#	k4
Andreje	Andrej	k1gMnSc2
Babiše	Babiš	k1gInSc2
dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2017	#num#	k4
jednáním	jednání	k1gNnSc7
o	o	k7c6
sestavení	sestavení	k1gNnSc6
nové	nový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
jej	on	k3xPp3gInSc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
premiérem	premiér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
vyjádření	vyjádření	k1gNnSc2
obou	dva	k4xCgMnPc2
aktérů	aktér	k1gMnPc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
vláda	vláda	k1gFnSc1
jmenována	jmenován	k2eAgFnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
seznámení	seznámení	k1gNnSc6
prezidenta	prezident	k1gMnSc2
s	s	k7c7
ministerskými	ministerský	k2eAgMnPc7d1
adepty	adept	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Babiše	Babiš	k1gInSc2
prezident	prezident	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
posunem	posun	k1gInSc7
termínu	termín	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
už	už	k9
jako	jako	k9
předseda	předseda	k1gMnSc1
jmenované	jmenovaný	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
mohl	moct	k5eAaImAgInS
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zúčastnit	zúčastnit	k5eAaPmF
summitu	summit	k1gInSc3
prezidentů	prezident	k1gMnPc2
a	a	k8xC
premiérů	premiér	k1gMnPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
v	v	k7c6
Bruselu	Brusel	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeman	Zeman	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
rovněž	rovněž	k9
osobně	osobně	k6eAd1
podpořit	podpořit	k5eAaPmF
získání	získání	k1gNnSc4
sněmovní	sněmovní	k2eAgFnSc2d1
důvěry	důvěra	k1gFnSc2
pro	pro	k7c4
Babišovu	Babišův	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
ustavení	ustavení	k1gNnSc6
své	svůj	k3xOyFgFnSc2
první	první	k4xOgFnSc2
vlády	vláda	k1gFnSc2
v	v	k7c6
prosinci	prosinec	k1gInSc6
2017	#num#	k4
odmítl	odmítnout	k5eAaPmAgMnS
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
vládní	vládní	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
SPD	SPD	kA
a	a	k8xC
KSČM	KSČM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
politologa	politolog	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
Pehe	Peh	k1gMnSc2
Babiš	Babiš	k1gMnSc1
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
ho	on	k3xPp3gInSc4
přímá	přímý	k2eAgFnSc1d1
účast	účast	k1gFnSc1
těchto	tento	k3xDgFnPc2
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
vládě	vláda	k1gFnSc6
mohla	moct	k5eAaImAgFnS
poškodit	poškodit	k5eAaPmF
v	v	k7c6
očích	oko	k1gNnPc6
těch	ten	k3xDgFnPc2
nadnárodních	nadnárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
nichž	jenž	k3xRgInPc6
závisí	záviset	k5eAaImIp3nS
ekonomika	ekonomika	k1gFnSc1
i	i	k8xC
bezpečnost	bezpečnost	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
EU	EU	kA
a	a	k8xC
NATO	NATO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
svém	svůj	k3xOyFgInSc6
neúspěchu	neúspěch	k1gInSc6
ve	v	k7c6
volbách	volba	k1gFnPc6
se	se	k3xPyFc4
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
(	(	kIx(
<g/>
ČSSD	ČSSD	kA
<g/>
)	)	kIx)
začala	začít	k5eAaPmAgFnS
zabývat	zabývat	k5eAaImF
i	i	k9
otázkou	otázka	k1gFnSc7
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
postavit	postavit	k5eAaPmF
k	k	k7c3
případné	případný	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
spolupráci	spolupráce	k1gFnSc3
s	s	k7c7
ANO	ano	k9
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
Babišovu	Babišův	k2eAgFnSc4d1
menšinovou	menšinový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
nechtěla	chtít	k5eNaImAgFnS
sociální	sociální	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
podpořit	podpořit	k5eAaPmF
<g/>
,	,	kIx,
dočasný	dočasný	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
Milan	Milan	k1gMnSc1
Chovanec	Chovanec	k1gMnSc1
však	však	k9
nevyloučil	vyloučit	k5eNaPmAgMnS
jednání	jednání	k1gNnSc4
s	s	k7c7
ANO	ano	k9
2011	#num#	k4
při	při	k7c6
druhém	druhý	k4xOgInSc6
pokusu	pokus	k1gInSc6
Andreje	Andrej	k1gMnSc2
Babiše	Babiš	k1gMnSc2
o	o	k7c4
sestavení	sestavení	k1gNnSc4
kabinetu	kabinet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSSD	ČSSD	kA
se	se	k3xPyFc4
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
„	„	k?
<g/>
mohla	moct	k5eAaImAgFnS
s	s	k7c7
ANO	ano	k9
o	o	k7c6
budoucnosti	budoucnost	k1gFnSc6
země	zem	k1gFnSc2
bavit	bavit	k5eAaImF
<g/>
,	,	kIx,
pokud	pokud	k8xS
jednání	jednání	k1gNnSc1
nebude	být	k5eNaImBp3nS
mít	mít	k5eAaImF
formu	forma	k1gFnSc4
diktátu	diktát	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gMnSc1
sám	sám	k3xTgMnSc1
zmínil	zmínit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
vliv	vliv	k1gInSc4
vedle	vedle	k7c2
tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
blížícího	blížící	k2eAgInSc2d1
sjezdu	sjezd	k1gInSc2
ČSSD	ČSSD	kA
i	i	k9
kongres	kongres	k1gInSc4
ODS	ODS	kA
v	v	k7c6
lednu	leden	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
sjezdu	sjezd	k1gInSc6
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
tato	tento	k3xDgFnSc1
strana	strana	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
svého	svůj	k3xOyFgMnSc2
nového	nový	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
Jana	Jan	k1gMnSc2
Hamáčka	Hamáček	k1gMnSc2
vyjednávat	vyjednávat	k5eAaImF
s	s	k7c7
ANO	ano	k9
2011	#num#	k4
o	o	k7c6
účasti	účast	k1gFnSc6
na	na	k7c6
vládě	vláda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
ČSSD	ČSSD	kA
pro	pro	k7c4
sebe	sebe	k3xPyFc4
požadovala	požadovat	k5eAaImAgNnP
pět	pět	k4xCc4
ministerských	ministerský	k2eAgNnPc2d1
křesel	křeslo	k1gNnPc2
včetně	včetně	k7c2
ministerstev	ministerstvo	k1gNnPc2
vnitra	vnitro	k1gNnSc2
a	a	k8xC
spravedlnosti	spravedlnost	k1gFnSc2
(	(	kIx(
<g/>
resorty	resort	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
váhu	váha	k1gFnSc4
ve	v	k7c6
spojitosti	spojitost	k1gFnSc6
s	s	k7c7
kauzou	kauza	k1gFnSc7
Čapí	čapí	k2eAgNnSc4d1
hnízdo	hnízdo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
ANO	ano	k9
2011	#num#	k4
tehdy	tehdy	k6eAd1
ochotno	ochoten	k2eAgNnSc1d1
přiznat	přiznat	k5eAaPmF
ČSSD	ČSSD	kA
jen	jen	k6eAd1
čtyři	čtyři	k4xCgNnPc1
ministerstva	ministerstvo	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
dubna	duben	k1gInSc2
2018	#num#	k4
skončilo	skončit	k5eAaPmAgNnS
první	první	k4xOgNnSc1
vyjednávání	vyjednávání	k1gNnSc1
mezi	mezi	k7c7
ČSSD	ČSSD	kA
a	a	k8xC
ANO	ano	k9
2011	#num#	k4
o	o	k7c6
společné	společný	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
neúspěchem	neúspěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
důvodů	důvod	k1gInPc2
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
neochota	neochota	k1gFnSc1
ANO	ano	k9
2011	#num#	k4
přenechat	přenechat	k5eAaPmF
sociálním	sociální	k2eAgMnPc3d1
demokratům	demokrat	k1gMnPc3
post	post	k1gInSc4
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Představitelé	představitel	k1gMnPc1
ČSSD	ČSSD	kA
Jan	Jan	k1gMnSc1
Hamáček	Hamáček	k1gMnSc1
a	a	k8xC
Jiří	Jiří	k1gMnSc1
Zimola	Zimola	k1gFnSc1
navštívili	navštívit	k5eAaPmAgMnP
4	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
prezidenta	prezident	k1gMnSc2
republiky	republika	k1gFnSc2
Miloše	Miloš	k1gMnSc2
Zemana	Zeman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
informací	informace	k1gFnPc2
Lidových	lidový	k2eAgFnPc2d1
novin	novina	k1gFnPc2
jim	on	k3xPp3gMnPc3
prezident	prezident	k1gMnSc1
doporučil	doporučit	k5eAaPmAgMnS
trvat	trvat	k5eAaImF
na	na	k7c6
svých	svůj	k3xOyFgInPc6
požadavcích	požadavek	k1gInPc6
a	a	k8xC
neustupovat	ustupovat	k5eNaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiér	premiéra	k1gFnPc2
v	v	k7c6
demisi	demise	k1gFnSc6
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
byl	být	k5eAaImAgMnS
podle	podle	k7c2
svých	svůj	k3xOyFgNnPc2
slov	slovo	k1gNnPc2
připraven	připravit	k5eAaPmNgMnS
podřídit	podřídit	k5eAaPmF
se	se	k3xPyFc4
Zemanově	Zemanův	k2eAgFnSc3d1
vůli	vůle	k1gFnSc3
<g/>
.	.	kIx.
„	„	k?
<g/>
Když	když	k8xS
prezident	prezident	k1gMnSc1
řekne	říct	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pověří	pověřit	k5eAaPmIp3nS
vyjednáváním	vyjednávání	k1gNnSc7
o	o	k7c6
vládě	vláda	k1gFnSc6
někoho	někdo	k3yInSc4
jiného	jiný	k2eAgNnSc2d1
z	z	k7c2
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
to	ten	k3xDgNnSc1
budu	být	k5eAaImBp1nS
akceptovat	akceptovat	k5eAaBmF
<g/>
,	,	kIx,
<g/>
“	“	k?
řekl	říct	k5eAaPmAgMnS
Babiš	Babiš	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezident	prezident	k1gMnSc1
Zeman	Zeman	k1gMnSc1
dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
podruhé	podruhé	k6eAd1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Andreje	Andrej	k1gMnSc4
Babiše	Babiš	k1gMnSc4
premiérem	premiér	k1gMnSc7
a	a	k8xC
vyzval	vyzvat	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mu	on	k3xPp3gMnSc3
předložil	předložit	k5eAaPmAgInS
seznam	seznam	k1gInSc1
příštích	příští	k2eAgInPc2d1
členů	člen	k1gInPc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yRgInPc7,k3yQgInPc7,k3yIgInPc7
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
seznámit	seznámit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
je	být	k5eAaImIp3nS
Babiš	Babiš	k1gInSc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
jej	on	k3xPp3gMnSc4
prezident	prezident	k1gMnSc1
Zeman	Zeman	k1gMnSc1
podruhé	podruhé	k6eAd1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
premiérem	premiér	k1gMnSc7
<g/>
,	,	kIx,
předsedou	předseda	k1gMnSc7
své	svůj	k3xOyFgFnSc2
druhé	druhý	k4xOgFnSc2
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
menšinovou	menšinový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
sestavil	sestavit	k5eAaPmAgInS
jak	jak	k6eAd1
ze	z	k7c2
zástupců	zástupce	k1gMnPc2
ANO	ano	k9
2011	#num#	k4
<g/>
,	,	kIx,
tak	tak	k6eAd1
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc4
a	a	k8xC
ministři	ministr	k1gMnPc1
složili	složit	k5eAaPmAgMnP
do	do	k7c2
rukou	ruka	k1gFnPc2
prezidenta	prezident	k1gMnSc2
předepsaný	předepsaný	k2eAgInSc4d1
slib	slib	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Zeman	Zeman	k1gMnSc1
nejmenoval	jmenovat	k5eNaImAgMnS
ministrem	ministr	k1gMnSc7
zahraničí	zahraničí	k1gNnSc2
zástupce	zástupce	k1gMnSc1
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
a	a	k8xC
poslance	poslanec	k1gMnSc2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
Miroslava	Miroslav	k1gMnSc2
Pocheho	Poche	k1gMnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
toto	tento	k3xDgNnSc1
ministerstvo	ministerstvo	k1gNnSc1
vede	vést	k5eAaImIp3nS
zatím	zatím	k6eAd1
z	z	k7c2
pověření	pověření	k1gNnSc2
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
a	a	k8xC
ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
Jan	Jan	k1gMnSc1
Hamáček	Hamáček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSSD	ČSSD	kA
obdržela	obdržet	k5eAaPmAgFnS
ve	v	k7c6
vládě	vláda	k1gFnSc6
celkově	celkově	k6eAd1
pět	pět	k4xCc1
křesel	křeslo	k1gNnPc2
včetně	včetně	k7c2
jí	on	k3xPp3gFnSc2
zprvu	zprvu	k6eAd1
odepíraného	odepíraný	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
ANO	ano	k9
2011	#num#	k4
dostala	dostat	k5eAaPmAgFnS
deset	deset	k4xCc4
křesel	křeslo	k1gNnPc2
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
podepsalo	podepsat	k5eAaPmAgNnS
hnutí	hnutí	k1gNnSc1
ANO	ano	k9
2011	#num#	k4
s	s	k7c7
ČSSD	ČSSD	kA
koaliční	koaliční	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
krátce	krátce	k6eAd1
po	po	k7c6
půlnoci	půlnoc	k1gFnSc6
získala	získat	k5eAaPmAgFnS
Babišova	Babišův	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
důvěru	důvěra	k1gFnSc4
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
poměrem	poměr	k1gInSc7
hlasů	hlas	k1gInPc2
105	#num#	k4
(	(	kIx(
<g/>
pro	pro	k7c4
<g/>
)	)	kIx)
ku	k	k7c3
91	#num#	k4
(	(	kIx(
<g/>
proti	proti	k7c3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
tomu	ten	k3xDgMnSc3
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
již	již	k6eAd1
před	před	k7c7
sestavením	sestavení	k1gNnSc7
vlády	vláda	k1gFnSc2
měla	mít	k5eAaImAgFnS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
ji	on	k3xPp3gFnSc4
podpořit	podpořit	k5eAaPmF
při	při	k7c6
hlasování	hlasování	k1gNnSc6
o	o	k7c6
důvěře	důvěra	k1gFnSc6
a	a	k8xC
posléze	posléze	k6eAd1
ji	on	k3xPp3gFnSc4
tolerovat	tolerovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Kauza	kauza	k1gFnSc1
Stoka	stoka	k1gFnSc1
</s>
<s>
V	v	k7c6
brněnském	brněnský	k2eAgInSc6d1
případu	případ	k1gInSc6
korupce	korupce	k1gFnSc2
a	a	k8xC
manipulace	manipulace	k1gFnSc2
s	s	k7c7
veřejnými	veřejný	k2eAgFnPc7d1
zakázkami	zakázka	k1gFnPc7
<g/>
,	,	kIx,
medializovaném	medializovaný	k2eAgInSc6d1
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
hlavním	hlavní	k2eAgMnSc7d1
aktérem	aktér	k1gMnSc7
stal	stát	k5eAaPmAgMnS
brněnský	brněnský	k2eAgMnSc1d1
radní	radní	k1gMnSc1
za	za	k7c2
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
Jiří	Jiří	k1gMnSc1
Švachula	Švachul	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
ním	on	k3xPp3gNnSc7
bylo	být	k5eAaImAgNnS
obviněno	obvinit	k5eAaPmNgNnS
dalších	další	k2eAgMnPc2d1
10	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
dvě	dva	k4xCgFnPc1
firmy	firma	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policisté	policista	k1gMnPc1
vyjádřili	vyjádřit	k5eAaPmAgMnP
podezření	podezření	k1gNnSc4
z	z	k7c2
korupce	korupce	k1gFnSc2
také	také	k9
u	u	k7c2
Jiřího	Jiří	k1gMnSc2
Faltýnka	Faltýnka	k1gFnSc1
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
prvního	první	k4xOgMnSc2
místopředsedy	místopředseda	k1gMnSc2
hnutí	hnutí	k1gNnSc2
Jaroslava	Jaroslav	k1gMnSc2
Faltýnka	Faltýnka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
zmiňoval	zmiňovat	k5eAaImAgMnS
obviněný	obviněný	k1gMnSc1
Švachula	Švachul	k1gMnSc2
v	v	k7c6
policejních	policejní	k2eAgInPc6d1
odposleších	odposlech	k1gInPc6
jako	jako	k8xS,k8xC
příjemce	příjemka	k1gFnSc3
úplatků	úplatek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Jiří	Jiří	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
pozastavil	pozastavit	k5eAaPmAgInS
členství	členství	k1gNnSc4
v	v	k7c6
ANO	ano	k9
a	a	k8xC
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
místo	místo	k1gNnSc4
poradce	poradce	k1gMnSc2
poslance	poslanec	k1gMnSc2
Rostislava	Rostislav	k1gMnSc2
Vyzuly	vyzout	k5eAaPmAgFnP
(	(	kIx(
<g/>
ANO	ano	k9
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
opustil	opustit	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
brněnský	brněnský	k2eAgMnSc1d1
primátor	primátor	k1gMnSc1
Petr	Petr	k1gMnSc1
Vokřál	Vokřál	k1gMnSc1
i	i	k8xC
jeho	jeho	k3xOp3gMnSc1
někdejší	někdejší	k2eAgMnSc1d1
náměstek	náměstek	k1gMnSc1
pro	pro	k7c4
investice	investice	k1gFnPc4
Richard	Richard	k1gMnSc1
Mrázek	Mrázek	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
blízkým	blízký	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
na	na	k7c4
Švachulu	Švachula	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hnutí	hnutí	k1gNnSc1
ANO	ano	k9
po	po	k7c6
medializaci	medializace	k1gFnSc6
kauzy	kauza	k1gFnSc2
rozpustilo	rozpustit	k5eAaPmAgNnS
brněnskou	brněnský	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Policejní	policejní	k2eAgInSc4d1
vyšetřování	vyšetřování	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
červnu	červen	k1gInSc6
2020	#num#	k4
rozšířilo	rozšířit	k5eAaPmAgNnS
na	na	k7c4
dalších	další	k2eAgFnPc2d1
26	#num#	k4
osob	osoba	k1gFnPc2
a	a	k8xC
jednu	jeden	k4xCgFnSc4
firmu	firma	k1gFnSc4
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
se	se	k3xPyFc4
prokázaly	prokázat	k5eAaPmAgInP
úplatky	úplatek	k1gInPc1
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
100	#num#	k4
tisíc	tisíc	k4xCgInPc2
korun	koruna	k1gFnPc2
až	až	k6eAd1
dva	dva	k4xCgInPc4
miliony	milion	k4xCgInPc4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
žalobce	žalobce	k1gMnSc2
šlo	jít	k5eAaImAgNnS
o	o	k7c4
desítky	desítka	k1gFnPc4
zakázek	zakázka	k1gFnPc2
v	v	k7c6
celkovém	celkový	k2eAgInSc6d1
objemu	objem	k1gInSc6
nejméně	málo	k6eAd3
za	za	k7c4
330	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Hospodářských	hospodářský	k2eAgFnPc2d1
novin	novina	k1gFnPc2
uplácelo	uplácet	k5eAaImAgNnS
v	v	k7c6
Brně	Brno	k1gNnSc6
90	#num#	k4
%	%	kIx~
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
kauze	kauza	k1gFnSc6
Stoka	stoka	k1gFnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
obviněných	obviněný	k1gMnPc2
také	také	k9
bývalý	bývalý	k2eAgMnSc1d1
místostarosta	místostarosta	k1gMnSc1
části	část	k1gFnSc2
Brno-sever	Brno-severo	k1gNnPc2
za	za	k7c4
Občanskou	občanský	k2eAgFnSc4d1
demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Jiří	Jiří	k1gMnSc1
Hos	Hos	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
pohřešován	pohřešován	k2eAgMnSc1d1
a	a	k8xC
pátrá	pátrat	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gInSc6
policie	policie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odchody	odchod	k1gInPc1
členů	člen	k1gMnPc2
</s>
<s>
2016	#num#	k4
Antonín	Antonín	k1gMnSc1
Prachař	Prachař	k1gMnSc1
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
činnosti	činnost	k1gFnSc3
prvního	první	k4xOgMnSc2
místopředsedy	místopředseda	k1gMnSc2
hnutí	hnutí	k1gNnSc2
Jaroslava	Jaroslav	k1gMnSc2
Faltýnka	Faltýnka	k1gFnSc1
</s>
<s>
2017	#num#	k4
Pavel	Pavel	k1gMnSc1
Telička	telička	k1gFnSc1
<g/>
,	,	kIx,
místopředseda	místopředseda	k1gMnSc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
někdejší	někdejší	k2eAgMnSc1d1
lídr	lídr	k1gMnSc1
voleb	volba	k1gFnPc2
do	do	k7c2
EP	EP	kA
<g/>
,	,	kIx,
zahraničněpolitický	zahraničněpolitický	k2eAgMnSc1d1
mluvčí	mluvčí	k1gMnSc1
ANO	ano	k9
<g/>
,	,	kIx,
pro	pro	k7c4
názorové	názorový	k2eAgFnPc4d1
neshody	neshoda	k1gFnPc4
s	s	k7c7
Andrejem	Andrej	k1gMnSc7
Babišem	Babiš	k1gMnSc7
</s>
<s>
2018	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Robert	Robert	k1gMnSc1
Pelikán	Pelikán	k1gMnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
někdejší	někdejší	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
názorový	názorový	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
s	s	k7c7
hnutím	hnutí	k1gNnSc7
</s>
<s>
Petr	Petr	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
EP	EP	kA
za	za	k7c7
ANO	ano	k9
<g/>
,	,	kIx,
na	na	k7c4
protest	protest	k1gInSc4
proti	proti	k7c3
podpoře	podpora	k1gFnSc3
ANO	ano	k9
pro	pro	k7c4
Miloše	Miloš	k1gMnSc4
Zemana	Zeman	k1gMnSc4
v	v	k7c6
prezidentské	prezidentský	k2eAgFnSc6d1
volbě	volba	k1gFnSc6
</s>
<s>
Martin	Martin	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
někdejší	někdejší	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
odchod	odchod	k1gInSc4
z	z	k7c2
politiky	politika	k1gFnSc2
</s>
<s>
2020	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Petr	Petr	k1gMnSc1
Vokřál	Vokřál	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
primátor	primátor	k1gMnSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
a	a	k8xC
připravovaný	připravovaný	k2eAgMnSc1d1
lídr	lídr	k1gMnSc1
kandidátky	kandidátka	k1gFnSc2
ANO	ano	k9
pro	pro	k7c4
krajské	krajský	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
neochotě	neochota	k1gFnSc3
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
řešit	řešit	k5eAaImF
korupční	korupční	k2eAgFnSc4d1
kauzu	kauza	k1gFnSc4
Stoka	stoka	k1gFnSc1
a	a	k8xC
pro	pro	k7c4
nesouhlas	nesouhlas	k1gInSc4
se	s	k7c7
směřováním	směřování	k1gNnSc7
hnutí	hnutí	k1gNnSc2
</s>
<s>
Adriana	Adriana	k1gFnSc1
Krnáčová	Krnáčová	k1gFnSc1
<g/>
,	,	kIx,
bývalá	bývalý	k2eAgFnSc1d1
primátorka	primátorka	k1gFnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
nesouhlas	nesouhlas	k1gInSc4
s	s	k7c7
vedením	vedení	k1gNnSc7
hnutí	hnutí	k1gNnSc2
Andrejem	Andrej	k1gMnSc7
Babišem	Babiš	k1gMnSc7
</s>
<s>
spoluzakladatelka	spoluzakladatelka	k1gFnSc1
hnutí	hnutí	k1gNnSc2
Radka	Radka	k1gFnSc1
Maxová	Maxová	k1gFnSc1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
odklonu	odklon	k1gInSc2
hnutí	hnutí	k1gNnSc2
od	od	k7c2
původních	původní	k2eAgFnPc2d1
idejí	idea	k1gFnPc2
a	a	k8xC
prosazování	prosazování	k1gNnSc6
marketingových	marketingový	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
hnutí	hnutí	k1gNnSc2
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volební	volební	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
hnutí	hnutí	k1gNnSc2
a	a	k8xC
volební	volební	k2eAgFnSc2d1
preference	preference	k1gFnSc2
</s>
<s>
Výsledky	výsledek	k1gInPc1
ANO	ano	k9
2011	#num#	k4
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
</s>
<s>
Výsledky	výsledek	k1gInPc1
ANO	ano	k9
2011	#num#	k4
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Hlasy	hlas	k1gInPc1
v	v	k7c6
%	%	kIx~
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
mandátů	mandát	k1gInPc2
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
927	#num#	k4
240	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
18,65	18,65	k4
</s>
<s>
–	–	k?
</s>
<s>
47	#num#	k4
</s>
<s>
–	–	k?
</s>
<s>
2017	#num#	k4
</s>
<s>
1	#num#	k4
500	#num#	k4
113	#num#	k4
</s>
<s>
▲	▲	k?
572	#num#	k4
873	#num#	k4
</s>
<s>
29,64	29,64	k4
</s>
<s>
▲	▲	k?
10,98	10,98	k4
</s>
<s>
78	#num#	k4
</s>
<s>
▲	▲	k?
31	#num#	k4
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc1
hlasůZměnaHlasy	hlasůZměnaHlasa	k1gFnSc2
v	v	k7c6
%	%	kIx~
<g/>
ZměnaPočet	ZměnaPočet	k1gInSc1
mandátůZměna	mandátůZměn	k2eAgFnSc1d1
</s>
<s>
2014	#num#	k4
</s>
<s>
244	#num#	k4
501	#num#	k4
<g/>
–	–	k?
<g/>
16,13	16,13	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
–	–	k?
</s>
<s>
2019	#num#	k4
</s>
<s>
502	#num#	k4
343	#num#	k4
<g/>
▲	▲	k?
257	#num#	k4
842	#num#	k4
21,18	21,18	k4
<g/>
▲	▲	k?
5,05	5,05	k4
6	#num#	k4
<g/>
▲	▲	k?
2	#num#	k4
</s>
<s>
Zastupitelstva	zastupitelstvo	k1gNnPc1
obcí	obec	k1gFnPc2
</s>
<s>
Celkové	celkový	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
</s>
<s>
Volby	volba	k1gFnPc1
</s>
<s>
Počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Hlasy	hlas	k1gInPc1
v	v	k7c6
%	%	kIx~
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
mandátů	mandát	k1gInPc2
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Hlasy	hlas	k1gInPc1
v	v	k7c6
%	%	kIx~
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
14	#num#	k4
458	#num#	k4
998	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
14,59	14,59	k4
</s>
<s>
-	-	kIx~
</s>
<s>
1	#num#	k4
610	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
2,58	2,58	k4
</s>
<s>
-	-	kIx~
</s>
<s>
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
16	#num#	k4
732	#num#	k4
285	#num#	k4
</s>
<s>
▲	▲	k?
2	#num#	k4
273	#num#	k4
287	#num#	k4
</s>
<s>
14,88	14,88	k4
</s>
<s>
▲	▲	k?
0,29	0,29	k4
</s>
<s>
1	#num#	k4
676	#num#	k4
</s>
<s>
▲	▲	k?
66	#num#	k4
</s>
<s>
Vedení	vedení	k1gNnSc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
předseda	předseda	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
místopředsedové	místopředseda	k1gMnPc1
Jaroslav	Jaroslav	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
<g/>
,	,	kIx,
Radka	Radka	k1gFnSc1
Maxová	Maxová	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
předseda	předseda	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
místopředsedové	místopředseda	k1gMnPc1
Věra	Věra	k1gFnSc1
Jourová	Jourová	k1gFnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Valinčičová	Valinčičová	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Hammer	Hammer	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Harvánek	Harvánek	k1gMnSc1
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
Greplová	Greplová	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
předseda	předseda	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
místopředsedové	místopředseda	k1gMnPc1
Jaroslav	Jaroslav	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Vokřál	Vokřál	k1gMnSc1
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
Jermanová	Jermanová	k1gFnSc1
<g/>
,	,	kIx,
Radmila	Radmila	k1gFnSc1
Kleslová	Kleslový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Volný	volný	k2eAgMnSc1d1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
předseda	předseda	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
místopředsedové	místopředseda	k1gMnPc1
Jaroslav	Jaroslav	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Stropnický	stropnický	k2eAgMnSc1d1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Vokřál	Vokřál	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Brabec	Brabec	k1gMnSc1
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
Jermanová	Jermanová	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
sněm	sněm	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
předseda	předseda	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
místopředsedové	místopředseda	k1gMnPc1
Jaroslav	Jaroslav	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Brabec	Brabec	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Vokřál	Vokřál	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
Vondráček	Vondráček	k1gMnSc1
<g/>
,	,	kIx,
Jaroslava	Jaroslava	k1gFnSc1
Pokorná	Pokorná	k1gFnSc1
Jermanová	Jermanová	k1gFnSc1
</s>
<s>
Činitelé	činitel	k1gMnPc1
hnutí	hnutí	k1gNnSc2
</s>
<s>
Předsednictvo	předsednictvo	k1gNnSc1
</s>
<s>
Na	na	k7c6
V.	V.	kA
sněmu	sněm	k1gInSc2
ANO	ano	k9
2011	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
zvoleno	zvolit	k5eAaPmNgNnS
předsednictvo	předsednictvo	k1gNnSc1
hnutí	hnutí	k1gNnSc2
v	v	k7c6
následujícím	následující	k2eAgNnSc6d1
složení	složení	k1gNnSc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
předseda	předseda	k1gMnSc1
–	–	k?
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
PČR	PČR	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
–	–	k?
Jaroslav	Jaroslav	k1gMnSc1
Faltýnek	Faltýnka	k1gFnPc2
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
rezignoval	rezignovat	k5eAaBmAgMnS
dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
</s>
<s>
místopředseda	místopředseda	k1gMnSc1
–	–	k?
Richard	Richard	k1gMnSc1
Brabec	Brabec	k1gMnSc1
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
PČR	PČR	kA
</s>
<s>
místopředseda	místopředseda	k1gMnSc1
–	–	k?
Petr	Petr	k1gMnSc1
Vokřál	Vokřál	k1gMnSc1
<g/>
,	,	kIx,
zastupitel	zastupitel	k1gMnSc1
města	město	k1gNnSc2
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
rezignoval	rezignovat	k5eAaBmAgMnS
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
</s>
<s>
místopředseda	místopředseda	k1gMnSc1
–	–	k?
Radek	Radek	k1gMnSc1
Vondráček	Vondráček	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
PČR	PČR	kA
</s>
<s>
místopředsedkyně	místopředsedkyně	k1gFnSc1
–	–	k?
Jaroslava	Jaroslava	k1gFnSc1
Pokorná	Pokorná	k1gFnSc1
Jermanová	Jermanová	k1gFnSc1
<g/>
,	,	kIx,
hejtmanka	hejtmanka	k1gFnSc1
Středočeského	středočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
zastupitelka	zastupitelka	k1gFnSc1
města	město	k1gNnSc2
Benešov	Benešov	k1gInSc1
</s>
<s>
členka	členka	k1gFnSc1
předsednictva	předsednictvo	k1gNnSc2
–	–	k?
Jana	Jana	k1gFnSc1
Mračková	Mračková	k1gFnSc1
Vildumetzová	Vildumetzová	k1gFnSc1
<g/>
,	,	kIx,
poslankyně	poslankyně	k1gFnSc1
PČR	PČR	kA
<g/>
,	,	kIx,
radní	radní	k1gMnPc1
Karlovarského	karlovarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
člen	člen	k1gMnSc1
předsednictva	předsednictvo	k1gNnSc2
–	–	k?
Tomáš	Tomáš	k1gMnSc1
Macura	Macura	k1gMnSc1
<g/>
,	,	kIx,
primátor	primátor	k1gMnSc1
města	město	k1gNnSc2
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
člen	člen	k1gMnSc1
předsednictva	předsednictvo	k1gNnSc2
–	–	k?
Roman	Roman	k1gMnSc1
Kubíček	Kubíček	k1gMnSc1
<g/>
,	,	kIx,
místopředseda	místopředseda	k1gMnSc1
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
radní	radní	k1gMnSc1
města	město	k1gNnSc2
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
člen	člen	k1gMnSc1
předsednictva	předsednictvo	k1gNnSc2
–	–	k?
Jan	Jan	k1gMnSc1
Říčař	Říčař	k1gMnSc1
<g/>
,	,	kIx,
zastupitel	zastupitel	k1gMnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha	Praha	k1gFnSc1
11	#num#	k4
</s>
<s>
člen	člen	k1gMnSc1
předsednictva	předsednictvo	k1gNnSc2
–	–	k?
Jan	Jan	k1gMnSc1
Volný	volný	k2eAgMnSc1d1
<g/>
,	,	kIx,
místopředseda	místopředseda	k1gMnSc1
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
zastupitel	zastupitel	k1gMnSc1
Plzeňského	plzeňský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
členka	členka	k1gFnSc1
předsednictva	předsednictvo	k1gNnSc2
–	–	k?
Jana	Jana	k1gFnSc1
Pastuchová	Pastuchová	k1gFnSc1
<g/>
,	,	kIx,
místopředsedkyně	místopředsedkyně	k1gFnSc1
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
zastupitelka	zastupitelka	k1gFnSc1
města	město	k1gNnSc2
Jablonec	Jablonec	k1gInSc1
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
</s>
<s>
člen	člen	k1gMnSc1
předsednictva	předsednictvo	k1gNnSc2
–	–	k?
Ladislav	Ladislav	k1gMnSc1
Okleštěk	okleštěk	k1gInSc1
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
PČR	PČR	kA
<g/>
,	,	kIx,
zastupitel	zastupitel	k1gMnSc1
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
člen	člen	k1gMnSc1
předsednictva	předsednictvo	k1gNnSc2
–	–	k?
Martin	Martin	k1gMnSc1
Kukla	Kukla	k1gMnSc1
<g/>
,	,	kIx,
náměstek	náměstek	k1gMnSc1
hejtmana	hejtman	k1gMnSc2
Kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Od	od	k7c2
října	říjen	k1gInSc2
2020	#num#	k4
hnutí	hnutí	k1gNnSc2
v	v	k7c6
europarlamentu	europarlament	k1gInSc6
reálně	reálně	k6eAd1
disponuje	disponovat	k5eAaBmIp3nS
5	#num#	k4
mandáty	mandát	k1gInPc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
z	z	k7c2
něj	on	k3xPp3gInSc2
vystoupila	vystoupit	k5eAaPmAgFnS
europoslankyně	europoslankyně	k1gFnSc1
Radka	Radka	k1gFnSc1
Maxová	Maxová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Štěpán	Štěpán	k1gMnSc1
Drahokoupil	Drahokoupil	k1gMnSc1
<g/>
:	:	kIx,
Volby	volba	k1gFnPc1
2013	#num#	k4
<g/>
:	:	kIx,
dekonsolidace	dekonsolidace	k1gFnSc2
stranického	stranický	k2eAgInSc2d1
systému	systém	k1gInSc2
a	a	k8xC
vzestup	vzestup	k1gInSc4
anti-politiky	anti-politika	k1gFnSc2
<g/>
,	,	kIx,
cz	cz	k?
<g/>
.	.	kIx.
<g/>
boell	boell	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Zahraniční	zahraniční	k2eAgNnPc1d1
média	médium	k1gNnPc1
<g/>
:	:	kIx,
Vítězství	vítězství	k1gNnSc1
populismu	populismus	k1gInSc2
a	a	k8xC
Sobotkova	Sobotkův	k2eAgFnSc1d1
těžká	těžký	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
<g/>
,	,	kIx,
zpravy	zprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
e	e	k0
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
KLIMEŠ	Klimeš	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimochodem	mimochodem	k6eAd1
Davida	David	k1gMnSc2
Klimeše	Klimeš	k1gMnSc2
<g/>
:	:	kIx,
Vítejte	vítat	k5eAaImRp2nP
v	v	k7c6
korporativistické	korporativistický	k2eAgFnSc6d1
druhé	druhý	k4xOgFnSc6
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iHned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
19.7	19.7	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PEŠEK	Pešek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petr	Petr	k1gMnSc1
Pešek	Pešek	k1gMnSc1
<g/>
:	:	kIx,
Hnutí	hnutí	k1gNnSc1
ANO	ano	k9
v	v	k7c6
poločase	poločas	k1gInSc6
rozpadu	rozpad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaké	jaký	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
dědictví	dědictví	k1gNnSc1
po	po	k7c6
sobě	sebe	k3xPyFc6
zanechá	zanechat	k5eAaPmIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forum	forum	k1gNnSc1
24	#num#	k4
<g/>
,	,	kIx,
3.11	3.11	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PEHE	PEHE	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
a	a	k8xC
pro	pro	k7c4
nikoho	nikdo	k3yNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
<g/>
,	,	kIx,
17.1	17.1	k4
<g/>
.2018	.2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
www.echo24.cz	www.echo24.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
archiv	archiv	k1gInSc1
<g/>
.	.	kIx.
<g/>
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
JUST	just	k6eAd1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
CHARVÁT	Charvát	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Business-Firm	Business-Firm	k1gInSc1
Parties	Parties	k1gInSc4
and	and	k?
the	the	k?
Czech	Czech	k1gInSc4
Party	parta	k1gFnSc2
System	Systo	k1gNnSc7
after	aftra	k1gFnPc2
2010	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Academia	academia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
83	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.mfcr.cz/cs/verejny-sektor/podpora-z-narodnich-zdroju/politicke-strany-a-hnuti/prispevky-ze-statniho-rozpoctu-uhrazene-37083	https://www.mfcr.cz/cs/verejny-sektor/podpora-z-narodnich-zdroju/politicke-strany-a-hnuti/prispevky-ze-statniho-rozpoctu-uhrazene-37083	k4
<g/>
↑	↑	k?
Minuta	minuta	k1gFnSc1
po	po	k7c6
minutě	minuta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-17	2019-02-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.volby.cz/pls/kz2016/kz111?xjazyk=CZ&	http://www.volby.cz/pls/kz2016/kz111?xjazyk=CZ&	k?
2	#num#	k4
Výsledky	výsledek	k1gInPc1
voleb	volba	k1gFnPc2
|	|	kIx~
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
volby	volba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stijn	Stijn	k1gInSc1
van	vana	k1gFnPc2
Kessel	Kessela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populist	Populist	k1gMnSc1
Parties	Parties	k1gMnSc1
in	in	k?
Europe	Europ	k1gInSc5
<g/>
:	:	kIx,
Agents	Agents	k1gInSc1
of	of	k?
Discontent	Discontent	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Palgrave	Palgrav	k1gInSc5
Macmillan	Macmillan	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
137	#num#	k4
<g/>
-	-	kIx~
<g/>
41411	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
41	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
E.	E.	kA
Gene	gen	k1gInSc5
Frankland	Franklando	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Green	Green	k2eAgInSc1d1
Parties	Parties	k1gInSc1
in	in	k?
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Emilie	Emilie	k1gFnSc2
van	vana	k1gFnPc2
Haute	Haut	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Taylor	Taylor	k1gMnSc1
&	&	k?
Francis	Francis	k1gInSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
317	#num#	k4
<g/>
-	-	kIx~
<g/>
12453	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Central	Central	k1gFnSc2
and	and	k?
Eastern	Eastern	k1gMnSc1
European	European	k1gMnSc1
Green	Green	k2eAgInSc4d1
Parties	Parties	k1gInSc4
<g/>
:	:	kIx,
Rise	Rise	k1gInSc1
<g/>
,	,	kIx,
fall	fall	k1gInSc1
and	and	k?
revival	revival	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
s.	s.	k?
104	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ondřej	Ondřej	k1gMnSc1
Císař	Císař	k1gMnSc1
<g/>
;	;	kIx,
VÁCLAV	Václav	k1gMnSc1
ŠTĚTKA	Štětka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populist	Populist	k1gMnSc1
Political	Political	k1gMnSc1
Communication	Communication	k1gInSc1
in	in	k?
Europe	Europ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Toril	Torila	k1gFnPc2
Aalberg	Aalberg	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
317	#num#	k4
<g/>
-	-	kIx~
<g/>
22474	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Czech	Czecha	k1gFnPc2
Republic	Republice	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Rise	Ris	k1gFnSc2
of	of	k?
Populism	Populism	k1gMnSc1
From	From	k1gMnSc1
the	the	k?
Fringes	Fringes	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
Mainstream	Mainstream	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
287	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Seznam	seznam	k1gInSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnPc2
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
2011	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.anobudelip.cz	www.anobudelip.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://domaci.eurozpravy.cz/politika/104227-jsme-radikalni-a-nekompromisni-rika-ano/	http://domaci.eurozpravy.cz/politika/104227-jsme-radikalni-a-nekompromisni-rika-ano/	k4
<g/>
↑	↑	k?
http://echo24.cz/a/w6Ux7/ano-je-pravicova-strana-se-socialnim-citenim-rekl-babis	http://echo24.cz/a/w6Ux7/ano-je-pravicova-strana-se-socialnim-citenim-rekl-babis	k1gInSc1
<g/>
↑	↑	k?
http://www.parlamentnilisty.cz/arena/monitor/Andrej-Babis-vesti-budoucnost-Budou-tu-dve-strany-ANO-a-CSSD-339352	http://www.parlamentnilisty.cz/arena/monitor/Andrej-Babis-vesti-budoucnost-Budou-tu-dve-strany-ANO-a-CSSD-339352	k4
<g/>
↑	↑	k?
Hnutí	hnutí	k1gNnSc1
ANO	ano	k9
je	být	k5eAaImIp3nS
oficiálně	oficiálně	k6eAd1
v	v	k7c6
rodině	rodina	k1gFnSc6
ALDE	ALDE	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Chceme	chtít	k5eAaImIp1nP
malý	malý	k2eAgInSc4d1
a	a	k8xC
levný	levný	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nikoho	nikdo	k3yNnSc4
neotravuje	otravovat	k5eNaImIp3nS
<g/>
,	,	kIx,
vyhlásil	vyhlásit	k5eAaPmAgMnS
Babiš	Babiš	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-17	2019-02-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BUREŠ	Bureš	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Private	Privat	k1gInSc5
Security	Securit	k1gInPc4
Companies	Companies	k1gInSc1
<g/>
:	:	kIx,
Transforming	Transforming	k1gInSc1
Politics	Politics	k1gInSc1
and	and	k?
Security	Securita	k1gFnSc2
in	in	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Palgrave	Palgrav	k1gInSc5
Macmillan	Macmillan	k1gMnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
137	#num#	k4
<g/>
-	-	kIx~
<g/>
47752	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
98	#num#	k4
<g/>
–	–	k?
<g/>
99	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KOPEČEK	Kopeček	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
;	;	kIx,
SVAČINOVÁ	Svačinová	k1gFnSc1
<g/>
,	,	kIx,
Petra	Petra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
rozhoduje	rozhodovat	k5eAaImIp3nS
v	v	k7c6
českých	český	k2eAgFnPc6d1
politických	politický	k2eAgFnPc6d1
stranách	strana	k1gFnPc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
Vzestup	vzestup	k1gInSc1
nových	nový	k2eAgMnPc2d1
politických	politický	k2eAgMnPc2d1
podnikatelů	podnikatel	k1gMnPc2
ve	v	k7c6
srovnávací	srovnávací	k2eAgFnSc6d1
perspektivě	perspektiva	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středoevropské	středoevropský	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
studie	studie	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Šéf	šéf	k1gMnSc1
ANO	ano	k9
2011	#num#	k4
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
:	:	kIx,
Stát	stát	k1gInSc1
bych	by	kYmCp1nS
řídil	řídit	k5eAaImAgInS
stejně	stejně	k6eAd1
jako	jako	k9
Agrofert	Agrofert	k1gMnSc1
<g/>
,	,	kIx,
denik	denik	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
15.4	15.4	k4
<g/>
.20131	.20131	k4
2	#num#	k4
http://www.anobudelip.cz/	http://www.anobudelip.cz/	k?
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
hnutí	hnutí	k1gNnSc3
ANO	ano	k9
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Celkové	celkový	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
hlasování	hlasování	k1gNnSc2
-	-	kIx~
volby	volba	k1gFnSc2
2013	#num#	k4
<g/>
,	,	kIx,
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
20131	#num#	k4
2	#num#	k4
Jmenné	jmenný	k2eAgInPc1d1
seznamy	seznam	k1gInPc1
-	-	kIx~
volby	volba	k1gFnPc1
2013	#num#	k4
-	-	kIx~
ANO	ano	k9
<g/>
,	,	kIx,
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
↑	↑	k?
Babiš	Babiš	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
připraven	připravit	k5eAaPmNgMnS
jednat	jednat	k5eAaImF
o	o	k7c6
vládě	vláda	k1gFnSc6
<g/>
,	,	kIx,
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
ANO	ano	k9
2011	#num#	k4
<g/>
:	:	kIx,
Seznam	seznam	k1gInSc1
dárců	dárce	k1gMnPc2
<g/>
.	.	kIx.
www.anobudelip.cz	www.anobudelip.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://zpravy.ihned.cz/politika/c1-60153470-babis-si-na-volby-najal-poradce-z-usa-chce-protlacit-ano-do-snemovny-jako-lidr	http://zpravy.ihned.cz/politika/c1-60153470-babis-si-na-volby-najal-poradce-z-usa-chce-protlacit-ano-do-snemovny-jako-lidr	k1gInSc1
(	(	kIx(
<g/>
on-line	on-lin	k1gInSc5
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
Náhlý	náhlý	k2eAgInSc4d1
konec	konec	k1gInSc4
dvojky	dvojka	k1gFnSc2
Babišova	Babišův	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
na	na	k7c6
Vysočině	vysočina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatajil	zatajit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
minulost	minulost	k1gFnSc4
politruka	politruk	k1gMnSc2
<g/>
,	,	kIx,
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Marcel	Marcel	k1gMnSc1
Moržor	Moržor	k1gMnSc1
<g/>
:	:	kIx,
Bačiak	Bačiak	k1gMnSc1
z	z	k7c2
čela	čelo	k1gNnSc2
kandidátky	kandidátka	k1gFnSc2
ANO	ano	k9
na	na	k7c6
Vysočině	vysočina	k1gFnSc6
složil	složit	k5eAaPmAgMnS
poslanecký	poslanecký	k2eAgInSc4d1
mandát	mandát	k1gInSc4
<g/>
,	,	kIx,
denik	denik	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Lucie	Lucie	k1gFnSc1
Kudláčková	Kudláčková	k1gFnSc1
<g/>
:	:	kIx,
Reportáž	reportáž	k1gFnSc1
od	od	k7c2
Babiše	Babiše	k1gFnSc2
<g/>
:	:	kIx,
Zrození	zrození	k1gNnSc1
vítěze	vítěz	k1gMnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
nechce	chtít	k5eNaImIp3nS
vládnout	vládnout	k5eAaImF
<g/>
,	,	kIx,
http://aktualne.centrum.cz	http://aktualne.centrum.cz	k1gMnSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
http://zpravy.idnes.cz/byvali-komunisti-kandiduji-do-snemovny-fcx-/domaci.aspx?c=A131006_191422_domaci_zt	http://zpravy.idnes.cz/byvali-komunisti-kandiduji-do-snemovny-fcx-/domaci.aspx?c=A131006_191422_domaci_zt	k1gInSc1
<g/>
↑	↑	k?
Etický	etický	k2eAgInSc1d1
kodex	kodex	k1gInSc1
ANO	ano	k9
2013	#num#	k4
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
4	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Wirnitzer	Wirnitzer	k1gMnSc1
<g/>
:	:	kIx,
Kodex	kodex	k1gInSc1
ANO	ano	k9
<g/>
:	:	kIx,
7	#num#	k4
000	#num#	k4
měsíčně	měsíčně	k6eAd1
na	na	k7c4
klub	klub	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
odbojné	odbojný	k2eAgNnSc4d1
hlasování	hlasování	k1gNnSc4
pokuta	pokuta	k1gFnSc1
52	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
ANO	ano	k9
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gMnPc1
začnou	začít	k5eAaPmIp3nP
řešit	řešit	k5eAaImF
společnou	společný	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
měsíci	měsíc	k1gInSc6
od	od	k7c2
voleb	volba	k1gFnPc2
<g/>
,	,	kIx,
iHned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Eliška	Eliška	k1gFnSc1
Nová	nový	k2eAgFnSc1d1
<g/>
:	:	kIx,
ANO	ano	k9
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
ČSSD	ČSSD	kA
získá	získat	k5eAaPmIp3nS
post	post	k1gInSc4
šéfa	šéf	k1gMnSc2
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
ANO	ano	k9
chce	chtít	k5eAaImIp3nS
na	na	k7c4
místopředsedkyni	místopředsedkyně	k1gFnSc4
Sněmovny	sněmovna	k1gFnSc2
Jermanovou	Jermanová	k1gFnSc7
<g/>
,	,	kIx,
ceskenoviny	ceskenovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Veronika	Veronika	k1gFnSc1
Zemanová	Zemanová	k1gFnSc1
<g/>
:	:	kIx,
Babiš	Babiš	k1gInSc1
navrhuje	navrhovat	k5eAaImIp3nS
na	na	k7c4
vnitro	vnitro	k1gNnSc4
Davida	David	k1gMnSc2
Ondráčku	Ondráček	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
chce	chtít	k5eAaImIp3nS
být	být	k5eAaImF
vicepremiérem	vicepremiér	k1gMnSc7
pro	pro	k7c4
ekonomiku	ekonomika	k1gFnSc4
<g/>
,	,	kIx,
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
ČSSD	ČSSD	kA
i	i	k8xC
ANO	ano	k9
chtějí	chtít	k5eAaImIp3nP
náměstky	náměstka	k1gFnPc4
na	na	k7c6
ministerstvech	ministerstvo	k1gNnPc6
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
nepovedou	povést	k5eNaPmIp3nP,k5eNaImIp3nP
<g/>
,	,	kIx,
ceskenoviny	ceskenovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
11.201	11.201	k4
<g/>
3	#num#	k4
<g/>
↑	↑	k?
Babiš	Babiš	k1gMnSc1
byl	být	k5eAaImAgMnS
u	u	k7c2
Zemana	Zeman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šéf	šéf	k1gMnSc1
ANO	ano	k9
zjišťoval	zjišťovat	k5eAaImAgMnS
<g/>
,	,	kIx,
zda	zda	k8xS
může	moct	k5eAaImIp3nS
do	do	k7c2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
lidovky	lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
Zástupci	zástupce	k1gMnPc1
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
ANO	ano	k9
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gMnPc1
podepsali	podepsat	k5eAaPmAgMnP
koaliční	koaliční	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
ceskenoviny	ceskenovina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
6	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
Aktuálně	aktuálně	k6eAd1
<g/>
:	:	kIx,
Sobotkův	Sobotkův	k2eAgInSc1d1
kabinet	kabinet	k1gInSc1
je	být	k5eAaImIp3nS
kompletní	kompletní	k2eAgMnSc1d1
-	-	kIx~
17	#num#	k4
členů	člen	k1gInPc2
nové	nový	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
aktualne	aktualnout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
Ministr	ministr	k1gMnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Brabec	Brabec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exředitel	exředitel	k1gMnSc1
Lovochemie	Lovochemie	k1gFnSc2
má	mít	k5eAaImIp3nS
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
prosazoval	prosazovat	k5eAaImAgMnS
<g/>
,	,	kIx,
zpravy	zprava	k1gFnPc1
<g/>
.	.	kIx.
<g/>
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
má	mít	k5eAaImIp3nS
novou	nový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
čele	čelo	k1gNnSc6
koaličního	koaliční	k2eAgInSc2d1
kabinetu	kabinet	k1gInSc2
stojí	stát	k5eAaImIp3nS
Sobotka	Sobotka	k1gFnSc1
<g/>
,	,	kIx,
ceskenoviny	ceskenovina	k1gFnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
29.01	29.01	k4
<g/>
.2014	.2014	k4
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
:	:	kIx,
Sněmovna	sněmovna	k1gFnSc1
podpořila	podpořit	k5eAaPmAgFnS
služební	služební	k2eAgFnSc4d1
novelu	novela	k1gFnSc4
<g/>
,	,	kIx,
Zemanovu	Zemanův	k2eAgFnSc4d1
podmínku	podmínka	k1gFnSc4
pro	pro	k7c4
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
ceskenoviny	ceskenovina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
22.01	22.01	k4
<g/>
.2014	.2014	k4
<g/>
↑	↑	k?
Eurovolby	Eurovolba	k1gFnSc2
-	-	kIx~
kandidáti	kandidát	k1gMnPc1
-	-	kIx~
2014	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
7	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
anobudelip	anobudelipit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
Celkové	celkový	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
hlasování	hlasování	k1gNnSc2
ČR	ČR	kA
2014	#num#	k4
<g/>
,	,	kIx,
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
Přednostní	přednostní	k2eAgInPc1d1
hlasy	hlas	k1gInPc1
pro	pro	k7c4
kandidáty	kandidát	k1gMnPc4
ČR	ČR	kA
<g />
.	.	kIx.
</s>
<s hack="1">
2014	#num#	k4
<g/>
,	,	kIx,
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
Jakub	Jakub	k1gMnSc1
Šídlo	Šídlo	k1gMnSc1
<g/>
:	:	kIx,
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
<g/>
:	:	kIx,
Vítězství	vítězství	k1gNnSc1
ANO	ano	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
europarlamentu	europarlament	k1gInSc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
nás	my	k3xPp1nPc4
překvapením	překvapení	k1gNnSc7
Archivováno	archivován	k2eAgNnSc4d1
28	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
<g/>
↑	↑	k?
ANO	ano	k9
požádalo	požádat	k5eAaPmAgNnS
o	o	k7c4
přijetí	přijetí	k1gNnSc4
k	k	k7c3
evropským	evropský	k2eAgMnPc3d1
liberálům	liberál	k1gMnPc3
<g/>
,	,	kIx,
rozhodnutí	rozhodnutí	k1gNnSc1
čeká	čekat	k5eAaImIp3nS
brzy	brzy	k6eAd1
<g/>
,	,	kIx,
,	,	kIx,
ceskenoviny	ceskenovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Europoslanec	Europoslanec	k1gInSc1
Telička	telička	k1gFnSc1
opouští	opouštět	k5eAaImIp3nS
hnutí	hnutí	k1gNnPc4
ANO	ano	k9
<g/>
,	,	kIx,
s	s	k7c7
Babišem	Babiš	k1gInSc7
se	se	k3xPyFc4
názorově	názorově	k6eAd1
rozešel	rozejít	k5eAaPmAgInS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-11	2017-10-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volební	volební	k2eAgInSc1d1
sněm	sněm	k1gInSc1
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
<g/>
,	,	kIx,
On-line	On-lin	k1gInSc5
reportáž	reportáž	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-02-28	2015-02-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Živě	Živa	k1gFnSc6
<g/>
:	:	kIx,
ANO	ano	k9
si	se	k3xPyFc3
zvolilo	zvolit	k5eAaPmAgNnS
nové	nový	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Musíme	muset	k5eAaImIp1nP
bojovat	bojovat	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
všichni	všechen	k3xTgMnPc1
u	u	k7c2
nás	my	k3xPp1nPc2
měli	mít	k5eAaImAgMnP
líp	dobře	k6eAd2
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
Babiš	Babiš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-02-26	2017-02-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Babis	Babis	k1gFnSc1
nahm	nahm	k6eAd1
linkem	linkem	k6eAd1
Lager	Lager	k1gMnSc1
Wähler	Wähler	k1gMnSc1
ab	ab	k?
<g/>
,	,	kIx,
Handelsblatt	Handelsblatt	k1gMnSc1
<g/>
,	,	kIx,
Düsseldorf	Düsseldorf	k1gMnSc1
<g/>
,	,	kIx,
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2017	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.anobudelip.cz	www.anobudelip.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeman	Zeman	k1gMnSc1
pověřil	pověřit	k5eAaPmAgMnS
Babiše	Babiš	k1gMnSc4
jednáním	jednání	k1gNnSc7
o	o	k7c4
sestavení	sestavení	k1gNnSc4
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
jmenování	jmenování	k1gNnSc4
premiérem	premiér	k1gMnSc7
nebude	být	k5eNaImBp3nS
chtít	chtít	k5eAaImF
podpisy	podpis	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-31	2017-10-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeman	Zeman	k1gMnSc1
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
Babiše	Babiš	k1gMnSc4
premiérem	premiér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
vládu	vláda	k1gFnSc4
osobně	osobně	k6eAd1
podpoří	podpořit	k5eAaPmIp3nP
ve	v	k7c6
sněmovně	sněmovna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2017-12-06	2017-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NEPRAŠOVÁ	NEPRAŠOVÁ	kA
<g/>
,	,	kIx,
Veronika	Veronika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeman	Zeman	k1gMnSc1
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Babiše	Babiš	k1gMnSc4
premiérem	premiér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
si	se	k3xPyFc3
zaslouží	zasloužit	k5eAaPmIp3nS
návrat	návrat	k1gInSc4
na	na	k7c4
špičku	špička	k1gFnSc4
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
šéf	šéf	k1gMnSc1
ANO	ano	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-06	2017-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PEHE	PEHE	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
KOMENTÁŘ	komentář	k1gInSc1
<g/>
:	:	kIx,
Blíží	blížit	k5eAaImIp3nS
se	se	k3xPyFc4
konec	konec	k1gInSc1
první	první	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
Jiří	Jiří	k1gMnSc1
Pehe	Peh	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-22	2017-12-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSSD	ČSSD	kA
začne	začít	k5eAaPmIp3nS
přemýšlet	přemýšlet	k5eAaImF
<g/>
,	,	kIx,
zda	zda	k8xS
při	při	k7c6
druhém	druhý	k4xOgInSc6
pokusu	pokus	k1gInSc6
podpoří	podpořit	k5eAaPmIp3nS
Babišovu	Babišův	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FROUZOVÁ	FROUZOVÁ	kA
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
ČSSD	ČSSD	kA
vyjednávají	vyjednávat	k5eAaImIp3nP
s	s	k7c7
ANO	ano	k9
zemanovci	zemanovec	k1gMnSc3
i	i	k8xC
sobotkovci	sobotkovec	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
veřejnost	veřejnost	k1gFnSc4
vyplouvají	vyplouvat	k5eAaImIp3nP
první	první	k4xOgNnPc1
možná	možný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
ministrů	ministr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SURMANOVÁ	SURMANOVÁ	kA
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
;	;	kIx,
KOUTNÍK	Koutník	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc1
<g/>
:	:	kIx,
Když	když	k8xS
Zeman	Zeman	k1gMnSc1
pověří	pověřit	k5eAaPmIp3nS
sestavením	sestavení	k1gNnSc7
vlády	vláda	k1gFnSc2
někoho	někdo	k3yInSc4
jiného	jiný	k2eAgMnSc4d1
z	z	k7c2
ANO	ano	k9
<g/>
,	,	kIx,
budu	být	k5eAaImBp1nS
to	ten	k3xDgNnSc1
akceptovat	akceptovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2018-04-09	2018-04-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PŘINOSILOVÁ	PŘINOSILOVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
;	;	kIx,
BURDA	Burda	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc1
je	být	k5eAaImIp3nS
podruhé	podruhé	k6eAd1
premiérem	premiér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Načasování	načasování	k1gNnSc1
jmenování	jmenování	k1gNnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nátlakem	nátlak	k1gInSc7
na	na	k7c6
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
myslí	myslet	k5eAaImIp3nS
si	se	k3xPyFc3
politolog	politolog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciál	speciál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČRo	ČRo	k1gFnSc1
Plus	plus	k1gNnSc2
<g/>
,	,	kIx,
2018-06-06	2018-06-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Po	po	k7c6
celodenním	celodenní	k2eAgInSc6d1
maratonu	maraton	k1gInSc6
má	mít	k5eAaImIp3nS
Babišova	Babišův	k2eAgFnSc1d1
druhá	druhý	k4xOgFnSc1
vláda	vláda	k1gFnSc1
důvěru	důvěra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-07-12	2018-07-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KSČM	KSČM	kA
bude	být	k5eAaImBp3nS
tolerovat	tolerovat	k5eAaImF
menšinovou	menšinový	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
a	a	k8xC
ČSSD	ČSSD	kA
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Švachulu	Švachul	k1gInSc2
obžalovali	obžalovat	k5eAaPmAgMnP
<g/>
,	,	kIx,
Faltýnek	Faltýnka	k1gFnPc2
mladší	mladý	k2eAgFnSc1d2
pozastavuje	pozastavovat	k5eAaImIp3nS
členství	členství	k1gNnSc2
v	v	k7c6
ANO	ano	k9
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
,	,	kIx,
6.3	6.3	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Po	po	k7c6
Vokřálovi	Vokřál	k1gMnSc6
z	z	k7c2
ANO	ano	k9
odchází	odcházet	k5eAaImIp3nS
jeho	jeho	k3xOp3gMnSc7
spolupracovník	spolupracovník	k1gMnSc1
Mrázek	Mrázek	k1gMnSc1
<g/>
,	,	kIx,
půjdou	jít	k5eAaImIp3nP
další	další	k2eAgFnPc1d1
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
,	,	kIx,
8.6	8.6	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Korupční	korupční	k2eAgFnSc1d1
kauza	kauza	k1gFnSc1
Stoka	stoka	k1gFnSc1
z	z	k7c2
prostředí	prostředí	k1gNnSc2
hnutí	hnutí	k1gNnSc2
ANO	ano	k9
narůstá	narůstat	k5eAaImIp3nS
<g/>
:	:	kIx,
Policie	policie	k1gFnSc1
obvinila	obvinit	k5eAaPmAgFnS
z	z	k7c2
korupce	korupce	k1gFnSc2
dalších	další	k2eAgInPc2d1
26	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
Forum	forum	k1gNnSc1
24	#num#	k4
<g/>
,	,	kIx,
8.7	8.7	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Adéla	Adéla	k1gFnSc1
Jelínková	Jelínková	k1gFnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
Valášek	Valášek	k1gMnSc1
<g/>
,	,	kIx,
Uplácelo	uplácet	k5eAaImAgNnS
90	#num#	k4
procent	procento	k1gNnPc2
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politik	politik	k1gMnSc1
ANO	ano	k9
podle	podle	k7c2
policie	policie	k1gFnSc2
ovládl	ovládnout	k5eAaPmAgInS
přidělování	přidělování	k1gNnSc4
zakázek	zakázka	k1gFnPc2
nejbohatší	bohatý	k2eAgFnSc2d3
brněnské	brněnský	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
Hospodářské	hospodářský	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
9.7	9.7	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Ondřej	Ondřej	k1gMnSc1
Koutník	Koutník	k1gMnSc1
<g/>
,	,	kIx,
Zmizel	zmizet	k5eAaPmAgMnS
vlivný	vlivný	k2eAgMnSc1d1
aktér	aktér	k1gMnSc1
kauzy	kauza	k1gFnSc2
Stoka	stoka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blízcí	blízký	k2eAgMnPc1d1
se	se	k3xPyFc4
bojí	bát	k5eAaImIp3nP
o	o	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
život	život	k1gInSc4
<g/>
,	,	kIx,
Seznam	seznam	k1gInSc4
Zprávy	zpráva	k1gFnSc2
<g/>
,	,	kIx,
5.11	5.11	k4
<g/>
.2020	.2020	k4
<g/>
↑	↑	k?
Pelikán	Pelikán	k1gMnSc1
<g/>
:	:	kIx,
V	v	k7c6
politice	politika	k1gFnSc6
končím	končit	k5eAaImIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
většinou	většina	k1gFnSc7
ANO	ano	k9
se	se	k3xPyFc4
názorově	názorově	k6eAd1
rozcházím	rozcházet	k5eAaImIp1nS
<g/>
,	,	kIx,
už	už	k9
to	ten	k3xDgNnSc1
pro	pro	k7c4
mě	já	k3xPp1nSc4
není	být	k5eNaImIp3nS
přijatelné	přijatelný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Stropnický	stropnický	k2eAgInSc4d1
složil	složit	k5eAaPmAgMnS
poslanecký	poslanecký	k2eAgInSc4d1
mandát	mandát	k1gInSc4
<g/>
,	,	kIx,
skončil	skončit	k5eAaPmAgInS
i	i	k9
ve	v	k7c6
vedení	vedení	k1gNnSc6
ANO	ano	k9
-	-	kIx~
Novinky	novinka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Velká	velký	k2eAgFnSc1d1
rána	rána	k1gFnSc1
pro	pro	k7c4
Babiše	Babiš	k1gMnSc4
<g/>
:	:	kIx,
Z	Z	kA
ANO	ano	k9
odchází	odcházet	k5eAaImIp3nS
spoluzakladatelka	spoluzakladatelka	k1gFnSc1
hnutí	hnutí	k1gNnSc2
a	a	k8xC
europoslankyně	europoslankyně	k1gFnSc1
Maxová	Maxová	k1gFnSc1
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výsledky	výsledek	k1gInPc1
voleb	volba	k1gFnPc2
2014	#num#	k4
<g/>
,	,	kIx,
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
V	v	k7c6
obcích	obec	k1gFnPc6
posílili	posílit	k5eAaPmAgMnP
nezávislí	závislý	k2eNgMnPc1d1
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gMnPc1
a	a	k8xC
STAN	stan	k1gInSc4
<g/>
,	,	kIx,
města	město	k1gNnPc4
ovládlo	ovládnout	k5eAaPmAgNnS
ANO	ano	k9
<g/>
,	,	kIx,
ceskenoviny	ceskenovina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
Minuta	minuta	k1gFnSc1
po	po	k7c6
minutě	minuta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-17	2019-02-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ANO	ano	k9
2011	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Text	text	k1gInSc1
koaliční	koaliční	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
ANO	ano	k9
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
a	a	k8xC
hnutí	hnutí	k1gNnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
(	(	kIx(
<g/>
200	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
2011	#num#	k4
(	(	kIx(
<g/>
78	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
63	#num#	k4
ANO	ano	k9
<g/>
,	,	kIx,
15	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
21	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
2	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
přímá	přímý	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
SPD	SPD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
13	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
3	#num#	k4
JAP	JAP	kA
<g/>
,	,	kIx,
3	#num#	k4
Trikolóra	trikolóra	k1gFnSc1
<g/>
)	)	kIx)
Senát	senát	k1gInSc1
(	(	kIx(
<g/>
81	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ODS	ODS	kA
a	a	k8xC
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
16	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
4	#num#	k4
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
6	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
3	#num#	k4
za	za	k7c4
ODS	ODS	kA
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
a	a	k8xC
2	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
SLK	SLK	kA
<g/>
,	,	kIx,
1	#num#	k4
Ostravak	Ostravak	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
OPAT	opat	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
MHS	MHS	kA
a	a	k8xC
13	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
–	–	k?
11	#num#	k4
za	za	k7c7
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SD	SD	kA
<g/>
–	–	k?
<g/>
SN	SN	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
KDU-ČSL	KDU-ČSL	k1gFnPc2
a	a	k8xC
5	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
4	#num#	k4
za	za	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
NK	NK	kA
<g/>
)	)	kIx)
</s>
<s>
PROREGION	PROREGION	kA
(	(	kIx(
<g/>
9	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
2	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
ANO	ano	k9
a	a	k9
6	#num#	k4
bezpartijních	bezpartijní	k2eAgMnPc2d1
<g/>
,	,	kIx,
4	#num#	k4
za	za	k7c4
ANO	ano	k9
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
ČSSD	ČSSD	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
„	„	k?
<g/>
OSN	OSN	kA
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
SEN	sen	k1gInSc1
21	#num#	k4
a	a	k8xC
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
7	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
SEN	sena	k1gFnPc2
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Zelení	zeleň	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
HPP	HPP	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
3	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
;	;	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
Piráty	pirát	k1gMnPc4
a	a	k8xC
1	#num#	k4
za	za	k7c4
HDK	HDK	kA
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
S.	S.	kA
<g/>
cz	cz	k?
a	a	k8xC
2	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
1	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
a	a	k8xC
1	#num#	k4
za	za	k7c4
Svobodní	svobodný	k2eAgMnPc1d1
<g/>
)	)	kIx)
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
RE	re	k9
</s>
<s>
ODS	ODS	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ECR	ECR	kA
</s>
<s>
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
Greens	Greens	k1gInSc4
<g/>
/	/	kIx~
<g/>
EFA	EFA	kA
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
STAN	stan	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
SPD	SPD	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ID	ido	k1gNnPc2
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
KSČM	KSČM	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
GUE-NGL	GUE-NGL	k1gFnSc6
Neparlamentní	parlamentní	k2eNgFnSc2d1
</s>
<s>
Alternativa	alternativa	k1gFnSc1
</s>
<s>
ANK	ANK	kA
2020	#num#	k4
</s>
<s>
APAČI	Apač	k1gMnSc3
2017	#num#	k4
</s>
<s>
ČP	ČP	kA
</s>
<s>
EU	EU	kA
TROLL	troll	k1gMnSc1
</s>
<s>
DSSS	DSSS	kA
</s>
<s>
DV	DV	kA
2016	#num#	k4
</s>
<s>
HLAS	hlas	k1gInSc1
</s>
<s>
IO	IO	kA
</s>
<s>
JsmePRO	JsmePRO	k?
<g/>
!	!	kIx.
</s>
<s>
M	M	kA
</s>
<s>
NBPLK	NBPLK	kA
</s>
<s>
Nezávislí	závislý	k2eNgMnPc1d1
</s>
<s>
NeKa	NeKa	k6eAd1
</s>
<s>
ODA	ODA	kA
</s>
<s>
HPP	HPP	kA
</s>
<s>
PV	PV	kA
</s>
<s>
PZS	PZS	kA
</s>
<s>
Rozumní	rozumný	k2eAgMnPc1d1
</s>
<s>
SNK	SNK	kA
ED	ED	kA
</s>
<s>
SNK1	SNK1	k4
</s>
<s>
SPOLEHNUTÍ	spolehnutí	k1gNnSc1
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
osobnosti	osobnost	k1gFnPc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
</s>
<s>
Strana	strana	k1gFnSc1
Práv	právo	k1gNnPc2
Občanů	občan	k1gMnPc2
</s>
<s>
Strana	strana	k1gFnSc1
soukromníků	soukromník	k1gMnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
T2020	T2020	k4
</s>
<s>
UFO	UFO	kA
</s>
<s>
USZ	USZ	kA
</s>
<s>
ZA	za	k7c4
OBČANY	občan	k1gMnPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Z	z	k7c2
2020	#num#	k4
Seznam	seznam	k1gInSc1
neparlamentních	parlamentní	k2eNgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
není	být	k5eNaImIp3nS
úplný	úplný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
