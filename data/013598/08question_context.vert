<s>
Tivoli	Tivoli	k6eAd1
je	být	k5eAaImIp3nS
sportovní	sportovní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
ve	v	k7c6
slovinském	slovinský	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Lublani	Lublaň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplex	komplex	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
hala	hala	k1gFnSc1
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
hlavně	hlavně	k9
pro	pro	k7c4
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
menší	malý	k2eAgFnSc1d2
hala	hala	k1gFnSc1
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
basketbalové	basketbalový	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
<g/>
.	.	kIx.
</s>