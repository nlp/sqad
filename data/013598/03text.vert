<s>
Hala	hala	k1gFnSc1
Tivoli	Tivole	k1gFnSc4
</s>
<s>
Hala	hala	k1gFnSc1
Tivoli	Tivole	k1gFnSc4
Poloha	poloha	k1gFnSc1
</s>
<s>
Lublaň	Lublaň	k1gFnSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
46	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
37,05	37,05	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
43	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Začátek	začátek	k1gInSc1
výstavby	výstavba	k1gFnSc2
</s>
<s>
listopad	listopad	k1gInSc1
1963	#num#	k4
Otevření	otevření	k1gNnSc2
</s>
<s>
březen	březen	k1gInSc1
1965	#num#	k4
Přestavění	přestavění	k1gNnSc2
</s>
<s>
2000	#num#	k4
Vlastník	vlastník	k1gMnSc1
</s>
<s>
město	město	k1gNnSc1
Lublaň	Lublaň	k1gFnSc1
Povrch	povrch	k1gInSc1
</s>
<s>
led	led	k1gInSc1
<g/>
,	,	kIx,
palubovka	palubovka	k1gFnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Marjan	Marjan	k1gMnSc1
Božič	Božič	k1gMnSc1
Stanko	Stanko	k1gMnSc1
Bloudek	bloudek	k1gMnSc1
Stavební	stavební	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
</s>
<s>
Javni	Javn	k1gMnPc1
zavod	zavoda	k1gFnPc2
Šport	Športa	k1gFnPc2
Ljubljana	Ljubljan	k1gMnSc2
Týmy	tým	k1gInPc5
</s>
<s>
HDD	HDD	kA
Telemach	Telemach	k1gMnSc1
Olimpija	Olimpija	k1gMnSc1
Kapacita	kapacita	k1gFnSc1
</s>
<s>
5600	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tivoli	Tivoli	k6eAd1
je	být	k5eAaImIp3nS
sportovní	sportovní	k2eAgFnSc1d1
hala	hala	k1gFnSc1
ve	v	k7c6
slovinském	slovinský	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Lublani	Lublaň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
parku	park	k1gInSc6
severně	severně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
města	město	k1gNnSc2
na	na	k7c6
úpatí	úpatí	k1gNnSc6
kopce	kopec	k1gInSc2
Rožnik	Rožnik	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplex	komplex	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
hala	hala	k1gFnSc1
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
hlavně	hlavně	k9
pro	pro	k7c4
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
menší	malý	k2eAgFnSc1d2
hala	hala	k1gFnSc1
<g/>
,	,	kIx,
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
basketbalové	basketbalový	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapacita	kapacita	k1gFnSc1
větší	veliký	k2eAgFnSc2d2
haly	hala	k1gFnSc2
je	být	k5eAaImIp3nS
4000	#num#	k4
osob	osoba	k1gFnPc2
pro	pro	k7c4
hokejové	hokejový	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
po	po	k7c6
odstranění	odstranění	k1gNnSc6
ledové	ledový	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
kapacita	kapacita	k1gFnSc1
zvýšit	zvýšit	k5eAaPmF
až	až	k9
na	na	k7c4
5600	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnSc1d2
hala	hala	k1gFnSc1
pojme	pojmout	k5eAaPmIp3nS
4050	#num#	k4
návštěvníků	návštěvník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Hala	hala	k1gFnSc1
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
hraje	hrát	k5eAaImIp3nS
hokejový	hokejový	k2eAgInSc1d1
klub	klub	k1gInSc1
HDD	HDD	kA
Telemach	Telemach	k1gInSc4
Olimpija	Olimpijum	k1gNnSc2
<g/>
,	,	kIx,
do	do	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
ji	on	k3xPp3gFnSc4
využíval	využívat	k5eAaImAgInS,k5eAaPmAgInS
i	i	k8xC
košíkářský	košíkářský	k2eAgInSc1d1
KK	KK	kA
Olimpija	Olimpijum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
konají	konat	k5eAaImIp3nP
koncerty	koncert	k1gInPc1
populární	populární	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
první	první	k4xOgInSc4
zde	zde	k6eAd1
vystoupil	vystoupit	k5eAaPmAgMnS
Louis	Louis	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
rekordní	rekordní	k2eAgFnSc7d1
návštěvou	návštěva	k1gFnSc7
bylo	být	k5eAaImAgNnS
8000	#num#	k4
lidí	člověk	k1gMnPc2
na	na	k7c6
koncertě	koncert	k1gInSc6
Dire	Dir	k1gFnSc2
Straits	Straitsa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Akce	akce	k1gFnSc2
konané	konaný	k2eAgFnSc2d1
v	v	k7c6
hale	hala	k1gFnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
stolním	stolní	k2eAgInSc6d1
tenise	tenis	k1gInSc6
1965	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
1966	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
basketbalu	basketbal	k1gInSc6
mužů	muž	k1gMnPc2
1970	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
krasobruslení	krasobruslení	k1gNnSc6
1970	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
vzpírání	vzpírání	k1gNnSc6
1982	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
házené	házená	k1gFnSc6
mužů	muž	k1gMnPc2
2004	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
basketbale	basketbal	k1gInSc6
mužů	muž	k1gMnPc2
2013	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.dnevnik.si/1042707270/magazin/aktualno/stozice-ji-ne-sezejo-niti-do-kolen	https://www.dnevnik.si/1042707270/magazin/aktualno/stozice-ji-ne-sezejo-niti-do-kolen	k2eAgMnSc1d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hala	hala	k1gFnSc1
Tivoli	Tivole	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Slovinsko	Slovinsko	k1gNnSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
