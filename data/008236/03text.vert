<p>
<s>
S.	S.	kA	S.
<g/>
P.	P.	kA	P.
<g/>
Q.	Q.	kA	Q.
<g/>
R.	R.	kA	R.
je	být	k5eAaImIp3nS	být
latinská	latinský	k2eAgFnSc1d1	Latinská
iniciálová	iniciálový	k2eAgFnSc1d1	iniciálová
zkratka	zkratka	k1gFnSc1	zkratka
ze	z	k7c2	z
sousloví	sousloví	k1gNnSc2	sousloví
Senatus	Senatus	k1gInSc4	Senatus
Populusque	Populusque	k1gNnSc2	Populusque
Romanus	Romanus	k1gInSc1	Romanus
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
senát	senát	k1gInSc1	senát
a	a	k8xC	a
lid	lid	k1gInSc1	lid
římský	římský	k2eAgInSc1d1	římský
<g/>
.	.	kIx.	.
</s>
<s>
Symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
složky	složka	k1gFnPc4	složka
vlády	vláda	k1gFnSc2	vláda
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Sousloví	sousloví	k1gNnSc1	sousloví
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
římské	římský	k2eAgFnSc2d1	římská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
i	i	k9	i
za	za	k7c4	za
císařství	císařství	k1gNnSc4	císařství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
SPQR	SPQR	kA	SPQR
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
výsostný	výsostný	k2eAgInSc4d1	výsostný
znak	znak	k1gInSc4	znak
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
nacházela	nacházet	k5eAaImAgFnS	nacházet
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
nebo	nebo	k8xC	nebo
na	na	k7c6	na
standartách	standarta	k1gFnPc6	standarta
římských	římský	k2eAgFnPc2d1	římská
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
římských	římský	k2eAgInPc2d1	římský
kanálových	kanálový	k2eAgInPc2d1	kanálový
poklopů	poklop	k1gInPc2	poklop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zkratku	zkratka	k1gFnSc4	zkratka
SPQ	SPQ	kA	SPQ
<g/>
*	*	kIx~	*
převzaly	převzít	k5eAaPmAgInP	převzít
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
státní	státní	k2eAgInPc1d1	státní
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
například	například	k6eAd1	například
ve	v	k7c6	v
znaku	znak	k1gInSc2	znak
některých	některý	k3yIgNnPc2	některý
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k8xC	i
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
:	:	kIx,	:
SPQO	SPQO	kA	SPQO
–	–	k?	–
Senatus	Senatus	k1gInSc1	Senatus
Populusque	Populusqu	k1gFnSc2	Populusqu
Olomouciensis	Olomouciensis	k1gFnSc2	Olomouciensis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Verze	verze	k1gFnSc1	verze
zkratky	zkratka	k1gFnSc2	zkratka
==	==	k?	==
</s>
</p>
<p>
<s>
Senatus	Senatus	k1gMnSc1	Senatus
Populusque	Populusque	k1gNnSc2	Populusque
Romanus	Romanus	k1gMnSc1	Romanus
</s>
</p>
<p>
<s>
Senatus	Senatus	k1gInSc1	Senatus
Populusque	Populusqu	k1gMnSc2	Populusqu
Romae	Roma	k1gMnSc2	Roma
</s>
</p>
<p>
<s>
Senatu	Senat	k2eAgFnSc4d1	Senat
Populoque	Populoque	k1gFnSc4	Populoque
Romae	Roma	k1gFnSc2	Roma
</s>
</p>
<p>
<s>
Senatu	Senat	k2eAgFnSc4d1	Senat
Populoque	Populoque	k1gFnSc4	Populoque
Romano	Romano	k1gMnSc1	Romano
</s>
</p>
<p>
<s>
Senatus	Senatus	k1gMnSc1	Senatus
Populusque	Populusqu	k1gFnSc2	Populusqu
Romanum	Romanum	k?	Romanum
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Římský	římský	k2eAgInSc1d1	římský
senát	senát	k1gInSc1	senát
</s>
</p>
<p>
<s>
Římská	římský	k2eAgNnPc1d1	římské
shromáždění	shromáždění	k1gNnPc1	shromáždění
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
SPQR	SPQR	kA	SPQR
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
SPQR	SPQR	kA	SPQR
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Senatus	Senatus	k1gInSc1	Senatus
populusque	populusquat	k5eAaPmIp3nS	populusquat
Romanus	Romanus	k1gInSc4	Romanus
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
