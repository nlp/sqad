<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
zavedení	zavedení	k1gNnSc4	zavedení
doktríny	doktrína	k1gFnSc2	doktrína
byly	být	k5eAaImAgInP	být
neúspěchy	neúspěch	k1gInPc1	neúspěch
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
jako	jako	k8xS	jako
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
a	a	k8xC	a
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
procházely	procházet	k5eAaImAgFnP	procházet
stagnací	stagnace	k1gFnSc7	stagnace
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
poklesem	pokles	k1gInSc7	pokles
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
prosperitou	prosperita	k1gFnSc7	prosperita
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
