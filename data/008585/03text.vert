<p>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Cold	Cold	k1gMnSc1	Cold
War	War	k1gMnSc1	War
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
х	х	k?	х
в	в	k?	в
<g/>
;	;	kIx,	;
zhruba	zhruba	k6eAd1	zhruba
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
stav	stav	k1gInSc1	stav
politického	politický	k2eAgNnSc2d1	politické
a	a	k8xC	a
vojenského	vojenský	k2eAgNnSc2d1	vojenské
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
komunistickými	komunistický	k2eAgInPc7d1	komunistický
státy	stát	k1gInPc7	stát
–	–	k?	–
zejména	zejména	k6eAd1	zejména
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
satelitními	satelitní	k2eAgInPc7d1	satelitní
státy	stát	k1gInPc7	stát
a	a	k8xC	a
spojenci	spojenec	k1gMnPc7	spojenec
–	–	k?	–
a	a	k8xC	a
západními	západní	k2eAgInPc7d1	západní
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
americkými	americký	k2eAgInPc7d1	americký
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc7	jejich
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Studené	Studená	k1gFnPc1	Studená
válce	válec	k1gInSc2	válec
předcházely	předcházet	k5eAaImAgFnP	předcházet
dvě	dva	k4xCgFnPc1	dva
největší	veliký	k2eAgFnPc1d3	veliký
světové	světový	k2eAgFnPc1d1	světová
katastrofy	katastrofa	k1gFnPc1	katastrofa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
a	a	k8xC	a
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
událostem	událost	k1gFnPc3	událost
se	se	k3xPyFc4	se
světové	světový	k2eAgNnSc1d1	světové
společenství	společenství	k1gNnSc1	společenství
dohodlo	dohodnout	k5eAaPmAgNnS	dohodnout
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Chartě	charta	k1gFnSc6	charta
ratifikované	ratifikovaný	k2eAgNnSc1d1	ratifikované
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
stanovila	stanovit	k5eAaPmAgFnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
státy	stát	k1gInPc1	stát
a	a	k8xC	a
národy	národ	k1gInPc1	národ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
členy	člen	k1gInPc1	člen
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zavazují	zavazovat	k5eAaImIp3nP	zavazovat
"	"	kIx"	"
<g/>
uchránit	uchránit	k5eAaPmF	uchránit
budoucí	budoucí	k2eAgNnPc1d1	budoucí
pokolení	pokolení	k1gNnPc1	pokolení
před	před	k7c7	před
metlou	metla	k1gFnSc7	metla
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c2	za
našeho	náš	k3xOp1gInSc2	náš
života	život	k1gInSc2	život
přinesla	přinést	k5eAaPmAgFnS	přinést
lidstvu	lidstvo	k1gNnSc3	lidstvo
nevýslovné	výslovný	k2eNgFnSc2d1	nevýslovná
strasti	strast	k1gFnSc2	strast
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
ozbrojené	ozbrojený	k2eAgInPc4d1	ozbrojený
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
období	období	k1gNnSc6	období
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
zároveň	zároveň	k6eAd1	zároveň
společné	společný	k2eAgInPc1d1	společný
něco	něco	k6eAd1	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
předchozích	předchozí	k2eAgFnPc2d1	předchozí
událostí	událost	k1gFnPc2	událost
<g/>
:	:	kIx,	:
tyto	tento	k3xDgFnPc1	tento
války	válka	k1gFnPc1	válka
jsou	být	k5eAaImIp3nP	být
nelegální	legální	k2eNgFnPc1d1	nelegální
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimiž	jenž	k3xRgInPc7	jenž
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
snahu	snaha	k1gFnSc4	snaha
organizovaným	organizovaný	k2eAgNnSc7d1	organizované
násilím	násilí	k1gNnSc7	násilí
či	či	k8xC	či
krutostí	krutost	k1gFnSc7	krutost
donutit	donutit	k5eAaPmF	donutit
protivníka	protivník	k1gMnSc4	protivník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
změnil	změnit	k5eAaPmAgInS	změnit
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
a	a	k8xC	a
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
odsouzeníhodné	odsouzeníhodný	k2eAgFnPc1d1	odsouzeníhodná
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
zhruba	zhruba	k6eAd1	zhruba
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
a	a	k8xC	a
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
nepřerostla	přerůst	k5eNaPmAgFnS	přerůst
ve	v	k7c4	v
skutečné	skutečný	k2eAgNnSc4d1	skutečné
válečné	válečný	k2eAgNnSc4d1	válečné
střetnutí	střetnutí	k1gNnSc4	střetnutí
<g/>
,	,	kIx,	,
střet	střet	k1gInSc1	střet
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
velmocemi	velmoc	k1gFnPc7	velmoc
ovšem	ovšem	k9	ovšem
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
ničím	nic	k3yNnSc7	nic
jiným	jiný	k2eAgNnSc7d1	jiné
než	než	k8xS	než
třetí	třetí	k4xOgFnSc7	třetí
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
první	první	k4xOgFnSc7	první
jadernou	jaderný	k2eAgFnSc7d1	jaderná
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zapojena	zapojen	k2eAgFnSc1d1	zapojena
celá	celý	k2eAgFnSc1d1	celá
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
odehrávalo	odehrávat	k5eAaImAgNnS	odehrávat
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
"	"	kIx"	"
<g/>
v	v	k7c6	v
zastoupení	zastoupení	k1gNnSc6	zastoupení
<g/>
"	"	kIx"	"
těchto	tento	k3xDgFnPc2	tento
supervelmocí	supervelmoc	k1gFnPc2	supervelmoc
<g/>
.	.	kIx.	.
</s>
<s>
Vedly	vést	k5eAaImAgFnP	vést
se	se	k3xPyFc4	se
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
regulérními	regulérní	k2eAgFnPc7d1	regulérní
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
války	válka	k1gFnPc1	válka
proti	proti	k7c3	proti
civilistům	civilista	k1gMnPc3	civilista
<g/>
,	,	kIx,	,
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
teroristické	teroristický	k2eAgFnSc6d1	teroristická
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
teroru	teror	k1gInSc3	teror
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
hlavní	hlavní	k2eAgMnPc1d1	hlavní
účastníci	účastník	k1gMnPc1	účastník
vedli	vést	k5eAaImAgMnP	vést
tento	tento	k3xDgInSc4	tento
boj	boj	k1gInSc4	boj
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
metodami	metoda	k1gFnPc7	metoda
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
budováním	budování	k1gNnSc7	budování
vojenských	vojenský	k2eAgFnPc2d1	vojenská
koalic	koalice	k1gFnPc2	koalice
<g/>
,	,	kIx,	,
zástupnými	zástupný	k2eAgFnPc7d1	zástupná
válkami	válka	k1gFnPc7	válka
<g/>
,	,	kIx,	,
závody	závod	k1gInPc7	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
<g/>
,	,	kIx,	,
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
pomocí	pomoc	k1gFnSc7	pomoc
zranitelným	zranitelný	k2eAgMnPc3d1	zranitelný
státům	stát	k1gInPc3	stát
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
sféře	sféra	k1gFnSc6	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
špionáží	špionáž	k1gFnSc7	špionáž
<g/>
,	,	kIx,	,
propagandou	propaganda	k1gFnSc7	propaganda
<g/>
,	,	kIx,	,
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
blokádami	blokáda	k1gFnPc7	blokáda
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
obilním	obilní	k2eAgNnSc7d1	obilní
embargem	embargo	k1gNnSc7	embargo
USA	USA	kA	USA
vůči	vůči	k7c3	vůči
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předháněním	předhánění	k1gNnSc7	předhánění
se	se	k3xPyFc4	se
v	v	k7c6	v
technické	technický	k2eAgFnSc6d1	technická
vyspělosti	vyspělost	k1gFnSc6	vyspělost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vesmírnými	vesmírný	k2eAgInPc7d1	vesmírný
závody	závod	k1gInPc7	závod
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
rivalitou	rivalita	k1gFnSc7	rivalita
při	při	k7c6	při
sportovních	sportovní	k2eAgNnPc6d1	sportovní
kláních	klání	k1gNnPc6	klání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
SSSR	SSSR	kA	SSSR
i	i	k8xC	i
USA	USA	kA	USA
patřily	patřit	k5eAaImAgFnP	patřit
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
mezi	mezi	k7c4	mezi
Spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
neshodly	shodnout	k5eNaPmAgInP	shodnout
se	se	k3xPyFc4	se
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
filosofii	filosofie	k1gFnSc6	filosofie
a	a	k8xC	a
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
na	na	k7c6	na
uspořádání	uspořádání	k1gNnSc6	uspořádání
poválečného	poválečný	k2eAgInSc2d1	poválečný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
východní	východní	k2eAgInSc1d1	východní
blok	blok	k1gInSc1	blok
ze	z	k7c2	z
států	stát	k1gInPc2	stát
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zprvu	zprvu	k6eAd1	zprvu
také	také	k9	také
vojensky	vojensky	k6eAd1	vojensky
okupoval	okupovat	k5eAaBmAgMnS	okupovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
upevnil	upevnit	k5eAaPmAgInS	upevnit
moc	moc	k1gFnSc4	moc
vytvořením	vytvoření	k1gNnSc7	vytvoření
vojenského	vojenský	k2eAgNnSc2d1	vojenské
uskupení	uskupení	k1gNnSc2	uskupení
zvaného	zvaný	k2eAgInSc2d1	zvaný
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
smlouva	smlouva	k1gFnSc1	smlouva
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
zvaného	zvaný	k2eAgMnSc4d1	zvaný
Rada	Rada	k1gMnSc1	Rada
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
pomoci	pomoc	k1gFnSc2	pomoc
(	(	kIx(	(
<g/>
RVHP	RVHP	kA	RVHP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
naopak	naopak	k6eAd1	naopak
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
západní	západní	k2eAgInSc4d1	západní
blok	blok	k1gInSc4	blok
ze	z	k7c2	z
států	stát	k1gInPc2	stát
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc6d1	hlavní
strategii	strategie	k1gFnSc6	strategie
používaly	používat	k5eAaImAgInP	používat
doktrínu	doktrína	k1gFnSc4	doktrína
zadržování	zadržování	k1gNnSc2	zadržování
(	(	kIx(	(
<g/>
containment	containment	k1gInSc1	containment
<g/>
)	)	kIx)	)
komunismu	komunismus	k1gInSc2	komunismus
–	–	k?	–
jakožto	jakožto	k8xS	jakožto
ideologie	ideologie	k1gFnSc2	ideologie
i	i	k8xC	i
státního	státní	k2eAgNnSc2d1	státní
uspořádání	uspořádání	k1gNnSc2	uspořádání
–	–	k?	–
a	a	k8xC	a
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
založily	založit	k5eAaPmAgFnP	založit
několik	několik	k4yIc4	několik
smluvních	smluvní	k2eAgFnPc2d1	smluvní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Severoatlantickou	severoatlantický	k2eAgFnSc4d1	Severoatlantická
alianci	aliance	k1gFnSc4	aliance
(	(	kIx(	(
<g/>
North	North	k1gInSc1	North
Atlantic	Atlantice	k1gFnPc2	Atlantice
Treaty	Treata	k1gFnSc2	Treata
Organization	Organization	k1gInSc1	Organization
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zůstaly	zůstat	k5eAaPmAgInP	zůstat
mimo	mimo	k7c4	mimo
dvě	dva	k4xCgNnPc4	dva
rivalizující	rivalizující	k2eAgNnPc4d1	rivalizující
společenství	společenství	k1gNnPc4	společenství
<g/>
,	,	kIx,	,
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
Hnutí	hnutí	k1gNnSc4	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
USA	USA	kA	USA
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Marshallův	Marshallův	k2eAgInSc4d1	Marshallův
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
rychlejší	rychlý	k2eAgFnSc4d2	rychlejší
obnovu	obnova	k1gFnSc4	obnova
poválečné	poválečný	k2eAgFnSc2d1	poválečná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nenechal	nechat	k5eNaPmAgInS	nechat
žádný	žádný	k3yNgInSc4	žádný
ze	z	k7c2	z
států	stát	k1gInPc2	stát
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
plán	plán	k1gInSc4	plán
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
zase	zase	k9	zase
SSSR	SSSR	kA	SSSR
podporovalo	podporovat	k5eAaImAgNnS	podporovat
socialistické	socialistický	k2eAgFnPc4d1	socialistická
revoluce	revoluce	k1gFnPc4	revoluce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
mnoha	mnoho	k4c3	mnoho
západním	západní	k2eAgFnPc3d1	západní
zemím	zem	k1gFnPc3	zem
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
snažily	snažit	k5eAaImAgFnP	snažit
socialistický	socialistický	k2eAgInSc4d1	socialistický
režim	režim	k1gInSc4	režim
svrhnout	svrhnout	k5eAaPmF	svrhnout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přineslo	přinést	k5eAaPmAgNnS	přinést
smíšené	smíšený	k2eAgInPc4d1	smíšený
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
období	období	k1gNnSc4	období
relativního	relativní	k2eAgInSc2d1	relativní
klidu	klid	k1gInSc2	klid
i	i	k8xC	i
vysokého	vysoký	k2eAgNnSc2d1	vysoké
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
napětí	napětí	k1gNnSc2	napětí
–	–	k?	–
např.	např.	kA	např.
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
blokáda	blokáda	k1gFnSc1	blokáda
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
berlínská	berlínský	k2eAgFnSc1d1	Berlínská
krize	krize	k1gFnSc1	krize
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zkušební	zkušební	k2eAgInSc1d1	zkušební
jaderný	jaderný	k2eAgInSc1d1	jaderný
útok	útok	k1gInSc1	útok
NATO	NATO	kA	NATO
Able	Able	k1gInSc4	Able
Archer	Archra	k1gFnPc2	Archra
83	[number]	k4	83
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
využívaly	využívat	k5eAaPmAgFnP	využívat
politiku	politika	k1gFnSc4	politika
détente	détente	k2eAgNnPc2d1	détente
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
doktrínu	doktrína	k1gFnSc4	doktrína
vzájemně	vzájemně	k6eAd1	vzájemně
zaručeného	zaručený	k2eAgInSc2d1	zaručený
zničení	zničení	k1gNnSc1	zničení
jadernými	jaderný	k2eAgFnPc7d1	jaderná
zbraněmi	zbraň	k1gFnPc7	zbraň
k	k	k7c3	k
vyhnutí	vyhnutí	k1gNnSc3	vyhnutí
se	s	k7c7	s
přímým	přímý	k2eAgNnSc7d1	přímé
vojenským	vojenský	k2eAgNnSc7d1	vojenské
střetnutím	střetnutí	k1gNnSc7	střetnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
USA	USA	kA	USA
pod	pod	k7c7	pod
Reaganovou	Reaganův	k2eAgFnSc7d1	Reaganova
doktrínou	doktrína	k1gFnSc7	doktrína
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
diplomatický	diplomatický	k2eAgInSc1d1	diplomatický
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgInSc1d1	vojenský
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stát	stát	k1gInSc1	stát
trpěl	trpět	k5eAaImAgInS	trpět
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
stagnací	stagnace	k1gFnSc7	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Michail	Michail	k1gMnSc1	Michail
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Gorbačov	Gorbačov	k1gInSc4	Gorbačov
zavedl	zavést	k5eAaPmAgMnS	zavést
reformní	reformní	k2eAgInPc4d1	reformní
kroky	krok	k1gInPc4	krok
perestrojka	perestrojka	k1gFnSc1	perestrojka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
a	a	k8xC	a
glasnosť	glasnosť	k1gFnSc1	glasnosť
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
otevřenost	otevřenost	k1gFnSc1	otevřenost
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
cca	cca	kA	cca
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nakonec	nakonec	k6eAd1	nakonec
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
zůstaly	zůstat	k5eAaPmAgInP	zůstat
dominantní	dominantní	k2eAgFnSc7d1	dominantní
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
mocností	mocnost	k1gFnSc7	mocnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
názvu	název	k1gInSc2	název
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
použil	použít	k5eAaPmAgInS	použít
termín	termín	k1gInSc1	termín
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
cold	cold	k1gInSc1	cold
war	war	k?	war
<g/>
)	)	kIx)	)
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
George	Georg	k1gMnSc2	Georg
Orwell	Orwell	k1gMnSc1	Orwell
v	v	k7c6	v
eseji	esej	k1gInSc6	esej
"	"	kIx"	"
<g/>
You	You	k1gFnSc1	You
and	and	k?	and
the	the	k?	the
Atomic	Atomice	k1gFnPc2	Atomice
Bomb	bomba	k1gFnPc2	bomba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Vy	vy	k3xPp2nPc1	vy
a	a	k8xC	a
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vydané	vydaný	k2eAgFnPc4d1	vydaná
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
Tribune	tribun	k1gMnSc5	tribun
<g/>
.	.	kIx.	.
</s>
<s>
Přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
o	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
neustálou	neustálý	k2eAgFnSc7d1	neustálá
hrozbou	hrozba	k1gFnSc7	hrozba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
varoval	varovat	k5eAaImAgMnS	varovat
před	před	k7c7	před
falešným	falešný	k2eAgInSc7d1	falešný
mírem	mír	k1gInSc7	mír
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
říkal	říkat	k5eAaImAgMnS	říkat
"	"	kIx"	"
<g/>
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
nazýval	nazývat	k5eAaImAgInS	nazývat
ideologický	ideologický	k2eAgInSc1d1	ideologický
střet	střet	k1gInSc1	střet
mezi	mezi	k7c7	mezi
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
západními	západní	k2eAgFnPc7d1	západní
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1946	[number]	k4	1946
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
The	The	k1gMnSc1	The
Observer	Observer	k1gMnSc1	Observer
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Po	po	k7c6	po
moskevské	moskevský	k2eAgFnSc6d1	Moskevská
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
položilo	položit	k5eAaPmAgNnS	položit
Rusko	Rusko	k1gNnSc1	Rusko
základy	základ	k1gInPc1	základ
pro	pro	k7c4	pro
,	,	kIx,	,
<g/>
studenou	studený	k2eAgFnSc4d1	studená
válku	válka	k1gFnSc4	válka
<g/>
'	'	kIx"	'
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Britským	britský	k2eAgNnSc7d1	Britské
impériem	impérium	k1gNnSc7	impérium
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
poválečného	poválečný	k2eAgNnSc2d1	poválečné
geopolitického	geopolitický	k2eAgNnSc2d1	geopolitické
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc7	jeho
satelitními	satelitní	k2eAgInPc7d1	satelitní
státy	stát	k1gInPc7	stát
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
západoevropskými	západoevropský	k2eAgFnPc7d1	západoevropská
zeměmi	zem	k1gFnPc7	zem
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc2	druhý
poprvé	poprvé	k6eAd1	poprvé
oficiálně	oficiálně	k6eAd1	oficiálně
použil	použít	k5eAaPmAgMnS	použít
Bernard	Bernard	k1gMnSc1	Bernard
Baruch	Baruch	k1gMnSc1	Baruch
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
finančník	finančník	k1gMnSc1	finančník
a	a	k8xC	a
prezidentský	prezidentský	k2eAgMnSc1d1	prezidentský
poradce	poradce	k1gMnSc1	poradce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Karolíně	Karolína	k1gFnSc6	Karolína
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1947	[number]	k4	1947
přednesl	přednést	k5eAaPmAgInS	přednést
projev	projev	k1gInSc1	projev
napsaný	napsaný	k2eAgInSc1d1	napsaný
žurnalistou	žurnalista	k1gMnSc7	žurnalista
Herbertem	Herbert	k1gMnSc7	Herbert
Swopem	Swop	k1gMnSc7	Swop
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nenechme	nechat	k5eNaPmRp1nP	nechat
se	se	k3xPyFc4	se
klamat	klamat	k5eAaImF	klamat
<g/>
:	:	kIx,	:
jsme	být	k5eAaImIp1nP	být
uprostřed	uprostřed	k7c2	uprostřed
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Novinář	novinář	k1gMnSc1	novinář
Walter	Walter	k1gMnSc1	Walter
Lippmann	Lippmann	k1gMnSc1	Lippmann
termín	termín	k1gInSc4	termín
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
svou	svůj	k3xOyFgFnSc7	svůj
knihou	kniha	k1gFnSc7	kniha
Cold	Colda	k1gFnPc2	Colda
War	War	k1gMnPc1	War
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozadí	pozadí	k1gNnSc1	pozadí
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
historiky	historik	k1gMnPc7	historik
nepanuje	panovat	k5eNaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
v	v	k7c6	v
určení	určení	k1gNnSc6	určení
začátku	začátek	k1gInSc2	začátek
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
historiků	historik	k1gMnPc2	historik
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
období	období	k1gNnSc1	období
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
ho	on	k3xPp3gNnSc4	on
ale	ale	k9	ale
vidí	vidět	k5eAaImIp3nS	vidět
už	už	k6eAd1	už
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
Ruským	ruský	k2eAgNnSc7d1	ruské
impériem	impérium	k1gNnSc7	impérium
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
ale	ale	k8xC	ale
trvalo	trvat	k5eAaImAgNnS	trvat
již	již	k6eAd1	již
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kvůli	kvůli	k7c3	kvůli
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
východní	východní	k2eAgFnSc3d1	východní
otázce	otázka	k1gFnSc3	otázka
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
sporům	spor	k1gInPc3	spor
o	o	k7c4	o
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
bolševické	bolševický	k2eAgFnSc6d1	bolševická
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
ostatní	ostatní	k2eAgInPc1d1	ostatní
státy	stát	k1gInPc1	stát
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
vůči	vůči	k7c3	vůči
SSSR	SSSR	kA	SSSR
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
postoj	postoj	k1gInSc4	postoj
a	a	k8xC	a
izolovaly	izolovat	k5eAaBmAgFnP	izolovat
jej	on	k3xPp3gNnSc4	on
od	od	k7c2	od
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
diplomacie	diplomacie	k1gFnSc2	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
Vladimir	Vladimir	k1gMnSc1	Vladimir
Iljič	Iljič	k1gMnSc1	Iljič
Lenin	Lenin	k1gMnSc1	Lenin
to	ten	k3xDgNnSc4	ten
komentoval	komentovat	k5eAaBmAgMnS	komentovat
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
nepřátelském	přátelský	k2eNgNnSc6d1	nepřátelské
kapitalistickém	kapitalistický	k2eAgNnSc6d1	kapitalistické
obklíčení	obklíčení	k1gNnSc6	obklíčení
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
diplomacii	diplomacie	k1gFnSc6	diplomacie
hleděl	hledět	k5eAaImAgMnS	hledět
jako	jako	k9	jako
na	na	k7c4	na
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pomůže	pomoct	k5eAaPmIp3nS	pomoct
Sovětům	Sovět	k1gMnPc3	Sovět
udržet	udržet	k5eAaPmF	udržet
své	svůj	k3xOyFgMnPc4	svůj
nepřátele	nepřítel	k1gMnPc4	nepřítel
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
Kominternu	Kominterna	k1gFnSc4	Kominterna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podněcovala	podněcovat	k5eAaImAgFnS	podněcovat
celosvětové	celosvětový	k2eAgNnSc4d1	celosvětové
komunistické	komunistický	k2eAgNnSc4d1	komunistické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
<g/>
Leninův	Leninův	k2eAgMnSc1d1	Leninův
nástupce	nástupce	k1gMnSc1	nástupce
Stalin	Stalin	k1gMnSc1	Stalin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
předvídal	předvídat	k5eAaImAgInS	předvídat
vznik	vznik	k1gInSc1	vznik
bipolárního	bipolární	k2eAgInSc2d1	bipolární
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
když	když	k8xS	když
předestřel	předestřít	k5eAaPmAgMnS	předestřít
vizi	vize	k1gFnSc4	vize
vzniku	vznik	k1gInSc2	vznik
dvou	dva	k4xCgNnPc2	dva
center	centrum	k1gNnPc2	centrum
–	–	k?	–
kapitalistického	kapitalistický	k2eAgInSc2d1	kapitalistický
a	a	k8xC	a
socialistického	socialistický	k2eAgInSc2d1	socialistický
(	(	kIx(	(
<g/>
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
)	)	kIx)	)
–	–	k?	–
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
budou	být	k5eAaImBp3nP	být
přitahovat	přitahovat	k5eAaImF	přitahovat
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
stejným	stejný	k2eAgNnSc7d1	stejné
zřízením	zřízení	k1gNnSc7	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
však	však	k9	však
hodnotil	hodnotit	k5eAaImAgMnS	hodnotit
situaci	situace	k1gFnSc4	situace
jako	jako	k8xC	jako
období	období	k1gNnSc4	období
pouhé	pouhý	k2eAgFnSc2d1	pouhá
"	"	kIx"	"
<g/>
dočasné	dočasný	k2eAgFnSc2d1	dočasná
stabilizace	stabilizace	k1gFnSc2	stabilizace
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
diplomacie	diplomacie	k1gFnSc1	diplomacie
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
zájmům	zájem	k1gInPc3	zájem
své	svůj	k3xOyFgFnPc4	svůj
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
socialistického	socialistický	k2eAgInSc2d1	socialistický
ostrova	ostrov	k1gInSc2	ostrov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
bránění	bránění	k1gNnSc1	bránění
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
agresi	agrese	k1gFnSc3	agrese
i	i	k8xC	i
využívání	využívání	k1gNnSc4	využívání
rozporů	rozpor	k1gInPc2	rozpor
mezi	mezi	k7c7	mezi
nepřátelskými	přátelský	k2eNgInPc7d1	nepřátelský
státy	stát	k1gInPc7	stát
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
výhod	výhoda	k1gFnPc2	výhoda
pro	pro	k7c4	pro
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
<g/>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
nedůvěra	nedůvěra	k1gFnSc1	nedůvěra
a	a	k8xC	a
podezřívání	podezřívání	k1gNnSc1	podezřívání
mezi	mezi	k7c7	mezi
západními	západní	k2eAgInPc7d1	západní
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
meziválečné	meziválečný	k2eAgNnSc4d1	meziválečné
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
živilo	živit	k5eAaImAgNnS	živit
ji	on	k3xPp3gFnSc4	on
např.	např.	kA	např.
bolševické	bolševický	k2eAgNnSc1d1	bolševické
odsuzování	odsuzování	k1gNnSc1	odsuzování
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
kritika	kritika	k1gFnSc1	kritika
socialismu	socialismus	k1gInSc2	socialismus
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
pokračující	pokračující	k2eAgFnSc1d1	pokračující
podpora	podpora	k1gFnSc1	podpora
antibolševických	antibolševický	k2eAgMnPc2d1	antibolševický
bělogvardějců	bělogvardějec	k1gMnPc2	bělogvardějec
Západem	západ	k1gInSc7	západ
a	a	k8xC	a
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
financování	financování	k1gNnSc2	financování
britské	britský	k2eAgFnSc2d1	britská
generální	generální	k2eAgFnSc2d1	generální
stávky	stávka	k1gFnSc2	stávka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
sovětskými	sovětský	k2eAgInPc7d1	sovětský
odbory	odbor	k1gInPc7	odbor
<g/>
;	;	kIx,	;
odmítání	odmítání	k1gNnSc1	odmítání
kapitalistických	kapitalistický	k2eAgFnPc2d1	kapitalistická
zemí	zem	k1gFnPc2	zem
navázat	navázat	k5eAaPmF	navázat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
–	–	k?	–
Američané	Američan	k1gMnPc1	Američan
uznali	uznat	k5eAaPmAgMnP	uznat
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
;	;	kIx,	;
a	a	k8xC	a
stalinistické	stalinistický	k2eAgInPc1d1	stalinistický
Moskevské	moskevský	k2eAgInPc1d1	moskevský
procesy	proces	k1gInPc1	proces
při	při	k7c6	při
Velké	velký	k2eAgFnSc6d1	velká
čistce	čistka	k1gFnSc6	čistka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
obviňováni	obviňovat	k5eAaImNgMnP	obviňovat
ze	z	k7c2	z
špionáže	špionáž	k1gFnSc2	špionáž
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
invazi	invaze	k1gFnSc6	invaze
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1941	[number]	k4	1941
se	se	k3xPyFc4	se
Spojenci	spojenec	k1gMnPc1	spojenec
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
;	;	kIx,	;
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
oficiální	oficiální	k2eAgNnSc4d1	oficiální
spojenectví	spojenectví	k1gNnSc4	spojenectví
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
neoficiální	oficiální	k2eNgInPc1d1	neoficiální
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
dodávaly	dodávat	k5eAaImAgInP	dodávat
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
a	a	k8xC	a
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
výzbroj	výzbroj	k1gInSc4	výzbroj
<g/>
,	,	kIx,	,
výstroj	výstroj	k1gFnSc4	výstroj
a	a	k8xC	a
materiály	materiál	k1gInPc4	materiál
přes	přes	k7c4	přes
program	program	k1gInSc4	program
půjčky	půjčka	k1gFnSc2	půjčka
a	a	k8xC	a
pronájmu	pronájem	k1gInSc2	pronájem
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
sovětské	sovětský	k2eAgNnSc1d1	sovětské
vedení	vedení	k1gNnSc1	vedení
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
podezřívavé	podezřívavý	k2eAgNnSc1d1	podezřívavé
a	a	k8xC	a
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Američané	Američan	k1gMnPc1	Američan
se	se	k3xPyFc4	se
spikli	spiknout	k5eAaPmAgMnP	spiknout
a	a	k8xC	a
záměrně	záměrně	k6eAd1	záměrně
oddálili	oddálit	k5eAaPmAgMnP	oddálit
vznik	vznik	k1gInSc4	vznik
druhé	druhý	k4xOgFnSc2	druhý
protiněmecké	protiněmecký	k2eAgFnSc2d1	protiněmecká
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nacistickému	nacistický	k2eAgInSc3d1	nacistický
náporu	nápor	k1gInSc3	nápor
čelil	čelit	k5eAaImAgMnS	čelit
co	co	k9	co
nejdéle	dlouho	k6eAd3	dlouho
pouze	pouze	k6eAd1	pouze
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konec	konec	k1gInSc4	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
poválečné	poválečný	k2eAgNnSc4d1	poválečné
období	období	k1gNnSc4	období
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
47	[number]	k4	47
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Konference	konference	k1gFnPc1	konference
během	během	k7c2	během
války	válka	k1gFnSc2	válka
o	o	k7c6	o
poválečné	poválečný	k2eAgFnSc6d1	poválečná
Evropě	Evropa	k1gFnSc6	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
se	se	k3xPyFc4	se
neshodli	shodnout	k5eNaPmAgMnP	shodnout
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
představách	představa	k1gFnPc6	představa
o	o	k7c6	o
uspořádání	uspořádání	k1gNnSc6	uspořádání
poválečné	poválečný	k2eAgFnSc2d1	poválečná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
strana	strana	k1gFnSc1	strana
měla	mít	k5eAaImAgFnS	mít
odlišné	odlišný	k2eAgInPc4d1	odlišný
názory	názor	k1gInPc4	názor
na	na	k7c4	na
udržování	udržování	k1gNnSc4	udržování
poválečné	poválečný	k2eAgFnSc2d1	poválečná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnPc1d1	západní
Spojenci	spojenec	k1gMnPc1	spojenec
si	se	k3xPyFc3	se
přáli	přát	k5eAaImAgMnP	přát
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
by	by	kYmCp3nP	by
demokratické	demokratický	k2eAgFnPc1d1	demokratická
země	zem	k1gFnPc1	zem
řešily	řešit	k5eAaImAgFnP	řešit
konflikty	konflikt	k1gInPc4	konflikt
mírovou	mírový	k2eAgFnSc7d1	mírová
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
organizace	organizace	k1gFnPc4	organizace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
chtěl	chtít	k5eAaImAgInS	chtít
zvýšit	zvýšit	k5eAaPmF	zvýšit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
vměšováním	vměšování	k1gNnSc7	vměšování
se	se	k3xPyFc4	se
do	do	k7c2	do
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
politiky	politika	k1gFnSc2	politika
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ho	on	k3xPp3gInSc4	on
obklopovaly	obklopovat	k5eAaImAgFnP	obklopovat
<g/>
.	.	kIx.	.
<g/>
Rozdíl	rozdíl	k1gInSc1	rozdíl
byl	být	k5eAaImAgInS	být
i	i	k9	i
mezi	mezi	k7c7	mezi
představami	představa	k1gFnPc7	představa
USA	USA	kA	USA
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Rooseveltovy	Rooseveltův	k2eAgInPc1d1	Rooseveltův
cíle	cíl	k1gInPc1	cíl
–	–	k?	–
vojenské	vojenský	k2eAgNnSc4d1	vojenské
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgNnSc4d1	ekonomické
překonání	překonání	k1gNnSc4	překonání
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
založení	založení	k1gNnSc1	založení
světové	světový	k2eAgFnSc2d1	světová
mírové	mírový	k2eAgFnSc2d1	mírová
organizace	organizace	k1gFnSc2	organizace
–	–	k?	–
byly	být	k5eAaImAgFnP	být
globálnějšího	globální	k2eAgInSc2d2	globálnější
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
Churchill	Churchill	k1gInSc1	Churchill
za	za	k7c4	za
hlavní	hlavní	k2eAgInPc4d1	hlavní
cíle	cíl	k1gInPc4	cíl
považoval	považovat	k5eAaImAgMnS	považovat
ochranu	ochrana	k1gFnSc4	ochrana
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
nezávislost	nezávislost	k1gFnSc4	nezávislost
východoevropských	východoevropský	k2eAgInPc2d1	východoevropský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
fungovaly	fungovat	k5eAaImAgInP	fungovat
jako	jako	k8xS	jako
nárazníkové	nárazníkový	k2eAgInPc1d1	nárazníkový
státy	stát	k1gInPc1	stát
mezi	mezi	k7c7	mezi
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
Američany	Američan	k1gMnPc4	Američan
představoval	představovat	k5eAaImAgMnS	představovat
Stalin	Stalin	k1gMnSc1	Stalin
potenciálního	potenciální	k2eAgMnSc2d1	potenciální
spojence	spojenec	k1gMnSc2	spojenec
při	při	k7c6	při
dosahování	dosahování	k1gNnSc6	dosahování
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Brity	Brit	k1gMnPc4	Brit
naopak	naopak	k6eAd1	naopak
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Sověti	Sovět	k1gMnPc1	Sovět
ovládali	ovládat	k5eAaImAgMnP	ovládat
většinu	většina	k1gFnSc4	většina
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
západní	západní	k2eAgFnPc1d1	západní
velmoci	velmoc	k1gFnPc1	velmoc
se	se	k3xPyFc4	se
přely	přít	k5eAaImAgFnP	přít
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
přízeň	přízeň	k1gFnSc4	přízeň
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
myšlení	myšlení	k1gNnSc6	myšlení
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
několika	několik	k4yIc3	několik
individuálním	individuální	k2eAgFnPc3d1	individuální
dohodám	dohoda	k1gFnPc3	dohoda
Sovětů	Sověty	k1gInPc2	Sověty
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1944	[number]	k4	1944
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
Churchill	Churchill	k1gMnSc1	Churchill
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Balkánu	Balkán	k1gInSc2	Balkán
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
v	v	k7c6	v
Jaltě	Jalta	k1gFnSc6	Jalta
podepsal	podepsat	k5eAaPmAgMnS	podepsat
se	s	k7c7	s
Stalinem	Stalin	k1gMnSc7	Stalin
dohodu	dohoda	k1gFnSc4	dohoda
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podpořit	podpořit	k5eAaPmF	podpořit
Churchilla	Churchilla	k1gMnSc1	Churchilla
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
Spojenců	spojenec	k1gMnPc2	spojenec
o	o	k7c6	o
poválečné	poválečný	k2eAgFnSc6d1	poválečná
situaci	situace	k1gFnSc6	situace
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c6	na
jaltské	jaltský	k2eAgFnSc6d1	Jaltská
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
mocnosti	mocnost	k1gFnPc1	mocnost
jednoznačně	jednoznačně	k6eAd1	jednoznačně
neshodly	shodnout	k5eNaPmAgFnP	shodnout
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Harry	Harra	k1gFnSc2	Harra
S.	S.	kA	S.
Truman	Truman	k1gMnSc1	Truman
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
podporou	podpora	k1gFnSc7	podpora
Polského	polský	k2eAgInSc2d1	polský
výboru	výbor	k1gInSc2	výbor
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
Sověty	Sovět	k1gMnPc4	Sovět
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
konkurence	konkurence	k1gFnSc1	konkurence
Polské	polský	k2eAgFnSc2d1	polská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
začali	začít	k5eAaPmAgMnP	začít
Sověti	Sovět	k1gMnPc1	Sovět
okupovat	okupovat	k5eAaBmF	okupovat
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
západní	západní	k2eAgMnPc1d1	západní
Spojenci	spojenec	k1gMnPc1	spojenec
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Okupované	okupovaný	k2eAgNnSc1d1	okupované
Německo	Německo	k1gNnSc1	Německo
si	se	k3xPyFc3	se
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
rozdělily	rozdělit	k5eAaPmAgInP	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
okupační	okupační	k2eAgFnPc4d1	okupační
zóny	zóna	k1gFnPc4	zóna
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
Spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
udržení	udržení	k1gNnSc2	udržení
světového	světový	k2eAgInSc2d1	světový
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
funkčnost	funkčnost	k1gFnSc1	funkčnost
její	její	k3xOp3gFnSc2	její
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
možností	možnost	k1gFnSc7	možnost
veta	veto	k1gNnSc2	veto
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
má	mít	k5eAaImIp3nS	mít
každý	každý	k3xTgMnSc1	každý
její	její	k3xOp3gMnSc1	její
člen	člen	k1gMnSc1	člen
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
OSN	OSN	kA	OSN
nakonec	nakonec	k6eAd1	nakonec
ztratila	ztratit	k5eAaPmAgNnP	ztratit
svou	svůj	k3xOyFgFnSc4	svůj
efektivitu	efektivita	k1gFnSc4	efektivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Postupimská	postupimský	k2eAgFnSc1d1	Postupimská
konference	konference	k1gFnSc1	konference
a	a	k8xC	a
porážka	porážka	k1gFnSc1	porážka
Japonska	Japonsko	k1gNnSc2	Japonsko
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
postupimské	postupimský	k2eAgFnSc6d1	Postupimská
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
července	červenec	k1gInSc2	červenec
po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Velká	velký	k2eAgFnSc1d1	velká
trojka	trojka	k1gFnSc1	trojka
dále	daleko	k6eAd2	daleko
dohadovala	dohadovat	k5eAaImAgFnS	dohadovat
o	o	k7c6	o
uspořádání	uspořádání	k1gNnSc6	uspořádání
poválečné	poválečný	k2eAgFnSc2d1	poválečná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Antipatie	antipatie	k1gFnSc1	antipatie
mezi	mezi	k7c7	mezi
mocnostmi	mocnost	k1gFnPc7	mocnost
nadále	nadále	k6eAd1	nadále
narůstala	narůstat	k5eAaImAgFnS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
konferenci	konference	k1gFnSc6	konference
Truman	Truman	k1gMnSc1	Truman
informoval	informovat	k5eAaBmAgMnS	informovat
Stalina	Stalin	k1gMnSc4	Stalin
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
silnou	silný	k2eAgFnSc4d1	silná
novou	nový	k2eAgFnSc4d1	nová
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Američané	Američan	k1gMnPc1	Američan
vyvíjeli	vyvíjet	k5eAaImAgMnP	vyvíjet
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
sovětský	sovětský	k2eAgInSc4d1	sovětský
program	program	k1gInSc4	program
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
vývoj	vývoj	k1gInSc4	vývoj
také	také	k9	také
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgInS	přijmout
zprávu	zpráva	k1gFnSc4	zpráva
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
ovšem	ovšem	k9	ovšem
se	s	k7c7	s
svržením	svržení	k1gNnSc7	svržení
jaderných	jaderný	k2eAgFnPc2d1	jaderná
hlavic	hlavice	k1gFnPc2	hlavice
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
učinily	učinit	k5eAaPmAgInP	učinit
týden	týden	k1gInSc4	týden
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
konečných	konečný	k2eAgFnPc2d1	konečná
fází	fáze	k1gFnPc2	fáze
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zformoval	zformovat	k5eAaPmAgInS	zformovat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
východní	východní	k2eAgInSc1d1	východní
blok	blok	k1gInSc1	blok
anexí	anexe	k1gFnPc2	anexe
několika	několik	k4yIc2	několik
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
původně	původně	k6eAd1	původně
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
paktem	pakt	k1gInSc7	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
<g/>
,	,	kIx,	,
jako	jako	k9	jako
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
bylo	být	k5eAaImAgNnS	být
východní	východní	k2eAgNnSc1d1	východní
Polsko	Polsko	k1gNnSc1	Polsko
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
(	(	kIx(	(
<g/>
z	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
(	(	kIx(	(
<g/>
Estonská	estonský	k2eAgFnSc1d1	Estonská
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Litva	Litva	k1gFnSc1	Litva
(	(	kIx(	(
<g/>
Litevská	litevský	k2eAgFnSc1d1	Litevská
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
část	část	k1gFnSc4	část
východního	východní	k2eAgNnSc2d1	východní
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(	(
<g/>
připojené	připojený	k2eAgInPc4d1	připojený
ke	k	k7c3	k
Karelo-finské	Kareloinský	k2eAgNnSc1d1	Karelo-finský
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
a	a	k8xC	a
východní	východní	k2eAgNnSc1d1	východní
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
(	(	kIx(	(
<g/>
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
SSR	SSR	kA	SSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Východoevropské	východoevropský	k2eAgInPc4d1	východoevropský
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Sověti	Sovět	k1gMnPc1	Sovět
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
od	od	k7c2	od
nacistů	nacista	k1gMnPc2	nacista
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
postupně	postupně	k6eAd1	postupně
začleněny	začleněn	k2eAgInPc4d1	začleněn
jako	jako	k8xS	jako
satelitní	satelitní	k2eAgInPc4d1	satelitní
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patřila	patřit	k5eAaImAgFnS	patřit
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Polská	polský	k2eAgFnSc1d1	polská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
též	též	k9	též
Československá	československý	k2eAgFnSc1d1	Československá
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rumunská	rumunský	k2eAgFnSc1d1	rumunská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Albánská	albánský	k2eAgFnSc1d1	albánská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
většiny	většina	k1gFnSc2	většina
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
dislokovaná	dislokovaný	k2eAgFnSc1d1	dislokovaná
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
až	až	k9	až
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
převzala	převzít	k5eAaPmAgFnS	převzít
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vybudování	vybudování	k1gNnSc3	vybudování
napětí	napětí	k1gNnSc3	napětí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1946	[number]	k4	1946
napsal	napsat	k5eAaBmAgMnS	napsat
George	George	k1gFnSc4	George
Frost	Frost	k1gMnSc1	Frost
Kennan	Kennan	k1gMnSc1	Kennan
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
telegram	telegram	k1gInSc4	telegram
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byla	být	k5eAaImAgFnS	být
vytyčena	vytyčen	k2eAgFnSc1d1	vytyčena
nová	nový	k2eAgFnSc1d1	nová
strategie	strategie	k1gFnSc1	strategie
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
vůči	vůči	k7c3	vůči
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1946	[number]	k4	1946
pronesl	pronést	k5eAaPmAgMnS	pronést
americký	americký	k2eAgMnSc1d1	americký
státník	státník	k1gMnSc1	státník
James	James	k1gMnSc1	James
Francis	Francis	k1gFnSc2	Francis
Byrnes	Byrnes	k1gMnSc1	Byrnes
proslov	proslov	k1gInSc1	proslov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
Morgenthauův	Morgenthauův	k2eAgInSc1d1	Morgenthauův
plán	plán	k1gInSc1	plán
(	(	kIx(	(
<g/>
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
a	a	k8xC	a
deindustrializaci	deindustrializace	k1gFnSc4	deindustrializace
poválečného	poválečný	k2eAgNnSc2d1	poválečné
Německa	Německo	k1gNnSc2	Německo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
upozornil	upozornit	k5eAaPmAgMnS	upozornit
Sověty	Sověty	k1gInPc4	Sověty
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
plánují	plánovat	k5eAaImIp3nP	plánovat
ponechat	ponechat	k5eAaPmF	ponechat
své	svůj	k3xOyFgFnPc4	svůj
vojenské	vojenský	k2eAgFnPc4d1	vojenská
jednotky	jednotka	k1gFnPc4	jednotka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
<g/>
Týden	týden	k1gInSc1	týden
po	po	k7c6	po
uveřejnění	uveřejnění	k1gNnSc6	uveřejnění
Dlouhého	Dlouhého	k2eAgInSc2d1	Dlouhého
telegramu	telegram	k1gInSc2	telegram
pronesl	pronést	k5eAaPmAgMnS	pronést
bývalý	bývalý	k2eAgMnSc1d1	bývalý
britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
Winston	Winston	k1gInSc1	Winston
Churchill	Churchill	k1gMnSc1	Churchill
ve	v	k7c6	v
Fultonu	Fulton	k1gInSc6	Fulton
v	v	k7c6	v
Missouri	Missouri	k1gFnSc6	Missouri
známý	známý	k2eAgInSc4d1	známý
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
použil	použít	k5eAaPmAgInS	použít
a	a	k8xC	a
do	do	k7c2	do
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
povědomí	povědomí	k1gNnSc2	povědomí
tak	tak	k6eAd1	tak
uvedl	uvést	k5eAaPmAgInS	uvést
výraz	výraz	k1gInSc1	výraz
železná	železný	k2eAgFnSc1d1	železná
opona	opona	k1gFnSc1	opona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
požadoval	požadovat	k5eAaImAgMnS	požadovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
USA	USA	kA	USA
proti	proti	k7c3	proti
Sovětům	Sověty	k1gInPc3	Sověty
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vinil	vinit	k5eAaImAgMnS	vinit
za	za	k7c4	za
vytvoření	vytvoření	k1gNnSc4	vytvoření
"	"	kIx"	"
<g/>
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
spuštěné	spuštěný	k2eAgFnPc4d1	spuštěná
napříč	napříč	k7c7	napříč
kontinentem	kontinent	k1gInSc7	kontinent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Od	od	k7c2	od
doktríny	doktrína	k1gFnSc2	doktrína
zadržování	zadržování	k1gNnSc2	zadržování
po	po	k7c4	po
Korejskou	korejský	k2eAgFnSc4d1	Korejská
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
53	[number]	k4	53
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kominforma	kominforma	k1gFnSc1	kominforma
a	a	k8xC	a
roztržka	roztržka	k1gFnSc1	roztržka
mezi	mezi	k7c7	mezi
Titem	Tit	k1gMnSc7	Tit
a	a	k8xC	a
Stalinem	Stalin	k1gMnSc7	Stalin
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1947	[number]	k4	1947
Sověti	Sovět	k1gMnPc1	Sovět
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Kominformu	kominforma	k1gFnSc4	kominforma
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
organizaci	organizace	k1gFnSc4	organizace
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
utužit	utužit	k5eAaPmF	utužit
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
sovětskými	sovětský	k2eAgInPc7d1	sovětský
satelitními	satelitní	k2eAgInPc7d1	satelitní
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
Kominforma	kominforma	k1gFnSc1	kominforma
kvůli	kvůli	k7c3	kvůli
roztržce	roztržka	k1gFnSc3	roztržka
mezi	mezi	k7c7	mezi
Titem	Tit	k1gMnSc7	Tit
a	a	k8xC	a
Stalinem	Stalin	k1gMnSc7	Stalin
nucena	nutit	k5eAaImNgFnS	nutit
vyloučit	vyloučit	k5eAaPmF	vyloučit
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
Jugoslávii	Jugoslávie	k1gFnSc3	Jugoslávie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sice	sice	k8xC	sice
zůstala	zůstat	k5eAaPmAgFnS	zůstat
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přidala	přidat	k5eAaPmAgFnS	přidat
se	se	k3xPyFc4	se
ke	k	k7c3	k
Hnutí	hnutí	k1gNnSc3	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
roztržky	roztržka	k1gFnSc2	roztržka
byla	být	k5eAaImAgFnS	být
Titova	Titův	k2eAgFnSc1d1	Titova
neochota	neochota	k1gFnSc1	neochota
plně	plně	k6eAd1	plně
se	se	k3xPyFc4	se
podřídit	podřídit	k5eAaPmF	podřídit
Stalinovi	Stalin	k1gMnSc3	Stalin
a	a	k8xC	a
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doktrína	doktrína	k1gFnSc1	doktrína
zadržování	zadržování	k1gNnSc1	zadržování
a	a	k8xC	a
Trumanova	Trumanův	k2eAgFnSc1d1	Trumanova
doktrína	doktrína	k1gFnSc1	doktrína
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
poradci	poradce	k1gMnPc1	poradce
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
Harryho	Harry	k1gMnSc4	Harry
S.	S.	kA	S.
Trumana	Truman	k1gMnSc4	Truman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
okamžitě	okamžitě	k6eAd1	okamžitě
podnikl	podniknout	k5eAaPmAgMnS	podniknout
kroky	krok	k1gInPc4	krok
pro	pro	k7c4	pro
potlačení	potlačení	k1gNnSc4	potlačení
sovětského	sovětský	k2eAgInSc2d1	sovětský
vlivu	vliv	k1gInSc2	vliv
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
snahy	snaha	k1gFnPc4	snaha
Stalina	Stalin	k1gMnSc2	Stalin
podkopat	podkopat	k5eAaPmF	podkopat
moc	moc	k1gFnSc4	moc
USA	USA	kA	USA
podporováním	podporování	k1gNnSc7	podporování
soupeření	soupeření	k1gNnSc2	soupeření
mezi	mezi	k7c7	mezi
kapitalistickými	kapitalistický	k2eAgInPc7d1	kapitalistický
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
by	by	kYmCp3nS	by
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
mohly	moct	k5eAaImAgFnP	moct
začít	začít	k5eAaPmF	začít
válčit	válčit	k5eAaImF	válčit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1947	[number]	k4	1947
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
si	se	k3xPyFc3	se
nemůže	moct	k5eNaImIp3nS	moct
dovolit	dovolit	k5eAaPmF	dovolit
financovat	financovat	k5eAaBmF	financovat
řeckou	řecký	k2eAgFnSc4d1	řecká
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
komunistickým	komunistický	k2eAgMnPc3d1	komunistický
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
oznámení	oznámení	k1gNnSc4	oznámení
odpověděly	odpovědět	k5eAaPmAgFnP	odpovědět
doktrínou	doktrína	k1gFnSc7	doktrína
zadržování	zadržování	k1gNnSc2	zadržování
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zastavit	zastavit	k5eAaPmF	zastavit
rozmáhání	rozmáhání	k1gNnSc1	rozmáhání
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Truman	Truman	k1gMnSc1	Truman
přednesl	přednést	k5eAaPmAgMnS	přednést
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
slíbil	slíbit	k5eAaPmAgInS	slíbit
poskytnout	poskytnout	k5eAaPmF	poskytnout
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Trumanovu	Trumanův	k2eAgFnSc4d1	Trumanova
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
konflikt	konflikt	k1gInSc4	konflikt
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
boj	boj	k1gInSc4	boj
mezi	mezi	k7c7	mezi
svobodnými	svobodný	k2eAgMnPc7d1	svobodný
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
totalitními	totalitní	k2eAgInPc7d1	totalitní
režimy	režim	k1gInPc7	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Marshallův	Marshallův	k2eAgInSc1d1	Marshallův
plán	plán	k1gInSc1	plán
a	a	k8xC	a
Únor	únor	k1gInSc1	únor
1948	[number]	k4	1948
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusily	pokusit	k5eAaPmAgInP	pokusit
o	o	k7c4	o
dohodu	dohoda	k1gFnSc4	dohoda
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
ekonomicky	ekonomicky	k6eAd1	ekonomicky
samostatné	samostatný	k2eAgNnSc4d1	samostatné
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1947	[number]	k4	1947
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
zahájily	zahájit	k5eAaPmAgInP	zahájit
Marshallův	Marshallův	k2eAgInSc4d1	Marshallův
plán	plán	k1gInSc4	plán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
pomoc	pomoc	k1gFnSc4	pomoc
poválečné	poválečný	k2eAgFnSc3d1	poválečná
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
<g/>
Záměry	záměr	k1gInPc1	záměr
plánu	plán	k1gInSc2	plán
byly	být	k5eAaImAgInP	být
přestavba	přestavba	k1gFnSc1	přestavba
demokratických	demokratický	k2eAgInPc2d1	demokratický
a	a	k8xC	a
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
se	se	k3xPyFc4	se
hrozbě	hrozba	k1gFnSc3	hrozba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
představovalo	představovat	k5eAaImAgNnS	představovat
šíření	šíření	k1gNnSc1	šíření
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
Truman	Truman	k1gMnSc1	Truman
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
národní	národní	k2eAgFnSc6d1	národní
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
sjednocené	sjednocený	k2eAgNnSc4d1	sjednocené
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
Ústřední	ústřední	k2eAgFnSc4d1	ústřední
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
službu	služba	k1gFnSc4	služba
(	(	kIx(	(
<g/>
CIA	CIA	kA	CIA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Národní	národní	k2eAgFnSc4d1	národní
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
<g/>
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
integrace	integrace	k1gFnSc1	integrace
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
státům	stát	k1gInPc3	stát
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
vymanit	vymanit	k5eAaPmF	vymanit
se	se	k3xPyFc4	se
ze	z	k7c2	z
sovětské	sovětský	k2eAgFnSc2d1	sovětská
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
že	že	k8xS	že
právě	právě	k9	právě
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
USA	USA	kA	USA
snaží	snažit	k5eAaImIp3nP	snažit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
těmto	tento	k3xDgInPc3	tento
státům	stát	k1gInPc3	stát
zakázal	zakázat	k5eAaPmAgMnS	zakázat
Marshallův	Marshallův	k2eAgInSc4d1	Marshallův
plán	plán	k1gInSc4	plán
přijmout	přijmout	k5eAaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Sovětskou	sovětský	k2eAgFnSc7d1	sovětská
alternativou	alternativa	k1gFnSc7	alternativa
pro	pro	k7c4	pro
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Molotovův	Molotovův	k2eAgInSc1d1	Molotovův
plán	plán	k1gInSc1	plán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
přetvořen	přetvořen	k2eAgInSc1d1	přetvořen
v	v	k7c6	v
Radu	rad	k1gInSc6	rad
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
komunisté	komunista	k1gMnPc1	komunista
převzali	převzít	k5eAaPmAgMnP	převzít
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jediná	jediný	k2eAgFnSc1d1	jediná
země	země	k1gFnSc1	země
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Sověti	Sovět	k1gMnPc1	Sovět
povolili	povolit	k5eAaPmAgMnP	povolit
ponechat	ponechat	k5eAaPmF	ponechat
si	se	k3xPyFc3	se
demokratický	demokratický	k2eAgInSc4d1	demokratický
systém	systém	k1gInSc4	systém
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
odstranil	odstranit	k5eAaPmAgInS	odstranit
poslední	poslední	k2eAgFnSc4d1	poslední
známky	známka	k1gFnPc4	známka
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
s	s	k7c7	s
Marshallovým	Marshallův	k2eAgInSc7d1	Marshallův
plánem	plán	k1gInSc7	plán
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Kongresu	kongres	k1gInSc6	kongres
<g/>
.	.	kIx.	.
<g/>
Trumanova	Trumanův	k2eAgFnSc1d1	Trumanova
doktrína	doktrína	k1gFnSc1	doktrína
a	a	k8xC	a
Marshallův	Marshallův	k2eAgInSc1d1	Marshallův
plán	plán	k1gInSc1	plán
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
miliardám	miliarda	k4xCgFnPc3	miliarda
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
pro	pro	k7c4	pro
státy	stát	k1gInPc4	stát
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
řecká	řecký	k2eAgFnSc1d1	řecká
armáda	armáda	k1gFnSc1	armáda
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
italští	italský	k2eAgMnPc1d1	italský
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
demokraté	demokrat	k1gMnPc1	demokrat
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
nad	nad	k7c7	nad
komunisticko-socialistickou	komunistickoocialistický	k2eAgFnSc7d1	komunisticko-socialistická
aliancí	aliance	k1gFnSc7	aliance
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
blokáda	blokáda	k1gFnSc1	blokáda
a	a	k8xC	a
letecký	letecký	k2eAgInSc1d1	letecký
most	most	k1gInSc1	most
===	===	k?	===
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Británie	Británie	k1gFnPc1	Británie
sloučily	sloučit	k5eAaPmAgFnP	sloučit
západoněmecké	západoněmecký	k2eAgFnPc1d1	západoněmecká
okupační	okupační	k2eAgFnPc1d1	okupační
zóny	zóna	k1gFnPc1	zóna
do	do	k7c2	do
bizónie	bizónie	k1gFnSc2	bizónie
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1947	[number]	k4	1947
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
trizónie	trizónie	k1gFnSc1	trizónie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
přestavby	přestavba	k1gFnPc1	přestavba
Německa	Německo	k1gNnSc2	Německo
zavedly	zavést	k5eAaPmAgFnP	zavést
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
západní	západní	k2eAgFnSc2d1	západní
mocnosti	mocnost	k1gFnSc2	mocnost
novou	nový	k2eAgFnSc4d1	nová
měnu	měna	k1gFnSc4	měna
–	–	k?	–
německou	německý	k2eAgFnSc4d1	německá
marku	marka	k1gFnSc4	marka
–	–	k?	–
a	a	k8xC	a
nahradili	nahradit	k5eAaPmAgMnP	nahradit
tak	tak	k6eAd1	tak
starší	starý	k2eAgFnSc4d2	starší
říšskou	říšský	k2eAgFnSc4d1	říšská
marku	marka	k1gFnSc4	marka
<g/>
.	.	kIx.	.
<g/>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
Stalin	Stalin	k1gMnSc1	Stalin
zahájil	zahájit	k5eAaPmAgMnS	zahájit
berlínskou	berlínský	k2eAgFnSc4d1	Berlínská
blokádu	blokáda	k1gFnSc4	blokáda
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
velkých	velký	k2eAgFnPc2d1	velká
krizí	krize	k1gFnPc2	krize
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
zabraňovala	zabraňovat	k5eAaImAgFnS	zabraňovat
dodávkám	dodávka	k1gFnPc3	dodávka
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
zásob	zásoba	k1gFnPc2	zásoba
do	do	k7c2	do
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
pozemní	pozemní	k2eAgFnSc7d1	pozemní
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
sovětské	sovětský	k2eAgFnPc4d1	sovětská
okupační	okupační	k2eAgFnPc4d1	okupační
zóny	zóna	k1gFnPc4	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
letecký	letecký	k2eAgInSc4d1	letecký
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
benzín	benzín	k1gInSc1	benzín
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
materiál	materiál	k1gInSc1	materiál
Západnímu	západní	k2eAgInSc3d1	západní
Berlínu	Berlín	k1gInSc3	Berlín
poskytovat	poskytovat	k5eAaImF	poskytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1949	[number]	k4	1949
Stalin	Stalin	k1gMnSc1	Stalin
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
a	a	k8xC	a
blokádu	blokáda	k1gFnSc4	blokáda
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
letecké	letecký	k2eAgFnPc1d1	letecká
dodávky	dodávka	k1gFnPc1	dodávka
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zhruba	zhruba	k6eAd1	zhruba
278	[number]	k4	278
000	[number]	k4	000
letů	let	k1gInPc2	let
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
dopraveno	dopraven	k2eAgNnSc1d1	dopraveno
přes	přes	k7c4	přes
2,3	[number]	k4	2,3
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
NATO	nato	k6eAd1	nato
a	a	k8xC	a
rádio	rádio	k1gNnSc4	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1949	[number]	k4	1949
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
a	a	k8xC	a
osm	osm	k4xCc1	osm
dalších	další	k2eAgInPc2d1	další
západoevropských	západoevropský	k2eAgInPc2d1	západoevropský
států	stát	k1gInPc2	stát
podepsaly	podepsat	k5eAaPmAgInP	podepsat
Severoatlantickou	severoatlantický	k2eAgFnSc4d1	Severoatlantická
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
založily	založit	k5eAaPmAgFnP	založit
Severoatlantickou	severoatlantický	k2eAgFnSc4d1	Severoatlantická
alianci	aliance	k1gFnSc4	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Semeji	Semej	k1gInSc6	Semej
v	v	k7c6	v
Kazašské	kazašský	k2eAgFnSc6d1	kazašská
SSR	SSR	kA	SSR
otestována	otestovat	k5eAaPmNgFnS	otestovat
první	první	k4xOgFnSc1	první
sovětská	sovětský	k2eAgFnSc1d1	sovětská
jaderná	jaderný	k2eAgFnSc1d1	jaderná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
obnově	obnova	k1gFnSc6	obnova
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
zřídily	zřídit	k5eAaPmAgFnP	zřídit
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1949	[number]	k4	1949
spojením	spojení	k1gNnSc7	spojení
svých	svůj	k3xOyFgFnPc2	svůj
tří	tři	k4xCgFnPc2	tři
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
zóny	zóna	k1gFnSc2	zóna
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Německou	německý	k2eAgFnSc4d1	německá
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
republiku	republika	k1gFnSc4	republika
(	(	kIx(	(
<g/>
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Média	médium	k1gNnPc4	médium
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
sloužila	sloužit	k5eAaImAgFnS	sloužit
ku	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Rádia	rádio	k1gNnPc1	rádio
a	a	k8xC	a
televizní	televizní	k2eAgFnPc1d1	televizní
společnosti	společnost	k1gFnPc1	společnost
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
států	stát	k1gInPc2	stát
a	a	k8xC	a
tištěná	tištěný	k2eAgNnPc4d1	tištěné
média	médium	k1gNnPc4	médium
většinou	většinou	k6eAd1	většinou
vlastnily	vlastnit	k5eAaImAgFnP	vlastnit
komunistické	komunistický	k2eAgFnPc1d1	komunistická
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
propaganda	propaganda	k1gFnSc1	propaganda
fungovala	fungovat	k5eAaImAgFnS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
napadání	napadání	k1gNnSc2	napadání
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
marxistickou	marxistický	k2eAgFnSc7d1	marxistická
filosofií	filosofie	k1gFnSc7	filosofie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
BBC	BBC	kA	BBC
a	a	k8xC	a
Hlasu	hlas	k1gInSc2	hlas
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
rozhlasovým	rozhlasový	k2eAgFnPc3d1	rozhlasová
stanicím	stanice	k1gFnPc3	stanice
vysílajícím	vysílající	k2eAgNnSc7d1	vysílající
pro	pro	k7c4	pro
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
přidala	přidat	k5eAaPmAgFnS	přidat
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
šířit	šířit	k5eAaImF	šířit
objektivní	objektivní	k2eAgFnPc4d1	objektivní
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
sloužit	sloužit	k5eAaImF	sloužit
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
alternativa	alternativa	k1gFnSc1	alternativa
ke	k	k7c3	k
kontrolovaným	kontrolovaný	k2eAgNnPc3d1	kontrolované
domácím	domácí	k2eAgNnPc3d1	domácí
médiím	médium	k1gNnPc3	médium
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
snažily	snažit	k5eAaImAgInP	snažit
o	o	k7c6	o
vyzbrojení	vyzbrojení	k1gNnSc6	vyzbrojení
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
stalo	stát	k5eAaPmAgNnS	stát
členem	člen	k1gInSc7	člen
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čínská	čínský	k2eAgFnSc1d1	čínská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
SEATO	SEATO	kA	SEATO
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
Lidová	lidový	k2eAgFnSc1d1	lidová
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
armáda	armáda	k1gFnSc1	armáda
Mao	Mao	k1gFnSc1	Mao
Ce-tunga	Ceunga	k1gFnSc1	Ce-tunga
porazila	porazit	k5eAaPmAgFnS	porazit
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc1	stát
podporovanou	podporovaný	k2eAgFnSc4d1	podporovaná
Čankajškovu	Čankajškův	k2eAgFnSc4d1	Čankajškova
vládu	vláda	k1gFnSc4	vláda
Kuomintangu	Kuomintang	k1gInSc2	Kuomintang
(	(	kIx(	(
<g/>
KMT	KMT	kA	KMT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
poté	poté	k6eAd1	poté
rychle	rychle	k6eAd1	rychle
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
nově	nově	k6eAd1	nově
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Čankajšek	Čankajšek	k1gInSc1	Čankajšek
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
Kuomintang	Kuomintang	k1gInSc1	Kuomintang
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
na	na	k7c4	na
Tchaj-wan	Tchajan	k1gInSc4	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Trumanova	Trumanův	k2eAgFnSc1d1	Trumanova
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
incidentu	incident	k1gInSc6	incident
snažila	snažit	k5eAaImAgFnS	snažit
rychle	rychle	k6eAd1	rychle
rozšířit	rozšířit	k5eAaPmF	rozšířit
doktrínu	doktrína	k1gFnSc4	doktrína
zadržování	zadržování	k1gNnSc2	zadržování
<g/>
.	.	kIx.	.
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
tak	tak	k9	tak
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
doktrínu	doktrína	k1gFnSc4	doktrína
zadržování	zadržování	k1gNnSc2	zadržování
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zabránily	zabránit	k5eAaPmAgFnP	zabránit
revolučním	revoluční	k2eAgNnSc7d1	revoluční
vlasteneckým	vlastenecký	k2eAgNnSc7d1	vlastenecké
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
vedeny	vést	k5eAaImNgInP	vést
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
financovány	financován	k2eAgMnPc4d1	financován
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
uzavřely	uzavřít	k5eAaPmAgInP	uzavřít
USA	USA	kA	USA
spojenectví	spojenectví	k1gNnSc1	spojenectví
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
,	,	kIx,	,
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
,	,	kIx,	,
Novým	nový	k2eAgInSc7d1	nový
Zélandem	Zéland	k1gInSc7	Zéland
<g/>
,	,	kIx,	,
Thajskem	Thajsko	k1gNnSc7	Thajsko
a	a	k8xC	a
Filipínami	Filipíny	k1gFnPc7	Filipíny
(	(	kIx(	(
<g/>
nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
jsou	být	k5eAaImIp3nP	být
ANZUS	ANZUS	kA	ANZUS
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
a	a	k8xC	a
SEATO	SEATO	kA	SEATO
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Korejská	korejský	k2eAgFnSc1d1	Korejská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgInPc2d1	významný
důsledků	důsledek	k1gInPc2	důsledek
doktríny	doktrína	k1gFnSc2	doktrína
zadržování	zadržování	k1gNnSc1	zadržování
byl	být	k5eAaImAgInS	být
začátek	začátek	k1gInSc4	začátek
Korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1950	[number]	k4	1950
Kim	Kim	k1gFnSc4	Kim
Ir-senova	Irenův	k2eAgFnSc1d1	Ir-senův
Korejská	korejský	k2eAgFnSc1d1	Korejská
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Stalinově	Stalinův	k2eAgFnSc3d1	Stalinova
překvapení	překvapení	k1gNnSc1	překvapení
podpořila	podpořit	k5eAaPmAgFnS	podpořit
obranu	obrana	k1gFnSc4	obrana
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Sověti	Sovět	k1gMnPc1	Sovět
bojkotovali	bojkotovat	k5eAaImAgMnP	bojkotovat
její	její	k3xOp3gNnSc4	její
setkání	setkání	k1gNnSc4	setkání
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
stálým	stálý	k2eAgMnSc7d1	stálý
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
byl	být	k5eAaImAgMnS	být
Tchaj-wan	Tchajan	k1gInSc4	Tchaj-wan
a	a	k8xC	a
ne	ne	k9	ne
komunistická	komunistický	k2eAgFnSc1d1	komunistická
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
invaze	invaze	k1gFnSc2	invaze
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
vojenské	vojenský	k2eAgFnPc1d1	vojenská
síly	síla	k1gFnPc1	síla
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
státech	stát	k1gInPc6	stát
účastnících	účastnící	k2eAgInPc6d1	účastnící
se	se	k3xPyFc4	se
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
rozdělené	rozdělená	k1gFnSc2	rozdělená
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
válku	válka	k1gFnSc4	válka
podporoval	podporovat	k5eAaImAgMnS	podporovat
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
odsuzoval	odsuzovat	k5eAaImAgMnS	odsuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
se	se	k3xPyFc4	se
báli	bát	k5eAaImAgMnP	bát
vystupňování	vystupňování	k1gNnSc4	vystupňování
konfliktu	konflikt	k1gInSc2	konflikt
ve	v	k7c4	v
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
v	v	k7c4	v
jadernou	jaderný	k2eAgFnSc4d1	jaderná
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
silnému	silný	k2eAgInSc3d1	silný
odporu	odpor	k1gInSc3	odpor
vůči	vůči	k7c3	vůči
válce	válka	k1gFnSc3	válka
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
snažili	snažit	k5eAaImAgMnP	snažit
konflikt	konflikt	k1gInSc4	konflikt
ukončit	ukončit	k5eAaPmF	ukončit
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
<g/>
.	.	kIx.	.
<g/>
I	i	k8xC	i
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
Číňané	Číňan	k1gMnPc1	Číňan
a	a	k8xC	a
Severokorejci	Severokorejec	k1gMnPc1	Severokorejec
válkou	válka	k1gFnSc7	válka
vysíleni	vysílit	k5eAaPmNgMnP	vysílit
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
ji	on	k3xPp3gFnSc4	on
ukončit	ukončit	k5eAaPmF	ukončit
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Stalin	Stalin	k1gMnSc1	Stalin
požadoval	požadovat	k5eAaImAgMnS	požadovat
pokračování	pokračování	k1gNnSc4	pokračování
bojů	boj	k1gInPc2	boj
a	a	k8xC	a
příměří	příměří	k1gNnSc2	příměří
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
až	až	k9	až
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1953	[number]	k4	1953
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Severokorejský	severokorejský	k2eAgMnSc1d1	severokorejský
vůdce	vůdce	k1gMnSc1	vůdce
Kim	Kim	k1gMnSc1	Kim
Ir-sen	Iren	k1gInSc4	Ir-sen
nastolil	nastolit	k5eAaPmAgMnS	nastolit
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
diktaturu	diktatura	k1gFnSc4	diktatura
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
neomezenou	omezený	k2eNgFnSc4d1	neomezená
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
okolo	okolo	k6eAd1	okolo
sebe	sebe	k3xPyFc4	sebe
silný	silný	k2eAgInSc1d1	silný
kult	kult	k1gInSc1	kult
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jihu	jih	k1gInSc6	jih
zkorumpovaný	zkorumpovaný	k2eAgMnSc1d1	zkorumpovaný
prezident	prezident	k1gMnSc1	prezident
I	i	k8xC	i
Sung-man	Sungan	k1gMnSc1	Sung-man
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
podobný	podobný	k2eAgInSc4d1	podobný
autoritativní	autoritativní	k2eAgInSc4d1	autoritativní
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
nucen	nutit	k5eAaImNgMnS	nutit
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
autoritativními	autoritativní	k2eAgMnPc7d1	autoritativní
vládci	vládce	k1gMnPc7	vládce
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
znovunastolena	znovunastolen	k2eAgFnSc1d1	znovunastolen
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Krize	krize	k1gFnPc4	krize
a	a	k8xC	a
vystupňování	vystupňování	k1gNnSc4	vystupňování
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
–	–	k?	–
<g/>
62	[number]	k4	62
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Chruščov	Chruščov	k1gInSc1	Chruščov
<g/>
,	,	kIx,	,
Eisenhower	Eisenhower	k1gInSc1	Eisenhower
a	a	k8xC	a
destalinizace	destalinizace	k1gFnSc1	destalinizace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
změnili	změnit	k5eAaPmAgMnP	změnit
političtí	politický	k2eAgMnPc1d1	politický
vůdci	vůdce	k1gMnPc1	vůdce
v	v	k7c6	v
USA	USA	kA	USA
i	i	k8xC	i
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Dwight	Dwight	k1gInSc1	Dwight
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gInSc4	Eisenhower
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgInPc2d1	poslední
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
Trumanovy	Trumanův	k2eAgFnSc2d1	Trumanova
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
americký	americký	k2eAgInSc1d1	americký
rozpočet	rozpočet	k1gInSc1	rozpočet
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
zečtyřnásobil	zečtyřnásobit	k5eAaPmAgMnS	zečtyřnásobit
a	a	k8xC	a
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
výdaje	výdaj	k1gInSc2	výdaj
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Stalina	Stalin	k1gMnSc2	Stalin
se	se	k3xPyFc4	se
moci	moct	k5eAaImF	moct
nad	nad	k7c7	nad
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
nechal	nechat	k5eAaPmAgMnS	nechat
popravit	popravit	k5eAaPmF	popravit
Lavrentije	Lavrentije	k1gFnSc4	Lavrentije
Pavloviče	Pavlovič	k1gMnSc2	Pavlovič
Beriju	Beriju	k1gMnSc2	Beriju
a	a	k8xC	a
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
odsunul	odsunout	k5eAaPmAgInS	odsunout
Georgije	Georgije	k1gFnSc4	Georgije
Malenkova	Malenkův	k2eAgMnSc2d1	Malenkův
a	a	k8xC	a
Vjačeslava	Vjačeslav	k1gMnSc2	Vjačeslav
Molotova	Molotův	k2eAgMnSc2d1	Molotův
<g/>
,	,	kIx,	,
ujal	ujmout	k5eAaPmAgMnS	ujmout
Nikita	Nikita	k1gMnSc1	Nikita
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Chruščov	Chruščovo	k1gNnPc2	Chruščovo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1956	[number]	k4	1956
na	na	k7c6	na
XX	XX	kA	XX
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
KSSS	KSSS	kA	KSSS
pronesl	pronést	k5eAaPmAgInS	pronést
projev	projev	k1gInSc1	projev
Kult	kult	k1gInSc4	kult
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
důsledky	důsledek	k1gInPc4	důsledek
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
odhalil	odhalit	k5eAaPmAgMnS	odhalit
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
Stalinovy	Stalinův	k2eAgInPc4d1	Stalinův
zločiny	zločin	k1gInPc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
proces	proces	k1gInSc1	proces
destalinizace	destalinizace	k1gFnSc2	destalinizace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
omezit	omezit	k5eAaPmF	omezit
negativní	negativní	k2eAgInPc4d1	negativní
důsledky	důsledek	k1gInPc4	důsledek
Stalinovy	Stalinův	k2eAgFnSc2d1	Stalinova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
NDR	NDR	kA	NDR
nejistota	nejistota	k1gFnSc1	nejistota
po	po	k7c6	po
Stalinově	Stalinův	k2eAgFnSc6d1	Stalinova
smrti	smrt	k1gFnSc6	smrt
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
povstání	povstání	k1gNnSc6	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1956	[number]	k4	1956
při	při	k7c6	při
přijímání	přijímání	k1gNnSc6	přijímání
velvyslanců	velvyslanec	k1gMnPc2	velvyslanec
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
ambasádě	ambasáda	k1gFnSc6	ambasáda
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
Chruščov	Chruščov	k1gInSc1	Chruščov
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ať	ať	k8xC	ať
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
to	ten	k3xDgNnSc1	ten
líbí	líbit	k5eAaImIp3nS	líbit
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc1	dějiny
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbíme	pohřbít	k5eAaPmIp1nP	pohřbít
vás	vy	k3xPp2nPc4	vy
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Později	pozdě	k6eAd2	pozdě
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemluvil	mluvit	k5eNaImAgMnS	mluvit
o	o	k7c6	o
jaderné	jaderný	k2eAgFnSc6d1	jaderná
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
o	o	k7c6	o
historicky	historicky	k6eAd1	historicky
předurčeném	předurčený	k2eAgNnSc6d1	předurčené
vítězství	vítězství	k1gNnSc6	vítězství
komunismu	komunismus	k1gInSc2	komunismus
nad	nad	k7c7	nad
kapitalismem	kapitalismus	k1gInSc7	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
SSSR	SSSR	kA	SSSR
pozadu	pozadu	k6eAd1	pozadu
za	za	k7c7	za
Západem	západ	k1gInSc7	západ
<g/>
,	,	kIx,	,
během	během	k7c2	během
jednoho	jeden	k4xCgNnSc2	jeden
desetiletí	desetiletí	k1gNnSc2	desetiletí
se	se	k3xPyFc4	se
rozdíly	rozdíl	k1gInPc1	rozdíl
smažou	smazat	k5eAaPmIp3nP	smazat
a	a	k8xC	a
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
desetiletí	desetiletí	k1gNnPc2	desetiletí
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
kapitalistické	kapitalistický	k2eAgFnPc4d1	kapitalistická
země	zem	k1gFnPc4	zem
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
<g/>
Eisenhowerův	Eisenhowerův	k2eAgMnSc1d1	Eisenhowerův
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
John	John	k1gMnSc1	John
Foster	Foster	k1gMnSc1	Foster
Dulles	Dulles	k1gMnSc1	Dulles
zahájil	zahájit	k5eAaPmAgMnS	zahájit
novou	nový	k2eAgFnSc4d1	nová
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
začaly	začít	k5eAaPmAgInP	začít
více	hodně	k6eAd2	hodně
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
hrozbu	hrozba	k1gFnSc4	hrozba
jiným	jiný	k2eAgInPc3d1	jiný
státům	stát	k1gInPc3	stát
jadernými	jaderný	k2eAgFnPc7d1	jaderná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Varšavská	varšavský	k2eAgFnSc1d1	Varšavská
smlouva	smlouva	k1gFnSc1	smlouva
a	a	k8xC	a
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
povstání	povstání	k1gNnSc1	povstání
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
mírně	mírně	k6eAd1	mírně
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zůstala	zůstat	k5eAaPmAgFnS	zůstat
komplikovaná	komplikovaný	k2eAgFnSc1d1	komplikovaná
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
zavedli	zavést	k5eAaPmAgMnP	zavést
systém	systém	k1gInSc4	systém
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
pomoci	pomoc	k1gFnSc2	pomoc
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
formální	formální	k2eAgInSc4d1	formální
pakt	pakt	k1gInSc4	pakt
<g/>
,	,	kIx,	,
Varšavskou	varšavský	k2eAgFnSc4d1	Varšavská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Chruščov	Chruščov	k1gInSc1	Chruščov
sesadil	sesadit	k5eAaPmAgInS	sesadit
maďarského	maďarský	k2eAgMnSc4d1	maďarský
stalinistického	stalinistický	k2eAgMnSc4d1	stalinistický
politika	politik	k1gMnSc4	politik
Mátyáse	Mátyás	k1gMnSc4	Mátyás
Rákosiho	Rákosi	k1gMnSc4	Rákosi
<g/>
,	,	kIx,	,
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
povstání	povstání	k1gNnSc1	povstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
celonárodní	celonárodní	k2eAgNnSc4d1	celonárodní
povstání	povstání	k1gNnSc4	povstání
nový	nový	k2eAgInSc1d1	nový
režim	režim	k1gInSc1	režim
rozpustil	rozpustit	k5eAaPmAgInS	rozpustit
tajnou	tajný	k2eAgFnSc4d1	tajná
policii	policie	k1gFnSc4	policie
<g/>
,	,	kIx,	,
oznámil	oznámit	k5eAaPmAgMnS	oznámit
úmysl	úmysl	k1gInSc4	úmysl
opustit	opustit	k5eAaPmF	opustit
Varšavskou	varšavský	k2eAgFnSc4d1	Varšavská
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgInS	slíbit
návrat	návrat	k1gInSc1	návrat
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
události	událost	k1gFnPc4	událost
odpověděli	odpovědět	k5eAaPmAgMnP	odpovědět
invazí	invaze	k1gFnSc7	invaze
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
Maďarů	Maďar	k1gMnPc2	Maďar
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
a	a	k8xC	a
tisíce	tisíc	k4xCgInPc1	tisíc
dalších	další	k2eAgNnPc6d1	další
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
<g/>
,	,	kIx,	,
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
a	a	k8xC	a
deportováno	deportován	k2eAgNnSc1d1	deportováno
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
dalších	další	k2eAgMnPc2d1	další
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
uteklo	utéct	k5eAaPmAgNnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Maďarský	maďarský	k2eAgMnSc1d1	maďarský
vůdce	vůdce	k1gMnSc1	vůdce
Imre	Imr	k1gFnSc2	Imr
Nagy	Naga	k1gFnSc2	Naga
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
tajných	tajný	k2eAgInPc6d1	tajný
soudních	soudní	k2eAgInPc6d1	soudní
procesech	proces	k1gInPc6	proces
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1957	[number]	k4	1957
a	a	k8xC	a
1961	[number]	k4	1961
Chruščov	Chruščovo	k1gNnPc2	Chruščovo
otevřeně	otevřeně	k6eAd1	otevřeně
hrozil	hrozit	k5eAaImAgInS	hrozit
Západu	západ	k1gInSc2	západ
jadernou	jaderný	k2eAgFnSc7d1	jaderná
zkázou	zkáza	k1gFnSc7	zkáza
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sovětské	sovětský	k2eAgFnPc1d1	sovětská
střely	střela	k1gFnPc1	střela
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
lepší	dobrý	k2eAgFnPc1d2	lepší
než	než	k8xS	než
ty	ten	k3xDgFnPc1	ten
americké	americký	k2eAgFnPc1d1	americká
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
zničit	zničit	k5eAaPmF	zničit
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
americké	americký	k2eAgNnSc4d1	americké
nebo	nebo	k8xC	nebo
evropské	evropský	k2eAgNnSc4d1	Evropské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Chruščov	Chruščov	k1gInSc1	Chruščov
nesdílel	sdílet	k5eNaImAgInS	sdílet
Stalinův	Stalinův	k2eAgInSc4d1	Stalinův
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
nevyhnutelná	vyhnutelný	k2eNgFnSc1d1	nevyhnutelná
<g/>
,	,	kIx,	,
a	a	k8xC	a
spíše	spíše	k9	spíše
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
"	"	kIx"	"
<g/>
mírové	mírový	k2eAgNnSc4d1	Mírové
soužití	soužití	k1gNnSc4	soužití
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
třídní	třídní	k2eAgInSc1d1	třídní
boj	boj	k1gInSc1	boj
znamenal	znamenat	k5eAaImAgInS	znamenat
nevyhnutelné	vyhnutelný	k2eNgNnSc4d1	nevyhnutelné
střetnutí	střetnutí	k1gNnSc4	střetnutí
soupeřících	soupeřící	k2eAgFnPc2d1	soupeřící
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
komunismus	komunismus	k1gInSc1	komunismus
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
při	při	k7c6	při
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
válce	válka	k1gFnSc6	válka
<g/>
;	;	kIx,	;
Chruščov	Chruščov	k1gInSc1	Chruščov
chtěl	chtít	k5eAaImAgInS	chtít
nechat	nechat	k5eAaPmF	nechat
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zhroutil	zhroutit	k5eAaPmAgMnS	zhroutit
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
posiloval	posilovat	k5eAaImAgInS	posilovat
své	svůj	k3xOyFgFnPc4	svůj
vojenské	vojenský	k2eAgFnPc4d1	vojenská
kapacity	kapacita	k1gFnPc4	kapacita
<g/>
.	.	kIx.	.
<g/>
Tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
zásah	zásah	k1gInSc1	zásah
Sovětů	Sovět	k1gMnPc2	Sovět
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
překvapil	překvapit	k5eAaPmAgInS	překvapit
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
komunistických	komunistický	k2eAgFnPc6d1	komunistická
i	i	k8xC	i
kapitalistických	kapitalistický	k2eAgFnPc6d1	kapitalistická
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
velký	velký	k2eAgInSc4d1	velký
pokles	pokles	k1gInSc4	pokles
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávský	jugoslávský	k2eAgMnSc1d1	jugoslávský
politik	politik	k1gMnSc1	politik
Milovan	Milovan	k1gMnSc1	Milovan
Djilas	Djilas	k1gMnSc1	Djilas
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rána	Rána	k1gFnSc1	Rána
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zasadila	zasadit	k5eAaPmAgFnS	zasadit
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
revoluce	revoluce	k1gFnSc1	revoluce
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
vyléčena	vyléčit	k5eAaPmNgFnS	vyléčit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Evropská	evropský	k2eAgFnSc1d1	Evropská
integrace	integrace	k1gFnSc1	integrace
a	a	k8xC	a
celosvětové	celosvětový	k2eAgInPc1d1	celosvětový
nepokoje	nepokoj	k1gInPc1	nepokoj
===	===	k?	===
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
znaků	znak	k1gInPc2	znak
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
počátek	počátek	k1gInSc4	počátek
evropské	evropský	k2eAgFnSc2d1	Evropská
integrace	integrace	k1gFnSc2	integrace
–	–	k?	–
zásadního	zásadní	k2eAgInSc2d1	zásadní
vedlejšího	vedlejší	k2eAgInSc2d1	vedlejší
produktu	produkt	k1gInSc2	produkt
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Truman	Truman	k1gMnSc1	Truman
a	a	k8xC	a
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
podporovali	podporovat	k5eAaImAgMnP	podporovat
politicky	politicky	k6eAd1	politicky
<g/>
,	,	kIx,	,
ekonomicky	ekonomicky	k6eAd1	ekonomicky
i	i	k9	i
vojensky	vojensky	k6eAd1	vojensky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nacionalistická	nacionalistický	k2eAgNnPc1d1	nacionalistické
hnutí	hnutí	k1gNnPc1	hnutí
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
a	a	k8xC	a
regionech	region	k1gInPc6	region
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c6	v
Guatemale	Guatemala	k1gFnSc6	Guatemala
<g/>
,	,	kIx,	,
Indonésii	Indonésie	k1gFnSc6	Indonésie
a	a	k8xC	a
Indočíně	Indočína	k1gFnSc6	Indočína
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
komunistickými	komunistický	k2eAgFnPc7d1	komunistická
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tak	tak	k9	tak
alespoň	alespoň	k9	alespoň
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
byla	být	k5eAaImAgFnS	být
chápána	chápán	k2eAgFnSc1d1	chápána
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dekolonizace	dekolonizace	k1gFnSc2	dekolonizace
ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
soupeřily	soupeřit	k5eAaImAgFnP	soupeřit
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
využívaly	využívat	k5eAaImAgInP	využívat
Ústřední	ústřední	k2eAgFnSc4d1	ústřední
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
službu	služba	k1gFnSc4	služba
(	(	kIx(	(
<g/>
CIA	CIA	kA	CIA
<g/>
)	)	kIx)	)
ke	k	k7c3	k
zbavování	zbavování	k1gNnSc3	zbavování
se	se	k3xPyFc4	se
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
vlád	vláda	k1gFnPc2	vláda
z	z	k7c2	z
Třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
a	a	k8xC	a
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
spřátelených	spřátelený	k2eAgFnPc2d1	spřátelená
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
CIA	CIA	kA	CIA
podnítila	podnítit	k5eAaPmAgFnS	podnítit
Íránský	íránský	k2eAgInSc4d1	íránský
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
zbavit	zbavit	k5eAaPmF	zbavit
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
Muhammada	Muhammada	k1gFnSc1	Muhammada
Mosaddeka	Mosaddeka	k1gMnSc1	Mosaddeka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
znárodnil	znárodnit	k5eAaPmAgInS	znárodnit
britskou	britský	k2eAgFnSc4d1	britská
ropnou	ropný	k2eAgFnSc4d1	ropná
společnost	společnost	k1gFnSc4	společnost
Anglo-Persian	Anglo-Persiany	k1gInPc2	Anglo-Persiany
Oil	Oil	k1gMnSc2	Oil
Company	Compana	k1gFnSc2	Compana
a	a	k8xC	a
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
USA	USA	kA	USA
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
posouval	posouvat	k5eAaImAgInS	posouvat
Írán	Írán	k1gInSc1	Írán
do	do	k7c2	do
sovětské	sovětský	k2eAgFnSc2d1	sovětská
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Moci	moct	k5eAaImF	moct
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
autokratický	autokratický	k2eAgMnSc1d1	autokratický
panovník	panovník	k1gMnSc1	panovník
ujal	ujmout	k5eAaPmAgMnS	ujmout
prozápadní	prozápadní	k2eAgMnSc1d1	prozápadní
šáh	šáh	k1gMnSc1	šáh
Muhammad	Muhammad	k1gInSc4	Muhammad
Rezá	Rezá	k1gFnSc1	Rezá
Pahlaví	Pahlavý	k2eAgMnPc5d1	Pahlavý
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zakázal	zakázat	k5eAaPmAgMnS	zakázat
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Tudeh	Tudeha	k1gFnPc2	Tudeha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Guatemale	Guatemala	k1gFnSc6	Guatemala
napomohla	napomoct	k5eAaPmAgFnS	napomoct
CIA	CIA	kA	CIA
k	k	k7c3	k
převratu	převrat	k1gInSc3	převrat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
svrhl	svrhnout	k5eAaPmAgMnS	svrhnout
levicového	levicový	k2eAgNnSc2d1	levicové
<g/>
,	,	kIx,	,
demokraticky	demokraticky	k6eAd1	demokraticky
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
prezidenta	prezident	k1gMnSc2	prezident
Jacoba	Jacoba	k1gFnSc1	Jacoba
Árbenze	Árbenze	k1gFnSc1	Árbenze
Guzmána	Guzmán	k2eAgFnSc1d1	Guzmán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
vyvlastnit	vyvlastnit	k5eAaPmF	vyvlastnit
a	a	k8xC	a
rozdat	rozdat	k5eAaPmF	rozdat
chudým	chudý	k2eAgMnPc3d1	chudý
bezzemkům	bezzemek	k1gMnPc3	bezzemek
neobdělávanou	obdělávaný	k2eNgFnSc4d1	neobdělávaná
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
patřící	patřící	k2eAgFnSc2d1	patřící
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
United	United	k1gMnSc1	United
Fruit	Fruit	k1gMnSc1	Fruit
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
junta	junta	k1gFnSc1	junta
vedená	vedený	k2eAgFnSc1d1	vedená
Carlosem	Carlos	k1gMnSc7	Carlos
Castillem	Castill	k1gMnSc7	Castill
Armasem	Armas	k1gInSc7	Armas
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
převzala	převzít	k5eAaPmAgFnS	převzít
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
USA	USA	kA	USA
založila	založit	k5eAaPmAgFnS	založit
Národní	národní	k2eAgInSc4d1	národní
výbor	výbor	k1gInSc4	výbor
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Konžské	konžský	k2eAgFnSc6d1	Konžská
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1960	[number]	k4	1960
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
od	od	k7c2	od
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgMnS	nařídit
prezident	prezident	k1gMnSc1	prezident
Joseph	Joseph	k1gMnSc1	Joseph
Kasavubu	Kasavub	k1gInSc2	Kasavub
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
CIA	CIA	kA	CIA
odvolání	odvolání	k1gNnSc1	odvolání
demokraticky	demokraticky	k6eAd1	demokraticky
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
Patrice	patrice	k1gFnSc2	patrice
Lumumby	Lumumba	k1gFnSc2	Lumumba
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
;	;	kIx,	;
Lumumba	Lumumba	k1gMnSc1	Lumumba
chtěl	chtít	k5eAaImAgMnS	chtít
zase	zase	k9	zase
sesadit	sesadit	k5eAaPmF	sesadit
Kasavuba	Kasavub	k1gMnSc4	Kasavub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
Konžské	konžský	k2eAgFnSc6d1	Konžská
krizi	krize	k1gFnSc6	krize
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgInS	chopit
moci	moc	k1gFnSc3	moc
Mobutu	Mobut	k1gInSc2	Mobut
Sese	Sese	k?	Sese
Seko	seko	k1gNnSc1	seko
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
podporovaly	podporovat	k5eAaImAgFnP	podporovat
USA	USA	kA	USA
a	a	k8xC	a
který	který	k3yRgInSc1	který
rychle	rychle	k6eAd1	rychle
provedl	provést	k5eAaPmAgInS	provést
vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
<g/>
.	.	kIx.	.
<g/>
Francie	Francie	k1gFnSc1	Francie
po	po	k7c6	po
Indočínské	indočínský	k2eAgFnSc6d1	indočínská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
hnutím	hnutí	k1gNnSc7	hnutí
Viet	Vietum	k1gNnPc2	Vietum
Minhem	Minh	k1gInSc7	Minh
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Dien	dien	k1gInSc1	dien
Bien	biena	k1gFnPc2	biena
Phu	Phu	k1gFnSc2	Phu
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
opustí	opustit	k5eAaPmIp3nS	opustit
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ženevské	ženevský	k2eAgFnSc6d1	Ženevská
konferenci	konference	k1gFnSc6	konference
konané	konaný	k2eAgMnPc4d1	konaný
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Vietnamu	Vietnam	k1gInSc2	Vietnam
na	na	k7c4	na
prosovětský	prosovětský	k2eAgInSc4d1	prosovětský
Severní	severní	k2eAgInSc4d1	severní
Vietnam	Vietnam	k1gInSc4	Vietnam
a	a	k8xC	a
prozápadní	prozápadní	k2eAgInSc1d1	prozápadní
Jižní	jižní	k2eAgInSc1d1	jižní
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
1954	[number]	k4	1954
a	a	k8xC	a
1961	[number]	k4	1961
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
posílaly	posílat	k5eAaImAgInP	posílat
Jižnímu	jižní	k2eAgInSc3d1	jižní
Vietnamu	Vietnam	k1gInSc2	Vietnam
finanční	finanční	k2eAgFnSc4d1	finanční
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k6eAd1	mnoho
nově	nově	k6eAd1	nově
vznikajících	vznikající	k2eAgInPc2d1	vznikající
států	stát	k1gInPc2	stát
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Latinské	latinský	k2eAgFnSc3d1	Latinská
Americe	Amerika	k1gFnSc3	Amerika
odmítalo	odmítat	k5eAaImAgNnS	odmítat
přidat	přidat	k5eAaPmF	přidat
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Sovětů	Sovět	k1gMnPc2	Sovět
nebo	nebo	k8xC	nebo
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
na	na	k7c6	na
Bandungské	bandungský	k2eAgFnSc6d1	Bandungská
konferenci	konference	k1gFnSc6	konference
několik	několik	k4yIc4	několik
států	stát	k1gInPc2	stát
ze	z	k7c2	z
Třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
neúčastnit	účastnit	k5eNaImF	účastnit
se	se	k3xPyFc4	se
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Hnutí	hnutí	k1gNnSc4	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Roztržka	roztržka	k1gFnSc1	roztržka
mezi	mezi	k7c7	mezi
Čínou	Čína	k1gFnSc7	Čína
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
ICBM	ICBM	kA	ICBM
===	===	k?	===
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
nebylo	být	k5eNaImAgNnS	být
pro	pro	k7c4	pro
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
moc	moc	k6eAd1	moc
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozpadat	rozpadat	k5eAaPmF	rozpadat
jeho	jeho	k3xOp3gFnSc1	jeho
aliance	aliance	k1gFnSc1	aliance
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
Stalina	Stalin	k1gMnSc4	Stalin
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ho	on	k3xPp3gInSc4	on
Chruščov	Chruščov	k1gInSc4	Chruščov
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
napadl	napadnout	k5eAaPmAgMnS	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Nového	Nového	k2eAgMnSc4d1	Nového
sovětského	sovětský	k2eAgMnSc4d1	sovětský
vůdce	vůdce	k1gMnSc4	vůdce
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
povrchního	povrchní	k2eAgMnSc4d1	povrchní
šplhavce	šplhavec	k1gMnSc4	šplhavec
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Chruščov	Chruščov	k1gInSc1	Chruščov
snažil	snažit	k5eAaImAgInS	snažit
čínsko-sovětské	čínskoovětský	k2eAgNnSc4d1	čínsko-sovětský
spojenectví	spojenectví	k1gNnSc4	spojenectví
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Mao	Mao	k1gMnSc1	Mao
ho	on	k3xPp3gInSc4	on
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
bezvýznamné	bezvýznamný	k2eAgInPc4d1	bezvýznamný
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
návrhy	návrh	k1gInPc4	návrh
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
dvou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
v	v	k7c4	v
propagandistickou	propagandistický	k2eAgFnSc4d1	propagandistická
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
soupeřili	soupeřit	k5eAaImAgMnP	soupeřit
o	o	k7c4	o
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
komunistickém	komunistický	k2eAgNnSc6d1	komunistické
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
.	.	kIx.	.
<g/>
USA	USA	kA	USA
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgInP	snažit
o	o	k7c4	o
vývoj	vývoj	k1gInSc4	vývoj
zbraní	zbraň	k1gFnPc2	zbraň
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
doletu	dolet	k1gInSc2	dolet
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
by	by	kYmCp3nS	by
mohly	moct	k5eAaImAgFnP	moct
napadnout	napadnout	k5eAaPmF	napadnout
území	území	k1gNnSc3	území
druhého	druhý	k4xOgInSc2	druhý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1957	[number]	k4	1957
Sověti	Sovět	k1gMnPc1	Sovět
vypustili	vypustit	k5eAaPmAgMnP	vypustit
první	první	k4xOgFnSc4	první
mezikontinentální	mezikontinentální	k2eAgFnSc4d1	mezikontinentální
balistickou	balistický	k2eAgFnSc4d1	balistická
raketu	raketa	k1gFnSc4	raketa
(	(	kIx(	(
<g/>
ICBM	ICBM	kA	ICBM
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
vyslali	vyslat	k5eAaPmAgMnP	vyslat
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
první	první	k4xOgInSc4	první
satelit	satelit	k1gInSc4	satelit
<g/>
,	,	kIx,	,
Sputnik	sputnik	k1gInSc4	sputnik
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
začaly	začít	k5eAaPmAgFnP	začít
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
přistání	přistání	k1gNnSc4	přistání
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgNnSc6	který
astronaut	astronaut	k1gMnSc1	astronaut
Frank	Frank	k1gMnSc1	Frank
Borman	Borman	k1gMnSc1	Borman
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
bitvou	bitva	k1gFnSc7	bitva
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
zátoka	zátoka	k1gFnSc1	zátoka
Sviní	svinit	k5eAaImIp3nS	svinit
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kubě	Kuba	k1gFnSc6	Kuba
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1959	[number]	k4	1959
chopilo	chopit	k5eAaPmAgNnS	chopit
moci	moct	k5eAaImF	moct
Hnutí	hnutí	k1gNnSc4	hnutí
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
svrhlo	svrhnout	k5eAaPmAgNnS	svrhnout
prezidenta	prezident	k1gMnSc4	prezident
Fulgencia	Fulgencius	k1gMnSc4	Fulgencius
Batistu	batist	k1gInSc2	batist
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
neoblíbený	oblíbený	k2eNgInSc1d1	neoblíbený
režim	režim	k1gInSc1	režim
ztratil	ztratit	k5eAaPmAgInS	ztratit
přízeň	přízeň	k1gFnSc4	přízeň
Eisenhowerovy	Eisenhowerův	k2eAgFnSc2d1	Eisenhowerova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Batistově	batistově	k6eAd1	batistově
pádu	pád	k1gInSc2	pád
mezi	mezi	k7c7	mezi
Kubou	Kuba	k1gFnSc7	Kuba
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
fungovaly	fungovat	k5eAaImAgFnP	fungovat
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
prezident	prezident	k1gMnSc1	prezident
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
úmyslně	úmyslně	k6eAd1	úmyslně
opustil	opustit	k5eAaPmAgMnS	opustit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
mladým	mladý	k2eAgMnSc7d1	mladý
kubánským	kubánský	k2eAgMnSc7d1	kubánský
vůdcem	vůdce	k1gMnSc7	vůdce
Fidelem	Fidel	k1gMnSc7	Fidel
Castrem	Castr	k1gMnSc7	Castr
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
proto	proto	k8xC	proto
viceprezident	viceprezident	k1gMnSc1	viceprezident
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gMnSc1	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
Castro	Castro	k1gNnSc4	Castro
komunista	komunista	k1gMnSc1	komunista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyli	být	k5eNaImAgMnP	být
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
snahami	snaha	k1gFnPc7	snaha
o	o	k7c4	o
menší	malý	k2eAgFnSc4d2	menší
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
nově	nově	k6eAd1	nově
zvoleného	zvolený	k2eAgMnSc2d1	zvolený
prezidenta	prezident	k1gMnSc2	prezident
Johna	John	k1gMnSc2	John
Fitzgeralda	Fitzgerald	k1gMnSc2	Fitzgerald
Kennedyho	Kennedy	k1gMnSc2	Kennedy
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
invaze	invaze	k1gFnSc1	invaze
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svinit	k5eAaImIp3nS	svinit
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
Castro	Castro	k6eAd1	Castro
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
bál	bát	k5eAaImAgMnS	bát
akcí	akce	k1gFnSc7	akce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mohly	moct	k5eAaImAgInP	moct
provést	provést	k5eAaPmF	provést
příště	příště	k6eAd1	příště
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
krize	krize	k1gFnSc1	krize
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
===	===	k?	===
</s>
</p>
<p>
<s>
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
krize	krize	k1gFnSc1	krize
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
byla	být	k5eAaImAgFnS	být
další	další	k2eAgFnSc4d1	další
velkou	velká	k1gFnSc4	velká
událostí	událost	k1gFnPc2	událost
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
poválečného	poválečný	k2eAgNnSc2d1	poválečné
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
zakazoval	zakazovat	k5eAaImAgInS	zakazovat
emigraci	emigrace	k1gFnSc4	emigrace
ze	z	k7c2	z
států	stát	k1gInPc2	stát
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnoho	mnoho	k4c1	mnoho
východních	východní	k2eAgMnPc2d1	východní
Němců	Němec	k1gMnPc2	Němec
ročně	ročně	k6eAd1	ročně
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
do	do	k7c2	do
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
kvůli	kvůli	k7c3	kvůli
otevřeným	otevřený	k2eAgFnPc3d1	otevřená
hranicím	hranice	k1gFnPc3	hranice
mezi	mezi	k7c7	mezi
Východním	východní	k2eAgInSc7d1	východní
a	a	k8xC	a
Západním	západní	k2eAgInSc7d1	západní
Berlínem	Berlín	k1gInSc7	Berlín
<g/>
.	.	kIx.	.
<g/>
To	ten	k3xDgNnSc1	ten
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
obrovský	obrovský	k2eAgInSc4d1	obrovský
odliv	odliv	k1gInSc4	odliv
mozků	mozek	k1gInPc2	mozek
z	z	k7c2	z
Východního	východní	k2eAgInSc2d1	východní
do	do	k7c2	do
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
000	[number]	k4	000
000	[number]	k4	000
východních	východní	k2eAgMnPc2d1	východní
Němců	Němec	k1gMnPc2	Němec
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
do	do	k7c2	do
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Chruščov	Chruščov	k1gInSc1	Chruščov
nejdříve	dříve	k6eAd3	dříve
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
NDR	NDR	kA	NDR
časem	časem	k6eAd1	časem
převýší	převýšit	k5eAaPmIp3nS	převýšit
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
životní	životní	k2eAgFnSc6d1	životní
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vlnu	vlna	k1gFnSc4	vlna
emigrace	emigrace	k1gFnSc2	emigrace
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1961	[number]	k4	1961
tak	tak	k6eAd1	tak
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
vztyčilo	vztyčit	k5eAaPmAgNnS	vztyčit
ostnatý	ostnatý	k2eAgInSc4d1	ostnatý
drát	drát	k1gInSc4	drát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
a	a	k8xC	a
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
Berlínskou	berlínský	k2eAgFnSc4d1	Berlínská
zeď	zeď	k1gFnSc4	zeď
a	a	k8xC	a
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
tak	tak	k6eAd1	tak
únikovou	únikový	k2eAgFnSc4d1	úniková
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
krize	krize	k1gFnSc1	krize
a	a	k8xC	a
sesazení	sesazení	k1gNnSc1	sesazení
Chruščova	Chruščův	k2eAgInSc2d1	Chruščův
===	===	k?	===
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Sviní	svině	k1gFnPc2	svině
dále	daleko	k6eAd2	daleko
snažily	snažit	k5eAaImAgFnP	snažit
sesadit	sesadit	k5eAaPmF	sesadit
Castrovu	Castrův	k2eAgFnSc4d1	Castrova
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Naděje	naděje	k1gFnPc1	naděje
byly	být	k5eAaImAgFnP	být
vkládány	vkládat	k5eAaImNgFnP	vkládat
do	do	k7c2	do
tajného	tajný	k2eAgInSc2d1	tajný
programu	program	k1gInSc2	program
s	s	k7c7	s
názvem	název	k1gInSc7	název
Kubánský	kubánský	k2eAgInSc1d1	kubánský
projekt	projekt	k1gInSc1	projekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
probíhal	probíhat	k5eAaImAgInS	probíhat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
Chruščov	Chruščov	k1gInSc4	Chruščov
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1962	[number]	k4	1962
dověděl	dovědět	k5eAaPmAgMnS	dovědět
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
plánovat	plánovat	k5eAaImF	plánovat
umístění	umístění	k1gNnSc4	umístění
sovětských	sovětský	k2eAgFnPc2d1	sovětská
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
.	.	kIx.	.
<g/>
Kennedy	Kenned	k1gMnPc4	Kenned
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
plány	plán	k1gInPc4	plán
zahájil	zahájit	k5eAaPmAgMnS	zahájit
námořní	námořní	k2eAgFnSc4d1	námořní
blokádu	blokáda	k1gFnSc4	blokáda
Kuby	Kuba	k1gFnSc2	Kuba
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
ultimátum	ultimátum	k1gNnSc4	ultimátum
na	na	k7c6	na
odstranění	odstranění	k1gNnSc6	odstranění
raket	raketa	k1gFnPc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Chruščov	Chruščov	k1gInSc1	Chruščov
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
odstranil	odstranit	k5eAaPmAgInS	odstranit
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
o	o	k7c6	o
invazi	invaze	k1gFnSc6	invaze
Kuby	Kuba	k1gFnSc2	Kuba
už	už	k6eAd1	už
nepokusí	pokusit	k5eNaPmIp3nS	pokusit
<g/>
.	.	kIx.	.
<g/>
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
krize	krize	k1gFnSc1	krize
přivedla	přivést	k5eAaPmAgFnS	přivést
svět	svět	k1gInSc4	svět
blíže	blíž	k1gFnSc2	blíž
k	k	k7c3	k
jaderné	jaderný	k2eAgFnSc3d1	jaderná
válce	válka	k1gFnSc3	válka
než	než	k8xS	než
kdykoli	kdykoli	k6eAd1	kdykoli
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Mocnosti	mocnost	k1gFnPc1	mocnost
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
využívaly	využívat	k5eAaPmAgInP	využívat
doktrínu	doktrína	k1gFnSc4	doktrína
vzájemně	vzájemně	k6eAd1	vzájemně
zaručeného	zaručený	k2eAgNnSc2d1	zaručené
zničení	zničení	k1gNnSc2	zničení
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byl	být	k5eAaImAgInS	být
Chruščov	Chruščov	k1gInSc1	Chruščov
zástupci	zástupce	k1gMnPc7	zástupce
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
sesazen	sesazen	k2eAgMnSc1d1	sesazen
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
mu	on	k3xPp3gMnSc3	on
dopřán	dopřát	k5eAaPmNgInS	dopřát
pokojný	pokojný	k2eAgInSc1d1	pokojný
důchod	důchod	k1gInSc1	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
hrubosti	hrubost	k1gFnSc2	hrubost
<g/>
,	,	kIx,	,
neschopnosti	neschopnost	k1gFnSc2	neschopnost
<g/>
,	,	kIx,	,
zruinování	zruinování	k1gNnSc1	zruinování
sovětského	sovětský	k2eAgNnSc2d1	sovětské
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
dovedení	dovedení	k1gNnSc2	dovedení
světa	svět	k1gInSc2	svět
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
nukleární	nukleární	k2eAgFnSc2d1	nukleární
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konfrontace	konfrontace	k1gFnSc1	konfrontace
přes	přes	k7c4	přes
détente	détente	k2eAgInPc4d1	détente
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
79	[number]	k4	79
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
a	a	k8xC	a
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
účastníci	účastník	k1gMnPc1	účastník
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
snažili	snažit	k5eAaImAgMnP	snažit
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
nové	nový	k2eAgFnSc3d1	nová
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
svět	svět	k1gInSc1	svět
již	již	k6eAd1	již
nerozděloval	rozdělovat	k5eNaImAgInS	rozdělovat
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
jasně	jasně	k6eAd1	jasně
odlišné	odlišný	k2eAgInPc4d1	odlišný
tábory	tábor	k1gInPc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
Japonsko	Japonsko	k1gNnSc1	Japonsko
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
rychle	rychle	k6eAd1	rychle
zotavily	zotavit	k5eAaPmAgFnP	zotavit
a	a	k8xC	a
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
udržovaly	udržovat	k5eAaImAgInP	udržovat
silný	silný	k2eAgInSc4d1	silný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
a	a	k8xC	a
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
se	se	k3xPyFc4	se
přibližovaly	přibližovat	k5eAaImAgFnP	přibližovat
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
ekonomika	ekonomika	k1gFnSc1	ekonomika
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
stagnovala	stagnovat	k5eAaImAgFnS	stagnovat
<g/>
.	.	kIx.	.
<g/>
Kvůli	kvůli	k7c3	kvůli
ropné	ropný	k2eAgFnSc3d1	ropná
krizi	krize	k1gFnSc3	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
a	a	k8xC	a
vzrůstajícímu	vzrůstající	k2eAgInSc3d1	vzrůstající
vlivu	vliv	k1gInSc3	vliv
organizací	organizace	k1gFnPc2	organizace
jako	jako	k8xS	jako
OPEC	opéct	k5eAaPmRp2nSwK	opéct
a	a	k8xC	a
Hnutí	hnutí	k1gNnSc1	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
slabší	slabý	k2eAgInPc1d2	slabší
státy	stát	k1gInPc1	stát
častěji	často	k6eAd2	často
snažily	snažit	k5eAaImAgInP	snažit
odolávat	odolávat	k5eAaImF	odolávat
tlaku	tlak	k1gInSc2	tlak
obou	dva	k4xCgFnPc2	dva
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
snažil	snažit	k5eAaImAgMnS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
domácí	domácí	k2eAgFnSc4d1	domácí
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
krizi	krize	k1gFnSc4	krize
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
období	období	k1gNnSc6	období
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vůdci	vůdce	k1gMnPc1	vůdce
jako	jako	k8xS	jako
Leonid	Leonid	k1gInSc1	Leonid
Iljič	Iljič	k1gMnSc1	Iljič
Brežněv	Brežněv	k1gMnSc1	Brežněv
a	a	k8xC	a
Alexej	Alexej	k1gMnSc1	Alexej
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Kosygin	Kosygin	k1gMnSc1	Kosygin
začali	začít	k5eAaPmAgMnP	začít
využívat	využívat	k5eAaImF	využívat
politiku	politika	k1gFnSc4	politika
détente	détente	k2eAgFnSc4d1	détente
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výstup	výstup	k1gInSc1	výstup
Francie	Francie	k1gFnSc2	Francie
z	z	k7c2	z
vojenských	vojenský	k2eAgFnPc2d1	vojenská
struktur	struktura	k1gFnPc2	struktura
NATO	NATO	kA	NATO
===	===	k?	===
</s>
</p>
<p>
<s>
Deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
se	se	k3xPyFc4	se
NATO	nato	k6eAd1	nato
začalo	začít	k5eAaPmAgNnS	začít
potýkat	potýkat	k5eAaImF	potýkat
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
prezidentem	prezident	k1gMnSc7	prezident
Francie	Francie	k1gFnSc2	Francie
stal	stát	k5eAaPmAgMnS	stát
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaull	k1gMnSc4	Gaull
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
silnému	silný	k2eAgInSc3d1	silný
vlivu	vliv	k1gInSc3	vliv
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
na	na	k7c4	na
Alianci	aliance	k1gFnSc4	aliance
a	a	k8xC	a
blízkým	blízký	k2eAgInPc3d1	blízký
vztahům	vztah	k1gInPc3	vztah
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1958	[number]	k4	1958
poslal	poslat	k5eAaPmAgMnS	poslat
prezidentovi	prezident	k1gMnSc3	prezident
Dwightu	Dwight	k1gMnSc3	Dwight
D.	D.	kA	D.
Eisenhowerovi	Eisenhower	k1gMnSc3	Eisenhower
a	a	k8xC	a
britskému	britský	k2eAgMnSc3d1	britský
premiérovi	premiér	k1gMnSc3	premiér
Haroldu	Harold	k1gMnSc3	Harold
Macmillanovi	Macmillan	k1gMnSc3	Macmillan
memorandum	memorandum	k1gNnSc4	memorandum
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
žádal	žádat	k5eAaImAgMnS	žádat
vyzdvižení	vyzdvižení	k1gNnSc1	vyzdvižení
Francie	Francie	k1gFnSc2	Francie
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
mají	mít	k5eAaImIp3nP	mít
USA	USA	kA	USA
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
působnosti	působnost	k1gFnSc2	působnost
NATO	NATO	kA	NATO
na	na	k7c6	na
oblasti	oblast	k1gFnSc6	oblast
jako	jako	k8xC	jako
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Francie	Francie	k1gFnSc1	Francie
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
pomoci	pomoct	k5eAaPmF	pomoct
se	s	k7c7	s
vzpourami	vzpoura	k1gFnPc7	vzpoura
<g/>
.	.	kIx.	.
<g/>
Odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
memorandum	memorandum	k1gNnSc4	memorandum
de	de	k?	de
Gaulla	Gaullo	k1gNnSc2	Gaullo
neuspokojila	uspokojit	k5eNaPmAgFnS	uspokojit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
Francie	Francie	k1gFnSc1	Francie
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
z	z	k7c2	z
operací	operace	k1gFnPc2	operace
NATO	NATO	kA	NATO
a	a	k8xC	a
vykázala	vykázat	k5eAaPmAgFnS	vykázat
jednotky	jednotka	k1gFnPc4	jednotka
NATO	NATO	kA	NATO
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
probíhalo	probíhat	k5eAaImAgNnS	probíhat
pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
období	období	k1gNnSc1	období
politického	politický	k2eAgNnSc2d1	politické
uvolnění	uvolnění	k1gNnSc2	uvolnění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc2	jehož
reformy	reforma	k1gFnSc2	reforma
se	se	k3xPyFc4	se
pokoušely	pokoušet	k5eAaImAgInP	pokoušet
Československo	Československo	k1gNnSc4	Československo
demokratizovat	demokratizovat	k5eAaBmF	demokratizovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pražského	pražský	k2eAgNnSc2d1	Pražské
jara	jaro	k1gNnSc2	jaro
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
svoboda	svoboda	k1gFnSc1	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
multipartijním	multipartijní	k2eAgInSc6d1	multipartijní
systému	systém	k1gInSc6	systém
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
omezována	omezován	k2eAgFnSc1d1	omezována
moc	moc	k1gFnSc1	moc
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c4	o
odstoupení	odstoupení	k1gNnSc4	odstoupení
od	od	k7c2	od
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
pražské	pražský	k2eAgNnSc4d1	Pražské
jaro	jaro	k1gNnSc4	jaro
sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
vojsky	vojsky	k6eAd1	vojsky
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
vpadla	vpadnout	k5eAaPmAgFnS	vpadnout
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
následovala	následovat	k5eAaImAgFnS	následovat
vlna	vlna	k1gFnSc1	vlna
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Československo	Československo	k1gNnSc1	Československo
opustilo	opustit	k5eAaPmAgNnS	opustit
na	na	k7c4	na
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
invazi	invaze	k1gFnSc3	invaze
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
komunistické	komunistický	k2eAgFnPc1d1	komunistická
strany	strana	k1gFnPc1	strana
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Brežněvova	Brežněvův	k2eAgFnSc1d1	Brežněvova
doktrína	doktrína	k1gFnSc1	doktrína
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Brežněv	Brežněv	k1gMnSc1	Brežněv
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgMnS	připsat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
porušení	porušení	k1gNnSc4	porušení
suverenity	suverenita	k1gFnSc2	suverenita
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
nahradit	nahradit	k5eAaPmF	nahradit
marxismus-leninismus	marxismuseninismus	k1gInSc1	marxismus-leninismus
kapitalismem	kapitalismus	k1gInSc7	kapitalismus
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
to	ten	k3xDgNnSc1	ten
už	už	k9	už
nadále	nadále	k6eAd1	nadále
není	být	k5eNaImIp3nS	být
záležitost	záležitost	k1gFnSc1	záležitost
jenom	jenom	k9	jenom
lidu	lid	k1gInSc2	lid
té	ten	k3xDgFnSc2	ten
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
společná	společný	k2eAgFnSc1d1	společná
věc	věc	k1gFnSc1	věc
všech	všecek	k3xTgFnPc2	všecek
socialistických	socialistický	k2eAgFnPc2d1	socialistická
zemí	zem	k1gFnPc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
zavedení	zavedení	k1gNnSc4	zavedení
doktríny	doktrína	k1gFnSc2	doktrína
byly	být	k5eAaImAgInP	být
neúspěchy	neúspěch	k1gInPc1	neúspěch
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
jako	jako	k8xS	jako
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
a	a	k8xC	a
Východní	východní	k2eAgNnSc1d1	východní
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
procházely	procházet	k5eAaImAgFnP	procházet
stagnací	stagnace	k1gFnSc7	stagnace
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
poklesem	pokles	k1gInSc7	pokles
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
prosperitou	prosperita	k1gFnSc7	prosperita
Západního	západní	k2eAgNnSc2d1	západní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vystupňování	vystupňování	k1gNnSc3	vystupňování
ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
světě	svět	k1gInSc6	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
vyslal	vyslat	k5eAaPmAgMnS	vyslat
prezident	prezident	k1gMnSc1	prezident
Lyndon	Lyndon	k1gMnSc1	Lyndon
B.	B.	kA	B.
Johnson	Johnson	k1gMnSc1	Johnson
do	do	k7c2	do
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
zhruba	zhruba	k6eAd1	zhruba
22	[number]	k4	22
000	[number]	k4	000
vojenských	vojenský	k2eAgFnPc2d1	vojenská
jednotek	jednotka	k1gFnPc2	jednotka
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
jednoroční	jednoroční	k2eAgFnSc2d1	jednoroční
okupace	okupace	k1gFnSc2	okupace
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
hrozbě	hrozba	k1gFnSc3	hrozba
revoluce	revoluce	k1gFnSc2	revoluce
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
konzervativní	konzervativní	k2eAgMnSc1d1	konzervativní
Joaquín	Joaquín	k1gMnSc1	Joaquín
Balaguer	Balaguer	k1gMnSc1	Balaguer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
se	se	k3xPyFc4	se
antikomunista	antikomunista	k1gMnSc1	antikomunista
Suharto	Suharta	k1gFnSc5	Suharta
násilně	násilně	k6eAd1	násilně
ujal	ujmout	k5eAaPmAgInS	ujmout
vlády	vláda	k1gFnPc4	vláda
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
když	když	k8xS	když
sesadil	sesadit	k5eAaPmAgMnS	sesadit
Sukarna	Sukarno	k1gNnPc4	Sukarno
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
zavést	zavést	k5eAaPmF	zavést
vládu	vláda	k1gFnSc4	vláda
"	"	kIx"	"
<g/>
Nového	Nového	k2eAgInSc2d1	Nového
řádu	řád	k1gInSc2	řád
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1965	[number]	k4	1965
a	a	k8xC	a
1966	[number]	k4	1966
bylo	být	k5eAaImAgNnS	být
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
zhruba	zhruba	k6eAd1	zhruba
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
členů	člen	k1gInPc2	člen
a	a	k8xC	a
podporovatelů	podporovatel	k1gMnPc2	podporovatel
Indonéské	indonéský	k2eAgFnSc2d1	Indonéská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
levicových	levicový	k2eAgFnPc2d1	levicová
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
stupňující	stupňující	k2eAgFnSc6d1	stupňující
se	se	k3xPyFc4	se
americké	americký	k2eAgFnSc3d1	americká
intervenci	intervence	k1gFnSc3	intervence
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
mezi	mezi	k7c7	mezi
Jižním	jižní	k2eAgInSc7d1	jižní
Vietnamem	Vietnam	k1gInSc7	Vietnam
a	a	k8xC	a
komunistickým	komunistický	k2eAgInSc7d1	komunistický
Vietkongem	Vietkong	k1gInSc7	Vietkong
prezident	prezident	k1gMnSc1	prezident
Johnson	Johnson	k1gMnSc1	Johnson
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
asi	asi	k9	asi
575	[number]	k4	575
000	[number]	k4	000
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
<g />
.	.	kIx.	.
</s>
<s>
drahá	drahý	k2eAgFnSc1d1	drahá
politika	politika	k1gFnSc1	politika
oslabila	oslabit	k5eAaPmAgFnS	oslabit
americkou	americký	k2eAgFnSc4d1	americká
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c6	v
pro	pro	k7c4	pro
USA	USA	kA	USA
ponižující	ponižující	k2eAgFnSc4d1	ponižující
porážku	porážka	k1gFnSc4	porážka
této	tento	k3xDgFnSc2	tento
světové	světový	k2eAgFnSc2d1	světová
velmoci	velmoc	k1gFnSc2	velmoc
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgInPc2d3	nejchudší
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
stal	stát	k5eAaPmAgMnS	stát
kandidát	kandidát	k1gMnSc1	kandidát
socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
Salvador	Salvador	k1gMnSc1	Salvador
Allende	Allend	k1gInSc5	Allend
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
demokraticky	demokraticky	k6eAd1	demokraticky
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
marxistickým	marxistický	k2eAgMnSc7d1	marxistický
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Augusto	Augusta	k1gMnSc5	Augusta
Pinochet	Pinochet	k1gInSc1	Pinochet
provedl	provést	k5eAaPmAgInS	provést
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1973	[number]	k4	1973
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
CIA	CIA	kA	CIA
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
</s>
<s>
Allendeovy	Allendeův	k2eAgFnPc1d1	Allendeův
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
reformy	reforma	k1gFnPc1	reforma
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
a	a	k8xC	a
levicoví	levicový	k2eAgMnPc1d1	levicový
odpůrci	odpůrce	k1gMnPc1	odpůrce
byli	být	k5eAaImAgMnP	být
zabiti	zabít	k5eAaPmNgMnP	zabít
nebo	nebo	k8xC	nebo
zajati	zajat	k2eAgMnPc1d1	zajat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
operace	operace	k1gFnSc1	operace
Condor	Condor	k1gInSc1	Condor
<g/>
,	,	kIx,	,
politická	politický	k2eAgFnSc1d1	politická
kampaň	kampaň	k1gFnSc1	kampaň
vedená	vedený	k2eAgFnSc1d1	vedená
proti	proti	k7c3	proti
komunismu	komunismus	k1gInSc3	komunismus
a	a	k8xC	a
podporována	podporovat	k5eAaImNgFnS	podporovat
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
účastnili	účastnit	k5eAaImAgMnP	účastnit
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc1	Chile
<g/>
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
a	a	k8xC	a
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Jamajce	Jamajka	k1gFnSc6	Jamajka
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
Michael	Michael	k1gMnSc1	Michael
Manley	Manlea	k1gMnSc2	Manlea
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
sbližovat	sbližovat	k5eAaImF	sbližovat
s	s	k7c7	s
kubánskou	kubánský	k2eAgFnSc7d1	kubánská
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Odpověď	odpověď	k1gFnSc1	odpověď
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
spočívala	spočívat	k5eAaImAgFnS	spočívat
ve	v	k7c4	v
financování	financování	k1gNnSc4	financování
jeho	jeho	k3xOp3gMnPc2	jeho
politických	politický	k2eAgMnPc2d1	politický
oponentů	oponent	k1gMnPc2	oponent
a	a	k8xC	a
podněcování	podněcování	k1gNnSc2	podněcování
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
jamajské	jamajský	k2eAgFnSc6d1	jamajská
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
i	i	k8xC	i
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
od	od	k7c2	od
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
stavil	stavit	k5eAaImAgMnS	stavit
proti	proti	k7c3	proti
prozápadnímu	prozápadní	k2eAgInSc3d1	prozápadní
Izraeli	Izrael	k1gInSc3	Izrael
v	v	k7c6	v
Šestidenní	šestidenní	k2eAgFnSc6d1	šestidenní
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
Opotřebovací	opotřebovací	k2eAgFnSc6d1	opotřebovací
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Anvara	Anvar	k1gMnSc2	Anvar
as-Sádáta	as-Sádát	k1gMnSc2	as-Sádát
se	se	k3xPyFc4	se
Egypt	Egypt	k1gInSc1	Egypt
začal	začít	k5eAaPmAgInS	začít
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
v	v	k7c4	v
proamerickou	proamerický	k2eAgFnSc4d1	proamerická
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
se	se	k3xPyFc4	se
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
podařilo	podařit	k5eAaPmAgNnS	podařit
navázat	navázat	k5eAaPmF	navázat
blízké	blízký	k2eAgInPc4d1	blízký
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Jižním	jižní	k2eAgInSc7d1	jižní
Jemenem	Jemen	k1gInSc7	Jemen
<g/>
,	,	kIx,	,
Alžírskem	Alžírsko	k1gNnSc7	Alžírsko
a	a	k8xC	a
Irákem	Irák	k1gInSc7	Irák
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
somálští	somálský	k2eAgMnPc1d1	somálský
vojenští	vojenský	k2eAgMnPc1d1	vojenský
důstojníci	důstojník	k1gMnPc1	důstojník
vedeni	vést	k5eAaImNgMnP	vést
Muhammadem	Muhammad	k1gInSc7	Muhammad
Siadem	Siad	k1gMnSc7	Siad
Barrem	Barr	k1gMnSc7	Barr
provedli	provést	k5eAaPmAgMnP	provést
nenásilný	násilný	k2eNgInSc4d1	nenásilný
státní	státní	k2eAgInSc4d1	státní
převrat	převrat	k1gInSc4	převrat
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
Somálskou	somálský	k2eAgFnSc4d1	Somálská
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
svrhl	svrhnout	k5eAaPmAgInS	svrhnout
derg	derg	k1gInSc1	derg
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
prosovětským	prosovětský	k2eAgMnSc7d1	prosovětský
Mengistem	Mengist	k1gMnSc7	Mengist
Hailem	Hail	k1gMnSc7	Hail
Mariamem	Mariam	k1gInSc7	Mariam
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
proamerického	proamerický	k2eAgMnSc2d1	proamerický
císaře	císař	k1gMnSc2	císař
Haileho	Haile	k1gMnSc4	Haile
Selassieho	Selassie	k1gMnSc2	Selassie
I.	I.	kA	I.
a	a	k8xC	a
Mariam	Mariam	k1gInSc4	Mariam
si	se	k3xPyFc3	se
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Kubou	Kuba	k1gFnSc7	Kuba
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
Somálskem	Somálsko	k1gNnSc7	Somálsko
a	a	k8xC	a
Etiopií	Etiopie	k1gFnSc7	Etiopie
vyústili	vyústit	k5eAaPmAgMnP	vyústit
v	v	k7c4	v
Etiopsko-somálskou	etiopskoomálský	k2eAgFnSc4d1	etiopsko-somálský
válku	válka	k1gFnSc4	válka
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Barre	Barr	k1gInSc5	Barr
ztratil	ztratit	k5eAaPmAgInS	ztratit
podporu	podpora	k1gFnSc4	podpora
Sovětů	Sovět	k1gMnPc2	Sovět
a	a	k8xC	a
spojil	spojit	k5eAaPmAgMnS	spojit
se	se	k3xPyFc4	se
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
Karafiátové	Karafiátové	k2eAgFnSc6d1	Karafiátové
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
navrátilo	navrátit	k5eAaPmAgNnS	navrátit
k	k	k7c3	k
multipartijnímu	multipartijní	k2eAgInSc3d1	multipartijní
systému	systém	k1gInSc3	systém
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
Angole	Angola	k1gFnSc3	Angola
a	a	k8xC	a
Východnímu	východní	k2eAgInSc3d1	východní
Timoru	Timor	k1gInSc3	Timor
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Angole	Angola	k1gFnSc6	Angola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předtím	předtím	k6eAd1	předtím
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
probíhala	probíhat	k5eAaImAgFnS	probíhat
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
hnutím	hnutí	k1gNnSc7	hnutí
MLPA	MLPA	kA	MLPA
a	a	k8xC	a
Národní	národní	k2eAgFnSc7d1	národní
frontou	fronta	k1gFnSc7	fronta
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Angoly	Angola	k1gFnSc2	Angola
(	(	kIx(	(
<g/>
FNLA	FNLA	kA	FNLA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
podporovaly	podporovat	k5eAaImAgInP	podporovat
zároveň	zároveň	k6eAd1	zároveň
hnutí	hnutí	k1gNnSc2	hnutí
FNLA	FNLA	kA	FNLA
i	i	k8xC	i
třetí	třetí	k4xOgFnSc4	třetí
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
svaz	svaz	k1gInSc1	svaz
za	za	k7c4	za
úplnou	úplný	k2eAgFnSc4d1	úplná
nezávislost	nezávislost	k1gFnSc4	nezávislost
Angoly	Angola	k1gFnSc2	Angola
(	(	kIx(	(
<g/>
UNITA	unita	k1gMnSc1	unita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
MLPA	MLPA	kA	MLPA
<g/>
.	.	kIx.	.
<g/>
Východní	východní	k2eAgInSc1d1	východní
Timor	Timor	k1gInSc1	Timor
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Indonésie	Indonésie	k1gFnSc1	Indonésie
okupaci	okupace	k1gFnSc4	okupace
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
čtvrt	čtvrt	k1gFnSc4	čtvrt
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sblížení	sblížení	k1gNnSc3	sblížení
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
===	===	k?	===
</s>
</p>
<p>
<s>
Čínsko-sovětské	čínskoovětský	k2eAgFnPc1d1	čínsko-sovětský
roztržky	roztržka	k1gFnPc1	roztržka
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
pohraniční	pohraniční	k2eAgInPc4d1	pohraniční
konflikty	konflikt	k1gInPc4	konflikt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
a	a	k8xC	a
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Richard	Richard	k1gMnSc1	Richard
Nixon	Nixon	k1gInSc4	Nixon
těchto	tento	k3xDgInPc2	tento
konfliktů	konflikt	k1gInPc2	konflikt
využil	využít	k5eAaPmAgInS	využít
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c4	mezi
USA	USA	kA	USA
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1972	[number]	k4	1972
Nixon	Nixon	k1gMnSc1	Nixon
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
na	na	k7c6	na
schůzi	schůze	k1gFnSc6	schůze
s	s	k7c7	s
Mao	Mao	k1gMnSc1	Mao
Ce-tungem	Ceung	k1gInSc7	Ce-tung
a	a	k8xC	a
Čou	Čou	k1gMnSc7	Čou
En-lajem	Enaj	k1gMnSc7	En-laj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
snižoval	snižovat	k5eAaImAgInS	snižovat
vliv	vliv	k1gInSc1	vliv
USA	USA	kA	USA
ve	v	k7c6	v
Třetím	třetí	k4xOgInSc6	třetí
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
i	i	k9	i
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
probíhaly	probíhat	k5eAaImAgInP	probíhat
konflikty	konflikt	k1gInPc1	konflikt
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
začínalo	začínat	k5eAaImAgNnS	začínat
zmírňovat	zmírňovat	k5eAaImF	zmírňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nixon	Nixon	k1gMnSc1	Nixon
<g/>
,	,	kIx,	,
Brežněv	Brežněv	k1gMnSc1	Brežněv
a	a	k8xC	a
détente	détente	k2eAgMnSc1d1	détente
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
Číny	Čína	k1gFnSc2	Čína
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Nixon	Nixon	k1gMnSc1	Nixon
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Brežněva	Brežněva	k1gFnSc1	Brežněva
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rozhovory	rozhovor	k1gInPc1	rozhovor
o	o	k7c4	o
omezení	omezení	k1gNnSc4	omezení
strategických	strategický	k2eAgFnPc2d1	strategická
zbraní	zbraň	k1gFnPc2	zbraň
vyústily	vyústit	k5eAaPmAgFnP	vyústit
ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
významné	významný	k2eAgFnPc4d1	významná
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
strategických	strategický	k2eAgFnPc6d1	strategická
zbraních	zbraň	k1gFnPc6	zbraň
<g/>
:	:	kIx,	:
SALT	salto	k1gNnPc2	salto
I	i	k9	i
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
detailní	detailní	k2eAgFnSc4d1	detailní
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
omezení	omezení	k1gNnSc6	omezení
zbraní	zbraň	k1gFnPc2	zbraň
podepsané	podepsaný	k2eAgInPc4d1	podepsaný
oběma	dva	k4xCgFnPc7	dva
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
smlouvu	smlouva	k1gFnSc4	smlouva
ABM	ABM	kA	ABM
o	o	k7c6	o
omezeních	omezení	k1gNnPc6	omezení
systémů	systém	k1gInPc2	systém
antibalistických	antibalistický	k2eAgFnPc2d1	antibalistický
raket	raketa	k1gFnPc2	raketa
použitých	použitý	k2eAgInPc2d1	použitý
proti	proti	k7c3	proti
raketovým	raketový	k2eAgFnPc3d1	raketová
nukleárním	nukleární	k2eAgFnPc3d1	nukleární
zbraním	zbraň	k1gFnPc3	zbraň
<g/>
.	.	kIx.	.
<g/>
Nixon	Nixon	k1gMnSc1	Nixon
a	a	k8xC	a
Brežněv	Brežněv	k1gMnPc1	Brežněv
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
nové	nový	k2eAgNnSc4d1	nové
období	období	k1gNnSc4	období
"	"	kIx"	"
<g/>
mírového	mírový	k2eAgNnSc2d1	Mírové
soužití	soužití	k1gNnSc2	soužití
<g/>
"	"	kIx"	"
a	a	k8xC	a
zavedli	zavést	k5eAaPmAgMnP	zavést
novou	nový	k2eAgFnSc4d1	nová
politiku	politika	k1gFnSc4	politika
détente	détente	k2eAgFnSc4d1	détente
(	(	kIx(	(
<g/>
uvolnění	uvolnění	k1gNnSc3	uvolnění
napětí	napětí	k1gNnSc3	napětí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
Brežněv	Brežněv	k1gMnSc1	Brežněv
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
oživení	oživení	k1gNnSc4	oživení
sovětské	sovětský	k2eAgFnSc2d1	sovětská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
vysokým	vysoký	k2eAgInPc3d1	vysoký
vojenským	vojenský	k2eAgInPc3d1	vojenský
nákladům	náklad	k1gInPc3	náklad
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
<g/>
.	.	kIx.	.
<g/>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
se	se	k3xPyFc4	se
shodoval	shodovat	k5eAaImAgInS	shodovat
s	s	k7c7	s
Ostpolitik	Ostpolitika	k1gFnPc2	Ostpolitika
(	(	kIx(	(
<g/>
politikou	politika	k1gFnSc7	politika
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c4	na
zlepšování	zlepšování	k1gNnSc4	zlepšování
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
východní	východní	k2eAgFnSc7d1	východní
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
)	)	kIx)	)
západoněmeckého	západoněmecký	k2eAgMnSc2d1	západoněmecký
kancléře	kancléř	k1gMnSc2	kancléř
Willyho	Willy	k1gMnSc2	Willy
Brandta	Brandt	k1gMnSc2	Brandt
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
učiněny	učiněn	k2eAgFnPc1d1	učiněna
další	další	k2eAgFnPc1d1	další
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
stabilizaci	stabilizace	k1gFnSc6	stabilizace
situace	situace	k1gFnSc2	situace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
Závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
akt	akt	k1gInSc4	akt
Konference	konference	k1gFnSc2	konference
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
podepsaný	podepsaný	k1gMnSc1	podepsaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zhoršení	zhoršení	k1gNnSc3	zhoršení
vztahů	vztah	k1gInPc2	vztah
na	na	k7c6	na
konci	konec	k1gInSc6	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
KGB	KGB	kA	KGB
vedená	vedený	k2eAgFnSc1d1	vedená
Jurijem	Jurij	k1gMnSc7	Jurij
Vladimirovičem	Vladimirovič	k1gMnSc7	Vladimirovič
Andropovem	Andropov	k1gInSc7	Andropov
v	v	k7c6	v
pronásledování	pronásledování	k1gNnSc6	pronásledování
sovětských	sovětský	k2eAgFnPc2d1	sovětská
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
–	–	k?	–
např.	např.	kA	např.
Alexandra	Alexandr	k1gMnSc2	Alexandr
Solženicyna	Solženicyn	k1gMnSc2	Solženicyn
a	a	k8xC	a
Andreje	Andrej	k1gMnSc2	Andrej
Sacharova	Sacharův	k2eAgMnSc2d1	Sacharův
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Třetím	třetí	k4xOgMnSc7	třetí
světe	svět	k1gInSc5	svět
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
nepřímý	přímý	k2eNgInSc4d1	nepřímý
souboj	souboj	k1gInSc4	souboj
mezi	mezi	k7c7	mezi
velmocemi	velmoc	k1gFnPc7	velmoc
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
prezident	prezident	k1gMnSc1	prezident
Jimmy	Jimma	k1gFnSc2	Jimma
Carter	Carter	k1gMnSc1	Carter
snažil	snažit	k5eAaImAgMnS	snažit
zbrzdit	zbrzdit	k5eAaPmF	zbrzdit
závody	závod	k1gInPc4	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc2	zbrojení
podepsáním	podepsání	k1gNnSc7	podepsání
smlouvy	smlouva	k1gFnSc2	smlouva
SALT	salto	k1gNnPc2	salto
II	II	kA	II
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
snahy	snaha	k1gFnSc2	snaha
byly	být	k5eAaImAgInP	být
podlomeny	podlomit	k5eAaPmNgInP	podlomit
jinými	jiný	k2eAgFnPc7d1	jiná
událostmi	událost	k1gFnPc7	událost
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
včetně	včetně	k7c2	včetně
Íránské	íránský	k2eAgFnSc2d1	íránská
islámské	islámský	k2eAgFnSc2d1	islámská
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
Nikaragujské	nikaragujský	k2eAgFnPc1d1	Nikaragujská
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obě	dva	k4xCgFnPc1	dva
sesadily	sesadit	k5eAaPmAgFnP	sesadit
proamerické	proamerický	k2eAgFnPc1d1	proamerická
vlády	vláda	k1gFnPc1	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Pojmem	pojem	k1gInSc7	pojem
druhá	druhý	k4xOgFnSc1	druhý
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
některými	některý	k3yIgFnPc7	některý
historiky	historik	k1gMnPc4	historik
označováno	označován	k2eAgNnSc1d1	označováno
období	období	k1gNnSc1	období
návratu	návrat	k1gInSc2	návrat
napětí	napětí	k1gNnSc2	napětí
do	do	k7c2	do
vztahu	vztah	k1gInSc2	vztah
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
od	od	k7c2	od
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
prosince	prosinec	k1gInSc2	prosinec
1979	[number]	k4	1979
asi	asi	k9	asi
75	[number]	k4	75
000	[number]	k4	000
sovětských	sovětský	k2eAgFnPc2d1	sovětská
jednotek	jednotka	k1gFnPc2	jednotka
vniklo	vniknout	k5eAaPmAgNnS	vniknout
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
kvůli	kvůli	k7c3	kvůli
podpoře	podpora	k1gFnSc3	podpora
marxistické	marxistický	k2eAgFnSc2d1	marxistická
vlády	vláda	k1gFnSc2	vláda
sestavené	sestavený	k2eAgFnSc2d1	sestavená
bývalým	bývalý	k2eAgMnSc7d1	bývalý
premiérem	premiér	k1gMnSc7	premiér
Núrem	Núr	k1gMnSc7	Núr
Muhammadem	Muhammad	k1gInSc7	Muhammad
Tarakim	Tarakim	k1gMnSc1	Tarakim
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
září	září	k1gNnSc4	září
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Carter	Carter	k1gMnSc1	Carter
na	na	k7c4	na
sovětský	sovětský	k2eAgInSc4d1	sovětský
zásah	zásah	k1gInSc4	zásah
reagoval	reagovat	k5eAaBmAgMnS	reagovat
odstoupením	odstoupení	k1gNnSc7	odstoupení
od	od	k7c2	od
smlouvy	smlouva	k1gFnSc2	smlouva
SALT	salto	k1gNnPc2	salto
II	II	kA	II
<g/>
,	,	kIx,	,
udělením	udělení	k1gNnSc7	udělení
embarga	embargo	k1gNnSc2	embargo
na	na	k7c4	na
dodávky	dodávka	k1gFnPc4	dodávka
obilí	obilí	k1gNnSc2	obilí
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
a	a	k8xC	a
žádostí	žádost	k1gFnPc2	žádost
o	o	k7c4	o
zvýšení	zvýšení	k1gNnSc4	zvýšení
armádních	armádní	k2eAgInPc2d1	armádní
výdajů	výdaj	k1gInPc2	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
budou	být	k5eAaImBp3nP	být
bojkotovat	bojkotovat	k5eAaImF	bojkotovat
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc4d1	sovětský
zásah	zásah	k1gInSc4	zásah
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejvážnější	vážní	k2eAgNnSc4d3	nejvážnější
ohrožení	ohrožení	k1gNnSc4	ohrožení
míru	mír	k1gInSc2	mír
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reagan	Reagan	k1gMnSc1	Reagan
a	a	k8xC	a
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
porazil	porazit	k5eAaPmAgMnS	porazit
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Jimmyho	Jimmy	k1gMnSc2	Jimmy
Cartera	Carter	k1gMnSc2	Carter
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
a	a	k8xC	a
nová	nový	k2eAgFnSc1d1	nová
britská	britský	k2eAgFnSc1d1	britská
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
vlády	vláda	k1gFnSc2	vláda
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
ideologii	ideologie	k1gFnSc4	ideologie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
détente	détente	k2eAgMnPc1d1	détente
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
označil	označit	k5eAaPmAgMnS	označit
komunismus	komunismus	k1gInSc4	komunismus
za	za	k7c4	za
"	"	kIx"	"
<g/>
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
úchylku	úchylka	k1gFnSc4	úchylka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
zmizí	zmizet	k5eAaPmIp3nS	zmizet
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
přirozeností	přirozenost	k1gFnSc7	přirozenost
<g/>
"	"	kIx"	"
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
označil	označit	k5eAaPmAgMnS	označit
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
za	za	k7c4	za
"	"	kIx"	"
<g/>
říši	říše	k1gFnSc4	říše
zla	zlo	k1gNnSc2	zlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
přes	přes	k7c4	přes
výsledky	výsledek	k1gInPc4	výsledek
Íránské	íránský	k2eAgFnSc2d1	íránská
islámské	islámský	k2eAgFnSc2d1	islámská
revoluce	revoluce	k1gFnSc2	revoluce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
a	a	k8xC	a
špatné	špatný	k2eAgInPc1d1	špatný
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
vládou	vláda	k1gFnSc7	vláda
Rúholláha	Rúholláh	k1gMnSc2	Rúholláh
Chomejního	Chomejní	k2eAgNnSc2d1	Chomejní
se	se	k3xPyFc4	se
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
administrativa	administrativa	k1gFnSc1	administrativa
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
snažila	snažil	k1gMnSc2	snažil
s	s	k7c7	s
antikomunistickým	antikomunistický	k2eAgInSc7d1	antikomunistický
Chomejním	Chomejní	k2eAgInSc7d1	Chomejní
sblížit	sblížit	k5eAaPmF	sblížit
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
mu	on	k3xPp3gNnSc3	on
např.	např.	kA	např.
ilegálním	ilegální	k2eAgInSc7d1	ilegální
prodejem	prodej	k1gInSc7	prodej
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
mu	on	k3xPp3gMnSc3	on
CIA	CIA	kA	CIA
vydala	vydat	k5eAaPmAgFnS	vydat
seznam	seznam	k1gInSc4	seznam
íránských	íránský	k2eAgMnPc2d1	íránský
komunistů	komunista	k1gMnPc2	komunista
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
levicových	levicový	k2eAgMnPc2d1	levicový
politiků	politik	k1gMnPc2	politik
pracujících	pracující	k2eAgMnPc2d1	pracující
v	v	k7c6	v
íránské	íránský	k2eAgFnSc6d1	íránská
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
<g/>
Reaganův	Reaganův	k2eAgInSc4d1	Reaganův
postoj	postoj	k1gInSc4	postoj
ke	k	k7c3	k
komunismu	komunismus	k1gInSc3	komunismus
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c4	v
Reaganovu	Reaganův	k2eAgFnSc4d1	Reaganova
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ospravedlňovala	ospravedlňovat	k5eAaImAgFnS	ospravedlňovat
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
podvracení	podvracení	k1gNnSc4	podvracení
komunistických	komunistický	k2eAgFnPc2d1	komunistická
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
CIA	CIA	kA	CIA
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
oslabit	oslabit	k5eAaPmF	oslabit
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
podporou	podpora	k1gFnSc7	podpora
islamismu	islamismus	k1gInSc2	islamismus
ve	v	k7c6	v
většinově	většinově	k6eAd1	většinově
muslimské	muslimský	k2eAgFnSc6d1	muslimská
sovětské	sovětský	k2eAgFnSc6d1	sovětská
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
CIA	CIA	kA	CIA
podněcovala	podněcovat	k5eAaImAgFnS	podněcovat
antikomunistickou	antikomunistický	k2eAgFnSc4d1	antikomunistická
pákistánskou	pákistánský	k2eAgFnSc4d1	pákistánská
službu	služba	k1gFnSc4	služba
Inter-Services	Inter-Servicesa	k1gFnPc2	Inter-Servicesa
Intelligence	Intelligence	k1gFnSc2	Intelligence
(	(	kIx(	(
<g/>
ISI	ISI	kA	ISI
<g/>
)	)	kIx)	)
k	k	k7c3	k
výcviku	výcvik	k1gInSc3	výcvik
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
účastnit	účastnit	k5eAaImF	účastnit
džihádu	džihád	k1gInSc3	džihád
proti	proti	k7c3	proti
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Solidarita	solidarita	k1gFnSc1	solidarita
a	a	k8xC	a
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
stav	stav	k1gInSc1	stav
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
Papež	Papež	k1gMnSc1	Papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
podporoval	podporovat	k5eAaImAgInS	podporovat
antikomunismus	antikomunismus	k1gInSc1	antikomunismus
<g/>
;	;	kIx,	;
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
svého	svůj	k3xOyFgNnSc2	svůj
rodného	rodný	k2eAgNnSc2d1	rodné
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
povzbudil	povzbudit	k5eAaPmAgInS	povzbudit
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
a	a	k8xC	a
nacionalistickou	nacionalistický	k2eAgFnSc4d1	nacionalistická
obnovu	obnova	k1gFnSc4	obnova
soustředěnou	soustředěný	k2eAgFnSc4d1	soustředěná
okolo	okolo	k7c2	okolo
hnutí	hnutí	k1gNnSc2	hnutí
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1981	[number]	k4	1981
polský	polský	k2eAgMnSc1d1	polský
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Wojciech	Wojciech	k1gInSc1	Wojciech
Jaruzelski	Jaruzelske	k1gFnSc4	Jaruzelske
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
vliv	vliv	k1gInSc1	vliv
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Reagan	Reagan	k1gMnSc1	Reagan
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
udělením	udělení	k1gNnSc7	udělení
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
sankcí	sankce	k1gFnPc2	sankce
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Vlivný	vlivný	k2eAgMnSc1d1	vlivný
komunistický	komunistický	k2eAgMnSc1d1	komunistický
politik	politik	k1gMnSc1	politik
Michail	Michail	k1gMnSc1	Michail
Andrejevič	Andrejevič	k1gMnSc1	Andrejevič
Suslov	Suslov	k1gInSc4	Suslov
radil	radit	k5eAaImAgMnS	radit
sovětským	sovětský	k2eAgMnSc7d1	sovětský
vůdcům	vůdce	k1gMnPc3	vůdce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nezasahovali	zasahovat	k5eNaImAgMnP	zasahovat
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
Polsko	Polsko	k1gNnSc1	Polsko
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
Solidarity	solidarita	k1gFnSc2	solidarita
<g/>
.	.	kIx.	.
</s>
<s>
Odůvodňoval	odůvodňovat	k5eAaImAgMnS	odůvodňovat
to	ten	k3xDgNnSc4	ten
případnými	případný	k2eAgFnPc7d1	případná
velkými	velký	k2eAgFnPc7d1	velká
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
sankcemi	sankce	k1gFnPc7	sankce
a	a	k8xC	a
hrozbou	hrozba	k1gFnSc7	hrozba
pro	pro	k7c4	pro
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
problémy	problém	k1gInPc1	problém
===	===	k?	===
</s>
</p>
<p>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
udržování	udržování	k1gNnSc1	udržování
stálo	stát	k5eAaImAgNnS	stát
až	až	k9	až
25	[number]	k4	25
%	%	kIx~	%
sovětského	sovětský	k2eAgInSc2d1	sovětský
hrubého	hrubý	k2eAgInSc2d1	hrubý
národního	národní	k2eAgInSc2d1	národní
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
HNP	HNP	kA	HNP
<g/>
)	)	kIx)	)
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
investic	investice	k1gFnPc2	investice
v	v	k7c6	v
civilním	civilní	k2eAgInSc6d1	civilní
sektoru	sektor	k1gInSc6	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Utrácení	utrácení	k1gNnSc1	utrácení
za	za	k7c7	za
závody	závod	k1gInPc7	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vyústily	vyústit	k5eAaPmAgInP	vyústit
v	v	k7c6	v
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
desetiletí	desetiletí	k1gNnSc6	desetiletí
trvající	trvající	k2eAgFnSc4d1	trvající
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
stagnaci	stagnace	k1gFnSc4	stagnace
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
letech	léto	k1gNnPc6	léto
Brežněvovy	Brežněvův	k2eAgFnSc2d1	Brežněvova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgFnPc1d1	sovětská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
největší	veliký	k2eAgFnSc7d3	veliký
armádou	armáda	k1gFnSc7	armáda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
co	co	k8xS	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
počtu	počet	k1gInSc2	počet
a	a	k8xC	a
typů	typ	k1gInPc2	typ
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
počtu	počet	k1gInSc2	počet
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
velikostí	velikost	k1gFnPc2	velikost
vojensko-průmyslového	vojenskorůmyslový	k2eAgInSc2d1	vojensko-průmyslový
komplexu	komplex	k1gInSc2	komplex
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
armáda	armáda	k1gFnSc1	armáda
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
překonala	překonat	k5eAaPmAgFnS	překonat
armádu	armáda	k1gFnSc4	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
prezident	prezident	k1gMnSc1	prezident
Carter	Carter	k1gMnSc1	Carter
vynakládat	vynakládat	k5eAaImF	vynakládat
více	hodně	k6eAd2	hodně
zdrojů	zdroj	k1gInPc2	zdroj
na	na	k7c4	na
budování	budování	k1gNnSc4	budování
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k8xC	i
administrace	administrace	k1gFnSc1	administrace
Ronalda	Ronald	k1gMnSc2	Ronald
Reagana	Reagan	k1gMnSc2	Reagan
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
armádu	armáda	k1gFnSc4	armáda
z	z	k7c2	z
5,3	[number]	k4	5,3
%	%	kIx~	%
HNP	HNP	kA	HNP
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
na	na	k7c4	na
6,5	[number]	k4	6,5
%	%	kIx~	%
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
nárůst	nárůst	k1gInSc4	nárůst
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
míru	mír	k1gInSc2	mír
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
<g/>
Budování	budování	k1gNnSc1	budování
napětí	napětí	k1gNnSc2	napětí
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
oznámily	oznámit	k5eAaPmAgInP	oznámit
program	program	k1gInSc4	program
B-1	B-1	k1gFnPc2	B-1
Lancer	Lancra	k1gFnPc2	Lancra
<g/>
,	,	kIx,	,
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
rakety	raketa	k1gFnPc1	raketa
LGM-	LGM-	k1gFnSc1	LGM-
<g/>
118	[number]	k4	118
<g/>
,	,	kIx,	,
umístily	umístit	k5eAaPmAgFnP	umístit
řízené	řízený	k2eAgFnPc4d1	řízená
střely	střela	k1gFnPc4	střela
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
oznámily	oznámit	k5eAaPmAgInP	oznámit
Strategickou	strategický	k2eAgFnSc4d1	strategická
obrannou	obranný	k2eAgFnSc4d1	obranná
iniciativu	iniciativa	k1gFnSc4	iniciativa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odpověď	odpověď	k1gFnSc1	odpověď
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
nespočívala	spočívat	k5eNaImAgFnS	spočívat
v	v	k7c6	v
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obrovské	obrovský	k2eAgInPc4d1	obrovský
armádní	armádní	k2eAgInPc4d1	armádní
náklady	náklad	k1gInPc4	náklad
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
ekonomikou	ekonomika	k1gFnSc7	ekonomika
a	a	k8xC	a
kolektivizací	kolektivizace	k1gFnSc7	kolektivizace
už	už	k6eAd1	už
tak	tak	k6eAd1	tak
zatěžovaly	zatěžovat	k5eAaImAgFnP	zatěžovat
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
přesvědčily	přesvědčit	k5eAaPmAgInP	přesvědčit
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
těžila	těžit	k5eAaImAgFnS	těžit
více	hodně	k6eAd2	hodně
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
těžbu	těžba	k1gFnSc4	těžba
zvyšovaly	zvyšovat	k5eAaImAgInP	zvyšovat
i	i	k9	i
státy	stát	k1gInPc1	stát
mimo	mimo	k6eAd1	mimo
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
nadbytku	nadbytek	k1gInSc3	nadbytek
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poškodil	poškodit	k5eAaPmAgInS	poškodit
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ropa	ropa	k1gFnSc1	ropa
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
sovětského	sovětský	k2eAgInSc2d1	sovětský
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
plánovanou	plánovaný	k2eAgFnSc7d1	plánovaná
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
,	,	kIx,	,
snižování	snižování	k1gNnSc1	snižování
ceny	cena	k1gFnSc2	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
vysoké	vysoký	k2eAgInPc1d1	vysoký
armádní	armádní	k2eAgInPc1d1	armádní
výdaje	výdaj	k1gInPc1	výdaj
postupně	postupně	k6eAd1	postupně
přivedly	přivést	k5eAaPmAgInP	přivést
Sovětskou	sovětský	k2eAgFnSc4d1	sovětská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
ke	k	k7c3	k
stagnaci	stagnace	k1gFnSc3	stagnace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1983	[number]	k4	1983
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
při	při	k7c6	při
letu	let	k1gInSc6	let
Korean	Koreana	k1gFnPc2	Koreana
Air	Air	k1gFnSc4	Air
007	[number]	k4	007
sestřelil	sestřelit	k5eAaPmAgInS	sestřelit
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
palubě	paluba	k1gFnSc6	paluba
bylo	být	k5eAaImAgNnS	být
269	[number]	k4	269
lidí	člověk	k1gMnPc2	člověk
včetně	včetně	k7c2	včetně
amerického	americký	k2eAgMnSc2d1	americký
kongresmana	kongresman	k1gMnSc2	kongresman
Larryho	Larry	k1gMnSc2	Larry
McDonalda	McDonald	k1gMnSc2	McDonald
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
narušil	narušit	k5eAaPmAgMnS	narušit
sovětský	sovětský	k2eAgInSc4d1	sovětský
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Katastrofu	katastrofa	k1gFnSc4	katastrofa
poblíž	poblíž	k6eAd1	poblíž
ostrovu	ostrov	k1gInSc3	ostrov
Sachalin	Sachalin	k1gInSc1	Sachalin
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
masakr	masakr	k1gInSc4	masakr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
nepřežil	přežít	k5eNaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Zkušební	zkušební	k2eAgInSc4d1	zkušební
jaderný	jaderný	k2eAgInSc4d1	jaderný
útok	útok	k1gInSc4	útok
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
Able	Able	k1gInSc4	Able
Archer	Archra	k1gFnPc2	Archra
83	[number]	k4	83
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejnebezpečnější	bezpečný	k2eNgInSc4d3	nejnebezpečnější
moment	moment	k1gInSc4	moment
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
od	od	k7c2	od
Karibské	karibský	k2eAgFnSc2d1	karibská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
cítil	cítit	k5eAaImAgInS	cítit
hrozbu	hrozba	k1gFnSc4	hrozba
jaderného	jaderný	k2eAgInSc2d1	jaderný
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
americkou	americký	k2eAgFnSc4d1	americká
veřejnost	veřejnost	k1gFnSc4	veřejnost
znepokojovaly	znepokojovat	k5eAaImAgInP	znepokojovat
zásahy	zásah	k1gInPc1	zásah
do	do	k7c2	do
cizích	cizí	k2eAgInPc2d1	cizí
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Reaganova	Reaganův	k2eAgFnSc1d1	Reaganova
administrace	administrace	k1gFnSc1	administrace
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
rychlou	rychlý	k2eAgFnSc4d1	rychlá
a	a	k8xC	a
levnou	levný	k2eAgFnSc4d1	levná
taktiku	taktika	k1gFnSc4	taktika
protipovstaleckých	protipovstalecký	k2eAgFnPc2d1	protipovstalecká
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
do	do	k7c2	do
Libanonské	libanonský	k2eAgFnSc2d1	libanonská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
vnikly	vniknout	k5eAaPmAgInP	vniknout
do	do	k7c2	do
Grenady	Grenada	k1gFnSc2	Grenada
<g/>
,	,	kIx,	,
bombardovaly	bombardovat	k5eAaImAgFnP	bombardovat
Libyi	Libye	k1gFnSc4	Libye
a	a	k8xC	a
podpořily	podpořit	k5eAaPmAgFnP	podpořit
Contras	Contras	k1gInSc4	Contras
<g/>
,	,	kIx,	,
středoamerické	středoamerický	k2eAgNnSc4d1	středoamerické
antikomunistické	antikomunistický	k2eAgFnPc4d1	antikomunistická
paramilitantní	paramilitantní	k2eAgFnPc4d1	paramilitantní
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
o	o	k7c6	o
sesazení	sesazení	k1gNnSc6	sesazení
prosovětských	prosovětský	k2eAgMnPc2d1	prosovětský
Sandinistů	sandinista	k1gMnPc2	sandinista
v	v	k7c6	v
Nikaragui	Nikaragua	k1gFnSc6	Nikaragua
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
byly	být	k5eAaImAgInP	být
mezi	mezi	k7c7	mezi
americkou	americký	k2eAgFnSc7d1	americká
veřejností	veřejnost	k1gFnSc7	veřejnost
podporovány	podporován	k2eAgInPc1d1	podporován
zásahy	zásah	k1gInPc1	zásah
v	v	k7c6	v
Grenadě	Grenada	k1gFnSc6	Grenada
a	a	k8xC	a
Libyi	Libye	k1gFnSc6	Libye
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
Contras	Contras	k1gInSc1	Contras
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
politický	politický	k2eAgInSc4d1	politický
skandál	skandál	k1gInSc4	skandál
<g/>
.	.	kIx.	.
<g/>
Mezitím	mezitím	k6eAd1	mezitím
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
platil	platit	k5eAaImAgInS	platit
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Brežněv	Brežněv	k1gMnSc1	Brežněv
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
nebude	být	k5eNaImBp3nS	být
trvat	trvat	k5eAaImF	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
Američany	Američan	k1gMnPc4	Američan
podporovaní	podporovaný	k2eAgMnPc1d1	podporovaný
muslimští	muslimský	k2eAgMnPc1d1	muslimský
partyzáni	partyzán	k1gMnPc1	partyzán
kladli	klást	k5eAaImAgMnP	klást
proti	proti	k7c3	proti
invazi	invaze	k1gFnSc3	invaze
tuhý	tuhý	k2eAgInSc4d1	tuhý
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
vyslal	vyslat	k5eAaPmAgInS	vyslat
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
svého	svůj	k3xOyFgInSc2	svůj
loutkového	loutkový	k2eAgInSc2d1	loutkový
režimu	režim	k1gInSc2	režim
100	[number]	k4	100
000	[number]	k4	000
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
což	což	k3yRnSc4	což
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
někdy	někdy	k6eAd1	někdy
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
sovětský	sovětský	k2eAgInSc1d1	sovětský
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
ničivější	ničivý	k2eAgFnSc1d2	ničivější
než	než	k8xS	než
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
pro	pro	k7c4	pro
Američany	Američan	k1gMnPc4	Američan
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
konflikt	konflikt	k1gInSc1	konflikt
probíhal	probíhat	k5eAaImAgInS	probíhat
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
jako	jako	k8xC	jako
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1982	[number]	k4	1982
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Leonid	Leonid	k1gInSc4	Leonid
Iljič	Iljič	k1gMnSc1	Iljič
Brežněv	Brežněv	k1gMnSc1	Brežněv
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vůdci	vůdce	k1gMnPc1	vůdce
<g/>
,	,	kIx,	,
Jurij	Jurij	k1gMnSc1	Jurij
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
Andropov	Andropovo	k1gNnPc2	Andropovo
a	a	k8xC	a
Konstantin	Konstantin	k1gMnSc1	Konstantin
Ustinovič	Ustinovič	k1gMnSc1	Ustinovič
Černěnko	Černěnka	k1gFnSc5	Černěnka
<g/>
,	,	kIx,	,
nevydrželi	vydržet	k5eNaPmAgMnP	vydržet
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
smrti	smrt	k1gFnSc3	smrt
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poslední	poslední	k2eAgInPc4d1	poslední
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
91	[number]	k4	91
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Gorbačovovy	Gorbačovův	k2eAgFnSc2d1	Gorbačovova
reformy	reforma	k1gFnSc2	reforma
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
ÚV	ÚV	kA	ÚV
KSSS	KSSS	kA	KSSS
stal	stát	k5eAaPmAgMnS	stát
Michail	Michail	k1gMnSc1	Michail
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Gorbačov	Gorbačovo	k1gNnPc2	Gorbačovo
<g/>
,	,	kIx,	,
sovětská	sovětský	k2eAgFnSc1d1	sovětská
ekonomika	ekonomika	k1gFnSc1	ekonomika
stagnovala	stagnovat	k5eAaImAgFnS	stagnovat
a	a	k8xC	a
čelila	čelit	k5eAaImAgFnS	čelit
následkům	následek	k1gInPc3	následek
přebytku	přebytek	k1gInSc2	přebytek
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
sovětský	sovětský	k2eAgMnSc1d1	sovětský
vůdce	vůdce	k1gMnSc1	vůdce
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInSc4d1	počáteční
skepticismus	skepticismus	k1gInSc4	skepticismus
Západu	západ	k1gInSc2	západ
namísto	namísto	k7c2	namísto
pokračování	pokračování	k1gNnSc2	pokračování
závodů	závod	k1gInPc2	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
zotavení	zotavení	k1gNnSc4	zotavení
sovětské	sovětský	k2eAgFnSc2d1	sovětská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zavést	zavést	k5eAaPmF	zavést
sérii	série	k1gFnSc4	série
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Oznámil	oznámit	k5eAaPmAgInS	oznámit
skupinu	skupina	k1gFnSc4	skupina
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
reforem	reforma	k1gFnPc2	reforma
zvanou	zvaný	k2eAgFnSc4d1	zvaná
perestrojka	perestrojka	k1gFnSc1	perestrojka
neboli	neboli	k8xC	neboli
přestavba	přestavba	k1gFnSc1	přestavba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
soukromé	soukromý	k2eAgNnSc4d1	soukromé
podnikání	podnikání	k1gNnSc4	podnikání
a	a	k8xC	a
zakládání	zakládání	k1gNnSc4	zakládání
podniků	podnik	k1gInPc2	podnik
se	s	k7c7	s
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
účastí	účast	k1gFnSc7	účast
<g/>
.	.	kIx.	.
<g/>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
perestrojkou	perestrojka	k1gFnSc7	perestrojka
zavedl	zavést	k5eAaPmAgInS	zavést
i	i	k9	i
glasnosť	glasnosť	k1gFnSc4	glasnosť
neboli	neboli	k8xC	neboli
otevřenost	otevřenost	k1gFnSc4	otevřenost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
svobodu	svoboda	k1gFnSc4	svoboda
tisku	tisk	k1gInSc2	tisk
a	a	k8xC	a
transparentnost	transparentnost	k1gFnSc1	transparentnost
státních	státní	k2eAgFnPc2d1	státní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Glasnosť	glasnosť	k1gFnSc1	glasnosť
také	také	k9	také
Sovětům	Sověty	k1gInPc3	Sověty
umožnila	umožnit	k5eAaPmAgFnS	umožnit
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
západním	západní	k2eAgInSc7d1	západní
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
umocňovala	umocňovat	k5eAaImAgFnS	umocňovat
politika	politika	k1gFnSc1	politika
détente	détente	k2eAgFnSc1d1	détente
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uvolnění	uvolnění	k1gNnSc1	uvolnění
vztahů	vztah	k1gInPc2	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vojenských	vojenský	k2eAgInPc6d1	vojenský
a	a	k8xC	a
politických	politický	k2eAgInPc6d1	politický
ústupcích	ústupek	k1gInPc6	ústupek
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
diskuzemi	diskuze	k1gFnPc7	diskuze
o	o	k7c6	o
ekonomických	ekonomický	k2eAgInPc6d1	ekonomický
problémech	problém	k1gInPc6	problém
a	a	k8xC	a
omezení	omezení	k1gNnSc6	omezení
závodů	závod	k1gInPc2	závod
ve	v	k7c6	v
zbrojení	zbrojení	k1gNnSc6	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
setkání	setkání	k1gNnSc1	setkání
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1985	[number]	k4	1985
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
skončilo	skončit	k5eAaPmAgNnS	skončit
bez	bez	k7c2	bez
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
výsledku	výsledek	k1gInSc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
setkání	setkání	k1gNnSc1	setkání
konající	konající	k2eAgMnSc1d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
Reykjavíku	Reykjavík	k1gInSc6	Reykjavík
probíhalo	probíhat	k5eAaImAgNnS	probíhat
dobře	dobře	k6eAd1	dobře
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
pozornost	pozornost	k1gFnSc1	pozornost
stočila	stočit	k5eAaPmAgFnS	stočit
na	na	k7c4	na
Strategickou	strategický	k2eAgFnSc4d1	strategická
obrannou	obranný	k2eAgFnSc4d1	obranná
iniciativu	iniciativa	k1gFnSc4	iniciativa
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
Reaganem	Reagan	k1gMnSc7	Reagan
<g/>
.	.	kIx.	.
</s>
<s>
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
po	po	k7c6	po
Reaganovi	Reagan	k1gMnSc3	Reagan
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
zrušil	zrušit	k5eAaPmAgMnS	zrušit
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
on	on	k3xPp3gMnSc1	on
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
summit	summit	k1gInSc4	summit
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc4	třetí
setkání	setkání	k1gNnPc2	setkání
konané	konaný	k2eAgFnSc2d1	konaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
likvidaci	likvidace	k1gFnSc6	likvidace
raket	raketa	k1gFnPc2	raketa
středního	střední	k2eAgInSc2d1	střední
a	a	k8xC	a
krátkého	krátký	k2eAgInSc2d1	krátký
doletu	dolet	k1gInSc2	dolet
(	(	kIx(	(
<g/>
INF	INF	kA	INF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvou	smlouva	k1gFnSc7	smlouva
INF	INF	kA	INF
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
zaručily	zaručit	k5eAaPmAgInP	zaručit
o	o	k7c6	o
zničení	zničení	k1gNnSc6	zničení
všech	všecek	k3xTgFnPc2	všecek
jaderných	jaderný	k2eAgFnPc2d1	jaderná
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
raket	raketa	k1gFnPc2	raketa
středního	střední	k2eAgInSc2d1	střední
doletu	dolet	k1gInSc2	dolet
(	(	kIx(	(
<g/>
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
5500	[number]	k4	5500
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vztahy	vztah	k1gInPc1	vztah
Západu	západ	k1gInSc2	západ
s	s	k7c7	s
Východem	východ	k1gInSc7	východ
rychle	rychle	k6eAd1	rychle
uvolňovaly	uvolňovat	k5eAaImAgFnP	uvolňovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
závěrečné	závěrečný	k2eAgNnSc4d1	závěrečné
setkání	setkání	k1gNnSc4	setkání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
a	a	k8xC	a
George	George	k1gInSc1	George
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
podepsali	podepsat	k5eAaPmAgMnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
START	start	k1gInSc1	start
I	i	k8xC	i
o	o	k7c6	o
snižování	snižování	k1gNnSc6	snižování
počtu	počet	k1gInSc2	počet
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
opouští	opouštět	k5eAaImIp3nS	opouštět
od	od	k7c2	od
Brežněvovy	Brežněvův	k2eAgFnSc2d1	Brežněvova
doktríny	doktrína	k1gFnSc2	doktrína
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
novou	nový	k2eAgFnSc4d1	nová
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
nazvána	nazván	k2eAgFnSc1d1	nazvána
Sinatrova	Sinatrův	k2eAgFnSc1d1	Sinatrova
doktrína	doktrína	k1gFnSc1	doktrína
a	a	k8xC	a
která	který	k3yQgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
státům	stát	k1gInPc3	stát
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
vlastních	vlastní	k2eAgFnPc6d1	vlastní
vnitřních	vnitřní	k2eAgFnPc6d1	vnitřní
otázkách	otázka	k1gFnPc6	otázka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
opustila	opustit	k5eAaPmAgFnS	opustit
Afghánistán	Afghánistán	k1gInSc4	Afghánistán
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
chybě	chyba	k1gFnSc3	chyba
člena	člen	k1gMnSc2	člen
politbyra	politbyro	k1gNnSc2	politbyro
Güntera	Günter	k1gMnSc2	Günter
Schabowského	Schabowský	k1gMnSc2	Schabowský
otevřela	otevřít	k5eAaPmAgFnS	otevřít
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Východním	východní	k2eAgInSc7d1	východní
a	a	k8xC	a
Západním	západní	k2eAgInSc7d1	západní
Berlínem	Berlín	k1gInSc7	Berlín
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
oficiálně	oficiálně	k6eAd1	oficiálně
schválil	schválit	k5eAaPmAgInS	schválit
znovusjednocení	znovusjednocení	k1gNnSc4	znovusjednocení
Německa	Německo	k1gNnSc2	Německo
<g/>
.3	.3	k4	.3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
Gorbačov	Gorbačovo	k1gNnPc2	Gorbačovo
a	a	k8xC	a
Reaganův	Reaganův	k2eAgMnSc1d1	Reaganův
nástupce	nástupce	k1gMnSc1	nástupce
George	Georg	k1gMnSc2	Georg
H.	H.	kA	H.
W.	W.	kA	W.
Bush	Bush	k1gMnSc1	Bush
na	na	k7c6	na
summitu	summit	k1gInSc6	summit
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
oznámili	oznámit	k5eAaPmAgMnP	oznámit
konec	konec	k1gInSc4	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dřívější	dřívější	k2eAgMnPc1d1	dřívější
soupeři	soupeř	k1gMnPc1	soupeř
spojili	spojit	k5eAaPmAgMnP	spojit
proti	proti	k7c3	proti
Iráku	Irák	k1gInSc3	Irák
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozpad	rozpad	k1gInSc1	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
sovětský	sovětský	k2eAgInSc1d1	sovětský
smluvní	smluvní	k2eAgInSc1d1	smluvní
systém	systém	k1gInSc1	systém
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
zhroucení	zhroucení	k1gNnSc2	zhroucení
a	a	k8xC	a
komunističtí	komunistický	k2eAgMnPc1d1	komunistický
vůdci	vůdce	k1gMnPc1	vůdce
zemí	zem	k1gFnPc2	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
ztráceli	ztrácet	k5eAaImAgMnP	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
oslabovala	oslabovat	k5eAaImAgFnS	oslabovat
glasnosť	glasnosť	k1gFnSc1	glasnosť
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
soudržnost	soudržnost	k1gFnSc4	soudržnost
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
blížil	blížit	k5eAaImAgInS	blížit
začátek	začátek	k1gInSc1	začátek
rozpadu	rozpad	k1gInSc2	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
nucena	nutit	k5eAaImNgFnS	nutit
po	po	k7c6	po
73	[number]	k4	73
letech	léto	k1gNnPc6	léto
předat	předat	k5eAaPmF	předat
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
vlna	vlna	k1gFnSc1	vlna
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
státy	stát	k1gInPc4	stát
jako	jako	k9	jako
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
svrhly	svrhnout	k5eAaPmAgInP	svrhnout
komunistickou	komunistický	k2eAgFnSc4d1	komunistická
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
změna	změna	k1gFnSc1	změna
režimu	režim	k1gInSc2	režim
neobešla	obešnout	k5eNaPmAgFnS	obešnout
bez	bez	k7c2	bez
výraznějšího	výrazný	k2eAgNnSc2d2	výraznější
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
armáda	armáda	k1gFnSc1	armáda
střílela	střílet	k5eAaImAgFnS	střílet
do	do	k7c2	do
demonstrantů	demonstrant	k1gMnPc2	demonstrant
a	a	k8xC	a
zabila	zabít	k5eAaPmAgFnS	zabít
97	[number]	k4	97
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
Nicolae	Nicolae	k1gNnSc4	Nicolae
Ceauș	Ceauș	k1gMnSc1	Ceauș
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
střílení	střílení	k1gNnSc4	střílení
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
<g/>
Gorbačovův	Gorbačovův	k2eAgInSc1d1	Gorbačovův
shovívavý	shovívavý	k2eAgInSc1d1	shovívavý
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
východní	východní	k2eAgFnSc3d1	východní
Evropě	Evropa	k1gFnSc3	Evropa
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
nevztahoval	vztahovat	k5eNaImAgInS	vztahovat
na	na	k7c4	na
sovětské	sovětský	k2eAgNnSc4d1	sovětské
území	území	k1gNnSc4	území
<g/>
;	;	kIx,	;
i	i	k9	i
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
Gorbačovem	Gorbačov	k1gInSc7	Gorbačov
snažil	snažit	k5eAaImAgInS	snažit
udržet	udržet	k5eAaPmF	udržet
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
si	se	k3xPyFc3	se
nepřejí	přát	k5eNaImIp3nP	přát
oslabit	oslabit	k5eAaPmF	oslabit
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
oslaben	oslabit	k5eAaPmNgInS	oslabit
potlačeným	potlačený	k2eAgInSc7d1	potlačený
Srpnovým	srpnový	k2eAgInSc7d1	srpnový
pučem	puč	k1gInSc7	puč
a	a	k8xC	a
vzrůstajícím	vzrůstající	k2eAgInSc7d1	vzrůstající
počtem	počet	k1gInSc7	počet
svazových	svazový	k2eAgFnPc2d1	svazová
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
hrozily	hrozit	k5eAaImAgFnP	hrozit
vystoupením	vystoupení	k1gNnSc7	vystoupení
ze	z	k7c2	z
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
konec	konec	k1gInSc4	konec
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
vznik	vznik	k1gInSc1	vznik
Společenství	společenství	k1gNnSc2	společenství
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
rozpad	rozpad	k1gInSc1	rozpad
ohlášen	ohlášen	k2eAgInSc1d1	ohlášen
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Následky	následek	k1gInPc4	následek
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
Rusko	Rusko	k1gNnSc1	Rusko
výrazně	výrazně	k6eAd1	výrazně
snížilo	snížit	k5eAaPmAgNnS	snížit
armádní	armádní	k2eAgInPc4d1	armádní
výdaje	výdaj	k1gInPc4	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
ve	v	k7c6	v
vojensko-průmyslovém	vojenskorůmyslový	k2eAgInSc6d1	vojensko-průmyslový
sektoru	sektor	k1gInSc6	sektor
pracovala	pracovat	k5eAaImAgFnS	pracovat
pětina	pětina	k1gFnSc1	pětina
dospělých	dospělý	k2eAgMnPc2d1	dospělý
Sovětů	Sovět	k1gMnPc2	Sovět
a	a	k8xC	a
po	po	k7c6	po
škrtech	škrt	k1gInPc6	škrt
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
bez	bez	k7c2	bez
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
prošlo	projít	k5eAaPmAgNnS	projít
sérií	série	k1gFnPc2	série
kapitalistických	kapitalistický	k2eAgFnPc2d1	kapitalistická
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
zažilo	zažít	k5eAaPmAgNnS	zažít
Rusko	Rusko	k1gNnSc1	Rusko
finanční	finanční	k2eAgFnSc4d1	finanční
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
recesi	recese	k1gFnSc4	recese
hlubší	hluboký	k2eAgMnSc1d2	hlubší
než	než	k8xS	než
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
během	během	k7c2	během
Velké	velký	k2eAgFnSc2d1	velká
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
ale	ale	k8xC	ale
ekonomika	ekonomika	k1gFnSc1	ekonomika
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
nárůst	nárůst	k1gInSc4	nárůst
<g/>
.	.	kIx.	.
<g/>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
světové	světový	k2eAgNnSc4d1	světové
dění	dění	k1gNnSc4	dění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
je	být	k5eAaImIp3nS	být
svět	svět	k1gInSc1	svět
často	často	k6eAd1	často
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
unipolární	unipolární	k2eAgFnSc4d1	unipolární
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
považovanými	považovaný	k2eAgInPc7d1	považovaný
za	za	k7c4	za
hegemona	hegemon	k1gMnSc4	hegemon
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
určila	určit	k5eAaPmAgFnS	určit
roli	role	k1gFnSc4	role
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
měly	mít	k5eAaImAgInP	mít
válečnou	válečný	k2eAgFnSc4d1	válečná
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
50	[number]	k4	50
státy	stát	k1gInPc7	stát
a	a	k8xC	a
526	[number]	k4	526
000	[number]	k4	000
jejich	jejich	k3xOp3gFnPc2	jejich
jednotek	jednotka	k1gFnPc2	jednotka
bylo	být	k5eAaImAgNnS	být
umístěných	umístěný	k2eAgFnPc2d1	umístěná
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
350	[number]	k4	350
000	[number]	k4	000
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
130	[number]	k4	130
000	[number]	k4	000
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc3d1	jižní
Koreji	Korea	k1gFnSc3	Korea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
také	také	k9	také
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
vrchol	vrchol	k1gInSc4	vrchol
vojensko-průmyslových	vojenskorůmyslový	k2eAgInPc2d1	vojensko-průmyslový
komplexů	komplex	k1gInPc2	komplex
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
financování	financování	k1gNnSc3	financování
vědy	věda	k1gFnSc2	věda
armádou	armáda	k1gFnSc7	armáda
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
<g/>
Armádní	armádní	k2eAgInPc1d1	armádní
výdaje	výdaj	k1gInPc1	výdaj
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
jsou	být	k5eAaImIp3nP	být
odhadovány	odhadovat	k5eAaImNgFnP	odhadovat
na	na	k7c4	na
8	[number]	k4	8
bilionů	bilion	k4xCgInPc2	bilion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
000	[number]	k4	000
Američanů	Američan	k1gMnPc2	Američan
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
Korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
a	a	k8xC	a
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
ztrát	ztráta	k1gFnPc2	ztráta
sovětských	sovětský	k2eAgMnPc2d1	sovětský
vojáků	voják	k1gMnPc2	voják
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podílem	podíl	k1gInSc7	podíl
hrubého	hrubý	k2eAgInSc2d1	hrubý
národního	národní	k2eAgInSc2d1	národní
produktu	produkt	k1gInSc2	produkt
byly	být	k5eAaImAgInP	být
výdaje	výdaj	k1gInPc1	výdaj
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
výdaje	výdaj	k1gInPc1	výdaj
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
zemřely	zemřít	k5eAaPmAgFnP	zemřít
během	běh	k1gInSc7	běh
zástupných	zástupný	k2eAgFnPc2d1	zástupná
válek	válka	k1gFnPc2	válka
miliony	milion	k4xCgInPc7	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
<g/>
Mocenský	mocenský	k2eAgInSc1d1	mocenský
převrat	převrat	k1gInSc1	převrat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
vyústil	vyústit	k5eAaPmAgMnS	vyústit
v	v	k7c4	v
nové	nový	k2eAgInPc4d1	nový
občanské	občanský	k2eAgInPc4d1	občanský
a	a	k8xC	a
etnické	etnický	k2eAgInPc4d1	etnický
konflikty	konflikt	k1gInPc4	konflikt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c4	v
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
liberální	liberální	k2eAgFnPc1d1	liberální
demokracie	demokracie	k1gFnPc1	demokracie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
státy	stát	k1gInPc1	stát
zhroutily	zhroutit	k5eAaPmAgInP	zhroutit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgNnSc1d1	kulturní
zobrazení	zobrazení	k1gNnSc1	zobrazení
===	===	k?	===
</s>
</p>
<p>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
tématem	téma	k1gNnSc7	téma
deskové	deskový	k2eAgFnSc2d1	desková
hry	hra	k1gFnSc2	hra
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
historicky	historicky	k6eAd1	historicky
hráči	hráč	k1gMnPc7	hráč
nejlépe	dobře	k6eAd3	dobře
hodnoceným	hodnocený	k2eAgFnPc3d1	hodnocená
deskovým	deskový	k2eAgFnPc3d1	desková
hrám	hra	k1gFnPc3	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historiografie	historiografie	k1gFnSc2	historiografie
==	==	k?	==
</s>
</p>
<p>
<s>
Historici	historik	k1gMnPc1	historik
běžně	běžně	k6eAd1	běžně
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
interpretačních	interpretační	k2eAgNnPc6d1	interpretační
stádiích	stádium	k1gNnPc6	stádium
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
tradicionalisty	tradicionalista	k1gMnSc2	tradicionalista
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
revizionisty	revizionista	k1gMnPc7	revizionista
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
postrevizionisty	postrevizionista	k1gMnSc2	postrevizionista
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Tradicionalisté	tradicionalista	k1gMnPc1	tradicionalista
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
studenou	studený	k2eAgFnSc4d1	studená
válku	válka	k1gFnSc4	válka
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc3	jeho
rozpínání	rozpínání	k1gNnSc1	rozpínání
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Revizionisté	revizionista	k1gMnPc1	revizionista
přisuzují	přisuzovat	k5eAaImIp3nP	přisuzovat
více	hodně	k6eAd2	hodně
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
a	a	k8xC	a
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
jejich	jejich	k3xOp3gFnSc4	jejich
snahu	snaha	k1gFnSc4	snaha
o	o	k7c6	o
izolaci	izolace	k1gFnSc6	izolace
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
už	už	k9	už
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Postrevizionisté	Postrevizionista	k1gMnPc1	Postrevizionista
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c4	o
syntézu	syntéza	k1gFnSc4	syntéza
předchozích	předchozí	k2eAgFnPc2d1	předchozí
dvou	dva	k4xCgFnPc2	dva
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cold	Cold	k1gMnSc1	Cold
War	War	k1gMnSc1	War
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Česky	Česko	k1gNnPc7	Česko
====	====	k?	====
</s>
</p>
<p>
<s>
ANDREW	ANDREW	kA	ANDREW
<g/>
,	,	kIx,	,
Christopher	Christophra	k1gFnPc2	Christophra
<g/>
;	;	kIx,	;
MITROCHIN	MITROCHIN	kA	MITROCHIN
<g/>
,	,	kIx,	,
Vasilij	Vasilij	k1gMnSc1	Vasilij
Nikitič	Nikitič	k1gMnSc1	Nikitič
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
KGB	KGB	kA	KGB
a	a	k8xC	a
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
:	:	kIx,	:
Mitrochinův	Mitrochinův	k2eAgInSc1d1	Mitrochinův
archiv	archiv	k1gInSc1	archiv
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rozmluvy	rozmluva	k1gFnSc2	rozmluva
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
502	[number]	k4	502
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7335	[number]	k4	7335
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DRULÁK	DRULÁK	kA	DRULÁK
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Metafory	metafora	k1gFnPc1	metafora
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
:	:	kIx,	:
interpretace	interpretace	k1gFnSc2	interpretace
politického	politický	k2eAgInSc2d1	politický
fenoménu	fenomén	k1gInSc2	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
293	[number]	k4	293
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7367	[number]	k4	7367
<g/>
-	-	kIx~	-
<g/>
594	[number]	k4	594
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GADDIS	GADDIS	kA	GADDIS
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
278	[number]	k4	278
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7209	[number]	k4	7209
<g/>
-	-	kIx~	-
<g/>
843	[number]	k4	843
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOURA	KOURA	k?	KOURA
<g/>
,	,	kIx,	,
JAN	Jan	k1gMnSc1	Jan
<g/>
:	:	kIx,	:
Zápas	zápas	k1gInSc1	zápas
o	o	k7c4	o
východní	východní	k2eAgNnPc4d1	východní
Středomoří	středomoří	k1gNnPc4	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
vůči	vůči	k7c3	vůči
Řecku	Řecko	k1gNnSc3	Řecko
a	a	k8xC	a
Turecku	Turecko	k1gNnSc3	Turecko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-468-4	[number]	k4	978-80-7308-468-4
</s>
</p>
<p>
<s>
LUŇÁK	Luňák	k1gMnSc1	Luňák
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
<g/>
:	:	kIx,	:
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Západní	západní	k2eAgFnPc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
460	[number]	k4	460
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
MASTNÝ	mastný	k2eAgMnSc1d1	mastný
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
sovětský	sovětský	k2eAgInSc1d1	sovětský
pocit	pocit	k1gInSc1	pocit
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
:	:	kIx,	:
1947	[number]	k4	1947
<g/>
-	-	kIx~	-
<g/>
53	[number]	k4	53
<g/>
,	,	kIx,	,
Stalinova	Stalinův	k2eAgNnPc4d1	Stalinovo
léta	léto	k1gNnPc4	léto
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Aurora	Aurora	k1gFnSc1	Aurora
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
294	[number]	k4	294
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7299	[number]	k4	7299
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
49	[number]	k4	49
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NÁLEVKA	nálevka	k1gFnSc1	nálevka
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Kapitoly	kapitola	k1gFnPc4	kapitola
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
středoevropskou	středoevropský	k2eAgFnSc4d1	středoevropská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
146	[number]	k4	146
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86130	[number]	k4	86130
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NÁLEVKA	nálevka	k1gFnSc1	nálevka
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Karibská	karibský	k2eAgFnSc1d1	karibská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
ISV	ISV	kA	ISV
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
241	[number]	k4	241
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85866	[number]	k4	85866
<g/>
-	-	kIx~	-
<g/>
89	[number]	k4	89
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NÁLEVKA	nálevka	k1gFnSc1	nálevka
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
234	[number]	k4	234
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
327	[number]	k4	327
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
NÁLEVKA	nálevka	k1gFnSc1	nálevka
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgFnSc1d1	Horké
krize	krize	k1gFnSc1	krize
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PACNER	PACNER	kA	PACNER
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Atomoví	atomový	k2eAgMnPc1d1	atomový
vyzvědači	vyzvědač	k1gMnPc1	vyzvědač
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Epocha	epocha	k1gFnSc1	epocha
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
501	[number]	k4	501
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7425	[number]	k4	7425
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
REIMAN	REIMAN	kA	REIMAN
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
;	;	kIx,	;
LUŇÁK	Luňák	k1gMnSc1	Luňák
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
:	:	kIx,	:
sovětské	sovětský	k2eAgInPc1d1	sovětský
dokumenty	dokument	k1gInPc1	dokument
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
archivech	archiv	k1gInPc6	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Doplněk	doplněk	k1gInSc1	doplněk
;	;	kIx,	;
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
soudobé	soudobý	k2eAgFnPc4d1	soudobá
dějiny	dějiny	k1gFnPc4	dějiny
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
439	[number]	k4	439
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7239	[number]	k4	7239
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7285	[number]	k4	7285
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ULVR	ULVR	kA	ULVR
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Nukleární	nukleární	k2eAgFnSc1d1	nukleární
společnost	společnost	k1gFnSc1	společnost
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7308-496-7	[number]	k4	978-80-7308-496-7
</s>
</p>
<p>
<s>
WANNER	WANNER	kA	WANNER
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Suez	Suez	k1gInSc4	Suez
<g/>
:	:	kIx,	:
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
arabsko-izraelský	arabskozraelský	k2eAgInSc1d1	arabsko-izraelský
konflikt	konflikt	k1gInSc1	konflikt
a	a	k8xC	a
britsko-francouzská	britskorancouzský	k2eAgFnSc1d1	britsko-francouzská
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
560	[number]	k4	560
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
298	[number]	k4	298
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Anglicky	Anglicko	k1gNnPc7	Anglicko
====	====	k?	====
</s>
</p>
<p>
<s>
GADDIS	GADDIS	kA	GADDIS
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lewis	Lewis	k1gFnSc2	Lewis
<g/>
.	.	kIx.	.
</s>
<s>
Russia	Russia	k1gFnSc1	Russia
<g/>
,	,	kIx,	,
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
Union	union	k1gInSc1	union
and	and	k?	and
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
<g/>
.	.	kIx.	.
</s>
<s>
An	An	k?	An
Interpretative	Interpretativ	k1gInSc5	Interpretativ
History	Histor	k1gMnPc7	Histor
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
McGraw-Hill	McGraw-Hill	k1gInSc1	McGraw-Hill
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
75572583	[number]	k4	75572583
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GEIGER	GEIGER	kA	GEIGER
<g/>
,	,	kIx,	,
Till	Till	k1gInSc1	Till
<g/>
.	.	kIx.	.
</s>
<s>
Britain	Britain	k1gInSc1	Britain
and	and	k?	and
the	the	k?	the
Economic	Economice	k1gFnPc2	Economice
Problem	Probl	k1gMnSc7	Probl
of	of	k?	of
the	the	k?	the
Cold	Cold	k1gMnSc1	Cold
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
</s>
<s>
Farnham	Farnham	k1gInSc4	Farnham
<g/>
:	:	kIx,	:
Ashgate	Ashgat	k1gMnSc5	Ashgat
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
754602877	[number]	k4	754602877
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KORT	KORT	kA	KORT
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Columbia	Columbia	k1gFnSc1	Columbia
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Cold	Coldo	k1gNnPc2	Coldo
War	War	k1gMnPc2	War
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
231107730	[number]	k4	231107730
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SMITH	SMITH	kA	SMITH
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Cold	Cold	k1gMnSc1	Cold
War	War	k1gMnSc1	War
<g/>
:	:	kIx,	:
1945	[number]	k4	1945
-	-	kIx~	-
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Hoboken	Hoboken	k1gInSc1	Hoboken
<g/>
:	:	kIx,	:
Wiley-Blackwell	Wiley-Blackwell	k1gInSc1	Wiley-Blackwell
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
631191380	[number]	k4	631191380
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SAKWA	SAKWA	kA	SAKWA
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
rise	risat	k5eAaPmIp3nS	risat
and	and	k?	and
fall	fall	k1gInSc1	fall
of	of	k?	of
the	the	k?	the
Soviet	Soviet	k1gInSc1	Soviet
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gNnSc1	Routledge
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
415122902	[number]	k4	415122902
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
aliance	aliance	k1gFnSc1	aliance
</s>
</p>
<p>
<s>
Mccarthismus	mccarthismus	k1gInSc1	mccarthismus
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Ruska	Rusko	k1gNnSc2	Rusko
</s>
</p>
<p>
<s>
Komunismus	komunismus	k1gInSc1	komunismus
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Portál	portál	k1gInSc1	portál
o	o	k7c6	o
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cold	Cold	k1gInSc1	Cold
War	War	k1gFnSc2	War
na	na	k7c6	na
Britannice	Britannika	k1gFnSc6	Britannika
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cold	Cold	k1gInSc1	Cold
War	War	k1gFnSc2	War
na	na	k7c6	na
BBC	BBC	kA	BBC
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cold	Cold	k1gInSc1	Cold
War	War	k1gFnPc2	War
Museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
Pořad	pořad	k1gInSc1	pořad
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Plus	plus	k1gNnSc1	plus
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zbavil	zbavit	k5eAaPmAgMnS	zbavit
Evropu	Evropa	k1gFnSc4	Evropa
rovnováhy	rovnováha	k1gFnSc2	rovnováha
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
podrobně	podrobně	k6eAd1	podrobně
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
koncem	koncem	k7c2	koncem
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
z	z	k7c2	z
perspektivy	perspektiva	k1gFnSc2	perspektiva
Sovetského	Sovetský	k2eAgInSc2d1	Sovetský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
sleduje	sledovat	k5eAaImIp3nS	sledovat
aktivity	aktivita	k1gFnPc4	aktivita
Eduarda	Eduard	k1gMnSc2	Eduard
Ševardnadzeho	Ševardnadze	k1gMnSc2	Ševardnadze
<g/>
.	.	kIx.	.
<g/>
Archivy	archiv	k1gInPc1	archiv
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Open	Open	k1gNnSc1	Open
Society	societa	k1gFnPc1	societa
Archives	Archives	k1gMnSc1	Archives
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CBC	CBC	kA	CBC
Digital	Digital	kA	Digital
Archives	Archives	k1gMnSc1	Archives
–	–	k?	–
Cold	Cold	k1gMnSc1	Cold
War	War	k1gMnSc1	War
Culture	Cultur	k1gMnSc5	Cultur
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Nuclear	Nuclear	k1gMnSc1	Nuclear
Fear	Fear	k1gMnSc1	Fear
of	of	k?	of
the	the	k?	the
1950	[number]	k4	1950
<g/>
s	s	k7c7	s
and	and	k?	and
1960	[number]	k4	1960
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnPc1	The
Cold	Cold	k1gInSc4	Cold
War	War	k1gMnPc2	War
International	International	k1gFnSc2	International
History	Histor	k1gInPc1	Histor
Project	Projecta	k1gFnPc2	Projecta
(	(	kIx(	(
<g/>
CWIHP	CWIHP	kA	CWIHP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Cold	Colda	k1gFnPc2	Colda
War	War	k1gMnSc2	War
FilesZprávy	FilesZpráva	k1gFnSc2	FilesZpráva
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zprávy	zpráva	k1gFnPc1	zpráva
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
během	během	k7c2	během
studené	studený	k2eAgFnSc2d1	studená
válkyTerminologie	válkyTerminologie	k1gFnSc2	válkyTerminologie
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
You	You	k1gFnSc1	You
and	and	k?	and
the	the	k?	the
Atom	atom	k1gInSc1	atom
Bomb	bomba	k1gFnPc2	bomba
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Kompletní	kompletní	k2eAgInSc1d1	kompletní
esej	esej	k1gInSc1	esej
George	Georg	k1gMnSc2	Georg
Orwella	Orwell	k1gMnSc2	Orwell
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
</s>
</p>
