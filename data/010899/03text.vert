<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Man	mana	k1gFnPc2	mana
(	(	kIx(	(
<g/>
také	také	k9	také
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
v	v	k7c6	v
manštině	manština	k1gFnSc6	manština
Mannin	Mannin	k1gInSc1	Mannin
<g/>
)	)	kIx)	)
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Irském	irský	k2eAgNnSc6d1	irské
moři	moře	k1gNnSc6	moře
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
Skotskem	Skotsko	k1gNnSc7	Skotsko
<g/>
,	,	kIx,	,
Irskem	Irsko	k1gNnSc7	Irsko
a	a	k8xC	a
Walesem	Wales	k1gInSc7	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozměry	rozměr	k1gInPc4	rozměr
asi	asi	k9	asi
48	[number]	k4	48
<g/>
×	×	k?	×
<g/>
16	[number]	k4	16
km	km	kA	km
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
572	[number]	k4	572
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Man	Man	k1gMnSc1	Man
je	být	k5eAaImIp3nS	být
samosprávným	samosprávný	k2eAgMnSc7d1	samosprávný
britským	britský	k2eAgMnSc7d1	britský
korunním	korunní	k2eAgMnSc7d1	korunní
závislým	závislý	k2eAgMnSc7d1	závislý
územím	území	k1gNnSc7	území
(	(	kIx(	(
<g/>
British	British	k1gInSc1	British
Crown	Crowna	k1gFnPc2	Crowna
Dependency	Dependenca	k1gFnSc2	Dependenca
<g/>
)	)	kIx)	)
Ostrov	ostrov	k1gInSc1	ostrov
Man	mana	k1gFnPc2	mana
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Isle	Isl	k1gMnSc2	Isl
of	of	k?	of
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
mansky	mansky	k6eAd1	mansky
Ellan	Ellan	k1gInSc1	Ellan
Vannin	Vannina	k1gFnPc2	Vannina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
není	být	k5eNaImIp3nS	být
integrální	integrální	k2eAgFnSc7d1	integrální
součástí	součást	k1gFnSc7	součást
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Douglas	Douglasa	k1gFnPc2	Douglasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Vikingové	Viking	k1gMnPc1	Viking
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Norsemen	Norsemen	k2eAgInSc1d1	Norsemen
<g/>
)	)	kIx)	)
osídlili	osídlit	k5eAaPmAgMnP	osídlit
ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
na	na	k7c6	na
konci	konec	k1gInSc6	konec
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1079	[number]	k4	1079
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
vikinsko-galský	vikinskoalský	k2eAgMnSc1d1	vikinsko-galský
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Godred	Godred	k1gMnSc1	Godred
Crovan	Crovan	k1gMnSc1	Crovan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
již	již	k6eAd1	již
byl	být	k5eAaImAgMnS	být
vládcem	vládce	k1gMnSc7	vládce
Království	království	k1gNnSc2	království
dublinského	dublinský	k2eAgNnSc2d1	Dublinské
(	(	kIx(	(
<g/>
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
Dublin	Dublin	k1gInSc1	Dublin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Království	království	k1gNnSc1	království
Mann	Mann	k1gMnSc1	Mann
a	a	k8xC	a
Ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
Mann	Mann	k1gMnSc1	Mann
and	and	k?	and
the	the	k?	the
Isles	Isles	k1gInSc1	Isles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Svatého	svatý	k2eAgNnSc2d1	svaté
Patricka	Patricko	k1gNnSc2	Patricko
a	a	k8xC	a
král	král	k1gMnSc1	král
sídlil	sídlit	k5eAaImAgMnS	sídlit
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Peel	Peela	k1gFnPc2	Peela
(	(	kIx(	(
<g/>
Peel	Peel	k1gInSc1	Peel
Castle	Castle	k1gFnSc2	Castle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
malému	malý	k2eAgNnSc3d1	malé
království	království	k1gNnSc3	království
patřily	patřit	k5eAaImAgFnP	patřit
kromě	kromě	k7c2	kromě
ostrova	ostrov	k1gInSc2	ostrov
Man	mana	k1gFnPc2	mana
další	další	k2eAgInPc1d1	další
ostrovy	ostrov	k1gInPc1	ostrov
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
od	od	k7c2	od
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1164	[number]	k4	1164
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
království	království	k1gNnPc4	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Godred	Godred	k1gMnSc1	Godred
Crovan	Crovan	k1gMnSc1	Crovan
<g/>
,	,	kIx,	,
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
král	král	k1gMnSc1	král
Orry	Orra	k1gFnSc2	Orra
nebo	nebo	k8xC	nebo
Gorry	Gorra	k1gFnSc2	Gorra
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1095	[number]	k4	1095
na	na	k7c4	na
mor	mor	k1gInSc4	mor
<g/>
.	.	kIx.	.
</s>
<s>
Zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
tři	tři	k4xCgFnPc4	tři
syny	syn	k1gMnPc7	syn
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jména	jméno	k1gNnPc1	jméno
byla	být	k5eAaImAgNnP	být
Lagmann	Lagmann	k1gInSc4	Lagmann
<g/>
,	,	kIx,	,
Olaf	Olaf	k1gInSc4	Olaf
I.	I.	kA	I.
a	a	k8xC	a
Harald	Harald	k1gMnSc1	Harald
<g/>
.	.	kIx.	.
</s>
<s>
Lagmann	Lagmann	k1gMnSc1	Lagmann
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
Haraldovi	Harald	k1gMnSc3	Harald
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
jej	on	k3xPp3gMnSc4	on
oslepit	oslepit	k5eAaPmF	oslepit
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
Lagmanna	Lagmanno	k1gNnSc2	Lagmanno
a	a	k8xC	a
Olafa	Olaf	k1gMnSc4	Olaf
I.	I.	kA	I.
vládli	vládnout	k5eAaImAgMnP	vládnout
pak	pak	k6eAd1	pak
nad	nad	k7c7	nad
královstvím	království	k1gNnSc7	království
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
jistý	jistý	k2eAgInSc1d1	jistý
Somerled	Somerled	k1gInSc1	Somerled
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
tohoto	tento	k3xDgMnSc4	tento
muže	muž	k1gMnSc4	muž
vládl	vládnout	k5eAaImAgMnS	vládnout
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1265	[number]	k4	1265
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ostrova	ostrov	k1gInSc2	ostrov
Man	mana	k1gFnPc2	mana
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
skotský	skotský	k2eAgMnSc1d1	skotský
král	král	k1gMnSc1	král
Alexandr	Alexandr	k1gMnSc1	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1275	[number]	k4	1275
se	se	k3xPyFc4	se
Godred	Godred	k1gInSc1	Godred
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
posledního	poslední	k2eAgMnSc2d1	poslední
krále	král	k1gMnSc2	král
malé	malý	k2eAgFnSc2d1	malá
ostrovní	ostrovní	k2eAgFnSc2d1	ostrovní
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Království	království	k1gNnSc1	království
Mann	Mann	k1gMnSc1	Mann
a	a	k8xC	a
Ostrovů	ostrov	k1gInPc2	ostrov
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
vikinské	vikinský	k2eAgFnSc2d1	vikinská
nadvlády	nadvláda	k1gFnSc2	nadvláda
formálně	formálně	k6eAd1	formálně
součástí	součást	k1gFnSc7	součást
Norského	norský	k2eAgNnSc2d1	norské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1266	[number]	k4	1266
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
norský	norský	k2eAgMnSc1d1	norský
král	král	k1gMnSc1	král
Magnus	Magnus	k1gMnSc1	Magnus
VI	VI	kA	VI
<g/>
.	.	kIx.	.
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Perthu	Perth	k1gInSc2	Perth
všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
ostrovy	ostrov	k1gInPc4	ostrov
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1284	[number]	k4	1284
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
Alexandr	Alexandr	k1gMnSc1	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
hlavu	hlava	k1gFnSc4	hlava
rodu	rod	k1gInSc2	rod
(	(	kIx(	(
<g/>
klanu	klan	k1gInSc2	klan
<g/>
)	)	kIx)	)
Macdonaldů	Macdonald	k1gMnPc2	Macdonald
Anguse	Anguse	k1gFnSc2	Anguse
Macdonalda	Macdonald	k1gMnSc2	Macdonald
(	(	kIx(	(
<g/>
zvaného	zvaný	k2eAgMnSc2d1	zvaný
též	též	k9	též
Aonghas	Aonghas	k1gInSc1	Aonghas
Mór	móra	k1gFnPc2	móra
<g/>
)	)	kIx)	)
pánem	pán	k1gMnSc7	pán
Ostrova	ostrov	k1gInSc2	ostrov
Man	mana	k1gFnPc2	mana
a	a	k8xC	a
Vnějších	vnější	k2eAgFnPc2d1	vnější
Hebrid	Hebridy	k1gFnPc2	Hebridy
<g/>
.	.	kIx.	.
</s>
<s>
Vůdcové	vůdce	k1gMnPc1	vůdce
klanu	klan	k1gInSc2	klan
Macdonaldů	Macdonald	k1gMnPc2	Macdonald
po	po	k7c4	po
další	další	k2eAgNnPc4d1	další
dvě	dva	k4xCgNnPc4	dva
staletí	staletí	k1gNnPc4	staletí
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
jako	jako	k8xC	jako
téměř	téměř	k6eAd1	téměř
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
vládci	vládce	k1gMnPc1	vládce
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
stavěli	stavět	k5eAaImAgMnP	stavět
proti	proti	k7c3	proti
skotskému	skotský	k2eAgMnSc3d1	skotský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
spory	spora	k1gFnPc1	spora
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Skotska	Skotsko	k1gNnSc2	Skotsko
o	o	k7c4	o
ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
střídavé	střídavý	k2eAgFnSc2d1	střídavá
nadvlády	nadvláda	k1gFnSc2	nadvláda
jedné	jeden	k4xCgFnSc2	jeden
nebo	nebo	k8xC	nebo
druhé	druhý	k4xOgFnSc2	druhý
země	zem	k1gFnSc2	zem
přešel	přejít	k5eAaPmAgInS	přejít
ostrov	ostrov	k1gInSc4	ostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1399	[number]	k4	1399
definitivně	definitivně	k6eAd1	definitivně
pod	pod	k7c4	pod
anglickou	anglický	k2eAgFnSc4d1	anglická
kontrolu	kontrola	k1gFnSc4	kontrola
(	(	kIx(	(
<g/>
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
feudal	feudat	k5eAaImAgMnS	feudat
lordship	lordship	k1gMnSc1	lordship
anglické	anglický	k2eAgFnSc2d1	anglická
koruny	koruna	k1gFnSc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
státoprávních	státoprávní	k2eAgFnPc2d1	státoprávní
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
britským	britský	k2eAgNnSc7d1	Britské
závislým	závislý	k2eAgNnSc7d1	závislé
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
obou	dva	k4xCgFnPc2	dva
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Man	mana	k1gFnPc2	mana
zřízeny	zřízen	k2eAgInPc1d1	zřízen
cizinecké	cizinecký	k2eAgInPc1d1	cizinecký
internační	internační	k2eAgInPc1d1	internační
tábory	tábor	k1gInPc1	tábor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
území	území	k1gNnSc2	území
ostrova	ostrov	k1gInSc2	ostrov
Man	mana	k1gFnPc2	mana
-	-	kIx~	-
oblast	oblast	k1gFnSc1	oblast
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
457	[number]	k4	457
000	[number]	k4	000
ha	ha	kA	ha
-	-	kIx~	-
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
biosférické	biosférický	k2eAgFnPc4d1	biosférická
rezervace	rezervace	k1gFnPc4	rezervace
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Man	Man	k1gMnSc1	Man
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
evropské	evropský	k2eAgFnSc2d1	Evropská
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Irském	irský	k2eAgNnSc6d1	irské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
uprostřed	uprostřed	k6eAd1	uprostřed
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
Skotskem	Skotsko	k1gNnSc7	Skotsko
a	a	k8xC	a
Severním	severní	k2eAgNnSc7d1	severní
Irskem	Irsko	k1gNnSc7	Irsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
asi	asi	k9	asi
48	[number]	k4	48
km	km	kA	km
a	a	k8xC	a
široký	široký	k2eAgInSc1d1	široký
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kopce	kopec	k1gInPc1	kopec
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
jsou	být	k5eAaImIp3nP	být
rozpůleny	rozpůlit	k5eAaPmNgInP	rozpůlit
údolím	údolí	k1gNnSc7	údolí
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
sever	sever	k1gInSc1	sever
je	být	k5eAaImIp3nS	být
výjimečně	výjimečně	k6eAd1	výjimečně
plochý	plochý	k2eAgInSc1d1	plochý
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
glaciálními	glaciální	k2eAgInPc7d1	glaciální
depozity	depozit	k1gInPc7	depozit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Snaefell	Snaefella	k1gFnPc2	Snaefella
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
621	[number]	k4	621
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
vrcholu	vrchol	k1gInSc2	vrchol
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
šest	šest	k4xCc4	šest
království	království	k1gNnPc2	království
<g/>
:	:	kIx,	:
bývalé	bývalý	k2eAgNnSc1d1	bývalé
království	království	k1gNnSc1	království
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
království	království	k1gNnSc2	království
skotské	skotský	k2eAgFnSc2d1	skotská
<g/>
,	,	kIx,	,
anglické	anglický	k2eAgFnSc2d1	anglická
<g/>
,	,	kIx,	,
irské	irský	k2eAgFnSc2d1	irská
<g/>
,	,	kIx,	,
waleské	waleský	k2eAgFnSc2d1	Waleská
a	a	k8xC	a
království	království	k1gNnPc4	království
nebeské	nebeský	k2eAgFnPc4d1	nebeská
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
verze	verze	k1gFnPc1	verze
ještě	ještě	k9	ještě
přidávají	přidávat	k5eAaImIp3nP	přidávat
sedmé	sedmý	k4xOgNnSc4	sedmý
království	království	k1gNnSc4	království
–	–	k?	–
království	království	k1gNnSc2	království
mořské	mořský	k2eAgFnSc2d1	mořská
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
království	království	k1gNnSc1	království
Neptunovo	Neptunův	k2eAgNnSc1d1	Neptunovo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Man	mana	k1gFnPc2	mana
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
80	[number]	k4	80
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
80	[number]	k4	80
058	[number]	k4	058
dle	dle	k7c2	dle
cenzu	cenzus	k1gInSc2	cenzus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
přibližně	přibližně	k6eAd1	přibližně
26	[number]	k4	26
tisíc	tisíc	k4xCgInPc2	tisíc
(	(	kIx(	(
<g/>
26	[number]	k4	26
218	[number]	k4	218
dle	dle	k7c2	dle
cenzu	cenzus	k1gInSc2	cenzus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Douglas	Douglasa	k1gFnPc2	Douglasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
140	[number]	k4	140
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
kilometr	kilometr	k1gInSc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
rodilý	rodilý	k2eAgMnSc1d1	rodilý
mluvčí	mluvčí	k1gMnSc1	mluvčí
<g/>
,	,	kIx,	,
hovořící	hovořící	k2eAgFnSc6d1	hovořící
manštinou	manština	k1gFnSc7	manština
sice	sice	k8xC	sice
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
r.	r.	kA	r.
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
však	však	k9	však
jazyk	jazyk	k1gInSc1	jazyk
znovu	znovu	k6eAd1	znovu
vyučován	vyučovat	k5eAaImNgInS	vyučovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Chov	chov	k1gInSc1	chov
dobytka	dobytek	k1gInSc2	dobytek
(	(	kIx(	(
<g/>
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
(	(	kIx(	(
<g/>
325	[number]	k4	325
tisíc	tisíc	k4xCgInPc2	tisíc
návštěvníků	návštěvník	k1gMnPc2	návštěvník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Man	mana	k1gFnPc2	mana
je	být	k5eAaImIp3nS	být
samosprávné	samosprávný	k2eAgNnSc1d1	samosprávné
závislé	závislý	k2eAgNnSc1d1	závislé
území	území	k1gNnSc1	území
britské	britský	k2eAgFnSc2d1	britská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnu	měna	k1gFnSc4	měna
(	(	kIx(	(
<g/>
manská	manský	k2eAgFnSc1d1	manská
libra	libra	k1gFnSc1	libra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
parlament	parlament	k1gInSc1	parlament
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
autonomií	autonomie	k1gFnSc7	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
Lord	lord	k1gMnSc1	lord
of	of	k?	of
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1765	[number]	k4	1765
britský	britský	k2eAgMnSc1d1	britský
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Panovníka	panovník	k1gMnSc4	panovník
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
guvernér	guvernér	k1gMnSc1	guvernér
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
Lieutenant	Lieutenant	k1gMnSc1	Lieutenant
Governor	Governor	k1gMnSc1	Governor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
obranu	obrana	k1gFnSc4	obrana
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
zastupování	zastupování	k1gNnSc1	zastupování
a	a	k8xC	a
"	"	kIx"	"
<g/>
dobrou	dobrý	k2eAgFnSc4d1	dobrá
správu	správa	k1gFnSc4	správa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Tynwald	Tynwald	k1gInSc1	Tynwald
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pravomoci	pravomoc	k1gFnSc2	pravomoc
prakticky	prakticky	k6eAd1	prakticky
nad	nad	k7c7	nad
všemi	všecek	k3xTgFnPc7	všecek
domácími	domácí	k2eAgFnPc7d1	domácí
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tynwald	Tynwaldo	k1gNnPc2	Tynwaldo
===	===	k?	===
</s>
</p>
<p>
<s>
Tynwald	Tynwald	k1gInSc1	Tynwald
<g/>
,	,	kIx,	,
parlament	parlament	k1gInSc1	parlament
ostrova	ostrov	k1gInSc2	ostrov
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
979	[number]	k4	979
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
nepřetržitých	přetržitý	k2eNgInPc2d1	nepřetržitý
parlamentů	parlament	k1gInPc2	parlament
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Každoroční	každoroční	k2eAgNnSc1d1	každoroční
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
národním	národní	k2eAgInSc6d1	národní
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Schůze	schůze	k1gFnSc1	schůze
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
koná	konat	k5eAaImIp3nS	konat
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
Tynwald	Tynwald	k1gMnSc1	Tynwald
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
podán	podán	k2eAgInSc4d1	podán
krátký	krátký	k2eAgInSc4d1	krátký
přehled	přehled	k1gInSc4	přehled
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
opatření	opatření	k1gNnPc2	opatření
přijatých	přijatý	k2eAgNnPc2d1	přijaté
za	za	k7c4	za
uplynulý	uplynulý	k2eAgInSc4d1	uplynulý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
opatření	opatření	k1gNnPc1	opatření
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
v	v	k7c6	v
manštině	manština	k1gFnSc6	manština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
House	house	k1gNnSc1	house
of	of	k?	of
Keys	Keysa	k1gFnPc2	Keysa
(	(	kIx(	(
<g/>
přímo	přímo	k6eAd1	přímo
volený	volený	k2eAgInSc1d1	volený
<g/>
)	)	kIx)	)
a	a	k8xC	a
Legislative	Legislativ	k1gInSc5	Legislativ
Council	Council	k1gInSc1	Council
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
voleni	volit	k5eAaImNgMnP	volit
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jmenováni	jmenován	k2eAgMnPc1d1	jmenován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
orgány	orgán	k1gInPc1	orgán
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
na	na	k7c6	na
společných	společný	k2eAgFnPc6d1	společná
schůzích	schůze	k1gFnPc6	schůze
jako	jako	k8xS	jako
Tynwald	Tynwald	k1gInSc4	Tynwald
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Exekutiva	exekutiva	k1gFnSc1	exekutiva
je	být	k5eAaImIp3nS	být
vykonávána	vykonávat	k5eAaImNgFnS	vykonávat
Radou	rada	k1gFnSc7	rada
ministrů	ministr	k1gMnPc2	ministr
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
ze	z	k7c2	z
členů	člen	k1gInPc2	člen
Tynwaldu	Tynwald	k1gInSc2	Tynwald
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
ostrova	ostrov	k1gInSc2	ostrov
Man	mana	k1gFnPc2	mana
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
obce	obec	k1gFnPc4	obec
neomezená	omezený	k2eNgFnSc1d1	neomezená
rychlost	rychlost	k1gFnSc1	rychlost
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
motorová	motorový	k2eAgNnPc4d1	motorové
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
ostrov	ostrov	k1gInSc4	ostrov
vybírá	vybírat	k5eAaImIp3nS	vybírat
štáb	štáb	k1gInSc4	štáb
BBC	BBC	kA	BBC
k	k	k7c3	k
natáčení	natáčení	k1gNnSc3	natáčení
populárního	populární	k2eAgInSc2d1	populární
pořadu	pořad	k1gInSc2	pořad
Top	topit	k5eAaImRp2nS	topit
Gear	Gear	k1gInSc4	Gear
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
hlavní	hlavní	k2eAgMnSc1d1	hlavní
představitel	představitel	k1gMnSc1	představitel
tohoto	tento	k3xDgInSc2	tento
pořadu	pořad	k1gInSc2	pořad
Jeremy	Jerema	k1gFnSc2	Jerema
Clarkson	Clarkson	k1gMnSc1	Clarkson
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
svého	svůj	k3xOyFgNnSc2	svůj
trvalého	trvalý	k2eAgNnSc2d1	trvalé
bydliště	bydliště	k1gNnSc2	bydliště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
legendární	legendární	k2eAgInSc4d1	legendární
TT-Tourist	TT-Tourist	k1gInSc4	TT-Tourist
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
závod	závod	k1gInSc4	závod
silničních	silniční	k2eAgInPc2d1	silniční
motocyklů	motocykl	k1gInPc2	motocykl
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
přírodním	přírodní	k2eAgInSc7d1	přírodní
okruhem	okruh	k1gInSc7	okruh
tvořeným	tvořený	k2eAgInSc7d1	tvořený
běžnými	běžný	k2eAgFnPc7d1	běžná
silnicemi	silnice	k1gFnPc7	silnice
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
37,73	[number]	k4	37,73
mil	míle	k1gFnPc2	míle
(	(	kIx(	(
<g/>
60	[number]	k4	60
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
rychlosti	rychlost	k1gFnPc1	rychlost
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
až	až	k9	až
210	[number]	k4	210
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
charakter	charakter	k1gInSc1	charakter
silnic	silnice	k1gFnPc2	silnice
činí	činit	k5eAaImIp3nS	činit
tento	tento	k3xDgInSc1	tento
závod	závod	k1gInSc1	závod
velmi	velmi	k6eAd1	velmi
nebezpečným	bezpečný	k2eNgFnPc3d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
230	[number]	k4	230
obětí	oběť	k1gFnPc2	oběť
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Závodu	závod	k1gInSc2	závod
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
také	také	k9	také
čeští	český	k2eAgMnPc1d1	český
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
např.	např.	kA	např.
František	František	k1gMnSc1	František
Šťastný	Šťastný	k1gMnSc1	Šťastný
nebo	nebo	k8xC	nebo
Václav	Václav	k1gMnSc1	Václav
Parus	Parus	k1gMnSc1	Parus
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
třeba	třeba	k6eAd1	třeba
Michal	Michal	k1gMnSc1	Michal
Dokoupil	Dokoupil	k1gMnSc1	Dokoupil
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
příznivce	příznivec	k1gMnPc4	příznivec
motocyklového	motocyklový	k2eAgInSc2d1	motocyklový
sportu	sport	k1gInSc2	sport
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kultovní	kultovní	k2eAgInSc4d1	kultovní
podnik	podnik	k1gInSc4	podnik
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
nejstarší	starý	k2eAgInSc1d3	nejstarší
a	a	k8xC	a
nejnebezpečnější	bezpečný	k2eNgInSc1d3	nejnebezpečnější
závod	závod	k1gInSc1	závod
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Man	mana	k1gFnPc2	mana
pocházejí	pocházet	k5eAaImIp3nP	pocházet
Barry	Barra	k1gFnSc2	Barra
<g/>
,	,	kIx,	,
Robin	Robina	k1gFnPc2	Robina
a	a	k8xC	a
Maurice	Maurika	k1gFnSc3	Maurika
Gibbovi	Gibba	k1gMnSc3	Gibba
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Bee	Bee	k1gFnSc2	Bee
Gees	Geesa	k1gFnPc2	Geesa
a	a	k8xC	a
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
silniční	silniční	k2eAgMnSc1d1	silniční
profesionální	profesionální	k2eAgMnSc1d1	profesionální
cyklista	cyklista	k1gMnSc1	cyklista
Mark	Mark	k1gMnSc1	Mark
Cavendish	Cavendish	k1gMnSc1	Cavendish
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Man	mana	k1gFnPc2	mana
žijí	žít	k5eAaImIp3nP	žít
bezocasé	bezocasý	k2eAgFnPc1d1	bezocasá
kočky	kočka	k1gFnPc1	kočka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Triskelion	Triskelion	k1gInSc1	Triskelion
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Man	mana	k1gFnPc2	mana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Man	mana	k1gFnPc2	mana
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
