<s>
Sněžka	Sněžka	k1gFnSc1	Sněžka
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Śnieżka	Śnieżka	k1gFnSc1	Śnieżka
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Schneekoppe	Schneekopp	k1gMnSc5	Schneekopp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
1603,30	[number]	k4	1603,30
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
uváděný	uváděný	k2eAgInSc1d1	uváděný
údaj	údaj	k1gInSc1	údaj
1602	[number]	k4	1602
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
je	být	k5eAaImIp3nS	být
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
trigonometrického	trigonometrický	k2eAgInSc2d1	trigonometrický
bodu	bod	k1gInSc2	bod
<g/>
)	)	kIx)	)
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Hraničního	hraniční	k2eAgInSc2d1	hraniční
(	(	kIx(	(
<g/>
Slezského	slezský	k2eAgInSc2d1	slezský
<g/>
)	)	kIx)	)
hřebenu	hřeben	k1gInSc2	hřeben
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
,	,	kIx,	,
Sudet	Sudety	k1gFnPc2	Sudety
<g/>
,	,	kIx,	,
Čech	Čechy	k1gFnPc2	Čechy
i	i	k8xC	i
celého	celý	k2eAgNnSc2d1	celé
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
celého	celý	k2eAgNnSc2d1	celé
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
významná	významný	k2eAgFnSc1d1	významná
dominanta	dominanta	k1gFnSc1	dominanta
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
vrchol	vrchol	k1gInSc4	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
prochází	procházet	k5eAaImIp3nS	procházet
česko-polská	českoolský	k2eAgFnSc1d1	česko-polská
hranice	hranice	k1gFnSc1	hranice
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
západně	západně	k6eAd1	západně
od	od	k7c2	od
kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
vede	vést	k5eAaImIp3nS	vést
ze	z	k7c2	z
4,5	[number]	k4	4,5
km	km	kA	km
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
Pece	Pec	k1gFnSc2	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
kabinková	kabinkový	k2eAgFnSc1d1	kabinková
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
strana	strana	k1gFnSc1	strana
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Lomničky	Lomnička	k1gFnSc2	Lomnička
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc2d1	západní
do	do	k7c2	do
Úpské	úpský	k2eAgFnSc2d1	Úpská
rašeliny	rašelina	k1gFnSc2	rašelina
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
část	část	k1gFnSc1	část
do	do	k7c2	do
Obřího	obří	k2eAgInSc2d1	obří
dolu	dol	k1gInSc2	dol
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Jeleního	jelení	k2eAgInSc2d1	jelení
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
přecházejí	přecházet	k5eAaImIp3nP	přecházet
v	v	k7c4	v
Obří	obří	k2eAgInSc4d1	obří
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholek	vrcholek	k1gInSc1	vrcholek
hory	hora	k1gFnSc2	hora
je	být	k5eAaImIp3nS	být
skalnatý	skalnatý	k2eAgMnSc1d1	skalnatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
okolo	okolo	k7c2	okolo
120	[number]	k4	120
000	[number]	k4	000
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Sněžka	Sněžka	k1gFnSc1	Sněžka
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
panoramatický	panoramatický	k2eAgInSc4d1	panoramatický
rozhled	rozhled	k1gInSc4	rozhled
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
častý	častý	k2eAgInSc1d1	častý
turistický	turistický	k2eAgInSc1d1	turistický
cíl	cíl	k1gInSc1	cíl
a	a	k8xC	a
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
dosažení	dosažení	k1gNnSc4	dosažení
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
turistických	turistický	k2eAgFnPc2d1	turistická
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
pěších	pěší	k2eAgFnPc6d1	pěší
<g/>
,	,	kIx,	,
cyklistických	cyklistický	k2eAgFnPc6d1	cyklistická
tak	tak	k6eAd1	tak
i	i	k8xC	i
běžkových	běžkových	k2eAgInSc1d1	běžkových
<g/>
.	.	kIx.	.
</s>
<s>
Georgius	Georgius	k1gMnSc1	Georgius
Agricola	Agricola	k1gFnSc1	Agricola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1546	[number]	k4	1546
označil	označit	k5eAaPmAgMnS	označit
horu	hora	k1gFnSc4	hora
názvem	název	k1gInSc7	název
Risenberg	Risenberg	k1gInSc1	Risenberg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
Riesengebirge	Riesengebirge	k1gFnSc1	Riesengebirge
(	(	kIx(	(
<g/>
Obří	obří	k2eAgFnSc1d1	obří
hory	hora	k1gFnPc1	hora
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
dosud	dosud	k6eAd1	dosud
užívaný	užívaný	k2eAgInSc1d1	užívaný
německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
dosud	dosud	k6eAd1	dosud
zanechal	zanechat	k5eAaPmAgMnS	zanechat
dozvuky	dozvuk	k1gInPc7	dozvuk
v	v	k7c6	v
dnešních	dnešní	k2eAgInPc6d1	dnešní
názvech	název	k1gInPc6	název
Obří	obří	k2eAgInSc4d1	obří
důl	důl	k1gInSc4	důl
a	a	k8xC	a
Obří	obří	k2eAgInSc4d1	obří
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Přibík	Přibík	k1gMnSc1	Přibík
Pulkava	Pulkava	k1gFnSc1	Pulkava
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
pro	pro	k7c4	pro
celé	celý	k2eAgFnPc4d1	celá
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
použil	použít	k5eAaPmAgInS	použít
název	název	k1gInSc1	název
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Sněžka	Sněžka	k1gFnSc1	Sněžka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
pojmenování	pojmenování	k1gNnSc2	pojmenování
Sněžná	sněžný	k2eAgFnSc1d1	sněžná
-	-	kIx~	-
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
sněhem	sníh	k1gInSc7	sníh
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
Pahrbek	pahrbek	k1gInSc4	pahrbek
Sněžný	sněžný	k2eAgInSc4d1	sněžný
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Sněžovka	Sněžovka	k1gFnSc1	Sněžovka
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
pak	pak	k6eAd1	pak
definitivně	definitivně	k6eAd1	definitivně
Sněžka	Sněžka	k1gFnSc1	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Narazit	narazit	k5eAaPmF	narazit
lze	lze	k6eAd1	lze
i	i	k9	i
na	na	k7c4	na
názvy	název	k1gInPc4	název
Sněhovka	sněhovka	k1gFnSc1	sněhovka
či	či	k8xC	či
doslovný	doslovný	k2eAgInSc1d1	doslovný
překlad	překlad	k1gInSc1	překlad
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
Sněhkopa	Sněhkop	k1gMnSc2	Sněhkop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
se	se	k3xPyFc4	se
zase	zase	k9	zase
kromě	kromě	k7c2	kromě
častějšího	častý	k2eAgInSc2d2	častější
Schneekoppe	Schneekopp	k1gInSc5	Schneekopp
objevoval	objevovat	k5eAaImAgInS	objevovat
i	i	k9	i
tvar	tvar	k1gInSc1	tvar
Riessenkoppe	Riessenkopp	k1gInSc5	Riessenkopp
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zaznamenaný	zaznamenaný	k2eAgInSc4d1	zaznamenaný
výstup	výstup	k1gInSc4	výstup
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1456	[number]	k4	1456
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jistý	jistý	k2eAgMnSc1d1	jistý
Benátčan	Benátčan	k1gMnSc1	Benátčan
hledal	hledat	k5eAaImAgMnS	hledat
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
drahé	drahý	k2eAgNnSc1d1	drahé
kamení	kamení	k1gNnSc1	kamení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1563	[number]	k4	1563
<g/>
-	-	kIx~	-
<g/>
1566	[number]	k4	1566
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
změřit	změřit	k5eAaPmF	změřit
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
hory	hora	k1gFnSc2	hora
slezský	slezský	k2eAgMnSc1d1	slezský
učenec	učenec	k1gMnSc1	učenec
Kryštof	Kryštof	k1gMnSc1	Kryštof
Schilling	schilling	k1gInSc4	schilling
<g/>
.	.	kIx.	.
</s>
<s>
Naměřil	naměřit	k5eAaBmAgMnS	naměřit
neuvěřitelných	uvěřitelný	k2eNgFnPc2d1	neuvěřitelná
5880	[number]	k4	5880
m	m	kA	m
(	(	kIx(	(
<g/>
o	o	k7c4	o
1000	[number]	k4	1000
metrů	metr	k1gInPc2	metr
více	hodně	k6eAd2	hodně
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1569	[number]	k4	1569
pan	pan	k1gMnSc1	pan
Jiřík	Jiřík	k1gMnSc1	Jiřík
z	z	k7c2	z
Řásně	řáseň	k1gFnSc2	řáseň
naměřil	naměřit	k5eAaBmAgMnS	naměřit
2035	[number]	k4	2035
m.	m.	k?	m.
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1935	[number]	k4	1935
vyjel	vyjet	k5eAaPmAgInS	vyjet
na	na	k7c4	na
Sněžku	Sněžka	k1gFnSc4	Sněžka
ve	v	k7c6	v
Škodě	škoda	k1gFnSc6	škoda
Popular	Popular	k1gMnSc1	Popular
Břetislav	Břetislav	k1gMnSc1	Břetislav
Jan	Jan	k1gMnSc1	Jan
Procházka	Procházka	k1gMnSc1	Procházka
a	a	k8xC	a
Mojmír	Mojmír	k1gMnSc1	Mojmír
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
však	však	k9	však
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
vyjel	vyjet	k5eAaPmAgMnS	vyjet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
již	již	k6eAd1	již
konstruktér	konstruktér	k1gMnSc1	konstruktér
Josef	Josef	k1gMnSc1	Josef
Bašek	Bašek	k1gMnSc1	Bašek
<g/>
,	,	kIx,	,
arci	arci	k0	arci
méně	málo	k6eAd2	málo
náročnou	náročný	k2eAgFnSc7d1	náročná
cestou	cesta	k1gFnSc7	cesta
po	po	k7c6	po
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
československo-německé	československoěmecký	k2eAgFnSc6d1	československo-německý
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
na	na	k7c6	na
Sněžce	Sněžka	k1gFnSc6	Sněžka
při	při	k7c6	při
seskoku	seskok	k1gInSc6	seskok
z	z	k7c2	z
rogala	rogalo	k1gNnSc2	rogalo
s	s	k7c7	s
wingsuitem	wingsuit	k1gInSc7	wingsuit
zabil	zabít	k5eAaPmAgMnS	zabít
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
český	český	k2eAgMnSc1d1	český
BASE	basa	k1gFnSc6	basa
jumper	jumper	k1gInSc1	jumper
Martin	Martina	k1gFnPc2	Martina
Trdla	trdlo	k1gNnSc2	trdlo
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rozlehlý	rozlehlý	k2eAgInSc1d1	rozlehlý
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc4d3	nejstarší
stavbu	stavba	k1gFnSc4	stavba
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
(	(	kIx(	(
<g/>
slezské	slezský	k2eAgFnSc6d1	Slezská
<g/>
)	)	kIx)	)
straně	strana	k1gFnSc6	strana
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
14	[number]	k4	14
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
<g/>
,	,	kIx,	,
o	o	k7c4	o
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
stavbu	stavba	k1gFnSc4	stavba
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
šlechtic	šlechtic	k1gMnSc1	šlechtic
Kryštof	Kryštof	k1gMnSc1	Kryštof
Leopold	Leopold	k1gMnSc1	Leopold
Schaffgotsch	Schaffgotsch	k1gMnSc1	Schaffgotsch
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1665	[number]	k4	1665
<g/>
-	-	kIx~	-
<g/>
1681	[number]	k4	1681
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
první	první	k4xOgFnSc1	první
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
začaly	začít	k5eAaPmAgInP	začít
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1653	[number]	k4	1653
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
ale	ale	k9	ale
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
kvůli	kvůli	k7c3	kvůli
majetkové	majetkový	k2eAgFnSc3d1	majetková
při	pře	k1gFnSc3	pře
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Humprechtem	Humprecht	k1gMnSc7	Humprecht
Černínem	Černín	k1gMnSc7	Černín
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
Sněžku	Sněžka	k1gFnSc4	Sněžka
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
panství	panství	k1gNnSc1	panství
Schmiedeberg	Schmiedeberg	k1gInSc1	Schmiedeberg
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Kowary	Kowara	k1gFnPc4	Kowara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
pozemky	pozemka	k1gFnPc4	pozemka
se	se	k3xPyFc4	se
táhl	táhnout	k5eAaImAgInS	táhnout
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
mohlo	moct	k5eAaImAgNnS	moct
začít	začít	k5eAaPmF	začít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1665	[number]	k4	1665
<g/>
.	.	kIx.	.
</s>
<s>
Bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgFnP	konat
pětkrát	pětkrát	k6eAd1	pětkrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
hospoda	hospoda	k?	hospoda
a	a	k8xC	a
útulek	útulek	k1gInSc4	útulek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
vysvěcena	vysvěcen	k2eAgFnSc1d1	vysvěcena
a	a	k8xC	a
následně	následně	k6eAd1	následně
několikrát	několikrát	k6eAd1	několikrát
poničena	poničit	k5eAaPmNgFnS	poničit
požárem	požár	k1gInSc7	požár
a	a	k8xC	a
obnovena	obnoven	k2eAgNnPc1d1	obnoveno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
Polská	polský	k2eAgFnSc1d1	polská
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Slezská	slezský	k2eAgFnSc1d1	Slezská
<g/>
)	)	kIx)	)
bouda	bouda	k1gFnSc1	bouda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
do	do	k7c2	do
užívání	užívání	k1gNnSc2	užívání
současný	současný	k2eAgInSc4d1	současný
horský	horský	k2eAgInSc4d1	horský
hotel	hotel	k1gInSc4	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
polského	polský	k2eAgMnSc2d1	polský
architekta	architekt	k1gMnSc2	architekt
Lipińského	Lipińský	k1gMnSc2	Lipińský
<g/>
.	.	kIx.	.
</s>
<s>
Polská	polský	k2eAgFnSc1d1	polská
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
observatoř	observatoř	k1gFnSc1	observatoř
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1966	[number]	k4	1966
až	až	k9	až
1974	[number]	k4	1974
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Polské	polský	k2eAgFnSc2d1	polská
boudy	bouda	k1gFnSc2	bouda
na	na	k7c6	na
Sněžce	Sněžka	k1gFnSc6	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
ocelovou	ocelový	k2eAgFnSc7d1	ocelová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
připomínající	připomínající	k2eAgInPc1d1	připomínající
vesmírné	vesmírný	k2eAgInPc1d1	vesmírný
talíře	talíř	k1gInPc1	talíř
<g/>
,	,	kIx,	,
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
architekti	architekt	k1gMnPc1	architekt
Witold	Witoldo	k1gNnPc2	Witoldo
Lipinski	Lipinski	k1gNnSc7	Lipinski
a	a	k8xC	a
Waldemar	Waldemar	k1gMnSc1	Waldemar
Wawrzyniak	Wawrzyniak	k1gMnSc1	Wawrzyniak
z	z	k7c2	z
Wroclawské	Wroclawský	k2eAgFnSc2d1	Wroclawský
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Observatoř	observatoř	k1gFnSc1	observatoř
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
disku	disk	k1gInSc6	disk
je	být	k5eAaImIp3nS	být
technické	technický	k2eAgNnSc4d1	technické
zázemí	zázemí	k1gNnSc4	zázemí
a	a	k8xC	a
sklady	sklad	k1gInPc4	sklad
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc4d3	veliký
spodní	spodní	k2eAgInSc4d1	spodní
disk	disk	k1gInSc4	disk
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
zapuštěn	zapustit	k5eAaPmNgInS	zapustit
do	do	k7c2	do
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
nevydržel	vydržet	k5eNaPmAgInS	vydržet
horní	horní	k2eAgInSc1d1	horní
disk	disk	k1gInSc1	disk
(	(	kIx(	(
<g/>
Meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
nápor	nápor	k1gInSc1	nápor
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
zřítil	zřítit	k5eAaPmAgInS	zřítit
se	se	k3xPyFc4	se
na	na	k7c4	na
spodní	spodní	k2eAgNnSc4d1	spodní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
důkladné	důkladný	k2eAgFnSc6d1	důkladná
expertize	expertiza	k1gFnSc6	expertiza
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
žádnému	žádný	k3yNgNnSc3	žádný
dalšímu	další	k2eAgNnSc3d1	další
poškození	poškození	k1gNnSc3	poškození
by	by	kYmCp3nP	by
nemělo	mít	k5eNaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
obnovena	obnovit	k5eAaPmNgFnS	obnovit
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2015	[number]	k4	2015
Rádio	rádio	k1gNnSc4	rádio
Wroclaw	Wroclaw	k1gFnSc2	Wroclaw
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ústav	ústav	k1gInSc1	ústav
meteorologie	meteorologie	k1gFnSc2	meteorologie
a	a	k8xC	a
vodního	vodní	k2eAgNnSc2d1	vodní
hospodářství	hospodářství	k1gNnSc2	hospodářství
chce	chtít	k5eAaImIp3nS	chtít
budovu	budova	k1gFnSc4	budova
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
prodat	prodat	k5eAaPmF	prodat
nebo	nebo	k8xC	nebo
pronajmout	pronajmout	k5eAaPmF	pronajmout
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
kvůli	kvůli	k7c3	kvůli
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
meteorologie	meteorologie	k1gFnSc2	meteorologie
a	a	k8xC	a
vodního	vodní	k2eAgNnSc2d1	vodní
hospodářství	hospodářství	k1gNnSc2	hospodářství
tuto	tento	k3xDgFnSc4	tento
zprávu	zpráva	k1gFnSc4	zpráva
popřel	popřít	k5eAaPmAgMnS	popřít
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
spekulaci	spekulace	k1gFnSc4	spekulace
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Ústav	ústav	k1gInSc1	ústav
prý	prý	k9	prý
neplánuje	plánovat	k5eNaImIp3nS	plánovat
meteorologickou	meteorologický	k2eAgFnSc4d1	meteorologická
observatoř	observatoř	k1gFnSc4	observatoř
prodat	prodat	k5eAaPmF	prodat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vedení	vedení	k1gNnSc1	vedení
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
pouze	pouze	k6eAd1	pouze
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
společné	společný	k2eAgFnSc3d1	společná
dohodě	dohoda	k1gFnSc3	dohoda
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
využití	využití	k1gNnSc2	využití
budovy	budova	k1gFnSc2	budova
jak	jak	k8xC	jak
pro	pro	k7c4	pro
vědecké	vědecký	k2eAgNnSc4d1	vědecké
<g/>
,	,	kIx,	,
tak	tak	k9	tak
pro	pro	k7c4	pro
turistické	turistický	k2eAgInPc4d1	turistický
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gFnSc1	mluvčí
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
budovy	budova	k1gFnSc2	budova
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
nutnou	nutný	k2eAgFnSc4d1	nutná
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zpevnit	zpevnit	k5eAaPmF	zpevnit
ocelovou	ocelový	k2eAgFnSc4d1	ocelová
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
,	,	kIx,	,
opravit	opravit	k5eAaPmF	opravit
elektroinstalaci	elektroinstalace	k1gFnSc4	elektroinstalace
<g/>
,	,	kIx,	,
větrání	větrání	k1gNnSc4	větrání
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc4	datum
zahájení	zahájení	k1gNnSc2	zahájení
prací	práce	k1gFnPc2	práce
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
zajištění	zajištění	k1gNnSc4	zajištění
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poskytování	poskytování	k1gNnSc1	poskytování
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
není	být	k5eNaImIp3nS	být
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
v	v	k7c6	v
rozpočtu	rozpočet	k1gInSc6	rozpočet
ústavu	ústav	k1gInSc2	ústav
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
jeho	jeho	k3xOp3gFnPc2	jeho
zákonných	zákonný	k2eAgFnPc2d1	zákonná
činností	činnost	k1gFnPc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
budova	budova	k1gFnSc1	budova
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
vysoká	vysoký	k2eAgFnSc1d1	vysoká
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
jedinou	jediný	k2eAgFnSc7d1	jediná
fungující	fungující	k2eAgFnSc7d1	fungující
meteorologickou	meteorologický	k2eAgFnSc7d1	meteorologická
stanicí	stanice	k1gFnSc7	stanice
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Stržena	stržen	k2eAgFnSc1d1	stržena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
Sněžky	Sněžka	k1gFnSc2	Sněžka
stála	stát	k5eAaImAgFnS	stát
ještě	ještě	k9	ještě
nedávno	nedávno	k6eAd1	nedávno
zchátralá	zchátralý	k2eAgFnSc1d1	zchátralá
budova	budova	k1gFnSc1	budova
České	český	k2eAgFnSc2d1	Česká
boudy	bouda	k1gFnSc2	bouda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005-2006	[number]	k4	2005-2006
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
Česká	český	k2eAgFnSc1d1	Česká
poštovna	poštovna	k1gFnSc1	poštovna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1899	[number]	k4	1899
a	a	k8xC	a
2009	[number]	k4	2009
stála	stát	k5eAaImAgFnS	stát
opodál	opodál	k6eAd1	opodál
budova	budova	k1gFnSc1	budova
původní	původní	k2eAgFnSc2d1	původní
poštovny	poštovna	k1gFnSc2	poštovna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
rozebrána	rozebrán	k2eAgFnSc1d1	rozebrána
a	a	k8xC	a
převezena	převezen	k2eAgFnSc1d1	převezena
na	na	k7c4	na
Javorovou	javorový	k2eAgFnSc4d1	Javorová
skálu	skála	k1gFnSc4	skála
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
znovu	znovu	k6eAd1	znovu
sestavena	sestavit	k5eAaPmNgFnS	sestavit
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
získat	získat	k5eAaPmF	získat
poštovní	poštovní	k2eAgNnSc4d1	poštovní
razítko	razítko	k1gNnSc4	razítko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc1d1	horní
stanice	stanice	k1gFnSc1	stanice
sedačkové	sedačkový	k2eAgFnSc2d1	sedačková
lanovky	lanovka	k1gFnSc2	lanovka
z	z	k7c2	z
Pece	Pec	k1gFnSc2	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Lanovka	lanovka	k1gFnSc1	lanovka
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
úseky	úsek	k1gInPc4	úsek
a	a	k8xC	a
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
Pec	Pec	k1gFnSc1	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
-	-	kIx~	-
Sněžka	Sněžka	k1gFnSc1	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
funguje	fungovat	k5eAaImIp3nS	fungovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
lanovka	lanovka	k1gFnSc1	lanovka
měla	mít	k5eAaImAgFnS	mít
podobu	podoba	k1gFnSc4	podoba
dvojsedačky	dvojsedačky	k?	dvojsedačky
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
cestující	cestující	k1gMnPc1	cestující
seděli	sedět	k5eAaImAgMnP	sedět
bokem	bokem	k6eAd1	bokem
ke	k	k7c3	k
svahu	svah	k1gInSc2	svah
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
byla	být	k5eAaImAgFnS	být
švýcarskou	švýcarský	k2eAgFnSc7d1	švýcarská
firmou	firma	k1gFnSc7	firma
Von	von	k1gInSc1	von
Roll	Rollum	k1gNnPc2	Rollum
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
trvala	trvat	k5eAaImAgFnS	trvat
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
21.12	[number]	k4	21.12
<g/>
.2013	.2013	k4	.2013
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
nová	nový	k2eAgFnSc1d1	nová
kabinková	kabinkový	k2eAgFnSc1d1	kabinková
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
původní	původní	k2eAgFnSc4d1	původní
lanovou	lanový	k2eAgFnSc4d1	lanová
dráhu	dráha	k1gFnSc4	dráha
sedačkovou	sedačkový	k2eAgFnSc4d1	sedačková
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Pece	Pec	k1gFnSc2	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
nyní	nyní	k6eAd1	nyní
kabinka	kabinka	k1gFnSc1	kabinka
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgFnPc4	čtyři
osoby	osoba	k1gFnPc4	osoba
vyveze	vyvézt	k5eAaPmIp3nS	vyvézt
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
až	až	k9	až
250	[number]	k4	250
návštěvníků	návštěvník	k1gMnPc2	návštěvník
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
trasa	trasa	k1gFnSc1	trasa
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
lanovek	lanovka	k1gFnPc2	lanovka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
mezistanici	mezistanice	k1gFnSc4	mezistanice
na	na	k7c6	na
Růžové	růžový	k2eAgFnSc6d1	růžová
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
přepravy	přeprava	k1gFnSc2	přeprava
turistů	turist	k1gMnPc2	turist
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
k	k	k7c3	k
zásobování	zásobování	k1gNnSc3	zásobování
turistických	turistický	k2eAgFnPc2d1	turistická
chat	chata	k1gFnPc2	chata
na	na	k7c6	na
Růžové	růžový	k2eAgFnSc6d1	růžová
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
na	na	k7c6	na
Sněžce	Sněžka	k1gFnSc6	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
polské	polský	k2eAgFnSc2d1	polská
strany	strana	k1gFnSc2	strana
vede	vést	k5eAaImIp3nS	vést
sedačková	sedačkový	k2eAgFnSc1d1	sedačková
lanovka	lanovka	k1gFnSc1	lanovka
z	z	k7c2	z
Karpacze	Karpacze	k1gFnSc2	Karpacze
na	na	k7c4	na
předvrchol	předvrchol	k1gInSc4	předvrchol
Kopa	kopa	k1gFnSc1	kopa
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pěšky	pěšky	k6eAd1	pěšky
dojít	dojít	k5eAaPmF	dojít
na	na	k7c4	na
Obří	obří	k2eAgNnSc4d1	obří
sedlo	sedlo	k1gNnSc4	sedlo
a	a	k8xC	a
vystoupat	vystoupat	k5eAaPmF	vystoupat
na	na	k7c4	na
Sněžku	Sněžka	k1gFnSc4	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sněžku	sněžek	k1gInSc6	sněžek
vede	vést	k5eAaImIp3nS	vést
sedm	sedm	k4xCc1	sedm
základních	základní	k2eAgInPc2d1	základní
pěších	pěší	k2eAgInPc2d1	pěší
výstupů	výstup	k1gInPc2	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Úpy	Úpa	k1gFnSc2	Úpa
stoupá	stoupat	k5eAaImIp3nS	stoupat
žlutá	žlutý	k2eAgFnSc1d1	žlutá
turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
po	po	k7c6	po
bývalé	bývalý	k2eAgFnSc6d1	bývalá
nosičské	nosičský	k2eAgFnSc6d1	nosičská
trase	trasa	k1gFnSc6	trasa
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
se	se	k3xPyFc4	se
na	na	k7c4	na
Sněžku	Sněžka	k1gFnSc4	Sněžka
nosily	nosit	k5eAaImAgFnP	nosit
zásoby	zásoba	k1gFnPc1	zásoba
a	a	k8xC	a
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hřeben	hřeben	k1gInSc4	hřeben
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c6	na
rozcestí	rozcestí	k1gNnSc6	rozcestí
Nad	nad	k7c7	nad
Portášovými	portášův	k2eAgFnPc7d1	portášův
boudami	bouda	k1gFnPc7	bouda
<g/>
,	,	kIx,	,
s	s	k7c7	s
lanovkou	lanovka	k1gFnSc7	lanovka
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
na	na	k7c6	na
Růžové	růžový	k2eAgFnSc6d1	růžová
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
směru	směr	k1gInSc6	směr
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Pece	Pec	k1gFnSc2	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
prochází	procházet	k5eAaImIp3nS	procházet
modrá	modrý	k2eAgFnSc1d1	modrá
turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
skrz	skrz	k7c4	skrz
Obří	obří	k2eAgInSc4d1	obří
důl	důl	k1gInSc4	důl
na	na	k7c4	na
Obří	obří	k2eAgNnSc4d1	obří
sedlo	sedlo	k1gNnSc4	sedlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
u	u	k7c2	u
polské	polský	k2eAgFnSc2d1	polská
chaty	chata	k1gFnSc2	chata
Slezský	slezský	k2eAgInSc1d1	slezský
dům	dům	k1gInSc1	dům
napojí	napojit	k5eAaPmIp3nS	napojit
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
hřeben	hřeben	k1gInSc4	hřeben
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
kvůli	kvůli	k7c3	kvůli
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
lavin	lavina	k1gFnPc2	lavina
<g/>
.	.	kIx.	.
/	/	kIx~	/
<g/>
;	;	kIx,	;
Ze	z	k7c2	z
Špindlerova	Špindlerův	k2eAgInSc2d1	Špindlerův
Mlýna	mlýn	k1gInSc2	mlýn
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyjít	vyjít	k5eAaPmF	vyjít
k	k	k7c3	k
Luční	luční	k2eAgFnSc3d1	luční
boudě	bouda	k1gFnSc3	bouda
buď	buď	k8xC	buď
po	po	k7c6	po
modré	modrý	k2eAgFnSc6d1	modrá
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
podél	podél	k7c2	podél
Bílého	bílý	k2eAgNnSc2d1	bílé
Labe	Labe	k1gNnSc2	Labe
nebo	nebo	k8xC	nebo
po	po	k7c6	po
červené	červený	k2eAgFnSc6d1	červená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
přes	přes	k7c4	přes
Kozí	kozí	k2eAgInPc4d1	kozí
hřbety	hřbet	k1gInPc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obě	dva	k4xCgFnPc1	dva
trasy	trasa	k1gFnPc1	trasa
k	k	k7c3	k
Luční	luční	k2eAgFnSc3d1	luční
boudě	bouda	k1gFnSc3	bouda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
lavinami	lavina	k1gFnPc7	lavina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pokračovat	pokračovat	k5eAaImF	pokračovat
po	po	k7c4	po
modré	modré	k1gNnSc4	modré
přes	přes	k7c4	přes
Úpské	úpský	k2eAgNnSc4d1	Úpské
rašeliniště	rašeliniště	k1gNnSc4	rašeliniště
do	do	k7c2	do
Obřího	obří	k2eAgNnSc2d1	obří
sedla	sedlo	k1gNnSc2	sedlo
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
po	po	k7c6	po
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
hřebeni	hřeben	k1gInSc6	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Slezského	slezský	k2eAgNnSc2d1	Slezské
sedla	sedlo	k1gNnSc2	sedlo
od	od	k7c2	od
Špindlerovky	Špindlerovka	k1gFnSc2	Špindlerovka
vede	vést	k5eAaImIp3nS	vést
červeně	červeně	k6eAd1	červeně
značené	značený	k2eAgNnSc1d1	značené
cesta	cesta	k1gFnSc1	cesta
Česko-polského	českoolský	k2eAgNnSc2d1	česko-polské
přátelství	přátelství	k1gNnSc2	přátelství
víceméně	víceméně	k9	víceméně
po	po	k7c6	po
hlavním	hlavní	k2eAgInSc6d1	hlavní
krkonošském	krkonošský	k2eAgInSc6d1	krkonošský
hřebeni	hřeben	k1gInSc6	hřeben
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
polském	polský	k2eAgNnSc6d1	polské
území	území	k1gNnSc6	území
na	na	k7c4	na
Obří	obří	k2eAgNnSc4d1	obří
sedlo	sedlo	k1gNnSc4	sedlo
ke	k	k7c3	k
Slezskému	slezský	k2eAgInSc3d1	slezský
domu	dům	k1gInSc3	dům
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
po	po	k7c6	po
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
hřebeni	hřeben	k1gInSc6	hřeben
zajištěném	zajištěný	k2eAgInSc6d1	zajištěný
řetězovým	řetězový	k2eAgNnSc7d1	řetězové
zábradlím	zábradlí	k1gNnSc7	zábradlí
<g/>
.	.	kIx.	.
//	//	k?	//
Z	z	k7c2	z
Karpacze	Karpacze	k1gFnSc2	Karpacze
na	na	k7c4	na
Obří	obří	k2eAgNnSc4d1	obří
sedlo	sedlo	k1gNnSc4	sedlo
vedou	vést	k5eAaImIp3nP	vést
tři	tři	k4xCgFnPc1	tři
varianty	varianta	k1gFnPc1	varianta
tras	trasa	k1gFnPc2	trasa
údolím	údolí	k1gNnSc7	údolí
říčky	říčka	k1gFnSc2	říčka
Plasawa	Plasaw	k1gInSc2	Plasaw
po	po	k7c6	po
modré	modrý	k2eAgFnSc6d1	modrá
značce	značka	k1gFnSc6	značka
<g/>
,	,	kIx,	,
údolím	údolí	k1gNnSc7	údolí
říčky	říčka	k1gFnSc2	říčka
Lomnica	Lomnic	k1gInSc2	Lomnic
po	po	k7c6	po
žluté	žlutý	k2eAgFnSc6d1	žlutá
značce	značka	k1gFnSc6	značka
a	a	k8xC	a
údolím	údolí	k1gNnSc7	údolí
Zlatého	zlatý	k2eAgInSc2d1	zlatý
potoka	potok	k1gInSc2	potok
po	po	k7c6	po
černé	černý	k2eAgFnSc6d1	černá
značce	značka	k1gFnSc6	značka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
na	na	k7c6	na
Obřím	obří	k2eAgNnSc6d1	obří
sedle	sedlo	k1gNnSc6	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
podél	podél	k7c2	podél
Zlatého	zlatý	k2eAgInSc2d1	zlatý
potoka	potok	k1gInSc2	potok
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
ohrožována	ohrožován	k2eAgFnSc1d1	ohrožována
obrovskými	obrovský	k2eAgFnPc7d1	obrovská
lavinami	lavina	k1gFnPc7	lavina
z	z	k7c2	z
kotle	kotel	k1gInSc2	kotel
Bialy	Biala	k1gFnSc2	Biala
Jar	jar	k1gFnSc1	jar
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
;	;	kIx,	;
Z	z	k7c2	z
Wilcza	Wilcz	k1gMnSc2	Wilcz
Poreba	Poreb	k1gMnSc2	Poreb
strmě	strmě	k6eAd1	strmě
stoupá	stoupat	k5eAaImIp3nS	stoupat
žlutá	žlutý	k2eAgFnSc1d1	žlutá
turistická	turistický	k2eAgFnSc1d1	turistická
značka	značka	k1gFnSc1	značka
podél	podél	k7c2	podél
potoka	potok	k1gInSc2	potok
Lomnička	Lomnička	k1gFnSc1	Lomnička
<g/>
,	,	kIx,	,
u	u	k7c2	u
turistické	turistický	k2eAgFnSc2d1	turistická
chaty	chata	k1gFnSc2	chata
Nad	nad	k7c7	nad
Lomniczka	Lomniczka	k1gFnSc1	Lomniczka
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
značkou	značka	k1gFnSc7	značka
z	z	k7c2	z
Karpacze	Karpacze	k1gFnSc2	Karpacze
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
Obří	obří	k2eAgNnSc4d1	obří
sedlo	sedlo	k1gNnSc4	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
cesta	cesta	k1gFnSc1	cesta
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
kvůli	kvůli	k7c3	kvůli
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
lavin	lavina	k1gFnPc2	lavina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Malé	Malé	k2eAgFnSc2d1	Malé
Úpy	Úpa	k1gFnSc2	Úpa
vede	vést	k5eAaImIp3nS	vést
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
cesta	cesta	k1gFnSc1	cesta
Česko-polského	českoolský	k2eAgNnSc2d1	česko-polské
přátelství	přátelství	k1gNnSc2	přátelství
od	od	k7c2	od
Pomezních	pomezní	k2eAgFnPc2d1	pomezní
bud	bouda	k1gFnPc2	bouda
přes	přes	k7c4	přes
Soví	soví	k2eAgNnSc4d1	soví
sedlo	sedlo	k1gNnSc4	sedlo
a	a	k8xC	a
Svorovou	svorový	k2eAgFnSc4d1	Svorová
horu	hora	k1gFnSc4	hora
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Soví	soví	k2eAgNnSc4d1	soví
sedlo	sedlo	k1gNnSc4	sedlo
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
z	z	k7c2	z
Pomezních	pomezní	k2eAgFnPc2d1	pomezní
bud	bouda	k1gFnPc2	bouda
dostat	dostat	k5eAaPmF	dostat
také	také	k9	také
po	po	k7c6	po
modré	modrý	k2eAgFnSc6d1	modrá
značce	značka	k1gFnSc6	značka
po	po	k7c6	po
Lesním	lesní	k2eAgInSc6d1	lesní
hřebeni	hřeben	k1gInSc6	hřeben
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
Karpacze	Karpacze	k1gFnSc2	Karpacze
po	po	k7c6	po
černé	černý	k2eAgFnSc6d1	černá
značce	značka	k1gFnSc6	značka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Svorovou	svorový	k2eAgFnSc4d1	Svorová
horu	hora	k1gFnSc4	hora
lze	lze	k6eAd1	lze
dorazit	dorazit	k5eAaPmF	dorazit
také	také	k9	také
po	po	k7c6	po
žluté	žlutý	k2eAgFnSc6d1	žlutá
značce	značka	k1gFnSc6	značka
přes	přes	k7c4	přes
turistickou	turistický	k2eAgFnSc4d1	turistická
boudu	bouda	k1gFnSc4	bouda
Jelenka	Jelenka	k1gFnSc1	Jelenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úbočí	úbočí	k1gNnSc6	úbočí
Sněžky	Sněžka	k1gFnSc2	Sněžka
vedou	vést	k5eAaImIp3nP	vést
dvě	dva	k4xCgFnPc1	dva
promenádní	promenádní	k2eAgFnPc1d1	promenádní
vyhlídkové	vyhlídkový	k2eAgFnPc1d1	vyhlídková
cesty	cesta	k1gFnPc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
traverzuje	traverzovat	k5eAaImIp3nS	traverzovat
jihovýchodní	jihovýchodní	k2eAgFnSc7d1	jihovýchodní
úbočí	úboč	k1gFnSc7	úboč
zeleně	zeleně	k6eAd1	zeleně
značená	značený	k2eAgFnSc1d1	značená
pěší	pěší	k2eAgFnSc1d1	pěší
cesta	cesta	k1gFnSc1	cesta
dlážděná	dlážděný	k2eAgFnSc1d1	dlážděná
velkými	velký	k2eAgFnPc7d1	velká
kameny	kámen	k1gInPc1	kámen
mezi	mezi	k7c7	mezi
Jelenkou	Jelenka	k1gFnSc7	Jelenka
a	a	k8xC	a
rozcestím	rozcestí	k1gNnSc7	rozcestí
Nad	nad	k7c7	nad
Růžovohorským	Růžovohorský	k2eAgNnSc7d1	Růžovohorský
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
protíná	protínat	k5eAaImIp3nS	protínat
severní	severní	k2eAgFnSc4d1	severní
stěnu	stěna	k1gFnSc4	stěna
stará	starat	k5eAaImIp3nS	starat
Slezská	slezský	k2eAgFnSc1d1	Slezská
silnice	silnice	k1gFnSc1	silnice
vydlážděná	vydlážděný	k2eAgFnSc1d1	vydlážděná
kočičími	kočičí	k2eAgFnPc7d1	kočičí
hlavami	hlava	k1gFnPc7	hlava
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yRgInPc4	který
dodnes	dodnes	k6eAd1	dodnes
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
vyjede	vyjet	k5eAaPmIp3nS	vyjet
terénní	terénní	k2eAgNnSc4d1	terénní
auto	auto	k1gNnSc4	auto
<g/>
.	.	kIx.	.
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1	Slezská
silnice	silnice	k1gFnSc1	silnice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
kvůli	kvůli	k7c3	kvůli
lavinovému	lavinový	k2eAgNnSc3d1	lavinové
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Sněžka	Sněžka	k1gFnSc1	Sněžka
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgNnSc7d1	tradiční
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
turistické	turistický	k2eAgNnSc4d1	turistické
i	i	k8xC	i
extrémní	extrémní	k2eAgNnSc4d1	extrémní
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
sjezdových	sjezdový	k2eAgFnPc2d1	sjezdová
tras	trasa	k1gFnPc2	trasa
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
běžkách	běžka	k1gFnPc6	běžka
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
sjet	sjet	k5eAaPmF	sjet
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
bezpečným	bezpečný	k2eAgInSc7d1	bezpečný
sjezdem	sjezd	k1gInSc7	sjezd
na	na	k7c4	na
Pomezní	pomezní	k2eAgFnPc4d1	pomezní
Boudy	bouda	k1gFnPc4	bouda
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
s	s	k7c7	s
lyžemi	lyže	k1gFnPc7	lyže
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
a	a	k8xC	a
potom	potom	k6eAd1	potom
na	na	k7c6	na
běžkách	běžka	k1gFnPc6	běžka
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c4	na
Růžovou	růžový	k2eAgFnSc4d1	růžová
horu	hora	k1gFnSc4	hora
a	a	k8xC	a
Portášky	Portáška	k1gFnPc4	Portáška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
skialpinistické	skialpinistický	k2eAgInPc1d1	skialpinistický
sjezdy	sjezd	k1gInPc1	sjezd
možné	možný	k2eAgInPc1d1	možný
jen	jen	k9	jen
po	po	k7c6	po
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc6d1	uvedená
značených	značený	k2eAgFnPc6d1	značená
cestách	cesta	k1gFnPc6	cesta
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrchol	vrchol	k1gInSc1	vrchol
Sněžky	Sněžka	k1gFnSc2	Sněžka
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
zóně	zóna	k1gFnSc6	zóna
KRNAP	KRNAP	kA	KRNAP
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
pohyb	pohyb	k1gInSc1	pohyb
mimo	mimo	k7c4	mimo
značené	značený	k2eAgFnPc4d1	značená
stezky	stezka	k1gFnPc4	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
zakázané	zakázaný	k2eAgFnPc1d1	zakázaná
<g/>
)	)	kIx)	)
sjezdové	sjezdový	k2eAgFnPc1d1	sjezdová
trasy	trasa	k1gFnPc1	trasa
vedou	vést	k5eAaImIp3nP	vést
skalnatou	skalnatý	k2eAgFnSc7d1	skalnatá
jižní	jižní	k2eAgFnSc7d1	jižní
stěnou	stěna	k1gFnSc7	stěna
do	do	k7c2	do
Obřího	obří	k2eAgInSc2d1	obří
dolu	dol	k1gInSc2	dol
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Pece	Pec	k1gFnSc2	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
<g/>
.	.	kIx.	.
</s>
<s>
Sutí	sutit	k5eAaImIp3nS	sutit
zasypanou	zasypaný	k2eAgFnSc7d1	zasypaná
severní	severní	k2eAgFnSc7d1	severní
stěnou	stěna	k1gFnSc7	stěna
se	se	k3xPyFc4	se
sjíždí	sjíždět	k5eAaImIp3nS	sjíždět
do	do	k7c2	do
Karpacze	Karpacze	k1gFnSc2	Karpacze
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
varianty	varianta	k1gFnPc1	varianta
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgInPc1d1	možný
z	z	k7c2	z
Obřího	obří	k2eAgNnSc2d1	obří
sedla	sedlo	k1gNnSc2	sedlo
na	na	k7c4	na
sever	sever	k1gInSc4	sever
i	i	k9	i
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
z	z	k7c2	z
Růžovohorského	Růžovohorský	k2eAgNnSc2d1	Růžovohorský
sedla	sedlo	k1gNnSc2	sedlo
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
celoroční	celoroční	k2eAgFnSc1d1	celoroční
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hory	hora	k1gFnSc2	hora
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pouze	pouze	k6eAd1	pouze
okolo	okolo	k7c2	okolo
0,2	[number]	k4	0,2
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c6	na
Sněžce	Sněžka	k1gFnSc6	Sněžka
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
216	[number]	k4	216
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
rekord	rekord	k1gInSc1	rekord
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
