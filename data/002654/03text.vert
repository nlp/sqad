<s>
Konopí	konopí	k1gNnSc1	konopí
(	(	kIx(	(
<g/>
Cannabis	Cannabis	k1gInSc1	Cannabis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
jednoletých	jednoletý	k2eAgFnPc2d1	jednoletá
bylin	bylina	k1gFnPc2	bylina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
konopovitých	konopovití	k1gMnPc2	konopovití
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dvoudomé	dvoudomý	k2eAgFnPc1d1	dvoudomá
byliny	bylina	k1gFnPc1	bylina
se	s	k7c7	s
vzpřímenými	vzpřímený	k2eAgFnPc7d1	vzpřímená
lodyhami	lodyha	k1gFnPc7	lodyha
<g/>
,	,	kIx,	,
samičí	samičí	k2eAgFnPc1d1	samičí
rostliny	rostlina	k1gFnPc1	rostlina
bývají	bývat	k5eAaImIp3nP	bývat
nižší	nízký	k2eAgFnPc1d2	nižší
než	než	k8xS	než
samčí	samčí	k2eAgFnPc1d1	samčí
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
štíhlejší	štíhlý	k2eAgFnPc1d2	štíhlejší
a	a	k8xC	a
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
asi	asi	k9	asi
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Lodyhy	lodyha	k1gFnPc1	lodyha
jsou	být	k5eAaImIp3nP	být
vysoké	vysoká	k1gFnSc3	vysoká
okolo	okolo	k7c2	okolo
2	[number]	k4	2
až	až	k9	až
3	[number]	k4	3
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
větvené	větvený	k2eAgFnSc6d1	větvená
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
dlanitě	dlanitě	k6eAd1	dlanitě
složené	složený	k2eAgInPc1d1	složený
<g/>
,	,	kIx,	,
3	[number]	k4	3
až	až	k9	až
9	[number]	k4	9
četné	četný	k2eAgInPc1d1	četný
se	s	k7c7	s
zubatými	zubatý	k2eAgInPc7d1	zubatý
čárkovitými	čárkovitý	k2eAgInPc7d1	čárkovitý
žilnatými	žilnatý	k2eAgInPc7d1	žilnatý
lístky	lístek	k1gInPc7	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Stopkaté	stopkatý	k2eAgInPc1d1	stopkatý
samčí	samčí	k2eAgInPc1d1	samčí
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
do	do	k7c2	do
lat	lata	k1gFnPc2	lata
<g/>
,	,	kIx,	,
samičí	samičí	k2eAgInPc4d1	samičí
úžlabní	úžlabní	k2eAgInPc4d1	úžlabní
květy	květ	k1gInPc4	květ
jsou	být	k5eAaImIp3nP	být
přisedlé	přisedlý	k2eAgInPc1d1	přisedlý
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
rostliny	rostlina	k1gFnPc1	rostlina
větrosnubné	větrosnubný	k2eAgFnPc1d1	větrosnubná
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
jednosemenná	jednosemenný	k2eAgFnSc1d1	jednosemenná
nažka	nažka	k1gFnSc1	nažka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
kořen	kořen	k1gInSc1	kořen
je	být	k5eAaImIp3nS	být
vřetenový	vřetenový	k2eAgInSc1d1	vřetenový
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
postranními	postranní	k2eAgInPc7d1	postranní
kořínky	kořínek	k1gInPc7	kořínek
<g/>
.	.	kIx.	.
</s>
<s>
Konopí	konopí	k1gNnSc1	konopí
je	být	k5eAaImIp3nS	být
teplomilná	teplomilný	k2eAgFnSc1d1	teplomilná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
neutrálních	neutrální	k2eAgFnPc6d1	neutrální
až	až	k8xS	až
mírně	mírně	k6eAd1	mírně
kyselých	kyselý	k2eAgFnPc6d1	kyselá
hlinitých	hlinitý	k2eAgFnPc6d1	hlinitá
půdách	půda	k1gFnPc6	půda
s	s	k7c7	s
dostatkem	dostatek	k1gInSc7	dostatek
vláhy	vláha	k1gFnSc2	vláha
a	a	k8xC	a
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
taxonomii	taxonomie	k1gFnSc6	taxonomie
konopí	konopí	k1gNnSc2	konopí
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c7	mezi
botaniky	botanik	k1gMnPc7	botanik
dosud	dosud	k6eAd1	dosud
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
Cannabis	Cannabis	k1gFnSc1	Cannabis
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
monotypický	monotypický	k2eAgInSc4d1	monotypický
taxon	taxon	k1gInSc4	taxon
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
Cannabis	Cannabis	k1gFnPc2	Cannabis
sativa	sativ	k1gMnSc2	sativ
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
poddruhy	poddruh	k1gInPc4	poddruh
C.	C.	kA	C.
sativa	sativa	k1gFnSc1	sativa
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sativa	sativa	k1gFnSc1	sativa
a	a	k8xC	a
C.	C.	kA	C.
sativa	sativa	k1gFnSc1	sativa
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
indica	indica	k1gFnSc1	indica
Lam	lama	k1gFnPc2	lama
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgInSc1d2	častější
polytypický	polytypický	k2eAgInSc1d1	polytypický
koncept	koncept	k1gInSc1	koncept
ale	ale	k9	ale
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
C.	C.	kA	C.
sativa	sativa	k1gFnSc1	sativa
a	a	k8xC	a
C.	C.	kA	C.
indica	indica	k6eAd1	indica
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
C.	C.	kA	C.
sativa	sativo	k1gNnSc2	sativo
L.	L.	kA	L.
<g/>
,	,	kIx,	,
C.	C.	kA	C.
indica	indica	k1gFnSc1	indica
Lam	lama	k1gFnPc2	lama
<g/>
.	.	kIx.	.
a	a	k8xC	a
C.	C.	kA	C.
ruderalis	ruderalis	k1gFnPc2	ruderalis
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Konopí	konopí	k1gNnSc1	konopí
seté	setý	k2eAgFnPc1d1	setá
(	(	kIx(	(
<g/>
Cannabis	Cannabis	k1gFnPc1	Cannabis
sativa	sativo	k1gNnSc2	sativo
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
svým	svůj	k3xOyFgInSc7	svůj
dlouhým	dlouhý	k2eAgMnSc7d1	dlouhý
<g/>
,	,	kIx,	,
štíhlým	štíhlý	k2eAgInSc7d1	štíhlý
stonkem	stonek	k1gInSc7	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
všestranně	všestranně	k6eAd1	všestranně
využitelná	využitelný	k2eAgFnSc1d1	využitelná
plodina	plodina	k1gFnSc1	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
nejznámější	známý	k2eAgNnPc1d3	nejznámější
využití	využití	k1gNnPc1	využití
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
textilní	textilní	k2eAgFnSc6d1	textilní
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stonku	stonek	k1gInSc2	stonek
se	se	k3xPyFc4	se
po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
namáčením	namáčení	k1gNnSc7	namáčení
a	a	k8xC	a
třením	tření	k1gNnSc7	tření
získávají	získávat	k5eAaImIp3nP	získávat
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
spřádají	spřádat	k5eAaImIp3nP	spřádat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
konopného	konopný	k2eAgNnSc2d1	konopné
textilního	textilní	k2eAgNnSc2d1	textilní
vlákna	vlákno	k1gNnSc2	vlákno
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
provazce	provazec	k1gInPc1	provazec
<g/>
,	,	kIx,	,
lana	lano	k1gNnPc1	lano
<g/>
,	,	kIx,	,
plachty	plachta	k1gFnPc1	plachta
<g/>
,	,	kIx,	,
rohože	rohož	k1gFnPc1	rohož
i	i	k8xC	i
pytle	pytel	k1gInPc1	pytel
velmi	velmi	k6eAd1	velmi
pevné	pevný	k2eAgInPc1d1	pevný
a	a	k8xC	a
odolné	odolný	k2eAgInPc1d1	odolný
proti	proti	k7c3	proti
vlhkosti	vlhkost	k1gFnSc3	vlhkost
a	a	k8xC	a
hnilobě	hniloba	k1gFnSc3	hniloba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
krátkých	krátký	k2eAgNnPc2d1	krátké
vláken	vlákno	k1gNnPc2	vlákno
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
např.	např.	kA	např.
koudel	koudel	k1gFnSc4	koudel
pro	pro	k7c4	pro
utěsňování	utěsňování	k1gNnSc4	utěsňování
šroubovaných	šroubovaný	k2eAgInPc2d1	šroubovaný
spojů	spoj	k1gInPc2	spoj
potrubí	potrubí	k1gNnSc2	potrubí
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
pěstované	pěstovaný	k2eAgFnPc1d1	pěstovaná
pro	pro	k7c4	pro
vlákno	vlákno	k1gNnSc4	vlákno
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgFnPc4d1	textilní
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
mít	mít	k5eAaImF	mít
co	co	k9	co
největší	veliký	k2eAgFnSc4d3	veliký
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
co	co	k9	co
nejméně	málo	k6eAd3	málo
kolének	kolénko	k1gNnPc2	kolénko
<g/>
.	.	kIx.	.
</s>
<s>
Vlákniny	vláknina	k1gFnPc4	vláknina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
asi	asi	k9	asi
25	[number]	k4	25
%	%	kIx~	%
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
dřevitá	dřevitý	k2eAgFnSc1d1	dřevitá
hmota	hmota	k1gFnSc1	hmota
-	-	kIx~	-
pazdeří	pazdeří	k1gNnSc1	pazdeří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
lisuje	lisovat	k5eAaImIp3nS	lisovat
bez	bez	k7c2	bez
pojiva	pojivo	k1gNnSc2	pojivo
do	do	k7c2	do
briket	briketa	k1gFnPc2	briketa
pro	pro	k7c4	pro
spalování	spalování	k1gNnSc4	spalování
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
tepelně	tepelně	k6eAd1	tepelně
izolační	izolační	k2eAgInSc4d1	izolační
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
konopí	konopí	k1gNnSc1	konopí
asi	asi	k9	asi
40	[number]	k4	40
let	léto	k1gNnPc2	léto
vůbec	vůbec	k9	vůbec
nepěstovalo	pěstovat	k5eNaImAgNnS	pěstovat
<g/>
,	,	kIx,	,
pěstování	pěstování	k1gNnSc1	pěstování
se	se	k3xPyFc4	se
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
167	[number]	k4	167
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
návykových	návykový	k2eAgFnPc6d1	návyková
látkách	látka	k1gFnPc6	látka
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
odrůdy	odrůda	k1gFnPc4	odrůda
je	být	k5eAaImIp3nS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
pěstovat	pěstovat	k5eAaImF	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
olej	olej	k1gInSc4	olej
bohatý	bohatý	k2eAgInSc4d1	bohatý
na	na	k7c4	na
vitamin	vitamin	k1gInSc4	vitamin
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
<g/>
E	E	kA	E
.	.	kIx.	.
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nenasycené	nasycený	k2eNgNnSc4d1	nenasycené
mastné	mastné	k1gNnSc4	mastné
kyseliny	kyselina	k1gFnSc2	kyselina
Omega	omega	k1gNnSc1	omega
<g/>
6	[number]	k4	6
a	a	k8xC	a
Omega	omega	k1gNnSc1	omega
<g/>
3	[number]	k4	3
v	v	k7c6	v
poměrném	poměrný	k2eAgNnSc6d1	poměrné
zastoupení	zastoupení	k1gNnSc6	zastoupení
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgNnSc4d1	stejné
je	být	k5eAaImIp3nS	být
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vylisovaný	vylisovaný	k2eAgInSc1d1	vylisovaný
olej	olej	k1gInSc1	olej
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
,	,	kIx,	,
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
mýdel	mýdlo	k1gNnPc2	mýdlo
<g/>
,	,	kIx,	,
mazadel	mazadlo	k1gNnPc2	mazadlo
a	a	k8xC	a
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pokrutiny	pokrutiny	k1gFnPc1	pokrutiny
zbylé	zbylý	k2eAgFnPc1d1	zbylá
po	po	k7c6	po
vylisování	vylisování	k1gNnSc6	vylisování
se	se	k3xPyFc4	se
zkrmují	zkrmovat	k5eAaImIp3nP	zkrmovat
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgNnPc1d1	celé
semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
tradičním	tradiční	k2eAgInSc7d1	tradiční
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
semenec	semenec	k1gInSc1	semenec
<g/>
"	"	kIx"	"
běžně	běžně	k6eAd1	běžně
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
krmivo	krmivo	k1gNnSc1	krmivo
pro	pro	k7c4	pro
ptactvo	ptactvo	k1gNnSc4	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dále	daleko	k6eAd2	daleko
mastné	mastný	k2eAgFnPc1d1	mastná
kyseliny	kyselina	k1gFnPc1	kyselina
(	(	kIx(	(
<g/>
palmitová	palmitový	k2eAgFnSc1d1	palmitová
<g/>
,	,	kIx,	,
stearová	stearový	k2eAgFnSc1d1	stearová
<g/>
,	,	kIx,	,
olejová	olejový	k2eAgFnSc1d1	olejová
<g/>
,	,	kIx,	,
linolová	linolový	k2eAgFnSc1d1	linolová
<g/>
,	,	kIx,	,
linolenová	linolenový	k2eAgFnSc1d1	linolenová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kultivary	kultivar	k1gInPc1	kultivar
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
konopí	konopí	k1gNnSc4	konopí
indické	indický	k2eAgFnSc2d1	indická
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
pro	pro	k7c4	pro
léčebné	léčebný	k2eAgInPc4d1	léčebný
a	a	k8xC	a
rekreační	rekreační	k2eAgInPc4d1	rekreační
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
jeho	jeho	k3xOp3gInPc1	jeho
metabolity	metabolit	k1gInPc1	metabolit
kanabinoidy	kanabinoida	k1gFnSc2	kanabinoida
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
THC	THC	kA	THC
<g/>
,	,	kIx,	,
CBD	CBD	kA	CBD
<g/>
,	,	kIx,	,
CBC	CBC	kA	CBC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sušené	sušený	k2eAgInPc4d1	sušený
listy	list	k1gInPc4	list
a	a	k8xC	a
především	především	k6eAd1	především
samičí	samičí	k2eAgNnPc1d1	samičí
květenství	květenství	k1gNnPc1	květenství
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
také	také	k9	také
marihuana	marihuana	k1gFnSc1	marihuana
<g/>
,	,	kIx,	,
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hašiš	hašiš	k1gInSc1	hašiš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celosvětovém	celosvětový	k2eAgInSc6d1	celosvětový
zákazu	zákaz	k1gInSc6	zákaz
pěstování	pěstování	k1gNnSc2	pěstování
konopí	konopí	k1gNnSc4	konopí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
daném	daný	k2eAgInSc6d1	daný
Jednotnou	jednotný	k2eAgFnSc7d1	jednotná
úmlouvou	úmlouva	k1gFnSc7	úmlouva
OSN	OSN	kA	OSN
o	o	k7c6	o
omamných	omamný	k2eAgFnPc6d1	omamná
látkách	látka	k1gFnPc6	látka
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
vyšlechtění	vyšlechtění	k1gNnSc3	vyšlechtění
odrůd	odrůda	k1gFnPc2	odrůda
konopí	konopí	k1gNnSc2	konopí
setého	setý	k2eAgNnSc2d1	seté
se	s	k7c7	s
sníženým	snížený	k2eAgInSc7d1	snížený
obsahem	obsah	k1gInSc7	obsah
THC	THC	kA	THC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
uznala	uznat	k5eAaPmAgFnS	uznat
EU	EU	kA	EU
pěstování	pěstování	k1gNnSc4	pěstování
konopí	konopí	k1gNnSc2	konopí
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
THC	THC	kA	THC
do	do	k7c2	do
0,2	[number]	k4	0,2
%	%	kIx~	%
pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
či	či	k8xC	či
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
účely	účel	k1gInPc4	účel
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
pojem	pojem	k1gInSc1	pojem
technické	technický	k2eAgFnSc2d1	technická
či	či	k8xC	či
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
konopí	konopí	k1gNnSc4	konopí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pěstování	pěstování	k1gNnSc1	pěstování
konopí	konopí	k1gNnSc2	konopí
(	(	kIx(	(
<g/>
THC	THC	kA	THC
max	max	kA	max
<g/>
.	.	kIx.	.
0,3	[number]	k4	0,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
167	[number]	k4	167
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
návykových	návykový	k2eAgFnPc6d1	návyková
látkách	látka	k1gFnPc6	látka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
současně	současně	k6eAd1	současně
pěstitelům	pěstitel	k1gMnPc3	pěstitel
určuje	určovat	k5eAaImIp3nS	určovat
i	i	k9	i
ohlašovací	ohlašovací	k2eAgFnSc1d1	ohlašovací
povinnost	povinnost	k1gFnSc1	povinnost
při	při	k7c6	při
osetí	osetí	k1gNnSc6	osetí
plochy	plocha	k1gFnSc2	plocha
nad	nad	k7c7	nad
100	[number]	k4	100
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
ĆSÚ	ĆSÚ	kA	ĆSÚ
udává	udávat	k5eAaImIp3nS	udávat
osetou	osetý	k2eAgFnSc4d1	osetá
plochu	plocha	k1gFnSc4	plocha
v	v	k7c6	v
ČR	ČR	kA	ČR
technickým	technický	k2eAgNnSc7d1	technické
konopím	konopí	k1gNnSc7	konopí
na	na	k7c4	na
427	[number]	k4	427
ha	ha	kA	ha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
o	o	k7c4	o
300	[number]	k4	300
ha	ha	kA	ha
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
výrazný	výrazný	k2eAgInSc1d1	výrazný
.	.	kIx.	.
</s>
<s>
Paragraf	paragraf	k1gInSc1	paragraf
č.	č.	k?	č.
285	[number]	k4	285
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
citováno	citován	k2eAgNnSc1d1	citováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
kdo	kdo	k3yQnSc1	kdo
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
potřebu	potřeba	k1gFnSc4	potřeba
přechovává	přechovávat	k5eAaImIp3nS	přechovávat
nebo	nebo	k8xC	nebo
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
konopí	konopí	k1gNnSc2	konopí
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
THC	THC	kA	THC
nad	nad	k7c7	nad
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
,	,	kIx,	,
dopouští	dopouštět	k5eAaImIp3nS	dopouštět
se	se	k3xPyFc4	se
přestupku	přestupek	k1gInSc2	přestupek
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
potrestán	potrestat	k5eAaPmNgInS	potrestat
pokutou	pokuta	k1gFnSc7	pokuta
až	až	k6eAd1	až
15	[number]	k4	15
000	[number]	k4	000
Kč	Kč	kA	Kč
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
kdo	kdo	k3yInSc1	kdo
totéž	týž	k3xTgNnSc1	týž
činí	činit	k5eAaImIp3nS	činit
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
množstvím	množství	k1gNnSc7	množství
<g/>
,	,	kIx,	,
dopouští	dopouštět	k5eAaImIp3nS	dopouštět
se	se	k3xPyFc4	se
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
<g/>
.	.	kIx.	.
</s>
<s>
Nabízení	nabízení	k1gNnSc1	nabízení
nebo	nebo	k8xC	nebo
prodej	prodej	k1gInSc1	prodej
je	být	k5eAaImIp3nS	být
však	však	k9	však
vždy	vždy	k6eAd1	vždy
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
<g/>
.	.	kIx.	.
</s>
<s>
Konopí	konopí	k1gNnSc1	konopí
indické	indický	k2eAgFnSc2d1	indická
(	(	kIx(	(
<g/>
Cannabis	Cannabis	k1gFnSc2	Cannabis
indica	indicus	k1gMnSc2	indicus
Lam	lama	k1gFnPc2	lama
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
košatější	košatý	k2eAgMnSc1d2	košatější
než	než	k8xS	než
konopí	konopí	k1gNnSc1	konopí
seté	setý	k2eAgNnSc1d1	seté
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
širší	široký	k2eAgInPc4d2	širší
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
konopí	konopí	k1gNnSc1	konopí
seté	setý	k2eAgNnSc1d1	seté
na	na	k7c6	na
listech	list	k1gInPc6	list
a	a	k8xC	a
samičích	samičí	k2eAgFnPc6d1	samičí
květenství	květenství	k1gNnSc2	květenství
vrstvičku	vrstvička	k1gFnSc4	vrstvička
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kanabinoidy	kanabinoida	k1gFnPc4	kanabinoida
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
především	především	k9	především
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Konopí	konopí	k1gNnSc1	konopí
rumištní	rumištní	k2eAgFnSc2d1	rumištní
(	(	kIx(	(
<g/>
Cannabis	Cannabis	k1gFnSc2	Cannabis
ruderalis	ruderalis	k1gFnSc2	ruderalis
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plevelnou	plevelný	k2eAgFnSc7d1	plevelná
rostlinou	rostlina	k1gFnSc7	rostlina
zavlečenou	zavlečený	k2eAgFnSc7d1	zavlečená
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
ze	z	k7c2	z
sibiřské	sibiřský	k2eAgFnSc2d1	sibiřská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnPc4	výška
do	do	k7c2	do
1,5	[number]	k4	1,5
m	m	kA	m
<g/>
,	,	kIx,	,
stonek	stonek	k1gInSc1	stonek
je	být	k5eAaImIp3nS	být
tenký	tenký	k2eAgInSc4d1	tenký
<g/>
,	,	kIx,	,
větvený	větvený	k2eAgInSc4d1	větvený
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
olistěný	olistěný	k2eAgMnSc1d1	olistěný
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
planou	planý	k2eAgFnSc4d1	planá
formu	forma	k1gFnSc4	forma
konopí	konopí	k1gNnSc2	konopí
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yQgFnSc2	který
obsah	obsah	k1gInSc4	obsah
delta-	delta-	k?	delta-
<g/>
9	[number]	k4	9
<g/>
-THC	-THC	k?	-THC
není	být	k5eNaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
běžně	běžně	k6eAd1	běžně
setkáme	setkat	k5eAaPmIp1nP	setkat
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
či	či	k8xC	či
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
kolem	kolem	k6eAd1	kolem
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nalézt	nalézt	k5eAaPmF	nalézt
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
i	i	k9	i
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
-	-	kIx~	-
podél	podél	k6eAd1	podél
Dyje	Dyje	k1gFnSc1	Dyje
či	či	k8xC	či
na	na	k7c6	na
Pálavsku	Pálavsko	k1gNnSc6	Pálavsko
<g/>
.	.	kIx.	.
</s>
<s>
Konopí	konopí	k1gNnSc1	konopí
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
civilizacích	civilizace	k1gFnPc6	civilizace
<g/>
,	,	kIx,	,
od	od	k7c2	od
Asie	Asie	k1gFnSc2	Asie
po	po	k7c4	po
Evropu	Evropa	k1gFnSc4	Evropa
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
od	od	k7c2	od
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
