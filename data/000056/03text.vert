<s>
Interval	interval	k1gInSc1	interval
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
tóny	tón	k1gInPc7	tón
<g/>
.	.	kIx.	.
</s>
<s>
Ozvou	ozva	k1gFnSc7	ozva
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
tóny	tón	k1gInPc1	tón
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
interval	interval	k1gInSc1	interval
harmonický	harmonický	k2eAgInSc1d1	harmonický
<g/>
,	,	kIx,	,
zazní	zaznět	k5eAaImIp3nS	zaznět
<g/>
-li	i	k?	-li
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
interval	interval	k1gInSc4	interval
melodický	melodický	k2eAgInSc4d1	melodický
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnSc4d3	nejmenší
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
tóny	tón	k1gInPc7	tón
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
půltón	půltón	k1gInSc1	půltón
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
dvou	dva	k4xCgInPc2	dva
půltónů	půltón	k1gInPc2	půltón
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
celý	celý	k2eAgInSc4d1	celý
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
intervalem	interval	k1gInSc7	interval
je	být	k5eAaImIp3nS	být
oktáva	oktáva	k1gFnSc1	oktáva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
rozmezí	rozmezí	k1gNnSc4	rozmezí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
na	na	k7c4	na
tóny	tón	k1gInPc4	tón
a	a	k8xC	a
půltóny	půltón	k1gInPc4	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Oktáva	oktáva	k1gFnSc1	oktáva
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
intervalem	interval	k1gInSc7	interval
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
primy	prima	k1gFnSc2	prima
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
stejně	stejně	k6eAd1	stejně
vysoké	vysoký	k2eAgInPc4d1	vysoký
tóny	tón	k1gInPc4	tón
-	-	kIx~	-
poměr	poměr	k1gInSc4	poměr
frekvencí	frekvence	k1gFnPc2	frekvence
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oktáva	oktáva	k1gFnSc1	oktáva
nejkonsonantnějším	konsonantní	k2eAgInSc7d3	konsonantní
intervalem	interval	k1gInSc7	interval
<g/>
:	:	kIx,	:
tóny	tón	k1gInPc1	tón
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
o	o	k7c6	o
oktávu	oktáv	k1gInSc6	oktáv
mají	mít	k5eAaImIp3nP	mít
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
možný	možný	k2eAgInSc4d1	možný
podíl	podíl	k1gInSc4	podíl
frekvencí	frekvence	k1gFnPc2	frekvence
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
tón	tón	k1gInSc1	tón
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
vyšší	vysoký	k2eAgFnSc4d2	vyšší
má	mít	k5eAaImIp3nS	mít
dvojnásobnou	dvojnásobný	k2eAgFnSc4d1	dvojnásobná
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
je	být	k5eAaImIp3nS	být
oktáva	oktáva	k1gFnSc1	oktáva
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvanáct	dvanáct	k4xCc4	dvanáct
půltónů	půltón	k1gInPc2	půltón
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
problematice	problematika	k1gFnSc6	problematika
v	v	k7c6	v
článku	článek	k1gInSc6	článek
ladění	ladění	k1gNnSc2	ladění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Intervaly	interval	k1gInPc1	interval
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
o	o	k7c4	o
kolik	kolik	k4yRc4	kolik
stupňů	stupeň	k1gInPc2	stupeň
diatonické	diatonický	k2eAgFnSc2d1	diatonická
stupnice	stupnice	k1gFnSc2	stupnice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
C	C	kA	C
dur	dur	k1gNnSc4	dur
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
oba	dva	k4xCgInPc1	dva
tóny	tón	k1gInPc1	tón
intervalu	interval	k1gInSc2	interval
vzdáleny	vzdálit	k5eAaPmNgFnP	vzdálit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
názvy	název	k1gInPc1	název
jsou	být	k5eAaImIp3nP	být
počeštělé	počeštělý	k2eAgFnSc2d1	počeštělá
latinské	latinský	k2eAgFnSc2d1	Latinská
řadové	řadový	k2eAgFnSc2d1	řadová
číslovky	číslovka	k1gFnSc2	číslovka
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
rodě	rod	k1gInSc6	rod
(	(	kIx(	(
<g/>
prima	primo	k1gNnSc2	primo
=	=	kIx~	=
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
secunda	secunda	k1gFnSc1	secunda
=	=	kIx~	=
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
tertia	tertium	k1gNnSc2	tertium
=	=	kIx~	=
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názvy	název	k1gInPc1	název
platí	platit	k5eAaImIp3nP	platit
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
diatonické	diatonický	k2eAgFnPc4d1	diatonická
stupnice	stupnice	k1gFnPc4	stupnice
-	-	kIx~	-
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
durové	durový	k2eAgMnPc4d1	durový
<g/>
,	,	kIx,	,
mollové	mollový	k2eAgFnPc1d1	mollová
a	a	k8xC	a
církevní	církevní	k2eAgFnPc1d1	církevní
hudební	hudební	k2eAgFnPc1d1	hudební
stupnice	stupnice	k1gFnPc1	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
níže	nízce	k6eAd2	nízce
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
příklady	příklad	k1gInPc1	příklad
pro	pro	k7c4	pro
stupnice	stupnice	k1gFnPc4	stupnice
C	C	kA	C
dur	dur	k1gNnSc2	dur
a	a	k8xC	a
e	e	k0	e
moll	moll	k1gNnSc6	moll
<g/>
.	.	kIx.	.
</s>
<s>
Diatonická	diatonický	k2eAgFnSc1d1	diatonická
stupnice	stupnice	k1gFnSc1	stupnice
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
sedmitónové	sedmitónový	k2eAgFnSc2d1	sedmitónový
řady	řada	k1gFnSc2	řada
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
oktáva	oktáva	k1gFnSc1	oktáva
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
12	[number]	k4	12
půltónů	půltón	k1gInPc2	půltón
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
prima	prima	k1gFnSc1	prima
<g/>
"	"	kIx"	"
až	až	k9	až
"	"	kIx"	"
<g/>
oktáva	oktáva	k1gFnSc1	oktáva
<g/>
"	"	kIx"	"
tedy	tedy	k9	tedy
nestačí	stačit	k5eNaBmIp3nS	stačit
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
všech	všecek	k3xTgMnPc2	všecek
dvanácti	dvanáct	k4xCc2	dvanáct
intervalů	interval	k1gInPc2	interval
v	v	k7c6	v
dvanáctitónové	dvanáctitónový	k2eAgFnSc6d1	dvanáctitónová
chromatické	chromatický	k2eAgFnSc6d1	chromatická
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nP	zavádět
doplňková	doplňkový	k2eAgNnPc1d1	doplňkové
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
<g/>
:	:	kIx,	:
čistá	čistý	k2eAgNnPc1d1	čisté
<g/>
,	,	kIx,	,
malá	malý	k2eAgNnPc1d1	malé
a	a	k8xC	a
velká	velký	k2eAgNnPc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Prima	prima	k1gFnSc1	prima
<g/>
,	,	kIx,	,
kvarta	kvarta	k1gFnSc1	kvarta
<g/>
,	,	kIx,	,
kvinta	kvinta	k1gFnSc1	kvinta
a	a	k8xC	a
oktáva	oktáva	k1gFnSc1	oktáva
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
;	;	kIx,	;
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
tercie	tercie	k1gFnSc1	tercie
<g/>
,	,	kIx,	,
sexta	sexta	k1gFnSc1	sexta
a	a	k8xC	a
septima	septima	k1gFnSc1	septima
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
malé	malý	k2eAgInPc4d1	malý
nebo	nebo	k8xC	nebo
velké	velký	k2eAgInPc4d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgInPc1d1	čistý
intervaly	interval	k1gInPc1	interval
mají	mít	k5eAaImIp3nP	mít
svoje	svůj	k3xOyFgNnSc4	svůj
označení	označení	k1gNnSc4	označení
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
poměr	poměr	k1gInSc4	poměr
frekvencí	frekvence	k1gFnPc2	frekvence
jejich	jejich	k3xOp3gInPc2	jejich
tónů	tón	k1gInPc2	tón
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
temperovaném	temperovaný	k2eAgNnSc6d1	temperované
ladění	ladění	k1gNnSc6	ladění
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
poměru	poměr	k1gInSc3	poměr
malých	malý	k2eAgFnPc2d1	malá
celých	celý	k2eAgFnPc2d1	celá
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
používá	používat	k5eAaImIp3nS	používat
čisté	čistý	k2eAgNnSc4d1	čisté
ladění	ladění	k1gNnSc4	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
interval	interval	k1gInSc1	interval
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dále	daleko	k6eAd2	daleko
zvětšený	zvětšený	k2eAgMnSc1d1	zvětšený
či	či	k8xC	či
zmenšený	zmenšený	k2eAgMnSc1d1	zmenšený
<g/>
.	.	kIx.	.
</s>
<s>
Zvětšený	zvětšený	k2eAgInSc1d1	zvětšený
interval	interval	k1gInSc1	interval
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
půltón	půltón	k1gInSc4	půltón
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
velký	velký	k2eAgInSc4d1	velký
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zmenšený	zmenšený	k2eAgInSc1d1	zmenšený
interval	interval	k1gInSc1	interval
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
půltón	půltón	k1gInSc4	půltón
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
malý	malý	k2eAgInSc4d1	malý
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvojzvětšený	dvojzvětšený	k2eAgInSc1d1	dvojzvětšený
interval	interval	k1gInSc1	interval
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
půltón	půltón	k1gInSc4	půltón
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
zvětšený	zvětšený	k2eAgInSc4d1	zvětšený
<g/>
,	,	kIx,	,
dvojzmenšený	dvojzmenšený	k2eAgInSc4d1	dvojzmenšený
interval	interval	k1gInSc4	interval
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
půltón	půltón	k1gInSc4	půltón
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
zmenšený	zmenšený	k2eAgInSc4d1	zmenšený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
dole	dole	k6eAd1	dole
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
úsporných	úsporný	k2eAgInPc2d1	úsporný
důvodů	důvod	k1gInPc2	důvod
příklady	příklad	k1gInPc4	příklad
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
tercii	tercie	k1gFnSc4	tercie
a	a	k8xC	a
kvartu	kvarta	k1gFnSc4	kvarta
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
omezením	omezení	k1gNnSc7	omezení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc1	žádný
tón	tón	k1gInSc1	tón
nesmí	smět	k5eNaImIp3nS	smět
mít	mít	k5eAaImF	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
křížky	křížek	k1gInPc4	křížek
(	(	kIx(	(
<g/>
#	#	kIx~	#
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvě	dva	k4xCgFnPc4	dva
bé	bé	k0	bé
(	(	kIx(	(
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
k	k	k7c3	k
výslovnosti	výslovnost	k1gFnSc3	výslovnost
<g/>
:	:	kIx,	:
Fbb	Fbb	k1gFnSc1	Fbb
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
feses	feses	k1gNnSc1	feses
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fb	Fb	k1gMnSc1	Fb
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
fes	fes	k1gNnSc1	fes
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
F	F	kA	F
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ef	ef	k?	ef
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
#	#	kIx~	#
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
fis	fis	k1gNnSc1	fis
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
##	##	k?	##
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
fisis	fisis	k1gNnSc1	fisis
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Pokud	pokud	k8xS	pokud
provedeme	provést	k5eAaPmIp1nP	provést
enharmonickou	enharmonický	k2eAgFnSc4d1	enharmonická
záměnu	záměna	k1gFnSc4	záměna
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jeden	jeden	k4xCgInSc4	jeden
interval	interval	k1gInSc4	interval
více	hodně	k6eAd2	hodně
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
interval	interval	k1gInSc4	interval
obsahující	obsahující	k2eAgInPc4d1	obsahující
čtyři	čtyři	k4xCgInPc4	čtyři
půltóny	půltón	k1gInPc4	půltón
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
tercie	tercie	k1gFnSc1	tercie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
C	C	kA	C
-	-	kIx~	-
E	E	kA	E
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
nazývat	nazývat	k5eAaImF	nazývat
dvojzvětšená	dvojzvětšený	k2eAgFnSc1d1	dvojzvětšený
sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
C	C	kA	C
-	-	kIx~	-
D	D	kA	D
<g/>
##	##	k?	##
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zmenšená	zmenšený	k2eAgFnSc1d1	zmenšená
kvarta	kvarta	k1gFnSc1	kvarta
(	(	kIx(	(
<g/>
C	C	kA	C
-	-	kIx~	-
Fb	Fb	k1gMnSc1	Fb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Varianty	varianta	k1gFnSc2	varianta
intervalů	interval	k1gInPc2	interval
s	s	k7c7	s
"	"	kIx"	"
<g/>
neobvyklými	obvyklý	k2eNgInPc7d1	neobvyklý
<g/>
"	"	kIx"	"
názvy	název	k1gInPc7	název
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
kvůli	kvůli	k7c3	kvůli
logice	logika	k1gFnSc3	logika
hudební	hudební	k2eAgFnSc2d1	hudební
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
:	:	kIx,	:
např.	např.	kA	např.
malá	malý	k2eAgFnSc1d1	malá
septima	septima	k1gFnSc1	septima
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
harmonii	harmonie	k1gFnSc6	harmonie
jinou	jiný	k2eAgFnSc4d1	jiná
funkci	funkce	k1gFnSc4	funkce
než	než	k8xS	než
zvětšená	zvětšený	k2eAgFnSc1d1	zvětšená
sexta	sexta	k1gFnSc1	sexta
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
oba	dva	k4xCgInPc1	dva
intervaly	interval	k1gInPc1	interval
v	v	k7c6	v
temperovaném	temperovaný	k2eAgNnSc6d1	temperované
ladění	ladění	k1gNnSc6	ladění
znějí	znět	k5eAaImIp3nP	znět
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Provedeme	provést	k5eAaPmIp1nP	provést
<g/>
-li	i	k?	-li
tedy	tedy	k9	tedy
enharmonickou	enharmonický	k2eAgFnSc4d1	enharmonická
záměnu	záměna	k1gFnSc4	záměna
v	v	k7c6	v
zápisu	zápis	k1gInSc6	zápis
not	nota	k1gFnPc2	nota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
místo	místo	k7c2	místo
G	G	kA	G
-	-	kIx~	-
E	E	kA	E
<g/>
#	#	kIx~	#
napíšeme	napsat	k5eAaPmIp1nP	napsat
G	G	kA	G
-	-	kIx~	-
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posluchač	posluchač	k1gMnSc1	posluchač
klavírní	klavírní	k2eAgFnSc2d1	klavírní
skladby	skladba	k1gFnSc2	skladba
nepozná	poznat	k5eNaPmIp3nS	poznat
žádný	žádný	k3yNgInSc1	žádný
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
harmonický	harmonický	k2eAgInSc1d1	harmonický
záměr	záměr	k1gInSc1	záměr
skladatele	skladatel	k1gMnSc2	skladatel
přestane	přestat	k5eAaPmIp3nS	přestat
být	být	k5eAaImF	být
z	z	k7c2	z
notového	notový	k2eAgInSc2d1	notový
zápisu	zápis	k1gInSc2	zápis
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
<g/>
..	..	k?	..
Přesné	přesný	k2eAgNnSc1d1	přesné
frekvence	frekvence	k1gFnPc4	frekvence
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
stupňů	stupeň	k1gInPc2	stupeň
závisejí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
použitém	použitý	k2eAgNnSc6d1	Použité
ladění	ladění	k1gNnSc6	ladění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čistých	čistá	k1gFnPc6	čistá
či	či	k8xC	či
přirozených	přirozený	k2eAgNnPc6d1	přirozené
laděních	ladění	k1gNnPc6	ladění
jsou	být	k5eAaImIp3nP	být
frekvence	frekvence	k1gFnPc1	frekvence
tónů	tón	k1gInPc2	tón
ve	v	k7c6	v
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
poměrech	poměr	k1gInPc6	poměr
<g/>
,	,	kIx,	,
vyjádřitelných	vyjádřitelný	k2eAgFnPc6d1	vyjádřitelná
celými	celý	k2eAgNnPc7d1	celé
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
poměry	poměr	k1gInPc1	poměr
jsou	být	k5eAaImIp3nP	být
dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
označovat	označovat	k5eAaImF	označovat
intervaly	interval	k1gInPc4	interval
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
oktáva	oktáva	k1gFnSc1	oktáva
je	být	k5eAaImIp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
poměrem	poměr	k1gInSc7	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
čistá	čistý	k2eAgFnSc1d1	čistá
kvinta	kvinta	k1gFnSc1	kvinta
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
atd.	atd.	kA	atd.
Pokud	pokud	k8xS	pokud
poměr	poměr	k1gInSc1	poměr
frekvencí	frekvence	k1gFnPc2	frekvence
nelze	lze	k6eNd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
zlomek	zlomek	k1gInSc4	zlomek
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
vhodné	vhodný	k2eAgNnSc1d1	vhodné
uvádět	uvádět	k5eAaImF	uvádět
jeho	jeho	k3xOp3gFnSc4	jeho
hodnotu	hodnota	k1gFnSc4	hodnota
jako	jako	k8xS	jako
desetinné	desetinný	k2eAgNnSc4d1	desetinné
číslo	číslo	k1gNnSc4	číslo
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vyjádření	vyjádření	k1gNnSc2	vyjádření
v	v	k7c6	v
centech	cent	k1gInPc6	cent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
temperované	temperovaný	k2eAgNnSc4d1	temperované
ladění	ladění	k1gNnSc4	ladění
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
žádný	žádný	k3yNgInSc4	žádný
interval	interval	k1gInSc4	interval
kromě	kromě	k7c2	kromě
oktávy	oktáva	k1gFnSc2	oktáva
nelze	lze	k6eNd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
zlomek	zlomek	k1gInSc4	zlomek
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
velká	velký	k2eAgFnSc1d1	velká
tercie	tercie	k1gFnSc1	tercie
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
1,259	[number]	k4	1,259
<g/>
921	[number]	k4	921
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
400	[number]	k4	400
centů	cent	k1gInPc2	cent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohé	mnohý	k2eAgFnSc6d1	mnohá
mimoevropské	mimoevropský	k2eAgFnSc6d1	mimoevropská
hudbě	hudba	k1gFnSc6	hudba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c4	v
arabské	arabský	k2eAgNnSc4d1	arabské
<g/>
,	,	kIx,	,
perské	perský	k2eAgNnSc4d1	perské
<g/>
,	,	kIx,	,
indické	indický	k2eAgNnSc4d1	indické
<g/>
,	,	kIx,	,
thaiské	thaiský	k2eAgNnSc4d1	thaiský
<g/>
,	,	kIx,	,
africké	africký	k2eAgNnSc4d1	africké
<g/>
,	,	kIx,	,
indonéské	indonéský	k2eAgNnSc4d1	indonéské
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
intervaly	interval	k1gInPc1	interval
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
intervaly	interval	k1gInPc7	interval
odlišnými	odlišný	k2eAgInPc7d1	odlišný
od	od	k7c2	od
dnešních	dnešní	k2eAgInPc2d1	dnešní
pracovala	pracovat	k5eAaImAgFnS	pracovat
i	i	k9	i
starořecká	starořecký	k2eAgFnSc1d1	starořecká
hudební	hudební	k2eAgFnSc1d1	hudební
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Neobvyklé	obvyklý	k2eNgInPc1d1	neobvyklý
intervaly	interval	k1gInPc1	interval
lze	lze	k6eAd1	lze
občas	občas	k6eAd1	občas
nalézt	nalézt	k5eAaPmF	nalézt
též	též	k9	též
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
lidové	lidový	k2eAgFnSc6d1	lidová
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
Slovácku	Slovácko	k1gNnSc6	Slovácko
i	i	k8xC	i
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
umělé	umělý	k2eAgFnPc1d1	umělá
(	(	kIx(	(
<g/>
Alois	Alois	k1gMnSc1	Alois
Hába	Hába	k1gMnSc1	Hába
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgInSc1d1	Eugen
Ysaye	Ysaye	k1gInSc1	Ysaye
<g/>
,	,	kIx,	,
Nicola	Nicola	k1gFnSc1	Nicola
Vicentino	Vicentina	k1gFnSc5	Vicentina
<g/>
,	,	kIx,	,
Adriaan	Adriaan	k1gMnSc1	Adriaan
Fokker	Fokker	k1gMnSc1	Fokker
<g/>
,	,	kIx,	,
Ivor	Ivor	k1gMnSc1	Ivor
Darreg	Darreg	k1gMnSc1	Darreg
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Erlich	Erlich	k1gMnSc1	Erlich
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Ives	Ives	k1gInSc1	Ives
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Wyschnegradsky	Wyschnegradsky	k1gMnSc1	Wyschnegradsky
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Schillinger	Schillinger	k1gMnSc1	Schillinger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hudba	hudba	k1gFnSc1	hudba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mikrotonální	mikrotonální	k2eAgFnSc1d1	mikrotonální
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
tóny	tón	k1gInPc7	tón
může	moct	k5eAaImIp3nS	moct
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
třetiny	třetina	k1gFnPc4	třetina
(	(	kIx(	(
<g/>
třetinotóny	třetinotóna	k1gFnPc4	třetinotóna
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
(	(	kIx(	(
<g/>
čtvrttóny	čtvrttón	k1gInPc1	čtvrttón
<g/>
)	)	kIx)	)
i	i	k9	i
menší	malý	k2eAgInPc4d2	menší
díly	díl	k1gInPc4	díl
<g/>
,	,	kIx,	,
používat	používat	k5eAaImF	používat
intervalů	interval	k1gInPc2	interval
větších	veliký	k2eAgInPc2d2	veliký
nebo	nebo	k8xC	nebo
konstruovaných	konstruovaný	k2eAgInPc2d1	konstruovaný
zcela	zcela	k6eAd1	zcela
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
stupnice	stupnice	k1gFnSc1	stupnice
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
ladění	ladění	k1gNnSc4	ladění
kvintový	kvintový	k2eAgInSc1d1	kvintový
kruh	kruh	k1gInSc1	kruh
transpozice	transpozice	k1gFnSc1	transpozice
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Interval	interval	k1gInSc4	interval
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
http://www.brozak.org/iksh+wts.gif	[url]	k1gMnSc1	http://www.brozak.org/iksh+wts.gif
http://www.instrumento.cz/clanky.php?tar=show_clanek&	[url]	k?	http://www.instrumento.cz/clanky.php?tar=show_clanek&
http://www.instrumento.cz/clanky.php?tar=show_clanek&	[url]	k?	http://www.instrumento.cz/clanky.php?tar=show_clanek&
Kurz	kurz	k1gInSc1	kurz
harmonie	harmonie	k1gFnSc2	harmonie
bez	bez	k7c2	bez
not	nota	k1gFnPc2	nota
-	-	kIx~	-
Intervaly	interval	k1gInPc1	interval
</s>
