<s>
Nejmenší	malý	k2eAgFnSc4d3	nejmenší
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
tóny	tón	k1gInPc7	tón
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
půltón	půltón	k1gInSc1	půltón
<g/>
,	,	kIx,	,
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
dvou	dva	k4xCgInPc2	dva
půltónů	půltón	k1gInPc2	půltón
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
celý	celý	k2eAgInSc4d1	celý
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
