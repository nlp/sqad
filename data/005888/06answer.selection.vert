<s>
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
chladná	chladný	k2eAgFnSc1d1	chladná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
na	na	k7c6	na
Hertzsprungově-Russellově	Hertzsprungově-Russellův	k2eAgInSc6d1	Hertzsprungově-Russellův
diagramu	diagram	k1gInSc6	diagram
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
posloupnosti	posloupnost	k1gFnSc6	posloupnost
pozdního	pozdní	k2eAgInSc2d1	pozdní
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
K	k	k7c3	k
případně	případně	k6eAd1	případně
M.	M.	kA	M.
</s>
