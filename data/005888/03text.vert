<s>
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
chladná	chladný	k2eAgFnSc1d1	chladná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
na	na	k7c6	na
Hertzsprungově-Russellově	Hertzsprungově-Russellův	k2eAgInSc6d1	Hertzsprungově-Russellův
diagramu	diagram	k1gInSc6	diagram
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
posloupnosti	posloupnost	k1gFnSc6	posloupnost
pozdního	pozdní	k2eAgInSc2d1	pozdní
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
K	k	k7c3	k
případně	případně	k6eAd1	případně
M.	M.	kA	M.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
hvězd	hvězda	k1gFnPc2	hvězda
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
třetina	třetina	k1gFnSc1	třetina
sluneční	sluneční	k2eAgFnSc1d1	sluneční
(	(	kIx(	(
<g/>
s	s	k7c7	s
dolní	dolní	k2eAgFnSc7d1	dolní
mezí	mez	k1gFnSc7	mez
0,08	[number]	k4	0,08
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hmotností	hmotnost	k1gFnPc2	hmotnost
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
hnědí	hnědý	k2eAgMnPc1d1	hnědý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nejpočetnějším	početní	k2eAgInSc7d3	nejpočetnější
typem	typ	k1gInSc7	typ
hvězd	hvězda	k1gFnPc2	hvězda
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
–	–	k?	–
okolo	okolo	k7c2	okolo
75	[number]	k4	75
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Galaxii	galaxie	k1gFnSc6	galaxie
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
červení	červený	k2eAgMnPc1d1	červený
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Červení	červený	k2eAgMnPc1d1	červený
trpaslíci	trpaslík	k1gMnPc1	trpaslík
mají	mít	k5eAaImIp3nP	mít
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
teplotu	teplota	k1gFnSc4	teplota
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
3	[number]	k4	3
500	[number]	k4	500
K.	K.	kA	K.
Vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
jen	jen	k9	jen
málo	málo	k4c4	málo
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
jen	jen	k9	jen
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
000	[number]	k4	000
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pomalému	pomalý	k2eAgNnSc3d1	pomalé
spalování	spalování	k1gNnSc3	spalování
vodíku	vodík	k1gInSc2	vodík
mají	mít	k5eAaImIp3nP	mít
enormně	enormně	k6eAd1	enormně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
odhadovanou	odhadovaný	k2eAgFnSc4d1	odhadovaná
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
od	od	k7c2	od
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
miliard	miliarda	k4xCgFnPc2	miliarda
až	až	k9	až
po	po	k7c6	po
bilióny	bilión	k4xCgInPc1	bilión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Červení	červený	k2eAgMnPc1d1	červený
trpaslíci	trpaslík	k1gMnPc1	trpaslík
nikdy	nikdy	k6eAd1	nikdy
nezažehnou	zažehnout	k5eNaPmIp3nP	zažehnout
jadernou	jaderný	k2eAgFnSc4d1	jaderná
fúzi	fúze	k1gFnSc4	fúze
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
nemohou	moct	k5eNaImIp3nP	moct
stát	stát	k5eAaImF	stát
rudými	rudý	k2eAgMnPc7d1	rudý
obry	obr	k1gMnPc7	obr
<g/>
;	;	kIx,	;
zvolna	zvolna	k6eAd1	zvolna
se	se	k3xPyFc4	se
smršťují	smršťovat	k5eAaImIp3nP	smršťovat
a	a	k8xC	a
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nespotřebují	spotřebovat	k5eNaPmIp3nP	spotřebovat
všechen	všechen	k3xTgInSc4	všechen
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
neuplynulo	uplynout	k5eNaPmAgNnS	uplynout
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
stihl	stihnout	k5eAaPmAgMnS	stihnout
opustit	opustit	k5eAaPmF	opustit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opuštění	opuštění	k1gNnSc6	opuštění
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
modrým	modrý	k2eAgMnSc7d1	modrý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bude	být	k5eAaImBp3nS	být
stále	stále	k6eAd1	stále
spalovat	spalovat	k5eAaImF	spalovat
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
vyčerpá	vyčerpat	k5eAaPmIp3nS	vyčerpat
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
bílým	bílý	k2eAgMnSc7d1	bílý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
červené	červený	k2eAgMnPc4d1	červený
trpaslíky	trpaslík	k1gMnPc4	trpaslík
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
i	i	k9	i
hvězda	hvězda	k1gFnSc1	hvězda
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
4,2	[number]	k4	4,2
světelného	světelný	k2eAgInSc2d1	světelný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hypotetickým	hypotetický	k2eAgMnSc7d1	hypotetický
zástupcem	zástupce	k1gMnSc7	zástupce
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
také	také	k9	také
temná	temný	k2eAgFnSc1d1	temná
hvězda	hvězda	k1gFnSc1	hvězda
Nemesis	Nemesis	k1gFnSc1	Nemesis
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
astronomů	astronom	k1gMnPc2	astronom
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
pravidelných	pravidelný	k2eAgNnPc2d1	pravidelné
vymírání	vymírání	k1gNnPc2	vymírání
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
takto	takto	k6eAd1	takto
blízká	blízký	k2eAgFnSc1d1	blízká
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
1,5	[number]	k4	1,5
-	-	kIx~	-
3	[number]	k4	3
světelné	světelný	k2eAgInPc1d1	světelný
roky	rok	k1gInPc1	rok
<g/>
)	)	kIx)	)
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ani	ani	k9	ani
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
Černý	Černý	k1gMnSc1	Černý
trpaslík	trpaslík	k1gMnSc1	trpaslík
Modrý	modrý	k2eAgMnSc1d1	modrý
trpaslík	trpaslík	k1gMnSc1	trpaslík
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
