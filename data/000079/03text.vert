<s>
Semestr	semestr	k1gInSc1	semestr
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
semestris	semestris	k1gFnSc2	semestris
-	-	kIx~	-
šestiměsíční	šestiměsíční	k2eAgInSc4d1	šestiměsíční
<g/>
:	:	kIx,	:
sex	sex	k1gInSc4	sex
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
<g/>
,	,	kIx,	,
a	a	k8xC	a
mensis	mensis	k1gInSc1	mensis
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc1	měsíc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
pololetí	pololetí	k1gNnSc4	pololetí
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
(	(	kIx(	(
<g/>
zimní	zimní	k2eAgInSc4d1	zimní
semestr	semestr	k1gInSc4	semestr
<g/>
,	,	kIx,	,
letní	letní	k2eAgInSc4d1	letní
semestr	semestr	k1gInSc4	semestr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trimestr	trimestr	k1gInSc4	trimestr
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
semestr	semestr	k1gInSc1	semestr
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
