<s>
Gabriel	Gabriel	k1gMnSc1	Gabriel
José	José	k1gNnSc2	José
García	García	k1gMnSc1	García
Márquez	Márquez	k1gMnSc1	Márquez
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Aracataca	Aracataca	k1gFnSc1	Aracataca
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k1gNnSc1	México
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
kolumbijský	kolumbijský	k2eAgMnSc1d1	kolumbijský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
píšící	píšící	k2eAgMnSc1d1	píšící
španělsky	španělsky	k6eAd1	španělsky
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
nejdůležitějšího	důležitý	k2eAgMnSc4d3	nejdůležitější
představitele	představitel	k1gMnSc4	představitel
tzv.	tzv.	kA	tzv.
magického	magický	k2eAgInSc2d1	magický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
příliš	příliš	k6eAd1	příliš
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Aracataca	Aracatac	k1gInSc2	Aracatac
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
prarodiči	prarodič	k1gMnPc7	prarodič
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
významně	významně	k6eAd1	významně
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
pozdější	pozdní	k2eAgFnSc4d2	pozdější
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
byl	být	k5eAaImAgMnS	být
plukovníkem	plukovník	k1gMnSc7	plukovník
v	v	k7c6	v
kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
liberálů	liberál	k1gMnPc2	liberál
<g/>
,	,	kIx,	,
babička	babička	k1gFnSc1	babička
pak	pak	k6eAd1	pak
pověrčivá	pověrčivý	k2eAgFnSc1d1	pověrčivá
žena	žena	k1gFnSc1	žena
znalá	znalý	k2eAgFnSc1d1	znalá
lidových	lidový	k2eAgFnPc2d1	lidová
pověstí	pověst	k1gFnSc7	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Bogotě	Bogota	k1gFnSc6	Bogota
<g/>
,	,	kIx,	,
na	na	k7c6	na
vysoké	vysoká	k1gFnSc6	vysoká
studoval	studovat	k5eAaImAgMnS	studovat
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studia	studio	k1gNnSc2	studio
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
novinářem	novinář	k1gMnSc7	novinář
a	a	k8xC	a
dopisovatelem	dopisovatel	k1gMnSc7	dopisovatel
různých	různý	k2eAgFnPc2d1	různá
provinčních	provinční	k2eAgFnPc2d1	provinční
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zpravodajem	zpravodaj	k1gMnSc7	zpravodaj
významného	významný	k2eAgInSc2d1	významný
bogotského	bogotský	k2eAgInSc2d1	bogotský
listu	list	k1gInSc2	list
El	Ela	k1gFnPc2	Ela
Espectador	Espectadora	k1gFnPc2	Espectadora
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyslán	vyslat	k5eAaPmNgInS	vyslat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
noviny	novina	k1gFnPc1	novina
však	však	k9	však
byly	být	k5eAaImAgFnP	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
příjezdu	příjezd	k1gInSc6	příjezd
úředně	úředně	k6eAd1	úředně
zastaveny	zastavit	k5eAaPmNgFnP	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
tamním	tamní	k2eAgNnSc6d1	tamní
hotelu	hotel	k1gInSc6	hotel
De	De	k?	De
Flandre	flandr	k1gInSc5	flandr
a	a	k8xC	a
psal	psát	k5eAaImAgInS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
stala	stát	k5eAaPmAgFnS	stát
Mercedes	mercedes	k1gInSc4	mercedes
Barcha	Barcha	k1gFnSc1	Barcha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castr	k1gMnSc2	Castr
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dopisovatelem	dopisovatel	k1gMnSc7	dopisovatel
kubánské	kubánský	k2eAgFnSc2d1	kubánská
tiskové	tiskový	k2eAgFnSc2d1	tisková
agentury	agentura	k1gFnSc2	agentura
Prensa	Prens	k1gMnSc2	Prens
Latina	Latin	k1gMnSc2	Latin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
napsal	napsat	k5eAaPmAgInS	napsat
několik	několik	k4yIc4	několik
povídkových	povídkový	k2eAgFnPc2d1	povídková
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
ovšem	ovšem	k9	ovšem
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
spokojen	spokojen	k2eAgMnSc1d1	spokojen
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
psaní	psaní	k1gNnSc2	psaní
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
dokonce	dokonce	k9	dokonce
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
režii	režie	k1gFnSc4	režie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krátko	krátko	k6eAd1	krátko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
psal	psát	k5eAaImAgInS	psát
i	i	k9	i
filmové	filmový	k2eAgInPc4d1	filmový
scénáře	scénář	k1gInPc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
dostavil	dostavit	k5eAaPmAgMnS	dostavit
literární	literární	k2eAgInSc4d1	literární
úspěch	úspěch	k1gInSc4	úspěch
-	-	kIx~	-
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
<g/>
,	,	kIx,	,
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
psal	psát	k5eAaImAgInS	psát
zavřený	zavřený	k2eAgInSc1d1	zavřený
v	v	k7c6	v
pracovně	pracovna	k1gFnSc6	pracovna
po	po	k7c4	po
osmnáct	osmnáct	k4xCc4	osmnáct
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
konečně	konečně	k6eAd1	konečně
vyšel	vyjít	k5eAaPmAgInS	vyjít
s	s	k7c7	s
třinácti	třináct	k4xCc7	třináct
sty	sto	k4xCgNnPc7	sto
stránkami	stránka	k1gFnPc7	stránka
rukopisu	rukopis	k1gInSc2	rukopis
notně	notně	k6eAd1	notně
čpícími	čpící	k2eAgInPc7d1	čpící
nikotinem	nikotin	k1gInSc7	nikotin
<g/>
.	.	kIx.	.
</s>
<s>
Prodeje	prodej	k1gInPc1	prodej
pomohly	pomoct	k5eAaPmAgInP	pomoct
Garcíovi	García	k1gMnSc3	García
Márquezovi	Márquez	k1gMnSc3	Márquez
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
z	z	k7c2	z
dluhů	dluh	k1gInPc2	dluh
a	a	k8xC	a
poskytly	poskytnout	k5eAaPmAgInP	poskytnout
mu	on	k3xPp3gMnSc3	on
možnost	možnost	k1gFnSc4	možnost
věnovat	věnovat	k5eAaPmF	věnovat
se	s	k7c7	s
psaní	psaní	k1gNnSc2	psaní
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
románu	román	k1gInSc2	román
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vydal	vydat	k5eAaPmAgInS	vydat
Neuvěřitelný	uvěřitelný	k2eNgInSc1d1	neuvěřitelný
a	a	k8xC	a
tklivý	tklivý	k2eAgInSc1d1	tklivý
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
bezelstné	bezelstný	k2eAgFnSc6d1	bezelstná
Eréndiře	Eréndira	k1gFnSc6	Eréndira
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
ukrutné	ukrutný	k2eAgFnSc2d1	ukrutná
babičce	babička	k1gFnSc3	babička
a	a	k8xC	a
Podzim	podzim	k1gInSc1	podzim
patriarchy	patriarcha	k1gMnSc2	patriarcha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
rodný	rodný	k2eAgInSc4d1	rodný
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
romány	román	k1gInPc4	román
a	a	k8xC	a
povídky	povídka	k1gFnPc4	povídka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
neskutečné	skutečný	k2eNgNnSc4d1	neskutečné
snoubí	snoubit	k5eAaImIp3nS	snoubit
s	s	k7c7	s
reálným	reálný	k2eAgNnSc7d1	reálné
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
plném	plný	k2eAgInSc6d1	plný
představivosti	představivost	k1gFnSc6	představivost
a	a	k8xC	a
odrážejícím	odrážející	k2eAgInSc6d1	odrážející
život	život	k1gInSc4	život
a	a	k8xC	a
konflikty	konflikt	k1gInPc4	konflikt
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
jako	jako	k8xS	jako
přítel	přítel	k1gMnSc1	přítel
Fidela	Fidel	k1gMnSc2	Fidel
Castra	Castrum	k1gNnSc2	Castrum
a	a	k8xC	a
příznivec	příznivec	k1gMnSc1	příznivec
různých	různý	k2eAgFnPc2d1	různá
revolučních	revoluční	k2eAgFnPc2d1	revoluční
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
nadsázek	nadsázka	k1gFnPc2	nadsázka
a	a	k8xC	a
fantastických	fantastický	k2eAgInPc2d1	fantastický
motivů	motiv	k1gInPc2	motiv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
všedního	všední	k2eAgInSc2d1	všední
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
rysem	rys	k1gInSc7	rys
je	být	k5eAaImIp3nS	být
vyprávění	vyprávění	k1gNnSc1	vyprávění
velkých	velký	k2eAgInPc2d1	velký
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
roztříští	roztříštit	k5eAaPmIp3nP	roztříštit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
bezvýznamných	bezvýznamný	k2eAgFnPc2d1	bezvýznamná
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dílo	dílo	k1gNnSc1	dílo
stává	stávat	k5eAaImIp3nS	stávat
záměrně	záměrně	k6eAd1	záměrně
nepřehledným	přehledný	k2eNgNnSc7d1	nepřehledné
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
neplyne	plynout	k5eNaImIp3nS	plynout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
otáčí	otáčet	k5eAaImIp3nS	otáčet
se	se	k3xPyFc4	se
dokola	dokola	k6eAd1	dokola
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgNnPc7d1	základní
tématy	téma	k1gNnPc7	téma
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
samota	samota	k1gFnSc1	samota
a	a	k8xC	a
stárnutí	stárnutí	k1gNnSc1	stárnutí
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
postavám	postava	k1gFnPc3	postava
přikládá	přikládat	k5eAaImIp3nS	přikládat
mythologické	mythologický	k2eAgInPc4d1	mythologický
rozměry	rozměr	k1gInPc4	rozměr
a	a	k8xC	a
legendární	legendární	k2eAgFnPc4d1	legendární
vlastnosti	vlastnost	k1gFnPc4	vlastnost
ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
je	on	k3xPp3gMnPc4	on
jakýmkoliv	jakýkoliv	k3yIgNnPc3	jakýkoliv
svým	svůj	k3xOyFgInSc7	svůj
jednáním	jednání	k1gNnPc3	jednání
zasloužily	zasloužit	k5eAaPmAgFnP	zasloužit
<g/>
.	.	kIx.	.
1955	[number]	k4	1955
–	–	k?	–
Všechna	všechen	k3xTgNnPc1	všechen
špína	špína	k1gFnSc1	špína
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
La	la	k1gNnSc2	la
hojarasca	hojarasc	k1gInSc2	hojarasc
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
2006	[number]	k4	2006
ISBN	ISBN	kA	ISBN
80-207-1205-4	[number]	k4	80-207-1205-4
(	(	kIx(	(
<g/>
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
uváděno	uvádět	k5eAaImNgNnS	uvádět
též	též	k9	též
jako	jako	k9	jako
Spadané	spadaný	k2eAgNnSc4d1	spadané
listí	listí	k1gNnSc4	listí
<g/>
)	)	kIx)	)
1961	[number]	k4	1961
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Plukovníkovi	plukovník	k1gMnSc6	plukovník
nemá	mít	k5eNaImIp3nS	mít
kdo	kdo	k3yRnSc1	kdo
psát	psát	k5eAaImF	psát
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
coronel	coronlo	k1gNnPc2	coronlo
no	no	k9	no
tiene	tienout	k5eAaImIp3nS	tienout
quien	quien	k1gInSc1	quien
le	le	k?	le
escriba	escrib	k1gMnSc2	escrib
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
80-207-1212-7	[number]	k4	80-207-1212-7
1962	[number]	k4	1962
–	–	k?	–
Pohřeb	pohřeb	k1gInSc1	pohřeb
velké	velká	k1gFnSc2	velká
Matky	matka	k1gFnSc2	matka
(	(	kIx(	(
<g/>
Los	los	k1gInSc1	los
funerales	funerales	k1gInSc1	funerales
de	de	k?	de
la	la	k1gNnSc7	la
Mamá	mamá	k1gFnSc2	mamá
Grande	grand	k1gMnSc5	grand
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
978-80-207-1271-4	[number]	k4	978-80-207-1271-4
1962	[number]	k4	1962
–	–	k?	–
Oči	oko	k1gNnPc4	oko
modrého	modré	k1gNnSc2	modré
psa	pes	k1gMnSc2	pes
(	(	kIx(	(
<g/>
Ojos	Ojos	k1gInSc1	Ojos
de	de	k?	de
perro	perro	k6eAd1	perro
azul	azunout	k5eAaPmAgInS	azunout
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
2016	[number]	k4	2016
1962	[number]	k4	1962
–	–	k?	–
Zlá	zlý	k2eAgFnSc1d1	zlá
hodina	hodina	k1gFnSc1	hodina
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
mala	mala	k1gFnSc1	mala
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-1216-X	[number]	k4	80-207-1216-X
1967	[number]	k4	1967
–	–	k?	–
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
(	(	kIx(	(
<g/>
Cien	Cien	k1gInSc1	Cien
añ	añ	k?	añ
de	de	k?	de
soledad	soledad	k1gInSc1	soledad
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
80-207-1139-2	[number]	k4	80-207-1139-2
1970	[number]	k4	1970
–	–	k?	–
Zpověď	zpověď	k1gFnSc1	zpověď
trosečníka	trosečník	k1gMnSc2	trosečník
(	(	kIx(	(
<g/>
Relato	Relat	k2eAgNnSc1d1	Relat
de	de	k?	de
un	un	k?	un
náufrago	náufrago	k1gNnSc4	náufrago
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
<g />
.	.	kIx.	.
</s>
<s>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-1158-9	[number]	k4	80-207-1158-9
1972	[number]	k4	1972
–	–	k?	–
Neuvěřitelný	uvěřitelný	k2eNgInSc1d1	neuvěřitelný
a	a	k8xC	a
tklivý	tklivý	k2eAgInSc1d1	tklivý
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
bezelstné	bezelstný	k2eAgFnSc6d1	bezelstná
Eréndiře	Eréndira	k1gFnSc6	Eréndira
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
ukrutné	ukrutný	k2eAgFnSc3d1	ukrutná
babičce	babička	k1gFnSc3	babička
(	(	kIx(	(
<g/>
La	la	k1gNnSc4	la
increíble	increíble	k6eAd1	increíble
y	y	k?	y
triste	trisit	k5eAaPmRp2nP	trisit
historia	historium	k1gNnPc1	historium
de	de	k?	de
Cándida	Cándid	k1gMnSc2	Cándid
Eréndira	Eréndir	k1gMnSc2	Eréndir
y	y	k?	y
de	de	k?	de
su	su	k?	su
abuela	abuela	k1gFnSc1	abuela
desalmada	desalmada	k1gFnSc1	desalmada
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-207-1256-1	[number]	k4	978-80-207-1256-1
1975	[number]	k4	1975
–	–	k?	–
Podzim	podzim	k1gInSc1	podzim
patriarchy	patriarcha	k1gMnSc2	patriarcha
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
otoñ	otoñ	k?	otoñ
del	del	k?	del
<g />
.	.	kIx.	.
</s>
<s>
patriarca	patriarca	k1gFnSc1	patriarca
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
80-207-1171-6	[number]	k4	80-207-1171-6
1979	[number]	k4	1979
–	–	k?	–
V	v	k7c6	v
tomhle	tenhle	k3xDgInSc6	tenhle
městečku	městečko	k1gNnSc6	městečko
se	se	k3xPyFc4	se
nekrade	krást	k5eNaImIp3nS	krást
–	–	k?	–
český	český	k2eAgInSc1d1	český
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
El	Ela	k1gFnPc2	Ela
coronel	coronela	k1gFnPc2	coronela
no	no	k9	no
tiene	tienout	k5eAaPmIp3nS	tienout
quien	quien	k2eAgMnSc1d1	quien
le	le	k?	le
escriba	escriba	k1gMnSc1	escriba
<g/>
,	,	kIx,	,
Los	los	k1gMnSc1	los
funerales	funeralesa	k1gFnPc2	funeralesa
de	de	k?	de
la	la	k1gNnSc4	la
mamá	mamá	k1gFnSc2	mamá
grande	grand	k1gMnSc5	grand
a	a	k8xC	a
La	la	k1gNnSc7	la
inscreíble	inscreíble	k6eAd1	inscreíble
y	y	k?	y
triste	trisit	k5eAaPmRp2nP	trisit
historia	historium	k1gNnPc1	historium
de	de	k?	de
Cándida	Cándid	k1gMnSc2	Cándid
Eréndira	Eréndir	k1gMnSc2	Eréndir
y	y	k?	y
de	de	k?	de
su	su	k?	su
abuela	abuet	k5eAaPmAgFnS	abuet
desalmada	desalmada	k1gFnSc1	desalmada
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
978-3-87291-148-3	[number]	k4	978-3-87291-148-3
1981	[number]	k4	1981
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Kronika	kronika	k1gFnSc1	kronika
ohlášené	ohlášená	k1gFnSc2	ohlášená
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
Crónica	Crónica	k1gFnSc1	Crónica
de	de	k?	de
una	una	k?	una
muerte	muert	k1gInSc5	muert
anunciada	anunciada	k1gFnSc1	anunciada
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
80-207-1183-X	[number]	k4	80-207-1183-X
1985	[number]	k4	1985
–	–	k?	–
Láska	láska	k1gFnSc1	láska
za	za	k7c2	za
časů	čas	k1gInPc2	čas
cholery	cholera	k1gFnSc2	cholera
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
amor	amor	k1gMnSc1	amor
en	en	k?	en
los	los	k1gInSc1	los
tiempos	tiempos	k1gInSc1	tiempos
del	del	k?	del
cólera	cóler	k1gMnSc2	cóler
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
80-207-1144-9	[number]	k4	80-207-1144-9
–	–	k?	–
román	román	k1gInSc1	román
o	o	k7c4	o
velké	velká	k1gFnPc4	velká
nesmrtelné	smrtelný	k2eNgFnSc3d1	nesmrtelná
lásce	láska	k1gFnSc3	láska
<g/>
,	,	kIx,	,
Florentino	Florentin	k2eAgNnSc1d1	Florentino
Ariza	Ariz	k1gMnSc2	Ariz
miluje	milovat	k5eAaImIp3nS	milovat
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
začíná	začínat	k5eAaImIp3nS	začínat
ji	on	k3xPp3gFnSc4	on
milovat	milovat	k5eAaImF	milovat
v	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ona	onen	k3xDgFnSc1	onen
ho	on	k3xPp3gMnSc4	on
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
vdá	vdát	k5eAaPmIp3nS	vdát
se	se	k3xPyFc4	se
za	za	k7c4	za
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
,	,	kIx,	,
nenaplněná	naplněný	k2eNgFnSc1d1	nenaplněná
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
čeká	čekat	k5eAaImIp3nS	čekat
až	až	k9	až
ji	on	k3xPp3gFnSc4	on
manžel	manžel	k1gMnSc1	manžel
zemře	zemřít	k5eAaPmIp3nS	zemřít
(	(	kIx(	(
<g/>
72	[number]	k4	72
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k8xC	jako
hybný	hybný	k2eAgInSc1d1	hybný
princip	princip	k1gInSc1	princip
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
–	–	k?	–
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
Miguela	Miguel	k1gMnSc2	Miguel
Littína	Littín	k1gMnSc2	Littín
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
(	(	kIx(	(
<g/>
La	la	k1gNnSc2	la
aventura	aventura	k1gFnSc1	aventura
de	de	k?	de
Miguel	Miguel	k1gMnSc1	Miguel
Littín	Littín	k1gMnSc1	Littín
clandestino	clandestin	k2eAgNnSc1d1	clandestino
en	en	k?	en
Chile	Chile	k1gNnSc1	Chile
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
978-80-207-1229-5	[number]	k4	978-80-207-1229-5
1989	[number]	k4	1989
–	–	k?	–
Generál	generál	k1gMnSc1	generál
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
labyrintu	labyrint	k1gInSc6	labyrint
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
general	generat	k5eAaPmAgMnS	generat
en	en	k?	en
su	su	k?	su
laberinto	laberinto	k1gNnSc4	laberinto
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
80-207-1177-5	[number]	k4	80-207-1177-5
1992	[number]	k4	1992
–	–	k?	–
Dvanáct	dvanáct	k4xCc1	dvanáct
povídek	povídka	k1gFnPc2	povídka
o	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s>
poutnících	poutník	k1gMnPc6	poutník
(	(	kIx(	(
<g/>
Doce	Doce	k1gInSc1	Doce
cuentos	cuentos	k1gMnSc1	cuentos
peregrinos	peregrinos	k1gMnSc1	peregrinos
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
80-207-1191-0	[number]	k4	80-207-1191-0
1994	[number]	k4	1994
–	–	k?	–
O	o	k7c6	o
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
běsech	běs	k1gInPc6	běs
(	(	kIx(	(
<g/>
Del	Del	k1gMnSc1	Del
amor	amor	k1gMnSc1	amor
y	y	k?	y
otros	otros	k1gMnSc1	otros
demonios	demonios	k1gMnSc1	demonios
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
ISBN	ISBN	kA	ISBN
80-207-1151-1	[number]	k4	80-207-1151-1
1996	[number]	k4	1996
–	–	k?	–
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
jednom	jeden	k4xCgInSc6	jeden
únosu	únos	k1gInSc6	únos
(	(	kIx(	(
<g/>
Noticia	Noticia	k1gFnSc1	Noticia
de	de	k?	de
un	un	k?	un
secuestro	secuestro	k1gNnSc4	secuestro
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-207-1247-9	[number]	k4	978-80-207-1247-9
<g />
.	.	kIx.	.
</s>
<s>
2002	[number]	k4	2002
–	–	k?	–
Žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
mohl	moct	k5eAaImAgMnS	moct
vyprávět	vyprávět	k5eAaImF	vyprávět
(	(	kIx(	(
<g/>
Vivir	Vivir	k1gInSc4	Vivir
para	para	k2eAgNnSc2d1	para
contarla	contarlo	k1gNnSc2	contarlo
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-1150-3	[number]	k4	80-207-1150-3
2004	[number]	k4	2004
–	–	k?	–
Na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
mým	můj	k3xOp1gMnPc3	můj
smutným	smutný	k2eAgMnPc3d1	smutný
courám	courat	k5eAaImIp1nS	courat
(	(	kIx(	(
<g/>
Memoria	Memorium	k1gNnSc2	Memorium
de	de	k?	de
mis	mísa	k1gFnPc2	mísa
putas	putas	k1gMnSc1	putas
tristes	tristes	k1gMnSc1	tristes
<g/>
)	)	kIx)	)
česky	česky	k6eAd1	česky
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-1187-2	[number]	k4	80-207-1187-2
Anna	Anna	k1gFnSc1	Anna
Housková	houskový	k2eAgFnSc1d1	Housková
Eva	Eva	k1gFnSc1	Eva
Lukavská	Lukavský	k2eAgFnSc1d1	Lukavská
Magický	magický	k2eAgInSc1d1	magický
realismus	realismus	k1gInSc1	realismus
Seznam	seznam	k1gInSc1	seznam
<g />
.	.	kIx.	.
</s>
<s>
literárních	literární	k2eAgInPc2d1	literární
překladů	překlad	k1gInPc2	překlad
ze	z	k7c2	z
španělštiny	španělština	k1gFnSc2	španělština
Postmoderna	postmoderna	k1gFnSc1	postmoderna
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gabriel	Gabriela	k1gFnPc2	Gabriela
García	Garcí	k1gInSc2	Garcí
Márquez	Márquez	k1gInSc4	Márquez
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Gabriel	Gabriel	k1gMnSc1	Gabriel
García	García	k1gMnSc1	García
Márquez	Márquez	k1gMnSc1	Márquez
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Gabriel	Gabriel	k1gMnSc1	Gabriel
García	García	k1gMnSc1	García
Márquez	Márquez	k1gMnSc1	Márquez
Stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
Garcíu	García	k1gFnSc4	García
Márquezovi	Márquezův	k2eAgMnPc1d1	Márquezův
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
na	na	k7c6	na
nobelprize	nobelpriza	k1gFnSc6	nobelpriza
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Osiem	Osiem	k1gInSc1	Osiem
krótkich	krótkich	k1gMnSc1	krótkich
uwag	uwag	k1gMnSc1	uwag
na	na	k7c4	na
marginesie	marginesie	k1gFnPc4	marginesie
'	'	kIx"	'
<g/>
Stu	sto	k4xCgNnSc3	sto
lat	lat	k1gInSc1	lat
samotności	samotnośce	k1gFnSc3	samotnośce
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
Andrei	Andrea	k1gFnSc3	Andrea
R.	R.	kA	R.
Mochola	Mochola	k1gFnSc1	Mochola
</s>
