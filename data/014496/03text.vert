<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
Albánii	Albánie	k1gFnSc6
</s>
<s>
Katolický	katolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
ve	v	k7c6
Vlorë	Vlorë	k1gFnSc6
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
je	být	k5eAaImIp3nS
třetí	třetí	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
náboženskou	náboženský	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
v	v	k7c6
Albánii	Albánie	k1gFnSc6
(	(	kIx(
<g/>
po	po	k7c6
islámu	islám	k1gInSc6
a	a	k8xC
pravoslaví	pravoslaví	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
albánských	albánský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
okolo	okolo	k7c2
350	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
tj.	tj.	kA
cca	cca	kA
10	#num#	k4
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dobách	doba	k1gFnPc6
socialistického	socialistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
mezi	mezi	k7c7
lety	let	k1gInPc7
1946	#num#	k4
až	až	k9
1991	#num#	k4
byli	být	k5eAaImAgMnP
křesťané	křesťan	k1gMnPc1
pronásledováni	pronásledovat	k5eAaImNgMnP
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
věřících	věřící	k1gMnPc2
<g/>
,	,	kIx,
kněží	kněz	k1gMnPc2
i	i	k8xC
biskupů	biskup	k1gMnPc2
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
či	či	k8xC
uvězněno	uvězněn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
Albánie	Albánie	k1gFnSc2
za	za	k7c4
ateistický	ateistický	k2eAgInSc4d1
stát	stát	k1gInSc4
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
dokonce	dokonce	k9
praktikování	praktikování	k1gNnSc4
křesťanství	křesťanství	k1gNnSc2
ilegální	ilegální	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostely	kostel	k1gInPc4
byly	být	k5eAaImAgInP
nejprve	nejprve	k6eAd1
zavřeny	zavřen	k2eAgInPc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
zbořeny	zbořen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
držení	držení	k1gNnSc4
Bible	bible	k1gFnSc2
či	či	k8xC
křest	křest	k1gInSc4
byly	být	k5eAaImAgInP
udělovány	udělován	k2eAgInPc1d1
přísné	přísný	k2eAgInPc1d1
tresty	trest	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
začaly	začít	k5eAaPmAgInP
být	být	k5eAaImF
však	však	k9
budovány	budován	k2eAgInPc4d1
nové	nový	k2eAgInPc4d1
chrámy	chrám	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
byť	byť	k8xS
je	být	k5eAaImIp3nS
Albánie	Albánie	k1gFnSc1
země	zem	k1gFnSc2
nábožensky	nábožensky	k6eAd1
velmi	velmi	k6eAd1
různorodá	různorodý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nedochází	docházet	k5eNaImIp3nS
zde	zde	k6eAd1
ke	k	k7c3
střetům	střet	k1gInPc3
příznivců	příznivec	k1gMnPc2
jednotlivých	jednotlivý	k2eAgNnPc2d1
náboženství	náboženství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
zemi	zem	k1gFnSc6
navštívil	navštívit	k5eAaPmAgMnS
papež	papež	k1gMnSc1
Jan	Jan	k1gMnSc1
Pavel	Pavel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
katedrále	katedrála	k1gFnSc6
ve	v	k7c6
Skadaru	Skadar	k1gInSc6
vysvětil	vysvětit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgInPc4
nové	nový	k2eAgInPc4d1
biskupy	biskup	k1gInPc4
(	(	kIx(
<g/>
šlo	jít	k5eAaImAgNnS
o	o	k7c4
první	první	k4xOgNnSc4
biskupské	biskupský	k2eAgNnSc4d1
svěcení	svěcení	k1gNnSc4
v	v	k7c6
Albánii	Albánie	k1gFnSc6
od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
byl	být	k5eAaImAgInS
historicky	historicky	k6eAd1
prvním	první	k4xOgMnSc7
albánským	albánský	k2eAgMnSc7d1
kardinálem	kardinál	k1gMnSc7
jmenován	jmenován	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
Mikel	Mikel	k1gMnSc1
Koliqi	Koliqe	k1gFnSc4
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
prožil	prožít	k5eAaPmAgInS
21	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
táborech	tábor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Sídla	sídlo	k1gNnPc4
albánských	albánský	k2eAgNnPc2d1
římskokatolických	římskokatolický	k2eAgNnPc2d1
biskupství	biskupství	k1gNnPc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
na	na	k7c4
dvě	dva	k4xCgFnPc4
církevní	církevní	k2eAgFnPc4d1
provincie	provincie	k1gFnPc4
složené	složený	k2eAgFnPc1d1
celkem	celkem	k6eAd1
z	z	k7c2
pěti	pět	k4xCc2
diecézí	diecéze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
arcidiecéze	arcidiecéze	k1gFnSc1
skadarsko-pultská	skadarsko-pultský	k2eAgFnSc1d1
(	(	kIx(
<g/>
vzn	vzn	k?
<g/>
.	.	kIx.
2005	#num#	k4
sjednocením	sjednocení	k1gNnSc7
arcid	arcida	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skadar	Skadar	k1gInSc1
z	z	k7c2
1867	#num#	k4
a	a	k8xC
diec	diec	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pult	pult	k1gInSc1
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
se	s	k7c7
sufragánními	sufragánní	k2eAgFnPc7d1
diecézemi	diecéze	k1gFnPc7
<g/>
:	:	kIx,
</s>
<s>
diecéze	diecéze	k1gFnSc1
Lezhë	Lezhë	k1gFnSc2
</s>
<s>
diecéze	diecéze	k1gFnSc1
Sapë	Sapë	k1gFnSc1
(	(	kIx(
<g/>
zal	zal	k?
<g/>
.	.	kIx.
1062	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
arcidiecéze	arcidiecéze	k1gFnSc1
tiransko-dračská	tiransko-dračský	k2eAgFnSc1d1
(	(	kIx(
<g/>
vzn	vzn	k?
<g/>
.	.	kIx.
1992	#num#	k4
sjednocením	sjednocení	k1gNnSc7
arcid	arcida	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drač	Drač	k1gFnSc1
z	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
a	a	k8xC
diec	diec	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tirana	Tirana	k1gFnSc1
<g/>
)	)	kIx)
se	s	k7c7
sufragánní	sufragánní	k2eAgFnSc7d1
diecézí	diecéze	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
diecéze	diecéze	k1gFnSc1
rrëshenská	rrëshenský	k2eAgFnSc1d1
(	(	kIx(
<g/>
vzn	vzn	k?
<g/>
.	.	kIx.
1996	#num#	k4
z	z	k7c2
územního	územní	k2eAgNnSc2d1
opatství	opatství	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Informace	informace	k1gFnPc1
na	na	k7c4
www.catholic-hierachy.org	www.catholic-hierachy.org	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Guernsey	Guernsea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
Území	území	k1gNnSc4
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
<g/>
:	:	kIx,
Abcházie	Abcházie	k1gFnSc2
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Podněstří	Podněstří	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
