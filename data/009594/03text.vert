<p>
<s>
Australské	australský	k2eAgInPc1d1	australský
státy	stát	k1gInPc1	stát
a	a	k8xC	a
teritoria	teritorium	k1gNnPc1	teritorium
spolu	spolu	k6eAd1	spolu
tvoří	tvořit	k5eAaImIp3nP	tvořit
šestou	šestý	k4xOgFnSc4	šestý
největší	veliký	k2eAgFnSc4d3	veliký
zemi	zem	k1gFnSc4	zem
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
států	stát	k1gInPc2	stát
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
;	;	kIx,	;
Pevninská	pevninský	k2eAgFnSc1d1	pevninská
Austrálie	Austrálie	k1gFnSc1	Austrálie
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pěti	pět	k4xCc7	pět
státy	stát	k1gInPc7	stát
a	a	k8xC	a
třemi	tři	k4xCgNnPc7	tři
teritorii	teritorium	k1gNnPc7	teritorium
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
malého	malý	k2eAgNnSc2d1	malé
Teritoria	teritorium	k1gNnSc2	teritorium
Jervis	Jervis	k1gFnSc1	Jervis
Bay	Bay	k1gFnSc1	Bay
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
šestý	šestý	k4xOgInSc1	šestý
australský	australský	k2eAgInSc1d1	australský
stát	stát	k1gInSc1	stát
-	-	kIx~	-
ostrov	ostrov	k1gInSc1	ostrov
Tasmánie	Tasmánie	k1gFnSc2	Tasmánie
-	-	kIx~	-
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
mateřské	mateřský	k2eAgFnSc2d1	mateřská
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Austrálii	Austrálie	k1gFnSc4	Austrálie
ještě	ještě	k6eAd1	ještě
patří	patřit	k5eAaImIp3nS	patřit
šest	šest	k4xCc4	šest
ostrovních	ostrovní	k2eAgMnPc2d1	ostrovní
<g/>
,	,	kIx,	,
externích	externí	k2eAgInPc2d1	externí
<g/>
,	,	kIx,	,
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
si	se	k3xPyFc3	se
také	také	k9	také
dělá	dělat	k5eAaImIp3nS	dělat
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
část	část	k1gFnSc4	část
Antarktidy	Antarktida	k1gFnSc2	Antarktida
(	(	kIx(	(
<g/>
na	na	k7c4	na
Australské	australský	k2eAgNnSc4d1	Australské
antarktické	antarktický	k2eAgNnSc4d1	antarktické
území	území	k1gNnSc4	území
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
teritoria	teritorium	k1gNnPc4	teritorium
(	(	kIx(	(
<g/>
Severní	severní	k2eAgNnSc4d1	severní
teritorium	teritorium	k1gNnSc4	teritorium
a	a	k8xC	a
Teritorium	teritorium	k1gNnSc4	teritorium
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
parlamenty	parlament	k1gInPc4	parlament
a	a	k8xC	a
samosprávu	samospráva	k1gFnSc4	samospráva
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgFnPc4	všechen
zbývající	zbývající	k2eAgFnPc4d1	zbývající
teritoria	teritorium	k1gNnPc4	teritorium
jsou	být	k5eAaImIp3nP	být
spravovány	spravován	k2eAgInPc1d1	spravován
federální	federální	k2eAgFnSc7d1	federální
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ostrov	ostrov	k1gInSc1	ostrov
Norfolk	Norfolka	k1gFnPc2	Norfolka
má	mít	k5eAaImIp3nS	mít
určitý	určitý	k2eAgInSc4d1	určitý
stupeň	stupeň	k1gInSc4	stupeň
samosprávy	samospráva	k1gFnSc2	samospráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Státy	stát	k1gInPc1	stát
a	a	k8xC	a
teritoria	teritorium	k1gNnPc1	teritorium
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vnější	vnější	k2eAgNnPc1d1	vnější
teritoria	teritorium	k1gNnPc1	teritorium
<g/>
,	,	kIx,	,
Antarktida	Antarktida	k1gFnSc1	Antarktida
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Statistické	statistický	k2eAgInPc1d1	statistický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Štáty	Štáta	k1gFnSc2	Štáta
a	a	k8xC	a
teritóriá	teritóriá	k1gFnSc1	teritóriá
Austrálie	Austrálie	k1gFnSc2	Austrálie
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
