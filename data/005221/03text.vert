<s>
Perioda	perioda	k1gFnSc1	perioda
označuje	označovat	k5eAaImIp3nS	označovat
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
veličinu	veličina	k1gFnSc4	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
jednoho	jeden	k4xCgNnSc2	jeden
opakování	opakování	k1gNnSc2	opakování
periodického	periodický	k2eAgInSc2d1	periodický
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Perioda	perioda	k1gFnSc1	perioda
tedy	tedy	k9	tedy
označuje	označovat	k5eAaImIp3nS	označovat
dobu	doba	k1gFnSc4	doba
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
systém	systém	k1gInSc1	systém
dostal	dostat	k5eAaPmAgInS	dostat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
výchozího	výchozí	k2eAgInSc2d1	výchozí
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
veličiny	veličina	k1gFnSc2	veličina
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
}	}	kIx)	}
:	:	kIx,	:
Základní	základní	k2eAgFnSc1d1	základní
jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
s	s	k7c7	s
Další	další	k2eAgMnSc1d1	další
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
:	:	kIx,	:
viz	vidět	k5eAaImRp2nS	vidět
čas	čas	k1gInSc4	čas
Mezi	mezi	k7c7	mezi
frekvencí	frekvence	k1gFnSc7	frekvence
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
f	f	k?	f
<g/>
}	}	kIx)	}
a	a	k8xC	a
periodou	perioda	k1gFnSc7	perioda
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
T	T	kA	T
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Při	při	k7c6	při
popisu	popis	k1gInSc6	popis
kmitání	kmitání	k1gNnSc2	kmitání
a	a	k8xC	a
vlnění	vlnění	k1gNnSc2	vlnění
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
úhlová	úhlový	k2eAgFnSc1d1	úhlová
frekvence	frekvence	k1gFnSc1	frekvence
(	(	kIx(	(
<g/>
úhlový	úhlový	k2eAgInSc1d1	úhlový
kmitočet	kmitočet	k1gInSc1	kmitočet
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
omega	omega	k1gNnPc3	omega
}	}	kIx)	}
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
π	π	k?	π
:	:	kIx,	:
ω	ω	k?	ω
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
omega	omega	k1gFnSc1	omega
}}}	}}}	k?	}}}
:	:	kIx,	:
Mezi	mezi	k7c7	mezi
periodou	perioda	k1gFnSc7	perioda
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
}	}	kIx)	}
a	a	k8xC	a
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
v	v	k7c4	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
v	v	k7c4	v
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
λ	λ	k?	λ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
fázová	fázový	k2eAgFnSc1d1	fázová
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Kmitání	kmitání	k1gNnSc1	kmitání
Vlnění	vlnění	k1gNnSc2	vlnění
</s>
