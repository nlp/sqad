<s>
Perioda	perioda	k1gFnSc1	perioda
označuje	označovat	k5eAaImIp3nS	označovat
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
veličinu	veličina	k1gFnSc4	veličina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
jednoho	jeden	k4xCgNnSc2	jeden
opakování	opakování	k1gNnSc2	opakování
periodického	periodický	k2eAgInSc2d1	periodický
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
