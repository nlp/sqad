<s>
Strašek	strašek	k1gMnSc1
paví	paví	k2eAgFnSc2d1
</s>
<s>
Strašek	strašek	k1gMnSc1
paví	paví	k2eAgFnSc2d1
Samička	samička	k1gFnSc1
straška	strašek	k1gMnSc2
pavího	paví	k2eAgInSc2d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
korýši	korýš	k1gMnPc1
(	(	kIx(
<g/>
Crustacea	Crustacea	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
rakovci	rakovec	k1gMnPc1
(	(	kIx(
<g/>
Malacostraca	Malacostraca	k1gMnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
Stomatopoda	Stomatopoda	k1gFnSc1
Čeleď	čeleď	k1gFnSc1
</s>
<s>
straškovití	straškovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Odontodactylidae	Odontodactylidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
strašek	strašek	k1gMnSc1
(	(	kIx(
<g/>
Odontodactylus	Odontodactylus	k1gMnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Odontodactylus	Odontodactylus	k1gInSc1
scyllarusL	scyllarusL	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1758	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Strašek	strašek	k1gMnSc1
paví	paví	k2eAgMnSc1d1
(	(	kIx(
<g/>
Odontodactylus	Odontodactylus	k1gMnSc1
scyllarus	scyllarus	k1gMnSc1
<g/>
;	;	kIx,
Linnaeus	Linnaeus	k1gMnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velký	velký	k2eAgMnSc1d1
korýš	korýš	k1gMnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
straškovitých	straškovitý	k2eAgFnPc2d1
obývající	obývající	k2eAgMnSc1d1
oblast	oblast	k1gFnSc4
Tichého	Tichého	k2eAgInSc2d1
a	a	k8xC
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
od	od	k7c2
ostrova	ostrov	k1gInSc2
Guam	Guam	k1gInSc1
až	až	k9
po	po	k7c4
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
vyhledává	vyhledávat	k5eAaImIp3nS
písčité	písčitý	k2eAgFnPc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
či	či	k8xC
bahnité	bahnitý	k2eAgNnSc4d1
dno	dno	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
denního	denní	k2eAgMnSc4d1
i	i	k8xC
nočního	noční	k2eAgMnSc4d1
dravce	dravec	k1gMnSc4
dorůstajícího	dorůstající	k2eAgInSc2d1
3	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1
požírá	požírat	k5eAaImIp3nS
malé	malý	k2eAgFnPc4d1
rybičky	rybička	k1gFnPc4
a	a	k8xC
ostatní	ostatní	k2eAgMnPc4d1
korýše	korýš	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Často	často	k6eAd1
je	být	k5eAaImIp3nS
chován	chovat	k5eAaImNgMnS
v	v	k7c6
akváriích	akvárium	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
hloubce	hloubka	k1gFnSc6
3	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
m	m	kA
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
ale	ale	k9
obývá	obývat	k5eAaImIp3nS
oblasti	oblast	k1gFnPc4
o	o	k7c6
hloubce	hloubka	k1gFnSc6
10	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
tělo	tělo	k1gNnSc4
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc1
obráceného	obrácený	k2eAgNnSc2d1
písmene	písmeno	k1gNnSc2
U	U	kA
a	a	k8xC
má	mít	k5eAaImIp3nS
většinou	většinou	k6eAd1
zelenou	zelený	k2eAgFnSc4d1
až	až	k8xS
olivínovou	olivínový	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
s	s	k7c7
leopardími	leopardí	k2eAgFnPc7d1
skvrnami	skvrna	k1gFnPc7
po	po	k7c6
těle	tělo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zajímavé	zajímavý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
oči	oko	k1gNnPc4
tohoto	tento	k3xDgMnSc2
korýše	korýš	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
vnímání	vnímání	k1gNnSc4
většího	veliký	k2eAgNnSc2d2
spektra	spektrum	k1gNnSc2
barev	barva	k1gFnPc2
než	než	k8xS
v	v	k7c6
případě	případ	k1gInSc6
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strašci	strašek	k1gMnPc1
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
agresivní	agresivní	k2eAgFnPc1d1
a	a	k8xC
velmi	velmi	k6eAd1
nebezpeční	bezpečný	k2eNgMnPc1d1
<g/>
,	,	kIx,
dokážou	dokázat	k5eAaPmIp3nP
usmrtit	usmrtit	k5eAaPmF
i	i	k9
podstatně	podstatně	k6eAd1
větší	veliký	k2eAgInSc1d2
živočichy	živočich	k1gMnPc7
a	a	k8xC
ranami	rána	k1gFnPc7
svých	svůj	k3xOyFgNnPc2
klepet	klepeto	k1gNnPc2
mohou	moct	k5eAaImIp3nP
ublížit	ublížit	k5eAaPmF
i	i	k9
člověku	člověk	k1gMnSc3
(	(	kIx(
<g/>
nebo	nebo	k8xC
třeba	třeba	k6eAd1
rozbít	rozbít	k5eAaPmF
stěnu	stěna	k1gFnSc4
akvária	akvárium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Akvaristika	akvaristika	k1gFnSc1
</s>
<s>
Strašek	strašek	k1gMnSc1
paví	paví	k2eAgMnSc1d1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
v	v	k7c6
akváriích	akvárium	k1gNnPc6
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
pestrou	pestrý	k2eAgFnSc4d1
barevnost	barevnost	k1gFnSc4
<g/>
,	,	kIx,
atypický	atypický	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
a	a	k8xC
aktivní	aktivní	k2eAgNnSc4d1
chování	chování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedoporučuje	doporučovat	k5eNaImIp3nS
se	se	k3xPyFc4
chovat	chovat	k5eAaImF
ve	v	k7c6
společenství	společenství	k1gNnSc6
dalších	další	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
akvária	akvárium	k1gNnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
dravce	dravec	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
většinu	většina	k1gFnSc4
okolních	okolní	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
pozře	pozřít	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Často	často	k6eAd1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
chová	chovat	k5eAaImIp3nS
samostatně	samostatně	k6eAd1
ve	v	k7c6
vlastním	vlastní	k2eAgNnSc6d1
akváriu	akvárium	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
vyžaduje	vyžadovat	k5eAaImIp3nS
vodu	voda	k1gFnSc4
o	o	k7c6
teplotě	teplota	k1gFnSc6
mezi	mezi	k7c7
22	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
°	°	k?
<g/>
C	C	kA
se	s	k7c7
salinitou	salinita	k1gFnSc7
33	#num#	k4
<g/>
–	–	k?
<g/>
36	#num#	k4
PSU	pes	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
chov	chov	k1gInSc4
se	se	k3xPyFc4
doporučuje	doporučovat	k5eAaImIp3nS
použít	použít	k5eAaPmF
akvárium	akvárium	k1gNnSc4
o	o	k7c6
objemu	objem	k1gInSc6
aspoň	aspoň	k9
100	#num#	k4
litrů	litr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
strašek	strašek	k1gMnSc1
může	moct	k5eAaImIp3nS
dorůst	dorůst	k5eAaPmF
v	v	k7c6
průměru	průměr	k1gInSc6
velikosti	velikost	k1gFnSc2
okolo	okolo	k7c2
13	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
nedoporučuje	doporučovat	k5eNaImIp3nS
se	se	k3xPyFc4
chov	chov	k1gInSc1
ve	v	k7c6
skleněném	skleněný	k2eAgNnSc6d1
akváriu	akvárium	k1gNnSc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
by	by	kYmCp3nS
ho	on	k3xPp3gMnSc4
mohl	moct	k5eAaImAgInS
rozbít	rozbít	k5eAaPmF
či	či	k8xC
prasknout	prasknout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInPc1
údery	úder	k1gInPc1
předními	přední	k2eAgFnPc7d1
končetinami	končetina	k1gFnPc7
mohou	moct	k5eAaImIp3nP
dosáhnout	dosáhnout	k5eAaPmF
síly	síla	k1gFnPc4
až	až	k9
1400	#num#	k4
N.	N.	kA
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
plexisklová	plexisklový	k2eAgNnPc1d1
akvária	akvárium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Peacock	Peacocka	k1gFnPc2
Mantis	mantisa	k1gFnPc2
Shrimp	Shrimp	k1gInSc1
-	-	kIx~
Facts	Facts	k1gInSc4
On	on	k3xPp3gMnSc1
The	The	k1gMnSc1
Beautiful	Beautifula	k1gFnPc2
Peacock	Peacock	k1gMnSc1
Mantis	mantisa	k1gFnPc2
Shrimp	Shrimp	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
hubpages	hubpages	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
/	/	kIx~
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Species	species	k1gFnPc2
<g/>
:	:	kIx,
Odontodactylus	Odontodactylus	k1gMnSc1
scyllarus	scyllarus	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Species	species	k1gFnSc1
Directory	Director	k1gInPc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.stoplusjednicka.cz/zrak-petkrat-jinak-zivocichove-s-nejpodivnejsima-ocima	https://www.stoplusjednicka.cz/zrak-petkrat-jinak-zivocichove-s-nejpodivnejsima-ocima	k1gFnSc1
<g/>
↑	↑	k?
The	The	k1gFnSc1
Evolution	Evolution	k1gInSc1
of	of	k?
the	the	k?
Hammer	Hammer	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
of	of	k?
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
Berkeley	Berkelea	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
strašek	strašek	k1gMnSc1
paví	paví	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Detailní	detailní	k2eAgInSc1d1
popis	popis	k1gInSc1
straška	strašek	k1gMnSc2
pavího	paví	k2eAgMnSc2d1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Video	video	k1gNnSc1
ukazující	ukazující	k2eAgFnSc2d1
straška	strašek	k1gMnSc4
pavího	paví	k2eAgNnSc2d1
</s>
<s>
Ninja-kreveta	Ninja-kreveta	k1gFnSc1
strašek	strašek	k1gMnSc1
najde	najít	k5eAaPmIp3nS
rakovinu	rakovina	k1gFnSc4
pouhým	pouhý	k2eAgInSc7d1
pohledem	pohled	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
to	ten	k3xDgNnSc4
zkouší	zkoušet	k5eAaImIp3nP
využít	využít	k5eAaPmF
pro	pro	k7c4
léčbu	léčba	k1gFnSc4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
</s>
