<s>
Strašek	strašek	k1gMnSc1
paví	paví	k2eAgMnSc1d1
(	(	kIx(
<g/>
Odontodactylus	Odontodactylus	k1gMnSc1
scyllarus	scyllarus	k1gMnSc1
<g/>
;	;	kIx,
Linnaeus	Linnaeus	k1gMnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velký	velký	k2eAgMnSc1d1
korýš	korýš	k1gMnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
straškovitých	straškovitý	k2eAgFnPc2d1
obývající	obývající	k2eAgMnSc1d1
oblast	oblast	k1gFnSc4
Tichého	Tichého	k2eAgInSc2d1
a	a	k8xC
Indického	indický	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
od	od	k7c2
ostrova	ostrov	k1gInSc2
Guam	Guam	k1gInSc1
až	až	k9
po	po	k7c4
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
vyhledává	vyhledávat	k5eAaImIp3nS
písčité	písčitý	k2eAgFnPc4d1
<g />
.	.	kIx.
</s>