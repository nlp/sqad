<p>
<s>
Land	Land	k1gInSc1	Land
art	art	k?	art
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
nepatří	patřit	k5eNaImIp3nP	patřit
k	k	k7c3	k
zahradnímu	zahradní	k2eAgNnSc3d1	zahradní
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
krajinné	krajinný	k2eAgFnSc6d1	krajinná
architektuře	architektura	k1gFnSc6	architektura
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používají	používat	k5eAaImIp3nP	používat
prvky	prvek	k1gInPc1	prvek
land	land	k6eAd1	land
artu	artu	k6eAd1	artu
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
skulptury	skulptura	k1gFnPc1	skulptura
a	a	k8xC	a
objekty	objekt	k1gInPc1	objekt
z	z	k7c2	z
organických	organický	k2eAgInPc2d1	organický
a	a	k8xC	a
anorganických	anorganický	k2eAgInPc2d1	anorganický
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
tohoto	tento	k3xDgInSc2	tento
směru	směr	k1gInSc2	směr
se	se	k3xPyFc4	se
obrátili	obrátit	k5eAaPmAgMnP	obrátit
proti	proti	k7c3	proti
komerčnímu	komerční	k2eAgNnSc3d1	komerční
umění	umění	k1gNnSc3	umění
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
záporný	záporný	k2eAgInSc4d1	záporný
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
galeriích	galerie	k1gFnPc6	galerie
a	a	k8xC	a
muzeích	muzeum	k1gNnPc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
tvořili	tvořit	k5eAaImAgMnP	tvořit
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
na	na	k7c6	na
přírodních	přírodní	k2eAgInPc6d1	přírodní
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
těžko	těžko	k6eAd1	těžko
dostupná	dostupný	k2eAgFnSc1d1	dostupná
a	a	k8xC	a
nepřetvořená	přetvořený	k2eNgFnSc1d1	přetvořený
zásahem	zásah	k1gInSc7	zásah
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc4	dílo
Land	Land	k1gInSc1	Land
artu	artus	k1gInSc2	artus
někdy	někdy	k6eAd1	někdy
působí	působit	k5eAaImIp3nS	působit
svou	svůj	k3xOyFgFnSc7	svůj
monumentalitou	monumentalita	k1gFnSc7	monumentalita
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
využívají	využívat	k5eAaPmIp3nP	využívat
intenzitu	intenzita	k1gFnSc4	intenzita
vnímání	vnímání	k1gNnSc2	vnímání
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc2d1	přírodní
zkušenosti	zkušenost	k1gFnSc2	zkušenost
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
s	s	k7c7	s
efekty	efekt	k1gInPc7	efekt
vznešenosti	vznešenost	k1gFnSc2	vznešenost
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
formami	forma	k1gFnPc7	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Landart	Landart	k1gInSc1	Landart
–	–	k?	–
neboli	neboli	k8xC	neboli
krajinné	krajinný	k2eAgNnSc1d1	krajinné
umění	umění	k1gNnSc1	umění
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
fotografii	fotografia	k1gFnSc3	fotografia
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
přechod	přechod	k1gInSc1	přechod
z	z	k7c2	z
ateliérů	ateliér	k1gInPc2	ateliér
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
předobraz	předobraz	k1gInSc4	předobraz
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
visutých	visutý	k2eAgFnPc2d1	visutá
zahrad	zahrada	k1gFnPc2	zahrada
královny	královna	k1gFnSc2	královna
Semiramidy	Semiramis	k1gFnSc2	Semiramis
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
parky	park	k1gInPc4	park
s	s	k7c7	s
keřovými	keřový	k2eAgNnPc7d1	keřové
bludišti	bludiště	k1gNnPc7	bludiště
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
přísně	přísně	k6eAd1	přísně
vymezené	vymezený	k2eAgFnSc2d1	vymezená
francouzské	francouzský	k2eAgFnSc2d1	francouzská
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
k	k	k7c3	k
čínským	čínský	k2eAgFnPc3d1	čínská
nebo	nebo	k8xC	nebo
japonským	japonský	k2eAgFnPc3d1	japonská
klášterním	klášterní	k2eAgFnPc3d1	klášterní
zahradám	zahrada	k1gFnPc3	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Evropští	evropský	k2eAgMnPc1d1	evropský
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
David	David	k1gMnSc1	David
Nash	Nash	k1gMnSc1	Nash
<g/>
,	,	kIx,	,
Andy	Anda	k1gFnPc1	Anda
Goldsworthy	Goldswortha	k1gFnSc2	Goldswortha
nebo	nebo	k8xC	nebo
Mihael	Mihael	k1gMnSc1	Mihael
Singer	Singer	k1gMnSc1	Singer
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
subtilnější	subtilní	k2eAgInSc4d2	subtilnější
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
přírodě	příroda	k1gFnSc3	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
díly	díl	k1gInPc7	díl
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
krajinu	krajina	k1gFnSc4	krajina
akcentovat	akcentovat	k5eAaImF	akcentovat
a	a	k8xC	a
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
přírodními	přírodní	k2eAgInPc7d1	přírodní
materiály	materiál	k1gInPc7	materiál
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
půda	půda	k1gFnSc1	půda
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
nebo	nebo	k8xC	nebo
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
byl	být	k5eAaImAgInS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
Ladislav	Ladislav	k1gMnSc1	Ladislav
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ho	on	k3xPp3gMnSc4	on
následují	následovat	k5eAaImIp3nP	následovat
například	například	k6eAd1	například
Jan	Jan	k1gMnSc1	Jan
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Šigut	Šigut	k1gMnSc1	Šigut
nebo	nebo	k8xC	nebo
Jan	Jan	k1gMnSc1	Jan
Pohribný	Pohribný	k2eAgMnSc1d1	Pohribný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fotograf	fotograf	k1gMnSc1	fotograf
Jan	Jan	k1gMnSc1	Jan
Pohribný	Pohribný	k2eAgMnSc1d1	Pohribný
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
tvůrčí	tvůrčí	k2eAgInPc4d1	tvůrčí
postupy	postup	k1gInPc4	postup
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
neostrost	neostrost	k1gFnSc4	neostrost
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
vícenásobnou	vícenásobný	k2eAgFnSc7d1	vícenásobná
expozicí	expozice	k1gFnSc7	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
nejen	nejen	k6eAd1	nejen
principy	princip	k1gInPc4	princip
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
výrazných	výrazný	k2eAgFnPc2d1	výrazná
barevných	barevný	k2eAgFnPc2d1	barevná
stop	stopa	k1gFnPc2	stopa
či	či	k8xC	či
monochromatických	monochromatický	k2eAgNnPc2d1	monochromatické
řešení	řešení	k1gNnPc2	řešení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívá	využívat	k5eAaPmIp3nS	využívat
také	také	k9	také
světelné	světelný	k2eAgFnSc2d1	světelná
malby	malba	k1gFnSc2	malba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
"	"	kIx"	"
<g/>
zviditelňuje	zviditelňovat	k5eAaImIp3nS	zviditelňovat
<g/>
"	"	kIx"	"
energie	energie	k1gFnSc1	energie
zvolených	zvolený	k2eAgFnPc2d1	zvolená
přírodních	přírodní	k2eAgFnPc2d1	přírodní
lokalit	lokalita	k1gFnPc2	lokalita
či	či	k8xC	či
posvátných	posvátný	k2eAgInPc2d1	posvátný
kamenů	kámen	k1gInPc2	kámen
vztyčených	vztyčený	k2eAgInPc2d1	vztyčený
lidskou	lidský	k2eAgFnSc7d1	lidská
rukou	ruka	k1gFnSc7	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šigut	Šigut	k1gMnSc1	Šigut
(	(	kIx(	(
<g/>
*	*	kIx~	*
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
věnoval	věnovat	k5eAaImAgInS	věnovat
technice	technika	k1gFnSc3	technika
fotogramu	fotogram	k1gInSc2	fotogram
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rozmísťoval	rozmísťovat	k5eAaImAgMnS	rozmísťovat
a	a	k8xC	a
nechával	nechávat	k5eAaImAgMnS	nechávat
ležet	ležet	k5eAaImF	ležet
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
fotocitlivý	fotocitlivý	k2eAgInSc4d1	fotocitlivý
papír	papír	k1gInSc4	papír
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
několikadenní	několikadenní	k2eAgFnSc6d1	několikadenní
expozici	expozice	k1gFnSc6	expozice
ustálil	ustálit	k5eAaPmAgMnS	ustálit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotografických	fotografický	k2eAgInPc6d1	fotografický
obrazech	obraz	k1gInPc6	obraz
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
tvarové	tvarový	k2eAgInPc4d1	tvarový
zlomky	zlomek	k1gInPc4	zlomek
přírodnin	přírodnina	k1gFnPc2	přírodnina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
záznam	záznam	k1gInSc4	záznam
přírodních	přírodní	k2eAgFnPc2d1	přírodní
světelných	světelný	k2eAgFnPc2d1	světelná
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
stopy	stopa	k1gFnSc2	stopa
energie	energie	k1gFnSc2	energie
či	či	k8xC	či
pohybu	pohyb	k1gInSc2	pohyb
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
světlušek	světluška	k1gFnPc2	světluška
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
živých	živý	k2eAgMnPc2d1	živý
tvorů	tvor	k1gMnPc2	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
i	i	k9	i
osobní	osobní	k2eAgInSc1d1	osobní
umělcův	umělcův	k2eAgInSc1d1	umělcův
přístup	přístup	k1gInSc1	přístup
a	a	k8xC	a
prožitek	prožitek	k1gInSc1	prožitek
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
do	do	k7c2	do
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c6	na
nejrůznějších	různý	k2eAgNnPc6d3	nejrůznější
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
vodní	vodní	k2eAgFnPc1d1	vodní
tůně	tůně	k1gFnPc1	tůně
<g/>
,	,	kIx,	,
potoky	potok	k1gInPc1	potok
<g/>
,	,	kIx,	,
pole	pole	k1gNnPc1	pole
nebo	nebo	k8xC	nebo
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
kladl	klást	k5eAaImAgMnS	klást
fotografické	fotografický	k2eAgInPc4d1	fotografický
papíry	papír	k1gInPc4	papír
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tu	tu	k6eAd1	tu
leží	ležet	k5eAaImIp3nP	ležet
i	i	k9	i
řadu	řada	k1gFnSc4	řada
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
až	až	k9	až
týdny	týden	k1gInPc7	týden
<g/>
,	,	kIx,	,
postrádají	postrádat	k5eAaImIp3nP	postrádat
okamžikovost	okamžikovost	k1gFnSc4	okamžikovost
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
příčně	příčně	k6eAd1	příčně
položeném	položený	k2eAgInSc6d1	položený
papíru	papír	k1gInSc6	papír
v	v	k7c6	v
potoce	potok	k1gInSc6	potok
se	se	k3xPyFc4	se
zobrazí	zobrazit	k5eAaPmIp3nP	zobrazit
přírodniny	přírodnina	k1gFnPc1	přírodnina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
potok	potok	k1gInSc1	potok
přinesl	přinést	k5eAaPmAgInS	přinést
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zanechají	zanechat	k5eAaPmIp3nP	zanechat
svou	svůj	k3xOyFgFnSc4	svůj
stopu	stopa	k1gFnSc4	stopa
a	a	k8xC	a
časem	časem	k6eAd1	časem
putují	putovat	k5eAaImIp3nP	putovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Spadlý	spadlý	k2eAgInSc1d1	spadlý
list	list	k1gInSc1	list
zaznamenaný	zaznamenaný	k2eAgInSc1d1	zaznamenaný
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pamětí	paměť	k1gFnSc7	paměť
stromu	strom	k1gInSc2	strom
nebo	nebo	k8xC	nebo
smutkem	smutek	k1gInSc7	smutek
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
<g/>
Karolina	Karolinum	k1gNnSc2	Karolinum
Kohoutková	Kohoutková	k1gFnSc1	Kohoutková
si	se	k3xPyFc3	se
zasadila	zasadit	k5eAaPmAgFnS	zasadit
rostlinu	rostlina	k1gFnSc4	rostlina
do	do	k7c2	do
pupíku	pupík	k1gInSc2	pupík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
břišní	břišní	k2eAgFnSc6d1	břišní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teresa	Teresa	k1gFnSc1	Teresa
Murak	Murak	k1gInSc1	Murak
používá	používat	k5eAaImIp3nS	používat
rostliny	rostlina	k1gFnPc4	rostlina
jako	jako	k8xS	jako
pokrývku	pokrývka	k1gFnSc4	pokrývka
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
sleduje	sledovat	k5eAaImIp3nS	sledovat
jejich	jejich	k3xOp3gInSc4	jejich
krátký	krátký	k2eAgInSc4d1	krátký
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
posouvá	posouvat	k5eAaImIp3nS	posouvat
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Štembera	Štember	k1gMnSc4	Štember
si	se	k3xPyFc3	se
vštěpoval	vštěpovat	k5eAaBmAgMnS	vštěpovat
větvičku	větvička	k1gFnSc4	větvička
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Havlík	Havlík	k1gMnSc1	Havlík
se	se	k3xPyFc4	se
při	při	k7c6	při
"	"	kIx"	"
<g/>
Pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
spánek	spánek	k1gInSc4	spánek
<g/>
"	"	kIx"	"
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
spočinout	spočinout	k5eAaPmF	spočinout
pod	pod	k7c7	pod
přikrývkou	přikrývka	k1gFnSc7	přikrývka
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělci	umělec	k1gMnPc1	umělec
==	==	k?	==
</s>
</p>
<p>
<s>
Athena	Athen	k2eAgFnSc1d1	Athena
Tacha	Tacha	k1gFnSc1	Tacha
</s>
</p>
<p>
<s>
Betty	Betty	k1gFnSc1	Betty
Beaumont	Beaumonta	k1gFnPc2	Beaumonta
</s>
</p>
<p>
<s>
Eberhard	Eberhard	k1gInSc1	Eberhard
Bosslet	Bosslet	k1gInSc1	Bosslet
</s>
</p>
<p>
<s>
Walter	Walter	k1gMnSc1	Walter
De	De	k?	De
Maria	Maria	k1gFnSc1	Maria
</s>
</p>
<p>
<s>
Lucien	Lucien	k2eAgInSc4d1	Lucien
den	den	k1gInSc4	den
Arend	arenda	k1gFnPc2	arenda
</s>
</p>
<p>
<s>
Agnes	Agnes	k1gMnSc1	Agnes
Denes	Denes	k1gMnSc1	Denes
</s>
</p>
<p>
<s>
Harvey	Harve	k2eAgFnPc1d1	Harve
Fite	Fit	k1gFnPc1	Fit
</s>
</p>
<p>
<s>
Andy	Anda	k1gFnPc1	Anda
Goldsworthy	Goldswortha	k1gFnSc2	Goldswortha
</s>
</p>
<p>
<s>
Michael	Michael	k1gMnSc1	Michael
Heizer	Heizer	k1gMnSc1	Heizer
</s>
</p>
<p>
<s>
Junichi	Junichi	k1gNnSc1	Junichi
Kakizaki	Kakizak	k1gFnSc2	Kakizak
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Long	Long	k1gMnSc1	Long
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
K.	K.	kA	K.
Melvin	Melvina	k1gFnPc2	Melvina
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Nash	Nash	k1gMnSc1	Nash
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Pfahl	Pfahl	k1gMnSc1	Pfahl
</s>
</p>
<p>
<s>
Andrew	Andrew	k?	Andrew
Rogers	Rogers	k1gInSc1	Rogers
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Smithson	Smithson	k1gMnSc1	Smithson
</s>
</p>
<p>
<s>
Alan	Alan	k1gMnSc1	Alan
Sonfist	Sonfist	k1gMnSc1	Sonfist
</s>
</p>
<p>
<s>
Jacek	Jacek	k1gMnSc1	Jacek
Tylicki	Tylick	k1gFnSc2	Tylick
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Turrell	Turrell	k1gMnSc1	Turrell
</s>
</p>
<p>
<s>
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
Wierzbicka	Wierzbicko	k1gNnSc2	Wierzbicko
Wela	Wela	k1gMnSc1	Wela
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Lawrence	Lawrence	k1gFnSc1	Lawrence
Alloway	Allowaa	k1gFnSc2	Allowaa
<g/>
,	,	kIx,	,
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Becker	Becker	k1gMnSc1	Becker
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Rosenblum	Rosenblum	k1gInSc1	Rosenblum
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
Sonfist	Sonfist	k1gMnSc1	Sonfist
<g/>
,	,	kIx,	,
Nature	Natur	k1gMnSc5	Natur
<g/>
:	:	kIx,	:
The	The	k1gMnSc6	The
End	End	k1gFnSc7	End
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
Gli	Gli	k1gMnSc1	Gli
Ori	Ori	k1gMnSc1	Ori
<g/>
,	,	kIx,	,
<g/>
Dist	Dist	k1gMnSc1	Dist
<g/>
.	.	kIx.	.
</s>
<s>
Thames	Thames	k1gMnSc1	Thames
&	&	k?	&
Hudson	Hudson	k1gMnSc1	Hudson
Florence	Florenc	k1gFnSc2	Florenc
<g/>
,	,	kIx,	,
Italy	Ital	k1gMnPc4	Ital
<g/>
,2004	,2004	k4	,2004
ISBN	ISBN	kA	ISBN
0-615-12533-6	[number]	k4	0-615-12533-6
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Andrews	Andrews	k1gInSc1	Andrews
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Land	Land	k1gMnSc1	Land
<g/>
,	,	kIx,	,
Art	Art	k1gMnSc1	Art
<g/>
:	:	kIx,	:
A	a	k8xC	a
Cultural	Cultural	k1gMnSc1	Cultural
Ecology	Ecologa	k1gFnSc2	Ecologa
Handbook	handbook	k1gInSc1	handbook
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
2006	[number]	k4	2006
ISBN	ISBN	kA	ISBN
978-0-901469-57-1	[number]	k4	978-0-901469-57-1
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Beardsley	Beardslea	k1gFnSc2	Beardslea
<g/>
:	:	kIx,	:
Earthworks	Earthworks	k1gInSc1	Earthworks
and	and	k?	and
Beyond	Beyond	k1gInSc1	Beyond
<g/>
.	.	kIx.	.
</s>
<s>
Contemporary	Contemporar	k1gInPc1	Contemporar
Art	Art	k1gFnSc2	Art
in	in	k?	in
the	the	k?	the
Landscape	Landscap	k1gMnSc5	Landscap
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
1998	[number]	k4	1998
ISBN	ISBN	kA	ISBN
0-7892-0296-4	[number]	k4	0-7892-0296-4
</s>
</p>
<p>
<s>
Suzaan	Suzaan	k1gMnSc1	Suzaan
Boettger	Boettger	k1gMnSc1	Boettger
<g/>
,	,	kIx,	,
Earthworks	Earthworks	k1gInSc1	Earthworks
<g/>
:	:	kIx,	:
Art	Art	k1gFnSc1	Art
and	and	k?	and
the	the	k?	the
Landscape	Landscap	k1gInSc5	Landscap
of	of	k?	of
the	the	k?	the
Sixties	Sixties	k1gInSc1	Sixties
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
Press	Press	k1gInSc4	Press
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-520-24116-9	[number]	k4	0-520-24116-9
</s>
</p>
<p>
<s>
Amy	Amy	k?	Amy
Dempsey	Dempsea	k1gFnPc1	Dempsea
<g/>
:	:	kIx,	:
Destination	Destination	k1gInSc1	Destination
Art	Art	k1gFnSc2	Art
<g/>
.	.	kIx.	.
</s>
<s>
Berkeley	Berkeley	k1gInPc1	Berkeley
CA	ca	kA	ca
2006	[number]	k4	2006
ISBN	ISBN	kA	ISBN
978-0-520-25025-3	[number]	k4	978-0-520-25025-3
</s>
</p>
<p>
<s>
Michel	Michel	k1gMnSc1	Michel
Draguet	Draguet	k1gMnSc1	Draguet
<g/>
,	,	kIx,	,
Nils-Udo	Nils-Uda	k1gFnSc5	Nils-Uda
<g/>
,	,	kIx,	,
Bob	Bob	k1gMnSc1	Bob
Verschueren	Verschuerna	k1gFnPc2	Verschuerna
<g/>
,	,	kIx,	,
Bruseels	Bruseels	k1gInSc1	Bruseels
<g/>
:	:	kIx,	:
Atelier	atelier	k1gNnSc1	atelier
340	[number]	k4	340
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Jack	Jack	k1gMnSc1	Jack
Flam	Flam	k1gMnSc1	Flam
(	(	kIx(	(
<g/>
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Smithson	Smithson	k1gMnSc1	Smithson
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Collected	Collected	k1gMnSc1	Collected
Writings	Writings	k1gInSc4	Writings
<g/>
,	,	kIx,	,
Berkeley	Berkele	k1gMnPc4	Berkele
CA	ca	kA	ca
1996	[number]	k4	1996
ISBN	ISBN	kA	ISBN
0-520-20385-2	[number]	k4	0-520-20385-2
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
K.	K.	kA	K.
Grande	grand	k1gMnSc5	grand
<g/>
:	:	kIx,	:
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Balance	balance	k1gFnSc1	balance
<g/>
:	:	kIx,	:
Art	Art	k1gFnSc1	Art
and	and	k?	and
Nature	Natur	k1gMnSc5	Natur
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Rose	Rose	k1gMnSc1	Rose
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
ISBN	ISBN	kA	ISBN
1-55164-234-4	[number]	k4	1-55164-234-4
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Hobbs	Hobbsa	k1gFnPc2	Hobbsa
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Smithson	Smithson	k1gMnSc1	Smithson
<g/>
:	:	kIx,	:
A	A	kA	A
Retrospective	Retrospectiv	k1gInSc5	Retrospectiv
View	View	k1gMnSc1	View
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Lehmbruck	Lehmbruck	k1gMnSc1	Lehmbruck
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
Duisburg	Duisburg	k1gInSc1	Duisburg
/	/	kIx~	/
Herbert	Herbert	k1gMnSc1	Herbert
F.	F.	kA	F.
Johnson	Johnson	k1gMnSc1	Johnson
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
Cornell	Cornell	k1gMnSc1	Cornell
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jeffrey	Jeffrea	k1gFnPc1	Jeffrea
Kastner	Kastnra	k1gFnPc2	Kastnra
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
Wallis	Wallis	k1gFnSc2	Wallis
<g/>
:	:	kIx,	:
Land	Land	k1gMnSc1	Land
and	and	k?	and
Environmental	Environmental	k1gMnSc1	Environmental
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
1998	[number]	k4	1998
ISBN	ISBN	kA	ISBN
0-7148-4519-1	[number]	k4	0-7148-4519-1
</s>
</p>
<p>
<s>
Lucy	Lucy	k1gInPc1	Lucy
R	R	kA	R
Lippard	Lipparda	k1gFnPc2	Lipparda
<g/>
:	:	kIx,	:
Overlay	Overla	k2eAgInPc1d1	Overla
<g/>
:	:	kIx,	:
Contemporary	Contemporar	k1gInPc1	Contemporar
Art	Art	k1gFnSc2	Art
and	and	k?	and
the	the	k?	the
Art	Art	k1gFnSc1	Art
of	of	k?	of
Prehistory	Prehistor	k1gInPc1	Prehistor
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
1983	[number]	k4	1983
ISBN	ISBN	kA	ISBN
0-394-51812-8	[number]	k4	0-394-51812-8
</s>
</p>
<p>
<s>
Udo	Udo	k?	Udo
Weilacher	Weilachra	k1gFnPc2	Weilachra
<g/>
:	:	kIx,	:
Between	Between	k1gInSc1	Between
Landscape	Landscap	k1gInSc5	Landscap
Architecture	Architectur	k1gMnSc5	Architectur
and	and	k?	and
Land	Land	k1gMnSc1	Land
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Basel	Basel	k1gInSc1	Basel
Berlin	berlina	k1gFnPc2	berlina
Boston	Boston	k1gInSc1	Boston
1999	[number]	k4	1999
ISBN	ISBN	kA	ISBN
3-7643-6119-0	[number]	k4	3-7643-6119-0
</s>
</p>
<p>
<s>
Edward	Edward	k1gMnSc1	Edward
Lucie-Smith	Lucie-Smith	k1gMnSc1	Lucie-Smith
(	(	kIx(	(
<g/>
Intro	Intro	k1gNnSc1	Intro
<g/>
)	)	kIx)	)
and	and	k?	and
John	John	k1gMnSc1	John
K.	K.	kA	K.
Grande	grand	k1gMnSc5	grand
<g/>
:	:	kIx,	:
Art	Art	k1gMnSc5	Art
Nature	Natur	k1gMnSc5	Natur
Dialogues	Dialoguesa	k1gFnPc2	Dialoguesa
<g/>
:	:	kIx,	:
Interviews	Interviewsa	k1gFnPc2	Interviewsa
with	with	k1gInSc1	with
Environmental	Environmental	k1gMnSc1	Environmental
Artists	Artists	k1gInSc1	Artists
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
2004	[number]	k4	2004
ISBN	ISBN	kA	ISBN
0-7914-6194-7	[number]	k4	0-7914-6194-7
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Peat	Peat	k1gMnSc1	Peat
&	&	k?	&
Edward	Edward	k1gMnSc1	Edward
Lucie-Smith	Lucie-Smith	k1gMnSc1	Lucie-Smith
(	(	kIx(	(
<g/>
Introduction	Introduction	k1gInSc1	Introduction
&	&	k?	&
forward	forward	k1gInSc1	forward
<g/>
)	)	kIx)	)
Dialogues	Dialogues	k1gInSc1	Dialogues
in	in	k?	in
Diversity	Diversit	k1gInPc7	Diversit
<g/>
,	,	kIx,	,
Italy	Ital	k1gMnPc7	Ital
<g/>
:	:	kIx,	:
Pari	pari	k1gNnPc7	pari
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-88-901960-7-2	[number]	k4	978-88-901960-7-2
</s>
</p>
<p>
<s>
Gilles	Gilles	k1gInSc4	Gilles
A.	A.	kA	A.
Tiberghien	Tiberghien	k2eAgMnSc1d1	Tiberghien
<g/>
:	:	kIx,	:
Land	Land	k1gMnSc1	Land
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Ed	Ed	k?	Ed
<g/>
.	.	kIx.	.
</s>
<s>
Carré	Carrý	k2eAgNnSc1d1	Carré
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
land	land	k6eAd1	land
art	art	k?	art
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Jana	Jan	k1gMnSc2	Jan
Pohribného	Pohribný	k2eAgInSc2d1	Pohribný
</s>
</p>
<p>
<s>
EarthArtists	EarthArtists	k1gInSc1	EarthArtists
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
-	-	kIx~	-
listings	listings	k6eAd1	listings
of	of	k?	of
Earth	Earth	k1gMnSc1	Earth
<g/>
,	,	kIx,	,
Land	Land	k1gMnSc1	Land
<g/>
,	,	kIx,	,
and	and	k?	and
Eco-artists	Ecortists	k1gInSc1	Eco-artists
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alan	Alan	k1gMnSc1	Alan
Sonfist	Sonfist	k1gMnSc1	Sonfist
Official	Official	k1gMnSc1	Official
Website	Websit	k1gInSc5	Websit
</s>
</p>
<p>
<s>
Artist	Artist	k1gInSc1	Artist
in	in	k?	in
Nature	Natur	k1gMnSc5	Natur
International	International	k1gMnSc5	International
Network	network	k1gInSc1	network
</s>
</p>
<p>
<s>
Denarend	Denarend	k1gInSc1	Denarend
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
About	About	k1gMnSc1	About
land	land	k1gMnSc1	land
art	art	k?	art
</s>
</p>
<p>
<s>
Land	Land	k6eAd1	Land
Arts	Arts	k1gInSc1	Arts
of	of	k?	of
the	the	k?	the
American	American	k1gMnSc1	American
West	West	k1gMnSc1	West
</s>
</p>
<p>
<s>
Land	Land	k1gMnSc1	Land
&	&	k?	&
Environmental	Environmental	k1gMnSc1	Environmental
Artists	Artistsa	k1gFnPc2	Artistsa
&	&	k?	&
Art	Art	k1gMnSc1	Art
</s>
</p>
<p>
<s>
Australian	Australian	k1gMnSc1	Australian
land	land	k1gMnSc1	land
arts	arts	k6eAd1	arts
</s>
</p>
<p>
<s>
Roden	Roden	k2eAgInSc4d1	Roden
Crater	Crater	k1gInSc4	Crater
by	by	kYmCp3nS	by
James	James	k1gMnSc1	James
Turrell	Turrell	k1gMnSc1	Turrell
</s>
</p>
<p>
<s>
Land	Land	k1gMnSc1	Land
Art	Art	k1gMnSc1	Art
project	project	k1gMnSc1	project
in	in	k?	in
South	South	k1gMnSc1	South
Africa	Africa	k1gMnSc1	Africa
</s>
</p>
