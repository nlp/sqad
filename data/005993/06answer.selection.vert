<s>
Hněv	hněv	k1gInSc4	hněv
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
afektivní	afektivní	k2eAgFnSc1d1	afektivní
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
při	při	k7c6	při
dosahování	dosahování	k1gNnSc6	dosahování
nějakého	nějaký	k3yIgInSc2	nějaký
cíle	cíl	k1gInSc2	cíl
nebo	nebo	k8xC	nebo
brání	bránit	k5eAaImIp3nS	bránit
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
