<s>
Plukovník	plukovník	k1gMnSc1	plukovník
je	být	k5eAaImIp3nS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
využívaná	využívaný	k2eAgFnSc1d1	využívaná
v	v	k7c6	v
bezpečnostních	bezpečnostní	k2eAgInPc6d1	bezpečnostní
sborech	sbor	k1gInPc6	sbor
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zkratku	zkratka	k1gFnSc4	zkratka
plk.	plk.	kA	plk.
</s>
