<p>
<s>
Plukovník	plukovník	k1gMnSc1	plukovník
je	být	k5eAaImIp3nS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
využívaná	využívaný	k2eAgFnSc1d1	využívaná
v	v	k7c6	v
bezpečnostních	bezpečnostní	k2eAgInPc6d1	bezpečnostní
sborech	sbor	k1gInPc6	sbor
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zkratku	zkratka	k1gFnSc4	zkratka
plk.	plk.	kA	plk.
</s>
<s>
V	v	k7c6	v
Armádě	armáda	k1gFnSc6	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pátou	pátý	k4xOgFnSc4	pátý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
důstojnickou	důstojnický	k2eAgFnSc4d1	důstojnická
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
,	,	kIx,	,
u	u	k7c2	u
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
o	o	k7c4	o
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
<g/>
.	.	kIx.	.
</s>
<s>
Hodnost	hodnost	k1gFnSc1	hodnost
plukovník	plukovník	k1gMnSc1	plukovník
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
podplukovník	podplukovník	k1gMnSc1	podplukovník
a	a	k8xC	a
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
brigádní	brigádní	k2eAgMnSc1d1	brigádní
generál	generál	k1gMnSc1	generál
<g/>
.	.	kIx.	.
</s>
<s>
Armádní	armádní	k2eAgNnSc4d1	armádní
označení	označení	k1gNnSc4	označení
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc1	tři
zlaté	zlatý	k2eAgFnPc1d1	zlatá
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
postranní	postranní	k2eAgFnSc7d1	postranní
tzv.	tzv.	kA	tzv.
kolejničkou	kolejnička	k1gFnSc7	kolejnička
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
proužek	proužek	k1gInSc1	proužek
zlaté	zlatý	k2eAgFnSc2d1	zlatá
barvy	barva	k1gFnSc2	barva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plukovník	plukovník	k1gMnSc1	plukovník
zpravidla	zpravidla	k6eAd1	zpravidla
velí	velet	k5eAaImIp3nS	velet
brigádě	brigáda	k1gFnSc3	brigáda
či	či	k8xC	či
pluku	pluk	k1gInSc3	pluk
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
praporu	prapor	k1gInSc2	prapor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgFnPc4d1	vojenská
hodnosti	hodnost	k1gFnPc4	hodnost
</s>
</p>
<p>
<s>
Hodnosti	hodnost	k1gFnPc1	hodnost
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
plukovník	plukovník	k1gMnSc1	plukovník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
plukovník	plukovník	k1gMnSc1	plukovník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Plukovník	plukovník	k1gMnSc1	plukovník
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
