<p>
<s>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
symbol	symbol	k1gInSc4	symbol
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
hnutí	hnutí	k1gNnSc2	hnutí
reprezentovaného	reprezentovaný	k2eAgNnSc2d1	reprezentované
Mezinárodním	mezinárodnět	k5eAaPmIp1nS	mezinárodnět
olympijským	olympijský	k2eAgInSc7d1	olympijský
výborem	výbor	k1gInSc7	výbor
(	(	kIx(	(
<g/>
MOV	MOV	kA	MOV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
bílého	bílý	k2eAgInSc2d1	bílý
obdélníku	obdélník	k1gInSc2	obdélník
(	(	kIx(	(
<g/>
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
symbol	symbol	k1gInSc1	symbol
olympismu	olympismus	k1gInSc2	olympismus
–	–	k?	–
pět	pět	k4xCc4	pět
navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
protínajících	protínající	k2eAgInPc2d1	protínající
kruhů	kruh	k1gInPc2	kruh
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnSc2d1	černá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnSc2d1	zelená
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobu	podoba	k1gFnSc4	podoba
vlajky	vlajka	k1gFnSc2	vlajka
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
olympijského	olympijský	k2eAgNnSc2d1	Olympijské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
baron	baron	k1gMnSc1	baron
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k1gMnSc1	Coubertin
a	a	k8xC	a
představil	představit	k5eAaPmAgMnS	představit
ji	on	k3xPp3gFnSc4	on
olympijskému	olympijský	k2eAgInSc3d1	olympijský
výboru	výbor	k1gInSc3	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Plánované	plánovaný	k2eAgNnSc4d1	plánované
první	první	k4xOgNnSc4	první
použití	použití	k1gNnSc4	použití
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c6	na
Olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1916	[number]	k4	1916
zhatila	zhatit	k5eAaPmAgFnS	zhatit
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
poprvé	poprvé	k6eAd1	poprvé
zavlála	zavlát	k5eAaPmAgFnS	zavlát
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
až	až	k9	až
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
přesnou	přesný	k2eAgFnSc4d1	přesná
podobu	podoba	k1gFnSc4	podoba
i	i	k8xC	i
hlavní	hlavní	k2eAgFnPc1d1	hlavní
zásady	zásada	k1gFnPc1	zásada
jejího	její	k3xOp3gNnSc2	její
používání	používání	k1gNnSc2	používání
definuje	definovat	k5eAaBmIp3nS	definovat
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
charta	charta	k1gFnSc1	charta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
vztyčována	vztyčován	k2eAgFnSc1d1	vztyčována
během	během	k7c2	během
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
zahajovacího	zahajovací	k2eAgInSc2d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
každých	každý	k3xTgFnPc2	každý
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
olympijský	olympijský	k2eAgInSc4d1	olympijský
stadion	stadion	k1gInSc4	stadion
ji	on	k3xPp3gFnSc4	on
přináší	přinášet	k5eAaImIp3nS	přinášet
rozprostřenou	rozprostřený	k2eAgFnSc4d1	rozprostřená
ve	v	k7c6	v
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
poloze	poloha	k1gFnSc6	poloha
delegace	delegace	k1gFnSc2	delegace
vybraných	vybraný	k2eAgMnPc2d1	vybraný
sportovců	sportovec	k1gMnPc2	sportovec
nebo	nebo	k8xC	nebo
významných	významný	k2eAgFnPc2d1	významná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
svojí	svůj	k3xOyFgFnSc7	svůj
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
prací	práce	k1gFnSc7	práce
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
rituál	rituál	k1gInSc4	rituál
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
musí	muset	k5eAaImIp3nS	muset
nad	nad	k7c7	nad
stadionem	stadion	k1gInSc7	stadion
vlát	vlát	k5eAaImF	vlát
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
her	hra	k1gFnPc2	hra
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
snětí	snětí	k1gNnSc1	snětí
během	během	k7c2	během
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
zakončovacího	zakončovací	k2eAgInSc2d1	zakončovací
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
je	být	k5eAaImIp3nS	být
signálem	signál	k1gInSc7	signál
ukončení	ukončení	k1gNnSc2	ukončení
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
při	při	k7c6	při
olympijském	olympijský	k2eAgInSc6d1	olympijský
slibu	slib	k1gInSc6	slib
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
her	hra	k1gFnPc2	hra
vrací	vracet	k5eAaImIp3nS	vracet
starosta	starosta	k1gMnSc1	starosta
(	(	kIx(	(
<g/>
primátor	primátor	k1gMnSc1	primátor
<g/>
)	)	kIx)	)
organizátorského	organizátorský	k2eAgNnSc2d1	organizátorské
města	město	k1gNnSc2	město
vlajku	vlajka	k1gFnSc4	vlajka
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
MOV	MOV	kA	MOV
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
předává	předávat	k5eAaImIp3nS	předávat
starostovi	starosta	k1gMnSc3	starosta
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
příštím	příští	k2eAgMnSc7d1	příští
hostitelem	hostitel	k1gMnSc7	hostitel
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgFnPc1	tři
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
exemplářů	exemplář	k1gInPc2	exemplář
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
lemovány	lemovat	k5eAaImNgInP	lemovat
šestibarevnými	šestibarevný	k2eAgFnPc7d1	šestibarevná
třásněmi	třáseň	k1gFnPc7	třáseň
a	a	k8xC	a
k	k	k7c3	k
žerdi	žerď	k1gFnSc3	žerď
jsou	být	k5eAaImIp3nP	být
upevněny	upevnit	k5eAaPmNgFnP	upevnit
pomocí	pomocí	k7c2	pomocí
šestibarevné	šestibarevný	k2eAgFnSc2d1	šestibarevná
stuhy	stuha	k1gFnSc2	stuha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Antverpská	antverpský	k2eAgFnSc1d1	Antverpská
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
Mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
olympijskému	olympijský	k2eAgInSc3d1	olympijský
výboru	výbor	k1gInSc3	výbor
věnována	věnovat	k5eAaPmNgFnS	věnovat
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1920	[number]	k4	1920
belgickým	belgický	k2eAgNnSc7d1	Belgické
městem	město	k1gNnSc7	město
Antverpy	Antverpy	k1gFnPc5	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
předávána	předávat	k5eAaImNgFnS	předávat
mezi	mezi	k7c7	mezi
organizátory	organizátor	k1gInPc7	organizátor
letních	letní	k2eAgFnPc2d1	letní
olympiád	olympiáda	k1gFnPc2	olympiáda
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oselská	oselský	k2eAgFnSc1d1	oselský
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
Mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
olympijskému	olympijský	k2eAgInSc3d1	olympijský
výboru	výbor	k1gInSc3	výbor
věnována	věnovat	k5eAaImNgFnS	věnovat
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1952	[number]	k4	1952
norským	norský	k2eAgNnSc7d1	norské
městem	město	k1gNnSc7	město
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
předávána	předávat	k5eAaImNgFnS	předávat
mezi	mezi	k7c7	mezi
organizátory	organizátor	k1gInPc7	organizátor
zimních	zimní	k2eAgFnPc2d1	zimní
olympiád	olympiáda	k1gFnPc2	olympiáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soulská	soulský	k2eAgFnSc1d1	Soulská
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
Mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
olympijskému	olympijský	k2eAgInSc3d1	olympijský
výboru	výbor	k1gInSc3	výbor
věnována	věnovat	k5eAaPmNgFnS	věnovat
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1988	[number]	k4	1988
korejským	korejský	k2eAgNnSc7d1	korejské
městem	město	k1gNnSc7	město
Soul	Soul	k1gInSc1	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
předávána	předávat	k5eAaImNgFnS	předávat
mezi	mezi	k7c7	mezi
organizátory	organizátor	k1gInPc7	organizátor
letních	letní	k2eAgFnPc2d1	letní
olympiád	olympiáda	k1gFnPc2	olympiáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
barev	barva	k1gFnPc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Baron	baron	k1gMnSc1	baron
de	de	k?	de
Coubertin	Coubertin	k1gMnSc1	Coubertin
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
podobu	podoba	k1gFnSc4	podoba
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
význam	význam	k1gInSc4	význam
zobrazených	zobrazený	k2eAgInPc2d1	zobrazený
symbolů	symbol	k1gInPc2	symbol
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
bílé	bílý	k2eAgNnSc4d1	bílé
pozadí	pozadí	k1gNnSc4	pozadí
s	s	k7c7	s
pěti	pět	k4xCc2	pět
protínajícími	protínající	k2eAgFnPc7d1	protínající
se	se	k3xPyFc4	se
kruhy	kruh	k1gInPc7	kruh
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
:	:	kIx,	:
Modrý	modrý	k2eAgInSc1d1	modrý
kruh	kruh	k1gInSc1	kruh
–	–	k?	–
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
Žlutý	žlutý	k2eAgInSc1d1	žlutý
kruh	kruh	k1gInSc1	kruh
–	–	k?	–
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Černý	černý	k2eAgInSc1d1	černý
kruh	kruh	k1gInSc1	kruh
–	–	k?	–
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Zelený	zelený	k2eAgInSc1d1	zelený
kruh	kruh	k1gInSc1	kruh
–	–	k?	–
Austrálie	Austrálie	k1gFnSc2	Austrálie
Oceánie	Oceánie	k1gFnSc2	Oceánie
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
kruh	kruh	k1gInSc1	kruh
–	–	k?	–
AmerikaTato	AmerikaTat	k2eAgNnSc1d1	AmerikaTat
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
symbolická	symbolický	k2eAgFnSc1d1	symbolická
<g/>
;	;	kIx,	;
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
pět	pět	k4xCc1	pět
kontinentů	kontinent	k1gInPc2	kontinent
spojených	spojený	k2eAgInPc2d1	spojený
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
myšlenkou	myšlenka	k1gFnSc7	myšlenka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
těchto	tento	k3xDgFnPc2	tento
šest	šest	k4xCc1	šest
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
i	i	k9	i
s	s	k7c7	s
bílým	bílý	k2eAgNnSc7d1	bílé
pozadím	pozadí	k1gNnSc7	pozadí
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
národních	národní	k2eAgFnPc6d1	národní
vlajkách	vlajka	k1gFnPc6	vlajka
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Textes	Textes	k1gMnSc1	Textes
choisis	choisis	k1gFnSc2	choisis
II	II	kA	II
<g/>
,	,	kIx,	,
p.	p.	k?	p.
<g/>
470	[number]	k4	470
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
baron	baron	k1gMnSc1	baron
de	de	k?	de
Coubertin	Coubertin	k1gMnSc1	Coubertin
očividně	očividně	k6eAd1	očividně
vnímal	vnímat	k5eAaImAgMnS	vnímat
kruhy	kruh	k1gInPc4	kruh
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
jako	jako	k8xS	jako
dva	dva	k4xCgInPc4	dva
nezávislé	závislý	k2eNgInPc4d1	nezávislý
symboly	symbol	k1gInPc4	symbol
a	a	k8xC	a
MOV	MOV	kA	MOV
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
kruhů	kruh	k1gInPc2	kruh
nereprezentuje	reprezentovat	k5eNaImIp3nS	reprezentovat
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
kontinent	kontinent	k1gInSc4	kontinent
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádí	uvádět	k5eAaImIp3nP	uvádět
seznamy	seznam	k1gInPc4	seznam
významů	význam	k1gInPc2	význam
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
</s>
</p>
<p>
<s>
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Olympic	Olympic	k1gMnSc1	Olympic
symbols	symbols	k6eAd1	symbols
</s>
</p>
