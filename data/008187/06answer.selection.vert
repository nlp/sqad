<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
bílého	bílý	k2eAgInSc2d1	bílý
obdélníku	obdélník	k1gInSc2	obdélník
(	(	kIx(	(
<g/>
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
symbol	symbol	k1gInSc1	symbol
olympismu	olympismus	k1gInSc2	olympismus
–	–	k?	–
pět	pět	k4xCc4	pět
navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
protínajících	protínající	k2eAgInPc2d1	protínající
kruhů	kruh	k1gInPc2	kruh
modré	modrý	k2eAgFnSc2d1	modrá
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnSc2d1	žlutá
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnSc2d1	černá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnSc2d1	zelená
a	a	k8xC	a
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
