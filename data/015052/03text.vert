<s>
Juuru	Juura	k1gFnSc4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Juuru	Juura	k1gFnSc4
vald	vald	k6eAd1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
Juuru	Juur	k1gInSc2
v	v	k7c6
rámci	rámec	k1gInSc6
kraje	kraj	k1gInSc2
Raplamaa	Raplama	k1gInSc2
(	(	kIx(
<g/>
před	před	k7c7
rokem	rok	k1gInSc7
2017	#num#	k4
<g/>
)	)	kIx)
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Juuru	Juura	k1gFnSc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
59	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
24	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
1	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
152,4	152,4	k4
km²	km²	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
1	#num#	k4
627	#num#	k4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
10,7	10,7	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Raplamaa	Raplamaa	k6eAd1
Druh	druh	k1gInSc1
celku	celek	k1gInSc2
</s>
<s>
Obec	obec	k1gFnSc1
(	(	kIx(
<g/>
vald	vald	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Juuru	Juur	k1gInSc2
(	(	kIx(
<g/>
estonsky	estonsky	k6eAd1
Juuru	Juura	k1gFnSc4
vald	valda	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
samosprávná	samosprávný	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
estonském	estonský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
Raplamaa	Raplama	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
zanikla	zaniknout	k5eAaPmAgFnS
svým	svůj	k3xOyFgNnSc7
začleněním	začlenění	k1gNnSc7
do	do	k7c2
obce	obec	k1gFnSc2
Rapla	Rapla	k?
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
bývalé	bývalý	k2eAgFnSc2d1
obce	obec	k1gFnSc2
žije	žít	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
šestnáct	šestnáct	k4xCc1
set	sto	k4xCgNnPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
asi	asi	k9
třetina	třetina	k1gFnSc1
v	v	k7c6
městečku	městečko	k1gNnSc6
Juuru	Juur	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
administrativním	administrativní	k2eAgInSc7d1
centrem	centr	k1gInSc7
obce	obec	k1gFnSc2
a	a	k8xC
podle	podle	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
byla	být	k5eAaImAgFnS
obec	obec	k1gFnSc1
pojmenována	pojmenovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
obce	obec	k1gFnSc2
dále	daleko	k6eAd2
patřilo	patřit	k5eAaImAgNnS
11	#num#	k4
vesnic	vesnice	k1gFnPc2
—	—	k?
Atla	Atla	k1gMnSc1
<g/>
,	,	kIx,
Hõ	Hõ	k1gMnSc1
<g/>
,	,	kIx,
Härgla	Härgla	k1gMnSc1
<g/>
,	,	kIx,
Jaluse	Jaluse	k1gFnSc1
<g/>
,	,	kIx,
Järlepa	Järlepa	k1gFnSc1
<g/>
,	,	kIx,
Lõ	Lõ	k1gFnSc1
<g/>
,	,	kIx,
Mahtra	Mahtra	k1gFnSc1
<g/>
,	,	kIx,
Maidla	Maidla	k1gFnSc1
<g/>
,	,	kIx,
Orguse	Orguse	k1gFnSc1
<g/>
,	,	kIx,
Pirgu	Pirga	k1gFnSc4
a	a	k8xC
Vankse	Vankse	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Juuru	Juur	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
(	(	kIx(
<g/>
estonsky	estonsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
