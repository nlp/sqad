<s>
Traband	Traband	k1gInSc1	Traband
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Traband	Traband	k1gInSc1	Traband
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xC	jako
trojčlenná	trojčlenný	k2eAgFnSc1d1	trojčlenná
kapela	kapela	k1gFnSc1	kapela
kolem	kolem	k7c2	kolem
textaře	textař	k1gMnSc2	textař
<g/>
,	,	kIx,	,
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
kytaristy	kytarista	k1gMnSc2	kytarista
Jardy	Jarda	k1gMnSc2	Jarda
Svobody	Svoboda	k1gMnSc2	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
hrál	hrát	k5eAaImAgMnS	hrát
Michal	Michal	k1gMnSc1	Michal
Kliner	Kliner	k1gMnSc1	Kliner
a	a	k8xC	a
na	na	k7c6	na
bicí	bicí	k2eAgMnPc1d1	bicí
Václav	Václav	k1gMnSc1	Václav
Pohl	Pohl	k1gMnSc1	Pohl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
o	o	k7c6	o
banjistu	banjist	k1gInSc6	banjist
Evžena	Evžen	k1gMnSc2	Evžen
Kredence	kredenc	k1gFnSc2	kredenc
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydali	vydat	k5eAaPmAgMnP	vydat
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
O	o	k7c6	o
čem	co	k3yInSc6	co
mluví	mluvit	k5eAaImIp3nP	mluvit
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
prvních	první	k4xOgNnPc2	první
let	léto	k1gNnPc2	léto
Trabandu	Traband	k1gInSc2	Traband
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zkomolenina	zkomolenina	k1gFnSc1	zkomolenina
slovního	slovní	k2eAgNnSc2d1	slovní
spojení	spojení	k1gNnSc2	spojení
trio-band	trioand	k1gInSc1	trio-band
(	(	kIx(	(
<g/>
tříčlenná	tříčlenný	k2eAgFnSc1d1	tříčlenná
kapela	kapela	k1gFnSc1	kapela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
parodie	parodie	k1gFnSc1	parodie
na	na	k7c4	na
automobil	automobil	k1gInSc4	automobil
Trabant	trabant	k1gInSc4	trabant
(	(	kIx(	(
<g/>
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
i	i	k9	i
označení	označení	k1gNnSc1	označení
členů	člen	k1gInPc2	člen
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
trabanditů	trabandita	k1gMnPc2	trabandita
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
jako	jako	k9	jako
rocková	rockový	k2eAgFnSc1d1	rocková
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sami	sám	k3xTgMnPc1	sám
tvůrci	tvůrce	k1gMnPc1	tvůrce
svůj	svůj	k3xOyFgInSc4	svůj
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
žánr	žánr	k1gInSc4	žánr
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dechno	dechno	k6eAd1	dechno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
smíšenina	smíšenina	k1gFnSc1	smíšenina
dechové	dechový	k2eAgFnSc2d1	dechová
muziky	muzika	k1gFnSc2	muzika
a	a	k8xC	a
techna	techn	k1gInSc2	techn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
také	také	k9	také
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dlouhým	dlouhý	k2eAgFnPc3d1	dlouhá
mezihrám	mezihra	k1gFnPc3	mezihra
s	s	k7c7	s
bicími	bicí	k2eAgInPc7d1	bicí
a	a	k8xC	a
žesťovými	žesťový	k2eAgInPc7d1	žesťový
nástroji	nástroj	k1gInPc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
obměně	obměna	k1gFnSc3	obměna
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Kliner	Kliner	k1gMnSc1	Kliner
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Pohl	Pohl	k1gMnSc1	Pohl
odešli	odejít	k5eAaPmAgMnP	odejít
a	a	k8xC	a
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
je	on	k3xPp3gMnPc4	on
Jana	Jan	k1gMnSc4	Jan
Modráčková	modráčkový	k2eAgFnSc1d1	Modráčková
(	(	kIx(	(
<g/>
trubka	trubka	k1gFnSc1	trubka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Škarda	Škarda	k1gMnSc1	Škarda
(	(	kIx(	(
<g/>
tuba	tuba	k1gFnSc1	tuba
<g/>
)	)	kIx)	)
a	a	k8xC	a
nový	nový	k2eAgMnSc1d1	nový
bubeník	bubeník	k1gMnSc1	bubeník
Petr	Petr	k1gMnSc1	Petr
Vizina	Vizina	k1gMnSc1	Vizina
<g/>
,	,	kIx,	,
Jarda	Jarda	k1gMnSc1	Jarda
Svoboda	Svoboda	k1gMnSc1	Svoboda
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
spíše	spíše	k9	spíše
na	na	k7c4	na
klarinet	klarinet	k1gInSc4	klarinet
a	a	k8xC	a
akordeon	akordeon	k1gInSc4	akordeon
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
Koubkem	Koubek	k1gMnSc7	Koubek
a	a	k8xC	a
s	s	k7c7	s
divadly	divadlo	k1gNnPc7	divadlo
Na	na	k7c6	na
voru	vor	k1gInSc6	vor
<g/>
,	,	kIx,	,
Koňmo	koňmo	k6eAd1	koňmo
a	a	k8xC	a
bratry	bratr	k1gMnPc4	bratr
Formanovými	Formanová	k1gFnPc7	Formanová
(	(	kIx(	(
<g/>
Matějem	Matěj	k1gMnSc7	Matěj
a	a	k8xC	a
Petrem	Petr	k1gMnSc7	Petr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
přišel	přijít	k5eAaPmAgMnS	přijít
baskřídlovkář	baskřídlovkář	k1gMnSc1	baskřídlovkář
Jakub	Jakub	k1gMnSc1	Jakub
Schmid	Schmid	k1gInSc4	Schmid
<g/>
,	,	kIx,	,
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
občas	občas	k6eAd1	občas
hrál	hrát	k5eAaImAgMnS	hrát
i	i	k9	i
saxofonista	saxofonista	k1gMnSc1	saxofonista
Vladimír	Vladimír	k1gMnSc1	Vladimír
Javorský	Javorský	k1gMnSc1	Javorský
<g/>
.	.	kIx.	.
</s>
<s>
Traband	Traband	k1gInSc1	Traband
jezdil	jezdit	k5eAaImAgInS	jezdit
na	na	k7c4	na
zahraniční	zahraniční	k2eAgNnSc4d1	zahraniční
turné	turné	k1gNnSc4	turné
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
i	i	k8xC	i
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
v	v	k7c4	v
první	první	k4xOgInSc4	první
jarní	jarní	k2eAgInSc4d1	jarní
den	den	k1gInSc4	den
kapela	kapela	k1gFnSc1	kapela
pořádala	pořádat	k5eAaImAgFnS	pořádat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Jaroslavení	Jaroslavení	k1gNnSc1	Jaroslavení
aneb	aneb	k?	aneb
MHD	MHD	kA	MHD
Tour	Tour	k1gInSc1	Tour
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
hrála	hrát	k5eAaImAgFnS	hrát
akusticky	akusticky	k6eAd1	akusticky
v	v	k7c6	v
tramvaji	tramvaj	k1gFnSc6	tramvaj
č.	č.	k?	č.
17	[number]	k4	17
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
provozu	provoz	k1gInSc2	provoz
cestou	cesta	k1gFnSc7	cesta
z	z	k7c2	z
Modřan	Modřany	k1gInPc2	Modřany
do	do	k7c2	do
Ďáblic	ďáblice	k1gFnPc2	ďáblice
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následoval	následovat	k5eAaImAgInS	následovat
klasický	klasický	k2eAgInSc4d1	klasický
koncert	koncert	k1gInSc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
odešel	odejít	k5eAaPmAgMnS	odejít
Petr	Petr	k1gMnSc1	Petr
Vizina	Vizina	k1gMnSc1	Vizina
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Pohl	Pohl	k1gMnSc1	Pohl
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
10	[number]	k4	10
let	léto	k1gNnPc2	léto
trvání	trvání	k1gNnSc2	trvání
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
záznam	záznam	k1gInSc1	záznam
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyšla	vyjít	k5eAaPmAgFnS	vyjít
také	také	k9	také
kniha	kniha	k1gFnSc1	kniha
Kontraband	kontrabanda	k1gFnPc2	kontrabanda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
8	[number]	k4	8
výtvarníků	výtvarník	k1gMnPc2	výtvarník
ztvárnilo	ztvárnit	k5eAaPmAgNnS	ztvárnit
písničky	písnička	k1gFnPc4	písnička
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
komiksové	komiksový	k2eAgFnSc6d1	komiksová
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roční	roční	k2eAgFnSc6d1	roční
pauze	pauza	k1gFnSc6	pauza
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2007	[number]	k4	2007
Traband	Traband	k1gInSc1	Traband
vrátil	vrátit	k5eAaPmAgInS	vrátit
ke	k	k7c3	k
koncertování	koncertování	k1gNnSc3	koncertování
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
repertoárem	repertoár	k1gInSc7	repertoár
a	a	k8xC	a
komornějším	komorní	k2eAgNnSc7d2	komornější
obsazením	obsazení	k1gNnSc7	obsazení
Jarda	Jarda	k1gMnSc1	Jarda
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Modráčková	modráčkový	k2eAgFnSc1d1	Modráčková
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Pohl	Pohl	k1gMnSc1	Pohl
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
nového	nový	k2eAgInSc2d1	nový
repertoáru	repertoár	k1gInSc2	repertoár
představil	představit	k5eAaPmAgMnS	představit
Jarda	Jarda	k1gMnSc1	Jarda
Svoboda	Svoboda	k1gMnSc1	Svoboda
divákům	divák	k1gMnPc3	divák
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
na	na	k7c6	na
několika	několik	k4yIc2	několik
sólových	sólový	k2eAgNnPc6d1	sólové
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
na	na	k7c4	na
harmonium	harmonium	k1gNnSc4	harmonium
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
s	s	k7c7	s
názvem	název	k1gInSc7	název
Přítel	přítel	k1gMnSc1	přítel
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
bylo	být	k5eAaImAgNnS	být
již	již	k9	již
oficiálně	oficiálně	k6eAd1	oficiálně
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
v	v	k7c6	v
mp	mp	k?	mp
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
či	či	k8xC	či
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
s	s	k7c7	s
Trabandem	Trabando	k1gNnSc7	Trabando
opět	opět	k6eAd1	opět
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
tubista	tubista	k1gMnSc1	tubista
Robert	Robert	k1gMnSc1	Robert
Škarda	Škarda	k1gMnSc1	Škarda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
se	se	k3xPyFc4	se
také	také	k9	také
vdala	vdát	k5eAaPmAgFnS	vdát
Jana	Jana	k1gFnSc1	Jana
Modráčková	modráčkový	k2eAgFnSc1d1	Modráčková
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
Traband	Traband	k1gInSc1	Traband
přerušil	přerušit	k5eAaPmAgInS	přerušit
koncertování	koncertování	k1gNnSc4	koncertování
kvůli	kvůli	k7c3	kvůli
těhotenství	těhotenství	k1gNnSc3	těhotenství
Jany	Jana	k1gFnSc2	Jana
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
Emilka	Emilka	k1gFnSc1	Emilka
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
a	a	k8xC	a
Traband	Traband	k1gInSc1	Traband
se	se	k3xPyFc4	se
koncerty	koncert	k1gInPc1	koncert
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
Jimramově	Jimramově	k1gFnSc6	Jimramově
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
a	a	k8xC	a
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
Vagóně	vagón	k1gInSc6	vagón
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
na	na	k7c6	na
křtu	křest	k1gInSc6	křest
alba	album	k1gNnSc2	album
Ztracený	ztracený	k2eAgInSc1d1	ztracený
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
A	a	k9	a
Tribute	tribut	k1gInSc5	tribut
to	ten	k3xDgNnSc4	ten
Oldřich	Oldřich	k1gMnSc1	Oldřich
Janota	Janota	k1gMnSc1	Janota
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
natočil	natočit	k5eAaBmAgInS	natočit
Janotovu	Janotův	k2eAgFnSc4d1	Janotova
píseň	píseň	k1gFnSc4	píseň
Druhý	druhý	k4xOgInSc4	druhý
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
pódia	pódium	k1gNnPc4	pódium
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
repertoárem	repertoár	k1gInSc7	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
nového	nový	k2eAgInSc2d1	nový
repertoáru	repertoár	k1gInSc2	repertoár
pak	pak	k6eAd1	pak
nahrál	nahrát	k5eAaBmAgMnS	nahrát
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c6	na
albu	album	k1gNnSc6	album
Domasa	Domas	k1gMnSc2	Domas
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgNnSc4d3	nejnovější
album	album	k1gNnSc4	album
a	a	k8xC	a
DVD	DVD	kA	DVD
vydal	vydat	k5eAaPmAgMnS	vydat
Traband	Traband	k1gInSc4	Traband
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
měl	mít	k5eAaImAgInS	mít
Traband	Traband	k1gInSc1	Traband
opět	opět	k6eAd1	opět
koncertní	koncertní	k2eAgFnSc4d1	koncertní
pauzu	pauza	k1gFnSc4	pauza
kvůli	kvůli	k7c3	kvůli
těhotenství	těhotenství	k1gNnSc3	těhotenství
Jany	Jana	k1gFnSc2	Jana
Kaplanové	Kaplanová	k1gFnSc2	Kaplanová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2014	[number]	k4	2014
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
hraje	hrát	k5eAaImIp3nS	hrát
stálý	stálý	k2eAgMnSc1d1	stálý
host	host	k1gMnSc1	host
Radim	Radim	k1gMnSc1	Radim
Huml	Huml	k1gMnSc1	Huml
na	na	k7c4	na
banjo	banjo	k1gNnSc4	banjo
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
nahráli	nahrát	k5eAaPmAgMnP	nahrát
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
v	v	k7c6	v
lesním	lesní	k2eAgNnSc6d1	lesní
studiu	studio	k1gNnSc6	studio
Michala	Michal	k1gMnSc2	Michal
Dittricha	Dittrich	k1gMnSc2	Dittrich
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
dále	daleko	k6eAd2	daleko
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
na	na	k7c6	na
albech	album	k1gNnPc6	album
Bůhví	bůhví	k0	bůhví
<g/>
,	,	kIx,	,
Ester	Ester	k1gFnPc1	Ester
<g/>
,	,	kIx,	,
Ivána	Ivána	k1gFnSc1	Ivána
Gutiérreze	Gutiérreze	k1gFnSc2	Gutiérreze
<g/>
,	,	kIx,	,
Hadrů	hadr	k1gInPc2	hadr
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
Jaksi	jaksi	k6eAd1	jaksi
Taksi	Takse	k1gFnSc4	Takse
<g/>
,	,	kIx,	,
Navzájem	navzájem	k6eAd1	navzájem
<g/>
,	,	kIx,	,
Sester	sestra	k1gFnPc2	sestra
Steinových	Steinová	k1gFnPc2	Steinová
<g/>
,	,	kIx,	,
Petra	Petr	k1gMnSc2	Petr
Nikla	Nikl	k1gMnSc2	Nikl
<g/>
,	,	kIx,	,
Šarközi	Šarköh	k1gMnPc1	Šarköh
a	a	k8xC	a
Tří	tři	k4xCgFnPc2	tři
sester	sestra	k1gFnPc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Jarda	Jarda	k1gMnSc1	Jarda
Svoboda	Svoboda	k1gMnSc1	Svoboda
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
živém	živé	k1gNnSc6	živé
DVD	DVD	kA	DVD
skupiny	skupina	k1gFnSc2	skupina
Sto	sto	k4xCgNnSc4	sto
zvířat	zvíře	k1gNnPc2	zvíře
Jste	být	k5eAaImIp2nP	být
normální	normální	k2eAgInPc1d1	normální
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
píseň	píseň	k1gFnSc4	píseň
Černej	černat	k5eAaImRp2nS	černat
pasažér	pasažér	k1gMnSc1	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
DémoNahrávky	DémoNahrávka	k1gFnPc1	DémoNahrávka
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
-	-	kIx~	-
demo	demo	k2eAgInPc2d1	demo
<g/>
)	)	kIx)	)
O	o	k7c6	o
čem	co	k3yInSc6	co
mluví	mluvit	k5eAaImIp3nP	mluvit
muži	muž	k1gMnPc1	muž
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
APAC	APAC	kA	APAC
<g/>
)	)	kIx)	)
Práce	práce	k1gFnSc1	práce
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
-	-	kIx~	-
demo	demo	k2eAgFnSc1d1	demo
<g/>
)	)	kIx)	)
Zebra	zebra	k1gFnSc1	zebra
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
sampler	sampler	k1gInSc1	sampler
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
časopisu	časopis	k1gInSc2	časopis
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
pop	pop	k1gInSc1	pop
-	-	kIx~	-
2	[number]	k4	2
písně	píseň	k1gFnPc1	píseň
<g/>
)	)	kIx)	)
Kolotoč	kolotoč	k1gInSc1	kolotoč
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Black	Black	k1gInSc1	Black
Point	pointa	k1gFnPc2	pointa
<g/>
)	)	kIx)	)
Road	Road	k1gInSc1	Road
Movie	Movie	k1gFnSc2	Movie
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Black	Black	k1gInSc1	Black
Point	pointa	k1gFnPc2	pointa
<g/>
)	)	kIx)	)
Hyjé	Hyjé	k1gNnSc2	Hyjé
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Records	Records	k1gInSc1	Records
<g/>
)	)	kIx)	)
-	-	kIx~	-
album	album	k1gNnSc1	album
oceněno	oceněn	k2eAgNnSc1d1	oceněno
Andělem	Anděl	k1gMnSc7	Anděl
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
worldmusic	worldmusice	k1gInPc2	worldmusice
10	[number]	k4	10
let	léto	k1gNnPc2	léto
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Records	Records	k1gInSc1	Records
<g/>
)	)	kIx)	)
CD	CD	kA	CD
<g/>
+	+	kIx~	+
<g/>
DVD	DVD	kA	DVD
Přítel	přítel	k1gMnSc1	přítel
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Scope	Scop	k1gInSc5	Scop
Records	Records	k1gInSc4	Records
<g/>
)	)	kIx)	)
-	-	kIx~	-
album	album	k1gNnSc1	album
oceněno	oceněn	k2eAgNnSc1d1	oceněno
Andělem	Anděl	k1gMnSc7	Anděl
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
folk	folk	k1gInSc1	folk
&	&	k?	&
country	country	k2eAgMnSc2d1	country
Domasa	Domas	k1gMnSc2	Domas
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Scope	Scop	k1gInSc5	Scop
<g/>
)	)	kIx)	)
-	-	kIx~	-
album	album	k1gNnSc1	album
oceněno	oceněn	k2eAgNnSc1d1	oceněno
Andělem	Anděl	k1gMnSc7	Anděl
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
folk	folk	k1gInSc1	folk
&	&	k?	&
country	country	k2eAgFnPc4d1	country
Neslýchané	slýchaný	k2eNgFnPc4d1	neslýchaná
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Indies	Indies	k1gInSc1	Indies
Scope	Scop	k1gInSc5	Scop
<g/>
)	)	kIx)	)
Vlnobeat	Vlnobeat	k1gInSc1	Vlnobeat
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Jarda	Jarda	k1gMnSc1	Jarda
Svoboda	Svoboda	k1gMnSc1	Svoboda
Otcovy	otcův	k2eAgFnPc4d1	otcova
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Naholou	Nahola	k1gFnSc7	Nahola
25	[number]	k4	25
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Zuby	zub	k1gInPc1	zub
nehty	nehet	k1gInPc7	nehet
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Jana	Jana	k1gFnSc1	Jana
Kaplanová	Kaplanová	k1gFnSc1	Kaplanová
Kolohříbek	Kolohříbek	k1gInSc4	Kolohříbek
Gousteen	Gousteen	k2eAgInSc1d1	Gousteen
Crowlers	Crowlers	k1gInSc1	Crowlers
Ztráty	ztráta	k1gFnPc1	ztráta
a	a	k8xC	a
nálezy	nález	k1gInPc1	nález
Apple	Apple	kA	Apple
Undersound	Undersounda	k1gFnPc2	Undersounda
Zuby	zub	k1gInPc1	zub
nehty	nehet	k1gInPc4	nehet
(	(	kIx(	(
<g/>
od	od	k7c2	od
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
Klec	klec	k1gFnSc1	klec
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Původní	původní	k2eAgMnSc1d1	původní
Bureš	Bureš	k1gMnSc1	Bureš
(	(	kIx(	(
<g/>
od	od	k7c2	od
2002	[number]	k4	2002
-	-	kIx~	-
příležitostně	příležitostně	k6eAd1	příležitostně
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
trubka	trubka	k1gFnSc1	trubka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Pláče	plakat	k5eAaImIp3nS	plakat
kočka	kočka	k1gFnSc1	kočka
(	(	kIx(	(
<g/>
od	od	k7c2	od
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
-	-	kIx~	-
bicí	bicí	k2eAgFnSc2d1	bicí
Mužy	Muža	k1gFnSc2	Muža
(	(	kIx(	(
<g/>
od	od	k7c2	od
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Jakub	Jakub	k1gMnSc1	Jakub
Schmid	Schmida	k1gFnPc2	Schmida
Jarabáci	Jarabák	k1gMnPc1	Jarabák
(	(	kIx(	(
<g/>
od	od	k7c2	od
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Klec	klec	k1gFnSc1	klec
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
-	-	kIx~	-
trubka	trubka	k1gFnSc1	trubka
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
<g/>
,	,	kIx,	,
akordeon	akordeon	k1gInSc1	akordeon
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Ponožky	ponožka	k1gFnSc2	ponožka
pana	pan	k1gMnSc2	pan
Semtamťuka	Semtamťuk	k1gMnSc2	Semtamťuk
(	(	kIx(	(
<g/>
od	od	k7c2	od
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Robert	Robert	k1gMnSc1	Robert
Škarda	Škarda	k1gMnSc1	Škarda
Kamil	Kamil	k1gMnSc1	Kamil
Jasmín	jasmín	k1gInSc4	jasmín
Síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
od	od	k7c2	od
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Ponožky	ponožka	k1gFnSc2	ponožka
pana	pan	k1gMnSc2	pan
Semtamťuka	Semtamťuk	k1gMnSc2	Semtamťuk
(	(	kIx(	(
<g/>
od	od	k7c2	od
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Tuning	Tuning	k1gInSc1	Tuning
Metronomes	Metronomes	k1gMnSc1	Metronomes
Václav	Václav	k1gMnSc1	Václav
Pohl	Pohl	k1gMnSc1	Pohl
Mrakoplaš	Mrakoplaš	k1gMnSc1	Mrakoplaš
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
H.A.	H.A.	k1gFnSc3	H.A.
<g/>
LL	LL	kA	LL
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Smažený	smažený	k2eAgInSc1d1	smažený
řízek	řízek	k1gInSc1	řízek
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
H.A.	H.A.	k1gFnSc6	H.A.
<g/>
LL	LL	kA	LL
Revival	revival	k1gInSc1	revival
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Echolama	Echolamum	k1gNnSc2	Echolamum
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Hanička	Hanička	k1gFnSc1	Hanička
písnička	písnička	k1gFnSc1	písnička
-	-	kIx~	-
H.	H.	kA	H.
Zagorová	Zagorová	k1gFnSc1	Zagorová
Revival	revival	k1gInSc1	revival
Band	band	k1gInSc1	band
(	(	kIx(	(
<g/>
od	od	k7c2	od
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Meky	Meka	k1gFnSc2	Meka
band	band	k1gInSc1	band
-	-	kIx~	-
M.	M.	kA	M.
Žbirka	Žbirka	k1gFnSc1	Žbirka
Revival	revival	k1gInSc1	revival
Band	band	k1gInSc1	band
(	(	kIx(	(
<g/>
od	od	k7c2	od
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Původní	původní	k2eAgMnSc1d1	původní
Bureš	Bureš	k1gMnSc1	Bureš
(	(	kIx(	(
<g/>
příležitostně	příležitostně	k6eAd1	příležitostně
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Traband	Trabanda	k1gFnPc2	Trabanda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stránky	stránka	k1gFnSc2	stránka
kapely	kapela	k1gFnSc2	kapela
Rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
Jardou	Jarda	k1gMnSc7	Jarda
Svobodou	Svoboda	k1gMnSc7	Svoboda
</s>
