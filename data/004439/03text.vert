<s>
Slezské	slezský	k2eAgFnPc4d1	Slezská
písně	píseň	k1gFnPc4	píseň
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
jediné	jediný	k2eAgFnSc2d1	jediná
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
českého	český	k2eAgMnSc2d1	český
básníka	básník	k1gMnSc2	básník
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Vaška	Vašek	k1gMnSc2	Vašek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novější	nový	k2eAgFnSc6d2	novější
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
Bezručovo	Bezručův	k2eAgNnSc1d1	Bezručovo
autorství	autorství	k1gNnSc1	autorství
zpochybňováno	zpochybňován	k2eAgNnSc1d1	zpochybňováno
<g/>
.	.	kIx.	.
</s>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
téměř	téměř	k6eAd1	téměř
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
-	-	kIx~	-
od	od	k7c2	od
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
autorovy	autorův	k2eAgFnSc2d1	autorova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Literárněvědná	literárněvědný	k2eAgFnSc1d1	literárněvědná
konference	konference	k1gFnSc1	konference
o	o	k7c6	o
Slezských	slezský	k2eAgFnPc6d1	Slezská
písních	píseň	k1gFnPc6	píseň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vzít	vzít	k5eAaPmF	vzít
za	za	k7c4	za
čtenářský	čtenářský	k2eAgInSc4d1	čtenářský
základ	základ	k1gInSc4	základ
vydání	vydání	k1gNnSc2	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
<s>
Nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
poslední	poslední	k2eAgFnSc4d1	poslední
literárněvědnou	literárněvědný	k2eAgFnSc4d1	literárněvědná
studii	studie	k1gFnSc4	studie
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Slezské	slezský	k2eAgFnPc1d1	Slezská
písně	píseň	k1gFnPc1	píseň
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
jádro	jádro	k1gNnSc1	jádro
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
básně	báseň	k1gFnPc1	báseň
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
bylo	být	k5eAaImAgNnS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
54	[number]	k4	54
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
básně	báseň	k1gFnPc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
vznikalo	vznikat	k5eAaImAgNnS	vznikat
jádro	jádro	k1gNnSc1	jádro
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nepodařilo	podařit	k5eNaPmAgNnS	podařit
spolehlivě	spolehlivě	k6eAd1	spolehlivě
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
teorie	teorie	k1gFnPc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
místecké	místecký	k2eAgFnPc1d1	Místecká
teorie	teorie	k1gFnPc1	teorie
<g/>
"	"	kIx"	"
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
některé	některý	k3yIgFnPc1	některý
básně	báseň	k1gFnPc1	báseň
již	již	k6eAd1	již
za	za	k7c2	za
pobytu	pobyt	k1gInSc2	pobyt
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Vaška	Vašek	k1gMnSc2	Vašek
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
a	a	k8xC	a
sbírka	sbírka	k1gFnSc1	sbírka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pozvolně	pozvolně	k6eAd1	pozvolně
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
teorie	teorie	k1gFnSc1	teorie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
básně	báseň	k1gFnPc1	báseň
"	"	kIx"	"
<g/>
jádra	jádro	k1gNnPc1	jádro
<g/>
"	"	kIx"	"
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
odesláním	odeslání	k1gNnSc7	odeslání
básní	básnit	k5eAaImIp3nS	básnit
Janu	Jana	k1gFnSc4	Jana
Herbenvi	Herbenev	k1gFnSc3	Herbenev
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
redaktoru	redaktor	k1gMnSc3	redaktor
týdeníku	týdeník	k1gInSc6	týdeník
Čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
básně	báseň	k1gFnSc2	báseň
vytryskly	vytrysknout	k5eAaPmAgInP	vytrysknout
naráz	naráz	k6eAd1	naráz
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
jakémsi	jakýsi	k3yIgInSc6	jakýsi
"	"	kIx"	"
<g/>
duševním	duševní	k2eAgInSc6d1	duševní
a	a	k8xC	a
uměleckém	umělecký	k2eAgNnSc6d1	umělecké
vytržení	vytržení	k1gNnSc6	vytržení
<g/>
"	"	kIx"	"
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
u	u	k7c2	u
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Vaška	Vašek	k1gMnSc2	Vašek
objevilo	objevit	k5eAaPmAgNnS	objevit
chrlení	chrlení	k1gNnSc1	chrlení
krve	krev	k1gFnSc2	krev
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
(	(	kIx(	(
<g/>
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
básních	báseň	k1gFnPc6	báseň
několik	několik	k4yIc4	několik
narážek	narážka	k1gFnPc2	narážka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Kdo	kdo	k3yRnSc1	kdo
na	na	k7c4	na
moje	můj	k3xOp1gNnSc4	můj
místo	místo	k1gNnSc4	místo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
variantu	varianta	k1gFnSc4	varianta
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
zastává	zastávat	k5eAaImIp3nS	zastávat
ji	on	k3xPp3gFnSc4	on
též	též	k9	též
většina	většina	k1gFnSc1	většina
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
část	část	k1gFnSc1	část
literárních	literární	k2eAgMnPc2d1	literární
vědců	vědec	k1gMnPc2	vědec
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
..	..	k?	..
Sám	sám	k3xTgMnSc1	sám
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
nijak	nijak	k6eAd1	nijak
neusnadňoval	usnadňovat	k5eNaImAgMnS	usnadňovat
poznání	poznání	k1gNnSc4	poznání
těchto	tento	k3xDgInPc2	tento
faktů	fakt	k1gInPc2	fakt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gNnSc4	jeho
vyjádření	vyjádření	k1gNnSc4	vyjádření
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
i	i	k9	i
mezi	mezi	k7c7	mezi
nejbližšími	blízký	k2eAgMnPc7d3	nejbližší
se	se	k3xPyFc4	se
měnilo	měnit	k5eAaImAgNnS	měnit
a	a	k8xC	a
často	často	k6eAd1	často
si	se	k3xPyFc3	se
protiřečilo	protiřečit	k5eAaImAgNnS	protiřečit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1899	[number]	k4	1899
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1904	[number]	k4	1904
zaslal	zaslat	k5eAaPmAgMnS	zaslat
v	v	k7c6	v
39	[number]	k4	39
zásilkách	zásilka	k1gFnPc6	zásilka
Janu	Jan	k1gMnSc3	Jan
Herbenovi	Herben	k1gMnSc3	Herben
<g/>
,	,	kIx,	,
šéfredaktorovi	šéfredaktor	k1gMnSc3	šéfredaktor
Času	čas	k1gInSc2	čas
74	[number]	k4	74
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
básní	báseň	k1gFnPc2	báseň
z	z	k7c2	z
první	první	k4xOgFnSc2	první
zásilky	zásilka	k1gFnSc2	zásilka
se	se	k3xPyFc4	se
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
báseň	báseň	k1gFnSc4	báseň
Hořící	hořící	k2eAgInSc1d1	hořící
keř	keř	k1gInSc1	keř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zásilka	zásilka	k1gFnSc1	zásilka
16	[number]	k4	16
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
-	-	kIx~	-
Den	den	k1gInSc1	den
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
obrazem	obraz	k1gInSc7	obraz
Prokopovým	Prokopův	k2eAgInSc7d1	Prokopův
<g/>
,	,	kIx,	,
Domaslovice	Domaslovice	k1gFnSc1	Domaslovice
<g/>
,	,	kIx,	,
Klec	klec	k1gFnSc1	klec
<g/>
,	,	kIx,	,
Jen	jen	k6eAd1	jen
jedenkrát	jedenkrát	k6eAd1	jedenkrát
Druhá	druhý	k4xOgFnSc1	druhý
zásilka	zásilka	k1gFnSc1	zásilka
25	[number]	k4	25
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
-	-	kIx~	-
Škaredý	škaredý	k2eAgInSc1d1	škaredý
zjev	zjev	k1gInSc1	zjev
Pátá	pátá	k1gFnSc1	pátá
zásilka	zásilka	k1gFnSc1	zásilka
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
-	-	kIx~	-
Setkání	setkání	k1gNnSc1	setkání
<g/>
,	,	kIx,	,
Michálkovice	Michálkovice	k1gFnPc1	Michálkovice
<g/>
,	,	kIx,	,
Kdo	kdo	k3yRnSc1	kdo
na	na	k7c4	na
moje	můj	k3xOp1gNnSc4	můj
místo	místo	k1gNnSc4	místo
<g/>
?	?	kIx.	?
</s>
<s>
Šestá	šestý	k4xOgFnSc1	šestý
zásilka	zásilka	k1gFnSc1	zásilka
1	[number]	k4	1
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
-	-	kIx~	-
Leonidas	Leonidasa	k1gFnPc2	Leonidasa
<g/>
,	,	kIx,	,
Hrabyň	Hrabyně	k1gFnPc2	Hrabyně
<g/>
,	,	kIx,	,
Blendovice	Blendovice	k1gFnSc1	Blendovice
<g/>
,	,	kIx,	,
Bernard	Bernard	k1gMnSc1	Bernard
Žár	žár	k1gInSc1	žár
<g/>
,	,	kIx,	,
Národní	národní	k2eAgFnSc1d1	národní
maškaráda	maškaráda	k1gFnSc1	maškaráda
<g/>
,	,	kIx,	,
Mistr	mistr	k1gMnSc1	mistr
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Třetí	třetí	k4xOgMnSc1	třetí
patron	patron	k1gMnSc1	patron
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
Kráska	kráska	k1gFnSc1	kráska
Sedmá	sedmý	k4xOgFnSc1	sedmý
zásilka	zásilka	k1gFnSc1	zásilka
24	[number]	k4	24
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
-	-	kIx~	-
Maryčka	Maryčka	k1gFnSc1	Maryčka
Magdonova	Magdonův	k2eAgFnSc1d1	Magdonova
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
<g/>
,	,	kIx,	,
Tošonovice	Tošonovice	k1gFnSc1	Tošonovice
<g/>
,	,	kIx,	,
Z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
do	do	k7c2	do
Těšína	Těšín	k1gInSc2	Těšín
<g/>
,	,	kIx,	,
70.000	[number]	k4	70.000
<g/>
,	,	kIx,	,
Markýz	markýz	k1gMnSc1	markýz
Géro	Géro	k1gMnSc1	Géro
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Hučín	Hučín	k1gMnSc1	Hučín
<g/>
,	,	kIx,	,
Jedna	jeden	k4xCgFnSc1	jeden
melodie	melodie	k1gFnSc2	melodie
Jedenáctá	jedenáctý	k4xOgFnSc1	jedenáctý
zásilka	zásilka	k1gFnSc1	zásilka
28	[number]	k4	28
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
-	-	kIx~	-
Rektor	rektor	k1gMnSc1	rektor
Halfar	Halfar	k1gMnSc1	Halfar
<g/>
,	,	kIx,	,
Dva	dva	k4xCgMnPc1	dva
hrobníci	hrobník	k1gMnPc1	hrobník
<g/>
,	,	kIx,	,
Motýl	motýl	k1gMnSc1	motýl
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Koniklec	koniklec	k1gInSc4	koniklec
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc4d1	červený
květ	květ	k1gInSc4	květ
Dvanáctá	dvanáctý	k4xOgFnSc1	dvanáctý
zásilka	zásilka	k1gFnSc1	zásilka
12	[number]	k4	12
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1899	[number]	k4	1899
-	-	kIx~	-
Dědina	dědina	k1gFnSc1	dědina
nad	nad	k7c7	nad
Ostravicí	Ostravice	k1gFnSc7	Ostravice
<g/>
,	,	kIx,	,
Dombrová	Dombrová	k1gFnSc1	Dombrová
<g/>
,	,	kIx,	,
Opava	Opava	k1gFnSc1	Opava
Třináctá	třináctý	k4xOgFnSc1	třináctý
zásilka	zásilka	k1gFnSc1	zásilka
počátek	počátek	k1gInSc1	počátek
června	červen	k1gInSc2	červen
1899	[number]	k4	1899
-	-	kIx~	-
Sviadnov	Sviadnovo	k1gNnPc2	Sviadnovo
<g/>
,	,	kIx,	,
Par	para	k1gFnPc2	para
nobile	nobile	k1gMnSc1	nobile
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
caput	caput	k1gMnSc1	caput
regni	regeň	k1gFnSc6	regeň
Sedmnáctá	sedmnáctý	k4xOgFnSc1	sedmnáctý
zásilka	zásilka	k1gFnSc1	zásilka
15	[number]	k4	15
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Orlová	Orlová	k1gFnSc1	Orlová
<g/>
,	,	kIx,	,
Ostravice	Ostravice	k1gFnSc1	Ostravice
<g/>
,	,	kIx,	,
Hořící	hořící	k2eAgInSc1d1	hořící
keř	keř	k1gInSc1	keř
Dvacátá	dvacátý	k4xOgFnSc1	dvacátý
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
zásilka	zásilka	k1gFnSc1	zásilka
4	[number]	k4	4
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1900	[number]	k4	1900
-	-	kIx~	-
Čtenáři	čtenář	k1gMnPc1	čtenář
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
Kovkop	kovkop	k1gMnSc1	kovkop
<g/>
,	,	kIx,	,
Pětvald	Pětvald	k1gMnSc1	Pětvald
I	i	k9	i
<g/>
,	,	kIx,	,
Pětvald	Pětvald	k1gMnSc1	Pětvald
II	II	kA	II
<g/>
,	,	kIx,	,
Křižák	křižák	k1gInSc4	křižák
z	z	k7c2	z
Modré	modrý	k2eAgFnSc2d1	modrá
<g/>
,	,	kIx,	,
Dombrová	Dombrový	k2eAgNnPc1d1	Dombrový
I	i	k9	i
<g/>
,	,	kIx,	,
Dombrová	Dombrová	k1gFnSc1	Dombrová
II	II	kA	II
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Lazy	Lazy	k?	Lazy
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgFnPc1	dva
dědiny	dědina	k1gFnPc1	dědina
<g/>
,	,	kIx,	,
Ondráš	Ondráš	k1gFnSc1	Ondráš
<g/>
,	,	kIx,	,
Ligotka	ligotka	k1gFnSc1	ligotka
Kameralna	Kameralna	k1gFnSc1	Kameralna
<g/>
,	,	kIx,	,
Pole	pole	k1gFnSc1	pole
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
Střebovský	Střebovský	k2eAgInSc1d1	Střebovský
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
Uhni	uhnout	k5eAaPmRp2nS	uhnout
mi	já	k3xPp1nSc3	já
z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
Oni	onen	k3xDgMnPc1	onen
a	a	k8xC	a
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
Osud	osud	k1gInSc1	osud
<g/>
,	,	kIx,	,
Vrbice	vrbice	k1gFnPc1	vrbice
<g/>
,	,	kIx,	,
Slezské	slezský	k2eAgInPc1d1	slezský
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
Hanácká	hanácký	k2eAgFnSc1d1	Hanácká
ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
Plumlov	Plumlov	k1gInSc1	Plumlov
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ptení	Ptení	k1gNnPc1	Ptení
<g/>
,	,	kIx,	,
Kyjov	Kyjov	k1gInSc1	Kyjov
<g/>
,	,	kIx,	,
Paskovské	paskovský	k2eAgInPc1d1	paskovský
rybníky	rybník	k1gInPc1	rybník
<g/>
,	,	kIx,	,
Úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
Chycený	chycený	k2eAgMnSc1d1	chycený
drozd	drozd	k1gMnSc1	drozd
Třicátá	třicátý	k4xOgFnSc1	třicátý
druhá	druhý	k4xOgFnSc1	druhý
26	[number]	k4	26
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1902	[number]	k4	1902
-	-	kIx~	-
Didus	Didus	k1gInSc1	Didus
ineptus	ineptus	k1gInSc1	ineptus
36	[number]	k4	36
<g/>
.	.	kIx.	.
zásilka	zásilka	k1gFnSc1	zásilka
1	[number]	k4	1
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1903	[number]	k4	1903
-	-	kIx~	-
Smrt	smrt	k1gFnSc1	smrt
césarova	césarův	k2eAgFnSc1d1	Césarova
<g/>
,	,	kIx,	,
Krásné	krásný	k2eAgNnSc1d1	krásné
Pole	pole	k1gNnSc1	pole
38	[number]	k4	38
<g/>
.	.	kIx.	.
zásilka	zásilka	k1gFnSc1	zásilka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
39	[number]	k4	39
<g/>
.	.	kIx.	.
zásilka	zásilka	k1gFnSc1	zásilka
17	[number]	k4	17
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1904	[number]	k4	1904
Labutinka	labutinka	k1gFnSc1	labutinka
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
Papírový	papírový	k2eAgInSc1d1	papírový
Mojšl	Mojšl	k1gInSc1	Mojšl
1864-1904	[number]	k4	1864-1904
70.000	[number]	k4	70.000
Bernard	Bernard	k1gMnSc1	Bernard
Žár	žár	k1gInSc1	žár
<g/>
;	;	kIx,	;
Blendovice	Blendovice	k1gFnSc1	Blendovice
Červený	Červený	k1gMnSc1	Červený
květ	květ	k1gInSc1	květ
<g/>
;	;	kIx,	;
Čtenáři	čtenář	k1gMnPc1	čtenář
veršů	verš	k1gInPc2	verš
Dědina	dědina	k1gFnSc1	dědina
nad	nad	k7c7	nad
Ostravicí	Ostravice	k1gFnSc7	Ostravice
<g/>
;	;	kIx,	;
Den	den	k1gInSc1	den
<g />
.	.	kIx.	.
</s>
<s>
Palackého	Palackého	k2eAgMnSc1d1	Palackého
<g/>
;	;	kIx,	;
Didus	Didus	k1gMnSc1	Didus
ineptus	ineptus	k1gMnSc1	ineptus
<g/>
;	;	kIx,	;
Dombrová	Dombrová	k1gFnSc1	Dombrová
I	i	k9	i
<g/>
;	;	kIx,	;
Dombrová	Dombrový	k2eAgFnSc1d1	Dombrový
II	II	kA	II
<g/>
;	;	kIx,	;
Dva	dva	k4xCgMnPc1	dva
hrobníci	hrobník	k1gMnPc1	hrobník
<g/>
;	;	kIx,	;
Dvě	dva	k4xCgFnPc1	dva
dědiny	dědina	k1gFnPc1	dědina
<g/>
;	;	kIx,	;
Dvě	dva	k4xCgFnPc1	dva
mohyly	mohyla	k1gFnSc2	mohyla
Hanácká	hanácký	k2eAgFnSc1d1	Hanácká
ves	ves	k1gFnSc1	ves
<g/>
;	;	kIx,	;
Hanys	Hanys	k1gMnSc1	Hanys
Horehleď	Horehleď	k1gMnSc1	Horehleď
<g/>
;	;	kIx,	;
Hölderlin	Hölderlin	k1gInSc1	Hölderlin
nad	nad	k7c7	nad
Neckarem	Neckar	k1gInSc7	Neckar
<g/>
;	;	kIx,	;
Hrabyň	Hrabyně	k1gFnPc2	Hrabyně
<g/>
;	;	kIx,	;
Hučín	Hučín	k1gMnSc1	Hučín
Chycený	chycený	k2eAgMnSc1d1	chycený
drozd	drozd	k1gMnSc1	drozd
Idyla	idyla	k1gFnSc1	idyla
ve	v	k7c6	v
mlýně	mlýn	k1gInSc6	mlýn
Já	já	k3xPp1nSc1	já
<g/>
;	;	kIx,	;
Jedna	jeden	k4xCgFnSc1	jeden
melodie	melodie	k1gFnSc1	melodie
Kalina	kalina	k1gFnSc1	kalina
I	i	k9	i
<g/>
;	;	kIx,	;
Kalina	Kalina	k1gMnSc1	Kalina
II	II	kA	II
<g/>
;	;	kIx,	;
Kalina	Kalina	k1gMnSc1	Kalina
III	III	kA	III
<g/>
;	;	kIx,	;
Kantor	Kantor	k1gMnSc1	Kantor
Halfar	Halfar	k1gMnSc1	Halfar
<g/>
;	;	kIx,	;
Kaštany	kaštan	k1gInPc1	kaštan
<g/>
;	;	kIx,	;
Kdo	kdo	k3yQnSc1	kdo
na	na	k7c4	na
moje	můj	k3xOp1gNnSc4	můj
místo	místo	k1gNnSc4	místo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
;	;	kIx,	;
Koniklec	koniklec	k1gInSc4	koniklec
<g/>
;	;	kIx,	;
Kovkop	kovkop	k1gMnSc1	kovkop
<g/>
;	;	kIx,	;
Krásné	krásný	k2eAgNnSc1d1	krásné
Pole	pole	k1gNnSc1	pole
<g/>
;	;	kIx,	;
Kyjov	Kyjov	k1gInSc1	Kyjov
Labutinka	labutinka	k1gFnSc1	labutinka
<g/>
;	;	kIx,	;
Lazy	Lazy	k?	Lazy
<g/>
;	;	kIx,	;
Leonidas	Leonidas	k1gMnSc1	Leonidas
<g/>
;	;	kIx,	;
Ligotka	ligotka	k1gFnSc1	ligotka
Kameralna	Kameralna	k1gFnSc1	Kameralna
Markýz	markýz	k1gMnSc1	markýz
Gero	Gero	k1gMnSc1	Gero
<g/>
;	;	kIx,	;
Maryčka	Maryčka	k1gFnSc1	Maryčka
Magdonova	Magdonův	k2eAgFnSc1d1	Magdonova
<g/>
;	;	kIx,	;
Maškarní	maškarní	k2eAgFnSc1d1	maškarní
ples	ples	k1gInSc1	ples
<g/>
;	;	kIx,	;
Michalkovice	Michalkovice	k1gFnSc1	Michalkovice
<g/>
;	;	kIx,	;
Mohelnice	Mohelnice	k1gFnSc1	Mohelnice
<g/>
;	;	kIx,	;
Motýl	motýl	k1gMnSc1	motýl
Nápis	nápis	k1gInSc4	nápis
na	na	k7c4	na
hrob	hrob	k1gInSc4	hrob
bojovníkův	bojovníkův	k2eAgInSc4d1	bojovníkův
<g/>
;	;	kIx,	;
Návrat	návrat	k1gInSc1	návrat
<g />
.	.	kIx.	.
</s>
<s>
Ondráš	Ondrat	k5eAaImIp2nS	Ondrat
<g/>
;	;	kIx,	;
Oni	onen	k3xDgMnPc1	onen
a	a	k8xC	a
my	my	k3xPp1nPc1	my
<g/>
;	;	kIx,	;
Opava	Opava	k1gFnSc1	Opava
<g/>
;	;	kIx,	;
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
;	;	kIx,	;
Osud	osud	k1gInSc1	osud
Papírový	papírový	k2eAgInSc1d1	papírový
Mojšl	Mojšl	k1gInSc4	Mojšl
<g/>
;	;	kIx,	;
Par	para	k1gFnPc2	para
nobile	nobile	k1gMnSc1	nobile
<g/>
;	;	kIx,	;
Pětvald	Pětvald	k1gMnSc1	Pětvald
I	i	k9	i
<g/>
;	;	kIx,	;
Pětvald	Pětvald	k1gInSc1	Pětvald
II	II	kA	II
<g/>
;	;	kIx,	;
Pluh	pluh	k1gInSc1	pluh
<g/>
;	;	kIx,	;
Plumlov	Plumlov	k1gInSc1	Plumlov
I	i	k9	i
<g/>
;	;	kIx,	;
Plumlov	Plumlov	k1gInSc1	Plumlov
II	II	kA	II
<g/>
;	;	kIx,	;
Pole	pole	k1gFnSc1	pole
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
<g/>
;	;	kIx,	;
Polská	polský	k2eAgFnSc1d1	polská
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
<g />
.	.	kIx.	.
</s>
<s>
;	;	kIx,	;
Praga	Praga	k1gFnSc1	Praga
caput	caput	k1gInSc4	caput
regni	regn	k1gMnPc1	regn
<g/>
;	;	kIx,	;
Ptení	Ptení	k2eAgInPc1d1	Ptení
Rybníky	rybník	k1gInPc1	rybník
za	za	k7c7	za
Paskovem	Paskov	k1gInSc7	Paskov
Sedm	sedm	k4xCc4	sedm
Havranů	Havran	k1gMnPc2	Havran
<g/>
;	;	kIx,	;
Setkání	setkání	k1gNnSc1	setkání
<g/>
;	;	kIx,	;
Slezské	slezský	k2eAgInPc1d1	slezský
lesy	les	k1gInPc1	les
<g/>
;	;	kIx,	;
Smrt	smrt	k1gFnSc1	smrt
césarova	césarův	k2eAgFnSc1d1	Césarova
<g/>
;	;	kIx,	;
Starček	starček	k1gInSc1	starček
<g/>
;	;	kIx,	;
Střebovský	Střebovský	k2eAgInSc1d1	Střebovský
mlýn	mlýn	k1gInSc1	mlýn
<g/>
;	;	kIx,	;
Sviadnov	Sviadnov	k1gInSc1	Sviadnov
I	i	k9	i
<g/>
;	;	kIx,	;
Sviadnov	Sviadnov	k1gInSc1	Sviadnov
II	II	kA	II
Škaredý	škaredý	k2eAgInSc1d1	škaredý
zjev	zjev	k1gInSc1	zjev
Tošonovice	Tošonovice	k1gFnSc2	Tošonovice
<g/>
;	;	kIx,	;
Ty	ty	k3xPp2nSc1	ty
a	a	k8xC	a
já	já	k3xPp1nSc1	já
Úspěch	úspěch	k1gInSc1	úspěch
Valčice	Valčice	k1gFnSc1	Valčice
<g/>
;	;	kIx,	;
Vrbice	vrbice	k1gFnSc1	vrbice
Z	z	k7c2	z
Ostravy	Ostrava	k1gFnSc2	Ostrava
do	do	k7c2	do
Těšína	Těšín	k1gInSc2	Těšín
<g/>
;	;	kIx,	;
Zem	zem	k1gFnSc4	zem
pod	pod	k7c7	pod
horami	hora	k1gFnPc7	hora
Žermanice	Žermanice	k1gInPc4	Žermanice
<g/>
;	;	kIx,	;
Žně	žeň	k1gFnPc4	žeň
Autorství	autorství	k1gNnSc2	autorství
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gInSc2	Bezruč
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
novější	nový	k2eAgFnSc6d2	novější
době	doba	k1gFnSc6	doba
zpochybňováno	zpochybňován	k2eAgNnSc1d1	zpochybňováno
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
-	-	kIx~	-
zejména	zejména	k9	zejména
Drahomír	Drahomír	k1gMnSc1	Drahomír
Šajtar	Šajtar	k1gMnSc1	Šajtar
-	-	kIx~	-
tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
nesdíleli	sdílet	k5eNaImAgMnP	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Doc.	doc.	kA	doc.
Jiří	Jiří	k1gMnSc1	Jiří
Urbanec	Urbanec	k1gMnSc1	Urbanec
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Ondřej	Ondřej	k1gMnSc1	Ondřej
Boleslav	Boleslav	k1gMnSc1	Boleslav
Petr	Petr	k1gMnSc1	Petr
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
autorem	autor	k1gMnSc7	autor
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
však	však	k9	však
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Vaška	Vašek	k1gMnSc4	Vašek
byl	být	k5eAaImAgInS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
uvedl	uvést	k5eAaPmAgMnS	uvést
jako	jako	k9	jako
autora	autor	k1gMnSc4	autor
výhradně	výhradně	k6eAd1	výhradně
Petra	Petra	k1gFnSc1	Petra
Bezruče	Bezruč	k1gFnSc2	Bezruč
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pochybnostem	pochybnost	k1gFnPc3	pochybnost
ohledně	ohledně	k7c2	ohledně
autorství	autorství	k1gNnSc2	autorství
přispělo	přispět	k5eAaPmAgNnS	přispět
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Skrývání	skrývání	k1gNnSc3	skrývání
identity	identita	k1gFnSc2	identita
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
<g/>
.	.	kIx.	.
</s>
<s>
Skrývání	skrývání	k1gNnSc1	skrývání
se	se	k3xPyFc4	se
před	před	k7c7	před
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
,	,	kIx,	,
také	také	k9	také
záměrné	záměrný	k2eAgNnSc4d1	záměrné
Vaškovo	Vaškův	k2eAgNnSc4d1	Vaškovo
překrucování	překrucování	k1gNnSc4	překrucování
a	a	k8xC	a
mlžení	mlžení	k1gNnSc4	mlžení
o	o	k7c6	o
vlastní	vlastní	k2eAgFnSc6d1	vlastní
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Pochyby	pochyba	k1gFnPc1	pochyba
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
literárních	literární	k2eAgMnPc2d1	literární
badatelů	badatel	k1gMnPc2	badatel
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Joži	Joža	k1gFnSc2	Joža
Vochaly	Vochal	k1gMnPc4	Vochal
a	a	k8xC	a
především	především	k9	především
Jana	Jan	k1gMnSc4	Jan
Drozda	Drozd	k1gMnSc4	Drozd
<g/>
.	.	kIx.	.
</s>
<s>
Ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
vztah	vztah	k1gInSc1	vztah
básníka	básník	k1gMnSc2	básník
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
dílu	dílo	k1gNnSc3	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
přehlížel	přehlížet	k5eAaImAgMnS	přehlížet
mnoho	mnoho	k4c4	mnoho
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
redaktorských	redaktorský	k2eAgFnPc2d1	redaktorská
úprav	úprava	k1gFnPc2	úprava
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
zásadních	zásadní	k2eAgFnPc6d1	zásadní
básních	báseň	k1gFnPc6	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Podstatné	podstatný	k2eAgFnPc1d1	podstatná
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
básních	báseň	k1gFnPc6	báseň
nechával	nechávat	k5eAaImAgInS	nechávat
bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
velmi	velmi	k6eAd1	velmi
rád	rád	k6eAd1	rád
ručně	ručně	k6eAd1	ručně
"	"	kIx"	"
<g/>
opravoval	opravovat	k5eAaImAgMnS	opravovat
<g/>
"	"	kIx"	"
již	již	k9	již
vytištěné	vytištěný	k2eAgFnPc1d1	vytištěná
Slezské	slezský	k2eAgFnPc1d1	Slezská
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
ke	k	k7c3	k
škodě	škoda	k1gFnSc3	škoda
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jediné	jediný	k2eAgNnSc1d1	jediné
výrazné	výrazný	k2eAgNnSc1d1	výrazné
básnické	básnický	k2eAgNnSc1d1	básnické
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgMnPc4	který
již	již	k6eAd1	již
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
plnohodnotně	plnohodnotně	k6eAd1	plnohodnotně
navázat	navázat	k5eAaPmF	navázat
<g/>
.	.	kIx.	.
</s>
<s>
Slezské	slezský	k2eAgFnPc1d1	Slezská
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
nejprve	nejprve	k6eAd1	nejprve
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Slezské	slezský	k2eAgNnSc1d1	Slezské
číslo	číslo	k1gNnSc1	číslo
literární	literární	k2eAgFnSc2d1	literární
přílohy	příloha	k1gFnSc2	příloha
časopisu	časopis	k1gInSc2	časopis
Čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
cenzurovány	cenzurovat	k5eAaImNgInP	cenzurovat
c.	c.	k?	c.
k.	k.	k?	k.
úřady	úřad	k1gInPc4	úřad
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
neustále	neustále	k6eAd1	neustále
rozšiřovány	rozšiřovat	k5eAaImNgInP	rozšiřovat
autorem	autor	k1gMnSc7	autor
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
básně	báseň	k1gFnPc4	báseň
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
téměř	téměř	k6eAd1	téměř
půlstoletí	půlstoletí	k1gNnSc1	půlstoletí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
jím	on	k3xPp3gMnSc7	on
nebyly	být	k5eNaImAgInP	být
kodifikovány	kodifikovat	k5eAaBmNgInP	kodifikovat
do	do	k7c2	do
"	"	kIx"	"
<g/>
závazné	závazný	k2eAgFnSc2d1	závazná
<g/>
"	"	kIx"	"
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
úpravy	úprava	k1gFnPc1	úprava
a	a	k8xC	a
dodatečné	dodatečný	k2eAgFnPc1d1	dodatečná
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
básních	báseň	k1gFnPc6	báseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Vašek	Vašek	k1gMnSc1	Vašek
rád	rád	k6eAd1	rád
prováděl	provádět	k5eAaImAgMnS	provádět
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
básním	báseň	k1gFnPc3	báseň
na	na	k7c4	na
škodu	škoda	k1gFnSc4	škoda
a	a	k8xC	a
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
opraveny	opraven	k2eAgFnPc4d1	opravena
literárními	literární	k2eAgMnPc7d1	literární
vědci	vědec	k1gMnPc7	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jeho	jeho	k3xOp3gNnPc2	jeho
"	"	kIx"	"
<g/>
jednorázové	jednorázový	k2eAgNnSc1d1	jednorázové
<g/>
"	"	kIx"	"
vystoupení	vystoupení	k1gNnSc1	vystoupení
proti	proti	k7c3	proti
sociálnímu	sociální	k2eAgMnSc3d1	sociální
a	a	k8xC	a
národnostnímu	národnostní	k2eAgInSc3d1	národnostní
útlaku	útlak	k1gInSc3	útlak
slezského	slezský	k2eAgInSc2d1	slezský
lidu	lid	k1gInSc2	lid
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
silně	silně	k6eAd1	silně
zapůsobilo	zapůsobit	k5eAaPmAgNnS	zapůsobit
během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1891	[number]	k4	1891
<g/>
-	-	kIx~	-
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podivné	podivný	k2eAgNnSc1d1	podivné
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
silně	silně	k6eAd1	silně
nevystoupil	vystoupit	k5eNaPmAgMnS	vystoupit
za	za	k7c2	za
česko-polského	českoolský	k2eAgInSc2d1	česko-polský
konfliktu	konflikt	k1gInSc2	konflikt
na	na	k7c6	na
Těšínsku	Těšínsko	k1gNnSc6	Těšínsko
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
nebo	nebo	k8xC	nebo
za	za	k7c4	za
německé	německý	k2eAgFnPc4d1	německá
okupace	okupace	k1gFnPc4	okupace
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitativní	kvalitativní	k2eAgInPc4d1	kvalitativní
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
melodii	melodie	k1gFnSc6	melodie
i	i	k8xC	i
metru	metro	k1gNnSc6	metro
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
básní	báseň	k1gFnPc2	báseň
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
jsou	být	k5eAaImIp3nP	být
místy	místy	k6eAd1	místy
tak	tak	k6eAd1	tak
nepochopitelné	pochopitelný	k2eNgNnSc1d1	nepochopitelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
zastánci	zastánce	k1gMnPc1	zastánce
Vaškova	Vaškův	k2eAgNnSc2d1	Vaškovo
autorství	autorství	k1gNnSc2	autorství
je	být	k5eAaImIp3nS	být
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
jádro	jádro	k1gNnSc1	jádro
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
básně	báseň	k1gFnPc1	báseň
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
byl	být	k5eAaImAgMnS	být
uzavřený	uzavřený	k2eAgMnSc1d1	uzavřený
<g/>
,	,	kIx,	,
nemluvný	mluvný	k2eNgMnSc1d1	nemluvný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jeho	jeho	k3xOp3gFnPc1	jeho
časté	častý	k2eAgFnPc1d1	častá
túry	túra	k1gFnPc1	túra
(	(	kIx(	(
<g/>
výplazy	výplaz	k1gInPc1	výplaz
<g/>
)	)	kIx)	)
po	po	k7c6	po
Beskydech	Beskyd	k1gInPc6	Beskyd
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Místku	místko	k1gNnSc6	místko
se	se	k3xPyFc4	se
podobaly	podobat	k5eAaImAgFnP	podobat
spíše	spíše	k9	spíše
chodeckým	chodecký	k2eAgInPc3d1	chodecký
závodům	závod	k1gInPc3	závod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
konal	konat	k5eAaImAgInS	konat
zpočátku	zpočátku	k6eAd1	zpočátku
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
rady	rada	k1gFnPc4	rada
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mnoha	mnoho	k4c3	mnoho
lidem	člověk	k1gMnPc3	člověk
nepřipadá	připadat	k5eNaImIp3nS	připadat
reálné	reálný	k2eAgNnSc1d1	reálné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
poznal	poznat	k5eAaPmAgMnS	poznat
skutečně	skutečně	k6eAd1	skutečně
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
tíživou	tíživý	k2eAgFnSc4d1	tíživá
realitu	realita	k1gFnSc4	realita
Slezska	Slezsko	k1gNnSc2	Slezsko
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
během	během	k7c2	během
pouhých	pouhý	k2eAgNnPc2d1	pouhé
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
neměl	mít	k5eNaImAgMnS	mít
hudební	hudební	k2eAgInSc4d1	hudební
sluch	sluch	k1gInSc4	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
rád	rád	k6eAd1	rád
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
nerozuměl	rozumět	k5eNaImAgMnS	rozumět
jí	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
když	když	k8xS	když
si	se	k3xPyFc3	se
čtenáři	čtenář	k1gMnPc1	čtenář
uvědomí	uvědomit	k5eAaPmIp3nP	uvědomit
geniální	geniální	k2eAgFnSc4d1	geniální
melodii	melodie	k1gFnSc4	melodie
některých	některý	k3yIgFnPc2	některý
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Kdo	kdo	k3yRnSc1	kdo
na	na	k7c4	na
moje	můj	k3xOp1gNnSc4	můj
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
Návrat	návrat	k1gInSc1	návrat
apod.	apod.	kA	apod.
Básně	báseň	k1gFnPc4	báseň
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
Slezska	Slezsko	k1gNnSc2	Slezsko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mnoho	mnoho	k4c4	mnoho
dobových	dobový	k2eAgFnPc2d1	dobová
reálií	reálie	k1gFnPc2	reálie
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
nekorespondují	korespondovat	k5eNaImIp3nP	korespondovat
s	s	k7c7	s
Vaškovým	Vaškův	k2eAgNnSc7d1	Vaškovo
uváděným	uváděný	k2eAgNnSc7d1	uváděné
obdobím	období	k1gNnSc7	období
vzniku	vznik	k1gInSc2	vznik
Některé	některý	k3yIgFnPc1	některý
básně	báseň	k1gFnPc1	báseň
jako	jako	k9	jako
by	by	kYmCp3nP	by
popisovaly	popisovat	k5eAaImAgInP	popisovat
život	život	k1gInSc4	život
O.	O.	kA	O.
B.	B.	kA	B.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
kritické	kritický	k2eAgNnSc1d1	kritické
vydání	vydání	k1gNnSc1	vydání
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vydal	vydat	k5eAaPmAgInS	vydat
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc4	tento
vydání	vydání	k1gNnSc4	vydání
připravili	připravit	k5eAaPmAgMnP	připravit
pracovníci	pracovník	k1gMnPc1	pracovník
ÚČL	ÚČL	kA	ÚČL
Mgr.	Mgr.	kA	Mgr.
Jiří	Jiří	k1gMnSc1	Jiří
Flaišman	Flaišman	k1gMnSc1	Flaišman
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mgr.	Mgr.	kA	Mgr.
Michal	Michal	k1gMnSc1	Michal
Kosák	kosák	k1gMnSc1	kosák
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Za	za	k7c2	za
jediného	jediný	k2eAgMnSc2d1	jediný
autora	autor	k1gMnSc2	autor
sbírky	sbírka	k1gFnSc2	sbírka
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgMnS	označit
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vašek	Vašek	k1gMnSc1	Vašek
<g/>
.	.	kIx.	.
</s>
<s>
Spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slezské	slezský	k2eAgFnSc2d1	Slezská
písně	píseň	k1gFnSc2	píseň
napsal	napsat	k5eAaBmAgMnS	napsat
jiný	jiný	k2eAgMnSc1d1	jiný
autor	autor	k1gMnSc1	autor
nebo	nebo	k8xC	nebo
že	že	k8xS	že
spoluatorem	spoluator	k1gInSc7	spoluator
sbírky	sbírka	k1gFnSc2	sbírka
byl	být	k5eAaImAgInS	být
O.	O.	kA	O.
B.	B.	kA	B.
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
editoři	editor	k1gMnPc1	editor
za	za	k7c4	za
"	"	kIx"	"
<g/>
pusté	pustý	k2eAgFnPc4d1	pustá
a	a	k8xC	a
nepodložené	podložený	k2eNgFnPc4d1	nepodložená
hypotézy	hypotéza	k1gFnPc4	hypotéza
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tři	tři	k4xCgFnPc1	tři
básně	báseň	k1gFnPc1	báseň
ze	z	k7c2	z
Slezských	slezský	k2eAgFnPc2d1	Slezská
písní	píseň	k1gFnPc2	píseň
zhudebnil	zhudebnit	k5eAaPmAgInS	zhudebnit
jako	jako	k9	jako
mužské	mužský	k2eAgInPc4d1	mužský
sbory	sbor	k1gInPc4	sbor
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
básně	báseň	k1gFnPc4	báseň
Maryčka	Maryčka	k1gFnSc1	Maryčka
Magdónova	Magdónův	k2eAgFnSc1d1	Magdónova
<g/>
,	,	kIx,	,
Kantor	Kantor	k1gMnSc1	Kantor
Halfar	Halfar	k1gMnSc1	Halfar
a	a	k8xC	a
70000	[number]	k4	70000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1982-1983	[number]	k4	1982-1983
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
písničkář	písničkář	k1gMnSc1	písničkář
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
pásmo	pásmo	k1gNnSc4	pásmo
zhudebněné	zhudebněný	k2eAgFnSc2d1	zhudebněná
poezie	poezie	k1gFnSc2	poezie
Petra	Petr	k1gMnSc2	Petr
Bezruče	Bezruč	k1gFnSc2	Bezruč
Maryčka	Maryčka	k1gFnSc1	Maryčka
&	&	k?	&
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Slezské	slezský	k2eAgFnSc2d1	Slezská
písně	píseň	k1gFnSc2	píseň
Bezručovy	Bezručův	k2eAgFnSc2d1	Bezručova
a	a	k8xC	a
slezské	slezský	k2eAgFnSc2d1	Slezská
písně	píseň	k1gFnSc2	píseň
lidové	lidový	k2eAgFnSc2d1	lidová
zpívá	zpívat	k5eAaImIp3nS	zpívat
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nohavica	Nohavica	k1gMnSc1	Nohavica
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
čtyř	čtyři	k4xCgFnPc2	čtyři
vlastních	vlastní	k2eAgFnPc2d1	vlastní
či	či	k8xC	či
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
zhudebnil	zhudebnit	k5eAaPmAgInS	zhudebnit
osm	osm	k4xCc4	osm
Bezručových	Bezručův	k2eAgFnPc2d1	Bezručova
básní	báseň	k1gFnPc2	báseň
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
texty	text	k1gInPc1	text
byly	být	k5eAaImAgInP	být
upraveny	upravit	k5eAaPmNgInP	upravit
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
zčásti	zčásti	k6eAd1	zčásti
(	(	kIx(	(
<g/>
Červený	červený	k2eAgInSc1d1	červený
květ	květ	k1gInSc1	květ
<g/>
)	)	kIx)	)
či	či	k8xC	či
jen	jen	k9	jen
jejich	jejich	k3xOp3gInPc1	jejich
zlomky	zlomek	k1gInPc1	zlomek
(	(	kIx(	(
<g/>
Já	já	k3xPp1nSc1	já
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
BEZRUČ	BEZRUČ	kA	BEZRUČ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Slezské	slezský	k2eAgFnPc1d1	Slezská
písně	píseň	k1gFnPc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Kritická	kritický	k2eAgFnSc1d1	kritická
edice	edice	k1gFnSc1	edice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
literaturu	literatura	k1gFnSc4	literatura
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v.v.i.	v.v.i.	k?	v.v.i.
<g/>
:	:	kIx,	:
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
2014	[number]	k4	2014
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
©	©	k?	©
<g/>
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
178	[number]	k4	178
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
88069	[number]	k4	88069
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
