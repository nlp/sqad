<s>
Jason	Jason	k1gInSc1
Statham	Statham	k1gInSc1
</s>
<s>
Jason	Jason	k1gNnSc1
Statham	Statham	k1gInSc1
Jason	Jason	k1gMnSc1
Statham	Statham	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
Narození	narození	k1gNnPc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1967	#num#	k4
(	(	kIx(
<g/>
53	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Shirebrook	Shirebrook	k1gInSc1
<g/>
,	,	kIx,
Derbyshire	Derbyshir	k1gMnSc5
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc2
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
1998	#num#	k4
<g/>
–	–	k?
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Rosie	Rosi	k1gMnPc4
Huntington-Whiteley	Huntington-Whitelea	k1gFnSc2
Děti	dítě	k1gFnPc1
</s>
<s>
1	#num#	k4
Významné	významný	k2eAgFnPc4d1
role	role	k1gFnPc4
</s>
<s>
Kurýr	kurýr	k1gMnSc1
<g/>
,	,	kIx,
Rychle	rychle	k6eAd1
a	a	k8xC
zběsile	zběsile	k6eAd1
8	#num#	k4
<g/>
,	,	kIx,
Podfu	Podf	k1gInSc2
<g/>
(	(	kIx(
<g/>
c	c	k0
<g/>
)	)	kIx)
<g/>
k	k	k7c3
<g/>
,	,	kIx,
Sbal	sbalit	k5eAaPmRp2nS
prachy	prach	k1gInPc4
a	a	k8xC
vypadni	vypadnout	k5eAaPmRp2nS
<g/>
,	,	kIx,
Loupež	loupež	k1gFnSc1
po	po	k7c6
Italsku	Italsko	k1gNnSc6
<g/>
,	,	kIx,
Postradatelní	postradatelný	k2eAgMnPc1d1
<g/>
,	,	kIx,
Rychle	rychle	k6eAd1
a	a	k8xC
zběsile	zběsile	k6eAd1
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jason	Jason	k1gMnSc1
Statham	Statham	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
26	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc4
1967	#num#	k4
Shirebrook	Shirebrook	k1gInSc4
<g/>
,	,	kIx,
Derbyshire	Derbyshir	k1gMnSc5
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
závodník	závodník	k1gMnSc1
ve	v	k7c6
skocích	skok	k1gInPc6
do	do	k7c2
vody	voda	k1gFnSc2
a	a	k8xC
model	model	k1gInSc4
proslavený	proslavený	k2eAgInSc4d1
filmy	film	k1gInPc4
<g/>
:	:	kIx,
Sbal	sbalit	k5eAaPmRp2nS
prachy	prach	k1gInPc4
a	a	k8xC
vypadni	vypadnout	k5eAaPmRp2nS
<g/>
,	,	kIx,
Kurýr	kurýr	k1gMnSc1
<g/>
,	,	kIx,
Crank	Crank	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Zastav	zastavit	k5eAaPmRp2nS
a	a	k8xC
nepřežiješ	přežít	k5eNaPmIp2nS
<g/>
"	"	kIx"
a	a	k8xC
Loupež	loupež	k1gFnSc1
po	po	k7c6
Italsku	Italsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Též	též	k6eAd1
namluvil	namluvit	k5eAaBmAgMnS,k5eAaPmAgMnS
protagonistu	protagonista	k1gMnSc4
FPS	FPS	kA
hry	hra	k1gFnPc1
Sniper	Sniper	k1gMnSc1
X	X	kA
With	With	k1gMnSc1
Jason	Jason	k1gMnSc1
Statham	Statham	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
po	po	k7c6
šestiletém	šestiletý	k2eAgInSc6d1
vztahu	vztah	k1gInSc6
zasnoubení	zasnoubení	k1gNnSc2
s	s	k7c7
modelkou	modelka	k1gFnSc7
a	a	k8xC
herečkou	herečka	k1gFnSc7
Rosie	Rosi	k1gMnPc4
Huntington-Whiteley	Huntington-Whiteleum	k1gNnPc7
(	(	kIx(
<g/>
*	*	kIx~
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2017	#num#	k4
pár	pár	k4xCyI
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
čekají	čekat	k5eAaImIp3nP
potomka	potomek	k1gMnSc4
a	a	k8xC
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
syn	syn	k1gMnSc1
Jack	Jack	k1gMnSc1
Oscar	Oscar	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1998	#num#	k4
</s>
<s>
Sbal	sbalit	k5eAaPmRp2nS
prachy	prach	k1gInPc4
a	a	k8xC
vypadni	vypadnout	k5eAaPmRp2nS
</s>
<s>
Bacon	Bacon	k1gMnSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
Podfu	Podf	k1gMnSc3
<g/>
(	(	kIx(
<g/>
c	c	k0
<g/>
)	)	kIx)
<g/>
k	k	k7c3
</s>
<s>
Turek	Turek	k1gMnSc1
</s>
<s>
Turn	Turn	k1gMnSc1
It	It	k1gMnSc1
Up	Up	k1gMnSc1
</s>
<s>
Mr	Mr	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
2001	#num#	k4
</s>
<s>
Duchové	duch	k1gMnPc1
Marsu	Mars	k1gInSc2
</s>
<s>
seržant	seržant	k1gMnSc1
Jericho	Jericho	k1gNnSc1
Butler	Butler	k1gMnSc1
</s>
<s>
Jedinečný	jedinečný	k2eAgInSc1d1
</s>
<s>
agent	agent	k1gMnSc1
MVA	MVA	kA
Evan	Evan	k1gMnSc1
Funsch	Funsch	k1gMnSc1
</s>
<s>
Fotbal	fotbal	k1gInSc1
za	za	k7c7
mřížemi	mříž	k1gFnPc7
</s>
<s>
Mnich	mnich	k1gMnSc1
</s>
<s>
2002	#num#	k4
</s>
<s>
Kurýr	kurýr	k1gMnSc1
</s>
<s>
Frank	Frank	k1gMnSc1
Martin	Martin	k1gMnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
Loupež	loupež	k1gFnSc1
po	po	k7c6
italsku	italsek	k1gInSc6
</s>
<s>
Hezoun	hezoun	k1gMnSc1
Rob	roba	k1gFnPc2
</s>
<s>
2004	#num#	k4
</s>
<s>
Collateral	Collaterat	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Frank	Frank	k1gMnSc1
Martin	Martin	k1gMnSc1
</s>
<s>
cameo	cameo	k6eAd1
</s>
<s>
Cellular	Cellular	k1gMnSc1
</s>
<s>
Ethan	ethan	k1gInSc1
Greer	Grera	k1gFnPc2
</s>
<s>
2005	#num#	k4
</s>
<s>
Kurýr	kurýr	k1gMnSc1
2	#num#	k4
</s>
<s>
Frank	Frank	k1gMnSc1
Martin	Martin	k1gMnSc1
</s>
<s>
Revolver	revolver	k1gInSc1
</s>
<s>
Jake	Jake	k6eAd1
Green	Green	k2eAgInSc1d1
</s>
<s>
London	London	k1gMnSc1
</s>
<s>
Bateman	Bateman	k1gMnSc1
</s>
<s>
Chaos	chaos	k1gInSc1
</s>
<s>
detektiv	detektiv	k1gMnSc1
Quentin	Quentin	k1gMnSc1
Conners	Conners	k1gInSc4
</s>
<s>
2006	#num#	k4
</s>
<s>
Růžový	růžový	k2eAgInSc1d1
panter	panter	k1gInSc1
</s>
<s>
Yves	Yves	k6eAd1
Gluant	Gluant	k1gInSc1
</s>
<s>
neuveden	uveden	k2eNgMnSc1d1
v	v	k7c6
titulcích	titulek	k1gInPc6
</s>
<s>
Zastav	zastavit	k5eAaPmRp2nS
a	a	k8xC
nepřežiješ	přežít	k5eNaPmIp2nS
</s>
<s>
Chev	Chev	k1gMnSc1
Chelios	Chelios	k1gMnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Boj	boj	k1gInSc1
</s>
<s>
agent	agent	k1gMnSc1
FBI	FBI	kA
John	John	k1gMnSc1
Crawford	Crawford	k1gMnSc1
</s>
<s>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
krále	král	k1gMnSc2
</s>
<s>
Farmer	Farmer	k1gMnSc1
Daimon	Daimon	k1gMnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1
práce	práce	k1gFnSc1
</s>
<s>
Terry	Terra	k1gFnPc1
Leather	Leathra	k1gFnPc2
</s>
<s>
Rallye	rallye	k1gFnSc1
smrti	smrt	k1gFnSc2
</s>
<s>
Jensen	Jensen	k2eAgMnSc1d1
Garner	Garner	k1gMnSc1
„	„	k?
<g/>
Frankenstein	Frankenstein	k1gMnSc1
<g/>
“	“	k?
Ames	Ames	k1gInSc1
</s>
<s>
Truth	Truth	k1gInSc1
in	in	k?
24	#num#	k4
</s>
<s>
vypravěč	vypravěč	k1gMnSc1
</s>
<s>
dokument	dokument	k1gInSc1
</s>
<s>
Kurýr	kurýr	k1gMnSc1
3	#num#	k4
</s>
<s>
Frank	Frank	k1gMnSc1
Martin	Martin	k1gMnSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Zastav	zastavit	k5eAaPmRp2nS
a	a	k8xC
nepřežiješ	přežít	k5eNaPmIp2nS
2	#num#	k4
-	-	kIx~
Vysoké	vysoký	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
</s>
<s>
Chev	Chev	k1gMnSc1
Chelios	Chelios	k1gMnSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Jasper	Jasper	k1gMnSc1
Bagges	Bagges	k1gMnSc1
</s>
<s>
Expendables	Expendables	k1gInSc1
<g/>
:	:	kIx,
Postradatelní	postradatelný	k2eAgMnPc1d1
</s>
<s>
Lee	Lea	k1gFnSc3
Christmas	Christmas	k1gMnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Mechanik	mechanik	k1gMnSc1
zabiják	zabiják	k1gMnSc1
</s>
<s>
Arthur	Arthur	k1gMnSc1
Bishop	Bishop	k1gInSc4
</s>
<s>
Gnomeo	Gnomeo	k6eAd1
&	&	k?
Julie	Julie	k1gFnSc1
</s>
<s>
Tybalt	Tybalt	k1gMnSc1
</s>
<s>
dabing	dabing	k1gInSc1
</s>
<s>
Blesk	blesk	k1gInSc1
</s>
<s>
detektiv	detektiv	k1gMnSc1
seržant	seržant	k1gMnSc1
Tom	Tom	k1gMnSc1
Brant	Brant	k?
</s>
<s>
Elitní	elitní	k2eAgMnPc1d1
zabijáci	zabiják	k1gMnPc1
</s>
<s>
Danny	Danen	k2eAgFnPc1d1
Bryce	Bryce	k1gFnPc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Le	Le	k?
Mans	Mans	k1gInSc1
-	-	kIx~
Každá	každý	k3xTgFnSc1
vteřina	vteřina	k1gFnSc1
se	se	k3xPyFc4
počítá	počítat	k5eAaImIp3nS
</s>
<s>
vypravěč	vypravěč	k1gMnSc1
</s>
<s>
dokument	dokument	k1gInSc1
</s>
<s>
Safe	safe	k1gInSc1
</s>
<s>
Luke	Luke	k6eAd1
Wright	Wright	k1gMnSc1
</s>
<s>
Expendables	Expendables	k1gInSc1
<g/>
:	:	kIx,
Postradatelní	postradatelný	k2eAgMnPc1d1
2	#num#	k4
</s>
<s>
Lee	Lea	k1gFnSc3
Christmas	Christmas	k1gMnSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Parker	Parker	k1gMnSc1
</s>
<s>
Parker	Parker	k1gMnSc1
</s>
<s>
Rychle	rychle	k6eAd1
a	a	k8xC
zběsile	zběsile	k6eAd1
6	#num#	k4
</s>
<s>
Deckard	Deckard	k1gMnSc1
Shaw	Shaw	k1gMnSc1
</s>
<s>
cameo	cameo	k6eAd1
</s>
<s>
Crayz	Crayz	k1gMnSc1
Joe	Joe	k1gMnSc1
</s>
<s>
Joey	Joea	k1gFnPc1
Jones	Jonesa	k1gFnPc2
</s>
<s>
Nevyřízený	vyřízený	k2eNgInSc4d1
účet	účet	k1gInSc4
</s>
<s>
Phil	Phil	k1gMnSc1
Broker	broker	k1gMnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Expendables	Expendables	k1gInSc1
<g/>
:	:	kIx,
Postradatelní	postradatelný	k2eAgMnPc1d1
3	#num#	k4
</s>
<s>
Lee	Lea	k1gFnSc3
Christmas	Christmas	k1gMnSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
Divoká	divoký	k2eAgFnSc1d1
karta	karta	k1gFnSc1
</s>
<s>
Nick	Nick	k1gMnSc1
Wild	Wild	k1gMnSc1
</s>
<s>
Rychle	rychle	k6eAd1
a	a	k8xC
zběsile	zběsile	k6eAd1
7	#num#	k4
</s>
<s>
Deckard	Deckard	k1gMnSc1
Shaw	Shaw	k1gMnSc1
</s>
<s>
Špión	špión	k1gMnSc1
</s>
<s>
Rick	Rick	k6eAd1
Ford	ford	k1gInSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Mechanik	mechanik	k1gMnSc1
zabiják	zabiják	k1gMnSc1
<g/>
:	:	kIx,
Vzkříšení	vzkříšení	k1gNnSc1
</s>
<s>
Arthur	Arthur	k1gMnSc1
Bishop	Bishop	k1gInSc4
</s>
<s>
2017	#num#	k4
</s>
<s>
Rychle	rychle	k6eAd1
a	a	k8xC
zběsile	zběsile	k6eAd1
8	#num#	k4
</s>
<s>
Deckard	Deckard	k1gMnSc1
Shaw	Shaw	k1gMnSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
MEG	MEG	kA
<g/>
:	:	kIx,
Monstrum	monstrum	k1gNnSc1
z	z	k7c2
hlubin	hlubina	k1gFnPc2
</s>
<s>
Jonas	Jonas	k1gMnSc1
Taylor	Taylor	k1gMnSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
Rychle	rychle	k6eAd1
a	a	k8xC
zběsile	zběsile	k6eAd1
<g/>
:	:	kIx,
Hobbs	Hobbs	k1gInSc1
a	a	k8xC
Shaw	Shaw	k1gFnSc1
</s>
<s>
Deckard	Deckard	k1gMnSc1
Shaw	Shaw	k1gMnSc1
</s>
<s>
bude	být	k5eAaImBp3nS
oznámeno	oznámen	k2eAgNnSc1d1
</s>
<s>
Cash	cash	k1gFnSc1
Truck	truck	k1gInSc1
</s>
<s>
H	H	kA
</s>
<s>
postprodukce	postprodukce	k1gFnSc1
</s>
<s>
Videohry	videohra	k1gFnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Role	role	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
2002	#num#	k4
</s>
<s>
Red	Red	k?
Faction	Faction	k1gInSc1
II	II	kA
</s>
<s>
Shrike	Shrike	k6eAd1
</s>
<s>
2003	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
</s>
<s>
seržant	seržant	k1gMnSc1
Waters	Watersa	k1gFnPc2
</s>
<s>
2015	#num#	k4
</s>
<s>
Sniper	Sniper	k1gMnSc1
X	X	kA
with	with	k1gMnSc1
Jason	Jason	k1gMnSc1
Statham	Statham	k1gInSc4
</s>
<s>
vůdce	vůdce	k1gMnSc1
týmu	tým	k1gInSc2
</s>
<s>
mobilní	mobilní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
GAVILANES	GAVILANES	kA
<g/>
,	,	kIx,
Grace	Grace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jack	Jack	k1gMnSc1
Oscar	Oscar	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
People	People	k1gFnPc2
<g/>
,	,	kIx,
2017-09-19	2017-09-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jason	Jason	k1gInSc1
Statham	Statham	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Jason	Jason	k1gInSc1
Statham	Statham	k1gInSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Jason	Jason	k1gInSc1
Statham	Statham	k1gInSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
27350	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1017120617	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1925	#num#	k4
5247	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2003003729	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
107876545	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2003003729	#num#	k4
</s>
