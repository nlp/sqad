<s>
Rubín	rubín	k1gInSc1	rubín
je	být	k5eAaImIp3nS	být
růžový	růžový	k2eAgInSc1d1	růžový
až	až	k8xS	až
červený	červený	k2eAgInSc1d1	červený
drahokam	drahokam	k1gInSc1	drahokam
sestávající	sestávající	k2eAgInSc1d1	sestávající
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
minerálu	minerál	k1gInSc2	minerál
korundu	korund	k1gInSc2	korund
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
chromu	chromat	k5eAaImIp1nS	chromat
způsobující	způsobující	k2eAgNnSc4d1	způsobující
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
