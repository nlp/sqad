<s>
František	František	k1gMnSc1
Továrek	Továrek	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Továrek	Továrek	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1915	#num#	k4
Konice	Konice	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
74	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Povolání	povolání	k1gNnPc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
dobrodružná	dobrodružný	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Továrek	Továrek	k1gMnSc1
(	(	kIx(
<g/>
1915	#num#	k4
Konice	Konice	k1gFnSc2
–	–	k?
1990	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
regionální	regionální	k2eAgMnSc1d1
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
dobrodružné	dobrodružný	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
pro	pro	k7c4
mládež	mládež	k1gFnSc4
a	a	k8xC
překladatel	překladatel	k1gMnSc1
z	z	k7c2
ukrajinštiny	ukrajinština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
k	k	k7c3
zakládající	zakládající	k2eAgFnSc3d1
generaci	generace	k1gFnSc3
konických	konický	k2eAgMnPc2d1
skautů	skaut	k1gMnPc2
a	a	k8xC
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
oddíl	oddíl	k1gInSc4
i	i	k8xC
vedl	vést	k5eAaImAgMnS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Převážnou	převážný	k2eAgFnSc4d1
část	část	k1gFnSc4
života	život	k1gInSc2
prožil	prožít	k5eAaPmAgInS
v	v	k7c6
Jevíčku	Jevíčko	k1gNnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Dobrodružné	dobrodružný	k2eAgInPc1d1
romány	román	k1gInPc1
pro	pro	k7c4
mládež	mládež	k1gFnSc4
</s>
<s>
Prázdniny	prázdniny	k1gFnPc1
na	na	k7c6
táboře	tábor	k1gInSc6
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
V	v	k7c6
kolibě	koliba	k1gFnSc6
pod	pod	k7c7
Holatýnem	Holatýn	k1gInSc7
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
"	"	kIx"
<g/>
Kronika	kronika	k1gFnSc1
mého	můj	k3xOp1gInSc2
oddílu	oddíl	k1gInSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Za	za	k7c7
vlajkou	vlajka	k1gFnSc7
vpřed	vpřed	k6eAd1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Začarovaný	začarovaný	k2eAgInSc1d1
tábor	tábor	k1gInSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
K	k	k7c3
poslední	poslední	k2eAgFnSc3d1
poctě	pocta	k1gFnSc3
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Překladová	překladový	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
"	"	kIx"
<g/>
Ježek	Ježek	k1gMnSc1
bohatýr	bohatýr	k1gMnSc1
<g/>
:	:	kIx,
Povídky	povídka	k1gFnPc1
a	a	k8xC
pohádky	pohádka	k1gFnPc1
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
Hory	hora	k1gFnPc1
a	a	k8xC
lidé	člověk	k1gMnPc1
-	-	kIx~
Reportáže	reportáž	k1gFnSc2
ze	z	k7c2
Zakarpatska	Zakarpatsk	k1gInSc2
<g/>
"	"	kIx"
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.skautikonice.skauting.cz/historie/Historie-na%20net.doc	http://www.skautikonice.skauting.cz/historie/Historie-na%20net.doc	k1gInSc1
<g/>
↑	↑	k?
http://www.gruntova.cz/druha-svetova-valka/vysadek-gustava-schneidera	http://www.gruntova.cz/druha-svetova-valka/vysadek-gustava-schneidera	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
František	František	k1gMnSc1
Továrek	Továrek	k1gMnSc1
</s>
<s>
http://www.databazeknih.cz/autori/frantisek-tovarek-45315	http://www.databazeknih.cz/autori/frantisek-tovarek-45315	k4
</s>
<s>
http://www.knihovna-jevicko.cz/-frantisek-tovarek/	http://www.knihovna-jevicko.cz/-frantisek-tovarek/	k?
</s>
<s>
http://www.regionmtj.cz/osobnosti-regionu/	http://www.regionmtj.cz/osobnosti-regionu/	k?
</s>
<s>
http://www.klub-pratel-boskovic.cz/osobnosti/kouril-frantisek.php	http://www.klub-pratel-boskovic.cz/osobnosti/kouril-frantisek.php	k1gMnSc1
</s>
<s>
http://www.skautikonice.skauting.cz/historie/Historie-na%20net.doc	http://www.skautikonice.skauting.cz/historie/Historie-na%20net.doc	k6eAd1
</s>
<s>
http://www.gruntova.cz/druha-svetova-valka/vysadek-gustava-schneidera	http://www.gruntova.cz/druha-svetova-valka/vysadek-gustava-schneidera	k1gFnSc1
</s>
<s>
http://www.divadlo-most.cz/magazin/archiv/2015_01/zajimavosti.html	http://www.divadlo-most.cz/magazin/archiv/2015_01/zajimavosti.html	k1gMnSc1
</s>
<s>
http://www.jevicko.cz/soubory/2015_rijen_priloha_inicialy.pdf	http://www.jevicko.cz/soubory/2015_rijen_priloha_inicialy.pdf	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
17	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1132836	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5561	#num#	k4
4166	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85330433	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
35911927	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85330433	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
