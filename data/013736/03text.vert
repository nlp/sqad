<s>
Zrůda	zrůda	k1gFnSc1
(	(	kIx(
<g/>
film	film	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zrůda	zrůda	k1gFnSc1
Původní	původní	k2eAgFnSc1d1
název	název	k1gInSc4
</s>
<s>
Monster	monstrum	k1gNnPc2
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
109	#num#	k4
minut	minuta	k1gFnPc2
Žánr	žánr	k1gInSc4
</s>
<s>
kriminální	kriminální	k2eAgInSc1d1
/	/	kIx~
životopisný	životopisný	k2eAgInSc1d1
Scénář	scénář	k1gInSc1
</s>
<s>
Patty	Patta	k1gFnPc1
Jenkins	Jenkinsa	k1gFnPc2
Režie	režie	k1gFnSc2
</s>
<s>
Patty	Patt	k1gInPc1
Jenkinsová	Jenkinsová	k1gFnSc1
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Charlize	Charlize	k1gFnSc1
TheronováChristina	TheronováChristina	k1gFnSc1
RicciBruce	RicciBruce	k1gFnSc1
DernLee	DernLee	k1gFnSc1
TergesenAnnie	TergesenAnnie	k1gFnSc1
Corleyová	Corleyová	k1gFnSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Charlize	Charlize	k6eAd1
TheronováMark	TheronováMark	k1gInSc1
DamonClark	DamonClark	k1gInSc1
PetersonPonald	PetersonPonalda	k1gFnPc2
KushnerBrad	KushnerBrad	k1gInSc1
Wyman	Wyman	k1gMnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
BT	BT	kA
Kamera	kamera	k1gFnSc1
</s>
<s>
Steven	Steven	k2eAgInSc1d1
Bernstein	Bernstein	k2eAgInSc1d1
Střih	střih	k1gInSc1
</s>
<s>
Arthur	Arthur	k1gMnSc1
CoburnJane	CoburnJan	k1gMnSc5
Kursonová	Kursonová	k1gFnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
20031217	#num#	k4
<g/>
a	a	k8xC
<g/>
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
20040715	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2004	#num#	k4
Produkční	produkční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Media	medium	k1gNnSc2
8	#num#	k4
EntertainmentDEJ	EntertainmentDEJ	k1gFnSc2
Productions	Productionsa	k1gFnPc2
Distribuce	distribuce	k1gFnSc2
</s>
<s>
Newmarket	Newmarket	k1gInSc1
Films	Films	k1gInSc1
Rozpočet	rozpočet	k1gInSc1
</s>
<s>
8	#num#	k4
000	#num#	k4
000	#num#	k4
USD	USD	kA
Tržby	tržba	k1gFnSc2
</s>
<s>
60	#num#	k4
378	#num#	k4
584	#num#	k4
USD	USD	kA
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Oscar	Oscar	k1gInSc1
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
Zrůda	zrůda	k1gFnSc1
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zrůda	zrůda	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Monster	Monster	k1gNnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americké	americký	k2eAgNnSc1d1
kriminální	kriminální	k2eAgNnSc1d1
drama	drama	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
o	o	k7c6
sériové	sériový	k2eAgFnSc6d1
vražedkyni	vražedkyně	k1gFnSc6
Aileen	Aileen	k1gFnPc2
Wuornosové	Wuornosový	k2eAgFnPc1d1
<g/>
,	,	kIx,
prostitutce	prostitutka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
za	za	k7c2
vraždy	vražda	k1gFnSc2
šesti	šest	k4xCc2
mužů	muž	k1gMnPc2
spáchaných	spáchaný	k2eAgInPc2d1
na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátkem	počátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
odsouzená	odsouzená	k1gFnSc1
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprava	poprava	k1gFnSc1
smrtící	smrtící	k2eAgMnSc1d1
injekcí	injekce	k1gFnSc7
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
ve	v	k7c6
floridské	floridský	k2eAgFnSc6d1
věznici	věznice	k1gFnSc6
v	v	k7c6
říjnu	říjen	k1gInSc6
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scénář	scénář	k1gInSc1
napsala	napsat	k5eAaPmAgFnS,k5eAaBmAgFnS
režisérka	režisérka	k1gFnSc1
snímku	snímek	k1gInSc2
Patty	Patta	k1gFnSc2
Jenkinsová	Jenkinsová	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
na	na	k7c6
berlínském	berlínský	k2eAgInSc6d1
mezinárodním	mezinárodní	k2eAgInSc6d1
filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
2004	#num#	k4
získala	získat	k5eAaPmAgFnS
nominaci	nominace	k1gFnSc4
na	na	k7c4
Zlatého	zlatý	k2eAgMnSc4d1
medvěda	medvěd	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
vražedkyně	vražedkyně	k1gFnSc2
ztvárnila	ztvárnit	k5eAaPmAgFnS
jihoafrická	jihoafrický	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
Charlize	Charlize	k1gFnSc1
Theronová	Theronová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
kvůli	kvůli	k7c3
roli	role	k1gFnSc3
přibrala	přibrat	k5eAaPmAgFnS
patnáct	patnáct	k4xCc4
kilogramů	kilogram	k1gInPc2
váhy	váha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
svůj	svůj	k3xOyFgInSc4
výkon	výkon	k1gInSc4
byla	být	k5eAaImAgFnS
oceněna	ocenit	k5eAaPmNgFnS
celkem	celkem	k6eAd1
sedmnácti	sedmnáct	k4xCc7
cenami	cena	k1gFnPc7
<g/>
,	,	kIx,
včetně	včetně	k7c2
Oscara	Oscar	k1gMnSc2
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
ženský	ženský	k2eAgInSc4d1
herecký	herecký	k2eAgInSc4d1
výkon	výkon	k1gInSc4
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
<g/>
,	,	kIx,
Zlatého	zlatý	k2eAgInSc2d1
glóbu	glóbus	k1gInSc2
pro	pro	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
herečku	herečka	k1gFnSc4
ve	v	k7c6
filmovém	filmový	k2eAgNnSc6d1
dramatu	drama	k1gNnSc6
a	a	k8xC
Stříbrného	stříbrný	k2eAgMnSc2d1
medvěda	medvěd	k1gMnSc2
pro	pro	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
herečku	herečka	k1gFnSc4
na	na	k7c6
Berlinale	Berlinale	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
filmovou	filmový	k2eAgFnSc7d1
partnerkou	partnerka	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
americká	americký	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
Christina	Christina	k1gFnSc1
Ricci	Ricce	k1gFnSc4
<g/>
,	,	kIx,
představující	představující	k2eAgFnSc4d1
mladou	mladý	k2eAgFnSc4d1
lesbickou	lesbický	k2eAgFnSc4d1
milenku	milenka	k1gFnSc4
Selby	Selba	k1gFnSc2
Wallovou	Wallová	k1gFnSc7
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
postava	postava	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
podle	podle	k7c2
skutečné	skutečný	k2eAgFnSc2d1
Wuornosové	Wuornosový	k2eAgFnSc2d1
partnerky	partnerka	k1gFnSc2
Tyrie	Tyrie	k1gFnSc2
Mooreové	Mooreová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Obhroublá	obhroublý	k2eAgFnSc1d1
prostitutka	prostitutka	k1gFnSc1
středního	střední	k2eAgInSc2d1
věku	věk	k1gInSc2
Aileen	Aileen	k1gInSc1
Wuornosová	Wuornosová	k1gFnSc1
(	(	kIx(
<g/>
Charlize	Charlize	k1gFnSc1
Theronová	Theronová	k1gFnSc1
<g/>
)	)	kIx)
přijíždí	přijíždět	k5eAaImIp3nS
na	na	k7c4
Floridu	Florida	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
lesbickém	lesbický	k2eAgInSc6d1
baru	bar	k1gInSc6
potkává	potkávat	k5eAaImIp3nS
Selby	Selba	k1gFnPc4
Wallovou	Wallová	k1gFnSc4
(	(	kIx(
<g/>
Christina	Christin	k2eAgFnSc1d1
Ricci	Ricce	k1gMnPc7
<g/>
)	)	kIx)
(	(	kIx(
<g/>
postava	postava	k1gFnSc1
vytvořená	vytvořený	k2eAgFnSc1d1
podle	podle	k7c2
její	její	k3xOp3gFnSc2
skutečné	skutečný	k2eAgFnSc2d1
partnerky	partnerka	k1gFnSc2
Tyrie	Tyrie	k1gFnSc2
Mooreové	Mooreová	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
prvotní	prvotní	k2eAgInSc4d1
odpor	odpor	k1gInSc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
stráví	strávit	k5eAaPmIp3nP
noc	noc	k1gFnSc4
a	a	k8xC
naváže	navázat	k5eAaPmIp3nS
dlouhodobější	dlouhodobý	k2eAgInSc4d2
vztah	vztah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
vypoví	vypovědět	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
před	před	k7c7
sebevraždou	sebevražda	k1gFnSc7
chtěla	chtít	k5eAaImAgFnS
utratit	utratit	k5eAaPmF
posledních	poslední	k2eAgInPc2d1
pět	pět	k4xCc4
dolarů	dolar	k1gInPc2
od	od	k7c2
zákazníka	zákazník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Bez	bez	k7c2
finančních	finanční	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
spolu	spolu	k6eAd1
nové	nový	k2eAgFnSc2d1
přítělkyně	přítělkyně	k1gFnSc2
odcházejí	odcházet	k5eAaImIp3nP
žít	žít	k5eAaImF
do	do	k7c2
hotelů	hotel	k1gInPc2
a	a	k8xC
pronajatých	pronajatý	k2eAgInPc2d1
bytů	byt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aileen	Aileen	k1gInSc1
se	se	k3xPyFc4
nevyhne	vyhnout	k5eNaPmIp3nS
další	další	k2eAgFnPc4d1
negativní	negativní	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
<g/>
,	,	kIx,
když	když	k8xS
ji	on	k3xPp3gFnSc4
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
zákazníků	zákazník	k1gMnPc2
Vincent	Vincent	k1gMnSc1
Corey	Corea	k1gFnSc2
(	(	kIx(
<g/>
Lee	Lea	k1gFnSc3
Tergesen	Tergesen	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
fyzicky	fyzicky	k6eAd1
napadne	napadnout	k5eAaPmIp3nS
v	v	k7c6
autě	auto	k1gNnSc6
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
podaří	podařit	k5eAaPmIp3nS
vyprostit	vyprostit	k5eAaPmF
ruce	ruka	k1gFnPc4
z	z	k7c2
uvázané	uvázaný	k2eAgFnSc2d1
smyčky	smyčka	k1gFnSc2
<g/>
,	,	kIx,
násilníka	násilník	k1gMnSc2
v	v	k7c6
sebeobraně	sebeobrana	k1gFnSc6
zastřelí	zastřelit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
zkušenosti	zkušenost	k1gFnSc6
se	se	k3xPyFc4
neúspěšně	úspěšně	k6eNd1
pokusí	pokusit	k5eAaPmIp3nS
vymanit	vymanit	k5eAaPmF
ze	z	k7c2
zaběhnutého	zaběhnutý	k2eAgInSc2d1
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
opustit	opustit	k5eAaPmF
práci	práce	k1gFnSc4
prostitutky	prostitutka	k1gFnSc2
a	a	k8xC
začít	začít	k5eAaPmF
lepší	dobrý	k2eAgFnSc4d2
etapu	etapa	k1gFnSc4
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Realita	realita	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
odlišná	odlišný	k2eAgFnSc1d1
<g/>
,	,	kIx,
než	než	k8xS
vysněné	vysněný	k2eAgInPc1d1
plány	plán	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dovede	dovést	k5eAaPmIp3nS
ji	on	k3xPp3gFnSc4
zpět	zpět	k6eAd1
k	k	k7c3
prostituci	prostituce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silná	silný	k2eAgFnSc1d1
averze	averze	k1gFnSc1
k	k	k7c3
mužům	muž	k1gMnPc3
nastartuje	nastartovat	k5eAaPmIp3nS
sérii	série	k1gFnSc4
vražd	vražda	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
postupně	postupně	k6eAd1
zastřelí	zastřelit	k5eAaPmIp3nS
několik	několik	k4yIc4
zákazníků	zákazník	k1gMnPc2
a	a	k8xC
také	také	k9
náhodného	náhodný	k2eAgInSc2d1
jezdce	jezdec	k1gInSc2
<g/>
,	,	kIx,
snažícího	snažící	k2eAgMnSc2d1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
pomoci	pomoc	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
se	s	k7c7
Selby	Selb	k1gInPc7
přežívají	přežívat	k5eAaImIp3nP
z	z	k7c2
peněz	peníze	k1gInPc2
oloupených	oloupený	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
si	se	k3xPyFc3
Selby	Selba	k1gFnPc4
začne	začít	k5eAaPmIp3nS
uvědomovat	uvědomovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c7
všemi	všecek	k3xTgFnPc7
vraždami	vražda	k1gFnPc7
může	moct	k5eAaImIp3nS
stát	stát	k5eAaImF,k5eAaPmF
přítelkyně	přítelkyně	k1gFnSc1
a	a	k8xC
spatřuje	spatřovat	k5eAaImIp3nS
portréty	portrét	k1gInPc1
sebe	sebe	k3xPyFc4
a	a	k8xC
své	svůj	k3xOyFgFnSc2
lásky	láska	k1gFnSc2
v	v	k7c6
televizi	televize	k1gFnSc6
<g/>
,	,	kIx,
vztah	vztah	k1gInSc1
končí	končit	k5eAaImIp3nS
a	a	k8xC
autobusem	autobus	k1gInSc7
odjíždí	odjíždět	k5eAaImIp3nS
domů	domů	k6eAd1
do	do	k7c2
Ohia	Ohio	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Aileen	Ailena	k1gFnPc2
je	být	k5eAaImIp3nS
zatčena	zatknout	k5eAaPmNgFnS
v	v	k7c6
motorkářském	motorkářský	k2eAgInSc6d1
baru	bar	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInSc1
telefonát	telefonát	k1gInSc1
z	z	k7c2
vězení	vězení	k1gNnSc2
se	se	k3xPyFc4
Selby	Selba	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
vyšetřovateli	vyšetřovatel	k1gMnPc7
<g/>
,	,	kIx,
dodává	dodávat	k5eAaImIp3nS
důkazní	důkazní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
pro	pro	k7c4
trestní	trestní	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
ochránila	ochránit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
lásku	láska	k1gFnSc4
<g/>
,	,	kIx,
Aillen	Aillen	k1gInSc1
doznává	doznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
spáchala	spáchat	k5eAaPmAgFnS
vraždy	vražda	k1gFnPc4
sama	sám	k3xTgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
soudní	soudní	k2eAgFnSc6d1
síni	síň	k1gFnSc6
pak	pak	k6eAd1
Selby	Selba	k1gFnSc2
svědčí	svědčit	k5eAaImIp3nS
z	z	k7c2
pozice	pozice	k1gFnSc2
svědkyně	svědkyně	k1gFnSc2
proti	proti	k7c3
ní	on	k3xPp3gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obžalovaná	obžalovaná	k1gFnSc1
je	být	k5eAaImIp3nS
shledána	shledat	k5eAaPmNgFnS
vinnou	vinný	k2eAgFnSc7d1
a	a	k8xC
odsouzena	odsouzet	k5eAaImNgFnS,k5eAaPmNgFnS
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Charlize	Charlize	k1gFnSc1
TheronováChristina	TheronováChristin	k2eAgFnSc1d1
Ricci	Ricce	k1gFnSc3
</s>
<s>
Charlize	Charlize	k1gFnSc1
Theronová	Theronová	k1gFnSc1
–	–	k?
Aileen	Aileen	k1gInSc1
Wuornosová	Wuornosová	k1gFnSc1
</s>
<s>
Christina	Christina	k1gFnSc1
Ricci	Ricec	k1gInSc3
–	–	k?
Selby	Selba	k1gMnSc2
Wallová	Wallový	k2eAgFnSc5d1
</s>
<s>
Bruce	Bruce	k1gMnSc1
Dern	Dern	k1gMnSc1
–	–	k?
Thomas	Thomas	k1gMnSc1
</s>
<s>
Lee	Lea	k1gFnSc3
Tergesen	Tergesen	k2eAgMnSc1d1
<g/>
–	–	k?
Vincent	Vincent	k1gMnSc1
Corey	Corea	k1gFnSc2
</s>
<s>
Annie	Annie	k1gFnSc1
Corleyová	Corleyový	k2eAgFnSc1d1
–	–	k?
Donna	donna	k1gFnSc1
</s>
<s>
Pruitt	Pruitt	k2eAgMnSc1d1
Taylor	Taylor	k1gMnSc1
Vince	Vince	k?
–	–	k?
Gene	gen	k1gInSc5
/	/	kIx~
koktavý	koktavý	k2eAgMnSc1d1
„	„	k?
<g/>
John	John	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
Marco	Marco	k1gMnSc1
St.	st.	kA
John	John	k1gMnSc1
–	–	k?
Evan	Evan	k1gMnSc1
/	/	kIx~
tajný	tajný	k2eAgMnSc1d1
„	„	k?
<g/>
John	John	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
Marc	Marc	k1gFnSc1
Macaulay	Macaulaa	k1gFnSc2
–	–	k?
Will	Will	k1gMnSc1
/	/	kIx~
táta	táta	k1gMnSc1
„	„	k?
<g/>
John	John	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
Scott	Scott	k1gMnSc1
Wilson	Wilson	k1gMnSc1
–	–	k?
Horton	Horton	k1gInSc1
/	/	kIx~
poslední	poslední	k2eAgMnSc1d1
„	„	k?
<g/>
John	John	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
Kane	kanout	k5eAaImIp3nS
Hodder	Hodder	k1gInSc4
–	–	k?
policejní	policejní	k2eAgMnSc1d1
agent	agent	k1gMnSc1
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
vyzdvihla	vyzdvihnout	k5eAaPmAgFnS
film	film	k1gInSc4
zejména	zejména	k9
za	za	k7c4
vysoce	vysoce	k6eAd1
přesvědčivý	přesvědčivý	k2eAgInSc4d1
výkon	výkon	k1gInSc4
Theronové	Theronový	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
neatraktivní	atraktivní	k2eNgNnSc1d1
duševně	duševně	k6eAd1
narušené	narušený	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
ženy	žena	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjma	vyjma	k7c2
nutnosti	nutnost	k1gFnSc2
nabrání	nabrání	k1gNnSc2
váhy	váha	k1gFnSc2
Jihoafričanka	Jihoafričanka	k1gFnSc1
hrála	hrát	k5eAaImAgFnS
s	s	k7c7
proteticky	proteticky	k6eAd1
upraveným	upravený	k2eAgInSc7d1
chrupem	chrup	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritici	kritik	k1gMnPc1
označili	označit	k5eAaPmAgMnP
její	její	k3xOp3gNnSc4
ztvárnění	ztvárnění	k1gNnSc4
a	a	k8xC
přeměnu	přeměna	k1gFnSc4
vzhledu	vzhled	k1gInSc2
za	za	k7c4
„	„	k?
<g/>
transformaci	transformace	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Filmový	filmový	k2eAgMnSc1d1
kritik	kritik	k1gMnSc1
Roger	Roger	k1gMnSc1
Ebert	Ebert	k1gInSc4
zhodnotil	zhodnotit	k5eAaPmAgMnS
snímek	snímek	k1gInSc4
jako	jako	k8xS,k8xC
nejlepší	dobrý	k2eAgInSc4d3
v	v	k7c6
daném	daný	k2eAgInSc6d1
roce	rok	k1gInSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
roku	rok	k1gInSc2
2009	#num#	k4
jej	on	k3xPp3gInSc4
pak	pak	k6eAd1
zařadil	zařadit	k5eAaPmAgMnS
mezi	mezi	k7c7
nejlepšími	dobrý	k2eAgInPc7d3
filmy	film	k1gInPc7
celé	celý	k2eAgFnSc2d1
dekády	dekáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
vydal	vydat	k5eAaPmAgMnS
autor	autor	k1gMnSc1
hudby	hudba	k1gFnSc2
BT	BT	kA
k	k	k7c3
filmu	film	k1gInSc3
soundtrack	soundtrack	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
MonsterInterpretBTDruh	MonsterInterpretBTDruh	k1gInSc4
albaSoundtrackVydáno	albaSoundtrackVydat	k5eAaPmNgNnS,k5eAaImNgNnS
<g/>
30	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2004	#num#	k4
<g/>
Žánryženský	Žánryženský	k2eAgInSc1d1
kamarádský	kamarádský	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
životopisný	životopisný	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
filmové	filmový	k2eAgNnSc1d1
drama	drama	k1gNnSc1
<g/>
,	,	kIx,
kriminální	kriminální	k2eAgInSc4d1
film	film	k1gInSc4
a	a	k8xC
film	film	k1gInSc4
zabývající	zabývající	k2eAgInSc4d1
se	se	k3xPyFc4
LGBTDélka	LGBTDélka	k1gMnSc1
<g/>
110	#num#	k4
minVydavatelstvídts	minVydavatelstvídts	k6eAd1
EntertainmentNěkterá	EntertainmentNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Všechny	všechen	k3xTgFnPc4
skladby	skladba	k1gFnPc4
složil	složit	k5eAaPmAgMnS
BT	BT	kA
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Childhood	Childhood	k1gInSc1
Montage	Montage	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Girls	girl	k1gFnPc1
Kiss	Kissa	k1gFnPc2
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
The	The	k1gFnSc1
Bus	bus	k1gInSc1
Stop	stop	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Turning	Turning	k1gInSc1
Tricks	Tricks	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
First	First	k1gMnSc1
Kill	Kill	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Job	Job	k1gMnSc1
Hunt	hunt	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Bad	Bad	k1gFnSc1
Cop	cop	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
'	'	kIx"
<g/>
Call	Call	k1gInSc1
Me	Me	k1gMnSc2
Daddy	Dadda	k1gMnSc2
<g/>
'	'	kIx"
Killing	Killing	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
I	i	k9
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Like	Like	k1gInSc1
It	It	k1gMnSc1
Rough	Rough	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Ferris	Ferris	k1gInSc1
Wheel	Wheel	k1gInSc1
(	(	kIx(
<g/>
Love	lov	k1gInSc5
Theme	Them	k1gInSc5
<g/>
)	)	kIx)
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Ditch	Ditch	k1gMnSc1
The	The	k1gMnSc1
Car	car	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Madman	Madman	k1gMnSc1
Speech	speech	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Cop	cop	k1gInSc1
Killing	Killing	k1gInSc1
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
News	News	k1gInSc1
On	on	k3xPp3gMnSc1
TV	TV	kA
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Courtroom	Courtroom	k1gInSc1
<g/>
“	“	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Monster	monstrum	k1gNnPc2
(	(	kIx(
<g/>
2003	#num#	k4
film	film	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
MARXTOVÁ	MARXTOVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pekla	peklo	k1gNnSc2
do	do	k7c2
ráje	ráj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týdeník	týdeník	k1gInSc1
Květy	Květa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prosinec	prosinec	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
XVII	XVII	kA
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
51	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Aileen	Aileen	k1gInSc1
<g/>
:	:	kIx,
Life	Life	k1gInSc1
and	and	k?
Death	Death	k1gInSc1
of	of	k?
a	a	k8xC
Serial	Serial	k1gMnSc1
Killer	Killer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dir	Dir	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nick	Nick	k1gMnSc1
Broomfield	Broomfield	k1gMnSc1
and	and	k?
Joan	Joan	k1gMnSc1
Churchill	Churchill	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Columbia	Columbia	k1gFnSc1
TriStar	TriStara	k1gFnPc2
Home	Home	k1gFnSc1
Entertainment	Entertainment	k1gInSc1
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Monster	monstrum	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rotten	Rotten	k2eAgInSc1d1
Tomatoes	Tomatoes	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
EBERT	EBERT	kA
<g/>
,	,	kIx,
Roger	Roger	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monster	monstrum	k1gNnPc2
<g/>
.	.	kIx.
rogerebert	rogerebert	k1gInSc1
<g/>
.	.	kIx.
<g/>
suntimes	suntimes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
Sun-Times	Sun-Timesa	k1gFnPc2
<g/>
,	,	kIx,
January	Januar	k1gInPc1
1	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Monster	monstrum	k1gNnPc2
Soundtrack	soundtrack	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SoundtrackNet	SoundtrackNet	k1gMnSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
4	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zrůda	zrůda	k1gFnSc1
–	–	k?
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
Zrůda	zrůda	k1gFnSc1
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Zrůda	zrůda	k1gFnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
311845631	#num#	k4
</s>
