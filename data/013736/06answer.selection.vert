<s>
Zrůda	zrůda	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Monster	Monster	k1gNnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americké	americký	k2eAgNnSc1d1
kriminální	kriminální	k2eAgNnSc1d1
drama	drama	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
o	o	k7c6
sériové	sériový	k2eAgFnSc6d1
vražedkyni	vražedkyně	k1gFnSc6
Aileen	Aileen	k1gFnPc2
Wuornosové	Wuornosový	k2eAgFnPc1d1
<g/>
,	,	kIx,
prostitutce	prostitutka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
za	za	k7c2
vraždy	vražda	k1gFnSc2
šesti	šest	k4xCc2
mužů	muž	k1gMnPc2
spáchaných	spáchaný	k2eAgInPc2d1
na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátkem	počátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
odsouzená	odsouzená	k1gFnSc1
k	k	k7c3
trestu	trest	k1gInSc3
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>