<s>
Lesk	lesk	k1gInSc1	lesk
na	na	k7c4	na
rty	ret	k1gInPc4	ret
je	být	k5eAaImIp3nS	být
kosmetický	kosmetický	k2eAgInSc1d1	kosmetický
doplněk	doplněk	k1gInSc1	doplněk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
lesklého	lesklý	k2eAgInSc2d1	lesklý
vzhledu	vzhled	k1gInSc2	vzhled
rtů	ret	k1gInPc2	ret
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rty	ret	k1gInPc4	ret
se	se	k3xPyFc4	se
nanáší	nanášet	k5eAaImIp3nS	nanášet
pomocí	pomocí	k7c2	pomocí
jemného	jemný	k2eAgInSc2d1	jemný
štětečku	štěteček	k1gInSc2	štěteček
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
namáčí	namáčet	k5eAaImIp3nS	namáčet
do	do	k7c2	do
směsi	směs	k1gFnSc2	směs
lesku	lesk	k1gInSc2	lesk
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
lahvičce	lahvička	k1gFnSc6	lahvička
či	či	k8xC	či
flakónku	flakónek	k1gInSc6	flakónek
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
zcela	zcela	k6eAd1	zcela
průsvitný	průsvitný	k2eAgMnSc1d1	průsvitný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
odstínech	odstín	k1gInPc6	odstín
či	či	k8xC	či
se	s	k7c7	s
třpytkami	třpytka	k1gFnPc7	třpytka
<g/>
.	.	kIx.	.
</s>
<s>
Konzistence	konzistence	k1gFnSc1	konzistence
lesku	lesk	k1gInSc2	lesk
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
kapalná	kapalný	k2eAgFnSc1d1	kapalná
či	či	k8xC	či
snadno	snadno	k6eAd1	snadno
roztíratelná	roztíratelný	k2eAgFnSc1d1	roztíratelná
pevnější	pevný	k2eAgFnSc1d2	pevnější
substance	substance	k1gFnSc1	substance
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
lesk	lesk	k1gInSc1	lesk
na	na	k7c4	na
rty	ret	k1gInPc4	ret
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
objevitele	objevitel	k1gMnSc4	objevitel
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Max	Max	k1gMnSc1	Max
Factor	Factor	k1gMnSc1	Factor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
vyrobit	vyrobit	k5eAaPmF	vyrobit
doplněk	doplněk	k1gInSc4	doplněk
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
lesku	lesk	k1gInSc2	lesk
rtů	ret	k1gInPc2	ret
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
komerčně	komerčně	k6eAd1	komerčně
prodáváný	prodáváný	k2eAgInSc1d1	prodáváný
výrobek	výrobek	k1gInSc1	výrobek
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
X-Rated	X-Rated	k1gInSc1	X-Rated
od	od	k7c2	od
Max	max	kA	max
Factor	Factor	k1gInSc1	Factor
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
distribucí	distribuce	k1gFnSc7	distribuce
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgInS	prodávat
lesk	lesk	k1gInSc1	lesk
na	na	k7c4	na
rty	ret	k1gInPc4	ret
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
pevném	pevný	k2eAgInSc6d1	pevný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snadno	snadno	k6eAd1	snadno
roztíratelné	roztíratelný	k2eAgFnSc6d1	roztíratelná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
