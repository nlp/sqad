<s>
Chajim	Chajim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
</s>
<s>
Chajim	Chajim	k1gMnSc1
Weizmannח	Weizmannח	k1gMnSc1
ו	ו	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
prezident	prezident	k1gMnSc1
Izraele	Izrael	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1949	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
David	David	k1gMnSc1
Ben	Ben	k1gInSc1
Gurion	Gurion	k1gInSc1
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Jicchak	Jicchak	k6eAd1
Ben	Ben	k1gInSc1
Cvi	Cvi	k1gFnSc2
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Všeobecní	všeobecný	k2eAgMnPc1d1
sionisté	sionista	k1gMnPc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1874	#num#	k4
Motal	motat	k5eAaImAgInS
<g/>
,	,	kIx,
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
<g/>
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
Bělorusko	Bělorusko	k1gNnSc1
<g/>
)	)	kIx)
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
(	(	kIx(
<g/>
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Rechovot	Rechovot	k1gInSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Rechovot	Rechovot	k1gMnSc1
Choť	choť	k1gMnSc1
</s>
<s>
Věra	Věra	k1gFnSc1
Weizmannová	Weizmannová	k1gFnSc1
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Sophia	Sophius	k1gMnSc4
Getzowa	Getzowus	k1gMnSc4
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Ezer	Ezer	k1gMnSc1
Weizman	Weizman	k1gMnSc1
(	(	kIx(
<g/>
synovec	synovec	k1gMnSc1
<g/>
)	)	kIx)
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Polytechnický	polytechnický	k2eAgInSc1d1
institut	institut	k1gInSc1
v	v	k7c6
DarmstadtuFreiburská	DarmstadtuFreiburský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Profese	profese	k1gFnSc1
</s>
<s>
chemik	chemik	k1gMnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
judaismus	judaismus	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
čestný	čestný	k2eAgInSc1d1
doktorát	doktorát	k1gInSc1
Hebrejské	hebrejský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
honorary	honorar	k1gInPc1
citizen	citizen	k2eAgInSc1d1
of	of	k?
Jerusalem	Jerusalem	k1gInSc1
Podpis	podpis	k1gInSc1
</s>
<s>
Commons	Commons	k6eAd1
</s>
<s>
Chaim	Chaim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Chajim	Chajim	k1gInSc1
Azri	Azri	k1gNnSc2
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
Weizmann	Weizmann	k1gInSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
ח	ח	k?
ע	ע	k?
ו	ו	k?
<g/>
;	;	kIx,
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1874	#num#	k4
<g/>
,	,	kIx,
Motal	motat	k5eAaImAgInS
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
<g/>
,	,	kIx,
Rechovot	Rechovot	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
sionistický	sionistický	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
Světové	světový	k2eAgFnSc2d1
sionistické	sionistický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1948	#num#	k4
až	až	k9
1952	#num#	k4
první	první	k4xOgMnSc1
prezident	prezident	k1gMnSc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
politiku	politika	k1gFnSc4
a	a	k8xC
diplomacii	diplomacie	k1gFnSc4
byl	být	k5eAaImAgMnS
rovněž	rovněž	k9
uznávaný	uznávaný	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
chemie	chemie	k1gFnSc2
a	a	k8xC
akademik	akademik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Německém	německý	k2eAgNnSc6d1
císařství	císařství	k1gNnSc6
a	a	k8xC
Švýcarsku	Švýcarsko	k1gNnSc6
a	a	k8xC
posléze	posléze	k6eAd1
přednášel	přednášet	k5eAaImAgMnS
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
a	a	k8xC
Manchesteru	Manchester	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
publikoval	publikovat	k5eAaBmAgMnS
na	na	k7c4
sto	sto	k4xCgNnSc4
vědeckých	vědecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
si	se	k3xPyFc3
zapsat	zapsat	k5eAaPmF
přes	přes	k7c4
sto	sto	k4xCgNnSc4
patentů	patent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
nejvýznamnějším	významný	k2eAgInSc7d3
objevem	objev	k1gInSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
metoda	metoda	k1gFnSc1
syntetické	syntetický	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
acetonu	aceton	k1gInSc2
prostřednictvím	prostřednictvím	k7c2
bakteriálního	bakteriální	k2eAgNnSc2d1
kvašení	kvašení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
výroby	výroba	k1gFnSc2
výrazně	výrazně	k6eAd1
pomohl	pomoct	k5eAaPmAgMnS
mocnostem	mocnost	k1gFnPc3
Dohody	dohoda	k1gFnSc2
během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
duchovním	duchovní	k2eAgMnSc7d1
otcem	otec	k1gMnSc7
Balfourovy	Balfourův	k2eAgFnSc2d1
deklarace	deklarace	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
vyjádřila	vyjádřit	k5eAaPmAgFnS
své	svůj	k3xOyFgFnPc4
sympatie	sympatie	k1gFnPc4
zřízení	zřízení	k1gNnSc2
židovské	židovský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
domoviny	domovina	k1gFnSc2
v	v	k7c6
Palestině	Palestina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stál	stát	k5eAaImAgMnS
u	u	k7c2
zrodu	zrod	k1gInSc2
nejstarší	starý	k2eAgFnSc2d3
izraelské	izraelský	k2eAgFnSc2d1
vysokoškolské	vysokoškolský	k2eAgFnSc2d1
instituce	instituce	k1gFnSc2
–	–	k?
Hebrejské	hebrejský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
pomáhal	pomáhat	k5eAaImAgInS
založit	založit	k5eAaPmF
společně	společně	k6eAd1
s	s	k7c7
Albertem	Albert	k1gMnSc7
Einsteinem	Einstein	k1gMnSc7
<g/>
,	,	kIx,
Sigmundem	Sigmund	k1gMnSc7
Freudem	Freud	k1gInSc7
a	a	k8xC
Martinem	Martin	k1gMnSc7
Buberem	Buber	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významně	významně	k6eAd1
se	se	k3xPyFc4
také	také	k9
zasadil	zasadit	k5eAaPmAgInS
o	o	k7c4
vznik	vznik	k1gInSc4
vědeckého	vědecký	k2eAgInSc2d1
výzkumného	výzkumný	k2eAgInSc2d1
institutu	institut	k1gInSc2
ve	v	k7c6
městě	město	k1gNnSc6
Rechovot	Rechovota	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
Weizmannův	Weizmannův	k2eAgInSc1d1
institut	institut	k1gInSc1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
a	a	k8xC
kariéra	kariéra	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
Motal	motat	k5eAaImAgMnS
poblíž	poblíž	k7c2
Pinsku	Pinsek	k1gInSc2
v	v	k7c6
tehdejším	tehdejší	k2eAgNnSc6d1
Ruském	ruský	k2eAgNnSc6d1
impériu	impérium	k1gNnSc6
(	(	kIx(
<g/>
dnešním	dnešní	k2eAgNnSc6d1
Bělorusku	Bělorusko	k1gNnSc6
<g/>
)	)	kIx)
jako	jako	k9
třetí	třetí	k4xOgMnSc1
z	z	k7c2
patnácti	patnáct	k4xCc2
dětí	dítě	k1gFnPc2
obchodníka	obchodník	k1gMnSc2
se	s	k7c7
dřevem	dřevo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dětství	dětství	k1gNnSc6
získal	získat	k5eAaPmAgMnS
klasické	klasický	k2eAgNnSc4d1
židovské	židovský	k2eAgNnSc4d1
náboženské	náboženský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
(	(	kIx(
<g/>
cheder	cheder	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
jedenácti	jedenáct	k4xCc6
letech	léto	k1gNnPc6
nastoupil	nastoupit	k5eAaPmAgInS
na	na	k7c4
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Pinsku	Pinsek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
odmaturoval	odmaturovat	k5eAaPmAgInS
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
odjel	odjet	k5eAaPmAgMnS
studovat	studovat	k5eAaImF
chemii	chemie	k1gFnSc4
do	do	k7c2
Německého	německý	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
a	a	k8xC
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
Polytechnický	polytechnický	k2eAgInSc4d1
institut	institut	k1gInSc4
v	v	k7c6
Darmstadtu	Darmstadt	k1gInSc6
a	a	k8xC
Freiburskou	freiburský	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
získal	získat	k5eAaPmAgMnS
doktorát	doktorát	k1gInSc4
z	z	k7c2
chemie	chemie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
začal	začít	k5eAaPmAgMnS
přednášet	přednášet	k5eAaImF
na	na	k7c6
Ženevské	ženevský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
zabývat	zabývat	k5eAaImF
výzkumem	výzkum	k1gInSc7
organické	organický	k2eAgFnSc2d1
chemie	chemie	k1gFnSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
pak	pak	k8xC
barvivy	barvivo	k1gNnPc7
a	a	k8xC
aromatickými	aromatický	k2eAgInPc7d1
uhlovodíky	uhlovodík	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
se	s	k7c7
studentkou	studentka	k1gFnSc7
medicíny	medicína	k1gFnSc2
Věrou	Věra	k1gFnSc7
Kacmanovou	Kacmanová	k1gFnSc7
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
i	i	k9
s	s	k7c7
manželkou	manželka	k1gFnSc7
emigroval	emigrovat	k5eAaBmAgMnS
do	do	k7c2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
biochemie	biochemie	k1gFnSc2
na	na	k7c6
Manchesterské	manchesterský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
uděleno	udělit	k5eAaPmNgNnS
britské	britský	k2eAgNnSc1d1
státní	státní	k2eAgNnSc1d1
občanství	občanství	k1gNnSc1
a	a	k8xC
během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
ředitelem	ředitel	k1gMnSc7
laboratoří	laboratoř	k1gFnPc2
britského	britský	k2eAgNnSc2d1
Královského	královský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gMnPc4
nejslavnější	slavný	k2eAgInSc1d3
objev	objev	k1gInSc1
patří	patřit	k5eAaImIp3nS
syntetický	syntetický	k2eAgInSc1d1
způsob	způsob	k1gInSc1
výroby	výroba	k1gFnSc2
acetonu	aceton	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Významným	významný	k2eAgMnSc7d1
byl	být	k5eAaImAgMnS
především	především	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
aceton	aceton	k1gInSc1
používal	používat	k5eAaImAgInS
k	k	k7c3
výrobě	výroba	k1gFnSc3
výbušnin	výbušnina	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgMnS
jej	on	k3xPp3gInSc4
nedostatek	nedostatek	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
získával	získávat	k5eAaImAgInS
pouze	pouze	k6eAd1
přírodní	přírodní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
z	z	k7c2
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Weizmannova	Weizmannův	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
syntetické	syntetický	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
spočívala	spočívat	k5eAaImAgFnS
v	v	k7c6
bakteriálním	bakteriální	k2eAgNnSc6d1
kvašení	kvašení	k1gNnSc6
za	za	k7c2
pomoci	pomoc	k1gFnSc2
bakterie	bakterie	k1gFnSc2
clostridium	clostridium	k1gNnSc1
acetobutylicum	acetobutylicum	k1gInSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
pojmenované	pojmenovaný	k2eAgNnSc1d1
Clostridium	Clostridium	k1gNnSc1
acetobutylicum	acetobutylicum	k1gInSc1
Weizmann	Weizmann	k1gMnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Weizmannův	Weizmannův	k2eAgInSc1d1
organismus	organismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
za	za	k7c4
svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
publikoval	publikovat	k5eAaBmAgMnS
na	na	k7c4
100	#num#	k4
vědeckých	vědecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
si	se	k3xPyFc3
zapsat	zapsat	k5eAaPmF
110	#num#	k4
patentů	patent	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sionistický	sionistický	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
</s>
<s>
K	k	k7c3
sionismu	sionismus	k1gInSc3
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
ještě	ještě	k9
za	za	k7c2
svého	svůj	k3xOyFgInSc2
pobytu	pobyt	k1gInSc2
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
a	a	k8xC
již	již	k6eAd1
od	od	k7c2
druhého	druhý	k4xOgInSc2
sionistického	sionistický	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
významnou	významný	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
sionistického	sionistický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
pomáhal	pomáhat	k5eAaImAgMnS
v	v	k7c6
rámci	rámec	k1gInSc6
hnutí	hnutí	k1gNnSc2
založit	založit	k5eAaPmF
Demokratickou	demokratický	k2eAgFnSc4d1
frakci	frakce	k1gFnSc4
a	a	k8xC
na	na	k7c6
šestém	šestý	k4xOgInSc6
sionistickém	sionistický	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
v	v	k7c6
srpnu	srpen	k1gInSc6
1903	#num#	k4
patřil	patřit	k5eAaImAgInS
mezi	mezi	k7c4
mladé	mladý	k2eAgNnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
oponenty	oponent	k1gMnPc4
Herzlova	Herzlův	k2eAgInSc2d1
kompromisního	kompromisní	k2eAgInSc2d1
plánu	plán	k1gInSc2
na	na	k7c6
zřízení	zřízení	k1gNnSc6
židovské	židovský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
domoviny	domovina	k1gFnSc2
v	v	k7c6
Ugandě	Uganda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tímto	tento	k3xDgInSc7
návrhem	návrh	k1gInSc7
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
bez	bez	k7c2
Sijónu	Sijón	k1gInSc2
není	být	k5eNaImIp3nS
sionismu	sionismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Tentýž	týž	k3xTgInSc4
rok	rok	k1gInSc4
rovněž	rovněž	k9
lobboval	lobbovat	k5eAaImAgMnS
za	za	k7c4
založení	založení	k1gNnSc4
Hebrejské	hebrejský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gInSc1
návrh	návrh	k1gInSc1
však	však	k9
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
emigroval	emigrovat	k5eAaBmAgMnS
do	do	k7c2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vůdcem	vůdce	k1gMnSc7
britských	britský	k2eAgMnPc2d1
sionistů	sionista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
byl	být	k5eAaImAgMnS
na	na	k7c6
osmém	osmý	k4xOgInSc6
sionistickém	sionistický	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
přijat	přijmout	k5eAaPmNgInS
jeho	jeho	k3xOp3gInSc1
plán	plán	k1gInSc1
takzvaného	takzvaný	k2eAgInSc2d1
„	„	k?
<g/>
syntetického	syntetický	k2eAgInSc2d1
sionismu	sionismus	k1gInSc2
<g/>
,	,	kIx,
<g/>
“	“	k?
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
podle	podle	k7c2
Weizmanna	Weizmanno	k1gNnSc2
průsečíkem	průsečík	k1gInSc7
politických	politický	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
a	a	k8xC
osidlování	osidlování	k1gNnSc2
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
Palestinu	Palestina	k1gFnSc4
poprvé	poprvé	k6eAd1
navštívil	navštívit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Chajim	Chajim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
a	a	k8xC
emír	emír	k1gMnSc1
Fajsal	Fajsal	k1gMnSc1
<g/>
,	,	kIx,
1918	#num#	k4
</s>
<s>
Díky	díky	k7c3
objevení	objevení	k1gNnSc3
způsobu	způsob	k1gInSc2
syntetické	syntetický	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
acetonu	aceton	k1gInSc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
kontaktu	kontakt	k1gInSc2
s	s	k7c7
předními	přední	k2eAgMnPc7d1
britskými	britský	k2eAgMnPc7d1
politiky	politik	k1gMnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
s	s	k7c7
Winstonem	Winston	k1gInSc7
Churchillem	Churchill	k1gMnSc7
<g/>
,	,	kIx,
Lloydem	Lloyd	k1gMnSc7
Georgem	Georg	k1gMnSc7
či	či	k8xC
Arthurem	Arthur	k1gMnSc7
Balfourem	Balfour	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc4
kontakty	kontakt	k1gInPc4
posléze	posléze	k6eAd1
využil	využít	k5eAaPmAgInS
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
svého	svůj	k3xOyFgNnSc2
diplomatického	diplomatický	k2eAgNnSc2d1
úsilí	úsilí	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
představení	představení	k1gNnSc3
myšlenky	myšlenka	k1gFnSc2
sionismu	sionismus	k1gInSc2
britským	britský	k2eAgMnPc3d1
politikům	politik	k1gMnPc3
a	a	k8xC
intelektuálům	intelektuál	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnPc1
snahy	snaha	k1gFnPc1
vyvrcholily	vyvrcholit	k5eAaPmAgFnP
Balfourovou	Balfourův	k2eAgFnSc7d1
deklarací	deklarace	k1gFnSc7
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
vyjádřila	vyjádřit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
podporu	podpora	k1gFnSc4
sionistickému	sionistický	k2eAgNnSc3d1
úsilí	úsilí	k1gNnSc3
v	v	k7c6
Palestině	Palestina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
deklaraci	deklarace	k1gFnSc6
bylo	být	k5eAaImAgNnS
konkrétně	konkrétně	k6eAd1
uvedeno	uvést	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
vláda	vláda	k1gFnSc1
Jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
Veličenstva	veličenstvo	k1gNnSc2
pohlíží	pohlížet	k5eAaImIp3nS
příznivě	příznivě	k6eAd1
na	na	k7c6
zřízení	zřízení	k1gNnSc6
národní	národní	k2eAgFnSc2d1
domoviny	domovina	k1gFnSc2
židovského	židovský	k2eAgInSc2d1
lidu	lid	k1gInSc2
v	v	k7c6
Palestině	Palestina	k1gFnSc6
<g/>
…	…	k?
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
byl	být	k5eAaImAgInS
britskou	britský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
předsedou	předseda	k1gMnSc7
Sionistické	sionistický	k2eAgFnSc2d1
komise	komise	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
vyslán	vyslat	k5eAaPmNgInS
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
prošetřil	prošetřit	k5eAaPmAgMnS
tamější	tamější	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
a	a	k8xC
následně	následně	k6eAd1
britským	britský	k2eAgMnPc3d1
úřadům	úřada	k1gMnPc3
podal	podat	k5eAaPmAgMnS
zprávu	zpráva	k1gFnSc4
společně	společně	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgNnSc7
doporučením	doporučení	k1gNnSc7
pro	pro	k7c4
budoucí	budoucí	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
cesty	cesta	k1gFnSc2
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
slavnostně	slavnostně	k6eAd1
položil	položit	k5eAaPmAgMnS
základní	základní	k2eAgInSc4d1
kámen	kámen	k1gInSc4
Hebrejské	hebrejský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgMnS
k	k	k7c3
bilaterálním	bilaterální	k2eAgNnPc3d1
jednáním	jednání	k1gNnPc3
s	s	k7c7
emírem	emír	k1gMnSc7
Fajsalem	Fajsal	k1gMnSc7
(	(	kIx(
<g/>
pozdějším	pozdní	k2eAgMnSc7d2
iráckým	irácký	k2eAgMnSc7d1
králem	král	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
jednání	jednání	k1gNnSc3
vyvrcholila	vyvrcholit	k5eAaPmAgFnS
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1919	#num#	k4
v	v	k7c6
podepsání	podepsání	k1gNnSc6
Fajsal-Weizmannovy	Fajsal-Weizmannův	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yQgFnSc2,k3yIgFnSc2
měl	mít	k5eAaImAgMnS
emír	emír	k1gMnSc1
podpořit	podpořit	k5eAaPmF
sionistickou	sionistický	k2eAgFnSc4d1
myšlenku	myšlenka	k1gFnSc4
výměnou	výměna	k1gFnSc7
za	za	k7c4
sionistickou	sionistický	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
arabského	arabský	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
i	i	k9
proti	proti	k7c3
vůli	vůle	k1gFnSc3
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Součástí	součást	k1gFnSc7
dohody	dohoda	k1gFnSc2
však	však	k9
byl	být	k5eAaImAgInS
i	i	k9
dodatek	dodatek	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
Fajsal	Fajsal	k1gFnSc1
nepodpoří	podpořit	k5eNaPmIp3nS
židovské	židovský	k2eAgNnSc4d1
přistěhovalectví	přistěhovalectví	k1gNnSc4
do	do	k7c2
založení	založení	k1gNnSc2
vlastního	vlastní	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohoda	dohoda	k1gFnSc1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
stala	stát	k5eAaPmAgFnS
neplatnou	platný	k2eNgFnSc7d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
nedodrželo	dodržet	k5eNaPmAgNnS
svou	svůj	k3xOyFgFnSc4
dohodu	dohoda	k1gFnSc4
s	s	k7c7
Araby	Arab	k1gMnPc7
a	a	k8xC
Sýrii	Sýrie	k1gFnSc3
ponechalo	ponechat	k5eAaPmAgNnS
Francouzům	Francouz	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Ben	Ben	k1gInSc1
Cijon	Cijon	k1gMnSc1
Morison	Morison	k1gMnSc1
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
Einstein	Einstein	k1gMnSc1
<g/>
,	,	kIx,
Chajim	Chajim	k1gMnSc1
Weizman	Weizman	k1gMnSc1
a	a	k8xC
Menachem	Menach	k1gMnSc7
Usiškin	Usiškin	k1gMnSc1
<g/>
;	;	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
1921	#num#	k4
</s>
<s>
Weizmann	Weizmann	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
na	na	k7c6
londýnské	londýnský	k2eAgFnSc6d1
sionistické	sionistický	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
zvolen	zvolit	k5eAaPmNgMnS
prezidentem	prezident	k1gMnSc7
Světové	světový	k2eAgFnSc2d1
sionistické	sionistický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
bylo	být	k5eAaImAgNnS
na	na	k7c6
konferenci	konference	k1gFnSc6
v	v	k7c6
San	San	k1gFnSc6
Remu	Remus	k1gInSc2
území	území	k1gNnSc2
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
duchu	duch	k1gMnSc6
Balfourovy	Balfourův	k2eAgFnSc2d1
deklarace	deklarace	k1gFnSc2
<g/>
,	,	kIx,
svěřeno	svěřen	k2eAgNnSc1d1
do	do	k7c2
mandátní	mandátní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
Spojenému	spojený	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
11	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
začal	začít	k5eAaPmAgInS
společně	společně	k6eAd1
s	s	k7c7
Albertem	Albert	k1gMnSc7
Einsteinem	Einstein	k1gMnSc7
shromažďovat	shromažďovat	k5eAaImF
peníze	peníz	k1gInPc4
na	na	k7c4
založení	založení	k1gNnSc4
Hebrejské	hebrejský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
společně	společně	k6eAd1
se	s	k7c7
Sigmundem	Sigmund	k1gMnSc7
Freudem	Freud	k1gInSc7
a	a	k8xC
Martinem	Martin	k1gMnSc7
Buberem	Buber	k1gMnSc7
založili	založit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
Weizmann	Weizmann	k1gMnSc1
neochotně	ochotně	k6eNd1
přijal	přijmout	k5eAaPmAgMnS
Churchillovu	Churchillův	k2eAgFnSc4d1
bílou	bílý	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
omezovala	omezovat	k5eAaImAgFnS
židovské	židovská	k1gFnPc4
přistěhovalectví	přistěhovalectví	k1gNnPc2
do	do	k7c2
mandátní	mandátní	k2eAgFnSc2d1
Palestiny	Palestina	k1gFnSc2
na	na	k7c6
„	„	k?
<g/>
ekonomickou	ekonomický	k2eAgFnSc4d1
kapacitu	kapacita	k1gFnSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
však	však	k9
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
vydalo	vydat	k5eAaPmAgNnS
Passfieldovu	Passfieldův	k2eAgFnSc4d1
bílou	bílý	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
ještě	ještě	k9
více	hodně	k6eAd2
omezující	omezující	k2eAgFnSc4d1
židovskou	židovský	k2eAgFnSc4d1
imigraci	imigrace	k1gFnSc4
<g/>
,	,	kIx,
Weizmann	Weizmann	k1gInSc4
rozčíleně	rozčíleně	k6eAd1
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
post	post	k1gInSc4
prezidenta	prezident	k1gMnSc2
Světového	světový	k2eAgInSc2d1
sionistického	sionistický	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
až	až	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
mu	on	k3xPp3gMnSc3
britský	britský	k2eAgMnSc1d1
ministerský	ministerský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Ramsay	Ramsaa	k1gFnSc2
MacDonald	Macdonald	k1gMnSc1
zaslal	zaslat	k5eAaPmAgMnS
dopis	dopis	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
opětovně	opětovně	k6eAd1
potvrdilo	potvrdit	k5eAaPmAgNnS
své	svůj	k3xOyFgInPc4
závazky	závazek	k1gInPc4
vůči	vůči	k7c3
židovské	židovský	k2eAgFnSc3d1
národní	národní	k2eAgFnSc3d1
domovině	domovina	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Weizmannův	Weizmannův	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
Rechovotu	Rechovot	k1gInSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
Weizmann	Weizmann	k1gMnSc1
neuspěl	uspět	k5eNaPmAgMnS
na	na	k7c6
sedmnáctém	sedmnáctý	k4xOgInSc6
sionistickém	sionistický	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
při	při	k7c6
obhajobě	obhajoba	k1gFnSc6
funkce	funkce	k1gFnSc2
prezidenta	prezident	k1gMnSc2
Světové	světový	k2eAgFnSc2d1
sionistické	sionistický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
následujících	následující	k2eAgNnPc2d1
čtyř	čtyři	k4xCgNnPc2
let	léto	k1gNnPc2
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
své	svůj	k3xOyFgNnSc4
úsilí	úsilí	k1gNnSc4
především	především	k9
vědě	věda	k1gFnSc3
<g/>
,	,	kIx,
avšak	avšak	k8xC
i	i	k9
nadále	nadále	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
podpoře	podpora	k1gFnSc6
sionistické	sionistický	k2eAgFnSc2d1
myšlenky	myšlenka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podnikal	podnikat	k5eAaImAgMnS
fundraisingové	fundraisingový	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
po	po	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
a	a	k8xC
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
rovněž	rovněž	k9
se	se	k3xPyFc4
zasazoval	zasazovat	k5eAaImAgInS
o	o	k7c4
záchranu	záchrana	k1gFnSc4
židovských	židovský	k2eAgMnPc2d1
uprchlíků	uprchlík	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
napomohl	napomoct	k5eAaPmAgMnS
založení	založení	k1gNnSc4
Institutu	institut	k1gInSc2
Daniela	Daniel	k1gMnSc2
Sieffa	Sieff	k1gMnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Rechovot	Rechovot	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
finančně	finančně	k6eAd1
podporován	podporovat	k5eAaImNgMnS
baronem	baron	k1gMnSc7
Jisra	Jisra	k1gMnSc1
<g/>
'	'	kIx"
<g/>
elem	elem	k1gMnSc1
Sieffem	Sieff	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
jej	on	k3xPp3gMnSc4
pojmenoval	pojmenovat	k5eAaPmAgMnS
po	po	k7c6
svém	svůj	k3xOyFgMnSc6
zesnulém	zesnulý	k1gMnSc6
synovi	syn	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vědeckého	vědecký	k2eAgInSc2d1
institutu	institut	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
zaměřoval	zaměřovat	k5eAaImAgInS
zejména	zejména	k9
na	na	k7c4
organickou	organický	k2eAgFnSc4d1
chemii	chemie	k1gFnSc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
zváni	zván	k2eAgMnPc1d1
židovští	židovský	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
přestěhoval	přestěhovat	k5eAaPmAgMnS
do	do	k7c2
domu	dům	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
si	se	k3xPyFc3
nechal	nechat	k5eAaPmAgInS
postavit	postavit	k5eAaPmF
v	v	k7c6
areálu	areál	k1gInSc6
institutu	institut	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
se	se	k3xPyFc4
opět	opět	k6eAd1
stal	stát	k5eAaPmAgMnS
prezidentem	prezident	k1gMnSc7
Světové	světový	k2eAgFnSc2d1
sionistické	sionistický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
zastával	zastávat	k5eAaImAgMnS
umírněnou	umírněný	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
vůči	vůči	k7c3
Arabům	Arab	k1gMnPc3
a	a	k8xC
podporoval	podporovat	k5eAaImAgInS
politickou	politický	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
bral	brát	k5eAaImAgMnS
v	v	k7c4
úvahu	úvaha	k1gFnSc4
demografické	demografický	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
představila	představit	k5eAaPmAgFnS
Peelova	Peelův	k2eAgFnSc1d1
komise	komise	k1gFnSc1
plán	plán	k1gInSc4
na	na	k7c4
rozdělení	rozdělení	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
mezi	mezi	k7c7
Židy	Žid	k1gMnPc7
a	a	k8xC
Araby	Arab	k1gMnPc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
sice	sice	k8xC
nebyl	být	k5eNaImAgMnS
podle	podle	k7c2
sionistických	sionistický	k2eAgFnPc2d1
představ	představa	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
Weizmann	Weizmann	k1gInSc1
podporoval	podporovat	k5eAaImAgInS
se	s	k7c7
slovy	slovo	k1gNnPc7
„	„	k?
<g/>
lepší	dobrý	k2eAgFnSc1d2
polovina	polovina	k1gFnSc1
bochníku	bochník	k1gInSc2
než	než	k8xS
žádný	žádný	k1gMnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
byla	být	k5eAaImAgFnS
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
přijata	přijmout	k5eAaPmNgFnS
MacDonaldova	Macdonaldův	k2eAgFnSc1d1
bílá	bílý	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
výrazně	výrazně	k6eAd1
omezila	omezit	k5eAaPmAgFnS
počet	počet	k1gInSc4
židovských	židovský	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
něž	jenž	k3xRgNnSc4
zavedla	zavést	k5eAaPmAgFnS
kvótu	kvóta	k1gFnSc4
75	#num#	k4
tisíc	tisíc	k4xCgInPc2
osob	osoba	k1gFnPc2
do	do	k7c2
roku	rok	k1gInSc2
1944	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
přimělo	přimět	k5eAaPmAgNnS
Weizmanna	Weizmanno	k1gNnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
Londýně	Londýn	k1gInSc6
a	a	k8xC
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
veškeré	veškerý	k3xTgNnSc4
své	svůj	k3xOyFgNnSc4
úsilí	úsilí	k1gNnSc4
ke	k	k7c3
změně	změna	k1gFnSc3
nepřátelské	přátelský	k2eNgFnSc2d1
britské	britský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
této	tento	k3xDgFnSc3
politice	politika	k1gFnSc3
se	se	k3xPyFc4
totiž	totiž	k9
Weizmann	Weizmann	k1gMnSc1
stále	stále	k6eAd1
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
židovské	židovský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
domoviny	domovina	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dosaženo	dosáhnout	k5eAaPmNgNnS
pouze	pouze	k6eAd1
za	za	k7c4
přispění	přispění	k1gNnSc4
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
umírněný	umírněný	k2eAgInSc1d1
a	a	k8xC
tolerantní	tolerantní	k2eAgInSc1d1
postoj	postoj	k1gInSc1
vůči	vůči	k7c3
Britům	Brit	k1gMnPc3
však	však	k8xC
vzbudil	vzbudit	k5eAaPmAgInS
odpor	odpor	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
sionistického	sionistický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
a	a	k8xC
zejména	zejména	k9
pak	pak	k6eAd1
mezi	mezi	k7c4
Židy	Žid	k1gMnPc4
v	v	k7c6
jišuvu	jišuv	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
zasazoval	zasazovat	k5eAaImAgInS
o	o	k7c6
vytvoření	vytvoření	k1gNnSc6
židovské	židovský	k2eAgFnSc2d1
brigády	brigáda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
nakonec	nakonec	k6eAd1
vytvořena	vytvořit	k5eAaPmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
britských	britský	k2eAgFnPc2d1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1944	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1942	#num#	k4
odletěl	odletět	k5eAaPmAgMnS
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pomohl	pomoct	k5eAaPmAgMnS
vyřešit	vyřešit	k5eAaPmF
problém	problém	k1gInSc4
s	s	k7c7
výrobou	výroba	k1gFnSc7
syntetické	syntetický	k2eAgFnSc2d1
pryže	pryž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
před	před	k7c7
odletem	odlet	k1gInSc7
se	se	k3xPyFc4
dozvěděl	dozvědět	k5eAaPmAgMnS
o	o	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
Michaela	Michael	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
jako	jako	k8xS,k8xC
pilot	pilot	k1gMnSc1
britského	britský	k2eAgNnSc2d1
Královského	královský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
sestřelen	sestřelen	k2eAgInSc4d1
nad	nad	k7c7
Biskajským	biskajský	k2eAgInSc7d1
zálivem	záliv	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
před	před	k7c7
Biltmorskou	Biltmorský	k2eAgFnSc7d1
konferencí	konference	k1gFnSc7
projevily	projevit	k5eAaPmAgInP
názorové	názorový	k2eAgInPc1d1
rozpory	rozpor	k1gInPc1
mezi	mezi	k7c7
Weizmannem	Weizmann	k1gMnSc7
předsedajícím	předsedající	k1gMnSc7
Světové	světový	k2eAgFnSc3d1
sionistické	sionistický	k2eAgFnSc3d1
organizaci	organizace	k1gFnSc3
a	a	k8xC
Davidem	David	k1gMnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
Ben	Ben	k1gInSc4
Gurionem	Gurion	k1gInSc7
předsedajícím	předsedající	k2eAgInSc7d1
Židovské	židovský	k2eAgFnPc1d1
agentuře	agentura	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Weizmann	Weizmann	k1gMnSc1
doufal	doufat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jednání	jednání	k1gNnPc1
s	s	k7c7
Brity	Brit	k1gMnPc7
povedou	povést	k5eAaPmIp3nP,k5eAaImIp3nP
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
židovské	židovský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
zrušení	zrušení	k1gNnSc4
omezení	omezení	k1gNnSc2
způsobených	způsobený	k2eAgInPc2d1
MacDonaldovou	Macdonaldův	k2eAgFnSc7d1
bílou	bílý	k2eAgFnSc7d1
knihou	kniha	k1gFnSc7
a	a	k8xC
za	za	k7c4
každou	každý	k3xTgFnSc4
cenu	cena	k1gFnSc4
se	se	k3xPyFc4
chtěl	chtít	k5eAaImAgMnS
vyhnout	vyhnout	k5eAaPmF
konfrontaci	konfrontace	k1gFnSc4
s	s	k7c7
britskou	britský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Ben	Ben	k1gInSc1
Gurion	Gurion	k1gInSc1
naproti	naproti	k7c3
tomu	ten	k3xDgMnSc3
zastával	zastávat	k5eAaImAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
Židé	Žid	k1gMnPc1
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
otázce	otázka	k1gFnSc6
zřízení	zřízení	k1gNnSc1
národní	národní	k2eAgFnSc2d1
domoviny	domovina	k1gFnSc2
nemohou	moct	k5eNaImIp3nP
opírat	opírat	k5eAaImF
o	o	k7c4
Spojené	spojený	k2eAgNnSc4d1
království	království	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
by	by	kYmCp3nS
Židovská	židovský	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
měla	mít	k5eAaImAgFnS
v	v	k7c6
Palestině	Palestina	k1gFnSc6
vládnout	vládnout	k5eAaImF
namísto	namísto	k7c2
mandátní	mandátní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Weizmann	Weizmann	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
v	v	k7c6
mandátu	mandát	k1gInSc6
nežil	žít	k5eNaImAgMnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
navíc	navíc	k6eAd1
odtržen	odtrhnout	k5eAaPmNgMnS
od	od	k7c2
reálné	reálný	k2eAgFnSc2d1
situace	situace	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
vliv	vliv	k1gInSc1
v	v	k7c6
sionistických	sionistický	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
postupně	postupně	k6eAd1
slábl	slábnout	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
projevilo	projevit	k5eAaPmAgNnS
na	na	k7c4
22	#num#	k4
<g/>
.	.	kIx.
sionistickém	sionistický	k2eAgInSc6d1
kongresu	kongres	k1gInSc6
1946	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nebyl	být	k5eNaImAgMnS
opětovně	opětovně	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
1944	#num#	k4
Weizmann	Weizmann	k1gMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
britskému	britský	k2eAgNnSc3d1
ministerstvu	ministerstvo	k1gNnSc3
zahraničí	zahraničí	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Královské	královský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
bombardovalo	bombardovat	k5eAaImAgNnS
plynové	plynový	k2eAgFnPc4d1
komory	komora	k1gFnPc4
koncentračního	koncentrační	k2eAgInSc2d1
tábora	tábor	k1gInSc2
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
září	září	k1gNnSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
mu	on	k3xPp3gMnSc3
však	však	k9
bylo	být	k5eAaImAgNnS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
Královské	královský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
návrh	návrh	k1gInSc4
z	z	k7c2
„	„	k?
<g/>
technických	technický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
“	“	k?
zamítlo	zamítnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1947	#num#	k4
ustavilo	ustavit	k5eAaPmAgNnS
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
OSN	OSN	kA
zvláštní	zvláštní	k2eAgFnSc4d1
komisi	komise	k1gFnSc4
pro	pro	k7c4
Palestinu	Palestina	k1gFnSc4
(	(	kIx(
<g/>
UNSCOP	UNSCOP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
následně	následně	k6eAd1
odcestovala	odcestovat	k5eAaPmAgFnS
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
a	a	k8xC
po	po	k7c6
pěti	pět	k4xCc6
dnech	den	k1gInPc6
vypracovala	vypracovat	k5eAaPmAgFnS
zprávu	zpráva	k1gFnSc4
vyzývající	vyzývající	k2eAgFnSc4d1
k	k	k7c3
ukončení	ukončení	k1gNnSc3
britského	britský	k2eAgInSc2d1
mandátu	mandát	k1gInSc2
a	a	k8xC
rozdělení	rozdělení	k1gNnSc4
země	zem	k1gFnSc2
na	na	k7c4
židovský	židovský	k2eAgInSc4d1
a	a	k8xC
arabský	arabský	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1947	#num#	k4
schválilo	schválit	k5eAaPmAgNnS
Valné	valný	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
plán	plán	k1gInSc1
výboru	výbor	k1gInSc3
na	na	k7c4
rozdělení	rozdělení	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
oznámilo	oznámit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
správu	správa	k1gFnSc4
nad	nad	k7c7
mandátem	mandát	k1gInSc7
ukončí	ukončit	k5eAaPmIp3nS
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovští	židovský	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
požádali	požádat	k5eAaPmAgMnP
Chajima	Chajim	k1gMnSc4
Weizmanna	Weizmann	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
do	do	k7c2
USA	USA	kA
přijel	přijet	k5eAaPmAgMnS
a	a	k8xC
pokusil	pokusit	k5eAaPmAgMnS
se	se	k3xPyFc4
přesvědčit	přesvědčit	k5eAaPmF
prezidenta	prezident	k1gMnSc2
Harryho	Harry	k1gMnSc2
S.	S.	kA
Trumana	Truman	k1gMnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
podpořil	podpořit	k5eAaPmAgInS
vznik	vznik	k1gInSc4
Izraele	Izrael	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
velkém	velký	k2eAgNnSc6d1
úsilí	úsilí	k1gNnSc6
byl	být	k5eAaImAgMnS
Weizman	Weizman	k1gMnSc1
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1948	#num#	k4
přijat	přijmout	k5eAaPmNgMnS
v	v	k7c6
Bílém	bílý	k2eAgInSc6d1
domě	dům	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
Weizmannovi	Weizmann	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
Trumana	Truman	k1gMnSc4
přesvědčit	přesvědčit	k5eAaPmF
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
uznaly	uznat	k5eAaPmAgInP
nezávislost	nezávislost	k1gFnSc4
Izraele	Izrael	k1gInSc2
bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
jejím	její	k3xOp3gNnSc6
vyhlášení	vyhlášení	k1gNnSc6
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Nadto	nadto	k6eAd1
se	se	k3xPyFc4
Weizmannovi	Weizmann	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
získal	získat	k5eAaPmAgMnS
půjčku	půjčka	k1gFnSc4
od	od	k7c2
americké	americký	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
100	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezidentství	prezidentství	k1gNnSc1
</s>
<s>
Weizmannova	Weizmannův	k2eAgFnSc1d1
inaugurace	inaugurace	k1gFnSc1
v	v	k7c6
Knesetu	Kneset	k1gInSc6
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1949	#num#	k4
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1948	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
předsedou	předseda	k1gMnSc7
Prozatímní	prozatímní	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
a	a	k8xC
v	v	k7c6
únoru	únor	k1gInSc6
1949	#num#	k4
<g/>
,	,	kIx,
měsíc	měsíc	k1gInSc4
po	po	k7c6
prvních	první	k4xOgFnPc6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
stranou	stranou	k6eAd1
Mapaj	Mapaj	k1gInSc1
nominován	nominován	k2eAgInSc1d1
do	do	k7c2
prezidentských	prezidentský	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
utkal	utkat	k5eAaPmAgMnS
s	s	k7c7
Josefem	Josef	k1gMnSc7
Klausnerem	Klausner	k1gMnSc7
nominovaným	nominovaný	k2eAgMnSc7d1
Cherutem	Cherut	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnPc4
Weizmann	Weizmann	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
v	v	k7c6
poměru	poměr	k1gInSc6
83	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
a	a	k8xC
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
tak	tak	k9
prvním	první	k4xOgMnSc7
izraelským	izraelský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Slavnostní	slavnostní	k2eAgFnSc4d1
přísahu	přísaha	k1gFnSc4
složil	složit	k5eAaPmAgMnS
v	v	k7c6
Knesetu	Kneset	k1gInSc6
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
byl	být	k5eAaImAgInS
také	také	k9
Sieffův	Sieffův	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
se	s	k7c7
souhlasem	souhlas	k1gInSc7
Sieffovy	Sieffův	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
přejmenován	přejmenován	k2eAgInSc4d1
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
na	na	k7c4
Weizmannův	Weizmannův	k2eAgInSc4d1
institut	institut	k1gInSc4
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
1949	#num#	k4
navštívil	navštívit	k5eAaPmAgInS
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gMnSc4
přivítaly	přivítat	k5eAaPmAgInP
velké	velký	k2eAgInPc1d1
davy	dav	k1gInPc1
lidí	člověk	k1gMnPc2
a	a	k8xC
při	při	k7c6
této	tento	k3xDgFnSc6
cestě	cesta	k1gFnSc6
převzal	převzít	k5eAaPmAgInS
výtěžek	výtěžek	k1gInSc1
z	z	k7c2
veřejné	veřejný	k2eAgFnSc2d1
sbírky	sbírka	k1gFnSc2
pro	pro	k7c4
Izrael	Izrael	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
činil	činit	k5eAaImAgInS
23	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hrob	hrob	k1gInSc1
Chajima	Chajim	k1gMnSc2
Weizmanna	Weizmann	k1gMnSc2
</s>
<s>
Weizmann	Weizmann	k1gInSc1
byl	být	k5eAaImAgInS
zklamán	zklamat	k5eAaPmNgInS
ceremoniálností	ceremoniálnost	k1gFnSc7
prezidentského	prezidentský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
však	však	k9
byl	být	k5eAaImAgInS
aktivním	aktivní	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
a	a	k8xC
díky	díky	k7c3
svým	svůj	k3xOyFgInPc3
dobrým	dobrý	k2eAgInPc3d1
vztahům	vztah	k1gInPc3
s	s	k7c7
britskými	britský	k2eAgMnPc7d1
politiky	politik	k1gMnPc7
pomohl	pomoct	k5eAaPmAgInS
dosáhnout	dosáhnout	k5eAaPmF
oficiálního	oficiální	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
Izraele	Izrael	k1gInSc2
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
de	de	k?
facto	facto	k1gNnSc4
již	již	k6eAd1
v	v	k7c6
lednu	leden	k1gInSc6
1949	#num#	k4
<g/>
,	,	kIx,
de	de	k?
iure	iurat	k5eAaPmIp3nS
pak	pak	k6eAd1
v	v	k7c6
dubnu	duben	k1gInSc6
1950	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
vážně	vážně	k6eAd1
onemocněl	onemocnět	k5eAaPmAgMnS
a	a	k8xC
musel	muset	k5eAaImAgMnS
omezit	omezit	k5eAaPmF
svou	svůj	k3xOyFgFnSc4
aktivitu	aktivita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
I	i	k8xC
nadále	nadále	k6eAd1
však	však	k9
přijímal	přijímat	k5eAaImAgMnS
zahraniční	zahraniční	k2eAgFnPc4d1
návštěvy	návštěva	k1gFnPc4
a	a	k8xC
sledoval	sledovat	k5eAaImAgMnS
aktuální	aktuální	k2eAgNnSc4d1
dění	dění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1951	#num#	k4
byl	být	k5eAaImAgMnS
opětovně	opětovně	k6eAd1
zvolen	zvolit	k5eAaPmNgMnS
prezidentem	prezident	k1gMnSc7
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
funkční	funkční	k2eAgNnSc1d1
období	období	k1gNnSc1
prezidenta	prezident	k1gMnSc2
závislé	závislý	k2eAgFnSc2d1
na	na	k7c6
funkčním	funkční	k2eAgNnSc6d1
období	období	k1gNnSc6
Knesetu	Kneset	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
prezidentskou	prezidentský	k2eAgFnSc4d1
přísahu	přísaha	k1gFnSc4
již	již	k6eAd1
nesložil	složit	k5eNaPmAgMnS
v	v	k7c6
Knesetu	Kneset	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
domě	dům	k1gInSc6
v	v	k7c6
Rechovotu	Rechovot	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
po	po	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
a	a	k8xC
bolestivé	bolestivý	k2eAgFnSc6d1
nemoci	nemoc	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
a	a	k8xC
na	na	k7c4
vlastní	vlastní	k2eAgNnPc4d1
přání	přání	k1gNnPc4
byl	být	k5eAaImAgMnS
pochován	pochován	k2eAgMnSc1d1
na	na	k7c6
zahradě	zahrada	k1gFnSc6
u	u	k7c2
svého	svůj	k3xOyFgInSc2
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
státního	státní	k2eAgInSc2d1
pohřbu	pohřeb	k1gInSc2
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
přes	přes	k7c4
čtvrt	čtvrt	k1xP,k1gFnSc4,k1gFnSc1
milionu	milion	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
prostý	prostý	k2eAgInSc1d1
hrob	hrob	k1gInSc1
ročně	ročně	k6eAd1
navštíví	navštívit	k5eAaPmIp3nS
stovky	stovka	k1gFnPc1
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
krocích	krok	k1gInPc6
svého	svůj	k1gMnSc4
strýce	strýc	k1gMnSc4
pokračoval	pokračovat	k5eAaImAgMnS
i	i	k9
Weizmannův	Weizmannův	k2eAgMnSc1d1
synovec	synovec	k1gMnSc1
Ezer	Ezer	k1gMnSc1
Weizman	Weizman	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
v	v	k7c6
letech	léto	k1gNnPc6
1993	#num#	k4
až	až	k9
2000	#num#	k4
stal	stát	k5eAaPmAgMnS
sedmým	sedmý	k4xOgMnSc7
izraelským	izraelský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Literární	literární	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
</s>
<s>
WEIZMANN	WEIZMANN	kA
<g/>
,	,	kIx,
Chajim	Chajim	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trial	trial	k1gInSc1
and	and	k?
Error	Error	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Autobiography	Autobiographa	k1gFnSc2
of	of	k?
Chaim	Chaim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Westport	Westport	k1gInSc1
<g/>
:	:	kIx,
Greenwood	Greenwood	k1gInSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
498	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
837161662	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
SACHAR	SACHAR	kA
<g/>
,	,	kIx,
Howard	Howard	k1gMnSc1
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Regia	Regia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902484	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
110	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
SACHAR	SACHAR	kA
<g/>
,	,	kIx,
H.	H.	kA
<g/>
,	,	kIx,
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Chaim	Chaim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopæ	Encyclopæ	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chaim	Chaim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
the	the	k?
Scientist	Scientist	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Weizmannův	Weizmannův	k2eAgInSc1d1
institut	institut	k1gInSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
2007-03-11	2007-03-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Weizmann	Weizmanna	k1gFnPc2
<g/>
,	,	kIx,
Chaim	Chaim	k1gMnSc1
(	(	kIx(
<g/>
1874	#num#	k4
<g/>
-	-	kIx~
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovská	židovský	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SACHAR	SACHAR	kA
<g/>
,	,	kIx,
H.	H.	kA
<g/>
,	,	kIx,
M.	M.	kA
Dějiny	dějiny	k1gFnPc1
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
761	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Chaim	Chaim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
<g/>
:	:	kIx,
First	First	k1gMnSc1
President	president	k1gMnSc1
of	of	k?
the	the	k?
State	status	k1gInSc5
of	of	k?
Israel	Israel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřad	úřad	k1gInSc1
prezidenta	prezident	k1gMnSc4
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Synthetic	Synthetice	k1gFnPc2
Zionism	Zionism	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jewish	Jewish	k1gInSc1
Virtual	Virtual	k1gInSc4
Library	Librara	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Chaim	Chaim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ČEJKA	Čejka	k1gMnSc1
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc4
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Barrister	Barrister	k1gMnSc1
&	&	k?
Principal	Principal	k1gMnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87029	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
36	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Chaim	Chaim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jewish	Jewish	k1gInSc1
Virtual	Virtual	k1gInSc4
Library	Librara	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pardubice	Pardubice	k1gInPc4
<g/>
:	:	kIx,
Kora	Kora	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901092	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
52	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Hebrejská	hebrejský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
mezi	mezi	k7c7
nejlepšími	dobrý	k2eAgFnPc7d3
univerzitami	univerzita	k1gFnPc7
světa	svět	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eretz	Eretz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2006-09-14	2006-09-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BLAUSTEIN	BLAUSTEIN	kA
<g/>
,	,	kIx,
Max	max	kA
<g/>
.	.	kIx.
Židovská	židovský	k2eAgFnSc1d1
historie	historie	k1gFnSc1
<g/>
:	:	kIx,
Britské	britský	k2eAgNnSc1d1
mandátní	mandátní	k2eAgNnSc1d1
území	území	k1gNnSc1
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Britské	britský	k2eAgNnSc1d1
administrativní	administrativní	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
Mandátu	mandát	k1gInSc2
na	na	k7c4
Transjordánsko	Transjordánsko	k1gNnSc4
a	a	k8xC
zbytkovou	zbytkový	k2eAgFnSc4d1
Palestinu	Palestina	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eretz	Eretz	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2006-01-18	2006-01-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
s.	s.	k?
61	#num#	k4
<g/>
↑	↑	k?
GILBERT	Gilbert	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
Art	Art	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
82	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7257	#num#	k4
<g/>
-	-	kIx~
<g/>
740	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
s.	s.	k?
108	#num#	k4
<g/>
-	-	kIx~
<g/>
109	#num#	k4
<g/>
↑	↑	k?
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
s.	s.	k?
66	#num#	k4
<g/>
↑	↑	k?
Casualty	Casualt	k1gInPc1
Details	Details	k1gInSc1
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Oser	Oser	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Commonwealth	Commonwealth	k1gMnSc1
War	War	k1gMnSc1
Graves	Graves	k1gMnSc1
Commision	Commision	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
s.	s.	k?
60	#num#	k4
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
s.	s.	k?
123	#num#	k4
<g/>
↑	↑	k?
TERNER	TERNER	kA
<g/>
,	,	kIx,
Erich	Erich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
<g/>
.	.	kIx.
s.	s.	k?
70	#num#	k4
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
a	a	k8xC
Palestina	Palestina	k1gFnSc1
-	-	kIx~
Minulost	minulost	k1gFnSc1
<g/>
,	,	kIx,
současnost	současnost	k1gFnSc1
a	a	k8xC
směřování	směřování	k1gNnSc1
blízkovýchodního	blízkovýchodní	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
s.	s.	k?
67	#num#	k4
<g/>
↑	↑	k?
Izrael	Izrael	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
<g/>
.	.	kIx.
s.	s.	k?
256	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Weizmannův	Weizmannův	k2eAgInSc1d1
institut	institut	k1gInSc1
věd	věda	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chajim	Chajima	k1gFnPc2
Weizmann	Weizmann	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Chajim	Chajimo	k1gNnPc2
Weizmann	Weizmanno	k1gNnPc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Weizmannova	Weizmannův	k2eAgInSc2d1
institutu	institut	k1gInSc2
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Kneset	Kneset	k1gMnSc1
-	-	kIx~
Chajim	Chajim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Jewishmag	Jewishmag	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
-	-	kIx~
Chajim	Chajim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hlavy	hlava	k1gFnSc2
Státu	stát	k1gInSc2
Izrael	Izrael	k1gInSc1
Předsedové	předseda	k1gMnPc1
prozatímní	prozatímní	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
David	David	k1gMnSc1
Ben	Ben	k1gInSc4
Gurion	Gurion	k1gInSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chajim	Chajim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
Standarta	standarta	k1gFnSc1
prezidenta	prezident	k1gMnSc2
Státu	stát	k1gInSc2
Izrael	Izrael	k1gMnSc1
Prezidenti	prezident	k1gMnPc1
</s>
<s>
Chajim	Chajim	k1gMnSc1
Weizmann	Weizmann	k1gMnSc1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jicchak	Jicchak	k1gInSc1
Ben	Ben	k1gInSc1
Cvi	Cvi	k1gFnSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zalman	Zalman	k1gMnSc1
Šazar	Šazar	k1gMnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Efrajim	Efrajim	k1gMnSc1
Kacir	Kacir	k1gMnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jicchak	Jicchak	k1gMnSc1
Navon	Navon	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chajim	Chajim	k1gMnSc1
Herzog	Herzog	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ezer	Ezer	k1gMnSc1
Weizman	Weizman	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Moše	mocha	k1gFnSc3
Kacav	Kacava	k1gFnPc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Šimon	Šimon	k1gMnSc1
Peres	Peres	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Re	re	k9
<g/>
'	'	kIx"
<g/>
uven	uven	k1gNnSc1
Rivlin	Rivlina	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2002157572	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118630709	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0907	#num#	k4
8777	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50003498	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
61778920	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50003498	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
