<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
27	[number]	k4	27
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
obsahem	obsah	k1gInSc7	obsah
je	být	k5eAaImIp3nS	být
působení	působení	k1gNnSc1	působení
a	a	k8xC	a
kázání	kázání	k1gNnSc1	kázání
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazareta	Nazaret	k1gInSc2	Nazaret
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
jakožto	jakožto	k8xS	jakožto
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
Ježíše	Ježíš	k1gMnSc4	Ježíš
Krista	Kristus	k1gMnSc4	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
spisy	spis	k1gInPc4	spis
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
50	[number]	k4	50
a	a	k8xC	a
150	[number]	k4	150
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
napsány	napsat	k5eAaPmNgInP	napsat
řecky	řecky	k6eAd1	řecky
a	a	k8xC	a
shromážděny	shromážděn	k2eAgFnPc1d1	shromážděna
jako	jako	k8xS	jako
závazný	závazný	k2eAgInSc1d1	závazný
soubor	soubor	k1gInSc1	soubor
(	(	kIx(	(
<g/>
kánon	kánon	k1gInSc1	kánon
<g/>
)	)	kIx)	)
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k9	už
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
několika	několik	k4yIc2	několik
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Nový	nový	k2eAgInSc4d1	nový
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
hé	hé	k0	hé
kainé	kainá	k1gFnSc6	kainá
diathéké	diathéká	k1gFnPc4	diathéká
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Novum	novum	k1gNnSc1	novum
testamentum	testamentum	k1gNnSc1	testamentum
<g/>
,	,	kIx,	,
jej	on	k3xPp3gNnSc4	on
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
židovské	židovský	k2eAgFnSc2d1	židovská
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
v	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
pojetí	pojetí	k1gNnSc6	pojetí
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zřejmě	zřejmě	k6eAd1	zřejmě
navazuje	navazovat	k5eAaImIp3nS	navazovat
už	už	k6eAd1	už
užitím	užití	k1gNnSc7	užití
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
posunutý	posunutý	k2eAgInSc4d1	posunutý
význam	význam	k1gInSc4	význam
a	a	k8xC	a
původně	původně	k6eAd1	původně
znamenalo	znamenat	k5eAaImAgNnS	znamenat
něco	něco	k3yInSc1	něco
jako	jako	k9	jako
"	"	kIx"	"
<g/>
odkaz	odkaz	k1gInSc1	odkaz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
latinskému	latinský	k2eAgNnSc3d1	latinské
Novum	novum	k1gNnSc1	novum
testamentum	testamentum	k1gNnSc1	testamentum
i	i	k8xC	i
ruskému	ruský	k2eAgMnSc3d1	ruský
Novyj	Novyj	k1gInSc4	Novyj
zavět	zavět	k5eAaPmF	zavět
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejské	hebrejský	k2eAgInPc1d1	hebrejský
berit	berit	k1gInSc1	berit
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
diathéké	diathéká	k1gFnSc2	diathéká
a	a	k8xC	a
testamentum	testamentum	k1gNnSc1	testamentum
překládalo	překládat	k5eAaImAgNnS	překládat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
znamenat	znamenat	k5eAaImF	znamenat
také	také	k9	také
"	"	kIx"	"
<g/>
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
chápání	chápání	k1gNnSc6	chápání
tak	tak	k8xS	tak
název	název	k1gInSc1	název
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
naznačoval	naznačovat	k5eAaImAgMnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
závaznou	závazný	k2eAgFnSc4d1	závazná
smlouvu	smlouva	k1gFnSc4	smlouva
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Der	drát	k5eAaImRp2nS	drát
neue	neue	k1gNnSc4	neue
Bund	bund	k1gInSc1	bund
<g/>
,	,	kIx,	,
také	také	k9	také
Český	český	k2eAgInSc1d1	český
studijní	studijní	k2eAgInSc1d1	studijní
překlad	překlad	k1gInSc1	překlad
překládá	překládat	k5eAaImIp3nS	překládat
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc1	její
strany	strana	k1gFnPc1	strana
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
rovnoprávné	rovnoprávný	k2eAgFnPc1d1	rovnoprávná
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
iniciativa	iniciativa	k1gFnSc1	iniciativa
vychází	vycházet	k5eAaImIp3nS	vycházet
jednoznačně	jednoznačně	k6eAd1	jednoznačně
od	od	k7c2	od
Hospodina	Hospodin	k1gMnSc2	Hospodin
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
biblický	biblický	k2eAgInSc1d1	biblický
zákon	zákon	k1gInSc1	zákon
podobá	podobat	k5eAaImIp3nS	podobat
odkazu	odkaz	k1gInSc3	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
Zjevení	zjevení	k1gNnSc4	zjevení
Božím	božit	k5eAaImIp1nS	božit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
zjevení	zjevení	k1gNnSc4	zjevení
však	však	k9	však
člověka	člověk	k1gMnSc4	člověk
zároveň	zároveň	k6eAd1	zároveň
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
jako	jako	k9	jako
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
:	:	kIx,	:
Čtyři	čtyři	k4xCgNnPc4	čtyři
evangelia	evangelium	k1gNnPc4	evangelium
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
líčí	líčit	k5eAaImIp3nP	líčit
kázání	kázání	k1gNnSc4	kázání
a	a	k8xC	a
působení	působení	k1gNnSc4	působení
Ježíše	Ježíš	k1gMnSc2	Ježíš
z	z	k7c2	z
Nazareta	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc2	jeho
umučení	umučení	k1gNnSc2	umučení
a	a	k8xC	a
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
<g/>
.	.	kIx.	.
</s>
<s>
Evangelia	evangelium	k1gNnPc1	evangelium
podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
<g/>
,	,	kIx,	,
Marka	Marek	k1gMnSc2	Marek
a	a	k8xC	a
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
obsahem	obsah	k1gInSc7	obsah
i	i	k8xC	i
líčením	líčení	k1gNnSc7	líčení
bližší	blízký	k2eAgInSc1d2	bližší
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
synoptická	synoptický	k2eAgNnPc1d1	synoptické
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
65	[number]	k4	65
až	až	k9	až
80	[number]	k4	80
na	na	k7c6	na
základě	základ	k1gInSc6	základ
staršího	starý	k2eAgNnSc2d2	starší
podání	podání	k1gNnSc2	podání
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nejmladší	mladý	k2eAgNnSc4d3	nejmladší
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
o	o	k7c4	o
dataci	datace	k1gFnSc4	datace
novozákonních	novozákonní	k2eAgFnPc2d1	novozákonní
knih	kniha	k1gFnPc2	kniha
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
debaty	debata	k1gFnPc1	debata
a	a	k8xC	a
názory	názor	k1gInPc1	názor
nejsou	být	k5eNaImIp3nP	být
jednotné	jednotný	k2eAgMnPc4d1	jednotný
<g/>
.	.	kIx.	.
</s>
<s>
Skutky	skutek	k1gInPc1	skutek
apoštolů	apoštol	k1gMnPc2	apoštol
popisují	popisovat	k5eAaImIp3nP	popisovat
vznik	vznik	k1gInSc4	vznik
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
Církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
jejího	její	k3xOp3gNnSc2	její
šíření	šíření	k1gNnSc2	šíření
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
misijní	misijní	k2eAgFnSc1d1	misijní
činnost	činnost	k1gFnSc1	činnost
apoštola	apoštol	k1gMnSc2	apoštol
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
21	[number]	k4	21
dopisů	dopis	k1gInPc2	dopis
(	(	kIx(	(
<g/>
epištol	epištola	k1gFnPc2	epištola
<g/>
)	)	kIx)	)
a	a	k8xC	a
kratších	krátký	k2eAgInPc2d2	kratší
textů	text	k1gInPc2	text
z	z	k7c2	z
první	první	k4xOgFnSc2	první
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
13	[number]	k4	13
listů	list	k1gInPc2	list
apoštola	apoštol	k1gMnSc2	apoštol
Pavla	Pavel	k1gMnSc2	Pavel
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
církevním	církevní	k2eAgFnPc3d1	církevní
obcím	obec	k1gFnPc3	obec
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
představeným	představený	k1gMnPc3	představený
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
51	[number]	k4	51
až	až	k9	až
100	[number]	k4	100
<g/>
.	.	kIx.	.
8	[number]	k4	8
listů	list	k1gInPc2	list
či	či	k8xC	či
kázání	kázání	k1gNnPc2	kázání
<g/>
,	,	kIx,	,
připisovaných	připisovaný	k2eAgNnPc2d1	připisované
apoštolům	apoštol	k1gMnPc3	apoštol
Pavlovi	Pavel	k1gMnSc3	Pavel
<g/>
,	,	kIx,	,
Petrovi	Petr	k1gMnSc3	Petr
<g/>
,	,	kIx,	,
Jakubovi	Jakub	k1gMnSc3	Jakub
a	a	k8xC	a
Janovi	Jan	k1gMnSc3	Jan
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
nemají	mít	k5eNaImIp3nP	mít
určitého	určitý	k2eAgMnSc4d1	určitý
adresáta	adresát	k1gMnSc4	adresát
a	a	k8xC	a
nazývají	nazývat	k5eAaImIp3nP	nazývat
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
obecné	obecný	k2eAgInPc1d1	obecný
či	či	k8xC	či
katolické	katolický	k2eAgInPc1d1	katolický
<g/>
.	.	kIx.	.
</s>
<s>
Pocházejí	pocházet	k5eAaImIp3nP	pocházet
patrně	patrně	k6eAd1	patrně
z	z	k7c2	z
konce	konec	k1gInSc2	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
začátku	začátek	k1gInSc2	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
Zjevení	zjevení	k1gNnSc2	zjevení
Janova	Janov	k1gInSc2	Janov
či	či	k8xC	či
Apokalypsu	apokalypsa	k1gFnSc4	apokalypsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
symbolických	symbolický	k2eAgInPc6d1	symbolický
obrazech	obraz	k1gInPc6	obraz
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
obce	obec	k1gFnSc2	obec
a	a	k8xC	a
předjímá	předjímat	k5eAaImIp3nS	předjímat
budoucnost	budoucnost	k1gFnSc4	budoucnost
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
datovat	datovat	k5eAaImF	datovat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
však	však	k9	však
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Podstatným	podstatný	k2eAgInSc7d1	podstatný
krokem	krok	k1gInSc7	krok
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
byl	být	k5eAaImAgInS	být
výběr	výběr	k1gInSc1	výběr
a	a	k8xC	a
sestavení	sestavení	k1gNnSc1	sestavení
závazného	závazný	k2eAgInSc2d1	závazný
souboru	soubor	k1gInSc2	soubor
(	(	kIx(	(
<g/>
kánonu	kánon	k1gInSc2	kánon
<g/>
)	)	kIx)	)
z	z	k7c2	z
jednotlivě	jednotlivě	k6eAd1	jednotlivě
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Evangelia	evangelium	k1gNnPc1	evangelium
a	a	k8xC	a
Pavlovy	Pavlův	k2eAgInPc1d1	Pavlův
listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
už	už	k6eAd1	už
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
citují	citovat	k5eAaBmIp3nP	citovat
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
křesťanských	křesťanský	k2eAgInPc6d1	křesťanský
spisech	spis	k1gInPc6	spis
jako	jako	k8xC	jako
nepochybné	pochybný	k2eNgFnPc4d1	nepochybná
autority	autorita	k1gFnPc4	autorita
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
200	[number]	k4	200
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nejstarší	starý	k2eAgInSc1d3	nejstarší
zachovaný	zachovaný	k2eAgInSc1d1	zachovaný
soupis	soupis	k1gInSc1	soupis
knih	kniha	k1gFnPc2	kniha
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
Zlomek	zlomek	k1gInSc1	zlomek
Muratoriho	Muratori	k1gMnSc2	Muratori
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
některých	některý	k3yIgInPc2	některý
obecných	obecný	k2eAgInPc2d1	obecný
listů	list	k1gInPc2	list
a	a	k8xC	a
Apokalypsy	apokalypsa	k1gFnSc2	apokalypsa
se	se	k3xPyFc4	se
diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
církevních	církevní	k2eAgFnPc6d1	církevní
obcích	obec	k1gFnPc6	obec
se	se	k3xPyFc4	se
četly	číst	k5eAaImAgFnP	číst
a	a	k8xC	a
uznávaly	uznávat	k5eAaImAgFnP	uznávat
i	i	k9	i
další	další	k2eAgInPc4d1	další
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
součástí	součást	k1gFnSc7	součást
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
nestaly	stát	k5eNaPmAgFnP	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
také	také	k9	také
odvolávaly	odvolávat	k5eAaImAgInP	odvolávat
na	na	k7c4	na
autoritu	autorita	k1gFnSc4	autorita
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
však	však	k9	však
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
obecného	obecný	k2eAgNnSc2d1	obecné
uznání	uznání	k1gNnSc2	uznání
(	(	kIx(	(
<g/>
List	lista	k1gFnPc2	lista
Laodicejským	Laodicejský	k2eAgFnPc3d1	Laodicejský
<g/>
,	,	kIx,	,
Evangelium	evangelium	k1gNnSc1	evangelium
Tomášovo	Tomášův	k2eAgNnSc1d1	Tomášovo
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
(	(	kIx(	(
<g/>
novozákonní	novozákonní	k2eAgInPc4d1	novozákonní
<g/>
)	)	kIx)	)
apokryfy	apokryf	k1gInPc4	apokryf
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
až	až	k9	až
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
tradice	tradice	k1gFnSc1	tradice
připisuje	připisovat	k5eAaImIp3nS	připisovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
knihy	kniha	k1gFnPc4	kniha
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
známým	známý	k2eAgFnPc3d1	známá
postavám	postava	k1gFnPc3	postava
<g/>
,	,	kIx,	,
apoštolům	apoštol	k1gMnPc3	apoštol
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
žákům	žák	k1gMnPc3	žák
<g/>
.	.	kIx.	.
</s>
<s>
Autorství	autorství	k1gNnSc1	autorství
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
tehdy	tehdy	k6eAd1	tehdy
chápalo	chápat	k5eAaImAgNnS	chápat
méně	málo	k6eAd2	málo
výlučně	výlučně	k6eAd1	výlučně
–	–	k?	–
ostatně	ostatně	k6eAd1	ostatně
starozákonní	starozákonní	k2eAgFnSc2d1	starozákonní
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Mojžíšovy	Mojžíšův	k2eAgInPc4d1	Mojžíšův
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
také	také	k9	také
Mojžíšovu	Mojžíšův	k2eAgFnSc4d1	Mojžíšova
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starých	starý	k2eAgInPc6d1	starý
rukopisech	rukopis	k1gInPc6	rukopis
mají	mít	k5eAaImIp3nP	mít
evangelia	evangelium	k1gNnPc1	evangelium
nadpisy	nadpis	k1gInPc4	nadpis
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Podle	podle	k7c2	podle
Matouše	Matouš	k1gMnSc2	Matouš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nemusí	muset	k5eNaImIp3nS	muset
znamenat	znamenat	k5eAaImF	znamenat
přímé	přímý	k2eAgNnSc4d1	přímé
autorství	autorství	k1gNnSc4	autorství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vzít	vzít	k5eAaPmF	vzít
také	také	k9	také
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
autoři	autor	k1gMnPc1	autor
své	svůj	k3xOyFgInPc4	svůj
texty	text	k1gInPc4	text
obvykle	obvykle	k6eAd1	obvykle
diktovali	diktovat	k5eAaImAgMnP	diktovat
písaři	písař	k1gMnPc1	písař
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
i	i	k9	i
formálně	formálně	k6eAd1	formálně
dotvářel	dotvářet	k5eAaImAgMnS	dotvářet
<g/>
.	.	kIx.	.
</s>
<s>
Textová	textový	k2eAgFnSc1d1	textová
a	a	k8xC	a
literární	literární	k2eAgFnSc1d1	literární
kritika	kritika	k1gFnSc1	kritika
dnes	dnes	k6eAd1	dnes
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
evangelia	evangelium	k1gNnPc1	evangelium
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
v	v	k7c6	v
letech	let	k1gInPc6	let
65	[number]	k4	65
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
napsána	napsat	k5eAaPmNgFnS	napsat
přímými	přímý	k2eAgMnPc7d1	přímý
Ježíšovými	Ježíšův	k2eAgMnPc7d1	Ježíšův
žáky	žák	k1gMnPc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
první	první	k4xOgFnSc1	první
(	(	kIx(	(
<g/>
synoptická	synoptický	k2eAgFnSc1d1	synoptická
<g/>
)	)	kIx)	)
evangelia	evangelium	k1gNnPc1	evangelium
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zřetelně	zřetelně	k6eAd1	zřetelně
redakce	redakce	k1gFnSc2	redakce
<g/>
,	,	kIx,	,
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c6	na
starších	starý	k2eAgFnPc6d2	starší
sbírkách	sbírka	k1gFnPc6	sbírka
výroků	výrok	k1gInPc2	výrok
<g/>
,	,	kIx,	,
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
podobenství	podobenství	k1gNnSc6	podobenství
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
diskusi	diskuse	k1gFnSc6	diskuse
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
Markovi	Marek	k1gMnSc3	Marek
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Lukášovi	Lukášův	k2eAgMnPc1d1	Lukášův
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
míní	mínit	k5eAaImIp3nS	mínit
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
původce	původce	k1gMnSc4	původce
(	(	kIx(	(
<g/>
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
)	)	kIx)	)
současného	současný	k2eAgInSc2d1	současný
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Skutků	skutek	k1gInPc2	skutek
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
téhož	týž	k3xTgMnSc2	týž
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
napsal	napsat	k5eAaBmAgInS	napsat
i	i	k9	i
Evangelium	evangelium	k1gNnSc4	evangelium
podle	podle	k7c2	podle
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
připisovaných	připisovaný	k2eAgInPc2d1	připisovaný
apoštolu	apoštol	k1gMnSc3	apoštol
Pavlovi	Pavel	k1gMnSc3	Pavel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
sedmi	sedm	k4xCc2	sedm
nepochybným	pochybný	k2eNgMnSc7d1	nepochybný
autorem	autor	k1gMnSc7	autor
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
Te	Te	k1gFnPc2	Te
<g/>
,	,	kIx,	,
Ga	Ga	k1gFnPc2	Ga
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
K	K	kA	K
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
K	K	kA	K
<g/>
,	,	kIx,	,
Ř	Ř	kA	Ř
<g/>
,	,	kIx,	,
Fp	Fp	k1gFnSc2	Fp
a	a	k8xC	a
Fm	Fm	k1gFnSc2	Fm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
šesti	šest	k4xCc2	šest
byl	být	k5eAaImAgInS	být
možná	možná	k9	možná
jen	jen	k9	jen
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
inspirátorem	inspirátor	k1gMnSc7	inspirátor
nějakého	nějaký	k3yIgMnSc4	nějaký
žáka	žák	k1gMnSc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
Janovy	Janův	k2eAgInPc1d1	Janův
<g/>
,	,	kIx,	,
Petrovy	Petrův	k2eAgInPc1d1	Petrův
a	a	k8xC	a
List	list	k1gInSc1	list
Židům	Žid	k1gMnPc3	Žid
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
patrně	patrně	k6eAd1	patrně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
100	[number]	k4	100
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
List	list	k1gInSc1	list
Judův	Judův	k2eAgInSc1d1	Judův
možná	možná	k9	možná
ještě	ještě	k9	ještě
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
a	a	k8xC	a
pocházejí	pocházet	k5eAaImIp3nP	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
od	od	k7c2	od
apoštolských	apoštolský	k2eAgMnPc2d1	apoštolský
žáků	žák	k1gMnPc2	žák
<g/>
.	.	kIx.	.
</s>
<s>
Spisy	spis	k1gInPc1	spis
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
v	v	k7c6	v
mimořádně	mimořádně	k6eAd1	mimořádně
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
starých	starý	k2eAgInPc2d1	starý
opisů	opis	k1gInPc2	opis
<g/>
,	,	kIx,	,
překladů	překlad	k1gInPc2	překlad
<g/>
,	,	kIx,	,
zlomků	zlomek	k1gInPc2	zlomek
a	a	k8xC	a
citátů	citát	k1gInPc2	citát
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
zdaleka	zdaleka	k6eAd1	zdaleka
nejlépe	dobře	k6eAd3	dobře
zajištěnými	zajištěný	k2eAgInPc7d1	zajištěný
starověkými	starověký	k2eAgInPc7d1	starověký
texty	text	k1gInPc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
zachované	zachovaný	k2eAgInPc1d1	zachovaný
zlomky	zlomek	k1gInPc1	zlomek
na	na	k7c6	na
papyru	papyr	k1gInSc6	papyr
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
;	;	kIx,	;
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
papyrus	papyrus	k1gInSc1	papyrus
Rylands	Rylands	k1gInSc1	Rylands
457	[number]	k4	457
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
52	[number]	k4	52
<g/>
)	)	kIx)	)
s	s	k7c7	s
několika	několik	k4yIc7	několik
verši	verš	k1gInPc7	verš
Janova	Janův	k2eAgNnSc2d1	Janovo
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
140	[number]	k4	140
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhého	druhý	k4xOgNnSc2	druhý
století	století	k1gNnSc2	století
vznikaly	vznikat	k5eAaImAgFnP	vznikat
i	i	k9	i
první	první	k4xOgInPc1	první
překlady	překlad	k1gInPc1	překlad
novozákonních	novozákonní	k2eAgFnPc2d1	novozákonní
knih	kniha	k1gFnPc2	kniha
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
do	do	k7c2	do
syrských	syrský	k2eAgInPc2d1	syrský
a	a	k8xC	a
egyptských	egyptský	k2eAgInPc2d1	egyptský
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
téže	týž	k3xTgFnSc2	týž
doby	doba	k1gFnSc2	doba
pocházejí	pocházet	k5eAaImIp3nP	pocházet
i	i	k9	i
nejstarší	starý	k2eAgInPc1d3	nejstarší
citáty	citát	k1gInPc1	citát
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
raně	raně	k6eAd1	raně
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
autorů	autor	k1gMnPc2	autor
(	(	kIx(	(
<g/>
Kléméns	Kléméns	k1gInSc1	Kléméns
Alexandrijský	alexandrijský	k2eAgInSc1d1	alexandrijský
<g/>
,	,	kIx,	,
Órigenés	Órigenés	k1gInSc1	Órigenés
<g/>
,	,	kIx,	,
Ireneus	Ireneus	k1gInSc1	Ireneus
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
už	už	k6eAd1	už
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
jako	jako	k9	jako
na	na	k7c4	na
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pocházejí	pocházet	k5eAaImIp3nP	pocházet
i	i	k9	i
nejstarší	starý	k2eAgFnPc1d3	nejstarší
a	a	k8xC	a
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
úplné	úplný	k2eAgInPc1d1	úplný
rukopisy	rukopis	k1gInPc1	rukopis
celé	celý	k2eAgFnSc2d1	celá
řecké	řecký	k2eAgFnSc2d1	řecká
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
Starého	Starého	k2eAgInSc2d1	Starého
i	i	k8xC	i
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
pergamenové	pergamenový	k2eAgInPc4d1	pergamenový
kodexy	kodex	k1gInPc4	kodex
Vatikánský	vatikánský	k2eAgMnSc1d1	vatikánský
(	(	kIx(	(
<g/>
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sinajský	sinajský	k2eAgInSc1d1	sinajský
(	(	kIx(	(
<g/>
Alef	alef	k1gInSc1	alef
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alexandrijský	alexandrijský	k2eAgMnSc1d1	alexandrijský
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
starší	starý	k2eAgFnSc2d2	starší
formy	forma	k1gFnSc2	forma
svitků	svitek	k1gInPc2	svitek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
opisovaly	opisovat	k5eAaImAgInP	opisovat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
,	,	kIx,	,
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
právě	právě	k9	právě
kodex	kodex	k1gInSc1	kodex
(	(	kIx(	(
<g/>
vázaná	vázaný	k2eAgFnSc1d1	vázaná
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
stanovení	stanovení	k1gNnSc1	stanovení
rozsahu	rozsah	k1gInSc2	rozsah
kánonu	kánon	k1gInSc2	kánon
<g/>
,	,	kIx,	,
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
knihy	kniha	k1gFnPc1	kniha
do	do	k7c2	do
něho	on	k3xPp3gMnSc2	on
patří	patřit	k5eAaImIp3nP	patřit
a	a	k8xC	a
které	který	k3yRgInPc1	který
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
křesťanství	křesťanství	k1gNnSc2	křesťanství
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
latina	latina	k1gFnSc1	latina
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
řečtinu	řečtina	k1gFnSc4	řečtina
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
světový	světový	k2eAgInSc4d1	světový
<g/>
"	"	kIx"	"
jazyk	jazyk	k1gInSc4	jazyk
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
historii	historie	k1gFnSc4	historie
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
působení	působení	k1gNnSc2	působení
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
rozhodující	rozhodující	k2eAgInPc1d1	rozhodující
překlady	překlad	k1gInPc1	překlad
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
latinské	latinský	k2eAgNnSc4d1	latinské
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
latinské	latinský	k2eAgInPc1d1	latinský
překlady	překlad	k1gInPc1	překlad
(	(	kIx(	(
<g/>
vetus	vetus	k1gInSc1	vetus
latina	latina	k1gFnSc1	latina
<g/>
)	)	kIx)	)
vznikaly	vznikat	k5eAaImAgFnP	vznikat
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pověřil	pověřit	k5eAaPmAgMnS	pověřit
papež	papež	k1gMnSc1	papež
Damasus	Damasus	k1gMnSc1	Damasus
učence	učenec	k1gMnSc2	učenec
Jeronýma	Jeroným	k1gMnSc2	Jeroným
(	(	kIx(	(
<g/>
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
opravil	opravit	k5eAaPmAgMnS	opravit
a	a	k8xC	a
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
<g/>
.	.	kIx.	.
</s>
<s>
Jeroným	Jeroným	k1gMnSc1	Jeroným
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
a	a	k8xC	a
tam	tam	k6eAd1	tam
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
latinský	latinský	k2eAgInSc1d1	latinský
překlad	překlad	k1gInSc1	překlad
celé	celý	k2eAgFnSc2d1	celá
Bible	bible	k1gFnSc2	bible
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
hebrejských	hebrejský	k2eAgInPc2d1	hebrejský
a	a	k8xC	a
řeckých	řecký	k2eAgInPc2d1	řecký
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vulgata	Vulgat	k1gMnSc2	Vulgat
užíval	užívat	k5eAaImAgInS	užívat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
také	také	k9	také
základem	základ	k1gInSc7	základ
prvních	první	k4xOgInPc2	první
překladů	překlad	k1gInPc2	překlad
do	do	k7c2	do
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
antice	antika	k1gFnSc6	antika
a	a	k8xC	a
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
překladů	překlad	k1gInPc2	překlad
do	do	k7c2	do
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
gótský	gótský	k2eAgInSc4d1	gótský
překlad	překlad	k1gInSc4	překlad
Wulfilův	Wulfilův	k2eAgInSc4d1	Wulfilův
kolem	kolem	k7c2	kolem
380	[number]	k4	380
<g/>
,	,	kIx,	,
arménský	arménský	k2eAgMnSc1d1	arménský
<g/>
,	,	kIx,	,
syrský	syrský	k2eAgMnSc1d1	syrský
<g/>
,	,	kIx,	,
koptský	koptský	k2eAgInSc1d1	koptský
a	a	k8xC	a
gruzínský	gruzínský	k2eAgInSc1d1	gruzínský
překlad	překlad	k1gInSc1	překlad
kolem	kolem	k7c2	kolem
500	[number]	k4	500
<g/>
,	,	kIx,	,
slovanský	slovanský	k2eAgInSc1d1	slovanský
překlad	překlad	k1gInSc1	překlad
sv.	sv.	kA	sv.
Metoděje	Metoděj	k1gMnSc2	Metoděj
kolem	kolem	k7c2	kolem
870	[number]	k4	870
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
části	část	k1gFnPc1	část
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
byly	být	k5eAaImAgInP	být
už	už	k6eAd1	už
ve	v	k7c6	v
středověku	středověk	k1gInSc2	středověk
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1350	[number]	k4	1350
se	se	k3xPyFc4	se
však	však	k9	však
objevují	objevovat	k5eAaImIp3nP	objevovat
překlady	překlad	k1gInPc1	překlad
celého	celý	k2eAgInSc2d1	celý
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
celé	celý	k2eAgFnSc2d1	celá
Bible	bible	k1gFnSc2	bible
<g/>
)	)	kIx)	)
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1360	[number]	k4	1360
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
<g/>
,	,	kIx,	,
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
,	,	kIx,	,
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
prvními	první	k4xOgFnPc7	první
tištěnými	tištěný	k2eAgFnPc7d1	tištěná
knihami	kniha	k1gFnPc7	kniha
byl	být	k5eAaImAgInS	být
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
a	a	k8xC	a
Bible	bible	k1gFnSc1	bible
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
(	(	kIx(	(
<g/>
1455	[number]	k4	1455
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
češtině	čeština	k1gFnSc6	čeština
(	(	kIx(	(
<g/>
1475	[number]	k4	1475
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
katalánštině	katalánština	k1gFnSc6	katalánština
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
koncem	koncem	k7c2	koncem
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
na	na	k7c6	na
západě	západ	k1gInSc6	západ
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
znalost	znalost	k1gFnSc1	znalost
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
vycházejí	vycházet	k5eAaImIp3nP	vycházet
tiskem	tisk	k1gInSc7	tisk
nová	nový	k2eAgNnPc4d1	nové
vydání	vydání	k1gNnPc4	vydání
řecké	řecký	k2eAgFnSc2d1	řecká
Bible	bible	k1gFnSc2	bible
i	i	k8xC	i
nové	nový	k2eAgInPc1d1	nový
překlady	překlad	k1gInPc1	překlad
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1514	[number]	k4	1514
<g/>
–	–	k?	–
<g/>
1520	[number]	k4	1520
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
kardinála	kardinál	k1gMnSc2	kardinál
Jimeneze	Jimeneze	k1gFnSc2	Jimeneze
v	v	k7c4	v
Alcalá	Alcalý	k2eAgNnPc4d1	Alcalý
paralelní	paralelní	k2eAgNnPc4d1	paralelní
vydání	vydání	k1gNnPc4	vydání
Bible	bible	k1gFnSc2	bible
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
<g/>
,	,	kIx,	,
řečtině	řečtina	k1gFnSc6	řečtina
a	a	k8xC	a
latině	latina	k1gFnSc6	latina
(	(	kIx(	(
<g/>
Polyglotta	Polyglotta	k1gFnSc1	Polyglotta
complutensis	complutensis	k1gFnSc2	complutensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1516	[number]	k4	1516
vyšel	vyjít	k5eAaPmAgInS	vyjít
Erasmův	Erasmův	k2eAgInSc1d1	Erasmův
řecký	řecký	k2eAgInSc1d1	řecký
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
knihtisku	knihtisk	k1gInSc2	knihtisk
i	i	k8xC	i
reformace	reformace	k1gFnSc2	reformace
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
potřeba	potřeba	k1gFnSc1	potřeba
soukromé	soukromý	k2eAgFnSc2d1	soukromá
četby	četba	k1gFnSc2	četba
Bible	bible	k1gFnSc2	bible
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgFnPc1d1	nová
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
významné	významný	k2eAgInPc4d1	významný
překlady	překlad	k1gInPc4	překlad
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Jana	Jan	k1gMnSc2	Jan
Blahoslava	Blahoslav	k1gMnSc2	Blahoslav
(	(	kIx(	(
<g/>
1564	[number]	k4	1564
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převzatý	převzatý	k2eAgInSc1d1	převzatý
pak	pak	k6eAd1	pak
do	do	k7c2	do
Bible	bible	k1gFnSc2	bible
kralické	kralický	k2eAgNnSc1d1	kralické
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgInSc1d1	anglický
překlad	překlad	k1gInSc1	překlad
Tyndalův	Tyndalův	k2eAgInSc1d1	Tyndalův
(	(	kIx(	(
<g/>
1527	[number]	k4	1527
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bible	bible	k1gFnSc1	bible
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
(	(	kIx(	(
<g/>
1611	[number]	k4	1611
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgInSc1d1	francouzský
překlad	překlad	k1gInSc1	překlad
Lefevra	Lefevr	k1gInSc2	Lefevr
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Etaples	Etaples	k1gInSc1	Etaples
(	(	kIx(	(
<g/>
1530	[number]	k4	1530
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lutherův	Lutherův	k2eAgInSc1d1	Lutherův
překlad	překlad	k1gInSc1	překlad
celé	celý	k2eAgFnSc2d1	celá
Bible	bible	k1gFnSc2	bible
(	(	kIx(	(
<g/>
1534	[number]	k4	1534
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
překlady	překlad	k1gInPc1	překlad
měly	mít	k5eAaImAgInP	mít
hluboký	hluboký	k2eAgInSc4d1	hluboký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
utváření	utváření	k1gNnSc4	utváření
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
tvořily	tvořit	k5eAaImAgFnP	tvořit
i	i	k8xC	i
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
normu	norma	k1gFnSc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
předlohy	předloha	k1gFnPc1	předloha
byly	být	k5eAaImAgFnP	být
však	však	k9	však
málo	málo	k6eAd1	málo
spolehlivé	spolehlivý	k2eAgNnSc1d1	spolehlivé
a	a	k8xC	a
Erasmus	Erasmus	k1gMnSc1	Erasmus
dokonce	dokonce	k9	dokonce
chybějící	chybějící	k2eAgFnSc4d1	chybějící
část	část	k1gFnSc4	část
Apokalypsy	apokalypsa	k1gFnSc2	apokalypsa
přeložil	přeložit	k5eAaPmAgMnS	přeložit
sám	sám	k3xTgMnSc1	sám
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
do	do	k7c2	do
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
soustavné	soustavný	k2eAgNnSc4d1	soustavné
kritické	kritický	k2eAgNnSc4d1	kritické
zkoumání	zkoumání	k1gNnSc4	zkoumání
a	a	k8xC	a
porovnávání	porovnávání	k1gNnSc4	porovnávání
biblických	biblický	k2eAgInPc2d1	biblický
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Jean	Jean	k1gMnSc1	Jean
Astruc	Astruc	k1gFnSc1	Astruc
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
v	v	k7c6	v
kritických	kritický	k2eAgNnPc6d1	kritické
vydáních	vydání	k1gNnPc6	vydání
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
misií	misie	k1gFnPc2	misie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
říší	říš	k1gFnPc2	říš
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
překlady	překlad	k1gInPc4	překlad
Bible	bible	k1gFnSc2	bible
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
i	i	k8xC	i
mimoevropských	mimoevropský	k2eAgInPc2d1	mimoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikají	vznikat	k5eAaImIp3nP	vznikat
Biblické	biblický	k2eAgFnPc1d1	biblická
společnosti	společnost	k1gFnPc1	společnost
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
překládání	překládání	k1gNnSc4	překládání
a	a	k8xC	a
vydávání	vydávání	k1gNnSc4	vydávání
Bible	bible	k1gFnSc2	bible
Podle	podle	k7c2	podle
jejích	její	k3xOp3gInPc2	její
údajů	údaj	k1gInPc2	údaj
byly	být	k5eAaImAgInP	být
aspoň	aspoň	k9	aspoň
části	část	k1gFnSc2	část
Bible	bible	k1gFnSc2	bible
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
2900	[number]	k4	2900
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
pro	pro	k7c4	pro
98	[number]	k4	98
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
Bible	bible	k1gFnSc1	bible
jazykově	jazykově	k6eAd1	jazykově
dostupná	dostupný	k2eAgFnSc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
vědcům	vědec	k1gMnPc3	vědec
podařilo	podařit	k5eAaPmAgNnS	podařit
shromáždit	shromáždit	k5eAaPmF	shromáždit
a	a	k8xC	a
porovnat	porovnat	k5eAaPmF	porovnat
stovky	stovka	k1gFnPc4	stovka
starých	starý	k2eAgInPc2d1	starý
rukopisů	rukopis	k1gInPc2	rukopis
a	a	k8xC	a
překladů	překlad	k1gInPc2	překlad
<g/>
,	,	kIx,	,
tisíce	tisíc	k4xCgInPc4	tisíc
citátů	citát	k1gInPc2	citát
a	a	k8xC	a
zlomků	zlomek	k1gInPc2	zlomek
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
řeckými	řecký	k2eAgInPc7d1	řecký
a	a	k8xC	a
latinskými	latinský	k2eAgInPc7d1	latinský
texty	text	k1gInPc7	text
se	se	k3xPyFc4	se
často	často	k6eAd1	často
podařilo	podařit	k5eAaPmAgNnS	podařit
uspořádat	uspořádat	k5eAaPmF	uspořádat
rukopisy	rukopis	k1gInPc4	rukopis
do	do	k7c2	do
"	"	kIx"	"
<g/>
rodokmenů	rodokmen	k1gInPc2	rodokmen
<g/>
"	"	kIx"	"
a	a	k8xC	a
jednotlivým	jednotlivý	k2eAgFnPc3d1	jednotlivá
verzím	verze	k1gFnPc3	verze
přiřadit	přiřadit	k5eAaPmF	přiřadit
poměrně	poměrně	k6eAd1	poměrně
spolehlivé	spolehlivý	k2eAgFnPc4d1	spolehlivá
váhy	váha	k1gFnPc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
textu	text	k1gInSc6	text
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
asi	asi	k9	asi
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInSc4	tisíc
odchylek	odchylka	k1gFnPc2	odchylka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
zřejmých	zřejmý	k2eAgFnPc2d1	zřejmá
písařských	písařský	k2eAgFnPc2d1	písařská
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
pokusů	pokus	k1gInPc2	pokus
odstranit	odstranit	k5eAaPmF	odstranit
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
líčením	líčení	k1gNnSc7	líčení
čtyř	čtyři	k4xCgNnPc2	čtyři
evangelií	evangelium	k1gNnPc2	evangelium
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
kritická	kritický	k2eAgFnSc1d1	kritická
vydání	vydání	k1gNnSc1	vydání
původních	původní	k2eAgInPc2d1	původní
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
moderní	moderní	k2eAgInPc4d1	moderní
překlady	překlad	k1gInPc4	překlad
<g/>
,	,	kIx,	,
registrují	registrovat	k5eAaBmIp3nP	registrovat
tisíce	tisíc	k4xCgInPc1	tisíc
drobných	drobný	k2eAgMnPc2d1	drobný
a	a	k8xC	a
několik	několik	k4yIc4	několik
významnějších	významný	k2eAgFnPc2d2	významnější
odchylek	odchylka	k1gFnPc2	odchylka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
v	v	k7c6	v
poznámkách	poznámka	k1gFnPc6	poznámka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
odchylky	odchylka	k1gFnPc4	odchylka
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
úryvek	úryvek	k1gInSc1	úryvek
o	o	k7c6	o
cizoložné	cizoložný	k2eAgFnSc6d1	cizoložná
ženě	žena	k1gFnSc6	žena
nebo	nebo	k8xC	nebo
výrok	výrok	k1gInSc4	výrok
o	o	k7c6	o
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
chybí	chybět	k5eAaImIp3nS	chybět
v	v	k7c6	v
nejstarších	starý	k2eAgInPc6d3	nejstarší
a	a	k8xC	a
nejlepších	dobrý	k2eAgInPc6d3	nejlepší
rukopisech	rukopis	k1gInPc6	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
několika	několik	k4yIc6	několik
starých	starý	k2eAgInPc6d1	starý
rukopisech	rukopis	k1gInPc6	rukopis
chybí	chybit	k5eAaPmIp3nS	chybit
část	část	k1gFnSc1	část
Mk	Mk	k1gFnSc1	Mk
16,9	[number]	k4	16,9
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
stručnějším	stručný	k2eAgNnSc7d2	stručnější
zněním	znění	k1gNnSc7	znění
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
jako	jako	k8xS	jako
nejčastěji	často	k6eAd3	často
používaná	používaný	k2eAgFnSc1d1	používaná
část	část	k1gFnSc1	část
Bible	bible	k1gFnSc2	bible
měl	mít	k5eAaImAgMnS	mít
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
i	i	k8xC	i
českou	český	k2eAgFnSc4d1	Česká
kulturu	kultura	k1gFnSc4	kultura
nesmírný	smírný	k2eNgInSc4d1	nesmírný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Biblické	biblický	k2eAgInPc1d1	biblický
překlady	překlad	k1gInPc1	překlad
patřily	patřit	k5eAaImAgInP	patřit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
psaným	psaný	k2eAgInPc3d1	psaný
textům	text	k1gInPc3	text
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
silně	silně	k6eAd1	silně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
vznik	vznik	k1gInSc4	vznik
spisovných	spisovný	k2eAgInPc2d1	spisovný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
vlnu	vlna	k1gFnSc4	vlna
tohoto	tento	k3xDgInSc2	tento
jazykového	jazykový	k2eAgInSc2d1	jazykový
vlivu	vliv	k1gInSc2	vliv
způsobily	způsobit	k5eAaPmAgInP	způsobit
překlady	překlad	k1gInPc1	překlad
reformační	reformační	k2eAgInPc1d1	reformační
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Bible	bible	k1gFnSc1	bible
kralická	kralický	k2eAgFnSc1d1	Kralická
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
i	i	k9	i
pro	pro	k7c4	pro
české	český	k2eAgNnSc4d1	české
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
představovala	představovat	k5eAaImAgFnS	představovat
téměř	téměř	k6eAd1	téměř
normu	norma	k1gFnSc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
překladech	překlad	k1gInPc6	překlad
se	se	k3xPyFc4	se
do	do	k7c2	do
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
dostala	dostat	k5eAaPmAgFnS	dostat
řada	řada	k1gFnSc1	řada
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
slovních	slovní	k2eAgNnPc2d1	slovní
spojení	spojení	k1gNnPc2	spojení
<g/>
,	,	kIx,	,
úsloví	úsloví	k1gNnSc1	úsloví
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
běžnému	běžný	k2eAgInSc3d1	běžný
jazykovému	jazykový	k2eAgInSc3d1	jazykový
fondu	fond	k1gInSc3	fond
<g/>
.	.	kIx.	.
</s>
<s>
Nejenom	nejenom	k6eAd1	nejenom
biblická	biblický	k2eAgNnPc1d1	biblické
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dodnes	dodnes	k6eAd1	dodnes
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
nejčastější	častý	k2eAgFnSc7d3	nejčastější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
slova	slovo	k1gNnPc1	slovo
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
farizej	farizej	k1gMnSc1	farizej
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
talent	talent	k1gInSc1	talent
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
pokušení	pokušení	k1gNnPc2	pokušení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
kříž	kříž	k1gInSc1	kříž
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
obraty	obrat	k1gInPc4	obrat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ztracený	ztracený	k2eAgMnSc1d1	ztracený
syn	syn	k1gMnSc1	syn
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
kdo	kdo	k3yQnSc1	kdo
nepracuje	pracovat	k5eNaImIp3nS	pracovat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
nejí	jíst	k5eNaImIp3nS	jíst
<g/>
"	"	kIx"	"
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
biblického	biblický	k2eAgInSc2d1	biblický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
velký	velký	k2eAgMnSc1d1	velký
byl	být	k5eAaImAgInS	být
i	i	k9	i
vliv	vliv	k1gInSc1	vliv
novozákonních	novozákonní	k2eAgFnPc2d1	novozákonní
metafor	metafora	k1gFnPc2	metafora
nebo	nebo	k8xC	nebo
podobenství	podobenství	k1gNnSc1	podobenství
a	a	k8xC	a
nedávná	dávný	k2eNgFnSc1d1	nedávná
publikace	publikace	k1gFnSc1	publikace
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
české	český	k2eAgFnSc6d1	Česká
poezii	poezie	k1gFnSc6	poezie
<g/>
.	.	kIx.	.
</s>
