<p>
<s>
Cigareta	cigareta	k1gFnSc1	cigareta
je	být	k5eAaImIp3nS	být
tabákový	tabákový	k2eAgInSc1d1	tabákový
výrobek	výrobek	k1gInSc1	výrobek
–	–	k?	–
jemně	jemně	k6eAd1	jemně
řezaná	řezaný	k2eAgFnSc1d1	řezaná
tabáková	tabákový	k2eAgFnSc1d1	tabáková
směs	směs	k1gFnSc1	směs
(	(	kIx(	(
<g/>
FCSA	FCSA	kA	FCSA
<g/>
)	)	kIx)	)
zabalená	zabalený	k2eAgFnSc1d1	zabalená
v	v	k7c6	v
cigaretovém	cigaretový	k2eAgInSc6d1	cigaretový
papírku	papírek	k1gInSc6	papírek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
cigarety	cigareta	k1gFnPc1	cigareta
většinou	většinou	k6eAd1	většinou
opatřené	opatřený	k2eAgInPc1d1	opatřený
filtrem	filtr	k1gInSc7	filtr
<g/>
.	.	kIx.	.
</s>
<s>
Tabák	tabák	k1gInSc1	tabák
v	v	k7c6	v
cigaretách	cigareta	k1gFnPc6	cigareta
je	být	k5eAaImIp3nS	být
směsí	směs	k1gFnPc2	směs
tabáků	tabák	k1gInPc2	tabák
(	(	kIx(	(
<g/>
až	až	k9	až
30	[number]	k4	30
druhů	druh	k1gInPc2	druh
tabáku	tabák	k1gInSc2	tabák
<g/>
)	)	kIx)	)
různého	různý	k2eAgInSc2d1	různý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
různými	různý	k2eAgInPc7d1	různý
postupy	postup	k1gInPc7	postup
fermentovaného	fermentovaný	k2eAgNnSc2d1	fermentované
a	a	k8xC	a
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
dalšími	další	k2eAgFnPc7d1	další
přísadami	přísada	k1gFnPc7	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
mix	mix	k1gInSc1	mix
dává	dávat	k5eAaImIp3nS	dávat
různým	různý	k2eAgFnPc3d1	různá
značkám	značka	k1gFnPc3	značka
cigaret	cigareta	k1gFnPc2	cigareta
různou	různý	k2eAgFnSc4d1	různá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Tabák	tabák	k1gInSc1	tabák
je	být	k5eAaImIp3nS	být
nakrájený	nakrájený	k2eAgInSc1d1	nakrájený
na	na	k7c4	na
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgInPc4d1	tenký
nitkovité	nitkovitý	k2eAgInPc4d1	nitkovitý
proužky	proužek	k1gInPc4	proužek
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
laminy	lamin	k2eAgFnSc2d1	lamina
a	a	k8xC	a
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
množstvím	množství	k1gNnSc7	množství
zlomků	zlomek	k1gInPc2	zlomek
laminy	lamin	k2eAgInPc1d1	lamin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
cigaret	cigareta	k1gFnPc2	cigareta
používán	používat	k5eAaImNgInS	používat
řez	řez	k1gInSc1	řez
0,9	[number]	k4	0,9
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
hrubší	hrubý	k2eAgInSc1d2	hrubší
řez	řez	k1gInSc1	řez
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
silnější	silný	k2eAgInPc1d2	silnější
a	a	k8xC	a
aromatičtější	aromatický	k2eAgInSc1d2	aromatičtější
kouř	kouř	k1gInSc1	kouř
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
tvorba	tvorba	k1gFnSc1	tvorba
částicové	částicový	k2eAgFnSc2d1	částicová
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
definice	definice	k1gFnPc1	definice
==	==	k?	==
</s>
</p>
<p>
<s>
Vyhláška	vyhláška	k1gFnSc1	vyhláška
českého	český	k2eAgNnSc2d1	české
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zemědělství	zemědělství	k1gNnSc2	zemědělství
č.	č.	k?	č.
344	[number]	k4	344
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
definuje	definovat	k5eAaBmIp3nS	definovat
cigaretu	cigareta	k1gFnSc4	cigareta
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Cigaretou	cigareta	k1gFnSc7	cigareta
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
tabákový	tabákový	k2eAgInSc1d1	tabákový
smotek	smotek	k1gInSc1	smotek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
kouřit	kouřit	k5eAaImF	kouřit
jako	jako	k8xS	jako
takový	takový	k3xDgMnSc1	takový
a	a	k8xC	a
který	který	k3yRgMnSc1	který
není	být	k5eNaImIp3nS	být
doutníkem	doutník	k1gInSc7	doutník
podle	podle	k7c2	podle
písmene	písmeno	k1gNnSc2	písmeno
c	c	k0	c
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
cigarillem	cigarill	k1gInSc7	cigarill
podle	podle	k7c2	podle
písmene	písmeno	k1gNnSc2	písmeno
d	d	k?	d
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
neprůmyslovou	průmyslový	k2eNgFnSc7d1	neprůmyslová
manipulací	manipulace	k1gFnSc7	manipulace
vloží	vložit	k5eAaPmIp3nS	vložit
do	do	k7c2	do
dutinky	dutinka	k1gFnSc2	dutinka
z	z	k7c2	z
cigaretového	cigaretový	k2eAgInSc2d1	cigaretový
papíru	papír	k1gInSc2	papír
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
neprůmyslovou	průmyslový	k2eNgFnSc7d1	neprůmyslová
manipulací	manipulace	k1gFnSc7	manipulace
zabalí	zabalit	k5eAaPmIp3nS	zabalit
do	do	k7c2	do
cigaretového	cigaretový	k2eAgInSc2d1	cigaretový
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc4	složení
cigarety	cigareta	k1gFnPc1	cigareta
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Náustkový	Náustkový	k2eAgInSc4d1	Náustkový
papír	papír	k1gInSc4	papír
===	===	k?	===
</s>
</p>
<p>
<s>
Prodyšný	prodyšný	k2eAgInSc1d1	prodyšný
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
regulující	regulující	k2eAgInSc4d1	regulující
tah	tah	k1gInSc4	tah
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
perforovaný	perforovaný	k2eAgInSc1d1	perforovaný
kvůli	kvůli	k7c3	kvůli
prodyšnosti	prodyšnost	k1gFnSc3	prodyšnost
<g/>
.	.	kIx.	.
</s>
<s>
Perforace	perforace	k1gFnSc1	perforace
je	být	k5eAaImIp3nS	být
souhrn	souhrn	k1gInSc4	souhrn
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
děr	děra	k1gFnPc2	děra
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
náustkového	náustkový	k2eAgInSc2d1	náustkový
papíru	papír	k1gInSc2	papír
(	(	kIx(	(
<g/>
provádí	provádět	k5eAaImIp3nS	provádět
se	se	k3xPyFc4	se
mechanicky	mechanicky	k6eAd1	mechanicky
nebo	nebo	k8xC	nebo
laserem	laser	k1gInSc7	laser
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náustkový	Náustkový	k2eAgInSc1d1	Náustkový
papír	papír	k1gInSc1	papír
je	být	k5eAaImIp3nS	být
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
pojítkem	pojítko	k1gNnSc7	pojítko
mezi	mezi	k7c7	mezi
částí	část	k1gFnSc7	část
filtru	filtr	k1gInSc2	filtr
a	a	k8xC	a
cigaretového	cigaretový	k2eAgInSc2d1	cigaretový
provazce	provazec	k1gInSc2	provazec
složeného	složený	k2eAgInSc2d1	složený
z	z	k7c2	z
tabákové	tabákový	k2eAgFnSc2d1	tabáková
směsi	směs	k1gFnSc2	směs
obalené	obalený	k2eAgNnSc1d1	obalené
cigaretovým	cigaretový	k2eAgInSc7d1	cigaretový
papírem	papír	k1gInSc7	papír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cigaretový	cigaretový	k2eAgInSc4d1	cigaretový
filtr	filtr	k1gInSc4	filtr
===	===	k?	===
</s>
</p>
<p>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
acetátu	acetát	k1gInSc2	acetát
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Filtry	filtr	k1gInPc1	filtr
cigaret	cigareta	k1gFnPc2	cigareta
tvoří	tvořit	k5eAaImIp3nP	tvořit
třetinu	třetina	k1gFnSc4	třetina
veškerého	veškerý	k3xTgInSc2	veškerý
odpadu	odpad	k1gInSc2	odpad
z	z	k7c2	z
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
sbíraného	sbíraný	k2eAgInSc2d1	sbíraný
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
zpráva	zpráva	k1gFnSc1	zpráva
zobecňuje	zobecňovat	k5eAaImIp3nS	zobecňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
90	[number]	k4	90
<g/>
%	%	kIx~	%
odpadu	odpad	k1gInSc2	odpad
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
tvoří	tvořit	k5eAaImIp3nP	tvořit
plasty	plast	k1gInPc1	plast
(	(	kIx(	(
<g/>
acetát	acetát	k1gInSc1	acetát
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
filtr	filtr	k1gInSc4	filtr
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
plasty	plast	k1gInPc7	plast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tzv.	tzv.	kA	tzv.
light	light	k5eAaPmF	light
cigarety	cigareta	k1gFnPc4	cigareta
ale	ale	k8xC	ale
nesnižují	snižovat	k5eNaImIp3nP	snižovat
zdravotní	zdravotní	k2eAgNnPc4d1	zdravotní
rizika	riziko	k1gNnPc4	riziko
kouření	kouření	k1gNnSc2	kouření
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cigaretový	cigaretový	k2eAgInSc4d1	cigaretový
papír	papír	k1gInSc4	papír
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
obal	obal	k1gInSc1	obal
obklopující	obklopující	k2eAgInSc1d1	obklopující
tabákový	tabákový	k2eAgInSc1d1	tabákový
provazec	provazec	k1gInSc1	provazec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nejvíce	hodně	k6eAd3	hodně
chemikálií	chemikálie	k1gFnPc2	chemikálie
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgInSc4d1	speciální
papír	papír	k1gInSc4	papír
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
ruční	ruční	k2eAgNnSc4d1	ruční
balení	balení	k1gNnSc4	balení
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
lněný	lněný	k2eAgMnSc1d1	lněný
nebo	nebo	k8xC	nebo
konopný	konopný	k2eAgMnSc1d1	konopný
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
celulózový	celulózový	k2eAgInSc4d1	celulózový
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Cigaretový	cigaretový	k2eAgInSc1d1	cigaretový
papír	papír	k1gInSc1	papír
reguluje	regulovat	k5eAaImIp3nS	regulovat
rychlost	rychlost	k1gFnSc4	rychlost
hoření	hoření	k2eAgFnSc2d1	hoření
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
papíru	papír	k1gInSc2	papír
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
různé	různý	k2eAgFnPc1d1	různá
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
–	–	k?	–
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
poréznost	poréznost	k1gFnSc4	poréznost
a	a	k8xC	a
hoření	hoření	k1gNnSc4	hoření
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
uhličitan	uhličitan	k1gInSc1	uhličitan
hořečnatý	hořečnatý	k2eAgInSc1d1	hořečnatý
–	–	k?	–
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
barvu	barva	k1gFnSc4	barva
popelu	popel	k1gInSc2	popel
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
oxid	oxid	k1gInSc1	oxid
titaničitý	titaničitý	k2eAgInSc1d1	titaničitý
–	–	k?	–
zběluje	zbělovat	k5eAaImIp3nS	zbělovat
popel	popel	k1gInSc1	popel
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
dusičnan	dusičnan	k1gInSc1	dusičnan
draselný	draselný	k2eAgInSc1d1	draselný
–	–	k?	–
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
hoření	hoření	k2eAgFnSc1d1	hoření
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
popelu	popel	k1gInSc3	popel
soudržnost	soudržnost	k1gFnSc4	soudržnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tabáková	tabákový	k2eAgFnSc1d1	tabáková
směs	směs	k1gFnSc1	směs
===	===	k?	===
</s>
</p>
<p>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
tabáků	tabák	k1gInPc2	tabák
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
promíchány	promíchán	k2eAgInPc1d1	promíchán
a	a	k8xC	a
následně	následně	k6eAd1	následně
fermentovány	fermentován	k2eAgFnPc1d1	fermentována
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
odrůdy	odrůda	k1gFnPc1	odrůda
tabáků	tabák	k1gInPc2	tabák
pro	pro	k7c4	pro
směs	směs	k1gFnSc4	směs
tvoří	tvořit	k5eAaImIp3nP	tvořit
burley	burlea	k1gFnPc1	burlea
<g/>
,	,	kIx,	,
oriental	oriental	k1gMnSc1	oriental
a	a	k8xC	a
virginia	virginium	k1gNnPc1	virginium
<g/>
.	.	kIx.	.
</s>
<s>
Fermentační	fermentační	k2eAgFnPc1d1	fermentační
přísady	přísada	k1gFnPc1	přísada
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
příchutě	příchuť	k1gFnPc4	příchuť
čokolády	čokoláda	k1gFnPc4	čokoláda
<g/>
,	,	kIx,	,
brandy	brandy	k1gFnPc4	brandy
<g/>
,	,	kIx,	,
vanilky	vanilka	k1gFnPc4	vanilka
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
tabáku	tabák	k1gInSc2	tabák
==	==	k?	==
</s>
</p>
<p>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
kvalitního	kvalitní	k2eAgInSc2d1	kvalitní
tabáku	tabák	k1gInSc2	tabák
začíná	začínat	k5eAaImIp3nS	začínat
pečlivou	pečlivý	k2eAgFnSc7d1	pečlivá
přípravou	příprava	k1gFnSc7	příprava
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
sazenic	sazenice	k1gFnPc2	sazenice
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
tabáku	tabák	k1gInSc2	tabák
jsou	být	k5eAaImIp3nP	být
malá	malý	k2eAgNnPc1d1	malé
(	(	kIx(	(
<g/>
na	na	k7c6	na
1	[number]	k4	1
gram	gram	k1gInSc1	gram
jich	on	k3xPp3gInPc2	on
připadá	připadat	k5eAaImIp3nS	připadat
10	[number]	k4	10
000	[number]	k4	000
až	až	k9	až
30	[number]	k4	30
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
však	však	k9	však
během	během	k7c2	během
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
dnů	den	k1gInPc2	den
vyklíčí	vyklíčit	k5eAaPmIp3nS	vyklíčit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správné	správný	k2eAgFnSc6d1	správná
přípravě	příprava	k1gFnSc6	příprava
půdy	půda	k1gFnSc2	půda
k	k	k7c3	k
výsevu	výsev	k1gInSc3	výsev
vyrostou	vyrůst	k5eAaPmIp3nP	vyrůst
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
15	[number]	k4	15
až	až	k9	až
20	[number]	k4	20
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přesazují	přesazovat	k5eAaImIp3nP	přesazovat
na	na	k7c4	na
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
měsících	měsíc	k1gInPc6	měsíc
na	na	k7c4	na
poli	pole	k1gFnSc4	pole
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc4	rostlina
zralé	zralý	k2eAgFnPc4d1	zralá
pro	pro	k7c4	pro
sklizeň	sklizeň	k1gFnSc4	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
odrůd	odrůda	k1gFnPc2	odrůda
tabáku	tabák	k1gInSc2	tabák
–	–	k?	–
Virginia	Virginium	k1gNnSc2	Virginium
a	a	k8xC	a
Oriental	Oriental	k1gMnSc1	Oriental
–	–	k?	–
se	se	k3xPyFc4	se
sklízejí	sklízet	k5eAaImIp3nP	sklízet
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
od	od	k7c2	od
časně	časně	k6eAd1	časně
zrajících	zrající	k2eAgInPc2d1	zrající
listů	list	k1gInPc2	list
u	u	k7c2	u
země	zem	k1gFnSc2	zem
postupně	postupně	k6eAd1	postupně
směrem	směr	k1gInSc7	směr
vzhůru	vzhůru	k6eAd1	vzhůru
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vrstvy	vrstva	k1gFnPc1	vrstva
na	na	k7c6	na
stvolu	stvol	k1gInSc6	stvol
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
důležitá	důležitý	k2eAgFnSc1d1	důležitá
odrůda	odrůda	k1gFnSc1	odrůda
tabáku	tabák	k1gInSc2	tabák
–	–	k?	–
Burley	Burlea	k1gFnSc2	Burlea
–	–	k?	–
se	se	k3xPyFc4	se
sklízí	sklízet	k5eAaImIp3nS	sklízet
zpravidla	zpravidla	k6eAd1	zpravidla
jednorázově	jednorázově	k6eAd1	jednorázově
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
se	se	k3xPyFc4	se
uřízne	uříznout	k5eAaPmIp3nS	uříznout
a	a	k8xC	a
po	po	k7c6	po
usušení	usušení	k1gNnSc6	usušení
se	se	k3xPyFc4	se
odtrhnou	odtrhnout	k5eAaPmIp3nP	odtrhnout
listy	list	k1gInPc1	list
od	od	k7c2	od
stvolu	stvol	k1gInSc2	stvol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Způsob	způsob	k1gInSc1	způsob
sušení	sušení	k1gNnSc1	sušení
zásadně	zásadně	k6eAd1	zásadně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
konečnou	konečný	k2eAgFnSc4d1	konečná
kvalitu	kvalita	k1gFnSc4	kvalita
listů	list	k1gInPc2	list
<g/>
;	;	kIx,	;
profesionální	profesionální	k2eAgFnPc1d1	profesionální
dovednosti	dovednost	k1gFnPc1	dovednost
pěstitele	pěstitel	k1gMnSc2	pěstitel
jsou	být	k5eAaImIp3nP	být
rozhodující	rozhodující	k2eAgFnPc1d1	rozhodující
pro	pro	k7c4	pro
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
různých	různý	k2eAgFnPc2d1	různá
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
chutí	chuť	k1gFnPc2	chuť
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tabák	tabák	k1gInSc1	tabák
odrůdy	odrůda	k1gFnSc2	odrůda
Virginia	Virginium	k1gNnSc2	Virginium
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nS	sušit
metodou	metoda	k1gFnSc7	metoda
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
"	"	kIx"	"
<g/>
flue	flue	k1gFnPc2	flue
curing	curing	k1gInSc1	curing
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tabák	tabák	k1gInSc1	tabák
se	se	k3xPyFc4	se
pověsí	pověsit	k5eAaPmIp3nS	pověsit
do	do	k7c2	do
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
sušáren	sušárna	k1gFnPc2	sušárna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
listů	list	k1gInPc2	list
odpaří	odpařit	k5eAaPmIp3nS	odpařit
vyhřívaný	vyhřívaný	k2eAgInSc1d1	vyhřívaný
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
trvá	trvat	k5eAaImIp3nS	trvat
až	až	k9	až
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
neustále	neustále	k6eAd1	neustále
sledovat	sledovat	k5eAaImF	sledovat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Příliš	příliš	k6eAd1	příliš
vysoké	vysoký	k2eAgFnPc1d1	vysoká
i	i	k8xC	i
příliš	příliš	k6eAd1	příliš
nízké	nízký	k2eAgFnPc4d1	nízká
teploty	teplota	k1gFnPc4	teplota
ve	v	k7c6	v
kterékoli	kterýkoli	k3yIgFnSc6	kterýkoli
fázi	fáze	k1gFnSc6	fáze
sušení	sušení	k1gNnSc2	sušení
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
negativní	negativní	k2eAgInSc4d1	negativní
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tabák	tabák	k1gInSc1	tabák
odrůdy	odrůda	k1gFnSc2	odrůda
Burley	Burlea	k1gFnSc2	Burlea
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nS	sušit
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
v	v	k7c6	v
dobře	dobře	k6eAd1	dobře
větraných	větraný	k2eAgFnPc6d1	větraná
sušárnách	sušárna	k1gFnPc6	sušárna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
trvající	trvající	k2eAgInSc1d1	trvající
až	až	k6eAd1	až
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Tabák	tabák	k1gInSc1	tabák
odrůdy	odrůda	k1gFnSc2	odrůda
Oriental	Oriental	k1gMnSc1	Oriental
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nS	sušit
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
pověsí	pověsit	k5eAaPmIp3nP	pověsit
ven	ven	k6eAd1	ven
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
záření	záření	k1gNnSc3	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
usušení	usušení	k1gNnSc6	usušení
pěstitel	pěstitel	k1gMnSc1	pěstitel
listy	list	k1gInPc1	list
tabáku	tabák	k1gInSc2	tabák
třídí	třídit	k5eAaImIp3nP	třídit
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
polohy	poloha	k1gFnSc2	poloha
na	na	k7c6	na
stvolu	stvol	k1gInSc6	stvol
a	a	k8xC	a
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
zabalí	zabalit	k5eAaPmIp3nS	zabalit
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
žoků	žok	k1gInPc2	žok
a	a	k8xC	a
dodá	dodat	k5eAaPmIp3nS	dodat
na	na	k7c4	na
aukci	aukce	k1gFnSc4	aukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměry	rozměr	k1gInPc4	rozměr
cigaret	cigareta	k1gFnPc2	cigareta
==	==	k?	==
</s>
</p>
<p>
<s>
Obvod	obvod	k1gInSc1	obvod
většiny	většina	k1gFnSc2	většina
cigaret	cigareta	k1gFnPc2	cigareta
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Vyhláška	vyhláška	k1gFnSc1	vyhláška
definuje	definovat	k5eAaBmIp3nS	definovat
čtyři	čtyři	k4xCgFnPc4	čtyři
délky	délka	k1gFnPc4	délka
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
cigareta	cigareta	k1gFnSc1	cigareta
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
70	[number]	k4	70
mm	mm	kA	mm
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cigareta	cigareta	k1gFnSc1	cigareta
typu	typ	k1gInSc2	typ
KING-SIZE	KING-SIZE	k1gFnSc2	KING-SIZE
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
85	[number]	k4	85
mm	mm	kA	mm
(	(	kIx(	(
<g/>
84	[number]	k4	84
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cigareta	cigareta	k1gFnSc1	cigareta
typu	typ	k1gInSc2	typ
SUPER	super	k2eAgFnSc1d1	super
KING-SIZE	KING-SIZE	k1gFnSc1	KING-SIZE
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
100	[number]	k4	100
mm	mm	kA	mm
(	(	kIx(	(
<g/>
93	[number]	k4	93
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Cigareta	cigareta	k1gFnSc1	cigareta
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
LONGS	LONGS	kA	LONGS
(	(	kIx(	(
<g/>
LONG	LONG	kA	LONG
SIZE	SIZE	kA	SIZE
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
120	[number]	k4	120
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cigareta	cigareta	k1gFnSc1	cigareta
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
SUPER	super	k2eAgNnSc2d1	super
LONGS	LONGS	kA	LONGS
(	(	kIx(	(
<g/>
SUPER	super	k1gInSc1	super
LONG	LONG	kA	LONG
SIZE	SIZE	kA	SIZE
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
150	[number]	k4	150
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
cigareta	cigareta	k1gFnSc1	cigareta
==	==	k?	==
</s>
</p>
<p>
<s>
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
cigareta	cigareta	k1gFnSc1	cigareta
je	být	k5eAaImIp3nS	být
alternativní	alternativní	k2eAgInSc4d1	alternativní
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
speciální	speciální	k2eAgFnSc2d1	speciální
nikotinové	nikotinový	k2eAgFnSc2d1	nikotinová
náplně	náplň	k1gFnSc2	náplň
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
aerosol	aerosol	k1gInSc1	aerosol
podobný	podobný	k2eAgInSc1d1	podobný
kouři	kouř	k1gInSc6	kouř
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
kuřák	kuřák	k1gInSc1	kuřák
vdechuje	vdechovat	k5eAaImIp3nS	vdechovat
<g/>
.	.	kIx.	.
</s>
<s>
Aerosol	aerosol	k1gInSc1	aerosol
bývá	bývat	k5eAaImIp3nS	bývat
prodejci	prodejce	k1gMnPc7	prodejce
elektronických	elektronický	k2eAgFnPc2d1	elektronická
cigaret	cigareta	k1gFnPc2	cigareta
někdy	někdy	k6eAd1	někdy
mylně	mylně	k6eAd1	mylně
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
<g/>
)	)	kIx)	)
pára	pára	k1gFnSc1	pára
<g/>
.	.	kIx.	.
</s>
<s>
Škodliviny	škodlivina	k1gFnPc1	škodlivina
v	v	k7c6	v
aerosolu	aerosol	k1gInSc6	aerosol
elektronických	elektronický	k2eAgFnPc2d1	elektronická
cigaret	cigareta	k1gFnPc2	cigareta
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
koncentracích	koncentrace	k1gFnPc6	koncentrace
než	než	k8xS	než
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
cigaretách	cigareta	k1gFnPc6	cigareta
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
látky	látka	k1gFnSc2	látka
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Aerosol	aerosol	k1gInSc1	aerosol
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
dehet	dehet	k1gInSc1	dehet
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
škodlivé	škodlivý	k2eAgFnPc4d1	škodlivá
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
vznikající	vznikající	k2eAgFnPc4d1	vznikající
rozkladem	rozklad	k1gInSc7	rozklad
propylenglykolu	propylenglykola	k1gFnSc4	propylenglykola
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
koncentrace	koncentrace	k1gFnSc1	koncentrace
těchto	tento	k3xDgFnPc2	tento
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
jsou	být	k5eAaImIp3nP	být
devětkrát	devětkrát	k6eAd1	devětkrát
až	až	k9	až
450	[number]	k4	450
<g/>
krát	krát	k6eAd1	krát
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
cigaretách	cigareta	k1gFnPc6	cigareta
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc4	tento
organické	organický	k2eAgFnPc4d1	organická
látky	látka	k1gFnPc4	látka
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
ve	v	k7c6	v
farmaceutických	farmaceutický	k2eAgInPc6d1	farmaceutický
inhalátorech	inhalátor	k1gInPc6	inhalátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Náplně	náplň	k1gFnPc4	náplň
do	do	k7c2	do
elektronických	elektronický	k2eAgFnPc2d1	elektronická
cigaret	cigareta	k1gFnPc2	cigareta
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
příchutích	příchuť	k1gFnPc6	příchuť
a	a	k8xC	a
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
koncentrací	koncentrace	k1gFnSc7	koncentrace
nikotinu	nikotin	k1gInSc2	nikotin
<g/>
.	.	kIx.	.
</s>
<s>
Zakoupíte	zakoupit	k5eAaPmIp2nP	zakoupit
je	on	k3xPp3gNnPc4	on
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
kapalné	kapalný	k2eAgFnSc6d1	kapalná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
elektronická	elektronický	k2eAgFnSc1d1	elektronická
cigareta	cigareta	k1gFnSc1	cigareta
spolehlivě	spolehlivě	k6eAd1	spolehlivě
fungovala	fungovat	k5eAaImAgFnS	fungovat
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
náplň	náplň	k1gFnSc1	náplň
průběžně	průběžně	k6eAd1	průběžně
doplňovat	doplňovat	k5eAaImF	doplňovat
do	do	k7c2	do
patrony	patrona	k1gFnSc2	patrona
(	(	kIx(	(
<g/>
cartrige	cartrige	k1gInSc1	cartrige
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výhody	výhoda	k1gFnPc1	výhoda
elektronických	elektronický	k2eAgFnPc2d1	elektronická
cigaret	cigareta	k1gFnPc2	cigareta
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
výhody	výhoda	k1gFnPc4	výhoda
kouření	kouření	k1gNnSc2	kouření
elektronických	elektronický	k2eAgFnPc2d1	elektronická
cigaret	cigareta	k1gFnPc2	cigareta
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
tyto	tento	k3xDgInPc4	tento
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Elektronická	elektronický	k2eAgFnSc1d1	elektronická
cigareta	cigareta	k1gFnSc1	cigareta
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
<g/>
,	,	kIx,	,
než	než	k8xS	než
klasická	klasický	k2eAgFnSc1d1	klasická
cigareta	cigareta	k1gFnSc1	cigareta
<g/>
.	.	kIx.	.
====	====	k?	====
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
klasickým	klasický	k2eAgFnPc3d1	klasická
cigaretám	cigareta	k1gFnPc3	cigareta
elektronická	elektronický	k2eAgFnSc1d1	elektronická
cigareta	cigareta	k1gFnSc1	cigareta
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
dehet	dehet	k1gInSc4	dehet
ani	ani	k8xC	ani
oxid	oxid	k1gInSc4	oxid
uhelnatý	uhelnatý	k2eAgInSc4d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
tělo	tělo	k1gNnSc1	tělo
tak	tak	k9	tak
není	být	k5eNaImIp3nS	být
zatěžováno	zatěžován	k2eAgNnSc1d1	zatěžováno
těmito	tento	k3xDgFnPc7	tento
nebezpečnými	bezpečný	k2eNgFnPc7d1	nebezpečná
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kouření	kouření	k1gNnSc3	kouření
elektronické	elektronický	k2eAgFnSc2d1	elektronická
cigarety	cigareta	k1gFnSc2	cigareta
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
nákladné	nákladný	k2eAgNnSc1d1	nákladné
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ušetřit	ušetřit	k5eAaPmF	ušetřit
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
násobek	násobek	k1gInSc1	násobek
financí	finance	k1gFnPc2	finance
vydaných	vydaný	k2eAgInPc2d1	vydaný
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Záleží	záležet	k5eAaImIp3nS	záležet
zde	zde	k6eAd1	zde
na	na	k7c6	na
několika	několik	k4yIc6	několik
faktorech	faktor	k1gInPc6	faktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
náklady	náklad	k1gInPc1	náklad
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
(	(	kIx(	(
<g/>
typ	typ	k1gInSc1	typ
e-cigarety	eigareta	k1gFnSc2	e-cigareta
<g/>
,	,	kIx,	,
použitá	použitý	k2eAgFnSc1d1	použitá
náplň	náplň	k1gFnSc1	náplň
<g/>
,	,	kIx,	,
styl	styl	k1gInSc1	styl
kouření	kouření	k1gNnSc2	kouření
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Kouření	kouření	k1gNnSc4	kouření
elektronické	elektronický	k2eAgFnSc2d1	elektronická
cigarety	cigareta	k1gFnSc2	cigareta
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
hlediska	hledisko	k1gNnSc2	hledisko
stále	stále	k6eAd1	stále
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
kouření	kouření	k1gNnSc1	kouření
klasických	klasický	k2eAgFnPc2d1	klasická
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Elektronickou	elektronický	k2eAgFnSc7d1	elektronická
cigaretou	cigareta	k1gFnSc7	cigareta
lze	lze	k6eAd1	lze
plnohodnotně	plnohodnotně	k6eAd1	plnohodnotně
nahradit	nahradit	k5eAaPmF	nahradit
klasickou	klasický	k2eAgFnSc4d1	klasická
cigaretu	cigareta	k1gFnSc4	cigareta
====	====	k?	====
</s>
</p>
<p>
<s>
Při	při	k7c6	při
správném	správný	k2eAgInSc6d1	správný
výběru	výběr	k1gInSc6	výběr
elektronické	elektronický	k2eAgFnSc2d1	elektronická
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
e-cigaretu	eigareta	k1gFnSc4	e-cigareta
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
100	[number]	k4	100
<g/>
%	%	kIx~	%
náhradu	náhrada	k1gFnSc4	náhrada
klasické	klasický	k2eAgFnSc2d1	klasická
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
elektronická	elektronický	k2eAgFnSc1d1	elektronická
cigareta	cigareta	k1gFnSc1	cigareta
se	s	k7c7	s
správnou	správný	k2eAgFnSc7d1	správná
sílou	síla	k1gFnSc7	síla
náplně	náplň	k1gFnSc2	náplň
zjednoduší	zjednodušet	k5eAaPmIp3nS	zjednodušet
přechod	přechod	k1gInSc1	přechod
z	z	k7c2	z
kouření	kouření	k1gNnSc2	kouření
klasické	klasický	k2eAgFnSc2d1	klasická
cigarety	cigareta	k1gFnSc2	cigareta
na	na	k7c4	na
elektronickou	elektronický	k2eAgFnSc4d1	elektronická
cigaretu	cigareta	k1gFnSc4	cigareta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pára	pára	k1gFnSc1	pára
z	z	k7c2	z
elektronické	elektronický	k2eAgFnSc2d1	elektronická
cigarety	cigareta	k1gFnSc2	cigareta
nezatěžuje	zatěžovat	k5eNaImIp3nS	zatěžovat
okolí	okolí	k1gNnSc1	okolí
====	====	k?	====
</s>
</p>
<p>
<s>
Pára	pára	k1gFnSc1	pára
z	z	k7c2	z
elektronické	elektronický	k2eAgFnSc2d1	elektronická
cigarety	cigareta	k1gFnSc2	cigareta
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
minimum	minimum	k1gNnSc1	minimum
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
kouřem	kouř	k1gInSc7	kouř
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
E-cigareta	Eigaret	k1gMnSc4	E-cigaret
nešíří	šířit	k5eNaImIp3nS	šířit
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
zápach	zápach	k1gInSc1	zápach
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
případě	případ	k1gInSc6	případ
použití	použití	k1gNnSc2	použití
ovocných	ovocný	k2eAgFnPc2d1	ovocná
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
vydává	vydávat	k5eAaImIp3nS	vydávat
příjemné	příjemný	k2eAgNnSc1d1	příjemné
aroma	aroma	k1gNnSc1	aroma
dané	daný	k2eAgFnSc2d1	daná
příchutě	příchuť	k1gFnSc2	příchuť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bidi	Bidi	k6eAd1	Bidi
</s>
</p>
<p>
<s>
Camel	Camel	k1gInSc1	Camel
(	(	kIx(	(
<g/>
cigarety	cigareta	k1gFnPc1	cigareta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marlboro	Marlboro	k1gNnSc1	Marlboro
(	(	kIx(	(
<g/>
cigarety	cigareta	k1gFnPc1	cigareta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cigaretová	cigaretový	k2eAgFnSc1d1	cigaretová
špička	špička	k1gFnSc1	špička
</s>
</p>
<p>
<s>
Doutník	doutník	k1gInSc1	doutník
</s>
</p>
<p>
<s>
Kouření	kouření	k1gNnSc1	kouření
</s>
</p>
<p>
<s>
Nikotin	nikotin	k1gInSc1	nikotin
</s>
</p>
<p>
<s>
Proud	proud	k1gInSc1	proud
bočního	boční	k2eAgInSc2d1	boční
dýmu	dým	k1gInSc2	dým
</s>
</p>
<p>
<s>
Složky	složka	k1gFnPc1	složka
cigaretového	cigaretový	k2eAgInSc2d1	cigaretový
kouře	kouř	k1gInSc2	kouř
</s>
</p>
<p>
<s>
Varování	varování	k1gNnSc1	varování
na	na	k7c6	na
krabičkách	krabička	k1gFnPc6	krabička
cigaret	cigareta	k1gFnPc2	cigareta
</s>
</p>
<p>
<s>
Zdravotní	zdravotní	k2eAgNnPc4d1	zdravotní
rizika	riziko	k1gNnPc4	riziko
kouření	kouření	k1gNnSc2	kouření
tabáku	tabák	k1gInSc2	tabák
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cigareta	cigareta	k1gFnSc1	cigareta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
cigareta	cigareta	k1gFnSc1	cigareta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Cigarretty	Cigarretta	k1gFnSc2	Cigarretta
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cigareta	cigareta	k1gFnSc1	cigareta
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Cigareta	cigareta	k1gFnSc1	cigareta
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
prodávat	prodávat	k5eAaImF	prodávat
jen	jen	k9	jen
samohasící	samohasící	k2eAgFnPc4d1	samohasící
cigarety	cigareta	k1gFnPc4	cigareta
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
https://www.stream.cz/fenomen/537164-cigarety-zabijacka-rozkos	[url]	k1gInSc1	https://www.stream.cz/fenomen/537164-cigarety-zabijacka-rozkos
-	-	kIx~	-
díl	díl	k1gInSc1	díl
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
pořadu	pořad	k1gInSc2	pořad
Fenomén	fenomén	k1gNnPc2	fenomén
na	na	k7c6	na
internetové	internetový	k2eAgFnSc6d1	internetová
televizi	televize	k1gFnSc6	televize
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
pojednávající	pojednávající	k2eAgFnSc1d1	pojednávající
o	o	k7c6	o
cigaretách	cigareta	k1gFnPc6	cigareta
</s>
</p>
<p>
<s>
http://www.kurakovaplice.cz/koureni_cigaret/	[url]	k?	http://www.kurakovaplice.cz/koureni_cigaret/
-	-	kIx~	-
web	web	k1gInSc1	web
Kuřákova	kuřákův	k2eAgFnSc1d1	kuřákova
plíce	plíce	k1gFnSc1	plíce
o	o	k7c6	o
cigaretách	cigareta	k1gFnPc6	cigareta
a	a	k8xC	a
kouření	kouření	k1gNnSc3	kouření
</s>
</p>
