<p>
<s>
Filatorium	Filatorium	k1gNnSc1	Filatorium
je	být	k5eAaImIp3nS	být
historické	historický	k2eAgFnPc4d1	historická
mechanické	mechanický	k2eAgFnPc4d1	mechanická
zařízeni	zařízen	k2eAgMnPc1d1	zařízen
ke	k	k7c3	k
skaní	skaní	k1gNnSc3	skaní
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
.	.	kIx.	.
<g/>
Filatorium	Filatorium	k1gNnSc1	Filatorium
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
textilních	textilní	k2eAgInPc2d1	textilní
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
mechanizmů	mechanizmus	k1gInPc2	mechanizmus
poháněných	poháněný	k2eAgInPc2d1	poháněný
vodním	vodní	k2eAgNnSc7d1	vodní
kolem	kolo	k1gNnSc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
filatoria	filatorium	k1gNnSc2	filatorium
není	být	k5eNaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pochází	pocházet	k5eAaImIp3nS	pocházet
nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
slovníku	slovník	k1gInSc2	slovník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1221	[number]	k4	1221
<g/>
,	,	kIx,	,
k	k	k7c3	k
praktickému	praktický	k2eAgNnSc3d1	praktické
použití	použití	k1gNnSc3	použití
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
poháněných	poháněný	k2eAgFnPc2d1	poháněná
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
žentourem	žentour	k1gInSc7	žentour
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
městě	město	k1gNnSc6	město
Lucca	Lucc	k1gInSc2	Lucc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1272	[number]	k4	1272
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
Derby	derby	k1gNnSc6	derby
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1718	[number]	k4	1718
nebo	nebo	k8xC	nebo
1719	[number]	k4	1719
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
skárna	skárno	k1gNnSc2	skárno
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
kompletně	kompletně	k6eAd1	kompletně
vybavená	vybavený	k2eAgFnSc1d1	vybavená
filatoriemi	filatorie	k1gFnPc7	filatorie
poháněnými	poháněný	k2eAgFnPc7d1	poháněná
vodním	vodní	k2eAgInPc3d1	vodní
kolem	kolem	k7c2	kolem
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
této	tento	k3xDgFnSc2	tento
továrny	továrna	k1gFnSc2	továrna
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
celosvětový	celosvětový	k2eAgInSc4d1	celosvětový
začátek	začátek	k1gInSc4	začátek
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
textilií	textilie	k1gFnPc2	textilie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
skaní	skaní	k1gNnSc2	skaní
na	na	k7c6	na
filatoriu	filatorium	k1gNnSc6	filatorium
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
se	se	k3xPyFc4	se
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
odvíjejí	odvíjet	k5eAaImIp3nP	odvíjet
z	z	k7c2	z
cívky	cívka	k1gFnSc2	cívka
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
sdružené	sdružený	k2eAgFnPc1d1	sdružená
hedvábné	hedvábný	k2eAgFnPc1d1	hedvábná
niti	nit	k1gFnPc1	nit
<g/>
,	,	kIx,	,
procházejí	procházet	k5eAaImIp3nP	procházet
vodičem	vodič	k1gInSc7	vodič
(	(	kIx(	(
<g/>
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
)	)	kIx)	)
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
S	s	k7c7	s
a	a	k8xC	a
namotávají	namotávat	k5eAaImIp3nP	namotávat
se	se	k3xPyFc4	se
na	na	k7c4	na
naviják	naviják	k1gInSc4	naviják
umístěný	umístěný	k2eAgInSc4d1	umístěný
nad	nad	k7c7	nad
vodičem	vodič	k1gInSc7	vodič
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vodič	vodič	k1gInSc1	vodič
otáčí	otáčet	k5eAaImIp3nS	otáčet
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
viják	viják	k1gInSc1	viják
<g/>
,	,	kIx,	,
procházející	procházející	k2eAgFnPc1d1	procházející
niti	nit	k1gFnPc1	nit
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
zakrucují	zakrucovat	k5eAaImIp3nP	zakrucovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
se	se	k3xPyFc4	se
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
současně	současně	k6eAd1	současně
až	až	k8xS	až
240	[number]	k4	240
skaných	skaný	k2eAgFnPc2d1	skaná
nití	nit	k1gFnPc2	nit
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
až	až	k9	až
padesátinásobným	padesátinásobný	k2eAgInSc7d1	padesátinásobný
oproti	oproti	k7c3	oproti
ručnímu	ruční	k2eAgNnSc3d1	ruční
skaní	skaní	k1gNnSc3	skaní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejstarší	starý	k2eAgInPc1d3	nejstarší
popisy	popis	k1gInPc1	popis
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejstarší	starý	k2eAgFnSc6d3	nejstarší
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
stroj	stroj	k1gInSc1	stroj
nezachoval	zachovat	k5eNaPmAgInS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
podrobné	podrobný	k2eAgInPc1d1	podrobný
popisy	popis	k1gInPc1	popis
a	a	k8xC	a
nákresy	nákres	k1gInPc1	nákres
historických	historický	k2eAgFnPc2d1	historická
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
:	:	kIx,	:
nejstarší	starý	k2eAgInSc1d3	nejstarší
mechanismus	mechanismus	k1gInSc1	mechanismus
s	s	k7c7	s
převody	převod	k1gInPc7	převod
ozubeného	ozubený	k2eAgNnSc2d1	ozubené
kola	kolo	k1gNnSc2	kolo
poháněného	poháněný	k2eAgInSc2d1	poháněný
mlýnským	mlýnský	k2eAgNnSc7d1	mlýnské
kolem	kolo	k1gNnSc7	kolo
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgMnS	dochovat
v	v	k7c6	v
kresbě	kresba	k1gFnSc6	kresba
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgInPc1d1	přesný
popisy	popis	k1gInPc1	popis
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Victoria	Victorium	k1gNnSc2	Victorium
Zonzy	Zonza	k1gFnSc2	Zonza
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
Padovy	Padova	k1gFnSc2	Padova
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1607	[number]	k4	1607
<g/>
..	..	k?	..
Nejlepší	dobrý	k2eAgInPc4d3	nejlepší
výrobní	výrobní	k2eAgInPc4d1	výrobní
popisy	popis	k1gInPc4	popis
filatorií	filatorium	k1gNnPc2	filatorium
s	s	k7c7	s
rytinami	rytina	k1gFnPc7	rytina
přinesla	přinést	k5eAaPmAgFnS	přinést
první	první	k4xOgFnSc1	první
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1771	[number]	k4	1771
<g/>
-	-	kIx~	-
<g/>
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
věnovala	věnovat	k5eAaImAgFnS	věnovat
celý	celý	k2eAgInSc4d1	celý
jeden	jeden	k4xCgInSc4	jeden
svazek	svazek	k1gInSc4	svazek
hedvábnictví	hedvábnictví	k1gNnSc2	hedvábnictví
(	(	kIx(	(
<g/>
Soierie	Soierie	k1gFnSc1	Soierie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dochované	dochovaný	k2eAgInPc1d1	dochovaný
příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Dochovala	dochovat	k5eAaPmAgNnP	dochovat
se	se	k3xPyFc4	se
dřevěná	dřevěný	k2eAgNnPc1d1	dřevěné
filatoria	filatorium	k1gNnPc1	filatorium
s	s	k7c7	s
železnými	železný	k2eAgFnPc7d1	železná
součástkami	součástka	k1gFnPc7	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
skárna	skárna	k1gFnSc1	skárna
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
Derby	derby	k1gNnSc6	derby
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
filatoria	filatorium	k1gNnSc2	filatorium
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
muzeích	muzeum	k1gNnPc6	muzeum
hedvábnictví	hedvábnictví	k1gNnSc2	hedvábnictví
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
Lyon	Lyon	k1gInSc1	Lyon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
severoitalských	severoitalský	k2eAgNnPc6d1	severoitalské
muzeích	muzeum	k1gNnPc6	muzeum
v	v	k7c6	v
Abbadia	Abbadium	k1gNnSc2	Abbadium
Lariana	Lariana	k1gFnSc1	Lariana
(	(	kIx(	(
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Garlate	Garlat	k1gInSc5	Garlat
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Como	Como	k6eAd1	Como
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jihoitalském	jihoitalský	k2eAgMnSc6d1	jihoitalský
San	San	k1gMnSc6	San
Leucio	Leucio	k6eAd1	Leucio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
:	:	kIx,	:
Filatorie	Filatorie	k1gFnSc1	Filatorie
v	v	k7c6	v
našich	náš	k3xOp1gNnPc6	náš
sídlech	sídlo	k1gNnPc6	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
staveb	stavba	k1gFnPc2	stavba
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
Dějiny	dějiny	k1gFnPc1	dějiny
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
editor	editor	k1gInSc1	editor
Petr	Petr	k1gMnSc1	Petr
Mikota	Mikota	k1gFnSc1	Mikota
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
</p>
