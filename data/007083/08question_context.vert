<s>
Rtuť	rtuť	k1gFnSc1	rtuť
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Hg	Hg	k1gFnSc2	Hg
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Hydrargyrum	Hydrargyrum	k1gInSc1	Hydrargyrum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
těžký	těžký	k2eAgMnSc1d1	těžký
<g/>
,	,	kIx,	,
toxický	toxický	k2eAgInSc1d1	toxický
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
slitin	slitina	k1gFnPc2	slitina
(	(	kIx(	(
<g/>
amalgámů	amalgám	k1gInPc2	amalgám
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
náplň	náplň	k1gFnSc4	náplň
různých	různý	k2eAgInPc2d1	různý
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
teploměry	teploměr	k1gInPc1	teploměr
<g/>
,	,	kIx,	,
barometry	barometr	k1gInPc1	barometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
