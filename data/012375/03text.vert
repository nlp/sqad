<p>
<s>
Kobylka	kobylka	k1gFnSc1	kobylka
sága	sága	k1gFnSc1	sága
(	(	kIx(	(
<g/>
Saga	Saga	k1gMnSc1	Saga
pedo	pedo	k1gMnSc1	pedo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
velká	velký	k2eAgFnSc1d1	velká
bezkřídlá	bezkřídlý	k2eAgFnSc1d1	bezkřídlá
kobylka	kobylka	k1gFnSc1	kobylka
dorůstající	dorůstající	k2eAgFnSc1d1	dorůstající
až	až	k9	až
7	[number]	k4	7
cm	cm	kA	cm
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
kladélka	kladélko	k1gNnSc2	kladélko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
zelené	zelená	k1gFnSc2	zelená
<g/>
,	,	kIx,	,
jižněji	jižně	k6eAd2	jižně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jedinci	jedinec	k1gMnPc1	jedinec
též	též	k9	též
hnědaví	hnědavý	k2eAgMnPc1d1	hnědavý
a	a	k8xC	a
skvrnití	skvrnitý	k2eAgMnPc1d1	skvrnitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
kobylka	kobylka	k1gFnSc1	kobylka
sága	sága	k1gFnSc1	sága
chráněna	chránit	k5eAaImNgFnS	chránit
zákonem	zákon	k1gInSc7	zákon
jako	jako	k8xS	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Samci	Samek	k1gMnPc1	Samek
nejsou	být	k5eNaImIp3nP	být
známi	znám	k2eAgMnPc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
jen	jen	k9	jen
partenogenezí	partenogeneze	k1gFnSc7	partenogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Klade	klást	k5eAaImIp3nS	klást
neobvykle	obvykle	k6eNd1	obvykle
velká	velký	k2eAgNnPc4d1	velké
vajíčka	vajíčko	k1gNnPc4	vajíčko
(	(	kIx(	(
<g/>
11,8	[number]	k4	11,8
×	×	k?	×
3,8	[number]	k4	3,8
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
líhne	líhnout	k5eAaImIp3nS	líhnout
nymfa	nymfa	k1gFnSc1	nymfa
podobná	podobný	k2eAgFnSc1d1	podobná
dospělci	dospělec	k1gMnPc7	dospělec
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
dravě	dravě	k6eAd1	dravě
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
sarančaty	saranče	k1gNnPc7	saranče
a	a	k8xC	a
jiným	jiný	k2eAgInSc7d1	jiný
hmyzem	hmyz	k1gInSc7	hmyz
(	(	kIx(	(
<g/>
i	i	k9	i
kudlankou	kudlanka	k1gFnSc7	kudlanka
nábožnou	nábožný	k2eAgFnSc7d1	nábožná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
s	s	k7c7	s
dospělou	dospělý	k2eAgFnSc7d1	dospělá
ságou	sága	k1gFnSc7	sága
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pontomediteránní	pontomediteránní	k2eAgInSc1d1	pontomediteránní
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
od	od	k7c2	od
Španělska	Španělsko	k1gNnSc2	Španělsko
až	až	k9	až
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
areálu	areál	k1gInSc2	areál
druhu	druh	k1gInSc2	druh
tvoří	tvořit	k5eAaImIp3nP	tvořit
Wallis	Wallis	k1gFnPc1	Wallis
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc6	okolí
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
xerotermních	xerotermní	k2eAgInPc6d1	xerotermní
kopcích	kopec	k1gInPc6	kopec
(	(	kIx(	(
<g/>
Pálava	Pálava	k1gFnSc1	Pálava
<g/>
,	,	kIx,	,
Devínská	Devínský	k2eAgFnSc1d1	Devínská
Kobyla	kobyla	k1gFnSc1	kobyla
a	a	k8xC	a
jižní	jižní	k2eAgInPc1d1	jižní
výběžky	výběžek	k1gInPc1	výběžek
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šíření	šíření	k1gNnSc1	šíření
ságy	sága	k1gFnSc2	sága
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pomalé	pomalý	k2eAgInPc4d1	pomalý
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
přítomnost	přítomnost	k1gFnSc1	přítomnost
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nejbližším	blízký	k2eAgNnSc6d3	nejbližší
okolí	okolí	k1gNnSc6	okolí
žije	žít	k5eAaImIp3nS	žít
od	od	k7c2	od
teplého	teplý	k2eAgNnSc2d1	teplé
poledového	poledový	k2eAgNnSc2d1	poledové
stepního	stepní	k2eAgNnSc2d1	stepní
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kobylka	kobylka	k1gFnSc1	kobylka
sága	ságo	k1gNnSc2	ságo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
