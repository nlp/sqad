<s>
Číčovice	Číčovice	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
obci	obec	k1gFnSc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
části	část	k1gFnSc6
obce	obec	k1gFnSc2
Nadějkov	Nadějkov	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Číčovice	Číčovice	k1gFnSc2
(	(	kIx(
<g/>
Nadějkov	Nadějkov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Číčovice	Číčovice	k1gFnSc1
Malé	Malé	k2eAgFnSc2d1
Číčovice	Číčovice	k1gFnSc2
od	od	k7c2
jihovýchodu	jihovýchod	k1gInSc2
</s>
<s>
znakvlajka	znakvlajka	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ020A	CZ020A	k4
539147	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
</s>
<s>
Roztoky	Roztoky	k1gInPc1
Obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Černošice	Černošice	k1gFnSc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha-západ	Praha-západ	k1gInSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
20	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Středočeský	středočeský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
20	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
308	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
6,52	6,52	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
283	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
262	#num#	k4
68	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
2	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
2	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Číčovice	Číčovice	k1gFnSc1
16252	#num#	k4
68	#num#	k4
Středokluky	Středokluk	k1gInPc7
info@cicovice.cz	info@cicovice.cz	k1gMnSc1
Starostka	starostka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Chuchutová	Chuchutový	k2eAgFnSc1d1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.cicovice.cz	www.cicovice.cz	k1gInSc1
</s>
<s>
Číčovice	Číčovice	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
403148	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Číčovice	Číčovice	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Praha-západ	Praha-západ	k1gInSc1
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
asi	asi	k9
16	#num#	k4
km	km	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
centra	centrum	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
2,5	2,5	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Středokluky	Středokluk	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
308	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
vesnice	vesnice	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
Velké	velký	k2eAgFnPc1d1
Číčovice	Číčovice	k1gFnPc1
a	a	k8xC
500	#num#	k4
m	m	kA
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
od	od	k7c2
nich	on	k3xPp3gFnPc2
Malé	Malé	k2eAgFnPc2d1
Číčovice	Číčovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
vesnice	vesnice	k1gFnPc1
mají	mít	k5eAaImIp3nP
zemědělský	zemědělský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
čítá	čítat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
35	#num#	k4
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1542	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Oficiální	oficiální	k2eAgInSc1d1
název	název	k1gInSc1
obce	obec	k1gFnSc2
je	být	k5eAaImIp3nS
Číčovice	Číčovice	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
neboť	neboť	k8xC
obec	obec	k1gFnSc1
Malé	Malé	k2eAgFnSc2d1
Číčovice	Číčovice	k1gFnSc2
a	a	k8xC
obec	obec	k1gFnSc1
Velké	velký	k2eAgFnPc1d1
Číčovice	Číčovice	k1gFnPc1
byly	být	k5eAaImAgFnP
sloučeny	sloučit	k5eAaPmNgFnP
do	do	k7c2
obce	obec	k1gFnSc2
Číčovice	Číčovice	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Obec	obec	k1gFnSc1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
dvou	dva	k4xCgNnPc6
katastrálních	katastrální	k2eAgNnPc6d1
územích	území	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
si	se	k3xPyFc3
zachovaly	zachovat	k5eAaPmAgInP
názvy	název	k1gInPc1
původních	původní	k2eAgFnPc2d1
vsí	ves	k1gFnPc2
<g/>
:	:	kIx,
Velké	velký	k2eAgFnPc1d1
Číčovice	Číčovice	k1gFnPc1
a	a	k8xC
Malé	Malé	k2eAgFnPc1d1
Číčovice	Číčovice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Územněsprávní	územněsprávní	k2eAgNnSc4d1
začlenění	začlenění	k1gNnSc4
</s>
<s>
Dějiny	dějiny	k1gFnPc1
územněsprávního	územněsprávní	k2eAgNnSc2d1
začleňování	začleňování	k1gNnSc2
zahrnují	zahrnovat	k5eAaImIp3nP
období	období	k1gNnSc4
od	od	k7c2
roku	rok	k1gInSc2
1850	#num#	k4
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
chronologickém	chronologický	k2eAgInSc6d1
přehledu	přehled	k1gInSc6
je	být	k5eAaImIp3nS
uvedena	uvést	k5eAaPmNgFnS
územně	územně	k6eAd1
administrativní	administrativní	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
obce	obec	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ke	k	k7c3
změně	změna	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
<g/>
:	:	kIx,
</s>
<s>
1850	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
kraj	kraj	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
i	i	k8xC
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
Smíchov	Smíchov	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1855	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
kraj	kraj	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
Smíchov	Smíchov	k1gInSc1
</s>
<s>
1868	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
i	i	k8xC
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
Smíchov	Smíchov	k1gInSc1
</s>
<s>
1927	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
okres	okres	k1gInSc1
Praha-venkov	Praha-venkov	k1gInSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
Praha-západ	Praha-západ	k1gInSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1939	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
Oberlandrat	Oberlandrat	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
okres	okres	k1gInSc1
Praha-venkov	Praha-venkov	k1gInSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
Praha-západ	Praha-západ	k1gInSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1942	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
Oberlandrat	Oberlandrat	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
okres	okres	k1gInSc1
Praha-venkov-sever	Praha-venkov-sever	k1gInSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
Praha-západ	Praha-západ	k1gInSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1945	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
správní	správní	k2eAgInSc1d1
okres	okres	k1gInSc1
Praha-venkov-sever	Praha-venkov-sever	k1gInSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
Praha-západ	Praha-západ	k1gInSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1949	#num#	k4
Pražský	pražský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Praha-západ	Praha-západ	k1gInSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1960	#num#	k4
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Praha-západ	Praha-západ	k1gInSc1
</s>
<s>
2003	#num#	k4
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
Černošice	Černošice	k1gFnSc2
</s>
<s>
Rok	rok	k1gInSc1
1932	#num#	k4
</s>
<s>
Ve	v	k7c6
vsi	ves	k1gFnSc6
Malé	Malé	k2eAgFnSc2d1
Čičovice	Čičovice	k1gFnSc2
(	(	kIx(
<g/>
252	#num#	k4
obyvatel	obyvatel	k1gMnSc1
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
evidovány	evidovat	k5eAaImNgFnP
tyto	tento	k3xDgFnPc1
živnosti	živnost	k1gFnPc1
a	a	k8xC
obchody	obchod	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
družstvo	družstvo	k1gNnSc1
pro	pro	k7c4
rozvod	rozvod	k1gInSc4
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
holič	holič	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
hostince	hostinec	k1gInSc2
<g/>
,	,	kIx,
kolář	kolář	k1gMnSc1
<g/>
,	,	kIx,
konsum	konsum	k1gInSc1
Včela	včela	k1gFnSc1
<g/>
,	,	kIx,
kovář	kovář	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
obuvníci	obuvník	k1gMnPc1
<g/>
,	,	kIx,
11	#num#	k4
rolníků	rolník	k1gMnPc2
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
se	s	k7c7
smíšeným	smíšený	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
trafika	trafika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vsi	ves	k1gFnSc6
Velké	velký	k2eAgFnSc2d1
Čičovice	Čičovice	k1gFnSc2
(	(	kIx(
<g/>
210	#num#	k4
obyvatel	obyvatel	k1gMnSc1
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
evidovány	evidovat	k5eAaImNgFnP
tyto	tento	k3xDgFnPc1
živnosti	živnost	k1gFnPc1
a	a	k8xC
obchody	obchod	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
obchod	obchod	k1gInSc1
s	s	k7c7
dobytkem	dobytek	k1gInSc7
<g/>
,	,	kIx,
družstvo	družstvo	k1gNnSc4
pro	pro	k7c4
rozvod	rozvod	k1gInSc4
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
ve	v	k7c6
Velkých	velký	k2eAgFnPc6d1
Číčovicích	Číčovice	k1gFnPc6
<g/>
,	,	kIx,
2	#num#	k4
hostince	hostinec	k1gInSc2
<g/>
,	,	kIx,
kolář	kolář	k1gMnSc1
<g/>
,	,	kIx,
kovář	kovář	k1gMnSc1
<g/>
,	,	kIx,
mlýn	mlýn	k1gInSc1
<g/>
,	,	kIx,
4	#num#	k4
rolníci	rolník	k1gMnPc1
<g/>
,	,	kIx,
2	#num#	k4
řezníci	řezník	k1gMnPc1
<g/>
,	,	kIx,
2	#num#	k4
obchody	obchod	k1gInPc4
se	s	k7c7
smíšeným	smíšený	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
trafika	trafika	k1gFnSc1
<g/>
,	,	kIx,
truhlář	truhlář	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
zámečníci	zámečník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Správním	správní	k2eAgNnSc7d1
územím	území	k1gNnSc7
obce	obec	k1gFnSc2
protéká	protékat	k5eAaImIp3nS
Zákolanský	Zákolanský	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
koryto	koryto	k1gNnSc1
a	a	k8xC
břehy	břeh	k1gInPc1
jsou	být	k5eAaImIp3nP
chráněné	chráněný	k2eAgInPc1d1
jako	jako	k8xC,k8xS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Zákolanský	Zákolanský	k2eAgInSc1d1
potok	potok	k1gInSc4
s	s	k7c7
populací	populace	k1gFnSc7
raka	rak	k1gMnSc2
kamenáče	kamenáč	k1gMnSc2
a	a	k8xC
raka	rak	k1gMnSc2
říčního	říční	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižně	jižně	k6eAd1
od	od	k7c2
Malých	Malých	k2eAgFnPc2d1
Číčovic	Číčovice	k1gFnPc2
se	se	k3xPyFc4
poblíž	poblíž	k7c2
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
nachází	nacházet	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Čičovický	Čičovický	k2eAgInSc1d1
kamýk	kamýk	k1gInSc1
(	(	kIx(
<g/>
345	#num#	k4
m	m	kA
<g/>
)	)	kIx)
–	–	k?
skalnatý	skalnatý	k2eAgInSc4d1
buližníkový	buližníkový	k2eAgInSc4d1
vršek	vršek	k1gInSc4
s	s	k7c7
paleontologickým	paleontologický	k2eAgNnSc7d1
nalezištěm	naleziště	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
za	za	k7c4
Číčovice	Číčovice	k1gFnPc4
podle	podle	k7c2
výsledků	výsledek	k1gInPc2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
včetně	včetně	k7c2
místních	místní	k2eAgFnPc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
k	k	k7c3
nim	on	k3xPp3gMnPc3
v	v	k7c6
konkrétní	konkrétní	k2eAgFnSc6d1
době	doba	k1gFnSc6
patří	patřit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
menších	malý	k2eAgFnPc6d2
obcích	obec	k1gFnPc6
Česka	Česko	k1gNnSc2
počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
letech	let	k1gInPc6
roste	růst	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
celé	celý	k2eAgFnSc6d1
číčovické	číčovický	k2eAgFnSc6d1
aglomeraci	aglomerace	k1gFnSc6
nicméně	nicméně	k8xC
žije	žít	k5eAaImIp3nS
necelých	celý	k2eNgInPc2d1
1	#num#	k4
tisíc	tisíc	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
18691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
779776774701719703702808850780725603703984	#num#	k4
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
domů	dům	k1gInPc2
za	za	k7c4
roky	rok	k1gInPc4
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
18691880189019001910192119301950196119701980199120012011	#num#	k4
</s>
<s>
128133137139144148177392243234236297313358	#num#	k4
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Kaple	kaple	k1gFnPc1
Nalezení	nalezení	k1gNnSc2
svatého	svatý	k2eAgInSc2d1
Kříže	kříž	k1gInSc2
–	–	k?
barokní	barokní	k2eAgFnSc1d1
osmiboká	osmiboký	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
z	z	k7c2
let	léto	k1gNnPc2
1711	#num#	k4
až	až	k6eAd1
1714	#num#	k4
na	na	k7c4
návrší	návrší	k1gNnSc4
na	na	k7c4
jv	jv	k?
<g/>
.	.	kIx.
okraji	okraj	k1gInSc6
Malých	Malý	k1gMnPc2
Číčovic	Číčovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Fotografie	fotografia	k1gFnPc1
</s>
<s>
Kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Kříže	Kříž	k1gMnSc2
</s>
<s>
Čičovický	Čičovický	k2eAgInSc1d1
kamýk	kamýk	k1gInSc1
</s>
<s>
Okraj	okraj	k1gInSc1
obce	obec	k1gFnSc2
ze	z	k7c2
směru	směr	k1gInSc2
na	na	k7c4
Buštěhrad	Buštěhrad	k1gInSc4
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Dopravní	dopravní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
</s>
<s>
Pozemní	pozemní	k2eAgFnSc1d1
komunikace	komunikace	k1gFnSc1
–	–	k?
Do	do	k7c2
obce	obec	k1gFnSc2
vedou	vést	k5eAaImIp3nP
silnice	silnice	k1gFnPc1
III	III	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Železnice	železnice	k1gFnSc1
–	–	k?
Železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
ani	ani	k8xC
stanice	stanice	k1gFnSc1
na	na	k7c6
území	území	k1gNnSc6
obce	obec	k1gFnSc2
nejsou	být	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
2011	#num#	k4
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
–	–	k?
V	v	k7c6
obci	obec	k1gFnSc6
zastavovaly	zastavovat	k5eAaImAgFnP
příměstské	příměstský	k2eAgFnPc1d1
autobusové	autobusový	k2eAgFnPc1d1
linky	linka	k1gFnPc1
Třebusice	Třebusice	k1gFnSc2
-	-	kIx~
Číčovice	Číčovice	k1gFnSc1
-	-	kIx~
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
dnech	den	k1gInPc6
14	#num#	k4
spojů	spoj	k1gInPc2
<g/>
,	,	kIx,
o	o	k7c6
víkendu	víkend	k1gInSc6
4	#num#	k4
spoje	spoj	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
Kladno	Kladno	k1gNnSc1
-	-	kIx~
Středokluky	Středokluk	k1gInPc1
-	-	kIx~
Tuchoměřice	Tuchoměřice	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
dnech	den	k1gInPc6
2	#num#	k4
spoje	spoj	k1gInSc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
dopravce	dopravce	k1gMnSc1
ČSAD	ČSAD	kA
MHD	MHD	kA
Kladno	Kladno	k1gNnSc4
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
v	v	k7c6
obci	obec	k1gFnSc6
zastavuje	zastavovat	k5eAaImIp3nS
autobus	autobus	k1gInSc1
323	#num#	k4
(	(	kIx(
<g/>
Koleč	Koleč	k1gInSc1
-	-	kIx~
Číčovice	Číčovice	k1gFnSc1
-	-	kIx~
Nádraží	nádraží	k1gNnSc1
Veleslavín	Veleslavín	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
350	#num#	k4
(	(	kIx(
<g/>
Kladno	Kladno	k1gNnSc1
-	-	kIx~
Číčovice	Číčovice	k1gFnSc1
-	-	kIx~
Dejvická	dejvický	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
cicovice	cicovice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Archivováno	archivován	k2eAgNnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
Historie	historie	k1gFnSc1
<g/>
↑	↑	k?
Povinně	povinně	k6eAd1
zveřejňované	zveřejňovaný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
dle	dle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
106	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
Sb	sb	kA
<g/>
..	..	k?
www.cicovice.cz	www.cicovice.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
13	#num#	k4
<g/>
/	/	kIx~
<g/>
1951	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
změnách	změna	k1gFnPc6
úředních	úřední	k2eAgInPc2d1
názvů	název	k1gInPc2
míst	místo	k1gNnPc2
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Správní	správní	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
Předlitavska	Předlitavsko	k1gNnSc2
1850	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
↑	↑	k?
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
205	#num#	k4
<g/>
/	/	kIx~
<g/>
1926	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
222	#num#	k4
<g/>
/	/	kIx~
<g/>
1926	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Amtliches	Amtliches	k1gMnSc1
Deutsches	Deutsches	k1gMnSc1
Ortsbuch	Ortsbuch	k1gMnSc1
für	für	k?
das	das	k?
Protektorat	Protektorat	k1gInSc1
Böhmen	Böhmen	k1gInSc1
und	und	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Mähren	Mährna	k1gFnPc2
<g/>
↑	↑	k?
Nařízení	nařízení	k1gNnSc1
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
185	#num#	k4
<g/>
/	/	kIx~
<g/>
1942	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dekret	dekret	k1gInSc1
presidenta	president	k1gMnSc2
republiky	republika	k1gFnSc2
č.	č.	k?
121	#num#	k4
<g/>
/	/	kIx~
<g/>
1945	#num#	k4
Sb	sb	kA
<g/>
..	..	k?
aplikace	aplikace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
mvcr	mvcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
..	..	k?
aplikace	aplikace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
mvcr	mvcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Adresář	adresář	k1gInSc1
republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
živnosti	živnost	k1gFnPc4
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
a	a	k8xC
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
sestavila	sestavit	k5eAaPmAgFnS
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Rudolf	Rudolfa	k1gFnPc2
Mosse	Mosse	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1932	#num#	k4
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
str	str	kA
<g/>
.	.	kIx.
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Adresář	adresář	k1gInSc1
republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
živnosti	živnost	k1gFnPc4
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
a	a	k8xC
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
sestavila	sestavit	k5eAaPmAgFnS
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Rudolf	Rudolfa	k1gFnPc2
Mosse	Mosse	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1932	#num#	k4
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1822	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
I.	I.	kA
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
-	-	kIx~
<g/>
1311	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
53	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Historický	historický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
1869	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2015-12-21	2015-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HANSL	HANSL	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smíchovsko	Smíchovsko	k1gNnSc1
a	a	k8xC
Zbraslavsko	Zbraslavsko	k1gNnSc1
:	:	kIx,
společnou	společný	k2eAgFnSc7d1
prací	práce	k1gFnSc7
učitelstva	učitelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smíchov	Smíchov	k1gInSc1
<g/>
:	:	kIx,
vl	vl	k?
<g/>
.	.	kIx.
<g/>
n.	n.	k?
<g/>
,	,	kIx,
1899	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Číčovice	Číčovice	k1gFnSc2
Malé	Malé	k2eAgFnSc2d1
i	i	k8xC
Velké	velký	k2eAgFnSc2d1
<g/>
,	,	kIx,
s.	s.	k?
197	#num#	k4
<g/>
-	-	kIx~
<g/>
198	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Číčovice	Číčovice	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Číčovice	Číčovice	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
<g/>
,	,	kIx,
městyse	městys	k1gInSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Praha-západ	Praha-západ	k1gInSc1
</s>
<s>
Bojanovice	Bojanovice	k1gFnSc1
•	•	k?
Bratřínov	Bratřínov	k1gInSc1
•	•	k?
Březová-Oleško	Březová-Oleška	k1gFnSc5
•	•	k?
Buš	buš	k1gInSc1
•	•	k?
Černolice	Černolice	k1gFnSc2
•	•	k?
Černošice	Černošice	k1gFnSc2
•	•	k?
Červený	červený	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
•	•	k?
Číčovice	Číčovice	k1gFnSc2
•	•	k?
Čisovice	Čisovice	k1gFnSc2
•	•	k?
Davle	Davle	k1gFnSc2
•	•	k?
Dobrovíz	Dobrovíz	k1gMnSc1
•	•	k?
Dobříč	Dobříč	k1gMnSc1
•	•	k?
Dobřichovice	Dobřichovice	k1gFnPc4
•	•	k?
Dolní	dolní	k2eAgInPc1d1
Břežany	Břežany	k1gInPc1
•	•	k?
Drahelčice	Drahelčice	k1gFnSc2
•	•	k?
Holubice	holubice	k1gFnSc2
•	•	k?
Horoměřice	Horoměřice	k1gFnSc2
•	•	k?
Hostivice	Hostivice	k1gFnSc2
•	•	k?
Hradištko	Hradištka	k1gFnSc5
•	•	k?
Hvozdnice	Hvozdnice	k1gFnSc2
•	•	k?
Choteč	Choteč	k1gMnSc1
•	•	k?
Chrášťany	Chrášťan	k1gMnPc4
•	•	k?
Chýně	Chýna	k1gFnSc6
•	•	k?
Chýnice	Chýnice	k1gFnSc2
•	•	k?
Jeneč	Jeneč	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Jesenice	Jesenice	k1gFnSc2
•	•	k?
Jílové	jílový	k2eAgNnSc4d1
u	u	k7c2
Prahy	Praha	k1gFnSc2
•	•	k?
Jíloviště	Jíloviště	k1gNnSc1
•	•	k?
Jinočany	Jinočan	k1gMnPc4
•	•	k?
Kamenný	kamenný	k2eAgInSc1d1
Přívoz	přívoz	k1gInSc1
•	•	k?
Karlík	Karlík	k1gMnSc1
•	•	k?
Klínec	klínec	k1gInSc1
•	•	k?
Kněževes	Kněževes	k1gFnSc1
•	•	k?
Kosoř	Kosoř	k1gFnSc1
•	•	k?
Kytín	Kytín	k1gInSc4
•	•	k?
Lety	let	k1gInPc4
•	•	k?
Libčice	Libčice	k1gFnSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
•	•	k?
Libeř	Libeř	k1gMnSc1
•	•	k?
Lichoceves	Lichoceves	k1gMnSc1
•	•	k?
Líšnice	Líšnice	k1gFnSc2
•	•	k?
Měchenice	Měchenice	k1gFnSc2
•	•	k?
Mníšek	Mníšek	k1gInSc1
pod	pod	k7c7
Brdy	Brdy	k1gInPc7
•	•	k?
Nučice	Nučice	k1gFnPc4
•	•	k?
Ohrobec	Ohrobec	k1gMnSc1
•	•	k?
Okoř	Okoř	k1gMnSc1
•	•	k?
Okrouhlo	okrouhlo	k1gNnSc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Ořech	ořech	k1gInSc1
•	•	k?
Petrov	Petrov	k1gInSc1
•	•	k?
Pohoří	pohořet	k5eAaPmIp3nS
•	•	k?
Průhonice	Průhonice	k1gFnPc4
•	•	k?
Psáry	Psára	k1gFnSc2
•	•	k?
Ptice	Ptice	k1gFnSc2
•	•	k?
Roblín	Roblín	k1gInSc1
•	•	k?
Roztoky	roztoka	k1gFnSc2
•	•	k?
Rudná	rudný	k2eAgFnSc1d1
•	•	k?
Řevnice	řevnice	k1gFnSc1
•	•	k?
Řitka	Řitka	k1gFnSc1
•	•	k?
Slapy	slap	k1gInPc1
•	•	k?
Statenice	Statenice	k1gFnSc2
•	•	k?
Středokluky	Středokluk	k1gInPc4
•	•	k?
Svrkyně	Svrkyně	k1gFnSc2
•	•	k?
Štěchovice	Štěchovice	k1gFnPc4
•	•	k?
Tachlovice	Tachlovice	k1gFnSc2
•	•	k?
Trnová	trnový	k2eAgFnSc1d1
•	•	k?
Třebotov	Třebotovo	k1gNnPc2
•	•	k?
Tuchoměřice	Tuchoměřice	k1gFnSc2
•	•	k?
Tursko	Tursko	k1gNnSc1
•	•	k?
Úholičky	Úholička	k1gFnSc2
•	•	k?
Úhonice	Úhonice	k1gFnSc2
•	•	k?
Únětice	Únětice	k1gFnSc2
•	•	k?
Velké	velký	k2eAgInPc1d1
Přílepy	přílep	k1gInPc1
•	•	k?
Vestec	Vestec	k1gInSc1
•	•	k?
Vonoklasy	Vonoklasa	k1gFnSc2
•	•	k?
Vrané	vraný	k2eAgFnSc2d1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
•	•	k?
Všenory	Všenora	k1gFnSc2
•	•	k?
Zahořany	Zahořan	k1gMnPc4
•	•	k?
Zbuzany	Zbuzan	k1gInPc4
•	•	k?
Zlatníky-Hodkovice	Zlatníky-Hodkovice	k1gFnSc2
•	•	k?
Zvole	Zvole	k1gInSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
766147	#num#	k4
</s>
