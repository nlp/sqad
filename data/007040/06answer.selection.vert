<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
moderátor	moderátor	k1gInSc1	moderátor
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
těžkovodní	těžkovodní	k2eAgFnSc1d1	těžkovodní
reaktor	reaktor	k1gInSc1	reaktor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
typ	typ	k1gInSc4	typ
Candu	Cand	k1gInSc2	Cand
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
při	při	k7c6	při
stejně	stejně	k6eAd1	stejně
silném	silný	k2eAgInSc6d1	silný
moderačním	moderační	k2eAgInSc6d1	moderační
účinku	účinek	k1gInSc6	účinek
pohltí	pohltit	k5eAaPmIp3nS	pohltit
méně	málo	k6eAd2	málo
neutronů	neutron	k1gInPc2	neutron
než	než	k8xS	než
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
