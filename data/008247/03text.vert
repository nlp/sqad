<p>
<s>
Astrachán	astrachán	k1gInSc1	astrachán
bílý	bílý	k2eAgInSc1d1	bílý
(	(	kIx(	(
<g/>
Malus	Malus	k1gInSc1	Malus
domestica	domestic	k1gInSc2	domestic
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ovocný	ovocný	k2eAgInSc1d1	ovocný
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
kultivar	kultivar	k1gInSc1	kultivar
druhu	druh	k1gInSc2	druh
jabloň	jabloň	k1gFnSc4	jabloň
domácí	domácí	k2eAgMnPc1d1	domácí
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
růžovitých	růžovitý	k2eAgMnPc2d1	růžovitý
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgInP	řadit
mezi	mezi	k7c4	mezi
odrůdy	odrůda	k1gFnPc4	odrůda
letních	letní	k2eAgNnPc2d1	letní
jablek	jablko	k1gNnPc2	jablko
<g/>
,	,	kIx,	,
sklízí	sklízet	k5eAaImIp3nS	sklízet
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
nebo	nebo	k8xC	nebo
srpnu	srpen	k1gInSc6	srpen
<g/>
,	,	kIx,	,
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
moučnatí	moučnatět	k5eAaImIp3nS	moučnatět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
===	===	k?	===
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Růst	růst	k1gInSc1	růst
===	===	k?	===
</s>
</p>
<p>
<s>
Růst	růst	k1gInSc1	růst
odrůdy	odrůda	k1gFnSc2	odrůda
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc1d1	střední
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plodnost	plodnost	k1gFnSc4	plodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Plodí	plodit	k5eAaImIp3nS	plodit
záhy	záhy	k6eAd1	záhy
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
střídavě	střídavě	k6eAd1	střídavě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plod	plod	k1gInSc4	plod
==	==	k?	==
</s>
</p>
<p>
<s>
Plod	plod	k1gInSc1	plod
je	být	k5eAaImIp3nS	být
ploše	ploš	k1gMnSc4	ploš
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
<g/>
.	.	kIx.	.
</s>
<s>
Slupka	slupka	k1gFnSc1	slupka
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
žlutozelené	žlutozelený	k2eAgNnSc1d1	žlutozelené
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
překryté	překrytý	k2eAgNnSc1d1	překryté
líčkem	líčko	k1gNnSc7	líčko
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
červeného	červený	k2eAgNnSc2d1	červené
žíhání	žíhání	k1gNnSc2	žíhání
<g/>
.	.	kIx.	.
</s>
<s>
Dužnina	dužnina	k1gFnSc1	dužnina
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
s	s	k7c7	s
navinulou	navinulý	k2eAgFnSc7d1	navinulá
chutí	chuť	k1gFnSc7	chuť
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Choroby	choroba	k1gFnPc1	choroba
a	a	k8xC	a
škůdci	škůdce	k1gMnPc1	škůdce
==	==	k?	==
</s>
</p>
<p>
<s>
Odrůda	odrůda	k1gFnSc1	odrůda
je	být	k5eAaImIp3nS	být
náchylná	náchylný	k2eAgFnSc1d1	náchylná
k	k	k7c3	k
strupovitosti	strupovitost	k1gFnSc3	strupovitost
jabloní	jabloň	k1gFnPc2	jabloň
a	a	k8xC	a
padlí	padlí	k1gNnPc2	padlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
