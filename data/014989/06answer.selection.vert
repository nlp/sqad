<s>
Dekolonizace	dekolonizace	k1gFnSc1
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
označuje	označovat	k5eAaImIp3nS
společensko-politické	společensko-politický	k2eAgInPc1d1
procesy	proces	k1gInPc1
osamostatnění	osamostatnění	k1gNnSc2
koloniálních	koloniální	k2eAgFnPc2d1
držav	država	k1gFnPc2
od	od	k7c2
metropole	metropol	k1gFnSc2
(	(	kIx(
<g/>
mateřské	mateřský	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>