<s>
Dekolonizace	dekolonizace	k1gFnSc1
</s>
<s>
Dekolonizace	dekolonizace	k1gFnSc1
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
označuje	označovat	k5eAaImIp3nS
společensko-politické	společensko-politický	k2eAgInPc1d1
procesy	proces	k1gInPc1
osamostatnění	osamostatnění	k1gNnSc2
koloniálních	koloniální	k2eAgFnPc2d1
držav	država	k1gFnPc2
od	od	k7c2
metropole	metropol	k1gFnSc2
(	(	kIx(
<g/>
mateřské	mateřský	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
dekolonizace	dekolonizace	k1gFnSc2
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
užívat	užívat	k5eAaImF
během	během	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přes	přes	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
procesy	proces	k1gInPc1
emancipace	emancipace	k1gFnSc2
mají	mít	k5eAaImIp3nP
daleko	daleko	k6eAd1
starší	starý	k2eAgFnSc4d2
historii	historie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dekolonizace	dekolonizace	k1gFnSc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
začala	začít	k5eAaPmAgFnS
již	již	k6eAd1
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc7d1
a	a	k8xC
stř	stř	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
nabrala	nabrat	k5eAaPmAgFnS
na	na	k7c6
obrátkách	obrátka	k1gFnPc6
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
především	především	k6eAd1
jako	jako	k8xC,k8xS
důsledek	důsledek	k1gInSc4
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dekolonizace	dekolonizace	k1gFnSc1
nezahrnuje	zahrnovat	k5eNaImIp3nS
pouze	pouze	k6eAd1
proces	proces	k1gInSc1
vlastního	vlastní	k2eAgNnSc2d1
osamostatnění	osamostatnění	k1gNnSc2
<g/>
,	,	kIx,
respektive	respektive	k9
vyhlášení	vyhlášení	k1gNnSc4
samostatnosti	samostatnost	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k6eAd1
procesy	proces	k1gInPc1
budování	budování	k1gNnSc1
samostatného	samostatný	k2eAgInSc2d1
státu	stát	k1gInSc2
v	v	k7c6
postkoloniálním	postkoloniální	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
a	a	k8xC
procesy	proces	k1gInPc4
hospodářské	hospodářský	k2eAgFnSc2d1
rekonstrukce	rekonstrukce	k1gFnSc2
nebo	nebo	k8xC
reorientace	reorientace	k1gFnSc2
bývalé	bývalý	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
také	také	k9
v	v	k7c6
rámci	rámec	k1gInSc6
neevropských	evropský	k2eNgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
dekolonizaci	dekolonizace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
vlna	vlna	k1gFnSc1
dekolonizace	dekolonizace	k1gFnSc2
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
a	a	k8xC
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
</s>
<s>
Mapa	mapa	k1gFnSc1
získávání	získávání	k1gNnSc2
nezávislosti	nezávislost	k1gFnSc2
v	v	k7c6
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Chronologie	chronologie	k1gFnSc2
dekolonizace	dekolonizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
první	první	k4xOgFnSc3
emancipaci	emancipace	k1gFnSc3
od	od	k7c2
koloniální	koloniální	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
na	na	k7c6
území	území	k1gNnSc6
13	#num#	k4
kolonií	kolonie	k1gFnPc2
ležících	ležící	k2eAgFnPc2d1
většinou	většinou	k6eAd1
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
dnešních	dnešní	k2eAgMnPc2d1
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
kolonie	kolonie	k1gFnPc1
byly	být	k5eAaImAgFnP
většinou	většinou	k6eAd1
osídleny	osídlit	k5eAaPmNgFnP
obyvatelstvem	obyvatelstvo	k1gNnSc7
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
vytlačilo	vytlačit	k5eAaPmAgNnS
původní	původní	k2eAgMnPc4d1
indiánské	indiánský	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčiny	příčina	k1gFnPc4
konfliktu	konflikt	k1gInSc2
s	s	k7c7
metropolí	metropol	k1gFnSc7
spočívaly	spočívat	k5eAaImAgFnP
bezprostředně	bezprostředně	k6eAd1
především	především	k6eAd1
v	v	k7c6
ekonomických	ekonomický	k2eAgFnPc6d1
politikách	politika	k1gFnPc6
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
regulovaly	regulovat	k5eAaImAgFnP
hospodářský	hospodářský	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
(	(	kIx(
<g/>
například	například	k6eAd1
dovoz-vývoz	dovoz-vývoz	k1gInSc1
<g/>
,	,	kIx,
cla	clo	k1gNnPc1
nebo	nebo	k8xC
daně	daň	k1gFnPc1
<g/>
)	)	kIx)
v	v	k7c6
koloniích	kolonie	k1gFnPc6
podle	podle	k7c2
potřeb	potřeba	k1gFnPc2
centra	centrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
díky	díky	k7c3
relativně	relativně	k6eAd1
velké	velký	k2eAgFnSc3d1
samosprávě	samospráva	k1gFnSc3
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
kolonie	kolonie	k1gFnPc1
měly	mít	k5eAaImAgFnP
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gInPc6
rozvinul	rozvinout	k5eAaPmAgInS
vlastní	vlastní	k2eAgInSc1d1
politický	politický	k2eAgInSc1d1
(	(	kIx(
<g/>
veřejný	veřejný	k2eAgInSc1d1
<g/>
)	)	kIx)
život	život	k1gInSc1
a	a	k8xC
identita	identita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
mělo	mít	k5eAaImAgNnS
osvícenství	osvícenství	k1gNnSc1
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
politické	politický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
(	(	kIx(
<g/>
především	především	k6eAd1
liberalismus	liberalismus	k1gInSc1
a	a	k8xC
fyziokratismus	fyziokratismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
získala	získat	k5eAaPmAgFnS
podporu	podpora	k1gFnSc4
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
zájem	zájem	k1gInSc4
o	o	k7c4
oslabení	oslabení	k1gNnSc4
britské	britský	k2eAgFnSc2d1
koloniální	koloniální	k2eAgFnSc2d1
moci	moc	k1gFnSc2
v	v	k7c6
Americe	Amerika	k1gFnSc6
i	i	k9
mimo	mimo	k7c4
ni	on	k3xPp3gFnSc4
(	(	kIx(
<g/>
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c6
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
Francie	Francie	k1gFnSc2
po	po	k7c6
sedmileté	sedmiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
ztrácela	ztrácet	k5eAaImAgFnS
vliv	vliv	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
revoluční	revoluční	k2eAgFnSc6d1
válce	válka	k1gFnSc6
mezi	mezi	k7c7
léty	léto	k1gNnPc7
1775-1783	1775-1783	k4
byli	být	k5eAaImAgMnP
zainteresováni	zainteresovat	k5eAaPmNgMnP
vedle	vedle	k7c2
Francie	Francie	k1gFnSc2
také	také	k6eAd1
Španělsko	Španělsko	k1gNnSc1
a	a	k8xC
Nizozemí	Nizozemí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Latinskoamerické	latinskoamerický	k2eAgNnSc1d1
osvobození	osvobození	k1gNnSc1
bylo	být	k5eAaImAgNnS
inspirováno	inspirovat	k5eAaBmNgNnS
severoamerickým	severoamerický	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
<g/>
,	,	kIx,
francouzskou	francouzský	k2eAgFnSc7d1
revolucí	revoluce	k1gFnSc7
a	a	k8xC
osvícenským	osvícenský	k2eAgNnSc7d1
politickým	politický	k2eAgNnSc7d1
myšlením	myšlení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Británie	Británie	k1gFnSc2
byla	být	k5eAaImAgFnS
španělská	španělský	k2eAgFnSc1d1
i	i	k8xC
portugalská	portugalský	k2eAgFnSc1d1
koloniální	koloniální	k2eAgFnSc1d1
správa	správa	k1gFnSc1
velmi	velmi	k6eAd1
centralizovaná	centralizovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškerá	veškerý	k3xTgNnPc1
závažná	závažný	k2eAgNnPc1d1
rozhodnutí	rozhodnutí	k1gNnPc1
byla	být	k5eAaImAgNnP
vždy	vždy	k6eAd1
činěna	činit	k5eAaImNgNnP
v	v	k7c6
Madridu	Madrid	k1gInSc6
či	či	k8xC
Lisabonu	Lisabon	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
samozřejmě	samozřejmě	k6eAd1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
neefektivní	efektivní	k2eNgNnSc4d1
řízení	řízení	k1gNnSc4
a	a	k8xC
pomalá	pomalý	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
správy	správa	k1gFnSc2
v	v	k7c6
koloniích	kolonie	k1gFnPc6
v	v	k7c6
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
byl	být	k5eAaImAgMnS
vicekrál	vicekrál	k1gMnSc1
<g/>
,	,	kIx,
kterým	který	k3yRgMnSc7,k3yQgMnSc7,k3yIgMnSc7
byl	být	k5eAaImAgMnS
téměř	téměř	k6eAd1
výlučně	výlučně	k6eAd1
Španěl	Španěl	k1gMnSc1
(	(	kIx(
<g/>
peninsularo	peninsulara	k1gFnSc5
<g/>
)	)	kIx)
z	z	k7c2
Iberského	iberský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znamenalo	znamenat	k5eAaImAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
nově	nově	k6eAd1
příchozí	příchozí	k1gMnSc1
byl	být	k5eAaImAgMnS
často	často	k6eAd1
velmi	velmi	k6eAd1
špatně	špatně	k6eAd1
informován	informovat	k5eAaBmNgMnS
o	o	k7c4
situaci	situace	k1gFnSc4
v	v	k7c6
místokrálovství	místokrálovství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
politika	politika	k1gFnSc1
otevřeně	otevřeně	k6eAd1
diskriminovala	diskriminovat	k5eAaBmAgFnS
místní	místní	k2eAgFnPc4d1
kreolské	kreolský	k2eAgFnPc4d1
elity	elita	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
měly	mít	k5eAaImAgFnP
stále	stále	k6eAd1
relativně	relativně	k6eAd1
malý	malý	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
správu	správa	k1gFnSc4
kolonii	kolonie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
pokusy	pokus	k1gInPc4
o	o	k7c4
reformu	reforma	k1gFnSc4
koloniální	koloniální	k2eAgFnSc2d1
správy	správa	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
osvícenských	osvícenský	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
Karla	Karel	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
zůstával	zůstávat	k5eAaImAgInS
problém	problém	k1gInSc1
centralizované	centralizovaný	k2eAgFnSc2d1
a	a	k8xC
rigidní	rigidní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
jedním	jeden	k4xCgInSc7
z	z	k7c2
příčin	příčina	k1gFnPc2
pro	pro	k7c4
vznik	vznik	k1gInSc4
opozice	opozice	k1gFnSc2
vůči	vůči	k7c3
Španělsku	Španělsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
<g/>
,	,	kIx,
neméně	málo	k6eNd2
významná	významný	k2eAgFnSc1d1
příčina	příčina	k1gFnSc1
spočívala	spočívat	k5eAaImAgFnS
v	v	k7c6
hospodářské	hospodářský	k2eAgFnSc6d1
slabosti	slabost	k1gFnSc6
iberského	iberský	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
17	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
koloniální	koloniální	k2eAgInSc4d1
obchod	obchod	k1gInSc4
kontrolován	kontrolován	k2eAgInSc4d1
Velkou	velký	k2eAgFnSc4d1
Británii	Británie	k1gFnSc4
a	a	k8xC
samotné	samotný	k2eAgNnSc1d1
Španělsko	Španělsko	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
závislým	závislý	k2eAgMnPc3d1
na	na	k7c4
Londýnu	Londýn	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1
proces	proces	k1gInSc1
osamostatnění	osamostatnění	k1gNnSc2
probíhal	probíhat	k5eAaImAgInS
v	v	k7c6
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
zhruba	zhruba	k6eAd1
mezi	mezi	k7c7
léty	léto	k1gNnPc7
1791	#num#	k4
<g/>
-	-	kIx~
<g/>
1830	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
kolonií	kolonie	k1gFnPc2
vyhlásilo	vyhlásit	k5eAaPmAgNnS
nezávislost	nezávislost	k1gFnSc4
Haiti	Haiti	k1gNnSc1
(	(	kIx(
<g/>
1804	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
mezi	mezi	k7c7
posledními	poslední	k2eAgMnPc7d1
získala	získat	k5eAaPmAgFnS
nezávislost	nezávislost	k1gFnSc4
Uruguay	Uruguay	k1gFnSc1
(	(	kIx(
<g/>
1828	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
středoamerické	středoamerický	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc4
nezávislých	závislý	k2eNgInPc2d1
amerických	americký	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
hispanoamerických	hispanoamerický	k2eAgFnPc2d1
válek	válka	k1gFnPc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
byl	být	k5eAaImAgInS
dlouhodobým	dlouhodobý	k2eAgInSc7d1
procesem	proces	k1gInSc7
a	a	k8xC
v	v	k7c6
první	první	k4xOgFnSc6
fázi	fáze	k1gFnSc6
byl	být	k5eAaImAgInS
ovlivněn	ovlivnit	k5eAaPmNgInS
integračními	integrační	k2eAgFnPc7d1
snahami	snaha	k1gFnPc7
Simona	Simon	k1gMnSc2
Bolívara	Bolívar	k1gMnSc2
(	(	kIx(
<g/>
Gran	Gran	k1gInSc1
Colombia	Colombium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
rysem	rys	k1gInSc7
bylo	být	k5eAaImAgNnS
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
oscilování	oscilování	k1gNnSc2
mezi	mezi	k7c7
republikou	republika	k1gFnSc7
a	a	k8xC
monarchii	monarchie	k1gFnSc4
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
celkem	celkem	k6eAd1
dvakrát	dvakrát	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
Mexického	mexický	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
(	(	kIx(
<g/>
1821	#num#	k4
<g/>
-	-	kIx~
<g/>
1823	#num#	k4
a	a	k8xC
1864	#num#	k4
<g/>
-	-	kIx~
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
u	u	k7c2
Brazilského	brazilský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
(	(	kIx(
<g/>
1822	#num#	k4
<g/>
-	-	kIx~
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiným	jiný	k2eAgInSc7d1
důležitým	důležitý	k2eAgInSc7d1
latinskoamerickým	latinskoamerický	k2eAgInSc7d1
trendem	trend	k1gInSc7
bylo	být	k5eAaImAgNnS
zachování	zachování	k1gNnSc1
celé	celý	k2eAgFnSc2d1
řady	řada	k1gFnSc2
diskriminačních	diskriminační	k2eAgFnPc2d1
politik	politika	k1gFnPc2
vůči	vůči	k7c3
míšeneckému	míšenecký	k2eAgNnSc3d1
a	a	k8xC
domorodému	domorodý	k2eAgNnSc3d1
obyvatelstvu	obyvatelstvo	k1gNnSc3
a	a	k8xC
vytvoření	vytvoření	k1gNnSc3
politických	politický	k2eAgFnPc2d1
oligarchií	oligarchie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
rekrutovaly	rekrutovat	k5eAaImAgFnP
z	z	k7c2
řad	řada	k1gFnPc2
kreolů	kreol	k1gMnPc2
a	a	k8xC
vládly	vládnout	k5eAaImAgFnP
zemím	zem	k1gFnPc3
často	často	k6eAd1
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vliv	vliv	k1gInSc1
světových	světový	k2eAgFnPc2d1
válek	válka	k1gFnPc2
na	na	k7c6
dekolonizaci	dekolonizace	k1gFnSc6
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
některým	některý	k3yIgFnPc3
změnám	změna	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
přičítat	přičítat	k5eAaImF
porážce	porážka	k1gFnSc3
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německo	Německo	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
mít	mít	k5eAaImF
koloniální	koloniální	k2eAgFnPc4d1
ambice	ambice	k1gFnPc4
až	až	k9
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
sjednocení	sjednocení	k1gNnSc6
tzn.	tzn.	kA
po	po	k7c6
roce	rok	k1gInSc6
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
některá	některý	k3yIgNnPc4
území	území	k1gNnPc4
především	především	k6eAd1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
<g/>
:	:	kIx,
Německá	německý	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
nebo	nebo	k8xC
Německá	německý	k2eAgFnSc1d1
jihozápadní	jihozápadní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
Oceánii	Oceánie	k1gFnSc6
(	(	kIx(
<g/>
Německá	německý	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
relativně	relativně	k6eAd1
malá	malý	k2eAgNnPc4d1
území	území	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
nebyla	být	k5eNaImAgNnP
ještě	ještě	k6eAd1
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
dvou	dva	k4xCgFnPc2
největších	veliký	k2eAgFnPc2d3
koloniálních	koloniální	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německé	německý	k2eAgFnPc4d1
ambice	ambice	k1gFnPc4
v	v	k7c6
koloniální	koloniální	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vidět	vidět	k5eAaImF
jako	jako	k9
jednu	jeden	k4xCgFnSc4
z	z	k7c2
příčin	příčina	k1gFnPc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
sice	sice	k8xC
dominantně	dominantně	k6eAd1
odehrávala	odehrávat	k5eAaImAgFnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
měla	mít	k5eAaImAgFnS
svůj	svůj	k3xOyFgInSc4
průběh	průběh	k1gInSc4
také	také	k9
v	v	k7c6
koloniích	kolonie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Tyto	tento	k3xDgFnPc1
změny	změna	k1gFnPc1
ale	ale	k9
neznamenaly	znamenat	k5eNaImAgFnP
dekolonizaci	dekolonizace	k1gFnSc4
ale	ale	k8xC
pouhý	pouhý	k2eAgInSc4d1
přesun	přesun	k1gInSc4
koloniální	koloniální	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
z	z	k7c2
Německa	Německo	k1gNnSc2
na	na	k7c4
například	například	k6eAd1
Belgii	Belgie	k1gFnSc4
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc4
a	a	k8xC
Británii	Británie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dekolonizace	dekolonizace	k1gFnSc1
Afriky	Afrika	k1gFnSc2
a	a	k8xC
Asie	Asie	k1gFnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
meziválečném	meziválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
spíše	spíše	k9
v	v	k7c6
zárodcích	zárodek	k1gInPc6
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vliv	vliv	k1gInSc1
dvou	dva	k4xCgFnPc2
největších	veliký	k2eAgFnPc2d3
koloniálních	koloniální	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
zvětšil	zvětšit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
ale	ale	k9
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
základě	základ	k1gInSc6
místních	místní	k2eAgInPc2d1
požadavků	požadavek	k1gInPc2
k	k	k7c3
některým	některý	k3yIgFnPc3
reformám	reforma	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
lze	lze	k6eAd1
vidět	vidět	k5eAaImF
jako	jako	k9
ústupky	ústupek	k1gInPc4
ze	z	k7c2
strany	strana	k1gFnSc2
metropole	metropol	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
příklad	příklad	k1gInSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
uvést	uvést	k5eAaPmF
Akt	akt	k1gInSc4
o	o	k7c6
vládě	vláda	k1gFnSc6
v	v	k7c6
Indii	Indie	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1935	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
mezi	mezi	k7c7
jiným	jiný	k2eAgInSc7d1
zaručoval	zaručovat	k5eAaImAgInS
místním	místní	k2eAgFnPc3d1
provinciím	provincie	k1gFnPc3
větší	veliký	k2eAgFnSc1d2
autonomii	autonomie	k1gFnSc3
<g/>
,	,	kIx,
rozšíření	rozšíření	k1gNnSc3
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
nebo	nebo	k8xC
zvýšení	zvýšení	k1gNnSc2
počtu	počet	k1gInSc2
indických	indický	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
v	v	k7c6
některých	některý	k3yIgInPc6
místních	místní	k2eAgInPc6d1
orgánech	orgán	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
závažné	závažný	k2eAgFnPc4d1
změny	změna	k1gFnPc4
ve	v	k7c6
správě	správa	k1gFnSc6
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
neznamenaly	znamenat	k5eNaImAgInP
v	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
dekolonizaci	dekolonizace	k1gFnSc4
ve	v	k7c6
smyslu	smysl	k1gInSc6
osamostatnění	osamostatnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
Maroka	Maroko	k1gNnSc2
nebo	nebo	k8xC
Alžírska	Alžírsko	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
meziválečném	meziválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
k	k	k7c3
zřetelným	zřetelný	k2eAgInPc3d1
projevům	projev	k1gInPc3
odporu	odpor	k1gInSc2
vůči	vůči	k7c3
Španělsku	Španělsko	k1gNnSc3
a	a	k8xC
Francii	Francie	k1gFnSc3
(	(	kIx(
<g/>
druhá	druhý	k4xOgFnSc1
marocká	marocký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Alžírsku	Alžírsko	k1gNnSc6
se	se	k3xPyFc4
zformovalo	zformovat	k5eAaPmAgNnS
nacionalistické	nacionalistický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
(	(	kIx(
<g/>
ENA	ENA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
lze	lze	k6eAd1
charakterizoval	charakterizovat	k5eAaBmAgMnS
období	období	k1gNnSc4
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
válkami	válka	k1gFnPc7
jako	jako	k8xC,k8xS
dobu	doba	k1gFnSc4
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
evropského	evropský	k2eAgInSc2d1
kolonialismu	kolonialismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
počátek	počátek	k1gInSc4
krize	krize	k1gFnSc2
kolonialismu	kolonialismus	k1gInSc2
vedoucí	vedoucí	k1gFnSc2
k	k	k7c3
postupné	postupný	k2eAgFnSc3d1
dekolonizaci	dekolonizace	k1gFnSc3
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
uvést	uvést	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
dopady	dopad	k1gInPc1
války	válka	k1gFnSc2
byly	být	k5eAaImAgInP
pro	pro	k7c4
Francii	Francie	k1gFnSc4
i	i	k8xC
Velkou	velký	k2eAgFnSc4d1
Británii	Británie	k1gFnSc4
-	-	kIx~
a	a	k8xC
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
vůbec	vůbec	k9
-	-	kIx~
devastující	devastující	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
samozřejmě	samozřejmě	k6eAd1
znamenalo	znamenat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnPc4
schopnosti	schopnost	k1gFnPc4
účinně	účinně	k6eAd1
kontrolovat	kontrolovat	k5eAaImF
kolonie	kolonie	k1gFnPc4
během	během	k7c2
války	válka	k1gFnSc2
citelně	citelně	k6eAd1
poklesly	poklesnout	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poválečná	poválečný	k2eAgFnSc1d1
devastace	devastace	k1gFnSc1
se	se	k3xPyFc4
samozřejmě	samozřejmě	k6eAd1
týkala	týkat	k5eAaImAgFnS
také	také	k9
ekonomiky	ekonomika	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolonie	kolonie	k1gFnSc1
byly	být	k5eAaImAgInP
ve	v	k7c6
většině	většina	k1gFnSc6
případů	případ	k1gInPc2
zcela	zcela	k6eAd1
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
poptávce	poptávka	k1gFnSc6
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
během	během	k7c2
války	válka	k1gFnSc2
a	a	k8xC
po	po	k7c6
ní	on	k3xPp3gFnSc6
značně	značně	k6eAd1
poklesla	poklesnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnohých	mnohý	k2eAgInPc6d1
případech	případ	k1gInPc6
byly	být	k5eAaImAgFnP
kolonie	kolonie	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gMnPc1
obyvatelé	obyvatel	k1gMnPc1
zataženi	zatáhnout	k5eAaPmNgMnP
do	do	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
bojovali	bojovat	k5eAaImAgMnP
po	po	k7c6
boku	bok	k1gInSc6
svých	svůj	k3xOyFgMnPc2
evropských	evropský	k2eAgMnPc2d1
koloniálních	koloniální	k2eAgMnPc2d1
pánů	pan	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
jim	on	k3xPp3gFnPc3
často	často	k6eAd1
nedostalo	dostat	k5eNaPmAgNnS
ocenění	ocenění	k1gNnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
větší	veliký	k2eAgFnSc2d2
autonomie	autonomie	k1gFnSc2
a	a	k8xC
práv	právo	k1gNnPc2
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
týkalo	týkat	k5eAaImAgNnS
nejzřetelněji	zřetelně	k6eAd3
francouzských	francouzský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Konečně	konečně	k6eAd1
hrály	hrát	k5eAaImAgFnP
svojí	svůj	k3xOyFgFnSc7
roli	role	k1gFnSc3
také	také	k9
místní	místní	k2eAgFnPc4d1
elity	elita	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
často	často	k6eAd1
získaly	získat	k5eAaPmAgFnP
evropské	evropský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
a	a	k8xC
začaly	začít	k5eAaPmAgFnP
už	už	k6eAd1
v	v	k7c6
meziválečném	meziválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
definovat	definovat	k5eAaBmF
nacionalistické	nacionalistický	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
na	na	k7c4
samostatnost	samostatnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oslabení	oslabení	k1gNnSc1
metropole	metropol	k1gFnSc2
potom	potom	k6eAd1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
jasnému	jasný	k2eAgNnSc3d1
artikulování	artikulování	k1gNnSc3
jejich	jejich	k3xOp3gInPc2
požadavků	požadavek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porážka	porážka	k1gFnSc1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
znamenala	znamenat	k5eAaImAgFnS
pro	pro	k7c4
Japonsko	Japonsko	k1gNnSc4
okamžitý	okamžitý	k2eAgInSc1d1
konec	konec	k1gInSc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
koloniálního	koloniální	k2eAgNnSc2d1
panství	panství	k1gNnSc2
hned	hned	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korea	Korea	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
okupována	okupován	k2eAgFnSc1d1
na	na	k7c6
severu	sever	k1gInSc6
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc2
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgInS
samostatnost	samostatnost	k1gFnSc4
Vietnam	Vietnam	k1gInSc1
(	(	kIx(
<g/>
vůči	vůči	k7c3
Francii	Francie	k1gFnSc3
<g/>
)	)	kIx)
a	a	k8xC
Indonésie	Indonésie	k1gFnSc1
(	(	kIx(
<g/>
vůči	vůči	k7c3
Nizozemí	Nizozemí	k1gNnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
jejich	jejich	k3xOp3gFnSc1
samostatnost	samostatnost	k1gFnSc1
byla	být	k5eAaImAgFnS
uznána	uznat	k5eAaPmNgFnS
až	až	k9
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Dekolonizace	dekolonizace	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1945-1975	1945-1975	k4
</s>
<s>
Koloniální	koloniální	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
světa	svět	k1gInSc2
k	k	k7c3
roku	rok	k1gInSc3
1945	#num#	k4
</s>
<s>
Dekolonizační	dekolonizační	k2eAgInSc1d1
proces	proces	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
především	především	k6eAd1
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
těžišti	těžiště	k1gNnSc6
koloniální	koloniální	k2eAgFnSc2d1
moci	moc	k1gFnSc2
dvou	dva	k4xCgFnPc2
největších	veliký	k2eAgFnPc2d3
koloniálních	koloniální	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
-	-	kIx~
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dekolonizace	dekolonizace	k1gFnSc1
se	se	k3xPyFc4
ale	ale	k9
dotkla	dotknout	k5eAaPmAgFnS
také	také	k6eAd1
menších	malý	k2eAgFnPc2d2
koloniálních	koloniální	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
jako	jako	k8xS,k8xC
byla	být	k5eAaImAgFnS
Belgie	Belgie	k1gFnSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
<g/>
,	,	kIx,
Nizozemí	Nizozemí	k1gNnSc1
a	a	k8xC
nebo	nebo	k8xC
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
obecného	obecný	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
byla	být	k5eAaImAgFnS
dekolonizace	dekolonizace	k1gFnSc1
hluboce	hluboko	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
vznikem	vznik	k1gInSc7
Studené	Studená	k1gFnSc2
války	válka	k1gFnSc2
a	a	k8xC
bipolarismu	bipolarismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dekolonizované	Dekolonizovaný	k2eAgInPc1d1
a	a	k8xC
nově	nově	k6eAd1
samostatné	samostatný	k2eAgFnPc1d1
země	zem	k1gFnPc1
se	se	k3xPyFc4
tak	tak	k6eAd1
hned	hned	k6eAd1
na	na	k7c6
počátku	počátek	k1gInSc6
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
ocitly	ocitnout	k5eAaPmAgFnP
uprostřed	uprostřed	k7c2
velmocenského	velmocenský	k2eAgInSc2d1
zápasu	zápas	k1gInSc2
mezi	mezi	k7c7
Západem	západ	k1gInSc7
vedeným	vedený	k2eAgNnSc7d1
USA	USA	kA
a	a	k8xC
Východem	východ	k1gInSc7
pod	pod	k7c7
vedením	vedení	k1gNnSc7
SSSR	SSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
ideologické	ideologický	k2eAgNnSc4d1
soupeření	soupeření	k1gNnSc4
v	v	k7c6
mnoha	mnoho	k4c6
ohledem	ohled	k1gInSc7
negativně	negativně	k6eAd1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
politický	politický	k2eAgInSc4d1
i	i	k8xC
ekonomický	ekonomický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
v	v	k7c6
dekolonizovaných	dekolonizovaný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
například	například	k6eAd1
Korea	Korea	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnSc1
rozdělení	rozdělení	k1gNnSc1
<g/>
,	,	kIx,
Kongo	Kongo	k1gNnSc1
nebo	nebo	k8xC
Angola	Angola	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
právě	právě	k9
zde	zde	k6eAd1
mělo	mít	k5eAaImAgNnS
nejčastěji	často	k6eAd3
vojenský	vojenský	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncepce	koncepce	k1gFnSc1
tzv.	tzv.	kA
Třetího	třetí	k4xOgInSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
původně	původně	k6eAd1
snažila	snažit	k5eAaImAgFnS
být	být	k5eAaImF
alternativou	alternativa	k1gFnSc7
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
bloky	blok	k1gInPc7
poválečného	poválečný	k2eAgNnSc2d1
rozdělení	rozdělení	k1gNnSc2
světa	svět	k1gInSc2
neměla	mít	k5eNaImAgFnS
úspěch	úspěch	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přes	přes	k7c4
pokusy	pokus	k1gInPc4
spojené	spojený	k2eAgInPc4d1
s	s	k7c7
hnutím	hnutí	k1gNnSc7
nezúčastněných	zúčastněný	k2eNgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
dekolonizace	dekolonizace	k1gFnSc1
byla	být	k5eAaImAgFnS
dlouhodobý	dlouhodobý	k2eAgInSc4d1
historický	historický	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
jednoduché	jednoduchý	k2eAgNnSc1d1
jej	on	k3xPp3gNnSc4
jednoznačně	jednoznačně	k6eAd1
charakterizovat	charakterizovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dekolonizace	dekolonizace	k1gFnSc1
měla	mít	k5eAaImAgFnS
projevy	projev	k1gInPc4
jak	jak	k6eAd1
násilného	násilný	k2eAgInSc2d1
procesu	proces	k1gInSc2
osamostatnění	osamostatnění	k1gNnSc2
(	(	kIx(
<g/>
ve	v	k7c6
smyslu	smysl	k1gInSc6
anti-koloniální	anti-koloniální	k2eAgFnSc2d1
války	válka	k1gFnSc2
nebo	nebo	k8xC
revoluce	revoluce	k1gFnSc2
<g/>
)	)	kIx)
ale	ale	k8xC
mohlo	moct	k5eAaImAgNnS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
také	také	k9
o	o	k7c4
řešení	řešení	k1gNnSc4
diplomatickou	diplomatický	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
či	či	k8xC
kompromisem	kompromis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Britská	britský	k2eAgFnSc1d1
dekolonizace	dekolonizace	k1gFnSc1
Afriky	Afrika	k1gFnSc2
</s>
<s>
Jako	jako	k9
příklad	příklad	k1gInSc1
kompromisního	kompromisní	k2eAgNnSc2d1
nebo	nebo	k8xC
více	hodně	k6eAd2
méně	málo	k6eAd2
mírového	mírový	k2eAgNnSc2d1
řešení	řešení	k1gNnSc2
je	být	k5eAaImIp3nS
často	často	k6eAd1
uváděna	uváděn	k2eAgFnSc1d1
dekolonizace	dekolonizace	k1gFnSc1
britských	britský	k2eAgFnPc2d1
držav	država	k1gFnPc2
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
a	a	k8xC
založení	založení	k1gNnSc4
britského	britský	k2eAgInSc2d1
Commonwealthu	Commonwealth	k1gInSc2
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
dekolonizace	dekolonizace	k1gFnSc2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc7
součástí	součást	k1gFnSc7
stala	stát	k5eAaPmAgFnS
většina	většina	k1gFnSc1
bývalých	bývalý	k2eAgFnPc2d1
britských	britský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
Commonwealth	Commonwealth	k1gInSc1
mezivládní	mezivládní	k2eAgInSc1d1
organizací	organizace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
sdružuje	sdružovat	k5eAaImIp3nS
nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
a	a	k8xC
má	mít	k5eAaImIp3nS
společnou	společný	k2eAgFnSc4d1
agendu	agenda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Commonwealth	Commonwealth	k1gMnSc1
následníkem	následník	k1gMnSc7
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
a	a	k8xC
často	často	k6eAd1
napomáhá	napomáhat	k5eAaBmIp3nS,k5eAaImIp3nS
zachovat	zachovat	k5eAaPmF
vliv	vliv	k1gInSc4
Británie	Británie	k1gFnSc2
v	v	k7c6
dekolonizovaných	dekolonizovaný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
i	i	k8xC
po	po	k7c6
dosažení	dosažení	k1gNnSc6
nezávislosti	nezávislost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
dekolonizace	dekolonizace	k1gFnSc2
probíhala	probíhat	k5eAaImAgFnS
v	v	k7c6
případě	případ	k1gInSc6
dvou	dva	k4xCgFnPc2
přistěhovaleckých	přistěhovalecký	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
<g/>
:	:	kIx,
Kanady	Kanada	k1gFnSc2
a	a	k8xC
Austrálie	Austrálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
země	zem	k1gFnPc1
byly	být	k5eAaImAgFnP
až	až	k9
do	do	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
dominii	dominion	k1gNnPc7
Velké	velká	k1gFnSc2
Británie	Británie	k1gFnSc2
a	a	k8xC
dodnes	dodnes	k6eAd1
jsou	být	k5eAaImIp3nP
členy	člen	k1gMnPc7
jiné	jiný	k2eAgFnSc2d1
britské	britský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Commonwealth	Commonwealth	k1gMnSc1
Realm	Realm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
se	se	k3xPyFc4
v	v	k7c6
případě	případ	k1gInSc6
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c4
dekolonizaci	dekolonizace	k1gFnSc4
spíše	spíše	k9
mírovou	mírový	k2eAgFnSc4d1
nebo	nebo	k8xC
diplomatickou	diplomatický	k2eAgFnSc4d1
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
neznamená	znamenat	k5eNaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
vlastní	vlastní	k2eAgNnSc1d1
budování	budování	k1gNnSc1
samostatných	samostatný	k2eAgInPc2d1
států	stát	k1gInPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
případě	případ	k1gInSc6
britských	britský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
jednodušší	jednoduchý	k2eAgMnSc1d2
nebo	nebo	k8xC
mírovější	mírový	k2eAgMnSc1d2
než	než	k8xS
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
řada	řada	k1gFnSc1
bývalých	bývalý	k2eAgFnPc2d1
britských	britský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
prošla	projít	k5eAaPmAgFnS
po	po	k7c6
získání	získání	k1gNnSc6
formální	formální	k2eAgFnSc2d1
nezávislosti	nezávislost	k1gFnSc2
roky	rok	k1gInPc4
krvavých	krvavý	k2eAgInPc2d1
vnitropolitických	vnitropolitický	k2eAgInPc2d1
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Protikladem	protiklad	k1gInSc7
britského	britský	k2eAgNnSc2d1
řešení	řešení	k1gNnSc2
byla	být	k5eAaImAgFnS
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
svých	svůj	k3xOyFgFnPc2
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
v	v	k7c6
Africe	Afrika	k1gFnSc6
a	a	k8xC
Asii	Asie	k1gFnSc3
nechtěla	chtít	k5eNaImAgFnS
smířit	smířit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejím	její	k3xOp3gInSc6
případě	případ	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
několika	několik	k4yIc2
válkám	válka	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
pro	pro	k7c4
Francii	Francie	k1gFnSc4
neskončily	skončit	k5eNaPmAgInP
úspěchem	úspěch	k1gInSc7
a	a	k8xC
značně	značně	k6eAd1
poškodily	poškodit	k5eAaPmAgFnP
její	její	k3xOp3gNnSc4
postavení	postavení	k1gNnSc4
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
bývalým	bývalý	k2eAgFnPc3d1
koloniím	kolonie	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
příklad	příklad	k1gInSc4
tzv.	tzv.	kA
francouzské	francouzský	k2eAgFnSc2d1
Indočíny	Indočína	k1gFnSc2
(	(	kIx(
<g/>
Vietnam	Vietnam	k1gInSc1
<g/>
,	,	kIx,
Laos	Laos	k1gInSc1
<g/>
,	,	kIx,
Kambodža	Kambodža	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
měla	mít	k5eAaImAgFnS
dekolonizace	dekolonizace	k1gFnPc4
násilný	násilný	k2eAgInSc4d1
charakter	charakter	k1gInSc4
a	a	k8xC
vedla	vést	k5eAaImAgFnS
k	k	k7c3
neméně	málo	k6eNd2
násilnému	násilný	k2eAgInSc3d1
samostatnému	samostatný	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Tunis	Tunis	k1gInSc1
a	a	k8xC
Maroko	Maroko	k1gNnSc1
získaly	získat	k5eAaPmAgInP
nezávislost	nezávislost	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
relativně	relativně	k6eAd1
nenásilnou	násilný	k2eNgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
Alžírska	Alžírsko	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
další	další	k2eAgFnSc3d1
válce	válka	k1gFnSc3
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
-	-	kIx~
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nicméně	nicméně	k8xC
případ	případ	k1gInSc1
Alžírska	Alžírsko	k1gNnSc2
se	se	k3xPyFc4
značně	značně	k6eAd1
liší	lišit	k5eAaImIp3nP
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
žila	žít	k5eAaImAgFnS
velice	velice	k6eAd1
početná	početný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
francouzských	francouzský	k2eAgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průběh	průběh	k1gInSc4
i	i	k8xC
následky	následek	k1gInPc4
Alžírské	alžírský	k2eAgFnSc2d1
války	válka	k1gFnSc2
způsobily	způsobit	k5eAaPmAgFnP
v	v	k7c6
obou	dva	k4xCgFnPc6
společnostech	společnost	k1gFnPc6
dlouhotrvající	dlouhotrvající	k2eAgNnSc1d1
trauma	trauma	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Zhruba	zhruba	k6eAd1
mezi	mezi	k7c7
roky	rok	k1gInPc7
1945	#num#	k4
-	-	kIx~
1965	#num#	k4
byla	být	k5eAaImAgFnS
osamostatněna	osamostatněn	k2eAgFnSc1d1
a	a	k8xC
uznána	uznán	k2eAgFnSc1d1
většina	většina	k1gFnSc1
britských	britský	k2eAgFnPc2d1
a	a	k8xC
francouzských	francouzský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
v	v	k7c6
Asii	Asie	k1gFnSc6
i	i	k8xC
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
kolonii	kolonie	k1gFnSc4
držených	držený	k2eAgFnPc2d1
Belgií	Belgie	k1gFnPc2
nebo	nebo	k8xC
Nizozemím	Nizozemí	k1gNnSc7
a	a	k8xC
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
poslední	poslední	k2eAgFnPc4d1
dekolonizované	dekolonizovaný	k2eAgFnPc4d1
země	zem	k1gFnPc4
patřily	patřit	k5eAaImAgFnP
portugalské	portugalský	k2eAgFnPc4d1
kolonie	kolonie	k1gFnPc4
v	v	k7c6
Africe	Afrika	k1gFnSc6
Angola	Angola	k1gFnSc1
a	a	k8xC
Mosambik	Mosambik	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
země	zem	k1gFnPc1
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
oficiálně	oficiálně	k6eAd1
osamostatnit	osamostatnit	k5eAaPmF
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
po	po	k7c6
pádu	pád	k1gInSc6
diktátora	diktátor	k1gMnSc2
A.	A.	kA
Salazara	Salazar	k1gMnSc2
v	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálnímu	oficiální	k2eAgNnSc3d1
osamostatnění	osamostatnění	k1gNnSc3
ale	ale	k8xC
předcházely	předcházet	k5eAaImAgInP
roky	rok	k1gInPc1
krvavé	krvavý	k2eAgFnSc2d1
koloniální	koloniální	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
dekolonizací	dekolonizace	k1gFnSc7
lze	lze	k6eAd1
ještě	ještě	k6eAd1
zmínit	zmínit	k5eAaPmF
navrácení	navrácení	k1gNnSc4
Macaa	Macao	k1gNnSc2
a	a	k8xC
Hongkongu	Hongkong	k1gInSc2
Číně	Čína	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Časový	časový	k2eAgInSc1d1
přehled	přehled	k1gInSc1
dekolonizace	dekolonizace	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
rokstátkolonizátor	rokstátkolonizátor	k1gMnSc1
rokstátkolonizátor	rokstátkolonizátor	k1gMnSc1
rokstátkolonizátor	rokstátkolonizátor	k1gMnSc1
</s>
<s>
1946	#num#	k4
<g/>
FilipínyUSA	FilipínyUSA	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
ČadFrancie	ČadFrancie	k1gFnSc1
<g/>
1963	#num#	k4
<g/>
KeňaBritánie	KeňaBritánie	k1gFnSc1
</s>
<s>
1946	#num#	k4
<g/>
JordánskoBritánie	JordánskoBritánie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
Dahome	Dahom	k1gInSc5
(	(	kIx(
<g/>
Benin	Benin	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
Francie	Francie	k1gFnSc2
<g/>
1963	#num#	k4
<g/>
SingapurBritánie	SingapurBritánie	k1gFnSc1
</s>
<s>
1947	#num#	k4
<g/>
BhútánBritánie	BhútánBritánie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
GabunFrancie	GabunFrancie	k1gFnSc1
<g/>
1963	#num#	k4
<g/>
Zanzibar	Zanzibar	k1gInSc1
(	(	kIx(
<g/>
Tanzanie	Tanzanie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Británie	Británie	k1gFnSc1
</s>
<s>
1947	#num#	k4
<g/>
IndieBritánie	IndieBritánie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
Horní	horní	k2eAgFnSc1d1
Volta	Volta	k1gFnSc1
(	(	kIx(
<g/>
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Francie	Francie	k1gFnSc1
<g/>
1964	#num#	k4
<g/>
MalawiBritánie	MalawiBritánie	k1gFnSc1
</s>
<s>
1947	#num#	k4
<g/>
PákistánBritánie	PákistánBritánie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
KamerunFrancie	KamerunFrancie	k1gFnSc1
<g/>
1964	#num#	k4
<g/>
MaltaBritánie	MaltaBritánie	k1gFnSc1
</s>
<s>
1948	#num#	k4
<g/>
Barma	Barma	k1gFnSc1
(	(	kIx(
<g/>
Myanmar	Myanmar	k1gInSc1
<g/>
)	)	kIx)
<g/>
Británie	Británie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
Kongo-BrazzavilleFrancie	Kongo-BrazzavilleFrancie	k1gFnSc1
<g/>
1966	#num#	k4
<g/>
GuyanaBritánie	GuyanaBritánie	k1gFnSc1
</s>
<s>
1948	#num#	k4
<g/>
Srí	Srí	k1gFnSc1
LankaBritánie	LankaBritánie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
Kongo-KinshasaBelgie	Kongo-KinshasaBelgie	k1gFnSc1
<g/>
1968	#num#	k4
<g/>
MauriciusBritánie	MauriciusBritánie	k1gFnSc1
</s>
<s>
1948	#num#	k4
<g/>
IzraelBritánie	IzraelBritánie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
KyprBritánie	KyprBritánie	k1gFnSc1
<g/>
1971	#num#	k4
<g/>
BahrajnBritánie	BahrajnBritánie	k1gFnSc1
</s>
<s>
1948	#num#	k4
<g/>
PalestinaBritánie	PalestinaBritánie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
MadagaskarFrancie	MadagaskarFrancie	k1gFnSc1
<g/>
1971	#num#	k4
<g/>
KatarBritánie	KatarBritánie	k1gFnSc1
</s>
<s>
1949	#num#	k4
<g/>
IndonésieNizozemsko	IndonésieNizozemsko	k1gNnSc1
<g/>
1960	#num#	k4
<g/>
MaliFrancie	MaliFrancie	k1gFnSc1
<g/>
1971	#num#	k4
<g/>
Spojené	spojený	k2eAgFnPc1d1
arabské	arabský	k2eAgFnPc1d1
emirátyBritánie	emirátyBritánie	k1gFnPc1
</s>
<s>
1951	#num#	k4
<g/>
LibyeBritánie	LibyeBritánie	k1gFnSc1
a	a	k8xC
Francie	Francie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
NigérieBritánie	NigérieBritánie	k1gFnSc1
<g/>
1973	#num#	k4
<g/>
BahamyBritánie	BahamyBritánie	k1gFnSc1
</s>
<s>
1954	#num#	k4
<g/>
Jižní	jižní	k2eAgFnSc1d1
VietnamFrancie	VietnamFrancie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
Pobřeží	pobřeží	k1gNnPc2
slonovinyFrancie	slonovinyFrancie	k1gFnSc1
<g/>
1975	#num#	k4
<g/>
AngolaPortugalsko	AngolaPortugalsko	k1gNnSc4
</s>
<s>
1954	#num#	k4
<g/>
Severní	severní	k2eAgFnSc1d1
VietnamFrancie	VietnamFrancie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
SenegalFrancie	SenegalFrancie	k1gFnSc1
<g/>
1975	#num#	k4
<g/>
KapverdyPortugalsko	KapverdyPortugalsko	k1gNnSc4
</s>
<s>
1954	#num#	k4
<g/>
KambodžaFrancie	KambodžaFrancie	k1gFnSc1
<g/>
1960	#num#	k4
<g/>
SomálskoBritánie	SomálskoBritánie	k1gFnSc1
<g/>
1976	#num#	k4
<g/>
SeychelyBritánie	SeychelyBritánie	k1gFnSc1
</s>
<s>
1954	#num#	k4
<g/>
LaosFrancie	LaosFrancie	k1gFnSc1
<g/>
1961	#num#	k4
<g/>
KuvajtBritánie	KuvajtBritánie	k1gFnSc1
<g/>
1980	#num#	k4
<g/>
ZimbabweBritánie	ZimbabweBritánie	k1gFnSc1
</s>
<s>
1956	#num#	k4
<g/>
MarokoFrancie	MarokoFrancie	k1gFnSc1
<g/>
1961	#num#	k4
<g/>
Sierra	Sierra	k1gFnSc1
LeoneBritánie	LeoneBritánie	k1gFnSc1
<g/>
1990	#num#	k4
<g/>
NamibieJižní	NamibieJižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
</s>
<s>
1956	#num#	k4
<g/>
SúdánBritánie	SúdánBritánie	k1gFnSc1
<g/>
1961	#num#	k4
<g/>
Tanganika	Tanganika	k1gFnSc1
(	(	kIx(
<g/>
Tanzanie	Tanzanie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Británie	Británie	k1gFnSc1
</s>
<s>
1956	#num#	k4
<g/>
TuniskoFrancie	TuniskoFrancie	k1gFnSc1
<g/>
1962	#num#	k4
<g/>
AlžírskoFrancie	AlžírskoFrancie	k1gFnSc1
</s>
<s>
1957	#num#	k4
<g/>
GhanaBritánie	GhanaBritánie	k1gFnSc1
<g/>
1962	#num#	k4
<g/>
JamajkaBritánie	JamajkaBritánie	k1gFnSc1
</s>
<s>
1957	#num#	k4
<g/>
MalajsieBritánie	MalajsieBritánie	k1gFnSc1
<g/>
1962	#num#	k4
<g/>
RwandaBelgie	RwandaBelgie	k1gFnSc1
</s>
<s>
1958	#num#	k4
<g/>
GuineaFrancie	GuineaFrancie	k1gFnSc1
<g/>
1962	#num#	k4
<g/>
UgandaBritánie	UgandaBritánie	k1gFnSc1
</s>
<s>
1962	#num#	k4
<g/>
Západní	západní	k2eAgFnPc4d1
SamoaBritánie	SamoaBritánie	k1gFnPc4
</s>
<s>
Koloniální	koloniální	k2eAgFnPc1d1
velmoci	velmoc	k1gFnPc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Oblasti	oblast	k1gFnPc1
kolonizování	kolonizování	k1gNnSc2
podle	podle	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
:	:	kIx,
východní	východní	k2eAgFnSc1d1
pobřeží	pobřeží	k1gNnSc4
dnešních	dnešní	k2eAgFnPc2d1
USA	USA	kA
a	a	k8xC
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
,	,	kIx,
ostrovy	ostrov	k1gInPc1
v	v	k7c6
Oceánii	Oceánie	k1gFnSc6
<g/>
,	,	kIx,
Indický	indický	k2eAgInSc1d1
subkontinent	subkontinent	k1gInSc1
(	(	kIx(
<g/>
Indie	Indie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hongkong	Hongkong	k1gInSc4
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
Blízkého	blízký	k2eAgInSc2d1
Východu	východ	k1gInSc2
<g/>
,	,	kIx,
oblasti	oblast	k1gFnSc2
jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
západní	západní	k2eAgFnSc1d1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Ghana	Ghana	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
severní	severní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
<g/>
:	:	kIx,
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Súdán	Súdán	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
jih	jih	k1gInSc1
USA	USA	kA
(	(	kIx(
<g/>
Louisiana	Louisian	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některá	některý	k3yIgNnPc1
území	území	k1gNnPc1
v	v	k7c6
Karibiku	Karibik	k1gInSc6
<g/>
,	,	kIx,
oblasti	oblast	k1gFnPc4
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
(	(	kIx(
<g/>
Indočína	Indočína	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velké	velký	k2eAgFnSc3d1
části	část	k1gFnSc3
severní	severní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
Alžírsko	Alžírsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oblasti	oblast	k1gFnPc4
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
Madagaskar	Madagaskar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
<g/>
:	:	kIx,
většina	většina	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
a	a	k8xC
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
dnešní	dnešní	k2eAgNnSc4d1
Mexiko	Mexiko	k1gNnSc4
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
<g/>
,	,	kIx,
malé	malý	k2eAgFnPc1d1
části	část	k1gFnPc1
v	v	k7c6
Africe	Afrika	k1gFnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
Maroko	Maroko	k1gNnSc1
a	a	k8xC
Rovníková	rovníkový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Asii	Asie	k1gFnSc6
především	především	k9
Filipíny	Filipíny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc4
<g/>
:	:	kIx,
části	část	k1gFnSc2
středozápadní	středozápadní	k2eAgFnSc2d1
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
Mosambik	Mosambik	k1gInSc1
a	a	k8xC
Angola	Angola	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
:	:	kIx,
Brazílie	Brazílie	k1gFnSc1
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc1
<g/>
:	:	kIx,
Macao	Macao	k1gNnSc1
<g/>
,	,	kIx,
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
nebo	nebo	k8xC
Goa	Goa	k1gFnSc1
</s>
<s>
Německo	Německo	k1gNnSc1
<g/>
:	:	kIx,
menší	malý	k2eAgFnPc1d2
části	část	k1gFnPc1
jihozápadní	jihozápadní	k2eAgFnSc2d1
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
Kamerun	Kamerun	k1gInSc1
<g/>
,	,	kIx,
Togo	Togo	k1gNnSc1
<g/>
,	,	kIx,
Rwanda	Rwanda	k1gFnSc1
<g/>
,	,	kIx,
Namibie	Namibie	k1gFnSc1
<g/>
...	...	k?
<g/>
)	)	kIx)
a	a	k8xC
ostrovy	ostrov	k1gInPc1
v	v	k7c6
Oceánii	Oceánie	k1gFnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
Karolíny	Karolína	k1gFnPc1
<g/>
,	,	kIx,
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
Samoa	Samoa	k1gFnSc1
<g/>
...	...	k?
<g/>
)	)	kIx)
</s>
<s>
Holandsko	Holandsko	k1gNnSc1
<g/>
:	:	kIx,
části	část	k1gFnPc4
jihovýchodní	jihovýchodní	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
(	(	kIx(
<g/>
Indonésie	Indonésie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oblasti	oblast	k1gFnPc4
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
Surinam	Surinam	k1gInSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
<g/>
:	:	kIx,
části	část	k1gFnSc2
střední	střední	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
Belgické	belgický	k2eAgNnSc1d1
Kongo	Kongo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Itálie	Itálie	k1gFnSc1
<g/>
:	:	kIx,
oblasti	oblast	k1gFnPc1
v	v	k7c6
severní	severní	k2eAgFnSc6d1
(	(	kIx(
<g/>
Libye	Libye	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
severovýchodní	severovýchodní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
(	(	kIx(
<g/>
Somálsko	Somálsko	k1gNnSc1
<g/>
,	,	kIx,
Eritrea	Eritrea	k1gFnSc1
a	a	k8xC
Etiopie	Etiopie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
<g/>
:	:	kIx,
v	v	k7c6
Asii	Asie	k1gFnSc6
Korea	Korea	k1gFnSc1
a	a	k8xC
Mandžusko	Mandžusko	k1gNnSc1
a	a	k8xC
bývalé	bývalý	k2eAgFnSc2d1
německé	německý	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
v	v	k7c6
Oceánii	Oceánie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3
osobnosti	osobnost	k1gFnPc1
dekolonizace	dekolonizace	k1gFnSc2
</s>
<s>
George	George	k1gInSc1
Washington	Washington	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Benjamin	Benjamin	k1gMnSc1
Franklin	Franklin	k1gInSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Thomas	Thomas	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Toussaint	Toussaint	k1gMnSc1
Louverture	Louvertur	k1gMnSc5
(	(	kIx(
<g/>
Haiti	Haiti	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Simon	Simon	k1gMnSc1
Bolívar	Bolívar	k1gInSc1
(	(	kIx(
<g/>
jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Miguel	Miguel	k1gMnSc1
Hidalgo	Hidalgo	k1gMnSc1
(	(	kIx(
<g/>
Mexiko	Mexiko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
José	José	k6eAd1
de	de	k?
San	San	k1gMnSc1
Martín	Martín	k1gMnSc1
(	(	kIx(
<g/>
Argentina	Argentina	k1gFnSc1
<g/>
,	,	kIx,
Chile	Chile	k1gNnSc1
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Mahátma	Mahátma	k1gFnSc1
Gándhí	Gándhí	k1gFnSc1
(	(	kIx(
<g/>
Indie	Indie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ahmed	Ahmed	k1gInSc1
Ben	Ben	k1gInSc1
Bella	Bella	k1gFnSc1
(	(	kIx(
<g/>
Alžírsko	Alžírsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Frantz	Frantz	k1gMnSc1
Fanon	Fanon	k1gMnSc1
(	(	kIx(
<g/>
Martinique	Martinique	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Aimé	Aimé	k6eAd1
Césaire	Césair	k1gInSc5
(	(	kIx(
<g/>
Martinique	Martinique	k1gNnPc3
<g/>
)	)	kIx)
</s>
<s>
Ho	on	k3xPp3gNnSc4
Či	či	k9wB
Min	min	kA
(	(	kIx(
<g/>
Vietnam	Vietnam	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kwame	Kwamat	k5eAaPmIp3nS
Nkrumah	Nkrumah	k1gInSc1
(	(	kIx(
<g/>
Ghana	Ghana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Patrice	patrice	k1gFnSc1
Lumumba	Lumumba	k1gFnSc1
(	(	kIx(
<g/>
Kongo	Kongo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Léopold	Léopold	k1gMnSc1
Sédar	Sédar	k1gMnSc1
Senghor	Senghor	k1gMnSc1
(	(	kIx(
<g/>
Senegal	Senegal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
António	António	k1gMnSc1
Agostinho	Agostin	k1gMnSc2
Neto	Neto	k1gMnSc1
(	(	kIx(
<g/>
Angola	Angola	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Ernesto	Ernesto	k6eAd1
Che	che	k0
Guevara	Guevara	k1gFnSc1
(	(	kIx(
<g/>
Argentina	Argentina	k1gFnSc1
<g/>
,	,	kIx,
Kuba	Kuba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Walter	Walter	k1gMnSc1
Rodney	Rodnea	k1gFnSc2
(	(	kIx(
<g/>
Guyana	Guyana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
-	-	kIx~
speciál	speciál	k1gInSc1
<g/>
,	,	kIx,
únor	únor	k1gInSc1
2011	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FERRO	Ferro	k1gNnSc1
<g/>
,	,	kIx,
Marc	Marc	k1gInSc1
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
kolonizací	kolonizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
dobývání	dobývání	k1gNnSc2
až	až	k9
po	po	k7c4
nezávislost	nezávislost	k1gFnSc4
13	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
Noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
21	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NÁLEVKA	nálevka	k1gFnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čas	čas	k1gInSc1
soumraku	soumrak	k1gInSc2
<g/>
:	:	kIx,
rozpad	rozpad	k1gInSc1
koloniálních	koloniální	k2eAgNnPc2d1
impérií	impérium	k1gNnPc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
199	#num#	k4
s.	s.	k?
Dějiny	dějiny	k1gFnPc1
do	do	k7c2
kapsy	kapsa	k1gFnSc2
<g/>
,	,	kIx,
sv.	sv.	kA
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7254	#num#	k4
<g/>
-	-	kIx~
<g/>
495	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZÍDEK	Zídek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československo	Československo	k1gNnSc1
a	a	k8xC
francouzská	francouzský	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1968	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
247	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
305	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kolonialismus	kolonialismus	k1gInSc1
</s>
<s>
Kolonie	kolonie	k1gFnSc1
</s>
<s>
Kolonizace	kolonizace	k1gFnSc1
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1
</s>
<s>
Osvobozenecké	osvobozenecký	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
</s>
<s>
Neokolonialismus	neokolonialismus	k1gInSc1
</s>
<s>
Postkolonialismus	Postkolonialismus	k1gInSc1
</s>
<s>
Sekularizace	sekularizace	k1gFnSc1
</s>
<s>
Imperialismus	imperialismus	k1gInSc1
</s>
<s>
Chronologie	chronologie	k1gFnSc1
dekolonizace	dekolonizace	k1gFnSc2
</s>
<s>
Rok	rok	k1gInSc1
Afriky	Afrika	k1gFnSc2
</s>
<s>
Den	den	k1gInSc1
pro	pro	k7c4
Šakala	šakal	k1gMnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kolonialismus	kolonialismus	k1gInSc1
|	|	kIx~
Historie	historie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
8504	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85036220	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85036220	#num#	k4
</s>
