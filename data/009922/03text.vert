<p>
<s>
Nachthareheb	Nachtharehba	k1gFnPc2	Nachtharehba
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
též	též	k9	též
jako	jako	k9	jako
Nektanebos	Nektanebos	k1gInSc4	Nektanebos
nebo	nebo	k8xC	nebo
Nektanebés	Nektanebés	k1gInSc4	Nektanebés
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
faraonem	faraon	k1gInSc7	faraon
30	[number]	k4	30
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
poslední	poslední	k2eAgInSc1d1	poslední
egyptský	egyptský	k2eAgInSc1d1	egyptský
faraon	faraon	k1gInSc1	faraon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
právě	právě	k6eAd1	právě
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
vládli	vládnout	k5eAaImAgMnP	vládnout
pouze	pouze	k6eAd1	pouze
cizí	cizí	k2eAgMnPc1d1	cizí
panovníci	panovník	k1gMnPc1	panovník
až	až	k9	až
do	do	k7c2	do
období	období	k1gNnSc2	období
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Vládl	vládnout	k5eAaImAgMnS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
360	[number]	k4	360
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
343	[number]	k4	343
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátek	počátek	k1gInSc1	počátek
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Persií	Persie	k1gFnSc7	Persie
==	==	k?	==
</s>
</p>
<p>
<s>
Králem	Král	k1gMnSc7	Král
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
během	během	k7c2	během
vojenského	vojenský	k2eAgNnSc2d1	vojenské
tažení	tažení	k1gNnSc2	tažení
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
faraona	faraon	k1gMnSc2	faraon
Džedhora	Džedhor	k1gMnSc2	Džedhor
<g/>
,	,	kIx,	,
do	do	k7c2	do
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
rozhořel	rozhořet	k5eAaPmAgMnS	rozhořet
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
povýšenosti	povýšenost	k1gFnSc3	povýšenost
spor	spor	k1gInSc4	spor
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
řeckými	řecký	k2eAgMnPc7d1	řecký
spojenci	spojenec	k1gMnPc7	spojenec
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
s	s	k7c7	s
vlastními	vlastní	k2eAgMnPc7d1	vlastní
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
skončil	skončit	k5eAaPmAgInS	skončit
Džedhorovým	Džedhorův	k2eAgInSc7d1	Džedhorův
útěkem	útěk	k1gInSc7	útěk
k	k	k7c3	k
Peršanům	peršan	k1gInPc3	peršan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
útěku	útěk	k1gInSc6	útěk
převzal	převzít	k5eAaPmAgMnS	převzít
Nachthareheb	Nachtharehba	k1gFnPc2	Nachtharehba
vedení	vedení	k1gNnSc1	vedení
nad	nad	k7c7	nad
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
odvedl	odvést	k5eAaPmAgMnS	odvést
jej	on	k3xPp3gInSc4	on
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
jeho	jeho	k3xOp3gMnSc2	jeho
předchůdce	předchůdce	k1gMnSc2	předchůdce
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
dostala	dostat	k5eAaPmAgFnS	dostat
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
vzpoury	vzpoura	k1gFnSc2	vzpoura
(	(	kIx(	(
<g/>
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Džedhora	Džedhor	k1gMnSc2	Džedhor
byly	být	k5eAaImAgFnP	být
tvrdě	tvrdě	k6eAd1	tvrdě
vymáhány	vymáhán	k2eAgFnPc1d1	vymáhána
daně	daň	k1gFnPc1	daň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nachthareheb	Nachtharehba	k1gFnPc2	Nachtharehba
jí	jíst	k5eAaImIp3nS	jíst
však	však	k9	však
zajistil	zajistit	k5eAaPmAgMnS	zajistit
prosperitu	prosperita	k1gFnSc4	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
uspořil	uspořit	k5eAaPmAgInS	uspořit
předem	předem	k6eAd1	předem
ukončenou	ukončený	k2eAgFnSc7d1	ukončená
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
využíval	využívat	k5eAaImAgInS	využívat
hlavně	hlavně	k9	hlavně
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Obnovil	obnovit	k5eAaPmAgMnS	obnovit
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
utužil	utužit	k5eAaPmAgMnS	utužit
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Řeky	Řek	k1gMnPc7	Řek
a	a	k8xC	a
Kypřany	Kypřan	k1gMnPc7	Kypřan
<g/>
,	,	kIx,	,
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
perského	perský	k2eAgInSc2d1	perský
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Egypt	Egypt	k1gInSc4	Egypt
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
vážné	vážný	k2eAgNnSc4d1	vážné
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
350	[number]	k4	350
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
Egypťané	Egypťan	k1gMnPc1	Egypťan
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
odrazili	odrazit	k5eAaPmAgMnP	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Přálo	přát	k5eAaImAgNnS	přát
jim	on	k3xPp3gMnPc3	on
totiž	totiž	k9	totiž
velké	velký	k2eAgNnSc1d1	velké
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Peršané	Peršan	k1gMnPc1	Peršan
chtěli	chtít	k5eAaImAgMnP	chtít
totiž	totiž	k9	totiž
obejít	obejít	k5eAaPmF	obejít
pohraniční	pohraniční	k2eAgFnPc4d1	pohraniční
pevnosti	pevnost	k1gFnPc4	pevnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zabloudili	zabloudit	k5eAaPmAgMnP	zabloudit
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
Nilu	Nil	k1gInSc3	Nil
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
právě	právě	k9	právě
období	období	k1gNnSc1	období
záplav	záplava	k1gFnPc2	záplava
<g/>
,	,	kIx,	,
spousta	spousta	k1gFnSc1	spousta
perských	perský	k2eAgMnPc2d1	perský
vojáků	voják	k1gMnPc2	voják
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
nalezla	naleznout	k5eAaPmAgFnS	naleznout
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vysílené	vysílený	k2eAgInPc1d1	vysílený
a	a	k8xC	a
zoufalé	zoufalý	k2eAgInPc1d1	zoufalý
zbytky	zbytek	k1gInPc1	zbytek
vojska	vojsko	k1gNnSc2	vojsko
poté	poté	k6eAd1	poté
napadly	napadnout	k5eAaPmAgInP	napadnout
pevnost	pevnost	k1gFnSc4	pevnost
Pelúsion	Pelúsion	k1gInSc1	Pelúsion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
pevnost	pevnost	k1gFnSc4	pevnost
Egypťané	Egypťan	k1gMnPc1	Egypťan
a	a	k8xC	a
Řekové	Řek	k1gMnPc1	Řek
Peršany	peršan	k1gInPc4	peršan
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
porazili	porazit	k5eAaPmAgMnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
bylo	být	k5eAaImAgNnS	být
poslední	poslední	k2eAgNnSc1d1	poslední
vítězství	vítězství	k1gNnSc1	vítězství
Egypťanů	Egypťan	k1gMnPc2	Egypťan
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
naposled	naposled	k6eAd1	naposled
na	na	k7c6	na
území	území	k1gNnSc6	území
jejich	jejich	k3xOp3gFnSc2	jejich
země	zem	k1gFnSc2	zem
a	a	k8xC	a
pod	pod	k7c7	pod
standartami	standarta	k1gFnPc7	standarta
jejich	jejich	k3xOp3gMnPc2	jejich
starých	starý	k2eAgMnPc2d1	starý
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
vítězství	vítězství	k1gNnSc6	vítězství
mohl	moct	k5eAaImAgInS	moct
Nachthareheb	Nachtharehba	k1gFnPc2	Nachtharehba
válku	válka	k1gFnSc4	válka
přesunout	přesunout	k5eAaPmF	přesunout
na	na	k7c6	na
území	území	k1gNnSc6	území
Persie	Persie	k1gFnSc2	Persie
<g/>
,	,	kIx,	,
přednost	přednost	k1gFnSc4	přednost
však	však	k9	však
dal	dát	k5eAaPmAgMnS	dát
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíš	nejspíš	k9	nejspíš
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Persie	Persie	k1gFnSc1	Persie
se	se	k3xPyFc4	se
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
porážky	porážka	k1gFnSc2	porážka
nevzpamatuje	vzpamatovat	k5eNaPmIp3nS	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
343	[number]	k4	343
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
však	však	k9	však
perská	perský	k2eAgNnPc1d1	perské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
tentokrát	tentokrát	k6eAd1	tentokrát
vedl	vést	k5eAaImAgMnS	vést
samotný	samotný	k2eAgMnSc1d1	samotný
král	král	k1gMnSc1	král
Artaxerxés	Artaxerxésa	k1gFnPc2	Artaxerxésa
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
stála	stát	k5eAaImAgFnS	stát
opět	opět	k6eAd1	opět
před	před	k7c7	před
Pelúsiem	Pelúsium	k1gNnSc7	Pelúsium
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
ale	ale	k9	ale
statečně	statečně	k6eAd1	statečně
vzdorovala	vzdorovat	k5eAaImAgFnS	vzdorovat
perskému	perský	k2eAgInSc3d1	perský
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Artaxerxés	Artaxerxés	k1gInSc1	Artaxerxés
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
pronikne	proniknout	k5eAaPmIp3nS	proniknout
po	po	k7c6	po
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
západního	západní	k2eAgNnSc2d1	západní
ramene	rameno	k1gNnSc2	rameno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgInS	střetnout
se	s	k7c7	s
sborem	sbor	k1gInSc7	sbor
řeckých	řecký	k2eAgMnPc2d1	řecký
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
však	však	k9	však
v	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
boji	boj	k1gInSc6	boj
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
perské	perský	k2eAgFnSc3d1	perská
přesile	přesila	k1gFnSc3	přesila
<g/>
.	.	kIx.	.
</s>
<s>
Peršanům	Peršan	k1gMnPc3	Peršan
potom	potom	k6eAd1	potom
padlo	padnout	k5eAaImAgNnS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc6	ruka
město	město	k1gNnSc1	město
Bubastis	Bubastis	k1gFnSc2	Bubastis
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
měl	mít	k5eAaImAgMnS	mít
volnou	volný	k2eAgFnSc4d1	volná
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Mennoferu	Mennofer	k1gInSc3	Mennofer
(	(	kIx(	(
<g/>
Memfidě	Memfida	k1gFnSc3	Memfida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnPc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
obsadil	obsadit	k5eAaPmAgMnS	obsadit
bývalé	bývalý	k2eAgNnSc4d1	bývalé
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
celého	celý	k2eAgInSc2d1	celý
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
chopil	chopit	k5eAaPmAgMnS	chopit
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
do	do	k7c2	do
města	město	k1gNnSc2	město
dorazil	dorazit	k5eAaPmAgMnS	dorazit
dřív	dříve	k6eAd2	dříve
Artaxerxés	Artaxerxésa	k1gFnPc2	Artaxerxésa
či	či	k8xC	či
Nachthareheb	Nachtharehba	k1gFnPc2	Nachtharehba
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Nachthareheb	Nachtharehba	k1gFnPc2	Nachtharehba
město	město	k1gNnSc4	město
obětoval	obětovat	k5eAaBmAgInS	obětovat
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
a	a	k8xC	a
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
měl	mít	k5eAaImAgMnS	mít
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
čítalo	čítat	k5eAaImAgNnS	čítat
40	[number]	k4	40
000	[number]	k4	000
řeckých	řecký	k2eAgMnPc2d1	řecký
a	a	k8xC	a
libyjských	libyjský	k2eAgMnPc2d1	libyjský
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
a	a	k8xC	a
60	[number]	k4	60
000	[number]	k4	000
egyptských	egyptský	k2eAgMnPc2d1	egyptský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
obrany	obrana	k1gFnSc2	obrana
Mennoferu	Mennofera	k1gFnSc4	Mennofera
<g/>
,	,	kIx,	,
naložil	naložit	k5eAaPmAgMnS	naložit
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
tolik	tolik	k4yIc4	tolik
drahocenností	drahocennost	k1gFnPc2	drahocennost
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
unesla	unést	k5eAaPmAgFnS	unést
<g/>
,	,	kIx,	,
a	a	k8xC	a
odplul	odplout	k5eAaPmAgInS	odplout
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
Nilu	Nil	k1gInSc2	Nil
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Núbie	Núbie	k1gFnSc2	Núbie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
osudu	osud	k1gInSc6	osud
není	být	k5eNaImIp3nS	být
více	hodně	k6eAd2	hodně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
králů	král	k1gMnPc2	král
starého	starý	k2eAgInSc2d1	starý
Egypta	Egypt	k1gInSc2	Egypt
</s>
</p>
