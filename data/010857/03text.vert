<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Vanáč	Vanáč	k1gMnSc1	Vanáč
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1963	[number]	k4	1963
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
radní	radní	k1gFnSc2	radní
a	a	k8xC	a
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
Lázně	lázeň	k1gFnSc2	lázeň
Bohdaneč	Bohdaneč	k1gMnSc1	Bohdaneč
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
lékař	lékař	k1gMnSc1	lékař
české	český	k2eAgFnSc2d1	Česká
hokejové	hokejový	k2eAgFnSc2d1	hokejová
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
specialistou	specialista	k1gMnSc7	specialista
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
chirurgie	chirurgie	k1gFnSc2	chirurgie
a	a	k8xC	a
sportovní	sportovní	k2eAgFnSc2d1	sportovní
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgMnS	být
primářem	primář	k1gMnSc7	primář
oddělení	oddělení	k1gNnSc2	oddělení
úrazové	úrazový	k2eAgFnSc2d1	Úrazová
chirurgie	chirurgie	k1gFnSc2	chirurgie
v	v	k7c6	v
krajské	krajský	k2eAgFnSc6d1	krajská
Nemocnici	nemocnice	k1gFnSc6	nemocnice
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k9	jako
lékař	lékař	k1gMnSc1	lékař
české	český	k2eAgFnSc2d1	Česká
hokejové	hokejový	k2eAgFnSc2d1	hokejová
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
let	let	k1gInSc1	let
je	být	k5eAaImIp3nS	být
klubovým	klubový	k2eAgMnSc7d1	klubový
lékařem	lékař	k1gMnSc7	lékař
pardubického	pardubický	k2eAgInSc2d1	pardubický
extraligového	extraligový	k2eAgInSc2d1	extraligový
hokejového	hokejový	k2eAgInSc2d1	hokejový
klubu	klub	k1gInSc2	klub
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
HC	HC	kA	HC
Dynamo	dynamo	k1gNnSc1	dynamo
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
lékař	lékař	k1gMnSc1	lékař
v	v	k7c6	v
Poradně	poradna	k1gFnSc6	poradna
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgFnSc4d1	sportovní
traumatologii	traumatologie	k1gFnSc4	traumatologie
při	při	k7c6	při
Chirurgickém	chirurgický	k2eAgNnSc6d1	chirurgické
oddělení	oddělení	k1gNnSc6	oddělení
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Pardubického	pardubický	k2eAgInSc2d1	pardubický
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
jako	jako	k9	jako
lékař	lékař	k1gMnSc1	lékař
na	na	k7c6	na
Poliklinice	poliklinika	k1gFnSc6	poliklinika
Vektor	vektor	k1gInSc1	vektor
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provádí	provádět	k5eAaImIp3nS	provádět
služby	služba	k1gFnPc4	služba
chirurgické	chirurgický	k2eAgFnSc2d1	chirurgická
ambulance	ambulance	k1gFnSc2	ambulance
a	a	k8xC	a
poradny	poradna	k1gFnSc2	poradna
pro	pro	k7c4	pro
sportovce	sportovec	k1gMnPc4	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
rovněž	rovněž	k9	rovněž
soukromě	soukromě	k6eAd1	soukromě
podniká	podnikat	k5eAaImIp3nS	podnikat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Vanáč	Vanáč	k1gMnSc1	Vanáč
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Lázně	lázeň	k1gFnSc2	lázeň
Bohdaneč	Bohdaneč	k1gInSc1	Bohdaneč
na	na	k7c6	na
Pardubicku	Pardubicko	k1gNnSc6	Pardubicko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
komunální	komunální	k2eAgFnSc2d1	komunální
politiky	politika	k1gFnSc2	politika
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
hnutí	hnutí	k1gNnSc4	hnutí
Nestraníci	nestraník	k1gMnPc1	nestraník
zvolen	zvolit	k5eAaPmNgMnS	zvolit
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
lídra	lídr	k1gMnSc2	lídr
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
subjektu	subjekt	k1gInSc2	subjekt
"	"	kIx"	"
<g/>
Koalice	koalice	k1gFnSc1	koalice
pro	pro	k7c4	pro
Lázně	lázeň	k1gFnPc4	lázeň
Bohdaneč	Bohdaneč	k1gInSc4	Bohdaneč
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
politického	politický	k2eAgNnSc2d1	politické
hnutí	hnutí	k1gNnSc2	hnutí
Nestraníci	nestraník	k1gMnPc1	nestraník
a	a	k8xC	a
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
"	"	kIx"	"
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zastupitelem	zastupitel	k1gMnSc7	zastupitel
města	město	k1gNnSc2	město
Lázně	lázeň	k1gFnSc2	lázeň
Bohdaneč	Bohdaneč	k1gInSc1	Bohdaneč
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
městského	městský	k2eAgMnSc2d1	městský
zastupitele	zastupitel	k1gMnSc2	zastupitel
obhájil	obhájit	k5eAaPmAgInS	obhájit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
když	když	k8xS	když
jako	jako	k8xC	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
vedl	vést	k5eAaImAgInS	vést
kandidátku	kandidátka	k1gFnSc4	kandidátka
sdružení	sdružení	k1gNnSc2	sdružení
nezávislých	závislý	k2eNgInPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
pak	pak	k6eAd1	pak
uspěl	uspět	k5eAaPmAgMnS	uspět
jako	jako	k9	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
na	na	k7c6	na
kandidátce	kandidátka	k1gFnSc6	kandidátka
sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
"	"	kIx"	"
<g/>
Lázeňské	lázeňský	k2eAgNnSc1d1	lázeňské
město	město	k1gNnSc1	město
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
také	také	k9	také
radním	radní	k1gMnPc3	radní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
lídrem	lídr	k1gMnSc7	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
STAN	stan	k1gInSc1	stan
v	v	k7c6	v
Pardubickém	pardubický	k2eAgInSc6d1	pardubický
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2016	[number]	k4	2016
však	však	k9	však
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
kandidátky	kandidátka	k1gFnSc2	kandidátka
z	z	k7c2	z
pracovních	pracovní	k2eAgInPc2d1	pracovní
a	a	k8xC	a
rodinných	rodinný	k2eAgInPc2d1	rodinný
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
lídra	lídr	k1gMnSc2	lídr
jej	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Hana	Hana	k1gFnSc1	Hana
Štěpánová	Štěpánová	k1gFnSc1	Štěpánová
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
17.10	[number]	k4	17.10
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
řídil	řídit	k5eAaImAgMnS	řídit
v	v	k7c6	v
opilosti	opilost	k1gFnSc6	opilost
automobil	automobil	k1gInSc4	automobil
a	a	k8xC	a
havaroval	havarovat	k5eAaPmAgMnS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dechové	dechový	k2eAgFnSc6d1	dechová
zkoušce	zkouška	k1gFnSc6	zkouška
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
naměřeno	naměřit	k5eAaBmNgNnS	naměřit
přes	přes	k7c4	přes
3	[number]	k4	3
promile	promile	k1gNnPc2	promile
alkoholu	alkohol	k1gInSc2	alkohol
v	v	k7c6	v
dechu	dech	k1gInSc6	dech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
