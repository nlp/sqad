<s>
Termín	termín	k1gInSc1	termín
prajazyk	prajazyk	k1gInSc1	prajazyk
(	(	kIx(	(
<g/>
řidčeji	řídce	k6eAd2	řídce
též	též	k9	též
protojazyk	protojazyk	k6eAd1	protojazyk
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
jazyk	jazyk	k1gMnSc1	jazyk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
diferenciací	diferenciace	k1gFnSc7	diferenciace
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
další	další	k2eAgInPc1d1	další
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
jazyky	jazyk	k1gInPc1	jazyk
či	či	k8xC	či
nářečí	nářečí	k1gNnSc1	nářečí
<g/>
,	,	kIx,	,
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
jejich	jejich	k3xOp3gMnSc4	jejich
společného	společný	k2eAgMnSc4d1	společný
předka	předek	k1gMnSc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc1d1	konkrétní
prajazyky	prajazyk	k1gInPc1	prajazyk
bývají	bývat	k5eAaImIp3nP	bývat
obvykle	obvykle	k6eAd1	obvykle
označeny	označit	k5eAaPmNgInP	označit
předponou	předpona	k1gFnSc7	předpona
pra-	pra-	k?	pra-
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
zvyklosti	zvyklost	k1gFnSc2	zvyklost
pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
upouští	upouštět	k5eAaImIp3nS	upouštět
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklady	příklad	k1gInPc1	příklad
prajazyků	prajazyk	k1gInPc2	prajazyk
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
příbuznosti	příbuznost	k1gFnSc2	příbuznost
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
praindoevropštinu	praindoevropština	k1gFnSc4	praindoevropština
<g/>
,	,	kIx,	,
praslovanštinu	praslovanština	k1gFnSc4	praslovanština
či	či	k8xC	či
pračeštinu	pračeština	k1gFnSc4	pračeština
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
lze	lze	k6eAd1	lze
prajazyk	prajazyk	k1gInSc1	prajazyk
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
doloženým	doložený	k2eAgInSc7d1	doložený
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
prarománština	prarománština	k1gFnSc1	prarománština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
společným	společný	k2eAgInSc7d1	společný
předkem	předek	k1gInSc7	předek
všech	všecek	k3xTgInPc2	všecek
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
překrývá	překrývat	k5eAaImIp3nS	překrývat
s	s	k7c7	s
lidovou	lidový	k2eAgFnSc7d1	lidová
latinou	latina	k1gFnSc7	latina
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
záznamy	záznam	k1gInPc1	záznam
prajazyků	prajazyk	k1gInPc2	prajazyk
jsou	být	k5eAaImIp3nP	být
buďto	buďto	k8xC	buďto
nedokonalé	dokonalý	k2eNgNnSc4d1	nedokonalé
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
prajazyků	prajazyk	k1gInPc2	prajazyk
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zejména	zejména	k9	zejména
metody	metoda	k1gFnPc1	metoda
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
a	a	k8xC	a
vnější	vnější	k2eAgFnSc2d1	vnější
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
<g/>
.	.	kIx.	.
</s>
