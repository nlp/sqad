<s>
Někdy	někdy	k6eAd1	někdy
lze	lze	k6eAd1	lze
prajazyk	prajazyk	k1gInSc1	prajazyk
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
doloženým	doložený	k2eAgInSc7d1	doložený
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
prarománština	prarománština	k1gFnSc1	prarománština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
společným	společný	k2eAgInSc7d1	společný
předkem	předek	k1gInSc7	předek
všech	všecek	k3xTgInPc2	všecek
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
překrývá	překrývat	k5eAaImIp3nS	překrývat
s	s	k7c7	s
lidovou	lidový	k2eAgFnSc7d1	lidová
latinou	latina	k1gFnSc7	latina
<g/>
.	.	kIx.	.
</s>
