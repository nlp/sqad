<p>
<s>
Trhák	trhák	k1gInSc1	trhák
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
komediální	komediální	k2eAgInSc1d1	komediální
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podskalský	podskalský	k2eAgMnSc1d1	podskalský
a	a	k8xC	a
kde	kde	k6eAd1	kde
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
tehdy	tehdy	k6eAd1	tehdy
populárních	populární	k2eAgInPc2d1	populární
československých	československý	k2eAgInPc2d1	československý
herců	herc	k1gInPc2	herc
i	i	k8xC	i
hvězd	hvězda	k1gFnPc2	hvězda
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
ve	v	k7c6	v
stylizované	stylizovaný	k2eAgFnSc6d1	stylizovaná
nadsázce	nadsázka	k1gFnSc6	nadsázka
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c4	o
natáčení	natáčení	k1gNnSc4	natáčení
muzikálu	muzikál	k1gInSc2	muzikál
ze	z	k7c2	z
života	život	k1gInSc2	život
současné	současný	k2eAgFnSc2d1	současná
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
text	text	k1gInSc1	text
distributora	distributor	k1gMnSc2	distributor
praví	pravit	k5eAaBmIp3nS	pravit
toto	tento	k3xDgNnSc1	tento
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Panu	Pan	k1gMnSc3	Pan
Jíšovi	Jíša	k1gMnSc3	Jíša
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
udat	udat	k5eAaPmF	udat
na	na	k7c6	na
Barrandově	Barrandov	k1gInSc6	Barrandov
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
přípravných	přípravný	k2eAgFnPc2d1	přípravná
prací	práce	k1gFnPc2	práce
s	s	k7c7	s
hrůzou	hrůza	k1gFnSc7	hrůza
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
filmaři	filmař	k1gMnPc1	filmař
naprosto	naprosto	k6eAd1	naprosto
obešli	obejít	k5eAaPmAgMnP	obejít
jeho	jeho	k3xOp3gInSc4	jeho
záměr	záměr	k1gInSc4	záměr
–	–	k?	–
místo	místo	k7c2	místo
sondy	sonda	k1gFnSc2	sonda
do	do	k7c2	do
života	život	k1gInSc2	život
současné	současný	k2eAgFnSc2d1	současná
vesnice	vesnice	k1gFnSc2	vesnice
připravují	připravovat	k5eAaImIp3nP	připravovat
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
hudební	hudební	k2eAgFnSc4d1	hudební
show	show	k1gFnSc4	show
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
divácký	divácký	k2eAgInSc1d1	divácký
trhák	trhák	k1gInSc1	trhák
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
první	první	k4xOgInPc1	první
záběry	záběr	k1gInPc1	záběr
hovoří	hovořit	k5eAaImIp3nP	hovořit
za	za	k7c4	za
mnohé	mnohé	k1gNnSc4	mnohé
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obce	obec	k1gFnSc2	obec
Lipovec	Lipovec	k1gInSc4	Lipovec
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
elegán	elegán	k1gMnSc1	elegán
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
všichni	všechen	k3xTgMnPc1	všechen
místní	místní	k2eAgMnPc1d1	místní
funkcionáři	funkcionář	k1gMnPc1	funkcionář
i	i	k8xC	i
prostí	prostý	k2eAgMnPc1d1	prostý
občané	občan	k1gMnPc1	občan
vítají	vítat	k5eAaImIp3nP	vítat
zpěvem	zpěv	k1gInSc7	zpěv
a	a	k8xC	a
tancem	tanec	k1gInSc7	tanec
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
tento	tento	k3xDgMnSc1	tento
kýžený	kýžený	k2eAgMnSc1d1	kýžený
odborník	odborník	k1gMnSc1	odborník
jim	on	k3xPp3gFnPc3	on
má	mít	k5eAaImIp3nS	mít
svými	svůj	k3xOyFgFnPc7	svůj
progresivními	progresivní	k2eAgFnPc7d1	progresivní
metodami	metoda	k1gFnPc7	metoda
pomoci	pomoc	k1gFnSc2	pomoc
zvýšit	zvýšit	k5eAaPmF	zvýšit
hektarové	hektarový	k2eAgInPc4d1	hektarový
výnosy	výnos	k1gInPc4	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
podobném	podobný	k2eAgMnSc6d1	podobný
duchu	duch	k1gMnSc6	duch
a	a	k8xC	a
novopečený	novopečený	k2eAgMnSc1d1	novopečený
scenárista	scenárista	k1gMnSc1	scenárista
se	se	k3xPyFc4	se
nestačí	stačit	k5eNaBmIp3nS	stačit
divit	divit	k5eAaImF	divit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
filmu	film	k1gInSc2	film
tedy	tedy	k9	tedy
začíná	začínat	k5eAaImIp3nS	začínat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
naivní	naivní	k2eAgMnSc1d1	naivní
scenárista	scenárista	k1gMnSc1	scenárista
Jíša	Jíša	k1gMnSc1	Jíša
dostaví	dostavit	k5eAaPmIp3nS	dostavit
do	do	k7c2	do
Filmového	filmový	k2eAgNnSc2d1	filmové
studia	studio	k1gNnSc2	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
a	a	k8xC	a
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
scénář	scénář	k1gInSc1	scénář
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původně	původně	k6eAd1	původně
skromného	skromný	k2eAgInSc2d1	skromný
nápadu	nápad	k1gInSc2	nápad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
co	co	k9	co
nejpravdivější	pravdivý	k2eAgFnSc4d3	nejpravdivější
<g/>
,	,	kIx,	,
však	však	k9	však
sebestředný	sebestředný	k2eAgMnSc1d1	sebestředný
režisér	režisér	k1gMnSc1	režisér
Kohoutek	Kohoutek	k1gMnSc1	Kohoutek
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
megalomanský	megalomanský	k2eAgInSc4d1	megalomanský
projekt	projekt	k1gInSc4	projekt
vlastní	vlastní	k2eAgFnSc2d1	vlastní
filmové	filmový	k2eAgFnSc2d1	filmová
vesnice	vesnice	k1gFnSc2	vesnice
včetně	včetně	k7c2	včetně
zámečku	zámeček	k1gInSc2	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
natáčení	natáčení	k1gNnSc2	natáčení
však	však	k9	však
svou	svůj	k3xOyFgFnSc7	svůj
neschopností	neschopnost	k1gFnSc7	neschopnost
způsobí	způsobit	k5eAaPmIp3nS	způsobit
zničení	zničení	k1gNnSc1	zničení
většiny	většina	k1gFnSc2	většina
filmové	filmový	k2eAgFnSc2d1	filmová
lokace	lokace	k1gFnSc2	lokace
a	a	k8xC	a
pro	pro	k7c4	pro
náhlý	náhlý	k2eAgInSc4d1	náhlý
nedostatek	nedostatek	k1gInSc4	nedostatek
peněz	peníze	k1gInPc2	peníze
musí	muset	k5eAaImIp3nS	muset
film	film	k1gInSc1	film
dotočit	dotočit	k5eAaPmF	dotočit
co	co	k9	co
nejúsporněji	úsporně	k6eAd3	úsporně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
vykrádání	vykrádání	k1gNnSc2	vykrádání
záběrů	záběr	k1gInPc2	záběr
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Věčně	věčně	k6eAd1	věčně
nervózní	nervózní	k2eAgMnSc1d1	nervózní
a	a	k8xC	a
agresivní	agresivní	k2eAgInSc1d1	agresivní
produkční	produkční	k2eAgInSc1d1	produkční
Šus	šus	k1gInSc1	šus
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
účastnit	účastnit	k5eAaImF	účastnit
premiérové	premiérový	k2eAgFnPc4d1	premiérová
projekce	projekce	k1gFnPc4	projekce
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
kině	kino	k1gNnSc6	kino
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
je	být	k5eAaImIp3nS	být
však	však	k9	však
diváky	divák	k1gMnPc4	divák
aplaudován	aplaudován	k2eAgInSc4d1	aplaudován
včetně	včetně	k7c2	včetně
poslední	poslední	k2eAgFnSc2d1	poslední
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vlivem	vliv	k1gInSc7	vliv
bouřky	bouřka	k1gFnSc2	bouřka
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
projekční	projekční	k2eAgFnSc2d1	projekční
plochy	plocha	k1gFnSc2	plocha
i	i	k8xC	i
filmové	filmový	k2eAgFnSc2d1	filmová
kopie	kopie	k1gFnSc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
to	ten	k3xDgNnSc4	ten
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
režijní	režijní	k2eAgInSc4d1	režijní
záměr	záměr	k1gInSc4	záměr
a	a	k8xC	a
při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
kina	kino	k1gNnSc2	kino
všichni	všechen	k3xTgMnPc1	všechen
do	do	k7c2	do
jednoho	jeden	k4xCgMnSc4	jeden
film	film	k1gInSc1	film
chválí	chválit	k5eAaImIp3nP	chválit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Autoři	autor	k1gMnPc1	autor
scénáře	scénář	k1gInSc2	scénář
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
využili	využít	k5eAaPmAgMnP	využít
neustálých	neustálý	k2eAgFnPc2d1	neustálá
žádostí	žádost	k1gFnPc2	žádost
kulturně-propagačních	kulturněropagační	k2eAgMnPc2d1	kulturně-propagační
pracovníků	pracovník	k1gMnPc2	pracovník
o	o	k7c4	o
komunální	komunální	k2eAgFnSc4d1	komunální
satiru	satira	k1gFnSc4	satira
a	a	k8xC	a
dovedli	dovést	k5eAaPmAgMnP	dovést
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
absurdní	absurdní	k2eAgFnSc3d1	absurdní
dokonalosti	dokonalost	k1gFnSc3	dokonalost
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
postavě	postava	k1gFnSc6	postava
nepoctivého	poctivý	k2eNgMnSc4d1	nepoctivý
zedníka	zedník	k1gMnSc4	zedník
Merunky	Merunka	k1gFnSc2	Merunka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parodovány	parodován	k2eAgFnPc1d1	parodována
byly	být	k5eAaImAgFnP	být
i	i	k9	i
tehdejší	tehdejší	k2eAgFnPc1d1	tehdejší
"	"	kIx"	"
<g/>
akce	akce	k1gFnPc1	akce
Z	z	k7c2	z
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
po	po	k7c6	po
Kohoutkově	Kohoutkův	k2eAgFnSc6d1	Kohoutkova
demolici	demolice	k1gFnSc6	demolice
vesnice	vesnice	k1gFnSc2	vesnice
bylo	být	k5eAaImAgNnS	být
mj.	mj.	kA	mj.
nutné	nutný	k2eAgNnSc1d1	nutné
natočit	natočit	k5eAaBmF	natočit
pohřeb	pohřeb	k1gInSc4	pohřeb
stařečka	stařeček	k1gMnSc2	stařeček
Lukeše	Lukeš	k1gMnSc2	Lukeš
<g/>
.	.	kIx.	.
</s>
<s>
Kohoutek	Kohoutek	k1gMnSc1	Kohoutek
to	ten	k3xDgNnSc4	ten
tedy	tedy	k9	tedy
musel	muset	k5eAaImAgMnS	muset
natočit	natočit	k5eAaBmF	natočit
jako	jako	k9	jako
brigádu	brigáda	k1gFnSc4	brigáda
a	a	k8xC	a
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ve	v	k7c6	v
filmu	film	k1gInSc6	film
zazní	zaznít	k5eAaPmIp3nS	zaznít
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgFnSc1	ten
akce	akce	k1gFnSc1	akce
Kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
kostým	kostým	k1gInSc4	kostým
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
za	za	k7c7	za
rakví	rakev	k1gFnSc7	rakev
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
původní	původní	k2eAgFnSc1d1	původní
replika	replika	k1gFnSc1	replika
zněla	znět	k5eAaImAgFnS	znět
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
má	mít	k5eAaImIp3nS	mít
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
do	do	k7c2	do
průvodu	průvod	k1gInSc2	průvod
<g/>
"	"	kIx"	"
–	–	k?	–
musela	muset	k5eAaImAgFnS	muset
však	však	k9	však
být	být	k5eAaImF	být
změněna	změnit	k5eAaPmNgFnS	změnit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nestala	stát	k5eNaPmAgFnS	stát
obětí	oběť	k1gFnSc7	oběť
cenzury	cenzura	k1gFnSc2	cenzura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezamýšlené	zamýšlený	k2eNgInPc4d1	nezamýšlený
důsledky	důsledek	k1gInPc4	důsledek
"	"	kIx"	"
<g/>
hurá-akcí	hurákce	k1gFnPc2	hurá-akce
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
předvedeny	předveden	k2eAgFnPc1d1	předvedena
vzápětí	vzápětí	k6eAd1	vzápětí
–	–	k?	–
pohřbívaný	pohřbívaný	k2eAgMnSc1d1	pohřbívaný
dědeček	dědeček	k1gMnSc1	dědeček
Lukeš	Lukeš	k1gMnSc1	Lukeš
jde	jít	k5eAaImIp3nS	jít
hned	hned	k6eAd1	hned
za	za	k7c7	za
"	"	kIx"	"
<g/>
svým	své	k1gNnSc7	své
<g/>
"	"	kIx"	"
pohřebním	pohřební	k2eAgInSc7d1	pohřební
vozem	vůz	k1gInSc7	vůz
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
záběrech	záběr	k1gInPc6	záběr
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
viditelnější	viditelný	k2eAgNnSc1d2	viditelnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
písní	píseň	k1gFnPc2	píseň
měla	mít	k5eAaImAgFnS	mít
úmyslně	úmyslně	k6eAd1	úmyslně
chytlavé	chytlavý	k2eAgFnSc2d1	chytlavá
melodie	melodie	k1gFnSc2	melodie
a	a	k8xC	a
infantilní	infantilní	k2eAgInPc1d1	infantilní
texty	text	k1gInPc1	text
–	–	k?	–
platí	platit	k5eAaImIp3nP	platit
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
pásmo	pásmo	k1gNnSc4	pásmo
Muzikál	muzikál	k1gInSc1	muzikál
na	na	k7c6	na
návsi	náves	k1gFnSc6	náves
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
jiné	jiný	k2eAgFnPc1d1	jiná
písně	píseň	k1gFnPc1	píseň
měly	mít	k5eAaImAgFnP	mít
poměrně	poměrně	k6eAd1	poměrně
hodnotnou	hodnotný	k2eAgFnSc4d1	hodnotná
melodii	melodie	k1gFnSc4	melodie
<g/>
,	,	kIx,	,
aranžmá	aranžmá	k1gNnSc4	aranžmá
i	i	k8xC	i
texty	text	k1gInPc4	text
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
interpreti	interpret	k1gMnPc1	interpret
je	on	k3xPp3gFnPc4	on
pak	pak	k6eAd1	pak
převzali	převzít	k5eAaPmAgMnP	převzít
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
stálého	stálý	k2eAgInSc2d1	stálý
repertoáru	repertoár	k1gInSc2	repertoár
–	–	k?	–
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
duet	duet	k1gInSc1	duet
Hany	Hana	k1gFnSc2	Hana
Zagorové	Zagorová	k1gFnSc2	Zagorová
a	a	k8xC	a
Juraje	Juraj	k1gInSc2	Juraj
Kukury	Kukura	k1gFnSc2	Kukura
Lásko	láska	k1gFnSc5	láska
<g/>
,	,	kIx,	,
Amore	Amor	k1gMnSc5	Amor
a	a	k8xC	a
duet	duet	k1gInSc1	duet
Waldemara	Waldemar	k1gMnSc2	Waldemar
Matušky	Matuška	k1gMnSc2	Matuška
a	a	k8xC	a
Laďky	Laďka	k1gMnSc2	Laďka
Kozderkové	Kozderková	k1gFnSc2	Kozderková
Znám	znát	k5eAaImIp1nS	znát
tu	tu	k6eAd1	tu
louku	louka	k1gFnSc4	louka
voňavou	voňavý	k2eAgFnSc4d1	voňavá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Kukurova	Kukurův	k2eAgFnSc1d1	Kukurův
sólová	sólový	k2eAgFnSc1d1	sólová
píseň	píseň	k1gFnSc1	píseň
Sek	sek	k1gInSc1	sek
<g/>
'	'	kIx"	'
jsem	být	k5eAaImIp1nS	být
sexem	sex	k1gInSc7	sex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
celé	celý	k2eAgFnPc1d1	celá
kapely	kapela	k1gFnPc1	kapela
–	–	k?	–
například	například	k6eAd1	například
Fešáci	fešák	k1gMnPc1	fešák
Michala	Michal	k1gMnSc2	Michal
Tučného	tučný	k2eAgMnSc2d1	tučný
se	se	k3xPyFc4	se
předvedou	předvést	k5eAaPmIp3nP	předvést
jako	jako	k8xS	jako
sbor	sbor	k1gInSc1	sbor
hasičů	hasič	k1gMnPc2	hasič
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
Apollobeat	Apollobeat	k1gInSc1	Apollobeat
jako	jako	k8xS	jako
tým	tým	k1gInSc1	tým
traktoristů	traktorista	k1gMnPc2	traktorista
<g/>
,	,	kIx,	,
Schovanky	schovanka	k1gFnPc1	schovanka
se	se	k3xPyFc4	se
převlékly	převléknout	k5eAaPmAgFnP	převléknout
za	za	k7c4	za
ošetřovatelky	ošetřovatelka	k1gFnPc4	ošetřovatelka
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Kroky	krok	k1gInPc4	krok
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
Kratochvílovou	Kratochvílová	k1gFnSc7	Kratochvílová
hrají	hrát	k5eAaImIp3nP	hrát
obsluhu	obsluha	k1gFnSc4	obsluha
vesnické	vesnický	k2eAgFnSc2d1	vesnická
prodejny	prodejna	k1gFnSc2	prodejna
Jednota	jednota	k1gFnSc1	jednota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
viditelné	viditelný	k2eAgFnSc6d1	viditelná
role	rola	k1gFnSc6	rola
se	se	k3xPyFc4	se
podělili	podělit	k5eAaPmAgMnP	podělit
i	i	k9	i
členové	člen	k1gMnPc1	člen
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dcery	dcera	k1gFnPc1	dcera
hajného	hajný	k1gMnSc2	hajný
Kaliny	Kalina	k1gMnSc2	Kalina
(	(	kIx(	(
<g/>
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
)	)	kIx)	)
hrají	hrát	k5eAaImIp3nP	hrát
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Olga	Olga	k1gFnSc1	Olga
Blechová	Blechová	k1gFnSc1	Blechová
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Hana	Hana	k1gFnSc1	Hana
Buštíková	Buštíková	k1gFnSc1	Buštíková
(	(	kIx(	(
<g/>
z	z	k7c2	z
dua	duo	k1gNnSc2	duo
Kamélie	kamélie	k1gFnSc2	kamélie
<g/>
)	)	kIx)	)
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
Dagmar	Dagmar	k1gFnSc1	Dagmar
Veškrnová	Veškrnová	k1gFnSc1	Veškrnová
(	(	kIx(	(
<g/>
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
jí	jíst	k5eAaImIp3nS	jíst
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
hlas	hlas	k1gInSc4	hlas
Jitka	Jitka	k1gFnSc1	Jitka
Zelenková	Zelenková	k1gFnSc1	Zelenková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
gagu	gag	k1gInSc6	gag
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
i	i	k9	i
fyzické	fyzický	k2eAgFnSc2d1	fyzická
podobnosti	podobnost	k1gFnSc2	podobnost
Ladislava	Ladislav	k1gMnSc2	Ladislav
Smoljaka	Smoljak	k1gMnSc2	Smoljak
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Kohoutek	Kohoutek	k1gMnSc1	Kohoutek
<g/>
)	)	kIx)	)
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Spáleným	spálený	k2eAgNnSc7d1	spálené
–	–	k?	–
když	když	k8xS	když
se	se	k3xPyFc4	se
při	při	k7c6	při
castingu	casting	k1gInSc6	casting
postavili	postavit	k5eAaPmAgMnP	postavit
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
úmyslně	úmyslně	k6eAd1	úmyslně
podobně	podobně	k6eAd1	podobně
oblečeni	oblečen	k2eAgMnPc1d1	oblečen
<g/>
,	,	kIx,	,
učesáni	učesán	k2eAgMnPc1d1	učesán
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
stejného	stejný	k2eAgInSc2d1	stejný
zástřihu	zástřih	k1gInSc2	zástřih
kníru	knír	k1gInSc2	knír
<g/>
)	)	kIx)	)
a	a	k8xC	a
postaveni	postavit	k5eAaPmNgMnP	postavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
hleděli	hledět	k5eAaImAgMnP	hledět
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
–	–	k?	–
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
téměř	téměř	k6eAd1	téměř
dvojníci	dvojník	k1gMnPc1	dvojník
a	a	k8xC	a
Kohoutek	kohoutek	k1gInSc1	kohoutek
nemohl	moct	k5eNaImAgInS	moct
do	do	k7c2	do
role	role	k1gFnSc2	role
předsedy	předseda	k1gMnSc2	předseda
JZD	JZD	kA	JZD
Rambouska	Rambousek	k1gMnSc2	Rambousek
vzít	vzít	k5eAaPmF	vzít
nikoho	nikdo	k3yNnSc4	nikdo
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
svých	svůj	k3xOyFgFnPc2	svůj
filmových	filmový	k2eAgFnPc2d1	filmová
rolí	role	k1gFnPc2	role
zde	zde	k6eAd1	zde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Václav	Václav	k1gMnSc1	Václav
Lohniský	Lohniský	k1gMnSc1	Lohniský
v	v	k7c6	v
roli	role	k1gFnSc6	role
nepoctivého	poctivý	k2eNgMnSc4d1	nepoctivý
zedníka	zedník	k1gMnSc4	zedník
Merunky	Merunka	k1gFnSc2	Merunka
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
totiž	totiž	k9	totiž
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
natáčení	natáčení	k1gNnSc2	natáčení
a	a	k8xC	a
ve	v	k7c6	v
filmu	film	k1gInSc6	film
jej	on	k3xPp3gMnSc4	on
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
skutečně	skutečně	k6eAd1	skutečně
zahlédnout	zahlédnout	k5eAaPmF	zahlédnout
jen	jen	k9	jen
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
záběru	záběr	k1gInSc6	záběr
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
dvojníkem	dvojník	k1gMnSc7	dvojník
a	a	k8xC	a
zabírán	zabírán	k2eAgInSc1d1	zabírán
pouze	pouze	k6eAd1	pouze
zezadu	zezadu	k6eAd1	zezadu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
filmu	film	k1gInSc2	film
jemně	jemně	k6eAd1	jemně
naráží	narážet	k5eAaImIp3nS	narážet
i	i	k9	i
na	na	k7c4	na
národnostní	národnostní	k2eAgNnSc4d1	národnostní
složení	složení	k1gNnSc4	složení
Československa	Československo	k1gNnSc2	Československo
–	–	k?	–
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nadšených	nadšený	k2eAgMnPc2d1	nadšený
diváků	divák	k1gMnPc2	divák
při	při	k7c6	při
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
kina	kino	k1gNnSc2	kino
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stejně	stejně	k6eAd1	stejně
jsme	být	k5eAaImIp1nP	být
my	my	k3xPp1nPc1	my
Češi	Čech	k1gMnPc5	Čech
šikovnej	šikovnej	k?	šikovnej
národ	národ	k1gInSc1	národ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kamera	kamera	k1gFnSc1	kamera
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
záběru	záběr	k1gInSc6	záběr
pomalu	pomalu	k6eAd1	pomalu
najede	najet	k5eAaPmIp3nS	najet
do	do	k7c2	do
hlediště	hlediště	k1gNnSc2	hlediště
kina	kino	k1gNnSc2	kino
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
osaměle	osaměle	k6eAd1	osaměle
sedí	sedit	k5eAaImIp3nS	sedit
zachmuřený	zachmuřený	k2eAgMnSc1d1	zachmuřený
bývalý	bývalý	k2eAgMnSc1d1	bývalý
"	"	kIx"	"
<g/>
hlavní	hlavní	k2eAgMnSc1d1	hlavní
milovník	milovník	k1gMnSc1	milovník
<g/>
"	"	kIx"	"
Lenský	Lenský	k1gMnSc1	Lenský
<g/>
,	,	kIx,	,
představovaný	představovaný	k2eAgInSc1d1	představovaný
Slovákem	Slovák	k1gMnSc7	Slovák
Jurajem	Juraj	k1gInSc7	Juraj
Kukurou	Kukura	k1gFnSc7	Kukura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Schelinger	Schelinger	k1gMnSc1	Schelinger
zemřel	zemřít	k5eAaPmAgMnS	zemřít
pouhých	pouhý	k2eAgInPc2d1	pouhý
dvanáct	dvanáct	k4xCc4	dvanáct
dnů	den	k1gInPc2	den
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
byla	být	k5eAaImAgFnS	být
1.	[number]	k4	1.
dubna	duben	k1gInSc2	duben
1981	[number]	k4	1981
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Schelinger	Schelinger	k1gMnSc1	Schelinger
zemřel	zemřít	k5eAaPmAgMnS	zemřít
13.	[number]	k4	13.
dubna	duben	k1gInSc2	duben
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
konečných	konečný	k2eAgInPc6d1	konečný
titulcích	titulek	k1gInPc6	titulek
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
chybně	chybně	k6eAd1	chybně
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
l	l	kA	l
(	(	kIx(	(
<g/>
Schellinger	Schellinger	k1gMnSc1	Schellinger
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
1.	[number]	k4	1.
díle	dílo	k1gNnSc6	dílo
zábavného	zábavný	k2eAgInSc2d1	zábavný
pořadu	pořad	k1gInSc2	pořad
Veselé	Veselé	k2eAgFnSc2d1	Veselé
příhody	příhoda	k1gFnSc2	příhoda
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
a	a	k8xC	a
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
mystifikační	mystifikační	k2eAgFnPc4d1	mystifikační
historky	historka	k1gFnPc4	historka
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
vzniku	vznik	k1gInSc3	vznik
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgFnPc1d1	technická
poznámky	poznámka	k1gFnPc1	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
vizuálně	vizuálně	k6eAd1	vizuálně
přísně	přísně	k6eAd1	přísně
oddělován	oddělován	k2eAgInSc1d1	oddělován
děj	děj	k1gInSc1	děj
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
a	a	k8xC	a
děj	děj	k1gInSc1	děj
jako	jako	k8xC	jako
z	z	k7c2	z
hotového	hotový	k2eAgInSc2d1	hotový
filmu	film	k1gInSc2	film
–	–	k?	–
druhé	druhý	k4xOgInPc4	druhý
záběry	záběr	k1gInPc4	záběr
jsou	být	k5eAaImIp3nP	být
úmyslně	úmyslně	k6eAd1	úmyslně
přeexponovány	přeexponován	k2eAgFnPc1d1	přeexponována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
jihočeských	jihočeský	k2eAgFnPc6d1	Jihočeská
<g/>
)	)	kIx)	)
lokacích	lokace	k1gFnPc6	lokace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Lčovice	Lčovice	k1gFnSc1	Lčovice
–	–	k?	–
skutečné	skutečný	k2eAgNnSc4d1	skutečné
jméno	jméno	k1gNnSc4	jméno
malého	malý	k2eAgNnSc2d1	malé
nádražíčka	nádražíčko	k1gNnSc2	nádražíčko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
přejmenovaného	přejmenovaný	k2eAgInSc2d1	přejmenovaný
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
Lipovec	Lipovec	k1gInSc1	Lipovec
</s>
</p>
<p>
<s>
Nahořany	Nahořan	k1gMnPc4	Nahořan
–	–	k?	–
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
návsi	náves	k1gFnSc2	náves
(	(	kIx(	(
<g/>
táž	týž	k3xTgFnSc1	týž
náves	náves	k1gFnSc1	náves
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Princezna	princezna	k1gFnSc1	princezna
ze	z	k7c2	z
mlejna	mlejn	k1gInSc2	mlejn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lukešovic	Lukešovice	k1gFnPc2	Lukešovice
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
kaplička	kaplička	k1gFnSc1	kaplička
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
stromem	strom	k1gInSc7	strom
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Dřešín	Dřešín	k1gInSc1	Dřešín
–	–	k?	–
prodejna	prodejna	k1gFnSc1	prodejna
Jednoty	jednota	k1gFnSc2	jednota
se	s	k7c7	s
svůdnou	svůdný	k2eAgFnSc7d1	svůdná
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
Janou	Jana	k1gFnSc7	Jana
Kratochvílovou	Kratochvílová	k1gFnSc7	Kratochvílová
</s>
</p>
<p>
<s>
Krušlov	Krušlov	k1gInSc4	Krušlov
–	–	k?	–
několikasekundové	několikasekundový	k2eAgInPc4d1	několikasekundový
záběry	záběr	k1gInPc4	záběr
z	z	k7c2	z
unikátního	unikátní	k2eAgInSc2d1	unikátní
vyřezávaného	vyřezávaný	k2eAgInSc2d1	vyřezávaný
včelínu	včelín	k1gInSc2	včelín
</s>
</p>
<p>
<s>
Červený	červený	k2eAgInSc1d1	červený
Hrádek	hrádek	k1gInSc1	hrádek
u	u	k7c2	u
Sedlčan	Sedlčany	k1gInPc2	Sedlčany
–	–	k?	–
pravé	pravý	k2eAgNnSc4d1	pravé
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
"	"	kIx"	"
<g/>
produkcí	produkce	k1gFnSc7	produkce
postavený	postavený	k2eAgInSc4d1	postavený
<g/>
"	"	kIx"	"
Lenského	Lenského	k2eAgInSc4d1	Lenského
zámeček	zámeček	k1gInSc4	zámeček
</s>
</p>
<p>
<s>
letiště	letiště	k1gNnSc1	letiště
Letňany	Letňan	k1gMnPc4	Letňan
–	–	k?	–
"	"	kIx"	"
<g/>
letecké	letecký	k2eAgInPc4d1	letecký
<g/>
"	"	kIx"	"
záběry	záběr	k1gInPc4	záběr
Jiřího	Jiří	k1gMnSc2	Jiří
Korna	korna	k1gFnSc1	korna
v	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
práškovacího	práškovací	k2eAgNnSc2d1	práškovací
letadla	letadlo	k1gNnSc2	letadlo
</s>
</p>
<p>
<s>
Vlachovo	Vlachův	k2eAgNnSc1d1	Vlachovo
Březí	březí	k1gNnSc1	březí
–	–	k?	–
budova	budova	k1gFnSc1	budova
pošty	pošta	k1gFnSc2	pošta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pošťačka	pošťačka	k1gFnSc1	pošťačka
(	(	kIx(	(
<g/>
Laďka	Laďka	k1gFnSc1	Laďka
Kozderková	Kozderková	k1gFnSc1	Kozderková
<g/>
)	)	kIx)	)
nemůže	moct	k5eNaImIp3nS	moct
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
od	od	k7c2	od
okna	okno	k1gNnSc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Táž	týž	k3xTgFnSc1	týž
budova	budova	k1gFnSc1	budova
představuje	představovat	k5eAaImIp3nS	představovat
i	i	k9	i
vyloupenou	vyloupený	k2eAgFnSc4d1	vyloupená
spořitelnu	spořitelna	k1gFnSc4	spořitelna
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Drahé	drahý	k2eAgFnSc2d1	drahá
tety	teta	k1gFnSc2	teta
a	a	k8xC	a
já	já	k3xPp1nSc1	já
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prachatice	Prachatice	k1gFnPc1	Prachatice
–	–	k?	–
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
svatební	svatební	k2eAgFnSc1d1	svatební
scéna	scéna	k1gFnSc1	scéna
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
čtveřicí	čtveřice	k1gFnSc7	čtveřice
bílých	bílý	k2eAgMnPc2d1	bílý
mercedesů	mercedes	k1gInPc2	mercedes
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
</s>
</p>
<p>
<s>
Písek	Písek	k1gInSc1	Písek
–	–	k?	–
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
scéna	scéna	k1gFnSc1	scéna
celého	celý	k2eAgInSc2d1	celý
filmu	film	k1gInSc2	film
z	z	k7c2	z
letního	letní	k2eAgNnSc2d1	letní
kina	kino	k1gNnSc2	kino
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
představení	představení	k1gNnSc4	představení
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
bouřkou	bouřka	k1gFnSc7	bouřka
</s>
</p>
<p>
<s>
Volenice	Volenice	k1gFnSc1	Volenice
–	–	k?	–
Kalinova	Kalinův	k2eAgFnSc1d1	Kalinova
hájovna	hájovna	k1gFnSc1	hájovna
je	být	k5eAaImIp3nS	být
hájenka	hájenka	k1gFnSc1	hájenka
Liz	liz	k1gInSc4	liz
</s>
</p>
<p>
<s>
Předslavice	Předslavice	k1gFnSc1	Předslavice
–	–	k?	–
budova	budova	k1gFnSc1	budova
MNV	MNV	kA	MNV
Lipovec	Lipovec	k1gInSc1	Lipovec
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
podle	podle	k7c2	podle
záběrů	záběr	k1gInPc2	záběr
Streetview	Streetview	k1gFnPc2	Streetview
velmi	velmi	k6eAd1	velmi
zchátraláHasičské	zchátraláHasičský	k2eAgNnSc4d1	zchátraláHasičský
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
sedí	sedit	k5eAaImIp3nS	sedit
skupina	skupina	k1gFnSc1	skupina
Fešáci	fešák	k1gMnPc1	fešák
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrestaurováno	zrestaurovat	k5eAaPmNgNnS	zrestaurovat
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
Vojenského	vojenský	k2eAgInSc2d1	vojenský
historického	historický	k2eAgInSc2d1	historický
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Lešanech	Lešan	k1gInPc6	Lešan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Soundtrack	soundtrack	k1gInSc4	soundtrack
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
80.	[number]	k4	80.
letech	let	k1gInPc6	let
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
zvykem	zvyk	k1gInSc7	zvyk
vydávat	vydávat	k5eAaImF	vydávat
soundtracky	soundtrack	k1gInPc4	soundtrack
k	k	k7c3	k
filmům	film	k1gInPc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ani	ani	k8xC	ani
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
Trháku	trhák	k1gInSc2	trhák
tehdy	tehdy	k6eAd1	tehdy
nebyly	být	k5eNaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
a	a	k8xC	a
jen	jen	k9	jen
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
albech	album	k1gNnPc6	album
jejich	jejich	k3xOp3gMnPc2	jejich
interpretů	interpret	k1gMnPc2	interpret
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
byly	být	k5eAaImAgInP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
rozhlasu	rozhlas	k1gInSc6	rozhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Originální	originální	k2eAgFnPc1d1	originální
nahrávky	nahrávka	k1gFnPc1	nahrávka
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
a	a	k8xC	a
asi	asi	k9	asi
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
konečně	konečně	k6eAd1	konečně
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
diváci	divák	k1gMnPc1	divák
teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
motivů	motiv	k1gInPc2	motiv
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
filmu	film	k1gInSc2	film
vystříháno	vystříhán	k2eAgNnSc1d1	vystříháno
–	–	k?	–
zda	zda	k8xS	zda
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
zásah	zásah	k1gInSc4	zásah
cenzury	cenzura	k1gFnSc2	cenzura
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
režijní	režijní	k2eAgInSc4d1	režijní
záměr	záměr	k1gInSc4	záměr
<g/>
,	,	kIx,	,
však	však	k9	však
není	být	k5eNaImIp3nS	být
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ROHÁL	ROHÁL	kA	ROHÁL
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
;	;	kIx,	;
CHADIMA	Chadima	k1gMnSc1	Chadima
<g/>
,	,	kIx,	,
Vítek	Vítek	k1gMnSc1	Vítek
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
zpívající	zpívající	k2eAgInPc1d1	zpívající
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
.	.	kIx.	.
<g/>
l	l	kA	l
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Petrklíč	petrklíč	k1gInSc1	petrklíč
<g/>
,	,	kIx,	,
2010.	[number]	k4	2010.
218	[number]	k4	218
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7229-235-6	[number]	k4	978-80-7229-235-6
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Trhák	trhák	k1gInSc1	trhák
<g/>
,	,	kIx,	,
s.	s.	k?	s.
175	[number]	k4	175
až	až	k9	až
194.	[number]	k4	194.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Trhák	trhák	k1gInSc1	trhák
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c4	na
Kinoboxu.cz	Kinoboxu.cz	k1gInSc4	Kinoboxu.cz
</s>
</p>
<p>
<s>
KFilmu	KFilmat	k5eAaPmIp1nS	KFilmat
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
:	:	kIx,	:
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
filmu	film	k1gInSc6	film
</s>
</p>
<p>
<s>
Filmovámísta	Filmovámísta	k1gFnSc1	Filmovámísta
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Lokace	lokace	k1gFnSc1	lokace
míst	místo	k1gNnPc2	místo
natáčení	natáčení	k1gNnSc2	natáčení
včetně	včetně	k7c2	včetně
současných	současný	k2eAgFnPc2d1	současná
fotografií	fotografia	k1gFnPc2	fotografia
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
</s>
</p>
<p>
<s>
IMDB.com	IMDB.com	k1gInSc1	IMDB.com
<g/>
:	:	kIx,	:
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
filmu	film	k1gInSc6	film
v	v	k7c6	v
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
