<s>
Delfínovec	Delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Lipotes	Lipotes	k1gMnSc1
vexillifer	vexillifer	k1gMnSc1
<g/>
,	,	kIx,
čínsky	čínsky	k6eAd1
白	白	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sladkovodní	sladkovodní	k2eAgMnSc1d1
delfín	delfín	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
řece	řeka	k1gFnSc6
Jang-c	Jang-c	k1gFnSc4
<g/>
’	’	k?
<g/>
-ťiang	-ťianga	k1gFnPc2
a	a	k8xC
v	v	k7c6
jezeře	jezero	k1gNnSc6
Tung-tching-chu	Tung-tching-ch	k1gInSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>