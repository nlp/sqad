<s desamb="1">
Červený	červený	k2eAgInSc1d1
seznam	seznam	k1gInSc1
IUCN	IUCN	kA
2007	#num#	k4
jej	on	k3xPp3gMnSc4
klasifikuje	klasifikovat	k5eAaImIp3nS
jako	jako	k8xC,k8xS
kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc4d1
druh	druh	k1gInSc4
a	a	k8xC
uznává	uznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pravděpodobně	pravděpodobně	k6eAd1
vyhynul	vyhynout	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
důsledku	důsledek	k1gInSc6
zhoršení	zhoršení	k1gNnSc2
podmínek	podmínka	k1gFnPc2
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
žil	žít	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>