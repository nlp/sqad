<s>
Delfínovec	Delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
</s>
<s>
Delfínovec	Delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
Ilustrace	ilustrace	k1gFnPc4
delfínovce	delfínovka	k1gFnSc6
čínského	čínský	k2eAgNnSc2d1
Porovnání	porovnání	k1gNnSc2
velikosti	velikost	k1gFnSc2
s	s	k7c7
průměrným	průměrný	k2eAgMnSc7d1
člověkem	člověk	k1gMnSc7
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
kytovci	kytovec	k1gMnPc1
(	(	kIx(
<g/>
Cetacea	cetaceum	k1gNnSc2
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
delfínovcovití	delfínovcovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Platanistidae	Platanistidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
delfínovec	delfínovec	k1gMnSc1
(	(	kIx(
<g/>
Lipotes	Lipotes	k1gMnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Lipotes	Lipotes	k1gMnSc1
vexilliferMiller	vexilliferMiller	k1gMnSc1
<g/>
,	,	kIx,
1918	#num#	k4
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
delfínovce	delfínovec	k1gMnSc2
čínského	čínský	k2eAgMnSc2d1
(	(	kIx(
<g/>
modrá	modrat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
delfínovce	delfínovec	k1gMnSc2
čínského	čínský	k2eAgMnSc2d1
(	(	kIx(
<g/>
modrá	modrat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Delfínovec	Delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Lipotes	Lipotes	k1gMnSc1
vexillifer	vexillifer	k1gMnSc1
<g/>
,	,	kIx,
čínsky	čínsky	k6eAd1
白	白	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
sladkovodní	sladkovodní	k2eAgMnSc1d1
delfín	delfín	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
řece	řeka	k1gFnSc6
Jang-c	Jang-c	k1gFnSc4
<g/>
’	’	k?
<g/>
-ťiang	-ťianga	k1gFnPc2
a	a	k8xC
v	v	k7c6
jezeře	jezero	k1gNnSc6
Tung-tching-chu	Tung-tching-ch	k1gInSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červený	červený	k2eAgInSc1d1
seznam	seznam	k1gInSc1
IUCN	IUCN	kA
2007	#num#	k4
jej	on	k3xPp3gMnSc4
klasifikuje	klasifikovat	k5eAaImIp3nS
jako	jako	k8xC,k8xS
kriticky	kriticky	k6eAd1
ohrožený	ohrožený	k2eAgInSc4d1
druh	druh	k1gInSc4
a	a	k8xC
uznává	uznávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pravděpodobně	pravděpodobně	k6eAd1
vyhynul	vyhynout	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
důsledku	důsledek	k1gInSc6
zhoršení	zhoršení	k1gNnSc2
podmínek	podmínka	k1gFnPc2
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
žil	žít	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
ozubený	ozubený	k2eAgMnSc1d1
kytovec	kytovec	k1gMnSc1
dlouhý	dlouhý	k2eAgMnSc1d1
2	#num#	k4
až	až	k9
2,5	2,5	k4
metrů	metr	k1gInPc2
a	a	k8xC
váží	vážit	k5eAaImIp3nS
průměrně	průměrně	k6eAd1
140	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zbarven	zbarvit	k5eAaPmNgInS
modrošedě	modrošedě	k6eAd1
až	až	k6eAd1
šedě	šedě	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
břiše	břicho	k1gNnSc6
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
světlejší	světlý	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čelisti	čelist	k1gFnPc1
má	mít	k5eAaImIp3nS
protažené	protažený	k2eAgNnSc1d1
<g/>
,	,	kIx,
zobákovité	zobákovitý	k2eAgNnSc1d1
a	a	k8xC
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gInPc6
průměrně	průměrně	k6eAd1
132	#num#	k4
až	až	k9
144	#num#	k4
zubů	zub	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
sice	sice	k8xC
funkční	funkční	k2eAgNnPc1d1
oči	oko	k1gNnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gFnPc1
zrakové	zrakový	k2eAgFnPc1d1
schopností	schopnost	k1gFnSc7
jsou	být	k5eAaImIp3nP
chabé	chabý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
orientaci	orientace	k1gFnSc6
a	a	k8xC
lovu	lov	k1gInSc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
echolokace	echolokace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
málo	málo	k6eAd1
prozkoumaný	prozkoumaný	k2eAgInSc1d1
druh	druh	k1gInSc1
delfínovce	delfínovec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
ve	v	k7c6
skupinách	skupina	k1gFnPc6
tří	tři	k4xCgNnPc2
až	až	k9
deseti	deset	k4xCc2
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
živí	živit	k5eAaImIp3nP
se	se	k3xPyFc4
převážně	převážně	k6eAd1
rybami	ryba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Perspektiva	perspektiva	k1gFnSc1
</s>
<s>
Populace	populace	k1gFnSc1
delfínovců	delfínovec	k1gMnPc2
čínských	čínský	k2eAgMnPc2d1
se	se	k3xPyFc4
drasticky	drasticky	k6eAd1
snížila	snížit	k5eAaPmAgFnS
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
desetiletích	desetiletí	k1gNnPc6
v	v	k7c6
důsledku	důsledek	k1gInSc6
industrializace	industrializace	k1gFnSc2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
hojnému	hojný	k2eAgInSc3d1
lovu	lov	k1gInSc3
<g/>
,	,	kIx,
využívání	využívání	k1gNnSc3
řeky	řeka	k1gFnSc2
k	k	k7c3
přepravě	přeprava	k1gFnSc3
a	a	k8xC
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
také	také	k9
stavby	stavba	k1gFnPc1
vodní	vodní	k2eAgFnSc2d1
elektrárny	elektrárna	k1gFnSc2
Tři	tři	k4xCgFnPc4
soutěsky	soutěska	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
vyvíjena	vyvíjet	k5eAaImNgFnS
snaha	snaha	k1gFnSc1
o	o	k7c4
zachování	zachování	k1gNnSc4
druhu	druh	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
poslední	poslední	k2eAgFnSc6d1
expedici	expedice	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
v	v	k7c6
řece	řeka	k1gFnSc6
najít	najít	k5eAaPmF
žádného	žádný	k3yNgMnSc4
delfínovce	delfínovec	k1gMnSc4
čínského	čínský	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořadatelé	pořadatel	k1gMnPc1
expedice	expedice	k1gFnSc1
jej	on	k3xPp3gMnSc4
prohlásili	prohlásit	k5eAaPmAgMnP
za	za	k7c4
funkčně	funkčně	k6eAd1
vyhynulého	vyhynulý	k2eAgMnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2007	#num#	k4
Zeng	Zeng	k1gMnSc1
Yujiang	Yujiang	k1gMnSc1
údajně	údajně	k6eAd1
nahrál	nahrát	k5eAaPmAgMnS,k5eAaBmAgMnS
velké	velký	k2eAgNnSc4d1
bílé	bílý	k2eAgNnSc4d1
zvíře	zvíře	k1gNnSc4
plovoucí	plovoucí	k2eAgNnSc4d1
v	v	k7c6
řece	řeka	k1gFnSc6
Jang-c	Jang-c	k1gInSc1
<g/>
’	’	k?
<g/>
-ťiang	-ťiang	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ačkoliv	ačkoliv	k8xS
Wang	Wang	k1gMnSc1
Kexiong	Kexiong	k1gMnSc1
z	z	k7c2
Institutu	institut	k1gInSc2
hydrobiologie	hydrobiologie	k1gFnSc2
z	z	k7c2
Čínské	čínský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
předběžně	předběžně	k6eAd1
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
že	že	k8xS
zvíře	zvíře	k1gNnSc1
na	na	k7c6
videu	video	k1gNnSc6
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
delfínovec	delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
<g/>
,	,	kIx,
přítomnost	přítomnost	k1gFnSc1
pouze	pouze	k6eAd1
jednoho	jeden	k4xCgMnSc4
nebo	nebo	k8xC
několika	několik	k4yIc7
málo	málo	k4c1
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
v	v	k7c6
pokročilém	pokročilý	k2eAgInSc6d1
věku	věk	k1gInSc6
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
dostatečná	dostatečný	k2eAgFnSc1d1
pro	pro	k7c4
záchranu	záchrana	k1gFnSc4
funkčně	funkčně	k6eAd1
vyhynulého	vyhynulý	k2eAgInSc2d1
druhu	druh	k1gInSc2
od	od	k7c2
úplného	úplný	k2eAgNnSc2d1
vyhynutí	vyhynutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgNnSc1d1
nesporné	sporný	k2eNgNnSc1d1
spatření	spatření	k1gNnSc1
bylo	být	k5eAaImAgNnS
zaznamenáno	zaznamenat	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
žilo	žít	k5eAaImAgNnS
v	v	k7c6
řece	řeka	k1gFnSc6
asi	asi	k9
400	#num#	k4
delfínovců	delfínovec	k1gMnPc2
čínských	čínský	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pět	pět	k4xCc4
let	léto	k1gNnPc2
poté	poté	k6eAd1
sotva	sotva	k8xS
polovina	polovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
jich	on	k3xPp3gMnPc2
přežívalo	přežívat	k5eAaImAgNnS
pouhých	pouhý	k2eAgInPc2d1
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Baiji	Baije	k1gFnSc4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
SMITH	SMITH	kA
<g/>
,	,	kIx,
B.D.	B.D.	k1gFnSc1
<g/>
;	;	kIx,
ZHOU	ZHOU	kA
<g/>
,	,	kIx,
K.	K.	kA
<g/>
;	;	kIx,
WANG	WANG	kA
<g/>
,	,	kIx,
D.	D.	kA
<g/>
,	,	kIx,
Reeves	Reeves	k1gMnSc1
R.	R.	kA
<g/>
R.	R.	kA
<g/>
,	,	kIx,
Barlow	Barlow	k1gFnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Taylor	Taylor	k1gMnSc1
<g/>
,	,	kIx,
B.L.	B.L.	k1gMnSc1
&	&	k?
Pitman	Pitman	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
Lipotes	Lipotes	k1gMnSc1
vexillifer	vexillifer	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BERKOVEC	BERKOVEC	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
PRUDKÝ	Prudký	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lipotes	Lipotes	k1gMnSc1
vexillifer	vexillifer	k1gMnSc1
(	(	kIx(
<g/>
delfínovec	delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Overview	Overview	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baiji	Baije	k1gFnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
foundation	foundation	k1gInSc1
<g/>
,	,	kIx,
2006-12-13	2006-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rare	Rare	k1gInSc1
Dolphin	Dolphin	k1gMnSc1
Seen	Seen	k1gMnSc1
in	in	k?
China	China	k1gFnSc1
<g/>
,	,	kIx,
Experts	Experts	k1gInSc1
Say	Say	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beijing	Beijing	k1gInSc1
<g/>
:	:	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
2007-08-30	2007-08-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AFP	AFP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
White	Whit	k1gInSc5
Dolphin	Dolphin	k1gMnSc1
Appears	Appearsa	k1gFnPc2
From	From	k1gMnSc1
the	the	k?
Brink	Brink	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Discovery	Discovero	k1gNnPc7
Channel	Channela	k1gFnPc2
<g/>
,	,	kIx,
2007-08-29	2007-08-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
delfínovec	delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
delfínovec	delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Lipotes	Lipotes	k1gInSc1
vexillifer	vexillifer	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Delfínovec	Delfínovec	k1gMnSc1
čínský	čínský	k2eAgMnSc1d1
na	na	k7c4
BioLib	BioLib	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
Mazák	mazák	k1gMnSc1
<g/>
,	,	kIx,
Zvířata	zvíře	k1gNnPc1
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
Státní	státní	k2eAgNnSc1d1
zemědělské	zemědělský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
sv.	sv.	kA
12	#num#	k4
<g/>
,	,	kIx,
1987	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85077313	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85077313	#num#	k4
</s>
