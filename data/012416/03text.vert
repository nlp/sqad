<p>
<s>
Erich	Erich	k1gMnSc1	Erich
Priebke	Priebk	k1gFnSc2	Priebk
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Hennigsdorf	Hennigsdorf	k1gInSc1	Hennigsdorf
<g/>
,	,	kIx,	,
Německé	německý	k2eAgNnSc1d1	německé
císařství	císařství	k1gNnSc1	císařství
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
válečný	válečný	k2eAgMnSc1d1	válečný
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
při	při	k7c6	při
usmrcení	usmrcení	k1gNnSc6	usmrcení
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tří	tři	k4xCgNnPc2	tři
set	sto	k4xCgNnPc2	sto
civilistů	civilista	k1gMnPc2	civilista
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
si	se	k3xPyFc3	se
však	však	k9	však
mohl	moct	k5eAaImAgInS	moct
trest	trest	k1gInSc1	trest
odpykávat	odpykávat	k5eAaImF	odpykávat
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Dopadení	dopadení	k1gNnSc4	dopadení
tohoto	tento	k3xDgMnSc2	tento
bývalého	bývalý	k2eAgMnSc2d1	bývalý
velitele	velitel	k1gMnSc2	velitel
nacistických	nacistický	k2eAgInPc2d1	nacistický
oddílů	oddíl	k1gInPc2	oddíl
SS	SS	kA	SS
trvalo	trvat	k5eAaImAgNnS	trvat
téměř	téměř	k6eAd1	téměř
50	[number]	k4	50
let	léto	k1gNnPc2	léto
–	–	k?	–
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
podařilo	podařit	k5eAaPmAgNnS	podařit
zmizet	zmizet	k5eAaPmF	zmizet
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
hoteliérem	hoteliér	k1gMnSc7	hoteliér
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
oslavil	oslavit	k5eAaPmAgInS	oslavit
100	[number]	k4	100
<g/>
.	.	kIx.	.
narozeniny	narozeniny	k1gFnPc4	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
domem	dům	k1gInSc7	dům
v	v	k7c4	v
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
demonstrovaly	demonstrovat	k5eAaBmAgFnP	demonstrovat
desítky	desítka	k1gFnPc1	desítka
demonstrantů	demonstrant	k1gMnPc2	demonstrant
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
vadilo	vadit	k5eAaImAgNnS	vadit
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
činy	čin	k1gInPc4	čin
nikdy	nikdy	k6eAd1	nikdy
veřejně	veřejně	k6eAd1	veřejně
neomluvil	omluvit	k5eNaPmAgMnS	omluvit
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
mírnost	mírnost	k1gFnSc1	mírnost
jeho	jeho	k3xOp3gNnSc2	jeho
vězení	vězení	k1gNnSc2	vězení
–	–	k?	–
po	po	k7c6	po
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
mohl	moct	k5eAaImAgInS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
zcela	zcela	k6eAd1	zcela
volně	volně	k6eAd1	volně
<g/>
;	;	kIx,	;
při	při	k7c6	při
nákupech	nákup	k1gInPc6	nákup
<g/>
,	,	kIx,	,
procházkách	procházka	k1gFnPc6	procházka
či	či	k8xC	či
v	v	k7c6	v
restauracích	restaurace	k1gFnPc6	restaurace
byl	být	k5eAaImAgInS	být
pouze	pouze	k6eAd1	pouze
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
ochrankou	ochranka	k1gFnSc7	ochranka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Ardeatinských	Ardeatinský	k2eAgFnPc6d1	Ardeatinský
jeskyních	jeskyně	k1gFnPc6	jeskyně
==	==	k?	==
</s>
</p>
<p>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
aktérů	aktér	k1gMnPc2	aktér
brutálního	brutální	k2eAgInSc2d1	brutální
masakru	masakr	k1gInSc2	masakr
civilistů	civilista	k1gMnPc2	civilista
v	v	k7c6	v
Ardeatinských	Ardeatinský	k2eAgFnPc6d1	Ardeatinský
jeskyních	jeskyně	k1gFnPc6	jeskyně
u	u	k7c2	u
Říma	Řím	k1gInSc2	Řím
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
též	též	k9	též
Karl	Karl	k1gMnSc1	Karl
Hass	Hassa	k1gFnPc2	Hassa
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
partyzánský	partyzánský	k2eAgInSc4d1	partyzánský
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
jednotce	jednotka	k1gFnSc3	jednotka
SS	SS	kA	SS
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
335	[number]	k4	335
civilních	civilní	k2eAgNnPc2d1	civilní
rukojmí	rukojmí	k1gNnPc2	rukojmí
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
75	[number]	k4	75
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útoku	útok	k1gInSc6	útok
totiž	totiž	k9	totiž
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
33	[number]	k4	33
německých	německý	k2eAgMnPc2d1	německý
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
podplukovník	podplukovník	k1gMnSc1	podplukovník
SS	SS	kA	SS
Herbert	Herbert	k1gMnSc1	Herbert
Kappler	Kappler	k1gMnSc1	Kappler
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
každého	každý	k3xTgMnSc4	každý
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
Němce	Němec	k1gMnSc4	Němec
zemře	zemřít	k5eAaPmIp3nS	zemřít
deset	deset	k4xCc4	deset
Italů	Ital	k1gMnPc2	Ital
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
onoho	onen	k3xDgInSc2	onen
osudného	osudný	k2eAgInSc2d1	osudný
dne	den	k1gInSc2	den
dva	dva	k4xCgInPc1	dva
muže	muž	k1gMnPc4	muž
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
"	"	kIx"	"
<g/>
odškrtával	odškrtávat	k5eAaImAgMnS	odškrtávat
<g/>
"	"	kIx"	"
další	další	k2eAgMnPc4d1	další
Italy	Ital	k1gMnPc4	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
procesu	proces	k1gInSc6	proces
se	se	k3xPyFc4	se
však	však	k9	však
cítil	cítit	k5eAaImAgMnS	cítit
nevinen	vinen	k2eNgMnSc1d1	nevinen
a	a	k8xC	a
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
plnil	plnit	k5eAaImAgInS	plnit
své	svůj	k3xOyFgFnPc4	svůj
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c6	o
masakru	masakr	k1gInSc6	masakr
v	v	k7c6	v
Ardeatinských	Ardeatinský	k2eAgFnPc6d1	Ardeatinský
jeskyních	jeskyně	k1gFnPc6	jeskyně
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Šarlatový	šarlatový	k2eAgMnSc1d1	šarlatový
a	a	k8xC	a
černý	černý	k2eAgMnSc1d1	černý
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
z	z	k7c2	z
britského	britský	k2eAgInSc2d1	britský
zajateckého	zajatecký	k2eAgInSc2d1	zajatecký
tábora	tábor	k1gInSc2	tábor
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
rakouského	rakouský	k2eAgMnSc2d1	rakouský
biskupa	biskup	k1gMnSc2	biskup
Aloise	Alois	k1gMnSc2	Alois
Hudala	hudat	k5eAaImAgFnS	hudat
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Italové	Ital	k1gMnPc1	Ital
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
začali	začít	k5eAaPmAgMnP	začít
pátrat	pátrat	k5eAaImF	pátrat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
proces	proces	k1gInSc1	proces
s	s	k7c7	s
viníky	viník	k1gMnPc7	viník
masakru	masakr	k1gInSc2	masakr
<g/>
,	,	kIx,	,
Priebke	Priebke	k1gInSc1	Priebke
byl	být	k5eAaImAgInS	být
souzen	soudit	k5eAaImNgInS	soudit
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
argentinském	argentinský	k2eAgNnSc6d1	argentinské
horském	horský	k2eAgNnSc6d1	horské
středisku	středisko	k1gNnSc6	středisko
Bariloche	Bariloch	k1gFnSc2	Bariloch
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
hoteliérem	hoteliér	k1gMnSc7	hoteliér
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
zdejší	zdejší	k2eAgFnSc2d1	zdejší
německé	německý	k2eAgFnSc2d1	německá
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Německo-argentinské	německorgentinský	k2eAgFnSc2d1	německo-argentinský
kulturní	kulturní	k2eAgFnSc2d1	kulturní
asociace	asociace	k1gFnSc2	asociace
<g/>
.	.	kIx.	.
<g/>
Klíčem	klíč	k1gInSc7	klíč
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
dopadení	dopadení	k1gNnSc3	dopadení
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kniha	kniha	k1gFnSc1	kniha
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
argentinského	argentinský	k2eAgInSc2d1	argentinský
původu	původ	k1gInSc2	původ
Estebana	Esteban	k1gMnSc2	Esteban
Bucha	Buch	k1gMnSc2	Buch
Malíř	malíř	k1gMnSc1	malíř
z	z	k7c2	z
argentinského	argentinský	k2eAgNnSc2d1	argentinské
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
dovedla	dovést	k5eAaPmAgFnS	dovést
reportéra	reportér	k1gMnSc4	reportér
americké	americký	k2eAgFnSc2d1	americká
televize	televize	k1gFnSc2	televize
ABC	ABC	kA	ABC
Sama	Sam	k1gMnSc2	Sam
Donaldsona	Donaldson	k1gMnSc2	Donaldson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
přiznal	přiznat	k5eAaPmAgMnS	přiznat
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
masakru	masakr	k1gInSc6	masakr
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
poslouchal	poslouchat	k5eAaImAgMnS	poslouchat
rozkazy	rozkaz	k1gInPc4	rozkaz
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vydán	vydán	k2eAgInSc1d1	vydán
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInSc1	první
soudní	soudní	k2eAgInSc1d1	soudní
proces	proces	k1gInSc1	proces
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
uznán	uznat	k5eAaPmNgInS	uznat
vinným	vinný	k2eAgInSc7d1	vinný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
třicetileté	třicetiletý	k2eAgFnSc3d1	třicetiletá
promlčecí	promlčecí	k2eAgFnSc3d1	promlčecí
lhůtě	lhůta	k1gFnSc3	lhůta
osvobozen	osvobozen	k2eAgInSc1d1	osvobozen
<g/>
.	.	kIx.	.
</s>
<s>
Verdikt	verdikt	k1gInSc4	verdikt
soudu	soud	k1gInSc2	soud
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
vlnu	vlna	k1gFnSc4	vlna
rozhořčení	rozhořčení	k1gNnSc2	rozhořčení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
15	[number]	k4	15
letům	let	k1gInPc3	let
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
jej	on	k3xPp3gInSc4	on
odvolací	odvolací	k2eAgInSc4d1	odvolací
soud	soud	k1gInSc4	soud
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1999	[number]	k4	1999
získal	získat	k5eAaPmAgInS	získat
povolení	povolení	k1gNnSc4	povolení
odpykat	odpykat	k5eAaPmF	odpykat
si	se	k3xPyFc3	se
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
trest	trest	k1gInSc4	trest
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
<g/>
Karl	Karl	k1gMnSc1	Karl
Hass	Hass	k1gInSc4	Hass
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
na	na	k7c6	na
masakru	masakr	k1gInSc6	masakr
u	u	k7c2	u
Říma	Řím	k1gInSc2	Řím
také	také	k9	také
podílel	podílet	k5eAaImAgInS	podílet
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
procesu	proces	k1gInSc6	proces
původně	původně	k6eAd1	původně
vystupovat	vystupovat	k5eAaImF	vystupovat
jako	jako	k9	jako
svědek	svědek	k1gMnSc1	svědek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
byl	být	k5eAaImAgMnS	být
však	však	k9	však
sám	sám	k3xTgMnSc1	sám
obviněn	obviněn	k2eAgMnSc1d1	obviněn
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Priebke	Priebke	k1gFnSc7	Priebke
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
