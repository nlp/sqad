<s>
Smíšené	smíšený	k2eAgNnSc1d1	smíšené
zboží	zboží	k1gNnSc1	zboží
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
rozličný	rozličný	k2eAgInSc1d1	rozličný
tovar	tovar	k1gInSc1	tovar
<g/>
,	,	kIx,	,
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
také	také	k9	také
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
koloniálním	koloniální	k2eAgNnSc7d1	koloniální
zbožím	zboží	k1gNnSc7	zboží
neboli	neboli	k8xC	neboli
koloniál	koloniál	k1gInSc1	koloniál
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
maloobchodní	maloobchodní	k2eAgFnSc2d1	maloobchodní
prodejny	prodejna	k1gFnSc2	prodejna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
spotřebiteli	spotřebitel	k1gMnSc3	spotřebitel
široký	široký	k2eAgInSc4d1	široký
sortiment	sortiment	k1gInSc4	sortiment
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
prodejnu	prodejna	k1gFnSc4	prodejna
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
nápojů	nápoj	k1gInPc2	nápoj
s	s	k7c7	s
doplňkovým	doplňkový	k2eAgInSc7d1	doplňkový
prodejem	prodej	k1gInSc7	prodej
nejpoptávanějšího	poptávaný	k2eAgNnSc2d3	poptávaný
drogistického	drogistický	k2eAgNnSc2d1	drogistické
<g/>
,	,	kIx,	,
papírenského	papírenský	k2eAgNnSc2d1	papírenské
a	a	k8xC	a
kosmetického	kosmetický	k2eAgNnSc2d1	kosmetické
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
obchod	obchod	k1gInSc1	obchod
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
čelí	čelit	k5eAaImIp3nS	čelit
silné	silný	k2eAgFnSc3d1	silná
konkurenci	konkurence	k1gFnSc3	konkurence
nákupních	nákupní	k2eAgInPc2d1	nákupní
řetězců	řetězec	k1gInPc2	řetězec
supermarketů	supermarket	k1gInPc2	supermarket
a	a	k8xC	a
hypermarketů	hypermarket	k1gInPc2	hypermarket
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
podobné	podobný	k2eAgFnPc1d1	podobná
prodejny	prodejna	k1gFnPc1	prodejna
nazývaly	nazývat	k5eAaImAgFnP	nazývat
hokynářství	hokynářství	k1gNnSc4	hokynářství
a	a	k8xC	a
kromě	kromě	k7c2	kromě
potravinářského	potravinářský	k2eAgInSc2d1	potravinářský
a	a	k8xC	a
drogistického	drogistický	k2eAgInSc2d1	drogistický
sortimentu	sortiment	k1gInSc2	sortiment
nabízely	nabízet	k5eAaImAgFnP	nabízet
i	i	k9	i
další	další	k2eAgNnSc4d1	další
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
zboží	zboží	k1gNnSc4	zboží
každodenní	každodenní	k2eAgFnSc2d1	každodenní
spotřeby	spotřeba	k1gFnSc2	spotřeba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nádobí	nádobí	k1gNnSc4	nádobí
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgNnSc4d1	pracovní
nářadí	nářadí	k1gNnSc4	nářadí
apod	apod	kA	apod
.	.	kIx.	.
</s>
<s>
Provozoval	provozovat	k5eAaImAgMnS	provozovat
je	on	k3xPp3gNnSc4	on
hokynář	hokynář	k1gMnSc1	hokynář
(	(	kIx(	(
<g/>
staročesky	staročesky	k6eAd1	staročesky
fragnář	fragnář	k1gInSc1	fragnář
<g/>
)	)	kIx)	)
jakožto	jakožto	k8xS	jakožto
samostatný	samostatný	k2eAgMnSc1d1	samostatný
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
"	"	kIx"	"
<g/>
Smíšené	smíšený	k2eAgNnSc4d1	smíšené
zboží	zboží	k1gNnSc4	zboží
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgNnP	být
znárodněná	znárodněný	k2eAgNnPc1d1	znárodněné
hokynářství	hokynářství	k1gNnPc1	hokynářství
a	a	k8xC	a
koloniály	koloniál	k1gInPc1	koloniál
přejmenována	přejmenován	k2eAgNnPc1d1	přejmenováno
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
socialistického	socialistický	k2eAgNnSc2d1	socialistické
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
sítě	síť	k1gFnPc1	síť
těchto	tento	k3xDgFnPc2	tento
maloobchodních	maloobchodní	k2eAgFnPc2d1	maloobchodní
prodejen	prodejna	k1gFnPc2	prodejna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
provozovala	provozovat	k5eAaImAgNnP	provozovat
především	především	k9	především
spotřební	spotřební	k2eAgNnPc1d1	spotřební
družstva	družstvo	k1gNnPc1	družstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zejména	zejména	k9	zejména
družstva	družstvo	k1gNnPc1	družstvo
Jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
Včela	včela	k1gFnSc1	včela
a	a	k8xC	a
Budoucnost	budoucnost	k1gFnSc1	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Smíšené	smíšený	k2eAgFnSc2d1	smíšená
zboží	zboží	k1gNnSc4	zboží
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hokynář	hokynář	k1gMnSc1	hokynář
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
