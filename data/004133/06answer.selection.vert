<s>
Baronka	baronka	k1gFnSc1	baronka
Bertha	Bertha	k1gFnSc1	Bertha
Sophia	Sophia	k1gFnSc1	Sophia
Felicita	Felicita	k1gFnSc1	Felicita
von	von	k1gInSc4	von
Suttnerová	Suttnerový	k2eAgFnSc1d1	Suttnerový
rozená	rozený	k2eAgFnSc1d1	rozená
hraběnka	hraběnka	k1gFnSc1	hraběnka
Kinská	Kinská	k1gFnSc1	Kinská
ze	z	k7c2	z
Vchynic	Vchynice	k1gFnPc2	Vchynice
a	a	k8xC	a
Tetova	Tetovo	k1gNnSc2	Tetovo
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1843	[number]	k4	1843
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
Vídeň	Vídeň	k1gFnSc1	Vídeň
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česko-rakouská	českoakouský	k2eAgFnSc1d1	česko-rakouská
radikální	radikální	k2eAgFnSc1d1	radikální
pacifistka	pacifistka	k1gFnSc1	pacifistka
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
