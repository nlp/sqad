<s>
Baronka	baronka	k1gFnSc1	baronka
Bertha	Bertha	k1gFnSc1	Bertha
Sophia	Sophia	k1gFnSc1	Sophia
Felicita	Felicita	k1gFnSc1	Felicita
von	von	k1gInSc4	von
Suttnerová	Suttnerový	k2eAgFnSc1d1	Suttnerový
rozená	rozený	k2eAgFnSc1d1	rozená
hraběnka	hraběnka	k1gFnSc1	hraběnka
Kinská	Kinská	k1gFnSc1	Kinská
ze	z	k7c2	z
Vchynic	Vchynice	k1gFnPc2	Vchynice
a	a	k8xC	a
Tetova	Tetovo	k1gNnSc2	Tetovo
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1843	[number]	k4	1843
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
Vídeň	Vídeň	k1gFnSc1	Vídeň
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česko-rakouská	českoakouský	k2eAgFnSc1d1	česko-rakouská
radikální	radikální	k2eAgFnSc1d1	radikální
pacifistka	pacifistka	k1gFnSc1	pacifistka
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Varianty	varianta	k1gFnPc1	varianta
jejího	její	k3xOp3gNnSc2	její
jména	jméno	k1gNnSc2	jméno
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Bert	Berta	k1gFnPc2	Berta
<g/>
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
a	a	k8xC	a
(	(	kIx(	(
<g/>
von	von	k1gInSc1	von
<g/>
)	)	kIx)	)
Suttner	Suttner	k1gInSc1	Suttner
<g/>
(	(	kIx(	(
<g/>
ová	ová	k?	ová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Berta	Berta	k1gFnSc1	Berta
Kinská-Suttnerová	Kinská-Suttnerová	k1gFnSc1	Kinská-Suttnerová
<g/>
,	,	kIx,	,
Berta	Berta	k1gFnSc1	Berta
ze	z	k7c2	z
Suttnerů	Suttner	k1gMnPc2	Suttner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
domě	dům	k1gInSc6	dům
č.	č.	k?	č.
697	[number]	k4	697
(	(	kIx(	(
<g/>
roh	roh	k1gInSc4	roh
Vodičkovy	Vodičkův	k2eAgFnSc2d1	Vodičkova
ulice	ulice	k1gFnSc2	ulice
a	a	k8xC	a
ulice	ulice	k1gFnSc2	ulice
V	v	k7c6	v
Jámě	jáma	k1gFnSc6	jáma
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
pražském	pražský	k2eAgNnSc6d1	Pražské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
Kinský	Kinský	k1gMnSc1	Kinský
(	(	kIx(	(
<g/>
1768	[number]	k4	1768
<g/>
-	-	kIx~	-
<g/>
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
polní	polní	k2eAgMnSc1d1	polní
podmaršál	podmaršál	k1gMnSc1	podmaršál
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
rodin	rodina	k1gFnPc2	rodina
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Sofie	Sofie	k1gFnSc1	Sofie
(	(	kIx(	(
<g/>
Žofie	Žofie	k1gFnSc1	Žofie
<g/>
)	)	kIx)	)
Vilemína	Vilemína	k1gFnSc1	Vilemína
hraběnka	hraběnka	k1gFnSc1	hraběnka
Kinská-Körnerová	Kinská-Körnerová	k1gFnSc1	Kinská-Körnerová
(	(	kIx(	(
<g/>
dcera	dcera	k1gFnSc1	dcera
hejtmana	hejtman	k1gMnSc2	hejtman
jízdy	jízda	k1gFnSc2	jízda
Josefa	Josef	k1gMnSc2	Josef
von	von	k1gInSc4	von
Körnera	Körner	k1gMnSc4	Körner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Berta	Berta	k1gFnSc1	Berta
žila	žít	k5eAaImAgFnS	žít
část	část	k1gFnSc4	část
dětství	dětství	k1gNnSc2	dětství
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
i	i	k8xC	i
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Kinských	Kinská	k1gFnPc2	Kinská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
nerovnost	nerovnost	k1gFnSc4	nerovnost
původu	původ	k1gInSc2	původ
Bertiných	Bertin	k2eAgMnPc2d1	Bertin
rodičů	rodič	k1gMnPc2	rodič
i	i	k8xC	i
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
otec	otec	k1gMnSc1	otec
již	již	k6eAd1	již
nežil	žít	k5eNaImAgMnS	žít
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
její	její	k3xOp3gFnSc7	její
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
širší	široký	k2eAgFnSc7d2	širší
rodinou	rodina	k1gFnSc7	rodina
Kinských	Kinských	k2eAgFnSc2d1	Kinských
napjaté	napjatý	k2eAgFnSc2d1	napjatá
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
proto	proto	k8xC	proto
odešly	odejít	k5eAaPmAgFnP	odejít
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
u	u	k7c2	u
Bertina	Bertin	k2eAgMnSc2d1	Bertin
poručníka	poručník	k1gMnSc2	poručník
<g/>
,	,	kIx,	,
otcova	otcův	k2eAgMnSc2d1	otcův
přítele	přítel	k1gMnSc2	přítel
Ernsta	Ernst	k1gMnSc2	Ernst
Egona	Egon	k1gMnSc2	Egon
Fürstenberga	Fürstenberg	k1gMnSc2	Fürstenberg
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dostalo	dostat	k5eAaPmAgNnS	dostat
péče	péče	k1gFnSc2	péče
i	i	k8xC	i
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
;	;	kIx,	;
také	také	k9	také
bohatá	bohatý	k2eAgFnSc1d1	bohatá
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
probudila	probudit	k5eAaPmAgFnS	probudit
zájmy	zájem	k1gInPc4	zájem
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
jí	jíst	k5eAaImIp3nS	jíst
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
jazykovým	jazykový	k2eAgNnSc7d1	jazykové
nadáním	nadání	k1gNnSc7	nadání
uvedly	uvést	k5eAaPmAgFnP	uvést
do	do	k7c2	do
světa	svět	k1gInSc2	svět
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
i	i	k8xC	i
humanistických	humanistický	k2eAgFnPc2d1	humanistická
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Naučila	naučit	k5eAaPmAgFnS	naučit
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
rodné	rodný	k2eAgFnSc2d1	rodná
němčiny	němčina	k1gFnSc2	němčina
a	a	k8xC	a
češtiny	čeština	k1gFnSc2	čeština
i	i	k9	i
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
italsky	italsky	k6eAd1	italsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
se	se	k3xPyFc4	se
odstěhovaly	odstěhovat	k5eAaPmAgFnP	odstěhovat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
k	k	k7c3	k
tetě	teta	k1gFnSc3	teta
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
sestřenice	sestřenice	k1gFnSc1	sestřenice
Elvíra	Elvíra	k1gFnSc1	Elvíra
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
Bertu	Berta	k1gFnSc4	Berta
kladný	kladný	k2eAgInSc4d1	kladný
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
zasvětila	zasvětit	k5eAaPmAgFnS	zasvětit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rodinné	rodinný	k2eAgNnSc1d1	rodinné
jmění	jmění	k1gNnSc1	jmění
(	(	kIx(	(
<g/>
zděděné	zděděný	k2eAgFnSc2d1	zděděná
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
téměř	téměř	k6eAd1	téměř
vyčerpáno	vyčerpat	k5eAaPmNgNnS	vyčerpat
<g/>
,	,	kIx,	,
přijala	přijmout	k5eAaPmAgFnS	přijmout
Berta	Berta	k1gFnSc1	Berta
místo	místo	k7c2	místo
guvernantky	guvernantka	k1gFnSc2	guvernantka
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
velkoprůmyslníka	velkoprůmyslník	k1gMnSc2	velkoprůmyslník
Karla	Karel	k1gMnSc2	Karel
von	von	k1gInSc4	von
Suttnera	Suttner	k1gMnSc2	Suttner
<g/>
.	.	kIx.	.
</s>
<s>
Vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
jeho	jeho	k3xOp3gFnPc4	jeho
čtyři	čtyři	k4xCgFnPc4	čtyři
dcery	dcera	k1gFnPc4	dcera
hudbě	hudba	k1gFnSc3	hudba
a	a	k8xC	a
jazykům	jazyk	k1gMnPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Sblížila	sblížit	k5eAaPmAgFnS	sblížit
se	se	k3xPyFc4	se
s	s	k7c7	s
Arturem	Artur	k1gMnSc7	Artur
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
synem	syn	k1gMnSc7	syn
Suttnerů	Suttner	k1gMnPc2	Suttner
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
rodiny	rodina	k1gFnSc2	rodina
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1876	[number]	k4	1876
Berta	Berta	k1gFnSc1	Berta
za	za	k7c4	za
Artura	Artur	k1gMnSc4	Artur
tajně	tajně	k6eAd1	tajně
provdala	provdat	k5eAaPmAgFnS	provdat
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Gruzie	Gruzie	k1gFnSc2	Gruzie
<g/>
,	,	kIx,	,
na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Bertiny	Bertin	k2eAgFnSc2d1	Bertina
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
<g/>
,	,	kIx,	,
kněžny	kněžna	k1gFnSc2	kněžna
Jekateriny	Jekaterin	k1gInPc1	Jekaterin
Dadiani	Dadiaň	k1gFnSc6	Dadiaň
z	z	k7c2	z
Mingrelie	Mingrelie	k1gFnSc2	Mingrelie
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
získávali	získávat	k5eAaImAgMnP	získávat
především	především	k9	především
vyučováním	vyučování	k1gNnSc7	vyučování
v	v	k7c6	v
tamních	tamní	k2eAgFnPc6d1	tamní
šlechtických	šlechtický	k2eAgFnPc6d1	šlechtická
rodinách	rodina	k1gFnPc6	rodina
a	a	k8xC	a
psaním	psaní	k1gNnSc7	psaní
článků	článek	k1gInPc2	článek
pro	pro	k7c4	pro
noviny	novina	k1gFnPc4	novina
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
též	též	k9	též
pracovali	pracovat	k5eAaImAgMnP	pracovat
ve	v	k7c6	v
vojenských	vojenský	k2eAgInPc6d1	vojenský
lazaretech	lazaret	k1gInPc6	lazaret
při	při	k7c6	při
rusko-turecké	ruskourecký	k2eAgFnSc6d1	rusko-turecká
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
Berta	Berta	k1gFnSc1	Berta
zde	zde	k6eAd1	zde
poznala	poznat	k5eAaPmAgFnS	poznat
velké	velká	k1gFnPc4	velká
a	a	k8xC	a
zbytečné	zbytečný	k2eAgNnSc4d1	zbytečné
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
byla	být	k5eAaImAgFnS	být
začátkem	začátek	k1gInSc7	začátek
její	její	k3xOp3gFnSc2	její
nové	nový	k2eAgFnSc2d1	nová
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
;	;	kIx,	;
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
utrpení	utrpení	k1gNnSc1	utrpení
<g/>
,	,	kIx,	,
přinášené	přinášený	k2eAgNnSc1d1	přinášené
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
zlem	zlo	k1gNnSc7	zlo
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterému	který	k3yIgInSc3	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
usmíření	usmíření	k1gNnSc3	usmíření
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
rodinného	rodinný	k2eAgInSc2d1	rodinný
zámku	zámek	k1gInSc2	zámek
Harmannsdorf	Harmannsdorf	k1gMnSc1	Harmannsdorf
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousích	Rakousy	k1gInPc6	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
odcestovali	odcestovat	k5eAaPmAgMnP	odcestovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
jako	jako	k9	jako
uznávaní	uznávaný	k2eAgMnPc1d1	uznávaný
publicisté	publicista	k1gMnPc1	publicista
vítáni	vítán	k2eAgMnPc1d1	vítán
mezi	mezi	k7c7	mezi
literáty	literát	k1gMnPc7	literát
a	a	k8xC	a
intelektuály	intelektuál	k1gMnPc7	intelektuál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
Berta	Berta	k1gFnSc1	Berta
von	von	k1gInSc4	von
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
vydala	vydat	k5eAaPmAgFnS	vydat
román	román	k1gInSc4	román
Die	Die	k1gFnSc2	Die
Waffen	Waffen	k2eAgInSc4d1	Waffen
nieder	nieder	k1gInSc4	nieder
<g/>
!	!	kIx.	!
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
Odzbrojte	odzbrojit	k5eAaPmRp2nP	odzbrojit
<g/>
!	!	kIx.	!
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
vydáno	vydat	k5eAaPmNgNnS	vydat
jako	jako	k8xS	jako
Lay	Lay	k1gMnSc1	Lay
Down	Down	k1gMnSc1	Down
Your	Your	k1gMnSc1	Your
Arms	Armsa	k1gFnPc2	Armsa
<g/>
,	,	kIx,	,
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
znala	znát	k5eAaImAgFnS	znát
válečné	válečný	k2eAgFnPc4d1	válečná
dějiny	dějiny	k1gFnPc4	dějiny
i	i	k8xC	i
historii	historie	k1gFnSc4	historie
Královéhradecka	Královéhradecko	k1gNnSc2	Královéhradecko
a	a	k8xC	a
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
mj.	mj.	kA	mj.
popisuje	popisovat	k5eAaImIp3nS	popisovat
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
v	v	k7c6	v
r.	r.	kA	r.
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
velký	velký	k2eAgInSc4d1	velký
rozruch	rozruch	k1gInSc4	rozruch
a	a	k8xC	a
postavil	postavit	k5eAaPmAgMnS	postavit
autorku	autorka	k1gFnSc4	autorka
mezi	mezi	k7c7	mezi
přední	přední	k2eAgFnSc7d1	přední
představitele	představitel	k1gMnSc2	představitel
mírového	mírový	k2eAgNnSc2d1	Mírové
hnutí	hnutí	k1gNnSc2	hnutí
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
angažovat	angažovat	k5eAaBmF	angažovat
v	v	k7c6	v
protiválečných	protiválečný	k2eAgFnPc6d1	protiválečná
aktivitách	aktivita	k1gFnPc6	aktivita
<g/>
;	;	kIx,	;
brzy	brzy	k6eAd1	brzy
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
výborů	výbor	k1gInPc2	výbor
mírových	mírový	k2eAgFnPc2d1	mírová
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
setkání	setkání	k1gNnSc6	setkání
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jezdila	jezdit	k5eAaImAgFnS	jezdit
na	na	k7c4	na
první	první	k4xOgInPc4	první
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
kongresy	kongres	k1gInPc4	kongres
<g/>
,	,	kIx,	,
propagovala	propagovat	k5eAaImAgFnS	propagovat
založení	založení	k1gNnSc3	založení
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
smírčího	smírčí	k2eAgInSc2d1	smírčí
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
a	a	k8xC	a
také	také	k9	také
další	další	k2eAgFnPc1d1	další
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
obhajující	obhajující	k2eAgNnPc1d1	obhajující
práva	právo	k1gNnPc1	právo
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
svůj	svůj	k3xOyFgInSc4	svůj
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
antisemitismu	antisemitismus	k1gInSc3	antisemitismus
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
vydávat	vydávat	k5eAaPmF	vydávat
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
pacifistický	pacifistický	k2eAgInSc1d1	pacifistický
časopis	časopis	k1gInSc1	časopis
Odzbrojte	odzbrojit	k5eAaPmRp2nP	odzbrojit
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc4	Die
Waffen	Waffna	k1gFnPc2	Waffna
nieder	niedero	k1gNnPc2	niedero
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
manžela	manžel	k1gMnSc2	manžel
Artura	Artur	k1gMnSc2	Artur
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
věnovala	věnovat	k5eAaImAgFnS	věnovat
své	svůj	k3xOyFgNnSc4	svůj
veškeré	veškerý	k3xTgNnSc4	veškerý
úsilí	úsilí	k1gNnSc4	úsilí
mírovým	mírový	k2eAgFnPc3d1	mírová
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Berty	Berta	k1gFnSc2	Berta
Suttnerové	Suttnerová	k1gFnSc2	Suttnerová
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgFnP	spojovat
zejména	zejména	k9	zejména
tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
<g/>
:	:	kIx,	:
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
italských	italský	k2eAgFnPc6d1	italská
Benátkách	Benátky	k1gFnPc6	Benátky
(	(	kIx(	(
<g/>
začátkem	začátkem	k7c2	začátkem
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
dala	dát	k5eAaPmAgFnS	dát
popud	popud	k1gInSc4	popud
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
Mírové	mírový	k2eAgFnSc2d1	mírová
společnosti	společnost	k1gFnSc2	společnost
Benátky	Benátky	k1gFnPc1	Benátky
(	(	kIx(	(
<g/>
Friedengesellschaft	Friedengesellschaft	k2eAgInSc1d1	Friedengesellschaft
Venedig	Venedig	k1gInSc1	Venedig
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uveřejnila	uveřejnit	k5eAaPmAgFnS	uveřejnit
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
pacifistické	pacifistický	k2eAgFnSc2d1	pacifistická
organizace	organizace	k1gFnSc2	organizace
<g/>
;	;	kIx,	;
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
staré	starý	k2eAgFnSc6d1	stará
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
radnici	radnice	k1gFnSc6	radnice
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1891	[number]	k4	1891
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Osterreichische	Osterreichische	k1gFnSc1	Osterreichische
Friedensgesselschaft	Friedensgesselschaft	k1gMnSc1	Friedensgesselschaft
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
mírová	mírový	k2eAgFnSc1d1	mírová
společnost	společnost	k1gFnSc1	společnost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
mírovém	mírový	k2eAgInSc6d1	mírový
kongresu	kongres	k1gInSc6	kongres
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgNnP	být
zvolena	zvolit	k5eAaPmNgNnP	zvolit
viceprezidentskou	viceprezidentský	k2eAgFnSc4d1	viceprezidentská
Mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
mírového	mírový	k2eAgNnSc2d1	Mírové
byra	byro	k1gNnSc2	byro
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
na	na	k7c4	na
Kapitolu	kapitola	k1gFnSc4	kapitola
a	a	k8xC	a
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
historickém	historický	k2eAgNnSc6d1	historické
místě	místo	k1gNnSc6	místo
pronesla	pronést	k5eAaPmAgFnS	pronést
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
úspěch	úspěch	k1gInSc4	úspěch
Berty	Berta	k1gFnSc2	Berta
a	a	k8xC	a
jejích	její	k3xOp3gMnPc2	její
přátel	přítel	k1gMnPc2	přítel
lze	lze	k6eAd1	lze
pokládat	pokládat	k5eAaImF	pokládat
i	i	k9	i
zřízení	zřízení	k1gNnSc1	zřízení
Stálého	stálý	k2eAgMnSc2d1	stálý
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
soudu	soud	k1gInSc2	soud
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c4	v
Den	den	k1gInSc4	den
Haagu	Haag	k1gInSc2	Haag
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1891	[number]	k4	1891
oznámila	oznámit	k5eAaPmAgFnS	oznámit
založení	založení	k1gNnSc4	založení
"	"	kIx"	"
<g/>
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
společnosti	společnost	k1gFnSc2	společnost
přátel	přítel	k1gMnPc2	přítel
míru	mír	k1gInSc2	mír
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Österreichische	Österreichische	k1gNnSc1	Österreichische
Gesellschaft	Gesellschafta	k1gFnPc2	Gesellschafta
der	drát	k5eAaImRp2nS	drát
Friedensfreunde	Friedensfreund	k1gMnSc5	Friedensfreund
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
schůzi	schůze	k1gFnSc6	schůze
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Bertu	Berta	k1gFnSc4	Berta
von	von	k1gInSc1	von
Suttner	Suttner	k1gMnSc1	Suttner
prezidentkou	prezidentka	k1gFnSc7	prezidentka
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
tou	ten	k3xDgFnSc7	ten
byla	být	k5eAaImAgFnS	být
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předala	předat	k5eAaPmAgFnS	předat
císaři	císař	k1gMnSc3	císař
Františku	František	k1gMnSc3	František
Josefovi	Josef	k1gMnSc3	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
podpisový	podpisový	k2eAgInSc1d1	podpisový
arch	arch	k1gInSc1	arch
s	s	k7c7	s
obhajovací	obhajovací	k2eAgFnSc7d1	obhajovací
řečí	řeč	k1gFnSc7	řeč
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
arbitrážní	arbitrážní	k2eAgInSc4d1	arbitrážní
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
příprav	příprava	k1gFnPc2	příprava
První	první	k4xOgFnSc2	první
Mírové	mírový	k2eAgFnSc2d1	mírová
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
(	(	kIx(	(
<g/>
k	k	k7c3	k
otázkám	otázka	k1gFnPc3	otázka
národní	národní	k2eAgFnSc2d1	národní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
odzbrojení	odzbrojení	k1gNnSc2	odzbrojení
a	a	k8xC	a
zřízení	zřízení	k1gNnSc2	zřízení
trvalého	trvalý	k2eAgInSc2d1	trvalý
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
Světového	světový	k2eAgInSc2d1	světový
mírového	mírový	k2eAgInSc2d1	mírový
kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
rok	rok	k1gInSc4	rok
cestovala	cestovat	k5eAaImAgFnS	cestovat
opět	opět	k6eAd1	opět
do	do	k7c2	do
Monaka	Monako	k1gNnSc2	Monako
na	na	k7c4	na
otevření	otevření	k1gNnSc4	otevření
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
institutu	institut	k1gInSc2	institut
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
Institut	institut	k1gInSc1	institut
International	International	k1gMnPc2	International
de	de	k?	de
la	la	k1gNnPc2	la
Paix	Paix	k1gInSc1	Paix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
ženská	ženský	k2eAgFnSc1d1	ženská
konference	konference	k1gFnSc1	konference
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Berta	Berta	k1gFnSc1	Berta
von	von	k1gInSc1	von
Suttner	Suttner	k1gInSc1	Suttner
na	na	k7c6	na
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
mírové	mírový	k2eAgFnSc6d1	mírová
demonstraci	demonstrace	k1gFnSc6	demonstrace
ve	v	k7c6	v
filharmonii	filharmonie	k1gFnSc6	filharmonie
měla	mít	k5eAaImAgFnS	mít
přednášku	přednáška	k1gFnSc4	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
USA	USA	kA	USA
na	na	k7c4	na
Světový	světový	k2eAgInSc4d1	světový
mírový	mírový	k2eAgInSc4d1	mírový
kongres	kongres	k1gInSc4	kongres
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
cestovala	cestovat	k5eAaImAgFnS	cestovat
po	po	k7c6	po
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
mívala	mívat	k5eAaImAgFnS	mívat
až	až	k9	až
tři	tři	k4xCgFnPc4	tři
přednášky	přednáška	k1gFnPc4	přednáška
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
DC	DC	kA	DC
byla	být	k5eAaImAgFnS	být
pozvána	pozvat	k5eAaPmNgFnS	pozvat
do	do	k7c2	do
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
k	k	k7c3	k
rozhovoru	rozhovor	k1gInSc3	rozhovor
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Theodorem	Theodor	k1gMnSc7	Theodor
Rooseveltem	Roosevelt	k1gMnSc7	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
a	a	k8xC	a
přednášela	přednášet	k5eAaImAgFnS	přednášet
tam	tam	k6eAd1	tam
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesáti	padesát	k4xCc6	padesát
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1904	[number]	k4	1904
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
domě	dům	k1gInSc6	dům
Casino	Casino	k1gNnSc4	Casino
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
přednáška	přednáška	k1gFnSc1	přednáška
Suttnerové	Suttnerová	k1gFnSc2	Suttnerová
jakožto	jakožto	k8xS	jakožto
prezidentky	prezidentka	k1gFnSc2	prezidentka
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
mírové	mírový	k2eAgFnSc2d1	mírová
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Noviny	novina	k1gFnPc1	novina
psaly	psát	k5eAaImAgFnP	psát
o	o	k7c4	o
mimořádně	mimořádně	k6eAd1	mimořádně
navštívené	navštívený	k2eAgFnSc6d1	navštívená
přednášce	přednáška	k1gFnSc6	přednáška
o	o	k7c6	o
pacifistickém	pacifistický	k2eAgNnSc6d1	pacifistické
hnutí	hnutí	k1gNnSc6	hnutí
a	a	k8xC	a
o	o	k7c6	o
boji	boj	k1gInSc6	boj
s	s	k7c7	s
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Udržovala	udržovat	k5eAaImAgFnS	udržovat
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodnou	rodný	k2eAgFnSc7d1	rodná
zemí	zem	k1gFnSc7	zem
<g/>
;	;	kIx,	;
za	za	k7c4	za
pacifistu	pacifista	k1gMnSc4	pacifista
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
stoupence	stoupenec	k1gMnSc2	stoupenec
považovala	považovat	k5eAaImAgFnS	považovat
také	také	k9	také
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Setkávala	setkávat	k5eAaImAgFnS	setkávat
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
básníky	básník	k1gMnPc7	básník
Svatoplukem	Svatopluk	k1gMnSc7	Svatopluk
Čechem	Čech	k1gMnSc7	Čech
a	a	k8xC	a
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Vrchlickým	Vrchlický	k1gMnSc7	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1895	[number]	k4	1895
byla	být	k5eAaImAgFnS	být
Berta	Berta	k1gFnSc1	Berta
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
ustavení	ustavení	k1gNnSc6	ustavení
české	český	k2eAgFnSc2d1	Česká
sekce	sekce	k1gFnSc2	sekce
Mírové	mírový	k2eAgFnSc2d1	mírová
společnosti	společnost	k1gFnSc2	společnost
mělo	mít	k5eAaImAgNnS	mít
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc4d1	německý
kruhy	kruh	k1gInPc4	kruh
chtěla	chtít	k5eAaImAgFnS	chtít
získat	získat	k5eAaPmF	získat
vystoupením	vystoupení	k1gNnSc7	vystoupení
v	v	k7c6	v
Tiskovém	tiskový	k2eAgInSc6d1	tiskový
klubu	klub	k1gInSc6	klub
Concordia	Concordium	k1gNnSc2	Concordium
(	(	kIx(	(
<g/>
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
domě	dům	k1gInSc6	dům
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Na	na	k7c6	na
příkopech	příkop	k1gInPc6	příkop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pacifismus	pacifismus	k1gInSc1	pacifismus
měl	mít	k5eAaImAgInS	mít
překlenout	překlenout	k5eAaPmF	překlenout
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
Čechy	Čech	k1gMnPc7	Čech
a	a	k8xC	a
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
snaha	snaha	k1gFnSc1	snaha
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
neúspěchem	neúspěch	k1gInSc7	neúspěch
a	a	k8xC	a
nepochopením	nepochopení	k1gNnSc7	nepochopení
německé	německý	k2eAgFnSc2d1	německá
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
aktivní	aktivní	k2eAgFnSc1d1	aktivní
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
účastnila	účastnit	k5eAaImAgFnS	účastnit
druhé	druhý	k4xOgFnPc4	druhý
mírové	mírový	k2eAgFnPc4d1	mírová
konference	konference	k1gFnPc4	konference
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
roku	rok	k1gInSc3	rok
1899	[number]	k4	1899
mnohem	mnohem	k6eAd1	mnohem
ostřeji	ostro	k6eAd2	ostro
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
pravidlech	pravidlo	k1gNnPc6	pravidlo
válečného	válečný	k2eAgNnSc2d1	válečné
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
o	o	k7c6	o
otázkách	otázka	k1gFnPc6	otázka
stabilního	stabilní	k2eAgInSc2d1	stabilní
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Berta	Berta	k1gFnSc1	Berta
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
<g/>
,	,	kIx,	,
rodačka	rodačka	k1gFnSc1	rodačka
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
nejproslulejší	proslulý	k2eAgFnSc4d3	nejproslulejší
ženu	žena	k1gFnSc4	žena
světa	svět	k1gInSc2	svět
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
literární	literární	k2eAgFnSc7d1	literární
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
činností	činnost	k1gFnSc7	činnost
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
míru	mír	k1gInSc2	mír
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
obránců	obránce	k1gMnPc2	obránce
humanistických	humanistický	k2eAgFnPc2d1	humanistická
idejí	idea	k1gFnPc2	idea
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
těžké	těžký	k2eAgFnSc3d1	těžká
nemoci	nemoc	k1gFnSc3	nemoc
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
71	[number]	k4	71
letech	let	k1gInPc6	let
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
psali	psát	k5eAaImAgMnP	psát
články	článek	k1gInPc4	článek
pro	pro	k7c4	pro
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
časopisy	časopis	k1gInPc4	časopis
a	a	k8xC	a
také	také	k9	také
literárně	literárně	k6eAd1	literárně
náročné	náročný	k2eAgFnPc4d1	náročná
stati	stať	k1gFnPc4	stať
pro	pro	k7c4	pro
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
a	a	k8xC	a
berlínský	berlínský	k2eAgInSc4d1	berlínský
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
seznamovali	seznamovat	k5eAaImAgMnP	seznamovat
veřejnost	veřejnost	k1gFnSc4	veřejnost
s	s	k7c7	s
exotickým	exotický	k2eAgNnSc7d1	exotické
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
psali	psát	k5eAaImAgMnP	psát
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
Rusko-Turecké	ruskourecký	k2eAgFnSc6d1	rusko-turecká
válce	válka	k1gFnSc6	válka
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
psala	psát	k5eAaImAgFnS	psát
také	také	k9	také
pod	pod	k7c4	pod
pseudonymy	pseudonym	k1gInPc4	pseudonym
B.	B.	kA	B.
Oulet	Oulet	k1gInSc4	Oulet
nebo	nebo	k8xC	nebo
Jemand	Jemand	k1gInSc4	Jemand
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Berty	Berta	k1gFnSc2	Berta
Suttnerové	Suttnerové	k2eAgInSc2d1	Suttnerové
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
v	v	k7c6	v
literárních	literární	k2eAgInPc6d1	literární
kruzích	kruh	k1gInPc6	kruh
Evropy	Evropa	k1gFnSc2	Evropa
pojmem	pojem	k1gInSc7	pojem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
napsala	napsat	k5eAaPmAgFnS	napsat
také	také	k9	také
několik	několik	k4yIc4	několik
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
publikovány	publikovat	k5eAaBmNgFnP	publikovat
a	a	k8xC	a
našly	najít	k5eAaPmAgFnP	najít
brzy	brzy	k6eAd1	brzy
své	svůj	k3xOyFgMnPc4	svůj
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
Arturem	Artur	k1gMnSc7	Artur
Suttnerem	Suttner	k1gMnSc7	Suttner
přeložili	přeložit	k5eAaPmAgMnP	přeložit
legendární	legendární	k2eAgInSc4d1	legendární
epos	epos	k1gInSc4	epos
od	od	k7c2	od
Šoty	šot	k1gInPc4	šot
Rustaveliho	Rustaveli	k1gMnSc2	Rustaveli
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
v	v	k7c6	v
tygří	tygří	k2eAgFnSc6d1	tygří
kůži	kůže	k1gFnSc6	kůže
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
památku	památka	k1gFnSc4	památka
staré	starý	k2eAgFnSc2d1	stará
gruzínské	gruzínský	k2eAgFnSc2d1	gruzínská
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
se	se	k3xPyFc4	se
Berta	Berta	k1gFnSc1	Berta
v	v	k7c6	v
psaní	psaní	k1gNnSc6	psaní
soustředila	soustředit	k5eAaPmAgFnS	soustředit
na	na	k7c4	na
myšlenku	myšlenka	k1gFnSc4	myšlenka
mírového	mírový	k2eAgInSc2d1	mírový
světa	svět	k1gInSc2	svět
bez	bez	k7c2	bez
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
upsala	upsat	k5eAaPmAgFnS	upsat
se	se	k3xPyFc4	se
pacifismu	pacifismus	k1gInSc3	pacifismus
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
High	High	k1gInSc1	High
Life	Lif	k1gInSc2	Lif
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
hlavní	hlavní	k2eAgNnSc4d1	hlavní
téma	téma	k1gNnSc4	téma
respektování	respektování	k1gNnSc2	respektování
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
svobodné	svobodný	k2eAgFnSc2d1	svobodná
vůle	vůle	k1gFnSc2	vůle
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c4	o
sobě	se	k3xPyFc3	se
samém	samý	k3xTgMnSc6	samý
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
chtěla	chtít	k5eAaImAgFnS	chtít
napsat	napsat	k5eAaBmF	napsat
knihu	kniha	k1gFnSc4	kniha
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
a	a	k8xC	a
cítí-	cítí-	k?	cítí-
bolest	bolest	k1gFnSc1	bolest
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
představa	představa	k1gFnSc1	představa
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
psala	psát	k5eAaImAgFnS	psát
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
a	a	k8xC	a
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
a	a	k8xC	a
vydala	vydat	k5eAaPmAgFnS	vydat
knihu	kniha	k1gFnSc4	kniha
Die	Die	k1gFnSc2	Die
Waffen	Waffen	k2eAgInSc1d1	Waffen
nieder	nieder	k1gInSc1	nieder
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
vydávána	vydáván	k2eAgNnPc1d1	vydáváno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Odzbrojte	odzbrojit	k5eAaPmRp2nP	odzbrojit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Vlasty	Vlasta	k1gFnSc2	Vlasta
Pittnerové	Pittnerová	k1gFnSc2	Pittnerová
s	s	k7c7	s
věnováním	věnování	k1gNnSc7	věnování
památce	památka	k1gFnSc3	památka
Vojty	Vojta	k1gMnSc2	Vojta
Náprstka	Náprstka	k1gFnSc1	Náprstka
<g/>
,	,	kIx,	,
iniciátorovi	iniciátorův	k2eAgMnPc1d1	iniciátorův
překladu	překlad	k1gInSc6	překlad
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Román	Román	k1gMnSc1	Román
měl	mít	k5eAaImAgMnS	mít
nebývalý	bývalý	k2eNgInSc4d1	bývalý
ohlas	ohlas	k1gInSc4	ohlas
<g/>
,	,	kIx,	,
v	v	k7c6	v
kritikách	kritika	k1gFnPc6	kritika
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
i	i	k9	i
srovnávání	srovnávání	k1gNnSc1	srovnávání
jeho	jeho	k3xOp3gInSc2	jeho
významu	význam	k1gInSc2	význam
s	s	k7c7	s
Biblí	bible	k1gFnSc7	bible
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
vešla	vejít	k5eAaPmAgFnS	vejít
i	i	k8xC	i
slova	slovo	k1gNnPc1	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Třebaže	třebaže	k8xS	třebaže
kniha	kniha	k1gFnSc1	kniha
není	být	k5eNaImIp3nS	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
schopna	schopen	k2eAgFnSc1d1	schopna
změnit	změnit	k5eAaPmF	změnit
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
Suttnerové	Suttnerová	k1gFnSc2	Suttnerová
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
S	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
její	její	k3xOp3gNnSc4	její
protiválečné	protiválečný	k2eAgNnSc4d1	protiválečné
dílo	dílo	k1gNnSc4	dílo
přivítal	přivítat	k5eAaPmAgMnS	přivítat
i	i	k9	i
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
<g/>
,	,	kIx,	,
Suttnerové	Suttnerové	k2eAgMnSc1d1	Suttnerové
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
dubna	duben	k1gInSc2	duben
1890	[number]	k4	1890
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Milá	milý	k2eAgFnSc5d1	Milá
baronko	baronka	k1gFnSc5	baronka
<g/>
,	,	kIx,	,
přítelkyně	přítelkyně	k1gFnSc5	přítelkyně
<g/>
!	!	kIx.	!
</s>
<s>
Dočetl	dočíst	k5eAaPmAgMnS	dočíst
jsem	být	k5eAaImIp1nS	být
právě	právě	k9	právě
Vaše	váš	k3xOp2gNnSc1	váš
mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Praví	pravit	k5eAaBmIp3nS	pravit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
2000	[number]	k4	2000
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
1999	[number]	k4	1999
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
-	-	kIx~	-
překlad	překlad	k1gInSc1	překlad
Vaší	váš	k3xOp2gFnSc2	váš
vynikající	vynikající	k2eAgFnSc2d1	vynikající
knihy	kniha	k1gFnSc2	kniha
by	by	kYmCp3nS	by
ovšem	ovšem	k9	ovšem
neměl	mít	k5eNaImAgInS	mít
chybět	chybět	k5eAaImF	chybět
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
z	z	k7c2	z
existujících	existující	k2eAgFnPc2d1	existující
řečí	řeč	k1gFnPc2	řeč
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
všude	všude	k6eAd1	všude
čtena	čten	k2eAgFnSc1d1	čtena
a	a	k8xC	a
diskutována	diskutován	k2eAgFnSc1d1	diskutována
<g/>
....	....	k?	....
Váš	váš	k3xOp2gMnSc1	váš
jednou	jednou	k6eAd1	jednou
provždy	provždy	k6eAd1	provždy
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
víc	hodně	k6eAd2	hodně
Alfred	Alfred	k1gMnSc1	Alfred
Nobel	Nobel	k1gMnSc1	Nobel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Berta	Berta	k1gFnSc1	Berta
Suttnerová	Suttnerová	k1gFnSc1	Suttnerová
psala	psát	k5eAaImAgFnS	psát
také	také	k9	také
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnSc2	povídka
a	a	k8xC	a
pacifistické	pacifistický	k2eAgInPc4d1	pacifistický
spisy	spis	k1gInPc4	spis
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
početného	početný	k2eAgNnSc2d1	početné
díla	dílo	k1gNnSc2	dílo
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Bertě	Berta	k1gFnSc3	Berta
Kinské	Kinská	k1gFnSc2	Kinská
<g/>
,	,	kIx,	,
guvernantce	guvernantka	k1gFnSc3	guvernantka
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Suttnerů	Suttner	k1gMnPc2	Suttner
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
paní	paní	k1gFnSc2	paní
baronky	baronka	k1gFnSc2	baronka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chtěla	chtít	k5eAaImAgFnS	chtít
zabránit	zabránit	k5eAaPmF	zabránit
dalšímu	další	k2eAgInSc3d1	další
vztahu	vztah	k1gInSc3	vztah
mezi	mezi	k7c7	mezi
Bertou	Berta	k1gFnSc7	Berta
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
synem	syn	k1gMnSc7	syn
Arturem	Artur	k1gMnSc7	Artur
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
doporučení	doporučení	k1gNnSc4	doporučení
přijmout	přijmout	k5eAaPmF	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
jiného	jiný	k2eAgNnSc2d1	jiné
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Předala	předat	k5eAaPmAgFnS	předat
jí	jíst	k5eAaImIp3nS	jíst
také	také	k9	také
inzerát	inzerát	k1gInSc4	inzerát
o	o	k7c4	o
hledání	hledání	k1gNnSc4	hledání
sekretářky	sekretářka	k1gFnSc2	sekretářka
znalé	znalý	k2eAgFnSc2d1	znalá
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Berta	Berta	k1gFnSc1	Berta
po	po	k7c6	po
korespondenci	korespondence	k1gFnSc6	korespondence
nabídku	nabídka	k1gFnSc4	nabídka
přijala	přijmout	k5eAaPmAgFnS	přijmout
<g/>
,	,	kIx,	,
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
jako	jako	k9	jako
tajemnice	tajemnice	k1gFnSc1	tajemnice
u	u	k7c2	u
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
požádán	požádat	k5eAaPmNgMnS	požádat
švédským	švédský	k2eAgMnSc7d1	švédský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Berta	Berta	k1gFnSc1	Berta
po	po	k7c6	po
pár	pár	k4xCyI	pár
týdnech	týden	k1gInPc6	týden
práce	práce	k1gFnSc2	práce
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
(	(	kIx(	(
<g/>
také	také	k9	také
na	na	k7c4	na
Arturovy	Arturův	k2eAgFnPc4d1	Arturova
písemné	písemný	k2eAgFnPc4d1	písemná
prosby	prosba	k1gFnPc4	prosba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátký	krátký	k2eAgInSc1d1	krátký
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
prožila	prožít	k5eAaPmAgFnS	prožít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
s	s	k7c7	s
Nobelem	Nobel	k1gMnSc7	Nobel
<g/>
,	,	kIx,	,
položil	položit	k5eAaPmAgMnS	položit
základ	základ	k1gInSc4	základ
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
celoživotnímu	celoživotní	k2eAgNnSc3d1	celoživotní
hlubokému	hluboký	k2eAgNnSc3d1	hluboké
přátelství	přátelství	k1gNnSc3	přátelství
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
úctě	úcta	k1gFnSc3	úcta
–	–	k?	–
a	a	k8xC	a
k	k	k7c3	k
dopisování	dopisování	k1gNnSc3	dopisování
po	po	k7c4	po
celou	celá	k1gFnSc4	celá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nobel	Nobel	k1gMnSc1	Nobel
si	se	k3xPyFc3	se
dobře	dobře	k6eAd1	dobře
rozuměl	rozumět	k5eAaImAgMnS	rozumět
i	i	k9	i
s	s	k7c7	s
Bertiným	Bertin	k2eAgMnSc7d1	Bertin
manželem	manžel	k1gMnSc7	manžel
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
sešli	sejít	k5eAaPmAgMnP	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
ledna	leden	k1gInSc2	leden
1893	[number]	k4	1893
Bertě	Berta	k1gFnSc6	Berta
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
mimořádně	mimořádně	k6eAd1	mimořádně
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
:	:	kIx,	:
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
Nobel	Nobel	k1gMnSc1	Nobel
Bertě	Berta	k1gFnSc3	Berta
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodlá	hodlat	k5eAaImIp3nS	hodlat
založit	založit	k5eAaPmF	založit
fond	fond	k1gInSc1	fond
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
vyplácet	vyplácet	k5eAaImF	vyplácet
ceny	cena	k1gFnPc4	cena
vědcům	vědec	k1gMnPc3	vědec
<g/>
,	,	kIx,	,
lékařům	lékař	k1gMnPc3	lékař
a	a	k8xC	a
že	že	k9	že
také	také	k9	také
míní	mínit	k5eAaImIp3nS	mínit
zřídit	zřídit	k5eAaPmF	zřídit
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
nějak	nějak	k6eAd1	nějak
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
světový	světový	k2eAgInSc4d1	světový
mír	mír	k1gInSc4	mír
a	a	k8xC	a
porozumění	porozumění	k1gNnSc4	porozumění
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
(	(	kIx(	(
<g/>
10.12	[number]	k4	10.12
<g/>
.1896	.1896	k4	.1896
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gFnSc1	jeho
závěť	závěť	k1gFnSc1	závěť
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
majetek	majetek	k1gInSc1	majetek
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
na	na	k7c4	na
"	"	kIx"	"
<g/>
ocenění	ocenění	k1gNnSc4	ocenění
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
během	během	k7c2	během
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
roku	rok	k1gInSc2	rok
prokázali	prokázat	k5eAaPmAgMnP	prokázat
největší	veliký	k2eAgInSc4d3	veliký
přínos	přínos	k1gInSc4	přínos
lidstvu	lidstvo	k1gNnSc3	lidstvo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
míra	míra	k1gFnSc1	míra
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
právě	právě	k6eAd1	právě
Bertě	Berta	k1gFnSc3	Berta
von	von	k1gInSc4	von
Suttnerové	Suttnerová	k1gFnSc2	Suttnerová
–	–	k?	–
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Nobela	Nobel	k1gMnSc4	Nobel
inspirací	inspirace	k1gFnPc2	inspirace
zejména	zejména	k9	zejména
při	při	k7c6	při
myšlence	myšlenka	k1gFnSc6	myšlenka
udělovat	udělovat	k5eAaImF	udělovat
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1905	[number]	k4	1905
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Berta	Berta	k1gFnSc1	Berta
von	von	k1gInSc4	von
Suttner	Suttner	k1gInSc1	Suttner
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
převzala	převzít	k5eAaPmAgFnS	převzít
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
v	v	k7c6	v
Kristianii	Kristianie	k1gFnSc6	Kristianie
(	(	kIx(	(
<g/>
Oslo	Oslo	k1gNnSc1	Oslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pronesla	pronést	k5eAaPmAgFnS	pronést
přednášku	přednáška	k1gFnSc4	přednáška
"	"	kIx"	"
<g/>
Vývoj	vývoj	k1gInSc1	vývoj
mírového	mírový	k2eAgNnSc2d1	Mírové
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Čestného	čestný	k2eAgNnSc2d1	čestné
ocenění	ocenění	k1gNnSc2	ocenění
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
také	také	k9	také
dostalo	dostat	k5eAaPmAgNnS	dostat
(	(	kIx(	(
<g/>
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
)	)	kIx)	)
při	při	k7c6	při
proslovu	proslov	k1gInSc6	proslov
Stefana	Stefan	k1gMnSc2	Stefan
Zweiga	Zweig	k1gMnSc2	Zweig
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
ženském	ženský	k2eAgInSc6d1	ženský
kongresu	kongres	k1gInSc6	kongres
k	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
národů	národ	k1gInPc2	národ
v	v	k7c6	v
Bernu	Bern	k1gInSc6	Bern
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyšla	vyjít	k5eAaPmAgFnS	vyjít
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
Život	život	k1gInSc1	život
pro	pro	k7c4	pro
mír	mír	k1gInSc4	mír
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
originál	originál	k1gInSc4	originál
Ein	Ein	k1gFnSc4	Ein
Leben	Leben	k2eAgInSc1d1	Leben
für	für	k?	für
den	den	k1gInSc1	den
Frieden	Frieden	k2eAgInSc1d1	Frieden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
577	[number]	k4	577
stranách	strana	k1gFnPc6	strana
popisuje	popisovat	k5eAaImIp3nS	popisovat
život	život	k1gInSc1	život
Berty	Berta	k1gFnSc2	Berta
Suttnerové	Suttnerová	k1gFnSc2	Suttnerová
<g/>
.	.	kIx.	.
</s>
<s>
Autorkou	autorka	k1gFnSc7	autorka
je	být	k5eAaImIp3nS	být
Brigitte	Brigitte	k1gFnSc1	Brigitte
Hamann	Hamanna	k1gFnPc2	Hamanna
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
knihu	kniha	k1gFnSc4	kniha
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Alena	Alena	k1gFnSc1	Alena
Bláhová	Bláhová	k1gFnSc1	Bláhová
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Paláci	palác	k1gInSc6	palác
Kinských	Kinská	k1gFnPc2	Kinská
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
rodačce	rodačka	k1gFnSc3	rodačka
Bertě	Berta	k1gFnSc3	Berta
von	von	k1gInSc4	von
Suttnerové-Kinské	Suttnerové-Kinský	k2eAgInPc1d1	Suttnerové-Kinský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
existuje	existovat	k5eAaImIp3nS	existovat
Společnost	společnost	k1gFnSc1	společnost
Berty	Berta	k1gFnSc2	Berta
Suttnerové	Suttnerové	k2eAgFnSc2d1	Suttnerové
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
odkazem	odkaz	k1gInSc7	odkaz
jejích	její	k3xOp3gFnPc2	její
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skandinávských	skandinávský	k2eAgFnPc6d1	skandinávská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
Suttnerové	Suttnerová	k1gFnSc2	Suttnerová
stalo	stát	k5eAaPmAgNnS	stát
námětem	námět	k1gInSc7	námět
pro	pro	k7c4	pro
stovky	stovka	k1gFnPc4	stovka
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2005	[number]	k4	2005
se	se	k3xPyFc4	se
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Senátu	senát	k1gInSc2	senát
ČR	ČR	kA	ČR
konala	konat	k5eAaImAgFnS	konat
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
konference	konference	k1gFnSc1	konference
(	(	kIx(	(
<g/>
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
stého	stý	k4xOgNnSc2	stý
výročí	výročí	k1gNnPc2	výročí
udělení	udělení	k1gNnSc2	udělení
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
)	)	kIx)	)
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
přítomnosti	přítomnost	k1gFnSc2	přítomnost
významných	významný	k2eAgMnPc2d1	významný
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
mírového	mírový	k2eAgInSc2d1	mírový
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
Mírového	mírový	k2eAgInSc2d1	mírový
kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
či	či	k8xC	či
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
Institutu	institut	k1gInSc2	institut
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
měst	město	k1gNnPc2	město
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nese	nést	k5eAaImIp3nS	nést
dodnes	dodnes	k6eAd1	dodnes
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
v	v	k7c6	v
názvech	název	k1gInPc6	název
ulic	ulice	k1gFnPc2	ulice
či	či	k8xC	či
náměstí	náměstí	k1gNnSc1	náměstí
a	a	k8xC	a
Bertha	Bertha	k1gFnSc1	Bertha
von	von	k1gInSc1	von
Suttner	Suttner	k1gMnSc1	Suttner
je	být	k5eAaImIp3nS	být
zobrazena	zobrazen	k2eAgMnSc4d1	zobrazen
na	na	k7c6	na
rakouské	rakouský	k2eAgFnSc6d1	rakouská
minci	mince	k1gFnSc6	mince
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
2	[number]	k4	2
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
portrét	portrét	k1gInSc1	portrét
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
i	i	k9	i
na	na	k7c6	na
starší	starý	k2eAgFnSc6d2	starší
rakouské	rakouský	k2eAgFnSc6d1	rakouská
bankovce-	bankovce-	k?	bankovce-
<g/>
1000	[number]	k4	1000
šilinků	šilink	k1gInPc2	šilink
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vyšel	vyjít	k5eAaPmAgInS	vyjít
její	její	k3xOp3gInSc4	její
portrét	portrét	k1gInSc4	portrét
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
osmnáctikorunové	osmnáctikorunový	k2eAgFnSc6d1	osmnáctikorunový
známce	známka	k1gFnSc6	známka
<g/>
.	.	kIx.	.
</s>
