<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
Logo	logo	k1gNnSc4
herní	herní	k2eAgFnSc2d1
sérieZákladní	sérieZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Žánr	žánr	k1gInSc1
</s>
<s>
first-person	first-person	k1gMnSc1
shooter	shooter	k1gMnSc1
Vývojáři	vývojář	k1gMnPc1
</s>
<s>
HlavníInfinity	HlavníInfinit	k1gInPc1
Ward	Warda	k1gFnPc2
<g/>
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
<g/>
Treyarch	Treyarch	k1gInSc1
<g/>
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
<g/>
Sledgehammer	Sledgehammer	k1gMnSc1
Games	Games	k1gMnSc1
<g/>
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
<g/>
DalšíRaven	DalšíRavna	k1gFnPc2
SoftwareNerve	SoftwareNerev	k1gFnSc2
SoftwareGray	SoftwareGraa	k1gFnSc2
Matter	Matter	k1gInSc1
InteractiveNokiaExakt	InteractiveNokiaExakt	k1gInSc1
EntertainmentSpark	EntertainmentSpark	k1gInSc4
UnlimitedAmaze	UnlimitedAmaha	k1gFnSc3
Entertainmentn-SpaceAspyrRebellion	Entertainmentn-SpaceAspyrRebellion	k1gInSc1
DevelopmentsIdeaworks	DevelopmentsIdeaworks	k1gInSc1
Game	game	k1gInSc1
StudionStigate	StudionStigat	k1gInSc5
GamesNeversoftCertain	GamesNeversoftCertain	k1gMnSc1
AffinityHigh	AffinityHigha	k1gFnPc2
Moon	Moon	k1gMnSc1
StudiosBeenoxMercenary	StudiosBeenoxMercenara	k1gFnSc2
TechnologyKuju	TechnologyKuju	k1gMnSc1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Activision	Activision	k1gInSc1
Platformy	platforma	k1gFnSc2
</s>
<s>
Microsoft	Microsoft	kA
WindowsOS	WindowsOS	k1gFnSc1
XNintendo	XNintendo	k6eAd1
DSGameCubeNokia	DSGameCubeNokium	k1gNnSc2
N-GagePlayStation	N-GagePlayStation	k1gInSc4
2	#num#	k4
<g/>
PlayStation	PlayStation	k1gInSc1
3	#num#	k4
<g/>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
<g/>
PlayStation	PlayStation	k1gInSc1
PortablePlayStation	PortablePlayStation	k1gInSc4
VitaWiiWii	VitaWiiWie	k1gFnSc3
UXboxXbox	UXboxXbox	k1gInSc1
360	#num#	k4
<g/>
Xbox	Xbox	k1gInSc1
OneiOSAndroidBlackBerryJ	OneiOSAndroidBlackBerryJ	k1gFnSc2
<g/>
2	#num#	k4
<g/>
ME	ME	kA
První	první	k4xOgInSc4
vydání	vydání	k1gNnSc6
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2003	#num#	k4
Poslední	poslední	k2eAgInSc4d1
vydání	vydání	k1gNnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
<g/>
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
volání	volání	k1gNnSc2
povinnosti	povinnost	k1gFnSc2
<g/>
;	;	kIx,
zkratkou	zkratka	k1gFnSc7
CoD	CoD	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
3D	3D	kA
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
hraných	hraný	k2eAgFnPc2d1
z	z	k7c2
pohledu	pohled	k1gInSc2
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
3	#num#	k4
díly	dílo	k1gNnPc7
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc4
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc4
jsou	být	k5eAaImIp3nP
z	z	k7c2
prostředí	prostředí	k1gNnSc2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
nebo	nebo	k8xC
v	v	k7c6
blízké	blízký	k2eAgFnSc6d1
budoucnosti	budoucnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgMnPc7d1
vývojáři	vývojář	k1gMnPc7
hry	hra	k1gFnSc2
jsou	být	k5eAaImIp3nP
Infinity	Infinita	k1gFnPc1
Ward	Warda	k1gFnPc2
<g/>
,	,	kIx,
Treyarch	Treyarcha	k1gFnPc2
<g/>
,	,	kIx,
Sledgehammer	Sledgehammra	k1gFnPc2
Games	Gamesa	k1gFnPc2
a	a	k8xC
jednu	jeden	k4xCgFnSc4
hru	hra	k1gFnSc4
vydalo	vydat	k5eAaPmAgNnS
studio	studio	k1gNnSc1
Nihilistic	Nihilistice	k1gFnPc2
<g/>
,	,	kIx,
vydavatelem	vydavatel	k1gMnSc7
pak	pak	k6eAd1
Activision	Activision	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Rok	rok	k1gInSc1
vydání	vydání	k1gNnSc2
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Platformy	platforma	k1gFnPc1
</s>
<s>
Vývojář	vývojář	k1gMnSc1
</s>
<s>
PC	PC	kA
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Kapesní	kapesní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
</s>
<s>
2003	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
</s>
<s>
Windows	Windows	kA
<g/>
,	,	kIx,
OS	OS	kA
X	X	kA
</s>
<s>
—	—	k?
</s>
<s>
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
,	,	kIx,
PS3	PS3	k1gFnSc1
</s>
<s>
—	—	k?
</s>
<s>
N-Gage	N-Gage	k6eAd1
</s>
<s>
Infinity	Infinita	k1gFnPc1
Ward	Warda	k1gFnPc2
</s>
<s>
2005	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
2	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
Xbox	Xbox	k1gInSc1
360	#num#	k4
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
</s>
<s>
Mobilní	mobilní	k2eAgInSc1d1
telefon	telefon	k1gInSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
3	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
PS	PS	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Xbox	Xbox	k1gInSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
,	,	kIx,
PS	PS	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Wii	Wii	k1gFnSc1
</s>
<s>
—	—	k?
</s>
<s>
Treyarch	Treyarch	k1gMnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
4	#num#	k4
<g/>
:	:	kIx,
Modern	Modern	k1gInSc1
Warfare	Warfar	k1gMnSc5
</s>
<s>
Windows	Windows	kA
<g/>
,	,	kIx,
OS	OS	kA
X	X	kA
</s>
<s>
—	—	k?
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gMnPc2
<g/>
,	,	kIx,
PS4	PS4	k1gMnPc2
</s>
<s>
Nintendo	Nintendo	k6eAd1
DS	DS	kA
</s>
<s>
Infinity	Infinita	k1gFnPc1
Ward	Warda	k1gFnPc2
</s>
<s>
2008	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
</s>
<s>
Windows	Windows	kA
</s>
<s>
PS2	PS2	k4
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
</s>
<s>
Nintendo	Nintendo	k1gNnSc1
DS	DS	kA
<g/>
,	,	kIx,
Windows	Windows	kA
Mobile	mobile	k1gNnSc1
</s>
<s>
Treyarch	Treyarch	k1gMnSc1
</s>
<s>
2009	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gInSc5
2	#num#	k4
</s>
<s>
Windows	Windows	kA
<g/>
,	,	kIx,
OS	OS	kA
X	X	kA
</s>
<s>
—	—	k?
</s>
<s>
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
,	,	kIx,
PS3	PS3	k1gFnSc1
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Infinity	Infinita	k1gFnPc1
Ward	Warda	k1gFnPc2
</s>
<s>
2010	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
</s>
<s>
—	—	k?
</s>
<s>
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
,	,	kIx,
PS	PS	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Wii	Wii	k1gFnSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
</s>
<s>
Nintendo	Nintendo	k6eAd1
DS	DS	kA
</s>
<s>
Treyarch	Treyarch	k1gMnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gInSc5
3	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Infinity	Infinit	k1gInPc1
Ward	Warda	k1gFnPc2
/	/	kIx~
Sledgehammer	Sledgehammer	k1gMnSc1
Games	Games	k1gMnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
II	II	kA
</s>
<s>
Windows	Windows	kA
</s>
<s>
—	—	k?
</s>
<s>
Xbox	Xbox	k1gInSc1
360	#num#	k4
<g/>
,	,	kIx,
PS3	PS3	k1gFnSc1
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gMnPc2
<g/>
,	,	kIx,
Wii	Wii	k1gMnPc2
U	u	k7c2
</s>
<s>
—	—	k?
</s>
<s>
Treyarch	Treyarch	k1gMnSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Ghosts	Ghosts	k1gInSc1
</s>
<s>
—	—	k?
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
<g/>
,	,	kIx,
PS	PS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
Wii	Wii	k1gFnSc1
U	u	k7c2
</s>
<s>
—	—	k?
</s>
<s>
Infinity	Infinita	k1gFnPc1
Ward	Warda	k1gFnPc2
</s>
<s>
2014	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Advanced	Advanced	k1gMnSc1
Warfare	Warfar	k1gInSc5
</s>
<s>
—	—	k?
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gMnPc2
<g/>
,	,	kIx,
PS4	PS4	k1gMnPc2
</s>
<s>
—	—	k?
</s>
<s>
Sledgehammer	Sledgehammer	k1gMnSc1
Games	Games	k1gMnSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
III	III	kA
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Treyarch	Treyarch	k1gMnSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Infinite	Infinit	k1gInSc5
Warfare	Warfar	k1gMnSc5
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Infinity	Infinita	k1gFnPc1
Ward	Warda	k1gFnPc2
</s>
<s>
2017	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
WWII	WWII	kA
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Sledgehammer	Sledgehammer	k1gMnSc1
Games	Games	k1gMnSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
4	#num#	k4
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Treyarch	Treyarch	k1gMnSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
—	—	k?
</s>
<s>
Infinity	Infinita	k1gFnPc1
Ward	Warda	k1gFnPc2
</s>
<s>
2020	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
Cold	Cold	k1gMnSc1
War	War	k1gMnSc1
</s>
<s>
Windows	Windows	kA
</s>
<s>
Xbox	Xbox	k1gInSc1
One	One	k1gMnPc2
<g/>
,	,	kIx,
PS4	PS4	k1gMnPc2
</s>
<s>
Treyarch	Treyarch	k1gMnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
(	(	kIx(
<g/>
hra	hra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
volání	volání	k1gNnSc1
povinnosti	povinnost	k1gFnSc2
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
CoD	coda	k1gFnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
retronymicky	retronymicky	k6eAd1
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
1	#num#	k4
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
CoD	coda	k1gFnPc2
1	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
3D	3D	k4
akční	akční	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
z	z	k7c2
prostředí	prostředí	k1gNnSc2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
bojuje	bojovat	k5eAaImIp3nS
za	za	k7c4
stranu	strana	k1gFnSc4
Sovětů	Sovět	k1gMnPc2
<g/>
,	,	kIx,
Britů	Brit	k1gMnPc2
a	a	k8xC
Američanů	Američan	k1gMnPc2
proti	proti	k7c3
Němcům	Němec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
oceněna	ocenit	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
Nejlepší	dobrý	k2eAgFnSc1d3
hra	hra	k1gFnSc1
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Výrobce	výrobce	k1gMnSc1
hry	hra	k1gFnSc2
je	být	k5eAaImIp3nS
Infinity	Infinita	k1gFnPc4
Ward	Wardo	k1gNnPc2
<g/>
,	,	kIx,
vydavatel	vydavatel	k1gMnSc1
společnost	společnost	k1gFnSc1
Activision	Activision	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Singleplayer	Singleplayer	k1gInSc1
<g/>
,	,	kIx,
neboli	neboli	k8xC
hra	hra	k1gFnSc1
pro	pro	k7c4
jednoho	jeden	k4xCgMnSc4
hráče	hráč	k1gMnSc4
<g/>
,	,	kIx,
hráče	hráč	k1gMnSc4
provádí	provádět	k5eAaImIp3nP
bitvami	bitva	k1gFnPc7
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bojuje	bojovat	k5eAaImIp3nS
za	za	k7c7
3	#num#	k4
národy	národ	k1gInPc7
–	–	k?
Američany	Američan	k1gMnPc7
<g/>
,	,	kIx,
Sověty	Sovět	k1gMnPc7
a	a	k8xC
Brity	Brit	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
také	také	k9
multiplayer	multiplayer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
2	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
2	#num#	k4
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
CoD	coda	k1gFnPc2
2	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
akční	akční	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
hra	hra	k1gFnSc1
od	od	k7c2
vývojářů	vývojář	k1gMnPc2
Infinity	Infinita	k1gFnSc2
Ward	Ward	k1gMnSc1
a	a	k8xC
vydavatele	vydavatel	k1gMnSc4
Activision	Activision	k1gInSc4
zasazena	zasadit	k5eAaPmNgFnS
do	do	k7c2
prostředí	prostředí	k1gNnSc2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydána	vydán	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
pro	pro	k7c4
PC	PC	kA
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
pro	pro	k7c4
Xbox	Xbox	k1gInSc4
360	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgFnP
verze	verze	k1gFnPc1
pro	pro	k7c4
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
<g/>
,	,	kIx,
Pocket	Pocket	k1gInSc4
PC	PC	kA
a	a	k8xC
Smartphone	Smartphon	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejlépe	dobře	k6eAd3
hodnocených	hodnocený	k2eAgFnPc2d1
her	hra	k1gFnPc2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
postupně	postupně	k6eAd1
ovládá	ovládat	k5eAaImIp3nS
čtyři	čtyři	k4xCgMnPc4
vojáky	voják	k1gMnPc4
(	(	kIx(
<g/>
2	#num#	k4
britské	britský	k2eAgInPc1d1
<g/>
,	,	kIx,
1	#num#	k4
amerického	americký	k2eAgInSc2d1
<g/>
,	,	kIx,
1	#num#	k4
sovětského	sovětský	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
individuálních	individuální	k2eAgFnPc6d1
kampaních	kampaň	k1gFnPc6
musí	muset	k5eAaImIp3nS
překonávat	překonávat	k5eAaImF
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
smrtelně	smrtelně	k6eAd1
nebezpečných	bezpečný	k2eNgFnPc2d1
bojových	bojový	k2eAgFnPc2d1
situací	situace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
výběr	výběr	k1gInSc4
je	být	k5eAaImIp3nS
několik	několik	k4yIc4
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
veškerým	veškerý	k3xTgInSc7
obsahem	obsah	k1gInSc7
hry	hra	k1gFnSc2
prokousat	prokousat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
postupně	postupně	k6eAd1
absolvovat	absolvovat	k5eAaPmF
všechny	všechen	k3xTgFnPc1
čtyři	čtyři	k4xCgFnPc1
kampaně	kampaň	k1gFnPc1
v	v	k7c6
řadě	řada	k1gFnSc6
nebo	nebo	k8xC
si	se	k3xPyFc3
vybrat	vybrat	k5eAaPmF
kteroukoli	kterýkoli	k3yIgFnSc4
bitvu	bitva	k1gFnSc4
z	z	k7c2
kterékoli	kterýkoli	k3yIgFnSc2
kampaně	kampaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
v	v	k7c6
tomto	tento	k3xDgInSc6
dílu	díl	k1gInSc6
série	série	k1gFnSc2
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
multiplayer	multiplayra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
3	#num#	k4
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
3	#num#	k4
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
CoD	coda	k1gFnPc2
3	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
třetí	třetí	k4xOgInSc4
díl	díl	k1gInSc4
úspěšné	úspěšný	k2eAgFnSc2d1
3D	3D	k4
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
ze	z	k7c2
série	série	k1gFnSc2
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
z	z	k7c2
prostředí	prostředí	k1gNnSc2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
poprvé	poprvé	k6eAd1
nehraje	hrát	k5eNaImIp3nS
jenom	jenom	k9
za	za	k7c4
Američany	Američan	k1gMnPc4
<g/>
,	,	kIx,
Brity	Brit	k1gMnPc4
a	a	k8xC
Rusy	Rus	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
místo	místo	k7c2
Rusů	Rus	k1gMnPc2
tu	tu	k6eAd1
jsou	být	k5eAaImIp3nP
Kanaďani	Kanaďan	k1gMnPc1
a	a	k8xC
Poláci	Polák	k1gMnPc1
(	(	kIx(
<g/>
Američani	Američan	k1gMnPc1
a	a	k8xC
Britové	Brit	k1gMnPc1
jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
stále	stále	k6eAd1
<g/>
)	)	kIx)
proti	proti	k7c3
nacistům	nacista	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobce	výrobce	k1gMnSc1
hry	hra	k1gFnSc2
je	být	k5eAaImIp3nS
Treyarch	Treyarch	k1gInSc1
a	a	k8xC
Pi	pi	k0
Studios	Studios	k?
<g/>
,	,	kIx,
vydavatel	vydavatel	k1gMnSc1
společnost	společnost	k1gFnSc1
Activision	Activision	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zatím	zatím	k6eAd1
jediná	jediný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
z	z	k7c2
hlavní	hlavní	k2eAgFnSc2d1
serie	serie	k1gFnSc2
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
nevyšla	vyjít	k5eNaPmAgFnS
pro	pro	k7c4
PC	PC	kA
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
4	#num#	k4
<g/>
:	:	kIx,
Modern	Modern	k1gInSc1
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
4	#num#	k4
<g/>
:	:	kIx,
Modern	Modern	k1gInSc1
Warfare	Warfar	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
4	#num#	k4
<g/>
:	:	kIx,
Modern	Modern	k1gInSc1
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
CoD	coda	k1gFnPc2
4	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čtvrtý	čtvrtý	k4xOgInSc1
díl	díl	k1gInSc1
úspěšné	úspěšný	k2eAgFnSc2d1
série	série	k1gFnSc2
3D	3D	k4
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
oceněna	ocenit	k5eAaPmNgFnS
jako	jako	k8xC,k8xS
Nejlepší	dobrý	k2eAgFnSc1d3
hra	hra	k1gFnSc1
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odehrává	odehrávat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nedaleké	daleký	k2eNgFnSc6d1
budoucnosti	budoucnost	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
první	první	k4xOgInSc4
díl	díl	k1gInSc4
ze	z	k7c2
série	série	k1gFnSc2
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
neodehrává	odehrávat	k5eNaImIp3nS
v	v	k7c6
době	doba	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c6
3	#num#	k4
části	část	k1gFnSc6
<g/>
.	.	kIx.
během	během	k7c2
nichž	jenž	k3xRgInPc2
hrajete	hrát	k5eAaImIp2nP
většinou	většina	k1gFnSc7
za	za	k7c4
seržanta	seržant	k1gMnSc4
Johna	John	k1gMnSc4
„	„	k?
<g/>
Soap	Soap	k1gMnSc1
<g/>
“	“	k?
MacTavishe	MacTavish	k1gFnSc2
nebo	nebo	k8xC
za	za	k7c2
seržanta	seržant	k1gMnSc2
Paula	Paul	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
během	během	k7c2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Rusku	Rusko	k1gNnSc6
a	a	k8xC
nepokojích	nepokoj	k1gInPc6
v	v	k7c6
jedné	jeden	k4xCgFnSc6
zemi	zem	k1gFnSc6
na	na	k7c6
středním	střední	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruští	ruský	k2eAgMnPc1d1
rebelové	rebel	k1gMnPc1
a	a	k8xC
muslimové	muslim	k1gMnPc1
spolu	spolu	k6eAd1
spolupracují	spolupracovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britská	britský	k2eAgFnSc1d1
SAS	Sas	k1gMnSc1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
podporuje	podporovat	k5eAaImIp3nS
akce	akce	k1gFnSc1
loajalistů	loajalista	k1gMnPc2
bránících	bránící	k2eAgMnPc2d1
současnou	současný	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
před	před	k7c4
rebely	rebel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
probíhá	probíhat	k5eAaImIp3nS
státní	státní	k2eAgInSc4d1
převrat	převrat	k1gInSc4
a	a	k8xC
zasahují	zasahovat	k5eAaImIp3nP
zde	zde	k6eAd1
Američané	Američan	k1gMnPc1
<g/>
,	,	kIx,
muslimové	muslim	k1gMnPc1
ale	ale	k8xC
kladou	klást	k5eAaImIp3nP
tvrdý	tvrdý	k2eAgInSc4d1
odpor	odpor	k1gInSc4
a	a	k8xC
akce	akce	k1gFnSc1
se	se	k3xPyFc4
protahuje	protahovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhé	druhý	k4xOgFnSc6
části	část	k1gFnSc6
hry	hra	k1gFnSc2
se	se	k3xPyFc4
na	na	k7c4
2	#num#	k4
mise	mise	k1gFnSc2
ocitnete	ocitnout	k5eAaPmIp2nP
v	v	k7c6
kůži	kůže	k1gFnSc6
kapitána	kapitán	k1gMnSc2
Price	Price	k1gMnSc2
(	(	kIx(
<g/>
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
poručík	poručík	k1gMnSc1
<g/>
)	)	kIx)
během	běh	k1gInSc7
během	během	k7c2
retrospektivně	retrospektivně	k6eAd1
vyprávěné	vyprávěný	k2eAgFnSc2d1
utajované	utajovaný	k2eAgFnSc2d1
mise	mise	k1gFnSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Černobylu	Černobyl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
nebo	nebo	k8xC
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
CoD	coda	k1gFnPc2
WaW	WaW	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pátý	pátý	k4xOgInSc1
díl	díl	k1gInSc1
úspěšné	úspěšný	k2eAgFnSc2d1
série	série	k1gFnSc2
3D	3D	k4
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
ze	z	k7c2
série	série	k1gFnSc2
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
odehrává	odehrávat	k5eAaImIp3nS
za	za	k7c4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráč	hráč	k1gMnSc1
během	během	k7c2
hry	hra	k1gFnSc2
hraje	hrát	k5eAaImIp3nS
jako	jako	k9
vojín	vojín	k1gMnSc1
C.	C.	kA
Miller	Miller	k1gMnSc1
za	za	k7c4
Američany	Američan	k1gMnPc4
proti	proti	k7c3
Japoncům	Japonec	k1gMnPc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
vojín	vojín	k1gMnSc1
Dimitri	Dimitr	k1gFnSc2
Petrenko	Petrenka	k1gFnSc5
za	za	k7c4
Sověty	Sověty	k1gInPc4
proti	proti	k7c3
Němcům	Němec	k1gMnPc3
a	a	k8xC
nakonec	nakonec	k6eAd1
jako	jako	k8xS,k8xC
PO	Po	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Locke	Lock	k1gInSc2
opět	opět	k6eAd1
za	za	k7c4
Američany	Američan	k1gMnPc4
jako	jako	k8xS,k8xC
člen	člen	k1gMnSc1
Posádky	posádka	k1gFnSc2
letadla	letadlo	k1gNnSc2
Consolidated	Consolidated	k1gMnSc1
PBY	PBY	kA
Catalina	Catalina	k1gFnSc1
zase	zase	k9
proti	proti	k7c3
Japoncům	Japonec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
za	za	k7c2
vojína	vojín	k1gMnSc2
Millera	Miller	k1gMnSc2
útěkem	útěk	k1gInSc7
ze	z	k7c2
zajateckého	zajatecký	k2eAgInSc2d1
tábora	tábor	k1gInSc2
a	a	k8xC
končí	končit	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
Call	Callum	k1gNnPc2
of	of	k?
Duty	Duty	k?
vztyčení	vztyčení	k1gNnSc2
sovětské	sovětský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
na	na	k7c6
střeše	střecha	k1gFnSc6
Reichstagu	Reichstag	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
také	také	k9
první	první	k4xOgInSc1
díl	díl	k1gInSc1
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
se	se	k3xPyFc4
neobjevuje	objevovat	k5eNaImIp3nS
pouze	pouze	k6eAd1
singleplayer	singleplayer	k1gInSc4
a	a	k8xC
multiplayer	multiplayer	k1gInSc4
ale	ale	k8xC
i	i	k9
zombie	zombie	k1gFnSc1
mód	móda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
2	#num#	k4
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
2	#num#	k4
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
také	také	k9
CoD	coda	k1gFnPc2
MW2	MW2	k1gMnSc1
nebo	nebo	k8xC
MW	MW	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c4
pořadí	pořadí	k1gNnSc4
šestý	šestý	k4xOgInSc1
díl	díl	k1gInSc1
úspěšné	úspěšný	k2eAgFnSc2d1
série	série	k1gFnSc2
3D	3D	k4
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
ze	z	k7c2
série	série	k1gFnSc2
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xC,k8xS
už	už	k6eAd1
sám	sám	k3xTgInSc1
název	název	k1gInSc1
napovídá	napovídat	k5eAaBmIp3nS
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
volné	volný	k2eAgNnSc4d1
pokračování	pokračování	k1gNnSc4
hry	hra	k1gFnSc2
CoD	coda	k1gFnPc2
4	#num#	k4
<g/>
:	:	kIx,
Modern	Modern	k1gInSc1
Warfare	Warfar	k1gMnSc5
<g/>
,	,	kIx,
a	a	k8xC
opět	opět	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
blízkou	blízký	k2eAgFnSc4d1
současnost	současnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ultranacionalisté	ultranacionalista	k1gMnPc1
vyhráli	vyhrát	k5eAaPmAgMnP
občanskou	občanský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
poté	poté	k6eAd1
operuje	operovat	k5eAaImIp3nS
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
zjišťuje	zjišťovat	k5eAaImIp3nS
zapojení	zapojení	k1gNnSc4
CIA	CIA	kA
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
území	území	k1gNnSc6
<g/>
,	,	kIx,
zareaguje	zareagovat	k5eAaPmIp3nS
invazí	invaze	k1gFnPc2
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
obsazuje	obsazovat	k5eAaImIp3nS
Aljašku	Aljaška	k1gFnSc4
a	a	k8xC
obě	dva	k4xCgNnPc4
pobřeží	pobřeží	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šestý	šestý	k4xOgInSc1
díl	díl	k1gInSc1
je	být	k5eAaImIp3nS
opět	opět	k6eAd1
od	od	k7c2
tvůrců	tvůrce	k1gMnPc2
z	z	k7c2
Infinity	Infinita	k1gFnSc2
Ward	Ward	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
všechny	všechen	k3xTgInPc4
předchozí	předchozí	k2eAgInPc4d1
díly	díl	k1gInPc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
dílu	díl	k1gInSc2
třetího	třetí	k4xOgNnSc2
(	(	kIx(
<g/>
CoD	coda	k1gFnPc2
3	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
pátého	pátý	k4xOgInSc2
(	(	kIx(
<g/>
CoD	coda	k1gFnPc2
5	#num#	k4
WaW	WaW	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Modern	Moderna	k1gFnPc2
Warfare	Warfar	k1gMnSc5
z	z	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
hráč	hráč	k1gMnSc1
možnost	možnost	k1gFnSc4
naklánět	naklánět	k5eAaImF
se	se	k3xPyFc4
doleva	doleva	k6eAd1
a	a	k8xC
doprava	doprava	k1gFnSc1
ze	z	k7c2
zákrytu	zákryt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejlépe	dobře	k6eAd3
oceněná	oceněný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
je	být	k5eAaImIp3nS
sedmý	sedmý	k4xOgInSc4
díl	díl	k1gInSc4
úspěšné	úspěšný	k2eAgFnSc2d1
série	série	k1gFnSc2
3D	3D	k4
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
vyvíjena	vyvíjet	k5eAaImNgFnS
společností	společnost	k1gFnSc7
Treyarch	Treyarch	k1gInSc1
a	a	k8xC
;	;	kIx,
<g/>
vydání	vydání	k1gNnSc1
je	být	k5eAaImIp3nS
oficiálně	oficiálně	k6eAd1
oznámeno	oznámit	k5eAaPmNgNnS
na	na	k7c4
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgInSc4
díl	díl	k1gInSc4
ze	z	k7c2
serie	serie	k1gFnSc2
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
ovládáte	ovládat	k5eAaImIp2nP
americké	americký	k2eAgInPc4d1
agenty	agens	k1gInPc4
během	během	k7c2
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
3	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
3	#num#	k4
je	být	k5eAaImIp3nS
třetím	třetí	k4xOgNnSc7
pokračováním	pokračování	k1gNnSc7
známé	známý	k2eAgFnSc2d1
podsérie	podsérie	k1gFnSc2
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
s	s	k7c7
názvem	název	k1gInSc7
Modern	Moderna	k1gFnPc2
Warfare	Warfar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračuje	pokračovat	k5eAaImIp3nS
ruská	ruský	k2eAgFnSc1d1
invaze	invaze	k1gFnSc1
do	do	k7c2
USA	USA	kA
a	a	k8xC
také	také	k9
se	se	k3xPyFc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
do	do	k7c2
Evropy	Evropa	k1gFnSc2
včetně	včetně	k7c2
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
hry	hra	k1gFnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
listopadu	listopad	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běží	běžet	k5eAaImIp3nS
na	na	k7c6
stejném	stejný	k2eAgInSc6d1
enginu	engin	k1gInSc6
jako	jako	k8xC,k8xS
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
2	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
četnými	četný	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
díl	díl	k1gInSc4
nás	my	k3xPp1nPc4
zavede	zavést	k5eAaPmIp3nS
do	do	k7c2
různých	různý	k2eAgNnPc2d1
měst	město	k1gNnPc2
jako	jako	k8xS,k8xC
např.	např.	kA
<g/>
:	:	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Hamburg	Hamburg	k1gInSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
a	a	k8xC
dokonce	dokonce	k9
si	se	k3xPyFc3
zahrajeme	zahrát	k5eAaPmIp1nP
v	v	k7c6
Praze	Praha	k1gFnSc6
i	i	k8xC
na	na	k7c6
hradě	hrad	k1gInSc6
Karlštejně	Karlštejn	k1gInSc6
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc2
II	II	kA
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
2	#num#	k4
je	být	k5eAaImIp3nS
volné	volný	k2eAgNnSc1d1
pokračování	pokračování	k1gNnSc1
sedmého	sedmý	k4xOgInSc2
dílu	díl	k1gInSc2
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
vypráví	vyprávět	k5eAaImIp3nS
hlavně	hlavně	k9
o	o	k7c4
synovi	syn	k1gMnSc3
hlavního	hlavní	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
původního	původní	k2eAgInSc2d1
Black	Black	k1gInSc4
Ops	Ops	k1gMnSc2
<g/>
,	,	kIx,
Davida	David	k1gMnSc2
Masona	mason	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
2025	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
protíná	protínat	k5eAaImIp3nS
s	s	k7c7
minulostí	minulost	k1gFnSc7
a	a	k8xC
dozvíme	dozvědět	k5eAaPmIp1nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
dopadne	dopadnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
díl	díl	k1gInSc1
prošel	projít	k5eAaPmAgInS
mnoha	mnoho	k4c7
změnami	změna	k1gFnPc7
od	od	k7c2
posledního	poslední	k2eAgNnSc2d1
CoD	coda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
hře	hra	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
hrát	hrát	k5eAaImF
také	také	k9
za	za	k7c4
stroje	stroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Ghosts	Ghosts	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Ghosts	Ghosts	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Ghosts	Ghosts	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
také	také	k9
CoD	coda	k1gFnPc2
G	G	kA
nebo	nebo	k8xC
Gh	Gh	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
pořadí	pořadí	k1gNnSc6
již	již	k9
desátý	desátý	k4xOgInSc4
díl	díl	k1gInSc4
úspěšné	úspěšný	k2eAgFnSc2d1
série	série	k1gFnSc2
3D	3D	k4
akčních	akční	k2eAgFnPc2d1
her	hra	k1gFnPc2
ze	z	k7c2
série	série	k1gFnSc2
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
poháněna	pohánět	k5eAaImNgFnS
novým	nový	k2eAgInSc7d1
enginem	engin	k1gInSc7
<g/>
,	,	kIx,
přinášejícím	přinášející	k2eAgInSc7d1
činnosti	činnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
dokáží	dokázat	k5eAaPmIp3nP
přeměnit	přeměnit	k5eAaPmF
celou	celý	k2eAgFnSc4d1
mapu	mapa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Advanced	Advanced	k1gMnSc1
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Advanced	Advanced	k1gMnSc1
Warfare	Warfar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc2
III	III	kA
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
pokračování	pokračování	k1gNnSc4
dílů	díl	k1gInPc2
série	série	k1gFnSc2
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
Call	Call	k1gInSc4
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Infinite	Infinit	k1gInSc5
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Infinite	Infinit	k1gInSc5
Warfare	Warfar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Infinite	Infinit	k1gInSc5
Warfare	Warfar	k1gMnSc5
od	od	k7c2
studia	studio	k1gNnSc2
Infinity	Infinita	k1gFnSc2
Ward	Warda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
díl	díl	k1gInSc4
nové	nový	k2eAgNnSc1d1
<g/>
,	,	kIx,
samostatné	samostatný	k2eAgFnPc4d1
série	série	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
Remastered	Remastered	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
Remastered	Remastered	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Nově	nově	k6eAd1
zpracovaná	zpracovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
4	#num#	k4
<g/>
:	:	kIx,
Modern	Modern	k1gInSc1
Warfare	Warfar	k1gMnSc5
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
WWII	WWII	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
WWII	WWII	kA
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
WWII	WWII	kA
je	být	k5eAaImIp3nS
čtrnáctým	čtrnáctý	k4xOgInSc7
dílem	díl	k1gInSc7
herní	herní	k2eAgFnSc2d1
série	série	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
vyvinula	vyvinout	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Sledgehammer	Sledgehammer	k1gMnSc1
Games	Games	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
celosvětově	celosvětově	k6eAd1
vydán	vydat	k5eAaPmNgInS
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
na	na	k7c6
platformách	platforma	k1gFnPc6
Microsoft	Microsoft	kA
Windows	Windows	kA
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
a	a	k8xC
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Díl	díl	k1gInSc1
je	být	k5eAaImIp3nS
zasazen	zasadit	k5eAaPmNgInS
do	do	k7c2
evropského	evropský	k2eAgNnSc2d1
období	období	k1gNnSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
muže	muž	k1gMnPc4
z	z	k7c2
1	#num#	k4
<g/>
.	.	kIx.
pěší	pěší	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
bojovali	bojovat	k5eAaImAgMnP
na	na	k7c6
západní	západní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
a	a	k8xC
účastnili	účastnit	k5eAaImAgMnP
se	se	k3xPyFc4
operace	operace	k1gFnSc2
Overlord	Overlorda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
4	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
IIII	IIII	kA
je	být	k5eAaImIp3nS
vyvíjeno	vyvíjet	k5eAaImNgNnS
studiem	studio	k1gNnSc7
Treyarch	Treyarcha	k1gFnPc2
a	a	k8xC
distribuované	distribuovaný	k2eAgFnSc2d1
společností	společnost	k1gFnSc7
Activision	Activision	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
patnáctou	patnáctý	k4xOgFnSc4
hlavní	hlavní	k2eAgFnSc4d1
hru	hra	k1gFnSc4
série	série	k1gFnSc2
a	a	k8xC
pátou	pátý	k4xOgFnSc4
hru	hra	k1gFnSc4
v	v	k7c6
příběhové	příběhový	k2eAgFnSc6d1
linii	linie	k1gFnSc6
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
titul	titul	k1gInSc1
ze	z	k7c2
série	série	k1gFnSc2
bez	bez	k7c2
tradičního	tradiční	k2eAgInSc2d1
režimu	režim	k1gInSc2
pro	pro	k7c4
jednoho	jeden	k4xCgMnSc4
hráče	hráč	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
hry	hra	k1gFnPc4
bylo	být	k5eAaImAgNnS
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Nejnovější	nový	k2eAgInSc1d3
díl	díl	k1gInSc1
s	s	k7c7
podtitulem	podtitul	k1gInSc7
Modern	Moderna	k1gFnPc2
Warfare	Warfar	k1gMnSc5
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
však	však	k9
nenavazuje	navazovat	k5eNaImIp3nS
na	na	k7c4
předešlé	předešlý	k2eAgFnPc4d1
hry	hra	k1gFnPc4
Modern	Moderna	k1gFnPc2
Warfare	Warfar	k1gMnSc5
<g/>
,	,	kIx,
Modern	Moderna	k1gFnPc2
Warfare	Warfar	k1gMnSc5
2	#num#	k4
a	a	k8xC
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Konzole	konzola	k1gFnSc3
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Finest	Finest	k1gMnSc1
Hour	Hour	k1gMnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Finest	Finest	k1gMnSc1
Hour	Hour	k1gMnSc1
je	být	k5eAaImIp3nS
první	první	k4xOgInSc4
konzolový	konzolový	k2eAgInSc4d1
dodatek	dodatek	k1gInSc4
hry	hra	k1gFnSc2
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
nové	nový	k2eAgFnPc4d1
herní	herní	k2eAgFnPc4d1
módy	móda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
2	#num#	k4
<g/>
:	:	kIx,
Big	Big	k1gMnSc1
Red	Red	k1gMnSc1
One	One	k1gMnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
2	#num#	k4
<g/>
:	:	kIx,
Big	Big	k1gMnSc1
Red	Red	k1gMnSc1
One	One	k1gMnSc1
je	být	k5eAaImIp3nS
spin-off	spin-off	k1gMnSc1
hry	hra	k1gFnSc2
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
–	–	k?
Final	Final	k1gInSc1
Fronts	Fronts	k1gInSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
–	–	k?
Final	Final	k1gInSc1
Fronts	Frontsa	k1gFnPc2
je	být	k5eAaImIp3nS
port	port	k1gInSc1
hry	hra	k1gFnSc2
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
pro	pro	k7c4
PlayStation	PlayStation	k1gInSc4
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gMnSc1
Collection	Collection	k1gInSc4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
The	The	k1gMnSc1
War	War	k1gFnSc2
Collection	Collection	k1gInSc1
je	být	k5eAaImIp3nS
boxový	boxový	k2eAgInSc1d1
komplet	komplet	k1gInSc1
her	hra	k1gFnPc2
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
2	#num#	k4
<g/>
,	,	kIx,
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
3	#num#	k4
a	a	k8xC
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kapesní	kapesní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
a	a	k8xC
mobily	mobil	k1gInPc4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Roads	Roads	k1gInSc1
to	ten	k3xDgNnSc1
Victory	Victor	k1gMnPc4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Roads	Roads	k1gInSc1
to	ten	k3xDgNnSc1
Victory	Victor	k1gMnPc4
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
pro	pro	k7c4
PlayStationPortable	PlayStationPortable	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
<g/>
:	:	kIx,
Mobilized	Mobilized	k1gInSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
<g/>
:	:	kIx,
Mobilized	Mobilized	k1gInSc1
is	is	k?
the	the	k?
Nintendo	Nintendo	k1gNnSc1
DS	DS	kA
companion	companion	k1gInSc1
game	game	k1gInSc1
for	forum	k1gNnPc2
Modern	Moderna	k1gFnPc2
Warfare	Warfar	k1gMnSc5
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
DS	DS	kA
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
DS	DS	kA
je	být	k5eAaImIp3nS
port	port	k1gInSc1
hry	hra	k1gFnSc2
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
na	na	k7c4
Nintendo	Nintendo	k1gNnSc4
DS	DS	kA
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Zombies	Zombies	k1gMnSc1
and	and	k?
Zombies	Zombies	k1gMnSc1
2	#num#	k4
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Zombies	Zombies	k1gMnSc1
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
z	z	k7c2
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
<g/>
:	:	kIx,
Declassified	Declassified	k1gMnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
<g/>
:	:	kIx,
Declassified	Declassified	k1gMnSc1
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
na	na	k7c4
PlayStation	PlayStation	k1gInSc4
Vitu	vit	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Strike	Strike	k1gFnSc1
Team	team	k1gInSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Strike	Strik	k1gMnSc2
Team	team	k1gInSc1
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
pro	pro	k7c4
Android	android	k1gInSc4
a	a	k8xC
iOS	iOS	k?
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Heroes	Heroes	k1gMnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Heroes	Heroesa	k1gFnPc2
je	být	k5eAaImIp3nS
strategická	strategický	k2eAgFnSc1d1
hra	hra	k1gFnSc1
od	od	k7c2
studia	studio	k1gNnSc2
Faceroll	Faceroll	k1gMnSc1
Games	Games	k1gMnSc1
na	na	k7c4
Android	android	k1gInSc4
a	a	k8xC
iOS	iOS	k?
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
Online	Onlin	k1gMnSc5
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
Online	Onlin	k1gInSc5
je	on	k3xPp3gFnPc4
F2P	F2P	k1gFnSc1
hra	hra	k1gFnSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Mobile	mobile	k1gNnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Mobile	mobile	k1gNnSc1
je	být	k5eAaImIp3nS
hra	hra	k1gFnSc1
od	od	k7c2
Timi	Tim	k1gFnSc2
Studio	studio	k1gNnSc1
Group	Group	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
na	na	k7c4
Android	android	k1gInSc4
a	a	k8xC
iOS	iOS	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zrušené	zrušený	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Combined	Combined	k1gMnSc1
Forces	Forces	k1gMnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Combined	Combined	k1gInSc1
Forces	Forces	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
sequelem	sequel	k1gInSc7
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Finest	Finest	k1gMnSc1
Hour	Hour	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
problémům	problém	k1gInPc3
mezi	mezi	k7c4
Spark	Spark	k1gInSc4
Unlimited	Unlimited	k1gInSc1
<g/>
,	,	kIx,
Electronic	Electronice	k1gFnPc2
Arts	Artsa	k1gFnPc2
a	a	k8xC
Activision	Activision	k1gInSc1
se	se	k3xPyFc4
hru	hra	k1gFnSc4
nepodařilo	podařit	k5eNaPmAgNnS
dokončit	dokončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Devil	Devil	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Brigade	Brigad	k1gInSc5
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Devil	Devil	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Brigade	Brigad	k1gInSc5
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
střílečka	střílečka	k1gFnSc1
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
zasazena	zasadit	k5eAaPmNgFnS
ve	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mělo	mít	k5eAaImAgNnS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
výhradně	výhradně	k6eAd1
o	o	k7c4
italskou	italský	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Vietnam	Vietnam	k1gInSc4
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
střílečka	střílečka	k1gFnSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
třetí	třetí	k4xOgFnSc2
osoby	osoba	k1gFnSc2
zasazena	zasadit	k5eAaPmNgFnS
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývoj	vývoj	k1gInSc1
byl	být	k5eAaImAgInS
zastaven	zastavit	k5eAaPmNgInS
z	z	k7c2
důvodu	důvod	k1gInSc2
dokončení	dokončení	k1gNnSc2
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Roman	Roman	k1gMnSc1
Wars	Warsa	k1gFnPc2
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Roman	Roman	k1gMnSc1
Wars	Wars	k1gInSc4
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
střílečka	střílečka	k1gFnSc1
z	z	k7c2
pohledu	pohled	k1gInSc2
třetí	třetí	k4xOgFnSc2
a	a	k8xC
první	první	k4xOgFnSc2
osoby	osoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
se	se	k3xPyFc4
odehrávala	odehrávat	k5eAaImAgFnS
v	v	k7c6
antickém	antický	k2eAgInSc6d1
Římě	Řím	k1gInSc6
a	a	k8xC
měla	mít	k5eAaImAgFnS
sledovat	sledovat	k5eAaImF
průběh	průběh	k1gInSc4
vlády	vláda	k1gFnSc2
Julia	Julius	k1gMnSc2
Caesara	Caesar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CAMPBELL	CAMPBELL	kA
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
WWII	WWII	kA
confirmed	confirmed	k1gMnSc1
<g/>
,	,	kIx,
full	full	k1gMnSc1
reveal	reveal	k1gMnSc1
next	next	k1gMnSc1
week	week	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polygon	polygon	k1gInSc1
<g/>
,	,	kIx,
2017-04-21	2017-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARTINDALE	MARTINDALE	kA
<g/>
,	,	kIx,
Jon	Jon	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Watch	Watch	k1gMnSc1
the	the	k?
just-unveiled	just-unveiled	k1gMnSc1
'	'	kIx"
<g/>
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
WWII	WWII	kA
<g/>
'	'	kIx"
trailer	trailer	k1gMnSc1
right	right	k1gMnSc1
here	herat	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digital	Digital	kA
Trends	Trendsa	k1gFnPc2
<g/>
,	,	kIx,
2017-04-26	2017-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TAKAHASHI	TAKAHASHI	kA
<g/>
,	,	kIx,
Dean	Dean	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
4	#num#	k4
is	is	k?
coming	coming	k1gInSc1
from	from	k1gMnSc1
Treyarch	Treyarcha	k1gFnPc2
this	thisa	k1gFnPc2
year	year	k1gInSc1
<g/>
,	,	kIx,
Activision	Activision	k1gInSc1
confirms	confirms	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venture	Ventur	k1gMnSc5
Beat	beat	k1gInSc1
<g/>
,	,	kIx,
2018-03-08	2018-03-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglick	anglick	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
Střílečka	střílečka	k1gFnSc1
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Mobile	mobile	k1gNnPc6
vyšla	vyjít	k5eAaPmAgFnS
na	na	k7c6
iOS	iOS	k?
a	a	k8xC
Android	android	k1gInSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-10-01	2019-10-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Odpočet	odpočet	k1gInSc1
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
WWII	WWII	kA
na	na	k7c6
Oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
</s>
<s>
Recenze	recenze	k1gFnSc1
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Ghosts	Ghosts	k1gInSc1
na	na	k7c4
Vytukej	Vytukej	k1gFnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Recenze	recenze	k1gFnSc1
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Advanced	Advanced	k1gMnSc1
Warfare	Warfar	k1gMnSc5
na	na	k7c6
Vytukej	Vytukej	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Recenze	recenze	k1gFnSc1
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Black	Black	k1gMnSc1
Ops	Ops	k1gFnSc2
2	#num#	k4
na	na	k7c6
Vytukej	Vytukej	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Recenze	recenze	k1gFnSc1
Call	Calla	k1gFnPc2
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
2	#num#	k4
na	na	k7c6
Vytukej	Vytukej	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
Hlavní	hlavní	k2eAgFnPc4d1
hry	hra	k1gFnPc4
</s>
<s>
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
(	(	kIx(
<g/>
United	United	k1gMnSc1
Offensive	Offensiev	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Call	Call	k1gInSc1
of	of	k?
Duty	Duty	k?
2	#num#	k4
•	•	k?
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
3	#num#	k4
•	•	k?
Call	Call	k1gMnSc1
of	of	k?
Duty	Duty	k?
<g/>
:	:	kIx,
WWII	WWII	kA
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gInSc5
</s>
<s>
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
•	•	k?
Modern	Moderna	k1gFnPc2
Warfare	Warfar	k1gMnSc5
2	#num#	k4
•	•	k?
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
3	#num#	k4
•	•	k?
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
</s>
<s>
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
•	•	k?
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
•	•	k?
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
II	II	kA
•	•	k?
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
III	III	kA
•	•	k?
Black	Black	k1gInSc1
Ops	Ops	k1gFnSc1
4	#num#	k4
•	•	k?
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
Cold	Cold	k1gMnSc1
War	War	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
Ghosts	Ghosts	k1gInSc1
•	•	k?
Advanced	Advanced	k1gInSc1
Warfare	Warfar	k1gMnSc5
•	•	k?
Infinite	Infinit	k1gInSc5
Warfare	Warfar	k1gInSc5
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
Konzolové	konzolový	k2eAgNnSc1d1
</s>
<s>
Finest	Finest	k1gMnSc1
Hour	Hour	k1gMnSc1
•	•	k?
Big	Big	k1gMnSc1
Red	Red	k1gMnSc1
One	One	k1gMnSc1
•	•	k?
World	World	k1gMnSc1
at	at	k?
War	War	k1gMnSc1
-	-	kIx~
Final	Final	k1gInSc1
Fronts	Fronts	k1gInSc1
Mobilní	mobilní	k2eAgInPc1d1
</s>
<s>
Roads	Roads	k6eAd1
to	ten	k3xDgNnSc1
Victory	Victor	k1gMnPc4
•	•	k?
Modern	Moderna	k1gFnPc2
Warfare	Warfar	k1gMnSc5
-	-	kIx~
Mobilized	Mobilized	k1gMnSc1
•	•	k?
Zombies	Zombies	k1gMnSc1
•	•	k?
Black	Black	k1gMnSc1
Ops	Ops	k1gMnSc1
-	-	kIx~
Declassified	Declassified	k1gMnSc1
•	•	k?
Strike	Strike	k1gInSc1
Team	team	k1gInSc1
•	•	k?
Heroes	Heroes	k1gInSc1
•	•	k?
Mobile	mobile	k1gNnSc7
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
</s>
<s>
Infinity	Infinit	k1gInPc1
Ward	Warda	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
•	•	k?
Treyarch	Treyarch	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
•	•	k?
Sledgehammer	Sledgehammer	k1gMnSc1
Games	Games	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
Související	související	k2eAgFnSc1d1
</s>
<s>
Modern	Modern	k1gMnSc1
Warfare	Warfar	k1gMnSc5
2	#num#	k4
<g/>
:	:	kIx,
Ghost	Ghost	k1gMnSc1
•	•	k?
Kapitán	kapitán	k1gMnSc1
Price	Price	k1gMnSc1
•	•	k?
Soap	Soap	k1gMnSc1
MacTavish	MacTavish	k1gMnSc1
•	•	k?
id	ido	k1gNnPc2
Tech	Tech	k?
3	#num#	k4
•	•	k?
IW	IW	kA
engine	enginout	k5eAaPmIp3nS
kategorie	kategorie	k1gFnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
