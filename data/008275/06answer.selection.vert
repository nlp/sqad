<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
při	při	k7c6	při
aditivním	aditivní	k2eAgNnSc6d1	aditivní
míchání	míchání	k1gNnSc6	míchání
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
RGB	RGB	kA	RGB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
doplňkem	doplněk	k1gInSc7	doplněk
je	být	k5eAaImIp3nS	být
fialová	fialový	k2eAgFnSc1d1	fialová
<g/>
.	.	kIx.	.
</s>
