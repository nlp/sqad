<s>
Thrash	Thrash	k1gInSc1	Thrash
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
odnoží	odnoží	k1gNnSc1	odnoží
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
je	být	k5eAaImIp3nS	být
především	především	k9	především
agresivnější	agresivní	k2eAgInSc1d2	agresivnější
zvuk	zvuk	k1gInSc1	zvuk
kytary	kytara	k1gFnSc2	kytara
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
dodávání	dodávání	k1gNnSc2	dodávání
hardcore	hardcor	k1gInSc5	hardcor
punkových	punkový	k2eAgMnPc6d1	punkový
vlivů	vliv	k1gInPc2	vliv
do	do	k7c2	do
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vystopovat	vystopovat	k5eAaPmF	vystopovat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
skupin	skupina	k1gFnPc2	skupina
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
NWOBHM	NWOBHM	kA	NWOBHM
a	a	k8xC	a
prvky	prvek	k1gInPc1	prvek
hardcore	hardcor	k1gMnSc5	hardcor
punku	punk	k1gMnSc5	punk
(	(	kIx(	(
<g/>
nezvyklá	zvyklý	k2eNgNnPc1d1	nezvyklé
tempa	tempo	k1gNnPc1	tempo
bicích	bicí	k2eAgFnPc2d1	bicí
<g/>
)	)	kIx)	)
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
nejbližšího	blízký	k2eAgMnSc2d3	nejbližší
příbuzného	příbuzný	k1gMnSc2	příbuzný
<g/>
,	,	kIx,	,
speed	speed	k1gInSc4	speed
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
o	o	k7c6	o
dost	dost	k6eAd1	dost
agresivnější	agresivní	k2eAgFnSc6d2	agresivnější
a	a	k8xC	a
spontánnější	spontánní	k2eAgFnSc6d2	spontánnější
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
významný	významný	k2eAgInSc4d1	významný
přechod	přechod	k1gInSc4	přechod
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
metalové	metalový	k2eAgFnSc2d1	metalová
kategorie	kategorie	k1gFnSc2	kategorie
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
i	i	k9	i
jinými	jiný	k2eAgInPc7d1	jiný
styly	styl	k1gInPc7	styl
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
klasickou	klasický	k2eAgFnSc7d1	klasická
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
jazzem	jazz	k1gInSc7	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Thrash	Thrash	k1gInSc1	Thrash
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
rychlém	rychlý	k2eAgNnSc6d1	rychlé
tempu	tempo	k1gNnSc6	tempo
<g/>
,	,	kIx,	,
rychlých	rychlý	k2eAgInPc6d1	rychlý
či	či	k8xC	či
komplikovaných	komplikovaný	k2eAgInPc6d1	komplikovaný
rytmicky	rytmicky	k6eAd1	rytmicky
sekaných	sekaný	k2eAgInPc6d1	sekaný
kytarových	kytarový	k2eAgInPc6d1	kytarový
riffech	riff	k1gInPc6	riff
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
kytarových	kytarový	k2eAgNnPc2d1	kytarové
sól	sólo	k1gNnPc2	sólo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
členitá	členitý	k2eAgFnSc1d1	členitá
rytmická	rytmický	k2eAgFnSc1d1	rytmická
struktura	struktura	k1gFnSc1	struktura
skladeb	skladba	k1gFnPc2	skladba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
běžně	běžně	k6eAd1	běžně
definuje	definovat	k5eAaBmIp3nS	definovat
thrash	thrash	k1gInSc1	thrash
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
rychlým	rychlý	k2eAgNnSc7d1	rychlé
<g/>
,	,	kIx,	,
intenzívním	intenzívní	k2eAgNnSc7d1	intenzívní
bubnovaním	bubnovaní	k1gNnSc7	bubnovaní
(	(	kIx(	(
<g/>
thrashoví	thrashový	k2eAgMnPc1d1	thrashový
bubeníci	bubeník	k1gMnPc1	bubeník
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
dvojpedálové	dvojpedálový	k2eAgInPc4d1	dvojpedálový
bicí	bicí	k2eAgInPc4d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
thrash	thrash	k1gInSc1	thrash
metal	metal	k1gInSc1	metal
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
následně	následně	k6eAd1	následně
začala	začít	k5eAaPmAgFnS	začít
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Kapely	kapela	k1gFnPc1	kapela
co	co	k9	co
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
věrné	věrný	k2eAgFnPc1d1	věrná
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
nové	nový	k2eAgFnPc1d1	nová
kapely	kapela	k1gFnPc1	kapela
a	a	k8xC	a
obrovská	obrovský	k2eAgFnSc1d1	obrovská
masa	masa	k1gFnSc1	masa
příznivců	příznivec	k1gMnPc2	příznivec
(	(	kIx(	(
<g/>
známí	známý	k1gMnPc1	známý
jako	jako	k8xS	jako
thrasheri	thrasher	k1gMnPc1	thrasher
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc4	on
udržují	udržovat	k5eAaImIp3nP	udržovat
při	při	k7c6	při
životě	život	k1gInSc6	život
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
termín	termín	k1gInSc1	termín
thrash	thrash	k1gInSc1	thrash
metal	metat	k5eAaImAgInS	metat
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
recenzi	recenze	k1gFnSc6	recenze
na	na	k7c4	na
album	album	k1gNnSc4	album
Spreading	Spreading	k1gInSc4	Spreading
the	the	k?	the
Disease	Diseas	k1gInSc6	Diseas
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
Anthrax	Anthrax	k1gInSc1	Anthrax
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
Anthrax	Anthrax	k1gInSc4	Anthrax
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
jako	jako	k8xC	jako
Slayer	Slayer	k1gInSc1	Slayer
<g/>
,	,	kIx,	,
Exodus	Exodus	k1gInSc1	Exodus
a	a	k8xC	a
Testament	testament	k1gInSc1	testament
<g/>
,	,	kIx,	,
Metallica	Metallica	k1gFnSc1	Metallica
a	a	k8xC	a
Megadeth	Megadetha	k1gFnPc2	Megadetha
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
průkopníky	průkopník	k1gMnPc4	průkopník
thrash	thrash	k1gInSc1	thrash
metalu	metal	k1gInSc6	metal
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
vůbec	vůbec	k9	vůbec
nejklasičtější	klasický	k2eAgNnPc1d3	nejklasičtější
alba	album	k1gNnPc1	album
jsou	být	k5eAaImIp3nP	být
všeobecně	všeobecně	k6eAd1	všeobecně
považována	považován	k2eAgFnSc1d1	považována
hlavně	hlavně	k9	hlavně
Reign	Reign	k1gInSc4	Reign
in	in	k?	in
Blood	Blood	k1gInSc1	Blood
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
od	od	k7c2	od
Slayer	Slayra	k1gFnPc2	Slayra
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppets	k1gInSc1	Puppets
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
od	od	k7c2	od
Metallicy	Metallica	k1gFnSc2	Metallica
<g/>
,	,	kIx,	,
deska	deska	k1gFnSc1	deska
Among	Among	k1gInSc1	Among
The	The	k1gMnSc1	The
Living	Living	k1gInSc1	Living
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
od	od	k7c2	od
Anthrax	Anthrax	k1gInSc1	Anthrax
<g/>
,	,	kIx,	,
debut	debut	k1gInSc1	debut
Bonded	Bonded	k1gInSc1	Bonded
by	by	kYmCp3nS	by
Blood	Blood	k1gInSc1	Blood
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
od	od	k7c2	od
Exodus	Exodus	k1gInSc1	Exodus
a	a	k8xC	a
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
hlavně	hlavně	k9	hlavně
Eternal	Eternal	k1gFnPc1	Eternal
Devastation	Devastation	k1gInSc1	Devastation
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
od	od	k7c2	od
Destruction	Destruction	k1gInSc1	Destruction
nebo	nebo	k8xC	nebo
Pleasure	Pleasur	k1gMnSc5	Pleasur
to	ten	k3xDgNnSc4	ten
Kill	Kill	k1gMnSc1	Kill
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
od	od	k7c2	od
Kreator	Kreator	k1gInSc4	Kreator
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
evropské	evropský	k2eAgFnPc1d1	Evropská
jsou	být	k5eAaImIp3nP	být
Destruction	Destruction	k1gInSc1	Destruction
<g/>
,	,	kIx,	,
Kreator	Kreator	k1gInSc1	Kreator
a	a	k8xC	a
Sodom	Sodoma	k1gFnPc2	Sodoma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dodnes	dodnes	k6eAd1	dodnes
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgFnPc7d3	nejznámější
českými	český	k2eAgFnPc7d1	Česká
thrashmetalovými	thrashmetalový	k2eAgFnPc7d1	thrashmetalová
kapelami	kapela	k1gFnPc7	kapela
jsou	být	k5eAaImIp3nP	být
Arakain	Arakain	k1gInSc4	Arakain
a	a	k8xC	a
Debustrol	Debustrol	k1gInSc4	Debustrol
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
fanoušků	fanoušek	k1gMnPc2	fanoušek
historie	historie	k1gFnSc2	historie
thrash	thrash	k1gInSc1	thrash
metalu	metal	k1gInSc2	metal
začíná	začínat	k5eAaImIp3nS	začínat
psát	psát	k5eAaImF	psát
až	až	k6eAd1	až
rokem	rok	k1gInSc7	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
skutečné	skutečný	k2eAgInPc1d1	skutečný
počátky	počátek	k1gInPc1	počátek
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
dřív	dříve	k6eAd2	dříve
<g/>
:	:	kIx,	:
první	první	k4xOgMnSc1	první
riff	riff	k1gMnSc1	riff
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Symptom	symptom	k1gInSc1	symptom
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc1	Universe
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
legendárních	legendární	k2eAgFnPc2d1	legendární
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbath	k1gMnSc1	Sabbath
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
thrashových	thrashův	k2eAgInPc2d1	thrashův
riffů	riff	k1gInPc2	riff
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jejich	jejich	k3xOp3gFnPc4	jejich
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Into	Into	k6eAd1	Into
the	the	k?	the
Void	Void	k1gInSc1	Void
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Children	Childrno	k1gNnPc2	Childrno
of	of	k?	of
the	the	k?	the
Grave	grave	k1gNnSc2	grave
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgFnP	mít
také	také	k9	také
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
tohoto	tento	k3xDgInSc2	tento
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
znalci	znalec	k1gMnPc1	znalec
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Stone	ston	k1gInSc5	ston
Cold	Cold	k1gInSc4	Cold
Crazy	Craza	k1gFnPc4	Craza
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
od	od	k7c2	od
anglických	anglický	k2eAgFnPc2d1	anglická
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
neobyčejně	obyčejně	k6eNd1	obyčejně
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
a	a	k8xC	a
rychlá	rychlý	k2eAgFnSc1d1	rychlá
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
repertoáru	repertoár	k1gInSc2	repertoár
zařadila	zařadit	k5eAaPmAgFnS	zařadit
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc1d3	nejznámější
dříve	dříve	k6eAd2	dříve
thrashová	thrashový	k2eAgFnSc1d1	thrashová
skupina	skupina	k1gFnSc1	skupina
Metallica	Metallica	k1gFnSc1	Metallica
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
této	tento	k3xDgFnSc6	tento
byla	být	k5eAaImAgFnS	být
i	i	k9	i
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Modern	Modern	k1gInSc1	Modern
Times	Times	k1gInSc1	Times
Rock	rock	k1gInSc1	rock
And	Anda	k1gFnPc2	Anda
Roll	Rolla	k1gFnPc2	Rolla
<g/>
"	"	kIx"	"
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
Queen	Quena	k1gFnPc2	Quena
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejrychlejší	rychlý	k2eAgFnPc1d3	nejrychlejší
ukázky	ukázka	k1gFnPc1	ukázka
rané	raný	k2eAgFnSc2d1	raná
thrashové	thrashový	k2eAgFnSc2d1	thrashová
muziky	muzika	k1gFnSc2	muzika
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
německá	německý	k2eAgFnSc1d1	německá
progresivní	progresivní	k2eAgFnSc1d1	progresivní
parta	parta	k1gFnSc1	parta
Night	Night	k1gMnSc1	Night
Sun	Sun	kA	Sun
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
by	by	kYmCp3nP	by
na	na	k7c4	na
škodu	škoda	k1gFnSc4	škoda
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
také	také	k9	také
Iggyho	Iggy	k1gMnSc4	Iggy
Popa	pop	k1gMnSc4	pop
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
kapelu	kapela	k1gFnSc4	kapela
The	The	k1gMnSc1	The
Stooges	Stooges	k1gMnSc1	Stooges
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
svými	svůj	k3xOyFgFnPc7	svůj
skladbami	skladba	k1gFnPc7	skladba
"	"	kIx"	"
<g/>
I	i	k9	i
Got	Got	k1gMnSc1	Got
a	a	k8xC	a
Right	Right	k1gMnSc1	Right
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Gimme	Gimm	k1gMnSc5	Gimm
Some	Somus	k1gMnSc5	Somus
Skin	skin	k1gMnSc1	skin
<g/>
"	"	kIx"	"
hluboce	hluboko	k6eAd1	hluboko
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
Motörhead	Motörhead	k1gInSc4	Motörhead
<g/>
.	.	kIx.	.
</s>
<s>
Newyorská	newyorský	k2eAgFnSc1d1	newyorská
kapela	kapela	k1gFnSc1	kapela
Overkill	Overkilla	k1gFnPc2	Overkilla
(	(	kIx(	(
<g/>
název	název	k1gInSc4	název
si	se	k3xPyFc3	se
zvolili	zvolit	k5eAaPmAgMnP	zvolit
podle	podle	k7c2	podle
alba	album	k1gNnSc2	album
Motörhead	Motörhead	k1gInSc1	Motörhead
Overkill	Overkillum	k1gNnPc2	Overkillum
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
napsala	napsat	k5eAaBmAgFnS	napsat
song	song	k1gInSc1	song
"	"	kIx"	"
<g/>
Unleash	Unleash	k1gMnSc1	Unleash
the	the	k?	the
Beast	Beast	k1gMnSc1	Beast
Within	Within	k1gMnSc1	Within
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
první	první	k4xOgFnSc4	první
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
thrashovou	thrashový	k2eAgFnSc4d1	thrashová
skladbu	skladba	k1gFnSc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
kapela	kapela	k1gFnSc1	kapela
Leather	Leathra	k1gFnPc2	Leathra
Charm	charm	k1gInSc4	charm
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
působil	působit	k5eAaImAgMnS	působit
James	James	k1gMnSc1	James
Hetfield	Hetfield	k1gMnSc1	Hetfield
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaPmAgFnS	napsat
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Hit	hit	k1gInSc1	hit
the	the	k?	the
Lights	Lights	k1gInSc1	Lights
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
ji	on	k3xPp3gFnSc4	on
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
repertoáru	repertoár	k1gInSc2	repertoár
své	svůj	k3xOyFgFnSc2	svůj
další	další	k2eAgFnSc2d1	další
skupiny	skupina	k1gFnSc2	skupina
Metallica	Metallic	k1gInSc2	Metallic
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Metal	metal	k1gInSc1	metal
Church	Church	k1gInSc1	Church
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980-81	[number]	k4	1980-81
vyprodukovala	vyprodukovat	k5eAaPmAgFnS	vyprodukovat
několik	několik	k4yIc4	několik
protothrashmetalových	protothrashmetalův	k2eAgFnPc2d1	protothrashmetalův
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
podobných	podobný	k2eAgFnPc2d1	podobná
pokusům	pokus	k1gInPc3	pokus
rané	raný	k2eAgFnSc2d1	raná
Metallicy	Metallica	k1gFnSc2	Metallica
a	a	k8xC	a
Overkillu	Overkill	k1gInSc2	Overkill
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
thrash-metalové	thrashetal	k1gMnPc1	thrash-metal
demo	demo	k2eAgMnPc1d1	demo
nahráli	nahrát	k5eAaBmAgMnP	nahrát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Metal	metal	k1gInSc4	metal
Church	Churcha	k1gFnPc2	Churcha
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Red	Red	k1gMnSc1	Red
Skies	Skies	k1gMnSc1	Skies
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
instrumentální	instrumentální	k2eAgNnSc1d1	instrumentální
demo	demo	k2eAgNnSc1d1	demo
nezískalo	získat	k5eNaPmAgNnS	získat
většího	veliký	k2eAgInSc2d2	veliký
ohlasu	ohlas	k1gInSc2	ohlas
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jejich	jejich	k3xOp3gNnSc2	jejich
dalšího	další	k2eAgNnSc2d1	další
dema	demum	k1gNnSc2	demum
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
1982	[number]	k4	1982
Four	Foura	k1gFnPc2	Foura
Hymns	Hymnsa	k1gFnPc2	Hymnsa
<g/>
.	.	kIx.	.
</s>
<s>
Metallica	Metallica	k1gFnSc1	Metallica
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
thrashovou	thrashový	k2eAgFnSc4d1	thrashová
scénu	scéna	k1gFnSc4	scéna
jako	jako	k8xS	jako
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
demo	demo	k2eAgInSc1d1	demo
Power	Power	k1gInSc1	Power
Metal	metat	k5eAaImAgInS	metat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
No	no	k9	no
Life	Life	k1gFnSc1	Life
'	'	kIx"	'
<g/>
til	til	k1gInSc1	til
Leather	Leathra	k1gFnPc2	Leathra
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgFnSc4	první
se	s	k7c7	s
studiovým	studiový	k2eAgNnSc7d1	studiové
albem	album	k1gNnSc7	album
(	(	kIx(	(
<g/>
Kill	Killum	k1gNnPc2	Killum
'	'	kIx"	'
<g/>
Em	Ema	k1gFnPc2	Ema
All	All	k1gFnSc2	All
<g/>
,	,	kIx,	,
července	červenec	k1gInSc2	červenec
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
mezitím	mezitím	k6eAd1	mezitím
Artillery	Artiller	k1gInPc4	Artiller
nahráli	nahrát	k5eAaPmAgMnP	nahrát
demo	demo	k2eAgMnPc1d1	demo
We	We	k1gMnPc1	We
Are	ar	k1gInSc5	ar
the	the	k?	the
Dead	Deado	k1gNnPc2	Deado
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
tvorbou	tvorba	k1gFnSc7	tvorba
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
;	;	kIx,	;
nebylo	být	k5eNaImAgNnS	být
proto	proto	k8xC	proto
tak	tak	k6eAd1	tak
rychlé	rychlý	k2eAgInPc4d1	rychlý
jako	jako	k8xS	jako
americký	americký	k2eAgInSc4d1	americký
thrash	thrash	k1gInSc4	thrash
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
mělo	mít	k5eAaImAgNnS	mít
podobné	podobný	k2eAgInPc4d1	podobný
riffy	riff	k1gInPc4	riff
jako	jako	k8xC	jako
Metallica	Metallicum	k1gNnPc4	Metallicum
<g/>
.	.	kIx.	.
</s>
<s>
Thrash	Thrash	k1gInSc1	Thrash
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
vynořil	vynořit	k5eAaPmAgInS	vynořit
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
když	když	k8xS	když
Anthrax	Anthrax	k1gInSc4	Anthrax
přišli	přijít	k5eAaPmAgMnP	přijít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
hymnou	hymna	k1gFnSc7	hymna
"	"	kIx"	"
<g/>
Metal	metal	k1gInSc1	metal
Thrashing	Thrashing	k1gInSc1	Thrashing
Mad	Mad	k1gFnSc1	Mad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Overkill	Overkill	k1gMnSc1	Overkill
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
demo	demo	k2eAgFnSc2d1	demo
Feel	Feel	k1gInSc4	Feel
the	the	k?	the
Fire	Fire	k1gFnPc2	Fire
a	a	k8xC	a
Slayer	Slayra	k1gFnPc2	Slayra
nahráli	nahrát	k5eAaPmAgMnP	nahrát
směrodatné	směrodatný	k2eAgFnSc2d1	směrodatná
Haunting	Haunting	k1gInSc4	Haunting
the	the	k?	the
Chapel	Chapel	k1gInSc1	Chapel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
temnějšímu	temný	k2eAgNnSc3d2	temnější
a	a	k8xC	a
tvrdšímu	tvrdý	k2eAgNnSc3d2	tvrdší
thrashi	thrashi	k1gNnSc3	thrashi
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
na	na	k7c6	na
albech	album	k1gNnPc6	album
Exodus	Exodus	k1gInSc4	Exodus
(	(	kIx(	(
<g/>
Bonded	Bonded	k1gInSc1	Bonded
by	by	kYmCp3nS	by
Blood	Blood	k1gInSc1	Blood
<g/>
)	)	kIx)	)
a	a	k8xC	a
Slayer	Slayer	k1gInSc1	Slayer
(	(	kIx(	(
<g/>
Hell	Hell	k1gInSc1	Hell
Awaits	Awaits	k1gInSc1	Awaits
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Destruction	Destruction	k1gInSc4	Destruction
vydali	vydat	k5eAaPmAgMnP	vydat
svůj	svůj	k3xOyFgInSc4	svůj
debut	debut	k1gInSc4	debut
Infernal	Infernal	k1gMnSc1	Infernal
Overkill	Overkill	k1gMnSc1	Overkill
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
Eudoxis	Eudoxis	k1gFnSc2	Eudoxis
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
vystupováním	vystupování	k1gNnSc7	vystupování
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
,	,	kIx,	,
kovových	kovový	k2eAgInPc2d1	kovový
cvokách	cvokách	k?	cvokách
a	a	k8xC	a
s	s	k7c7	s
bicími	bicí	k2eAgInPc7d1	bicí
z	z	k7c2	z
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
vynesli	vynést	k5eAaPmAgMnP	vynést
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
demo	demo	k2eAgInSc1d1	demo
Metal	metal	k1gInSc1	metal
Fix	fix	k1gInSc1	fix
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Megadeth	Megadeth	k1gInSc1	Megadeth
<g/>
,	,	kIx,	,
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zformoval	zformovat	k5eAaPmAgMnS	zformovat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
Metallicy	Metallica	k1gFnSc2	Metallica
Dave	Dav	k1gInSc5	Dav
Mustaine	Mustain	k1gInSc5	Mustain
<g/>
,	,	kIx,	,
debutovali	debutovat	k5eAaBmAgMnP	debutovat
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
nahrávkou	nahrávka	k1gFnSc7	nahrávka
Killing	Killing	k1gInSc4	Killing
Is	Is	k1gMnSc1	Is
My	my	k3xPp1nPc1	my
Business	business	k1gInSc1	business
<g/>
...	...	k?	...
and	and	k?	and
Business	business	k1gInSc1	business
Is	Is	k1gMnSc1	Is
Good	Good	k1gMnSc1	Good
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Megadeth	Megadeth	k1gInSc4	Megadeth
zkombinovali	zkombinovat	k5eAaPmAgMnP	zkombinovat
thrash-metalové	thrashetal	k1gMnPc1	thrash-metal
riffy	riff	k1gMnPc7	riff
se	se	k3xPyFc4	se
sóly	sólo	k1gNnPc7	sólo
podobné	podobný	k2eAgInPc4d1	podobný
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gInSc1	Priest
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
koncept	koncept	k1gInSc4	koncept
nejlépe	dobře	k6eAd3	dobře
zrealizovali	zrealizovat	k5eAaPmAgMnP	zrealizovat
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
na	na	k7c6	na
nahrávce	nahrávka	k1gFnSc6	nahrávka
Rust	Rust	k1gMnSc1	Rust
in	in	k?	in
Peace	Peace	k1gMnSc1	Peace
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
milníkem	milník	k1gInSc7	milník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
rok	rok	k1gInSc1	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
thrash-metalových	thrashetalův	k2eAgFnPc2d1	thrash-metalův
alb	alba	k1gFnPc2	alba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dark	Dark	k1gMnSc1	Dark
Angel	angel	k1gMnSc1	angel
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
podceňované	podceňovaný	k2eAgNnSc4d1	podceňované
album	album	k1gNnSc4	album
Darkness	Darknessa	k1gFnPc2	Darknessa
Descends	Descendsa	k1gFnPc2	Descendsa
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejtvrdších	tvrdý	k2eAgNnPc2d3	nejtvrdší
a	a	k8xC	a
nejrychlejších	rychlý	k2eAgNnPc2d3	nejrychlejší
thrash-metalových	thrashetalův	k2eAgNnPc2d1	thrash-metalův
alb	album	k1gNnPc2	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Slayer	Slayer	k1gInSc4	Slayer
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
klasickým	klasický	k2eAgInSc7d1	klasický
Reign	Reign	k1gNnSc4	Reign
in	in	k?	in
Blood	Blood	k1gInSc1	Blood
a	a	k8xC	a
němečtí	německý	k2eAgMnPc1d1	německý
thrasheři	thrasher	k1gMnPc1	thrasher
Kreator	Kreator	k1gInSc4	Kreator
vydali	vydat	k5eAaPmAgMnP	vydat
brutální	brutální	k2eAgInSc4d1	brutální
výtvor	výtvor	k1gInSc4	výtvor
Pleasure	Pleasur	k1gMnSc5	Pleasur
to	ten	k3xDgNnSc1	ten
Kill	Kill	k1gInSc4	Kill
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
death	death	k1gInSc4	death
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Megadeth	Megadeth	k1gInSc4	Megadeth
vydali	vydat	k5eAaPmAgMnP	vydat
desku	deska	k1gFnSc4	deska
Peace	Peaec	k1gInSc2	Peaec
Sells	Sells	k1gInSc1	Sells
<g/>
...	...	k?	...
But	But	k1gMnSc1	But
Who	Who	k1gMnSc1	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Buying	Buying	k1gInSc1	Buying
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Metallica	Metallic	k2eAgFnSc1d1	Metallica
své	svůj	k3xOyFgNnSc4	svůj
legendární	legendární	k2eAgNnSc4d1	legendární
album	album	k1gNnSc4	album
Master	master	k1gMnSc1	master
of	of	k?	of
Puppets	Puppets	k1gInSc1	Puppets
a	a	k8xC	a
Nuclear	Nuclear	k1gInSc1	Nuclear
Assault	Assaulta	k1gFnPc2	Assaulta
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
hardcore	hardcor	k1gInSc5	hardcor
punkem	punk	k1gMnSc7	punk
ovlivněným	ovlivněný	k2eAgMnSc7d1	ovlivněný
thrashovým	thrashův	k2eAgInSc7d1	thrashův
opusem	opus	k1gInSc7	opus
Game	game	k1gInSc1	game
Over	Overa	k1gFnPc2	Overa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
skupina	skupina	k1gFnSc1	skupina
Anthrax	Anthrax	k1gInSc1	Anthrax
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
nejúspěšnější	úspěšný	k2eAgNnSc4d3	nejúspěšnější
album	album	k1gNnSc4	album
Among	Among	k1gMnSc1	Among
the	the	k?	the
Living	Living	k1gInSc1	Living
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
znova	znova	k6eAd1	znova
otevřela	otevřít	k5eAaPmAgFnS	otevřít
thrashová	thrashový	k2eAgFnSc1d1	thrashová
scéna	scéna	k1gFnSc1	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
skladby	skladba	k1gFnPc1	skladba
byly	být	k5eAaImAgFnP	být
trochu	trochu	k6eAd1	trochu
melodičtější	melodický	k2eAgFnPc1d2	melodičtější
<g/>
,	,	kIx,	,
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
thrashi	thrash	k1gInSc6	thrash
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
zásluhou	zásluha	k1gFnSc7	zásluha
chytlavých	chytlavý	k2eAgInPc2d1	chytlavý
riffů	riff	k1gInPc2	riff
a	a	k8xC	a
melodičtějším	melodický	k2eAgInSc7d2	melodičtější
<g/>
,	,	kIx,	,
ne	ne	k9	ne
tolik	tolik	k6eAd1	tolik
"	"	kIx"	"
<g/>
štěkaným	štěkaný	k2eAgInSc7d1	štěkaný
<g/>
"	"	kIx"	"
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jejich	jejich	k3xOp3gNnSc1	jejich
pojetí	pojetí	k1gNnSc1	pojetí
textů	text	k1gInPc2	text
se	se	k3xPyFc4	se
lišilo	lišit	k5eAaImAgNnS	lišit
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
thrashových	thrashův	k2eAgFnPc2d1	thrashův
kapel	kapela	k1gFnPc2	kapela
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
skupiny	skupina	k1gFnPc1	skupina
braly	brát	k5eAaImAgFnP	brát
texty	text	k1gInPc4	text
vážněji	vážně	k6eAd2	vážně
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
texty	text	k1gInPc4	text
Anthrax	Anthrax	k1gInSc1	Anthrax
hraničily	hraničit	k5eAaImAgFnP	hraničit
s	s	k7c7	s
parodií	parodie	k1gFnSc7	parodie
(	(	kIx(	(
<g/>
především	především	k9	především
ve	v	k7c6	v
skladbách	skladba	k1gFnPc6	skladba
<g/>
:	:	kIx,	:
N.	N.	kA	N.
<g/>
F.	F.	kA	F.
<g/>
L.	L.	kA	L.
<g/>
,	,	kIx,	,
I	i	k9	i
Am	Am	k1gMnSc1	Am
the	the	k?	the
Law	Law	k1gMnSc1	Law
<g/>
,	,	kIx,	,
A	a	k8xC	a
Skeleton	skeleton	k1gInSc1	skeleton
In	In	k1gFnSc2	In
the	the	k?	the
Closet	Closeta	k1gFnPc2	Closeta
-	-	kIx~	-
všechny	všechen	k3xTgFnPc1	všechen
z	z	k7c2	z
alba	album	k1gNnSc2	album
Among	Among	k1gMnSc1	Among
the	the	k?	the
Living	Living	k1gInSc1	Living
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
texty	text	k1gInPc1	text
ještě	ještě	k9	ještě
doplňovaly	doplňovat	k5eAaImAgInP	doplňovat
humorná	humorný	k2eAgNnPc1d1	humorné
vystoupení	vystoupení	k1gNnPc1	vystoupení
na	na	k7c6	na
pódiích	pódium	k1gNnPc6	pódium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vydali	vydat	k5eAaPmAgMnP	vydat
svůj	svůj	k3xOyFgInSc4	svůj
debut	debut	k1gInSc4	debut
američtí	americký	k2eAgMnPc1d1	americký
thrasheři	thrasher	k1gMnPc1	thrasher
Testament	testament	k1gInSc4	testament
<g/>
,	,	kIx,	,
The	The	k1gFnSc4	The
Legacy	Legaca	k1gFnSc2	Legaca
<g/>
.	.	kIx.	.
</s>
<s>
Testament	testament	k1gInSc4	testament
všeobecně	všeobecně	k6eAd1	všeobecně
kladli	klást	k5eAaImAgMnP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
progresivnější	progresivní	k2eAgInPc4d2	progresivnější
prvky	prvek	k1gInPc4	prvek
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
skladby	skladba	k1gFnPc1	skladba
byly	být	k5eAaImAgFnP	být
oproti	oproti	k7c3	oproti
konkurenci	konkurence	k1gFnSc3	konkurence
melodičtější	melodický	k2eAgFnSc3d2	melodičtější
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
zaobíraly	zaobírat	k5eAaImAgInP	zaobírat
především	především	k9	především
okultní	okultní	k2eAgMnSc1d1	okultní
a	a	k8xC	a
satanistickou	satanistický	k2eAgFnSc7d1	satanistická
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bezpochyby	bezpochyby	k6eAd1	bezpochyby
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
textovou	textový	k2eAgFnSc4d1	textová
stránku	stránka	k1gFnSc4	stránka
death	death	k1gMnSc1	death
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
thrash	thrash	k1gInSc1	thrash
metal	metal	k1gInSc4	metal
začal	začít	k5eAaPmAgInS	začít
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
vícero	vícero	k1gNnSc4	vícero
stylů	styl	k1gInPc2	styl
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Death	Death	k1gInSc1	Death
a	a	k8xC	a
Possessed	Possessed	k1gInSc1	Possessed
<g/>
.	.	kIx.	.
</s>
<s>
Possessed	Possessed	k1gInSc1	Possessed
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
první	první	k4xOgMnPc1	první
death-metalové	deathetal	k1gMnPc1	death-metal
kapely	kapela	k1gFnSc2	kapela
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
vydali	vydat	k5eAaPmAgMnP	vydat
svoje	své	k1gNnSc4	své
první	první	k4xOgFnSc2	první
demo	demo	k2eAgFnSc2d1	demo
v	v	k7c6	v
thrash-metalovém	thrashetalový	k2eAgInSc6d1	thrash-metalový
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
temnějším	temný	k2eAgInSc7d2	temnější
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
temný	temný	k2eAgInSc1d1	temný
thrash	thrash	k1gInSc1	thrash
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
nazývat	nazývat	k5eAaImF	nazývat
death	death	k1gInSc4	death
metal	metal	k1gInSc1	metal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
Suicidal	Suicidal	k1gMnSc1	Suicidal
Tendencies	Tendencies	k1gMnSc1	Tendencies
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
předtím	předtím	k6eAd1	předtím
poctivou	poctivý	k2eAgFnSc7d1	poctivá
hardcore	hardcor	k1gInSc5	hardcor
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
u	u	k7c2	u
velké	velký	k2eAgFnSc2d1	velká
společnosti	společnost	k1gFnSc2	společnost
-	-	kIx~	-
How	How	k1gMnSc1	How
Will	Will	k1gMnSc1	Will
I	i	k8xC	i
Laugh	Laugh	k1gMnSc1	Laugh
Tomorrow	Tomorrow	k1gMnSc1	Tomorrow
If	If	k1gMnSc1	If
I	i	k8xC	i
Can	Can	k1gMnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Even	Even	k1gInSc1	Even
Smile	smil	k1gInSc5	smil
Today	Today	k1gInPc4	Today
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Album	album	k1gNnSc1	album
mělo	mít	k5eAaImAgNnS	mít
thrashové	thrashový	k2eAgNnSc1d1	thrashový
kytarové	kytarový	k2eAgInPc1d1	kytarový
riffy	riff	k1gInPc1	riff
a	a	k8xC	a
zvuk	zvuk	k1gInSc1	zvuk
byl	být	k5eAaImAgInS	být
celkově	celkově	k6eAd1	celkově
"	"	kIx"	"
<g/>
metalový	metalový	k2eAgMnSc1d1	metalový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
komplikovanější	komplikovaný	k2eAgFnSc7d2	komplikovanější
strukturou	struktura	k1gFnSc7	struktura
skladeb	skladba	k1gFnPc2	skladba
než	než	k8xS	než
na	na	k7c6	na
předcházejících	předcházející	k2eAgNnPc6d1	předcházející
albech	album	k1gNnPc6	album
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
kořenům	kořen	k1gInPc3	kořen
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
hardcore	hardcor	k1gInSc5	hardcor
byly	být	k5eAaImAgInP	být
skladby	skladba	k1gFnPc4	skladba
melodické	melodický	k2eAgNnSc4d1	melodické
a	a	k8xC	a
měly	mít	k5eAaImAgInP	mít
chytlavé	chytlavý	k2eAgInPc1d1	chytlavý
refrény	refrén	k1gInPc1	refrén
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
thrash	thrash	k1gInSc1	thrash
nasycený	nasycený	k2eAgInSc1d1	nasycený
novými	nový	k2eAgFnPc7d1	nová
kapelami	kapela	k1gFnPc7	kapela
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klasické	klasický	k2eAgInPc1d1	klasický
počiny	počin	k1gInPc1	počin
se	se	k3xPyFc4	se
nepřestávaly	přestávat	k5eNaImAgInP	přestávat
nahrávat	nahrávat	k5eAaImF	nahrávat
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
album	album	k1gNnSc1	album
Sepultury	Sepultura	k1gFnSc2	Sepultura
<g/>
,	,	kIx,	,
Beneath	Beneath	k1gMnSc1	Beneath
the	the	k?	the
Remains	Remains	k1gInSc1	Remains
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
kapele	kapela	k1gFnSc6	kapela
přinesl	přinést	k5eAaPmAgMnS	přinést
pozornost	pozornost	k1gFnSc4	pozornost
mainstreamového	mainstreamový	k2eAgNnSc2d1	mainstreamové
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Vio-lence	Vioenec	k1gMnPc4	Vio-lenec
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
Eternal	Eternal	k1gMnSc5	Eternal
Nightmare	Nightmar	k1gMnSc5	Nightmar
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
spojuje	spojovat	k5eAaImIp3nS	spojovat
nekompromisnost	nekompromisnost	k1gFnSc4	nekompromisnost
thrashových	thrashův	k2eAgInPc2d1	thrashův
riffů	riff	k1gInPc2	riff
s	s	k7c7	s
hardcorovými	hardcorův	k2eAgInPc7d1	hardcorův
vokály	vokál	k1gInPc7	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Progresívní	progresívní	k2eAgNnSc1d1	progresívní
album	album	k1gNnSc1	album
Rust	Rust	k1gMnSc1	Rust
in	in	k?	in
Peace	Peace	k1gMnSc1	Peace
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nejvydařenější	vydařený	k2eAgInSc4d3	nejvydařenější
kousek	kousek	k1gInSc4	kousek
Megadeth	Megadetha	k1gFnPc2	Megadetha
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
...	...	k?	...
<g/>
And	Anda	k1gFnPc2	Anda
Justice	justice	k1gFnSc1	justice
for	forum	k1gNnPc2	forum
All	All	k1gFnSc2	All
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
extrémně	extrémně	k6eAd1	extrémně
komplexní	komplexní	k2eAgFnSc7d1	komplexní
strukturou	struktura	k1gFnSc7	struktura
skladby	skladba	k1gFnSc2	skladba
<g/>
,	,	kIx,	,
Metallica	Metallica	k1gFnSc1	Metallica
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
progresivního	progresivní	k2eAgInSc2d1	progresivní
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Thrashové	Thrashový	k2eAgFnPc1d1	Thrashový
skupiny	skupina	k1gFnPc1	skupina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
přišly	přijít	k5eAaPmAgFnP	přijít
s	s	k7c7	s
novějším	nový	k2eAgInSc7d2	novější
<g/>
,	,	kIx,	,
progresívnějším	progresívní	k2eAgInSc7d2	progresívní
zvukem	zvuk	k1gInSc7	zvuk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tradiční	tradiční	k2eAgInSc1d1	tradiční
thrash	thrash	k1gInSc1	thrash
metal	metal	k1gInSc1	metal
byli	být	k5eAaImAgMnP	být
částečně	částečně	k6eAd1	částečně
vnímané	vnímaný	k2eAgMnPc4d1	vnímaný
už	už	k9	už
jen	jen	k6eAd1	jen
jako	jako	k8xS	jako
přežitek	přežitek	k1gInSc4	přežitek
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
skvělých	skvělý	k2eAgInPc2d1	skvělý
kousků	kousek	k1gInPc2	kousek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
album	album	k1gNnSc1	album
Night	Nighta	k1gFnPc2	Nighta
of	of	k?	of
the	the	k?	the
Stormrider	Stormrider	k1gInSc1	Stormrider
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
od	od	k7c2	od
Iced	Iceda	k1gFnPc2	Iceda
Earth	Eartha	k1gFnPc2	Eartha
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kombinovalo	kombinovat	k5eAaImAgNnS	kombinovat
power	power	k1gInSc4	power
metal	metal	k1gInSc4	metal
s	s	k7c7	s
thrashem	thrash	k1gInSc7	thrash
nebo	nebo	k8xC	nebo
Sacred	Sacred	k1gInSc1	Sacred
Reich	Reich	k?	Reich
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
třetí	třetí	k4xOgFnSc1	třetí
deska	deska	k1gFnSc1	deska
Independent	independent	k1gMnSc1	independent
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
mnohé	mnohý	k2eAgFnPc1d1	mnohá
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Machine	Machin	k1gInSc5	Machin
Head	Head	k1gInSc4	Head
a	a	k8xC	a
Pantera	panter	k1gMnSc2	panter
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
zaměřily	zaměřit	k5eAaPmAgInP	zaměřit
na	na	k7c4	na
pomalejší	pomalý	k2eAgInSc4d2	pomalejší
a	a	k8xC	a
zatěžkanější	zatěžkaný	k2eAgInSc4d2	zatěžkaný
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
dominantním	dominantní	k2eAgInSc7d1	dominantní
stylem	styl	k1gInSc7	styl
rockové	rockový	k2eAgFnSc2d1	rocková
muziky	muzika	k1gFnSc2	muzika
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
alternativní	alternativní	k2eAgInSc1d1	alternativní
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
thrash	thrash	k1gInSc1	thrash
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
nezanedbatelný	zanedbatelný	k2eNgInSc1d1	nezanedbatelný
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
funk-metalová	funketalový	k2eAgFnSc1d1	funk-metalový
skupina	skupina	k1gFnSc1	skupina
Primus	primus	k1gInSc1	primus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
hrál	hrát	k5eAaImAgMnS	hrát
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kytarista	kytarista	k1gMnSc1	kytarista
Possessed	Possessed	k1gMnSc1	Possessed
Ler	Ler	k1gMnSc5	Ler
LaLonde	LaLond	k1gMnSc5	LaLond
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rané	raný	k2eAgFnSc6d1	raná
tvorbě	tvorba	k1gFnSc6	tvorba
mixovala	mixovat	k5eAaImAgFnS	mixovat
funkový	funkový	k2eAgInSc4d1	funkový
styl	styl	k1gInSc4	styl
baskytary	baskytara	k1gFnSc2	baskytara
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
thrashově	thrashově	k6eAd1	thrashově
ovlivněnými	ovlivněný	k2eAgInPc7d1	ovlivněný
riffy	riff	k1gInPc7	riff
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
Metallica	Metallica	k1gFnSc1	Metallica
vydala	vydat	k5eAaPmAgFnS	vydat
rádiově	rádiově	k6eAd1	rádiově
orientované	orientovaný	k2eAgNnSc4d1	orientované
metalové	metalový	k2eAgNnSc4d1	metalové
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Černé	Černé	k2eAgNnSc1d1	Černé
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
kapele	kapela	k1gFnSc3	kapela
přineslo	přinést	k5eAaPmAgNnS	přinést
obrovský	obrovský	k2eAgInSc4d1	obrovský
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
si	se	k3xPyFc3	se
pověstně	pověstně	k6eAd1	pověstně
ostříhali	ostříhat	k5eAaPmAgMnP	ostříhat
své	svůj	k3xOyFgInPc4	svůj
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
Load	Load	k1gInSc1	Load
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
a	a	k8xC	a
ReLoad	ReLoad	k1gInSc1	ReLoad
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nesou	nést	k5eAaImIp3nP	nést
spíše	spíše	k9	spíše
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
alternativního	alternativní	k2eAgInSc2d1	alternativní
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
jižanského	jižanský	k2eAgInSc2d1	jižanský
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
thrash	thrash	k1gInSc4	thrash
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
už	už	k6eAd1	už
považovaný	považovaný	k2eAgInSc1d1	považovaný
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
styly	styl	k1gInPc7	styl
"	"	kIx"	"
<g/>
extrémního	extrémní	k2eAgInSc2d1	extrémní
metalu	metal	k1gInSc2	metal
<g/>
"	"	kIx"	"
za	za	k7c4	za
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
získávat	získávat	k5eAaImF	získávat
nové	nový	k2eAgNnSc4d1	nové
mladé	mladý	k2eAgNnSc4d1	mladé
publikum	publikum	k1gNnSc4	publikum
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgInSc3d1	opětovný
nárůstu	nárůst	k1gInSc3	nárůst
popularity	popularita	k1gFnSc2	popularita
žánru	žánr	k1gInSc2	žánr
a	a	k8xC	a
k	k	k7c3	k
nárůstů	nárůst	k1gInPc2	nárůst
fanouškovské	fanouškovský	k2eAgFnSc2d1	fanouškovská
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Nová	nový	k2eAgFnSc1d1	nová
thrash	thrash	k1gInSc1	thrash
metalová	metalový	k2eAgFnSc1d1	metalová
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bývají	bývat	k5eAaImIp3nP	bývat
thrashové	thrashový	k2eAgFnPc1d1	thrashová
kapely	kapela	k1gFnPc1	kapela
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
nového	nový	k2eAgNnSc2d1	nové
milénia	milénium	k1gNnSc2	milénium
označovány	označován	k2eAgFnPc1d1	označována
<g/>
,	,	kIx,	,
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesla	přinést	k5eAaPmAgFnS	přinést
i	i	k9	i
řadu	řada	k1gFnSc4	řada
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
alb	album	k1gNnPc2	album
z	z	k7c2	z
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
však	však	k9	však
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
počtu	počet	k1gInSc6	počet
vlivy	vliv	k1gInPc4	vliv
novějších	nový	k2eAgInPc2d2	novější
metalových	metalový	k2eAgInPc2d1	metalový
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
základní	základní	k2eAgInPc4d1	základní
pilíře	pilíř	k1gInPc4	pilíř
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
thrashových	thrashův	k2eAgFnPc2d1	thrashův
kapel	kapela	k1gFnPc2	kapela
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
Municipal	Municipal	k1gFnSc4	Municipal
Waste	Wast	k1gMnSc5	Wast
a	a	k8xC	a
především	především	k9	především
jejich	jejich	k3xOp3gNnSc1	jejich
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
Hazardous	Hazardous	k1gMnSc1	Hazardous
Mutation	Mutation	k1gInSc1	Mutation
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
určitě	určitě	k6eAd1	určitě
nesmíme	smět	k5eNaImIp1nP	smět
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
nekompromisní	kompromisní	k2eNgInSc4d1	nekompromisní
Violator	Violator	k1gInSc4	Violator
-	-	kIx~	-
Chemical	Chemical	k1gMnSc1	Chemical
Assault	Assault	k1gMnSc1	Assault
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gama	gama	k1gNnSc1	gama
Bomb	bomba	k1gFnPc2	bomba
-	-	kIx~	-
Citizen	Citizen	kA	Citizen
Brain	Brain	k1gMnSc1	Brain
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Evile	Evile	k1gFnSc1	Evile
-	-	kIx~	-
Enter	Enter	k1gInSc1	Enter
The	The	k1gFnSc2	The
Grave	grave	k1gNnSc2	grave
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Toxic	Toxic	k1gMnSc1	Toxic
Holocaust	holocaust	k1gInSc1	holocaust
-	-	kIx~	-
An	An	k1gFnSc2	An
Overdose	Overdosa	k1gFnSc3	Overdosa
of	of	k?	of
Death	Death	k1gInSc1	Death
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Angelus	Angelus	k1gInSc1	Angelus
Apatrida	Apatrida	k1gFnSc1	Apatrida
-	-	kIx~	-
Evil	Evil	k1gMnSc1	Evil
Unleashed	Unleashed	k1gMnSc1	Unleashed
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Warbringer	Warbringer	k1gMnSc1	Warbringer
-	-	kIx~	-
War	War	k1gMnSc1	War
Without	Without	k1gMnSc1	Without
End	End	k1gMnSc1	End
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vektor	vektor	k1gInSc1	vektor
-	-	kIx~	-
Black	Black	k1gInSc1	Black
Future	Futur	k1gMnSc5	Futur
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
také	také	k9	také
Havok	Havok	k1gInSc1	Havok
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
vůdčích	vůdčí	k2eAgFnPc2d1	vůdčí
kapel	kapela	k1gFnPc2	kapela
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
druhou	druhý	k4xOgFnSc7	druhý
deskou	deska	k1gFnSc7	deska
Time	Time	k1gNnSc2	Time
Is	Is	k1gMnSc1	Is
Up	Up	k1gMnSc1	Up
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
thrash	thrasha	k1gFnPc2	thrasha
metalových	metalový	k2eAgFnPc2d1	metalová
kapel	kapela	k1gFnPc2	kapela
na	na	k7c4	na
Encyclopaedia	Encyclopaedium	k1gNnPc4	Encyclopaedium
Metallum	Metallum	k1gNnSc4	Metallum
</s>
