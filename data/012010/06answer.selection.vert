<s>
Moudrý	moudrý	k2eAgInSc1d1	moudrý
klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Sorting	Sorting	k1gInSc1	Sorting
Hat	hat	k0	hat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
každoročně	každoročně	k6eAd1	každoročně
při	při	k7c6	při
zařazování	zařazování	k1gNnSc6	zařazování
studentů	student	k1gMnPc2	student
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kolejí	kolej	k1gFnPc2	kolej
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
,	,	kIx,	,
Havraspáru	Havraspár	k1gInSc2	Havraspár
<g/>
,	,	kIx,	,
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
.	.	kIx.	.
</s>
