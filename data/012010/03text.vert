<p>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc1	seznam
některých	některý	k3yIgInPc2	některý
kouzelných	kouzelný	k2eAgInPc2d1	kouzelný
předmětů	předmět	k1gInPc2	předmět
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
od	od	k7c2	od
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc4d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bleskobrk	Bleskobrk	k1gInSc4	Bleskobrk
==	==	k?	==
</s>
</p>
<p>
<s>
Bleskobrk	Bleskobrk	k1gInSc4	Bleskobrk
anglicky	anglicky	k6eAd1	anglicky
Quick-Quotes	Quick-Quotes	k1gMnSc1	Quick-Quotes
Quill	Quill	k1gMnSc1	Quill
je	být	k5eAaImIp3nS	být
očarovaný	očarovaný	k2eAgInSc4d1	očarovaný
psací	psací	k2eAgInSc4d1	psací
brk	brk	k1gInSc4	brk
<g/>
.	.	kIx.	.
</s>
<s>
Normální	normální	k2eAgInSc1d1	normální
bleskobrk	bleskobrk	k1gInSc1	bleskobrk
píše	psát	k5eAaImIp3nS	psát
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
mu	on	k3xPp3gMnSc3	on
kouzelník	kouzelník	k1gMnSc1	kouzelník
nadiktuje	nadiktovat	k5eAaPmIp3nS	nadiktovat
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
/	/	kIx~	/
<g/>
filmu	film	k1gInSc6	film
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
patřil	patřit	k5eAaImAgMnS	patřit
dopisovatelce	dopisovatelka	k1gFnSc6	dopisovatelka
Denního	denní	k2eAgMnSc4d1	denní
věštce	věštec	k1gMnSc4	věštec
Ritě	Rita	k1gFnSc3	Rita
Holoubkové	Holoubkové	k2eAgMnPc1d1	Holoubkové
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
jedovatě	jedovatě	k6eAd1	jedovatě
zelený	zelený	k2eAgInSc1d1	zelený
bleskobrk	bleskobrk	k1gInSc1	bleskobrk
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
očarovaný	očarovaný	k2eAgInSc1d1	očarovaný
aby	aby	kYmCp3nS	aby
psal	psát	k5eAaImAgInS	psát
vše	všechen	k3xTgNnSc4	všechen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
líbilo	líbit	k5eAaImAgNnS	líbit
Ritě	Rita	k1gFnSc3	Rita
Holoubkové	Holoubkové	k2eAgFnSc3d1	Holoubkové
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
tedy	tedy	k9	tedy
překrucoval	překrucovat	k5eAaImAgMnS	překrucovat
pravdu	pravda	k1gFnSc4	pravda
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
dělal	dělat	k5eAaImAgMnS	dělat
z	z	k7c2	z
komára	komár	k1gMnSc2	komár
velblouda	velbloud	k1gMnSc2	velbloud
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bradavický	Bradavický	k2eAgInSc1d1	Bradavický
expres	expres	k1gInSc1	expres
==	==	k?	==
</s>
</p>
<p>
<s>
Bradavický	Bradavický	k2eAgInSc1d1	Bradavický
expres	expres	k1gInSc1	expres
je	být	k5eAaImIp3nS	být
zářivě	zářivě	k6eAd1	zářivě
červená	červený	k2eAgFnSc1d1	červená
parní	parní	k2eAgFnSc1d1	parní
souprava	souprava	k1gFnSc1	souprava
dopravující	dopravující	k2eAgFnSc1d1	dopravující
každoročně	každoročně	k6eAd1	každoročně
studenty	student	k1gMnPc4	student
do	do	k7c2	do
Školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
a	a	k8xC	a
o	o	k7c6	o
vánočních	vánoční	k2eAgFnPc6d1	vánoční
prázdninách	prázdniny	k1gFnPc6	prázdniny
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
vždy	vždy	k6eAd1	vždy
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
z	z	k7c2	z
londýnského	londýnský	k2eAgNnSc2d1	Londýnské
nádraží	nádraží	k1gNnSc2	nádraží
King	King	k1gInSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Cross	k1gInSc1	Cross
z	z	k7c2	z
nástupiště	nástupiště	k1gNnSc2	nástupiště
devět	devět	k4xCc4	devět
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
červená	červený	k2eAgFnSc1d1	červená
jen	jen	k9	jen
z	z	k7c2	z
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čidlo	čidlo	k1gNnSc1	čidlo
tajností	tajnost	k1gFnPc2	tajnost
==	==	k?	==
</s>
</p>
<p>
<s>
Čidlo	čidlo	k1gNnSc1	čidlo
vypadající	vypadající	k2eAgNnSc1d1	vypadající
jako	jako	k8xC	jako
pokroucená	pokroucený	k2eAgFnSc1d1	pokroucená
zlatá	zlatý	k2eAgFnSc1d1	zlatá
televizní	televizní	k2eAgFnSc1d1	televizní
anténa	anténa	k1gFnSc1	anténa
se	se	k3xPyFc4	se
rozkmitá	rozkmitat	k5eAaPmIp3nS	rozkmitat
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdo	někdo	k3yInSc1	někdo
něco	něco	k3yInSc4	něco
zatajuje	zatajovat	k5eAaImIp3nS	zatajovat
nebo	nebo	k8xC	nebo
lže	lhát	k5eAaImIp3nS	lhát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
měl	mít	k5eAaImAgInS	mít
čidlo	čidlo	k1gNnSc1	čidlo
Alastor	Alastor	k1gInSc4	Alastor
Moody	Mooda	k1gFnSc2	Mooda
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
Bartemius	Bartemius	k1gMnSc1	Bartemius
Skrk	Skrk	k1gMnSc1	Skrk
a	a	k8xC	a
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Princ	princa	k1gFnPc2	princa
dvojí	dvojit	k5eAaImIp3nP	dvojit
krve	krev	k1gFnSc2	krev
Filch	Filcha	k1gFnPc2	Filcha
jím	jíst	k5eAaImIp1nS	jíst
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
nejsou	být	k5eNaImIp3nP	být
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
pašovány	pašován	k2eAgInPc4d1	pašován
nepovolené	povolený	k2eNgInPc4d1	nepovolený
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Čidlo	čidlo	k1gNnSc1	čidlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
stále	stále	k6eAd1	stále
bzučelo	bzučet	k5eAaImAgNnS	bzučet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
studenti	student	k1gMnPc1	student
všude	všude	k6eAd1	všude
kolem	kolem	k6eAd1	kolem
lžou	lhát	k5eAaImIp3nP	lhát
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nepřinesli	přinést	k5eNaPmAgMnP	přinést
své	svůj	k3xOyFgInPc4	svůj
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
Moody	Mooda	k1gFnPc4	Mooda
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čokoládové	čokoládový	k2eAgFnSc2d1	čokoládová
žabky	žabka	k1gFnSc2	žabka
==	==	k?	==
</s>
</p>
<p>
<s>
Čokoládové	čokoládový	k2eAgFnPc1d1	čokoládová
žabky	žabka	k1gFnPc1	žabka
jsou	být	k5eAaImIp3nP	být
očarované	očarovaný	k2eAgFnPc1d1	očarovaná
žabky	žabka	k1gFnPc1	žabka
z	z	k7c2	z
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
normální	normální	k2eAgFnSc2d1	normální
živé	živý	k2eAgFnSc2d1	živá
žáby	žába	k1gFnSc2	žába
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
chutnají	chutnat	k5eAaImIp3nP	chutnat
čokoládově	čokoládově	k6eAd1	čokoládově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krabičce	krabička	k1gFnSc6	krabička
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
zabaleny	zabalit	k5eAaPmNgFnP	zabalit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přiložena	přiložen	k2eAgFnSc1d1	přiložena
karta	karta	k1gFnSc1	karta
se	s	k7c7	s
slavným	slavný	k2eAgMnSc7d1	slavný
kouzelníkem	kouzelník	k1gMnSc7	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
/	/	kIx~	/
<g/>
filmu	film	k1gInSc6	film
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
Čokoládové	čokoládový	k2eAgFnPc1d1	čokoládová
žabky	žabka	k1gFnPc1	žabka
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
kouzelnickým	kouzelnický	k2eAgFnPc3d1	kouzelnická
sladkostem	sladkost	k1gFnPc3	sladkost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
Harrymu	Harrym	k1gInSc2	Harrym
žabka	žabka	k1gFnSc1	žabka
vyskočila	vyskočit	k5eAaPmAgFnS	vyskočit
ve	v	k7c6	v
vlaku	vlak	k1gInSc6	vlak
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Denní	denní	k2eAgMnSc1d1	denní
věštec	věštec	k1gMnSc1	věštec
==	==	k?	==
</s>
</p>
<p>
<s>
Denní	denní	k2eAgMnSc1d1	denní
věštec	věštec	k1gMnSc1	věštec
jsou	být	k5eAaImIp3nP	být
kouzelnické	kouzelnický	k2eAgFnPc4d1	kouzelnická
noviny	novina	k1gFnPc4	novina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kouzelníků	kouzelník	k1gMnPc2	kouzelník
a	a	k8xC	a
čarodějek	čarodějka	k1gFnPc2	čarodějka
si	se	k3xPyFc3	se
kupuje	kupovat	k5eAaImIp3nS	kupovat
Věštce	věštec	k1gMnSc2	věštec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
například	například	k6eAd1	například
Jinotaj	jinotaj	k1gInSc1	jinotaj
se	s	k7c7	s
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
Xenofiliem	Xenofilium	k1gNnSc7	Xenofilium
Láskorádem	Láskorád	k1gInSc7	Láskorád
(	(	kIx(	(
<g/>
otcem	otec	k1gMnSc7	otec
Lenky	Lenka	k1gFnSc2	Lenka
Láskorádové	Láskorádový	k2eAgInPc1d1	Láskorádový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Věštec	věštec	k1gMnSc1	věštec
někdy	někdy	k6eAd1	někdy
píše	psát	k5eAaImIp3nS	psát
hlouposti	hloupost	k1gFnPc4	hloupost
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
autorkou	autorka	k1gFnSc7	autorka
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
dopisovatelka	dopisovatelka	k1gFnSc1	dopisovatelka
Rita	Rita	k1gFnSc1	Rita
Holoubková	Holoubková	k1gFnSc1	Holoubková
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
si	se	k3xPyFc3	se
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
předkoupila	předkoupit	k5eAaPmAgFnS	předkoupit
Věštce	věštec	k1gMnSc4	věštec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dřív	dříve	k6eAd2	dříve
věděla	vědět	k5eAaImAgFnS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Rita	Rita	k1gFnSc1	Rita
napsala	napsat	k5eAaBmAgFnS	napsat
něco	něco	k3yInSc1	něco
špatného	špatný	k2eAgNnSc2d1	špatné
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
nebo	nebo	k8xC	nebo
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
moci	moct	k5eAaImF	moct
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
<g/>
,	,	kIx,	,
Věštec	věštec	k1gMnSc1	věštec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podřídil	podřídit	k5eAaPmAgInS	podřídit
–	–	k?	–
psal	psát	k5eAaImAgInS	psát
jen	jen	k9	jen
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
on	on	k3xPp3gMnSc1	on
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hulák	Hulák	k1gInSc4	Hulák
==	==	k?	==
</s>
</p>
<p>
<s>
Hulák	Hulák	k1gInSc1	Hulák
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejnepříjemnější	příjemný	k2eNgNnSc1d3	nejnepříjemnější
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
může	moct	k5eAaImIp3nS	moct
student	student	k1gMnSc1	student
na	na	k7c4	na
snídani	snídaně	k1gFnSc4	snídaně
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zářivě	zářivě	k6eAd1	zářivě
rudá	rudý	k2eAgFnSc1d1	rudá
obálka	obálka	k1gFnSc1	obálka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
když	když	k8xS	když
se	se	k3xPyFc4	se
neotevře	otevřít	k5eNaPmIp3nS	otevřít
tak	tak	k8xC	tak
vybuchne	vybuchnout	k5eAaPmIp3nS	vybuchnout
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
ji	on	k3xPp3gFnSc4	on
jen	jen	k9	jen
tak	tak	k6eAd1	tak
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jisté	jistý	k2eAgNnSc4d1	jisté
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
kouzlem	kouzlo	k1gNnSc7	kouzlo
zesílí	zesílit	k5eAaPmIp3nS	zesílit
hlas	hlas	k1gInSc1	hlas
odesílatele	odesílatel	k1gMnSc2	odesílatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
slyšet	slyšet	k5eAaImF	slyšet
široko	široko	k6eAd1	široko
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgInS	získat
Ron	ron	k1gInSc1	ron
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
přiletěli	přiletět	k5eAaPmAgMnP	přiletět
létajícím	létající	k2eAgNnSc7d1	létající
autem	auto	k1gNnSc7	auto
pana	pan	k1gMnSc2	pan
Weasleyho	Weasley	k1gMnSc2	Weasley
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
díle	dílo	k1gNnSc6	dílo
pošle	poslat	k5eAaPmIp3nS	poslat
jeden	jeden	k4xCgInSc4	jeden
Nevillovi	Nevillův	k2eAgMnPc1d1	Nevillův
jeho	jeho	k3xOp3gMnSc3	jeho
babička	babička	k1gFnSc1	babička
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ztratil	ztratit	k5eAaPmAgMnS	ztratit
lístek	lístek	k1gInSc4	lístek
s	s	k7c7	s
hesly	heslo	k1gNnPc7	heslo
do	do	k7c2	do
Nebelvírské	Nebelvírský	k2eAgFnSc2d1	Nebelvírská
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
a	a	k8xC	a
umožní	umožnit	k5eAaPmIp3nP	umožnit
tak	tak	k6eAd1	tak
Siriusovi	Siriusův	k2eAgMnPc1d1	Siriusův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vloupal	vloupat	k5eAaPmAgMnS	vloupat
<g/>
.	.	kIx.	.
</s>
<s>
Naposled	naposled	k6eAd1	naposled
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednoho	jeden	k4xCgMnSc2	jeden
pošle	poslat	k5eAaPmIp3nS	poslat
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
tetě	teta	k1gFnSc3	teta
Petunii	Petunie	k1gFnSc4	Petunie
s	s	k7c7	s
textem	text	k1gInSc7	text
Pamatuj	pamatovat	k5eAaImRp2nS	pamatovat
na	na	k7c4	na
posledně	posledně	k6eAd1	posledně
<g/>
,	,	kIx,	,
Petunie	Petunie	k1gFnSc1	Petunie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
strýc	strýc	k1gMnSc1	strýc
Vernon	Vernon	k1gMnSc1	Vernon
chtěl	chtít	k5eAaImAgMnS	chtít
Harryho	Harry	k1gMnSc4	Harry
vyhodit	vyhodit	k5eAaPmF	vyhodit
z	z	k7c2	z
domu	dům	k1gInSc2	dům
a	a	k8xC	a
tak	tak	k6eAd1	tak
chtěl	chtít	k5eAaImAgMnS	chtít
Petunii	Petunie	k1gFnSc4	Petunie
připomenout	připomenout	k5eAaPmF	připomenout
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
vzali	vzít	k5eAaPmAgMnP	vzít
Harryho	Harry	k1gMnSc4	Harry
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Letaxová	Letaxový	k2eAgFnSc1d1	Letaxová
síť	síť	k1gFnSc1	síť
==	==	k?	==
</s>
</p>
<p>
<s>
Letaxová	Letaxový	k2eAgFnSc1d1	Letaxová
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
cestovat	cestovat	k5eAaImF	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vhození	vhození	k1gNnSc6	vhození
jistého	jistý	k2eAgInSc2d1	jistý
prášku	prášek	k1gInSc2	prášek
do	do	k7c2	do
krbu	krb	k1gInSc2	krb
a	a	k8xC	a
v	v	k7c6	v
krbu	krb	k1gInSc6	krb
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
smaragdově	smaragdově	k6eAd1	smaragdově
zelený	zelený	k2eAgInSc1d1	zelený
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
kouzelník	kouzelník	k1gMnSc1	kouzelník
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
krbu	krb	k1gInSc2	krb
<g/>
,	,	kIx,	,
řekne	říct	k5eAaPmIp3nS	říct
jméno	jméno	k1gNnSc1	jméno
místa	místo	k1gNnSc2	místo
krbu	krb	k1gInSc2	krb
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
roztočí	roztočit	k5eAaPmIp3nS	roztočit
se	se	k3xPyFc4	se
a	a	k8xC	a
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
krbu	krb	k1gInSc6	krb
toho	ten	k3xDgNnSc2	ten
místa	místo	k1gNnSc2	místo
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
přemístit	přemístit	k5eAaPmF	přemístit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
krb	krb	k1gInSc1	krb
není	být	k5eNaImIp3nS	být
nějak	nějak	k6eAd1	nějak
zabezpečený	zabezpečený	k2eAgInSc1d1	zabezpečený
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
krby	krb	k1gInPc4	krb
připojené	připojený	k2eAgInPc4d1	připojený
k	k	k7c3	k
Letaxu	Letax	k1gInSc3	Letax
hlídá	hlídat	k5eAaImIp3nS	hlídat
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lotroskop	Lotroskop	k1gInSc4	Lotroskop
==	==	k?	==
</s>
</p>
<p>
<s>
Lotroskop	Lotroskop	k1gInSc1	Lotroskop
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
upozorní	upozornit	k5eAaPmIp3nS	upozornit
kouzelníka	kouzelník	k1gMnSc4	kouzelník
točením	točení	k1gNnSc7	točení
a	a	k8xC	a
rozsvěcením	rozsvěcení	k1gNnSc7	rozsvěcení
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
blízkosti	blízkost	k1gFnSc6	blízkost
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nemůže	moct	k5eNaImIp3nS	moct
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Lotroskop	Lotroskop	k1gInSc1	Lotroskop
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
spoustu	spousta	k1gFnSc4	spousta
podob	podoba	k1gFnPc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
dostal	dostat	k5eAaPmAgInS	dostat
Harry	Harra	k1gFnSc2	Harra
lotroskop	lotroskop	k1gInSc1	lotroskop
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malého	malý	k2eAgMnSc4d1	malý
skleněného	skleněný	k2eAgMnSc4d1	skleněný
vlčka	vlček	k1gMnSc4	vlček
jako	jako	k8xC	jako
dárek	dárek	k1gInSc4	dárek
k	k	k7c3	k
narozeninám	narozeniny	k1gFnPc3	narozeniny
od	od	k7c2	od
Rona	Ron	k1gMnSc2	Ron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc4	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
měl	mít	k5eAaImAgInS	mít
lotroskop	lotroskop	k1gInSc1	lotroskop
Alastor	Alastor	k1gInSc4	Alastor
Moody	Mooda	k1gFnSc2	Mooda
<g/>
.	.	kIx.	.
</s>
<s>
Moodyho	Moodyze	k6eAd1	Moodyze
lotroskop	lotroskop	k1gInSc1	lotroskop
vypadal	vypadat	k5eAaImAgInS	vypadat
jako	jako	k8xS	jako
veliká	veliký	k2eAgFnSc1d1	veliká
skleněná	skleněný	k2eAgFnSc1d1	skleněná
káča	káča	k1gFnSc1	káča
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Moody	Mooda	k1gFnSc2	Mooda
ho	on	k3xPp3gNnSc4	on
musel	muset	k5eAaImAgMnS	muset
odstavit	odstavit	k5eAaPmF	odstavit
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
popraskaný	popraskaný	k2eAgMnSc1d1	popraskaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
studenti	student	k1gMnPc1	student
neustále	neustále	k6eAd1	neustále
lhali	lhát	k5eAaImAgMnP	lhát
a	a	k8xC	a
lotroskop	lotroskop	k1gInSc1	lotroskop
se	se	k3xPyFc4	se
pořád	pořád	k6eAd1	pořád
rozsvěcel	rozsvěcet	k5eAaImAgMnS	rozsvěcet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Meč	meč	k1gInSc1	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
Godrik	Godrik	k1gMnSc1	Godrik
Nebelvír	Nebelvír	k1gMnSc1	Nebelvír
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgMnS	vyrobit
skřety	skřet	k1gMnPc7	skřet
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
dává	dávat	k5eAaImIp3nS	dávat
neobyčejnou	obyčejný	k2eNgFnSc4d1	neobyčejná
schopnost	schopnost	k1gFnSc4	schopnost
nasávat	nasávat	k5eAaImF	nasávat
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ho	on	k3xPp3gMnSc4	on
posílí	posílit	k5eAaPmIp3nS	posílit
a	a	k8xC	a
odolávat	odolávat	k5eAaImF	odolávat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
mohlo	moct	k5eAaImAgNnS	moct
poškodit	poškodit	k5eAaPmF	poškodit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nemůže	moct	k5eNaImIp3nS	moct
zrezivět	zrezivět	k5eAaPmF	zrezivět
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
zašpinit	zašpinit	k5eAaPmF	zašpinit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
vstřebat	vstřebat	k5eAaPmF	vstřebat
baziliškův	baziliškův	k2eAgInSc1d1	baziliškův
jed	jed	k1gInSc1	jed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebelvírský	Nebelvírský	k2eAgMnSc1d1	Nebelvírský
student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgNnSc2	ten
hoden	hoden	k2eAgInSc1d1	hoden
<g/>
,	,	kIx,	,
ho	on	k3xPp3gInSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
z	z	k7c2	z
Moudrého	moudrý	k2eAgInSc2d1	moudrý
klobouku	klobouk	k1gInSc2	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
Harrymu	Harrym	k1gInSc2	Harrym
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
díle	dílo	k1gNnSc6	dílo
zabil	zabít	k5eAaPmAgMnS	zabít
baziliška	bazilišek	k1gMnSc4	bazilišek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Nevillovi	Nevill	k1gMnSc3	Nevill
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
zabil	zabít	k5eAaPmAgMnS	zabít
Naginiho	Nagini	k1gMnSc2	Nagini
<g/>
,	,	kIx,	,
hada	had	k1gMnSc2	had
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
viteál	viteát	k5eAaPmAgMnS	viteát
lorda	lord	k1gMnSc4	lord
Voldemorta	Voldemort	k1gMnSc4	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
nasál	nasát	k5eAaPmAgInS	nasát
jed	jed	k1gInSc1	jed
baziliška	bazilišek	k1gMnSc2	bazilišek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
meč	meč	k1gInSc1	meč
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
mála	málo	k4c2	málo
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zničit	zničit	k5eAaPmF	zničit
viteál	viteál	k1gInSc4	viteál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
díle	díl	k1gInSc6	díl
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
kopie	kopie	k1gFnSc1	kopie
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
trezoru	trezor	k1gInSc6	trezor
Lestrangeových	Lestrangeův	k2eAgFnPc2d1	Lestrangeův
v	v	k7c6	v
Gringottově	Gringottův	k2eAgFnSc6d1	Gringottova
bance	banka	k1gFnSc6	banka
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
ji	on	k3xPp3gFnSc4	on
umístil	umístit	k5eAaPmAgMnS	umístit
Severus	Severus	k1gMnSc1	Severus
Snape	Snap	k1gInSc5	Snap
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgInSc4d1	pravý
originál	originál	k1gInSc4	originál
meče	meč	k1gInSc2	meč
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
a	a	k8xC	a
tajně	tajně	k6eAd1	tajně
ho	on	k3xPp3gMnSc4	on
předal	předat	k5eAaPmAgMnS	předat
do	do	k7c2	do
jezírka	jezírko	k1gNnSc2	jezírko
Harrymu	Harrym	k1gInSc2	Harrym
Potterovi	Potter	k1gMnSc3	Potter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
meči	meč	k1gInSc6	meč
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
například	například	k6eAd1	například
Merlin	Merlin	k1gInSc1	Merlin
<g/>
,	,	kIx,	,
orel	orel	k1gMnSc1	orel
a	a	k8xC	a
jednorožec	jednorožec	k1gMnSc1	jednorožec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Moudrý	moudrý	k2eAgInSc1d1	moudrý
klobouk	klobouk	k1gInSc1	klobouk
==	==	k?	==
</s>
</p>
<p>
<s>
Moudrý	moudrý	k2eAgInSc1d1	moudrý
klobouk	klobouk	k1gInSc1	klobouk
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Sorting	Sorting	k1gInSc1	Sorting
Hat	hat	k0	hat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
každoročně	každoročně	k6eAd1	každoročně
při	při	k7c6	při
zařazování	zařazování	k1gNnSc6	zařazování
studentů	student	k1gMnPc2	student
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kolejí	kolej	k1gFnPc2	kolej
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
,	,	kIx,	,
Havraspáru	Havraspár	k1gInSc2	Havraspár
<g/>
,	,	kIx,	,
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
odzpívá	odzpívat	k5eAaPmIp3nS	odzpívat
píseň	píseň	k1gFnSc4	píseň
(	(	kIx(	(
<g/>
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
novou	nový	k2eAgFnSc4d1	nová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
nasazují	nasazovat	k5eAaImIp3nP	nasazovat
na	na	k7c4	na
hlavy	hlava	k1gFnPc4	hlava
prvňáků	prvňák	k1gMnPc2	prvňák
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
název	název	k1gInSc4	název
koleje	kolej	k1gFnSc2	kolej
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
studenta	student	k1gMnSc4	student
pošle	poslat	k5eAaPmIp3nS	poslat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
díle	dílo	k1gNnSc6	dílo
pomohl	pomoct	k5eAaPmAgMnS	pomoct
Harrymu	Harrymum	k1gNnSc3	Harrymum
při	při	k7c6	při
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
baziliškem	bazilišek	k1gMnSc7	bazilišek
v	v	k7c6	v
Tajemné	tajemný	k2eAgFnSc6d1	tajemná
komnatě	komnata	k1gFnSc6	komnata
-	-	kIx~	-
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
mu	on	k3xPp3gInSc3	on
meč	meč	k1gInSc1	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
dá	dát	k5eAaPmIp3nS	dát
meč	meč	k1gInSc1	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
Nevillovi	Nevillův	k2eAgMnPc1d1	Nevillův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Myslánka	Myslánka	k1gFnSc1	Myslánka
==	==	k?	==
</s>
</p>
<p>
<s>
Myslánka	Myslánka	k1gFnSc1	Myslánka
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Pensieve	Pensieev	k1gFnSc2	Pensieev
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kouzelná	kouzelný	k2eAgFnSc1d1	kouzelná
nádoba	nádoba	k1gFnSc1	nádoba
se	s	k7c7	s
starobylými	starobylý	k2eAgFnPc7d1	starobylá
runami	runa	k1gFnPc7	runa
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
úložiště	úložiště	k1gNnSc1	úložiště
nepotřebných	potřebný	k2eNgFnPc2d1	nepotřebná
či	či	k8xC	či
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
hůlky	hůlka	k1gFnSc2	hůlka
přiložené	přiložený	k2eAgFnSc2d1	přiložená
ke	k	k7c3	k
spánku	spánek	k1gInSc3	spánek
vysají	vysát	k5eAaPmIp3nP	vysát
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tenkých	tenký	k2eAgInPc2d1	tenký
provázků	provázek	k1gInPc2	provázek
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
konzistence	konzistence	k1gFnSc2	konzistence
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
,	,	kIx,	,
uložených	uložený	k2eAgFnPc2d1	uložená
v	v	k7c6	v
Myslánce	Myslánka	k1gFnSc6	Myslánka
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
přemisťovat	přemisťovat	k5eAaImF	přemisťovat
a	a	k8xC	a
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
přímo	přímo	k6eAd1	přímo
jejich	jejich	k3xOp3gFnSc7	jejich
součástí	součást	k1gFnSc7	součást
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
sledovat	sledovat	k5eAaImF	sledovat
děj	děj	k1gInSc4	děj
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
sám	sám	k3xTgInSc4	sám
nebýt	být	k5eNaImF	být
osobami	osoba	k1gFnPc7	osoba
ve	v	k7c6	v
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
spatřen	spatřit	k5eAaPmNgInS	spatřit
<g/>
.	.	kIx.	.
</s>
<s>
Bradavický	Bradavický	k2eAgMnSc1d1	Bradavický
ředitel	ředitel	k1gMnSc1	ředitel
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
<g/>
,	,	kIx,	,
majitel	majitel	k1gMnSc1	majitel
tohoto	tento	k3xDgInSc2	tento
kouzelného	kouzelný	k2eAgInSc2d1	kouzelný
artefaktu	artefakt	k1gInSc2	artefakt
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
pomocí	pomocí	k7c2	pomocí
Myslánky	Myslánka	k1gFnSc2	Myslánka
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
ulehčoval	ulehčovat	k5eAaImAgMnS	ulehčovat
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Myslánky	Myslánka	k1gFnSc2	Myslánka
-	-	kIx~	-
Pensieve	Pensieev	k1gFnSc2	Pensieev
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
autorka	autorka	k1gFnSc1	autorka
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
slova	slovo	k1gNnSc2	slovo
penser	penser	k1gInSc1	penser
(	(	kIx(	(
<g/>
myslet	myslet	k5eAaImF	myslet
<g/>
)	)	kIx)	)
a	a	k8xC	a
anglického	anglický	k2eAgMnSc2d1	anglický
sieve	sieev	k1gFnPc1	sieev
(	(	kIx(	(
<g/>
síto	síto	k1gNnSc1	síto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Myslánka	Myslánka	k1gFnSc1	Myslánka
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
také	také	k9	také
jako	jako	k9	jako
pomůcka	pomůcka	k1gFnSc1	pomůcka
<g/>
,	,	kIx,	,
když	když	k8xS	když
Brumbál	brumbál	k1gMnSc1	brumbál
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Harry	Harra	k1gFnSc2	Harra
<g/>
)	)	kIx)	)
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
do	do	k7c2	do
důležité	důležitý	k2eAgFnSc2d1	důležitá
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
někoho	někdo	k3yInSc4	někdo
jiného	jiný	k2eAgMnSc4d1	jiný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
profesora	profesor	k1gMnSc2	profesor
Křiklana	Křiklan	k1gMnSc2	Křiklan
v	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
díle	dílo	k1gNnSc6	dílo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Neviditelný	viditelný	k2eNgInSc1d1	neviditelný
plášť	plášť	k1gInSc1	plášť
==	==	k?	==
</s>
</p>
<p>
<s>
Neviditelný	viditelný	k2eNgInSc1d1	neviditelný
plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
užitečný	užitečný	k2eAgInSc1d1	užitečný
předmět	předmět	k1gInSc1	předmět
na	na	k7c4	na
porušování	porušování	k1gNnSc4	porušování
školního	školní	k2eAgInSc2d1	školní
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
jej	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
Harryho	Harry	k1gMnSc2	Harry
otce	otec	k1gMnSc2	otec
Jamese	Jamese	k1gFnSc2	Jamese
Pottera	Potter	k1gMnSc2	Potter
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
otci	otec	k1gMnSc3	otec
atd.	atd.	kA	atd.
až	až	k8xS	až
k	k	k7c3	k
Ignotu	Ignot	k1gMnSc3	Ignot
Peverellovi	Peverell	k1gMnSc3	Peverell
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
jej	on	k3xPp3gInSc4	on
údajně	údajně	k6eAd1	údajně
dala	dát	k5eAaPmAgFnS	dát
Smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Harryho	Harryze	k6eAd1	Harryze
neviditelný	viditelný	k2eNgInSc1d1	neviditelný
plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
skutečný	skutečný	k2eAgInSc1d1	skutečný
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgInPc1d1	ostatní
pláště	plášť	k1gInPc1	plášť
jsou	být	k5eAaImIp3nP	být
opatřeny	opatřit	k5eAaPmNgInP	opatřit
zastíracím	zastírací	k2eAgNnSc7d1	zastírací
kouzlem	kouzlo	k1gNnSc7	kouzlo
nebo	nebo	k8xC	nebo
upleteny	uplést	k5eAaPmNgFnP	uplést
ze	z	k7c2	z
srsti	srst	k1gFnSc2	srst
polovida	polovida	k1gFnSc1	polovida
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgMnPc4	ten
časem	časem	k6eAd1	časem
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
účinek	účinek	k1gInSc4	účinek
a	a	k8xC	a
stoprocentně	stoprocentně	k6eAd1	stoprocentně
platí	platit	k5eAaImIp3nS	platit
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
kupříkladu	kupříkladu	k6eAd1	kupříkladu
na	na	k7c4	na
kočky	kočka	k1gFnPc4	kočka
-	-	kIx~	-
ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
mozkomory	mozkomor	k1gMnPc4	mozkomor
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
==	==	k?	==
</s>
</p>
<p>
<s>
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Goblet	Goblet	k1gInSc1	Goblet
of	of	k?	of
Fire	Fire	k1gInSc1	Fire
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
magický	magický	k2eAgInSc4d1	magický
předmět	předmět	k1gInSc4	předmět
využívaný	využívaný	k2eAgInSc4d1	využívaný
k	k	k7c3	k
výběru	výběr	k1gInSc3	výběr
šampiónů	šampión	k1gMnPc2	šampión
do	do	k7c2	do
Turnaje	turnaj	k1gInSc2	turnaj
tří	tři	k4xCgFnPc2	tři
kouzelnických	kouzelnický	k2eAgFnPc2d1	kouzelnická
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
nahrubo	nahrubo	k6eAd1	nahrubo
vyřezaný	vyřezaný	k2eAgInSc1d1	vyřezaný
pohár	pohár	k1gInSc1	pohár
je	být	k5eAaImIp3nS	být
také	také	k9	také
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Nestranný	nestranný	k2eAgMnSc1d1	nestranný
soudce	soudce	k1gMnSc1	soudce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poháru	pohár	k1gInSc6	pohár
hoří	hořet	k5eAaImIp3nP	hořet
modré	modrý	k2eAgInPc1d1	modrý
plamínky	plamínek	k1gInPc1	plamínek
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Ohnivého	ohnivý	k2eAgInSc2d1	ohnivý
poháru	pohár	k1gInSc2	pohár
vhazují	vhazovat	k5eAaImIp3nP	vhazovat
studenti	student	k1gMnPc1	student
pergameny	pergamen	k1gInPc7	pergamen
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
školou	škola	k1gFnSc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
šampiónů	šampión	k1gMnPc2	šampión
pohár	pohár	k1gInSc4	pohár
"	"	kIx"	"
<g/>
vyplivne	vyplivnout	k5eAaPmIp3nS	vyplivnout
<g/>
"	"	kIx"	"
tři	tři	k4xCgInPc4	tři
pergameny	pergamen	k1gInPc4	pergamen
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
a	a	k8xC	a
školou	škola	k1gFnSc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
turnaje	turnaj	k1gInSc2	turnaj
pohár	pohár	k1gInSc1	pohár
zhasne	zhasnout	k5eAaPmIp3nS	zhasnout
a	a	k8xC	a
rozhoří	rozhořet	k5eAaPmIp3nS	rozhořet
se	se	k3xPyFc4	se
až	až	k9	až
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dalšího	další	k2eAgInSc2d1	další
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
pohár	pohár	k1gInSc1	pohár
znovu	znovu	k6eAd1	znovu
rozhoří	rozhořet	k5eAaPmIp3nS	rozhořet
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
uběhnout	uběhnout	k5eAaPmF	uběhnout
třicet	třicet	k4xCc4	třicet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
čaromocný	čaromocný	k2eAgInSc4d1	čaromocný
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
musíte	muset	k5eAaImIp2nP	muset
použít	použít	k5eAaPmF	použít
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgNnSc4d1	silné
matoucí	matoucí	k2eAgNnSc4d1	matoucí
kouzlo	kouzlo	k1gNnSc4	kouzlo
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
ho	on	k3xPp3gMnSc4	on
přiměli	přimět	k5eAaPmAgMnP	přimět
vybrat	vybrat	k5eAaPmF	vybrat
více	hodně	k6eAd2	hodně
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
také	také	k6eAd1	také
zařídil	zařídit	k5eAaPmAgInS	zařídit
Barty	Barta	k1gFnSc2	Barta
Skrk	Skrk	k1gInSc1	Skrk
jr	jr	k?	jr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pohár	pohár	k1gInSc4	pohár
vybral	vybrat	k5eAaPmAgMnS	vybrat
Harryho	Harry	k1gMnSc4	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Pohár	pohár	k1gInSc1	pohár
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
i	i	k8xC	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Turnaj	turnaj	k1gInSc4	turnaj
tří	tři	k4xCgMnPc2	tři
kouzelníků	kouzelník	k1gMnPc2	kouzelník
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vybral	vybrat	k5eAaPmAgMnS	vybrat
výjimečně	výjimečně	k6eAd1	výjimečně
čtyři	čtyři	k4xCgMnPc4	čtyři
šampióny	šampión	k1gMnPc4	šampión
-	-	kIx~	-
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
(	(	kIx(	(
<g/>
za	za	k7c2	za
Bradavice	bradavice	k1gFnSc2	bradavice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cedrica	Cedricus	k1gMnSc2	Cedricus
Diggoryho	Diggory	k1gMnSc2	Diggory
(	(	kIx(	(
<g/>
za	za	k7c2	za
Bradavice	bradavice	k1gFnSc2	bradavice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fleur	Fleur	k1gMnSc1	Fleur
Delacourovou	Delacourův	k2eAgFnSc7d1	Delacourův
(	(	kIx(	(
<g/>
za	za	k7c4	za
Krásnohůlskou	Krásnohůlský	k2eAgFnSc4d1	Krásnohůlský
akademii	akademie	k1gFnSc4	akademie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Viktora	Viktor	k1gMnSc2	Viktor
Kruma	Krum	k1gMnSc2	Krum
(	(	kIx(	(
<g/>
za	za	k7c4	za
školu	škola	k1gFnSc4	škola
Kruval	Kruval	k1gFnSc2	Kruval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamatováček	Pamatováček	k1gInSc4	Pamatováček
==	==	k?	==
</s>
</p>
<p>
<s>
Pamatováček	Pamatováček	k1gInSc1	Pamatováček
je	být	k5eAaImIp3nS	být
kulatý	kulatý	k2eAgInSc1d1	kulatý
předmět	předmět	k1gInSc1	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
zapomnětlivým	zapomnětlivý	k2eAgMnPc3d1	zapomnětlivý
kouzelníkům	kouzelník	k1gMnPc3	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
ve	v	k7c6	v
skleněném	skleněný	k2eAgInSc6d1	skleněný
míčku	míček	k1gInSc6	míček
objeví	objevit	k5eAaPmIp3nS	objevit
červený	červený	k2eAgInSc1d1	červený
kouř	kouř	k1gInSc1	kouř
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
ho	on	k3xPp3gMnSc4	on
kouzelník	kouzelník	k1gMnSc1	kouzelník
držet	držet	k5eAaImF	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kouzelník	kouzelník	k1gMnSc1	kouzelník
na	na	k7c4	na
něco	něco	k3yInSc4	něco
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
Pamatováčku	Pamatováček	k1gInSc6	Pamatováček
nic	nic	k3yNnSc4	nic
neobjeví	objevit	k5eNaPmIp3nS	objevit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Harry	Harra	k1gFnPc4	Harra
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
a	a	k8xC	a
Kamení	kamení	k1gNnSc1	kamení
mudrců	mudrc	k1gMnPc2	mudrc
ho	on	k3xPp3gMnSc4	on
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
babičky	babička	k1gFnSc2	babička
dostal	dostat	k5eAaPmAgInS	dostat
Neville	Neville	k1gInSc1	Neville
Longbottom	Longbottom	k1gInSc1	Longbottom
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
často	často	k6eAd1	často
ani	ani	k8xC	ani
nevíte	vědět	k5eNaImIp2nP	vědět
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
jste	být	k5eAaImIp2nP	být
zapomněli	zapomenout	k5eAaPmAgMnP	zapomenout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pobertův	pobertův	k2eAgInSc1d1	pobertův
plánek	plánek	k1gInSc1	plánek
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
správném	správný	k2eAgNnSc6d1	správné
použití	použití	k1gNnSc6	použití
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Pobertův	pobertův	k2eAgInSc1d1	pobertův
plánek	plánek	k1gInSc1	plánek
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Marauder	Maraudra	k1gFnPc2	Maraudra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Map	mapa	k1gFnPc2	mapa
<g/>
)	)	kIx)	)
každého	každý	k3xTgNnSc2	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
hradě	hrad	k1gInSc6	hrad
kromě	kromě	k7c2	kromě
komnaty	komnata	k1gFnSc2	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrci	tvůrce	k1gMnPc1	tvůrce
plánku	plánek	k1gInSc2	plánek
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Potter	Potter	k1gMnSc1	Potter
<g/>
,	,	kIx,	,
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
<g/>
,	,	kIx,	,
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Pobertové	poberta	k1gMnPc1	poberta
<g/>
.	.	kIx.	.
</s>
<s>
Plánek	plánek	k1gInSc1	plánek
se	se	k3xPyFc4	se
aktivuje	aktivovat	k5eAaBmIp3nS	aktivovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
kouzelník	kouzelník	k1gMnSc1	kouzelník
klepne	klepnout	k5eAaPmIp3nS	klepnout
kouzelnou	kouzelný	k2eAgFnSc7d1	kouzelná
hůlkou	hůlka	k1gFnSc7	hůlka
<g/>
,	,	kIx,	,
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Slavnostně	slavnostně	k6eAd1	slavnostně
přísahám	přísahat	k5eAaImIp1nS	přísahat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
připraven	připravit	k5eAaPmNgInS	připravit
ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
špatnosti	špatnost	k1gFnSc3	špatnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
použití	použití	k1gNnSc6	použití
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
není	být	k5eNaImIp3nS	být
nezbytné	zbytný	k2eNgNnSc1d1	nezbytné
<g/>
,	,	kIx,	,
klepnout	klepnout	k5eAaPmF	klepnout
na	na	k7c4	na
plánek	plánek	k1gInSc4	plánek
hůlkou	hůlka	k1gFnSc7	hůlka
a	a	k8xC	a
říci	říct	k5eAaPmF	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Neplecha	neplecha	k1gFnSc1	neplecha
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
plánku	plánek	k1gInSc2	plánek
skryje	skrýt	k5eAaPmIp3nS	skrýt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ruka	ruka	k1gFnSc1	ruka
slávy	sláva	k1gFnSc2	sláva
==	==	k?	==
</s>
</p>
<p>
<s>
Ruka	ruka	k1gFnSc1	ruka
slávy	sláva	k1gFnSc2	sláva
je	být	k5eAaImIp3nS	být
předmět	předmět	k1gInSc1	předmět
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
prodávají	prodávat	k5eAaImIp3nP	prodávat
u	u	k7c2	u
Borgina	Borgin	k1gMnSc2	Borgin
&	&	k?	&
Burkese	Burkese	k1gFnSc2	Burkese
na	na	k7c6	na
Obrtlé	Obrtlý	k2eAgFnSc6d1	Obrtlá
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
normální	normální	k2eAgFnSc1d1	normální
useknutá	useknutý	k2eAgFnSc1d1	useknutá
lidská	lidský	k2eAgFnSc1d1	lidská
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
seschlá	seschlý	k2eAgFnSc1d1	seschlá
asi	asi	k9	asi
jako	jako	k8xS	jako
ruka	ruka	k1gFnSc1	ruka
mozkomora	mozkomora	k1gFnSc1	mozkomora
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vloží	vložit	k5eAaPmIp3nS	vložit
svíce	svíce	k1gFnPc4	svíce
či	či	k8xC	či
nějaký	nějaký	k3yIgInSc4	nějaký
jiný	jiný	k2eAgInSc4d1	jiný
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
posvítí	posvítit	k5eAaPmIp3nP	posvítit
jen	jen	k9	jen
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ji	on	k3xPp3gFnSc4	on
nese	nést	k5eAaImIp3nS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
díle	díl	k1gInSc6	díl
ji	on	k3xPp3gFnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
Lucius	Lucius	k1gMnSc1	Lucius
Malfoy	Malfoa	k1gFnSc2	Malfoa
svému	svůj	k1gMnSc3	svůj
synovi	syn	k1gMnSc3	syn
<g/>
,	,	kIx,	,
Dracovi	Draec	k1gMnSc3	Draec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ji	on	k3xPp3gFnSc4	on
opět	opět	k6eAd1	opět
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
astronomickou	astronomický	k2eAgFnSc4d1	astronomická
věž	věž	k1gFnSc4	věž
k	k	k7c3	k
Brumbálovi	brumbál	k1gMnSc3	brumbál
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
Harry	Harra	k1gMnSc2	Harra
cestoval	cestovat	k5eAaImAgMnS	cestovat
od	od	k7c2	od
Weasleyových	Weasleyův	k2eAgFnPc2d1	Weasleyova
pomocí	pomoc	k1gFnPc2	pomoc
letaxu	letax	k1gInSc2	letax
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Příčné	příčný	k2eAgFnSc6d1	příčná
ulici	ulice	k1gFnSc6	ulice
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
Obrtlé	Obrtlý	k2eAgFnSc6d1	Obrtlá
ulici	ulice	k1gFnSc6	ulice
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
u	u	k7c2	u
Borgina	Borgin	k2eAgInSc2d1	Borgin
&	&	k?	&
Burkese	Burkese	k1gFnSc1	Burkese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
takovou	takový	k3xDgFnSc4	takový
ruku	ruka	k1gFnSc4	ruka
viděl	vidět	k5eAaImAgMnS	vidět
a	a	k8xC	a
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
smůle	smůla	k1gFnSc3	smůla
i	i	k8xC	i
stiskl	stisknout	k5eAaPmAgMnS	stisknout
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
stisk	stisk	k1gInSc1	stisk
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
silně	silně	k6eAd1	silně
vrátila	vrátit	k5eAaPmAgFnS	vrátit
a	a	k8xC	a
Harry	Harra	k1gFnSc2	Harra
měl	mít	k5eAaImAgMnS	mít
co	co	k3yRnSc4	co
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
stisk	stisk	k1gInSc4	stisk
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slídivé	slídivý	k2eAgNnSc1d1	slídivé
kukátko	kukátko	k1gNnSc1	kukátko
==	==	k?	==
</s>
</p>
<p>
<s>
Slídivé	slídivý	k2eAgNnSc1d1	slídivé
kukátko	kukátko	k1gNnSc1	kukátko
je	být	k5eAaImIp3nS	být
magické	magický	k2eAgNnSc1d1	magické
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
míhají	míhat	k5eAaImIp3nP	míhat
přízračné	přízračný	k2eAgFnPc1d1	přízračná
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nelze	lze	k6eNd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějaká	nějaký	k3yIgFnSc1	nějaký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
opravdu	opravdu	k6eAd1	opravdu
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
,	,	kIx,	,
uvidí	uvidět	k5eAaPmIp3nS	uvidět
kouzelník	kouzelník	k1gMnSc1	kouzelník
před	před	k7c7	před
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
bělma	bělmo	k1gNnSc2	bělmo
jejich	jejich	k3xOp3gNnPc2	jejich
očí	oko	k1gNnPc2	oko
-	-	kIx~	-
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
včas	včas	k6eAd1	včas
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
<g/>
/	/	kIx~	/
<g/>
filmu	film	k1gInSc6	film
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
měl	mít	k5eAaImAgInS	mít
Slídivé	slídivý	k2eAgNnSc4d1	slídivé
kukátko	kukátko	k1gNnSc4	kukátko
učitel	učitel	k1gMnSc1	učitel
Obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
Alastor	Alastor	k1gInSc4	Alastor
Moody	Mooda	k1gFnSc2	Mooda
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
předmětů	předmět	k1gInPc2	předmět
v	v	k7c4	v
Moodyho	Moody	k1gMnSc4	Moody
kabinetu	kabinet	k1gInSc2	kabinet
byla	být	k5eAaImAgNnP	být
stará	starý	k2eAgNnPc1d1	staré
bystrozorská	bystrozorský	k2eAgNnPc1d1	bystrozorské
zařízení	zařízení	k1gNnPc1	zařízení
(	(	kIx(	(
<g/>
Moody	Mooda	k1gFnSc2	Mooda
býval	bývat	k5eAaImAgMnS	bývat
bystrozorem	bystrozor	k1gInSc7	bystrozor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Viteál	Viteál	k1gInSc4	Viteál
==	==	k?	==
</s>
</p>
<p>
<s>
Viteál	Viteál	k1gInSc1	Viteál
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
též	též	k9	též
anglicky	anglicky	k6eAd1	anglicky
Horcrux	Horcrux	k1gInSc4	Horcrux
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
knize	kniha	k1gFnSc6	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
pojem	pojem	k1gInSc1	pojem
viteál	viteála	k1gFnPc2	viteála
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
až	až	k9	až
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
šesté	šestý	k4xOgFnSc6	šestý
<g/>
,	,	kIx,	,
Harry	Harra	k1gFnPc1	Harra
Potter	Pottrum	k1gNnPc2	Pottrum
a	a	k8xC	a
Princ	princa	k1gFnPc2	princa
dvojí	dvojit	k5eAaImIp3nP	dvojit
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Voldemort	Voldemort	k1gInSc1	Voldemort
se	se	k3xPyFc4	se
o	o	k7c6	o
viteálech	viteál	k1gInPc6	viteál
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
Čáry	čára	k1gFnSc2	čára
nejtemnější	temný	k2eAgMnSc1d3	nejtemnější
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
fungování	fungování	k1gNnSc4	fungování
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
u	u	k7c2	u
Horacia	Horacius	k1gMnSc2	Horacius
Křiklana	Křiklan	k1gMnSc2	Křiklan
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
předmět	předmět	k1gInSc4	předmět
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgMnSc4	který
černokněžník	černokněžník	k1gMnSc1	černokněžník
schová	schovat	k5eAaPmIp3nS	schovat
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nesmrtelným	nesmrtelný	k1gMnSc7	nesmrtelný
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
svět	svět	k1gInSc4	svět
opustit	opustit	k5eAaPmF	opustit
celá	celý	k2eAgFnSc1d1	celá
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
čaroděj	čaroděj	k1gMnSc1	čaroděj
stává	stávat	k5eAaImIp3nS	stávat
nesmrtelným	smrtelný	k2eNgMnSc7d1	nesmrtelný
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
viteál	viteál	k1gMnSc1	viteál
není	být	k5eNaImIp3nS	být
zlikvidován	zlikvidován	k2eAgMnSc1d1	zlikvidován
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
tedy	tedy	k9	tedy
plně	plně	k6eAd1	plně
usmrtit	usmrtit	k5eAaPmF	usmrtit
ani	ani	k8xC	ani
jeho	on	k3xPp3gMnSc2	on
původce	původce	k1gMnSc2	původce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
není	být	k5eNaImIp3nS	být
řečeno	říct	k5eAaPmNgNnS	říct
jací	jaký	k3yIgMnPc1	jaký
další	další	k2eAgMnPc1d1	další
černokněžníci	černokněžník	k1gMnPc1	černokněžník
vitéaly	vitéala	k1gFnSc2	vitéala
měli	mít	k5eAaImAgMnP	mít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
určitě	určitě	k6eAd1	určitě
měli	mít	k5eAaImAgMnP	mít
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
viteál	viteál	k1gMnSc1	viteál
<g/>
,	,	kIx,	,
Voldemort	Voldemort	k1gInSc1	Voldemort
byl	být	k5eAaImAgInS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
7	[number]	k4	7
viteály	viteál	k1gInPc7	viteál
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
viteálů	viteál	k1gMnPc2	viteál
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
tolikrát	tolikrát	k6eAd1	tolikrát
rozpolcená	rozpolcený	k2eAgFnSc1d1	rozpolcená
duše	duše	k1gFnSc1	duše
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
změnu	změna	k1gFnSc4	změna
Voldemortovy	Voldemortův	k2eAgFnSc2d1	Voldemortova
podoby	podoba	k1gFnSc2	podoba
(	(	kIx(	(
<g/>
hadí	hadí	k2eAgInSc1d1	hadí
nos	nos	k1gInSc1	nos
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgInPc1	žádný
rty	ret	k1gInPc1	ret
<g/>
,	,	kIx,	,
šedivá	šedivý	k2eAgFnSc1d1	šedivá
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
viteál	viteát	k5eAaPmAgMnS	viteát
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
zřejmě	zřejmě	k6eAd1	zřejmě
cokoli	cokoli	k3yInSc4	cokoli
<g/>
,	,	kIx,	,
stvořit	stvořit	k5eAaPmF	stvořit
ho	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
jen	jen	k9	jen
při	při	k7c6	při
opovrženíhodném	opovrženíhodný	k2eAgInSc6d1	opovrženíhodný
činu	čin	k1gInSc6	čin
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
je	být	k5eAaImIp3nS	být
vražda	vražda	k1gFnSc1	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
lze	lze	k6eAd1	lze
roztrhnout	roztrhnout	k5eAaPmF	roztrhnout
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
ve	v	k7c6	v
dví	dví	k1xP	dví
<g/>
.	.	kIx.	.
</s>
<s>
Voldemort	Voldemort	k1gInSc1	Voldemort
roztrhnul	roztrhnout	k5eAaPmAgInS	roztrhnout
postupně	postupně	k6eAd1	postupně
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
na	na	k7c4	na
7	[number]	k4	7
dílů	díl	k1gInPc2	díl
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
šest	šest	k4xCc1	šest
viteálů	viteál	k1gInPc2	viteál
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
vlastním	vlastní	k2eAgNnSc6d1	vlastní
těle	tělo	k1gNnSc6	tělo
<g/>
;	;	kIx,	;
Voldemort	Voldemort	k1gInSc1	Voldemort
totiž	totiž	k9	totiž
věřil	věřit	k5eAaImAgInS	věřit
v	v	k7c4	v
magickou	magický	k2eAgFnSc4d1	magická
moc	moc	k1gFnSc4	moc
čísla	číslo	k1gNnSc2	číslo
7	[number]	k4	7
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kniha	kniha	k1gFnSc1	kniha
Princ	princa	k1gFnPc2	princa
dvojí	dvojit	k5eAaImIp3nP	dvojit
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
byl	být	k5eAaImAgInS	být
stvořen	stvořen	k2eAgInSc1d1	stvořen
neúmyslně	úmyslně	k6eNd1	úmyslně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c4	po
zabití	zabití	k1gNnSc4	zabití
rodičů	rodič	k1gMnPc2	rodič
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kus	kus	k1gInSc4	kus
Voldemortovy	Voldemortův	k2eAgFnSc2d1	Voldemortova
duše	duše	k1gFnSc2	duše
přichytl	přichytnout	k5eAaPmAgMnS	přichytnout
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jediné	jediné	k1gNnSc4	jediné
žijící	žijící	k2eAgMnPc1d1	žijící
<g/>
,	,	kIx,	,
na	na	k7c4	na
Harryho	Harry	k1gMnSc4	Harry
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
mluví	mluvit	k5eAaImIp3nS	mluvit
hadím	hadí	k2eAgInSc7d1	hadí
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
Voldemortovy	Voldemortův	k2eAgFnPc4d1	Voldemortova
vize	vize	k1gFnPc4	vize
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
Voldemort	Voldemort	k1gInSc4	Voldemort
nevěděl	vědět	k5eNaImAgMnS	vědět
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
schovával	schovávat	k5eAaImAgMnS	schovávat
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
7	[number]	k4	7
<g/>
.	.	kIx.	.
viteál	viteál	k1gInSc1	viteál
(	(	kIx(	(
<g/>
osmý	osmý	k4xOgInSc1	osmý
kousek	kousek	k1gInSc1	kousek
duše	duše	k1gFnSc2	duše
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
použít	použít	k5eAaPmF	použít
<g/>
)	)	kIx)	)
z	z	k7c2	z
Naginiho	Nagini	k1gMnSc2	Nagini
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Voldemort	Voldemort	k1gInSc1	Voldemort
si	se	k3xPyFc3	se
nevybíral	vybírat	k5eNaImAgInS	vybírat
kdejaké	kdejaký	k3yIgInPc4	kdejaký
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Vybíral	vybírat	k5eAaImAgMnS	vybírat
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
mají	mít	k5eAaImIp3nP	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
magickou	magický	k2eAgFnSc4d1	magická
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
či	či	k8xC	či
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgMnS	použít
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
následující	následující	k2eAgInPc4d1	následující
předměty	předmět	k1gInPc4	předmět
<g/>
:	:	kIx,	:
svůj	svůj	k3xOyFgInSc4	svůj
deník	deník	k1gInSc4	deník
(	(	kIx(	(
<g/>
při	při	k7c6	při
smrti	smrt	k1gFnSc6	smrt
Ufňukané	ufňukaný	k2eAgFnSc2d1	ufňukaná
Uršuly	Uršula	k1gFnSc2	Uršula
<g/>
...	...	k?	...
zničil	zničit	k5eAaPmAgMnS	zničit
ho	on	k3xPp3gMnSc2	on
Harry	Harra	k1gMnSc2	Harra
v	v	k7c6	v
Tajemné	tajemný	k2eAgFnSc6d1	tajemná
komnatě	komnata	k1gFnSc6	komnata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prsten	prsten	k1gInSc1	prsten
svého	svůj	k3xOyFgMnSc4	svůj
dědečka	dědeček	k1gMnSc4	dědeček
Gaunta	Gaunt	k1gMnSc4	Gaunt
(	(	kIx(	(
<g/>
při	při	k7c6	při
zabití	zabití	k1gNnSc6	zabití
svého	svůj	k1gMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Toma	Tom	k1gMnSc2	Tom
Raddla	Raddla	k1gMnSc2	Raddla
<g/>
...	...	k?	...
zničen	zničit	k5eAaPmNgInS	zničit
Albusem	Albus	k1gMnSc7	Albus
Brumbálem	brumbál	k1gMnSc7	brumbál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
medailonek	medailonek	k1gInSc1	medailonek
Salazara	Salazar	k1gMnSc2	Salazar
Zmijozela	Zmijozela	k1gMnSc2	Zmijozela
(	(	kIx(	(
<g/>
který	který	k3yQgInSc4	který
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
po	po	k7c6	po
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
potomek	potomek	k1gMnSc1	potomek
Zmijozela	Zmijozela	k1gMnSc1	Zmijozela
<g/>
)	)	kIx)	)
a	a	k8xC	a
který	který	k3yQgInSc4	který
jeho	jeho	k3xOp3gInSc4	jeho
matka	matka	k1gFnSc1	matka
prodala	prodat	k5eAaPmAgFnS	prodat
za	za	k7c4	za
10	[number]	k4	10
galeonů	galeon	k1gInPc2	galeon
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vůbec	vůbec	k9	vůbec
přežila	přežít	k5eAaPmAgFnS	přežít
<g/>
...	...	k?	...
zničen	zničit	k5eAaPmNgInS	zničit
Ronem	ron	k1gInSc7	ron
Weasleym	Weasleym	k1gInSc1	Weasleym
pomocí	pomocí	k7c2	pomocí
meče	meč	k1gInSc2	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diadém	diadém	k1gInSc1	diadém
Roweny	Rowena	k1gFnSc2	Rowena
z	z	k7c2	z
Havraspáru	Havraspár	k1gInSc2	Havraspár
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
zničen	zničen	k2eAgInSc1d1	zničen
Vincentem	Vincent	k1gMnSc7	Vincent
Crabbem	Crabb	k1gMnSc7	Crabb
pomocí	pomocí	k7c2	pomocí
zložáru	zložár	k1gInSc2	zložár
v	v	k7c6	v
Komnatě	komnata	k1gFnSc6	komnata
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
zničen	zničit	k5eAaPmNgInS	zničit
zložárem	zložár	k1gInSc7	zložár
a	a	k8xC	a
Harrym	Harrym	k1gInSc1	Harrym
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohár	pohár	k1gInSc4	pohár
Helgy	Helga	k1gFnSc2	Helga
z	z	k7c2	z
Mrzimoru	Mrzimor	k1gInSc2	Mrzimor
(	(	kIx(	(
<g/>
při	při	k7c6	při
smrti	smrt	k1gFnSc6	smrt
Hepziby	Hepziba	k1gFnSc2	Hepziba
Smithové	Smithová	k1gFnSc2	Smithová
<g/>
...	...	k?	...
zničen	zničit	k5eAaPmNgMnS	zničit
Hermionou	Hermiona	k1gFnSc7	Hermiona
pomocí	pomocí	k7c2	pomocí
baziliškova	baziliškův	k2eAgInSc2d1	baziliškův
zubu	zub	k1gInSc2	zub
v	v	k7c6	v
Tajemné	tajemný	k2eAgFnSc6d1	tajemná
komnatě	komnata	k1gFnSc6	komnata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc4	svůj
hada	had	k1gMnSc4	had
Naginiho	Nagini	k1gMnSc4	Nagini
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
zabil	zabít	k5eAaPmAgMnS	zabít
ho	on	k3xPp3gMnSc4	on
Neville	Nevill	k1gMnSc4	Nevill
Longbottom	Longbottom	k1gInSc4	Longbottom
mečem	meč	k1gInSc7	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gMnSc2	Harra
sám	sám	k3xTgMnSc1	sám
paradoxně	paradoxně	k6eAd1	paradoxně
ze	z	k7c2	z
7	[number]	k4	7
viteálů	viteál	k1gInPc2	viteál
zničil	zničit	k5eAaPmAgInS	zničit
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmu	film	k1gInSc6	film
dva	dva	k4xCgInPc4	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
kouzelníkovo	kouzelníkův	k2eAgNnSc1d1	Kouzelníkovo
tělo	tělo	k1gNnSc1	tělo
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
si	se	k3xPyFc3	se
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
vytvořit	vytvořit	k5eAaPmF	vytvořit
tělo	tělo	k1gNnSc4	tělo
nové	nový	k2eAgNnSc4d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Voldemort	Voldemort	k1gInSc1	Voldemort
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
použil	použít	k5eAaPmAgInS	použít
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
knize	kniha	k1gFnSc6	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
maso	maso	k1gNnSc1	maso
služebníka	služebník	k1gMnSc2	služebník
(	(	kIx(	(
<g/>
darované	darovaný	k2eAgFnPc1d1	darovaná
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kost	kost	k1gFnSc4	kost
otce	otec	k1gMnSc2	otec
(	(	kIx(	(
<g/>
darované	darovaný	k2eAgFnPc1d1	darovaná
nevědomky	nevědomky	k6eAd1	nevědomky
<g/>
)	)	kIx)	)
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
nepřítele	nepřítel	k1gMnSc2	nepřítel
(	(	kIx(	(
<g/>
získané	získaný	k2eAgNnSc1d1	získané
násilím	násilí	k1gNnSc7	násilí
-	-	kIx~	-
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
krev	krev	k1gFnSc4	krev
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
černokněžník	černokněžník	k1gMnSc1	černokněžník
takové	takový	k3xDgNnSc4	takový
tělo	tělo	k1gNnSc4	tělo
opatří	opatřit	k5eAaPmIp3nS	opatřit
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
jen	jen	k9	jen
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
položivotem	položivot	k1gInSc7	položivot
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
nejmenší	malý	k2eAgMnSc1d3	nejmenší
z	z	k7c2	z
duchů	duch	k1gMnPc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
se	se	k3xPyFc4	se
právě	právě	k9	právě
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
,	,	kIx,	,
když	když	k8xS	když
použil	použít	k5eAaPmAgInS	použít
smrtící	smrtící	k2eAgFnSc4d1	smrtící
kletbu	kletba	k1gFnSc4	kletba
Avada	Avad	k1gMnSc2	Avad
kedavra	kedavr	k1gMnSc2	kedavr
na	na	k7c4	na
Harryho	Harry	k1gMnSc4	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Kletba	kletba	k1gFnSc1	kletba
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
Voldemort	Voldemort	k1gInSc4	Voldemort
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
duše	duše	k1gFnSc2	duše
se	se	k3xPyFc4	se
ale	ale	k9	ale
přenesla	přenést	k5eAaPmAgFnS	přenést
na	na	k7c4	na
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nechtěně	chtěně	k6eNd1	chtěně
stal	stát	k5eAaPmAgMnS	stát
sedmým	sedmý	k4xOgMnSc7	sedmý
viteálem	viteál	k1gMnSc7	viteál
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Voldemort	Voldemort	k1gInSc4	Voldemort
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Začarované	začarovaný	k2eAgInPc1d1	začarovaný
galeony	galeon	k1gInPc1	galeon
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
díle	dílo	k1gNnSc6	dílo
rozdávala	rozdávat	k5eAaImAgFnS	rozdávat
Hermiona	Hermiona	k1gFnSc1	Hermiona
členům	člen	k1gMnPc3	člen
Brumbálovy	brumbálův	k2eAgFnSc2d1	Brumbálova
armády	armáda	k1gFnSc2	armáda
falešné	falešný	k2eAgInPc1d1	falešný
peníze	peníz	k1gInPc1	peníz
se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
Harry	Harra	k1gFnPc4	Harra
svolat	svolat	k5eAaPmF	svolat
další	další	k2eAgFnSc4d1	další
schůzku	schůzka	k1gFnSc4	schůzka
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
přijít	přijít	k5eAaPmF	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
se	s	k7c7	s
znamením	znamení	k1gNnSc7	znamení
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
šestém	šestý	k4xOgNnSc6	šestý
díle	dílo	k1gNnSc6	dílo
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
dorozumívání	dorozumívání	k1gNnSc2	dorozumívání
potom	potom	k6eAd1	potom
používala	používat	k5eAaImAgFnS	používat
i	i	k9	i
Madam	madam	k1gFnSc1	madam
Rosmerta	Rosmerta	k1gFnSc1	Rosmerta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
kletby	kletba	k1gFnSc2	kletba
Imperius	Imperius	k1gInSc1	Imperius
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dala	dát	k5eAaPmAgFnS	dát
vědět	vědět	k5eAaImF	vědět
Dracu	Drac	k1gMnSc3	Drac
Malfoyovi	Malfoya	k1gMnSc3	Malfoya
<g/>
,	,	kIx,	,
že	že	k8xS	že
Brumbál	brumbál	k1gMnSc1	brumbál
opustil	opustit	k5eAaPmAgMnS	opustit
Bradavice	bradavice	k1gFnPc4	bradavice
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
rovnou	rovnou	k6eAd1	rovnou
na	na	k7c4	na
Astronomickou	astronomický	k2eAgFnSc4d1	astronomická
věž	věž	k1gFnSc4	věž
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
níž	jenž	k3xRgFnSc7	jenž
čnělo	čnět	k5eAaImAgNnS	čnět
znamení	znamení	k1gNnSc1	znamení
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zatemňovač	Zatemňovač	k1gInSc1	Zatemňovač
(	(	kIx(	(
<g/>
Zhasínadlo	zhasínadlo	k1gNnSc1	zhasínadlo
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Zatemňovač	Zatemňovač	k1gInSc1	Zatemňovač
(	(	kIx(	(
<g/>
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
zván	zván	k2eAgInSc4d1	zván
Zhasínadlo	zhasínadlo	k1gNnSc4	zhasínadlo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Deluminator	Deluminator	k1gInSc1	Deluminator
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vynález	vynález	k1gInSc1	vynález
Albuse	Albuse	k1gFnSc2	Albuse
Percivala	Percivala	k1gMnSc2	Percivala
Wulfrica	Wulfricus	k1gMnSc2	Wulfricus
Briana	Brian	k1gMnSc2	Brian
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
jím	on	k3xPp3gNnSc7	on
pouhým	pouhý	k2eAgNnSc7d1	pouhé
stisknutím	stisknutí	k1gNnSc7	stisknutí
knoflíku	knoflík	k1gInSc2	knoflík
zhasnout	zhasnout	k5eAaPmF	zhasnout
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
lucernu	lucerna	k1gFnSc4	lucerna
a	a	k8xC	a
jedním	jeden	k4xCgNnSc7	jeden
cvaknutím	cvaknutí	k1gNnSc7	cvaknutí
ji	on	k3xPp3gFnSc4	on
zase	zase	k9	zase
rozsvítit	rozsvítit	k5eAaPmF	rozsvítit
<g/>
,	,	kIx,	,
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
stříbrný	stříbrný	k2eAgInSc4d1	stříbrný
zapalovač	zapalovač	k1gInSc4	zapalovač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
jej	on	k3xPp3gMnSc4	on
Brumbál	brumbál	k1gMnSc1	brumbál
odkázal	odkázat	k5eAaPmAgMnS	odkázat
v	v	k7c4	v
závěti	závěť	k1gFnPc4	závěť
Ronovi	Ron	k1gMnSc3	Ron
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
Harrymu	Harrym	k1gInSc3	Harrym
a	a	k8xC	a
Hermioně	Hermiona	k1gFnSc3	Hermiona
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jím	jíst	k5eAaImIp1nS	jíst
Brumbál	brumbál	k1gMnSc1	brumbál
zatemnil	zatemnit	k5eAaPmAgMnS	zatemnit
Zobí	Zobí	k2eAgFnSc4d1	Zobí
ulici	ulice	k1gFnSc4	ulice
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
tak	tak	k8xS	tak
nikým	nikdo	k3yNnSc7	nikdo
neviděn	viděn	k2eNgInSc1d1	neviděn
dořešit	dořešit	k5eAaPmF	dořešit
Harryho	Harryha	k1gFnSc5	Harryha
předání	předání	k1gNnSc3	předání
Dursleyovým	Dursleyův	k2eAgMnPc3d1	Dursleyův
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jím	jíst	k5eAaImIp1nS	jíst
Pošuk	pošuk	k1gMnSc1	pošuk
Moody	Mood	k1gInPc4	Mood
zatemnil	zatemnit	k5eAaPmAgMnS	zatemnit
Grimmauldovo	Grimmauldův	k2eAgNnSc4d1	Grimmauldovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
také	také	k9	také
v	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
Brumbál	brumbál	k1gMnSc1	brumbál
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
ve	v	k7c4	v
své	svůj	k3xOyFgFnPc4	svůj
závěti	závěť	k1gFnPc4	závěť
Ronovi	Ronův	k2eAgMnPc1d1	Ronův
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
objevuje	objevovat	k5eAaImIp3nS	objevovat
jeho	jeho	k3xOp3gFnSc4	jeho
novou	nový	k2eAgFnSc4d1	nová
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xC	jako
světelná	světelný	k2eAgFnSc1d1	světelná
koule	koule	k1gFnSc1	koule
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
vletěla	vletět	k5eAaPmAgFnS	vletět
do	do	k7c2	do
prsou	prsa	k1gNnPc2	prsa
a	a	k8xC	a
dovedla	dovést	k5eAaPmAgFnS	dovést
ho	on	k3xPp3gMnSc4	on
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Hermioně	Hermiona	k1gFnSc3	Hermiona
a	a	k8xC	a
Harrymu	Harrym	k1gInSc3	Harrym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
z	z	k7c2	z
Erisedu	Erised	k1gMnSc6	Erised
==	==	k?	==
</s>
</p>
<p>
<s>
Kouzelné	kouzelný	k2eAgNnSc1d1	kouzelné
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
objevující	objevující	k2eAgMnSc1d1	objevující
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
pohlédne	pohlédnout	k5eAaPmIp3nS	pohlédnout
<g/>
,	,	kIx,	,
neukáže	ukázat	k5eNaPmIp3nS	ukázat
jeho	jeho	k3xOp3gInSc4	jeho
odraz	odraz	k1gInSc4	odraz
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jeho	jeho	k3xOp3gNnPc4	jeho
nejhlubší	hluboký	k2eAgNnPc4d3	nejhlubší
přání	přání	k1gNnPc4	přání
a	a	k8xC	a
touhy	touha	k1gFnPc4	touha
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnozí	mnohý	k2eAgMnPc1d1	mnohý
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
strávili	strávit	k5eAaPmAgMnP	strávit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
zešíleli	zešílet	k5eAaPmAgMnP	zešílet
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
objeví	objevit	k5eAaPmIp3nP	objevit
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
díky	díky	k7c3	díky
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
Ronovi	Ron	k1gMnSc3	Ron
a	a	k8xC	a
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
pomocí	pomocí	k7c2	pomocí
něj	on	k3xPp3gInSc2	on
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
přemůže	přemoct	k5eAaPmIp3nS	přemoct
lorda	lord	k1gMnSc4	lord
Voldemorta	Voldemort	k1gMnSc4	Voldemort
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
</s>
</p>
