<s>
Hrdly	hrdla	k1gFnPc1
(	(	kIx(
<g/>
zámek	zámek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hrdly	hrdnout	k5eAaImAgFnP
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Sloh	sloha	k1gFnPc2
</s>
<s>
barokní	barokní	k2eAgMnSc1d1
Architekt	architekt	k1gMnSc1
</s>
<s>
Kilián	Kilián	k1gMnSc1
Ignác	Ignác	k1gMnSc1
Dientzenhofer	Dientzenhofer	k1gMnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1746	#num#	k4
<g/>
–	–	k?
<g/>
1747	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Hrdly	hrdla	k1gFnPc1
<g/>
,	,	kIx,
Bohušovice	Bohušovice	k1gFnPc1
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Hrdly	hrdla	k1gFnPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
17724	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2046	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hrdly	hrdla	k1gFnPc4
je	být	k5eAaImIp3nS
název	název	k1gInSc1
zámku	zámek	k1gInSc2
s	s	k7c7
hospodářským	hospodářský	k2eAgInSc7d1
dvorem	dvůr	k1gInSc7
ve	v	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
u	u	k7c2
Bohušovic	Bohušovice	k1gFnPc2
nad	nad	k7c7
Ohří	Ohře	k1gFnSc7
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
postaven	postavit	k5eAaPmNgMnS
v	v	k7c6
průběhu	průběh	k1gInSc6
šestnáctého	šestnáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
dochovaná	dochovaný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
barokní	barokní	k2eAgFnSc2d1
přestavby	přestavba	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
Kilián	Kilián	k1gMnSc1
Ignác	Ignác	k1gMnSc1
Dientzenhofer	Dientzenhofer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářský	hospodářský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
se	se	k3xPyFc4
zámkem	zámek	k1gInSc7
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xC,k8xS
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Samostatně	samostatně	k6eAd1
byla	být	k5eAaImAgFnS
jako	jako	k8xS,k8xC
památka	památka	k1gFnSc1
chráněna	chránit	k5eAaImNgFnS
také	také	k9
kovárna	kovárna	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
stála	stát	k5eAaImAgFnS
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
vnitřního	vnitřní	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
částečném	částečný	k2eAgNnSc6d1
zřícení	zřícení	k1gNnSc6
stavby	stavba	k1gFnSc2
existenci	existence	k1gFnSc4
tohoto	tento	k3xDgInSc2
objektu	objekt	k1gInSc2
ukončila	ukončit	k5eAaPmAgFnS
demolice	demolice	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgNnSc1
panské	panský	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Hrdlech	hrdlo	k1gNnPc6
zmiňováno	zmiňovat	k5eAaImNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1620	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
„	„	k?
<g/>
ves	ves	k1gFnSc4
s	s	k7c7
dvorem	dvůr	k1gInSc7
a	a	k8xC
rezidencí	rezidence	k1gFnSc7
<g/>
“	“	k?
koupil	koupit	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
Vilém	Vilém	k1gMnSc1
z	z	k7c2
Roupova	roupův	k2eAgInSc2d1
na	na	k7c6
Žitenicích	Žitenice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
jím	on	k3xPp3gMnSc7
renesanční	renesanční	k2eAgInPc4d1
zámek	zámek	k1gInSc1
postavený	postavený	k2eAgInSc1d1
pravděpodobně	pravděpodobně	k6eAd1
břevnovským	břevnovský	k2eAgInSc7d1
klášterem	klášter	k1gInSc7
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
roku	rok	k1gInSc2
1523	#num#	k4
vykoupil	vykoupit	k5eAaPmAgInS
panství	panství	k1gNnSc4
ze	z	k7c2
zástavy	zástava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
potlačení	potlačení	k1gNnSc6
stavovského	stavovský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1621	#num#	k4
majetek	majetek	k1gInSc1
Václava	Václav	k1gMnSc2
z	z	k7c2
Roupova	roupův	k2eAgInSc2d1
zkonfiskován	zkonfiskován	k2eAgInSc1d1
a	a	k8xC
Hrdly	Hrdly	k1gFnPc1
se	se	k3xPyFc4
vrátily	vrátit	k5eAaPmAgFnP
zpět	zpět	k6eAd1
klášteru	klášter	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
válek	válka	k1gFnPc2
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
byla	být	k5eAaImAgFnS
vesnice	vesnice	k1gFnSc1
i	i	k9
s	s	k7c7
dvorem	dvůr	k1gInSc7
v	v	k7c6
letech	léto	k1gNnPc6
1742	#num#	k4
a	a	k8xC
1744	#num#	k4
vydrancována	vydrancovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poškozené	poškozený	k2eAgNnSc1d1
renesanční	renesanční	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
letech	let	k1gInPc6
1746	#num#	k4
<g/>
–	–	k?
<g/>
1747	#num#	k4
nahrazeno	nahradit	k5eAaPmNgNnS
barokním	barokní	k2eAgInSc7d1
zámkem	zámek	k1gInSc7
postaveným	postavený	k2eAgInSc7d1
podle	podle	k7c2
projektu	projekt	k1gInSc2
Kiliána	Kilián	k1gMnSc2
Ignáce	Ignác	k1gMnSc2
Dientzenhofera	Dientzenhofer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zámek	zámek	k1gInSc1
roku	rok	k1gInSc2
1824	#num#	k4
vyhořel	vyhořet	k5eAaPmAgInS
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
požáru	požár	k1gInSc2
zanikla	zaniknout	k5eAaPmAgFnS
část	část	k1gFnSc1
umělecké	umělecký	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Areál	areál	k1gInSc1
zámku	zámek	k1gInSc2
není	být	k5eNaImIp3nS
veřejnosti	veřejnost	k1gFnSc3
přístupný	přístupný	k2eAgMnSc1d1
<g/>
,	,	kIx,
vnitřní	vnitřní	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
byly	být	k5eAaImAgFnP
přestavěny	přestavět	k5eAaPmNgFnP
na	na	k7c4
byty	byt	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
</s>
<s>
Renesanční	renesanční	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
tvořila	tvořit	k5eAaImAgFnS
jednopatrová	jednopatrový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
krytá	krytý	k2eAgFnSc1d1
šindelovou	šindelový	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
s	s	k7c7
hodinovou	hodinový	k2eAgFnSc7d1
věží	věž	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k6eAd1
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
kaple	kaple	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Tomáše	Tomáš	k1gMnSc2
<g/>
,	,	kIx,
sklepy	sklep	k1gInPc1
s	s	k7c7
lednicí	lednice	k1gFnSc7
<g/>
,	,	kIx,
předsíň	předsíň	k1gFnSc1
<g/>
,	,	kIx,
komora	komora	k1gFnSc1
<g/>
,	,	kIx,
kuchyně	kuchyně	k1gFnSc1
a	a	k8xC
šest	šest	k4xCc4
pokojů	pokoj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
barokní	barokní	k2eAgFnSc6d1
přestavbě	přestavba	k1gFnSc6
zámek	zámek	k1gInSc1
získal	získat	k5eAaPmAgInS
obdélný	obdélný	k2eAgInSc4d1
půdorys	půdorys	k1gInSc4
se	s	k7c7
čtyřbokou	čtyřboký	k2eAgFnSc7d1
věžicí	věžicí	k?
na	na	k7c6
střeše	střecha	k1gFnSc6
a	a	k8xC
s	s	k7c7
krátkým	krátký	k2eAgNnSc7d1
příčným	příčný	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
na	na	k7c6
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přízemní	přízemní	k2eAgFnPc4d1
místnosti	místnost	k1gFnPc4
jsou	být	k5eAaImIp3nP
zaklenuté	zaklenutý	k2eAgInPc1d1
valenými	valený	k2eAgFnPc7d1
a	a	k8xC
křížovými	křížový	k2eAgFnPc7d1
klenbami	klenba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sousedství	sousedství	k1gNnSc6
věžice	věžice	k?
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
zámecká	zámecký	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
zvýrazněné	zvýrazněný	k2eAgFnSc2d1
párem	pár	k1gInSc7
pilastrů	pilastr	k1gInPc2
na	na	k7c6
obou	dva	k4xCgNnPc6
průčelích	průčelí	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
dvora	dvůr	k1gInSc2
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
vstupuje	vstupovat	k5eAaImIp3nS
obdélným	obdélný	k2eAgInSc7d1
portálem	portál	k1gInSc7
se	s	k7c7
supraportou	supraporta	k1gFnSc7
a	a	k8xC
osvětluje	osvětlovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
velké	velký	k2eAgNnSc1d1
kasulové	kasulový	k2eAgNnSc1d1
okno	okno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interiér	interiér	k1gInSc1
zaklenutý	zaklenutý	k2eAgInSc1d1
plackovou	plackový	k2eAgFnSc7d1
klenbou	klenba	k1gFnSc7
s	s	k7c7
postranními	postranní	k2eAgFnPc7d1
poli	pole	k1gNnSc6
valené	valený	k2eAgFnSc2d1
klenby	klenba	k1gFnSc2
má	mít	k5eAaImIp3nS
zkosené	zkosený	k2eAgInPc4d1
rohy	roh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klenba	klenba	k1gFnSc1
je	být	k5eAaImIp3nS
zdobená	zdobený	k2eAgFnSc1d1
freskovými	freskový	k2eAgFnPc7d1
malbami	malba	k1gFnPc7
českých	český	k2eAgMnPc2d1
patronů	patron	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
128386	#num#	k4
:	:	kIx,
Zemědělský	zemědělský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
1	#num#	k4
2	#num#	k4
ANDĚL	Anděla	k1gFnPc2
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
,	,	kIx,
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrady	hrad	k1gInPc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
a	a	k8xC
tvrze	tvrz	k1gFnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
664	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Hrdly	hrdla	k1gFnSc2
–	–	k?
zámek	zámek	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
156	#num#	k4
<g/>
–	–	k?
<g/>
157	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
POCHE	POCHE	kA
<g/>
,	,	kIx,
Emanuel	Emanuel	k1gMnSc1
<g/>
,	,	kIx,
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělecké	umělecký	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	A	kA
<g/>
/	/	kIx~
<g/>
J.	J.	kA
Svazek	svazek	k1gInSc1
I.	I.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
644	#num#	k4
s.	s.	k?
Heslo	heslo	k1gNnSc4
Hrdly	hrdla	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
469	#num#	k4
<g/>
–	–	k?
<g/>
470	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
zámků	zámek	k1gInPc2
v	v	k7c6
Ústeckém	ústecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hrdly	hrdla	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Hrdly	hrdla	k1gFnPc1
na	na	k7c6
webu	web	k1gInSc6
Hrady	hrad	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Historie	historie	k1gFnSc1
</s>
