<p>
<s>
will	will	k1gInSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Will	Will	k1gMnSc1	Will
<g/>
.	.	kIx.	.
<g/>
I.	I.	kA	I.
<g/>
Am	Am	k1gFnSc6	Am
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
William	William	k1gInSc1	William
Adams	Adams	k1gInSc1	Adams
<g/>
;	;	kIx,	;
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1975	[number]	k4	1975
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
rapper	rapper	k1gMnSc1	rapper
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
příležitostný	příležitostný	k2eAgMnSc1d1	příležitostný
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
a	a	k8xC	a
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peas	k1gInSc4	Peas
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
sestru	sestra	k1gFnSc4	sestra
Celene	Celen	k1gInSc5	Celen
a	a	k8xC	a
bratra	bratr	k1gMnSc4	bratr
Carla	Carl	k1gMnSc4	Carl
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
psaní	psaní	k1gNnSc2	psaní
písní	píseň	k1gFnPc2	píseň
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
breakdance	breakdance	k1gFnSc1	breakdance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
ho	on	k3xPp3gMnSc4	on
už	už	k9	už
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
podporovala	podporovat	k5eAaImAgFnS	podporovat
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neskončil	skončit	k5eNaPmAgMnS	skončit
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
kalifornských	kalifornský	k2eAgNnPc2d1	kalifornské
ghet	gheto	k1gNnPc2	gheto
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gMnPc2	jeho
spolužáků	spolužák	k1gMnPc2	spolužák
a	a	k8xC	a
kamarádů	kamarád	k1gMnPc2	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
začal	začít	k5eAaPmAgInS	začít
více	hodně	k6eAd2	hodně
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
hudební	hudební	k2eAgFnSc6d1	hudební
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
budoucím	budoucí	k2eAgMnSc7d1	budoucí
kolegou	kolega	k1gMnSc7	kolega
z	z	k7c2	z
The	The	k1gFnSc2	The
Black	Black	k1gInSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peasa	k1gFnPc2	Peasa
apl	apl	k?	apl
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
<g/>
.	.	kIx.	.
<g/>
apem	apem	k1gMnSc1	apem
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
začal	začít	k5eAaPmAgInS	začít
Will	Will	k1gInSc1	Will
skládat	skládat	k5eAaImF	skládat
první	první	k4xOgFnPc4	první
písničky	písnička	k1gFnPc4	písnička
a	a	k8xC	a
s	s	k7c7	s
apl	apl	k?	apl
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
<g/>
.	.	kIx.	.
<g/>
apem	apem	k6eAd1	apem
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c4	v
Los	los	k1gInSc4	los
Angelských	Angelský	k2eAgInPc6d1	Angelský
klubech	klub	k1gInPc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
zde	zde	k6eAd1	zde
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hudební	hudební	k2eAgFnSc4d1	hudební
skupinu	skupina	k1gFnSc4	skupina
Atban	Atban	k1gMnSc1	Atban
Klann	Klann	k1gMnSc1	Klann
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
to	ten	k3xDgNnSc1	ten
šlo	jít	k5eAaImAgNnS	jít
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
hudební	hudební	k2eAgFnSc7d1	hudební
kariérou	kariéra	k1gFnSc7	kariéra
prudce	prudko	k6eAd1	prudko
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Připojil	připojit	k5eAaPmAgMnS	připojit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
rapper	rapper	k1gInSc1	rapper
Taboo	Taboo	k6eAd1	Taboo
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
Black	Blacka	k1gFnPc2	Blacka
Eyed	Eyeda	k1gFnPc2	Eyeda
Peas	Peas	k1gInSc1	Peas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vydali	vydat	k5eAaPmAgMnP	vydat
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Behind	Behind	k1gMnSc1	Behind
the	the	k?	the
Front	front	k1gInSc1	front
a	a	k8xC	a
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
album	album	k1gNnSc4	album
Bridging	Bridging	k1gInSc4	Bridging
the	the	k?	the
Gap	Gap	k1gFnSc2	Gap
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přidala	přidat	k5eAaPmAgFnS	přidat
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Fergie	Fergie	k1gFnSc1	Fergie
<g/>
,	,	kIx,	,
změnili	změnit	k5eAaPmAgMnP	změnit
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
z	z	k7c2	z
Black	Blacko	k1gNnPc2	Blacko
Eyed	Eyeda	k1gFnPc2	Eyeda
Peas	Peasa	k1gFnPc2	Peasa
na	na	k7c6	na
The	The	k1gFnSc6	The
Black	Black	k1gMnSc1	Black
Eyed	Eyeda	k1gFnPc2	Eyeda
Peas	Peas	k1gInSc4	Peas
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
Elephunk	Elephunka	k1gFnPc2	Elephunka
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
alba	alba	k1gFnSc1	alba
jako	jako	k8xS	jako
Monkey	Monkea	k1gFnPc1	Monkea
Business	business	k1gInSc1	business
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
E.	E.	kA	E.
<g/>
N.	N.	kA	N.
<g/>
D.	D.	kA	D.
(	(	kIx(	(
<g/>
The	The	k1gFnSc2	The
Energy	Energ	k1gInPc1	Energ
Never	Nevra	k1gFnPc2	Nevra
Dies	Diesa	k1gFnPc2	Diesa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
nejnovější	nový	k2eAgNnSc1d3	nejnovější
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Beginning	Beginning	k1gInSc1	Beginning
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složil	Složil	k1gMnSc1	Složil
a	a	k8xC	a
nazpíval	nazpívat	k5eAaPmAgMnS	nazpívat
mnoho	mnoho	k4c4	mnoho
soundtracků	soundtrack	k1gInPc2	soundtrack
k	k	k7c3	k
filmům	film	k1gInPc3	film
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Scary	Scara	k1gFnPc1	Scara
Movie	Movie	k1gFnPc1	Movie
<g/>
,	,	kIx,	,
Garfield	Garfield	k1gInSc1	Garfield
<g/>
,	,	kIx,	,
Taxi	taxi	k1gNnSc1	taxi
<g/>
,	,	kIx,	,
Shrek	Shrek	k6eAd1	Shrek
Třetí	třetí	k4xOgFnSc7	třetí
<g/>
,	,	kIx,	,
Znovu	znovu	k6eAd1	znovu
17	[number]	k4	17
<g/>
,	,	kIx,	,
G-FORCE	G-FORCE	k1gFnSc1	G-FORCE
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
2	[number]	k4	2
<g/>
:	:	kIx,	:
Útěk	útěk	k1gInSc1	útěk
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
nebo	nebo	k8xC	nebo
Velký	velký	k2eAgInSc4d1	velký
Gatsby	Gatsb	k1gInPc4	Gatsb
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
film	film	k1gInSc4	film
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
2	[number]	k4	2
<g/>
:	:	kIx,	:
Útěk	útěk	k1gInSc1	útěk
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
známou	známý	k2eAgFnSc4d1	známá
píseň	píseň	k1gFnSc4	píseň
I	i	k8xC	i
Like	Like	k1gInSc4	Like
to	ten	k3xDgNnSc1	ten
Move	Move	k1gNnSc1	Move
It.	It.	k1gMnPc2	It.
Navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
co	co	k9	co
by	by	kYmCp3nS	by
dabér	dabér	k1gMnSc1	dabér
<g/>
,	,	kIx,	,
střihnul	střihnout	k5eAaPmAgInS	střihnout
roli	role	k1gFnSc4	role
hrocha	hroch	k1gMnSc2	hroch
Moto	moto	k1gNnSc4	moto
Moto	moto	k1gNnSc4	moto
<g/>
.	.	kIx.	.
</s>
<s>
Daboval	dabovat	k5eAaBmAgMnS	dabovat
ptáka	pták	k1gMnSc4	pták
Pedro	Pedro	k1gNnSc1	Pedro
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Rio	Rio	k1gFnSc2	Rio
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
The	The	k1gMnPc7	The
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peasa	k1gFnPc2	Peasa
se	se	k3xPyFc4	se
také	také	k9	také
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Buď	buď	k8xC	buď
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Travoltou	Travolta	k1gMnSc7	Travolta
nebo	nebo	k8xC	nebo
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
<g/>
:	:	kIx,	:
Kasíno	Kasína	k1gFnSc5	Kasína
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgFnSc1d3	veliký
herecká	herecký	k2eAgFnSc1d1	herecká
role	role	k1gFnSc1	role
přišla	přijít	k5eAaPmAgFnS	přijít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
Johna	John	k1gMnSc2	John
Wraitha	Wraith	k1gMnSc2	Wraith
ve	v	k7c6	v
filmu	film	k1gInSc6	film
X-Men	X-Men	k2eAgInSc4d1	X-Men
Origins	Origins	k1gInSc4	Origins
<g/>
:	:	kIx,	:
Wolverine	Wolverin	k1gInSc5	Wolverin
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Yes	Yes	k1gFnSc7	Yes
We	We	k1gFnSc7	We
Can	Can	k1gFnSc7	Can
<g/>
,	,	kIx,	,
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
kampaně	kampaň	k1gFnSc2	kampaň
prezidenta	prezident	k1gMnSc2	prezident
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
cenu	cena	k1gFnSc4	cena
Daytime	Daytim	k1gInSc5	Daytim
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
zasedá	zasedat	k5eAaImIp3nS	zasedat
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
porotě	porota	k1gFnSc6	porota
britské	britský	k2eAgFnSc2d1	britská
pěvecké	pěvecký	k2eAgFnSc2d1	pěvecká
soutěže	soutěž	k1gFnSc2	soutěž
The	The	k1gMnSc1	The
Voice	Voice	k1gMnSc1	Voice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nosil	nosit	k5eAaImAgInS	nosit
zatím	zatím	k6eAd1	zatím
jen	jen	k6eAd1	jen
dva	dva	k4xCgInPc4	dva
účesy	účes	k1gInPc4	účes
<g/>
:	:	kIx,	:
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
vlasy	vlas	k1gInPc4	vlas
s	s	k7c7	s
dredy	dredo	k1gNnPc7	dredo
a	a	k8xC	a
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
pěšinku	pěšinka	k1gFnSc4	pěšinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
nápojem	nápoj	k1gInSc7	nápoj
je	být	k5eAaImIp3nS	být
Pepsi	Pepsi	k1gFnSc1	Pepsi
Max	max	kA	max
<g/>
,	,	kIx,	,
natočit	natočit	k5eAaBmF	natočit
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
reklamu	reklama	k1gFnSc4	reklama
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Lost	Lost	k1gInSc1	Lost
Change	change	k1gFnSc2	change
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Must	Must	k1gInSc1	Must
B	B	kA	B
21	[number]	k4	21
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Songs	Songs	k6eAd1	Songs
About	About	k1gInSc1	About
Girls	girl	k1gFnPc2	girl
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
#	#	kIx~	#
<g/>
willpower	willpowero	k1gNnPc2	willpowero
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Singly	singl	k1gInPc4	singl
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Jako	jako	k8xS	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
umělec	umělec	k1gMnSc1	umělec
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
I	i	k9	i
Got	Got	k1gMnSc1	Got
It	It	k1gMnSc1	It
From	From	k1gMnSc1	From
My	my	k3xPp1nPc1	my
Mama	mama	k1gFnSc1	mama
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Heartbreaker	Heartbreaker	k1gInSc4	Heartbreaker
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Cheryl	Cheryl	k1gInSc1	Cheryl
Cole	cola	k1gFnSc3	cola
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
One	One	k1gMnSc1	One
More	mor	k1gInSc5	mor
Chance	Chanec	k1gInPc4	Chanec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
It	It	k1gMnPc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
New	New	k1gMnSc1	New
Day	Day	k1gMnSc1	Day
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Check	Check	k1gMnSc1	Check
It	It	k1gMnSc1	It
Out	Out	k1gMnSc1	Out
<g/>
"	"	kIx"	"
-	-	kIx~	-
společně	společně	k6eAd1	společně
s	s	k7c7	s
Nicki	Nicki	k1gNnSc7	Nicki
Minaj	Minaj	k1gMnSc1	Minaj
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
T.H.E.	T.H.E.	k1gFnSc6	T.H.E.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Hardest	Hardest	k1gMnSc1	Hardest
Ever	Ever	k1gMnSc1	Ever
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
-	-	kIx~	-
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jennifer	Jennifer	k1gMnSc1	Jennifer
Lopez	Lopez	k1gMnSc1	Lopez
a	a	k8xC	a
Mick	Mick	k1gMnSc1	Mick
Jagger	Jagger	k1gMnSc1	Jagger
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
This	This	k1gInSc1	This
Is	Is	k1gFnSc2	Is
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Eva	Eva	k1gFnSc1	Eva
Simons	Simonsa	k1gFnPc2	Simonsa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Scream	Scream	k1gInSc1	Scream
&	&	k?	&
Shout	Shout	k1gInSc1	Shout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Britney	Britney	k1gInPc1	Britney
Spears	Spearsa	k1gFnPc2	Spearsa
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
#	#	kIx~	#
<g/>
thatpower	thatpower	k1gInSc1	thatpower
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Justin	Justin	k1gMnSc1	Justin
Bieber	Bieber	k1gMnSc1	Bieber
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Bang	Bang	k1gMnSc1	Bang
Bang	Bang	k1gMnSc1	Bang
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Feelin	Feelin	k2eAgInSc4d1	Feelin
<g/>
'	'	kIx"	'
Myself	Myself	k1gInSc4	Myself
<g/>
"	"	kIx"	"
-	-	kIx~	-
společně	společně	k6eAd1	společně
s	s	k7c7	s
Miley	Mile	k1gMnPc7	Mile
Cyrus	Cyrus	k1gInSc4	Cyrus
<g/>
,	,	kIx,	,
French	French	k1gInSc4	French
Montanou	Montana	k1gFnSc7	Montana
a	a	k8xC	a
Wiz	Wiz	k1gFnSc7	Wiz
Khalifou	Khalifa	k1gFnSc7	Khalifa
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
It	It	k1gMnPc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
My	my	k3xPp1nPc1	my
Birthday	Birthdaum	k1gNnPc7	Birthdaum
<g/>
"	"	kIx"	"
-	-	kIx~	-
společne	společnout	k5eAaPmIp3nS	společnout
s	s	k7c7	s
Cody	coda	k1gFnPc1	coda
Wise	Wise	k1gFnSc7	Wise
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Jako	jako	k8xC	jako
hostující	hostující	k2eAgMnSc1d1	hostující
umělec	umělec	k1gMnSc1	umělec
===	===	k?	===
</s>
</p>
<p>
<s>
Pussycat	Pussycat	k5eAaPmF	Pussycat
Dolls	Dolls	k1gInSc4	Dolls
-	-	kIx~	-
"	"	kIx"	"
<g/>
Beep	Beep	k1gInSc1	Beep
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Busta	busta	k1gFnSc1	busta
Rhymes	Rhymesa	k1gFnPc2	Rhymesa
-	-	kIx~	-
"	"	kIx"	"
<g/>
I	i	k9	i
Love	lov	k1gInSc5	lov
My	my	k3xPp1nPc1	my
Bitch	Bitch	k1gInSc4	Bitch
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
a	a	k8xC	a
Kelis	Kelis	k1gFnSc1	Kelis
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fergie	Fergie	k1gFnSc1	Fergie
-	-	kIx~	-
"	"	kIx"	"
<g/>
Fergalicious	Fergalicious	k1gInSc1	Fergalicious
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nas	Nas	k?	Nas
-	-	kIx~	-
Hip	hip	k0	hip
Hop	hop	k0	hop
Is	Is	k1gFnPc3	Is
Dead	Dead	k1gInSc4	Dead
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Flo	Flo	k?	Flo
Rida	Rida	k1gMnSc1	Rida
-	-	kIx~	-
In	In	k1gMnSc1	In
the	the	k?	the
Ayer	Ayer	k1gMnSc1	Ayer
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Usher	Usher	k1gMnSc1	Usher
-	-	kIx~	-
OMG	OMG	kA	OMG
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Natalia	Natalia	k1gFnSc1	Natalia
Kills	Killsa	k1gFnPc2	Killsa
-	-	kIx~	-
"	"	kIx"	"
<g/>
Free	Free	k1gFnSc1	Free
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Guetta	Guetta	k1gMnSc1	Guetta
-	-	kIx~	-
"	"	kIx"	"
<g/>
Nothing	Nothing	k1gInSc1	Nothing
Really	Realla	k1gFnSc2	Realla
Matters	Mattersa	k1gFnPc2	Mattersa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Script	Script	k1gInSc1	Script
-	-	kIx~	-
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
.	.	kIx.	.
will	will	k1gMnSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
<g/>
$	$	kIx~	$
<g/>
ha	ha	kA	ha
-	-	kIx~	-
Crazy	Craz	k1gInPc1	Craz
Kids	Kidsa	k1gFnPc2	Kidsa
(	(	kIx(	(
<g/>
ft	ft	k?	ft
<g/>
,	,	kIx,	,
will	will	k1gInSc1	will
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
Will	Willa	k1gFnPc2	Willa
<g/>
.	.	kIx.	.
<g/>
i.	i.	k?	i.
<g/>
am	am	k?	am
zpíval	zpívat	k5eAaImAgMnS	zpívat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Cher	Cher	k1gMnSc1	Cher
Lloyd	Lloyd	k1gMnSc1	Lloyd
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
soutěži	soutěž	k1gFnSc6	soutěž
X-Factor	X-Factor	k1gInSc4	X-Factor
<g/>
.	.	kIx.	.
</s>
<s>
Porotoval	Porotovat	k5eAaImAgMnS	Porotovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Cheryl	Cheryl	k1gInSc1	Cheryl
Cole	cola	k1gFnSc3	cola
v	v	k7c4	v
Judges	Judges	k1gInSc4	Judges
Houses	Housesa	k1gFnPc2	Housesa
<g/>
.	.	kIx.	.
</s>
</p>
