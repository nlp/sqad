<s>
Android	android	k1gInSc1
(	(	kIx(
<g/>
robot	robot	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
android	android	k1gInSc1
–	–	k?
spíš	spát	k5eAaImIp2nS
komplikovaná	komplikovaný	k2eAgFnSc1d1
figurína	figurína	k1gFnSc1
vyvinutá	vyvinutý	k2eAgFnSc1d1
pro	pro	k7c4
testování	testování	k1gNnSc4
kosmických	kosmický	k2eAgInPc2d1
skafandrů	skafandr	k1gInPc2
</s>
<s>
Android	android	k1gInSc1
je	být	k5eAaImIp3nS
člověku	člověk	k1gMnSc3
podobný	podobný	k2eAgInSc4d1
robot	robot	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Android	android	k1gInSc1
je	být	k5eAaImIp3nS
stroj	stroj	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
chovat	chovat	k5eAaImF
se	se	k3xPyFc4
<g/>
,	,	kIx,
pracovat	pracovat	k5eAaImF
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
myslet	myslet	k5eAaImF
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
člověk	člověk	k1gMnSc1
díky	díky	k7c3
umělé	umělý	k2eAgFnSc3d1
inteligenci	inteligence	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
„	„	k?
<g/>
Android	android	k1gInSc1
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řeckých	řecký	k2eAgInPc2d1
výrazů	výraz	k1gInPc2
andros	androsa	k1gFnPc2
(	(	kIx(
<g/>
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
člověk	člověk	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
-eides	-eides	k1gInSc1
(	(	kIx(
<g/>
stejného	stejný	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
,	,	kIx,
podobný	podobný	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
legendy	legenda	k1gFnPc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
<g/>
,	,	kIx,
alchymie	alchymie	k1gFnSc1
</s>
<s>
Napodobit	napodobit	k5eAaPmF
či	či	k8xC
zopakovat	zopakovat	k5eAaPmF
stvořitelský	stvořitelský	k2eAgInSc4d1
čin	čin	k1gInSc4
Boha	bůh	k1gMnSc2
či	či	k8xC
Přírody	příroda	k1gFnSc2
–	–	k?
tato	tento	k3xDgFnSc1
myšlenka	myšlenka	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
historii	historie	k1gFnSc6
lidstva	lidstvo	k1gNnSc2
opakuje	opakovat	k5eAaImIp3nS
mnohokrát	mnohokrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
mýtus	mýtus	k1gInSc1
o	o	k7c6
Kyperském	kyperský	k2eAgMnSc6d1
králi	král	k1gMnSc6
Pygmalion	Pygmalion	k1gInSc1
–	–	k?
zřejmě	zřejmě	k6eAd1
předcházel	předcházet	k5eAaImAgInS
Ovidiovu	Ovidiův	k2eAgFnSc4d1
ztvárnění	ztvárnění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
homunkulus	homunkulus	k1gMnSc1
–	–	k?
doslova	doslova	k6eAd1
človíček	človíček	k1gMnSc1
<g/>
,	,	kIx,
často	často	k6eAd1
výraz	výraz	k1gInSc1
pro	pro	k7c4
syntetickou	syntetický	k2eAgFnSc4d1
(	(	kIx(
<g/>
umělou	umělý	k2eAgFnSc4d1
<g/>
)	)	kIx)
bytost	bytost	k1gFnSc4
</s>
<s>
Golem	Golem	k1gMnSc1
je	být	k5eAaImIp3nS
typickým	typický	k2eAgInSc7d1
legendárním	legendární	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
androida	android	k1gMnSc2
</s>
<s>
Automaty	automat	k1gInPc1
</s>
<s>
S	s	k7c7
rozvojem	rozvoj	k1gInSc7
jemné	jemný	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
bylo	být	k5eAaImAgNnS
navrhováno	navrhovat	k5eAaImNgNnS
a	a	k8xC
konstruováno	konstruovat	k5eAaImNgNnS
mnoho	mnoho	k4c1
automatů	automat	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
samohybných	samohybný	k2eAgInPc2d1
mechanismů	mechanismus	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mnoho	mnoho	k4c1
z	z	k7c2
nich	on	k3xPp3gInPc2
bylo	být	k5eAaImAgNnS
umisťováno	umisťován	k2eAgNnSc1d1
do	do	k7c2
člověkupodobných	člověkupodobný	k2eAgFnPc2d1
figur	figura	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc4
pohyb	pohyb	k1gInSc4
ovládaly	ovládat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s>
čínské	čínský	k2eAgInPc1d1
automaty	automat	k1gInPc1
–	–	k?
jsou	být	k5eAaImIp3nP
popisovány	popisován	k2eAgInPc4d1
již	již	k6eAd1
v	v	k7c6
dobách	doba	k1gFnPc6
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
</s>
<s>
Řecko	Řecko	k1gNnSc1
–	–	k?
Hérón	Hérón	k1gMnSc1
Alexandrijský	alexandrijský	k2eAgMnSc1d1
kolem	kolem	k7c2
r.	r.	kA
<g/>
50	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
popisuje	popisovat	k5eAaImIp3nS
automat	automat	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
naléval	nalévat	k5eAaImAgInS
víno	víno	k1gNnSc4
na	na	k7c6
párty	párty	k1gFnSc6
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Leonardův	Leonardův	k2eAgMnSc1d1
robot	robot	k1gMnSc1
–	–	k?
automatický	automatický	k2eAgMnSc1d1
bojovník	bojovník	k1gMnSc1
v	v	k7c6
brnění	brnění	k1gNnSc6
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
navrhl	navrhnout	k5eAaPmAgMnS
Leonardo	Leonardo	k1gMnSc1
da	da	k?
Vinci	Vinca	k1gMnSc2
kolem	kolem	k7c2
r.	r.	kA
1464	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
–	–	k?
konstruováno	konstruovat	k5eAaImNgNnS
mnoho	mnoho	k4c1
naprogramovaných	naprogramovaný	k2eAgFnPc2d1
figur	figura	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zvládaly	zvládat	k5eAaImAgFnP
velmi	velmi	k6eAd1
složité	složitý	k2eAgInPc1d1
pohyby	pohyb	k1gInPc1
–	–	k?
hru	hra	k1gFnSc4
na	na	k7c4
hudební	hudební	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
tanec	tanec	k1gInSc4
atd.	atd.	kA
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
Publius	Publius	k1gMnSc1
Ovidius	Ovidius	k1gMnSc1
Naso	Naso	k1gMnSc1
</s>
<s>
Proměny	proměna	k1gFnPc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
kniha	kniha	k1gFnSc1
<g/>
:	:	kIx,
Zbožný	zbožný	k2eAgInSc1d1
Pygmalion	Pygmalion	k1gInSc1
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
znechucen	znechutit	k5eAaPmNgMnS
ženskou	ženský	k2eAgFnSc7d1
proradností	proradnost	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
zůstává	zůstávat	k5eAaImIp3nS
starým	starý	k2eAgMnSc7d1
mládencem	mládenec	k1gMnSc7
<g/>
,	,	kIx,
panicem	panic	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
náhradu	náhrada	k1gFnSc4
za	za	k7c4
ženskou	ženský	k2eAgFnSc4d1
lásku	láska	k1gFnSc4
si	se	k3xPyFc3
vyřeže	vyřezat	k5eAaPmIp3nS
ze	z	k7c2
slonové	slonový	k2eAgFnSc2d1
kosti	kost	k1gFnSc2
sochu	socha	k1gFnSc4
ideální	ideální	k2eAgFnSc2d1
panny	panna	k1gFnSc2
tak	tak	k9
krásnou	krásný	k2eAgFnSc4d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
zamiluje	zamilovat	k5eAaPmIp3nS
–	–	k?
mluví	mluvit	k5eAaImIp3nS
k	k	k7c3
ní	on	k3xPp3gFnSc3
a	a	k8xC
projevuje	projevovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
i	i	k9
jiné	jiný	k2eAgFnPc4d1
něžnosti	něžnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modlí	modlit	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
bohům	bůh	k1gMnPc3
<g/>
,	,	kIx,
Venuše	Venuše	k1gFnSc2
jeho	jeho	k3xOp3gFnSc2
prosby	prosba	k1gFnSc2
vyslyší	vyslyšet	k5eAaPmIp3nS
a	a	k8xC
způsobí	způsobit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
socha	socha	k1gFnSc1
v	v	k7c6
Pygmalionově	Pygmalionův	k2eAgNnSc6d1
objetí	objetí	k1gNnSc6
postupně	postupně	k6eAd1
ožívá	ožívat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pygmalion	Pygmalion	k1gInSc1
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
bere	brát	k5eAaImIp3nS
za	za	k7c4
manželku	manželka	k1gFnSc4
a	a	k8xC
ta	ten	k3xDgFnSc1
mu	on	k3xPp3gMnSc3
porodí	porodit	k5eAaPmIp3nS
dceru	dcera	k1gFnSc4
Pafé	Pafá	k1gFnSc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
opačný	opačný	k2eAgInSc1d1
proces	proces	k1gInSc1
k	k	k7c3
jinak	jinak	k6eAd1
celkem	celkem	k6eAd1
obvyklejšímu	obvyklý	k2eAgNnSc3d2
zkamenění	zkamenění	k1gNnSc3
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Jean-Jacques	Jean-Jacques	k1gMnSc1
Rousseau	Rousseau	k1gMnSc1
</s>
<s>
Melodrama	Melodrama	k?
Pygmalion	Pygmalion	k1gInSc1
–	–	k?
Rousseau	Rousseau	k1gMnSc1
přepracovává	přepracovávat	k5eAaImIp3nS
Ovidiovu	Ovidiův	k2eAgFnSc4d1
myšlenku	myšlenka	k1gFnSc4
<g/>
,	,	kIx,
nejde	jít	k5eNaImIp3nS
zde	zde	k6eAd1
už	už	k6eAd1
o	o	k7c4
pokoru	pokora	k1gFnSc4
před	před	k7c7
bohy	bůh	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
spíš	spíš	k9
o	o	k7c4
vzpouru	vzpoura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soše	socha	k1gFnSc6
dává	dávat	k5eAaImIp3nS
jméno	jméno	k1gNnSc4
Galatea	Galate	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
E.	E.	kA
T.	T.	kA
A.	A.	kA
Hoffmann	Hoffmann	k1gMnSc1
</s>
<s>
Povídka	povídka	k1gFnSc1
Der	drát	k5eAaImRp2nS
Sandmann	Sandmann	k1gMnSc1
<g/>
:	:	kIx,
Student	student	k1gMnSc1
Nathanael	Nathanael	k1gMnSc1
odmítne	odmítnout	k5eAaPmIp3nS
reálnou	reálný	k2eAgFnSc4d1
dívku	dívka	k1gFnSc4
Claru	Clara	k1gFnSc4
a	a	k8xC
zamiluje	zamilovat	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
mechanické	mechanický	k2eAgFnSc2d1
loutky	loutka	k1gFnSc2
Olimpie	Olimpie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
smontoval	smontovat	k5eAaPmAgMnS
prodavač	prodavač	k1gMnSc1
Copolla	Copolla	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oči	oko	k1gNnPc1
jí	on	k3xPp3gFnSc2
dodal	dodat	k5eAaPmAgMnS
profesor	profesor	k1gMnSc1
Spalanzani	Spalanzan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nathanael	Nathanaela	k1gFnPc2
ji	on	k3xPp3gFnSc4
oživuje	oživovat	k5eAaImIp3nS
svým	svůj	k3xOyFgInSc7
vášnivým	vášnivý	k2eAgInSc7d1
pohledem	pohled	k1gInSc7
–	–	k?
zpočátku	zpočátku	k6eAd1
pomocí	pomocí	k7c2
kouzelného	kouzelný	k2eAgInSc2d1
dalekohledu	dalekohled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechaničnost	mechaničnost	k1gFnSc1
Olimpie	Olimpie	k1gFnSc2
uvolňuje	uvolňovat	k5eAaImIp3nS
Nathanaelovi	Nathanael	k1gMnSc3
prostor	prostor	k1gInSc4
pro	pro	k7c4
vlastní	vlastní	k2eAgFnPc4d1
iluze	iluze	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
umělou	umělý	k2eAgFnSc4d1
bytost	bytost	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
vytvoření	vytvoření	k1gNnSc6
se	se	k3xPyFc4
podílelo	podílet	k5eAaImAgNnS
vícero	vícero	k4xRyIgNnSc1
tvůrců	tvůrce	k1gMnPc2
<g/>
,	,	kIx,
vícero	vícero	k1gNnSc1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Auguste	August	k1gMnSc5
Villiers	Villiers	k1gInSc4
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Isle	Isl	k1gMnSc2
Adam	Adam	k1gMnSc1
</s>
<s>
Termín	termín	k1gInSc1
android	android	k1gInSc1
zpopularizoval	zpopularizovat	k5eAaPmAgMnS
hrabě	hrabě	k1gMnSc1
Auguste	August	k1gMnSc5
Villiers	Villiers	k1gInSc4
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Isle	Isl	k1gMnSc2
Adam	Adam	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
symbolistickém	symbolistický	k2eAgInSc6d1
sci-firománu	sci-firomán	k1gMnSc3
L	L	kA
<g/>
'	'	kIx"
<g/>
È	È	k1gMnSc5
future	futur	k1gMnSc5
v	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
Budoucí	budoucí	k2eAgFnSc1d1
Eva	Eva	k1gFnSc1
<g/>
,	,	kIx,
přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
A.	A.	kA
Růžičková	Růžičková	k1gFnSc1
<g/>
;	;	kIx,
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
F.	F.	kA
Topič	topič	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1920	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trauma	trauma	k1gNnSc1
Lorda	lord	k1gMnSc2
Ewalda	Ewald	k1gMnSc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zamiluje	zamilovat	k5eAaPmIp3nS
do	do	k7c2
zpěvačky	zpěvačka	k1gFnSc2
Alicie	Alicie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
krásná	krásný	k2eAgFnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
hloupá	hloupý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoufalou	zoufalý	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
řeší	řešit	k5eAaImIp3nS
jeho	jeho	k3xOp3gMnSc1
přítel	přítel	k1gMnSc1
<g/>
,	,	kIx,
vynálezce	vynálezce	k1gMnSc1
Edison	Edison	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
sestrojí	sestrojit	k5eAaPmIp3nS
věrnou	věrný	k2eAgFnSc4d1
figurínu	figurína	k1gFnSc4
Alicie	Alicie	k1gFnSc2
<g/>
,	,	kIx,
oživenou	oživený	k2eAgFnSc4d1
duchovní	duchovní	k2eAgFnSc4d1
bytosti	bytost	k1gFnSc3
Sowana	Sowana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tom	ten	k3xDgNnSc6
jim	on	k3xPp3gMnPc3
pomáhají	pomáhat	k5eAaImIp3nP
nejmodernější	moderní	k2eAgFnPc1d3
technologie	technologie	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
Edisonův	Edisonův	k2eAgInSc4d1
fonograf	fonograf	k1gInSc4
pro	pro	k7c4
záznam	záznam	k1gInSc4
hlasu	hlas	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
nová	nový	k2eAgFnSc1d1
bytost	bytost	k1gFnSc1
–	–	k?
Hadala	Hadala	k1gFnSc1
<g/>
,	,	kIx,
krásná	krásný	k2eAgFnSc1d1
a	a	k8xC
inteligentní	inteligentní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměrem	záměr	k1gInSc7
tvůrců	tvůrce	k1gMnPc2
je	být	k5eAaImIp3nS
také	také	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
pečlivým	pečlivý	k2eAgNnSc7d1
naprogramováním	naprogramování	k1gNnSc7
redukují	redukovat	k5eAaBmIp3nP
obvyklé	obvyklý	k2eAgNnSc1d1
ženské	ženský	k2eAgInPc1d1
vrtochy	vrtoch	k1gInPc1
a	a	k8xC
tak	tak	k8xS,k8xC
se	se	k3xPyFc4
jimi	on	k3xPp3gMnPc7
vytvořená	vytvořený	k2eAgFnSc1d1
bytost	bytost	k1gFnSc1
stane	stanout	k5eAaPmIp3nS
dobře	dobře	k6eAd1
ovladatelnou	ovladatelný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hadala	Hadala	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
vymkne	vymknout	k5eAaPmIp3nS
z	z	k7c2
jejich	jejich	k3xOp3gFnSc2
manipulace	manipulace	k1gFnSc2
<g/>
,	,	kIx,
začne	začít	k5eAaPmIp3nS
hovořit	hovořit	k5eAaImF
svým	svůj	k3xOyFgInSc7
vlastním	vlastní	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
a	a	k8xC
žít	žít	k5eAaImF
si	se	k3xPyFc3
svým	svůj	k3xOyFgInSc7
vlastním	vlastní	k2eAgInSc7d1
životem	život	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
použitím	použití	k1gNnSc7
moderní	moderní	k2eAgFnSc2d1
terminologie	terminologie	k1gFnSc2
byla	být	k5eAaImAgFnS
Hadala	Hadala	k1gFnSc1
vlastně	vlastně	k9
prototypem	prototyp	k1gInSc7
gynoida	gynoid	k1gMnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
žensky	žensky	k6eAd1
vypadajícího	vypadající	k2eAgMnSc4d1
robota	robot	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
Čapek	Čapek	k1gMnSc1
</s>
<s>
Čapkovi	Čapkův	k2eAgMnPc1d1
roboti	robot	k1gMnPc1
<g/>
,	,	kIx,
výrobky	výrobek	k1gInPc1
firmy	firma	k1gFnSc2
R.	R.	kA
<g/>
U.	U.	kA
<g/>
R.	R.	kA
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
stejnojmenném	stejnojmenný	k2eAgInSc6d1
dramatu	drama	k1gNnSc2
člověku	člověk	k1gMnSc6
podobní	podobný	k2eAgMnPc1d1
roboti	robot	k1gMnPc1
<g/>
,	,	kIx,
tedy	tedy	k9
androidi	android	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Villiersova	Villiersův	k2eAgInSc2d1
románu	román	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
Hadala	Hadala	k1gFnSc1
prototypem	prototyp	k1gInSc7
androida	android	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
uspokojit	uspokojit	k5eAaPmF
Ewaldovy	Ewaldův	k2eAgFnSc2d1
erotické	erotický	k2eAgFnSc2d1
tužby	tužba	k1gFnSc2
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
Čapkovi	Čapkův	k2eAgMnPc1d1
roboti	robot	k1gMnPc1
hromadně	hromadně	k6eAd1
vyráběné	vyráběný	k2eAgFnPc1d1
bytosti	bytost	k1gFnPc1
určené	určený	k2eAgFnPc1d1
k	k	k7c3
vykonávání	vykonávání	k1gNnSc3
těžké	těžký	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
robot	robot	k1gInSc4
(	(	kIx(
<g/>
odvozený	odvozený	k2eAgMnSc1d1
ze	z	k7c2
staršího	starý	k2eAgInSc2d2
českého	český	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
robota	robot	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
použit	použít	k5eAaPmNgInS
v	v	k7c6
tomto	tento	k3xDgInSc6
dramatu	drama	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
pak	pak	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
na	na	k7c4
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
stroje	stroj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
zastupují	zastupovat	k5eAaImIp3nP
člověka	člověk	k1gMnSc4
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
namáhavé	namáhavý	k2eAgFnSc6d1
práci	práce	k1gFnSc6
–	–	k?
např.	např.	kA
kuchyňský	kuchyňský	k1gMnSc1
robot	robot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Sci-fi	sci-fi	k1gFnSc1
film	film	k1gInSc1
</s>
<s>
Cyloni	Cylon	k1gMnPc1
v	v	k7c6
seriálech	seriál	k1gInPc6
Battlestar	Battlestar	k1gMnSc1
Galactica	Galactica	k1gMnSc1
<g/>
:	:	kIx,
někteří	některý	k3yIgMnPc1
jsou	být	k5eAaImIp3nP
naprogramováni	naprogramovat	k5eAaPmNgMnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c6
sobě	sebe	k3xPyFc6
ani	ani	k9
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
androidi	android	k1gMnPc1
(	(	kIx(
<g/>
pokládají	pokládat	k5eAaImIp3nP
se	se	k3xPyFc4
za	za	k7c2
lidské	lidský	k2eAgFnSc2d1
bytosti	bytost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
to	ten	k3xDgNnSc1
o	o	k7c6
sobě	sebe	k3xPyFc6
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
tají	tajit	k5eAaImIp3nP
to	ten	k3xDgNnSc4
<g/>
.	.	kIx.
</s>
<s>
Vetřelec	vetřelec	k1gMnSc1
<g/>
:	:	kIx,
V	v	k7c6
prvním	první	k4xOgInSc6
dílu	díl	k1gInSc6
série	série	k1gFnSc2
tajil	tajit	k5eAaImAgInS
android	android	k1gInSc1
Ash	Ash	k1gFnSc2
svou	svůj	k3xOyFgFnSc4
podstatu	podstata	k1gFnSc4
až	až	k9
téměř	téměř	k6eAd1
do	do	k7c2
svého	svůj	k3xOyFgNnSc2
zničení	zničení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
díle	díl	k1gInSc6
Vetřelci	vetřelec	k1gMnPc1
ale	ale	k8xC
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Bishop	Bishop	k1gInSc1
je	být	k5eAaImIp3nS
android	android	k1gInSc4
<g/>
,	,	kIx,
věděli	vědět	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
od	od	k7c2
začátku	začátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
C-3PO	C-3PO	k4
(	(	kIx(
<g/>
Star	Star	kA
Wars	Wars	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
typický	typický	k2eAgInSc4d1
android	android	k1gInSc4
chováním	chování	k1gNnSc7
i	i	k8xC
vzhledem	vzhled	k1gInSc7
(	(	kIx(
<g/>
snad	snad	k9
na	na	k7c4
stroj	stroj	k1gInSc4
příliš	příliš	k6eAd1
bojácný	bojácný	k2eAgInSc4d1
a	a	k8xC
starostlivý	starostlivý	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Kryton	Kryton	k1gInSc1
(	(	kIx(
<g/>
Červený	Červený	k1gMnSc1
trpaslík	trpaslík	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Stan	stan	k1gInSc1
(	(	kIx(
<g/>
Aaron	Aaron	k1gMnSc1
Stone	ston	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Dat	datum	k1gNnPc2
(	(	kIx(
<g/>
Star	Star	kA
Trek	Trek	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
android	android	k1gInSc1
ve	v	k7c6
službách	služba	k1gFnPc6
Hvězdné	hvězdný	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
snažící	snažící	k2eAgFnSc2d1
se	se	k3xPyFc4
dosáhnout	dosáhnout	k5eAaPmF
lidství	lidství	k1gNnSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Naopak	naopak	k6eAd1
Borgové	Borg	k1gMnPc1
nebyli	být	k5eNaImAgMnP
androidi	android	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
kyborgové	kyborg	k1gMnPc1
<g/>
:	:	kIx,
původně	původně	k6eAd1
živí	živit	k5eAaImIp3nP
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
až	až	k9
následně	následně	k6eAd1
ovládnutí	ovládnutí	k1gNnSc1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Často	často	k6eAd1
je	být	k5eAaImIp3nS
android	android	k1gInSc1
alespoň	alespoň	k9
částečně	částečně	k6eAd1
dotvořený	dotvořený	k2eAgMnSc1d1
biologickými	biologický	k2eAgFnPc7d1
částmi	část	k1gFnPc7
<g/>
:	:	kIx,
Pro	pro	k7c4
androidy	android	k1gInPc4
se	se	k3xPyFc4
předpokládají	předpokládat	k5eAaImIp3nP
sice	sice	k8xC
biologické	biologický	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
uměle	uměle	k6eAd1
vytvořené	vytvořený	k2eAgFnSc3d1
živé	živý	k2eAgFnSc3d1
části	část	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Současné	současný	k2eAgFnPc1d1
technologie	technologie	k1gFnPc1
</s>
<s>
Na	na	k7c6
úrovni	úroveň	k1gFnSc6
dnešních	dnešní	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
je	být	k5eAaImIp3nS
nejen	nejen	k6eAd1
nemožné	možný	k2eNgNnSc1d1
sestrojit	sestrojit	k5eAaPmF
robota	robot	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
ve	v	k7c6
všech	všecek	k3xTgInPc6
ohledech	ohled	k1gInPc6
podobný	podobný	k2eAgInSc4d1
člověku	člověk	k1gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
ani	ani	k8xC
to	ten	k3xDgNnSc1
nedává	dávat	k5eNaImIp3nS
velký	velký	k2eAgInSc4d1
smysl	smysl	k1gInSc4
<g/>
:	:	kIx,
Naopak	naopak	k6eAd1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgInPc1d1
roboty	robot	k1gInPc1
jsou	být	k5eAaImIp3nP
konstruovány	konstruovat	k5eAaImNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nahrazovaly	nahrazovat	k5eAaImAgFnP
člověka	člověk	k1gMnSc4
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
přirozené	přirozený	k2eAgFnPc4d1
lidské	lidský	k2eAgFnPc4d1
síly	síla	k1gFnPc4
a	a	k8xC
schopnosti	schopnost	k1gFnPc4
nestačí	stačit	k5eNaBmIp3nP
(	(	kIx(
<g/>
např.	např.	kA
jemné	jemný	k2eAgFnPc4d1
neurochirurgické	neurochirurgický	k2eAgFnPc4d1
operace	operace	k1gFnPc4
<g/>
,	,	kIx,
práce	práce	k1gFnPc4
v	v	k7c6
těžko	těžko	k6eAd1
přístupném	přístupný	k2eAgMnSc6d1
a	a	k8xC
nebezpečném	bezpečný	k2eNgNnSc6d1
prostředí	prostředí	k1gNnSc6
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
jsou	být	k5eAaImIp3nP
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
má	mít	k5eAaImIp3nS
nějaký	nějaký	k3yIgInSc4
smysl	smysl	k1gInSc4
napodobovat	napodobovat	k5eAaImF
některé	některý	k3yIgInPc4
lidské	lidský	k2eAgInPc4d1
rysy	rys	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
Vnější	vnější	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
<g/>
:	:	kIx,
Různé	různý	k2eAgFnPc1d1
pohybující	pohybující	k2eAgFnPc1d1
se	se	k3xPyFc4
a	a	k8xC
mluvící	mluvící	k2eAgFnPc1d1
figury	figura	k1gFnPc1
<g/>
,	,	kIx,
reklama	reklama	k1gFnSc1
<g/>
,	,	kIx,
dětské	dětský	k2eAgFnPc1d1
hračky	hračka	k1gFnPc1
apod.	apod.	kA
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
<g/>
:	:	kIx,
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
modelovat	modelovat	k5eAaImF
a	a	k8xC
či	či	k8xC
napodobit	napodobit	k5eAaPmF
lidské	lidský	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
při	při	k7c6
řešení	řešení	k1gNnSc6
různých	různý	k2eAgInPc2d1
kognitivních	kognitivní	k2eAgInPc2d1
aj.	aj.	kA
problémů	problém	k1gInPc2
</s>
<s>
nápodoba	nápodoba	k1gFnSc1
emocí	emoce	k1gFnPc2
a	a	k8xC
emotivních	emotivní	k2eAgInPc2d1
stavů	stav	k1gInPc2
v	v	k7c6
programech	program	k1gInPc6
a	a	k8xC
u	u	k7c2
automatů	automat	k1gInPc2
</s>
<s>
bionika	bionik	k1gMnSc2
<g/>
:	:	kIx,
Snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
obecně	obecně	k6eAd1
napodobovat	napodobovat	k5eAaImF
různé	různý	k2eAgFnPc4d1
biologické	biologický	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
v	v	k7c6
inženýrství	inženýrství	k1gNnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
kráčející	kráčející	k2eAgInSc1d1
robot	robot	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
dokáže	dokázat	k5eAaPmIp3nS
pohybovat	pohybovat	k5eAaImF
v	v	k7c6
těžkém	těžký	k2eAgInSc6d1
terénu	terén	k1gInSc6
<g/>
)	)	kIx)
</s>
<s>
porozumění	porozumění	k1gNnSc1
a	a	k8xC
syntéza	syntéza	k1gFnSc1
lidské	lidský	k2eAgFnSc2d1
řeči	řeč	k1gFnSc2
</s>
<s>
analýza	analýza	k1gFnSc1
3D	3D	k4
scény	scéna	k1gFnSc2
a	a	k8xC
pohyb	pohyb	k1gInSc1
v	v	k7c6
3D	3D	k4
prostoru	prostor	k1gInSc6
</s>
<s>
Příklady	příklad	k1gInPc1
novodobých	novodobý	k2eAgInPc2d1
androidů	android	k1gInPc2
</s>
<s>
Za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejdokonalejších	dokonalý	k2eAgMnPc2d3
robotů	robot	k1gInPc2
humanoidní	humanoidní	k2eAgFnSc2d1
konstrukce	konstrukce	k1gFnSc2
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
robota	robot	k1gMnSc4
týmu	tým	k1gInSc2
SCHAFT	SCHAFT	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
soutěž	soutěž	k1gFnSc4
DARPA	DARPA	kA
Robotics	Robotics	k1gInSc4
Challenge	Challeng	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Honda	honda	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
serie	serie	k1gFnSc1
E	E	kA
<g/>
:	:	kIx,
v	v	k7c6
r.	r.	kA
1986	#num#	k4
zkonstruován	zkonstruován	k2eAgInSc4d1
android	android	k1gInSc4
Honda	honda	k1gFnSc1
E0	E0	k1gFnSc1
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gInSc6
další	další	k2eAgMnPc1d1
roboti	robot	k1gMnPc1
serie	serie	k1gFnSc2
E1	E1	k1gMnSc1
–	–	k?
E6	E6	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kráčející	kráčející	k2eAgFnSc1d1
po	po	k7c6
nohách	noha	k1gFnPc6
<g/>
,	,	kIx,
podobných	podobný	k2eAgFnPc6d1
lidským	lidský	k2eAgNnPc3d1
<g/>
.	.	kIx.
</s>
<s>
serie	serie	k1gFnSc1
P	P	kA
<g/>
:	:	kIx,
P	P	kA
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
–	–	k?
P	P	kA
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
k	k	k7c3
nohám	noha	k1gFnPc3
přibyly	přibýt	k5eAaPmAgFnP
i	i	k9
ruce	ruka	k1gFnPc1
<g/>
,	,	kIx,
celkový	celkový	k2eAgInSc1d1
zjev	zjev	k1gInSc1
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nS
lidské	lidský	k2eAgFnSc3d1
postavě	postava	k1gFnSc3
</s>
<s>
ASIMO	ASIMO	kA
<g/>
:	:	kIx,
V	v	k7c6
r.	r.	kA
2000	#num#	k4
vytvořen	vytvořit	k5eAaPmNgInS
první	první	k4xOgInSc1
prototyp	prototyp	k1gInSc1
humanoidního	humanoidní	k2eAgMnSc2d1
robota	robot	k1gMnSc2
ASIMO	ASIMO	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
Advanced	Advanced	k1gInSc1
Step	step	k1gFnSc1
in	in	k?
Innovative	Innovativ	k1gInSc5
MObility	mobilita	k1gFnSc2
–	–	k?
zjevně	zjevně	k6eAd1
naráží	narážet	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
jméno	jméno	k1gNnSc4
Isaaca	Isaac	k1gInSc2
Asimova	Asimův	k2eAgInSc2d1
<g/>
)	)	kIx)
</s>
<s>
KAIST	KAIST	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
<g/>
:	:	kIx,
Korea	Korea	k1gFnSc1
Advanced	Advanced	k1gInSc1
Institute	institut	k1gInSc5
of	of	k?
Science	Science	k1gFnSc1
and	and	k?
Technology	technolog	k1gMnPc7
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
serie	serie	k1gFnSc1
KHR-0	KHR-0	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
–	–	k?
KHR-2	KHR-2	k1gFnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
–	–	k?
kráčející	kráčející	k2eAgFnSc2d1
humanoidní	humanoidní	k2eAgFnSc2d1
roboty	robota	k1gFnSc2
(	(	kIx(
<g/>
trochu	trochu	k6eAd1
podobné	podobný	k2eAgInPc4d1
androidům	android	k1gInPc3
Honda	honda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
modelu	model	k1gInSc2
KHR-3	KHR-3	k1gFnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
HUBO	huba	k1gFnSc5
jako	jako	k9
akronym	akronym	k1gInSc1
HUmanoid	HUmanoid	k1gInSc1
roBOt	robot	k1gInSc4
–	–	k?
nejznámější	známý	k2eAgMnSc1d3
Albert	Albert	k1gMnSc1
HUBO	huba	k1gFnSc5
dle	dle	k7c2
masky	maska	k1gFnSc2
napodobující	napodobující	k2eAgInSc4d1
obličej	obličej	k1gInSc4
Albert	Albert	k1gMnSc1
Einstein	Einstein	k1gMnSc1
–	–	k?
maska	maska	k1gFnSc1
byla	být	k5eAaImAgFnS
oživena	oživit	k5eAaPmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
dokáže	dokázat	k5eAaPmIp3nS
napodobovat	napodobovat	k5eAaImF
i	i	k9
různé	různý	k2eAgFnPc4d1
grimasy	grimasa	k1gFnPc4
obličeje	obličej	k1gInSc2
–	–	k?
celkem	celkem	k6eAd1
má	mít	k5eAaImIp3nS
tento	tento	k3xDgInSc1
android	android	k1gInSc1
66	#num#	k4
stupňů	stupeň	k1gInPc2
volnosti	volnost	k1gFnSc2
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Fujitsu	Fujitsa	k1gFnSc4
Automation	Automation	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
<g/>
:	:	kIx,
Miyachi	Miyachi	k1gNnSc4
Systems	Systemsa	k1gFnPc2
Corp	Corp	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
však	však	k9
již	již	k6eAd1
výrobou	výroba	k1gFnSc7
humanoidů	humanoid	k1gInPc2
zřejmě	zřejmě	k6eAd1
nezabývá	zabývat	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
serie	serie	k1gFnSc1
HOAP	HOAP	kA
=	=	kIx~
akronym	akronym	k1gInSc1
"	"	kIx"
<g/>
Humanoid	Humanoid	k1gInSc1
for	forum	k1gNnPc2
Open	Opena	k1gFnPc2
Architecture	Architectur	k1gMnSc5
Platform	Platform	k1gInSc4
<g/>
"	"	kIx"
=	=	kIx~
humanoidi	humanoid	k1gMnPc1
pro	pro	k7c4
otevřenou	otevřený	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
řada	řada	k1gFnSc1
středně	středně	k6eAd1
velkých	velká	k1gFnPc2
komerčně	komerčně	k6eAd1
vyráběných	vyráběný	k2eAgInPc2d1
androidů	android	k1gInPc2
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
na	na	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
RT-Linux	RT-Linux	k1gInSc1
(	(	kIx(
<g/>
tj.	tj.	kA
real-time	real-timat	k5eAaPmIp3nS
Linux	linux	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prototypy	prototyp	k1gInPc7
<g/>
:	:	kIx,
HOAP-1	HOAP-1	k1gFnSc7
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
HOAP-2	HOAP-2	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
HOAP-3	HOAP-3	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určený	určený	k2eAgInSc1d1
převážně	převážně	k6eAd1
pro	pro	k7c4
výzkumné	výzkumný	k2eAgInPc4d1
a	a	k8xC
vývojové	vývojový	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Sony	Sony	kA
<g/>
:	:	kIx,
</s>
<s>
QRIO	QRIO	kA
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
–	–	k?
0,6	0,6	k4
m	m	kA
vysoký	vysoký	k2eAgInSc4d1
android	android	k1gInSc4
<g/>
,	,	kIx,
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
emotivní	emotivní	k2eAgFnSc1d1
hračka	hračka	k1gFnSc1
</s>
<s>
Osaka	Osaka	k1gFnSc1
University	universita	k1gFnSc2
(	(	kIx(
<g/>
návrh	návrh	k1gInSc1
<g/>
)	)	kIx)
+	+	kIx~
Kokoro	Kokora	k1gFnSc5
Company	Compan	k1gInPc7
Ltd	ltd	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
výroba	výroba	k1gFnSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
modely	model	k1gInPc1
Actroid	Actroid	k1gInSc1
–	–	k?
od	od	k7c2
r.	r.	kA
2003	#num#	k4
–	–	k?
celkem	celkem	k6eAd1
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
deset	deset	k4xCc4
androidů	android	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
působit	působit	k5eAaImF
zjevem	zjev	k1gInSc7
jako	jako	k8xC,k8xS
člověk	člověk	k1gMnSc1
–	–	k?
nebo	nebo	k8xC
spíš	spíš	k9
jako	jako	k9
žena	žena	k1gFnSc1
–	–	k?
proto	proto	k8xC
se	se	k3xPyFc4
pro	pro	k7c4
tento	tento	k3xDgInSc4
zjev	zjev	k1gInSc4
humanoidních	humanoidní	k2eAgInPc2d1
robotů	robot	k1gInPc2
ujal	ujmout	k5eAaPmAgInS
název	název	k1gInSc1
gynoid	gynoid	k1gInSc1
<g/>
,	,	kIx,
tj.	tj.	kA
ženský	ženský	k2eAgInSc1d1
robot	robot	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
PAL	pal	k1gInSc1
Robotics	Robotics	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
serie	serie	k1gFnSc1
REEM	REEM	kA
<g/>
:	:	kIx,
REEM-1	REEM-1	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
REEM-2	REEM-2	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
–	–	k?
pohybují	pohybovat	k5eAaImIp3nP
se	se	k3xPyFc4
po	po	k7c6
budovách	budova	k1gFnPc6
<g/>
,	,	kIx,
orientují	orientovat	k5eAaBmIp3nP
se	se	k3xPyFc4
v	v	k7c6
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
rozpoznávají	rozpoznávat	k5eAaImIp3nP
překážky	překážka	k1gFnPc1
<g/>
,	,	kIx,
přemísťují	přemísťovat	k5eAaImIp3nP
břemena	břemeno	k1gNnPc1
<g/>
,	,	kIx,
hrají	hrát	k5eAaImIp3nP
šachy	šach	k1gInPc1
atd.	atd.	kA
</s>
<s>
TOSY	TOSY	kA
Robotics	Robotics	k1gInSc1
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
<g/>
;	;	kIx,
firma	firma	k1gFnSc1
vyrábí	vyrábět	k5eAaImIp3nS
především	především	k9
industriální	industriální	k2eAgInPc1d1
roboty	robot	k1gInPc1
<g/>
,	,	kIx,
spíš	spíš	k9
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zviditelnila	zviditelnit	k5eAaPmAgFnS
<g/>
,	,	kIx,
vyvíjí	vyvíjet	k5eAaImIp3nS
i	i	k8xC
androidy	android	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
TOPIO	TOPIO	kA
–	–	k?
nejznámější	známý	k2eAgInSc4d3
android	android	k1gInSc4
této	tento	k3xDgFnSc2
firmy	firma	k1gFnSc2
<g/>
,	,	kIx,
akronym	akronym	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
TOSY	TOSY	kA
Ping	pingo	k1gNnPc2
Pong	Pong	k1gInSc4
Playing	Playing	k1gInSc4
Robot	robota	k1gFnPc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
robot	robot	k1gInSc1
hrající	hrající	k2eAgInSc1d1
Stolní	stolní	k2eAgInSc4d1
tenis	tenis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
začal	začít	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byly	být	k5eAaImAgFnP
postaveny	postaven	k2eAgFnPc1d1
verze	verze	k1gFnPc1
1.0	1.0	k4
<g/>
,	,	kIx,
2.0	2.0	k4
a	a	k8xC
3.0	3.0	k4
</s>
<s>
Aldebaran	Aldebaran	k1gInSc1
Robotics	Robotics	k1gInSc1
–	–	k?
francouzská	francouzský	k2eAgFnSc1d1
firma	firma	k1gFnSc1
</s>
<s>
Nao	Nao	k?
(	(	kIx(
<g/>
robot	robot	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
vyvíjený	vyvíjený	k2eAgMnSc1d1
od	od	k7c2
r.	r.	kA
2005	#num#	k4
<g/>
,	,	kIx,
středně	středně	k6eAd1
vysoký	vysoký	k2eAgInSc4d1
android	android	k1gInSc4
<g/>
,	,	kIx,
sériově	sériově	k6eAd1
vyráběný	vyráběný	k2eAgMnSc1d1
dle	dle	k7c2
různých	různý	k2eAgInPc2d1
prototypů	prototyp	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
akademická	akademický	k2eAgFnSc1d1
verse	verse	k1gFnSc1
pro	pro	k7c4
výzkumné	výzkumný	k2eAgInPc4d1
účely	účel	k1gInPc4
anebo	anebo	k8xC
jako	jako	k9
hráč	hráč	k1gMnSc1
do	do	k7c2
soutěže	soutěž	k1gFnSc2
RoboCup	RoboCup	k1gMnSc1
(	(	kIx(
<g/>
Robot	robot	k1gMnSc1
Soccer	Soccer	k1gMnSc1
World	World	k1gMnSc1
Cup	cup	k1gInSc4
<g/>
,	,	kIx,
tj.	tj.	kA
fotbalový	fotbalový	k2eAgInSc1d1
šampionát	šampionát	k1gInSc4
<g/>
,	,	kIx,
hraný	hraný	k2eAgInSc4d1
roboty	robot	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Takto	takto	k6eAd1
androida	android	k1gMnSc4
popsal	popsat	k5eAaPmAgInS
Isaac	Isaac	k1gInSc1
Asimov	Asimov	k1gInSc1
v	v	k7c6
povídce	povídka	k1gFnSc6
Dvěstěletý	dvěstěletý	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
z	z	k7c2
antologie	antologie	k1gFnSc2
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
robot	robot	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
hlavní	hlavní	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
povýšil	povýšit	k5eAaPmAgMnS
z	z	k7c2
robota	robot	k1gMnSc2
na	na	k7c4
androida	android	k1gMnSc4
a	a	k8xC
nakonec	nakonec	k6eAd1
i	i	k9
na	na	k7c4
člověka	člověk	k1gMnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
jestli	jestli	k8xS
vypadal	vypadat	k5eAaImAgInS,k5eAaPmAgInS
spíš	spíš	k9
jako	jako	k9
číšník	číšník	k1gMnSc1
anebo	anebo	k8xC
nápojový	nápojový	k2eAgInSc1d1
automat	automat	k1gInSc1
<g/>
,	,	kIx,
jaký	jaký	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
známe	znát	k5eAaImIp1nP
dnes	dnes	k6eAd1
<g/>
↑	↑	k?
DARPA	DARPA	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
top	topit	k5eAaImRp2nS
robotics	robotics	k1gInSc1
challenge	challenge	k1gNnSc6
contender	contender	k1gInSc1
to	ten	k3xDgNnSc1
become	becom	k1gInSc5
a	a	k8xC
commercial	commercial	k1gMnSc1
Google	Google	k1gFnSc2
robot	robot	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Kybernetika	kybernetika	k1gFnSc1
</s>
<s>
Android	android	k1gInSc1
(	(	kIx(
<g/>
operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Bionika	Bionik	k1gMnSc4
</s>
<s>
Robot	robot	k1gMnSc1
</s>
<s>
Kyborg	Kyborg	k1gMnSc1
</s>
<s>
Droid	Droid	k1gInSc1
</s>
<s>
Dron	Dron	k1gMnSc1
</s>
<s>
Gynoid	Gynoid	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
android	android	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
android	android	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Eva	Eva	k1gFnSc1
Voldřichová	Voldřichová	k1gFnSc1
Beránková	Beránková	k1gFnSc1
<g/>
:	:	kIx,
A	a	k9
muž	muž	k1gMnSc1
stvořil	stvořit	k5eAaPmAgMnS
ženu	žena	k1gFnSc4
<g/>
...	...	k?
Krátké	Krátké	k2eAgNnSc3d1
zamyšlení	zamyšlení	k1gNnSc3
nad	nad	k7c7
moderními	moderní	k2eAgFnPc7d1
verzemi	verze	k1gFnPc7
pygmaliónského	pygmaliónský	k2eAgInSc2d1
mýtu	mýtus	k1gInSc2
</s>
