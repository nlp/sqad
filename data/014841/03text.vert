<s>
Stadion	stadion	k1gInSc4
Rudolfa	Rudolf	k1gMnSc2
Labaje	Labaj	k1gInSc2
</s>
<s>
Stadion	stadion	k1gInSc1
Rudolfa	Rudolf	k1gMnSc4
Labaje	Labaj	k1gInSc2
Poloha	poloha	k1gFnSc1
</s>
<s>
Třinec	Třinec	k1gInSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
6,1	6,1	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
′	′	k?
<g/>
4,33	4,33	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Přestavění	přestavění	k1gNnSc2
</s>
<s>
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Povrch	povrch	k1gInSc1
</s>
<s>
tráva	tráva	k1gFnSc1
Týmy	tým	k1gInPc1
</s>
<s>
FK	FK	kA
Fotbal	fotbal	k1gInSc1
Třinec	Třinec	k1gInSc1
Kapacita	kapacita	k1gFnSc1
</s>
<s>
2	#num#	k4
200	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Rozměry	rozměra	k1gFnSc2
</s>
<s>
105	#num#	k4
×	×	k?
68	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Stadion	stadion	k1gInSc4
Rudolfa	Rudolf	k1gMnSc2
Labaje	Labaj	k1gInSc2
je	být	k5eAaImIp3nS
fotbalový	fotbalový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
slezském	slezský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Třinci	Třinec	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
zde	zde	k6eAd1
odehrává	odehrávat	k5eAaImIp3nS
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
FK	FK	kA
Fotbal	fotbal	k1gInSc1
Třinec	Třinec	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Maximální	maximální	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
stadionu	stadion	k1gInSc2
činí	činit	k5eAaImIp3nS
2	#num#	k4
200	#num#	k4
sedicích	sedicí	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Stadion	stadion	k1gInSc1
je	být	k5eAaImIp3nS
bez	bez	k7c2
umělého	umělý	k2eAgNnSc2d1
osvětlení	osvětlení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pojmenován	pojmenován	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
po	po	k7c6
bývalém	bývalý	k2eAgInSc6d1
třineckém	třinecký	k2eAgInSc6d1
trenérovi	trenér	k1gMnSc3
Rudolfu	Rudolf	k1gMnSc3
Labajovi	Labaj	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2016	#num#	k4
by	by	kYmCp3nP
měla	mít	k5eAaImAgFnS
proběhnout	proběhnout	k5eAaPmF
rekonstrukce	rekonstrukce	k1gFnSc1
hlavní	hlavní	k2eAgFnSc2d1
tribuny	tribuna	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
ke	k	k7c3
kompletnímu	kompletní	k2eAgNnSc3d1
zastřešení	zastřešení	k1gNnSc3
a	a	k8xC
k	k	k7c3
výměně	výměna	k1gFnSc3
zastaralých	zastaralý	k2eAgFnPc2d1
sedaček	sedačka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Třinečtí	třinecký	k2eAgMnPc1d1
fotbalisté	fotbalista	k1gMnPc1
přestavějí	přestavět	k5eAaPmIp3nP
stadion	stadion	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
nevědí	vědět	k5eNaImIp3nP
<g/>
,	,	kIx,
kde	kde	k6eAd1
budou	být	k5eAaImBp3nP
hrát	hrát	k5eAaImF
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
2.06	2.06	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
"	"	kIx"
<g/>
Stadion	stadion	k1gNnSc4
Rudolfa	Rudolf	k1gMnSc2
Labaje	Labaj	k1gInSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fotbaltrinec	fotbaltrinec	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Stadión	stadión	k1gInSc1
Rudolfa	Rudolf	k1gMnSc4
Labaje	Labaj	k1gInSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fotbalovestadiony	fotbalovestadion	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
"	"	kIx"
<g/>
Oslavy	oslava	k1gFnSc2
90	#num#	k4
let	léto	k1gNnPc2
třineckého	třinecký	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
jsou	být	k5eAaImIp3nP
minulostí	minulost	k1gFnSc7
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
denik	denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
14.09	14.09	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
stadionu	stadion	k1gInSc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
fotbalovestadiony	fotbalovestadion	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
stadionu	stadion	k1gInSc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
europlan-online	europlan-onlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
de	de	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Slezsko	Slezsko	k1gNnSc1
</s>
