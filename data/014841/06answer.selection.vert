<s desamb="1">
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
zde	zde	k6eAd1
odehrává	odehrávat	k5eAaImIp3nS
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
FK	FK	kA
Fotbal	fotbal	k1gInSc1
Třinec	Třinec	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Maximální	maximální	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
stadionu	stadion	k1gInSc2
činí	činit	k5eAaImIp3nS
2	#num#	k4
200	#num#	k4
sedicích	sedicí	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Stadion	stadion	k1gInSc1
je	být	k5eAaImIp3nS
bez	bez	k7c2
umělého	umělý	k2eAgNnSc2d1
osvětlení	osvětlení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pojmenován	pojmenován	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
po	po	k7c6
bývalém	bývalý	k2eAgInSc6d1
třineckém	třinecký	k2eAgInSc6d1
trenérovi	trenér	k1gMnSc3
Rudolfu	Rudolf	k1gMnSc3
Labajovi	Labaj	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>