<s>
Lutecium	lutecium	k1gNnSc4	lutecium
objevili	objevit	k5eAaPmAgMnP	objevit
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Georges	Georges	k1gMnSc1	Georges
Urbain	Urbain	k1gMnSc1	Urbain
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
mineralog	mineralog	k1gMnSc1	mineralog
Carl	Carl	k1gMnSc1	Carl
Auer	Auer	k1gMnSc1	Auer
von	von	k1gInSc4	von
Welsbach	Welsbacha	k1gFnPc2	Welsbacha
jako	jako	k8xC	jako
nečistotu	nečistota	k1gFnSc4	nečistota
v	v	k7c6	v
oxidu	oxid	k1gInSc6	oxid
dalšího	další	k2eAgInSc2d1	další
lanthanoidu	lanthanoid	k1gInSc2	lanthanoid
-	-	kIx~	-
ytterbia	ytterbium	k1gNnSc2	ytterbium
<g/>
.	.	kIx.	.
</s>
