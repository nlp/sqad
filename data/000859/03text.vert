<s>
Lutecium	lutecium	k1gNnSc1	lutecium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Lu	Lu	k1gFnSc2	Lu
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Lutetium	Lutetium	k1gNnSc4	Lutetium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
vnitřně	vnitřně	k6eAd1	vnitřně
přechodný	přechodný	k2eAgInSc1d1	přechodný
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
pevného	pevný	k2eAgNnSc2d1	pevné
skupenství	skupenství	k1gNnSc2	skupenství
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc1d1	poslední
člen	člen	k1gInSc1	člen
skupiny	skupina	k1gFnSc2	skupina
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Lutecium	lutecium	k1gNnSc1	lutecium
je	být	k5eAaImIp3nS	být
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgInSc1d1	měkký
přechodný	přechodný	k2eAgInSc1d1	přechodný
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
kovové	kovový	k2eAgNnSc4d1	kovové
lutecium	lutecium	k1gNnSc4	lutecium
poměrně	poměrně	k6eAd1	poměrně
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suchém	suchý	k2eAgInSc6d1	suchý
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
oxidu	oxid	k1gInSc2	oxid
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Lu	Lu	k1gFnSc2	Lu
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
Lu	Lu	k1gFnPc2	Lu
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
podobné	podobný	k2eAgFnPc4d1	podobná
sloučeninám	sloučenina	k1gFnPc3	sloučenina
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
například	například	k6eAd1	například
vysoce	vysoce	k6eAd1	vysoce
stabilní	stabilní	k2eAgInPc1d1	stabilní
oxidy	oxid	k1gInPc1	oxid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nereagují	reagovat	k5eNaBmIp3nP	reagovat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
redukují	redukovat	k5eAaBmIp3nP	redukovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
solí	sůl	k1gFnPc2	sůl
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
především	především	k9	především
fluoridy	fluorid	k1gInPc4	fluorid
a	a	k8xC	a
fosforečnany	fosforečnan	k1gInPc4	fosforečnan
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
nerozpustnosti	nerozpustnost	k1gFnPc1	nerozpustnost
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
separaci	separace	k1gFnSc3	separace
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
<g/>
.	.	kIx.	.
</s>
<s>
Lutecité	Lutecitý	k2eAgFnPc1d1	Lutecitý
soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
.	.	kIx.	.
</s>
<s>
Lutecium	lutecium	k1gNnSc4	lutecium
objevili	objevit	k5eAaPmAgMnP	objevit
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Georges	Georges	k1gMnSc1	Georges
Urbain	Urbain	k1gMnSc1	Urbain
a	a	k8xC	a
rakouský	rakouský	k2eAgMnSc1d1	rakouský
mineralog	mineralog	k1gMnSc1	mineralog
Carl	Carl	k1gMnSc1	Carl
Auer	Auer	k1gMnSc1	Auer
von	von	k1gInSc4	von
Welsbach	Welsbacha	k1gFnPc2	Welsbacha
jako	jako	k8xS	jako
nečistotu	nečistota	k1gFnSc4	nečistota
v	v	k7c6	v
oxidu	oxid	k1gInSc6	oxid
dalšího	další	k2eAgInSc2d1	další
lanthanoidu	lanthanoid	k1gInSc2	lanthanoid
-	-	kIx~	-
ytterbia	ytterbium	k1gNnSc2	ytterbium
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dal	dát	k5eAaPmAgInS	dát
prvku	prvek	k1gInSc2	prvek
Georges	Georges	k1gMnSc1	Georges
Urbain	Urbain	k1gMnSc1	Urbain
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
latinského	latinský	k2eAgNnSc2d1	latinské
pojmenování	pojmenování	k1gNnSc2	pojmenování
Paříže	Paříž	k1gFnSc2	Paříž
-	-	kIx~	-
Lutetia	Lutetia	k1gFnSc1	Lutetia
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gInSc1	Carl
Auer	Auer	k1gInSc1	Auer
von	von	k1gInSc1	von
Welsbach	Welsbach	k1gInSc4	Welsbach
preferoval	preferovat	k5eAaImAgInS	preferovat
název	název	k1gInSc1	název
cassiopium	cassiopium	k1gNnSc1	cassiopium
a	a	k8xC	a
oficiální	oficiální	k2eAgNnSc1d1	oficiální
pojmenování	pojmenování	k1gNnSc1	pojmenování
posledního	poslední	k2eAgNnSc2d1	poslední
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
bylo	být	k5eAaImAgNnS	být
tak	tak	k9	tak
poměrně	poměrně	k6eAd1	poměrně
dlouho	dlouho	k6eAd1	dlouho
předmětem	předmět	k1gInSc7	předmět
sporů	spor	k1gInPc2	spor
a	a	k8xC	a
debat	debata	k1gFnPc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Lutecium	lutecium	k1gNnSc1	lutecium
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dosti	dosti	k6eAd1	dosti
vzácný	vzácný	k2eAgInSc4d1	vzácný
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
0,75	[number]	k4	0,75
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
obsahu	obsah	k1gInSc6	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
údaje	údaj	k1gInPc1	údaj
chybí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
lutecia	lutecium	k1gNnSc2	lutecium
na	na	k7c4	na
1000	[number]	k4	1000
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
lutecium	lutecium	k1gNnSc1	lutecium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
však	však	k9	však
ani	ani	k9	ani
minerály	minerál	k1gInPc1	minerál
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
lanthanoidy	lanthanoida	k1gFnPc1	lanthanoida
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc1	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
<g/>
)	)	kIx)	)
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
minerály	minerál	k1gInPc4	minerál
směsné	směsný	k2eAgInPc4d1	směsný
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejznámější	známý	k2eAgFnSc7d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
monazity	monazit	k1gInPc1	monazit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Th	Th	k1gFnSc1	Th
<g/>
,	,	kIx,	,
Nd	Nd	k1gFnSc1	Nd
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
a	a	k8xC	a
xenotim	xenotim	k1gInSc1	xenotim
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
fosforečnany	fosforečnan	k1gInPc1	fosforečnan
lanthanoidů	lanthanoid	k1gInPc2	lanthanoid
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
bastnäsity	bastnäsit	k1gInPc1	bastnäsit
(	(	kIx(	(
<g/>
Ce	Ce	k1gFnSc1	Ce
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
,	,	kIx,	,
Y	Y	kA	Y
<g/>
)	)	kIx)	)
<g/>
CO	co	k8xS	co
<g/>
<g />
.	.	kIx.	.
</s>
<s>
3	[number]	k4	3
<g/>
F	F	kA	F
-	-	kIx~	-
směsné	směsný	k2eAgInPc1d1	směsný
flourouhličitany	flourouhličitan	k1gInPc1	flourouhličitan
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
a	a	k8xC	a
např.	např.	kA	např.
minerál	minerál	k1gInSc1	minerál
euxenit	euxenit	k5eAaImF	euxenit
(	(	kIx(	(
<g/>
Y	Y	kA	Y
<g/>
,	,	kIx,	,
<g/>
Ca	ca	kA	ca
<g/>
,	,	kIx,	,
<g/>
Ce	Ce	k1gMnSc1	Ce
<g/>
,	,	kIx,	,
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
<g/>
Th	Th	k1gFnSc1	Th
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
Nb	Nb	k1gFnSc1	Nb
<g/>
,	,	kIx,	,
<g/>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
<g/>
Ti	ty	k3xPp2nSc3	ty
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
ve	v	k7c4	v
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
fosfátové	fosfátový	k2eAgFnPc4d1	fosfátová
suroviny	surovina	k1gFnPc4	surovina
-	-	kIx~	-
apatity	apatit	k1gInPc4	apatit
z	z	k7c2	z
poloostrova	poloostrov	k1gInSc2	poloostrov
Kola	kolo	k1gNnSc2	kolo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
výrobě	výroba	k1gFnSc6	výroba
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgInPc2d1	vzácný
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
rudy	ruda	k1gFnPc1	ruda
nejprve	nejprve	k6eAd1	nejprve
louží	loužit	k5eAaImIp3nP	loužit
směsí	směs	k1gFnSc7	směs
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
ze	z	k7c2	z
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
roztoku	roztok	k1gInSc2	roztok
solí	solit	k5eAaImIp3nS	solit
se	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
hydroxidy	hydroxid	k1gInPc7	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Separace	separace	k1gFnSc1	separace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
řadou	řada	k1gFnSc7	řada
různých	různý	k2eAgInPc2d1	různý
postupů	postup	k1gInPc2	postup
-	-	kIx~	-
kapalinovou	kapalinový	k2eAgFnSc7d1	kapalinová
extrakcí	extrakce	k1gFnSc7	extrakce
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
ionexových	ionexový	k2eAgFnPc2d1	ionexová
kolon	kolona	k1gFnPc2	kolona
nebo	nebo	k8xC	nebo
selektivním	selektivní	k2eAgNnSc7d1	selektivní
srážením	srážení	k1gNnSc7	srážení
nerozpustných	rozpustný	k2eNgFnPc2d1	nerozpustná
komplexních	komplexní	k2eAgFnPc2d1	komplexní
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
elektrochemicky	elektrochemicky	k6eAd1	elektrochemicky
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
směsi	směs	k1gFnSc2	směs
chloridů	chlorid	k1gInPc2	chlorid
lutecia	lutecium	k1gNnSc2	lutecium
<g/>
,	,	kIx,	,
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
sodíku	sodík	k1gInSc2	sodík
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgNnSc1d1	elementární
lutecium	lutecium	k1gNnSc1	lutecium
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
na	na	k7c6	na
grafitové	grafitový	k2eAgFnSc6d1	grafitová
katodě	katoda	k1gFnSc6	katoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
kladné	kladný	k2eAgFnSc6d1	kladná
elektrodě	elektroda	k1gFnSc6	elektroda
(	(	kIx(	(
<g/>
anodě	anoda	k1gFnSc6	anoda
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolňování	uvolňování	k1gNnSc3	uvolňování
elementárního	elementární	k2eAgInSc2d1	elementární
plynného	plynný	k2eAgInSc2d1	plynný
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
velmi	velmi	k6eAd1	velmi
řídkému	řídký	k2eAgInSc3d1	řídký
výskytu	výskyt	k1gInSc3	výskyt
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc3d1	vysoká
výrobní	výrobní	k2eAgFnSc3d1	výrobní
ceně	cena	k1gFnSc3	cena
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
nemají	mít	k5eNaImIp3nP	mít
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
kovové	kovový	k2eAgNnSc4d1	kovové
lutecium	lutecium	k1gNnSc4	lutecium
ani	ani	k8xC	ani
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
žádné	žádný	k3yNgNnSc4	žádný
významné	významný	k2eAgNnSc4d1	významné
komerční	komerční	k2eAgNnSc4d1	komerční
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Potenciálním	potenciální	k2eAgInSc7d1	potenciální
oborem	obor	k1gInSc7	obor
využití	využití	k1gNnSc1	využití
jsou	být	k5eAaImIp3nP	být
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
katalyzátory	katalyzátor	k1gInPc1	katalyzátor
pro	pro	k7c4	pro
petrochemický	petrochemický	k2eAgInSc4d1	petrochemický
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
polymerace	polymerace	k1gFnPc4	polymerace
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
II	II	kA	II
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lutecium	lutecium	k1gNnSc4	lutecium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lutecium	lutecium	k1gNnSc4	lutecium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
