<s>
Dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
trik	trik	k1gInSc1	trik
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Prestige	Prestige	k1gInSc1	Prestige
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
filmový	filmový	k2eAgInSc1d1	filmový
thriller	thriller	k1gInSc1	thriller
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
režiséra	režisér	k1gMnSc2	režisér
Christophera	Christopher	k1gMnSc2	Christopher
Nolana	Nolan	k1gMnSc2	Nolan
natočený	natočený	k2eAgInSc4d1	natočený
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Christophera	Christopher	k1gMnSc2	Christopher
Priesta	Priest	k1gMnSc2	Priest
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
Nežádoucí	žádoucí	k2eNgInSc1d1	nežádoucí
efekt	efekt	k1gInSc1	efekt
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Prestige	Prestige	k1gInSc1	Prestige
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
sleduje	sledovat	k5eAaImIp3nS	sledovat
Roberta	Robert	k1gMnSc4	Robert
Angiera	Angier	k1gMnSc4	Angier
a	a	k8xC	a
Alfreda	Alfred	k1gMnSc4	Alfred
Bordena	Borden	k1gMnSc4	Borden
<g/>
,	,	kIx,	,
rivaly	rival	k1gMnPc4	rival
a	a	k8xC	a
jevištní	jevištní	k2eAgMnPc4d1	jevištní
kouzelníky	kouzelník	k1gMnPc4	kouzelník
<g/>
,	,	kIx,	,
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Posedlí	posedlit	k5eAaPmIp3nS	posedlit
vytvářením	vytváření	k1gNnSc7	vytváření
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
iluzí	iluze	k1gFnPc2	iluze
upadnou	upadnout	k5eAaPmIp3nP	upadnout
do	do	k7c2	do
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
snah	snaha	k1gFnPc2	snaha
se	se	k3xPyFc4	se
trumfnout	trumfnout	k5eAaPmF	trumfnout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
tragické	tragický	k2eAgInPc4d1	tragický
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
Roberta	Robert	k1gMnSc2	Robert
Angiera	Angier	k1gMnSc2	Angier
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
Hugh	Hugh	k1gMnSc1	Hugh
Jackman	Jackman	k1gMnSc1	Jackman
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gMnSc4	on
protivníka	protivník	k1gMnSc4	protivník
Alfreda	Alfred	k1gMnSc4	Alfred
Bordena	Borden	k1gMnSc4	Borden
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Christian	Christian	k1gMnSc1	Christian
Bale	bal	k1gInSc5	bal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gMnSc2	Tesla
se	se	k3xPyFc4	se
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
objevil	objevit	k5eAaPmAgMnS	objevit
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
rolí	role	k1gFnPc2	role
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
Michael	Michael	k1gMnSc1	Michael
Caine	Cain	k1gInSc5	Cain
<g/>
,	,	kIx,	,
Scarlett	Scarlett	k2eAgMnSc1d1	Scarlett
Johansson	Johansson	k1gMnSc1	Johansson
<g/>
,	,	kIx,	,
Piper	Piper	k1gMnSc1	Piper
Perabo	Peraba	k1gFnSc5	Peraba
<g/>
,	,	kIx,	,
Andy	Anda	k1gFnPc4	Anda
Serkis	Serkis	k1gFnSc2	Serkis
a	a	k8xC	a
Rebecca	Rebecca	k1gMnSc1	Rebecca
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
režisér	režisér	k1gMnSc1	režisér
Nolan	Nolan	k1gMnSc1	Nolan
znovu	znovu	k6eAd1	znovu
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
Balem	bal	k1gInSc7	bal
a	a	k8xC	a
Cainem	Cain	k1gInSc7	Cain
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgMnPc7	který
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
již	již	k9	již
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
Batman	Batman	k1gMnSc1	Batman
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Priestův	Priestův	k2eAgInSc1d1	Priestův
román	román	k1gInSc1	román
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
převedl	převést	k5eAaPmAgInS	převést
Nolan	Nolan	k1gInSc1	Nolan
do	do	k7c2	do
scénáře	scénář	k1gInSc2	scénář
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Jonathanem	Jonathan	k1gMnSc7	Jonathan
a	a	k8xC	a
využili	využít	k5eAaPmAgMnP	využít
nelineární	lineární	k2eNgFnSc4d1	nelineární
formu	forma	k1gFnSc4	forma
vyprávění	vyprávění	k1gNnSc2	vyprávění
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Angier	Angier	k1gMnSc1	Angier
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Borden	Bordna	k1gFnPc2	Bordna
jsou	být	k5eAaImIp3nP	být
placení	placený	k2eAgMnPc1d1	placený
pomocníci	pomocník	k1gMnPc1	pomocník
kouzelníka	kouzelník	k1gMnSc2	kouzelník
Miltona	Milton	k1gMnSc2	Milton
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
kouzelnických	kouzelnický	k2eAgInPc2d1	kouzelnický
triků	trik	k1gInPc2	trik
rovněž	rovněž	k9	rovněž
John	John	k1gMnSc1	John
Cutter	Cutter	k1gMnSc1	Cutter
<g/>
.	.	kIx.	.
</s>
<s>
Angierova	Angierův	k2eAgFnSc1d1	Angierův
manželka	manželka	k1gFnSc1	manželka
se	se	k3xPyFc4	se
během	během	k7c2	během
jednoho	jeden	k4xCgNnSc2	jeden
vystoupení	vystoupení	k1gNnSc2	vystoupení
utopí	utopit	k5eAaPmIp3nS	utopit
a	a	k8xC	a
Angier	Angier	k1gInSc1	Angier
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Borden	Bordna	k1gFnPc2	Bordna
ji	on	k3xPp3gFnSc4	on
svázal	svázat	k5eAaPmAgInS	svázat
novým	nový	k2eAgInSc7d1	nový
uzlem	uzel	k1gInSc7	uzel
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
nebyla	být	k5eNaImAgFnS	být
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
a	a	k8xC	a
jenž	jenž	k3xRgInSc1	jenž
Borden	Bordna	k1gFnPc2	Bordna
předtím	předtím	k6eAd1	předtím
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
Cutterovi	Cutter	k1gMnSc3	Cutter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pohřbu	pohřeb	k1gInSc6	pohřeb
Borden	Bordna	k1gFnPc2	Bordna
Angiera	Angiera	k1gFnSc1	Angiera
rozčílí	rozčílit	k5eAaPmIp3nS	rozčílit
<g/>
,	,	kIx,	,
když	když	k8xS	když
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
uzlů	uzel	k1gInPc2	uzel
použil	použít	k5eAaPmAgInS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
začnou	začít	k5eAaPmIp3nP	začít
každý	každý	k3xTgInSc4	každý
zvlášť	zvlášť	k6eAd1	zvlášť
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
kouzelníci	kouzelník	k1gMnPc1	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordno	k1gNnPc2	Bordno
<g/>
,	,	kIx,	,
vystupující	vystupující	k2eAgMnPc1d1	vystupující
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
najme	najmout	k5eAaPmIp3nS	najmout
inženýra	inženýr	k1gMnSc4	inženýr
Bernarda	Bernard	k1gMnSc4	Bernard
Fallona	Fallon	k1gMnSc4	Fallon
<g/>
.	.	kIx.	.
</s>
<s>
Angier	Angier	k1gInSc1	Angier
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
Velký	velký	k2eAgInSc1d1	velký
Danton	Danton	k1gInSc1	Danton
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Cutterem	Cutter	k1gInSc7	Cutter
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
asistentku	asistentka	k1gFnSc4	asistentka
si	se	k3xPyFc3	se
najme	najmout	k5eAaPmIp3nS	najmout
Olivii	Olivie	k1gFnSc4	Olivie
Wenscombeovou	Wenscombeův	k2eAgFnSc7d1	Wenscombeův
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	s	k7c7	s
Sarah	Sarah	k1gFnSc7	Sarah
<g/>
,	,	kIx,	,
vezmou	vzít	k5eAaPmIp3nP	vzít
se	se	k3xPyFc4	se
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
dceru	dcera	k1gFnSc4	dcera
Jess	Jessa	k1gFnPc2	Jessa
<g/>
.	.	kIx.	.
</s>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
se	se	k3xPyFc4	se
necítí	cítit	k5eNaImIp3nS	cítit
dobře	dobře	k6eAd1	dobře
kvůli	kvůli	k7c3	kvůli
Bordenově	Bordenův	k2eAgFnSc3d1	Bordenův
zřejmé	zřejmý	k2eAgFnSc3d1	zřejmá
těkavosti	těkavost	k1gFnSc3	těkavost
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
miluje	milovat	k5eAaImIp3nS	milovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jednou	jednou	k6eAd1	jednou
Borden	Bordna	k1gFnPc2	Bordna
předvádí	předvádět	k5eAaImIp3nS	předvádět
číslo	číslo	k1gNnSc1	číslo
s	s	k7c7	s
chycením	chycení	k1gNnSc7	chycení
vystřelené	vystřelený	k2eAgFnSc2d1	vystřelená
kulky	kulka	k1gFnSc2	kulka
<g/>
,	,	kIx,	,
přestrojený	přestrojený	k2eAgMnSc1d1	přestrojený
Angier	Angier	k1gMnSc1	Angier
je	být	k5eAaImIp3nS	být
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c4	na
Bordena	Borden	k1gMnSc4	Borden
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
čísla	číslo	k1gNnSc2	číslo
se	se	k3xPyFc4	se
Angier	Angier	k1gInSc1	Angier
znovu	znovu	k6eAd1	znovu
dožaduje	dožadovat	k5eAaImIp3nS	dožadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
Borden	Bordna	k1gFnPc2	Bordna
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uzel	uzel	k1gInSc4	uzel
použil	použít	k5eAaPmAgMnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
a	a	k8xC	a
Fallon	Fallon	k1gInSc1	Fallon
si	se	k3xPyFc3	se
rychle	rychle	k6eAd1	rychle
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angier	Angier	k1gMnSc1	Angier
střelí	střelit	k5eAaPmIp3nS	střelit
Bordena	Bordeno	k1gNnSc2	Bordeno
nabitou	nabitý	k2eAgFnSc7d1	nabitá
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
Fallon	Fallon	k1gInSc1	Fallon
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
a	a	k8xC	a
kulka	kulka	k1gFnSc1	kulka
se	se	k3xPyFc4	se
trefí	trefit	k5eAaPmIp3nS	trefit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
Bordenových	Bordenův	k2eAgInPc2d1	Bordenův
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Přestrojený	přestrojený	k2eAgInSc1d1	přestrojený
Borden	Bordna	k1gFnPc2	Bordna
později	pozdě	k6eAd2	pozdě
sabotuje	sabotovat	k5eAaImIp3nS	sabotovat
Angierovo	Angierův	k2eAgNnSc1d1	Angierův
číslo	číslo	k1gNnSc1	číslo
s	s	k7c7	s
mizejícím	mizející	k2eAgMnSc7d1	mizející
ptákem	pták	k1gMnSc7	pták
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
poškodí	poškodit	k5eAaPmIp3nS	poškodit
Angierovu	Angierův	k2eAgFnSc4d1	Angierův
pověst	pověst	k1gFnSc4	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
začne	začít	k5eAaPmIp3nS	začít
brzy	brzy	k6eAd1	brzy
ohromovat	ohromovat	k5eAaImF	ohromovat
davy	dav	k1gInPc4	dav
svým	svůj	k3xOyFgFnPc3	svůj
číslem	číslo	k1gNnSc7	číslo
teleportujícího	teleportující	k2eAgInSc2d1	teleportující
se	se	k3xPyFc4	se
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgInSc2	který
nechává	nechávat	k5eAaImIp3nS	nechávat
hopsat	hopsat	k5eAaImF	hopsat
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
míček	míček	k1gInSc4	míček
<g/>
,	,	kIx,	,
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
skříně	skříň	k1gFnSc2	skříň
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
vyleze	vylézt	k5eAaPmIp3nS	vylézt
ze	z	k7c2	z
skříně	skříň	k1gFnSc2	skříň
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
pódia	pódium	k1gNnSc2	pódium
a	a	k8xC	a
míček	míček	k1gInSc1	míček
chytne	chytnout	k5eAaPmIp3nS	chytnout
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
kouzlo	kouzlo	k1gNnSc1	kouzlo
ohromí	ohromit	k5eAaPmIp3nS	ohromit
i	i	k9	i
Angiera	Angiera	k1gFnSc1	Angiera
a	a	k8xC	a
Olivii	Olivie	k1gFnSc3	Olivie
<g/>
.	.	kIx.	.
</s>
<s>
Posedlý	posedlý	k2eAgInSc1d1	posedlý
Bordenovou	Bordenový	k2eAgFnSc7d1	Bordenový
porážkou	porážka	k1gFnSc7	porážka
si	se	k3xPyFc3	se
najme	najmout	k5eAaPmIp3nS	najmout
Angier	Angier	k1gInSc4	Angier
dvojníka	dvojník	k1gMnSc2	dvojník
<g/>
,	,	kIx,	,
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
Bordenův	Bordenův	k2eAgInSc4d1	Bordenův
trik	trik	k1gInSc4	trik
a	a	k8xC	a
trochu	trochu	k6eAd1	trochu
ho	on	k3xPp3gMnSc4	on
pozmění	pozměnit	k5eAaPmIp3nS	pozměnit
<g/>
.	.	kIx.	.
</s>
<s>
Potlesk	potlesk	k1gInSc1	potlesk
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
si	se	k3xPyFc3	se
ale	ale	k9	ale
vychutnává	vychutnávat	k5eAaImIp3nS	vychutnávat
dvojník	dvojník	k1gMnSc1	dvojník
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Angier	Angier	k1gInSc1	Angier
ho	on	k3xPp3gNnSc4	on
může	moct	k5eAaImIp3nS	moct
poslouchat	poslouchat	k5eAaImF	poslouchat
pouze	pouze	k6eAd1	pouze
zpod	zpod	k7c2	zpod
pódia	pódium	k1gNnSc2	pódium
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
vadí	vadit	k5eAaImIp3nS	vadit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
chce	chtít	k5eAaImIp3nS	chtít
odhalit	odhalit	k5eAaPmF	odhalit
tajemství	tajemství	k1gNnSc4	tajemství
Bordenova	Bordenův	k2eAgInSc2d1	Bordenův
triku	trik	k1gInSc2	trik
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Angier	Angier	k1gInSc1	Angier
vyšle	vyslat	k5eAaPmIp3nS	vyslat
k	k	k7c3	k
Bordenovi	Borden	k1gMnSc3	Borden
Olivii	Olivie	k1gFnSc4	Olivie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ukradla	ukradnout	k5eAaPmAgFnS	ukradnout
jeho	jeho	k3xOp3gNnSc2	jeho
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Olivia	Olivia	k1gFnSc1	Olivia
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
Angierovi	Angier	k1gMnSc3	Angier
Bordenův	Bordenův	k2eAgInSc1d1	Bordenův
zašifrovaný	zašifrovaný	k2eAgInSc1d1	zašifrovaný
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Bordena	Borden	k1gMnSc2	Borden
a	a	k8xC	a
Angiera	Angier	k1gMnSc2	Angier
podvede	podvést	k5eAaPmIp3nS	podvést
–	–	k?	–
dovolí	dovolit	k5eAaPmIp3nP	dovolit
Bordenovi	Bordenův	k2eAgMnPc1d1	Bordenův
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pokazil	pokazit	k5eAaPmAgMnS	pokazit
Angierovo	Angierův	k2eAgNnSc4d1	Angierův
číslo	číslo	k1gNnSc4	číslo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstraní	odstranit	k5eAaPmIp3nS	odstranit
matraci	matrace	k1gFnSc4	matrace
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
má	mít	k5eAaImIp3nS	mít
Angier	Angier	k1gMnSc1	Angier
dopadnout	dopadnout	k5eAaPmF	dopadnout
<g/>
.	.	kIx.	.
</s>
<s>
Angier	Angier	k1gMnSc1	Angier
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
trvale	trvale	k6eAd1	trvale
zmrzačí	zmrzačit	k5eAaPmIp3nP	zmrzačit
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
Angier	Angier	k1gInSc4	Angier
a	a	k8xC	a
Cutter	Cutter	k1gInSc4	Cutter
unesou	unést	k5eAaPmIp3nP	unést
Fallona	Fallona	k1gFnSc1	Fallona
a	a	k8xC	a
zaživa	zaživa	k6eAd1	zaživa
ho	on	k3xPp3gMnSc4	on
pohřbí	pohřbít	k5eAaPmIp3nS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
polohu	poloha	k1gFnSc4	poloha
Bordenovi	Bordenův	k2eAgMnPc1d1	Bordenův
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jim	on	k3xPp3gMnPc3	on
prozradí	prozradit	k5eAaPmIp3nP	prozradit
heslo	heslo	k1gNnSc4	heslo
ke	k	k7c3	k
svému	své	k1gNnSc3	své
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
řekne	říct	k5eAaPmIp3nS	říct
Angierovi	Angier	k1gMnSc3	Angier
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
TESLA	Tesla	k1gFnSc1	Tesla
<g/>
"	"	kIx"	"
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
pouze	pouze	k6eAd1	pouze
šifrovací	šifrovací	k2eAgNnSc1d1	šifrovací
slovo	slovo	k1gNnSc1	slovo
k	k	k7c3	k
deníku	deník	k1gInSc3	deník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
klíč	klíč	k1gInSc1	klíč
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
iluzi	iluze	k1gFnSc3	iluze
teleportace	teleportace	k1gFnSc2	teleportace
<g/>
.	.	kIx.	.
</s>
<s>
Angier	Angier	k1gInSc1	Angier
odjede	odjet	k5eAaPmIp3nS	odjet
do	do	k7c2	do
Colorado	Colorado	k1gNnSc1	Colorado
Springs	Springs	k1gInSc4	Springs
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Nikolou	Nikola	k1gMnSc7	Nikola
Teslou	Tesla	k1gMnSc7	Tesla
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
tajemství	tajemství	k1gNnSc4	tajemství
Bordenovy	Bordenův	k2eAgFnSc2d1	Bordenův
iluze	iluze	k1gFnSc2	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
přístroj	přístroj	k1gInSc4	přístroj
pro	pro	k7c4	pro
teleportace	teleportace	k1gFnPc4	teleportace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
zpočátku	zpočátku	k6eAd1	zpočátku
nefunguje	fungovat	k5eNaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Angier	Angier	k1gMnSc1	Angier
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
z	z	k7c2	z
Bordenova	Bordenův	k2eAgInSc2d1	Bordenův
deníku	deník	k1gInSc2	deník
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
zbytečnou	zbytečný	k2eAgFnSc4d1	zbytečná
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Podvedený	podvedený	k2eAgMnSc1d1	podvedený
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
Teslovy	Teslův	k2eAgFnSc2d1	Teslova
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přístroj	přístroj	k1gInSc1	přístroj
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
duplikát	duplikát	k1gInSc4	duplikát
věci	věc	k1gFnSc2	věc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
umístěna	umístěn	k2eAgFnSc1d1	umístěna
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
někde	někde	k6eAd1	někde
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
potom	potom	k6eAd1	potom
musí	muset	k5eAaImIp3nS	muset
Colorado	Colorado	k1gNnSc1	Colorado
Springs	Springsa	k1gFnPc2	Springsa
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gMnSc1	jeho
rival	rival	k1gMnSc1	rival
Thomas	Thomas	k1gMnSc1	Thomas
Edison	Edison	k1gMnSc1	Edison
poslal	poslat	k5eAaPmAgMnS	poslat
své	svůj	k3xOyFgMnPc4	svůj
stoupence	stoupenec	k1gMnPc4	stoupenec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zničili	zničit	k5eAaPmAgMnP	zničit
Teslovu	Teslův	k2eAgFnSc4d1	Teslova
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Angierovi	Angier	k1gMnSc3	Angier
ale	ale	k9	ale
zanechá	zanechat	k5eAaPmIp3nS	zanechat
vylepšenou	vylepšený	k2eAgFnSc4d1	vylepšená
verzi	verze	k1gFnSc4	verze
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
dopisu	dopis	k1gInSc2	dopis
ho	on	k3xPp3gMnSc4	on
nabádá	nabádat	k5eAaBmIp3nS	nabádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přístroj	přístroj	k1gInSc1	přístroj
zničil	zničit	k5eAaPmAgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Bordenův	Bordenův	k2eAgInSc1d1	Bordenův
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
Olivii	Olivie	k1gFnSc3	Olivie
má	mít	k5eAaImIp3nS	mít
špatný	špatný	k2eAgInSc4d1	špatný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Sarah	Sarah	k1gFnSc4	Sarah
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začne	začít	k5eAaPmIp3nS	začít
pít	pít	k5eAaImF	pít
<g/>
.	.	kIx.	.
</s>
<s>
Bordenovo	Bordenův	k2eAgNnSc1d1	Bordenův
kolísavé	kolísavý	k2eAgNnSc1d1	kolísavé
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
střídavá	střídavý	k2eAgFnSc1d1	střídavá
náklonnost	náklonnost	k1gFnSc1	náklonnost
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
vztahem	vztah	k1gInSc7	vztah
mezi	mezi	k7c7	mezi
Bordenem	Borden	k1gInSc7	Borden
a	a	k8xC	a
Olivií	Olivie	k1gFnSc7	Olivie
dožene	dohnat	k5eAaPmIp3nS	dohnat
Sarah	Sarah	k1gFnSc1	Sarah
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
oběsí	oběsit	k5eAaPmIp3nS	oběsit
v	v	k7c6	v
Bordenově	Bordenův	k2eAgFnSc6d1	Bordenův
dílně	dílna	k1gFnSc6	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Angier	Angier	k1gInSc1	Angier
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
kvůli	kvůli	k7c3	kvůli
poslední	poslední	k2eAgFnSc3d1	poslední
stovce	stovka	k1gFnSc3	stovka
vystoupení	vystoupení	k1gNnSc2	vystoupení
s	s	k7c7	s
novým	nový	k2eAgNnSc7d1	nové
číslem	číslo	k1gNnSc7	číslo
–	–	k?	–
opravdu	opravdu	k9	opravdu
teleportovaným	teleportovaný	k2eAgMnSc7d1	teleportovaný
mužem	muž	k1gMnSc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cutter	Cutter	k1gInSc1	Cutter
u	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
čísla	číslo	k1gNnSc2	číslo
zůstane	zůstat	k5eAaPmIp3nS	zůstat
před	před	k7c7	před
jevištěm	jeviště	k1gNnSc7	jeviště
a	a	k8xC	a
v	v	k7c6	v
zákulisí	zákulisí	k1gNnSc6	zákulisí
mu	on	k3xPp3gNnSc3	on
budou	být	k5eAaImBp3nP	být
pomáhat	pomáhat	k5eAaImF	pomáhat
jen	jen	k9	jen
slepci	slepec	k1gMnPc1	slepec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
iluzi	iluze	k1gFnSc6	iluze
Angier	Angira	k1gFnPc2	Angira
zmizí	zmizet	k5eAaPmIp3nS	zmizet
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
elektrickém	elektrický	k2eAgInSc6d1	elektrický
oblouku	oblouk	k1gInSc6	oblouk
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
50	[number]	k4	50
yardů	yard	k1gInPc2	yard
od	od	k7c2	od
jeviště	jeviště	k1gNnSc2	jeviště
na	na	k7c6	na
balkóně	balkón	k1gInSc6	balkón
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
je	být	k5eAaImIp3nS	být
zmaten	zmást	k5eAaPmNgMnS	zmást
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objeví	objevit	k5eAaPmIp3nS	objevit
padací	padací	k2eAgFnPc4d1	padací
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
vystoupení	vystoupení	k1gNnSc6	vystoupení
sleduje	sledovat	k5eAaImIp3nS	sledovat
Angierovy	Angierův	k2eAgMnPc4d1	Angierův
pomocníky	pomocník	k1gMnPc4	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
přes	přes	k7c4	přes
město	město	k1gNnSc4	město
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
velkou	velký	k2eAgFnSc4d1	velká
nádrž	nádrž	k1gFnSc4	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
znovu	znovu	k6eAd1	znovu
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Angierovo	Angierův	k2eAgNnSc4d1	Angierův
vystoupení	vystoupení	k1gNnSc4	vystoupení
<g/>
,	,	kIx,	,
pronikne	proniknout	k5eAaPmIp3nS	proniknout
do	do	k7c2	do
zákulisí	zákulisí	k1gNnSc2	zákulisí
a	a	k8xC	a
objeví	objevit	k5eAaPmIp3nS	objevit
zamknutou	zamknutý	k2eAgFnSc4d1	zamknutá
nádrž	nádrž	k1gFnSc4	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
topí	topit	k5eAaImIp3nS	topit
Angier	Angier	k1gInSc1	Angier
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Angier	Angier	k1gInSc1	Angier
se	se	k3xPyFc4	se
utopí	utopit	k5eAaPmIp3nS	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Cutter	Cutter	k1gInSc4	Cutter
tam	tam	k6eAd1	tam
Bordena	Bordena	k1gFnSc1	Bordena
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
oběšení	oběšení	k1gNnSc3	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
Borden	Bordno	k1gNnPc2	Bordno
čte	číst	k5eAaImIp3nS	číst
Angierův	Angierův	k2eAgInSc1d1	Angierův
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
mluví	mluvit	k5eAaImIp3nS	mluvit
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
Bordenovi	Borden	k1gMnSc3	Borden
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
vraždu	vražda	k1gFnSc4	vražda
shnije	shnít	k5eAaPmIp3nS	shnít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Jess	Jessa	k1gFnPc2	Jessa
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
schovankyní	schovankyně	k1gFnSc7	schovankyně
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
Borden	Bordna	k1gFnPc2	Bordna
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
tajemnou	tajemný	k2eAgFnSc4d1	tajemná
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
lord	lord	k1gMnSc1	lord
Caldlow	Caldlow	k1gMnSc1	Caldlow
pošle	poslat	k5eAaPmIp3nS	poslat
svého	svůj	k3xOyFgMnSc4	svůj
advokáta	advokát	k1gMnSc4	advokát
za	za	k7c7	za
Bordenem	Borden	k1gInSc7	Borden
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Caldlow	Caldlow	k?	Caldlow
<g/>
,	,	kIx,	,
sběratel	sběratel	k1gMnSc1	sběratel
kouzelnického	kouzelnický	k2eAgNnSc2d1	kouzelnické
náčiní	náčiní	k1gNnSc2	náčiní
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
všechny	všechen	k3xTgInPc4	všechen
Bordenovy	Bordenův	k2eAgInPc4d1	Bordenův
přístroje	přístroj	k1gInPc4	přístroj
a	a	k8xC	a
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
pravdy	pravda	k1gFnSc2	pravda
o	o	k7c6	o
teleportujícím	teleportující	k2eAgMnSc6d1	teleportující
se	se	k3xPyFc4	se
muži	muž	k1gMnSc6	muž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
adoptuje	adoptovat	k5eAaPmIp3nS	adoptovat
Jess	Jess	k1gInSc1	Jess
a	a	k8xC	a
vychová	vychovat	k5eAaPmIp3nS	vychovat
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
bohatství	bohatství	k1gNnSc6	bohatství
a	a	k8xC	a
přepychu	přepych	k1gInSc6	přepych
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítá	odmítat	k5eAaImIp3nS	odmítat
odhalit	odhalit	k5eAaPmF	odhalit
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
ještě	ještě	k6eAd1	ještě
neuvidí	uvidět	k5eNaPmIp3nS	uvidět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
lord	lord	k1gMnSc1	lord
Caldlow	Caldlow	k1gMnSc2	Caldlow
Bordena	Bordeno	k1gNnSc2	Bordeno
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
popravy	poprava	k1gFnSc2	poprava
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
navštíví	navštívit	k5eAaPmIp3nS	navštívit
<g/>
,	,	kIx,	,
vyjde	vyjít	k5eAaPmIp3nS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Angiera	Angier	k1gMnSc4	Angier
<g/>
.	.	kIx.	.
</s>
<s>
Poražený	poražený	k1gMnSc1	poražený
Borden	Bordna	k1gFnPc2	Bordna
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
papíru	papír	k1gInSc2	papír
předá	předat	k5eAaPmIp3nS	předat
tajemství	tajemství	k1gNnSc4	tajemství
svého	svůj	k3xOyFgInSc2	svůj
triku	trik	k1gInSc2	trik
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Angier	Angier	k1gInSc1	Angier
jej	on	k3xPp3gMnSc4	on
roztrhá	roztrhat	k5eAaPmIp3nS	roztrhat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
přečetl	přečíst	k5eAaPmAgMnS	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Lorda	lord	k1gMnSc4	lord
navštíví	navštívit	k5eAaPmIp3nS	navštívit
rovněž	rovněž	k9	rovněž
Cutter	Cutter	k1gMnSc1	Cutter
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Angiera	Angiero	k1gNnPc4	Angiero
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Borden	Bordna	k1gFnPc2	Bordna
nevinný	vinný	k2eNgMnSc1d1	nevinný
<g/>
.	.	kIx.	.
</s>
<s>
Cutter	Cutter	k1gInSc1	Cutter
odhalí	odhalit	k5eAaPmIp3nS	odhalit
celou	celý	k2eAgFnSc4d1	celá
cenu	cena	k1gFnSc4	cena
Angierovy	Angierův	k2eAgFnSc2d1	Angierův
posedlosti	posedlost	k1gFnSc2	posedlost
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
adoptoval	adoptovat	k5eAaPmAgInS	adoptovat
Jess	Jess	k1gInSc1	Jess
<g/>
.	.	kIx.	.
</s>
<s>
Cutter	Cutter	k1gMnSc1	Cutter
je	být	k5eAaImIp3nS	být
rozčílený	rozčílený	k2eAgMnSc1d1	rozčílený
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Bordena	Borden	k1gMnSc4	Borden
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
oběšen	oběsit	k5eAaPmNgInS	oběsit
<g/>
.	.	kIx.	.
</s>
<s>
Cutter	Cutter	k1gInSc1	Cutter
doprovodí	doprovodit	k5eAaPmIp3nS	doprovodit
Angiera	Angier	k1gMnSc4	Angier
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
skladovány	skladován	k2eAgFnPc1d1	skladována
nádrže	nádrž	k1gFnPc1	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
mu	on	k3xPp3gMnSc3	on
uskladnit	uskladnit	k5eAaPmF	uskladnit
přístroj	přístroj	k1gInSc4	přístroj
na	na	k7c4	na
teleportaci	teleportace	k1gFnSc4	teleportace
<g/>
.	.	kIx.	.
</s>
<s>
Cutter	Cutter	k1gInSc1	Cutter
odchází	odcházet	k5eAaImIp3nS	odcházet
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mlčky	mlčky	k6eAd1	mlčky
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přišel	přijít	k5eAaPmAgMnS	přijít
Borden	Bordna	k1gFnPc2	Bordna
a	a	k8xC	a
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
Angiera	Angiero	k1gNnPc4	Angiero
<g/>
.	.	kIx.	.
</s>
<s>
Borden	Bordna	k1gFnPc2	Bordna
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
"	"	kIx"	"
<g/>
Fallon	Fallon	k1gInSc1	Fallon
<g/>
"	"	kIx"	"
byli	být	k5eAaImAgMnP	být
identická	identický	k2eAgNnPc4d1	identické
dvojčata	dvojče	k1gNnPc4	dvojče
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
žila	žít	k5eAaImAgFnS	žít
jeden	jeden	k4xCgInSc4	jeden
život	život	k1gInSc4	život
a	a	k8xC	a
měnila	měnit	k5eAaImAgFnS	měnit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
dvojče	dvojče	k1gNnSc1	dvojče
bylo	být	k5eAaImAgNnS	být
manželem	manžel	k1gMnSc7	manžel
Sarah	Sarah	k1gFnSc2	Sarah
a	a	k8xC	a
otcem	otec	k1gMnSc7	otec
Jess	Jessa	k1gFnPc2	Jessa
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc6	druhý
milovalo	milovat	k5eAaImAgNnS	milovat
Olivii	Olivie	k1gFnSc3	Olivie
–	–	k?	–
to	ten	k3xDgNnSc1	ten
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c6	na
šibenici	šibenice	k1gFnSc6	šibenice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
iluzi	iluze	k1gFnSc6	iluze
bylo	být	k5eAaImAgNnS	být
dvojníkem	dvojník	k1gMnSc7	dvojník
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
dvojče	dvojče	k1gNnSc1	dvojče
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
iluzím	iluze	k1gFnPc3	iluze
byli	být	k5eAaImAgMnP	být
tak	tak	k9	tak
oddáni	oddán	k2eAgMnPc1d1	oddán
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
bratr	bratr	k1gMnSc1	bratr
usekl	useknout	k5eAaPmAgMnS	useknout
druhému	druhý	k4xOgInSc3	druhý
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
napodobil	napodobit	k5eAaPmAgMnS	napodobit
zranění	zranění	k1gNnSc4	zranění
prvního	první	k4xOgMnSc4	první
<g/>
.	.	kIx.	.
</s>
<s>
Flashbacky	Flashbacky	k6eAd1	Flashbacky
odhalí	odhalit	k5eAaPmIp3nS	odhalit
rovněž	rovněž	k9	rovněž
Angierovu	Angierův	k2eAgFnSc4d1	Angierův
metodu	metoda	k1gFnSc4	metoda
–	–	k?	–
pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
,	,	kIx,	,
když	když	k8xS	když
během	během	k7c2	během
kouzla	kouzlo	k1gNnSc2	kouzlo
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
,	,	kIx,	,
přístroj	přístroj	k1gInSc4	přístroj
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
duplikát	duplikát	k1gInSc1	duplikát
–	–	k?	–
první	první	k4xOgFnSc4	první
propadl	propadnout	k5eAaPmAgInS	propadnout
padacími	padací	k2eAgFnPc7d1	padací
dveřmi	dveře	k1gFnPc7	dveře
do	do	k7c2	do
nádrže	nádrž	k1gFnSc2	nádrž
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
utopil	utopit	k5eAaPmAgInS	utopit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
byl	být	k5eAaImAgInS	být
teleportován	teleportovat	k5eAaBmNgInS	teleportovat
na	na	k7c4	na
balkón	balkón	k1gInSc4	balkón
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
nádrže	nádrž	k1gFnPc1	nádrž
uchovávají	uchovávat	k5eAaImIp3nP	uchovávat
jednoho	jeden	k4xCgMnSc4	jeden
Angiera	Angier	k1gMnSc4	Angier
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
se	se	k3xPyFc4	se
Alfred	Alfred	k1gMnSc1	Alfred
Borden	Bordna	k1gFnPc2	Bordna
ohlédne	ohlédnout	k5eAaPmIp3nS	ohlédnout
za	za	k7c7	za
nádržemi	nádrž	k1gFnPc7	nádrž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
jsou	být	k5eAaImIp3nP	být
mrtvé	mrtvý	k2eAgInPc1d1	mrtvý
duplikáty	duplikát	k1gInPc1	duplikát
a	a	k8xC	a
pak	pak	k6eAd1	pak
opouští	opouštět	k5eAaImIp3nS	opouštět
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
Angiera	Angier	k1gMnSc4	Angier
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
začíná	začínat	k5eAaImIp3nS	začínat
ničit	ničit	k5eAaImF	ničit
celou	celý	k2eAgFnSc4d1	celá
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Cutter	Cutter	k1gInSc1	Cutter
shledává	shledávat	k5eAaImIp3nS	shledávat
s	s	k7c7	s
Bordenem	Borden	k1gInSc7	Borden
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
dcerou	dcera	k1gFnSc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Hugh	Hugh	k1gMnSc1	Hugh
Jackman	Jackman	k1gMnSc1	Jackman
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
Angier	Angier	k1gMnSc1	Angier
alias	alias	k9	alias
Velký	velký	k2eAgInSc1d1	velký
Danton	Danton	k1gInSc1	Danton
<g/>
,	,	kIx,	,
kouzelník	kouzelník	k1gMnSc1	kouzelník
z	z	k7c2	z
aristokracie	aristokracie	k1gFnSc2	aristokracie
s	s	k7c7	s
talentem	talent	k1gInSc7	talent
pro	pro	k7c4	pro
vystupování	vystupování	k1gNnSc4	vystupování
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
Jackman	Jackman	k1gMnSc1	Jackman
přečetl	přečíst	k5eAaPmAgMnS	přečíst
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
se	se	k3xPyFc4	se
chopit	chopit	k5eAaPmF	chopit
role	role	k1gFnPc4	role
Roberta	Robert	k1gMnSc2	Robert
Angiera	Angier	k1gMnSc2	Angier
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
přišel	přijít	k5eAaPmAgMnS	přijít
Christopher	Christophra	k1gFnPc2	Christophra
Nolan	Nolan	k1gInSc1	Nolan
<g/>
,	,	kIx,	,
sešel	sejít	k5eAaPmAgMnS	sejít
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jackman	Jackman	k1gMnSc1	Jackman
má	mít	k5eAaImIp3nS	mít
kvality	kvalita	k1gFnPc4	kvalita
šoumenství	šoumenství	k1gNnPc2	šoumenství
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Nolan	Nolan	k1gInSc1	Nolan
hledá	hledat	k5eAaImIp3nS	hledat
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
Angiera	Angiero	k1gNnSc2	Angiero
<g/>
.	.	kIx.	.
</s>
<s>
Nolan	Nolan	k1gMnSc1	Nolan
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angier	Angier	k1gInSc1	Angier
"	"	kIx"	"
<g/>
výborně	výborně	k6eAd1	výborně
rozuměl	rozumět	k5eAaImAgInS	rozumět
interakci	interakce	k1gFnSc4	interakce
mezi	mezi	k7c7	mezi
vystupujícím	vystupující	k2eAgNnSc7d1	vystupující
a	a	k8xC	a
živým	živý	k2eAgNnSc7d1	živé
publikem	publikum	k1gNnSc7	publikum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
kvalita	kvalita	k1gFnSc1	kvalita
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgInS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Jackman	Jackman	k1gMnSc1	Jackman
<g/>
.	.	kIx.	.
</s>
<s>
Nolan	Nolan	k1gMnSc1	Nolan
také	také	k9	také
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jackman	Jackman	k1gMnSc1	Jackman
"	"	kIx"	"
<g/>
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
herecký	herecký	k2eAgInSc4d1	herecký
rozsah	rozsah	k1gInSc4	rozsah
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ještě	ještě	k6eAd1	ještě
nebyl	být	k5eNaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
ještě	ještě	k6eAd1	ještě
neměli	mít	k5eNaImAgMnP	mít
šanci	šance	k1gFnSc4	šance
opravdu	opravdu	k6eAd1	opravdu
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
dokáže	dokázat	k5eAaPmIp3nS	dokázat
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
tohle	tenhle	k3xDgNnSc1	tenhle
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
dovolí	dovolit	k5eAaPmIp3nS	dovolit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jackman	Jackman	k1gMnSc1	Jackman
své	svůj	k3xOyFgNnSc4	svůj
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
Angiera	Angier	k1gMnSc2	Angier
založil	založit	k5eAaPmAgMnS	založit
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kouzelníkovi	kouzelníkův	k2eAgMnPc1d1	kouzelníkův
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
Channingu	Channing	k1gInSc2	Channing
Pollockovi	Pollocek	k1gMnSc3	Pollocek
<g/>
.	.	kIx.	.
</s>
<s>
Christian	Christian	k1gMnSc1	Christian
Bale	bal	k1gInSc5	bal
–	–	k?	–
Alfred	Alfred	k1gMnSc1	Alfred
Borden	Bordna	k1gFnPc2	Bordna
alias	alias	k9	alias
Profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
kouzelník	kouzelník	k1gMnSc1	kouzelník
z	z	k7c2	z
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
třídy	třída	k1gFnSc2	třída
s	s	k7c7	s
porozuměním	porozumění	k1gNnSc7	porozumění
pro	pro	k7c4	pro
magii	magie	k1gFnSc4	magie
<g/>
.	.	kIx.	.
</s>
<s>
Bale	bal	k1gInSc5	bal
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
zájem	zájem	k1gInSc4	zájem
ztvárnit	ztvárnit	k5eAaPmF	ztvárnit
Bordena	Bordeno	k1gNnPc4	Bordeno
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
až	až	k9	až
po	po	k7c6	po
Jackmanovi	Jackman	k1gMnSc6	Jackman
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Nolan	Nolan	k1gInSc1	Nolan
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Balea	Balea	k1gMnSc1	Balea
do	do	k7c2	do
role	role	k1gFnSc2	role
Batmana	Batman	k1gMnSc2	Batman
v	v	k7c6	v
Batman	Batman	k1gMnSc1	Batman
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
Baleovi	Baleus	k1gMnSc3	Baleus
svěřit	svěřit	k5eAaPmF	svěřit
roli	role	k1gFnSc4	role
Bordena	Bordeno	k1gNnSc2	Bordeno
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
sám	sám	k3xTgMnSc1	sám
neozval	ozvat	k5eNaPmAgMnS	ozvat
<g/>
.	.	kIx.	.
</s>
<s>
Nolan	Nolan	k1gMnSc1	Nolan
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bale	bal	k1gInSc5	bal
je	on	k3xPp3gMnPc4	on
tím	ten	k3xDgNnSc7	ten
pravým	pravý	k2eAgMnSc7d1	pravý
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
Bordena	Bordeno	k1gNnSc2	Bordeno
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
nemyslitelné	myslitelný	k2eNgNnSc4d1	nemyslitelné
<g/>
"	"	kIx"	"
dát	dát	k5eAaPmF	dát
roli	role	k1gFnSc4	role
někomu	někdo	k3yInSc3	někdo
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Nolan	Nolan	k1gInSc1	Nolan
o	o	k7c4	o
Baleovi	Baleus	k1gMnSc3	Baleus
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
skvělé	skvělý	k2eAgNnSc1d1	skvělé
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
a	a	k8xC	a
že	že	k8xS	že
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
velmi	velmi	k6eAd1	velmi
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vážně	vážně	k6eAd1	vážně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
doporučil	doporučit	k5eAaPmAgMnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
herci	herec	k1gMnPc1	herec
nečetli	číst	k5eNaImAgMnP	číst
původní	původní	k2eAgFnSc4d1	původní
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bale	bal	k1gInSc5	bal
jeho	jeho	k3xOp3gMnSc1	jeho
radu	rada	k1gFnSc4	rada
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Caine	Cain	k1gInSc5	Cain
–	–	k?	–
John	John	k1gMnSc1	John
Cutter	Cutter	k1gMnSc1	Cutter
<g/>
,	,	kIx,	,
jevištní	jevištní	k2eAgMnSc1d1	jevištní
inženýr	inženýr	k1gMnSc1	inženýr
pracující	pracující	k2eAgMnSc1d1	pracující
pro	pro	k7c4	pro
Angiera	Angiero	k1gNnPc4	Angiero
<g/>
.	.	kIx.	.
</s>
<s>
Caine	Cainout	k5eAaPmIp3nS	Cainout
již	již	k6eAd1	již
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Nolanem	Nolan	k1gInSc7	Nolan
a	a	k8xC	a
Balem	bal	k1gInSc7	bal
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
Batman	Batman	k1gMnSc1	Batman
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
role	role	k1gFnSc2	role
sluhy	sluha	k1gMnSc2	sluha
rodiny	rodina	k1gFnSc2	rodina
Wayneových	Wayneův	k2eAgMnPc2d1	Wayneův
Alfreda	Alfred	k1gMnSc2	Alfred
Pennywortha	Pennyworth	k1gMnSc2	Pennyworth
<g/>
.	.	kIx.	.
</s>
<s>
Nolan	Nolan	k1gMnSc1	Nolan
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
role	role	k1gFnSc1	role
Cuttera	Cutter	k1gMnSc2	Cutter
nebyla	být	k5eNaImAgFnS	být
původně	původně	k6eAd1	původně
napsaná	napsaný	k2eAgFnSc1d1	napsaná
pro	pro	k7c4	pro
Cainea	Caineus	k1gMnSc4	Caineus
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
postava	postava	k1gFnSc1	postava
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
Nolan	Nolan	k1gInSc1	Nolan
Cainea	Caine	k1gInSc2	Caine
potkal	potkat	k5eAaPmAgInS	potkat
<g/>
.	.	kIx.	.
</s>
<s>
Caine	Cainout	k5eAaImIp3nS	Cainout
popisuje	popisovat	k5eAaImIp3nS	popisovat
Cuttera	Cutter	k1gMnSc4	Cutter
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Angierova	Angierův	k2eAgMnSc4d1	Angierův
učitele	učitel	k1gMnSc4	učitel
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
rádce	rádce	k1gMnSc4	rádce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Cuttera	Cutter	k1gMnSc4	Cutter
dobře	dobře	k6eAd1	dobře
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgMnS	změnit
Caine	Cain	k1gInSc5	Cain
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc1	hlas
a	a	k8xC	a
držení	držení	k1gNnSc1	držení
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nolan	Nolan	k1gMnSc1	Nolan
později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Postava	postava	k1gFnSc1	postava
Michaela	Michael	k1gMnSc2	Michael
Caine	Cain	k1gInSc5	Cain
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
srdcem	srdce	k1gNnSc7	srdce
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vřelý	vřelý	k2eAgInSc1d1	vřelý
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
emoce	emoce	k1gFnSc1	emoce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vás	vy	k3xPp2nPc4	vy
vtáhne	vtáhnout	k5eAaPmIp3nS	vtáhnout
do	do	k7c2	do
děje	děj	k1gInSc2	děj
a	a	k8xC	a
dovolí	dovolit	k5eAaPmIp3nP	dovolit
vám	vy	k3xPp2nPc3	vy
postavy	postava	k1gFnPc1	postava
nesoudit	soudit	k5eNaImF	soudit
příliš	příliš	k6eAd1	příliš
přísně	přísně	k6eAd1	přísně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Scarlett	Scarlett	k2eAgMnSc1d1	Scarlett
Johansson	Johansson	k1gMnSc1	Johansson
–	–	k?	–
Olivia	Olivia	k1gFnSc1	Olivia
Wenscombeová	Wenscombeová	k1gFnSc1	Wenscombeová
<g/>
,	,	kIx,	,
Angierova	Angierův	k2eAgFnSc1d1	Angierův
asistentka	asistentka	k1gFnSc1	asistentka
a	a	k8xC	a
Bordenova	Bordenův	k2eAgFnSc1d1	Bordenův
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Nolan	Nolan	k1gMnSc1	Nolan
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
moc	moc	k6eAd1	moc
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Johansson	Johansson	k1gMnSc1	Johansson
ujala	ujmout	k5eAaPmAgFnS	ujmout
role	role	k1gFnSc1	role
Olivie	Olivie	k1gFnSc1	Olivie
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
,	,	kIx,	,
roli	role	k1gFnSc4	role
si	se	k3xPyFc3	se
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
chválila	chválit	k5eAaImAgFnS	chválit
Nolanovy	Nolanův	k2eAgFnPc4d1	Nolanova
režisérské	režisérský	k2eAgFnPc4d1	režisérská
metody	metoda	k1gFnPc4	metoda
a	a	k8xC	a
ráda	rád	k2eAgFnSc1d1	ráda
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
pracovala	pracovat	k5eAaImAgFnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
–	–	k?	–
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
<g/>
,	,	kIx,	,
skutečný	skutečný	k2eAgMnSc1d1	skutečný
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
stroj	stroj	k1gInSc4	stroj
pro	pro	k7c4	pro
Angiera	Angier	k1gMnSc4	Angier
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
chtěl	chtít	k5eAaImAgMnS	chtít
režisér	režisér	k1gMnSc1	režisér
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
filmovou	filmový	k2eAgFnSc7d1	filmová
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
mimořádně	mimořádně	k6eAd1	mimořádně
charismatický	charismatický	k2eAgMnSc1d1	charismatický
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
byl	být	k5eAaImAgMnS	být
opravdu	opravdu	k6eAd1	opravdu
jediný	jediný	k2eAgMnSc1d1	jediný
chlap	chlap	k1gMnSc1	chlap
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgNnSc6	který
jsem	být	k5eAaImIp1nS	být
přemýšlel	přemýšlet	k5eAaImAgMnS	přemýšlet
kvůli	kvůli	k7c3	kvůli
roli	role	k1gFnSc3	role
Tesly	Tesla	k1gFnSc2	Tesla
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
řekl	říct	k5eAaPmAgMnS	říct
Nolan	Nolan	k1gMnSc1	Nolan
<g/>
.	.	kIx.	.
</s>
<s>
Nolan	Nolan	k1gMnSc1	Nolan
se	se	k3xPyFc4	se
Bowiemu	Bowiema	k1gFnSc4	Bowiema
ozval	ozvat	k5eAaPmAgMnS	ozvat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
nejdříve	dříve	k6eAd3	dříve
roli	role	k1gFnSc4	role
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
celoživotní	celoživotní	k2eAgMnSc1d1	celoživotní
fanoušek	fanoušek	k1gMnSc1	fanoušek
Bowieho	Bowie	k1gMnSc2	Bowie
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
Nolan	Nolan	k1gMnSc1	Nolan
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zpěváka	zpěvák	k1gMnSc2	zpěvák
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c6	o
ztvárnění	ztvárnění	k1gNnSc6	ztvárnění
role	role	k1gFnSc2	role
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgInSc1d1	jiný
postavu	postava	k1gFnSc4	postava
nemůže	moct	k5eNaImIp3nS	moct
zahrát	zahrát	k5eAaPmF	zahrát
<g/>
.	.	kIx.	.
</s>
<s>
Bowie	Bowie	k1gFnSc1	Bowie
po	po	k7c6	po
několika	několik	k4yIc6	několik
minutách	minuta	k1gFnPc6	minuta
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Piper	Piprat	k5eAaPmRp2nS	Piprat
Perabo	Peraba	k1gMnSc5	Peraba
–	–	k?	–
Julia	Julius	k1gMnSc2	Julius
McCulloughová	McCulloughový	k2eAgFnSc1d1	McCulloughová
<g/>
,	,	kIx,	,
Angierova	Angierův	k2eAgFnSc1d1	Angierův
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc1	Anda
Serkis	Serkis	k1gFnSc2	Serkis
–	–	k?	–
pan	pan	k1gMnSc1	pan
Alley	Allea	k1gFnSc2	Allea
<g/>
,	,	kIx,	,
Teslův	Teslův	k2eAgMnSc1d1	Teslův
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Serkis	Serkis	k1gFnSc4	Serkis
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrál	hrát	k5eAaImAgMnS	hrát
postavu	postava	k1gFnSc4	postava
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jednou	jednou	k6eAd1	jednou
byl	být	k5eAaImAgMnS	být
společenský	společenský	k2eAgMnSc1d1	společenský
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
nadšen	nadchnout	k5eAaPmNgMnS	nadchnout
nekonformním	konformní	k2eNgMnSc7d1	nekonformní
Teslou	Tesla	k1gMnSc7	Tesla
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vyskočil	vyskočit	k5eAaPmAgMnS	vyskočit
z	z	k7c2	z
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Serkis	Serkis	k1gInSc1	Serkis
popisuje	popisovat	k5eAaImIp3nS	popisovat
svou	svůj	k3xOyFgFnSc4	svůj
postavu	postava	k1gFnSc4	postava
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
strážného	strážný	k2eAgNnSc2d1	strážné
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
podvodníka	podvodník	k1gMnSc4	podvodník
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
zrcadlový	zrcadlový	k2eAgInSc1d1	zrcadlový
obraz	obraz	k1gInSc1	obraz
postavy	postava	k1gFnSc2	postava
Michaela	Michael	k1gMnSc2	Michael
Cainea	Caineus	k1gMnSc2	Caineus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Serkis	Serkis	k1gInSc1	Serkis
<g/>
,	,	kIx,	,
velký	velký	k2eAgMnSc1d1	velký
fanoušek	fanoušek	k1gMnSc1	fanoušek
Bowieho	Bowie	k1gMnSc2	Bowie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nadšen	nadchnout	k5eAaPmNgMnS	nadchnout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Rebecca	Rebecca	k1gMnSc1	Rebecca
Hall	Hall	k1gMnSc1	Hall
–	–	k?	–
Sarah	Sarah	k1gFnSc1	Sarah
Bordenová	Bordenová	k1gFnSc1	Bordenová
<g/>
,	,	kIx,	,
Bordenova	Bordenův	k2eAgFnSc1d1	Bordenův
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
natáčení	natáčení	k1gNnSc3	natáčení
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
Hall	Hallum	k1gNnPc2	Hallum
musela	muset	k5eAaImAgFnS	muset
přemístit	přemístit	k5eAaPmF	přemístit
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
samotný	samotný	k2eAgInSc1d1	samotný
film	film	k1gInSc1	film
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Hall	Hall	k1gInSc4	Hall
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
nadšená	nadšený	k2eAgFnSc1d1	nadšená
jen	jen	k9	jen
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
na	na	k7c6	na
filmu	film	k1gInSc6	film
podílet	podílet	k5eAaImF	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Ricky	Ricky	k6eAd1	Ricky
Jay	Jay	k1gMnSc1	Jay
–	–	k?	–
Milton	Milton	k1gInSc1	Milton
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něhož	jenž	k3xRgMnSc4	jenž
Borden	Bordno	k1gNnPc2	Bordno
a	a	k8xC	a
Angier	Angira	k1gFnPc2	Angira
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
příběhu	příběh	k1gInSc2	příběh
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Jay	Jay	k?	Jay
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
americký	americký	k2eAgMnSc1d1	americký
kouzelník	kouzelník	k1gMnSc1	kouzelník
<g/>
,	,	kIx,	,
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Weber	Weber	k1gMnSc1	Weber
trénovali	trénovat	k5eAaImAgMnP	trénovat
Jackmana	Jackman	k1gMnSc4	Jackman
a	a	k8xC	a
Bale	bal	k1gInSc5	bal
a	a	k8xC	a
stručně	stručně	k6eAd1	stručně
je	on	k3xPp3gNnSc4	on
instruovali	instruovat	k5eAaBmAgMnP	instruovat
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
jevištních	jevištní	k2eAgFnPc6d1	jevištní
iluzích	iluze	k1gFnPc6	iluze
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
na	na	k7c4	na
Christophera	Christopher	k1gMnSc4	Christopher
Priesta	Priest	k1gMnSc4	Priest
kvůli	kvůli	k7c3	kvůli
adaptaci	adaptace	k1gFnSc6	adaptace
jeho	on	k3xPp3gInSc2	on
románu	román	k1gInSc2	román
Nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
efekt	efekt	k1gInSc4	efekt
obrátili	obrátit	k5eAaPmAgMnP	obrátit
producenti	producent	k1gMnPc1	producent
režisérů	režisér	k1gMnPc2	režisér
Juliana	Julian	k1gMnSc2	Julian
Jarrolda	Jarrold	k1gMnSc2	Jarrold
a	a	k8xC	a
Sama	sám	k3xTgFnSc1	sám
Mendese	Mendese	k1gFnSc1	Mendese
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Priesta	Priest	k1gMnSc4	Priest
zapůsobily	zapůsobit	k5eAaPmAgInP	zapůsobit
Nolanovy	Nolanův	k2eAgInPc1d1	Nolanův
filmy	film	k1gInPc1	film
Sledování	sledování	k1gNnSc2	sledování
a	a	k8xC	a
Memento	memento	k1gNnSc4	memento
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
producentka	producentka	k1gFnSc1	producentka
Valerie	Valerie	k1gFnSc2	Valerie
Dean	Deana	k1gFnPc2	Deana
získala	získat	k5eAaPmAgFnS	získat
pozornost	pozornost	k1gFnSc1	pozornost
Christophera	Christopher	k1gMnSc2	Christopher
Nolana	Nolan	k1gMnSc2	Nolan
pro	pro	k7c4	pro
Priestovu	Priestův	k2eAgFnSc4d1	Priestův
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2000	[number]	k4	2000
se	se	k3xPyFc4	se
Nolan	Nolan	k1gMnSc1	Nolan
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
propagoval	propagovat	k5eAaImAgMnS	propagovat
své	svůj	k3xOyFgNnSc4	svůj
Memento	memento	k1gNnSc4	memento
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
přečetl	přečíst	k5eAaPmAgMnS	přečíst
Priestovu	Priestův	k2eAgFnSc4d1	Priestův
knihu	kniha	k1gFnSc4	kniha
a	a	k8xC	a
během	během	k7c2	během
procházky	procházka	k1gFnSc2	procházka
v	v	k7c6	v
Highgate	Highgat	k1gInSc5	Highgat
(	(	kIx(	(
<g/>
lokaci	lokace	k1gFnSc4	lokace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
použita	použít	k5eAaPmNgFnS	použít
ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Angier	Angier	k1gMnSc1	Angier
vykupuje	vykupovat	k5eAaImIp3nS	vykupovat
Bordenova	Bordenův	k2eAgMnSc4d1	Bordenův
inženýra	inženýr	k1gMnSc4	inženýr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
podělil	podělit	k5eAaPmAgMnS	podělit
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
Dokonalý	dokonalý	k2eAgInSc4d1	dokonalý
trik	trik	k1gInSc4	trik
napsal	napsat	k5eAaPmAgMnS	napsat
anglický	anglický	k2eAgMnSc1d1	anglický
hudebník	hudebník	k1gMnSc1	hudebník
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
David	David	k1gMnSc1	David
Julyan	Julyan	k1gMnSc1	Julyan
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
Nolan	Nolan	k1gInSc1	Nolan
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
již	již	k6eAd1	již
na	na	k7c6	na
filmech	film	k1gInPc6	film
Memento	memento	k1gNnSc1	memento
a	a	k8xC	a
Insomnie	insomnie	k1gFnSc1	insomnie
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ve	v	k7c6	v
filmu	film	k1gInSc6	film
byl	být	k5eAaImAgInS	být
soundtrack	soundtrack	k1gInSc1	soundtrack
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
–	–	k?	–
Nabídka	nabídka	k1gFnSc1	nabídka
(	(	kIx(	(
<g/>
Pledge	Pledge	k1gInSc1	Pledge
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Změna	změna	k1gFnSc1	změna
(	(	kIx(	(
<g/>
Turn	Turn	k1gInSc1	Turn
<g/>
)	)	kIx)	)
a	a	k8xC	a
Prestiž	prestiž	k1gFnSc1	prestiž
(	(	kIx(	(
<g/>
Prestige	Prestige	k1gInSc1	Prestige
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritikové	kritik	k1gMnPc1	kritik
byli	být	k5eAaImAgMnP	být
hudbou	hudba	k1gFnSc7	hudba
filmu	film	k1gInSc2	film
zklamaní	zklamaný	k2eAgMnPc1d1	zklamaný
<g/>
.	.	kIx.	.
</s>
<s>
Uznávají	uznávat	k5eAaImIp3nP	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
seděla	sedět	k5eAaImAgFnS	sedět
ke	k	k7c3	k
kontextu	kontext	k1gInSc3	kontext
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
příjemná	příjemný	k2eAgFnSc1d1	příjemná
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
trik	trik	k1gInSc1	trik
vynesl	vynést	k5eAaPmAgInS	vynést
jen	jen	k9	jen
během	během	k7c2	během
úvodního	úvodní	k2eAgInSc2d1	úvodní
víkendu	víkend	k1gInSc2	víkend
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
14	[number]	k4	14
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
udělalo	udělat	k5eAaPmAgNnS	udělat
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
film	film	k1gInSc4	film
víkendu	víkend	k1gInSc2	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
celkově	celkově	k6eAd1	celkově
film	film	k1gInSc1	film
utržil	utržit	k5eAaPmAgInS	utržit
53	[number]	k4	53
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
více	hodně	k6eAd2	hodně
než	než	k8xS	než
109	[number]	k4	109
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
získal	získat	k5eAaPmAgInS	získat
dvě	dva	k4xCgFnPc4	dva
nominace	nominace	k1gFnPc1	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
výprava	výprava	k1gFnSc1	výprava
a	a	k8xC	a
dekorace	dekorace	k1gFnSc1	dekorace
a	a	k8xC	a
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
převážně	převážně	k6eAd1	převážně
příznivé	příznivý	k2eAgNnSc4d1	příznivé
hodnocení	hodnocení	k1gNnSc4	hodnocení
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Rotten	Rotten	k2eAgInSc4d1	Rotten
Tomatoes	Tomatoes	k1gInSc4	Tomatoes
na	na	k7c6	na
základě	základ	k1gInSc6	základ
187	[number]	k4	187
recenzí	recenze	k1gFnPc2	recenze
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
75	[number]	k4	75
%	%	kIx~	%
recenzí	recenze	k1gFnSc7	recenze
filmu	film	k1gInSc2	film
bylo	být	k5eAaImAgNnS	být
kladných	kladný	k2eAgNnPc2d1	kladné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Metacritic	Metacritice	k1gFnPc2	Metacritice
získal	získat	k5eAaPmAgInS	získat
Dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
trik	trik	k1gInSc1	trik
na	na	k7c6	na
základě	základ	k1gInSc6	základ
36	[number]	k4	36
recenzí	recenze	k1gFnSc7	recenze
skóre	skóre	k1gNnSc2	skóre
66	[number]	k4	66
ze	z	k7c2	z
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
Rotten	Rottno	k1gNnPc2	Rottno
Tomatoes	Tomatoesa	k1gFnPc2	Tomatoesa
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
snímek	snímek	k1gInSc1	snímek
90	[number]	k4	90
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
uživatelé	uživatel	k1gMnPc1	uživatel
Metacritic	Metacritice	k1gFnPc2	Metacritice
mu	on	k3xPp3gNnSc3	on
udělují	udělovat	k5eAaImIp3nP	udělovat
skóre	skóre	k1gNnSc4	skóre
8,6	[number]	k4	8,6
z	z	k7c2	z
10	[number]	k4	10
a	a	k8xC	a
u	u	k7c2	u
uživatelů	uživatel	k1gMnPc2	uživatel
ČSFD	ČSFD	kA	ČSFD
film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
88	[number]	k4	88
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
činí	činit	k5eAaImIp3nS	činit
75	[number]	k4	75
<g/>
.	.	kIx.	.
nejúspěšnější	úspěšný	k2eAgInSc1d3	nejúspěšnější
film	film	k1gInSc1	film
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
ČSFD	ČSFD	kA	ČSFD
<g/>
.	.	kIx.	.
</s>
