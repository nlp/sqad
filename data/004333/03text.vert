<s>
Sestřičky	sestřička	k1gFnPc4	sestřička
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
režisérem	režisér	k1gMnSc7	režisér
Karlem	Karel	k1gMnSc7	Karel
Kachyňou	Kachyňa	k1gMnSc7	Kachyňa
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
popisuje	popisovat	k5eAaImIp3nS	popisovat
těžký	těžký	k2eAgInSc4d1	těžký
život	život	k1gInSc4	život
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
sester	sestra	k1gFnPc2	sestra
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
musejí	muset	k5eAaImIp3nP	muset
objíždět	objíždět	k5eAaImF	objíždět
nemocné	nemocný	k1gMnPc4	nemocný
a	a	k8xC	a
dostanou	dostat	k5eAaPmIp3nP	dostat
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
zásadních	zásadní	k2eAgFnPc2d1	zásadní
životních	životní	k2eAgFnPc2d1	životní
situací	situace	k1gFnPc2	situace
svých	svůj	k3xOyFgMnPc2	svůj
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
sestra	sestra	k1gFnSc1	sestra
(	(	kIx(	(
<g/>
Jiřina	Jiřina	k1gFnSc1	Jiřina
Jirásková	Jirásková	k1gFnSc1	Jirásková
<g/>
)	)	kIx)	)
bere	brát	k5eAaImIp3nS	brát
věci	věc	k1gFnPc4	věc
s	s	k7c7	s
nadhledem	nadhled	k1gInSc7	nadhled
a	a	k8xC	a
předává	předávat	k5eAaImIp3nS	předávat
své	svůj	k3xOyFgFnSc2	svůj
bohaté	bohatý	k2eAgFnSc2d1	bohatá
zkušenosti	zkušenost	k1gFnSc2	zkušenost
mladší	mladý	k2eAgFnSc1d2	mladší
(	(	kIx(	(
<g/>
Alena	Alena	k1gFnSc1	Alena
Mihulová	Mihulový	k2eAgFnSc1d1	Mihulová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
<g/>
:	:	kIx,	:
Adolf	Adolf	k1gMnSc1	Adolf
Branald	Branald	k1gMnSc1	Branald
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Kachyňa	Kachyňa	k1gMnSc1	Kachyňa
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Luboš	Luboš	k1gMnSc1	Luboš
Fišer	Fišer	k1gMnSc1	Fišer
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Čuřík	Čuřík	k1gMnSc1	Čuřík
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Kachyňa	Kachyňa	k1gMnSc1	Kachyňa
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Jiřina	Jiřina	k1gFnSc1	Jiřina
Jirásková	Jirásková	k1gFnSc1	Jirásková
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
Mihulová	Mihulový	k2eAgFnSc1d1	Mihulová
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Husák	Husák	k1gMnSc1	Husák
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Vízner	Vízner	k1gMnSc1	Vízner
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
Vetchý	vetchý	k2eAgMnSc1d1	vetchý
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Růžička	Růžička	k1gMnSc1	Růžička
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Lackovič	Lackovič	k1gMnSc1	Lackovič
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Žák	Žák	k1gMnSc1	Žák
Další	další	k2eAgInPc4d1	další
údaje	údaj	k1gInPc4	údaj
<g/>
:	:	kIx,	:
barevný	barevný	k2eAgInSc4d1	barevný
<g/>
,	,	kIx,	,
85	[number]	k4	85
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
hořká	hořký	k2eAgFnSc1d1	hořká
komedie	komedie	k1gFnSc1	komedie
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Sestřičky	sestřička	k1gFnSc2	sestřička
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Sestřičky	sestřička	k1gFnSc2	sestřička
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
