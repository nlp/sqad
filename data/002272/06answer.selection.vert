<s>
Polsko	Polsko	k1gNnSc1	Polsko
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
členskou	členský	k2eAgFnSc7d1	členská
zemí	zem	k1gFnSc7	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
(	(	kIx(	(
<g/>
NATO	NATO	kA	NATO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
OECD	OECD	kA	OECD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
a	a	k8xC	a
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
