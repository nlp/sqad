<s>
Problém	problém	k1gInSc1	problém
zastavení	zastavení	k1gNnSc1	zastavení
(	(	kIx(	(
<g/>
halting	halting	k1gInSc1	halting
problem	probl	k1gInSc7	probl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
úloha	úloha	k1gFnSc1	úloha
teorie	teorie	k1gFnSc2	teorie
vyčíslitelnosti	vyčíslitelnost	k1gFnSc2	vyčíslitelnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
neformálně	formálně	k6eNd1	formálně
zadána	zadat	k5eAaPmNgNnP	zadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Znáte	znát	k5eAaImIp2nP	znát
<g/>
-li	i	k?	-li
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
programu	program	k1gInSc2	program
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
vstup	vstup	k1gInSc4	vstup
<g/>
,	,	kIx,	,
rozhodněte	rozhodnout	k5eAaPmRp2nP	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
program	program	k1gInSc1	program
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
poběží	poběžet	k5eAaPmIp3nS	poběžet
navždy	navždy	k6eAd1	navždy
bez	bez	k7c2	bez
zastavení	zastavení	k1gNnPc2	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
Alan	alan	k1gInSc1	alan
Turing	Turing	k1gInSc4	Turing
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obecný	obecný	k2eAgInSc1d1	obecný
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
řešil	řešit	k5eAaImAgInS	řešit
problém	problém	k1gInSc1	problém
zastavení	zastavení	k1gNnSc1	zastavení
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
vstupy	vstup	k1gInPc4	vstup
všech	všecek	k3xTgInPc2	všecek
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
zastavení	zastavení	k1gNnSc2	zastavení
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
algoritmicky	algoritmicky	k6eAd1	algoritmicky
nerozhodnutelný	rozhodnutelný	k2eNgInSc1d1	nerozhodnutelný
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nerozhodnutelnost	Nerozhodnutelnost	k1gFnSc1	Nerozhodnutelnost
problému	problém	k1gInSc2	problém
zastavení	zastavení	k1gNnSc2	zastavení
lze	lze	k6eAd1	lze
dokázat	dokázat	k5eAaPmF	dokázat
sporem	spor	k1gInSc7	spor
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInSc1d1	počáteční
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
:	:	kIx,	:
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
problém	problém	k1gInSc1	problém
zastavení	zastavení	k1gNnSc4	zastavení
lze	lze	k6eAd1	lze
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
nějaký	nějaký	k3yIgInSc1	nějaký
program	program	k1gInSc1	program
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc1	vstup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
univerzálním	univerzální	k2eAgNnSc7d1	univerzální
řešením	řešení	k1gNnSc7	řešení
problému	problém	k1gInSc2	problém
zastavení	zastavení	k1gNnSc4	zastavení
–	–	k?	–
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
tomuto	tento	k3xDgInSc3	tento
programu	program	k1gInSc3	program
předáme	předat	k5eAaPmIp1nP	předat
libovolný	libovolný	k2eAgInSc4d1	libovolný
program	program	k1gInSc4	program
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
vstup	vstup	k1gInSc4	vstup
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
program	program	k1gInSc1	program
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
čase	čas	k1gInSc6	čas
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g />
.	.	kIx.	.
</s>
<s>
odpověď	odpověď	k1gFnSc4	odpověď
takovou	takový	k3xDgFnSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
volání	volání	k1gNnSc1	volání
program	program	k1gInSc1	program
<g/>
(	(	kIx(	(
<g/>
vstup	vstup	k1gInSc1	vstup
<g/>
)	)	kIx)	)
po	po	k7c6	po
konečném	konečný	k2eAgInSc6d1	konečný
počtu	počet	k1gInSc6	počet
kroků	krok	k1gInPc2	krok
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc1	vstup
<g/>
)	)	kIx)	)
vrátí	vrátit	k5eAaPmIp3nS	vrátit
hodnotu	hodnota	k1gFnSc4	hodnota
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
volání	volání	k1gNnSc1	volání
program	program	k1gInSc1	program
<g/>
(	(	kIx(	(
<g/>
vstup	vstup	k1gInSc1	vstup
<g/>
)	)	kIx)	)
zacyklilo	zacyklit	k5eAaPmAgNnS	zacyklit
<g/>
)	)	kIx)	)
vrátí	vrátit	k5eAaPmIp3nS	vrátit
hodnotu	hodnota	k1gFnSc4	hodnota
NE	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
zkonstruujme	zkonstruovat	k5eAaPmRp1nP	zkonstruovat
program	program	k1gInSc4	program
Paradox	paradox	k1gInSc1	paradox
<g/>
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zavolá	zavolat	k5eAaPmIp3nS	zavolat
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokud	pokud	k8xS	pokud
toto	tento	k3xDgNnSc1	tento
volání	volání	k1gNnSc1	volání
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
záměrně	záměrně	k6eAd1	záměrně
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
NE	ne	k9	ne
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
ihned	ihned	k6eAd1	ihned
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
ptejme	ptat	k5eAaImRp1nP	ptat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
volání	volání	k1gNnSc2	volání
Paradox	paradox	k1gInSc1	paradox
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
vrací	vracet	k5eAaImIp3nS	vracet
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
programu	program	k1gInSc2	program
Paradox	paradox	k1gInSc1	paradox
pak	pak	k6eAd1	pak
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Paradox	paradox	k1gInSc1	paradox
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
programu	program	k1gInSc2	program
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
ale	ale	k8xC	ale
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Paradox	paradox	k1gInSc1	paradox
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
zacyklí	zacyklit	k5eAaPmIp3nS	zacyklit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nP	muset
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
vracet	vracet	k5eAaImF	vracet
NE	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Došli	dojít	k5eAaPmAgMnP	dojít
jsme	být	k5eAaImIp1nP	být
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
že	že	k8xS	že
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
vrací	vracet	k5eAaImIp3nS	vracet
NE	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
programu	program	k1gInSc2	program
Paradox	paradox	k1gInSc1	paradox
pak	pak	k6eAd1	pak
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Paradox	paradox	k1gInSc1	paradox
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
definice	definice	k1gFnSc2	definice
programu	program	k1gInSc2	program
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
ale	ale	k8xC	ale
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Paradox	paradox	k1gInSc1	paradox
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nP	muset
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
(	(	kIx(	(
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
,	,	kIx,	,
Paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
vracet	vracet	k5eAaImF	vracet
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
jsme	být	k5eAaImIp1nP	být
došli	dojít	k5eAaPmAgMnP	dojít
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsme	být	k5eAaImIp1nP	být
vyčerpali	vyčerpat	k5eAaPmAgMnP	vyčerpat
všechny	všechen	k3xTgFnPc4	všechen
možnosti	možnost	k1gFnPc4	možnost
a	a	k8xC	a
vždy	vždy	k6eAd1	vždy
došli	dojít	k5eAaPmAgMnP	dojít
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nepravdivý	pravdivý	k2eNgInSc1d1	nepravdivý
počáteční	počáteční	k2eAgInSc1d1	počáteční
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
program	program	k1gInSc1	program
Zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
vstup	vstup	k1gInSc1	vstup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xS	jak
byl	být	k5eAaImAgInS	být
definován	definovat	k5eAaBmNgInS	definovat
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
důkazu	důkaz	k1gInSc2	důkaz
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
diagonalizace	diagonalizace	k1gFnSc1	diagonalizace
a	a	k8xC	a
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
důkazu	důkaz	k1gInSc6	důkaz
jej	on	k3xPp3gNnSc2	on
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
Gödelovy	Gödelův	k2eAgFnPc1d1	Gödelova
věty	věta	k1gFnPc1	věta
o	o	k7c6	o
neúplnosti	neúplnost	k1gFnSc6	neúplnost
z	z	k7c2	z
matematické	matematický	k2eAgFnSc2d1	matematická
logiky	logika	k1gFnSc2	logika
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
za	za	k7c2	za
jistých	jistý	k2eAgInPc2d1	jistý
předpokladů	předpoklad	k1gInPc2	předpoklad
žádná	žádný	k3yNgFnSc1	žádný
teorie	teorie	k1gFnSc1	teorie
prvního	první	k4xOgInSc2	první
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
aritmetika	aritmetika	k1gFnSc1	aritmetika
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
dokázat	dokázat	k5eAaPmF	dokázat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
bezespornost	bezespornost	k1gFnSc4	bezespornost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Cantorova	Cantorův	k2eAgFnSc1d1	Cantorova
věta	věta	k1gFnSc1	věta
z	z	k7c2	z
teorie	teorie	k1gFnSc2	teorie
množin	množina	k1gFnPc2	množina
z	z	k7c2	z
matematické	matematický	k2eAgFnSc2d1	matematická
logiky	logika	k1gFnSc2	logika
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
žádnou	žádný	k3yNgFnSc4	žádný
množinu	množina	k1gFnSc4	množina
nelze	lze	k6eNd1	lze
bijektivně	bijektivně	k6eAd1	bijektivně
zobrazit	zobrazit	k5eAaPmF	zobrazit
na	na	k7c4	na
systém	systém	k1gInSc4	systém
jejích	její	k3xOp3gFnPc2	její
podmnožin	podmnožina	k1gFnPc2	podmnožina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
nespočetná	spočetný	k2eNgFnSc1d1	nespočetná
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
Cantorovy	Cantorův	k2eAgFnSc2d1	Cantorova
věty	věta	k1gFnSc2	věta
<g/>
)	)	kIx)	)
Co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nenaprogramovatelné	naprogramovatelný	k2eNgNnSc1d1	naprogramovatelný
<g/>
?	?	kIx.	?
</s>
<s>
na	na	k7c6	na
serveru	server	k1gInSc6	server
Root	Roota	k1gFnPc2	Roota
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
