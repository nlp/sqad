<s>
Batman	Batman	k1gMnSc1	Batman
je	být	k5eAaImIp3nS	být
komiksová	komiksový	k2eAgFnSc1d1	komiksová
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
stvořená	stvořený	k2eAgFnSc1d1	stvořená
Bobem	Bob	k1gMnSc7	Bob
Kanem	Kanem	k?	Kanem
a	a	k8xC	a
Billem	Bill	k1gMnSc7	Bill
Fingerem	Finger	k1gMnSc7	Finger
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Batmana	Batman	k1gMnSc2	Batman
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
27	[number]	k4	27
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
superhrdina	superhrdina	k1gFnSc1	superhrdina
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
komiksech	komiks	k1gInPc6	komiks
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnSc1d1	původní
Batman	Batman	k1gMnSc1	Batman
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
pouze	pouze	k6eAd1	pouze
Bat-Man	Bat-Man	k1gMnSc1	Bat-Man
<g/>
,	,	kIx,	,
než	než	k8xS	než
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
netopýřímu	netopýří	k2eAgInSc3d1	netopýří
muži	muž	k1gMnPc7	muž
se	se	k3xPyFc4	se
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Caped	Caped	k1gMnSc1	Caped
Crusader	Crusader	k1gMnSc1	Crusader
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Darknight	Darknight	k1gMnSc1	Darknight
Detective	Detectiv	k1gInSc5	Detectiv
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
World	World	k1gMnSc1	World
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Greatest	Greatest	k1gInSc1	Greatest
Detective	Detectiv	k1gInSc5	Detectiv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
populární	populární	k2eAgMnSc1d1	populární
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
uvedení	uvedení	k1gNnSc6	uvedení
v	v	k7c6	v
komiksové	komiksový	k2eAgFnSc6d1	komiksová
sérii	série	k1gFnSc6	série
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
27	[number]	k4	27
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Case	Cas	k1gFnSc2	Cas
of	of	k?	of
the	the	k?	the
Chemical	Chemical	k1gMnSc5	Chemical
Syndicate	Syndicat	k1gMnSc5	Syndicat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
několika	několik	k4yIc6	několik
prvních	první	k4xOgNnPc6	první
číslech	číslo	k1gNnPc6	číslo
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Batman	Batman	k1gMnSc1	Batman
své	svůj	k3xOyFgMnPc4	svůj
protivníky	protivník	k1gMnPc4	protivník
nikdy	nikdy	k6eAd1	nikdy
nezabíjí	zabíjet	k5eNaImIp3nS	zabíjet
(	(	kIx(	(
<g/>
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
komiksu	komiks	k1gInSc2	komiks
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
881	[number]	k4	881
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
hlavní	hlavní	k2eAgFnSc7d1	hlavní
sérií	série	k1gFnSc7	série
byla	být	k5eAaImAgFnS	být
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1940	[number]	k4	1940
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
také	také	k9	také
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
713	[number]	k4	713
<g/>
.	.	kIx.	.
</s>
<s>
Batmanovy	Batmanův	k2eAgInPc1d1	Batmanův
příběhy	příběh	k1gInPc1	příběh
byly	být	k5eAaImAgInP	být
dále	daleko	k6eAd2	daleko
vydávány	vydávat	k5eAaImNgInP	vydávat
v	v	k7c6	v
komiksu	komiks	k1gInSc6	komiks
World	Worlda	k1gFnPc2	Worlda
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Finest	Finest	k1gInSc1	Finest
Comics	comics	k1gInSc1	comics
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
první	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1940	[number]	k4	1940
a	a	k8xC	a
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1986	[number]	k4	1986
číslem	číslo	k1gNnSc7	číslo
323	[number]	k4	323
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
série	série	k1gFnSc1	série
The	The	k1gFnSc2	The
Brave	brav	k1gInSc5	brav
and	and	k?	and
the	the	k?	the
Bold	Bold	k1gInSc1	Bold
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
67	[number]	k4	67
<g/>
.	.	kIx.	.
čísle	číslo	k1gNnSc6	číslo
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
objevil	objevit	k5eAaPmAgMnS	objevit
Batman	Batman	k1gMnSc1	Batman
a	a	k8xC	a
i	i	k9	i
tuto	tento	k3xDgFnSc4	tento
sérii	série	k1gFnSc4	série
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
získal	získat	k5eAaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
vydávání	vydávání	k1gNnSc1	vydávání
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1983	[number]	k4	1983
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1975	[number]	k4	1975
a	a	k8xC	a
1976	[number]	k4	1976
vyšla	vyjít	k5eAaPmAgFnS	vyjít
minisérie	minisérie	k1gFnSc1	minisérie
The	The	k1gFnSc2	The
Joker	Jokra	k1gFnPc2	Jokra
o	o	k7c6	o
devíti	devět	k4xCc6	devět
číslech	číslo	k1gNnPc6	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
i	i	k9	i
dvacet	dvacet	k4xCc1	dvacet
čísel	číslo	k1gNnPc2	číslo
minisérie	minisérie	k1gFnSc2	minisérie
Batman	Batman	k1gMnSc1	Batman
Family	Famil	k1gInPc4	Famil
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
Man-Bat	Man-Bat	k2eAgInSc1d1	Man-Bat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
série	série	k1gFnPc1	série
Batman	Batman	k1gMnSc1	Batman
and	and	k?	and
the	the	k?	the
Outsiders	Outsiders	k1gInSc1	Outsiders
(	(	kIx(	(
<g/>
32	[number]	k4	32
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Outsiders	Outsidersa	k1gFnPc2	Outsidersa
(	(	kIx(	(
<g/>
28	[number]	k4	28
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
the	the	k?	the
Outsiders	Outsiders	k1gInSc1	Outsiders
(	(	kIx(	(
<g/>
13	[number]	k4	13
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Legends	Legends	k1gInSc1	Legends
of	of	k?	of
the	the	k?	the
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
(	(	kIx(	(
<g/>
36	[number]	k4	36
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
napsal	napsat	k5eAaPmAgMnS	napsat
Frank	Frank	k1gMnSc1	Frank
Miller	Miller	k1gMnSc1	Miller
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
přelomových	přelomový	k2eAgFnPc2d1	přelomová
komiksových	komiksový	k2eAgFnPc2d1	komiksová
knih	kniha	k1gFnPc2	kniha
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k2eAgInSc4d1	Knight
Returns	Returns	k1gInSc4	Returns
(	(	kIx(	(
<g/>
Návrat	návrat	k1gInSc1	návrat
temného	temný	k2eAgMnSc2d1	temný
rytíře	rytíř	k1gMnSc2	rytíř
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
poté	poté	k6eAd1	poté
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Year	Year	k1gMnSc1	Year
One	One	k1gMnSc1	One
(	(	kIx(	(
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
jedna	jeden	k4xCgFnSc1	jeden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Devadesátá	devadesátý	k4xOgNnPc1	devadesátý
léta	léto	k1gNnPc1	léto
byla	být	k5eAaImAgNnP	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
nových	nový	k2eAgFnPc2d1	nová
sérií	série	k1gFnPc2	série
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Shadow	Shadow	k1gMnSc1	Shadow
of	of	k?	of
the	the	k?	the
Bat	Bat	k1gFnSc2	Bat
(	(	kIx(	(
<g/>
94	[number]	k4	94
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Legends	Legends	k1gInSc1	Legends
of	of	k?	of
the	the	k?	the
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
(	(	kIx(	(
<g/>
177	[number]	k4	177
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
představil	představit	k5eAaPmAgMnS	představit
i	i	k9	i
scenárista	scenárista	k1gMnSc1	scenárista
Grant	grant	k1gInSc4	grant
Morrison	Morrison	k1gMnSc1	Morrison
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
Adventures	Adventures	k1gMnSc1	Adventures
(	(	kIx(	(
<g/>
36	[number]	k4	36
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
Chronicles	Chronicles	k1gMnSc1	Chronicles
(	(	kIx(	(
<g/>
23	[number]	k4	23
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
and	and	k?	and
Robin	robin	k2eAgMnSc1d1	robin
Adventures	Adventures	k1gMnSc1	Adventures
(	(	kIx(	(
<g/>
25	[number]	k4	25
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Gotham	Gotham	k1gInSc1	Gotham
Adventures	Adventures	k1gInSc1	Adventures
(	(	kIx(	(
<g/>
60	[number]	k4	60
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stále	stále	k6eAd1	stále
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
původní	původní	k2eAgFnPc4d1	původní
série	série	k1gFnPc4	série
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
a	a	k8xC	a
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vznikaly	vznikat	k5eAaImAgFnP	vznikat
i	i	k9	i
nové	nový	k2eAgFnPc4d1	nová
série	série	k1gFnPc4	série
<g/>
.	.	kIx.	.
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Gotham	Gotham	k1gInSc1	Gotham
Knights	Knights	k1gInSc1	Knights
(	(	kIx(	(
<g/>
74	[number]	k4	74
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gotham	Gotham	k1gInSc1	Gotham
Central	Central	k1gFnSc2	Central
(	(	kIx(	(
<g/>
40	[number]	k4	40
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
Adventures	Adventures	k1gMnSc1	Adventures
volume	volum	k1gInSc5	volum
2	[number]	k4	2
(	(	kIx(	(
<g/>
17	[number]	k4	17
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Superman	superman	k1gMnSc1	superman
<g/>
/	/	kIx~	/
<g/>
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
87	[number]	k4	87
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
<g />
.	.	kIx.	.
</s>
<s>
Strikes	Strikes	k1gInSc1	Strikes
(	(	kIx(	(
<g/>
50	[number]	k4	50
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
Confidential	Confidential	k1gMnSc1	Confidential
(	(	kIx(	(
<g/>
54	[number]	k4	54
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Brave	brav	k1gInSc5	brav
and	and	k?	and
the	the	k?	the
Bold	Bold	k1gInSc1	Bold
(	(	kIx(	(
<g/>
22	[number]	k4	22
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Streets	Streets	k1gInSc1	Streets
of	of	k?	of
Gotham	Gotham	k1gInSc1	Gotham
(	(	kIx(	(
<g/>
21	[number]	k4	21
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
and	and	k?	and
Robin	Robina	k1gFnPc2	Robina
(	(	kIx(	(
<g/>
26	[number]	k4	26
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
Incorporated	Incorporated	k1gMnSc1	Incorporated
(	(	kIx(	(
<g/>
8	[number]	k4	8
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
DC	DC	kA	DC
vesmír	vesmír	k1gInSc1	vesmír
prošel	projít	k5eAaPmAgMnS	projít
restartem	restart	k1gInSc7	restart
a	a	k8xC	a
nové	nový	k2eAgInPc1d1	nový
komiksy	komiks	k1gInPc1	komiks
jsou	být	k5eAaImIp3nP	být
vydávány	vydávat	k5eAaPmNgInP	vydávat
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
New	New	k1gFnSc7	New
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
tak	tak	k6eAd1	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
série	série	k1gFnPc4	série
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
and	and	k?	and
Robin	robin	k2eAgInSc1d1	robin
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
jako	jako	k9	jako
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
i	i	k8xC	i
Legends	Legends	k1gInSc1	Legends
of	of	k?	of
the	the	k?	the
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nové	nový	k2eAgFnSc2d1	nová
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
All	All	k1gMnSc1	All
New	New	k1gMnSc1	New
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Brave	brav	k1gInSc5	brav
and	and	k?	and
the	the	k?	the
Bold	Bold	k1gMnSc1	Bold
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Arkham	Arkham	k1gInSc1	Arkham
Unhinged	Unhinged	k1gInSc1	Unhinged
<g/>
,	,	kIx,	,
Batman	Batman	k1gMnSc1	Batman
'	'	kIx"	'
<g/>
66	[number]	k4	66
a	a	k8xC	a
Batman	Batman	k1gMnSc1	Batman
Eternal	Eternal	k1gMnSc1	Eternal
<g/>
.	.	kIx.	.
</s>
<s>
Současnými	současný	k2eAgMnPc7d1	současný
autory	autor	k1gMnPc7	autor
například	například	k6eAd1	například
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Peter	Peter	k1gMnSc1	Peter
Tomasi	Tomas	k1gMnPc1	Tomas
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Layman	Layman	k1gMnSc1	Layman
a	a	k8xC	a
Scott	Scott	k1gMnSc1	Scott
Snyder	Snyder	k1gMnSc1	Snyder
<g/>
.	.	kIx.	.
</s>
<s>
Biografie	biografie	k1gFnSc1	biografie
Batmana	Batman	k1gMnSc2	Batman
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
měněna	měnit	k5eAaImNgFnS	měnit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
taky	taky	k6eAd1	taky
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
komiksových	komiksový	k2eAgFnPc2d1	komiksová
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
pevně	pevně	k6eAd1	pevně
zakotvena	zakotvit	k5eAaPmNgFnS	zakotvit
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
minulost	minulost	k1gFnSc1	minulost
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
čísle	číslo	k1gNnSc6	číslo
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
<g/>
33	[number]	k4	33
Během	během	k7c2	během
Zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
byl	být	k5eAaImAgMnS	být
Batman	Batman	k1gMnSc1	Batman
poprvé	poprvé	k6eAd1	poprvé
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
27	[number]	k4	27
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ještě	ještě	k9	ještě
nedostalo	dostat	k5eNaPmAgNnS	dostat
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
vlastního	vlastní	k2eAgInSc2d1	vlastní
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
navázal	navázat	k5eAaPmAgInS	navázat
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
#	#	kIx~	#
33	[number]	k4	33
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Bruce	Bruce	k1gMnSc1	Bruce
konečně	konečně	k6eAd1	konečně
vyobrazen	vyobrazen	k2eAgMnSc1d1	vyobrazen
v	v	k7c6	v
původních	původní	k2eAgInPc6d1	původní
znacích	znak	k1gInPc6	znak
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
vlastní	vlastní	k2eAgInSc4d1	vlastní
příběh	příběh	k1gInSc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
Bruce	Bruce	k1gMnSc1	Bruce
Wayne	Wayn	k1gInSc5	Wayn
byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
velmi	velmi	k6eAd1	velmi
známých	známý	k2eAgMnPc2d1	známý
gothamských	gothamský	k2eAgMnPc2d1	gothamský
filantropů	filantrop	k1gMnPc2	filantrop
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
už	už	k6eAd1	už
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
patřili	patřit	k5eAaImAgMnP	patřit
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
společenské	společenský	k2eAgFnSc2d1	společenská
vrstvy	vrstva	k1gFnSc2	vrstva
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
tak	tak	k6eAd1	tak
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
ve	v	k7c6	v
Wayne	Wayn	k1gInSc5	Wayn
Manor	Manor	k1gInSc4	Manor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
den	den	k1gInSc4	den
co	co	k9	co
den	den	k1gInSc4	den
prožíval	prožívat	k5eAaImAgInS	prožívat
vznešený	vznešený	k2eAgInSc1d1	vznešený
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
chladnokrevnou	chladnokrevný	k2eAgFnSc7d1	chladnokrevná
vraždou	vražda	k1gFnSc7	vražda
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
smrti	smrt	k1gFnSc6	smrt
tak	tak	k6eAd1	tak
přísahal	přísahat	k5eAaImAgMnS	přísahat
na	na	k7c4	na
jejich	jejich	k3xOp3gInPc4	jejich
hroby	hrob	k1gInPc4	hrob
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomstí	pomstit	k5eAaImIp3nS	pomstit
jejich	jejich	k3xOp3gFnSc4	jejich
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
navždy	navždy	k6eAd1	navždy
vymýtí	vymýtit	k5eAaPmIp3nS	vymýtit
kriminalitu	kriminalita	k1gFnSc4	kriminalita
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Gothamu	Gotham	k1gInSc6	Gotham
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
rozličným	rozličný	k2eAgFnPc3d1	rozličná
studiím	studie	k1gFnPc3	studie
a	a	k8xC	a
tréninkům	trénink	k1gInPc3	trénink
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yIgFnPc3	který
získal	získat	k5eAaPmAgMnS	získat
mnoho	mnoho	k4c4	mnoho
dovedností	dovednost	k1gFnPc2	dovednost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
bojových	bojový	k2eAgNnPc2d1	bojové
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
kriminologie	kriminologie	k1gFnSc1	kriminologie
apod.	apod.	kA	apod.
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
tréninku	trénink	k1gInSc2	trénink
si	se	k3xPyFc3	se
Wayne	Wayn	k1gInSc5	Wayn
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc4	jeho
dovednosti	dovednost	k1gFnPc4	dovednost
sami	sám	k3xTgMnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
kriminálnících	kriminálník	k1gMnPc6	kriminálník
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
vyvolat	vyvolat	k5eAaPmF	vyvolat
bázeň	bázeň	k1gFnSc4	bázeň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
pravou	pravý	k2eAgFnSc7d1	pravá
metlou	metla	k1gFnSc7	metla
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
netopýr	netopýr	k1gMnSc1	netopýr
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
Bruce	Bruce	k1gMnSc1	Bruce
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
později	pozdě	k6eAd2	pozdě
tak	tak	k9	tak
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
symbol	symbol	k1gInSc4	symbol
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kterého	který	k3yQgMnSc4	který
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
kriminalitě	kriminalita	k1gFnSc3	kriminalita
Gothamu	Gotham	k1gInSc2	Gotham
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejtragičtějších	tragický	k2eAgFnPc2d3	nejtragičtější
a	a	k8xC	a
nejzásadnějších	zásadní	k2eAgFnPc2d3	nejzásadnější
událostí	událost	k1gFnPc2	událost
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
však	však	k9	však
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechala	nechat	k5eNaPmAgNnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Wayne	Waynout	k5eAaPmIp3nS	Waynout
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
navštívit	navštívit	k5eAaPmF	navštívit
představení	představení	k1gNnSc4	představení
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Flying	Flying	k1gInSc1	Flying
Graysons	Graysons	k1gInSc1	Graysons
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
rodina	rodina	k1gFnSc1	rodina
velmi	velmi	k6eAd1	velmi
schopných	schopný	k2eAgMnPc2d1	schopný
akrobatů	akrobat	k1gMnPc2	akrobat
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
zabiti	zabít	k5eAaPmNgMnP	zabít
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
odmítnutí	odmítnutí	k1gNnSc3	odmítnutí
vyplacení	vyplacení	k1gNnSc2	vyplacení
výpalného	výpalné	k1gNnSc2	výpalné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
jeden	jeden	k4xCgMnSc1	jeden
Gothamský	Gothamský	k2eAgMnSc1d1	Gothamský
pomatenec	pomatenec	k1gMnSc1	pomatenec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
jejich	jejich	k3xOp3gMnSc7	jejich
potomkem	potomek	k1gMnSc7	potomek
Richardem	Richard	k1gMnSc7	Richard
Graysonem	Grayson	k1gMnSc7	Grayson
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
uzřel	uzřít	k5eAaPmAgMnS	uzřít
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
kdysi	kdysi	k6eAd1	kdysi
před	před	k7c7	před
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
chlapce	chlapec	k1gMnSc4	chlapec
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
ujmout	ujmout	k5eAaPmF	ujmout
a	a	k8xC	a
cvičit	cvičit	k5eAaImF	cvičit
jeho	jeho	k3xOp3gFnPc4	jeho
fyzické	fyzický	k2eAgFnPc4d1	fyzická
a	a	k8xC	a
dedukční	dedukční	k2eAgFnPc4d1	dedukční
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zdárném	zdárný	k2eAgNnSc6d1	zdárné
dokončení	dokončení	k1gNnSc6	dokončení
tréninku	trénink	k1gInSc2	trénink
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
Dicka	Dicek	k1gInSc2	Dicek
učinil	učinit	k5eAaImAgInS	učinit
historicky	historicky	k6eAd1	historicky
prvního	první	k4xOgNnSc2	první
Robina	robin	k2eAgMnSc4d1	robin
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
společně	společně	k6eAd1	společně
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
Tonyho	Tony	k1gMnSc4	Tony
Tucca	Tucc	k2eAgMnSc4d1	Tucc
<g/>
,	,	kIx,	,
mafiánského	mafiánský	k2eAgMnSc4d1	mafiánský
bosse	boss	k1gMnSc4	boss
a	a	k8xC	a
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
Graysonových	Graysonův	k2eAgMnPc2d1	Graysonův
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Roky	rok	k1gInPc1	rok
plynuly	plynout	k5eAaImAgInP	plynout
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
převraty	převrat	k1gInPc1	převrat
i	i	k8xC	i
hrdinové	hrdina	k1gMnPc1	hrdina
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
nenechaly	nechat	k5eNaPmAgFnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Bruce	Bruce	k1gMnSc1	Bruce
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
stává	stávat	k5eAaImIp3nS	stávat
s	s	k7c7	s
určitými	určitý	k2eAgMnPc7d1	určitý
hrdiny	hrdina	k1gMnPc7	hrdina
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
Justice	justice	k1gFnSc2	justice
League	League	k1gFnSc1	League
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
nijak	nijak	k6eAd1	nijak
neomezovalo	omezovat	k5eNaImAgNnS	omezovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
individuálním	individuální	k2eAgInSc6d1	individuální
souboji	souboj	k1gInSc6	souboj
proti	proti	k7c3	proti
Gothamskému	Gothamský	k2eAgNnSc3d1	Gothamský
podsvětí	podsvětí	k1gNnSc3	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
k	k	k7c3	k
Bruceovi	Bruceus	k1gMnSc3	Bruceus
a	a	k8xC	a
Dickovi	Dicek	k1gMnSc3	Dicek
připojil	připojit	k5eAaPmAgInS	připojit
jistý	jistý	k2eAgMnSc1d1	jistý
Alfred	Alfred	k1gMnSc1	Alfred
Pennyworth	Pennyworth	k1gMnSc1	Pennyworth
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
čirou	čirý	k2eAgFnSc4d1	čirá
náhodou	náhodou	k6eAd1	náhodou
zjistil	zjistit	k5eAaPmAgMnS	zjistit
jejich	jejich	k3xOp3gMnPc3	jejich
"	"	kIx"	"
<g/>
pravé	pravý	k2eAgFnSc6d1	pravá
<g/>
"	"	kIx"	"
totožnosti	totožnost	k1gFnSc6	totožnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
objevil	objevit	k5eAaPmAgInS	objevit
totiž	totiž	k9	totiž
tajemství	tajemství	k1gNnSc4	tajemství
starých	starý	k2eAgFnPc2d1	stará
pendlovek	pendlovky	k1gFnPc2	pendlovky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
členů	člen	k1gInPc2	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jejich	jejich	k3xOp3gNnSc4	jejich
tajemství	tajemství	k1gNnSc4	tajemství
vždy	vždy	k6eAd1	vždy
držel	držet	k5eAaImAgInS	držet
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
nebyl	být	k5eNaImAgMnS	být
Bruce	Bruce	k1gMnSc1	Bruce
Wayne	Wayn	k1gInSc5	Wayn
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
úplného	úplný	k2eAgMnSc4d1	úplný
"	"	kIx"	"
<g/>
suchara	suchar	k1gMnSc4	suchar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gMnSc3	on
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
vsazena	vsadit	k5eAaPmNgFnS	vsadit
do	do	k7c2	do
života	život	k1gInSc2	život
žena	žena	k1gFnSc1	žena
jménem	jméno	k1gNnSc7	jméno
Vicky	Vicka	k1gFnSc2	Vicka
Vale	val	k1gInSc6	val
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
obdobou	obdoba	k1gFnSc7	obdoba
Lois	Lois	k1gInSc1	Lois
Lane	Lane	k1gFnSc7	Lane
z	z	k7c2	z
Metropolis	Metropolis	k1gFnSc2	Metropolis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
žena	žena	k1gFnSc1	žena
se	se	k3xPyFc4	se
s	s	k7c7	s
Brucem	Bruec	k1gInSc7	Bruec
snažila	snažit	k5eAaImAgFnS	snažit
trávit	trávit	k5eAaImF	trávit
co	co	k9	co
nejvíc	nejvíc	k6eAd1	nejvíc
času	čas	k1gInSc2	čas
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
na	na	k7c4	na
odhalení	odhalení	k1gNnSc4	odhalení
Batmanovy	Batmanův	k2eAgFnSc2d1	Batmanova
pravé	pravý	k2eAgFnSc2d1	pravá
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
překvapení	překvapení	k1gNnSc3	překvapení
přece	přece	k8xC	přece
jen	jen	k9	jen
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Křižákem	křižák	k1gInSc7	křižák
Gothamu	Gotham	k1gInSc2	Gotham
nebyl	být	k5eNaImAgMnS	být
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
než	než	k8xS	než
sám	sám	k3xTgMnSc1	sám
velký	velký	k2eAgMnSc1d1	velký
Bruce	Bruce	k1gMnSc1	Bruce
Wayne	Wayn	k1gMnSc5	Wayn
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
snažil	snažit	k5eAaImAgMnS	snažit
pod	pod	k7c7	pod
svou	svůj	k3xOyFgFnSc7	svůj
kápí	kápě	k1gFnSc7	kápě
neustále	neustále	k6eAd1	neustále
vyvracet	vyvracet	k5eAaImF	vyvracet
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tyto	tento	k3xDgFnPc1	tento
honby	honba	k1gFnPc1	honba
za	za	k7c7	za
identitami	identita	k1gFnPc7	identita
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
utajování	utajování	k1gNnSc1	utajování
a	a	k8xC	a
objevování	objevování	k1gNnSc1	objevování
se	se	k3xPyFc4	se
stávaly	stávat	k5eAaImAgFnP	stávat
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
znaků	znak	k1gInPc2	znak
Zlatého	zlatý	k2eAgInSc2d1	zlatý
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
pravý	pravý	k2eAgMnSc1d1	pravý
Batman	Batman	k1gMnSc1	Batman
původně	původně	k6eAd1	původně
vylíčen	vylíčen	k2eAgMnSc1d1	vylíčen
jako	jako	k8xS	jako
chladnokrevný	chladnokrevný	k2eAgMnSc1d1	chladnokrevný
a	a	k8xC	a
bdělý	bdělý	k2eAgMnSc1d1	bdělý
strážce	strážce	k1gMnSc1	strážce
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
nedělalo	dělat	k5eNaImAgNnS	dělat
problém	problém	k1gInSc4	problém
zabíjet	zabíjet	k5eAaImF	zabíjet
jednoho	jeden	k4xCgMnSc4	jeden
kriminálníka	kriminálník	k1gMnSc4	kriminálník
po	po	k7c6	po
druhém	druhý	k4xOgInSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nelíbilo	líbit	k5eNaImAgNnS	líbit
mnoha	mnoho	k4c3	mnoho
čtenářům	čtenář	k1gMnPc3	čtenář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
chtěli	chtít	k5eAaImAgMnP	chtít
pravého	pravý	k2eAgMnSc4d1	pravý
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
charakter	charakter	k1gInSc4	charakter
Batmana	Batman	k1gMnSc2	Batman
čekal	čekat	k5eAaImAgMnS	čekat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
mravní	mravní	k2eAgInSc4d1	mravní
kodex	kodex	k1gInSc4	kodex
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
mužem	muž	k1gMnSc7	muž
jménem	jméno	k1gNnSc7	jméno
Whitney	Whitnea	k1gFnSc2	Whitnea
Ellsworth	Ellswortha	k1gFnPc2	Ellswortha
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tento	tento	k3xDgMnSc1	tento
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
Bruceovy	Bruceův	k2eAgInPc4d1	Bruceův
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
rysy	rys	k1gInPc4	rys
(	(	kIx(	(
<g/>
nikdy	nikdy	k6eAd1	nikdy
svou	svůj	k3xOyFgFnSc4	svůj
oběť	oběť	k1gFnSc4	oběť
nezabít	zabít	k5eNaPmF	zabít
a	a	k8xC	a
nepoužívat	používat	k5eNaImF	používat
žádné	žádný	k3yNgFnPc4	žádný
střelné	střelný	k2eAgFnPc4d1	střelná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
stříbrné	stříbrný	k2eAgFnSc6d1	stříbrná
éře	éra	k1gFnSc6	éra
komiksu	komiks	k1gInSc2	komiks
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Batmana	Batman	k1gMnSc4	Batman
poznamenala	poznamenat	k5eAaPmAgFnS	poznamenat
až	až	k6eAd1	až
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vytvořen	vytvořen	k2eAgMnSc1d1	vytvořen
Batman	Batman	k1gMnSc1	Batman
v	v	k7c6	v
paralelním	paralelní	k2eAgInSc6d1	paralelní
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
Země-dvě	Zeměva	k1gFnSc6	Země-dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
Bruce	Bruce	k1gMnSc1	Bruce
Wayne	Wayn	k1gInSc5	Wayn
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
Selinou	Selin	k2eAgFnSc7d1	Selina
Kyle	Kyle	k1gFnSc7	Kyle
(	(	kIx(	(
<g/>
Catwoman	Catwoman	k1gMnSc1	Catwoman
<g/>
)	)	kIx)	)
a	a	k8xC	a
společně	společně	k6eAd1	společně
vychovali	vychovat	k5eAaPmAgMnP	vychovat
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
Helenu	Helena	k1gFnSc4	Helena
Wayne	Wayn	k1gInSc5	Wayn
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
Huntress	Huntress	k1gInSc4	Huntress
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dickem	Dicek	k1gMnSc7	Dicek
Graysonem	Grayson	k1gMnSc7	Grayson
(	(	kIx(	(
<g/>
paralelní	paralelní	k2eAgInSc1d1	paralelní
Robin	robin	k2eAgInSc1d1	robin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
převzala	převzít	k5eAaPmAgFnS	převzít
ochranu	ochrana	k1gFnSc4	ochrana
Gothamu	Gotham	k1gInSc2	Gotham
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Bruce	Bruce	k1gMnSc1	Bruce
Wayne	Wayn	k1gInSc5	Wayn
odložil	odložit	k5eAaPmAgInS	odložit
identitu	identita	k1gFnSc4	identita
Batmana	Batman	k1gMnSc2	Batman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
policejním	policejní	k2eAgMnSc7d1	policejní
komisařem	komisař	k1gMnSc7	komisař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
paralelním	paralelní	k2eAgInSc6d1	paralelní
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
Bruce	Bruce	k1gMnSc1	Bruce
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1954	[number]	k4	1954
až	až	k8xS	až
1984	[number]	k4	1984
vycházela	vycházet	k5eAaImAgFnS	vycházet
série	série	k1gFnSc1	série
World	World	k1gMnSc1	World
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Finest	Finest	k1gInSc1	Finest
Comics	comics	k1gInSc4	comics
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
původní	původní	k2eAgMnSc1d1	původní
Batman	Batman	k1gMnSc1	Batman
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
Supermanem	superman	k1gMnSc7	superman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
týmu	tým	k1gInSc2	tým
Justice	justice	k1gFnSc1	justice
League	League	k1gFnSc1	League
of	of	k?	of
America	Americ	k1gInSc2	Americ
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
příběhy	příběh	k1gInPc1	příběh
byly	být	k5eAaImAgInP	být
vydávány	vydávat	k5eAaPmNgInP	vydávat
v	v	k7c6	v
komiksu	komiks	k1gInSc6	komiks
Brave	brav	k1gInSc5	brav
and	and	k?	and
the	the	k?	the
Bold	Bold	k1gInSc1	Bold
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
Dick	Dick	k1gMnSc1	Dick
Grayson	Grayson	k1gMnSc1	Grayson
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
univerzitu	univerzita	k1gFnSc4	univerzita
a	a	k8xC	a
Bruce	Bruce	k1gMnSc1	Bruce
opustil	opustit	k5eAaPmAgMnS	opustit
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
Wayne	Wayn	k1gInSc5	Wayn
Manor	Manor	k1gMnSc1	Manor
a	a	k8xC	a
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
Gothamu	Gotham	k1gInSc2	Gotham
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
blíže	blízce	k6eAd2	blízce
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
batmanovy	batmanův	k2eAgInPc1d1	batmanův
příběhy	příběh	k1gInPc1	příběh
více	hodně	k6eAd2	hodně
temnými	temný	k2eAgFnPc7d1	temná
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
geniální	geniální	k2eAgMnSc1d1	geniální
maniak	maniak	k1gMnSc1	maniak
Ra	ra	k0	ra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
al	ala	k1gFnPc2	ala
Ghul	Ghulum	k1gNnPc2	Ghulum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
Robina	robin	k2eAgInSc2d1	robin
<g/>
,	,	kIx,	,
Dicka	Dicek	k1gMnSc2	Dicek
Graysona	Grayson	k1gMnSc2	Grayson
stal	stát	k5eAaPmAgInS	stát
superhrdina	superhrdin	k2eAgFnSc1d1	superhrdina
Nightwing	Nightwing	k1gInSc1	Nightwing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
Batmanovu	Batmanův	k2eAgFnSc4d1	Batmanova
historii	historie	k1gFnSc4	historie
převyprávěl	převyprávět	k5eAaPmAgMnS	převyprávět
Frank	Frank	k1gMnSc1	Frank
Miller	Miller	k1gMnSc1	Miller
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Batmana	Batman	k1gMnSc4	Batman
pojal	pojmout	k5eAaPmAgMnS	pojmout
temněji	temně	k6eAd2	temně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
realisticky	realisticky	k6eAd1	realisticky
<g/>
.	.	kIx.	.
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
z	z	k7c2	z
paralelní	paralelní	k2eAgFnSc2d1	paralelní
Země-dvě	Zeměva	k1gFnSc3	Země-dva
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
éře	éra	k1gFnSc6	éra
vymazán	vymazán	k2eAgInSc4d1	vymazán
z	z	k7c2	z
DC	DC	kA	DC
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Millerově	Millerův	k2eAgNnSc6d1	Millerovo
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
gothamská	gothamský	k2eAgFnSc1d1	gothamský
policie	policie	k1gFnSc1	policie
zkorumpovaná	zkorumpovaný	k2eAgFnSc1d1	zkorumpovaná
a	a	k8xC	a
nepřátelská	přátelský	k2eNgFnSc1d1	nepřátelská
k	k	k7c3	k
Batmanovi	Batman	k1gMnSc3	Batman
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
jediným	jediný	k2eAgMnSc7d1	jediný
policejním	policejní	k2eAgMnSc7d1	policejní
spojencem	spojenec	k1gMnSc7	spojenec
je	být	k5eAaImIp3nS	být
komisař	komisař	k1gMnSc1	komisař
James	James	k1gMnSc1	James
Gordon	Gordon	k1gMnSc1	Gordon
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgMnSc1	druhý
Robin	robin	k2eAgMnSc1d1	robin
Jason	Jason	k1gMnSc1	Jason
Todd	Todd	k1gMnSc1	Todd
je	být	k5eAaImIp3nS	být
osiřelým	osiřelý	k2eAgMnSc7d1	osiřelý
synem	syn	k1gMnSc7	syn
zlodějíčka	zlodějíček	k1gMnSc2	zlodějíček
<g/>
.	.	kIx.	.
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
současně	současně	k6eAd1	současně
není	být	k5eNaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Justice	justice	k1gFnSc2	justice
League	Leagu	k1gInSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
"	"	kIx"	"
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
Jason	Jason	k1gMnSc1	Jason
Todd	Todd	k1gMnSc1	Todd
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
Jokerem	Joker	k1gMnSc7	Joker
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
ztráty	ztráta	k1gFnSc2	ztráta
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
byl	být	k5eAaImAgMnS	být
Bruce	Bruce	k1gMnSc1	Bruce
tak	tak	k6eAd1	tak
zdrcen	zdrcen	k2eAgMnSc1d1	zdrcen
<g/>
,	,	kIx,	,
že	že	k8xS	že
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
více	hodně	k6eAd2	hodně
brutální	brutální	k2eAgFnPc4d1	brutální
metody	metoda	k1gFnPc4	metoda
v	v	k7c6	v
boji	boj	k1gInSc6	boj
se	s	k7c7	s
zločinem	zločin	k1gInSc7	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
pracoval	pracovat	k5eAaImAgMnS	pracovat
samostatně	samostatně	k6eAd1	samostatně
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
novým	nový	k2eAgMnSc7d1	nový
Robinem	Robin	k1gMnSc7	Robin
stal	stát	k5eAaPmAgInS	stát
Tim	Tim	k?	Tim
Drake	Drake	k1gInSc1	Drake
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
Jason	Jason	k1gMnSc1	Jason
Todd	Todd	k1gMnSc1	Todd
oživen	oživen	k2eAgMnSc1d1	oživen
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
zabijákem	zabiják	k1gInSc7	zabiják
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Red	Red	k1gMnSc1	Red
Hood	Hood	k1gMnSc1	Hood
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
příběhové	příběhový	k2eAgFnSc6d1	příběhová
linii	linie	k1gFnSc6	linie
Knightfall	Knightfalla	k1gFnPc2	Knightfalla
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgMnSc1d1	nový
nepřítel	nepřítel	k1gMnSc1	nepřítel
Bane	Ban	k1gFnSc2	Ban
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
výrazně	výrazně	k6eAd1	výrazně
zranit	zranit	k5eAaPmF	zranit
a	a	k8xC	a
ochromit	ochromit	k5eAaPmF	ochromit
Batmana	Batman	k1gMnSc4	Batman
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
rekonvalescence	rekonvalescence	k1gFnSc2	rekonvalescence
převzal	převzít	k5eAaPmAgInS	převzít
identitu	identita	k1gFnSc4	identita
Batmana	Batman	k1gMnSc2	Batman
Dick	Dick	k1gMnSc1	Dick
Grayson	Grayson	k1gMnSc1	Grayson
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
Batmanovy	Batmanův	k2eAgInPc1d1	Batmanův
příběhy	příběh	k1gInPc1	příběh
vydávány	vydáván	k2eAgInPc1d1	vydáván
nevázaně	vázaně	k6eNd1	vázaně
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
několika	několik	k4yIc7	několik
různými	různý	k2eAgMnPc7d1	různý
autory	autor	k1gMnPc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
dějových	dějový	k2eAgFnPc2d1	dějová
linií	linie	k1gFnPc2	linie
byl	být	k5eAaImAgMnS	být
Bruce	Bruce	k1gMnSc1	Bruce
Wayne	Wayn	k1gInSc5	Wayn
zabit	zabít	k5eAaPmNgMnS	zabít
a	a	k8xC	a
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
identitu	identita	k1gFnSc4	identita
Batmana	Batman	k1gMnSc2	Batman
svedli	svést	k5eAaPmAgMnP	svést
boj	boj	k1gInSc4	boj
Dick	Dicka	k1gFnPc2	Dicka
Grayson	Grayson	k1gNnSc1	Grayson
a	a	k8xC	a
Tim	Tim	k?	Tim
Drake	Drake	k1gInSc1	Drake
<g/>
.	.	kIx.	.
</s>
<s>
Grayson	Grayson	k1gMnSc1	Grayson
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Batmanem	Batman	k1gInSc7	Batman
a	a	k8xC	a
Drake	Drake	k1gFnSc7	Drake
vlastní	vlastní	k2eAgFnSc7d1	vlastní
postavou	postava	k1gFnSc7	postava
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Red	Red	k1gFnSc2	Red
Robin	Robina	k1gFnPc2	Robina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
DC	DC	kA	DC
vesmír	vesmír	k1gInSc1	vesmír
znovu	znovu	k6eAd1	znovu
pozměněn	pozměněn	k2eAgInSc1d1	pozměněn
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
52	[number]	k4	52
restart	restarta	k1gFnPc2	restarta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Grayson	Grayson	k1gMnSc1	Grayson
znovu	znovu	k6eAd1	znovu
přejal	přejmout	k5eAaPmAgMnS	přejmout
identitu	identita	k1gFnSc4	identita
Nightwinga	Nightwinga	k1gFnSc1	Nightwinga
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
role	role	k1gFnSc2	role
Batmana	Batman	k1gMnSc2	Batman
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
Bruce	Bruce	k1gMnSc1	Bruce
Wayne	Wayn	k1gInSc5	Wayn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
vydávány	vydáván	k2eAgFnPc1d1	vydávána
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
série	série	k1gFnPc1	série
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
a	a	k8xC	a
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vydávají	vydávat	k5eAaImIp3nP	vydávat
komiksové	komiksový	k2eAgFnSc2d1	komiksová
knihy	kniha	k1gFnSc2	kniha
Batman	Batman	k1gMnSc1	Batman
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
a	a	k8xC	a
Crew	Crew	k1gMnSc1	Crew
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Monstra	monstrum	k1gNnPc4	monstrum
ve	v	k7c6	v
stoce	stoka	k1gFnSc6	stoka
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
1	[number]	k4	1
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Strnad	Strnad	k1gMnSc1	Strnad
a	a	k8xC	a
Kevin	Kevin	k1gMnSc1	Kevin
Nowlan	Nowlan	k1gMnSc1	Nowlan
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
#	#	kIx~	#
<g/>
4	[number]	k4	4
-	-	kIx~	-
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Monsters	Monsters	k1gInSc1	Monsters
in	in	k?	in
the	the	k?	the
Closet	Closet	k1gInSc1	Closet
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Kaměňák	Kaměňák	k1gMnSc1	Kaměňák
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Alan	Alan	k1gMnSc1	Alan
Moore	Moor	k1gInSc5	Moor
a	a	k8xC	a
Brian	Brian	k1gMnSc1	Brian
Bolland	Bolland	k1gInSc1	Bolland
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Killing	Killing	k1gInSc1	Killing
Joke	Joke	k1gFnSc1	Joke
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Černý	Černý	k1gMnSc1	Černý
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
7	[number]	k4	7
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
a	a	k8xC	a
Simon	Simon	k1gMnSc1	Simon
Bisley	Bislea	k1gFnSc2	Bislea
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
#	#	kIx~	#
<g/>
2	[number]	k4	2
-	-	kIx~	-
"	"	kIx"	"
<g/>
A	a	k9	a
Black	Black	k1gInSc1	Black
&	&	k?	&
White	Whit	k1gInSc5	Whit
World	Worldo	k1gNnPc2	Worldo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
člověk	člověk	k1gMnSc1	člověk
ještě	ještě	k9	ještě
žije	žít	k5eAaImIp3nS	žít
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
10	[number]	k4	10
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Brian	Brian	k1gInSc1	Brian
Bolland	Bolland	k1gInSc1	Bolland
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
#	#	kIx~	#
<g/>
4	[number]	k4	4
-	-	kIx~	-
"	"	kIx"	"
<g/>
An	An	k1gMnSc1	An
Innocent	Innocent	k1gMnSc1	Innocent
Guy	Guy	k1gMnSc1	Guy
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Darkness	Darkness	k1gInSc1	Darkness
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnPc2	Crew
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Scott	Scott	k1gMnSc1	Scott
Lobdell	Lobdell	k1gMnSc1	Lobdell
<g/>
,	,	kIx,	,
Jeph	Jeph	k1gMnSc1	Jeph
Loeb	Loeb	k1gMnSc1	Loeb
a	a	k8xC	a
Marc	Marc	k1gFnSc1	Marc
Silvestri	Silvestr	k1gFnSc2	Silvestr
<g/>
:	:	kIx,	:
Darkness	Darkness	k1gInSc1	Darkness
<g/>
/	/	kIx~	/
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
pár	pár	k1gInSc1	pár
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Bruce	Bruce	k1gMnSc1	Bruce
Timm	Timm	k1gMnSc1	Timm
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
#	#	kIx~	#
<g/>
1	[number]	k4	1
-	-	kIx~	-
"	"	kIx"	"
<g/>
Two	Two	k1gMnSc1	Two
of	of	k?	of
Kind	Kind	k1gMnSc1	Kind
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Věčný	věčný	k2eAgInSc1d1	věčný
smutek	smutek	k1gInSc1	smutek
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Ted	Ted	k1gMnSc1	Ted
McKeever	McKeever	k1gMnSc1	McKeever
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
#	#	kIx~	#
<g/>
1	[number]	k4	1
-	-	kIx~	-
"	"	kIx"	"
<g/>
Perpetual	Perpetual	k1gInSc1	Perpetual
Mourning	Mourning	k1gInSc1	Mourning
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Drobné	drobný	k2eAgInPc1d1	drobný
přestupky	přestupek	k1gInPc1	přestupek
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Howard	Howard	k1gMnSc1	Howard
Chaykin	Chaykin	k1gMnSc1	Chaykin
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
#	#	kIx~	#
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
-	-	kIx~	-
"	"	kIx"	"
<g/>
Petty	Pett	k1gInPc1	Pett
Crimes	Crimesa	k1gFnPc2	Crimesa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Chorobopis	chorobopis	k1gInSc1	chorobopis
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Paul	Paul	k1gMnSc1	Paul
Dini	Din	k1gFnSc2	Din
a	a	k8xC	a
Alex	Alex	k1gMnSc1	Alex
Ross	Ross	k1gInSc1	Ross
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
-	-	kIx~	-
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Case	Case	k1gInSc1	Case
Study	stud	k1gInPc1	stud
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
/	/	kIx~	/
<g/>
Soudce	soudce	k1gMnSc1	soudce
Dredd	Dredd	k1gMnSc1	Dredd
-	-	kIx~	-
Poslední	poslední	k2eAgFnSc1d1	poslední
hádanka	hádanka	k1gFnSc1	hádanka
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
/	/	kIx~	/
Alan	alan	k1gInSc1	alan
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
John	John	k1gMnSc1	John
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
/	/	kIx~	/
<g/>
Judge	Judge	k1gFnSc1	Judge
Dredd	Dredd	k1gMnSc1	Dredd
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Ultimate	Ultimat	k1gInSc5	Ultimat
Riddle	Riddle	k1gMnPc7	Riddle
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Budu	být	k5eAaImBp1nS	být
tě	ty	k3xPp2nSc4	ty
sledovat	sledovat	k5eAaImF	sledovat
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
/	/	kIx~	/
Ed	Ed	k1gMnSc1	Ed
Brubaker	Brubaker	k1gMnSc1	Brubaker
a	a	k8xC	a
Ryan	Ryan	k1gMnSc1	Ryan
Sook	Sook	k1gMnSc1	Sook
<g/>
:	:	kIx,	:
Gotham	Gotham	k1gInSc1	Gotham
Knights	Knights	k1gInSc1	Knights
#	#	kIx~	#
<g/>
41	[number]	k4	41
-	-	kIx~	-
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
́	́	k?	́
<g/>
ll	ll	k?	ll
Be	Be	k1gFnSc1	Be
Watching	Watching	k1gInSc1	Watching
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Šílená	šílený	k2eAgFnSc1d1	šílená
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
25	[number]	k4	25
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Paul	Paul	k1gMnSc1	Paul
Dini	Din	k1gFnSc2	Din
a	a	k8xC	a
Bruce	Bruce	k1gMnSc1	Bruce
Timm	Timm	k1gMnSc1	Timm
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
Adventures	Adventures	k1gMnSc1	Adventures
<g/>
:	:	kIx,	:
Mad	Mad	k1gMnSc1	Mad
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Lobo	Lobo	k1gNnSc1	Lobo
(	(	kIx(	(
<g/>
Crew	Crew	k1gFnSc1	Crew
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Alan	alan	k1gInSc1	alan
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
Simon	Simon	k1gMnSc1	Simon
Bisley	Bislea	k1gFnSc2	Bislea
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
/	/	kIx~	/
<g/>
Lobo	Lobo	k1gMnSc1	Lobo
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
1997	[number]	k4	1997
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
a	a	k8xC	a
Robin	robin	k2eAgMnSc1d1	robin
/	/	kIx~	/
(	(	kIx(	(
<g/>
D.	D.	kA	D.
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Neil	Neil	k1gMnSc1	Neil
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Damaqqio	Damaqqio	k1gMnSc1	Damaqqio
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
&	&	k?	&
Robin	robin	k2eAgMnSc1d1	robin
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
/	/	kIx~	/
<g/>
Soudce	soudce	k1gMnSc1	soudce
Dredd	Dredd	k1gMnSc1	Dredd
-	-	kIx~	-
Rozsudek	rozsudek	k1gInSc1	rozsudek
nad	nad	k7c4	nad
Gotham	Gotham	k1gInSc4	Gotham
/	/	kIx~	/
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Bisley	Bislea	k1gFnSc2	Bislea
<g/>
:	:	kIx,	:
Judgment	Judgment	k1gMnSc1	Judgment
on	on	k3xPp3gMnSc1	on
Gotham	Gotham	k1gInSc1	Gotham
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Soudce	soudce	k1gMnSc1	soudce
Dredd	Dredd	k1gMnSc1	Dredd
-	-	kIx~	-
Zemřít	zemřít	k5eAaPmF	zemřít
smíchy	smích	k1gInPc4	smích
/	/	kIx~	/
(	(	kIx(	(
<g/>
A.	A.	kA	A.
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
G.	G.	kA	G.
Fabry	Fabr	k1gInPc1	Fabr
<g/>
:	:	kIx,	:
Die	Die	k1gFnSc1	Die
Laughing	Laughing	k1gInSc1	Laughing
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
(	(	kIx(	(
<g/>
Unicorn	Unicorn	k1gMnSc1	Unicorn
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
/	/	kIx~	/
(	(	kIx(	(
<g/>
Různé	různý	k2eAgInPc4d1	různý
komiksy	komiks	k1gInPc4	komiks
vydané	vydaný	k2eAgNnSc4d1	vydané
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
temné	temný	k2eAgNnSc1d1	temné
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Comicsové	comicsový	k2eAgFnPc1d1	comicsová
legendy	legenda	k1gFnPc1	legenda
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
/	/	kIx~	/
P.	P.	kA	P.
Milligan	Milligan	k1gInSc1	Milligan
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Dwyer	Dwyer	k1gMnSc1	Dwyer
<g/>
:	:	kIx,	:
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
<g/>
,	,	kIx,	,
Dark	Dark	k1gMnSc1	Dark
city	city	k1gFnSc2	city
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Další	další	k2eAgFnPc1d1	další
podkapitoly	podkapitola	k1gFnPc1	podkapitola
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
dle	dle	k7c2	dle
původního	původní	k2eAgInSc2d1	původní
roku	rok	k1gInSc2	rok
vydání	vydání	k1gNnSc2	vydání
originálu	originál	k1gInSc2	originál
<g/>
.	.	kIx.	.
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
Temného	temný	k2eAgMnSc2d1	temný
rytíře	rytíř	k1gMnSc2	rytíř
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
/	/	kIx~	/
(	(	kIx(	(
<g/>
Frank	Frank	k1gMnSc1	Frank
Miller	Miller	k1gMnSc1	Miller
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
Returns	Returnsa	k1gFnPc2	Returnsa
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
/	/	kIx~	/
(	(	kIx(	(
<g/>
Frank	Frank	k1gMnSc1	Frank
Miller	Miller	k1gMnSc1	Miller
a	a	k8xC	a
David	David	k1gMnSc1	David
Mazzucchelli	Mazzucchelle	k1gFnSc4	Mazzucchelle
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
#	#	kIx~	#
<g/>
404	[number]	k4	404
<g/>
-	-	kIx~	-
<g/>
<g />
.	.	kIx.	.
</s>
<s>
407	[number]	k4	407
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Year	Year	k1gMnSc1	Year
One	One	k1gMnSc1	One
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Deset	deset	k4xCc1	deset
nocí	noc	k1gFnPc2	noc
KGBeasta	KGBeasta	k1gMnSc1	KGBeasta
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jim	on	k3xPp3gMnPc3	on
Starlin	Starlin	k2eAgInSc4d1	Starlin
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
Aparo	Aparo	k1gNnSc1	Aparo
a	a	k8xC	a
Mike	Mike	k1gNnSc1	Mike
De	De	k?	De
Carlo	Carlo	k1gNnSc1	Carlo
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
#	#	kIx~	#
<g/>
417	[number]	k4	417
<g/>
-	-	kIx~	-
<g/>
420	[number]	k4	420
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Ten	ten	k3xDgInSc1	ten
Nights	Nights	k1gInSc1	Nights
of	of	k?	of
the	the	k?	the
Beast	Beast	k1gInSc1	Beast
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Arkham	Arkham	k1gInSc1	Arkham
-	-	kIx~	-
Pochmurný	pochmurný	k2eAgInSc1d1	pochmurný
dům	dům	k1gInSc1	dům
v	v	k7c6	v
pochmurném	pochmurný	k2eAgInSc6d1	pochmurný
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
/	/	kIx~	/
(	(	kIx(	(
<g/>
Grant	grant	k1gInSc1	grant
Morrison	Morrisona	k1gFnPc2	Morrisona
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
McKean	McKean	k1gInSc1	McKean
<g/>
:	:	kIx,	:
Arkham	Arkham	k1gInSc1	Arkham
Asylum	Asylum	k1gInSc1	Asylum
<g/>
:	:	kIx,	:
A	A	kA	A
Serious	Serious	k1gInSc1	Serious
House	house	k1gNnSc1	house
on	on	k3xPp3gMnSc1	on
Serious	Serious	k1gMnSc1	Serious
Earth	Earth	k1gMnSc1	Earth
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
Halloween	Halloween	k1gInSc4	Halloween
1	[number]	k4	1
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jeph	Jeph	k1gMnSc1	Jeph
Loeb	Loeb	k1gMnSc1	Loeb
a	a	k8xC	a
Tim	Tim	k?	Tim
Sale	Sale	k1gFnSc1	Sale
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
Halloween	Hallowena	k1gFnPc2	Hallowena
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
Halloween	Halloween	k1gInSc4	Halloween
2	[number]	k4	2
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jeph	Jeph	k1gMnSc1	Jeph
Loeb	Loeb	k1gMnSc1	Loeb
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Tim	Tim	k?	Tim
Sale	Sale	k1gFnSc1	Sale
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
Halloween	Hallowena	k1gFnPc2	Hallowena
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Temné	temný	k2eAgNnSc1d1	temné
vítězství	vítězství	k1gNnSc1	vítězství
1	[number]	k4	1
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jeph	Jeph	k1gMnSc1	Jeph
Loeb	Loeb	k1gMnSc1	Loeb
a	a	k8xC	a
Tim	Tim	k?	Tim
Sale	Sale	k1gFnSc1	Sale
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Dark	Dark	k1gMnSc1	Dark
Victory	Victor	k1gMnPc7	Victor
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Temné	temný	k2eAgNnSc1d1	temné
vítězství	vítězství	k1gNnSc1	vítězství
2	[number]	k4	2
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jeph	Jeph	k1gMnSc1	Jeph
Loeb	Loeb	k1gMnSc1	Loeb
a	a	k8xC	a
Tim	Tim	k?	Tim
Sale	Sale	k1gFnSc1	Sale
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Dark	Dark	k1gMnSc1	Dark
Victory	Victor	k1gMnPc7	Victor
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Ticho	ticho	k1gNnSc1	ticho
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jeph	Jeph	k1gInSc1	Jeph
Loeb	Loeba	k1gFnPc2	Loeba
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
#	#	kIx~	#
<g/>
608	[number]	k4	608
<g/>
-	-	kIx~	-
<g/>
613	[number]	k4	613
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hush	Hush	k1gInSc1	Hush
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Ticho	ticho	k1gNnSc1	ticho
2	[number]	k4	2
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jeph	Jeph	k1gInSc1	Jeph
Loeb	Loeba	k1gFnPc2	Loeba
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
#	#	kIx~	#
<g />
.	.	kIx.	.
</s>
<s>
<g/>
614	[number]	k4	614
<g/>
-	-	kIx~	-
<g/>
619	[number]	k4	619
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hush	Hush	k1gInSc1	Hush
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Ticho	ticho	k1gNnSc1	ticho
-	-	kIx~	-
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgFnSc6	druhý
a	a	k8xC	a
ucelené	ucelený	k2eAgNnSc1d1	ucelené
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jeph	Jeph	k1gInSc1	Jeph
Loeb	Loeba	k1gFnPc2	Loeba
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
#	#	kIx~	#
<g/>
608	[number]	k4	608
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
619	[number]	k4	619
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hush	Hush	k1gInSc1	Hush
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
DC	DC	kA	DC
komiksový	komiksový	k2eAgInSc1d1	komiksový
komplet	komplet	k1gInSc1	komplet
001	[number]	k4	001
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Ticho	ticho	k1gNnSc1	ticho
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
/	/	kIx~	/
(	(	kIx(	(
<g/>
Jeph	Jeph	k1gInSc1	Jeph
Loeb	Loeba	k1gFnPc2	Loeba
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Lee	Lea	k1gFnSc3	Lea
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
#	#	kIx~	#
<g/>
608	[number]	k4	608
<g/>
-	-	kIx~	-
<g/>
613	[number]	k4	613
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hush	Hush	k1gInSc1	Hush
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
+	+	kIx~	+
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
(	(	kIx(	(
<g/>
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Kameňák	Kameňák	k?	Kameňák
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
/	/	kIx~	/
(	(	kIx(	(
<g/>
Alan	Alan	k1gMnSc1	Alan
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
Bolland	Bolland	k1gInSc1	Bolland
<g/>
:	:	kIx,	:
The	The	k1gFnPc1	The
Killing	Killing	k1gInSc4	Killing
Joke	Jok	k1gFnSc2	Jok
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
;	;	kIx,	;
Ed	Ed	k1gMnSc1	Ed
Brubaker	Brubaker	k1gMnSc1	Brubaker
<g/>
,	,	kIx,	,
Doug	Doug	k1gMnSc1	Doug
Mahnke	Mahnk	k1gFnSc2	Mahnk
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Man	Man	k1gMnSc1	Man
Who	Who	k1gMnSc1	Who
Laughs	Laughsa	k1gFnPc2	Laughsa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2012	[number]	k4	2012
/	/	kIx~	/
(	(	kIx(	(
<g/>
Grant	grant	k1gInSc1	grant
Morrison	Morrisona	k1gFnPc2	Morrisona
a	a	k8xC	a
Andy	Anda	k1gFnSc2	Anda
Kubert	Kubert	k1gMnSc1	Kubert
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
#	#	kIx~	#
<g/>
655	[number]	k4	655
<g/>
-	-	kIx~	-
<g/>
658	[number]	k4	658
a	a	k8xC	a
#	#	kIx~	#
<g/>
663	[number]	k4	663
<g/>
-	-	kIx~	-
<g/>
666	[number]	k4	666
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Batman	Batman	k1gMnSc1	Batman
&	&	k?	&
Son	son	k1gInSc1	son
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
R.	R.	kA	R.
<g/>
I.	I.	kA	I.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
P.	P.	kA	P.
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
/	/	kIx~	/
(	(	kIx(	(
<g/>
Grant	grant	k1gInSc1	grant
Morrison	Morrison	k1gMnSc1	Morrison
a	a	k8xC	a
Tony	Tony	k1gMnSc1	Tony
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
#	#	kIx~	#
<g/>
676	[number]	k4	676
<g/>
-	-	kIx~	-
<g/>
683	[number]	k4	683
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Batman	Batman	k1gMnSc1	Batman
R.	R.	kA	R.
<g/>
I.	I.	kA	I.
<g/>
P.	P.	kA	P.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
&	&	k?	&
Robin	Robina	k1gFnPc2	Robina
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
znovuzrozený	znovuzrozený	k2eAgMnSc1d1	znovuzrozený
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2013	[number]	k4	2013
/	/	kIx~	/
(	(	kIx(	(
<g/>
Grant	grant	k1gInSc1	grant
Morrison	Morrison	k1gMnSc1	Morrison
a	a	k8xC	a
Frank	Frank	k1gMnSc1	Frank
Quitely	Quitela	k1gFnSc2	Quitela
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
and	and	k?	and
Robin	Robina	k1gFnPc2	Robina
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
/	/	kIx~	/
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
Batman	Batman	k1gMnSc1	Batman
/	/	kIx~	/
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
1	[number]	k4	1
<g/>
:	:	kIx,	:
Tváře	tvář	k1gFnSc2	tvář
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
/	/	kIx~	/
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Tony	Tony	k1gMnSc1	Tony
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
:	:	kIx,	:
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Faces	Faces	k1gInSc1	Faces
of	of	k?	of
Death	Death	k1gInSc1	Death
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
/	/	kIx~	/
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
2	[number]	k4	2
<g/>
:	:	kIx,	:
Zastrašovací	zastrašovací	k2eAgFnSc2d1	zastrašovací
taktiky	taktika	k1gFnSc2	taktika
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
/	/	kIx~	/
(	(	kIx(	(
<g/>
Tony	Tony	k1gMnSc1	Tony
Daniel	Daniel	k1gMnSc1	Daniel
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
a	a	k8xC	a
Annual	Annual	k1gMnSc1	Annual
#	#	kIx~	#
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Scare	Scar	k1gMnSc5	Scar
Tactics	Tactics	k1gInSc4	Tactics
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
/	/	kIx~	/
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
3	[number]	k4	3
<g/>
:	:	kIx,	:
Imperátor	imperátor	k1gMnSc1	imperátor
Penguin	Penguin	k1gMnSc1	Penguin
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Layman	Layman	k1gMnSc1	Layman
<g/>
,	,	kIx,	,
Jason	Jason	k1gMnSc1	Jason
Fabok	Fabok	k1gInSc1	Fabok
a	a	k8xC	a
Andy	Anda	k1gFnPc1	Anda
Clarke	Clarke	k1gFnSc7	Clarke
<g/>
:	:	kIx,	:
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Emperor	Emperor	k1gMnSc1	Emperor
Penguin	Penguin	k1gMnSc1	Penguin
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
/	/	kIx~	/
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc1	comics
4	[number]	k4	4
<g/>
:	:	kIx,	:
Trest	trest	k1gInSc1	trest
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
/	/	kIx~	/
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Layman	Layman	k1gMnSc1	Layman
<g/>
,	,	kIx,	,
Jason	Jason	k1gMnSc1	Jason
Fabok	Fabok	k1gInSc1	Fabok
a	a	k8xC	a
Andy	Anda	k1gFnPc1	Anda
Clarke	Clarke	k1gFnSc7	Clarke
<g/>
:	:	kIx,	:
Detective	Detectiv	k1gInSc5	Detectiv
Comics	comics	k1gInSc4	comics
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
a	a	k8xC	a
Annual	Annual	k1gMnSc1	Annual
#	#	kIx~	#
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Wrath	Wrath	k1gMnSc1	Wrath
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Soví	soví	k2eAgInSc1d1	soví
tribunál	tribunál	k1gInSc1	tribunál
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
/	/	kIx~	/
(	(	kIx(	(
<g/>
Scott	Scott	k2eAgInSc1d1	Scott
Snyder	Snyder	k1gInSc1	Snyder
a	a	k8xC	a
Greg	Greg	k1gInSc1	Greg
Capullo	Capullo	k1gNnSc1	Capullo
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Court	Court	k1gInSc1	Court
of	of	k?	of
Owls	Owls	k1gInSc1	Owls
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Soví	soví	k2eAgNnSc1d1	soví
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
/	/	kIx~	/
(	(	kIx(	(
<g/>
Scott	Scott	k2eAgInSc1d1	Scott
Snyder	Snyder	k1gInSc1	Snyder
a	a	k8xC	a
Greg	Greg	k1gInSc1	Greg
Capullo	Capullo	k1gNnSc1	Capullo
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
a	a	k8xC	a
Annual	Annual	k1gMnSc1	Annual
#	#	kIx~	#
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
City	City	k1gFnSc2	City
of	of	k?	of
Owls	Owls	k1gInSc1	Owls
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
3	[number]	k4	3
<g/>
:	:	kIx,	:
Smrt	smrt	k1gFnSc1	smrt
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
/	/	kIx~	/
(	(	kIx(	(
<g/>
Scott	Scott	k2eAgInSc1d1	Scott
Snyder	Snyder	k1gInSc1	Snyder
<g/>
,	,	kIx,	,
Greg	Greg	k1gInSc1	Greg
Capullo	Capullo	k1gNnSc1	Capullo
a	a	k8xC	a
Jonathan	Jonathan	k1gMnSc1	Jonathan
Glapion	Glapion	k1gInSc1	Glapion
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Death	Death	k1gInSc1	Death
of	of	k?	of
the	the	k?	the
Family	Famila	k1gFnSc2	Famila
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
4	[number]	k4	4
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
nula	nula	k1gFnSc1	nula
-	-	kIx~	-
Tajné	tajný	k2eAgNnSc1d1	tajné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
/	/	kIx~	/
(	(	kIx(	(
<g/>
Scott	Scott	k1gMnSc1	Scott
Snyder	Snyder	k1gMnSc1	Snyder
<g/>
,	,	kIx,	,
Greg	Greg	k1gMnSc1	Greg
Capullo	Capullo	k1gNnSc4	Capullo
a	a	k8xC	a
Danny	Danen	k2eAgFnPc4d1	Danna
Miki	Mik	k1gFnPc4	Mik
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zero	Zero	k1gMnSc1	Zero
Year	Year	k1gMnSc1	Year
-	-	kIx~	-
Secret	Secret	k1gMnSc1	Secret
City	City	k1gFnSc2	City
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
5	[number]	k4	5
<g/>
:	:	kIx,	:
Rok	rok	k1gInSc1	rok
nula	nula	k1gFnSc1	nula
-	-	kIx~	-
Temné	temný	k2eAgNnSc1d1	temné
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
/	/	kIx~	/
(	(	kIx(	(
<g/>
Scott	Scott	k2eAgInSc1d1	Scott
Snyder	Snyder	k1gInSc1	Snyder
a	a	k8xC	a
Greg	Greg	k1gInSc1	Greg
Capullo	Capullo	k1gNnSc1	Capullo
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
33	[number]	k4	33
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zero	Zero	k1gMnSc1	Zero
Year	Year	k1gMnSc1	Year
-	-	kIx~	-
Dark	Dark	k1gInSc1	Dark
City	city	k1gNnSc1	city
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
6	[number]	k4	6
<g/>
:	:	kIx,	:
Hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
směna	směna	k1gFnSc1	směna
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
/	/	kIx~	/
(	(	kIx(	(
<g/>
Scott	Scott	k2eAgInSc1d1	Scott
Snyder	Snyder	k1gInSc1	Snyder
a	a	k8xC	a
Greg	Greg	k1gInSc1	Greg
Capullo	Capullo	k1gNnSc1	Capullo
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g />
.	.	kIx.	.
</s>
<s>
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
34	[number]	k4	34
<g/>
;	;	kIx,	;
Batman	Batman	k1gMnSc1	Batman
Annual	Annual	k1gMnSc1	Annual
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Graveyard	Graveyard	k1gInSc1	Graveyard
Shift	Shift	kA	Shift
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Temné	temný	k2eAgInPc4d1	temný
děsy	děs	k1gInPc4	děs
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
/	/	kIx~	/
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Finch	Finch	k1gMnSc1	Finch
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Jenkins	Jenkins	k1gInSc1	Jenkins
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Friend	Friend	k1gMnSc1	Friend
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Knight	Knight	k2eAgInSc1d1	Knight
Terrors	Terrors	k1gInSc1	Terrors
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
2	[number]	k4	2
<g/>
:	:	kIx,	:
Kruh	kruh	k1gInSc1	kruh
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
/	/	kIx~	/
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Finch	Finch	k1gMnSc1	Finch
a	a	k8xC	a
Gregg	Gregg	k1gMnSc1	Gregg
Hurwitz	Hurwitz	k1gMnSc1	Hurwitz
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
0	[number]	k4	0
a	a	k8xC	a
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Cycle	Cycle	k1gNnPc1	Cycle
of	of	k?	of
Violence	Violenec	k1gMnSc2	Violenec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
3	[number]	k4	3
<g/>
:	:	kIx,	:
Šílený	šílený	k2eAgMnSc1d1	šílený
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
/	/	kIx~	/
(	(	kIx(	(
<g/>
Gregg	Gregg	k1gMnSc1	Gregg
Hurwitz	Hurwitz	k1gMnSc1	Hurwitz
<g/>
,	,	kIx,	,
Ethan	ethan	k1gInSc1	ethan
Van	vana	k1gFnPc2	vana
Sciver	Sciver	k1gInSc1	Sciver
a	a	k8xC	a
Szymon	Szymon	k1gInSc1	Szymon
Kudranski	Kudransk	k1gFnSc2	Kudransk
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
#	#	kIx~	#
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
a	a	k8xC	a
Annual	Annual	k1gMnSc1	Annual
#	#	kIx~	#
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mad	Mad	k1gFnSc1	Mad
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
4	[number]	k4	4
<g/>
:	:	kIx,	:
Proměny	proměna	k1gFnSc2	proměna
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
/	/	kIx~	/
(	(	kIx(	(
<g/>
Gregg	Gregg	k1gMnSc1	Gregg
Hurwitz	Hurwitz	k1gMnSc1	Hurwitz
<g/>
,	,	kIx,	,
Alex	Alex	k1gMnSc1	Alex
Maleev	Maleev	k1gFnSc1	Maleev
<g/>
,	,	kIx,	,
Alberto	Alberta	k1gFnSc5	Alberta
Ponticelli	Ponticell	k1gMnSc6	Ponticell
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Ethan	ethan	k1gInSc1	ethan
Van	vana	k1gFnPc2	vana
Sciver	Sciver	k1gMnSc1	Sciver
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k1gMnSc1	Knight
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
#	#	kIx~	#
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Clay	Cla	k2eAgFnPc1d1	Cla
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Země	země	k1gFnSc1	země
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
/	/	kIx~	/
(	(	kIx(	(
<g/>
Geoff	Geoff	k1gInSc1	Geoff
Johns	Johnsa	k1gFnPc2	Johnsa
a	a	k8xC	a
Gary	Gara	k1gFnSc2	Gara
Frank	Frank	k1gMnSc1	Frank
<g/>
:	:	kIx,	:
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Earth	Earth	k1gMnSc1	Earth
One	One	k1gMnSc1	One
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
populárním	populární	k2eAgMnSc6d1	populární
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
Batman	Batman	k1gMnSc1	Batman
<g/>
.	.	kIx.	.
</s>
<s>
Světovou	světový	k2eAgFnSc4d1	světová
proslulost	proslulost	k1gFnSc4	proslulost
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
filmu	film	k1gInSc3	film
Batman	Batman	k1gMnSc1	Batman
režiséra	režisér	k1gMnSc4	režisér
Tima	Timus	k1gMnSc4	Timus
Burtona	Burton	k1gMnSc4	Burton
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
následován	následovat	k5eAaImNgInS	následovat
filmem	film	k1gInSc7	film
Batman	Batman	k1gMnSc1	Batman
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
filmy	film	k1gInPc4	film
Joela	Joel	k1gMnSc4	Joel
Schumachera	Schumacher	k1gMnSc4	Schumacher
Batman	Batman	k1gMnSc1	Batman
navždy	navždy	k6eAd1	navždy
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
a	a	k8xC	a
Batman	Batman	k1gMnSc1	Batman
a	a	k8xC	a
Robin	robin	k2eAgMnSc1d1	robin
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledně	posledně	k6eAd1	posledně
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
uváděn	uvádět	k5eAaImNgInS	uvádět
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
filmem	film	k1gInSc7	film
Catwoman	Catwoman	k1gMnSc1	Catwoman
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejhorších	zlý	k2eAgFnPc2d3	nejhorší
komiksových	komiksový	k2eAgFnPc2d1	komiksová
adaptací	adaptace	k1gFnPc2	adaptace
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2005	[number]	k4	2005
a	a	k8xC	a
2012	[number]	k4	2012
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
trilogie	trilogie	k1gFnSc1	trilogie
Christophera	Christopher	k1gMnSc2	Christopher
Nolana	Nolan	k1gMnSc2	Nolan
filmů	film	k1gInPc2	film
Batman	Batman	k1gMnSc1	Batman
začíná	začínat	k5eAaImIp3nS	začínat
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
povstal	povstat	k5eAaPmAgMnS	povstat
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1943	[number]	k4	1943
-	-	kIx~	-
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
filmový	filmový	k2eAgInSc1d1	filmový
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Lambert	Lambert	k1gMnSc1	Lambert
<g />
.	.	kIx.	.
</s>
<s>
Hillyer	Hillyer	k1gInSc1	Hillyer
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Lewis	Lewis	k1gFnSc2	Lewis
Wilson	Wilson	k1gInSc1	Wilson
1949	[number]	k4	1949
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
and	and	k?	and
Robin	Robina	k1gFnPc2	Robina
-	-	kIx~	-
filmový	filmový	k2eAgInSc1d1	filmový
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Spencer	Spencer	k1gMnSc1	Spencer
Gordon	Gordon	k1gMnSc1	Gordon
Bennet	Bennet	k1gMnSc1	Bennet
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Robert	Robert	k1gMnSc1	Robert
Lowery	Lowera	k1gFnSc2	Lowera
1966	[number]	k4	1966
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
film	film	k1gInSc1	film
odvozený	odvozený	k2eAgInSc1d1	odvozený
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
Leslie	Leslie	k1gFnSc2	Leslie
H.	H.	kA	H.
Martinson	Martinson	k1gInSc1	Martinson
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g />
.	.	kIx.	.
</s>
<s>
Adam	Adam	k1gMnSc1	Adam
West	West	k1gMnSc1	West
1989	[number]	k4	1989
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Michael	Michael	k1gMnSc1	Michael
Keaton	Keaton	k1gInSc4	Keaton
1992	[number]	k4	1992
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Tim	Tim	k?	Tim
Burton	Burton	k1gInSc1	Burton
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Michael	Michael	k1gMnSc1	Michael
Keaton	Keaton	k1gInSc4	Keaton
1995	[number]	k4	1995
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
navždy	navždy	k6eAd1	navždy
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Joel	Joela	k1gFnPc2	Joela
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Val	val	k1gInSc4	val
Kilmer	Kilmer	k1gInSc1	Kilmer
1997	[number]	k4	1997
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Robin	robin	k2eAgInSc1d1	robin
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Joel	Joela	k1gFnPc2	Joela
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
George	Georg	k1gMnSc2	Georg
Clooney	Cloonea	k1gMnSc2	Cloonea
2005	[number]	k4	2005
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
začíná	začínat	k5eAaImIp3nS	začínat
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Christopher	Christophra	k1gFnPc2	Christophra
Nolan	Nolana	k1gFnPc2	Nolana
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Christian	Christian	k1gMnSc1	Christian
Bale	bal	k1gInSc5	bal
2008	[number]	k4	2008
-	-	kIx~	-
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Christopher	Christophra	k1gFnPc2	Christophra
Nolan	Nolana	k1gFnPc2	Nolana
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Christian	Christian	k1gMnSc1	Christian
Bale	bal	k1gInSc5	bal
2012	[number]	k4	2012
-	-	kIx~	-
Temný	temný	k2eAgMnSc1d1	temný
rytíř	rytíř	k1gMnSc1	rytíř
povstal	povstat	k5eAaPmAgMnS	povstat
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
<g />
.	.	kIx.	.
</s>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Nolan	Nolany	k1gInPc2	Nolany
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Christian	Christian	k1gMnSc1	Christian
Bale	bal	k1gInSc5	bal
2016	[number]	k4	2016
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
v	v	k7c6	v
Superman	superman	k1gMnSc1	superman
<g/>
:	:	kIx,	:
Úsvit	úsvit	k1gInSc1	úsvit
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
-	-	kIx~	-
režie	režie	k1gFnSc1	režie
Zack	Zack	k1gMnSc1	Zack
Snyder	Snyder	k1gMnSc1	Snyder
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Ben	Ben	k1gInSc4	Ben
Affleck	Affleck	k1gInSc1	Affleck
1966	[number]	k4	1966
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
120	[number]	k4	120
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Adam	Adam	k1gMnSc1	Adam
West	West	k1gMnSc1	West
2002	[number]	k4	2002
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
-	-	kIx~	-
Birds	Birds	k1gInSc1	Birds
of	of	k?	of
Prey	Prea	k1gFnSc2	Prea
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Gotham	Gotham	k1gInSc4	Gotham
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
Batmana	Batman	k1gMnSc4	Batman
neznámý	známý	k2eNgMnSc1d1	neznámý
herec	herec	k1gMnSc1	herec
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
dosud	dosud	k6eAd1	dosud
-	-	kIx~	-
Gotham	Gotham	k1gInSc1	Gotham
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Gotham	Gotham	k1gInSc4	Gotham
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
v	v	k7c6	v
roli	role	k1gFnSc6	role
Bruce	Bruce	k1gMnSc1	Bruce
Waynea	Waynea	k1gMnSc1	Waynea
David	David	k1gMnSc1	David
Mazouz	Mazouz	k1gMnSc1	Mazouz
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
69	[number]	k4	69
-	-	kIx~	-
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
Superman	superman	k1gMnSc1	superman
Hour	Hour	k1gMnSc1	Hour
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
1977	[number]	k4	1977
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
-	-	kIx~	-
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Animated	Animated	k1gMnSc1	Animated
Series	Series	k1gMnSc1	Series
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
1993	[number]	k4	1993
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Mask	Mask	k1gMnSc1	Mask
of	of	k?	of
the	the	k?	the
Phantasm	Phantasm	k1gInSc1	Phantasm
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
-	-	kIx~	-
The	The	k1gMnSc1	The
New	New	k1gMnSc1	New
Batman	Batman	k1gMnSc1	Batman
Adventures	Adventures	k1gMnSc1	Adventures
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
1998	[number]	k4	1998
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
&	&	k?	&
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Freeze	Freeze	k1gFnSc1	Freeze
<g/>
:	:	kIx,	:
SubZero	SubZero	k1gNnSc1	SubZero
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
Beyond	Beyond	k1gMnSc1	Beyond
/	/	kIx~	/
(	(	kIx(	(
<g/>
Batman	Batman	k1gMnSc1	Batman
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
)	)	kIx)	)
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
2000	[number]	k4	2000
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
Beyond	Beyond	k1gMnSc1	Beyond
<g/>
:	:	kIx,	:
Return	Return	k1gMnSc1	Return
of	of	k?	of
the	the	k?	the
Joker	Joker	k1gInSc1	Joker
2003	[number]	k4	2003
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Mystery	Myster	k1gInPc1	Myster
of	of	k?	of
the	the	k?	the
Batwoman	Batwoman	k1gMnSc1	Batwoman
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
-	-	kIx~	-
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
/	/	kIx~	/
(	(	kIx(	(
<g/>
Batman	Batman	k1gMnSc1	Batman
vítězí	vítězit	k5eAaImIp3nS	vítězit
<g/>
)	)	kIx)	)
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
2005	[number]	k4	2005
-	-	kIx~	-
The	The	k1gMnSc1	The
Batman	Batman	k1gMnSc1	Batman
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Dracula	Dracula	k1gFnSc1	Dracula
2008	[number]	k4	2008
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Gotham	Gotham	k1gInSc1	Gotham
Knight	Knight	k1gInSc1	Knight
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Brave	brav	k1gInSc5	brav
and	and	k?	and
the	the	k?	the
Bold	Bold	k1gInSc1	Bold
/	/	kIx~	/
(	(	kIx(	(
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Odvážný	odvážný	k2eAgMnSc1d1	odvážný
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
)	)	kIx)	)
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
2009	[number]	k4	2009
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
<g/>
/	/	kIx~	/
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Public	publicum	k1gNnPc2	publicum
Enemies	Enemiesa	k1gFnPc2	Enemiesa
2010	[number]	k4	2010
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Under	Under	k1gMnSc1	Under
the	the	k?	the
Red	Red	k1gMnSc1	Red
Hood	Hood	k1gMnSc1	Hood
2010	[number]	k4	2010
-	-	kIx~	-
Superman	superman	k1gMnSc1	superman
<g/>
/	/	kIx~	/
<g/>
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Apocalypse	Apocalypse	k1gFnSc1	Apocalypse
2011	[number]	k4	2011
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Year	Year	k1gMnSc1	Year
One	One	k1gMnSc1	One
2012	[number]	k4	2012
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k2eAgInSc4d1	Knight
Returns	Returns	k1gInSc4	Returns
Part	part	k1gInSc1	part
1	[number]	k4	1
2013	[number]	k4	2013
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Dark	Dark	k1gMnSc1	Dark
Knight	Knight	k2eAgInSc4d1	Knight
Returns	Returns	k1gInSc4	Returns
Part	part	k1gInSc1	part
2	[number]	k4	2
2013	[number]	k4	2013
-	-	kIx~	-
LEGO	lego	k1gNnSc1	lego
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Movie	Movie	k1gFnSc2	Movie
-	-	kIx~	-
DC	DC	kA	DC
Superheroes	Superheroes	k1gMnSc1	Superheroes
Unite	Unit	k1gInSc5	Unit
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
dodnes	dodnes	k6eAd1	dodnes
-	-	kIx~	-
Beware	Bewar	k1gMnSc5	Bewar
the	the	k?	the
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
seriál	seriál	k1gInSc1	seriál
2014	[number]	k4	2014
-	-	kIx~	-
Son	son	k1gInSc1	son
of	of	k?	of
Batman	Batman	k1gMnSc1	Batman
2014	[number]	k4	2014
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Assault	Assault	k1gMnSc1	Assault
on	on	k3xPp3gMnSc1	on
Arkham	Arkham	k1gInSc1	Arkham
2015	[number]	k4	2015
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Robin	robin	k2eAgMnSc1d1	robin
2015	[number]	k4	2015
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
Unlimited	Unlimited	k1gMnSc1	Unlimited
<g/>
:	:	kIx,	:
Animal	animal	k1gMnSc1	animal
Instincts	Instincts	k1gInSc4	Instincts
2015	[number]	k4	2015
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
Unlimited	Unlimited	k1gMnSc1	Unlimited
<g/>
:	:	kIx,	:
Monster	monstrum	k1gNnPc2	monstrum
Mayhem	Mayh	k1gInSc7	Mayh
2016	[number]	k4	2016
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
Bad	Bad	k1gFnSc1	Bad
Blood	Blood	k1gInSc1	Blood
2016	[number]	k4	2016
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Killing	Killing	k1gInSc1	Killing
Joke	Jok	k1gInSc2	Jok
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Batman	Batman	k1gMnSc1	Batman
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
ComixZone	ComixZon	k1gInSc5	ComixZon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Batman	Batman	k1gMnSc1	Batman
-	-	kIx~	-
detailnější	detailní	k2eAgFnPc1d2	detailnější
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
postavě	postava	k1gFnSc6	postava
Batman	Batman	k1gMnSc1	Batman
na	na	k7c4	na
Comixarium	Comixarium	k1gNnSc4	Comixarium
-	-	kIx~	-
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
postavě	postava	k1gFnSc6	postava
-	-	kIx~	-
neplatný	platný	k2eNgInSc1d1	neplatný
odkaz	odkaz	k1gInSc1	odkaz
!	!	kIx.	!
</s>
<s>
Batman	Batman	k1gMnSc1	Batman
na	na	k7c4	na
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
postavě	postava	k1gFnSc6	postava
</s>
