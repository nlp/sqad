<s>
Jura	jura	k1gFnSc1	jura
je	být	k5eAaImIp3nS	být
geologická	geologický	k2eAgFnSc1d1	geologická
perioda	perioda	k1gFnSc1	perioda
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
éry	éra	k1gFnSc2	éra
mezozoika	mezozoikum	k1gNnSc2	mezozoikum
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
začátek	začátek	k1gInSc1	začátek
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
200	[number]	k4	200
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
konec	konec	k1gInSc1	konec
před	před	k7c7	před
145	[number]	k4	145
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
pohoří	pohoří	k1gNnSc2	pohoří
Jura	jura	k1gFnSc1	jura
(	(	kIx(	(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
A.	A.	kA	A.
von	von	k1gInSc1	von
Humbolt	Humbolt	k1gInSc1	Humbolt
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
útvar	útvar	k1gInSc1	útvar
byl	být	k5eAaImAgInS	být
definován	definován	k2eAgInSc1d1	definován
roku	rok	k1gInSc2	rok
1829	[number]	k4	1829
A.	A.	kA	A.
Boném	Boný	k2eAgInSc6d1	Boný
<g/>
.	.	kIx.	.
</s>
<s>
Juru	jura	k1gFnSc4	jura
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
transgrese	transgrese	k1gFnSc1	transgrese
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pokrývalo	pokrývat	k5eAaImAgNnS	pokrývat
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
dnešní	dnešní	k2eAgFnSc2d1	dnešní
známé	známý	k2eAgFnSc2d1	známá
souše	souš	k1gFnSc2	souš
<g/>
.	.	kIx.	.
</s>
<s>
Moře	moře	k1gNnSc1	moře
pokrývalo	pokrývat	k5eAaImAgNnS	pokrývat
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
spodní	spodní	k2eAgInPc4d1	spodní
(	(	kIx(	(
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
175	[number]	k4	175
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
(	(	kIx(	(
<g/>
175	[number]	k4	175
<g/>
-	-	kIx~	-
<g/>
160	[number]	k4	160
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
svrchní	svrchní	k2eAgFnSc1d1	svrchní
(	(	kIx(	(
<g/>
160	[number]	k4	160
<g/>
-	-	kIx~	-
<g/>
145	[number]	k4	145
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
L.v.	L.v.	k1gFnSc2	L.v.
<g/>
Buch	buch	k1gInSc1	buch
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
zavedl	zavést	k5eAaPmAgInS	zavést
dle	dle	k7c2	dle
převládající	převládající	k2eAgFnSc2d1	převládající
barvy	barva	k1gFnSc2	barva
hornin	hornina	k1gFnPc2	hornina
termíny	termín	k1gInPc1	termín
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
hnědá	hnědý	k2eAgFnSc1d1	hnědá
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
jura	jura	k1gFnSc1	jura
<g/>
.	.	kIx.	.
</s>
<s>
A.	A.	kA	A.
Oppel	Oppel	k1gMnSc1	Oppel
pak	pak	k6eAd1	pak
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
oddělení	oddělení	k1gNnPc4	oddělení
nazval	nazvat	k5eAaPmAgInS	nazvat
lias	lias	k1gInSc1	lias
<g/>
,	,	kIx,	,
dogger	dogger	k1gInSc1	dogger
a	a	k8xC	a
malm	malm	k1gInSc1	malm
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
názvy	název	k1gInPc1	název
byly	být	k5eAaImAgInP	být
odvozeny	odvodit	k5eAaPmNgInP	odvodit
od	od	k7c2	od
označení	označení	k1gNnSc2	označení
hornin	hornina	k1gFnPc2	hornina
používaných	používaný	k2eAgFnPc2d1	používaná
dělníky	dělník	k1gMnPc7	dělník
v	v	k7c6	v
lomech	lom	k1gInPc6	lom
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Detailní	detailní	k2eAgNnSc1d1	detailní
členění	členění	k1gNnSc1	členění
jury	jura	k1gFnSc2	jura
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
odpovídajících	odpovídající	k2eAgFnPc2d1	odpovídající
regionálních	regionální	k2eAgFnPc2d1	regionální
jednotek	jednotka	k1gFnPc2	jednotka
uvádí	uvádět	k5eAaImIp3nS	uvádět
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Hojní	hojnit	k5eAaImIp3nP	hojnit
byli	být	k5eAaImAgMnP	být
prvoci	prvok	k1gMnPc1	prvok
(	(	kIx(	(
<g/>
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
Calpionella	Calpionella	k1gFnSc1	Calpionella
alpina	alpinum	k1gNnSc2	alpinum
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
juře	jura	k1gFnSc6	jura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
koráli	korál	k1gMnPc1	korál
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgMnSc1d1	vytvářející
v	v	k7c6	v
teplých	teplý	k2eAgNnPc6d1	teplé
mořích	moře	k1gNnPc6	moře
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
útesy	útes	k1gInPc4	útes
<g/>
,	,	kIx,	,
mechovky	mechovka	k1gFnPc4	mechovka
<g/>
,	,	kIx,	,
měkkýši	měkkýš	k1gMnPc1	měkkýš
(	(	kIx(	(
<g/>
Trigonia	Trigonium	k1gNnSc2	Trigonium
<g/>
,	,	kIx,	,
Posidonia	Posidonium	k1gNnSc2	Posidonium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
byl	být	k5eAaImAgInS	být
rozvoj	rozvoj	k1gInSc1	rozvoj
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
amonitů	amonit	k1gInPc2	amonit
(	(	kIx(	(
<g/>
vůdčí	vůdčí	k2eAgFnPc4d1	vůdčí
fosílie	fosílie	k1gFnPc4	fosílie
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
příbuzných	příbuzný	k1gMnPc2	příbuzný
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
belemnitů	belemnit	k1gInPc2	belemnit
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
také	také	k9	také
zástupci	zástupce	k1gMnPc1	zástupce
moderních	moderní	k2eAgFnPc2d1	moderní
skupin	skupina	k1gFnPc2	skupina
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
blechy	blecha	k1gFnPc1	blecha
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Pseudopulex	Pseudopulex	k1gInSc1	Pseudopulex
<g/>
)	)	kIx)	)
a	a	k8xC	a
snad	snad	k9	snad
i	i	k9	i
klíšťata	klíště	k1gNnPc1	klíště
a	a	k8xC	a
vši	veš	k1gFnPc1	veš
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgFnPc1d2	častější
jsou	být	k5eAaImIp3nP	být
kosticovité	kosticovitý	k2eAgFnPc1d1	kosticovitý
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
Teleostei	Teleoste	k1gFnPc1	Teleoste
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známy	znám	k2eAgInPc1d1	znám
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
pravé	pravý	k2eAgFnPc4d1	pravá
žáby	žába	k1gFnPc4	žába
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
rybou	ryba	k1gFnSc7	ryba
vůbec	vůbec	k9	vůbec
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
rod	rod	k1gInSc4	rod
Leedsichthys	Leedsichthysa	k1gFnPc2	Leedsichthysa
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc1d1	žijící
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
juře	jura	k1gFnSc6	jura
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
také	také	k9	také
praví	pravý	k2eAgMnPc1d1	pravý
krokodýli	krokodýl	k1gMnPc1	krokodýl
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnPc1d1	obývající
nejen	nejen	k6eAd1	nejen
sladké	sladký	k2eAgFnPc1d1	sladká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
nálezy	nález	k1gInPc1	nález
primitivních	primitivní	k2eAgFnPc2d1	primitivní
želv	želva	k1gFnPc2	želva
s	s	k7c7	s
tělem	tělo	k1gNnSc7	tělo
již	již	k6eAd1	již
částečně	částečně	k6eAd1	částečně
pokrytým	pokrytý	k2eAgInSc7d1	pokrytý
krunýřem	krunýř	k1gInSc7	krunýř
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
vývoj	vývoj	k1gInSc1	vývoj
drobných	drobný	k2eAgMnPc2d1	drobný
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
pomalu	pomalu	k6eAd1	pomalu
začínají	začínat	k5eAaImIp3nP	začínat
rozrůzňovat	rozrůzňovat	k5eAaImF	rozrůzňovat
a	a	k8xC	a
specializovat	specializovat	k5eAaBmF	specializovat
<g/>
.	.	kIx.	.
</s>
<s>
Teplá	Teplá	k1gFnSc1	Teplá
moře	moře	k1gNnSc1	moře
obývají	obývat	k5eAaImIp3nP	obývat
žraloci	žralok	k1gMnPc1	žralok
a	a	k8xC	a
rejnoci	rejnok	k1gMnPc1	rejnok
ve	v	k7c6	v
formách	forma	k1gFnPc6	forma
velmi	velmi	k6eAd1	velmi
podobných	podobný	k2eAgMnPc2d1	podobný
současným	současný	k2eAgMnPc3d1	současný
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
období	období	k1gNnSc3	období
však	však	k8xC	však
stále	stále	k6eAd1	stále
dominují	dominovat	k5eAaImIp3nP	dominovat
zejména	zejména	k9	zejména
plazi	plaz	k1gMnPc1	plaz
<g/>
.	.	kIx.	.
</s>
<s>
Vody	voda	k1gFnPc1	voda
obývají	obývat	k5eAaImIp3nP	obývat
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
ichtyosaurů	ichtyosaurus	k1gMnPc2	ichtyosaurus
(	(	kIx(	(
<g/>
ryboještěři	ryboještěr	k1gMnPc1	ryboještěr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
lokalita	lokalita	k1gFnSc1	lokalita
Holzmaden	Holzmaden	k2eAgInSc1d1	Holzmaden
v	v	k7c6	v
SRN	SRN	kA	SRN
<g/>
)	)	kIx)	)
a	a	k8xC	a
dravých	dravý	k2eAgMnPc2d1	dravý
dlouhokrkých	dlouhokrký	k2eAgMnPc2d1	dlouhokrký
plesiosaurů	plesiosaur	k1gMnPc2	plesiosaur
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
souši	souš	k1gFnSc6	souš
již	již	k6eAd1	již
bezkonkurenčně	bezkonkurenčně	k6eAd1	bezkonkurenčně
dominují	dominovat	k5eAaImIp3nP	dominovat
různé	různý	k2eAgFnPc4d1	různá
skupiny	skupina	k1gFnPc4	skupina
dravých	dravý	k2eAgMnPc2d1	dravý
i	i	k8xC	i
býložravých	býložravý	k2eAgMnPc2d1	býložravý
neptačích	ptačí	k2eNgMnPc2d1	neptačí
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
obřích	obří	k2eAgInPc2d1	obří
sauropodů	sauropod	k1gInPc2	sauropod
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgInSc1d3	veliký
byl	být	k5eAaImAgInS	být
možná	možná	k9	možná
rod	rod	k1gInSc1	rod
Amphicoelias	Amphicoeliasa	k1gFnPc2	Amphicoeliasa
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
ptakoještěři	ptakoještěr	k1gMnPc1	ptakoještěr
(	(	kIx(	(
<g/>
Pterosauria	Pterosaurium	k1gNnSc2	Pterosaurium
-	-	kIx~	-
např.	např.	kA	např.
Pterodactylus	Pterodactylus	k1gInSc4	Pterodactylus
<g/>
,	,	kIx,	,
Dimorphodon	Dimorphodon	k1gNnSc4	Dimorphodon
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
také	také	k9	také
nálezy	nález	k1gInPc1	nález
prvních	první	k4xOgMnPc2	první
"	"	kIx"	"
<g/>
praptáků	prapták	k1gMnPc2	prapták
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Archaeopteryx	Archaeopteryx	k1gInSc1	Archaeopteryx
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
u	u	k7c2	u
Solnhofenu	Solnhofen	k1gInSc2	Solnhofen
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
žil	žít	k5eAaImAgMnS	žít
ještě	ještě	k9	ještě
o	o	k7c4	o
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
opeřený	opeřený	k2eAgInSc4d1	opeřený
troodontid	troodontid	k1gInSc4	troodontid
rodu	rod	k1gInSc2	rod
Anchiornis	Anchiornis	k1gFnSc2	Anchiornis
(	(	kIx(	(
<g/>
asi	asi	k9	asi
před	před	k7c7	před
160	[number]	k4	160
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jura	jura	k1gFnSc1	jura
je	být	k5eAaImIp3nS	být
vrcholným	vrcholný	k2eAgNnSc7d1	vrcholné
obdobím	období	k1gNnSc7	období
rostlin	rostlina	k1gFnPc2	rostlina
mezofytika	mezofytikum	k1gNnSc2	mezofytikum
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
dvě	dva	k4xCgFnPc4	dva
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
indoevropskou	indoevropský	k2eAgFnSc4d1	indoevropská
<g/>
,	,	kIx,	,
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
tropickým	tropický	k2eAgNnSc7d1	tropické
a	a	k8xC	a
subtropickým	subtropický	k2eAgNnSc7d1	subtropické
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
klimatem	klima	k1gNnSc7	klima
a	a	k8xC	a
sibiřskou	sibiřský	k2eAgFnSc7d1	sibiřská
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
dnešnímu	dnešní	k2eAgNnSc3d1	dnešní
mírnému	mírný	k2eAgNnSc3d1	mírné
pásmu	pásmo	k1gNnSc3	pásmo
(	(	kIx(	(
<g/>
s	s	k7c7	s
uhelnými	uhelný	k2eAgFnPc7d1	uhelná
pánvemi	pánev	k1gFnPc7	pánev
známými	známý	k2eAgFnPc7d1	známá
z	z	k7c2	z
Aljašky	Aljaška	k1gFnSc2	Aljaška
a	a	k8xC	a
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převládají	převládat	k5eAaImIp3nP	převládat
cykasovité	cykasovitý	k2eAgFnPc1d1	cykasovitý
rostliny	rostlina	k1gFnPc1	rostlina
(	(	kIx(	(
<g/>
Zamites	Zamites	k1gInSc1	Zamites
<g/>
,	,	kIx,	,
Nilssonia	Nilssonium	k1gNnPc1	Nilssonium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
benetitové	benetit	k1gMnPc1	benetit
(	(	kIx(	(
<g/>
Bennettitales	Bennettitales	k1gMnSc1	Bennettitales
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
ginkgovitých	ginkgovití	k1gMnPc2	ginkgovití
<g/>
,	,	kIx,	,
hojní	hojný	k2eAgMnPc1d1	hojný
jsou	být	k5eAaImIp3nP	být
předchůdci	předchůdce	k1gMnPc1	předchůdce
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
první	první	k4xOgFnPc1	první
krytosemenné	krytosemenný	k2eAgFnPc1d1	krytosemenná
rostliny	rostlina	k1gFnPc1	rostlina
a	a	k8xC	a
mořské	mořský	k2eAgFnPc1d1	mořská
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
ruduchy	ruducha	k1gFnPc1	ruducha
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
triasu	trias	k1gInSc2	trias
a	a	k8xC	a
jury	jura	k1gFnSc2	jura
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tektonicky	tektonicky	k6eAd1	tektonicky
nevýraznému	výrazný	k2eNgNnSc3d1	nevýrazné
vrásnění	vrásnění	k1gNnSc3	vrásnění
starokimerské	starokimerský	k2eAgFnSc2d1	starokimerský
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jury	jura	k1gFnSc2	jura
začíná	začínat	k5eAaImIp3nS	začínat
rozpad	rozpad	k1gInSc1	rozpad
Gondwany	Gondwana	k1gFnSc2	Gondwana
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
světadíly	světadíl	k1gInPc4	světadíl
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
oceánického	oceánický	k2eAgInSc2d1	oceánický
hřbetu	hřbet	k1gInSc2	hřbet
se	se	k3xPyFc4	se
rozevírá	rozevírat	k5eAaImIp3nS	rozevírat
Atlantický	atlantický	k2eAgInSc1d1	atlantický
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
se	se	k3xPyFc4	se
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
od	od	k7c2	od
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
tektonickým	tektonický	k2eAgInSc7d1	tektonický
prvkem	prvek	k1gInSc7	prvek
je	být	k5eAaImIp3nS	být
vrásnění	vrásnění	k1gNnSc1	vrásnění
mladokimerské	mladokimerský	k2eAgFnSc2d1	mladokimerský
fáze	fáze	k1gFnSc2	fáze
na	na	k7c6	na
konci	konec	k1gInSc6	konec
jury	jura	k1gFnSc2	jura
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
regresi	regrese	k1gFnSc3	regrese
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
vrásnění	vrásnění	k1gNnSc1	vrásnění
předznamenává	předznamenávat	k5eAaImIp3nS	předznamenávat
vznik	vznik	k1gInSc1	vznik
pásma	pásmo	k1gNnSc2	pásmo
pacifických	pacifický	k2eAgNnPc2d1	pacifické
pohoří	pohoří	k1gNnPc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sev	sev	k?	sev
<g/>
.	.	kIx.	.
</s>
<s>
Americe	Amerika	k1gFnSc3	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
nevadské	nevadský	k2eAgNnSc1d1	nevadské
vrásnění	vrásnění	k1gNnSc1	vrásnění
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
obrovských	obrovský	k2eAgInPc2d1	obrovský
granitových	granitový	k2eAgInPc2d1	granitový
batolitů	batolit	k1gInPc2	batolit
(	(	kIx(	(
<g/>
svrchní	svrchní	k2eAgFnSc1d1	svrchní
jura-spodní	jurapodní	k2eAgFnSc1d1	jura-spodní
křída	křída	k1gFnSc1	křída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
četný	četný	k2eAgInSc1d1	četný
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
produkuje	produkovat	k5eAaImIp3nS	produkovat
lávové	lávový	k2eAgInPc4d1	lávový
příkrovy	příkrov	k1gInPc4	příkrov
mocné	mocný	k2eAgInPc4d1	mocný
až	až	k9	až
6	[number]	k4	6
km	km	kA	km
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výlevy	výlev	k1gInPc1	výlev
láv	láva	k1gFnPc2	láva
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
i	i	k9	i
z	z	k7c2	z
formace	formace	k1gFnSc2	formace
Karroo	Karroo	k6eAd1	Karroo
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
a	a	k8xC	a
na	na	k7c6	na
Kavkazu	Kavkaz	k1gInSc6	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Vydělují	vydělovat	k5eAaImIp3nP	vydělovat
se	se	k3xPyFc4	se
tři	tři	k4xCgFnPc1	tři
oblasti	oblast	k1gFnPc1	oblast
<g/>
:	:	kIx,	:
geosynklinální	geosynklinální	k2eAgFnSc1d1	geosynklinální
mediteránní	mediteránní	k2eAgFnSc1d1	mediteránní
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
formováním	formování	k1gNnSc7	formování
alpsko-karpatského	alpskoarpatský	k2eAgInSc2d1	alpsko-karpatský
oblouku	oblouk	k1gInSc2	oblouk
<g/>
,	,	kIx,	,
zastoupená	zastoupený	k2eAgFnSc1d1	zastoupená
vápenci	vápenec	k1gInPc7	vápenec
<g/>
,	,	kIx,	,
rohovci	rohovec	k1gInPc7	rohovec
a	a	k8xC	a
radiolarity	radiolarit	k1gInPc1	radiolarit
epikontinentální	epikontinentální	k2eAgInPc1d1	epikontinentální
oblast	oblast	k1gFnSc4	oblast
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
s	s	k7c7	s
pánvemi	pánev	k1gFnPc7	pánev
anglo-pařížskou	angloařížský	k2eAgFnSc7d1	anglo-pařížský
<g/>
,	,	kIx,	,
akvitánskou	akvitánský	k2eAgFnSc7d1	akvitánský
<g/>
,	,	kIx,	,
švábsko-franckou	švábskorancký	k2eAgFnSc7d1	švábsko-francký
a	a	k8xC	a
rýnskou	rýnský	k2eAgFnSc7d1	Rýnská
s	s	k7c7	s
pestrým	pestrý	k2eAgNnSc7d1	pestré
horninovým	horninový	k2eAgNnSc7d1	horninové
složením	složení	k1gNnSc7	složení
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
slepence	slepenec	k1gInPc1	slepenec
<g/>
,	,	kIx,	,
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
břidlice	břidlice	k1gFnPc1	břidlice
<g/>
,	,	kIx,	,
slíny	slín	k1gInPc1	slín
<g/>
,	,	kIx,	,
slínovce	slínovec	k1gInPc1	slínovec
<g/>
,	,	kIx,	,
vápence	vápenec	k1gInPc1	vápenec
<g/>
)	)	kIx)	)
boreální	boreální	k2eAgFnSc1d1	boreální
oblast	oblast	k1gFnSc1	oblast
severu	sever	k1gInSc2	sever
Evropy	Evropa	k1gFnSc2	Evropa
Denudanční	Denudanční	k2eAgInPc1d1	Denudanční
zbytky	zbytek	k1gInPc1	zbytek
stáří	stáří	k1gNnSc2	stáří
kelloway-kimeridž	kellowayimeridž	k1gFnSc4	kelloway-kimeridž
s	s	k7c7	s
bazálními	bazální	k2eAgMnPc7d1	bazální
klastiky	klastik	k1gMnPc7	klastik
(	(	kIx(	(
<g/>
písky	písek	k1gInPc7	písek
<g/>
)	)	kIx)	)
a	a	k8xC	a
dolomitickými	dolomitický	k2eAgInPc7d1	dolomitický
vápenci	vápenec	k1gInPc7	vápenec
o	o	k7c6	o
mocnosti	mocnost	k1gFnSc6	mocnost
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
m	m	kA	m
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
podél	podél	k7c2	podél
lužického	lužický	k2eAgInSc2d1	lužický
zlomu	zlom	k1gInSc2	zlom
(	(	kIx(	(
<g/>
Krásná	krásný	k2eAgFnSc1d1	krásná
Lípa	lípa	k1gFnSc1	lípa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
(	(	kIx(	(
<g/>
Olomučany	Olomučan	k1gMnPc4	Olomučan
<g/>
,	,	kIx,	,
Rudice	Rudice	k1gFnPc4	Rudice
<g/>
,	,	kIx,	,
Babice	Babice	k1gFnPc4	Babice
<g/>
)	)	kIx)	)
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
Hády	Hádes	k1gMnPc4	Hádes
<g/>
,	,	kIx,	,
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
Švédské	švédský	k2eAgFnPc1d1	švédská
šance	šance	k1gFnPc1	šance
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
stáří	stáří	k1gNnSc2	stáří
kelloway-oxford	kellowayxford	k6eAd1	kelloway-oxford
zastoupeny	zastoupen	k2eAgInPc4d1	zastoupen
bazálními	bazální	k2eAgInPc7d1	bazální
slepenci	slepenec	k1gInPc7	slepenec
<g/>
,	,	kIx,	,
písky	písek	k1gInPc7	písek
a	a	k8xC	a
jíly	jíl	k1gInPc7	jíl
v	v	k7c6	v
nadloží	nadloží	k1gNnSc6	nadloží
s	s	k7c7	s
vápenci	vápenec	k1gInPc7	vápenec
(	(	kIx(	(
<g/>
největší	veliký	k2eAgFnSc4d3	veliký
mocnost	mocnost	k1gFnSc4	mocnost
134	[number]	k4	134
známa	známo	k1gNnSc2	známo
z	z	k7c2	z
vrtu	vrt	k1gInSc2	vrt
Slatina	slatina	k1gFnSc1	slatina
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
rozsah	rozsah	k1gInSc4	rozsah
má	mít	k5eAaImIp3nS	mít
jura	jura	k1gFnSc1	jura
v	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
neogénu	neogén	k1gInSc2	neogén
čelní	čelní	k2eAgFnSc2d1	čelní
hlubiny	hlubina	k1gFnSc2	hlubina
a	a	k8xC	a
flyšového	flyšový	k2eAgNnSc2d1	flyšový
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Lias-dogger	Liasogger	k1gInSc4	Lias-dogger
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
diváckými	divácký	k2eAgFnPc7d1	divácká
vrstvami	vrstva	k1gFnPc7	vrstva
(	(	kIx(	(
<g/>
lokálně	lokálně	k6eAd1	lokálně
s	s	k7c7	s
uhelnou	uhelný	k2eAgFnSc7d1	uhelná
sedimentací	sedimentace	k1gFnSc7	sedimentace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
transgresí	transgrese	k1gFnSc7	transgrese
moře	moře	k1gNnSc2	moře
v	v	k7c4	v
kelloway-oxfordu	kellowayxforda	k1gFnSc4	kelloway-oxforda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
tithónu	tithón	k1gInSc2	tithón
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
vápence	vápenec	k1gInPc1	vápenec
hrušovanské	hrušovanský	k2eAgInPc1d1	hrušovanský
<g/>
,	,	kIx,	,
novosedelské	novosedelský	k2eAgInPc1d1	novosedelský
<g/>
,	,	kIx,	,
pasohlávské	pasohlávský	k2eAgInPc1d1	pasohlávský
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
nikolčické	nikolčický	k2eAgFnPc1d1	nikolčický
vrstvy	vrstva	k1gFnPc1	vrstva
<g/>
,	,	kIx,	,
mušovské	mušovský	k2eAgNnSc1d1	mušovský
souvrství	souvrství	k1gNnSc1	souvrství
<g/>
,	,	kIx,	,
kurdějovské	kurdějovský	k2eAgNnSc1d1	kurdějovský
<g />
.	.	kIx.	.
</s>
<s>
vápence	vápenec	k1gInPc4	vápenec
a	a	k8xC	a
kobylské	kobylský	k2eAgInPc4d1	kobylský
vápence	vápenec	k1gInPc4	vápenec
s	s	k7c7	s
dolomity	dolomit	k1gInPc7	dolomit
<g/>
.	.	kIx.	.
ždánická	ždánický	k2eAgFnSc1d1	Ždánická
jednotka	jednotka	k1gFnSc1	jednotka
–	–	k?	–
klentnické	klentnický	k2eAgFnPc4d1	klentnická
vrstvy	vrstva	k1gFnPc4	vrstva
malmu	malm	k1gInSc2	malm
tvoří	tvořit	k5eAaImIp3nP	tvořit
výrazný	výrazný	k2eAgInSc4d1	výrazný
morfologický	morfologický	k2eAgInSc4d1	morfologický
prvek	prvek	k1gInSc4	prvek
–	–	k?	–
vápencová	vápencový	k2eAgNnPc4d1	vápencové
bradla	bradla	k1gNnPc4	bradla
Pavlovských	pavlovský	k2eAgInPc2d1	pavlovský
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
ernstbrunnský	ernstbrunnský	k2eAgInSc1d1	ernstbrunnský
vápenec	vápenec	k1gInSc1	vápenec
pak	pak	k6eAd1	pak
Svatý	svatý	k2eAgMnSc1d1	svatý
Kopeček	Kopeček	k1gMnSc1	Kopeček
u	u	k7c2	u
Mikulova	Mikulov	k1gInSc2	Mikulov
slezská	slezský	k2eAgFnSc1d1	Slezská
jednotka	jednotka	k1gFnSc1	jednotka
–	–	k?	–
štramberský	štramberský	k2eAgInSc4d1	štramberský
vápenec	vápenec	k1gInSc4	vápenec
tvořící	tvořící	k2eAgNnSc4d1	tvořící
bradlo	bradlo	k1gNnSc4	bradlo
Kotouč	kotouč	k1gInSc1	kotouč
u	u	k7c2	u
Štramberka	Štramberk	k1gInSc2	Štramberk
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
světovou	světový	k2eAgFnSc7d1	světová
paleontologickou	paleontologický	k2eAgFnSc7d1	paleontologická
lokalitou	lokalita	k1gFnSc7	lokalita
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
tam	tam	k6eAd1	tam
popsáno	popsat	k5eAaPmNgNnS	popsat
přes	přes	k7c4	přes
600	[number]	k4	600
druhů	druh	k1gInPc2	druh
zkamenělin	zkamenělina	k1gFnPc2	zkamenělina
představuje	představovat	k5eAaImIp3nS	představovat
nejsvrchnější	svrchní	k2eAgInSc1d3	nejsvrchnější
stupeň	stupeň	k1gInSc1	stupeň
tithon	tithona	k1gFnPc2	tithona
<g/>
...	...	k?	...
Bohužel	bohužel	k9	bohužel
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
tektonickou	tektonický	k2eAgFnSc4d1	tektonická
trosku	troska	k1gFnSc4	troska
bez	bez	k7c2	bez
podloží	podloží	k1gNnSc2	podloží
obklopenou	obklopený	k2eAgFnSc7d1	obklopená
křídou	křída	k1gFnSc7	křída
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
globální	globální	k2eAgInSc1d1	globální
stratotyp	stratotyp	k1gInSc1	stratotyp
<g/>
...	...	k?	...
Významná	významný	k2eAgFnSc1d1	významná
jsou	být	k5eAaImIp3nP	být
předevšímm	především	k1gNnSc7	především
ložiska	ložisko	k1gNnSc2	ložisko
černého	černý	k2eAgNnSc2d1	černé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
v	v	k7c6	v
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
molase	molasa	k1gFnSc6	molasa
podél	podél	k7c2	podél
Skalnatých	skalnatý	k2eAgFnPc2d1	skalnatá
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
v	v	k7c4	v
Asii	Asie	k1gFnSc4	Asie
čeljabinská	čeljabinský	k2eAgFnSc1d1	Čeljabinská
<g/>
,	,	kIx,	,
irkutská	irkutský	k2eAgFnSc1d1	Irkutská
<g/>
,	,	kIx,	,
turgajská	turgajský	k2eAgFnSc1d1	turgajský
pánev	pánev	k1gFnSc1	pánev
<g/>
,	,	kIx,	,
ložiska	ložisko	k1gNnPc1	ložisko
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
ložisko	ložisko	k1gNnSc4	ložisko
Gresten	Gresten	k2eAgMnSc1d1	Gresten
<g/>
,	,	kIx,	,
pánev	pánev	k1gFnSc1	pánev
Mecsek	Mecska	k1gFnPc2	Mecska
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sedimenty	sediment	k1gInPc4	sediment
plošiny	plošina	k1gFnSc2	plošina
Colorado	Colorado	k1gNnSc1	Colorado
jsou	být	k5eAaImIp3nP	být
vázána	vázán	k2eAgNnPc1d1	vázáno
ložiska	ložisko	k1gNnPc1	ložisko
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mexickém	mexický	k2eAgInSc6d1	mexický
zálivu	záliv	k1gInSc6	záliv
jsou	být	k5eAaImIp3nP	být
významná	významný	k2eAgNnPc1d1	významné
ložiska	ložisko	k1gNnPc1	ložisko
evaporitů	evaporit	k1gInPc2	evaporit
a	a	k8xC	a
důležité	důležitý	k2eAgInPc1d1	důležitý
jsou	být	k5eAaImIp3nP	být
solné	solný	k2eAgInPc1d1	solný
pně	peň	k1gInPc1	peň
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
struktury	struktura	k1gFnPc4	struktura
vhodné	vhodný	k2eAgFnPc4d1	vhodná
pro	pro	k7c4	pro
ložiska	ložisko	k1gNnPc4	ložisko
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
lotrinská	lotrinský	k2eAgFnSc1d1	lotrinská
mineta	mineta	k1gFnSc1	mineta
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
oolitických	oolitický	k2eAgFnPc2d1	oolitický
rud	ruda	k1gFnPc2	ruda
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
na	na	k7c6	na
ložisku	ložisko	k1gNnSc6	ložisko
Urkút	Urkúta	k1gFnPc2	Urkúta
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
rudy	ruda	k1gFnSc2	ruda
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
významná	významný	k2eAgNnPc1d1	významné
jsou	být	k5eAaImIp3nP	být
ložiska	ložisko	k1gNnPc1	ložisko
bauxitu	bauxit	k1gInSc2	bauxit
v	v	k7c6	v
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
a	a	k8xC	a
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Geologický	geologický	k2eAgInSc4d1	geologický
čas	čas	k1gInSc4	čas
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jura	jura	k1gFnSc1	jura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stratigrafická	Stratigrafický	k2eAgFnSc1d1	Stratigrafická
tabulka	tabulka	k1gFnSc1	tabulka
-	-	kIx~	-
formát	formát	k1gInSc1	formát
PDF	PDF	kA	PDF
</s>
