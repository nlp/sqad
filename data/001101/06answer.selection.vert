<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Sekora	Sekor	k1gMnSc2	Sekor
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1899	[number]	k4	1899
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gFnSc1	pole
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1967	[number]	k4	1967
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
výrazný	výrazný	k2eAgMnSc1d1	výrazný
český	český	k2eAgMnSc1d1	český
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
žurnalista	žurnalista	k1gMnSc1	žurnalista
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
karikaturista	karikaturista	k1gMnSc1	karikaturista
a	a	k8xC	a
entomolog	entomolog	k1gMnSc1	entomolog
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
nositel	nositel	k1gMnSc1	nositel
titulu	titul	k1gInSc2	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
