<s>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
svaz	svaz	k1gInSc1	svaz
československého	československý	k2eAgNnSc2d1	Československé
studentstva	studentstvo	k1gNnSc2	studentstvo
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
naší	náš	k3xOp1gFnSc2	náš
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
aktivní	aktivní	k2eAgFnSc4d1	aktivní
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
byla	být	k5eAaImAgFnS	být
právě	právě	k6eAd1	právě
londýnská	londýnský	k2eAgFnSc1d1	londýnská
schůze	schůze	k1gFnSc1	schůze
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
studentské	studentský	k2eAgFnSc2d1	studentská
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
Prohlášení	prohlášení	k1gNnSc4	prohlášení
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
studentů	student	k1gMnPc2	student
k	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
dnem	den	k1gInSc7	den
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
.	.	kIx.	.
</s>
