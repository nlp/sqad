<s>
Richmond	Richmond	k1gInSc1	Richmond
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Richmondem	Richmond	k1gInSc7	Richmond
prochází	procházet	k5eAaImIp3nS	procházet
několik	několik	k4yIc1	několik
důležitých	důležitý	k2eAgFnPc2d1	důležitá
vnitrostátních	vnitrostátní	k2eAgFnPc2d1	vnitrostátní
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemu	co	k3yQnSc3	co
byla	být	k5eAaImAgFnS	být
celková	celkový	k2eAgFnSc1d1	celková
populace	populace	k1gFnSc1	populace
města	město	k1gNnSc2	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
1,194,008	[number]	k4	1,194,008
lidí	člověk	k1gMnPc2	člověk
během	během	k7c2	během
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Okolí	okolí	k1gNnSc1	okolí
Richmondu	Richmond	k1gInSc2	Richmond
a	a	k8xC	a
řeky	řeka	k1gFnSc2	řeka
James	Jamesa	k1gFnPc2	Jamesa
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
již	již	k9	již
roku	rok	k1gInSc2	rok
1607	[number]	k4	1607
anglickými	anglický	k2eAgMnPc7d1	anglický
osadníky	osadník	k1gMnPc7	osadník
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
město	město	k1gNnSc1	město
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
až	až	k9	až
roku	rok	k1gInSc2	rok
1737	[number]	k4	1737
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
americké	americký	k2eAgFnSc2d1	americká
revoluce	revoluce	k1gFnSc2	revoluce
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
svědkem	svědek	k1gMnSc7	svědek
několika	několik	k4yIc2	několik
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Patrick	Patrick	k1gMnSc1	Patrick
Henry	Henry	k1gMnSc1	Henry
zde	zde	k6eAd1	zde
pronesl	pronést	k5eAaPmAgMnS	pronést
svoji	svůj	k3xOyFgFnSc4	svůj
slavnou	slavný	k2eAgFnSc4d1	slavná
řeč	řeč	k1gFnSc4	řeč
Život	život	k1gInSc1	život
nebo	nebo	k8xC	nebo
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Richmond	Richmond	k1gInSc1	Richmond
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Konfederovaných	konfederovaný	k2eAgInPc2d1	konfederovaný
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Richmondská	Richmondský	k2eAgFnSc1d1	Richmondská
ekonomika	ekonomika	k1gFnSc1	ekonomika
žije	žít	k5eAaImIp3nS	žít
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
svoje	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
několik	několik	k4yIc4	několik
velkých	velká	k1gFnPc2	velká
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
právnických	právnický	k2eAgFnPc2d1	právnická
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
poboček	pobočka	k1gFnPc2	pobočka
Federální	federální	k2eAgFnSc2d1	federální
rezervní	rezervní	k2eAgFnSc2d1	rezervní
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
žije	žít	k5eAaImIp3nS	žít
také	také	k9	také
z	z	k7c2	z
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
strávil	strávit	k5eAaPmAgMnS	strávit
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Monroeová	Monroeová	k1gFnSc1	Monroeová
(	(	kIx(	(
<g/>
1768	[number]	k4	1768
-	-	kIx~	-
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
USA	USA	kA	USA
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
prezidenta	prezident	k1gMnSc2	prezident
Jamese	Jamese	k1gFnSc2	Jamese
Monroea	Monroeus	k1gMnSc2	Monroeus
Julia	Julius	k1gMnSc2	Julius
Gardiner	Gardiner	k1gMnSc1	Gardiner
Tylerová	Tylerová	k1gFnSc1	Tylerová
(	(	kIx(	(
<g/>
1820	[number]	k4	1820
-	-	kIx~	-
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
USA	USA	kA	USA
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
manželka	manželka	k1gFnSc1	manželka
prezidenta	prezident	k1gMnSc2	prezident
Johna	John	k1gMnSc2	John
Tylera	Tyler	k1gMnSc2	Tyler
John	John	k1gMnSc1	John
Bennett	Bennett	k1gMnSc1	Bennett
Fenn	Fenn	k1gMnSc1	Fenn
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
-	-	kIx~	-
2010	[number]	k4	2010
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
profesor	profesor	k1gMnSc1	profesor
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
Alexandra	Alexandra	k1gFnSc1	Alexandra
Ripleyová	Ripleyová	k1gFnSc1	Ripleyová
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
-	-	kIx~	-
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Shirley	Shirlea	k1gFnSc2	Shirlea
MacLaine	MacLain	k1gInSc5	MacLain
(	(	kIx(	(
<g/>
*	*	kIx~	*
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
publicistka	publicistka	k1gFnSc1	publicistka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Warren	Warrna	k1gFnPc2	Warrna
Beatty	Beatta	k1gFnSc2	Beatta
(	(	kIx(	(
<g/>
*	*	kIx~	*
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
Arthur	Arthura	k1gFnPc2	Arthura
Ashe	Ashe	k1gFnPc2	Ashe
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
-	-	kIx~	-
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Stan	stan	k1gInSc1	stan
Winston	Winston	k1gInSc1	Winston
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
specialista	specialista	k1gMnSc1	specialista
na	na	k7c4	na
filmové	filmový	k2eAgInPc4d1	filmový
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
masky	maska	k1gFnPc4	maska
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Caroline	Carolin	k1gInSc5	Carolin
Aaronová	Aaronový	k2eAgNnPc1d1	Aaronový
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Joey	Joea	k1gFnSc2	Joea
Baron	baron	k1gMnSc1	baron
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jazzový	jazzový	k2eAgMnSc1d1	jazzový
bubeník	bubeník	k1gMnSc1	bubeník
Joe	Joe	k1gFnSc2	Joe
Edwards	Edwards	k1gInSc1	Edwards
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vojenský	vojenský	k2eAgMnSc1d1	vojenský
pilot	pilot	k1gMnSc1	pilot
a	a	k8xC	a
astronaut	astronaut	k1gMnSc1	astronaut
Wes	Wes	k1gFnSc2	Wes
Borland	Borland	kA	Borland
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k1gMnSc1	Bizkit
Mickie	Mickie	k1gFnSc2	Mickie
James	James	k1gMnSc1	James
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
wrestlerka	wrestlerka	k1gFnSc1	wrestlerka
a	a	k8xC	a
country	country	k2eAgFnSc1d1	country
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Meghann	Meghanna	k1gFnPc2	Meghanna
Shaughnessy	Shaughnessa	k1gFnSc2	Shaughnessa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Patrick	Patrick	k1gMnSc1	Patrick
Estes	Estes	k1gMnSc1	Estes
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Hunter	Hunter	k1gMnSc1	Hunter
Parrish	Parrish	k1gMnSc1	Parrish
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Čeng-čou	Čeng-čá	k1gFnSc4	Čeng-čá
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Olsztyn	Olsztyn	k1gInSc1	Olsztyn
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Richmond-upon-Thames	Richmondpon-Thamesa	k1gFnPc2	Richmond-upon-Thamesa
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Saitama	Saitama	k1gFnSc1	Saitama
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Ségou	Séga	k1gFnSc7	Séga
<g/>
,	,	kIx,	,
Mali	Mali	k1gNnSc7	Mali
Uijongbu	Uijongb	k1gInSc2	Uijongb
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Windhoek	Windhoek	k1gInSc1	Windhoek
<g/>
,	,	kIx,	,
Namíbie	Namíbie	k1gFnSc1	Namíbie
</s>
