<p>
<s>
NTC	NTC	kA	NTC
aréna	aréna	k1gFnSc1	aréna
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
AXA	AXA	kA	AXA
aréna	aréna	k1gFnSc1	aréna
NTC	NTC	kA	NTC
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Sibamac	Sibamac	k1gFnSc1	Sibamac
Arena	Arena	k1gFnSc1	Arena
a	a	k8xC	a
Aegon	Aegon	k1gMnSc1	Aegon
aréna	aréna	k1gFnSc1	aréna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
víceúčelová	víceúčelový	k2eAgFnSc1d1	víceúčelová
aréna	aréna	k1gFnSc1	aréna
se	s	k7c7	s
zatahovací	zatahovací	k2eAgFnSc7d1	zatahovací
střechou	střecha	k1gFnSc7	střecha
ve	v	k7c6	v
slovenské	slovenský	k2eAgFnSc6d1	slovenská
metropoli	metropol	k1gFnSc6	metropol
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
arény	aréna	k1gFnSc2	aréna
došlo	dojít	k5eAaPmAgNnS	dojít
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
typu	typ	k1gInSc6	typ
pořádané	pořádaný	k2eAgFnSc2d1	pořádaná
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tenisové	tenisový	k2eAgInPc4d1	tenisový
zápasy	zápas	k1gInPc4	zápas
činí	činit	k5eAaImIp3nS	činit
4	[number]	k4	4
000	[number]	k4	000
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
na	na	k7c4	na
koncerty	koncert	k1gInPc4	koncert
pak	pak	k6eAd1	pak
6	[number]	k4	6
076	[number]	k4	076
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Provozují	provozovat	k5eAaImIp3nP	provozovat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
také	také	k9	také
squash	squash	k1gInSc1	squash
<g/>
,	,	kIx,	,
badminton	badminton	k1gInSc1	badminton
či	či	k8xC	či
fitness	fitness	k1gInSc1	fitness
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgMnSc1d1	slovenský
daviscupový	daviscupový	k2eAgMnSc1d1	daviscupový
a	a	k8xC	a
fedcupový	fedcupový	k2eAgInSc1d1	fedcupový
tým	tým	k1gInSc1	tým
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
hraje	hrát	k5eAaImIp3nS	hrát
mezistátní	mezistátní	k2eAgNnSc4d1	mezistátní
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
činily	činit	k5eAaImAgFnP	činit
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
<g/>
Stadion	stadion	k1gInSc1	stadion
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Národního	národní	k2eAgNnSc2d1	národní
tenisového	tenisový	k2eAgNnSc2d1	tenisové
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
dějištěm	dějiště	k1gNnSc7	dějiště
finále	finále	k1gNnPc2	finále
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Slovensko	Slovensko	k1gNnSc1	Slovensko
na	na	k7c6	na
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
povrchu	povrch	k1gInSc6	povrch
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
těsně	těsně	k6eAd1	těsně
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Probíhá	probíhat	k5eAaImIp3nS	probíhat
zde	zde	k6eAd1	zde
také	také	k9	také
challenger	challenger	k1gMnSc1	challenger
Slovak	Slovak	k1gMnSc1	Slovak
Open	Open	k1gMnSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Aréna	aréna	k1gFnSc1	aréna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
místem	místem	k6eAd1	místem
pořádání	pořádání	k1gNnSc4	pořádání
kulturních	kulturní	k2eAgFnPc2d1	kulturní
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
koncertů	koncert	k1gInPc2	koncert
Joe	Joe	k1gMnSc2	Joe
Cockera	Cocker	k1gMnSc2	Cocker
<g/>
,	,	kIx,	,
Marilyna	Marilyn	k1gMnSc2	Marilyn
Mansona	Manson	k1gMnSc2	Manson
<g/>
,	,	kIx,	,
Deep	Deep	k1gInSc4	Deep
Purple	Purple	k1gFnSc2	Purple
a	a	k8xC	a
Rihanny	Rihanna	k1gFnSc2	Rihanna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Aegon	Aegon	k1gMnSc1	Aegon
aréna	aréna	k1gFnSc1	aréna
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
NTC	NTC	kA	NTC
aréna	aréna	k1gFnSc1	aréna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
tenisové	tenisový	k2eAgNnSc1d1	tenisové
centrum	centrum	k1gNnSc1	centrum
–	–	k?	–
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
</s>
</p>
