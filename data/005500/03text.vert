<s>
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Frauenberg	Frauenberg	k1gInSc1	Frauenberg
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Fronburg	Fronburg	k1gInSc1	Fronburg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ležící	ležící	k2eAgNnSc1d1	ležící
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
po	po	k7c6	po
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
zhruba	zhruba	k6eAd1	zhruba
9	[number]	k4	9
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
5	[number]	k4	5
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
4921	[number]	k4	4921
<g/>
)	)	kIx)	)
a	a	k8xC	a
město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
posunulo	posunout	k5eAaPmAgNnS	posunout
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
výměra	výměra	k1gFnSc1	výměra
9112	[number]	k4	9112
ha	ha	kA	ha
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
Hluboké	Hluboká	k1gFnSc2	Hluboká
po	po	k7c6	po
Lišově	Lišov	k1gInSc6	Lišov
druhou	druhý	k4xOgFnSc4	druhý
nejrozlehlejší	rozlehlý	k2eAgFnSc1d3	nejrozlehlejší
obec	obec	k1gFnSc1	obec
Českobudějovicka	Českobudějovicko	k1gNnSc2	Českobudějovicko
<g/>
.	.	kIx.	.
</s>
<s>
PSČ	PSČ	kA	PSČ
je	být	k5eAaImIp3nS	být
373	[number]	k4	373
41	[number]	k4	41
vyjma	vyjma	k7c2	vyjma
částí	část	k1gFnPc2	část
Purkarec	Purkarec	k1gInSc1	Purkarec
a	a	k8xC	a
Jeznice	Jeznice	k1gFnPc1	Jeznice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
PSČ	PSČ	kA	PSČ
373	[number]	k4	373
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
<s>
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
pověřenou	pověřený	k2eAgFnSc7d1	pověřená
obcí	obec	k1gFnSc7	obec
též	též	k9	též
pro	pro	k7c4	pro
obce	obec	k1gFnPc4	obec
Dříteň	Dříteň	k1gFnSc4	Dříteň
<g/>
,	,	kIx,	,
Nákří	Nákří	k1gNnSc4	Nákří
a	a	k8xC	a
Olešník	olešník	k1gInSc4	olešník
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
místní	místní	k2eAgFnSc1d1	místní
část	část	k1gFnSc1	část
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
pod	pod	k7c7	pod
zámeckým	zámecký	k2eAgInSc7d1	zámecký
vrchem	vrch	k1gInSc7	vrch
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řeka	řeka	k1gFnSc1	řeka
Vltava	Vltava	k1gFnSc1	Vltava
opouští	opouštět	k5eAaImIp3nS	opouštět
rovinatou	rovinatý	k2eAgFnSc4d1	rovinatá
Českobudějovickou	českobudějovický	k2eAgFnSc4d1	českobudějovická
pánev	pánev	k1gFnSc4	pánev
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
si	se	k3xPyFc3	se
razit	razit	k5eAaImF	razit
cestu	cesta	k1gFnSc4	cesta
Táborskou	táborský	k2eAgFnSc7d1	táborská
pahorkatinou	pahorkatina	k1gFnSc7	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
někdejší	někdejší	k2eAgNnSc1d1	někdejší
podhradí	podhradí	k1gNnSc1	podhradí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
situováno	situovat	k5eAaBmNgNnS	situovat
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
vltavském	vltavský	k2eAgInSc6d1	vltavský
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nynější	nynější	k2eAgFnSc2d1	nynější
podoby	podoba	k1gFnSc2	podoba
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
sídelní	sídelní	k2eAgInSc1d1	sídelní
útvar	útvar	k1gInSc1	útvar
rozrostl	rozrůst	k5eAaPmAgInS	rozrůst
spojením	spojení	k1gNnSc7	spojení
Hluboké	Hluboká	k1gFnSc2	Hluboká
<g/>
,	,	kIx,	,
Podskalí	Podskalí	k1gNnSc2	Podskalí
<g/>
,	,	kIx,	,
Hamrů	Hamry	k1gInPc2	Hamry
a	a	k8xC	a
pravobřežního	pravobřežní	k2eAgNnSc2d1	pravobřežní
Zámostí	Zámost	k1gFnSc7	Zámost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
ze	z	k7c2	z
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
1263	[number]	k4	1263
domů	dům	k1gInPc2	dům
a	a	k8xC	a
4538	[number]	k4	4538
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Bavorovice	Bavorovice	k1gFnPc1	Bavorovice
<g/>
;	;	kIx,	;
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
Bavorovice	Bavorovice	k1gFnSc2	Bavorovice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
5,25	[number]	k4	5,25
km2	km2	k4	km2
<g/>
,	,	kIx,	,
96	[number]	k4	96
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
310	[number]	k4	310
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
Buzkov	Buzkov	k1gInSc1	Buzkov
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Jeznice	Jeznice	k1gFnSc2	Jeznice
<g/>
,	,	kIx,	,
0	[number]	k4	0
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
0	[number]	k4	0
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
část	část	k1gFnSc1	část
<g />
.	.	kIx.	.
</s>
<s>
obce	obec	k1gFnPc1	obec
Purkarec	Purkarec	k1gInSc1	Purkarec
<g/>
;	;	kIx,	;
úplně	úplně	k6eAd1	úplně
zatopen	zatopen	k2eAgInSc1d1	zatopen
Hněvkovickou	Hněvkovický	k2eAgFnSc7d1	Hněvkovická
přehradou	přehrada	k1gFnSc7	přehrada
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
,	,	kIx,	,
34,10	[number]	k4	34,10
km2	km2	k4	km2
<g/>
,	,	kIx,	,
867	[number]	k4	867
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
3591	[number]	k4	3591
obyvatel	obyvatel	k1gMnPc2	obyvatel
Hroznějovice	Hroznějovice	k1gFnSc2	Hroznějovice
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Hroznějovice	Hroznějovice	k1gFnSc2	Hroznějovice
<g/>
,	,	kIx,	,
5,52	[number]	k4	5,52
km2	km2	k4	km2
<g/>
,	,	kIx,	,
36	[number]	k4	36
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
54	[number]	k4	54
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Kostelec	Kostelec	k1gInSc1	Kostelec
Jaroslavice	Jaroslavice	k1gFnSc2	Jaroslavice
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Jaroslavice	Jaroslavice	k1gFnSc2	Jaroslavice
u	u	k7c2	u
Kostelce	Kostelec	k1gInSc2	Kostelec
<g/>
,	,	kIx,	,
3,71	[number]	k4	3,71
km2	km2	k4	km2
<g/>
,	,	kIx,	,
0	[number]	k4	0
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
0	[number]	k4	0
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
;	;	kIx,	;
úplně	úplně	k6eAd1	úplně
zatopeny	zatopen	k2eAgFnPc4d1	zatopena
<g />
.	.	kIx.	.
</s>
<s>
Hněvkovickou	Hněvkovický	k2eAgFnSc7d1	Hněvkovická
přehradou	přehrada	k1gFnSc7	přehrada
Jeznice	Jeznice	k1gFnSc2	Jeznice
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Jeznice	Jeznice	k1gFnSc2	Jeznice
<g/>
,	,	kIx,	,
6,07	[number]	k4	6,07
km2	km2	k4	km2
<g/>
,	,	kIx,	,
26	[number]	k4	26
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
35	[number]	k4	35
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Purkarec	Purkarec	k1gInSc1	Purkarec
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
,	,	kIx,	,
4,51	[number]	k4	4,51
km2	km2	k4	km2
<g/>
,	,	kIx,	,
55	[number]	k4	55
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
134	[number]	k4	134
obyvatel	obyvatel	k1gMnSc1	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
Líšnice	Líšnice	k1gFnSc2	Líšnice
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Líšnice	Líšnice	k1gFnSc2	Líšnice
u	u	k7c2	u
Kostelce	Kostelec	k1gInSc2	Kostelec
<g/>
,	,	kIx,	,
12,57	[number]	k4	12,57
km2	km2	k4	km2
<g/>
,	,	kIx,	,
26	[number]	k4	26
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
31	[number]	k4	31
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Kostelec	Kostelec	k1gInSc1	Kostelec
Munice	munice	k1gFnSc2	munice
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Munice	munice	k1gFnSc2	munice
<g/>
,	,	kIx,	,
5,70	[number]	k4	5,70
km2	km2	k4	km2
<g/>
,	,	kIx,	,
69	[number]	k4	69
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
200	[number]	k4	200
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
Poněšice	Poněšice	k1gFnSc2	Poněšice
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Poněšice	Poněšice	k1gFnSc2	Poněšice
<g/>
,	,	kIx,	,
6,31	[number]	k4	6,31
km2	km2	k4	km2
<g/>
,	,	kIx,	,
29	[number]	k4	29
číslovaných	číslovaný	k2eAgInPc2d1	číslovaný
domů	dům	k1gInPc2	dům
a	a	k8xC	a
několik	několik	k4yIc4	několik
budov	budova	k1gFnPc2	budova
výukového	výukový	k2eAgNnSc2d1	výukové
centra	centrum	k1gNnSc2	centrum
<g />
.	.	kIx.	.
</s>
<s>
Akademie	akademie	k1gFnSc1	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
33	[number]	k4	33
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Kostelec	Kostelec	k1gInSc1	Kostelec
Purkarec	Purkarec	k1gInSc1	Purkarec
<g/>
;	;	kIx,	;
k.	k.	k?	k.
ú.	ú.	k?	ú.
Purkarec	Purkarec	k1gMnSc1	Purkarec
<g/>
,	,	kIx,	,
7,39	[number]	k4	7,39
km2	km2	k4	km2
<g/>
,	,	kIx,	,
59	[number]	k4	59
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
150	[number]	k4	150
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
samostatná	samostatný	k2eAgFnSc1d1	samostatná
obec	obec	k1gFnSc1	obec
<g/>
;	;	kIx,	;
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zatopen	zatopen	k2eAgInSc1d1	zatopen
Hněvkovickou	Hněvkovický	k2eAgFnSc7d1	Hněvkovická
přehradou	přehrada	k1gFnSc7	přehrada
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
městu	město	k1gNnSc3	město
patří	patřit	k5eAaImIp3nS	patřit
dvůr	dvůr	k1gInSc1	dvůr
Vondrov	Vondrov	k1gInSc1	Vondrov
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
v	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
někdejšího	někdejší	k2eAgInSc2d1	někdejší
královského	královský	k2eAgInSc2d1	královský
hradu	hrad	k1gInSc2	hrad
Froburg	Froburg	k1gInSc1	Froburg
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Wroburch	Wroburch	k1gInSc1	Wroburch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbudovaného	zbudovaný	k2eAgInSc2d1	zbudovaný
ve	v	k7c4	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
připomíná	připomínat	k5eAaImIp3nS	připomínat
roku	rok	k1gInSc2	rok
1378	[number]	k4	1378
(	(	kIx(	(
<g/>
Markt	Markt	k1gInSc1	Markt
Podhrad	Podhrad	k1gInSc1	Podhrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
hradu	hrad	k1gInSc2	hrad
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
středohornoněmeckého	středohornoněmecký	k2eAgNnSc2d1	středohornoněmecké
slova	slovo	k1gNnSc2	slovo
vrô	vrô	k?	vrô
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
pán	pán	k1gMnSc1	pán
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k1gMnPc4	příbuzný
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgNnSc1d1	moderní
slovo	slovo	k1gNnSc1	slovo
Frau	Fraus	k1gInSc2	Fraus
<g/>
)	)	kIx)	)
a	a	k8xC	a
znamenal	znamenat	k5eAaImAgInS	znamenat
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Pánův	pánův	k2eAgInSc4d1	pánův
hrad	hrad	k1gInSc4	hrad
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Hrad	hrad	k1gInSc1	hrad
Páně	páně	k2eAgMnSc2d1	páně
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
zván	zván	k2eAgInSc1d1	zván
též	též	k9	též
česky	česky	k6eAd1	česky
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
,	,	kIx,	,
snad	snad	k9	snad
pro	pro	k7c4	pro
někdejší	někdejší	k2eAgInSc4d1	někdejší
hluboký	hluboký	k2eAgInSc4d1	hluboký
les	les	k1gInSc4	les
či	či	k8xC	či
vyvýšenou	vyvýšený	k2eAgFnSc4d1	vyvýšená
polohu	poloha	k1gFnSc4	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
Foburg	Foburg	k1gInSc1	Foburg
<g/>
,	,	kIx,	,
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
půli	půle	k1gFnSc6	půle
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Čeča	Čeč	k1gInSc2	Čeč
z	z	k7c2	z
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
moci	moc	k1gFnSc3	moc
Vítkovců	Vítkovec	k1gMnPc2	Vítkovec
hrad	hrad	k1gInSc4	hrad
zabavil	zabavit	k5eAaPmAgMnS	zabavit
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
začlenil	začlenit	k5eAaPmAgMnS	začlenit
jej	on	k3xPp3gInSc4	on
do	do	k7c2	do
královských	královský	k2eAgInPc2d1	královský
hradů	hrad	k1gInPc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
prof.	prof.	kA	prof.
Josef	Josef	k1gMnSc1	Josef
Žemlička	Žemlička	k1gMnSc1	Žemlička
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
král	král	k1gMnSc1	král
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
věků	věk	k1gInPc2	věk
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
215	[number]	k4	215
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pás	pás	k1gInSc1	pás
Přemyslovských	přemyslovský	k2eAgFnPc2d1	Přemyslovská
fundací	fundace	k1gFnPc2	fundace
impozantní	impozantní	k2eAgFnSc1d1	impozantní
<g/>
.	.	kIx.	.
</s>
<s>
Hluboká	Hluboká	k1gFnSc1	Hluboká
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
královo	králův	k2eAgNnSc1d1	královo
hradních	hradní	k2eAgFnPc2d1	hradní
opor	opora	k1gFnPc2	opora
jako	jako	k8xC	jako
byl	být	k5eAaImAgInS	být
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
Bechyně	Bechyně	k1gFnSc1	Bechyně
<g/>
,	,	kIx,	,
Vimperk	Vimperk	k1gInSc1	Vimperk
<g/>
,	,	kIx,	,
Myšenec	Myšenec	k1gInSc1	Myšenec
<g/>
,	,	kIx,	,
Protivín	Protivín	k1gInSc1	Protivín
a	a	k8xC	a
Újezdec	Újezdec	k1gMnSc1	Újezdec
u	u	k7c2	u
Týna	Týn	k1gInSc2	Týn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
drželi	držet	k5eAaImAgMnP	držet
krátce	krátce	k6eAd1	krátce
Hlubokou	Hluboká	k1gFnSc4	Hluboká
Vítkovci	Vítkovec	k1gMnSc3	Vítkovec
–	–	k?	–
Záviš	Záviš	k1gMnSc1	Záviš
z	z	k7c2	z
Falknštejna	Falknštejn	k1gInSc2	Falknštejn
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
popravě	poprava	k1gFnSc6	poprava
Falknštejna	Falknštejno	k1gNnSc2	Falknštejno
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
se	se	k3xPyFc4	se
hrad	hrad	k1gInSc1	hrad
opět	opět	k6eAd1	opět
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
držení	držení	k1gNnSc2	držení
královské	královský	k2eAgFnSc2d1	královská
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
hrad	hrad	k1gInSc4	hrad
se	s	k7c7	s
vsí	ves	k1gFnSc7	ves
zastavil	zastavit	k5eAaPmAgInS	zastavit
Mikuláši	mikuláš	k1gInPc7	mikuláš
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
a	a	k8xC	a
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
jej	on	k3xPp3gInSc4	on
vykoupil	vykoupit	k5eAaPmAgInS	vykoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1454	[number]	k4	1454
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
jej	on	k3xPp3gMnSc4	on
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k9	jako
věno	věno	k1gNnSc4	věno
své	svůj	k3xOyFgFnSc6	svůj
druhé	druhý	k4xOgFnSc6	druhý
ženě	žena	k1gFnSc6	žena
Johance	Johanka	k1gFnSc6	Johanka
z	z	k7c2	z
Rožmitála	Rožmitál	k1gMnSc2	Rožmitál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
se	se	k3xPyFc4	se
hradu	hrad	k1gInSc2	hrad
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
Jindřich	Jindřich	k1gMnSc1	Jindřich
Roubík	Roubík	k1gMnSc1	Roubík
z	z	k7c2	z
Hlavatec	hlavatec	k1gInSc1	hlavatec
a	a	k8xC	a
královně	královna	k1gFnSc3	královna
Johance	Johanka	k1gFnSc3	Johanka
ho	on	k3xPp3gMnSc4	on
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
zaplaceny	zaplacen	k2eAgFnPc1d1	zaplacena
válečné	válečný	k2eAgFnPc1d1	válečná
škody	škoda	k1gFnPc1	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
epizodu	epizoda	k1gFnSc4	epizoda
byla	být	k5eAaImAgFnS	být
tedy	tedy	k9	tedy
Hluboká	Hluboká	k1gFnSc1	Hluboká
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Johanky	Johanka	k1gFnSc2	Johanka
21	[number]	k4	21
let	léto	k1gNnPc2	léto
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
17	[number]	k4	17
let	léto	k1gNnPc2	léto
ji	on	k3xPp3gFnSc4	on
držela	držet	k5eAaImAgFnS	držet
jako	jako	k9	jako
česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
královny	královna	k1gFnSc2	královna
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1475	[number]	k4	1475
přešla	přejít	k5eAaPmAgFnS	přejít
Hluboká	Hluboká	k1gFnSc1	Hluboká
do	do	k7c2	do
držení	držení	k1gNnSc2	držení
jejího	její	k3xOp3gMnSc2	její
bratra	bratr	k1gMnSc2	bratr
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Lva	Lev	k1gMnSc2	Lev
z	z	k7c2	z
Rožmitála	Rožmitál	k1gMnSc2	Rožmitál
a	a	k8xC	a
na	na	k7c4	na
Blatné	blatný	k2eAgNnSc4d1	Blatné
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
Hlubokou	hluboký	k2eAgFnSc4d1	hluboká
a	a	k8xC	a
ves	ves	k1gFnSc4	ves
v	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
získal	získat	k5eAaPmAgInS	získat
Vilémem	Vilém	k1gMnSc7	Vilém
z	z	k7c2	z
Pernštejna	Pernštejn	k1gInSc2	Pernštejn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ves	ves	k1gFnSc4	ves
povýšil	povýšit	k5eAaPmAgInS	povýšit
na	na	k7c4	na
městys	městys	k1gInSc4	městys
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1496	[number]	k4	1496
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
let	léto	k1gNnPc2	léto
1660-1670	[number]	k4	1660-1670
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
zvaný	zvaný	k2eAgInSc1d1	zvaný
Frauenberg	Frauenberg	k1gInSc1	Frauenberg
stal	stát	k5eAaPmAgInS	stát
majetkem	majetek	k1gInSc7	majetek
rodu	rod	k1gInSc2	rod
Schwarzenbergů	Schwarzenberg	k1gMnPc2	Schwarzenberg
<g/>
,	,	kIx,	,
<g/>
kteří	který	k3yIgMnPc1	který
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1871	[number]	k4	1871
přestavěli	přestavět	k5eAaPmAgMnP	přestavět
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
romantické	romantický	k2eAgNnSc1d1	romantické
novogotiky	novogotika	k1gFnPc4	novogotika
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
anglického	anglický	k2eAgInSc2d1	anglický
Windsoru	Windsor	k1gInSc2	Windsor
<g/>
.	.	kIx.	.
</s>
<s>
Městečko	městečko	k1gNnSc1	městečko
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
neslo	nést	k5eAaImAgNnS	nést
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
prostý	prostý	k2eAgInSc4d1	prostý
název	název	k1gInSc4	název
Podhradí	Podhradí	k1gNnSc1	Podhradí
či	či	k8xC	či
Podhrad	Podhrad	k1gInSc1	Podhrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
úředně	úředně	k6eAd1	úředně
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Hluboká	hluboký	k2eAgNnPc4d1	hluboké
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
přívlastkem	přívlastek	k1gInSc7	přívlastek
do	do	k7c2	do
nynější	nynější	k2eAgFnSc2d1	nynější
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
stala	stát	k5eAaPmAgFnS	stát
spojením	spojení	k1gNnSc7	spojení
osad	osada	k1gFnPc2	osada
Podhrad	Podhrad	k1gInSc4	Podhrad
<g/>
,	,	kIx,	,
Hamr	hamr	k1gInSc4	hamr
<g/>
,	,	kIx,	,
Podskalí	Podskalí	k1gNnSc4	Podskalí
a	a	k8xC	a
Zámostí	Zámost	k1gFnSc7	Zámost
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
městyse	městys	k1gInSc2	městys
povýšena	povýšen	k2eAgFnSc1d1	povýšena
dekretem	dekret	k1gInSc7	dekret
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
Za	za	k7c4	za
propůjčení	propůjčení	k1gNnSc4	propůjčení
městského	městský	k2eAgInSc2d1	městský
znaku	znak	k1gInSc2	znak
tehdy	tehdy	k6eAd1	tehdy
radní	radní	k1gMnPc1	radní
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
210	[number]	k4	210
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
vyhotovení	vyhotovení	k1gNnSc1	vyhotovení
diplomu	diplom	k1gInSc2	diplom
je	on	k3xPp3gNnSc4	on
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c6	na
350	[number]	k4	350
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
turistického	turistický	k2eAgNnSc2d1	turistické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
velice	velice	k6eAd1	velice
atraktivní	atraktivní	k2eAgMnPc1d1	atraktivní
<g/>
,	,	kIx,	,
největším	veliký	k2eAgNnSc7d3	veliký
lákadlem	lákadlo	k1gNnSc7	lákadlo
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
je	být	k5eAaImIp3nS	být
novogotický	novogotický	k2eAgInSc1d1	novogotický
Zámek	zámek	k1gInSc1	zámek
Hluboká	Hluboká	k1gFnSc1	Hluboká
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
další	další	k2eAgInPc4d1	další
atraktivní	atraktivní	k2eAgInPc4d1	atraktivní
cíle	cíl	k1gInPc4	cíl
<g/>
:	:	kIx,	:
Zoo	zoo	k1gFnSc1	zoo
Ohrada	ohrada	k1gFnSc1	ohrada
<g/>
,	,	kIx,	,
přepychové	přepychový	k2eAgNnSc1d1	přepychové
golfové	golfový	k2eAgNnSc1d1	golfové
hřiště	hřiště	k1gNnSc1	hřiště
<g/>
,	,	kIx,	,
Knížecí	knížecí	k2eAgInSc1d1	knížecí
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
Alšova	Alšův	k2eAgFnSc1d1	Alšova
jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejkomplexnějších	komplexní	k2eAgInPc2d3	nejkomplexnější
sportovně	sportovně	k6eAd1	sportovně
relaxačních	relaxační	k2eAgInPc2d1	relaxační
areálů	areál	k1gInPc2	areál
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
kilometry	kilometr	k1gInPc4	kilometr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
rybník	rybník	k1gInSc1	rybník
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Bezdrev	Bezdrev	k1gInSc1	Bezdrev
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
provozují	provozovat	k5eAaImIp3nP	provozovat
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jachting	jachting	k1gInSc1	jachting
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Podskalské	podskalský	k2eAgFnSc6d1	Podskalská
louce	louka	k1gFnSc6	louka
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
každoročně	každoročně	k6eAd1	každoročně
pořádaly	pořádat	k5eAaImAgInP	pořádat
dostihy	dostih	k1gInPc1	dostih
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
závodiště	závodiště	k1gNnSc1	závodiště
upraveno	upravit	k5eAaPmNgNnS	upravit
na	na	k7c4	na
golfové	golfový	k2eAgNnSc4d1	golfové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Hluboké	Hluboká	k1gFnSc6	Hluboká
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
HC	HC	kA	HC
Hluboká	Hluboká	k1gFnSc1	Hluboká
Knights	Knights	k1gInSc1	Knights
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
turistické	turistický	k2eAgFnSc2d1	turistická
oblasti	oblast	k1gFnSc2	oblast
Českobudějovicko-Hlubocko	Českobudějovicko-Hlubocko	k1gNnSc1	Českobudějovicko-Hlubocko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Hluboké	Hluboká	k1gFnSc6	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Neustadt	Neustadt	k1gInSc1	Neustadt
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Aisch	Aisch	k1gInSc1	Aisch
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Grein	Greina	k1gFnPc2	Greina
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Donau	donau	k1gNnSc1	donau
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Bollingen	Bollingen	k1gInSc1	Bollingen
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
MATLAS	MATLAS	kA	MATLAS
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Shovívavá	shovívavý	k2eAgFnSc1d1	shovívavá
vrchnost	vrchnost	k1gFnSc1	vrchnost
a	a	k8xC	a
neukáznění	ukázněný	k2eNgMnPc1d1	neukázněný
poddaní	poddaný	k1gMnPc1	poddaný
<g/>
?	?	kIx.	?
</s>
<s>
:	:	kIx,	:
hranice	hranice	k1gFnPc4	hranice
trestní	trestní	k2eAgFnSc2d1	trestní
disciplinace	disciplinace	k1gFnSc2	disciplinace
poddaného	poddaný	k2eAgNnSc2d1	poddané
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
panství	panství	k1gNnSc6	panství
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
325	[number]	k4	325
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
257	[number]	k4	257
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
382	[number]	k4	382
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
11	[number]	k4	11
<g/>
,	,	kIx,	,
str	str	kA	str
376	[number]	k4	376
Synagoga	synagoga	k1gFnSc1	synagoga
v	v	k7c6	v
Hluboké	Hluboká	k1gFnSc6	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
v	v	k7c6	v
Hluboké	Hluboká	k1gFnSc6	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Neoficiální	oficiální	k2eNgFnSc2d1	neoficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Zámek	zámek	k1gInSc1	zámek
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
Turistický	turistický	k2eAgMnSc1d1	turistický
průvodce	průvodce	k1gMnSc1	průvodce
Podrobný	podrobný	k2eAgInSc4d1	podrobný
profil	profil	k1gInSc4	profil
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
příloha	příloha	k1gFnSc1	příloha
Strategie	strategie	k1gFnSc1	strategie
rozvoje	rozvoj	k1gInSc2	rozvoj
města	město	k1gNnSc2	město
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
Statistické	statistický	k2eAgInPc1d1	statistický
údaje	údaj	k1gInPc1	údaj
města	město	k1gNnSc2	město
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
Galerie	galerie	k1gFnSc2	galerie
fotografií	fotografia	k1gFnPc2	fotografia
zámku	zámek	k1gInSc2	zámek
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
Knights	Knightsa	k1gFnPc2	Knightsa
TV	TV	kA	TV
DIVE	div	k1gInSc5	div
Festival	festival	k1gInSc4	festival
Voda	voda	k1gFnSc1	voda
Moře	moře	k1gNnSc2	moře
Oceány	oceán	k1gInPc1	oceán
-	-	kIx~	-
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
9	[number]	k4	9
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
</s>
