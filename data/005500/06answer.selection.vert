<s>
Hluboká	Hluboká	k1gFnSc1	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Frauenberg	Frauenberg	k1gInSc1	Frauenberg
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Fronburg	Fronburg	k1gInSc1	Fronburg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
ležící	ležící	k2eAgNnSc1d1	ležící
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
po	po	k7c6	po
obou	dva	k4xCgInPc6	dva
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
zhruba	zhruba	k6eAd1	zhruba
9	[number]	k4	9
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
