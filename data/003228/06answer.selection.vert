<s>
Popularita	popularita	k1gFnSc1	popularita
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xC	jako
Morbid	Morbid	k1gInSc1	Morbid
Angel	Angela	k1gFnPc2	Angela
těšily	těšit	k5eAaImAgFnP	těšit
komerčnímu	komerční	k2eAgInSc3d1	komerční
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
;	;	kIx,	;
avšak	avšak	k8xC	avšak
žánr	žánr	k1gInSc1	žánr
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
extrémní	extrémní	k2eAgFnSc3d1	extrémní
povaze	povaha	k1gFnSc3	povaha
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
do	do	k7c2	do
středního	střední	k2eAgInSc2d1	střední
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
