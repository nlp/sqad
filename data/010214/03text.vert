<p>
<s>
Gibson	Gibson	k1gMnSc1	Gibson
ES-335	ES-335	k1gMnSc1	ES-335
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
legendární	legendární	k2eAgFnSc1d1	legendární
sériově	sériově	k6eAd1	sériově
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
pololubová	pololubový	k2eAgFnSc1d1	pololubový
kytara	kytara	k1gFnSc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
kusy	kus	k1gInPc1	kus
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
jako	jako	k8xS	jako
série	série	k1gFnSc1	série
ES	ES	kA	ES
(	(	kIx(	(
<g/>
Elektrické	elektrický	k2eAgFnSc2d1	elektrická
španělky	španělka	k1gFnSc2	španělka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kytara	kytara	k1gFnSc1	kytara
je	být	k5eAaImIp3nS	být
osazena	osadit	k5eAaPmNgFnS	osadit
dvěma	dva	k4xCgInPc7	dva
snímači	snímač	k1gInPc7	snímač
typu	typ	k1gInSc2	typ
Humbucker	Humbuckero	k1gNnPc2	Humbuckero
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
modely	model	k1gInPc1	model
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
vybavené	vybavený	k2eAgNnSc1d1	vybavené
dvojzvratným	dvojzvratný	k2eAgNnSc7d1	dvojzvratné
tremolem	tremolo	k1gNnSc7	tremolo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
Gibson	Gibson	k1gMnSc1	Gibson
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
jen	jen	k9	jen
lubové	lubový	k2eAgFnPc4d1	lubová
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
větším	veliký	k2eAgNnSc6d2	veliký
zesílení	zesílení	k1gNnSc6	zesílení
náchylné	náchylný	k2eAgFnSc2d1	náchylná
ke	k	k7c3	k
zpětné	zpětný	k2eAgFnSc3d1	zpětná
vazbě	vazba	k1gFnSc3	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
bylo	být	k5eAaImAgNnS	být
představeno	představit	k5eAaPmNgNnS	představit
několik	několik	k4yIc1	několik
modelů	model	k1gInPc2	model
s	s	k7c7	s
masivním	masivní	k2eAgNnSc7d1	masivní
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
mnohem	mnohem	k6eAd1	mnohem
lepší	dobrý	k2eAgFnSc4d2	lepší
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
zpětné	zpětný	k2eAgFnSc3d1	zpětná
vazbě	vazba	k1gFnSc3	vazba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
postrádaly	postrádat	k5eAaImAgFnP	postrádat
teplejší	teplý	k2eAgInSc4d2	teplejší
tón	tón	k1gInSc4	tón
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
lubových	lubový	k2eAgMnPc2d1	lubový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
tenhle	tenhle	k3xDgInSc4	tenhle
fakt	fakt	k1gInSc4	fakt
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kytara	kytara	k1gFnSc1	kytara
ES-	ES-	k1gFnSc1	ES-
<g/>
335	[number]	k4	335
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
kompromisem	kompromis	k1gInSc7	kompromis
mezi	mezi	k7c7	mezi
masivním	masivní	k2eAgNnSc7d1	masivní
a	a	k8xC	a
lubovým	lubový	k2eAgNnSc7d1	lubový
tělem	tělo	k1gNnSc7	tělo
<g/>
:	:	kIx,	:
teplejší	teplý	k2eAgInSc4d2	teplejší
tón	tón	k1gInSc4	tón
než	než	k8xS	než
nástroj	nástroj	k1gInSc4	nástroj
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
nulová	nulový	k2eAgFnSc1d1	nulová
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
těla	tělo	k1gNnSc2	tělo
prochází	procházet	k5eAaImIp3nS	procházet
blok	blok	k1gInSc1	blok
masivního	masivní	k2eAgNnSc2d1	masivní
dřeva	dřevo	k1gNnSc2	dřevo
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
o	o	k7c4	o
boční	boční	k2eAgInPc4d1	boční
luby	lub	k1gInPc4	lub
<g/>
.	.	kIx.	.
</s>
<s>
Luby	lub	k1gInPc1	lub
jsou	být	k5eAaImIp3nP	být
duté	dutý	k2eAgInPc1d1	dutý
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
desce	deska	k1gFnSc6	deska
jsou	být	k5eAaImIp3nP	být
vyřezány	vyřezat	k5eAaPmNgInP	vyřezat
dva	dva	k4xCgInPc1	dva
otvory	otvor	k1gInPc1	otvor
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
f	f	k?	f
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
efa	efa	k?	efa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umělci	umělec	k1gMnPc1	umělec
==	==	k?	==
</s>
</p>
<p>
<s>
Umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
hráli	hrát	k5eAaImAgMnP	hrát
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
model	model	k1gInSc4	model
kytary	kytara	k1gFnSc2	kytara
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Alvin	Alvin	k1gMnSc1	Alvin
Lee	Lea	k1gFnSc3	Lea
</s>
</p>
<p>
<s>
B.	B.	kA	B.
B.	B.	kA	B.
King	King	k1gMnSc1	King
</s>
</p>
<p>
<s>
Dave	Dav	k1gInSc5	Dav
Grohl	Grohl	k1gFnSc7	Grohl
</s>
</p>
<p>
<s>
Dennis	Dennis	k1gFnSc1	Dennis
Coffey	Coffea	k1gFnSc2	Coffea
</s>
</p>
<p>
<s>
Eric	Eric	k6eAd1	Eric
Clapton	Clapton	k1gInSc1	Clapton
</s>
</p>
<p>
<s>
Eric	Eric	k6eAd1	Eric
Johnson	Johnson	k1gMnSc1	Johnson
</s>
</p>
<p>
<s>
Johnny	Johnna	k1gFnPc1	Johnna
Marr	Marra	k1gFnPc2	Marra
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
McLaughlin	McLaughlina	k1gFnPc2	McLaughlina
</s>
</p>
<p>
<s>
Chuck	Chuck	k1gInSc1	Chuck
Berry	Berra	k1gFnSc2	Berra
</s>
</p>
<p>
<s>
Larry	Larra	k1gFnPc1	Larra
Carlton	Carlton	k1gInSc1	Carlton
</s>
</p>
<p>
<s>
Sheryl	Sheryl	k1gInSc1	Sheryl
Crow	Crow	k1gFnSc2	Crow
</s>
</p>
<p>
<s>
The	The	k?	The
Edge	Edge	k1gInSc1	Edge
</s>
</p>
<p>
<s>
Tal	Tal	k?	Tal
Farlow	Farlow	k1gFnSc1	Farlow
</s>
</p>
<p>
<s>
Slash	Slash	k1gMnSc1	Slash
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gibson	Gibsona	k1gFnPc2	Gibsona
ES-335	ES-335	k1gFnSc2	ES-335
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
kytary	kytara	k1gFnSc2	kytara
</s>
</p>
