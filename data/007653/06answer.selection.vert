<s>
Roku	rok	k1gInSc2	rok
1864	[number]	k4	1864
byl	být	k5eAaImAgMnS	být
Erben	Erben	k1gMnSc1	Erben
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ředitelem	ředitel	k1gMnSc7	ředitel
pomocných	pomocný	k2eAgFnPc2d1	pomocná
kanceláří	kancelář	k1gFnPc2	kancelář
úřadů	úřad	k1gInPc2	úřad
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
také	také	k9	také
vydává	vydávat	k5eAaImIp3nS	vydávat
Prostonárodní	prostonárodní	k2eAgFnPc4d1	prostonárodní
české	český	k2eAgFnPc4d1	Česká
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
říkadla	říkadlo	k1gNnPc4	říkadlo
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Sto	sto	k4xCgNnSc4	sto
prostonárodních	prostonárodní	k2eAgFnPc2d1	prostonárodní
pohádek	pohádka	k1gFnPc2	pohádka
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
v	v	k7c6	v
nářečích	nářečí	k1gNnPc6	nářečí
původních	původní	k2eAgNnPc6d1	původní
<g/>
.	.	kIx.	.
</s>
