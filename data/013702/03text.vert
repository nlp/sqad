<s>
Sajpulla	Sajpulla	k6eAd1
Absaidov	Absaidov	k1gInSc1
</s>
<s>
Sajpulla	Sajpulla	k6eAd1
Absaidov	Absaidov	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1958	#num#	k4
(	(	kIx(
<g/>
62	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Tarki	Tark	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Dagestánská	dagestánský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
zápasník	zápasník	k1gMnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Zasloužilý	zasloužilý	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
sportu	sport	k1gInSc2
SSSR	SSSR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Zápas	zápas	k1gInSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
LOH	LOH	kA
1980	#num#	k4
</s>
<s>
volný	volný	k2eAgInSc4d1
styl	styl	k1gInSc4
do	do	k7c2
68	#num#	k4
kg	kg	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
zápasu	zápas	k1gInSc6
ve	v	k7c6
volném	volný	k2eAgInSc6d1
stylu	styl	k1gInSc6
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
1981	#num#	k4
</s>
<s>
lehká	lehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
Sajpulla	Sajpulla	k1gMnSc1
Atavovič	Atavovič	k1gMnSc1
Absaidov	Absaidov	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
С	С	k?
А	А	k?
А	А	k?
<g/>
;	;	kIx,
*	*	kIx~
14	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1958	#num#	k4
Tarki	Tarki	k1gNnSc1
<g/>
,	,	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
sovětský	sovětský	k2eAgMnSc1d1
zápasník	zápasník	k1gMnSc1
<g/>
,	,	kIx,
volnostylař	volnostylař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
v	v	k7c6
kategorii	kategorie	k1gFnSc6
do	do	k7c2
68	#num#	k4
kg	kg	kA
vybojoval	vybojovat	k5eAaPmAgInS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
také	také	k9
vybojoval	vybojovat	k5eAaPmAgMnS
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
o	o	k7c4
jednu	jeden	k4xCgFnSc4
příčku	příčka	k1gFnSc4
vylepšil	vylepšit	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
umístění	umístění	k1gNnSc4
z	z	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
vybojoval	vybojovat	k5eAaPmAgMnS
titul	titul	k1gInSc4
mistra	mistr	k1gMnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2009	#num#	k4
nahradil	nahradit	k5eAaPmAgMnS
na	na	k7c6
postu	post	k1gInSc6
hlavního	hlavní	k2eAgMnSc2d1
trenéra	trenér	k1gMnSc2
ázerbájdžánské	ázerbájdžánský	k2eAgFnSc2d1
volnostylařské	volnostylařský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
Magomedchana	Magomedchan	k1gMnSc2
Aracilova	Aracilův	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Profil	profil	k1gInSc1
na	na	k7c6
sports-reference	sports-referenka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
com	com	k?
Archivováno	archivován	k2eAgNnSc1d1
7	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
С	С	k?
А	А	k?
п	п	k?
в	в	k?
б	б	k?
в	в	k?
С	С	k?
А	А	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Sajpulla	Sajpulla	k6eAd1
Absaidov	Absaidov	k1gInSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
v	v	k7c6
zápase	zápas	k1gInSc6
ve	v	k7c6
volném	volný	k2eAgInSc6d1
stylu	styl	k1gInSc6
-	-	kIx~
lehká	lehký	k2eAgFnSc1d1
váha	váha	k1gFnSc1
</s>
<s>
1904	#num#	k4
<g/>
:	:	kIx,
Otto	Otto	k1gMnSc1
Roehm	Roehma	k1gFnPc2
•	•	k?
1908	#num#	k4
<g/>
:	:	kIx,
George	Georg	k1gInSc2
de	de	k?
Relwyskow	Relwyskow	k1gFnSc2
•	•	k?
1912	#num#	k4
nebyl	být	k5eNaImAgInS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1920	#num#	k4
<g/>
:	:	kIx,
Kalle	Kalle	k1gInSc1
Anttila	Anttil	k1gMnSc2
•	•	k?
1924	#num#	k4
<g/>
:	:	kIx,
Russell	Russell	k1gInSc1
Vis	vis	k1gInSc1
•	•	k?
1928	#num#	k4
<g/>
:	:	kIx,
Osvald	Osvald	k1gInSc1
Käpp	Käpp	k1gInSc1
•	•	k?
1932	#num#	k4
<g/>
:	:	kIx,
Charles	Charles	k1gMnSc1
Pacôme	Pacôm	k1gInSc5
•	•	k?
1936	#num#	k4
<g/>
:	:	kIx,
Károly	Károla	k1gFnSc2
Kárpáti	Kárpát	k1gMnPc1
•	•	k?
1948	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Celal	Celal	k1gMnSc1
Atik	Atika	k1gFnPc2
•	•	k?
1952	#num#	k4
<g/>
:	:	kIx,
Olle	Oll	k1gInSc2
Anderberg	Anderberg	k1gInSc1
•	•	k?
1956	#num#	k4
<g/>
:	:	kIx,
Emámalí	Emámalý	k2eAgMnPc1d1
Habíbí	Habíbí	k1gNnSc4
•	•	k?
1960	#num#	k4
<g/>
:	:	kIx,
Shelby	Shelba	k1gFnSc2
Wilson	Wilson	k1gInSc1
•	•	k?
1964	#num#	k4
<g/>
:	:	kIx,
Enju	Enjus	k1gInSc2
Valčev	Valčev	k1gFnSc2
•	•	k?
1968	#num#	k4
<g/>
:	:	kIx,
Abdollah	Abdollah	k1gInSc1
Movahed	Movahed	k1gInSc1
•	•	k?
1972	#num#	k4
<g/>
:	:	kIx,
Dan	Dan	k1gMnSc1
Gable	Gable	k1gFnSc2
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Pinigin	Pinigina	k1gFnPc2
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
Sajpulla	Sajpulla	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Absaidov	Absaidov	k1gInSc1
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Ju	ju	k0
In-tchak	In-tchak	k1gInSc4
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Arsen	arsen	k1gInSc1
Fadzajev	Fadzajev	k1gFnSc2
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Arsen	arsen	k1gInSc1
Fadzajev	Fadzajev	k1gFnSc2
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Vadim	Vadim	k?
Bogijev	Bogijev	k1gFnSc1
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Daniel	Daniel	k1gMnSc1
Igali	Igale	k1gFnSc4
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Elbrus	Elbrus	k1gInSc1
Tedejev	Tedejev	k1gFnSc2
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Ramazan	ramazan	k1gInSc1
Şahin	Şahin	k1gInSc1
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Tacuhiro	Tacuhiro	k1gNnSc1
Jonemicu	Jonemicus	k1gInSc2
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Soslan	Soslana	k1gFnPc2
Ramonov	Ramonovo	k1gNnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
zápasu	zápas	k1gInSc6
ve	v	k7c6
volném	volný	k2eAgInSc6d1
stylu	styl	k1gInSc6
-	-	kIx~
zápas	zápas	k1gInSc4
mužů	muž	k1gMnPc2
65	#num#	k4
kg	kg	kA
</s>
<s>
1951	#num#	k4
Olle	Olle	k1gInSc1
Anderberg	Anderberg	k1gInSc4
•	•	k?
1954	#num#	k4
Jahanbakht	Jahanbakht	k2eAgInSc4d1
Tofigh	Tofigh	k1gInSc4
•	•	k?
1957	#num#	k4
Alimbeg	Alimbeg	k1gInSc1
Bestajev	Bestajev	k1gFnSc2
•	•	k?
1959	#num#	k4
Vladimir	Vladimir	k1gInSc1
Siňavskij	Siňavskij	k1gFnSc2
•	•	k?
1961	#num#	k4
Mohammad	Mohammad	k1gInSc1
Ali	Ali	k1gFnSc2
Sanatkaran	Sanatkarana	k1gFnPc2
•	•	k?
1962	#num#	k4
Enju	Enj	k2eAgFnSc4d1
Valčev	Valčev	k1gFnSc4
•	•	k?
1963	#num#	k4
Iwao	Iwao	k6eAd1
Horiuči	Horiuč	k1gInSc3
•	•	k?
1965	#num#	k4
Abdollah	Abdollah	k1gInSc1
Movahed	Movahed	k1gInSc4
•	•	k?
1966	#num#	k4
Abdollah	Abdollah	k1gInSc1
Movahed	Movahed	k1gInSc4
•	•	k?
1967	#num#	k4
Abdollah	Abdollah	k1gInSc1
Movahed	Movahed	k1gInSc4
•	•	k?
1969	#num#	k4
Abdollah	Abdollah	k1gInSc1
Movahed	Movahed	k1gInSc4
•	•	k?
1970	#num#	k4
Abdollah	Abdollah	k1gInSc1
Movahed	Movahed	k1gInSc4
•	•	k?
1971	#num#	k4
Dan	Dan	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Gable	Gable	k1gFnSc1
•	•	k?
1973	#num#	k4
Lloyd	Lloyd	k1gInSc1
Keaser	Keaser	k1gInSc4
•	•	k?
1974	#num#	k4
Nasrula	Nasrula	k1gFnSc1
Nasrulajev	Nasrulajev	k1gFnSc1
•	•	k?
1975	#num#	k4
Pavel	Pavel	k1gMnSc1
Pinigin	Pinigin	k1gMnSc1
•	•	k?
1977	#num#	k4
Pavel	Pavel	k1gMnSc1
Pinigin	Pinigin	k1gMnSc1
•	•	k?
1978	#num#	k4
Pavel	Pavel	k1gMnSc1
Pinigin	Pinigin	k1gMnSc1
•	•	k?
1979	#num#	k4
Michail	Michaila	k1gFnPc2
Charačura	Charačura	k1gFnSc1
•	•	k?
1981	#num#	k4
Sajpulla	Sajpullo	k1gNnSc2
Absaidov	Absaidov	k1gInSc1
•	•	k?
1982	#num#	k4
Mifhail	Mifhaila	k1gFnPc2
Charačura	Charačura	k1gFnSc1
•	•	k?
1983	#num#	k4
Arsen	arsen	k1gInSc1
Fadzajev	Fadzajev	k1gFnSc2
•	•	k?
1985	#num#	k4
Arsen	arsen	k1gInSc1
Fadzajev	Fadzajev	k1gFnSc2
•	•	k?
1986	#num#	k4
Arsen	arsen	k1gInSc1
Fadzajev	Fadzajev	k1gFnSc2
•	•	k?
1987	#num#	k4
Arsen	arsen	k1gInSc1
Fadzajev	Fadzajev	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1989	#num#	k4
Boris	Boris	k1gMnSc1
Budayev	Budayva	k1gFnPc2
•	•	k?
1990	#num#	k4
Arsen	arsen	k1gInSc1
Fadzajev	Fadzajev	k1gFnSc2
•	•	k?
1991	#num#	k4
Arsen	arsen	k1gInSc1
Fadzajev	Fadzajev	k1gFnSc2
•	•	k?
1993	#num#	k4
Akbar	Akbar	k1gInSc1
Falláh	Falláh	k1gInSc4
•	•	k?
1994	#num#	k4
Alexander	Alexandra	k1gFnPc2
Leipold	Leipolda	k1gFnPc2
•	•	k?
1995	#num#	k4
Arajik	Arajik	k1gInSc1
Gevorgjan	Gevorgjan	k1gInSc4
•	•	k?
1997	#num#	k4
Arajik	Arajik	k1gInSc1
Gevorgjan	Gevorgjan	k1gInSc4
•	•	k?
<g/>
1998	#num#	k4
Arajik	Arajik	k1gInSc1
Gevorgjan	Gevorgjan	k1gInSc4
•	•	k?
1999	#num#	k4
Daniel	Daniela	k1gFnPc2
Igali	Igale	k1gFnSc4
•	•	k?
2001	#num#	k4
Nikolaj	Nikolaj	k1gMnSc1
Paslar	Paslar	k1gMnSc1
•	•	k?
2002	#num#	k4
Elbrus	Elbrus	k1gInSc1
Tedejev	Tedejev	k1gFnSc2
•	•	k?
2003	#num#	k4
Irbek	Irbek	k1gInSc1
Farnijev	Farnijev	k1gFnSc2
•	•	k?
2005	#num#	k4
Machač	Machač	k1gMnSc1
Murtazalijev	Murtazalijev	k1gMnSc1
•	•	k?
2006	#num#	k4
Bill	Bill	k1gMnSc1
Zadick	Zadick	k1gMnSc1
•	•	k?
2007	#num#	k4
Ramazan	ramazan	k1gInSc1
Şahin	Şahin	k1gInSc4
•	•	k?
2009	#num#	k4
Mahdí	Mahdí	k1gNnPc2
Tagáví	Tagáev	k1gFnPc2
•	•	k?
2010	#num#	k4
Sušíl	Sušíl	k1gInSc1
Kumár	Kumár	k1gInSc4
•	•	k?
2011	#num#	k4
Mahdí	Mahdí	k1gNnPc2
Tagáví	Tagáev	k1gFnPc2
•	•	k?
2013	#num#	k4
Davit	Davit	k2eAgInSc4d1
Safarjan	Safarjan	k1gInSc4
•	•	k?
2014	#num#	k4
Soslan	Soslan	k1gInSc1
Ramonov	Ramonov	k1gInSc4
•	•	k?
2015	#num#	k4
Frank	frank	k1gInSc1
Chamizo	Chamiza	k1gFnSc5
•	•	k?
2017	#num#	k4
Zurab	Zurab	k1gInSc1
Ijakobišvili	Ijakobišvili	k1gFnSc2
•	•	k?
2018	#num#	k4
Takuto	Takut	k2eAgNnSc1d1
Otoguro	Otogura	k1gFnSc5
•	•	k?
2019	#num#	k4
Gadžimurad	Gadžimurad	k1gInSc1
Rašidov	Rašidov	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
