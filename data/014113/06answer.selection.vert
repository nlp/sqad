<s>
Společně	společně	k6eAd1
s	s	k7c7
Friedrichem	Friedrich	k1gMnSc7
Engelsem	Engels	k1gMnSc7
rozpracoval	rozpracovat	k5eAaPmAgMnS
vlastní	vlastní	k2eAgMnSc1d1
materialistické	materialistický	k2eAgNnSc4d1
pojetí	pojetí	k1gNnSc4
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
založené	založený	k2eAgInPc1d1
na	na	k7c6
ekonomických	ekonomický	k2eAgFnPc6d1
zákonitostech	zákonitost	k1gFnPc6
<g/>
.	.	kIx.
</s>