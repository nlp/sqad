<s desamb="1">
Ve	v	k7c6
společnosti	společnost	k1gFnSc6
je	být	k5eAaImIp3nS
podle	podle	k7c2
nich	on	k3xPp3gNnPc2
přítomen	přítomno	k1gNnPc2
konflikt	konflikt	k1gInSc1
mezi	mezi	k7c7
ovládanými	ovládaný	k2eAgFnPc7d1
a	a	k8xC
vládnoucími	vládnoucí	k2eAgFnPc7d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
bude	být	k5eAaImBp3nS
odstraněn	odstranit	k5eAaPmNgInS
zrušením	zrušení	k1gNnSc7
soukromého	soukromý	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
nastolením	nastolení	k1gNnSc7
beztřídní	beztřídní	k2eAgFnSc2d1
<g/>
,	,	kIx,
komunistické	komunistický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>