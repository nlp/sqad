<s>
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Marx	Marx	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
filosofovi	filosof	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jiných	jiný	k2eAgFnPc6d1
nositelích	nositel	k1gMnPc6
jména	jméno	k1gNnPc4
Marx	Marx	k1gMnSc1
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Marx	Marx	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
</s>
<s>
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1875	#num#	k4
<g/>
Celé	celý	k2eAgInPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Karl	Karl	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Marx	Marx	k1gMnSc1
Období	období	k1gNnSc1
</s>
<s>
Filosofie	filosofie	k1gFnSc1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1818	#num#	k4
TrevírNěmecký	TrevírNěmecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Německý	německý	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1883	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
LondýnSpojené	LondýnSpojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Škola	škola	k1gFnSc1
<g/>
/	/	kIx~
<g/>
tradice	tradice	k1gFnSc1
</s>
<s>
Marxismus	marxismus	k1gInSc1
<g/>
,	,	kIx,
komunismus	komunismus	k1gInSc1
<g/>
,	,	kIx,
socialismus	socialismus	k1gInSc1
<g/>
,	,	kIx,
materialismus	materialismus	k1gInSc1
Oblasti	oblast	k1gFnSc2
zájmu	zájem	k1gInSc2
</s>
<s>
Politika	politika	k1gFnSc1
<g/>
,	,	kIx,
ekonomie	ekonomie	k1gFnSc1
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc1
<g/>
,	,	kIx,
sociologie	sociologie	k1gFnSc1
<g/>
,	,	kIx,
historie	historie	k1gFnSc1
<g/>
,	,	kIx,
třídní	třídní	k2eAgInSc1d1
boj	boj	k1gInSc1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
Významná	významný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Economic	Economic	k1gMnSc1
and	and	k?
Philosophic	Philosophic	k1gMnSc1
Manuscripts	Manuscripts	k1gInSc1
of	of	k?
1844	#num#	k4
<g/>
,	,	kIx,
Kapitál	kapitál	k1gInSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
German	German	k1gMnSc1
Ideology	ideolog	k1gMnPc4
a	a	k8xC
Komunistický	komunistický	k2eAgInSc1d1
manifest	manifest	k1gInSc1
Vlivy	vliv	k1gInPc1
</s>
<s>
Hegel	Hegel	k1gMnSc1
<g/>
,	,	kIx,
Feuerbach	Feuerbach	k1gMnSc1
<g/>
,	,	kIx,
Spinoza	Spinoza	k1gFnSc1
<g/>
,	,	kIx,
Proudhon	Proudhon	k1gMnSc1
<g/>
,	,	kIx,
Stirner	Stirner	k1gMnSc1
<g/>
,	,	kIx,
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Voltaire	Voltair	k1gMnSc5
<g/>
,	,	kIx,
Ricardo	Ricarda	k1gMnSc5
<g/>
,	,	kIx,
Vico	Vica	k1gMnSc5
<g/>
,	,	kIx,
Robespierre	Robespierr	k1gMnSc5
<g/>
,	,	kIx,
Rousseau	Rousseau	k1gMnSc1
<g/>
,	,	kIx,
Shakespeare	Shakespeare	k1gMnSc1
<g/>
,	,	kIx,
Goethe	Goethe	k1gFnSc1
<g/>
,	,	kIx,
Helvétius	Helvétius	k1gMnSc1
<g/>
,	,	kIx,
d	d	k?
<g/>
'	'	kIx"
<g/>
Holbach	Holbach	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Liebig	Liebig	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Darwin	Darwin	k1gMnSc1
<g/>
,	,	kIx,
Fourier	Fourier	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Owen	Owena	k1gFnPc2
<g/>
,	,	kIx,
Hess	Hessa	k1gFnPc2
<g/>
,	,	kIx,
Guizot	Guizota	k1gFnPc2
<g/>
,	,	kIx,
Aristotelés	Aristotelésa	k1gFnPc2
<g/>
,	,	kIx,
Epikúros	Epikúrosa	k1gFnPc2
Podpis	podpis	k1gInSc4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Karl	Karl	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Marx	Marx	k1gMnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
uváděn	uvádět	k5eAaImNgMnS
také	také	k9
jako	jako	k9
Karel	Karel	k1gMnSc1
Marx	Marx	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1818	#num#	k4
Trevír	Trevír	k1gInSc1
–	–	k?
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1883	#num#	k4
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
<g/>
,	,	kIx,
politický	politický	k2eAgMnSc1d1
publicista	publicista	k1gMnSc1
<g/>
,	,	kIx,
kritik	kritik	k1gMnSc1
klasické	klasický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
teoretik	teoretik	k1gMnSc1
dělnického	dělnický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
socialismu	socialismus	k1gInSc2
a	a	k8xC
komunismu	komunismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gNnPc4
nejznámější	známý	k2eAgNnPc4d3
díla	dílo	k1gNnPc4
patří	patřit	k5eAaImIp3nP
Ekonomicko-filosofické	ekonomicko-filosofický	k2eAgInPc1d1
rukopisy	rukopis	k1gInPc1
<g/>
,	,	kIx,
Komunistický	komunistický	k2eAgInSc1d1
manifest	manifest	k1gInSc1
a	a	k8xC
nedokončený	dokončený	k2eNgInSc1d1
Kapitál	kapitál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
Friedrichem	Friedrich	k1gMnSc7
Engelsem	Engels	k1gMnSc7
rozpracoval	rozpracovat	k5eAaPmAgMnS
vlastní	vlastní	k2eAgMnSc1d1
materialistické	materialistický	k2eAgNnSc4d1
pojetí	pojetí	k1gNnSc4
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
založené	založený	k2eAgInPc1d1
na	na	k7c6
ekonomických	ekonomický	k2eAgFnPc6d1
zákonitostech	zákonitost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
společnosti	společnost	k1gFnSc6
je	být	k5eAaImIp3nS
podle	podle	k7c2
nich	on	k3xPp3gNnPc2
přítomen	přítomno	k1gNnPc2
konflikt	konflikt	k1gInSc1
mezi	mezi	k7c7
ovládanými	ovládaný	k2eAgFnPc7d1
a	a	k8xC
vládnoucími	vládnoucí	k2eAgFnPc7d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1
bude	být	k5eAaImBp3nS
odstraněn	odstranit	k5eAaPmNgInS
zrušením	zrušení	k1gNnSc7
soukromého	soukromý	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
a	a	k8xC
nastolením	nastolení	k1gNnSc7
beztřídní	beztřídní	k2eAgFnSc2d1
<g/>
,	,	kIx,
komunistické	komunistický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
filosofický	filosofický	k2eAgInSc1d1
i	i	k8xC
politický	politický	k2eAgInSc1d1
vliv	vliv	k1gInSc1
byl	být	k5eAaImAgInS
obrovský	obrovský	k2eAgInSc1d1
a	a	k8xC
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
myšlenek	myšlenka	k1gFnPc2
vyšla	vyjít	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
směrů	směr	k1gInPc2
v	v	k7c6
levé	levý	k2eAgFnSc6d1
části	část	k1gFnSc6
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
oficiální	oficiální	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
socialistických	socialistický	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
marxismus-leninismus	marxismus-leninismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
nejrůznější	různý	k2eAgFnPc1d3
variace	variace	k1gFnPc1
neortodoxního	ortodoxní	k2eNgInSc2d1
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
například	například	k6eAd1
frankfurtská	frankfurtský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Marx	Marx	k1gMnSc1
v	v	k7c6
době	doba	k1gFnSc6
studentských	studentský	k2eAgNnPc2d1
let	léto	k1gNnPc2
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
třetí	třetí	k4xOgNnSc4
dítě	dítě	k1gNnSc4
do	do	k7c2
původně	původně	k6eAd1
židovské	židovský	k2eAgFnSc2d1
rabínské	rabínský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
v	v	k7c6
tehdy	tehdy	k6eAd1
pruském	pruský	k2eAgInSc6d1
Trevíru	Trevír	k1gInSc6
v	v	k7c6
Porýní	Porýní	k1gNnSc6
(	(	kIx(
<g/>
z	z	k7c2
českého	český	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
je	být	k5eAaImIp3nS
zajímavé	zajímavý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnPc1
předkové	předek	k1gMnPc1
pocházeli	pocházet	k5eAaImAgMnP
mj.	mj.	kA
z	z	k7c2
Postoloprt	Postoloprta	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
však	však	k9
těsně	těsně	k6eAd1
před	před	k7c7
jeho	jeho	k3xOp3gNnSc7
narozením	narození	k1gNnSc7
(	(	kIx(
<g/>
1816	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
konvertoval	konvertovat	k5eAaBmAgMnS
ke	k	k7c3
státnímu	státní	k2eAgNnSc3d1
luteránství	luteránství	k1gNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
nadále	nadále	k6eAd1
vykonávat	vykonávat	k5eAaImF
práci	práce	k1gFnSc4
advokáta	advokát	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1824	#num#	k4
pak	pak	k6eAd1
byly	být	k5eAaImAgFnP
pokřtěny	pokřtít	k5eAaPmNgFnP
také	také	k9
jeho	jeho	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Trevíru	Trevír	k1gInSc6
navštěvoval	navštěvovat	k5eAaImAgInS
gymnázium	gymnázium	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
budoucím	budoucí	k2eAgMnSc7d1
švagrem	švagr	k1gMnSc7
Edgarem	Edgar	k1gMnSc7
von	von	k1gInSc4
Westphalen	Westphalen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1836	#num#	k4
se	se	k3xPyFc4
zasnoubil	zasnoubit	k5eAaPmAgInS
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
sestrou	sestra	k1gFnSc7
Jenny	Jenna	k1gFnSc2
von	von	k1gInSc1
Westphalen	Westphalen	k2eAgInSc1d1
(	(	kIx(
<g/>
1814	#num#	k4
<g/>
–	–	k?
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
poté	poté	k6eAd1
strávil	strávit	k5eAaPmAgMnS
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Coby	Coby	k?
student	student	k1gMnSc1
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
skvěle	skvěle	k6eAd1
francouzsky	francouzsky	k6eAd1
a	a	k8xC
podnikl	podniknout	k5eAaPmAgMnS
několik	několik	k4yIc4
literárních	literární	k2eAgInPc2d1
pokusů	pokus	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
však	však	k9
záhy	záhy	k6eAd1
zavrhl	zavrhnout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
již	již	k9
studoval	studovat	k5eAaImAgMnS
práva	právo	k1gNnSc2
v	v	k7c6
Bonnu	Bonn	k1gInSc6
(	(	kIx(
<g/>
1835	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
poté	poté	k6eAd1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgInS
do	do	k7c2
bouřlivých	bouřlivý	k2eAgFnPc2d1
diskusí	diskuse	k1gFnPc2
nad	nad	k7c7
myšlenkovým	myšlenkový	k2eAgInSc7d1
odkazem	odkaz	k1gInSc7
nedávno	nedávno	k6eAd1
zemřelého	zemřelý	k2eAgMnSc4d1
Hegela	Hegel	k1gMnSc4
a	a	k8xC
přiklonil	přiklonit	k5eAaPmAgMnS
se	se	k3xPyFc4
k	k	k7c3
reformistům	reformista	k1gMnPc3
<g/>
,	,	kIx,
tzv.	tzv.	kA
mladohegelovcům	mladohegelovec	k1gMnPc3
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
poslouchal	poslouchat	k5eAaImAgMnS
Bruna	Bruna	k1gMnSc1
Bauera	Bauer	k1gMnSc2
a	a	k8xC
Ludwiga	Ludwig	k1gMnSc2
Feuerbacha	Feuerbach	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1841	#num#	k4
pak	pak	k6eAd1
in	in	k?
absentia	absentia	k1gFnSc1
promoval	promovat	k5eAaBmAgInS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Jeně	Jena	k1gFnSc6
prací	práce	k1gFnPc2
na	na	k7c4
téma	téma	k1gNnSc4
rozdílu	rozdíl	k1gInSc2
Démokritovy	Démokritův	k2eAgFnSc2d1
a	a	k8xC
Epikúrovy	Epikúrův	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1842	#num#	k4
začaly	začít	k5eAaPmAgInP
v	v	k7c6
Kolíně	Kolín	k1gInSc6
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
vycházet	vycházet	k5eAaImF
liberálně	liberálně	k6eAd1
zaměřené	zaměřený	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
Rheinische	Rheinisch	k1gInSc2
Zeitung	Zeitunga	k1gFnPc2
<g/>
;	;	kIx,
brzy	brzy	k6eAd1
si	se	k3xPyFc3
osvojil	osvojit	k5eAaPmAgInS
břitký	břitký	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
redakčních	redakční	k2eAgMnPc2d1
spolupracovníků	spolupracovník	k1gMnPc2
a	a	k8xC
posléze	posléze	k6eAd1
i	i	k8xC
šéfredaktorem	šéfredaktor	k1gMnSc7
tohoto	tento	k3xDgInSc2
listu	list	k1gInSc2
<g/>
,	,	kIx,
radikálně	radikálně	k6eAd1
opozičního	opoziční	k2eAgInSc2d1
vůči	vůči	k7c3
pruské	pruský	k2eAgFnSc3d1
politice	politika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ovšem	ovšem	k9
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
cenzurním	cenzurní	k2eAgInPc3d1
zásahům	zásah	k1gInPc3
státní	státní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
k	k	k7c3
úplnému	úplný	k2eAgInSc3d1
zákazu	zákaz	k1gInSc3
novin	novina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
setkal	setkat	k5eAaPmAgMnS
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
pozdějším	pozdní	k2eAgMnSc7d2
spolupracovníkem	spolupracovník	k1gMnSc7
Friedrichem	Friedrich	k1gMnSc7
Engelsem	Engels	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Paříži	Paříž	k1gFnSc6
a	a	k8xC
Bruselu	Brusel	k1gInSc6
</s>
<s>
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
a	a	k8xC
Friedrich	Friedrich	k1gMnSc1
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
vpředu	vpředu	k7c2
Marxovy	Marxův	k2eAgFnSc2d1
dcery	dcera	k1gFnSc2
Jenny	Jenna	k1gFnSc2
Caroline	Carolin	k1gInSc5
<g/>
,	,	kIx,
Jenny	Jenna	k1gMnSc2
Laura	Laura	k1gFnSc1
a	a	k8xC
Jenny	Jenna	k1gMnSc2
Julia	Julius	k1gMnSc2
Eleanor	Eleanor	k1gMnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1843	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Bad	Bad	k1gFnSc6
Kreuznachu	Kreuznach	k1gInSc2
oženil	oženit	k5eAaPmAgMnS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
snoubenkou	snoubenka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jejich	jejich	k3xOp3gMnPc2
sedmi	sedm	k4xCc2
dětí	dítě	k1gFnPc2
přežily	přežít	k5eAaPmAgFnP
jen	jen	k9
tři	tři	k4xCgFnPc1
dcery	dcera	k1gFnPc1
<g/>
:	:	kIx,
Jenny	Jenna	k1gFnPc1
<g/>
,	,	kIx,
Laura	Laura	k1gFnSc1
a	a	k8xC
Eleanor	Eleanor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
s	s	k7c7
rodinou	rodina	k1gFnSc7
odjel	odjet	k5eAaPmAgMnS
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zde	zde	k6eAd1
společně	společně	k6eAd1
s	s	k7c7
Arnoldem	Arnold	k1gMnSc7
Rugem	Rug	k1gMnSc7
vydával	vydávat	k5eAaPmAgMnS,k5eAaImAgMnS
Německo-francouzskou	německo-francouzský	k2eAgFnSc4d1
ročenku	ročenka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Paříži	Paříž	k1gFnSc6
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
německým	německý	k2eAgMnSc7d1
básníkem	básník	k1gMnSc7
Heinrichem	Heinrich	k1gMnSc7
Heinem	Hein	k1gMnSc7
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
vedl	vést	k5eAaImAgMnS
během	během	k7c2
pařížského	pařížský	k2eAgNnSc2d1
období	období	k1gNnSc2
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
politické	politický	k2eAgFnSc2d1
diskuse	diskuse	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Ročenky	ročenka	k1gFnSc2
však	však	k9
vyšlo	vyjít	k5eAaPmAgNnS
pouze	pouze	k6eAd1
jedno	jeden	k4xCgNnSc4
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
neshodám	neshoda	k1gFnPc3
jak	jak	k8xC,k8xS
s	s	k7c7
Rugem	Rug	k1gInSc7
<g/>
,	,	kIx,
tak	tak	k6eAd1
s	s	k7c7
francouzskými	francouzský	k2eAgMnPc7d1
socialisty	socialist	k1gMnPc7
<g/>
:	:	kIx,
v	v	k7c6
polemice	polemika	k1gFnSc6
s	s	k7c7
nimi	on	k3xPp3gInPc7
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
zabývat	zabývat	k5eAaImF
ekonomií	ekonomie	k1gFnSc7
a	a	k8xC
směřoval	směřovat	k5eAaImAgInS
ke	k	k7c3
komunistickým	komunistický	k2eAgFnPc3d1
pozicím	pozice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
posun	posun	k1gInSc1
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgInSc1d1
v	v	k7c6
Ekonomicko-politických	ekonomicko-politický	k2eAgInPc6d1
rukopisech	rukopis	k1gInPc6
z	z	k7c2
roku	rok	k1gInSc2
1844	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
zavádějí	zavádět	k5eAaImIp3nP
pojem	pojem	k1gInSc4
„	„	k?
<g/>
odcizené	odcizený	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Rukopisy	rukopis	k1gInPc1
však	však	k9
nedokončil	dokončit	k5eNaPmAgMnS
<g/>
;	;	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
jej	on	k3xPp3gMnSc4
navštívil	navštívit	k5eAaPmAgMnS
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
uzavřeli	uzavřít	k5eAaPmAgMnP
oba	dva	k4xCgMnPc1
muži	muž	k1gMnPc1
přátelství	přátelství	k1gNnSc2
<g/>
,	,	kIx,
vedli	vést	k5eAaImAgMnP
obsáhlou	obsáhlý	k2eAgFnSc4d1
korespondenci	korespondence	k1gFnSc4
a	a	k8xC
společně	společně	k6eAd1
sepsali	sepsat	k5eAaPmAgMnP
několik	několik	k4yIc4
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
polemizujících	polemizující	k2eAgFnPc2d1
s	s	k7c7
mladohegeliány	mladohegelián	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
pařížského	pařížský	k2eAgNnSc2d1
období	období	k1gNnSc2
byl	být	k5eAaImAgInS
tedy	tedy	k9
v	v	k7c6
základech	základ	k1gInPc6
formulován	formulován	k2eAgInSc4d1
historický	historický	k2eAgInSc4d1
materialismus	materialismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Engels	Engels	k1gMnSc1
zároveň	zároveň	k6eAd1
Marxe	Marx	k1gMnSc4
všemožně	všemožně	k6eAd1
podporoval	podporovat	k5eAaImAgMnS
<g/>
:	:	kIx,
posílal	posílat	k5eAaImAgMnS
mu	on	k3xPp3gMnSc3
kapesné	kapesné	k1gNnSc4
<g/>
,	,	kIx,
dělal	dělat	k5eAaImAgMnS
si	se	k3xPyFc3
starosti	starost	k1gFnPc1
s	s	k7c7
jeho	jeho	k3xOp3gInSc7
zdravotním	zdravotní	k2eAgInSc7d1
stavem	stav	k1gInSc7
<g/>
,	,	kIx,
nabádal	nabádat	k5eAaBmAgInS,k5eAaImAgInS
jej	on	k3xPp3gMnSc4
k	k	k7c3
systematičtější	systematický	k2eAgFnSc3d2
práci	práce	k1gFnSc3
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Engels	Engels	k1gMnSc1
sepsal	sepsat	k5eAaPmAgMnS
i	i	k9
mnoho	mnoho	k4c4
novinových	novinový	k2eAgInPc2d1
článků	článek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
vyšly	vyjít	k5eAaPmAgInP
pod	pod	k7c7
Marxovým	Marxův	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
1845	#num#	k4
musel	muset	k5eAaImAgMnS
Marx	Marx	k1gMnSc1
přesídlit	přesídlit	k5eAaPmF
do	do	k7c2
Bruselu	Brusel	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
jej	on	k3xPp3gMnSc4
následoval	následovat	k5eAaImAgMnS
Engels	Engels	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
společně	společně	k6eAd1
podnikli	podniknout	k5eAaPmAgMnP
studijní	studijní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
navázali	navázat	k5eAaPmAgMnP
kontakt	kontakt	k1gInSc4
s	s	k7c7
tamním	tamní	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
chartistů	chartista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
byl	být	k5eAaImAgMnS
i	i	k9
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Belgie	Belgie	k1gFnSc2
pronásledován	pronásledován	k2eAgInSc1d1
pruskými	pruský	k2eAgInPc7d1
úřady	úřad	k1gInPc7
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
raději	rád	k6eAd2
vzdal	vzdát	k5eAaPmAgInS
pruského	pruský	k2eAgNnSc2d1
občanství	občanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bruselu	Brusel	k1gInSc6
společně	společně	k6eAd1
s	s	k7c7
Engelsem	Engels	k1gMnSc7
založil	založit	k5eAaPmAgInS
Komunistický	komunistický	k2eAgInSc1d1
korespondenční	korespondenční	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
zastřešení	zastřešení	k1gNnSc1
různých	různý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
účastnících	účastnící	k2eAgFnPc2d1
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
již	již	k6eAd1
mezinárodního	mezinárodní	k2eAgNnSc2d1
dělnického	dělnický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1847	#num#	k4
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
Svazu	svaz	k1gInSc2
spravedlivých	spravedlivý	k2eAgInPc2d1
<g/>
,	,	kIx,
jejž	jenž	k3xRgInSc4
založil	založit	k5eAaPmAgMnS
raný	raný	k2eAgMnSc1d1
socialista	socialista	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Weitling	Weitling	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizace	organizace	k1gFnSc1
byla	být	k5eAaImAgFnS
poté	poté	k6eAd1
na	na	k7c6
sjezdu	sjezd	k1gInSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
přejmenována	přejmenován	k2eAgFnSc1d1
na	na	k7c4
Svaz	svaz	k1gInSc4
komunistů	komunista	k1gMnPc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
připravovat	připravovat	k5eAaImF
komunistický	komunistický	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
manifest	manifest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
zveřejněn	zveřejnit	k5eAaPmNgInS
v	v	k7c6
revolučním	revoluční	k2eAgInSc6d1
roce	rok	k1gInSc6
1848	#num#	k4
jakožto	jakožto	k8xS
Manifest	manifest	k1gInSc1
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1850	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
chtěli	chtít	k5eAaImAgMnP
s	s	k7c7
Engelsem	Engels	k1gMnSc7
proti	proti	k7c3
stanovám	stanova	k1gFnPc3
zřídit	zřídit	k5eAaPmF
pobočku	pobočka	k1gFnSc4
v	v	k7c6
Kolíně	Kolín	k1gInSc6
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
však	však	k9
z	z	k7c2
pololegálního	pololegální	k2eAgInSc2d1
svazu	svaz	k1gInSc2
vyloučeni	vyloučit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Politické	politický	k2eAgInPc1d1
otřesy	otřes	k1gInPc1
roku	rok	k1gInSc2
1848	#num#	k4
znamenaly	znamenat	k5eAaImAgFnP
pro	pro	k7c4
Marxe	Marx	k1gMnSc4
vyhoštění	vyhoštění	k1gNnSc2
z	z	k7c2
Belgie	Belgie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
pozvání	pozvání	k1gNnSc4
prozatímní	prozatímní	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
přesídlil	přesídlit	k5eAaPmAgInS
v	v	k7c6
únoru	únor	k1gInSc6
do	do	k7c2
Paříže	Paříž	k1gFnSc2
a	a	k8xC
v	v	k7c6
březnu	březen	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vypukla	vypuknout	k5eAaPmAgFnS
revoluce	revoluce	k1gFnSc1
i	i	k9
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
do	do	k7c2
Kolína	Kolín	k1gInSc2
<g/>
,	,	kIx,
doufaje	doufat	k5eAaImSgMnS
v	v	k7c4
revoluci	revoluce	k1gFnSc4
socialistickou	socialistický	k2eAgFnSc4d1
<g/>
;	;	kIx,
k	k	k7c3
té	ten	k3xDgFnSc3
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
přispět	přispět	k5eAaPmF
vydáváním	vydávání	k1gNnSc7
radikálního	radikální	k2eAgInSc2d1
listu	list	k1gInSc2
Neue	Neue	k1gNnSc2
Rheinische	Rheinische	k1gNnSc1
Zeitung	Zeitung	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1849	#num#	k4
však	však	k9
pruská	pruský	k2eAgFnSc1d1
reakční	reakční	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
redakci	redakce	k1gFnSc4
rozpustila	rozpustit	k5eAaPmAgFnS
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
vykázala	vykázat	k5eAaPmAgFnS
ze	z	k7c2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1844	#num#	k4
až	až	k9
1848	#num#	k4
si	se	k3xPyFc3
Marx	Marx	k1gMnSc1
žil	žít	k5eAaImAgMnS
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
Paříži	Paříž	k1gFnSc6
utrácel	utrácet	k5eAaImAgInS
šestinásobek	šestinásobek	k1gInSc1
tehdejšího	tehdejší	k2eAgInSc2d1
průměrného	průměrný	k2eAgInSc2d1
platu	plat	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1844	#num#	k4
na	na	k7c4
něj	on	k3xPp3gMnSc4
přátelé	přítel	k1gMnPc1
z	z	k7c2
Německa	Německo	k1gNnSc2
vybrali	vybrat	k5eAaPmAgMnP
1000	#num#	k4
tolarů	tolar	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
odpovídalo	odpovídat	k5eAaImAgNnS
např.	např.	kA
tříletému	tříletý	k2eAgInSc3d1
příjmu	příjem	k1gInSc2
slezského	slezský	k2eAgMnSc2d1
tkalce	tkadlec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
obdržel	obdržet	k5eAaPmAgMnS
Marx	Marx	k1gMnSc1
ještě	ještě	k9
800	#num#	k4
tolarů	tolar	k1gInPc2
a	a	k8xC
roční	roční	k2eAgInSc4d1
plat	plat	k1gInSc4
1800	#num#	k4
tolarů	tolar	k1gInPc2
za	za	k7c4
práci	práce	k1gFnSc4
na	na	k7c6
novinách	novina	k1gFnPc6
Vorwärts	Vorwärtsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
dostal	dostat	k5eAaPmAgInS
4000	#num#	k4
franků	frank	k1gInPc2
od	od	k7c2
„	„	k?
<g/>
Kolínského	kolínský	k2eAgInSc2d1
kruhu	kruh	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
financoval	financovat	k5eAaBmAgInS
krátkou	krátký	k2eAgFnSc4d1
existenci	existence	k1gFnSc4
novin	novina	k1gFnPc2
Rheinische	Rheinisch	k1gFnSc2
Zeitung	Zeitunga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
1844	#num#	k4
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
vyplaceno	vyplatit	k5eAaPmNgNnS
1000	#num#	k4
franků	frank	k1gInPc2
za	za	k7c4
vydání	vydání	k1gNnSc4
Svaté	svatý	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
březnu	březen	k1gInSc6
1848	#num#	k4
zdědil	zdědit	k5eAaPmAgMnS
po	po	k7c6
otci	otec	k1gMnSc6
6000	#num#	k4
franků	frank	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těchto	tento	k3xDgMnPc2
peněz	peníze	k1gInPc2
měl	mít	k5eAaImAgInS
údajně	údajně	k6eAd1
věnovat	věnovat	k5eAaPmF,k5eAaImF
5000	#num#	k4
franků	frank	k1gInPc2
na	na	k7c4
zakoupení	zakoupení	k1gNnSc4
zbraní	zbraň	k1gFnPc2
pro	pro	k7c4
belgické	belgický	k2eAgMnPc4d1
pracující	pracující	k1gMnPc4
<g/>
,	,	kIx,
o	o	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
se	se	k3xPyFc4
však	však	k9
nedochoval	dochovat	k5eNaPmAgInS
žádný	žádný	k3yNgInSc4
doklad	doklad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Engels	Engels	k1gMnSc1
mu	on	k3xPp3gMnSc3
také	také	k9
přenechal	přenechat	k5eAaPmAgInS
honorář	honorář	k1gInSc1
za	za	k7c4
svou	svůj	k3xOyFgFnSc4
knihu	kniha	k1gFnSc4
Podmínky	podmínka	k1gFnSc2
pracující	pracující	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finanční	finanční	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
začal	začít	k5eAaPmAgMnS
Marx	Marx	k1gMnSc1
mít	mít	k5eAaImF
až	až	k9
po	po	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Marx	Marx	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
také	také	k9
na	na	k7c4
čas	čas	k1gInSc1
placeným	placený	k2eAgMnSc7d1
informátorem	informátor	k1gMnSc7
rakouské	rakouský	k2eAgFnSc2d1
policie	policie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Londýnský	londýnský	k2eAgInSc1d1
exil	exil	k1gInSc1
</s>
<s>
Kenotaf	kenotaf	k1gInSc1
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
hřbitova	hřbitov	k1gInSc2
Highgate	Highgat	k1gInSc5
Cemetery	Cemeter	k1gMnPc7
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomník	pomník	k1gInSc1
s	s	k7c7
nápisem	nápis	k1gInSc7
Workers	Workers	k1gInSc4
of	of	k?
all	all	k?
lands	lands	k1gInSc4
<g/>
,	,	kIx,
unite	uniit	k5eAaBmRp2nP,k5eAaImRp2nP,k5eAaPmRp2nP
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Proletáři	proletář	k1gMnPc1
všech	všecek	k3xTgFnPc2
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
spojte	spojit	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
a	a	k8xC
citátem	citát	k1gInSc7
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teze	teze	k1gFnSc1
o	o	k7c6
Feuerbachovi	Feuerbach	k1gMnSc6
nechala	nechat	k5eAaPmAgFnS
zřídit	zřídit	k5eAaPmF
CPGB	CPGB	kA
roku	rok	k1gInSc2
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
rodinou	rodina	k1gFnSc7
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
uchýlil	uchýlit	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
již	již	k6eAd1
po	po	k7c6
měsíci	měsíc	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jim	on	k3xPp3gMnPc3
hrozila	hrozit	k5eAaImAgFnS
deportace	deportace	k1gFnSc1
do	do	k7c2
odlehlé	odlehlý	k2eAgFnSc2d1
Bretaně	Bretaň	k1gFnSc2
<g/>
,	,	kIx,
Francii	Francie	k1gFnSc4
opustili	opustit	k5eAaPmAgMnP
a	a	k8xC
usídlili	usídlit	k5eAaPmAgMnP
se	se	k3xPyFc4
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
sem	sem	k6eAd1
za	za	k7c7
nimi	on	k3xPp3gMnPc7
přesídlil	přesídlit	k5eAaPmAgMnS
také	také	k9
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
díky	díky	k7c3
jehož	jehož	k3xOyRp3gFnSc3
podpoře	podpora	k1gFnSc3
zde	zde	k6eAd1
přežívali	přežívat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
se	se	k3xPyFc4
pustili	pustit	k5eAaPmAgMnP
do	do	k7c2
další	další	k2eAgFnSc2d1
publikační	publikační	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
a	a	k8xC
mezinárodní	mezinárodní	k2eAgFnSc2d1
agitace	agitace	k1gFnSc2
pro	pro	k7c4
komunistické	komunistický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marxova	Marxův	k2eAgFnSc1d1
hmotná	hmotný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
se	se	k3xPyFc4
zlepšila	zlepšit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1852	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
londýnským	londýnský	k2eAgMnSc7d1
korespondentem	korespondent	k1gMnSc7
New	New	k1gFnPc2
York	York	k1gInSc1
Tribune	tribun	k1gMnSc5
<g/>
,	,	kIx,
kam	kam	k6eAd1
zasílal	zasílat	k5eAaImAgMnS
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
kritické	kritický	k2eAgFnPc4d1
politicko-ekonomické	politicko-ekonomický	k2eAgFnPc4d1
analýzy	analýza	k1gFnPc4
poměrů	poměr	k1gInPc2
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
mocnostech	mocnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupráci	spolupráce	k1gFnSc3
však	však	k9
přerušila	přerušit	k5eAaPmAgFnS
americká	americký	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1851	#num#	k4
se	se	k3xPyFc4
Marxovi	Marx	k1gMnSc3
pravděpodobně	pravděpodobně	k6eAd1
narodil	narodit	k5eAaPmAgMnS
nemanželský	manželský	k2eNgMnSc1d1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
Frederick	Frederick	k1gMnSc1
Lewis	Lewis	k1gFnSc2
Demuth	Demuth	k1gMnSc1
(	(	kIx(
<g/>
1851	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc7
matkou	matka	k1gFnSc7
byla	být	k5eAaImAgFnS
Marxova	Marxův	k2eAgFnSc1d1
služka	služka	k1gFnSc1
Helene	Helen	k1gInSc5
Demuthová	Demuthová	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
nebyl	být	k5eNaImAgMnS
ochoten	ochoten	k2eAgMnSc1d1
syna	syn	k1gMnSc4
uznat	uznat	k5eAaPmF
za	za	k7c2
svého	svůj	k3xOyFgNnSc2
<g/>
,	,	kIx,
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
otcovství	otcovství	k1gNnSc3
se	se	k3xPyFc4
zcela	zcela	k6eAd1
jistě	jistě	k6eAd1
lživě	lživě	k6eAd1
přihlásil	přihlásit	k5eAaPmAgMnS
Friedrich	Friedrich	k1gMnSc1
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
Frederick	Frederick	k1gMnSc1
byl	být	k5eAaImAgMnS
poslán	poslat	k5eAaPmNgMnS
do	do	k7c2
dělnického	dělnický	k2eAgInSc2d1
sirotčince	sirotčinec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
by	by	kYmCp3nS
tak	tak	k6eAd1
měl	mít	k5eAaImAgMnS
Marx	Marx	k1gMnSc1
osm	osm	k4xCc1
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
čtyři	čtyři	k4xCgInPc1
se	se	k3xPyFc4
dožily	dožít	k5eAaPmAgFnP
dospělosti	dospělost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Původně	původně	k6eAd1
Marxovi	Marxův	k2eAgMnPc1d1
bydleli	bydlet	k5eAaImAgMnP
v	v	k7c6
bytě	byt	k1gInSc6
Chelsey	Chelsea	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
byli	být	k5eAaImAgMnP
pro	pro	k7c4
neplacení	neplacení	k1gNnSc4
nájemného	nájemné	k1gNnSc2
v	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1850	#num#	k4
vystěhováni	vystěhovat	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
bydleli	bydlet	k5eAaImAgMnP
dočasně	dočasně	k6eAd1
v	v	k7c4
Dean	Dean	k1gInSc4
Street	Streeta	k1gFnPc2
v	v	k7c4
Soho	Soho	k1gNnSc4
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
přestěhovali	přestěhovat	k5eAaPmAgMnP
jen	jen	k9
o	o	k7c4
kousek	kousek	k1gInSc4
dále	daleko	k6eAd2
do	do	k7c2
trvalejšího	trvalý	k2eAgInSc2d2
příbytku	příbytek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byt	byt	k1gInSc1
byl	být	k5eAaImAgInS
pouze	pouze	k6eAd1
dvoupokojový	dvoupokojový	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Londýně	Londýn	k1gInSc6
vznikala	vznikat	k5eAaImAgFnS
jeho	jeho	k3xOp3gNnPc4
hlavní	hlavní	k2eAgNnPc4d1
ekonomická	ekonomický	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1859	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
Kritika	kritika	k1gFnSc1
politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
však	však	k9
nevzbudila	vzbudit	k5eNaPmAgFnS
očekávaný	očekávaný	k2eAgInSc4d1
ohlas	ohlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nespokojený	spokojený	k2eNgMnSc1d1
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
hodlá	hodlat	k5eAaImIp3nS
vydat	vydat	k5eAaPmF
další	další	k2eAgInPc4d1
<g/>
,	,	kIx,
podrobnější	podrobný	k2eAgInPc4d2
díly	díl	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mu	on	k3xPp3gMnSc3
s	s	k7c7
dalším	další	k2eAgNnSc7d1
studiem	studio	k1gNnSc7
pod	pod	k7c7
rukama	ruka	k1gFnPc7
dále	daleko	k6eAd2
narůstaly	narůstat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
a	a	k8xC
nesystematicky	systematicky	k6eNd1
rodilo	rodit	k5eAaImAgNnS
jeho	jeho	k3xOp3gNnSc4
hlavní	hlavní	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
Kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
první	první	k4xOgInSc1
díl	díl	k1gInSc1
dokončoval	dokončovat	k5eAaImAgInS
sužován	sužovat	k5eAaImNgInS
různými	různý	k2eAgFnPc7d1
chorobami	choroba	k1gFnPc7
a	a	k8xC
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1867	#num#	k4
byl	být	k5eAaImAgInS
předán	předat	k5eAaPmNgInS
do	do	k7c2
tisku	tisk	k1gInSc2
a	a	k8xC
vydán	vydán	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
práce	práce	k1gFnSc2
na	na	k7c6
Kapitálu	kapitál	k1gInSc6
se	se	k3xPyFc4
také	také	k9
aktivně	aktivně	k6eAd1
zapojoval	zapojovat	k5eAaImAgMnS
do	do	k7c2
dělnického	dělnický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
:	:	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1864	#num#	k4
stál	stát	k5eAaImAgMnS
u	u	k7c2
zrodu	zrod	k1gInSc2
První	první	k4xOgFnSc2
internacionály	internacionála	k1gFnSc2
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
1872	#num#	k4
v	v	k7c6
ní	on	k3xPp3gFnSc6
zastával	zastávat	k5eAaImAgInS
vedoucí	vedoucí	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
;	;	kIx,
formuloval	formulovat	k5eAaImAgInS
její	její	k3xOp3gFnPc4
stanovy	stanova	k1gFnPc4
a	a	k8xC
zakládající	zakládající	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byla	být	k5eAaImAgNnP
ale	ale	k8xC
rozpuštěna	rozpuštěn	k2eAgFnSc1d1
kvůli	kvůli	k7c3
neshodám	neshoda	k1gFnPc3
mezi	mezi	k7c7
komunisty	komunista	k1gMnPc7
<g/>
,	,	kIx,
proudhonovskými	proudhonovský	k2eAgMnPc7d1
a	a	k8xC
jinými	jiný	k2eAgMnPc7d1
anarchisty	anarchista	k1gMnPc7
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
roku	rok	k1gInSc2
1863	#num#	k4
socialista	socialista	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Lassalle	Lassalle	k1gInSc4
založil	založit	k5eAaPmAgMnS
v	v	k7c6
Prusku	Prusko	k1gNnSc6
svaz	svaz	k1gInSc1
Allgemeiner	Allgemeiner	k1gMnSc1
Deutscher	Deutschra	k1gFnPc2
Arbeitverein	Arbeitverein	k1gMnSc1
<g/>
,	,	kIx,
Marx	Marx	k1gMnSc1
se	se	k3xPyFc4
odmítl	odmítnout	k5eAaPmAgMnS
připojit	připojit	k5eAaPmF
<g/>
,	,	kIx,
neboť	neboť	k8xC
neuznával	uznávat	k5eNaImAgInS
jeho	jeho	k3xOp3gFnSc4
národní	národní	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
<g/>
;	;	kIx,
roku	rok	k1gInSc2
1869	#num#	k4
však	však	k9
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
Wilhelmem	Wilhelm	k1gMnSc7
Liebknechtem	Liebknecht	k1gMnSc7
<g/>
,	,	kIx,
zakladatelem	zakladatel	k1gMnSc7
Sociálně-demokratické	Sociálně-demokratický	k2eAgFnSc2d1
dělnické	dělnický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1875	#num#	k4
spojila	spojit	k5eAaPmAgNnP
s	s	k7c7
lasalleovskou	lasalleovský	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
v	v	k7c4
dodnes	dodnes	k6eAd1
fungující	fungující	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
SPD	SPD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
program	program	k1gInSc1
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
kritizoval	kritizovat	k5eAaImAgMnS
ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
předposlední	předposlední	k2eAgFnSc6d1
samostatné	samostatný	k2eAgFnSc6d1
práci	práce	k1gFnSc6
s	s	k7c7
názvem	název	k1gInSc7
Kritika	kritika	k1gFnSc1
Gothajského	gothajský	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dělnickém	dělnický	k2eAgNnSc6d1
hnutí	hnutí	k1gNnSc6
se	se	k3xPyFc4
angažovaly	angažovat	k5eAaBmAgFnP
také	také	k9
jeho	jeho	k3xOp3gFnPc1
dcery	dcera	k1gFnPc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgMnPc7
manželi	manžel	k1gMnPc7
<g/>
:	:	kIx,
Eleanor	Eleanora	k1gFnPc2
a	a	k8xC
Jenny	Jenna	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
Laura	Laura	k1gFnSc1
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mezitím	mezitím	k6eAd1
se	se	k3xPyFc4
však	však	k9
postupně	postupně	k6eAd1
zhoršoval	zhoršovat	k5eAaImAgInS
jeho	jeho	k3xOp3gInSc1
zdravotní	zdravotní	k2eAgInSc1d1
stav	stav	k1gInSc1
<g/>
;	;	kIx,
oslabila	oslabit	k5eAaPmAgFnS
jej	on	k3xPp3gNnSc4
především	především	k6eAd1
vleklá	vleklý	k2eAgFnSc1d1
kožní	kožní	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1881	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Jenny	Jenna	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
jejich	jejich	k3xOp3gFnSc1
dcera	dcera	k1gFnSc1
Jenny	Jenna	k1gFnSc2
Longuetová	Longuetový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgNnSc4d1
léta	léto	k1gNnPc4
života	život	k1gInSc2
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
práci	práce	k1gFnSc4
na	na	k7c4
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
III	III	kA
<g/>
.	.	kIx.
dílu	dílo	k1gNnSc3
Kapitálu	kapitál	k1gInSc2
<g/>
;	;	kIx,
když	když	k8xS
Marx	Marx	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1883	#num#	k4
coby	coby	k?
čtyřiašedesátiletý	čtyřiašedesátiletý	k2eAgMnSc1d1
vdovec	vdovec	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
našel	najít	k5eAaPmAgMnS
Engels	Engels	k1gMnSc1
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
pozůstalosti	pozůstalost	k1gFnSc6
obrovské	obrovský	k2eAgInPc4d1
neuspořádané	uspořádaný	k2eNgInPc4d1
svazky	svazek	k1gInPc4
poznámek	poznámka	k1gFnPc2
a	a	k8xC
náčrtů	náčrt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
mnohdy	mnohdy	k6eAd1
nebyl	být	k5eNaImAgInS
s	s	k7c7
to	ten	k3xDgNnSc4
rozluštit	rozluštit	k5eAaPmF
ani	ani	k8xC
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
chudém	chudý	k2eAgInSc6d1
pohřbu	pohřeb	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
pronesl	pronést	k5eAaPmAgMnS
řeč	řeč	k1gFnSc4
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
pochován	pochován	k2eAgMnSc1d1
na	na	k7c6
londýnském	londýnský	k2eAgInSc6d1
hřbitově	hřbitov	k1gInSc6
Highgate	Highgat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Chudoba	chudoba	k1gFnSc1
a	a	k8xC
zlepšení	zlepšení	k1gNnSc1
finanční	finanční	k2eAgFnSc2d1
situace	situace	k1gFnSc2
během	během	k7c2
londýnského	londýnský	k2eAgInSc2d1
exilu	exil	k1gInSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1861	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Marxova	Marxův	k2eAgFnSc1d1
finanční	finanční	k2eAgFnSc1d1
situace	situace	k1gFnSc1
velmi	velmi	k6eAd1
špatnou	špatný	k2eAgFnSc7d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
vlivem	vlivem	k7c2
dopadů	dopad	k1gInPc2
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
a	a	k8xC
následného	následný	k2eAgNnSc2d1
embarga	embargo	k1gNnSc2
na	na	k7c4
vývoz	vývoz	k1gInSc4
bavlny	bavlna	k1gFnSc2
na	na	k7c4
anglický	anglický	k2eAgInSc4d1
trh	trh	k1gInSc4
ze	z	k7c2
strany	strana	k1gFnSc2
Jihu	jih	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
postihlo	postihnout	k5eAaPmAgNnS
průmysl	průmysl	k1gInSc4
a	a	k8xC
dělníky	dělník	k1gMnPc4
a	a	k8xC
Engels	Engels	k1gMnSc1
nebyl	být	k5eNaImAgMnS
schopen	schopit	k5eAaPmNgMnS
po	po	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
poslat	poslat	k5eAaPmF
Marxovi	Marx	k1gMnSc3
mnoho	mnoho	k6eAd1
peněz	peníze	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
také	také	k9
přišel	přijít	k5eAaPmAgMnS
o	o	k7c4
zdroj	zdroj	k1gInSc4
příjmů	příjem	k1gInPc2
za	za	k7c4
své	svůj	k3xOyFgInPc4
sloupky	sloupek	k1gInPc4
v	v	k7c6
novinách	novina	k1gFnPc6
Tribune	tribun	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
zatím	zatím	k6eAd1
občas	občas	k6eAd1
živil	živit	k5eAaImAgMnS
jen	jen	k9
občasnou	občasný	k2eAgFnSc7d1
novinářskou	novinářský	k2eAgFnSc7d1
prací	práce	k1gFnSc7
<g/>
,	,	kIx,
si	se	k3xPyFc3
začal	začít	k5eAaPmAgInS
hledat	hledat	k5eAaImF
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1864	#num#	k4
však	však	k8xC
Marx	Marx	k1gMnSc1
získal	získat	k5eAaPmAgMnS
značné	značný	k2eAgNnSc4d1
jmění	jmění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostal	dostat	k5eAaPmAgMnS
dědictví	dědictví	k1gNnSc3
po	po	k7c6
své	svůj	k3xOyFgFnSc6
matce	matka	k1gFnSc6
<g/>
,	,	kIx,
peníze	peníz	k1gInPc4
od	od	k7c2
Engelse	Engels	k1gMnSc2
a	a	k8xC
dědictví	dědictví	k1gNnSc1
po	po	k7c6
Wilhelmu	Wilhelm	k1gMnSc6
Wolffovi	Wolff	k1gMnSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Už	už	k9
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
na	na	k7c6
mizině	mizina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Z	z	k7c2
matčina	matčin	k2eAgNnSc2d1
jmění	jmění	k1gNnSc2
se	se	k3xPyFc4
Marx	Marx	k1gMnSc1
dočkal	dočkat	k5eAaPmAgMnS
méně	málo	k6eAd2
než	než	k8xS
100	#num#	k4
liber	libra	k1gFnPc2
a	a	k8xC
po	po	k7c6
Wolffovi	Wolff	k1gMnSc6
zdědil	zdědit	k5eAaPmAgInS
824	#num#	k4
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
penězi	peníze	k1gInPc7
od	od	k7c2
Engelse	Engels	k1gMnSc2
měl	mít	k5eAaImAgMnS
příjmy	příjem	k1gInPc1
téměř	téměř	k6eAd1
1000	#num#	k4
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marxův	Marxův	k2eAgInSc1d1
příjem	příjem	k1gInSc1
roku	rok	k1gInSc2
1864	#num#	k4
byl	být	k5eAaImAgMnS
ekvivalentem	ekvivalent	k1gInSc7
20	#num#	k4
tehdejších	tehdejší	k2eAgFnPc2d1
dělnických	dělnický	k2eAgFnPc2d1
mezd	mzda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nicméně	nicméně	k8xC
od	od	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
situace	situace	k1gFnSc1
Marxe	Marx	k1gMnSc4
i	i	k9
jeho	jeho	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
zlepšovala	zlepšovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
roku	rok	k1gInSc2
1864	#num#	k4
se	se	k3xPyFc4
přestěhovali	přestěhovat	k5eAaPmAgMnP
do	do	k7c2
nového	nový	k2eAgInSc2d1
domu	dům	k1gInSc2
na	na	k7c4
Maitland	Maitland	k1gInSc4
Park	park	k1gInSc1
Road	Road	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1875	#num#	k4
se	se	k3xPyFc4
přestěhovali	přestěhovat	k5eAaPmAgMnP
do	do	k7c2
jiného	jiný	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
však	však	k9
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
zanikl	zaniknout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1864	#num#	k4
také	také	k6eAd1
Marxova	Marxův	k2eAgFnSc1d1
žena	žena	k1gFnSc1
Jeny	jen	k1gInPc1
pořádala	pořádat	k5eAaImAgFnS
plesy	ples	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1869	#num#	k4
zařídil	zařídit	k5eAaPmAgMnS
Engels	Engels	k1gMnSc1
Marxovi	Marx	k1gMnSc3
penzi	penze	k1gFnSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
350	#num#	k4
liber	libra	k1gFnPc2
za	za	k7c4
rok	rok	k1gInSc4
a	a	k8xC
splatil	splatit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gInPc4
dluhy	dluh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
Marxovi	Marx	k1gMnSc3
žít	žít	k5eAaImF
pohodlně	pohodlně	k6eAd1
s	s	k7c7
příjmem	příjem	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ho	on	k3xPp3gMnSc4
řadil	řadit	k5eAaImAgInS
mezi	mezi	k7c4
nejbohatší	bohatý	k2eAgNnSc4d3
2	#num#	k4
%	%	kIx~
britské	britský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Myšlení	myšlení	k1gNnSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
plodného	plodný	k2eAgMnSc4d1
autora	autor	k1gMnSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
myšlení	myšlení	k1gNnSc1
prošlo	projít	k5eAaPmAgNnS
poměrně	poměrně	k6eAd1
složitým	složitý	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohý	k2eAgInPc1d1
jeho	jeho	k3xOp3gInPc1
spisy	spis	k1gInPc1
vyšly	vyjít	k5eAaPmAgInP
teprve	teprve	k6eAd1
posmrtně	posmrtně	k6eAd1
<g/>
;	;	kIx,
s	s	k7c7
jejich	jejich	k3xOp3gNnSc7
zpřístupněním	zpřístupnění	k1gNnSc7
se	se	k3xPyFc4
ukázaly	ukázat	k5eAaPmAgInP
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
mladým	mladý	k1gMnSc7
Marxem	Marx	k1gMnSc7
(	(	kIx(
<g/>
cca	cca	kA
do	do	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
vydán	vydat	k5eAaPmNgInS
Komunistický	komunistický	k2eAgInSc1d1
manifest	manifest	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
zralým	zralý	k2eAgMnSc7d1
Marxem	Marx	k1gMnSc7
<g/>
,	,	kIx,
autorem	autor	k1gMnSc7
Kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
těžiště	těžiště	k1gNnSc1
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
zájmu	zájem	k1gInSc2
leželo	ležet	k5eAaImAgNnS
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Centrálním	centrální	k2eAgInSc7d1
konceptem	koncept	k1gInSc7
jeho	jeho	k3xOp3gFnSc2
filosofie	filosofie	k1gFnSc2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
postupně	postupně	k6eAd1
formulované	formulovaný	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
dějin	dějiny	k1gFnPc2
<g/>
,	,	kIx,
Engelsem	Engels	k1gMnSc7
později	pozdě	k6eAd2
označované	označovaný	k2eAgFnPc1d1
jako	jako	k8xS,k8xC
historický	historický	k2eAgInSc1d1
materialismus	materialismus	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
pozdějšími	pozdní	k2eAgMnPc7d2
marxisty	marxista	k1gMnPc7
také	také	k9
jako	jako	k9
dialektický	dialektický	k2eAgInSc1d1
materialismus	materialismus	k1gInSc1
(	(	kIx(
<g/>
Marx	Marx	k1gMnSc1
sám	sám	k3xTgMnSc1
tato	tento	k3xDgNnPc1
označení	označení	k1gNnPc4
nepoužíval	používat	k5eNaImAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
Hegelově	Hegelův	k2eAgFnSc6d1
dialektice	dialektika	k1gFnSc6
<g/>
,	,	kIx,
tedy	tedy	k8xC
nahlížení	nahlížení	k1gNnSc1
vývoje	vývoj	k1gInSc2
jako	jako	k8xC,k8xS
stálého	stálý	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
tezí	teze	k1gFnPc2
a	a	k8xC
antitezí	antiteze	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ústí	ústit	k5eAaImIp3nS
v	v	k7c6
syntezi	synteze	k1gFnSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vývoj	vývoj	k1gInSc4
posouvá	posouvat	k5eAaImIp3nS
o	o	k7c4
krok	krok	k1gInSc4
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
princip	princip	k1gInSc4
věčného	věčný	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
základní	základní	k2eAgFnSc4d1
vlastnost	vlastnost	k1gFnSc4
společenského	společenský	k2eAgInSc2d1
řádu	řád	k1gInSc2
označil	označit	k5eAaPmAgInS
konflikt	konflikt	k1gInSc1
různých	různý	k2eAgFnPc2d1
společenských	společenský	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Dějiny	dějiny	k1gFnPc1
všech	všecek	k3xTgFnPc2
dosavadních	dosavadní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
jsou	být	k5eAaImIp3nP
dějinami	dějiny	k1gFnPc7
třídních	třídní	k2eAgInPc2d1
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
—	—	k?
Svobodný	svobodný	k2eAgMnSc1d1
a	a	k8xC
otrok	otrok	k1gMnSc1
<g/>
,	,	kIx,
patricij	patricij	k1gMnSc1
a	a	k8xC
plebejec	plebejec	k1gMnSc1
<g/>
,	,	kIx,
baron	baron	k1gMnSc1
a	a	k8xC
nevolník	nevolník	k1gMnSc1
<g/>
,	,	kIx,
cechovní	cechovní	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
a	a	k8xC
tovaryš	tovaryš	k1gMnSc1
<g/>
,	,	kIx,
vedli	vést	k5eAaImAgMnP
nepřetržitý	přetržitý	k2eNgInSc4d1
boj	boj	k1gInSc4
<g/>
,	,	kIx,
tu	tu	k6eAd1
skrytý	skrytý	k2eAgInSc4d1
<g/>
,	,	kIx,
tu	tu	k6eAd1
otevřený	otevřený	k2eAgInSc1d1
<g/>
,	,	kIx,
boj	boj	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pokaždé	pokaždé	k6eAd1
skončil	skončit	k5eAaPmAgInS
revolučním	revoluční	k2eAgNnSc7d1
přetvořením	přetvoření	k1gNnSc7
celé	celý	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
nebo	nebo	k8xC
společným	společný	k2eAgInSc7d1
zánikem	zánik	k1gInSc7
bojujících	bojující	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Komunistický	komunistický	k2eAgInSc1d1
manifest	manifest	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
—	—	k?
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1
stupněm	stupeň	k1gInSc7
třídního	třídní	k2eAgInSc2d1
boje	boj	k1gInSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
soudobý	soudobý	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
proletariátu	proletariát	k1gInSc2
a	a	k8xC
buržoazie	buržoazie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gMnSc2
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
a	a	k8xC
nevyhnutelným	vyhnutelný	k2eNgNnSc7d1
rozuzlením	rozuzlení	k1gNnSc7
tohoto	tento	k3xDgInSc2
věčného	věčný	k2eAgInSc2d1
konfliktu	konflikt	k1gInSc2
beztřídní	beztřídní	k2eAgFnSc1d1
<g/>
,	,	kIx,
komunistická	komunistický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociální	sociální	k2eAgFnPc4d1
spravedlnosti	spravedlnost	k1gFnPc4
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
dosaženo	dosáhnout	k5eAaPmNgNnS
odstraněním	odstranění	k1gNnSc7
příčiny	příčina	k1gFnSc2
třídního	třídní	k2eAgInSc2d1
boje	boj	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
zrušením	zrušení	k1gNnSc7
soukromého	soukromý	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
za	za	k7c4
cenu	cena	k1gFnSc4
násilí	násilí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1
filosofie	filosofie	k1gFnSc1
je	být	k5eAaImIp3nS
do	do	k7c2
určité	určitý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
scientistická	scientistický	k2eAgFnSc1d1
<g/>
:	:	kIx,
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
byl	být	k5eAaImAgInS
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
konečnou	konečný	k2eAgFnSc7d1
platností	platnost	k1gFnSc7
odhalil	odhalit	k5eAaPmAgMnS
historicko-společenské	historicko-společenský	k2eAgFnSc3d1
zákonitosti	zákonitost	k1gFnSc3
vývoje	vývoj	k1gInSc2
–	–	k?
proto	proto	k8xC
bývá	bývat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
nauka	nauka	k1gFnSc1
nazývána	nazýván	k2eAgFnSc1d1
„	„	k?
<g/>
vědecký	vědecký	k2eAgInSc1d1
socialismus	socialismus	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
proto	proto	k8xC
ho	on	k3xPp3gMnSc4
Engels	Engels	k1gMnSc1
v	v	k7c6
pohřební	pohřební	k2eAgFnSc6d1
řeči	řeč	k1gFnSc6
přirovnal	přirovnat	k5eAaPmAgMnS
k	k	k7c3
Darwinovi	Darwin	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
však	však	k8xC
nevypracoval	vypracovat	k5eNaPmAgMnS
žádnou	žádný	k3yNgFnSc4
ucelenější	ucelený	k2eAgFnSc4d2
koncepci	koncepce	k1gFnSc4
sociálního	sociální	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
nastat	nastat	k5eAaPmF
po	po	k7c6
konci	konec	k1gInSc6
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1
myšlení	myšlení	k1gNnSc1
bývá	bývat	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
syntézu	syntéza	k1gFnSc4
tří	tři	k4xCgInPc2
směrů	směr	k1gInPc2
evropského	evropský	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c4
něž	jenž	k3xRgNnSc4
navazoval	navazovat	k5eAaImAgInS
<g/>
:	:	kIx,
německé	německý	k2eAgFnSc2d1
klasické	klasický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
francouzského	francouzský	k2eAgInSc2d1
utopického	utopický	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
(	(	kIx(
<g/>
Charles	Charles	k1gMnSc1
Fourier	Fourier	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
anglické	anglický	k2eAgFnSc2d1
klasické	klasický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
zejména	zejména	k9
David	David	k1gMnSc1
Ricardo	Ricardo	k1gNnSc4
a	a	k8xC
J.	J.	kA
S.	S.	kA
Mill	Mill	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marxovo	Marxův	k2eAgNnSc1d1
tvůrčí	tvůrčí	k2eAgNnSc1d1
období	období	k1gNnSc1
lze	lze	k6eAd1
zhruba	zhruba	k6eAd1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1867	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Marx	Marx	k1gMnSc1
publikoval	publikovat	k5eAaBmAgMnS
(	(	kIx(
<g/>
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
<g/>
)	)	kIx)
většinu	většina	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
publikována	publikovat	k5eAaBmNgFnS
již	již	k9
jen	jen	k9
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Kritika	kritika	k1gFnSc1
Gothajského	gothajský	k2eAgInSc2d1
programu	program	k1gInSc2
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Engelsovy	Engelsův	k2eAgFnSc2d1
informace	informace	k1gFnSc2
v	v	k7c6
předmluvách	předmluva	k1gFnPc6
k	k	k7c3
druhému	druhý	k4xOgMnSc3
a	a	k8xC
třetímu	třetí	k4xOgMnSc3
dílu	dílo	k1gNnSc3
Kapitálu	kapitál	k1gInSc2
byly	být	k5eAaImAgInP
rukopisy	rukopis	k1gInPc1
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
dílů	díl	k1gInPc2
sepsány	sepsán	k2eAgMnPc4d1
v	v	k7c6
letech	léto	k1gNnPc6
1864	#num#	k4
až	až	k9
1867	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
se	se	k3xPyFc4
Marx	Marx	k1gMnSc1
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
například	například	k6eAd1
přírodovědě	přírodověda	k1gFnSc3
<g/>
,	,	kIx,
chemii	chemie	k1gFnSc6
<g/>
,	,	kIx,
matematice	matematika	k1gFnSc6
(	(	kIx(
<g/>
sepsal	sepsat	k5eAaPmAgMnS
několik	několik	k4yIc4
matematických	matematický	k2eAgNnPc2d1
pojednání	pojednání	k1gNnPc2
<g/>
)	)	kIx)
atp.	atp.	kA
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Mladý	mladý	k2eAgMnSc1d1
Marx	Marx	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
Intelektuální	intelektuální	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
psal	psát	k5eAaImAgInS
své	svůj	k3xOyFgInPc4
rané	raný	k2eAgInPc4d1
spisy	spis	k1gInPc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
hluboce	hluboko	k6eAd1
ovlivněno	ovlivnit	k5eAaPmNgNnS
myšlením	myšlení	k1gNnSc7
G.	G.	kA
W.	W.	kA
F.	F.	kA
Hegela	Hegela	k1gFnSc1
(	(	kIx(
<g/>
†	†	k?
1831	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
následovníci	následovník	k1gMnPc1
se	se	k3xPyFc4
záhy	záhy	k6eAd1
rozštěpili	rozštěpit	k5eAaPmAgMnP
na	na	k7c4
konzervativní	konzervativní	k2eAgMnPc4d1
<g/>
,	,	kIx,
„	„	k?
<g/>
pravé	pravá	k1gFnSc2
<g/>
“	“	k?
hegeliány	hegelián	k1gMnPc4
a	a	k8xC
na	na	k7c4
tzv.	tzv.	kA
mladohegelovce	mladohegelovec	k1gMnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
zaměření	zaměření	k1gNnSc1
vyústilo	vyústit	k5eAaPmAgNnS
do	do	k7c2
panteismu	panteismus	k1gInSc2
a	a	k8xC
posléze	posléze	k6eAd1
ateismu	ateismus	k1gInSc3
<g/>
;	;	kIx,
k	k	k7c3
mladohegelovcům	mladohegelovec	k1gMnPc3
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc7
nejvýznamnější	významný	k2eAgFnSc7d3
postavou	postava	k1gFnSc7
byl	být	k5eAaImAgMnS
Ludwig	Ludwig	k1gMnSc1
Feuerbach	Feuerbach	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
zpočátku	zpočátku	k6eAd1
řadil	řadit	k5eAaImAgMnS
také	také	k9
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
a	a	k8xC
společně	společně	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
i	i	k9
Friedrich	Friedrich	k1gMnSc1
Engels	Engels	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
chtěli	chtít	k5eAaImAgMnP
–	–	k?
jeho	jeho	k3xOp3gNnPc7
vlastními	vlastní	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
–	–	k?
„	„	k?
<g/>
postavit	postavit	k5eAaPmF
hegelovskou	hegelovský	k2eAgFnSc4d1
dialektiku	dialektika	k1gFnSc4
z	z	k7c2
hlavy	hlava	k1gFnSc2
na	na	k7c4
nohy	noha	k1gFnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
spisy	spis	k1gInPc4
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
polemikami	polemika	k1gFnPc7
s	s	k7c7
jeho	jeho	k3xOp3gMnPc7
současníky	současník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
židovské	židovský	k2eAgFnSc3d1
otázce	otázka	k1gFnSc3
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Krátký	krátký	k2eAgInSc1d1
spis	spis	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
Německo-francouzské	německo-francouzský	k2eAgFnSc6d1
ročence	ročenka	k1gFnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
roku	rok	k1gInSc2
1844	#num#	k4
<g/>
,	,	kIx,
kritizuje	kritizovat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc1
studie	studie	k1gFnPc1
Bruna	Bruno	k1gMnSc2
Bauera	Bauer	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
věnují	věnovat	k5eAaImIp3nP,k5eAaPmIp3nP
možnosti	možnost	k1gFnPc1
politické	politický	k2eAgFnSc2d1
emancipace	emancipace	k1gFnSc2
Židů	Žid	k1gMnPc2
v	v	k7c6
Prusku	Prusko	k1gNnSc6
a	a	k8xC
prosazují	prosazovat	k5eAaImIp3nP
sekulární	sekulární	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gInSc2
nestačí	stačit	k5eNaBmIp3nS
emancipace	emancipace	k1gFnSc1
politická	politický	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
náboženství	náboženství	k1gNnSc4
není	být	k5eNaImIp3nS
překážkou	překážka	k1gFnSc7
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
např.	např.	kA
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
hovoří	hovořit	k5eAaImIp3nS
také	také	k9
o	o	k7c6
„	„	k?
<g/>
lidské	lidský	k2eAgFnSc3d1
emancipaci	emancipace	k1gFnSc3
<g/>
“	“	k?
a	a	k8xC
svobodě	svoboda	k1gFnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
přičemž	přičemž	k6eAd1
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
dle	dle	k7c2
něj	on	k3xPp3gNnSc2
ale	ale	k9
možná	možná	k9
jen	jen	k9
v	v	k7c4
nenáboženské	náboženský	k2eNgMnPc4d1
(	(	kIx(
<g/>
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
případě	případ	k1gInSc6
socialistické	socialistický	k2eAgFnPc1d1
<g/>
)	)	kIx)
společnosti	společnost	k1gFnPc1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
sekulární	sekulární	k2eAgInSc1d1
stát	stát	k1gInSc1
náboženství	náboženství	k1gNnSc2
nepopírá	popírat	k5eNaImIp3nS
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
ač	ač	k8xS
narozený	narozený	k2eAgMnSc1d1
do	do	k7c2
původně	původně	k6eAd1
židovské	židovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
k	k	k7c3
židovství	židovství	k1gNnSc3
komplikovaný	komplikovaný	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
;	;	kIx,
někteří	některý	k3yIgMnPc1
komentátoři	komentátor	k1gMnPc1
(	(	kIx(
<g/>
např.	např.	kA
Šlomo	Šloma	k1gFnSc5
Avineri	Avineri	k1gNnPc1
<g/>
)	)	kIx)
spatřují	spatřovat	k5eAaImIp3nP
ve	v	k7c6
spisku	spisek	k1gInSc6
antisemitské	antisemitský	k2eAgInPc4d1
názory	názor	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ke	k	k7c3
kritice	kritika	k1gFnSc3
Hegelovy	Hegelův	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
práva	právo	k1gNnSc2
(	(	kIx(
<g/>
1843	#num#	k4
<g/>
–	–	k?
<g/>
44	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
raném	raný	k2eAgInSc6d1
spise	spis	k1gInSc6
kritizuje	kritizovat	k5eAaImIp3nS
Hegelův	Hegelův	k2eAgInSc1d1
spis	spis	k1gInSc1
Základy	základ	k1gInPc1
filosofie	filosofie	k1gFnSc2
práva	právo	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1821	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hegelův	Hegelův	k2eAgInSc1d1
dialektický	dialektický	k2eAgInSc1d1
systém	systém	k1gInSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
zcela	zcela	k6eAd1
abstraktní	abstraktní	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
úvodu	úvod	k1gInSc6
<g/>
,	,	kIx,
napsaném	napsaný	k2eAgInSc6d1
v	v	k7c6
lednu	leden	k1gInSc6
1844	#num#	k4
<g/>
,	,	kIx,
pak	pak	k6eAd1
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
náboženství	náboženství	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
lidskou	lidský	k2eAgFnSc4d1
iluzi	iluze	k1gFnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Náboženství	náboženství	k1gNnSc1
je	být	k5eAaImIp3nS
vzdychání	vzdychání	k1gNnSc4
utlačovaného	utlačovaný	k1gMnSc2
tvora	tvor	k1gMnSc2
<g/>
,	,	kIx,
srdce	srdce	k1gNnSc1
nelítostného	lítostný	k2eNgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
duch	duch	k1gMnSc1
bezduché	bezduchý	k2eAgFnSc2d1
situace	situace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
opium	opium	k1gNnSc1
lidstva	lidstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravé	pravý	k2eAgInPc1d1
štěstí	štěstí	k1gNnSc4
lidu	lid	k1gInSc2
vyžaduje	vyžadovat	k5eAaImIp3nS
odstranění	odstranění	k1gNnSc4
náboženství	náboženství	k1gNnSc2
jako	jako	k8xC,k8xS
iluzorního	iluzorní	k2eAgNnSc2d1
štěstí	štěstí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomicko-filosofické	ekonomicko-filosofický	k2eAgInPc1d1
rukopisy	rukopis	k1gInPc1
z	z	k7c2
roku	rok	k1gInSc2
1844	#num#	k4
</s>
<s>
V	v	k7c6
těchto	tento	k3xDgInPc6
rukopisech	rukopis	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
zveřejněny	zveřejnit	k5eAaPmNgInP
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1932	#num#	k4
a	a	k8xC
následně	následně	k6eAd1
získaly	získat	k5eAaPmAgFnP
mezi	mezi	k7c4
marxisty	marxista	k1gMnPc4
velký	velký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
vyjádřil	vyjádřit	k5eAaPmAgMnS
k	k	k7c3
národní	národní	k2eAgFnSc3d1
ekonomii	ekonomie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
předpokládá	předpokládat	k5eAaImIp3nS
soukromé	soukromý	k2eAgNnSc1d1
vlastnictví	vlastnictví	k1gNnSc1
<g/>
,	,	kIx,
Marx	Marx	k1gMnSc1
je	být	k5eAaImIp3nS
však	však	k9
nijak	nijak	k6eAd1
nevysvětluje	vysvětlovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádrem	jádro	k1gNnSc7
spisu	spis	k1gInSc2
je	být	k5eAaImIp3nS
kapitola	kapitola	k1gFnSc1
Odcizená	odcizený	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
přebírá	přebírat	k5eAaImIp3nS
hegelovskou	hegelovský	k2eAgFnSc4d1
dialektiku	dialektika	k1gFnSc4
a	a	k8xC
pojem	pojem	k1gInSc4
odcizení	odcizení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
dělník	dělník	k1gMnSc1
nepracuje	pracovat	k5eNaImIp3nS
pro	pro	k7c4
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
pro	pro	k7c4
kapitalistu	kapitalista	k1gMnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
produkt	produkt	k1gInSc1
jeho	jeho	k3xOp3gFnSc2
práce	práce	k1gFnSc2
čímsi	čísi	k3xOyIgMnPc3
cizím	cizí	k2eAgMnPc3d1
<g/>
,	,	kIx,
odcizeným	odcizený	k2eAgMnPc3d1
<g/>
,	,	kIx,
neboli	neboli	k8xC
„	„	k?
<g/>
zvnějšněným	zvnějšněný	k2eAgMnSc7d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
odvozeny	odvozen	k2eAgInPc1d1
čtyři	čtyři	k4xCgInPc1
stupně	stupeň	k1gInPc1
odcizení	odcizení	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
znemožňují	znemožňovat	k5eAaImIp3nP
dělníkovi	dělník	k1gMnSc3
důstojný	důstojný	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
odcizení	odcizení	k1gNnSc1
od	od	k7c2
produktu	produkt	k1gInSc2
vlastní	vlastní	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
odcizení	odcizení	k1gNnSc1
od	od	k7c2
výrobní	výrobní	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
samé	samý	k3xTgFnPc1
(	(	kIx(
<g/>
a	a	k8xC
tedy	tedy	k9
od	od	k7c2
sebe	sebe	k3xPyFc4
sama	sám	k3xTgMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
odcizení	odcizení	k1gNnSc1
od	od	k7c2
rodové	rodový	k2eAgFnSc2d1
podstaty	podstata	k1gFnSc2
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
odcizení	odcizení	k1gNnSc1
od	od	k7c2
druhých	druhý	k4xOgMnPc2
jednotlivců	jednotlivec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
(	(	kIx(
<g/>
1844	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
s	s	k7c7
podtitulem	podtitul	k1gInSc7
Kritika	kritika	k1gFnSc1
kritické	kritický	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc7
prvním	první	k4xOgInSc7
dílem	díl	k1gInSc7
vytvořeným	vytvořený	k2eAgInSc7d1
společně	společně	k6eAd1
s	s	k7c7
Engelsem	Engels	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polemicky	polemicky	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
obracejí	obracet	k5eAaImIp3nP
zejména	zejména	k9
proti	proti	k7c3
„	„	k?
<g/>
pravému	pravý	k2eAgNnSc3d1
<g/>
“	“	k?
mladohegelovci	mladohegelovec	k1gMnSc3
a	a	k8xC
svému	svůj	k3xOyFgMnSc3
někdejšímu	někdejší	k2eAgMnSc3d1
učiteli	učitel	k1gMnSc3
Brunovi	Bruna	k1gMnSc3
Bauerovi	Bauer	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
spise	spis	k1gInSc6
se	se	k3xPyFc4
Marx	Marx	k1gMnSc1
dosud	dosud	k6eAd1
hlásí	hlásit	k5eAaImIp3nS
k	k	k7c3
Feuerbachovi	Feuerbach	k1gMnSc3
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yRgMnSc4,k3yQgMnSc4
však	však	k9
již	již	k6eAd1
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
bude	být	k5eAaImBp3nS
tvrdě	tvrdě	k6eAd1
kritizovat	kritizovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Německá	německý	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
Německé	německý	k2eAgFnSc6d1
ideologii	ideologie	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
také	také	k9
Engels	Engels	k1gMnSc1
a	a	k8xC
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyšla	vyjít	k5eAaPmAgFnS
až	až	k9
posmrtně	posmrtně	k6eAd1
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
a	a	k8xC
Lipsko	Lipsko	k1gNnSc1
1932	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
satiricky	satiricky	k6eAd1
kritizován	kritizovat	k5eAaImNgMnS
především	především	k9
Max	Max	k1gMnSc1
Stirner	Stirner	k1gInSc4
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
mladohegelovci	mladohegelovec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
uceleně	uceleně	k6eAd1
představeno	představen	k2eAgNnSc1d1
Marxovo	Marxův	k2eAgNnSc1d1
materialistické	materialistický	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidské	lidský	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
určováno	určovat	k5eAaImNgNnS
životními	životní	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
epoch	epocha	k1gFnPc2
<g/>
,	,	kIx,
nikoli	nikoli	k9
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každé	každý	k3xTgFnSc6
epoše	epocha	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
střetu	střet	k1gInSc3
dvou	dva	k4xCgFnPc2
společenských	společenský	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
vládnoucí	vládnoucí	k2eAgFnPc1d1
a	a	k8xC
utlačované	utlačovaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládnoucí	vládnoucí	k2eAgFnSc1d1
třída	třída	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
rukou	ruka	k1gFnPc6
materiální	materiální	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
podle	podle	k7c2
materialismu	materialismus	k1gInSc2
monopol	monopol	k1gInSc4
i	i	k9
na	na	k7c4
duchovní	duchovní	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
vydávat	vydávat	k5eAaPmF,k5eAaImF
své	svůj	k3xOyFgNnSc4
myšlení	myšlení	k1gNnSc4
za	za	k7c4
jediné	jediný	k2eAgFnPc4d1
platné	platný	k2eAgFnPc4d1
a	a	k8xC
legitimizovat	legitimizovat	k5eAaBmF
jím	on	k3xPp3gNnSc7
svou	svůj	k3xOyFgFnSc4
nadvládu	nadvláda	k1gFnSc4
<g/>
;	;	kIx,
to	ten	k3xDgNnSc1
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
nazýval	nazývat	k5eAaImAgMnS
ideologií	ideologie	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
rozkrýt	rozkrýt	k5eAaPmF
pouze	pouze	k6eAd1
kritikou	kritika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Teze	teze	k1gFnSc1
o	o	k7c6
Feuerbachovi	Feuerbach	k1gMnSc6
(	(	kIx(
<g/>
1845	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jedenáctá	jedenáctý	k4xOgFnSc1
teze	teze	k1gFnSc1
o	o	k7c6
Feuerbachovi	Feuerbach	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Originální	originální	k2eAgInSc4d1
Marxův	Marxův	k2eAgInSc4d1
rukopis	rukopis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Teze	teze	k1gFnSc1
o	o	k7c6
Feuerbachovi	Feuerbach	k1gMnSc6
(	(	kIx(
<g/>
poprvé	poprvé	k6eAd1
publikovány	publikovat	k5eAaBmNgInP
Engelsem	Engels	k1gMnSc7
ve	v	k7c6
spise	spis	k1gInSc6
L.	L.	kA
Feuerbach	Feuerbach	k1gMnSc1
a	a	k8xC
vyústění	vyústění	k1gNnSc1
klasické	klasický	k2eAgFnSc2d1
německé	německý	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
1888	#num#	k4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
souborem	soubor	k1gInSc7
jedenácti	jedenáct	k4xCc2
krátkých	krátký	k2eAgFnPc2d1
poznámek	poznámka	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
kritizuje	kritizovat	k5eAaImIp3nS
Ludwiga	Ludwiga	k1gFnSc1
Feuerbacha	Feuerbach	k1gMnSc2
a	a	k8xC
mladohegelovce	mladohegelovec	k1gMnSc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sice	sice	k8xC
oceňuje	oceňovat	k5eAaImIp3nS
Feuerbachův	Feuerbachův	k2eAgInSc4d1
materialismus	materialismus	k1gInSc4
<g/>
;	;	kIx,
ten	ten	k3xDgMnSc1
však	však	k9
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
jen	jen	k9
„	„	k?
<g/>
nazíravý	nazíravý	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
projevovat	projevovat	k5eAaImF
v	v	k7c6
praxi	praxe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
<g/>
,	,	kIx,
jedenáctá	jedenáctý	k4xOgFnSc1
teze	teze	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nejznámějších	známý	k2eAgMnPc2d3
filosofických	filosofický	k2eAgMnPc2d1
citátů	citát	k1gInPc2
vůbec	vůbec	k9
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Filosofové	filosof	k1gMnPc1
svět	svět	k1gInSc1
jen	jen	k9
různě	různě	k6eAd1
vykládali	vykládat	k5eAaImAgMnP
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
však	však	k9
o	o	k7c4
to	ten	k3xDgNnSc4
jej	on	k3xPp3gMnSc4
změnit	změnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Teze	teze	k1gFnSc1
o	o	k7c6
Feuerbachovi	Feuerbach	k1gMnSc6
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Bída	bída	k1gFnSc1
filosofie	filosofie	k1gFnSc1
(	(	kIx(
<g/>
1847	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Název	název	k1gInSc1
představuje	představovat	k5eAaImIp3nS
narážku	narážka	k1gFnSc4
na	na	k7c4
dílo	dílo	k1gNnSc4
s	s	k7c7
podtitulem	podtitul	k1gInSc7
Filosofie	filosofie	k1gFnSc2
bídy	bída	k1gFnSc2
(	(	kIx(
<g/>
Systè	Systè	k1gInSc5
des	des	k1gNnSc7
contradictions	contradictions	k1gInSc1
économiques	économiques	k1gInSc1
ou	ou	k0
Philosophie	Philosophie	k1gFnPc1
de	de	k?
la	la	k0
misè	misè	k1gMnSc5
<g/>
,	,	kIx,
1846	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
mutualista	mutualista	k1gMnSc1
Pierre-Joseph	Pierre-Joseph	k1gMnSc1
Proudhon	Proudhon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
kritizuje	kritizovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
vizi	vize	k1gFnSc4
socialismu	socialismus	k1gInSc2
za	za	k7c4
přílišné	přílišný	k2eAgNnSc4d1
protežování	protežování	k1gNnSc4
buržoazie	buržoazie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psáno	psát	k5eAaImNgNnS
francouzsky	francouzsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Zralý	zralý	k2eAgMnSc1d1
Marx	Marx	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
Rukopis	rukopis	k1gInSc1
Manifestu	manifest	k1gInSc2
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
Po	po	k7c6
vydání	vydání	k1gNnSc6
Komunistického	komunistický	k2eAgInSc2d1
manifestu	manifest	k1gInSc2
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
Marx	Marx	k1gMnSc1
vrací	vracet	k5eAaImIp3nS
k	k	k7c3
národohospodářským	národohospodářský	k2eAgFnPc3d1
otázkám	otázka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kritice	kritika	k1gFnSc6
politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
je	být	k5eAaImIp3nS
vztah	vztah	k1gInSc4
kultury	kultura	k1gFnSc2
a	a	k8xC
ekonomiky	ekonomika	k1gFnSc2
vysvětlován	vysvětlovat	k5eAaImNgInS
pojmy	pojem	k1gInPc4
základna	základna	k1gFnSc1
a	a	k8xC
nadstavba	nadstavba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadstavba	nadstavba	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
kultura	kultura	k1gFnSc1
a	a	k8xC
instituce	instituce	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
určována	určovat	k5eAaImNgFnS
ekonomickými	ekonomický	k2eAgInPc7d1
faktory	faktor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rané	raný	k2eAgFnPc1d1
Marxovy	Marxův	k2eAgFnPc1d1
práce	práce	k1gFnPc1
uznávají	uznávat	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
zpětného	zpětný	k2eAgNnSc2d1
(	(	kIx(
<g/>
dialektického	dialektický	k2eAgNnSc2d1
<g/>
)	)	kIx)
ovlivnění	ovlivnění	k1gNnSc1
základny	základna	k1gFnSc2
nadstavbou	nadstavba	k1gFnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pozdější	pozdní	k2eAgFnPc4d2
práce	práce	k1gFnPc4
se	se	k3xPyFc4
blíží	blížit	k5eAaImIp3nP
stanovisku	stanovisko	k1gNnSc3
jednosměrného	jednosměrný	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
základny	základna	k1gFnSc2
na	na	k7c4
nadstavbu	nadstavba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umění	umění	k1gNnSc1
a	a	k8xC
kultura	kultura	k1gFnSc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
jen	jen	k9
odrazem	odraz	k1gInSc7
sociálně-ekonomických	sociálně-ekonomický	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
dané	daný	k2eAgFnSc2d1
epochy	epocha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Komunistický	komunistický	k2eAgInSc1d1
manifest	manifest	k1gInSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Komunistický	komunistický	k2eAgInSc4d1
manifest	manifest	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Manifest	manifest	k1gInSc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byl	být	k5eAaImAgInS
sepsán	sepsán	k2eAgInSc1d1
s	s	k7c7
F.	F.	kA
Engelsem	Engels	k1gMnSc7
a	a	k8xC
zveřejněn	zveřejněn	k2eAgInSc1d1
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
revolučního	revoluční	k2eAgInSc2d1
roku	rok	k1gInSc2
1848	#num#	k4
na	na	k7c6
sjezdu	sjezd	k1gInSc6
Svazu	svaz	k1gInSc2
komunistů	komunista	k1gMnPc2
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manifest	manifest	k1gInSc1
ve	v	k7c6
zhuštěné	zhuštěný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
podává	podávat	k5eAaImIp3nS
nauku	nauka	k1gFnSc4
o	o	k7c6
třídním	třídní	k2eAgInSc6d1
boji	boj	k1gInSc6
a	a	k8xC
končí	končit	k5eAaImIp3nS
vyzváním	vyzvání	k1gNnSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Proletáři	proletář	k1gMnPc1
všech	všecek	k3xTgFnPc2
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
spojte	spojit	k5eAaPmRp2nP
se	se	k3xPyFc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
brumaire	brumaire	k1gInSc1
Ludvíka	Ludvík	k1gMnSc2
Bonaparta	Bonapart	k1gMnSc2
(	(	kIx(
<g/>
1852	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spis	spis	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
publikován	publikovat	k5eAaBmNgInS
v	v	k7c6
americkém	americký	k2eAgInSc6d1
německojazyčném	německojazyčný	k2eAgInSc6d1
časopise	časopis	k1gInSc6
Die	Die	k1gFnSc1
Revolution	Revolution	k1gInSc1
<g/>
,	,	kIx,
analyzuje	analyzovat	k5eAaImIp3nS
francouzský	francouzský	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
převrat	převrat	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1851	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
nějž	jenž	k3xRgInSc2
vyšel	vyjít	k5eAaPmAgInS
Napoleon	napoleon	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
coby	coby	k?
diktátor	diktátor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
spisu	spis	k1gInSc2
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
podobnou	podobný	k2eAgFnSc4d1
událost	událost	k1gFnSc4
z	z	k7c2
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1799	#num#	k4
(	(	kIx(
<g/>
podle	podle	k7c2
tehdejšího	tehdejší	k2eAgInSc2d1
republikánského	republikánský	k2eAgInSc2d1
kalendáře	kalendář	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
brumaire	brumaire	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Napoleon	napoleon	k1gInSc1
I.	I.	kA
po	po	k7c4
svržení	svržení	k1gNnSc4
direktoria	direktorium	k1gNnSc2
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
konzulem	konzul	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
událost	událost	k1gFnSc4
Marx	Marx	k1gMnSc1
interpretuje	interpretovat	k5eAaBmIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
boje	boj	k1gInSc2
společenských	společenský	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
provádí	provádět	k5eAaImIp3nS
analýzu	analýza	k1gFnSc4
francouzské	francouzský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
vykládá	vykládat	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
pojetí	pojetí	k1gNnSc4
vztahu	vztah	k1gInSc2
společnosti	společnost	k1gFnSc2
a	a	k8xC
dějin	dějiny	k1gFnPc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Lidé	člověk	k1gMnPc1
dělají	dělat	k5eAaImIp3nP
své	svůj	k3xOyFgFnPc4
dějiny	dějiny	k1gFnPc4
sami	sám	k3xTgMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
nedělají	dělat	k5eNaImIp3nP
je	on	k3xPp3gNnPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
jim	on	k3xPp3gMnPc3
napadne	napadnout	k5eAaPmIp3nS
<g/>
,	,	kIx,
za	za	k7c2
okolností	okolnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
si	se	k3xPyFc3
sami	sám	k3xTgMnPc1
zvolili	zvolit	k5eAaPmAgMnP
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
za	za	k7c2
okolností	okolnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
tu	tu	k6eAd1
bezprostředně	bezprostředně	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základy	základ	k1gInPc1
kritiky	kritika	k1gFnSc2
politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
1857	#num#	k4
<g/>
–	–	k?
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
velmi	velmi	k6eAd1
rozsáhlý	rozsáhlý	k2eAgInSc4d1
<g/>
,	,	kIx,
nesystematický	systematický	k2eNgInSc4d1
a	a	k8xC
útržkovitý	útržkovitý	k2eAgInSc4d1
rukopis	rukopis	k1gInSc4
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
také	také	k9
pod	pod	k7c7
kratším	krátký	k2eAgNnSc7d2
označením	označení	k1gNnSc7
Die	Die	k1gMnSc2
Grundrisse	Grundriss	k1gMnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Základy	základ	k1gInPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
teprve	teprve	k6eAd1
roku	rok	k1gInSc2
1938	#num#	k4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
jakýsi	jakýsi	k3yIgInSc4
most	most	k1gInSc4
mezi	mezi	k7c7
Ekonomicko-filozofickými	ekonomicko-filozofický	k2eAgInPc7d1
rukopisy	rukopis	k1gInPc7
a	a	k8xC
Kapitálem	kapitál	k1gInSc7
<g/>
,	,	kIx,
jejž	jenž	k3xRgInSc4
připravoval	připravovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
spisu	spis	k1gInSc2
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
tzv.	tzv.	kA
předkapitalistickými	předkapitalistický	k2eAgFnPc7d1
ekonomickými	ekonomický	k2eAgFnPc7d1
formami	forma	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
se	se	k3xPyFc4
zde	zde	k6eAd1
Marx	Marx	k1gMnSc1
pouští	poušť	k1gFnPc2
do	do	k7c2
rozboru	rozbor	k1gInSc2
nadhodnoty	nadhodnota	k1gFnSc2
<g/>
,	,	kIx,
pracovní	pracovní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
,	,	kIx,
produkce	produkce	k1gFnSc2
<g/>
,	,	kIx,
distribuce	distribuce	k1gFnSc2
<g/>
,	,	kIx,
směny	směna	k1gFnSc2
<g/>
,	,	kIx,
spotřeby	spotřeba	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
ekonomických	ekonomický	k2eAgNnPc2d1
témat	téma	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
kritice	kritika	k1gFnSc3
politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Také	také	k9
tento	tento	k3xDgInSc1
spis	spis	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
přípravou	příprava	k1gFnSc7
Kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
dočkal	dočkat	k5eAaPmAgMnS
se	se	k3xPyFc4
však	však	k9
vydání	vydání	k1gNnSc1
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1859	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkoumá	zkoumat	k5eAaImIp3nS
a	a	k8xC
kritizuje	kritizovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
systém	systém	k1gInSc1
buržoazní	buržoazní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
(	(	kIx(
<g/>
kapitalismus	kapitalismus	k1gInSc4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
rozlišuje	rozlišovat	k5eAaImIp3nS
se	se	k3xPyFc4
užitná	užitný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
a	a	k8xC
směnná	směnný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
zboží	zboží	k1gNnSc2
a	a	k8xC
připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
teorie	teorie	k1gFnSc1
nadhodnoty	nadhodnota	k1gFnSc2
<g/>
,	,	kIx,
přepracovaná	přepracovaný	k2eAgFnSc1d1
v	v	k7c6
Kapitálu	kapitál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Kapitál	kapitál	k1gInSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
prvního	první	k4xOgNnSc2
vydání	vydání	k1gNnSc2
Kapitálu	kapitál	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1867	#num#	k4
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Kapitál	kapitála	k1gFnPc2
(	(	kIx(
<g/>
kniha	kniha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Desítky	desítka	k1gFnPc1
let	léto	k1gNnPc2
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
svému	svůj	k3xOyFgNnSc3
hlavnímu	hlavní	k2eAgNnSc3d1
dílu	dílo	k1gNnSc3
<g/>
,	,	kIx,
Kapitálu	kapitála	k1gFnSc4
(	(	kIx(
<g/>
s	s	k7c7
podtitulem	podtitul	k1gInSc7
Kritika	kritika	k1gFnSc1
politické	politický	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
jehož	jehož	k3xOyRp3gInPc2
tří	tři	k4xCgInPc2
(	(	kIx(
<g/>
resp.	resp.	kA
čtyř	čtyři	k4xCgMnPc2
<g/>
)	)	kIx)
dílů	díl	k1gInPc2
vyšel	vyjít	k5eAaPmAgInS
za	za	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
a	a	k8xC
třetí	třetí	k4xOgInSc4
díl	díl	k1gInSc4
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
z	z	k7c2
jeho	jeho	k3xOp3gFnSc2
pozůstalosti	pozůstalost	k1gFnSc2
Friedrichem	Friedrich	k1gMnSc7
Engelsem	Engels	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takzvaný	takzvaný	k2eAgInSc4d1
čtvrtý	čtvrtý	k4xOgInSc4
díl	díl	k1gInSc4
–	–	k?
kritické	kritický	k2eAgFnSc2d1
poznámky	poznámka	k1gFnSc2
k	k	k7c3
různým	různý	k2eAgFnPc3d1
teoriím	teorie	k1gFnPc3
nadhodnoty	nadhodnota	k1gFnSc2
–	–	k?
vydal	vydat	k5eAaPmAgInS
až	až	k9
počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
Karl	Karla	k1gFnPc2
Kautsky	Kautsky	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobní	výrobní	k2eAgInSc4d1
proces	proces	k1gInSc4
kapitálu	kapitál	k1gInSc2
(	(	kIx(
<g/>
Band	band	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
Produktionsprozess	Produktionsprozess	k1gInSc1
des	des	k1gNnSc6
Kapitals	Kapitals	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamburk	Hamburk	k1gInSc1
1867	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Díl	díl	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proces	proces	k1gInSc1
oběhu	oběh	k1gInSc2
kapitálu	kapitál	k1gInSc2
(	(	kIx(
<g/>
Band	band	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
Zirkulationsprozess	Zirkulationsprozess	k1gInSc1
des	des	k1gNnSc6
Kapitals	Kapitals	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydal	vydat	k5eAaPmAgMnS
F.	F.	kA
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
Hamburk	Hamburk	k1gInSc1
1885	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Díl	díl	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
proces	proces	k1gInSc1
kapitalistické	kapitalistický	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
(	(	kIx(
<g/>
Band	band	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
Der	drát	k5eAaImRp2nS
Gesamtprozess	Gesamtprozess	k1gInSc1
der	drát	k5eAaImRp2nS
kapitalistischen	kapitalistischen	k1gInSc4
Produktion	Produktion	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydal	vydat	k5eAaPmAgMnS
F.	F.	kA
Engels	Engels	k1gMnSc1
<g/>
,	,	kIx,
Hamburk	Hamburk	k1gInSc1
1894	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Obsahem	obsah	k1gInSc7
knihy	kniha	k1gFnSc2
je	být	k5eAaImIp3nS
analýza	analýza	k1gFnSc1
a	a	k8xC
kritika	kritika	k1gFnSc1
kapitalistické	kapitalistický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
zejména	zejména	k9
kapitalistického	kapitalistický	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
výroby	výroba	k1gFnSc2
a	a	k8xC
distribuce	distribuce	k1gFnSc2
statků	statek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
<g/>
,	,	kIx,
nejstudovanější	studovaný	k2eAgInSc1d3
část	část	k1gFnSc4
Kapitálu	kapitál	k1gInSc2
obsahuje	obsahovat	k5eAaImIp3nS
analýzy	analýza	k1gFnPc4
hodnoty	hodnota	k1gFnSc2
zboží	zboží	k1gNnSc2
a	a	k8xC
peněžního	peněžní	k2eAgInSc2d1
oběhu	oběh	k1gInSc2
<g/>
;	;	kIx,
opět	opět	k6eAd1
je	být	k5eAaImIp3nS
probírána	probírán	k2eAgFnSc1d1
užitná	užitný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
a	a	k8xC
směnná	směnný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
<g/>
,	,	kIx,
vztahy	vztah	k1gInPc1
vzniku	vznik	k1gInSc2
směnné	směnný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
a	a	k8xC
pracovní	pracovní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
dělníka	dělník	k1gMnSc2
<g/>
,	,	kIx,
přeměna	přeměna	k1gFnSc1
práce	práce	k1gFnSc2
v	v	k7c4
peníze	peníz	k1gInPc4
apod.	apod.	kA
Proto	proto	k8xC
zde	zde	k6eAd1
zavádí	zavádět	k5eAaImIp3nS
pojem	pojem	k1gInSc1
„	„	k?
<g/>
nadpráce	nadpráce	k1gFnSc2
<g/>
“	“	k?
<g/>
:	:	kIx,
dělník	dělník	k1gMnSc1
musí	muset	k5eAaImIp3nS
odpracovat	odpracovat	k5eAaPmF
určitou	určitý	k2eAgFnSc4d1
nutnou	nutný	k2eAgFnSc4d1
pracovní	pracovní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vznikla	vzniknout	k5eAaPmAgFnS
hodnota	hodnota	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
uživí	uživit	k5eAaPmIp3nS
sebe	sebe	k3xPyFc4
a	a	k8xC
rodinu	rodina	k1gFnSc4
<g/>
;	;	kIx,
poté	poté	k6eAd1
však	však	k9
musí	muset	k5eAaImIp3nS
pracovat	pracovat	k5eAaImF
dále	daleko	k6eAd2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
uživil	uživit	k5eAaPmAgMnS
také	také	k9
kapitalistu	kapitalista	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
této	tento	k3xDgFnSc2
„	„	k?
<g/>
nadpráce	nadpráce	k1gFnSc2
<g/>
“	“	k?
tedy	tedy	k9
vzniká	vznikat	k5eAaImIp3nS
nadhodnota	nadhodnota	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
však	však	k9
dělníkovi	dělník	k1gMnSc3
odcizena	odcizen	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
si	se	k3xPyFc3
ji	on	k3xPp3gFnSc4
přivlastní	přivlastnit	k5eAaPmIp3nS
kapitalista	kapitalista	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
dělníka	dělník	k1gMnSc4
vykořisťuje	vykořisťovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
také	také	k9
problému	problém	k1gInSc2
přelidnění	přelidnění	k1gNnSc2
<g/>
,	,	kIx,
dělby	dělba	k1gFnSc2
práce	práce	k1gFnSc2
a	a	k8xC
strojové	strojový	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
prodlužování	prodlužování	k1gNnSc3
pracovní	pracovní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
stroj	stroj	k1gInSc1
je	být	k5eAaImIp3nS
nejefektivnější	efektivní	k2eAgInSc1d3
<g/>
,	,	kIx,
běží	běžet	k5eAaImIp3nS
<g/>
-li	-li	k?
stále	stále	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
zboží	zboží	k1gNnSc2
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
vysvětlitelné	vysvětlitelný	k2eAgFnPc1d1
jedině	jedině	k6eAd1
ze	z	k7c2
vzájemných	vzájemný	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
výrobci	výrobce	k1gMnPc7
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
ze	z	k7c2
zboží	zboží	k1gNnSc2
samého	samý	k3xTgMnSc4
<g/>
:	:	kIx,
zatímco	zatímco	k8xS
si	se	k3xPyFc3
lidé	člověk	k1gMnPc1
myslí	myslet	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
hodnota	hodnota	k1gFnSc1
zboží	zboží	k1gNnSc2
–	–	k?
např.	např.	kA
zlata	zlato	k1gNnSc2
–	–	k?
je	být	k5eAaImIp3nS
objektivně	objektivně	k6eAd1
danou	daný	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
předmětu	předmět	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
sociálně-ekonomických	sociálně-ekonomický	k2eAgInPc6d1
vztazích	vztah	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buržoazní	buržoazní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
připisuje	připisovat	k5eAaImIp3nS
zboží	zboží	k1gNnSc4
nevnímatelné	vnímatelný	k2eNgFnSc2d1
vlastnosti	vlastnost	k1gFnSc2
a	a	k8xC
hodnoty	hodnota	k1gFnSc2
a	a	k8xC
„	„	k?
<g/>
uctívá	uctívat	k5eAaImIp3nS
<g/>
“	“	k?
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
tak	tak	k6eAd1
dopouští	dopouštět	k5eAaImIp3nS
„	„	k?
<g/>
zbožního	zbožní	k2eAgInSc2d1
fetišismu	fetišismus	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
přirovnáván	přirovnávat	k5eAaImNgInS
k	k	k7c3
uctívání	uctívání	k1gNnSc3
svatých	svatý	k2eAgInPc2d1
ostatků	ostatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
díl	díl	k1gInSc1
analyzuje	analyzovat	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc4
fázi	fáze	k1gFnSc4
kapitálu	kapitál	k1gInSc2
<g/>
:	:	kIx,
po	po	k7c6
fázi	fáze	k1gFnSc6
produkce	produkce	k1gFnSc2
v	v	k7c6
prvním	první	k4xOgInSc6
dílu	díl	k1gInSc6
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
různé	různý	k2eAgFnPc4d1
formy	forma	k1gFnPc4
oběhu	oběh	k1gInSc2
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgInPc4
díl	díl	k1gInSc4
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
vysvětlit	vysvětlit	k5eAaPmF
přeměnu	přeměna	k1gFnSc4
nadhodnoty	nadhodnota	k1gFnSc2
v	v	k7c4
zisk	zisk	k1gInSc4
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
mj.	mj.	kA
takzvaný	takzvaný	k2eAgInSc1d1
„	„	k?
<g/>
zákon	zákon	k1gInSc1
tendence	tendence	k1gFnSc2
míry	míra	k1gFnSc2
zisku	zisk	k1gInSc2
klesat	klesat	k5eAaImF
<g/>
“	“	k?
<g/>
;	;	kIx,
příčinu	příčina	k1gFnSc4
krizí	krize	k1gFnPc2
kapitalismu	kapitalismus	k1gInSc2
tak	tak	k8xS,k8xC
Marx	Marx	k1gMnSc1
klade	klást	k5eAaImIp3nS
přímo	přímo	k6eAd1
do	do	k7c2
procesu	proces	k1gInSc2
výroby	výroba	k1gFnSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kapitál	kapitál	k1gInSc1
byl	být	k5eAaImAgInS
marxismem-leninismem	marxismem-leninismus	k1gInSc7
vykládán	vykládat	k5eAaImNgInS
dogmaticky	dogmaticky	k6eAd1
<g/>
,	,	kIx,
tj.	tj.	kA
jako	jako	k8xC,k8xS
soustava	soustava	k1gFnSc1
neměnných	neměnný	k2eAgFnPc2d1,k2eNgFnPc2d1
zákonitostí	zákonitost	k1gFnPc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
samotné	samotný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
není	být	k5eNaImIp3nS
systematické	systematický	k2eAgNnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
spíše	spíše	k9
charakter	charakter	k1gInSc1
hledání	hledání	k1gNnSc1
<g/>
;	;	kIx,
nebylo	být	k5eNaImAgNnS
ostatně	ostatně	k6eAd1
nikdy	nikdy	k6eAd1
dokončeno	dokončit	k5eAaPmNgNnS
a	a	k8xC
většina	většina	k1gFnSc1
ho	on	k3xPp3gMnSc4
zůstala	zůstat	k5eAaPmAgFnS
ve	v	k7c6
fragmentech	fragment	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
než	než	k8xS
poučky	poučka	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
hypotézy	hypotéza	k1gFnPc4
<g/>
,	,	kIx,
nejrůznější	různý	k2eAgFnPc4d3
odbočky	odbočka	k1gFnPc4
<g/>
,	,	kIx,
anekdotická	anekdotický	k2eAgNnPc4d1
vyprávění	vyprávění	k1gNnPc4
<g/>
,	,	kIx,
příklady	příklad	k1gInPc4
z	z	k7c2
praxe	praxe	k1gFnSc2
<g/>
,	,	kIx,
citace	citace	k1gFnPc1
novinových	novinový	k2eAgInPc2d1
článků	článek	k1gInPc2
o	o	k7c6
různých	různý	k2eAgFnPc6d1
tragédiích	tragédie	k1gFnPc6
a	a	k8xC
otřesných	otřesný	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
dělníků	dělník	k1gMnPc2
či	či	k8xC
o	o	k7c6
dětské	dětský	k2eAgFnSc6d1
práci	práce	k1gFnSc6
<g/>
,	,	kIx,
jedovaté	jedovatý	k2eAgInPc4d1
výpady	výpad	k1gInPc4
proti	proti	k7c3
kapitalismu	kapitalismus	k1gInSc3
apod.	apod.	kA
<g/>
;	;	kIx,
v	v	k7c6
nadsázce	nadsázka	k1gFnSc6
se	se	k3xPyFc4
někdy	někdy	k6eAd1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
že	že	k8xS
Kapitál	kapitál	k1gInSc4
lze	lze	k6eAd1
číst	číst	k5eAaImF
jako	jako	k8xC,k8xS
strašidelný	strašidelný	k2eAgInSc4d1
gotický	gotický	k2eAgInSc4d1
román	román	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Například	například	k6eAd1
v	v	k7c6
osmé	osmý	k4xOgFnSc6
kapitole	kapitola	k1gFnSc6
prvního	první	k4xOgInSc2
dílu	díl	k1gInSc2
Marx	Marx	k1gMnSc1
udává	udávat	k5eAaImIp3nS
příklady	příklad	k1gInPc1
porušování	porušování	k1gNnSc1
továrního	tovární	k2eAgInSc2d1
zákona	zákon	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1833	#num#	k4
(	(	kIx(
<g/>
Factory	Factor	k1gInPc1
Act	Act	k1gFnSc2
of	of	k?
1833	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
omezoval	omezovat	k5eAaImAgInS
pracovní	pracovní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
dětí	dítě	k1gFnPc2
v	v	k7c6
továrnách	továrna	k1gFnPc6
na	na	k7c6
max	max	kA
<g/>
.	.	kIx.
12	#num#	k4
hodin	hodina	k1gFnPc2
denně	denně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Začátkem	začátkem	k7c2
června	červen	k1gInSc2
1836	#num#	k4
došla	dojít	k5eAaPmAgFnS
smírčím	smírčí	k2eAgMnPc3d1
soudcům	soudce	k1gMnPc3
v	v	k7c4
Dewsbury	Dewsbura	k1gFnPc4
(	(	kIx(
<g/>
Yorkshire	Yorkshir	k1gInSc5
<g/>
)	)	kIx)
udání	udání	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
majitelé	majitel	k1gMnPc1
8	#num#	k4
velkých	velká	k1gFnPc2
továren	továrna	k1gFnPc2
nedaleko	nedaleko	k7c2
Batley	Batlea	k1gFnSc2
přestupují	přestupovat	k5eAaImIp3nP
tovární	tovární	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
těchto	tento	k3xDgMnPc2
pánů	pan	k1gMnPc2
byla	být	k5eAaImAgFnS
obviněna	obviněn	k2eAgFnSc1d1
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
nechali	nechat	k5eAaPmAgMnP
pracovat	pracovat	k5eAaImF
5	#num#	k4
chlapců	chlapec	k1gMnPc2
ve	v	k7c6
věku	věk	k1gInSc6
od	od	k7c2
12	#num#	k4
do	do	k7c2
15	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
6	#num#	k4
hodin	hodina	k1gFnPc2
ráno	ráno	k6eAd1
v	v	k7c4
pátek	pátek	k1gInSc4
do	do	k7c2
4	#num#	k4
hodin	hodina	k1gFnPc2
odpoledne	odpoledne	k6eAd1
následující	následující	k2eAgFnPc4d1
soboty	sobota	k1gFnPc4
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
jim	on	k3xPp3gMnPc3
dovolili	dovolit	k5eAaPmAgMnP
oddech	oddech	k1gInSc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
doby	doba	k1gFnSc2
na	na	k7c4
jídlo	jídlo	k1gNnSc4
a	a	k8xC
jedné	jeden	k4xCgFnSc2
hodiny	hodina	k1gFnSc2
spánku	spánek	k1gInSc2
o	o	k7c6
půlnoci	půlnoc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tyto	tento	k3xDgFnPc1
děti	dítě	k1gFnPc1
musely	muset	k5eAaImAgFnP
pracovat	pracovat	k5eAaImF
nepřetržitě	přetržitě	k6eNd1
30	#num#	k4
hodin	hodina	k1gFnPc2
v	v	k7c6
"	"	kIx"
<g/>
Shoddy	Shodda	k1gFnPc4
hole	hole	k6eAd1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
díře	díra	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
trhají	trhat	k5eAaImIp3nP
vlněné	vlněný	k2eAgInPc1d1
hadry	hadr	k1gInPc1
a	a	k8xC
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
vzduch	vzduch	k1gInSc1
tak	tak	k6eAd1
prosycen	prosytit	k5eAaPmNgInS
prachem	prach	k1gInSc7
<g/>
,	,	kIx,
chloupky	chloupek	k1gInPc4
z	z	k7c2
hadrů	hadr	k1gInPc2
atd.	atd.	kA
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
dospělí	dospělý	k2eAgMnPc1d1
dělníci	dělník	k1gMnPc1
si	se	k3xPyFc3
tu	tu	k6eAd1
musí	muset	k5eAaImIp3nS
ustavičně	ustavičně	k6eAd1
ovazovat	ovazovat	k5eAaImF
ústa	ústa	k1gNnPc4
kapesníkem	kapesník	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
si	se	k3xPyFc3
chránili	chránit	k5eAaImAgMnP
plíce	plíce	k1gFnPc4
<g/>
!	!	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Kapitál	kapitál	k1gInSc1
<g/>
,	,	kIx,
I.	I.	kA
díl	díl	k1gInSc1
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
kapitola	kapitola	k1gFnSc1
</s>
<s>
Kritika	kritika	k1gFnSc1
Gothajského	gothajský	k2eAgInSc2d1
programu	program	k1gInSc2
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spis	spis	k1gInSc1
<g/>
,	,	kIx,
vydaný	vydaný	k2eAgInSc1d1
posmrtně	posmrtně	k6eAd1
roku	rok	k1gInSc2
1891	#num#	k4
<g/>
,	,	kIx,
kriticky	kriticky	k6eAd1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
Gothajský	gothajský	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
přijala	přijmout	k5eAaPmAgFnS
právě	právě	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
o	o	k7c6
programu	program	k1gInSc6
vyjadřuje	vyjadřovat	k5eAaImIp3nS
velice	velice	k6eAd1
nepříznivě	příznivě	k6eNd1
a	a	k8xC
považuje	považovat	k5eAaImIp3nS
ho	on	k3xPp3gNnSc4
za	za	k7c4
příliš	příliš	k6eAd1
kompromisní	kompromisní	k2eAgInSc4d1
<g/>
,	,	kIx,
nevědecký	vědecký	k2eNgInSc4d1
<g/>
,	,	kIx,
nedostatečně	dostatečně	k6eNd1
internacionální	internacionální	k2eAgFnSc1d1
<g/>
,	,	kIx,
„	„	k?
<g/>
zavrženíhodný	zavrženíhodný	k2eAgInSc1d1
a	a	k8xC
pro	pro	k7c4
stranu	strana	k1gFnSc4
demoralizující	demoralizující	k2eAgFnSc4d1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
spise	spis	k1gInSc6
jsou	být	k5eAaImIp3nP
patrné	patrný	k2eAgInPc1d1
vznikající	vznikající	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c4
formující	formující	k2eAgInPc4d1
se	se	k3xPyFc4
sociální	sociální	k2eAgFnSc7d1
demokracií	demokracie	k1gFnSc7
a	a	k8xC
klasickým	klasický	k2eAgInSc7d1
marxismem	marxismus	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Marx	Marx	k1gMnSc1
zde	zde	k6eAd1
také	také	k9
naznačuje	naznačovat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
etapy	etapa	k1gFnPc4
revoluční	revoluční	k2eAgFnSc2d1
proměny	proměna	k1gFnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
až	až	k9
po	po	k7c6
první	první	k4xOgFnSc6
etapě	etapa	k1gFnSc6
vzrůstu	vzrůst	k1gInSc2
produktivity	produktivita	k1gFnSc2
bude	být	k5eAaImBp3nS
všeho	všecek	k3xTgNnSc2
dostatek	dostatek	k1gInSc1
<g/>
,	,	kIx,
ztratí	ztratit	k5eAaPmIp3nS
soukromé	soukromý	k2eAgNnSc4d1
vlastnictví	vlastnictví	k1gNnSc4
smysl	smysl	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
...	...	k?
<g/>
až	až	k9
s	s	k7c7
všestranným	všestranný	k2eAgInSc7d1
rozvojem	rozvoj	k1gInSc7
jednotlivců	jednotlivec	k1gMnPc2
vzrostou	vzrůst	k5eAaPmIp3nP
i	i	k9
výrobní	výrobní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
a	a	k8xC
všechny	všechen	k3xTgInPc1
zdroje	zdroj	k1gInPc1
sdruženého	sdružený	k2eAgNnSc2d1
bohatství	bohatství	k1gNnSc2
potečou	téct	k5eAaImIp3nP
plným	plný	k2eAgInSc7d1
proudem	proud	k1gInSc7
<g/>
,	,	kIx,
společnost	společnost	k1gFnSc1
bude	být	k5eAaImBp3nS
moci	moct	k5eAaImF
vepsat	vepsat	k5eAaPmF
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
prapor	prapor	k1gInSc4
<g/>
:	:	kIx,
Každý	každý	k3xTgMnSc1
podle	podle	k7c2
svých	svůj	k3xOyFgFnPc2
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
každému	každý	k3xTgMnSc3
podle	podle	k7c2
jeho	jeho	k3xOp3gFnPc2
potřeb	potřeba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Kritika	kritika	k1gFnSc1
Gothajského	gothajský	k2eAgInSc2d1
programu	program	k1gInSc2
<g/>
,	,	kIx,
I.	I.	kA
</s>
<s>
Protislovanské	protislovanský	k2eAgInPc1d1
postoje	postoj	k1gInPc1
</s>
<s>
Po	po	k7c4
určitou	určitý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
bylo	být	k5eAaImAgNnS
zaměření	zaměření	k1gNnSc1
Marxe	Marx	k1gMnSc2
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
Engelse	Engels	k1gMnSc4
značně	značně	k6eAd1
proti-slovanské	proti-slovanský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
revoluce	revoluce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
slovanské	slovanský	k2eAgInPc1d1
národy	národ	k1gInPc1
v	v	k7c6
rakouské	rakouský	k2eAgFnSc6d1
řiši	řiš	k1gFnSc6
nepodpořily	podpořit	k5eNaPmAgInP
německou	německý	k2eAgFnSc4d1
a	a	k8xC
maďarskou	maďarský	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
a	a	k8xC
londýnské	londýnský	k2eAgNnSc4d1
Times	Timesa	k1gFnPc2
opisovaly	opisovat	k5eAaImAgFnP
z	z	k7c2
pražského	pražský	k2eAgInSc2d1
německého	německý	k2eAgInSc2d1
listu	list	k1gInSc2
Bohemie	bohemia	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
povstání	povstání	k1gNnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
„	„	k?
<g/>
mělo	mít	k5eAaImAgNnS
za	za	k7c4
cíl	cíl	k1gInSc4
vytvořit	vytvořit	k5eAaPmF
s	s	k7c7
ruskou	ruský	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
českou	český	k2eAgFnSc4d1
říši	říše	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
Marx	Marx	k1gMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
Neue	Neue	k1gFnPc6
Rheinische	Rheinisch	k1gFnSc2
Zeitung	Zeitung	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Teď	teď	k6eAd1
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
nepřátelé	nepřítel	k1gMnPc1
revoluce	revoluce	k1gFnSc2
–	–	k?
v	v	k7c6
Rusku	Rusko	k1gNnSc6
a	a	k8xC
ve	v	k7c6
slovanských	slovanský	k2eAgFnPc6d1
provinciích	provincie	k1gFnPc6
Rakouska	Rakousko	k1gNnSc2
<g/>
…	…	k?
Nesmiřitelný	smiřitelný	k2eNgInSc1d1
boj	boj	k1gInSc1
<g/>
,	,	kIx,
válka	válka	k1gFnSc1
až	až	k9
do	do	k7c2
posledního	poslední	k2eAgInSc2d1
dechu	dech	k1gInSc2
proti	proti	k7c3
Slovanům	Slovan	k1gMnPc3
<g/>
,	,	kIx,
zrádcům	zrádce	k1gMnPc3
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
vyhlazení	vyhlazení	k1gNnSc1
<g/>
,	,	kIx,
terorismus	terorismus	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Ne	Ne	kA
v	v	k7c6
zájmu	zájem	k1gInSc6
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
v	v	k7c6
zájmu	zájem	k1gInSc6
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
projevu	projev	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1867	#num#	k4
Marx	Marx	k1gMnSc1
odsoudil	odsoudit	k5eAaPmAgMnS
„	„	k?
<g/>
panslavistickou	panslavistický	k2eAgFnSc4d1
propagandu	propaganda	k1gFnSc4
<g/>
“	“	k?
šířenou	šířený	k2eAgFnSc4d1
z	z	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
připomněl	připomnět	k5eAaPmAgInS
revoluční	revoluční	k2eAgNnPc4d1
léta	léto	k1gNnPc4
1848	#num#	k4
<g/>
-	-	kIx~
<g/>
49	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
podle	podle	k7c2
Marxe	Marx	k1gMnSc2
bylo	být	k5eAaImAgNnS
Maďarsko	Maďarsko	k1gNnSc1
napadeno	napadnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
Vídeň	Vídeň	k1gFnSc1
zničena	zničit	k5eAaPmNgFnS
a	a	k8xC
Itálie	Itálie	k1gFnSc1
rozdrcena	rozdrcen	k2eAgFnSc1d1
Slovany	Slovan	k1gInPc7
bojujícími	bojující	k2eAgInPc7d1
pod	pod	k7c7
vlajkami	vlajka	k1gFnPc7
Jelačiće	Jelačiće	k1gFnSc1
<g/>
,	,	kIx,
Windischgrätze	Windischgrätze	k1gFnSc1
a	a	k8xC
Radeckého	Radeckého	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marx	Marx	k1gMnSc1
a	a	k8xC
Engels	Engels	k1gMnSc1
byli	být	k5eAaImAgMnP
velkými	velký	k2eAgMnPc7d1
odpůrci	odpůrce	k1gMnPc7
carského	carský	k2eAgNnSc2d1
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
považovali	považovat	k5eAaImAgMnP
za	za	k7c4
kontrarevoluční	kontrarevoluční	k2eAgFnSc4d1
hrozbu	hrozba	k1gFnSc4
evropským	evropský	k2eAgFnPc3d1
revolucím	revoluce	k1gFnPc3
<g/>
,	,	kIx,
a	a	k8xC
poukazovali	poukazovat	k5eAaImAgMnP
na	na	k7c4
ruskou	ruský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
při	při	k7c6
potlačení	potlačení	k1gNnSc6
maďarské	maďarský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
podporoval	podporovat	k5eAaImAgMnS
protiruská	protiruský	k2eAgNnPc4d1
povstání	povstání	k1gNnSc4
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nezávislý	závislý	k2eNgInSc1d1
polský	polský	k2eAgInSc1d1
stát	stát	k1gInSc1
může	moct	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
nárazníkovou	nárazníkový	k2eAgFnSc4d1
zónu	zóna	k1gFnSc4
mezi	mezi	k7c7
Ruskem	Rusko	k1gNnSc7
a	a	k8xC
Evropou	Evropa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
projevu	projev	k1gInSc2
v	v	k7c6
Londýně	Londýn	k1gInSc6
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1867	#num#	k4
veřejně	veřejně	k6eAd1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
existuje	existovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
jediná	jediný	k2eAgFnSc1d1
alternativa	alternativa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Buď	buď	k8xC
asijský	asijský	k2eAgInSc1d1
barbarismus	barbarismus	k1gInSc1
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
exploduje	explodovat	k5eAaBmIp3nS
kolem	kolem	k7c2
Evropy	Evropa	k1gFnSc2
jako	jako	k8xS,k8xC
lavina	lavina	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
musíme	muset	k5eAaImIp1nP
obnovit	obnovit	k5eAaPmF
Polsko	Polsko	k1gNnSc4
<g/>
,	,	kIx,
postavit	postavit	k5eAaPmF
dvacet	dvacet	k4xCc4
milionů	milion	k4xCgInPc2
hrdinů	hrdina	k1gMnPc2
mezi	mezi	k7c7
nás	my	k3xPp1nPc2
a	a	k8xC
Asii	Asie	k1gFnSc6
a	a	k8xC
získat	získat	k5eAaPmF
tak	tak	k9
čas	čas	k1gInSc4
pro	pro	k7c4
uskutečnění	uskutečnění	k1gNnSc4
evropské	evropský	k2eAgFnSc2d1
sociální	sociální	k2eAgFnSc2d1
obrody	obroda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Rusko	Rusko	k1gNnSc1
označil	označit	k5eAaPmAgMnS
za	za	k7c4
zemi	zem	k1gFnSc4
toužící	toužící	k2eAgFnSc4d1
po	po	k7c6
světovládě	světovláda	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
civilizovanou	civilizovaný	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
ovládající	ovládající	k2eAgFnSc4d1
„	„	k?
<g/>
barbarské	barbarský	k2eAgFnSc2d1
masy	masa	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marxův	Marxův	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
a	a	k8xC
kritika	kritika	k1gFnSc1
</s>
<s>
Západoněmecká	západoněmecký	k2eAgFnSc1d1
poštovní	poštovní	k2eAgFnSc1d1
známka	známka	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Marxismus	marxismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Marx	Marx	k1gMnSc1
v	v	k7c6
XIX	XIX	kA
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
nejgeniálnějším	geniální	k2eAgMnSc7d3
myslitelem	myslitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
jednoho	jeden	k4xCgMnSc4
národohospodáře	národohospodář	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
Marxem	Marx	k1gMnSc7
nezabýval	zabývat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
od	od	k7c2
něho	on	k3xPp3gMnSc2
mnoho	mnoho	k6eAd1
naučil	naučit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
působení	působení	k1gNnSc1
bylo	být	k5eAaImAgNnS
ohromné	ohromný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marxism	Marxism	k1gInSc1
vynutil	vynutit	k5eAaPmAgInS
reformy	reforma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociální	sociální	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
jsou	být	k5eAaImIp3nP
prací	prací	k2eAgFnSc7d1
ideí	ide	k1gFnSc7
Marxových	Marxová	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
duchovní	duchovní	k2eAgInPc4d1
směry	směr	k1gInPc4
<g/>
,	,	kIx,
politiku	politika	k1gFnSc4
vlád	vláda	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
vše	všechen	k3xTgNnSc4
měl	mít	k5eAaImAgInS
marxismus	marxismus	k1gInSc1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
<g/>
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigue	k1gNnSc1
Masaryk	Masaryk	k1gMnSc1
<g/>
,	,	kIx,
Velicí	veliký	k2eAgMnPc1d1
mužové	muž	k1gMnPc1
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1
teorie	teorie	k1gFnPc1
byly	být	k5eAaImAgFnP
pozdějšími	pozdní	k2eAgMnPc7d2
marxisty	marxista	k1gMnPc4
interpretovány	interpretován	k2eAgMnPc4d1
různě	různě	k6eAd1
až	až	k8xS
protikladně	protikladně	k6eAd1
<g/>
:	:	kIx,
od	od	k7c2
nerevoluční	revoluční	k2eNgFnSc2d1
<g/>
,	,	kIx,
reformistické	reformistický	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
sociální	sociální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
Karla	Karel	k1gMnSc2
Kautskyho	Kautsky	k1gMnSc2
přes	přes	k7c4
dogmatické	dogmatický	k2eAgFnPc4d1
interpretace	interpretace	k1gFnPc4
tzv.	tzv.	kA
reálného	reálný	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
někdejšího	někdejší	k2eAgInSc2d1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
satelitů	satelit	k1gMnPc2
<g/>
,	,	kIx,
čínského	čínský	k2eAgInSc2d1
maoismu	maoismus	k1gInSc2
<g/>
,	,	kIx,
až	až	k9
k	k	k7c3
nedogmatickým	dogmatický	k2eNgFnPc3d1
<g/>
,	,	kIx,
sociálně	sociálně	k6eAd1
kritickým	kritický	k2eAgMnPc3d1
teoretikům	teoretik	k1gMnPc3
<g/>
,	,	kIx,
jako	jako	k9
byli	být	k5eAaImAgMnP
Antonio	Antonio	k1gMnSc1
Gramsci	Gramsek	k1gMnPc1
<g/>
,	,	kIx,
György	Györg	k1gMnPc4
Lukács	Lukácsa	k1gFnPc2
<g/>
,	,	kIx,
myslitelé	myslitel	k1gMnPc1
Frankfurtské	frankfurtský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
nebo	nebo	k8xC
pozdější	pozdní	k2eAgFnSc1d2
Nová	nový	k2eAgFnSc1d1
levice	levice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myšlení	myšlení	k1gNnSc1
mladého	mladý	k2eAgMnSc2d1
Marxe	Marx	k1gMnSc2
ovlivnilo	ovlivnit	k5eAaPmAgNnS
také	také	k9
reformní	reformní	k2eAgMnPc4d1
socialisty	socialist	k1gMnPc4
v	v	k7c6
období	období	k1gNnSc6
tzv.	tzv.	kA
pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
nebo	nebo	k8xC
podobně	podobně	k6eAd1
orientovanou	orientovaný	k2eAgFnSc4d1
jugoslávskou	jugoslávský	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
Praxis	Praxis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Marx	Marx	k1gMnSc1
na	na	k7c6
nástěnné	nástěnný	k2eAgFnSc6d1
malbě	malba	k1gFnSc6
od	od	k7c2
Diega	Dieg	k1gMnSc2
Rivery	Rivera	k1gFnSc2
v	v	k7c6
Národním	národní	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Mexico	Mexico	k1gNnSc4
City	City	k1gFnSc2
</s>
<s>
O	o	k7c6
jeho	jeho	k3xOp3gInSc6
přetrvávajícím	přetrvávající	k2eAgInSc6d1
vlivu	vliv	k1gInSc6
svědčí	svědčit	k5eAaImIp3nS
kromě	kromě	k7c2
teoretických	teoretický	k2eAgFnPc2d1
diskusí	diskuse	k1gFnPc2
také	také	k9
opětovný	opětovný	k2eAgInSc4d1
zájem	zájem	k1gInSc4
o	o	k7c4
jeho	jeho	k3xOp3gInPc4
spisy	spis	k1gInPc4
v	v	k7c6
době	doba	k1gFnSc6
hospodářské	hospodářský	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
umístění	umístění	k1gNnSc4
na	na	k7c6
třetí	třetí	k4xOgFnSc6
příčce	příčka	k1gFnSc6
v	v	k7c6
soutěži	soutěž	k1gFnSc6
o	o	k7c6
„	„	k?
<g/>
největšího	veliký	k2eAgMnSc2d3
Němce	Němec	k1gMnSc2
<g/>
“	“	k?
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
nebo	nebo	k8xC
popularita	popularita	k1gFnSc1
patrně	patrně	k6eAd1
nejznámějšího	známý	k2eAgMnSc2d3
současného	současný	k2eAgMnSc2d1
myslitele	myslitel	k1gMnSc2
navazujícího	navazující	k2eAgMnSc2d1
na	na	k7c4
Marxe	Marx	k1gMnSc4
<g/>
,	,	kIx,
Slovince	Slovinec	k1gMnSc4
Slavoje	Slavoj	k1gMnSc4
Žižeka	Žižeek	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
četné	četný	k2eAgFnPc4d1
kritiky	kritika	k1gFnPc4
patřili	patřit	k5eAaImAgMnP
představitelé	představitel	k1gMnPc1
křesťanství	křesťanství	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
náboženství	náboženství	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
obecně	obecně	k6eAd1
odmítal	odmítat	k5eAaImAgMnS
(	(	kIx(
<g/>
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
díla	dílo	k1gNnSc2
však	však	k9
nikdy	nikdy	k6eAd1
nebyla	být	k5eNaImAgFnS
katolickou	katolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
zařazena	zařadit	k5eAaPmNgFnS
mezi	mezi	k7c4
zakázané	zakázaný	k2eAgFnPc4d1
knihy	kniha	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc7
analýzou	analýza	k1gFnSc7
kapitalismu	kapitalismus	k1gInSc2
inspirovala	inspirovat	k5eAaBmAgFnS
tzv.	tzv.	kA
teologie	teologie	k1gFnSc1
osvobození	osvobození	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Složitý	složitý	k2eAgInSc1d1
a	a	k8xC
různorodý	různorodý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
vztah	vztah	k1gInSc1
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
<g/>
)	)	kIx)
liberálních	liberální	k2eAgMnPc2d1
myslitelů	myslitel	k1gMnPc2
k	k	k7c3
Marxovi	Marx	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
oceňují	oceňovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gNnSc1
analýzy	analýza	k1gFnPc1
kapitalismu	kapitalismus	k1gInSc2
<g/>
,	,	kIx,
stavějí	stavět	k5eAaImIp3nP
se	se	k3xPyFc4
však	však	k9
proti	proti	k7c3
vědeckému	vědecký	k2eAgNnSc3d1
pojetí	pojetí	k1gNnSc3
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filosof	filosof	k1gMnSc1
Karl	Karl	k1gMnSc1
Raimund	Raimunda	k1gFnPc2
Popper	Popper	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
Bídě	bída	k1gFnSc6
historicismu	historicismus	k1gInSc2
nachází	nacházet	k5eAaImIp3nS
u	u	k7c2
Marxe	Marx	k1gMnSc2
fatalistické	fatalistický	k2eAgNnSc4d1
pojetí	pojetí	k1gNnSc4
dějin	dějiny	k1gFnPc2
<g/>
:	:	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
známe	znát	k5eAaImIp1nP
neměnné	měnný	k2eNgFnPc1d1,k2eAgFnPc1d1
zákonitosti	zákonitost	k1gFnPc1
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
měnit	měnit	k5eAaImF
společnost	společnost	k1gFnSc4
„	„	k?
<g/>
proti	proti	k7c3
nim	on	k3xPp3gNnPc3
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
vzniká	vznikat	k5eAaImIp3nS
naopak	naopak	k6eAd1
„	„	k?
<g/>
vědecké	vědecký	k2eAgFnSc2d1
<g/>
“	“	k?
centrální	centrální	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
může	moct	k5eAaImIp3nS
přerůst	přerůst	k5eAaPmF
do	do	k7c2
totalitarismu	totalitarismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Marxismus	marxismus	k1gInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
něho	on	k3xPp3gMnSc2
„	„	k?
<g/>
nejčistší	čistý	k2eAgFnSc7d3
<g/>
,	,	kIx,
nejrozvinutější	rozvinutý	k2eAgFnSc7d3
a	a	k8xC
nejnebezpečnější	bezpečný	k2eNgFnSc7d3
podobou	podoba	k1gFnSc7
historicismu	historicismus	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
bývá	bývat	k5eAaImIp3nS
namítáno	namítán	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
zánik	zánik	k1gInSc1
kapitalismu	kapitalismus	k1gInSc2
nevyhnutelný	vyhnutelný	k2eNgInSc1d1
<g/>
,	,	kIx,
zůstává	zůstávat	k5eAaImIp3nS
organizovaná	organizovaný	k2eAgFnSc1d1
akce	akce	k1gFnSc1
vykořisťovaných	vykořisťovaný	k1gMnPc2
neopodstatněná	opodstatněný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Poppera	Popper	k1gMnSc4
již	již	k6eAd1
samotná	samotný	k2eAgFnSc1d1
dialektická	dialektický	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
je	být	k5eAaImIp3nS
pouhým	pouhý	k2eAgInSc7d1
zesíleným	zesílený	k2eAgInSc7d1
dogmatismem	dogmatismus	k1gInSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
zůstává	zůstávat	k5eAaImIp3nS
v	v	k7c6
očividném	očividný	k2eAgInSc6d1
nesouladu	nesoulad	k1gInSc6
s	s	k7c7
pravidly	pravidlo	k1gNnPc7
logiky	logika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popper	Popper	k1gMnSc1
odmítal	odmítat	k5eAaImAgMnS
marxistický	marxistický	k2eAgInSc4d1
ekonomismus	ekonomismus	k1gInSc4
<g/>
,	,	kIx,
tj.	tj.	kA
pojímání	pojímání	k1gNnSc1
člověka	člověk	k1gMnSc2
jako	jako	k8xC,k8xS
bytosti	bytost	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc2
činnosti	činnost	k1gFnSc2
a	a	k8xC
chápání	chápání	k1gNnSc2
světa	svět	k1gInSc2
jsou	být	k5eAaImIp3nP
určeny	určen	k2eAgInPc1d1
ekonomickými	ekonomický	k2eAgInPc7d1
zájmy	zájem	k1gInPc7
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
chápání	chápání	k1gNnSc3
celých	celý	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
jako	jako	k8xS,k8xC
třídní	třídní	k2eAgInSc1d1
zápas	zápas	k1gInSc1
o	o	k7c4
ekonomickou	ekonomický	k2eAgFnSc4d1
nadvládu	nadvláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marxistická	marxistický	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
třídního	třídní	k2eAgInSc2d1
boje	boj	k1gInSc2
je	být	k5eAaImIp3nS
dle	dle	k7c2
něj	on	k3xPp3gMnSc2
velmi	velmi	k6eAd1
podobná	podobný	k2eAgFnSc1d1
koncepci	koncepce	k1gFnSc4
boje	boj	k1gInSc2
o	o	k7c4
rasovou	rasový	k2eAgFnSc4d1
nadvládu	nadvláda	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
vyznávají	vyznávat	k5eAaImIp3nP
rasistické	rasistický	k2eAgFnPc1d1
doktríny	doktrína	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
používání	používání	k1gNnSc3
indukční	indukční	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
Popper	Popper	k1gMnSc1
pokládal	pokládat	k5eAaImAgMnS
marxismus	marxismus	k1gInSc4
buď	buď	k8xC
za	za	k7c4
směs	směs	k1gFnSc4
vědy	věda	k1gFnSc2
a	a	k8xC
proroctví	proroctví	k1gNnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
za	za	k7c4
pavědu	pavěda	k1gFnSc4
<g/>
,	,	kIx,
anebo	anebo	k8xC
také	také	k9
ničím	ničit	k5eAaImIp1nS
neopodstatněné	opodstatněný	k2eNgNnSc4d1
všeználkovství	všeználkovství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobně	obdobně	k6eAd1
kriticky	kriticky	k6eAd1
se	se	k3xPyFc4
stavěl	stavět	k5eAaImAgMnS
i	i	k9
k	k	k7c3
psychoanalýze	psychoanalýza	k1gFnSc3
Sigmunda	Sigmund	k1gMnSc2
Freuda	Freud	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
Poppera	Popper	k1gMnSc2
také	také	k9
existuje	existovat	k5eAaImIp3nS
ideová	ideový	k2eAgFnSc1d1
příbuznost	příbuznost	k1gFnSc1
mezi	mezi	k7c7
marxismem	marxismus	k1gInSc7
a	a	k8xC
fašismem	fašismus	k1gInSc7
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
však	však	k9
dle	dle	k7c2
něj	on	k3xPp3gInSc2
identické	identický	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popper	Popper	k1gMnSc1
byl	být	k5eAaImAgMnS
více	hodně	k6eAd2
kritičtější	kritický	k2eAgMnSc1d2
vůči	vůči	k7c3
fašismu	fašismus	k1gInSc3
než	než	k8xS
vůči	vůči	k7c3
marxismu	marxismus	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sousoší	sousoší	k1gNnSc1
Marxe	Marx	k1gMnSc2
a	a	k8xC
Engelse	Engels	k1gMnSc2
ve	v	k7c6
veřejném	veřejný	k2eAgInSc6d1
parku	park	k1gInSc6
v	v	k7c6
Šanghaji	Šanghaj	k1gFnSc6
v	v	k7c6
Číně	Čína	k1gFnSc6
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7
velkým	velký	k2eAgMnSc7d1
znalcem	znalec	k1gMnSc7
byl	být	k5eAaImAgMnS
především	především	k9
Raymond	Raymond	k1gMnSc1
Aron	Aron	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
knihy	kniha	k1gFnSc2
Marxův	Marxův	k2eAgInSc1d1
marxismus	marxismus	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
autora	autor	k1gMnSc2
Kapitálu	kapitál	k1gInSc2
respektoval	respektovat	k5eAaImAgInS
<g/>
,	,	kIx,
zato	zato	k6eAd1
však	však	k9
tvrdě	tvrdě	k6eAd1
kritizoval	kritizovat	k5eAaImAgMnS
soudobý	soudobý	k2eAgMnSc1d1
<g/>
,	,	kIx,
zejména	zejména	k9
francouzský	francouzský	k2eAgInSc1d1
marxismus	marxismus	k1gInSc1
(	(	kIx(
<g/>
Althusser	Althusser	k1gInSc1
<g/>
,	,	kIx,
Sartre	Sartr	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
podle	podle	k7c2
něho	on	k3xPp3gNnSc2
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
kritické	kritický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
další	další	k2eAgFnSc2d1
ideologie	ideologie	k1gFnSc2
<g/>
,	,	kIx,
spíše	spíše	k9
politickým	politický	k2eAgNnSc7d1
náboženstvím	náboženství	k1gNnSc7
<g/>
;	;	kIx,
proto	proto	k8xC
v	v	k7c6
parafrázi	parafráze	k1gFnSc6
Marxe	Marx	k1gMnSc2
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
„	„	k?
<g/>
opiu	opium	k1gNnSc6
intelektuálů	intelektuál	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Raymonda	Raymond	k1gMnSc2
Arona	Aron	k1gMnSc2
<g/>
:	:	kIx,
„	„	k?
<g/>
Marxistická	marxistický	k2eAgFnSc1d1
eschatologie	eschatologie	k1gFnSc1
připisuje	připisovat	k5eAaImIp3nS
proletariátu	proletariát	k1gInSc3
úlohu	úloha	k1gFnSc4
kolektivního	kolektivní	k2eAgMnSc2d1
spasitele	spasitel	k1gMnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Komunismus	komunismus	k1gInSc1
byl	být	k5eAaImAgInS
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
prvním	první	k4xOgMnSc6
náboženstvím	náboženství	k1gNnSc7
intelektuálů	intelektuál	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
mělo	mít	k5eAaImAgNnS
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
naplnil	naplnit	k5eAaPmAgMnS
N.	N.	kA
Machiavelliho	Machiavelli	k1gMnSc2
touhu	touha	k1gFnSc4
po	po	k7c4
náboženství	náboženství	k1gNnSc4
organizující	organizující	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1
kritický	kritický	k2eAgInSc1d1
rozbor	rozbor	k1gInSc1
jeho	jeho	k3xOp3gFnSc2
nauky	nauka	k1gFnSc2
podává	podávat	k5eAaImIp3nS
také	také	k9
Leszek	Leszek	k1gInSc1
Kołakowski	Kołakowsk	k1gFnSc2
v	v	k7c6
práci	práce	k1gFnSc6
Hlavní	hlavní	k2eAgInSc4d1
směry	směr	k1gInPc4
marxismu	marxismus	k1gInSc2
nebo	nebo	k8xC
T.	T.	kA
G.	G.	kA
Masaryk	Masaryk	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
Otázka	otázka	k1gFnSc1
sociální	sociální	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Marxismus	marxismus	k1gInSc1
je	být	k5eAaImIp3nS
dle	dle	k7c2
Leszka	Leszka	k1gFnSc1
Kolakowskeho	Kolakowskeha	k1gFnSc5
šiřitelem	šiřitel	k1gMnSc7
slepé	slepý	k2eAgFnSc2d1
důvěry	důvěra	k1gFnSc2
ke	k	k7c3
vznešenému	vznešený	k2eAgInSc3d1
světu	svět	k1gInSc3
všeobecného	všeobecný	k2eAgNnSc2d1
uspokojení	uspokojení	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
čeká	čekat	k5eAaImIp3nS
na	na	k7c4
lidstvo	lidstvo	k1gNnSc4
hned	hned	k6eAd1
za	za	k7c7
rohem	roh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všechna	všechen	k3xTgNnPc4
proroctví	proroctví	k1gNnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
Marxova	Marxův	k2eAgFnSc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
pozdějších	pozdní	k2eAgMnPc2d2
marxistů	marxista	k1gMnPc2
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
být	být	k5eAaImF
proroctví	proroctví	k1gNnSc4
falešnými	falešný	k2eAgMnPc7d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
nikterak	nikterak	k6eAd1
nenarušuje	narušovat	k5eNaImIp3nS
stav	stav	k1gInSc1
duchovní	duchovní	k2eAgFnSc2d1
sebejistoty	sebejistota	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
žijí	žít	k5eAaImIp3nP
vyznavači	vyznavač	k1gMnPc7
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
případech	případ	k1gInPc6
všech	všecek	k3xTgNnPc2
očekávání	očekávání	k1gNnPc2
známých	známá	k1gFnPc2
z	z	k7c2
chiliastických	chiliastický	k2eAgNnPc2d1
náboženských	náboženský	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
Kolakowskeho	Kolakowske	k1gMnSc2
Marx	Marx	k1gMnSc1
také	také	k9
nepředvídal	předvídat	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
zestátnění	zestátnění	k1gNnSc6
výrobních	výrobní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
lze	lze	k6eAd1
vykořisťování	vykořisťování	k1gNnSc3
a	a	k8xC
ekonomickou	ekonomický	k2eAgFnSc4d1
nadvládu	nadvláda	k1gFnSc4
nad	nad	k7c7
pracujícími	pracující	k1gMnPc7
znásobit	znásobit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolakowsi	Kolakows	k1gMnPc7
také	také	k6eAd1
upozorňoval	upozorňovat	k5eAaImAgMnS
na	na	k7c4
nesoudržnost	nesoudržnost	k1gFnSc4
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
plný	plný	k2eAgInSc4d1
dvojsmyslů	dvojsmysl	k1gInPc2
a	a	k8xC
neúplných	úplný	k2eNgFnPc2d1
formulací	formulace	k1gFnPc2
<g/>
,	,	kIx,
či	či	k8xC
nevysvětlitelných	vysvětlitelný	k2eNgFnPc2d1
narážek	narážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
něj	on	k3xPp3gMnSc2
proto	proto	k8xC
ti	ten	k3xDgMnPc1
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
kritizovali	kritizovat	k5eAaImAgMnP
V.	V.	kA
I.	I.	kA
Lenina	Lenin	k2eAgFnSc1d1
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
zpronevěřuje	zpronevěřovat	k5eAaImIp3nS
Marxovým	Marxův	k2eAgFnPc3d1
důležitým	důležitý	k2eAgFnPc3d1
myšlenkám	myšlenka	k1gFnPc3
(	(	kIx(
<g/>
například	například	k6eAd1
dle	dle	k7c2
nich	on	k3xPp3gMnPc6
Marxova	Marxův	k2eAgFnSc1d1
diktatura	diktatura	k1gFnSc1
neznamená	znamenat	k5eNaImIp3nS
despotickou	despotický	k2eAgFnSc4d1
a	a	k8xC
nezákonnou	zákonný	k2eNgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neodhalovali	odhalovat	k5eNaImAgMnP
Leninovo	Leninův	k2eAgNnSc4d1
zpronevěření	zpronevěření	k1gNnSc4
se	se	k3xPyFc4
Marxovi	Marx	k1gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
nedůslednost	nedůslednost	k1gFnSc1
Marxe	Marx	k1gMnSc2
samotného	samotný	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Německo-americký	německo-americký	k2eAgMnSc1d1
psycholog	psycholog	k1gMnSc1
Erich	Erich	k1gMnSc1
Fromm	Fromm	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
knihy	kniha	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
interpretací	interpretace	k1gFnSc7
Marxe	Marx	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Obrazu	obraz	k1gInSc6
člověka	člověk	k1gMnSc2
u	u	k7c2
Marxe	Marx	k1gMnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
sekci	sekce	k1gFnSc4
Literatura	literatura	k1gFnSc1
<g/>
)	)	kIx)
jde	jít	k5eAaImIp3nS
hlavně	hlavně	k9
o	o	k7c4
sociálně	sociálně	k6eAd1
humanistickou	humanistický	k2eAgFnSc4d1
interpretaci	interpretace	k1gFnSc4
postavenou	postavený	k2eAgFnSc4d1
na	na	k7c6
Ekonomicko-filozofických	ekonomicko-filozofický	k2eAgInPc6d1
rukopisech	rukopis	k1gInPc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
podstatná	podstatný	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
obsahem	obsah	k1gInSc7
tohoto	tento	k3xDgNnSc2
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
titulu	titul	k1gInSc6
Beyond	Beyond	k1gInSc1
the	the	k?
Chains	Chains	k1gInSc1
of	of	k?
Illusion	Illusion	k1gInSc1
se	se	k3xPyFc4
Fromm	Fromm	k1gMnSc1
vyrovnává	vyrovnávat	k5eAaImIp3nS
se	s	k7c7
svým	svůj	k3xOyFgInSc7
vztahem	vztah	k1gInSc7
k	k	k7c3
Freudovu	Freudův	k2eAgNnSc3d1
a	a	k8xC
Marxovu	Marxův	k2eAgNnSc3d1
učení	učení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fromm	Fromm	k1gMnSc1
jako	jako	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
mála	málo	k1gNnSc2
kritiků	kritik	k1gMnPc2
poukázal	poukázat	k5eAaPmAgMnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Marx	Marx	k1gMnSc1
vždy	vždy	k6eAd1
hledal	hledat	k5eAaImAgMnS
„	„	k?
<g/>
dynamiku	dynamika	k1gFnSc4
<g/>
“	“	k?
společenských	společenský	k2eAgInPc2d1
dějů	děj	k1gInPc2
<g/>
,	,	kIx,
tzn.	tzn.	kA
jejich	jejich	k3xOp3gInPc4
hybné	hybný	k2eAgInPc4d1
síly	síl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
odpovídá	odpovídat	k5eAaImIp3nS
na	na	k7c4
námitky	námitka	k1gFnPc4
proti	proti	k7c3
možnému	možný	k2eAgInSc3d1
fatalismu	fatalismus	k1gInSc3
a	a	k8xC
neměnnosti	neměnnost	k1gFnSc3
jím	on	k3xPp3gMnSc7
objevených	objevený	k2eAgNnPc2d1
a	a	k8xC
zkoumaných	zkoumaný	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
pro	pro	k7c4
společenské	společenský	k2eAgInPc4d1
pohyby	pohyb	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Dle	dle	k7c2
kritiky	kritika	k1gFnSc2
Nikolaje	Nikolaj	k1gMnSc2
Berďajeva	Berďajev	k1gMnSc2
v	v	k7c6
marximsu	marxims	k1gInSc6
kolektiv	kolektiv	k1gInSc1
<g/>
,	,	kIx,
kterému	který	k3yIgInSc3,k3yQgInSc3,k3yRgInSc3
je	být	k5eAaImIp3nS
projevována	projevován	k2eAgFnSc1d1
úcta	úcta	k1gFnSc1
<g/>
,	,	kIx,
zaujímá	zaujímat	k5eAaImIp3nS
místo	místo	k1gNnSc1
určené	určený	k2eAgNnSc1d1
bohu	bůh	k1gMnSc6
a	a	k8xC
člověku	člověk	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proletariát	proletariát	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
Marxe	Marx	k1gMnPc4
novým	nový	k2eAgInSc7d1
Izraelem	Izrael	k1gInSc7
<g/>
,	,	kIx,
spasitelem	spasitel	k1gMnSc7
a	a	k8xC
budovatelem	budovatel	k1gMnSc7
nového	nový	k2eAgNnSc2d1
království	království	k1gNnSc2
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marxův	Marxův	k2eAgInSc1d1
proletářský	proletářský	k2eAgInSc1d1
komunismus	komunismus	k1gInSc1
je	být	k5eAaImIp3nS
odchylkou	odchylka	k1gFnSc7
od	od	k7c2
starohebrejského	starohebrejský	k2eAgInSc2d1
chiliasmu	chiliasmus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyvolený	vyvolený	k2eAgInSc1d1
národ	národ	k1gInSc1
se	se	k3xPyFc4
proměnil	proměnit	k5eAaPmAgInS
ve	v	k7c4
vyvolenou	vyvolený	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Maria	Mario	k1gMnSc2
Bocheński	Bocheński	k1gNnSc2
byl	být	k5eAaImAgMnS
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
marxismus	marxismus	k1gInSc1
není	být	k5eNaImIp3nS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
žádnou	žádný	k3yNgFnSc7
vědeckou	vědecký	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
to	ten	k3xDgNnSc1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neopírá	opírat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
zkušenosti	zkušenost	k1gFnPc4
a	a	k8xC
fakta	faktum	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c6
arbitrárně	arbitrárně	k6eAd1
a	a	k8xC
dopředu	dopředu	k6eAd1
určená	určený	k2eAgNnPc1d1
dogmata	dogma	k1gNnPc1
<g/>
;	;	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
logicky	logicky	k6eAd1
uspořádaným	uspořádaný	k2eAgMnSc7d1
a	a	k8xC
vnitřně	vnitřně	k6eAd1
soudržným	soudržný	k2eAgInSc7d1
systémem	systém	k1gInSc7
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
vyznačuje	vyznačovat	k5eAaImIp3nS
se	se	k3xPyFc4
řadou	řada	k1gFnSc7
rozporů	rozpor	k1gInPc2
a	a	k8xC
jakýmsi	jakýsi	k3yIgMnSc7
svérázným	svérázný	k2eAgInSc7d1
intelektuálním	intelektuální	k2eAgInSc7d1
zmatkem	zmatek	k1gInSc7
či	či	k8xC
nepořádkem	nepořádek	k1gInSc7
<g/>
;	;	kIx,
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítá	odmítat	k5eAaImIp3nS
veškerou	veškerý	k3xTgFnSc4
kritiku	kritika	k1gFnSc4
<g/>
,	,	kIx,
odmítá	odmítat	k5eAaImIp3nS
podrobit	podrobit	k5eAaPmF
se	se	k3xPyFc4
empirické	empirický	k2eAgFnSc3d1
verifikaci	verifikace	k1gFnSc3
a	a	k8xC
logickému	logický	k2eAgNnSc3d1
posouzení	posouzení	k1gNnSc3
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
sám	sám	k3xTgMnSc1
sobě	se	k3xPyFc3
přiznává	přiznávat	k5eAaImIp3nS
hodnoty	hodnota	k1gFnPc4
pravdivosti	pravdivost	k1gFnSc2
<g/>
,	,	kIx,
věčnosti	věčnost	k1gFnSc2
a	a	k8xC
neměnnosti	neměnnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bocheński	Bocheński	k1gNnSc1
si	se	k3xPyFc3
povšiml	povšimnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
komunistická	komunistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
se	se	k3xPyFc4
opírá	opírat	k5eAaImIp3nS
o	o	k7c4
neomylné	omylný	k2eNgFnPc4d1
‚	‚	k?
<g/>
klasiky	klasika	k1gFnSc2
<g/>
‘	‘	k?
<g/>
,	,	kIx,
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
povětšinou	povětšina	k1gFnSc7
z	z	k7c2
komentářů	komentář	k1gInPc2
k	k	k7c3
nim	on	k3xPp3gFnPc3
<g/>
,	,	kIx,
je	on	k3xPp3gFnPc4
krajně	krajně	k6eAd1
dogmatická	dogmatický	k2eAgFnSc1d1
a	a	k8xC
ohání	ohánět	k5eAaImIp3nS
se	se	k3xPyFc4
pravověrností	pravověrnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
něj	on	k3xPp3gInSc2
je	být	k5eAaImIp3nS
komunismus	komunismus	k1gInSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
teorii	teorie	k1gFnSc6
a	a	k8xC
praxi	praxe	k1gFnSc6
přímo	přímo	k6eAd1
neuvěřitelným	uvěřitelný	k2eNgNnSc7d1
zjednodušením	zjednodušení	k1gNnSc7
všeho	všecek	k3xTgNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Dle	dle	k7c2
Giovanniho	Giovanni	k1gMnSc2
Sartoriho	Sartori	k1gMnSc2
byla	být	k5eAaImAgFnS
Marxova	Marxův	k2eAgFnSc1d1
spasitelská	spasitelský	k2eAgFnSc1d1
mise	mise	k1gFnSc1
libertariánská	libertariánský	k2eAgFnSc1d1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
rovnostářská	rovnostářský	k2eAgFnSc1d1
<g/>
,	,	kIx,
Marx	Marx	k1gMnSc1
předložil	předložit	k5eAaPmAgMnS
kontra-ideál	kontra-ideál	k1gInSc4
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
rovnostářství	rovnostářství	k1gNnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
ideálem	ideál	k1gInSc7
libertarianismu	libertarianismus	k1gInSc2
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
krajní	krajní	k2eAgFnSc6d1
a	a	k8xC
millenaristické	millenaristický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pomník	pomník	k1gInSc1
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Dle	dle	k7c2
Alfreda	Alfred	k1gMnSc2
G.	G.	kA
Meyera	Meyer	k1gMnSc2
se	se	k3xPyFc4
mladý	mladý	k2eAgMnSc1d1
Marx	Marx	k1gMnSc1
otevřeně	otevřeně	k6eAd1
přikláněl	přiklánět	k5eAaImAgMnS
k	k	k7c3
anarchismu	anarchismus	k1gInSc3
a	a	k8xC
naprosto	naprosto	k6eAd1
odmítal	odmítat	k5eAaImAgInS
stát	stát	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
považoval	považovat	k5eAaImAgInS
za	za	k7c4
nereformovatelnou	reformovatelný	k2eNgFnSc4d1
represivní	represivní	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
jsoucí	jsoucí	k2eAgFnSc4d1
pouhým	pouhý	k2eAgInSc7d1
prostředkem	prostředek	k1gInSc7
třídního	třídní	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
dle	dle	k7c2
Meyera	Meyero	k1gNnSc2
překvapivě	překvapivě	k6eAd1
převzal	převzít	k5eAaPmAgInS
některé	některý	k3yIgInPc4
své	svůj	k3xOyFgInPc4
názory	názor	k1gInPc4
od	od	k7c2
krajně	krajně	k6eAd1
konzervativních	konzervativní	k2eAgMnPc2d1
tradicionalistů	tradicionalista	k1gMnPc2
Louise	Louis	k1gMnSc2
Gabriela	Gabriel	k1gMnSc2
Ambroisea	Ambroiseus	k1gMnSc2
de	de	k?
Bonalda	Bonald	k1gMnSc2
a	a	k8xC
Josepha	Joseph	k1gMnSc2
de	de	k?
Maistreho	Maistre	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
zuřivě	zuřivě	k6eAd1
kritizoval	kritizovat	k5eAaImAgMnS
liberalismus	liberalismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
revolučního	revoluční	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
vyvozoval	vyvozovat	k5eAaImAgMnS
z	z	k7c2
naprostého	naprostý	k2eAgNnSc2d1
odmítnutí	odmítnutí	k1gNnSc2
liberalismu	liberalismus	k1gInSc2
<g/>
,	,	kIx,
obdobně	obdobně	k6eAd1
jako	jako	k9
tito	tento	k3xDgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
však	však	k9
z	z	k7c2
takového	takový	k3xDgNnSc2
odmítnutí	odmítnutí	k1gNnSc2
vyvodili	vyvodit	k5eAaBmAgMnP,k5eAaPmAgMnP
program	program	k1gInSc4
konzervativní	konzervativní	k2eAgInSc4d1
a	a	k8xC
tradicionalistický	tradicionalistický	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
byl	být	k5eAaImAgMnS
dle	dle	k7c2
Meyera	Meyero	k1gNnSc2
silně	silně	k6eAd1
ovlivněn	ovlivnit	k5eAaPmNgInS
Henri	Henr	k1gFnPc1
de	de	k?
Saint-Simonem	Saint-Simon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
podlehl	podlehnout	k5eAaPmAgMnS
pokušení	pokušení	k1gNnSc4
utopie	utopie	k1gFnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nekriticky	kriticky	k6eNd1
věřil	věřit	k5eAaImAgMnS
v	v	k7c4
pokrok	pokrok	k1gInSc4
a	a	k8xC
především	především	k6eAd1
představil	představit	k5eAaPmAgInS
vizi	vize	k1gFnSc4
komunistické	komunistický	k2eAgFnSc2d1
Arkádie	Arkádie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
čeká	čekat	k5eAaImIp3nS
lidstvo	lidstvo	k1gNnSc4
po	po	k7c6
porážce	porážka	k1gFnSc6
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marxismus	marxismus	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
ohledu	ohled	k1gInSc6
amalgamem	amalgam	k1gInSc7
spojujícím	spojující	k2eAgInSc7d1
utopismus	utopismus	k1gInSc4
s	s	k7c7
prométheismem	prométheismus	k1gInSc7
v	v	k7c4
jeden	jeden	k4xCgInSc4
celek	celek	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Marxistickou	marxistický	k2eAgFnSc4d1
filozofii	filozofie	k1gFnSc4
dějin	dějiny	k1gFnPc2
Meyer	Meyer	k1gMnSc1
kritizoval	kritizovat	k5eAaImAgMnS
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
máme	mít	k5eAaImIp1nP
paradox	paradox	k1gInSc4
<g/>
:	:	kIx,
člověk	člověk	k1gMnSc1
tvoří	tvořit	k5eAaImIp3nS
dějiny	dějiny	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
jejich	jejich	k3xOp3gMnSc7
pánem	pán	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiné	k1gNnPc7
slovy	slovo	k1gNnPc7
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
dějiny	dějiny	k1gFnPc1
jsou	být	k5eAaImIp3nP
výsledkem	výsledek	k1gInSc7
lidské	lidský	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
a	a	k8xC
současně	současně	k6eAd1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
výtvorem	výtvor	k1gInSc7
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
Meyera	Meyero	k1gNnSc2
je	být	k5eAaImIp3nS
marxismus	marxismus	k1gInSc1
mnohovrstvený	mnohovrstvený	k2eAgInSc1d1
a	a	k8xC
zároveň	zároveň	k6eAd1
vnitřně	vnitřně	k6eAd1
značně	značně	k6eAd1
nesoudržný	soudržný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
také	také	k9
není	být	k5eNaImIp3nS
leninismus	leninismus	k1gInSc1
do	do	k7c2
očí	oko	k1gNnPc2
bijícím	bijící	k2eAgNnSc7d1
popřením	popření	k1gNnSc7
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
si	se	k3xPyFc3
myslí	myslet	k5eAaImIp3nS
rozhodující	rozhodující	k2eAgFnSc1d1
většina	většina	k1gFnSc1
badatelů	badatel	k1gMnPc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jednou	jeden	k4xCgFnSc7
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
obměn	obměna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meyer	Meyer	k1gInSc1
vždy	vždy	k6eAd1
zdůrazňoval	zdůrazňovat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
marxismus	marxismus	k1gInSc1
<g/>
,	,	kIx,
bezmezně	bezmezně	k6eAd1
přesvědčený	přesvědčený	k2eAgInSc1d1
o	o	k7c6
svých	svůj	k3xOyFgFnPc6
morálních	morální	k2eAgFnPc6d1
pravdách	pravda	k1gFnPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
příkladem	příklad	k1gInSc7
takového	takový	k3xDgInSc2
typu	typ	k1gInSc2
světského	světský	k2eAgInSc2d1
humanismu	humanismus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
ospravedlňuje	ospravedlňovat	k5eAaImIp3nS
násilí	násilí	k1gNnSc4
a	a	k8xC
dopouštění	dopouštění	k1gNnSc4
se	se	k3xPyFc4
nelidských	lidský	k2eNgFnPc2d1
či	či	k8xC
přímo	přímo	k6eAd1
bestiálních	bestiální	k2eAgInPc2d1
činů	čin	k1gInPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
ve	v	k7c6
jménu	jméno	k1gNnSc6
vyšší	vysoký	k2eAgFnSc2d2
pravdy	pravda	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
otevírá	otevírat	k5eAaImIp3nS
dveře	dveře	k1gFnPc4
dokořán	dokořán	k6eAd1
totalitarismu	totalitarismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pomníky	pomník	k1gInPc1
</s>
<s>
Veřejný	veřejný	k2eAgInSc1d1
park	park	k1gInSc1
Marx-Engel-Forum	Marx-Engel-Forum	k1gInSc1
v	v	k7c6
Berlíně	Berlín	k1gInSc6
v	v	k7c6
Německu	Německo	k1gNnSc6
</s>
<s>
Zejména	zejména	k9
po	po	k7c6
ustavení	ustavení	k1gNnSc6
komunistických	komunistický	k2eAgFnPc2d1
vlád	vláda	k1gFnPc2
v	v	k7c6
některých	některý	k3yIgFnPc6
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
a	a	k8xC
v	v	k7c6
menší	malý	k2eAgFnSc6d2
míře	míra	k1gFnSc6
i	i	k8xC
v	v	k7c6
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
byly	být	k5eAaImAgFnP
Marxovi	Marx	k1gMnSc3
budovány	budován	k2eAgMnPc4d1
pomníky	pomník	k1gInPc4
a	a	k8xC
vzdávány	vzdáván	k2eAgFnPc4d1
nejrůznější	různý	k2eAgFnPc4d3
pocty	pocta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
po	po	k7c6
něm	on	k3xPp3gInSc6
pojmenovány	pojmenovat	k5eAaPmNgFnP
ulice	ulice	k1gFnPc1
<g/>
,	,	kIx,
prostranství	prostranství	k1gNnSc1
<g/>
,	,	kIx,
školy	škola	k1gFnPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
projevilo	projevit	k5eAaPmAgNnS
v	v	k7c6
NDR	NDR	kA
<g/>
:	:	kIx,
jedno	jeden	k4xCgNnSc1
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
Chemnitz	Chemnitz	k1gInSc1
<g/>
,	,	kIx,
neslo	nést	k5eAaImAgNnS
v	v	k7c6
letech	let	k1gInPc6
1953	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
název	název	k1gInSc1
Karl-Marx-Stadt	Karl-Marx-Stadt	k1gInSc1
a	a	k8xC
byl	být	k5eAaImAgInS
zde	zde	k6eAd1
vybudován	vybudovat	k5eAaPmNgInS
mohutný	mohutný	k2eAgInSc1d1
Karl-Marx-Monument	Karl-Marx-Monument	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
nesla	nést	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
také	také	k9
proslulá	proslulý	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
nedalekém	daleký	k2eNgNnSc6d1
Lipsku	Lipsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Východní	východní	k2eAgFnSc1d1
částí	část	k1gFnSc7
Berlína	Berlín	k1gInSc2
dosud	dosud	k6eAd1
prochází	procházet	k5eAaImIp3nS
Karl-Marx-Allee	Karl-Marx-Allee	k1gFnSc1
<g/>
,	,	kIx,
nedaleko	daleko	k6eNd1
stojí	stát	k5eAaImIp3nS
pomník	pomník	k1gInSc4
Marxe	Marx	k1gMnSc2
a	a	k8xC
Engelse	Engels	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ruském	ruský	k2eAgNnSc6d1
Povolží	Povolží	k1gNnSc6
pak	pak	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
stojí	stát	k5eAaImIp3nS
město	město	k1gNnSc4
Marx	Marx	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
ASSR	ASSR	kA
Povolžských	povolžský	k2eAgMnPc2d1
Němců	Němec	k1gMnPc2
(	(	kIx(
<g/>
nedaleko	daleko	k6eNd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
též	též	k9
město	město	k1gNnSc1
Engels	Engels	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
byla	být	k5eAaImAgFnS
ku	k	k7c3
příležitosti	příležitost	k1gFnSc3
výročí	výročí	k1gNnSc2
200	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
narození	narození	k1gNnSc2
Marxovi	Marx	k1gMnSc3
odhalena	odhalit	k5eAaPmNgFnS
socha	socha	k1gFnSc1
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
rodném	rodný	k2eAgNnSc6d1
městě	město	k1gNnSc6
Trevír	Trevír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostního	slavnostní	k2eAgNnSc2d1
odhalení	odhalení	k1gNnSc2
tohoto	tento	k3xDgInSc2
pomníku	pomník	k1gInSc2
v	v	k7c6
nadživotní	nadživotní	k2eAgFnSc6d1
velikosti	velikost	k1gFnSc6
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
také	také	k9
Jean-Claude	Jean-Claud	k1gInSc5
Juncker	Juncker	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
Evropské	evropský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
něj	on	k3xPp3gMnSc4
Marx	Marx	k1gMnSc1
nemůže	moct	k5eNaImIp3nS
za	za	k7c4
zločiny	zločin	k1gInPc4
komunismu	komunismus	k1gInSc2
a	a	k8xC
měl	mít	k5eAaImAgInS
by	by	kYmCp3nS
být	být	k5eAaImF
chápan	chápan	k1gMnSc1
„	„	k?
<g/>
v	v	k7c6
kontextu	kontext	k1gInSc6
jeho	jeho	k3xOp3gFnSc2
doby	doba	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Mehring	Mehring	k1gInSc1
<g/>
,	,	kIx,
Franz	Franz	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Story	story	k1gFnSc2
of	of	k?
His	his	k1gNnSc1
Life	Life	k1gFnSc1
(	(	kIx(
<g/>
Routledge	Routledge	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
)	)	kIx)
pg	pg	k?
<g/>
.	.	kIx.
75	#num#	k4
<g/>
↑	↑	k?
John	John	k1gMnSc1
Bellamy	Bellam	k1gInPc4
Foster	Foster	k1gInSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Marx	Marx	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Theory	Theor	k1gInPc7
of	of	k?
Metabolic	Metabolice	k1gFnPc2
Rift	Rift	k1gMnSc1
<g/>
:	:	kIx,
Classical	Classical	k1gMnPc1
Foundations	Foundationsa	k1gFnPc2
for	forum	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
Environmental	Environmental	k1gMnSc1
Sociology	sociolog	k1gMnPc7
<g/>
"	"	kIx"
<g/>
,	,	kIx,
American	American	k1gMnSc1
Journal	Journal	k1gFnSc2
of	of	k?
Sociology	sociolog	k1gMnPc7
<g/>
,	,	kIx,
Vol	vol	k6eAd1
<g/>
.	.	kIx.
105	#num#	k4
<g/>
,	,	kIx,
No	no	k9
<g/>
.	.	kIx.
2	#num#	k4
(	(	kIx(
<g/>
September	September	k1gInSc1
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
366	#num#	k4
<g/>
–	–	k?
<g/>
405	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Postoloprty	Postoloprta	k1gFnSc2
<g/>
,	,	kIx,
starý	starý	k2eAgInSc4d1
židovský	židovský	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
v	v	k7c4
databázi	databáze	k1gFnSc4
Poškozené	poškozený	k2eAgFnPc1d1
a	a	k8xC
zničené	zničený	k2eAgInPc1d1
kostely	kostel	k1gInPc1
<g/>
,	,	kIx,
kaple	kaple	k1gFnPc1
a	a	k8xC
synagogy	synagoga	k1gFnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
↑	↑	k?
F.	F.	kA
Wheen	Wheen	k1gInSc4
<g/>
:	:	kIx,
Marxův	Marxův	k2eAgInSc4d1
Kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
a	a	k8xC
18	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
K.	K.	kA
Marx	Marx	k1gMnSc1
<g/>
:	:	kIx,
Differenz	Differenz	k1gMnSc1
der	drát	k5eAaImRp2nS
demokritischen	demokritischen	k1gInSc1
und	und	k?
epikureischen	epikureischen	k1gInSc4
Naturphilosophie	Naturphilosophie	k1gFnSc2
nebst	bst	k5eNaPmF
einem	einem	k1gInSc4
Anhange	Anhang	k1gFnSc2
na	na	k7c4
zeno	zeno	k1gNnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jost	Jost	k2eAgInSc4d1
Hermand	Hermand	k1gInSc4
<g/>
:	:	kIx,
Heinrich	Heinrich	k1gMnSc1
Heine	Hein	k1gInSc5
<g/>
,	,	kIx,
Köln	Köln	k1gInSc1
–	–	k?
Weimar	Weimar	k1gInSc1
2007	#num#	k4
<g/>
,	,	kIx,
kap	kap	k1gInSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Heine	Hein	k1gInSc5
und	und	k?
Marx	Marx	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
s	s	k7c7
85	#num#	k4
<g/>
n.	n.	k?
<g/>
↑	↑	k?
F.	F.	kA
Wheen	Wheen	k1gInSc4
<g/>
:	:	kIx,
Marxův	Marxův	k2eAgInSc4d1
Kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
28	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
G.	G.	kA
North	North	k1gMnSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
jak	jak	k8xS,k8xC
ho	on	k3xPp3gMnSc4
málokdo	málokdo	k3yInSc1
zná	znát	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
↑	↑	k?
F.	F.	kA
Wheen	Wheen	k1gInSc4
<g/>
:	:	kIx,
Marxův	Marxův	k2eAgInSc4d1
Kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
—	—	k?
<g/>
34	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
G.	G.	kA
North	North	k1gMnSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
jak	jak	k8xS,k8xC
ho	on	k3xPp3gMnSc4
málokdo	málokdo	k3yInSc1
zná	znát	k5eAaImIp3nS
<g/>
,	,	kIx,
s.	s.	k?
7	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
“	“	k?
<g/>
↑	↑	k?
G.	G.	kA
North	North	k1gMnSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
jak	jak	k8xC,k8xS
ho	on	k3xPp3gMnSc4
málokdo	málokdo	k3yInSc1
zná	znát	k5eAaImIp3nS
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
“	“	k?
<g/>
↑	↑	k?
G.	G.	kA
North	North	k1gMnSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
jak	jak	k8xC,k8xS
ho	on	k3xPp3gMnSc4
málokdo	málokdo	k3yInSc1
zná	znát	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
,	,	kIx,
s.	s.	k?
9	#num#	k4
<g/>
↑	↑	k?
F.	F.	kA
Wheen	Wheen	k1gInSc4
<g/>
:	:	kIx,
Marxův	Marxův	k2eAgInSc4d1
Kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
36	#num#	k4
<g/>
–	–	k?
<g/>
37	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
F.	F.	kA
Wheen	Wheen	k1gInSc4
<g/>
:	:	kIx,
Marxův	Marxův	k2eAgInSc4d1
Kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
54.1	54.1	k4
2	#num#	k4
F.	F.	kA
Engels	Engels	k1gMnSc1
<g/>
:	:	kIx,
Řeč	řeč	k1gFnSc1
nad	nad	k7c7
hrobem	hrob	k1gInSc7
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
<g/>
↑	↑	k?
G.	G.	kA
North	North	k1gMnSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
jak	jak	k8xC,k8xS
ho	on	k3xPp3gMnSc4
málokdo	málokdo	k3yInSc1
zná	znát	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
,	,	kIx,
s.	s.	k?
9	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
<g/>
“	“	k?
<g/>
↑	↑	k?
G.	G.	kA
North	North	k1gMnSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
jak	jak	k8xS,k8xC
ho	on	k3xPp3gMnSc4
málokdo	málokdo	k3yInSc1
zná	znát	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
↑	↑	k?
F.	F.	kA
Engels	Engels	k1gMnSc1
<g/>
:	:	kIx,
Über	Über	k1gMnSc1
historischen	historischen	k2eAgMnSc1d1
Materialismus	materialismus	k1gInSc4
na	na	k7c4
zeno	zeno	k1gNnSc4
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
H.	H.	kA
J.	J.	kA
Störig	Störiga	k1gFnPc2
<g/>
:	:	kIx,
Malé	Malé	k2eAgFnPc1d1
dějiny	dějiny	k1gFnPc1
filosofie	filosofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelní	kostelní	k2eAgFnSc1d1
Vydří	vydří	k2eAgFnSc1d1
2000	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
373	#num#	k4
<g/>
—	—	k?
<g/>
374	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
G.	G.	kA
North	North	k1gMnSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
jak	jak	k8xS,k8xC
ho	on	k3xPp3gMnSc4
málokdo	málokdo	k3yInSc1
zná	znát	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
a	a	k8xC
16	#num#	k4
<g/>
↑	↑	k?
Stanford	Stanford	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosoph	k1gInPc1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
<g/>
↑	↑	k?
Š.	Š.	kA
Avineri	Aviner	k1gFnSc2
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
and	and	k?
Jewish	Jewish	k1gInSc1
Emancipation	Emancipation	k1gInSc1
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
History	Histor	k1gInPc1
of	of	k?
Ideas	Ideas	k1gInSc1
25	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
1964	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
445	#num#	k4
<g/>
—	—	k?
<g/>
50	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marx-Engels-Werke	Marx-Engels-Werk	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
I	I	kA
<g/>
,	,	kIx,
378	#num#	k4
<g/>
—	—	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marx-Engels-Werke	Marx-Engels-Werke	k1gNnSc1
III	III	kA
<g/>
,	,	kIx,
46	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Manifest	manifest	k1gInSc4
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
↑	↑	k?
Marx-Engels	Marx-Engels	k1gInSc1
Werke	Werke	k1gInSc1
VIII	VIII	kA
<g/>
,	,	kIx,
115	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
M.	M.	kA
Bloch	Bloch	k1gMnSc1
<g/>
:	:	kIx,
Marxism	Marxism	k1gMnSc1
and	and	k?
Anthropology	Anthropolog	k1gMnPc7
<g/>
:	:	kIx,
The	The	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
a	a	k8xC
Relationship	Relationship	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rouledge	Rouledg	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
32	#num#	k4
<g/>
—	—	k?
<g/>
33	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
K.	K.	kA
Marx	Marx	k1gMnSc1
<g/>
:	:	kIx,
Kapitál	kapitál	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
1	#num#	k4
<g/>
,	,	kIx,
kap	kap	k1gInSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
F.	F.	kA
Wheen	Wheen	k1gInSc4
<g/>
:	:	kIx,
Marxův	Marxův	k2eAgInSc4d1
Kapitál	kapitál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2007	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
100	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
1833	#num#	k4
Factory	Factor	k1gInPc7
Act	Act	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nationalarchives	Nationalarchives	k1gInSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Marx-Engels-Werke	Marx-Engels-Werke	k1gNnSc1
XIX	XIX	kA
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Blackwellova	Blackwellův	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
politického	politický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
455	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pravda	pravda	k9
vítězí	vítězit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
nejlíp	dobře	k6eAd3
mečem	meč	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2014.1	2014.1	k4
2	#num#	k4
3	#num#	k4
Publikováno	publikovat	k5eAaBmNgNnS
v	v	k7c4
Le	Le	k1gFnSc4
Socialisme	socialismus	k1gInSc5
<g/>
,	,	kIx,
15	#num#	k4
March	March	k1gInSc1
1908	#num#	k4
<g/>
;	;	kIx,
Odbudowa	Odbudow	k2eAgNnPc1d1
Polski	Polski	k1gNnPc1
(	(	kIx(
<g/>
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
119	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
;	;	kIx,
Mysl	mysl	k1gFnSc1
Socjalistyczna	Socjalistyczna	k1gFnSc1
<g/>
,	,	kIx,
Květen	květen	k1gInSc1
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
sbírky	sbírka	k1gFnSc2
článků	článek	k1gInPc2
a	a	k8xC
projevů	projev	k1gInPc2
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
a	a	k8xC
Friedricha	Friedrich	k1gMnSc2
Engelse	Engels	k1gMnSc2
The	The	k1gMnSc1
Russian	Russian	k1gMnSc1
Menace	Menace	k1gFnSc2
to	ten	k3xDgNnSc4
Europe	Europ	k1gInSc5
<g/>
,	,	kIx,
George	Georg	k1gFnPc1
Allen	Allen	k1gMnSc1
and	and	k?
Unwin	Unwin	k1gMnSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
104	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MASARYK	Masaryk	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velicí	veliký	k2eAgMnPc1d1
mužové	muž	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
.	.	kIx.
200	#num#	k4
s.	s.	k?
cnb	cnb	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
907616	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Citovaný	citovaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
155	#num#	k4
<g/>
;	;	kIx,
poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
otištěn	otisknout	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
též	též	k9
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Marxův	Marxův	k2eAgInSc1d1
Kapitál	kapitál	k1gInSc1
slaví	slavit	k5eAaImIp3nS
v	v	k7c6
Německu	Německo	k1gNnSc6
zase	zase	k9
úspěch	úspěch	k1gInSc4
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Slavoj	Slavoj	k1gInSc1
Zizek	Zizky	k1gFnPc2
Biography	Biographa	k1gFnSc2
na	na	k7c4
notablebiographies	notablebiographies	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Secrets	Secrets	k1gInSc1
BehindThe	BehindThe	k1gFnPc2
Forbidden	Forbiddna	k1gFnPc2
Books	Books	k1gInSc1
America	Americ	k1gInSc2
Magazine	Magazin	k1gInSc5
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stanford	Stanford	k1gMnSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosoph	k1gInPc1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Karl	Karl	k1gMnSc1
Popper	Popper	k1gMnSc1
<g/>
↑	↑	k?
BANKOWICZ	BANKOWICZ	kA
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritikové	kritik	k1gMnPc1
marxismu	marxismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Barrister	Barrister	k1gMnSc1
&	&	k?
Principal	Principal	k1gMnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7485	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
104	#num#	k4
<g/>
,	,	kIx,
105	#num#	k4
<g/>
,	,	kIx,
107	#num#	k4
<g/>
,	,	kIx,
109	#num#	k4
a	a	k8xC
110	#num#	k4
↑	↑	k?
Marx	Marx	k1gMnSc1
et	et	k?
Aron	Aron	k1gInSc1
font	font	k1gInSc1
bon	bon	k1gInSc4
ménage	ménagat	k5eAaPmIp3nS
Recenze	recenze	k1gFnSc1
knihy	kniha	k1gFnSc2
Le	Le	k1gFnSc2
Marxisme	marxismus	k1gInSc5
de	de	k?
Marx	Marx	k1gMnSc1
na	na	k7c4
parutions	parutions	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
com	com	k?
<g/>
↑	↑	k?
Bankowicz	Bankowicz	k1gInSc1
<g/>
:	:	kIx,
Kritikové	kritik	k1gMnPc1
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
27	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
↑	↑	k?
J.	J.	kA
Srovnal	srovnat	k5eAaPmAgMnS
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
Otázka	otázka	k1gFnSc1
sociální	sociální	k2eAgFnSc1d1
<g/>
:	:	kIx,
dílo	dílo	k1gNnSc1
a	a	k8xC
dějiny	dějiny	k1gFnPc1
Archivováno	archivován	k2eAgNnSc4d1
27	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Bankowicz	Bankowicz	k1gInSc1
<g/>
:	:	kIx,
Kritikové	kritik	k1gMnPc1
marxismu	marxismus	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
s.	s.	k?
75	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
↑	↑	k?
Bankowicz	Bankowicz	k1gInSc1
<g/>
:	:	kIx,
Kritikové	kritik	k1gMnPc1
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
36	#num#	k4
a	a	k8xC
39	#num#	k4
<g/>
↑	↑	k?
Bankowicz	Bankowicz	k1gInSc1
<g/>
:	:	kIx,
Kritikové	kritik	k1gMnPc1
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
,	,	kIx,
47	#num#	k4
a	a	k8xC
49	#num#	k4
<g/>
↑	↑	k?
Bankowicz	Bankowicz	k1gInSc1
<g/>
:	:	kIx,
Kritikové	kritik	k1gMnPc1
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
127	#num#	k4
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Bankowicz	Bankowicz	k1gInSc1
<g/>
:	:	kIx,
Kritikové	kritik	k1gMnPc1
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
90	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
↑	↑	k?
Bankowicz	Bankowicz	k1gInSc1
<g/>
:	:	kIx,
Kritikové	kritik	k1gMnPc1
marxismu	marxismus	k1gInSc2
<g/>
,	,	kIx,
s.	s.	k?
96	#num#	k4
<g/>
,	,	kIx,
98	#num#	k4
a	a	k8xC
102	#num#	k4
<g/>
↑	↑	k?
Uni-leipzig	Uni-leipziga	k1gFnPc2
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
:	:	kIx,
Geschichte	Geschicht	k1gInSc5
der	drát	k5eAaImRp2nS
Universität	Universität	k1gInSc1
<g/>
.	.	kIx.
www.uni-leipzig.de	www.uni-leipzig.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Němci	Němec	k1gMnPc1
odhalili	odhalit	k5eAaPmAgMnP
čínskou	čínský	k2eAgFnSc4d1
sochu	socha	k1gFnSc4
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idol	idol	k1gInSc4
komunistů	komunista	k1gMnPc2
oslavil	oslavit	k5eAaPmAgMnS
i	i	k9
šéf	šéf	k1gMnSc1
EU	EU	kA
Juncker	Juncker	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BYZNYS	byznys	k1gInSc1
NOVINY	novina	k1gFnSc2
<g/>
,	,	kIx,
2018-05-05	2018-05-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jelikož	jelikož	k8xS
oficiální	oficiální	k2eAgFnSc7d1
ideologií	ideologie	k1gFnSc7
v	v	k7c6
době	doba	k1gFnSc6
komunistické	komunistický	k2eAgFnSc2d1
diktatury	diktatura	k1gFnSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
byl	být	k5eAaImAgInS
marxismus-leninismus	marxismus-leninismus	k1gInSc4
<g/>
,	,	kIx,
Marxova	Marxův	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
byla	být	k5eAaImAgFnS
vydávána	vydávat	k5eAaImNgFnS,k5eAaPmNgFnS
často	často	k6eAd1
a	a	k8xC
ve	v	k7c6
velkých	velký	k2eAgInPc6d1
nákladech	náklad	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrozsáhlejší	rozsáhlý	k2eAgFnSc1d3
souborné	souborný	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
o	o	k7c6
39	#num#	k4
svazcích	svazek	k1gInPc6
začalo	začít	k5eAaPmAgNnS
vycházet	vycházet	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1956	#num#	k4
v	v	k7c6
nakladatelství	nakladatelství	k1gNnSc6
Svoboda	Svoboda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
však	však	k9
nebylo	být	k5eNaImAgNnS
nově	nově	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
téměř	téměř	k6eAd1
žádné	žádný	k3yNgNnSc4
Marxovo	Marxův	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
</s>
<s>
Prací	práce	k1gFnSc7
o	o	k7c4
Marxovi	Marx	k1gMnSc3
vyšlo	vyjít	k5eAaPmAgNnS
sice	sice	k8xC
v	v	k7c6
češtině	čeština	k1gFnSc6
mnoho	mnoho	k6eAd1
<g/>
,	,	kIx,
naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
však	však	k9
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
rokem	rok	k1gInSc7
1989	#num#	k4
<g/>
;	;	kIx,
tyto	tento	k3xDgFnPc1
publikace	publikace	k1gFnPc1
zpravidla	zpravidla	k6eAd1
představují	představovat	k5eAaImIp3nP
dogmatický	dogmatický	k2eAgInSc4d1
marxisticko-leninský	marxisticko-leninský	k2eAgInSc4d1
pohled	pohled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
překlady	překlad	k1gInPc4
z	z	k7c2
ruštiny	ruština	k1gFnSc2
<g/>
;	;	kIx,
k	k	k7c3
dispozici	dispozice	k1gFnSc3
jsou	být	k5eAaImIp3nP
např.	např.	kA
Leninovy	Leninův	k2eAgInPc1d1
komentáře	komentář	k1gInPc1
nebo	nebo	k8xC
práce	práce	k1gFnPc1
pozdějších	pozdní	k2eAgMnPc2d2
sovětských	sovětský	k2eAgMnPc2d1
marxistů	marxista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
začínají	začínat	k5eAaImIp3nP
objevovat	objevovat	k5eAaImF
publikace	publikace	k1gFnSc2
nezávislé	závislý	k2eNgFnSc2d1
na	na	k7c4
někdejší	někdejší	k2eAgFnSc4d1
oficiální	oficiální	k2eAgFnSc4d1
ideologii	ideologie	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
G.	G.	kA
Duménil	Duménil	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
Lévy	Léva	k1gFnSc2
<g/>
:	:	kIx,
Marxistická	marxistický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeň	Všeň	k1gFnSc1
<g/>
:	:	kIx,
Grimmus	Grimmus	k1gInSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-87461-04-4	978-80-87461-04-4	k4
</s>
<s>
E.	E.	kA
Fromm	Fromm	k1gMnSc1
<g/>
:	:	kIx,
Obraz	obraz	k1gInSc1
člověka	člověk	k1gMnSc2
u	u	k7c2
Marxe	Marx	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
L.	L.	kA
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86263-53-3	80-86263-53-3	k4
(	(	kIx(
<g/>
objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
nový	nový	k2eAgInSc1d1
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
Ekonomicko-filosofických	ekonomicko-filosofický	k2eAgInPc2d1
rukopisů	rukopis	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
M.	M.	kA
Hauser	Hauser	k1gMnSc1
<g/>
:	:	kIx,
Prolegomena	prolegomena	k1gNnPc4
k	k	k7c3
filosofii	filosofie	k1gFnSc3
současnosti	současnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Filosofia	Filosofia	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-7007-270-7	978-80-7007-270-7	k4
</s>
<s>
D.	D.	kA
Pals	Pals	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Náboženství	náboženství	k1gNnSc1
jako	jako	k8xC,k8xS
odcizení	odcizení	k1gNnSc1
<g/>
:	:	kIx,
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
in	in	k?
Osm	osm	k4xCc4
teorií	teorie	k1gFnPc2
náboženství	náboženství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ExOriente	ExOrient	k1gMnSc5
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
;	;	kIx,
s.	s.	k?
161	#num#	k4
<g/>
–	–	k?
<g/>
199	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-905211-2-4	978-80-905211-2-4	k4
(	(	kIx(
<g/>
rozbor	rozbor	k1gInSc4
Marxova	Marxův	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
náboženství	náboženství	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
M.	M.	kA
Valach	valach	k1gInSc1
<g/>
:	:	kIx,
Marxova	Marxův	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
L.	L.	kA
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-86263-65-7	80-86263-65-7	k4
</s>
<s>
F.	F.	kA
Wheen	Wheen	k1gInSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
art	art	k?
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7257-930-4	80-7257-930-4	k4
</s>
<s>
F.	F.	kA
Wheen	Wheen	k2eAgInSc1d1
<g/>
:	:	kIx,
Marxův	Marxův	k2eAgInSc1d1
Kapitál	kapitál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
–	–	k?
Beta	beta	k1gNnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978-80-7306-315-3	978-80-7306-315-3	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
</s>
<s>
Autor	autor	k1gMnSc1
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Osoba	osoba	k1gFnSc1
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Výběr	výběr	k1gInSc1
spisů	spis	k1gInPc2
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Rozhlasový	rozhlasový	k2eAgInSc1d1
pořad	pořad	k1gInSc1
o	o	k7c6
Marxovi	Marx	k1gMnSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Ekonomické	ekonomický	k2eAgInPc1d1
spisy	spis	k1gInPc1
Marxe	Marx	k1gMnSc2
a	a	k8xC
Engelse	Engels	k1gMnSc2
Archivováno	archivován	k2eAgNnSc1d1
23	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Ekonomicko-filozofické	ekonomicko-filozofický	k2eAgInPc4d1
rukopisy	rukopis	k1gInPc4
z	z	k7c2
r.	r.	kA
1844	#num#	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Články	článek	k1gInPc1
a	a	k8xC
diskuse	diskuse	k1gFnPc1
o	o	k7c6
Marxovi	Marx	k1gMnSc6
na	na	k7c6
Britských	britský	k2eAgInPc6d1
listech	list	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Karel	Karel	k1gMnSc1
Marx	Marx	k1gMnSc1
–	–	k?
video	video	k1gNnSc4
z	z	k7c2
cyklu	cyklus	k1gInSc2
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Historický	historický	k2eAgInSc1d1
magazín	magazín	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
M.	M.	kA
N.	N.	kA
Rothbard	Rothbard	k1gMnSc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Marx	Marx	k1gMnSc1
<g/>
:	:	kIx,
Komunista	komunista	k1gMnSc1
jako	jako	k8xC,k8xS
náboženský	náboženský	k2eAgMnSc1d1
eschatologista	eschatologista	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Gary	Gary	k1gInPc4
North	North	k1gInSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
jak	jak	k8xS,k8xC
ho	on	k3xPp3gMnSc4
málokdo	málokdo	k3yInSc1
zná	znát	k5eAaImIp3nS
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Eugen	Eugen	k2eAgInSc1d1
von	von	k1gInSc1
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1
<g/>
:	:	kIx,
Marx	Marx	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1818	#num#	k4
<g/>
–	–	k?
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
ekonomická	ekonomický	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
K.	K.	kA
<g/>
Marxe	Marx	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Jednu	jeden	k4xCgFnSc4
revoluci	revoluce	k1gFnSc4
zažil	zažít	k5eAaPmAgMnS
a	a	k8xC
v	v	k7c4
další	další	k1gNnSc4
věřil	věřit	k5eAaImAgMnS
Pořad	pořad	k1gInSc4
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
Plus	plus	k1gInSc1
<g/>
,	,	kIx,
vliv	vliv	k1gInSc1
roku	rok	k1gInSc2
1848	#num#	k4
na	na	k7c4
způsob	způsob	k1gInSc4
uvažování	uvažování	k1gNnSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Marx-Engels-Werke	Marx-Engels-Werke	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Historický	historický	k2eAgInSc1d1
čtvrtletník	čtvrtletník	k1gInSc1
deníku	deník	k1gInSc2
Die	Die	k1gFnPc2
Zeit	Zeit	k1gInSc1
<g/>
:	:	kIx,
číslo	číslo	k1gNnSc1
3	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
věnované	věnovaný	k2eAgFnSc2d1
Marxovi	Marx	k1gMnSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Stanford	Stanford	k1gInSc1
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Philosophy	Philosoph	k1gInPc1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Revue	revue	k1gFnSc1
Actuel	Actuel	k1gMnSc1
Marx	Marx	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Makroekonomie	makroekonomie	k1gFnPc1
Hlavní	hlavní	k2eAgFnPc1d1
koncepty	koncept	k1gInPc4
</s>
<s>
Agregátní	agregátní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Agregátní	agregátní	k2eAgFnSc1d1
nabídka	nabídka	k1gFnSc1
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
•	•	k?
Deflace	deflace	k1gFnSc2
•	•	k?
Poptávkový	poptávkový	k2eAgInSc1d1
šok	šok	k1gInSc1
•	•	k?
Nabídkový	nabídkový	k2eAgInSc1d1
šok	šok	k1gInSc1
<g/>
•	•	k?
Dezinflace	Dezinflace	k1gFnSc2
•	•	k?
Efektivní	efektivní	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
•	•	k?
Očekávání	očekávání	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Adaptivní	adaptivní	k2eAgFnPc1d1
•	•	k?
Racionální	racionální	k2eAgFnPc1d1
<g/>
)	)	kIx)
•	•	k?
Finanční	finanční	k2eAgFnSc2d1
krize	krize	k1gFnSc2
•	•	k?
Hospodářský	hospodářský	k2eAgInSc1d1
růst	růst	k1gInSc1
•	•	k?
Inflace	inflace	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Tahem	tah	k1gInSc7
poptávky	poptávka	k1gFnSc2
•	•	k?
Nákladová	nákladový	k2eAgFnSc1d1
)	)	kIx)
•	•	k?
Úroková	úrokový	k2eAgFnSc1d1
sazba	sazba	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Investice	investice	k1gFnSc1
•	•	k?
Past	pasta	k1gFnPc2
na	na	k7c4
likviditu	likvidita	k1gFnSc4
•	•	k?
Národní	národní	k2eAgInSc1d1
důchod	důchod	k1gInSc1
<g/>
(	(	kIx(
<g/>
HDP	HDP	kA
•	•	k?
HNP	HNP	kA
•	•	k?
ČND	ČND	kA
<g/>
)	)	kIx)
•	•	k?
Microfoundations	Microfoundations	k1gInSc4
•	•	k?
Peníze	peníz	k1gInPc4
<g/>
(	(	kIx(
<g/>
Endogenní	endogenní	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Tvorba	tvorba	k1gFnSc1
peněz	peníze	k1gInPc2
•	•	k?
Poptávka	poptávka	k1gFnSc1
po	po	k7c6
penězích	peníze	k1gInPc6
•	•	k?
Preference	preference	k1gFnPc1
likvidity	likvidita	k1gFnSc2
•	•	k?
Peněžní	peněžní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
•	•	k?
Národní	národní	k2eAgInPc4d1
účty	účet	k1gInPc4
<g/>
(	(	kIx(
<g/>
Systém	systém	k1gInSc1
národních	národní	k2eAgInPc2d1
účtů	účet	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Nominal	Nominal	k1gMnSc1
rigidity	rigidita	k1gFnSc2
•	•	k?
Cenová	cenový	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
•	•	k?
Recese	recese	k1gFnSc1
•	•	k?
Shrinkflation	Shrinkflation	k1gInSc1
•	•	k?
Stagflace	Stagflace	k1gFnSc2
•	•	k?
Úspory	úspora	k1gFnSc2
•	•	k?
Nezaměstnanost	nezaměstnanost	k1gFnSc1
Politiky	politika	k1gFnSc2
</s>
<s>
Fiskální	fiskální	k2eAgFnSc1d1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
•	•	k?
Obchodní	obchodní	k2eAgFnSc1d1
•	•	k?
Centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Modely	model	k1gInPc1
</s>
<s>
IS-LM	IS-LM	k?
•	•	k?
AD	ad	k7c4
<g/>
–	–	k?
<g/>
AS	as	k1gNnSc4
•	•	k?
Keynesiánský	keynesiánský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
•	•	k?
Multiplikátor	multiplikátor	k1gInSc1
•	•	k?
Akcelerátorový	Akcelerátorový	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
Phillipsova	Phillipsův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Arrow	Arrow	k1gFnSc2
<g/>
–	–	k?
<g/>
Debreu	Debreus	k1gInSc2
•	•	k?
Harrod	Harroda	k1gFnPc2
<g/>
–	–	k?
<g/>
Domar	Domar	k1gMnSc1
•	•	k?
Solow	Solow	k1gMnSc1
<g/>
–	–	k?
<g/>
Swan	Swan	k1gMnSc1
•	•	k?
Ramsey	Ramsea	k1gFnSc2
<g/>
–	–	k?
<g/>
Cass	Cass	k1gInSc1
<g/>
–	–	k?
<g/>
Koopmans	Koopmans	k1gInSc1
•	•	k?
Model	modla	k1gFnPc2
překrývajících	překrývající	k2eAgFnPc2d1
se	se	k3xPyFc4
generací	generace	k1gFnPc2
•	•	k?
Obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
rovnováhy	rovnováha	k1gFnSc2
•	•	k?
Teorie	teorie	k1gFnSc1
endogenního	endogenní	k2eAgInSc2d1
růstu	růst	k1gInSc2
•	•	k?
Teorie	teorie	k1gFnSc1
shody	shoda	k1gFnSc2
•	•	k?
Mundell	Mundell	k1gInSc1
<g/>
–	–	k?
<g/>
Fleming	Fleming	k1gInSc1
•	•	k?
Model	model	k1gInSc1
překročení	překročení	k1gNnSc2
•	•	k?
NAIRU	NAIRU	kA
Související	související	k2eAgFnSc1d1
</s>
<s>
Ekonometrie	Ekonometrie	k1gFnSc1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
•	•	k?
Monetární	monetární	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Rozvojová	rozvojový	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
Školy	škola	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
proud	proud	k1gInSc1
</s>
<s>
Keynesiánství	Keynesiánství	k1gNnSc1
(	(	kIx(
<g/>
Neo-Nová	Neo-Nová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
•	•	k?
Monetarismus	Monetarismus	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
klasická	klasický	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
(	(	kIx(
<g/>
Teorie	teorie	k1gFnSc1
reálného	reálný	k2eAgInSc2d1
obchdního	obchdní	k2eAgInSc2d1
cykklu	cykkl	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Stockholmská	stockholmský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
strany	strana	k1gFnSc2
nabídky	nabídka	k1gFnSc2
•	•	k?
Nová	nový	k2eAgFnSc1d1
neoklasická	neoklasický	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
•	•	k?
Slano	slano	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Sladko	sladko	k6eAd1
vodní	vodní	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
Heterodox	Heterodox	k1gInSc1
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Chartalism	Chartalism	k1gInSc1
(	(	kIx(
<g/>
Moderní	moderní	k2eAgFnSc1d1
monetární	monetární	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rovnovážná	rovnovážný	k2eAgFnSc1d1
makroekonomie	makroekonomie	k1gFnSc1
•	•	k?
Marxistická	marxistický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Postkeynesiánství	Postkeynesiánství	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Cirkulační	cirkulační	k2eAgFnSc1d1
<g/>
)	)	kIx)
•	•	k?
Tržní	tržní	k2eAgInSc1d1
monetarismus	monetarismus	k1gInSc1
</s>
<s>
Významní	významný	k2eAgMnPc1d1
makroekonomové	makroekonom	k1gMnPc1
</s>
<s>
François	François	k1gFnSc1
Quesnay	Quesnaa	k1gFnSc2
•	•	k?
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Robert	Robert	k1gMnSc1
Malthus	Malthus	k1gMnSc1
•	•	k?
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
•	•	k?
Léon	Léon	k1gMnSc1
Walras	Walras	k1gMnSc1
•	•	k?
Georg	Georg	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
Knapp	Knapp	k1gMnSc1
•	•	k?
Knut	knuta	k1gFnPc2
Wicksell	Wicksell	k1gMnSc1
•	•	k?
Irving	Irving	k1gInSc1
Fisher	Fishra	k1gFnPc2
•	•	k?
Wesley	Weslea	k1gFnSc2
Clair	Clair	k1gMnSc1
Mitchell	Mitchell	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
Maynard	Maynard	k1gMnSc1
Keynes	Keynes	k1gMnSc1
•	•	k?
Alvin	Alvin	k1gMnSc1
Hansen	Hansen	k2eAgMnSc1d1
•	•	k?
Michał	Michał	k1gMnSc1
Kalecki	Kaleck	k1gFnSc2
•	•	k?
Gunnar	Gunnar	k1gMnSc1
Myrdal	Myrdal	k1gMnSc1
•	•	k?
Simon	Simon	k1gMnSc1
Kuznets	Kuznetsa	k1gFnPc2
•	•	k?
Joan	Joan	k1gMnSc1
Robinson	Robinson	k1gMnSc1
•	•	k?
Friedrich	Friedrich	k1gMnSc1
Hayek	Hayek	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
John	John	k1gMnSc1
Hicks	Hicksa	k1gFnPc2
•	•	k?
Richard	Richard	k1gMnSc1
Stone	ston	k1gInSc5
•	•	k?
Hyman	Hyman	k1gMnSc1
Minsky	minsky	k6eAd1
•	•	k?
Milton	Milton	k1gInSc1
Friedman	Friedman	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
•	•	k?
Lawrence	Lawrence	k1gFnSc2
Klein	Klein	k1gMnSc1
•	•	k?
Edmund	Edmund	k1gMnSc1
Phelps	Phelpsa	k1gFnPc2
•	•	k?
Robert	Robert	k1gMnSc1
Lucas	Lucas	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
•	•	k?
Edward	Edward	k1gMnSc1
C.	C.	kA
Prescott	Prescott	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Diamond	Diamond	k1gMnSc1
•	•	k?
William	William	k1gInSc1
Nordhaus	Nordhaus	k1gMnSc1
•	•	k?
Joseph	Joseph	k1gMnSc1
Stiglitz	Stiglitz	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
J.	J.	kA
Sargent	Sargent	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Krugman	Krugman	k1gMnSc1
•	•	k?
Gregory	Gregor	k1gMnPc7
Mankiw	Mankiw	k1gFnPc7
K	k	k7c3
vidění	vidění	k1gNnSc3
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Ekonomika	ekonomik	k1gMnSc2
•	•	k?
Makroekonomický	makroekonomický	k2eAgInSc4d1
model	model	k1gInSc4
•	•	k?
Seznam	seznam	k1gInSc1
významných	významný	k2eAgFnPc2d1
ekonomických	ekonomický	k2eAgFnPc2d1
publikací	publikace	k1gFnPc2
•	•	k?
Mikroekonomie	mikroekonomie	k1gFnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990005454	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118578537	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2279	#num#	k4
6570	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79006935	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500234949	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
49228757	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79006935	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Filosofie	filosofie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
