<s>
Vigdís	Vigdís	k6eAd1
Finnbogadóttir	Finnbogadóttir	k1gInSc1
</s>
<s>
Vigdís	Vigdís	k6eAd1
Finnbogadóttir	Finnbogadóttir	k1gInSc1
je	být	k5eAaImIp3nS
islandské	islandský	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
Finnbogadóttir	Finnbogadóttir	k1gMnSc1
je	být	k5eAaImIp3nS
patronymum	patronymum	k1gInSc4
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
klasické	klasický	k2eAgNnSc4d1
příjmení	příjmení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
osoba	osoba	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
islandském	islandský	k2eAgInSc6d1
prostředí	prostředí	k1gNnSc2
označována	označovat	k5eAaImNgFnS
pouze	pouze	k6eAd1
jako	jako	k8xS,k8xC
Vigdís	Vigdís	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vigdís	Vigdís	k6eAd1
Finnbogadóttir	Finnbogadóttir	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezidentka	prezidentka	k1gFnSc1
Islandu	Island	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1980	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1996	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Kristján	Kristján	k1gMnSc1
Eldjárn	Eldjárn	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Ólafur	Ólafur	k1gMnSc1
Ragnar	Ragnar	k1gMnSc1
Grímsson	Grímsson	k1gMnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Strana	strana	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1930	#num#	k4
(	(	kIx(
<g/>
91	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Reykjavík	Reykjavík	k1gInSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
Island	Island	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Fakulta	fakulta	k1gFnSc1
umění	umění	k1gNnSc2
Pařížské	pařížský	k2eAgFnSc2d1
univerzityIslandská	univerzityIslandský	k2eAgFnSc1d1
univerzitaKodaňská	univerzitaKodaňský	k2eAgFnSc1d1
univerzitaJoseph	univerzitaJoseph	k1gMnSc1
Fourier	Fourier	k1gInSc4
University	universita	k1gFnSc2
Profese	profese	k1gFnSc1
</s>
<s>
politička	politička	k1gFnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Řád	řád	k1gInSc1
slona	slon	k1gMnSc2
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
velkokříž	velkokříž	k1gInSc1
speciální	speciální	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
Záslužného	záslužný	k2eAgInSc2d1
řádu	řád	k1gInSc2
Spolkové	spolkový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Německo	Německo	k1gNnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
<g/>
Cena	cena	k1gFnSc1
Må	Må	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
Nordens	Nordens	k1gInSc1
språ	språ	k1gFnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
Honorary	Honorara	k1gFnSc2
doctor	doctor	k1gInSc1
of	of	k?
the	the	k?
Paris-Sorbonne	Paris-Sorbonn	k1gInSc5
University	universita	k1gFnPc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Commons	Commonsa	k1gFnPc2
</s>
<s>
Vigdís	Vigdís	k6eAd1
Finnbogadóttir	Finnbogadóttir	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vigdís	Vigdís	k1gFnSc1
Finnbogadóttir	Finnbogadóttir	k1gFnSc1
(	(	kIx(
<g/>
přechýleně	přechýleně	k6eAd1
Finnbogadóttirová	Finnbogadóttirová	k1gFnSc1
<g/>
;	;	kIx,
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1930	#num#	k4
Reykjavík	Reykjavík	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
islandská	islandský	k2eAgFnSc1d1
politička	politička	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1980	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
čtvrtý	čtvrtý	k4xOgMnSc1
islandský	islandský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvolením	zvolení	k1gNnSc7
na	na	k7c4
post	post	k1gInSc4
prezidenta	prezident	k1gMnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
první	první	k4xOgFnSc7
prezidentkou	prezidentka	k1gFnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
vůbec	vůbec	k9
první	první	k4xOgFnSc7
ženskou	ženský	k2eAgFnSc7d1
hlavou	hlava	k1gFnSc7
státu	stát	k1gInSc2
zvolenou	zvolený	k2eAgFnSc7d1
v	v	k7c6
demokratických	demokratický	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Studovala	studovat	k5eAaImAgFnS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Grenoblu	Grenoble	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
Sorbonně	Sorbonna	k1gFnSc6
francouzskou	francouzský	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
a	a	k8xC
poté	poté	k6eAd1
i	i	k9
v	v	k7c6
Kodani	Kodaň	k1gFnSc6
a	a	k8xC
na	na	k7c6
islandské	islandský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
dějiny	dějiny	k1gFnPc4
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
pracovala	pracovat	k5eAaImAgFnS
v	v	k7c6
divadelnictví	divadelnictví	k1gNnSc6
a	a	k8xC
vyučovala	vyučovat	k5eAaImAgFnS
francouzštinu	francouzština	k1gFnSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
známá	známý	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
aktivistka	aktivistka	k1gFnSc1
bojující	bojující	k2eAgFnSc1d1
proti	proti	k7c3
ozbrojeným	ozbrojený	k2eAgFnPc3d1
silám	síla	k1gFnPc3
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
sídlícím	sídlící	k2eAgInPc3d1
na	na	k7c6
území	území	k1gNnSc6
Islandu	Island	k1gInSc2
a	a	k8xC
vyžadující	vyžadující	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
Islandu	Island	k1gInSc2
z	z	k7c2
NATO	NATO	kA
(	(	kIx(
<g/>
„	„	k?
<g/>
Ísland	Ísland	k1gInSc1
úr	úr	k?
NATO	NATO	kA
<g/>
,	,	kIx,
herinn	herinn	k1gMnSc1
burt	burt	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zvolení	zvolení	k1gNnSc6
prezidentkou	prezidentka	k1gFnSc7
pracovala	pracovat	k5eAaImAgFnS
na	na	k7c6
vytvoření	vytvoření	k1gNnSc6
dobrého	dobrý	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
své	svůj	k3xOyFgFnSc2
země	zem	k1gFnSc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
odešla	odejít	k5eAaPmAgFnS
z	z	k7c2
funkce	funkce	k1gFnSc2
<g/>
,	,	kIx,
podílela	podílet	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c6
různých	různý	k2eAgInPc6d1
mezinárodních	mezinárodní	k2eAgInPc6d1
mírových	mírový	k2eAgInPc6d1
projektech	projekt	k1gInPc6
<g/>
,	,	kIx,
dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
vyslankyní	vyslankyně	k1gFnSc7
dobré	dobrý	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
UNESCO	Unesco	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Vigdís	Vigdís	k1gInSc1
Finnbogadóttir	Finnbogadóttir	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vigdís	Vigdísa	k1gFnPc2
Finnbogadóttir	Finnbogadóttira	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Prezidenti	prezident	k1gMnPc1
Islandu	Island	k1gInSc2
</s>
<s>
Sveinn	Sveinn	k1gMnSc1
Björnsson	Björnsson	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ásgeir	Ásgeir	k1gMnSc1
Ásgeirsson	Ásgeirsson	k1gMnSc1
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kristján	Kristján	k1gMnSc1
Eldjárn	Eldjárn	k1gMnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vigdís	Vigdís	k1gInSc1
Finnbogadóttir	Finnbogadóttir	k1gInSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ólafur	Ólafur	k1gMnSc1
Ragnar	Ragnar	k1gMnSc1
Grímsson	Grímsson	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Guð	Guð	k1gMnPc1
Thorlacius	Thorlacius	k1gMnSc1
Jóhannesson	Jóhannesson	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
211906	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
136228704	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1059	#num#	k4
5675	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81137459	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
43170438	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81137459	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Island	Island	k1gInSc1
</s>
