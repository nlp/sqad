<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
ofic	ofic	k6eAd1	ofic
<g/>
.	.	kIx.	.
VUT	VUT	kA	VUT
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
název	název	k1gInSc1	název
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veřejná	veřejný	k2eAgFnSc1d1	veřejná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c6	na
technické	technický	k2eAgFnSc6d1	technická
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
a	a	k8xC	a
umělecké	umělecký	k2eAgFnSc2d1	umělecká
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
