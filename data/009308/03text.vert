<p>
<s>
Javořice	Javořice	k1gFnSc1	Javořice
(	(	kIx(	(
<g/>
837	[number]	k4	837
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
<g/>
Jaborschützberg	Jaborschützberg	k1gInSc1	Jaborschützberg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Jihlavských	jihlavský	k2eAgInPc2d1	jihlavský
vrchů	vrch	k1gInPc2	vrch
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
celé	celý	k2eAgFnSc2d1	celá
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
okresu	okres	k1gInSc2	okres
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
9	[number]	k4	9
kilometrů	kilometr	k1gInPc2	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Telče	Telč	k1gFnSc2	Telč
a	a	k8xC	a
5	[number]	k4	5
kilometrů	kilometr	k1gInPc2	kilometr
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Studené	Studená	k1gFnSc2	Studená
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Mrákotín	Mrákotína	k1gFnPc2	Mrákotína
u	u	k7c2	u
Telče	Telč	k1gFnSc2	Telč
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgNnPc4d1	jihozápadní
úbočí	úbočí	k1gNnPc4	úbočí
do	do	k7c2	do
k.	k.	k?	k.
ú.	ú.	k?	ú.
Světlá	světlat	k5eAaImIp3nS	světlat
pod	pod	k7c7	pod
Javořicí	Javořice	k1gFnSc7	Javořice
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
Studená	studený	k2eAgFnSc1d1	studená
<g/>
)	)	kIx)	)
a	a	k8xC	a
severozápadní	severozápadní	k2eAgNnPc4d1	severozápadní
úbočí	úbočí	k1gNnPc4	úbočí
do	do	k7c2	do
k.	k.	k?	k.
ú.	ú.	k?	ú.
Klatovec	Klatovec	k1gInSc1	Klatovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrcholek	vrcholek	k1gInSc1	vrcholek
je	být	k5eAaImIp3nS	být
ukryt	ukrýt	k5eAaPmNgInS	ukrýt
v	v	k7c6	v
rozlehlém	rozlehlý	k2eAgInSc6d1	rozlehlý
komplexu	komplex	k1gInSc6	komplex
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
166	[number]	k4	166
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgInSc4d1	vysoký
stožár	stožár	k1gInSc4	stožár
televizního	televizní	k2eAgInSc2d1	televizní
vysílače	vysílač	k1gInSc2	vysílač
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
luk	louka	k1gFnPc2	louka
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
výhled	výhled	k1gInSc1	výhled
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
(	(	kIx(	(
<g/>
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
alpský	alpský	k2eAgInSc1d1	alpský
Schneeberg	Schneeberg	k1gInSc1	Schneeberg
<g/>
)	)	kIx)	)
až	až	k9	až
jihozápad	jihozápad	k1gInSc1	jihozápad
(	(	kIx(	(
<g/>
Novohradské	novohradský	k2eAgFnPc1d1	Novohradská
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
Šumava	Šumava	k1gFnSc1	Šumava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Částečný	částečný	k2eAgInSc1d1	částečný
výhled	výhled	k1gInSc1	výhled
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rovněž	rovněž	k9	rovněž
blízká	blízký	k2eAgFnSc1d1	blízká
Míchova	Míchův	k2eAgFnSc1d1	Míchova
skála	skála	k1gFnSc1	skála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
Javořice	Javořice	k1gFnSc1	Javořice
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
střežený	střežený	k2eAgInSc1d1	střežený
areál	areál	k1gInSc1	areál
vysílače	vysílač	k1gInSc2	vysílač
není	být	k5eNaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
<g/>
Trvá	trvat	k5eAaImIp3nS	trvat
myšlenka	myšlenka	k1gFnSc1	myšlenka
stavby	stavba	k1gFnSc2	stavba
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
kdysi	kdysi	k6eAd1	kdysi
stála	stát	k5eAaImAgFnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
Josef	Josef	k1gMnSc1	Josef
Herbrych	Herbrych	k1gMnSc1	Herbrych
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plány	plán	k1gInPc1	plán
na	na	k7c4	na
rozhlednu	rozhledna	k1gFnSc4	rozhledna
stále	stále	k6eAd1	stále
trvají	trvat	k5eAaImIp3nP	trvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgMnSc1d1	možný
od	od	k7c2	od
Světlé	světlý	k2eAgFnSc2d1	světlá
(	(	kIx(	(
<g/>
2	[number]	k4	2
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
žluté	žlutý	k2eAgFnSc6d1	žlutá
a	a	k8xC	a
následně	následně	k6eAd1	následně
zelené	zelený	k2eAgFnSc6d1	zelená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
od	od	k7c2	od
Studené	Studená	k1gFnSc2	Studená
(	(	kIx(	(
<g/>
7	[number]	k4	7
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
po	po	k7c6	po
červené	červený	k2eAgFnSc6d1	červená
značce	značka	k1gFnSc6	značka
od	od	k7c2	od
Telče	Telč	k1gFnSc2	Telč
(	(	kIx(	(
<g/>
12	[number]	k4	12
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
cyklostezek	cyklostezka	k1gFnPc2	cyklostezka
vede	vést	k5eAaImIp3nS	vést
rovněž	rovněž	k9	rovněž
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Automobilem	automobil	k1gInSc7	automobil
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
přijet	přijet	k5eAaPmF	přijet
k	k	k7c3	k
Velkému	velký	k2eAgInSc3d1	velký
pařezitému	pařezitý	k2eAgInSc3d1	pařezitý
rybníku	rybník	k1gInSc3	rybník
(	(	kIx(	(
<g/>
poblíž	poblíž	k6eAd1	poblíž
leží	ležet	k5eAaImIp3nS	ležet
autokemp	autokemp	k1gInSc1	autokemp
Řásná	řásnat	k5eAaImIp3nS	řásnat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
zelená	zelený	k2eAgFnSc1d1	zelená
značka	značka	k1gFnSc1	značka
(	(	kIx(	(
<g/>
3	[number]	k4	3
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
je	být	k5eAaImIp3nS	být
nádraží	nádraží	k1gNnSc1	nádraží
v	v	k7c6	v
Jihlávce	Jihlávka	k1gFnSc6	Jihlávka
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
asi	asi	k9	asi
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
nízké	nízký	k2eAgFnSc3d1	nízká
vytíženosti	vytíženost	k1gFnSc3	vytíženost
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
Kraj	kraj	k1gInSc4	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
vedených	vedený	k2eAgInPc2d1	vedený
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
stanici	stanice	k1gFnSc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc4	trať
mají	mít	k5eAaImIp3nP	mít
nadále	nadále	k6eAd1	nadále
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
rychlíky	rychlík	k1gInPc4	rychlík
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
zastavovat	zastavovat	k5eAaImF	zastavovat
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
Počátky-Žirovnice	Počátky-Žirovnice	k1gFnSc2	Počátky-Žirovnice
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc1d1	horní
Cerekev	Cerekev	k1gFnSc1	Cerekev
a	a	k8xC	a
Batelov	Batelov	k1gInSc1	Batelov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
od	od	k7c2	od
Javořice	Javořice	k1gFnSc2	Javořice
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistické	turistický	k2eAgInPc1d1	turistický
cíle	cíl	k1gInPc1	cíl
==	==	k?	==
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
500	[number]	k4	500
m	m	kA	m
pod	pod	k7c7	pod
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
svahu	svah	k1gInSc6	svah
odpočinkové	odpočinkový	k2eAgNnSc4d1	odpočinkové
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
pramenem	pramen	k1gInSc7	pramen
známým	známý	k2eAgInSc7d1	známý
jako	jako	k8xC	jako
Studánka	studánka	k1gFnSc1	studánka
Páně	páně	k2eAgFnSc1d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1	severovýchodně
<g/>
,	,	kIx,	,
na	na	k7c6	na
zelené	zelený	k2eAgFnSc6d1	zelená
turistické	turistický	k2eAgFnSc6d1	turistická
značce	značka	k1gFnSc6	značka
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
lze	lze	k6eAd1	lze
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
ke	k	k7c3	k
hradu	hrad	k1gInSc3	hrad
Roštejn	Roštejna	k1gFnPc2	Roštejna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
skalní	skalní	k2eAgInSc1d1	skalní
útvar	útvar	k1gInSc1	útvar
Míchova	Míchův	k2eAgFnSc1d1	Míchova
skála	skála	k1gFnSc1	skála
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Velkého	velký	k2eAgInSc2d1	velký
Pařezitého	Pařezitý	k2eAgInSc2d1	Pařezitý
rybníka	rybník	k1gInSc2	rybník
je	být	k5eAaImIp3nS	být
arboretum	arboretum	k1gNnSc4	arboretum
a	a	k8xC	a
autokemp	autokemp	k1gInSc4	autokemp
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Telče	Telč	k1gFnSc2	Telč
prochází	procházet	k5eAaImIp3nS	procházet
javořickými	javořický	k2eAgInPc7d1	javořický
lesy	les	k1gInPc7	les
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Otokara	Otokar	k1gMnSc2	Otokar
Březiny	Březina	k1gMnSc2	Březina
<g/>
.	.	kIx.	.
</s>
<s>
Zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Štamberk	Štamberk	k1gInSc1	Štamberk
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k6eAd1	poblíž
Řásné	řásný	k2eAgNnSc1d1	řásný
<g/>
,	,	kIx,	,
památník	památník	k1gInSc1	památník
příchodu	příchod	k1gInSc2	příchod
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
severovýchodně	severovýchodně	k6eAd1	severovýchodně
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Světlá	světlat	k5eAaImIp3nS	světlat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Geoparku	Geopark	k1gInSc2	Geopark
Vysočina	vysočina	k1gFnSc1	vysočina
byla	být	k5eAaImAgFnS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
stezka	stezka	k1gFnSc1	stezka
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
v	v	k7c6	v
Roštejnské	Roštejnský	k2eAgFnSc6d1	Roštejnský
oboře	obora	k1gFnSc6	obora
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
přírodovědně-výukovým	přírodovědněýukový	k2eAgNnSc7d1	přírodovědně-výukový
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
však	však	k9	však
tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
zřejmě	zřejmě	k6eAd1	zřejmě
pozastaven	pozastaven	k2eAgInSc1d1	pozastaven
či	či	k8xC	či
dále	daleko	k6eAd2	daleko
upravován	upravovat	k5eAaImNgInS	upravovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
zvažována	zvažován	k2eAgFnSc1d1	zvažována
i	i	k9	i
stavba	stavba	k1gFnSc1	stavba
samotné	samotný	k2eAgFnSc2d1	samotná
rozhledny	rozhledna	k1gFnSc2	rozhledna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Javořice	Javořice	k1gFnSc2	Javořice
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
tisku	tisk	k1gInSc6	tisk
==	==	k?	==
</s>
</p>
<p>
<s>
Javořice	Javořice	k1gFnSc1	Javořice
je	být	k5eAaImIp3nS	být
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
např.	např.	kA	např.
v	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Příšerné	příšerný	k2eAgInPc4d1	příšerný
příběhy	příběh	k1gInPc4	příběh
od	od	k7c2	od
Egona	Egon	k1gMnSc2	Egon
Bondyho	Bondy	k1gMnSc2	Bondy
<g/>
,	,	kIx,	,
v	v	k7c6	v
básních	báseň	k1gFnPc6	báseň
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Šeříka	Šeřík	k1gMnSc2	Šeřík
(	(	kIx(	(
<g/>
sbírky	sbírka	k1gFnSc2	sbírka
Rybničná	Rybničný	k2eAgFnSc1d1	Rybničná
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
Nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
<g/>
)	)	kIx)	)
,	,	kIx,	,
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
Vlasty	Vlasta	k1gFnSc2	Vlasta
Javořické	Javořická	k1gFnSc2	Javořická
(	(	kIx(	(
<g/>
sbírky	sbírka	k1gFnSc2	sbírka
Pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
Javořicí	Javořice	k1gFnSc7	Javořice
<g/>
,	,	kIx,	,
Obrázky	obrázek	k1gInPc4	obrázek
z	z	k7c2	z
Vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
periodickém	periodický	k2eAgInSc6d1	periodický
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Javořice	Javořice	k1gFnSc2	Javořice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
