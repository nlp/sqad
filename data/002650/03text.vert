<s>
Mariáš	mariáš	k1gInSc1	mariáš
je	být	k5eAaImIp3nS	být
zdvihová	zdvihový	k2eAgFnSc1d1	zdvihová
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
až	až	k9	až
čtyři	čtyři	k4xCgMnPc4	čtyři
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
s	s	k7c7	s
balíčkem	balíček	k1gInSc7	balíček
32	[number]	k4	32
karet	kareta	k1gFnPc2	kareta
německého	německý	k2eAgInSc2d1	německý
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
zvanými	zvaný	k2eAgInPc7d1	zvaný
"	"	kIx"	"
<g/>
mariášky	mariášek	k1gInPc7	mariášek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
dvouhlavé	dvouhlavý	k2eAgFnPc1d1	dvouhlavá
francouzské	francouzský	k2eAgFnPc1d1	francouzská
karty	karta	k1gFnPc1	karta
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
pikety	piketa	k1gFnSc2	piketa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hry	hra	k1gFnSc2	hra
Mariage	Mariag	k1gFnSc2	Mariag
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Slovem	slovem	k6eAd1	slovem
"	"	kIx"	"
<g/>
mariage	mariage	k6eAd1	mariage
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
svatba	svatba	k1gFnSc1	svatba
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
označovala	označovat	k5eAaImAgFnS	označovat
hláška	hláška	k1gFnSc1	hláška
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
svršek	svršek	k1gInSc1	svršek
-	-	kIx~	-
u	u	k7c2	u
karet	kareta	k1gFnPc2	kareta
francouzského	francouzský	k2eAgInSc2d1	francouzský
typu	typ	k1gInSc2	typ
dáma	dáma	k1gFnSc1	dáma
-	-	kIx~	-
a	a	k8xC	a
král	král	k1gMnSc1	král
téže	týž	k3xTgFnSc2	týž
barvy	barva	k1gFnSc2	barva
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
téhož	týž	k3xTgMnSc2	týž
hráče	hráč	k1gMnSc2	hráč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
hry	hra	k1gFnSc2	hra
Mariage	Mariag	k1gFnSc2	Mariag
převzal	převzít	k5eAaPmAgInS	převzít
mariáš	mariáš	k1gInSc1	mariáš
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
i	i	k9	i
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
her	hra	k1gFnPc2	hra
jakými	jaký	k3yIgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
taroky	tarok	k1gInPc1	tarok
<g/>
,	,	kIx,	,
skat	skat	k1gInSc1	skat
či	či	k8xC	či
špády	špáda	k1gFnPc1	špáda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hry	hra	k1gFnSc2	hra
jsou	být	k5eAaImIp3nP	být
hráči	hráč	k1gMnPc1	hráč
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
uhraje	uhrát	k5eAaPmIp3nS	uhrát
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
či	či	k8xC	či
zda	zda	k8xS	zda
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
sehrání	sehrání	k1gNnSc3	sehrání
zvoleného	zvolený	k2eAgInSc2d1	zvolený
způsobu	způsob	k1gInSc2	způsob
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgFnPc7d1	základní
variantami	varianta	k1gFnPc7	varianta
hry	hra	k1gFnPc4	hra
jsou	být	k5eAaImIp3nP	být
mariáš	mariáš	k1gInSc1	mariáš
volený	volený	k2eAgInSc1d1	volený
či	či	k8xC	či
licitovaný	licitovaný	k2eAgInSc1d1	licitovaný
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
určeny	určen	k2eAgInPc1d1	určen
pro	pro	k7c4	pro
tři	tři	k4xCgMnPc4	tři
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dva	dva	k4xCgMnPc4	dva
hráče	hráč	k1gMnPc4	hráč
existuje	existovat	k5eAaImIp3nS	existovat
zjednodušený	zjednodušený	k2eAgInSc1d1	zjednodušený
lízaný	lízaný	k2eAgInSc1d1	lízaný
mariáš	mariáš	k1gInSc1	mariáš
a	a	k8xC	a
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgMnPc4	čtyři
hráče	hráč	k1gMnPc4	hráč
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
čtyřka	čtyřka	k1gFnSc1	čtyřka
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
lízaný	lízaný	k2eAgInSc1d1	lízaný
mariáš	mariáš	k1gInSc1	mariáš
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
čtyřka	čtyřka	k1gFnSc1	čtyřka
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
volenému	volený	k2eAgInSc3d1	volený
mariáši	mariáš	k1gInSc3	mariáš
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
čtyři	čtyři	k4xCgMnPc1	čtyři
hráči	hráč	k1gMnPc1	hráč
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
hrát	hrát	k5eAaImF	hrát
hru	hra	k1gFnSc4	hra
pro	pro	k7c4	pro
tři	tři	k4xCgMnPc4	tři
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
sehrávce	sehrávka	k1gFnSc3	sehrávka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
hru	hra	k1gFnSc4	hra
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
<g/>
,	,	kIx,	,
nehraje	hrát	k5eNaImIp3nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc1	takový
hře	hra	k1gFnSc3	hra
říkáme	říkat	k5eAaImIp1nP	říkat
pauzírovaný	pauzírovaný	k2eAgInSc4d1	pauzírovaný
mariáš	mariáš	k1gInSc4	mariáš
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
pauzírák	pauzírák	k1gInSc1	pauzírák
<g/>
"	"	kIx"	"
Rozdávající	rozdávající	k2eAgMnSc1d1	rozdávající
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
"	"	kIx"	"
<g/>
forhonta	forhont	k1gMnSc4	forhont
<g/>
"	"	kIx"	"
a	a	k8xC	a
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
"	"	kIx"	"
<g/>
zadáka	zadák	k1gMnSc2	zadák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
křížem	křížem	k6eAd1	křížem
proti	proti	k7c3	proti
rozdávajícímu	rozdávající	k2eAgMnSc3d1	rozdávající
(	(	kIx(	(
<g/>
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
<g/>
)	)	kIx)	)
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc1	žádný
vžitý	vžitý	k2eAgInSc1d1	vžitý
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
jako	jako	k8xS	jako
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
středu	středa	k1gFnSc4	středa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
kolech	kolo	k1gNnPc6	kolo
se	se	k3xPyFc4	se
postavení	postavení	k1gNnSc1	postavení
hráčů	hráč	k1gMnPc2	hráč
posouvá	posouvat	k5eAaImIp3nS	posouvat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
chodu	chod	k1gInSc2	chod
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
forhont	forhont	k1gInSc1	forhont
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
hru	hra	k1gFnSc4	hra
rozdávajícím	rozdávající	k2eAgFnPc3d1	rozdávající
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
karty	karta	k1gFnPc1	karta
zamíchají	zamíchat	k5eAaPmIp3nP	zamíchat
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nesmí	smět	k5eNaImIp3nS	smět
míchat	míchat	k5eAaImF	míchat
<g/>
.	.	kIx.	.
</s>
<s>
Porušení	porušení	k1gNnSc1	porušení
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
prohřešek	prohřešek	k1gInSc4	prohřešek
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
renonc	renonc	k1gInSc1	renonc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
ale	ale	k8xC	ale
striktně	striktně	k6eAd1	striktně
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
spíše	spíše	k9	spíše
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dohrání	dohrání	k1gNnSc6	dohrání
jedné	jeden	k4xCgFnSc2	jeden
sehrávky	sehrávka	k1gFnSc2	sehrávka
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
označení	označení	k1gNnSc1	označení
hráčů	hráč	k1gMnPc2	hráč
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
pozici	pozice	k1gFnSc4	pozice
a	a	k8xC	a
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
rozdávajícím	rozdávající	k2eAgInSc7d1	rozdávající
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
právo	právo	k1gNnSc1	právo
složit	složit	k5eAaPmF	složit
karty	karta	k1gFnPc4	karta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
doposud	doposud	k6eAd1	doposud
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
hromádkách	hromádka	k1gFnPc6	hromádka
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
,	,	kIx,	,
do	do	k7c2	do
balíčku	balíček	k1gInSc2	balíček
(	(	kIx(	(
<g/>
karty	karta	k1gFnSc2	karta
však	však	k9	však
nesmí	smět	k5eNaImIp3nS	smět
míchat	míchat	k5eAaImF	míchat
<g/>
)	)	kIx)	)
a	a	k8xC	a
složený	složený	k2eAgInSc1d1	složený
balíček	balíček	k1gInSc1	balíček
všech	všecek	k3xTgFnPc2	všecek
32	[number]	k4	32
karet	kareta	k1gFnPc2	kareta
položit	položit	k5eAaPmF	položit
před	před	k7c4	před
zadáka	zadák	k1gMnSc4	zadák
a	a	k8xC	a
požádat	požádat	k5eAaPmF	požádat
jej	on	k3xPp3gMnSc4	on
o	o	k7c4	o
sejmutí	sejmutí	k1gNnSc4	sejmutí
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	on	k3xPp3gNnSc4	on
sejme	sejmout	k5eAaPmIp3nS	sejmout
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
sejmout	sejmout	k5eAaPmF	sejmout
minimálně	minimálně	k6eAd1	minimálně
dvě	dva	k4xCgFnPc4	dva
karty	karta	k1gFnPc4	karta
(	(	kIx(	(
<g/>
od	od	k7c2	od
vrchu	vrch	k1gInSc2	vrch
či	či	k8xC	či
od	od	k7c2	od
spodu	spod	k1gInSc2	spod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
počet	počet	k1gInSc1	počet
nesmí	smět	k5eNaImIp3nS	smět
odpočítávat	odpočítávat	k5eAaImF	odpočítávat
či	či	k8xC	či
odlistovávat	odlistovávat	k5eAaImF	odlistovávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mariáši	mariáš	k1gInSc6	mariáš
se	se	k3xPyFc4	se
karetní	karetní	k2eAgFnPc1d1	karetní
barvy	barva	k1gFnPc1	barva
označují	označovat	k5eAaImIp3nP	označovat
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInPc2	jejich
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
tak	tak	k6eAd1	tak
<g/>
:	:	kIx,	:
U	u	k7c2	u
piketových	piketový	k2eAgFnPc2d1	piketový
karet	kareta	k1gFnPc2	kareta
užívaných	užívaný	k2eAgFnPc2d1	užívaná
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
barvy	barva	k1gFnSc2	barva
"	"	kIx"	"
<g/>
herce	herec	k1gMnSc2	herec
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
též	též	k9	též
"	"	kIx"	"
<g/>
červené	červený	k2eAgNnSc1d1	červené
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
káry	kára	k1gFnPc4	kára
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
též	též	k9	též
"	"	kIx"	"
<g/>
kule	kule	k1gFnSc1	kule
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
piky	pik	k1gInPc7	pik
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
kříže	kříž	k1gInPc1	kříž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nehraje	hrát	k5eNaImIp3nS	hrát
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
betl	betl	k1gMnSc1	betl
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
durch	durch	k6eAd1	durch
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pořadí	pořadí	k1gNnSc1	pořadí
hodnot	hodnota	k1gFnPc2	hodnota
karet	kareta	k1gFnPc2	kareta
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
od	od	k7c2	od
nejnižšího	nízký	k2eAgMnSc2d3	nejnižší
<g/>
)	)	kIx)	)
ve	v	k7c6	v
sledu	sled	k1gInSc6	sled
7	[number]	k4	7
-	-	kIx~	-
8	[number]	k4	8
-	-	kIx~	-
9	[number]	k4	9
-	-	kIx~	-
spodek	spodek	k1gInSc1	spodek
-	-	kIx~	-
svršek	svršek	k1gInSc1	svršek
-	-	kIx~	-
král	král	k1gMnSc1	král
-	-	kIx~	-
10	[number]	k4	10
-	-	kIx~	-
eso	eso	k1gNnSc4	eso
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
betla	betl	k1gMnSc2	betl
a	a	k8xC	a
durcha	durch	k1gInSc2	durch
je	být	k5eAaImIp3nS	být
sled	sled	k1gInSc1	sled
karetních	karetní	k2eAgFnPc2d1	karetní
hodnot	hodnota	k1gFnPc2	hodnota
standardní	standardní	k2eAgNnSc1d1	standardní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
(	(	kIx(	(
<g/>
od	od	k7c2	od
nejnižšího	nízký	k2eAgInSc2d3	nejnižší
<g/>
)	)	kIx)	)
7	[number]	k4	7
-	-	kIx~	-
8	[number]	k4	8
-	-	kIx~	-
9	[number]	k4	9
-	-	kIx~	-
10	[number]	k4	10
-	-	kIx~	-
spodek	spodek	k1gInSc1	spodek
-	-	kIx~	-
svršek	svršek	k1gInSc1	svršek
-	-	kIx~	-
král	král	k1gMnSc1	král
-	-	kIx~	-
eso	eso	k1gNnSc1	eso
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
v	v	k7c6	v
rozdílné	rozdílný	k2eAgFnSc6d1	rozdílná
hierarchické	hierarchický	k2eAgFnSc6d1	hierarchická
pozici	pozice	k1gFnSc6	pozice
u	u	k7c2	u
karty	karta	k1gFnSc2	karta
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mariáši	mariáš	k1gInSc6	mariáš
lze	lze	k6eAd1	lze
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
následující	následující	k2eAgFnPc4d1	následující
hry	hra	k1gFnPc4	hra
<g/>
:	:	kIx,	:
hra	hra	k1gFnSc1	hra
lepší	dobrý	k2eAgFnSc1d2	lepší
hra	hra	k1gFnSc1	hra
sedma	sedma	k1gFnSc1	sedma
lepší	dobrý	k2eAgFnSc1d2	lepší
sedma	sedma	k1gFnSc1	sedma
stovka	stovka	k1gFnSc1	stovka
stosedm	stosedm	k6eAd1	stosedm
lepší	dobrý	k2eAgFnSc1d2	lepší
stovka	stovka	k1gFnSc1	stovka
lepší	lepšit	k5eAaImIp3nS	lepšit
stosedm	stosedm	k6eAd1	stosedm
betl	betl	k1gMnSc1	betl
durch	durch	k6eAd1	durch
dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
lepší	lepšit	k5eAaImIp3nP	lepšit
dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
"	"	kIx"	"
<g/>
Lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
červených	červená	k1gFnPc6	červená
Během	během	k7c2	během
sehrávky	sehrávka	k1gFnSc2	sehrávka
jsou	být	k5eAaImIp3nP	být
hráči	hráč	k1gMnPc1	hráč
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
uhraje	uhrát	k5eAaPmIp3nS	uhrát
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc4	bod
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
za	za	k7c4	za
uhrané	uhraný	k2eAgNnSc4d1	uhrané
eso	eso	k1gNnSc4	eso
či	či	k8xC	či
desítku	desítka	k1gFnSc4	desítka
během	během	k7c2	během
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hry	hra	k1gFnSc2	hra
dvojici	dvojice	k1gFnSc4	dvojice
svrška	svršek	k1gMnSc2	svršek
a	a	k8xC	a
krále	král	k1gMnSc2	král
od	od	k7c2	od
stejné	stejný	k2eAgFnSc2d1	stejná
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
eso	eso	k1gNnSc4	eso
nebo	nebo	k8xC	nebo
desítku	desítka	k1gFnSc4	desítka
získává	získávat	k5eAaImIp3nS	získávat
hráč	hráč	k1gMnSc1	hráč
10	[number]	k4	10
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
tuto	tento	k3xDgFnSc4	tento
dvojici	dvojice	k1gFnSc4	dvojice
svrška	svršek	k1gMnSc2	svršek
a	a	k8xC	a
krále	král	k1gMnSc2	král
získává	získávat	k5eAaImIp3nS	získávat
hráč	hráč	k1gMnSc1	hráč
20	[number]	k4	20
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
dvojice	dvojice	k1gFnSc1	dvojice
v	v	k7c6	v
barvě	barva	k1gFnSc6	barva
trumfové	trumfový	k2eAgNnSc1d1	trumfové
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
nepočítá	počítat	k5eNaImIp3nS	počítat
se	se	k3xPyFc4	se
za	za	k7c4	za
20	[number]	k4	20
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
za	za	k7c4	za
40	[number]	k4	40
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
získává	získávat	k5eAaImIp3nS	získávat
ten	ten	k3xDgMnSc1	ten
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
poslední	poslední	k2eAgInSc4d1	poslední
štych	štych	k1gInSc4	štych
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInSc4d1	poslední
zdvih	zdvih	k1gInSc4	zdvih
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
hře	hra	k1gFnSc6	hra
získat	získat	k5eAaPmF	získat
maximálně	maximálně	k6eAd1	maximálně
190	[number]	k4	190
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
sehrávce	sehrávka	k1gFnSc6	sehrávka
se	se	k3xPyFc4	se
však	však	k9	však
vždy	vždy	k6eAd1	vždy
hraje	hrát	k5eAaImIp3nS	hrát
o	o	k7c4	o
minimálně	minimálně	k6eAd1	minimálně
90	[number]	k4	90
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
protistrany	protistrana	k1gFnPc1	protistrana
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
zdvizích	zdvih	k1gInPc6	zdvih
uhráno	uhrát	k5eAaPmNgNnS	uhrát
více	hodně	k6eAd2	hodně
bodovaných	bodovaný	k2eAgFnPc2d1	bodovaná
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Bodovanými	bodovaný	k2eAgFnPc7d1	bodovaná
kartami	karta	k1gFnPc7	karta
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
esa	eso	k1gNnPc4	eso
a	a	k8xC	a
desítky	desítka	k1gFnPc4	desítka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
získaným	získaný	k2eAgFnPc3d1	získaná
bodovaným	bodovaný	k2eAgFnPc3d1	bodovaná
kartám	karta	k1gFnPc3	karta
se	se	k3xPyFc4	se
připočítává	připočítávat	k5eAaImIp3nS	připočítávat
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
získání	získání	k1gNnSc4	získání
posledního	poslední	k2eAgInSc2d1	poslední
zdvihu	zdvih	k1gInSc2	zdvih
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ultimo	ultimo	k1gNnSc1	ultimo
<g/>
,	,	kIx,	,
a	a	k8xC	a
bonifikace	bonifikace	k1gFnPc1	bonifikace
za	za	k7c4	za
nahlášené	nahlášený	k2eAgFnPc4d1	nahlášená
hlášky	hláška	k1gFnPc4	hláška
neboli	neboli	k8xC	neboli
mariáše	mariáš	k1gInPc4	mariáš
<g/>
.	.	kIx.	.
</s>
<s>
Hláškou	hláška	k1gFnSc7	hláška
či	či	k8xC	či
mariášem	mariáš	k1gInSc7	mariáš
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
svršek	svršek	k1gInSc1	svršek
a	a	k8xC	a
král	král	k1gMnSc1	král
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
jednoho	jeden	k4xCgMnSc2	jeden
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
nahlášené	nahlášený	k2eAgFnSc2d1	nahlášená
hlášky	hláška	k1gFnSc2	hláška
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
trumfové	trumfový	k2eAgFnSc6d1	trumfová
barvě	barva	k1gFnSc6	barva
40	[number]	k4	40
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Hláška	hláška	k1gFnSc1	hláška
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nahlášenou	nahlášený	k2eAgFnSc4d1	nahlášená
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
majitel	majitel	k1gMnSc1	majitel
oznámí	oznámit	k5eAaPmIp3nS	oznámit
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
první	první	k4xOgMnSc1	první
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
dvojice	dvojice	k1gFnSc2	dvojice
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Oznámení	oznámení	k1gNnSc1	oznámení
hlášky	hláška	k1gFnSc2	hláška
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
položením	položení	k1gNnSc7	položení
odehrávané	odehrávaný	k2eAgFnSc2d1	odehrávaná
karty	karta	k1gFnSc2	karta
(	(	kIx(	(
<g/>
svrška	svršek	k1gMnSc2	svršek
nebo	nebo	k8xC	nebo
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
lícem	líc	k1gInSc7	líc
na	na	k7c4	na
svoje	svůj	k3xOyFgInPc4	svůj
zdvihy	zdvih	k1gInPc4	zdvih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
získá	získat	k5eAaPmIp3nS	získat
sto	sto	k4xCgNnSc1	sto
a	a	k8xC	a
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
tichou	tichý	k2eAgFnSc4d1	tichá
stovku	stovka	k1gFnSc4	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
uhrát	uhrát	k5eAaPmF	uhrát
poslední	poslední	k2eAgInSc4d1	poslední
zdvih	zdvih	k1gInSc4	zdvih
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
svoji	svůj	k3xOyFgFnSc4	svůj
trumfovou	trumfový	k2eAgFnSc4d1	trumfová
sedmou	sedma	k1gFnSc7	sedma
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyhrát	vyhrát	k5eAaPmF	vyhrát
hru	hra	k1gFnSc4	hra
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
získat	získat	k5eAaPmF	získat
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
než	než	k8xS	než
obrana	obrana	k1gFnSc1	obrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
součástí	součást	k1gFnPc2	součást
se	se	k3xPyFc4	se
flekuje	flekovat	k5eAaImIp3nS	flekovat
zvlášť	zvlášť	k6eAd1	zvlášť
a	a	k8xC	a
zvlášť	zvlášť	k6eAd1	zvlášť
se	se	k3xPyFc4	se
i	i	k9	i
počítá	počítat	k5eAaImIp3nS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Závazek	závazek	k1gInSc1	závazek
sedma	sedma	k1gFnSc1	sedma
není	být	k5eNaImIp3nS	být
splněn	splněn	k2eAgMnSc1d1	splněn
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
aktér	aktér	k1gMnSc1	aktér
nucen	nutit	k5eAaImNgMnS	nutit
hrát	hrát	k5eAaImF	hrát
trumfovou	trumfový	k2eAgFnSc4d1	trumfová
sedmu	sedma	k1gFnSc4	sedma
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zdvihu	zdvih	k1gInSc6	zdvih
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
obrana	obrana	k1gFnSc1	obrana
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zdvihu	zdvih	k1gInSc6	zdvih
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
jiný	jiný	k2eAgInSc1d1	jiný
trumf	trumf	k1gInSc1	trumf
<g/>
.	.	kIx.	.
</s>
<s>
Aktér	aktér	k1gMnSc1	aktér
musí	muset	k5eAaImIp3nS	muset
hrát	hrát	k5eAaImF	hrát
trumfovou	trumfový	k2eAgFnSc4d1	trumfová
sedmu	sedma	k1gFnSc4	sedma
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
trumf	trumf	k1gInSc4	trumf
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
"	"	kIx"	"
<g/>
sedma	sedma	k1gFnSc1	sedma
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
lepší	dobrý	k2eAgFnSc1d2	lepší
sedma	sedma	k1gFnSc1	sedma
<g/>
"	"	kIx"	"
nelze	lze	k6eNd1	lze
ohlásit	ohlásit	k5eAaPmF	ohlásit
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
bez	bez	k7c2	bez
sedmy	sedma	k1gFnSc2	sedma
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
!	!	kIx.	!
</s>
<s>
Závazek	závazek	k1gInSc1	závazek
sto	sto	k4xCgNnSc4	sto
je	být	k5eAaImIp3nS	být
variantou	varianta	k1gFnSc7	varianta
závazku	závazek	k1gInSc2	závazek
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
aktér	aktér	k1gMnSc1	aktér
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
uhrát	uhrát	k5eAaPmF	uhrát
alespoň	alespoň	k9	alespoň
sto	sto	k4xCgNnSc1	sto
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
počítá	počítat	k5eAaImIp3nS	počítat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
hláška	hláška	k1gFnSc1	hláška
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc1	základ
stovky	stovka	k1gFnSc2	stovka
trumfová	trumfový	k2eAgFnSc1d1	trumfová
hláška	hláška	k1gFnSc1	hláška
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
aktér	aktér	k1gMnSc1	aktér
získat	získat	k5eAaPmF	získat
alespoň	alespoň	k9	alespoň
60	[number]	k4	60
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
90	[number]	k4	90
možných	možný	k2eAgInPc2d1	možný
(	(	kIx(	(
<g/>
4	[number]	k4	4
esa	eso	k1gNnPc4	eso
<g/>
,	,	kIx,	,
4	[number]	k4	4
desítky	desítka	k1gFnPc1	desítka
a	a	k8xC	a
ultimo	ultimo	k1gNnSc1	ultimo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
stovky	stovka	k1gFnSc2	stovka
hláška	hláška	k1gFnSc1	hláška
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
získat	získat	k5eAaPmF	získat
alespoň	alespoň	k9	alespoň
80	[number]	k4	80
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
obrana	obrana	k1gFnSc1	obrana
stovku	stovka	k1gFnSc4	stovka
poráží	porážet	k5eAaImIp3nS	porážet
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
<g/>
-li	i	k?	-li
alespoň	alespoň	k9	alespoň
40	[number]	k4	40
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
-li	i	k?	-li
aktér	aktér	k1gMnSc1	aktér
s	s	k7c7	s
trumfovou	trumfový	k2eAgFnSc7d1	trumfová
hláškou	hláška	k1gFnSc7	hláška
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
získá	získat	k5eAaPmIp3nS	získat
<g/>
-li	i	k?	-li
alespoň	alespoň	k9	alespoň
20	[number]	k4	20
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
-li	i	k?	-li
aktér	aktér	k1gMnSc1	aktér
s	s	k7c7	s
hláškou	hláška	k1gFnSc7	hláška
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Vítězná	vítězný	k2eAgFnSc1d1	vítězná
strana	strana	k1gFnSc1	strana
dostane	dostat	k5eAaPmIp3nS	dostat
uhrazenu	uhrazen	k2eAgFnSc4d1	uhrazena
sazbu	sazba	k1gFnSc4	sazba
za	za	k7c4	za
stovku	stovka	k1gFnSc4	stovka
plus	plus	k6eAd1	plus
stejnou	stejný	k2eAgFnSc4d1	stejná
částku	částka	k1gFnSc4	částka
za	za	k7c4	za
každých	každý	k3xTgInPc2	každý
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
získala	získat	k5eAaPmAgFnS	získat
nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
dalších	další	k2eAgFnPc2d1	další
hlášek	hláška	k1gFnPc2	hláška
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
č.	č.	k?	č.
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Aktér	aktér	k1gMnSc1	aktér
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
trumfovou	trumfový	k2eAgFnSc4d1	trumfová
hlášku	hláška	k1gFnSc4	hláška
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
70	[number]	k4	70
z	z	k7c2	z
90	[number]	k4	90
možných	možný	k2eAgInPc2d1	možný
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
další	další	k2eAgFnSc4d1	další
hlášku	hláška	k1gFnSc4	hláška
<g/>
.	.	kIx.	.
</s>
<s>
Závazek	závazek	k1gInSc1	závazek
sto	sto	k4xCgNnSc4	sto
splnil	splnit	k5eAaPmAgInS	splnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	s	k7c7	s
započtením	započtení	k1gNnSc7	započtení
jedné	jeden	k4xCgFnSc2	jeden
Hlášky	hláška	k1gFnSc2	hláška
uhrál	uhrát	k5eAaPmAgInS	uhrát
110	[number]	k4	110
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
hláškou	hláška	k1gFnSc7	hláška
získal	získat	k5eAaPmAgMnS	získat
130	[number]	k4	130
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
tedy	tedy	k9	tedy
od	od	k7c2	od
každého	každý	k3xTgMnSc2	každý
hráče	hráč	k1gMnSc2	hráč
dostane	dostat	k5eAaPmIp3nS	dostat
zaplaceno	zaplatit	k5eAaPmNgNnS	zaplatit
4	[number]	k4	4
<g/>
x	x	k?	x
sazbu	sazba	k1gFnSc4	sazba
za	za	k7c4	za
stovku	stovka	k1gFnSc4	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
č.	č.	k?	č.
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
Aktér	aktér	k1gMnSc1	aktér
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
netrumfovou	trumfový	k2eNgFnSc4d1	trumfový
hlášku	hláška	k1gFnSc4	hláška
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
60	[number]	k4	60
z	z	k7c2	z
90	[number]	k4	90
možných	možný	k2eAgInPc2d1	možný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Obrana	obrana	k1gFnSc1	obrana
měla	mít	k5eAaImAgFnS	mít
dvě	dva	k4xCgFnPc4	dva
jiné	jiný	k2eAgFnPc4d1	jiná
hlášky	hláška	k1gFnPc4	hláška
<g/>
.	.	kIx.	.
</s>
<s>
Obraně	obraně	k6eAd1	obraně
stačilo	stačit	k5eAaBmAgNnS	stačit
k	k	k7c3	k
poražení	poražení	k1gNnSc3	poražení
stovky	stovka	k1gFnSc2	stovka
20	[number]	k4	20
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
získala	získat	k5eAaPmAgFnS	získat
jich	on	k3xPp3gNnPc2	on
30	[number]	k4	30
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dalších	další	k2eAgInPc2d1	další
40	[number]	k4	40
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
hlášky	hláška	k1gFnPc4	hláška
<g/>
.	.	kIx.	.
</s>
<s>
Aktér	aktér	k1gMnSc1	aktér
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
každému	každý	k3xTgMnSc3	každý
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
zaplatit	zaplatit	k5eAaPmF	zaplatit
6	[number]	k4	6
<g/>
x	x	k?	x
sazbu	sazba	k1gFnSc4	sazba
za	za	k7c4	za
stovku	stovka	k1gFnSc4	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
uhrát	uhrát	k5eAaPmF	uhrát
stovku	stovka	k1gFnSc4	stovka
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
zdvih	zdvih	k1gInSc4	zdvih
vzít	vzít	k5eAaPmF	vzít
svoji	svůj	k3xOyFgFnSc4	svůj
trumfovou	trumfový	k2eAgFnSc4d1	trumfová
sedmou	sedmý	k4xOgFnSc4	sedmý
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
součástí	součást	k1gFnPc2	součást
se	se	k3xPyFc4	se
flekuje	flekovat	k5eAaImIp3nS	flekovat
zvlášť	zvlášť	k6eAd1	zvlášť
a	a	k8xC	a
zvlášť	zvlášť	k6eAd1	zvlášť
se	se	k3xPyFc4	se
i	i	k9	i
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
a	a	k8xC	a
počítá	počítat	k5eAaImIp3nS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
stosedm	stosedm	k6eAd1	stosedm
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ohlášena	ohlásit	k5eAaPmNgFnS	ohlásit
<g/>
,	,	kIx,	,
i	i	k9	i
pokud	pokud	k8xS	pokud
aktér	aktér	k1gMnSc1	aktér
nemá	mít	k5eNaImIp3nS	mít
trumfovou	trumfový	k2eAgFnSc4d1	trumfová
sedmu	sedma	k1gFnSc4	sedma
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
sedmy	sedma	k1gFnSc2	sedma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
neuhrát	uhrát	k5eNaPmF	uhrát
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
zdvih	zdvih	k1gInSc4	zdvih
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
první	první	k4xOgInSc4	první
výnos	výnos	k1gInSc4	výnos
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
voleny	volen	k2eAgInPc1d1	volen
trumfy	trumf	k1gInPc1	trumf
a	a	k8xC	a
desítka	desítka	k1gFnSc1	desítka
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
mezi	mezi	k7c7	mezi
spodkem	spodek	k1gInSc7	spodek
a	a	k8xC	a
devítkou	devítka	k1gFnSc7	devítka
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
uhrát	uhrát	k5eAaPmF	uhrát
všechny	všechen	k3xTgInPc4	všechen
zdvihy	zdvih	k1gInPc4	zdvih
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
první	první	k4xOgInSc4	první
výnos	výnos	k1gInSc4	výnos
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
voleny	volen	k2eAgInPc1d1	volen
trumfy	trumf	k1gInPc1	trumf
a	a	k8xC	a
desítka	desítka	k1gFnSc1	desítka
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
mezi	mezi	k7c7	mezi
spodkem	spodek	k1gInSc7	spodek
a	a	k8xC	a
devítkou	devítka	k1gFnSc7	devítka
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
oznámí	oznámit	k5eAaPmIp3nS	oznámit
hru	hra	k1gFnSc4	hra
např.	např.	kA	např.
"	"	kIx"	"
<g/>
Kule	kule	k1gFnSc1	kule
trumf	trumf	k1gInSc1	trumf
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
tlačí	tlačit	k5eAaImIp3nS	tlačit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předposlední	předposlední	k2eAgInSc1d1	předposlední
zdvih	zdvih	k1gInSc1	zdvih
vezme	vzít	k5eAaPmIp3nS	vzít
tlačnou	tlačný	k2eAgFnSc7d1	tlačná
červenou	červený	k2eAgFnSc7d1	červená
sedmou	sedma	k1gFnSc7	sedma
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
zdvih	zdvih	k1gInSc1	zdvih
trumfovou	trumfový	k2eAgFnSc7d1	trumfová
kulovou	kulový	k2eAgFnSc7d1	kulová
sedmou	sedma	k1gFnSc7	sedma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
neúspěchu	neúspěch	k1gInSc3	neúspěch
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
sedem	sedma	k1gFnPc2	sedma
přebita	přebit	k2eAgFnSc1d1	přebita
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
aktér	aktér	k1gMnSc1	aktér
donucen	donutit	k5eAaPmNgMnS	donutit
hrát	hrát	k5eAaImF	hrát
trumfovou	trumfový	k2eAgFnSc4d1	trumfová
sedmu	sedma	k1gFnSc4	sedma
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zdvihu	zdvih	k1gInSc6	zdvih
<g/>
.	.	kIx.	.
</s>
<s>
Tlačná	tlačný	k2eAgFnSc1d1	tlačná
barva	barva	k1gFnSc1	barva
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgNnSc4	žádný
podobné	podobný	k2eAgNnSc4d1	podobné
postavení	postavení	k1gNnSc4	postavení
jako	jako	k8xC	jako
trumf	trumf	k1gInSc4	trumf
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
mají	mít	k5eAaImIp3nP	mít
status	status	k1gInSc4	status
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
desítka	desítka	k1gFnSc1	desítka
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
esem	eso	k1gNnSc7	eso
a	a	k8xC	a
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
esa	eso	k1gNnPc4	eso
ani	ani	k8xC	ani
desítky	desítka	k1gFnPc4	desítka
proto	proto	k8xC	proto
nesmí	smět	k5eNaImIp3nS	smět
do	do	k7c2	do
talonu	talon	k1gInSc2	talon
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
licitovaném	licitovaný	k2eAgInSc6d1	licitovaný
mariáši	mariáš	k1gInSc6	mariáš
nebo	nebo	k8xC	nebo
v	v	k7c6	v
křížovém	křížový	k2eAgInSc6d1	křížový
licitovaném	licitovaný	k2eAgInSc6d1	licitovaný
mariáši	mariáš	k1gInSc6	mariáš
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hra	hra	k1gFnSc1	hra
v	v	k7c6	v
licitovaném	licitovaný	k2eAgInSc6d1	licitovaný
mariáši	mariáš	k1gInSc6	mariáš
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
jako	jako	k9	jako
kombinace	kombinace	k1gFnSc1	kombinace
stovky	stovka	k1gFnSc2	stovka
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
sedem	sedma	k1gFnPc2	sedma
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
tedy	tedy	k9	tedy
musí	muset	k5eAaImIp3nS	muset
splnit	splnit	k5eAaPmF	splnit
oba	dva	k4xCgInPc4	dva
závazky	závazek	k1gInPc4	závazek
-	-	kIx~	-
předposlední	předposlední	k2eAgInSc4d1	předposlední
zdvih	zdvih	k1gInSc4	zdvih
vzít	vzít	k5eAaPmF	vzít
sedmou	sedma	k1gFnSc7	sedma
v	v	k7c6	v
tlačné	tlačný	k2eAgFnSc6d1	tlačná
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
zdvih	zdvih	k1gInSc4	zdvih
sedmou	sedma	k1gFnSc7	sedma
v	v	k7c6	v
trumfové	trumfový	k2eAgFnSc6d1	trumfová
barvě	barva	k1gFnSc6	barva
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
jedné	jeden	k4xCgFnSc2	jeden
hlášky	hláška	k1gFnSc2	hláška
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
alespoň	alespoň	k9	alespoň
sta	sto	k4xCgNnPc4	sto
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
ze	z	k7c2	z
závazků	závazek	k1gInPc2	závazek
se	se	k3xPyFc4	se
flekuje	flekovat	k5eAaImIp3nS	flekovat
<g/>
,	,	kIx,	,
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
a	a	k8xC	a
počítá	počítat	k5eAaImIp3nS	počítat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Přebije	přebít	k5eAaPmIp3nS	přebít
<g/>
-li	i	k?	-li
některý	některý	k3yIgInSc4	některý
hráč	hráč	k1gMnSc1	hráč
poslední	poslední	k2eAgInSc4d1	poslední
štych	štych	k1gInSc4	štych
trumfovou	trumfový	k2eAgFnSc7d1	trumfová
sedmou	sedma	k1gFnSc7	sedma
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sám	sám	k3xTgMnSc1	sám
neohlásil	ohlásit	k5eNaPmAgInS	ohlásit
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uhrál	uhrát	k5eAaPmAgInS	uhrát
tichou	tichý	k2eAgFnSc4d1	tichá
sedmu	sedma	k1gFnSc4	sedma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
však	však	k9	však
takováto	takovýto	k3xDgFnSc1	takovýto
neohlášená	ohlášený	k2eNgFnSc1d1	neohlášená
sedma	sedma	k1gFnSc1	sedma
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
štychu	štych	k1gInSc6	štych
někým	někdo	k3yInSc7	někdo
přebita	přebít	k5eAaPmNgFnS	přebít
<g/>
,	,	kIx,	,
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
držitel	držitel	k1gMnSc1	držitel
sedmy	sedma	k1gFnSc2	sedma
cenu	cena	k1gFnSc4	cena
tiché	tichý	k2eAgFnSc2d1	tichá
sedmy	sedma	k1gFnSc2	sedma
protistraně	protistrana	k1gFnSc3	protistrana
<g/>
.	.	kIx.	.
</s>
<s>
Uhraje	uhrát	k5eAaPmIp3nS	uhrát
<g/>
-li	i	k?	-li
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
protistran	protistrana	k1gFnPc2	protistrana
neohlášenou	ohlášený	k2eNgFnSc4d1	neohlášená
-	-	kIx~	-
tichou	tichý	k2eAgFnSc4d1	tichá
stovku	stovka	k1gFnSc4	stovka
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgFnPc2	všecek
hlášek	hláška	k1gFnPc2	hláška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
zaplaceno	zaplacen	k2eAgNnSc1d1	zaplaceno
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
ceny	cena	k1gFnSc2	cena
právě	právě	k6eAd1	právě
hrané	hraný	k2eAgFnSc2d1	hraná
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
desítku	desítka	k1gFnSc4	desítka
nad	nad	k7c4	nad
sto	sto	k4xCgNnSc4	sto
dostane	dostat	k5eAaPmIp3nS	dostat
opět	opět	k6eAd1	opět
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
20	[number]	k4	20
hal	hala	k1gFnPc2	hala
<g/>
.	.	kIx.	.
sazba	sazba	k1gFnSc1	sazba
<g/>
,	,	kIx,	,
FLEK	flek	k1gInSc1	flek
-	-	kIx~	-
RE	re	k9	re
-	-	kIx~	-
TUTTI	tutti	k2eAgFnSc1d1	tutti
na	na	k7c4	na
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
obrana	obrana	k1gFnSc1	obrana
uhrává	uhrávat	k5eAaImIp3nS	uhrávat
120	[number]	k4	120
<g/>
.	.	kIx.	.
</s>
<s>
FLEK	flek	k1gInSc1	flek
-	-	kIx~	-
0,40	[number]	k4	0,40
RE	re	k9	re
-	-	kIx~	-
0,80	[number]	k4	0,80
TUTTI	tutti	k2eAgMnPc2d1	tutti
-	-	kIx~	-
1,60	[number]	k4	1,60
tichá	tichý	k2eAgFnSc1d1	tichá
100	[number]	k4	100
-	-	kIx~	-
3,20	[number]	k4	3,20
(	(	kIx(	(
<g/>
zdvojnásobení	zdvojnásobení	k1gNnSc4	zdvojnásobení
sazby	sazba	k1gFnSc2	sazba
<g/>
)	)	kIx)	)
tichých	tichý	k2eAgInPc2d1	tichý
120	[number]	k4	120
-	-	kIx~	-
3	[number]	k4	3
x	x	k?	x
3,20	[number]	k4	3,20
=	=	kIx~	=
9,60	[number]	k4	9,60
Tichou	tichý	k2eAgFnSc4d1	tichá
stovku	stovka	k1gFnSc4	stovka
nelze	lze	k6eNd1	lze
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tiché	tichý	k2eAgFnSc2d1	tichá
sedmy	sedma	k1gFnSc2	sedma
<g/>
)	)	kIx)	)
prohrát	prohrát	k5eAaPmF	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
tři	tři	k4xCgMnPc1	tři
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgMnPc1	čtyři
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
hráči	hráč	k1gMnPc1	hráč
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
přímo	přímo	k6eAd1	přímo
zapojeni	zapojit	k5eAaPmNgMnP	zapojit
do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
hráči	hráč	k1gMnPc1	hráč
čtyři	čtyři	k4xCgInPc4	čtyři
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
hře	hra	k1gFnSc6	hra
jeden	jeden	k4xCgInSc4	jeden
tak	tak	k6eAd1	tak
zvaně	zvaně	k6eAd1	zvaně
pauzíruje	pauzírovat	k5eAaImIp3nS	pauzírovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
hře	hra	k1gFnSc6	hra
pauzíruje	pauzírovat	k5eAaImIp3nS	pauzírovat
vždy	vždy	k6eAd1	vždy
ten	ten	k3xDgInSc4	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
hře	hra	k1gFnSc6	hra
volícím	volící	k2eAgNnSc7d1	volící
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vybral	vybrat	k5eAaPmAgMnS	vybrat
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
aktér	aktér	k1gMnSc1	aktér
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
sám	sám	k3xTgMnSc1	sám
proti	proti	k7c3	proti
dvojici	dvojice	k1gFnSc3	dvojice
obránců	obránce	k1gMnPc2	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Obránci	obránce	k1gMnPc1	obránce
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samozřejmě	samozřejmě	k6eAd1	samozřejmě
si	se	k3xPyFc3	se
nesmějí	smát	k5eNaImIp3nP	smát
nahlížet	nahlížet	k5eAaImF	nahlížet
do	do	k7c2	do
karet	kareta	k1gFnPc2	kareta
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
jakkoliv	jakkoliv	k6eAd1	jakkoliv
domlouvat	domlouvat	k5eAaImF	domlouvat
na	na	k7c6	na
sehrávce	sehrávka	k1gFnSc6	sehrávka
<g/>
.	.	kIx.	.
</s>
<s>
Vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
-li	i	k?	-li
hru	hra	k1gFnSc4	hra
aktér	aktér	k1gMnSc1	aktér
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
platby	platba	k1gFnPc1	platba
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
případného	případný	k2eAgMnSc2d1	případný
pauzírujícího	pauzírující	k2eAgMnSc2d1	pauzírující
hráče	hráč	k1gMnSc2	hráč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
-li	i	k?	-li
obrana	obrana	k1gFnSc1	obrana
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
aktér	aktér	k1gMnSc1	aktér
všem	všecek	k3xTgNnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sejmutí	sejmutí	k1gNnSc6	sejmutí
se	se	k3xPyFc4	se
rozdává	rozdávat	k5eAaImIp3nS	rozdávat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Volícímu	volící	k2eAgMnSc3d1	volící
hráči	hráč	k1gMnSc3	hráč
7	[number]	k4	7
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
zbylým	zbylý	k2eAgMnPc3d1	zbylý
dvěma	dva	k4xCgFnPc7	dva
po	po	k7c6	po
5	[number]	k4	5
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
všem	všecek	k3xTgNnPc3	všecek
po	po	k7c4	po
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Volící	volící	k2eAgMnSc1d1	volící
hráč	hráč	k1gMnSc1	hráč
vybere	vybrat	k5eAaPmIp3nS	vybrat
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
sedmi	sedm	k4xCc2	sedm
karet	kareta	k1gFnPc2	kareta
trumfy	trumf	k1gInPc1	trumf
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
také	také	k9	také
vybrat	vybrat	k5eAaPmF	vybrat
náhodně	náhodně	k6eAd1	náhodně
ze	z	k7c2	z
zbylých	zbylý	k2eAgNnPc2d1	zbylé
pěti	pět	k4xCc2	pět
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
z	z	k7c2	z
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zbylých	zbylý	k2eAgFnPc2d1	zbylá
5	[number]	k4	5
karet	kareta	k1gFnPc2	kareta
se	se	k3xPyFc4	se
před	před	k7c7	před
výběrem	výběr	k1gInSc7	výběr
trumfů	trumf	k1gInPc2	trumf
nesmí	smět	k5eNaImIp3nS	smět
podívat	podívat	k5eAaImF	podívat
<g/>
.	.	kIx.	.
</s>
<s>
Volící	volící	k2eAgMnSc1d1	volící
hráč	hráč	k1gMnSc1	hráč
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
hry	hra	k1gFnSc2	hra
odloží	odložit	k5eAaPmIp3nS	odložit
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
dvanácti	dvanáct	k4xCc2	dvanáct
karet	kareta	k1gFnPc2	kareta
dvě	dva	k4xCgFnPc4	dva
do	do	k7c2	do
talonu	talon	k1gInSc6	talon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
až	až	k9	až
do	do	k7c2	do
ohlášení	ohlášení	k1gNnSc2	ohlášení
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
talonu	talon	k1gInSc2	talon
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
odložit	odložit	k5eAaPmF	odložit
bodovaná	bodovaný	k2eAgFnSc1d1	bodovaná
karta	karta	k1gFnSc1	karta
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
eso	eso	k1gNnSc1	eso
nebo	nebo	k8xC	nebo
desítka	desítka	k1gFnSc1	desítka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
regionálních	regionální	k2eAgFnPc2d1	regionální
variant	varianta	k1gFnPc2	varianta
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
odložení	odložení	k1gNnSc4	odložení
Desítky	desítka	k1gFnSc2	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hrách	hra	k1gFnPc6	hra
betl	betl	k1gMnSc1	betl
a	a	k8xC	a
durch	durch	k6eAd1	durch
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
talonu	talon	k1gInSc2	talon
odložit	odložit	k5eAaPmF	odložit
cokoliv	cokoliv	k3yInSc4	cokoliv
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
trumfů	trumf	k1gInPc2	trumf
volícím	volící	k2eAgMnSc7d1	volící
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
nahlásit	nahlásit	k5eAaPmF	nahlásit
další	další	k2eAgInPc4d1	další
závazky	závazek	k1gInPc4	závazek
jako	jako	k8xS	jako
stovka	stovka	k1gFnSc1	stovka
nebo	nebo	k8xC	nebo
sedma	sedma	k1gFnSc1	sedma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
protivníci	protivník	k1gMnPc1	protivník
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
chtějí	chtít	k5eAaImIp3nP	chtít
hrát	hrát	k5eAaImF	hrát
normálně	normálně	k6eAd1	normálně
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
betl	betl	k1gMnSc1	betl
či	či	k8xC	či
durch	durch	k6eAd1	durch
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Špatná	špatný	k2eAgFnSc1d1	špatná
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
,	,	kIx,	,
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
sehrávka	sehrávka	k1gFnSc1	sehrávka
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
ze	z	k7c2	z
zbylých	zbylý	k2eAgMnPc2d1	zbylý
hráčů	hráč	k1gMnPc2	hráč
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
barvu	barva	k1gFnSc4	barva
za	za	k7c4	za
špatnou	špatný	k2eAgFnSc4d1	špatná
<g/>
,	,	kIx,	,
vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
odhozený	odhozený	k2eAgInSc4d1	odhozený
talon	talon	k1gInSc4	talon
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
místo	místo	k7c2	místo
něho	on	k3xPp3gMnSc2	on
odložit	odložit	k5eAaPmF	odložit
jiné	jiný	k2eAgFnPc4d1	jiná
dvě	dva	k4xCgFnPc4	dva
karty	karta	k1gFnPc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
betl	betl	k1gMnSc1	betl
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
místo	místo	k7c2	místo
schválení	schválení	k1gNnSc2	schválení
někdo	někdo	k3yInSc1	někdo
ze	z	k7c2	z
soupeřů	soupeř	k1gMnPc2	soupeř
stejným	stejný	k2eAgInSc7d1	stejný
postupem	postup	k1gInSc7	postup
změnit	změnit	k5eAaPmF	změnit
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
durch	durch	k1gInSc4	durch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ohlášení	ohlášení	k1gNnSc6	ohlášení
hry	hra	k1gFnSc2	hra
může	moct	k5eAaImIp3nS	moct
následovat	následovat	k5eAaImF	následovat
flekování	flekování	k?	flekování
a	a	k8xC	a
poté	poté	k6eAd1	poté
sehrávka	sehrávka	k1gFnSc1	sehrávka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sehrávky	sehrávka	k1gFnSc2	sehrávka
se	se	k3xPyFc4	se
sečtou	sečíst	k5eAaPmIp3nP	sečíst
body	bod	k1gInPc7	bod
a	a	k8xC	a
určí	určit	k5eAaPmIp3nS	určit
vítězná	vítězný	k2eAgFnSc1d1	vítězná
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sečtení	sečtení	k1gNnSc6	sečtení
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
vyplacení	vyplacení	k1gNnSc4	vyplacení
výher	výhra	k1gFnPc2	výhra
se	se	k3xPyFc4	se
rozdávajícím	rozdávající	k2eAgFnPc3d1	rozdávající
stane	stanout	k5eAaPmIp3nS	stanout
další	další	k2eAgMnSc1d1	další
hráč	hráč	k1gMnSc1	hráč
po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
další	další	k2eAgFnSc1d1	další
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnPc1d1	oficiální
pravidla	pravidlo	k1gNnPc1	pravidlo
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
mariáše	mariáš	k1gInSc2	mariáš
pro	pro	k7c4	pro
volený	volený	k2eAgInSc4d1	volený
mariáš	mariáš	k1gInSc4	mariáš
Hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
tři	tři	k4xCgMnPc1	tři
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgMnPc1	čtyři
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
hráči	hráč	k1gMnPc1	hráč
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc1	tři
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
přímo	přímo	k6eAd1	přímo
zapojeni	zapojit	k5eAaPmNgMnP	zapojit
do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
hráči	hráč	k1gMnPc1	hráč
čtyři	čtyři	k4xCgInPc4	čtyři
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
hře	hra	k1gFnSc6	hra
jeden	jeden	k4xCgInSc4	jeden
takzvaně	takzvaně	k6eAd1	takzvaně
pauzíruje	pauzírovat	k5eAaImIp3nS	pauzírovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
hře	hra	k1gFnSc6	hra
pauzíruje	pauzírovat	k5eAaImIp3nS	pauzírovat
vždy	vždy	k6eAd1	vždy
ten	ten	k3xDgInSc4	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
hře	hra	k1gFnSc6	hra
volícím	volící	k2eAgNnSc7d1	volící
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vybral	vybrat	k5eAaPmAgMnS	vybrat
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
aktér	aktér	k1gMnSc1	aktér
a	a	k8xC	a
hraje	hrát	k5eAaImIp3nS	hrát
sám	sám	k3xTgMnSc1	sám
proti	proti	k7c3	proti
dvojici	dvojice	k1gFnSc3	dvojice
obránců	obránce	k1gMnPc2	obránce
<g/>
.	.	kIx.	.
</s>
<s>
Obránci	obránce	k1gMnPc1	obránce
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samozřejmě	samozřejmě	k6eAd1	samozřejmě
si	se	k3xPyFc3	se
nesmějí	smát	k5eNaImIp3nP	smát
nahlížet	nahlížet	k5eAaImF	nahlížet
do	do	k7c2	do
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
jakkoliv	jakkoliv	k6eAd1	jakkoliv
domlouvat	domlouvat	k5eAaImF	domlouvat
na	na	k7c6	na
sehrávce	sehrávka	k1gFnSc6	sehrávka
<g/>
.	.	kIx.	.
</s>
<s>
Vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
-li	i	k?	-li
hru	hra	k1gFnSc4	hra
aktér	aktér	k1gMnSc1	aktér
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
platby	platba	k1gFnPc1	platba
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
případného	případný	k2eAgMnSc2d1	případný
pauzírujícího	pauzírující	k2eAgMnSc2d1	pauzírující
hráče	hráč	k1gMnSc2	hráč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
-li	i	k?	-li
obrana	obrana	k1gFnSc1	obrana
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
aktér	aktér	k1gMnSc1	aktér
všem	všecek	k3xTgNnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
licitovaném	licitovaný	k2eAgInSc6d1	licitovaný
mariáši	mariáš	k1gInSc6	mariáš
se	se	k3xPyFc4	se
rozdává	rozdávat	k5eAaImIp3nS	rozdávat
všem	všecek	k3xTgMnPc3	všecek
hráčům	hráč	k1gMnPc3	hráč
nejprve	nejprve	k6eAd1	nejprve
po	po	k7c6	po
5	[number]	k4	5
kartách	karta	k1gFnPc6	karta
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
odloží	odložit	k5eAaPmIp3nS	odložit
lícem	líc	k1gInSc7	líc
dolů	dol	k1gInPc2	dol
2	[number]	k4	2
karty	karta	k1gFnSc2	karta
talonu	talon	k1gInSc2	talon
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
znovu	znovu	k6eAd1	znovu
všem	všecek	k3xTgMnPc3	všecek
po	po	k7c6	po
5	[number]	k4	5
kartách	karta	k1gFnPc6	karta
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
hráči	hráč	k1gMnPc1	hráč
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
všech	všecek	k3xTgInPc2	všecek
svých	svůj	k3xOyFgFnPc2	svůj
10	[number]	k4	10
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
za	za	k7c4	za
rozdávajícím	rozdávající	k2eAgInSc6d1	rozdávající
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
jako	jako	k9	jako
forhont	forhont	k1gInSc1	forhont
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
nejprve	nejprve	k6eAd1	nejprve
rozdávající	rozdávající	k2eAgMnSc1d1	rozdávající
hráč	hráč	k1gMnSc1	hráč
nabízí	nabízet	k5eAaImIp3nS	nabízet
postupně	postupně	k6eAd1	postupně
stále	stále	k6eAd1	stále
vyšší	vysoký	k2eAgInPc4d2	vyšší
závazky	závazek	k1gInPc4	závazek
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
forhont	forhont	k1gMnSc1	forhont
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabízenou	nabízený	k2eAgFnSc4d1	nabízená
hru	hra	k1gFnSc4	hra
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgMnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
sám	sám	k3xTgMnSc1	sám
schopen	schopen	k2eAgMnSc1d1	schopen
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
"	"	kIx"	"
<g/>
mám	mít	k5eAaImIp1nS	mít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
licitace	licitace	k1gFnSc1	licitace
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vyzyvatel	vyzyvatel	k?	vyzyvatel
nenabídne	nabídnout	k5eNaPmIp3nS	nabídnout
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hru	hra	k1gFnSc4	hra
nebo	nebo	k8xC	nebo
forhont	forhont	k1gMnSc1	forhont
hru	hra	k1gFnSc4	hra
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vítězi	vítěz	k1gMnSc3	vítěz
licitace	licitace	k1gFnSc2	licitace
licituje	licitovat	k5eAaImIp3nS	licitovat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
třetí	třetí	k4xOgMnSc1	třetí
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
z	z	k7c2	z
licitace	licitace	k1gFnSc2	licitace
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
forhont	forhont	k1gMnSc1	forhont
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
třetímu	třetí	k4xOgMnSc3	třetí
hráči	hráč	k1gMnSc3	hráč
držet	držet	k5eAaImF	držet
stupeň	stupeň	k1gInSc4	stupeň
nabízený	nabízený	k2eAgInSc4d1	nabízený
rozdávajícím	rozdávající	k2eAgMnSc7d1	rozdávající
hráčem	hráč	k1gMnSc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
licituje	licitovat	k5eAaImIp3nS	licitovat
proti	proti	k7c3	proti
forhontovi	forhont	k1gMnSc3	forhont
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
nabízet	nabízet	k5eAaImF	nabízet
vyšší	vysoký	k2eAgInSc4d2	vyšší
stupeň	stupeň	k1gInSc4	stupeň
a	a	k8xC	a
forhontovi	forhontův	k2eAgMnPc1d1	forhontův
stačí	stačit	k5eAaBmIp3nP	stačit
nabídku	nabídka	k1gFnSc4	nabídka
držet	držet	k5eAaImF	držet
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
kompletní	kompletní	k2eAgFnSc2d1	kompletní
licitace	licitace	k1gFnSc2	licitace
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
talon	talon	k1gInSc4	talon
<g/>
,	,	kIx,	,
odhodí	odhodit	k5eAaPmIp3nS	odhodit
2	[number]	k4	2
karty	karta	k1gFnSc2	karta
a	a	k8xC	a
ohlásí	ohlásit	k5eAaPmIp3nS	ohlásit
stejný	stejný	k2eAgInSc4d1	stejný
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgInSc4d2	vyšší
závazek	závazek	k1gInSc4	závazek
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
vylicitován	vylicitovat	k5eAaPmNgInS	vylicitovat
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
pak	pak	k6eAd1	pak
proti	proti	k7c3	proti
zbylým	zbylý	k2eAgMnPc3d1	zbylý
dvěma	dva	k4xCgMnPc3	dva
hráčům	hráč	k1gMnPc3	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
případné	případný	k2eAgNnSc1d1	případné
flekování	flekování	k?	flekování
a	a	k8xC	a
běžná	běžný	k2eAgFnSc1d1	běžná
sehrávka	sehrávka	k1gFnSc1	sehrávka
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
voleného	volený	k2eAgInSc2d1	volený
mariáše	mariáš	k1gInSc2	mariáš
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
durchu	durch	k1gInSc2	durch
a	a	k8xC	a
betla	betl	k1gMnSc2	betl
vynáší	vynášet	k5eAaImIp3nS	vynášet
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
forhont	forhont	k1gInSc4	forhont
i	i	k8xC	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
licitaci	licitace	k1gFnSc4	licitace
<g/>
.	.	kIx.	.
</s>
<s>
Závazky	závazek	k1gInPc1	závazek
licitace	licitace	k1gFnSc2	licitace
jsou	být	k5eAaImIp3nP	být
postupně	postupně	k6eAd1	postupně
<g/>
:	:	kIx,	:
Sedma	sedma	k1gFnSc1	sedma
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc1d2	lepší
sedma	sedma	k1gFnSc1	sedma
<g/>
,	,	kIx,	,
sto	sto	k4xCgNnSc4	sto
<g/>
,	,	kIx,	,
stosedm	stosedmo	k1gNnPc2	stosedmo
<g/>
,	,	kIx,	,
lepší	lepšit	k5eAaImIp3nS	lepšit
sto	sto	k4xCgNnSc1	sto
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgMnSc1d2	lepší
stosedm	stosedm	k1gMnSc1	stosedm
<g/>
,	,	kIx,	,
betl	betl	k1gMnSc1	betl
<g/>
,	,	kIx,	,
durch	durch	k6eAd1	durch
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
se	se	k3xPyFc4	se
stem	sto	k4xCgNnSc7	sto
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnPc4d2	lepší
dvě	dva	k4xCgFnPc4	dva
sedmy	sedma	k1gFnPc4	sedma
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnPc4d2	lepší
dvě	dva	k4xCgFnPc4	dva
sedmy	sedma	k1gFnPc4	sedma
se	se	k3xPyFc4	se
stem	sto	k4xCgNnSc7	sto
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgFnSc7d2	lepší
hrou	hra	k1gFnSc7	hra
je	být	k5eAaImIp3nS	být
určena	určen	k2eAgFnSc1d1	určena
jako	jako	k8xC	jako
trumfová	trumfový	k2eAgFnSc1d1	trumfová
barva	barva	k1gFnSc1	barva
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nikdo	nikdo	k3yNnSc1	nikdo
nelicituje	licitovat	k5eNaImIp3nS	licitovat
ani	ani	k8xC	ani
sedmu	sedma	k1gFnSc4	sedma
<g/>
,	,	kIx,	,
karty	karta	k1gFnPc1	karta
se	se	k3xPyFc4	se
složí	složit	k5eAaPmIp3nP	složit
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
nikomu	nikdo	k3yNnSc3	nikdo
nic	nic	k3yNnSc1	nic
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
vylicituje	vylicitovat	k5eAaPmIp3nS	vylicitovat
sedmu	sedma	k1gFnSc4	sedma
a	a	k8xC	a
nechce	chtít	k5eNaImIp3nS	chtít
nebo	nebo	k8xC	nebo
nemůže	moct	k5eNaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
hrát	hrát	k5eAaImF	hrát
(	(	kIx(	(
<g/>
a	a	k8xC	a
nechce	chtít	k5eNaImIp3nS	chtít
hrát	hrát	k5eAaImF	hrát
ani	ani	k8xC	ani
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hru	hra	k1gFnSc4	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
zaplatit	zaplatit	k5eAaPmF	zaplatit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
omyl	omyl	k1gInSc1	omyl
<g/>
"	"	kIx"	"
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
6	[number]	k4	6
platebních	platební	k2eAgFnPc2d1	platební
jednotek	jednotka	k1gFnPc2	jednotka
každému	každý	k3xTgMnSc3	každý
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
vylicituje	vylicitovat	k5eAaPmIp3nS	vylicitovat
lepší	dobrý	k2eAgFnSc4d2	lepší
sedmu	sedma	k1gFnSc4	sedma
<g/>
,	,	kIx,	,
karty	karta	k1gFnPc1	karta
nelze	lze	k6eNd1	lze
takto	takto	k6eAd1	takto
složit	složit	k5eAaPmF	složit
a	a	k8xC	a
aktér	aktér	k1gMnSc1	aktér
musí	muset	k5eAaImIp3nS	muset
hrát	hrát	k5eAaImF	hrát
lepší	dobrý	k2eAgFnSc4d2	lepší
sedmu	sedma	k1gFnSc4	sedma
nebo	nebo	k8xC	nebo
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnPc1d1	oficiální
pravidla	pravidlo	k1gNnPc1	pravidlo
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
mariáše	mariáš	k1gInSc2	mariáš
pro	pro	k7c4	pro
licitovaný	licitovaný	k2eAgInSc4d1	licitovaný
mariáš	mariáš	k1gInSc4	mariáš
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgMnPc6	dva
hráčích	hráč	k1gMnPc6	hráč
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
hra	hra	k1gFnSc1	hra
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
hraných	hraný	k2eAgFnPc2d1	hraná
variant	varianta	k1gFnPc2	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Rozdá	rozdat	k5eAaPmIp3nS	rozdat
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
čtyři	čtyři	k4xCgFnPc4	čtyři
karty	karta	k1gFnPc1	karta
(	(	kIx(	(
<g/>
varianta	varianta	k1gFnSc1	varianta
jednou	jednou	k9	jednou
pět	pět	k4xCc4	pět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nerozdaný	rozdaný	k2eNgInSc1d1	rozdaný
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
talon	talon	k1gInSc4	talon
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zvolí	zvolit	k5eAaPmIp3nP	zvolit
trumfy	trumf	k1gInPc1	trumf
<g/>
,	,	kIx,	,
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
kole	kolo	k1gNnSc6	kolo
se	s	k7c7	s
1	[number]	k4	1
karta	karta	k1gFnSc1	karta
líže	lízat	k5eAaImIp3nS	lízat
z	z	k7c2	z
talonu	talon	k1gInSc2	talon
a	a	k8xC	a
1	[number]	k4	1
odhazuje	odhazovat	k5eAaImIp3nS	odhazovat
<g/>
.	.	kIx.	.
</s>
<s>
Líže	lízat	k5eAaImIp3nS	lízat
první	první	k4xOgInSc4	první
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
uhrál	uhrát	k5eAaPmAgMnS	uhrát
kolo	kolo	k1gNnSc4	kolo
(	(	kIx(	(
<g/>
štych	štych	k1gInSc1	štych
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
povinnost	povinnost	k1gFnSc1	povinnost
dodržovat	dodržovat	k5eAaImF	dodržovat
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
přebíjet	přebíjet	k5eAaImF	přebíjet
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
hrát	hrát	k5eAaImF	hrát
sedmu	sedma	k1gFnSc4	sedma
(	(	kIx(	(
<g/>
nahlásit	nahlásit	k5eAaPmF	nahlásit
před	před	k7c7	před
rozebráním	rozebrání	k1gNnSc7	rozebrání
talonu	talon	k1gInSc2	talon
<g/>
)	)	kIx)	)
či	či	k8xC	či
stovku	stovka	k1gFnSc4	stovka
.	.	kIx.	.
</s>
<s>
Durch	durch	k6eAd1	durch
či	či	k8xC	či
betl	betl	k1gMnSc1	betl
se	se	k3xPyFc4	se
nehrají	hrát	k5eNaImIp3nP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
že	že	k8xS	že
trumfy	trumf	k1gInPc1	trumf
platí	platit	k5eAaImIp3nP	platit
až	až	k9	až
po	po	k7c6	po
dobrání	dobrání	k1gNnSc6	dobrání
talonu	talon	k1gInSc2	talon
<g/>
.	.	kIx.	.
</s>
<s>
Mariáš	mariáš	k1gInSc1	mariáš
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
turnajích	turnaj	k1gInPc6	turnaj
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
dvacetníkový	dvacetníkový	k2eAgMnSc1d1	dvacetníkový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
hra	hra	k1gFnSc1	hra
stojí	stát	k5eAaImIp3nS	stát
20	[number]	k4	20
haléřů	haléř	k1gInPc2	haléř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
samozřejmě	samozřejmě	k6eAd1	samozřejmě
mohou	moct	k5eAaImIp3nP	moct
hráči	hráč	k1gMnPc1	hráč
domluvit	domluvit	k5eAaPmF	domluvit
na	na	k7c6	na
libovolné	libovolný	k2eAgFnSc6d1	libovolná
výšce	výška	k1gFnSc6	výška
základní	základní	k2eAgFnSc2d1	základní
sazby	sazba	k1gFnSc2	sazba
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
sazby	sazba	k1gFnPc1	sazba
(	(	kIx(	(
<g/>
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgNnPc6d1	uvedené
platidlech	platidlo	k1gNnPc6	platidlo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
hra	hra	k1gFnSc1	hra
1	[number]	k4	1
lepší	dobrý	k2eAgFnSc1d2	lepší
hra	hra	k1gFnSc1	hra
2	[number]	k4	2
sedma	sedma	k1gFnSc1	sedma
2	[number]	k4	2
lepší	dobrý	k2eAgFnSc1d2	lepší
sedma	sedma	k1gFnSc1	sedma
4	[number]	k4	4
sto	sto	k4xCgNnSc4	sto
4	[number]	k4	4
lepší	dobrý	k2eAgMnSc1d2	lepší
sto	sto	k4xCgNnSc4	sto
8	[number]	k4	8
betl	betl	k1gMnSc1	betl
15	[number]	k4	15
durch	durch	k6eAd1	durch
30	[number]	k4	30
dvě	dva	k4xCgFnPc4	dva
sedmy	sedma	k1gFnPc4	sedma
40	[number]	k4	40
lepší	lepšit	k5eAaImIp3nS	lepšit
dvě	dva	k4xCgFnPc4	dva
sedmy	sedma	k1gFnPc4	sedma
80	[number]	k4	80
Schvalování	schvalování	k1gNnPc2	schvalování
a	a	k8xC	a
flekování	flekování	k?	flekování
Každý	každý	k3xTgInSc4	každý
flek	flek	k1gInSc4	flek
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
cenu	cena	k1gFnSc4	cena
ohlášeného	ohlášený	k2eAgInSc2d1	ohlášený
herního	herní	k2eAgInSc2d1	herní
závazku	závazek	k1gInSc2	závazek
na	na	k7c4	na
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
flekují	flekovat	k5eAaImIp3nP	flekovat
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
podle	podle	k7c2	podle
směru	směr	k1gInSc2	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
hraje	hrát	k5eAaImIp3nS	hrát
opačně	opačně	k6eAd1	opačně
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ako	ako	k?	ako
sa	sa	k?	sa
kosí	kosit	k5eAaImIp3nS	kosit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
zřetelně	zřetelně	k6eAd1	zřetelně
říci	říct	k5eAaPmF	říct
"	"	kIx"	"
<g/>
dobrý	dobrý	k2eAgInSc4d1	dobrý
<g/>
"	"	kIx"	"
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechce	chtít	k5eNaImIp3nS	chtít
flekovat	flekovat	k5eAaImF	flekovat
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
ohlásí	ohlásit	k5eAaPmIp3nS	ohlásit
flek	flek	k1gInSc4	flek
s	s	k7c7	s
případným	případný	k2eAgNnSc7d1	případné
upřesněním	upřesnění	k1gNnSc7	upřesnění
(	(	kIx(	(
<g/>
na	na	k7c4	na
co	co	k9	co
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Porušení	porušení	k1gNnSc1	porušení
pořadí	pořadí	k1gNnSc2	pořadí
je	být	k5eAaImIp3nS	být
renoncem	renonc	k1gInSc7	renonc
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
sedma	sedma	k1gFnSc1	sedma
nebo	nebo	k8xC	nebo
stosedm	stosedm	k1gInSc1	stosedm
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
lepších	dobrý	k2eAgFnPc6d2	lepší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
flekovat	flekovat	k5eAaImF	flekovat
tu	ten	k3xDgFnSc4	ten
součást	součást	k1gFnSc4	součást
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
kole	kolo	k1gNnSc6	kolo
flekování	flekování	k?	flekování
flekována	flekován	k2eAgFnSc1d1	flekován
<g/>
.	.	kIx.	.
</s>
<s>
Flekování	Flekování	k?	Flekování
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
zazní	zaznít	k5eAaPmIp3nS	zaznít
<g/>
-li	i	k?	-li
ode	ode	k7c2	ode
všech	všecek	k3xTgMnPc2	všecek
hráčů	hráč	k1gMnPc2	hráč
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
"	"	kIx"	"
<g/>
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ložené	ložený	k2eAgFnSc2d1	ložená
hry	hra	k1gFnSc2	hra
Hráč	hráč	k1gMnSc1	hráč
flekující	flekující	k2eAgFnSc4d1	flekující
loženou	ložený	k2eAgFnSc4d1	ložená
hru	hra	k1gFnSc4	hra
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
zaplatit	zaplatit	k5eAaPmF	zaplatit
navýšení	navýšení	k1gNnPc4	navýšení
ceny	cena	k1gFnSc2	cena
hry	hra	k1gFnSc2	hra
nejen	nejen	k6eAd1	nejen
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
za	za	k7c4	za
poškozeného	poškozený	k2eAgMnSc4d1	poškozený
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Ložené	ložený	k2eAgFnPc1d1	ložená
hry	hra	k1gFnPc1	hra
mají	mít	k5eAaImIp3nP	mít
svoji	svůj	k3xOyFgFnSc4	svůj
přesnou	přesný	k2eAgFnSc4d1	přesná
definici	definice	k1gFnSc4	definice
<g/>
:	:	kIx,	:
sedma	sedma	k1gFnSc1	sedma
je	být	k5eAaImIp3nS	být
ložená	ložený	k2eAgFnSc1d1	ložená
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
aktér	aktér	k1gMnSc1	aktér
nemůže	moct	k5eNaImIp3nS	moct
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nS	muset
ztratit	ztratit	k5eAaPmF	ztratit
žádný	žádný	k3yNgInSc4	žádný
zdvih	zdvih	k1gInSc4	zdvih
hra	hra	k1gFnSc1	hra
a	a	k8xC	a
stovka	stovka	k1gFnSc1	stovka
je	být	k5eAaImIp3nS	být
ložená	ložený	k2eAgFnSc1d1	ložená
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
aktér	aktér	k1gMnSc1	aktér
nemůže	moct	k5eNaImIp3nS	moct
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nS	muset
ztratit	ztratit	k5eAaPmF	ztratit
ani	ani	k9	ani
deset	deset	k4xCc4	deset
betl	betl	k1gMnSc1	betl
je	být	k5eAaImIp3nS	být
ložený	ložený	k2eAgMnSc1d1	ložený
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
aktér	aktér	k1gMnSc1	aktér
nemůže	moct	k5eNaImIp3nS	moct
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nS	muset
vzít	vzít	k5eAaPmF	vzít
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
zdvih	zdvih	k1gInSc4	zdvih
<g />
.	.	kIx.	.
</s>
<s>
durch	durch	k1gInSc1	durch
je	být	k5eAaImIp3nS	být
ložený	ložený	k2eAgInSc1d1	ložený
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
aktér	aktér	k1gMnSc1	aktér
nemůže	moct	k5eNaImIp3nS	moct
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nS	muset
ztratit	ztratit	k5eAaPmF	ztratit
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
zdvih	zdvih	k1gInSc4	zdvih
dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
jsou	být	k5eAaImIp3nP	být
ložené	ložený	k2eAgFnPc1d1	ložená
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gFnPc4	on
aktér	aktér	k1gMnSc1	aktér
nemůže	moct	k5eNaImIp3nS	moct
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nS	muset
prohrát	prohrát	k5eAaPmF	prohrát
ani	ani	k9	ani
proti	proti	k7c3	proti
plné	plný	k2eAgFnSc3d1	plná
sloze	sloha	k1gFnSc3	sloha
trumfů	trumf	k1gInPc2	trumf
a	a	k8xC	a
strkajících	strkající	k2eAgInPc2d1	strkající
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ruce	ruka	k1gFnSc6	ruka
obrany	obrana	k1gFnSc2	obrana
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rozlohu	rozloha	k1gFnSc4	rozloha
karet	kareta	k1gFnPc2	kareta
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
barvách	barva	k1gFnPc6	barva
<g/>
)	)	kIx)	)
Např.	např.	kA	např.
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
základní	základní	k2eAgFnSc1d1	základní
sazba	sazba	k1gFnSc1	sazba
za	za	k7c4	za
betla	betl	k1gMnSc4	betl
3	[number]	k4	3
Kč	Kč	kA	Kč
a	a	k8xC	a
padne	padnout	k5eAaPmIp3nS	padnout
Flek	flek	k1gInSc1	flek
<g/>
/	/	kIx~	/
<g/>
RE	re	k9	re
na	na	k7c4	na
loženou	ložený	k2eAgFnSc4d1	ložená
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
hradí	hradit	k5eAaImIp3nP	hradit
poškození	poškozený	k2eAgMnPc1d1	poškozený
spoluhráči	spoluhráč	k1gMnPc1	spoluhráč
jen	jen	k6eAd1	jen
základní	základní	k2eAgFnSc4d1	základní
sazbu	sazba	k1gFnSc4	sazba
3	[number]	k4	3
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnSc1d1	zbývající
část	část	k1gFnSc1	část
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
ceny	cena	k1gFnSc2	cena
hry	hra	k1gFnSc2	hra
po	po	k7c6	po
flekování	flekování	k?	flekování
hradí	hradit	k5eAaImIp3nS	hradit
flekující	flekující	k2eAgMnSc1d1	flekující
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
bylo	být	k5eAaImAgNnS	být
21	[number]	k4	21
Kč	Kč	kA	Kč
a	a	k8xC	a
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
30	[number]	k4	30
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Které	který	k3yIgFnPc1	který
hry	hra	k1gFnPc1	hra
se	se	k3xPyFc4	se
nehrají	hrát	k5eNaImIp3nP	hrát
složené	složený	k2eAgFnSc2d1	složená
hry	hra	k1gFnSc2	hra
hra	hra	k1gFnSc1	hra
bez	bez	k7c2	bez
"	"	kIx"	"
<g/>
re	re	k9	re
<g/>
"	"	kIx"	"
sedma	sedma	k1gFnSc1	sedma
bez	bez	k7c2	bez
"	"	kIx"	"
<g/>
fleku	flek	k1gInSc2	flek
na	na	k7c4	na
sedmu	sedma	k1gFnSc4	sedma
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
"	"	kIx"	"
<g/>
re	re	k9	re
na	na	k7c4	na
hru	hra	k1gFnSc4	hra
<g/>
"	"	kIx"	"
ložené	ložený	k2eAgFnPc4d1	ložená
hry	hra	k1gFnPc4	hra
"	"	kIx"	"
<g/>
dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
i	i	k9	i
lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
)	)	kIx)	)
při	při	k7c6	při
čtyřech	čtyři	k4xCgFnPc6	čtyři
a	a	k8xC	a
více	hodně	k6eAd2	hodně
tlačících	tlačící	k2eAgFnPc6d1	tlačící
kartách	karta	k1gFnPc6	karta
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
protihráčů	protihráč	k1gMnPc2	protihráč
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
a	a	k8xC	a
více	hodně	k6eAd2	hodně
tlačných	tlačný	k2eAgInPc2d1	tlačný
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ruce	ruka	k1gFnSc6	ruka
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
"	"	kIx"	"
<g/>
sto	sto	k4xCgNnSc1	sto
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
závazek	závazek	k1gInSc1	závazek
"	"	kIx"	"
<g/>
dvě	dva	k4xCgFnPc1	dva
sedmy	sedma	k1gFnPc1	sedma
<g/>
"	"	kIx"	"
aktér	aktér	k1gMnSc1	aktér
prohrává	prohrávat	k5eAaImIp3nS	prohrávat
automaticky	automaticky	k6eAd1	automaticky
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
obrana	obrana	k1gFnSc1	obrana
během	během	k7c2	během
sehrávky	sehrávka	k1gFnSc2	sehrávka
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
tlačných	tlačný	k2eAgInPc2d1	tlačný
odhodila	odhodit	k5eAaPmAgNnP	odhodit
<g/>
.	.	kIx.	.
</s>
<s>
Renonc	renonc	k1gInSc1	renonc
Tento	tento	k3xDgInSc4	tento
výraz	výraz	k1gInSc4	výraz
označuje	označovat	k5eAaImIp3nS	označovat
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
dopustil	dopustit	k5eAaPmAgInS	dopustit
jednání	jednání	k1gNnSc4	jednání
proti	proti	k7c3	proti
pravidlům	pravidlo	k1gNnPc3	pravidlo
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nepřiznal	přiznat	k5eNaPmAgInS	přiznat
barvu	barva	k1gFnSc4	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
oslavil	oslavit	k5eAaPmAgInS	oslavit
výročí	výročí	k1gNnSc4	výročí
20	[number]	k4	20
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
mariáše	mariáš	k1gInSc2	mariáš
(	(	kIx(	(
<g/>
ČSM	ČSM	kA	ČSM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
organizovaného	organizovaný	k2eAgNnSc2d1	organizované
soutěžení	soutěžení	k1gNnSc2	soutěžení
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
známých	známý	k2eAgInPc6d1	známý
druzích	druh	k1gInPc6	druh
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
zejména	zejména	k6eAd1	zejména
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
forma	forma	k1gFnSc1	forma
licitovaného	licitovaný	k2eAgInSc2d1	licitovaný
mariáše	mariáš	k1gInSc2	mariáš
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
právě	právě	k6eAd1	právě
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
ČSM	ČSM	kA	ČSM
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
soutěžního	soutěžní	k2eAgInSc2d1	soutěžní
mariáše	mariáš	k1gInSc2	mariáš
je	být	k5eAaImIp3nS	být
kladení	kladení	k1gNnSc1	kladení
důrazu	důraz	k1gInSc2	důraz
na	na	k7c4	na
pestrost	pestrost	k1gFnSc4	pestrost
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
hraní	hraní	k1gNnSc4	hraní
her	hra	k1gFnPc2	hra
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
stupněm	stupeň	k1gInSc7	stupeň
rizika	riziko	k1gNnSc2	riziko
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
hráč	hráč	k1gMnSc1	hráč
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
peněžního	peněžní	k2eAgInSc2d1	peněžní
zisku	zisk	k1gInSc2	zisk
odměněn	odměnit	k5eAaPmNgInS	odměnit
též	též	k9	též
prémiovými	prémiový	k2eAgInPc7d1	prémiový
body	bod	k1gInPc7	bod
dle	dle	k7c2	dle
tabulky	tabulka	k1gFnSc2	tabulka
platné	platný	k2eAgFnSc2d1	platná
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
hráče	hráč	k1gMnSc2	hráč
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
tvořen	tvořit	k5eAaImNgInS	tvořit
součtem	součet	k1gInSc7	součet
vyhraných	vyhraná	k1gFnPc2	vyhraná
/	/	kIx~	/
prohraných	prohraný	k2eAgInPc2d1	prohraný
peněz	peníze	k1gInPc2	peníze
(	(	kIx(	(
<g/>
ziskové	ziskový	k2eAgInPc1d1	ziskový
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
ZB	ZB	kA	ZB
<g/>
)	)	kIx)	)
a	a	k8xC	a
získaných	získaný	k2eAgInPc2d1	získaný
prémiových	prémiový	k2eAgInPc2d1	prémiový
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
PB	PB	kA	PB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bodovací	bodovací	k2eAgFnSc1d1	bodovací
tabulka	tabulka	k1gFnSc1	tabulka
ČSM	ČSM	kA	ČSM
je	být	k5eAaImIp3nS	být
vyhlašována	vyhlašován	k2eAgFnSc1d1	vyhlašována
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
pro	pro	k7c4	pro
mariáš	mariáš	k1gInSc4	mariáš
se	s	k7c7	s
základní	základní	k2eAgFnSc7d1	základní
sazbou	sazba	k1gFnSc7	sazba
20	[number]	k4	20
haléřů	haléř	k1gInPc2	haléř
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jiné	jiné	k1gNnSc4	jiné
základní	základní	k2eAgFnSc2d1	základní
sazby	sazba	k1gFnSc2	sazba
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
peníze	peníz	k1gInPc4	peníz
získané	získaný	k2eAgInPc4d1	získaný
za	za	k7c4	za
body	bod	k1gInPc4	bod
musejí	muset	k5eAaImIp3nP	muset
adekvátně	adekvátně	k6eAd1	adekvátně
přepočítat	přepočítat	k5eAaPmF	přepočítat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vynásobit	vynásobit	k5eAaPmF	vynásobit
sazbu	sazba	k1gFnSc4	sazba
za	za	k7c4	za
1	[number]	k4	1
PB	PB	kA	PB
pěti	pět	k4xCc2	pět
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
mariáš	mariáš	k1gInSc1	mariáš
korunový	korunový	k2eAgInSc1d1	korunový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
na	na	k7c4	na
prémiové	prémiový	k2eAgInPc4d1	prémiový
body	bod	k1gInPc4	bod
mimo	mimo	k7c4	mimo
turnaj	turnaj	k1gInSc4	turnaj
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
hráči	hráč	k1gMnPc1	hráč
domluví	domluvit	k5eAaPmIp3nP	domluvit
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
proplácení	proplácení	k1gNnSc4	proplácení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacetihaléřovém	dvacetihaléřový	k2eAgInSc6d1	dvacetihaléřový
mariáši	mariáš	k1gInSc6	mariáš
je	být	k5eAaImIp3nS	být
adekvátní	adekvátní	k2eAgFnSc7d1	adekvátní
sazbou	sazba	k1gFnSc7	sazba
50	[number]	k4	50
haléřů	haléř	k1gInPc2	haléř
za	za	k7c4	za
1	[number]	k4	1
získaný	získaný	k2eAgInSc4d1	získaný
PB	PB	kA	PB
<g/>
.	.	kIx.	.
</s>
