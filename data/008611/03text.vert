<p>
<s>
ISO	ISO	kA	ISO
4217	[number]	k4	4217
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
standard	standard	k1gInSc4	standard
pro	pro	k7c4	pro
označování	označování	k1gNnSc4	označování
měn	měna	k1gFnPc2	měna
pomocí	pomocí	k7c2	pomocí
tříznakových	tříznakový	k2eAgInPc2d1	tříznakový
(	(	kIx(	(
<g/>
písmenných	písmenný	k2eAgInPc2d1	písmenný
a	a	k8xC	a
číselných	číselný	k2eAgInPc2d1	číselný
<g/>
)	)	kIx)	)
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
definovaný	definovaný	k2eAgInSc1d1	definovaný
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
organizací	organizace	k1gFnSc7	organizace
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
(	(	kIx(	(
<g/>
ISO	ISO	kA	ISO
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
jako	jako	k9	jako
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
4217	[number]	k4	4217
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kódy	kód	k1gInPc1	kód
pro	pro	k7c4	pro
měny	měna	k1gFnPc4	měna
a	a	k8xC	a
fondy	fond	k1gInPc4	fond
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norma	Norma	k1gFnSc1	Norma
definuje	definovat	k5eAaBmIp3nS	definovat
dva	dva	k4xCgInPc4	dva
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
kódy	kód	k1gInPc4	kód
<g/>
:	:	kIx,	:
třípísmenný	třípísmenný	k2eAgInSc4d1	třípísmenný
abecední	abecední	k2eAgInSc4d1	abecední
a	a	k8xC	a
třímístný	třímístný	k2eAgInSc4d1	třímístný
číslicový	číslicový	k2eAgInSc4d1	číslicový
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
normě	norma	k1gFnSc6	norma
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
tři	tři	k4xCgFnPc1	tři
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kódů	kód	k1gInPc2	kód
měn	měna	k1gFnPc2	měna
a	a	k8xC	a
fondů	fond	k1gInPc2	fond
–	–	k?	–
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
měnu	měna	k1gFnSc4	měna
či	či	k8xC	či
fond	fond	k1gInSc4	fond
(	(	kIx(	(
<g/>
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
v	v	k7c6	v
normě	norma	k1gFnSc6	norma
míní	mínit	k5eAaImIp3nS	mínit
další	další	k2eAgInPc4d1	další
peněžní	peněžní	k2eAgInPc4d1	peněžní
zdroje	zdroj	k1gInPc4	zdroj
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
např.	např.	kA	např.
frank	frank	k1gInSc1	frank
WIR	WIR	kA	WIR
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jejich	jejich	k3xOp3gInSc4	jejich
abecední	abecední	k2eAgInSc4d1	abecední
a	a	k8xC	a
číslicový	číslicový	k2eAgInSc4d1	číslicový
kód	kód	k1gInSc4	kód
a	a	k8xC	a
informaci	informace	k1gFnSc4	informace
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
měnové	měnový	k2eAgFnSc2d1	měnová
jednotky	jednotka	k1gFnSc2	jednotka
na	na	k7c4	na
dílčí	dílčí	k2eAgFnPc4d1	dílčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kódy	kód	k1gInPc1	kód
fondů	fond	k1gInPc2	fond
registrované	registrovaný	k2eAgFnSc2d1	registrovaná
MA	MA	kA	MA
(	(	kIx(	(
<g/>
Udržovací	udržovací	k2eAgFnSc7d1	udržovací
agenturou	agentura	k1gFnSc7	agentura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kódy	kód	k1gInPc1	kód
historických	historický	k2eAgFnPc2d1	historická
denominací	denominace	k1gFnPc2	denominace
měn	měna	k1gFnPc2	měna
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
již	již	k6eAd1	již
neužívané	užívaný	k2eNgFnSc2d1	neužívaná
historické	historický	k2eAgFnSc2d1	historická
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jejich	jejich	k3xOp3gFnSc6	jejich
denominace	denominace	k1gFnSc1	denominace
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
abecedního	abecední	k2eAgInSc2d1	abecední
kódu	kód	k1gInSc2	kód
tvoří	tvořit	k5eAaImIp3nP	tvořit
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
písmena	písmeno	k1gNnPc4	písmeno
označení	označení	k1gNnSc2	označení
země	zem	k1gFnSc2	zem
podle	podle	k7c2	podle
kódu	kód	k1gInSc2	kód
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
alpha-	alpha-	k?	alpha-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgNnSc4	třetí
písmeno	písmeno	k1gNnSc4	písmeno
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
mnemotechnicky	mnemotechnicky	k6eAd1	mnemotechnicky
podle	podle	k7c2	podle
názvu	název	k1gInSc2	název
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jako	jako	k8xC	jako
první	první	k4xOgNnSc4	první
písmeno	písmeno	k1gNnSc4	písmeno
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
měn	měna	k1gFnPc2	měna
a	a	k8xC	a
fondů	fond	k1gInPc2	fond
entit	entita	k1gFnPc2	entita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
ISO	ISO	kA	ISO
3166	[number]	k4	3166
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgNnPc4	první
dvě	dva	k4xCgNnPc4	dva
písmena	písmeno	k1gNnPc4	písmeno
používá	používat	k5eAaImIp3nS	používat
rozsah	rozsah	k1gInSc4	rozsah
XA	XA	kA	XA
<g/>
–	–	k?	–
<g/>
XZ	XZ	kA	XZ
(	(	kIx(	(
<g/>
v	v	k7c6	v
ISO	ISO	kA	ISO
3166-1	[number]	k4	3166-1
vyhrazený	vyhrazený	k2eAgInSc4d1	vyhrazený
pro	pro	k7c4	pro
kódy	kód	k1gInPc4	kód
určované	určovaný	k2eAgInPc4d1	určovaný
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
volby	volba	k1gFnSc2	volba
Udržovací	udržovací	k2eAgFnSc2d1	udržovací
agentury	agentura	k1gFnSc2	agentura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číslicový	číslicový	k2eAgInSc1d1	číslicový
kód	kód	k1gInSc1	kód
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
z	z	k7c2	z
kódu	kód	k1gInSc2	kód
země	zem	k1gFnSc2	zem
či	či	k8xC	či
regionu	region	k1gInSc2	region
podle	podle	k7c2	podle
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
kódy	kód	k1gInPc1	kód
se	se	k3xPyFc4	se
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
z	z	k7c2	z
intervalu	interval	k1gInSc2	interval
950	[number]	k4	950
<g/>
–	–	k?	–
<g/>
998	[number]	k4	998
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kódy	kód	k1gInPc1	kód
fondů	fond	k1gInPc2	fond
se	se	k3xPyFc4	se
přidělují	přidělovat	k5eAaImIp3nP	přidělovat
sestupně	sestupně	k6eAd1	sestupně
od	od	k7c2	od
998	[number]	k4	998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
česká	český	k2eAgFnSc1d1	Česká
koruna	koruna	k1gFnSc1	koruna
má	mít	k5eAaImIp3nS	mít
kódy	kód	k1gInPc4	kód
CZK	CZK	kA	CZK
a	a	k8xC	a
203	[number]	k4	203
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc1d1	americký
dolar	dolar	k1gInSc1	dolar
USD	USD	kA	USD
a	a	k8xC	a
840	[number]	k4	840
<g/>
,	,	kIx,	,
euro	euro	k1gNnSc1	euro
EUR	euro	k1gNnPc2	euro
a	a	k8xC	a
978	[number]	k4	978
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgInSc2	ten
např.	např.	kA	např.
zlato	zlato	k1gNnSc1	zlato
má	mít	k5eAaImIp3nS	mít
určeny	určen	k2eAgInPc4d1	určen
kódy	kód	k1gInPc4	kód
XAU	XAU	kA	XAU
a	a	k8xC	a
959	[number]	k4	959
<g/>
.	.	kIx.	.
</s>
<s>
Kódy	kód	k1gInPc1	kód
XTS	XTS	kA	XTS
a	a	k8xC	a
963	[number]	k4	963
jsou	být	k5eAaImIp3nP	být
určeny	určen	k2eAgInPc1d1	určen
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
a	a	k8xC	a
kódy	kód	k1gInPc1	kód
XXX	XXX	kA	XXX
a	a	k8xC	a
999	[number]	k4	999
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
transakce	transakce	k1gFnSc2	transakce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nepoužívají	používat	k5eNaImIp3nP	používat
měny	měna	k1gFnSc2	měna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednou	jednou	k6eAd1	jednou
přiřazené	přiřazený	k2eAgInPc4d1	přiřazený
abecední	abecední	k2eAgInPc4d1	abecední
kódy	kód	k1gInPc4	kód
již	již	k6eAd1	již
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
měna	měna	k1gFnSc1	měna
redenominována	redenominován	k2eAgFnSc1d1	redenominován
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
nový	nový	k2eAgInSc4d1	nový
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
např.	např.	kA	např.
ruský	ruský	k2eAgInSc1d1	ruský
rubl	rubl	k1gInSc1	rubl
měl	mít	k5eAaImAgInS	mít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
kód	kód	k1gInSc4	kód
RUR	RUR	kA	RUR
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
reformy	reforma	k1gFnSc2	reforma
používá	používat	k5eAaImIp3nS	používat
stávající	stávající	k2eAgInSc1d1	stávající
rubl	rubl	k1gInSc1	rubl
kód	kód	k1gInSc1	kód
RUB	rub	k1gInSc1	rub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
měn	měna	k1gFnPc2	měna
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
kódů	kód	k1gInPc2	kód
měn	měna	k1gFnPc2	měna
a	a	k8xC	a
fondů	fond	k1gInPc2	fond
na	na	k7c6	na
webu	web	k1gInSc6	web
Udržovací	udržovací	k2eAgFnSc2d1	udržovací
agentury	agentura	k1gFnSc2	agentura
</s>
</p>
<p>
<s>
Náhled	náhled	k1gInSc1	náhled
normy	norma	k1gFnSc2	norma
ČSN	ČSN	kA	ČSN
ISO	ISO	kA	ISO
4217	[number]	k4	4217
</s>
</p>
