<p>
<s>
Denfert-Rochereau	Denfert-Rochereau	k6eAd1	Denfert-Rochereau
je	být	k5eAaImIp3nS	být
přestupní	přestupní	k2eAgFnSc1d1	přestupní
stanice	stanice	k1gFnSc1	stanice
pařížského	pařížský	k2eAgNnSc2d1	pařížské
metra	metro	k1gNnSc2	metro
mezi	mezi	k7c7	mezi
linkami	linka	k1gFnPc7	linka
4	[number]	k4	4
a	a	k8xC	a
6	[number]	k4	6
a	a	k8xC	a
linkou	linka	k1gFnSc7	linka
RER	RER	kA	RER
B.	B.	kA	B.
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
obvodu	obvod	k1gInSc2	obvod
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
pod	pod	k7c7	pod
náměstím	náměstí	k1gNnSc7	náměstí
Place	plac	k1gInSc6	plac
Denfert-Rochereau	Denfert-Rochereaus	k1gInSc6	Denfert-Rochereaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
při	při	k7c6	při
zprovoznění	zprovoznění	k1gNnSc6	zprovoznění
úseku	úsek	k1gInSc2	úsek
Passy	Passa	k1gFnSc2	Passa
↔	↔	k?	↔
Place	plac	k1gInSc6	plac
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italie	Italie	k1gFnSc2	Italie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
oddělil	oddělit	k5eAaPmAgMnS	oddělit
od	od	k7c2	od
linky	linka	k1gFnSc2	linka
1	[number]	k4	1
úsek	úsek	k1gInSc1	úsek
počínaje	počínaje	k7c7	počínaje
stanicí	stanice	k1gFnSc7	stanice
Étoile	Étoila	k1gFnSc3	Étoila
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
linka	linka	k1gFnSc1	linka
2	[number]	k4	2
Sud	suda	k1gFnPc2	suda
(	(	kIx(	(
<g/>
2	[number]	k4	2
Jih	jih	k1gInSc1	jih
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Circulaire	Circulair	k1gInSc5	Circulair
Sud	sud	k1gInSc1	sud
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgInSc1d1	jižní
okruh	okruh	k1gInSc1	okruh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
linka	linka	k1gFnSc1	linka
byla	být	k5eAaImAgFnS	být
právě	právě	k6eAd1	právě
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
Passy	Passa	k1gFnSc2	Passa
po	po	k7c6	po
Place	plac	k1gInSc6	plac
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italie	Italie	k1gFnSc1	Italie
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1907	[number]	k4	1907
byla	být	k5eAaImAgFnS	být
linka	linka	k1gFnSc1	linka
2	[number]	k4	2
Sud	suda	k1gFnPc2	suda
zrušena	zrušen	k2eAgFnSc1d1	zrušena
a	a	k8xC	a
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
lince	linka	k1gFnSc3	linka
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
byla	být	k5eAaImAgFnS	být
stanice	stanice	k1gFnSc1	stanice
Raspail	Raspaila	k1gFnPc2	Raspaila
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
celá	celý	k2eAgFnSc1d1	celá
část	část	k1gFnSc1	část
Étoile	Étoila	k1gFnSc3	Étoila
↔	↔	k?	↔
Place	plac	k1gInSc6	plac
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Italie	Italie	k1gFnSc1	Italie
opět	opět	k6eAd1	opět
odpojena	odpojit	k5eAaPmNgFnS	odpojit
od	od	k7c2	od
linky	linka	k1gFnSc2	linka
5	[number]	k4	5
a	a	k8xC	a
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
linkou	linka	k1gFnSc7	linka
6	[number]	k4	6
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Linka	linka	k1gFnSc1	linka
4	[number]	k4	4
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
napojena	napojen	k2eAgFnSc1d1	napojena
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
zprovozněn	zprovozněn	k2eAgInSc1d1	zprovozněn
jižní	jižní	k2eAgInSc1d1	jižní
úsek	úsek	k1gInSc1	úsek
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Raspail	Raspaila	k1gFnPc2	Raspaila
do	do	k7c2	do
Porte	port	k1gInSc5	port
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Orléans	Orléans	k1gInSc1	Orléans
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1977	[number]	k4	1977
byl	být	k5eAaImAgInS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
přestup	přestup	k1gInSc1	přestup
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
RER	RER	kA	RER
B	B	kA	B
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
stanici	stanice	k1gFnSc6	stanice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
linka	linka	k1gFnSc1	linka
6	[number]	k4	6
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
hloubce	hloubka	k1gFnSc6	hloubka
než	než	k8xS	než
linka	linka	k1gFnSc1	linka
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Nástupiště	nástupiště	k1gNnSc1	nástupiště
linky	linka	k1gFnSc2	linka
6	[number]	k4	6
jsou	být	k5eAaImIp3nP	být
vyzdobeny	vyzdoben	k2eAgInPc1d1	vyzdoben
původními	původní	k2eAgFnPc7d1	původní
oranžovými	oranžový	k2eAgFnPc7d1	oranžová
dlaždicemi	dlaždice	k1gFnPc7	dlaždice
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
linky	linka	k1gFnSc2	linka
4	[number]	k4	4
jsou	být	k5eAaImIp3nP	být
obklady	obklad	k1gInPc1	obklad
modré	modrý	k2eAgFnSc2d1	modrá
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
stanice	stanice	k1gFnSc2	stanice
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
názvu	název	k1gInSc2	název
náměstí	náměstí	k1gNnSc2	náměstí
Place	plac	k1gInSc6	plac
Denfert-Rochereau	Denfert-Rochereaus	k1gInSc6	Denfert-Rochereaus
<g/>
.	.	kIx.	.
</s>
<s>
Pierre	Pierr	k1gMnSc5	Pierr
Philippe	Philipp	k1gMnSc5	Philipp
Denfert-Rochereau	Denfert-Rocherea	k1gMnSc6	Denfert-Rocherea
(	(	kIx(	(
<g/>
1823	[number]	k4	1823
<g/>
–	–	k?	–
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
plukovník	plukovník	k1gMnSc1	plukovník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
obhájil	obhájit	k5eAaPmAgMnS	obhájit
město	město	k1gNnSc4	město
Belfort	Belfort	k1gInSc1	Belfort
proti	proti	k7c3	proti
Prusům	Prus	k1gMnPc3	Prus
během	během	k7c2	během
Prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
osvobození	osvobození	k1gNnSc2	osvobození
Paříže	Paříž	k1gFnSc2	Paříž
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
stanice	stanice	k1gFnSc2	stanice
doplněn	doplnit	k5eAaPmNgInS	doplnit
ještě	ještě	k9	ještě
podnázvem	podnázev	k1gInSc7	podnázev
psaným	psaný	k2eAgNnSc7d1	psané
malým	malý	k2eAgNnSc7d1	malé
písmem	písmo	k1gNnSc7	písmo
<g/>
:	:	kIx,	:
Colonel	Colonel	k1gInSc1	Colonel
Rol-Tanguy	Rol-Tangua	k1gFnSc2	Rol-Tangua
neboli	neboli	k8xC	neboli
plukovník	plukovník	k1gMnSc1	plukovník
Rol-Tanguy	Rol-Tangua	k1gFnSc2	Rol-Tangua
podle	podle	k7c2	podle
Avenue	avenue	k1gFnSc2	avenue
du	du	k?	du
Colonel	Colonel	k1gMnSc1	Colonel
Henri	Henr	k1gFnSc2	Henr
Rol-Tanguy	Rol-Tangua	k1gFnSc2	Rol-Tangua
<g/>
.	.	kIx.	.
</s>
<s>
Plukovník	plukovník	k1gMnSc1	plukovník
Henri	Henr	k1gFnSc2	Henr
Rol-Tanguy	Rol-Tangua	k1gFnSc2	Rol-Tangua
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
velitel	velitel	k1gMnSc1	velitel
francouzského	francouzský	k2eAgInSc2d1	francouzský
odboje	odboj	k1gInSc2	odboj
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
podnázev	podnázev	k1gInSc1	podnázev
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
informačních	informační	k2eAgFnPc6d1	informační
tabulích	tabule	k1gFnPc6	tabule
linky	linka	k1gFnSc2	linka
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vstupy	vstup	k1gInPc4	vstup
==	==	k?	==
</s>
</p>
<p>
<s>
Stanice	stanice	k1gFnSc1	stanice
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
východů	východ	k1gInPc2	východ
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Avenue	avenue	k1gFnSc1	avenue
du	du	k?	du
Colonel	Colonel	k1gInSc1	Colonel
Henri	Henr	k1gFnSc2	Henr
Rol-Tanguy	Rol-Tangua	k1gFnSc2	Rol-Tangua
u	u	k7c2	u
domu	dům	k1gInSc2	dům
č.	č.	k?	č.
2	[number]	k4	2
</s>
</p>
<p>
<s>
Avenue	avenue	k1gFnSc1	avenue
du	du	k?	du
Général	Général	k1gFnSc1	Général
Leclerc	Leclerc	k1gFnSc1	Leclerc
u	u	k7c2	u
domů	dům	k1gInPc2	dům
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
a	a	k8xC	a
4	[number]	k4	4
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
Place	plac	k1gInSc5	plac
Denfert-Rochereau	Denfert-Rocherea	k1gMnSc6	Denfert-Rocherea
</s>
</p>
<p>
<s>
Replika	replika	k1gFnSc1	replika
sochy	socha	k1gFnSc2	socha
Lion	Lion	k1gMnSc1	Lion
de	de	k?	de
Belfort	Belfort	k1gInSc1	Belfort
(	(	kIx(	(
<g/>
Lev	lev	k1gInSc1	lev
z	z	k7c2	z
Belfortu	Belfort	k1gInSc2	Belfort
<g/>
)	)	kIx)	)
uprostřed	uprostřed	k7c2	uprostřed
náměstí	náměstí	k1gNnSc2	náměstí
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
obranu	obrana	k1gFnSc4	obrana
plukovníka	plukovník	k1gMnSc4	plukovník
Denfert-Rochereau	Denfert-Rochereaa	k1gMnSc4	Denfert-Rochereaa
</s>
</p>
<p>
<s>
Pařížské	pařížský	k2eAgFnPc1d1	Pařížská
katakomby	katakomby	k1gFnPc1	katakomby
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Denfert-Rochereau	Denfert-Rochereaus	k1gInSc2	Denfert-Rochereaus
(	(	kIx(	(
<g/>
métro	métro	k6eAd1	métro
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Denfert-Rochereau	Denfert-Rochereaus	k1gInSc2	Denfert-Rochereaus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Záznam	záznam	k1gInSc1	záznam
v	v	k7c6	v
evidenci	evidence	k1gFnSc6	evidence
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
</s>
</p>
