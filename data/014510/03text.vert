<s>
Stoicismus	stoicismus	k1gInSc1
</s>
<s>
Zénón	Zénón	k1gMnSc1
z	z	k7c2
Kitia	Kitium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foto	foto	k1gNnSc1
Paolo	Paolo	k1gNnSc1
Monti	Monť	k1gFnSc2
<g/>
,	,	kIx,
1969	#num#	k4
</s>
<s>
Stoicismus	stoicismus	k1gInSc1
je	být	k5eAaImIp3nS
filosofický	filosofický	k2eAgInSc1d1
směr	směr	k1gInSc1
<g/>
,	,	kIx,
založený	založený	k2eAgMnSc1d1
oficiálně	oficiálně	k6eAd1
Zénonem	Zénon	k1gInSc7
z	z	k7c2
Kitia	Kitium	k1gNnSc2
počátkem	počátek	k1gInSc7
3	#num#	k4
<g/>
.	.	kIx.
st.	st.	kA
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
nejdůležitější	důležitý	k2eAgInSc4d3
a	a	k8xC
nejvlivnější	vlivný	k2eAgInSc4d3
směr	směr	k1gInSc4
helénistické	helénistický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
hlásilo	hlásit	k5eAaImAgNnS
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gMnSc3
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
vzdělaných	vzdělaný	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
z	z	k7c2
Řecka	Řecko	k1gNnSc2
i	i	k8xC
Říma	Řím	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Období	období	k1gNnSc1
stoicismu	stoicismus	k1gInSc2
trvalo	trvat	k5eAaImAgNnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
529	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
císař	císař	k1gMnSc1
Justinián	Justinián	k1gMnSc1
I.	I.	kA
nechal	nechat	k5eAaPmAgMnS
zavřít	zavřít	k5eAaPmF
všechny	všechen	k3xTgFnPc4
filosofické	filosofický	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Stoicismus	stoicismus	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
zvláště	zvláště	k6eAd1
z	z	k7c2
kořenů	kořen	k1gInPc2
učení	učení	k1gNnSc2
kynického	kynický	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoikové	stoik	k1gMnPc1
se	se	k3xPyFc4
zabývali	zabývat	k5eAaImAgMnP
aktivním	aktivní	k2eAgInSc7d1
vztahem	vztah	k1gInSc7
mezi	mezi	k7c7
kosmickým	kosmický	k2eAgInSc7d1
determinismem	determinismus	k1gInSc7
a	a	k8xC
lidskou	lidský	k2eAgFnSc7d1
svobodou	svoboda	k1gFnSc7
<g/>
,	,	kIx,
za	za	k7c4
cestu	cesta	k1gFnSc4
ke	k	k7c3
svobodě	svoboda	k1gFnSc3
považovali	považovat	k5eAaImAgMnP
život	život	k1gInSc4
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
přírodou	příroda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učení	učení	k1gNnPc2
stoiků	stoik	k1gMnPc2
poznamenalo	poznamenat	k5eAaPmAgNnS
i	i	k9
pozdější	pozdní	k2eAgFnSc4d2
filosofii	filosofie	k1gFnSc4
<g/>
,	,	kIx,
vliv	vliv	k1gInSc4
stoicismu	stoicismus	k1gInSc2
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
jak	jak	k8xS,k8xC
u	u	k7c2
křesťanských	křesťanský	k2eAgMnPc2d1
otců	otec	k1gMnPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
v	v	k7c6
dílech	dílo	k1gNnPc6
renesančních	renesanční	k2eAgMnPc2d1
a	a	k8xC
novověkých	novověký	k2eAgMnPc2d1
myslitelů	myslitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dějinné	dějinný	k2eAgFnPc1d1
okolnosti	okolnost	k1gFnPc1
</s>
<s>
Z	z	k7c2
filosofických	filosofický	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
či	či	k8xC
působily	působit	k5eAaImAgFnP
v	v	k7c6
době	doba	k1gFnSc6
helénistické	helénistický	k2eAgFnSc6d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
škola	škola	k1gFnSc1
nejdelší	dlouhý	k2eAgFnSc4d3
životnost	životnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
zvláštní	zvláštní	k2eAgInSc1d1
ráz	ráz	k1gInSc1
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgInS
právě	právě	k9
dobou	doba	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
působí	působit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působí	působit	k5eAaImIp3nS
totiž	totiž	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Řekové	Řek	k1gMnPc1
už	už	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
samostatnost	samostatnost	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c4
místo	místo	k1gNnSc4
drobných	drobný	k2eAgInPc2d1
městských	městský	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
polis	polis	k1gFnPc2
<g/>
)	)	kIx)
nastoupily	nastoupit	k5eAaPmAgFnP
větší	veliký	k2eAgInPc4d2
státní	státní	k2eAgInPc4d1
celky	celek	k1gInPc4
<g/>
,	,	kIx,
monarchie	monarchie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
občan	občan	k1gMnSc1
už	už	k6eAd1
nemohl	moct	k5eNaImAgMnS
účastnit	účastnit	k5eAaImF
veřejného	veřejný	k2eAgInSc2d1
života	život	k1gInSc2
jako	jako	k8xC,k8xS
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemožnost	nemožnost	k1gFnSc4
uplatnit	uplatnit	k5eAaPmF
se	se	k3xPyFc4
v	v	k7c6
politickém	politický	k2eAgInSc6d1
životě	život	k1gInSc6
vedla	vést	k5eAaImAgFnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
člověk	člověk	k1gMnSc1
se	se	k3xPyFc4
zaměřoval	zaměřovat	k5eAaImAgMnS
více	hodně	k6eAd2
na	na	k7c4
sebe	sebe	k3xPyFc4
samého	samý	k3xTgNnSc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
místo	místo	k7c2
etiky	etika	k1gFnSc2
sociální	sociální	k2eAgInSc4d1
stále	stále	k6eAd1
více	hodně	k6eAd2
hlásila	hlásit	k5eAaImAgFnS
ke	k	k7c3
slovu	slovo	k1gNnSc3
etika	etika	k1gFnSc1
individuální	individuální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
význačným	význačný	k2eAgInSc7d1
znakem	znak	k1gInSc7
stoicismu	stoicismus	k1gInSc2
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
systematičnost	systematičnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoičtí	stoický	k2eAgMnPc1d1
myslitelé	myslitel	k1gMnPc1
usilují	usilovat	k5eAaImIp3nP
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
jednotlivé	jednotlivý	k2eAgFnSc2d1
myšlenkové	myšlenkový	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
vše	všechen	k3xTgNnSc4
do	do	k7c2
sebe	sebe	k3xPyFc4
navzájem	navzájem	k6eAd1
zapadalo	zapadat	k5eAaImAgNnS,k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rys	rys	k1gInSc1
se	se	k3xPyFc4
uplatnil	uplatnit	k5eAaPmAgInS
také	také	k9
v	v	k7c6
pevné	pevný	k2eAgFnSc6d1
organizovanosti	organizovanost	k1gFnSc6
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
kladla	klást	k5eAaImAgFnS
velký	velký	k2eAgInSc4d1
důraz	důraz	k1gInSc4
i	i	k9
na	na	k7c4
udržování	udržování	k1gNnSc4
své	svůj	k3xOyFgFnSc2
filosofické	filosofický	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šíře	šíře	k1gFnSc1
mohla	moct	k5eAaImAgFnS
stoa	stoa	k1gFnSc1
ve	v	k7c6
světě	svět	k1gInSc6
působit	působit	k5eAaImF
rovněž	rovněž	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
zmizela	zmizet	k5eAaPmAgNnP
uzavřenost	uzavřenost	k1gFnSc4
malých	malý	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
cítit	cítit	k5eAaImF
součástí	součást	k1gFnSc7
větších	veliký	k2eAgInPc2d2
celků	celek	k1gInPc2
<g/>
,	,	kIx,
ba	ba	k9
celého	celý	k2eAgNnSc2d1
lidstva	lidstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
místo	místo	k1gNnSc4
polis	polis	k1gFnSc2
nastupuje	nastupovat	k5eAaImIp3nS
představa	představa	k1gFnSc1
jakési	jakýsi	k3yIgFnSc2
kosmopolis	kosmopolis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
šíří	šířit	k5eAaImIp3nS
se	se	k3xPyFc4
sklon	sklon	k1gInSc1
ke	k	k7c3
kosmopolitismu	kosmopolitismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světoobčan	světoobčan	k1gMnSc1
se	se	k3xPyFc4
cítí	cítit	k5eAaImIp3nS
příslušníkem	příslušník	k1gMnSc7
celého	celý	k2eAgNnSc2d1
lidstva	lidstvo	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
vlastně	vlastně	k9
tvoří	tvořit	k5eAaImIp3nP
jednotu	jednota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodí	rodit	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
i	i	k9
humanismus	humanismus	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
spřízněni	spřízněn	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
ke	k	k7c3
stoi	stoa	k1gFnSc3
mohou	moct	k5eAaImIp3nP
brzy	brzy	k6eAd1
hlásit	hlásit	k5eAaImF
i	i	k9
cizinci	cizinec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
myšlenkou	myšlenka	k1gFnSc7
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
ve	v	k7c6
stoickém	stoický	k2eAgInSc6d1
systému	systém	k1gInSc6
všechno	všechen	k3xTgNnSc1
slouží	sloužit	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
samo	sám	k3xTgNnSc1
pojetí	pojetí	k1gNnSc1
filosofie	filosofie	k1gFnSc2
jako	jako	k8xS,k8xC
návodu	návod	k1gInSc2
k	k	k7c3
dosažení	dosažení	k1gNnSc3
blaženého	blažený	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideálem	ideál	k1gInSc7
je	být	k5eAaImIp3nS
mudrc	mudrc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
moudrostí	moudrost	k1gFnPc2
lze	lze	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
blaženosti	blaženost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
slova	slovo	k1gNnSc2
a	a	k8xC
základní	základní	k2eAgInPc4d1
postuláty	postulát	k1gInPc4
</s>
<s>
Název	název	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
zdobeného	zdobený	k2eAgNnSc2d1
sloupořadí	sloupořadí	k1gNnSc2
–	–	k?
stoa	stoa	k1gFnSc1
poikilé	poikilý	k2eAgFnPc1d1
(	(	kIx(
<g/>
stoa-sloup	stoa-sloup	k1gInSc1
<g/>
,	,	kIx,
poikilos-barevný	poikilos-barevný	k2eAgInSc1d1
<g/>
)	)	kIx)
–	–	k?
znamená	znamenat	k5eAaImIp3nS
zdobené	zdobený	k2eAgNnSc1d1
sloupořadí	sloupořadí	k1gNnPc1
lemující	lemující	k2eAgNnPc1d1
antická	antický	k2eAgNnPc1d1
náměstí	náměstí	k1gNnPc1
<g/>
,	,	kIx,
na	na	k7c6
těchto	tento	k3xDgNnPc6
místech	místo	k1gNnPc6
debatovali	debatovat	k5eAaImAgMnP
antičtí	antický	k2eAgMnPc1d1
učenci	učenec	k1gMnPc1
a	a	k8xC
filosofové	filosof	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžiště	těžiště	k1gNnSc1
stoické	stoický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
její	její	k3xOp3gFnSc6
etice	etika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoici	stoik	k1gMnPc1
srovnávali	srovnávat	k5eAaImAgMnP
vzájemný	vzájemný	k2eAgInSc4d1
poměr	poměr	k1gInSc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
filosofie	filosofie	k1gFnSc1
takto	takto	k6eAd1
<g/>
:	:	kIx,
Logika	logika	k1gFnSc1
je	být	k5eAaImIp3nS
skořápka	skořápka	k1gFnSc1
<g/>
,	,	kIx,
fyzika	fyzika	k1gFnSc1
je	být	k5eAaImIp3nS
bílek	bílek	k1gInSc1
a	a	k8xC
morálka	morálka	k1gFnSc1
žloutek	žloutek	k1gInSc1
<g/>
,	,	kIx,
nejdůležitější	důležitý	k2eAgFnSc1d3
část	část	k1gFnSc1
vajíčka	vajíčko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mravnost	mravnost	k1gFnSc1
chápali	chápat	k5eAaImAgMnP
v	v	k7c6
žití	žití	k1gNnSc6
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
přírodou	příroda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
je	být	k5eAaImIp3nS
chápána	chápat	k5eAaImNgFnS
i	i	k9
blaženost	blaženost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgFnSc7d3
ctností	ctnost	k1gFnSc7
je	být	k5eAaImIp3nS
žít	žít	k5eAaImF
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
rozumem	rozum	k1gInSc7
a	a	k8xC
přírodou	příroda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
označováno	označovat	k5eAaImNgNnS
jako	jako	k9
blaženost	blaženost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vývojové	vývojový	k2eAgFnPc1d1
etapy	etapa	k1gFnPc1
a	a	k8xC
představitelé	představitel	k1gMnPc1
</s>
<s>
Stoa	stoa	k1gFnSc1
prošla	projít	k5eAaPmAgFnS
třemi	tři	k4xCgFnPc7
vývojovými	vývojový	k2eAgFnPc7d1
etapami	etapa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluvíme	mluvit	k5eAaImIp1nP
proto	proto	k8xC
o	o	k7c6
stoi	stoa	k1gFnSc6
starší	starý	k2eAgFnSc6d2
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc4d1
a	a	k8xC
mladší	mladý	k2eAgFnSc4d2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
tato	tento	k3xDgFnSc1
poslední	poslední	k2eAgFnSc1d1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
v	v	k7c6
době	doba	k1gFnSc6
tzv.	tzv.	kA
císařské	císařský	k2eAgFnPc1d1
<g/>
,	,	kIx,
již	již	k6eAd1
počítáme	počítat	k5eAaImIp1nP
od	od	k7c2
vlády	vláda	k1gFnSc2
císaře	císař	k1gMnSc2
Augusta	August	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založení	založení	k1gNnSc1
původní	původní	k2eAgFnSc2d1
stoy	stoa	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc1
působení	působení	k1gNnSc1
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
můžeme	moct	k5eAaImIp1nP
datovat	datovat	k5eAaImF
přibližně	přibližně	k6eAd1
rokem	rok	k1gInSc7
300	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
dostala	dostat	k5eAaPmAgFnS
tato	tento	k3xDgFnSc1
škola	škola	k1gFnSc1
podle	podle	k7c2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
působila	působit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
athénská	athénský	k2eAgFnSc1d1
stoá	stoat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
poikilé	poikilý	k2eAgNnSc1d1
<g/>
,	,	kIx,
sloupová	sloupový	k2eAgFnSc1d1
síň	síň	k1gFnSc1
vyzdobená	vyzdobený	k2eAgFnSc1d1
obrazy	obraz	k1gInPc7
Polygnótovými	Polygnótův	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladatelem	zakladatel	k1gMnSc7
stoické	stoický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
byl	být	k5eAaImAgInS
Zénon	Zénon	k1gInSc1
z	z	k7c2
Kitia	Kitium	k1gNnSc2
na	na	k7c6
Kypru	Kypr	k1gInSc6
(	(	kIx(
<g/>
asi	asi	k9
335	#num#	k4
<g/>
–	–	k?
<g/>
263	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
systematikem	systematik	k1gMnSc7
jejího	její	k3xOp3gNnSc2
učení	učení	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Chrýsippos	Chrýsippos	k1gInSc1
ze	z	k7c2
Soloi	Solo	k1gFnSc2
v	v	k7c6
Kilikii	Kilikie	k1gFnSc6
(	(	kIx(
<g/>
280	#num#	k4
<g/>
–	–	k?
<g/>
207	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
ve	v	k7c6
starověku	starověk	k1gInSc6
se	se	k3xPyFc4
pro	pro	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
říkalo	říkat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
nebýt	být	k5eNaImF
Chrýsippa	Chrýsipp	k1gMnSc4
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
by	by	kYmCp3nS
ani	ani	k9
stoy	stoa	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starý	k2eAgFnSc1d2
a	a	k8xC
střední	střední	k2eAgFnSc1d1
stoa	stoa	k1gFnSc1
položily	položit	k5eAaPmAgInP
všechny	všechen	k3xTgInPc1
důležité	důležitý	k2eAgInPc1d1
základy	základ	k1gInPc1
stoicismu	stoicismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přínos	přínos	k1gInSc1
mladší	mladý	k2eAgFnSc2d2
stoy	stoa	k1gFnSc2
ke	k	k7c3
stoickému	stoický	k2eAgNnSc3d1
učení	učení	k1gNnSc3
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
výrazný	výrazný	k2eAgInSc1d1
<g/>
,	,	kIx,
zasloužila	zasloužit	k5eAaPmAgFnS
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
jeho	jeho	k3xOp3gFnSc4
další	další	k2eAgFnSc4d1
popularizaci	popularizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
děl	dělo	k1gNnPc2
starších	starší	k1gMnPc2
a	a	k8xC
středních	střední	k2eAgMnPc2d1
stoiků	stoik	k1gMnPc2
se	se	k3xPyFc4
dochovaly	dochovat	k5eAaPmAgInP
pouze	pouze	k6eAd1
zlomky	zlomek	k1gInPc1
<g/>
,	,	kIx,
nejstarší	starý	k2eAgInPc1d3
známé	známý	k2eAgInPc1d1
celistvé	celistvý	k2eAgInPc1d1
stoické	stoický	k2eAgInPc1d1
texty	text	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
od	od	k7c2
autorů	autor	k1gMnPc2
mladší	mladý	k2eAgFnSc2d2
stoy	stoa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Představitelé	představitel	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Starší	starý	k2eAgFnSc1d2
stoa	stoa	k1gFnSc1
</s>
<s>
Zénon	Zénon	k1gMnSc1
z	z	k7c2
Kitia	Kitium	k1gNnSc2
–	–	k?
zakladatel	zakladatel	k1gMnSc1
stoicismu	stoicismus	k1gInSc2
</s>
<s>
Kleanthés	Kleanthés	k6eAd1
z	z	k7c2
Assu	Assus	k1gInSc2
</s>
<s>
Chrýsippos	Chrýsippos	k1gInSc1
ze	z	k7c2
Soloi	Solo	k1gFnSc2
</s>
<s>
Díogenés	Díogenés	k6eAd1
z	z	k7c2
Seleukeie	Seleukeie	k1gFnSc2
</s>
<s>
Střední	střední	k2eAgFnSc1d1
stoa	stoa	k1gFnSc1
</s>
<s>
Panaitios	Panaitios	k1gMnSc1
</s>
<s>
Poseidónios	Poseidónios	k1gMnSc1
</s>
<s>
Hekatón	Hekatón	k1gInSc1
z	z	k7c2
Rhodu	Rhodos	k1gInSc2
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2
stoa	stoa	k1gFnSc1
</s>
<s>
Luccius	Luccius	k1gMnSc1
Annaeus	Annaeus	k1gMnSc1
Seneca	Seneca	k1gMnSc1
</s>
<s>
Epiktétos	Epiktétos	k1gMnSc1
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
</s>
<s>
Myšlenková	myšlenkový	k2eAgNnPc1d1
východiska	východisko	k1gNnPc1
</s>
<s>
V	v	k7c6
logice	logika	k1gFnSc6
navazují	navazovat	k5eAaImIp3nP
stoikové	stoik	k1gMnPc1
na	na	k7c4
aristotelskou	aristotelský	k2eAgFnSc4d1
elementární	elementární	k2eAgFnSc4d1
logiku	logika	k1gFnSc4
a	a	k8xC
na	na	k7c4
kynický	kynický	k2eAgInSc4d1
senzualismus	senzualismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soudy	soud	k1gInPc4
v	v	k7c6
zásadě	zásada	k1gFnSc6
rozdělují	rozdělovat	k5eAaImIp3nP
na	na	k7c4
kladné	kladný	k2eAgNnSc4d1
<g/>
,	,	kIx,
záporné	záporný	k2eAgNnSc4d1
a	a	k8xC
nerozhodné	rozhodný	k2eNgNnSc4d1
(	(	kIx(
<g/>
zdržení	zdržení	k1gNnSc4
soudu	soud	k1gInSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
epoché	epochý	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
10	#num#	k4
Aristotelových	Aristotelův	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
mluví	mluvit	k5eAaImIp3nP
stoici	stoik	k1gMnPc1
o	o	k7c6
čtyřech	čtyři	k4xCgFnPc6
–	–	k?
podstata	podstata	k1gFnSc1
(	(	kIx(
<g/>
či	či	k8xC
substrát	substrát	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podstatná	podstatný	k2eAgFnSc1d1
vlastnost	vlastnost	k1gFnSc1
či	či	k8xC
jakost	jakost	k1gFnSc1
(	(	kIx(
<g/>
jaké	jaký	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
<g/>
,	,	kIx,
nějaké	nějaký	k3yIgInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stav	stav	k1gInSc1
(	(	kIx(
<g/>
spíše	spíše	k9
nahodilý	nahodilý	k2eAgMnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
vztah	vztah	k1gInSc1
(	(	kIx(
<g/>
poměr	poměr	k1gInSc1
jedné	jeden	k4xCgFnSc2
věci	věc	k1gFnSc2
k	k	k7c3
druhé	druhý	k4xOgFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
každá	každý	k3xTgFnSc1
další	další	k2eAgFnSc1d1
kategorie	kategorie	k1gFnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
bližším	blízký	k2eAgNnSc7d2
určením	určení	k1gNnSc7
předešlé	předešlý	k2eAgFnSc2d1
(	(	kIx(
<g/>
např.	např.	kA
jakost	jakost	k1gFnSc1
podstaty	podstata	k1gFnSc2
<g/>
,	,	kIx,
stav	stav	k1gInSc4
jakosti	jakost	k1gFnSc2
podstaty	podstata	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
velmi	velmi	k6eAd1
propracované	propracovaný	k2eAgFnSc2d1
stoické	stoický	k2eAgFnSc2d1
etiky	etika	k1gFnSc2
<g/>
,	,	kIx,
vše	všechen	k3xTgNnSc4
se	se	k3xPyFc4
„	„	k?
<g/>
točí	točit	k5eAaImIp3nS
<g/>
“	“	k?
kolem	kolem	k7c2
jediného	jediný	k2eAgInSc2d1
imperativu	imperativ	k1gInSc2
–	–	k?
žij	žít	k5eAaImRp2nS
ctnostně	ctnostně	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
považují	považovat	k5eAaImIp3nP
stoici	stoik	k1gMnPc1
za	za	k7c4
jediné	jediný	k2eAgNnSc4d1
dobro	dobro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
jaké	jaký	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
jsou	být	k5eAaImIp3nP
hlavní	hlavní	k2eAgInPc1d1
atributy	atribut	k1gInPc1
tak	tak	k6eAd1
zvaného	zvaný	k2eAgInSc2d1
ctnostného	ctnostný	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
rozumnost	rozumnost	k1gFnSc1
<g/>
,	,	kIx,
uměřenost	uměřenost	k1gFnSc1
<g/>
,	,	kIx,
spravedlnost	spravedlnost	k1gFnSc1
a	a	k8xC
statečnost	statečnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žít	žít	k5eAaImF
ctnostně	ctnostně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
žít	žít	k5eAaImF
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
přírodou	příroda	k1gFnSc7
a	a	k8xC
rozumem	rozum	k1gInSc7
(	(	kIx(
<g/>
či	či	k8xC
přírodním	přírodní	k2eAgInSc7d1
rozumem	rozum	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
ostatní	ostatní	k1gNnSc1
je	být	k5eAaImIp3nS
lhostejné	lhostejný	k2eAgNnSc1d1
(	(	kIx(
<g/>
adiaforon	adiaforon	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
je	být	k5eAaImIp3nS
myšleno	myšlen	k2eAgNnSc1d1
např.	např.	kA
bohatství	bohatství	k1gNnSc2
<g/>
,	,	kIx,
postavení	postavení	k1gNnSc2
<g/>
,	,	kIx,
sláva	sláva	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc4
skutečnosti	skutečnost	k1gFnPc4
nemůže	moct	k5eNaImIp3nS
člověk	člověk	k1gMnSc1
ovlivnit	ovlivnit	k5eAaPmF
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
nad	nad	k7c4
ně	on	k3xPp3gMnPc4
povznést	povznést	k5eAaPmF
<g/>
,	,	kIx,
smířit	smířit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
života	život	k1gInSc2
člověka	člověk	k1gMnSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
podle	podle	k7c2
stoiků	stoik	k1gMnPc2
vyrovnání	vyrovnání	k1gNnSc2
se	se	k3xPyFc4
se	s	k7c7
skutečností	skutečnost	k1gFnSc7
<g/>
,	,	kIx,
dosáhnutí	dosáhnutý	k2eAgMnPc1d1
duševního	duševní	k2eAgInSc2d1
klidu	klid	k1gInSc2
a	a	k8xC
neochvějnosti	neochvějnost	k1gFnSc2
(	(	kIx(
<g/>
ataraxia	ataraxia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zbavení	zbavení	k1gNnSc1
se	se	k3xPyFc4
všech	všecek	k3xTgFnPc2
životních	životní	k2eAgFnPc2d1
vášní	vášeň	k1gFnPc2
a	a	k8xC
tím	ten	k3xDgNnSc7
dosažení	dosažení	k1gNnSc4
apatheia	apatheium	k1gNnSc2
(	(	kIx(
<g/>
nenáruživosti	nenáruživost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
stav	stav	k1gInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
stoického	stoický	k2eAgNnSc2d1
učení	učení	k1gNnSc2
ideální	ideální	k2eAgInSc1d1
pro	pro	k7c4
mudrce	mudrc	k1gMnSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
stojí	stát	k5eAaImIp3nS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
dosažitelné	dosažitelný	k2eAgFnSc2d1
dokonalosti	dokonalost	k1gFnSc2
pro	pro	k7c4
běžného	běžný	k2eAgMnSc4d1
smrtelníka	smrtelník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
dokonalý	dokonalý	k2eAgMnSc1d1
stoik	stoik	k1gMnSc1
jest	být	k5eAaImIp3nS
přísně	přísně	k6eAd1
morální	morální	k2eAgMnSc1d1
(	(	kIx(
<g/>
zásadní	zásadní	k2eAgInSc1d1
vliv	vliv	k1gInSc1
na	na	k7c4
formování	formování	k1gNnSc4
křesťanství	křesťanství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stává	stávat	k5eAaImIp3nS
se	se	k3xPyFc4
též	též	k9
nejsvobodnějším	svobodný	k2eAgMnSc7d3
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
pouze	pouze	k6eAd1
rozumem	rozum	k1gInSc7
a	a	k8xC
je	být	k5eAaImIp3nS
nezávislý	závislý	k2eNgInSc1d1
na	na	k7c6
věcech	věc	k1gFnPc6
vnějších	vnější	k2eAgFnPc6d1
<g/>
.	.	kIx.
</s>
<s>
Stoici	stoik	k1gMnPc1
byli	být	k5eAaImAgMnP
pantheisté	pantheista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
byl	být	k5eAaImAgMnS
u	u	k7c2
nich	on	k3xPp3gMnPc2
cosi	cosi	k3yInSc4
jako	jako	k8xS,k8xC
světová	světový	k2eAgFnSc1d1
substance	substance	k1gFnSc1
<g/>
,	,	kIx,
světlo	světlo	k1gNnSc1
<g/>
,	,	kIx,
pneuma	pneuma	k?
pronikající	pronikající	k2eAgInSc1d1
vším	všecek	k3xTgInSc7
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jakýsi	jakýsi	k3yIgInSc1
tvůrčí	tvůrčí	k2eAgInSc1d1
oheň	oheň	k1gInSc1
<g/>
,	,	kIx,
původce	původce	k1gMnSc1
všeho	všecek	k3xTgNnSc2
dění	dění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
život	život	k1gInSc4
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
přírodou	příroda	k1gFnSc7
a	a	k8xC
rozumem	rozum	k1gInSc7
je	být	k5eAaImIp3nS
život	život	k1gInSc4
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
Bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žít	žít	k5eAaImF
takto	takto	k6eAd1
je	být	k5eAaImIp3nS
povinnost	povinnost	k1gFnSc1
uložená	uložený	k2eAgFnSc1d1
moudrým	moudrý	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc1
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
rozumem	rozum	k1gInSc7
a	a	k8xC
přírodou	příroda	k1gFnSc7
byl	být	k5eAaImAgInS
chápán	chápat	k5eAaImNgInS
jako	jako	k8xS,k8xC
jediná	jediný	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ke	k	k7c3
svobodě	svoboda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoici	stoik	k1gMnPc1
byli	být	k5eAaImAgMnP
deterministé	determinista	k1gMnPc1
–	–	k?
vše	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
spojeno	spojit	k5eAaPmNgNnS
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
<g/>
;	;	kIx,
všechno	všechen	k3xTgNnSc1
vnější	vnější	k2eAgMnSc1d1
se	se	k3xPyFc4
děje	dít	k5eAaImIp3nS
nutně	nutně	k6eAd1
<g/>
,	,	kIx,
příčinně	příčinně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srovnávali	srovnávat	k5eAaImAgMnP
člověka	člověk	k1gMnSc4
se	s	k7c7
psem	pes	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
zapřažen	zapřáhnout	k5eAaPmNgMnS
k	k	k7c3
vozíku	vozík	k1gInSc3
a	a	k8xC
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
si	se	k3xPyFc3
svobodný	svobodný	k2eAgMnSc1d1
<g/>
,	,	kIx,
dokud	dokud	k8xS
běží	běžet	k5eAaImIp3nS
jak	jak	k8xC,k8xS
má	mít	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
nucen	nutit	k5eAaImNgMnS
jít	jít	k5eAaImF
proti	proti	k7c3
své	svůj	k3xOyFgFnSc3
vůli	vůle	k1gFnSc3
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
dokonce	dokonce	k9
vlečen	vlečen	k2eAgMnSc1d1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
nechce	chtít	k5eNaImIp3nS
přizpůsobit	přizpůsobit	k5eAaPmF
pohybu	pohyb	k1gInSc3
svého	svůj	k3xOyFgInSc2
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seneca	Seneca	k1gMnSc1
toto	tento	k3xDgNnSc4
zachytil	zachytit	k5eAaPmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
citátu	citát	k1gInSc6
<g/>
:	:	kIx,
„	„	k?
<g/>
Volentem	Volent	k1gInSc7
fata	fatum	k1gNnSc2
ducunt	ducunta	k1gFnPc2
<g/>
,	,	kIx,
nolentem	nolento	k1gNnSc7
trahunt	trahunta	k1gFnPc2
<g/>
“	“	k?
–	–	k?
„	„	k?
<g/>
Chtějícího	chtějící	k2eAgInSc2d1
osud	osud	k1gInSc1
vede	vést	k5eAaImIp3nS
<g/>
,	,	kIx,
nechtějícího	chtějící	k2eNgInSc2d1
vleče	vléct	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Blaženost	blaženost	k1gFnSc1
rozhodně	rozhodně	k6eAd1
není	být	k5eNaImIp3nS
blaženost	blaženost	k1gFnSc1
vášnivá	vášnivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
například	například	k6eAd1
sexuální	sexuální	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
vášní	vášeň	k1gFnPc2
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
naopak	naopak	k6eAd1
odpoutat	odpoutat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
stoiků	stoik	k1gMnPc2
nezávisí	záviset	k5eNaImIp3nS
hodnota	hodnota	k1gFnSc1
života	život	k1gInSc2
na	na	k7c6
věcech	věc	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
např.	např.	kA
bohatství	bohatství	k1gNnSc2
a	a	k8xC
chudoba	chudoba	k1gFnSc1
<g/>
,	,	kIx,
zdraví	zdraví	k1gNnSc4
a	a	k8xC
nemoc	nemoc	k1gFnSc4
<g/>
,	,	kIx,
úspěch	úspěch	k1gInSc4
a	a	k8xC
neúspěch	neúspěch	k1gInSc4
v	v	k7c6
životě	život	k1gInSc6
<g/>
,	,	kIx,
svoboda	svoboda	k1gFnSc1
a	a	k8xC
otroctví	otroctví	k1gNnSc1
nebo	nebo	k8xC
život	život	k1gInSc4
a	a	k8xC
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoikové	stoik	k1gMnPc1
hlásají	hlásat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
existují	existovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
druhy	druh	k1gInPc1
lidí	člověk	k1gMnPc2
–	–	k?
stoici	stoik	k1gMnPc1
a	a	k8xC
blázni	blázen	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoik	stoik	k1gMnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
nich	on	k3xPp3gMnPc2
blažený	blažený	k2eAgMnSc1d1
za	za	k7c2
všech	všecek	k3xTgFnPc2
okolností	okolnost	k1gFnPc2
právě	právě	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
stoická	stoický	k2eAgFnSc1d1
ctnost	ctnost	k1gFnSc1
je	být	k5eAaImIp3nS
totožná	totožný	k2eAgFnSc1d1
s	s	k7c7
blažeností	blaženost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čili	čili	k8xC
je	být	k5eAaImIp3nS
svoboden	svoboden	k2eAgMnSc1d1
i	i	k8xC
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
otrokem	otrok	k1gMnSc7
<g/>
,	,	kIx,
bohatý	bohatý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
i	i	k9
žebrák	žebrák	k1gMnSc1
<g/>
,	,	kIx,
vladař	vladař	k1gMnSc1
je	být	k5eAaImIp3nS
i	i	k8xC
ten	ten	k3xDgInSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
nikdo	nikdo	k3yNnSc1
neposlouchá	poslouchat	k5eNaImIp3nS
<g/>
,	,	kIx,
atp.	atp.	kA
Všichni	všechen	k3xTgMnPc1
ostatní	ostatní	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
chudáci	chudák	k1gMnPc1
<g/>
,	,	kIx,
žebráci	žebrák	k1gMnPc1
a	a	k8xC
šílenci	šílenec	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
právě	právě	k9
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
hledají	hledat	k5eAaImIp3nP
blaženost	blaženost	k1gFnSc4
ve	v	k7c6
vnějškovostech	vnějškovost	k1gFnPc6
–	–	k?
tj.	tj.	kA
ve	v	k7c6
věcech	věc	k1gFnPc6
požitkových	požitkový	k2eAgFnPc6d1
jako	jako	k8xC,k8xS
vášeň	vášeň	k1gFnSc4
apod.	apod.	kA
</s>
<s>
Specifickým	specifický	k2eAgInSc7d1
znakem	znak	k1gInSc7
celého	celý	k2eAgInSc2d1
řeckého	řecký	k2eAgInSc2d1
stoicismu	stoicismus	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
staré	starý	k2eAgFnSc2d1
i	i	k8xC
střední	střední	k2eAgFnSc2d1
stoy	stoa	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
–	–	k?
a	a	k8xC
to	ten	k3xDgNnSc1
proti	proti	k7c3
všem	všecek	k3xTgInPc3
ostatním	ostatní	k2eAgInPc3d1
směrům	směr	k1gInPc3
řecké	řecký	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
–	–	k?
daleko	daleko	k6eAd1
větší	veliký	k2eAgFnSc1d2
míra	míra	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
přizpůsobivosti	přizpůsobivost	k1gFnSc2
v	v	k7c6
otázce	otázka	k1gFnSc6
poměru	poměr	k1gInSc2
k	k	k7c3
helénistické	helénistický	k2eAgFnSc3d1
státní	státní	k2eAgFnSc3d1
formě	forma	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
stoikové	stoik	k1gMnPc1
<g/>
,	,	kIx,
řecky	řecky	k6eAd1
sice	sice	k8xC
píšící	píšící	k2eAgNnSc1d1
a	a	k8xC
učící	učící	k2eAgNnSc1d1
<g/>
,	,	kIx,
nebyli	být	k5eNaImAgMnP
původem	původ	k1gInSc7
většinou	většina	k1gFnSc7
Řeky	řeka	k1gFnPc1
a	a	k8xC
ani	ani	k8xC
nebyli	být	k5eNaImAgMnP
nijak	nijak	k6eAd1
úzce	úzko	k6eAd1
spjati	spjat	k2eAgMnPc1d1
s	s	k7c7
Athénami	Athéna	k1gFnPc7
a	a	k8xC
ostatním	ostatní	k2eAgNnSc7d1
Řeckem	Řecko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
si	se	k3xPyFc3
lze	lze	k6eAd1
vysvětlit	vysvětlit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
proto	proto	k8xC
tedy	tedy	k9
nebyli	být	k5eNaImAgMnP
tolik	tolik	k6eAd1
ovlivněni	ovlivněn	k2eAgMnPc1d1
již	již	k6eAd1
přežilou	přežilý	k2eAgFnSc7d1
ideologií	ideologie	k1gFnSc7
řeckého	řecký	k2eAgInSc2d1
městského	městský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoicismus	stoicismus	k1gInSc4
a	a	k8xC
sami	sám	k3xTgMnPc1
stoikové	stoik	k1gMnPc1
se	se	k3xPyFc4
lépe	dobře	k6eAd2
než	než	k8xS
kterýkoliv	kterýkoliv	k3yIgInSc1
jiný	jiný	k2eAgInSc1d1
tehdejší	tehdejší	k2eAgInSc1d1
filosofický	filosofický	k2eAgInSc1d1
směr	směr	k1gInSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
představitelé	představitel	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
přizpůsobit	přizpůsobit	k5eAaPmF
nejen	nejen	k6eAd1
širším	široký	k2eAgFnPc3d2
světovým	světový	k2eAgFnPc3d1
podmínkám	podmínka	k1gFnPc3
helénistických	helénistický	k2eAgFnPc2d1
říší	říš	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
zejména	zejména	k9
podmínkám	podmínka	k1gFnPc3
Římského	římský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
pravdě	pravda	k1gFnSc6
řečeno	říct	k5eAaPmNgNnS
je	být	k5eAaImIp3nS
římský	římský	k2eAgInSc1d1
stoicismus	stoicismus	k1gInSc1
jediným	jediný	k2eAgMnSc7d1
římským	římský	k2eAgMnSc7d1
filosofickým	filosofický	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
můžeme	moct	k5eAaImIp1nP
v	v	k7c6
jistém	jistý	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
nazývat	nazývat	k5eAaImF
„	„	k?
<g/>
školou	škola	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
tím	ten	k3xDgNnSc7
nazýváme	nazývat	k5eAaImIp1nP
neoprávněně	oprávněně	k6eNd1
prostě	prostě	k9
tu	ten	k3xDgFnSc4
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
v	v	k7c6
Římě	Řím	k1gInSc6
najdeme	najít	k5eAaPmIp1nP
různé	různý	k2eAgInPc1d1
typy	typ	k1gInPc1
světonázorového	světonázorový	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
této	tento	k3xDgFnSc2
školy	škola	k1gFnSc2
vzešly	vzejít	k5eAaPmAgFnP
nauky	nauka	k1gFnPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
měly	mít	k5eAaImAgFnP
veliký	veliký	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
socialismus	socialismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stoici	stoik	k1gMnPc1
byli	být	k5eAaImAgMnP
první	první	k4xOgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
hlásali	hlásat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
děti	dítě	k1gFnPc4
společného	společný	k2eAgMnSc4d1
boha	bůh	k1gMnSc4
<g/>
,	,	kIx,
podléhají	podléhat	k5eAaImIp3nP
stejným	stejný	k2eAgInPc3d1
zákonům	zákon	k1gInPc3
<g/>
,	,	kIx,
a	a	k8xC
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
rovní	rovný	k2eAgMnPc1d1
občané	občan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
lidského	lidský	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
podle	podle	k7c2
nich	on	k3xPp3gInPc2
nelze	lze	k6eNd1
vylučovat	vylučovat	k5eAaImF
ani	ani	k9
barbary	barbar	k1gMnPc4
či	či	k8xC
nepřátele	nepřítel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Významné	významný	k2eAgInPc1d1
výroky	výrok	k1gInPc1
a	a	k8xC
citáty	citát	k1gInPc1
stoiků	stoik	k1gMnPc2
</s>
<s>
Epiktétos	Epiktétos	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Nechtěj	chtěj	k6eNd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
chtěj	chtěj	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
budeš	být	k5eAaImBp2nS
spokojen	spokojen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Nepřemožitelný	přemožitelný	k2eNgInSc1d1
můžeš	moct	k5eAaImIp2nS
být	být	k5eAaImF
<g/>
,	,	kIx,
nepustíš	pustit	k5eNaPmIp2nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
do	do	k7c2
žádného	žádný	k3yNgInSc2
zápasu	zápas	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
zvítězit	zvítězit	k5eAaPmF
není	být	k5eNaImIp3nS
ve	v	k7c6
tvé	tvůj	k3xOp2gFnSc6
moci	moc	k1gFnSc6
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Nikdy	nikdy	k6eAd1
o	o	k7c6
ničem	nic	k3yNnSc6
neříkej	říkat	k5eNaImRp2nS
<g/>
:	:	kIx,
ztratil	ztratit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
vrátil	vrátit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Řeknou	říct	k5eAaPmIp3nP
<g/>
-li	-li	k?
ti	ty	k3xPp2nSc3
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c6
tobě	ty	k3xPp2nSc6
někdo	někdo	k3yInSc1
špatně	špatně	k6eAd1
mluví	mluvit	k5eAaImIp3nS
<g/>
,	,	kIx,
nehaj	hájit	k5eNaImRp2nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
odpověz	odpovědět	k5eAaPmRp2nS
<g/>
:	:	kIx,
Takže	takže	k8xS
nevěděl	vědět	k5eNaImAgMnS
o	o	k7c6
ostatních	ostatní	k2eAgFnPc6d1
mých	můj	k3xOp1gFnPc6
chybách	chyba	k1gFnPc6
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
by	by	kYmCp3nS
nebyl	být	k5eNaImAgInS
mluvil	mluvit	k5eAaImAgInS
jen	jen	k6eAd1
o	o	k7c6
těchto	tento	k3xDgFnPc6
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Někdo	někdo	k3yInSc1
hodně	hodně	k6eAd1
pije	pít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neříkej	říkat	k5eNaImRp2nS
<g/>
,	,	kIx,
že	že	k8xS
špatně	špatně	k6eAd1
činí	činit	k5eAaImIp3nS
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jen	jen	k9
<g/>
,	,	kIx,
že	že	k8xS
hodně	hodně	k6eAd1
pije	pít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdo	někdo	k3yInSc1
se	se	k3xPyFc4
časně	časně	k6eAd1
koupá	koupat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neříkej	říkat	k5eNaImRp2nS
<g/>
,	,	kIx,
že	že	k8xS
špatně	špatně	k6eAd1
činí	činit	k5eAaImIp3nS
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
jen	jen	k9
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
časně	časně	k6eAd1
koupá	koupat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Nežádej	žádat	k5eNaImRp2nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
věci	věc	k1gFnPc1
děly	dít	k5eAaImAgFnP,k5eAaBmAgFnP
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
chceš	chtít	k5eAaImIp2nS
<g/>
,	,	kIx,
ale	ale	k8xC
chtěj	chtěj	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
věci	věc	k1gFnPc1
děly	dít	k5eAaImAgFnP,k5eAaBmAgFnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
dějí	dít	k5eAaImIp3nP,k5eAaBmIp3nP
<g/>
,	,	kIx,
a	a	k8xC
bude	být	k5eAaImBp3nS
ti	ten	k3xDgMnPc1
v	v	k7c6
životě	život	k1gInSc6
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Seneca	Seneca	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Chtějícího	chtějící	k2eAgInSc2d1
osud	osud	k1gInSc1
vede	vést	k5eAaImIp3nS
<g/>
,	,	kIx,
nechtějícího	chtějící	k2eNgInSc2d1
vleče	vléct	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
…	…	k?
máme	mít	k5eAaImIp1nP
alespoň	alespoň	k9
omezovat	omezovat	k5eAaImF
svůj	svůj	k3xOyFgInSc4
majetek	majetek	k1gInSc4
<g/>
,	,	kIx,
abychom	aby	kYmCp1nP
byli	být	k5eAaImAgMnP
méně	málo	k6eAd2
vystaveni	vystavit	k5eAaPmNgMnP
ranám	rána	k1gFnPc3
osudu	osud	k1gInSc3
…	…	k?
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Lidštější	lidský	k2eAgFnPc4d2
je	být	k5eAaImIp3nS
životu	život	k1gInSc3
se	se	k3xPyFc4
vysmívat	vysmívat	k5eAaImF
než	než	k8xS
nad	nad	k7c7
ním	on	k3xPp3gMnSc7
bědovat	bědovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
…	…	k?
chápej	chápat	k5eAaImRp2nS
se	se	k3xPyFc4
každé	každý	k3xTgFnSc2
hodiny	hodina	k1gFnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Dosáhneš	dosáhnout	k5eAaPmIp2nS
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
budeš	být	k5eAaImBp2nS
méně	málo	k6eAd2
závislý	závislý	k2eAgInSc1d1
na	na	k7c6
zítřku	zítřek	k1gInSc6
<g/>
,	,	kIx,
uchopíš	uchopit	k5eAaPmIp2nS
<g/>
-li	-li	k?
do	do	k7c2
rukou	ruka	k1gFnPc2
dnešek	dnešek	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Na	na	k7c4
všechen	všechen	k3xTgInSc4
způsob	způsob	k1gInSc4
budiž	budiž	k9
duše	duše	k1gFnSc1
odvrácena	odvrácen	k2eAgFnSc1d1
ode	ode	k7c2
všeho	všecek	k3xTgNnSc2
vnějšího	vnější	k2eAgNnSc2d1
a	a	k8xC
pohřížena	pohřížit	k5eAaPmNgFnS
v	v	k7c4
sebe	sebe	k3xPyFc4
<g/>
:	:	kIx,
sobě	se	k3xPyFc3
ať	ať	k9
důvěřuje	důvěřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
sobě	se	k3xPyFc3
se	se	k3xPyFc4
raduje	radovat	k5eAaImIp3nS
<g/>
,	,	kIx,
…	…	k?
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Žít	žít	k5eAaImF
znamená	znamenat	k5eAaImIp3nS
bojovat	bojovat	k5eAaImF
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
</s>
<s>
Marcus	Marcus	k1gMnSc1
Aurelius	Aurelius	k1gMnSc1
</s>
<s>
„	„	k?
<g/>
Měj	mít	k5eAaImRp2nS
na	na	k7c6
paměti	paměť	k1gFnSc6
úhrn	úhrn	k1gInSc4
jsoucna	jsoucno	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
jsi	být	k5eAaImIp2nS
pramalý	pramalý	k2eAgMnSc1d1
podílník	podílník	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
úhrn	úhrn	k1gInSc1
času	čas	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgMnSc2
je	být	k5eAaImIp3nS
ti	ten	k3xDgMnPc1
vyměřena	vyměřit	k5eAaPmNgFnS
kratinká	kratinký	k2eAgFnSc1d1
a	a	k8xC
nepatrná	patrný	k2eNgFnSc1d1,k2eAgFnSc1d1
píď	píď	k1gFnSc1
i	i	k8xC
osud	osud	k1gInSc1
<g/>
:	:	kIx,
kolikát	kolikát	k1gInSc1
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
zlomek	zlomek	k1gInSc4
je	být	k5eAaImIp3nS
tvůj	tvůj	k3xOp2gInSc4
život	život	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
</s>
<s>
„	„	k?
<g/>
Jak	jak	k8xC,k8xS
směšným	směšný	k2eAgMnSc7d1
cizincem	cizinec	k1gMnSc7
je	být	k5eAaImIp3nS
ve	v	k7c6
světě	svět	k1gInSc6
ten	ten	k3xDgInSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
diví	divit	k5eAaImIp3nS
kterékoli	kterýkoli	k3yIgFnSc2
události	událost	k1gFnSc2
v	v	k7c6
životě	život	k1gInSc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Helénismus	helénismus	k1gInSc1
</s>
<s>
Kynismus	kynismus	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
LONG	LONG	kA
<g/>
,	,	kIx,
A.	A.	kA
A.	A.	kA
Hellénistická	Hellénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
OIKOYMENH	OIKOYMENH	kA
<g/>
,	,	kIx,
2003	#num#	k4
ISBN	ISBN	kA
80-7298-077-7	80-7298-077-7	k4
</s>
<s>
RICKEN	RICKEN	kA
<g/>
,	,	kIx,
F.	F.	kA
Antická	antický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
ISBN	ISBN	kA
80-7182-134-9	80-7182-134-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Stoická	stoický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
filosofická	filosofický	k2eAgFnSc1d1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Antická	antický	k2eAgFnSc1d1
řecká	řecký	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
Předsókratovci	Předsókratovec	k1gMnSc3
</s>
<s>
iónská	iónský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
milétská	milétský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
Hérakleitos	Hérakleitos	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
pythagoreismus	pythagoreismus	k1gInSc1
•	•	k?
eleaté	eleata	k1gMnPc1
•	•	k?
pluralisté	pluralista	k1gMnPc1
•	•	k?
atomisté	atomista	k1gMnPc1
•	•	k?
sofisté	sofista	k1gMnPc1
</s>
<s>
Sókratovská	Sókratovský	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
</s>
<s>
platonismus	platonismus	k1gInSc1
(	(	kIx(
<g/>
Platón	platón	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
peripatetikové	peripatetik	k1gMnPc1
(	(	kIx(
<g/>
Aristotelés	Aristotelés	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
kynismus	kynismus	k1gInSc4
•	•	k?
kyrénaikové	kyrénaikový	k2eAgFnSc2d1
•	•	k?
megarská	megarský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
•	•	k?
élidská	élidský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Helénistická	helénistický	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
</s>
<s>
epikureismus	epikureismus	k1gInSc1
•	•	k?
stoicismus	stoicismus	k1gInSc1
•	•	k?
pyrrhonismus	pyrrhonismus	k1gInSc1
(	(	kIx(
<g/>
Pyrrhón	Pyrrhón	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
neopythagoreismus	neopythagoreismus	k1gInSc1
•	•	k?
neoplatonismus	neoplatonismus	k1gInSc1
(	(	kIx(
<g/>
Plótínos	Plótínos	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
dějiny	dějiny	k1gFnPc1
západní	západní	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
•	•	k?
starověká	starověký	k2eAgFnSc1d1
filosofie	filosofie	k1gFnSc1
•	•	k?
starověké	starověký	k2eAgNnSc4d1
Řecko	Řecko	k1gNnSc4
•	•	k?
řecká	řecký	k2eAgFnSc1d1
mytologie	mytologie	k1gFnSc1
•	•	k?
patristika	patristika	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
2841	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85128242	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85128242	#num#	k4
</s>
