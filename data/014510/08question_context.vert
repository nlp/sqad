<s desamb="1">
Působí	působit	k5eAaImIp3nS
totiž	totiž	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Řekové	Řek	k1gMnPc1
už	už	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
samostatnost	samostatnost	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c4
místo	místo	k1gNnSc4
drobných	drobný	k2eAgInPc2d1
městských	městský	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
polis	polis	k1gFnPc2
<g/>
)	)	kIx)
nastoupily	nastoupit	k5eAaPmAgFnP
větší	veliký	k2eAgInPc4d2
státní	státní	k2eAgInPc4d1
celky	celek	k1gInPc4
<g/>
,	,	kIx,
monarchie	monarchie	k1gFnSc1
<g/>
.	.	kIx.
</s>