<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
španělskou	španělský	k2eAgFnSc4d1	španělská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
současného	současný	k2eAgInSc2d1	současný
výkladu	výklad	k1gInSc2	výklad
<g/>
)	)	kIx)	)
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
bohatství	bohatství	k1gNnSc4	bohatství
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
svit	svit	k1gInSc4	svit
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
obilná	obilný	k2eAgNnPc1d1	obilné
pole	pole	k1gNnPc1	pole
<g/>
;	;	kIx,	;
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
vodstvo	vodstvo	k1gNnSc4	vodstvo
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
<g/>
;	;	kIx,	;
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
krev	krev	k1gFnSc1	krev
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
