<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
–	–	k?	–
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
a	a	k8xC	a
červený	červený	k2eAgInSc1d1	červený
–	–	k?	–
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
listu	list	k1gInSc2	list
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
poměr	poměr	k1gInSc4	poměr
stran	strana	k1gFnPc2	strana
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
španělskou	španělský	k2eAgFnSc4d1	španělská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
současného	současný	k2eAgInSc2d1	současný
výkladu	výklad	k1gInSc2	výklad
<g/>
)	)	kIx)	)
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
bohatství	bohatství	k1gNnSc4	bohatství
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
svit	svit	k1gInSc4	svit
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
obilná	obilný	k2eAgNnPc1d1	obilné
pole	pole	k1gNnPc1	pole
<g/>
;	;	kIx,	;
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
vodstvo	vodstvo	k1gNnSc4	vodstvo
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
<g/>
;	;	kIx,	;
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
krev	krev	k1gFnSc1	krev
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
Registro	Registro	k1gNnSc1	Registro
Oficial	Oficial	k1gInSc1	Oficial
č.	č.	k?	č.
1272	[number]	k4	1272
bylo	být	k5eAaImAgNnS	být
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1900	[number]	k4	1900
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
pro	pro	k7c4	pro
civilní	civilní	k2eAgNnSc4d1	civilní
použití	použití	k1gNnSc4	použití
bude	být	k5eAaImBp3nS	být
bez	bez	k7c2	bez
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
bude	být	k5eAaImBp3nS	být
znak	znak	k1gInSc1	znak
umístěn	umístit	k5eAaPmNgInS	umístit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
však	však	k9	však
lze	lze	k6eAd1	lze
běžně	běžně	k6eAd1	běžně
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
civilním	civilní	k2eAgNnSc7d1	civilní
užitím	užití	k1gNnSc7	užití
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
<g/>
Ekvádorská	ekvádorský	k2eAgFnSc1d1	ekvádorská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
venezuelské	venezuelský	k2eAgFnSc3d1	venezuelská
a	a	k8xC	a
kolumbijské	kolumbijský	k2eAgFnSc3d1	kolumbijská
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
záměně	záměna	k1gFnSc3	záměna
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
(	(	kIx(	(
<g/>
ekvádorská	ekvádorský	k2eAgFnSc1d1	ekvádorská
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
kolumbijskou	kolumbijský	k2eAgFnSc7d1	kolumbijská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
území	území	k1gNnSc1	území
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
dobyli	dobýt	k5eAaPmAgMnP	dobýt
koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
Inkové	Ink	k1gMnPc1	Ink
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1532	[number]	k4	1532
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Inckou	incký	k2eAgFnSc7d1	incká
říši	říš	k1gFnSc6	říš
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
i	i	k9	i
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1563	[number]	k4	1563
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
Quitu	Quit	k1gInSc6	Quit
audiencie	audiencie	k1gFnSc2	audiencie
a	a	k8xC	a
území	území	k1gNnSc1	území
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
Místokrálovství	Místokrálovství	k1gNnSc3	Místokrálovství
Peru	Peru	k1gNnSc2	Peru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1717	[number]	k4	1717
<g/>
–	–	k?	–
<g/>
1724	[number]	k4	1724
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
součástí	součást	k1gFnPc2	součást
Místokrálovství	Místokrálovství	k1gNnSc2	Místokrálovství
Nová	nový	k2eAgFnSc1d1	nová
Granada	Granada	k1gFnSc1	Granada
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
znovu	znovu	k6eAd1	znovu
součástí	součást	k1gFnSc7	součást
Místokrálovství	Místokrálovství	k1gNnSc4	Místokrálovství
Peru	prát	k5eAaImIp1nS	prát
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1739	[number]	k4	1739
opětovně	opětovně	k6eAd1	opětovně
Místokrálovství	Místokrálovství	k1gNnSc4	Místokrálovství
Nová	nový	k2eAgFnSc1d1	nová
Granada	Granada	k1gFnSc1	Granada
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
vlajkami	vlajka	k1gFnPc7	vlajka
užívanými	užívaný	k2eAgFnPc7d1	užívaná
na	na	k7c4	na
území	území	k1gNnSc4	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
byly	být	k5eAaImAgInP	být
vlajky	vlajka	k1gFnPc4	vlajka
španělské	španělský	k2eAgNnSc4d1	španělské
(	(	kIx(	(
<g/>
užívání	užívání	k1gNnSc4	užívání
inckých	incký	k2eAgFnPc2d1	incká
vlajek	vlajka	k1gFnPc2	vlajka
je	být	k5eAaImIp3nS	být
nejasné	jasný	k2eNgNnSc1d1	nejasné
<g/>
)	)	kIx)	)
<g/>
.10	.10	k4	.10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1809	[number]	k4	1809
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Quitu	Quit	k1gInSc6	Quit
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
revoluční	revoluční	k2eAgFnSc1d1	revoluční
junta	junta	k1gFnSc1	junta
<g/>
,	,	kIx,	,
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
nezávislost	nezávislost	k1gFnSc1	nezávislost
a	a	k8xC	a
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
první	první	k4xOgFnSc1	první
národní	národní	k2eAgFnSc1d1	národní
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
:	:	kIx,	:
celočervený	celočervený	k2eAgInSc1d1	celočervený
list	list	k1gInSc1	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
asi	asi	k9	asi
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
bílý	bílý	k2eAgInSc1d1	bílý
Ondřejský	ondřejský	k2eAgInSc1d1	ondřejský
kříž	kříž	k1gInSc1	kříž
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
měla	mít	k5eAaImAgFnS	mít
poměr	poměr	k1gInSc4	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
potlačeno	potlačen	k2eAgNnSc1d1	potlačeno
a	a	k8xC	a
užívány	užíván	k2eAgFnPc1d1	užívána
byly	být	k5eAaImAgFnP	být
opět	opět	k6eAd1	opět
pouze	pouze	k6eAd1	pouze
španělské	španělský	k2eAgFnPc1d1	španělská
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.9	.9	k4	.9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1820	[number]	k4	1820
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
básníka	básník	k1gMnSc2	básník
Josého	Josý	k1gMnSc2	Josý
Joaquina	Joaquin	k1gMnSc2	Joaquin
de	de	k?	de
Olmeda	Olmed	k1gMnSc2	Olmed
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
provincie	provincie	k1gFnSc1	provincie
Guayaquil	Guayaquila	k1gFnPc2	Guayaquila
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
tohoto	tento	k3xDgInSc2	tento
státního	státní	k2eAgInSc2d1	státní
útvaru	útvar	k1gInSc2	útvar
byl	být	k5eAaImAgInS	být
list	list	k1gInSc1	list
s	s	k7c7	s
pěti	pět	k4xCc7	pět
horizontálními	horizontální	k2eAgInPc7d1	horizontální
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
střídavě	střídavě	k6eAd1	střídavě
třemi	tři	k4xCgNnPc7	tři
modrými	modrý	k2eAgNnPc7d1	modré
a	a	k8xC	a
dvěma	dva	k4xCgNnPc7	dva
bílými	bílé	k1gNnPc7	bílé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředním	prostřední	k2eAgMnSc6d1	prostřední
<g/>
,	,	kIx,	,
modrém	modrý	k2eAgInSc6d1	modrý
pruhu	pruh	k1gInSc6	pruh
byly	být	k5eAaImAgFnP	být
uprostřed	uprostřed	k7c2	uprostřed
každé	každý	k3xTgFnSc2	každý
třetiny	třetina	k1gFnSc2	třetina
vlajky	vlajka	k1gFnSc2	vlajka
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
bílé	bílý	k2eAgFnSc6d1	bílá
pěticípé	pěticípý	k2eAgFnSc6d1	pěticípá
hvězdě	hvězda	k1gFnSc6	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
města	město	k1gNnPc1	město
Cuenca	Cuenca	k1gFnSc1	Cuenca
<g/>
,	,	kIx,	,
Guayaquil	Guayaquil	k1gInSc1	Guayaquil
a	a	k8xC	a
Quito	Quito	k1gNnSc1	Quito
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
v	v	k7c4	v
různý	různý	k2eAgInSc4d1	různý
zdrojích	zdroj	k1gInPc6	zdroj
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
odstín	odstín	k1gInSc1	odstín
modré	modré	k1gNnSc1	modré
a	a	k8xC	a
poloha	poloha	k1gFnSc1	poloha
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1822	[number]	k4	1822
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
osvobozeneckých	osvobozenecký	k2eAgInPc6d1	osvobozenecký
bojích	boj	k1gInPc6	boj
Simóna	Simón	k1gMnSc4	Simón
Bolívara	Bolívar	k1gMnSc4	Bolívar
k	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
Cuency	Cuenca	k1gFnSc2	Cuenca
a	a	k8xC	a
Quita	Quit	k1gInSc2	Quit
ke	k	k7c3	k
konfederaci	konfederace	k1gFnSc3	konfederace
Velká	velký	k2eAgFnSc1d1	velká
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
juntou	junta	k1gFnSc7	junta
schválena	schválen	k2eAgFnSc1d1	schválena
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
provincie	provincie	k1gFnSc2	provincie
Guayaquil	Guayaquila	k1gFnPc2	Guayaquila
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
znovu	znovu	k6eAd1	znovu
José	Josá	k1gFnPc4	Josá
Joaquin	Joaquina	k1gFnPc2	Joaquina
de	de	k?	de
Olmedo	Olmedo	k1gNnSc4	Olmedo
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
se	s	k7c7	s
zachovaným	zachovaný	k2eAgInSc7d1	zachovaný
poměrem	poměr	k1gInSc7	poměr
stran	strana	k1gFnPc2	strana
asi	asi	k9	asi
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
s	s	k7c7	s
modrým	modrý	k2eAgNnSc7d1	modré
karé	karé	k1gNnSc7	karé
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byla	být	k5eAaImAgFnS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
odstín	odstín	k1gInSc1	odstín
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
zdrojích	zdroj	k1gInPc6	zdroj
znovu	znovu	k6eAd1	znovu
liší	lišit	k5eAaImIp3nS	lišit
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
Velké	velký	k2eAgFnSc3d1	velká
Kolumbii	Kolumbie	k1gFnSc3	Kolumbie
připojen	připojit	k5eAaPmNgInS	připojit
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
území	území	k1gNnSc2	území
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
a	a	k8xC	a
ve	v	k7c6	v
vzniklém	vzniklý	k2eAgInSc6d1	vzniklý
departementu	departement	k1gInSc6	departement
Jižní	jižní	k2eAgFnSc1d1	jižní
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
užívat	užívat	k5eAaImF	užívat
vlajka	vlajka	k1gFnSc1	vlajka
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
(	(	kIx(	(
<g/>
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
variantách	varianta	k1gFnPc6	varianta
dle	dle	k7c2	dle
zdroje	zdroj	k1gInSc2	zdroj
<g/>
)	)	kIx)	)
<g/>
.11	.11	k4	.11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1830	[number]	k4	1830
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nezávislý	závislý	k2eNgInSc1d1	nezávislý
Stát	stát	k1gInSc1	stát
Jižní	jižní	k2eAgFnSc2d1	jižní
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
vlajky	vlajka	k1gFnSc2	vlajka
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
znak	znak	k1gInSc1	znak
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
nahrazen	nahradit	k5eAaPmNgInS	nahradit
znakem	znak	k1gInSc7	znak
novým	nový	k2eAgInSc7d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
znaku	znak	k1gInSc6	znak
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
černý	černý	k2eAgInSc1d1	černý
nápis	nápis	k1gInSc1	nápis
EL	Ela	k1gFnPc2	Ela
ECUADOR	Ecuador	k1gInSc4	Ecuador
EN	EN	kA	EN
COLOMBIA	COLOMBIA	kA	COLOMBIA
ale	ale	k8xC	ale
až	až	k9	až
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
oficiálně	oficiálně	k6eAd1	oficiálně
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Stát	stát	k1gInSc4	stát
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
(	(	kIx(	(
<g/>
ecuador	ecuador	k1gInSc1	ecuador
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
rovník	rovník	k1gInSc1	rovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
a	a	k8xC	a
svržení	svržení	k1gNnSc4	svržení
prezidenta	prezident	k1gMnSc2	prezident
Florese	Florese	k1gFnSc2	Florese
<g/>
,	,	kIx,	,
přijala	přijmout	k5eAaPmAgFnS	přijmout
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1845	[number]	k4	1845
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
ekvádorská	ekvádorský	k2eAgFnSc1d1	ekvádorská
vláda	vláda	k1gFnSc1	vláda
novou	nový	k2eAgFnSc4d1	nová
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
sestávala	sestávat	k5eAaImAgFnS	sestávat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
vertikálních	vertikální	k2eAgInPc2d1	vertikální
pruhů	pruh	k1gInPc2	pruh
<g/>
:	:	kIx,	:
bílého	bílé	k1gNnSc2	bílé
<g/>
,	,	kIx,	,
modrého	modré	k1gNnSc2	modré
a	a	k8xC	a
bílého	bílé	k1gNnSc2	bílé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředním	prostřední	k2eAgInSc6d1	prostřední
modrém	modrý	k2eAgInSc6d1	modrý
pruhu	pruh	k1gInSc6	pruh
byla	být	k5eAaImAgFnS	být
trojice	trojice	k1gFnSc1	trojice
bílých	bílý	k2eAgFnPc2d1	bílá
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byl	být	k5eAaImAgInS	být
národním	národní	k2eAgInSc7d1	národní
konventem	konvent	k1gInSc7	konvent
počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
změněn	změněn	k2eAgMnSc1d1	změněn
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
počtu	počet	k1gInSc3	počet
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Rozmístění	rozmístění	k1gNnSc1	rozmístění
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
odstín	odstín	k1gInSc1	odstín
modré	modré	k1gNnSc1	modré
nebyly	být	k5eNaImAgFnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
přesně	přesně	k6eAd1	přesně
definovány	definovat	k5eAaBmNgInP	definovat
<g/>
,	,	kIx,	,
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
několik	několik	k4yIc1	několik
variant	varianta	k1gFnPc2	varianta
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlajky	vlajka	k1gFnPc1	vlajka
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k9	jako
vlajky	vlajka	k1gFnPc1	vlajka
národní	národní	k2eAgFnSc1d1	národní
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
měla	mít	k5eAaImAgFnS	mít
pod	pod	k7c7	pod
hvězdami	hvězda	k1gFnPc7	hvězda
nový	nový	k2eAgInSc4d1	nový
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1860	[number]	k4	1860
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
požadavků	požadavek	k1gInPc2	požadavek
části	část	k1gFnSc2	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
po	po	k7c6	po
povstání	povstání	k1gNnSc6	povstání
v	v	k7c6	v
provinciích	provincie	k1gFnPc6	provincie
Guayaquil	Guayaquil	k1gInSc1	Guayaquil
a	a	k8xC	a
Cuenca	Cuenca	k1gFnSc1	Cuenca
<g/>
,	,	kIx,	,
přijat	přijat	k2eAgInSc1d1	přijat
vládou	vláda	k1gFnSc7	vláda
dekret	dekret	k1gInSc1	dekret
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
měla	mít	k5eAaImAgFnS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
:	:	kIx,	:
žlutý	žlutý	k2eAgInSc4d1	žlutý
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc4d1	modrý
a	a	k8xC	a
červený	červený	k2eAgInSc4d1	červený
se	s	k7c7	s
šířkami	šířka	k1gFnPc7	šířka
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
též	též	k9	též
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1861	[number]	k4	1861
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
přijata	přijmout	k5eAaPmNgFnS	přijmout
téměř	téměř	k6eAd1	téměř
shodná	shodný	k2eAgFnSc1d1	shodná
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
lišící	lišící	k2eAgFnSc1d1	lišící
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
poměrem	poměr	k1gInSc7	poměr
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
častým	častý	k2eAgFnPc3d1	častá
záměnám	záměna	k1gFnPc3	záměna
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
1272	[number]	k4	1272
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1900	[number]	k4	1900
ekvádorská	ekvádorský	k2eAgFnSc1d1	ekvádorská
státní	státní	k2eAgFnSc1d1	státní
vlajka	vlajka	k1gFnSc1	vlajka
upravena	upravit	k5eAaPmNgFnS	upravit
přidáním	přidání	k1gNnSc7	přidání
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
doprostřed	doprostřed	k7c2	doprostřed
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
(	(	kIx(	(
<g/>
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Pedro	Pedro	k1gNnSc4	Pedro
Pablo	Pablo	k1gNnSc4	Pablo
Traversari	Traversar	k1gFnSc2	Traversar
Salazar	Salazara	k1gFnPc2	Salazara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
drobná	drobný	k2eAgFnSc1d1	drobná
úprava	úprava	k1gFnSc1	úprava
znaku	znak	k1gInSc2	znak
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
barva	barva	k1gFnSc1	barva
lodi	loď	k1gFnSc2	loď
změněna	změnit	k5eAaPmNgFnS	změnit
z	z	k7c2	z
šedé	šedá	k1gFnSc2	šedá
na	na	k7c4	na
žluto	žluto	k1gNnSc4	žluto
<g/>
–	–	k?	–
<g/>
modro	modro	k6eAd1	modro
<g/>
–	–	k?	–
<g/>
červenou	červená	k1gFnSc4	červená
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
ekvádorské	ekvádorský	k2eAgFnSc2d1	ekvádorská
vlajky	vlajka	k1gFnSc2	vlajka
změněn	změnit	k5eAaPmNgInS	změnit
z	z	k7c2	z
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Den	den	k1gInSc4	den
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
ekvádorských	ekvádorský	k2eAgInPc2d1	ekvádorský
státních	státní	k2eAgInPc2d1	státní
svátků	svátek	k1gInPc2	svátek
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Připomíná	připomínat	k5eAaImIp3nS	připomínat
přijetí	přijetí	k1gNnSc4	přijetí
dekretu	dekret	k1gInSc2	dekret
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
ekvádorskou	ekvádorský	k2eAgFnSc7d1	ekvádorská
vládou	vláda	k1gFnSc7	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
upravena	upravit	k5eAaPmNgFnS	upravit
přidáním	přidání	k1gNnSc7	přidání
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
(	(	kIx(	(
<g/>
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
možné	možný	k2eAgFnPc1d1	možná
záměny	záměna	k1gFnPc1	záměna
s	s	k7c7	s
kolumbijskou	kolumbijský	k2eAgFnSc7d1	kolumbijská
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
přijatou	přijatý	k2eAgFnSc4d1	přijatá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc1	vlajka
ekvádorských	ekvádorský	k2eAgFnPc2d1	ekvádorská
provincií	provincie	k1gFnPc2	provincie
==	==	k?	==
</s>
</p>
<p>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
24	[number]	k4	24
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc1	všechen
užívají	užívat	k5eAaImIp3nP	užívat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
i	i	k9	i
poměry	poměr	k1gInPc1	poměr
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dle	dle	k7c2	dle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
poměr	poměr	k1gInSc4	poměr
stran	stran	k7c2	stran
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
provincií	provincie	k1gFnPc2	provincie
Santa	Santa	k1gFnSc1	Santa
Elena	Elena	k1gFnSc1	Elena
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
)	)	kIx)	)
a	a	k8xC	a
Santo	Santo	k1gNnSc1	Santo
Domingo	Domingo	k1gMnSc1	Domingo
de	de	k?	de
los	los	k1gMnSc1	los
Tsáchilas	Tsáchilas	k1gMnSc1	Tsáchilas
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
–	–	k?	–
ty	ten	k3xDgInPc1	ten
mají	mít	k5eAaImIp3nP	mít
poměr	poměr	k1gInSc1	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
</s>
</p>
<p>
<s>
Ekvádorská	ekvádorský	k2eAgFnSc1d1	ekvádorská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ekvádorská	ekvádorský	k2eAgFnSc1d1	ekvádorská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
