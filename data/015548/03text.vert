<s>
Mořkovský	Mořkovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Mořkovský	Mořkovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
Mořkovský	Mořkovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
427,1	427,1	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Západobeskydské	Západobeskydský	k2eAgNnSc1d1
podhůří	podhůří	k1gNnSc1
/	/	kIx~
Podbeskydská	Podbeskydský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
Štramberská	štramberský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Petřkovické	petřkovický	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
49	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Mořkovský	Mořkovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Povodí	povodí	k1gNnSc1
</s>
<s>
Jičínka	Jičínka	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mořkovský	Mořkovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
je	být	k5eAaImIp3nS
kopec	kopec	k1gInSc4
v	v	k7c6
Podbeskydské	Podbeskydský	k2eAgFnSc6d1
pahorkatině	pahorkatina	k1gFnSc6
<g/>
,	,	kIx,
vypínající	vypínající	k2eAgInPc4d1
se	se	k3xPyFc4
do	do	k7c2
výšky	výška	k1gFnSc2
427,1	427,1	k4
m	m	kA
n.	n.	k?
m.	m.	k?
severozápadně	severozápadně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Mořkov	Mořkov	k1gInSc1
a	a	k8xC
jižně	jižně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Životice	Životice	k1gFnSc2
u	u	k7c2
Nového	Nového	k2eAgInSc2d1
Jičína	Jičín	k1gInSc2
(	(	kIx(
<g/>
hranice	hranice	k1gFnSc1
obou	dva	k4xCgInPc2
katastrů	katastr	k1gInPc2
probíhá	probíhat	k5eAaImIp3nS
napříč	napříč	k7c7
vrcholem	vrchol	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pastvinách	pastvina	k1gFnPc6
na	na	k7c6
jeho	jeho	k3xOp3gInPc6
západních	západní	k2eAgInPc6d1
podvrcholových	podvrcholový	k2eAgInPc6d1
svazích	svah	k1gInPc6
roste	růst	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čičorečka	čičorečka	k1gFnSc1
pestrá	pestrý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Securigera	Securigera	k1gFnSc1
varia	varia	k1gNnPc5
<g/>
)	)	kIx)
</s>
<s>
Dobromysl	dobromysl	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Origanum	Origanum	k1gInSc1
vulgare	vulgar	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Chrastavec	chrastavec	k1gInSc1
rolní	rolní	k2eAgInSc1d1
(	(	kIx(
<g/>
Knautia	Knautia	k1gFnSc1
arvensis	arvensis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Klinopád	klinopád	k1gInSc1
obecný	obecný	k2eAgInSc1d1
(	(	kIx(
<g/>
Clinopodium	Clinopodium	k1gNnSc1
vulgare	vulgar	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Pryskyřník	pryskyřník	k1gInSc1
mnohokvětý	mnohokvětý	k2eAgInSc1d1
(	(	kIx(
<g/>
Ranunculus	Ranunculus	k1gInSc1
polyanthemos	polyanthemos	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Pupava	pupava	k1gFnSc1
bezlodyžná	bezlodyžný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Carlina	Carlin	k2eAgFnSc1d1
acaulis	acaulis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Řepík	řepík	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
(	(	kIx(
<g/>
Agrimonia	Agrimonium	k1gNnSc2
eupatoria	eupatorium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Tolice	tolice	k1gFnSc1
srpovitá	srpovitý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Medicago	Medicago	k6eAd1
falcata	falcat	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Trnka	trnka	k1gFnSc1
obecná	obecná	k1gFnSc1
(	(	kIx(
<g/>
Prunus	Prunus	k1gInSc1
spinosa	spinosa	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
je	být	k5eAaImIp3nS
umístěna	umístěn	k2eAgFnSc1d1
základnová	základnový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
(	(	kIx(
<g/>
BTS	BTS	kA
<g/>
)	)	kIx)
společností	společnost	k1gFnPc2
O2	O2	k1gMnPc2
Czech	Czech	k1gInSc4
Republic	Republice	k1gFnPc2
a	a	k8xC
T-Mobile	T-Mobila	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Mořkovský	Mořkovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
:	:	kIx,
Muzeum	muzeum	k1gNnSc1
Novojičínska	Novojičínsko	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
HÁLA	Hála	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
7702	#num#	k4
NJ	NJ	kA
Mořkov	Mořkov	k1gInSc4
<g/>
,	,	kIx,
Mořkovský	Mořkovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
427	#num#	k4
m	m	kA
<g/>
,	,	kIx,
betonový	betonový	k2eAgInSc1d1
stožár	stožár	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeseník	Jeseník	k1gInSc4
<g/>
:	:	kIx,
GSMweb	GSMwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-08-10	2011-08-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HÁLA	Hála	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
242605800	#num#	k4
NJ	NJ	kA
Mořkov	Mořkov	k1gInSc4
(	(	kIx(
<g/>
Mořkovský	Mořkovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
427	#num#	k4
m	m	kA
<g/>
,	,	kIx,
betonový	betonový	k2eAgInSc1d1
stožár	stožár	k1gInSc1
<g/>
,	,	kIx,
+	+	kIx~
<g/>
O	o	k7c4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeseník	Jeseník	k1gInSc4
<g/>
:	:	kIx,
GSMweb	GSMwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-08-10	2011-08-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
