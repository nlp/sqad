<s>
Jehlan	jehlan	k1gInSc1	jehlan
je	být	k5eAaImIp3nS	být
trojrozměrné	trojrozměrný	k2eAgNnSc4d1	trojrozměrné
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
základnu	základna	k1gFnSc4	základna
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
podstavu	podstava	k1gFnSc4	podstava
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
mnohoúhelník	mnohoúhelník	k1gInSc4	mnohoúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholy	vrchol	k1gInPc1	vrchol
základny	základna	k1gFnSc2	základna
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
bodem	bod	k1gInSc7	bod
mimo	mimo	k7c4	mimo
rovinu	rovina	k1gFnSc4	rovina
základny	základna	k1gFnSc2	základna
–	–	k?	–
tento	tento	k3xDgInSc4	tento
bod	bod	k1gInSc4	bod
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc4d1	hlavní
<g/>
)	)	kIx)	)
vrchol	vrchol	k1gInSc4	vrchol
jehlanu	jehlan	k1gInSc2	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
Kolmá	kolmý	k2eAgFnSc1d1	kolmá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
vrcholu	vrchol	k1gInSc2	vrchol
od	od	k7c2	od
roviny	rovina	k1gFnSc2	rovina
podstavy	podstava	k1gFnSc2	podstava
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
výška	výška	k1gFnSc1	výška
jehlanu	jehlan	k1gInSc2	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
jehlanu	jehlan	k1gInSc2	jehlan
se	se	k3xPyFc4	se
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
v	v	k7c6	v
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
S_	S_	k1gFnSc1	S_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S_	S_	k1gFnSc6	S_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc4	obsah
podstavy	podstava	k1gFnSc2	podstava
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c4	v
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
výška	výška	k1gFnSc1	výška
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
jehlanu	jehlan	k1gInSc2	jehlan
se	se	k3xPyFc4	se
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
jako	jako	k9	jako
součet	součet	k1gInSc1	součet
obsahu	obsah	k1gInSc2	obsah
základny	základna	k1gFnSc2	základna
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
trojúhelníkových	trojúhelníkový	k2eAgFnPc2d1	trojúhelníková
stěn	stěna	k1gFnPc2	stěna
-	-	kIx~	-
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
počtem	počet	k1gInSc7	počet
stran	stran	k7c2	stran
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
P	P	kA	P
+	+	kIx~	+
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
P	P	kA	P
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc4	obsah
podstavy	podstava	k1gFnSc2	podstava
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc4	obsah
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgInPc6d1	uvedený
vzorcích	vzorec	k1gInPc6	vzorec
je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
budu	být	k5eAaImBp1nS	být
vrchol	vrchol	k1gInSc1	vrchol
jehlanu	jehlan	k1gInSc2	jehlan
posunovat	posunovat	k5eAaImF	posunovat
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
rovnoběžné	rovnoběžný	k2eAgInPc1d1	rovnoběžný
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
nemění	měnit	k5eNaImIp3nS	měnit
se	se	k3xPyFc4	se
objem	objem	k1gInSc1	objem
(	(	kIx(	(
<g/>
obsah	obsah	k1gInSc1	obsah
podstavy	podstava	k1gFnSc2	podstava
i	i	k8xC	i
výška	výška	k1gFnSc1	výška
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
stejné	stejný	k2eAgInPc1d1	stejný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
povrch	povrch	k1gInSc4	povrch
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
posouvání	posouvání	k1gNnSc6	posouvání
vrcholu	vrchol	k1gInSc2	vrchol
"	"	kIx"	"
<g/>
dostatečně	dostatečně	k6eAd1	dostatečně
daleko	daleko	k6eAd1	daleko
<g/>
"	"	kIx"	"
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
rovině	rovina	k1gFnSc6	rovina
růst	růst	k1gInSc1	růst
nad	nad	k7c4	nad
všechny	všechen	k3xTgFnPc4	všechen
meze	mez	k1gFnPc4	mez
<g/>
.	.	kIx.	.
</s>
<s>
Jehlan	jehlan	k1gInSc1	jehlan
nemůže	moct	k5eNaImIp3nS	moct
nikdy	nikdy	k6eAd1	nikdy
být	být	k5eAaImF	být
středově	středově	k6eAd1	středově
souměrný	souměrný	k2eAgMnSc1d1	souměrný
<g/>
.	.	kIx.	.
</s>
<s>
Jehlan	jehlan	k1gInSc1	jehlan
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrný	souměrný	k2eAgInSc1d1	souměrný
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
základna	základna	k1gFnSc1	základna
středově	středově	k6eAd1	středově
souměrná	souměrný	k2eAgFnSc1d1	souměrná
a	a	k8xC	a
průmět	průmět	k1gInSc1	průmět
vrcholu	vrchol	k1gInSc2	vrchol
jehlanu	jehlan	k1gInSc2	jehlan
do	do	k7c2	do
roviny	rovina	k1gFnSc2	rovina
základny	základna	k1gFnSc2	základna
je	být	k5eAaImIp3nS	být
shodný	shodný	k2eAgInSc1d1	shodný
se	s	k7c7	s
středem	střed	k1gInSc7	střed
souměrnosti	souměrnost	k1gFnSc2	souměrnost
základny	základna	k1gFnSc2	základna
(	(	kIx(	(
<g/>
laičtěji	laicky	k6eAd2	laicky
<g/>
:	:	kIx,	:
vrchol	vrchol	k1gInSc1	vrchol
jehlanu	jehlan	k1gInSc2	jehlan
musí	muset	k5eAaImIp3nS	muset
ležet	ležet	k5eAaImF	ležet
"	"	kIx"	"
<g/>
kolmo	kolmo	k6eAd1	kolmo
nad	nad	k7c7	nad
středem	střed	k1gInSc7	střed
souměrnosti	souměrnost	k1gFnSc2	souměrnost
základny	základna	k1gFnSc2	základna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osou	osa	k1gFnSc7	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
spojnice	spojnice	k1gFnSc1	spojnice
vrcholu	vrchol	k1gInSc2	vrchol
se	s	k7c7	s
středem	střed	k1gInSc7	střed
souměrnosti	souměrnost	k1gFnSc2	souměrnost
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Jehlan	jehlan	k1gInSc1	jehlan
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovinově	rovinově	k6eAd1	rovinově
souměrný	souměrný	k2eAgInSc4d1	souměrný
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
základna	základna	k1gFnSc1	základna
osově	osově	k6eAd1	osově
souměrná	souměrný	k2eAgFnSc1d1	souměrná
a	a	k8xC	a
průmět	průmět	k1gInSc1	průmět
vrcholu	vrchol	k1gInSc2	vrchol
jehlanu	jehlan	k1gInSc2	jehlan
do	do	k7c2	do
roviny	rovina	k1gFnSc2	rovina
základny	základna	k1gFnSc2	základna
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Lidštěji	lidsky	k6eAd2	lidsky
<g/>
:	:	kIx,	:
vrchol	vrchol	k1gInSc1	vrchol
jehlanu	jehlan	k1gInSc2	jehlan
musí	muset	k5eAaImIp3nS	muset
ležet	ležet	k5eAaImF	ležet
"	"	kIx"	"
<g/>
kolmo	kolmo	k6eAd1	kolmo
nad	nad	k7c7	nad
osou	osa	k1gFnSc7	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
základny	základna	k1gFnSc2	základna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Rovinou	rovina	k1gFnSc7	rovina
souměrnosti	souměrnost	k1gFnSc2	souměrnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
rovina	rovina	k1gFnSc1	rovina
určená	určený	k2eAgFnSc1d1	určená
osou	osa	k1gFnSc7	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
základny	základna	k1gFnSc2	základna
a	a	k8xC	a
vrcholem	vrchol	k1gInSc7	vrchol
jehlanu	jehlan	k1gInSc2	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tvoří	tvořit	k5eAaImIp3nP	tvořit
základnu	základna	k1gFnSc4	základna
jehlanu	jehlan	k1gInSc2	jehlan
mnohoúhelník	mnohoúhelník	k1gInSc4	mnohoúhelník
o	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
stranách	strana	k1gFnPc6	strana
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jehlan	jehlan	k1gInSc1	jehlan
<g/>
:	:	kIx,	:
celkem	celkem	k6eAd1	celkem
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
vrcholů	vrchol	k1gInPc2	vrchol
celkem	celkem	k6eAd1	celkem
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
⋅	⋅	k?	⋅
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
hran	hrana	k1gFnPc2	hrana
celkem	celkem	k6eAd1	celkem
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
stěn	stěn	k1gInSc1	stěn
Jehlan	jehlan	k1gInSc1	jehlan
nemá	mít	k5eNaImIp3nS	mít
tělesové	tělesový	k2eAgFnPc4d1	tělesová
úhlopříčky	úhlopříčka	k1gFnPc4	úhlopříčka
<g/>
,	,	kIx,	,
stěnové	stěnový	k2eAgInPc1d1	stěnový
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
v	v	k7c6	v
základně	základna	k1gFnSc6	základna
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
n	n	k0	n
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jehlan	jehlan	k1gInSc1	jehlan
je	být	k5eAaImIp3nS	být
konvexní	konvexní	k2eAgInSc1d1	konvexní
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
konvexní	konvexní	k2eAgFnSc1d1	konvexní
jeho	jeho	k3xOp3gFnSc1	jeho
základna	základna	k1gFnSc1	základna
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
základnou	základna	k1gFnSc7	základna
jehlanu	jehlan	k1gInSc2	jehlan
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
mnohoúhelník	mnohoúhelník	k1gInSc1	mnohoúhelník
a	a	k8xC	a
vrchol	vrchol	k1gInSc1	vrchol
leží	ležet	k5eAaImIp3nS	ležet
kolmo	kolmo	k6eAd1	kolmo
nad	nad	k7c7	nad
těžištěm	těžiště	k1gNnSc7	těžiště
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
jehlanu	jehlan	k1gInSc6	jehlan
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Pravidelnost	pravidelnost	k1gFnSc1	pravidelnost
<g/>
"	"	kIx"	"
jehlanu	jehlan	k1gInSc2	jehlan
obvykle	obvykle	k6eAd1	obvykle
podstatně	podstatně	k6eAd1	podstatně
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
výpočet	výpočet	k1gInSc1	výpočet
jeho	on	k3xPp3gInSc2	on
objemu	objem	k1gInSc2	objem
a	a	k8xC	a
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
čtyřstěn	čtyřstěn	k1gInSc1	čtyřstěn
je	být	k5eAaImIp3nS	být
jehlan	jehlan	k1gInSc4	jehlan
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
základnu	základna	k1gFnSc4	základna
i	i	k8xC	i
všechny	všechen	k3xTgInPc1	všechen
tři	tři	k4xCgInPc1	tři
boční	boční	k2eAgInPc1d1	boční
stěny	stěn	k1gInPc1	stěn
jsou	být	k5eAaImIp3nP	být
rovnostranné	rovnostranný	k2eAgInPc1d1	rovnostranný
trojúhelníky	trojúhelník	k1gInPc1	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čtyřstěn	čtyřstěn	k1gInSc1	čtyřstěn
má	mít	k5eAaImIp3nS	mít
stejný	stejný	k2eAgInSc1d1	stejný
tvar	tvar	k1gInSc1	tvar
všech	všecek	k3xTgFnPc2	všecek
stěn	stěna	k1gFnPc2	stěna
i	i	k9	i
délku	délka	k1gFnSc4	délka
všech	všecek	k3xTgFnPc2	všecek
hran	hrana	k1gFnPc2	hrana
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
platónských	platónský	k2eAgNnPc2d1	platónské
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
objem	objem	k1gInSc1	objem
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
a	a	k8xC	a
obsah	obsah	k1gInSc4	obsah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
z	z	k7c2	z
délky	délka	k1gFnSc2	délka
jeho	jeho	k3xOp3gFnSc2	jeho
hrany	hrana	k1gFnSc2	hrana
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
matrix	matrix	k1gInSc1	matrix
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
matrix	matrix	k1gInSc1	matrix
<g/>
}}	}}	k?	}}
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Jeho	jeho	k3xOp3gFnSc4	jeho
výšku	výška	k1gFnSc4	výška
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
=	=	kIx~	=
(	(	kIx(	(
a	a	k8xC	a
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
3	[number]	k4	3
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
jehlan	jehlan	k1gInSc1	jehlan
čtvercovou	čtvercový	k2eAgFnSc4d1	čtvercová
základnu	základna	k1gFnSc4	základna
a	a	k8xC	a
vrchol	vrchol	k1gInSc4	vrchol
kolmo	kolmo	k6eAd1	kolmo
nad	nad	k7c7	nad
průsečíkem	průsečík	k1gInSc7	průsečík
úhlopříček	úhlopříčka	k1gFnPc2	úhlopříčka
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
čtyřbokém	čtyřboký	k2eAgInSc6d1	čtyřboký
jehlanu	jehlan	k1gInSc6	jehlan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
objem	objem	k1gInSc1	objem
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
a	a	k8xC	a
povrch	povrch	k1gInSc4	povrch
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
z	z	k7c2	z
délky	délka	k1gFnSc2	délka
strany	strana	k1gFnSc2	strana
základny	základna	k1gFnSc2	základna
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
a	a	k8xC	a
výšky	výška	k1gFnSc2	výška
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c4	v
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
v	v	k7c4	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
abv	abv	k?	abv
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
=	=	kIx~	=
a	a	k8xC	a
b	b	k?	b
+	+	kIx~	+
a	a	k8xC	a
:	:	kIx,	:
w	w	k?	w
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
b	b	k?	b
:	:	kIx,	:
w	w	k?	w
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
ab	ab	k?	ab
<g/>
+	+	kIx~	+
<g/>
aw_	aw_	k?	aw_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
bw_	bw_	k?	bw_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Rektorys	Rektorys	k1gInSc1	Rektorys
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Přehled	přehled	k1gInSc1	přehled
užité	užitý	k2eAgFnSc2d1	užitá
matematiky	matematika	k1gFnSc2	matematika
I	i	k9	i
<g/>
,	,	kIx,	,
Prometheus	Prometheus	k1gMnSc1	Prometheus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85849	[number]	k4	85849
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
104-106	[number]	k4	104-106
Marcela	Marcela	k1gFnSc1	Marcela
Palková	Palková	k1gFnSc1	Palková
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Průvodce	průvodce	k1gMnSc1	průvodce
matematikou	matematika	k1gFnSc7	matematika
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Didaktis	Didaktis	k1gFnSc1	Didaktis
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7358	[number]	k4	7358
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
117-120	[number]	k4	117-120
Geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
Mnohostěn	mnohostěn	k1gInSc1	mnohostěn
Komolý	komolý	k2eAgInSc1d1	komolý
jehlan	jehlan	k1gInSc1	jehlan
Válec	válec	k1gInSc1	válec
Platónská	platónský	k2eAgFnSc1d1	platónská
tělesa	těleso	k1gNnSc2	těleso
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jehlan	jehlan	k1gInSc1	jehlan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jehlan	jehlan	k1gInSc1	jehlan
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
