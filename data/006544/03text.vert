<s>
Nukleové	nukleový	k2eAgFnPc1d1	nukleová
báze	báze	k1gFnPc1	báze
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnSc7d1	základní
součástí	součást	k1gFnSc7	součást
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
báze	báze	k1gFnPc4	báze
purinové	purinový	k2eAgFnPc4d1	purinová
(	(	kIx(	(
<g/>
adenin	adenin	k1gInSc4	adenin
<g/>
,	,	kIx,	,
guanin	guanin	k1gInSc4	guanin
<g/>
)	)	kIx)	)
a	a	k8xC	a
báze	báze	k1gFnPc1	báze
pyrimidinové	pyrimidinový	k2eAgFnPc1d1	pyrimidinová
(	(	kIx(	(
<g/>
cytosin	cytosin	k1gInSc1	cytosin
<g/>
,	,	kIx,	,
uracil	uracil	k1gInSc1	uracil
<g/>
,	,	kIx,	,
thymin	thymin	k1gInSc1	thymin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
doplňkové	doplňkový	k2eAgFnPc4d1	doplňková
dvojice	dvojice	k1gFnPc4	dvojice
(	(	kIx(	(
<g/>
komplementární	komplementární	k2eAgInPc4d1	komplementární
páry	pár	k1gInPc4	pár
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
bp	bp	k?	bp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
vždy	vždy	k6eAd1	vždy
1	[number]	k4	1
purinová	purinový	k2eAgFnSc1d1	purinová
a	a	k8xC	a
1	[number]	k4	1
pyrimidinová	pyrimidinový	k2eAgFnSc1d1	pyrimidinová
báze	báze	k1gFnSc1	báze
vzájemně	vzájemně	k6eAd1	vzájemně
vážou	vázat	k5eAaImIp3nP	vázat
vodíkovými	vodíkový	k2eAgFnPc7d1	vodíková
vazbami	vazba	k1gFnPc7	vazba
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
komplementarita	komplementarita	k1gFnSc1	komplementarita
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Guanin	guanin	k1gInSc1	guanin
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
s	s	k7c7	s
cytosinem	cytosin	k1gInSc7	cytosin
a	a	k8xC	a
adenin	adenin	k1gInSc1	adenin
s	s	k7c7	s
thyminem	thymin	k1gInSc7	thymin
nebo	nebo	k8xC	nebo
s	s	k7c7	s
uracilem	uracil	k1gInSc7	uracil
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
kód	kód	k1gInSc4	kód
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Komplementární	komplementární	k2eAgNnSc1d1	komplementární
párování	párování	k1gNnSc1	párování
pak	pak	k6eAd1	pak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tuto	tento	k3xDgFnSc4	tento
informaci	informace	k1gFnSc4	informace
realizovat	realizovat	k5eAaBmF	realizovat
při	při	k7c6	při
procesech	proces	k1gInPc6	proces
replikace	replikace	k1gFnSc2	replikace
<g/>
,	,	kIx,	,
transkripce	transkripce	k1gFnSc2	transkripce
a	a	k8xC	a
translace	translace	k1gFnSc2	translace
<g/>
.	.	kIx.	.
</s>
<s>
Genom	genom	k1gInSc1	genom
daného	daný	k2eAgInSc2d1	daný
organizmu	organizmus	k1gInSc2	organizmus
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
stálý	stálý	k2eAgInSc1d1	stálý
počet	počet	k1gInSc1	počet
komplementárních	komplementární	k2eAgInPc2d1	komplementární
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
bp	bp	k?	bp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
v	v	k7c6	v
kilobázích	kilobáh	k1gInPc6	kilobáh
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
kbp	kbp	k?	kbp
-	-	kIx~	-
kilobase	kilobase	k6eAd1	kilobase
pair	pair	k1gMnSc1	pair
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
1000	[number]	k4	1000
bp	bp	k?	bp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
megabázích	megabáze	k1gFnPc6	megabáze
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Mbp	Mbp	k1gFnSc6	Mbp
-	-	kIx~	-
megabase	megabase	k6eAd1	megabase
pair	pair	k1gMnSc1	pair
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
milion	milion	k4xCgInSc1	milion
bp	bp	k?	bp
<g/>
)	)	kIx)	)
či	či	k8xC	či
gigabázích	gigabáze	k1gFnPc6	gigabáze
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Gbp	Gbp	k1gFnSc6	Gbp
-	-	kIx~	-
gigabase	gigabase	k6eAd1	gigabase
pair	pair	k1gMnSc1	pair
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
miliarda	miliarda	k4xCgFnSc1	miliarda
bp	bp	k?	bp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
základních	základní	k2eAgFnPc2d1	základní
pěti	pět	k4xCc2	pět
bází	báze	k1gFnPc2	báze
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
modifikovaných	modifikovaný	k2eAgFnPc2d1	modifikovaná
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgMnSc1d3	Nejběžnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
následující	následující	k2eAgInPc4d1	následující
nukleosidy	nukleosid	k1gInPc4	nukleosid
<g/>
:	:	kIx,	:
modifikace	modifikace	k1gFnPc4	modifikace
uridinu	uridina	k1gFnSc4	uridina
<g/>
:	:	kIx,	:
ribothymidin	ribothymidin	k2eAgInSc1d1	ribothymidin
dihydrouridin	dihydrouridin	k2eAgInSc1d1	dihydrouridin
thiouridin	thiouridin	k2eAgInSc1d1	thiouridin
pseudouridin	pseudouridin	k2eAgInSc1d1	pseudouridin
methylkarbonyl	methylkarbonyl	k1gInSc1	methylkarbonyl
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
modifikace	modifikace	k1gFnSc2	modifikace
cytidinu	cytidin	k1gInSc2	cytidin
5	[number]	k4	5
<g/>
-methylcytidin	ethylcytidin	k2eAgInSc4d1	-methylcytidin
5	[number]	k4	5
<g/>
-hydroxymethylcytidin	ydroxymethylcytidin	k2eAgInSc4d1	-hydroxymethylcytidin
5	[number]	k4	5
<g/>
-formylcytidin	ormylcytidin	k2eAgInSc4d1	-formylcytidin
5	[number]	k4	5
<g/>
-karboxylcytidin	arboxylcytidin	k2eAgInSc4d1	-karboxylcytidin
2	[number]	k4	2
<g/>
-lysylcytidin	ysylcytidin	k1gInSc4	-lysylcytidin
modifikace	modifikace	k1gFnSc2	modifikace
adenosinu	adenosina	k1gFnSc4	adenosina
inosin	inosin	k1gInSc1	inosin
N	N	kA	N
<g/>
6	[number]	k4	6
<g/>
-methyladenosin	ethyladenosin	k1gInSc4	-methyladenosin
N	N	kA	N
<g/>
6	[number]	k4	6
<g/>
-isopentenyladenosin	sopentenyladenosin	k1gInSc4	-isopentenyladenosin
modifikace	modifikace	k1gFnSc2	modifikace
guanosinu	guanosin	k2eAgMnSc3d1	guanosin
N	N	kA	N
<g/>
7	[number]	k4	7
<g/>
-methylguanosin	ethylguanosin	k2eAgInSc1d1	-methylguanosin
queosin	queosin	k2eAgInSc1d1	queosin
wyosin	wyosin	k1gInSc1	wyosin
Nejčastější	častý	k2eAgFnSc2d3	nejčastější
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
purinové	purinový	k2eAgFnSc2d1	purinová
báze	báze	k1gFnSc2	báze
Nejčastější	častý	k2eAgFnSc2d3	nejčastější
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
pyrimidinové	pyrimidinový	k2eAgFnSc2d1	pyrimidinová
báze	báze	k1gFnSc2	báze
Vědcům	vědec	k1gMnPc3	vědec
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
již	již	k9	již
mnoho	mnoho	k4c1	mnoho
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
nepřirozené	přirozený	k2eNgFnPc4d1	nepřirozená
nukleové	nukleový	k2eAgFnPc4d1	nukleová
báze	báze	k1gFnPc4	báze
<g/>
,	,	kIx,	,
jen	jen	k9	jen
naprostá	naprostý	k2eAgFnSc1d1	naprostá
menšina	menšina	k1gFnSc1	menšina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
však	však	k9	však
skutečně	skutečně	k6eAd1	skutečně
replikovatelná	replikovatelný	k2eAgFnSc1d1	replikovatelná
DNA	dno	k1gNnSc2	dno
polymerázami	polymeráza	k1gFnPc7	polymeráza
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
transkripci	transkripce	k1gFnSc4	transkripce
do	do	k7c2	do
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
jediného	jediný	k2eAgInSc2d1	jediný
umělého	umělý	k2eAgInSc2d1	umělý
páru	pár	k1gInSc2	pár
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
byla	být	k5eAaImAgFnS	být
dosud	dosud	k6eAd1	dosud
prokázána	prokázat	k5eAaPmNgFnS	prokázat
in	in	k?	in
vivo	vivo	k6eAd1	vivo
funkční	funkční	k2eAgFnPc1d1	funkční
ekvivalence	ekvivalence	k1gFnPc1	ekvivalence
s	s	k7c7	s
přirozenými	přirozený	k2eAgInPc7d1	přirozený
páry	pár	k1gInPc7	pár
(	(	kIx(	(
<g/>
cytosin-guanin	cytosinuanina	k1gFnPc2	cytosin-guanina
<g/>
,	,	kIx,	,
adenin-thymin	adeninhymin	k1gInSc1	adenin-thymin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
báze	báze	k1gFnPc4	báze
"	"	kIx"	"
<g/>
5	[number]	k4	5
<g/>
SICS	SICS	kA	SICS
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
NaM	NaM	k1gFnSc1	NaM
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jako	jako	k9	jako
deoxynukleotidy	deoxynukleotid	k1gInPc4	deoxynukleotid
značeny	značen	k2eAgInPc4d1	značen
d	d	k?	d
<g/>
5	[number]	k4	5
<g/>
SICS	SICS	kA	SICS
resp.	resp.	kA	resp.
dNaM	dNaM	k?	dNaM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
z	z	k7c2	z
purinu	purin	k1gInSc2	purin
a	a	k8xC	a
pyrimidinu	pyrimidin	k1gInSc2	pyrimidin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obě	dva	k4xCgFnPc1	dva
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
dva	dva	k4xCgInPc1	dva
kondenzované	kondenzovaný	k2eAgInPc1d1	kondenzovaný
aromatické	aromatický	k2eAgInPc1d1	aromatický
cykly	cyklus	k1gInPc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Párování	párování	k1gNnSc2	párování
bází	báze	k1gFnPc2	báze
<g/>
#	#	kIx~	#
<g/>
Nepřirozené	přirozený	k2eNgInPc1d1	nepřirozený
páry	pár	k1gInPc1	pár
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
