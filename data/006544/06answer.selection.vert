<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
báze	báze	k1gFnPc4	báze
purinové	purinový	k2eAgFnPc4d1	purinová
(	(	kIx(	(
<g/>
adenin	adenin	k1gInSc4	adenin
<g/>
,	,	kIx,	,
guanin	guanin	k1gInSc4	guanin
<g/>
)	)	kIx)	)
a	a	k8xC	a
báze	báze	k1gFnPc1	báze
pyrimidinové	pyrimidinový	k2eAgFnPc1d1	pyrimidinová
(	(	kIx(	(
<g/>
cytosin	cytosin	k1gInSc1	cytosin
<g/>
,	,	kIx,	,
uracil	uracil	k1gInSc1	uracil
<g/>
,	,	kIx,	,
thymin	thymin	k1gInSc1	thymin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
