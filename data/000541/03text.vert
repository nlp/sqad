<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Svatý	svatý	k2eAgMnSc1d1	svatý
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Svatý	svatý	k2eAgMnSc1d1	svatý
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Svatí	svatý	k1gMnPc1	svatý
Konstantin	Konstantin	k1gMnSc1	Konstantin
(	(	kIx(	(
<g/>
Cyril	Cyril	k1gMnSc1	Cyril
<g/>
)	)	kIx)	)
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
označováni	označován	k2eAgMnPc1d1	označován
někdy	někdy	k6eAd1	někdy
jednoduše	jednoduše	k6eAd1	jednoduše
jako	jako	k8xS	jako
Soluňští	soluňský	k2eAgMnPc1d1	soluňský
bratři	bratr	k1gMnPc1	bratr
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
známí	známý	k1gMnPc1	známý
jako	jako	k8xS	jako
Apoštolové	apoštol	k1gMnPc1	apoštol
Slovanů	Slovan	k1gInPc2	Slovan
nebo	nebo	k8xC	nebo
také	také	k9	také
Slovanští	slovanský	k2eAgMnPc1d1	slovanský
věrozvěstové	věrozvěst	k1gMnPc1	věrozvěst
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Konstantin	Konstantin	k1gMnSc1	Konstantin
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
,	,	kIx,	,
Konstantinos	Konstantinos	k1gMnSc1	Konstantinos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Filosof	filosof	k1gMnSc1	filosof
827	[number]	k4	827
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
869	[number]	k4	869
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
z	z	k7c2	z
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
filosofie	filosofie	k1gFnSc2	filosofie
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
855	[number]	k4	855
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
misie	misie	k1gFnSc2	misie
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
prosadili	prosadit	k5eAaPmAgMnP	prosadit
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
jako	jako	k8xS	jako
bohoslužebný	bohoslužebný	k2eAgInSc4d1	bohoslužebný
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
který	který	k3yIgInSc4	který
Konstantin	Konstantin	k1gMnSc1	Konstantin
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
písmo	písmo	k1gNnSc4	písmo
hlaholici	hlaholice	k1gFnSc4	hlaholice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Lev	Lev	k1gMnSc1	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
svatořečil	svatořečit	k5eAaBmAgInS	svatořečit
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
spolupatrony	spolupatron	k1gInPc4	spolupatron
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
hlavními	hlavní	k2eAgMnPc7d1	hlavní
patrony	patron	k1gMnPc7	patron
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc1	svátek
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
Svatý	svatý	k2eAgMnSc1d1	svatý
Konstantin	Konstantin	k1gMnSc1	Konstantin
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
,	,	kIx,	,
Konstantinos	Konstantinos	k1gMnSc1	Konstantinos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Filosof	filosof	k1gMnSc1	filosof
(	(	kIx(	(
<g/>
827	[number]	k4	827
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
869	[number]	k4	869
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
z	z	k7c2	z
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
učenců	učenec	k1gMnPc2	učenec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
teologem	teolog	k1gMnSc7	teolog
<g/>
,	,	kIx,	,
řečníkem	řečník	k1gMnSc7	řečník
a	a	k8xC	a
polyglotem	polyglot	k1gMnSc7	polyglot
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
filosofie	filosofie	k1gFnSc2	filosofie
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
vedl	vést	k5eAaImAgMnS	vést
několik	několik	k4yIc4	několik
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
náboženskopolitických	náboženskopolitický	k2eAgFnPc2d1	náboženskopolitický
misí	mise	k1gFnPc2	mise
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
teologických	teologický	k2eAgFnPc2d1	teologická
disputací	disputace	k1gFnPc2	disputace
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začíná	začínat	k5eAaImIp3nS	začínat
také	také	k9	také
jejich	jejich	k3xOp3gFnSc1	jejich
společná	společný	k2eAgFnSc1d1	společná
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
mnich	mnich	k1gMnSc1	mnich
však	však	k9	však
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
až	až	k9	až
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijal	přijmout	k5eAaPmAgMnS	přijmout
řeholní	řeholní	k2eAgNnSc4d1	řeholní
jméno	jméno	k1gNnSc4	jméno
Cyril	Cyril	k1gMnSc1	Cyril
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
,	,	kIx,	,
Kyrillos	Kyrillos	k1gInSc1	Kyrillos
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
Cyrillus	Cyrillus	k1gInSc1	Cyrillus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
známější	známý	k2eAgFnSc1d2	známější
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Metoděj	Metoděj	k1gMnSc1	Metoděj
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Μ	Μ	k?	Μ
<g/>
,	,	kIx,	,
Methodios	Methodios	k1gInSc1	Methodios
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
Methodius	Methodius	k1gInSc1	Methodius
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
813	[number]	k4	813
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
885	[number]	k4	885
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
křestním	křestní	k2eAgNnSc7d1	křestní
jménem	jméno	k1gNnSc7	jméno
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Konstantina	Konstantin	k1gMnSc2	Konstantin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
byzantským	byzantský	k2eAgMnSc7d1	byzantský
státním	státní	k2eAgMnSc7d1	státní
úředníkem	úředník	k1gMnSc7	úředník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
mnichem	mnich	k1gInSc7	mnich
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
společného	společný	k2eAgInSc2d1	společný
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
patrně	patrně	k6eAd1	patrně
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
svého	svůj	k1gMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
účastní	účastnit	k5eAaImIp3nS	účastnit
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
cest	cesta	k1gFnPc2	cesta
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Byzance	Byzanc	k1gFnSc2	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Konstantinově	Konstantinův	k2eAgFnSc6d1	Konstantinova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
vedení	vedení	k1gNnSc4	vedení
velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
misie	misie	k1gFnSc2	misie
a	a	k8xC	a
završil	završit	k5eAaPmAgInS	završit
jejich	jejich	k3xOp3gNnSc4	jejich
společné	společný	k2eAgNnSc4d1	společné
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
hájil	hájit	k5eAaImAgMnS	hájit
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
bylo	být	k5eAaImAgNnS	být
zřízení	zřízení	k1gNnSc1	zřízení
moravsko-panonské	moravskoanonský	k2eAgFnSc2d1	moravsko-panonský
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
prvním	první	k4xOgMnSc7	první
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
biskupy	biskup	k1gInPc4	biskup
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
nad	nad	k7c7	nad
jejich	jejich	k3xOp3gFnSc7	jejich
biskupskou	biskupský	k2eAgFnSc7d1	biskupská
hodností	hodnost	k1gFnSc7	hodnost
nepanuje	panovat	k5eNaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
svatí	svatý	k1gMnPc1	svatý
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
běžně	běžně	k6eAd1	běžně
označováni	označovat	k5eAaImNgMnP	označovat
staročeskými	staročeský	k2eAgInPc7d1	staročeský
jmény	jméno	k1gNnPc7	jméno
Crha	Crha	k1gMnSc1	Crha
a	a	k8xC	a
Strachota	strachota	k1gFnSc1	strachota
<g/>
.	.	kIx.	.
</s>
<s>
Crha	Crha	k1gMnSc1	Crha
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
Cyril	Cyril	k1gMnSc1	Cyril
<g/>
;	;	kIx,	;
Strachota	strachota	k1gFnSc1	strachota
špatným	špatný	k2eAgInSc7d1	špatný
překladem	překlad	k1gInSc7	překlad
<g/>
:	:	kIx,	:
řecké	řecký	k2eAgNnSc1d1	řecké
Methodios	Methodios	k1gInSc1	Methodios
bylo	být	k5eAaImAgNnS	být
mylně	mylně	k6eAd1	mylně
přikloněno	přikloněn	k2eAgNnSc1d1	přikloněno
k	k	k7c3	k
latinskému	latinský	k2eAgInSc3d1	latinský
metus	metus	k1gInSc4	metus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
strach	strach	k1gInSc1	strach
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Soluně	Soluň	k1gFnSc2	Soluň
<g/>
,	,	kIx,	,
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
hodnostáře	hodnostář	k1gMnPc4	hodnostář
Byzantské	byzantský	k2eAgFnSc2d1	byzantská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gFnPc2	jejich
znalostí	znalost	k1gFnPc2	znalost
slovanského	slovanský	k2eAgInSc2d1	slovanský
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Řek	Řek	k1gMnSc1	Řek
Konstantin	Konstantin	k1gMnSc1	Konstantin
G.	G.	kA	G.
Bonis	Bonis	k1gFnSc1	Bonis
na	na	k7c6	na
základě	základ	k1gInSc6	základ
genealogie	genealogie	k1gFnSc2	genealogie
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
manželka	manželka	k1gFnSc1	manželka
Lva	Lev	k1gMnSc2	Lev
(	(	kIx(	(
<g/>
Leona	Leo	k1gMnSc2	Leo
<g/>
)	)	kIx)	)
Soluňského	soluňský	k2eAgMnSc2d1	soluňský
Marie	Marie	k1gFnSc1	Marie
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
císaře	císař	k1gMnSc2	císař
Konstantina	Konstantin	k1gMnSc2	Konstantin
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Metoděj	Metoděj	k1gMnSc1	Metoděj
ještě	ještě	k6eAd1	ještě
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Michal	Michala	k1gFnPc2	Michala
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
věnoval	věnovat	k5eAaImAgMnS	věnovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
činnosti	činnost	k1gFnPc1	činnost
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Byzanci	Byzanc	k1gFnSc6	Byzanc
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
r.	r.	kA	r.
835	[number]	k4	835
jmenován	jmenovat	k5eAaImNgInS	jmenovat
archontem	archón	k1gMnSc7	archón
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vojenským	vojenský	k2eAgMnSc7d1	vojenský
úředníkem	úředník	k1gMnSc7	úředník
a	a	k8xC	a
správcem	správce	k1gMnSc7	správce
(	(	kIx(	(
<g/>
vládcem	vládce	k1gMnSc7	vládce
<g/>
,	,	kIx,	,
místodržitelem	místodržitel	k1gMnSc7	místodržitel
<g/>
)	)	kIx)	)
v	v	k7c6	v
byzantské	byzantský	k2eAgFnSc6d1	byzantská
provincii	provincie	k1gFnSc6	provincie
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Strymon	Strymona	k1gFnPc2	Strymona
(	(	kIx(	(
<g/>
severně	severně	k6eAd1	severně
od	od	k7c2	od
Soluně	Soluň	k1gFnSc2	Soluň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývané	obývaný	k2eAgInPc1d1	obývaný
Slovany	Slovan	k1gInPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgNnSc2	svůj
povolání	povolání	k1gNnSc2	povolání
se	se	k3xPyFc4	se
však	však	k9	však
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
845	[number]	k4	845
zřekl	zřeknout	k5eAaPmAgInS	zřeknout
a	a	k8xC	a
úřad	úřad	k1gInSc1	úřad
archonta	archón	k1gMnSc4	archón
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
skoncovat	skoncovat	k5eAaPmF	skoncovat
se	s	k7c7	s
světským	světský	k2eAgInSc7d1	světský
životem	život	k1gInSc7	život
a	a	k8xC	a
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
maloasijského	maloasijský	k2eAgInSc2d1	maloasijský
Olympu	Olymp	k1gInSc2	Olymp
v	v	k7c6	v
bithýnském	bithýnský	k2eAgInSc6d1	bithýnský
kraji	kraj	k1gInSc6	kraj
do	do	k7c2	do
střediska	středisko	k1gNnSc2	středisko
řeckého	řecký	k2eAgNnSc2d1	řecké
mnišstva	mnišstvo	k1gNnSc2	mnišstvo
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
klášterů	klášter	k1gInPc2	klášter
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
(	(	kIx(	(
<g/>
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
klášterů	klášter	k1gInPc2	klášter
na	na	k7c4	na
úbočí	úbočí	k1gNnSc4	úbočí
tohoto	tento	k3xDgInSc2	tento
asijského	asijský	k2eAgInSc2d1	asijský
Olympu	Olymp	k1gInSc2	Olymp
(	(	kIx(	(
<g/>
turecky	turecky	k6eAd1	turecky
Kešiš-Dag	Kešiš-Dag	k1gInSc1	Kešiš-Dag
<g/>
,	,	kIx,	,
mnišská	mnišský	k2eAgFnSc1d1	mnišská
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
2500	[number]	k4	2500
m	m	kA	m
vysokého	vysoký	k2eAgInSc2d1	vysoký
masívu	masív	k1gInSc2	masív
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
Bursa	bursa	k1gFnSc1	bursa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mnichem	mnich	k1gInSc7	mnich
a	a	k8xC	a
diakonem	diakon	k1gMnSc7	diakon
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
mnišské	mnišský	k2eAgNnSc4d1	mnišské
jméno	jméno	k1gNnSc4	jméno
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
filozofii	filozofie	k1gFnSc4	filozofie
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
na	na	k7c6	na
císařské	císařský	k2eAgFnSc6d1	císařská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
dokončil	dokončit	k5eAaPmAgMnS	dokončit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
847	[number]	k4	847
<g/>
.	.	kIx.	.
</s>
<s>
Kancléř	kancléř	k1gMnSc1	kancléř
císařovny	císařovna	k1gFnSc2	císařovna
Theodory	Theodora	k1gFnSc2	Theodora
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Theoktistos	Theoktistos	k1gMnSc1	Theoktistos
dal	dát	k5eAaPmAgMnS	dát
Konstantina	Konstantin	k1gMnSc4	Konstantin
vysvětit	vysvětit	k5eAaPmF	vysvětit
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
jej	on	k3xPp3gMnSc4	on
chartofylaxem	chartofylax	k1gInSc7	chartofylax
(	(	kIx(	(
<g/>
knihovníkem	knihovník	k1gMnSc7	knihovník
a	a	k8xC	a
archivářem	archivář	k1gMnSc7	archivář
<g/>
)	)	kIx)	)
nového	nový	k2eAgMnSc2d1	nový
patriarchy	patriarcha	k1gMnSc2	patriarcha
Ignatia	Ignatius	k1gMnSc2	Ignatius
<g/>
.	.	kIx.	.
</s>
<s>
Konstantina	Konstantin	k1gMnSc4	Konstantin
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
ubíjela	ubíjet	k5eAaImAgFnS	ubíjet
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
patriarchou	patriarcha	k1gMnSc7	patriarcha
byl	být	k5eAaImAgInS	být
vypjatý	vypjatý	k2eAgInSc1d1	vypjatý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
tajně	tajně	k6eAd1	tajně
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
samoty	samota	k1gFnSc2	samota
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
klášterů	klášter	k1gInPc2	klášter
na	na	k7c6	na
zalesněném	zalesněný	k2eAgNnSc6d1	zalesněné
mořském	mořský	k2eAgNnSc6d1	mořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
asi	asi	k9	asi
po	po	k7c4	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
vypátrán	vypátrat	k5eAaPmNgInS	vypátrat
a	a	k8xC	a
přemluven	přemluvit	k5eAaPmNgInS	přemluvit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
<g/>
,	,	kIx,	,
Konstantin	Konstantin	k1gMnSc1	Konstantin
zdvořile	zdvořile	k6eAd1	zdvořile
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
bývalou	bývalý	k2eAgFnSc4d1	bývalá
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
raději	rád	k6eAd2	rád
nabízené	nabízený	k2eAgNnSc4d1	nabízené
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
filozofie	filozofie	k1gFnSc2	filozofie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
předtím	předtím	k6eAd1	předtím
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
prokázal	prokázat	k5eAaPmAgInS	prokázat
svoji	svůj	k3xOyFgFnSc4	svůj
způsobilost	způsobilost	k1gFnSc4	způsobilost
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc2	schopnost
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
zkouškou	zkouška	k1gFnSc7	zkouška
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
náboženské	náboženský	k2eAgFnSc2d1	náboženská
disputace	disputace	k1gFnSc2	disputace
s	s	k7c7	s
dříve	dříve	k6eAd2	dříve
sesazeným	sesazený	k2eAgMnSc7d1	sesazený
patriarchou	patriarcha	k1gMnSc7	patriarcha
Ioannem	Ioann	k1gMnSc7	Ioann
Grammatikem	Grammatik	k1gMnSc7	Grammatik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
851	[number]	k4	851
byl	být	k5eAaImAgInS	být
24	[number]	k4	24
<g/>
letý	letý	k2eAgMnSc1d1	letý
Konstantin	Konstantin	k1gMnSc1	Konstantin
císařovnou	císařovna	k1gFnSc7	císařovna
Theodorou	Theodora	k1gFnSc7	Theodora
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
učení	učení	k1gNnSc4	učení
u	u	k7c2	u
Saracénů	Saracén	k1gMnPc2	Saracén
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
se	se	k3xPyFc4	se
s	s	k7c7	s
tajemníkem	tajemník	k1gMnSc7	tajemník
Georgiem	Georgius	k1gMnSc7	Georgius
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
Samarry	Samarra	k1gFnSc2	Samarra
severně	severně	k6eAd1	severně
od	od	k7c2	od
Bagdádu	Bagdád	k1gInSc2	Bagdád
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
jejich	jejich	k3xOp3gFnSc2	jejich
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
si	se	k3xPyFc3	se
počínal	počínat	k5eAaImAgMnS	počínat
natolik	natolik	k6eAd1	natolik
moudře	moudřit	k5eAaImSgInS	moudřit
a	a	k8xC	a
vtipně	vtipně	k6eAd1	vtipně
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokonce	dokonce	k9	dokonce
vyprovokoval	vyprovokovat	k5eAaPmAgMnS	vyprovokovat
Araby	Arab	k1gMnPc4	Arab
k	k	k7c3	k
neúspěšnému	úspěšný	k2eNgInSc3d1	neúspěšný
pokusu	pokus	k1gInSc3	pokus
jej	on	k3xPp3gMnSc4	on
otrávit	otrávit	k5eAaPmF	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
Samarry	Samarra	k1gFnSc2	Samarra
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
Theoktistovu	Theoktistův	k2eAgFnSc4d1	Theoktistův
snahu	snaha	k1gFnSc4	snaha
udržet	udržet	k5eAaPmF	udržet
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
Cařihradu	Cařihrad	k1gInSc3	Cařihrad
<g/>
,	,	kIx,	,
toužil	toužit	k5eAaImAgMnS	toužit
Konstantin	Konstantin	k1gMnSc1	Konstantin
opět	opět	k6eAd1	opět
po	po	k7c6	po
samotě	samota	k1gFnSc6	samota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
855	[number]	k4	855
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
politickými	politický	k2eAgInPc7d1	politický
zvraty	zvrat	k1gInPc7	zvrat
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
odchází	odcházet	k5eAaImIp3nS	odcházet
a	a	k8xC	a
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
se	se	k3xPyFc4	se
do	do	k7c2	do
stejného	stejný	k2eAgInSc2d1	stejný
kláštera	klášter	k1gInSc2	klášter
jako	jako	k8xS	jako
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Řeholní	řeholní	k2eAgInPc1d1	řeholní
sliby	slib	k1gInPc1	slib
však	však	k9	však
nesložil	složit	k5eNaPmAgMnS	složit
<g/>
.	.	kIx.	.
</s>
<s>
Soluňští	soluňský	k2eAgMnPc1d1	soluňský
bratři	bratr	k1gMnPc1	bratr
se	se	k3xPyFc4	se
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
kláštera	klášter	k1gInSc2	klášter
oddávali	oddávat	k5eAaImAgMnP	oddávat
studiu	studio	k1gNnSc3	studio
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
tady	tady	k6eAd1	tady
začal	začít	k5eAaPmAgInS	začít
vznikat	vznikat	k5eAaImF	vznikat
teoretický	teoretický	k2eAgInSc1d1	teoretický
základ	základ	k1gInSc1	základ
slovanského	slovanský	k2eAgNnSc2d1	slovanské
písemnictví	písemnictví	k1gNnSc2	písemnictví
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
zde	zde	k6eAd1	zde
získali	získat	k5eAaPmAgMnP	získat
první	první	k4xOgMnPc4	první
žáky	žák	k1gMnPc4	žák
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
Kliment	Kliment	k1gMnSc1	Kliment
Ochridský	Ochridský	k2eAgMnSc1d1	Ochridský
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nanejvýš	nanejvýš	k6eAd1	nanejvýš
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
odklonu	odklon	k1gInSc6	odklon
bratrské	bratrský	k2eAgFnSc2d1	bratrská
dvojice	dvojice	k1gFnSc2	dvojice
od	od	k7c2	od
státní	státní	k2eAgFnSc2d1	státní
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
veškerého	veškerý	k3xTgInSc2	veškerý
světského	světský	k2eAgInSc2d1	světský
života	život	k1gInSc2	život
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
následném	následný	k2eAgInSc6d1	následný
útěku	útěk	k1gInSc6	útěk
do	do	k7c2	do
klidného	klidný	k2eAgNnSc2d1	klidné
klášterního	klášterní	k2eAgNnSc2d1	klášterní
prostředí	prostředí	k1gNnSc2	prostředí
měly	mít	k5eAaImAgFnP	mít
drsné	drsný	k2eAgFnPc1d1	drsná
politické	politický	k2eAgFnPc1d1	politická
praktiky	praktika	k1gFnPc1	praktika
a	a	k8xC	a
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
hektickou	hektický	k2eAgFnSc4d1	hektická
dobu	doba	k1gFnSc4	doba
silné	silný	k2eAgInPc4d1	silný
politicko-náboženské	politickoáboženský	k2eAgInPc4d1	politicko-náboženský
střety	střet	k1gInPc4	střet
<g/>
.	.	kIx.	.
</s>
<s>
Theodora	Theodora	k1gFnSc1	Theodora
II	II	kA	II
<g/>
.	.	kIx.	.
vládla	vládnout	k5eAaImAgFnS	vládnout
jako	jako	k9	jako
císařovna-zástupkyně	císařovnaástupkyně	k1gFnSc1	císařovna-zástupkyně
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
kancléře	kancléř	k1gMnSc2	kancléř
Theoktista	Theoktista	k1gMnSc1	Theoktista
místo	místo	k7c2	místo
nezletilého	nezletilý	k1gMnSc2	nezletilý
syna	syn	k1gMnSc2	syn
Michaela	Michael	k1gMnSc2	Michael
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Císařovnin	císařovnin	k2eAgMnSc1d1	císařovnin
bratr	bratr	k1gMnSc1	bratr
Bardas	Bardas	k1gMnSc1	Bardas
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
učenec	učenec	k1gMnSc1	učenec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
povahou	povaha	k1gFnSc7	povaha
bezcharakterní	bezcharakterní	k2eAgFnSc7d1	bezcharakterní
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Theoktistův	Theoktistův	k2eAgMnSc1d1	Theoktistův
nepřítel	nepřítel	k1gMnSc1	nepřítel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nelibostí	nelibost	k1gFnSc7	nelibost
nesl	nést	k5eAaImAgInS	nést
spolupráci	spolupráce	k1gFnSc3	spolupráce
císařovny	císařovna	k1gFnSc2	císařovna
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
kancléře	kancléř	k1gMnSc2	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Theoktistovi	Theoktista	k1gMnSc3	Theoktista
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhnat	vyhnat	k5eAaPmF	vyhnat
Bardu	bard	k1gMnSc3	bard
z	z	k7c2	z
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
však	však	k9	však
měl	mít	k5eAaImAgInS	mít
značný	značný	k2eAgInSc1d1	značný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
mladého	mladý	k2eAgMnSc4d1	mladý
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
rozmařilého	rozmařilý	k2eAgMnSc2d1	rozmařilý
Michaela	Michael	k1gMnSc2	Michael
povolán	povolat	k5eAaPmNgMnS	povolat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
<g/>
.	.	kIx.	.
</s>
<s>
Nastal	nastat	k5eAaPmAgInS	nastat
otevřený	otevřený	k2eAgInSc1d1	otevřený
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
spiknutí	spiknutí	k1gNnSc1	spiknutí
Michaela	Michael	k1gMnSc2	Michael
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
strýce	strýc	k1gMnSc2	strýc
Bardy	bard	k1gMnPc4	bard
proti	proti	k7c3	proti
Theoktistovi	Theoktista	k1gMnSc3	Theoktista
<g/>
.	.	kIx.	.
</s>
<s>
Theoktistos	Theoktistos	k1gMnSc1	Theoktistos
<g/>
,	,	kIx,	,
ochránce	ochránce	k1gMnSc2	ochránce
Konstantina	Konstantin	k1gMnSc4	Konstantin
Filozofa	filozof	k1gMnSc4	filozof
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
uvržen	uvrhnout	k5eAaPmNgInS	uvrhnout
do	do	k7c2	do
žaláře	žalář	k1gInSc2	žalář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
okolo	okolo	k7c2	okolo
r.	r.	kA	r.
855	[number]	k4	855
zemřel	zemřít	k5eAaPmAgInS	zemřít
násilnou	násilný	k2eAgFnSc7d1	násilná
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Spiknutí	spiknutí	k1gNnSc1	spiknutí
Bardy	bard	k1gMnPc4	bard
a	a	k8xC	a
Michaela	Michael	k1gMnSc4	Michael
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ovlivněného	ovlivněný	k2eAgInSc2d1	ovlivněný
nestřídmým	střídmý	k2eNgNnSc7d1	nestřídmé
pitím	pití	k1gNnSc7	pití
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
proti	proti	k7c3	proti
Theodoře	Theodora	k1gFnSc3	Theodora
<g/>
.	.	kIx.	.
</s>
<s>
Nestačilo	stačit	k5eNaBmAgNnS	stačit
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
856	[number]	k4	856
vzdala	vzdát	k5eAaPmAgFnS	vzdát
vlády	vláda	k1gFnPc4	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přinutili	přinutit	k5eAaPmAgMnP	přinutit
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
i	i	k9	i
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
třemi	tři	k4xCgInPc7	tři
dcerami	dcera	k1gFnPc7	dcera
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
roku	rok	k1gInSc2	rok
857	[number]	k4	857
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
přenesl	přenést	k5eAaPmAgInS	přenést
na	na	k7c4	na
církevní	církevní	k2eAgFnSc4d1	církevní
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Nevázaný	vázaný	k2eNgMnSc1d1	nevázaný
mladý	mladý	k2eAgMnSc1d1	mladý
císař	císař	k1gMnSc1	císař
všemožně	všemožně	k6eAd1	všemožně
ztrpčoval	ztrpčovat	k5eAaImAgMnS	ztrpčovat
život	život	k1gInSc4	život
patriarchovi	patriarcha	k1gMnSc3	patriarcha
Ignatiovi	Ignatius	k1gMnSc3	Ignatius
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
komplic	komplic	k1gMnSc1	komplic
Bardas	Bardas	k1gMnSc1	Bardas
<g/>
.	.	kIx.	.
</s>
<s>
Ignatios	Ignatios	k1gInSc1	Ignatios
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
kritice	kritika	k1gFnSc3	kritika
Bardova	bardův	k2eAgInSc2d1	bardův
pohoršlivého	pohoršlivý	k2eAgInSc2d1	pohoršlivý
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
vyhnán	vyhnat	k5eAaPmNgInS	vyhnat
z	z	k7c2	z
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přes	přes	k7c4	přes
hrozby	hrozba	k1gFnPc4	hrozba
<g/>
,	,	kIx,	,
lsti	lest	k1gFnPc4	lest
i	i	k8xC	i
útoky	útok	k1gInPc4	útok
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
nátlak	nátlak	k1gInSc1	nátlak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zřekl	zřeknout	k5eAaPmAgMnS	zřeknout
stolce	stolec	k1gInSc2	stolec
patriarchy	patriarcha	k1gMnSc2	patriarcha
<g/>
.	.	kIx.	.
</s>
<s>
Bardas	Bardas	k1gMnSc1	Bardas
nechtěje	chtít	k5eNaImSgInS	chtít
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
až	až	k8xS	až
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
Ignatius	Ignatius	k1gInSc1	Ignatius
souzen	soudit	k5eAaImNgInS	soudit
a	a	k8xC	a
sesazen	sesadit	k5eAaPmNgInS	sesadit
zákonným	zákonný	k2eAgInSc7d1	zákonný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
ustavil	ustavit	k5eAaPmAgMnS	ustavit
novým	nový	k2eAgMnSc7d1	nový
patriarchou	patriarcha	k1gMnSc7	patriarcha
učence	učenec	k1gMnSc2	učenec
Fotia	Fotius	k1gMnSc2	Fotius
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
světského	světský	k2eAgInSc2d1	světský
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pouhých	pouhý	k2eAgInPc2d1	pouhý
šesti	šest	k4xCc2	šest
dnů	den	k1gInPc2	den
přijal	přijmout	k5eAaPmAgMnS	přijmout
Fotios	Fotios	k1gMnSc1	Fotios
veškerá	veškerý	k3xTgNnPc4	veškerý
církevní	církevní	k2eAgNnPc4d1	církevní
svěcení	svěcení	k1gNnPc4	svěcení
a	a	k8xC	a
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
roku	rok	k1gInSc2	rok
858	[number]	k4	858
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
na	na	k7c6	na
biskupa	biskup	k1gInSc2	biskup
a	a	k8xC	a
ustaven	ustavit	k5eAaPmNgMnS	ustavit
patriarchou	patriarcha	k1gMnSc7	patriarcha
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgInSc1d1	církevní
sněm	sněm	k1gInSc1	sněm
biskupů	biskup	k1gMnPc2	biskup
však	však	k9	však
držel	držet	k5eAaImAgMnS	držet
s	s	k7c7	s
Ignatiem	Ignatius	k1gMnSc7	Ignatius
a	a	k8xC	a
Fotia	Fotius	k1gMnSc4	Fotius
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
Fotios	Fotios	k1gMnSc1	Fotios
tedy	tedy	k9	tedy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
859	[number]	k4	859
svolal	svolat	k5eAaPmAgMnS	svolat
svůj	svůj	k3xOyFgInSc4	svůj
církevní	církevní	k2eAgInSc4d1	církevní
sněm	sněm	k1gInSc4	sněm
a	a	k8xC	a
vládním	vládní	k2eAgInSc7d1	vládní
nátlakem	nátlak	k1gInSc7	nátlak
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
sesazení	sesazení	k1gNnSc4	sesazení
Ignatia	Ignatius	k1gMnSc2	Ignatius
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
pronásledování	pronásledování	k1gNnSc4	pronásledování
jeho	jeho	k3xOp3gMnPc2	jeho
přívrženců	přívrženec	k1gMnPc2	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Konstantin	Konstantin	k1gMnSc1	Konstantin
zastával	zastávat	k5eAaImAgMnS	zastávat
Ignatia	Ignatius	k1gMnSc4	Ignatius
<g/>
,	,	kIx,	,
někdejší	někdejší	k2eAgMnSc1d1	někdejší
Konstantinův	Konstantinův	k2eAgMnSc1d1	Konstantinův
učitel	učitel	k1gMnSc1	učitel
Fotios	Fotios	k1gMnSc1	Fotios
si	se	k3xPyFc3	se
dobře	dobře	k6eAd1	dobře
pamatoval	pamatovat	k5eAaImAgInS	pamatovat
moudrost	moudrost	k1gFnSc4	moudrost
mladého	mladý	k2eAgMnSc2d1	mladý
filosofa	filosof	k1gMnSc2	filosof
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
státním	státní	k2eAgInSc6d1	státní
a	a	k8xC	a
církevním	církevní	k2eAgInSc6d1	církevní
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
Byzanci	Byzanc	k1gFnSc6	Byzanc
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
860	[number]	k4	860
byli	být	k5eAaImAgMnP	být
proto	proto	k8xC	proto
Konstantin	Konstantin	k1gMnSc1	Konstantin
i	i	k9	i
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Metodějem	Metoděj	k1gMnSc7	Metoděj
povoláni	povolat	k5eAaPmNgMnP	povolat
z	z	k7c2	z
kláštera	klášter	k1gInSc2	klášter
do	do	k7c2	do
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
a	a	k8xC	a
vysláni	vyslat	k5eAaPmNgMnP	vyslat
byzantským	byzantský	k2eAgMnSc7d1	byzantský
císařem	císař	k1gMnSc7	císař
Michaelem	Michael	k1gMnSc7	Michael
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c4	na
misijní	misijní	k2eAgFnSc4d1	misijní
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Chazarům	Chazar	k1gMnPc3	Chazar
přes	přes	k7c4	přes
poloostrov	poloostrov	k1gInSc4	poloostrov
Krym	Krym	k1gInSc4	Krym
až	až	k9	až
ke	k	k7c3	k
Kaspickému	kaspický	k2eAgNnSc3d1	Kaspické
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
nájezdu	nájezd	k1gInSc6	nájezd
Rusů	Rus	k1gMnPc2	Rus
na	na	k7c4	na
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
obou	dva	k4xCgMnPc2	dva
bratří	bratr	k1gMnPc2	bratr
měla	mít	k5eAaImAgFnS	mít
proto	proto	k8xC	proto
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
v	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
řadě	řada	k1gFnSc6	řada
obnovit	obnovit	k5eAaPmF	obnovit
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Chazary	Chazar	k1gMnPc7	Chazar
<g/>
.	.	kIx.	.
</s>
<s>
Chazaři	Chazar	k1gMnPc1	Chazar
byli	být	k5eAaImAgMnP	být
jediným	jediný	k2eAgNnSc7d1	jediné
nehebrejským	hebrejský	k2eNgNnSc7d1	hebrejský
etnikem	etnikum	k1gNnSc7	etnikum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
hromadně	hromadně	k6eAd1	hromadně
přijalo	přijmout	k5eAaPmAgNnS	přijmout
židovské	židovský	k2eAgNnSc4d1	Židovské
vyznání	vyznání	k1gNnSc4	vyznání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
státním	státní	k2eAgNnSc7d1	státní
náboženstvím	náboženství	k1gNnSc7	náboženství
tohoto	tento	k3xDgInSc2	tento
útvaru	útvar	k1gInSc2	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
politického	politický	k2eAgInSc2d1	politický
úkolu	úkol	k1gInSc2	úkol
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
byla	být	k5eAaImAgFnS	být
společná	společný	k2eAgFnSc1d1	společná
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
nájezdům	nájezd	k1gInPc3	nájezd
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
i	i	k8xC	i
úkol	úkol	k1gInSc1	úkol
náboženský	náboženský	k2eAgInSc1d1	náboženský
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
chazarská	chazarský	k2eAgFnSc1d1	Chazarská
polemika	polemika	k1gFnSc1	polemika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
bratrům	bratr	k1gMnPc3	bratr
podařilo	podařit	k5eAaPmAgNnS	podařit
nalézt	nalézt	k5eAaBmF	nalézt
a	a	k8xC	a
vyzvednout	vyzvednout	k5eAaPmF	vyzvednout
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
ostatky	ostatek	k1gInPc7	ostatek
papeže	papež	k1gMnSc4	papež
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
I.	I.	kA	I.
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pak	pak	k6eAd1	pak
údajně	údajně	k6eAd1	údajně
přinesli	přinést	k5eAaPmAgMnP	přinést
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
snad	snad	k9	snad
na	na	k7c4	na
Hradisko	hradisko	k1gNnSc4	hradisko
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
vzali	vzít	k5eAaPmAgMnP	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
jako	jako	k8xS	jako
dar	dar	k1gInSc4	dar
papeži	papež	k1gMnSc3	papež
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
nedaleko	nedaleko	k7c2	nedaleko
Kolosea	Koloseum	k1gNnSc2	Koloseum
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
došlo	dojít	k5eAaPmAgNnS	dojít
opět	opět	k6eAd1	opět
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
novým	nový	k2eAgMnSc7d1	nový
patriarchou	patriarcha	k1gMnSc7	patriarcha
Fotiem	Fotius	k1gMnSc7	Fotius
a	a	k8xC	a
přívrženci	přívrženec	k1gMnPc1	přívrženec
dřívějšího	dřívější	k2eAgMnSc2d1	dřívější
patriarchy	patriarcha	k1gMnSc2	patriarcha
Ignatia	Ignatius	k1gMnSc2	Ignatius
<g/>
.	.	kIx.	.
</s>
<s>
Fotios	Fotios	k1gInSc1	Fotios
tedy	tedy	k9	tedy
znovu	znovu	k6eAd1	znovu
svolal	svolat	k5eAaPmAgInS	svolat
církevní	církevní	k2eAgInSc1d1	církevní
sněm	sněm	k1gInSc1	sněm
a	a	k8xC	a
na	na	k7c6	na
cařihradském	cařihradský	k2eAgInSc6d1	cařihradský
synodě	synoda	k1gFnSc6	synoda
roku	rok	k1gInSc2	rok
861	[number]	k4	861
byl	být	k5eAaImAgMnS	být
Ignatius	Ignatius	k1gMnSc1	Ignatius
opětovně	opětovně	k6eAd1	opětovně
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
a	a	k8xC	a
sesazen	sesazen	k2eAgMnSc1d1	sesazen
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
biskupů	biskup	k1gMnPc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Císařský	císařský	k2eAgInSc1d1	císařský
a	a	k8xC	a
patriarší	patriarší	k2eAgInSc1d1	patriarší
dvůr	dvůr	k1gInSc1	dvůr
proto	proto	k8xC	proto
hledal	hledat	k5eAaImAgInS	hledat
muže	muž	k1gMnPc4	muž
způsobilé	způsobilý	k2eAgMnPc4d1	způsobilý
pro	pro	k7c4	pro
uprázdněná	uprázdněný	k2eAgNnPc4d1	uprázdněné
biskupství	biskupství	k1gNnPc4	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
mu	on	k3xPp3gNnSc3	on
známy	znám	k2eAgFnPc1d1	známa
správní	správní	k2eAgFnPc1d1	správní
schopnosti	schopnost	k1gFnPc1	schopnost
Metodějovy	Metodějův	k2eAgFnPc1d1	Metodějova
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
dostal	dostat	k5eAaPmAgMnS	dostat
proto	proto	k8xC	proto
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
úřadu	úřad	k1gInSc3	úřad
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
v	v	k7c6	v
Byzanci	Byzanc	k1gFnSc6	Byzanc
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
ale	ale	k9	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
pouze	pouze	k6eAd1	pouze
hodnost	hodnost	k1gFnSc4	hodnost
igumena	igumen	k1gMnSc4	igumen
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
představeného	představený	k2eAgInSc2d1	představený
kláštera	klášter	k1gInSc2	klášter
Polychron	Polychron	k1gMnSc1	Polychron
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
maloasijské	maloasijský	k2eAgFnSc6d1	maloasijská
Bithýnii	Bithýnie	k1gFnSc6	Bithýnie
<g/>
,	,	kIx,	,
byzantské	byzantský	k2eAgFnSc6d1	byzantská
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
asijském	asijský	k2eAgInSc6d1	asijský
břehu	břeh	k1gInSc6	břeh
Marmarského	Marmarský	k2eAgNnSc2d1	Marmarské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
údajný	údajný	k2eAgInSc4d1	údajný
dopis	dopis	k1gInSc4	dopis
knížete	kníže	k1gMnSc2	kníže
Rostislava	Rostislav	k1gMnSc2	Rostislav
byzantskému	byzantský	k2eAgMnSc3d1	byzantský
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
Konstantinův	Konstantinův	k2eAgInSc1d1	Konstantinův
V	v	k7c6	v
roce	rok	k1gInSc6	rok
862	[number]	k4	862
vyslal	vyslat	k5eAaPmAgMnS	vyslat
kníže	kníže	k1gMnSc1	kníže
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
Rostislav	Rostislava	k1gFnPc2	Rostislava
poselstvo	poselstvo	k1gNnSc1	poselstvo
k	k	k7c3	k
byzantskému	byzantský	k2eAgMnSc3d1	byzantský
císaři	císař	k1gMnSc3	císař
Michaelu	Michael	k1gMnSc3	Michael
III	III	kA	III
<g/>
.	.	kIx.	.
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
vyslání	vyslání	k1gNnSc4	vyslání
biskupa	biskup	k1gMnSc2	biskup
a	a	k8xC	a
učitele	učitel	k1gMnSc2	učitel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
položil	položit	k5eAaPmAgMnS	položit
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
základy	základ	k1gInPc4	základ
(	(	kIx(	(
<g/>
na	na	k7c6	na
franských	franský	k2eAgInPc6d1	franský
biskupech	biskup	k1gInPc6	biskup
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
)	)	kIx)	)
církve	církev	k1gFnPc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sice	sice	k8xC	sice
seznámili	seznámit	k5eAaPmAgMnP	seznámit
s	s	k7c7	s
církevním	církevní	k2eAgNnSc7d1	církevní
učením	učení	k1gNnSc7	učení
misionářů	misionář	k1gMnPc2	misionář
z	z	k7c2	z
Východofranského	východofranský	k2eAgNnSc2d1	východofranský
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kníže	kníže	k1gMnSc1	kníže
Rostislav	Rostislav	k1gMnSc1	Rostislav
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
náboženského	náboženský	k2eAgInSc2d1	náboženský
vlivu	vliv	k1gInSc2	vliv
Východofranské	východofranský	k2eAgFnSc2d1	Východofranská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
nechal	nechat	k5eAaPmAgMnS	nechat
franské	franský	k2eAgMnPc4d1	franský
kněze	kněz	k1gMnPc4	kněz
vyhnat	vyhnat	k5eAaPmF	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
tak	tak	k6eAd1	tak
postrádala	postrádat	k5eAaImAgFnS	postrádat
dostatek	dostatek	k1gInSc4	dostatek
duchovních	duchovní	k1gMnPc2	duchovní
a	a	k8xC	a
i	i	k9	i
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
důvodů	důvod	k1gInPc2	důvod
Rostislavovy	Rostislavův	k2eAgFnSc2d1	Rostislavova
žádosti	žádost	k1gFnSc2	žádost
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
patriarcha	patriarcha	k1gMnSc1	patriarcha
Fotios	Fotios	k1gMnSc1	Fotios
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
nakonec	nakonec	k6eAd1	nakonec
Rostislavově	Rostislavův	k2eAgFnSc3d1	Rostislavova
žádosti	žádost	k1gFnSc3	žádost
částečně	částečně	k6eAd1	částečně
vyhovět	vyhovět	k5eAaPmF	vyhovět
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
znalosti	znalost	k1gFnSc3	znalost
jazyka	jazyk	k1gInSc2	jazyk
byli	být	k5eAaImAgMnP	být
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
misi	mise	k1gFnSc4	mise
vybráni	vybrat	k5eAaPmNgMnP	vybrat
právě	právě	k6eAd1	právě
Konstantin	Konstantin	k1gMnSc1	Konstantin
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přípravách	příprava	k1gFnPc6	příprava
Konstantin	Konstantin	k1gMnSc1	Konstantin
sestavil	sestavit	k5eAaPmAgMnS	sestavit
nové	nový	k2eAgNnSc4d1	nové
písmo	písmo	k1gNnSc4	písmo
(	(	kIx(	(
<g/>
hlaholici	hlaholice	k1gFnSc4	hlaholice
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
slovanský	slovanský	k2eAgInSc4d1	slovanský
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Metodějem	Metoděj	k1gMnSc7	Metoděj
přeložili	přeložit	k5eAaPmAgMnP	přeložit
do	do	k7c2	do
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
liturgické	liturgický	k2eAgFnSc2d1	liturgická
knihy	kniha	k1gFnSc2	kniha
potřebné	potřebný	k2eAgFnSc2d1	potřebná
pro	pro	k7c4	pro
konání	konání	k1gNnSc4	konání
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Konstantinovi	Konstantin	k1gMnSc3	Konstantin
je	být	k5eAaImIp3nS	být
připisováno	připisován	k2eAgNnSc4d1	připisováno
sestavení	sestavení	k1gNnSc4	sestavení
Proglasu	Proglas	k1gInSc2	Proglas
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
předmluvy	předmluva	k1gFnPc1	předmluva
ke	k	k7c3	k
staroslověnskému	staroslověnský	k2eAgInSc3d1	staroslověnský
překladu	překlad	k1gInSc2	překlad
evangelií	evangelium	k1gNnPc2	evangelium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
dorazili	dorazit	k5eAaPmAgMnP	dorazit
bratři	bratr	k1gMnPc1	bratr
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
863	[number]	k4	863
(	(	kIx(	(
<g/>
Konstantin	Konstantin	k1gMnSc1	Konstantin
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
presbytera	presbyter	k1gMnSc4	presbyter
<g/>
/	/	kIx~	/
<g/>
kněze	kněz	k1gMnSc4	kněz
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
patrně	patrně	k6eAd1	patrně
jako	jako	k9	jako
jáhen	jáhen	k1gMnSc1	jáhen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
misie	misie	k1gFnSc1	misie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
velkých	velký	k2eAgMnPc2d1	velký
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
zejm.	zejm.	k?	zejm.
díky	díky	k7c3	díky
plné	plný	k2eAgFnSc3d1	plná
podpoře	podpora	k1gFnSc3	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Rostislava	Rostislava	k1gFnSc1	Rostislava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
organizačním	organizační	k2eAgFnPc3d1	organizační
schopnostem	schopnost	k1gFnPc3	schopnost
obou	dva	k4xCgMnPc2	dva
bratří	bratr	k1gMnPc2	bratr
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc3	jejich
značné	značný	k2eAgFnSc3d1	značná
osobní	osobní	k2eAgFnSc3d1	osobní
autoritě	autorita	k1gFnSc3	autorita
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
Konstantinově	Konstantinův	k2eAgFnSc3d1	Konstantinova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
hrálo	hrát	k5eAaImAgNnS	hrát
také	také	k9	také
zavedení	zavedení	k1gNnSc4	zavedení
nové	nový	k2eAgFnSc2d1	nová
<g/>
,	,	kIx,	,
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
liturgie	liturgie	k1gFnSc2	liturgie
a	a	k8xC	a
užití	užití	k1gNnSc2	užití
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
při	při	k7c6	při
kázání	kázání	k1gNnSc6	kázání
a	a	k8xC	a
výuce	výuka	k1gFnSc6	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
s	s	k7c7	s
Metodějem	Metoděj	k1gMnSc7	Metoděj
sestavili	sestavit	k5eAaPmAgMnP	sestavit
také	také	k9	také
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
Zakon	Zakon	k1gInSc4	Zakon
sudnyj	sudnyj	k1gFnSc4	sudnyj
ljudem	ljud	k1gInSc7	ljud
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Soudní	soudní	k2eAgInSc1d1	soudní
zákon	zákon	k1gInSc1	zákon
pro	pro	k7c4	pro
laiky	laik	k1gMnPc4	laik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
bodě	bod	k1gInSc6	bod
vypořádává	vypořádávat	k5eAaImIp3nS	vypořádávat
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
co	co	k9	co
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
křesťanství	křesťanství	k1gNnSc4	křesťanství
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
přísné	přísný	k2eAgInPc4d1	přísný
tresty	trest	k1gInPc4	trest
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
konají	konat	k5eAaImIp3nP	konat
pohanské	pohanský	k2eAgFnPc4d1	pohanská
oběti	oběť	k1gFnPc4	oběť
nebo	nebo	k8xC	nebo
přísahy	přísaha	k1gFnPc4	přísaha
<g/>
.	.	kIx.	.
</s>
<s>
Trestem	trest	k1gInSc7	trest
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Každá	každý	k3xTgFnSc1	každý
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
oběti	oběť	k1gFnPc1	oběť
nebo	nebo	k8xC	nebo
přísahy	přísaha	k1gFnPc1	přísaha
pohanské	pohanský	k2eAgFnPc1d1	pohanská
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
je	být	k5eAaImIp3nS	být
předána	předat	k5eAaPmNgFnS	předat
Božímu	boží	k2eAgNnSc3d1	boží
chrámu	chrám	k1gInSc2	chrám
se	se	k3xPyFc4	se
vším	všecek	k3xTgInSc7	všecek
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
pánům	pan	k1gMnPc3	pan
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
konají	konat	k5eAaImIp3nP	konat
oběti	oběť	k1gFnPc4	oběť
a	a	k8xC	a
přísahy	přísaha	k1gFnPc4	přísaha
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
jsou	být	k5eAaImIp3nP	být
prodáni	prodán	k2eAgMnPc1d1	prodán
s	s	k7c7	s
veškerým	veškerý	k3xTgNnSc7	veškerý
svým	svůj	k3xOyFgInSc7	svůj
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
získaný	získaný	k2eAgInSc1d1	získaný
výnos	výnos	k1gInSc1	výnos
ať	ať	k9	ať
se	se	k3xPyFc4	se
rozdá	rozdat	k5eAaPmIp3nS	rozdat
chudým	chudý	k2eAgMnSc7d1	chudý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
864	[number]	k4	864
napadl	napadnout	k5eAaPmAgMnS	napadnout
Ludvík	Ludvík	k1gMnSc1	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
Velkomoravskou	velkomoravský	k2eAgFnSc4d1	Velkomoravská
říši	říše	k1gFnSc4	říše
a	a	k8xC	a
kníže	kníže	k1gMnSc1	kníže
Rostislav	Rostislav	k1gMnSc1	Rostislav
<g/>
,	,	kIx,	,
obležen	obležen	k2eAgMnSc1d1	obležen
na	na	k7c6	na
Děvíně	Děvín	k1gInSc6	Děvín
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
přiznat	přiznat	k5eAaPmF	přiznat
vazalství	vazalství	k1gNnSc1	vazalství
vůči	vůči	k7c3	vůči
Východofranské	východofranský	k2eAgFnSc3d1	Východofranská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
návrat	návrat	k1gInSc1	návrat
latinským	latinský	k2eAgMnPc3d1	latinský
kněžím	kněz	k1gMnPc3	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
příchodu	příchod	k1gInSc6	příchod
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
země	zem	k1gFnSc2	zem
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
svárům	svár	k1gInPc3	svár
dvou	dva	k4xCgFnPc2	dva
koncepcí	koncepce	k1gFnPc2	koncepce
-	-	kIx~	-
latinské	latinský	k2eAgFnPc1d1	Latinská
a	a	k8xC	a
staroslověnské	staroslověnský	k2eAgFnPc1d1	staroslověnská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
866	[number]	k4	866
se	se	k3xPyFc4	se
věrozvěstové	věrozvěst	k1gMnPc1	věrozvěst
odebrali	odebrat	k5eAaPmAgMnP	odebrat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Akvileje	Akvileje	k1gFnSc2	Akvileje
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tam	tam	k6eAd1	tam
zařídili	zařídit	k5eAaPmAgMnP	zařídit
potřebné	potřebný	k2eAgFnPc4d1	potřebná
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
867	[number]	k4	867
se	se	k3xPyFc4	se
cestou	cesta	k1gFnSc7	cesta
asi	asi	k9	asi
na	na	k7c4	na
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
zastavili	zastavit	k5eAaPmAgMnP	zastavit
u	u	k7c2	u
knížete	kníže	k1gMnSc2	kníže
Kocela	Kocela	k1gFnSc1	Kocela
v	v	k7c6	v
politicky	politicky	k6eAd1	politicky
klidnější	klidný	k2eAgFnSc6d2	klidnější
Panonii	Panonie	k1gFnSc6	Panonie
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
jim	on	k3xPp3gMnPc3	on
přivedl	přivést	k5eAaPmAgMnS	přivést
50	[number]	k4	50
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
bratři	bratr	k1gMnPc1	bratr
připravili	připravit	k5eAaPmAgMnP	připravit
pro	pro	k7c4	pro
stav	stav	k1gInSc4	stav
duchovní	duchovní	k2eAgFnSc1d1	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Slovanští	slovanský	k2eAgMnPc1d1	slovanský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Panonie	Panonie	k1gFnSc2	Panonie
přijímali	přijímat	k5eAaImAgMnP	přijímat
s	s	k7c7	s
vděčností	vděčnost	k1gFnSc7	vděčnost
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
misii	misie	k1gFnSc4	misie
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
<g/>
,	,	kIx,	,
slovanském	slovanský	k2eAgInSc6d1	slovanský
jazyku	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Kocel	Kocel	k1gMnSc1	Kocel
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nejednou	jednou	k6eNd1	jednou
písemně	písemně	k6eAd1	písemně
informoval	informovat	k5eAaBmAgMnS	informovat
papeže	papež	k1gMnSc4	papež
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
I.	I.	kA	I.
a	a	k8xC	a
vychvaloval	vychvalovat	k5eAaImAgInS	vychvalovat
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
christianizaci	christianizace	k1gFnSc4	christianizace
jeho	on	k3xPp3gNnSc2	on
knížectví	knížectví	k1gNnSc2	knížectví
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
obětavých	obětavý	k2eAgMnPc2d1	obětavý
soluňských	soluňský	k2eAgMnPc2d1	soluňský
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
povaze	povaha	k1gFnSc6	povaha
bratrské	bratrský	k2eAgFnSc2d1	bratrská
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
,	,	kIx,	,
odmítali	odmítat	k5eAaImAgMnP	odmítat
bohaté	bohatý	k2eAgInPc4d1	bohatý
dary	dar	k1gInPc4	dar
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
vyjednali	vyjednat	k5eAaPmAgMnP	vyjednat
propuštění	propuštění	k1gNnSc4	propuštění
900	[number]	k4	900
otroků	otrok	k1gMnPc2	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
867	[number]	k4	867
se	se	k3xPyFc4	se
apoštolové	apoštol	k1gMnPc1	apoštol
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
867	[number]	k4	867
byli	být	k5eAaImAgMnP	být
již	jenž	k3xRgMnPc1	jenž
duchovní	duchovní	k2eAgMnPc1d1	duchovní
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
vzděláváni	vzdělávat	k5eAaImNgMnP	vzdělávat
bratry	bratr	k1gMnPc7	bratr
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
připraveni	připravit	k5eAaPmNgMnP	připravit
na	na	k7c4	na
vysvěcení	vysvěcení	k1gNnSc4	vysvěcení
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
a	a	k8xC	a
biskupy	biskup	k1gMnPc4	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Konstantinem	Konstantin	k1gMnSc7	Konstantin
a	a	k8xC	a
Metodějem	Metoděj	k1gMnSc7	Metoděj
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
nebo	nebo	k8xC	nebo
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mělo	mít	k5eAaImAgNnS	mít
svěcení	svěcení	k1gNnSc1	svěcení
proběhnout	proběhnout	k5eAaPmF	proběhnout
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Balkán	Balkán	k1gInSc4	Balkán
byla	být	k5eAaImAgFnS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
lodí	loď	k1gFnPc2	loď
buď	buď	k8xC	buď
do	do	k7c2	do
Raveny	Ravena	k1gFnSc2	Ravena
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Konstantinopole	Konstantinopol	k1gInSc2	Konstantinopol
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
se	se	k3xPyFc4	se
však	však	k9	však
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Byzanci	Byzanc	k1gFnSc6	Byzanc
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Michael	Michael	k1gMnSc1	Michael
III	III	kA	III
<g/>
.	.	kIx.	.
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
;	;	kIx,	;
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
sesazen	sesadit	k5eAaPmNgMnS	sesadit
jejich	jejich	k3xOp3gMnSc1	jejich
přítel	přítel	k1gMnSc1	přítel
patriarcha	patriarcha	k1gMnSc1	patriarcha
Fotios	Fotios	k1gMnSc1	Fotios
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
misionáři	misionář	k1gMnPc1	misionář
bratři	bratr	k1gMnPc1	bratr
byli	být	k5eAaImAgMnP	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
věrozvěsty	věrozvěst	k1gMnPc7	věrozvěst
zastihl	zastihnout	k5eAaPmAgMnS	zastihnout
i	i	k9	i
zvací	zvací	k2eAgInSc1d1	zvací
list	list	k1gInSc1	list
napsaný	napsaný	k2eAgInSc1d1	napsaný
papežem	papež	k1gMnSc7	papež
Mikulášem	Mikuláš	k1gMnSc7	Mikuláš
I.	I.	kA	I.
Svatý	svatý	k2eAgMnSc1d1	svatý
otec	otec	k1gMnSc1	otec
totiž	totiž	k9	totiž
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
dva	dva	k4xCgMnPc1	dva
Byzantinci	Byzantinec	k1gMnPc1	Byzantinec
káží	kážit	k5eAaImIp3nP	kážit
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
i	i	k8xC	i
v	v	k7c6	v
Blatensku	Blatensko	k1gNnSc6	Blatensko
sice	sice	k8xC	sice
v	v	k7c6	v
nepovoleném	povolený	k2eNgInSc6d1	nepovolený
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
úspěšnými	úspěšný	k2eAgInPc7d1	úspěšný
misijními	misijní	k2eAgInPc7d1	misijní
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
vzal	vzít	k5eAaPmAgMnS	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
kosti	kost	k1gFnPc1	kost
svatého	svatý	k2eAgMnSc2d1	svatý
Klimenta	Kliment	k1gMnSc2	Kliment
jako	jako	k8xS	jako
dar	dar	k1gInSc4	dar
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
však	však	k9	však
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
I.	I.	kA	I.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Hadrián	Hadrián	k1gMnSc1	Hadrián
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
vyšel	vyjít	k5eAaPmAgInS	vyjít
vstříc	vstříc	k6eAd1	vstříc
před	před	k7c4	před
brány	brána	k1gFnPc4	brána
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
poklonil	poklonit	k5eAaPmAgMnS	poklonit
se	se	k3xPyFc4	se
Klimentovým	Klimentův	k2eAgInPc3d1	Klimentův
ostatkům	ostatek	k1gInPc3	ostatek
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
své	svůj	k3xOyFgMnPc4	svůj
hosty	host	k1gMnPc4	host
s	s	k7c7	s
velkolepostí	velkolepost	k1gFnSc7	velkolepost
hodnou	hodný	k2eAgFnSc4d1	hodná
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
učených	učený	k2eAgFnPc6d1	učená
disputacích	disputace	k1gFnPc6	disputace
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Konstantinovi	Konstantin	k1gMnSc3	Konstantin
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
obhájit	obhájit	k5eAaPmF	obhájit
před	před	k7c7	před
papežem	papež	k1gMnSc7	papež
a	a	k8xC	a
všemi	všecek	k3xTgFnPc7	všecek
církevními	církevní	k2eAgFnPc7d1	církevní
otci	otec	k1gMnSc6	otec
oprávněnost	oprávněnost	k1gFnSc4	oprávněnost
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
liturgie	liturgie	k1gFnSc2	liturgie
přímo	přímo	k6eAd1	přímo
dokonale	dokonale	k6eAd1	dokonale
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
868	[number]	k4	868
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
papež	papež	k1gMnSc1	papež
slovanský	slovanský	k2eAgInSc4d1	slovanský
překlad	překlad	k1gInSc4	překlad
bohoslužebných	bohoslužebný	k2eAgFnPc2d1	bohoslužebná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Položil	položit	k5eAaPmAgMnS	položit
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
oltář	oltář	k1gInSc4	oltář
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgNnSc1d1	Sněžné
<g/>
,	,	kIx,	,
posvětil	posvětit	k5eAaPmAgMnS	posvětit
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
Metoděje	Metoděj	k1gMnPc4	Metoděj
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
stála	stát	k5eAaImAgFnS	stát
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
snaha	snaha	k1gFnSc1	snaha
papeže	papež	k1gMnSc2	papež
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
spojence	spojenec	k1gMnSc2	spojenec
v	v	k7c6	v
byzantském	byzantský	k2eAgInSc6d1	byzantský
císaři	císař	k1gMnSc3	císař
Basileiovi	Basileius	k1gMnSc3	Basileius
I.	I.	kA	I.
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
muslimům	muslim	k1gMnPc3	muslim
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ohrožovali	ohrožovat	k5eAaImAgMnP	ohrožovat
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
zdrželi	zdržet	k5eAaPmAgMnP	zdržet
bratří	bratr	k1gMnPc1	bratr
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
869	[number]	k4	869
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
říká	říkat	k5eAaImIp3nS	říkat
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Konstantin	Konstantin	k1gMnSc1	Konstantin
však	však	k9	však
ochuravěl	ochuravět	k5eAaPmAgMnS	ochuravět
<g/>
,	,	kIx,	,
odřekl	odřeknout	k5eAaPmAgMnS	odřeknout
se	se	k3xPyFc4	se
další	další	k2eAgFnPc1d1	další
práce	práce	k1gFnPc1	práce
ve	v	k7c6	v
Slovanstvu	Slovanstvo	k1gNnSc6	Slovanstvo
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
nemocný	mocný	k2eNgMnSc1d1	nemocný
(	(	kIx(	(
<g/>
vyčerpaný	vyčerpaný	k2eAgMnSc1d1	vyčerpaný
mj.	mj.	kA	mj.
i	i	k8xC	i
neustálými	neustálý	k2eAgFnPc7d1	neustálá
hádkami	hádka	k1gFnPc7	hádka
se	s	k7c7	s
západním	západní	k2eAgInSc7d1	západní
klérem	klér	k1gInSc7	klér
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
zle	zle	k6eAd1	zle
dotíral	dotírat	k5eAaImAgMnS	dotírat
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
slouží	sloužit	k5eAaImIp3nS	sloužit
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
tito	tento	k3xDgMnPc1	tento
západní	západní	k2eAgMnPc1d1	západní
<g />
.	.	kIx.	.
</s>
<s>
klerici	klerik	k1gMnPc1	klerik
(	(	kIx(	(
<g/>
trojjazyčníci	trojjazyčník	k1gMnPc1	trojjazyčník
<g/>
)	)	kIx)	)
trvali	trvat	k5eAaImAgMnP	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
smějí	smát	k5eAaImIp3nP	smát
vykonávat	vykonávat	k5eAaImF	vykonávat
jen	jen	k9	jen
v	v	k7c6	v
těch	ten	k3xDgInPc6	ten
třech	tři	k4xCgInPc6	tři
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
nechal	nechat	k5eAaPmAgMnS	nechat
Pilát	Pilát	k1gMnSc1	Pilát
napsat	napsat	k5eAaPmF	napsat
nápis	nápis	k1gInSc4	nápis
na	na	k7c4	na
Kristův	Kristův	k2eAgInSc4d1	Kristův
kříž	kříž	k1gInSc4	kříž
<g/>
:	:	kIx,	:
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
a	a	k8xC	a
latinsky	latinsky	k6eAd1	latinsky
<g/>
;	;	kIx,	;
sv.	sv.	kA	sv.
Konstantin-Cyril	Konstantin-Cyril	k1gInSc1	Konstantin-Cyril
tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
nazýval	nazývat	k5eAaImAgMnS	nazývat
"	"	kIx"	"
<g/>
pilátnictvím	pilátnictví	k1gNnSc7	pilátnictví
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
trojjazyčnou	trojjazyčný	k2eAgFnSc7d1	trojjazyčná
herezí	hereze	k1gFnSc7	hereze
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svojí	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Konstantin	Konstantin	k1gMnSc1	Konstantin
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgInS	přijmout
řeholní	řeholní	k2eAgNnSc4d1	řeholní
(	(	kIx(	(
<g/>
mnišské	mnišský	k2eAgNnSc4d1	mnišské
<g/>
)	)	kIx)	)
jméno	jméno	k1gNnSc4	jméno
Cyril	Cyril	k1gMnSc1	Cyril
(	(	kIx(	(
<g/>
Kyrillos	Kyrillos	k1gMnSc1	Kyrillos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
padesát	padesát	k4xCc4	padesát
dní	den	k1gInPc2	den
po	po	k7c6	po
vstoupení	vstoupení	k1gNnSc6	vstoupení
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
řeckých	řecký	k2eAgInPc2d1	řecký
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
869	[number]	k4	869
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvaačtyřiceti	dvaačtyřicet	k4xCc2	dvaačtyřicet
let	léto	k1gNnPc2	léto
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
(	(	kIx(	(
<g/>
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
trpěl	trpět	k5eAaImAgMnS	trpět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
869	[number]	k4	869
vydal	vydat	k5eAaPmAgMnS	vydat
papež	papež	k1gMnSc1	papež
bulu	bula	k1gFnSc4	bula
Gloria	gloria	k1gNnSc2	gloria
in	in	k?	in
excelsis	excelsis	k1gFnSc1	excelsis
Deo	Deo	k1gFnSc1	Deo
(	(	kIx(	(
<g/>
Sláva	Sláva	k1gFnSc1	Sláva
na	na	k7c6	na
výsostech	výsost	k1gFnPc6	výsost
Bohu	bůh	k1gMnSc3	bůh
<g/>
)	)	kIx)	)
adresovanou	adresovaný	k2eAgFnSc4d1	adresovaná
Rastislavu	Rastislava	k1gFnSc4	Rastislava
a	a	k8xC	a
Svatopluku	Svatopluk	k1gMnSc3	Svatopluk
a	a	k8xC	a
Kocelovi	Kocel	k1gMnSc3	Kocel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
schválil	schválit	k5eAaPmAgInS	schválit
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
liturgii	liturgie	k1gFnSc4	liturgie
<g/>
.	.	kIx.	.
</s>
<s>
Metoděj	Metoděj	k1gMnSc1	Metoděj
byl	být	k5eAaImAgMnS	být
vyslán	vyslán	k2eAgMnSc1d1	vyslán
coby	coby	k?	coby
papežský	papežský	k2eAgInSc1d1	papežský
legát	legát	k1gInSc1	legát
do	do	k7c2	do
Panonie	Panonie	k1gFnSc2	Panonie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
Kocelem	Kocel	k1gInSc7	Kocel
dojednal	dojednat	k5eAaPmAgMnS	dojednat
papežův	papežův	k2eAgInSc4d1	papežův
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
nové	nový	k2eAgFnSc2d1	nová
samostatné	samostatný	k2eAgFnSc2d1	samostatná
panonské	panonský	k2eAgFnSc2d1	Panonská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
obnovit	obnovit	k5eAaPmF	obnovit
někdejší	někdejší	k2eAgFnSc4d1	někdejší
sremskou	sremský	k2eAgFnSc4d1	sremský
arcidiecézi	arcidiecéze	k1gFnSc4	arcidiecéze
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Sirmiu	Sirmium	k1gNnSc6	Sirmium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mosaburgu	Mosaburg	k1gInSc6	Mosaburg
byl	být	k5eAaImAgInS	být
vřele	vřele	k6eAd1	vřele
přivítán	přivítat	k5eAaPmNgInS	přivítat
<g/>
,	,	kIx,	,
Kocel	Kocel	k1gInSc1	Kocel
s	s	k7c7	s
návrhy	návrh	k1gInPc7	návrh
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
souhlasil	souhlasit	k5eAaImAgInS	souhlasit
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
salcburského	salcburský	k2eAgMnSc4d1	salcburský
archipresbitera	archipresbiter	k1gMnSc4	archipresbiter
Riphalda	Riphaldo	k1gNnSc2	Riphaldo
a	a	k8xC	a
církevní	církevní	k2eAgFnSc7d1	církevní
správou	správa	k1gFnSc7	správa
Panonie	Panonie	k1gFnSc2	Panonie
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Metoděje	Metoděj	k1gMnSc4	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
s	s	k7c7	s
Kocelovou	Kocelův	k2eAgFnSc7d1	Kocelův
kladnou	kladný	k2eAgFnSc7d1	kladná
odpovědí	odpověď	k1gFnSc7	odpověď
a	a	k8xC	a
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
dvaceti	dvacet	k4xCc7	dvacet
vyslanci	vyslanec	k1gMnPc1	vyslanec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
869	[number]	k4	869
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
na	na	k7c4	na
biskupa	biskup	k1gMnSc4	biskup
a	a	k8xC	a
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
prvním	první	k4xOgMnSc6	první
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
panonsko-moravské	panonskooravský	k2eAgFnSc2d1	panonsko-moravský
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
do	do	k7c2	do
Panonie	Panonie	k1gFnSc2	Panonie
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
Metoděj	Metoděj	k1gMnSc1	Metoděj
vracel	vracet	k5eAaImAgMnS	vracet
(	(	kIx(	(
<g/>
po	po	k7c6	po
Vánocích	Vánoce	k1gFnPc6	Vánoce
roku	rok	k1gInSc2	rok
869	[number]	k4	869
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
coby	coby	k?	coby
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
a	a	k8xC	a
papežský	papežský	k2eAgMnSc1d1	papežský
legát	legát	k1gMnSc1	legát
s	s	k7c7	s
bulou	bula	k1gFnSc7	bula
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jednoznačně	jednoznačně	k6eAd1	jednoznačně
stálo	stát	k5eAaImAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	zem	k1gFnPc1	zem
knížat	kníže	k1gMnPc2wR	kníže
Rostislava	Rostislav	k1gMnSc2	Rostislav
<g/>
,	,	kIx,	,
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
a	a	k8xC	a
Kocela	Kocel	k1gMnSc2	Kocel
náležejí	náležet	k5eAaImIp3nP	náležet
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
římského	římský	k2eAgInSc2d1	římský
stolce	stolec	k1gInSc2	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tříměsíčním	tříměsíční	k2eAgNnSc6d1	tříměsíční
zdržení	zdržení	k1gNnSc6	zdržení
v	v	k7c6	v
Panonii	Panonie	k1gFnSc6	Panonie
se	se	k3xPyFc4	se
Metoděj	Metoděj	k1gMnSc1	Metoděj
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
870	[number]	k4	870
se	se	k3xPyFc4	se
Rostislavův	Rostislavův	k2eAgMnSc1d1	Rostislavův
synovec	synovec	k1gMnSc1	synovec
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
<g/>
.	.	kIx.	.
<g/>
pověřený	pověřený	k2eAgMnSc1d1	pověřený
panováním	panování	k1gNnSc7	panování
nad	nad	k7c7	nad
Nitranským	nitranský	k2eAgNnSc7d1	Nitranské
knížectvím	knížectví	k1gNnSc7	knížectví
(	(	kIx(	(
<g/>
samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
součást	součást	k1gFnSc1	součást
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Východofranskou	východofranský	k2eAgFnSc7d1	Východofranská
říší	říš	k1gFnSc7	říš
<g/>
,	,	kIx,	,
zajal	zajmout	k5eAaPmAgMnS	zajmout
Rostislava	Rostislava	k1gFnSc1	Rostislava
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
jej	on	k3xPp3gMnSc4	on
Frankům	Franky	k1gInPc3	Franky
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
byl	být	k5eAaImAgMnS	být
oslepen	oslepit	k5eAaPmNgMnS	oslepit
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
v	v	k7c6	v
některém	některý	k3yIgInSc6	některý
z	z	k7c2	z
bavorských	bavorský	k2eAgInPc2d1	bavorský
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
snad	snad	k9	snad
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
sv.	sv.	kA	sv.
Jimráma	Jimrám	k1gMnSc2	Jimrám
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zanedlouho	zanedlouho	k6eAd1	zanedlouho
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
politicky	politicky	k6eAd1	politicky
složité	složitý	k2eAgFnSc2d1	složitá
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgMnS	vracet
Metoděj	Metoděj	k1gMnSc1	Metoděj
přes	přes	k7c4	přes
Panonii	Panonie	k1gFnSc4	Panonie
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
poslání	poslání	k1gNnSc6	poslání
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k9	ještě
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
Franky	Franky	k1gInPc7	Franky
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
870	[number]	k4	870
povolán	povolat	k5eAaPmNgMnS	povolat
na	na	k7c4	na
církevní	církevní	k2eAgInSc4d1	církevní
sjezd	sjezd	k1gInSc4	sjezd
nejspíš	nejspíš	k9	nejspíš
do	do	k7c2	do
Řezna	Řezno	k1gNnSc2	Řezno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
byl	být	k5eAaImAgInS	být
započat	započat	k2eAgInSc4d1	započat
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
podnítil	podnítit	k5eAaPmAgMnS	podnítit
Ludvík	Ludvík	k1gMnSc1	Ludvík
Němec	Němec	k1gMnSc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Metoděj	Metoděj	k1gMnSc1	Metoděj
byl	být	k5eAaImAgMnS	být
sesazen	sesadit	k5eAaPmNgMnS	sesadit
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ho	on	k3xPp3gMnSc4	on
dopravili	dopravit	k5eAaPmAgMnP	dopravit
patrně	patrně	k6eAd1	patrně
do	do	k7c2	do
Ellwangenu	Ellwangen	k1gInSc2	Ellwangen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
klášterní	klášterní	k2eAgFnSc6d1	klášterní
pevnosti	pevnost	k1gFnSc6	pevnost
věznili	věznit	k5eAaImAgMnP	věznit
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
jámě	jáma	k1gFnSc6	jáma
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
ho	on	k3xPp3gInSc4	on
až	až	k6eAd1	až
papežský	papežský	k2eAgInSc4d1	papežský
zásah	zásah	k1gInSc4	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Metoděj	Metoděj	k1gMnSc1	Metoděj
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
propuštěn	propuštěn	k2eAgInSc4d1	propuštěn
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
dosazen	dosadit	k5eAaPmNgInS	dosadit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
roku	rok	k1gInSc2	rok
873	[number]	k4	873
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
biskup	biskup	k1gMnSc1	biskup
Metoděj	Metoděj	k1gMnSc1	Metoděj
ujímá	ujímat	k5eAaImIp3nS	ujímat
správy	správa	k1gFnSc2	správa
všech	všecek	k3xTgInPc2	všecek
moravských	moravský	k2eAgInPc2d1	moravský
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
kleriků	klerik	k1gMnPc2	klerik
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Za	za	k7c4	za
moravské	moravský	k2eAgNnSc4d1	Moravské
sídlo	sídlo	k1gNnSc4	sídlo
biskupa	biskup	k1gInSc2	biskup
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
považovány	považován	k2eAgFnPc1d1	považována
dnešní	dnešní	k2eAgFnPc1d1	dnešní
Sady	sada	k1gFnPc1	sada
u	u	k7c2	u
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
v	v	k7c6	v
r.	r.	kA	r.
870	[number]	k4	870
vydal	vydat	k5eAaPmAgMnS	vydat
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Frankům	Frank	k1gMnPc3	Frank
zajatého	zajatý	k1gMnSc4	zajatý
Rostislava	Rostislav	k1gMnSc4	Rostislav
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
i	i	k9	i
on	on	k3xPp3gMnSc1	on
jimi	on	k3xPp3gMnPc7	on
zajat	zajmout	k5eAaPmNgMnS	zajmout
a	a	k8xC	a
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Obvinění	obviněný	k1gMnPc1	obviněný
ze	z	k7c2	z
zrady	zrada	k1gFnSc2	zrada
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
propuštěn	propustit	k5eAaPmNgMnS	propustit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
r.	r.	kA	r.
871	[number]	k4	871
vyslán	vyslat	k5eAaPmNgMnS	vyslat
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
franského	franský	k2eAgNnSc2d1	franské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
svěřil	svěřit	k5eAaPmAgMnS	svěřit
syn	syn	k1gMnSc1	syn
Ludvíka	Ludvík	k1gMnSc2	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Němce	Němka	k1gFnSc3	Němka
Karloman	Karloman	k1gMnSc1	Karloman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
potlačil	potlačit	k5eAaPmAgMnS	potlačit
povstání	povstání	k1gNnSc4	povstání
Moravanů	Moravan	k1gMnPc2	Moravan
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
se	s	k7c7	s
Slavomírem	Slavomír	k1gMnSc7	Slavomír
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
starým	starý	k2eAgNnSc7d1	staré
Rostislavovým	Rostislavův	k2eAgNnSc7d1	Rostislavovo
městem	město	k1gNnSc7	město
však	však	k8xC	však
zanechal	zanechat	k5eAaPmAgInS	zanechat
Franky	Frank	k1gMnPc4	Frank
budovat	budovat	k5eAaImF	budovat
tábor	tábor	k1gInSc1	tábor
a	a	k8xC	a
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Moravanů	Moravan	k1gMnPc2	Moravan
a	a	k8xC	a
nic	nic	k6eAd1	nic
netušící	tušící	k2eNgMnPc4d1	netušící
Franky	Frank	k1gMnPc4	Frank
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Slavomírova	Slavomírův	k2eAgNnSc2d1	Slavomírův
vojska	vojsko	k1gNnSc2	vojsko
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
a	a	k8xC	a
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
armádu	armáda	k1gFnSc4	armáda
zničil	zničit	k5eAaPmAgMnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
drtivou	drtivý	k2eAgFnSc7d1	drtivá
porážkou	porážka	k1gFnSc7	porážka
Franků	Franky	k1gInPc2	Franky
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
prakticky	prakticky	k6eAd1	prakticky
převzal	převzít	k5eAaPmAgMnS	převzít
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
porážkami	porážka	k1gFnPc7	porážka
Franků	Frank	k1gMnPc2	Frank
v	v	k7c6	v
r.	r.	kA	r.
872	[number]	k4	872
zcela	zcela	k6eAd1	zcela
osamostatnil	osamostatnit	k5eAaPmAgMnS	osamostatnit
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
sám	sám	k3xTgMnSc1	sám
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
za	za	k7c2	za
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Metodějem	Metoděj	k1gMnSc7	Metoděj
<g/>
)	)	kIx)	)
šířit	šířit	k5eAaImF	šířit
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
vliv	vliv	k1gInSc1	vliv
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
nerušeně	nerušeně	k6eAd1	nerušeně
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
svou	svůj	k3xOyFgFnSc4	svůj
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
874	[number]	k4	874
s	s	k7c7	s
Franky	Frank	k1gMnPc4	Frank
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
poselstva	poselstvo	k1gNnSc2	poselstvo
při	při	k7c6	při
uzavírání	uzavírání	k1gNnSc6	uzavírání
míru	mír	k1gInSc2	mír
stál	stát	k5eAaImAgMnS	stát
kněz	kněz	k1gMnSc1	kněz
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
,	,	kIx,	,
Svatoplukův	Svatoplukův	k2eAgMnSc1d1	Svatoplukův
důvěrník	důvěrník	k1gMnSc1	důvěrník
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
byl	být	k5eAaImAgMnS	být
Metoděj	Metoděj	k1gMnSc1	Metoděj
obviněn	obviněn	k2eAgMnSc1d1	obviněn
právě	právě	k6eAd1	právě
knězem	kněz	k1gMnSc7	kněz
Janem	Jan	k1gMnSc7	Jan
z	z	k7c2	z
Benátek	Benátky	k1gFnPc2	Benátky
z	z	k7c2	z
kacířství	kacířství	k1gNnSc2	kacířství
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
povolán	povolán	k2eAgInSc1d1	povolán
v	v	k7c6	v
r.	r.	kA	r.
879	[number]	k4	879
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
před	před	k7c7	před
papežem	papež	k1gMnSc7	papež
Janem	Jan	k1gMnSc7	Jan
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
používání	používání	k1gNnSc2	používání
staroslověnštiny	staroslověnština	k1gFnSc2	staroslověnština
obhájil	obhájit	k5eAaPmAgInS	obhájit
<g/>
;	;	kIx,	;
následně	následně	k6eAd1	následně
papež	papež	k1gMnSc1	papež
Hadrián	Hadrián	k1gMnSc1	Hadrián
II	II	kA	II
<g/>
.	.	kIx.	.
staroslověnskou	staroslověnský	k2eAgFnSc4d1	staroslověnská
liturgii	liturgie	k1gFnSc4	liturgie
schválil	schválit	k5eAaPmAgInS	schválit
bulou	bula	k1gFnSc7	bula
Industriae	Industria	k1gInSc2	Industria
tuae	tuae	k1gInSc1	tuae
(	(	kIx(	(
<g/>
Horlivosti	horlivost	k1gFnPc1	horlivost
tvé	tvůj	k3xOp2gFnSc2	tvůj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
epištola	epištola	k1gFnSc1	epištola
a	a	k8xC	a
evangelium	evangelium	k1gNnSc1	evangelium
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
čteny	číst	k5eAaImNgInP	číst
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
a	a	k8xC	a
až	až	k6eAd1	až
poté	poté	k6eAd1	poté
ve	v	k7c6	v
staroslověnštině	staroslověnština	k1gFnSc6	staroslověnština
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
knížete	kníže	k1gMnSc2	kníže
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
papež	papež	k1gMnSc1	papež
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
nitranským	nitranský	k2eAgMnSc7d1	nitranský
biskupem	biskup	k1gMnSc7	biskup
německého	německý	k2eAgMnSc2d1	německý
kněze	kněz	k1gMnSc2	kněz
Wichinga	Wiching	k1gMnSc2	Wiching
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
nový	nový	k2eAgInSc1d1	nový
biskup	biskup	k1gInSc1	biskup
Wiching	Wiching	k1gInSc1	Wiching
Metoděje	Metoděj	k1gMnSc2	Metoděj
předběhl	předběhnout	k5eAaPmAgInS	předběhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
dřív	dříve	k6eAd2	dříve
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
mu	on	k3xPp3gMnSc3	on
předložit	předložit	k5eAaPmF	předložit
padělanou	padělaný	k2eAgFnSc4d1	padělaná
listinu	listina	k1gFnSc4	listina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
papež	papež	k1gMnSc1	papež
Metoděje	Metoděj	k1gMnSc4	Metoděj
sesadil	sesadit	k5eAaPmAgMnS	sesadit
<g/>
.	.	kIx.	.
</s>
<s>
Podvod	podvod	k1gInSc1	podvod
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
odhalen	odhalen	k2eAgMnSc1d1	odhalen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgInS	pokusit
Wiching	Wiching	k1gInSc1	Wiching
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
podvrh	podvrh	k1gInSc4	podvrh
<g/>
,	,	kIx,	,
když	když	k8xS	když
zfalšoval	zfalšovat	k5eAaPmAgMnS	zfalšovat
papežský	papežský	k2eAgInSc4d1	papežský
dopis	dopis	k1gInSc4	dopis
proti	proti	k7c3	proti
Metodějovi	Metoděj	k1gMnSc3	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Metodějův	Metodějův	k2eAgInSc4d1	Metodějův
dotaz	dotaz	k1gInSc4	dotaz
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
881	[number]	k4	881
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
podobný	podobný	k2eAgInSc1d1	podobný
list	list	k1gInSc1	list
Wichingovi	Wiching	k1gMnSc3	Wiching
poslal	poslat	k5eAaPmAgMnS	poslat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Metodějovi	Metoděj	k1gMnSc3	Metoděj
plnou	plný	k2eAgFnSc4d1	plná
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
slíbil	slíbit	k5eAaPmAgMnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wichinga	Wichinga	k1gFnSc1	Wichinga
zavolá	zavolat	k5eAaPmIp3nS	zavolat
před	před	k7c4	před
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
882	[number]	k4	882
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Metoděj	Metoděj	k1gMnSc1	Metoděj
list	list	k1gInSc4	list
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
Basileia	Basileium	k1gNnSc2	Basileium
I.	I.	kA	I.
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
císař	císař	k1gMnSc1	císař
přání	přání	k1gNnSc4	přání
vidět	vidět	k5eAaImF	vidět
se	se	k3xPyFc4	se
s	s	k7c7	s
proslulým	proslulý	k2eAgMnSc7d1	proslulý
Metodějem	Metoděj	k1gMnSc7	Metoděj
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
ještě	ještě	k6eAd1	ještě
živi	živ	k2eAgMnPc1d1	živ
<g/>
.	.	kIx.	.
</s>
<s>
Metoděj	Metoděj	k1gMnSc1	Metoděj
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
váhání	váhání	k1gNnSc2	váhání
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
poslední	poslední	k2eAgFnSc4d1	poslední
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
řecké	řecký	k2eAgFnSc2d1	řecká
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
vřele	vřele	k6eAd1	vřele
a	a	k8xC	a
slavnostně	slavnostně	k6eAd1	slavnostně
přijat	přijmout	k5eAaPmNgMnS	přijmout
Basileiem	Basileius	k1gMnSc7	Basileius
I.	I.	kA	I.
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
zanechal	zanechat	k5eAaPmAgMnS	zanechat
opisy	opis	k1gInPc4	opis
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
bohoslužebných	bohoslužebný	k2eAgFnPc2d1	bohoslužebná
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
využity	využít	k5eAaPmNgFnP	využít
při	při	k7c6	při
dalších	další	k2eAgFnPc6d1	další
misijních	misijní	k2eAgFnPc6d1	misijní
cestách	cesta	k1gFnPc6	cesta
do	do	k7c2	do
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
a	a	k8xC	a
na	na	k7c4	na
Rus	Rus	k1gFnSc4	Rus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
884	[number]	k4	884
byl	být	k5eAaImAgMnS	být
Metoděj	Metoděj	k1gMnSc1	Metoděj
již	již	k9	již
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
překladu	překlad	k1gInSc6	překlad
zbývajících	zbývající	k2eAgFnPc2d1	zbývající
částí	část	k1gFnPc2	část
Písma	písmo	k1gNnSc2	písmo
svatého	svatý	k2eAgNnSc2d1	svaté
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
Metoděj	Metoděj	k1gMnSc1	Metoděj
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
uvalit	uvalit	k5eAaPmF	uvalit
na	na	k7c4	na
Wichinga	Wiching	k1gMnSc4	Wiching
klatbu	klatba	k1gFnSc4	klatba
a	a	k8xC	a
zbavit	zbavit	k5eAaPmF	zbavit
ho	on	k3xPp3gMnSc4	on
tak	tak	k6eAd1	tak
biskupské	biskupský	k2eAgFnPc1d1	biskupská
funkce	funkce	k1gFnPc1	funkce
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
podřídí	podřídit	k5eAaPmIp3nS	podřídit
jeho	jeho	k3xOp3gFnSc3	jeho
autoritě	autorita	k1gFnSc3	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Wiching	Wiching	k1gInSc1	Wiching
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
udání	udání	k1gNnSc2	udání
<g/>
)	)	kIx)	)
docílil	docílit	k5eAaPmAgMnS	docílit
u	u	k7c2	u
nového	nový	k2eAgMnSc2d1	nový
papeže	papež	k1gMnSc2	papež
Štěpána	Štěpán	k1gMnSc2	Štěpán
V.	V.	kA	V.
<g/>
,	,	kIx,	,
zákazu	zákaz	k1gInSc2	zákaz
slovanské	slovanský	k2eAgFnPc4d1	Slovanská
liturgie	liturgie	k1gFnPc4	liturgie
a	a	k8xC	a
nařízení	nařízení	k1gNnPc4	nařízení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
moravští	moravský	k2eAgMnPc1d1	moravský
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nepodřídí	podřídit	k5eNaPmIp3nS	podřídit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
vypuzeni	vypudit	k5eAaPmNgMnP	vypudit
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Těžce	těžce	k6eAd1	těžce
nemocný	nemocný	k2eAgMnSc1d1	nemocný
Metoděj	Metoděj	k1gMnSc1	Metoděj
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
překladů	překlad	k1gInPc2	překlad
<g/>
,	,	kIx,	,
když	když	k8xS	když
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
naplňovat	naplňovat	k5eAaImF	naplňovat
<g/>
,	,	kIx,	,
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
svým	svůj	k3xOyFgMnSc7	svůj
nástupcem	nástupce	k1gMnSc7	nástupce
Slovana	Slovan	k1gMnSc2	Slovan
Gorazda	Gorazd	k1gMnSc2	Gorazd
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
"	"	kIx"	"
<g/>
šestého	šestý	k4xOgInSc2	šestý
dne	den	k1gInSc2	den
měsíce	měsíc	k1gInSc2	měsíc
dubna	duben	k1gInSc2	duben
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
indikci	indikce	k1gFnSc6	indikce
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
šestitisícího	šestitisící	k2eAgNnSc2d1	šestitisící
třístého	třístý	k2eAgNnSc2d1	třístý
devadesátého	devadesátý	k4xOgInSc2	devadesátý
třetího	třetí	k4xOgInSc2	třetí
od	od	k7c2	od
stvoření	stvoření	k1gNnSc2	stvoření
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
nového	nový	k2eAgInSc2d1	nový
kalendáře	kalendář	k1gInSc2	kalendář
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
885	[number]	k4	885
<g/>
)	)	kIx)	)
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
72	[number]	k4	72
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Metodějově	Metodějův	k2eAgFnSc6d1	Metodějova
smrti	smrt	k1gFnSc6	smrt
intrikoval	intrikovat	k5eAaImAgMnS	intrikovat
Wiching	Wiching	k1gInSc4	Wiching
s	s	k7c7	s
Římem	Řím	k1gInSc7	Řím
a	a	k8xC	a
ujal	ujmout	k5eAaPmAgMnS	ujmout
se	se	k3xPyFc4	se
církevních	církevní	k2eAgFnPc2d1	církevní
záležitostí	záležitost	k1gFnPc2	záležitost
s	s	k7c7	s
brutalitou	brutalita	k1gFnSc7	brutalita
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc4	jaký
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
jenom	jenom	k9	jenom
málokterý	málokterý	k3yIgMnSc1	málokterý
z	z	k7c2	z
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
duchovních	duchovní	k2eAgMnPc2d1	duchovní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Wichingových	Wichingový	k2eAgFnPc2d1	Wichingový
zpráv	zpráva	k1gFnPc2	zpráva
byl	být	k5eAaImAgInS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
papežský	papežský	k2eAgInSc1d1	papežský
list	list	k1gInSc1	list
Štěpána	Štěpán	k1gMnSc2	Štěpán
V.	V.	kA	V.
(	(	kIx(	(
<g/>
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
byl	být	k5eAaImAgInS	být
právě	právě	k6eAd1	právě
Wiching	Wiching	k1gInSc1	Wiching
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
Metodějovým	Metodějův	k2eAgMnSc7d1	Metodějův
nástupcem	nástupce	k1gMnSc7	nástupce
jako	jako	k8xS	jako
moravský	moravský	k2eAgInSc1d1	moravský
biskup	biskup	k1gInSc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
Metodějem	Metoděj	k1gMnSc7	Metoděj
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
moravského	moravský	k2eAgInSc2d1	moravský
původu	původ	k1gInSc2	původ
jménem	jméno	k1gNnSc7	jméno
Gorazd	Gorazd	k1gMnSc1	Gorazd
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
uvržen	uvrhnout	k5eAaPmNgInS	uvrhnout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
jinými	jiný	k2eAgMnPc7d1	jiný
do	do	k7c2	do
žaláře	žalář	k1gInSc2	žalář
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
zprávy	zpráva	k1gFnPc1	zpráva
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Staroslověnsky	staroslověnsky	k6eAd1	staroslověnsky
vzdělaní	vzdělaný	k2eAgMnPc1d1	vzdělaný
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
mniši	mnich	k1gMnPc1	mnich
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
stovky	stovka	k1gFnPc4	stovka
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
doslova	doslova	k6eAd1	doslova
vyvedeni	vyvést	k5eAaPmNgMnP	vyvést
v	v	k7c6	v
okovech	okov	k1gInPc6	okov
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tito	tento	k3xDgMnPc1	tento
Metodějovi	Metodějův	k2eAgMnPc1d1	Metodějův
žáci	žák	k1gMnPc1	žák
nechtěli	chtít	k5eNaImAgMnP	chtít
zradit	zradit	k5eAaPmF	zradit
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
odkaz	odkaz	k1gInSc4	odkaz
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
a	a	k8xC	a
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
podřídit	podřídit	k5eAaPmF	podřídit
se	se	k3xPyFc4	se
papežovu	papežův	k2eAgNnSc3d1	papežovo
usnesení	usnesení	k1gNnSc3	usnesení
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
našli	najít	k5eAaPmAgMnP	najít
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc3	Bulharsko
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
i	i	k8xC	i
archeologové	archeolog	k1gMnPc1	archeolog
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
Života	život	k1gInSc2	život
Metodějova	Metodějův	k2eAgInSc2d1	Metodějův
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
písemná	písemný	k2eAgFnSc1d1	písemná
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
krátkých	krátký	k2eAgInPc2d1	krátký
<g/>
,	,	kIx,	,
takzvaných	takzvaný	k2eAgInPc2d1	takzvaný
"	"	kIx"	"
<g/>
proložních	proložní	k2eAgInPc2d1	proložní
<g/>
"	"	kIx"	"
životopisů	životopis	k1gInPc2	životopis
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
o	o	k7c6	o
Metodějově	Metodějův	k2eAgInSc6d1	Metodějův
pohřbu	pohřeb	k1gInSc6	pohřeb
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
naučil	naučit	k5eAaPmAgMnS	naučit
své	svůj	k3xOyFgMnPc4	svůj
učedníky	učedník	k1gMnPc4	učedník
pravé	pravý	k2eAgFnSc2d1	pravá
víře	víra	k1gFnSc3	víra
a	a	k8xC	a
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
svou	svůj	k3xOyFgFnSc4	svůj
smrt	smrt	k1gFnSc4	smrt
tři	tři	k4xCgFnPc4	tři
dny	dna	k1gFnPc4	dna
předem	předem	k6eAd1	předem
<g/>
,	,	kIx,	,
zesnul	zesnout	k5eAaPmAgMnS	zesnout
smířen	smířit	k5eAaPmNgMnS	smířit
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
pak	pak	k6eAd1	pak
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
chrámu	chrám	k1gInSc6	chrám
moravském	moravský	k2eAgInSc6d1	moravský
po	po	k7c6	po
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
za	za	k7c7	za
oltářem	oltář	k1gInSc7	oltář
svaté	svatý	k2eAgFnSc2d1	svatá
bohorodičky	bohorodička	k1gFnSc2	bohorodička
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pro	pro	k7c4	pro
archeology	archeolog	k1gMnPc4	archeolog
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
najít	najít	k5eAaPmF	najít
Metodějův	Metodějův	k2eAgInSc4d1	Metodějův
hrob	hrob	k1gInSc4	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
objevil	objevit	k5eAaPmAgMnS	objevit
profesor	profesor	k1gMnSc1	profesor
Josef	Josef	k1gMnSc1	Josef
Poulík	Poulík	k1gMnSc1	Poulík
vedle	vedle	k7c2	vedle
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
zdi	zeď	k1gFnSc2	zeď
trojlodní	trojlodní	k2eAgFnSc2d1	trojlodní
baziliky	bazilika	k1gFnSc2	bazilika
v	v	k7c6	v
Mikulčicích	Mikulčice	k1gFnPc6	Mikulčice
místo	místo	k7c2	místo
(	(	kIx(	(
<g/>
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xS	jako
hrob	hrob	k1gInSc1	hrob
580	[number]	k4	580
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
historiků	historik	k1gMnPc2	historik
mohlo	moct	k5eAaImAgNnS	moct
patřit	patřit	k5eAaImF	patřit
Metodějovi	Metodějův	k2eAgMnPc1d1	Metodějův
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
odhadovaného	odhadovaný	k2eAgInSc2d1	odhadovaný
věku	věk	k1gInSc2	věk
40	[number]	k4	40
let	léto	k1gNnPc2	léto
spíš	spát	k5eAaImIp2nS	spát
významnému	významný	k2eAgMnSc3d1	významný
Mojmírovci	Mojmírovec	k1gMnSc3	Mojmírovec
<g/>
,	,	kIx,	,
předměty	předmět	k1gInPc4	předmět
zde	zde	k6eAd1	zde
nalezené	nalezený	k2eAgFnPc4d1	nalezená
(	(	kIx(	(
<g/>
těžký	těžký	k2eAgInSc4d1	těžký
<g />
.	.	kIx.	.
</s>
<s>
dvojsečný	dvojsečný	k2eAgInSc1d1	dvojsečný
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
sekera-bradatice	sekeraradatice	k1gFnSc1	sekera-bradatice
<g/>
,	,	kIx,	,
mečík	mečík	k1gInSc1	mečík
či	či	k8xC	či
dýka	dýka	k1gFnSc1	dýka
<g/>
,	,	kIx,	,
nožík	nožík	k1gInSc1	nožík
či	či	k8xC	či
břitva	břitva	k1gFnSc1	břitva
<g/>
,	,	kIx,	,
vědro	vědro	k1gNnSc1	vědro
<g/>
,	,	kIx,	,
kování	kování	k1gNnSc1	kování
opasku	opasek	k1gInSc2	opasek
<g/>
,	,	kIx,	,
zlatý	zlatý	k2eAgInSc1d1	zlatý
gombík	gombík	k1gInSc1	gombík
<g/>
)	)	kIx)	)
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochovaný	pochovaný	k2eAgInSc1d1	pochovaný
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
nejvyšší	vysoký	k2eAgFnSc3d3	nejvyšší
společenské	společenský	k2eAgFnSc3d1	společenská
vrstvě	vrstva	k1gFnSc3	vrstva
Velké	velký	k2eAgFnSc2d1	velká
Moravy	Morava	k1gFnSc2	Morava
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
značně	značně	k6eAd1	značně
zetlelé	zetlelý	k2eAgInPc1d1	zetlelý
<g/>
.	.	kIx.	.
</s>
<s>
Datování	datování	k1gNnSc1	datování
nálezu	nález	k1gInSc2	nález
<g/>
:	:	kIx,	:
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
poslední	poslední	k2eAgFnSc2d1	poslední
třetiny	třetina	k1gFnSc2	třetina
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
upřesněno	upřesnit	k5eAaPmNgNnS	upřesnit
revizním	revizní	k2eAgInSc7d1	revizní
výzkumem	výzkum	k1gInSc7	výzkum
provedeným	provedený	k2eAgInSc7d1	provedený
archeologem	archeolog	k1gMnSc7	archeolog
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Lumírem	Lumír	k1gMnSc7	Lumír
Poláčkem	Poláček	k1gMnSc7	Poláček
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Velmož	velmož	k1gMnSc1	velmož
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
nejpozději	pozdě	k6eAd3	pozdě
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
1963	[number]	k4	1963
objevil	objevit	k5eAaPmAgMnS	objevit
vedoucí	vedoucí	k1gMnSc1	vedoucí
archeologického	archeologický	k2eAgInSc2d1	archeologický
výzkumu	výzkum	k1gInSc2	výzkum
tzv.	tzv.	kA	tzv.
metropolitní	metropolitní	k2eAgFnPc4d1	metropolitní
výšiny	výšina	k1gFnPc4	výšina
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
-	-	kIx~	-
<g/>
Sadech	sad	k1gInPc6	sad
Vilém	Vilém	k1gMnSc1	Vilém
Hrubý	Hrubý	k1gMnSc1	Hrubý
ve	v	k7c6	v
zdivu	zdivo	k1gNnSc6	zdivo
velkomoravského	velkomoravský	k2eAgInSc2d1	velkomoravský
kostela	kostel	k1gInSc2	kostel
hrobové	hrobový	k2eAgFnSc2d1	hrobová
dutiny	dutina	k1gFnSc2	dutina
z	z	k7c2	z
vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
přizdívkami	přizdívka	k1gFnPc7	přizdívka
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poloze	poloha	k1gFnSc6	poloha
možného	možný	k2eAgInSc2d1	možný
hrobu	hrob	k1gInSc2	hrob
sv.	sv.	kA	sv.
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
,	,	kIx,	,
uvedené	uvedený	k2eAgInPc1d1	uvedený
v	v	k7c6	v
písemné	písemný	k2eAgFnSc6d1	písemná
zprávě	zpráva	k1gFnSc6	zpráva
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
však	však	k9	však
našel	najít	k5eAaPmAgInS	najít
hrob	hrob	k1gInSc1	hrob
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kromě	kromě	k7c2	kromě
umístění	umístění	k1gNnSc2	umístění
hrobky	hrobka	k1gFnSc2	hrobka
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
svědčilo	svědčit	k5eAaImAgNnS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
pochován	pochován	k2eAgMnSc1d1	pochován
Metoděj	Metoděj	k1gMnSc1	Metoděj
(	(	kIx(	(
<g/>
na	na	k7c4	na
Metoděje	Metoděj	k1gMnSc4	Metoděj
byla	být	k5eAaImAgFnS	být
uvržena	uvržen	k2eAgFnSc1d1	uvržena
klatba	klatba	k1gFnSc1	klatba
a	a	k8xC	a
z	z	k7c2	z
hrobu	hrob	k1gInSc2	hrob
byl	být	k5eAaImAgInS	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
přemístěn	přemístěn	k2eAgInSc1d1	přemístěn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
i	i	k8xC	i
Slovensko	Slovensko	k1gNnSc1	Slovensko
slaví	slavit	k5eAaImIp3nS	slavit
svátek	svátek	k1gInSc4	svátek
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Bedřicha	Bedřich	k1gMnSc2	Bedřich
z	z	k7c2	z
Fürstenberka	Fürstenberka	k1gFnSc1	Fürstenberka
stanoveny	stanovit	k5eAaPmNgInP	stanovit
papežem	papež	k1gMnSc7	papež
Piem	Pius	k1gMnSc7	Pius
IX	IX	kA	IX
<g/>
.	.	kIx.	.
oslavy	oslava	k1gFnPc1	oslava
tisíciletého	tisíciletý	k2eAgNnSc2d1	tisícileté
výročí	výročí	k1gNnSc2	výročí
příchodu	příchod	k1gInSc2	příchod
bratří	bratr	k1gMnPc2	bratr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
863	[number]	k4	863
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
datum	datum	k1gNnSc1	datum
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
nemá	mít	k5eNaImIp3nS	mít
zřejmou	zřejmý	k2eAgFnSc4d1	zřejmá
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
životem	život	k1gInSc7	život
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
ani	ani	k8xC	ani
s	s	k7c7	s
cyrilometodějstvím	cyrilometodějství	k1gNnSc7	cyrilometodějství
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
důvody	důvod	k1gInPc7	důvod
pro	pro	k7c4	pro
přesun	přesun	k1gInSc4	přesun
byla	být	k5eAaImAgFnS	být
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
roční	roční	k2eAgFnSc1d1	roční
i	i	k8xC	i
liturgická	liturgický	k2eAgFnSc1d1	liturgická
doba	doba	k1gFnSc1	doba
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
data	datum	k1gNnSc2	datum
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
také	také	k9	také
vzrůstající	vzrůstající	k2eAgInSc1d1	vzrůstající
kult	kult	k1gInSc1	kult
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
svátek	svátek	k1gInSc1	svátek
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Okružním	okružní	k2eAgInSc7d1	okružní
listem	list	k1gInSc7	list
"	"	kIx"	"
<g/>
Grande	grand	k1gMnSc5	grand
munus	munus	k1gInSc4	munus
<g/>
"	"	kIx"	"
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1880	[number]	k4	1880
vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
papež	papež	k1gMnSc1	papež
Lev	Lev	k1gMnSc1	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
zásluhy	zásluha	k1gFnSc2	zásluha
obou	dva	k4xCgMnPc2	dva
bratří	bratr	k1gMnPc2	bratr
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
jejich	jejich	k3xOp3gInSc1	jejich
svátek	svátek	k1gInSc1	svátek
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
římskokatolickou	římskokatolický	k2eAgFnSc4d1	Římskokatolická
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Slovanské	slovanský	k2eAgFnPc1d1	Slovanská
pravoslavné	pravoslavný	k2eAgFnPc1d1	pravoslavná
církve	církev	k1gFnPc1	církev
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
den	den	k1gInSc4	den
oslavy	oslava	k1gFnSc2	oslava
svatých	svatý	k1gMnPc2	svatý
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
podle	podle	k7c2	podle
pravoslavného	pravoslavný	k2eAgInSc2d1	pravoslavný
kalendáře	kalendář	k1gInSc2	kalendář
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
dle	dle	k7c2	dle
občanského	občanský	k2eAgInSc2d1	občanský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
ze	z	k7c2	z
Soluně	Soluň	k1gFnSc2	Soluň
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
doloženo	doložit	k5eAaPmNgNnS	doložit
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
863	[number]	k4	863
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
je	být	k5eAaImIp3nS	být
24	[number]	k4	24
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
slaven	slaven	k2eAgInSc4d1	slaven
jako	jako	k9	jako
Den	den	k1gInSc4	den
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc4	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc4	Metoděj
a	a	k8xC	a
Den	den	k1gInSc4	den
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
svátkem	svátek	k1gInSc7	svátek
a	a	k8xC	a
dnem	den	k1gInSc7	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
římský	římský	k2eAgInSc1d1	římský
kalendář	kalendář	k1gInSc1	kalendář
platící	platící	k2eAgInSc1d1	platící
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
klade	klást	k5eAaImIp3nS	klást
zase	zase	k9	zase
památku	památka	k1gFnSc4	památka
na	na	k7c4	na
den	den	k1gInSc4	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
datum	datum	k1gNnSc1	datum
úmrtí	úmrť	k1gFnPc2	úmrť
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc2	Cyril
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2012	[number]	k4	2012
a	a	k8xC	a
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
naučně-dobrodružný	naučněobrodružný	k2eAgInSc1d1	naučně-dobrodružný
film	film	k1gInSc1	film
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
-	-	kIx~	-
Apoštolové	apoštol	k1gMnPc1	apoštol
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
režírovaný	režírovaný	k2eAgInSc4d1	režírovaný
Petrem	Petr	k1gMnSc7	Petr
Nikolaevem	Nikolaev	k1gInSc7	Nikolaev
<g/>
.	.	kIx.	.
</s>
