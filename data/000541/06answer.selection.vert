<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Konstantin	Konstantin	k1gMnSc1	Konstantin
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Κ	Κ	k?	Κ
<g/>
,	,	kIx,	,
Konstantinos	Konstantinos	k1gMnSc1	Konstantinos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Filosof	filosof	k1gMnSc1	filosof
827	[number]	k4	827
v	v	k7c6	v
Soluni	Soluň	k1gFnSc6	Soluň
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
869	[number]	k4	869
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
z	z	k7c2	z
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
filosofie	filosofie	k1gFnSc2	filosofie
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
855	[number]	k4	855
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
