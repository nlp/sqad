<s>
Západoslovanské	západoslovanský	k2eAgInPc1d1	západoslovanský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
skupinou	skupina	k1gFnSc7	skupina
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
především	především	k6eAd1	především
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
<g/>
:	:	kIx,	:
česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
větev	větev	k1gFnSc1	větev
čeština	čeština	k1gFnSc1	čeština
–	–	k?	–
Česko	Česko	k1gNnSc1	Česko
slovenština	slovenština	k1gFnSc1	slovenština
–	–	k?	–
Slovensko	Slovensko	k1gNnSc4	Slovensko
lechická	lechický	k2eAgFnSc1d1	lechický
větev	větev	k1gFnSc1	větev
</s>
<s>
pomořanština	pomořanština	k1gFnSc1	pomořanština
–	–	k?	–
vymřelý	vymřelý	k2eAgInSc4d1	vymřelý
jazyk	jazyk	k1gInSc4	jazyk
</s>
<s>
kašubština	kašubština	k1gFnSc1	kašubština
–	–	k?	–
jazyk	jazyk	k1gInSc4	jazyk
používaný	používaný	k2eAgInSc4d1	používaný
západně	západně	k6eAd1	západně
od	od	k7c2	od
polského	polský	k2eAgNnSc2d1	polské
města	město	k1gNnSc2	město
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
</s>
<s>
severní	severní	k2eAgFnSc1d1	severní
slovinština	slovinština	k1gFnSc1	slovinština
–	–	k?	–
vymřelý	vymřelý	k2eAgInSc4d1	vymřelý
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
dialekt	dialekt	k1gInSc4	dialekt
kašubštiny	kašubština	k1gFnSc2	kašubština
</s>
<s>
polabština	polabština	k1gFnSc1	polabština
–	–	k?	–
vymřelý	vymřelý	k2eAgInSc4d1	vymřelý
liturgický	liturgický	k2eAgInSc4d1	liturgický
jazyk	jazyk	k1gInSc4	jazyk
z	z	k7c2	z
dolního	dolní	k2eAgInSc2d1	dolní
toku	tok	k1gInSc2	tok
Labe	Labe	k1gNnSc2	Labe
</s>
<s>
polština	polština	k1gFnSc1	polština
–	–	k?	–
Polsko	Polsko	k1gNnSc1	Polsko
slezština	slezština	k1gFnSc1	slezština
–	–	k?	–
Slezsko	Slezsko	k1gNnSc1	Slezsko
goralština	goralština	k1gFnSc1	goralština
-	-	kIx~	-
nářečí	nářečí	k1gNnSc1	nářečí
slezštiny	slezština	k1gFnSc2	slezština
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
lužickosrbská	lužickosrbský	k2eAgFnSc1d1	lužickosrbská
větev	větev	k1gFnSc1	větev
dolnolužičtina	dolnolužičtina	k1gFnSc1	dolnolužičtina
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc1d1	dolní
lužická	lužický	k2eAgFnSc1d1	Lužická
srbština	srbština	k1gFnSc1	srbština
<g/>
)	)	kIx)	)
–	–	k?	–
Německo	Německo	k1gNnSc1	Německo
hornolužičtina	hornolužičtina	k1gFnSc1	hornolužičtina
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
lužická	lužický	k2eAgFnSc1d1	Lužická
srbština	srbština	k1gFnSc1	srbština
<g/>
)	)	kIx)	)
–	–	k?	–
Německo	Německo	k1gNnSc1	Německo
</s>
