<s>
Západoslovanské	západoslovanský	k2eAgInPc1d1	západoslovanský
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
skupinou	skupina	k1gFnSc7	skupina
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
především	především	k6eAd1	především
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
