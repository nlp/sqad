<s>
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Gogol	Gogol	k1gMnSc1	Gogol
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Н	Н	k?	Н
В	В	k?	В
Г	Г	k?	Г
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
М	М	k?	М
В	В	k?	В
Г	Г	k?	Г
<g/>
,	,	kIx,	,
Mykola	Mykola	k1gFnSc1	Mykola
Vasyľjovyč	Vasyľjovyč	k1gFnSc1	Vasyľjovyč
Hohoĺ	Hohoĺ	k1gFnSc1	Hohoĺ
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
březnajul	březnajout	k5eAaPmAgInS	březnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1809	[number]	k4	1809
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Soročynci	Soročynec	k1gInSc6	Soročynec
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Velyki	Velyk	k1gMnSc3	Velyk
Soročynci	Soročynec	k1gMnSc3	Soročynec
<g/>
,	,	kIx,	,
Poltavská	poltavský	k2eAgFnSc1d1	Poltavská
<g />
.	.	kIx.	.
</s>
<s>
oblast	oblast	k1gFnSc1	oblast
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
únorajul	únorajout	k5eAaPmAgInS	únorajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1852	[number]	k4	1852
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
ruského	ruský	k2eAgInSc2d1	ruský
romantismu	romantismus	k1gInSc2	romantismus
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
silnými	silný	k2eAgInPc7d1	silný
prvky	prvek	k1gInPc7	prvek
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
za	za	k7c4	za
jehož	jenž	k3xRgMnSc4	jenž
zakladatele	zakladatel	k1gMnSc4	zakladatel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
literatuře	literatura	k1gFnSc6	literatura
považován	považován	k2eAgInSc1d1	považován
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
středních	střední	k2eAgMnPc2d1	střední
ukrajinských	ukrajinský	k2eAgMnPc2d1	ukrajinský
statkářů	statkář	k1gMnPc2	statkář
(	(	kIx(	(
<g/>
Gogolovi	Gogolův	k2eAgMnPc1d1	Gogolův
měli	mít	k5eAaImAgMnP	mít
asi	asi	k9	asi
80	[number]	k4	80
nevolníků	nevolník	k1gMnPc2	nevolník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
vášnivý	vášnivý	k2eAgMnSc1d1	vášnivý
milovník	milovník	k1gMnSc1	milovník
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
verše	verš	k1gInPc4	verš
i	i	k8xC	i
komedie	komedie	k1gFnSc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
malý	malý	k1gMnSc1	malý
Gogol	Gogol	k1gMnSc1	Gogol
hrál	hrát	k5eAaImAgMnS	hrát
–	–	k?	–
zásadně	zásadně	k6eAd1	zásadně
ženské	ženský	k2eAgFnSc2d1	ženská
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
v	v	k7c6	v
přesvědčení	přesvědčení	k1gNnSc6	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
geniální	geniální	k2eAgNnSc4d1	geniální
a	a	k8xC	a
zázračné	zázračný	k2eAgNnSc4d1	zázračné
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
gymnázia	gymnázium	k1gNnSc2	gymnázium
odešel	odejít	k5eAaPmAgMnS	odejít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vydat	vydat	k5eAaPmF	vydat
své	svůj	k3xOyFgFnPc4	svůj
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
uspořádat	uspořádat	k5eAaPmF	uspořádat
výstavu	výstava	k1gFnSc4	výstava
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
kritikou	kritika	k1gFnSc7	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnSc2	léto
1836	[number]	k4	1836
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
strávil	strávit	k5eAaPmAgInS	strávit
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
prožíval	prožívat	k5eAaImAgMnS	prožívat
Gogol	Gogol	k1gMnSc1	Gogol
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
depresi	deprese	k1gFnSc4	deprese
<g/>
,	,	kIx,	,
začínal	začínat	k5eAaImAgMnS	začínat
podléhat	podléhat	k5eAaImF	podléhat
náboženským	náboženský	k2eAgFnPc3d1	náboženská
a	a	k8xC	a
mystickým	mystický	k2eAgFnPc3d1	mystická
náladám	nálada	k1gFnPc3	nálada
a	a	k8xC	a
pochybnostem	pochybnost	k1gFnPc3	pochybnost
o	o	k7c6	o
smyslu	smysl	k1gInSc6	smysl
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zešílel	zešílet	k5eAaPmAgMnS	zešílet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Gogolovu	Gogolův	k2eAgFnSc4d1	Gogolova
tvorbu	tvorba	k1gFnSc4	tvorba
navazuje	navazovat	k5eAaImIp3nS	navazovat
prakticky	prakticky	k6eAd1	prakticky
celá	celý	k2eAgFnSc1d1	celá
ruská	ruský	k2eAgFnSc1d1	ruská
literatura	literatura	k1gFnSc1	literatura
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgFnPc1d1	považována
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
plně	plně	k6eAd1	plně
využil	využít	k5eAaPmAgMnS	využít
své	svůj	k3xOyFgNnSc4	svůj
vypravěčské	vypravěčský	k2eAgNnSc4d1	vypravěčské
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
spojovat	spojovat	k5eAaImF	spojovat
reálné	reálný	k2eAgNnSc4d1	reálné
s	s	k7c7	s
fantastickým	fantastický	k2eAgInSc7d1	fantastický
<g/>
.	.	kIx.	.
</s>
<s>
Hans	Hans	k1gMnSc1	Hans
Küchelgarten	Küchelgarten	k2eAgMnSc1d1	Küchelgarten
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
,	,	kIx,	,
Г	Г	k?	Г
К	К	k?	К
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poema	poema	k1gFnSc1	poema
<g/>
,	,	kIx,	,
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
sentimentální	sentimentální	k2eAgFnSc1d1	sentimentální
idylka	idylka	k1gFnSc1	idylka
napsaná	napsaný	k2eAgFnSc1d1	napsaná
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
V.	V.	kA	V.
Alov	Alov	k1gInSc1	Alov
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
neprodaný	prodaný	k2eNgInSc4d1	neprodaný
náklad	náklad	k1gInSc4	náklad
Gogol	Gogol	k1gMnSc1	Gogol
sám	sám	k3xTgMnSc1	sám
skoupil	skoupit	k5eAaPmAgMnS	skoupit
a	a	k8xC	a
zničil	zničit	k5eAaPmAgMnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Večery	večer	k1gInPc1	večer
na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
Dikaňky	Dikaňka	k1gFnSc2	Dikaňka
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
–	–	k?	–
<g/>
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
В	В	k?	В
н	н	k?	н
х	х	k?	х
б	б	k?	б
Д	Д	k?	Д
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
cyklus	cyklus	k1gInSc1	cyklus
humorně	humorně	k6eAd1	humorně
i	i	k9	i
strašidelně	strašidelně	k6eAd1	strašidelně
laděných	laděný	k2eAgFnPc2d1	laděná
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
pohádek	pohádka	k1gFnPc2	pohádka
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
slov	slovo	k1gNnPc2	slovo
o	o	k7c6	o
Puškinovi	Puškin	k1gMnSc6	Puškin
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
Н	Н	k?	Н
с	с	k?	с
о	о	k?	о
П	П	k?	П
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
literárně-kritická	literárněritický	k2eAgFnSc1d1	literárně-kritická
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
Arabesky	arabeska	k1gFnPc1	arabeska
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
А	А	k?	А
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc4	sborník
statí	stať	k1gFnPc2	stať
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tři	tři	k4xCgInPc4	tři
tzv.	tzv.	kA	tzv.
petrohradské	petrohradský	k2eAgFnSc2d1	Petrohradská
povídky	povídka	k1gFnSc2	povídka
Něvská	něvský	k2eAgFnSc1d1	Něvská
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
Н	Н	k?	Н
п	п	k?	п
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bláznovy	bláznův	k2eAgInPc1d1	bláznův
zápisky	zápisek	k1gInPc1	zápisek
(	(	kIx(	(
<g/>
З	З	k?	З
с	с	k?	с
<g/>
)	)	kIx)	)
a	a	k8xC	a
Podobizna	podobizna	k1gFnSc1	podobizna
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mirgorod	Mirgorod	k1gInSc1	Mirgorod
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
vlastně	vlastně	k9	vlastně
jakési	jakýsi	k3yIgNnSc4	jakýsi
pokračování	pokračování	k1gNnSc4	pokračování
Večerů	večer	k1gInPc2	večer
na	na	k7c6	na
samotě	samota	k1gFnSc6	samota
u	u	k7c2	u
Dikanky	Dikanka	k1gFnSc2	Dikanka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
Taras	taras	k1gInSc4	taras
Bulba	Bulb	k1gMnSc2	Bulb
<g/>
.	.	kIx.	.
</s>
<s>
Vij	vít	k5eAaImRp2nS	vít
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
,	,	kIx,	,
В	В	k?	В
<g/>
)	)	kIx)	)
horrorová	horrorový	k2eAgFnSc1d1	horrorová
povídka	povídka	k1gFnSc1	povídka
o	o	k7c6	o
setkání	setkání	k1gNnSc6	setkání
seminaristy	seminarista	k1gMnSc2	seminarista
Chomy	Choma	k1gFnSc2	Choma
Bruta	Brut	k1gMnSc2	Brut
s	s	k7c7	s
pekelnými	pekelný	k2eAgFnPc7d1	pekelná
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Nos	nos	k1gInSc1	nos
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Н	Н	k?	Н
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
zařazovaná	zařazovaný	k2eAgFnSc1d1	zařazovaná
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
petrohradských	petrohradský	k2eAgFnPc2d1	Petrohradská
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
Kočár	kočár	k1gInSc1	kočár
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
К	К	k?	К
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sice	sice	k8xC	sice
k	k	k7c3	k
petrohradským	petrohradský	k2eAgFnPc3d1	Petrohradská
povídkám	povídka	k1gFnPc3	povídka
nepatří	patřit	k5eNaImIp3nS	patřit
(	(	kIx(	(
<g/>
její	její	k3xOp3gInSc1	její
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
provinciálním	provinciální	k2eAgNnSc6d1	provinciální
městečku	městečko	k1gNnSc6	městečko
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
na	na	k7c6	na
usedlosti	usedlost	k1gFnSc6	usedlost
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svými	svůj	k3xOyFgInPc7	svůj
základními	základní	k2eAgInPc7d1	základní
rysy	rys	k1gInPc7	rys
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
blíží	blížit	k5eAaImIp3nS	blížit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
bývá	bývat	k5eAaImIp3nS	bývat
přiřazována	přiřazován	k2eAgFnSc1d1	přiřazován
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
sv.	sv.	kA	sv.
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
třetího	třetí	k4xOgInSc2	třetí
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
-	-	kIx~	-
<g/>
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
В	В	k?	В
т	т	k?	т
с	с	k?	с
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
Gogolův	Gogolův	k2eAgInSc4d1	Gogolův
dramatický	dramatický	k2eAgInSc4d1	dramatický
pokus	pokus	k1gInSc4	pokus
<g/>
,	,	kIx,	,
nedokončená	dokončený	k2eNgFnSc1d1	nedokončená
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
Gogol	Gogol	k1gMnSc1	Gogol
publikoval	publikovat	k5eAaBmAgMnS	publikovat
úryvky	úryvek	k1gInPc4	úryvek
a	a	k8xC	a
také	také	k9	také
samostatné	samostatný	k2eAgFnPc4d1	samostatná
scény	scéna	k1gFnPc4	scéna
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jednoaktovek	jednoaktovka	k1gFnPc2	jednoaktovka
<g/>
:	:	kIx,	:
Úředníkovo	úředníkův	k2eAgNnSc1d1	úředníkovo
ráno	ráno	k1gNnSc1	ráno
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
У	У	k?	У
д	д	k?	д
ч	ч	k?	ч
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spor	spor	k1gInSc1	spor
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Т	Т	k?	Т
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Podlézavá	podlézavý	k2eAgFnSc1d1	podlézavá
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
-	-	kIx~	-
<g/>
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Scény	scéna	k1gFnPc4	scéna
ze	z	k7c2	z
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
С	С	k?	С
и	и	k?	и
с	с	k?	с
ж	ж	k?	ж
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Revizor	revizor	k1gMnSc1	revizor
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
Р	Р	k?	Р
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pětiaktová	pětiaktový	k2eAgFnSc1d1	pětiaktová
satirická	satirický	k2eAgFnSc1d1	satirická
komedie	komedie	k1gFnSc1	komedie
patřící	patřící	k2eAgFnSc2d1	patřící
k	k	k7c3	k
nejslavnějším	slavný	k2eAgInPc3d3	nejslavnější
dílům	díl	k1gInPc3	díl
světové	světový	k2eAgFnSc2d1	světová
dramatické	dramatický	k2eAgFnSc2d1	dramatická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
И	И	k?	И
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
hře	hra	k1gFnSc6	hra
<g/>
,	,	kIx,	,
o	o	k7c6	o
vášni	vášeň	k1gFnSc6	vášeň
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
<g/>
,	,	kIx,	,
o	o	k7c6	o
podvodu	podvod	k1gInSc2	podvod
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
podvodu	podvod	k1gInSc2	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
atraktivním	atraktivní	k2eAgNnSc6d1	atraktivní
prostředí	prostředí	k1gNnSc6	prostředí
falešných	falešný	k2eAgMnPc2d1	falešný
karetních	karetní	k2eAgMnPc2d1	karetní
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
filozofie	filozofie	k1gFnSc1	filozofie
je	být	k5eAaImIp3nS	být
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
bystrý	bystrý	k2eAgInSc4d1	bystrý
vtip	vtip	k1gInSc4	vtip
<g/>
,	,	kIx,	,
jasný	jasný	k2eAgInSc4d1	jasný
rozum	rozum	k1gInSc4	rozum
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
žádné	žádný	k3yNgInPc4	žádný
ohledy	ohled	k1gInPc4	ohled
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
duše	duše	k1gFnPc1	duše
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
-	-	kIx~	-
<g/>
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
М	М	k?	М
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
román	román	k1gInSc1	román
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
poema	poema	k1gNnSc1	poema
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
carskou	carský	k2eAgFnSc4d1	carská
byrokracii	byrokracie	k1gFnSc4	byrokracie
<g/>
.	.	kIx.	.
</s>
<s>
Gogol	Gogol	k1gMnSc1	Gogol
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
několikrát	několikrát	k6eAd1	několikrát
přepracovával	přepracovávat	k5eAaImAgInS	přepracovávat
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
spálil	spálit	k5eAaPmAgMnS	spálit
část	část	k1gFnSc1	část
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
poslední	poslední	k2eAgFnSc4d1	poslední
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Ženitba	ženitba	k1gFnSc1	ženitba
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
Ж	Ж	k?	Ж
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
satirická	satirický	k2eAgFnSc1d1	satirická
komedie	komedie	k1gFnSc1	komedie
o	o	k7c6	o
dvou	dva	k4xCgNnPc6	dva
dějstvích	dějství	k1gNnPc6	dějství
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
starého	starý	k2eAgMnSc2d1	starý
mládence	mládenec	k1gMnSc2	mládenec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
pohrává	pohrávat	k5eAaImIp3nS	pohrávat
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
na	na	k7c4	na
manželství	manželství	k1gNnSc4	manželství
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
skoro	skoro	k6eAd1	skoro
žení	ženit	k5eAaImIp3nP	ženit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
včas	včas	k6eAd1	včas
uteče	utéct	k5eAaPmIp3nS	utéct
oknem	okno	k1gNnSc7	okno
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
stárnoucí	stárnoucí	k2eAgFnSc1d1	stárnoucí
nevěsta	nevěsta	k1gFnSc1	nevěsta
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
nepřijít	přijít	k5eNaPmF	přijít
o	o	k7c4	o
žádného	žádný	k3yNgMnSc4	žádný
z	z	k7c2	z
pěti	pět	k4xCc2	pět
nápadníků	nápadník	k1gMnPc2	nápadník
tak	tak	k6eAd1	tak
nakonec	nakonec	k6eAd1	nakonec
přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
komedie	komedie	k1gFnSc2	komedie
napsal	napsat	k5eAaBmAgMnS	napsat
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
český	český	k2eAgMnSc1d1	český
skladatel	skladatel	k1gMnSc1	skladatel
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Martinů	Martinů	k2eAgFnSc4d1	Martinů
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Petrohradské	petrohradský	k2eAgFnPc1d1	Petrohradská
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
П	П	k?	П
п	п	k?	п
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
vydáních	vydání	k1gNnPc6	vydání
označován	označovat	k5eAaImNgInS	označovat
cyklus	cyklus	k1gInSc1	cyklus
povídek	povídka	k1gFnPc2	povídka
vydaných	vydaný	k2eAgFnPc2d1	vydaná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čtyřsvazkových	čtyřsvazkový	k2eAgInPc2d1	čtyřsvazkový
Gogolových	Gogolových	k2eAgInPc2d1	Gogolových
sebraných	sebraný	k2eAgInPc2d1	sebraný
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
přepracované	přepracovaný	k2eAgFnSc2d1	přepracovaná
povídky	povídka	k1gFnSc2	povídka
Něvská	něvský	k2eAgFnSc1d1	Něvská
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
Н	Н	k?	Н
п	п	k?	п
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bláznovy	bláznův	k2eAgInPc1d1	bláznův
zápisky	zápisek	k1gInPc1	zápisek
(	(	kIx(	(
<g/>
З	З	k?	З
с	с	k?	с
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Podobizna	podobizna	k1gFnSc1	podobizna
(	(	kIx(	(
<g/>
П	П	k?	П
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nos	nos	k1gInSc4	nos
(	(	kIx(	(
Н	Н	k?	Н
<g/>
)	)	kIx)	)
a	a	k8xC	a
definitivní	definitivní	k2eAgFnSc4d1	definitivní
verzi	verze	k1gFnSc4	verze
nové	nový	k2eAgFnSc2d1	nová
povídky	povídka	k1gFnSc2	povídka
Plášť	plášť	k1gInSc1	plášť
(	(	kIx(	(
<g/>
Ш	Ш	k?	Ш
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
Gogol	Gogol	k1gMnSc1	Gogol
pracoval	pracovat	k5eAaImAgMnS	pracovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
Р	Р	k?	Р
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fragment	fragment	k1gInSc1	fragment
nedokončeného	dokončený	k2eNgInSc2d1	nedokončený
románu	román	k1gInSc2	román
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čtyřsvazkových	čtyřsvazkový	k2eAgInPc2d1	čtyřsvazkový
Gogolových	Gogolových	k2eAgInPc2d1	Gogolových
sebraných	sebraný	k2eAgInPc2d1	sebraný
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
Na	na	k7c6	na
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
1842	[number]	k4	1842
<g/>
,	,	kIx,	,
Т	Т	k?	Т
р	р	k?	р
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoaktovka	jednoaktovka	k1gFnSc1	jednoaktovka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
Gogol	Gogol	k1gMnSc1	Gogol
líčí	líčit	k5eAaImIp3nS	líčit
dojmy	dojem	k1gInPc4	dojem
po	po	k7c4	po
představení	představení	k1gNnSc4	představení
Revizora	revizor	k1gMnSc2	revizor
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
společenské	společenský	k2eAgFnPc4d1	společenská
vrstvy	vrstva	k1gFnPc4	vrstva
a	a	k8xC	a
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jeho	jeho	k3xOp3gFnPc4	jeho
myšlenky	myšlenka	k1gFnPc4	myšlenka
o	o	k7c6	o
velkém	velký	k2eAgInSc6d1	velký
významu	význam	k1gInSc6	význam
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
umělecké	umělecký	k2eAgFnSc2d1	umělecká
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Vybraná	vybraný	k2eAgNnPc1d1	vybrané
místa	místo	k1gNnPc1	místo
z	z	k7c2	z
korespondence	korespondence	k1gFnSc2	korespondence
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
В	В	k?	В
м	м	k?	м
и	и	k?	и
п	п	k?	п
с	с	k?	с
д	д	k?	д
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
publicistická	publicistický	k2eAgFnSc1d1	publicistická
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
letech	léto	k1gNnPc6	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
Gogol	Gogol	k1gMnSc1	Gogol
výrazně	výrazně	k6eAd1	výrazně
rozcházel	rozcházet	k5eAaImAgMnS	rozcházet
s	s	k7c7	s
pokrokovými	pokrokový	k2eAgInPc7d1	pokrokový
proudy	proud	k1gInPc7	proud
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Rozjímání	rozjímání	k1gNnSc1	rozjímání
o	o	k7c6	o
božské	božský	k2eAgFnSc6d1	božská
liturgii	liturgie	k1gFnSc6	liturgie
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
,	,	kIx,	,
Р	Р	k?	Р
о	о	k?	о
Б	Б	k?	Б
Л	Л	k?	Л
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Gogol	Gogol	k1gMnSc1	Gogol
svou	svůj	k3xOyFgFnSc7	svůj
osobitou	osobitý	k2eAgFnSc7d1	osobitá
formou	forma	k1gFnSc7	forma
zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
mystérium	mystérium	k1gNnSc1	mystérium
východní	východní	k2eAgFnSc2d1	východní
liturgie	liturgie	k1gFnSc2	liturgie
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k8xS	jako
Rozjímání	rozjímání	k1gNnSc1	rozjímání
o	o	k7c6	o
mši	mše	k1gFnSc6	mše
svaté	svatý	k2eAgFnSc6d1	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Vij	vít	k5eAaImRp2nS	vít
Volně	volně	k6eAd1	volně
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Jana	Jan	k1gMnSc2	Jan
Reye	Rey	k1gMnSc2	Rey
napsal	napsat	k5eAaBmAgMnS	napsat
Ivan	Ivan	k1gMnSc1	Ivan
Hejna	Hejna	k1gMnSc1	Hejna
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
Petr	Petr	k1gMnSc1	Petr
Mandel	mandel	k1gInSc1	mandel
<g/>
,	,	kIx,	,
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
Eva	Eva	k1gFnSc1	Eva
Košlerová	Košlerová	k1gFnSc1	Košlerová
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Weinlich	Weinlich	k1gMnSc1	Weinlich
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Ivan	Ivan	k1gMnSc1	Ivan
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Gübel	Gübel	k1gMnSc1	Gübel
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Doulová-Krobotová	Doulová-Krobotový	k2eAgFnSc1d1	Doulová-Krobotový
<g/>
,	,	kIx,	,
Yvetta	Yvetta	k1gFnSc1	Yvetta
Blanarovičová	Blanarovičová	k1gFnSc1	Blanarovičová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pelzer	Pelzer	k1gMnSc1	Pelzer
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kepka	Kepka	k1gMnSc1	Kepka
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Hardt	Hardt	k1gMnSc1	Hardt
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kasalá	Kasalý	k2eAgFnSc1d1	Kasalý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Weinlich	Weinlich	k1gMnSc1	Weinlich
a	a	k8xC	a
Lenka	Lenka	k1gFnSc1	Lenka
Vintrová	Vintrová	k1gFnSc1	Vintrová
<g/>
.	.	kIx.	.
</s>
<s>
Natočeno	natočen	k2eAgNnSc1d1	natočeno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
</s>
