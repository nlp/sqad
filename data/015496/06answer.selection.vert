<s>
Korán	korán	k1gInSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
ا	ا	k?
'	'	kIx"
<g/>
al-Qurʾ	al-Qurʾ	k1gMnSc1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
recitace	recitace	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc1d1
náboženský	náboženský	k2eAgInSc1d1
text	text	k1gInSc1
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
recitací	recitace	k1gFnPc2
Muhammada	Muhammada	k1gFnSc1
(	(	kIx(
<g/>
proroka	prorok	k1gMnSc2
islámu	islám	k1gInSc2
<g/>
)	)	kIx)
během	během	k7c2
posledních	poslední	k2eAgNnPc2d1
23	#num#	k4
let	léto	k1gNnPc2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>