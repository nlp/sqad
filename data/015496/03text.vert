<s>
Korán	korán	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Korán	korán	k1gInSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
ا	ا	k?
'	'	kIx"
<g/>
al-Qurʾ	al-Qurʾ	k1gMnSc1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
recitace	recitace	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgInSc1d1
náboženský	náboženský	k2eAgInSc1d1
text	text	k1gInSc1
islámu	islám	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
recitací	recitace	k1gFnPc2
Muhammada	Muhammada	k1gFnSc1
(	(	kIx(
<g/>
proroka	prorok	k1gMnSc2
islámu	islám	k1gInSc2
<g/>
)	)	kIx)
během	během	k7c2
posledních	poslední	k2eAgNnPc2d1
23	#num#	k4
let	léto	k1gNnPc2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslimové	muslim	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
verše	verš	k1gInPc1
koránu	korán	k1gInSc2
mu	on	k3xPp3gMnSc3
diktoval	diktovat	k5eAaImAgMnS
anděl	anděl	k1gMnSc1
Gabriel	Gabriel	k1gMnSc1
jako	jako	k8xS,k8xC
zprávy	zpráva	k1gFnPc1
od	od	k7c2
jediného	jediný	k2eAgMnSc2d1
Boha	bůh	k1gMnSc2
(	(	kIx(
<g/>
Aláha	Aláha	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
114	#num#	k4
súr	súra	k1gFnPc2
(	(	kIx(
<g/>
kapitol	kapitola	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nejsou	být	k5eNaImIp3nP
</s>
<s>
chronologicky	chronologicky	k6eAd1
uspořádány	uspořádat	k5eAaPmNgInP
dle	dle	k7c2
času	čas	k1gInSc2
zjevení	zjevení	k1gNnSc1
(	(	kIx(
<g/>
historicky	historicky	k6eAd1
první	první	k4xOgNnSc1
Muhammadovo	Muhammadův	k2eAgNnSc1d1
zjevení	zjevení	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
až	až	k9
v	v	k7c6
96	#num#	k4
<g/>
.	.	kIx.
súře	súra	k1gFnSc6
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
súra	súra	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
určitého	určitý	k2eAgInSc2d1
počtu	počet	k1gInSc2
veršů	verš	k1gInPc2
(	(	kIx(
<g/>
ayat	ayat	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInPc1d3
rukopisné	rukopisný	k2eAgInPc1d1
exempláře	exemplář	k1gInPc1
jsou	být	k5eAaImIp3nP
uchovávány	uchovávat	k5eAaImNgInP
ve	v	k7c6
dvou	dva	k4xCgInPc6
přepisech	přepis	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
Taškentu	Taškent	k1gInSc6
a	a	k8xC
v	v	k7c6
muzeu	muzeum	k1gNnSc6
Topkapi	Topkap	k1gFnSc2
v	v	k7c6
Istanbulu	Istanbul	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Korán	korán	k1gInSc1
a	a	k8xC
sunna	sunna	k1gFnSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
cesta	cesta	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
hadísů	hadís	k1gInPc2
a	a	k8xC
síry	síra	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
základní	základní	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
islámského	islámský	k2eAgNnSc2d1
vyznání	vyznání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc4
jakožto	jakožto	k8xS
„	„	k?
<g/>
svatá	svatat	k5eAaImIp3nS
kniha	kniha	k1gFnSc1
islámu	islám	k1gInSc2
<g/>
“	“	k?
má	mít	k5eAaImIp3nS
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
podobný	podobný	k2eAgInSc1d1
význam	význam	k1gInSc1
jako	jako	k8xC,k8xS
Bible	bible	k1gFnSc1
pro	pro	k7c4
křesťanství	křesťanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
se	se	k3xPyFc4
však	však	k9
od	od	k7c2
Bible	bible	k1gFnSc2
velmi	velmi	k6eAd1
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verše	verš	k1gInSc2
v	v	k7c6
koránu	korán	k1gInSc6
často	často	k6eAd1
reagují	reagovat	k5eAaBmIp3nP
na	na	k7c4
události	událost	k1gFnPc4
v	v	k7c6
Muhammadově	Muhammadův	k2eAgInSc6d1
životě	život	k1gInSc6
a	a	k8xC
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gInPc6
pouze	pouze	k6eAd1
zjevení	zjevení	k1gNnSc1
(	(	kIx(
<g/>
řeč	řeč	k1gFnSc1
Aláha	Aláha	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bez	bez	k7c2
popisu	popis	k1gInSc2
historického	historický	k2eAgInSc2d1
kontextu	kontext	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc4d1
kontext	kontext	k1gInSc4
najdeme	najít	k5eAaPmIp1nP
v	v	k7c6
hadísech	hadís	k1gInPc6
a	a	k8xC
síře	síra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
koránu	korán	k1gInSc6
se	se	k3xPyFc4
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Muhammad	Muhammad	k1gInSc1
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
krásný	krásný	k2eAgInSc1d1
příklad	příklad	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
oswatun	oswatun	k1gMnSc1
hasanatun	hasanatun	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
by	by	kYmCp3nP
muslimové	muslim	k1gMnPc1
měli	mít	k5eAaImAgMnP
napodobovat	napodobovat	k5eAaImF
,	,	kIx,
takže	takže	k8xS
nejen	nejen	k6eAd1
korán	korán	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
hadísy	hadísa	k1gFnSc2
(	(	kIx(
<g/>
vyprávění	vyprávění	k1gNnSc1
o	o	k7c6
tom	ten	k3xDgNnSc6
co	co	k9
Muhammad	Muhammad	k1gInSc4
řekl	říct	k5eAaPmAgMnS
nebo	nebo	k8xC
udělal	udělat	k5eAaPmAgInS
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
muslimy	muslim	k1gMnPc4
závazné	závazný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Podrobným	podrobný	k2eAgInSc7d1
výkladem	výklad	k1gInSc7
veršů	verš	k1gInPc2
koránu	korán	k1gInSc2
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
tzv.	tzv.	kA
tafsíry	tafsír	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
vysvětlují	vysvětlovat	k5eAaImIp3nP
významy	význam	k1gInPc4
použitých	použitý	k2eAgFnPc2d1
arabských	arabský	k2eAgFnPc2d1
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
význam	význam	k1gInSc4
veršů	verš	k1gInPc2
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
historický	historický	k2eAgInSc4d1
kontext	kontext	k1gInSc4
jejich	jejich	k3xOp3gNnSc2
zjevení	zjevení	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
vztah	vztah	k1gInSc4
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
verši	verš	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
koránu	korán	k1gInSc6
je	být	k5eAaImIp3nS
zmíněno	zmínit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
některé	některý	k3yIgInPc1
verše	verš	k1gInPc1
jsou	být	k5eAaImIp3nP
rušeny	rušit	k5eAaImNgInP
verši	verš	k1gInPc7
novými	nový	k2eAgInPc7d1
(	(	kIx(
<g/>
princip	princip	k1gInSc1
abrogace	abrogace	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
právě	právě	k9
tafsíry	tafsír	k1gInPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
verše	verš	k1gInPc1
byly	být	k5eAaImAgInP
zrušeny	zrušit	k5eAaPmNgInP
a	a	k8xC
čím	čí	k3xOyRgFnPc3,k3xOyQgFnPc3
byly	být	k5eAaImAgFnP
nahrazeny	nahradit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tafsíry	Tafsír	k1gInPc7
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
hadísů	hadís	k1gInPc2
a	a	k8xC
síry	síra	k1gFnSc2
-	-	kIx~
neobsahují	obsahovat	k5eNaImIp3nP
subjektivní	subjektivní	k2eAgInPc4d1
názory	názor	k1gInPc4
na	na	k7c4
výklad	výklad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Korán	korán	k1gInSc1
Mašhadu	Mašhad	k1gInSc2
v	v	k7c6
Íránu	Írán	k1gInSc6
<g/>
,	,	kIx,
napsaný	napsaný	k2eAgInSc1d1
Alím	Alím	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Vědci	vědec	k1gMnPc1
islámské	islámský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
studovali	studovat	k5eAaImAgMnP
vývoj	vývoj	k1gInSc4
Qibla	Qibl	k1gMnSc4
v	v	k7c6
čase	čas	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rodilo	rodit	k5eAaImAgNnS
islám	islám	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patricia	Patricius	k1gMnSc2
Crone	Cron	k1gInSc5
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Cook	Cook	k1gMnSc1
a	a	k8xC
řada	řada	k1gFnSc1
dalších	další	k2eAgMnPc2d1
badatelů	badatel	k1gMnPc2
na	na	k7c6
základě	základ	k1gInSc6
textu	text	k1gInSc2
a	a	k8xC
archeologických	archeologický	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Masjid	Masjid	k1gInSc1
al-Haram	al-Haram	k1gInSc1
<g/>
“	“	k?
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
na	na	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
Arabském	arabský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ne	ne	k9
v	v	k7c6
Mekce	Mekka	k1gFnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
vyjádřeno	vyjádřit	k5eAaPmNgNnS
v	v	k7c6
klasických	klasický	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
založených	založený	k2eAgFnPc6d1
na	na	k7c4
narativní	narativní	k2eAgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
kultuře	kultura	k1gFnSc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Dan	Dan	k1gMnSc1
Gibson	Gibson	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgInPc1
aspekty	aspekt	k1gInPc1
islámského	islámský	k2eAgInSc2d1
masjidu	masjid	k1gInSc2
a	a	k8xC
hřbitova	hřbitov	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
islámu	islám	k1gInSc6
zvané	zvaný	k2eAgNnSc1d1
qibla	qibla	k6eAd1
<g/>
,	,	kIx,
označují	označovat	k5eAaImIp3nP
Petra	Petr	k1gMnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
zde	zde	k6eAd1
založen	založit	k5eAaPmNgInS
islám	islám	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
koránu	korán	k1gInSc2
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
korán	korán	k1gInSc1
vznikal	vznikat	k5eAaImAgInS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
na	na	k7c6
Arabském	arabský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
už	už	k6eAd1
hodně	hodně	k6eAd1
židů	žid	k1gMnPc2
i	i	k8xC
křesťanů	křesťan	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c6
venkově	venkov	k1gInSc6
ale	ale	k9
lidé	člověk	k1gMnPc1
dále	daleko	k6eAd2
praktikovali	praktikovat	k5eAaImAgMnP
mnohobožství	mnohobožství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
6	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
7	#num#	k4
<g/>
,	,	kIx,
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
monoteismus	monoteismus	k1gInSc1
patrně	patrně	k6eAd1
na	na	k7c6
postupu	postup	k1gInSc6
už	už	k6eAd1
před	před	k7c7
Mohamedem	Mohamed	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
i	i	k9
několik	několik	k4yIc1
jeho	jeho	k3xOp3gMnPc2
proroků	prorok	k1gMnPc2
<g/>
,	,	kIx,
Mohamedových	Mohamedův	k2eAgMnPc2d1
současníků	současník	k1gMnPc2
a	a	k8xC
konkurentů	konkurent	k1gMnPc2
<g/>
.	.	kIx.
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
ale	ale	k9
Mohamed	Mohamed	k1gMnSc1
i	i	k8xC
jeho	jeho	k3xOp3gMnSc1
první	první	k4xOgMnSc1
nástupce	nástupce	k1gMnSc1
Abu	Abu	k1gMnSc1
Bakr	Bakr	k1gMnSc1
tvrdě	tvrdě	k6eAd1
potlačili	potlačit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Mohamedově	Mohamedův	k2eAgInSc6d1
mimořádném	mimořádný	k2eAgInSc6d1
úspěchu	úspěch	k1gInSc6
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
podílet	podílet	k5eAaImF
i	i	k9
dobové	dobový	k2eAgInPc4d1
politické	politický	k2eAgInPc4d1
střety	střet	k1gInPc4
<g/>
,	,	kIx,
například	například	k6eAd1
soupeření	soupeření	k1gNnSc1
mezi	mezi	k7c7
perskými	perský	k2eAgMnPc7d1
Sasánovci	Sasánovec	k1gMnPc7
a	a	k8xC
byzantským	byzantský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
právě	právě	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
622	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
válečné	válečný	k2eAgNnSc4d1
tažení	tažení	k1gNnSc4
proti	proti	k7c3
Persii	Persie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohamedovo	Mohamedův	k2eAgNnSc4d1
pozvání	pozvání	k1gNnSc4
do	do	k7c2
Mediny	Medina	k1gFnSc2
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
mohlo	moct	k5eAaImAgNnS
přímo	přímo	k6eAd1
souviset	souviset	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Muslimové	muslim	k1gMnPc1
věří	věřit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
korán	korán	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
doslovně	doslovně	k6eAd1
zaznamenané	zaznamenaný	k2eAgNnSc1d1
zjevení	zjevení	k1gNnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
se	se	k3xPyFc4
Muhammadovi	Muhammada	k1gMnSc3
dostalo	dostat	k5eAaPmAgNnS
prostřednictvím	prostřednictvím	k7c2
anděla	anděl	k1gMnSc2
Gabriela	Gabriel	k1gMnSc2
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
Džibríl	Džibríl	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Muhammad	Muhammad	k1gInSc4
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
korán	korán	k1gInSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
bible	bible	k1gFnSc1
a	a	k8xC
tóra	tóra	k1gFnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
obrazem	obraz	k1gInSc7
dokonalého	dokonalý	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
Božího	boží	k2eAgInSc2d1
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
Matka	matka	k1gFnSc1
knih	kniha	k1gFnPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
umm-al-kitáb	umm-al-kitáb	k1gMnSc1
<g/>
)	)	kIx)
existující	existující	k2eAgMnSc1d1
věčně	věčně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
kritikové	kritik	k1gMnPc1
později	pozdě	k6eAd2
poukazovali	poukazovat	k5eAaImAgMnP
na	na	k7c4
rozpory	rozpor	k1gInPc4
mezi	mezi	k7c7
koránem	korán	k1gInSc7
a	a	k8xC
staršími	starý	k2eAgInPc7d2
texty	text	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
vysvětlení	vysvětlení	k1gNnSc3
v	v	k7c6
koránu	korán	k1gInSc6
o	o	k7c4
pokřivení	pokřivení	k1gNnSc4
původně	původně	k6eAd1
významově	významově	k6eAd1
shodných	shodný	k2eAgFnPc2d1
knih	kniha	k1gFnPc2
křesťanů	křesťan	k1gMnPc2
a	a	k8xC
židů	žid	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc1
je	být	k5eAaImIp3nS
označen	označit	k5eAaPmNgInS
za	za	k7c2
tzv.	tzv.	kA
„	„	k?
<g/>
Pečeť	pečeť	k1gFnSc1
proroků	prorok	k1gMnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
sled	sled	k1gInSc4
řady	řada	k1gFnSc2
předchozích	předchozí	k2eAgMnPc2d1
proroků	prorok	k1gMnPc2
<g/>
,	,	kIx,
Abraháma	Abrahám	k1gMnSc2
<g/>
,	,	kIx,
Mojžíše	Mojžíš	k1gMnSc2
<g/>
,	,	kIx,
Ježíše	Ježíš	k1gMnSc2
a	a	k8xC
mnoha	mnoho	k4c2
dalších	další	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Sesílání	sesílání	k1gNnSc1
veršů	verš	k1gInPc2
trvalo	trvat	k5eAaImAgNnS
dohromady	dohromady	k6eAd1
23	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
609	#num#	k4
<g/>
–	–	k?
<g/>
632	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
podoby	podoba	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
jaké	jaký	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
korán	korán	k1gInSc4
známe	znát	k5eAaImIp1nP
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
sestaven	sestaven	k2eAgInSc1d1
roku	rok	k1gInSc2
651	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
19	#num#	k4
let	léto	k1gNnPc2
po	po	k7c6
Muhammadově	Muhammadův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
byl	být	k5eAaImAgInS
nejprve	nejprve	k6eAd1
uchováván	uchovávat	k5eAaImNgInS
v	v	k7c6
paměti	paměť	k1gFnSc6
Muhammada	Muhammada	k1gFnSc1
a	a	k8xC
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnPc1
následovníci	následovník	k1gMnPc1
islámu	islám	k1gInSc2
znali	znát	k5eAaImAgMnP
kompletně	kompletně	k6eAd1
celý	celý	k2eAgInSc4d1
korán	korán	k1gInSc4
nazpaměť	nazpaměť	k6eAd1
a	a	k8xC
neexistovala	existovat	k5eNaImAgFnS
psaná	psaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
se	se	k3xPyFc4
opakovalo	opakovat	k5eAaImAgNnS
a	a	k8xC
připomínalo	připomínat	k5eAaImAgNnS
neustálým	neustálý	k2eAgNnSc7d1
přednášením	přednášení	k1gNnSc7
mezi	mezi	k7c7
věřícími	věřící	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
však	však	k9
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
ještě	ještě	k9
před	před	k7c7
odchodem	odchod	k1gInSc7
z	z	k7c2
Mekky	Mekka	k1gFnSc2
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
jeho	jeho	k3xOp3gNnPc1
slova	slovo	k1gNnPc1
zapisována	zapisován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
medínského	medínský	k2eAgNnSc2d1
období	období	k1gNnSc2
tradice	tradice	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Muhammad	Muhammad	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
psát	psát	k5eAaImF
neuměl	umět	k5eNaImAgMnS
<g/>
,	,	kIx,
užíval	užívat	k5eAaImAgMnS
některé	některý	k3yIgInPc4
ze	z	k7c2
svých	svůj	k3xOyFgMnPc2
přívrženců	přívrženec	k1gMnPc2
jako	jako	k8xC,k8xS
písaře	písař	k1gMnSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
řada	řada	k1gFnSc1
lidí	člověk	k1gMnPc2
uměla	umět	k5eAaImAgFnS
části	část	k1gFnPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
dokonce	dokonce	k9
celé	celý	k2eAgNnSc1d1
zjevení	zjevení	k1gNnSc1
nazpaměť	nazpaměť	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
potřeba	potřeba	k1gFnSc1
stanovit	stanovit	k5eAaPmF
jednoznačnou	jednoznačný	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zjevení	zjevení	k1gNnSc1
uchráněno	uchránit	k5eAaPmNgNnS
deformací	deformace	k1gFnSc7
a	a	k8xC
odlišných	odlišný	k2eAgFnPc2d1
interpretací	interpretace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgFnSc1d1
arabské	arabský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
například	například	k6eAd1
nezaznamenávalo	zaznamenávat	k5eNaImAgNnS
krátké	krátké	k1gNnSc1
i	i	k8xC
některé	některý	k3yIgFnPc1
dlouhé	dlouhý	k2eAgFnPc1d1
samohlásky	samohláska	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existoval	existovat	k5eAaImAgMnS
tak	tak	k9
problém	problém	k1gInSc4
se	s	k7c7
záměnami	záměna	k1gFnPc7
gramaticky	gramaticky	k6eAd1
podobných	podobný	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
měla	mít	k5eAaImAgFnS
písemně	písemně	k6eAd1
zachycená	zachycený	k2eAgNnPc4d1
seslání	seslání	k1gNnPc4
nižší	nízký	k2eAgFnSc1d2
hodnotu	hodnota	k1gFnSc4
než	než	k8xS
recitace	recitace	k1gFnPc4
zpaměti	zpaměti	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
tradice	tradice	k1gFnSc2
existovalo	existovat	k5eAaImAgNnS
v	v	k7c6
jisté	jistý	k2eAgFnSc6d1
době	doba	k1gFnSc6
celkem	celkem	k6eAd1
pět	pět	k4xCc1
exemplářů	exemplář	k1gInPc2
koránu	korán	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
drobně	drobně	k6eAd1
odlišovaly	odlišovat	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mělo	mít	k5eAaImAgNnS
jít	jít	k5eAaImF
o	o	k7c4
kodex	kodex	k1gInSc4
Muhammadovy	Muhammadův	k2eAgFnSc2d1
manželky	manželka	k1gFnSc2
Hafsy	Hafsa	k1gFnSc2
<g/>
,	,	kIx,
Hafs	Hafs	k1gInSc4
Ubajje	Ubajj	k1gFnSc2
ibn	ibn	k?
Ka	Ka	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ba	ba	k9
<g/>
,	,	kIx,
Abdalláha	Abdalláha	k1gFnSc1
ibn	ibn	k?
Mas	masa	k1gFnPc2
<g/>
'	'	kIx"
<g/>
úda	úd	k1gMnSc2
<g/>
,	,	kIx,
Abú	abú	k1gMnSc2
Músy	Músa	k1gMnSc2
al-Aš	al-Aš	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ařího	aříze	k6eAd1
a	a	k8xC
Miqdada	Miqdada	k1gFnSc1
ibn	ibn	k?
'	'	kIx"
<g/>
Amra	Amra	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
způsobí	způsobit	k5eAaPmIp3nS
rozkol	rozkol	k1gInSc1
v	v	k7c6
islámské	islámský	k2eAgFnSc6d1
obci	obec	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
bylo	být	k5eAaImAgNnS
třeba	třeba	k6eAd1
provést	provést	k5eAaPmF
definitivní	definitivní	k2eAgFnSc4d1
redakci	redakce	k1gFnSc4
textu	text	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
třetího	třetí	k4xOgNnSc2
chalífy	chalífa	k1gMnSc2
Uthmána	Uthmán	k1gMnSc2
tak	tak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
redakční	redakční	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
především	především	k9
na	na	k7c6
základě	základ	k1gInSc6
Hafsiny	Hafsin	k2eAgFnSc2d1
verze	verze	k1gFnSc2
koránu	korán	k1gInSc2
a	a	k8xC
sbírky	sbírka	k1gFnSc2
předsedy	předseda	k1gMnSc2
komise	komise	k1gFnSc2
Zajda	Zajda	k1gMnSc1
ibn	ibn	k?
Thábita	Thábita	k1gFnSc1
sestavila	sestavit	k5eAaPmAgFnS
korán	korán	k1gInSc4
do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Vulgáty	Vulgáta	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
651	#num#	k4
<g/>
–	–	k?
<g/>
656	#num#	k4
v	v	k7c6
Uthmánském	Uthmánský	k2eAgInSc6d1
kodexu	kodex	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
se	se	k3xPyFc4
v	v	k7c6
historii	historie	k1gFnSc6
objevily	objevit	k5eAaPmAgFnP
pochyby	pochyba	k1gFnPc1
o	o	k7c6
pravosti	pravost	k1gFnSc6
této	tento	k3xDgFnSc2
verze	verze	k1gFnSc2
koránu	korán	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
muslimy	muslim	k1gMnPc7
používána	používat	k5eAaImNgFnS
dodnes	dodnes	k6eAd1
<g/>
,	,	kIx,
přes	přes	k7c4
některé	některý	k3yIgFnPc4
změny	změna	k1gFnPc4
z	z	k7c2
důvodu	důvod	k1gInSc2
vývoje	vývoj	k1gInSc2
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
je	být	k5eAaImIp3nS
egyptské	egyptský	k2eAgNnSc1d1
vydání	vydání	k1gNnSc1
z	z	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
námitku	námitka	k1gFnSc4
proti	proti	k7c3
Vulgátě	Vulgáta	k1gFnSc3
vznesli	vznést	k5eAaPmAgMnP
šíité	šíita	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
Uthmána	Uthmán	k1gMnSc4
podezírají	podezírat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
koránu	korán	k1gInSc2
vyškrtal	vyškrtat	k5eAaPmAgMnS
všechny	všechen	k3xTgFnPc4
zmínky	zmínka	k1gFnPc4
o	o	k7c4
Alím	Alím	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
však	však	k9
různé	různý	k2eAgFnPc1d1
šíitské	šíitský	k2eAgFnPc1d1
sekty	sekta	k1gFnPc1
nikdy	nikdy	k6eAd1
nedokázaly	dokázat	k5eNaPmAgFnP
shodnout	shodnout	k5eAaPmF,k5eAaBmF
na	na	k7c6
společné	společný	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
při	při	k7c6
Uthmánově	Uthmánův	k2eAgInSc6d1
textu	text	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdí	tvrdit	k5eAaImIp3nS
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
skutečně	skutečně	k6eAd1
správnou	správný	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
zná	znát	k5eAaImIp3nS
jedině	jedině	k6eAd1
„	„	k?
<g/>
skrytý	skrytý	k2eAgMnSc1d1
imám	imám	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
příchod	příchod	k1gInSc4
očekávají	očekávat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Textová	textový	k2eAgFnSc1d1
kritika	kritika	k1gFnSc1
</s>
<s>
Korán	korán	k1gInSc1
se	se	k3xPyFc4
po	po	k7c6
staletí	staletí	k1gNnSc6
šířil	šířit	k5eAaImAgInS
ústním	ústní	k2eAgNnSc7d1
podáním	podání	k1gNnSc7
nebo	nebo	k8xC
rukopisy	rukopis	k1gInPc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
výhradně	výhradně	k6eAd1
v	v	k7c6
arabštině	arabština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
jej	on	k3xPp3gInSc4
káhirská	káhirský	k2eAgFnSc1d1
islámská	islámský	k2eAgFnSc1d1
Univerzita	univerzita	k1gFnSc1
al-Azhar	al-Azhara	k1gFnPc2
vydala	vydat	k5eAaPmAgFnS
poprvé	poprvé	k6eAd1
tiskem	tisk	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
muslimské	muslimský	k2eAgFnPc1d1
autority	autorita	k1gFnPc1
schválily	schválit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2016	#num#	k4
vydal	vydat	k5eAaPmAgMnS
uznávaný	uznávaný	k2eAgMnSc1d1
islámský	islámský	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
Abdelmajid	Abdelmajida	k1gFnPc2
Charfi	Charf	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
volený	volený	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Tuniské	tuniský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
průkopnické	průkopnický	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
<g/>
:	:	kIx,
první	první	k4xOgNnSc4
kritické	kritický	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
koránu	korán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pěti	pět	k4xCc6
svazcích	svazek	k1gInPc6
a	a	k8xC
na	na	k7c6
2330	#num#	k4
stránkách	stránka	k1gFnPc6
následují	následovat	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc1d1
verše	verš	k1gInPc1
koránu	korán	k1gInSc2
v	v	k7c6
kaligrafii	kaligrafie	k1gFnSc6
i	i	k8xC
v	v	k7c6
současném	současný	k2eAgNnSc6d1
arabském	arabský	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
a	a	k8xC
za	za	k7c7
nimi	on	k3xPp3gInPc7
textové	textový	k2eAgInPc1d1
varianty	variant	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
několika	několik	k4yIc2
nejkratších	krátký	k2eAgInPc2d3
veršů	verš	k1gInPc2
je	být	k5eAaImIp3nS
ke	k	k7c3
každému	každý	k3xTgInSc3
verši	verš	k1gInSc3
řada	řada	k1gFnSc1
variant	varianta	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
publikace	publikace	k1gFnPc4
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
s	s	k7c7
autorem	autor	k1gMnSc7
a	a	k8xC
zdrojem	zdroj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Charfi	Charfi	k6eAd1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
toto	tento	k3xDgNnSc4
kritické	kritický	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
není	být	k5eNaImIp3nS
pro	pro	k7c4
široké	široký	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
je	být	k5eAaImIp3nS
jako	jako	k9
věřící	věřící	k2eAgMnSc1d1
muslim	muslim	k1gMnSc1
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
změna	změna	k1gFnSc1
v	v	k7c6
přístupu	přístup	k1gInSc6
ke	k	k7c3
koránu	korán	k1gInSc3
čeká	čekat	k5eAaImIp3nS
všechny	všechen	k3xTgFnPc4
věřící	věřící	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
textových	textový	k2eAgFnPc2d1
variant	varianta	k1gFnPc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
většinou	většina	k1gFnSc7
formálních	formální	k2eAgInPc2d1
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
nebezpečné	bezpečný	k2eNgNnSc1d1
doslovné	doslovný	k2eAgNnSc1d1
čtení	čtení	k1gNnSc1
starých	starý	k2eAgInPc2d1
textů	text	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
korán	korán	k1gInSc1
také	také	k9
vznikal	vznikat	k5eAaImAgInS
za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
historických	historický	k2eAgFnPc2d1
a	a	k8xC
společenských	společenský	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
problematické	problematický	k2eAgNnSc1d1
tvrdit	tvrdit	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
korán	korán	k2eAgMnSc1d1
tvrdí	tvrdit	k5eAaImIp3nS
to	ten	k3xDgNnSc1
a	a	k8xC
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
a	a	k8xC
pečlivý	pečlivý	k2eAgMnSc1d1
čtenář	čtenář	k1gMnSc1
najde	najít	k5eAaPmIp3nS
místa	místo	k1gNnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
říká	říkat	k5eAaImIp3nS
něco	něco	k3yInSc1
jiného	jiný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
i	i	k9
dnes	dnes	k6eAd1
velmi	velmi	k6eAd1
aktuálních	aktuální	k2eAgNnPc2d1
míst	místo	k1gNnPc2
o	o	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
„	„	k?
<g/>
nevěřícím	nevěřící	k1gMnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
k	k	k7c3
židům	žid	k1gMnPc3
a	a	k8xC
křesťanům	křesťan	k1gMnPc3
<g/>
,	,	kIx,
kterých	který	k3yQgMnPc2,k3yIgMnPc2,k3yRgMnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
kánonickém	kánonický	k2eAgInSc6d1
textu	text	k1gInSc6
koránu	korán	k1gInSc2
přes	přes	k7c4
250	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
mnohaleté	mnohaletý	k2eAgFnSc2d1
práce	práce	k1gFnSc2
dospěl	dochvít	k5eAaPmAgInS
Chárfí	Chárfí	k1gNnSc4
k	k	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
současný	současný	k2eAgInSc1d1
text	text	k1gInSc1
koránu	korán	k1gInSc2
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
vrstev	vrstva	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
původní	původní	k2eAgInPc4d1
křesťanské	křesťanský	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
,	,	kIx,
</s>
<s>
muslimské	muslimský	k2eAgFnSc3d1
interpreatce	interpreatka	k1gFnSc3
tohoto	tento	k3xDgInSc2
„	„	k?
<g/>
neislámského	islámský	k2eNgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
</s>
<s>
texty	text	k1gInPc1
<g/>
,	,	kIx,
připisované	připisovaný	k2eAgInPc1d1
přímo	přímo	k6eAd1
Muhammadovi	Muhammadův	k2eAgMnPc1d1
a	a	k8xC
</s>
<s>
části	část	k1gFnPc1
doplněné	doplněný	k2eAgFnPc1d1
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
633	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jen	jen	k9
krátké	krátký	k2eAgNnSc1d1
a	a	k8xC
snadno	snadno	k6eAd1
zapamatovatelné	zapamatovatelný	k2eAgFnPc1d1
súry	súra	k1gFnPc1
z	z	k7c2
prvního	první	k4xOgInSc2
(	(	kIx(
<g/>
mekkánského	mekkánský	k2eAgInSc2d1
<g/>
)	)	kIx)
období	období	k1gNnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
všech	všecek	k3xTgFnPc6
islámských	islámský	k2eAgFnPc6d1
tradicích	tradice	k1gFnPc6
totožné	totožný	k2eAgFnSc2d1
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nS
asi	asi	k9
5	#num#	k4
%	%	kIx~
celého	celý	k2eAgInSc2d1
koránu	korán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mravenčí	mravenčí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Charfiho	Charfi	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc2
dobrovolných	dobrovolný	k2eAgMnPc2d1
spolupracovníků	spolupracovník	k1gMnPc2
narazila	narazit	k5eAaPmAgFnS
ovšem	ovšem	k9
také	také	k9
na	na	k7c4
odpor	odpor	k1gInSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
cenzurována	cenzurovat	k5eAaImNgFnS
a	a	k8xC
nesměla	smět	k5eNaImAgFnS
vyjít	vyjít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charfi	Charf	k1gInSc6
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
vzdát	vzdát	k5eAaPmF
postavení	postavení	k1gNnSc4
děkana	děkan	k1gMnSc2
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
pokračoval	pokračovat	k5eAaImAgMnS
jako	jako	k9
soukromník	soukromník	k1gMnSc1
<g/>
,	,	kIx,
s	s	k7c7
podporou	podpora	k1gFnSc7
Adenauerovy	Adenauerův	k2eAgFnSc2d1
nadace	nadace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
hradila	hradit	k5eAaImAgFnS
cesty	cesta	k1gFnPc4
do	do	k7c2
archivů	archiv	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
našel	najít	k5eAaPmAgMnS
i	i	k9
libanonský	libanonský	k2eAgMnSc1d1
mecenáš	mecenáš	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zaplatil	zaplatit	k5eAaPmAgMnS
nákladný	nákladný	k2eAgInSc4d1
tisk	tisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
byla	být	k5eAaImAgFnS
kniha	kniha	k1gFnSc1
okamžitě	okamžitě	k6eAd1
zabavena	zabaven	k2eAgFnSc1d1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
v	v	k7c6
Tunisu	Tunis	k1gInSc6
je	být	k5eAaImIp3nS
vyprodána	vyprodat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Struktura	struktura	k1gFnSc1
koránu	korán	k1gInSc2
</s>
<s>
Korán	korán	k1gInSc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
nemá	mít	k5eNaImIp3nS
žádnou	žádný	k3yNgFnSc4
pevnou	pevný	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
,	,	kIx,
souvislý	souvislý	k2eAgInSc4d1
„	„	k?
<g/>
příběh	příběh	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spíše	spíše	k9
než	než	k8xS
kniha	kniha	k1gFnSc1
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
sbírka	sbírka	k1gFnSc1
písemně	písemně	k6eAd1
zaznamenaných	zaznamenaný	k2eAgNnPc2d1
poselství	poselství	k1gNnPc2
proroka	prorok	k1gMnSc2
Muhammada	Muhammada	k1gFnSc1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
napomenutí	napomenutí	k1gNnSc4
do	do	k7c2
života	život	k1gInSc2
věřících	věřící	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
také	také	k6eAd1
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
,	,	kIx,
proč	proč	k6eAd1
se	se	k3xPyFc4
některé	některý	k3yIgInPc1
motivy	motiv	k1gInPc1
a	a	k8xC
témata	téma	k1gNnPc1
opakují	opakovat	k5eAaImIp3nP
i	i	k9
desetkrát	desetkrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
sice	sice	k8xC
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
114	#num#	k4
súr	súra	k1gFnPc2
(	(	kIx(
<g/>
kapitol	kapitola	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
6236	#num#	k4
áj	áj	k?
(	(	kIx(
<g/>
veršů	verš	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
jednotlivé	jednotlivý	k2eAgFnPc1d1
súry	súra	k1gFnPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
složeny	složit	k5eAaPmNgInP
z	z	k7c2
nesourodých	sourodý	k2eNgFnPc2d1
skupin	skupina	k1gFnPc2
veršů	verš	k1gInPc2
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
patrně	patrně	k6eAd1
seslaných	seslaný	k2eAgMnPc2d1
v	v	k7c6
rozpětí	rozpětí	k1gNnSc6
mnoha	mnoho	k4c2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíč	klíč	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
byly	být	k5eAaImAgInP
verše	verš	k1gInPc1
zařazeny	zařadit	k5eAaPmNgInP
do	do	k7c2
právě	právě	k6eAd1
114	#num#	k4
súr	súra	k1gFnPc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
jasný	jasný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základ	základ	k1gInSc1
rozdělení	rozdělení	k1gNnSc2
na	na	k7c4
súry	súra	k1gFnPc1
byl	být	k5eAaImAgInS
položen	položit	k5eAaPmNgInS
již	již	k6eAd1
Muhammadem	Muhammad	k1gInSc7
<g/>
,	,	kIx,
definitivní	definitivní	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
dostal	dostat	k5eAaPmAgMnS
korán	korán	k1gInSc4
při	při	k7c6
uthmánské	uthmánský	k2eAgFnSc6d1
redakci	redakce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
seřazení	seřazení	k1gNnSc3
súr	súra	k1gFnPc2
podle	podle	k7c2
délky	délka	k1gFnSc2
od	od	k7c2
nejdelších	dlouhý	k2eAgInPc2d3
k	k	k7c3
nejkratším	krátký	k2eAgFnPc3d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
tento	tento	k3xDgInSc1
postup	postup	k1gInSc1
však	však	k9
nebyl	být	k5eNaImAgInS
zcela	zcela	k6eAd1
dodržen	dodržet	k5eAaPmNgInS
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c4
začátek	začátek	k1gInSc4
byla	být	k5eAaImAgFnS
zařazena	zařadit	k5eAaPmNgFnS
krátká	krátký	k2eAgFnSc1d1
súra	súra	k1gFnSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
Otevíratelka	Otevíratelka	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
al-fátiha	al-fátiha	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
deváté	devátý	k4xOgFnSc2
súry	súra	k1gFnSc2
(	(	kIx(
<g/>
At-Tawba	At-Tawba	k1gMnSc1
<g/>
)	)	kIx)
všechny	všechen	k3xTgInPc4
ostatní	ostatní	k2eAgInPc4d1
uvozuje	uvozovat	k5eAaImIp3nS
tzv.	tzv.	kA
basmala	basmat	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Boha	bůh	k1gMnSc2
milosrdného	milosrdný	k2eAgMnSc2d1
<g/>
,	,	kIx,
slitovného	slitovný	k2eAgNnSc2d1
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
do	do	k7c2
počtu	počet	k1gInSc2
veršů	verš	k1gInPc2
započítává	započítávat	k5eAaImIp3nS
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pokusy	pokus	k1gInPc1
o	o	k7c4
seřazení	seřazení	k1gNnSc4
súr	súra	k1gFnPc2
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1
učenci	učenec	k1gMnPc1
se	se	k3xPyFc4
opakovaně	opakovaně	k6eAd1
pokoušeli	pokoušet	k5eAaImAgMnP
seřadit	seřadit	k5eAaPmF
súry	súra	k1gFnPc4
chronologicky	chronologicky	k6eAd1
<g/>
,	,	kIx,
podle	podle	k7c2
předpokládané	předpokládaný	k2eAgFnSc2d1
doby	doba	k1gFnSc2
seslání	seslání	k1gNnSc2
jejich	jejich	k3xOp3gFnSc2
podstatné	podstatný	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
činí	činit	k5eAaImIp3nS
české	český	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
z	z	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
v	v	k7c6
překladu	překlad	k1gInSc6
Ivana	Ivan	k1gMnSc4
Hrbka	Hrbek	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
řazení	řazení	k1gNnSc1
je	být	k5eAaImIp3nS
logičtější	logický	k2eAgNnSc1d2
<g/>
,	,	kIx,
přesto	přesto	k8xC
text	text	k1gInSc1
ani	ani	k8xC
tak	tak	k6eAd1
nezískává	získávat	k5eNaImIp3nS
ucelenou	ucelený	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chronologické	chronologický	k2eAgNnSc1d1
řazení	řazení	k1gNnSc1
je	být	k5eAaImIp3nS
však	však	k9
důležité	důležitý	k2eAgNnSc1d1
i	i	k8xC
kvůli	kvůli	k7c3
učení	učení	k1gNnSc3
o	o	k7c6
verších	verš	k1gInPc6
„	„	k?
<g/>
zrušených	zrušený	k2eAgMnPc2d1
a	a	k8xC
zrušujících	zrušující	k2eAgFnPc2d1
<g/>
“	“	k?
nasch	nascha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
praxe	praxe	k1gFnSc2
<g/>
,	,	kIx,
zavedené	zavedený	k2eAgInPc4d1
již	již	k6eAd1
Muhammadem	Muhammad	k1gInSc7
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
později	pozdě	k6eAd2
seslané	seslaný	k2eAgInPc1d1
verše	verš	k1gInPc1
vyšší	vysoký	k2eAgFnSc4d2
hodnotu	hodnota	k1gFnSc4
a	a	k8xC
ruší	rušit	k5eAaImIp3nS
význam	význam	k1gInSc4
těch	ten	k3xDgMnPc2
starších	starší	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
byla	být	k5eAaImAgFnS
nutnost	nutnost	k1gFnSc1
znovu	znovu	k6eAd1
přeformulovávat	přeformulovávat	k5eAaPmF,k5eAaImF
příkazy	příkaz	k1gInPc7
a	a	k8xC
zákazy	zákaz	k1gInPc7
podle	podle	k7c2
aktuální	aktuální	k2eAgFnSc2d1
situace	situace	k1gFnSc2
islámské	islámský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
princip	princip	k1gInSc1
(	(	kIx(
<g/>
nasch	nasch	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
však	však	k9
poměrně	poměrně	k6eAd1
sporný	sporný	k2eAgInSc1d1
a	a	k8xC
jsou	být	k5eAaImIp3nP
o	o	k7c6
něm	on	k3xPp3gNnSc6
vedeny	vést	k5eAaImNgInP
vášnivé	vášnivý	k2eAgInPc1d1
spory	spor	k1gInPc1
mezi	mezi	k7c7
tradicionalisty	tradicionalista	k1gMnPc7
a	a	k8xC
modernisty	modernista	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Orientalisté	orientalista	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
řadili	řadit	k5eAaImAgMnP
súry	súra	k1gFnSc2
chronologickým	chronologický	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
:	:	kIx,
</s>
<s>
Němec	Němec	k1gMnSc1
Theodor	Theodor	k1gMnSc1
Nöldeke	Nöldeke	k1gFnSc1
(	(	kIx(
<g/>
vydání	vydání	k1gNnSc1
1860	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Angličan	Angličan	k1gMnSc1
John	John	k1gMnSc1
Medows	Medowsa	k1gFnPc2
Rodwell	Rodwell	k1gMnSc1
(	(	kIx(
<g/>
vydání	vydání	k1gNnSc1
1876	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dán	dán	k2eAgInSc1d1
Frants	Frants	k1gInSc1
Buhl	Buhl	k1gInSc1
(	(	kIx(
<g/>
vydání	vydání	k1gNnSc1
1921	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Francouz	Francouz	k1gMnSc1
Régis	Régis	k1gFnSc2
Blachè	Blachè	k1gInSc5
(	(	kIx(
<g/>
vydání	vydání	k1gNnSc2
1949	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
o	o	k7c4
chronologické	chronologický	k2eAgNnSc4d1
řazení	řazení	k1gNnSc4
pokusil	pokusit	k5eAaPmAgMnS
Ivan	Ivan	k1gMnSc1
Hrbek	Hrbek	k1gMnSc1
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Srovnání	srovnání	k1gNnSc1
s	s	k7c7
Biblí	bible	k1gFnSc7
</s>
<s>
Korán	korán	k1gInSc1
není	být	k5eNaImIp3nS
srovnatelný	srovnatelný	k2eAgInSc1d1
s	s	k7c7
hebrejskou	hebrejský	k2eAgFnSc7d1
Biblí	bible	k1gFnSc7
formou	forma	k1gFnSc7
ani	ani	k8xC
obsahem	obsah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starý	starý	k2eAgInSc1d1
zákon	zákon	k1gInSc1
čili	čili	k8xC
Tanach	Tanach	k1gInSc1
je	být	k5eAaImIp3nS
rozsáhlejší	rozsáhlý	k2eAgInSc1d2
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
rozmanité	rozmanitý	k2eAgInPc4d1
texty	text	k1gInPc4
(	(	kIx(
<g/>
knihy	kniha	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
vznikaly	vznikat	k5eAaImAgFnP
téměř	téměř	k6eAd1
tisíc	tisíc	k4xCgInSc4
let	let	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
písně	píseň	k1gFnPc4
a	a	k8xC
básně	báseň	k1gFnPc4
<g/>
,	,	kIx,
příběhy	příběh	k1gInPc4
<g/>
,	,	kIx,
historická	historický	k2eAgNnPc4d1
vyprávění	vyprávění	k1gNnPc4
i	i	k8xC
prorocké	prorocký	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srovnávat	srovnávat	k5eAaImF
s	s	k7c7
koránem	korán	k1gInSc7
by	by	kYmCp3nP
se	se	k3xPyFc4
snad	snad	k9
daly	dát	k5eAaPmAgInP
náboženské	náboženský	k2eAgInPc1d1
a	a	k8xC
právní	právní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
<g/>
,	,	kIx,
konkrétní	konkrétní	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
<g/>
,	,	kIx,
příkazy	příkaz	k1gInPc1
<g/>
,	,	kIx,
zákazy	zákaz	k1gInPc1
a	a	k8xC
někdy	někdy	k6eAd1
i	i	k9
tresty	trest	k1gInPc1
za	za	k7c2
porušení	porušení	k1gNnSc2
těchto	tento	k3xDgNnPc2
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc1
nerozlišuje	rozlišovat	k5eNaImIp3nS
mezi	mezi	k7c7
náboženstvím	náboženství	k1gNnSc7
a	a	k8xC
právem	právo	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
začíná	začínat	k5eAaImIp3nS
již	již	k6eAd1
v	v	k7c6
koránu	korán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčinou	příčina	k1gFnSc7
jsou	být	k5eAaImIp3nP
okolnosti	okolnost	k1gFnPc1
vzniku	vznik	k1gInSc2
této	tento	k3xDgFnSc2
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc1
nebyl	být	k5eNaImAgInS
pouze	pouze	k6eAd1
duchovní	duchovní	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
politický	politický	k2eAgMnSc1d1
a	a	k8xC
vojenský	vojenský	k2eAgMnSc1d1
představitel	představitel	k1gMnSc1
<g/>
,	,	kIx,
obsah	obsah	k1gInSc1
koránu	korán	k1gInSc2
proto	proto	k8xC
odpovídá	odpovídat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
aktuální	aktuální	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
a	a	k8xC
potřebám	potřeba	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
Mekce	Mekka	k1gFnSc6
však	však	k9
korán	korán	k1gInSc1
formuloval	formulovat	k5eAaImAgInS
některé	některý	k3yIgFnPc4
zásady	zásada	k1gFnPc4
věřících	věřící	k2eAgMnPc2d1
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
například	například	k6eAd1
podoba	podoba	k1gFnSc1
modlitby	modlitba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výklad	výklad	k1gInSc1
koránu	korán	k1gInSc2
je	být	k5eAaImIp3nS
oblastí	oblast	k1gFnSc7
zájmu	zájem	k1gInSc2
islámských	islámský	k2eAgMnPc2d1
učenců	učenec	k1gMnPc2
po	po	k7c4
celé	celý	k2eAgFnPc4d1
islámské	islámský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
korán	korán	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
základ	základ	k1gInSc4
šarí	šarí	k1gFnSc2
<g/>
'	'	kIx"
<g/>
y	y	k?
<g/>
,	,	kIx,
pro	pro	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
výklad	výklad	k1gInSc4
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgInP
další	další	k2eAgInPc1d1
prameny	pramen	k1gInPc1
a	a	k8xC
uznávané	uznávaný	k2eAgInPc1d1
právní	právní	k2eAgInPc1d1
postupy	postup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradičně	tradičně	k6eAd1
se	se	k3xPyFc4
korán	korán	k1gInSc1
dělil	dělit	k5eAaImAgInS
na	na	k7c4
30	#num#	k4
zhruba	zhruba	k6eAd1
stejně	stejně	k6eAd1
dlouhých	dlouhý	k2eAgFnPc2d1
částí	část	k1gFnPc2
(	(	kIx(
<g/>
ajzá	ajzá	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dal	dát	k5eAaPmAgInS
celý	celý	k2eAgInSc4d1
projít	projít	k5eAaPmF
v	v	k7c6
modlitbě	modlitba	k1gFnSc6
za	za	k7c4
30	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
historického	historický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
koránský	koránský	k2eAgInSc1d1
text	text	k1gInSc1
rozděluje	rozdělovat	k5eAaImIp3nS
na	na	k7c4
4	#num#	k4
části	část	k1gFnSc2
podle	podle	k7c2
doby	doba	k1gFnSc2
seslání	seslání	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
spojuje	spojovat	k5eAaImIp3nS
podobný	podobný	k2eAgInSc1d1
obsah	obsah	k1gInSc1
a	a	k8xC
forma	forma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgInSc4
<g/>
,	,	kIx,
druhé	druhý	k4xOgNnSc4
a	a	k8xC
třetí	třetí	k4xOgNnSc4
mekkánské	mekkánský	k2eAgNnSc4d1
období	období	k1gNnSc4
a	a	k8xC
medínské	medínský	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozdělení	rozdělení	k1gNnSc1
je	být	k5eAaImIp3nS
však	však	k9
spíše	spíše	k9
přibližné	přibližný	k2eAgNnSc1d1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
určit	určit	k5eAaPmF
jejich	jejich	k3xOp3gFnPc4
přesné	přesný	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
(	(	kIx(
<g/>
snad	snad	k9
vyjma	vyjma	k7c2
posledního	poslední	k2eAgInSc2d1
<g/>
)	)	kIx)
a	a	k8xC
celá	celý	k2eAgFnSc1d1
věc	věc	k1gFnSc1
je	být	k5eAaImIp3nS
ještě	ještě	k9
komplikována	komplikovat	k5eAaBmNgFnS
pozdějšími	pozdní	k2eAgFnPc7d2
úpravami	úprava	k1gFnPc7
a	a	k8xC
doplňováním	doplňování	k1gNnSc7
starších	starý	k2eAgFnPc2d2
súr	súra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otázka	otázka	k1gFnSc1
původu	původ	k1gInSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
úseků	úsek	k1gInPc2
súr	súra	k1gFnPc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
složitá	složitý	k2eAgFnSc1d1
a	a	k8xC
názory	názor	k1gInPc1
nejsou	být	k5eNaImIp3nP
jednotné	jednotný	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Jazyk	jazyk	k1gInSc1
koránu	korán	k1gInSc2
</s>
<s>
Korán	korán	k2eAgInSc4d1
vytvořený	vytvořený	k2eAgInSc4d1
Tatary	tatar	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vidění	vidění	k1gNnSc3
je	být	k5eAaImIp3nS
v	v	k7c6
Chánském	chánský	k2eAgInSc6d1
paláci	palác	k1gInSc6
v	v	k7c6
Bachčisaraji	Bachčisaraj	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jazyk	jazyk	k1gInSc1
a	a	k8xC
forma	forma	k1gFnSc1
koránu	korán	k1gInSc2
jsou	být	k5eAaImIp3nP
velice	velice	k6eAd1
rozmanité	rozmanitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoduše	jednoduše	k6eAd1
můžeme	moct	k5eAaImIp1nP
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
lze	lze	k6eAd1
sledovat	sledovat	k5eAaImF
vývoj	vývoj	k1gInSc4
od	od	k7c2
velice	velice	k6eAd1
poetických	poetický	k2eAgInPc2d1
a	a	k8xC
obrazných	obrazný	k2eAgInPc2d1
veršů	verš	k1gInPc2
k	k	k7c3
sušším	suchý	k2eAgInPc3d2
spíše	spíše	k9
právnickým	právnický	k2eAgInPc3d1
a	a	k8xC
prozaickým	prozaický	k2eAgInPc3d1
textům	text	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
formou	forma	k1gFnSc7
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
i	i	k9
obsahem	obsah	k1gInSc7
sdělení	sdělení	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
ze	z	k7c2
začátku	začátek	k1gInSc2
Muhammad	Muhammad	k1gInSc1
nijak	nijak	k6eAd1
nevymyká	vymykat	k5eNaImIp3nS
z	z	k7c2
řady	řada	k1gFnSc2
předchozích	předchozí	k2eAgMnPc2d1
staroarabských	staroarabský	k2eAgMnPc2d1
hanífů	haníf	k1gMnPc2
(	(	kIx(
<g/>
poslů	posel	k1gMnPc2
monoteismu	monoteismus	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
káhinů	káhin	k1gMnPc2
(	(	kIx(
<g/>
věštců	věštec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jejich	jejich	k3xOp3gFnSc3
charakteristice	charakteristika	k1gFnSc3
patří	patřit	k5eAaImIp3nS
krátký	krátký	k2eAgInSc1d1
verš	verš	k1gInSc1
<g/>
,	,	kIx,
extatická	extatický	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgInPc4d1
a	a	k8xC
kosmické	kosmický	k2eAgInPc4d1
obrazy	obraz	k1gInPc4
<g/>
,	,	kIx,
přísahy	přísaha	k1gFnPc4
<g/>
,	,	kIx,
hrozby	hrozba	k1gFnPc4
<g/>
,	,	kIx,
kletby	kletba	k1gFnPc4
a	a	k8xC
apokalyptické	apokalyptický	k2eAgFnPc4d1
vize	vize	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přirovnávání	přirovnávání	k1gNnPc1
ke	k	k7c3
káhinům	káhina	k1gMnPc3
se	se	k3xPyFc4
Muhammad	Muhammad	k1gInSc1
při	při	k7c6
diskusích	diskuse	k1gFnPc6
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
kritiky	kritik	k1gMnPc7
ostře	ostro	k6eAd1
bránil	bránit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
nejpoetičtější	poetický	k2eAgFnSc4d3
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
považovat	považovat	k5eAaImF
verše	verš	k1gInPc4
z	z	k7c2
prvního	první	k4xOgNnSc2
mekkánského	mekkánský	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
zmiňována	zmiňovat	k5eAaImNgFnS
bývá	bývat	k5eAaImIp3nS
především	především	k9
súra	súra	k1gFnSc1
55	#num#	k4
pro	pro	k7c4
hojný	hojný	k2eAgInSc4d1
výskyt	výskyt	k1gInSc4
až	až	k9
monotónně	monotónně	k6eAd1
se	se	k3xPyFc4
opakujícího	opakující	k2eAgInSc2d1
refrénu	refrén	k1gInSc2
„	„	k?
<g/>
Které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
z	z	k7c2
dobrodiní	dobrodiní	k1gNnSc2
Pána	pán	k1gMnSc2
svého	svůj	k1gMnSc2
můžete	moct	k5eAaImIp2nP
popírat	popírat	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
dalších	další	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
se	se	k3xPyFc4
postupně	postupně	k6eAd1
prosazují	prosazovat	k5eAaImIp3nP
delší	dlouhý	k2eAgFnPc4d2
súry	súra	k1gFnPc4
a	a	k8xC
delší	dlouhý	k2eAgInPc4d2
verše	verš	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
začínají	začínat	k5eAaImIp3nP
postrádat	postrádat	k5eAaImF
poetické	poetický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
,	,	kIx,
jakými	jaký	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
jsou	být	k5eAaImIp3nP
anafora	anafora	k1gFnSc1
(	(	kIx(
<g/>
užití	užití	k1gNnSc1
stejných	stejná	k1gFnPc2
slov	slovo	k1gNnPc2
na	na	k7c6
začátku	začátek	k1gInSc6
veršů	verš	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
epifora	epifora	k1gFnSc1
(	(	kIx(
<g/>
opakování	opakování	k1gNnSc1
stejných	stejná	k1gFnPc2
slov	slovo	k1gNnPc2
na	na	k7c6
konci	konec	k1gInSc6
veršů	verš	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
již	již	k6eAd1
zmíněný	zmíněný	k2eAgInSc1d1
refrén	refrén	k1gInSc1
<g/>
,	,	kIx,
až	až	k6eAd1
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
v	v	k7c6
medínském	medínský	k2eAgNnSc6d1
období	období	k1gNnSc6
vytrácejí	vytrácet	k5eAaImIp3nP
takřka	takřka	k6eAd1
docela	docela	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
přísahy	přísaha	k1gFnSc2
a	a	k8xC
zvolání	zvolání	k1gNnSc4
k	k	k7c3
Bohu	bůh	k1gMnSc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
jsou	být	k5eAaImIp3nP
ze	z	k7c2
začátku	začátek	k1gInSc2
velmi	velmi	k6eAd1
často	často	k6eAd1
využívány	využívat	k5eAaImNgInP,k5eAaPmNgInP
jako	jako	k8xS,k8xC
jakési	jakýsi	k3yIgInPc4
úvody	úvod	k1gInPc4
súr	súra	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
v	v	k7c6
medínském	medínský	k2eAgNnSc6d1
období	období	k1gNnSc6
nevyskytují	vyskytovat	k5eNaImIp3nP
zdaleka	zdaleka	k6eAd1
v	v	k7c6
tak	tak	k6eAd1
hojném	hojný	k2eAgInSc6d1
počtu	počet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
zmíněno	zmínit	k5eAaPmNgNnS
<g/>
,	,	kIx,
arabský	arabský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
v	v	k7c6
době	doba	k1gFnSc6
vzniku	vznik	k1gInSc2
koránu	korán	k1gInSc2
trpěl	trpět	k5eAaImAgMnS
především	především	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
psané	psaný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
značnou	značný	k2eAgFnSc7d1
nedokonalostí	nedokonalost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledně	posledně	k6eAd1
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
také	také	k9
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
slovní	slovní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
arabštiny	arabština	k1gFnSc2
neodpovídala	odpovídat	k5eNaImAgFnS
Muhammadovým	Muhammadový	k2eAgFnPc3d1
potřebám	potřeba	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
proto	proto	k8xC
uchyloval	uchylovat	k5eAaImAgMnS
jednak	jednak	k8xC
k	k	k7c3
aktualizaci	aktualizace	k1gFnSc3
významu	význam	k1gInSc2
již	již	k6eAd1
existujících	existující	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
k	k	k7c3
přejímání	přejímání	k1gNnSc3
zcela	zcela	k6eAd1
nových	nový	k2eAgInPc2d1
výrazů	výraz	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
z	z	k7c2
aramejštiny	aramejština	k1gFnSc2
<g/>
,	,	kIx,
hebrejštiny	hebrejština	k1gFnSc2
<g/>
,	,	kIx,
syrštiny	syrština	k1gFnSc2
či	či	k8xC
etiopštiny	etiopština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
však	však	k9
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
zásadnější	zásadní	k2eAgInSc4d2
vliv	vliv	k1gInSc4
na	na	k7c4
výklad	výklad	k1gInSc4
koránu	korán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odlišné	odlišný	k2eAgInPc1d1
výklady	výklad	k1gInPc1
se	se	k3xPyFc4
spíše	spíše	k9
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
některými	některý	k3yIgFnPc7
gramatickými	gramatický	k2eAgFnPc7d1
obměnami	obměna	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mohou	moct	k5eAaImIp3nP
i	i	k9
zcela	zcela	k6eAd1
zásadně	zásadně	k6eAd1
změnit	změnit	k5eAaPmF
význam	význam	k1gInSc4
veršů	verš	k1gInPc2
a	a	k8xC
posloužit	posloužit	k5eAaPmF
tak	tak	k6eAd1
jako	jako	k8xS,k8xC
argument	argument	k1gInSc1
pro	pro	k7c4
ten	ten	k3xDgInSc4
či	či	k8xC
onen	onen	k3xDgInSc1
postoj	postoj	k1gInSc1
odporující	odporující	k2eAgInSc1d1
tradičnímu	tradiční	k2eAgInSc3d1
výkladu	výklad	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výklad	výklad	k1gInSc1
gramatiky	gramatika	k1gFnSc2
byl	být	k5eAaImAgInS
také	také	k6eAd1
jednou	jeden	k4xCgFnSc7
z	z	k7c2
metod	metoda	k1gFnPc2
fikhu	fikh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jazyk	jazyk	k1gInSc1
koránu	korán	k1gInSc2
byl	být	k5eAaImAgInS
až	až	k9
do	do	k7c2
moderní	moderní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
přijímán	přijímat	k5eAaImNgMnS
jako	jako	k9
norma	norma	k1gFnSc1
spisovné	spisovný	k2eAgFnSc2d1
arabštiny	arabština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
inspirace	inspirace	k1gFnSc2
</s>
<s>
Královna	královna	k1gFnSc1
ze	z	k7c2
Sáby	Sába	k1gFnSc2
látogatása	látogatása	k1gFnSc1
Šalomoun	Šalomoun	k1gMnSc1
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Poynter	Poynter	k1gMnSc1
<g/>
,	,	kIx,
1890	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šalomoun	Šalomoun	k1gMnSc1
prófétaként	prófétaként	k1gMnSc1
lép	lép	k?
be	be	k?
a	a	k8xC
Koránba	Koránba	k1gMnSc1
<g/>
,	,	kIx,
aki	aki	k?
uralkodik	uralkodik	k1gInSc1
az	az	k?
emberen	emberna	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
dzsinnen	dzsinnen	k1gInSc1
és	és	k?
a	a	k8xC
természeten	természeten	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Muhammad	Muhammad	k1gInSc1
považoval	považovat	k5eAaImAgInS
své	svůj	k3xOyFgNnSc4
proroctví	proroctví	k1gNnSc4
za	za	k7c4
návrat	návrat	k1gInSc4
k	k	k7c3
přirozenému	přirozený	k2eAgInSc3d1
monoteismu	monoteismus	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
hlásali	hlásat	k5eAaImAgMnP
již	již	k6eAd1
před	před	k7c7
ním	on	k3xPp3gMnSc7
mnozí	mnohý	k2eAgMnPc1d1
prorokové	prorokové	k?
a	a	k8xC
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
judaismus	judaismus	k1gInSc4
a	a	k8xC
křesťanství	křesťanství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odchodu	odchod	k1gInSc6
do	do	k7c2
Medíny	Medína	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
setkala	setkat	k5eAaPmAgFnS
komunita	komunita	k1gFnSc1
prvních	první	k4xOgMnPc2
muslimů	muslim	k1gMnPc2
s	s	k7c7
židovskou	židovský	k2eAgFnSc7d1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
křesťanskou	křesťanský	k2eAgFnSc4d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
zjevují	zjevovat	k5eAaImIp3nP
verše	verš	k1gInPc1
reagující	reagující	k2eAgInPc1d1
na	na	k7c4
aktuální	aktuální	k2eAgFnPc4d1
situace	situace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orientace	orientace	k1gFnSc1
modlitby	modlitba	k1gFnSc2
se	se	k3xPyFc4
přesunuje	přesunovat	k5eAaImIp3nS
z	z	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
na	na	k7c4
Mekku	Mekka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc1
deklaruje	deklarovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
přináší	přinášet	k5eAaImIp3nS
stejné	stejný	k2eAgNnSc4d1
poselství	poselství	k1gNnSc4
jako	jako	k8xC,k8xS
přední	přední	k2eAgMnSc1d1
hanífa	haníf	k1gMnSc2
Abrahám	Abrahám	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
založil	založit	k5eAaPmAgMnS
modlitebnu	modlitebna	k1gFnSc4
Ka	Ka	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ba	ba	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
považuje	považovat	k5eAaImIp3nS
židy	žid	k1gMnPc7
a	a	k8xC
křesťany	křesťan	k1gMnPc7
za	za	k7c2
„	„	k?
<g/>
lid	lid	k1gInSc1
Knihy	kniha	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
sice	sice	k8xC
zbloudil	zbloudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
zaslouží	zasloužit	k5eAaPmIp3nS
si	se	k3xPyFc3
ochranu	ochrana	k1gFnSc4
a	a	k8xC
úctu	úcta	k1gFnSc4
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
proti	proti	k7c3
nim	on	k3xPp3gInPc3
nevystupují	vystupovat	k5eNaImIp3nP
nepřátelsky	přátelsky	k6eNd1
<g/>
;	;	kIx,
na	na	k7c6
jiných	jiný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
je	být	k5eAaImIp3nS
ale	ale	k9
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
"	"	kIx"
<g/>
nevěřící	věřící	k2eNgInSc4d1
<g/>
"	"	kIx"
a	a	k8xC
hrozí	hrozit	k5eAaImIp3nS
vyhubením	vyhubení	k1gNnSc7
(	(	kIx(
<g/>
Súra	súra	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
253	#num#	k4
<g/>
;	;	kIx,
8	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
koránu	korán	k1gInSc6
se	se	k3xPyFc4
například	například	k6eAd1
objevují	objevovat	k5eAaImIp3nP
příběhy	příběh	k1gInPc4
o	o	k7c6
starých	starý	k2eAgFnPc6d1
arabských	arabský	k2eAgFnPc6d1
prorocích	prorok	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nebyli	být	k5eNaImAgMnP
vyslyšeni	vyslyšet	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
koránu	korán	k1gInSc6
jsou	být	k5eAaImIp3nP
zmínky	zmínka	k1gFnPc1
o	o	k7c6
džinech	džin	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
se	se	k3xPyFc4
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
džinové	džin	k1gMnPc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
dobří	dobrý	k2eAgMnPc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
zlí	zlý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Témata	téma	k1gNnPc1
v	v	k7c6
odlišných	odlišný	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
</s>
<s>
Ze	z	k7c2
súr	súra	k1gFnPc2
prvního	první	k4xOgNnSc2
mekkánského	mekkánský	k2eAgNnSc2d1
období	období	k1gNnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
cítit	cítit	k5eAaImF
nadšení	nadšení	k1gNnSc4
z	z	k7c2
přítomnosti	přítomnost	k1gFnSc2
Boží	boží	k2eAgFnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
moci	moct	k5eAaImF
stvořitele	stvořitel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Líčení	líčení	k1gNnSc1
Boha	bůh	k1gMnSc2
jako	jako	k8xC,k8xS
tvůrce	tvůrce	k1gMnSc2
všeho	všecek	k3xTgNnSc2
<g/>
,	,	kIx,
nejvyššího	vysoký	k2eAgMnSc2d3
vládce	vládce	k1gMnSc2
<g/>
,	,	kIx,
kterému	který	k3yIgNnSc3,k3yQgNnSc3,k3yRgNnSc3
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
se	se	k3xPyFc4
bezpodmínečně	bezpodmínečně	k6eAd1
odevzdat	odevzdat	k5eAaPmF
a	a	k8xC
poslouchat	poslouchat	k5eAaImF
jeho	jeho	k3xOp3gFnSc4
vůli	vůle	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
záhy	záhy	k6eAd1
doplňováno	doplňovat	k5eAaImNgNnS
eschatologickými	eschatologický	k2eAgFnPc7d1
představami	představa	k1gFnPc7
o	o	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
posledním	poslední	k2eAgInSc6d1
soudu	soud	k1gInSc6
<g/>
,	,	kIx,
ráji	rája	k1gFnSc3
a	a	k8xC
peklu	peklo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc1
se	se	k3xPyFc4
asi	asi	k9
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
setkal	setkat	k5eAaPmAgMnS
s	s	k7c7
odporem	odpor	k1gInSc7
<g/>
,	,	kIx,
zjevuje	zjevovat	k5eAaImIp3nS
se	se	k3xPyFc4
podrobný	podrobný	k2eAgInSc4d1
popis	popis	k1gInSc4
ráje	ráj	k1gInSc2
a	a	k8xC
pekla	peklo	k1gNnSc2
k	k	k7c3
přesvědčování	přesvědčování	k1gNnSc3
oponentů	oponent	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verše	verš	k1gInSc2
o	o	k7c6
posmrtném	posmrtný	k2eAgInSc6d1
životě	život	k1gInSc6
jsou	být	k5eAaImIp3nP
velice	velice	k6eAd1
sugestivní	sugestivní	k2eAgFnPc1d1
a	a	k8xC
přímočaré	přímočarý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdější	pozdní	k2eAgFnSc1d2
kritika	kritika	k1gFnSc1
Muhammada	Muhammada	k1gFnSc1
ze	z	k7c2
strany	strana	k1gFnSc2
židů	žid	k1gMnPc2
a	a	k8xC
křesťanů	křesťan	k1gMnPc2
se	se	k3xPyFc4
často	často	k6eAd1
opírá	opírat	k5eAaImIp3nS
právě	právě	k9
o	o	k7c4
tuto	tento	k3xDgFnSc4
část	část	k1gFnSc4
koránu	korán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vadí	vadit	k5eAaImIp3nP
jim	on	k3xPp3gMnPc3
na	na	k7c6
ní	on	k3xPp3gFnSc6
její	její	k3xOp3gFnSc7
materialismus	materialismus	k1gInSc1
<g/>
,	,	kIx,
přesvědčování	přesvědčování	k1gNnSc1
k	k	k7c3
víře	víra	k1gFnSc3
skrze	skrze	k?
přesné	přesný	k2eAgInPc1d1
popisy	popis	k1gInPc1
rajských	rajský	k2eAgFnPc2d1
slastí	slast	k1gFnPc2
a	a	k8xC
naopak	naopak	k6eAd1
vyhrožování	vyhrožování	k1gNnSc4
fyzickým	fyzický	k2eAgNnSc7d1
utrpením	utrpení	k1gNnSc7
v	v	k7c6
pekle	peklo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
závěr	závěr	k1gInSc4
prvního	první	k4xOgNnSc2
období	období	k1gNnSc2
<g/>
,	,	kIx,
po	po	k7c4
odvolání	odvolání	k1gNnSc4
„	„	k?
<g/>
Satanských	satanský	k2eAgInPc2d1
veršů	verš	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
pokračuje	pokračovat	k5eAaImIp3nS
korán	korán	k1gInSc4
druhým	druhý	k4xOgNnSc7
obdobím	období	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhé	druhý	k4xOgFnSc2
období	období	k1gNnPc2
je	být	k5eAaImIp3nS
charakteristické	charakteristický	k2eAgNnSc1d1
právě	právě	k9
zdůrazňováním	zdůrazňování	k1gNnSc7
Boží	boží	k2eAgFnSc2d1
jedinosti	jedinost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eschatologické	eschatologický	k2eAgFnPc4d1
představy	představa	k1gFnPc4
poněkud	poněkud	k6eAd1
ustupují	ustupovat	k5eAaImIp3nP
do	do	k7c2
pozadí	pozadí	k1gNnSc2
a	a	k8xC
hlavní	hlavní	k2eAgInSc1d1
argumentační	argumentační	k2eAgInSc1d1
proud	proud	k1gInSc1
se	se	k3xPyFc4
točí	točit	k5eAaImIp3nS
okolo	okolo	k7c2
příběhů	příběh	k1gInPc2
předchozích	předchozí	k2eAgMnPc2d1
proroků	prorok	k1gMnPc2
a	a	k8xC
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
je	on	k3xPp3gNnSc4
neuposlechly	uposlechnout	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasická	klasický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
příběhu	příběh	k1gInSc2
<g/>
,	,	kIx,
prorok	prorok	k1gMnSc1
–	–	k?
odmítnutí	odmítnutí	k1gNnSc6
–	–	k?
Boží	boží	k2eAgFnSc1d1
pomsta	pomsta	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
neustále	neustále	k6eAd1
opakuje	opakovat	k5eAaImIp3nS
<g/>
,	,	kIx,
zaměňována	zaměňován	k2eAgNnPc1d1
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
jména	jméno	k1gNnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
několikrát	několikrát	k6eAd1
opakují	opakovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickou	charakteristický	k2eAgFnSc7d1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgNnP
být	být	k5eAaImF
poměrně	poměrně	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
súra	súra	k1gFnSc1
26	#num#	k4
–	–	k?
Básníci	básník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Podoba	podoba	k1gFnSc1
seslání	seslání	k1gNnSc2
z	z	k7c2
třetího	třetí	k4xOgNnSc2
mekkánského	mekkánský	k2eAgNnSc2d1
období	období	k1gNnSc2
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
určena	určit	k5eAaPmNgFnS
situací	situace	k1gFnSc7
Muhammada	Muhammada	k1gFnSc1
v	v	k7c6
Mekce	Mekka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
začínala	začínat	k5eAaImAgFnS
být	být	k5eAaImF
značně	značně	k6eAd1
kritická	kritický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
již	již	k6eAd1
Muhammad	Muhammad	k1gInSc1
začal	začít	k5eAaPmAgInS
zcela	zcela	k6eAd1
vážně	vážně	k6eAd1
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
útěku	útěk	k1gInSc6
z	z	k7c2
Mekky	Mekka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
měl	mít	k5eAaImAgInS
islám	islám	k1gInSc1
rozšířit	rozšířit	k5eAaPmF
i	i	k9
mimo	mimo	k7c4
své	svůj	k3xOyFgNnSc4
domovské	domovský	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
formy	forma	k1gFnSc2
týče	týkat	k5eAaImIp3nS
<g/>
,	,	kIx,
styl	styl	k1gInSc1
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
blížit	blížit	k5eAaImF
medínskému	medínský	k2eAgMnSc3d1
<g/>
,	,	kIx,
poezie	poezie	k1gFnSc1
ustupuje	ustupovat	k5eAaImIp3nS
próze	próza	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
bychom	by	kYmCp1nP
měli	mít	k5eAaImAgMnP
vybrat	vybrat	k5eAaPmF
jeden	jeden	k4xCgInSc4
charakteristický	charakteristický	k2eAgInSc4d1
rys	rys	k1gInSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
by	by	kYmCp3nP
to	ten	k3xDgNnSc1
představa	představa	k1gFnSc1
o	o	k7c6
predestinaci	predestinace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammadovi	Muhammadův	k2eAgMnPc5d1
<g/>
,	,	kIx,
čelícímu	čelící	k2eAgNnSc3d1
vytrvalému	vytrvalý	k2eAgNnSc3d1
odmítání	odmítání	k1gNnSc3
a	a	k8xC
posměchu	posměch	k1gInSc3
spojenému	spojený	k2eAgInSc3d1
se	s	k7c7
zcela	zcela	k6eAd1
reálnými	reálný	k2eAgFnPc7d1
hrozbami	hrozba	k1gFnPc7
i	i	k9
vůči	vůči	k7c3
samotnému	samotný	k2eAgInSc3d1
životu	život	k1gInSc3
Proroka	prorok	k1gMnSc2
<g/>
,	,	kIx,
zjevují	zjevovat	k5eAaImIp3nP
se	se	k3xPyFc4
verše	verš	k1gInPc1
o	o	k7c4
předurčenosti	předurčenost	k1gFnPc4
lidí	člověk	k1gMnPc2
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
věřícími	věřící	k1gMnPc7
nebo	nebo	k8xC
zůstat	zůstat	k5eAaPmF
v	v	k7c6
rouhačské	rouhačský	k2eAgFnSc6d1
lži	lež	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozpor	rozpor	k1gInSc1
Boží	boží	k2eAgFnSc2d1
všemocnosti	všemocnost	k1gFnSc2
a	a	k8xC
vševědoucnosti	vševědoucnost	k1gFnSc2
se	s	k7c7
svobodou	svoboda	k1gFnSc7
lidské	lidský	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
je	být	k5eAaImIp3nS
ostatně	ostatně	k6eAd1
trvalým	trvalý	k2eAgNnSc7d1
filosofickým	filosofický	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
i	i	k9
v	v	k7c6
křesťanském	křesťanský	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
koránu	korán	k1gInSc6
se	se	k3xPyFc4
píše	psát	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kdyby	kdyby	kYmCp3nS
Bůh	bůh	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
by	by	kYmCp3nP
byli	být	k5eAaImAgMnP
věřící	věřící	k2eAgMnPc1d1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
:	:	kIx,
<g/>
99	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
„	„	k?
<g/>
A	a	k8xC
kdyby	kdyby	kYmCp3nP
si	se	k3xPyFc3
tvůj	tvůj	k3xOp2gMnSc1
Pán	pán	k1gMnSc1
přál	přát	k5eAaImAgMnS
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
na	na	k7c6
zemi	zem	k1gFnSc6
by	by	kYmCp3nP
rázem	rázem	k6eAd1
uvěřili	uvěřit	k5eAaPmAgMnP
<g/>
!	!	kIx.
</s>
<s desamb="1">
Budeš	být	k5eAaImBp2nS
tedy	tedy	k9
(	(	kIx(
<g/>
Muhammade	Muhammad	k1gInSc5
<g/>
)	)	kIx)
nutit	nutit	k5eAaImF
lidi	člověk	k1gMnPc4
proti	proti	k7c3
jejich	jejich	k3xOp3gFnSc3
vůli	vůle	k1gFnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
věřícími	věřící	k1gFnPc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
</s>
<s>
Medínské	medínský	k2eAgNnSc1d1
období	období	k1gNnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
našeho	náš	k3xOp1gInSc2
pohledu	pohled	k1gInSc2
nejdůležitější	důležitý	k2eAgFnPc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
přesunu	přesun	k1gInSc6
do	do	k7c2
Medíny	Medína	k1gFnSc2
získal	získat	k5eAaPmAgInS
Muhammad	Muhammad	k1gInSc1
možnost	možnost	k1gFnSc4
určit	určit	k5eAaPmF
konkrétní	konkrétní	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
života	život	k1gInSc2
své	svůj	k3xOyFgFnSc2
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
korán	korán	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
jen	jen	k9
velmi	velmi	k6eAd1
málo	málo	k4c1
jasných	jasný	k2eAgInPc2d1
příkazů	příkaz	k1gInPc2
<g/>
,	,	kIx,
naprosto	naprosto	k6eAd1
minimálně	minimálně	k6eAd1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
postihy	postih	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
proto	proto	k6eAd1
doplňován	doplňovat	k5eAaImNgInS
dalšími	další	k2eAgInPc7d1
prameny	pramen	k1gInPc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
obecným	obecný	k2eAgInSc7d1
základem	základ	k1gInSc7
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
i	i	k8xC
etiky	etika	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
mají	mít	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgFnSc4d3
právní	právní	k2eAgFnSc4d1
autoritu	autorita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vybraná	vybraný	k2eAgNnPc1d1
témata	téma	k1gNnPc1
koránu	korán	k1gInSc2
</s>
<s>
Témata	téma	k1gNnPc4
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yRgFnPc7,k3yIgFnPc7
se	se	k3xPyFc4
korán	korán	k1gInSc1
zabývá	zabývat	k5eAaImIp3nS
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nS
základ	základ	k1gInSc4
islámské	islámský	k2eAgFnSc2d1
věrouky	věrouka	k1gFnSc2
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
jinak	jinak	k6eAd1
významná	významný	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Eschatologie	eschatologie	k1gFnSc1
</s>
<s>
Koránská	Koránský	k2eAgFnSc1d1
eschatologie	eschatologie	k1gFnSc1
je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
především	především	k9
na	na	k7c4
vypočítávání	vypočítávání	k1gNnSc4
požitků	požitek	k1gInPc2
ráje	ráj	k1gInSc2
a	a	k8xC
utrpení	utrpení	k1gNnSc2
v	v	k7c6
pekle	peklo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verše	verš	k1gInSc2
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
barvité	barvitý	k2eAgFnPc1d1
a	a	k8xC
nápadité	nápaditý	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Podle	podle	k7c2
koránu	korán	k1gInSc2
budou	být	k5eAaImBp3nP
v	v	k7c4
den	den	k1gInSc4
posledního	poslední	k2eAgInSc2d1
soudu	soud	k1gInSc2
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
rozděleni	rozdělit	k5eAaPmNgMnP
podle	podle	k7c2
svých	svůj	k3xOyFgInPc2
skutků	skutek	k1gInPc2
na	na	k7c4
tři	tři	k4xCgFnPc4
skupiny	skupina	k1gFnPc4
(	(	kIx(
<g/>
56	#num#	k4
<g/>
:	:	kIx,
<g/>
7	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pravici	pravice	k1gFnSc6
budou	být	k5eAaImBp3nP
stát	stát	k5eAaPmF,k5eAaImF
ti	ten	k3xDgMnPc1
věřící	věřící	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
za	za	k7c2
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
konali	konat	k5eAaImAgMnP
dobré	dobrý	k2eAgInPc4d1
skutky	skutek	k1gInPc4
(	(	kIx(
<g/>
a	a	k8xC
opakovaně	opakovaně	k6eAd1
je	být	k5eAaImIp3nS
zdůrazňováno	zdůrazňovat	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
nestačí	stačit	k5eNaBmIp3nS
být	být	k5eAaImF
ani	ani	k8xC
pouze	pouze	k6eAd1
věřící	věřící	k2eAgMnPc1d1
<g/>
,	,	kIx,
ani	ani	k8xC
pouze	pouze	k6eAd1
dobrý	dobrý	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
po	po	k7c6
levici	levice	k1gFnSc6
pak	pak	k6eAd1
nevěřící	věřící	k2eNgInSc4d1
a	a	k8xC
hříšníci	hříšník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetí	třetí	k4xOgFnSc6
skupině	skupina	k1gFnSc6
jsou	být	k5eAaImIp3nP
„	„	k?
<g/>
předáci	předák	k1gMnPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
význam	význam	k1gInSc1
zmínky	zmínka	k1gFnSc2
o	o	k7c6
nich	on	k3xPp3gNnPc6
je	být	k5eAaImIp3nS
nejasný	jasný	k2eNgInSc1d1
<g/>
,	,	kIx,
snad	snad	k9
má	mít	k5eAaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
posly	posel	k1gMnPc4
a	a	k8xC
proroky	prorok	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
budou	být	k5eAaImBp3nP
zvlášť	zvlášť	k6eAd1
blízko	blízko	k6eAd1
Bohu	bůh	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Líčení	líčení	k1gNnSc1
pekelných	pekelný	k2eAgNnPc2d1
utrpení	utrpení	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
budou	být	k5eAaImBp3nP
trvat	trvat	k5eAaImF
věčně	věčně	k6eAd1
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
spojováno	spojovat	k5eAaImNgNnS
s	s	k7c7
bolestí	bolest	k1gFnSc7
<g/>
,	,	kIx,
řetězem	řetěz	k1gInSc7
a	a	k8xC
ohněm	oheň	k1gInSc7
(	(	kIx(
<g/>
súra	súra	k1gFnSc1
88	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
též	též	k9
s	s	k7c7
utrpením	utrpení	k1gNnSc7
žízní	žízeň	k1gFnSc7
a	a	k8xC
hladem	hlad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
v	v	k7c6
ráji	ráj	k1gInSc6
se	se	k3xPyFc4
věřící	věřící	k2eAgMnPc1d1
mají	mít	k5eAaImIp3nP
dočkat	dočkat	k5eAaPmF
zasloužené	zasloužený	k2eAgFnPc4d1
odměny	odměna	k1gFnPc4
<g/>
,	,	kIx,
potečou	téct	k5eAaImIp3nP
zde	zde	k6eAd1
řeky	řeka	k1gFnPc1
chladivé	chladivý	k2eAgFnPc1d1
<g/>
,	,	kIx,
dostatek	dostatek	k1gInSc1
bude	být	k5eAaImBp3nS
všeho	všecek	k3xTgNnSc2
jídla	jídlo	k1gNnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
ovoce	ovoce	k1gNnSc1
(	(	kIx(
<g/>
súra	súra	k1gFnSc1
78	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevšední	všední	k2eNgFnSc1d1
pozornost	pozornost	k1gFnSc1
Muhammad	Muhammad	k1gInSc1
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
ráji	ráj	k1gInSc6
nebude	být	k5eNaImBp3nS
nikoho	nikdo	k3yNnSc2
obtěžovat	obtěžovat	k5eAaImF
prázdné	prázdný	k2eAgNnSc4d1
tlachání	tlachání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejspornějším	sporný	k2eAgNnSc7d3
podobenstvím	podobenství	k1gNnSc7
ráje	ráj	k1gInSc2
je	být	k5eAaImIp3nS
představa	představa	k1gFnSc1
tzv.	tzv.	kA
hurisek	huriska	k1gFnPc2
<g/>
,	,	kIx,
húrí	húr	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
„	„	k?
<g/>
dívky	dívka	k1gFnPc4
s	s	k7c7
plnými	plný	k2eAgNnPc7d1
ňadry	ňadro	k1gNnPc7
<g/>
“	“	k?
(	(	kIx(
<g/>
78	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
jednou	jeden	k4xCgFnSc7
z	z	k7c2
odměn	odměna	k1gFnPc2
věřícím	věřící	k2eAgNnSc7d1
<g/>
;	;	kIx,
verš	verš	k1gInSc1
56	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
je	být	k5eAaImIp3nS
vykládán	vykládat	k5eAaImNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc1
panenství	panenství	k1gNnSc1
bude	být	k5eAaImBp3nS
neustále	neustále	k6eAd1
obnovovat	obnovovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
věřící	věřící	k2eAgMnPc1d1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
v	v	k7c6
ráji	ráj	k1gInSc6
stejného	stejný	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
,	,	kIx,
podle	podle	k7c2
tradice	tradice	k1gFnSc2
jim	on	k3xPp3gMnPc3
bude	být	k5eAaImBp3nS
33	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalších	další	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
již	již	k6eAd1
není	být	k5eNaImIp3nS
o	o	k7c6
huriskách	huriska	k1gFnPc6
řeč	řeč	k1gFnSc1
<g/>
,	,	kIx,
místo	místo	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
při	při	k7c6
líčení	líčení	k1gNnSc6
posmrtného	posmrtný	k2eAgInSc2d1
života	život	k1gInSc2
v	v	k7c6
ráji	ráj	k1gInSc6
vždy	vždy	k6eAd1
objevují	objevovat	k5eAaImIp3nP
manželky	manželka	k1gFnPc1
věřících	věřící	k1gMnPc2
(	(	kIx(
<g/>
36	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
tak	tak	k6eAd1
zatímco	zatímco	k8xS
verš	verš	k1gInSc1
78	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
naznačuje	naznačovat	k5eAaImIp3nS
blízkost	blízkost	k1gFnSc4
konce	konec	k1gInSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
již	již	k6eAd1
je	být	k5eAaImIp3nS
korán	korán	k1gInSc1
méně	málo	k6eAd2
jednoznačný	jednoznačný	k2eAgInSc1d1
a	a	k8xC
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c6
této	tento	k3xDgFnSc6
otázce	otázka	k1gFnSc6
je	být	k5eAaImIp3nS
zpraven	zpraven	k2eAgMnSc1d1
pouze	pouze	k6eAd1
Bůh	bůh	k1gMnSc1
sám	sám	k3xTgMnSc1
(	(	kIx(
<g/>
72	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Satanské	satanský	k2eAgInPc1d1
verše	verš	k1gInPc1
a	a	k8xC
nasch	nasch	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Satanské	satanský	k2eAgInPc4d1
verše	verš	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Základ	základ	k1gInSc1
teorie	teorie	k1gFnSc2
o	o	k7c6
verších	verš	k1gInPc6
zrušených	zrušený	k2eAgInPc6d1
a	a	k8xC
zrušujících	zrušující	k2eAgInPc6d1
–	–	k?
nasch	nasch	k1gInSc1
–	–	k?
byl	být	k5eAaImAgInS
položen	položit	k5eAaPmNgInS
odvoláním	odvolání	k1gNnSc7
tzv.	tzv.	kA
satanských	satanský	k2eAgInPc2d1
veršů	verš	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
původně	původně	k6eAd1
umístěny	umístit	k5eAaPmNgInP
v	v	k7c6
53	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
–	–	k?
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polyteismus	polyteismus	k1gInSc1
připouštějící	připouštějící	k2eAgInPc1d1
verše	verš	k1gInPc1
byly	být	k5eAaImAgInP
odvolány	odvolat	k5eAaPmNgInP
a	a	k8xC
nahrazeny	nahradit	k5eAaPmNgInP
verši	verš	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
naopak	naopak	k6eAd1
na	na	k7c6
mnohobožství	mnohobožství	k1gNnSc6
tvrdě	tvrdě	k6eAd1
útočí	útočit	k5eAaImIp3nS
<g/>
,	,	kIx,
využívajíce	využívat	k5eAaImSgFnP
k	k	k7c3
tomu	ten	k3xDgMnSc3
staroarabskou	staroarabský	k2eAgFnSc4d1
představu	představa	k1gFnSc4
bohyň	bohyně	k1gFnPc2
ženského	ženský	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Verš	verš	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
100	#num#	k4
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
každý	každý	k3xTgInSc4
zapomenutý	zapomenutý	k2eAgInSc4d1
či	či	k8xC
zrušený	zrušený	k2eAgInSc4d1
verš	verš	k1gInSc4
je	být	k5eAaImIp3nS
seslán	seslán	k2eAgMnSc1d1
jiný	jiný	k2eAgMnSc1d1
a	a	k8xC
lepší	dobrý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
22	#num#	k4
<g/>
:	:	kIx,
<g/>
51	#num#	k4
jde	jít	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
konstrukce	konstrukce	k1gFnSc1
tak	tak	k6eAd1
daleko	daleko	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
prý	prý	k9
v	v	k7c4
poselství	poselství	k1gNnSc4
každého	každý	k3xTgMnSc2
proroka	prorok	k1gMnSc2
byla	být	k5eAaImAgFnS
část	část	k1gFnSc1
podvržena	podvrhnout	k5eAaPmNgFnS
satanem	satan	k1gInSc7
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
později	pozdě	k6eAd2
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
odvolána	odvolat	k5eAaPmNgFnS
a	a	k8xC
bylo	být	k5eAaImAgNnS
odhaleno	odhalit	k5eAaPmNgNnS
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
skutečně	skutečně	k6eAd1
pevný	pevný	k2eAgInSc1d1
ve	v	k7c6
víře	víra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jasně	jasně	k6eAd1
se	se	k3xPyFc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
verš	verš	k1gInSc1
13	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
(	(	kIx(
<g/>
Alláh	Alláh	k1gMnSc1
<g/>
)	)	kIx)
vymazává	vymazávat	k5eAaImIp3nS
i	i	k8xC
potvrzuje	potvrzovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
chce	chtít	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
u	u	k7c2
Něho	on	k3xPp3gNnSc2
je	být	k5eAaImIp3nS
kniha	kniha	k1gFnSc1
původní	původní	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Kultovní	kultovní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
</s>
<s>
Kultovní	kultovní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
islámu	islám	k1gInSc2
částečně	částečně	k6eAd1
vycházejí	vycházet	k5eAaImIp3nP
z	z	k7c2
koránu	korán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víra	víra	k1gFnSc1
v	v	k7c4
Boha	bůh	k1gMnSc4
je	být	k5eAaImIp3nS
prokazovaná	prokazovaný	k2eAgFnSc1d1
skutky	skutek	k1gInPc1
lásky	láska	k1gFnSc2
k	k	k7c3
bližnímu	bližní	k1gMnSc3
a	a	k8xC
kultovními	kultovní	k2eAgInPc7d1
úkony	úkon	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámská	islámský	k2eAgFnSc1d1
teologie	teologie	k1gFnSc1
později	pozdě	k6eAd2
formulovala	formulovat	k5eAaImAgFnS
5	#num#	k4
základních	základní	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
Pět	pět	k4xCc4
pilířů	pilíř	k1gInPc2
islámu	islám	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
některé	některý	k3yIgFnPc4
sekty	sekta	k1gFnPc4
přidávají	přidávat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
šestý	šestý	k4xOgInSc1
pilíř	pilíř	k1gInSc1
džihád	džihád	k1gInSc1
<g/>
.	.	kIx.
<g/>
Muslimové	muslim	k1gMnPc1
se	se	k3xPyFc4
původně	původně	k6eAd1
modlili	modlit	k5eAaImAgMnP
především	především	k9
v	v	k7c6
noci	noc	k1gFnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
nevystavovali	vystavovat	k5eNaImAgMnP
zbytečnému	zbytečný	k2eAgNnSc3d1
pronásledování	pronásledování	k1gNnSc3
nepřátelského	přátelský	k2eNgNnSc2d1
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
koránu	korán	k1gInSc6
se	se	k3xPyFc4
dokonce	dokonce	k9
vybízí	vybízet	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
to	ten	k3xDgNnSc4
muslimové	muslim	k1gMnPc1
se	s	k7c7
zbožností	zbožnost	k1gFnSc7
nepřeháněli	přehánět	k5eNaImAgMnP
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
se	se	k3xPyFc4
prý	prý	k9
modlili	modlit	k5eAaImAgMnP
i	i	k9
celou	celý	k2eAgFnSc4d1
noc	noc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
modliteb	modlitba	k1gFnPc2
byl	být	k5eAaImAgInS
nižší	nízký	k2eAgMnSc1d2
než	než	k8xS
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
začátku	začátek	k1gInSc2
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
přibyla	přibýt	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
a	a	k8xC
třetí	třetí	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učení	učení	k1gNnSc1
o	o	k7c6
pěti	pět	k4xCc6
modlitbách	modlitba	k1gFnPc6
pochází	pocházet	k5eAaImIp3nS
až	až	k6eAd1
z	z	k7c2
období	období	k1gNnSc2
po	po	k7c6
Muhammadově	Muhammadův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesná	přesný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
modlitby	modlitba	k1gFnSc2
není	být	k5eNaImIp3nS
v	v	k7c6
koránu	korán	k1gInSc6
řešena	řešen	k2eAgFnSc1d1
<g/>
,	,	kIx,
Muhammad	Muhammad	k1gInSc1
zanechal	zanechat	k5eAaPmAgInS
odkaz	odkaz	k1gInSc4
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
se	se	k3xPyFc4
lidé	člověk	k1gMnPc1
modlí	modlit	k5eAaImIp3nP
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
viděli	vidět	k5eAaImAgMnP
se	se	k3xPyFc4
modlit	modlit	k5eAaImF
jeho	jeho	k3xOp3gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orientace	orientace	k1gFnSc1
modlitby	modlitba	k1gFnSc2
<g/>
,	,	kIx,
qibla	qiblo	k1gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
původně	původně	k6eAd1
směrem	směr	k1gInSc7
k	k	k7c3
Jeruzalému	Jeruzalém	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
rozkolu	rozkol	k1gInSc6
s	s	k7c7
židy	žid	k1gMnPc7
byla	být	k5eAaImAgFnS
veršem	verš	k1gInSc7
2	#num#	k4
<g/>
:	:	kIx,
<g/>
136	#num#	k4
změněna	změnit	k5eAaPmNgFnS
na	na	k7c4
Mekku	Mekka	k1gFnSc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
rozvinutím	rozvinutí	k1gNnSc7
učení	učení	k1gNnSc2
o	o	k7c6
Abrahámovi	Abrahám	k1gMnSc6
jako	jako	k8xC,k8xS
zakladateli	zakladatel	k1gMnSc6
Ka	Ka	k1gMnSc6
<g/>
'	'	kIx"
<g/>
by	by	kYmCp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
klade	klást	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
velký	velký	k2eAgInSc1d1
důraz	důraz	k1gInSc1
na	na	k7c4
sociální	sociální	k2eAgInPc4d1
aspekty	aspekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přikazuje	přikazovat	k5eAaImIp3nS
dávat	dávat	k5eAaImF
almužnu	almužna	k1gFnSc4
<g/>
,	,	kIx,
pomáhat	pomáhat	k5eAaImF
nemajetným	majetný	k2eNgMnSc7d1
<g/>
,	,	kIx,
sirotkům	sirotek	k1gMnPc3
<g/>
,	,	kIx,
starým	starý	k1gMnPc3
a	a	k8xC
nemocným	nemocný	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
povinnost	povinnost	k1gFnSc1
<g/>
,	,	kIx,
zakát	zakát	k1gInSc1
(	(	kIx(
<g/>
očista	očista	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
postupně	postupně	k6eAd1
změnila	změnit	k5eAaPmAgFnS
v	v	k7c4
náboženskou	náboženský	k2eAgFnSc4d1
daň	daň	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
vůdcové	vůdce	k1gMnPc1
náboženské	náboženský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
mohli	moct	k5eAaImAgMnP
využívat	využívat	k5eAaPmF,k5eAaImF
nejen	nejen	k6eAd1
na	na	k7c4
sociální	sociální	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
státu	stát	k1gInSc2
a	a	k8xC
na	na	k7c4
vojenské	vojenský	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společným	společný	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
všech	všecek	k3xTgNnPc2
monoteistických	monoteistický	k2eAgNnPc2d1
náboženství	náboženství	k1gNnPc2
je	být	k5eAaImIp3nS
půst	půst	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
koránu	korán	k1gInSc6
rozveden	rozvést	k5eAaPmNgInS
a	a	k8xC
určen	určit	k5eAaPmNgInS
na	na	k7c4
měsíc	měsíc	k1gInSc4
ramadán	ramadán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půst	půst	k1gInSc1
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
od	od	k7c2
východu	východ	k1gInSc2
do	do	k7c2
západu	západ	k1gInSc2
slunce	slunce	k1gNnSc2
a	a	k8xC
nemocní	nemocný	k1gMnPc1
<g/>
,	,	kIx,
cestující	cestující	k1gMnPc1
a	a	k8xC
další	další	k2eAgMnPc1d1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nemohou	moct	k5eNaImIp3nP
běžně	běžně	k6eAd1
držet	držet	k5eAaImF
půst	půst	k1gInSc4
<g/>
,	,	kIx,
jej	on	k3xPp3gInSc4
mohou	moct	k5eAaImIp3nP
odložit	odložit	k5eAaPmF
na	na	k7c4
jiný	jiný	k2eAgInSc4d1
termín	termín	k1gInSc4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
180	#num#	k4
<g/>
–	–	k?
<g/>
181	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
náboženskou	náboženský	k2eAgFnSc7d1
povinností	povinnost	k1gFnSc7
je	být	k5eAaImIp3nS
pouť	pouť	k1gFnSc1
do	do	k7c2
Mekky	Mekka	k1gFnSc2
(	(	kIx(
<g/>
velká	velký	k2eAgFnSc1d1
a	a	k8xC
malá	malý	k2eAgFnSc1d1
pouť	pouť	k1gFnSc1
<g/>
,	,	kIx,
hadždž	hadždž	k6eAd1
a	a	k8xC
'	'	kIx"
<g/>
umra	umra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zvyk	zvyk	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
doby	doba	k1gFnSc2
prvních	první	k4xOgMnPc2
proroků	prorok	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vystoupení	vystoupení	k1gNnSc6
Muhammada	Muhammada	k1gFnSc1
se	se	k3xPyFc4
mnozí	mnohý	k2eAgMnPc1d1
Mekkánci	Mekkánek	k1gMnPc1
obávali	obávat	k5eAaImAgMnP
o	o	k7c4
osud	osud	k1gInSc4
tohoto	tento	k3xDgInSc2
zvyku	zvyk	k1gInSc2
a	a	k8xC
svých	svůj	k3xOyFgInPc2
příjmů	příjem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
pouť	pouť	k1gFnSc4
zachovává	zachovávat	k5eAaImIp3nS
<g/>
,	,	kIx,
vrací	vracet	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
však	však	k9
původní	původní	k2eAgInSc4d1
smysl	smysl	k1gInSc4
<g/>
,	,	kIx,
tj.	tj.	kA
uctívání	uctívání	k1gNnSc2
jednoho	jeden	k4xCgMnSc2
Boha	bůh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mekkánská	Mekkánský	k2eAgFnSc1d1
modlitebna	modlitebna	k1gFnSc1
Ka	Ka	k1gFnSc2
<g/>
'	'	kIx"
<g/>
aba	aba	k?
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
hanífem	haníf	k1gMnSc7
Abrahámem	Abrahám	k1gMnSc7
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
symbolem	symbol	k1gInSc7
nezkaženého	zkažený	k2eNgInSc2d1
původního	původní	k2eAgInSc2d1
monoteismu	monoteismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdůrazňován	zdůrazňován	k2eAgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
kult	kult	k1gInSc1
Ismaela	Ismael	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholem	vrchol	k1gInSc7
pouti	pouť	k1gFnSc3
je	být	k5eAaImIp3nS
rituální	rituální	k2eAgNnSc1d1
obětování	obětování	k1gNnSc1
zvířat	zvíře	k1gNnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
i	i	k9
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemá	mít	k5eNaImIp3nS
to	ten	k3xDgNnSc4
však	však	k9
být	být	k5eAaImF
oběť	oběť	k1gFnSc4
pro	pro	k7c4
Boha	bůh	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
výraz	výraz	k1gInSc1
zbožnosti	zbožnost	k1gFnSc2
–	–	k?
oběti	oběť	k1gFnPc1
jsou	být	k5eAaImIp3nP
rozdány	rozdat	k5eAaPmNgFnP
nemajetným	majetný	k2eNgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koránská	Koránský	k2eAgFnSc1d1
pravidla	pravidlo	k1gNnSc2
stravování	stravování	k1gNnSc2
jsou	být	k5eAaImIp3nP
o	o	k7c4
poznání	poznání	k1gNnSc4
volnější	volný	k2eAgFnSc1d2
než	než	k8xS
pravidla	pravidlo	k1gNnPc1
židovská	židovský	k2eAgNnPc1d1
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
i	i	k9
než	než	k8xS
původní	původní	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
arabská	arabský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jídel	jídlo	k1gNnPc2
je	být	k5eAaImIp3nS
zakázáno	zakázat	k5eAaPmNgNnS
pouze	pouze	k6eAd1
vepřové	vepřový	k2eAgNnSc1d1
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
zdechliny	zdechlina	k1gFnPc1
a	a	k8xC
maso	maso	k1gNnSc1
zvířat	zvíře	k1gNnPc2
zasvěcených	zasvěcený	k2eAgFnPc2d1
jinému	jiný	k2eAgMnSc3d1
bohu	bůh	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
naopak	naopak	k6eAd1
zcela	zcela	k6eAd1
zakazuje	zakazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
hazardními	hazardní	k2eAgFnPc7d1
hrami	hra	k1gFnPc7
<g/>
,	,	kIx,
víno	víno	k1gNnSc4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
216	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
:	:	kIx,
<g/>
46	#num#	k4
a	a	k8xC
5	#num#	k4
<g/>
:	:	kIx,
<g/>
92	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zákaz	zákaz	k1gInSc1
byl	být	k5eAaImAgInS
přenesen	přenést	k5eAaPmNgInS
na	na	k7c4
alkoholické	alkoholický	k2eAgInPc4d1
nápoje	nápoj	k1gInPc4
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavé	zajímavý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
starším	starý	k2eAgInSc6d2
verši	verš	k1gInSc6
16	#num#	k4
<g/>
:	:	kIx,
<g/>
67	#num#	k4
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
existence	existence	k1gFnSc1
skvělého	skvělý	k2eAgInSc2d1
nápoje	nápoj	k1gInSc2
vína	víno	k1gNnSc2
ukazována	ukazovat	k5eAaImNgNnP
jako	jako	k8xS,k8xC
důkaz	důkaz	k1gInSc1
Boží	boží	k2eAgFnSc2d1
dobrotivosti	dobrotivost	k1gFnSc2
<g/>
,	,	kIx,
objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
ale	ale	k9
<g/>
,	,	kIx,
že	že	k8xS
víno	víno	k1gNnSc1
má	mít	k5eAaImIp3nS
dobré	dobrý	k2eAgNnSc1d1
i	i	k9
špatné	špatný	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vztah	vztah	k1gInSc1
k	k	k7c3
národům	národ	k1gInPc3
Knihy	kniha	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Korán	korán	k1gInSc1
o	o	k7c6
židech	žid	k1gMnPc6
a	a	k8xC
křesťanech	křesťan	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
Muhammada	Muhammada	k1gFnSc1
k	k	k7c3
židům	žid	k1gMnPc3
a	a	k8xC
křesťanům	křesťan	k1gMnPc3
prošel	projít	k5eAaPmAgInS
poměrně	poměrně	k6eAd1
dramatickým	dramatický	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
odrazem	odraz	k1gInSc7
je	být	k5eAaImIp3nS
koránský	koránský	k2eAgInSc1d1
text	text	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
začátku	začátek	k1gInSc2
se	se	k3xPyFc4
Muhammad	Muhammad	k1gInSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
hlasatele	hlasatel	k1gMnPc4
názorů	názor	k1gInPc2
těchto	tento	k3xDgInPc2
monoteistických	monoteistický	k2eAgInPc2d1
systémů	systém	k1gInPc2
jako	jako	k8xS,k8xC
protikladu	protiklad	k1gInSc2
k	k	k7c3
realitě	realita	k1gFnSc3
předislámské	předislámský	k2eAgFnSc2d1
polyteistické	polyteistický	k2eAgFnSc2d1
Mekky	Mekka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odchodu	odchod	k1gInSc6
do	do	k7c2
Medíny	Medína	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
žilo	žít	k5eAaImAgNnS
několik	několik	k4yIc1
židovských	židovský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
očekával	očekávat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
podrobí	podrobit	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc3
autoritě	autorita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
nestalo	stát	k5eNaPmAgNnS
–	–	k?
židé	žid	k1gMnPc1
jeho	jeho	k3xOp3gNnSc4
proroctví	proroctví	k1gNnSc4
zpochybňovali	zpochybňovat	k5eAaImAgMnP
a	a	k8xC
dokonce	dokonce	k9
se	se	k3xPyFc4
spojili	spojit	k5eAaPmAgMnP
s	s	k7c7
jeho	jeho	k3xOp3gMnPc7
nepřáteli	nepřítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
těmto	tento	k3xDgInPc3
židovským	židovský	k2eAgInPc3d1
kmenům	kmen	k1gInPc3
Muhammad	Muhammad	k1gInSc4
později	pozdě	k6eAd2
nařídil	nařídit	k5eAaPmAgMnS
vojenské	vojenský	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Seznam	seznam	k1gInSc4
Muhammadových	Muhammadový	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
expedicí	expedice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozchod	rozchod	k1gInSc1
se	s	k7c7
židy	žid	k1gMnPc7
je	být	k5eAaImIp3nS
završen	završit	k5eAaPmNgInS
změnou	změna	k1gFnSc7
kultovních	kultovní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
i	i	k8xC
rozpracováním	rozpracování	k1gNnSc7
představy	představa	k1gFnSc2
o	o	k7c6
původním	původní	k2eAgInSc6d1
monoteismu	monoteismus	k1gInSc6
Abraháma	Abrahám	k1gMnSc2
(	(	kIx(
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
120	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
nazván	nazván	k2eAgMnSc1d1
„	„	k?
<g/>
hanífou	hanífou	k6eAd1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
spojenectví	spojenectví	k1gNnSc4
s	s	k7c7
křesťany	křesťan	k1gMnPc7
nakonec	nakonec	k6eAd1
padá	padat	k5eAaImIp3nS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
koránu	korán	k1gInSc6
objevují	objevovat	k5eAaImIp3nP
útoky	útok	k1gInPc1
na	na	k7c4
křesťanské	křesťanský	k2eAgNnSc4d1
učení	učení	k1gNnSc4
o	o	k7c6
Trojici	trojice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Přes	přes	k7c4
to	ten	k3xDgNnSc4
všechno	všechen	k3xTgNnSc4
je	být	k5eAaImIp3nS
tón	tón	k1gInSc1
koránu	korán	k1gInSc2
k	k	k7c3
„	„	k?
<g/>
národům	národ	k1gInPc3
Knihy	kniha	k1gFnSc2
<g/>
“	“	k?
většinou	většinou	k6eAd1
příznivější	příznivý	k2eAgMnSc1d2
<g/>
;	;	kIx,
nepatří	patřit	k5eNaImIp3nP
mezi	mezi	k7c4
zásadní	zásadní	k2eAgMnPc4d1
nepřátele	nepřítel	k1gMnPc4
jako	jako	k9
modloslužebníci	modloslužebník	k1gMnPc1
a	a	k8xC
nevěřící	nevěřící	k1gMnPc1
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
některé	některý	k3yIgInPc1
skutky	skutek	k1gInPc1
a	a	k8xC
činy	čin	k1gInPc1
je	být	k5eAaImIp3nS
do	do	k7c2
této	tento	k3xDgFnSc2
kategorie	kategorie	k1gFnSc2
mohou	moct	k5eAaImIp3nP
uvrhnout	uvrhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konverzi	konverze	k1gFnSc3
nemají	mít	k5eNaImIp3nP
být	být	k5eAaImF
donucováni	donucovat	k5eAaImNgMnP
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
nevěřících	nevěřící	k1gMnPc2
či	či	k8xC
polyteistů	polyteista	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
podrobit	podrobit	k5eAaPmF
pravidlům	pravidlo	k1gNnPc3
islámské	islámský	k2eAgFnPc4d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Etika	etika	k1gFnSc1
a	a	k8xC
právo	právo	k1gNnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojována	ozdrojován	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Jak	jak	k6eAd1
bylo	být	k5eAaImAgNnS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
islám	islám	k1gInSc1
sjednocuje	sjednocovat	k5eAaImIp3nS
v	v	k7c6
sobě	se	k3xPyFc3
veškeré	veškerý	k3xTgInPc4
aspekty	aspekt	k1gInPc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náboženství	náboženství	k1gNnSc1
<g/>
,	,	kIx,
etika	etika	k1gFnSc1
a	a	k8xC
právo	právo	k1gNnSc4
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
pevně	pevně	k6eAd1
spojené	spojený	k2eAgInPc4d1
pojmy	pojem	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islám	islám	k1gInSc1
nahrazuje	nahrazovat	k5eAaImIp3nS
místní	místní	k2eAgMnSc1d1
z	z	k7c2
pojetí	pojetí	k1gNnSc2
beduínských	beduínský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
bratrských	bratrský	k2eAgNnPc2d1
společenství	společenství	k1gNnPc2
bez	bez	k7c2
nerovnosti	nerovnost	k1gFnSc2
a	a	k8xC
nahrazuje	nahrazovat	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
bratrstvím	bratrství	k1gNnSc7
celé	celá	k1gFnSc2
muslimské	muslimský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
ummy	umma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
také	také	k9
islám	islám	k1gInSc1
veskrze	veskrze	k6eAd1
neindividualistický	individualistický	k2eNgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nejjasněji	jasně	k6eAd3
vyjádřeno	vyjádřit	k5eAaPmNgNnS
kolektivní	kolektivní	k2eAgFnSc7d1
povinností	povinnost	k1gFnSc7
všech	všecek	k3xTgMnPc2
muslimů	muslim	k1gMnPc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
postavení	postavení	k1gNnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
přikazuj	přikazovat	k5eAaImRp2nS
vhodné	vhodný	k2eAgNnSc4d1
a	a	k8xC
zakazuj	zakazovat	k5eAaImRp2nS
zavrženíhodné	zavrženíhodný	k2eAgFnPc4d1
<g/>
“	“	k?
(	(	kIx(
<g/>
31	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
chce	chtít	k5eAaImIp3nS
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
si	se	k3xPyFc3
v	v	k7c6
koránu	korán	k1gInSc6
najít	najít	k5eAaPmF
i	i	k9
verše	verš	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
naznačují	naznačovat	k5eAaImIp3nP
soukromí	soukromí	k1gNnSc4
svědomí	svědomí	k1gNnSc2
před	před	k7c7
Bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
verších	verš	k1gInPc6
11	#num#	k4
<g/>
:	:	kIx,
<g/>
87	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
:	:	kIx,
<g/>
68	#num#	k4
a	a	k8xC
160	#num#	k4
a	a	k8xC
velmi	velmi	k6eAd1
jasně	jasně	k6eAd1
ve	v	k7c6
42	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
se	se	k3xPyFc4
praví	pravit	k5eAaBmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
prorok	prorok	k1gMnSc1
není	být	k5eNaImIp3nS
strážcem	strážce	k1gMnSc7
nad	nad	k7c7
lidem	lid	k1gInSc7
svým	svůj	k3xOyFgInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vyloženo	vyložit	k5eAaPmNgNnS
i	i	k9
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
teprve	teprve	k6eAd1
Bůh	bůh	k1gMnSc1
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc4
rozsoudit	rozsoudit	k5eAaPmF
poctivé	poctivý	k2eAgInPc4d1
a	a	k8xC
nepoctivé	poctivý	k2eNgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutno	nutno	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
odmítnout	odmítnout	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
islám	islám	k1gInSc1
popíral	popírat	k5eAaImAgInS
hierarchii	hierarchie	k1gFnSc4
vlády	vláda	k1gFnSc2
i	i	k8xC
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatně	ostatně	k6eAd1
v	v	k7c6
čele	čelo	k1gNnSc6
ummy	umma	k1gFnSc2
stál	stát	k5eAaImAgMnS
Prorok	prorok	k1gMnSc1
a	a	k8xC
následné	následný	k2eAgNnSc1d1
ustanovení	ustanovení	k1gNnSc1
chalífátu	chalífát	k1gInSc2
není	být	k5eNaImIp3nS
s	s	k7c7
islámským	islámský	k2eAgNnSc7d1
učením	učení	k1gNnSc7
v	v	k7c6
rozporu	rozpor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedošlo	dojít	k5eNaPmAgNnS
ani	ani	k8xC
ke	k	k7c3
zrušení	zrušení	k1gNnSc3
otroctví	otroctví	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
propouštění	propouštění	k1gNnSc1
věřících	věřící	k2eAgMnPc2d1
otroků	otrok	k1gMnPc2
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
dobrý	dobrý	k2eAgInSc4d1
skutek	skutek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
de	de	k?
facto	fact	k2eAgNnSc1d1
otroctví	otroctví	k1gNnSc1
časem	časem	k6eAd1
vytratilo	vytratit	k5eAaPmAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
lidé	člověk	k1gMnPc1
své	svůj	k3xOyFgMnPc4
otroky	otrok	k1gMnPc4
propouštěli	propouštět	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
společnost	společnost	k1gFnSc4
nenastal	nastat	k5eNaPmAgInS
žádný	žádný	k3yNgInSc1
ekonomický	ekonomický	k2eAgInSc1d1
šok	šok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1
věřící	věřící	k1gMnPc1
po	po	k7c6
Muhammadovi	Muhammada	k1gMnSc6
požadovali	požadovat	k5eAaImAgMnP
jakési	jakýsi	k3yIgFnSc3
islámské	islámský	k2eAgNnSc1d1
„	„	k?
<g/>
Desatero	desatero	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Takto	takto	k6eAd1
formulovaného	formulovaný	k2eAgInSc2d1
seznamu	seznam	k1gInSc2
pravidel	pravidlo	k1gNnPc2
se	se	k3xPyFc4
nedočkali	dočkat	k5eNaPmAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c4
dobrý	dobrý	k2eAgInSc4d1
základ	základ	k1gInSc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
považovány	považován	k2eAgInPc1d1
verše	verš	k1gInPc1
17	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
–	–	k?
<g/>
41	#num#	k4
či	či	k8xC
zkráceně	zkráceně	k6eAd1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
152	#num#	k4
<g/>
–	–	k?
<g/>
154	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Korán	korán	k1gInSc1
(	(	kIx(
<g/>
až	až	k9
na	na	k7c4
zabití	zabití	k1gNnSc4
<g/>
,	,	kIx,
krádež	krádež	k1gFnSc4
a	a	k8xC
nevěru	nevěra	k1gFnSc4
<g/>
)	)	kIx)
nestanovuje	stanovovat	k5eNaImIp3nS
za	za	k7c2
porušení	porušení	k1gNnSc2
svých	svůj	k3xOyFgNnPc2
pravidel	pravidlo	k1gNnPc2
žádné	žádný	k3yNgInPc4
tresty	trest	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
islámském	islámský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
vyšším	vysoký	k2eAgInSc6d2
významu	význam	k1gInSc6
svědomí	svědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
korán	korán	k1gInSc4
není	být	k5eNaImIp3nS
jako	jako	k9
bible	bible	k1gFnSc1
(	(	kIx(
<g/>
respektive	respektive	k9
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
neobsahuje	obsahovat	k5eNaImIp3nS
jen	jen	k9
obecná	obecný	k2eAgNnPc4d1
mravní	mravní	k2eAgNnPc4d1
poselství	poselství	k1gNnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
řadu	řada	k1gFnSc4
velmi	velmi	k6eAd1
konkrétních	konkrétní	k2eAgNnPc2d1
pojednání	pojednání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samozřejmě	samozřejmě	k6eAd1
že	že	k8xS
i	i	k9
verše	verš	k1gInSc2
obecnějšího	obecní	k2eAgInSc2d2
rázu	ráz	k1gInSc2
zde	zde	k6eAd1
nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důraz	důraz	k1gInSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
kladen	klást	k5eAaImNgInS
na	na	k7c4
sociální	sociální	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
(	(	kIx(
<g/>
sirotci	sirotek	k1gMnPc1
<g/>
,	,	kIx,
chudina	chudina	k1gMnSc1
<g/>
,	,	kIx,
nemocní	mocnit	k5eNaImIp3nS
<g/>
)	)	kIx)
či	či	k8xC
na	na	k7c4
slušnost	slušnost	k1gFnSc4
k	k	k7c3
ostatním	ostatní	k2eAgMnPc3d1
<g/>
,	,	kIx,
laskavost	laskavost	k1gFnSc1
k	k	k7c3
rodině	rodina	k1gFnSc3
<g/>
,	,	kIx,
dodržování	dodržování	k1gNnSc3
smluv	smlouva	k1gFnPc2
<g/>
,	,	kIx,
čestnost	čestnost	k1gFnSc4
<g/>
,	,	kIx,
upřímnost	upřímnost	k1gFnSc4
a	a	k8xC
pravdomluvnost	pravdomluvnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Poctivost	poctivost	k1gFnSc1
v	v	k7c6
obchodě	obchod	k1gInSc6
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
důležitých	důležitý	k2eAgInPc2d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
přikázání	přikázání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc1
<g/>
,	,	kIx,
původem	původ	k1gInSc7
obchodník	obchodník	k1gMnSc1
<g/>
,	,	kIx,
opakovaně	opakovaně	k6eAd1
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
poctivost	poctivost	k1gFnSc4
v	v	k7c4
dávání	dávání	k1gNnSc4
měr	míra	k1gFnPc2
a	a	k8xC
vah	váha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitější	důležitý	k2eAgInSc1d2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
jsou	být	k5eAaImIp3nP
však	však	k9
verše	verš	k1gInPc1
zakazující	zakazující	k2eAgFnSc4d1
lichvu	lichva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnPc1
zmínka	zmínka	k1gFnSc1
o	o	k7c4
ni	on	k3xPp3gFnSc4
je	být	k5eAaImIp3nS
ve	v	k7c6
verši	verš	k1gInSc6
30	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
zde	zde	k6eAd1
ještě	ještě	k9
není	být	k5eNaImIp3nS
zakázána	zakázat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
je	být	k5eAaImIp3nS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
dáváním	dávání	k1gNnSc7
úroku	úrok	k1gInSc2
nezíská	získat	k5eNaPmIp3nS
člověk	člověk	k1gMnSc1
žádného	žádný	k1gMnSc2
úroku	úrok	k1gInSc2
u	u	k7c2
Boha	bůh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definitivní	definitivní	k2eAgInSc1d1
zákaz	zákaz	k1gInSc1
pak	pak	k6eAd1
přichází	přicházet	k5eAaImIp3nS
ve	v	k7c6
verších	verš	k1gInPc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
130	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
275	#num#	k4
<g/>
–	–	k?
<g/>
280	#num#	k4
a	a	k8xC
4	#num#	k4
<g/>
:	:	kIx,
<g/>
161	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
se	se	k3xPyFc4
mnohé	mnohé	k1gNnSc1
muslimské	muslimský	k2eAgFnSc2d1
tradicionalisticky	tradicionalisticky	k6eAd1
nebo	nebo	k8xC
fundamentalisticky	fundamentalisticky	k6eAd1
založené	založený	k2eAgInPc1d1
státy	stát	k1gInPc1
pokoušejí	pokoušet	k5eAaImIp3nP
rozvinout	rozvinout	k5eAaPmF
tzv.	tzv.	kA
islámské	islámský	k2eAgNnSc1d1
bankovnictví	bankovnictví	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
úrok	úrok	k1gInSc1
nahrazuje	nahrazovat	k5eAaImIp3nS
složitými	složitý	k2eAgFnPc7d1
dohodami	dohoda	k1gFnPc7
o	o	k7c4
podílnictví	podílnictví	k1gNnSc4
na	na	k7c6
zisku	zisk	k1gInSc6
apod.	apod.	kA
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1
msta	msta	k1gFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
vykonávána	vykonávat	k5eAaImNgFnS
jedině	jedině	k6eAd1
na	na	k7c6
pachateli	pachatel	k1gMnSc6
samotném	samotný	k2eAgMnSc6d1
<g/>
,	,	kIx,
ne	ne	k9
na	na	k7c6
celém	celý	k2eAgInSc6d1
jeho	jeho	k3xOp3gInSc3
kmeni	kmen	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
verši	verš	k1gInSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
178	#num#	k4
stojí	stát	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
muž	muž	k1gMnSc1
za	za	k7c4
muže	muž	k1gMnPc4
<g/>
,	,	kIx,
otrok	otrok	k1gMnSc1
za	za	k7c2
otroka	otrok	k1gMnSc2
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
za	za	k7c4
ženu	žena	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučována	doporučován	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
umírněnost	umírněnost	k1gFnSc1
a	a	k8xC
přijímání	přijímání	k1gNnSc1
výkupného	výkupné	k1gNnSc2
(	(	kIx(
<g/>
dija	dija	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Dalším	další	k2eAgInSc7d1
prohřeškem	prohřešek	k1gInSc7
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
je	být	k5eAaImIp3nS
stanoven	stanoven	k2eAgInSc1d1
postih	postih	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
krádež	krádež	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
verše	verš	k1gInSc2
5	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
takovémuto	takovýto	k3xDgMnSc3
viníkovi	viník	k1gMnSc3
useknuta	useknout	k5eAaPmNgFnS
ruka	ruka	k1gFnSc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
specifikováno	specifikovat	k5eAaBmNgNnS
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1
hřích	hřích	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
přesný	přesný	k2eAgInSc1d1
trest	trest	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
cizoložství	cizoložství	k1gNnSc1
(	(	kIx(
<g/>
ziná	ziná	k1gFnSc1
<g/>
'	'	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgNnSc3
tématu	téma	k1gNnSc3
se	se	k3xPyFc4
věnují	věnovat	k5eAaPmIp3nP,k5eAaImIp3nP
verše	verš	k1gInPc1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
–	–	k?
<g/>
16	#num#	k4
a	a	k8xC
24	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trestem	trest	k1gInSc7
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
sto	sto	k4xCgNnSc4
ran	rána	k1gFnPc2
bičem	bič	k1gInSc7
oběma	dva	k4xCgMnPc7
viníkům	viník	k1gMnPc3
a	a	k8xC
zákaz	zákaz	k1gInSc4
vstupu	vstup	k1gInSc2
do	do	k7c2
manželství	manželství	k1gNnSc2
s	s	k7c7
věřícím	věřící	k1gMnSc7
(	(	kIx(
<g/>
změkčeno	změkčen	k2eAgNnSc1d1
možností	možnost	k1gFnSc7
pokání	pokání	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgMnSc1
chalífa	chalífa	k1gMnSc1
Umar	Umar	k1gMnSc1
ibn	ibn	k?
al-Chattáb	al-Chattáb	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
slyšel	slyšet	k5eAaImAgMnS
Muhammada	Muhammada	k1gFnSc1
recitovat	recitovat	k5eAaImF
verše	verš	k1gInPc4
o	o	k7c4
kamenování	kamenování	k1gNnSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
přesvědčit	přesvědčit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mýlil	mýlit	k5eAaImAgMnS
a	a	k8xC
v	v	k7c6
koránu	korán	k1gInSc6
se	se	k3xPyFc4
nic	nic	k3yNnSc1
takového	takový	k3xDgNnSc2
neobjevilo	objevit	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
požaduje	požadovat	k5eAaImIp3nS
čtyři	čtyři	k4xCgMnPc4
svědky	svědek	k1gMnPc4
nevěry	nevěra	k1gMnSc2
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
dosáhnout	dosáhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
poněkud	poněkud	k6eAd1
změkčeno	změkčit	k5eAaPmNgNnS
možností	možnost	k1gFnSc7
samotného	samotný	k2eAgNnSc2d1
svědectví	svědectví	k1gNnSc2
manžela	manžel	k1gMnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
pětkrát	pětkrát	k6eAd1
přísahat	přísahat	k5eAaImF
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
ani	ani	k8xC
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
platné	platný	k2eAgNnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
manželka	manželka	k1gFnSc1
pětkrát	pětkrát	k6eAd1
přísahat	přísahat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
pravda	pravda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
postavení	postavení	k1gNnSc1
žen	žena	k1gFnPc2
v	v	k7c6
islámském	islámský	k2eAgNnSc6d1
právu	právo	k1gNnSc6
<g/>
,	,	kIx,
žena	žena	k1gFnSc1
stojí	stát	k5eAaImIp3nS
o	o	k7c4
stupeň	stupeň	k1gInSc4
níže	nízce	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
muž	muž	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
228	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
verši	verš	k1gInSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
vysvětleno	vysvětlit	k5eAaPmNgNnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Bůh	bůh	k1gMnSc1
tak	tak	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgMnS
a	a	k8xC
že	že	k8xS
muž	muž	k1gMnSc1
ženu	žena	k1gFnSc4
živí	živit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
verš	verš	k1gInSc1
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
kontroverzní	kontroverzní	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
A	a	k9
ty	ten	k3xDgFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
neposlušnosti	neposlušnost	k1gFnPc1
se	se	k3xPyFc4
obáváte	obávat	k5eAaImIp2nP
<g/>
,	,	kIx,
varujte	varovat	k5eAaImRp2nP
a	a	k8xC
vykažte	vykázat	k5eAaPmRp2nP
jim	on	k3xPp3gMnPc3
místa	místo	k1gNnPc1
na	na	k7c4
spaní	spaní	k1gNnSc4
a	a	k8xC
bijte	bít	k5eAaImRp2nP
je	on	k3xPp3gNnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
Jestliže	jestliže	k8xS
vás	vy	k3xPp2nPc4
však	však	k9
jsou	být	k5eAaImIp3nP
poslušny	poslušen	k2eAgInPc4d1
<g/>
,	,	kIx,
nevyhledávejte	vyhledávat	k5eNaImRp2nP
proti	proti	k7c3
nim	on	k3xPp3gInPc3
důvody	důvod	k1gInPc7
<g/>
…	…	k?
<g/>
“	“	k?
</s>
<s>
Oproti	oproti	k7c3
minulosti	minulost	k1gFnSc3
je	být	k5eAaImIp3nS
postavení	postavení	k1gNnSc1
žen	žena	k1gFnPc2
výrazně	výrazně	k6eAd1
posíleno	posílen	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Muhammad	Muhammad	k1gInSc1
kritizoval	kritizovat	k5eAaImAgInS
praxi	praxe	k1gFnSc3
zahrabávání	zahrabávání	k1gNnSc1
novorozeňat	novorozeně	k1gNnPc2
ženského	ženský	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
do	do	k7c2
země	zem	k1gFnSc2
z	z	k7c2
obavy	obava	k1gFnSc2
před	před	k7c7
zchudnutím	zchudnutí	k1gNnSc7
(	(	kIx(
<g/>
16	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
nerovné	rovný	k2eNgNnSc4d1
postavení	postavení	k1gNnSc4
žen	žena	k1gFnPc2
především	především	k6eAd1
v	v	k7c6
manželství	manželství	k1gNnSc6
zůstává	zůstávat	k5eAaImIp3nS
<g/>
,	,	kIx,
svědectví	svědectví	k1gNnSc2
ženy	žena	k1gFnSc2
má	mít	k5eAaImIp3nS
také	také	k9
jen	jen	k9
poloviční	poloviční	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
co	co	k9
svědectví	svědectví	k1gNnSc2
muže	muž	k1gMnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
282	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korán	korán	k1gInSc1
velice	velice	k6eAd1
podrobně	podrobně	k6eAd1
rozebírá	rozebírat	k5eAaImIp3nS
dědické	dědický	k2eAgInPc4d1
podíly	podíl	k1gInPc4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
12	#num#	k4
a	a	k8xC
dále	daleko	k6eAd2
<g/>
)	)	kIx)
a	a	k8xC
i	i	k9
zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
ženy	žena	k1gFnPc1
značně	značně	k6eAd1
znevýhodněné	znevýhodněný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
synové	syn	k1gMnPc1
sice	sice	k8xC
mají	mít	k5eAaImIp3nP
dědit	dědit	k5eAaImF
rovné	rovný	k2eAgInPc4d1
podíly	podíl	k1gInPc4
a	a	k8xC
dědí	dědit	k5eAaImIp3nP
i	i	k9
dcery	dcera	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
vdovy	vdova	k1gFnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
dědictví	dědictví	k1gNnSc2
vyloučeny	vyloučen	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
je	být	k5eAaImIp3nS
uváděno	uvádět	k5eAaImNgNnS
<g/>
,	,	kIx,
že	že	k8xS
umírající	umírající	k1gMnSc1
musí	muset	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
manželku	manželka	k1gFnSc4
zaopatřit	zaopatřit	k5eAaPmF
na	na	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
240	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženy	žena	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
mnoha	mnoho	k4c6
verších	verš	k1gInPc6
vybízeny	vybízen	k2eAgFnPc1d1
k	k	k7c3
cudnosti	cudnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verš	verš	k1gInSc1
33	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
je	být	k5eAaImIp3nS
vybízí	vybízet	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
„	„	k?
<g/>
přitahovaly	přitahovat	k5eAaImAgFnP
k	k	k7c3
sobě	se	k3xPyFc3
závoje	závoj	k1gInPc4
své	své	k1gNnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
fundamentalisty	fundamentalista	k1gMnPc4
<g/>
[	[	kIx(
<g/>
kým	kdo	k3yRnSc7,k3yInSc7,k3yQnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
interpretováno	interpretovat	k5eAaBmNgNnS
jako	jako	k9
výzva	výzva	k1gFnSc1
k	k	k7c3
zahalování	zahalování	k1gNnSc3
obličejů	obličej	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravým	pravý	k2eAgNnSc7d1
význam	význam	k1gInSc4
je	být	k5eAaImIp3nS
ale	ale	k8xC
výzva	výzva	k1gFnSc1
k	k	k7c3
nenošení	nenošení	k1gNnSc3
otevřených	otevřený	k2eAgInPc2d1
vyzývavých	vyzývavý	k2eAgInPc2d1
šatů	šat	k1gInPc2
<g/>
,	,	kIx,
zahalování	zahalování	k1gNnSc1
je	být	k5eAaImIp3nS
výmyslem	výmysl	k1gInSc7
až	až	k9
pozdějších	pozdní	k2eAgFnPc2d2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
ostatně	ostatně	k6eAd1
hovoří	hovořit	k5eAaImIp3nS
verše	verš	k1gInPc4
24	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
doporučují	doporučovat	k5eAaImIp3nP
zahalování	zahalování	k1gNnSc4
ňader	ňadro	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
zachovanou	zachovaný	k2eAgFnSc7d1
praxí	praxe	k1gFnSc7
je	být	k5eAaImIp3nS
mnohoženství	mnohoženství	k1gNnSc1
<g/>
,	,	kIx,
korán	korán	k1gInSc1
naopak	naopak	k6eAd1
zakazuje	zakazovat	k5eAaImIp3nS
polyandrii	polyandrie	k1gFnSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
mnohomužství	mnohomužství	k1gNnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muž	muž	k1gMnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
až	až	k6eAd1
čtyři	čtyři	k4xCgFnPc1
manželky	manželka	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
musí	muset	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
věnovat	věnovat	k5eAaPmF,k5eAaImF
naprosto	naprosto	k6eAd1
stejnou	stejný	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
verši	verš	k1gInSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
128	#num#	k4
označeno	označit	k5eAaPmNgNnS
za	za	k7c4
nemožné	možný	k2eNgNnSc4d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
slouží	sloužit	k5eAaImIp3nS
modernistům	modernista	k1gMnPc3
k	k	k7c3
argumentaci	argumentace	k1gFnSc3
za	za	k7c4
zákaz	zákaz	k1gInSc4
tohoto	tento	k3xDgNnSc2
pravidla	pravidlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každopádně	každopádně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
koránu	korán	k1gInSc6
doporučuje	doporučovat	k5eAaImIp3nS
mít	mít	k5eAaImF
pouze	pouze	k6eAd1
jednu	jeden	k4xCgFnSc4
manželku	manželka	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
by	by	kYmCp3nS
si	se	k3xPyFc3
člověk	člověk	k1gMnSc1
nebyl	být	k5eNaImAgMnS
jist	jist	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
schopen	schopen	k2eAgMnSc1d1
dostát	dostát	k5eAaPmF
svým	svůj	k3xOyFgInPc3
závazkům	závazek	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verš	verš	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
220	#num#	k4
zakazuje	zakazovat	k5eAaImIp3nS
sňatek	sňatek	k1gInSc1
s	s	k7c7
modloslužebnicí	modloslužebnice	k1gFnSc7
a	a	k8xC
za	za	k7c4
lepší	dobrý	k2eAgNnSc4d2
považuje	považovat	k5eAaImIp3nS
právě	právě	k9
věřící	věřící	k2eAgFnSc4d1
konkubínu	konkubína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
otázce	otázka	k1gFnSc6
rozvodu	rozvod	k1gInSc2
zavedl	zavést	k5eAaPmAgInS
korán	korán	k1gInSc1
čtyřměsíční	čtyřměsíční	k2eAgFnSc4d1
čekací	čekací	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
pro	pro	k7c4
muže	muž	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
požádají	požádat	k5eAaPmIp3nP
o	o	k7c4
rozvod	rozvod	k1gInSc4
a	a	k8xC
zavedl	zavést	k5eAaPmAgInS
povinnost	povinnost	k1gFnSc4
postarat	postarat	k5eAaPmF
se	se	k3xPyFc4
o	o	k7c4
bývalou	bývalý	k2eAgFnSc4d1
manželku	manželka	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
korán	korán	k1gInSc4
nijak	nijak	k6eAd1
zvlášť	zvlášť	k6eAd1
neřeší	řešit	k5eNaImIp3nS
homosexualitu	homosexualita	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Několik	několik	k4yIc4
negativních	negativní	k2eAgFnPc2d1
narážek	narážka	k1gFnPc2
lze	lze	k6eAd1
najít	najít	k5eAaPmF
ve	v	k7c6
vyprávění	vyprávění	k1gNnSc6
o	o	k7c6
Lotovi	Lot	k1gMnSc6
a	a	k8xC
Sodomě	Sodoma	k1gFnSc6
a	a	k8xC
Gomoře	Gomora	k1gFnSc6
(	(	kIx(
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
80	#num#	k4
<g/>
–	–	k?
<g/>
84	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
:	:	kIx,
<g/>
77	#num#	k4
<g/>
–	–	k?
<g/>
83	#num#	k4
<g/>
,	,	kIx,
21	#num#	k4
<g/>
:	:	kIx,
<g/>
74	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
:	:	kIx,
<g/>
43	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
:	:	kIx,
<g/>
165	#num#	k4
<g/>
–	–	k?
<g/>
175	#num#	k4
<g/>
,	,	kIx,
27	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
a	a	k8xC
29	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
–	–	k?
<g/>
33	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Korán	korán	k1gInSc1
neuznává	uznávat	k5eNaImIp3nS
rovnoprávnost	rovnoprávnost	k1gFnSc4
věřících	věřící	k1gMnPc2
a	a	k8xC
nevěřících	nevěřící	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevěřící	nevěřící	k1gMnSc1
přirovnává	přirovnávat	k5eAaImIp3nS
k	k	k7c3
dobytku	dobytek	k1gInSc3
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
171	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nazývá	nazývat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
nespravedlivými	spravedlivý	k2eNgFnPc7d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
254	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zakazuje	zakazovat	k5eAaImIp3nS
přátelství	přátelství	k1gNnSc1
mezi	mezi	k7c7
věřícími	věřící	k1gMnPc7
a	a	k8xC
nevěřícími	nevěřící	k1gMnPc7
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
144	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
vyzývá	vyzývat	k5eAaImIp3nS
k	k	k7c3
boji	boj	k1gInSc3
proti	proti	k7c3
nevěřícím	nevěřící	k1gMnPc3
(	(	kIx(
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
65	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
:	:	kIx,
<g/>
73	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
<g/>
:	:	kIx,
<g/>
123	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Verše	verš	k1gInSc2
23	#num#	k4
<g/>
:	:	kIx,
<g/>
97	#num#	k4
<g/>
–	–	k?
<g/>
98	#num#	k4
<g/>
,	,	kIx,
41	#num#	k4
<g/>
:	:	kIx,
<g/>
34	#num#	k4
a	a	k8xC
16	#num#	k4
<g/>
:	:	kIx,
<g/>
127	#num#	k4
praví	pravit	k5eAaBmIp3nS,k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Jsme	být	k5eAaImIp1nP
věru	věra	k1gFnSc4
s	s	k7c7
to	ten	k3xDgNnSc1
ti	ten	k3xDgMnPc1
ukázat	ukázat	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
jsme	být	k5eAaImIp1nP
jim	on	k3xPp3gMnPc3
slíbili	slíbit	k5eAaPmAgMnP
<g/>
,	,	kIx,
avšak	avšak	k8xC
raději	rád	k6eAd2
odvracej	odvracet	k5eAaImRp2nS
zlé	zlá	k1gFnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
lepší	dobrý	k2eAgMnSc1d2
<g/>
!	!	kIx.
</s>
<s desamb="1">
My	my	k3xPp1nPc1
dobře	dobře	k6eAd1
známe	znát	k5eAaImIp1nP
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
si	se	k3xPyFc3
vymýšlejí	vymýšlet	k5eAaImIp3nP
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
verše	verš	k1gInPc4
pocházejí	pocházet	k5eAaImIp3nP
ještě	ještě	k9
z	z	k7c2
mekkánského	mekkánský	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
islámskou	islámský	k2eAgFnSc4d1
etiku	etika	k1gFnSc4
mají	mít	k5eAaImIp3nP
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
–	–	k?
říkají	říkat	k5eAaImIp3nP
vlastně	vlastně	k9
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
zlo	zlo	k1gNnSc4
má	mít	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
odpovídat	odpovídat	k5eAaImF
ještě	ještě	k9
větším	veliký	k2eAgNnSc7d2
dobrem	dobro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
dokladem	doklad	k1gInSc7
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
islámská	islámský	k2eAgFnSc1d1
etika	etika	k1gFnSc1
skokem	skok	k1gInSc7
k	k	k7c3
větší	veliký	k2eAgFnSc3d2
humanitě	humanita	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Predestinace	predestinace	k1gFnSc1
vs	vs	k?
<g/>
.	.	kIx.
svobodná	svobodný	k2eAgFnSc1d1
vůle	vůle	k1gFnSc1
</s>
<s>
V	v	k7c6
koránu	korán	k1gInSc6
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
řadu	řada	k1gFnSc4
veršů	verš	k1gInPc2
o	o	k7c6
predestinaci	predestinace	k1gFnSc6
i	i	k8xC
svobodné	svobodný	k2eAgFnSc6d1
vůli	vůle	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
však	však	k9
nutno	nutno	k6eAd1
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jednoznačně	jednoznačně	k6eAd1
inklinuje	inklinovat	k5eAaImIp3nS
k	k	k7c3
predestinaci	predestinace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
myšlenku	myšlenka	k1gFnSc4
svobodné	svobodný	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
z	z	k7c2
veršů	verš	k1gInPc2
spíše	spíše	k9
odhadovat	odhadovat	k5eAaImF
(	(	kIx(
<g/>
29	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
,	,	kIx,
33	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
verše	verš	k1gInPc1
o	o	k7c4
predestinaci	predestinace	k1gFnSc4
hovoří	hovořit	k5eAaImIp3nS
jasněji	jasně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
předurčením	předurčení	k1gNnSc7
zůstávat	zůstávat	k5eAaImF
nevěřícím	nevěřící	k1gMnSc6
(	(	kIx(
<g/>
76	#num#	k4
<g/>
:	:	kIx,
<g/>
30	#num#	k4
<g/>
,	,	kIx,
32	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velice	velice	k6eAd1
pěkný	pěkný	k2eAgInSc4d1
verš	verš	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
však	však	k9
o	o	k7c6
sporu	spor	k1gInSc6
osudu	osud	k1gInSc2
se	s	k7c7
svobodou	svoboda	k1gFnSc7
až	až	k9
tolik	tolik	k6eAd1
neříká	říkat	k5eNaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
47	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Život	život	k1gInSc1
pozemský	pozemský	k2eAgInSc1d1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
hra	hra	k1gFnSc1
a	a	k8xC
zábava	zábava	k1gFnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Muhammad	Muhammad	k6eAd1
</s>
<s>
V	v	k7c6
koránu	korán	k1gInSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
větší	veliký	k2eAgNnSc1d2
množství	množství	k1gNnSc1
veršů	verš	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
obrací	obracet	k5eAaImIp3nP
na	na	k7c4
Muhammada	Muhammada	k1gFnSc1
<g/>
,	,	kIx,
upravuje	upravovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
postavení	postavení	k1gNnSc4
a	a	k8xC
práva	právo	k1gNnPc4
<g/>
,	,	kIx,
či	či	k8xC
ho	on	k3xPp3gMnSc4
dokonce	dokonce	k9
kárá	kárat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verš	verš	k1gInSc1
11	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
naráží	narážet	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
Muhammad	Muhammad	k1gInSc4
snad	snad	k9
nesděloval	sdělovat	k5eNaImAgMnS
veškerá	veškerý	k3xTgNnPc4
seslání	seslání	k1gNnPc4
<g/>
,	,	kIx,
kterých	který	k3yIgNnPc2,k3yRgNnPc2,k3yQgNnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gInSc3
dostalo	dostat	k5eAaPmAgNnS
<g/>
.	.	kIx.
29	#num#	k4
<g/>
:	:	kIx,
<g/>
47	#num#	k4
zase	zase	k9
popírá	popírat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
kdy	kdy	k6eAd1
Prorok	prorok	k1gMnSc1
četl	číst	k5eAaImAgMnS
bibli	bible	k1gFnSc4
nebo	nebo	k8xC
tóru	tóra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
medínském	medínský	k2eAgNnSc6d1
období	období	k1gNnSc6
se	se	k3xPyFc4
také	také	k9
objevují	objevovat	k5eAaImIp3nP
materiálnější	materiální	k2eAgNnPc1d2
seslání	seslání	k1gNnPc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
:	:	kIx,
<g/>
42	#num#	k4
stanovuje	stanovovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pětina	pětina	k1gFnSc1
veškeré	veškerý	k3xTgFnSc2
kořisti	kořist	k1gFnSc2
patří	patřit	k5eAaImIp3nP
Bohu	bůh	k1gMnSc3
a	a	k8xC
poslu	posel	k1gMnSc3
<g/>
,	,	kIx,
příbuzným	příbuzný	k1gMnPc3
<g/>
,	,	kIx,
sirotkům	sirotek	k1gMnPc3
a	a	k8xC
nuzným	nuzný	k2eAgMnPc3d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
že	že	k8xS
je	být	k5eAaImIp3nS
Muhammadovi	Muhammada	k1gMnSc3
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
33	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
zase	zase	k9
o	o	k7c4
Muhammadovi	Muhammadův	k2eAgMnPc1d1
hovoří	hovořit	k5eAaImIp3nP
jako	jako	k9
o	o	k7c6
Pečeti	pečeť	k1gFnSc6
proroků	prorok	k1gMnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vyjadřuje	vyjadřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
po	po	k7c6
Muhammadovi	Muhammada	k1gMnSc6
již	již	k6eAd1
žádný	žádný	k3yNgMnSc1
další	další	k2eAgMnSc1d1
prorok	prorok	k1gMnSc1
nepřijde	přijít	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Zvláště	zvláště	k6eAd1
sporně	sporně	k6eAd1
působí	působit	k5eAaImIp3nP
verše	verš	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
k	k	k7c3
Muhammadově	Muhammadův	k2eAgFnSc3d1
manželství	manželství	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verš	verš	k1gInSc1
33	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
obhajuje	obhajovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnPc2
manželství	manželství	k1gNnPc2
s	s	k7c7
bývalou	bývalý	k2eAgFnSc7d1
manželkou	manželka	k1gFnSc7
Muhammadova	Muhammadův	k2eAgMnSc4d1
nevlastního	vlastní	k2eNgMnSc4d1
syna	syn	k1gMnSc4
<g/>
.	.	kIx.
33	#num#	k4
<g/>
:	:	kIx,
<g/>
49	#num#	k4
zase	zase	k9
umožňuje	umožňovat	k5eAaImIp3nS
právě	právě	k9
Muhammadovi	Muhammada	k1gMnSc3
jako	jako	k8xC,k8xS
Prorokovi	prorok	k1gMnSc3
mít	mít	k5eAaImF
neomezený	omezený	k2eNgInSc4d1
počet	počet	k1gInSc4
manželek	manželka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojována	ozdrojován	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
biblických	biblický	k2eAgInPc2d1
motivů	motiv	k1gInPc2
nachází	nacházet	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
místo	místo	k1gNnSc4
i	i	k9
v	v	k7c6
koránu	korán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
zde	zde	k6eAd1
ovšem	ovšem	k9
mnohé	mnohý	k2eAgFnPc4d1
odchylky	odchylka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
kupříkladu	kupříkladu	k6eAd1
v	v	k7c6
příhodě	příhoda	k1gFnSc6
o	o	k7c6
Boží	boží	k2eAgFnSc6d1
zkoušce	zkouška	k1gFnSc6
Abraháma	Abrahám	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
mu	on	k3xPp3gMnSc3
má	mít	k5eAaImIp3nS
obětovat	obětovat	k5eAaBmF
svého	svůj	k3xOyFgMnSc4
syna	syn	k1gMnSc4
Izáka	Izák	k1gMnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Izák	Izák	k1gMnSc1
nahrazen	nahradit	k5eAaPmNgMnS
jiným	jiný	k2eAgMnSc7d1
Abrahámovým	Abrahámův	k2eAgMnSc7d1
synem	syn	k1gMnSc7
<g/>
,	,	kIx,
údajným	údajný	k2eAgMnSc7d1
prapředkem	prapředek	k1gMnSc7
Arabů	Arab	k1gMnPc2
<g/>
,	,	kIx,
Ismaelem	Ismael	k1gInSc7
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
(	(	kIx(
<g/>
37	#num#	k4
<g/>
:	:	kIx,
<g/>
101	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
pojetí	pojetí	k1gNnSc3
tohoto	tento	k3xDgInSc2
příběhu	příběh	k1gInSc2
v	v	k7c6
bibli	bible	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
koránu	korán	k1gInSc6
Abrahám	Abrahám	k1gMnSc1
na	na	k7c6
pochybách	pochyba	k1gFnPc6
a	a	k8xC
ptá	ptat	k5eAaImIp3nS
se	se	k3xPyFc4
Izmaela	Izmaela	k1gFnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
má	mít	k5eAaImIp3nS
dělat	dělat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
mu	on	k3xPp3gMnSc3
pak	pak	k6eAd1
potvrdí	potvrdit	k5eAaPmIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
odevzdal	odevzdat	k5eAaPmAgMnS
do	do	k7c2
vůle	vůle	k1gFnSc2
Boží	boží	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
verších	verš	k1gInPc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
171	#num#	k4
a	a	k8xC
5	#num#	k4
<g/>
:	:	kIx,
<g/>
73	#num#	k4
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
také	také	k9
zmínka	zmínka	k1gFnSc1
o	o	k7c6
Svaté	svatý	k2eAgFnSc6d1
trojici	trojice	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
kritizována	kritizovat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s>
Velice	velice	k6eAd1
zajímavý	zajímavý	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
původ	původ	k1gInSc4
veršů	verš	k1gInPc2
18	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
a	a	k8xC
dále	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
súra	súra	k1gFnSc1
obecně	obecně	k6eAd1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejoblíbenějším	oblíbený	k2eAgFnPc3d3
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
někde	někde	k6eAd1
se	se	k3xPyFc4
recituje	recitovat	k5eAaImIp3nS
každý	každý	k3xTgInSc4
pátek	pátek	k1gInSc4
v	v	k7c6
mešitách	mešita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmiňovaná	zmiňovaný	k2eAgFnSc1d1
pasáž	pasáž	k1gFnSc1
súry	súra	k1gFnSc2
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
spojením	spojení	k1gNnSc7
příběhu	příběh	k1gInSc2
Mojžíše	Mojžíš	k1gMnSc2
<g/>
,	,	kIx,
Alexandra	Alexandr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
a	a	k8xC
rabínské	rabínský	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
o	o	k7c6
proroku	prorok	k1gMnSc6
Eliášovi	Eliáš	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poselstvím	poselství	k1gNnSc7
je	být	k5eAaImIp3nS
ukázka	ukázka	k1gFnSc1
lidské	lidský	k2eAgFnSc2d1
nevědomosti	nevědomost	k1gFnSc2
a	a	k8xC
nutnosti	nutnost	k1gFnSc2
odevzdat	odevzdat	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
vůle	vůle	k1gFnSc2
vševědoucího	vševědoucí	k2eAgMnSc2d1
a	a	k8xC
všemocného	všemocný	k2eAgMnSc2d1
Boha	bůh	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Části	část	k1gFnPc1
koránu	korán	k1gInSc2
mají	mít	k5eAaImIp3nP
také	také	k9
velký	velký	k2eAgInSc4d1
mystický	mystický	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
„	„	k?
<g/>
Verš	verš	k1gInSc1
světla	světlo	k1gNnSc2
<g/>
“	“	k?
24	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
je	být	k5eAaImIp3nS
občas	občas	k6eAd1
označován	označovat	k5eAaImNgInS
za	za	k7c4
nejpoetičtější	poetický	k2eAgNnSc4d3
vůbec	vůbec	k9
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
vepsán	vepsat	k5eAaPmNgMnS
do	do	k7c2
kopule	kopule	k1gFnSc2
istanbulské	istanbulský	k2eAgFnSc2d1
mešity	mešita	k1gFnSc2
Hagia	Hagia	k1gFnSc1
Sofia	Sofia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nejkrásnější	krásný	k2eAgFnSc4d3
súru	súra	k1gFnSc4
vůbec	vůbec	k9
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
súra	súra	k1gFnSc1
12	#num#	k4
–	–	k?
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cháridžovské	Cháridžovský	k2eAgFnPc4d1
sekty	sekta	k1gFnPc4
ji	on	k3xPp3gFnSc4
naopak	naopak	k6eAd1
považovaly	považovat	k5eAaImAgFnP
za	za	k7c7
podvrženou	podvržený	k2eAgFnSc7d1
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
údajnou	údajný	k2eAgFnSc4d1
přílišnou	přílišný	k2eAgFnSc4d1
světskost	světskost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Verši	verš	k1gInSc3
17	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
17	#num#	k4
<g/>
:	:	kIx,
<g/>
95	#num#	k4
podložili	podložit	k5eAaPmAgMnP
islámští	islámský	k2eAgMnPc1d1
teologové	teolog	k1gMnPc1
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
rozsáhlý	rozsáhlý	k2eAgInSc1d1
příběh	příběh	k1gInSc1
o	o	k7c6
údajném	údajný	k2eAgNnSc6d1
Muhammadově	Muhammadův	k2eAgNnSc6d1
setkání	setkání	k1gNnSc6
s	s	k7c7
Bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Muhammad	Muhammad	k1gInSc1
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
v	v	k7c6
noci	noc	k1gFnSc6
zázračně	zázračně	k6eAd1
přepravit	přepravit	k5eAaPmF
do	do	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
v	v	k7c6
(	(	kIx(
<g/>
Izraeli	Izrael	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
vystoupat	vystoupat	k5eAaPmF
na	na	k7c4
Nebesa	nebesa	k1gNnPc4
(	(	kIx(
<g/>
Mi	já	k3xPp1nSc3
<g/>
'	'	kIx"
<g/>
rádž	rádž	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgInSc6
základě	základ	k1gInSc6
je	být	k5eAaImIp3nS
Jeruzalém	Jeruzalém	k1gInSc1
muslimy	muslim	k1gMnPc4
považován	považován	k2eAgInSc1d1
za	za	k7c4
třetí	třetí	k4xOgNnSc4
nejposvátnější	posvátný	k2eAgNnSc4d3
město	město	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
hraje	hrát	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
v	v	k7c6
palestinsko-izraelském	palestinsko-izraelský	k2eAgInSc6d1
konfliktu	konflikt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Korán	korán	k1gInSc1
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
rozličnými	rozličný	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
základ	základ	k1gInSc4
často	často	k6eAd1
i	i	k9
protichůdných	protichůdný	k2eAgInPc2d1
názorů	názor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charakteristickým	charakteristický	k2eAgNnSc7d1
je	být	k5eAaImIp3nS
výběr	výběr	k1gInSc1
pouze	pouze	k6eAd1
několika	několik	k4yIc2
konkrétních	konkrétní	k2eAgInPc2d1
veršů	verš	k1gInPc2
a	a	k8xC
konkrétního	konkrétní	k2eAgInSc2d1
výkladu	výklad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modernisté	modernista	k1gMnPc1
i	i	k8xC
fundamentalisté	fundamentalista	k1gMnPc1
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
koránu	korán	k1gInSc2
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
politickém	politický	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
to	ten	k3xDgNnSc1
zrovna	zrovna	k6eAd1
hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgInPc1d1
překlady	překlad	k1gInPc1
koránu	korán	k1gInSc2
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
existují	existovat	k5eAaImIp3nP
čtyři	čtyři	k4xCgInPc1
české	český	k2eAgInPc1d1
překlady	překlad	k1gInPc1
koránu	korán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgFnPc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
jsou	být	k5eAaImIp3nP
překlady	překlad	k1gInPc1
orientalistů	orientalista	k1gMnPc2
a	a	k8xC
jeden	jeden	k4xCgMnSc1
je	být	k5eAaImIp3nS
překladem	překlad	k1gInSc7
skupiny	skupina	k1gFnSc2
ahmadíja	ahmadíj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejběžnějším	běžný	k2eAgInSc7d3
překladem	překlad	k1gInSc7
u	u	k7c2
nás	my	k3xPp1nPc4
je	být	k5eAaImIp3nS
překlad	překlad	k1gInSc1
I.	I.	kA
A.	A.	kA
Hrbka	Hrbek	k1gMnSc2
(	(	kIx(
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
používán	používat	k5eAaImNgInS
i	i	k8xC
česky	česky	k6eAd1
hovořícími	hovořící	k2eAgMnPc7d1
muslimy	muslim	k1gMnPc7
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
jsou	být	k5eAaImIp3nP
k	k	k7c3
sehnání	sehnání	k1gNnSc3
i	i	k9
překlady	překlad	k1gInPc1
A.	A.	kA
R.	R.	kA
Nykla	Nykl	k1gMnSc2
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
I.	I.	kA
Veselého	Veselého	k2eAgMnSc1d1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tři	tři	k4xCgInPc4
překlady	překlad	k1gInPc4
jsou	být	k5eAaImIp3nP
také	také	k9
dostupné	dostupný	k2eAgFnPc1d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
muslimské	muslimský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
ahmadíja	ahmadíja	k6eAd1
je	být	k5eAaImIp3nS
většinovou	většinový	k2eAgFnSc7d1
částí	část	k1gFnSc7
muslimů	muslim	k1gMnPc2
považován	považován	k2eAgMnSc1d1
za	za	k7c7
nevyhovující	vyhovující	k2eNgFnSc7d1
z	z	k7c2
důvodu	důvod	k1gInSc2
sektářství	sektářství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritici	kritik	k1gMnPc1
také	také	k9
namítají	namítat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
nevychází	vycházet	k5eNaImIp3nS
přímo	přímo	k6eAd1
z	z	k7c2
arabštiny	arabština	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
překladu	překlad	k1gInSc2
<g/>
,	,	kIx,
čili	čili	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
překlad	překlad	k1gInSc4
překladu	překlad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Islámská	islámský	k2eAgFnSc1d1
eschatologie	eschatologie	k1gFnSc1
nevychází	vycházet	k5eNaImIp3nS
jen	jen	k9
z	z	k7c2
koránu	korán	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
z	z	k7c2
četných	četný	k2eAgMnPc2d1
hadísů	hadís	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
v	v	k7c6
publikacích	publikace	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
OSTŘANSKÝ	OSTŘANSKÝ	kA
<g/>
,	,	kIx,
Bronislav	Bronislav	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smrt	smrt	k1gFnSc1
<g/>
,	,	kIx,
hroby	hrob	k1gInPc1
a	a	k8xC
záhrobí	záhrobí	k1gNnSc1
v	v	k7c6
islámu	islám	k1gInSc6
<g/>
:	:	kIx,
poslední	poslední	k2eAgFnSc6d1
věci	věc	k1gFnSc6
člověka	člověk	k1gMnSc2
pohledem	pohled	k1gInSc7
muslimských	muslimský	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
304	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
2305	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WERNER	Werner	k1gMnSc1
<g/>
,	,	kIx,
Helmut	Helmut	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Islámská	islámský	k2eAgFnSc1d1
Kniha	kniha	k1gFnSc1
mrtvých	mrtvý	k1gMnPc2
<g/>
:	:	kIx,
představy	představa	k1gFnSc2
islámu	islám	k1gInSc2
o	o	k7c6
onom	onen	k3xDgInSc6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Pragma	Pragma	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2003	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
237	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7205	#num#	k4
<g/>
-	-	kIx~
<g/>
948	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dle	dle	k7c2
koránu	korán	k1gInSc2
je	být	k5eAaImIp3nS
víno	víno	k1gNnSc1
věcí	věc	k1gFnPc2
hnusnou	hnusný	k2eAgFnSc4d1
„	„	k?
<g/>
z	z	k7c2
díla	dílo	k1gNnSc2
satanova	satanův	k2eAgNnSc2d1
<g/>
"	"	kIx"
a	a	k8xC
je	být	k5eAaImIp3nS
až	až	k9
do	do	k7c2
soudného	soudný	k2eAgInSc2d1
dne	den	k1gInSc2
muslimům	muslim	k1gMnPc3
přísně	přísně	k6eAd1
zakázáno	zakázat	k5eAaPmNgNnS
(	(	kIx(
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
90	#num#	k4
<g/>
–	–	k?
<g/>
91	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
„	„	k?
<g/>
Vy	vy	k3xPp2nPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
věříte	věřit	k5eAaImIp2nP
<g/>
!	!	kIx.
</s>
<s desamb="1">
Víno	víno	k1gNnSc1
<g/>
,	,	kIx,
hra	hra	k1gFnSc1
majsir	majsira	k1gFnPc2
<g/>
,	,	kIx,
obětní	obětní	k2eAgInPc4d1
kameny	kámen	k1gInPc4
a	a	k8xC
vrhání	vrhání	k1gNnPc4
losů	los	k1gMnPc2
šípy	šíp	k1gInPc4
jsou	být	k5eAaImIp3nP
věru	věra	k1gFnSc4
věci	věc	k1gFnSc3
hnusné	hnusný	k2eAgFnSc3d1
z	z	k7c2
díla	dílo	k1gNnSc2
satanova	satanův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystříhejte	vystříhat	k5eAaPmRp2nP,k5eAaBmRp2nP
se	se	k3xPyFc4
toho	ten	k3xDgMnSc2
–	–	k?
a	a	k8xC
možná	možná	k9
<g/>
,	,	kIx,
že	že	k8xS
budete	být	k5eAaImBp2nP
blaženi	blažit	k5eAaImNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Satan	satan	k1gInSc1
chce	chtít	k5eAaImIp3nS
mezi	mezi	k7c7
vámi	vy	k3xPp2nPc7
podnítit	podnítit	k5eAaPmF
pomocí	pomoc	k1gFnSc7
vína	víno	k1gNnSc2
a	a	k8xC
hry	hra	k1gFnSc2
majsiru	majsir	k1gInSc2
nepřátelství	nepřátelství	k1gNnSc2
a	a	k8xC
nenávist	nenávist	k1gFnSc4
a	a	k8xC
odvést	odvést	k5eAaPmF
vás	vy	k3xPp2nPc4
od	od	k7c2
vzývání	vzývání	k1gNnPc2
Boha	bůh	k1gMnSc2
a	a	k8xC
od	od	k7c2
modlitby	modlitba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestanete	přestat	k5eAaPmIp2nP
s	s	k7c7
tím	ten	k3xDgNnSc7
tedy	tedy	k9
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
Jinak	jinak	k6eAd1
tomu	ten	k3xDgNnSc3
bude	být	k5eAaImBp3nS
v	v	k7c6
ráji	ráj	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
„	„	k?
<g/>
řeky	řeka	k1gFnSc2
jsou	být	k5eAaImIp3nP
s	s	k7c7
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
nezahnívá	zahnívat	k5eNaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
řeky	řeka	k1gFnPc1
mléka	mléko	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
chuť	chuť	k1gFnSc1
je	být	k5eAaImIp3nS
neměnná	neměnný	k2eAgFnSc1d1,k2eNgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
řeky	řeka	k1gFnPc1
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
rozkoší	rozkoš	k1gFnSc7
je	být	k5eAaImIp3nS
pijícím	pijící	k2eAgNnSc7d1
<g/>
,	,	kIx,
a	a	k8xC
řeky	řeka	k1gFnPc1
medu	med	k1gInSc2
očištěného	očištěný	k2eAgInSc2d1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
korán	korán	k1gInSc1
47	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bude	být	k5eAaImBp3nS
to	ten	k3xDgNnSc1
ale	ale	k9
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
není	být	k5eNaImIp3nS
opojné	opojný	k2eAgNnSc1d1
a	a	k8xC
nevede	vést	k5eNaImIp3nS
k	k	k7c3
opilosti	opilost	k1gFnSc3
<g/>
:	:	kIx,
„	„	k?
<g/>
Kolovat	kolovat	k5eAaImF
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
budou	být	k5eAaImBp3nP
číše	číš	k1gFnPc1
čirého	čirý	k2eAgInSc2d1
nápoje	nápoj	k1gInSc2
jasného	jasný	k2eAgInSc2d1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
rozkoší	rozkoš	k1gFnSc7
pro	pro	k7c4
pijící	pijící	k2eAgNnSc4d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
v	v	k7c6
něm	on	k3xPp3gInSc6
opojnosti	opojnost	k1gFnPc4
není	být	k5eNaImIp3nS
a	a	k8xC
nebudou	být	k5eNaImBp3nP
jím	on	k3xPp3gMnSc7
zmoženi	zmoct	k5eAaPmNgMnP
<g/>
"	"	kIx"
(	(	kIx(
<g/>
korán	korán	k1gInSc1
37	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
<g/>
–	–	k?
<g/>
47	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgFnSc1d1
učení	učení	k1gNnSc4
muslimských	muslimský	k2eAgFnPc2d1
autorit	autorita	k1gFnPc2
o	o	k7c6
alkoholu	alkohol	k1gInSc6
je	být	k5eAaImIp3nS
obsaženo	obsáhnout	k5eAaPmNgNnS
v	v	k7c6
kázání	kázání	k1gNnSc6
šejcha	šejch	k1gMnSc2
Ahmada	Ahmada	k1gFnSc1
Rajaba	Rajaba	k1gFnSc1
„	„	k?
<g/>
Alkohol	alkohol	k1gInSc1
je	být	k5eAaImIp3nS
velkým	velký	k2eAgNnSc7d1
zlem	zlo	k1gNnSc7
<g/>
"	"	kIx"
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
pronesl	pronést	k5eAaPmAgMnS
v	v	k7c6
pražské	pražský	k2eAgFnSc6d1
mešitě	mešita	k1gFnSc6
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
→	→	k?
dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://bora.uib.no/bora-xmlui/bitstream/handle/1956/12367/144806851.pdf?sequence=4&	https://bora.uib.no/bora-xmlui/bitstream/handle/1956/12367/144806851.pdf?sequence=4&	k1gInPc1
<g/>
↑	↑	k?
Meccan	Meccan	k1gInSc1
Trade	Trad	k1gInSc5
And	Anda	k1gFnPc2
The	The	k1gMnSc2
Rise	Ris	k1gMnSc2
Of	Of	k1gMnSc2
Islam	Islam	k1gInSc4
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Princeton	Princeton	k1gInSc1
<g/>
,	,	kIx,
U.	U.	kA
<g/>
S.	S.	kA
<g/>
A	A	kA
<g/>
:	:	kIx,
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
↑	↑	k?
https://repository.upenn.edu/cgi/viewcontent.cgi?article=5006&	https://repository.upenn.edu/cgi/viewcontent.cgi?article=5006&	k1gInSc1
<g/>
↑	↑	k?
https://dergipark.org.tr/tr/download/article-file/592002	https://dergipark.org.tr/tr/download/article-file/592002	k4
<g/>
↑	↑	k?
Dan	Dan	k1gMnSc1
Gibson	Gibson	k1gMnSc1
<g/>
:	:	kIx,
Qur	Qur	k1gMnSc1
<g/>
'	'	kIx"
<g/>
ā	ā	k1gFnPc2
geography	geographa	k1gFnSc2
<g/>
:	:	kIx,
a	a	k8xC
survey	survey	k1gInPc1
and	and	k?
evaluation	evaluation	k1gInSc1
of	of	k?
the	the	k?
geographical	geographicat	k5eAaPmAgMnS
references	references	k1gMnSc1
in	in	k?
the	the	k?
qurã	qurã	k1gInSc1
with	with	k1gInSc1
suggested	suggested	k1gInSc1
solutions	solutionsa	k1gFnPc2
for	forum	k1gNnPc2
various	various	k1gMnSc1
problems	problemsa	k1gFnPc2
and	and	k?
issues	issuesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Independent	independent	k1gMnSc1
Scholars	Scholarsa	k1gFnPc2
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Surrey	Surrey	k1gInPc1
(	(	kIx(
<g/>
BC	BC	kA
<g/>
)	)	kIx)
2011	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9733642	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
↑	↑	k?
O	o	k7c6
Abdelmajidu	Abdelmajid	k1gInSc6
Charfim	Charfima	k1gFnPc2
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
[	[	kIx(
<g/>
https://www.nzz.ch/feuilleton/das-beruehrt-die-grundfesten-des-islam-ld.1365477	https://www.nzz.ch/feuilleton/das-beruehrt-die-grundfesten-des-islam-ld.1365477	k4
Neue	Neue	k1gNnPc2
Zürcher	Zürchra	k1gFnPc2
Zeitung	Zeitunga	k1gFnPc2
<g/>
:	:	kIx,
Článek	článek	k1gInSc1
o	o	k7c6
významu	význam	k1gInSc6
kritické	kritický	k2eAgFnSc2d1
edice	edice	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Bowersock	Bowersock	k1gMnSc1
<g/>
,	,	kIx,
Glen	Glen	k1gMnSc1
W.	W.	kA
Die	Die	k1gFnSc1
Wiege	Wieg	k1gFnSc2
des	des	k1gNnSc1
Islam	Islam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohammed	Mohammed	k1gMnSc1
<g/>
,	,	kIx,
der	drát	k5eAaImRp2nS
Koran	Koran	k1gInSc1
und	und	k?
die	die	k?
antiken	antikno	k1gNnPc2
Kulturen	Kulturno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rita	Rita	k1gFnSc1
Seuss	Seussa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
C.	C.	kA
H.	H.	kA
Beck	Beck	k1gMnSc1
<g/>
,	,	kIx,
München	München	k2eAgMnSc1d1
2019	#num#	k4
<g/>
.	.	kIx.
160	#num#	k4
str	str	kA
<g/>
.	.	kIx.
</s>
<s>
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muhammad	Muhammad	k1gInSc4
a	a	k8xC
Korán	korán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Korán	korán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
794	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
444	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Uvedená	uvedený	k2eAgFnSc1d1
stať	stať	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
–	–	k?
<g/>
104	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
LAWRENCE	LAWRENCE	kA
<g/>
,	,	kIx,
Bruce	Bruce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
Koránu	korán	k1gInSc6
<g/>
:	:	kIx,
biografie	biografie	k1gFnSc1
=	=	kIx~
[	[	kIx(
<g/>
Orig	Orig	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
The	The	k1gMnSc1
Qur	Qur	k1gMnSc1
<g/>
´	´	k?
<g/>
an	an	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Biography	Biograph	k1gInPc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
–	–	k?
BETA	beta	k1gNnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
238	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7306	#num#	k4
<g/>
-	-	kIx~
<g/>
316	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
:	:	kIx,
illustrovaná	illustrovaný	k2eAgFnSc1d1
encyklopaedie	encyklopaedie	k1gFnSc1
obecných	obecný	k2eAgFnPc2d1
vědomostí	vědomost	k1gFnPc2
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1899	#num#	k4
<g/>
.	.	kIx.
1066	#num#	k4
s.	s.	k?
[	[	kIx(
<g/>
Viz	vidět	k5eAaImRp2nS
heslo	heslo	k1gNnSc4
„	„	k?
<g/>
Korán	korán	k1gInSc1
<g/>
"	"	kIx"
na	na	k7c6
str	str	kA
<g/>
.	.	kIx.
800	#num#	k4
<g/>
–	–	k?
<g/>
803	#num#	k4
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Antialkorán	Antialkorán	k1gInSc1
</s>
<s>
Lidé	člověk	k1gMnPc1
knihy	kniha	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Korán	korán	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
korán	korán	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Dílo	dílo	k1gNnSc1
Korán	korán	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Al-Quran	Al-Quran	k1gInSc1
projektovat	projektovat	k5eAaBmF
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
140	#num#	k4
překladů	překlad	k1gInPc2
do	do	k7c2
více	hodně	k6eAd2
než	než	k8xS
35	#num#	k4
jazyků	jazyk	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
česky	česky	k6eAd1
'	'	kIx"
<g/>
Alois	Alois	k1gMnSc1
Richard	Richard	k1gMnSc1
Nykl	Nykl	k1gMnSc1
<g/>
'	'	kIx"
i	i	k9
'	'	kIx"
<g/>
Preklad	Preklad	k1gInSc1
I.	I.	kA
Hrbek	hrbek	k1gInSc1
<g/>
'	'	kIx"
<g/>
)	)	kIx)
</s>
<s>
QUR	QUR	kA
<g/>
'	'	kIx"
<g/>
Ā	Ā	k?
–	–	k?
více	hodně	k6eAd2
než	než	k8xS
45	#num#	k4
překladů	překlad	k1gInPc2
do	do	k7c2
více	hodně	k6eAd2
než	než	k8xS
35	#num#	k4
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
audio	audio	k2eAgFnSc4d1
</s>
<s>
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
koránu	korán	k1gInSc2
s	s	k7c7
vyhledáváním	vyhledávání	k1gNnSc7
</s>
<s>
Encyclopæ	Encyclopæ	k1gNnPc1
Britannica	Britannic	k2eAgNnPc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Qurʾ	Qurʾ	k1gMnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
angl.	angl.	k?
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Islám	islám	k1gInSc1
Víra	víra	k1gFnSc1
a	a	k8xC
praktiky	praktika	k1gFnPc1
</s>
<s>
Jedinost	jedinost	k1gFnSc1
Boha	bůh	k1gMnSc2
•	•	k?
Vyznání	vyznání	k1gNnSc1
víry	víra	k1gFnSc2
•	•	k?
Modlitba	modlitba	k1gFnSc1
•	•	k?
Půst	půst	k1gInSc1
•	•	k?
Pouť	pouť	k1gFnSc1
•	•	k?
Almužna	almužna	k1gFnSc1
•	•	k?
Mešita	mešita	k1gFnSc1
Osobnosti	osobnost	k1gFnSc2
</s>
<s>
Mohamed	Mohamed	k1gMnSc1
•	•	k?
Abú	abú	k1gMnSc1
Bakr	Bakr	k1gMnSc1
•	•	k?
Umar	Umar	k1gMnSc1
ibn	ibn	k?
al-Chattáb	al-Chattáb	k1gInSc1
•	•	k?
Uthmán	Uthmán	k2eAgInSc1d1
ibn	ibn	k?
Affán	Affán	k1gInSc1
•	•	k?
Alí	Alí	k1gFnSc2
ibn	ibn	k?
Abí	Abí	k1gMnSc1
Tálib	Tálib	k1gMnSc1
•	•	k?
Mohamedovi	Mohamedův	k2eAgMnPc1d1
současníci	současník	k1gMnPc1
•	•	k?
Členové	člen	k1gMnPc1
Mohamedovy	Mohamedův	k2eAgFnSc2d1
domácnosti	domácnost	k1gFnSc2
•	•	k?
Proroci	prorok	k1gMnPc1
islámu	islám	k1gInSc2
Náboženské	náboženský	k2eAgInPc4d1
texty	text	k1gInPc4
</s>
<s>
Korán	korán	k1gInSc1
•	•	k?
Sunna	sunna	k1gFnSc1
•	•	k?
Hadís	Hadís	k1gInSc4
Větve	větev	k1gFnSc2
islámu	islám	k1gInSc2
</s>
<s>
Sunnité	sunnita	k1gMnPc1
(	(	kIx(
<g/>
Hanífovský	Hanífovský	k2eAgMnSc1d1
•	•	k?
Málikovský	Málikovský	k2eAgInSc1d1
•	•	k?
Šáfiovský	Šáfiovský	k2eAgInSc1d1
•	•	k?
Hanbalovský	Hanbalovský	k2eAgInSc1d1
•	•	k?
Záhirovský	Záhirovský	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Šíité	šíita	k1gMnPc1
(	(	kIx(
<g/>
Ismá	Ismá	k1gFnSc1
<g/>
'	'	kIx"
<g/>
ílíja	ílíja	k1gFnSc1
(	(	kIx(
<g/>
Nizáríja	Nizáríja	k1gFnSc1
(	(	kIx(
<g/>
Asasíni	Asasín	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
Mustálíja	Mustálíja	k1gFnSc1
•	•	k?
Drúzové	drúzový	k2eAgFnPc4d1
<g/>
)	)	kIx)
•	•	k?
Isná	Isná	k1gFnSc1
ašaríja	ašaríja	k1gMnSc1
(	(	kIx(
<g/>
Dža	Dža	k1gMnSc1
<g/>
'	'	kIx"
<g/>
farovský	farovský	k1gMnSc1
•	•	k?
Alavité	Alavitý	k2eAgNnSc1d1
•	•	k?
Alevité	Alevitý	k2eAgFnPc1d1
<g/>
)	)	kIx)
•	•	k?
Zajdíja	Zajdíja	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Súfismus	súfismus	k1gInSc1
(	(	kIx(
<g/>
Bektašíja	Bektašíj	k2eAgFnSc1d1
•	•	k?
Čištíja	Čištíj	k2eAgFnSc1d1
•	•	k?
Mauláwíja	Mauláwíja	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Cháridža	Cháridža	k1gFnSc1
(	(	kIx(
<g/>
Ibádíja	Ibádíja	k1gFnSc1
<g/>
)	)	kIx)
Sociopolitické	Sociopolitický	k2eAgInPc1d1
aspekty	aspekt	k1gInPc1
</s>
<s>
Islámský	islámský	k2eAgInSc1d1
svět	svět	k1gInSc1
•	•	k?
Umění	umění	k1gNnSc1
•	•	k?
Architektura	architektura	k1gFnSc1
•	•	k?
Města	město	k1gNnSc2
•	•	k?
Kalendář	kalendář	k1gInSc1
•	•	k?
Věda	věda	k1gFnSc1
•	•	k?
Filosofie	filosofie	k1gFnSc1
•	•	k?
Náboženští	náboženský	k2eAgMnPc1d1
vůdcové	vůdce	k1gMnPc1
•	•	k?
Ženy	žena	k1gFnPc1
v	v	k7c6
islámu	islám	k1gInSc6
•	•	k?
Politický	politický	k2eAgInSc1d1
islám	islám	k1gInSc1
•	•	k?
Liberální	liberální	k2eAgInSc1d1
islám	islám	k1gInSc1
•	•	k?
Džihád	džihád	k1gInSc1
•	•	k?
Salafismus	Salafismus	k1gInSc1
(	(	kIx(
<g/>
Wahhábismus	Wahhábismus	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Koránští	Koránský	k2eAgMnPc1d1
muslimové	muslim	k1gMnPc1
•	•	k?
Ahmadíja	Ahmadíj	k1gInSc2
•	•	k?
Islamismus	islamismus	k1gInSc1
•	•	k?
Islámský	islámský	k2eAgInSc1d1
terorismus	terorismus	k1gInSc1
Související	související	k2eAgInSc1d1
články	článek	k1gInPc4
</s>
<s>
Káfir	Káfir	k1gInSc1
•	•	k?
Kritika	kritika	k1gFnSc1
islámu	islám	k1gInSc2
•	•	k?
LGBT	LGBT	kA
v	v	k7c6
islámu	islám	k1gInSc6
•	•	k?
Šaría	Šarí	k2eAgFnSc1d1
Kategorie	kategorie	k1gFnSc1
článků	článek	k1gInPc2
o	o	k7c6
islámu	islám	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Islám	islám	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
unn	unn	k?
<g/>
2006375010	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4032444-8	4032444-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
7935	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79046204	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
175382719	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79046204	#num#	k4
</s>
