<s>
Morava	Morava	k1gFnSc1
Valles	Vallesa	k1gFnPc2
</s>
<s>
Morava	Morava	k1gFnSc1
VallesPoloha	VallesPoloha	k1gFnSc1
</s>
<s>
24.4	24.4	k4
<g/>
W	W	kA
×	×	k?
13.4	13.4	k4
<g/>
S	s	k7c7
Útvar	útvar	k1gInSc1
</s>
<s>
údolí	údolí	k1gNnPc4
Rozměr	rozměr	k1gInSc1
</s>
<s>
325	#num#	k4
km	km	kA
</s>
<s>
Morava	Morava	k1gFnSc1
Valles	Valles	k1gInSc1
je	být	k5eAaImIp3nS
údolí	údolí	k1gNnPc4
na	na	k7c6
povrchu	povrch	k1gInSc6
Marsu	Mars	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
leží	ležet	k5eAaImIp3nP
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údolí	údolí	k1gNnSc6
je	být	k5eAaImIp3nS
dlouhé	dlouhý	k2eAgNnSc4d1
325	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenováno	pojmenován	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
po	po	k7c6
řece	řeka	k1gFnSc6
Moravě	Morava	k1gFnSc6
ležící	ležící	k2eAgFnSc1d1
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Oblast	oblast	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
kvadrantu	kvadrant	k1gInSc6
Margaritifer	Margaritifer	k1gMnSc1
Sinus	sinus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
vyobrazené	vyobrazený	k2eAgNnSc4d1
Morava	Morava	k1gFnSc1
Valles	Vallesa	k1gFnPc2
nalevo	nalevo	k6eAd1
a	a	k8xC
napravo	napravo	k6eAd1
zhruba	zhruba	k6eAd1
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
je	být	k5eAaImIp3nS
kráter	kráter	k1gInSc4
Beer	Beera	k1gFnPc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
také	také	k9
Pivo	pivo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Planetarynames	Planetarynames	k1gInSc1
-	-	kIx~
Morava	Morava	k1gFnSc1
Valles	Valles	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
údolí	údolí	k1gNnSc2
na	na	k7c6
Marsu	Mars	k1gInSc6
</s>
