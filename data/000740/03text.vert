<s>
Titus	Titus	k1gMnSc1	Titus
Flavius	Flavius	k1gMnSc1	Flavius
Domitianus	Domitianus	k1gMnSc1	Domitianus
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
51	[number]	k4	51
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
96	[number]	k4	96
<g/>
,	,	kIx,	,
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
z	z	k7c2	z
flaviovské	flaviovský	k2eAgFnSc2d1	flaviovský
dynastie	dynastie	k1gFnSc2	dynastie
(	(	kIx(	(
<g/>
69	[number]	k4	69
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
<g/>
)	)	kIx)	)
panující	panující	k2eAgFnSc1d1	panující
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
81	[number]	k4	81
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
mladším	mladý	k2eAgMnSc7d2	mladší
synem	syn	k1gMnSc7	syn
císaře	císař	k1gMnSc2	císař
Vespasiana	Vespasian	k1gMnSc2	Vespasian
a	a	k8xC	a
současně	současně	k6eAd1	současně
posledním	poslední	k2eAgMnSc7d1	poslední
flaviovským	flaviovský	k1gMnSc7	flaviovský
vladařem	vladař	k1gMnSc7	vladař
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Tita	Titus	k1gMnSc2	Titus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
postižen	postihnout	k5eAaPmNgInS	postihnout
těžkou	těžký	k2eAgFnSc7d1	těžká
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
přímo	přímo	k6eAd1	přímo
ukázkovými	ukázkový	k2eAgInPc7d1	ukázkový
vzory	vzor	k1gInPc7	vzor
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Domitianus	Domitianus	k1gInSc1	Domitianus
stal	stát	k5eAaPmAgInS	stát
vzorem	vzor	k1gInSc7	vzor
tyrana	tyran	k1gMnSc2	tyran
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
sporům	spor	k1gInPc3	spor
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
krutému	krutý	k2eAgNnSc3d1	kruté
pronásledování	pronásledování	k1gNnSc3	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Domitianus	Domitianus	k1gInSc1	Domitianus
zavedl	zavést	k5eAaPmAgInS	zavést
síť	síť	k1gFnSc4	síť
placených	placený	k2eAgMnPc2d1	placený
udavačů	udavač	k1gMnPc2	udavač
<g/>
,	,	kIx,	,
mrhal	mrhat	k5eAaImAgMnS	mrhat
státními	státní	k2eAgInPc7d1	státní
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňováním	uplatňování	k1gNnSc7	uplatňování
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
urážce	urážka	k1gFnSc6	urážka
majestátu	majestát	k1gInSc2	majestát
překonal	překonat	k5eAaPmAgInS	překonat
dokonce	dokonce	k9	dokonce
i	i	k9	i
císaře	císař	k1gMnSc4	císař
Nerona	Nero	k1gMnSc4	Nero
a	a	k8xC	a
čekal	čekat	k5eAaImAgMnS	čekat
ho	on	k3xPp3gMnSc4	on
i	i	k8xC	i
podobný	podobný	k2eAgInSc4d1	podobný
osud	osud	k1gInSc4	osud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
96	[number]	k4	96
byl	být	k5eAaImAgInS	být
svými	svůj	k3xOyFgMnPc7	svůj
odpůrci	odpůrce	k1gMnPc7	odpůrce
(	(	kIx(	(
<g/>
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
manželkou	manželka	k1gFnSc7	manželka
Domitií	Domitie	k1gFnPc2	Domitie
<g/>
)	)	kIx)	)
zavražděn	zavraždit	k5eAaPmNgInS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Domitianus	Domitianus	k1gMnSc1	Domitianus
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
51	[number]	k4	51
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
budoucího	budoucí	k2eAgMnSc2d1	budoucí
císaře	císař	k1gMnSc2	císař
Vespasiana	Vespasian	k1gMnSc2	Vespasian
a	a	k8xC	a
Flavie	Flavie	k1gFnSc2	Flavie
Domitilly	Domitilla	k1gFnSc2	Domitilla
do	do	k7c2	do
poměrně	poměrně	k6eAd1	poměrně
chudých	chudý	k2eAgInPc2d1	chudý
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vzdělání	vzdělání	k1gNnSc1	vzdělání
ve	v	k7c6	v
svobodných	svobodný	k2eAgNnPc6d1	svobodné
uměních	umění	k1gNnPc6	umění
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
však	však	k9	však
později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
dále	daleko	k6eAd2	daleko
nerozvíjel	rozvíjet	k5eNaImAgMnS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
schopným	schopný	k2eAgMnSc7d1	schopný
řečníkem	řečník	k1gMnSc7	řečník
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c4	o
drobné	drobný	k2eAgInPc4d1	drobný
literární	literární	k2eAgInPc4d1	literární
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
vydal	vydat	k5eAaPmAgInS	vydat
spisek	spisek	k1gInSc1	spisek
o	o	k7c4	o
pěstování	pěstování	k1gNnSc4	pěstování
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Suetonius	Suetonius	k1gInSc1	Suetonius
nám	my	k3xPp1nPc3	my
zanechal	zanechat	k5eAaPmAgInS	zanechat
popis	popis	k1gInSc1	popis
jeho	jeho	k3xOp3gInSc2	jeho
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Postavu	postava	k1gFnSc4	postava
měl	mít	k5eAaImAgMnS	mít
urostlou	urostlý	k2eAgFnSc4d1	urostlá
<g/>
,	,	kIx,	,
tvář	tvář	k1gFnSc4	tvář
skromnou	skromný	k2eAgFnSc4d1	skromná
a	a	k8xC	a
plnou	plný	k2eAgFnSc4d1	plná
červeně	červeň	k1gFnPc4	červeň
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc4	oko
veliké	veliký	k2eAgFnSc2d1	veliká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátkozraké	krátkozraký	k2eAgNnSc1d1	krátkozraké
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
byl	být	k5eAaImAgInS	být
hezký	hezký	k2eAgInSc4d1	hezký
a	a	k8xC	a
sličný	sličný	k2eAgInSc4d1	sličný
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
celým	celý	k2eAgInSc7d1	celý
tělem	tělo	k1gNnSc7	tělo
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
noh	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
měl	mít	k5eAaImAgInS	mít
příliš	příliš	k6eAd1	příliš
krátké	krátký	k2eAgInPc4d1	krátký
prsty	prst	k1gInPc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
jej	on	k3xPp3gNnSc4	on
hyzdila	hyzdit	k5eAaImAgFnS	hyzdit
také	také	k9	také
pleš	pleš	k1gFnSc4	pleš
<g/>
,	,	kIx,	,
obtloustlé	obtloustlý	k2eAgNnSc4d1	obtloustlé
břicho	břicho	k1gNnSc4	břicho
a	a	k8xC	a
hubené	hubený	k2eAgFnPc4d1	hubená
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ty	k3xPp2nSc1	ty
mu	on	k3xPp3gMnSc3	on
zchřadly	zchřadnout	k5eAaPmAgFnP	zchřadnout
teprve	teprve	k9	teprve
po	po	k7c4	po
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Domitií	Domitie	k1gFnSc7	Domitie
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
význačného	význačný	k2eAgMnSc2d1	význačný
Neronova	Neronův	k2eAgMnSc2d1	Neronův
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Gnaea	Gnaeus	k1gMnSc2	Gnaeus
Domitia	Domitius	k1gMnSc2	Domitius
Corbulona	Corbulon	k1gMnSc2	Corbulon
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
udělil	udělit	k5eAaPmAgMnS	udělit
titul	titul	k1gInSc4	titul
augusta	august	k1gMnSc2	august
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
příliš	příliš	k6eAd1	příliš
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
roku	rok	k1gInSc2	rok
83	[number]	k4	83
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
osobní	osobní	k2eAgFnSc1d1	osobní
tragédie	tragédie	k1gFnSc1	tragédie
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
poté	poté	k6eAd1	poté
Domitii	Domitie	k1gFnSc4	Domitie
zapudil	zapudit	k5eAaPmAgMnS	zapudit
<g/>
,	,	kIx,	,
antické	antický	k2eAgInPc4d1	antický
prameny	pramen	k1gInPc4	pramen
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
její	její	k3xOp3gFnSc3	její
nevěře	nevěra	k1gFnSc3	nevěra
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Paridem	Paris	k1gMnSc7	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Domitianus	Domitianus	k1gMnSc1	Domitianus
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
velké	velký	k2eAgFnSc2d1	velká
skrupule	skrupule	k1gFnSc2	skrupule
<g/>
.	.	kIx.	.
</s>
<s>
Suetonius	Suetonius	k1gInSc1	Suetonius
nás	my	k3xPp1nPc4	my
informuje	informovat	k5eAaBmIp3nS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
náruživě	náruživě	k6eAd1	náruživě
liboval	libovat	k5eAaImAgMnS	libovat
v	v	k7c6	v
"	"	kIx"	"
<g/>
lůžkovém	lůžkový	k2eAgNnSc6d1	lůžkové
zápolení	zápolení	k1gNnSc6	zápolení
<g/>
"	"	kIx"	"
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
dokonce	dokonce	k9	dokonce
udržoval	udržovat	k5eAaImAgInS	udržovat
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
neteří	neteř	k1gFnSc7	neteř
<g/>
,	,	kIx,	,
Titovou	Titový	k2eAgFnSc7d1	Titový
dcerou	dcera	k1gFnSc7	dcera
Julií	Julie	k1gFnPc2	Julie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
s	s	k7c7	s
Domitií	Domitie	k1gFnSc7	Domitie
opět	opět	k6eAd1	opět
usmířili	usmířit	k5eAaPmAgMnP	usmířit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
syrské	syrský	k2eAgFnPc1d1	Syrská
legie	legie	k1gFnPc1	legie
prohlásily	prohlásit	k5eAaPmAgFnP	prohlásit
Vespasiana	Vespasiana	k1gFnSc1	Vespasiana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
69	[number]	k4	69
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
pobýval	pobývat	k5eAaImAgMnS	pobývat
Domitianus	Domitianus	k1gMnSc1	Domitianus
právě	právě	k6eAd1	právě
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
strýcem	strýc	k1gMnSc7	strýc
Flaviem	Flavius	k1gMnSc7	Flavius
Sabinem	Sabinem	k?	Sabinem
byli	být	k5eAaImAgMnP	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
flaviovské	flaviovský	k2eAgFnSc2d1	flaviovský
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Sabinus	Sabinus	k1gMnSc1	Sabinus
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
dojednat	dojednat	k5eAaPmF	dojednat
s	s	k7c7	s
Vitelliem	Vitellium	k1gNnSc7	Vitellium
o	o	k7c4	o
podmínky	podmínka	k1gFnPc4	podmínka
kapitulace	kapitulace	k1gFnSc2	kapitulace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Vitelliovi	Vitelliův	k2eAgMnPc1d1	Vitelliův
vojáci	voják	k1gMnPc1	voják
obklíčili	obklíčit	k5eAaPmAgMnP	obklíčit
Vespasianovy	Vespasianův	k2eAgMnPc4d1	Vespasianův
přívržence	přívrženec	k1gMnPc4	přívrženec
na	na	k7c6	na
Kapitolu	Kapitol	k1gInSc6	Kapitol
a	a	k8xC	a
po	po	k7c6	po
několikadenních	několikadenní	k2eAgInPc6d1	několikadenní
bojích	boj	k1gInPc6	boj
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
celý	celý	k2eAgInSc1d1	celý
Kapitol	Kapitol	k1gInSc1	Kapitol
lehl	lehnout	k5eAaPmAgInS	lehnout
popelem	popel	k1gInSc7	popel
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
jej	on	k3xPp3gNnSc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Sabinus	Sabinus	k1gInSc1	Sabinus
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Domitianovi	Domitian	k1gMnSc3	Domitian
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
za	za	k7c4	za
kněze-obětníka	knězebětník	k1gMnSc4	kněze-obětník
a	a	k8xC	a
ukrýt	ukrýt	k5eAaPmF	ukrýt
se	se	k3xPyFc4	se
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
otcových	otcův	k2eAgMnPc2d1	otcův
klientů	klient	k1gMnPc2	klient
<g/>
.	.	kIx.	.
</s>
<s>
Vyjít	vyjít	k5eAaPmF	vyjít
ven	ven	k6eAd1	ven
se	se	k3xPyFc4	se
odvážil	odvážit	k5eAaPmAgMnS	odvážit
až	až	k6eAd1	až
po	po	k7c6	po
definitivní	definitivní	k2eAgFnSc3d1	definitivní
Vitelliově	Vitelliův	k2eAgFnSc3d1	Vitelliova
porážce	porážka	k1gFnSc3	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
jej	on	k3xPp3gMnSc4	on
pozdravili	pozdravit	k5eAaPmAgMnP	pozdravit
jako	jako	k8xS	jako
císařského	císařský	k2eAgMnSc2d1	císařský
prince	princ	k1gMnSc2	princ
a	a	k8xC	a
Domitianus	Domitianus	k1gInSc1	Domitianus
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
ruka	ruka	k1gFnSc1	ruka
Vespasianova	Vespasianův	k2eAgFnSc1d1	Vespasianova
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
nalézal	nalézat	k5eAaImAgMnS	nalézat
v	v	k7c6	v
Judeji	Judea	k1gFnSc6	Judea
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
začaly	začít	k5eAaPmAgFnP	začít
projevovat	projevovat	k5eAaImF	projevovat
negativní	negativní	k2eAgFnPc4d1	negativní
stránky	stránka	k1gFnPc4	stránka
Domitianovy	Domitianův	k2eAgFnSc2d1	Domitianova
povahy	povaha	k1gFnSc2	povaha
-	-	kIx~	-
využíval	využívat	k5eAaPmAgMnS	využívat
až	až	k8xS	až
zneužíval	zneužívat	k5eAaImAgMnS	zneužívat
všech	všecek	k3xTgFnPc2	všecek
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnPc4	pozice
nabízela	nabízet	k5eAaImAgFnS	nabízet
<g/>
,	,	kIx,	,
počínal	počínat	k5eAaImAgMnS	počínat
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
pyšně	pyšně	k6eAd1	pyšně
a	a	k8xC	a
rozmařile	rozmařile	k6eAd1	rozmařile
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenechal	nechat	k5eNaPmAgMnS	nechat
jedinou	jediný	k2eAgFnSc4d1	jediná
sukni	sukně	k1gFnSc4	sukně
na	na	k7c6	na
pokoji	pokoj	k1gInSc6	pokoj
-	-	kIx~	-
až	až	k6eAd1	až
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
spěšně	spěšně	k6eAd1	spěšně
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
umravnil	umravnit	k5eAaPmAgMnS	umravnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
70	[number]	k4	70
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
jmenování	jmenování	k1gNnSc4	jmenování
prétorem	prétor	k1gMnSc7	prétor
s	s	k7c7	s
konzulskou	konzulský	k2eAgFnSc7d1	konzulská
pravomocí	pravomoc	k1gFnSc7	pravomoc
(	(	kIx(	(
<g/>
praetor	praetor	k1gMnSc1	praetor
urbanus	urbanus	k1gMnSc1	urbanus
consulari	consularit	k5eAaPmRp2nS	consularit
potestate	potestat	k1gMnSc5	potestat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
žárlil	žárlit	k5eAaImAgMnS	žárlit
na	na	k7c4	na
úspěchy	úspěch	k1gInPc4	úspěch
a	a	k8xC	a
hodnosti	hodnost	k1gFnPc4	hodnost
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starý	k2eAgMnSc2d2	starší
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
dychtil	dychtit	k5eAaImAgMnS	dychtit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
všemožně	všemožně	k6eAd1	všemožně
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
uvítal	uvítat	k5eAaPmAgMnS	uvítat
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Galie	Galie	k1gFnSc2	Galie
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
vzpoura	vzpoura	k1gFnSc1	vzpoura
místních	místní	k2eAgInPc2d1	místní
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
horlivě	horlivě	k6eAd1	horlivě
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgMnS	vypravit
ji	on	k3xPp3gFnSc4	on
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Plamen	plamen	k1gInSc1	plamen
vzpoury	vzpoura	k1gFnSc2	vzpoura
byl	být	k5eAaImAgInS	být
však	však	k9	však
uhašen	uhasit	k5eAaPmNgInS	uhasit
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
dospěl	dochvít	k5eAaPmAgMnS	dochvít
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
musel	muset	k5eAaImAgMnS	muset
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
nevolí	nevole	k1gFnSc7	nevole
sledovat	sledovat	k5eAaImF	sledovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
bratr	bratr	k1gMnSc1	bratr
zastiňuje	zastiňovat	k5eAaImIp3nS	zastiňovat
svými	svůj	k3xOyFgInPc7	svůj
vojenskými	vojenský	k2eAgInPc7d1	vojenský
úspěchy	úspěch	k1gInPc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Domitianus	Domitianus	k1gMnSc1	Domitianus
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
vědám	věda	k1gFnPc3	věda
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Vespasianově	Vespasianův	k2eAgFnSc6d1	Vespasianova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
začala	začít	k5eAaPmAgFnS	začít
pomalu	pomalu	k6eAd1	pomalu
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Titus	Titus	k1gMnSc1	Titus
udělil	udělit	k5eAaPmAgMnS	udělit
Domitianovi	Domitian	k1gMnSc3	Domitian
další	další	k2eAgFnSc2d1	další
pocty	pocta	k1gFnSc2	pocta
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
80	[number]	k4	80
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vybral	vybrat	k5eAaPmAgInS	vybrat
za	za	k7c4	za
kolegu	kolega	k1gMnSc4	kolega
v	v	k7c6	v
konzulátu	konzulát	k1gInSc6	konzulát
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
jej	on	k3xPp3gMnSc4	on
veřejně	veřejně	k6eAd1	veřejně
označoval	označovat	k5eAaImAgMnS	označovat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
-	-	kIx~	-
přesto	přesto	k8xC	přesto
však	však	k9	však
byl	být	k5eAaImAgInS	být
reálný	reálný	k2eAgInSc1d1	reálný
Domitianův	Domitianův	k2eAgInSc1d1	Domitianův
vliv	vliv	k1gInSc1	vliv
stále	stále	k6eAd1	stále
nulový	nulový	k2eAgInSc1d1	nulový
<g/>
.	.	kIx.	.
</s>
<s>
Nemůžeme	moct	k5eNaImIp1nP	moct
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
divit	divit	k5eAaImF	divit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
Titus	Titus	k1gMnSc1	Titus
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
Domitianově	Domitianův	k2eAgInSc6d1	Domitianův
podílu	podíl	k1gInSc6	podíl
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nečekaná	čekaný	k2eNgFnSc1d1	nečekaná
Titova	Titův	k2eAgFnSc1d1	Titova
smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
81	[number]	k4	81
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
Domitianovi	Domitian	k1gMnSc3	Domitian
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
císařský	císařský	k2eAgInSc4d1	císařský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
velmi	velmi	k6eAd1	velmi
negativnímu	negativní	k2eAgInSc3d1	negativní
tónu	tón	k1gInSc3	tón
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
většina	většina	k1gFnSc1	většina
antických	antický	k2eAgMnPc2d1	antický
historiků	historik	k1gMnPc2	historik
líčí	líčit	k5eAaImIp3nS	líčit
jeho	jeho	k3xOp3gFnSc4	jeho
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
alespoň	alespoň	k9	alespoň
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
držel	držet	k5eAaImAgMnS	držet
zásad	zásada	k1gFnPc2	zásada
vládnutí	vládnutí	k1gNnSc2	vládnutí
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
prohlásit	prohlásit	k5eAaPmF	prohlásit
bohem	bůh	k1gMnSc7	bůh
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
veškeré	veškerý	k3xTgInPc4	veškerý
dary	dar	k1gInPc4	dar
a	a	k8xC	a
pocty	pocta	k1gFnPc4	pocta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Titus	Titus	k1gInSc1	Titus
udělil	udělit	k5eAaPmAgInS	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Svědomitě	svědomitě	k6eAd1	svědomitě
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
soudním	soudní	k2eAgFnPc3d1	soudní
povinnostem	povinnost	k1gFnPc3	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Úplatné	úplatný	k2eAgMnPc4d1	úplatný
soudce	soudce	k1gMnPc4	soudce
trestal	trestat	k5eAaImAgMnS	trestat
a	a	k8xC	a
nabádal	nabádat	k5eAaImAgMnS	nabádat
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nezdráhal	zdráhat	k5eNaImAgMnS	zdráhat
takového	takový	k3xDgMnSc4	takový
člověka	člověk	k1gMnSc4	člověk
ohlásit	ohlásit	k5eAaPmF	ohlásit
tribunům	tribun	k1gMnPc3	tribun
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
řádně	řádně	k6eAd1	řádně
zakročeno	zakročen	k2eAgNnSc1d1	zakročeno
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
nesmlouvavě	smlouvavě	k6eNd1	smlouvavě
se	se	k3xPyFc4	se
choval	chovat	k5eAaImAgMnS	chovat
i	i	k9	i
k	k	k7c3	k
úředníkům	úředník	k1gMnPc3	úředník
v	v	k7c6	v
provinciích	provincie	k1gFnPc6	provincie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
Suetonius	Suetonius	k1gInSc4	Suetonius
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nikdy	nikdy	k6eAd1	nikdy
nevykonávali	vykonávat	k5eNaImAgMnP	vykonávat
svou	svůj	k3xOyFgFnSc4	svůj
povinnost	povinnost	k1gFnSc4	povinnost
skromněji	skromně	k6eAd2	skromně
a	a	k8xC	a
spravedlivěji	spravedlivě	k6eAd2	spravedlivě
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přísnými	přísný	k2eAgInPc7d1	přísný
tresty	trest	k1gInPc7	trest
stíhal	stíhat	k5eAaImAgMnS	stíhat
i	i	k9	i
udavačství	udavačství	k1gNnSc4	udavačství
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
rozsáhlé	rozsáhlý	k2eAgFnSc6d1	rozsáhlá
výstavbě	výstavba	k1gFnSc6	výstavba
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Dokončil	dokončit	k5eAaPmAgInS	dokončit
stavbu	stavba	k1gFnSc4	stavba
Flaviovského	Flaviovský	k2eAgInSc2d1	Flaviovský
amfiteátru	amfiteátr	k1gInSc2	amfiteátr
<g/>
,	,	kIx,	,
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Kolosea	Koloseum	k1gNnSc2	Koloseum
<g/>
.	.	kIx.	.
</s>
<s>
Kapitol	Kapitol	k1gInSc1	Kapitol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
za	za	k7c2	za
Tita	Tit	k1gInSc2	Tit
opět	opět	k6eAd1	opět
vyhořel	vyhořet	k5eAaPmAgInS	vyhořet
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
Stadion	stadion	k1gInSc4	stadion
<g/>
,	,	kIx,	,
Odeion	Odeion	k1gInSc4	Odeion
<g/>
,	,	kIx,	,
naumachii	naumachie	k1gFnSc4	naumachie
(	(	kIx(	(
<g/>
stadion	stadion	k1gInSc4	stadion
pro	pro	k7c4	pro
předvádění	předvádění	k1gNnSc4	předvádění
námořních	námořní	k2eAgFnPc2d1	námořní
bitev	bitva	k1gFnPc2	bitva
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
pořádal	pořádat	k5eAaImAgInS	pořádat
okázalé	okázalý	k2eAgFnPc4d1	okázalá
hostiny	hostina	k1gFnPc4	hostina
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
hojně	hojně	k6eAd1	hojně
rozdával	rozdávat	k5eAaImAgMnS	rozdávat
dary	dar	k1gInPc4	dar
<g/>
.	.	kIx.	.
</s>
<s>
Domitianus	Domitianus	k1gMnSc1	Domitianus
velmi	velmi	k6eAd1	velmi
lpěl	lpět	k5eAaImAgMnS	lpět
na	na	k7c6	na
starých	starý	k2eAgFnPc6d1	stará
tradicích	tradice	k1gFnPc6	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
opět	opět	k6eAd1	opět
zavedl	zavést	k5eAaPmAgMnS	zavést
sektory	sektor	k1gInPc4	sektor
vyhrazené	vyhrazený	k2eAgInPc4d1	vyhrazený
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
stavy	stav	k1gInPc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
doživotní	doživotní	k2eAgMnSc1d1	doživotní
censor	censor	k1gMnSc1	censor
vyloučil	vyloučit	k5eAaPmAgMnS	vyloučit
ze	z	k7c2	z
senátu	senát	k1gInSc2	senát
několik	několik	k4yIc1	několik
osob	osoba	k1gFnPc2	osoba
pro	pro	k7c4	pro
chování	chování	k1gNnSc4	chování
příčící	příčící	k2eAgNnSc4d1	příčící
se	se	k3xPyFc4	se
mravům	mrav	k1gInPc3	mrav
předků	předek	k1gInPc2	předek
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
herce	herec	k1gMnPc4	herec
nebo	nebo	k8xC	nebo
bývalé	bývalý	k2eAgMnPc4d1	bývalý
otroky	otrok	k1gMnPc4	otrok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zakázal	zakázat	k5eAaPmAgMnS	zakázat
vyklešťování	vyklešťování	k1gNnPc4	vyklešťování
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
brojil	brojit	k5eAaImAgMnS	brojit
proti	proti	k7c3	proti
homosexualitě	homosexualita	k1gFnSc3	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Oddaně	oddaně	k6eAd1	oddaně
uctíval	uctívat	k5eAaImAgMnS	uctívat
bohy	bůh	k1gMnPc4	bůh
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
především	především	k9	především
Minervu	Minerva	k1gFnSc4	Minerva
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
z	z	k7c2	z
mnoha	mnoho	k4c3	mnoho
jeho	jeho	k3xOp3gFnSc7	jeho
mincí	mince	k1gFnSc7	mince
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
počest	počest	k1gFnSc4	počest
pořádal	pořádat	k5eAaImAgInS	pořádat
každoročně	každoročně	k6eAd1	každoročně
Panathénské	Panathénský	k2eAgFnPc4d1	Panathénský
slavnosti	slavnost	k1gFnPc4	slavnost
s	s	k7c7	s
gladiátorskými	gladiátorský	k2eAgInPc7d1	gladiátorský
zápasy	zápas	k1gInPc7	zápas
i	i	k8xC	i
závody	závod	k1gInPc7	závod
básníků	básník	k1gMnPc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
obnovil	obnovit	k5eAaPmAgInS	obnovit
prastarý	prastarý	k2eAgInSc1d1	prastarý
trest	trest	k1gInSc1	trest
pohřbení	pohřbení	k1gNnSc2	pohřbení
zaživa	zaživa	k6eAd1	zaživa
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
se	se	k3xPyFc4	se
trestaly	trestat	k5eAaImAgFnP	trestat
Vestiny	Vestin	k2eAgFnPc1d1	Vestin
kněžky	kněžka	k1gFnPc1	kněžka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
poskvrnily	poskvrnit	k5eAaPmAgFnP	poskvrnit
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Domitianus	Domitianus	k1gMnSc1	Domitianus
velmi	velmi	k6eAd1	velmi
toužil	toužit	k5eAaImAgMnS	toužit
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
se	se	k3xPyFc4	se
co	co	k9	co
do	do	k7c2	do
vojenské	vojenský	k2eAgFnSc2d1	vojenská
slávy	sláva	k1gFnSc2	sláva
svému	svůj	k1gMnSc3	svůj
otci	otec	k1gMnSc3	otec
i	i	k8xC	i
bratru	bratr	k1gMnSc3	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
83	[number]	k4	83
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
germánskému	germánský	k2eAgInSc3d1	germánský
kmeni	kmen	k1gInSc3	kmen
Chattů	Chatta	k1gMnPc2	Chatta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
i	i	k9	i
on	on	k3xPp3gMnSc1	on
mohl	moct	k5eAaImAgInS	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
významným	významný	k2eAgInSc7d1	významný
vojenským	vojenský	k2eAgInSc7d1	vojenský
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgInPc1d1	skutečný
úspěchy	úspěch	k1gInPc1	úspěch
tohoto	tento	k3xDgNnSc2	tento
tažení	tažení	k1gNnSc2	tažení
však	však	k9	však
byly	být	k5eAaImAgFnP	být
pochybné	pochybný	k2eAgFnPc1d1	pochybná
<g/>
.	.	kIx.	.
</s>
<s>
Antičtí	antický	k2eAgMnPc1d1	antický
historikové	historik	k1gMnPc1	historik
nám	my	k3xPp1nPc3	my
sdělují	sdělovat	k5eAaImIp3nP	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
víceméně	víceméně	k9	víceméně
o	o	k7c4	o
loupeživé	loupeživý	k2eAgInPc4d1	loupeživý
nájezdy	nájezd	k1gInPc4	nájezd
na	na	k7c4	na
kmeny	kmen	k1gInPc4	kmen
žijící	žijící	k2eAgInPc4d1	žijící
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Porýní	Porýní	k1gNnSc2	Porýní
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k9	už
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
,	,	kIx,	,
nezabránilo	zabránit	k5eNaPmAgNnS	zabránit
to	ten	k3xDgNnSc1	ten
Domitianovi	Domitianův	k2eAgMnPc1d1	Domitianův
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
oslavil	oslavit	k5eAaPmAgInS	oslavit
velkolepý	velkolepý	k2eAgInSc1d1	velkolepý
triumf	triumf	k1gInSc1	triumf
a	a	k8xC	a
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
mincích	mince	k1gFnPc6	mince
se	se	k3xPyFc4	se
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
jako	jako	k9	jako
přemožitel	přemožitel	k1gMnSc1	přemožitel
Germánie	Germánie	k1gFnSc2	Germánie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
zřídil	zřídit	k5eAaPmAgInS	zřídit
v	v	k7c4	v
Porýní	Porýní	k1gNnSc4	Porýní
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
Horní	horní	k2eAgFnSc4d1	horní
Germánii	Germánie	k1gFnSc4	Germánie
(	(	kIx(	(
<g/>
Germania	germanium	k1gNnSc2	germanium
Superior	superior	k1gMnSc1	superior
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc4d1	dolní
Germánii	Germánie	k1gFnSc4	Germánie
(	(	kIx(	(
<g/>
Germania	germanium	k1gNnPc1	germanium
Inferior	Inferiora	k1gFnPc2	Inferiora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokončil	dokončit	k5eAaPmAgInS	dokončit
zábor	zábor	k1gInSc1	zábor
území	území	k1gNnSc4	území
tzv.	tzv.	kA	tzv.
Agri	Agri	k1gNnSc4	Agri
Decumates	Decumates	k1gInSc1	Decumates
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
cílem	cíl	k1gInSc7	cíl
Domitianových	Domitianův	k2eAgInPc2d1	Domitianův
výbojů	výboj	k1gInPc2	výboj
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Podunají	Podunají	k1gNnSc1	Podunají
<g/>
.	.	kIx.	.
</s>
<s>
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
hranice	hranice	k1gFnSc1	hranice
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
znepokojována	znepokojovat	k5eAaImNgFnS	znepokojovat
loupeživými	loupeživý	k2eAgInPc7d1	loupeživý
nájezdy	nájezd	k1gInPc7	nájezd
Svébů	Svéb	k1gInPc2	Svéb
<g/>
,	,	kIx,	,
sarmatských	sarmatský	k2eAgInPc2d1	sarmatský
Jazygů	Jazyg	k1gInPc2	Jazyg
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
Dáků	Dák	k1gMnPc2	Dák
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
tehdy	tehdy	k6eAd1	tehdy
vládl	vládnout	k5eAaImAgInS	vládnout
vojensky	vojensky	k6eAd1	vojensky
velmi	velmi	k6eAd1	velmi
zdatný	zdatný	k2eAgMnSc1d1	zdatný
Decebalus	Decebalus	k1gMnSc1	Decebalus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nájezdů	nájezd	k1gInPc2	nájezd
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
i	i	k8xC	i
moesijský	moesijský	k2eAgMnSc1d1	moesijský
správce	správce	k1gMnSc1	správce
Oppius	Oppius	k1gMnSc1	Oppius
Sabinus	Sabinus	k1gMnSc1	Sabinus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
germánského	germánský	k2eAgNnSc2d1	germánské
tažení	tažení	k1gNnSc2	tažení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vojenské	vojenský	k2eAgFnSc2d1	vojenská
operace	operace	k1gFnSc2	operace
vedl	vést	k5eAaImAgMnS	vést
císař	císař	k1gMnSc1	císař
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
při	při	k7c6	při
pannonských	pannonský	k2eAgInPc6d1	pannonský
bojích	boj	k1gInPc6	boj
svěřil	svěřit	k5eAaPmAgMnS	svěřit
vedení	vedení	k1gNnSc1	vedení
války	válka	k1gFnSc2	válka
svým	svůj	k3xOyFgMnPc3	svůj
zástupcům	zástupce	k1gMnPc3	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
války	válka	k1gFnSc2	válka
šli	jít	k5eAaImAgMnP	jít
Římané	Říman	k1gMnPc1	Říman
od	od	k7c2	od
porážky	porážka	k1gFnSc2	porážka
k	k	k7c3	k
porážce	porážka	k1gFnSc3	porážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
bitvě	bitva	k1gFnSc6	bitva
byla	být	k5eAaImAgFnS	být
pobita	pobit	k2eAgFnSc1d1	pobita
celá	celý	k2eAgFnSc1d1	celá
legie	legie	k1gFnSc1	legie
i	i	k9	i
s	s	k7c7	s
velitelem	velitel	k1gMnSc7	velitel
Corneliem	Cornelium	k1gNnSc7	Cornelium
Fuskem	Fusek	k1gMnSc7	Fusek
<g/>
.	.	kIx.	.
</s>
<s>
Rozzuřený	rozzuřený	k2eAgMnSc1d1	rozzuřený
Domitianus	Domitianus	k1gMnSc1	Domitianus
<g/>
,	,	kIx,	,
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
proti	proti	k7c3	proti
Markomanům	Markoman	k1gMnPc3	Markoman
a	a	k8xC	a
Kvádům	Kvád	k1gMnPc3	Kvád
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
dohodou	dohoda	k1gFnSc7	dohoda
nevyslali	vyslat	k5eNaPmAgMnP	vyslat
do	do	k7c2	do
dácké	dácký	k2eAgFnSc2d1	dácká
války	válka	k1gFnSc2	válka
pomocné	pomocný	k2eAgInPc1d1	pomocný
sbory	sbor	k1gInPc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
však	však	k9	však
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
byl	být	k5eAaImAgInS	být
donucen	donucen	k2eAgInSc1d1	donucen
uzavřít	uzavřít	k5eAaPmF	uzavřít
s	s	k7c7	s
Decebalem	Decebal	k1gInSc7	Decebal
potupný	potupný	k2eAgInSc4d1	potupný
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Decebalus	Decebalus	k1gMnSc1	Decebalus
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
od	od	k7c2	od
Domitiana	Domitian	k1gMnSc2	Domitian
nechal	nechat	k5eAaPmAgMnS	nechat
naoko	naoko	k6eAd1	naoko
korunovat	korunovat	k5eAaBmF	korunovat
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
zůstal	zůstat	k5eAaPmAgMnS	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
nezávislý	závislý	k2eNgInSc1d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
Řím	Řím	k1gInSc1	Řím
se	se	k3xPyFc4	se
v	v	k7c6	v
mírové	mírový	k2eAgFnSc6d1	mírová
smlouvě	smlouva	k1gFnSc6	smlouva
zavázal	zavázat	k5eAaPmAgMnS	zavázat
platit	platit	k5eAaImF	platit
Dákům	Dák	k1gMnPc3	Dák
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
roční	roční	k2eAgInSc4d1	roční
poplatek	poplatek	k1gInSc4	poplatek
a	a	k8xC	a
nadto	nadto	k6eAd1	nadto
i	i	k9	i
vyslat	vyslat	k5eAaPmF	vyslat
ke	k	k7c3	k
králi	král	k1gMnSc3	král
odborníky	odborník	k1gMnPc7	odborník
ovládající	ovládající	k2eAgNnPc4d1	ovládající
řemesla	řemeslo	k1gNnPc4	řemeslo
mírová	mírový	k2eAgNnPc4d1	Mírové
i	i	k8xC	i
válečná	válečný	k2eAgNnPc4d1	válečné
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
zjevný	zjevný	k2eAgInSc4d1	zjevný
neúspěch	neúspěch	k1gInSc4	neúspěch
se	se	k3xPyFc4	se
Domitianus	Domitianus	k1gMnSc1	Domitianus
nezdráhal	zdráhat	k5eNaImAgMnS	zdráhat
prezentovat	prezentovat	k5eAaBmF	prezentovat
výsledek	výsledek	k1gInSc4	výsledek
války	válka	k1gFnSc2	válka
jako	jako	k8xS	jako
své	svůj	k3xOyFgNnSc4	svůj
nesporné	sporný	k2eNgNnSc4d1	nesporné
vítězství	vítězství	k1gNnSc4	vítězství
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
86	[number]	k4	86
oslavil	oslavit	k5eAaPmAgInS	oslavit
další	další	k2eAgInSc1d1	další
triumf	triumf	k1gInSc1	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
zajatců	zajatec	k1gMnPc2	zajatec
a	a	k8xC	a
kořisti	kořist	k1gFnSc2	kořist
údajně	údajně	k6eAd1	údajně
pochodovali	pochodovat	k5eAaImAgMnP	pochodovat
v	v	k7c6	v
triumfu	triumf	k1gInSc6	triumf
koupení	koupený	k2eAgMnPc1d1	koupený
otroci	otrok	k1gMnPc1	otrok
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
vezeny	vézt	k5eAaImNgInP	vézt
i	i	k9	i
předměty	předmět	k1gInPc4	předmět
z	z	k7c2	z
císařských	císařský	k2eAgInPc2d1	císařský
skladů	sklad	k1gInPc2	sklad
<g/>
.	.	kIx.	.
</s>
<s>
Zakrátko	zakrátko	k6eAd1	zakrátko
byly	být	k5eAaImAgInP	být
boje	boj	k1gInPc1	boj
v	v	k7c6	v
Dákii	Dákie	k1gFnSc6	Dákie
opět	opět	k6eAd1	opět
obnoveny	obnovit	k5eAaPmNgInP	obnovit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
úspěšněji	úspěšně	k6eAd2	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Římským	římský	k2eAgFnPc3d1	římská
legiím	legie	k1gFnPc3	legie
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Tettia	Tettius	k1gMnSc2	Tettius
Juliana	Julian	k1gMnSc2	Julian
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
Decebalovy	Decebalův	k2eAgInPc4d1	Decebalův
oddíly	oddíl	k1gInPc4	oddíl
u	u	k7c2	u
Tapae	Tapa	k1gFnSc2	Tapa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
88	[number]	k4	88
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
ukončení	ukončení	k1gNnSc3	ukončení
dáckých	dácký	k2eAgFnPc2d1	dácká
válek	válka	k1gFnPc2	válka
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Traiana	Traian	k1gMnSc4	Traian
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
byl	být	k5eAaImAgInS	být
Domitianus	Domitianus	k1gInSc1	Domitianus
u	u	k7c2	u
vojska	vojsko	k1gNnSc2	vojsko
velmi	velmi	k6eAd1	velmi
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
i	i	k9	i
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvedl	zvednout	k5eAaPmAgMnS	zvednout
žold	žold	k1gInSc4	žold
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
<g/>
,	,	kIx,	,
ze	z	k7c2	z
300	[number]	k4	300
sesterciů	sestercius	k1gInPc2	sestercius
na	na	k7c4	na
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
<s>
Dobrým	dobrý	k2eAgInSc7d1	dobrý
příkladem	příklad	k1gInSc7	příklad
jeho	jeho	k3xOp3gFnSc2	jeho
oblíbenosti	oblíbenost	k1gFnSc2	oblíbenost
u	u	k7c2	u
vojáků	voják	k1gMnPc2	voják
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
zavraždění	zavražděný	k2eAgMnPc1d1	zavražděný
senátoři	senátor	k1gMnPc1	senátor
a	a	k8xC	a
lid	lid	k1gInSc4	lid
slavili	slavit	k5eAaImAgMnP	slavit
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
vojska	vojsko	k1gNnPc4	vojsko
žádala	žádat	k5eAaImAgFnS	žádat
tvrdé	tvrdý	k2eAgNnSc4d1	tvrdé
potrestání	potrestání	k1gNnSc4	potrestání
viníků	viník	k1gMnPc2	viník
<g/>
.	.	kIx.	.
</s>
<s>
Vespasianus	Vespasianus	k1gInSc1	Vespasianus
a	a	k8xC	a
Titus	Titus	k1gInSc1	Titus
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
byli	být	k5eAaImAgMnP	být
nespornými	sporný	k2eNgMnPc7d1	nesporný
vládci	vládce	k1gMnPc7	vládce
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
udržovat	udržovat	k5eAaImF	udržovat
korektní	korektní	k2eAgInPc4d1	korektní
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Domitiana	Domitian	k1gMnSc2	Domitian
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nijak	nijak	k6eAd1	nijak
nesnažil	snažit	k5eNaImAgMnS	snažit
uchovat	uchovat	k5eAaPmF	uchovat
alespoň	alespoň	k9	alespoň
zdání	zdání	k1gNnSc1	zdání
autority	autorita	k1gFnSc2	autorita
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
v	v	k7c6	v
senátorech	senátor	k1gMnPc6	senátor
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgMnSc2	ten
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
také	také	k9	také
choval	chovat	k5eAaImAgInS	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
inauguraci	inaugurace	k1gFnSc6	inaugurace
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
slibu	slib	k1gInSc3	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nevztáhne	vztáhnout	k5eNaPmIp3nS	vztáhnout
ruku	ruka	k1gFnSc4	ruka
na	na	k7c4	na
žádného	žádný	k3yNgMnSc4	žádný
senátora	senátor	k1gMnSc4	senátor
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
měli	mít	k5eAaImAgMnP	mít
jeho	jeho	k3xOp3gMnPc1	jeho
předkové	předek	k1gMnPc1	předek
ve	v	k7c6	v
zvyku	zvyk	k1gInSc6	zvyk
slibovat	slibovat	k5eAaImF	slibovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
se	se	k3xPyFc4	se
nespokojil	spokojit	k5eNaPmAgMnS	spokojit
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
prvního	první	k4xOgNnSc2	první
mezi	mezi	k7c4	mezi
senátory	senátor	k1gMnPc4	senátor
(	(	kIx(	(
<g/>
princeps	princeps	k6eAd1	princeps
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
titulovat	titulovat	k5eAaImF	titulovat
jako	jako	k9	jako
Pán	pán	k1gMnSc1	pán
a	a	k8xC	a
Bůh	bůh	k1gMnSc1	bůh
(	(	kIx(	(
<g/>
Dominus	Dominus	k1gMnSc1	Dominus
et	et	k?	et
Deus	Deus	k1gInSc1	Deus
<g/>
)	)	kIx)	)
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
užíval	užívat	k5eAaImAgInS	užívat
i	i	k9	i
při	při	k7c6	při
oficiálních	oficiální	k2eAgNnPc6d1	oficiální
jednáních	jednání	k1gNnPc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
triumfu	triumf	k1gInSc2	triumf
nad	nad	k7c7	nad
germánskými	germánský	k2eAgMnPc7d1	germánský
Chatty	Chatt	k1gMnPc7	Chatt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
83	[number]	k4	83
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgInS	nechat
odhlasovat	odhlasovat	k5eAaPmF	odhlasovat
neobvykle	obvykle	k6eNd1	obvykle
vysoké	vysoký	k2eAgFnPc4d1	vysoká
pocty	pocta	k1gFnPc4	pocta
<g/>
.	.	kIx.	.
</s>
<s>
Nejenže	nejenže	k6eAd1	nejenže
získal	získat	k5eAaPmAgInS	získat
právo	právo	k1gNnSc4	právo
kdykoliv	kdykoliv	k6eAd1	kdykoliv
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
v	v	k7c6	v
šatu	šat	k1gInSc6	šat
triumfátora	triumfátor	k1gMnSc2	triumfátor
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
jeho	jeho	k3xOp3gNnSc2	jeho
vítězství	vítězství	k1gNnSc2	vítězství
byl	být	k5eAaImAgInS	být
přejmenován	přejmenován	k2eAgInSc1d1	přejmenován
měsíc	měsíc	k1gInSc1	měsíc
září	září	k1gNnSc2	září
na	na	k7c4	na
Germanicus	Germanicus	k1gInSc4	Germanicus
a	a	k8xC	a
říjen	říjen	k1gInSc4	říjen
na	na	k7c4	na
Domitianus	Domitianus	k1gInSc4	Domitianus
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
85	[number]	k4	85
nechal	nechat	k5eAaPmAgInS	nechat
jmenovat	jmenovat	k5eAaBmF	jmenovat
doživotním	doživotní	k2eAgMnSc7d1	doživotní
censorem	censor	k1gMnSc7	censor
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
de	de	k?	de
facto	facto	k1gNnSc4	facto
neomezeným	omezený	k2eNgMnSc7d1	neomezený
pánem	pán	k1gMnSc7	pán
senátu	senát	k1gInSc2	senát
s	s	k7c7	s
pravomocí	pravomoc	k1gFnSc7	pravomoc
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c4	o
vylučování	vylučování	k1gNnSc4	vylučování
a	a	k8xC	a
přijímání	přijímání	k1gNnSc4	přijímání
jeho	jeho	k3xOp3gInPc2	jeho
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInPc1	jeho
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
dosti	dosti	k6eAd1	dosti
napjaté	napjatý	k2eAgFnPc1d1	napjatá
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
živnou	živný	k2eAgFnSc7d1	živná
půdou	půda	k1gFnSc7	půda
pro	pro	k7c4	pro
Domitianovu	Domitianův	k2eAgFnSc4d1	Domitianova
podezíravost	podezíravost	k1gFnSc4	podezíravost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
začalo	začít	k5eAaPmAgNnS	začít
přibývat	přibývat	k5eAaImF	přibývat
skutečných	skutečný	k2eAgNnPc2d1	skutečné
či	či	k8xC	či
domnělých	domnělý	k2eAgNnPc2d1	domnělé
spiknutí	spiknutí	k1gNnPc2	spiknutí
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
senátoři	senátor	k1gMnPc1	senátor
a	a	k8xC	a
jezdci	jezdec	k1gMnPc1	jezdec
skončili	skončit	k5eAaPmAgMnP	skončit
na	na	k7c6	na
popravišti	popraviště	k1gNnSc6	popraviště
<g/>
.	.	kIx.	.
</s>
<s>
Olej	olej	k1gInSc1	olej
do	do	k7c2	do
ohně	oheň	k1gInSc2	oheň
ještě	ještě	k6eAd1	ještě
přililo	přilít	k5eAaPmAgNnS	přilít
povstání	povstání	k1gNnSc1	povstání
Lucia	Lucius	k1gMnSc2	Lucius
Antonia	Antonio	k1gMnSc2	Antonio
Saturnina	Saturnin	k1gMnSc2	Saturnin
<g/>
,	,	kIx,	,
velitele	velitel	k1gMnSc2	velitel
legií	legie	k1gFnPc2	legie
v	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Germánii	Germánie	k1gFnSc6	Germánie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
bylo	být	k5eAaImAgNnS	být
rychle	rychle	k6eAd1	rychle
potlačeno	potlačit	k5eAaPmNgNnS	potlačit
zásahem	zásah	k1gInSc7	zásah
císařova	císařův	k2eAgMnSc2d1	císařův
velitele	velitel	k1gMnSc2	velitel
Lucia	Lucia	k1gFnSc1	Lucia
Maxima	Maxima	k1gFnSc1	Maxima
a	a	k8xC	a
veškeré	veškerý	k3xTgInPc1	veškerý
kompromitující	kompromitující	k2eAgInPc1d1	kompromitující
dokumenty	dokument	k1gInPc1	dokument
byly	být	k5eAaImAgInP	být
zničeny	zničen	k2eAgInPc1d1	zničen
<g/>
,	,	kIx,	,
Domitianus	Domitianus	k1gInSc1	Domitianus
jej	on	k3xPp3gMnSc4	on
využil	využít	k5eAaPmAgInS	využít
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
represím	represe	k1gFnPc3	represe
proti	proti	k7c3	proti
svým	svůj	k3xOyFgMnPc3	svůj
odpůrcům	odpůrce	k1gMnPc3	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
Domitianovy	Domitianův	k2eAgFnPc4d1	Domitianova
oběti	oběť	k1gFnPc4	oběť
náleží	náležet	k5eAaImIp3nS	náležet
např.	např.	kA	např.
Herennius	Herennius	k1gMnSc1	Herennius
Senecio	Senecio	k1gMnSc1	Senecio
<g/>
,	,	kIx,	,
Arulenus	Arulenus	k1gMnSc1	Arulenus
Rusticus	Rusticus	k1gMnSc1	Rusticus
a	a	k8xC	a
Neronův	Neronův	k2eAgMnSc1d1	Neronův
propuštěnec	propuštěnec	k1gMnSc1	propuštěnec
Epafroditos	Epafroditos	k1gMnSc1	Epafroditos
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
Domitianovy	Domitianův	k2eAgFnSc2d1	Domitianova
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
nemohli	moct	k5eNaImAgMnP	moct
cítit	cítit	k5eAaImF	cítit
v	v	k7c4	v
bezpečí	bezpečí	k1gNnSc4	bezpečí
ani	ani	k8xC	ani
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
příbuzní	příbuzný	k1gMnPc1	příbuzný
Flaviovců	Flaviovec	k1gMnPc2	Flaviovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
95	[number]	k4	95
nechal	nechat	k5eAaPmAgMnS	nechat
císař	císař	k1gMnSc1	císař
zavraždit	zavraždit	k5eAaPmF	zavraždit
svého	svůj	k3xOyFgMnSc4	svůj
bratrance	bratranec	k1gMnSc4	bratranec
a	a	k8xC	a
spolukonzula	spolukonzul	k1gMnSc4	spolukonzul
Flavia	Flavius	k1gMnSc4	Flavius
Clementa	Clement	k1gMnSc4	Clement
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gMnPc4	jeho
malé	malý	k2eAgMnPc4d1	malý
syny	syn	k1gMnPc4	syn
Vespasiana	Vespasian	k1gMnSc4	Vespasian
a	a	k8xC	a
Domitiana	Domitian	k1gMnSc4	Domitian
určil	určit	k5eAaPmAgMnS	určit
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
vraždění	vraždění	k1gNnSc2	vraždění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
nikdo	nikdo	k3yNnSc1	nikdo
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
jist	jist	k2eAgMnSc1d1	jist
vlastním	vlastní	k2eAgInSc7d1	vlastní
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
spiknutí	spiknutí	k1gNnSc1	spiknutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nakonec	nakonec	k6eAd1	nakonec
stálo	stát	k5eAaImAgNnS	stát
Domitiana	Domitian	k1gMnSc4	Domitian
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
hlavními	hlavní	k2eAgMnPc7d1	hlavní
organizátory	organizátor	k1gMnPc7	organizátor
byli	být	k5eAaImAgMnP	být
císařovi	císař	k1gMnSc3	císař
správci	správce	k1gMnSc3	správce
Parthenius	Parthenius	k1gMnSc1	Parthenius
<g/>
,	,	kIx,	,
Sigerius	Sigerius	k1gMnSc1	Sigerius
<g/>
,	,	kIx,	,
Entellus	Entellus	k1gMnSc1	Entellus
a	a	k8xC	a
Stephanus	Stephanus	k1gMnSc1	Stephanus
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc1	síť
spiklenců	spiklenec	k1gMnPc2	spiklenec
však	však	k9	však
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mnohem	mnohem	k6eAd1	mnohem
širší	široký	k2eAgFnSc1d2	širší
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
zmínek	zmínka	k1gFnPc2	zmínka
historiků	historik	k1gMnPc2	historik
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
přinejmenším	přinejmenším	k6eAd1	přinejmenším
věděla	vědět	k5eAaImAgFnS	vědět
i	i	k9	i
císařovna	císařovna	k1gFnSc1	císařovna
Domitia	Domitia	k1gFnSc1	Domitia
a	a	k8xC	a
městští	městský	k2eAgMnPc1d1	městský
prefekti	prefekt	k1gMnPc1	prefekt
Norbanus	Norbanus	k1gMnSc1	Norbanus
a	a	k8xC	a
Petronius	Petronius	k1gMnSc1	Petronius
Secundus	Secundus	k1gMnSc1	Secundus
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
vraždy	vražda	k1gFnSc2	vražda
byl	být	k5eAaImAgInS	být
prostý	prostý	k2eAgInSc1d1	prostý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pečlivě	pečlivě	k6eAd1	pečlivě
připravený	připravený	k2eAgInSc1d1	připravený
<g/>
.	.	kIx.	.
</s>
<s>
Spiklenci	spiklenec	k1gMnPc1	spiklenec
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Domitianus	Domitianus	k1gInSc1	Domitianus
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
zvyku	zvyk	k1gInSc6	zvyk
si	se	k3xPyFc3	se
po	po	k7c6	po
zasedání	zasedání	k1gNnSc6	zasedání
na	na	k7c6	na
soudu	soud	k1gInSc6	soud
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
zdřímnout	zdřímnout	k5eAaPmF	zdřímnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
toho	ten	k3xDgMnSc4	ten
hodlali	hodlat	k5eAaImAgMnP	hodlat
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
jako	jako	k9	jako
obvykle	obvykle	k6eAd1	obvykle
odebral	odebrat	k5eAaPmAgMnS	odebrat
na	na	k7c4	na
lože	lože	k1gNnSc4	lože
<g/>
,	,	kIx,	,
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
do	do	k7c2	do
ložnice	ložnice	k1gFnSc2	ložnice
Stephanus	Stephanus	k1gInSc1	Stephanus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
spiklenců	spiklenec	k1gMnPc2	spiklenec
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
zápase	zápas	k1gInSc6	zápas
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
císaře	císař	k1gMnSc4	císař
několika	několik	k4yIc7	několik
dobře	dobře	k6eAd1	dobře
mířenými	mířený	k2eAgFnPc7d1	mířená
ranami	rána	k1gFnPc7	rána
dýkou	dýka	k1gFnSc7	dýka
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
nenáviděného	nenáviděný	k2eAgMnSc2d1	nenáviděný
panovníka	panovník	k1gMnSc2	panovník
přivítal	přivítat	k5eAaPmAgInS	přivítat
senát	senát	k1gInSc1	senát
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
obrazy	obraz	k1gInPc1	obraz
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
památka	památka	k1gFnSc1	památka
prokleta	proklet	k2eAgFnSc1d1	prokleta
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
z	z	k7c2	z
Domitianovy	Domitianův	k2eAgFnSc2d1	Domitianova
smrti	smrt	k1gFnSc2	smrt
radost	radost	k1gFnSc1	radost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
pretoriánů	pretorián	k1gMnPc2	pretorián
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
poměrně	poměrně	k6eAd1	poměrně
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
neslo	nést	k5eAaImAgNnS	nést
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
nelibě	libě	k6eNd1	libě
<g/>
.	.	kIx.	.
</s>
<s>
Prefekt	prefekt	k1gMnSc1	prefekt
pretoriánů	pretorián	k1gMnPc2	pretorián
Casperius	Casperius	k1gMnSc1	Casperius
Aelianus	Aelianus	k1gMnSc1	Aelianus
si	se	k3xPyFc3	se
následně	následně	k6eAd1	následně
vymohl	vymoct	k5eAaPmAgMnS	vymoct
na	na	k7c4	na
Domitianovu	Domitianův	k2eAgMnSc3d1	Domitianův
nástupci	nástupce	k1gMnSc3	nástupce
císaři	císař	k1gMnSc3	císař
Nervovi	Nerva	k1gMnSc3	Nerva
potrestání	potrestání	k1gNnSc4	potrestání
předních	přední	k2eAgMnPc2d1	přední
spiklenců	spiklenec	k1gMnPc2	spiklenec
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
CASSIUS	CASSIUS	kA	CASSIUS
DIO	DIO	kA	DIO
<g/>
.	.	kIx.	.
</s>
<s>
Römische	Römischus	k1gMnSc5	Römischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Otto	Otto	k1gMnSc1	Otto
Veh	Veh	k1gMnSc1	Veh
<g/>
.	.	kIx.	.
</s>
<s>
Düsseldorf	Düsseldorf	k1gInSc1	Düsseldorf
:	:	kIx,	:
Artemis	Artemis	k1gFnSc1	Artemis
&	&	k?	&
Winkler	Winkler	k1gMnSc1	Winkler
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
574	[number]	k4	574
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
538	[number]	k4	538
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3109	[number]	k4	3109
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
SUETONIUS	SUETONIUS	kA	SUETONIUS
TRANQUILLUS	TRANQUILLUS	kA	TRANQUILLUS
<g/>
,	,	kIx,	,
Gaius	Gaius	k1gInSc1	Gaius
<g/>
.	.	kIx.	.
</s>
<s>
Životopisy	životopis	k1gInPc1	životopis
dvanácti	dvanáct	k4xCc2	dvanáct
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Bohumil	Bohumil	k1gMnSc1	Bohumil
Ryba	Ryba	k1gMnSc1	Ryba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
584	[number]	k4	584
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
TACITUS	TACITUS	kA	TACITUS
<g/>
,	,	kIx,	,
Cornelius	Cornelius	k1gInSc1	Cornelius
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
císařského	císařský	k2eAgInSc2d1	císařský
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Bahník	bahník	k1gMnSc1	bahník
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Hartmann	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
476	[number]	k4	476
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Bibliografie	bibliografie	k1gFnSc1	bibliografie
GRANT	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
císařové	císař	k1gMnPc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
387	[number]	k4	387
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7257	[number]	k4	7257
<g/>
-	-	kIx~	-
<g/>
731	[number]	k4	731
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
PETRÁŇ	Petráň	k1gMnSc1	Petráň
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
FRIDRICHOVSKÝ	FRIDRICHOVSKÝ	kA	FRIDRICHOVSKÝ
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
císařoven	císařovna	k1gFnPc2	císařovna
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
jejich	jejich	k3xOp3gFnPc2	jejich
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
365	[number]	k4	365
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
267	[number]	k4	267
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
WOLTERS	WOLTERS	kA	WOLTERS
<g/>
,	,	kIx,	,
Reinhard	Reinhard	k1gInSc1	Reinhard
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
v	v	k7c6	v
Germáni	Germán	k1gMnPc1	Germán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
141	[number]	k4	141
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
539	[number]	k4	539
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Domitianus	Domitianus	k1gInSc4	Domitianus
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
Domitianus	Domitianus	k1gInSc4	Domitianus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Domitianus	Domitianus	k1gInSc1	Domitianus
(	(	kIx(	(
<g/>
Římské	římský	k2eAgNnSc1d1	římské
císařství	císařství	k1gNnSc1	císařství
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Domitianova	Domitianův	k2eAgFnSc1d1	Domitianova
biografie	biografie	k1gFnSc1	biografie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cassius	Cassius	k1gMnSc1	Cassius
Dio	Dio	k1gMnSc1	Dio
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
67	[number]	k4	67
</s>
