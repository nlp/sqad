<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
liberálně	liberálně	k6eAd1	liberálně
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
pravicová	pravicový	k2eAgFnSc1d1	pravicová
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgFnSc1d1	vládní
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
potřetí	potřetí	k4xO	potřetí
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2006	[number]	k4	2006
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
2009	[number]	k4	2009
a	a	k8xC	a
opět	opět	k6eAd1	opět
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2013	[number]	k4	2013
jako	jako	k8xS	jako
nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
koaliční	koaliční	k2eAgMnSc1d1	koaliční
partner	partner	k1gMnSc1	partner
v	v	k7c6	v
kabinetu	kabinet	k1gInSc6	kabinet
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
