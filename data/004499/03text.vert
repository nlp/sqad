<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
Vlasta	Vlasta	k1gFnSc1	Vlasta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc1d1	mužské
křestní	křestní	k2eAgNnSc1d1	křestní
jméno	jméno	k1gNnSc1	jméno
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
milující	milující	k2eAgFnSc1d1	milující
svou	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
překladem	překlad	k1gInSc7	překlad
jména	jméno	k1gNnSc2	jméno
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
Patrik	Patrik	k1gMnSc1	Patrik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
–	–	k?	–
<g/>
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tusar	Tusar	k1gMnSc1	Tusar
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Brodský	Brodský	k1gMnSc1	Brodský
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Vlasta	Vlasta	k1gMnSc1	Vlasta
Burian	Burian	k1gMnSc1	Burian
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Kopecký	Kopecký	k1gMnSc1	Kopecký
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Harapes	Harapes	k1gMnSc1	Harapes
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
tanečník	tanečník	k1gMnSc1	tanečník
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Horváth	Horváth	k1gMnSc1	Horváth
–	–	k?	–
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Třešňák	Třešňák	k1gMnSc1	Třešňák
–	–	k?	–
český	český	k2eAgMnSc1d1	český
folkový	folkový	k2eAgMnSc1d1	folkový
zpěvák	zpěvák	k1gMnSc1	zpěvák
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
–	–	k?	–
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Vlasta	Vlasta	k1gMnSc1	Vlasta
Vrána	Vrána	k1gMnSc1	Vrána
–	–	k?	–
česko-kanadský	českoanadský	k2eAgMnSc1d1	česko-kanadský
herec	herec	k1gMnSc1	herec
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g/>
"	"	kIx"	"
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
