<s>
Wabi	Wabi	k6eAd1	Wabi
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
občanským	občanský	k2eAgNnSc7d1	občanské
jménem	jméno	k1gNnSc7	jméno
Stanislav	Stanislav	k1gMnSc1	Stanislav
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1947	[number]	k4	1947
Zlín	Zlín	k1gInSc1	Zlín
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
Praha	Praha	k1gFnSc1	Praha
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
folkový	folkový	k2eAgMnSc1d1	folkový
písničkář	písničkář	k1gMnSc1	písničkář
a	a	k8xC	a
trampský	trampský	k2eAgMnSc1d1	trampský
bard	bard	k1gMnSc1	bard
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
písně	píseň	k1gFnSc2	píseň
Rosa	Rosa	k1gFnSc1	Rosa
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
neoficiální	oficiální	k2eNgFnSc4d1	neoficiální
trampskou	trampský	k2eAgFnSc4d1	trampská
hymnu	hymna	k1gFnSc4	hymna
<g/>
.	.	kIx.	.
</s>
