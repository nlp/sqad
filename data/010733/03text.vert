<p>
<s>
Wabi	Wabi	k6eAd1	Wabi
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
občanským	občanský	k2eAgNnSc7d1	občanské
jménem	jméno	k1gNnSc7	jméno
Stanislav	Stanislav	k1gMnSc1	Stanislav
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1947	[number]	k4	1947
Zlín	Zlín	k1gInSc1	Zlín
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
Praha	Praha	k1gFnSc1	Praha
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
folkový	folkový	k2eAgMnSc1d1	folkový
písničkář	písničkář	k1gMnSc1	písničkář
a	a	k8xC	a
trampský	trampský	k2eAgMnSc1d1	trampský
bard	bard	k1gMnSc1	bard
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
písně	píseň	k1gFnSc2	píseň
Rosa	Rosa	k1gFnSc1	Rosa
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
neoficiální	oficiální	k2eNgFnSc4d1	neoficiální
trampskou	trampský	k2eAgFnSc4d1	trampská
hymnu	hymna	k1gFnSc4	hymna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Otec	otec	k1gMnSc1	otec
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
swing	swing	k1gInSc1	swing
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Blahotice	Blahotice	k1gFnSc2	Blahotice
u	u	k7c2	u
Těšína	Těšín	k1gInSc2	Těšín
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
straně	strana	k1gFnSc6	strana
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
byli	být	k5eAaImAgMnP	být
skauti	skaut	k1gMnPc1	skaut
a	a	k8xC	a
zpívali	zpívat	k5eAaImAgMnP	zpívat
trampské	trampský	k2eAgFnPc4d1	trampská
a	a	k8xC	a
skautské	skautský	k2eAgFnPc4d1	skautská
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Gottwaldově	Gottwaldov	k1gInSc6	Gottwaldov
vyučil	vyučit	k5eAaPmAgInS	vyučit
zámečníkem	zámečník	k1gMnSc7	zámečník
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
pak	pak	k6eAd1	pak
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
zámečník	zámečník	k1gMnSc1	zámečník
<g/>
,	,	kIx,	,
svářeč	svářeč	k1gMnSc1	svářeč
<g/>
,	,	kIx,	,
prodavač	prodavač	k1gMnSc1	prodavač
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
nejdéle	dlouho	k6eAd3	dlouho
jako	jako	k9	jako
řidič	řidič	k1gMnSc1	řidič
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
pak	pak	k6eAd1	pak
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
profesionální	profesionální	k2eAgFnSc4d1	profesionální
dráhu	dráha	k1gFnSc4	dráha
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přezdívku	přezdívka	k1gFnSc4	přezdívka
Wabi	Wab	k1gFnSc2	Wab
získal	získat	k5eAaPmAgMnS	získat
podle	podle	k7c2	podle
Wabiho	Wabi	k1gMnSc2	Wabi
Ryvoly	Ryvola	k1gFnSc2	Ryvola
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
písně	píseň	k1gFnPc4	píseň
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
svým	svůj	k3xOyFgMnPc3	svůj
kamarádům	kamarád	k1gMnPc3	kamarád
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Chodově	Chodov	k1gInSc6	Chodov
na	na	k7c6	na
Sokolovsku	Sokolovsko	k1gNnSc6	Sokolovsko
<g/>
,	,	kIx,	,
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
se	s	k7c7	s
sokolovskou	sokolovský	k2eAgFnSc7d1	Sokolovská
skupinou	skupina	k1gFnSc7	skupina
Plížák	Plížák	k1gInSc1	Plížák
a	a	k8xC	a
Vojtou	Vojta	k1gMnSc7	Vojta
Kiďákem	Kiďák	k1gMnSc7	Kiďák
Tomáškem	Tomášek	k1gMnSc7	Tomášek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
rodného	rodný	k2eAgInSc2d1	rodný
Gottwaldova	Gottwaldov	k1gInSc2	Gottwaldov
<g/>
)	)	kIx)	)
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Rosa	Rosa	k1gMnSc1	Rosa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
sólově	sólově	k6eAd1	sólově
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
začal	začít	k5eAaPmAgInS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Milošem	Miloš	k1gMnSc7	Miloš
Dvořáčkem	Dvořáček	k1gMnSc7	Dvořáček
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
trvala	trvat	k5eAaImAgFnS	trvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Průběžně	průběžně	k6eAd1	průběžně
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
také	také	k9	také
s	s	k7c7	s
hudebníky	hudebník	k1gMnPc7	hudebník
jako	jako	k8xS	jako
Miki	Mike	k1gFnSc4	Mike
Ryvola	Ryvola	k1gFnSc1	Ryvola
<g/>
,	,	kIx,	,
Kapitán	kapitán	k1gMnSc1	kapitán
Kid	Kid	k1gMnSc1	Kid
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
Pacifik	Pacifik	k1gInSc1	Pacifik
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Ďáblovo	ďáblův	k2eAgNnSc4d1	ďáblovo
stádo	stádo	k1gNnSc4	stádo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
složil	složit	k5eAaPmAgMnS	složit
přes	přes	k7c4	přes
150	[number]	k4	150
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
zlidověly	zlidovět	k5eAaPmAgFnP	zlidovět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
skladbám	skladba	k1gFnPc3	skladba
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Rosa	Rosa	k1gFnSc1	Rosa
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
supraphonského	supraphonský	k2eAgNnSc2d1	supraphonské
gramofonového	gramofonový	k2eAgNnSc2d1	gramofonové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
mě	já	k3xPp1nSc4	já
učil	učít	k5eAaPmAgInS	učít
listopad	listopad	k1gInSc1	listopad
<g/>
,	,	kIx,	,
Ročník	ročník	k1gInSc1	ročník
47	[number]	k4	47
či	či	k8xC	či
Hudsonské	Hudsonský	k2eAgInPc1d1	Hudsonský
šífy	šíf	k1gInPc1	šíf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
–	–	k?	–
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
dcery	dcera	k1gFnSc2	dcera
Markétu	Markéta	k1gFnSc4	Markéta
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
a	a	k8xC	a
Renatu	Renata	k1gFnSc4	Renata
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ze	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Evou	Eva	k1gFnSc7	Eva
syny	syn	k1gMnPc7	syn
Stanislava	Stanislav	k1gMnSc2	Stanislav
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šimona	Šimon	k1gMnSc2	Šimon
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c6	na
alkoholu	alkohol	k1gInSc6	alkohol
<g/>
,	,	kIx,	,
závislosti	závislost	k1gFnSc6	závislost
se	se	k3xPyFc4	se
zbavil	zbavit	k5eAaPmAgInS	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
již	již	k6eAd1	již
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
zrušil	zrušit	k5eAaPmAgInS	zrušit
všechny	všechen	k3xTgInPc4	všechen
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
Jiří	Jiří	k1gMnSc1	Jiří
Černý	Černý	k1gMnSc1	Černý
považuje	považovat	k5eAaImIp3nS	považovat
Wabiho	Wabi	k1gMnSc4	Wabi
Daňka	Daněk	k1gMnSc4	Daněk
za	za	k7c4	za
výborného	výborný	k2eAgMnSc4d1	výborný
melodika	melodik	k1gMnSc4	melodik
<g/>
,	,	kIx,	,
textaře	textař	k1gMnSc2	textař
a	a	k8xC	a
přirozeného	přirozený	k2eAgMnSc2d1	přirozený
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
to	ten	k3xDgNnSc4	ten
jej	on	k3xPp3gNnSc4	on
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
nejvýraznější	výrazný	k2eAgFnSc4d3	nejvýraznější
postavu	postava	k1gFnSc4	postava
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
trampské	trampský	k2eAgFnSc2d1	trampská
písně	píseň	k1gFnSc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Rosa	Rosa	k1gFnSc1	Rosa
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Černého	Černý	k1gMnSc2	Černý
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
debutů	debut	k1gInPc2	debut
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Autorská	autorský	k2eAgFnSc1d1	autorská
Porta	porta	k1gFnSc1	porta
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
–	–	k?	–
píseň	píseň	k1gFnSc1	píseň
Ročník	ročník	k1gInSc1	ročník
47	[number]	k4	47
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
–	–	k?	–
píseň	píseň	k1gFnSc4	píseň
Fotky	fotka	k1gFnSc2	fotka
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
–	–	k?	–
píseň	píseň	k1gFnSc4	píseň
Nevadí	vadit	k5eNaImIp3nS	vadit
</s>
</p>
<p>
<s>
Interpretační	interpretační	k2eAgFnSc1d1	interpretační
Porta	porta	k1gFnSc1	porta
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
Plížák	Plížák	k1gMnSc1	Plížák
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
–	–	k?	–
skupina	skupina	k1gFnSc1	skupina
Rosa	Rosa	k1gFnSc1	Rosa
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Porta	porta	k1gFnSc1	porta
1979	[number]	k4	1979
–	–	k?	–
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
žánru	žánr	k1gInSc2	žánr
</s>
</p>
<p>
<s>
Tip	tip	k1gInSc1	tip
Melodie	melodie	k1gFnSc2	melodie
–	–	k?	–
píseň	píseň	k1gFnSc1	píseň
Bílá	bílý	k2eAgFnSc1d1	bílá
vrána	vrána	k1gFnSc1	vrána
</s>
</p>
<p>
<s>
==	==	k?	==
Hity	hit	k1gInPc4	hit
==	==	k?	==
</s>
</p>
<p>
<s>
Rosa	Rosa	k1gFnSc1	Rosa
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mě	já	k3xPp1nSc4	já
učil	učít	k5eAaPmAgInS	učít
listopad	listopad	k1gInSc1	listopad
</s>
</p>
<p>
<s>
Hudsonský	Hudsonský	k2eAgInSc4d1	Hudsonský
šífy	šíf	k1gInPc4	šíf
</s>
</p>
<p>
<s>
Outsider	outsider	k1gMnSc1	outsider
waltz	waltz	k1gInSc4	waltz
</s>
</p>
<p>
<s>
Fotky	fotka	k1gFnPc1	fotka
</s>
</p>
<p>
<s>
Rybí	rybí	k2eAgFnSc1d1	rybí
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
Ročník	ročník	k1gInSc1	ročník
47	[number]	k4	47
</s>
</p>
<p>
<s>
Nevadí	vadit	k5eNaImIp3nS	vadit
</s>
</p>
<p>
<s>
==	==	k?	==
Vybraná	vybraný	k2eAgFnSc1d1	vybraná
diskografie	diskografie	k1gFnSc1	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
LP	LP	kA	LP
Písně	píseň	k1gFnPc1	píseň
dlouhejch	dlouhejch	k?	dlouhejch
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
;	;	kIx,	;
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Hoboes	Hoboesa	k1gFnPc2	Hoboesa
<g/>
,	,	kIx,	,
Kapitánem	kapitán	k1gMnSc7	kapitán
Kidem	Kid	k1gMnSc7	Kid
a	a	k8xC	a
skupinou	skupina	k1gFnSc7	skupina
Pacifik	Pacifik	k1gInSc4	Pacifik
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LP	LP	kA	LP
Rosa	Rosa	k1gFnSc1	Rosa
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc4	supraphon
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LP	LP	kA	LP
Vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CD	CD	kA	CD
Profil	profil	k1gInSc1	profil
(	(	kIx(	(
<g/>
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CD	CD	kA	CD
Pískoviště	pískoviště	k1gNnSc1	pískoviště
</s>
</p>
<p>
<s>
CD	CD	kA	CD
Valašský	valašský	k2eAgInSc1d1	valašský
drtivý	drtivý	k2eAgInSc1d1	drtivý
styl	styl	k1gInSc1	styl
(	(	kIx(	(
<g/>
Wenkow	Wenkow	k1gFnSc1	Wenkow
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CD	CD	kA	CD
Wabi	Wabi	k1gNnSc1	Wabi
Daněk	Daněk	k1gMnSc1	Daněk
-	-	kIx~	-
master	master	k1gMnSc1	master
série	série	k1gFnSc2	série
</s>
</p>
<p>
<s>
CD	CD	kA	CD
Nech	nechat	k5eAaPmRp2nS	nechat
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
dál	daleko	k6eAd2	daleko
</s>
</p>
<p>
<s>
CD	CD	kA	CD
A	a	k9	a
život	život	k1gInSc4	život
běží	běžet	k5eAaImIp3nS	běžet
dál	daleko	k6eAd2	daleko
(	(	kIx(	(
<g/>
Universal	Universal	k1gMnSc1	Universal
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CD	CD	kA	CD
Wabi	Wabi	k1gNnSc1	Wabi
a	a	k8xC	a
Ďáblovo	ďáblův	k2eAgNnSc1d1	ďáblovo
stádo	stádo	k1gNnSc1	stádo
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CD	CD	kA	CD
Wabi	Wabi	k1gNnSc1	Wabi
a	a	k8xC	a
Ďáblovo	ďáblův	k2eAgNnSc1d1	ďáblovo
stádo	stádo	k1gNnSc1	stádo
–	–	k?	–
Příběhy	příběh	k1gInPc1	příběh
písní	píseň	k1gFnPc2	píseň
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
spoluúčastČeská	spoluúčastČeskat	k5eAaPmIp3nS	spoluúčastČeskat
mše	mše	k1gFnSc1	mše
vánoční	vánoční	k2eAgFnSc1d1	vánoční
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wabi	Wab	k1gFnSc2	Wab
Daněk	Daněk	k1gMnSc1	Daněk
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Wabi	Wab	k1gFnSc2	Wab
Daněk	Daněk	k1gMnSc1	Daněk
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Wabi	Wabi	k1gNnSc4	Wabi
Daněk	Daněk	k1gMnSc1	Daněk
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Wabi	Wabi	k6eAd1	Wabi
Daněk	Daněk	k1gMnSc1	Daněk
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Wabi	Wabi	k6eAd1	Wabi
Daněk	Daněk	k1gMnSc1	Daněk
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
komnata	komnata	k1gFnSc1	komnata
Wabiho	Wabi	k1gMnSc2	Wabi
Daňka	Daněk	k1gMnSc2	Daněk
–	–	k?	–
dokument	dokument	k1gInSc1	dokument
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
online	onlinout	k5eAaPmIp3nS	onlinout
přehrání	přehrání	k1gNnSc4	přehrání
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
písničkář	písničkář	k1gMnSc1	písničkář
Wabi	Wabi	k1gNnSc2	Wabi
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
neoficiální	neoficiální	k2eAgFnSc2d1	neoficiální
trampské	trampský	k2eAgFnSc2d1	trampská
hymny	hymna	k1gFnSc2	hymna
Rosa	Rosa	k1gFnSc1	Rosa
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
</s>
</p>
