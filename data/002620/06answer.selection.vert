<s>
V	v	k7c6	v
Besnickém	Besnický	k2eAgInSc6d1	Besnický
tunelu	tunel	k1gInSc6	tunel
pod	pod	k7c7	pod
Besnickým	Besnický	k2eAgNnSc7d1	Besnický
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
trať	trať	k1gFnSc1	trať
překonává	překonávat	k5eAaImIp3nS	překonávat
rozvodí	rozvodí	k1gNnSc4	rozvodí
mezi	mezi	k7c7	mezi
Hnilcem	Hnilec	k1gMnSc7	Hnilec
a	a	k8xC	a
Hronem	Hron	k1gMnSc7	Hron
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
955,5	[number]	k4	955,5
m	m	kA	m
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvýše	nejvýše	k6eAd1	nejvýše
položený	položený	k2eAgInSc4d1	položený
bod	bod	k1gInSc4	bod
na	na	k7c6	na
normálněrozchodných	normálněrozchodný	k2eAgFnPc6d1	normálněrozchodná
tratích	trať	k1gFnPc6	trať
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
