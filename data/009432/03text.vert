<p>
<s>
Meruňka	meruňka	k1gFnSc1	meruňka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Prunus	Prunus	k1gMnSc1	Prunus
armeniaca	armeniaca	k1gMnSc1	armeniaca
<g/>
,	,	kIx,	,
synonymum	synonymum	k1gNnSc1	synonymum
Armeniaca	Armeniac	k1gInSc2	Armeniac
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc1	strom
z	z	k7c2	z
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
rodu	rod	k1gInSc2	rod
slivoň	slivoň	k1gFnSc1	slivoň
(	(	kIx(	(
<g/>
Prunus	Prunus	k1gInSc1	Prunus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
broskvoň	broskvoň	k1gFnSc1	broskvoň
<g/>
,	,	kIx,	,
třešeň	třešeň	k1gFnSc1	třešeň
nebo	nebo	k8xC	nebo
švestka	švestka	k1gFnSc1	švestka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
přes	přes	k7c4	přes
Arménii	Arménie	k1gFnSc4	Arménie
počátkem	počátkem	k7c2	počátkem
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
přes	přes	k7c4	přes
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
<g/>
,	,	kIx,	,
Štýrsko	Štýrsko	k1gNnSc4	Štýrsko
a	a	k8xC	a
Rakousy	Rakousy	k1gInPc4	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Arménský	arménský	k2eAgInSc1d1	arménský
původ	původ	k1gInSc1	původ
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
její	její	k3xOp3gInSc1	její
latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
armenica	armenic	k1gInSc2	armenic
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgMnSc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
české	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
meruňka	meruňka	k1gFnSc1	meruňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plody	plod	k1gInPc4	plod
==	==	k?	==
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
meruňky	meruňka	k1gFnSc2	meruňka
jsou	být	k5eAaImIp3nP	být
peckovice	peckovice	k1gFnPc1	peckovice
s	s	k7c7	s
oranžovým	oranžový	k2eAgNnSc7d1	oranžové
oplodím	oplodí	k1gNnSc7	oplodí
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
na	na	k7c6	na
loňských	loňský	k2eAgInPc6d1	loňský
letorostech	letorost	k1gInPc6	letorost
<g/>
.	.	kIx.	.
</s>
<s>
Dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
většinou	většina	k1gFnSc7	většina
uprostřed	uprostřed	k7c2	uprostřed
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
zavařené	zavařené	k1gNnSc1	zavařené
či	či	k8xC	či
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
džemu	džem	k1gInSc2	džem
nebo	nebo	k8xC	nebo
marmelády	marmeláda	k1gFnSc2	marmeláda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
pro	pro	k7c4	pro
kyselosladkou	kyselosladký	k2eAgFnSc4d1	kyselosladký
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
sušené	sušený	k2eAgFnPc1d1	sušená
meruňky	meruňka	k1gFnPc1	meruňka
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
vyzrálé	vyzrálý	k2eAgInPc1d1	vyzrálý
až	až	k8xS	až
přezrálé	přezrálý	k2eAgInPc1d1	přezrálý
plody	plod	k1gInPc1	plod
se	se	k3xPyFc4	se
sbírají	sbírat	k5eAaImIp3nP	sbírat
na	na	k7c4	na
kvas	kvas	k1gInSc4	kvas
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
destiluje	destilovat	k5eAaImIp3nS	destilovat
alkoholický	alkoholický	k2eAgInSc1d1	alkoholický
nápoj	nápoj	k1gInSc1	nápoj
–	–	k?	–
meruňkovice	meruňkovice	k1gFnSc2	meruňkovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
plody	plod	k1gInPc1	plod
různých	různý	k2eAgFnPc2d1	různá
odrůd	odrůda	k1gFnPc2	odrůda
mají	mít	k5eAaImIp3nP	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
40	[number]	k4	40
-	-	kIx~	-
90	[number]	k4	90
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
mírnou	mírný	k2eAgFnSc7d1	mírná
zimou	zima	k1gFnSc7	zima
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jen	jen	k9	jen
ve	v	k7c6	v
vinorodných	vinorodný	k2eAgInPc6d1	vinorodný
krajích	kraj	k1gInPc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
meruňkové	meruňkový	k2eAgFnPc1d1	meruňková
polohy	poloha	k1gFnPc1	poloha
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
a	a	k8xC	a
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
do	do	k7c2	do
250	[number]	k4	250
m	m	kA	m
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
roční	roční	k2eAgFnSc7d1	roční
teplotou	teplota	k1gFnSc7	teplota
nad	nad	k7c7	nad
8,5	[number]	k4	8,5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
úhrnem	úhrn	k1gInSc7	úhrn
ročních	roční	k2eAgFnPc2d1	roční
srážek	srážka	k1gFnPc2	srážka
550	[number]	k4	550
mm	mm	kA	mm
<g/>
;	;	kIx,	;
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
chráněny	chránit	k5eAaImNgInP	chránit
před	před	k7c7	před
pronikáním	pronikání	k1gNnSc7	pronikání
chladných	chladný	k2eAgInPc2d1	chladný
větrů	vítr	k1gInPc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Meruňky	meruňka	k1gFnPc1	meruňka
lze	lze	k6eAd1	lze
úspěšně	úspěšně	k6eAd1	úspěšně
pěstovat	pěstovat	k5eAaImF	pěstovat
na	na	k7c6	na
černozemích	černozem	k1gFnPc6	černozem
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
na	na	k7c6	na
hnědozemích	hnědozem	k1gFnPc6	hnědozem
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlinitých	hlinitý	k2eAgFnPc6d1	hlinitá
až	až	k8xS	až
písčitohlinitých	písčitohlinitý	k2eAgFnPc6d1	písčitohlinitá
půdách	půda	k1gFnPc6	půda
v	v	k7c6	v
kukuřičném	kukuřičný	k2eAgInSc6d1	kukuřičný
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
v	v	k7c6	v
řepařském	řepařský	k2eAgInSc6d1	řepařský
výrobním	výrobní	k2eAgInSc6d1	výrobní
typu	typ	k1gInSc6	typ
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
těžší	těžký	k2eAgFnPc4d2	těžší
půdy	půda	k1gFnPc4	půda
jsou	být	k5eAaImIp3nP	být
vhodnější	vhodný	k2eAgInPc1d2	vhodnější
generativně	generativně	k6eAd1	generativně
množené	množený	k2eAgInPc1d1	množený
podnože	podnož	k1gInPc1	podnož
s	s	k7c7	s
bujnějším	bujný	k2eAgInSc7d2	bujnější
růstem	růst	k1gInSc7	růst
<g/>
;	;	kIx,	;
meruňkové	meruňkový	k2eAgInPc1d1	meruňkový
semenáče	semenáč	k1gInPc1	semenáč
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
některé	některý	k3yIgInPc4	některý
podnože	podnož	k1gInPc4	podnož
slivoní	slivoň	k1gFnPc2	slivoň
(	(	kIx(	(
<g/>
renklóda	renklóda	k1gFnSc1	renklóda
<g/>
,	,	kIx,	,
špendlík	špendlík	k1gInSc1	špendlík
žlutý	žlutý	k2eAgInSc1d1	žlutý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
krátkou	krátký	k2eAgFnSc4d1	krátká
dormanci	dormance	k1gFnSc4	dormance
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
jí	on	k3xPp3gFnSc3	on
škodí	škodit	k5eAaImIp3nS	škodit
jarní	jarní	k2eAgInPc1d1	jarní
mrazy	mráz	k1gInPc1	mráz
během	během	k7c2	během
kvetení	kvetení	k1gNnSc2	kvetení
a	a	k8xC	a
náhlé	náhlý	k2eAgInPc1d1	náhlý
silné	silný	k2eAgInPc1d1	silný
mrazy	mráz	k1gInPc1	mráz
v	v	k7c6	v
předjaří	předjaří	k1gNnSc6	předjaří
<g/>
,	,	kIx,	,
během	během	k7c2	během
rašení	rašení	k1gNnSc2	rašení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
vážným	vážný	k2eAgFnPc3d1	vážná
škodám	škoda	k1gFnPc3	škoda
zejména	zejména	k9	zejména
na	na	k7c6	na
osluněné	osluněný	k2eAgFnSc6d1	osluněná
části	část	k1gFnSc6	část
kmene	kmen	k1gInSc2	kmen
a	a	k8xC	a
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
běžné	běžný	k2eAgFnSc2d1	běžná
zimy	zima	k1gFnSc2	zima
snáší	snášet	k5eAaImIp3nS	snášet
dobře	dobře	k6eAd1	dobře
i	i	k9	i
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgFnPc1d2	nižší
teploty	teplota	k1gFnPc1	teplota
než	než	k8xS	než
broskev	broskev	k1gFnSc1	broskev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
chladné	chladný	k2eAgNnSc4d1	chladné
jarní	jarní	k2eAgNnSc4d1	jarní
počasí	počasí	k1gNnSc4	počasí
kolem	kolem	k7c2	kolem
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
jsou	být	k5eAaImIp3nP	být
citlivé	citlivý	k2eAgInPc1d1	citlivý
hlavně	hlavně	k9	hlavně
květy	květ	k1gInPc4	květ
a	a	k8xC	a
mladé	mladý	k2eAgInPc4d1	mladý
plůdky	plůdek	k1gInPc4	plůdek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
odrůda	odrůda	k1gFnSc1	odrůda
Velkopavlovická	Velkopavlovický	k2eAgFnSc1d1	Velkopavlovická
<g/>
,	,	kIx,	,
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
velkými	velký	k2eAgInPc7d1	velký
plody	plod	k1gInPc7	plod
výborné	výborný	k2eAgFnSc2d1	výborná
kvality	kvalita	k1gFnSc2	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
jarní	jarní	k2eAgInPc4d1	jarní
mrazy	mráz	k1gInPc4	mráz
a	a	k8xC	a
houbové	houbový	k2eAgFnPc4d1	houbová
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
nově	nově	k6eAd1	nově
vyšlechtěné	vyšlechtěný	k2eAgFnPc4d1	vyšlechtěná
<g/>
,	,	kIx,	,
odolnější	odolný	k2eAgFnPc4d2	odolnější
odrůdy	odrůda	k1gFnPc4	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
odrůdy	odrůda	k1gFnPc1	odrůda
meruněk	meruňka	k1gFnPc2	meruňka
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Bergeron	Bergeron	k1gInSc1	Bergeron
<g/>
,	,	kIx,	,
Karola	Karola	k1gFnSc1	Karola
<g/>
,	,	kIx,	,
Goldrich	Goldrich	k1gInSc1	Goldrich
<g/>
,	,	kIx,	,
Hargrand	Hargrand	k1gInSc1	Hargrand
nebo	nebo	k8xC	nebo
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nebezpečnost	nebezpečnost	k1gFnSc1	nebezpečnost
jadérek	jadérko	k1gNnPc2	jadérko
===	===	k?	===
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obsahu	obsah	k1gInSc3	obsah
kyanogenního	kyanogenní	k2eAgInSc2d1	kyanogenní
glykosidu	glykosid	k1gInSc2	glykosid
amygdalinu	amygdalin	k1gInSc2	amygdalin
v	v	k7c6	v
meruňkových	meruňkový	k2eAgNnPc6d1	meruňkové
jádrech	jádro	k1gNnPc6	jádro
by	by	kYmCp3nP	by
dospělí	dospělí	k1gMnPc1	dospělí
mohli	moct	k5eAaImAgMnP	moct
sníst	sníst	k5eAaPmF	sníst
asi	asi	k9	asi
3	[number]	k4	3
malá	malá	k1gFnSc1	malá
jadérka	jadérko	k1gNnSc2	jadérko
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
překročili	překročit	k5eAaPmAgMnP	překročit
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
dávku	dávka	k1gFnSc4	dávka
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
EFSA	EFSA	kA	EFSA
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
může	moct	k5eAaImIp3nS	moct
stačit	stačit	k5eAaBmF	stačit
i	i	k9	i
polovina	polovina	k1gFnSc1	polovina
jadérka	jadérko	k1gNnSc2	jadérko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
Meruňka	meruňka	k1gFnSc1	meruňka
(	(	kIx(	(
<g/>
Armeniaca	Armeniaca	k1gFnSc1	Armeniaca
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
jako	jako	k8xS	jako
podrod	podrod	k1gInSc1	podrod
rodu	rod	k1gInSc2	rod
slivoň	slivoň	k1gFnSc1	slivoň
(	(	kIx(	(
<g/>
Prunus	Prunus	k1gInSc1	Prunus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
byla	být	k5eAaImAgNnP	být
krátce	krátce	k6eAd1	krátce
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
rod	rod	k1gInSc4	rod
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
čeleď	čeleď	k1gFnSc1	čeleď
růžovité	růžovitý	k2eAgFnSc2d1	růžovitý
byla	být	k5eAaImAgFnS	být
rozdělována	rozdělovat	k5eAaImNgFnS	rozdělovat
na	na	k7c4	na
několik	několik	k4yIc4	několik
samostatných	samostatný	k2eAgFnPc2d1	samostatná
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
meruňka	meruňka	k1gFnSc1	meruňka
byla	být	k5eAaImAgFnS	být
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
mandloňovité	mandloňovitý	k2eAgFnSc2d1	mandloňovitý
(	(	kIx(	(
<g/>
Amygdalaceae	Amygdalacea	k1gFnSc2	Amygdalacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
již	již	k6eAd1	již
tato	tento	k3xDgFnSc1	tento
systematika	systematika	k1gFnSc1	systematika
téměř	téměř	k6eAd1	téměř
neužívá	užívat	k5eNaImIp3nS	užívat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
druhu	druh	k1gInSc2	druh
zůstal	zůstat	k5eAaPmAgMnS	zůstat
meruňka	meruňka	k1gFnSc1	meruňka
obecná	obecný	k2eAgFnSc1d1	obecná
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
správnější	správní	k2eAgMnSc1d2	správnější
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nazývat	nazývat	k5eAaImF	nazývat
ho	on	k3xPp3gInSc4	on
slivoň	slivoň	k1gFnSc1	slivoň
meruňka	meruňka	k1gFnSc1	meruňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
==	==	k?	==
</s>
</p>
<p>
<s>
Poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
meruňkách	meruňka	k1gFnPc6	meruňka
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
velká	velký	k2eAgFnSc1d1	velká
<g/>
,	,	kIx,	,
že	že	k8xS	že
ceny	cena	k1gFnPc1	cena
vždy	vždy	k6eAd1	vždy
bývají	bývat	k5eAaImIp3nP	bývat
výhodné	výhodný	k2eAgFnPc1d1	výhodná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nicméně	nicméně	k8xC	nicméně
mít	mít	k5eAaImF	mít
na	na	k7c6	na
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
že	že	k8xS	že
meruňky	meruňka	k1gFnPc1	meruňka
kvetou	kvést	k5eAaImIp3nP	kvést
ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
než	než	k8xS	než
broskve	broskev	k1gFnPc1	broskev
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
pěstovány	pěstovat	k5eAaImNgInP	pěstovat
bezpečně	bezpečně	k6eAd1	bezpečně
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
pozdními	pozdní	k2eAgInPc7d1	pozdní
jarními	jarní	k2eAgInPc7d1	jarní
mrazy	mráz	k1gInPc7	mráz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
již	již	k6eAd1	již
broskve	broskev	k1gFnPc1	broskev
nepoškozují	poškozovat	k5eNaImIp3nP	poškozovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řez	řez	k1gInSc4	řez
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
tří	tři	k4xCgInPc2	tři
let	léto	k1gNnPc2	léto
po	po	k7c6	po
výsadbě	výsadba	k1gFnSc6	výsadba
na	na	k7c6	na
stanovišti	stanoviště	k1gNnSc6	stanoviště
je	být	k5eAaImIp3nS	být
formována	formován	k2eAgFnSc1d1	formována
koruna	koruna	k1gFnSc1	koruna
a	a	k8xC	a
obrost	obrost	k1gInSc1	obrost
kolem	kolem	k7c2	kolem
základních	základní	k2eAgFnPc2d1	základní
větví	větev	k1gFnPc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Meruňky	meruňka	k1gFnPc1	meruňka
obvykle	obvykle	k6eAd1	obvykle
rostou	růst	k5eAaImIp3nP	růst
velmi	velmi	k6eAd1	velmi
bujně	bujně	k6eAd1	bujně
a	a	k8xC	a
mívají	mívat	k5eAaImIp3nP	mívat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
výhonků	výhonek	k1gInPc2	výhonek
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
přebytečné	přebytečný	k2eAgInPc1d1	přebytečný
a	a	k8xC	a
slabé	slabý	k2eAgInPc1d1	slabý
výhony	výhon	k1gInPc1	výhon
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
a	a	k8xC	a
jen	jen	k9	jen
pět	pět	k4xCc4	pět
výhonů	výhon	k1gInPc2	výhon
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
ponecháno	ponechat	k5eAaPmNgNnS	ponechat
<g/>
,	,	kIx,	,
<g/>
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
dobré	dobrý	k2eAgInPc1d1	dobrý
předpoklady	předpoklad	k1gInPc1	předpoklad
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
kosterními	kosterní	k2eAgFnPc7d1	kosterní
větvemi	větev	k1gFnPc7	větev
okolo	okolo	k7c2	okolo
terminálu	terminál	k1gInSc2	terminál
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kotlovité	kotlovitý	k2eAgFnSc6d1	kotlovitá
koruně	koruna	k1gFnSc6	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Po	Po	kA	Po
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
koncem	koncem	k7c2	koncem
období	období	k1gNnSc2	období
vegetačního	vegetační	k2eAgInSc2d1	vegetační
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
zbývající	zbývající	k2eAgFnPc1d1	zbývající
větve	větev	k1gFnPc1	větev
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
řezem	řez	k1gInSc7	řez
zkráceny	zkrátit	k5eAaPmNgFnP	zkrátit
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
polovinu	polovina	k1gFnSc4	polovina
až	až	k8xS	až
třetinu	třetina	k1gFnSc4	třetina
jejich	jejich	k3xOp3gFnSc2	jejich
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
postranních	postranní	k2eAgInPc2d1	postranní
pupenů	pupen	k1gInPc2	pupen
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
výhony	výhon	k1gInPc1	výhon
být	být	k5eAaImF	být
omezeny	omezit	k5eAaPmNgFnP	omezit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
až	až	k8xS	až
pět	pět	k4xCc4	pět
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
hlavní	hlavní	k2eAgFnSc6d1	hlavní
větvi	větev	k1gFnSc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Prořezávání	prořezávání	k1gNnSc1	prořezávání
příští	příští	k2eAgInSc4d1	příští
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
strom	strom	k1gInSc1	strom
tak	tak	k6eAd1	tak
nízkou	nízký	k2eAgFnSc4d1	nízká
a	a	k8xC	a
širokou	široký	k2eAgFnSc4d1	široká
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
prořezávání	prořezávání	k1gNnSc2	prořezávání
by	by	kYmCp3nS	by
druhý	druhý	k4xOgInSc4	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
rok	rok	k1gInSc4	rok
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
koruna	koruna	k1gFnSc1	koruna
téměř	téměř	k6eAd1	téměř
stejně	stejně	k6eAd1	stejně
přísně	přísně	k6eAd1	přísně
řezána	řezán	k2eAgFnSc1d1	řezána
<g/>
.	.	kIx.	.
</s>
<s>
Letorosty	letorost	k1gInPc1	letorost
vertikálně	vertikálně	k6eAd1	vertikálně
vyrůstající	vyrůstající	k2eAgInPc1d1	vyrůstající
z	z	k7c2	z
adventivních	adventivní	k2eAgInPc2d1	adventivní
pupenů	pupen	k1gInPc2	pupen
na	na	k7c6	na
kmeni	kmen	k1gInSc6	kmen
a	a	k8xC	a
kosterních	kosterní	k2eAgFnPc6d1	kosterní
větvích	větev	k1gFnPc6	větev
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
odstraněny	odstranit	k5eAaPmNgInP	odstranit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
vzduch	vzduch	k1gInSc1	vzduch
pronikaly	pronikat	k5eAaImAgInP	pronikat
do	do	k7c2	do
koruny	koruna	k1gFnSc2	koruna
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
zásadou	zásada	k1gFnSc7	zásada
je	být	k5eAaImIp3nS	být
neponechávat	ponechávat	k5eNaImF	ponechávat
kosterní	kosterní	k2eAgFnSc2d1	kosterní
větve	větev	k1gFnSc2	větev
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
<g/>
Počínaje	počínaje	k7c7	počínaje
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
rokem	rok	k1gInSc7	rok
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
očekávat	očekávat	k5eAaImF	očekávat
dobrá	dobrý	k2eAgFnSc1d1	dobrá
úroda	úroda	k1gFnSc1	úroda
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zrají	zrát	k5eAaImIp3nP	zrát
na	na	k7c6	na
slabých	slabý	k2eAgFnPc6d1	slabá
větévkách	větévka	k1gFnPc6	větévka
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
kmene	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
lámou	lámat	k5eAaImIp3nP	lámat
nebo	nebo	k8xC	nebo
opadávají	opadávat	k5eAaImIp3nP	opadávat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
prováděn	provádět	k5eAaImNgInS	provádět
spíše	spíše	k9	spíše
hlubší	hluboký	k2eAgInSc1d2	hlubší
zpětný	zpětný	k2eAgInSc1d1	zpětný
řez	řez	k1gInSc1	řez
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc1d1	silný
řez	řez	k1gInSc1	řez
podporuje	podporovat	k5eAaImIp3nS	podporovat
nový	nový	k2eAgInSc4d1	nový
růst	růst	k1gInSc4	růst
na	na	k7c6	na
hlavních	hlavní	k2eAgFnPc6d1	hlavní
větvích	větev	k1gFnPc6	větev
,	,	kIx,	,
čímž	což	k3yRnSc7	což
udržuje	udržovat	k5eAaImIp3nS	udržovat
plodný	plodný	k2eAgInSc4d1	plodný
obrost	obrost	k1gInSc4	obrost
rovnoměrný	rovnoměrný	k2eAgInSc4d1	rovnoměrný
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
prořezávání	prořezávání	k1gNnSc2	prořezávání
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
růstu	růst	k1gInSc6	růst
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
řezu	řez	k1gInSc2	řez
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
snížena	snížit	k5eAaPmNgFnS	snížit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
větve	větev	k1gFnPc1	větev
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
řídce	řídce	k6eAd1	řídce
rozložené	rozložený	k2eAgFnPc4d1	rozložená
a	a	k8xC	a
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
slunce	slunce	k1gNnSc1	slunce
procházelo	procházet	k5eAaImAgNnS	procházet
do	do	k7c2	do
středu	střed	k1gInSc2	střed
koruny	koruna	k1gFnSc2	koruna
kvůli	kvůli	k7c3	kvůli
zrání	zrání	k1gNnSc3	zrání
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
.	.	kIx.	.
<g/>
Řez	řez	k1gInSc1	řez
meruněk	meruňka	k1gFnPc2	meruňka
v	v	k7c6	v
ČR	ČR	kA	ČR
bývá	bývat	k5eAaImIp3nS	bývat
zanedbán	zanedbán	k2eAgInSc1d1	zanedbán
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
příčin	příčina	k1gFnPc2	příčina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Neznalost	neznalost	k1gFnSc1	neznalost
a	a	k8xC	a
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
meruňky	meruňka	k1gFnPc1	meruňka
není	být	k5eNaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
řezat	řezat	k5eAaImF	řezat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pragmatismus	pragmatismus	k1gInSc1	pragmatismus
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mladé	mladý	k2eAgFnPc4d1	mladá
meruňky	meruňka	k1gFnPc4	meruňka
je	být	k5eAaImIp3nS	být
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
řezat	řezat	k5eAaImF	řezat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zatím	zatím	k6eAd1	zatím
dobře	dobře	k6eAd1	dobře
rostou	růst	k5eAaImIp3nP	růst
a	a	k8xC	a
plodí	plodit	k5eAaImIp3nP	plodit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chybný	chybný	k2eAgInSc4d1	chybný
úsudek	úsudek	k1gInSc4	úsudek
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
si	se	k3xPyFc3	se
všímá	všímat	k5eAaImIp3nS	všímat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řezem	řez	k1gInSc7	řez
se	se	k3xPyFc4	se
u	u	k7c2	u
ovocných	ovocný	k2eAgInPc2d1	ovocný
stromů	strom	k1gInPc2	strom
snižuje	snižovat	k5eAaImIp3nS	snižovat
úroda	úroda	k1gFnSc1	úroda
a	a	k8xC	a
stromy	strom	k1gInPc1	strom
zbytečně	zbytečně	k6eAd1	zbytečně
příliš	příliš	k6eAd1	příliš
rostou	růst	k5eAaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obavy	obava	k1gFnPc1	obava
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
meruňky	meruňka	k1gFnPc1	meruňka
je	být	k5eAaImIp3nS	být
škodlivé	škodlivý	k2eAgNnSc1d1	škodlivé
řezat	řezat	k5eAaImF	řezat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
uhynout	uhynout	k5eAaPmF	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
opravdu	opravdu	k6eAd1	opravdu
může	moct	k5eAaImIp3nS	moct
výjimečně	výjimečně	k6eAd1	výjimečně
stát	stát	k5eAaPmF	stát
při	při	k7c6	při
nevhodně	vhodně	k6eNd1	vhodně
provedeném	provedený	k2eAgInSc6d1	provedený
silném	silný	k2eAgInSc6d1	silný
řezu	řez	k1gInSc6	řez
nebo	nebo	k8xC	nebo
neadekvátní	adekvátní	k2eNgFnSc3d1	neadekvátní
reakci	reakce	k1gFnSc3	reakce
dřeviny	dřevina	k1gFnSc2	dřevina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Efektivita	efektivita	k1gFnSc1	efektivita
řezu	řez	k1gInSc2	řez
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
meruňky	meruňka	k1gFnPc1	meruňka
je	být	k5eAaImIp3nS	být
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
řezat	řezat	k5eAaImF	řezat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
poškozené	poškozený	k2eAgFnPc1d1	poškozená
(	(	kIx(	(
<g/>
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
větve	větev	k1gFnPc1	větev
polámané	polámaný	k2eAgFnPc1d1	polámaná
nadúrodou	nadúroda	k1gFnSc7	nadúroda
a	a	k8xC	a
sněhem	sníh	k1gInSc7	sníh
či	či	k8xC	či
deštěm	dešť	k1gInSc7	dešť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemocné	mocný	k2eNgNnSc4d1	nemocné
(	(	kIx(	(
<g/>
klejotok	klejotok	k1gInSc4	klejotok
<g/>
)	)	kIx)	)
a	a	k8xC	a
prosychají	prosychat	k5eAaImIp3nP	prosychat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
stále	stále	k6eAd1	stále
zcela	zcela	k6eAd1	zcela
přirozeně	přirozeně	k6eAd1	přirozeně
znovu	znovu	k6eAd1	znovu
obrůstají	obrůstat	k5eAaImIp3nP	obrůstat
<g/>
,	,	kIx,	,
obává	obávat	k5eAaImIp3nS	obávat
se	se	k3xPyFc4	se
o	o	k7c4	o
zbytek	zbytek	k1gInSc4	zbytek
úrody	úroda	k1gFnSc2	úroda
a	a	k8xC	a
řez	řez	k1gInSc1	řez
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
neefektivní	efektivní	k2eNgFnPc4d1	neefektivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obnovování	obnovování	k1gNnSc4	obnovování
starých	starý	k2eAgInPc2d1	starý
meruňkových	meruňkový	k2eAgInPc2d1	meruňkový
sadů	sad	k1gInPc2	sad
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
při	při	k7c6	při
renovaci	renovace	k1gFnSc6	renovace
bývá	bývat	k5eAaImIp3nS	bývat
zorání	zorání	k1gNnSc1	zorání
pozemku	pozemek	k1gInSc2	pozemek
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Sad	sad	k1gInSc1	sad
pak	pak	k6eAd1	pak
bývá	bývat	k5eAaImIp3nS	bývat
důkladně	důkladně	k6eAd1	důkladně
seřezán	seřezán	k2eAgInSc1d1	seřezán
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
podrážející	podrážející	k2eAgInPc1d1	podrážející
výhony	výhon	k1gInPc1	výhon
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
zlomené	zlomený	k2eAgFnPc1d1	zlomená
a	a	k8xC	a
nemocné	nemocný	k2eAgFnPc1d1	nemocná
větve	větev	k1gFnPc1	větev
bývají	bývat	k5eAaImIp3nP	bývat
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
a	a	k8xC	a
koruny	koruna	k1gFnPc1	koruna
silně	silně	k6eAd1	silně
sníženy	snížen	k2eAgFnPc1d1	snížena
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
<g/>
-li	i	k?	-li
sad	sad	k1gInSc1	sad
stříhán	stříhán	k2eAgInSc1d1	stříhán
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
vhodné	vhodný	k2eAgNnSc1d1	vhodné
alespoň	alespoň	k9	alespoň
polovinu	polovina	k1gFnSc4	polovina
délky	délka	k1gFnSc2	délka
všech	všecek	k3xTgFnPc2	všecek
silných	silný	k2eAgFnPc2d1	silná
větví	větev	k1gFnPc2	větev
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hnojení	hnojení	k1gNnSc1	hnojení
===	===	k?	===
</s>
</p>
<p>
<s>
Nejčastějšími	častý	k2eAgNnPc7d3	nejčastější
hnojivy	hnojivo	k1gNnPc7	hnojivo
používanými	používaný	k2eAgFnPc7d1	používaná
v	v	k7c6	v
meruňkových	meruňkový	k2eAgInPc6d1	meruňkový
sadech	sad	k1gInPc6	sad
je	být	k5eAaImIp3nS	být
kostní	kostní	k2eAgFnSc1d1	kostní
moučka	moučka	k1gFnSc1	moučka
<g/>
,	,	kIx,	,
ledek	ledek	k1gInSc1	ledek
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc1	chlorid
draselný	draselný	k2eAgInSc1d1	draselný
a	a	k8xC	a
superfosfát	superfosfát	k1gInSc1	superfosfát
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
listy	list	k1gInPc1	list
stromů	strom	k1gInPc2	strom
světlé	světlý	k2eAgFnSc2d1	světlá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
růst	růst	k1gInSc1	růst
stromů	strom	k1gInPc2	strom
je	být	k5eAaImIp3nS	být
slabý	slabý	k2eAgInSc1d1	slabý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
aplikovat	aplikovat	k5eAaBmF	aplikovat
dusíkatá	dusíkatý	k2eAgNnPc4d1	dusíkaté
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
příznivý	příznivý	k2eAgInSc4d1	příznivý
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
plody	plod	k1gInPc1	plod
nejsou	být	k5eNaImIp3nP	být
až	až	k6eAd1	až
standardní	standardní	k2eAgNnPc1d1	standardní
v	v	k7c6	v
chuti	chuť	k1gFnSc6	chuť
a	a	k8xC	a
textuře	textura	k1gFnSc6	textura
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prospěšná	prospěšný	k2eAgFnSc1d1	prospěšná
aplikace	aplikace	k1gFnSc1	aplikace
superfosfátu	superfosfát	k1gInSc2	superfosfát
a	a	k8xC	a
chloridu	chlorid	k1gInSc2	chlorid
draselného	draselný	k2eAgInSc2d1	draselný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
==	==	k?	==
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
syrových	syrový	k2eAgFnPc6d1	syrová
meruňkách	meruňka	k1gFnPc6	meruňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
Bredská	Bredský	k2eAgFnSc1d1	Bredský
</s>
</p>
<p>
<s>
Karola	Karola	k1gFnSc1	Karola
</s>
</p>
<p>
<s>
Leskora	Leskora	k1gFnSc1	Leskora
</s>
</p>
<p>
<s>
Barbora	Barbora	k1gFnSc1	Barbora
</s>
</p>
<p>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
</s>
</p>
<p>
<s>
Paviot	Paviot	k1gMnSc1	Paviot
</s>
</p>
<p>
<s>
Rakovského	Rakovského	k2eAgFnSc1d1	Rakovského
</s>
</p>
<p>
<s>
Sabinovská	Sabinovský	k2eAgFnSc1d1	Sabinovská
</s>
</p>
<p>
<s>
Veecot	Veecot	k1gMnSc1	Veecot
</s>
</p>
<p>
<s>
Velkopavlovická	Velkopavlovický	k2eAgFnSc1d1	Velkopavlovická
</s>
</p>
<p>
<s>
Bergeron	Bergeron	k1gMnSc1	Bergeron
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ŘÍHA	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
ovoce	ovoce	k1gNnSc1	ovoce
:	:	kIx,	:
Díl	díl	k1gInSc1	díl
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Meruňky	meruňka	k1gFnPc1	meruňka
<g/>
,	,	kIx,	,
broskve	broskev	k1gFnPc1	broskev
<g/>
,	,	kIx,	,
srstky	srstka	k1gFnPc1	srstka
<g/>
,	,	kIx,	,
rybíz	rybíz	k1gInSc1	rybíz
<g/>
,	,	kIx,	,
maliny	malina	k1gFnPc1	malina
a	a	k8xC	a
ostružiny	ostružina	k1gFnPc1	ostružina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ovocnický	ovocnický	k2eAgInSc1d1	ovocnický
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
království	království	k1gNnSc4	království
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Meruňky	meruňka	k1gFnSc2	meruňka
<g/>
,	,	kIx,	,
s.	s.	k?	s.
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
meruňka	meruňka	k1gFnSc1	meruňka
obecná	obecný	k2eAgFnSc1d1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
meruňka	meruňka	k1gFnSc1	meruňka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
meruňka	meruňka	k1gFnSc1	meruňka
obecná	obecná	k1gFnSc1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
