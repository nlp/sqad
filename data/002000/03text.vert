<s>
Pojmem	pojem	k1gInSc7	pojem
datagram	datagram	k1gInSc1	datagram
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
datový	datový	k2eAgInSc1d1	datový
paket	paket	k1gInSc1	paket
v	v	k7c6	v
protokolu	protokol	k1gInSc6	protokol
IP	IP	kA	IP
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
analogie	analogie	k1gFnSc2	analogie
s	s	k7c7	s
telegramem	telegram	k1gInSc7	telegram
-	-	kIx~	-
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
telegramy	telegram	k1gInPc1	telegram
jsou	být	k5eAaImIp3nP	být
datagramy	datagram	k1gInPc1	datagram
přenášeny	přenášet	k5eAaImNgInP	přenášet
každý	každý	k3xTgMnSc1	každý
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
ostatních	ostatní	k2eAgFnPc6d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
může	moct	k5eAaImIp3nS	moct
každý	každý	k3xTgMnSc1	každý
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
datagramů	datagram	k1gInPc2	datagram
odeslaných	odeslaný	k2eAgInPc2d1	odeslaný
stejným	stejný	k2eAgInSc7d1	stejný
strojem	stroj	k1gInSc7	stroj
témuž	týž	k3xTgMnSc3	týž
adresátovi	adresát	k1gMnSc3	adresát
putovat	putovat	k5eAaImF	putovat
jinou	jiný	k2eAgFnSc7d1	jiná
cestou	cesta	k1gFnSc7	cesta
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
(	(	kIx(	(
<g/>
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
tak	tak	k6eAd1	tak
divoké	divoký	k2eAgFnPc4d1	divoká
změny	změna	k1gFnPc4	změna
nebývají	bývat	k5eNaImIp3nP	bývat
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
směrování	směrování	k1gNnSc4	směrování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
IPv	IPv	k1gFnSc2	IPv
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Hlavička	hlavička	k1gFnSc1	hlavička
IPv	IPv	k1gFnSc2	IPv
<g/>
6	[number]	k4	6
datagramu	datagram	k1gInSc2	datagram
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
-	-	kIx~	-
cílem	cíl	k1gInSc7	cíl
návrhu	návrh	k1gInSc2	návrh
bylo	být	k5eAaImAgNnS	být
umožnit	umožnit	k5eAaPmF	umožnit
její	její	k3xOp3gNnSc4	její
rychlé	rychlý	k2eAgNnSc4d1	rychlé
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
všechny	všechen	k3xTgFnPc1	všechen
zbytné	zbytný	k2eAgFnPc1d1	zbytná
položky	položka	k1gFnPc1	položka
i	i	k8xC	i
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
součet	součet	k1gInSc1	součet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přepočítávat	přepočítávat	k5eAaImF	přepočítávat
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
směrovači	směrovač	k1gInSc6	směrovač
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
životnost	životnost	k1gFnSc1	životnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
jen	jen	k9	jen
na	na	k7c4	na
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
(	(	kIx(	(
<g/>
40	[number]	k4	40
B	B	kA	B
proti	proti	k7c3	proti
původním	původní	k2eAgFnPc3d1	původní
20	[number]	k4	20
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
délka	délka	k1gFnSc1	délka
nesených	nesený	k2eAgFnPc2d1	nesená
adres	adresa	k1gFnPc2	adresa
je	být	k5eAaImIp3nS	být
čtyřnásobná	čtyřnásobný	k2eAgFnSc1d1	čtyřnásobná
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
následující	následující	k2eAgFnPc4d1	následující
položky	položka	k1gFnPc4	položka
<g/>
:	:	kIx,	:
Verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
Verze	verze	k1gFnSc1	verze
protokolu	protokol	k1gInSc2	protokol
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Třída	třída	k1gFnSc1	třída
provozu	provoz	k1gInSc2	provoz
<g/>
:	:	kIx,	:
Význam	význam	k1gInSc1	význam
není	být	k5eNaImIp3nS	být
pevně	pevně	k6eAd1	pevně
definován	definovat	k5eAaBmNgInS	definovat
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
má	mít	k5eAaImIp3nS	mít
sloužit	sloužit	k5eAaImF	sloužit
pro	pro	k7c4	pro
služby	služba	k1gFnPc4	služba
s	s	k7c7	s
definovanou	definovaný	k2eAgFnSc7d1	definovaná
kvalitou	kvalita	k1gFnSc7	kvalita
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
TOS	TOS	kA	TOS
v	v	k7c6	v
IPv	IPv	k1gFnSc6	IPv
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
toku	tok	k1gInSc2	tok
<g/>
:	:	kIx,	:
Koncepce	koncepce	k1gFnSc1	koncepce
toků	tok	k1gInPc2	tok
má	mít	k5eAaImIp3nS	mít
umožnit	umožnit	k5eAaPmF	umožnit
optimalizaci	optimalizace	k1gFnSc4	optimalizace
směrování	směrování	k1gNnSc2	směrování
pro	pro	k7c4	pro
sled	sled	k1gInSc4	sled
datagramů	datagram	k1gInPc2	datagram
tvořících	tvořící	k2eAgInPc2d1	tvořící
jeden	jeden	k4xCgMnSc1	jeden
logický	logický	k2eAgInSc1d1	logický
celek	celek	k1gInSc1	celek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přenos	přenos	k1gInSc4	přenos
souboru	soubor	k1gInSc2	soubor
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc4d1	přesný
význam	význam	k1gInSc4	význam
opět	opět	k6eAd1	opět
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgInS	být
definován	definovat	k5eAaBmNgInS	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
dat	datum	k1gNnPc2	datum
<g/>
:	:	kIx,	:
Délka	délka	k1gFnSc1	délka
datagramu	datagram	k1gInSc2	datagram
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nepočítá	počítat	k5eNaImIp3nS	počítat
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
úvodní	úvodní	k2eAgFnSc2d1	úvodní
20B	[number]	k4	20B
hlavička	hlavička	k1gFnSc1	hlavička
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
hlavička	hlavička	k1gFnSc1	hlavička
<g/>
:	:	kIx,	:
Rozšiřující	rozšiřující	k2eAgInPc1d1	rozšiřující
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
IPv	IPv	k1gFnSc6	IPv
<g/>
6	[number]	k4	6
přesunuty	přesunut	k2eAgFnPc4d1	přesunuta
do	do	k7c2	do
rozšiřujících	rozšiřující	k2eAgFnPc2d1	rozšiřující
hlaviček	hlavička	k1gFnPc2	hlavička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
připojují	připojovat	k5eAaImIp3nP	připojovat
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
hlavičku	hlavička	k1gFnSc4	hlavička
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zřetězeny	zřetězit	k5eAaPmNgFnP	zřetězit
položkami	položka	k1gFnPc7	položka
Další	další	k2eAgFnPc1d1	další
hlavička	hlavička	k1gFnSc1	hlavička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vždy	vždy	k6eAd1	vždy
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
typ	typ	k1gInSc4	typ
následující	následující	k2eAgFnSc2d1	následující
hlavičky	hlavička	k1gFnSc2	hlavička
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
hlavička	hlavička	k1gFnSc1	hlavička
pak	pak	k6eAd1	pak
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
položce	položka	k1gFnSc6	položka
nese	nést	k5eAaImIp3nS	nést
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
protokolu	protokol	k1gInSc6	protokol
vyšší	vysoký	k2eAgFnSc2d2	vyšší
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
data	datum	k1gNnPc4	datum
předána	předán	k2eAgNnPc4d1	předáno
při	při	k7c6	při
doručení	doručení	k1gNnSc4	doručení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tedy	tedy	k9	tedy
datagram	datagram	k1gInSc4	datagram
žádné	žádný	k3yNgFnSc2	žádný
rozšiřující	rozšiřující	k2eAgFnSc2d1	rozšiřující
hlavičky	hlavička	k1gFnSc2	hlavička
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
základní	základní	k2eAgFnSc1d1	základní
hlavička	hlavička	k1gFnSc1	hlavička
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
položce	položka	k1gFnSc6	položka
stanoví	stanovit	k5eAaPmIp3nS	stanovit
protokol	protokol	k1gInSc4	protokol
vyšší	vysoký	k2eAgFnSc2d2	vyšší
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Maximum	maximum	k1gNnSc1	maximum
skoků	skok	k1gInPc2	skok
<g/>
:	:	kIx,	:
Životnost	životnost	k1gFnSc4	životnost
datagramu	datagram	k1gInSc2	datagram
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
TTL	TTL	kA	TTL
v	v	k7c6	v
IPv	IPv	k1gFnSc6	IPv
<g/>
4	[number]	k4	4
zde	zde	k6eAd1	zde
každý	každý	k3xTgInSc1	každý
směrovač	směrovač	k1gInSc1	směrovač
zmenší	zmenšit	k5eAaPmIp3nS	zmenšit
hodnotu	hodnota	k1gFnSc4	hodnota
o	o	k7c4	o
jedničku	jednička	k1gFnSc4	jednička
a	a	k8xC	a
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
do	do	k7c2	do
nuly	nula	k1gFnSc2	nula
<g/>
,	,	kIx,	,
datagram	datagram	k1gInSc4	datagram
zahodí	zahodit	k5eAaPmIp3nP	zahodit
<g/>
.	.	kIx.	.
</s>
<s>
Adresa	adresa	k1gFnSc1	adresa
odesilatele	odesilatel	k1gMnSc2	odesilatel
<g/>
:	:	kIx,	:
IPv	IPv	k1gFnSc1	IPv
<g/>
6	[number]	k4	6
adresa	adresa	k1gFnSc1	adresa
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
datagram	datagram	k1gInSc4	datagram
vyslal	vyslat	k5eAaPmAgMnS	vyslat
<g/>
.	.	kIx.	.
</s>
<s>
Cílová	cílový	k2eAgFnSc1d1	cílová
adresa	adresa	k1gFnSc1	adresa
<g/>
:	:	kIx,	:
IPv	IPv	k1gFnSc1	IPv
<g/>
6	[number]	k4	6
adresa	adresa	k1gFnSc1	adresa
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
je	být	k5eAaImIp3nS	být
datagram	datagram	k1gInSc1	datagram
určen	určit	k5eAaPmNgInS	určit
<g/>
.	.	kIx.	.
</s>
