<s>
Zemské	zemský	k2eAgFnPc1d1	zemská
desky	deska	k1gFnPc1	deska
(	(	kIx(	(
<g/>
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
používán	používat	k5eAaImNgInS	používat
název	název	k1gInSc1	název
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
opačném	opačný	k2eAgNnSc6d1	opačné
pořadí	pořadí	k1gNnSc6	pořadí
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
desky	deska	k1gFnSc2	deska
zemské	zemský	k2eAgFnSc2d1	zemská
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zásadně	zásadně	k6eAd1	zásadně
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc4	ten
desky	deska	k1gFnPc4	deska
zemské	zemský	k2eAgFnSc2d1	zemská
<g/>
,	,	kIx,	,
v	v	k7c6	v
genitivu	genitiv	k1gInSc6	genitiv
používán	používán	k2eAgInSc4d1	používán
tvar	tvar	k1gInSc4	tvar
<g/>
:	:	kIx,	:
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
předchůdcem	předchůdce	k1gMnSc7	předchůdce
pozemkových	pozemkový	k2eAgFnPc2d1	pozemková
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rejstřík	rejstřík	k1gInSc1	rejstřík
a	a	k8xC	a
doklad	doklad	k1gInSc1	doklad
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
zpupná	zpupný	k2eAgNnPc1d1	zpupné
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
šlechtická	šlechtický	k2eAgNnPc1d1	šlechtické
<g/>
)	)	kIx)	)
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
(	(	kIx(	(
<g/>
allod	allod	k1gInSc1	allod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápisem	zápis	k1gInSc7	zápis
majetku	majetek	k1gInSc2	majetek
do	do	k7c2	do
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
se	se	k3xPyFc4	se
majetek	majetek	k1gInSc1	majetek
stával	stávat	k5eAaImAgInS	stávat
dědičným	dědičný	k2eAgInSc7d1	dědičný
majetkem	majetek	k1gInSc7	majetek
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
osvobozen	osvobodit	k5eAaPmNgMnS	osvobodit
od	od	k7c2	od
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deskách	deska	k1gFnPc6	deska
zemských	zemský	k2eAgFnPc2d1	zemská
byla	být	k5eAaImAgFnS	být
však	však	k9	však
také	také	k9	také
zapisována	zapisován	k2eAgFnSc1d1	zapisována
veškerá	veškerý	k3xTgFnSc1	veškerý
ustanovení	ustanovení	k1gNnPc4	ustanovení
zemských	zemský	k2eAgInPc2d1	zemský
sněmů	sněm	k1gInPc2	sněm
–	–	k?	–
tedy	tedy	k8xC	tedy
vlastně	vlastně	k9	vlastně
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
obecně	obecně	k6eAd1	obecně
závazné	závazný	k2eAgInPc1d1	závazný
dokumenty	dokument	k1gInPc1	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
desk	deska	k1gFnPc2	deska
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1348	[number]	k4	1348
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
řadách	řada	k1gFnPc6	řada
půhonných	půhonný	k2eAgFnPc6d1	půhonný
a	a	k8xC	a
trhových	trhový	k2eAgFnPc6d1	trhová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Čech	Čechy	k1gFnPc2	Čechy
byly	být	k5eAaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
současně	současně	k6eAd1	současně
v	v	k7c6	v
sídlech	sídlo	k1gNnPc6	sídlo
moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
obou	dva	k4xCgInPc2	dva
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
k	k	k7c3	k
zápisům	zápis	k1gInPc3	zápis
dochází	docházet	k5eAaImIp3nS	docházet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
také	také	k9	také
byly	být	k5eAaImAgInP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kvaterny	kvatern	k1gInPc1	kvatern
desk	deska	k1gFnPc2	deska
trhových	trhový	k2eAgFnPc2d1	trhová
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
vedeny	vést	k5eAaImNgInP	vést
jednotlivě	jednotlivě	k6eAd1	jednotlivě
podle	podle	k7c2	podle
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
zemském	zemský	k2eAgInSc6d1	zemský
archivu	archiv	k1gInSc6	archiv
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
jsou	být	k5eAaImIp3nP	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
dokumentem	dokument	k1gInSc7	dokument
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
nepřetržitě	přetržitě	k6eNd1	přetržitě
vedeném	vedený	k2eAgNnSc6d1	vedené
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
CHLUMECKÝ	chlumecký	k2eAgMnSc1d1	chlumecký
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
-	-	kIx~	-
CHYTIL	Chytil	k1gMnSc1	Chytil
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
-	-	kIx~	-
DEMUTH	DEMUTH	kA	DEMUTH
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
-	-	kIx~	-
WOLFSKRON	WOLFSKRON	kA	WOLFSKRON
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Landtafel	Landtafel	k1gInSc4	Landtafel
des	des	k1gNnSc2	des
Markgrafthumes	Markgrafthumesa	k1gFnPc2	Markgrafthumesa
Mähren	Mährna	k1gFnPc2	Mährna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
sv.	sv.	kA	sv.
řady	řada	k1gFnSc2	řada
brněnské	brněnský	k2eAgFnSc2d1	brněnská
1348	[number]	k4	1348
-	-	kIx~	-
1466	[number]	k4	1466
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
KALINA	Kalina	k1gMnSc1	Kalina
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
řady	řada	k1gFnSc2	řada
brněnské	brněnský	k2eAgFnSc2d1	brněnská
1480	[number]	k4	1480
-	-	kIx~	-
1566	[number]	k4	1566
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
ROHLÍK	Rohlík	k1gMnSc1	Rohlík
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
řady	řada	k1gFnSc2	řada
brněnské	brněnský	k2eAgFnSc2d1	brněnská
1567	[number]	k4	1567
-	-	kIx~	-
1641	[number]	k4	1641
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
CHLUMECKÝ	chlumecký	k2eAgMnSc1d1	chlumecký
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
-	-	kIx~	-
CHYTIL	Chytil	k1gMnSc1	Chytil
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
-	-	kIx~	-
DEMUTH	DEMUTH	kA	DEMUTH
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
-	-	kIx~	-
WOLFSKRON	WOLFSKRON	kA	WOLFSKRON
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Landtafel	Landtafel	k1gInSc4	Landtafel
des	des	k1gNnSc2	des
Markgrafthumes	Markgrafthumesa	k1gFnPc2	Markgrafthumesa
Mähren	Mährna	k1gFnPc2	Mährna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
sv.	sv.	kA	sv.
řady	řada	k1gFnSc2	řada
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
1348	[number]	k4	1348
-	-	kIx~	-
1466	[number]	k4	1466
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
MATĚJEK	MATĚJEK	kA	MATĚJEK
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
řady	řada	k1gFnSc2	řada
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
1480	[number]	k4	1480
-	-	kIx~	-
1566	[number]	k4	1566
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
MATĚJEK	MATĚJEK	kA	MATĚJEK
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
řady	řada	k1gFnSc2	řada
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
1567	[number]	k4	1567
-	-	kIx~	-
1642	[number]	k4	1642
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
BRANDL	BRANDL	kA	BRANDL
<g/>
,	,	kIx,	,
Vincenc	Vincenc	k1gMnSc1	Vincenc
-	-	kIx~	-
BRETHOLZ	BRETHOLZ	kA	BRETHOLZ
<g/>
,	,	kIx,	,
Bertold	Bertold	k1gInSc1	Bertold
<g/>
.	.	kIx.	.
</s>
<s>
Libri	Libri	k6eAd1	Libri
citationum	citationum	k1gInSc1	citationum
et	et	k?	et
sententiarum	sententiarum	k1gInSc1	sententiarum
seu	seu	k?	seu
Knihy	kniha	k1gFnSc2	kniha
půhonné	půhonný	k2eAgFnPc1d1	půhonný
a	a	k8xC	a
nálezové	nálezový	k2eAgFnPc1d1	nálezová
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
1872	[number]	k4	1872
<g/>
-	-	kIx~	-
<g/>
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Redakce	redakce	k1gFnSc1	redakce
Selského	selský	k2eAgInSc2d1	selský
archivu	archiv	k1gInSc2	archiv
[	[	kIx(	[
<g/>
i.	i.	k?	i.
e.	e.	k?	e.
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Odhadní	odhadní	k2eAgInPc1d1	odhadní
kvaterny	kvatern	k1gInPc1	kvatern
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
DEMUTH	DEMUTH	kA	DEMUTH
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
.	.	kIx.	.
</s>
<s>
Geschichte	Geschicht	k1gMnSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
Landtafel	Landtafel	k1gInSc1	Landtafel
im	im	k?	im
Markgrafthume	Markgrafthum	k1gInSc5	Markgrafthum
Mähren	Mährna	k1gFnPc2	Mährna
<g/>
.	.	kIx.	.
</s>
<s>
Brünn	Brünn	k1gInSc1	Brünn
:	:	kIx,	:
Nitsch	Nitsch	k1gInSc1	Nitsch
&	&	k?	&
Grosse	Gross	k1gMnSc2	Gross
<g/>
,	,	kIx,	,
1857	[number]	k4	1857
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ger	ger	k?	ger
<g/>
)	)	kIx)	)
JANIŠ	JANIŠ	kA	JANIŠ
<g/>
,	,	kIx,	,
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
.	.	kIx.	.
</s>
<s>
Úvahy	úvaha	k1gFnPc1	úvaha
nad	nad	k7c4	nad
počátky	počátek	k1gInPc4	počátek
zemských	zemský	k2eAgFnPc2d1	zemská
desk	deska	k1gFnPc2	deska
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
CHOCHOLÁČ	chocholáč	k1gMnSc1	chocholáč
<g/>
,	,	kIx,	,
Bronislav	Bronislav	k1gMnSc1	Bronislav
<g/>
;	;	kIx,	;
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
;	;	kIx,	;
KNOZ	KNOZ	kA	KNOZ
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
Mars	Mars	k1gInSc1	Mars
moravicus	moravicus	k1gInSc1	moravicus
aneb	aneb	k?	aneb
Sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
věnovali	věnovat	k5eAaPmAgMnP	věnovat
prof.	prof.	kA	prof.
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josefu	Josef	k1gMnSc3	Josef
Válkovi	Válek	k1gMnSc3	Válek
jeho	jeho	k3xOp3gNnSc4	jeho
žáci	žák	k1gMnPc1	žák
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
k	k	k7c3	k
sedmdesátinám	sedmdesátina	k1gFnPc3	sedmdesátina
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902304	[number]	k4	902304
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
243	[number]	k4	243
<g/>
-	-	kIx~	-
<g/>
250	[number]	k4	250
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
cze	cze	k?	cze
<g/>
)	)	kIx)	)
</s>
