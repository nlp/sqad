<s>
John	John	k1gMnSc1
Fabian	Fabian	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
McCreary	McCreara	k1gFnSc2
Fabian	Fabian	k1gMnSc1
Astronaut	astronaut	k1gMnSc1
NASA	NASA	kA
Státní	státní	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgNnSc4d1
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1939	#num#	k4
(	(	kIx(
<g/>
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Goose	Goose	k6eAd1
Creck	Creck	k1gInSc1
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Čas	čas	k1gInSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
13	#num#	k4
dní	den	k1gInPc2
4	#num#	k4
hodiny	hodina	k1gFnSc2
3	#num#	k4
minuty	minuta	k1gFnSc2
Kosmonaut	kosmonaut	k1gMnSc1
od	od	k7c2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1978	#num#	k4
Mise	mise	k1gFnSc1
</s>
<s>
STS-	STS-	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
STS-51-G	STS-51-G	k1gFnSc2
Znaky	znak	k1gInPc1
misí	mise	k1gFnPc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
McCreary	McCreary	k1gMnSc1
Fabian	Fabian	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1939	#num#	k4
v	v	k7c6
Goose	Goosa	k1gFnSc6
Crecku	Creck	k1gInSc2
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
americký	americký	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
a	a	k8xC
astronaut	astronaut	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
dvou	dva	k4xCgInPc2
letů	let	k1gInPc2
s	s	k7c7
raketoplánem	raketoplán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
a	a	k8xC
výcvik	výcvik	k1gInSc1
</s>
<s>
Po	po	k7c6
základní	základní	k2eAgFnSc6d1
a	a	k8xC
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
absolvoval	absolvovat	k5eAaPmAgMnS
University	universita	k1gFnSc2
of	of	k?
Washington	Washington	k1gInSc1
a	a	k8xC
Washington	Washington	k1gInSc1
State	status	k1gInSc5
University	universita	k1gFnSc2
a	a	k8xC
po	po	k7c6
obhajobě	obhajoba	k1gFnSc6
zde	zde	k6eAd1
získal	získat	k5eAaPmAgMnS
doktorát	doktorát	k1gInSc4
letectví	letectví	k1gNnSc2
a	a	k8xC
astronautiky	astronautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
asistentem	asistent	k1gMnSc7
profesora	profesor	k1gMnSc2
letectví	letectví	k1gNnSc2
na	na	k7c6
vojenské	vojenský	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
akademii	akademie	k1gFnSc6
v	v	k7c6
Coloradu	Colorado	k1gNnSc6
(	(	kIx(
<g/>
Air	Air	k1gFnSc1
Force	force	k1gFnSc2
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
týmu	tým	k1gInSc2
astronautů	astronaut	k1gMnPc2
byl	být	k5eAaImAgMnS
přijat	přijmout	k5eAaPmNgMnS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
celých	celý	k2eAgNnPc2d1
15	#num#	k4
let	léto	k1gNnPc2
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
let	let	k1gInSc4
čekal	čekat	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lety	let	k1gInPc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
120	#num#	k4
<g/>
.	.	kIx.
člověkem	člověk	k1gMnSc7
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1983	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c6
palubě	paluba	k1gFnSc6
raketoplánu	raketoplán	k1gInSc2
Challenger	Challenger	k1gInSc1
odstartoval	odstartovat	k5eAaPmAgInS
z	z	k7c2
Floridy	Florida	k1gFnSc2
k	k	k7c3
šestidenní	šestidenní	k2eAgFnSc3d1
misi	mise	k1gFnSc3
STS-	STS-	k1gFnSc1
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
tomto	tento	k3xDgNnSc6
složení	složení	k1gNnSc6
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Crippen	Crippen	k2eAgMnSc1d1
<g/>
,	,	kIx,
Frederick	Frederick	k1gMnSc1
Hauck	Hauck	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Fabian	Fabian	k1gMnSc1
<g/>
,	,	kIx,
Norman	Norman	k1gMnSc1
Thagard	Thagard	k1gMnSc1
<g/>
,	,	kIx,
Sally	Salla	k1gMnSc2
Rideová	Rideová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
letu	let	k1gInSc2
vypustili	vypustit	k5eAaPmAgMnP
dvě	dva	k4xCgFnPc4
družice	družice	k1gFnPc4
a	a	k8xC
jednu	jeden	k4xCgFnSc4
zas	zas	k9
později	pozdě	k6eAd2
několikrát	několikrát	k6eAd1
cvičně	cvičně	k6eAd1
zachytili	zachytit	k5eAaPmAgMnP
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
vrátili	vrátit	k5eAaPmAgMnP
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Podruhé	podruhé	k6eAd1
letěl	letět	k5eAaImAgMnS
na	na	k7c6
sedmidenní	sedmidenní	k2eAgFnSc6d1
misi	mise	k1gFnSc6
STS-51-G	STS-51-G	k1gFnSc2
na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
1985	#num#	k4
na	na	k7c6
palubě	paluba	k1gFnSc6
Discovery	Discovera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posádka	posádka	k1gFnSc1
raketoplánu	raketoplán	k1gInSc2
byla	být	k5eAaImAgFnS
<g/>
:	:	kIx,
Daniel	Daniel	k1gMnSc1
Brandenstein	Brandenstein	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Creighton	Creighton	k1gInSc1
<g/>
,	,	kIx,
Steven	Steven	k2eAgMnSc1d1
Nagel	Nagel	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Fabian	Fabian	k1gMnSc1
<g/>
,	,	kIx,
Shannon	Shannona	k1gFnPc2
Lucidová	Lucidový	k2eAgFnSc1d1
<g/>
,	,	kIx,
francouzský	francouzský	k2eAgMnSc1d1
astronaut	astronaut	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Baudry	Baudr	k1gInPc4
a	a	k8xC
synovec	synovec	k1gMnSc1
saúdskoarabského	saúdskoarabský	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
Sultan	Sultan	k1gInSc1
Al-Saud	Al-Saudo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vypustili	vypustit	k5eAaPmAgMnP
na	na	k7c6
orbitě	orbita	k1gFnSc6
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
družic	družice	k1gFnPc2
-	-	kIx~
Spartan	Spartan	k?
1	#num#	k4
<g/>
,	,	kIx,
Morelos	Morelos	k1gInSc1
1	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
Arabsat	Arabsat	k1gFnSc1
1B	1B	k4
,	,	kIx,
<g/>
Telstar	Telstar	k1gInSc1
3	#num#	k4
<g/>
D.	D.	kA
Provedli	provést	k5eAaPmAgMnP
také	také	k6eAd1
experimenty	experiment	k1gInPc1
pro	pro	k7c4
program	program	k1gInSc4
Hvězdných	hvězdný	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Oba	dva	k4xCgInPc1
lety	let	k1gInPc1
měly	mít	k5eAaImAgInP
start	start	k1gInSc4
na	na	k7c6
Floridě	Florida	k1gFnSc6
<g/>
,	,	kIx,
mys	mys	k1gInSc4
Canaveral	Canaveral	k1gFnPc2
<g/>
,	,	kIx,
přistání	přistání	k1gNnSc4
na	na	k7c6
základně	základna	k1gFnSc6
Edwards	Edwardsa	k1gFnPc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
obou	dva	k4xCgMnPc2
svých	svůj	k3xOyFgMnPc2
letů	let	k1gInPc2
strávil	strávit	k5eAaPmAgInS
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
13	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
STS-7	STS-7	k4
Challenger	Challenger	k1gInSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1983	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
STS-51-G	STS-51-G	k4
Discovery	Discovera	k1gFnPc1
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1985	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1985	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
letech	léto	k1gNnPc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
společnosti	společnost	k1gFnSc2
Analytic	Analytice	k1gFnPc2
Services	Services	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ANSER	ANSER	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
městě	město	k1gNnSc6
Arlington	Arlington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
;	;	kIx,
LÁLA	Lála	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kosmonauti-specialisté	Kosmonauti-specialista	k1gMnPc1
USA	USA	kA
<g/>
,	,	kIx,
s.	s.	k?
361	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Američtí	americký	k2eAgMnPc1d1
astronauti-kandidáti	astronauti-kandidát	k1gMnPc1
vybraní	vybraný	k2eAgMnPc1d1
v	v	k7c6
USA	USA	kA
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
458	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
John	John	k1gMnSc1
Fabian	Fabian	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
Kosmo	Kosma	k1gMnSc5
</s>
<s>
Na	na	k7c6
webu	web	k1gInSc6
Space	Space	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
