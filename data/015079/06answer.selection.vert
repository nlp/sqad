<s>
Ekonomicko-správní	ekonomickoprávní	k2eAgFnSc1d1	ekonomicko-správní
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
ECON	ECON	kA	ECON
MUNI	MUNI	k?	MUNI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fakulta	fakulta	k1gFnSc1	fakulta
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
informační	informační	k2eAgInPc4d1	informační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgNnSc4d1	finanční
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
,	,	kIx,	,
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
management	management	k1gInSc4	management
<g/>
,	,	kIx,	,
národní	národní	k2eAgNnSc4d1	národní
a	a	k8xC	a
podnikové	podnikový	k2eAgNnSc4d1	podnikové
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgInSc4d1	regionální
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc4	součást
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
