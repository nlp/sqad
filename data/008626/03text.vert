<p>
<s>
Ladění	ladění	k1gNnSc1	ladění
(	(	kIx(	(
<g/>
temperatura	temperatura	k1gFnSc1	temperatura
<g/>
)	)	kIx)	)
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
přesné	přesný	k2eAgFnPc4d1	přesná
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tónů	tón	k1gInPc2	tón
v	v	k7c6	v
hudební	hudební	k2eAgFnSc6d1	hudební
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Ladění	ladění	k1gNnSc1	ladění
tak	tak	k6eAd1	tak
určuje	určovat	k5eAaImIp3nS	určovat
frekvence	frekvence	k1gFnSc1	frekvence
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
poměry	poměr	k1gInPc1	poměr
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základy	základ	k1gInPc1	základ
ladění	ladění	k1gNnSc2	ladění
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Tón	tón	k1gInSc1	tón
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
charakteristika	charakteristika	k1gFnSc1	charakteristika
===	===	k?	===
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
zvuk	zvuk	k1gInSc1	zvuk
vzniká	vznikat	k5eAaImIp3nS	vznikat
chvěním	chvění	k1gNnSc7	chvění
nějakého	nějaký	k3yIgNnSc2	nějaký
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
kmitající	kmitající	k2eAgNnPc1d1	kmitající
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
,	,	kIx,	,
vydávají	vydávat	k5eAaPmIp3nP	vydávat
tóny	tón	k1gInPc4	tón
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
chvějící	chvějící	k2eAgFnSc1d1	chvějící
se	se	k3xPyFc4	se
struna	struna	k1gFnSc1	struna
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nepravidelně	pravidelně	k6eNd1	pravidelně
kmitající	kmitající	k2eAgNnPc1d1	kmitající
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
kmity	kmit	k1gInPc1	kmit
nemají	mít	k5eNaImIp3nP	mít
konstantní	konstantní	k2eAgFnSc4d1	konstantní
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
,	,	kIx,	,
vydávají	vydávat	k5eAaImIp3nP	vydávat
hluky	hluk	k1gInPc1	hluk
či	či	k8xC	či
šramoty	šramot	k1gInPc1	šramot
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
činely	činela	k1gFnPc4	činela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
charakteristiky	charakteristika	k1gFnPc1	charakteristika
tónu	tón	k1gInSc2	tón
jsou	být	k5eAaImIp3nP	být
výška	výška	k1gFnSc1	výška
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
teorii	teorie	k1gFnSc4	teorie
ladění	ladění	k1gNnSc2	ladění
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
pouze	pouze	k6eAd1	pouze
výška	výška	k1gFnSc1	výška
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
s	s	k7c7	s
laděním	ladění	k1gNnSc7	ladění
hluboce	hluboko	k6eAd1	hluboko
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
barva	barva	k1gFnSc1	barva
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
bude	být	k5eAaImBp3nS	být
objasněno	objasnit	k5eAaPmNgNnS	objasnit
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vnímaná	vnímaný	k2eAgFnSc1d1	vnímaná
výška	výška	k1gFnSc1	výška
tónu	tón	k1gInSc2	tón
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
především	především	k9	především
na	na	k7c4	na
frekvenci	frekvence	k1gFnSc4	frekvence
kmitání	kmitání	k1gNnSc2	kmitání
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
vyšší	vysoký	k2eAgMnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
kmitů	kmit	k1gInPc2	kmit
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
frekvence	frekvence	k1gFnSc1	frekvence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
vyšší	vysoký	k2eAgInPc4d2	vyšší
tón	tón	k1gInSc4	tón
vnímáme	vnímat	k5eAaImIp1nP	vnímat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podstata	podstata	k1gFnSc1	podstata
barvy	barva	k1gFnSc2	barva
tónu	tón	k1gInSc2	tón
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
tón	tón	k1gInSc1	tón
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
tónů	tón	k1gInPc2	tón
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
již	již	k6eAd1	již
jako	jako	k9	jako
samostatné	samostatný	k2eAgInPc1d1	samostatný
tóny	tón	k1gInPc1	tón
neslyšíme	slyšet	k5eNaImIp1nP	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tzv.	tzv.	kA	tzv.
alikvotní	alikvotní	k2eAgInPc1d1	alikvotní
tóny	tón	k1gInPc1	tón
(	(	kIx(	(
<g/>
též	též	k9	též
tzv.	tzv.	kA	tzv.
vyšší	vysoký	k2eAgFnSc1d2	vyšší
harmonické	harmonický	k2eAgFnPc4d1	harmonická
frekvence	frekvence	k1gFnPc4	frekvence
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
předmět	předmět	k1gInSc4	předmět
vydávající	vydávající	k2eAgInSc1d1	vydávající
zvuk	zvuk	k1gInSc1	zvuk
se	se	k3xPyFc4	se
chvěje	chvět	k5eAaImIp3nS	chvět
velmi	velmi	k6eAd1	velmi
složitým	složitý	k2eAgInSc7d1	složitý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
chvění	chvění	k1gNnSc2	chvění
vcelku	vcelku	k6eAd1	vcelku
zároveň	zároveň	k6eAd1	zároveň
odráží	odrážet	k5eAaImIp3nS	odrážet
i	i	k9	i
chvění	chvění	k1gNnSc1	chvění
stále	stále	k6eAd1	stále
menších	malý	k2eAgFnPc2d2	menší
a	a	k8xC	a
menších	malý	k2eAgFnPc2d2	menší
částí	část	k1gFnPc2	část
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Struna	struna	k1gFnSc1	struna
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
chvěje	chvět	k5eAaImIp3nS	chvět
celá	celý	k2eAgFnSc1d1	celá
(	(	kIx(	(
<g/>
schéma	schéma	k1gNnSc4	schéma
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
chvějí	chvět	k5eAaImIp3nP	chvět
i	i	k9	i
její	její	k3xOp3gFnPc1	její
poloviny	polovina	k1gFnPc1	polovina
(	(	kIx(	(
<g/>
schéma	schéma	k1gNnSc1	schéma
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetiny	třetina	k1gFnPc4	třetina
(	(	kIx(	(
<g/>
schéma	schéma	k1gNnSc4	schéma
3	[number]	k4	3
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
teoreticky	teoreticky	k6eAd1	teoreticky
až	až	k9	až
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
struny	struna	k1gFnSc2	struna
tedy	tedy	k8xC	tedy
vydávají	vydávat	k5eAaPmIp3nP	vydávat
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
<g/>
,	,	kIx,	,
sluchem	sluch	k1gInSc7	sluch
samostatně	samostatně	k6eAd1	samostatně
takřka	takřka	k6eAd1	takřka
nezachytitelné	zachytitelný	k2eNgInPc4d1	nezachytitelný
tóny	tón	k1gInPc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Součtem	součet	k1gInSc7	součet
těchto	tento	k3xDgNnPc2	tento
vlnění	vlnění	k1gNnPc2	vlnění
vzniká	vznikat	k5eAaImIp3nS	vznikat
složité	složitý	k2eAgNnSc4d1	složité
vlnění	vlnění	k1gNnSc4	vlnění
struny	struna	k1gFnSc2	struna
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
časový	časový	k2eAgInSc4d1	časový
průběh	průběh	k1gInSc4	průběh
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
výsledná	výsledný	k2eAgFnSc1d1	výsledná
barva	barva	k1gFnSc1	barva
tónu	tón	k1gInSc2	tón
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
alikvotní	alikvotní	k2eAgInPc1d1	alikvotní
tóny	tón	k1gInPc1	tón
silné	silný	k2eAgInPc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
se	se	k3xPyFc4	se
neomezuje	omezovat	k5eNaImIp3nS	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
struny	struna	k1gFnPc4	struna
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
i	i	k9	i
u	u	k7c2	u
trubice	trubice	k1gFnSc2	trubice
dechových	dechový	k2eAgInPc2d1	dechový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgFnPc4d1	lidská
hlasivky	hlasivka	k1gFnPc4	hlasivka
apod.	apod.	kA	apod.
Zvuková	zvukový	k2eAgFnSc1d1	zvuková
ukázka	ukázka	k1gFnSc1	ukázka
čistého	čistý	k2eAgInSc2d1	čistý
uměle	uměle	k6eAd1	uměle
syntetizovaného	syntetizovaný	k2eAgInSc2d1	syntetizovaný
sinusového	sinusový	k2eAgInSc2d1	sinusový
zvuku	zvuk	k1gInSc2	zvuk
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
alikvót	alikvóta	k1gFnPc2	alikvóta
<g/>
,	,	kIx,	,
odpovídajícího	odpovídající	k2eAgInSc2d1	odpovídající
první	první	k4xOgFnSc6	první
harmonické	harmonický	k2eAgFnSc6d1	harmonická
složce	složka	k1gFnSc6	složka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
schéma	schéma	k1gNnSc4	schéma
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zde	zde	k6eAd1	zde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uspořádání	uspořádání	k1gNnSc1	uspořádání
alikvotních	alikvotní	k2eAgInPc2d1	alikvotní
tónů	tón	k1gInPc2	tón
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
zásadní	zásadní	k2eAgInSc1d1	zásadní
pro	pro	k7c4	pro
teorii	teorie	k1gFnSc4	teorie
ladění	ladění	k1gNnSc2	ladění
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
principy	princip	k1gInPc1	princip
lze	lze	k6eAd1	lze
ukázat	ukázat	k5eAaPmF	ukázat
pomocí	pomocí	k7c2	pomocí
metody	metoda	k1gFnSc2	metoda
dělení	dělení	k1gNnSc2	dělení
struny	struna	k1gFnSc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
alikvotní	alikvotní	k2eAgInPc1d1	alikvotní
tóny	tón	k1gInPc1	tón
vznikají	vznikat	k5eAaImIp3nP	vznikat
samostatným	samostatný	k2eAgNnSc7d1	samostatné
chvěním	chvění	k1gNnSc7	chvění
poloviny	polovina	k1gFnSc2	polovina
<g/>
,	,	kIx,	,
třetiny	třetina	k1gFnSc2	třetina
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
zlomků	zlomek	k1gInPc2	zlomek
struny	struna	k1gFnSc2	struna
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
tělesa	těleso	k1gNnSc2	těleso
vydávajícího	vydávající	k2eAgNnSc2d1	vydávající
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
nic	nic	k3yNnSc1	nic
lehčího	lehký	k2eAgMnSc2d2	lehčí
<g/>
,	,	kIx,	,
než	než	k8xS	než
zkrátit	zkrátit	k5eAaPmF	zkrátit
strunu	struna	k1gFnSc4	struna
na	na	k7c4	na
příslušný	příslušný	k2eAgInSc4d1	příslušný
zlomek	zlomek	k1gInSc4	zlomek
a	a	k8xC	a
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
tón	tón	k1gInSc4	tón
takto	takto	k6eAd1	takto
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
struna	struna	k1gFnSc1	struna
vydává	vydávat	k5eAaImIp3nS	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Struna	struna	k1gFnSc1	struna
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
zní	znět	k5eAaImIp3nS	znět
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
výše	výše	k1gFnSc2	výše
než	než	k8xS	než
celá	celý	k2eAgFnSc1d1	celá
<g/>
;	;	kIx,	;
struna	struna	k1gFnSc1	struna
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
na	na	k7c4	na
třetinu	třetina	k1gFnSc4	třetina
zní	znět	k5eAaImIp3nS	znět
o	o	k7c4	o
oktávu	oktáva	k1gFnSc4	oktáva
a	a	k8xC	a
kvintu	kvinta	k1gFnSc4	kvinta
(	(	kIx(	(
<g/>
duodecimu	duodecima	k1gFnSc4	duodecima
<g/>
)	)	kIx)	)
výše	výše	k1gFnSc1	výše
<g/>
;	;	kIx,	;
struna	struna	k1gFnSc1	struna
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
na	na	k7c4	na
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
zní	znět	k5eAaImIp3nS	znět
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
oktávy	oktáva	k1gFnPc4	oktáva
výše	vysoce	k6eAd2	vysoce
a	a	k8xC	a
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Výšky	výška	k1gFnPc1	výška
prvních	první	k4xOgMnPc2	první
šestnácti	šestnáct	k4xCc2	šestnáct
harmonických	harmonický	k2eAgFnPc2d1	harmonická
frekvencí	frekvence	k1gFnPc2	frekvence
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
obrázek	obrázek	k1gInSc1	obrázek
vpravo	vpravo	k6eAd1	vpravo
dole	dole	k6eAd1	dole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Intervaly	interval	k1gInPc1	interval
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
tedy	tedy	k9	tedy
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
první	první	k4xOgFnSc7	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
harmonickou	harmonický	k2eAgFnSc7d1	harmonická
složkou	složka	k1gFnSc7	složka
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
mezi	mezi	k7c7	mezi
základním	základní	k2eAgInSc7d1	základní
tónem	tón	k1gInSc7	tón
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
alikvotním	alikvotní	k2eAgInPc3d1	alikvotní
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oktáva	oktáva	k1gFnSc1	oktáva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
interval	interval	k1gInSc1	interval
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
konsonantní	konsonantní	k2eAgNnSc1d1	konsonantní
<g/>
,	,	kIx,	,
že	že	k8xS	že
tóny	tón	k1gInPc1	tón
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
oktáv	oktáva	k1gFnPc2	oktáva
označujeme	označovat	k5eAaImIp1nP	označovat
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
např.	např.	kA	např.
C.	C.	kA	C.
Míra	Míra	k1gFnSc1	Míra
konsonantnosti	konsonantnost	k1gFnSc2	konsonantnost
intervalu	interval	k1gInSc2	interval
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
určit	určit	k5eAaPmF	určit
také	také	k9	také
ze	z	k7c2	z
součtu	součet	k1gInSc2	součet
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
čitateli	čitatel	k1gInSc6	čitatel
a	a	k8xC	a
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
zlomku	zlomek	k1gInSc2	zlomek
<g/>
,	,	kIx,	,
vyjadřujícího	vyjadřující	k2eAgInSc2d1	vyjadřující
poměr	poměr	k1gInSc1	poměr
frekvencí	frekvence	k1gFnSc7	frekvence
tónů	tón	k1gInPc2	tón
intervalu	interval	k1gInSc2	interval
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
unisonu	unisono	k1gNnSc6	unisono
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
frekvencí	frekvence	k1gFnSc7	frekvence
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
součet	součet	k1gInSc1	součet
2	[number]	k4	2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
další	další	k2eAgInSc1d1	další
nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
možný	možný	k2eAgInSc1d1	možný
poměr	poměr	k1gInSc1	poměr
právě	právě	k9	právě
u	u	k7c2	u
oktávy	oktáva	k1gFnSc2	oktáva
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
součet	součet	k1gInSc1	součet
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc7	třetí
harmonickou	harmonický	k2eAgFnSc7d1	harmonická
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
kvinta	kvinta	k1gFnSc1	kvinta
<g/>
;	;	kIx,	;
poměr	poměr	k1gInSc1	poměr
frekvencí	frekvence	k1gFnPc2	frekvence
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
porovnání	porovnání	k1gNnSc6	porovnání
schémat	schéma	k1gNnPc2	schéma
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
třetí	třetí	k4xOgFnSc7	třetí
a	a	k8xC	a
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
kvarta	kvarta	k1gFnSc1	kvarta
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
frekvencí	frekvence	k1gFnSc7	frekvence
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
a	a	k8xC	a
pátou	pátý	k4xOgFnSc7	pátý
složkou	složka	k1gFnSc7	složka
velká	velký	k2eAgFnSc1d1	velká
tercie	tercie	k1gFnSc1	tercie
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
frekvencí	frekvence	k1gFnSc7	frekvence
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
atd.	atd.	kA	atd.
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
i	i	k8xC	i
jiné	jiný	k2eAgFnSc6d1	jiná
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
i	i	k9	i
intervaly	interval	k1gInPc1	interval
i	i	k8xC	i
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
poměry	poměr	k1gInPc7	poměr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
velká	velký	k2eAgFnSc1d1	velká
sexta	sexta	k1gFnSc1	sexta
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
frekvencí	frekvence	k1gFnSc7	frekvence
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
obrázek	obrázek	k1gInSc4	obrázek
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
harmonických	harmonický	k2eAgFnPc2d1	harmonická
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
teorii	teorie	k1gFnSc4	teorie
ladění	ladění	k1gNnSc2	ladění
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
především	především	k9	především
intervaly	interval	k1gInPc1	interval
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
jedné	jeden	k4xCgFnSc2	jeden
oktávy	oktáva	k1gFnSc2	oktáva
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
poměru	poměr	k1gInSc2	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc4	dva
stejné	stejný	k2eAgInPc4d1	stejný
tóny	tón	k1gInPc4	tón
–	–	k?	–
unisono	unisono	k6eAd1	unisono
–	–	k?	–
čistá	čistý	k2eAgFnSc1d1	čistá
prima	prima	k1gFnSc1	prima
<g/>
)	)	kIx)	)
do	do	k7c2	do
poměru	poměr	k1gInSc2	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
(	(	kIx(	(
<g/>
čistá	čistý	k2eAgFnSc1d1	čistá
oktáva	oktáva	k1gFnSc1	oktáva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
decima	decima	k1gFnSc1	decima
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
frekvencí	frekvence	k1gFnSc7	frekvence
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc4	obrázek
s	s	k7c7	s
alikvotními	alikvotní	k2eAgInPc7d1	alikvotní
tóny	tón	k1gInPc7	tón
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
rozepsat	rozepsat	k5eAaPmF	rozepsat
jako	jako	k9	jako
oktáva	oktáva	k1gFnSc1	oktáva
+	+	kIx~	+
velká	velký	k2eAgFnSc1d1	velká
tercie	tercie	k1gFnSc1	tercie
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
=	=	kIx~	=
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
x	x	k?	x
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oktávové	oktávový	k2eAgFnSc6d1	oktávová
transpozici	transpozice	k1gFnSc6	transpozice
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
z	z	k7c2	z
intervalu	interval	k1gInSc2	interval
velké	velký	k2eAgFnSc2d1	velká
decimy	decima	k1gFnSc2	decima
odvodit	odvodit	k5eAaPmF	odvodit
velikost	velikost	k1gFnSc4	velikost
velké	velký	k2eAgFnSc2d1	velká
tercie	tercie	k1gFnSc2	tercie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudební	hudební	k2eAgFnSc2d1	hudební
stupnice	stupnice	k1gFnSc2	stupnice
a	a	k8xC	a
určování	určování	k1gNnSc2	určování
tónů	tón	k1gInPc2	tón
===	===	k?	===
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
stupnice	stupnice	k1gFnSc1	stupnice
dává	dávat	k5eAaImIp3nS	dávat
intervalům	interval	k1gInPc3	interval
určitý	určitý	k2eAgInSc1d1	určitý
řád	řád	k1gInSc1	řád
<g/>
;	;	kIx,	;
aby	aby	kYmCp3nP	aby
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyplnit	vyplnit	k5eAaPmF	vyplnit
oktávu	oktáva	k1gFnSc4	oktáva
řadou	řada	k1gFnSc7	řada
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dob	doba	k1gFnPc2	doba
značně	značně	k6eAd1	značně
rozmanitými	rozmanitý	k2eAgInPc7d1	rozmanitý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
stupnicím	stupnice	k1gFnPc3	stupnice
patří	patřit	k5eAaImIp3nS	patřit
pětitónová	pětitónový	k2eAgFnSc1d1	pětitónová
stupnice	stupnice	k1gFnSc1	stupnice
bez	bez	k7c2	bez
půltónů	půltón	k1gInPc2	půltón
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
anhemitonická	anhemitonický	k2eAgFnSc1d1	anhemitonický
pentatonika	pentatonika	k1gFnSc1	pentatonika
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
již	již	k9	již
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pythagorejském	pythagorejský	k2eAgNnSc6d1	pythagorejské
ladění	ladění	k1gNnSc6	ladění
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následujících	následující	k2eAgFnPc2d1	následující
kvint	kvinta	k1gFnPc2	kvinta
<g/>
:	:	kIx,	:
např.	např.	kA	např.
c-g-d-a-e	c	k1gFnSc6	c-g-d-a-e
<g/>
;	;	kIx,	;
po	po	k7c6	po
uspořádání	uspořádání	k1gNnSc6	uspořádání
do	do	k7c2	do
rozmezí	rozmezí	k1gNnSc2	rozmezí
jedné	jeden	k4xCgFnSc2	jeden
oktávy	oktáva	k1gFnSc2	oktáva
c-d-e-g-a	c-	k1gInSc2	c-d-e-g-
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
doplněná	doplněná	k1gFnSc1	doplněná
o	o	k7c4	o
tón	tón	k1gInSc4	tón
c.	c.	k?	c.
Pentatonika	pentatonika	k1gFnSc1	pentatonika
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
národů	národ	k1gInPc2	národ
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
řeckých	řecký	k2eAgFnPc2d1	řecká
stupnic	stupnice	k1gFnPc2	stupnice
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
tetrachordy	tetrachord	k1gInPc1	tetrachord
(	(	kIx(	(
<g/>
posloupnosti	posloupnost	k1gFnPc1	posloupnost
čtyř	čtyři	k4xCgInPc2	čtyři
tónů	tón	k1gInPc2	tón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělením	rozdělení	k1gNnSc7	rozdělení
tercií	tercie	k1gFnSc7	tercie
okolo	okolo	k7c2	okolo
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tetrachordy	tetrachord	k1gInPc1	tetrachord
složené	složený	k2eAgInPc1d1	složený
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
celých	celý	k2eAgInPc2d1	celý
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
půltónů	půltón	k1gInPc2	půltón
<g/>
;	;	kIx,	;
kombinací	kombinace	k1gFnPc2	kombinace
dvou	dva	k4xCgInPc2	dva
tetrachordů	tetrachord	k1gInPc2	tetrachord
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základ	základ	k1gInSc1	základ
středověkých	středověký	k2eAgFnPc2d1	středověká
stupnic	stupnice	k1gFnPc2	stupnice
–	–	k?	–
církevních	církevní	k2eAgInPc2d1	církevní
modů	modus	k1gInPc2	modus
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
hudba	hudba	k1gFnSc1	hudba
dospěla	dochvít	k5eAaPmAgFnS	dochvít
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
až	až	k9	až
ke	k	k7c3	k
dvanáctitónové	dvanáctitónový	k2eAgFnSc3d1	dvanáctitónová
chromatické	chromatický	k2eAgFnSc3d1	chromatická
stupnici	stupnice	k1gFnSc3	stupnice
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
půltónů	půltón	k1gInPc2	půltón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vytvořit	vytvořit	k5eAaPmF	vytvořit
tuto	tento	k3xDgFnSc4	tento
stupnici	stupnice	k1gFnSc4	stupnice
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všechny	všechen	k3xTgInPc1	všechen
intervaly	interval	k1gInPc1	interval
byly	být	k5eAaImAgInP	být
"	"	kIx"	"
<g/>
čisté	čistý	k2eAgNnSc1d1	čisté
<g/>
"	"	kIx"	"
však	však	k9	však
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
oktáva	oktáva	k1gFnSc1	oktáva
má	mít	k5eAaImIp3nS	mít
poměr	poměr	k1gInSc4	poměr
frekvencí	frekvence	k1gFnPc2	frekvence
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
=	=	kIx~	=
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kdybychom	kdyby	kYmCp1nP	kdyby
chtěli	chtít	k5eAaImAgMnP	chtít
sestavit	sestavit	k5eAaPmF	sestavit
stupnici	stupnice	k1gFnSc4	stupnice
např.	např.	kA	např.
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
čistých	čistý	k2eAgInPc2d1	čistý
půltónů	půltón	k1gInPc2	půltón
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
frekvencí	frekvence	k1gFnSc7	frekvence
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
půltón	půltón	k1gInSc4	půltón
nebude	být	k5eNaImBp3nS	být
souhlasit	souhlasit	k5eAaImF	souhlasit
s	s	k7c7	s
čistou	čistý	k2eAgFnSc7d1	čistá
oktávou	oktáva	k1gFnSc7	oktáva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bude	být	k5eAaImBp3nS	být
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půltón	půltón	k1gInSc4	půltón
výše	výše	k1gFnSc2	výše
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
12	[number]	k4	12
=	=	kIx~	=
2,169	[number]	k4	2,169
<g/>
425	[number]	k4	425
=	=	kIx~	=
141	[number]	k4	141
centů	cent	k1gInPc2	cent
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
nastává	nastávat	k5eAaImIp3nS	nastávat
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
intervalů	interval	k1gInPc2	interval
<g/>
.	.	kIx.	.
</s>
<s>
Nemožnost	nemožnost	k1gFnSc1	nemožnost
čistého	čistý	k2eAgNnSc2d1	čisté
rozdělení	rozdělení	k1gNnSc2	rozdělení
oktávy	oktáva	k1gFnSc2	oktáva
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
každý	každý	k3xTgInSc1	každý
systém	systém	k1gInSc1	systém
ladění	ladění	k1gNnSc4	ladění
obejít	obejít	k5eAaPmF	obejít
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
co	co	k9	co
možná	možná	k9	možná
nejpříjemnější	příjemný	k2eAgFnSc1d3	nejpříjemnější
pro	pro	k7c4	pro
poslech	poslech	k1gInSc4	poslech
<g/>
;	;	kIx,	;
každý	každý	k3xTgMnSc1	každý
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
v	v	k7c6	v
něčem	něco	k3yInSc6	něco
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
pythagorejském	pythagorejský	k2eAgNnSc6d1	pythagorejské
ladění	ladění	k1gNnSc6	ladění
existuje	existovat	k5eAaImIp3nS	existovat
tzv.	tzv.	kA	tzv.
Pythagorejské	pythagorejský	k2eAgNnSc1d1	pythagorejské
komma	komma	k1gNnSc1	komma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
"	"	kIx"	"
<g/>
barokních	barokní	k2eAgNnPc6d1	barokní
<g/>
"	"	kIx"	"
laděních	ladění	k1gNnPc6	ladění
zní	znět	k5eAaImIp3nS	znět
stupnice	stupnice	k1gFnSc1	stupnice
v	v	k7c6	v
tóninách	tónina	k1gFnPc6	tónina
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
od	od	k7c2	od
základní	základní	k2eAgFnSc2d1	základní
tóniny	tónina	k1gFnSc2	tónina
falešně	falešně	k6eAd1	falešně
apod.	apod.	kA	apod.
<g/>
;	;	kIx,	;
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
ladění	ladění	k1gNnPc2	ladění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používané	používaný	k2eAgNnSc4d1	používané
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
temperované	temperovaný	k2eAgNnSc4d1	temperované
ladění	ladění	k1gNnSc4	ladění
sice	sice	k8xC	sice
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
oktávu	oktáva	k1gFnSc4	oktáva
na	na	k7c4	na
dvanáct	dvanáct	k4xCc4	dvanáct
stejných	stejný	k2eAgInPc2d1	stejný
půltónových	půltónový	k2eAgInPc2d1	půltónový
intervalů	interval	k1gInPc2	interval
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
frekvencí	frekvence	k1gFnSc7	frekvence
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
12	[number]	k4	12
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
<g/>
[	[	kIx(	[
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
žádný	žádný	k3yNgInSc4	žádný
interval	interval	k1gInSc4	interval
kromě	kromě	k7c2	kromě
oktávy	oktáva	k1gFnSc2	oktáva
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
"	"	kIx"	"
<g/>
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
<g/>
"	"	kIx"	"
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgInPc4d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
alikvotních	alikvotní	k2eAgInPc6d1	alikvotní
tónech	tón	k1gInPc6	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čistá	čistý	k2eAgNnPc1d1	čisté
ladění	ladění	k1gNnPc1	ladění
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
čistá	čistý	k2eAgFnSc1d1	čistá
nebo	nebo	k8xC	nebo
také	také	k9	také
přirozená	přirozený	k2eAgNnPc1d1	přirozené
ladění	ladění	k1gNnPc1	ladění
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
ladění	ladění	k1gNnPc1	ladění
využívající	využívající	k2eAgNnPc1d1	využívající
pouze	pouze	k6eAd1	pouze
tóny	tón	k1gInPc7	tón
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
frekvence	frekvence	k1gFnPc4	frekvence
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
poměrech	poměr	k1gInPc6	poměr
vyjádřitelných	vyjádřitelný	k2eAgInPc6d1	vyjádřitelný
celými	celý	k2eAgNnPc7d1	celé
čísly	číslo	k1gNnPc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgInPc4d1	přesný
významy	význam	k1gInPc4	význam
těchto	tento	k3xDgInPc2	tento
termínů	termín	k1gInPc2	termín
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
poměrně	poměrně	k6eAd1	poměrně
nejednotné	jednotný	k2eNgFnPc4d1	nejednotná
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pythagorejské	pythagorejský	k2eAgNnSc4d1	pythagorejské
ladění	ladění	k1gNnSc4	ladění
–	–	k?	–
pro	pro	k7c4	pro
odvození	odvození	k1gNnSc4	odvození
všech	všecek	k3xTgInPc2	všecek
tónů	tón	k1gInPc2	tón
používá	používat	k5eAaImIp3nS	používat
oktávu	oktáva	k1gFnSc4	oktáva
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
kvintu	kvinta	k1gFnSc4	kvinta
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Didymické	Didymický	k2eAgNnSc4d1	Didymický
čisté	čistý	k2eAgNnSc4d1	čisté
ladění	ladění	k1gNnSc4	ladění
přidává	přidávat	k5eAaImIp3nS	přidávat
ještě	ještě	k6eAd1	ještě
interval	interval	k1gInSc4	interval
velké	velký	k2eAgFnSc2d1	velká
tercie	tercie	k1gFnSc2	tercie
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
Ladění	ladění	k1gNnSc4	ladění
s	s	k7c7	s
intervaly	interval	k1gInPc7	interval
obsahujícími	obsahující	k2eAgInPc7d1	obsahující
prvočísla	prvočíslo	k1gNnPc1	prvočíslo
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
5	[number]	k4	5
se	se	k3xPyFc4	se
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
použití	použití	k1gNnSc2	použití
čistých	čistý	k2eAgNnPc2d1	čisté
ladění	ladění	k1gNnPc2	ladění
mají	mít	k5eAaImIp3nP	mít
intervaly	interval	k1gInPc1	interval
"	"	kIx"	"
<g/>
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
<g/>
"	"	kIx"	"
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
danou	daný	k2eAgFnSc4d1	daná
poměrem	poměr	k1gInSc7	poměr
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
dostatečně	dostatečně	k6eAd1	dostatečně
malá	malý	k2eAgNnPc1d1	malé
<g/>
,	,	kIx,	,
znějí	znět	k5eAaImIp3nP	znět
souzvuky	souzvuk	k1gInPc4	souzvuk
"	"	kIx"	"
<g/>
čistě	čistě	k6eAd1	čistě
<g/>
"	"	kIx"	"
a	a	k8xC	a
přinášejí	přinášet	k5eAaImIp3nP	přinášet
velice	velice	k6eAd1	velice
příjemný	příjemný	k2eAgInSc4d1	příjemný
sluchový	sluchový	k2eAgInSc4d1	sluchový
vjem	vjem	k1gInSc4	vjem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
ladění	ladění	k1gNnPc1	ladění
mají	mít	k5eAaImIp3nP	mít
ale	ale	k8xC	ale
i	i	k9	i
své	svůj	k3xOyFgFnPc4	svůj
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
fakticky	fakticky	k6eAd1	fakticky
znemožňují	znemožňovat	k5eAaImIp3nP	znemožňovat
jejich	jejich	k3xOp3gNnSc4	jejich
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
současné	současný	k2eAgFnSc6d1	současná
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Důsledné	důsledný	k2eAgNnSc4d1	důsledné
uplatnění	uplatnění	k1gNnSc4	uplatnění
čistých	čistý	k2eAgNnPc2d1	čisté
ladění	ladění	k1gNnPc2	ladění
by	by	kYmCp3nP	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nekonečně	konečně	k6eNd1	konečně
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
tónů	tón	k1gInPc2	tón
v	v	k7c6	v
oktávě	oktáva	k1gFnSc6	oktáva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
použití	použití	k1gNnSc6	použití
omezeného	omezený	k2eAgInSc2d1	omezený
počtu	počet	k1gInSc2	počet
tónů	tón	k1gInPc2	tón
se	se	k3xPyFc4	se
v	v	k7c6	v
systému	systém	k1gInSc6	systém
ladění	ladění	k1gNnSc2	ladění
objevují	objevovat	k5eAaImIp3nP	objevovat
"	"	kIx"	"
<g/>
nečisté	čistý	k2eNgFnPc1d1	nečistá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nelibozvučné	libozvučný	k2eNgInPc1d1	nelibozvučný
intervaly	interval	k1gInPc1	interval
(	(	kIx(	(
<g/>
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
vlčí	vlčí	k2eAgInPc1d1	vlčí
intervaly	interval	k1gInPc1	interval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc1d1	přesné
frekvence	frekvence	k1gFnPc4	frekvence
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
tónů	tón	k1gInPc2	tón
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
stupnicích	stupnice	k1gFnPc6	stupnice
u	u	k7c2	u
čistých	čistý	k2eAgNnPc2d1	čisté
ladění	ladění	k1gNnPc2	ladění
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nota	nota	k1gFnSc1	nota
A	a	k9	a
v	v	k7c6	v
C	C	kA	C
dur	dur	k1gNnSc1	dur
nemá	mít	k5eNaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
frekvenci	frekvence	k1gFnSc4	frekvence
jako	jako	k8xC	jako
nota	nota	k1gFnSc1	nota
A	a	k9	a
v	v	k7c6	v
D	D	kA	D
dur	dur	k1gNnSc1	dur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
modulaci	modulace	k1gFnSc6	modulace
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
tóniny	tónina	k1gFnSc2	tónina
znějí	znět	k5eAaImIp3nP	znět
proto	proto	k8xC	proto
některé	některý	k3yIgInPc1	některý
intervaly	interval	k1gInPc1	interval
rozladěně	rozladěně	k6eAd1	rozladěně
<g/>
,	,	kIx,	,
čistá	čistý	k2eAgNnPc1d1	čisté
ladění	ladění	k1gNnPc1	ladění
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
složitější	složitý	k2eAgFnSc4d2	složitější
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgFnSc7d1	využívající
modulací	modulace	k1gFnSc7	modulace
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
nevyužitelná	využitelný	k2eNgFnSc1d1	nevyužitelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čistých	čistý	k2eAgNnPc6d1	čisté
laděních	ladění	k1gNnPc6	ladění
také	také	k6eAd1	také
není	být	k5eNaImIp3nS	být
možná	možná	k9	možná
enharmonická	enharmonický	k2eAgFnSc1d1	enharmonická
záměna	záměna	k1gFnSc1	záměna
většiny	většina	k1gFnSc2	většina
tónů	tón	k1gInPc2	tón
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
C	C	kA	C
<g/>
#	#	kIx~	#
není	být	k5eNaImIp3nS	být
Db	db	kA	db
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
působí	působit	k5eAaImIp3nP	působit
potíže	potíž	k1gFnPc1	potíž
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
a	a	k8xC	a
ladění	ladění	k1gNnSc6	ladění
nástrojů	nástroj	k1gInPc2	nástroj
s	s	k7c7	s
pevnými	pevný	k2eAgFnPc7d1	pevná
výškami	výška	k1gFnPc7	výška
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Temperovaná	temperovaný	k2eAgNnPc1d1	temperované
ladění	ladění	k1gNnPc1	ladění
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zmírnění	zmírnění	k1gNnSc4	zmírnění
některých	některý	k3yIgInPc2	některý
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgInPc2d1	uvedený
problémů	problém	k1gInPc2	problém
čistých	čistý	k2eAgNnPc2d1	čisté
ladění	ladění	k1gNnPc2	ladění
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
mnoho	mnoho	k6eAd1	mnoho
temperovaných	temperovaný	k2eAgNnPc2d1	temperované
ladění	ladění	k1gNnPc2	ladění
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
čisté	čistý	k2eAgInPc1d1	čistý
intervaly	interval	k1gInPc1	interval
záměrně	záměrně	k6eAd1	záměrně
rozladěny	rozladit	k5eAaPmNgInP	rozladit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
docílilo	docílit	k5eAaPmAgNnS	docílit
přesnějšího	přesný	k2eAgNnSc2d2	přesnější
naladění	naladění	k1gNnSc2	naladění
intervalů	interval	k1gInPc2	interval
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
předností	přednost	k1gFnSc7	přednost
temperovaných	temperovaný	k2eAgNnPc2d1	temperované
ladění	ladění	k1gNnPc2	ladění
je	být	k5eAaImIp3nS	být
umožnění	umožnění	k1gNnSc1	umožnění
modulace	modulace	k1gFnSc2	modulace
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
tónin	tónina	k1gFnPc2	tónina
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
objevovaly	objevovat	k5eAaImAgFnP	objevovat
disharmonické	disharmonický	k2eAgInPc4d1	disharmonický
vlčí	vlčí	k2eAgInPc4d1	vlčí
intervaly	interval	k1gInPc4	interval
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
těchto	tento	k3xDgNnPc2	tento
ladění	ladění	k1gNnPc2	ladění
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
enharmonická	enharmonický	k2eAgFnSc1d1	enharmonická
záměna	záměna	k1gFnSc1	záměna
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
D	D	kA	D
<g/>
#	#	kIx~	#
=	=	kIx~	=
Eb	Eb	k1gMnSc1	Eb
=	=	kIx~	=
Fbb	Fbb	k1gMnSc1	Fbb
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
omezit	omezit	k5eAaPmF	omezit
počet	počet	k1gInSc1	počet
tónů	tón	k1gInPc2	tón
v	v	k7c6	v
oktávě	oktáva	k1gFnSc6	oktáva
(	(	kIx(	(
<g/>
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
na	na	k7c4	na
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
ladění	ladění	k1gNnPc1	ladění
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
temperovaná	temperovaný	k2eAgNnPc1d1	temperované
ladění	ladění	k1gNnPc1	ladění
mají	mít	k5eAaImIp3nP	mít
frekvence	frekvence	k1gFnPc1	frekvence
tónů	tón	k1gInPc2	tón
upraveny	upraven	k2eAgInPc1d1	upraven
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
blízké	blízký	k2eAgFnPc1d1	blízká
tóniny	tónina	k1gFnPc1	tónina
od	od	k7c2	od
základní	základní	k2eAgFnSc2d1	základní
tóniny	tónina	k1gFnSc2	tónina
zněly	znět	k5eAaImAgInP	znět
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
a	a	k8xC	a
vzdálené	vzdálený	k2eAgFnPc1d1	vzdálená
tóniny	tónina	k1gFnPc1	tónina
alespoň	alespoň	k9	alespoň
použitelně	použitelně	k6eAd1	použitelně
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
intervaly	interval	k1gInPc1	interval
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
preferované	preferovaný	k2eAgFnPc1d1	preferovaná
<g/>
"	"	kIx"	"
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
temperované	temperovaný	k2eAgFnPc1d1	temperovaná
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vycházely	vycházet	k5eAaImAgFnP	vycházet
jako	jako	k9	jako
čisté	čistý	k2eAgFnPc1d1	čistá
<g/>
;	;	kIx,	;
některé	některý	k3yIgNnSc1	některý
zní	znět	k5eAaImIp3nS	znět
disonantněji	disonantně	k6eAd2	disonantně
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
ladění	ladění	k1gNnSc2	ladění
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Parejovo	Parejův	k2eAgNnSc4d1	Parejův
ladění	ladění	k1gNnSc4	ladění
(	(	kIx(	(
<g/>
1482	[number]	k4	1482
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Schlickovo	Schlickův	k2eAgNnSc4d1	Schlickův
ladění	ladění	k1gNnSc4	ladění
(	(	kIx(	(
<g/>
1511	[number]	k4	1511
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Grammateovo	Grammateův	k2eAgNnSc4d1	Grammateův
ladění	ladění	k1gNnSc4	ladění
(	(	kIx(	(
<g/>
1518	[number]	k4	1518
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Středotónové	Středotónový	k2eAgNnSc4d1	Středotónový
ladění	ladění	k1gNnSc4	ladění
(	(	kIx(	(
<g/>
nejrozšířenější	rozšířený	k2eAgNnSc4d3	nejrozšířenější
ladění	ladění	k1gNnSc4	ladění
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Werckmeisterovo	Werckmeisterův	k2eAgNnSc4d1	Werckmeisterův
ladění	ladění	k1gNnSc4	ladění
(	(	kIx(	(
<g/>
1691	[number]	k4	1691
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kirnbergerovo	Kirnbergerův	k2eAgNnSc4d1	Kirnbergerův
ladění	ladění	k1gNnSc4	ladění
(	(	kIx(	(
<g/>
1766,1771	[number]	k4	1766,1771
<g/>
,1779	,1779	k4	,1779
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Valottiho	Valottize	k6eAd1	Valottize
ladění	ladění	k1gNnSc4	ladění
</s>
</p>
<p>
<s>
Youngovo	Youngův	k2eAgNnSc1d1	Youngovo
laděníRovnoměrně	laděníRovnoměrně	k6eAd1	laděníRovnoměrně
temperované	temperovaný	k2eAgNnSc1d1	temperované
ladění	ladění	k1gNnSc1	ladění
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejpoužívanějším	používaný	k2eAgNnSc7d3	nejpoužívanější
laděním	ladění	k1gNnSc7	ladění
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
intervaly	interval	k1gInPc1	interval
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
kvinty	kvinta	k1gFnPc1	kvinta
<g/>
,	,	kIx,	,
kvarty	kvart	k1gInPc1	kvart
<g/>
,	,	kIx,	,
tercie	tercie	k1gFnPc1	tercie
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
velké	velký	k2eAgFnPc1d1	velká
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
"	"	kIx"	"
<g/>
rozladěné	rozladěný	k2eAgNnSc1d1	rozladěné
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádný	žádný	k3yNgInSc4	žádný
interval	interval	k1gInSc4	interval
kromě	kromě	k7c2	kromě
oktáv	oktáva	k1gFnPc2	oktáva
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
"	"	kIx"	"
<g/>
čistý	čistý	k2eAgInSc1d1	čistý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
všechny	všechen	k3xTgFnPc1	všechen
tóniny	tónina	k1gFnPc1	tónina
jsou	být	k5eAaImIp3nP	být
rovnocenné	rovnocenný	k2eAgFnPc1d1	rovnocenná
<g/>
,	,	kIx,	,
modulace	modulace	k1gFnSc1	modulace
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
do	do	k7c2	do
libovolně	libovolně	k6eAd1	libovolně
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
tónin	tónina	k1gFnPc2	tónina
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
kvalitu	kvalita	k1gFnSc4	kvalita
intervalů	interval	k1gInPc2	interval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Exotická	exotický	k2eAgNnPc1d1	exotické
ladění	ladění	k1gNnPc1	ladění
==	==	k?	==
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
indické	indický	k2eAgNnSc1d1	indické
ladění	ladění	k1gNnSc1	ladění
dělí	dělit	k5eAaImIp3nS	dělit
oktávu	oktáva	k1gFnSc4	oktáva
na	na	k7c4	na
22	[number]	k4	22
dílů	díl	k1gInPc2	díl
(	(	kIx(	(
<g/>
šruti	šrutit	k5eAaPmRp2nS	šrutit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
však	však	k9	však
základních	základní	k2eAgInPc2d1	základní
sedm	sedm	k4xCc4	sedm
stupňů	stupeň	k1gInPc2	stupeň
mnohdy	mnohdy	k6eAd1	mnohdy
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
evropské	evropský	k2eAgFnSc3d1	Evropská
diatonice	diatonika	k1gFnSc3	diatonika
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
arabská	arabský	k2eAgNnPc1d1	arabské
ladění	ladění	k1gNnPc1	ladění
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
ladění	ladění	k1gNnSc2	ladění
pythagorejského	pythagorejský	k2eAgNnSc2d1	pythagorejské
<g/>
,	,	kIx,	,
oktávu	oktáv	k1gInSc3	oktáv
však	však	k9	však
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
17	[number]	k4	17
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
teorii	teorie	k1gFnSc6	teorie
arabské	arabský	k2eAgFnSc2d1	arabská
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
dělení	dělení	k1gNnSc1	dělení
oktávy	oktáva	k1gFnSc2	oktáva
na	na	k7c4	na
24	[number]	k4	24
shodných	shodný	k2eAgInPc2d1	shodný
dílů	díl	k1gInPc2	díl
–	–	k?	–
temperovaných	temperovaný	k2eAgInPc2d1	temperovaný
čtvrttónů	čtvrttón	k1gInPc2	čtvrttón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
mnoha	mnoho	k4c2	mnoho
kultur	kultura	k1gFnPc2	kultura
není	být	k5eNaImIp3nS	být
harmonie	harmonie	k1gFnSc1	harmonie
a	a	k8xC	a
konsonance	konsonance	k1gFnSc2	konsonance
podstatným	podstatný	k2eAgInSc7d1	podstatný
prvkem	prvek	k1gInSc7	prvek
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Používané	používaný	k2eAgInPc1d1	používaný
hudební	hudební	k2eAgInPc1d1	hudební
nástroje	nástroj	k1gInPc1	nástroj
bývají	bývat	k5eAaImIp3nP	bývat
proto	proto	k8xC	proto
laděny	laděn	k2eAgFnPc4d1	laděna
zcela	zcela	k6eAd1	zcela
odlišným	odlišný	k2eAgInSc7d1	odlišný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
matematických	matematický	k2eAgInPc6d1	matematický
principech	princip	k1gInPc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
indonéské	indonéský	k2eAgInPc4d1	indonéský
systémy	systém	k1gInPc4	systém
sléndro	sléndro	k6eAd1	sléndro
a	a	k8xC	a
pélog	pélog	k1gInSc4	pélog
nepoužívají	používat	k5eNaImIp3nP	používat
čisté	čistý	k2eAgInPc1d1	čistý
oktávy	oktáv	k1gInPc1	oktáv
a	a	k8xC	a
přesné	přesný	k2eAgFnPc1d1	přesná
výšky	výška	k1gFnPc1	výška
tónů	tón	k1gInPc2	tón
se	se	k3xPyFc4	se
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
orchestrů	orchestr	k1gInPc2	orchestr
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
je	být	k5eAaImIp3nS	být
i	i	k9	i
situace	situace	k1gFnSc1	situace
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
ladění	ladění	k1gNnPc2	ladění
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
ladění	ladění	k1gNnSc2	ladění
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
ladění	ladění	k1gNnSc2	ladění
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
tradiční	tradiční	k2eAgInSc4d1	tradiční
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
ladění	ladění	k1gNnSc2	ladění
dalších	další	k1gNnPc2	další
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
používána	používán	k2eAgMnSc4d1	používán
v	v	k7c6	v
mikrotonální	mikrotonální	k2eAgFnSc6d1	mikrotonální
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohá	mnohé	k1gNnPc1	mnohé
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
základ	základ	k1gInSc4	základ
čistou	čistý	k2eAgFnSc4d1	čistá
oktávu	oktáva	k1gFnSc4	oktáva
<g/>
,	,	kIx,	,
dělí	dělit	k5eAaImIp3nP	dělit
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
počet	počet	k1gInSc4	počet
dílů	díl	k1gInPc2	díl
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
ladění	ladění	k1gNnSc1	ladění
čtvrttónové	čtvrttónový	k2eAgNnSc1d1	čtvrttónový
<g/>
,	,	kIx,	,
běžné	běžný	k2eAgNnSc1d1	běžné
je	být	k5eAaImIp3nS	být
i	i	k9	i
dělení	dělení	k1gNnSc1	dělení
na	na	k7c4	na
19	[number]	k4	19
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
,	,	kIx,	,
53	[number]	k4	53
nebo	nebo	k8xC	nebo
72	[number]	k4	72
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
ladění	ladění	k1gNnPc1	ladění
nemají	mít	k5eNaImIp3nP	mít
za	za	k7c4	za
základ	základ	k1gInSc4	základ
oktávu	oktáv	k1gInSc2	oktáv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiný	jiný	k2eAgInSc4d1	jiný
interval	interval	k1gInSc4	interval
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
patří	patřit	k5eAaImIp3nP	patřit
ladění	ladění	k1gNnSc4	ladění
Bohlen-Pierce	Bohlen-Pierka	k1gFnSc3	Bohlen-Pierka
vycházející	vycházející	k2eAgNnSc4d1	vycházející
z	z	k7c2	z
duodecimy	duodecima	k1gFnSc2	duodecima
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc2	ladění
88	[number]	k4	88
<g/>
-cet	eta	k1gFnPc2	-ceta
Garyho	Gary	k1gMnSc4	Gary
Morrisona	Morrison	k1gMnSc4	Morrison
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
intervalu	interval	k1gInSc6	interval
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
88	[number]	k4	88
centů	cent	k1gInPc2	cent
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stupnice	stupnice	k1gFnSc1	stupnice
alfa	alfa	k1gNnSc1	alfa
<g/>
,	,	kIx,	,
beta	beta	k1gNnSc1	beta
nebo	nebo	k8xC	nebo
gama	gama	k1gNnSc1	gama
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Wendy	Wend	k1gInPc4	Wend
Carlos	Carlosa	k1gFnPc2	Carlosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
i	i	k9	i
ladění	ladění	k1gNnSc4	ladění
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
po	po	k7c6	po
žádné	žádný	k3yNgFnSc6	žádný
transpozici	transpozice	k1gFnSc6	transpozice
neopakuje	opakovat	k5eNaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Software	software	k1gInSc1	software
==	==	k?	==
</s>
</p>
<p>
<s>
Scala	scát	k5eAaImAgFnS	scát
<g/>
:	:	kIx,	:
Program	program	k1gInSc4	program
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
<g/>
,	,	kIx,	,
analýze	analýza	k1gFnSc3	analýza
a	a	k8xC	a
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
ladění	ladění	k1gNnSc1	ladění
(	(	kIx(	(
<g/>
Linux	linux	k1gInSc1	linux
<g/>
,	,	kIx,	,
MacOS	MacOS	k1gFnSc1	MacOS
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Hudební	hudební	k2eAgFnSc1d1	hudební
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
,	,	kIx,	,
Freeware	freeware	k1gInSc1	freeware
</s>
</p>
<p>
<s>
Mutabor	Mutabor	k1gInSc1	Mutabor
<g/>
:	:	kIx,	:
Živé	živé	k1gNnSc1	živé
muzicírování	muzicírování	k?	muzicírování
s	s	k7c7	s
mikrotóny	mikrotón	k1gInPc7	mikrotón
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hermode	Hermod	k1gMnSc5	Hermod
tuning	tuning	k1gInSc1	tuning
<g/>
:	:	kIx,	:
Programem	program	k1gInSc7	program
řízené	řízený	k2eAgNnSc1d1	řízené
ladění	ladění	k1gNnSc1	ladění
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
o	o	k7c6	o
historických	historický	k2eAgNnPc6d1	historické
laděních	ladění	k1gNnPc6	ladění
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
významy	význam	k1gInPc1	význam
slova	slovo	k1gNnSc2	slovo
ladění	ladění	k1gNnSc2	ladění
==	==	k?	==
</s>
</p>
<p>
<s>
Výraz	výraz	k1gInSc1	výraz
ladění	ladění	k1gNnSc2	ladění
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
významech	význam	k1gInPc6	význam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
tón	tón	k1gInSc1	tón
daného	daný	k2eAgInSc2d1	daný
dechového	dechový	k2eAgInSc2d1	dechový
nástroje	nástroj	k1gInSc2	nástroj
–	–	k?	–
např.	např.	kA	např.
zobcové	zobcový	k2eAgFnPc4d1	zobcová
flétny	flétna	k1gFnPc4	flétna
bývají	bývat	k5eAaImIp3nP	bývat
buď	buď	k8xC	buď
v	v	k7c6	v
ladění	ladění	k1gNnSc6	ladění
C	C	kA	C
(	(	kIx(	(
<g/>
sopránová	sopránový	k2eAgFnSc1d1	sopránová
a	a	k8xC	a
tenorová	tenorový	k2eAgFnSc1d1	Tenorová
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
ladění	ladění	k1gNnSc6	ladění
F	F	kA	F
(	(	kIx(	(
<g/>
altová	altový	k2eAgNnPc1d1	altové
a	a	k8xC	a
basová	basový	k2eAgNnPc1d1	basové
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
dechových	dechový	k2eAgInPc2d1	dechový
nástrojů	nástroj	k1gInPc2	nástroj
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
ladění	ladění	k1gNnSc6	ladění
F	F	kA	F
(	(	kIx(	(
<g/>
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
B	B	kA	B
(	(	kIx(	(
<g/>
nejrozšířenější	rozšířený	k2eAgNnSc4d3	nejrozšířenější
ladění	ladění	k1gNnSc4	ladění
klarinetu	klarinet	k1gInSc2	klarinet
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
i	i	k9	i
osazení	osazení	k1gNnSc4	osazení
strunných	strunný	k2eAgInPc2d1	strunný
nástrojů	nástroj	k1gInPc2	nástroj
strunami	struna	k1gFnPc7	struna
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
výškou	výška	k1gFnSc7	výška
tónu	tón	k1gInSc2	tón
–	–	k?	–
např.	např.	kA	např.
dnešní	dnešní	k2eAgFnPc4d1	dnešní
housle	housle	k1gFnPc4	housle
mají	mít	k5eAaImIp3nP	mít
ladění	ladění	k1gNnSc3	ladění
g-d-a-e	g-	k1gInSc2	g-d-a-
<g/>
,	,	kIx,	,
kytary	kytara	k1gFnPc1	kytara
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
ladění	ladění	k1gNnSc6	ladění
e-a-d-g-h-e	et	k5eAaPmIp3nS	e-a-d-g-h-at
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
loutny	loutna	k1gFnSc2	loutna
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
historické	historický	k2eAgInPc4d1	historický
nástroje	nástroj	k1gInPc4	nástroj
používají	používat	k5eAaImIp3nP	používat
mnoho	mnoho	k6eAd1	mnoho
různých	různý	k2eAgNnPc2d1	různé
ladění	ladění	k1gNnPc2	ladění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
používá	používat	k5eAaImIp3nS	používat
hudebník	hudebník	k1gMnSc1	hudebník
nebo	nebo	k8xC	nebo
ladič	ladič	k1gMnSc1	ladič
při	při	k7c6	při
ladění	ladění	k1gNnSc6	ladění
hudebního	hudební	k2eAgInSc2d1	hudební
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ladění	ladění	k1gNnSc4	ladění
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
