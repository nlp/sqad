<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
úřad	úřad	k1gInSc1	úřad
(	(	kIx(	(
<g/>
NKÚ	NKÚ	kA	NKÚ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
orgán	orgán	k1gInSc1	orgán
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
kontrolu	kontrola	k1gFnSc4	kontrola
hospodaření	hospodaření	k1gNnSc2	hospodaření
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
plnění	plnění	k1gNnSc1	plnění
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
