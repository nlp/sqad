<s>
Pizzicato	pizzicato	k6eAd1	pizzicato
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
cimbálové	cimbálový	k2eAgFnSc2d1	cimbálová
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cimbalista	cimbalista	k1gMnSc1	cimbalista
rozeznívá	rozeznívat	k5eAaImIp3nS	rozeznívat
strunu	struna	k1gFnSc4	struna
pomocí	pomocí	k7c2	pomocí
drnkání	drnkání	k1gNnSc2	drnkání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většina	k1gFnSc7	většina
vlastními	vlastní	k2eAgInPc7d1	vlastní
nehty	nehet	k1gInPc7	nehet
či	či	k8xC	či
trsátkem	trsátko	k1gNnSc7	trsátko
<g/>
.	.	kIx.	.
</s>
