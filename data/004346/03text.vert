<s>
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Jasného	jasný	k2eAgMnSc2d1	jasný
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
osudy	osud	k1gInPc4	osud
vesnických	vesnický	k2eAgMnPc2d1	vesnický
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jen	jen	k6eAd1	jen
necelé	celý	k2eNgInPc4d1	necelý
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
podzimu	podzim	k1gInSc2	podzim
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
jej	on	k3xPp3gMnSc4	on
vidělo	vidět	k5eAaImAgNnS	vidět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
900	[number]	k4	900
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
ukryt	ukryt	k2eAgInSc1d1	ukryt
v	v	k7c6	v
trezoru	trezor	k1gInSc6	trezor
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
pádu	pád	k1gInSc2	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
nesměl	smět	k5eNaImAgMnS	smět
promítat	promítat	k5eAaImF	promítat
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
soustředěn	soustředit	k5eAaPmNgInS	soustředit
na	na	k7c4	na
proměny	proměna	k1gFnPc4	proměna
osudů	osud	k1gInPc2	osud
skupiny	skupina	k1gFnSc2	skupina
sedmi	sedm	k4xCc2	sedm
kamarádů	kamarád	k1gMnPc2	kamarád
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
moravském	moravský	k2eAgNnSc6d1	Moravské
městečku	městečko	k1gNnSc6	městečko
Kelč	Kelč	k1gFnSc4	Kelč
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
tady	tady	k6eAd1	tady
prožívají	prožívat	k5eAaImIp3nP	prožívat
euforii	euforie	k1gFnSc4	euforie
po	po	k7c6	po
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
večer	večer	k6eAd1	večer
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
scházejí	scházet	k5eAaImIp3nP	scházet
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
U	u	k7c2	u
Vola	vůl	k1gMnSc2	vůl
nad	nad	k7c7	nad
vínem	víno	k1gNnSc7	víno
a	a	k8xC	a
při	při	k7c6	při
muzice	muzika	k1gFnSc6	muzika
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
však	však	k9	však
únor	únor	k1gInSc4	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
znárodňování	znárodňování	k1gNnSc1	znárodňování
majetku	majetek	k1gInSc2	majetek
sedláků	sedlák	k1gMnPc2	sedlák
<g/>
,	,	kIx,	,
vytváření	vytváření	k1gNnSc1	vytváření
jednotných	jednotný	k2eAgNnPc2d1	jednotné
zemědělských	zemědělský	k2eAgNnPc2d1	zemědělské
družstev	družstvo	k1gNnPc2	družstvo
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
procesy	proces	k1gInPc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
přála	přát	k5eAaImAgFnS	přát
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
bezcharakterním	bezcharakterní	k2eAgMnPc3d1	bezcharakterní
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
národních	národní	k2eAgInPc2d1	národní
výborů	výbor	k1gInPc2	výbor
mohli	moct	k5eAaImAgMnP	moct
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
životech	život	k1gInPc6	život
a	a	k8xC	a
majetku	majetek	k1gInSc3	majetek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
naopak	naopak	k6eAd1	naopak
těžce	těžce	k6eAd1	těžce
postihla	postihnout	k5eAaPmAgFnS	postihnout
ztrátou	ztráta	k1gFnSc7	ztráta
blízkých	blízký	k2eAgMnPc2d1	blízký
a	a	k8xC	a
rozpadem	rozpad	k1gInSc7	rozpad
rodinných	rodinný	k2eAgInPc2d1	rodinný
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
jistot	jistota	k1gFnPc2	jistota
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
přátel	přítel	k1gMnPc2	přítel
z	z	k7c2	z
malého	malý	k2eAgNnSc2d1	malé
městečka	městečko	k1gNnSc2	městečko
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaPmIp3nS	rozpadat
a	a	k8xC	a
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
nesmiřitelné	smiřitelný	k2eNgInPc4d1	nesmiřitelný
tábory	tábor	k1gInPc4	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vzdorují	vzdorovat	k5eAaImIp3nP	vzdorovat
kolektivizačnímu	kolektivizační	k2eAgInSc3d1	kolektivizační
nátlaku	nátlak	k1gInSc3	nátlak
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
respektovaný	respektovaný	k2eAgMnSc1d1	respektovaný
sedlák	sedlák	k1gMnSc1	sedlák
František	František	k1gMnSc1	František
(	(	kIx(	(
<g/>
Radoslav	Radoslav	k1gMnSc1	Radoslav
Brzobohatý	Brzobohatý	k1gMnSc1	Brzobohatý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Bystré	bystrý	k2eAgFnSc6d1	bystrá
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Svitavy	Svitava	k1gFnPc1	Svitava
<g/>
,	,	kIx,	,
scény	scéna	k1gFnPc1	scéna
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
rychtou	rychta	k1gFnSc7	rychta
Zášínka	Zášínka	k1gFnSc1	Zášínka
(	(	kIx(	(
<g/>
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
natáčeny	natáčet	k5eAaImNgFnP	natáčet
ve	v	k7c6	v
Sněžném	sněžný	k2eAgInSc6d1	sněžný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
film	film	k1gInSc1	film
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
Kelč	Kelč	k1gFnSc1	Kelč
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Valašska	Valašsko	k1gNnSc2	Valašsko
a	a	k8xC	a
Hané	Haná	k1gFnSc2	Haná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
režisér	režisér	k1gMnSc1	režisér
Jasný	jasný	k2eAgMnSc1d1	jasný
trávil	trávit	k5eAaImAgMnS	trávit
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
lokace	lokace	k1gFnSc2	lokace
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
jednak	jednak	k8xC	jednak
kvůli	kvůli	k7c3	kvůli
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
Kelče	Kelč	k1gFnSc2	Kelč
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
kelčských	kelčský	k2eAgInPc2d1	kelčský
k	k	k7c3	k
natáčení	natáčení	k1gNnSc3	natáčení
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
komplikoval	komplikovat	k5eAaBmAgInS	komplikovat
obsazení	obsazení	k1gNnSc3	obsazení
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Bystrého	bystrý	k2eAgInSc2d1	bystrý
měli	mít	k5eAaImAgMnP	mít
potřebný	potřebný	k2eAgInSc4d1	potřebný
moravský	moravský	k2eAgInSc4d1	moravský
akcent	akcent	k1gInSc4	akcent
a	a	k8xC	a
s	s	k7c7	s
natáčením	natáčení	k1gNnSc7	natáčení
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Reálnou	reálný	k2eAgFnSc7d1	reálná
předlohou	předloha	k1gFnSc7	předloha
pro	pro	k7c4	pro
postavu	postava	k1gFnSc4	postava
Františka	František	k1gMnSc4	František
byl	být	k5eAaImAgMnS	být
sedlák	sedlák	k1gMnSc1	sedlák
z	z	k7c2	z
Kelče	Kelč	k1gInSc2	Kelč
František	František	k1gMnSc1	František
Slimáček	slimáček	k1gMnSc1	slimáček
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
R.	R.	kA	R.
Brzobohatý	Brzobohatý	k1gMnSc1	Brzobohatý
před	před	k7c7	před
natáčením	natáčení	k1gNnSc7	natáčení
důkladně	důkladně	k6eAd1	důkladně
seznámil	seznámit	k5eAaPmAgMnS	seznámit
<g/>
.	.	kIx.	.
</s>
<s>
Lampa	lampa	k1gFnSc1	lampa
<g/>
:	:	kIx,	:
Není	být	k5eNaImIp3nS	být
mi	já	k3xPp1nSc3	já
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
enem	enem	k?	enem
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
posrál	posrál	k1gMnSc1	posrál
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
;	;	kIx,	;
MFF	MFF	kA	MFF
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
1969	[number]	k4	1969
Čestná	čestný	k2eAgFnSc1d1	čestná
cena	cena	k1gFnSc1	cena
(	(	kIx(	(
<g/>
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
týden	týden	k1gInSc4	týden
Verona	Verona	k1gFnSc1	Verona
/	/	kIx~	/
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
1991	[number]	k4	1991
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
o	o	k7c4	o
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
česko-slovenský	českolovenský	k2eAgInSc4d1	česko-slovenský
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
století	století	k1gNnSc2	století
1998	[number]	k4	1998
Trilobit	trilobit	k1gMnSc1	trilobit
1968	[number]	k4	1968
za	za	k7c4	za
režii	režie	k1gFnSc4	režie
Vojtěchu	Vojtěch	k1gMnSc3	Vojtěch
Jasnému	jasný	k2eAgMnSc3d1	jasný
1968	[number]	k4	1968
Trilobit	trilobit	k1gMnSc1	trilobit
1968	[number]	k4	1968
za	za	k7c4	za
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
Vladimíru	Vladimír	k1gMnSc3	Vladimír
Menšíkovi	Menšík	k1gMnSc3	Menšík
Cena	cena	k1gFnSc1	cena
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1968	[number]	k4	1968
1969	[number]	k4	1969
Malé	Malé	k2eAgInPc1d1	Malé
zlaté	zlatý	k1gInPc1	zlatý
slunce	slunce	k1gNnSc2	slunce
Vladimíru	Vladimír	k1gMnSc3	Vladimír
Menšíkovi	Menšík	k1gMnSc3	Menšík
(	(	kIx(	(
<g/>
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
FFM	FFM	kA	FFM
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
Výroční	výroční	k2eAgFnSc1d1	výroční
cena	cena	k1gFnSc1	cena
Karel	Karel	k1gMnSc1	Karel
69	[number]	k4	69
časopisu	časopis	k1gInSc2	časopis
Kino	kino	k1gNnSc1	kino
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
Vladimíru	Vladimír	k1gMnSc3	Vladimír
Menšíkovi	Menšík	k1gMnSc3	Menšík
(	(	kIx(	(
<g/>
XX	XX	kA	XX
<g/>
.	.	kIx.	.
</s>
<s>
FFP	FFP	kA	FFP
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
do	do	k7c2	do
českých	český	k2eAgNnPc2d1	české
kin	kino	k1gNnPc2	kino
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
vrátili	vrátit	k5eAaPmAgMnP	vrátit
až	až	k6eAd1	až
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Filmový	filmový	k2eAgInSc1d1	filmový
přehled	přehled	k1gInSc1	přehled
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
<g/>
č.	č.	k?	č.
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
46	[number]	k4	46
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
opět	opět	k6eAd1	opět
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
kinech	kino	k1gNnPc6	kino
<g/>
,	,	kIx,	,
vidělo	vidět	k5eAaImAgNnS	vidět
jej	on	k3xPp3gMnSc4	on
3	[number]	k4	3
150	[number]	k4	150
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Všichni	všechen	k3xTgMnPc1	všechen
dobří	dobrý	k2eAgMnPc1d1	dobrý
rodáci	rodák	k1gMnPc1	rodák
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databas	k1gInSc6	Databas
</s>
