<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1906	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
Oficiální	oficiální	k2eAgNnSc1d1
web	web	k1gInSc4
</s>
<s>
www.iec.ch	www.iec.ch	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
(	(	kIx(
<g/>
IEC	IEC	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
International	International	k1gMnSc1
Electrotechnical	Electrotechnical	k1gFnSc2
Commission	Commission	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Commission	Commission	k1gInSc1
électrotechnique	électrotechniquat	k5eAaPmIp3nS
internationale	internationale	k6eAd1
CEI	CEI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
světová	světový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
založená	založený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vypracovává	vypracovávat	k5eAaImIp3nS
a	a	k8xC
publikuje	publikovat	k5eAaBmIp3nS
mezinárodní	mezinárodní	k2eAgFnPc4d1
normy	norma	k1gFnPc4
pro	pro	k7c4
elektrotechniku	elektrotechnika	k1gFnSc4
<g/>
,	,	kIx,
elektroniku	elektronika	k1gFnSc4
<g/>
,	,	kIx,
sdělovací	sdělovací	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
a	a	k8xC
příbuzné	příbuzný	k2eAgInPc4d1
obory	obor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Jejím	její	k3xOp3gNnSc7
posláním	poslání	k1gNnSc7
je	být	k5eAaImIp3nS
propagovat	propagovat	k5eAaImF
a	a	k8xC
podporovat	podporovat	k5eAaImF
mezinárodní	mezinárodní	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
v	v	k7c6
otázkách	otázka	k1gFnPc6
normalizace	normalizace	k1gFnSc2
elektrotechniky	elektrotechnika	k1gFnSc2
a	a	k8xC
příbuzných	příbuzný	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existence	existence	k1gFnSc1
mezinárodních	mezinárodní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
omezování	omezování	k1gNnSc3
obchodních	obchodní	k2eAgFnPc2d1
bariér	bariéra	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
otvírání	otvírání	k1gNnSc3
nových	nový	k2eAgInPc2d1
trhů	trh	k1gInPc2
a	a	k8xC
podpoře	podpora	k1gFnSc3
hospodářského	hospodářský	k2eAgInSc2d1
růstu	růst	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Normy	Norma	k1gFnSc2
IEC	IEC	kA
by	by	kYmCp3nS
měly	mít	k5eAaImAgInP
přispívat	přispívat	k5eAaImF
k	k	k7c3
bezpečnosti	bezpečnost	k1gFnSc3
<g/>
,	,	kIx,
ochraně	ochrana	k1gFnSc3
zdraví	zdraví	k1gNnSc2
a	a	k8xC
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
IEC	IEC	kA
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
IEC	IEC	kA
sídlila	sídlit	k5eAaImAgFnS
původně	původně	k6eAd1
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
Ženevy	Ženeva	k1gFnSc2
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEC	IEC	kA
má	mít	k5eAaImIp3nS
regionální	regionální	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
v	v	k7c6
Asii	Asie	k1gFnSc6
(	(	kIx(
<g/>
Singapore	Singapor	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Sã	Sã	k6eAd1
Paulo	Paula	k1gFnSc5
<g/>
,	,	kIx,
Brazílie	Brazílie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
(	(	kIx(
<g/>
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
IEC	IEC	kA
navrhla	navrhnout	k5eAaPmAgFnS
systém	systém	k1gInSc4
měr	míra	k1gFnPc2
a	a	k8xC
vah	váha	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
mezinárodní	mezinárodní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
jednotek	jednotka	k1gFnPc2
SI	se	k3xPyFc3
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
IEC	IEC	kA
udržuje	udržovat	k5eAaImIp3nS
vícejazyčný	vícejazyčný	k2eAgInSc4d1
slovník	slovník	k1gInSc4
elektrotechnických	elektrotechnický	k2eAgInPc2d1
termínů	termín	k1gInPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
účelem	účel	k1gInSc7
bylo	být	k5eAaImAgNnS
sjednotit	sjednotit	k5eAaPmF
terminologii	terminologie	k1gFnSc4
v	v	k7c6
oboru	obor	k1gInSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
norem	norma	k1gFnPc2
</s>
<s>
Technickou	technický	k2eAgFnSc4d1
práci	práce	k1gFnSc4
v	v	k7c6
IEC	IEC	kA
provádí	provádět	k5eAaImIp3nS
asi	asi	k9
200	#num#	k4
technických	technický	k2eAgFnPc2d1
komisí	komise	k1gFnPc2
a	a	k8xC
subkomisí	subkomise	k1gFnPc2
a	a	k8xC
asi	asi	k9
700	#num#	k4
pracovních	pracovní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technické	technický	k2eAgFnPc4d1
komise	komise	k1gFnPc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
působnosti	působnost	k1gFnSc6
vypracovávají	vypracovávat	k5eAaImIp3nP
technické	technický	k2eAgInPc4d1
dokumenty	dokument	k1gInPc4
se	s	k7c7
specifickým	specifický	k2eAgNnSc7d1
zaměřením	zaměření	k1gNnSc7
a	a	k8xC
ty	ten	k3xDgInPc1
se	se	k3xPyFc4
potom	potom	k6eAd1
předkládají	předkládat	k5eAaImIp3nP
národním	národní	k2eAgInPc3d1
komitétům	komitét	k1gInPc3
(	(	kIx(
<g/>
členům	člen	k1gMnPc3
IEC	IEC	kA
<g/>
)	)	kIx)
k	k	k7c3
hlasování	hlasování	k1gNnSc3
s	s	k7c7
cílem	cíl	k1gInSc7
jejich	jejich	k3xOp3gNnSc2
schválení	schválení	k1gNnSc2
jako	jako	k8xC,k8xS
mezinárodní	mezinárodní	k2eAgFnSc2d1
normy	norma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
technické	technický	k2eAgFnSc6d1
práci	práce	k1gFnSc6
IEC	IEC	kA
podílí	podílet	k5eAaImIp3nS
asi	asi	k9
10	#num#	k4
000	#num#	k4
odborníků	odborník	k1gMnPc2
na	na	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Normy	Norma	k1gFnPc1
IEC	IEC	kA
mají	mít	k5eAaImIp3nP
čísla	číslo	k1gNnPc4
v	v	k7c6
rozsahu	rozsah	k1gInSc6
60000	#num#	k4
<g/>
–	–	k?
<g/>
79999	#num#	k4
a	a	k8xC
názvy	název	k1gInPc4
tvaru	tvar	k1gInSc2
IEC	IEC	kA
60417	#num#	k4
<g/>
:	:	kIx,
Graphical	Graphical	k1gMnPc1
symbols	symbolsa	k1gFnPc2
for	forum	k1gNnPc2
use	usus	k1gInSc5
on	on	k3xPp3gMnSc1
equipment	equipment	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
starších	starý	k2eAgInPc2d2
IEC	IEC	kA
norem	norma	k1gFnPc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
přečíslováno	přečíslovat	k5eAaPmNgNnS
přičtením	přičtení	k1gNnSc7
60000	#num#	k4
<g/>
,	,	kIx,
takže	takže	k8xS
např.	např.	kA
původní	původní	k2eAgFnSc1d1
norma	norma	k1gFnSc1
IEC	IEC	kA
27	#num#	k4
má	mít	k5eAaImIp3nS
nyní	nyní	k6eAd1
označení	označení	k1gNnSc2
IEC	IEC	kA
60027	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
IEC	IEC	kA
úzce	úzko	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
Mezinárodní	mezinárodní	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
(	(	kIx(
<g/>
ISO	ISO	kA
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
Mezinárodní	mezinárodní	k2eAgFnSc7d1
telekomunikační	telekomunikační	k2eAgFnSc7d1
unií	unie	k1gFnSc7
(	(	kIx(
<g/>
ITU	ITU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgMnSc2
spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
několika	několik	k4yIc7
významnými	významný	k2eAgFnPc7d1
standardizačními	standardizační	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
IEEE	IEEE	kA
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yRgInPc7,k3yQgInPc7,k3yIgInPc7
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
podepsala	podepsat	k5eAaPmAgFnS
smlouvu	smlouva	k1gFnSc4
o	o	k7c6
spolupráci	spolupráce	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
doplněna	doplnit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zahrnovala	zahrnovat	k5eAaImAgFnS
společné	společný	k2eAgFnPc4d1
vývojové	vývojový	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Normy	Norma	k1gFnPc1
vytvořené	vytvořený	k2eAgFnPc1d1
v	v	k7c6
součinnosti	součinnost	k1gFnSc6
s	s	k7c7
ISO	ISO	kA
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
26300	#num#	k4
<g/>
,	,	kIx,
Open	Open	k1gInSc1
Document	Document	k1gInSc1
Format	Format	k1gInSc1
for	forum	k1gNnPc2
Office	Office	kA
Applications	Applications	k1gInSc1
(	(	kIx(
<g/>
OpenPocument	OpenPocument	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
<g/>
1.0	1.0	k4
nesou	nést	k5eAaImIp3nP
v	v	k7c6
označení	označení	k1gNnSc6
zkratku	zkratka	k1gFnSc4
obou	dva	k4xCgFnPc2
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
nesou	nést	k5eAaImIp3nP
publikace	publikace	k1gFnPc1
vytvořené	vytvořený	k2eAgNnSc1d1
Technickým	technický	k2eAgInSc7d1
výborem	výbor	k1gInSc7
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
Joint	Joint	k1gMnSc1
Technical	Technical	k1gMnSc1
Committee	Committee	k1gInSc4
1	#num#	k4
pro	pro	k7c4
informační	informační	k2eAgFnSc4d1
technologie	technologie	k1gFnPc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
normy	norma	k1gFnPc1
vytvořené	vytvořený	k2eAgFnPc1d1
„	„	k?
<g/>
Komisí	komise	k1gFnSc7
pro	pro	k7c4
posuzování	posuzování	k1gNnSc4
shody	shoda	k1gFnSc2
<g/>
“	“	k?
ISO	ISO	kA
CASCO	CASCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgFnPc4d1
normy	norma	k1gFnPc4
vytvořené	vytvořený	k2eAgFnPc4d1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
mezi	mezi	k7c7
IEC	IEC	kA
a	a	k8xC
ISO	ISO	kA
mají	mít	k5eAaImIp3nP
čísla	číslo	k1gNnPc4
v	v	k7c6
sérii	série	k1gFnSc6
80000	#num#	k4
<g/>
,	,	kIx,
například	například	k6eAd1
IEC	IEC	kA
82045	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Normy	Norma	k1gFnPc1
série	série	k1gFnSc1
60000	#num#	k4
vycházející	vycházející	k2eAgInPc1d1
z	z	k7c2
evropských	evropský	k2eAgFnPc2d1
norem	norma	k1gFnPc2
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
také	také	k9
označení	označení	k1gNnSc4
začínající	začínající	k2eAgFnSc2d1
EN	EN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
IEC	IEC	kA
60034	#num#	k4
je	být	k5eAaImIp3nS
také	také	k9
EN	EN	kA
60034	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
IEC	IEC	kA
normy	norma	k1gFnPc1
jsou	být	k5eAaImIp3nP
také	také	k9
přebírány	přebírán	k2eAgFnPc1d1
harmonizované	harmonizovaný	k2eAgFnPc1d1
normy	norma	k1gFnPc1
vytvořené	vytvořený	k2eAgFnPc1d1
jinými	jiný	k2eAgFnPc7d1
standardizačními	standardizační	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
BSI	BSI	kA
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
CSA	CSA	kA
(	(	kIx(
<g/>
Kanada	Kanada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
UL	ul	kA
&	&	k?
ANSI	ANSI	kA
<g/>
/	/	kIx~
<g/>
INCITS	INCITS	kA
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
SABS	SABS	kA
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
SAI	SAI	kA
(	(	kIx(
<g/>
Austrálie	Austrálie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
SPC	SPC	kA
<g/>
/	/	kIx~
<g/>
GB	GB	kA
(	(	kIx(
<g/>
Čína	Čína	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
DIN	din	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Normy	Norma	k1gFnSc2
IEC	IEC	kA
harmonizované	harmonizovaný	k2eAgInPc1d1
jinými	jiný	k2eAgFnPc7d1
standardizačními	standardizační	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
odlišovat	odlišovat	k5eAaImF
od	od	k7c2
původních	původní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členství	členství	k1gNnSc1
</s>
<s>
plné	plný	k2eAgNnSc4d1
členství	členství	k1gNnSc4
přidružené	přidružený	k2eAgNnSc4d1
členství	členství	k1gNnSc4
účastníci	účastník	k1gMnPc1
Affiliate	Affiliat	k1gInSc5
Country	country	k2eAgFnPc1d1
Programme	Programme	k1gFnSc1
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
má	mít	k5eAaImIp3nS
IEC	IEC	kA
82	#num#	k4
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
mezi	mezi	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
patří	patřit	k5eAaImIp3nS
i	i	k9
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgInPc2d1
82	#num#	k4
států	stát	k1gInPc2
má	mít	k5eAaImIp3nS
přidružené	přidružený	k2eAgNnSc1d1
členství	členství	k1gNnSc1
(	(	kIx(
<g/>
Affiliate	Affiliat	k1gMnSc5
Country	country	k2eAgFnPc1d1
Programme	Programme	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Členy	člen	k1gInPc1
IEC	IEC	kA
jsou	být	k5eAaImIp3nP
národní	národní	k2eAgInPc1d1
komitéty	komitét	k1gInPc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
national	nationat	k5eAaImAgInS,k5eAaPmAgInS
committees	committees	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
reprezentují	reprezentovat	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
národní	národní	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
v	v	k7c6
oboru	obor	k1gInSc6
(	(	kIx(
<g/>
měly	mít	k5eAaImAgInP
by	by	kYmCp3nP
reprezentovat	reprezentovat	k5eAaImF
výrobce	výrobce	k1gMnPc4
<g/>
,	,	kIx,
poskytovatele	poskytovatel	k1gMnPc4
<g/>
,	,	kIx,
distributory	distributor	k1gMnPc4
<g/>
,	,	kIx,
spotřebitele	spotřebitel	k1gMnPc4
<g/>
,	,	kIx,
uživatele	uživatel	k1gMnPc4
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
<g/>
,	,	kIx,
profesní	profesní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
a	a	k8xC
národní	národní	k2eAgFnSc2d1
standardizační	standardizační	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
IEC	IEC	kA
Statutes	Statutes	k1gMnSc1
and	and	k?
Rules	Rules	k1gMnSc1
of	of	k?
Procedure	Procedur	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEC	IEC	kA
<g/>
,	,	kIx,
2011-07-01	2011-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
International	International	k1gMnPc4
Electrotechnical	Electrotechnical	k1gFnSc2
Vocabulary	Vocabulara	k1gFnSc2
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.iec.ch	www.iec.ch	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mezinárodní	mezinárodní	k2eAgFnSc1d1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
IEC	IEC	kA
Home	Home	k1gFnSc1
Page	Page	k1gFnSc1
</s>
<s>
Free	Free	k6eAd1
online	onlinout	k5eAaPmIp3nS
multilingual	multilingual	k1gMnSc1
dictionary	dictionara	k1gFnSc2
of	of	k?
20	#num#	k4
000	#num#	k4
electrical	electricat	k5eAaPmAgMnS
and	and	k?
electronic	electronice	k1gFnPc2
terms	terms	k6eAd1
</s>
<s>
IEC	IEC	kA
System	Systo	k1gNnSc7
for	forum	k1gNnPc2
quality	qualita	k1gFnSc2
assessment	assessment	k1gInSc1
of	of	k?
electronic	electronice	k1gFnPc2
components	components	k6eAd1
and	and	k?
associated	associated	k1gInSc1
materials	materials	k1gInSc1
and	and	k?
processes	processes	k1gInSc1
</s>
<s>
IEC	IEC	kA
Scheme	Schem	k1gInSc5
for	forum	k1gNnPc2
certification	certification	k1gInSc1
to	ten	k3xDgNnSc4
standards	standards	k6eAd1
for	forum	k1gNnPc2
electrical	electricat	k5eAaPmAgInS
equipment	equipment	k1gInSc1
for	forum	k1gNnPc2
explosive	explosivat	k5eAaPmIp3nS
atmospheres	atmospheres	k1gInSc1
</s>
<s>
List	list	k1gInSc1
of	of	k?
IEC	IEC	kA
Technical	Technical	k1gMnSc1
Committees	Committees	k1gMnSc1
on	on	k3xPp3gMnSc1
IEC	IEC	kA
Official	Official	k1gInSc1
Website	Websit	k1gInSc5
</s>
<s>
WWW	WWW	kA
stránky	stránka	k1gFnPc1
technických	technický	k2eAgInPc2d1
komitétů	komitét	k1gInPc2
</s>
<s>
https://web.archive.org/web/20190910054947/http://tc3.iec.ch/	https://web.archive.org/web/20190910054947/http://tc3.iec.ch/	k4
—	—	k?
IEC	IEC	kA
Graphical	Graphical	k1gMnPc2
Symbols	Symbolsa	k1gFnPc2
</s>
<s>
https://web.archive.org/web/20181117164610/http://tc4.iec.ch/	https://web.archive.org/web/20181117164610/http://tc4.iec.ch/	k4
—	—	k?
IEC	IEC	kA
Hydraulic	Hydraulice	k1gFnPc2
Turbines	Turbinesa	k1gFnPc2
</s>
<s>
https://web.archive.org/web/20190910055302/http://tc17.iec.ch/	https://web.archive.org/web/20190910055302/http://tc17.iec.ch/	k4
—	—	k?
IEC	IEC	kA
Switchgear	Switchgear	k1gMnSc1
</s>
<s>
http://tc56.iec.ch	http://tc56.iec.ch	k1gInSc1
—	—	k?
IEC	IEC	kA
Dependability	Dependabilita	k1gFnSc2
</s>
<s>
https://web.archive.org/web/20170713220304/http://tc57.iec.ch/	https://web.archive.org/web/20170713220304/http://tc57.iec.ch/	k4
—	—	k?
IEC	IEC	kA
Power	Power	k1gInSc1
Systems	Systems	k1gInSc1
Management	management	k1gInSc1
</s>
<s>
https://web.archive.org/web/20070114210453/http://tc86.iec.ch/	https://web.archive.org/web/20070114210453/http://tc86.iec.ch/	k4
—	—	k?
IEC	IEC	kA
Fibre	Fibr	k1gInSc5
Optics	Optics	k1gInSc1
</s>
<s>
https://web.archive.org/web/20191030185034/http://tc100.iec.ch/	https://web.archive.org/web/20191030185034/http://tc100.iec.ch/	k4
—	—	k?
IEC	IEC	kA
Multimedia	multimedium	k1gNnPc1
</s>
<s>
Normy	Norma	k1gFnPc1
IEC	IEC	kA
a	a	k8xC
nástroje	nástroj	k1gInPc1
v	v	k7c6
databázovém	databázový	k2eAgInSc6d1
formátu	formát	k1gInSc6
</s>
<s>
International	Internationat	k5eAaPmAgMnS,k5eAaImAgMnS
Electrotechnical	Electrotechnical	k1gMnSc1
Vocabulary	Vocabulara	k1gFnSc2
</s>
<s>
IEC	IEC	kA
Glossary	Glossar	k1gInPc1
</s>
<s>
IEC	IEC	kA
60061	#num#	k4
<g/>
:	:	kIx,
Lamp	lampa	k1gFnPc2
caps	caps	k1gInSc1
<g/>
,	,	kIx,
lampholders	lampholders	k1gInSc1
and	and	k?
gauges	gauges	k1gInSc1
</s>
<s>
IEC	IEC	kA
60417	#num#	k4
-	-	kIx~
ISO	ISO	kA
7000	#num#	k4
<g/>
:	:	kIx,
Graphical	Graphical	k1gMnPc1
Symbols	Symbolsa	k1gFnPc2
for	forum	k1gNnPc2
Use	usus	k1gInSc5
on	on	k3xPp3gMnSc1
Equipment	Equipment	k1gInSc4
</s>
<s>
IEC	IEC	kA
60617	#num#	k4
<g/>
:	:	kIx,
Graphical	Graphical	k1gMnPc1
Symbols	Symbolsa	k1gFnPc2
for	forum	k1gNnPc2
Diagrams	Diagramsa	k1gFnPc2
</s>
<s>
IEC	IEC	kA
61360	#num#	k4
<g/>
:	:	kIx,
Component	Component	k1gInSc1
Data	datum	k1gNnSc2
Dictionary	Dictionara	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2003181180	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
7235-7	7235-7	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1956	#num#	k4
1631	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81008302	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
133254239	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81008302	#num#	k4
</s>
