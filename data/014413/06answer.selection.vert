<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
(	(	kIx(
<g/>
IEC	IEC	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
International	International	k1gMnSc1
Electrotechnical	Electrotechnical	k1gFnSc2
Commission	Commission	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Commission	Commission	k1gInSc1
électrotechnique	électrotechniquat	k5eAaPmIp3nS
internationale	internationale	k6eAd1
CEI	CEI	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
světová	světový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
založená	založený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vypracovává	vypracovávat	k5eAaImIp3nS
a	a	k8xC
publikuje	publikovat	k5eAaBmIp3nS
mezinárodní	mezinárodní	k2eAgFnPc4d1
normy	norma	k1gFnPc4
pro	pro	k7c4
elektrotechniku	elektrotechnika	k1gFnSc4
<g/>
,	,	kIx,
elektroniku	elektronika	k1gFnSc4
<g/>
,	,	kIx,
sdělovací	sdělovací	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
a	a	k8xC
příbuzné	příbuzný	k2eAgInPc4d1
obory	obor	k1gInPc4
<g/>
.	.	kIx.
</s>