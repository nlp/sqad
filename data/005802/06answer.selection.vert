<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgFnPc1	čtyři
právnické	právnický	k2eAgFnPc1d1	právnická
fakulty	fakulta	k1gFnPc1	fakulta
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
:	:	kIx,	:
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
Fakulta	fakulta	k1gFnSc1	fakulta
právnická	právnický	k2eAgFnSc1d1	právnická
Západočeské	západočeský	k2eAgFnPc4d1	Západočeská
univerzity	univerzita	k1gFnPc4	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
