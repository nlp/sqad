<s>
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
studovat	studovat	k5eAaImF	studovat
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Absolvent	absolvent	k1gMnSc1	absolvent
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
právníkem	právník	k1gMnSc7	právník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
vedle	vedle	k7c2	vedle
fakulty	fakulta	k1gFnSc2	fakulta
teologické	teologický	k2eAgFnSc2d1	teologická
a	a	k8xC	a
lékařské	lékařský	k2eAgFnSc2d1	lékařská
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
základních	základní	k2eAgFnPc2d1	základní
odborných	odborný	k2eAgFnPc2d1	odborná
fakult	fakulta	k1gFnPc2	fakulta
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
absolvování	absolvování	k1gNnSc4	absolvování
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
nejdříve	dříve	k6eAd3	dříve
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
fakultu	fakulta	k1gFnSc4	fakulta
artistickou	artistický	k2eAgFnSc4d1	artistická
<g/>
.	.	kIx.	.
</s>
<s>
Vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
právu	právo	k1gNnSc3	právo
římskému	římský	k2eAgNnSc3d1	římské
i	i	k9	i
kanonickému	kanonický	k2eAgNnSc3d1	kanonické
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
obojího	obojí	k4xRgMnSc2	obojí
práva	právo	k1gNnSc2	právo
-	-	kIx~	-
JUDr.	JUDr.	kA	JUDr.
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
právnické	právnický	k2eAgFnPc1d1	právnická
fakulty	fakulta	k1gFnPc1	fakulta
reformovány	reformován	k2eAgFnPc1d1	reformována
<g/>
.	.	kIx.	.
</s>
<s>
Vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
jak	jak	k6eAd1	jak
předmětům	předmět	k1gInPc3	předmět
historickoprávním	historickoprávní	k2eAgInPc3d1	historickoprávní
(	(	kIx(	(
<g/>
právní	právní	k2eAgFnPc4d1	právní
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
římské	římský	k2eAgNnSc4d1	římské
právo	právo	k1gNnSc4	právo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
platnému	platný	k2eAgNnSc3d1	platné
právu	právo	k1gNnSc3	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
souvisejícím	související	k2eAgInPc3d1	související
předmětům	předmět	k1gInPc3	předmět
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
národohospodářská	národohospodářský	k2eAgFnSc1d1	Národohospodářská
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
trvalo	trvat	k5eAaImAgNnS	trvat
8	[number]	k4	8
semestrů	semestr	k1gInPc2	semestr
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
9	[number]	k4	9
semestrů	semestr	k1gInPc2	semestr
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyučování	vyučování	k1gNnSc1	vyučování
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
formou	forma	k1gFnSc7	forma
přednášek	přednáška	k1gFnPc2	přednáška
a	a	k8xC	a
seminářů	seminář	k1gInPc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
mohl	moct	k5eAaImAgMnS	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusel	muset	k5eNaImAgMnS	muset
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
studia	studio	k1gNnSc2	studio
absolvovat	absolvovat	k5eAaPmF	absolvovat
tři	tři	k4xCgFnPc4	tři
státní	státní	k2eAgFnPc4d1	státní
zkoušky	zkouška	k1gFnPc4	zkouška
(	(	kIx(	(
<g/>
historickoprávní	historickoprávní	k2eAgFnSc4d1	historickoprávní
<g/>
,	,	kIx,	,
judiciální	judiciální	k2eAgFnSc4d1	judiciální
a	a	k8xC	a
státovědeckou	státovědecký	k2eAgFnSc4d1	státovědecký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
poté	poté	k6eAd1	poté
opravňovaly	opravňovat	k5eAaImAgFnP	opravňovat
k	k	k7c3	k
zaměstnání	zaměstnání	k1gNnSc3	zaměstnání
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chtěl	chtít	k5eAaImAgMnS	chtít
navíc	navíc	k6eAd1	navíc
získat	získat	k5eAaPmF	získat
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
kromě	kromě	k7c2	kromě
nich	on	k3xPp3gFnPc2	on
úspěšně	úspěšně	k6eAd1	úspěšně
projít	projít	k5eAaPmF	projít
i	i	k9	i
třemi	tři	k4xCgNnPc7	tři
stejně	stejně	k9	stejně
pojmenovanými	pojmenovaný	k2eAgFnPc7d1	pojmenovaná
přísnými	přísný	k2eAgFnPc7d1	přísná
zkouškami	zkouška	k1gFnPc7	zkouška
doktorskými	doktorský	k2eAgFnPc7d1	doktorská
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
rigorózy	rigorózum	k1gNnPc7	rigorózum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
rigorózu	rigorózum	k1gNnSc6	rigorózum
studenti	student	k1gMnPc1	student
používali	používat	k5eAaImAgMnP	používat
neoficiální	oficiální	k2eNgInSc4d1	neoficiální
čekatelský	čekatelský	k2eAgInSc4d1	čekatelský
titul	titul	k1gInSc4	titul
JUC	JUC	kA	JUC
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
doktora	doktor	k1gMnSc2	doktor
práv	právo	k1gNnPc2	právo
byl	být	k5eAaImAgInS	být
podmínkou	podmínka	k1gFnSc7	podmínka
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
advokacie	advokacie	k1gFnSc2	advokacie
<g/>
,	,	kIx,	,
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
právnickým	právnický	k2eAgFnPc3d1	právnická
profesím	profes	k1gFnPc3	profes
<g/>
,	,	kIx,	,
např.	např.	kA	např.
soudce	soudce	k1gMnSc2	soudce
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
nebylo	být	k5eNaImAgNnS	být
třeba	třeba	k6eAd1	třeba
<g/>
,	,	kIx,	,
postačovaly	postačovat	k5eAaImAgFnP	postačovat
státní	státní	k2eAgFnPc4d1	státní
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
upravené	upravený	k2eAgNnSc1d1	upravené
studium	studium	k1gNnSc1	studium
platilo	platit	k5eAaImAgNnS	platit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
existují	existovat	k5eAaImIp3nP	existovat
čtyři	čtyři	k4xCgFnPc1	čtyři
právnické	právnický	k2eAgFnPc1d1	právnická
fakulty	fakulta	k1gFnPc1	fakulta
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
:	:	kIx,	:
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
Fakulta	fakulta	k1gFnSc1	fakulta
právnická	právnický	k2eAgFnSc1d1	právnická
Západočeské	západočeský	k2eAgFnPc4d1	Západočeská
univerzity	univerzita	k1gFnPc4	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
<s>
Na	na	k7c6	na
právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
možné	možný	k2eAgNnSc1d1	možné
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
oboru	obor	k1gInSc6	obor
Právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
program	program	k1gInSc1	program
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
právní	právní	k2eAgFnSc1d1	právní
věda	věda	k1gFnSc1	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
absolvováním	absolvování	k1gNnSc7	absolvování
se	se	k3xPyFc4	se
student	student	k1gMnSc1	student
stává	stávat	k5eAaImIp3nS	stávat
právníkem	právník	k1gMnSc7	právník
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
doktorských	doktorský	k2eAgInPc6d1	doktorský
studijních	studijní	k2eAgInPc6d1	studijní
oborech	obor	k1gInPc6	obor
v	v	k7c6	v
programu	program	k1gInSc6	program
Teoretické	teoretický	k2eAgFnSc2d1	teoretická
právní	právní	k2eAgFnSc2d1	právní
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
pražské	pražský	k2eAgFnSc2d1	Pražská
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
bakalářských	bakalářský	k2eAgInPc6d1	bakalářský
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
moravské	moravský	k2eAgFnSc2d1	Moravská
fakulty	fakulta	k1gFnSc2	fakulta
nabízejí	nabízet	k5eAaImIp3nP	nabízet
i	i	k9	i
navazující	navazující	k2eAgInPc1d1	navazující
magisterské	magisterský	k2eAgInPc1d1	magisterský
studijní	studijní	k2eAgInPc1d1	studijní
obory	obor	k1gInPc1	obor
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
zde	zde	k6eAd1	zde
fungují	fungovat	k5eAaImIp3nP	fungovat
i	i	k9	i
tři	tři	k4xCgFnPc1	tři
soukromé	soukromý	k2eAgFnPc1d1	soukromá
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
s	s	k7c7	s
právním	právní	k2eAgNnSc7d1	právní
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Ostravě	Ostrava	k1gFnSc6	Ostrava
funguje	fungovat	k5eAaImIp3nS	fungovat
soukromá	soukromý	k2eAgFnSc1d1	soukromá
Fakulta	fakulta	k1gFnSc1	fakulta
práva	právo	k1gNnSc2	právo
Panevropské	panevropský	k2eAgFnSc2d1	panevropská
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
škola	škola	k1gFnSc1	škola
však	však	k9	však
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
akreditována	akreditovat	k5eAaBmNgFnS	akreditovat
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
absolventi	absolvent	k1gMnPc1	absolvent
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
bez	bez	k7c2	bez
dalšího	další	k2eAgInSc2d1	další
zapsáni	zapsat	k5eAaPmNgMnP	zapsat
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
advokátních	advokátní	k2eAgMnPc2d1	advokátní
koncipientů	koncipient	k1gMnPc2	koncipient
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
působící	působící	k2eAgInPc1d1	působící
na	na	k7c4	na
území	území	k1gNnSc4	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgNnSc1d1	právní
zaměření	zaměření	k1gNnSc1	zaměření
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
soukromá	soukromý	k2eAgFnSc1d1	soukromá
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
lze	lze	k6eAd1	lze
studovat	studovat	k5eAaImF	studovat
pouze	pouze	k6eAd1	pouze
bakalářské	bakalářský	k2eAgInPc4d1	bakalářský
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
a	a	k8xC	a
Fakulta	fakulta	k1gFnSc1	fakulta
právních	právní	k2eAgFnPc2d1	právní
a	a	k8xC	a
správních	správní	k2eAgFnPc2d1	správní
studií	studie	k1gFnPc2	studie
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
finanční	finanční	k2eAgFnSc2d1	finanční
a	a	k8xC	a
správní	správní	k2eAgFnSc2d1	správní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
čtyři	čtyři	k4xCgFnPc1	čtyři
právnické	právnický	k2eAgFnPc1d1	právnická
fakulty	fakulta	k1gFnPc1	fakulta
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
:	:	kIx,	:
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Pavla	Pavel	k1gMnSc2	Pavel
Jozefa	Jozef	k1gMnSc2	Jozef
Šafárika	Šafárik	k1gMnSc2	Šafárik
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Mateja	Matej	k2eAgFnSc1d1	Mateja
Bela	Bela	k1gFnSc1	Bela
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Trnavské	trnavský	k2eAgFnSc2d1	Trnavská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Trnavě	Trnava	k1gFnSc6	Trnava
Ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
právnické	právnický	k2eAgFnPc1d1	právnická
fakulty	fakulta	k1gFnPc1	fakulta
veřejných	veřejný	k2eAgFnPc2d1	veřejná
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
jedna	jeden	k4xCgFnSc1	jeden
právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
soukromé	soukromý	k2eAgFnSc2d1	soukromá
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
:	:	kIx,	:
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Lublani	Lublaň	k1gFnSc6	Lublaň
(	(	kIx(	(
<g/>
od	od	k7c2	od
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Mariboru	Maribor	k1gInSc6	Maribor
(	(	kIx(	(
<g/>
od	od	k7c2	od
1960	[number]	k4	1960
jako	jako	k8xS	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
právnická	právnický	k2eAgFnSc1d1	právnická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
od	od	k7c2	od
1991	[number]	k4	1991
jako	jako	k8xC	jako
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
)	)	kIx)	)
Evropská	evropský	k2eAgFnSc1d1	Evropská
právnická	právnický	k2eAgFnSc1d1	právnická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Nove	nov	k1gInSc5	nov
Gorici	Goric	k1gMnSc6	Goric
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
