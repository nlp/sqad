<s>
Čádor	čádor	k1gInSc1	čádor
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
islámského	islámský	k2eAgInSc2d1	islámský
oděvu	oděv	k1gInSc2	oděv
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
,	,	kIx,	,
splývavý	splývavý	k2eAgInSc4d1	splývavý
oděv	oděv	k1gInSc4	oděv
z	z	k7c2	z
černé	černá	k1gFnSc2	černá
nebo	nebo	k8xC	nebo
tmavě	tmavě	k6eAd1	tmavě
vzorované	vzorovaný	k2eAgFnSc2d1	vzorovaná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
.	.	kIx.	.
</s>
