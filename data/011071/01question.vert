<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
splývavý	splývavý	k2eAgInSc1d1	splývavý
oděv	oděv	k1gInSc1	oděv
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
z	z	k7c2	z
černé	černá	k1gFnSc2	černá
nebo	nebo	k8xC	nebo
tmavě	tmavě	k6eAd1	tmavě
vzorované	vzorovaný	k2eAgFnSc2d1	vzorovaná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
?	?	kIx.	?
</s>
