<p>
<s>
Čádor	čádor	k1gInSc1	čádor
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
islámského	islámský	k2eAgInSc2d1	islámský
oděvu	oděv	k1gInSc2	oděv
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
,	,	kIx,	,
splývavý	splývavý	k2eAgInSc4d1	splývavý
oděv	oděv	k1gInSc4	oděv
z	z	k7c2	z
černé	černá	k1gFnSc2	černá
nebo	nebo	k8xC	nebo
tmavě	tmavě	k6eAd1	tmavě
vzorované	vzorovaný	k2eAgFnSc2d1	vzorovaná
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
ženinu	ženin	k2eAgFnSc4d1	ženina
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
zbytek	zbytek	k1gInSc4	zbytek
jejího	její	k3xOp3gNnSc2	její
těla	tělo	k1gNnSc2	tělo
až	až	k9	až
po	po	k7c4	po
kotníky	kotník	k1gInPc4	kotník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Írán	Írán	k1gInSc1	Írán
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
povinný	povinný	k2eAgInSc1d1	povinný
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
mešity	mešita	k1gFnSc2	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Čádor	čádor	k1gInSc1	čádor
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
také	také	k9	také
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hidžáb	Hidžáb	k1gMnSc1	Hidžáb
</s>
</p>
<p>
<s>
Nikáb	Nikáb	k1gMnSc1	Nikáb
</s>
</p>
<p>
<s>
Burka	burka	k1gFnSc1	burka
</s>
</p>
