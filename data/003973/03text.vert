<s>
Libanon	Libanon	k1gInSc1	Libanon
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ل	ل	k?	ل
<g/>
,	,	kIx,	,
Lubnán	Lubnán	k2eAgMnSc1d1	Lubnán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Libanonská	libanonský	k2eAgFnSc1d1	libanonská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
ا	ا	k?	ا
<g/>
,	,	kIx,	,
al-džumhúríja	alžumhúríj	k2eAgFnSc1d1	al-džumhúríj
al-Lubnáníja	al-Lubnáníja	k1gFnSc1	al-Lubnáníja
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
(	(	kIx(	(
<g/>
a	a	k8xC	a
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
)	)	kIx)	)
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
Východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
při	při	k7c6	při
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	s	k7c7	s
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1516	[number]	k4	1516
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
součástí	součást	k1gFnSc7	součást
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
Libanon	Libanon	k1gInSc1	Libanon
získává	získávat	k5eAaImIp3nS	získávat
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
k	k	k7c3	k
nastolení	nastolení	k1gNnSc3	nastolení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
diktatury	diktatura	k1gFnSc2	diktatura
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Džamala	Džamala	k1gMnSc2	Džamala
Paši	paša	k1gMnSc2	paša
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
porážkou	porážka	k1gFnSc7	porážka
centrálních	centrální	k2eAgFnPc2d1	centrální
mocností	mocnost	k1gFnPc2	mocnost
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
rozpadu	rozpad	k1gInSc3	rozpad
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
podepisují	podepisovat	k5eAaImIp3nP	podepisovat
tajnou	tajný	k2eAgFnSc4d1	tajná
Sykes-Picotovu	Sykes-Picotův	k2eAgFnSc4d1	Sykes-Picotova
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
arabské	arabský	k2eAgFnPc4d1	arabská
provincie	provincie	k1gFnPc4	provincie
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Sýrií	Sýrie	k1gFnSc7	Sýrie
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
stát	stát	k5eAaImF	stát
sférou	sféra	k1gFnSc7	sféra
vlivu	vliv	k1gInSc2	vliv
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
tureckých	turecký	k2eAgNnPc2d1	turecké
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Libanon	Libanon	k1gInSc1	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Libanon	Libanon	k1gInSc1	Libanon
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Libanon	Libanon	k1gInSc1	Libanon
okupuje	okupovat	k5eAaBmIp3nS	okupovat
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
na	na	k7c4	na
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
Sykes-Picotovy	Sykes-Picotův	k2eAgFnPc4d1	Sykes-Picotova
dohody	dohoda	k1gFnPc4	dohoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
USA	USA	kA	USA
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
mandátní	mandátní	k2eAgFnSc4d1	mandátní
formu	forma	k1gFnSc4	forma
uspořádání	uspořádání	k1gNnSc2	uspořádání
země	země	k1gFnSc1	země
–	–	k?	–
tento	tento	k3xDgInSc4	tento
mandát	mandát	k1gInSc4	mandát
měl	mít	k5eAaImAgMnS	mít
zemi	zem	k1gFnSc3	zem
připravit	připravit	k5eAaPmF	připravit
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
Libanon	Libanon	k1gInSc1	Libanon
prohlášen	prohlášen	k2eAgInSc1d1	prohlášen
mandátním	mandátní	k2eAgNnSc7d1	mandátní
územím	území	k1gNnSc7	území
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Libanon	Libanon	k1gInSc1	Libanon
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
Velkého	velký	k2eAgInSc2d1	velký
Libanonu	Libanon	k1gInSc2	Libanon
pod	pod	k7c7	pod
francouzským	francouzský	k2eAgInSc7d1	francouzský
mandátem	mandát	k1gInSc7	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
je	být	k5eAaImIp3nS	být
Libanon	Libanon	k1gInSc1	Libanon
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
libanonské	libanonský	k2eAgFnSc2d1	libanonská
ústavy	ústava	k1gFnSc2	ústava
(	(	kIx(	(
<g/>
dochází	docházet	k5eAaImIp3nS	docházet
např.	např.	kA	např.
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
Velkého	velký	k2eAgInSc2d1	velký
Libanonu	Libanon	k1gInSc2	Libanon
na	na	k7c4	na
Libanonskou	libanonský	k2eAgFnSc4d1	libanonská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
Libanon	Libanon	k1gInSc1	Libanon
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k9	jako
stát	stát	k1gInSc1	stát
nezávislý	závislý	k2eNgInSc1d1	nezávislý
<g/>
,	,	kIx,	,
jednotný	jednotný	k2eAgMnSc1d1	jednotný
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
francouzským	francouzský	k2eAgNnSc7d1	francouzské
mandátním	mandátní	k2eAgNnSc7d1	mandátní
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
Libanon	Libanon	k1gInSc1	Libanon
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
vichistické	vichistický	k2eAgFnSc2d1	vichistická
kolaborantské	kolaborantský	k2eAgFnSc2d1	kolaborantská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k9	již
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
do	do	k7c2	do
Libanonu	Libanon	k1gInSc2	Libanon
přichází	přicházet	k5eAaImIp3nS	přicházet
jednotky	jednotka	k1gFnPc4	jednotka
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zrušen	zrušit	k5eAaPmNgInS	zrušit
francouzský	francouzský	k2eAgInSc1d1	francouzský
mandát	mandát	k1gInSc1	mandát
a	a	k8xC	a
formálně	formálně	k6eAd1	formálně
je	být	k5eAaImIp3nS	být
uznána	uznán	k2eAgFnSc1d1	uznána
nezávislost	nezávislost	k1gFnSc4	nezávislost
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
Francie	Francie	k1gFnSc2	Francie
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zcela	zcela	k6eAd1	zcela
staženy	stáhnout	k5eAaPmNgFnP	stáhnout
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
po	po	k7c6	po
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
k	k	k7c3	k
důležité	důležitý	k2eAgFnSc3d1	důležitá
dohodě	dohoda	k1gFnSc3	dohoda
mezi	mezi	k7c7	mezi
jejími	její	k3xOp3gMnPc7	její
hlavními	hlavní	k2eAgMnPc7d1	hlavní
představiteli	představitel	k1gMnPc7	představitel
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Národnímu	národní	k2eAgInSc3d1	národní
paktu	pakt	k1gInSc3	pakt
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dohodu	dohoda	k1gFnSc4	dohoda
nepsanou	nepsaný	k2eAgFnSc4d1	nepsaná
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
náboženské	náboženský	k2eAgFnPc4d1	náboženská
skupiny	skupina	k1gFnPc4	skupina
–	–	k?	–
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
maronita	maronita	k1gFnSc1	maronita
<g/>
,	,	kIx,	,
premiérem	premiér	k1gMnSc7	premiér
sunnita	sunnita	k1gMnSc1	sunnita
a	a	k8xC	a
předsedou	předseda	k1gMnSc7	předseda
parlamentu	parlament	k1gInSc2	parlament
šíita	šíita	k1gMnSc1	šíita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
arabská	arabský	k2eAgFnSc1d1	arabská
země	země	k1gFnSc1	země
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
Libanon	Libanon	k1gInSc4	Libanon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
Eisenhowerovu	Eisenhowerův	k2eAgFnSc4d1	Eisenhowerova
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
sousední	sousední	k2eAgFnSc2d1	sousední
Sýrie	Sýrie	k1gFnSc2	Sýrie
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
panarabské	panarabský	k2eAgFnSc2d1	panarabská
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
šíří	šíř	k1gFnPc2	šíř
mezi	mezi	k7c7	mezi
arabskými	arabský	k2eAgFnPc7d1	arabská
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Libanonští	libanonský	k2eAgMnPc1d1	libanonský
křesťané	křesťan	k1gMnPc1	křesťan
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
obávají	obávat	k5eAaImIp3nP	obávat
možnosti	možnost	k1gFnPc4	možnost
sloučení	sloučení	k1gNnSc2	sloučení
Libanonu	Libanon	k1gInSc2	Libanon
se	se	k3xPyFc4	se
Sýrii	Sýrie	k1gFnSc3	Sýrie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
Sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
arabské	arabský	k2eAgFnSc2d1	arabská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
totiž	totiž	k9	totiž
požadují	požadovat	k5eAaImIp3nP	požadovat
především	především	k9	především
libanonští	libanonský	k2eAgMnPc1d1	libanonský
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
situaci	situace	k1gFnSc3	situace
se	se	k3xPyFc4	se
maronitský	maronitský	k2eAgMnSc1d1	maronitský
prezident	prezident	k1gMnSc1	prezident
Šamún	Šamún	k1gMnSc1	Šamún
obrací	obracet	k5eAaImIp3nS	obracet
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c6	na
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vysílají	vysílat	k5eAaImIp3nP	vysílat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
své	svůj	k3xOyFgFnSc2	svůj
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uklidnění	uklidnění	k1gNnSc6	uklidnění
situace	situace	k1gFnSc2	situace
je	být	k5eAaImIp3nS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
staženy	stažen	k2eAgFnPc1d1	stažena
jednotky	jednotka	k1gFnPc1	jednotka
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgInSc7d1	postupný
příchodem	příchod	k1gInSc7	příchod
Palestinců	Palestinec	k1gMnPc2	Palestinec
(	(	kIx(	(
<g/>
OOP	OOP	kA	OOP
<g/>
)	)	kIx)	)
do	do	k7c2	do
Libanonu	Libanon	k1gInSc2	Libanon
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
libanonskou	libanonský	k2eAgFnSc7d1	libanonská
armádou	armáda	k1gFnSc7	armáda
ke	k	k7c3	k
střetům	střet	k1gInPc3	střet
<g/>
.	.	kIx.	.
</s>
<s>
OOP	OOP	kA	OOP
také	také	k9	také
podniká	podnikat	k5eAaImIp3nS	podnikat
útoky	útok	k1gInPc4	útok
z	z	k7c2	z
libanonského	libanonský	k2eAgNnSc2d1	libanonské
území	území	k1gNnSc2	území
proti	proti	k7c3	proti
Izraeli	Izrael	k1gInSc3	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
provádí	provádět	k5eAaImIp3nS	provádět
odvetné	odvetný	k2eAgFnPc4d1	odvetná
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
proniká	pronikat	k5eAaImIp3nS	pronikat
na	na	k7c4	na
libanonské	libanonský	k2eAgNnSc4d1	libanonské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Libanonská	libanonský	k2eAgFnSc1d1	libanonská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
První	první	k4xOgFnSc1	první
libanonská	libanonský	k2eAgFnSc1d1	libanonská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzrůstu	vzrůst	k1gInSc6	vzrůst
napětí	napětí	k1gNnSc2	napětí
měli	mít	k5eAaImAgMnP	mít
značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
palestinští	palestinský	k2eAgMnPc1d1	palestinský
uprchlíci	uprchlík	k1gMnPc1	uprchlík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přišli	přijít	k5eAaPmAgMnP	přijít
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
prvním	první	k4xOgMnSc6	první
arabsko-izraelském	arabskozraelský	k2eAgInSc6d1	arabsko-izraelský
konfliktu	konflikt	k1gInSc6	konflikt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
po	po	k7c6	po
šestidenní	šestidenní	k2eAgFnSc6d1	šestidenní
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
OOP	OOP	kA	OOP
"	"	kIx"	"
<g/>
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
<g/>
"	"	kIx"	"
do	do	k7c2	do
Libanonu	Libanon	k1gInSc2	Libanon
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
začala	začít	k5eAaPmAgFnS	začít
budovat	budovat	k5eAaImF	budovat
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Palestinci	Palestinec	k1gMnPc1	Palestinec
se	se	k3xPyFc4	se
střetávali	střetávat	k5eAaImAgMnP	střetávat
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
cítili	cítit	k5eAaImAgMnP	cítit
utlačováni	utlačován	k2eAgMnPc1d1	utlačován
(	(	kIx(	(
<g/>
nespokojení	spokojený	k2eNgMnPc1d1	nespokojený
byli	být	k5eAaImAgMnP	být
především	především	k9	především
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
stáli	stát	k5eAaImAgMnP	stát
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
falangisté	falangista	k1gMnPc1	falangista
a	a	k8xC	a
palestinští	palestinský	k2eAgMnPc1d1	palestinský
uprchlíci	uprchlík	k1gMnPc1	uprchlík
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sunnitskými	sunnitský	k2eAgMnPc7d1	sunnitský
muslimy	muslim	k1gMnPc7	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
stálo	stát	k5eAaImAgNnS	stát
několik	několik	k4yIc1	několik
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
atentát	atentát	k1gInSc1	atentát
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
namířen	namířit	k5eAaPmNgInS	namířit
na	na	k7c4	na
autobus	autobus	k1gInSc4	autobus
převážející	převážející	k2eAgMnPc4d1	převážející
členy	člen	k1gMnPc4	člen
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
Falangy	falanga	k1gFnSc2	falanga
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
falangy	falanga	k1gFnSc2	falanga
obvinil	obvinit	k5eAaPmAgMnS	obvinit
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
činu	čin	k1gInSc2	čin
Palestince	Palestinec	k1gMnPc4	Palestinec
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
odvetu	odveta	k1gFnSc4	odveta
falangisté	falangista	k1gMnPc1	falangista
napadli	napadnout	k5eAaPmAgMnP	napadnout
autobus	autobus	k1gInSc4	autobus
převážející	převážející	k2eAgFnSc2d1	převážející
palestinské	palestinský	k2eAgFnSc2d1	palestinská
dělníky	dělník	k1gMnPc7	dělník
do	do	k7c2	do
uprchlického	uprchlický	k2eAgInSc2d1	uprchlický
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
libanonské	libanonský	k2eAgFnSc2d1	libanonská
vlády	vláda	k1gFnSc2	vláda
do	do	k7c2	do
země	zem	k1gFnSc2	zem
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1976	[number]	k4	1976
přicházejí	přicházet	k5eAaImIp3nP	přicházet
syrské	syrský	k2eAgFnPc1d1	Syrská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
syrskou	syrský	k2eAgFnSc7d1	Syrská
armádou	armáda	k1gFnSc7	armáda
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
palestinské	palestinský	k2eAgMnPc4d1	palestinský
bojovníky	bojovník	k1gMnPc4	bojovník
na	na	k7c4	na
jih	jih	k1gInSc4	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Příměří	příměří	k1gNnSc1	příměří
ovšem	ovšem	k9	ovšem
dlouho	dlouho	k6eAd1	dlouho
nevydrželo	vydržet	k5eNaPmAgNnS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
vypukají	vypukat	k5eAaImIp3nP	vypukat
další	další	k2eAgInPc4d1	další
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Syřané	Syřan	k1gMnPc1	Syřan
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Palestinců	Palestinec	k1gMnPc2	Palestinec
<g/>
.	.	kIx.	.
</s>
<s>
OOP	OOP	kA	OOP
ostřeluje	ostřelovat	k5eAaImIp3nS	ostřelovat
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Libanonu	Libanon	k1gInSc2	Libanon
sever	sever	k1gInSc1	sever
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
do	do	k7c2	do
války	válka	k1gFnSc2	válka
zapojují	zapojovat	k5eAaImIp3nP	zapojovat
i	i	k9	i
izraelské	izraelský	k2eAgFnPc4d1	izraelská
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
podporují	podporovat	k5eAaImIp3nP	podporovat
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
jednotky	jednotka	k1gFnPc1	jednotka
plukovníka	plukovník	k1gMnSc2	plukovník
Haddáda	Haddáda	k1gFnSc1	Haddáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1978	[number]	k4	1978
překročila	překročit	k5eAaPmAgFnS	překročit
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Lítání	Lítání	k?	Lítání
<g/>
,	,	kIx,	,
izraelská	izraelský	k2eAgFnSc1d1	izraelská
vojska	vojsko	k1gNnSc2	vojsko
hranice	hranice	k1gFnPc4	hranice
Libanonu	Libanon	k1gInSc2	Libanon
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgFnS	obsadit
jih	jih	k1gInSc4	jih
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Reagovala	reagovat	k5eAaBmAgFnS	reagovat
tak	tak	k9	tak
na	na	k7c4	na
opětovné	opětovný	k2eAgNnSc4d1	opětovné
ostřelování	ostřelování	k1gNnSc4	ostřelování
severu	sever	k1gInSc2	sever
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
rezolucí	rezoluce	k1gFnPc2	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
izraelské	izraelský	k2eAgFnSc2d1	izraelská
jednotky	jednotka	k1gFnSc2	jednotka
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
byl	být	k5eAaImAgInS	být
fakticky	fakticky	k6eAd1	fakticky
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
(	(	kIx(	(
<g/>
ovládanou	ovládaný	k2eAgFnSc7d1	ovládaná
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tripolis	Tripolis	k1gInSc1	Tripolis
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
Libanon	Libanon	k1gInSc1	Libanon
a	a	k8xC	a
východní	východní	k2eAgInSc1d1	východní
Bejrút	Bejrút	k1gInSc1	Bejrút
(	(	kIx(	(
<g/>
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
muslimů	muslim	k1gMnPc2	muslim
<g/>
)	)	kIx)	)
a	a	k8xC	a
západní	západní	k2eAgInSc4d1	západní
Bejrút	Bejrút	k1gInSc4	Bejrút
(	(	kIx(	(
<g/>
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
křesťanů	křesťan	k1gMnPc2	křesťan
podporovaných	podporovaný	k2eAgFnPc2d1	podporovaná
Izraelci	Izraelec	k1gMnPc1	Izraelec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
léta	léto	k1gNnSc2	léto
1981	[number]	k4	1981
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
1982	[number]	k4	1982
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgFnPc1d1	přítomna
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
jednotky	jednotka	k1gFnPc1	jednotka
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
relativní	relativní	k2eAgFnSc3d1	relativní
stabilizaci	stabilizace	k1gFnSc3	stabilizace
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ovšem	ovšem	k9	ovšem
Palestinci	Palestinec	k1gMnPc1	Palestinec
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
ostřelování	ostřelování	k1gNnSc6	ostřelování
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
operace	operace	k1gFnSc1	operace
Mír	mír	k1gInSc4	mír
pro	pro	k7c4	pro
Galileu	Galilea	k1gFnSc4	Galilea
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
první	první	k4xOgFnSc1	první
libanonská	libanonský	k2eAgFnSc1d1	libanonská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
jednotky	jednotka	k1gFnPc1	jednotka
překročily	překročit	k5eAaPmAgFnP	překročit
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
místech	místo	k1gNnPc6	místo
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Libanonem	Libanon	k1gInSc7	Libanon
a	a	k8xC	a
obsadily	obsadit	k5eAaPmAgFnP	obsadit
třetinu	třetina	k1gFnSc4	třetina
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
obklíčili	obklíčit	k5eAaPmAgMnP	obklíčit
i	i	k8xC	i
Bejrút	Bejrút	k1gInSc4	Bejrút
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obklíčení	obklíčení	k1gNnSc4	obklíčení
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Izraele	Izrael	k1gInSc2	Izrael
trvalo	trvat	k5eAaImAgNnS	trvat
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
představitel	představitel	k1gMnSc1	představitel
OOP	OOP	kA	OOP
Jásir	Jásir	k1gMnSc1	Jásir
Arafat	Arafat	k1gMnSc1	Arafat
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Libanon	Libanon	k1gInSc1	Libanon
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
jednotkami	jednotka	k1gFnPc7	jednotka
opustí	opustit	k5eAaPmIp3nS	opustit
a	a	k8xC	a
hlavní	hlavní	k2eAgInSc1d1	hlavní
stan	stan	k1gInSc1	stan
OOP	OOP	kA	OOP
přestěhuje	přestěhovat	k5eAaPmIp3nS	přestěhovat
do	do	k7c2	do
Tunisu	Tunis	k1gInSc2	Tunis
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1982	[number]	k4	1982
byl	být	k5eAaImAgInS	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
uprchlických	uprchlický	k2eAgInPc6d1	uprchlický
táborech	tábor	k1gInPc6	tábor
Šabra	Šabro	k1gNnSc2	Šabro
a	a	k8xC	a
Šatíla	Šatílo	k1gNnSc2	Šatílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
falangisté	falangista	k1gMnPc1	falangista
napadli	napadnout	k5eAaPmAgMnP	napadnout
uprchlické	uprchlický	k2eAgInPc4d1	uprchlický
tábory	tábor	k1gInPc4	tábor
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Bejrútu	Bejrút	k1gInSc6	Bejrút
<g/>
.	.	kIx.	.
</s>
<s>
Reagovali	reagovat	k5eAaBmAgMnP	reagovat
tak	tak	k6eAd1	tak
na	na	k7c4	na
atentát	atentát	k1gInSc4	atentát
spáchaný	spáchaný	k2eAgInSc4d1	spáchaný
na	na	k7c4	na
velitele	velitel	k1gMnSc4	velitel
Falangy	falanga	k1gFnSc2	falanga
Bašíra	Bašír	k1gMnSc4	Bašír
Džamáíla	Džamáíl	k1gMnSc4	Džamáíl
<g/>
.	.	kIx.	.
</s>
<s>
Falangisté	falangista	k1gMnPc1	falangista
napadli	napadnout	k5eAaPmAgMnP	napadnout
Palestince	Palestinec	k1gMnPc4	Palestinec
na	na	k7c4	na
území	území	k1gNnSc4	území
kontrolovaném	kontrolovaný	k2eAgInSc6d1	kontrolovaný
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
izraelští	izraelský	k2eAgMnPc1d1	izraelský
vojáci	voják	k1gMnPc1	voják
zasáhli	zasáhnout	k5eAaPmAgMnP	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Celou	celý	k2eAgFnSc4d1	celá
záležitost	záležitost	k1gFnSc4	záležitost
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
prošetřovala	prošetřovat	k5eAaImAgFnS	prošetřovat
Kahanova	Kahanův	k2eAgFnSc1d1	Kahanova
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
<s>
Pokračovala	pokračovat	k5eAaImAgNnP	pokračovat
i	i	k9	i
jednání	jednání	k1gNnPc1	jednání
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1983	[number]	k4	1983
Libanon	Libanon	k1gInSc1	Libanon
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
USA	USA	kA	USA
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
izraelském	izraelský	k2eAgNnSc6d1	izraelské
stažení	stažení	k1gNnSc6	stažení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
odchodem	odchod	k1gInSc7	odchod
syrských	syrský	k2eAgFnPc2d1	Syrská
jednotek	jednotka	k1gFnPc2	jednotka
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
Sýrie	Sýrie	k1gFnSc1	Sýrie
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
izraelské	izraelský	k2eAgFnSc2d1	izraelská
jednotky	jednotka	k1gFnSc2	jednotka
postupně	postupně	k6eAd1	postupně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
do	do	k7c2	do
války	válka	k1gFnSc2	válka
jen	jen	k6eAd1	jen
podporou	podpora	k1gFnSc7	podpora
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
jednotek	jednotka	k1gFnPc2	jednotka
generála	generál	k1gMnSc4	generál
Aúna	Aúnus	k1gMnSc4	Aúnus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
zahájil	zahájit	k5eAaPmAgInS	zahájit
boj	boj	k1gInSc1	boj
za	za	k7c4	za
vyhnání	vyhnání	k1gNnSc4	vyhnání
syrských	syrský	k2eAgFnPc2d1	Syrská
jednotek	jednotka	k1gFnPc2	jednotka
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
úspěchu	úspěch	k1gInSc6	úspěch
byl	být	k5eAaImAgMnS	být
Aún	Aún	k1gMnSc1	Aún
obklíčen	obklíčit	k5eAaPmNgMnS	obklíčit
Syřany	Syřan	k1gMnPc7	Syřan
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
uprchnout	uprchnout	k5eAaPmF	uprchnout
na	na	k7c4	na
francouzské	francouzský	k2eAgNnSc4d1	francouzské
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
<g/>
.	.	kIx.	.
</s>
<s>
Občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
ukončila	ukončit	k5eAaPmAgFnS	ukončit
Taífská	Taífský	k2eAgFnSc1d1	Taífský
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
dokument	dokument	k1gInSc1	dokument
přijatý	přijatý	k2eAgInSc1d1	přijatý
libanonskými	libanonský	k2eAgMnPc7d1	libanonský
poslanci	poslanec	k1gMnPc7	poslanec
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1989	[number]	k4	1989
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
v	v	k7c6	v
saúdskoarabském	saúdskoarabský	k2eAgInSc6d1	saúdskoarabský
Taífu	Taíf	k1gInSc6	Taíf
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
tzv.	tzv.	kA	tzv.
Charta	charta	k1gFnSc1	charta
národního	národní	k2eAgNnSc2d1	národní
usmíření	usmíření	k1gNnSc2	usmíření
–	–	k?	–
ústavní	ústavní	k2eAgFnSc1d1	ústavní
a	a	k8xC	a
správní	správní	k2eAgFnSc1d1	správní
reforma	reforma	k1gFnSc1	reforma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
poněkud	poněkud	k6eAd1	poněkud
omezila	omezit	k5eAaPmAgFnS	omezit
privilegované	privilegovaný	k2eAgNnSc4d1	privilegované
postavení	postavení	k1gNnSc4	postavení
maronitů	maronit	k1gInPc2	maronit
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
vládě	vláda	k1gFnSc6	vláda
národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
a	a	k8xC	a
legalizace	legalizace	k1gFnSc2	legalizace
pobytu	pobyt	k1gInSc2	pobyt
syrských	syrský	k2eAgFnPc2d1	Syrská
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Taifská	Taifský	k2eAgFnSc1d1	Taifský
dohoda	dohoda	k1gFnSc1	dohoda
ukončila	ukončit	k5eAaPmAgFnS	ukončit
patnáctiletou	patnáctiletý	k2eAgFnSc4d1	patnáctiletá
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Druhá	druhý	k4xOgFnSc1	druhý
libanonská	libanonský	k2eAgFnSc1d1	libanonská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
začíná	začínat	k5eAaImIp3nS	začínat
poválečná	poválečný	k2eAgFnSc1d1	poválečná
obnova	obnova	k1gFnSc1	obnova
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
jih	jih	k1gInSc1	jih
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
nadále	nadále	k6eAd1	nadále
okupován	okupovat	k5eAaBmNgInS	okupovat
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
,	,	kIx,	,
na	na	k7c6	na
zbylém	zbylý	k2eAgNnSc6d1	zbylé
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
40	[number]	k4	40
000	[number]	k4	000
syrských	syrský	k2eAgMnPc2d1	syrský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
má	mít	k5eAaImIp3nS	mít
zajištěnu	zajištěn	k2eAgFnSc4d1	zajištěna
politickou	politický	k2eAgFnSc4d1	politická
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1992	[number]	k4	1992
vedly	vést	k5eAaImAgInP	vést
sociální	sociální	k2eAgInPc1d1	sociální
nepokoje	nepokoj	k1gInPc1	nepokoj
vyvolané	vyvolaný	k2eAgInPc1d1	vyvolaný
tíživou	tíživý	k2eAgFnSc7d1	tíživá
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
situací	situace	k1gFnSc7	situace
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
následným	následný	k2eAgFnPc3d1	následná
parlamentním	parlamentní	k2eAgFnPc3d1	parlamentní
volbám	volba	k1gFnPc3	volba
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
prosyrská	prosyrský	k2eAgFnSc1d1	prosyrská
většina	většina	k1gFnSc1	většina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
vláda	vláda	k1gFnSc1	vláda
Rafíka	Rafík	k1gMnSc2	Rafík
Harírího	Harírí	k1gMnSc2	Harírí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
obnovu	obnova	k1gFnSc4	obnova
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
roste	růst	k5eAaImIp3nS	růst
také	také	k9	také
zadlužování	zadlužování	k1gNnSc1	zadlužování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
devadesátá	devadesátý	k4xOgNnPc4	devadesátý
léta	léto	k1gNnPc4	léto
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
útoky	útok	k1gInPc1	útok
Hizballáhu	Hizballáh	k1gInSc2	Hizballáh
na	na	k7c4	na
Izrael	Izrael	k1gInSc4	Izrael
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgInPc4	jenž
reaguje	reagovat	k5eAaBmIp3nS	reagovat
Izrael	Izrael	k1gInSc1	Izrael
odvetnými	odvetný	k2eAgInPc7d1	odvetný
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2000	[number]	k4	2000
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
Izrael	Izrael	k1gInSc4	Izrael
okupaci	okupace	k1gFnSc4	okupace
jižního	jižní	k2eAgInSc2d1	jižní
Libanonu	Libanon	k1gInSc2	Libanon
a	a	k8xC	a
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
rezoluci	rezoluce	k1gFnSc4	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
425	[number]	k4	425
stažením	stažení	k1gNnSc7	stažení
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stažení	stažení	k1gNnSc6	stažení
Izraele	Izrael	k1gInSc2	Izrael
převzaly	převzít	k5eAaPmAgFnP	převzít
milice	milice	k1gFnPc1	milice
Hizballáhu	Hizballáh	k1gInSc2	Hizballáh
moc	moc	k6eAd1	moc
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
farem	farma	k1gFnPc2	farma
Šibáa	Šibáa	k1gMnSc1	Šibáa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
komunita	komunita	k1gFnSc1	komunita
s	s	k7c7	s
různě	různě	k6eAd1	různě
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
přestávkami	přestávka	k1gFnPc7	přestávka
protestuje	protestovat	k5eAaBmIp3nS	protestovat
proti	proti	k7c3	proti
přítomnosti	přítomnost	k1gFnSc3	přítomnost
syrských	syrský	k2eAgFnPc2d1	Syrská
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
proti	proti	k7c3	proti
vměšování	vměšování	k1gNnSc3	vměšování
Sýrie	Sýrie	k1gFnSc2	Sýrie
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2001	[number]	k4	2001
stahuje	stahovat	k5eAaImIp3nS	stahovat
také	také	k9	také
Sýrie	Sýrie	k1gFnSc1	Sýrie
svá	svůj	k3xOyFgNnPc4	svůj
vojska	vojsko	k1gNnPc1	vojsko
z	z	k7c2	z
Bejrútu	Bejrút	k1gInSc2	Bejrút
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
požadavkům	požadavek	k1gInPc3	požadavek
Libanonců	Libanonec	k1gMnPc2	Libanonec
<g/>
.	.	kIx.	.
</s>
<s>
Syrská	syrský	k2eAgNnPc1d1	syrské
vojska	vojsko	k1gNnPc1	vojsko
a	a	k8xC	a
zpravodajské	zpravodajský	k2eAgFnPc1d1	zpravodajská
služby	služba	k1gFnPc1	služba
začínají	začínat	k5eAaImIp3nP	začínat
opouštět	opouštět	k5eAaImF	opouštět
území	území	k1gNnSc4	území
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
proveden	provést	k5eAaPmNgInS	provést
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
bývalého	bývalý	k2eAgMnSc4d1	bývalý
libanonského	libanonský	k2eAgMnSc4d1	libanonský
premiéra	premiér	k1gMnSc4	premiér
Harírího	Harírí	k1gMnSc4	Harírí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
symbol	symbol	k1gInSc4	symbol
poválečné	poválečný	k2eAgFnSc2d1	poválečná
obnovy	obnova	k1gFnSc2	obnova
Libanonu	Libanon	k1gInSc2	Libanon
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc2	jeho
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
liberální	liberální	k2eAgFnSc2d1	liberální
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
akt	akt	k1gInSc1	akt
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
vlnu	vlna	k1gFnSc4	vlna
protisyrských	protisyrský	k2eAgFnPc2d1	protisyrský
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
už	už	k6eAd1	už
tentokrát	tentokrát	k6eAd1	tentokrát
neomezují	omezovat	k5eNaImIp3nP	omezovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
komunitu	komunita	k1gFnSc4	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
počátek	počátek	k1gInSc4	počátek
tzv.	tzv.	kA	tzv.
cedrové	cedrový	k2eAgFnSc2d1	cedrová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
dokončeno	dokončen	k2eAgNnSc4d1	dokončeno
stahování	stahování	k1gNnSc4	stahování
syrských	syrský	k2eAgNnPc2d1	syrské
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Syrskému	syrský	k2eAgNnSc3d1	syrské
stažení	stažení	k1gNnSc3	stažení
předcházel	předcházet	k5eAaImAgInS	předcházet
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
tlak	tlak	k1gInSc1	tlak
jak	jak	k8xS	jak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
tak	tak	k9	tak
od	od	k7c2	od
libanonské	libanonský	k2eAgFnSc2d1	libanonská
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
otevřeně	otevřeně	k6eAd1	otevřeně
obvinila	obvinit	k5eAaPmAgFnS	obvinit
Damašek	Damašek	k1gInSc4	Damašek
z	z	k7c2	z
podílu	podíl	k1gInSc2	podíl
na	na	k7c6	na
únorovém	únorový	k2eAgInSc6d1	únorový
atentátu	atentát	k1gInSc6	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Sýrie	Sýrie	k1gFnSc1	Sýrie
sice	sice	k8xC	sice
popírá	popírat	k5eAaImIp3nS	popírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
s	s	k7c7	s
vraždou	vražda	k1gFnSc7	vražda
měla	mít	k5eAaImAgFnS	mít
cokoliv	cokoliv	k3yInSc1	cokoliv
společného	společný	k2eAgNnSc2d1	společné
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Libanonců	Libanonec	k1gMnPc2	Libanonec
je	být	k5eAaImIp3nS	být
však	však	k9	však
přesvědčena	přesvědčit	k5eAaPmNgFnS	přesvědčit
o	o	k7c6	o
opaku	opak	k1gInSc6	opak
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
na	na	k7c6	na
libanonské	libanonský	k2eAgFnSc6d1	libanonská
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ozbrojenou	ozbrojený	k2eAgFnSc4d1	ozbrojená
akci	akce	k1gFnSc4	akce
Hizballáhu	Hizballáh	k1gInSc2	Hizballáh
–	–	k?	–
raketový	raketový	k2eAgInSc1d1	raketový
útok	útok	k1gInSc1	útok
na	na	k7c4	na
město	město	k1gNnSc4	město
Šlomi	Šlo	k1gFnPc7	Šlo
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
vojáků	voják	k1gMnPc2	voják
zabito	zabít	k5eAaPmNgNnS	zabít
a	a	k8xC	a
2	[number]	k4	2
uneseni	unesen	k2eAgMnPc1d1	unesen
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
požaduje	požadovat	k5eAaImIp3nS	požadovat
jejich	jejich	k3xOp3gNnSc4	jejich
propuštění	propuštění	k1gNnSc4	propuštění
a	a	k8xC	a
Hizballáh	Hizballáh	k1gInSc1	Hizballáh
požaduje	požadovat	k5eAaImIp3nS	požadovat
propuštění	propuštění	k1gNnSc4	propuštění
svých	svůj	k3xOyFgMnPc2	svůj
zajatců	zajatec	k1gMnPc2	zajatec
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
OSN	OSN	kA	OSN
vybízí	vybízet	k5eAaImIp3nS	vybízet
k	k	k7c3	k
propuštění	propuštění	k1gNnSc3	propuštění
a	a	k8xC	a
vybízí	vybízet	k5eAaImIp3nS	vybízet
také	také	k9	také
vládu	vláda	k1gFnSc4	vláda
Libanonu	Libanon	k1gInSc2	Libanon
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
jižním	jižní	k2eAgNnSc7d1	jižní
územím	území	k1gNnSc7	území
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byli	být	k5eAaImAgMnP	být
zajati	zajat	k2eAgMnPc1d1	zajat
na	na	k7c6	na
izraelském	izraelský	k2eAgInSc6d1	izraelský
nebo	nebo	k8xC	nebo
libanonském	libanonský	k2eAgNnSc6d1	libanonské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
zahajuje	zahajovat	k5eAaImIp3nS	zahajovat
letecké	letecký	k2eAgInPc4d1	letecký
útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Kofi	Kofi	k1gNnSc1	Kofi
Annan	Annana	k1gFnPc2	Annana
vybízí	vybízet	k5eAaImIp3nS	vybízet
ke	k	k7c3	k
zdrženlivosti	zdrženlivost	k1gFnSc3	zdrženlivost
a	a	k8xC	a
vysílá	vysílat	k5eAaImIp3nS	vysílat
sbor	sbor	k1gInSc1	sbor
3	[number]	k4	3
vyjednávačů	vyjednávač	k1gMnPc2	vyjednávač
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc4	útok
přesto	přesto	k8xC	přesto
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
konfliktu	konflikt	k1gInSc2	konflikt
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
Bejrútu	Bejrút	k1gInSc2	Bejrút
hlavní	hlavní	k2eAgMnSc1d1	hlavní
vyjednávač	vyjednávač	k1gMnSc1	vyjednávač
OSN	OSN	kA	OSN
Vijay	Vijaa	k1gFnPc1	Vijaa
Nambiar	Nambiar	k1gMnSc1	Nambiar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
solidaritu	solidarita	k1gFnSc4	solidarita
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
libanonské	libanonský	k2eAgFnSc3d1	libanonská
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zničena	zničen	k2eAgFnSc1d1	zničena
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
vojenská	vojenský	k2eAgFnSc1d1	vojenská
blokáda	blokáda	k1gFnSc1	blokáda
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Humanitární	humanitární	k2eAgFnPc1d1	humanitární
podmínky	podmínka	k1gFnPc1	podmínka
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
a	a	k8xC	a
OSN	OSN	kA	OSN
není	být	k5eNaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
pomáhat	pomáhat	k5eAaImF	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Kofi	Kofi	k6eAd1	Kofi
Annan	Annan	k1gMnSc1	Annan
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
krveprolití	krveprolití	k1gNnSc2	krveprolití
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
umírají	umírat	k5eAaImIp3nP	umírat
4	[number]	k4	4
příslušníci	příslušník	k1gMnPc1	příslušník
mírové	mírový	k2eAgFnSc2d1	mírová
mise	mise	k1gFnSc2	mise
UNIFIL	UNIFIL	kA	UNIFIL
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
pozorovatelské	pozorovatelský	k2eAgFnPc4d1	pozorovatelská
mise	mise	k1gFnPc4	mise
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Pozice	pozice	k1gFnSc1	pozice
UNIFIL	UNIFIL	kA	UNIFIL
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
terčem	terč	k1gInSc7	terč
útoků	útok	k1gInPc2	útok
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
proveden	provést	k5eAaPmNgInS	provést
nálet	nálet	k1gInSc1	nálet
na	na	k7c4	na
město	město	k1gNnSc4	město
Kana	kanout	k5eAaImSgInS	kanout
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
přes	přes	k7c4	přes
50	[number]	k4	50
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
posílá	posílat	k5eAaImIp3nS	posílat
humanitární	humanitární	k2eAgInPc4d1	humanitární
konvoje	konvoj	k1gInPc4	konvoj
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
vysídleno	vysídlen	k2eAgNnSc1d1	vysídleno
<g/>
,	,	kIx,	,
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
utíká	utíkat	k5eAaImIp3nS	utíkat
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
mandát	mandát	k1gInSc1	mandát
mise	mise	k1gFnSc2	mise
UNIFIL	UNIFIL	kA	UNIFIL
o	o	k7c4	o
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
i	i	k9	i
2	[number]	k4	2
týdny	týden	k1gInPc4	týden
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
přijímá	přijímat	k5eAaImIp3nS	přijímat
RB	RB	kA	RB
OSN	OSN	kA	OSN
rezoluci	rezoluce	k1gFnSc4	rezoluce
č.	č.	k?	č.
1701	[number]	k4	1701
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
vojenských	vojenský	k2eAgInPc2d1	vojenský
útoků	útok	k1gInPc2	útok
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
udržováno	udržován	k2eAgNnSc4d1	udržováno
příměří	příměří	k1gNnSc4	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
stahuje	stahovat	k5eAaImIp3nS	stahovat
za	za	k7c4	za
modrou	modrý	k2eAgFnSc4d1	modrá
linii	linie	k1gFnSc4	linie
a	a	k8xC	a
libanonští	libanonský	k2eAgMnPc1d1	libanonský
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
rozmisťují	rozmisťovat	k5eAaImIp3nP	rozmisťovat
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
jižním	jižní	k2eAgNnSc6d1	jižní
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sídle	sídlo	k1gNnSc6	sídlo
OSN	OSN	kA	OSN
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
probíhají	probíhat	k5eAaImIp3nP	probíhat
jednání	jednání	k1gNnPc1	jednání
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
UNIFIL	UNIFIL	kA	UNIFIL
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
ze	z	k7c2	z
2000	[number]	k4	2000
příslušníků	příslušník	k1gMnPc2	příslušník
na	na	k7c4	na
cca	cca	kA	cca
15	[number]	k4	15
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
hornatá	hornatý	k2eAgFnSc1d1	hornatá
země	země	k1gFnSc1	země
ležící	ležící	k2eAgFnSc1d1	ležící
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
středomoří	středomoří	k1gNnSc6	středomoří
(	(	kIx(	(
<g/>
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
225	[number]	k4	225
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
nedaleko	daleko	k6eNd1	daleko
je	být	k5eAaImIp3nS	být
syrská	syrský	k2eAgFnSc1d1	Syrská
poušť	poušť	k1gFnSc1	poušť
<g/>
,	,	kIx,	,
v	v	k7c6	v
libanonském	libanonský	k2eAgNnSc6d1	libanonské
pohoří	pohoří	k1gNnSc6	pohoří
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
lyžování	lyžování	k1gNnSc4	lyžování
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
až	až	k9	až
3000	[number]	k4	3000
m	m	kA	m
vysokým	vysoký	k2eAgFnPc3d1	vysoká
horám	hora	k1gFnPc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hornatý	hornatý	k2eAgInSc1d1	hornatý
pás	pás	k1gInSc1	pás
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
pohoří	pohoří	k1gNnSc6	pohoří
Libanon	Libanon	k1gInSc4	Libanon
a	a	k8xC	a
Antilibanon	Antilibanon	k1gInSc4	Antilibanon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
leží	ležet	k5eAaImIp3nS	ležet
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
pobřežím	pobřeží	k1gNnSc7	pobřeží
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Antilibanon	Antilibanon	k1gInSc1	Antilibanon
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
podél	podél	k7c2	podél
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Zasněžené	zasněžený	k2eAgFnPc1d1	zasněžená
hory	hora	k1gFnPc1	hora
daly	dát	k5eAaPmAgFnP	dát
zemi	zem	k1gFnSc4	zem
také	také	k9	také
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
aramejské	aramejský	k2eAgNnSc4d1	aramejské
slovo	slovo	k1gNnSc4	slovo
laban	labany	k1gInPc2	labany
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohořích	pohoří	k1gNnPc6	pohoří
Libanon	Libanon	k1gInSc1	Libanon
a	a	k8xC	a
Antilibanon	Antilibanona	k1gFnPc2	Antilibanona
<g/>
,	,	kIx,	,
rozdělených	rozdělený	k2eAgFnPc2d1	rozdělená
údolím	údolí	k1gNnSc7	údolí
Bikáa	Biká	k1gInSc2	Biká
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vzácné	vzácný	k2eAgInPc1d1	vzácný
a	a	k8xC	a
chráněné	chráněný	k2eAgInPc1d1	chráněný
zbytky	zbytek	k1gInPc1	zbytek
cedrových	cedrový	k2eAgInPc2d1	cedrový
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Libanonu	Libanon	k1gInSc2	Libanon
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
země	země	k1gFnSc1	země
cedrů	cedr	k1gInPc2	cedr
a	a	k8xC	a
cedr	cedr	k1gInSc1	cedr
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
položena	položit	k5eAaPmNgFnS	položit
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
AFRISOU	AFRISOU	kA	AFRISOU
<g/>
,	,	kIx,	,
<g/>
Bajer	Bajer	k1gMnSc1	Bajer
<g/>
)	)	kIx)	)
Bejrút	Bejrút	k1gInSc1	Bejrút
1	[number]	k4	1
500	[number]	k4	500
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Tripolis	Tripolis	k1gInSc4	Tripolis
200	[number]	k4	200
000	[number]	k4	000
Zahlé	Zahlý	k2eAgNnSc4d1	Zahlý
200	[number]	k4	200
000	[number]	k4	000
Sidón	Sidón	k1gInSc1	Sidón
100	[number]	k4	100
000	[number]	k4	000
Týros	Týros	k1gInSc1	Týros
70	[number]	k4	70
000	[number]	k4	000
Další	další	k1gNnSc4	další
sídla	sídlo	k1gNnSc2	sídlo
<g/>
:	:	kIx,	:
Baalbek	Baalbek	k1gMnSc1	Baalbek
<g/>
,	,	kIx,	,
Džizín	Džizín	k1gMnSc1	Džizín
<g/>
,	,	kIx,	,
Džubajl	Džubajl	k1gMnSc1	Džubajl
<g/>
,	,	kIx,	,
al-Hirmíl	al-Hirmíl	k1gMnSc1	al-Hirmíl
<g/>
,	,	kIx,	,
Mardžajún	Mardžajún	k1gMnSc1	Mardžajún
<g/>
,	,	kIx,	,
al-Miná	al-Miná	k1gFnSc1	al-Miná
<g/>
,	,	kIx,	,
Nabatíja	Nabatíja	k1gFnSc1	Nabatíja
<g/>
,	,	kIx,	,
az-Zahrání	az-Zahrání	k1gNnSc1	az-Zahrání
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
6	[number]	k4	6
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
<g/>
ُ	ُ	k?	ُ
<g/>
ح	ح	k?	ح
<g/>
َ	َ	k?	َ
<g/>
ا	ا	k?	ا
<g/>
َ	َ	k?	َ
<g/>
ظ	ظ	k?	ظ
<g/>
َ	َ	k?	َ
<g/>
ة	ة	k?	ة
muḥ	muḥ	k?	muḥ
<g/>
,	,	kIx,	,
plurál	plurál	k1gInSc1	plurál
م	م	k?	م
muḥ	muḥ	k?	muḥ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bejrút	Bejrút	k1gInSc1	Bejrút
م	م	k?	م
ب	ب	k?	ب
(	(	kIx(	(
<g/>
Bajrút	Bajrút	k1gInSc1	Bajrút
<g/>
;	;	kIx,	;
území	území	k1gNnSc1	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
Bikáa	Bikáa	k1gFnSc1	Bikáa
م	م	k?	م
ا	ا	k?	ا
(	(	kIx(	(
<g/>
al-Biqáa	al-Biqáa	k1gMnSc1	al-Biqáa
<g/>
)	)	kIx)	)
Horský	horský	k2eAgInSc1d1	horský
Libanon	Libanon	k1gInSc1	Libanon
م	م	k?	م
ج	ج	k?	ج
ل	ل	k?	ل
(	(	kIx(	(
<g/>
džabal	džabal	k1gInSc1	džabal
Lubnán	Lubnán	k2eAgInSc1d1	Lubnán
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgInSc1d1	jižní
Libanon	Libanon	k1gInSc1	Libanon
م	م	k?	م
ا	ا	k?	ا
(	(	kIx(	(
<g/>
al-Džanúb	al-Džanúb	k1gMnSc1	al-Džanúb
<g/>
)	)	kIx)	)
Nabatíja	Nabatíja	k1gMnSc1	Nabatíja
م	م	k?	م
ا	ا	k?	ا
(	(	kIx(	(
<g/>
an-Nabatíja	an-Nabatíja	k1gFnSc1	an-Nabatíja
<g/>
)	)	kIx)	)
Severní	severní	k2eAgFnSc1d1	severní
Libanon	Libanon	k1gInSc1	Libanon
م	م	k?	م
ا	ا	k?	ا
(	(	kIx(	(
<g/>
aš-Šamál	aš-Šamál	k1gMnSc1	aš-Šamál
<g/>
)	)	kIx)	)
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
žije	žít	k5eAaImIp3nS	žít
4,1	[number]	k4	4,1
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
95	[number]	k4	95
%	%	kIx~	%
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
arménská	arménský	k2eAgFnSc1d1	arménská
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
menšiny	menšina	k1gFnPc1	menšina
<g/>
.	.	kIx.	.
60	[number]	k4	60
%	%	kIx~	%
Libanonců	Libanonec	k1gMnPc2	Libanonec
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
maronité	maronitý	k2eAgNnSc1d1	maronité
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
posledním	poslední	k2eAgNnSc6d1	poslední
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
bylo	být	k5eAaImAgNnS	být
křesťanů	křesťan	k1gMnPc2	křesťan
55	[number]	k4	55
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Porodnost	porodnost	k1gFnSc1	porodnost
muslimů	muslim	k1gMnPc2	muslim
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Gramotných	gramotný	k2eAgInPc2d1	gramotný
je	být	k5eAaImIp3nS	být
89	[number]	k4	89
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
nápisů	nápis	k1gInPc2	nápis
ve	v	k7c6	v
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
ještě	ještě	k9	ještě
anglicky	anglicky	k6eAd1	anglicky
nebo	nebo	k8xC	nebo
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
žije	žít	k5eAaImIp3nS	žít
3,8	[number]	k4	3,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
95	[number]	k4	95
%	%	kIx~	%
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
4	[number]	k4	4
%	%	kIx~	%
Arménů	Armén	k1gMnPc2	Armén
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
Palestinci	Palestinec	k1gMnPc1	Palestinec
<g/>
,	,	kIx,	,
Turkmeni	Turkmen	k1gMnPc1	Turkmen
aj.	aj.	kA	aj.
Šedesát	šedesát	k4xCc4	šedesát
procent	procento	k1gNnPc2	procento
Libanonců	Libanonec	k1gMnPc2	Libanonec
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
40	[number]	k4	40
%	%	kIx~	%
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
udávané	udávaný	k2eAgNnSc4d1	udávané
oficiální	oficiální	k2eAgNnSc4d1	oficiální
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
situace	situace	k1gFnSc2	situace
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
muslimů	muslim	k1gMnPc2	muslim
70	[number]	k4	70
%	%	kIx~	%
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
;	;	kIx,	;
křesťanů	křesťan	k1gMnPc2	křesťan
asi	asi	k9	asi
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgNnPc1d1	přesné
čísla	číslo	k1gNnPc1	číslo
nejsou	být	k5eNaImIp3nP	být
známá	známý	k2eAgNnPc1d1	známé
kvůli	kvůli	k7c3	kvůli
nepřehledné	přehledný	k2eNgFnSc3d1	nepřehledná
situaci	situace	k1gFnSc3	situace
a	a	k8xC	a
občanským	občanský	k2eAgFnPc3d1	občanská
válkám	válka	k1gFnPc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
však	však	k9	však
Palestinců	Palestinec	k1gMnPc2	Palestinec
až	až	k6eAd1	až
12	[number]	k4	12
%	%	kIx~	%
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
přiznané	přiznaný	k2eAgNnSc4d1	přiznané
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k8xC	ani
pas	pas	k1gInSc4	pas
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
proto	proto	k8xC	proto
vycestovat	vycestovat	k5eAaPmF	vycestovat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
UNRWA	UNRWA	kA	UNRWA
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
více	hodně	k6eAd2	hodně
než	než	k8xS	než
390	[number]	k4	390
000	[number]	k4	000
palestinských	palestinský	k2eAgMnPc2d1	palestinský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Armény	Armén	k1gMnPc4	Armén
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
především	především	k6eAd1	především
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
arménské	arménský	k2eAgFnSc2d1	arménská
genocidy	genocida	k1gFnSc2	genocida
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
určité	určitý	k2eAgNnSc4d1	určité
procento	procento	k1gNnSc4	procento
Syřanů	Syřan	k1gMnPc2	Syřan
a	a	k8xC	a
Kurdů	Kurd	k1gMnPc2	Kurd
(	(	kIx(	(
<g/>
dohromady	dohromady	k6eAd1	dohromady
však	však	k9	však
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Syřanů	Syřan	k1gMnPc2	Syřan
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
zejména	zejména	k9	zejména
o	o	k7c4	o
sezónní	sezónní	k2eAgFnSc4d1	sezónní
pracovní	pracovní	k2eAgFnSc4d1	pracovní
migraci	migrace	k1gFnSc4	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
důležitých	důležitý	k2eAgFnPc2d1	důležitá
menšin	menšina	k1gFnPc2	menšina
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
na	na	k7c4	na
45	[number]	k4	45
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
občanů	občan	k1gMnPc2	občan
Šrí	Šrí	k1gMnPc2	Šrí
Lanky	lanko	k1gNnPc7	lanko
<g/>
,	,	kIx,	,
Filipín	Filipíny	k1gFnPc2	Filipíny
a	a	k8xC	a
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
Libanonu	Libanon	k1gInSc2	Libanon
přicházejí	přicházet	k5eAaImIp3nP	přicházet
rovněž	rovněž	k9	rovněž
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
muslimům	muslim	k1gMnPc3	muslim
(	(	kIx(	(
<g/>
60	[number]	k4	60
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Šíité	šíita	k1gMnPc1	šíita
(	(	kIx(	(
<g/>
32	[number]	k4	32
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sunnité	sunnita	k1gMnPc1	sunnita
(	(	kIx(	(
<g/>
21	[number]	k4	21
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Drúzové	drúzový	k2eAgFnSc2d1	drúzový
(	(	kIx(	(
<g/>
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Alavité	Alavitý	k2eAgFnPc1d1	Alavitý
(	(	kIx(	(
<g/>
Nusairové	Nusairová	k1gFnPc1	Nusairová
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc1	náboženství
starší	starší	k1gMnSc1	starší
než	než	k8xS	než
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
přijali	přijmout	k5eAaPmAgMnP	přijmout
šíitský	šíitský	k2eAgInSc4d1	šíitský
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ismailité	Ismailitý	k2eAgFnSc3d1	Ismailitý
(	(	kIx(	(
<g/>
šíitská	šíitský	k2eAgFnSc1d1	šíitská
sekta	sekta	k1gFnSc1	sekta
<g/>
)	)	kIx)	)
Ke	k	k7c3	k
křesťanům	křesťan	k1gMnPc3	křesťan
(	(	kIx(	(
<g/>
39	[number]	k4	39
%	%	kIx~	%
<g/>
)	)	kIx)	)
největší	veliký	k2eAgFnSc1d3	veliký
skupina	skupina	k1gFnSc1	skupina
Maronitská	Maronitský	k2eAgFnSc1d1	Maronitská
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
Řečtí	řecký	k2eAgMnPc1d1	řecký
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
Řecká	řecký	k2eAgFnSc1d1	řecká
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
<g/>
,	,	kIx,	,
Arménská	arménský	k2eAgFnSc1d1	arménská
apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
Syrská	syrský	k2eAgFnSc1d1	Syrská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Ostatní	ostatní	k2eAgNnSc1d1	ostatní
náboženství	náboženství	k1gNnSc1	náboženství
včetně	včetně	k7c2	včetně
asi	asi	k9	asi
1000	[number]	k4	1000
židů	žid	k1gMnPc2	žid
tvoří	tvořit	k5eAaImIp3nS	tvořit
zbylé	zbylý	k2eAgNnSc4d1	zbylé
1	[number]	k4	1
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
oficiálně	oficiálně	k6eAd1	oficiálně
uznávaných	uznávaný	k2eAgInPc2d1	uznávaný
je	být	k5eAaImIp3nS	být
17	[number]	k4	17
náboženských	náboženský	k2eAgFnPc2d1	náboženská
sekt	sekta	k1gFnPc2	sekta
Gramotných	gramotný	k2eAgInPc2d1	gramotný
je	být	k5eAaImIp3nS	být
89	[number]	k4	89
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Osídleno	osídlen	k2eAgNnSc1d1	osídleno
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
pobřeží	pobřeží	k1gNnSc4	pobřeží
a	a	k8xC	a
pohoří	pohoří	k1gNnSc4	pohoří
Libanon	Libanon	k1gInSc1	Libanon
kolem	kolem	k7c2	kolem
Bejrútu	Bejrút	k1gInSc2	Bejrút
(	(	kIx(	(
<g/>
90	[number]	k4	90
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
diaspora	diaspora	k1gFnSc1	diaspora
přesahující	přesahující	k2eAgFnSc1d1	přesahující
12	[number]	k4	12
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
Libanonců	Libanonec	k1gMnPc2	Libanonec
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
údajů	údaj	k1gInPc2	údaj
až	až	k9	až
15	[number]	k4	15
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Emigrace	emigrace	k1gFnSc1	emigrace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
několika	několik	k4yIc6	několik
vlnách	vlna	k1gFnPc6	vlna
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
další	další	k2eAgFnSc1d1	další
její	její	k3xOp3gFnSc1	její
významná	významný	k2eAgFnSc1d1	významná
fáze	fáze	k1gFnSc1	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
závěru	závěr	k1gInSc2	závěr
studií	studie	k1gFnPc2	studie
UNPF	UNPF	kA	UNPF
a	a	k8xC	a
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
sociálních	sociální	k2eAgFnPc2d1	sociální
záležitostí	záležitost	k1gFnPc2	záležitost
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
a	a	k8xC	a
2002	[number]	k4	2002
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
mezi	mezi	k7c7	mezi
8	[number]	k4	8
až	až	k9	až
15	[number]	k4	15
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
</s>
<s>
Libanonců	Libanonec	k1gMnPc2	Libanonec
měsíčně	měsíčně	k6eAd1	měsíčně
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
o	o	k7c4	o
muže	muž	k1gMnPc4	muž
–	–	k?	–
křesťany	křesťan	k1gMnPc7	křesťan
-	-	kIx~	-
mezi	mezi	k7c4	mezi
25	[number]	k4	25
a	a	k8xC	a
35	[number]	k4	35
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
nejistá	jistý	k2eNgFnSc1d1	nejistá
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
vědomí	vědomí	k1gNnSc4	vědomí
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
varující	varující	k2eAgNnSc1d1	varující
zejména	zejména	k9	zejména
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
absolventů	absolvent	k1gMnPc2	absolvent
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
Konkrétní	konkrétní	k2eAgNnPc1d1	konkrétní
demografická	demografický	k2eAgNnPc1d1	demografické
data	datum	k1gNnPc1	datum
<g/>
:	:	kIx,	:
Přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1,2	[number]	k4	1,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
"	"	kIx"	"
<g/>
Emerging	Emerging	k1gInSc1	Emerging
Lebanon	Lebanon	k1gNnSc1	Lebanon
2005	[number]	k4	2005
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2,4	[number]	k4	2,4
%	%	kIx~	%
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
UNICEF	UNICEF	kA	UNICEF
<g/>
)	)	kIx)	)
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
domácnost	domácnost	k1gFnSc1	domácnost
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
4,1	[number]	k4	4,1
členů	člen	k1gInPc2	člen
v	v	k7c6	v
Bejrútu	Bejrút	k1gInSc6	Bejrút
<g/>
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
demografické	demografický	k2eAgFnSc2d1	demografická
konference	konference	k1gFnSc2	konference
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
5,5	[number]	k4	5,5
členů	člen	k1gInPc2	člen
ve	v	k7c6	v
venkovských	venkovský	k2eAgFnPc6d1	venkovská
oblastech	oblast	k1gFnPc6	oblast
Počet	počet	k1gInSc4	počet
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
průměrně	průměrně	k6eAd1	průměrně
2	[number]	k4	2
děti	dítě	k1gFnPc1	dítě
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
"	"	kIx"	"
<g/>
Emerging	Emerging	k1gInSc1	Emerging
Lebanon	Lebanon	k1gNnSc1	Lebanon
2005	[number]	k4	2005
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
ve	v	k7c6	v
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
90	[number]	k4	90
%	%	kIx~	%
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
"	"	kIx"	"
<g/>
Emerging	Emerging	k1gInSc1	Emerging
<g />
.	.	kIx.	.
</s>
<s>
Lebanon	Lebanon	k1gInSc1	Lebanon
2005	[number]	k4	2005
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Věková	věkový	k2eAgFnSc1d1	věková
struktura	struktura	k1gFnSc1	struktura
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
0	[number]	k4	0
-	-	kIx~	-
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
31,1	[number]	k4	31,1
%	%	kIx~	%
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
ESCWA	ESCWA	kA	ESCWA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
15	[number]	k4	15
-	-	kIx~	-
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
62,8	[number]	k4	62,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
64	[number]	k4	64
<g/>
+	+	kIx~	+
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
6,1	[number]	k4	6,1
%	%	kIx~	%
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
<g />
.	.	kIx.	.
</s>
<s>
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
71,3	[number]	k4	71,3
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
<g/>
:	:	kIx,	:
68,87	[number]	k4	68,87
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
ženy	žena	k1gFnPc1	žena
<g/>
:	:	kIx,	:
73,74	[number]	k4	73,74
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
www.abacci.com/atlas	www.abacci.com/atlas	k1gInSc1	www.abacci.com/atlas
<g/>
)	)	kIx)	)
Negramotnost	negramotnost	k1gFnSc1	negramotnost
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
10	[number]	k4	10
%	%	kIx~	%
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
"	"	kIx"	"
<g/>
Emerging	Emerging	k1gInSc1	Emerging
<g />
.	.	kIx.	.
</s>
<s>
Lebanon	Lebanon	k1gInSc1	Lebanon
2005	[number]	k4	2005
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Poměr	poměr	k1gInSc1	poměr
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
49	[number]	k4	49
%	%	kIx~	%
:	:	kIx,	:
51	[number]	k4	51
%	%	kIx~	%
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
ESCWA	ESCWA	kA	ESCWA
<g/>
)	)	kIx)	)
Úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
5	[number]	k4	5
na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
UNICEF	UNICEF	kA	UNICEF
<g/>
)	)	kIx)	)
Kojenecká	kojenecký	k2eAgFnSc1d1	kojenecká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
28	[number]	k4	28
na	na	k7c4	na
1000	[number]	k4	1000
porodů	porod	k1gInPc2	porod
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
UNICEF	UNICEF	kA	UNICEF
<g/>
)	)	kIx)	)
Palestinci	Palestinec	k1gMnPc1	Palestinec
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
libanonským	libanonský	k2eAgNnSc7d1	libanonské
bolavým	bolavý	k2eAgNnSc7d1	bolavé
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
narodilo	narodit	k5eAaPmAgNnS	narodit
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc4	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
však	však	k9	však
snáší	snášet	k5eAaImIp3nS	snášet
většina	většina	k1gFnSc1	většina
Libanonců	Libanonec	k1gMnPc2	Libanonec
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
omezováni	omezovat	k5eAaImNgMnP	omezovat
řadou	řada	k1gFnSc7	řada
vyhlášek	vyhláška	k1gFnPc2	vyhláška
a	a	k8xC	a
nařízení	nařízení	k1gNnSc2	nařízení
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
uprchlických	uprchlický	k2eAgInPc6d1	uprchlický
táborech	tábor	k1gInPc6	tábor
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
darech	dar	k1gInPc6	dar
od	od	k7c2	od
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
počtu	počet	k1gInSc2	počet
uprchlíků	uprchlík	k1gMnPc2	uprchlík
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitá	důležitý	k2eAgFnSc1d1	důležitá
početní	početní	k2eAgFnSc1d1	početní
velikost	velikost	k1gFnSc1	velikost
populace	populace	k1gFnSc2	populace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
uprchlíky	uprchlík	k1gMnPc4	uprchlík
přijímá	přijímat	k5eAaImIp3nS	přijímat
a	a	k8xC	a
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
je	být	k5eAaImIp3nS	být
uprchlíkem	uprchlík	k1gMnSc7	uprchlík
každý	každý	k3xTgMnSc1	každý
desátý	desátý	k4xOgMnSc1	desátý
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
v	v	k7c6	v
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
každý	každý	k3xTgInSc4	každý
pátý	pátý	k4xOgInSc4	pátý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uprchlíci	uprchlík	k1gMnPc1	uprchlík
tvoří	tvořit	k5eAaImIp3nP	tvořit
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
a	a	k8xC	a
pak	pak	k6eAd1	pak
v	v	k7c6	v
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
<g/>
,	,	kIx,	,
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
v	v	k7c6	v
Pásmu	pásmo	k1gNnSc6	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
.	.	kIx.	.
</s>
<s>
Uprchlíci	uprchlík	k1gMnPc1	uprchlík
před	před	k7c7	před
syrskou	syrský	k2eAgFnSc7d1	Syrská
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Libanon	Libanon	k1gInSc4	Libanon
dalším	další	k2eAgInSc7d1	další
obrovským	obrovský	k2eAgInSc7d1	obrovský
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
přes	přes	k7c4	přes
1,1	[number]	k4	1,1
milionu	milion	k4xCgInSc2	milion
syrských	syrský	k2eAgInPc2d1	syrský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
podle	podle	k7c2	podle
OSN	OSN	kA	OSN
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
poměr	poměr	k1gInSc1	poměr
mohl	moct	k5eAaImAgInS	moct
brzy	brzy	k6eAd1	brzy
vzrůst	vzrůst	k1gInSc1	vzrůst
na	na	k7c4	na
třetinu	třetina	k1gFnSc4	třetina
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
malý	malý	k2eAgInSc4d1	malý
Libanon	Libanon	k1gInSc4	Libanon
velkou	velký	k2eAgFnSc4d1	velká
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
zátěž	zátěž	k1gFnSc4	zátěž
<g/>
.	.	kIx.	.
šíité	šíita	k1gMnPc1	šíita
32	[number]	k4	32
%	%	kIx~	%
-	-	kIx~	-
hl.	hl.	k?	hl.
jih	jih	k1gInSc4	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Bikáa	Biká	k1gInSc2	Biká
a	a	k8xC	a
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
předměstí	předměstí	k1gNnSc6	předměstí
Bejrútu	Bejrút	k1gInSc2	Bejrút
maronité	maronitý	k2eAgInPc1d1	maronitý
24,5	[number]	k4	24,5
%	%	kIx~	%
-	-	kIx~	-
Mount	Mount	k1gMnSc1	Mount
Lebanon	Lebanon	k1gMnSc1	Lebanon
a	a	k8xC	a
východní	východní	k2eAgInSc1d1	východní
Bejrút	Bejrút	k1gInSc1	Bejrút
sunnité	sunnita	k1gMnPc1	sunnita
21	[number]	k4	21
%	%	kIx~	%
-	-	kIx~	-
žijí	žít	k5eAaImIp3nP	žít
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Tripolisu	Tripolis	k1gInSc6	Tripolis
<g/>
,	,	kIx,	,
Sidónu	Sidón	k1gInSc2	Sidón
a	a	k8xC	a
Bejrútu	Bejrút	k1gInSc2	Bejrút
drúzové	drúzový	k2eAgFnSc2d1	drúzový
<g />
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
%	%	kIx~	%
-	-	kIx~	-
obývají	obývat	k5eAaImIp3nP	obývat
pohoří	pohoří	k1gNnSc4	pohoří
Chouf	Chouf	k1gInSc4	Chouf
řečtí	řecký	k2eAgMnPc1d1	řecký
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
6,5	[number]	k4	6,5
%	%	kIx~	%
řečtí	řecký	k2eAgMnPc1d1	řecký
katolíci	katolík	k1gMnPc1	katolík
4	[number]	k4	4
%	%	kIx~	%
arménští	arménský	k2eAgMnPc1d1	arménský
křesťané	křesťan	k1gMnPc1	křesťan
4	[number]	k4	4
%	%	kIx~	%
ostatní	ostatní	k1gNnSc4	ostatní
1	[number]	k4	1
%	%	kIx~	%
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
asi	asi	k9	asi
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Libanonu	Libanon	k1gInSc2	Libanon
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
židovskému	židovský	k2eAgInSc3d1	židovský
vyznání	vyznání	k1gNnPc2	vyznání
5	[number]	k4	5
legálně	legálně	k6eAd1	legálně
uznaných	uznaný	k2eAgFnPc2d1	uznaná
muslimských	muslimský	k2eAgFnPc2d1	muslimská
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
abecedně	abecedně	k6eAd1	abecedně
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
alawité	alawitý	k2eAgFnSc2d1	alawitý
<g/>
,	,	kIx,	,
drúzové	drúzový	k2eAgFnSc2d1	drúzový
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ismailité	ismailitý	k2eAgFnPc1d1	ismailitý
<g/>
,	,	kIx,	,
šíité	šíita	k1gMnPc1	šíita
<g/>
,	,	kIx,	,
sunnité	sunnita	k1gMnPc1	sunnita
(	(	kIx(	(
<g/>
cca	cca	kA	cca
60	[number]	k4	60
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
11	[number]	k4	11
legálně	legálně	k6eAd1	legálně
uznaných	uznaný	k2eAgFnPc2d1	uznaná
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
4	[number]	k4	4
ortodoxní	ortodoxní	k2eAgInPc1d1	ortodoxní
<g/>
,	,	kIx,	,
6	[number]	k4	6
katolických	katolický	k2eAgMnPc2d1	katolický
<g/>
,	,	kIx,	,
1	[number]	k4	1
protestantská	protestantský	k2eAgFnSc1d1	protestantská
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
přiznáváno	přiznávat	k5eAaImNgNnS	přiznávat
cca	cca	kA	cca
39	[number]	k4	39
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
asi	asi	k9	asi
25	[number]	k4	25
%	%	kIx~	%
<g/>
)	)	kIx)	)
Maronité	Maronitý	k2eAgFnPc1d1	Maronitý
-	-	kIx~	-
Odvozují	odvozovat	k5eAaImIp3nP	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
mnicha	mnich	k1gMnSc2	mnich
Marona	Maron	k1gMnSc2	Maron
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
1182	[number]	k4	1182
unie	unie	k1gFnSc1	unie
s	s	k7c7	s
římskou	římska	k1gFnSc7	římska
církví	církev	k1gFnPc2	církev
–	–	k?	–
zachování	zachování	k1gNnSc4	zachování
jiných	jiný	k2eAgFnPc2d1	jiná
liturgii	liturgie	k1gFnSc3	liturgie
a	a	k8xC	a
zvyklostí	zvyklost	k1gFnPc2	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
značná	značný	k2eAgFnSc1d1	značná
autonomie	autonomie	k1gFnSc1	autonomie
a	a	k8xC	a
politický	politický	k2eAgInSc1d1	politický
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
politicky	politicky	k6eAd1	politicky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Libanon	Libanon	k1gInSc4	Libanon
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
asi	asi	k9	asi
3	[number]	k4	3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Drůzové	Drůz	k1gMnPc1	Drůz
-	-	kIx~	-
pojmenovaní	pojmenovaný	k2eAgMnPc1d1	pojmenovaný
po	po	k7c6	po
Muhammadu	Muhammad	k1gInSc6	Muhammad
al-Darazím	al-Darazit	k5eAaPmIp1nS	al-Darazit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odštěpili	odštěpit	k5eAaPmAgMnP	odštěpit
r.	r.	kA	r.
1017	[number]	k4	1017
od	od	k7c2	od
šíitského	šíitský	k2eAgInSc2d1	šíitský
směru	směr	k1gInSc2	směr
Ismáílíjcù	Ismáílíjcù	k1gFnSc2	Ismáílíjcù
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
a	a	k8xC	a
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
přísné	přísný	k2eAgNnSc4d1	přísné
členění	členění	k1gNnSc4	členění
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
"	"	kIx"	"
<g/>
moudré	moudrý	k2eAgFnSc3d1	moudrá
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
nevědomé	nevědomé	k1gNnSc1	nevědomé
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
učení	učení	k1gNnSc3	učení
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
moudří	moudrý	k2eAgMnPc1d1	moudrý
(	(	kIx(	(
<g/>
AFRISOU	AFRISOU	kA	AFRISOU
<g/>
,	,	kIx,	,
Bajer	Bajer	k1gMnSc1	Bajer
<g/>
)	)	kIx)	)
V	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
ztratil	ztratit	k5eAaPmAgInS	ztratit
Libanon	Libanon	k1gInSc1	Libanon
mnoho	mnoho	k6eAd1	mnoho
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
"	"	kIx"	"
<g/>
Středomořské	středomořský	k2eAgNnSc4d1	středomořské
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ustálil	ustálit	k5eAaPmAgMnS	ustálit
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
i	i	k9	i
politickou	politický	k2eAgFnSc4d1	politická
situaci	situace	k1gFnSc4	situace
Rafík	Rafík	k1gMnSc1	Rafík
Harírí	Harírí	k1gMnSc1	Harírí
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
velikou	veliký	k2eAgFnSc4d1	veliká
míru	míra	k1gFnSc4	míra
zadlužení	zadlužení	k1gNnSc2	zadlužení
–	–	k?	–
170	[number]	k4	170
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
dnes	dnes	k6eAd1	dnes
roste	růst	k5eAaImIp3nS	růst
tempem	tempo	k1gNnSc7	tempo
4	[number]	k4	4
%	%	kIx~	%
a	a	k8xC	a
HDP	HDP	kA	HDP
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
10	[number]	k4	10
000	[number]	k4	000
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ji	on	k3xPp3gFnSc4	on
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
silnější	silný	k2eAgInSc1d2	silnější
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
ohrožována	ohrožovat	k5eAaImNgFnS	ohrožovat
nestabilitou	nestabilita	k1gFnSc7	nestabilita
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
vnější	vnější	k2eAgNnSc1d1	vnější
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
8	[number]	k4	8
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
extrémní	extrémní	k2eAgFnSc6d1	extrémní
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nestabilitu	nestabilita	k1gFnSc4	nestabilita
a	a	k8xC	a
minulost	minulost	k1gFnSc4	minulost
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Libanon	Libanon	k1gInSc1	Libanon
nejvyspělejší	vyspělý	k2eAgInSc1d3	nejvyspělejší
arabskou	arabský	k2eAgFnSc7d1	arabská
zemí	zem	k1gFnSc7	zem
vyjma	vyjma	k7c2	vyjma
ropných	ropný	k2eAgFnPc2d1	ropná
velmocí	velmoc	k1gFnPc2	velmoc
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
okolo	okolo	k7c2	okolo
15	[number]	k4	15
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
rajčata	rajče	k1gNnPc1	rajče
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
či	či	k8xC	či
citrusy	citrus	k1gInPc1	citrus
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
rozvinulo	rozvinout	k5eAaPmAgNnS	rozvinout
pěstování	pěstování	k1gNnSc3	pěstování
opia	opium	k1gNnSc2	opium
a	a	k8xC	a
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
.	.	kIx.	.
</s>
<s>
Chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
především	především	k9	především
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
už	už	k6eAd1	už
kozy	koza	k1gFnSc2	koza
či	či	k8xC	či
ovce	ovce	k1gFnSc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nP	těžet
se	se	k3xPyFc4	se
stavební	stavební	k2eAgFnPc1d1	stavební
hmoty	hmota	k1gFnPc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
energie	energie	k1gFnSc2	energie
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
spalování	spalování	k1gNnSc2	spalování
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
cement	cement	k1gInSc1	cement
<g/>
,	,	kIx,	,
oděvy	oděv	k1gInPc1	oděv
a	a	k8xC	a
šperky	šperk	k1gInPc1	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnPc1	silnice
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
války	válka	k1gFnSc2	válka
nebyly	být	k5eNaImAgInP	být
udržované	udržovaný	k2eAgInPc1d1	udržovaný
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
byla	být	k5eAaImAgFnS	být
válkou	válka	k1gFnSc7	válka
kompletně	kompletně	k6eAd1	kompletně
vyřazena	vyřadit	k5eAaPmNgFnS	vyřadit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bejrútu	Bejrút	k1gInSc6	Bejrút
<g/>
.	.	kIx.	.
</s>
<s>
Vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
potraviny	potravina	k1gFnPc1	potravina
a	a	k8xC	a
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
výrobky	výrobek	k1gInPc1	výrobek
(	(	kIx(	(
<g/>
pomeranče	pomeranč	k1gInPc1	pomeranč
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dováží	dovážet	k5eAaImIp3nP	dovážet
se	se	k3xPyFc4	se
stavebniny	stavebnina	k1gFnSc2	stavebnina
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc4	stroj
a	a	k8xC	a
spotřební	spotřební	k2eAgNnSc4d1	spotřební
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
také	také	k9	také
elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
se	se	k3xPyFc4	se
dováží	dovážet	k5eAaImIp3nS	dovážet
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
obchodní	obchodní	k2eAgInPc1d1	obchodní
kontakty	kontakt	k1gInPc1	kontakt
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
sídlícím	sídlící	k2eAgMnSc7d1	sídlící
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Bejrút	Bejrút	k1gInSc1	Bejrút
<g/>
.	.	kIx.	.
</s>
<s>
Libanon	Libanon	k1gInSc1	Libanon
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
republika	republika	k1gFnSc1	republika
–	–	k?	–
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
organizované	organizovaný	k2eAgFnPc4d1	organizovaná
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
konfesním	konfesní	k2eAgInSc6d1	konfesní
principu	princip	k1gInSc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1926	[number]	k4	1926
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgMnSc1d3	nejstarší
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
platná	platný	k2eAgFnSc1d1	platná
ústava	ústava	k1gFnSc1	ústava
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
Taífskou	Taífský	k2eAgFnSc7d1	Taífský
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zaručila	zaručit	k5eAaPmAgFnS	zaručit
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
muslimů	muslim	k1gMnPc2	muslim
a	a	k8xC	a
křesťanů	křesťan	k1gMnPc2	křesťan
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
počet	počet	k1gInSc1	počet
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
99	[number]	k4	99
upraven	upravit	k5eAaPmNgMnS	upravit
na	na	k7c4	na
128	[number]	k4	128
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
64	[number]	k4	64
křesel	křeslo	k1gNnPc2	křeslo
bylo	být	k5eAaImAgNnS	být
přiděleno	přidělit	k5eAaPmNgNnS	přidělit
muslimům	muslim	k1gMnPc3	muslim
a	a	k8xC	a
zbývajících	zbývající	k2eAgInPc2d1	zbývající
64	[number]	k4	64
křesťanům	křesťan	k1gMnPc3	křesťan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
tzv.	tzv.	kA	tzv.
politický	politický	k2eAgInSc4d1	politický
konfesionalismus	konfesionalismus	k1gInSc4	konfesionalismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
politické	politický	k2eAgInPc4d1	politický
mandáty	mandát	k1gInPc4	mandát
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
podle	podle	k7c2	podle
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
představuje	představovat	k5eAaImIp3nS	představovat
jednokomorové	jednokomorový	k2eAgNnSc1d1	jednokomorové
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
(	(	kIx(	(
<g/>
Assemblee	Assemblee	k1gFnSc1	Assemblee
Nationale	Nationale	k1gFnSc2	Nationale
–	–	k?	–
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
Madžlis	Madžlis	k1gInSc1	Madžlis
al-Nuwab	al-Nuwab	k1gInSc1	al-Nuwab
–	–	k?	–
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
se	s	k7c7	s
128	[number]	k4	128
křesly	křeslo	k1gNnPc7	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poslední	poslední	k2eAgInSc4d1	poslední
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
shodnout	shodnout	k5eAaPmF	shodnout
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
volebním	volební	k2eAgInSc6d1	volební
zákoně	zákon	k1gInSc6	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
volbě	volba	k1gFnSc6	volba
prezidenta	prezident	k1gMnSc2	prezident
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
si	se	k3xPyFc3	se
poslanci	poslanec	k1gMnPc1	poslanec
podruhé	podruhé	k6eAd1	podruhé
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
až	až	k9	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
následné	následný	k2eAgNnSc4d1	následné
schválení	schválení	k1gNnSc4	schválení
nového	nový	k2eAgInSc2d1	nový
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
odložil	odložit	k5eAaPmAgMnS	odložit
prezident	prezident	k1gMnSc1	prezident
Michel	Michel	k1gMnSc1	Michel
Aoun	Aoun	k1gMnSc1	Aoun
volby	volba	k1gFnPc4	volba
na	na	k7c4	na
květen	květen	k1gInSc4	květen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
poslancům	poslanec	k1gMnPc3	poslanec
potřetí	potřetí	k4xO	potřetí
prodloužil	prodloužit	k5eAaPmAgInS	prodloužit
mandát	mandát	k1gInSc1	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Rozestup	rozestup	k1gInSc1	rozestup
mezi	mezi	k7c7	mezi
volbami	volba	k1gFnPc7	volba
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
že	že	k8xS	že
současní	současný	k2eAgMnPc1d1	současný
poslanci	poslanec	k1gMnPc1	poslanec
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
strávili	strávit	k5eAaPmAgMnP	strávit
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
mandát	mandát	k1gInSc4	mandát
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
poslanci	poslanec	k1gMnPc1	poslanec
z	z	k7c2	z
20	[number]	k4	20
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
7	[number]	k4	7
nezávislých	závislý	k2eNgInPc2d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
Pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
stranu	strana	k1gFnSc4	strana
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
strany	strana	k1gFnPc4	strana
součástí	součást	k1gFnPc2	součást
buďto	buďto	k8xC	buďto
Aliance	aliance	k1gFnSc1	aliance
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
antisionistická	antisionistický	k2eAgFnSc1d1	antisionistická
a	a	k8xC	a
prosyrská	prosyrský	k2eAgFnSc1d1	prosyrská
politická	politický	k2eAgFnSc1d1	politická
koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Hizballáh	Hizballáh	k1gInSc1	Hizballáh
<g/>
,	,	kIx,	,
Volné	volný	k2eAgNnSc1d1	volné
vlastenecké	vlastenecký	k2eAgNnSc1d1	vlastenecké
hnutí	hnutí	k1gNnSc1	hnutí
nebo	nebo	k8xC	nebo
Amal	Amal	k1gInSc1	Amal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Aliance	aliance	k1gFnSc1	aliance
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
(	(	kIx(	(
<g/>
protisyrská	protisyrský	k2eAgFnSc1d1	protisyrský
a	a	k8xC	a
nacionalistická	nacionalistický	k2eAgFnSc1d1	nacionalistická
aliance	aliance	k1gFnSc1	aliance
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Hnutím	hnutí	k1gNnSc7	hnutí
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
členské	členský	k2eAgFnPc1d1	členská
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Katáib	Katáib	k1gInSc1	Katáib
<g/>
,	,	kIx,	,
Libanonské	libanonský	k2eAgInPc1d1	libanonský
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
převážně	převážně	k6eAd1	převážně
muslimské	muslimský	k2eAgNnSc1d1	muslimské
liberální	liberální	k2eAgNnSc1d1	liberální
Hnutí	hnutí	k1gNnSc1	hnutí
budoucnosti	budoucnost	k1gFnSc2	budoucnost
vedené	vedený	k2eAgFnSc2d1	vedená
Saadem	Saad	k1gMnSc7	Saad
Harírím	Harírí	k1gMnSc7	Harírí
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
zesnulého	zesnulý	k1gMnSc2	zesnulý
Rafíka	Rafík	k1gMnSc2	Rafík
Harírího	Harírí	k1gMnSc2	Harírí
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
budoucnosti	budoucnost	k1gFnSc2	budoucnost
je	být	k5eAaImIp3nS	být
následované	následovaný	k2eAgNnSc1d1	následované
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
Volným	volný	k2eAgNnSc7d1	volné
vlasteneckým	vlastenecký	k2eAgNnSc7d1	vlastenecké
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
šíitskými	šíitský	k2eAgNnPc7d1	šíitské
hnutími	hnutí	k1gNnPc7	hnutí
Amal	Amal	k1gMnSc1	Amal
a	a	k8xC	a
Hizballáh	Hizballáh	k1gMnSc1	Hizballáh
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
i	i	k9	i
strana	strana	k1gFnSc1	strana
Katáib	Katáiba	k1gFnPc2	Katáiba
nebo	nebo	k8xC	nebo
Libanonské	libanonský	k2eAgFnSc2d1	libanonská
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Symbolickou	symbolický	k2eAgFnSc4d1	symbolická
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
strana	strana	k1gFnSc1	strana
Baas	Baasa	k1gFnPc2	Baasa
nebo	nebo	k8xC	nebo
Syrská	syrský	k2eAgFnSc1d1	Syrská
sociálně	sociálně	k6eAd1	sociálně
nacionální	nacionální	k2eAgFnSc1d1	nacionální
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Libanonu	Libanon	k1gInSc2	Libanon
má	mít	k5eAaImIp3nS	mít
30	[number]	k4	30
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vedená	vedený	k2eAgFnSc1d1	vedená
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
sunnitský	sunnitský	k2eAgMnSc1d1	sunnitský
muslim	muslim	k1gMnSc1	muslim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
lídr	lídr	k1gMnSc1	lídr
Hnutí	hnutí	k1gNnSc2	hnutí
budoucnosti	budoucnost	k1gFnSc2	budoucnost
Saad	Saad	k1gMnSc1	Saad
Harírí	Harírí	k1gMnSc1	Harírí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
10	[number]	k4	10
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
je	být	k5eAaImIp3nS	být
vláda	vláda	k1gFnSc1	vláda
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
křesťany	křesťan	k1gMnPc4	křesťan
a	a	k8xC	a
muslimy	muslim	k1gMnPc4	muslim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
nejvíce	hodně	k6eAd3	hodně
míst	místo	k1gNnPc2	místo
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Volné	volný	k2eAgNnSc1d1	volné
vlastenecké	vlastenecký	k2eAgNnSc1d1	vlastenecké
hnutí	hnutí	k1gNnSc1	hnutí
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nasledované	nasledovaný	k2eAgNnSc1d1	nasledované
Hnutím	hnutí	k1gNnSc7	hnutí
budoucnosti	budoucnost	k1gFnSc2	budoucnost
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hizballáh	Hizballáh	k1gInSc1	Hizballáh
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
2	[number]	k4	2
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
ministry	ministr	k1gMnPc7	ministr
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Libanonu	Libanon	k1gInSc2	Libanon
existuje	existovat	k5eAaImIp3nS	existovat
již	již	k6eAd1	již
4	[number]	k4	4
000	[number]	k4	000
let	léto	k1gNnPc2	léto
Název	název	k1gInSc1	název
Libanonu	Libanon	k1gInSc2	Libanon
byl	být	k5eAaImAgInS	být
71	[number]	k4	71
krát	krát	k6eAd1	krát
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
Libanon	Libanon	k1gInSc4	Libanon
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
blízkovýchodní	blízkovýchodní	k2eAgFnSc1d1	blízkovýchodní
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nemá	mít	k5eNaImIp3nS	mít
poušť	poušť	k1gFnSc4	poušť
Libanon	Libanon	k1gInSc1	Libanon
má	mít	k5eAaImIp3nS	mít
17	[number]	k4	17
náboženských	náboženský	k2eAgFnPc2d1	náboženská
komunit	komunita	k1gFnPc2	komunita
V	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
řek	řeka	k1gFnPc2	řeka
Bejrút	Bejrút	k1gInSc4	Bejrút
byl	být	k5eAaImAgInS	být
sedmkrát	sedmkrát	k6eAd1	sedmkrát
zničen	zničit	k5eAaPmNgInS	zničit
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
postaven	postaven	k2eAgMnSc1d1	postaven
MUSIL	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Libanonu	Libanon	k1gInSc2	Libanon
k	k	k7c3	k
Tigridu	Tigris	k1gInSc3	Tigris
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
Syrie	Syrie	k1gFnSc1	Syrie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
252	[number]	k4	252
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Libanon	Libanon	k1gInSc1	Libanon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Libanon	Libanon	k1gInSc1	Libanon
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
Libanon	Libanon	k1gInSc1	Libanon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kategorie	kategorie	k1gFnSc1	kategorie
Libanon	Libanon	k1gInSc1	Libanon
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
CIA	CIA	kA	CIA
-	-	kIx~	-
The	The	k1gMnSc1	The
World	Worlda	k1gFnPc2	Worlda
Factbook	Factbook	k1gInSc1	Factbook
https://www.cia.gov/library/publications/the-world-factbook/geos/le.html	[url]	k1gMnSc1	https://www.cia.gov/library/publications/the-world-factbook/geos/le.html
lebweb	lebwba	k1gFnPc2	lebwba
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Mapy	mapa	k1gFnPc4	mapa
Libanon	Libanon	k1gInSc4	Libanon
Lebanon	Lebanon	k1gNnSc1	Lebanon
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Lebanon	Lebanon	k1gInSc1	Lebanon
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k1gMnSc3	Burea
of	of	k?	of
Near	Near	k1gMnSc1	Near
Eastern	Eastern	k1gMnSc1	Eastern
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Lebanon	Lebanon	k1gInSc1	Lebanon
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Lebanon	Lebanon	k1gInSc1	Lebanon
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Bejrútu	Bejrút	k1gInSc6	Bejrút
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Libanon	Libanon	k1gInSc1	Libanon
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
BARNETT	BARNETT	kA	BARNETT
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lebanon	Lebanon	k1gInSc1	Lebanon
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
