<p>
<s>
Aérospatiale-BAC	Aérospatiale-BAC	k?	Aérospatiale-BAC
Concorde	Concord	k1gMnSc5	Concord
101	[number]	k4	101
<g/>
/	/	kIx~	/
<g/>
102	[number]	k4	102
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
concordia	concordium	k1gNnSc2	concordium
<g/>
,	,	kIx,	,
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
svornost	svornost	k1gFnSc1	svornost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nadzvukový	nadzvukový	k2eAgInSc1d1	nadzvukový
dopravní	dopravní	k2eAgInSc1d1	dopravní
letoun	letoun	k1gInSc1	letoun
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
provozovaly	provozovat	k5eAaImAgInP	provozovat
British	British	k1gInSc4	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
a	a	k8xC	a
Air	Air	k1gFnPc2	Air
France	Franc	k1gMnSc4	Franc
<g/>
.	.	kIx.	.
</s>
<s>
Symbolizoval	symbolizovat	k5eAaImAgMnS	symbolizovat
usmíření	usmíření	k1gNnSc1	usmíření
rivality	rivalita	k1gFnSc2	rivalita
mezi	mezi	k7c7	mezi
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Concorde	Concorde	k6eAd1	Concorde
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
patnáctiletém	patnáctiletý	k2eAgInSc6d1	patnáctiletý
vývoji	vývoj	k1gInSc6	vývoj
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
letové	letový	k2eAgFnSc2d1	letová
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
neštěstí	neštěstí	k1gNnSc4	neštěstí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
nedošlo	dojít	k5eNaPmAgNnS	dojít
během	během	k7c2	během
27	[number]	k4	27
let	léto	k1gNnPc2	léto
jeho	on	k3xPp3gInSc2	on
provozu	provoz	k1gInSc2	provoz
nikdy	nikdy	k6eAd1	nikdy
k	k	k7c3	k
větším	veliký	k2eAgFnPc3d2	veliký
nehodám	nehoda	k1gFnPc3	nehoda
nebo	nebo	k8xC	nebo
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
krále	král	k1gMnSc4	král
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
byla	být	k5eAaImAgFnS	být
navržena	navržen	k2eAgFnSc1d1	navržena
a	a	k8xC	a
postavená	postavený	k2eAgFnSc1d1	postavená
firmou	firma	k1gFnSc7	firma
Aérospatiale	Aérospatiala	k1gFnSc3	Aérospatiala
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Airbus	airbus	k1gInSc1	airbus
Group	Group	k1gInSc1	Group
<g/>
)	)	kIx)	)
a	a	k8xC	a
British	British	k1gMnSc1	British
Aircraft	Aircraft	k2eAgInSc4d1	Aircraft
Corporation	Corporation	k1gInSc4	Corporation
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
BAE	BAE	kA	BAE
Systems	Systemsa	k1gFnPc2	Systemsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
Olympus	Olympus	k1gInSc1	Olympus
593	[number]	k4	593
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
a	a	k8xC	a
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
firmou	firma	k1gFnSc7	firma
Rolls-Royce	Rolls-Royce	k1gFnSc2	Rolls-Royce
(	(	kIx(	(
<g/>
Bristol	Bristol	k1gInSc1	Bristol
Siddeley	Siddelea	k1gFnSc2	Siddelea
<g/>
)	)	kIx)	)
a	a	k8xC	a
Snecma	Snecma	k1gFnSc1	Snecma
<g/>
.	.	kIx.	.
</s>
<s>
Přelet	přelet	k1gInSc1	přelet
přes	přes	k7c4	přes
Atlantik	Atlantik	k1gInSc4	Atlantik
trval	trvat	k5eAaImAgInS	trvat
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
až	až	k9	až
3,5	[number]	k4	3,5
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
letová	letový	k2eAgFnSc1d1	letová
výška	výška	k1gFnSc1	výška
byla	být	k5eAaImAgFnS	být
15	[number]	k4	15
km	km	kA	km
po	po	k7c6	po
startu	start	k1gInSc6	start
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
stoupala	stoupat	k5eAaImAgFnS	stoupat
na	na	k7c4	na
18	[number]	k4	18
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Concorde	Concorde	k6eAd1	Concorde
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
u	u	k7c2	u
aerolinií	aerolinie	k1gFnPc2	aerolinie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
i	i	k8xC	i
německá	německý	k2eAgFnSc1d1	německá
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
objednala	objednat	k5eAaPmAgFnS	objednat
tři	tři	k4xCgNnPc4	tři
letadla	letadlo	k1gNnPc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc1	první
let	léto	k1gNnPc2	léto
Concordu	Concord	k1gInSc2	Concord
(	(	kIx(	(
<g/>
F-WTSS	F-WTSS	k1gFnSc1	F-WTSS
<g/>
)	)	kIx)	)
provedla	provést	k5eAaPmAgFnS	provést
zkušební	zkušební	k2eAgFnSc1d1	zkušební
posádka	posádka	k1gFnSc1	posádka
velitele	velitel	k1gMnSc2	velitel
Andrého	André	k1gMnSc2	André
Turcata	Turcat	k1gMnSc2	Turcat
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Toulouse	Toulouse	k1gInSc6	Toulouse
<g/>
.	.	kIx.	.
</s>
<s>
Zálet	zálet	k1gInSc1	zálet
britského	britský	k2eAgInSc2d1	britský
stroje	stroj	k1gInSc2	stroj
(	(	kIx(	(
<g/>
G-BSST	G-BSST	k1gFnSc1	G-BSST
<g/>
)	)	kIx)	)
provedla	provést	k5eAaPmAgFnS	provést
posádka	posádka	k1gFnSc1	posádka
Brian	Brian	k1gMnSc1	Brian
Trubshaw	Trubshaw	k1gMnSc1	Trubshaw
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Cochrane	Cochran	k1gInSc5	Cochran
a	a	k8xC	a
Brian	Brian	k1gMnSc1	Brian
Watts	Watts	k1gInSc4	Watts
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
z	z	k7c2	z
továrního	tovární	k2eAgNnSc2d1	tovární
letiště	letiště	k1gNnSc2	letiště
ve	v	k7c6	v
Filtonu	Filton	k1gInSc6	Filton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
všem	všecek	k3xTgNnPc3	všecek
očekáváním	očekávání	k1gNnPc3	očekávání
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
ropné	ropný	k2eAgFnSc3d1	ropná
krizi	krize	k1gFnSc3	krize
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
komerční	komerční	k2eAgNnPc1d1	komerční
nadzvuková	nadzvukový	k2eAgNnPc1d1	nadzvukové
letadla	letadlo	k1gNnPc1	letadlo
ukázala	ukázat	k5eAaPmAgNnP	ukázat
jako	jako	k9	jako
nezajímavá	zajímavý	k2eNgNnPc1d1	nezajímavé
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
enormní	enormní	k2eAgInPc4d1	enormní
provozní	provozní	k2eAgInPc4d1	provozní
náklady	náklad	k1gInPc4	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
objednávky	objednávka	k1gFnPc1	objednávka
byly	být	k5eAaImAgFnP	být
stornovány	stornovat	k5eAaBmNgFnP	stornovat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
a	a	k8xC	a
British	British	k1gInSc4	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
musely	muset	k5eAaImAgInP	muset
Concordy	Concord	k1gInPc1	Concord
převzít	převzít	k5eAaPmF	převzít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trasách	trasa	k1gFnPc6	trasa
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
/	/	kIx~	/
<g/>
Londýn	Londýn	k1gInSc1	Londýn
–	–	k?	–
New	New	k1gMnSc4	New
York	York	k1gInSc1	York
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
dosažen	dosažen	k2eAgInSc1d1	dosažen
značný	značný	k2eAgInSc1d1	značný
zisk	zisk	k1gInSc1	zisk
v	v	k7c6	v
luxusní	luxusní	k2eAgFnSc6d1	luxusní
přepravě	přeprava	k1gFnSc6	přeprava
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Concorde	Concord	k1gInSc5	Concord
létal	létat	k5eAaImAgMnS	létat
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
British	British	k1gMnSc1	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
také	také	k9	také
na	na	k7c6	na
trasách	trasa	k1gFnPc6	trasa
dalších	další	k2eAgFnPc2d1	další
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
–	–	k?	–
Singapore	Singapor	k1gInSc5	Singapor
Airlines	Airlines	k1gMnSc1	Airlines
a	a	k8xC	a
Braniff	Braniff	k1gMnSc1	Braniff
International	International	k1gMnSc1	International
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
letadlo	letadlo	k1gNnSc1	letadlo
mělo	mít	k5eAaImAgNnS	mít
dokonce	dokonce	k9	dokonce
nápis	nápis	k1gInSc1	nápis
Singapore	Singapor	k1gInSc5	Singapor
Airlines	Airlines	k1gInSc4	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
–	–	k?	–
2	[number]	k4	2
prototypy	prototyp	k1gInPc4	prototyp
<g/>
,	,	kIx,	,
2	[number]	k4	2
předsériové	předsériový	k2eAgInPc1d1	předsériový
modely	model	k1gInPc1	model
<g/>
,	,	kIx,	,
16	[number]	k4	16
sériových	sériový	k2eAgNnPc2d1	sériové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
svého	svůj	k3xOyFgInSc2	svůj
provozu	provoz	k1gInSc2	provoz
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1976	[number]	k4	1976
letoun	letoun	k1gInSc1	letoun
létal	létat	k5eAaImAgInS	létat
i	i	k9	i
do	do	k7c2	do
Ria	Ria	k1gMnSc2	Ria
de	de	k?	de
Janiera	Janier	k1gMnSc2	Janier
a	a	k8xC	a
Singapuru	Singapur	k1gInSc2	Singapur
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
létal	létat	k5eAaImAgInS	létat
jen	jen	k9	jen
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
mezi	mezi	k7c7	mezi
Paříží	Paříž	k1gFnSc7	Paříž
resp.	resp.	kA	resp.
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
New	New	k1gFnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
krátký	krátký	k2eAgInSc1d1	krátký
dolet	dolet	k1gInSc1	dolet
(	(	kIx(	(
<g/>
6	[number]	k4	6
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
mnohých	mnohý	k2eAgNnPc6d1	mnohé
letištích	letiště	k1gNnPc6	letiště
nedostal	dostat	k5eNaPmAgMnS	dostat
povolení	povolení	k1gNnSc4	povolení
k	k	k7c3	k
přistání	přistání	k1gNnSc3	přistání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
příliš	příliš	k6eAd1	příliš
hlučný	hlučný	k2eAgMnSc1d1	hlučný
(	(	kIx(	(
<g/>
startující	startující	k2eAgMnSc1d1	startující
Concorde	Concord	k1gInSc5	Concord
byl	být	k5eAaImAgInS	být
slyšet	slyšet	k5eAaImF	slyšet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
8	[number]	k4	8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prestižních	prestižní	k2eAgInPc2d1	prestižní
důvodů	důvod	k1gInPc2	důvod
Concorde	Concord	k1gInSc5	Concord
létal	létat	k5eAaImAgMnS	létat
charterové	charterový	k2eAgInPc4d1	charterový
lety	let	k1gInPc4	let
i	i	k9	i
na	na	k7c4	na
jiná	jiný	k2eAgNnPc4d1	jiné
letiště	letiště	k1gNnPc4	letiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konec	konec	k1gInSc1	konec
Concordu	Concord	k1gInSc2	Concord
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2000	[number]	k4	2000
se	se	k3xPyFc4	se
Concordu	Concord	k1gInSc6	Concord
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
při	při	k7c6	při
startu	start	k1gInSc6	start
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaulla	Gaull	k1gMnSc2	Gaull
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roztrhla	roztrhnout	k5eAaPmAgFnS	roztrhnout
pneumatika	pneumatika	k1gFnSc1	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Protrhl	protrhnout	k5eAaPmAgMnS	protrhnout
ji	on	k3xPp3gFnSc4	on
kovový	kovový	k2eAgInSc1d1	kovový
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
odpadl	odpadnout	k5eAaPmAgInS	odpadnout
z	z	k7c2	z
letounu	letoun	k1gInSc2	letoun
DC-10	DC-10	k1gMnSc1	DC-10
Continental	Continental	k1gMnSc1	Continental
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
startoval	startovat	k5eAaBmAgMnS	startovat
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
této	tento	k3xDgFnSc2	tento
oficiální	oficiální	k2eAgFnSc2d1	oficiální
příčiny	příčina	k1gFnSc2	příčina
ovšem	ovšem	k9	ovšem
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečným	skutečný	k2eAgInSc7d1	skutečný
důvodem	důvod	k1gInSc7	důvod
roztržení	roztržení	k1gNnSc2	roztržení
pneumatiky	pneumatika	k1gFnSc2	pneumatika
byl	být	k5eAaImAgInS	být
špatně	špatně	k6eAd1	špatně
sesazený	sesazený	k2eAgInSc1d1	sesazený
podvozek	podvozek	k1gInSc1	podvozek
(	(	kIx(	(
<g/>
chybějící	chybějící	k2eAgInSc1d1	chybějící
distanční	distanční	k2eAgInSc1d1	distanční
kroužek	kroužek	k1gInSc1	kroužek
na	na	k7c6	na
hřídeli	hřídel	k1gInSc6	hřídel
mezi	mezi	k7c7	mezi
koly	kolo	k1gNnPc7	kolo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
rozkmitání	rozkmitání	k1gNnSc1	rozkmitání
kol	kola	k1gFnPc2	kola
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
přehřátí	přehřátí	k1gNnSc2	přehřátí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vymrštěné	vymrštěný	k2eAgFnPc1d1	vymrštěná
gumové	gumový	k2eAgFnPc1d1	gumová
části	část	k1gFnPc1	část
pneumatiky	pneumatika	k1gFnSc2	pneumatika
prorazily	prorazit	k5eAaPmAgFnP	prorazit
elektrický	elektrický	k2eAgInSc4d1	elektrický
kabel	kabel	k1gInSc4	kabel
levého	levý	k2eAgInSc2d1	levý
podvozku	podvozek	k1gInSc2	podvozek
a	a	k8xC	a
levou	levý	k2eAgFnSc4d1	levá
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
zčásti	zčásti	k6eAd1	zčásti
také	také	k9	také
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nárazu	náraz	k1gInSc6	náraz
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
nádržích	nádrž	k1gFnPc6	nádrž
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Vytékající	vytékající	k2eAgNnSc1d1	vytékající
palivo	palivo	k1gNnSc1	palivo
se	se	k3xPyFc4	se
zapálilo	zapálit	k5eAaPmAgNnS	zapálit
o	o	k7c4	o
elektrický	elektrický	k2eAgInSc4d1	elektrický
kabel	kabel	k1gInSc4	kabel
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pohonné	pohonný	k2eAgFnSc2d1	pohonná
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vzňalo	vznít	k5eAaPmAgNnS	vznít
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
levém	levý	k2eAgNnSc6d1	levé
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
se	se	k3xPyFc4	se
už	už	k6eAd1	už
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okamžiku	okamžik	k1gInSc6	okamžik
nedal	dát	k5eNaPmAgMnS	dát
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
,	,	kIx,	,
letoun	letoun	k1gInSc1	letoun
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
nouzové	nouzový	k2eAgNnSc4d1	nouzové
brzdění	brzdění	k1gNnSc4	brzdění
příliš	příliš	k6eAd1	příliš
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
.	.	kIx.	.
</s>
<s>
Pilot	pilot	k1gInSc1	pilot
<g/>
,	,	kIx,	,
alarmovaný	alarmovaný	k2eAgInSc1d1	alarmovaný
palubními	palubní	k2eAgInPc7d1	palubní
přístroji	přístroj	k1gInPc7	přístroj
a	a	k8xC	a
řídící	řídící	k2eAgFnSc7d1	řídící
věží	věž	k1gFnSc7	věž
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
jedinou	jediný	k2eAgFnSc4d1	jediná
možnost	možnost	k1gFnSc4	možnost
-	-	kIx~	-
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
a	a	k8xC	a
pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
nouzově	nouzově	k6eAd1	nouzově
přistát	přistát	k5eAaImF	přistát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
požár	požár	k1gInSc1	požár
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
ničil	ničit	k5eAaImAgInS	ničit
strukturu	struktura	k1gFnSc4	struktura
křídla	křídlo	k1gNnSc2	křídlo
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozpadajícím	rozpadající	k2eAgMnSc7d1	rozpadající
se	se	k3xPyFc4	se
křídlem	křídlo	k1gNnSc7	křídlo
a	a	k8xC	a
jen	jen	k9	jen
částečnou	částečný	k2eAgFnSc7d1	částečná
hnací	hnací	k2eAgFnSc7d1	hnací
silou	síla	k1gFnSc7	síla
stroj	stroj	k1gInSc1	stroj
nedokázal	dokázat	k5eNaPmAgInS	dokázat
nabrat	nabrat	k5eAaPmF	nabrat
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzletu	vzlet	k1gInSc6	vzlet
se	se	k3xPyFc4	se
zřítil	zřítit	k5eAaPmAgInS	zřítit
na	na	k7c4	na
hotel	hotel	k1gInSc4	hotel
v	v	k7c6	v
městečku	městečko	k1gNnSc6	městečko
Gonesse	Gonesse	k1gFnSc2	Gonesse
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgNnPc2	všecek
109	[number]	k4	109
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc1	čtyři
osoby	osoba	k1gFnPc1	osoba
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
přišly	přijít	k5eAaPmAgFnP	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
proto	proto	k8xC	proto
zastavila	zastavit	k5eAaPmAgFnS	zastavit
provoz	provoz	k1gInSc4	provoz
všech	všecek	k3xTgMnPc2	všecek
Concordů	Concord	k1gMnPc2	Concord
a	a	k8xC	a
britský	britský	k2eAgInSc1d1	britský
letecký	letecký	k2eAgInSc1d1	letecký
úřad	úřad	k1gInSc1	úřad
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
povolení	povolení	k1gNnPc4	povolení
na	na	k7c4	na
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
opět	opět	k6eAd1	opět
obnoveny	obnovit	k5eAaPmNgInP	obnovit
až	až	k9	až
po	po	k7c6	po
četných	četný	k2eAgFnPc6d1	četná
konstrukčních	konstrukční	k2eAgFnPc6d1	konstrukční
změnách	změna	k1gFnPc6	změna
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
zesílení	zesílení	k1gNnSc4	zesílení
nádrží	nádrž	k1gFnPc2	nádrž
pomocí	pomocí	k7c2	pomocí
vložené	vložený	k2eAgFnSc2d1	vložená
tkaniny	tkanina	k1gFnSc2	tkanina
z	z	k7c2	z
kevlarových	kevlarův	k2eAgNnPc2d1	kevlarův
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
výrobce	výrobce	k1gMnSc1	výrobce
pneumatik	pneumatika	k1gFnPc2	pneumatika
Michelin	Michelina	k1gFnPc2	Michelina
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
stabilnější	stabilní	k2eAgFnSc4d2	stabilnější
pneumatiky	pneumatika	k1gFnPc4	pneumatika
<g/>
,	,	kIx,	,
odolné	odolný	k2eAgFnPc1d1	odolná
vůči	vůči	k7c3	vůči
prasknutí	prasknutí	k1gNnSc3	prasknutí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
u	u	k7c2	u
Airbusu	airbus	k1gInSc2	airbus
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stály	stát	k5eAaImAgFnP	stát
okolo	okolo	k7c2	okolo
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
euro	euro	k1gNnSc1	euro
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Concorde	Concord	k1gInSc5	Concord
těžší	těžký	k2eAgMnSc1d2	těžší
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
jeho	jeho	k3xOp3gFnSc4	jeho
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
hospodárné	hospodárný	k2eAgNnSc4d1	hospodárné
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
letecká	letecký	k2eAgFnSc1d1	letecká
linka	linka	k1gFnSc1	linka
mezi	mezi	k7c7	mezi
Paříží	Paříž	k1gFnPc2	Paříž
resp.	resp.	kA	resp.
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
New	New	k1gFnSc7	New
Yorkem	York	k1gInSc7	York
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
kvůli	kvůli	k7c3	kvůli
malému	malý	k2eAgInSc3d1	malý
zájmu	zájem	k1gInSc3	zájem
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
ovlivněného	ovlivněný	k2eAgNnSc2d1	ovlivněné
i	i	k9	i
leteckou	letecký	k2eAgFnSc7d1	letecká
krizí	krize	k1gFnSc7	krize
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
událostmi	událost	k1gFnPc7	událost
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
zánikem	zánik	k1gInSc7	zánik
WTC	WTC	kA	WTC
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
mnozí	mnohý	k2eAgMnPc1d1	mnohý
cestující	cestující	k1gMnPc1	cestující
dříve	dříve	k6eAd2	dříve
směřovali	směřovat	k5eAaImAgMnP	směřovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
novým	nový	k2eAgInPc3d1	nový
bezpečnostním	bezpečnostní	k2eAgInPc3d1	bezpečnostní
nedostatkům	nedostatek	k1gInPc3	nedostatek
<g/>
,	,	kIx,	,
vyhlásily	vyhlásit	k5eAaPmAgFnP	vyhlásit
společnosti	společnost	k1gFnPc1	společnost
Air	Air	k1gMnPc2	Air
France	Franc	k1gMnSc2	Franc
a	a	k8xC	a
British	British	k1gInSc4	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
budou	být	k5eAaImBp3nP	být
letecké	letecký	k2eAgFnPc1d1	letecká
linky	linka	k1gFnPc1	linka
Concordu	Concord	k1gInSc2	Concord
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
let	let	k1gInSc4	let
Concordu	Concord	k1gInSc2	Concord
společnosti	společnost	k1gFnSc2	společnost
Air-France	Air-France	k1gFnSc2	Air-France
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
British	British	k1gInSc1	British
Airways	Airways	k1gInSc1	Airways
ukončil	ukončit	k5eAaPmAgInS	ukončit
lety	let	k1gInPc4	let
Concordu	Concord	k1gInSc2	Concord
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
poslední	poslední	k2eAgInSc4d1	poslední
let	let	k1gInSc4	let
Concordu	Concord	k1gInSc2	Concord
s	s	k7c7	s
imatrikulací	imatrikulace	k1gFnSc7	imatrikulace
G-BOAF	G-BOAF	k1gFnPc2	G-BOAF
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
šéfpilota	šéfpilot	k1gMnSc2	šéfpilot
Mike	Mik	k1gMnSc2	Mik
Bannistera	Bannister	k1gMnSc2	Bannister
z	z	k7c2	z
londýnského	londýnský	k2eAgMnSc2d1	londýnský
Heathrow	Heathrow	k1gMnSc2	Heathrow
do	do	k7c2	do
Filtonu	Filton	k1gInSc2	Filton
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
spotřeba	spotřeba	k1gFnSc1	spotřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
se	se	k3xPyFc4	se
Concordu	Concord	k1gMnSc3	Concord
podařil	podařit	k5eAaPmAgInS	podařit
nejrychlejší	rychlý	k2eAgInSc1d3	nejrychlejší
let	let	k1gInSc1	let
okolo	okolo	k7c2	okolo
světa	svět	k1gInSc2	svět
za	za	k7c4	za
31	[number]	k4	31
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
27	[number]	k4	27
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
49	[number]	k4	49
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Měřil	měřit	k5eAaImAgMnS	měřit
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
celkový	celkový	k2eAgInSc1d1	celkový
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
od	od	k7c2	od
startu	start	k1gInSc2	start
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
až	až	k9	až
po	po	k7c4	po
opětovné	opětovný	k2eAgNnSc4d1	opětovné
přistání	přistání	k1gNnSc4	přistání
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgNnPc2	všecek
mezipřistání	mezipřistání	k1gNnPc2	mezipřistání
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1996	[number]	k4	1996
Concorde	Concord	k1gInSc5	Concord
British	British	k1gMnSc1	British
Airways	Airways	k1gInSc4	Airways
přeletěl	přeletět	k5eAaPmAgMnS	přeletět
trasu	trasa	k1gFnSc4	trasa
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
–	–	k?	–
Londýn	Londýn	k1gInSc1	Londýn
za	za	k7c4	za
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
52	[number]	k4	52
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
59	[number]	k4	59
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
rekord	rekord	k1gInSc4	rekord
pro	pro	k7c4	pro
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
přelet	přelet	k1gInSc4	přelet
přes	přes	k7c4	přes
Atlantik	Atlantik	k1gInSc4	Atlantik
v	v	k7c6	v
civilním	civilní	k2eAgNnSc6d1	civilní
letadle	letadlo	k1gNnSc6	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
letecký	letecký	k2eAgInSc4d1	letecký
průmysl	průmysl	k1gInSc4	průmysl
==	==	k?	==
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
Concordu	Concordo	k1gNnSc3	Concordo
byl	být	k5eAaImAgInS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
výlučně	výlučně	k6eAd1	výlučně
vysokými	vysoký	k2eAgFnPc7d1	vysoká
státními	státní	k2eAgFnPc7d1	státní
podporami	podpora	k1gFnPc7	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
provozu	provoz	k1gInSc6	provoz
Concorde	Concord	k1gInSc5	Concord
přinášel	přinášet	k5eAaImAgInS	přinášet
zisk	zisk	k1gInSc4	zisk
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rekordních	rekordní	k2eAgInPc2d1	rekordní
výkonů	výkon	k1gInPc2	výkon
v	v	k7c6	v
nadzvukovém	nadzvukový	k2eAgNnSc6d1	nadzvukové
létání	létání	k1gNnSc6	létání
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
dosaženým	dosažený	k2eAgInSc7d1	dosažený
přínosem	přínos	k1gInSc7	přínos
technologický	technologický	k2eAgInSc4d1	technologický
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
leteckém	letecký	k2eAgNnSc6d1	letecké
průmyslu	průmysl	k1gInSc6	průmysl
–	–	k?	–
z	z	k7c2	z
vývoje	vývoj	k1gInSc2	vývoj
Concordu	Concord	k1gInSc2	Concord
a	a	k8xC	a
Tu-	Tu-	k1gFnSc4	Tu-
<g/>
144	[number]	k4	144
těží	těžet	k5eAaImIp3nP	těžet
výrobci	výrobce	k1gMnPc1	výrobce
letadel	letadlo	k1gNnPc2	letadlo
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
Concorde	Concord	k1gInSc5	Concord
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
nikdy	nikdy	k6eAd1	nikdy
ziskový	ziskový	k2eAgInSc1d1	ziskový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vybavení	vybavení	k1gNnSc1	vybavení
paluby	paluba	k1gFnSc2	paluba
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
kapacita	kapacita	k1gFnSc1	kapacita
letadla	letadlo	k1gNnSc2	letadlo
činila	činit	k5eAaImAgFnS	činit
144	[number]	k4	144
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Sedadla	sedadlo	k1gNnPc1	sedadlo
mohla	moct	k5eAaImAgNnP	moct
být	být	k5eAaImF	být
uspořádána	uspořádán	k2eAgNnPc1d1	uspořádáno
různě	různě	k6eAd1	různě
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
řadách	řada	k1gFnPc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Paluba	paluba	k1gFnSc1	paluba
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
toaletami	toaleta	k1gFnPc7	toaleta
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
kuchyněmi	kuchyně	k1gFnPc7	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
přední	přední	k2eAgFnSc7d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc7d1	zadní
částí	část	k1gFnSc7	část
paluby	paluba	k1gFnSc2	paluba
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
zavazadla	zavazadlo	k1gNnPc4	zavazadlo
<g/>
.	.	kIx.	.
</s>
<s>
Příruční	příruční	k2eAgNnPc1d1	příruční
zavazadla	zavazadlo	k1gNnPc1	zavazadlo
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
povolena	povolit	k5eAaPmNgFnS	povolit
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
British	British	k1gMnSc1	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
provozoval	provozovat	k5eAaImAgMnS	provozovat
letoun	letoun	k1gInSc4	letoun
se	s	k7c7	s
čtyřicetimístnou	čtyřicetimístný	k2eAgFnSc7d1	čtyřicetimístný
přední	přední	k2eAgFnSc7d1	přední
a	a	k8xC	a
padesátimístnou	padesátimístný	k2eAgFnSc7d1	padesátimístný
zadní	zadní	k2eAgFnSc7d1	zadní
kabinou	kabina	k1gFnSc7	kabina
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
obsluhovala	obsluhovat	k5eAaImAgFnS	obsluhovat
šestičlenná	šestičlenný	k2eAgFnSc1d1	šestičlenná
posádka	posádka	k1gFnSc1	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
existovala	existovat	k5eAaImAgFnS	existovat
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
třída	třída	k1gFnSc1	třída
–	–	k?	–
první	první	k4xOgFnSc6	první
<g/>
,	,	kIx,	,
letenky	letenka	k1gFnPc1	letenka
byly	být	k5eAaImAgFnP	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
dražší	drahý	k2eAgMnSc1d2	dražší
než	než	k8xS	než
letenky	letenka	k1gFnPc1	letenka
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
v	v	k7c6	v
podzvukových	podzvukový	k2eAgNnPc6d1	podzvukové
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
stísněný	stísněný	k2eAgInSc1d1	stísněný
prostor	prostor	k1gInSc1	prostor
na	na	k7c6	na
sezení	sezení	k1gNnSc6	sezení
byl	být	k5eAaImAgInS	být
vykompenzován	vykompenzován	k2eAgInSc1d1	vykompenzován
koženými	kožený	k2eAgNnPc7d1	kožené
sedadly	sedadlo	k1gNnPc7	sedadlo
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgFnSc7d1	vynikající
kuchyní	kuchyně	k1gFnSc7	kuchyně
<g/>
,	,	kIx,	,
podávanou	podávaný	k2eAgFnSc4d1	podávaná
v	v	k7c6	v
porcelánu	porcelán	k1gInSc6	porcelán
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
se	s	k7c7	s
šampaňským	šampaňské	k1gNnSc7	šampaňské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Letová	letový	k2eAgFnSc1d1	letová
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
letová	letový	k2eAgFnSc1d1	letová
výška	výška	k1gFnSc1	výška
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
letu	let	k1gInSc6	let
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
letoun	letoun	k1gInSc1	letoun
vyletěl	vyletět	k5eAaPmAgInS	vyletět
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
8	[number]	k4	8
400	[number]	k4	400
m	m	kA	m
podzvukovou	podzvukový	k2eAgFnSc7d1	podzvuková
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Bristolu	Bristol	k1gInSc2	Bristol
zapnul	zapnout	k5eAaPmAgMnS	zapnout
přídavné	přídavný	k2eAgNnSc4d1	přídavné
spalování	spalování	k1gNnSc4	spalování
–	–	k?	–
forsáž	forsáž	k1gFnSc1	forsáž
–	–	k?	–
a	a	k8xC	a
stoupal	stoupat	k5eAaImAgInS	stoupat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
na	na	k7c4	na
nadzvukovou	nadzvukový	k2eAgFnSc4d1	nadzvuková
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
přibližně	přibližně	k6eAd1	přibližně
nad	nad	k7c7	nad
ostrovem	ostrov	k1gInSc7	ostrov
Lundy	Lunda	k1gMnSc2	Lunda
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvukové	zvukový	k2eAgFnPc4d1	zvuková
vlny	vlna	k1gFnPc4	vlna
a	a	k8xC	a
zvukový	zvukový	k2eAgInSc4d1	zvukový
náraz	náraz	k1gInSc4	náraz
(	(	kIx(	(
<g/>
aerodynamický	aerodynamický	k2eAgInSc4d1	aerodynamický
třesk	třesk	k1gInSc4	třesk
<g/>
)	)	kIx)	)
při	při	k7c6	při
překonávání	překonávání	k1gNnSc6	překonávání
nadzvukové	nadzvukový	k2eAgFnSc2d1	nadzvuková
rychlosti	rychlost	k1gFnSc2	rychlost
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Letoun	letoun	k1gMnSc1	letoun
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
dál	daleko	k6eAd2	daleko
na	na	k7c6	na
Mach	macha	k1gFnPc2	macha
1,7	[number]	k4	1,7
<g/>
,	,	kIx,	,
vypnul	vypnout	k5eAaPmAgInS	vypnout
přídavné	přídavný	k2eAgNnSc4d1	přídavné
spalování	spalování	k1gNnSc4	spalování
a	a	k8xC	a
při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
výšky	výška	k1gFnSc2	výška
15	[number]	k4	15
000	[number]	k4	000
m	m	kA	m
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
rychlosti	rychlost	k1gFnSc3	rychlost
Mach	Mach	k1gMnSc1	Mach
2	[number]	k4	2
<g/>
,	,	kIx,	,
během	během	k7c2	během
letu	let	k1gInSc2	let
stoupal	stoupat	k5eAaImAgInS	stoupat
dál	daleko	k6eAd2	daleko
pozvolna	pozvolna	k6eAd1	pozvolna
až	až	k9	až
na	na	k7c4	na
17	[number]	k4	17
700	[number]	k4	700
m.	m.	k?	m.
To	ten	k3xDgNnSc1	ten
trvalo	trvat	k5eAaImAgNnS	trvat
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
klesání	klesání	k1gNnSc2	klesání
při	při	k7c6	při
přistání	přistání	k1gNnSc6	přistání
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgNnPc1d1	klasické
proudová	proudový	k2eAgNnPc1d1	proudové
letadla	letadlo	k1gNnPc1	letadlo
létala	létat	k5eAaImAgNnP	létat
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
12	[number]	k4	12
000	[number]	k4	000
m	m	kA	m
až	až	k9	až
13	[number]	k4	13
500	[number]	k4	500
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Kabina	kabina	k1gFnSc1	kabina
pilota	pilota	k1gFnSc1	pilota
a	a	k8xC	a
výhled	výhled	k1gInSc1	výhled
z	z	k7c2	z
kabiny	kabina	k1gFnSc2	kabina
===	===	k?	===
</s>
</p>
<p>
<s>
Špičku	špička	k1gFnSc4	špička
Concordu	Concord	k1gInSc2	Concord
tvoří	tvořit	k5eAaImIp3nS	tvořit
hydraulicky	hydraulicky	k6eAd1	hydraulicky
sklápěný	sklápěný	k2eAgInSc1d1	sklápěný
zobák	zobák	k1gInSc1	zobák
se	s	k7c7	s
sklopným	sklopný	k2eAgNnSc7d1	sklopné
bočním	boční	k2eAgNnSc7d1	boční
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rychlostech	rychlost	k1gFnPc6	rychlost
nad	nad	k7c7	nad
460	[number]	k4	460
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
byl	být	k5eAaImAgInS	být
zobák	zobák	k1gInSc1	zobák
a	a	k8xC	a
ochranný	ochranný	k2eAgInSc1d1	ochranný
štít	štít	k1gInSc1	štít
úplně	úplně	k6eAd1	úplně
vytažen	vytáhnout	k5eAaPmNgInS	vytáhnout
kvůli	kvůli	k7c3	kvůli
aerodynamickému	aerodynamický	k2eAgInSc3d1	aerodynamický
odporu	odpor	k1gInSc3	odpor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
poloze	poloha	k1gFnSc6	poloha
měl	mít	k5eAaImAgMnS	mít
pilot	pilot	k1gMnSc1	pilot
během	během	k7c2	během
letu	let	k1gInSc2	let
omezený	omezený	k2eAgInSc4d1	omezený
přední	přední	k2eAgInSc4d1	přední
výhled	výhled	k1gInSc4	výhled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
pod	pod	k7c7	pod
3	[number]	k4	3
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
při	při	k7c6	při
rychlosti	rychlost	k1gFnSc6	rychlost
okolo	okolo	k7c2	okolo
460	[number]	k4	460
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
byl	být	k5eAaImAgInS	být
zobák	zobák	k1gInSc1	zobák
sklopen	sklopit	k5eAaPmNgInS	sklopit
o	o	k7c4	o
5	[number]	k4	5
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zabezpečovalo	zabezpečovat	k5eAaImAgNnS	zabezpečovat
dobrý	dobrý	k2eAgInSc4d1	dobrý
přední	přední	k2eAgInSc4d1	přední
výhled	výhled	k1gInSc4	výhled
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přistávání	přistávání	k1gNnSc6	přistávání
byl	být	k5eAaImAgInS	být
zobák	zobák	k1gInSc1	zobák
sklopen	sklopit	k5eAaPmNgInS	sklopit
o	o	k7c4	o
12	[number]	k4	12
<g/>
°	°	k?	°
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgMnS	umožnit
tak	tak	k6eAd1	tak
pilotovi	pilot	k1gMnSc3	pilot
optimální	optimální	k2eAgInSc4d1	optimální
výhled	výhled	k1gInSc4	výhled
při	při	k7c6	při
vysunutém	vysunutý	k2eAgInSc6d1	vysunutý
podvozku	podvozek	k1gInSc6	podvozek
<g/>
.	.	kIx.	.
</s>
<s>
Kabina	kabina	k1gFnSc1	kabina
pilota	pilota	k1gFnSc1	pilota
v	v	k7c6	v
Concordu	Concordo	k1gNnSc6	Concordo
leží	ležet	k5eAaImIp3nS	ležet
11,4	[number]	k4	11,4
m	m	kA	m
před	před	k7c7	před
předním	přední	k2eAgInSc7d1	přední
podvozkem	podvozek	k1gInSc7	podvozek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
odbočení	odbočení	k1gNnSc6	odbočení
na	na	k7c6	na
VPD	VPD	kA	VPD
se	se	k3xPyFc4	se
pilotova	pilotův	k2eAgFnSc1d1	pilotova
kabina	kabina	k1gFnSc1	kabina
nacházela	nacházet	k5eAaImAgFnS	nacházet
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
hraničním	hraniční	k2eAgInSc7d1	hraniční
trávníkem	trávník	k1gInSc7	trávník
<g/>
.	.	kIx.	.
</s>
<s>
Pilot	pilot	k1gMnSc1	pilot
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
pilot	pilot	k1gInSc4	pilot
seděli	sedět	k5eAaImAgMnP	sedět
v	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
letový	letový	k2eAgMnSc1d1	letový
inženýr	inženýr	k1gMnSc1	inženýr
seděl	sedět	k5eAaImAgMnS	sedět
na	na	k7c6	na
otočné	otočný	k2eAgFnSc6d1	otočná
židli	židle	k1gFnSc6	židle
za	za	k7c7	za
druhým	druhý	k4xOgInSc7	druhý
pilotem	pilot	k1gInSc7	pilot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chronologie	chronologie	k1gFnSc2	chronologie
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
Paříž	Paříž	k1gFnSc1	Paříž
–	–	k?	–
Dakar	Dakar	k1gInSc1	Dakar
–	–	k?	–
Rio	Rio	k1gFnSc2	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
a	a	k8xC	a
Londýn	Londýn	k1gInSc1	Londýn
<g/>
–	–	k?	–
<g/>
Bahrajn	Bahrajn	k1gInSc1	Bahrajn
</s>
</p>
<p>
<s>
==	==	k?	==
Návštěvy	návštěva	k1gFnPc1	návštěva
Concorde	Concord	k1gInSc5	Concord
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
Concorde	Concord	k1gInSc5	Concord
přistála	přistát	k5eAaImAgFnS	přistát
několikrát	několikrát	k6eAd1	několikrát
i	i	k9	i
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
letišti	letiště	k1gNnSc6	letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyrobená	vyrobený	k2eAgNnPc1d1	vyrobené
letadla	letadlo	k1gNnPc1	letadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Uvedeny	uvést	k5eAaPmNgFnP	uvést
pouze	pouze	k6eAd1	pouze
komerčně	komerčně	k6eAd1	komerčně
nasazované	nasazovaný	k2eAgInPc4d1	nasazovaný
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Specifikace	specifikace	k1gFnSc1	specifikace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
===	===	k?	===
</s>
</p>
<p>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
3	[number]	k4	3
(	(	kIx(	(
<g/>
dva	dva	k4xCgMnPc1	dva
piloti	pilot	k1gMnPc1	pilot
a	a	k8xC	a
palubní	palubní	k2eAgMnSc1d1	palubní
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
<g/>
:	:	kIx,	:
92	[number]	k4	92
<g/>
–	–	k?	–
<g/>
120	[number]	k4	120
cestujících	cestující	k1gMnPc2	cestující
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
61,66	[number]	k4	61,66
m	m	kA	m
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
<g/>
:	:	kIx,	:
25,6	[number]	k4	25,6
m	m	kA	m
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
12,2	[number]	k4	12,2
m	m	kA	m
</s>
</p>
<p>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
plocha	plocha	k1gFnSc1	plocha
<g/>
:	:	kIx,	:
358,28	[number]	k4	358,28
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
78	[number]	k4	78
700	[number]	k4	700
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Užitečné	užitečný	k2eAgNnSc1d1	užitečné
zatížení	zatížení	k1gNnSc1	zatížení
<g/>
:	:	kIx,	:
111	[number]	k4	111
130	[number]	k4	130
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
hmotnost	hmotnost	k1gFnSc1	hmotnost
při	při	k7c6	při
pojíždění	pojíždění	k1gNnSc6	pojíždění
<g/>
:	:	kIx,	:
187	[number]	k4	187
000	[number]	k4	000
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
185	[number]	k4	185
070	[number]	k4	070
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
4	[number]	k4	4
<g/>
×	×	k?	×
proudový	proudový	k2eAgInSc1d1	proudový
motor	motor	k1gInSc1	motor
Rolls-Royce	Rolls-Royce	k1gFnSc1	Rolls-Royce
<g/>
/	/	kIx~	/
<g/>
Snecma	Snecma	k1gFnSc1	Snecma
Olympus	Olympus	k1gInSc1	Olympus
593	[number]	k4	593
s	s	k7c7	s
přídavným	přídavný	k2eAgNnSc7d1	přídavné
spalováním	spalování	k1gNnSc7	spalování
každý	každý	k3xTgMnSc1	každý
o	o	k7c6	o
tahu	tah	k1gInSc6	tah
169	[number]	k4	169
kN	kN	k?	kN
(	(	kIx(	(
<g/>
140	[number]	k4	140
kN	kN	k?	kN
bez	bez	k7c2	bez
příd	příd	k?	příd
<g/>
.	.	kIx.	.
spalování	spalování	k1gNnSc1	spalování
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
2,04	[number]	k4	2,04
M	M	kA	M
(	(	kIx(	(
<g/>
≈	≈	k?	≈
<g/>
1,354	[number]	k4	1,354
mph	mph	k?	mph
<g/>
,	,	kIx,	,
2	[number]	k4	2
179	[number]	k4	179
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
1	[number]	k4	1
176	[number]	k4	176
uzlů	uzel	k1gInPc2	uzel
<g/>
)	)	kIx)	)
v	v	k7c6	v
cestovní	cestovní	k2eAgFnSc6d1	cestovní
výšce	výška	k1gFnSc6	výška
</s>
</p>
<p>
<s>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
2,02	[number]	k4	2,02
M	M	kA	M
(	(	kIx(	(
<g/>
≈	≈	k?	≈
<g/>
1,340	[number]	k4	1,340
mph	mph	k?	mph
<g/>
,	,	kIx,	,
2	[number]	k4	2
158	[number]	k4	158
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
1	[number]	k4	1
164	[number]	k4	164
uzlů	uzel	k1gInPc2	uzel
<g/>
)	)	kIx)	)
v	v	k7c6	v
cestovní	cestovní	k2eAgFnSc6d1	cestovní
výšce	výška	k1gFnSc6	výška
</s>
</p>
<p>
<s>
Dolet	dolet	k1gInSc1	dolet
<g/>
:	:	kIx,	:
7	[number]	k4	7
222,8	[number]	k4	222,8
km	km	kA	km
</s>
</p>
<p>
<s>
Dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
18	[number]	k4	18
300	[number]	k4	300
m	m	kA	m
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
stoupavost	stoupavost	k1gFnSc1	stoupavost
<g/>
:	:	kIx,	:
50,8	[number]	k4	50,8
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Tah	tah	k1gInSc1	tah
<g/>
/	/	kIx~	/
<g/>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
0,373	[number]	k4	0,373
</s>
</p>
<p>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
paliva	palivo	k1gNnSc2	palivo
<g/>
:	:	kIx,	:
46,85	[number]	k4	46,85
lb	lb	k?	lb
<g/>
/	/	kIx~	/
<g/>
mi	já	k3xPp1nSc3	já
(	(	kIx(	(
<g/>
13,2	[number]	k4	13,2
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
km	km	kA	km
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
maximální	maximální	k2eAgInSc4d1	maximální
dolet	dolet	k1gInSc4	dolet
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
teplota	teplota	k1gFnSc1	teplota
špičky	špička	k1gFnSc2	špička
nosu	nos	k1gInSc2	nos
<g/>
:	:	kIx,	:
130	[number]	k4	130
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
NICCOLI	NICCOLI	kA	NICCOLI
<g/>
,	,	kIx,	,
Riccardo	Riccardo	k1gNnSc1	Riccardo
<g/>
.	.	kIx.	.
</s>
<s>
Letadla	letadlo	k1gNnPc1	letadlo
Nejvýznamnější	významný	k2eAgFnSc2d3	nejvýznamnější
současné	současný	k2eAgFnSc2d1	současná
i	i	k8xC	i
historické	historický	k2eAgFnSc2d1	historická
typy	typa	k1gFnSc2	typa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
651	[number]	k4	651
<g/>
-	-	kIx~	-
<g/>
x.	x.	k?	x.
</s>
</p>
<p>
<s>
CVRKAL	cvrkat	k5eAaImAgMnS	cvrkat
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Adieu	adieu	k0	adieu
<g/>
,	,	kIx,	,
Farewell	Farewell	k1gMnSc1	Farewell
<g/>
,	,	kIx,	,
Concorde	Concord	k1gMnSc5	Concord
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Letectví	letectví	k1gNnSc1	letectví
a	a	k8xC	a
kosmonautika	kosmonautika	k1gFnSc1	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Květen	květen	k1gInSc1	květen
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
79	[number]	k4	79
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
s.	s.	k?	s.
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
1156	[number]	k4	1156
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Tupolev	Tupolet	k5eAaPmDgInS	Tupolet
Tu-	Tu-	k1gFnPc2	Tu-
<g/>
144	[number]	k4	144
</s>
</p>
<p>
<s>
Boeing	boeing	k1gInSc1	boeing
2707	[number]	k4	2707
</s>
</p>
<p>
<s>
Lockheed	Lockheed	k1gMnSc1	Lockheed
L-2000	L-2000	k1gMnSc1	L-2000
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Concorde	Concord	k1gInSc5	Concord
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Reportáž	reportáž	k1gFnSc1	reportáž
"	"	kIx"	"
<g/>
Den	den	k1gInSc1	den
havárie	havárie	k1gFnSc2	havárie
Concordu	Concord	k1gInSc2	Concord
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc4	červenec
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Slavné	slavný	k2eAgInPc4d1	slavný
dny	den	k1gInPc4	den
televize	televize	k1gFnSc2	televize
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
British	British	k1gInSc1	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
Concorde	Concord	k1gInSc5	Concord
page	pagat	k5eAaPmIp3nS	pagat
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Design	design	k1gInSc1	design
Museum	museum	k1gNnSc1	museum
(	(	kIx(	(
<g/>
UK	UK	kA	UK
<g/>
)	)	kIx)	)
Concorde	Concord	k1gMnSc5	Concord
page	pagus	k1gMnSc5	pagus
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
www.concordesst.com	www.concordesst.com	k1gInSc1	www.concordesst.com
Podrobné	podrobný	k2eAgFnSc2d1	podrobná
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
Concorde	Concord	k1gInSc5	Concord
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
článek	článek	k1gInSc1	článek
o	o	k7c4	o
Concorde	Concord	k1gInSc5	Concord
</s>
</p>
