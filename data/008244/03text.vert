<p>
<s>
Pavučinec	pavučinec	k1gInSc1	pavučinec
různý	různý	k2eAgInSc1d1	různý
(	(	kIx(	(
<g/>
Cortinarius	Cortinarius	k1gInSc1	Cortinarius
varius	varius	k1gInSc1	varius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedlá	jedlý	k2eAgFnSc1d1	jedlá
houba	houba	k1gFnSc1	houba
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
v	v	k7c6	v
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
lesích	les	k1gInPc6	les
(	(	kIx(	(
<g/>
borovice	borovice	k1gFnPc1	borovice
<g/>
,	,	kIx,	,
smrk	smrk	k1gInSc1	smrk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
vápnité	vápnitý	k2eAgFnSc6d1	vápnitá
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
cm	cm	kA	cm
široký	široký	k2eAgMnSc1d1	široký
<g/>
,	,	kIx,	,
polokulovitě	polokulovitě	k6eAd1	polokulovitě
klenutý	klenutý	k2eAgMnSc1d1	klenutý
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
rozložený	rozložený	k2eAgMnSc1d1	rozložený
až	až	k8xS	až
plochý	plochý	k2eAgMnSc1d1	plochý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k8xC	i
vmáčklý	vmáčklý	k2eAgInSc4d1	vmáčklý
s	s	k7c7	s
tenkým	tenký	k2eAgInSc7d1	tenký
okrajem	okraj	k1gInSc7	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Zbarven	zbarven	k2eAgMnSc1d1	zbarven
je	být	k5eAaImIp3nS	být
žlutookrově	žlutookrově	k6eAd1	žlutookrově
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
žloutkově	žloutkově	k6eAd1	žloutkově
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
někdy	někdy	k6eAd1	někdy
červenohnědý	červenohnědý	k2eAgInSc1d1	červenohnědý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
zlatohnědý	zlatohnědý	k2eAgInSc4d1	zlatohnědý
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
světlejší	světlý	k2eAgNnSc1d2	světlejší
<g/>
,	,	kIx,	,
slizký	slizký	k2eAgInSc4d1	slizký
<g/>
,	,	kIx,	,
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
,	,	kIx,	,
lesklý	lesklý	k2eAgInSc4d1	lesklý
<g/>
,	,	kIx,	,
za	za	k7c2	za
sucha	sucho	k1gNnSc2	sucho
mírně	mírně	k6eAd1	mírně
matný	matný	k2eAgInSc1d1	matný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lupeny	lupen	k1gInPc1	lupen
jsou	být	k5eAaImIp3nP	být
husté	hustý	k2eAgInPc1d1	hustý
<g/>
,	,	kIx,	,
u	u	k7c2	u
třeně	třeň	k1gInSc2	třeň
většinou	většinou	k6eAd1	většinou
mělce	mělce	k6eAd1	mělce
vykrojené	vykrojený	k2eAgInPc1d1	vykrojený
do	do	k7c2	do
zubu	zub	k1gInSc2	zub
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgInPc1d1	tenký
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
sytě	sytě	k6eAd1	sytě
makově	makově	k6eAd1	makově
modré	modrý	k2eAgNnSc1d1	modré
<g/>
,	,	kIx,	,
do	do	k7c2	do
fialova	fialovo	k1gNnSc2	fialovo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
okrově	okrově	k6eAd1	okrově
skořicové	skořicový	k2eAgFnPc1d1	skořicová
<g/>
,	,	kIx,	,
s	s	k7c7	s
ostřím	ostří	k1gNnSc7	ostří
mělce	mělce	k6eAd1	mělce
vroubkovaným	vroubkovaný	k2eAgNnSc7d1	vroubkované
<g/>
.	.	kIx.	.
</s>
<s>
Výtrusy	výtrus	k1gInPc1	výtrus
jsou	být	k5eAaImIp3nP	být
bledě	bledě	k6eAd1	bledě
rezavě	rezavě	k6eAd1	rezavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
a	a	k8xC	a
elipsovitě	elipsovitě	k6eAd1	elipsovitě
mandlovité	mandlovitý	k2eAgNnSc1d1	mandlovité
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třeň	třeň	k1gInSc1	třeň
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc1d1	plný
<g/>
,	,	kIx,	,
k	k	k7c3	k
dolejšku	dolejšek	k1gInSc3	dolejšek
kyjovitě	kyjovitě	k6eAd1	kyjovitě
ztloustlý	ztloustlý	k2eAgInSc1d1	ztloustlý
<g/>
,	,	kIx,	,
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
obyčejně	obyčejně	k6eAd1	obyčejně
dosti	dosti	k6eAd1	dosti
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
protažený	protažený	k2eAgMnSc1d1	protažený
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vločkatě	vločkatě	k6eAd1	vločkatě
vláknitý	vláknitý	k2eAgInSc1d1	vláknitý
až	až	k9	až
skoro	skoro	k6eAd1	skoro
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
,	,	kIx,	,
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
trochu	trochu	k6eAd1	trochu
namodralý	namodralý	k2eAgMnSc1d1	namodralý
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
odstín	odstín	k1gInSc1	odstín
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolejší	dolejší	k2eAgFnSc6d1	dolejší
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
žlutavě	žlutavě	k6eAd1	žlutavě
krémový	krémový	k2eAgInSc1d1	krémový
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
celý	celý	k2eAgInSc4d1	celý
žlutookrový	žlutookrový	k2eAgInSc4d1	žlutookrový
<g/>
.	.	kIx.	.
</s>
<s>
Třeň	třeň	k1gInSc1	třeň
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
dutý	dutý	k2eAgMnSc1d1	dutý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dužnina	dužnina	k1gFnSc1	dužnina
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
a	a	k8xC	a
kompaktně	kompaktně	k6eAd1	kompaktně
masitá	masitý	k2eAgFnSc1d1	masitá
<g/>
,	,	kIx,	,
v	v	k7c6	v
klobouku	klobouk	k1gInSc6	klobouk
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
slabým	slabý	k2eAgInSc7d1	slabý
žlutým	žlutý	k2eAgInSc7d1	žlutý
nádechem	nádech	k1gInSc7	nádech
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třeni	třeň	k1gInSc6	třeň
vlnkatě	vlnkatě	k6eAd1	vlnkatě
vláknitá	vláknitý	k2eAgFnSc1d1	vláknitá
se	se	k3xPyFc4	se
slabým	slabý	k2eAgInSc7d1	slabý
žlutavým	žlutavý	k2eAgInSc7d1	žlutavý
nádechem	nádech	k1gInSc7	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Slabě	slabě	k6eAd1	slabě
voní	vonět	k5eAaImIp3nP	vonět
a	a	k8xC	a
příjemně	příjemně	k6eAd1	příjemně
chutná	chutnat	k5eAaImIp3nS	chutnat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
lesích	les	k1gInPc6	les
na	na	k7c6	na
vápencové	vápencový	k2eAgFnSc6d1	vápencová
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
lesů	les	k1gInPc2	les
od	od	k7c2	od
konce	konec	k1gInSc2	konec
léta	léto	k1gNnSc2	léto
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Upotřebení	upotřebení	k1gNnSc2	upotřebení
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedlou	jedlý	k2eAgFnSc4d1	jedlá
a	a	k8xC	a
chutnou	chutný	k2eAgFnSc4d1	chutná
houbu	houba	k1gFnSc4	houba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
požívat	požívat	k5eAaImF	požívat
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
úpravě	úprava	k1gFnSc6	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GARIBOVOVÁ	GARIBOVOVÁ	kA	GARIBOVOVÁ
<g/>
,	,	kIx,	,
L.V.	L.V.	k1gFnSc1	L.V.
<g/>
,	,	kIx,	,
M.	M.	kA	M.
SVRČEK	SVRČEK	kA	SVRČEK
a	a	k8xC	a
J.	J.	kA	J.
BAIER	Baier	k1gMnSc1	Baier
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houby	k6eAd1	houby
<g/>
:	:	kIx,	:
poznáváme	poznávat	k5eAaImIp1nP	poznávat
sbíráme	sbírat	k5eAaImIp1nP	sbírat
upravujeme	upravovat	k5eAaImIp1nP	upravovat
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Lidové	lidový	k2eAgNnSc1d1	lidové
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SVRČEK	SVRČEK	kA	SVRČEK
<g/>
,	,	kIx,	,
M.	M.	kA	M.
a	a	k8xC	a
B.	B.	kA	B.
VANČURA	Vančura	k1gMnSc1	Vančura
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houba	k1gFnPc1	houba
<g/>
.	.	kIx.	.
3912	[number]	k4	3912
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
ARTIA	ARTIA	kA	ARTIA
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PILÁT	Pilát	k1gMnSc1	Pilát
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnPc1	náš
houby	houba	k1gFnPc1	houba
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Kritické	kritický	k2eAgInPc1d1	kritický
druhy	druh	k1gInPc1	druh
našich	náš	k3xOp1gFnPc2	náš
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
775	[number]	k4	775
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Československé	československý	k2eAgFnSc2d1	Československá
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GARNWEIDNER	GARNWEIDNER	kA	GARNWEIDNER
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houba	k1gFnPc1	houba
<g/>
:	:	kIx,	:
Kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pavučinec	pavučinec	k1gInSc1	pavučinec
různý	různý	k2eAgInSc1d1	různý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Pavučinec	pavučinec	k1gInSc1	pavučinec
různý	různý	k2eAgInSc1d1	různý
na	na	k7c4	na
biolib	biolib	k1gInSc4	biolib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
