<s>
Python	Python	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
programovacím	programovací	k2eAgInSc6d1
jazyku	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Python	Python	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážel	odrážet	k5eAaImAgMnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Komentář	komentář	k1gInSc1
<g/>
:	:	kIx,
10	#num#	k4
let	léto	k1gNnPc2
nedotknutá	dotknutý	k2eNgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
kapitola	kapitola	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
verzi	verze	k1gFnSc6
3	#num#	k4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
novou	nový	k2eAgFnSc4d1
<g/>
;	;	kIx,
v	v	k7c6
příkladech	příklad	k1gInPc6
upřednostňovat	upřednostňovat	k5eAaImF
verzi	verze	k1gFnSc4
3	#num#	k4
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Python	Python	k1gMnSc1
</s>
<s>
Logo	logo	k1gNnSc1
Pythonu	Python	k1gMnSc3
Paradigma	paradigma	k1gNnSc1
</s>
<s>
multiparadigmatický	multiparadigmatický	k2eAgMnSc1d1
Vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
</s>
<s>
1991	#num#	k4
Autor	autor	k1gMnSc1
</s>
<s>
Guido	Guido	k1gNnSc1
van	vana	k1gFnPc2
Rossum	Rossum	k1gNnSc1
Vývojář	vývojář	k1gMnSc1
</s>
<s>
Python	Python	k1gMnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc4
První	první	k4xOgNnPc4
vydání	vydání	k1gNnPc4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1991	#num#	k4
Poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
2.7	2.7	k4
<g/>
.18	.18	k4
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
3.9	3.9	k4
<g/>
.0	.0	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2020	#num#	k4
<g/>
)	)	kIx)
Poslední	poslední	k2eAgFnSc1d1
nestabilní	stabilní	k2eNgFnSc1d1
verze	verze	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
Ve	v	k7c6
vývoji	vývoj	k1gInSc6
<g/>
)	)	kIx)
Typová	typový	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
</s>
<s>
silná	silný	k2eAgFnSc1d1
<g/>
,	,	kIx,
dynamická	dynamický	k2eAgFnSc1d1
<g/>
,	,	kIx,
duck-typing	duck-typing	k1gInSc1
Hlavní	hlavní	k2eAgFnSc2d1
implementace	implementace	k1gFnSc2
</s>
<s>
CPython	CPython	k1gMnSc1
<g/>
,	,	kIx,
Jython	Jython	k1gMnSc1
<g/>
,	,	kIx,
IronPython	IronPython	k1gMnSc1
<g/>
,	,	kIx,
PyPy	PyP	k2eAgInPc4d1
Dialekty	dialekt	k1gInPc4
</s>
<s>
Stackless	Stackless	k6eAd1
Python	Python	k1gMnSc1
<g/>
,	,	kIx,
RPython	RPython	k1gMnSc1
<g/>
,	,	kIx,
Cython	Cython	k1gInSc1
Ovlivněn	ovlivněn	k2eAgInSc1d1
jazyky	jazyk	k1gInPc4
</s>
<s>
ABC	ABC	kA
<g/>
,	,	kIx,
Perl	perl	k1gInSc1
<g/>
,	,	kIx,
Lisp	Lisp	k1gMnSc1
<g/>
,	,	kIx,
Smalltalk	Smalltalk	k1gMnSc1
<g/>
,	,	kIx,
Tcl	Tcl	k1gMnSc1
Ovlivnil	ovlivnit	k5eAaPmAgMnS
jazyky	jazyk	k1gInPc4
</s>
<s>
Ruby	rub	k1gInPc1
<g/>
,	,	kIx,
Boo	boa	k1gFnSc5
<g/>
,	,	kIx,
Groovy	Groov	k1gInPc7
OS	OS	kA
</s>
<s>
multiplatformní	multiplatformní	k2eAgFnSc1d1
Licence	licence	k1gFnSc1
</s>
<s>
Python	Python	k1gMnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc4
License	License	k1gFnSc2
Web	web	k1gInSc1
</s>
<s>
www.python.org	www.python.org	k1gMnSc1
</s>
<s>
Python	Python	k1gMnSc1
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
ˈ	ˈ	k1gInSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vysokoúrovňový	vysokoúrovňový	k2eAgInSc1d1
skriptovací	skriptovací	k2eAgInSc1d1
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
navrhl	navrhnout	k5eAaPmAgInS
Guido	Guido	k1gNnSc4
van	vana	k1gFnPc2
Rossum	Rossum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízí	nabízet	k5eAaImIp3nS
dynamickou	dynamický	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
datových	datový	k2eAgInPc2d1
typů	typ	k1gInPc2
a	a	k8xC
podporuje	podporovat	k5eAaImIp3nS
různá	různý	k2eAgNnPc4d1
programovací	programovací	k2eAgNnPc4d1
paradigmata	paradigma	k1gNnPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
objektově	objektově	k6eAd1
orientovaného	orientovaný	k2eAgInSc2d1
<g/>
,	,	kIx,
imperativního	imperativní	k2eAgInSc2d1
<g/>
,	,	kIx,
procedurálního	procedurální	k2eAgInSc2d1
nebo	nebo	k8xC
funkcionálního	funkcionální	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
vzrostla	vzrůst	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
popularita	popularita	k1gFnSc1
a	a	k8xC
zařadil	zařadit	k5eAaPmAgMnS
se	se	k3xPyFc4
mezi	mezi	k7c4
nejoblíbenější	oblíbený	k2eAgInPc4d3
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
řadě	řada	k1gFnSc6
různých	různý	k2eAgInPc2d1
žebříčků	žebříček	k1gInPc2
dosahuje	dosahovat	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
prvních	první	k4xOgInPc2
třech	tři	k4xCgInPc2
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
výjimkou	výjimka	k1gFnSc7
nebývají	bývat	k5eNaImIp3nP
první	první	k4xOgNnPc1
místa	místo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Python	Python	k1gMnSc1
je	být	k5eAaImIp3nS
vyvíjen	vyvíjen	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
open	open	k1gMnSc1
source	source	k1gMnSc1
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zdarma	zdarma	k6eAd1
nabízí	nabízet	k5eAaImIp3nS
instalační	instalační	k2eAgInPc4d1
balíky	balík	k1gInPc4
pro	pro	k7c4
většinu	většina	k1gFnSc4
běžných	běžný	k2eAgFnPc2d1
platforem	platforma	k1gFnPc2
(	(	kIx(
<g/>
Unix	Unix	k1gInSc1
<g/>
,	,	kIx,
MS	MS	kA
Windows	Windows	kA
<g/>
,	,	kIx,
macOS	macOS	k?
<g/>
,	,	kIx,
Android	android	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
ve	v	k7c6
většině	většina	k1gFnSc6
distribucí	distribuce	k1gFnPc2
systému	systém	k1gInSc2
GNU	gnu	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Linux	Linux	kA
je	být	k5eAaImIp3nS
Python	Python	k1gMnSc1
součástí	součást	k1gFnPc2
základní	základní	k2eAgFnSc2d1
instalace	instalace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gNnSc6
implementován	implementován	k2eAgInSc1d1
aplikační	aplikační	k2eAgInSc4d1
server	server	k1gInSc4
Zope	Zop	k1gFnSc2
<g/>
,	,	kIx,
instalátor	instalátor	k1gMnSc1
a	a	k8xC
většina	většina	k1gFnSc1
konfiguračních	konfigurační	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
Linuxové	linuxový	k2eAgFnSc2d1
distribuce	distribuce	k1gFnSc2
firmy	firma	k1gFnSc2
Red	Red	k1gFnSc2
Hat	hat	k0
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jazyk	jazyk	k1gMnSc1
Python	Python	k1gMnSc1
se	se	k3xPyFc4
vyvíjí	vyvíjet	k5eAaImIp3nS
a	a	k8xC
postupem	postup	k1gInSc7
času	čas	k1gInSc2
vznikly	vzniknout	k5eAaPmAgFnP
tři	tři	k4xCgFnPc1
nekompatibilní	kompatibilní	k2eNgFnPc1d1
major	major	k1gMnSc1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
2	#num#	k4
a	a	k8xC
Python	Python	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Python	Python	k1gMnSc1
1	#num#	k4
se	se	k3xPyFc4
už	už	k6eAd1
nepoužívá	používat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
0.9	0.9	k4
<g/>
.0	.0	k4
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
1.0	1.0	k4
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
a	a	k8xC
poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
1.6	1.6	k4
<g/>
.1	.1	k4
vyšla	vyjít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Python	Python	k1gMnSc1
2	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
útlumu	útlum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
2.0	2.0	k4
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
2.7	2.7	k4
<g/>
.18	.18	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
je	být	k5eAaImIp3nS
dle	dle	k7c2
PEP	Pepa	k1gFnPc2
373	#num#	k4
oficiálně	oficiálně	k6eAd1
nepodporován	podporovat	k5eNaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podobě	podoba	k1gFnSc6
2.7	2.7	k4
<g/>
.18	.18	k4
bude	být	k5eAaImBp3nS
druhá	druhý	k4xOgFnSc1
řada	řada	k1gFnSc1
Pythonu	Python	k1gMnSc3
zmrazena	zmrazit	k5eAaPmNgNnP
a	a	k8xC
nebude	být	k5eNaImBp3nS
se	se	k3xPyFc4
nadále	nadále	k6eAd1
vyvíjet	vyvíjet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
PEP	Pepa	k1gFnPc2
404	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nevznikne	vzniknout	k5eNaPmIp3nS
verze	verze	k1gFnSc1
2.8	2.8	k4
<g/>
.	.	kIx.
</s>
<s>
Souběh	souběh	k1gInSc1
řady	řada	k1gFnSc2
2	#num#	k4
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
2	#num#	k4
a	a	k8xC
Python	Python	k1gMnSc1
3	#num#	k4
byly	být	k5eAaImAgInP
mnoho	mnoho	k6eAd1
let	léto	k1gNnPc2
vyvíjeny	vyvíjet	k5eAaImNgInP
paralelně	paralelně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Pythonu	Python	k1gMnSc3
2	#num#	k4
byly	být	k5eAaImAgFnP
přeneseny	přenést	k5eAaPmNgFnP
některé	některý	k3yIgFnPc1
nové	nový	k2eAgFnPc1d1
vlastností	vlastnost	k1gFnSc7
z	z	k7c2
raných	raný	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
Pythonu	Python	k1gMnSc3
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snahou	snaha	k1gFnSc7
bylo	být	k5eAaImAgNnS
Python	Python	k1gMnSc1
2	#num#	k4
a	a	k8xC
Python	Python	k1gMnSc1
3	#num#	k4
k	k	k7c3
sobě	se	k3xPyFc3
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
nejvíce	hodně	k6eAd3,k6eAd1
přiblížit	přiblížit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
usnadněn	usnadnit	k5eAaPmNgInS
přechod	přechod	k1gInSc1
řady	řada	k1gFnSc2
existujících	existující	k2eAgInPc2d1
projektů	projekt	k1gInPc2
z	z	k7c2
Pythonu	Python	k1gMnSc3
2	#num#	k4
na	na	k7c4
Python	Python	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
náročný	náročný	k2eAgInSc4d1
úkol	úkol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naplánovaný	naplánovaný	k2eAgInSc1d1
termín	termín	k1gInSc1
ukončení	ukončení	k1gNnSc2
podpory	podpora	k1gFnSc2
Pythonu	Python	k1gMnSc3
2	#num#	k4
se	se	k3xPyFc4
opakovaně	opakovaně	k6eAd1
oddaloval	oddalovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
3.0	3.0	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
téměř	téměř	k6eAd1
současně	současně	k6eAd1
(	(	kIx(
<g/>
o	o	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
později	pozdě	k6eAd2
<g/>
)	)	kIx)
s	s	k7c7
verzí	verze	k1gFnSc7
2.6	2.6	k4
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Python	Python	k1gMnSc1
3	#num#	k4
je	být	k5eAaImIp3nS
aktivní	aktivní	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
vydaná	vydaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
je	být	k5eAaImIp3nS
3.9	3.9	k4
<g/>
.0	.0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řada	řada	k1gFnSc1
3	#num#	k4
opravuje	opravovat	k5eAaImIp3nS
chybná	chybný	k2eAgFnSc1d1
a	a	k8xC
překonaná	překonaný	k2eAgNnPc1d1
designová	designový	k2eAgNnPc1d1
rozhodnutí	rozhodnutí	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Python	Python	k1gMnSc1
2	#num#	k4
používá	používat	k5eAaImIp3nS
textové	textový	k2eAgInPc4d1
řetězce	řetězec	k1gInPc4
se	s	k7c7
zastaralým	zastaralý	k2eAgMnSc7d1
8	#num#	k4
<g/>
bitovým	bitový	k2eAgNnSc7d1
kódováním	kódování	k1gNnSc7
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
3	#num#	k4
přešel	přejít	k5eAaPmAgMnS
na	na	k7c6
moderní	moderní	k2eAgFnSc6d1
a	a	k8xC
univerzální	univerzální	k2eAgFnSc6d1
Unicode	Unicod	k1gInSc5
textové	textový	k2eAgInPc1d1
řetězce	řetězec	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Python	Python	k1gMnSc1
je	být	k5eAaImIp3nS
dynamický	dynamický	k2eAgInSc4d1
interpretovaný	interpretovaný	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
bývá	bývat	k5eAaImIp3nS
zařazován	zařazovat	k5eAaImNgInS
mezi	mezi	k7c4
takzvané	takzvaný	k2eAgInPc4d1
skriptovací	skriptovací	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
možnosti	možnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
ale	ale	k9
větší	veliký	k2eAgInPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
byl	být	k5eAaImAgMnS
navržen	navrhnout	k5eAaPmNgMnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
umožňoval	umožňovat	k5eAaImAgInS
tvorbu	tvorba	k1gFnSc4
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
<g/>
,	,	kIx,
plnohodnotných	plnohodnotný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
grafického	grafický	k2eAgNnSc2d1
uživatelského	uživatelský	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
–	–	k?
viz	vidět	k5eAaImRp2nS
například	například	k6eAd1
wxPython	wxPython	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
wxWidgets	wxWidgets	k6eAd1
<g/>
,	,	kIx,
nebo	nebo	k8xC
PySide	PySid	k1gInSc5
a	a	k8xC
PyQT	PyQT	k1gMnPc7
pro	pro	k7c4
Qt	Qt	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
nebo	nebo	k8xC
PyGTK	PyGTK	k1gFnSc1
pro	pro	k7c4
GTK	GTK	kA
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Python	Python	k1gInSc1
je	být	k5eAaImIp3nS
hybridní	hybridní	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
také	také	k9
multiparadigmatický	multiparadigmatický	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
umožňuje	umožňovat	k5eAaImIp3nS
při	při	k7c6
psaní	psaní	k1gNnSc6
programů	program	k1gInPc2
používat	používat	k5eAaImF
nejen	nejen	k6eAd1
objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
paradigma	paradigma	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
procedurální	procedurální	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
i	i	k9
funkcionální	funkcionální	k2eAgInSc1d1
<g/>
,	,	kIx,
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
komu	kdo	k3yInSc3,k3yRnSc3,k3yQnSc3
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
vyhovuje	vyhovovat	k5eAaImIp3nS
nebo	nebo	k8xC
se	se	k3xPyFc4
pro	pro	k7c4
danou	daný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
nejlépe	dobře	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
má	mít	k5eAaImIp3nS
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
vynikající	vynikající	k2eAgFnPc1d1
vyjadřovací	vyjadřovací	k2eAgFnPc1d1
schopnosti	schopnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kód	kód	k1gInSc1
programu	program	k1gInSc2
je	být	k5eAaImIp3nS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
krátký	krátký	k2eAgInSc4d1
a	a	k8xC
dobře	dobře	k6eAd1
čitelný	čitelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
význačným	význačný	k2eAgFnPc3d1
vlastnostem	vlastnost	k1gFnPc3
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
patří	patřit	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
jednoduchost	jednoduchost	k1gFnSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
učení	učení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
dokonce	dokonce	k9
považován	považován	k2eAgMnSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejvhodnějších	vhodný	k2eAgInPc2d3
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
pro	pro	k7c4
začátečníky	začátečník	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
je	být	k5eAaImIp3nS
dána	dát	k5eAaPmNgFnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jedním	jeden	k4xCgInSc7
z	z	k7c2
jeho	jeho	k3xOp3gInPc2
silných	silný	k2eAgInPc2d1
inspiračních	inspirační	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
byl	být	k5eAaImAgInS
programovací	programovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
ABC	ABC	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
jako	jako	k9
jazyk	jazyk	k1gInSc1
pro	pro	k7c4
výuku	výuka	k1gFnSc4
a	a	k8xC
pro	pro	k7c4
použití	použití	k1gNnSc4
začátečníky	začátečník	k1gMnPc4
přímo	přímo	k6eAd1
vytvořen	vytvořit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
ale	ale	k9
současně	současně	k6eAd1
bourá	bourat	k5eAaImIp3nS
zažitou	zažitý	k2eAgFnSc4d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
představu	představa	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
jazyk	jazyk	k1gInSc1
vhodný	vhodný	k2eAgInSc1d1
pro	pro	k7c4
výuku	výuka	k1gFnSc4
není	být	k5eNaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
pro	pro	k7c4
praxi	praxe	k1gFnSc4
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatnou	podstatný	k2eAgFnSc7d1
měrou	míra	k1gFnSc7wR
k	k	k7c3
tomu	ten	k3xDgNnSc3
přispívá	přispívat	k5eAaImIp3nS
čistota	čistota	k1gFnSc1
a	a	k8xC
jednoduchost	jednoduchost	k1gFnSc1
syntaxe	syntaxe	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
se	se	k3xPyFc4
při	při	k7c6
vývoji	vývoj	k1gInSc6
jazyka	jazyk	k1gInSc2
hodně	hodně	k6eAd1
dbá	dbát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
definici	definice	k1gFnSc3
bloků	blok	k1gInPc2
se	se	k3xPyFc4
v	v	k7c6
Pythonu	Python	k1gMnSc6
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
většiny	většina	k1gFnSc2
jazyků	jazyk	k1gInPc2
<g/>
)	)	kIx)
používá	používat	k5eAaImIp3nS
pouze	pouze	k6eAd1
odsazování	odsazování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Význačnou	význačný	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
je	být	k5eAaImIp3nS
produktivnost	produktivnost	k1gFnSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
rychlosti	rychlost	k1gFnSc2
psaní	psaní	k1gNnSc2
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týká	týkat	k5eAaImIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
jak	jak	k8xS,k8xC
nejjednodušších	jednoduchý	k2eAgInPc2d3
programů	program	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
aplikací	aplikace	k1gFnPc2
velmi	velmi	k6eAd1
rozsáhlých	rozsáhlý	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
jednoduchých	jednoduchý	k2eAgInPc2d1
programů	program	k1gInPc2
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
vlastnost	vlastnost	k1gFnSc1
projevuje	projevovat	k5eAaImIp3nS
především	především	k9
stručností	stručnost	k1gFnSc7
zápisu	zápis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
velkých	velký	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
je	být	k5eAaImIp3nS
produktivnost	produktivnost	k1gFnSc1
podpořena	podpořen	k2eAgFnSc1d1
rysy	rys	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
při	při	k7c6
programování	programování	k1gNnSc6
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
přirozená	přirozený	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
jmenných	jmenný	k2eAgInPc2d1
prostorů	prostor	k1gInPc2
<g/>
,	,	kIx,
používání	používání	k1gNnSc1
výjimek	výjimka	k1gFnPc2
<g/>
,	,	kIx,
standardně	standardně	k6eAd1
dodávané	dodávaný	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
pro	pro	k7c4
psaní	psaní	k1gNnSc4
testů	test	k1gInPc2
(	(	kIx(
<g/>
unit	unit	k2eAgInSc1d1
testing	testing	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
dalšími	další	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
produktivností	produktivnost	k1gFnSc7
souvisí	souviset	k5eAaImIp3nS
dostupnost	dostupnost	k1gFnSc1
a	a	k8xC
snadná	snadný	k2eAgFnSc1d1
použitelnost	použitelnost	k1gFnSc1
široké	široký	k2eAgFnSc2d1
škály	škála	k1gFnSc2
knihovních	knihovní	k2eAgInPc2d1
modulů	modul	k1gInPc2
<g/>
,	,	kIx,
umožňujících	umožňující	k2eAgInPc2d1
snadné	snadný	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
úloh	úloha	k1gFnPc2
z	z	k7c2
řady	řada	k1gFnSc2
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Python	Python	k1gMnSc1
se	se	k3xPyFc4
snadno	snadno	k6eAd1
vkládá	vkládat	k5eAaImIp3nS
do	do	k7c2
jiných	jiný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
(	(	kIx(
<g/>
embedding	embedding	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
pak	pak	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
jejich	jejich	k3xOp3gInSc1
skriptovací	skriptovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
lze	lze	k6eAd1
aplikacím	aplikace	k1gFnPc3
psaným	psaný	k2eAgFnPc3d1
v	v	k7c6
kompilovaných	kompilovaný	k2eAgInPc6d1
programovacích	programovací	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
dodávat	dodávat	k5eAaImF
chybějící	chybějící	k2eAgFnSc4d1
pružnost	pružnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgFnPc4d1
aplikace	aplikace	k1gFnPc4
nebo	nebo	k8xC
aplikační	aplikační	k2eAgFnPc1d1
knihovny	knihovna	k1gFnPc1
mohou	moct	k5eAaImIp3nP
naopak	naopak	k6eAd1
implementovat	implementovat	k5eAaImF
rozhraní	rozhraní	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
umožní	umožnit	k5eAaPmIp3nS
jejich	jejich	k3xOp3gNnSc4
použití	použití	k1gNnSc4
v	v	k7c6
roli	role	k1gFnSc6
pythonovského	pythonovský	k2eAgInSc2d1
modulu	modul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
pythonovský	pythonovský	k2eAgInSc1d1
program	program	k1gInSc1
je	on	k3xPp3gMnPc4
může	moct	k5eAaImIp3nS
využívat	využívat	k5eAaImF,k5eAaPmF
jako	jako	k9
modul	modul	k1gInSc1
dostupný	dostupný	k2eAgInSc1d1
přímo	přímo	k6eAd1
z	z	k7c2
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
(	(	kIx(
<g/>
tj.	tj.	kA
extending	extending	k1gInSc4
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
sekce	sekce	k1gFnSc1
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
aplikacemi	aplikace	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Programování	programování	k1gNnSc1
v	v	k7c6
Pythonu	Python	k1gMnSc6
klade	klást	k5eAaImIp3nS
velký	velký	k2eAgInSc4d1
důraz	důraz	k1gInSc4
na	na	k7c4
produktivitu	produktivita	k1gFnSc4
práce	práce	k1gFnSc2
programátora	programátor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myšlenky	myšlenka	k1gFnSc2
návrhu	návrh	k1gInSc2
jazyka	jazyk	k1gInSc2
jsou	být	k5eAaImIp3nP
shrnuty	shrnout	k5eAaPmNgFnP
ve	v	k7c4
filosofii	filosofie	k1gFnSc4
Pythonu	Python	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Nebezpečnou	bezpečný	k2eNgFnSc7d1
vlastností	vlastnost	k1gFnSc7
Pythonu	Python	k1gMnSc3
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
obsahuje	obsahovat	k5eAaImIp3nS
nedokumentované	dokumentovaný	k2eNgFnPc4d1
funkce	funkce	k1gFnPc4
a	a	k8xC
lokální	lokální	k2eAgFnPc4d1
proměnné	proměnná	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
zneužity	zneužit	k2eAgFnPc1d1
pro	pro	k7c4
spuštění	spuštění	k1gNnSc4
příkazu	příkaz	k1gInSc2
v	v	k7c6
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
Python	Python	k1gMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
provozu	provoz	k1gInSc6
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
vlastní	vlastní	k2eAgInSc4d1
repozitář	repozitář	k1gInSc4
balíčků	balíček	k1gInPc2
s	s	k7c7
knihovnami	knihovna	k1gFnPc7
<g/>
,	,	kIx,
PyPI	PyPI	k1gFnPc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
podporuje	podporovat	k5eAaImIp3nS
snadnou	snadný	k2eAgFnSc4d1
instalaci	instalace	k1gFnSc4
balíčků	balíček	k1gInPc2
programem	program	k1gInSc7
pip	pipa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Různé	různý	k2eAgFnPc1d1
implementace	implementace	k1gFnPc1
Pythonu	Python	k1gMnSc3
</s>
<s>
Standardní	standardní	k2eAgMnSc1d1
Python	Python	k1gMnSc1
je	být	k5eAaImIp3nS
implementován	implementovat	k5eAaImNgMnS
v	v	k7c6
jazyce	jazyk	k1gInSc6
C.	C.	kA
Tuto	tento	k3xDgFnSc4
implementaci	implementace	k1gFnSc4
vyvíjí	vyvíjet	k5eAaImIp3nS
Python	Python	k1gMnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc1
a	a	k8xC
tato	tento	k3xDgFnSc1
implementace	implementace	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
a	a	k8xC
definuje	definovat	k5eAaBmIp3nS
standard	standard	k1gInSc4
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
ale	ale	k9
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
implementací	implementace	k1gFnPc2
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
pro	pro	k7c4
různá	různý	k2eAgNnPc4d1
prostředí	prostředí	k1gNnPc4
nebo	nebo	k8xC
další	další	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
CPython	CPython	k1gMnSc1
</s>
<s>
Standardní	standardní	k2eAgMnSc1d1
Python	Python	k1gMnSc1
je	být	k5eAaImIp3nS
implementován	implementovat	k5eAaImNgMnS
v	v	k7c6
jazyce	jazyk	k1gInSc6
C	C	kA
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
implementace	implementace	k1gFnSc1
je	být	k5eAaImIp3nS
označována	označován	k2eAgFnSc1d1
CPython	CPython	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ní	on	k3xPp3gFnSc6
probíhá	probíhat	k5eAaImIp3nS
další	další	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
jsou	být	k5eAaImIp3nP
zveřejňovány	zveřejňován	k2eAgFnPc4d1
jak	jak	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
přeložených	přeložený	k2eAgInPc2d1
instalačních	instalační	k2eAgInPc2d1
balíků	balík	k1gInPc2
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
cílové	cílový	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dostupnost	dostupnost	k1gFnSc1
zdrojového	zdrojový	k2eAgInSc2d1
kódu	kód	k1gInSc2
a	a	k8xC
vlastnosti	vlastnost	k1gFnSc2
jazyka	jazyk	k1gInSc2
C	C	kA
umožňují	umožňovat	k5eAaImIp3nP
zabudovat	zabudovat	k5eAaPmF
interpret	interpret	k1gMnSc1
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
do	do	k7c2
jiné	jiný	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
psané	psaný	k2eAgFnSc2d1
v	v	k7c6
jazycích	jazyk	k1gInPc6
C	C	kA
nebo	nebo	k8xC
C	C	kA
<g/>
++	++	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
zabudovaný	zabudovaný	k2eAgMnSc1d1
interpret	interpret	k1gMnSc1
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
pak	pak	k6eAd1
představuje	představovat	k5eAaImIp3nS
nástroj	nástroj	k1gInSc4
pro	pro	k7c4
pružné	pružný	k2eAgNnSc4d1
rozšiřování	rozšiřování	k1gNnSc4
funkčnosti	funkčnost	k1gFnSc2
výsledné	výsledný	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
zvenčí	zvenčí	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
i	i	k9
projekt	projekt	k1gInSc1
pro	pro	k7c4
užší	úzký	k2eAgFnSc4d2
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
C	C	kA
<g/>
++	++	k?
nazvaný	nazvaný	k2eAgInSc1d1
Boost	Boost	k1gInSc1
<g/>
.	.	kIx.
<g/>
Python	Python	k1gMnSc1
</s>
<s>
Z	z	k7c2
těchto	tento	k3xDgInPc2
důvodů	důvod	k1gInPc2
–	–	k?
a	a	k8xC
s	s	k7c7
přihlédnutím	přihlédnutí	k1gNnSc7
k	k	k7c3
obecně	obecně	k6eAd1
vysokému	vysoký	k2eAgInSc3d1
výkonu	výkon	k1gInSc3
aplikací	aplikace	k1gFnPc2
psaných	psaný	k2eAgFnPc2d1
v	v	k7c6
jazyce	jazyk	k1gInSc6
C	C	kA
–	–	k?
je	být	k5eAaImIp3nS
CPython	CPython	k1gNnSc4
nejpoužívanější	používaný	k2eAgFnSc7d3
implementací	implementace	k1gFnSc7
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Jython	Jython	k1gMnSc1
</s>
<s>
Jython	Jython	k1gInSc1
je	být	k5eAaImIp3nS
implementace	implementace	k1gFnSc1
Pythonu	Python	k1gMnSc3
pro	pro	k7c4
prostředí	prostředí	k1gNnSc4
JVM	JVM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
implementován	implementovat	k5eAaImNgInS
v	v	k7c6
jazyce	jazyk	k1gInSc6
Java	Jav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kód	kód	k1gInSc1
napsaný	napsaný	k2eAgInSc1d1
v	v	k7c6
Jythonu	Jython	k1gInSc6
běží	běžet	k5eAaImIp3nS
v	v	k7c6
JVM	JVM	kA
Javy	Jav	k1gMnPc7
a	a	k8xC
může	moct	k5eAaImIp3nS
používat	používat	k5eAaImF
všechny	všechen	k3xTgFnPc4
knihovny	knihovna	k1gFnPc4
prostředí	prostředí	k1gNnSc2
Java	Jav	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Javě	Java	k1gFnSc6
lze	lze	k6eAd1
naopak	naopak	k6eAd1
používat	používat	k5eAaImF
všechny	všechen	k3xTgFnPc4
knihovny	knihovna	k1gFnPc4
napsané	napsaný	k2eAgFnPc4d1
v	v	k7c6
Jythonu	Jython	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jython	Jython	k1gInSc1
je	být	k5eAaImIp3nS
implementace	implementace	k1gFnSc2
CPythonu	CPython	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
stabilní	stabilní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Jythonu	Jythona	k1gFnSc4
2.7	2.7	k4
<g/>
.1	.1	k4
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuální	aktuální	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
viz	vidět	k5eAaImRp2nS
stránky	stránka	k1gFnPc4
projektu	projekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
IronPython	IronPython	k1gMnSc1
</s>
<s>
IronPython	IronPython	k1gInSc1
je	být	k5eAaImIp3nS
implementace	implementace	k1gFnSc1
Pythonu	Python	k1gMnSc3
pro	pro	k7c4
prostředí	prostředí	k1gNnSc4
.	.	kIx.
<g/>
NET	NET	kA
<g/>
/	/	kIx~
<g/>
Mono	mono	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
výhody	výhoda	k1gFnPc4
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Python	Python	k1gMnSc1
tímto	tento	k3xDgInSc7
stává	stávat	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
jazyků	jazyk	k1gInPc2
pro	pro	k7c4
platformu	platforma	k1gFnSc4
.	.	kIx.
<g/>
NET	NET	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
současně	současně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
lze	lze	k6eAd1
přímo	přímo	k6eAd1
využívat	využívat	k5eAaImF,k5eAaPmF
ve	v	k7c6
všech	všecek	k3xTgInPc6
jazycích	jazyk	k1gInPc6
platformy	platforma	k1gFnSc2
.	.	kIx.
<g/>
NET	NET	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
významu	význam	k1gInSc3
<g/>
,	,	kIx,
jaký	jaký	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
platformě	platforma	k1gFnSc3
.	.	kIx.
<g/>
NET	NET	kA
přikládá	přikládat	k5eAaImIp3nS
firma	firma	k1gFnSc1
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
lze	lze	k6eAd1
očekávat	očekávat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
význam	význam	k1gInSc1
implementace	implementace	k1gFnSc2
IronPython	IronPythona	k1gFnPc2
dále	daleko	k6eAd2
poroste	růst	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
vlastnostem	vlastnost	k1gFnPc3
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
lze	lze	k6eAd1
také	také	k9
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
implementace	implementace	k1gFnSc2
IronPython	IronPythona	k1gFnPc2
stane	stanout	k5eAaPmIp3nS
dlouhodobě	dlouhodobě	k6eAd1
podporovanou	podporovaný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
IronPython	IronPython	k1gInSc4
je	být	k5eAaImIp3nS
implementace	implementace	k1gFnSc2
CPythonu	CPython	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
IronPythonu	IronPython	k1gInSc2
je	být	k5eAaImIp3nS
2.7	2.7	k4
<g/>
.8	.8	k4
vydaná	vydaný	k2eAgNnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negativně	negativně	k6eAd1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vnímána	vnímán	k2eAgFnSc1d1
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
implementace	implementace	k1gFnSc1
IronPython	IronPythona	k1gFnPc2
je	být	k5eAaImIp3nS
vyvíjena	vyvíjet	k5eAaImNgFnS
firmou	firma	k1gFnSc7
Microsoft	Microsoft	kA
pod	pod	k7c7
Microsoft	Microsoft	kA
Public	publicum	k1gNnPc2
License	Licens	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Brython	Brython	k1gMnSc1
</s>
<s>
Brython	Brython	k1gMnSc1
je	být	k5eAaImIp3nS
implementace	implementace	k1gFnPc4
Pythonu	Python	k1gMnSc3
3	#num#	k4
v	v	k7c6
JavaScriptu	JavaScript	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
umožnit	umožnit	k5eAaPmF
ve	v	k7c6
webovém	webový	k2eAgInSc6d1
prohlížeči	prohlížeč	k1gInSc6
programovat	programovat	k5eAaImF
v	v	k7c6
jazyce	jazyk	k1gInSc6
Pythonu	Python	k1gMnSc3
místo	místo	k1gNnSc4
v	v	k7c6
JavaScriptu	JavaScript	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brython	Brython	k1gInSc1
je	být	k5eAaImIp3nS
transkompilátor	transkompilátor	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
překladač	překladač	k1gMnSc1
Python	Python	k1gMnSc1
kódu	kód	k1gInSc2
do	do	k7c2
JavaScript	JavaScripta	k1gFnPc2
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
překlad	překlad	k1gInSc1
se	se	k3xPyFc4
spouští	spouštět	k5eAaImIp3nS
automaticky	automaticky	k6eAd1
na	na	k7c6
pozadí	pozadí	k1gNnSc6
<g/>
,	,	kIx,
programátor	programátor	k1gMnSc1
může	moct	k5eAaImIp3nS
psát	psát	k5eAaImF
Python	Python	k1gMnSc1
kód	kód	k1gInSc4
rovnou	rovnou	k6eAd1
do	do	k7c2
html	htmla	k1gFnPc2
stránky	stránka	k1gFnSc2
jako	jako	k8xC,k8xS
<script type='text/python'>
.	.	kIx.
</s>
<s desamb="1">
Možnosti	možnost	k1gFnPc1
Python	Python	k1gMnSc1
programu	program	k1gInSc6
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
omezeny	omezit	k5eAaPmNgFnP
možnostmi	možnost	k1gFnPc7
prohlížeče	prohlížeč	k1gMnSc2
a	a	k8xC
JavaScriptu	JavaScripta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
používat	používat	k5eAaImF
např.	např.	kA
blokující	blokující	k2eAgNnSc1d1
volání	volání	k1gNnSc1
time	time	k1gFnSc1
<g/>
.	.	kIx.
<g/>
sleep	sleep	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
nebo	nebo	k8xC
modul	modul	k1gInSc1
async	async	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgInSc2
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
alternativní	alternativní	k2eAgInPc4d1
moduly	modul	k1gInPc4
kompatibilní	kompatibilní	k2eAgInPc4d1
s	s	k7c7
webovými	webový	k2eAgInPc7d1
prohlížeči	prohlížeč	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
RPython	RPython	k1gMnSc1
</s>
<s>
RPython	RPython	k1gInSc1
je	být	k5eAaImIp3nS
dialekt	dialekt	k1gInSc4
Pythonu	Python	k1gMnSc3
pro	pro	k7c4
velmi	velmi	k6eAd1
specifické	specifický	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
vývoj	vývoj	k1gInSc4
dynamických	dynamický	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
interpretů	interpret	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syntaxe	syntaxe	k1gFnSc1
jazyka	jazyk	k1gInSc2
RPython	RPythona	k1gFnPc2
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
omezená	omezený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
R	R	kA
v	v	k7c6
názvu	název	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
restricted	restricted	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teoreticky	teoreticky	k6eAd1
lze	lze	k6eAd1
využít	využít	k5eAaPmF
i	i	k9
k	k	k7c3
vývoji	vývoj	k1gInSc3
jiných	jiný	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
nepředpokládá	předpokládat	k5eNaImIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RPython	RPython	k1gInSc1
není	být	k5eNaImIp3nS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
CPythonu	CPython	k1gInSc2
interpret	interpret	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
překladač	překladač	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
nativní	nativní	k2eAgInSc1d1
spustitelný	spustitelný	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
interpretovaným	interpretovaný	k2eAgInPc3d1
programům	program	k1gInPc3
významně	významně	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Cython	Cython	k1gMnSc1
</s>
<s>
Cython	Cython	k1gInSc1
je	být	k5eAaImIp3nS
C	C	kA
rozšíření	rozšíření	k1gNnSc1
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
a	a	k8xC
transkompiler	transkompiler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cython	Cython	k1gMnSc1
překládá	překládat	k5eAaImIp3nS
zdrojový	zdrojový	k2eAgMnSc1d1
Python	Python	k1gMnSc1
kód	kód	k1gInSc4
do	do	k7c2
C	C	kA
kódu	kód	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
standardním	standardní	k2eAgInSc7d1
překladačem	překladač	k1gInSc7
překládá	překládat	k5eAaImIp3nS
do	do	k7c2
binárního	binární	k2eAgInSc2d1
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
tedy	tedy	k9
nativní	nativní	k2eAgInSc1d1
program	program	k1gInSc1
stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
RPythonu	RPython	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
RPythonu	RPython	k1gInSc2
je	být	k5eAaImIp3nS
Cython	Cython	k1gMnSc1
univerzální	univerzální	k2eAgMnSc1d1
a	a	k8xC
neklade	klást	k5eNaImIp3nS
si	se	k3xPyFc3
žádné	žádný	k3yNgNnSc4
omezení	omezení	k1gNnSc4
na	na	k7c4
syntaxi	syntaxe	k1gFnSc4
Pythonu	Python	k1gMnSc3
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
ji	on	k3xPp3gFnSc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
přeložení	přeložení	k1gNnSc4
čistého	čistý	k2eAgInSc2d1
Python	Python	k1gMnSc1
kódu	kód	k1gInSc3
Cythonem	Cython	k1gMnSc7
vede	vést	k5eAaImIp3nS
typicky	typicky	k6eAd1
k	k	k7c3
dvakrát	dvakrát	k6eAd1
rychlejšímu	rychlý	k2eAgInSc3d2
programu	program	k1gInSc3
oproti	oproti	k7c3
interpretované	interpretovaný	k2eAgFnSc3d1
verzi	verze	k1gFnSc3
v	v	k7c6
CPythonu	CPython	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Pomocí	pomocí	k7c2
optimalizací	optimalizace	k1gFnPc2
lze	lze	k6eAd1
výkon	výkon	k1gInSc4
programu	program	k1gInSc2
téměř	téměř	k6eAd1
na	na	k7c4
úroveň	úroveň	k1gFnSc4
implementace	implementace	k1gFnSc2
takového	takový	k3xDgInSc2
programu	program	k1gInSc2
přímo	přímo	k6eAd1
v	v	k7c6
jazyce	jazyk	k1gInSc6
C.	C.	kA
Cython	Cythona	k1gFnPc2
přidává	přidávat	k5eAaImIp3nS
do	do	k7c2
syntaxe	syntax	k1gFnSc2
Pythonu	Python	k1gMnSc3
možnost	možnost	k1gFnSc4
statických	statický	k2eAgInPc2d1
typů	typ	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
jazyka	jazyk	k1gInSc2
C	C	kA
<g/>
,	,	kIx,
včetně	včetně	k7c2
používání	používání	k1gNnSc2
funkcí	funkce	k1gFnPc2
ze	z	k7c2
standardních	standardní	k2eAgInPc2d1
C	C	kA
knihoven	knihovna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
Cythonu	Cython	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
buď	buď	k8xC
spustitelný	spustitelný	k2eAgInSc4d1
program	program	k1gInSc4
nebo	nebo	k8xC
modul	modul	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
implementovány	implementovat	k5eAaImNgFnP
výpočetně	výpočetně	k6eAd1
náročné	náročný	k2eAgFnPc1d1
funkce	funkce	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
pak	pak	k6eAd1
lze	lze	k6eAd1
využívat	využívat	k5eAaPmF,k5eAaImF
ze	z	k7c2
standardního	standardní	k2eAgInSc2d1
CPythonu	CPython	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viz	vidět	k5eAaImRp2nS
kap	kap	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkon	výkon	k1gInSc1
Pythonu	Python	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
PyPy	PyPa	k1gFnPc1
</s>
<s>
PyPy	PyPa	k1gFnPc4
je	být	k5eAaImIp3nS
další	další	k2eAgMnSc1d1
alternativní	alternativní	k2eAgMnSc1d1
interpret	interpret	k1gMnSc1
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
zaměřen	zaměřit	k5eAaPmNgInS
na	na	k7c4
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
interpret	interpret	k1gMnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
RPythonu	RPythona	k1gFnSc4
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
implementaci	implementace	k1gFnSc4
JIT	jit	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
i	i	k9
další	další	k2eAgFnPc4d1
výkonově	výkonově	k6eAd1
užitečné	užitečný	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
stackless	stackless	k6eAd1
mód	mód	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
poskytuje	poskytovat	k5eAaImIp3nS
výkonné	výkonný	k2eAgNnSc4d1
mikro	mikro	k1gNnSc4
thready	threada	k1gFnSc2
pro	pro	k7c4
masivní	masivní	k2eAgNnPc4d1
paralelní	paralelní	k2eAgNnPc4d1
programování	programování	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
verze	verze	k1gFnSc1
PyPy	PyPa	k1gFnSc2
implementuje	implementovat	k5eAaImIp3nS
Python	Python	k1gMnSc1
2.7	2.7	k4
<g/>
.13	.13	k4
a	a	k8xC
3.6	3.6	k4
<g/>
.9	.9	k4
<g/>
.	.	kIx.
</s>
<s>
RustPython	RustPython	k1gMnSc1
</s>
<s>
RustPython	RustPython	k1gMnSc1
je	být	k5eAaImIp3nS
implementace	implementace	k1gFnPc4
Pythonu	Python	k1gMnSc3
3	#num#	k4
v	v	k7c6
jazyce	jazyk	k1gInSc6
Rust	Rust	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
implementace	implementace	k1gFnSc1
je	být	k5eAaImIp3nS
označena	označen	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
vývojová	vývojový	k2eAgFnSc1d1
a	a	k8xC
nevhodná	vhodný	k2eNgFnSc1d1
pro	pro	k7c4
produktivní	produktivní	k2eAgNnSc4d1
nasazení	nasazení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Implementuje	implementovat	k5eAaImIp3nS
CPython	CPython	k1gInSc4
3.5	3.5	k4
<g/>
.	.	kIx.
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Výkonnost	výkonnost	k1gFnSc1
Pythonu	Python	k1gMnSc3
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
nevyrovnaná	vyrovnaný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
to	ten	k3xDgNnSc1
často	často	k6eAd1
nevadí	vadit	k5eNaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
výkon	výkon	k1gInSc1
současných	současný	k2eAgMnPc2d1
počítačů	počítač	k1gMnPc2
je	být	k5eAaImIp3nS
dostatečně	dostatečně	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
někdy	někdy	k6eAd1
používá	používat	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
zpracování	zpracování	k1gNnSc4
rozsáhlých	rozsáhlý	k2eAgNnPc2d1
dat	datum	k1gNnPc2
nebo	nebo	k8xC
na	na	k7c6
strojích	stroj	k1gInPc6
s	s	k7c7
velmi	velmi	k6eAd1
omezenými	omezený	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c6
jednodeskových	jednodeskový	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
Raspberry	Raspberra	k1gFnSc2
Pi	pi	k0
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
mají	mít	k5eAaImIp3nP
několikanásobně	několikanásobně	k6eAd1
nižší	nízký	k2eAgInSc4d2
výkon	výkon	k1gInSc4
než	než	k8xS
libovolné	libovolný	k2eAgFnSc6d1
domácí	domácí	k2eAgFnSc6d1
PC	PC	kA
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
desktop	desktop	k1gInSc1
nebo	nebo	k8xC
notebook	notebook	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
narazíme	narazit	k5eAaPmIp1nP
na	na	k7c4
výkonnostní	výkonnostní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
optimalizace	optimalizace	k1gFnSc1
kódu	kód	k1gInSc2
v	v	k7c6
Pythonu	Python	k1gMnSc6
přinést	přinést	k5eAaPmF
i	i	k9
více	hodně	k6eAd2
než	než	k8xS
řádové	řádový	k2eAgNnSc4d1
zlepšení	zlepšení	k1gNnSc4
–	–	k?
větší	veliký	k2eAgMnPc1d2
než	než	k8xS
u	u	k7c2
jiných	jiný	k2eAgInPc2d1
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
používat	používat	k5eAaImF
takové	takový	k3xDgFnPc4
datové	datový	k2eAgFnPc4d1
struktury	struktura	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
požadované	požadovaný	k2eAgFnPc4d1
operace	operace	k1gFnPc4
dostatečně	dostatečně	k6eAd1
efektivní	efektivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
minimalizovat	minimalizovat	k5eAaBmF
počet	počet	k1gInSc4
vyvolání	vyvolání	k1gNnSc3
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
místo	místo	k7c2
načítání	načítání	k1gNnSc2
souboru	soubor	k1gInSc2
po	po	k7c6
znacích	znak	k1gInPc6
načítat	načítat	k5eAaPmF,k5eAaBmF
celé	celý	k2eAgInPc4d1
řádky	řádek	k1gInPc4
<g/>
,	,	kIx,
místo	místo	k7c2
zpracování	zpracování	k1gNnSc2
řetězců	řetězec	k1gInPc2
v	v	k7c6
cyklu	cyklus	k1gInSc6
používat	používat	k5eAaImF
funkce	funkce	k1gFnPc4
pracující	pracující	k2eAgFnPc4d1
s	s	k7c7
celými	celý	k2eAgInPc7d1
řetězci	řetězec	k1gInPc7
<g/>
,	,	kIx,
např.	např.	kA
provádějící	provádějící	k2eAgFnSc2d1
globální	globální	k2eAgFnSc2d1
operace	operace	k1gFnSc2
s	s	k7c7
regulárními	regulární	k2eAgInPc7d1
výrazy	výraz	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkonově	výkonově	k6eAd1
kritické	kritický	k2eAgFnPc4d1
knihovny	knihovna	k1gFnPc4
jsou	být	k5eAaImIp3nP
implementovány	implementovat	k5eAaImNgFnP
v	v	k7c6
jazyce	jazyk	k1gInSc6
C	C	kA
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
Python	Python	k1gMnSc1
výborně	výborně	k6eAd1
spolupracuje	spolupracovat	k5eAaImIp3nS
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
lepší	dobrý	k2eAgMnSc1d2
provést	provést	k5eAaPmF
desítky	desítka	k1gFnPc4
tisíc	tisíc	k4xCgInPc2
volání	volání	k1gNnPc2
mocné	mocný	k2eAgFnSc2d1
knihovní	knihovní	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
než	než	k8xS
milion	milion	k4xCgInSc1
volání	volání	k1gNnPc2
jednoduché	jednoduchý	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složité	složitý	k2eAgInPc1d1
a	a	k8xC
kaskádované	kaskádovaný	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
se	se	k3xPyFc4
snažíme	snažit	k5eAaImIp1nP
upravit	upravit	k5eAaPmF
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
složité	složitý	k2eAgFnPc1d1
testy	testa	k1gFnPc1
prováděly	provádět	k5eAaImAgFnP
<g/>
,	,	kIx,
až	až	k8xS
když	když	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nezbytné	zbytný	k2eNgNnSc1d1,k2eAgNnSc1d1
<g/>
,	,	kIx,
případně	případně	k6eAd1
aby	aby	kYmCp3nS
se	se	k3xPyFc4
rychle	rychle	k6eAd1
vyloučily	vyloučit	k5eAaPmAgInP
obvyklé	obvyklý	k2eAgInPc1d1
případy	případ	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
práci	práce	k1gFnSc6
s	s	k7c7
regulárními	regulární	k2eAgInPc7d1
výrazy	výraz	k1gInPc7
dáváme	dávat	k5eAaImIp1nP
přednost	přednost	k1gFnSc4
metodě	metoda	k1gFnSc3
match	match	k1gMnSc1
před	před	k7c7
metodou	metoda	k1gFnSc7
search	searcha	k1gFnPc2
<g/>
,	,	kIx,
snažíme	snažit	k5eAaImIp1nP
se	se	k3xPyFc4
používat	používat	k5eAaImF
„	„	k?
<g/>
kotvy	kotva	k1gFnPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
^	^	kIx~
a	a	k8xC
$	$	kIx~
<g/>
)	)	kIx)
a	a	k8xC
místo	místo	k7c2
použití	použití	k1gNnSc2
série	série	k1gFnSc2
regulárních	regulární	k2eAgInPc2d1
výrazů	výraz	k1gInPc2
raději	rád	k6eAd2
použijeme	použít	k5eAaPmIp1nP
jeden	jeden	k4xCgMnSc1
s	s	k7c7
více	hodně	k6eAd2
možnostmi	možnost	k1gFnPc7
spojenými	spojený	k2eAgFnPc7d1
symbolem	symbol	k1gInSc7
|	|	kIx~
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jednotlivé	jednotlivý	k2eAgFnPc4d1
alternativy	alternativa	k1gFnPc4
rozlišíme	rozlišit	k5eAaPmIp1nP
použitím	použití	k1gNnSc7
metody	metoda	k1gFnSc2
group	group	k1gMnSc1
aplikované	aplikovaný	k2eAgFnSc2d1
na	na	k7c4
objekt	objekt	k1gInSc4
vrácený	vrácený	k2eAgInSc4d1
metodou	metoda	k1gFnSc7
match	match	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
historii	historie	k1gFnSc6
Pythonu	Python	k1gMnSc3
vznikla	vzniknout	k5eAaPmAgFnS
řada	řada	k1gFnSc1
optimalizačních	optimalizační	k2eAgFnPc2d1
technik	technika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
se	se	k3xPyFc4
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
výkonu	výkon	k1gInSc2
používala	používat	k5eAaImAgFnS
snadno	snadno	k6eAd1
použitelná	použitelný	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
Psyco	Psyco	k6eAd1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
transparentně	transparentně	k6eAd1
optimalizovala	optimalizovat	k5eAaBmAgFnS
kód	kód	k1gInSc4
Pythonu	Python	k1gMnSc3
na	na	k7c4
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
JIT	jit	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
operace	operace	k1gFnPc1
byly	být	k5eAaImAgFnP
pomocí	pomoc	k1gFnSc7
Psyco	Psyco	k6eAd1
urychleny	urychlit	k5eAaPmNgInP
až	až	k9
řádově	řádově	k6eAd1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
knihovna	knihovna	k1gFnSc1
neudržovaná	udržovaný	k2eNgFnSc1d1
(	(	kIx(
<g/>
cca	cca	kA
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
použitelná	použitelný	k2eAgFnSc1d1
jen	jen	k9
pro	pro	k7c4
32	#num#	k4
<g/>
bitové	bitový	k2eAgFnSc2d1
prostředí	prostředí	k1gNnSc3
a	a	k8xC
podporuje	podporovat	k5eAaImIp3nS
Python	Python	k1gMnSc1
jen	jen	k9
do	do	k7c2
verze	verze	k1gFnSc2
2.6	2.6	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
tuto	tento	k3xDgFnSc4
knihovnu	knihovna	k1gFnSc4
nahradily	nahradit	k5eAaPmAgInP
projekty	projekt	k1gInPc1
jako	jako	k8xC,k8xS
PyPy	PyPy	k1gInPc1
<g/>
,	,	kIx,
Cython	Cython	k1gNnSc1
a	a	k8xC
další	další	k2eAgInPc1d1
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
alternativní	alternativní	k2eAgFnSc2d1
implementace	implementace	k1gFnSc2
Pythonu	Python	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Nejúčinnější	účinný	k2eAgInSc1d3
způsob	způsob	k1gInSc1
dosažení	dosažení	k1gNnSc2
výkonu	výkon	k1gInSc2
v	v	k7c6
Pythonu	Python	k1gMnSc6
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc4
Cythonu	Cython	k1gInSc2
s	s	k7c7
optimalizací	optimalizace	k1gFnSc7
kódu	kód	k1gInSc2
na	na	k7c4
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc1d1
příklad	příklad	k1gInSc1
ukazuje	ukazovat	k5eAaImIp3nS
neoptimalizovanou	optimalizovaný	k2eNgFnSc4d1
a	a	k8xC
maximálně	maximálně	k6eAd1
optimalizovanou	optimalizovaný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
změny	změna	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
učinit	učinit	k5eAaImF,k5eAaPmF
pro	pro	k7c4
získání	získání	k1gNnSc4
maximálního	maximální	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
pro	pro	k7c4
transkompiler	transkompiler	k1gInSc4
Cython	Cythona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
def	def	k?
add_two_numbers	add_two_numbers	k1gInSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
y	y	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
print	print	k1gInSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
</s>
<s>
return	return	k1gInSc1
x	x	k?
+	+	kIx~
y	y	k?
</s>
<s>
z	z	k7c2
=	=	kIx~
add_two_numbers	add_two_numbersa	k1gFnPc2
<g/>
(	(	kIx(
<g/>
123	#num#	k4
<g/>
,	,	kIx,
456	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
print	print	k1gInSc1
<g/>
(	(	kIx(
<g/>
z	z	k7c2
<g/>
)	)	kIx)
</s>
<s>
from	from	k6eAd1
libc	libc	k6eAd1
<g/>
.	.	kIx.
<g/>
stdio	stdio	k6eAd1
cimport	cimport	k1gInSc1
printf	printf	k1gInSc1
</s>
<s>
cdef	cdef	k1gInSc1
int	int	k?
add_two_numbers	add_two_numbers	k1gInSc1
<g/>
(	(	kIx(
<g/>
int	int	k?
x	x	k?
<g/>
,	,	kIx,
int	int	k?
y	y	k?
<g/>
)	)	kIx)
nogil	nogil	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
printf	printf	k1gInSc1
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
%	%	kIx~
<g/>
i	i	k8xC
<g/>
\	\	kIx~
<g/>
n	n	k0
<g/>
"	"	kIx"
<g/>
,	,	kIx,
x	x	k?
<g/>
)	)	kIx)
</s>
<s>
return	return	k1gInSc1
x	x	k?
+	+	kIx~
y	y	k?
</s>
<s>
z	z	k7c2
=	=	kIx~
add_two_numbers	add_two_numbersa	k1gFnPc2
<g/>
(	(	kIx(
<g/>
123	#num#	k4
<g/>
,	,	kIx,
456	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
print	print	k1gInSc1
<g/>
(	(	kIx(
<g/>
z	z	k7c2
<g/>
)	)	kIx)
</s>
<s>
Účinnost	účinnost	k1gFnSc1
těchto	tento	k3xDgFnPc2
optimalizací	optimalizace	k1gFnPc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
optimalizovaný	optimalizovaný	k2eAgInSc1d1
program	program	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
téměř	téměř	k6eAd1
výkonu	výkon	k1gInSc3
aplikace	aplikace	k1gFnSc2
napsané	napsaný	k2eAgFnSc2d1
přímo	přímo	k6eAd1
v	v	k7c6
jazyce	jazyk	k1gInSc6
C.	C.	kA
Viz	vidět	k5eAaImRp2nS
benchmark	benchmark	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
porovnává	porovnávat	k5eAaImIp3nS
výkon	výkon	k1gInSc4
programu	program	k1gInSc2
při	při	k7c6
výpočtu	výpočet	k1gInSc6
velké	velký	k2eAgFnSc2d1
Mandelbrotovy	Mandelbrotův	k2eAgFnSc2d1
množiny	množina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Benchmark	Benchmark	k1gInSc1
výkonu	výkon	k1gInSc2
funkce	funkce	k1gFnSc2
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
Mandelbrotovy	Mandelbrotův	k2eAgFnSc2d1
množiny	množina	k1gFnSc2
v	v	k7c6
sec	sec	kA
<g/>
.	.	kIx.
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1
</s>
<s>
CPython	CPython	k1gInSc1
2	#num#	k4
</s>
<s>
CPython	CPython	k1gInSc1
3	#num#	k4
</s>
<s>
Jython	Jython	k1gMnSc1
</s>
<s>
RPython	RPython	k1gMnSc1
</s>
<s>
ANSI	ANSI	kA
C	C	kA
</s>
<s>
Cython	Cython	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
bez	bez	k7c2
úprav	úprava	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Cython	Cython	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
typy	typ	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Cython	Cython	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
plná	plný	k2eAgFnSc1d1
optimalizace	optimalizace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
4096	#num#	k4
<g/>
×	×	k?
<g/>
4096	#num#	k4
</s>
<s>
150,31	150,31	k4
</s>
<s>
152,21	152,21	k4
</s>
<s>
203,18	203,18	k4
</s>
<s>
18,64	18,64	k4
</s>
<s>
4,75	4,75	k4
</s>
<s>
88,67	88,67	k4
</s>
<s>
16,42	16,42	k4
</s>
<s>
4,80	4,80	k4
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
aplikacemi	aplikace	k1gFnPc7
</s>
<s>
Jak	jak	k6eAd1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
Python	Python	k1gMnSc1
se	se	k3xPyFc4
snadno	snadno	k6eAd1
vkládá	vkládat	k5eAaImIp3nS
do	do	k7c2
jiných	jiný	k2eAgFnPc2d1
aplikací	aplikace	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pak	pak	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
jejich	jejich	k3xOp3gInSc1
skriptovací	skriptovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
ho	on	k3xPp3gMnSc4
najít	najít	k5eAaPmF
např.	např.	kA
v	v	k7c6
3D	3D	k4
programu	program	k1gInSc2
Blender	Blender	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
počítačové	počítačový	k2eAgFnSc6d1
hře	hra	k1gFnSc6
Civilization	Civilization	k1gInSc1
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
kancelářském	kancelářský	k2eAgInSc6d1
balíku	balík	k1gInSc6
OpenOffice	OpenOffice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
v	v	k7c6
textovém	textový	k2eAgInSc6d1
editoru	editor	k1gInSc6
Vim	Vim	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
jej	on	k3xPp3gMnSc4
alternativně	alternativně	k6eAd1
použít	použít	k5eAaPmF
jako	jako	k8xS,k8xC
skriptovací	skriptovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
aplikace	aplikace	k1gFnSc2
GIMP	GIMP	kA
<g/>
,	,	kIx,
existují	existovat	k5eAaImIp3nP
pythonovská	pythonovský	k2eAgNnPc1d1
aplikační	aplikační	k2eAgNnPc1d1
rozhraní	rozhraní	k1gNnPc1
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
dalších	další	k2eAgInPc2d1
projektů	projekt	k1gInPc2
–	–	k?
například	například	k6eAd1
pro	pro	k7c4
ImageMagick	ImageMagick	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varianta	varianta	k1gFnSc1
Jython	Jythona	k1gFnPc2
(	(	kIx(
<g/>
implementace	implementace	k1gFnSc1
Pythonu	Python	k1gMnSc6
v	v	k7c6
Javě	Jav	k1gInSc6
<g/>
)	)	kIx)
jej	on	k3xPp3gNnSc4
umožňuje	umožňovat	k5eAaImIp3nS
používat	používat	k5eAaImF
jako	jako	k9
skriptovací	skriptovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
všude	všude	k6eAd1
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
lze	lze	k6eAd1
používat	používat	k5eAaImF
skripty	skript	k1gInPc4
v	v	k7c6
Javě	Java	k1gFnSc6
–	–	k?
například	například	k6eAd1
v	v	k7c6
editoru	editor	k1gInSc6
jEdit	jEdit	k5eAaImF,k5eAaBmF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Příklady	příklad	k1gInPc1
</s>
<s>
Ukázkový	ukázkový	k2eAgInSc1d1
program	program	k1gInSc1
Hello	Hello	k1gNnSc1
world	world	k6eAd1
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
velmi	velmi	k6eAd1
jednoduše	jednoduše	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
print	print	k1gInSc1
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Hello	Hello	k1gNnSc1
<g/>
,	,	kIx,
World	World	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
)	)	kIx)
#	#	kIx~
ve	v	k7c6
verzích	verze	k1gFnPc6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
print	print	k1gInSc1
"	"	kIx"
<g/>
Hello	Hello	k1gNnSc1
<g/>
,	,	kIx,
World	World	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
</s>
<s>
Program	program	k1gInSc1
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
obsahu	obsah	k1gInSc2
kruhu	kruh	k1gInSc2
ze	z	k7c2
zadaného	zadaný	k2eAgInSc2d1
poloměru	poloměr	k1gInSc2
v	v	k7c6
syntaxi	syntax	k1gFnSc6
Python	Python	k1gMnSc1
3	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
#	#	kIx~
toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
komentář	komentář	k1gInSc1
a	a	k8xC
interpret	interpret	k1gMnSc1
jej	on	k3xPp3gNnSc4
ignoruje	ignorovat	k5eAaImIp3nS
</s>
<s>
import	import	k1gInSc1
math	math	k1gInSc1
#	#	kIx~
zpřístupní	zpřístupnit	k5eAaPmIp3nS
modul	modul	k1gInSc1
s	s	k7c7
matematickými	matematický	k2eAgFnPc7d1
funkcemi	funkce	k1gFnPc7
a	a	k8xC
konstantami	konstanta	k1gFnPc7
(	(	kIx(
<g/>
sin	sin	kA
<g/>
,	,	kIx,
cos	cos	kA
<g/>
,	,	kIx,
pi	pi	k0
atp.	atp.	kA
<g/>
)	)	kIx)
</s>
<s>
vstup	vstup	k1gInSc1
=	=	kIx~
input	input	k1gInSc1
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Zadejte	zadat	k5eAaPmRp2nP
polomer	polomer	k1gInSc4
<g/>
:	:	kIx,
"	"	kIx"
<g/>
)	)	kIx)
#	#	kIx~
zobrazí	zobrazit	k5eAaPmIp3nS
výzvu	výzva	k1gFnSc4
a	a	k8xC
načte	načíst	k5eAaBmIp3nS,k5eAaPmIp3nS
nějaký	nějaký	k3yIgInSc1
řetězec	řetězec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
verzi	verze	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
se	se	k3xPyFc4
místo	místo	k7c2
funkce	funkce	k1gFnSc2
input	input	k1gInSc1
používá	používat	k5eAaImIp3nS
funkce	funkce	k1gFnSc1
raw_input	raw_input	k1gInSc4
</s>
<s>
r	r	kA
=	=	kIx~
float	float	k2eAgInSc4d1
<g/>
(	(	kIx(
<g/>
vstup	vstup	k1gInSc4
<g/>
)	)	kIx)
#	#	kIx~
převede	převést	k5eAaPmIp3nS
řetězec	řetězec	k1gInSc1
na	na	k7c4
desetinné	desetinný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
</s>
<s>
S	s	k7c7
=	=	kIx~
r	r	kA
<g/>
**	**	k?
<g/>
2	#num#	k4
*	*	kIx~
math	math	k1gInSc1
<g/>
.	.	kIx.
<g/>
pi	pi	k0
#	#	kIx~
umocní	umocnit	k5eAaPmIp3nP
r	r	kA
na	na	k7c4
2	#num#	k4
a	a	k8xC
vynásobí	vynásobit	k5eAaPmIp3nS
jej	on	k3xPp3gNnSc4
pí	pí	k1gNnSc4
</s>
<s>
print	print	k1gInSc1
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Výsledek	výsledek	k1gInSc1
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
"	"	kIx"
<g/>
,	,	kIx,
S	s	k7c7
<g/>
)	)	kIx)
#	#	kIx~
zobrazí	zobrazit	k5eAaPmIp3nS
výsledek	výsledek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
verzi	verze	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
se	se	k3xPyFc4
píše	psát	k5eAaImIp3nS
bez	bez	k7c2
závorek	závorka	k1gFnPc2
</s>
<s>
Výpočet	výpočet	k1gInSc1
faktoriálu	faktoriál	k1gInSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
jazykem	jazyk	k1gInSc7
C	C	kA
<g/>
:	:	kIx,
</s>
<s>
Program	program	k1gInSc1
v	v	k7c6
jazyce	jazyk	k1gInSc6
Python	Python	k1gMnSc1
</s>
<s>
Odpovídající	odpovídající	k2eAgInSc1d1
program	program	k1gInSc1
v	v	k7c6
jazyce	jazyk	k1gInSc6
C	C	kA
</s>
<s>
def	def	k?
factorial	factorial	k1gInSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
if	if	k?
x	x	k?
<	<	kIx(
<g/>
=	=	kIx~
0	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
return	return	k1gInSc1
1	#num#	k4
</s>
<s>
else	elsat	k5eAaPmIp3nS
<g/>
:	:	kIx,
</s>
<s>
return	return	k1gInSc1
x	x	k?
*	*	kIx~
factorial	factorial	k1gMnSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
-	-	kIx~
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
def	def	k?
factorial_kratsi	factorial_kratse	k1gFnSc4
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
return	return	k1gInSc1
1	#num#	k4
if	if	k?
x	x	k?
<	<	kIx(
<g/>
=	=	kIx~
0	#num#	k4
else	els	k1gFnSc2
x	x	k?
*	*	kIx~
factorial_kratsi	factorial_kratse	k1gFnSc4
<g/>
(	(	kIx(
<g/>
x	x	k?
-	-	kIx~
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
int	int	k?
factorial	factorial	k1gInSc1
<g/>
(	(	kIx(
<g/>
int	int	k?
x	x	k?
<g/>
)	)	kIx)
{	{	kIx(
</s>
<s>
if	if	k?
(	(	kIx(
<g/>
x	x	k?
<	<	kIx(
<g/>
=	=	kIx~
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
return	return	k1gInSc1
1	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
else	else	k6eAd1
</s>
<s>
return	return	k1gInSc1
x	x	k?
*	*	kIx~
factorial	factorial	k1gMnSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
-	-	kIx~
1	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
}	}	kIx)
</s>
<s>
int	int	k?
factorial_kratsi	factorial_kratse	k1gFnSc4
<g/>
(	(	kIx(
<g/>
int	int	k?
x	x	k?
<g/>
)	)	kIx)
{	{	kIx(
</s>
<s>
return	return	k1gInSc1
x	x	k?
<	<	kIx(
<g/>
=	=	kIx~
0	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
1	#num#	k4
:	:	kIx,
x	x	k?
*	*	kIx~
factorial_kratsi	factorial_kratse	k1gFnSc4
<g/>
(	(	kIx(
<g/>
x	x	k?
-	-	kIx~
1	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
}	}	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
a	a	k8xC
použití	použití	k1gNnSc1
jazyka	jazyk	k1gInSc2
</s>
<s>
Proměnná	proměnná	k1gFnSc1
je	být	k5eAaImIp3nS
pojmenovaným	pojmenovaný	k2eAgInSc7d1
odkazem	odkaz	k1gInSc7
na	na	k7c4
objekt	objekt	k1gInSc4
</s>
<s>
Každá	každý	k3xTgFnSc1
proměnná	proměnná	k1gFnSc1
se	se	k3xPyFc4
chápe	chápat	k5eAaImIp3nS
jako	jako	k9
pojmenovaný	pojmenovaný	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
na	na	k7c4
objekt	objekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
jméno	jméno	k1gNnSc4
proměnné	proměnná	k1gFnSc2
je	být	k5eAaImIp3nS
svázáno	svázat	k5eAaPmNgNnS
s	s	k7c7
jinak	jinak	k6eAd1
bezejmenným	bezejmenný	k2eAgInSc7d1
objektem	objekt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkaz	příkaz	k1gInSc1
přiřazení	přiřazení	k1gNnSc2
nezajistí	zajistit	k5eNaPmIp3nS
okopírování	okopírování	k1gNnSc1
hodnoty	hodnota	k1gFnSc2
navázaného	navázaný	k2eAgInSc2d1
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provede	provést	k5eAaPmIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
svázání	svázání	k1gNnSc1
nového	nový	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
s	s	k7c7
původním	původní	k2eAgInSc7d1
objektem	objekt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
a	a	k8xC
=	=	kIx~
[	[	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
b	b	k?
=	=	kIx~
a	a	k8xC
</s>
<s>
Jména	jméno	k1gNnPc4
a	a	k8xC
i	i	k8xC
b	b	k?
jsou	být	k5eAaImIp3nP
nyní	nyní	k6eAd1
svázána	svázat	k5eAaPmNgFnS
se	s	k7c7
stejným	stejný	k2eAgInSc7d1
objektem	objekt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
objekt	objekt	k1gInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
měněn	měněn	k2eAgMnSc1d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
změna	změna	k1gFnSc1
provedená	provedený	k2eAgFnSc1d1
přes	přes	k7c4
jméno	jméno	k1gNnSc4
b	b	k?
projeví	projevit	k5eAaPmIp3nS
i	i	k9
při	při	k7c6
následném	následný	k2eAgInSc6d1
přístupu	přístup	k1gInSc6
přes	přes	k7c4
jméno	jméno	k1gNnSc4
a.	a.	k?
Příklad	příklad	k1gInSc1
–	–	k?
zrušíme	zrušit	k5eAaPmIp1nP
první	první	k4xOgInSc4
prvek	prvek	k1gInSc4
seznamu	seznam	k1gInSc2
přes	přes	k7c4
jméno	jméno	k1gNnSc4
b	b	k?
a	a	k8xC
zobrazíme	zobrazit	k5eAaPmIp1nP
obsah	obsah	k1gInSc4
seznamu	seznam	k1gInSc2
přes	přes	k7c4
jméno	jméno	k1gNnSc4
a	a	k8xC
<g/>
:	:	kIx,
</s>
<s>
del	del	k?
b	b	k?
<g/>
[	[	kIx(
<g/>
0	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
výsledku	výsledek	k1gInSc6
mají	mít	k5eAaImIp3nP
a	a	k8xC
a	a	k8xC
b	b	k?
stejnou	stejný	k2eAgFnSc4d1
„	„	k?
<g/>
hodnotu	hodnota	k1gFnSc4
<g/>
“	“	k?
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odkazování	odkazování	k1gNnSc1
na	na	k7c4
stejný	stejný	k2eAgInSc4d1
objekt	objekt	k1gInSc4
lze	lze	k6eAd1
zjistit	zjistit	k5eAaPmF
konstrukcí	konstrukce	k1gFnSc7
<g/>
:	:	kIx,
</s>
<s>
a	a	k8xC
is	is	k?
b	b	k?
</s>
<s>
#	#	kIx~
=	=	kIx~
<g/>
>	>	kIx)
True	True	k1gInSc1
</s>
<s>
Funkce	funkce	k1gFnSc1
se	se	k3xPyFc4
uchovává	uchovávat	k5eAaImIp3nS
jako	jako	k9
objekt	objekt	k1gInSc1
</s>
<s>
Funkce	funkce	k1gFnSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
jako	jako	k9
běžný	běžný	k2eAgInSc4d1
objekt	objekt	k1gInSc4
<g/>
,	,	kIx,
dokud	dokud	k8xS
není	být	k5eNaImIp3nS
zavolána	zavolán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
def	def	k?
funkce	funkce	k1gFnSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
print	print	k1gMnSc1
'	'	kIx"
<g/>
Python	Python	k1gMnSc1
<g/>
'	'	kIx"
</s>
<s>
f	f	k?
=	=	kIx~
funkce	funkce	k1gFnSc2
</s>
<s>
p	p	k?
=	=	kIx~
[	[	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
'	'	kIx"
<g/>
test	test	k1gInSc1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
f	f	k?
<g/>
]	]	kIx)
</s>
<s>
p	p	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
)	)	kIx)
</s>
<s>
Lze	lze	k6eAd1
s	s	k7c7
ní	on	k3xPp3gFnSc7
manipulovat	manipulovat	k5eAaImF
<g/>
,	,	kIx,
ukládat	ukládat	k5eAaImF
do	do	k7c2
proměnných	proměnná	k1gFnPc2
<g/>
,	,	kIx,
polí	pole	k1gFnPc2
<g/>
,	,	kIx,
objektů	objekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
manipuluje	manipulovat	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
odkazem	odkaz	k1gInSc7
na	na	k7c4
objekt	objekt	k1gInSc4
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
objektem	objekt	k1gInSc7
funkce	funkce	k1gFnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
podle	podle	k7c2
potřeby	potřeba	k1gFnSc2
svázat	svázat	k5eAaPmF
i	i	k9
nové	nový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
ji	on	k3xPp3gFnSc4
i	i	k9
kdykoliv	kdykoliv	k6eAd1
předefinovat	předefinovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
složených	složený	k2eAgFnPc2d1
datových	datový	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
se	se	k3xPyFc4
ukládají	ukládat	k5eAaImIp3nP
odkazy	odkaz	k1gInPc1
</s>
<s>
Do	do	k7c2
složených	složený	k2eAgFnPc2d1
datových	datový	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
se	se	k3xPyFc4
ukládají	ukládat	k5eAaImIp3nP
odkazy	odkaz	k1gInPc1
na	na	k7c4
objekty	objekt	k1gInPc4
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
objekty	objekt	k1gInPc1
samotné	samotný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typ	typ	k1gInSc1
objektu	objekt	k1gInSc2
není	být	k5eNaImIp3nS
vázán	vázat	k5eAaImNgInS
na	na	k7c4
odkaz	odkaz	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
svázán	svázat	k5eAaPmNgMnS
až	až	k6eAd1
s	s	k7c7
odkazovaným	odkazovaný	k2eAgInSc7d1
objektem	objekt	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
například	například	k6eAd1
do	do	k7c2
jednoho	jeden	k4xCgInSc2
seznamu	seznam	k1gInSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
současně	současně	k6eAd1
uložit	uložit	k5eAaPmF
odkazy	odkaz	k1gInPc4
na	na	k7c4
objekty	objekt	k1gInPc4
libovolného	libovolný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
a	a	k8xC
=	=	kIx~
[	[	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
'	'	kIx"
<g/>
pokus	pokus	k1gInSc1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
u	u	k7c2
<g/>
"	"	kIx"
<g/>
UNICODE	UNICODE	kA
<g/>
"	"	kIx"
<g/>
,	,	kIx,
(	(	kIx(
<g/>
'	'	kIx"
<g/>
a	a	k8xC
tak	tak	k6eAd1
<g/>
'	'	kIx"
<g/>
,	,	kIx,
u	u	k7c2
<g/>
'	'	kIx"
<g/>
dále	daleko	k6eAd2
<g/>
...	...	k?
<g/>
'	'	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
{	{	kIx(
<g/>
'	'	kIx"
<g/>
4	#num#	k4
<g/>
'	'	kIx"
<g/>
:	:	kIx,
<g/>
44	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
<g/>
}	}	kIx)
<g/>
]	]	kIx)
<g/>
#	#	kIx~
<g/>
od	od	k7c2
verze	verze	k1gFnSc2
3.0	3.0	k4
není	být	k5eNaImIp3nS
potřeba	potřeba	k1gFnSc1
před	před	k7c4
řetězce	řetězec	k1gInPc4
psát	psát	k5eAaImF
U	u	k7c2
<g/>
,	,	kIx,
protože	protože	k8xS
všechny	všechen	k3xTgInPc1
řetězce	řetězec	k1gInPc1
jsou	být	k5eAaImIp3nP
Unicode	Unicod	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
,	,	kIx,
z	z	k7c2
technického	technický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
jsou	být	k5eAaImIp3nP
odkazy	odkaz	k1gInPc4
všechny	všechen	k3xTgMnPc4
stejného	stejný	k2eAgInSc2d1
typu	typ	k1gInSc2
(	(	kIx(
<g/>
interního	interní	k2eAgInSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
vztah	vztah	k1gInSc4
k	k	k7c3
typu	typ	k1gInSc3
odkazovaného	odkazovaný	k2eAgInSc2d1
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technicky	technicky	k6eAd1
lze	lze	k6eAd1
tedy	tedy	k8xC
seznam	seznam	k1gInSc4
považovat	považovat	k5eAaImF
za	za	k7c4
homogenní	homogenní	k2eAgInSc4d1
datový	datový	k2eAgInSc4d1
typ	typ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
uživatelského	uživatelský	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
to	ten	k3xDgNnSc1
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
seznamu	seznam	k1gInSc2
můžeme	moct	k5eAaImIp1nP
vkládat	vkládat	k5eAaImF
hodnoty	hodnota	k1gFnPc4
různého	různý	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
jednou	jednou	k6eAd1
–	–	k?
do	do	k7c2
seznamu	seznam	k1gInSc2
se	se	k3xPyFc4
nevkládají	vkládat	k5eNaImIp3nP
hodnoty	hodnota	k1gFnPc1
daných	daný	k2eAgInPc2d1
typů	typ	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
beztypové	beztypový	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
na	na	k7c4
příslušné	příslušný	k2eAgInPc4d1
objekty	objekt	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Proměnné	proměnný	k2eAgNnSc1d1
není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
deklarovat	deklarovat	k5eAaBmF
</s>
<s>
V	v	k7c6
jiných	jiný	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
se	se	k3xPyFc4
při	při	k7c6
deklaraci	deklarace	k1gFnSc6
proměnné	proměnná	k1gFnSc2
uvádí	uvádět	k5eAaImIp3nS
souvislost	souvislost	k1gFnSc1
jména	jméno	k1gNnSc2
proměnné	proměnná	k1gFnSc2
s	s	k7c7
typem	typ	k1gInSc7
ukládané	ukládaný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jazyce	jazyk	k1gInSc6
Python	Python	k1gMnSc1
je	být	k5eAaImIp3nS
proměnná	proměnný	k2eAgFnSc1d1
jen	jen	k6eAd1
pojmenovaným	pojmenovaný	k2eAgInSc7d1
odkazem	odkaz	k1gInSc7
na	na	k7c4
nějaký	nějaký	k3yIgInSc4
objekt	objekt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typ	typ	k1gInSc1
objektu	objekt	k1gInSc2
je	být	k5eAaImIp3nS
ale	ale	k8xC
vázán	vázat	k5eAaImNgMnS
na	na	k7c4
odkazovaný	odkazovaný	k2eAgInSc4d1
objekt	objekt	k1gInSc4
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
na	na	k7c4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřeba	potřeba	k1gFnSc1
deklarace	deklarace	k1gFnSc2
proměnné	proměnná	k1gFnSc2
ve	v	k7c6
významu	význam	k1gInSc6
určení	určení	k1gNnSc2
souvisejícího	související	k2eAgInSc2d1
typu	typ	k1gInSc2
dat	datum	k1gNnPc2
tedy	tedy	k9
odpadá	odpadat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Existence	existence	k1gFnSc1
<g/>
,	,	kIx,
či	či	k8xC
neexistence	neexistence	k1gFnSc1
jména	jméno	k1gNnSc2
přímo	přímo	k6eAd1
nesouvisí	souviset	k5eNaImIp3nS
s	s	k7c7
existencí	existence	k1gFnSc7
či	či	k8xC
neexistencí	neexistence	k1gFnSc7
hodnotového	hodnotový	k2eAgInSc2d1
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Význam	význam	k1gInSc1
deklarace	deklarace	k1gFnSc2
proměnné	proměnná	k1gFnSc2
ve	v	k7c6
smyslu	smysl	k1gInSc6
popisu	popis	k1gInSc2
existence	existence	k1gFnSc2
související	související	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
tedy	tedy	k8xC
rovněž	rovněž	k9
odpadá	odpadat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proměnná	proměnná	k1gFnSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
pojmenovaný	pojmenovaný	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
v	v	k7c6
okamžiku	okamžik	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
jméno	jméno	k1gNnSc1
objeví	objevit	k5eAaPmIp3nS
na	na	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
přiřazovacího	přiřazovací	k2eAgInSc2d1
příkazu	příkaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
proměnné	proměnná	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
později	pozdě	k6eAd2
svázáno	svázat	k5eAaPmNgNnS
dalším	další	k2eAgNnSc7d1
přiřazením	přiřazení	k1gNnSc7
s	s	k7c7
jiným	jiný	k2eAgInSc7d1
objektem	objekt	k1gInSc7
zcela	zcela	k6eAd1
jiného	jiný	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
p	p	k?
=	=	kIx~
1	#num#	k4
</s>
<s>
p	p	k?
<g/>
2	#num#	k4
=	=	kIx~
""	""	k?
</s>
<s>
p	p	k?
<g/>
3	#num#	k4
=	=	kIx~
p	p	k?
#	#	kIx~
Kopie	kopie	k1gFnSc1
odkazu	odkaz	k1gInSc2
p	p	k?
</s>
<s>
Členské	členský	k2eAgFnPc1d1
proměnné	proměnná	k1gFnPc1
tříd	třída	k1gFnPc2
mohou	moct	k5eAaImIp3nP
vznikat	vznikat	k5eAaImF
až	až	k9
za	za	k7c2
běhu	běh	k1gInSc2
</s>
<s>
Mezi	mezi	k7c4
běžné	běžný	k2eAgFnPc4d1
praktiky	praktika	k1gFnPc4
při	při	k7c6
vytváření	vytváření	k1gNnSc6
objektu	objekt	k1gInSc2
patří	patřit	k5eAaImIp3nS
i	i	k9
založení	založení	k1gNnSc3
používaných	používaný	k2eAgFnPc2d1
členských	členský	k2eAgFnPc2d1
proměnných	proměnná	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
obrat	obrat	k1gInSc1
se	se	k3xPyFc4
ale	ale	k8xC
v	v	k7c6
jazyce	jazyk	k1gInSc6
Python	Python	k1gMnSc1
chápe	chápat	k5eAaImIp3nS
jako	jako	k9
užitečná	užitečný	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
jako	jako	k8xS,k8xC
nutnost	nutnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členské	členský	k2eAgFnPc4d1
proměnné	proměnná	k1gFnPc4
(	(	kIx(
<g/>
čili	čili	k8xC
proměnné	proměnná	k1gFnPc1
uvnitř	uvnitř	k7c2
objektu	objekt	k1gInSc2
<g/>
)	)	kIx)
mohou	moct	k5eAaImIp3nP
vznikat	vznikat	k5eAaImF
až	až	k9
za	za	k7c2
běhu	běh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
class	class	k1gInSc1
pokus	pokus	k1gInSc1
<g/>
:	:	kIx,
pass	pass	k1gInSc1
#	#	kIx~
<g/>
prázdná	prázdný	k2eAgFnSc1d1
třída	třída	k1gFnSc1
</s>
<s>
obj	obj	k?
=	=	kIx~
pokus	pokus	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
</s>
<s>
obj	obj	k?
<g/>
.	.	kIx.
<g/>
field	field	k1gInSc1
<g/>
1	#num#	k4
=	=	kIx~
33	#num#	k4
</s>
<s>
obj	obj	k?
<g/>
.	.	kIx.
<g/>
field	field	k1gInSc1
<g/>
2	#num#	k4
=	=	kIx~
'	'	kIx"
<g/>
str	str	kA
<g/>
'	'	kIx"
</s>
<s>
Existují	existovat	k5eAaImIp3nP
ale	ale	k9
techniky	technika	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
prostředky	prostředek	k1gInPc7
jazyka	jazyk	k1gInSc2
zamezit	zamezit	k5eAaPmF
možnost	možnost	k1gFnSc4
dodatečného	dodatečný	k2eAgNnSc2d1
přidávání	přidávání	k1gNnSc2
členských	členský	k2eAgFnPc2d1
proměnných	proměnná	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dynamická	dynamický	k2eAgFnSc1d1
silná	silný	k2eAgFnSc1d1
typová	typový	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
</s>
<s>
Při	při	k7c6
operacích	operace	k1gFnPc6
nad	nad	k7c7
objekty	objekt	k1gInPc7
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
provádí	provádět	k5eAaImIp3nS
silná	silný	k2eAgFnSc1d1
typová	typový	k2eAgFnSc1d1
kontrola	kontrola	k1gFnSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
operacích	operace	k1gFnPc6
s	s	k7c7
typy	typ	k1gInPc7
nedochází	docházet	k5eNaImIp3nS
k	k	k7c3
automatickému	automatický	k2eAgNnSc3d1
přetypování	přetypování	k1gNnSc3
hodnot	hodnota	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
jsou	být	k5eAaImIp3nP
v	v	k7c6
Pythonu	Python	k1gMnSc6
2	#num#	k4
datové	datový	k2eAgInPc1d1
typy	typ	k1gInPc1
int	int	k?
a	a	k8xC
long	long	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
nedochází	docházet	k5eNaImIp3nS
k	k	k7c3
přetečení	přetečení	k1gNnSc3
datového	datový	k2eAgInSc2d1
typu	typ	k1gInSc2
int	int	k?
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
automatickému	automatický	k2eAgNnSc3d1
přetypování	přetypování	k1gNnSc3
hodnoty	hodnota	k1gFnSc2
z	z	k7c2
int	int	k?
na	na	k7c4
long	long	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
3	#num#	k4
už	už	k6eAd1
podporuje	podporovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
datový	datový	k2eAgInSc1d1
typ	typ	k1gInSc1
int	int	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
vlastnosti	vlastnost	k1gFnPc4
jako	jako	k9
datový	datový	k2eAgInSc1d1
typ	typ	k1gInSc1
long	long	k1gInSc1
v	v	k7c6
Pythonu	Python	k1gMnSc6
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
v	v	k7c6
Pythonu	Python	k1gMnSc6
2	#num#	k4
i	i	k8xC
Pythonu	Python	k1gMnSc3
3	#num#	k4
podporovány	podporován	k2eAgFnPc1d1
aritmetické	aritmetický	k2eAgFnPc1d1
operace	operace	k1gFnPc1
různých	různý	k2eAgInPc2d1
numerických	numerický	k2eAgInPc2d1
datových	datový	k2eAgInPc2d1
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
lze	lze	k6eAd1
sečíst	sečíst	k5eAaPmF
datové	datový	k2eAgInPc1d1
typy	typ	k1gInPc1
int	int	k?
a	a	k8xC
float	float	k2eAgMnSc1d1
<g/>
,	,	kIx,
výsledkem	výsledek	k1gInSc7
bude	být	k5eAaImBp3nS
datový	datový	k2eAgInSc1d1
typ	typ	k1gInSc1
float	float	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
+	+	kIx~
1.0	1.0	k4
=	=	kIx~
2.0	2.0	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
řady	řada	k1gFnSc2
jiných	jiný	k2eAgInPc2d1
interpretovaných	interpretovaný	k2eAgInPc2d1
dynamických	dynamický	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
nedochází	docházet	k5eNaImIp3nS
k	k	k7c3
automatickému	automatický	k2eAgInSc3d1
převodu	převod	k1gInSc3
číselných	číselný	k2eAgInPc2d1
textových	textový	k2eAgInPc2d1
řetězců	řetězec	k1gInPc2
na	na	k7c4
čísla	číslo	k1gNnPc4
<g/>
,	,	kIx,
proto	proto	k8xC
1	#num#	k4
+	+	kIx~
'	'	kIx"
<g/>
1	#num#	k4
<g/>
'	'	kIx"
=	=	kIx~
výjimka	výjimka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Výjimku	výjimka	k1gFnSc4
vyvolá	vyvolat	k5eAaPmIp3nS
každá	každý	k3xTgFnSc1
nepodporovaná	podporovaný	k2eNgFnSc1d1
operace	operace	k1gFnSc1
různých	různý	k2eAgInPc2d1
datových	datový	k2eAgInPc2d1
typů	typ	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepodporované	podporovaný	k2eNgInPc1d1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
dělení	dělení	k1gNnSc1
řetězců	řetězec	k1gInPc2
<g/>
,	,	kIx,
proto	proto	k8xC
'	'	kIx"
<g/>
abcd	abcd	k6eAd1
<g/>
'	'	kIx"
/	/	kIx~
2	#num#	k4
=	=	kIx~
výjimka	výjimka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Násobení	násobení	k1gNnSc1
podporované	podporovaný	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
proto	proto	k8xC
'	'	kIx"
<g/>
abcd	abcd	k6eAd1
<g/>
'	'	kIx"
*	*	kIx~
2	#num#	k4
=	=	kIx~
'	'	kIx"
<g/>
abcdabcd	abcdabcd	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s>
Datové	datový	k2eAgInPc1d1
typy	typ	k1gInPc1
se	se	k3xPyFc4
kontrolují	kontrolovat	k5eAaImIp3nP
dynamicky	dynamicky	k6eAd1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jest	být	k5eAaImIp3nS
až	až	k9
během	během	k7c2
chodu	chod	k1gInSc2
programu	program	k1gInSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
při	při	k7c6
kompilaci	kompilace	k1gFnSc6
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Python	Python	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
podporuje	podporovat	k5eAaImIp3nS
volitelné	volitelný	k2eAgFnPc4d1
statické	statický	k2eAgFnPc4d1
typové	typový	k2eAgFnPc4d1
anotace	anotace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
externím	externí	k2eAgInPc3d1
nástrojům	nástroj	k1gInPc3
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
např.	např.	kA
mypy	mypa	k1gFnSc2
<g/>
,	,	kIx,
provádět	provádět	k5eAaImF
statickou	statický	k2eAgFnSc4d1
analýzu	analýza	k1gFnSc4
a	a	k8xC
kontrolu	kontrola	k1gFnSc4
datových	datový	k2eAgInPc2d1
typů	typ	k1gInPc2
v	v	k7c6
python	python	k1gMnSc1
kódu	kód	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc1d1
standardní	standardní	k2eAgInSc1d1
interpret	interpret	k1gMnSc1
Pythonu	Python	k1gMnSc3
s	s	k7c7
nimi	on	k3xPp3gMnPc7
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
nepracuje	pracovat	k5eNaImIp3nS
(	(	kIx(
<g/>
ignoruje	ignorovat	k5eAaImIp3nS
je	on	k3xPp3gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
umožňuje	umožňovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
syntaxi	syntaxe	k1gFnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
nevyužívají	využívat	k5eNaImIp3nP
k	k	k7c3
optimalizaci	optimalizace	k1gFnSc3
rychlosti	rychlost	k1gFnSc2
běhu	běh	k1gInSc2
rychlosti	rychlost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ortogonalita	Ortogonalita	k1gFnSc1
operátorů	operátor	k1gMnPc2
</s>
<s>
Při	při	k7c6
vývoji	vývoj	k1gInSc6
jazyka	jazyk	k1gInSc2
se	se	k3xPyFc4
kladl	klást	k5eAaImAgMnS
a	a	k8xC
klade	klást	k5eAaImIp3nS
důraz	důraz	k1gInSc4
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
operátory	operátor	k1gInPc1
nebyly	být	k5eNaImAgInP
vázány	vázat	k5eAaImNgInP
na	na	k7c4
specifické	specifický	k2eAgInPc4d1
datové	datový	k2eAgInPc4d1
typy	typ	k1gInPc4
(	(	kIx(
<g/>
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
možné	možný	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přípustnost	přípustnost	k1gFnSc1
použití	použití	k1gNnSc2
operátoru	operátor	k1gInSc2
pro	pro	k7c4
konkrétní	konkrétní	k2eAgInPc4d1
operandy	operand	k1gInPc4
se	se	k3xPyFc4
navíc	navíc	k6eAd1
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
až	až	k9
za	za	k7c2
běhu	běh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prakticky	prakticky	k6eAd1
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
například	například	k6eAd1
následující	následující	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
v	v	k7c6
těle	tělo	k1gNnSc6
používá	používat	k5eAaImIp3nS
operátor	operátor	k1gInSc1
plus	plus	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
předat	předat	k5eAaPmF
jednak	jednak	k8xC
číselné	číselný	k2eAgFnPc1d1
a	a	k8xC
jednak	jednak	k8xC
řetězcové	řetězcový	k2eAgInPc4d1
argumenty	argument	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
def	def	k?
dohromady	dohromady	k6eAd1
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
,	,	kIx,
b	b	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
return	return	k1gInSc1
a	a	k8xC
+	+	kIx~
b	b	k?
</s>
<s>
dohromady	dohromady	k6eAd1
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
)	)	kIx)
#	#	kIx~
vrátí	vrátit	k5eAaPmIp3nS
5	#num#	k4
</s>
<s>
dohromady	dohromady	k6eAd1
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
ahoj	ahoj	k0
<g/>
"	"	kIx"
<g/>
,	,	kIx,
'	'	kIx"
nazdar	nazdar	k0
<g/>
'	'	kIx"
<g/>
)	)	kIx)
#	#	kIx~
vrátí	vrátit	k5eAaPmIp3nS
'	'	kIx"
<g/>
ahoj	ahoj	k0
nazdar	nazdar	k0
<g/>
'	'	kIx"
</s>
<s>
Nejde	jít	k5eNaImIp3nS
jen	jen	k9
o	o	k7c4
zajímavou	zajímavý	k2eAgFnSc4d1
hříčku	hříčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžné	běžný	k2eAgFnSc2d1
pythonovské	pythonovský	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
tím	ten	k3xDgNnSc7
získávají	získávat	k5eAaImIp3nP
vlastnosti	vlastnost	k1gFnPc1
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
generické	generický	k2eAgNnSc1d1
programování	programování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Interaktivní	interaktivní	k2eAgInSc1d1
režim	režim	k1gInSc1
překladače	překladač	k1gInSc2
</s>
<s>
Interpret	interpret	k1gMnSc1
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
můžeme	moct	k5eAaImIp1nP
spustit	spustit	k5eAaPmF
v	v	k7c6
interaktivním	interaktivní	k2eAgInSc6d1
režimu	režim	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
režim	režim	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
především	především	k9
pro	pro	k7c4
rychlé	rychlý	k2eAgInPc4d1
pokusy	pokus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řádkový	řádkový	k2eAgInSc1d1
vstup	vstup	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
uvozen	uvozen	k2eAgInSc4d1
znaky	znak	k1gInPc4
>>>	>>>	k?
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
očekáván	očekáván	k2eAgInSc1d1
pokračovací	pokračovací	k2eAgInSc1d1
řádek	řádek	k1gInSc1
zápisu	zápis	k1gInSc2
dosud	dosud	k6eAd1
nedokončené	dokončený	k2eNgFnPc1d1
konstrukce	konstrukce	k1gFnPc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
vstupní	vstupní	k2eAgInSc4d1
řádek	řádek	k1gInSc4
uvozen	uvozen	k2eAgInSc4d1
znaky	znak	k1gInPc4
....	....	k?
Dokončení	dokončení	k1gNnSc3
zápisu	zápis	k1gInSc2
konstrukce	konstrukce	k1gFnSc2
vyjadřujeme	vyjadřovat	k5eAaImIp1nP
v	v	k7c6
interaktivním	interaktivní	k2eAgInSc6d1
režimu	režim	k1gInSc6
zadáním	zadání	k1gNnSc7
prázdného	prázdný	k2eAgInSc2d1
řádku	řádek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
>>>	>>>	k?
def	def	k?
f	f	k?
<g/>
(	(	kIx(
<g/>
c	c	k0
<g/>
,	,	kIx,
n	n	k0
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
...	...	k?
return	returna	k1gFnPc2
c	c	k0
*	*	kIx~
n	n	k0
</s>
<s>
...	...	k?
</s>
<s>
>>>	>>>	k?
f	f	k?
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
,	,	kIx,
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
15	#num#	k4
</s>
<s>
V	v	k7c6
interaktivním	interaktivní	k2eAgInSc6d1
režimu	režim	k1gInSc6
většinou	většinou	k6eAd1
nepoužíváme	používat	k5eNaImIp1nP
příkaz	příkaz	k1gInSc4
print	print	k1gInSc1
(	(	kIx(
<g/>
ale	ale	k8xC
nic	nic	k3yNnSc1
nám	my	k3xPp1nPc3
v	v	k7c6
tom	ten	k3xDgNnSc6
nebrání	bránit	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
chceme	chtít	k5eAaImIp1nP
zobrazit	zobrazit	k5eAaPmF
obsah	obsah	k1gInSc4
proměnné	proměnná	k1gFnSc2
<g/>
,	,	kIx,
stačí	stačit	k5eAaBmIp3nS
za	za	k7c4
úvodní	úvodní	k2eAgInPc4d1
znaky	znak	k1gInPc4
zapsat	zapsat	k5eAaPmF
její	její	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
>>>	>>>	k?
a	a	k8xC
=	=	kIx~
1	#num#	k4
+	+	kIx~
2	#num#	k4
</s>
<s>
>>>	>>>	k?
a	a	k8xC
</s>
<s>
3	#num#	k4
</s>
<s>
Proměnná	proměnná	k1gFnSc1
_	_	kIx~
obsahuje	obsahovat	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc4d1
takto	takto	k6eAd1
použitou	použitý	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
>>>	>>>	k?
f	f	k?
<g/>
(	(	kIx(
<g/>
'	'	kIx"
<g/>
x	x	k?
<g/>
'	'	kIx"
<g/>
,	,	kIx,
a	a	k8xC
<g/>
)	)	kIx)
</s>
<s>
'	'	kIx"
<g/>
xxx	xxx	k?
<g/>
'	'	kIx"
</s>
<s>
>>>	>>>	k?
len	len	k1gInSc4
<g/>
(	(	kIx(
<g/>
_	_	kIx~
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
Python	Python	k1gMnSc1
3.0	3.0	k4
</s>
<s>
Python	Python	k1gMnSc1
je	být	k5eAaImIp3nS
vyvíjen	vyvíjet	k5eAaImNgInS
s	s	k7c7
důrazem	důraz	k1gInSc7
na	na	k7c4
pragmatičnost	pragmatičnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
vývoj	vývoj	k1gInSc1
jeho	jeho	k3xOp3gFnPc2
verzí	verze	k1gFnPc2
je	být	k5eAaImIp3nS
spíše	spíše	k9
evoluční	evoluční	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přirozeným	přirozený	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
takového	takový	k3xDgInSc2
přístupu	přístup	k1gInSc2
je	být	k5eAaImIp3nS
i	i	k9
zpětné	zpětný	k2eAgNnSc1d1
hodnocení	hodnocení	k1gNnSc1
dobrých	dobrý	k2eAgFnPc2d1
a	a	k8xC
horších	zlý	k2eAgFnPc2d2
vlastností	vlastnost	k1gFnPc2
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
výsledkem	výsledek	k1gInSc7
byl	být	k5eAaImAgInS
projekt	projekt	k1gInSc1
Python	Python	k1gMnSc1
3000	#num#	k4
(	(	kIx(
<g/>
Py	Py	k1gFnSc6
<g/>
3	#num#	k4
<g/>
k	k	k7c3
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
základ	základ	k1gInSc4
vývoje	vývoj	k1gInSc2
přelomové	přelomový	k2eAgFnSc2d1
verze	verze	k1gFnSc2
Python	Python	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stabilní	stabilní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Python	Python	k1gMnSc1
3.0	3.0	k4
byla	být	k5eAaImAgFnS
vypuštěna	vypustit	k5eAaPmNgFnS
v	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zpětně	zpětně	k6eAd1
nekompatibilní	kompatibilní	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přechodovou	přechodový	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
mezi	mezi	k7c7
Python	Python	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
a	a	k8xC
Python	Python	k1gMnSc1
3.0	3.0	k4
představuje	představovat	k5eAaImIp3nS
Python	Python	k1gMnSc1
2.7	2.7	k4
(	(	kIx(
<g/>
varování	varování	k1gNnSc1
při	při	k7c6
použití	použití	k1gNnSc6
syntaxe	syntax	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nebude	být	k5eNaImBp3nS
ve	v	k7c6
verzi	verze	k1gFnSc6
3.0	3.0	k4
platná	platný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byly	být	k5eAaImAgInP
vyvinuty	vyvinout	k5eAaPmNgInP
nástroje	nástroj	k1gInPc1
pro	pro	k7c4
usnadnění	usnadnění	k1gNnSc4
konverze	konverze	k1gFnSc2
starších	starý	k2eAgInPc2d2
zdrojových	zdrojový	k2eAgInPc2d1
textů	text	k1gInPc2
do	do	k7c2
podoby	podoba	k1gFnSc2
pro	pro	k7c4
verzi	verze	k1gFnSc4
Python	Python	k1gMnSc1
3.0	3.0	k4
<g/>
.	.	kIx.
</s>
<s>
Jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejviditelnějších	viditelný	k2eAgFnPc2d3
změn	změna	k1gFnPc2
v	v	k7c6
Python	Python	k1gMnSc1
3.0	3.0	k4
(	(	kIx(
<g/>
pragmatický	pragmatický	k2eAgInSc4d1
pohled	pohled	k1gInSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
prostého	prostý	k2eAgMnSc2d1
uživatele	uživatel	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
převedení	převedení	k1gNnSc1
příkazu	příkaz	k1gInSc2
print	printa	k1gFnPc2
na	na	k7c4
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstrukce	konstrukce	k1gFnSc1
print	printa	k1gFnPc2
"	"	kIx"
<g/>
hello	hello	k1gNnSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
neplatná	platný	k2eNgFnSc1d1
<g/>
,	,	kIx,
správný	správný	k2eAgInSc1d1
zápis	zápis	k1gInSc1
je	být	k5eAaImIp3nS
print	print	k1gInSc4
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
hello	hello	k1gNnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Velmi	velmi	k6eAd1
významnou	významný	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
je	být	k5eAaImIp3nS
důsledné	důsledný	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
abstrakcí	abstrakce	k1gFnPc2
řetězec	řetězec	k1gInSc4
a	a	k8xC
posloupnost	posloupnost	k1gFnSc4
bajtů	bajt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řetězce	řetězec	k1gInSc2
se	se	k3xPyFc4
důsledně	důsledně	k6eAd1
změnily	změnit	k5eAaPmAgInP
na	na	k7c4
typ	typ	k1gInSc4
unicode	unicod	k1gInSc5
(	(	kIx(
<g/>
"	"	kIx"
<g/>
hruška	hruška	k1gFnSc1
<g/>
"	"	kIx"
bude	být	k5eAaImBp3nS
ekvivalentní	ekvivalentní	k2eAgFnSc1d1
s	s	k7c7
bývalou	bývalý	k2eAgFnSc7d1
u	u	k7c2
<g/>
"	"	kIx"
<g/>
hruška	hruška	k1gFnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
posloupnosti	posloupnost	k1gFnPc4
bajtů	bajt	k1gInPc2
je	být	k5eAaImIp3nS
zaveden	zavést	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
typ	typ	k1gInSc1
bytes	bytesa	k1gFnPc2
(	(	kIx(
<g/>
immutable	immutable	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
bytearray	bytearra	k2eAgFnPc1d1
(	(	kIx(
<g/>
mutable	mutable	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vzájemném	vzájemný	k2eAgInSc6d1
převodu	převod	k1gInSc6
mezi	mezi	k7c7
řetězcem	řetězec	k1gInSc7
a	a	k8xC
posloupností	posloupnost	k1gFnSc7
bajtů	bajt	k1gInPc2
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
vždy	vždy	k6eAd1
uvádět	uvádět	k5eAaImF
požadované	požadovaný	k2eAgNnSc4d1
kódování	kódování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
vést	vést	k5eAaImF
k	k	k7c3
důslednému	důsledný	k2eAgNnSc3d1
vyřešení	vyřešení	k1gNnSc3
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
projevovaly	projevovat	k5eAaImAgInP
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
znaky	znak	k1gInPc7
národních	národní	k2eAgFnPc2d1
abeced	abeceda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
raw_input	raw_input	k1gInSc1
byla	být	k5eAaImAgFnS
přejmenována	přejmenovat	k5eAaPmNgFnS
na	na	k7c4
input	input	k1gInSc4
(	(	kIx(
<g/>
bývalý	bývalý	k2eAgMnSc1d1
input	input	k1gMnSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
zmizí	zmizet	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
také	také	k9
iterativní	iterativní	k2eAgFnPc4d1
varianty	varianta	k1gFnPc4
funkcí	funkce	k1gFnPc2
a	a	k8xC
metod	metoda	k1gFnPc2
nahradily	nahradit	k5eAaPmAgFnP
svoje	svůj	k3xOyFgMnPc4
předchůdce	předchůdce	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
vracely	vracet	k5eAaImAgInP
seznamy	seznam	k1gInPc1
<g/>
,	,	kIx,
takže	takže	k8xS
například	například	k6eAd1
funkce	funkce	k1gFnSc1
range	rang	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
bývalá	bývalý	k2eAgFnSc1d1
xrange	xrange	k1gFnSc1
<g/>
,	,	kIx,
analogicky	analogicky	k6eAd1
tomu	ten	k3xDgMnSc3
je	být	k5eAaImIp3nS
s	s	k7c7
file	file	k1gNnSc7
<g/>
.	.	kIx.
<g/>
readlines	readlines	k1gMnSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
file	file	k1gInSc1
<g/>
.	.	kIx.
<g/>
xreadlines	xreadlines	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
další	další	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
dřív	dříve	k6eAd2
vraceli	vracet	k5eAaImAgMnP
list	list	k1gInSc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
vrací	vracet	k5eAaImIp3nS
iterátor	iterátor	k1gInSc4
–	–	k?
např.	např.	kA
dict	dict	k1gInSc1
<g/>
.	.	kIx.
<g/>
keys	keys	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
#	#	kIx~
python	python	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
>>>	>>>	k?
d	d	k?
=	=	kIx~
{	{	kIx(
'	'	kIx"
<g/>
a	a	k8xC
<g/>
'	'	kIx"
<g/>
:	:	kIx,
1	#num#	k4
<g/>
,	,	kIx,
'	'	kIx"
<g/>
b	b	k?
<g/>
'	'	kIx"
<g/>
:	:	kIx,
7	#num#	k4
}	}	kIx)
</s>
<s>
>>>	>>>	k?
d.	d.	k?
<g/>
keys	keys	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
'	'	kIx"
<g/>
,	,	kIx,
'	'	kIx"
<g/>
b	b	k?
<g/>
'	'	kIx"
<g/>
]	]	kIx)
</s>
<s>
#	#	kIx~
python	python	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
>>>	>>>	k?
d.	d.	k?
<g/>
keys	keys	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
</s>
<s>
<	<	kIx(
<g/>
dict_keys	dict_keysit	k5eAaPmRp2nS
object	object	k1gInSc1
at	at	k?
0	#num#	k4
<g/>
xb	xb	k?
<g/>
7	#num#	k4
<g/>
c	c	k0
<g/>
0	#num#	k4
<g/>
8540	#num#	k4
<g/>
&	&	k?
<g/>
>	>	kIx)
</s>
<s>
>>>	>>>	k?
[	[	kIx(
x	x	k?
for	forum	k1gNnPc2
x	x	k?
in	in	k?
d.	d.	k?
<g/>
keys	keys	k1gInSc1
<g/>
(	(	kIx(
<g/>
)	)	kIx)
]	]	kIx)
#	#	kIx~
nebo	nebo	k8xC
list	list	k1gInSc1
<g/>
(	(	kIx(
<g/>
d.	d.	k?
<g/>
keys	keys	k1gInSc1
<g/>
(	(	kIx(
<g/>
))	))	k?
</s>
<s>
[	[	kIx(
<g/>
'	'	kIx"
<g/>
a	a	k8xC
<g/>
'	'	kIx"
<g/>
,	,	kIx,
'	'	kIx"
<g/>
b	b	k?
<g/>
'	'	kIx"
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc7d1
viditelnější	viditelný	k2eAgFnSc7d2
změnou	změna	k1gFnSc7
je	být	k5eAaImIp3nS
chování	chování	k1gNnSc1
operátoru	operátor	k1gInSc2
dělení	dělení	k1gNnSc2
/	/	kIx~
při	při	k7c6
použití	použití	k1gNnSc6
s	s	k7c7
celými	celý	k2eAgNnPc7d1
čísly	číslo	k1gNnPc7
<g/>
:	:	kIx,
</s>
<s>
#	#	kIx~
python	python	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
>>>	>>>	k?
5	#num#	k4
/	/	kIx~
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
>>>	>>>	k?
5	#num#	k4
/	/	kIx~
2	#num#	k4
==	==	k?
5	#num#	k4
//	//	k?
2	#num#	k4
</s>
<s>
True	True	k6eAd1
</s>
<s>
#	#	kIx~
python	python	k1gMnSc1
3	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
>>>	>>>	k?
5	#num#	k4
/	/	kIx~
2	#num#	k4
</s>
<s>
2.5	2.5	k4
</s>
<s>
>>>	>>>	k?
5	#num#	k4
//	//	k?
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Zmizí	zmizet	k5eAaPmIp3nS
dict	dict	k5eAaPmF
<g/>
.	.	kIx.
<g/>
has_key	has_key	k1gInPc4
<g/>
(	(	kIx(
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zápis	zápis	k1gInSc1
množin	množina	k1gFnPc2
se	se	k3xPyFc4
zjednodušil	zjednodušit	k5eAaPmAgMnS
<g/>
,	,	kIx,
set	set	k1gInSc4
<g/>
(	(	kIx(
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
napsat	napsat	k5eAaBmF,k5eAaPmF
{	{	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
například	například	k6eAd1
přibyla	přibýt	k5eAaPmAgFnS
řetězcům	řetězec	k1gInPc3
nová	nový	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
format	format	k1gInSc1
jako	jako	k8xS,k8xC
alternativa	alternativa	k1gFnSc1
ke	k	k7c3
starému	starý	k2eAgNnSc3d1
formátování	formátování	k1gNnSc3
řetězců	řetězec	k1gInPc2
procentovou	procentový	k2eAgFnSc7d1
notací	notace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
foo	foo	k?
<g/>
=	=	kIx~
<g/>
7	#num#	k4
#	#	kIx~
<g/>
společné	společný	k2eAgNnSc1d1
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
verze	verze	k1gFnPc4
</s>
<s>
#	#	kIx~
<g/>
python	python	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
x	x	k?
</s>
<s>
"	"	kIx"
<g/>
ID	ido	k1gNnPc2
<g/>
:	:	kIx,
%	%	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
%	%	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
%	%	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
"	"	kIx"
%	%	kIx~
(	(	kIx(
<g/>
"	"	kIx"
<g/>
0	#num#	k4
<g/>
s	s	k7c7
<g/>
9	#num#	k4
<g/>
d	d	k?
<g/>
8	#num#	k4
<g/>
f	f	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
foo	foo	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
#	#	kIx~
či	či	k8xC
</s>
<s>
"	"	kIx"
<g/>
ID	ido	k1gNnPc2
<g/>
:	:	kIx,
%	%	kIx~
<g/>
(	(	kIx(
<g/>
id	idy	k1gFnPc2
<g/>
)	)	kIx)
<g/>
s	s	k7c7
(	(	kIx(
<g/>
%	%	kIx~
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
s	s	k7c7
<g/>
,	,	kIx,
%	%	kIx~
<g/>
(	(	kIx(
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
"	"	kIx"
%	%	kIx~
{	{	kIx(
<g/>
'	'	kIx"
<g/>
x	x	k?
<g/>
'	'	kIx"
<g/>
:	:	kIx,
foo	foo	k?
<g/>
,	,	kIx,
'	'	kIx"
<g/>
y	y	k?
<g/>
'	'	kIx"
<g/>
:	:	kIx,
3	#num#	k4
<g/>
,	,	kIx,
'	'	kIx"
<g/>
id	idy	k1gFnPc2
<g/>
'	'	kIx"
<g/>
:	:	kIx,
"	"	kIx"
<g/>
0	#num#	k4
<g/>
s	s	k7c7
<g/>
9	#num#	k4
<g/>
d	d	k?
<g/>
8	#num#	k4
<g/>
f	f	k?
<g/>
"	"	kIx"
<g/>
}	}	kIx)
</s>
<s>
#	#	kIx~
<g/>
python	python	k1gMnSc1
3.0	3.0	k4
přidává	přidávat	k5eAaImIp3nS
také	také	k9
notaci	notace	k1gFnSc4
</s>
<s>
"	"	kIx"
<g/>
ID	ido	k1gNnPc2
<g/>
:	:	kIx,
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
(	(	kIx(
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
,	,	kIx,
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
format	format	k5eAaPmF
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
0	#num#	k4
<g/>
s	s	k7c7
<g/>
9	#num#	k4
<g/>
d	d	k?
<g/>
8	#num#	k4
<g/>
f	f	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
foo	foo	k?
<g/>
,	,	kIx,
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
#	#	kIx~
a	a	k8xC
</s>
<s>
"	"	kIx"
<g/>
ID	ido	k1gNnPc2
<g/>
:	:	kIx,
{	{	kIx(
<g/>
id	idy	k1gFnPc2
<g/>
}	}	kIx)
(	(	kIx(
<g/>
{	{	kIx(
<g/>
x	x	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
{	{	kIx(
<g/>
y	y	k?
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
format	format	k1gInSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
=	=	kIx~
<g/>
foo	foo	k?
<g/>
,	,	kIx,
y	y	k?
<g/>
=	=	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
id	idy	k1gFnPc2
<g/>
=	=	kIx~
<g/>
"	"	kIx"
<g/>
0	#num#	k4
<g/>
s	s	k7c7
<g/>
9	#num#	k4
<g/>
d	d	k?
<g/>
8	#num#	k4
<g/>
f	f	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
</s>
<s>
#	#	kIx~
od	od	k7c2
verze	verze	k1gFnSc2
3.6	3.6	k4
zle	zle	k6eAd1
navíc	navíc	k6eAd1
použít	použít	k5eAaPmF
takzvané	takzvaný	k2eAgNnSc4d1
f-string	f-string	k1gInSc1
</s>
<s>
f	f	k?
<g/>
"	"	kIx"
<g/>
ID	Ida	k1gFnPc2
<g/>
:	:	kIx,
0	#num#	k4
<g/>
s	s	k7c7
<g/>
9	#num#	k4
<g/>
d	d	k?
<g/>
8	#num#	k4
<g/>
f	f	k?
(	(	kIx(
<g/>
{	{	kIx(
<g/>
foo	foo	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
"	"	kIx"
</s>
<s>
Dalším	další	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
porušení	porušení	k1gNnSc2
zpětné	zpětný	k2eAgFnSc2d1
kompatibility	kompatibilita	k1gFnSc2
je	být	k5eAaImIp3nS
zavedení	zavedení	k1gNnSc1
nové	nový	k2eAgFnSc2d1
syntaxe	syntax	k1gFnSc2
pro	pro	k7c4
oktalová	oktalový	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
dříve	dříve	k6eAd2
zapisovala	zapisovat	k5eAaImAgNnP
s	s	k7c7
nulou	nula	k1gFnSc7
na	na	k7c6
začátku	začátek	k1gInSc6
<g/>
,	,	kIx,
např.	např.	kA
0	#num#	k4
<g/>
777	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
formát	formát	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
neplatným	platný	k2eNgMnSc7d1
a	a	k8xC
nový	nový	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
analogický	analogický	k2eAgInSc1d1
se	s	k7c7
zápisem	zápis	k1gInSc7
hexadecimálních	hexadecimální	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
x	x	k?
<g/>
1	#num#	k4
<g/>
ff	ff	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správný	správný	k2eAgInSc1d1
zápis	zápis	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
0	#num#	k4
<g/>
o	o	k7c4
<g/>
777	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zapisovat	zapisovat	k5eAaImF
i	i	k8xC
čísla	číslo	k1gNnPc4
v	v	k7c6
binární	binární	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
>>>	>>>	k?
012	#num#	k4
</s>
<s>
File	File	k1gFnSc1
"	"	kIx"
<g/>
<stdin>
"	"	kIx"
<g/>
,	,	kIx,
line	linout	k5eAaImIp3nS
1	#num#	k4
</s>
<s>
012	#num#	k4
</s>
<s>
^	^	kIx~
</s>
<s>
SyntaxError	SyntaxError	k1gInSc1
<g/>
:	:	kIx,
invalid	invalid	k1gInSc1
token	tokna	k1gFnPc2
</s>
<s>
>>>	>>>	k?
10	#num#	k4
==	==	k?
0	#num#	k4
<g/>
xa	xa	k?
==	==	k?
0	#num#	k4
<g/>
o	o	k7c4
<g/>
12	#num#	k4
==	==	k?
0	#num#	k4
<g/>
b	b	k?
<g/>
1010	#num#	k4
</s>
<s>
True	True	k6eAd1
</s>
<s>
Podrobnější	podrobný	k2eAgInSc4d2
seznam	seznam	k1gInSc4
změn	změna	k1gFnPc2
najdete	najít	k5eAaPmIp2nP
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
dokumentu	dokument	k1gInSc6
What	Whata	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
New	New	k1gMnSc1
In	In	k1gMnSc1
Python	Python	k1gMnSc1
3.0	3.0	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VAN	van	k1gInSc1
ROSSUM	ROSSUM	kA
<g/>
,	,	kIx,
Guido	Guido	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
-	-	kIx~
zdrojové	zdrojový	k2eAgInPc4d1
kódy	kód	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Python	Python	k1gMnSc1
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
http://svn.python.org/view/*checkout*/python/trunk/Misc/HISTORY.	http://svn.python.org/view/*checkout*/python/trunk/Misc/HISTORY.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KRČMÁŘ	Krčmář	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chyby	Chyba	k1gMnPc7
v	v	k7c6
programovacích	programovací	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
ohrožují	ohrožovat	k5eAaImIp3nP
bezpečnost	bezpečnost	k1gFnSc4
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
root	root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://shootout.alioth.debian.org/gp4sandbox/benchmark.php?test=all&	http://shootout.alioth.debian.org/gp4sandbox/benchmark.php?test=all&	k?
<g/>
↑	↑	k?
http://www.artima.com/weblogs/viewpost.jsp?thread=208549	http://www.artima.com/weblogs/viewpost.jsp?thread=208549	k4
<g/>
↑	↑	k?
http://www.python.org/dev/peps/pep-3000/	http://www.python.org/dev/peps/pep-3000/	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Python	Python	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
www.python.org	www.python.org	k1gInSc1
–	–	k?
oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
projektu	projekt	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
část	část	k1gFnSc1
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
python	python	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
–	–	k?
Python	Python	k1gMnSc1
CZ	CZ	kA
<g/>
,	,	kIx,
rozcestník	rozcestník	k1gInSc1
české	český	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
</s>
<s>
www.py.cz	www.py.cz	k1gMnSc1
–	–	k?
PyCZ	PyCZ	k1gMnSc1
<g/>
,	,	kIx,
komunitní	komunitní	k2eAgInSc1d1
český	český	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
Boost	Boost	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Python	Python	k1gMnSc1
–	–	k?
C	C	kA
<g/>
++	++	k?
knihovna	knihovna	k1gFnSc1
pro	pro	k7c4
spolupráci	spolupráce	k1gFnSc4
mezi	mezi	k7c7
C	C	kA
<g/>
++	++	k?
a	a	k8xC
Pythonem	Python	k1gMnSc7
</s>
<s>
Dokumentace	dokumentace	k1gFnSc1
<g/>
,	,	kIx,
učebnice	učebnice	k1gFnPc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
tutorial	tutorial	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
dokumentace	dokumentace	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Učebnice	učebnice	k1gFnPc1
jazyka	jazyk	k1gInSc2
Python	Python	k1gMnSc1
(	(	kIx(
<g/>
aneb	aneb	k?
Létající	létající	k2eAgInSc1d1
cirkus	cirkus	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Létající	létající	k2eAgInSc1d1
cirkus	cirkus	k1gInSc1
<g/>
,	,	kIx,
PDF	PDF	kA
–	–	k?
seriál	seriál	k1gInSc1
na	na	k7c4
ROOT	ROOT	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
,	,	kIx,
21	#num#	k4
dílů	díl	k1gInPc2
</s>
<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
naučit	naučit	k5eAaPmF
programovat	programovat	k5eAaImF
–	–	k?
Alan	Alan	k1gMnSc1
Gauld	Gauld	k1gMnSc1
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Python	Python	k1gMnSc1
–	–	k?
programování	programování	k1gNnSc2
zábavou	zábava	k1gFnSc7
–	–	k?
český	český	k2eAgInSc4d1
web	web	k1gInSc4
o	o	k7c6
Pythonu	Python	k1gMnSc6
<g/>
,	,	kIx,
základy	základ	k1gInPc1
<g/>
,	,	kIx,
ukázky	ukázka	k1gFnPc1
<g/>
,	,	kIx,
praxe	praxe	k1gFnPc1
</s>
<s>
Ponořme	ponořit	k5eAaPmRp1nP
se	se	k3xPyFc4
do	do	k7c2
Pythonu	Python	k1gMnSc3
3	#num#	k4
–	–	k?
Mark	Mark	k1gMnSc1
Pilgrim	Pilgrima	k1gFnPc2
(	(	kIx(
<g/>
český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
z	z	k7c2
originálního	originální	k2eAgInSc2d1
Dive	div	k1gInSc5
Into	Inta	k1gFnSc5
Python	Python	k1gMnSc1
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
vydáno	vydán	k2eAgNnSc1d1
též	též	k9
v	v	k7c6
edici	edice	k1gFnSc6
CZ	CZ	kA
<g/>
.	.	kIx.
<g/>
NIC	nic	k9
–	–	k?
knižně	knižně	k6eAd1
i	i	k9
elektronicky	elektronicky	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Programovací	programovací	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
abecední	abecední	k2eAgInSc1d1
seznam	seznam	k1gInSc4
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
multiparadigmatické	multiparadigmatický	k2eAgFnSc2d1
</s>
<s>
Ada	Ada	kA
•	•	k?
C	C	kA
<g/>
++	++	k?
•	•	k?
Common	Common	k1gInSc1
Lisp	Lisp	k1gInSc1
•	•	k?
D	D	kA
•	•	k?
F	F	kA
<g/>
#	#	kIx~
•	•	k?
Go	Go	k1gMnSc1
•	•	k?
Oberon	Oberon	k1gMnSc1
•	•	k?
Perl	perl	k1gInSc1
•	•	k?
PHP	PHP	kA
•	•	k?
Python	Python	k1gMnSc1
•	•	k?
Ruby	rub	k1gInPc1
•	•	k?
Rust	Rust	k1gInSc1
•	•	k?
Scala	scát	k5eAaImAgFnS
•	•	k?
Swift	Swift	k1gMnSc1
•	•	k?
Tcl	Tcl	k1gMnSc1
(	(	kIx(
<g/>
Tk	Tk	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Vala	Vala	k1gMnSc1
strukturované	strukturovaný	k2eAgFnPc4d1
(	(	kIx(
<g/>
procedurální	procedurální	k2eAgFnPc4d1
<g/>
)	)	kIx)
</s>
<s>
AWK	AWK	kA
•	•	k?
C	C	kA
•	•	k?
COBOL	Cobol	kA
•	•	k?
DCL	DCL	kA
•	•	k?
Forth	Forth	k1gInSc1
•	•	k?
Fortran	Fortran	kA
•	•	k?
Lua	Lua	k1gMnSc1
•	•	k?
Modula-	Modula-	k1gMnSc1
<g/>
2	#num#	k4
/	/	kIx~
Modula-	Modula-	k1gFnSc1
<g/>
3	#num#	k4
•	•	k?
Pascal	pascal	k1gInSc1
•	•	k?
Pawn	Pawn	k1gInSc1
•	•	k?
PL	PL	kA
<g/>
/	/	kIx~
<g/>
SQL	SQL	kA
objektové	objektový	k2eAgInPc1d1
</s>
<s>
BETA	beta	k1gNnSc1
•	•	k?
Boo	boa	k1gFnSc5
•	•	k?
C	C	kA
<g/>
#	#	kIx~
•	•	k?
Eiffel	Eiffel	k1gMnSc1
•	•	k?
Java	Java	k1gMnSc1
(	(	kIx(
<g/>
Groovy	Groov	k1gInPc1
<g/>
,	,	kIx,
Kotlin	kotlina	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
JavaScript	JavaScript	k1gMnSc1
•	•	k?
Object	Object	k1gMnSc1
Pascal	Pascal	k1gMnSc1
•	•	k?
Objective-C	Objective-C	k1gMnSc1
•	•	k?
Smalltalk	Smalltalk	k1gMnSc1
•	•	k?
VB	VB	kA
<g/>
.	.	kIx.
<g/>
NET	NET	kA
funkcionální	funkcionální	k2eAgInSc1d1
</s>
<s>
Clean	Clean	k1gInSc1
•	•	k?
Ceylon	Ceylon	k1gInSc1
•	•	k?
Erlang	Erlang	k1gInSc1
•	•	k?
Haskell	Haskell	k1gInSc1
•	•	k?
J	J	kA
•	•	k?
Lisp	Lisp	k1gMnSc1
•	•	k?
Mathematica	Mathematica	k1gMnSc1
•	•	k?
Miranda	Miranda	k1gFnSc1
•	•	k?
OCaml	OCaml	k1gInSc1
•	•	k?
Scheme	Schem	k1gInSc5
dotazovací	dotazovací	k2eAgInPc1d1
</s>
<s>
LINQ	LINQ	kA
•	•	k?
SPARQL	SPARQL	kA
•	•	k?
SQL	SQL	kA
•	•	k?
XPath	XPath	k1gInSc1
logické	logický	k2eAgInPc1d1
</s>
<s>
Gödel	Gödel	k1gInSc1
•	•	k?
Prolog	prolog	k1gInSc1
výukové	výukový	k2eAgInPc1d1
</s>
<s>
Baltazar	Baltazar	k1gMnSc1
•	•	k?
Baltík	Baltík	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
•	•	k?
Kodu	Kodus	k1gInSc2
Game	game	k1gInSc1
Lab	Lab	k1gFnSc1
•	•	k?
Logo	logo	k1gNnSc1
•	•	k?
Microsoft	Microsoft	kA
Small	Small	k1gInSc4
Basic	Basic	kA
•	•	k?
Petr	Petr	k1gMnSc1
•	•	k?
Scratch	Scratch	k1gMnSc1
ezoterické	ezoterický	k2eAgInPc4d1
</s>
<s>
Befunge	Befunge	k1gFnSc1
•	•	k?
Brainfuck	Brainfuck	k1gInSc1
•	•	k?
HQ	HQ	kA
<g/>
9	#num#	k4
<g/>
+	+	kIx~
•	•	k?
Malbolge	Malbolge	k1gInSc1
•	•	k?
Ook	Ook	k1gFnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
Piet	pieta	k1gFnPc2
•	•	k?
Whitespace	Whitespace	k1gFnSc2
historické	historický	k2eAgFnSc2d1
</s>
<s>
ALGOL	Algol	k1gInSc1
•	•	k?
APL	APL	kA
•	•	k?
B	B	kA
•	•	k?
BASIC	Basic	kA
•	•	k?
CPL	CPL	kA
(	(	kIx(
<g/>
BCPL	BCPL	kA
<g/>
)	)	kIx)
•	•	k?
J	J	kA
•	•	k?
MUMPS	MUMPS	kA
•	•	k?
PL	PL	kA
<g/>
/	/	kIx~
<g/>
I	i	k9
•	•	k?
Simula	Simula	k1gFnSc1
67	#num#	k4
•	•	k?
SNOBOL	SNOBOL	kA
další	další	k2eAgFnSc1d1
</s>
<s>
ABAP	ABAP	kA
•	•	k?
AppleScript	AppleScript	k1gInSc1
•	•	k?
ColdFusion	ColdFusion	k1gInSc1
•	•	k?
JSA	být	k5eAaImSgInS
•	•	k?
Julia	Julius	k1gMnSc2
•	•	k?
MATLAB	MATLAB	kA
•	•	k?
R	R	kA
•	•	k?
Visual	Visual	k1gInSc1
Basic	Basic	kA
(	(	kIx(
<g/>
VBScript	VBScript	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Vimscript	Vimscript	k1gMnSc1
•	•	k?
Visual	Visual	k1gMnSc1
FoxPro	FoxPro	k1gNnSc1
•	•	k?
XSLT	XSLT	kA
skriptovací	skriptovací	k2eAgMnSc1d1
<g/>
/	/	kIx~
<g/>
strojový	strojový	k2eAgInSc1d1
kód	kód	k1gInSc1
•	•	k?
kompilované	kompilovaný	k2eAgFnPc4d1
<g/>
/	/	kIx~
<g/>
interpretované	interpretovaný	k2eAgFnPc4d1
•	•	k?
interaktivní	interaktivní	k2eAgFnPc4d1
<g/>
/	/	kIx~
<g/>
dávkové	dávkový	k2eAgFnPc4d1
•	•	k?
WYSIWYG	WYSIWYG	kA
</s>
<s>
Python	Python	k1gMnSc1
Implementace	implementace	k1gFnSc2
</s>
<s>
ChinesePython	ChinesePython	k1gMnSc1
</s>
<s>
CLPython	CLPython	k1gMnSc1
</s>
<s>
CPython	CPython	k1gMnSc1
</s>
<s>
JPype	JPypat	k5eAaPmIp3nS
</s>
<s>
Jython	Jython	k1gMnSc1
</s>
<s>
IronPython	IronPython	k1gMnSc1
</s>
<s>
PyPy	PyPa	k1gFnPc1
</s>
<s>
Psyco	Psyco	k1gMnSc1
Webové	webový	k2eAgFnSc2d1
frameworky	frameworka	k1gFnSc2
</s>
<s>
CherryPy	CherryPa	k1gFnPc1
</s>
<s>
Django	Django	k6eAd1
</s>
<s>
Flask	Flask	k1gInSc1
</s>
<s>
Pylons	Pylons	k6eAd1
</s>
<s>
Pyramid	pyramid	k1gInSc1
</s>
<s>
Quixote	Quixot	k1gMnSc5
</s>
<s>
Tornádo	tornádo	k1gNnSc1
</s>
<s>
Tryton	Tryton	k1gInSc1
</s>
<s>
TurboGears	TurboGears	k6eAd1
</s>
<s>
Twisted	Twisted	k1gMnSc1
</s>
<s>
Web	web	k1gInSc1
<g/>
2	#num#	k4
<g/>
py	py	k?
</s>
<s>
Zope	Zope	k1gFnSc1
IDE	IDE	kA
</s>
<s>
Boa	boa	k1gNnSc1
konstruktor	konstruktor	k1gMnSc1
</s>
<s>
Eclipse	Eclipse	k1gFnSc1
+	+	kIx~
PyDev	PyDev	k1gFnSc1
</s>
<s>
Eric	Eric	k6eAd1
</s>
<s>
Geany	Geana	k1gFnPc1
</s>
<s>
IDLE	IDLE	kA
</s>
<s>
Komodo	komoda	k1gFnSc5
</s>
<s>
NetBeans	NetBeans	k6eAd1
</s>
<s>
PyCharm	PyCharm	k1gInSc1
</s>
<s>
PyScripter	PyScripter	k1gMnSc1
</s>
<s>
Visual	Visual	k1gInSc1
Studio	studio	k1gNnSc1
+	+	kIx~
PTVS	PTVS	kA
Knihovny	knihovna	k1gFnSc2
</s>
<s>
Databáze	databáze	k1gFnPc1
</s>
<s>
SQLAlchemy	SQLAlchem	k1gInPc1
GUI	GUI	kA
</s>
<s>
PyGTK	PyGTK	k?
</s>
<s>
PyQt	PyQt	k1gMnSc1
</s>
<s>
PythonQt	PythonQt	k1gMnSc1
</s>
<s>
PySide	PySid	k1gMnSc5
</s>
<s>
WxPython	WxPython	k1gMnSc1
</s>
<s>
Tkinter	Tkinter	k1gMnSc1
</s>
<s>
Rapyd-Tk	Rapyd-Tk	k6eAd1
Zpracování	zpracování	k1gNnSc1
obrazu	obraz	k1gInSc2
</s>
<s>
PIL	pít	k5eAaImAgInS
Herní	herní	k2eAgInSc1d1
</s>
<s>
Pygame	Pygam	k1gInSc5
</s>
<s>
PySDL	PySDL	k?
</s>
<s>
Cocos	Cocos	k1gInSc1
<g/>
2	#num#	k4
<g/>
d	d	k?
Vědecké	vědecký	k2eAgInPc1d1
</s>
<s>
PyNGL	PyNGL	k?
</s>
<s>
PyNIO	PyNIO	k?
</s>
<s>
Python	Python	k1gMnSc1
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
</s>
<s>
Matplotlib	Matplotlib	k1gMnSc1
</s>
<s>
NLTK	NLTK	kA
</s>
<s>
NumPy	NumPa	k1gFnPc1
</s>
<s>
Scipy	Scip	k1gInPc1
Analýza	analýza	k1gFnSc1
dat	datum	k1gNnPc2
</s>
<s>
Beautiful	Beautiful	k1gInSc1
Soup	Soup	k1gInSc1
</s>
<s>
PyParsing	PyParsing	k1gInSc1
Nasazení	nasazení	k1gNnSc1
</s>
<s>
Py	Py	k?
<g/>
2	#num#	k4
<g/>
exe	exe	k?
</s>
<s>
Py	Py	k?
<g/>
2	#num#	k4
<g/>
App	App	k1gFnSc1
</s>
<s>
Cx	Cx	k?
Freeze	Freeze	k1gFnSc1
</s>
<s>
PyInstaller	PyInstaller	k1gMnSc1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
ActivePython	ActivePython	k1gMnSc1
</s>
<s>
PyPI	PyPI	k?
</s>
<s>
Python	Python	k1gMnSc1
Software	software	k1gInSc1
Foundation	Foundation	k1gInSc4
</s>
<s>
Python	Python	k1gMnSc1
licence	licence	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4434275-5	4434275-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
13861	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
96008834	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
96008834	#num#	k4
</s>
