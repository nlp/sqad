<s>
Python	Python	k1gInSc1
je	být	k5eAaImIp3nS
hybridní	hybridní	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
také	také	k9
multiparadigmatický	multiparadigmatický	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
umožňuje	umožňovat	k5eAaImIp3nS
při	při	k7c6
psaní	psaní	k1gNnSc6
programů	program	k1gInPc2
používat	používat	k5eAaImF
nejen	nejen	k6eAd1
objektově	objektově	k6eAd1
orientované	orientovaný	k2eAgNnSc4d1
paradigma	paradigma	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
procedurální	procedurální	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
i	i	k9
funkcionální	funkcionální	k2eAgInSc1d1
<g/>
,	,	kIx,
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
komu	kdo	k3yInSc3,k3yRnSc3,k3yQnSc3
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
vyhovuje	vyhovovat	k5eAaImIp3nS
nebo	nebo	k8xC
se	se	k3xPyFc4
pro	pro	k7c4
danou	daný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
hodí	hodit	k5eAaPmIp3nS,k5eAaImIp3nS
nejlépe	dobře	k6eAd3
<g/>
.	.	kIx.
</s>