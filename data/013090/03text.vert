<p>
<s>
Jacob	Jacoba	k1gFnPc2	Jacoba
Ludwig	Ludwig	k1gMnSc1	Ludwig
Carl	Carl	k1gMnSc1	Carl
Grimm	Grimm	k1gMnSc1	Grimm
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1785	[number]	k4	1785
Hanau	Hanaus	k1gInSc2	Hanaus
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1863	[number]	k4	1863
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
lexikograf	lexikograf	k1gMnSc1	lexikograf
<g/>
,	,	kIx,	,
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
germanistiky	germanistika	k1gFnSc2	germanistika
jako	jako	k8xS	jako
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
a	a	k8xC	a
sběratel	sběratel	k1gMnSc1	sběratel
lidových	lidový	k2eAgFnPc2d1	lidová
pohádek	pohádka	k1gFnPc2	pohádka
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Grimmem	Grimm	k1gMnSc7	Grimm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
formuloval	formulovat	k5eAaImAgMnS	formulovat
první	první	k4xOgInSc4	první
hláskoslovný	hláskoslovný	k2eAgInSc4d1	hláskoslovný
zákon	zákon	k1gInSc4	zákon
pro	pro	k7c4	pro
germánské	germánský	k2eAgInPc4d1	germánský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
znám	znám	k2eAgInSc1d1	znám
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Grimmův	Grimmův	k2eAgInSc1d1	Grimmův
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
významného	významný	k2eAgNnSc2d1	významné
díla	dílo	k1gNnSc2	dílo
Deutsche	Deutsche	k1gFnSc1	Deutsche
Mythologie	mythologie	k1gFnSc1	mythologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bratři	bratr	k1gMnPc1	bratr
Grimmové	Grimmová	k1gFnSc2	Grimmová
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jacob	Jacoba	k1gFnPc2	Jacoba
Grimm	Grimmo	k1gNnPc2	Grimmo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Jacob	Jacoba	k1gFnPc2	Jacoba
Grimm	Grimm	k1gMnSc1	Grimm
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jacob	Jacoba	k1gFnPc2	Jacoba
Grimm	Grimm	k1gMnSc1	Grimm
</s>
</p>
