<s>
Jacob	Jacoba	k1gFnPc2	Jacoba
Ludwig	Ludwig	k1gMnSc1	Ludwig
Carl	Carl	k1gMnSc1	Carl
Grimm	Grimm	k1gMnSc1	Grimm
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1785	[number]	k4	1785
Hanau	Hanaus	k1gInSc2	Hanaus
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1863	[number]	k4	1863
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
lexikograf	lexikograf	k1gMnSc1	lexikograf
<g/>
,	,	kIx,	,
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
germanistiky	germanistika	k1gFnSc2	germanistika
jako	jako	k8xC	jako
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
a	a	k8xC	a
sběratel	sběratel	k1gMnSc1	sběratel
lidových	lidový	k2eAgFnPc2d1	lidová
pohádek	pohádka	k1gFnPc2	pohádka
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Grimmem	Grimm	k1gMnSc7	Grimm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
