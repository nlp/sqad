<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
hrabství	hrabství	k1gNnSc6	hrabství
Tyne	Tyne	k1gFnPc2	Tyne
a	a	k8xC	a
Wear	Weara	k1gFnPc2	Weara
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
přístavem	přístav	k1gInSc7	přístav
pro	pro	k7c4	pro
obchodování	obchodování	k1gNnSc4	obchodování
s	s	k7c7	s
uhlím	uhlí	k1gNnSc7	uhlí
a	a	k8xC	a
solí	sůl	k1gFnSc7	sůl
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řeky	řeka	k1gFnSc2	řeka
Wear	Weara	k1gFnPc2	Weara
byla	být	k5eAaImAgFnS	být
osídlena	osídlen	k2eAgFnSc1d1	osídlena
kmenem	kmen	k1gInSc7	kmen
Brigantinů	Brigantin	k1gMnPc2	Brigantin
ještě	ještě	k9	ještě
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
doložené	doložený	k2eAgNnSc1d1	doložené
osídlení	osídlení	k1gNnSc1	osídlení
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
674	[number]	k4	674
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
benediktýnský	benediktýnský	k2eAgMnSc1d1	benediktýnský
biskup	biskup	k1gMnSc1	biskup
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
králem	král	k1gMnSc7	král
Ecgfrithem	Ecgfrith	k1gInSc7	Ecgfrith
založil	založit	k5eAaPmAgInS	založit
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
Monkwearmouth	Monkwearmouth	k1gInSc1	Monkwearmouth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
686	[number]	k4	686
se	se	k3xPyFc4	se
klášter	klášter	k1gInSc1	klášter
stal	stát	k5eAaPmAgInS	stát
střediskem	středisko	k1gNnSc7	středisko
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
knihovnu	knihovna	k1gFnSc4	knihovna
s	s	k7c7	s
asi	asi	k9	asi
300	[number]	k4	300
svazky	svazek	k1gInPc4	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Amiatinský	Amiatinský	k2eAgInSc1d1	Amiatinský
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
latinský	latinský	k2eAgInSc1d1	latinský
překlad	překlad	k1gInSc1	překlad
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
klášteru	klášter	k1gInSc3	klášter
a	a	k8xC	a
za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
autora	autor	k1gMnSc4	autor
je	být	k5eAaImIp3nS	být
pokládán	pokládán	k2eAgMnSc1d1	pokládán
Beda	Beda	k1gMnSc1	Beda
Venerabilis	Venerabilis	k1gFnSc2	Venerabilis
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
673	[number]	k4	673
ve	v	k7c6	v
Wearmouthu	Wearmouth	k1gInSc6	Wearmouth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
klášteru	klášter	k1gInSc3	klášter
dokončil	dokončit	k5eAaPmAgInS	dokončit
roku	rok	k1gInSc2	rok
731	[number]	k4	731
i	i	k8xC	i
Historia	Historium	k1gNnPc4	Historium
ecclesiastica	ecclesiasticum	k1gNnSc2	ecclesiasticum
gentis	gentis	k1gFnSc2	gentis
Anglorum	Anglorum	k1gInSc1	Anglorum
<g/>
,	,	kIx,	,
dějiny	dějiny	k1gFnPc1	dějiny
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začali	začít	k5eAaPmAgMnP	začít
pobřeží	pobřeží	k1gNnSc4	pobřeží
napadat	napadat	k5eAaImF	napadat
Vikingové	Viking	k1gMnPc1	Viking
a	a	k8xC	a
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
930	[number]	k4	930
králem	král	k1gMnSc7	král
Athelstanem	Athelstan	k1gInSc7	Athelstan
věnovaná	věnovaný	k2eAgNnPc1d1	věnované
biskupovi	biskup	k1gMnSc6	biskup
z	z	k7c2	z
Durhamu	Durham	k1gInSc2	Durham
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
bývá	bývat	k5eAaImIp3nS	bývat
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
Bishopwearmouth	Bishopwearmouth	k1gInSc1	Bishopwearmouth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1100	[number]	k4	1100
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházela	nacházet	k5eAaImAgFnS	nacházet
malá	malý	k2eAgFnSc1d1	malá
rybářská	rybářský	k2eAgFnSc1d1	rybářská
vesnice	vesnice	k1gFnSc1	vesnice
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Soender-land	Soenderand	k1gInSc1	Soender-land
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
pojmenování	pojmenování	k1gNnSc2	pojmenování
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
název	název	k1gInSc1	název
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
Sunderland	Sunderlanda	k1gFnPc2	Sunderlanda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1346	[number]	k4	1346
začal	začít	k5eAaPmAgMnS	začít
Thomas	Thomas	k1gMnSc1	Thomas
Menville	Menville	k1gFnSc2	Menville
v	v	k7c6	v
Sunderlandu	Sunderlando	k1gNnSc6	Sunderlando
stavět	stavět	k5eAaImF	stavět
lodě	loď	k1gFnPc4	loď
i	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
malým	malý	k2eAgInSc7d1	malý
a	a	k8xC	a
nevýznamným	významný	k2eNgInSc7d1	nevýznamný
přístavem	přístav	k1gInSc7	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1589	[number]	k4	1589
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
Sunderlandu	Sunderland	k1gInSc6	Sunderland
vyrábět	vyrábět	k5eAaImF	vyrábět
sůl	sůl	k1gFnSc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odpaření	odpaření	k1gNnSc4	odpaření
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
soli	sůl	k1gFnSc2	sůl
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
uhlí	uhlí	k1gNnSc1	uhlí
a	a	k8xC	a
tak	tak	k9	tak
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
těžební	těžební	k2eAgFnPc1d1	těžební
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
soli	sůl	k1gFnSc2	sůl
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
pouze	pouze	k6eAd1	pouze
nekvalitní	kvalitní	k2eNgNnSc1d1	nekvalitní
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
bylo	být	k5eAaImAgNnS	být
vyváženo	vyvážet	k5eAaImNgNnS	vyvážet
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Sunderland	Sunderland	k1gInSc1	Sunderland
začal	začít	k5eAaPmAgInS	začít
konkurovat	konkurovat	k5eAaImF	konkurovat
sousednímu	sousední	k2eAgNnSc3d1	sousední
Newcastlu	Newcastlo	k1gNnSc3	Newcastlo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
udělil	udělit	k5eAaPmAgMnS	udělit
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
uhlí	uhlí	k1gNnSc2	uhlí
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Anglii	Anglie	k1gFnSc6	Anglie
Newcastlu	Newcastl	k1gInSc2	Newcastl
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Sunderland	Sunderland	k1gInSc4	Sunderland
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
uhlím	uhlí	k1gNnSc7	uhlí
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
nelibost	nelibost	k1gFnSc4	nelibost
vůči	vůči	k7c3	vůči
Newcastlu	Newcastl	k1gInSc3	Newcastl
a	a	k8xC	a
monarchii	monarchie	k1gFnSc3	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
stál	stát	k5eAaImAgInS	stát
převážně	převážně	k6eAd1	převážně
protestantský	protestantský	k2eAgInSc1d1	protestantský
Sunderland	Sunderland	k1gInSc1	Sunderland
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
parlamentaristů	parlamentarista	k1gMnPc2	parlamentarista
<g/>
,	,	kIx,	,
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
proti	proti	k7c3	proti
katolickému	katolický	k2eAgInSc3d1	katolický
Newcastlu	Newcastl	k1gInSc3	Newcastl
<g/>
.	.	kIx.	.
</s>
<s>
Blokáda	blokáda	k1gFnSc1	blokáda
řeky	řeka	k1gFnSc2	řeka
Tyne	Tyn	k1gMnSc2	Tyn
parlamentaristy	parlamentarista	k1gMnSc2	parlamentarista
působila	působit	k5eAaImAgFnS	působit
vážné	vážný	k2eAgInPc4d1	vážný
problémy	problém	k1gInPc4	problém
Newcastlu	Newcastl	k1gInSc2	Newcastl
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
uhlím	uhlí	k1gNnSc7	uhlí
čehož	což	k3yQnSc2	což
využívali	využívat	k5eAaImAgMnP	využívat
obchodníci	obchodník	k1gMnPc1	obchodník
ze	z	k7c2	z
Sunderlandu	Sunderland	k1gInSc2	Sunderland
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
původní	původní	k2eAgFnPc1d1	původní
osady	osada	k1gFnPc1	osada
Wearmouthu	Wearmouth	k1gInSc2	Wearmouth
(	(	kIx(	(
<g/>
Bishopwearmouth	Bishopwearmouth	k1gInSc1	Bishopwearmouth
<g/>
,	,	kIx,	,
Monkwearmouth	Monkwearmouth	k1gInSc1	Monkwearmouth
a	a	k8xC	a
Sunderland	Sunderland	k1gInSc1	Sunderland
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
spojovat	spojovat	k5eAaImF	spojovat
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
hlavně	hlavně	k9	hlavně
vlivem	vliv	k1gInSc7	vliv
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
rozvoje	rozvoj	k1gInSc2	rozvoj
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Sunderlandu	Sunderland	k1gInSc6	Sunderland
<g/>
,	,	kIx,	,
výroby	výroba	k1gFnPc4	výroba
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
stavby	stavba	k1gFnSc2	stavba
lodí	loď	k1gFnPc2	loď
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Wear	Weara	k1gFnPc2	Weara
<g/>
.	.	kIx.	.
</s>
<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
důležitý	důležitý	k2eAgInSc4d1	důležitý
obchodní	obchodní	k2eAgInSc4d1	obchodní
přístav	přístav	k1gInSc4	přístav
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postižen	postihnout	k5eAaPmNgInS	postihnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1831	[number]	k4	1831
epidemií	epidemie	k1gFnPc2	epidemie
cholery	cholera	k1gFnSc2	cholera
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
město	město	k1gNnSc4	město
byla	být	k5eAaImAgFnS	být
uvalena	uvalen	k2eAgFnSc1d1	uvalena
karanténa	karanténa	k1gFnSc1	karanténa
a	a	k8xC	a
přístav	přístav	k1gInSc1	přístav
byl	být	k5eAaImAgInS	být
blokován	blokovat	k5eAaImNgInS	blokovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
nákaza	nákaza	k1gFnSc1	nákaza
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
Gatesheadu	Gateshead	k1gInSc2	Gateshead
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
obětí	oběť	k1gFnSc7	oběť
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
asi	asi	k9	asi
32	[number]	k4	32
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Wearmouthský	Wearmouthský	k2eAgInSc1d1	Wearmouthský
železný	železný	k2eAgInSc1d1	železný
most	most	k1gInSc1	most
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgNnSc7	druhý
<g/>
,	,	kIx,	,
železným	železný	k2eAgInSc7d1	železný
mostem	most	k1gInSc7	most
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
dokončení	dokončení	k1gNnSc2	dokončení
byl	být	k5eAaImAgInS	být
největším	veliký	k2eAgInSc7d3	veliký
jednoobloukovým	jednoobloukový	k2eAgInSc7d1	jednoobloukový
mostem	most	k1gInSc7	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
proti	proti	k7c3	proti
proudu	proud	k1gInSc3	proud
řeky	řeka	k1gFnSc2	řeka
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
postaven	postaven	k2eAgInSc1d1	postaven
další	další	k2eAgInSc1d1	další
most	most	k1gInSc1	most
královny	královna	k1gFnSc2	královna
Alexandry	Alexandra	k1gFnSc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Sunderlnad	Sunderlnad	k1gInSc1	Sunderlnad
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
měst	město	k1gNnPc2	město
nejvíce	nejvíce	k6eAd1	nejvíce
zasažených	zasažený	k2eAgNnPc2d1	zasažené
bombardováním	bombardování	k1gNnPc3	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
byly	být	k5eAaImAgFnP	být
poškozené	poškozený	k2eAgFnPc1d1	poškozená
oblasti	oblast	k1gFnPc1	oblast
zastavěné	zastavěný	k2eAgFnSc2d1	zastavěná
nevzhlednými	vzhledný	k2eNgFnPc7d1	nevzhledná
betonovými	betonový	k2eAgFnPc7d1	betonová
stavbami	stavba	k1gFnPc7	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
mnoho	mnoho	k4c1	mnoho
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
starobylých	starobylý	k2eAgFnPc2d1	starobylá
staveb	stavba	k1gFnPc2	stavba
bylo	být	k5eAaImAgNnS	být
zachráněno	zachránit	k5eAaPmNgNnS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
církevní	církevní	k2eAgFnPc1d1	církevní
stavby	stavba	k1gFnPc1	stavba
Kostel	kostel	k1gInSc1	kostel
svaté	svatá	k1gFnSc2	svatá
trojice	trojice	k1gFnSc2	trojice
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
roku	rok	k1gInSc2	rok
1719	[number]	k4	1719
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Svatého	svatý	k2eAgMnSc2d1	svatý
Michala	Michal	k1gMnSc2	Michal
<g/>
,	,	kIx,	,
Monkwearmouth	Monkwearmouth	k1gInSc1	Monkwearmouth
jehož	jehož	k3xOyRp3gNnSc1	jehož
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
674	[number]	k4	674
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yRnSc4	co
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úpadku	úpadek	k1gInSc3	úpadek
těžkého	těžký	k2eAgNnSc2d1	těžké
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
,	,	kIx,	,
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jiná	jiný	k2eAgNnPc1d1	jiné
odvětví	odvětví	k1gNnPc1	odvětví
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
chemická	chemický	k2eAgFnSc1d1	chemická
výroba	výroba	k1gFnSc1	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
společností	společnost	k1gFnPc2	společnost
vybudovaly	vybudovat	k5eAaPmAgInP	vybudovat
své	svůj	k3xOyFgNnSc1	svůj
výrobní	výrobní	k2eAgInPc1d1	výrobní
závody	závod	k1gInPc1	závod
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vyhovující	vyhovující	k2eAgFnPc4d1	vyhovující
prostory	prostora	k1gFnPc4	prostora
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
účelových	účelový	k2eAgFnPc2d1	účelová
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
prostorů	prostor	k1gInPc2	prostor
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Wear	Wear	k1gInSc4	Wear
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
stavěly	stavět	k5eAaImAgInP	stavět
lodě	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
zde	zde	k6eAd1	zde
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
obchodních	obchodní	k2eAgNnPc2d1	obchodní
center	centrum	k1gNnPc2	centrum
a	a	k8xC	a
kancelářských	kancelářský	k2eAgInPc2d1	kancelářský
komplexů	komplex	k1gInPc2	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
planině	planina	k1gFnSc6	planina
podél	podél	k7c2	podél
nízkého	nízký	k2eAgInSc2d1	nízký
pásu	pás	k1gInSc2	pás
pahorků	pahorek	k1gInPc2	pahorek
táhnoucích	táhnoucí	k2eAgInPc2d1	táhnoucí
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
této	tento	k3xDgFnSc2	tento
planiny	planina	k1gFnSc2	planina
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
80	[number]	k4	80
<g/>
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
řekou	řeka	k1gFnSc7	řeka
Wear	Weara	k1gFnPc2	Weara
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgInPc7d1	jediný
dvěma	dva	k4xCgInPc7	dva
mosty	most	k1gInPc7	most
pro	pro	k7c4	pro
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
dopravu	doprava	k1gFnSc4	doprava
spojující	spojující	k2eAgInSc4d1	spojující
sever	sever	k1gInSc4	sever
a	a	k8xC	a
jih	jih	k1gInSc4	jih
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
most	most	k1gInSc4	most
královny	královna	k1gFnSc2	královna
Alexandry	Alexandra	k1gFnSc2	Alexandra
u	u	k7c2	u
Pallionu	Pallion	k1gInSc2	Pallion
a	a	k8xC	a
Wearmouthský	Wearmouthský	k2eAgInSc4d1	Wearmouthský
most	most	k1gInSc4	most
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
předměstí	předměstí	k1gNnSc2	předměstí
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
naproti	naproti	k7c3	naproti
západní	západní	k2eAgFnSc3d1	západní
části	část	k1gFnSc3	část
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
70	[number]	k4	70
<g/>
%	%	kIx~	%
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
bydlí	bydlet	k5eAaImIp3nP	bydlet
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
30	[number]	k4	30
<g/>
%	%	kIx~	%
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgNnPc1d1	ostatní
města	město	k1gNnPc1	město
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
i	i	k8xC	i
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Sunderlandu	Sunderland	k1gInSc2	Sunderland
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
mlhy	mlha	k1gFnPc1	mlha
způsobené	způsobený	k2eAgFnPc1d1	způsobená
vlhkostí	vlhkost	k1gFnSc7	vlhkost
proudící	proudící	k2eAgFnSc7d1	proudící
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
26	[number]	k4	26
příčce	příčka	k1gFnSc6	příčka
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
dle	dle	k7c2	dle
velikosti	velikost	k1gFnSc2	velikost
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
je	být	k5eAaImIp3nS	být
98	[number]	k4	98
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
bělochů	běloch	k1gMnPc2	běloch
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
%	%	kIx~	%
asiatů	asiat	k1gMnPc2	asiat
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
bez	bez	k7c2	bez
rozlišení	rozlišení	k1gNnPc2	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
dle	dle	k7c2	dle
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
bylo	být	k5eAaImAgNnS	být
následující	následující	k2eAgNnSc1d1	následující
-	-	kIx~	-
81,5	[number]	k4	81,5
<g/>
%	%	kIx~	%
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
9,6	[number]	k4	9,6
<g/>
%	%	kIx~	%
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
<g/>
,	,	kIx,	,
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
muslimů	muslim	k1gMnPc2	muslim
a	a	k8xC	a
7,6	[number]	k4	7,6
<g/>
%	%	kIx~	%
nechtělo	chtít	k5eNaImAgNnS	chtít
tento	tento	k3xDgInSc4	tento
údaj	údaj	k1gInSc4	údaj
uvést	uvést	k5eAaPmF	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
téměř	téměř	k6eAd1	téměř
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
(	(	kIx(	(
<g/>
takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
označilo	označit	k5eAaPmAgNnS	označit
jen	jen	k9	jen
114	[number]	k4	114
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
společenství	společenství	k1gNnSc1	společenství
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
především	především	k9	především
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
)	)	kIx)	)
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Židé	Žid	k1gMnPc1	Žid
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
anglických	anglický	k2eAgNnPc2d1	anglické
měst	město	k1gNnPc2	město
z	z	k7c2	z
rozsáhlejší	rozsáhlý	k2eAgInPc1d2	rozsáhlejší
židovskou	židovský	k2eAgFnSc7d1	židovská
komunitou	komunita	k1gFnSc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sunderlandu	Sunderland	k1gInSc6	Sunderland
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgFnP	stavět
lodě	loď	k1gFnPc1	loď
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1346	[number]	k4	1346
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
město	město	k1gNnSc1	město
s	s	k7c7	s
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
stavbou	stavba	k1gFnSc7	stavba
lodí	loď	k1gFnPc2	loď
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Sunderlandský	Sunderlandský	k2eAgInSc1d1	Sunderlandský
přístav	přístav	k1gInSc1	přístav
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
významně	významně	k6eAd1	významně
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
vybudováním	vybudování	k1gNnSc7	vybudování
Hudsonových	Hudsonův	k2eAgInPc2d1	Hudsonův
doků	dok	k1gInPc2	dok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
až	až	k8xS	až
1945	[number]	k4	1945
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
245	[number]	k4	245
obchodních	obchodní	k2eAgFnPc2d1	obchodní
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
výtlakem	výtlak	k1gInSc7	výtlak
1,5	[number]	k4	1,5
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představovalo	představovat	k5eAaImAgNnS	představovat
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
produkce	produkce	k1gFnSc2	produkce
lodí	loď	k1gFnPc2	loď
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Zámořská	zámořský	k2eAgFnSc1d1	zámořská
konkurence	konkurence	k1gFnSc1	konkurence
způsobila	způsobit	k5eAaPmAgFnS	způsobit
pokles	pokles	k1gInSc4	pokles
zakázek	zakázka	k1gFnPc2	zakázka
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
uzavřen	uzavřen	k2eAgInSc1d1	uzavřen
poslední	poslední	k2eAgInSc1d1	poslední
dok	dok	k1gInSc1	dok
<g/>
.	.	kIx.	.
</s>
<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
část	část	k1gFnSc1	část
Durhamské	Durhamský	k2eAgFnSc2d1	Durhamská
uhelné	uhelný	k2eAgFnSc2d1	uhelná
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
hornickou	hornický	k2eAgFnSc4d1	hornická
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
rozvoje	rozvoj	k1gInSc2	rozvoj
tohoto	tento	k3xDgNnSc2	tento
odvětví	odvětví	k1gNnSc2	odvětví
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Durhamu	Durham	k1gInSc6	Durham
asi	asi	k9	asi
170	[number]	k4	170
000	[number]	k4	000
horníků	horník	k1gMnPc2	horník
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
těžby	těžba	k1gFnSc2	těžba
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
přinesl	přinést	k5eAaPmAgMnS	přinést
postupné	postupný	k2eAgNnSc4d1	postupné
uzavírání	uzavírání	k1gNnSc4	uzavírání
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
místní	místní	k2eAgInSc1d1	místní
důl	důl	k1gInSc1	důl
byl	být	k5eAaImAgInS	být
uzavřen	uzavřen	k2eAgInSc1d1	uzavřen
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
výroby	výroba	k1gFnSc2	výroba
skla	sklo	k1gNnSc2	sklo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
1	[number]	k4	1
500	[number]	k4	500
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
hornictví	hornictví	k1gNnSc2	hornictví
působí	působit	k5eAaImIp3nP	působit
konkurence	konkurence	k1gFnPc4	konkurence
výrazný	výrazný	k2eAgInSc4d1	výrazný
pokles	pokles	k1gInSc4	pokles
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
odvětví	odvětví	k1gNnSc6	odvětví
a	a	k8xC	a
například	například	k6eAd1	například
Corning	Corning	k1gInSc1	Corning
Glass	Glassa	k1gFnPc2	Glassa
Works	Works	kA	Works
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgNnSc1d1	vyrábějící
sklo	sklo	k1gNnSc1	sklo
asi	asi	k9	asi
120	[number]	k4	120
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
osud	osud	k1gInSc1	osud
potkal	potkat	k5eAaPmAgInS	potkat
i	i	k9	i
Vaux	Vaux	k1gInSc1	Vaux
Breweries	Breweriesa	k1gFnPc2	Breweriesa
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc4	pivovar
produkující	produkující	k2eAgNnSc4d1	produkující
pivo	pivo	k1gNnSc4	pivo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
a	a	k8xC	a
po	po	k7c4	po
110	[number]	k4	110
let	léto	k1gNnPc2	léto
největší	veliký	k2eAgMnSc1d3	veliký
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
konsolidaci	konsolidace	k1gFnSc4	konsolidace
pivovarů	pivovar	k1gInPc2	pivovar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ekonomickém	ekonomický	k2eAgInSc6d1	ekonomický
poklesu	pokles	k1gInSc6	pokles
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
pomalu	pomalu	k6eAd1	pomalu
zotavovala	zotavovat	k5eAaImAgFnS	zotavovat
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
výstavba	výstavba	k1gFnSc1	výstavba
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
montáž	montáž	k1gFnSc4	montáž
automobilů	automobil	k1gInPc2	automobil
společnosti	společnost	k1gFnSc2	společnost
Nissan	nissan	k1gInSc4	nissan
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
společností	společnost	k1gFnPc2	společnost
orientovaných	orientovaný	k2eAgFnPc2d1	orientovaná
na	na	k7c6	na
poskytování	poskytování	k1gNnSc6	poskytování
služeb	služba	k1gFnPc2	služba
což	což	k9	což
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
mnoho	mnoho	k6eAd1	mnoho
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2005	[number]	k4	2005
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
měst	město	k1gNnPc2	město
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
municipálním	municipální	k2eAgInSc7d1	municipální
obvodem	obvod	k1gInSc7	obvod
hrabství	hrabství	k1gNnSc2	hrabství
Durham	Durham	k1gInSc1	Durham
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
správní	správní	k2eAgFnSc6d1	správní
reformě	reforma	k1gFnSc6	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Sunderland	Sunderland	k1gInSc1	Sunderland
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
nezávislým	závislý	k2eNgNnSc7d1	nezávislé
na	na	k7c4	na
hrabství	hrabství	k1gNnSc4	hrabství
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
správy	správa	k1gFnSc2	správa
zrušena	zrušit	k5eAaPmNgNnP	zrušit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
okolní	okolní	k2eAgInPc1d1	okolní
obvody	obvod	k1gInPc1	obvod
staly	stát	k5eAaPmAgInP	stát
metropolitním	metropolitní	k2eAgInSc7d1	metropolitní
distriktem	distrikt	k1gInSc7	distrikt
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hrabství	hrabství	k1gNnSc2	hrabství
Tyne	Tyne	k1gFnPc2	Tyne
a	a	k8xC	a
Wear	Weara	k1gFnPc2	Weara
<g/>
.	.	kIx.	.
</s>
<s>
Distriktu	distrikt	k1gInSc3	distrikt
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
vlády	vláda	k1gFnSc2	vláda
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
udělen	udělen	k2eAgInSc4d1	udělen
status	status	k1gInSc4	status
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Sunderlandská	Sunderlandský	k2eAgFnSc1d1	Sunderlandský
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1965	[number]	k4	1965
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
účastníků	účastník	k1gMnPc2	účastník
a	a	k8xC	a
návštěvníků	návštěvník	k1gMnPc2	návštěvník
světového	světový	k2eAgInSc2d1	světový
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
šampionátu	šampionát	k1gInSc2	šampionát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Vlakovou	vlakový	k2eAgFnSc4d1	vlaková
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
Northern	Northerna	k1gFnPc2	Northerna
Rail	Rail	k1gInSc4	Rail
mezi	mezi	k7c7	mezi
Newcastle	Newcastle	k1gFnSc7	Newcastle
a	a	k8xC	a
Middlesbrough	Middlesbrougha	k1gFnPc2	Middlesbrougha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
trasa	trasa	k1gFnSc1	trasa
metra	metro	k1gNnSc2	metro
Tyne	Tyne	k1gNnPc2	Tyne
a	a	k8xC	a
Wear	Weara	k1gFnPc2	Weara
i	i	k8xC	i
do	do	k7c2	do
Sunderlandu	Sunderland	k1gInSc2	Sunderland
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
metra	metro	k1gNnSc2	metro
končí	končit	k5eAaImIp3nS	končit
u	u	k7c2	u
South	Southa	k1gFnPc2	Southa
Hylton	Hylton	k1gInSc1	Hylton
<g/>
,	,	kIx,	,
když	když	k8xS	když
předtím	předtím	k6eAd1	předtím
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
stanici	stanice	k1gFnSc6	stanice
a	a	k8xC	a
autobusovém	autobusový	k2eAgNnSc6d1	autobusové
stanovišti	stanoviště	k1gNnSc6	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Spoje	spoj	k1gInPc1	spoj
jsou	být	k5eAaImIp3nP	být
dost	dost	k6eAd1	dost
časté	častý	k2eAgFnPc1d1	častá
a	a	k8xC	a
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
Newcastle	Newcastle	k1gFnSc4	Newcastle
upon	upon	k1gInSc1	upon
Tyne	Tyn	k1gFnPc1	Tyn
a	a	k8xC	a
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Newcastle	Newcastle	k1gFnSc2	Newcastle
<g/>
.	.	kIx.	.
</s>
<s>
Oblastí	oblast	k1gFnSc7	oblast
Sunderlandu	Sunderland	k1gInSc2	Sunderland
neprochází	procházet	k5eNaImIp3nS	procházet
žádná	žádný	k3yNgFnSc1	žádný
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Nejrušnější	rušný	k2eAgFnSc7d3	nejrušnější
silnicí	silnice	k1gFnSc7	silnice
je	být	k5eAaImIp3nS	být
A	a	k9	a
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
podél	podél	k7c2	podél
západního	západní	k2eAgInSc2d1	západní
okraje	okraj	k1gInSc2	okraj
města	město	k1gNnSc2	město
a	a	k8xC	a
překonává	překonávat	k5eAaImIp3nS	překonávat
řeku	řeka	k1gFnSc4	řeka
Wear	Weara	k1gFnPc2	Weara
u	u	k7c2	u
Hyltonu	Hylton	k1gInSc2	Hylton
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Park	park	k1gInSc1	park
Lane	Lane	k1gFnPc2	Lane
byl	být	k5eAaImAgInS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1999	[number]	k4	1999
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
autobusové	autobusový	k2eAgNnSc1d1	autobusové
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
750	[number]	k4	750
000	[number]	k4	000
cestujícími	cestující	k1gMnPc7	cestující
odbavenými	odbavený	k2eAgMnPc7d1	odbavený
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
nejrušnějším	rušný	k2eAgNnSc7d3	nejrušnější
autobusovým	autobusový	k2eAgNnSc7d1	autobusové
stanovištěm	stanoviště	k1gNnSc7	stanoviště
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
po	po	k7c6	po
londýnském	londýnský	k2eAgInSc6d1	londýnský
Victoria	Victorium	k1gNnSc2	Victorium
Coach	Coach	k1gInSc1	Coach
Station	station	k1gInSc1	station
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
stanoviště	stanoviště	k1gNnPc1	stanoviště
vybudována	vybudován	k2eAgFnSc1d1	vybudována
stanice	stanice	k1gFnSc1	stanice
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Northern	Northern	k1gInSc1	Northern
Gallery	Galler	k1gInPc1	Galler
for	forum	k1gNnPc2	forum
Contemporary	Contemporara	k1gFnSc2	Contemporara
Art	Art	k1gFnSc2	Art
na	na	k7c4	na
Fawcett	Fawcett	k1gInSc4	Fawcett
Street	Streeta	k1gFnPc2	Streeta
a	a	k8xC	a
Sunderland	Sunderlanda	k1gFnPc2	Sunderlanda
Museum	museum	k1gNnSc1	museum
and	and	k?	and
Winter	Winter	k1gMnSc1	Winter
Gardens	Gardens	k1gInSc4	Gardens
vystavují	vystavovat	k5eAaImIp3nP	vystavovat
kolekce	kolekce	k1gFnPc1	kolekce
děl	dělo	k1gNnPc2	dělo
současných	současný	k2eAgMnPc2d1	současný
i	i	k8xC	i
etablovaných	etablovaný	k2eAgMnPc2d1	etablovaný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
National	Nationat	k5eAaImAgInS	Nationat
Glass	Glass	k1gInSc1	Glass
Centre	centr	k1gInSc5	centr
na	na	k7c4	na
Liberty	Libert	k1gMnPc4	Libert
Way	Way	k1gFnSc2	Way
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
sbírku	sbírka	k1gFnSc4	sbírka
skleněných	skleněný	k2eAgFnPc2d1	skleněná
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Sunderland	Sunderland	k1gInSc1	Sunderland
Empire	empir	k1gInSc5	empir
Theatre	Theatr	k1gInSc5	Theatr
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
divadlem	divadlo	k1gNnSc7	divadlo
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
důkladné	důkladný	k2eAgFnSc6d1	důkladná
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
znovu	znovu	k6eAd1	znovu
otevřeno	otevřen	k2eAgNnSc1d1	otevřeno
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
uvádět	uvádět	k5eAaImF	uvádět
velká	velký	k2eAgNnPc4d1	velké
představení	představení	k1gNnPc4	představení
typu	typ	k1gInSc2	typ
Miss	miss	k1gFnSc2	miss
Saigon	Saigon	k1gInSc1	Saigon
nebo	nebo	k8xC	nebo
My	my	k3xPp1nPc1	my
Fair	fair	k6eAd1	fair
Lady	lady	k1gFnPc6	lady
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
Sunderland	Sunderland	k1gInSc1	Sunderland
International	International	k1gFnSc4	International
Airshow	Airshow	k1gFnPc1	Airshow
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
ji	on	k3xPp3gFnSc4	on
asi	asi	k9	asi
1,2	[number]	k4	1,2
miliónu	milión	k4xCgInSc2	milión
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Sunderlandská	Sunderlandský	k2eAgFnSc1d1	Sunderlandský
univerzita	univerzita	k1gFnSc1	univerzita
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ji	on	k3xPp3gFnSc4	on
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
asi	asi	k9	asi
16	[number]	k4	16
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
centra	centrum	k1gNnPc4	centrum
-	-	kIx~	-
jedno	jeden	k4xCgNnSc1	jeden
ve	v	k7c4	v
Wearmouth	Wearmouth	k1gInSc4	Wearmouth
Hall	Halla	k1gFnPc2	Halla
nedaleko	nedaleko	k7c2	nedaleko
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
hlavní	hlavní	k2eAgFnSc1d1	hlavní
vysokoškolská	vysokoškolský	k2eAgFnSc1d1	vysokoškolská
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
v	v	k7c6	v
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeka	řeka	k1gFnSc1	řeka
Wear	Wear	k1gInSc1	Wear
poblíž	poblíž	k6eAd1	poblíž
National	National	k1gMnSc3	National
Glass	Glassa	k1gFnPc2	Glassa
Centre	centr	k1gInSc5	centr
<g/>
.	.	kIx.	.
</s>
<s>
City	city	k1gNnSc1	city
of	of	k?	of
Sunderland	Sunderlanda	k1gFnPc2	Sunderlanda
College	College	k1gFnSc1	College
je	být	k5eAaImIp3nS	být
institucí	instituce	k1gFnSc7	instituce
pro	pro	k7c4	pro
další	další	k1gNnSc4	další
vzdělávaní	vzdělávaný	k2eAgMnPc1d1	vzdělávaný
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Durham	Durham	k1gInSc1	Durham
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
ji	on	k3xPp3gFnSc4	on
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
asi	asi	k9	asi
14	[number]	k4	14
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
dvacet	dvacet	k4xCc1	dvacet
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
všeobecně	všeobecně	k6eAd1	všeobecně
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
<g/>
,	,	kIx,	,
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostmi	zajímavost	k1gFnPc7	zajímavost
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
ve	v	k7c6	v
městě	město	k1gNnSc6	město
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Penshaw	Penshaw	k1gFnSc4	Penshaw
Monument	monument	k1gInSc1	monument
<g/>
,	,	kIx,	,
Souter	Souter	k1gInSc1	Souter
Lighthouse	Lighthouse	k1gFnSc1	Lighthouse
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
maják	maják	k1gInSc1	maják
napájený	napájený	k2eAgInSc1d1	napájený
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrad	hrad	k1gInSc1	hrad
Hilton	Hilton	k1gInSc1	Hilton
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
park	park	k1gInSc1	park
Wildfowl	Wildfowl	k1gInSc1	Wildfowl
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
Washington	Washington	k1gInSc1	Washington
a	a	k8xC	a
pláže	pláž	k1gFnPc1	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
získalo	získat	k5eAaPmAgNnS	získat
několik	několik	k4yIc4	několik
ocenění	ocenění	k1gNnPc2	ocenění
za	za	k7c4	za
závazek	závazek	k1gInSc4	závazek
ochrany	ochrana	k1gFnSc2	ochrana
přírodních	přírodní	k2eAgFnPc2d1	přírodní
krás	krása	k1gFnPc2	krása
<g/>
.	.	kIx.	.
</s>
<s>
Washington	Washington	k1gInSc1	Washington
D.C.	D.C.	k1gFnSc2	D.C.
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sunderland	Sunderlanda	k1gFnPc2	Sunderlanda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Archív	archív	k1gInSc4	archív
Tyne	Tyne	k1gFnSc4	Tyne
a	a	k8xC	a
Wear	Wear	k1gInSc4	Wear
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Sunderland	Sunderland	k1gInSc1	Sunderland
-	-	kIx~	-
turistický	turistický	k2eAgMnSc1d1	turistický
průvodce	průvodce	k1gMnSc1	průvodce
</s>
