<s>
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
</s>
<s>
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
dvourozměrný	dvourozměrný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
integrál	integrál	k1gInSc4
z	z	k7c2
funkce	funkce	k1gFnSc2
dvou	dva	k4xCgFnPc2
proměnných	proměnná	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
k	k	k7c3
výpočtu	výpočet	k1gInSc3
objemu	objem	k1gInSc2
pod	pod	k7c7
prostorovou	prostorový	k2eAgFnSc7d1
křivkou	křivka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definičním	definiční	k2eAgInSc7d1
oborem	obor	k1gInSc7
těchto	tento	k3xDgFnPc2
funkcí	funkce	k1gFnPc2
je	být	k5eAaImIp3nS
rovina	rovina	k1gFnSc1
nebo	nebo	k8xC
její	její	k3xOp3gFnSc1
část	část	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
se	se	k3xPyFc4
proto	proto	k8xC
počítá	počítat	k5eAaImIp3nS
na	na	k7c6
nějaké	nějaký	k3yIgFnSc6
rovinné	rovinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objem	objem	k1gInSc1
pod	pod	k7c7
prostorovou	prostorový	k2eAgFnSc7d1
křivkou	křivka	k1gFnSc7
funkce	funkce	k1gFnSc2
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
na	na	k7c6
oblasti	oblast	k1gFnSc6
</s>
<s>
I	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I	i	k9
<g/>
}	}	kIx)
</s>
<s>
zapíšeme	zapsat	k5eAaPmIp1nP
jako	jako	k9
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
Výpočet	výpočet	k1gInSc1
</s>
<s>
Dvojný	dvojný	k2eAgInSc4d1
integrál	integrál	k1gInSc4
se	se	k3xPyFc4
snažíme	snažit	k5eAaImIp1nP
převést	převést	k5eAaPmF
na	na	k7c4
tzv.	tzv.	kA
integrál	integrál	k1gInSc4
dvojnásobný	dvojnásobný	k2eAgInSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
dva	dva	k4xCgInPc4
jednoduché	jednoduchý	k2eAgInPc4d1
integrály	integrál	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
na	na	k7c6
obdélníku	obdélník	k1gInSc6
</s>
<s>
Obdélník	obdélník	k1gInSc1
definovaný	definovaný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
</s>
<s>
⟨	⟨	k?
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
×	×	k?
</s>
<s>
⟨	⟨	k?
</s>
<s>
c	c	k0
</s>
<s>
,	,	kIx,
</s>
<s>
d	d	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
langle	langle	k6eAd1
a	a	k8xC
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
times	times	k1gInSc1
\	\	kIx~
<g/>
langle	langle	k1gInSc1
c	c	k0
<g/>
,	,	kIx,
<g/>
d	d	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
}	}	kIx)
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3
rovinná	rovinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
pro	pro	k7c4
výpočet	výpočet	k1gInSc4
integrálu	integrál	k1gInSc2
je	být	k5eAaImIp3nS
obdélník	obdélník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdélník	obdélník	k1gInSc1
lze	lze	k6eAd1
definovat	definovat	k5eAaBmF
pomocí	pomocí	k7c2
tzv.	tzv.	kA
dvourozměrného	dvourozměrný	k2eAgInSc2d1
intervalu	interval	k1gInSc2
ve	v	k7c6
tvaru	tvar	k1gInSc6
</s>
<s>
⟨	⟨	k?
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
×	×	k?
</s>
<s>
⟨	⟨	k?
</s>
<s>
c	c	k0
</s>
<s>
,	,	kIx,
</s>
<s>
d	d	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
langle	langle	k6eAd1
a	a	k8xC
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
times	times	k1gInSc1
\	\	kIx~
<g/>
langle	langle	k1gInSc1
c	c	k0
<g/>
,	,	kIx,
<g/>
d	d	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
}	}	kIx)
</s>
<s>
,	,	kIx,
kde	kde	k6eAd1
</s>
<s>
⟨	⟨	k?
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
langle	langle	k6eAd1
a	a	k8xC
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
interval	interval	k1gInSc1
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
body	bod	k1gInPc7
na	na	k7c6
ose	osa	k1gFnSc6
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
⟨	⟨	k?
</s>
<s>
c	c	k0
</s>
<s>
,	,	kIx,
</s>
<s>
d	d	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
langle	langl	k1gInSc5
c	c	k0
<g/>
,	,	kIx,
<g/>
d	d	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
}	}	kIx)
</s>
<s>
na	na	k7c6
ose	osa	k1gFnSc6
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
kartézský	kartézský	k2eAgInSc4d1
součin	součin	k1gInSc4
dvou	dva	k4xCgInPc2
intervalů	interval	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říkáme	říkat	k5eAaImIp1nP
tedy	tedy	k9
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
kam	kam	k6eAd1
jdu	jít	k5eAaImIp1nS
na	na	k7c6
obou	dva	k4xCgFnPc6
osách	osa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
na	na	k7c6
obdélníku	obdélník	k1gInSc6
spočítáme	spočítat	k5eAaPmIp1nP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
přepíšeme	přepsat	k5eAaPmIp1nP
na	na	k7c4
dvojnásobný	dvojnásobný	k2eAgInSc4d1
integrál	integrál	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
hranice	hranice	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
integrálů	integrál	k1gInPc2
budou	být	k5eAaImBp3nP
podle	podle	k7c2
intervalů	interval	k1gInPc2
popisujících	popisující	k2eAgInPc2d1
obdélník	obdélník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obdélníku	obdélník	k1gInSc6
nezáleží	záležet	k5eNaImIp3nS
na	na	k7c6
pořadí	pořadí	k1gNnSc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
hranic	hranice	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
musíme	muset	k5eAaImIp1nP
ve	v	k7c6
správném	správný	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
napsat	napsat	k5eAaBmF,k5eAaPmF
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Dvojný	dvojný	k2eAgInSc1d1
intergál	intergál	k1gInSc1
na	na	k7c6
oblasti	oblast	k1gFnSc6
</s>
<s>
I	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
⟨	⟨	k?
</s>
<s>
a	a	k8xC
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
×	×	k?
</s>
<s>
⟨	⟨	k?
</s>
<s>
c	c	k0
</s>
<s>
,	,	kIx,
</s>
<s>
d	d	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I	i	k9
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
langle	langle	k6eAd1
a	a	k8xC
<g/>
,	,	kIx,
<g/>
b	b	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
\	\	kIx~
<g/>
times	times	k1gInSc1
\	\	kIx~
<g/>
langle	langle	k1gInSc1
c	c	k0
<g/>
,	,	kIx,
<g/>
d	d	k?
<g/>
\	\	kIx~
<g/>
rangle	rangle	k1gFnSc1
}	}	kIx)
</s>
<s>
tedy	tedy	k9
spočítáme	spočítat	k5eAaPmIp1nP
jako	jako	k9
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∫	∫	k?
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
(	(	kIx(
</s>
<s>
∫	∫	k?
</s>
<s>
c	c	k0
</s>
<s>
d	d	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∫	∫	k?
</s>
<s>
c	c	k0
</s>
<s>
d	d	k?
</s>
<s>
(	(	kIx(
</s>
<s>
∫	∫	k?
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
U	u	k7c2
integrálu	integrál	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
hranice	hranice	k1gFnPc1
popisují	popisovat	k5eAaImIp3nP
osu	osa	k1gFnSc4
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
je	on	k3xPp3gNnPc4
třeba	třeba	k6eAd1
napsat	napsat	k5eAaPmF,k5eAaBmF
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
Stejně	stejně	k6eAd1
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
i	i	k9
u	u	k7c2
osy	osa	k1gFnSc2
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
na	na	k7c6
libovolné	libovolný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
</s>
<s>
Plocha	plocha	k1gFnSc1
mezi	mezi	k7c7
křivkami	křivka	k1gFnPc7
funkcí	funkce	k1gFnPc2
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
=	=	kIx~
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
na	na	k7c6
libovolné	libovolný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
spočítá	spočítat	k5eAaPmIp3nS
pomocí	pomoc	k1gFnSc7
tzv.	tzv.	kA
Fubiniovy	Fubiniův	k2eAgFnSc2d1
věty	věta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
nám	my	k3xPp1nPc3
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dvojný	dvojný	k2eAgInSc4d1
integrál	integrál	k1gInSc4
lze	lze	k6eAd1
rozepsat	rozepsat	k5eAaPmF
na	na	k7c4
dva	dva	k4xCgInPc4
jednoduché	jednoduchý	k2eAgInPc4d1
integrály	integrál	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
hranicemi	hranice	k1gFnPc7
budou	být	k5eAaImBp3nP
funkce	funkce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ale	ale	k9
nutno	nutno	k6eAd1
dodržet	dodržet	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
vnější	vnější	k2eAgInSc1d1
integrál	integrál	k1gInSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
jako	jako	k9
hranice	hranice	k1gFnSc1
číselné	číselný	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
by	by	kYmCp3nS
nakonec	nakonec	k6eAd1
neyvšlo	yvšnout	k5eNaPmAgNnS,k5eNaImAgNnS
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
nějaká	nějaký	k3yIgFnSc1
funkce	funkce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
oblast	oblast	k1gFnSc4
</s>
<s>
I	i	k9
</s>
<s>
:	:	kIx,
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
≤	≤	k?
</s>
<s>
y	y	k?
</s>
<s>
≤	≤	k?
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I	i	k9
<g/>
:	:	kIx,
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
leq	leq	k?
y	y	k?
<g/>
\	\	kIx~
<g/>
leq	leq	k?
g	g	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∫	∫	k?
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
(	(	kIx(
</s>
<s>
∫	∫	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
g	g	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
a	a	k8xC
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
}	}	kIx)
</s>
<s>
omezují	omezovat	k5eAaImIp3nP
osu	osa	k1gFnSc4
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
Opět	opět	k6eAd1
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
dodržet	dodržet	k5eAaPmF
správné	správný	k2eAgNnSc4d1
pořadí	pořadí	k1gNnSc4
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
–	–	k?
popisuje	popisovat	k5eAaImIp3nS
<g/>
-li	-li	k?
integrál	integrál	k1gInSc1
osu	osa	k1gFnSc4
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
musí	muset	k5eAaImIp3nS
u	u	k7c2
něj	on	k3xPp3gMnSc2
být	být	k5eAaImF
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc4
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
by	by	kYmCp3nP
šlo	jít	k5eAaImAgNnS
nadefinovat	nadefinovat	k5eAaPmF
Fubiniovu	Fubiniův	k2eAgFnSc4d1
větu	věta	k1gFnSc4
i	i	k9
pro	pro	k7c4
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
definičním	definiční	k2eAgInSc7d1
oborem	obor	k1gInSc7
je	být	k5eAaImIp3nS
osa	osa	k1gFnSc1
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
oborem	obor	k1gInSc7
hodnot	hodnota	k1gFnPc2
osa	osa	k1gFnSc1
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Pro	pro	k7c4
oblast	oblast	k1gFnSc4
</s>
<s>
I	i	k9
</s>
<s>
:	:	kIx,
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
≤	≤	k?
</s>
<s>
x	x	k?
</s>
<s>
≤	≤	k?
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I	i	k9
<g/>
:	:	kIx,
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
leq	leq	k?
x	x	k?
<g/>
\	\	kIx~
<g/>
leq	leq	k?
g	g	kA
<g/>
(	(	kIx(
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
s	s	k7c7
omezením	omezení	k1gNnSc7
na	na	k7c6
ose	osa	k1gFnSc6
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
body	bod	k1gInPc1
</s>
<s>
a	a	k8xC
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
a	a	k8xC
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
}	}	kIx)
</s>
<s>
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∫	∫	k?
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
(	(	kIx(
</s>
<s>
∫	∫	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
g	g	kA
<g/>
(	(	kIx(
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
Příklad	příklad	k1gInSc1
</s>
<s>
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
na	na	k7c6
oblasti	oblast	k1gFnSc6
mezi	mezi	k7c7
křivkami	křivka	k1gFnPc7
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
=	=	kIx~
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
=	=	kIx~
<g/>
2	#num#	k4
<g/>
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
obrázek	obrázek	k1gInSc1
<g/>
)	)	kIx)
z	z	k7c2
funkce	funkce	k1gFnSc2
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
x	x	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
xy	xy	k?
<g/>
}	}	kIx)
</s>
<s>
Oblast	oblast	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
ose	osa	k1gFnSc6
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
ohraničena	ohraničen	k2eAgFnSc1d1
těmito	tento	k3xDgFnPc7
dvěma	dva	k4xCgFnPc7
křivkami	křivka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ose	osa	k1gFnSc6
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
jde	jít	k5eAaImIp3nS
od	od	k7c2
0	#num#	k4
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojný	dvojný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
tedy	tedy	k9
rozepsal	rozepsat	k5eAaPmAgMnS
na	na	k7c4
dvojnásobný	dvojnásobný	k2eAgInSc4d1
například	například	k6eAd1
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
x	x	k?
</s>
<s>
y	y	k?
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∫	∫	k?
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
∫	∫	k?
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
x	x	k?
</s>
<s>
x	x	k?
</s>
<s>
y	y	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
xy	xy	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g/>
{	{	kIx(
<g/>
d	d	k?
<g/>
}}	}}	k?
<g/>
x	x	k?
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
text	text	k1gInSc1
<g/>
{	{	kIx(
<g/>
d	d	k?
<g/>
}}	}}	k?
<g/>
y	y	k?
<g/>
=	=	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
x	x	k?
<g/>
}	}	kIx)
<g/>
xy	xy	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
Definice	definice	k1gFnSc1
</s>
<s>
Na	na	k7c6
obdélníku	obdélník	k1gInSc6
</s>
<s>
Obdélníková	obdélníkový	k2eAgFnSc1d1
integrační	integrační	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
na	na	k7c6
definičním	definiční	k2eAgInSc6d1
oboru	obor	k1gInSc6
funkce	funkce	k1gFnSc2
dvou	dva	k4xCgFnPc2
proměnných	proměnná	k1gFnPc2
rozdělená	rozdělená	k1gFnSc1
na	na	k7c4
</s>
<s>
n	n	k0
</s>
<s>
×	×	k?
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
\	\	kIx~
<g/>
times	times	k1gInSc4
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
menších	malý	k2eAgInPc2d2
obdélníků	obdélník	k1gInPc2
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
integrál	integrál	k1gInSc4
jednoduchý	jednoduchý	k2eAgInSc4d1
se	se	k3xPyFc4
dvojný	dvojný	k2eAgInSc4d1
integrál	integrál	k1gInSc4
definuje	definovat	k5eAaBmIp3nS
jako	jako	k9
rozdělení	rozdělení	k1gNnSc1
objemu	objem	k1gInSc2
pod	pod	k7c7
prostorovou	prostorový	k2eAgFnSc7d1
křivkou	křivka	k1gFnSc7
na	na	k7c4
nekonečně	konečně	k6eNd1
malé	malý	k2eAgInPc4d1
segmenty	segment	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definiční	definiční	k2eAgInSc1d1
obor	obor	k1gInSc1
se	se	k3xPyFc4
rozdělí	rozdělit	k5eAaPmIp3nS
na	na	k7c4
několik	několik	k4yIc4
menších	malý	k2eAgInPc2d2
obdélníků	obdélník	k1gInPc2
a	a	k8xC
nad	nad	k7c7
nimi	on	k3xPp3gInPc7
následně	následně	k6eAd1
počítáme	počítat	k5eAaImIp1nP
přibližný	přibližný	k2eAgInSc4d1
objem	objem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objem	objem	k1gInSc1
pod	pod	k7c7
celou	celý	k2eAgFnSc7d1
křivkou	křivka	k1gFnSc7
se	se	k3xPyFc4
tedy	tedy	k9
rozdělí	rozdělit	k5eAaPmIp3nS
na	na	k7c4
několik	několik	k4yIc4
malých	malý	k2eAgInPc2d1
kvádrů	kvádr	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
objem	objem	k1gInSc1
už	už	k9
spočítáme	spočítat	k5eAaPmIp1nP
jednoduše	jednoduše	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
sečteme	sečíst	k5eAaPmIp1nP
objemy	objem	k1gInPc4
všech	všecek	k3xTgMnPc2
těchto	tento	k3xDgMnPc2
kvádrů	kvádr	k1gInPc2
<g/>
,	,	kIx,
dostaneme	dostat	k5eAaPmIp1nP
přibližný	přibližný	k2eAgInSc4d1
objem	objem	k1gInSc4
právě	právě	k6eAd1
pod	pod	k7c7
křivkou	křivka	k1gFnSc7
té	ten	k3xDgFnSc2
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obdélník	obdélník	k1gInSc1
se	se	k3xPyFc4
na	na	k7c6
ose	osa	k1gFnSc6
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
rozdělí	rozdělit	k5eAaPmIp3nS
na	na	k7c4
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
sloupců	sloupec	k1gInPc2
a	a	k8xC
na	na	k7c6
ose	osa	k1gFnSc6
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
na	na	k7c6
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
řádků	řádek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objem	objem	k1gInSc4
kvádru	kvádr	k1gInSc2
na	na	k7c6
jednom	jeden	k4xCgInSc6
obdélníčku	obdélníček	k1gInSc6
spočítáme	spočítat	k5eAaPmIp1nP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vynásobíme	vynásobit	k5eAaPmIp1nP
jeho	jeho	k3xOp3gFnPc4
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
tedy	tedy	k9
jako	jako	k9
</s>
<s>
V	v	k7c6
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
V_	V_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
(	(	kIx(
<g/>
x_	x_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
-x_	-x_	k?
<g/>
{	{	kIx(
<g/>
i-	i-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
(	(	kIx(
<g/>
y_	y_	k?
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
}	}	kIx)
<g/>
-y_	-y_	k?
<g/>
{	{	kIx(
<g/>
j-	j-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
z	z	k7c2
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Hodnotu	hodnota	k1gFnSc4
na	na	k7c6
ose	osa	k1gFnSc6
</s>
<s>
z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
z	z	k7c2
<g/>
}	}	kIx)
</s>
<s>
určíme	určit	k5eAaPmIp1nP
dvěma	dva	k4xCgInPc7
způsoby	způsob	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
Horní	horní	k2eAgInSc1d1
součet	součet	k1gInSc1
–	–	k?
za	za	k7c4
</s>
<s>
z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
z	z	k7c2
<g/>
}	}	kIx)
</s>
<s>
dosazujeme	dosazovat	k5eAaImIp1nP
nejvyšší	vysoký	k2eAgFnSc4d3
možnou	možný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
funkce	funkce	k1gFnSc1
nabývá	nabývat	k5eAaImIp3nS
na	na	k7c6
daném	daný	k2eAgInSc6d1
obdélníčku	obdélníček	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
horní	horní	k2eAgInSc1d1
součet	součet	k1gInSc1
bude	být	k5eAaImBp3nS
určitě	určitě	k6eAd1
větší	veliký	k2eAgInSc1d2
nebo	nebo	k8xC
roven	roven	k2eAgInSc1d1
přesnému	přesný	k2eAgInSc3d1
objemu	objem	k1gInSc3
pod	pod	k7c7
prostorovou	prostorový	k2eAgFnSc7d1
křivkou	křivka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Spodní	spodní	k2eAgInSc1d1
součet	součet	k1gInSc1
–	–	k?
za	za	k7c4
</s>
<s>
z	z	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
z	z	k7c2
<g/>
}	}	kIx)
</s>
<s>
dosazujeme	dosazovat	k5eAaImIp1nP
nejnižší	nízký	k2eAgFnSc4d3
možnou	možný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
funkce	funkce	k1gFnSc1
nabývá	nabývat	k5eAaImIp3nS
na	na	k7c6
daném	daný	k2eAgInSc6d1
obdélníčku	obdélníček	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
spodní	spodní	k2eAgInSc1d1
součet	součet	k1gInSc1
bude	být	k5eAaImBp3nS
určitě	určitě	k6eAd1
menší	malý	k2eAgInSc1d2
nebo	nebo	k8xC
roven	roven	k2eAgInSc1d1
přesnému	přesný	k2eAgInSc3d1
objemu	objem	k1gInSc3
pod	pod	k7c7
prostorovou	prostorový	k2eAgFnSc7d1
křivkou	křivka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Jestliže	jestliže	k8xS
minimální	minimální	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
označíme	označit	k5eAaPmIp1nP
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
maximální	maximální	k2eAgFnSc1d1
</s>
<s>
B	B	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
B	B	kA
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
pak	pak	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
b	b	k?
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
inf	inf	k?
</s>
<s>
{	{	kIx(
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
;	;	kIx,
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
∈	∈	k?
</s>
<s>
A	a	k9
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
}	}	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b_	b_	k?
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
inf	inf	k?
<g/>
\	\	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
in	in	k?
A_	A_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
}}	}}	k?
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
sup	sup	k1gMnSc1
</s>
<s>
{	{	kIx(
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
;	;	kIx,
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
∈	∈	k?
</s>
<s>
A	a	k9
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
}	}	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
B_	B_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sup	sup	k1gMnSc1
<g/>
\	\	kIx~
<g/>
{	{	kIx(
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
in	in	k?
A_	A_	k1gFnSc1
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
}}	}}	k?
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
A	a	k9
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A_	A_	k1gFnSc6
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}}	}}	k?
</s>
<s>
označuje	označovat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
obdélníček	obdélníček	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Součet	součet	k1gInSc4
objemu	objem	k1gInSc2
všech	všecek	k3xTgInPc2
kvádrů	kvádr	k1gInPc2
nad	nad	k7c7
obdélníčky	obdélníček	k1gInPc7
zapíšeme	zapsat	k5eAaPmIp1nP
jako	jako	k9
</s>
<s>
s	s	k7c7
</s>
<s>
(	(	kIx(
</s>
<s>
D	D	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
∑	∑	k?
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
(	(	kIx(
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
b	b	k?
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
s	s	k7c7
<g/>
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
=	=	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
}	}	kIx)
<g/>
((	((	k?
<g/>
x_	x_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
-x_	-x_	k?
<g/>
{	{	kIx(
<g/>
i-	i-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
(	(	kIx(
<g/>
y_	y_	k?
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
}	}	kIx)
<g/>
-y_	-y_	k?
<g/>
{	{	kIx(
<g/>
j-	j-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
b_	b_	k?
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
))	))	k?
<g/>
}	}	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
(	(	kIx(
</s>
<s>
D	D	kA
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
∑	∑	k?
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
∑	∑	k?
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
(	(	kIx(
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S	s	k7c7
<g/>
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
=	=	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
}	}	kIx)
<g/>
((	((	k?
<g/>
x_	x_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
-x_	-x_	k?
<g/>
{	{	kIx(
<g/>
i-	i-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
(	(	kIx(
<g/>
y_	y_	k?
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
}	}	kIx)
<g/>
-y_	-y_	k?
<g/>
{	{	kIx(
<g/>
j-	j-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
))	))	k?
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
s	s	k7c7
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
s	s	k7c7
<g/>
}	}	kIx)
</s>
<s>
značí	značit	k5eAaImIp3nS
spodní	spodní	k2eAgInSc1d1
součet	součet	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
S	s	k7c7
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S	s	k7c7
<g/>
}	}	kIx)
</s>
<s>
součet	součet	k1gInSc1
horní	horní	k2eAgInSc1d1
a	a	k8xC
</s>
<s>
D	D	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
D	D	kA
<g/>
}	}	kIx)
</s>
<s>
způsob	způsob	k1gInSc1
dělení	dělení	k1gNnSc2
obdélníku	obdélník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
definice	definice	k1gFnSc2
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
integrál	integrál	k1gInSc4
na	na	k7c6
původním	původní	k2eAgInSc6d1
obdélníku	obdélník	k1gInSc6
</s>
<s>
I	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I	i	k9
<g/>
}	}	kIx)
</s>
<s>
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
s	s	k7c7
</s>
<s>
(	(	kIx(
</s>
<s>
D	D	kA
</s>
<s>
)	)	kIx)
</s>
<s>
≤	≤	k?
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
≤	≤	k?
</s>
<s>
S	s	k7c7
</s>
<s>
(	(	kIx(
</s>
<s>
D	D	kA
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
s	s	k7c7
<g/>
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
leq	leq	k?
\	\	kIx~
<g/>
iint	iint	k1gMnSc1
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
\	\	kIx~
<g/>
leq	leq	k?
S	s	k7c7
<g/>
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
Na	na	k7c6
čím	čí	k3xOyQgNnSc7,k3xOyRgNnSc7
menší	malý	k2eAgInPc4d2
dílky	dílek	k1gInPc4
původní	původní	k2eAgInSc1d1
obdélník	obdélník	k1gInSc1
rozdělím	rozdělit	k5eAaPmIp1nS
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
přesněji	přesně	k6eAd2
budou	být	k5eAaImBp3nP
naše	náš	k3xOp1gInPc1
kvádry	kvádr	k1gInPc1
opisovat	opisovat	k5eAaImF
tvar	tvar	k1gInSc4
té	ten	k3xDgFnSc2
prostorové	prostorový	k2eAgFnSc2d1
křivky	křivka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
tedy	tedy	k9
rozdělíme	rozdělit	k5eAaPmIp1nP
obdélník	obdélník	k1gInSc4
na	na	k7c4
nekonečno	nekonečno	k1gNnSc4
malých	malý	k2eAgInPc2d1
dílků	dílek	k1gInPc2
<g/>
,	,	kIx,
dostaneme	dostat	k5eAaPmIp1nP
přesně	přesně	k6eAd1
ten	ten	k3xDgInSc4
objem	objem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
lim	limo	k1gNnPc2
</s>
<s>
n	n	k0
</s>
<s>
→	→	k?
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
lim	limo	k1gNnPc2
</s>
<s>
m	m	kA
</s>
<s>
→	→	k?
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
∑	∑	k?
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
∑	∑	k?
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
(	(	kIx(
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
b	b	k?
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
lim	limo	k1gNnPc2
</s>
<s>
n	n	k0
</s>
<s>
→	→	k?
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
lim	limo	k1gNnPc2
</s>
<s>
m	m	kA
</s>
<s>
→	→	k?
</s>
<s>
∞	∞	k?
</s>
<s>
(	(	kIx(
</s>
<s>
∑	∑	k?
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
∑	∑	k?
</s>
<s>
j	j	k?
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
(	(	kIx(
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
x	x	k?
</s>
<s>
i	i	k9
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
(	(	kIx(
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
y	y	k?
</s>
<s>
j	j	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
B	B	kA
</s>
<s>
i	i	k9
</s>
<s>
j	j	k?
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
lim	limo	k1gNnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
infty	inft	k1gInPc4
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
lim	limo	k1gNnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
infty	inft	k1gInPc4
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
}	}	kIx)
<g/>
((	((	k?
<g/>
x_	x_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-x_	-x_	k?
<g/>
{	{	kIx(
<g/>
i-	i-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
(	(	kIx(
<g/>
y_	y_	k?
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
}	}	kIx)
<g/>
-y_	-y_	k?
<g/>
{	{	kIx(
<g/>
j-	j-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
b_	b_	k?
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
))))	))))	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
=	=	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
lim	limo	k1gNnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
infty	inft	k1gInPc4
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
lim	limo	k1gNnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
infty	inft	k1gInPc4
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
sum	suma	k1gFnPc2
_	_	kIx~
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
}	}	kIx)
<g/>
((	((	k?
<g/>
x_	x_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
-x_	-x_	k?
<g/>
{	{	kIx(
<g/>
i-	i-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
(	(	kIx(
<g/>
y_	y_	k?
<g/>
{	{	kIx(
<g/>
j	j	k?
<g/>
}	}	kIx)
<g/>
-y_	-y_	k?
<g/>
{	{	kIx(
<g/>
j-	j-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
B_	B_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
ij	ij	k?
<g/>
}	}	kIx)
<g/>
))))	))))	k?
<g/>
}	}	kIx)
</s>
<s>
Na	na	k7c6
libovolné	libovolný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
</s>
<s>
Nechť	nechť	k9
</s>
<s>
χ	χ	k?
</s>
<s>
A	a	k9
</s>
<s>
:	:	kIx,
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
{	{	kIx(
</s>
<s>
0	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
1	#num#	k4
</s>
<s>
}	}	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
chi	chi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
:	:	kIx,
<g/>
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
\	\	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
1	#num#	k4
<g/>
\	\	kIx~
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
charakteristickou	charakteristický	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
množiny	množina	k1gFnSc2
</s>
<s>
A	a	k9
</s>
<s>
⊆	⊆	k?
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A	a	k9
<g/>
\	\	kIx~
<g/>
subseteq	subseteq	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Pak	pak	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
χ	χ	k?
</s>
<s>
A	a	k9
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
{	{	kIx(
</s>
<s>
1	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
∈	∈	k?
</s>
<s>
A	a	k9
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
∉	∉	k?
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
chi	chi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gMnSc1
<g/>
{	{	kIx(
<g/>
cases	cases	k1gMnSc1
<g/>
}	}	kIx)
<g/>
1	#num#	k4
<g/>
,	,	kIx,
<g/>
&	&	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
in	in	k?
A	A	kA
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
<g/>
&	&	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
notin	notin	k1gInSc1
A	A	kA
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
cases	cases	k1gMnSc1
<g/>
}}}	}}}	k?
</s>
<s>
Omezená	omezený	k2eAgFnSc1d1
množina	množina	k1gFnSc1
</s>
<s>
A	a	k9
</s>
<s>
⊆	⊆	k?
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A	a	k9
<g/>
\	\	kIx~
<g/>
subseteq	subseteq	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
Jordanovsky	Jordanovsky	k1gFnSc7
měřitelná	měřitelný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
<g/>
,	,	kIx,
pokud	pokud	k8xS
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
existuje	existovat	k5eAaImIp3nS
nějaký	nějaký	k3yIgInSc1
obdélník	obdélník	k1gInSc1
</s>
<s>
R	R	kA
</s>
<s>
⊇	⊇	k?
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R	R	kA
<g/>
\	\	kIx~
<g/>
supseteq	supseteq	k?
A	A	kA
<g/>
}	}	kIx)
</s>
<s>
takový	takový	k3xDgMnSc1
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
χ	χ	k?
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
chi	chi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
integrovatelná	integrovatelný	k2eAgFnSc1d1
na	na	k7c6
obdélníku	obdélník	k1gInSc6
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R	R	kA
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Mějme	mít	k5eAaImRp1nP
Jordanovsky	Jordanovsky	k1gFnSc7
měřitelnou	měřitelný	k2eAgFnSc4d1
množinu	množina	k1gFnSc4
</s>
<s>
A	a	k9
</s>
<s>
⊆	⊆	k?
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A	a	k9
<g/>
\	\	kIx~
<g/>
subseteq	subseteq	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
ohraničenou	ohraničený	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
</s>
<s>
f	f	k?
</s>
<s>
:	:	kIx,
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
:	:	kIx,
<g/>
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
}	}	kIx)
</s>
<s>
Pak	pak	k6eAd1
je	být	k5eAaImIp3nS
funkce	funkce	k1gFnSc1
</s>
<s>
f	f	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
}	}	kIx)
</s>
<s>
integrovatelná	integrovatelný	k2eAgFnSc1d1
na	na	k7c6
množině	množina	k1gFnSc6
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A	a	k9
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
pro	pro	k7c4
nějaký	nějaký	k3yIgInSc4
obdélník	obdélník	k1gInSc4
</s>
<s>
R	R	kA
</s>
<s>
⊇	⊇	k?
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R	R	kA
<g/>
\	\	kIx~
<g/>
supseteq	supseteq	k?
A	A	kA
<g/>
}	}	kIx)
</s>
<s>
integrovatelná	integrovatelný	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
</s>
<s>
(	(	kIx(
</s>
<s>
χ	χ	k?
</s>
<s>
A	a	k9
</s>
<s>
f	f	k?
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
{	{	kIx(
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
∈	∈	k?
</s>
<s>
A	a	k9
</s>
<s>
0	#num#	k4
</s>
<s>
,	,	kIx,
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
∉	∉	k?
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
(	(	kIx(
<g/>
\	\	kIx~
<g/>
chi	chi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gMnSc1
<g/>
{	{	kIx(
<g/>
cases	cases	k1gMnSc1
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
&	&	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
in	in	k?
A	A	kA
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
,	,	kIx,
<g/>
&	&	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
notin	notin	k1gInSc1
A	A	kA
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
cases	cases	k1gMnSc1
<g/>
}}}	}}}	k?
</s>
<s>
na	na	k7c6
obdélníku	obdélník	k1gInSc6
</s>
<s>
R	R	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
R	R	kA
<g/>
}	}	kIx)
</s>
<s>
Potom	potom	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
∬	∬	k?
</s>
<s>
A	a	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
R	R	kA
</s>
<s>
(	(	kIx(
</s>
<s>
χ	χ	k?
</s>
<s>
A	a	k9
</s>
<s>
f	f	k?
</s>
<s>
)	)	kIx)
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
chi	chi	k0
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
c	c	k0
</s>
<s>
⋅	⋅	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
⋅	⋅	k?
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k8xC
<g/>
}	}	kIx)
<g/>
c	c	k0
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
=	=	kIx~
<g/>
c	c	k0
<g/>
\	\	kIx~
<g/>
cdot	cdot	k2eAgInSc4d1
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
(	(	kIx(
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
+	+	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
+	+	kIx~
<g/>
g	g	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
))	))	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
g	g	kA
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
míra	míra	k1gFnSc1
množiny	množina	k1gFnSc2
</s>
<s>
A	a	k9
</s>
<s>
⊆	⊆	k?
</s>
<s>
R	R	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A	a	k9
<g/>
\	\	kIx~
<g/>
subseteq	subseteq	k?
\	\	kIx~
<g/>
mathbb	mathbb	k1gMnSc1
{	{	kIx(
<g/>
R	R	kA
<g/>
}	}	kIx)
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
nula	nula	k1gFnSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
i	i	k9
</s>
<s>
∬	∬	k?
</s>
<s>
A	a	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Pokud	pokud	k8xS
</s>
<s>
A	a	k9
</s>
<s>
1	#num#	k4
</s>
<s>
∪	∪	k?
</s>
<s>
A	a	k9
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A_	A_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cup	cup	k1gInSc1
A_	A_	k1gFnSc2
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
A	a	k9
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
míra	míra	k1gFnSc1
množiny	množina	k1gFnSc2
</s>
<s>
A	a	k9
</s>
<s>
1	#num#	k4
</s>
<s>
∩	∩	k?
</s>
<s>
A	a	k9
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
A_	A_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cap	cap	k1gMnSc1
A_	A_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
nula	nula	k1gFnSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
</s>
<s>
∬	∬	k?
</s>
<s>
A	a	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
A	a	k9
</s>
<s>
1	#num#	k4
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
+	+	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
A	a	k9
</s>
<s>
2	#num#	k4
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A_	A_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
y	y	k?
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A_	A_	k1gMnSc1
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Transformace	transformace	k1gFnSc1
dvojného	dvojný	k2eAgInSc2d1
integrálu	integrál	k1gInSc2
</s>
<s>
Dvojné	dvojný	k2eAgFnPc1d1
intergály	intergála	k1gFnPc1
můžeme	moct	k5eAaImIp1nP
transformovat	transformovat	k5eAaBmF
za	za	k7c7
účelem	účel	k1gInSc7
zjednodušení	zjednodušení	k1gNnSc2
jejich	jejich	k3xOp3gFnPc2
hranic	hranice	k1gFnPc2
(	(	kIx(
<g/>
ne	ne	k9
integrované	integrovaný	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transformaci	transformace	k1gFnSc4
provedeme	provést	k5eAaPmIp1nP
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
dosadíme	dosadit	k5eAaPmIp1nP
nějaké	nějaký	k3yIgFnPc4
dvě	dva	k4xCgFnPc4
funkce	funkce	k1gFnPc4
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
dvou	dva	k4xCgFnPc6
proměnných	proměnná	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
tyto	tento	k3xDgFnPc4
funkce	funkce	k1gFnPc4
dosadíme	dosadit	k5eAaPmIp1nP
do	do	k7c2
integrálu	integrál	k1gInSc2
a	a	k8xC
přepočítáme	přepočítat	k5eAaPmIp1nP
hranice	hranice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abychom	aby	kYmCp1nP
mohli	moct	k5eAaImAgMnP
integrovat	integrovat	k5eAaBmF
podle	podle	k7c2
těchto	tento	k3xDgFnPc2
nových	nový	k2eAgFnPc2d1
proměnných	proměnná	k1gFnPc2
<g/>
,	,	kIx,
musíme	muset	k5eAaImIp1nP
vnitřek	vnitřek	k1gInSc4
integrálu	integrál	k1gInSc2
vynásobit	vynásobit	k5eAaPmF
absolutní	absolutní	k2eAgInSc4d1
hodnotou	hodnota	k1gFnSc7
tzv.	tzv.	kA
jakobiánu	jakobián	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
A	a	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
u	u	k7c2
</s>
<s>
,	,	kIx,
</s>
<s>
v	v	k7c6
</s>
<s>
)	)	kIx)
</s>
<s>
,	,	kIx,
</s>
<s>
h	h	k?
</s>
<s>
(	(	kIx(
</s>
<s>
u	u	k7c2
</s>
<s>
,	,	kIx,
</s>
<s>
v	v	k7c6
</s>
<s>
)	)	kIx)
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
|	|	kIx~
</s>
<s>
J	J	kA
</s>
<s>
|	|	kIx~
</s>
<s>
d	d	k?
</s>
<s>
u	u	k7c2
</s>
<s>
d	d	k?
</s>
<s>
v	v	k7c6
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
g	g	kA
<g/>
(	(	kIx(
<g/>
u	u	k7c2
<g/>
,	,	kIx,
<g/>
v	v	k7c6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
h	h	k?
<g/>
(	(	kIx(
<g/>
u	u	k7c2
<g/>
,	,	kIx,
<g/>
v	v	k7c6
<g/>
))	))	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
|	|	kIx~
<g/>
J	J	kA
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
u	u	k7c2
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
v	v	k7c6
<g/>
}	}	kIx)
</s>
<s>
Jakobián	Jakobián	k1gMnSc1
funkce	funkce	k1gFnSc2
2	#num#	k4
proměnných	proměnná	k1gFnPc2
spočítáme	spočítat	k5eAaPmIp1nP
jako	jako	k9
determinant	determinant	k1gInSc4
2	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
J	J	kA
</s>
<s>
=	=	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
g	g	kA
</s>
<s>
u	u	k7c2
</s>
<s>
g	g	kA
</s>
<s>
v	v	k7c6
</s>
<s>
h	h	k?
</s>
<s>
u	u	k7c2
</s>
<s>
h	h	k?
</s>
<s>
v	v	k7c6
</s>
<s>
|	|	kIx~
</s>
<s>
=	=	kIx~
</s>
<s>
g	g	kA
</s>
<s>
u	u	k7c2
</s>
<s>
h	h	k?
</s>
<s>
v	v	k7c6
</s>
<s>
−	−	k?
</s>
<s>
g	g	kA
</s>
<s>
v	v	k7c6
</s>
<s>
h	h	k?
</s>
<s>
u	u	k7c2
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
J	J	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
vmatrix	vmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
g_	g_	k?
<g/>
{	{	kIx(
<g/>
u	u	k7c2
<g/>
}	}	kIx)
<g/>
&	&	k?
<g/>
g_	g_	k?
<g/>
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
<g/>
\\	\\	k?
<g/>
h_	h_	k?
<g/>
{	{	kIx(
<g/>
u	u	k7c2
<g/>
}	}	kIx)
<g/>
&	&	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
h_	h_	k?
<g/>
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
vmatrix	vmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
g_	g_	k?
<g/>
{	{	kIx(
<g/>
u	u	k7c2
<g/>
}	}	kIx)
<g/>
h_	h_	k?
<g/>
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
<g/>
-g_	-g_	k?
<g/>
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}	}	kIx)
<g/>
h_	h_	k?
<g/>
{	{	kIx(
<g/>
u	u	k7c2
<g/>
}}	}}	k?
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
g	g	kA
</s>
<s>
u	u	k7c2
</s>
<s>
,	,	kIx,
</s>
<s>
g	g	kA
</s>
<s>
v	v	k7c6
</s>
<s>
,	,	kIx,
</s>
<s>
h	h	k?
</s>
<s>
u	u	k7c2
</s>
<s>
,	,	kIx,
</s>
<s>
h	h	k?
</s>
<s>
v	v	k7c6
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
g_	g_	k?
<g/>
{	{	kIx(
<g/>
u	u	k7c2
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
g_	g_	k?
<g/>
{	{	kIx(
<g/>
v	v	k7c4
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
h_	h_	k?
<g/>
{	{	kIx(
<g/>
u	u	k7c2
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
h_	h_	k?
<g/>
{	{	kIx(
<g/>
v	v	k7c6
<g/>
}}	}}	k?
</s>
<s>
jsou	být	k5eAaImIp3nP
parciální	parciální	k2eAgFnSc1d1
derivace	derivace	k1gFnSc1
těchto	tento	k3xDgFnPc2
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dilatace	dilatace	k1gFnSc1
</s>
<s>
Dilatace	dilatace	k1gFnSc1
je	být	k5eAaImIp3nS
natažení	natažení	k1gNnSc4
nebo	nebo	k8xC
smrštění	smrštění	k1gNnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
něco	něco	k3yInSc4
jako	jako	k9
změna	změna	k1gFnSc1
měřítka	měřítko	k1gNnSc2
grafu	graf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
každou	každý	k3xTgFnSc4
z	z	k7c2
proměnných	proměnná	k1gFnPc2
dosadím	dosadit	k5eAaPmIp1nS
nějaký	nějaký	k3yIgInSc4
násobek	násobek	k1gInSc4
nových	nový	k2eAgFnPc2d1
proměnných	proměnná	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
u	u	k7c2
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
b	b	k?
</s>
<s>
v	v	k7c6
</s>
<s>
|	|	kIx~
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
A	a	k9
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
a	a	k8xC
</s>
<s>
u	u	k7c2
</s>
<s>
,	,	kIx,
</s>
<s>
b	b	k?
</s>
<s>
v	v	k7c6
</s>
<s>
)	)	kIx)
</s>
<s>
⋅	⋅	k?
</s>
<s>
|	|	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
|	|	kIx~
</s>
<s>
d	d	k?
</s>
<s>
u	u	k7c2
</s>
<s>
d	d	k?
</s>
<s>
v	v	k7c6
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
vmatrix	vmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
x	x	k?
<g/>
=	=	kIx~
<g/>
au	au	k0
<g/>
\\	\\	k?
<g/>
y	y	k?
<g/>
=	=	kIx~
<g/>
bv	bv	k?
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
vmatrix	vmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
A	a	k9
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
au	au	k0
<g/>
,	,	kIx,
<g/>
bv	bv	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gInSc1
|	|	kIx~
<g/>
ab	ab	k?
<g/>
|	|	kIx~
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
u	u	k7c2
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
v	v	k7c6
<g/>
}	}	kIx)
</s>
<s>
jelikož	jelikož	k8xS
jakobián	jakobián	k1gMnSc1
vyjde	vyjít	k5eAaPmIp3nS
</s>
<s>
J	J	kA
</s>
<s>
=	=	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
|	|	kIx~
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
J	J	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k1gInSc1
<g/>
{	{	kIx(
<g/>
vmatrix	vmatrix	k1gInSc1
<g/>
}	}	kIx)
<g/>
a	a	k8xC
<g/>
&	&	k?
<g/>
0	#num#	k4
<g/>
\\	\\	k?
<g/>
0	#num#	k4
<g/>
&	&	k?
<g/>
b	b	k?
<g/>
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
vmatrix	vmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
ab	ab	k?
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Posunutí	posunutí	k1gNnSc1
</s>
<s>
Posunutí	posunutí	k1gNnSc1
je	být	k5eAaImIp3nS
transformace	transformace	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
každá	každý	k3xTgFnSc1
proměnná	proměnná	k1gFnSc1
pouze	pouze	k6eAd1
posune	posunout	k5eAaPmIp3nS
o	o	k7c4
určitou	určitý	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakobián	Jakobián	k1gMnSc1
vyjde	vyjít	k5eAaPmIp3nS
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Polární	polární	k2eAgFnPc1d1
souřadnice	souřadnice	k1gFnPc1
</s>
<s>
Polární	polární	k2eAgFnPc1d1
souřadnice	souřadnice	k1gFnPc1
</s>
<s>
Polární	polární	k2eAgFnPc1d1
souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
typickou	typický	k2eAgFnSc7d1
transformací	transformace	k1gFnSc7
<g/>
,	,	kIx,
počítáme	počítat	k5eAaImIp1nP
<g/>
-li	-li	k?
integrál	integrál	k1gInSc4
na	na	k7c4
nějaké	nějaký	k3yIgFnPc4
části	část	k1gFnPc4
kruhu	kruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokáží	dokázat	k5eAaPmIp3nP
hranice	hranice	k1gFnPc4
zjednodušit	zjednodušit	k5eAaPmF
opravdu	opravdu	k6eAd1
hodně	hodně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bod	bod	k1gInSc4
polárními	polární	k2eAgFnPc7d1
souřadnicemi	souřadnice	k1gFnPc7
popíšeme	popsat	k5eAaPmIp1nP
jako	jako	k8xS,k8xC
vzdálenost	vzdálenost	k1gFnSc4
od	od	k7c2
počátku	počátek	k1gInSc2
(	(	kIx(
<g/>
zpravidla	zpravidla	k6eAd1
označujeme	označovat	k5eAaImIp1nP
</s>
<s>
ρ	ρ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
rho	rho	k?
}	}	kIx)
</s>
<s>
–	–	k?
ró	ró	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
úhel	úhel	k1gInSc4
(	(	kIx(
<g/>
označujeme	označovat	k5eAaImIp1nP
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
phi	phi	k?
}	}	kIx)
</s>
<s>
–	–	k?
fí	fí	k0
<g/>
)	)	kIx)
–	–	k?
viz	vidět	k5eAaImRp2nS
obrázek	obrázek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
definice	definice	k1gFnSc2
goniometrických	goniometrický	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
na	na	k7c6
jednotkové	jednotkový	k2eAgFnSc6d1
kružnici	kružnice	k1gFnSc6
lze	lze	k6eAd1
vyvodit	vyvodit	k5eAaBmF,k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
phi	phi	k?
}	}	kIx)
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
sin	sin	kA
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
phi	phi	k?
}	}	kIx)
</s>
<s>
ρ	ρ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
rho	rho	k?
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
nějaká	nějaký	k3yIgFnSc1
nezáporná	záporný	k2eNgFnSc1d1
hodnota	hodnota	k1gFnSc1
a	a	k8xC
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
phi	phi	k?
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
nějaký	nějaký	k3yIgInSc4
úhel	úhel	k1gInSc4
od	od	k7c2
0	#num#	k4
do	do	k7c2
360	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
tedy	tedy	k9
2	#num#	k4
<g/>
π	π	k?
Jakobián	Jakobián	k1gMnSc1
vyjde	vyjít	k5eAaPmIp3nS
</s>
<s>
J	J	kA
</s>
<s>
=	=	kIx~
</s>
<s>
|	|	kIx~
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
−	−	k?
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
sin	sin	kA
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
sin	sin	kA
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
|	|	kIx~
</s>
<s>
=	=	kIx~
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
2	#num#	k4
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
+	+	kIx~
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
sin	sin	kA
</s>
<s>
2	#num#	k4
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
=	=	kIx~
</s>
<s>
ρ	ρ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
J	J	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
begin	begin	k2eAgInSc4d1
<g/>
{	{	kIx(
<g/>
vmatrix	vmatrix	k1gInSc4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
phi	phi	k?
&	&	k?
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
phi	phi	k?
\\\	\\\	k?
<g/>
sin	sin	kA
\	\	kIx~
<g/>
phi	phi	k?
&	&	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
phi	phi	k?
\	\	kIx~
<g/>
end	end	k?
<g/>
{	{	kIx(
<g/>
vmatrix	vmatrix	k1gInSc1
<g/>
}}	}}	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
cos	cos	kA
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
phi	phi	k?
+	+	kIx~
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
sin	sin	kA
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
phi	phi	k?
=	=	kIx~
<g/>
\	\	kIx~
<g/>
rho	rho	k?
}	}	kIx)
</s>
<s>
Jelikož	jelikož	k8xS
</s>
<s>
ρ	ρ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
rho	rho	k?
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
nezáporné	záporný	k2eNgNnSc1d1
<g/>
,	,	kIx,
absolutní	absolutní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
z	z	k7c2
jakobiánu	jakobián	k1gInSc2
je	být	k5eAaImIp3nS
jen	jen	k9
</s>
<s>
ρ	ρ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
rho	rho	k?
}	}	kIx)
</s>
<s>
</s>
<s>
Při	při	k7c6
přepisu	přepis	k1gInSc6
na	na	k7c4
dvojnásobný	dvojnásobný	k2eAgInSc4d1
integrál	integrál	k1gInSc4
se	se	k3xPyFc4
jako	jako	k9
hranice	hranice	k1gFnSc1
uvede	uvést	k5eAaPmIp3nS
právě	právě	k9
úhel	úhel	k1gInSc4
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
phi	phi	k?
}	}	kIx)
</s>
<s>
a	a	k8xC
poloměr	poloměr	k1gInSc1
</s>
<s>
ρ	ρ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
rho	rho	k?
}	}	kIx)
</s>
<s>
Například	například	k6eAd1
integrál	integrál	k1gInSc1
na	na	k7c6
kruhu	kruh	k1gInSc6
s	s	k7c7
poloměrem	poloměr	k1gInSc7
</s>
<s>
r	r	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
r	r	kA
<g/>
}	}	kIx)
</s>
<s>
:	:	kIx,
</s>
<s>
∬	∬	k?
</s>
<s>
K	k	k7c3
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
P	P	kA
</s>
<s>
ρ	ρ	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
,	,	kIx,
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
sin	sin	kA
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
K	k	k7c3
<g/>
}	}	kIx)
<g/>
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
P	P	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rho	rho	k?
f	f	k?
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
phi	phi	k?
,	,	kIx,
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
phi	phi	k?
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
Kde	kde	k6eAd1
</s>
<s>
K	k	k7c3
</s>
<s>
:	:	kIx,
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
y	y	k?
</s>
<s>
2	#num#	k4
</s>
<s>
≤	≤	k?
</s>
<s>
r	r	kA
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
K	k	k7c3
<g/>
:	:	kIx,
<g/>
x	x	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
y	y	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
leq	leq	k?
r	r	kA
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
</s>
<s>
P	P	kA
</s>
<s>
:	:	kIx,
</s>
<s>
ρ	ρ	k?
</s>
<s>
∈	∈	k?
</s>
<s>
⟨	⟨	k?
</s>
<s>
0	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
r	r	kA
</s>
<s>
⟩	⟩	k?
</s>
<s>
,	,	kIx,
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
∈	∈	k?
</s>
<s>
⟨	⟨	k?
</s>
<s>
0	#num#	k4
</s>
<s>
;	;	kIx,
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
⟩	⟩	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
P	P	kA
<g/>
:	:	kIx,
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
langle	langle	k1gInSc1
0	#num#	k4
<g/>
;	;	kIx,
<g/>
r	r	kA
<g/>
\	\	kIx~
<g/>
rangle	rangl	k1gMnSc2
,	,	kIx,
<g/>
\	\	kIx~
<g/>
phi	phi	k?
\	\	kIx~
<g/>
in	in	k?
\	\	kIx~
<g/>
langle	langle	k1gInSc1
0	#num#	k4
<g/>
;	;	kIx,
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
\	\	kIx~
<g/>
rangle	rangle	k1gNnPc3
}	}	kIx)
</s>
<s>
Jelikož	jelikož	k8xS
přímo	přímo	k6eAd1
říkám	říkat	k5eAaImIp1nS
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
kam	kam	k6eAd1
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
proměnná	proměnná	k1gFnSc1
jde	jít	k5eAaImIp3nS
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
integrál	integrál	k1gInSc4
na	na	k7c6
obdélníku	obdélník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
∬	∬	k?
</s>
<s>
P	P	kA
</s>
<s>
ρ	ρ	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
,	,	kIx,
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
sin	sin	kA
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
∫	∫	k?
</s>
<s>
0	#num#	k4
</s>
<s>
r	r	kA
</s>
<s>
(	(	kIx(
</s>
<s>
∫	∫	k?
</s>
<s>
0	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
ρ	ρ	k?
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
,	,	kIx,
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
sin	sin	kA
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
)	)	kIx)
</s>
<s>
d	d	k?
</s>
<s>
ρ	ρ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
P	P	kA
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rho	rho	k?
f	f	k?
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
phi	phi	k?
,	,	kIx,
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
phi	phi	k?
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
r	r	kA
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
int	int	k?
\	\	kIx~
<g/>
limits	limits	k1gInSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rho	rho	k?
f	f	k?
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
phi	phi	k?
,	,	kIx,
<g/>
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
phi	phi	k?
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
phi	phi	k?
)	)	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
\	\	kIx~
<g/>
rho	rho	k?
}	}	kIx)
</s>
<s>
Polární	polární	k2eAgFnPc1d1
souřadnice	souřadnice	k1gFnPc1
samozřejmě	samozřejmě	k6eAd1
lze	lze	k6eAd1
zobecnit	zobecnit	k5eAaPmF
na	na	k7c4
libovolně	libovolně	k6eAd1
protaženou	protažený	k2eAgFnSc4d1
či	či	k8xC
smrštěnou	smrštěný	k2eAgFnSc4d1
kružnici	kružnice	k1gFnSc4
(	(	kIx(
<g/>
tedy	tedy	k8xC
elipsu	elipsa	k1gFnSc4
<g/>
)	)	kIx)
nebo	nebo	k8xC
na	na	k7c4
kružnici	kružnice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
nemá	mít	k5eNaImIp3nS
střed	střed	k1gInSc4
v	v	k7c6
počátku	počátek	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
jednom	jeden	k4xCgInSc6
kroku	krok	k1gInSc6
aplikuji	aplikovat	k5eAaBmIp1nS
jak	jak	k6eAd1
transformaci	transformace	k1gFnSc4
do	do	k7c2
polárních	polární	k2eAgFnPc2d1
souřadnic	souřadnice	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
dilataci	dilatace	k1gFnSc4
a	a	k8xC
posunutí	posunutí	k1gNnSc4
<g/>
:	:	kIx,
</s>
<s>
x	x	k?
</s>
<s>
=	=	kIx~
</s>
<s>
a	a	k8xC
</s>
<s>
+	+	kIx~
</s>
<s>
b	b	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
cos	cos	k3yInSc1
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
=	=	kIx~
<g/>
a	a	k8xC
<g/>
+	+	kIx~
<g/>
b	b	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
cos	cos	kA
\	\	kIx~
<g/>
phi	phi	k?
}	}	kIx)
</s>
<s>
y	y	k?
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
+	+	kIx~
</s>
<s>
d	d	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
ρ	ρ	k?
</s>
<s>
⋅	⋅	k?
</s>
<s>
sin	sin	kA
</s>
<s>
ϕ	ϕ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
=	=	kIx~
<g/>
c	c	k0
<g/>
+	+	kIx~
<g/>
d	d	k?
<g/>
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
rho	rho	k?
\	\	kIx~
<g/>
cdot	cdot	k1gMnSc1
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
phi	phi	k?
}	}	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Pomocí	pomocí	k7c2
dvojného	dvojný	k2eAgInSc2d1
integrálu	integrál	k1gInSc2
se	se	k3xPyFc4
kromě	kromě	k7c2
objemu	objem	k1gInSc2
pod	pod	k7c7
prostorovou	prostorový	k2eAgFnSc7d1
křivkou	křivka	k1gFnSc7
dá	dát	k5eAaPmIp3nS
spočítat	spočítat	k5eAaPmF
i	i	k9
obsah	obsah	k1gInSc4
rovinného	rovinný	k2eAgInSc2d1
obrazce	obrazec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počítáme	počítat	k5eAaImIp1nP
<g/>
-li	-li	k?
integrál	integrál	k1gInSc4
z	z	k7c2
nějaké	nějaký	k3yIgFnSc2
funkce	funkce	k1gFnSc2
na	na	k7c6
nějaké	nějaký	k3yIgFnSc6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
danou	daný	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
vytáhneme	vytáhnout	k5eAaPmIp1nP
do	do	k7c2
výšky	výška	k1gFnSc2
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
nedosáhne	dosáhnout	k5eNaPmIp3nS
naší	náš	k3xOp1gFnSc2
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
takto	takto	k6eAd1
vytáhneme	vytáhnout	k5eAaPmIp1nP
rovinný	rovinný	k2eAgInSc4d1
útvar	útvar	k1gInSc4
do	do	k7c2
výšky	výška	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
obsah	obsah	k1gInSc1
bude	být	k5eAaImBp3nS
shodný	shodný	k2eAgInSc1d1
s	s	k7c7
objemem	objem	k1gInSc7
takto	takto	k6eAd1
vytvořeného	vytvořený	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
(	(	kIx(
<g/>
samozřejmě	samozřejmě	k6eAd1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
lišit	lišit	k5eAaImF
jednotky	jednotka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
</s>
<s>
S	s	k7c7
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S	s	k7c7
<g/>
}	}	kIx)
</s>
<s>
rovinné	rovinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
</s>
<s>
I	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I	i	k9
<g/>
}	}	kIx)
</s>
<s>
tedy	tedy	k9
spočítáme	spočítat	k5eAaPmIp1nP
jako	jako	k9
</s>
<s>
S	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S	s	k7c7
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
Také	také	k9
můžeme	moct	k5eAaImIp1nP
pomocí	pomocí	k7c2
dvojných	dvojný	k2eAgInPc2d1
integrálů	integrál	k1gInPc2
spočítat	spočítat	k5eAaPmF
povrch	povrch	k6eAd1wR
prostorové	prostorový	k2eAgFnPc4d1
křivky	křivka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
výpočet	výpočet	k1gInSc4
povrchu	povrch	k1gInSc2
</s>
<s>
S	s	k7c7
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S	s	k7c7
<g/>
}	}	kIx)
</s>
<s>
prostorové	prostorový	k2eAgFnPc4d1
křivky	křivka	k1gFnPc4
funkce	funkce	k1gFnSc2
</s>
<s>
f	f	k?
</s>
<s>
(	(	kIx(
</s>
<s>
x	x	k?
</s>
<s>
,	,	kIx,
</s>
<s>
y	y	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
<g/>
y	y	k?
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
na	na	k7c6
oblasti	oblast	k1gFnSc6
</s>
<s>
I	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
I	i	k9
<g/>
}	}	kIx)
</s>
<s>
platí	platit	k5eAaImIp3nS
následující	následující	k2eAgInSc4d1
vztah	vztah	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
S	s	k7c7
</s>
<s>
=	=	kIx~
</s>
<s>
∬	∬	k?
</s>
<s>
I	i	k9
</s>
<s>
1	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
f	f	k?
</s>
<s>
x	x	k?
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
f	f	k?
</s>
<s>
y	y	k?
</s>
<s>
2	#num#	k4
</s>
<s>
d	d	k?
</s>
<s>
x	x	k?
</s>
<s>
d	d	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
S	s	k7c7
<g/>
=	=	kIx~
<g/>
\	\	kIx~
<g/>
iint	iint	k2eAgInSc4d1
\	\	kIx~
<g/>
limits	limits	k1gInSc4
_	_	kIx~
<g/>
{	{	kIx(
<g/>
I	i	k9
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sqrt	sqrt	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
f_	f_	k?
<g/>
{	{	kIx(
<g/>
x	x	k?
<g/>
}}	}}	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
{	{	kIx(
<g/>
f_	f_	k?
<g/>
{	{	kIx(
<g/>
y	y	k?
<g/>
}}	}}	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
x	x	k?
<g/>
\	\	kIx~
<g/>
mathrm	mathrm	k1gInSc1
{	{	kIx(
<g/>
d	d	k?
<g/>
}	}	kIx)
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
f	f	k?
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f_	f_	k?
<g/>
{	{	kIx(
<g/>
x	x	k?
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
parciální	parciální	k2eAgFnSc1d1
defivace	defivace	k1gFnSc1
funkce	funkce	k1gFnSc2
</s>
<s>
f	f	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
}	}	kIx)
</s>
<s>
podle	podle	k7c2
</s>
<s>
x	x	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
x	x	k?
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
f	f	k?
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f_	f_	k?
<g/>
{	{	kIx(
<g/>
y	y	k?
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
parciální	parciální	k2eAgFnSc1d1
derivace	derivace	k1gFnSc1
</s>
<s>
f	f	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
f	f	k?
<g/>
}	}	kIx)
</s>
<s>
podle	podle	k7c2
</s>
<s>
y	y	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
y	y	k?
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Integrál	integrál	k1gInSc1
</s>
<s>
Integrální	integrální	k2eAgInSc1d1
počet	počet	k1gInSc1
</s>
<s>
Vícerozměrný	vícerozměrný	k2eAgInSc1d1
integrál	integrál	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
