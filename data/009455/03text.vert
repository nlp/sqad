<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Etzler	Etzler	k1gMnSc1	Etzler
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1963	[number]	k4	1963
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
až	až	k9	až
2014	[number]	k4	2014
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
stálý	stálý	k2eAgInSc1d1	stálý
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
prvního	první	k4xOgMnSc4	první
českého	český	k2eAgMnSc4d1	český
novináře	novinář	k1gMnSc4	novinář
oceněného	oceněný	k2eAgInSc2d1	oceněný
Cenou	cena	k1gFnSc7	cena
Emmy	Emma	k1gFnPc1	Emma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
zeměpis	zeměpis	k1gInSc1	zeměpis
a	a	k8xC	a
tělocvik	tělocvik	k1gInSc1	tělocvik
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
byl	být	k5eAaImAgInS	být
dramaturgem	dramaturg	k1gMnSc7	dramaturg
zábavných	zábavný	k2eAgInPc2d1	zábavný
pořadů	pořad	k1gInPc2	pořad
ostravského	ostravský	k2eAgNnSc2d1	ostravské
studia	studio	k1gNnSc2	studio
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
živit	živit	k5eAaImF	živit
jako	jako	k9	jako
umývač	umývač	k1gInSc4	umývač
oken	okno	k1gNnPc2	okno
či	či	k8xC	či
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
nabídnuta	nabídnut	k2eAgFnSc1d1	nabídnuta
práce	práce	k1gFnSc1	práce
v	v	k7c6	v
CNN	CNN	kA	CNN
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
produkční	produkční	k2eAgFnSc1d1	produkční
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
působil	působit	k5eAaImAgMnS	působit
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
jako	jako	k8xS	jako
válečný	válečný	k2eAgInSc1d1	válečný
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
CNN	CNN	kA	CNN
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
<g/>
,	,	kIx,	,
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
Cenu	cena	k1gFnSc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
aplikaci	aplikace	k1gFnSc4	aplikace
satelitních	satelitní	k2eAgInPc2d1	satelitní
přenosů	přenos	k1gInPc2	přenos
pomocí	pomocí	k7c2	pomocí
technologie	technologie	k1gFnSc2	technologie
BGAN	BGAN	kA	BGAN
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mobilní	mobilní	k2eAgInPc4d1	mobilní
živé	živý	k2eAgInPc4d1	živý
vstupy	vstup	k1gInPc4	vstup
bez	bez	k7c2	bez
přenosových	přenosový	k2eAgInPc2d1	přenosový
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zpravodajem	zpravodaj	k1gMnSc7	zpravodaj
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2014	[number]	k4	2014
ho	on	k3xPp3gInSc4	on
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
postu	post	k1gInSc6	post
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
Barbora	Barbora	k1gFnSc1	Barbora
Šámalová	Šámalová	k1gFnSc1	Šámalová
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
připravil	připravit	k5eAaPmAgMnS	připravit
a	a	k8xC	a
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
skoro	skoro	k6eAd1	skoro
pět	pět	k4xCc4	pět
set	set	k1gInSc4	set
reportáží	reportáž	k1gFnPc2	reportáž
nebo	nebo	k8xC	nebo
živých	živý	k2eAgInPc2d1	živý
vstupů	vstup	k1gInPc2	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ukončení	ukončení	k1gNnSc6	ukončení
své	svůj	k3xOyFgFnSc2	svůj
mise	mise	k1gFnSc2	mise
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
poděkovat	poděkovat	k5eAaPmF	poděkovat
i	i	k9	i
za	za	k7c4	za
volnost	volnost	k1gFnSc4	volnost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jsem	být	k5eAaImIp1nS	být
měl	mít	k5eAaImAgMnS	mít
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
témat	téma	k1gNnPc2	téma
reportáží	reportáž	k1gFnPc2	reportáž
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
naprostou	naprostý	k2eAgFnSc4d1	naprostá
editorskou	editorský	k2eAgFnSc4d1	editorská
svobodu	svoboda	k1gFnSc4	svoboda
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
zpracování	zpracování	k1gNnSc6	zpracování
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Miroslav	Miroslav	k1gMnSc1	Miroslav
Etzler	Etzler	k1gMnSc1	Etzler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Etzler	Etzler	k1gMnSc1	Etzler
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
bojuje	bojovat	k5eAaImIp3nS	bojovat
místo	místo	k7c2	místo
vojáků	voják	k1gMnPc2	voják
kapitálem	kapitál	k1gInSc7	kapitál
<g/>
,	,	kIx,	,
rozhovor	rozhovor	k1gInSc4	rozhovor
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Etzler	Etzler	k1gMnSc1	Etzler
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
výzva	výzva	k1gFnSc1	výzva
<g/>
,	,	kIx,	,
rozhovor	rozhovor	k1gInSc1	rozhovor
na	na	k7c6	na
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Etzler	Etzler	k1gMnSc1	Etzler
<g/>
:	:	kIx,	:
Naši	náš	k3xOp1gMnPc1	náš
politici	politik	k1gMnPc1	politik
zradili	zradit	k5eAaPmAgMnP	zradit
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
Číně	Čína	k1gFnSc3	Čína
svobodná	svobodný	k2eAgFnSc1d1	svobodná
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
rozhovor	rozhovor	k1gInSc1	rozhovor
na	na	k7c4	na
aktualne	aktualnout	k5eAaPmIp3nS	aktualnout
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Etzler	Etzler	k1gMnSc1	Etzler
–	–	k?	–
blog	blog	k1gMnSc1	blog
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
ČT24	ČT24	k1gFnSc2	ČT24
</s>
</p>
