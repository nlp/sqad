<p>
<s>
František	František	k1gMnSc1	František
Pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hlinka	Hlinka	k1gMnSc1	Hlinka
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
Nekrasín	Nekrasín	k1gInSc1	Nekrasín
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Dětství	dětství	k1gNnSc4	dětství
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
statku	statek	k1gInSc6	statek
"	"	kIx"	"
<g/>
u	u	k7c2	u
Hajných	hajná	k1gFnPc2	hajná
<g/>
"	"	kIx"	"
v	v	k7c6	v
Nekrasíně	Nekrasína	k1gFnSc6	Nekrasína
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Nové	Nové	k2eAgFnSc2d1	Nové
Včelnice	včelnice	k1gFnSc2	včelnice
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Nový	nový	k2eAgMnSc1d1	nový
Etynk	Etynk	k1gMnSc1	Etynk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
taktéž	taktéž	k?	taktéž
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
gymnázium	gymnázium	k1gNnSc1	gymnázium
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
a	a	k8xC	a
poté	poté	k6eAd1	poté
filosofii	filosofie	k1gFnSc3	filosofie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Bohosloví	bohosloví	k1gNnSc1	bohosloví
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
studia	studio	k1gNnSc2	studio
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
pražského	pražský	k2eAgInSc2d1	pražský
semináře	seminář	k1gInSc2	seminář
<g/>
.	.	kIx.	.
</s>
<s>
Seznamoval	seznamovat	k5eAaImAgMnS	seznamovat
se	se	k3xPyFc4	se
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
sociálního	sociální	k2eAgMnSc2d1	sociální
myslitele	myslitel	k1gMnSc2	myslitel
Bernarda	Bernard	k1gMnSc2	Bernard
Bolzana	Bolzan	k1gMnSc2	Bolzan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vysvěcení	vysvěcení	k1gNnSc6	vysvěcení
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
kaplan	kaplan	k1gMnSc1	kaplan
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kvílicích	Kvílice	k1gFnPc6	Kvílice
u	u	k7c2	u
Slaného	Slaný	k1gInSc2	Slaný
a	a	k8xC	a
Jarošově	Jarošův	k2eAgMnSc6d1	Jarošův
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1847	[number]	k4	1847
byl	být	k5eAaImAgInS	být
vychovatelem	vychovatel	k1gMnSc7	vychovatel
ve	v	k7c6	v
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
rodině	rodina	k1gFnSc6	rodina
barona	baron	k1gMnSc2	baron
Sturmfedera	Sturmfeder	k1gMnSc2	Sturmfeder
v	v	k7c6	v
Hrádku	Hrádok	k1gInSc6	Hrádok
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1	pochován
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
zdounském	zdounský	k2eAgInSc6d1	zdounský
hřbitově	hřbitov	k1gInSc6	hřbitov
u	u	k7c2	u
Sušice	Sušice	k1gFnSc2	Sušice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
v	v	k7c6	v
Hrádku	Hrádok	k1gInSc6	Hrádok
připomíná	připomínat	k5eAaImIp3nS	připomínat
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
budova	budova	k1gFnSc1	budova
pošty	pošta	k1gFnSc2	pošta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
také	také	k9	také
nese	nést	k5eAaImIp3nS	nést
místní	místní	k2eAgFnSc1d1	místní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Psal	psát	k5eAaImAgMnS	psát
česky	česky	k6eAd1	česky
psané	psaný	k2eAgFnPc4d1	psaná
povídky	povídka	k1gFnPc4	povídka
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
katolické	katolický	k2eAgFnSc2d1	katolická
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
křesťanské	křesťanský	k2eAgFnPc4d1	křesťanská
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
povídek	povídka	k1gFnPc2	povídka
vkládal	vkládat	k5eAaImAgInS	vkládat
národnostní	národnostní	k2eAgInSc1d1	národnostní
a	a	k8xC	a
buditelské	buditelský	k2eAgFnPc1d1	buditelská
tendence	tendence	k1gFnPc1	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
<g/>
,	,	kIx,	,
prostých	prostý	k2eAgMnPc2d1	prostý
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
upadlo	upadnout	k5eAaPmAgNnS	upadnout
v	v	k7c6	v
zapomnění	zapomnění	k1gNnSc6	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
Knižně	knižně	k6eAd1	knižně
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
přes	přes	k7c4	přes
150	[number]	k4	150
jeho	jeho	k3xOp3gInPc2	jeho
spisů	spis	k1gInPc2	spis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povídky	povídka	k1gFnPc1	povídka
===	===	k?	===
</s>
</p>
<p>
<s>
povídky	povídka	k1gFnPc1	povídka
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
Poutník	poutník	k1gMnSc1	poutník
<g/>
,	,	kIx,	,
Lumír	Lumír	k1gMnSc1	Lumír
<g/>
,	,	kIx,	,
Posel	posel	k1gMnSc1	posel
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
povídky	povídka	k1gFnPc1	povídka
v	v	k7c6	v
kalendářích	kalendář	k1gInPc6	kalendář
Perly	perla	k1gFnSc2	perla
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
Pečírkův	Pečírkův	k2eAgInSc4d1	Pečírkův
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
Zábavník	zábavník	k1gInSc4	zábavník
učitelský	učitelský	k2eAgInSc4d1	učitelský
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
ráje	ráj	k1gInSc2	ráj
</s>
</p>
<p>
<s>
Františka	Františka	k1gFnSc1	Františka
Pravdy	pravda	k1gFnSc2	pravda
Sebrané	sebraný	k2eAgFnSc2d1	sebraná
povídky	povídka	k1gFnSc2	povídka
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
</s>
</p>
<p>
<s>
Františka	František	k1gMnSc4	František
Pravdy	pravda	k1gFnSc2	pravda
Sebrané	sebraný	k2eAgInPc4d1	sebraný
spisy	spis	k1gInPc4	spis
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c6	o
autorovi	autor	k1gMnSc6	autor
===	===	k?	===
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Brabcová	Brabcová	k1gFnSc1	Brabcová
<g/>
:	:	kIx,	:
Z	z	k7c2	z
Nekrasína	Nekrasín	k1gInSc2	Nekrasín
do	do	k7c2	do
Hrádku	Hrádok	k1gInSc2	Hrádok
</s>
</p>
<p>
<s>
videokazeta	videokazeta	k1gFnSc1	videokazeta
ze	z	k7c2	z
vzpomínkové	vzpomínkový	k2eAgFnSc2d1	vzpomínková
akce	akce	k1gFnSc2	akce
k	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
úmrtí	úmrtí	k1gNnSc2	úmrtí
Františka	František	k1gMnSc2	František
Pravdy	pravda	k1gFnSc2	pravda
</s>
</p>
<p>
<s>
===	===	k?	===
Online	Onlin	k1gInSc5	Onlin
dostupné	dostupný	k2eAgFnPc1d1	dostupná
===	===	k?	===
</s>
</p>
<p>
<s>
PRAVDA	pravda	k9	pravda
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Ženské	ženský	k2eAgNnSc1d1	ženské
srdce	srdce	k1gNnSc1	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
V.	V.	kA	V.
Žirovnický	Žirovnický	k2eAgMnSc1d1	Žirovnický
<g/>
,	,	kIx,	,
1862	[number]	k4	1862
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
http://www.cesky-jazyk.cz/zivotopisy/frantisek-pravda.html	[url]	k1gMnSc1	http://www.cesky-jazyk.cz/zivotopisy/frantisek-pravda.html
</s>
</p>
<p>
<s>
http://www.hradekususice.cz/knihovna/f-pravda.htm	[url]	k6eAd1	http://www.hradekususice.cz/knihovna/f-pravda.htm
</s>
</p>
<p>
<s>
https://web.archive.org/web/20090611121350/http://vcelnice.cz/osobnosti/pravda.html	[url]	k1gMnSc1	https://web.archive.org/web/20090611121350/http://vcelnice.cz/osobnosti/pravda.html
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Humoristické	humoristický	k2eAgInPc1d1	humoristický
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
43	[number]	k4	43
<g/>
,	,	kIx,	,
s.	s.	k?	s.
338	[number]	k4	338
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
František	Františka	k1gFnPc2	Františka
Pravda	pravda	k9	pravda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
František	František	k1gMnSc1	František
Pravda	pravda	k9	pravda
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Pravda	pravda	k9	pravda
</s>
</p>
