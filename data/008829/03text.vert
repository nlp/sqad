<p>
<s>
Palác	palác	k1gInSc1	palác
Schützenů	Schützen	k1gInPc2	Schützen
nebo	nebo	k8xC	nebo
také	také	k9	také
Schützů	Schütz	k1gMnPc2	Schütz
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
Harbuval-Chamaré	Harbuval-Chamarý	k2eAgInPc1d1	Harbuval-Chamarý
či	či	k8xC	či
Bylandt-Rheidtovský	Bylandt-Rheidtovský	k2eAgInSc1d1	Bylandt-Rheidtovský
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
památka	památka	k1gFnSc1	památka
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Sněmovní	sněmovní	k2eAgNnSc1d1	sněmovní
171	[number]	k4	171
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc4d1	barokní
stavbu	stavba	k1gFnSc4	stavba
nepravidelného	pravidelný	k2eNgInSc2d1	nepravidelný
palácového	palácový	k2eAgInSc2d1	palácový
typu	typ	k1gInSc2	typ
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
v	v	k7c6	v
letech	let	k1gInPc6	let
1685	[number]	k4	1685
<g/>
–	–	k?	–
<g/>
1699	[number]	k4	1699
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dvou	dva	k4xCgInPc2	dva
středověkých	středověký	k2eAgMnPc2d1	středověký
domů	dům	k1gInPc2	dům
a	a	k8xC	a
kostelíka	kostelík	k1gInSc2	kostelík
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Palác	palác	k1gInSc1	palác
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dvou	dva	k4xCgInPc2	dva
středověkých	středověký	k2eAgMnPc2d1	středověký
domů	dům	k1gInPc2	dům
a	a	k8xC	a
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Michala	Michala	k1gFnSc1	Michala
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
hřbitovem	hřbitov	k1gInSc7	hřbitov
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
část	část	k1gFnSc1	část
kostela	kostel	k1gInSc2	kostel
je	být	k5eAaImIp3nS	být
zakomponovaná	zakomponovaný	k2eAgFnSc1d1	zakomponovaná
do	do	k7c2	do
samotné	samotný	k2eAgFnSc2d1	samotná
stavby	stavba	k1gFnSc2	stavba
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvoře	dvůr	k1gInSc6	dvůr
paláce	palác	k1gInSc2	palác
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
studna	studna	k1gFnSc1	studna
s	s	k7c7	s
žulovým	žulový	k2eAgNnSc7d1	žulové
roubením	roubení	k1gNnSc7	roubení
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
mříží	mříž	k1gFnSc7	mříž
se	s	k7c7	s
skleněnou	skleněný	k2eAgFnSc7d1	skleněná
plastikou	plastika	k1gFnSc7	plastika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horních	horní	k2eAgFnPc6d1	horní
partiích	partie	k1gFnPc6	partie
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
doložena	doložen	k2eAgFnSc1d1	doložena
existence	existence	k1gFnSc1	existence
románského	románský	k2eAgNnSc2d1	románské
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Úprava	úprava	k1gFnSc1	úprava
zahrady	zahrada	k1gFnSc2	zahrada
s	s	k7c7	s
nikou	nika	k1gFnSc7	nika
a	a	k8xC	a
sochařskou	sochařský	k2eAgFnSc7d1	sochařská
výzdobou	výzdoba	k1gFnSc7	výzdoba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
palác	palác	k1gInSc1	palác
vlastnila	vlastnit	k5eAaImAgNnP	vlastnit
Hrabata	hrabě	k1gNnPc1	hrabě
z	z	k7c2	z
Klenové	klenový	k2eAgFnSc2d1	Klenová
a	a	k8xC	a
Janovic	Janovice	k1gFnPc2	Janovice
(	(	kIx(	(
<g/>
1728	[number]	k4	1728
<g/>
–	–	k?	–
<g/>
1769	[number]	k4	1769
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
erb	erb	k1gInSc1	erb
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
jak	jak	k6eAd1	jak
ve	v	k7c6	v
štukové	štukový	k2eAgFnSc6d1	štuková
výzdobě	výzdoba	k1gFnSc6	výzdoba
niky	nika	k1gFnPc4	nika
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jej	on	k3xPp3gMnSc4	on
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
plastik	plastika	k1gFnPc2	plastika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
měla	mít	k5eAaImAgFnS	mít
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
dispozici	dispozice	k1gFnSc4	dispozice
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
kompoziční	kompoziční	k2eAgFnSc7d1	kompoziční
osou	osa	k1gFnSc7	osa
ukotvenou	ukotvený	k2eAgFnSc7d1	ukotvená
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
zahradním	zahradní	k2eAgNnSc6d1	zahradní
průčelí	průčelí	k1gNnSc6	průčelí
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
v	v	k7c6	v
ohradní	ohradní	k2eAgFnSc3d1	ohradní
zdi	zeď	k1gFnSc3	zeď
–	–	k?	–
toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
i	i	k9	i
přes	přes	k7c4	přes
pozdější	pozdní	k2eAgFnPc4d2	pozdější
úpravy	úprava	k1gFnPc4	úprava
patrné	patrný	k2eAgFnPc1d1	patrná
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
zahradní	zahradní	k2eAgNnSc1d1	zahradní
křídlo	křídlo	k1gNnSc1	křídlo
paláce	palác	k1gInSc2	palác
přestavěno	přestavěn	k2eAgNnSc1d1	přestavěno
a	a	k8xC	a
prodlouženo	prodloužen	k2eAgNnSc1d1	prodlouženo
až	až	k6eAd1	až
k	k	k7c3	k
ohradní	ohradní	k2eAgFnSc3d1	ohradní
zdi	zeď	k1gFnSc3	zeď
s	s	k7c7	s
nikou	nika	k1gFnSc7	nika
a	a	k8xC	a
terasní	terasní	k2eAgFnSc4d1	terasní
zeď	zeď	k1gFnSc4	zeď
přiléhající	přiléhající	k2eAgFnSc4d1	přiléhající
ke	k	k7c3	k
křídlu	křídlo	k1gNnSc3	křídlo
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
novou	nový	k2eAgFnSc7d1	nová
zdí	zeď	k1gFnSc7	zeď
s	s	k7c7	s
arkádami	arkáda	k1gFnPc7	arkáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
zahrady	zahrada	k1gFnSc2	zahrada
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
rehabilitace	rehabilitace	k1gFnSc2	rehabilitace
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
zahrada	zahrada	k1gFnSc1	zahrada
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1	[number]	k4	1
005	[number]	k4	005
m2	m2	k4	m2
je	být	k5eAaImIp3nS	být
vsazena	vsadit	k5eAaPmNgFnS	vsadit
mezi	mezi	k7c4	mezi
zahradní	zahradní	k2eAgNnSc4d1	zahradní
a	a	k8xC	a
západní	západní	k2eAgNnSc4d1	západní
křídlo	křídlo	k1gNnSc4	křídlo
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
položena	položit	k5eAaPmNgFnS	položit
v	v	k7c6	v
úrovni	úroveň	k1gFnSc6	úroveň
druhého	druhý	k4xOgNnSc2	druhý
patra	patro	k1gNnPc4	patro
západního	západní	k2eAgNnSc2d1	západní
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
pohledově	pohledově	k6eAd1	pohledově
patrná	patrný	k2eAgFnSc1d1	patrná
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
Na	na	k7c6	na
Valech	val	k1gInPc6	val
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
od	od	k7c2	od
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
oddělena	oddělit	k5eAaPmNgFnS	oddělit
zahradním	zahradní	k2eAgNnSc7d1	zahradní
křídlem	křídlo	k1gNnSc7	křídlo
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
úzkou	úzký	k2eAgFnSc7d1	úzká
zahrádkou	zahrádka	k1gFnSc7	zahrádka
sousedícího	sousedící	k2eAgInSc2d1	sousedící
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
a	a	k8xC	a
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
Lesliovského	Lesliovský	k2eAgInSc2d1	Lesliovský
paláce	palác	k1gInSc2	palác
čp.	čp.	k?	čp.
180	[number]	k4	180
(	(	kIx(	(
<g/>
britské	britský	k2eAgNnSc1d1	Britské
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
bezprostředně	bezprostředně	k6eAd1	bezprostředně
sousedí	sousedit	k5eAaImIp3nS	sousedit
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
významným	významný	k2eAgInSc7d1	významný
dokladem	doklad	k1gInSc7	doklad
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgFnSc2d1	barokní
zahrady	zahrada	k1gFnSc2	zahrada
palácového	palácový	k2eAgInSc2d1	palácový
typu	typ	k1gInSc2	typ
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
význam	význam	k1gInSc1	význam
ještě	ještě	k6eAd1	ještě
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nezastupitelnou	zastupitelný	k2eNgFnSc7d1	nezastupitelná
součástí	součást	k1gFnSc7	součást
jedinečného	jedinečný	k2eAgInSc2d1	jedinečný
urbanismu	urbanismus	k1gInSc2	urbanismus
místa	místo	k1gNnSc2	místo
–	–	k?	–
pásu	pás	k1gInSc2	pás
palácových	palácový	k2eAgFnPc2d1	palácová
zahrad	zahrada	k1gFnPc2	zahrada
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
svahu	svah	k1gInSc6	svah
pod	pod	k7c7	pod
Pražským	pražský	k2eAgInSc7d1	pražský
hradem	hrad	k1gInSc7	hrad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
počal	počnout	k5eAaPmAgMnS	počnout
utvářet	utvářet	k5eAaImF	utvářet
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vývoj	vývoj	k1gInSc1	vývoj
byl	být	k5eAaImAgInS	být
završen	završit	k5eAaPmNgInS	završit
a	a	k8xC	a
kodifikován	kodifikovat	k5eAaBmNgInS	kodifikovat
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
zůstal	zůstat	k5eAaPmAgInS	zůstat
uchován	uchován	k2eAgInSc1d1	uchován
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
při	při	k7c6	při
domu	dům	k1gInSc6wR	dům
čp.	čp.	k?	čp.
171	[number]	k4	171
je	být	k5eAaImIp3nS	být
jmenovitě	jmenovitě	k6eAd1	jmenovitě
zapsanou	zapsaný	k2eAgFnSc7d1	zapsaná
nemovitou	movitý	k2eNgFnSc7d1	nemovitá
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
(	(	kIx(	(
<g/>
r.	r.	kA	r.
č.	č.	k?	č.
44435	[number]	k4	44435
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
626	[number]	k4	626
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plošná	plošný	k2eAgFnSc1d1	plošná
vestavba	vestavba	k1gFnSc1	vestavba
bazénu	bazén	k1gInSc2	bazén
pod	pod	k7c4	pod
historickou	historický	k2eAgFnSc4d1	historická
zahradu	zahrada	k1gFnSc4	zahrada
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc3	princip
nevhodným	vhodný	k2eNgInSc7d1	nevhodný
zásahem	zásah	k1gInSc7	zásah
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
organismu	organismus	k1gInSc2	organismus
<g/>
,	,	kIx,	,
mizí	mizet	k5eAaImIp3nS	mizet
historicky	historicky	k6eAd1	historicky
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
souvrství	souvrství	k1gNnSc1	souvrství
jejího	její	k3xOp3gNnSc2	její
podloží	podloží	k1gNnSc2	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
tak	tak	k6eAd1	tak
rozměrného	rozměrný	k2eAgInSc2d1	rozměrný
podzemního	podzemní	k2eAgInSc2d1	podzemní
bazénu	bazén	k1gInSc2	bazén
s	s	k7c7	s
příslušenstvím	příslušenství	k1gNnSc7	příslušenství
o	o	k7c6	o
zastavěné	zastavěný	k2eAgFnSc6d1	zastavěná
ploše	plocha	k1gFnSc6	plocha
nad	nad	k7c7	nad
300	[number]	k4	300
m2	m2	k4	m2
a	a	k8xC	a
hloubce	hloubka	k1gFnSc3	hloubka
výkopu	výkop	k1gInSc2	výkop
cca	cca	kA	cca
8	[number]	k4	8
m	m	kA	m
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
zásadami	zásada	k1gFnPc7	zásada
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
historické	historický	k2eAgFnPc4d1	historická
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
článkem	článek	k1gInSc7	článek
č.	č.	k?	č.
14	[number]	k4	14
Florentské	florentský	k2eAgFnSc2d1	florentská
charty	charta	k1gFnSc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Stavbou	stavba	k1gFnSc7	stavba
bazénu	bazén	k1gInSc2	bazén
bude	být	k5eAaImBp3nS	být
znehodnocena	znehodnotit	k5eAaPmNgFnS	znehodnotit
třetina	třetina	k1gFnSc1	třetina
plochy	plocha	k1gFnSc2	plocha
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
narušen	narušit	k5eAaPmNgInS	narušit
stávající	stávající	k2eAgInSc1d1	stávající
vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
znemožněna	znemožněn	k2eAgFnSc1d1	znemožněna
existence	existence	k1gFnSc1	existence
vzrostlých	vzrostlý	k2eAgInPc2d1	vzrostlý
stromů	strom	k1gInPc2	strom
nad	nad	k7c7	nad
podzemním	podzemní	k2eAgInSc7d1	podzemní
bazénem	bazén	k1gInSc7	bazén
a	a	k8xC	a
ekosystém	ekosystém	k1gInSc4	ekosystém
zahrady	zahrada	k1gFnSc2	zahrada
tak	tak	k9	tak
bude	být	k5eAaImBp3nS	být
rozvrácen	rozvrácen	k2eAgInSc1d1	rozvrácen
<g/>
.	.	kIx.	.
</s>
<s>
Přístavba	přístavba	k1gFnSc1	přístavba
objektu	objekt	k1gInSc2	objekt
snídárny	snídárna	k1gFnSc2	snídárna
k	k	k7c3	k
jižní	jižní	k2eAgFnSc3d1	jižní
ohradní	ohradní	k2eAgFnSc3d1	ohradní
stěně	stěna	k1gFnSc3	stěna
zahrady	zahrada	k1gFnSc2	zahrada
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
historickými	historický	k2eAgInPc7d1	historický
materiály	materiál	k1gInPc7	materiál
žádná	žádný	k3yNgFnSc1	žádný
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
plochu	plocha	k1gFnSc4	plocha
zahrady	zahrada	k1gFnSc2	zahrada
o	o	k7c4	o
cca	cca	kA	cca
140	[number]	k4	140
m2	m2	k4	m2
a	a	k8xC	a
deformuje	deformovat	k5eAaImIp3nS	deformovat
její	její	k3xOp3gInSc4	její
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
půdorys	půdorys	k1gInSc4	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Stavby	stavba	k1gFnPc1	stavba
navržené	navržený	k2eAgFnPc1d1	navržená
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
–	–	k?	–
snídárna	snídárna	k1gFnSc1	snídárna
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
podzemním	podzemní	k2eAgInSc7d1	podzemní
bazénem	bazén	k1gInSc7	bazén
–	–	k?	–
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
výměry	výměra	k1gFnSc2	výměra
historické	historický	k2eAgFnSc2d1	historická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nepřiměřený	přiměřený	k2eNgInSc4d1	nepřiměřený
zásah	zásah	k1gInSc4	zásah
do	do	k7c2	do
architektonické	architektonický	k2eAgFnSc2d1	architektonická
kompozice	kompozice	k1gFnSc2	kompozice
zahrady	zahrada	k1gFnSc2	zahrada
a	a	k8xC	a
hmoty	hmota	k1gFnSc2	hmota
jejího	její	k3xOp3gNnSc2	její
podloží	podloží	k1gNnSc2	podloží
<g/>
,	,	kIx,	,
zasahující	zasahující	k2eAgFnSc4d1	zasahující
její	její	k3xOp3gFnSc4	její
další	další	k2eAgFnSc4d1	další
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
složitým	složitý	k2eAgInPc3d1	složitý
terénním	terénní	k2eAgInPc3d1	terénní
poměrům	poměr	k1gInPc3	poměr
jižního	jižní	k2eAgInSc2d1	jižní
svahu	svah	k1gInSc2	svah
pod	pod	k7c7	pod
Pražským	pražský	k2eAgInSc7d1	pražský
hradem	hrad	k1gInSc7	hrad
<g/>
,	,	kIx,	,
situování	situování	k1gNnSc4	situování
zahrady	zahrada	k1gFnSc2	zahrada
pod	pod	k7c7	pod
svahem	svah	k1gInSc7	svah
zahrady	zahrada	k1gFnSc2	zahrada
Na	na	k7c6	na
Valech	val	k1gInPc6	val
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
pod	pod	k7c7	pod
zahradou	zahrada	k1gFnSc7	zahrada
Lesliovského	Lesliovský	k2eAgInSc2d1	Lesliovský
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
cca	cca	kA	cca
10	[number]	k4	10
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
opěrná	opěrný	k2eAgFnSc1d1	opěrná
zeď	zeď	k1gFnSc1	zeď
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vybudování	vybudování	k1gNnSc2	vybudování
podzemního	podzemní	k2eAgInSc2d1	podzemní
bazénu	bazén	k1gInSc2	bazén
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
vodního	vodní	k2eAgInSc2d1	vodní
režimu	režim	k1gInSc2	režim
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
ohrožení	ohrožení	k1gNnSc3	ohrožení
existence	existence	k1gFnSc2	existence
sousedních	sousední	k2eAgFnPc2d1	sousední
palácových	palácový	k2eAgFnPc2d1	palácová
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
záměru	záměr	k1gInSc2	záměr
je	být	k5eAaImIp3nS	být
výstavba	výstavba	k1gFnSc1	výstavba
podzemního	podzemní	k2eAgInSc2d1	podzemní
bazénu	bazén	k1gInSc2	bazén
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
bývalého	bývalý	k2eAgInSc2d1	bývalý
kláštera	klášter	k1gInSc2	klášter
theatinů	theatin	k1gMnPc2	theatin
čp.	čp.	k?	čp.
192	[number]	k4	192
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celkové	celkový	k2eAgFnSc2d1	celková
přestavby	přestavba	k1gFnSc2	přestavba
objektu	objekt	k1gInSc2	objekt
na	na	k7c4	na
rezidenční	rezidenční	k2eAgNnSc4d1	rezidenční
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
si	se	k3xPyFc3	se
investor	investor	k1gMnSc1	investor
přál	přát	k5eAaImAgMnS	přát
umístění	umístění	k1gNnSc4	umístění
bazénu	bazén	k1gInSc2	bazén
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
refektáři	refektář	k1gInSc6	refektář
<g/>
,	,	kIx,	,
od	od	k7c2	od
čehož	což	k3yQnSc2	což
později	pozdě	k6eAd2	pozdě
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
a	a	k8xC	a
přesunul	přesunout	k5eAaPmAgMnS	přesunout
bazén	bazén	k1gInSc4	bazén
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
bývalých	bývalý	k2eAgInPc2d1	bývalý
skleníků	skleník	k1gInPc2	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Bazén	bazén	k1gInSc1	bazén
je	být	k5eAaImIp3nS	být
zahloubený	zahloubený	k2eAgInSc1d1	zahloubený
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
podzemní	podzemní	k2eAgFnPc1d1	podzemní
a	a	k8xC	a
s	s	k7c7	s
budovou	budova	k1gFnSc7	budova
jej	on	k3xPp3gMnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
podzemní	podzemní	k2eAgFnSc1d1	podzemní
chodba	chodba	k1gFnSc1	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
umístění	umístění	k1gNnSc2	umístění
bazénu	bazén	k1gInSc2	bazén
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
písemným	písemný	k2eAgNnSc7d1	písemné
vyjádřením	vyjádření	k1gNnSc7	vyjádření
NPÚ	NPÚ	kA	NPÚ
HMP	HMP	kA	HMP
čj.	čj.	k?	čj.
NPÚ-	NPÚ-	k1gMnSc1	NPÚ-
<g/>
311	[number]	k4	311
<g/>
/	/	kIx~	/
<g/>
13845	[number]	k4	13845
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
však	však	k9	však
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
stanovisko	stanovisko	k1gNnSc1	stanovisko
odboru	odbor	k1gInSc2	odbor
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
Magistrátu	magistrát	k1gInSc2	magistrát
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
