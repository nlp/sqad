<s>
Palác	palác	k1gInSc1	palác
Schützenů	Schützen	k1gInPc2	Schützen
nebo	nebo	k8xC	nebo
také	také	k9	také
Schützů	Schütz	k1gMnPc2	Schütz
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
Harbuval-Chamaré	Harbuval-Chamarý	k2eAgInPc1d1	Harbuval-Chamarý
či	či	k8xC	či
Bylandt-Rheidtovský	Bylandt-Rheidtovský	k2eAgInSc1d1	Bylandt-Rheidtovský
palác	palác	k1gInSc1	palác
je	být	k5eAaImIp3nS	být
památka	památka	k1gFnSc1	památka
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Sněmovní	sněmovní	k2eAgNnSc1d1	sněmovní
171	[number]	k4	171
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
