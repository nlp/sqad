<p>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hodin	hodina	k1gFnPc2	hodina
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
české	český	k2eAgFnSc2d1	Česká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Hany	Hana	k1gFnSc2	Hana
Andronikové	Andronikový	k2eAgFnSc2d1	Androniková
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Román	román	k1gInSc1	román
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
Tomáše	Tomáš	k1gMnSc4	Tomáš
Kepplera	Keppler	k1gMnSc4	Keppler
<g/>
,	,	kIx,	,
stavaře	stavař	k1gMnSc4	stavař
pracujícího	pracující	k1gMnSc4	pracující
za	za	k7c4	za
první	první	k4xOgFnPc4	první
republiky	republika	k1gFnPc4	republika
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
ženy	žena	k1gFnPc1	žena
Rachel	Rachela	k1gFnPc2	Rachela
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
Daniela	Daniel	k1gMnSc2	Daniel
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
ze	z	k7c2	z
Zlína	Zlín	k1gInSc2	Zlín
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
pak	pak	k6eAd1	pak
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
zasažené	zasažený	k2eAgFnSc2d1	zasažená
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autorka	autorka	k1gFnSc1	autorka
používá	používat	k5eAaImIp3nS	používat
postmoderní	postmoderní	k2eAgInPc4d1	postmoderní
vypravěčské	vypravěčský	k2eAgInPc4d1	vypravěčský
postupy	postup	k1gInPc4	postup
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
střídání	střídání	k1gNnSc1	střídání
časových	časový	k2eAgFnPc2d1	časová
rovin	rovina	k1gFnPc2	rovina
a	a	k8xC	a
hledisek	hledisko	k1gNnPc2	hledisko
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
či	či	k8xC	či
fabulace	fabulace	k1gFnSc1	fabulace
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
skutečných	skutečný	k2eAgFnPc6d1	skutečná
postavách	postava	k1gFnPc6	postava
a	a	k8xC	a
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvuk	zvuk	k1gInSc1	zvuk
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hodin	hodina	k1gFnPc2	hodina
získal	získat	k5eAaPmAgInS	získat
Literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
Knižního	knižní	k2eAgInSc2d1	knižní
klubu	klub	k1gInSc2	klub
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2001	[number]	k4	2001
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
získala	získat	k5eAaPmAgFnS	získat
autorka	autorka	k1gFnSc1	autorka
cenu	cena	k1gFnSc4	cena
Magnesia	magnesium	k1gNnSc2	magnesium
Litera	litera	k1gFnSc1	litera
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kritikách	kritika	k1gFnPc6	kritika
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
pochvaly	pochvala	k1gFnPc1	pochvala
pro	pro	k7c4	pro
absenci	absence	k1gFnSc4	absence
sentimentu	sentiment	k1gInSc2	sentiment
(	(	kIx(	(
<g/>
Dana	Dana	k1gFnSc1	Dana
Malá	malý	k2eAgFnSc1d1	malá
v	v	k7c6	v
Mladé	mladý	k2eAgFnSc6d1	mladá
frontě	fronta	k1gFnSc6	fronta
Dnes	dnes	k6eAd1	dnes
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
propracovaný	propracovaný	k2eAgInSc1d1	propracovaný
obraz	obraz	k1gInSc1	obraz
dobového	dobový	k2eAgInSc2d1	dobový
kontextu	kontext	k1gInSc2	kontext
a	a	k8xC	a
místních	místní	k2eAgFnPc2d1	místní
reálií	reálie	k1gFnPc2	reálie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Ondřejová	Ondřejový	k2eAgFnSc1d1	Ondřejová
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
či	či	k8xC	či
rafinovanou	rafinovaný	k2eAgFnSc4d1	rafinovaná
fabulaci	fabulace	k1gFnSc4	fabulace
(	(	kIx(	(
<g/>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Jindra	Jindra	k1gMnSc1	Jindra
v	v	k7c6	v
doslovu	doslov	k1gInSc6	doslov
knihy	kniha	k1gFnSc2	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
v	v	k7c6	v
Coloradu	Colorado	k1gNnSc6	Colorado
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
český	český	k2eAgMnSc1d1	český
emigrant	emigrant	k1gMnSc1	emigrant
Dan	Dan	k1gMnSc1	Dan
Keppler	Keppler	k1gMnSc1	Keppler
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
se	se	k3xPyFc4	se
z	z	k7c2	z
Annou	Anna	k1gFnSc7	Anna
<g/>
,	,	kIx,	,
také	také	k9	také
češkou	češka	k1gFnSc7	češka
<g/>
,	,	kIx,	,
majitelkou	majitelka	k1gFnSc7	majitelka
malého	malý	k2eAgInSc2d1	malý
hotelu	hotel	k1gInSc2	hotel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Dan	Dan	k1gMnSc1	Dan
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
silvestrovskou	silvestrovský	k2eAgFnSc4d1	silvestrovská
noc	noc	k1gFnSc4	noc
si	se	k3xPyFc3	se
vypráví	vyprávět	k5eAaImIp3nP	vyprávět
své	svůj	k3xOyFgInPc4	svůj
dramatické	dramatický	k2eAgInPc4d1	dramatický
osudy	osud	k1gInPc4	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Danovi	Danův	k2eAgMnPc1d1	Danův
rodiče	rodič	k1gMnPc1	rodič
jsou	být	k5eAaImIp3nP	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
židovka	židovka	k1gFnSc1	židovka
Ráchel	Ráchel	k1gFnSc1	Ráchel
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
architekt	architekt	k1gMnSc1	architekt
u	u	k7c2	u
obuvnické	obuvnický	k2eAgFnSc2d1	obuvnická
firmy	firma	k1gFnSc2	firma
Baťa	Baťa	k1gMnSc1	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Baťově	Baťův	k2eAgFnSc3d1	Baťova
expanzi	expanze	k1gFnSc3	expanze
se	se	k3xPyFc4	se
Tom	Tom	k1gMnSc1	Tom
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Tom	Tom	k1gMnSc1	Tom
projektuje	projektovat	k5eAaBmIp3nS	projektovat
novou	nový	k2eAgFnSc4d1	nová
obuvnickou	obuvnický	k2eAgFnSc4d1	obuvnická
továrnu	továrna	k1gFnSc4	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Ráchel	Ráchel	k1gFnSc1	Ráchel
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
odmítá	odmítat	k5eAaImIp3nS	odmítat
být	být	k5eAaImF	být
ženou	žena	k1gFnSc7	žena
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
těm	ten	k3xDgMnPc3	ten
nejchudším	chudý	k2eAgMnPc3d3	nejchudší
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
žijícím	žijící	k2eAgMnSc7d1	žijící
v	v	k7c6	v
indických	indický	k2eAgInPc6d1	indický
slumech	slum	k1gInPc6	slum
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
započetím	započetí	k1gNnSc7	započetí
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přijde	přijít	k5eAaPmIp3nS	přijít
Ráchel	Ráchel	k1gFnSc1	Ráchel
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
o	o	k7c6	o
velmi	velmi	k6eAd1	velmi
vážném	vážný	k2eAgInSc6d1	vážný
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Tomáš	Tomáš	k1gMnSc1	Tomáš
odmítá	odmítat	k5eAaImIp3nS	odmítat
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
Zlína	Zlín	k1gInSc2	Zlín
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
se	se	k3xPyFc4	se
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
nebudou	být	k5eNaImBp3nP	být
moci	moct	k5eAaImF	moct
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
Ráchel	Ráchel	k1gFnSc1	Ráchel
ho	on	k3xPp3gNnSc4	on
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Ráchel	Ráchel	k1gFnSc1	Ráchel
se	se	k3xPyFc4	se
shledá	shledat	k5eAaPmIp3nS	shledat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
útěk	útěk	k1gInSc4	útěk
už	už	k6eAd1	už
nestihnou	stihnout	k5eNaPmIp3nP	stihnout
a	a	k8xC	a
Ráchel	Ráchel	k1gFnSc1	Ráchel
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
německých	německý	k2eAgInPc2d1	německý
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Ráchel	Ráchel	k1gFnSc1	Ráchel
je	být	k5eAaImIp3nS	být
deportována	deportovat	k5eAaBmNgFnS	deportovat
do	do	k7c2	do
terezínského	terezínský	k2eAgNnSc2d1	Terezínské
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
ošetřovatelka	ošetřovatelka	k1gFnSc1	ošetřovatelka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začínají	začínat	k5eAaImIp3nP	začínat
pravé	pravý	k2eAgFnPc4d1	pravá
útrapy	útrapa	k1gFnPc4	útrapa
nacistické	nacistický	k2eAgFnSc2d1	nacistická
zvůle	zvůle	k1gFnSc2	zvůle
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
pracovním	pracovní	k2eAgInSc6d1	pracovní
táboře	tábor	k1gInSc6	tábor
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
totálním	totální	k2eAgNnSc6d1	totální
nasazení	nasazení	k1gNnSc6	nasazení
a	a	k8xC	a
odklízejí	odklízet	k5eAaImIp3nP	odklízet
trosky	troska	k1gFnPc1	troska
zpustošených	zpustošený	k2eAgNnPc2d1	zpustošené
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
s	s	k7c7	s
Danem	Dan	k1gMnSc7	Dan
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
,	,	kIx,	,
německé	německý	k2eAgInPc4d1	německý
úřady	úřad	k1gInPc4	úřad
se	se	k3xPyFc4	se
ale	ale	k9	ale
začínají	začínat	k5eAaImIp3nP	začínat
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
Dana	Dan	k1gMnSc4	Dan
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
o	o	k7c4	o
polovičního	poloviční	k2eAgMnSc4d1	poloviční
žida	žid	k1gMnSc4	žid
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
mu	on	k3xPp3gMnSc3	on
tedy	tedy	k9	tedy
vystaví	vystavit	k5eAaPmIp3nS	vystavit
u	u	k7c2	u
známého	známý	k2eAgMnSc2d1	známý
lékaře	lékař	k1gMnSc2	lékař
falešný	falešný	k2eAgInSc1d1	falešný
úmrtní	úmrtní	k2eAgInSc1d1	úmrtní
list	list	k1gInSc1	list
a	a	k8xC	a
Dan	Dan	k1gMnSc1	Dan
přežívá	přežívat	k5eAaImIp3nS	přežívat
v	v	k7c6	v
tajné	tajný	k2eAgFnSc6d1	tajná
místnosti	místnost	k1gFnSc6	místnost
ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
pomalu	pomalu	k6eAd1	pomalu
končí	končit	k5eAaImIp3nS	končit
a	a	k8xC	a
koncentrační	koncentrační	k2eAgInPc1d1	koncentrační
tábory	tábor	k1gInPc1	tábor
jsou	být	k5eAaImIp3nP	být
osvobozovány	osvobozován	k2eAgInPc1d1	osvobozován
<g/>
,	,	kIx,	,
Ráchel	Ráchel	k1gFnPc1	Ráchel
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Anna	Anna	k1gFnSc1	Anna
je	být	k5eAaImIp3nS	být
těžce	těžce	k6eAd1	těžce
nemocná	nemocný	k2eAgFnSc1d1	nemocná
a	a	k8xC	a
Ráchel	Ráchel	k1gFnSc1	Ráchel
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
chce	chtít	k5eAaImIp3nS	chtít
zůstat	zůstat	k5eAaPmF	zůstat
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
nakonec	nakonec	k6eAd1	nakonec
přežije	přežít	k5eAaPmIp3nS	přežít
<g/>
,	,	kIx,	,
emigruje	emigrovat	k5eAaBmIp3nS	emigrovat
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založí	založit	k5eAaPmIp3nP	založit
malý	malý	k2eAgInSc4d1	malý
hotel	hotel	k1gInSc4	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Ráchel	Ráchel	k1gFnSc1	Ráchel
však	však	k9	však
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
s	s	k7c7	s
Tomem	Tom	k1gMnSc7	Tom
a	a	k8xC	a
s	s	k7c7	s
Danem	Dan	k1gMnSc7	Dan
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nesetká	setkat	k5eNaPmIp3nS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Toma	Tom	k1gMnSc4	Tom
její	její	k3xOp3gFnPc1	její
smrt	smrt	k1gFnSc1	smrt
těžce	těžce	k6eAd1	těžce
poznamená	poznamenat	k5eAaPmIp3nS	poznamenat
a	a	k8xC	a
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
ho	on	k3xPp3gMnSc4	on
znechutí	znechutit	k5eAaPmIp3nS	znechutit
<g/>
.	.	kIx.	.
</s>
<s>
Dan	Dan	k1gMnSc1	Dan
dostuduje	dostudovat	k5eAaPmIp3nS	dostudovat
a	a	k8xC	a
ožení	oženit	k5eAaPmIp3nS	oženit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
emigruje	emigrovat	k5eAaBmIp3nS	emigrovat
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
shledá	shledat	k5eAaPmIp3nS	shledat
s	s	k7c7	s
matčinou	matčin	k2eAgFnSc7d1	matčina
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Annou	Anna	k1gFnSc7	Anna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Odeon	odeon	k1gInSc4	odeon
vydalo	vydat	k5eAaPmAgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
druhé	druhý	k4xOgFnPc4	druhý
<g/>
,	,	kIx,	,
pozměněné	pozměněný	k2eAgNnSc4d1	pozměněné
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc4	vydání
románu	román	k1gInSc2	román
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-242-0689-7	[number]	k4	80-242-0689-7
</s>
</p>
<p>
<s>
pozměněné	pozměněný	k2eAgNnSc1d1	pozměněné
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-207-1261-5	[number]	k4	978-80-207-1261-5
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
