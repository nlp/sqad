<s>
Krysa	krysa	k1gFnSc1	krysa
dinagatská	dinagatský	k2eAgFnSc1d1	dinagatský
(	(	kIx(	(
<g/>
Crateromys	Crateromys	k1gInSc1	Crateromys
australis	australis	k1gFnSc2	australis
<g/>
)	)	kIx)	)
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Crateromys	Crateromys	k1gInSc1	Crateromys
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
myšovitých	myšovitý	k2eAgMnPc2d1	myšovitý
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
obláčkových	obláčkový	k2eAgFnPc2d1	obláčková
krys	krysa	k1gFnPc2	krysa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
žijí	žít	k5eAaImIp3nP	žít
jen	jen	k9	jen
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
bílý	bílý	k2eAgInSc4d1	bílý
konec	konec	k1gInSc4	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
ocasem	ocas	k1gInSc7	ocas
měří	měřit	k5eAaImIp3nS	měřit
55	[number]	k4	55
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
jediný	jediný	k2eAgInSc1d1	jediný
exemplář	exemplář	k1gInSc1	exemplář
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
na	na	k7c6	na
severu	sever	k1gInSc6	sever
ostrova	ostrov	k1gInSc2	ostrov
Dinagat	Dinagat	k1gInSc1	Dinagat
<g/>
;	;	kIx,	;
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
marně	marně	k6eAd1	marně
pátrala	pátrat	k5eAaImAgFnS	pátrat
řada	řada	k1gFnSc1	řada
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
expedic	expedice	k1gFnPc2	expedice
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
vyhynulou	vyhynulý	k2eAgFnSc4d1	vyhynulá
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
se	se	k3xPyFc4	se
tuto	tento	k3xDgFnSc4	tento
krysu	krysa	k1gFnSc4	krysa
podařilo	podařit	k5eAaPmAgNnS	podařit
vyfotografovat	vyfotografovat	k5eAaPmF	vyfotografovat
a	a	k8xC	a
natočit	natočit	k5eAaBmF	natočit
českým	český	k2eAgMnPc3d1	český
badatelům	badatel	k1gMnPc3	badatel
Václavu	Václav	k1gMnSc6	Václav
a	a	k8xC	a
Miladě	Milada	k1gFnSc6	Milada
Řehákovým	Řehákův	k2eAgNnSc7d1	Řehákův
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krysa	krysa	k1gFnSc1	krysa
dinagatská	dinagatský	k2eAgFnSc1d1	dinagatský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
ŘEHÁKOVÁ	Řeháková	k1gFnSc1	Řeháková
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
<g/>
;	;	kIx,	;
ŘEHÁK	Řehák	k1gMnSc1	Řehák
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Velemyš	Velemyš	k5eAaPmIp2nS	Velemyš
dinagatská	dinagatský	k2eAgFnSc1d1	dinagatský
-	-	kIx~	-
znovuobjevení	znovuobjevení	k1gNnPc2	znovuobjevení
a	a	k8xC	a
nové	nový	k2eAgNnSc1d1	nové
české	český	k2eAgNnSc1d1	české
jméno	jméno	k1gNnSc1	jméno
domněle	domněle	k6eAd1	domněle
vyhynulého	vyhynulý	k2eAgMnSc4d1	vyhynulý
hlodavce	hlodavec	k1gMnSc4	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Živa	Živa	k1gFnSc1	Živa
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
4812	[number]	k4	4812
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Crateromys	Crateromys	k1gInSc1	Crateromys
australis	australis	k1gInSc1	australis
(	(	kIx(	(
<g/>
krysa	krysa	k1gFnSc1	krysa
dinagatská	dinagatský	k2eAgFnSc1d1	dinagatský
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
