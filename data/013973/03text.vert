<s>
CEFTA	CEFTA	kA
</s>
<s>
vlajka	vlajka	k1gFnSc1
CEFTA	CEFTA	kA
</s>
<s>
ekonomické	ekonomický	k2eAgInPc4d1
bloky	blok	k1gInPc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
:	:	kIx,
<g/>
modře	modro	k6eAd1
-	-	kIx~
Evropská	evropský	k2eAgFnSc1d1
uniežlutě	uniežlutě	k6eAd1
-	-	kIx~
Středoevropská	středoevropský	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
volného	volný	k2eAgMnSc2d1
obchoduzeleně	obchoduzeleně	k6eAd1
-	-	kIx~
Evropské	evropský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
volného	volný	k2eAgInSc2d1
obchoduoranžově	obchoduoranžův	k2eAgFnSc3d1
-	-	kIx~
Společenství	společenství	k1gNnSc1
nezávislých	závislý	k2eNgInPc2d1
států	stát	k1gInPc2
</s>
<s>
CEFTA	CEFTA	kA
<g/>
,	,	kIx,
Central	Central	k1gFnSc1
European	European	k1gMnSc1
Free	Free	k1gInSc1
Trade	Trad	k1gInSc5
Agreement	Agreement	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
Středoevropská	středoevropský	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
sdružuje	sdružovat	k5eAaImIp3nS
některé	některý	k3yIgFnPc4
evropské	evropský	k2eAgFnPc4d1
země	zem	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
nejsou	být	k5eNaImIp3nP
členy	člen	k1gMnPc7
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jednou	jednou	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
budou	být	k5eAaImBp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1992	#num#	k4
v	v	k7c6
polském	polský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Krakově	krakův	k2eAgNnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2013	#num#	k4
měla	mít	k5eAaImAgFnS
sedm	sedm	k4xCc4
členů	člen	k1gMnPc2
<g/>
:	:	kIx,
Albánii	Albánie	k1gFnSc4
<g/>
,	,	kIx,
Bosnu	Bosna	k1gFnSc4
a	a	k8xC
Hercegovinu	Hercegovina	k1gFnSc4
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc4d1
Makedonii	Makedonie	k1gFnSc4
<g/>
,	,	kIx,
Moldavsko	Moldavsko	k1gNnSc1
<g/>
,	,	kIx,
Černou	černý	k2eAgFnSc4d1
Horu	hora	k1gFnSc4
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc4
a	a	k8xC
UNMIK	UNMIK	kA
(	(	kIx(
<g/>
Kosovo	Kosův	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česko	Česko	k1gNnSc1
bylo	být	k5eAaImAgNnS
členem	člen	k1gInSc7
od	od	k7c2
jejího	její	k3xOp3gNnSc2
založení	založení	k1gNnSc2
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
vzniku	vznik	k1gInSc2
CEFTA	CEFTA	kA
se	se	k3xPyFc4
ze	z	k7c2
států	stát	k1gInPc2
bývalého	bývalý	k2eAgInSc2d1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
v	v	k7c6
Pobaltí	Pobaltí	k1gNnSc6
zformovala	zformovat	k5eAaPmAgFnS
obdobná	obdobný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
BAFTA	BAFTA	kA
(	(	kIx(
<g/>
Baltic	Baltice	k1gFnPc2
Free	Free	k1gNnPc2
Trade	Trad	k1gInSc5
Area	area	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestala	přestat	k5eAaPmAgNnP
existovat	existovat	k5eAaImF
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
její	její	k3xOp3gInPc1
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
Litva	Litva	k1gFnSc1
<g/>
,	,	kIx,
Lotyšsko	Lotyšsko	k1gNnSc1
a	a	k8xC
Estonsko	Estonsko	k1gNnSc1
<g/>
)	)	kIx)
vstoupily	vstoupit	k5eAaPmAgInP
do	do	k7c2
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Dohoda	dohoda	k1gFnSc1
o	o	k7c6
zóně	zóna	k1gFnSc6
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
byla	být	k5eAaImAgFnS
podepsána	podepsat	k5eAaPmNgFnS
ministry	ministr	k1gMnPc7
zahraničí	zahraničí	k1gNnSc2
Visegrádské	visegrádský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
v	v	k7c6
Krakově	Krakov	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1992	#num#	k4
s	s	k7c7
platností	platnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
se	se	k3xPyFc4
volný	volný	k2eAgInSc1d1
obchod	obchod	k1gInSc1
týkal	týkat	k5eAaImAgInS
téměř	téměř	k6eAd1
všech	všecek	k3xTgInPc2
průmyslových	průmyslový	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
byla	být	k5eAaImAgFnS
úplně	úplně	k6eAd1
odstraněna	odstraněn	k2eAgNnPc4d1
cla	clo	k1gNnPc4
u	u	k7c2
zemědělských	zemědělský	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
StátVstupOdchod	StátVstupOdchod	k1gInSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
<g/>
19922004	#num#	k4
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
<g/>
19922004	#num#	k4
</s>
<s>
ČeskoslovenskoČesko	ČeskoslovenskoČesko	k6eAd1
<g/>
19922004	#num#	k4
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
Slovinsko	Slovinsko	k1gNnSc1
<g/>
19962004	#num#	k4
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
<g/>
19972007	#num#	k4
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
<g/>
19992007	#num#	k4
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
20022013	#num#	k4
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
<g/>
2006	#num#	k4
<g/>
—	—	k?
</s>
<s>
Albánie	Albánie	k1gFnSc1
<g/>
2007	#num#	k4
<g/>
—	—	k?
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
2007	#num#	k4
<g/>
—	—	k?
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
2007	#num#	k4
<g/>
—	—	k?
</s>
<s>
Kosovo	Kosův	k2eAgNnSc4d1
<g/>
2007	#num#	k4
<g/>
—	—	k?
</s>
<s>
Moldavsko	Moldavsko	k1gNnSc1
<g/>
2007	#num#	k4
<g/>
—	—	k?
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
<g/>
2007	#num#	k4
<g/>
—	—	k?
</s>
<s>
1992	#num#	k4
</s>
<s>
2003	#num#	k4
</s>
<s>
2007	#num#	k4
</s>
<s>
2013	#num#	k4
</s>
<s>
Kritéria	kritérion	k1gNnPc1
pro	pro	k7c4
členství	členství	k1gNnSc4
</s>
<s>
Původní	původní	k2eAgNnPc1d1
kritéria	kritérion	k1gNnPc1
pro	pro	k7c4
členství	členství	k1gNnSc4
podle	podle	k7c2
Poznaňské	poznaňský	k2eAgFnSc2d1
deklarace	deklarace	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
člen	člen	k1gMnSc1
světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
(	(	kIx(
<g/>
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Organisation	Organisation	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
asociační	asociační	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
s	s	k7c7
EU	EU	kA
s	s	k7c7
ustanoveními	ustanovení	k1gNnPc7
o	o	k7c6
budoucím	budoucí	k2eAgNnSc6d1
úplném	úplný	k2eAgNnSc6d1
členství	členství	k1gNnSc6
v	v	k7c6
EU	EU	kA
</s>
<s>
Sdružení	sdružení	k1gNnSc1
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
se	s	k7c7
současnými	současný	k2eAgInPc7d1
členskými	členský	k2eAgInPc7d1
státy	stát	k1gInPc7
CEFTA	CEFTA	kA
</s>
<s>
Současná	současný	k2eAgNnPc1d1
kritéria	kritérion	k1gNnPc1
podle	podle	k7c2
dohody	dohoda	k1gFnSc2
v	v	k7c6
Záhřebu	Záhřeb	k1gInSc6
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
člen	člen	k1gMnSc1
světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
(	(	kIx(
<g/>
World	World	k1gInSc1
Trade	Trad	k1gInSc5
Organisation	Organisation	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
závazek	závazek	k1gInSc4
respektování	respektování	k1gNnSc2
všech	všecek	k3xTgInPc2
předpisů	předpis	k1gInPc2
WTO	WTO	kA
</s>
<s>
jakákoli	jakýkoli	k3yIgFnSc1
asociační	asociační	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
s	s	k7c7
EU	EU	kA
</s>
<s>
Sdružení	sdružení	k1gNnSc1
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
se	s	k7c7
současnými	současný	k2eAgInPc7d1
členskými	členský	k2eAgInPc7d1
státy	stát	k1gInPc7
CEFTA	CEFTA	kA
</s>
<s>
Kritéria	kritérion	k1gNnPc1
byla	být	k5eAaImAgNnP
snížena	snížit	k5eAaPmNgNnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
a	a	k8xC
Moldavsko	Moldavsko	k1gNnSc1
mohly	moct	k5eAaImAgInP
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
organizace	organizace	k1gFnSc2
dřív	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Stredoeurópske	Stredoeurópske	k1gNnSc2
združenie	združenie	k1gFnSc2
voľného	voľný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VEBER	VEBER	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
sjednocené	sjednocený	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
645	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
663	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
Kapitola	kapitola	k1gFnSc1
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
393	#num#	k4
<g/>
–	–	k?
<g/>
394	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Evropské	evropský	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
</s>
<s>
Mercosur	Mercosur	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CEFTA	CEFTA	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Stránky	stránka	k1gFnPc1
organizace	organizace	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4515844-7	4515844-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
96121295	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
261998091	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
96121295	#num#	k4
</s>
