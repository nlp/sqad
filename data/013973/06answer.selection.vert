<s>
CEFTA	CEFTA	kA
<g/>
,	,	kIx,
Central	Central	k1gFnSc1
European	European	k1gMnSc1
Free	Free	k1gInSc1
Trade	Trad	k1gInSc5
Agreement	Agreement	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
Středoevropská	středoevropský	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
volného	volný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
sdružuje	sdružovat	k5eAaImIp3nS
některé	některý	k3yIgFnPc4
evropské	evropský	k2eAgFnPc4d1
země	zem	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
nejsou	být	k5eNaImIp3nP
členy	člen	k1gMnPc7
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jednou	jednou	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
budou	být	k5eAaImBp3nP
<g/>
.	.	kIx.
</s>