<s>
Sir	sir	k1gMnSc1	sir
Peter	Peter	k1gMnSc1	Peter
Robert	Robert	k1gMnSc1	Robert
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Pukerua	Pukeru	k2eAgFnSc1d1	Pukeru
Bay	Bay	k1gFnSc1	Bay
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
novozélandský	novozélandský	k2eAgMnSc1d1	novozélandský
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
držitel	držitel	k1gMnSc1	držitel
Oscara	Oscar	k1gMnSc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
režírování	režírování	k1gNnSc3	režírování
trilogie	trilogie	k1gFnSc2	trilogie
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
a	a	k8xC	a
Hobit	hobit	k1gMnSc1	hobit
natočené	natočený	k2eAgFnSc2d1	natočená
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
novým	nový	k2eAgNnSc7d1	nové
zpracováním	zpracování	k1gNnSc7	zpracování
filmu	film	k1gInSc2	film
King	King	k1gInSc4	King
Kong	Kongo	k1gNnPc2	Kongo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1961	[number]	k4	1961
v	v	k7c4	v
Pukerua	Pukeruus	k1gMnSc4	Pukeruus
Bay	Bay	k1gFnSc6	Bay
<g/>
,	,	kIx,	,
pobřežním	pobřežní	k2eAgNnSc6d1	pobřežní
městě	město	k1gNnSc6	město
poblíž	poblíž	k7c2	poblíž
Wellingtonu	Wellington	k1gInSc2	Wellington
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gMnPc2	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dítě	dítě	k1gNnSc4	dítě
byl	být	k5eAaImAgMnS	být
velký	velký	k2eAgMnSc1d1	velký
filmový	filmový	k2eAgMnSc1d1	filmový
fanoušek	fanoušek	k1gMnSc1	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dal	dát	k5eAaPmAgMnS	dát
známý	známý	k2eAgMnSc1d1	známý
jeho	jeho	k3xOp3gNnSc2	jeho
rodičům	rodič	k1gMnPc3	rodič
8	[number]	k4	8
mm	mm	kA	mm
filmovou	filmový	k2eAgFnSc4d1	filmová
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
natáčet	natáčet	k5eAaImF	natáčet
krátké	krátký	k2eAgInPc4d1	krátký
filmy	film	k1gInPc4	film
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
kamarády	kamarád	k1gMnPc7	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
zanechal	zanechat	k5eAaPmAgMnS	zanechat
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
v	v	k7c6	v
jako	jako	k9	jako
pomocná	pomocný	k2eAgFnSc1d1	pomocná
síla	síla	k1gFnSc1	síla
ve	v	k7c6	v
fotolaboratoři	fotolaboratoř	k1gFnSc6	fotolaboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
za	za	k7c2	za
úspory	úspora	k1gFnSc2	úspora
koupil	koupit	k5eAaPmAgMnS	koupit
16	[number]	k4	16
mm	mm	kA	mm
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
natáčet	natáčet	k5eAaImF	natáčet
sci-fi	scii	k1gFnSc4	sci-fi
komedii	komedie	k1gFnSc3	komedie
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yIgFnSc2	který
brzy	brzy	k6eAd1	brzy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
film	film	k1gInSc1	film
Bad	Bad	k1gFnSc2	Bad
Taste	tasit	k5eAaPmRp2nP	tasit
–	–	k?	–
Vesmírní	vesmírný	k2eAgMnPc1d1	vesmírný
kanibalové	kanibal	k1gMnPc1	kanibal
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
film	film	k1gInSc4	film
podepsal	podepsat	k5eAaPmAgMnS	podepsat
vícekrát	vícekrát	k6eAd1	vícekrát
–	–	k?	–
kromě	kromě	k7c2	kromě
režie	režie	k1gFnSc2	režie
<g/>
,	,	kIx,	,
scénáře	scénář	k1gInSc2	scénář
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
kamery	kamera	k1gFnSc2	kamera
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
i	i	k9	i
o	o	k7c4	o
triky	trik	k1gInPc4	trik
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
filmem	film	k1gInSc7	film
byla	být	k5eAaImAgFnS	být
drsná	drsný	k2eAgFnSc1d1	drsná
loutková	loutkový	k2eAgFnSc1d1	loutková
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
zábavní	zábavní	k2eAgInSc4d1	zábavní
průmysl	průmysl	k1gInSc4	průmysl
Meet	Meet	k1gMnSc1	Meet
The	The	k1gMnSc1	The
Feebles	Feebles	k1gMnSc1	Feebles
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přišel	přijít	k5eAaPmAgInS	přijít
Braindead	Braindead	k1gInSc1	Braindead
–	–	k?	–
Živí	živit	k5eAaImIp3nS	živit
mrtví	mrtvý	k1gMnPc1	mrtvý
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejkrvavějších	krvavý	k2eAgInPc2d3	nejkrvavější
filmů	film	k1gInPc2	film
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vymanit	vymanit	k5eAaPmF	vymanit
se	se	k3xPyFc4	se
ze	z	k7c2	z
škalulky	škalulka	k1gFnSc2	škalulka
kultovního	kultovní	k2eAgMnSc2d1	kultovní
a	a	k8xC	a
nezávislého	závislý	k2eNgMnSc2d1	nezávislý
tvůrce	tvůrce	k1gMnSc2	tvůrce
krváků	krvák	k1gInPc2	krvák
na	na	k7c4	na
vkusného	vkusný	k2eAgMnSc4d1	vkusný
a	a	k8xC	a
slavného	slavný	k2eAgMnSc4d1	slavný
filmaře	filmař	k1gMnSc4	filmař
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
seriózní	seriózní	k2eAgNnSc1d1	seriózní
drama	dramo	k1gNnPc1	dramo
Nebeská	nebeský	k2eAgNnPc1d1	nebeské
stvoření	stvoření	k1gNnPc1	stvoření
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
skutečné	skutečný	k2eAgFnSc2d1	skutečná
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
udála	udát	k5eAaPmAgFnS	udát
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
školačky	školačka	k1gFnPc1	školačka
se	se	k3xPyFc4	se
natolik	natolik	k6eAd1	natolik
zblížily	zblížit	k5eAaPmAgFnP	zblížit
<g/>
,	,	kIx,	,
že	že	k8xS	že
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
podezřívané	podezřívaný	k2eAgFnPc4d1	podezřívaná
z	z	k7c2	z
lesbického	lesbický	k2eAgInSc2d1	lesbický
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
je	on	k3xPp3gMnPc4	on
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odloučit	odloučit	k5eAaPmF	odloučit
<g/>
.	.	kIx.	.
</s>
<s>
Děvčata	děvče	k1gNnPc4	děvče
(	(	kIx(	(
<g/>
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
Kate	kat	k1gInSc5	kat
Winsletová	Winsletový	k2eAgFnSc1d1	Winsletová
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
proto	proto	k6eAd1	proto
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
zavraždit	zavraždit	k5eAaPmF	zavraždit
matku	matka	k1gFnSc4	matka
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
film	film	k1gInSc1	film
Přízraky	přízrak	k1gInPc1	přízrak
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pomyslným	pomyslný	k2eAgInSc7d1	pomyslný
krokem	krok	k1gInSc7	krok
do	do	k7c2	do
velkého	velký	k2eAgInSc2d1	velký
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
–	–	k?	–
producentem	producent	k1gMnSc7	producent
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
Robert	Robert	k1gMnSc1	Robert
Zemeckis	Zemeckis	k1gFnSc2	Zemeckis
(	(	kIx(	(
<g/>
režisér	režisér	k1gMnSc1	režisér
Návratů	návrat	k1gInPc2	návrat
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
a	a	k8xC	a
Forresta	Forrest	k1gMnSc2	Forrest
Gumpa	Gump	k1gMnSc2	Gump
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
trochu	trochu	k6eAd1	trochu
potrhlém	potrhlý	k2eAgMnSc6d1	potrhlý
mladíkovi	mladík	k1gMnSc6	mladík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
kamarádí	kamarádit	k5eAaImIp3nS	kamarádit
s	s	k7c7	s
duchy	duch	k1gMnPc7	duch
a	a	k8xC	a
patřičně	patřičně	k6eAd1	patřičně
to	ten	k3xDgNnSc1	ten
využívá	využívat	k5eAaPmIp3nS	využívat
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
finanční	finanční	k2eAgInSc4d1	finanční
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
(	(	kIx(	(
<g/>
filmová	filmový	k2eAgFnSc1d1	filmová
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnPc1	právo
na	na	k7c4	na
filmování	filmování	k1gNnSc4	filmování
Tolkienova	Tolkienův	k2eAgNnSc2d1	Tolkienovo
díla	dílo	k1gNnSc2	dílo
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Saulem	Saul	k1gMnSc7	Saul
Zaentzem	Zaentz	k1gMnSc7	Zaentz
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
měl	mít	k5eAaImAgInS	mít
dělat	dělat	k5eAaImF	dělat
dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
vměstnat	vměstnat	k5eAaPmF	vměstnat
celou	celý	k2eAgFnSc4d1	celá
trilogii	trilogie	k1gFnSc4	trilogie
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
New	New	k1gFnSc2	New
Line	linout	k5eAaImIp3nS	linout
Cinema	Cinema	k1gFnSc1	Cinema
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
přála	přát	k5eAaImAgFnS	přát
trilogii	trilogie	k1gFnSc4	trilogie
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
natáčení	natáčení	k1gNnSc1	natáčení
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
probíhalo	probíhat	k5eAaImAgNnS	probíhat
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Trilogie	trilogie	k1gFnSc1	trilogie
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
velkolepý	velkolepý	k2eAgInSc4d1	velkolepý
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
jej	on	k3xPp3gMnSc4	on
osobně	osobně	k6eAd1	osobně
vysoko	vysoko	k6eAd1	vysoko
vyhoupla	vyhoupnout	k5eAaPmAgFnS	vyhoupnout
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
nadšeným	nadšený	k2eAgInSc7d1	nadšený
ohlasem	ohlas	k1gInSc7	ohlas
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jedenáct	jedenáct	k4xCc4	jedenáct
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgMnPc7	který
byl	být	k5eAaImAgMnS	být
Oscar	Oscar	k1gMnSc1	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
a	a	k8xC	a
Oscar	Oscar	k1gInSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
Jackson	Jackson	k1gMnSc1	Jackson
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pouze	pouze	k6eAd1	pouze
šesti	šest	k4xCc2	šest
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Oscary	Oscar	k1gInPc4	Oscar
za	za	k7c4	za
produkci	produkce	k1gFnSc4	produkce
<g/>
,	,	kIx,	,
režii	režie	k1gFnSc4	režie
a	a	k8xC	a
scénář	scénář	k1gInSc4	scénář
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgMnSc6	první
ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhé	druhý	k4xOgNnSc4	druhý
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
byl	být	k5eAaImAgMnS	být
Kmotr	kmotr	k1gMnSc1	kmotr
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bad	Bad	k?	Bad
Taste	tasit	k5eAaPmRp2nP	tasit
–	–	k?	–
Vesmírní	vesmírný	k2eAgMnPc1d1	vesmírný
kanibalové	kanibal	k1gMnPc1	kanibal
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
producent	producent	k1gMnSc1	producent
Meet	Meet	k1gMnSc1	Meet
the	the	k?	the
Feebles	Feebles	k1gMnSc1	Feebles
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
producent	producent	k1gMnSc1	producent
Valley	Vallea	k1gFnSc2	Vallea
of	of	k?	of
the	the	k?	the
Stereos	Stereos	k1gInSc1	Stereos
(	(	kIx(	(
<g/>
krátký	krátký	k2eAgInSc1d1	krátký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
spoluproducent	spoluproducent	k1gMnSc1	spoluproducent
Jack	Jack	k1gMnSc1	Jack
Brown	Brown	k1gMnSc1	Brown
Genius	genius	k1gMnSc1	genius
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
producent	producent	k1gMnSc1	producent
Nebeská	nebeský	k2eAgNnPc1d1	nebeské
stvoření	stvoření	k1gNnPc1	stvoření
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
spoluproducent	spoluproducent	k1gMnSc1	spoluproducent
Přízraky	přízrak	k1gInPc1	přízrak
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
producent	producent	k1gMnSc1	producent
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
:	:	kIx,	:
Společenstvo	společenstvo	k1gNnSc1	společenstvo
Prstenu	prsten	k1gInSc2	prsten
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
režisér	režisér	k1gMnSc1	režisér
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
:	:	kIx,	:
Dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
režisér	režisér	k1gMnSc1	režisér
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
Short	Short	k1gInSc1	Short
of	of	k?	of
It	It	k1gFnSc1	It
(	(	kIx(	(
<g/>
krátký	krátký	k2eAgInSc1d1	krátký
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
výkonný	výkonný	k2eAgMnSc1d1	výkonný
producent	producent	k1gMnSc1	producent
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
režisér	režisér	k1gMnSc1	režisér
King	King	k1gMnSc1	King
Kong	Kongo	k1gNnPc2	Kongo
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
režisér	režisér	k1gMnSc1	režisér
Pevné	pevný	k2eAgNnSc1d1	pevné
pouto	pouto	k1gNnSc1	pouto
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
režisér	režisér	k1gMnSc1	režisér
District	District	k1gMnSc1	District
9	[number]	k4	9
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
producent	producent	k1gMnSc1	producent
</s>
