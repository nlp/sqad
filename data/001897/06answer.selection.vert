<s>
Centrem	centrum	k1gNnSc7	centrum
fotosyntetických	fotosyntetický	k2eAgFnPc2d1	fotosyntetická
reakcí	reakce	k1gFnPc2	reakce
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
tzv.	tzv.	kA	tzv.
thylakoidy	thylakoida	k1gFnSc2	thylakoida
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
primitivní	primitivní	k2eAgFnSc2d1	primitivní
sinice	sinice	k1gFnSc2	sinice
Gloeobacter	Gloeobactra	k1gFnPc2	Gloeobactra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gInPc4	on
nemá	mít	k5eNaImIp3nS	mít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
struktury	struktura	k1gFnPc1	struktura
totiž	totiž	k9	totiž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vlastní	vlastní	k2eAgNnPc4d1	vlastní
fotosyntetická	fotosyntetický	k2eAgNnPc4d1	fotosyntetické
barviva	barvivo	k1gNnPc4	barvivo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
nutná	nutný	k2eAgNnPc1d1	nutné
pro	pro	k7c4	pro
přeměnu	přeměna	k1gFnSc4	přeměna
světelné	světelný	k2eAgFnSc2d1	světelná
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
chemickou	chemický	k2eAgFnSc4d1	chemická
<g/>
.	.	kIx.	.
</s>
