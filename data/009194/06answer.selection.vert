<s>
Na	na	k7c4	na
dnešní	dnešní	k2eAgNnSc4d1	dnešní
jeviště	jeviště	k1gNnSc4	jeviště
Periho	Peri	k1gMnSc2	Peri
opery	opera	k1gFnSc2	opera
nejsou	být	k5eNaImIp3nP	být
často	často	k6eAd1	často
uváděny	uvádět	k5eAaImNgFnP	uvádět
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
je	být	k5eAaImIp3nS	být
přece	přece	k9	přece
jen	jen	k9	jen
poněkud	poněkud	k6eAd1	poněkud
staromódní	staromódní	k2eAgFnSc1d1	staromódní
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
mladšími	mladý	k2eAgMnPc7d2	mladší
skladateli	skladatel	k1gMnPc7	skladatel
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
Claudio	Claudio	k1gMnSc1	Claudio
Monteverdi	Monteverd	k1gMnPc1	Monteverd
<g/>
.	.	kIx.	.
</s>
