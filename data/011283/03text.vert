<p>
<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
obranné	obranný	k2eAgFnPc1d1	obranná
síly	síla	k1gFnPc1	síla
(	(	kIx(	(
<g/>
IOS	IOS	kA	IOS
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
צ	צ	k?	צ
<g/>
ְ	ְ	k?	ְ
<g/>
ב	ב	k?	ב
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
ה	ה	k?	ה
<g/>
ַ	ַ	k?	ַ
<g/>
ה	ה	k?	ה
<g/>
ֲ	ֲ	k?	ֲ
<g/>
ג	ג	k?	ג
<g/>
ָ	ָ	k?	ָ
<g/>
נ	נ	k?	נ
<g/>
ָ	ָ	k?	ָ
<g/>
ה	ה	k?	ה
ל	ל	k?	ל
<g/>
ְ	ְ	k?	ְ
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ש	ש	k?	ש
<g/>
ְ	ְ	k?	ְ
<g/>
ׂ	ׂ	k?	ׂ
<g/>
ר	ר	k?	ר
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
<g/>
ֵ	ֵ	k?	ֵ
<g/>
ל	ל	k?	ל
<g/>
,	,	kIx,	,
Cva	Cva	k1gMnSc2	Cva
ha-hagana	haagan	k1gMnSc2	ha-hagan
le-Jisrael	le-Jisrael	k1gInSc4	le-Jisrael
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Israel	Israel	k1gMnSc1	Israel
Defense	defense	k1gFnSc2	defense
Forces	Forces	k1gMnSc1	Forces
<g/>
,	,	kIx,	,
IDF	IDF	kA	IDF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
běžné	běžný	k2eAgFnSc2d1	běžná
známé	známá	k1gFnSc2	známá
pod	pod	k7c7	pod
hebrejským	hebrejský	k2eAgInSc7d1	hebrejský
akronymem	akronym	k1gInSc7	akronym
Cahal	Cahal	k1gInSc1	Cahal
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
צ	צ	k?	צ
<g/>
"	"	kIx"	"
<g/>
ל	ל	k?	ל
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
vojenského	vojenský	k2eAgNnSc2d1	vojenské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediné	jediný	k2eAgNnSc4d1	jediné
vojenské	vojenský	k2eAgNnSc4d1	vojenské
křídlo	křídlo	k1gNnSc4	křídlo
izraelských	izraelský	k2eAgFnPc2d1	izraelská
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
žádnou	žádný	k3yNgFnSc4	žádný
civilní	civilní	k2eAgFnSc4d1	civilní
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
IOS	IOS	kA	IOS
stojí	stát	k5eAaImIp3nS	stát
náčelník	náčelník	k1gMnSc1	náčelník
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
(	(	kIx(	(
<g/>
Ramatkal	Ramatkal	k1gMnSc1	Ramatkal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
podřízený	podřízený	k2eAgMnSc1d1	podřízený
ministru	ministr	k1gMnSc3	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zastává	zastávat	k5eAaImIp3nS	zastávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
funkci	funkce	k1gFnSc4	funkce
Ramatkala	Ramatkal	k1gMnSc4	Ramatkal
generálporučík	generálporučík	k1gMnSc1	generálporučík
Gadi	Gad	k1gFnSc2	Gad
Eizenkot	Eizenkot	k1gInSc1	Eizenkot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
obranné	obranný	k2eAgFnPc1d1	obranná
síly	síla	k1gFnPc1	síla
byly	být	k5eAaImAgFnP	být
zřízeny	zřízen	k2eAgInPc1d1	zřízen
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
premiéra	premiér	k1gMnSc2	premiér
Davida	David	k1gMnSc2	David
Ben	Ben	k1gInSc1	Ben
Guriona	Guriona	k1gFnSc1	Guriona
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vojenské	vojenský	k2eAgFnSc2d1	vojenská
organizace	organizace	k1gFnSc2	organizace
Hagana	Hagan	k1gMnSc2	Hagan
a	a	k8xC	a
militantních	militantní	k2eAgFnPc2d1	militantní
skupin	skupina	k1gFnPc2	skupina
Irgun	Irgun	k1gNnSc4	Irgun
a	a	k8xC	a
Lechi	Lechi	k1gNnSc4	Lechi
<g/>
.	.	kIx.	.
</s>
<s>
Bránily	bránit	k5eAaImAgInP	bránit
Izrael	Izrael	k1gInSc4	Izrael
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
prošel	projít	k5eAaPmAgMnS	projít
<g/>
:	:	kIx,	:
válkou	válka	k1gFnSc7	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
sinajskou	sinajský	k2eAgFnSc7d1	Sinajská
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
šestidenní	šestidenní	k2eAgFnSc7d1	šestidenní
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
opotřebovací	opotřebovací	k2eAgFnSc7d1	opotřebovací
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
jomkipurskou	jomkipurský	k2eAgFnSc7d1	jomkipurská
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
první	první	k4xOgFnSc7	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
libanonskou	libanonský	k2eAgFnSc7d1	libanonská
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
původně	původně	k6eAd1	původně
IOS	IOS	kA	IOS
operovaly	operovat	k5eAaImAgFnP	operovat
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
frontách	fronta	k1gFnPc6	fronta
–	–	k?	–
proti	proti	k7c3	proti
Libanonu	Libanon	k1gInSc3	Libanon
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc3	Sýrie
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Jordánsku	Jordánsko	k1gNnSc6	Jordánsko
a	a	k8xC	a
Iráku	Irák	k1gInSc6	Irák
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Egyptu	Egypt	k1gInSc3	Egypt
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
–	–	k?	–
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
egyptsko-izraelské	egyptskozraelský	k2eAgFnSc2d1	egyptsko-izraelská
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc2	její
aktivity	aktivita	k1gFnSc2	aktivita
zaměřily	zaměřit	k5eAaPmAgInP	zaměřit
především	především	k6eAd1	především
na	na	k7c4	na
jižní	jižní	k2eAgInSc4d1	jižní
Libanon	Libanon	k1gInSc4	Libanon
a	a	k8xC	a
palestinská	palestinský	k2eAgNnPc4d1	palestinské
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
IOS	IOS	kA	IOS
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
zapojily	zapojit	k5eAaPmAgFnP	zapojit
do	do	k7c2	do
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
intifády	intifáda	k1gFnSc2	intifáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
obranné	obranný	k2eAgFnPc1d1	obranná
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgFnPc2d1	ostatní
armád	armáda	k1gFnPc2	armáda
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
v	v	k7c6	v
povinné	povinný	k2eAgFnSc6d1	povinná
vojenské	vojenský	k2eAgFnSc6d1	vojenská
službě	služba	k1gFnSc6	služba
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
struktuře	struktura	k1gFnSc6	struktura
a	a	k8xC	a
v	v	k7c6	v
úzkých	úzký	k2eAgInPc6d1	úzký
stycích	styk	k1gInPc6	styk
mezi	mezi	k7c7	mezi
pozemním	pozemní	k2eAgNnSc7d1	pozemní
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
letectvem	letectvo	k1gNnSc7	letectvo
a	a	k8xC	a
námořnictvem	námořnictvo	k1gNnSc7	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
se	se	k3xPyFc4	se
IOS	IOS	kA	IOS
snažily	snažit	k5eAaImAgInP	snažit
být	být	k5eAaImF	být
jedinečnou	jedinečný	k2eAgFnSc7d1	jedinečná
armádou	armáda	k1gFnSc7	armáda
přesně	přesně	k6eAd1	přesně
vyhovující	vyhovující	k2eAgMnSc1d1	vyhovující
specifickým	specifický	k2eAgFnPc3d1	specifická
izraelským	izraelský	k2eAgFnPc3d1	izraelská
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
IOS	IOS	kA	IOS
používají	používat	k5eAaImIp3nP	používat
mnohé	mnohé	k1gNnSc4	mnohé
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
vyvinuté	vyvinutý	k2eAgFnSc2d1	vyvinutá
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
mnoho	mnoho	k6eAd1	mnoho
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
právě	právě	k9	právě
pro	pro	k7c4	pro
specifické	specifický	k2eAgFnPc4d1	specifická
potřeby	potřeba	k1gFnPc4	potřeba
této	tento	k3xDgFnSc2	tento
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
hlavní	hlavní	k2eAgInSc1d1	hlavní
bojový	bojový	k2eAgInSc1d1	bojový
tank	tank	k1gInSc1	tank
Merkava	Merkava	k1gFnSc1	Merkava
<g/>
,	,	kIx,	,
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
hi-tech	hi	k1gInPc6	hi-t
zbrojní	zbrojní	k2eAgInPc4d1	zbrojní
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
samopaly	samopal	k1gInPc4	samopal
Uzi	Uzi	k1gFnSc2	Uzi
<g/>
,	,	kIx,	,
poloautomatická	poloautomatický	k2eAgFnSc1d1	poloautomatická
pistole	pistole	k1gFnSc1	pistole
Desert	desert	k1gInSc4	desert
Eagle	Eagle	k1gFnSc2	Eagle
<g/>
,	,	kIx,	,
útočné	útočný	k2eAgFnSc2d1	útočná
pušky	puška	k1gFnSc2	puška
Galil	Galila	k1gFnPc2	Galila
či	či	k8xC	či
Tavor	Tavora	k1gFnPc2	Tavora
<g/>
.	.	kIx.	.
</s>
<s>
IOS	IOS	kA	IOS
udržují	udržovat	k5eAaImIp3nP	udržovat
úzké	úzký	k2eAgInPc1d1	úzký
vojenské	vojenský	k2eAgInPc1d1	vojenský
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
spolupráci	spolupráce	k1gFnSc3	spolupráce
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
zbraňových	zbraňový	k2eAgInPc2d1	zbraňový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
například	například	k6eAd1	například
letoun	letoun	k1gInSc1	letoun
F-	F-	k1gFnSc2	F-
<g/>
15	[number]	k4	15
<g/>
I	I	kA	I
<g/>
,	,	kIx,	,
taktický	taktický	k2eAgInSc1d1	taktický
laserový	laserový	k2eAgInSc1d1	laserový
výkonový	výkonový	k2eAgInSc1d1	výkonový
systém	systém	k1gInSc1	systém
THEL	THEL	kA	THEL
či	či	k8xC	či
systém	systém	k1gInSc1	systém
protiraketové	protiraketový	k2eAgFnSc2d1	protiraketová
obrany	obrana	k1gFnSc2	obrana
Arrow	Arrow	k1gFnSc2	Arrow
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Izraelské	izraelský	k2eAgFnSc2d1	izraelská
obranné	obranný	k2eAgFnSc2d1	obranná
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
צ	צ	k?	צ
<g/>
ְ	ְ	k?	ְ
<g/>
ב	ב	k?	ב
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
ה	ה	k?	ה
<g/>
ַ	ַ	k?	ַ
<g/>
ה	ה	k?	ה
<g/>
ֲ	ֲ	k?	ֲ
<g/>
ג	ג	k?	ג
<g/>
ָ	ָ	k?	ָ
<g/>
נ	נ	k?	נ
<g/>
ָ	ָ	k?	ָ
<g/>
ה	ה	k?	ה
ל	ל	k?	ל
<g/>
ְ	ְ	k?	ְ
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ש	ש	k?	ש
<g/>
ְ	ְ	k?	ְ
<g/>
ׂ	ׂ	k?	ׂ
<g/>
ר	ר	k?	ר
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
<g/>
ֵ	ֵ	k?	ֵ
<g/>
ל	ל	k?	ל
<g/>
,	,	kIx,	,
Cva	Cva	k1gMnSc2	Cva
ha-hagana	haagan	k1gMnSc2	ha-hagan
le-Jisrael	le-Jisrael	k1gInSc4	le-Jisrael
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
obranná	obranný	k2eAgFnSc1d1	obranná
armáda	armáda	k1gFnSc1	armáda
Izraele	Izrael	k1gInSc2	Izrael
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
vládou	vláda	k1gFnSc7	vláda
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tato	tento	k3xDgFnSc1	tento
armáda	armáda	k1gFnSc1	armáda
z	z	k7c2	z
rozkazu	rozkaz	k1gInSc2	rozkaz
Davida	David	k1gMnSc2	David
Ben	Ben	k1gInSc4	Ben
Guriona	Guriona	k1gFnSc1	Guriona
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
hlavní	hlavní	k2eAgFnSc7d1	hlavní
navrhovanou	navrhovaný	k2eAgFnSc7d1	navrhovaná
alternativou	alternativa	k1gFnSc7	alternativa
názvu	název	k1gInSc2	název
byla	být	k5eAaImAgFnS	být
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
či	či	k8xC	či
Armáda	armáda	k1gFnSc1	armáda
Izraele	Izrael	k1gInSc2	Izrael
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
צ	צ	k?	צ
<g/>
ְ	ְ	k?	ְ
<g/>
ב	ב	k?	ב
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ש	ש	k?	ש
<g/>
ְ	ְ	k?	ְ
<g/>
ׂ	ׂ	k?	ׂ
<g/>
ר	ר	k?	ר
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
<g/>
ֵ	ֵ	k?	ֵ
<g/>
ל	ל	k?	ל
<g/>
,	,	kIx,	,
Cva	Cva	k1gMnSc1	Cva
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
existovaly	existovat	k5eAaImAgInP	existovat
i	i	k9	i
návrhy	návrh	k1gInPc1	návrh
jako	jako	k8xC	jako
Židovská	židovský	k2eAgFnSc1d1	židovská
armáda	armáda	k1gFnSc1	armáda
či	či	k8xC	či
Armáda	armáda	k1gFnSc1	armáda
Státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
vybrán	vybrat	k5eAaPmNgInS	vybrat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
účelem	účel	k1gInSc7	účel
armády	armáda	k1gFnSc2	armáda
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
obrana	obrana	k1gFnSc1	obrana
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
název	název	k1gInSc4	název
Hagana	Hagan	k1gMnSc2	Hagan
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
podzemní	podzemní	k2eAgFnSc4d1	podzemní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
IOS	IOS	kA	IOS
vzešla	vzejít	k5eAaPmAgFnS	vzejít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
autorství	autorství	k1gNnSc3	autorství
či	či	k8xC	či
přispění	přispění	k1gNnSc4	přispění
k	k	k7c3	k
názvu	název	k1gInSc3	název
se	se	k3xPyFc4	se
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgMnPc1	tři
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
výkonný	výkonný	k2eAgMnSc1d1	výkonný
ředitel	ředitel	k1gMnSc1	ředitel
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
David	David	k1gMnSc1	David
Jisra	Jisra	k1gMnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
eli	eli	k?	eli
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Levi	Lev	k1gMnSc3	Lev
Eškolovi	Eškol	k1gMnSc3	Eškol
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gNnSc4	on
dále	daleko	k6eAd2	daleko
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Náčelník	náčelník	k1gInSc1	náčelník
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
Ja	Ja	k?	Ja
<g/>
'	'	kIx"	'
<g/>
akov	akov	k1gMnSc1	akov
Dori	Dor	k1gFnSc2	Dor
sice	sice	k8xC	sice
netvrdí	tvrdit	k5eNaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
převažovalo	převažovat	k5eAaImAgNnS	převažovat
mezi	mezi	k7c7	mezi
staršími	starý	k2eAgMnPc7d2	starší
důstojníky	důstojník	k1gMnPc7	důstojník
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
přirozený	přirozený	k2eAgInSc4d1	přirozený
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgMnSc7	třetí
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
Ben	Ben	k1gInSc1	Ben
Gurion	Gurion	k1gInSc4	Gurion
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
je	být	k5eAaImIp3nS	být
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
Izraelské	izraelský	k2eAgFnPc4d1	izraelská
obranné	obranný	k2eAgFnPc4d1	obranná
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c7	mezi
hlavními	hlavní	k2eAgMnPc7d1	hlavní
oponenty	oponent	k1gMnPc7	oponent
současného	současný	k2eAgInSc2d1	současný
názvu	název	k1gInSc2	název
byli	být	k5eAaImAgMnP	být
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
Chajim-Moše	Chajim-Moš	k1gMnSc2	Chajim-Moš
Šapira	Šapira	k1gMnSc1	Šapira
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
ha-Cora	ha-Cora	k1gFnSc1	ha-Cora
<g/>
;	;	kIx,	;
oba	dva	k4xCgMnPc1	dva
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
název	název	k1gInSc1	název
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
IOS	IOS	kA	IOS
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
aby	aby	kYmCp3nP	aby
chránily	chránit	k5eAaImAgInP	chránit
jeho	jeho	k3xOp3gFnSc1	jeho
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
bojovali	bojovat	k5eAaImAgMnP	bojovat
proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
formám	forma	k1gFnPc3	forma
terorismu	terorismus	k1gInSc3	terorismus
ohrožujících	ohrožující	k2eAgInPc2d1	ohrožující
každodenní	každodenní	k2eAgInSc4d1	každodenní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
IOS	IOS	kA	IOS
jsou	být	k5eAaImIp3nP	být
následnicí	následnice	k1gFnSc7	následnice
Hagany	Hagana	k1gFnSc2	Hagana
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
její	její	k3xOp3gFnPc1	její
elitní	elitní	k2eAgFnPc1d1	elitní
úderné	úderný	k2eAgFnPc1d1	úderná
jednotky	jednotka	k1gFnPc1	jednotka
Palmach	Palmacha	k1gFnPc2	Palmacha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
stálá	stálý	k2eAgFnSc1d1	stálá
armáda	armáda	k1gFnSc1	armáda
židovského	židovský	k2eAgInSc2d1	židovský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
rovněž	rovněž	k9	rovněž
někteří	některý	k3yIgMnPc1	některý
bývalí	bývalý	k2eAgMnPc1d1	bývalý
příslušníci	příslušník	k1gMnPc1	příslušník
Židovské	židovský	k2eAgFnSc2d1	židovská
brigády	brigáda	k1gFnSc2	brigáda
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
bojovali	bojovat	k5eAaImAgMnP	bojovat
pod	pod	k7c7	pod
britskou	britský	k2eAgFnSc7d1	britská
vlajkou	vlajka	k1gFnSc7	vlajka
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
IOS	IOS	kA	IOS
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojily	spojit	k5eAaPmAgFnP	spojit
dvě	dva	k4xCgFnPc1	dva
židovské	židovský	k2eAgFnPc1d1	židovská
podzemní	podzemní	k2eAgFnPc1d1	podzemní
organizace	organizace	k1gFnPc1	organizace
–	–	k?	–
Irgun	Irgun	k1gNnSc1	Irgun
a	a	k8xC	a
Lechi	Lechi	k1gNnSc1	Lechi
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohly	moct	k5eAaImAgInP	moct
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
působit	působit	k5eAaImF	působit
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
organizace	organizace	k1gFnPc1	organizace
rozpuštěny	rozpuštěn	k2eAgFnPc1d1	rozpuštěna
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
začleněni	začlenit	k5eAaPmNgMnP	začlenit
pod	pod	k7c4	pod
IOS	IOS	kA	IOS
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
IOS	IOS	kA	IOS
se	se	k3xPyFc4	se
utvářela	utvářet	k5eAaImAgFnS	utvářet
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
až	až	k9	až
1956	[number]	k4	1956
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
z	z	k7c2	z
regionálních	regionální	k2eAgInPc2d1	regionální
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
arabskými	arabský	k2eAgMnPc7d1	arabský
sousedy	soused	k1gMnPc7	soused
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1956	[number]	k4	1956
až	až	k9	až
1966	[number]	k4	1966
bylo	být	k5eAaImAgNnS	být
konfliktů	konflikt	k1gInPc2	konflikt
méně	málo	k6eAd2	málo
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
čas	čas	k1gInSc4	čas
IOS	IOS	kA	IOS
využila	využít	k5eAaPmAgFnS	využít
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
nového	nový	k2eAgNnSc2d1	nové
vybavení	vybavení	k1gNnSc2	vybavení
a	a	k8xC	a
přerodu	přerod	k1gInSc2	přerod
z	z	k7c2	z
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
armády	armáda	k1gFnSc2	armáda
na	na	k7c4	na
profesionální	profesionální	k2eAgFnSc4d1	profesionální
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
rovněž	rovněž	k9	rovněž
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrodu	zrod	k1gInSc3	zrod
izraelského	izraelský	k2eAgInSc2d1	izraelský
jaderného	jaderný	k2eAgInSc2d1	jaderný
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
IOS	IOS	kA	IOS
stala	stát	k5eAaPmAgFnS	stát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
vybavených	vybavený	k2eAgNnPc2d1	vybavené
a	a	k8xC	a
bojem	boj	k1gInSc7	boj
prošlých	prošlý	k2eAgFnPc2d1	prošlá
armád	armáda	k1gFnPc2	armáda
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Povinná	povinný	k2eAgFnSc1d1	povinná
služba	služba	k1gFnSc1	služba
===	===	k?	===
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
povinná	povinný	k2eAgFnSc1d1	povinná
pro	pro	k7c4	pro
židovské	židovský	k2eAgInPc4d1	židovský
a	a	k8xC	a
drúzské	drúzský	k2eAgMnPc4d1	drúzský
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
židovské	židovský	k2eAgFnPc4d1	židovská
ženy	žena	k1gFnPc4	žena
starší	starý	k2eAgFnPc4d2	starší
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
výjimky	výjimka	k1gFnPc4	výjimka
na	na	k7c6	na
základě	základ	k1gInSc6	základ
náboženských	náboženský	k2eAgFnPc2d1	náboženská
<g/>
,	,	kIx,	,
fyzických	fyzický	k2eAgFnPc2d1	fyzická
či	či	k8xC	či
psychologických	psychologický	k2eAgFnPc2d1	psychologická
příčin	příčina	k1gFnPc2	příčina
a	a	k8xC	a
například	například	k6eAd1	například
pro	pro	k7c4	pro
vdané	vdaný	k2eAgFnPc4d1	vdaná
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
Profil	profil	k1gInSc1	profil
21	[number]	k4	21
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vykonána	vykonat	k5eAaPmNgFnS	vykonat
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muži	muž	k1gMnPc1	muž
slouží	sloužit	k5eAaImIp3nP	sloužit
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ženy	žena	k1gFnPc1	žena
dvacet	dvacet	k4xCc4	dvacet
dva	dva	k4xCgInPc4	dva
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
IOS	IOS	kA	IOS
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přihlásí	přihlásit	k5eAaPmIp3nP	přihlásit
do	do	k7c2	do
bojových	bojový	k2eAgFnPc2d1	bojová
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
sloužit	sloužit	k5eAaImF	sloužit
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
výcvik	výcvik	k1gInSc1	výcvik
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
pozicích	pozice	k1gFnPc6	pozice
trvá	trvat	k5eAaImIp3nS	trvat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
pozicích	pozice	k1gFnPc6	pozice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
programátorky	programátorka	k1gFnPc1	programátorka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rovněž	rovněž	k9	rovněž
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
delší	dlouhý	k2eAgInSc4d2	delší
výcvik	výcvik	k1gInSc4	výcvik
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
sloužit	sloužit	k5eAaImF	sloužit
po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
bojových	bojový	k2eAgFnPc6d1	bojová
pozicích	pozice	k1gFnPc6	pozice
i	i	k8xC	i
po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
slouží	sloužit	k5eAaImIp3nS	sloužit
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
rezervistky	rezervistka	k1gFnSc2	rezervistka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Služba	služba	k1gFnSc1	služba
u	u	k7c2	u
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
stráže	stráž	k1gFnSc2	stráž
===	===	k?	===
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
vojáci	voják	k1gMnPc1	voják
IOS	IOS	kA	IOS
mohou	moct	k5eAaImIp3nP	moct
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
povinné	povinný	k2eAgFnSc2d1	povinná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
sloužit	sloužit	k5eAaImF	sloužit
u	u	k7c2	u
Hraniční	hraniční	k2eAgFnSc2d1	hraniční
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
Mišmar	Mišmar	k1gInSc1	Mišmar
haGvul	haGvul	k1gInSc1	haGvul
<g/>
,	,	kIx,	,
znám	znám	k2eAgInSc1d1	znám
je	být	k5eAaImIp3nS	být
akronym	akronym	k1gInSc1	akronym
Magav	Magava	k1gFnPc2	Magava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sekce	sekce	k1gFnSc1	sekce
Izraelské	izraelský	k2eAgFnSc2d1	izraelská
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vojáci	voják	k1gMnPc1	voják
projdou	projít	k5eAaPmIp3nP	projít
základním	základní	k2eAgInSc7d1	základní
bojovým	bojový	k2eAgInSc7d1	bojový
výcvikem	výcvik	k1gInSc7	výcvik
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
projít	projít	k5eAaPmF	projít
dalším	další	k2eAgInSc7d1	další
protiteroristickým	protiteroristický	k2eAgInSc7d1	protiteroristický
a	a	k8xC	a
pohraničním	pohraniční	k2eAgInSc7d1	pohraniční
výcvikem	výcvik	k1gInSc7	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jsou	být	k5eAaImIp3nP	být
přiřazeni	přiřadit	k5eAaPmNgMnP	přiřadit
k	k	k7c3	k
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
Hraniční	hraniční	k2eAgFnSc2d1	hraniční
policie	policie	k1gFnSc2	policie
bojují	bojovat	k5eAaImIp3nP	bojovat
bok	bok	k1gInSc4	bok
po	po	k7c6	po
boku	bok	k1gInSc6	bok
s	s	k7c7	s
bojovými	bojový	k2eAgFnPc7d1	bojová
jednotkami	jednotka	k1gFnPc7	jednotka
IOS	IOS	kA	IOS
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
odpovědné	odpovědný	k2eAgNnSc4d1	odpovědné
za	za	k7c4	za
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
silně	silně	k6eAd1	silně
zalidněných	zalidněný	k2eAgFnPc2d1	zalidněná
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
sekce	sekce	k1gFnPc1	sekce
IOS	IOS	kA	IOS
jsou	být	k5eAaImIp3nP	být
podřízeny	podřízen	k2eAgFnPc1d1	podřízena
Generálnímu	generální	k2eAgInSc3d1	generální
štábu	štáb	k1gInSc3	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Náčelník	náčelník	k1gInSc1	náčelník
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
sloužící	sloužící	k2eAgMnSc1d1	sloužící
důstojník	důstojník	k1gMnSc1	důstojník
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
generálporučík	generálporučík	k1gMnSc1	generálporučík
(	(	kIx(	(
<g/>
Rav	Rav	k1gMnSc1	Rav
Aluf	Aluf	k1gMnSc1	Aluf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
ministru	ministr	k1gMnSc3	ministr
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
pak	pak	k6eAd1	pak
izraelskému	izraelský	k2eAgMnSc3d1	izraelský
premiérovi	premiér	k1gMnSc3	premiér
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Náčelník	náčelník	k1gInSc1	náčelník
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
formálně	formálně	k6eAd1	formálně
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
vládou	vláda	k1gFnSc7	vláda
na	na	k7c4	na
tříleté	tříletý	k2eAgNnSc4d1	tříleté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vláda	vláda	k1gFnSc1	vláda
může	moct	k5eAaImIp3nS	moct
hlasováním	hlasování	k1gNnSc7	hlasování
toto	tento	k3xDgNnSc4	tento
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
prodloužit	prodloužit	k5eAaPmF	prodloužit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
za	za	k7c2	za
vzácných	vzácný	k2eAgFnPc2d1	vzácná
okolností	okolnost	k1gFnPc2	okolnost
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgInSc7d1	současný
náčelníkem	náčelník	k1gInSc7	náčelník
štábu	štáb	k1gInSc2	štáb
je	být	k5eAaImIp3nS	být
Gadi	Gad	k1gInSc3	Gad
Eizenkot	Eizenkot	k1gInSc1	Eizenkot
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
ve	v	k7c4	v
funkci	funkce	k1gFnSc4	funkce
nahradil	nahradit	k5eAaPmAgInS	nahradit
Benjamina	Benjamin	k1gMnSc4	Benjamin
Gance	Gance	k1gMnSc4	Gance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
obranné	obranný	k2eAgFnPc1d1	obranná
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
následujících	následující	k2eAgInPc2d1	následující
sborů	sbor	k1gInPc2	sbor
(	(	kIx(	(
<g/>
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mají	mít	k5eAaImIp3nP	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
Generálním	generální	k2eAgInSc6d1	generální
štábu	štáb	k1gInSc6	štáb
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
tučně	tučně	k6eAd1	tučně
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
sbory	sbor	k1gInPc1	sbor
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInPc1d1	následující
sbory	sbor	k1gInPc1	sbor
a	a	k8xC	a
společnosti	společnost	k1gFnPc1	společnost
s	s	k7c7	s
IOS	IOS	kA	IOS
úzce	úzko	k6eAd1	úzko
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nenáleží	náležet	k5eNaImIp3nS	náležet
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
náleží	náležet	k5eAaImIp3nS	náležet
pouze	pouze	k6eAd1	pouze
částečně	částečně	k6eAd1	částečně
<g/>
)	)	kIx)	)
k	k	k7c3	k
jejím	její	k3xOp3gFnPc3	její
formálním	formální	k2eAgFnPc3d1	formální
strukturám	struktura	k1gFnPc3	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hodnosti	hodnost	k1gFnPc4	hodnost
a	a	k8xC	a
insignie	insignie	k1gFnPc4	insignie
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Hodnosti	hodnost	k1gFnSc6	hodnost
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
světových	světový	k2eAgFnPc2d1	světová
armád	armáda	k1gFnPc2	armáda
jsou	být	k5eAaImIp3nP	být
hodnosti	hodnost	k1gFnSc2	hodnost
v	v	k7c4	v
IOS	IOS	kA	IOS
společné	společný	k2eAgNnSc1d1	společné
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
sbory	sbor	k1gInPc4	sbor
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
letectva	letectvo	k1gNnSc2	letectvo
a	a	k8xC	a
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Poddůstojnické	poddůstojnický	k2eAgFnPc1d1	poddůstojnická
hodnosti	hodnost	k1gFnPc1	hodnost
jsou	být	k5eAaImIp3nP	být
udíleny	udílet	k5eAaImNgFnP	udílet
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
strávenou	strávený	k2eAgFnSc7d1	strávená
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
za	za	k7c4	za
úspěchy	úspěch	k1gInPc4	úspěch
či	či	k8xC	či
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Insignie	insignie	k1gFnPc4	insignie
====	====	k?	====
</s>
</p>
<p>
<s>
Vojáci	voják	k1gMnPc1	voják
IOS	IOS	kA	IOS
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
insignií	insignie	k1gFnPc2	insignie
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgFnSc2d1	jiná
než	než	k8xS	než
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
lze	lze	k6eAd1	lze
poznat	poznat	k5eAaPmF	poznat
jejich	jejich	k3xOp3gInSc4	jejich
sbor	sbor	k1gInSc4	sbor
<g/>
,	,	kIx,	,
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
jednotku	jednotka	k1gFnSc4	jednotka
a	a	k8xC	a
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbory	sbor	k1gInPc4	sbor
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
podle	podle	k7c2	podle
odznaků	odznak	k1gInPc2	odznak
na	na	k7c6	na
beretách	bereta	k1gFnPc6	bereta
<g/>
.	.	kIx.	.
</s>
<s>
Vojáky	voják	k1gMnPc7	voják
sloužící	sloužící	k1gFnSc2	sloužící
ve	v	k7c6	v
štábech	štáb	k1gInPc6	štáb
na	na	k7c6	na
nižší	nízký	k2eAgFnSc6d2	nižší
úrovni	úroveň	k1gFnSc6	úroveň
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
často	často	k6eAd1	často
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
podle	podle	k7c2	podle
odznaku	odznak	k1gInSc2	odznak
jejich	jejich	k3xOp3gInSc2	jejich
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
pod	pod	k7c4	pod
něj	on	k3xPp3gMnSc4	on
oficiálně	oficiálně	k6eAd1	oficiálně
přímo	přímo	k6eAd1	přímo
nepatří	patřit	k5eNaImIp3nS	patřit
<g/>
,	,	kIx,	,
či	či	k8xC	či
podle	podle	k7c2	podle
odznaku	odznak	k1gInSc2	odznak
nejbližšího	blízký	k2eAgInSc2d3	Nejbližší
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Noví	nový	k2eAgMnPc1d1	nový
branci	branec	k1gMnPc1	branec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
prochází	procházet	k5eAaImIp3nP	procházet
základním	základní	k2eAgInSc7d1	základní
výcvikem	výcvik	k1gInSc7	výcvik
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
odznak	odznak	k1gInSc4	odznak
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
berety	bereta	k1gFnSc2	bereta
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
příslušnosti	příslušnost	k1gFnSc2	příslušnost
ke	k	k7c3	k
sboru	sbor	k1gInSc3	sbor
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
většina	většina	k1gFnSc1	většina
nebojových	bojový	k2eNgInPc2d1	nebojový
sborů	sbor	k1gInPc2	sbor
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
berety	bereta	k1gFnPc4	bereta
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
používají	používat	k5eAaImIp3nP	používat
barvu	barva	k1gFnSc4	barva
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
základně	základna	k1gFnSc6	základna
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
jednotky	jednotka	k1gFnPc1	jednotka
lze	lze	k6eAd1	lze
identifikovat	identifikovat	k5eAaBmF	identifikovat
podle	podle	k7c2	podle
nárameníku	nárameník	k1gInSc2	nárameník
připevněného	připevněný	k2eAgMnSc2d1	připevněný
k	k	k7c3	k
ramennímu	ramenní	k2eAgInSc3d1	ramenní
popruhu	popruh	k1gInSc3	popruh
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jednotek	jednotka	k1gFnPc2	jednotka
IOS	IOS	kA	IOS
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
štítek	štítek	k1gInSc4	štítek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
pozice	pozice	k1gFnSc1	pozice
či	či	k8xC	či
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
voják	voják	k1gMnSc1	voják
zastává	zastávat	k5eAaImIp3nS	zastávat
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
snadno	snadno	k6eAd1	snadno
rozpoznána	rozpoznat	k5eAaPmNgFnS	rozpoznat
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc4	dva
volitelné	volitelný	k2eAgInPc4d1	volitelný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tuto	tento	k3xDgFnSc4	tento
identifikaci	identifikace	k1gFnSc4	identifikace
mohou	moct	k5eAaImIp3nP	moct
usnadnit	usnadnit	k5eAaPmF	usnadnit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
barevnou	barevný	k2eAgFnSc4d1	barevná
šňůrku	šňůrka	k1gFnSc4	šňůrka
připevněnou	připevněný	k2eAgFnSc4d1	připevněná
k	k	k7c3	k
levému	levý	k2eAgInSc3d1	levý
nárameníku	nárameník	k1gInSc3	nárameník
a	a	k8xC	a
kapse	kapsa	k1gFnSc3	kapsa
uniformy	uniforma	k1gFnSc2	uniforma
a	a	k8xC	a
odznak	odznak	k1gInSc4	odznak
sdělující	sdělující	k2eAgNnSc4d1	sdělující
vojákovo	vojákův	k2eAgNnSc4d1	vojákovo
pracovní	pracovní	k2eAgNnSc4d1	pracovní
zaměření	zaměření	k1gNnSc4	zaměření
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
voják	voják	k1gMnSc1	voják
obvykle	obvykle	k6eAd1	obvykle
obdrží	obdržet	k5eAaPmIp3nS	obdržet
po	po	k7c6	po
profesním	profesní	k2eAgInSc6d1	profesní
kurzu	kurz	k1gInSc6	kurz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
odznaky	odznak	k1gInPc1	odznak
mohou	moct	k5eAaImIp3nP	moct
sdělovat	sdělovat	k5eAaImF	sdělovat
příslušnost	příslušnost	k1gFnSc4	příslušnost
ke	k	k7c3	k
sboru	sbor	k1gInSc3	sbor
či	či	k8xC	či
dodatečně	dodatečně	k6eAd1	dodatečně
absolvované	absolvovaný	k2eAgInPc4d1	absolvovaný
kurzy	kurz	k1gInPc4	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
může	moct	k5eAaImIp3nS	moct
voják	voják	k1gMnSc1	voják
na	na	k7c6	na
uniformě	uniforma	k1gFnSc6	uniforma
mít	mít	k5eAaImF	mít
odznak	odznak	k1gInSc4	odznak
(	(	kIx(	(
<g/>
nášivku	nášivek	k1gInSc2	nášivek
<g/>
)	)	kIx)	)
sdělující	sdělující	k2eAgNnSc1d1	sdělující
jaké	jaký	k3yRgFnPc4	jaký
války	válka	k1gFnPc4	válka
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Israel	Israel	k1gInSc1	Israel
Defense	defense	k1gFnSc2	defense
Forces	Forcesa	k1gFnPc2	Forcesa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Jozue	Jozue	k1gMnSc1	Jozue
</s>
</p>
<p>
<s>
Operace	operace	k1gFnSc1	operace
Šalamoun	Šalamoun	k1gMnSc1	Šalamoun
</s>
</p>
<p>
<s>
Sajeret	Sajeret	k1gMnSc1	Sajeret
Matkal	Matkal	k1gMnSc1	Matkal
</s>
</p>
<p>
<s>
Sajeret	Sajeret	k1gMnSc1	Sajeret
</s>
</p>
<p>
<s>
Hagana	Hagana	k1gFnSc1	Hagana
</s>
</p>
<p>
<s>
Gilad	Gilad	k6eAd1	Gilad
Šalit	Šalit	k2eAgInSc1d1	Šalit
</s>
</p>
<p>
<s>
Gadna	Gadna	k6eAd1	Gadna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Izraelské	izraelský	k2eAgFnSc2d1	izraelská
obranné	obranný	k2eAgFnSc2d1	obranná
síly	síla	k1gFnSc2	síla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
IDF	IDF	kA	IDF
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Video	video	k1gNnSc1	video
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
