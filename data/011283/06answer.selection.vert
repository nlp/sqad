<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
obranné	obranný	k2eAgFnPc1d1	obranná
síly	síla	k1gFnPc1	síla
(	(	kIx(	(
<g/>
IOS	IOS	kA	IOS
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
צ	צ	k?	צ
<g/>
ְ	ְ	k?	ְ
<g/>
ב	ב	k?	ב
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
ה	ה	k?	ה
<g/>
ַ	ַ	k?	ַ
<g/>
ה	ה	k?	ה
<g/>
ֲ	ֲ	k?	ֲ
<g/>
ג	ג	k?	ג
<g/>
ָ	ָ	k?	ָ
<g/>
נ	נ	k?	נ
<g/>
ָ	ָ	k?	ָ
<g/>
ה	ה	k?	ה
ל	ל	k?	ל
<g/>
ְ	ְ	k?	ְ
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ש	ש	k?	ש
<g/>
ְ	ְ	k?	ְ
<g/>
ׂ	ׂ	k?	ׂ
<g/>
ר	ר	k?	ר
<g/>
ָ	ָ	k?	ָ
<g/>
א	א	k?	א
<g/>
ֵ	ֵ	k?	ֵ
<g/>
ל	ל	k?	ל
<g/>
,	,	kIx,	,
Cva	Cva	k1gMnSc2	Cva
ha-hagana	haagan	k1gMnSc2	ha-hagan
le-Jisrael	le-Jisrael	k1gInSc4	le-Jisrael
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Israel	Israel	k1gMnSc1	Israel
Defense	defense	k1gFnSc2	defense
Forces	Forces	k1gMnSc1	Forces
<g/>
,	,	kIx,	,
IDF	IDF	kA	IDF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
běžné	běžný	k2eAgFnSc2d1	běžná
známé	známá	k1gFnSc2	známá
pod	pod	k7c7	pod
hebrejským	hebrejský	k2eAgInSc7d1	hebrejský
akronymem	akronym	k1gInSc7	akronym
Cahal	Cahal	k1gInSc1	Cahal
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
צ	צ	k?	צ
<g/>
"	"	kIx"	"
<g/>
ל	ל	k?	ל
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
vojenského	vojenský	k2eAgNnSc2d1	vojenské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
