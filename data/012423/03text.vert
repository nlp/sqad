<p>
<s>
Rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Ľudovíta	Ľudovít	k1gMnSc2	Ľudovít
Štúra	Štúr	k1gMnSc2	Štúr
a	a	k8xC	a
Alexandra	Alexandr	k1gMnSc2	Alexandr
Dubčeka	Dubčeek	k1gMnSc2	Dubčeek
je	být	k5eAaImIp3nS	být
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Uhrovec	Uhrovec	k1gInSc4	Uhrovec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
okresu	okres	k1gInSc2	okres
Bánovce	Bánovec	k1gInSc2	Bánovec
nad	nad	k7c7	nad
Bebravou	Bebrava	k1gFnSc7	Bebrava
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
místo	místo	k1gNnSc4	místo
narození	narození	k1gNnSc2	narození
Ľudovíta	Ľudovít	k1gMnSc2	Ľudovít
Štúra	Štúr	k1gMnSc2	Štúr
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kodifikátora	kodifikátor	k1gMnSc4	kodifikátor
spisovné	spisovný	k2eAgFnSc2d1	spisovná
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
významné	významný	k2eAgFnPc4d1	významná
osobnosti	osobnost	k1gFnPc4	osobnost
politického	politický	k2eAgNnSc2d1	politické
dění	dění	k1gNnSc2	dění
Alexandra	Alexandr	k1gMnSc2	Alexandr
Dubčeka	Dubčeek	k1gMnSc2	Dubčeek
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921	[number]	k4	1921
až	až	k9	až
24	[number]	k4	24
<g/>
,	,	kIx,	,
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
a	a	k8xC	a
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
učitelským	učitelský	k2eAgInSc7d1	učitelský
bytem	byt	k1gInSc7	byt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
expozice	expozice	k1gFnPc4	expozice
Trenčínského	trenčínský	k2eAgNnSc2d1	trenčínské
muzea	muzeum	k1gNnSc2	muzeum
věnované	věnovaný	k2eAgNnSc4d1	věnované
zejména	zejména	k9	zejména
Štúrovi	Štúrův	k2eAgMnPc1d1	Štúrův
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
i	i	k9	i
Dubčekovi	Dubčekův	k2eAgMnPc1d1	Dubčekův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Rodný	rodný	k2eAgInSc4d1	rodný
dom	dom	k?	dom
Ľudovíta	Ľudovít	k1gMnSc2	Ľudovít
Štúra	Štúr	k1gMnSc2	Štúr
a	a	k8xC	a
Alexandra	Alexandr	k1gMnSc2	Alexandr
Dubčeka	Dubčeek	k1gMnSc2	Dubčeek
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
obce	obec	k1gFnSc2	obec
Uhrovec	Uhrovec	k1gInSc1	Uhrovec
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Dům	dům	k1gInSc1	dům
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Trenčínského	trenčínský	k2eAgInSc2d1	trenčínský
muzea	muzeum	k1gNnPc1	muzeum
</s>
</p>
