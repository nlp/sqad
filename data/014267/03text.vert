<s>
Rotunda	rotunda	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
znojemské	znojemský	k2eAgFnSc6d1
rotundě	rotunda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
rotundě	rotunda	k1gFnSc6
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
Třebové	Třebová	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
Rotunda	rotunda	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
Třebová	Třebová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rotunda	rotunda	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
Místo	místo	k1gNnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Architektonický	architektonický	k2eAgInSc1d1
popis	popis	k1gInSc1
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc4
</s>
<s>
románská	románský	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Hradní	hradní	k2eAgInSc1d1
Kód	kód	k1gInSc1
památky	památka	k1gFnSc2
</s>
<s>
11796	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
6944	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc1d1
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Rotunda	rotunda	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
stojí	stát	k5eAaImIp3nS
v	v	k7c6
areálu	areál	k1gInSc6
přemyslovského	přemyslovský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
mnoha	mnoho	k4c2
rotund	rotunda	k1gFnPc2
postavených	postavený	k2eAgFnPc2d1
v	v	k7c6
raném	raný	k2eAgInSc6d1
středověku	středověk	k1gInSc6
na	na	k7c6
území	území	k1gNnSc6
Moravy	Morava	k1gFnSc2
a	a	k8xC
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
zachovanými	zachovaný	k2eAgFnPc7d1
románskými	románský	k2eAgFnPc7d1
nástěnnými	nástěnný	k2eAgFnPc7d1
malbami	malba	k1gFnPc7
z	z	k7c2
roku	rok	k1gInSc2
1134	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
malby	malba	k1gFnPc1
jsou	být	k5eAaImIp3nP
výjimečné	výjimečný	k2eAgFnPc1d1
v	v	k7c6
celém	celý	k2eAgNnSc6d1
románském	románský	k2eAgNnSc6d1
umění	umění	k1gNnSc6
Evropy	Evropa	k1gFnSc2
svým	svůj	k3xOyFgNnSc7
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
ideologii	ideologie	k1gFnSc4
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1
průzkum	průzkum	k1gInSc1
doložil	doložit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
lokalita	lokalita	k1gFnSc1
byla	být	k5eAaImAgFnS
osídlena	osídlit	k5eAaPmNgFnS
již	již	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
bronzové	bronzový	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přímo	přímo	k6eAd1
na	na	k7c6
místě	místo	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
rotundy	rotunda	k1gFnSc2
byly	být	k5eAaImAgFnP
nalezeny	nalezen	k2eAgFnPc1d1
stopy	stopa	k1gFnPc1
lehčího	lehký	k2eAgInSc2d2
<g/>
,	,	kIx,
do	do	k7c2
skály	skála	k1gFnSc2
zasekaného	zasekaný	k2eAgInSc2d1
objektu	objekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
skalnatá	skalnatý	k2eAgFnSc1d1
vyvýšenina	vyvýšenina	k1gFnSc1
měla	mít	k5eAaImAgFnS
již	již	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
kultovní	kultovní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Románská	románský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
velmi	velmi	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
znojemského	znojemský	k2eAgInSc2d1
hradu	hrad	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
uprostřed	uprostřed	k7c2
rozsáhlého	rozsáhlý	k2eAgNnSc2d1
předhradí	předhradí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládalo	předpokládat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
jako	jako	k9
součást	součást	k1gFnSc1
zmíněného	zmíněný	k2eAgInSc2d1
hradu	hrad	k1gInSc2
vybudována	vybudovat	k5eAaPmNgFnS
Břetislavem	Břetislav	k1gMnSc7
I.	I.	kA
<g/>
,	,	kIx,
buď	buď	k8xC
během	během	k7c2
jeho	jeho	k3xOp3gFnSc2
moravské	moravský	k2eAgFnSc2d1
nebo	nebo	k8xC
pozdější	pozdní	k2eAgFnSc2d2
české	český	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
nejdříve	dříve	k6eAd3
však	však	k9
roku	rok	k1gInSc2
1019	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
Přemyslovci	Přemyslovec	k1gMnPc1
Morava	Morava	k1gFnSc1
dobyta	dobyt	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prozatím	prozatím	k6eAd1
také	také	k9
nebyla	být	k5eNaImAgFnS
potvrzena	potvrzen	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
byla	být	k5eAaImAgFnS
rotunda	rotunda	k1gFnSc1
i	i	k9
celý	celý	k2eAgInSc4d1
hrad	hrad	k1gInSc4
vybudována	vybudován	k2eAgFnSc1d1
až	až	k9
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
až	až	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozdělení	rozdělení	k1gNnSc3
Moravy	Morava	k1gFnSc2
na	na	k7c4
tři	tři	k4xCgNnPc4
údělná	údělný	k2eAgNnPc4d1
přemyslovská	přemyslovský	k2eAgNnPc4d1
knížectví	knížectví	k1gNnPc4
(	(	kIx(
<g/>
brněnské	brněnský	k2eAgNnSc4d1
<g/>
,	,	kIx,
znojemské	znojemský	k2eAgNnSc4d1
a	a	k8xC
olomoucké	olomoucký	k2eAgNnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
novějších	nový	k2eAgInPc2d2
vědeckých	vědecký	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
stavba	stavba	k1gFnSc1
rotundy	rotunda	k1gFnSc2
i	i	k9
malby	malba	k1gFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
za	za	k7c4
panování	panování	k1gNnSc4
knížete	kníže	k1gMnSc2
Konráda	Konrád	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemského	znojemský	k2eAgInSc2d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
zasvěcení	zasvěcení	k1gNnSc2
svaté	svatý	k2eAgFnSc6d1
Kateřině	Kateřina	k1gFnSc6
Alexandrijské	alexandrijský	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Nápis	nápis	k1gInSc1
v	v	k7c6
omítce	omítka	k1gFnSc6
</s>
<s>
Rotunda	rotunda	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
hradní	hradní	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
restaurátorských	restaurátorský	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
prováděných	prováděný	k2eAgNnPc2d1
Františkem	František	k1gMnSc7
Fišerem	Fišer	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
byl	být	k5eAaImAgInS
objeven	objevit	k5eAaPmNgInS
v	v	k7c6
omítce	omítka	k1gFnSc6
vyrytý	vyrytý	k2eAgInSc1d1
latinský	latinský	k2eAgInSc1d1
nápis	nápis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
částečné	částečný	k2eAgNnSc4d1
poškození	poškození	k1gNnSc4
a	a	k8xC
běžnou	běžný	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
vynechávání	vynechávání	k1gNnSc2
hlásek	hláska	k1gFnPc2
není	být	k5eNaImIp3nS
interpretace	interpretace	k1gFnSc1
nápisu	nápis	k1gInSc2
jednoznačná	jednoznačný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
převládajícího	převládající	k2eAgInSc2d1
výkladu	výklad	k1gInSc2
se	se	k3xPyFc4
však	však	k9
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
roku	rok	k1gInSc2
1134	#num#	k4
kníže	kníže	k1gMnSc1
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemský	znojemský	k2eAgInSc1d1
zakladatel	zakladatel	k1gMnSc1
<g/>
,	,	kIx,
upravil	upravit	k5eAaPmAgMnS
statut	statut	k1gInSc4
svatyně	svatyně	k1gFnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
a	a	k8xC
zasvětil	zasvětit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
svaté	svatý	k2eAgFnSc3d1
Kateřině	Kateřina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nápis	nápis	k1gInSc1
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
písemným	písemný	k2eAgInSc7d1
dokladem	doklad	k1gInSc7
o	o	k7c6
původním	původní	k2eAgNnSc6d1
uvažovaném	uvažovaný	k2eAgNnSc6d1
zasvěcení	zasvěcení	k1gNnSc6
rotundy	rotunda	k1gFnSc2
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
<g/>
,	,	kIx,
v	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
vznikl	vzniknout	k5eAaPmAgInS
snad	snad	k9
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
jako	jako	k8xC,k8xS
opis	opis	k1gInSc4
staršího	starý	k2eAgInSc2d2
originálního	originální	k2eAgInSc2d1
textu	text	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
povýšení	povýšení	k1gNnSc6
Znojma	Znojmo	k1gNnSc2
na	na	k7c4
město	město	k1gNnSc4
byla	být	k5eAaImAgFnS
stavba	stavba	k1gFnSc1
roku	rok	k1gInSc2
1226	#num#	k4
jako	jako	k8xS,k8xC
filiální	filiální	k2eAgInSc1d1
kostel	kostel	k1gInSc1
podřízena	podřízen	k2eAgNnPc1d1
farnímu	farní	k2eAgInSc3d1
kostelu	kostel	k1gInSc3
svatého	svatý	k2eAgMnSc2d1
Mikuláše	Mikuláš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1287	#num#	k4
byla	být	k5eAaImAgFnS
filiálním	filiální	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
k	k	k7c3
farnímu	farní	k2eAgInSc3d1
kostelu	kostel	k1gInSc3
svatého	svatý	k2eAgMnSc2d1
Michala	Michal	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
rotundy	rotunda	k1gFnSc2
<g/>
)	)	kIx)
roku	rok	k1gInSc2
1320	#num#	k4
Jindřich	Jindřich	k1gMnSc1
z	z	k7c2
Lipé	Lipá	k1gFnSc2
odstoupil	odstoupit	k5eAaPmAgMnS
klášteru	klášter	k1gInSc3
jeptišek	jeptiška	k1gFnPc2
svaté	svatý	k2eAgFnSc2d1
Kláry	Klára	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1551	#num#	k4
získalo	získat	k5eAaPmAgNnS
faru	fara	k1gFnSc4
u	u	k7c2
svatého	svatý	k2eAgMnSc2d1
Michala	Michal	k1gMnSc2
město	město	k1gNnSc4
<g/>
,	,	kIx,
kterému	který	k3yIgInSc3,k3yQgInSc3,k3yRgInSc3
následně	následně	k6eAd1
klarisky	klariska	k1gFnSc2
roku	rok	k1gInSc2
1555	#num#	k4
prodaly	prodat	k5eAaPmAgFnP
i	i	k9
rotundu	rotunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
byla	být	k5eAaImAgFnS
užívána	užívat	k5eAaImNgFnS
v	v	k7c6
následujícím	následující	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1710	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
předhradí	předhradí	k1gNnSc6
zřízen	zřídit	k5eAaPmNgInS
měšťanský	měšťanský	k2eAgInSc1d1
pivovar	pivovar	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
jehož	jehož	k3xOyRp3gNnSc6
budování	budování	k1gNnSc6
byly	být	k5eAaImAgFnP
strženy	strhnout	k5eAaPmNgFnP
všechny	všechen	k3xTgFnPc1
budovy	budova	k1gFnPc1
kromě	kromě	k7c2
rotundy	rotunda	k1gFnSc2
(	(	kIx(
<g/>
označené	označený	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
Heidentempl	Heidentempl	k1gFnSc1
=	=	kIx~
pohanský	pohanský	k2eAgInSc1d1
chrám	chrám	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
velmi	velmi	k6eAd1
staré	starý	k2eAgFnPc1d1
osmiboké	osmiboký	k2eAgFnPc1d1
Loupežnické	loupežnický	k2eAgFnPc1d1
věže	věž	k1gFnPc1
(	(	kIx(
<g/>
Räuberturm	Räuberturm	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
v	v	k7c6
rotundě	rotunda	k1gFnSc6
umístěn	umístit	k5eAaPmNgInS
chlév	chlév	k1gInSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
stavebním	stavební	k2eAgFnPc3d1
úpravám	úprava	k1gFnPc3
<g/>
,	,	kIx,
po	po	k7c6
nichž	jenž	k3xRgFnPc6
rotunda	rotunda	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
tančírna	tančírna	k1gFnSc1
a	a	k8xC
výčep	výčep	k1gInSc1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
1879	#num#	k4
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
doložena	doložit	k5eAaPmNgFnS
košíkářská	košíkářský	k2eAgFnSc1d1
dílna	dílna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1888	#num#	k4
byla	být	k5eAaImAgFnS
rotunda	rotunda	k1gFnSc1
rekonstruována	rekonstruovat	k5eAaBmNgFnS
a	a	k8xC
následně	následně	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1891	#num#	k4
<g/>
–	–	k?
<g/>
93	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvnímu	první	k4xOgNnSc3
restaurování	restaurování	k1gNnSc3
maleb	malba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc4d1
restaurování	restaurování	k1gNnSc1
maleb	malba	k1gFnPc2
proběhlo	proběhnout	k5eAaPmAgNnS
v	v	k7c6
letech	léto	k1gNnPc6
1938	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
–	–	k?
<g/>
49,196	49,196	k4
<g/>
9	#num#	k4
a	a	k8xC
1971	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladší	mladý	k2eAgFnPc1d2
stavební	stavební	k2eAgFnPc1d1
úpravy	úprava	k1gFnPc1
rotundy	rotunda	k1gFnSc2
proběhly	proběhnout	k5eAaPmAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1966	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
a	a	k8xC
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
maleb	malba	k1gFnPc2
</s>
<s>
Oráčská	oráčský	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
ve	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
pásu	pás	k1gInSc2
</s>
<s>
Příčiny	příčina	k1gFnPc4
vzniku	vznik	k1gInSc2
výjimečných	výjimečný	k2eAgFnPc2d1
maleb	malba	k1gFnPc2
obsahující	obsahující	k2eAgInSc1d1
panovnický	panovnický	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
v	v	k7c6
rotundě	rotunda	k1gFnSc6
jsou	být	k5eAaImIp3nP
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
objasněny	objasněn	k2eAgInPc1d1
zjištěním	zjištění	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
byly	být	k5eAaImAgFnP
vytvořeny	vytvořit	k5eAaPmNgFnP
ke	k	k7c3
svatbě	svatba	k1gFnSc3
Konráda	Konrád	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemského	znojemský	k2eAgMnSc2d1
s	s	k7c7
Marií	Maria	k1gFnSc7
Srbskou	srbský	k2eAgFnSc7d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
konala	konat	k5eAaImAgFnS
roku	rok	k1gInSc2
1134	#num#	k4
v	v	k7c6
době	doba	k1gFnSc6
svatodušních	svatodušní	k2eAgInPc2d1
svátků	svátek	k1gInPc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
malířská	malířský	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
lodi	loď	k1gFnSc2
rotundy	rotunda	k1gFnSc2
vrcholí	vrcholit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
dva	dva	k4xCgMnPc1
svatebčané	svatebčan	k1gMnPc1
jsou	být	k5eAaImIp3nP
vymalování	vymalování	k1gNnSc4
na	na	k7c6
triumfálním	triumfální	k2eAgInSc6d1
oblouku	oblouk	k1gInSc6
(	(	kIx(
<g/>
vítězném	vítězný	k2eAgInSc6d1
oblouku	oblouk	k1gInSc6
<g/>
)	)	kIx)
<g/>
v	v	k7c6
interiéru	interiér	k1gInSc6
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
vévoda	vévoda	k1gMnSc1
Soběslav	Soběslav	k1gFnSc1
I.	I.	kA
totiž	totiž	k9
nezískal	získat	k5eNaPmAgInS
trůn	trůn	k1gInSc4
roku	rok	k1gInSc2
1125	#num#	k4
právem	právem	k6eAd1
(	(	kIx(
<g/>
podle	podle	k7c2
stařešinského	stařešinský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
Břetislava	Břetislav	k1gMnSc2
I.	I.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
si	se	k3xPyFc3
vládu	vláda	k1gFnSc4
pojistit	pojistit	k5eAaPmF
a	a	k8xC
během	během	k7c2
prvních	první	k4xOgNnPc2
let	léto	k1gNnPc2
vlády	vláda	k1gFnSc2
nechal	nechat	k5eAaPmAgInS
zatknout	zatknout	k5eAaPmF
a	a	k8xC
uvěznit	uvěznit	k5eAaPmF
všechny	všechen	k3xTgMnPc4
ostatní	ostatní	k2eAgMnPc4d1
Přemyslovce	Přemyslovec	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
žádný	žádný	k3yNgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
nemohl	moct	k5eNaImAgMnS
hlásit	hlásit	k5eAaImF
o	o	k7c4
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražský	pražský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
se	se	k3xPyFc4
rovněž	rovněž	k9
snažil	snažit	k5eAaImAgMnS
zajistit	zajistit	k5eAaPmF
svým	svůj	k3xOyFgMnPc3
synům	syn	k1gMnPc3
navzdory	navzdory	k6eAd1
stařešinskému	stařešinský	k2eAgInSc3d1
řádu	řád	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soběslav	Soběslav	k1gMnSc1
také	také	k9
vládl	vládnout	k5eAaImAgMnS
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
mínění	mínění	k1gNnSc4
velmožů	velmož	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
proti	proti	k7c3
němu	on	k3xPp3gInSc3
roku	rok	k1gInSc3
1130	#num#	k4
připravili	připravit	k5eAaPmAgMnP
spiknutí	spiknutí	k1gNnSc4
s	s	k7c7
úmyslem	úmysl	k1gInSc7
ho	on	k3xPp3gInSc2
zabít	zabít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povstání	povstání	k1gNnSc1
sice	sice	k8xC
bylo	být	k5eAaImAgNnS
odhaleno	odhalen	k2eAgNnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
strůjci	strůjce	k1gMnPc1
potrestáni	potrestat	k5eAaPmNgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
Soběslav	Soběslav	k1gMnSc1
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
změnit	změnit	k5eAaPmF
způsob	způsob	k1gInSc4
vládnutí	vládnutí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smířil	smířit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
Přemyslovci	Přemyslovec	k1gMnPc7
<g/>
,	,	kIx,
moravským	moravský	k2eAgNnSc7d1
vrátil	vrátit	k5eAaPmAgInS
jejich	jejich	k3xOp3gInPc7
úděly	úděl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sblížil	sblížit	k5eAaPmAgMnS
se	se	k3xPyFc4
více	hodně	k6eAd2
s	s	k7c7
Konrádem	Konrád	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemským	znojemský	k2eAgNnSc7d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
ten	ten	k3xDgMnSc1
měl	mít	k5eAaImAgMnS
podle	podle	k7c2
stařešinského	stařešinský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
po	po	k7c6
Soběslavovi	Soběslav	k1gMnSc6
nastoupit	nastoupit	k5eAaPmF
na	na	k7c4
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgInS
s	s	k7c7
ním	on	k3xPp3gInSc7
dohodu	dohoda	k1gFnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
níž	jenž	k3xRgFnSc2
dostal	dostat	k5eAaPmAgMnS
Konrád	Konrád	k1gMnSc1
příslib	příslib	k1gInSc4
nástupnictví	nástupnictví	k1gNnSc2
po	po	k7c6
Soběslavovi	Soběslav	k1gMnSc6
výměnou	výměna	k1gFnSc7
za	za	k7c4
opatrovnictví	opatrovnictví	k1gNnSc4
Soběslavových	Soběslavův	k2eAgMnPc2d1
synů	syn	k1gMnPc2
<g/>
,	,	kIx,
kterým	který	k3yRgMnSc7,k3yQgMnSc7,k3yIgMnSc7
měl	mít	k5eAaImAgMnS
po	po	k7c6
sobě	se	k3xPyFc3
zajistit	zajistit	k5eAaPmF
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
dohoda	dohoda	k1gFnSc1
byla	být	k5eAaImAgFnS
uzavřena	uzavřít	k5eAaPmNgFnS
při	při	k7c6
příležitosti	příležitost	k1gFnSc6
svatby	svatba	k1gFnSc2
Konráda	Konrád	k1gMnSc2
a	a	k8xC
Marie	Maria	k1gFnSc2
Srbské	srbský	k2eAgFnSc2d1
(	(	kIx(
<g/>
dcery	dcera	k1gFnSc2
srbského	srbský	k2eAgMnSc4d1
vládce	vládce	k1gMnSc4
Uroše	Uroš	k1gMnSc2
I.	I.	kA
<g/>
)	)	kIx)
roku	rok	k1gInSc2
1134	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následná	následný	k2eAgFnSc1d1
výmalba	výmalba	k1gFnSc1
rotundy	rotunda	k1gFnSc2
postavami	postava	k1gFnPc7
přemyslovských	přemyslovský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
a	a	k8xC
pověstí	pověst	k1gFnPc2
o	o	k7c6
vzniku	vznik	k1gInSc6
dynastie	dynastie	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
státu	stát	k1gInSc2
<g/>
)	)	kIx)
měla	mít	k5eAaImAgFnS
symbolizovat	symbolizovat	k5eAaImF
Konrádův	Konrádův	k2eAgInSc4d1
nárok	nárok	k1gInSc4
na	na	k7c4
pražský	pražský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
dávají	dávat	k5eAaImIp3nP
malby	malba	k1gFnPc1
souvislosti	souvislost	k1gFnPc1
s	s	k7c7
oslavami	oslava	k1gFnPc7
stého	stý	k4xOgNnSc2
výročí	výročí	k1gNnPc2
panování	panování	k1gNnSc2
Přemyslovců	Přemyslovec	k1gMnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
jako	jako	k9
zdůraznění	zdůraznění	k1gNnSc3
principu	princip	k1gInSc2
Translatio	Translatio	k6eAd1
Regni	Regen	k2eAgMnPc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
přenesení	přenesení	k1gNnSc1
koruny	koruna	k1gFnSc2
z	z	k7c2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
do	do	k7c2
přemyslovských	přemyslovský	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
princip	princip	k1gInSc1
měl	mít	k5eAaImAgInS
podpořit	podpořit	k5eAaPmF
snahu	snaha	k1gFnSc4
o	o	k7c4
získání	získání	k1gNnSc4
a	a	k8xC
vysvětlit	vysvětlit	k5eAaPmF
původ	původ	k1gInSc4
královského	královský	k2eAgInSc2d1
titulu	titul	k1gInSc2
(	(	kIx(
<g/>
velkomoravský	velkomoravský	k2eAgMnSc1d1
Svatopluk	Svatopluk	k1gMnSc1
I.	I.	kA
byl	být	k5eAaImAgMnS
již	již	k6eAd1
papežem	papež	k1gMnSc7
titulován	titulovat	k5eAaImNgMnS
jako	jako	k9
král	král	k1gMnSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
pro	pro	k7c4
přemyslovský	přemyslovský	k2eAgInSc4d1
rod	rod	k1gInSc4
o	o	k7c4
dvě	dva	k4xCgNnPc4
století	století	k1gNnPc2
později	pozdě	k6eAd2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
zdůvodnit	zdůvodnit	k5eAaPmF
nárok	nárok	k1gInSc4
na	na	k7c4
pražské	pražský	k2eAgNnSc4d1
arcibiskupství	arcibiskupství	k1gNnSc4
(	(	kIx(
<g/>
získal	získat	k5eAaPmAgInS
ho	on	k3xPp3gNnSc4
až	až	k6eAd1
Lucemburk	Lucemburk	k1gMnSc1
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
rotundy	rotunda	k1gFnSc2
</s>
<s>
Rotunda	rotunda	k1gFnSc1
na	na	k7c6
skalní	skalní	k2eAgFnSc6d1
vyvýšenině	vyvýšenina	k1gFnSc6
</s>
<s>
Rotundu	rotunda	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nS
válcová	válcový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
je	být	k5eAaImIp3nS
na	na	k7c6
východě	východ	k1gInSc6
připojena	připojen	k2eAgFnSc1d1
apsida	apsida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
postavena	postaven	k2eAgFnSc1d1
na	na	k7c6
vyvýšeném	vyvýšený	k2eAgInSc6d1
skalním	skalní	k2eAgInSc6d1
výběžku	výběžek	k1gInSc6
a	a	k8xC
je	být	k5eAaImIp3nS
neobvyklá	obvyklý	k2eNgFnSc1d1
i	i	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
vstupuje	vstupovat	k5eAaImIp3nS
po	po	k7c6
schodišti	schodiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interiér	interiér	k1gInSc1
osvětlují	osvětlovat	k5eAaImIp3nP
drobná	drobný	k2eAgNnPc1d1
románská	románský	k2eAgNnPc1d1
okénka	okénko	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdivo	zdivo	k1gNnSc1
netvoří	tvořit	k5eNaImIp3nP
klasické	klasický	k2eAgInPc1d1
románské	románský	k2eAgInPc1d1
kvádříky	kvádřík	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
nepravidelné	pravidelný	k2eNgInPc1d1
tmavé	tmavý	k2eAgInPc1d1
lomové	lomový	k2eAgInPc1d1
kameny	kámen	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
však	však	k9
velmi	velmi	k6eAd1
pečlivě	pečlivě	k6eAd1
řádkovány	řádkován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
zdivu	zdivo	k1gNnSc6
se	se	k3xPyFc4
dochovaly	dochovat	k5eAaPmAgFnP
zazděné	zazděný	k2eAgFnPc1d1
kapsy	kapsa	k1gFnPc1
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
ukotvení	ukotvení	k1gNnSc3
trámů	trám	k1gInPc2
lešení	lešení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
stavebního	stavební	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
byla	být	k5eAaImAgFnS
klenba	klenba	k1gFnSc1
nad	nad	k7c7
lodí	loď	k1gFnSc7
prováděna	prováděn	k2eAgFnSc1d1
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
návaznosti	návaznost	k1gFnSc6
na	na	k7c6
zaklenutí	zaklenutí	k1gNnSc6
vítězného	vítězný	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
a	a	k8xC
rovněž	rovněž	k9
zdi	zeď	k1gFnSc3
stavby	stavba	k1gFnSc2
byly	být	k5eAaImAgInP
od	od	k7c2
počátku	počátek	k1gInSc2
provedeny	provést	k5eAaPmNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
unesly	unést	k5eAaPmAgInP
tíhu	tíha	k1gFnSc4
kupole	kupole	k1gFnSc2
lodi	loď	k1gFnSc2
<g/>
,	,	kIx,
<g/>
což	což	k3yRnSc1,k3yQnSc1
potvrzuje	potvrzovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
rotunda	rotunda	k1gFnSc1
byla	být	k5eAaImAgFnS
vystavěna	vystavět	k5eAaPmNgFnS
v	v	k7c6
jedné	jeden	k4xCgFnSc6
stavební	stavební	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
na	na	k7c6
střeše	střecha	k1gFnSc6
lucerna	lucerna	k1gFnSc1
(	(	kIx(
<g/>
tj.	tj.	kA
samostatně	samostatně	k6eAd1
zastřešený	zastřešený	k2eAgMnSc1d1
a	a	k8xC
okny	okno	k1gNnPc7
opatřený	opatřený	k2eAgInSc4d1
architektonický	architektonický	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
přitom	přitom	k6eAd1
prokazatelně	prokazatelně	k6eAd1
nesloužila	sloužit	k5eNaImAgFnS
k	k	k7c3
osvětlení	osvětlení	k1gNnSc3
interiéru	interiér	k1gInSc2
rotundy	rotunda	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
klenbě	klenba	k1gFnSc6
rotundy	rotunda	k1gFnSc2
bylo	být	k5eAaImAgNnS
pouze	pouze	k6eAd1
sedm	sedm	k4xCc1
malých	malý	k2eAgInPc2d1
otvorů	otvor	k1gInPc2
<g/>
,	,	kIx,
symbolizujících	symbolizující	k2eAgMnPc2d1
spolu	spolu	k6eAd1
s	s	k7c7
holubicí	holubice	k1gFnSc7
Seslání	seslání	k1gNnPc2
ducha	duch	k1gMnSc2
svatého	svatý	k1gMnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Sedm	sedm	k4xCc1
darů	dar	k1gInPc2
ducha	duch	k1gMnSc2
svatého	svatý	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Interiér	interiér	k1gInSc1
rotundy	rotunda	k1gFnSc2
je	být	k5eAaImIp3nS
celoplošně	celoplošně	k6eAd1
pokryt	pokryt	k2eAgInSc1d1
mimořádně	mimořádně	k6eAd1
cennou	cenný	k2eAgFnSc7d1
románskou	románský	k2eAgFnSc7d1
malířskou	malířský	k2eAgFnSc7d1
výzdobou	výzdoba	k1gFnSc7
(	(	kIx(
<g/>
1134	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
maleb	malba	k1gFnPc2
</s>
<s>
Malby	malba	k1gFnPc1
<g/>
,	,	kIx,
malované	malovaný	k2eAgFnPc1d1
technikou	technika	k1gFnSc7
secco	secco	k6eAd1
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
suché	suchý	k2eAgFnSc2d1
omítky	omítka	k1gFnSc2
<g/>
,	,	kIx,
zaplňují	zaplňovat	k5eAaImIp3nP
celou	celý	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
vnitřních	vnitřní	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
rotundy	rotunda	k1gFnSc2
v	v	k7c6
apsidě	apsida	k1gFnSc6
i	i	k8xC
hlavní	hlavní	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
od	od	k7c2
podlahy	podlaha	k1gFnSc2
až	až	k9
do	do	k7c2
kupole	kupole	k1gFnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
uspořádány	uspořádat	k5eAaPmNgInP
v	v	k7c6
několika	několik	k4yIc6
souběžných	souběžný	k2eAgInPc6d1
pásech	pás	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
malířská	malířský	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
je	být	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
činí	činit	k5eAaImIp3nS
rotundu	rotunda	k1gFnSc4
tak	tak	k6eAd1
ojedinělou	ojedinělý	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
identifikace	identifikace	k1gFnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
světských	světský	k2eAgFnPc2d1
postav	postava	k1gFnPc2
je	být	k5eAaImIp3nS
poněkud	poněkud	k6eAd1
nejasná	jasný	k2eNgFnSc1d1
<g/>
,	,	kIx,
i	i	k8xC
mezi	mezi	k7c7
historiky	historik	k1gMnPc7
existuje	existovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
odlišných	odlišný	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snahou	snaha	k1gFnSc7
je	být	k5eAaImIp3nS
podle	podle	k7c2
atributů	atribut	k1gInPc2
postav	postava	k1gFnPc2
(	(	kIx(
<g/>
pláště	plášť	k1gInPc1
<g/>
,	,	kIx,
kopí	kopí	k1gNnPc1
<g/>
,	,	kIx,
praporce	praporec	k1gInPc1
<g/>
,	,	kIx,
štíty	štít	k1gInPc1
<g/>
,	,	kIx,
pokrývky	pokrývka	k1gFnPc1
hlavy	hlava	k1gFnSc2
/	/	kIx~
<g/>
helmice	helmice	k1gFnSc1
<g/>
,	,	kIx,
čelenka	čelenka	k1gFnSc1
<g/>
,	,	kIx,
myrta	myrta	k1gFnSc1
<g/>
/	/	kIx~
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInPc2
vzájemných	vzájemný	k2eAgInPc2d1
postojů	postoj	k1gInPc2
a	a	k8xC
umístění	umístění	k1gNnSc2
na	na	k7c6
čestných	čestný	k2eAgFnPc6d1
<g/>
,	,	kIx,
či	či	k8xC
méně	málo	k6eAd2
čestných	čestný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
identifikovat	identifikovat	k5eAaBmF
jednotlivé	jednotlivý	k2eAgMnPc4d1
panovníky	panovník	k1gMnPc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
mýtická	mýtický	k2eAgNnPc4d1
knížata	kníže	k1gNnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
moravské	moravský	k2eAgMnPc4d1
údělníky	údělník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
jmen	jméno	k1gNnPc2
<g/>
,	,	kIx,
vztahující	vztahující	k2eAgNnPc1d1
se	se	k3xPyFc4
k	k	k7c3
malbám	malba	k1gFnPc3
je	být	k5eAaImIp3nS
tedy	tedy	k9
třeba	třeba	k6eAd1
brát	brát	k5eAaImF
s	s	k7c7
jistou	jistý	k2eAgFnSc7d1
obezřetností	obezřetnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Apsida	apsida	k1gFnSc1
</s>
<s>
Ve	v	k7c6
spodním	spodní	k2eAgInSc6d1
pásu	pás	k1gInSc6
je	být	k5eAaImIp3nS
vymalována	vymalován	k2eAgFnSc1d1
textilní	textilní	k2eAgFnSc1d1
drapérie	drapérie	k1gFnSc1
<g/>
,	,	kIx,
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
pás	pás	k1gInSc1
s	s	k7c7
postavami	postava	k1gFnPc7
dvanácti	dvanáct	k4xCc2
apoštolů	apoštol	k1gMnPc2
pod	pod	k7c7
arkádami	arkáda	k1gFnPc7
<g/>
,	,	kIx,
nad	nad	k7c7
druhým	druhý	k4xOgInSc7
pásem	pás	k1gInSc7
je	být	k5eAaImIp3nS
kupole	kupole	k1gFnSc1
s	s	k7c7
obrazem	obraz	k1gInSc7
žehnajícího	žehnající	k2eAgMnSc2d1
Krista	Kristus	k1gMnSc2
na	na	k7c6
trůnu	trůn	k1gInSc6
a	a	k8xC
symboly	symbol	k1gInPc7
čtyř	čtyři	k4xCgInPc2
evangelistů	evangelista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
stranách	strana	k1gFnPc6
jsou	být	k5eAaImIp3nP
Panna	Panna	k1gFnSc1
Marie	Marie	k1gFnSc1
a	a	k8xC
svatý	svatý	k2eAgMnSc1d1
Jan	Jan	k1gMnSc1
Křtitel	křtitel	k1gMnSc1
jako	jako	k8xC,k8xS
přímluvci	přímluvce	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
pásu	pás	k1gInSc2
maleb	malba	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
pás	pás	k1gInSc1
maleb	malba	k1gFnPc2
v	v	k7c6
lodi	loď	k1gFnSc6
</s>
<s>
Je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
malovanou	malovaný	k2eAgFnSc7d1
drapérií	drapérie	k1gFnSc7
jako	jako	k8xS,k8xC
v	v	k7c6
apsidě	apsida	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
zavěšena	zavěsit	k5eAaPmNgFnS
na	na	k7c6
kovových	kovový	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
a	a	k8xC
končí	končit	k5eAaImIp3nS
asi	asi	k9
60	#num#	k4
cm	cm	kA
nad	nad	k7c7
podlahou	podlaha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
pás	pás	k1gInSc1
maleb	malba	k1gFnPc2
v	v	k7c6
lodi	loď	k1gFnSc6
</s>
<s>
Pás	pás	k1gInSc1
mariánského	mariánský	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
–	–	k?
Zvěstování	zvěstování	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
Narození	narození	k1gNnSc4
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
Sen	sen	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Josefa	Josef	k1gMnSc2
<g/>
,	,	kIx,
Zvěstování	zvěstování	k1gNnSc1
pastýřům	pastýř	k1gMnPc3
<g/>
,	,	kIx,
Klanění	klanění	k1gNnSc1
Tří	tři	k4xCgMnPc2
králů	král	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
téměř	téměř	k6eAd1
nečitelné	čitelný	k2eNgFnSc2d1
scény	scéna	k1gFnSc2
Vraždění	vraždění	k1gNnSc2
neviňátek	neviňátko	k1gNnPc2
a	a	k8xC
Útěk	útěk	k1gInSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
pás	pás	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
scény	scéna	k1gFnSc2
z	z	k7c2
období	období	k1gNnSc2
příchodu	příchod	k1gInSc2
a	a	k8xC
raného	raný	k2eAgNnSc2d1
dětství	dětství	k1gNnSc2
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
existují	existovat	k5eAaImIp3nP
úvahy	úvaha	k1gFnPc4
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
nedokončenosti	nedokončenost	k1gFnSc6
z	z	k7c2
důvodu	důvod	k1gInSc2
následujícího	následující	k2eAgInSc2d1
cyklu	cyklus	k1gInSc2
přemyslovské	přemyslovský	k2eAgFnSc2d1
genealogie	genealogie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
pás	pás	k1gInSc1
maleb	malba	k1gFnPc2
v	v	k7c6
lodi	loď	k1gFnSc6
</s>
<s>
Pás	pás	k1gInSc1
s	s	k7c7
přemyslovskou	přemyslovský	k2eAgFnSc7d1
scénou	scéna	k1gFnSc7
zobrazující	zobrazující	k2eAgNnSc4d1
povolání	povolání	k1gNnSc4
Přemysla	Přemysl	k1gMnSc2
Oráče	oráč	k1gMnSc2
k	k	k7c3
vládě	vláda	k1gFnSc3
<g/>
,	,	kIx,
pak	pak	k6eAd1
osm	osm	k4xCc1
postav	postava	k1gFnPc2
s	s	k7c7
panovnickými	panovnický	k2eAgInPc7d1
symboly	symbol	k1gInPc7
a	a	k8xC
pláštěm	plášť	k1gInSc7
a	a	k8xC
nakonec	nakonec	k6eAd1
vedle	vedle	k7c2
vstupu	vstup	k1gInSc2
do	do	k7c2
apsidy	apsida	k1gFnSc2
postavy	postava	k1gFnSc2
dvou	dva	k4xCgMnPc2
donátorů	donátor	k1gMnPc2
(	(	kIx(
<g/>
Konráda	Konrád	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemského	znojemský	k2eAgNnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnPc4
ženy	žena	k1gFnPc4
Marie	Maria	k1gFnSc2
Srbské	srbský	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
mezi	mezi	k7c7
historiky	historik	k1gMnPc7
panuje	panovat	k5eAaImIp3nS
přesvědčení	přesvědčení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
plášť	plášť	k1gInSc1
symbolizuje	symbolizovat	k5eAaImIp3nS
vládu	vláda	k1gFnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
těchto	tento	k3xDgFnPc2
osm	osm	k4xCc1
postav	postava	k1gFnPc2
identifikováno	identifikovat	k5eAaBmNgNnS
buď	buď	k8xC
jako	jako	k9
mýtická	mýtický	k2eAgNnPc4d1
přemyslovská	přemyslovský	k2eAgNnPc4d1
knížata	kníže	k1gNnPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
jako	jako	k9
historičtí	historický	k2eAgMnPc1d1
Přemyslovci	Přemyslovec	k1gMnPc1
<g/>
,	,	kIx,
počínaje	počínaje	k7c7
<g/>
,	,	kIx,
dle	dle	k7c2
nejnovějšího	nový	k2eAgNnSc2d3
zjištění	zjištění	k1gNnSc2
svatým	svatý	k1gMnSc7
Václavem	Václav	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
pás	pás	k1gInSc1
maleb	malba	k1gFnPc2
v	v	k7c6
lodi	loď	k1gFnSc6
a	a	k8xC
kupole	kupole	k1gFnSc1
</s>
<s>
Pás	pás	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
dalších	další	k2eAgFnPc2d1
deset	deset	k4xCc1
panovnických	panovnický	k2eAgFnPc2d1
postav	postava	k1gFnPc2
v	v	k7c6
pláštích	plášť	k1gInPc6
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
považování	považování	k1gNnSc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
postavami	postava	k1gFnPc7
v	v	k7c6
předchozím	předchozí	k2eAgInSc6d1
pásu	pás	k1gInSc6
<g/>
,	,	kIx,
za	za	k7c4
české	český	k2eAgMnPc4d1
vládnoucí	vládnoucí	k2eAgMnPc4d1
Přemyslovce	Přemyslovec	k1gMnPc4
<g/>
)	)	kIx)
a	a	k8xC
devět	devět	k4xCc4
panovnických	panovnický	k2eAgFnPc2d1
postav	postava	k1gFnPc2
bez	bez	k7c2
pláště	plášť	k1gInSc2
(	(	kIx(
<g/>
někteří	některý	k3yIgMnPc1
z	z	k7c2
přemyslovských	přemyslovský	k2eAgMnPc2d1
údělníků	údělník	k1gMnPc2
<g/>
,	,	kIx,
vládnoucí	vládnoucí	k2eAgFnSc1d1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
zde	zde	k6eAd1
existuje	existovat	k5eAaImIp3nS
řada	řada	k1gFnSc1
výkladů	výklad	k1gInPc2
a	a	k8xC
zřejmě	zřejmě	k6eAd1
jediným	jediný	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
se	se	k3xPyFc4
všichni	všechen	k3xTgMnPc1
historikové	historik	k1gMnPc1
shodnou	shodnout	k5eAaPmIp3nP,k5eAaBmIp3nP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
první	první	k4xOgMnSc1
nositel	nositel	k1gMnSc1
královské	královský	k2eAgFnSc2d1
koruny	koruna	k1gFnSc2
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
král	král	k1gMnSc1
Vratislav	Vratislav	k1gMnSc1
I.	I.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kupoli	kupole	k1gFnSc6
je	být	k5eAaImIp3nS
holubice	holubice	k1gFnSc1
jako	jako	k8xC,k8xS
symbol	symbol	k1gInSc1
Ducha	duch	k1gMnSc2
svatého	svatý	k1gMnSc2
<g/>
,	,	kIx,
pod	pod	k7c7
ní	on	k3xPp3gFnSc7
čtyři	čtyři	k4xCgMnPc1
evangelisté	evangelista	k1gMnPc1
u	u	k7c2
psacích	psací	k2eAgInPc2d1
pultů	pult	k1gInPc2
střídavě	střídavě	k6eAd1
se	s	k7c7
čtyřmi	čtyři	k4xCgMnPc7
serafíny	serafín	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
nápisu	nápis	k1gInSc2
</s>
<s>
Jeden	jeden	k4xCgInSc1
z	z	k7c2
možných	možný	k2eAgInPc2d1
výkladů	výklad	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
In	In	k?
reg	reg	k?
<g/>
(	(	kIx(
<g/>
no	no	k9
<g/>
)	)	kIx)
d	d	k?
<g/>
(	(	kIx(
<g/>
omi	omi	k?
<g/>
)	)	kIx)
<g/>
ni	on	k3xPp3gFnSc4
n	n	k0
<g/>
(	(	kIx(
<g/>
ost	ost	k?
<g/>
)	)	kIx)
<g/>
ri	ri	k?
jh	jh	k?
<g/>
(	(	kIx(
<g/>
es	es	k1gNnPc2
<g/>
)	)	kIx)
<g/>
u	u	k7c2
X	X	kA
<g/>
(	(	kIx(
<g/>
risti	rist	k5eAaBmF,k5eAaImF,k5eAaPmF
<g/>
)	)	kIx)
<g/>
/	/	kIx~
Anno	Anna	k1gFnSc5
do	do	k7c2
<g/>
(	(	kIx(
<g/>
min	mina	k1gFnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
ice	ice	k?
ab	ab	k?
incarnati	incarnat	k5eAaPmF,k5eAaImF
<g/>
(	(	kIx(
<g/>
onis	onis	k6eAd1
<g/>
)	)	kIx)
ei	ei	k?
<g/>
(	(	kIx(
<g/>
us	us	k?
<g/>
)	)	kIx)
<g/>
d	d	k?
<g/>
(	(	kIx(
<g/>
em	em	k?
<g/>
)	)	kIx)
Mill	Mill	k1gMnSc1
<g/>
(	(	kIx(
<g/>
esim	esim	k1gMnSc1
<g/>
)	)	kIx)
<g/>
o	o	k7c4
<g/>
/	/	kIx~
<g/>
CXXXIIII	CXXXIIII	kA
p	p	k?
<g/>
(	(	kIx(
<g/>
er	er	k?
<g/>
)	)	kIx)
<g/>
act	act	k?
<g/>
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
is	is	k?
<g/>
)	)	kIx)
pict	pict	k1gMnSc1
<g/>
(	(	kIx(
<g/>
uris	uris	k1gInSc1
<g/>
)	)	kIx)
reg	reg	k?
<g/>
(	(	kIx(
<g/>
um	um	k1gInSc4
<g/>
)	)	kIx)
aux	aux	k?
<g/>
(	(	kIx(
<g/>
it	it	k?
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
cella	cello	k1gNnSc2
<g/>
(	(	kIx(
<g/>
m	m	kA
<g/>
)	)	kIx)
b	b	k?
<g/>
(	(	kIx(
<g/>
e	e	k0
<g/>
)	)	kIx)
<g/>
a	a	k8xC
<g/>
(	(	kIx(
<g/>
t	t	k?
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
e	e	k0
v	v	k7c6
<g/>
(	(	kIx(
<g/>
irginis	irginis	k1gFnSc6
<g/>
)	)	kIx)
Ma	Ma	k1gMnSc1
<g/>
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
<g/>
i	i	k9
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
e	e	k0
et	et	k?
S.	S.	kA
Cathar	Cathar	k1gInSc1
<g/>
(	(	kIx(
<g/>
i	i	k9
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
[	[	kIx(
<g/>
n	n	k0
<g/>
]	]	kIx)
<g/>
e	e	k0
dux	dux	k?
Conrad	Conrad	k1gInSc1
<g/>
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
us	us	k?
<g/>
)	)	kIx)
s	s	k7c7
<g/>
(	(	kIx(
<g/>
e	e	k0
<g/>
)	)	kIx)
<g/>
c	c	k0
<g/>
(	(	kIx(
<g/>
un	un	k?
<g/>
)	)	kIx)
<g/>
d	d	k?
<g/>
(	(	kIx(
<g/>
us	us	k?
<g/>
)	)	kIx)
f	f	k?
<g/>
[	[	kIx(
<g/>
un	un	k?
<g/>
]	]	kIx)
<g/>
dator	dator	k1gMnSc1
<g/>
/	/	kIx~
<g/>
[	[	kIx(
<g/>
et	et	k?
nov	nov	k1gInSc1
<g/>
]	]	kIx)
<g/>
am	am	k?
[	[	kIx(
<g/>
f	f	k?
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
ec	ec	k?
<g/>
)	)	kIx)
<g/>
it	it	k?
cons	cons	k1gInSc1
<g/>
(	(	kIx(
<g/>
truc	truc	k1gInSc1
<g/>
)	)	kIx)
<g/>
tio	tio	k?
<g/>
(	(	kIx(
<g/>
nem	nem	k?
<g/>
)	)	kIx)
eius	eius	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
království	království	k1gNnSc6
Pána	pán	k1gMnSc4
našeho	náš	k3xOp1gMnSc4
Ježíše	Ježíš	k1gMnSc4
Krista	Kristus	k1gMnSc4
<g/>
/	/	kIx~
Roku	rok	k1gInSc2
Páně	páně	k2eAgFnSc1d1
od	od	k7c2
narození	narození	k1gNnSc2
téhož	týž	k3xTgInSc2
tisícího	tisící	k4xOgInSc2
<g/>
/	/	kIx~
stého	stý	k4xOgInSc2
třicátého	třicátý	k4xOgNnSc2
čtvrtého	čtvrtý	k4xOgNnSc2
provedenými	provedený	k2eAgFnPc7d1
malbami	malba	k1gFnPc7
králů	král	k1gMnPc2
zvelebil	zvelebit	k5eAaPmAgInS
<g/>
/	/	kIx~
svatyni	svatyně	k1gFnSc4
blahoslavené	blahoslavený	k2eAgFnSc2d1
panny	panna	k1gFnSc2
Marie	Maria	k1gFnSc2
a	a	k8xC
sv.	sv.	kA
Kateři	Kater	k1gMnPc1
<g/>
/	/	kIx~
<g/>
ny	ny	k?
kníže	kníže	k1gNnSc1wR
Konrád	Konrád	k1gMnSc1
druhý	druhý	k4xOgMnSc1
zakladatel	zakladatel	k1gMnSc1
<g/>
/	/	kIx~
a	a	k8xC
udělal	udělat	k5eAaPmAgMnS
její	její	k3xOp3gFnSc4
novou	nový	k2eAgFnSc4d1
(	(	kIx(
<g/>
pře	pře	k1gFnSc2
<g/>
)	)	kIx)
<g/>
stavbu	stavba	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Alternativní	alternativní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
rozvířil	rozvířit	k5eAaPmAgMnS
veřejné	veřejný	k2eAgNnSc4d1
mínění	mínění	k1gNnSc4
brněnský	brněnský	k2eAgMnSc1d1
historik	historik	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Zástěra	Zástěra	k1gMnSc1
řadou	řada	k1gFnSc7
odvážných	odvážný	k2eAgInPc2d1
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
ne	ne	k9
dostatečně	dostatečně	k6eAd1
doložených	doložený	k2eAgFnPc2d1
hypotéz	hypotéza	k1gFnPc2
<g/>
,	,	kIx,
vztahujích	vztahuj	k1gInPc6
se	se	k3xPyFc4
především	především	k9
k	k	k7c3
předvelkomoravskému	předvelkomoravský	k2eAgNnSc3d1
a	a	k8xC
velkomoravskému	velkomoravský	k2eAgNnSc3d1
období	období	k1gNnSc3
<g/>
,	,	kIx,
historii	historie	k1gFnSc3
Znojma	Znojmo	k1gNnSc2
a	a	k8xC
rotundy	rotunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
nich	on	k3xPp3gMnPc2
měl	mít	k5eAaImAgInS
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
sídlit	sídlit	k5eAaImF
velkomoravský	velkomoravský	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Rostislav	Rostislav	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
rotunda	rotunda	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
postavena	postaven	k2eAgFnSc1d1
i	i	k9
částečně	částečně	k6eAd1
vymalována	vymalován	k2eAgFnSc1d1
po	po	k7c6
příchodu	příchod	k1gInSc6
cyrilometodějské	cyrilometodějský	k2eAgFnSc2d1
mise	mise	k1gFnSc2
řemeslníky	řemeslník	k1gMnPc7
a	a	k8xC
umělci	umělec	k1gMnPc7
z	z	k7c2
doprovodu	doprovod	k1gInSc2
soluňských	soluňský	k2eAgMnPc2d1
bratří	bratr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástěra	Zástěra	k1gMnSc1
přichází	přicházet	k5eAaImIp3nS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
plášť	plášť	k1gInSc1
je	být	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
panovnické	panovnický	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
sahající	sahající	k2eAgFnSc2d1
na	na	k7c4
Moravu	Morava	k1gFnSc4
a	a	k8xC
postavy	postava	k1gFnPc4
ve	v	k7c6
třetím	třetí	k4xOgInSc6
pásu	pás	k1gInSc6
ztotožňuje	ztotožňovat	k5eAaImIp3nS
se	s	k7c7
Sámem	Sámo	k1gMnSc7
<g/>
,	,	kIx,
přes	přes	k7c4
jeho	jeho	k3xOp3gMnPc4
následovníky	následovník	k1gMnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc2
jména	jméno	k1gNnSc2
historie	historie	k1gFnSc2
nezná	neznat	k5eAaImIp3nS,k5eNaImIp3nS
<g/>
,	,	kIx,
až	až	k8xS
po	po	k7c4
Mojmíra	Mojmír	k1gMnSc4
a	a	k8xC
Rostislava	Rostislav	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oráčské	oráčský	k2eAgFnSc6d1
scéně	scéna	k1gFnSc6
vidí	vidět	k5eAaImIp3nS
Sáma	Sámo	k1gMnSc4
<g/>
,	,	kIx,
vyorávající	vyorávající	k2eAgFnSc4d1
slavnostně	slavnostně	k6eAd1
brázdu	brázda	k1gFnSc4
pro	pro	k7c4
založení	založení	k1gNnSc4
Znojma	Znojmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
stejného	stejný	k2eAgInSc2d1
klíče	klíč	k1gInSc2
identifikuje	identifikovat	k5eAaBmIp3nS
i	i	k9
postavy	postava	k1gFnPc4
ve	v	k7c4
4	#num#	k4
<g/>
.	.	kIx.
pásu	pás	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
navíc	navíc	k6eAd1
přichází	přicházet	k5eAaImIp3nS
s	s	k7c7
hypotézou	hypotéza	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
postavy	postava	k1gFnPc1
s	s	k7c7
„	„	k?
<g/>
obráceným	obrácený	k2eAgInSc7d1
<g/>
“	“	k?
štítem	štít	k1gInSc7
jsou	být	k5eAaImIp3nP
v	v	k7c6
malbách	malba	k1gFnPc6
zachyceny	zachytit	k5eAaPmNgFnP
dvakrát	dvakrát	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
kníže	kníže	k1gMnSc1
<g/>
/	/	kIx~
<g/>
světec	světec	k1gMnSc1
Václav	Václav	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
považuje	považovat	k5eAaImIp3nS
Bořivoje	Bořivoj	k1gMnSc4
za	za	k7c4
velkomoravského	velkomoravský	k2eAgMnSc4d1
místodržitele	místodržitel	k1gMnSc4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
něj	on	k3xPp3gNnSc2
dějiny	dějiny	k1gFnPc4
Znojma	Znojmo	k1gNnSc2
nejstaršími	starý	k2eAgFnPc7d3
dějinami	dějiny	k1gFnPc7
naší	náš	k3xOp1gFnSc2
státnosti	státnost	k1gFnSc2
<g/>
,	,	kIx,
čehož	což	k3yQnSc2,k3yRnSc2
si	se	k3xPyFc3
Přemyslovci	Přemyslovec	k1gMnPc1
byli	být	k5eAaImAgMnP
vědomi	vědom	k2eAgMnPc1d1
a	a	k8xC
Znojmo	Znojmo	k1gNnSc1
s	s	k7c7
rotundou	rotunda	k1gFnSc7
mělo	mít	k5eAaImAgNnS
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
ideologii	ideologie	k1gFnSc6
mimořádné	mimořádný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
slavné	slavný	k2eAgFnPc1d1
i	i	k8xC
zatajované	zatajovaný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgFnSc2
hypotézy	hypotéza	k1gFnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
snaží	snažit	k5eAaImIp3nS
podložit	podložit	k5eAaPmF
velkým	velký	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
rozličných	rozličný	k2eAgFnPc2d1
indicií	indicie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zástěrovy	Zástěrův	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
byly	být	k5eAaImAgFnP
odbornou	odborný	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
přijaty	přijmout	k5eAaPmNgInP
spíše	spíše	k9
rezervovaně	rezervovaně	k6eAd1
až	až	k9
zamítavě	zamítavě	k6eAd1
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
však	však	k9
vyvolaly	vyvolat	k5eAaPmAgFnP
řadu	řada	k1gFnSc4
emotivních	emotivní	k2eAgFnPc2d1
diskusí	diskuse	k1gFnPc2
a	a	k8xC
polemik	polemika	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
díky	díky	k7c3
nim	on	k3xPp3gMnPc3
stala	stát	k5eAaPmAgFnS
rotunda	rotunda	k1gFnSc1
mnohem	mnohem	k6eAd1
známější	známý	k2eAgFnSc1d2
a	a	k8xC
nakonec	nakonec	k6eAd1
i	i	k9
běžně	běžně	k6eAd1
přístupná	přístupný	k2eAgFnSc1d1
po	po	k7c6
nově	nova	k1gFnSc6
vystavěné	vystavěný	k2eAgFnSc3d1
lávce	lávka	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Klub	klub	k1gInSc1
přátel	přítel	k1gMnPc2
znojemské	znojemský	k2eAgFnSc2d1
rotundy	rotunda	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
i	i	k9
po	po	k7c6
Zástěrově	Zástěrův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
pokračuje	pokračovat	k5eAaImIp3nS
v	v	k7c6
rozvíjení	rozvíjení	k1gNnSc6
nebo	nebo	k8xC
modifikování	modifikování	k1gNnSc6
hypotéz	hypotéza	k1gFnPc2
v	v	k7c6
návaznosti	návaznost	k1gFnSc6
na	na	k7c4
nové	nový	k2eAgInPc4d1
objevy	objev	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
podobnou	podobný	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
výkladu	výklad	k1gInSc2
maleb	malba	k1gFnPc2
přišel	přijít	k5eAaPmAgMnS
český	český	k2eAgMnSc1d1
publicista	publicista	k1gMnSc1
a	a	k8xC
záhadolog	záhadolog	k1gMnSc1
Václav	Václav	k1gMnSc1
Tatíček	tatíček	k1gMnSc1
v	v	k7c6
knize	kniha	k1gFnSc6
Nejstarší	starý	k2eAgFnPc4d3
dějiny	dějiny	k1gFnPc4
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
Apropos	Apropos	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
Tatíček	tatíček	k1gMnSc1
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
malby	malba	k1gFnPc1
představují	představovat	k5eAaImIp3nP
obrazovou	obrazový	k2eAgFnSc4d1
kroniku	kronika	k1gFnSc4
vytvořenou	vytvořený	k2eAgFnSc4d1
na	na	k7c4
popud	popud	k1gInSc4
opata	opat	k1gMnSc2
sázavského	sázavský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
Božetěcha	Božetěch	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
události	událost	k1gFnPc4
a	a	k8xC
datace	datace	k1gFnPc4
jsou	být	k5eAaImIp3nP
zakódovány	zakódovat	k5eAaPmNgFnP
do	do	k7c2
gest	gesto	k1gNnPc2
a	a	k8xC
jiných	jiný	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
maleb	malba	k1gFnPc2
a	a	k8xC
Tatíček	tatíček	k1gMnSc1
také	také	k9
nabízí	nabízet	k5eAaImIp3nS
systém	systém	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
jednotlivé	jednotlivý	k2eAgFnPc1d1
malby	malba	k1gFnPc1
interpretovat	interpretovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Části	část	k1gFnSc6
tezí	teze	k1gFnPc2
publikoval	publikovat	k5eAaBmAgMnS
již	již	k6eAd1
roku	rok	k1gInSc2
1999	#num#	k4
v	v	k7c6
knize	kniha	k1gFnSc6
Přemyslovské	přemyslovský	k2eAgNnSc4d1
lustrování	lustrování	k1gNnSc4
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
starších	starý	k2eAgFnPc2d2
prací	práce	k1gFnPc2
Jaroslava	Jaroslav	k1gMnSc2
Zástěry	Zástěra	k1gMnSc2
a	a	k8xC
Lubomíra	Lubomír	k1gMnSc2
Konečného	Konečný	k1gMnSc2
a	a	k8xC
stejně	stejně	k6eAd1
jako	jako	k9
jejich	jejich	k3xOp3gFnPc4
práce	práce	k1gFnPc4
nelze	lze	k6eNd1
ani	ani	k8xC
tuto	tento	k3xDgFnSc4
hypotézu	hypotéza	k1gFnSc4
historickými	historický	k2eAgFnPc7d1
vědami	věda	k1gFnPc7
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jaroslav	Jaroslav	k1gMnSc1
Zástěra	Zástěra	k1gMnSc1
<g/>
:	:	kIx,
Znojemská	znojemský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
a	a	k8xC
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
1990	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Znojemská	znojemský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
:	:	kIx,
malby	malba	k1gFnPc1
v	v	k7c6
národní	národní	k2eAgFnSc6d1
kulturní	kulturní	k2eAgFnSc6d1
památce	památka	k1gFnSc6
Rotunda	rotunda	k1gFnSc1
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnSc2
a	a	k8xC
výsledky	výsledek	k1gInPc4
současného	současný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
:	:	kIx,
sborník	sborník	k1gInSc1
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
konference	konference	k1gFnSc2
o	o	k7c6
rotundě	rotunda	k1gFnSc6
<g/>
,	,	kIx,
konané	konaný	k2eAgFnSc6d1
25	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
26	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojmo	Znojmo	k1gNnSc1
<g/>
:	:	kIx,
Město	město	k1gNnSc1
Znojmo	Znojmo	k1gNnSc1
;	;	kIx,
Městská	městský	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85064	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CIPRIAN	CIPRIAN	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemská	znojemský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
ve	v	k7c6
světle	světlo	k1gNnSc6
vědeckého	vědecký	k2eAgNnSc2d1
poznání	poznání	k1gNnSc2
:	:	kIx,
vědecká	vědecký	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
Znojmo	Znojmo	k1gNnSc1
23	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
25.9	25.9	k4
<g/>
.1996	.1996	k4
:	:	kIx,
sborník	sborník	k1gInSc4
příspěvků	příspěvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojmo	Znojmo	k1gNnSc1
<g/>
:	:	kIx,
Jihomoravské	jihomoravský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
176	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902383	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DVOŘÁKOVÁ	Dvořáková	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rotunda	rotunda	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojmo	Znojmo	k1gNnSc1
<g/>
:	:	kIx,
Jihomoravské	jihomoravský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
40	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86974	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DVOŘÁKOVÁ	Dvořáková	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikace	identifikace	k1gFnSc1
pigmentů	pigment	k1gInPc2
v	v	k7c6
rotundě	rotunda	k1gFnSc6
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnSc2
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
službách	služba	k1gFnPc6
archeologie	archeologie	k1gFnSc2
:	:	kIx,
časopis	časopis	k1gInSc1
věnovaný	věnovaný	k2eAgInSc1d1
75	#num#	k4
<g/>
.	.	kIx.
narozeninám	narozeniny	k1gFnPc3
PhDr.	PhDr.	kA
Zlatice	zlatice	k1gFnSc1
Čilinskej	Čilinskej	k1gFnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
=	=	kIx~
In	In	k1gFnSc1
service	service	k1gFnSc2
to	ten	k3xDgNnSc4
archaeology	archaeolog	k1gMnPc7
:	:	kIx,
this	this	k6eAd1
issue	issuat	k5eAaPmIp3nS
is	is	k?
dedicated	dedicated	k1gInSc1
to	ten	k3xDgNnSc1
PhDr.	PhDr.	kA
Zlatica	Zlatica	k1gFnSc1
Čilinská	Čilinský	k2eAgFnSc1d1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
..	..	k?
2008	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
81	#num#	k4
<g/>
–	–	k?
<g/>
84	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
5463	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DVOŘÁKOVÁ	Dvořáková	k1gFnSc1
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
;	;	kIx,
HOTÁREK	Hotárek	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orientace	orientace	k1gFnSc1
rotundy	rotunda	k1gFnSc2
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnSc2
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
podle	podle	k7c2
skutečného	skutečný	k2eAgInSc2d1
východu	východ	k1gInSc2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
službách	služba	k1gFnPc6
archeologie	archeologie	k1gFnSc2
:	:	kIx,
časopis	časopis	k1gInSc1
věnovaný	věnovaný	k2eAgInSc1d1
75	#num#	k4
<g/>
.	.	kIx.
narozeninám	narozeniny	k1gFnPc3
PhDr.	PhDr.	kA
Zlatice	zlatice	k1gFnSc1
Čilinskej	Čilinskej	k1gFnSc1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
.	.	kIx.
=	=	kIx~
In	In	k1gFnSc1
service	service	k1gFnSc2
to	ten	k3xDgNnSc4
archaeology	archaeolog	k1gMnPc7
:	:	kIx,
this	this	k6eAd1
issue	issuat	k5eAaPmIp3nS
is	is	k?
dedicated	dedicated	k1gInSc1
to	ten	k3xDgNnSc1
PhDr.	PhDr.	kA
Zlatica	Zlatica	k1gFnSc1
Čilinská	Čilinský	k2eAgFnSc1d1
<g/>
,	,	kIx,
DrSc	DrSc	kA
<g/>
..	..	k?
2008	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
48	#num#	k4
<g/>
–	–	k?
<g/>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
5463	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Friedl	Friednout	k5eAaPmAgMnS
<g/>
,	,	kIx,
A	a	k9
<g/>
.1966	.1966	k4
Přemyslovci	Přemyslovec	k1gMnPc7
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Hašek	Hašek	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
/	/	kIx~
Konečný	Konečný	k1gMnSc1
<g/>
,	,	kIx,
L.	L.	kA
<g/>
/	/	kIx~
Tomešek	Tomešek	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
/	/	kIx~
<g/>
Unger	Unger	k1gMnSc1
<g/>
,	,	kIx,
J	J	kA
<g/>
.2009	.2009	k4
<g/>
,	,	kIx,
Nedestruktivní	destruktivní	k2eNgFnSc2d1
metody	metoda	k1gFnSc2
průzkumu	průzkum	k1gInSc2
románských	románský	k2eAgFnPc2d1
sakrálních	sakrální	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
</s>
<s>
JAN	Jan	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
poznámek	poznámka	k1gFnPc2
k	k	k7c3
nejstarší	starý	k2eAgFnSc3d3
církevní	církevní	k2eAgFnSc3d1
organizaci	organizace	k1gFnSc3
na	na	k7c6
Znojemsku	Znojemsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Matice	matice	k1gFnSc2
moravské	moravský	k2eAgFnSc2d1
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
116	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
39	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
323	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
X.	X.	kA
</s>
<s>
JAN	Jan	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
poznámek	poznámka	k1gFnPc2
k	k	k7c3
nejstarší	starý	k2eAgFnSc3d3
církevní	církevní	k2eAgFnSc3d1
organizaci	organizace	k1gFnSc3
na	na	k7c6
Znojemsku	Znojemsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Matice	matice	k1gFnSc2
moravské	moravský	k2eAgFnSc2d1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
118	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
445	#num#	k4
<g/>
–	–	k?
<g/>
452	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
323	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
X.	X.	kA
</s>
<s>
KLÍMA	Klíma	k1gMnSc1
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znojemská	znojemský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
ve	v	k7c6
světle	světlo	k1gNnSc6
archeologických	archeologický	k2eAgInPc2d1
výzkumů	výzkum	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
234	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
1243	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KONEČNÝ	Konečný	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Románská	románský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
:	:	kIx,
ikonologie	ikonologie	k1gFnSc1
maleb	malba	k1gFnPc2
a	a	k8xC
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
HOST	host	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
449	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7294	#num#	k4
<g/>
-	-	kIx~
<g/>
171	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Konečný	Konečný	k1gMnSc1
<g/>
,	,	kIx,
L	L	kA
<g/>
.1984	.1984	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
kaple	kaple	k1gFnSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
P.	P.	kA
Marie	Maria	k1gFnSc2
a	a	k8xC
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Výzkum	výzkum	k1gInSc1
románské	románský	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
SPFFBU	SPFFBU	kA
<g/>
,	,	kIx,
F	F	kA
18	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
;	;	kIx,
29	#num#	k4
–	–	k?
41	#num#	k4
</s>
<s>
KRZEMIEŃSKA	KRZEMIEŃSKA	kA
<g/>
,	,	kIx,
Barbara	Barbara	k1gFnSc1
<g/>
;	;	kIx,
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
;	;	kIx,
MERHAUTOVÁ	MERHAUTOVÁ	kA
<g/>
,	,	kIx,
Anežka	Anežka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravští	moravský	k2eAgMnPc1d1
Přemyslovci	Přemyslovec	k1gMnPc1
ve	v	k7c6
znojemské	znojemský	k2eAgFnSc6d1
rotundě	rotunda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Set	set	k1gInSc1
out	out	k?
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
135	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86277	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Krzemienska	Krzemiensko	k1gNnPc1
<g/>
,	,	kIx,
B	B	kA
<g/>
,	,	kIx,
Moravští	moravský	k2eAgMnPc1d1
Přemyslovci	Přemyslovec	k1gMnPc1
ve	v	k7c6
znojemské	znojemský	k2eAgFnSc6d1
rotundě	rotunda	k1gFnSc6
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
1985	#num#	k4
</s>
<s>
Kučková	Kučková	k1gFnSc1
<g/>
,	,	kIx,
Š.	Š.	kA
<g/>
,	,	kIx,
Hynek	Hynek	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
<g/>
,	,	kIx,
Kodíček	Kodíček	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
2007	#num#	k4
Identifikace	identifikace	k1gFnSc2
proteinových	proteinový	k2eAgFnPc2d1
komponent	komponenta	k1gFnPc2
v	v	k7c6
omítkách	omítka	k1gFnPc6
rotundy	rotunda	k1gFnSc2
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnSc2
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
,	,	kIx,
VSA	VSA	kA
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
;	;	kIx,
173	#num#	k4
–	–	k?
176	#num#	k4
</s>
<s>
Mencl	Mencl	k1gMnSc1
<g/>
,	,	kIx,
V	V	kA
<g/>
.1959	.1959	k4
<g/>
,	,	kIx,
Architektura	architektura	k1gFnSc1
předrománských	předrománský	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Umění	umění	k1gNnSc1
7	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
;	;	kIx,
349	#num#	k4
</s>
<s>
MERHAUTOVÁ	MERHAUTOVÁ	kA
<g/>
,	,	kIx,
Anežka	Anežka	k1gFnSc1
<g/>
;	;	kIx,
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Románské	románský	k2eAgInPc1d1
umění	umění	k1gNnSc4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
1984	#num#	k4
<g/>
.	.	kIx.
362	#num#	k4
s.	s.	k?
</s>
<s>
MĚŘÍNSKÝ	MĚŘÍNSKÝ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
poznámek	poznámka	k1gFnPc2
k	k	k7c3
výzkumům	výzkum	k1gInPc3
znojemské	znojemský	k2eAgFnSc2d1
rotundy	rotunda	k1gFnSc2
a	a	k8xC
hradu	hrad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Matice	matice	k1gFnSc2
moravské	moravský	k2eAgFnSc2d1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
118	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
453	#num#	k4
<g/>
–	–	k?
<g/>
463	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
323	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
X.	X.	kA
</s>
<s>
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Conradus	Conradus	k1gMnSc1
secundus	secundus	k1gMnSc1
fundator	fundator	k1gMnSc1
aneb	aneb	k?
úvahy	úvaha	k1gFnSc2
nad	nad	k7c7
významem	význam	k1gInSc7
jedné	jeden	k4xCgFnSc2
čárky	čárka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Matice	matice	k1gFnSc2
moravské	moravský	k2eAgFnSc2d1
<g/>
.	.	kIx.
1999	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
118	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
437	#num#	k4
<g/>
–	–	k?
<g/>
443	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
323	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
52	#num#	k4
<g/>
X.	X.	kA
</s>
<s>
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgFnSc1d1
906	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
464	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
563	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Testament	testament	k1gInSc1
knížete	kníže	k1gMnSc2
Břetislava	Břetislav	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
FIFKOVÁ	FIFKOVÁ	kA
<g/>
,	,	kIx,
R.	R.	kA
Sága	sága	k1gFnSc1
moravských	moravský	k2eAgMnPc2d1
Přemyslovců	Přemyslovec	k1gMnPc2
:	:	kIx,
život	život	k1gInSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
od	od	k7c2
XI	XI	kA
<g/>
.	.	kIx.
do	do	k7c2
počátku	počátek	k1gInSc2
XIV	XIV	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
:	:	kIx,
sborník	sborník	k1gInSc1
a	a	k8xC
katalog	katalog	k1gInSc1
výstavy	výstava	k1gFnSc2
pořádané	pořádaný	k2eAgFnSc2d1
Vlastivědným	vlastivědný	k2eAgNnSc7d1
muzeem	muzeum	k1gNnSc7
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
a	a	k8xC
Muzeem	muzeum	k1gNnSc7
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
k	k	k7c3
700	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
tragické	tragický	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
Václava	Václav	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
posledního	poslední	k2eAgMnSc2d1
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
;	;	kIx,
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
;	;	kIx,
Muzeum	muzeum	k1gNnSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85037	#num#	k4
<g/>
-	-	kIx~
<g/>
42	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čechy	Čech	k1gMnPc7
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgFnSc1d1
1034	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
712	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
905	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
románských	románský	k2eAgFnPc2d1
rotund	rotunda	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Románské	románský	k2eAgNnSc1d1
malířství	malířství	k1gNnSc1
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1
</s>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1
sakrální	sakrální	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rotunda	rotunda	k1gFnSc1
svaté	svatý	k2eAgFnSc2d1
Kateřiny	Kateřina	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Klub	klub	k1gInSc1
přátel	přítel	k1gMnPc2
znojemské	znojemský	k2eAgFnSc2d1
rotundy	rotunda	k1gFnSc2
</s>
<s>
Znojemská	znojemský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Jihomoravského	jihomoravský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
</s>
<s>
Znojemská	znojemský	k2eAgFnSc1d1
rotunda	rotunda	k1gFnSc1
na	na	k7c6
oficiálních	oficiální	k2eAgFnPc6d1
stránkách	stránka	k1gFnPc6
města	město	k1gNnSc2
Znojma	Znojmo	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
informací	informace	k1gFnPc2
o	o	k7c6
otevíracích	otevírací	k2eAgFnPc6d1
hodinách	hodina	k1gFnPc6
a	a	k8xC
vstupném	vstupné	k1gNnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010710601	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
292411083	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
