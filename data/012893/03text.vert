<p>
<s>
Bojiště	bojiště	k1gNnSc1	bojiště
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnPc4	vesnice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kolín	Kolín	k1gInSc1	Kolín
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
obce	obec	k1gFnSc2	obec
Červené	Červené	k2eAgFnSc2d1	Červené
Pečky	pečka	k1gFnSc2	pečka
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
1,1	[number]	k4	1,1
km	km	kA	km
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Červených	Červených	k2eAgFnPc2d1	Červených
Peček	Pečky	k1gFnPc2	Pečky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
evidováno	evidovat	k5eAaImNgNnS	evidovat
56	[number]	k4	56
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Bojiště	bojiště	k1gNnSc1	bojiště
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Červené	Červené	k2eAgFnSc2d1	Červené
Pečky	pečka	k1gFnSc2	pečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bojiště	bojiště	k1gNnSc2	bojiště
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
Červených	Červených	k2eAgFnPc2d1	Červených
Peček	Pečky	k1gFnPc2	Pečky
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
části	část	k1gFnSc2	část
Bojiště	bojiště	k1gNnSc2	bojiště
</s>
</p>
