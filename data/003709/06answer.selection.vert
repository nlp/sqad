<s>
Ždánický	ždánický	k2eAgInSc1d1	ždánický
les	les	k1gInSc1	les
je	být	k5eAaImIp3nS	být
plochá	plochý	k2eAgFnSc1d1	plochá
vrchovina	vrchovina	k1gFnSc1	vrchovina
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
470	[number]	k4	470
km2	km2	k4	km2
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
U	u	k7c2	u
Slepice	slepice	k1gFnSc2	slepice
438	[number]	k4	438
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc7d1	střední
výškou	výška	k1gFnSc7	výška
270,7	[number]	k4	270,7
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
sklonu	sklon	k1gInSc6	sklon
4	[number]	k4	4
<g/>
°	°	k?	°
54	[number]	k4	54
<g/>
́	́	k?	́
<g/>
.	.	kIx.	.
</s>
