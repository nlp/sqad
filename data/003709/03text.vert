<s>
Ždánický	ždánický	k2eAgInSc1d1	ždánický
les	les	k1gInSc1	les
je	být	k5eAaImIp3nS	být
plochá	plochý	k2eAgFnSc1d1	plochá
vrchovina	vrchovina	k1gFnSc1	vrchovina
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
470	[number]	k4	470
km2	km2	k4	km2
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
U	u	k7c2	u
Slepice	slepice	k1gFnSc2	slepice
438	[number]	k4	438
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc7d1	střední
výškou	výška	k1gFnSc7	výška
270,7	[number]	k4	270,7
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
sklonu	sklon	k1gInSc6	sklon
4	[number]	k4	4
<g/>
°	°	k?	°
54	[number]	k4	54
<g/>
́	́	k?	́
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
Břeclav	Břeclav	k1gFnSc1	Břeclav
<g/>
,	,	kIx,	,
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
a	a	k8xC	a
Vyškov	Vyškov	k1gInSc1	Vyškov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Litenčickou	Litenčický	k2eAgFnSc7d1	Litenčická
pahorkatinou	pahorkatina	k1gFnSc7	pahorkatina
<g/>
,	,	kIx,	,
Dyjsko-svrateckým	dyjskovratecký	k2eAgInSc7d1	dyjsko-svratecký
úvalem	úval	k1gInSc7	úval
<g/>
,	,	kIx,	,
Dolnomoravským	dolnomoravský	k2eAgInSc7d1	dolnomoravský
úvalem	úval	k1gInSc7	úval
a	a	k8xC	a
Kyjovskou	kyjovský	k2eAgFnSc7d1	Kyjovská
pahorkatinou	pahorkatina	k1gFnSc7	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Geomorfologický	geomorfologický	k2eAgInSc1d1	geomorfologický
celek	celek	k1gInSc1	celek
Ždánický	ždánický	k2eAgInSc1d1	ždánický
les	les	k1gInSc1	les
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
geomorfologické	geomorfologický	k2eAgFnSc2d1	geomorfologická
oblasti	oblast	k1gFnSc2	oblast
Středomoravské	středomoravský	k2eAgInPc4d1	středomoravský
Karpaty	Karpaty	k1gInPc4	Karpaty
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
geomorfologické	geomorfologický	k2eAgFnPc1d1	geomorfologická
subprovincie	subprovincie	k1gFnPc1	subprovincie
Vnější	vnější	k2eAgInPc4d1	vnější
Západní	západní	k2eAgInPc4d1	západní
Karpaty	Karpaty	k1gInPc4	Karpaty
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
částí	část	k1gFnSc7	část
geomorfologické	geomorfologický	k2eAgFnPc1d1	geomorfologická
provicie	provicie	k1gFnPc1	provicie
Západní	západní	k2eAgFnPc1d1	západní
Karpaty	Karpaty	k1gInPc4	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Ždánický	ždánický	k2eAgInSc1d1	ždánický
les	les	k1gInSc1	les
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
geomorfologické	geomorfologický	k2eAgInPc4d1	geomorfologický
podcelky	podcelek	k1gInPc4	podcelek
<g/>
:	:	kIx,	:
Hustopečská	hustopečský	k2eAgFnSc1d1	Hustopečská
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
Boleradická	Boleradický	k2eAgFnSc1d1	Boleradická
vrchovina	vrchovina	k1gFnSc1	vrchovina
Dambořická	Dambořický	k2eAgFnSc1d1	Dambořický
vrchovina	vrchovina	k1gFnSc1	vrchovina
Podloží	podloží	k1gNnSc2	podloží
Ždánického	ždánický	k2eAgInSc2d1	ždánický
lesa	les	k1gInSc2	les
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
především	především	k9	především
paleogenní	paleogenní	k2eAgInPc1d1	paleogenní
sedimenty	sediment	k1gInPc1	sediment
ždánické	ždánický	k2eAgFnSc2d1	Ždánická
a	a	k8xC	a
pouzdřanské	pouzdřanský	k2eAgFnSc2d1	pouzdřanský
jednotky	jednotka	k1gFnSc2	jednotka
vnější	vnější	k2eAgFnSc2d1	vnější
skupiny	skupina	k1gFnSc2	skupina
příkrovů	příkrov	k1gInPc2	příkrov
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
jsou	být	k5eAaImIp3nP	být
mohutné	mohutný	k2eAgFnPc1d1	mohutná
překryvy	překryva	k1gFnPc1	překryva
kvartérních	kvartérní	k2eAgFnPc2d1	kvartérní
spraší	spraš	k1gFnPc2	spraš
a	a	k8xC	a
sprašových	sprašový	k2eAgFnPc2d1	sprašová
hlín	hlína	k1gFnPc2	hlína
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Ždánického	ždánický	k2eAgInSc2d1	ždánický
lesa	les	k1gInSc2	les
pochází	pocházet	k5eAaImIp3nS	pocházet
lehká	lehký	k2eAgFnSc1d1	lehká
<g/>
,	,	kIx,	,
parafinická	parafinický	k2eAgFnSc1d1	parafinický
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
ložisko	ložisko	k1gNnSc1	ložisko
ropy	ropa	k1gFnSc2	ropa
u	u	k7c2	u
Ždánic	Ždánice	k1gFnPc2	Ždánice
(	(	kIx(	(
<g/>
maximum	maximum	k1gNnSc1	maximum
těžby	těžba	k1gFnSc2	těžba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
5641.0000005	[number]	k4	5641.0000005
641	[number]	k4	641
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
u	u	k7c2	u
Dambořic	Dambořice	k1gFnPc2	Dambořice
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
ložisk	ložisko	k1gNnPc2	ložisko
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
denní	denní	k2eAgFnSc1d1	denní
těžba	těžba	k1gFnSc1	těžba
představuje	představovat	k5eAaImIp3nS	představovat
55	[number]	k4	55
%	%	kIx~	%
produkce	produkce	k1gFnSc2	produkce
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v	v	k7c6	v
širší	široký	k2eAgFnSc6d2	širší
oblasti	oblast	k1gFnSc6	oblast
Dambořice	Dambořice	k1gFnSc2	Dambořice
–	–	k?	–
Uhřice	Uhřice	k1gFnSc2	Uhřice
–	–	k?	–
Žarošice	Žarošice	k1gFnSc2	Žarošice
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
88	[number]	k4	88
%	%	kIx~	%
produkce	produkce	k1gFnSc2	produkce
ropy	ropa	k1gFnSc2	ropa
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
činila	činit	k5eAaImAgFnS	činit
&	&	k?	&
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
246200.000000246	[number]	k4	246200.000000246
200	[number]	k4	200
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ždánický	ždánický	k2eAgInSc1d1	ždánický
les	les	k1gInSc1	les
je	být	k5eAaImIp3nS	být
odvodňován	odvodňovat	k5eAaImNgInS	odvodňovat
Litavou	Litava	k1gFnSc7	Litava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Trkmankou	Trkmanka	k1gFnSc7	Trkmanka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
.	.	kIx.	.
</s>
<s>
Ždánický	ždánický	k2eAgInSc1d1	ždánický
les	les	k1gInSc1	les
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
teplým	teplý	k2eAgNnSc7d1	teplé
<g/>
,	,	kIx,	,
suchým	suchý	k2eAgNnSc7d1	suché
klimatem	klima	k1gNnSc7	klima
<g/>
.	.	kIx.	.
</s>
<s>
Pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
vegetačním	vegetační	k2eAgInSc6d1	vegetační
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnPc1d2	nižší
polohy	poloha	k1gFnPc1	poloha
oblasti	oblast	k1gFnSc2	oblast
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
velmi	velmi	k6eAd1	velmi
úrodné	úrodný	k2eAgFnPc1d1	úrodná
půdy	půda	k1gFnPc1	půda
využíváy	využívá	k2eAgFnPc1d1	využívá
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
,	,	kIx,	,
daří	dařit	k5eAaImIp3nS	dařit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vinohradnictví	vinohradnictví	k1gNnSc1	vinohradnictví
a	a	k8xC	a
sadařství	sadařství	k1gNnSc1	sadařství
teplomilných	teplomilný	k2eAgInPc2d1	teplomilný
druhů	druh	k1gInPc2	druh
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
meruňky	meruňka	k1gFnPc1	meruňka
a	a	k8xC	a
broskvoně	broskvoň	k1gFnPc1	broskvoň
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
zde	zde	k6eAd1	zde
nalézt	nalézt	k5eAaBmF	nalézt
oskeruše	oskeruše	k1gFnPc4	oskeruše
či	či	k8xC	či
mandloně	mandloň	k1gFnPc4	mandloň
<g/>
.	.	kIx.	.
</s>
<s>
Souvislejší	souvislý	k2eAgInPc1d2	souvislejší
lesní	lesní	k2eAgInPc1d1	lesní
porosty	porost	k1gInPc1	porost
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
v	v	k7c6	v
Dambořické	Dambořický	k2eAgFnSc6d1	Dambořický
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
jak	jak	k6eAd1	jak
teplomilná	teplomilný	k2eAgNnPc4d1	teplomilné
spečenstva	spečenstvo	k1gNnPc4	spečenstvo
panonských	panonský	k2eAgFnPc2d1	Panonská
a	a	k8xC	a
karpatských	karpatský	k2eAgFnPc2d1	Karpatská
dubohabřin	dubohabřina	k1gFnPc2	dubohabřina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
společenstva	společenstvo	k1gNnPc4	společenstvo
bučin	bučina	k1gFnPc2	bučina
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
lesostepní	lesostepní	k2eAgNnPc1d1	lesostepní
a	a	k8xC	a
stepní	stepní	k2eAgNnPc1d1	stepní
společenstva	společenstvo	k1gNnPc1	společenstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Ždánickém	ždánický	k2eAgInSc6d1	ždánický
lesebylo	lesebýt	k5eAaPmAgNnS	lesebýt
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
celkem	celkem	k6eAd1	celkem
22	[number]	k4	22
maloplošných	maloplošný	k2eAgNnPc2d1	maloplošné
zvláště	zvláště	k6eAd1	zvláště
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
1	[number]	k4	1
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
(	(	kIx(	(
<g/>
NPP	NPP	kA	NPP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
9	[number]	k4	9
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
(	(	kIx(	(
<g/>
PR	pr	k0	pr
<g/>
)	)	kIx)	)
a	a	k8xC	a
12	[number]	k4	12
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
(	(	kIx(	(
<g/>
PP	PP	kA	PP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
Ždánický	ždánický	k2eAgInSc1d1	ždánický
les	les	k1gInSc1	les
NPP	NPP	kA	NPP
Kukle	kukle	k1gFnSc2	kukle
PR	pr	k0	pr
Hrádek	hrádek	k1gInSc1	hrádek
PR	pr	k0	pr
Mušenice	Mušenice	k1gFnPc1	Mušenice
PR	pr	k0	pr
Nosperk	Nosperk	k1gInSc1	Nosperk
PR	pr	k0	pr
Rašovický	Rašovický	k2eAgInSc1d1	Rašovický
zlom	zlom	k1gInSc1	zlom
-	-	kIx~	-
Chobot	chobot	k1gInSc1	chobot
PR	pr	k0	pr
Šévy	Šévy	k1gInPc1	Šévy
PR	pr	k0	pr
U	u	k7c2	u
Vrby	Vrba	k1gMnSc2	Vrba
PR	pr	k0	pr
Velký	velký	k2eAgInSc1d1	velký
Kuntínov	Kuntínov	k1gInSc1	Kuntínov
PR	pr	k0	pr
Visengrunty	Visengrunt	k1gInPc1	Visengrunt
PR	pr	k0	pr
Zázmoníky	Zázmoník	k1gInPc4	Zázmoník
PP	PP	kA	PP
Baračka	Baračka	k1gFnSc1	Baračka
PP	PP	kA	PP
Hochberk	Hochberk	k1gInSc4	Hochberk
PP	PP	kA	PP
Hrubá	hrubý	k2eAgFnSc1d1	hrubá
louka	louka	k1gFnSc1	louka
PP	PP	kA	PP
Jalový	jalový	k2eAgInSc1d1	jalový
dvůr	dvůr	k1gInSc1	dvůr
PP	PP	kA	PP
Jesličky	jesličky	k1gFnPc1	jesličky
PP	PP	kA	PP
Kamenný	kamenný	k2eAgInSc4d1	kamenný
vrch	vrch	k1gInSc4	vrch
u	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s>
Kurdějova	Kurdějův	k2eAgFnSc1d1	Kurdějova
PP	PP	kA	PP
Lipiny	lipina	k1gFnSc2	lipina
PP	PP	kA	PP
Ochozy	ochoz	k1gInPc1	ochoz
PP	PP	kA	PP
Polámanky	Polámanka	k1gFnSc2	Polámanka
PP	PP	kA	PP
Přední	přední	k2eAgFnSc2d1	přední
kopaniny	kopanina	k1gFnSc2	kopanina
PP	PP	kA	PP
Roviny	rovina	k1gFnSc2	rovina
PP	PP	kA	PP
Žlíbek	žlíbek	k1gInSc1	žlíbek
Tvrz	tvrz	k1gFnSc1	tvrz
Bošovice	Bošovice	k1gFnSc1	Bošovice
<g/>
:	:	kIx,	:
gotická	gotický	k2eAgFnSc1d1	gotická
tvrz	tvrz	k1gFnSc1	tvrz
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Dambořicích	Dambořik	k1gInPc6	Dambořik
<g/>
:	:	kIx,	:
založen	založen	k2eAgInSc1d1	založen
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
kolem	kolem	k6eAd1	kolem
400	[number]	k4	400
náhrobních	náhrobní	k2eAgInPc2d1	náhrobní
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Diváky	Diváky	k1gInPc1	Diváky
<g/>
:	:	kIx,	:
barokní	barokní	k2eAgInSc1d1	barokní
zámek	zámek	k1gInSc1	zámek
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1747	[number]	k4	1747
přestavěn	přestavět	k5eAaPmNgInS	přestavět
a	a	k8xC	a
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenštejnův	Lichtenštejnův	k2eAgInSc1d1	Lichtenštejnův
památník	památník	k1gInSc1	památník
u	u	k7c2	u
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
jelena	jelen	k1gMnSc2	jelen
<g/>
,	,	kIx,	,
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
:	:	kIx,	:
kamenný	kamenný	k2eAgInSc1d1	kamenný
památník	památník	k1gInSc1	památník
ke	k	k7c3	k
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
vlády	vláda	k1gFnSc2	vláda
Johanna	Johann	k1gInSc2	Johann
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Josefa	Josef	k1gMnSc2	Josef
Františka	František	k1gMnSc2	František
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
na	na	k7c4	na
boj	boj	k1gInSc4	boj
u	u	k7c2	u
hájenky	hájenka	k1gFnSc2	hájenka
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
jelen	jelen	k1gMnSc1	jelen
<g/>
,	,	kIx,	,
Heršpice	Heršpice	k1gFnPc1	Heršpice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zde	zde	k6eAd1	zde
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
mezi	mezi	k7c7	mezi
partyzánským	partyzánský	k2eAgInSc7d1	partyzánský
oddílem	oddíl	k1gInSc7	oddíl
Olga	Olga	k1gFnSc1	Olga
a	a	k8xC	a
nacistickým	nacistický	k2eAgInSc7d1	nacistický
Jagdkommandem	Jagdkommand	k1gInSc7	Jagdkommand
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Klobouky	Klobouky	k1gInPc1	Klobouky
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
:	:	kIx,	:
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
budovat	budovat	k5eAaImF	budovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1589	[number]	k4	1589
jako	jako	k8xS	jako
renesanční	renesanční	k2eAgFnSc1d1	renesanční
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
interiéry	interiér	k1gInPc1	interiér
přestavěny	přestavěn	k2eAgInPc1d1	přestavěn
klasicistně	klasicistně	k6eAd1	klasicistně
<g/>
.	.	kIx.	.
</s>
<s>
Větrný	větrný	k2eAgInSc1d1	větrný
mlýn	mlýn	k1gInSc1	mlýn
v	v	k7c6	v
Kloboucích	Klobouky	k1gInPc6	Klobouky
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
:	:	kIx,	:
kulturní	kulturní	k2eAgFnSc1d1	kulturní
a	a	k8xC	a
technická	technický	k2eAgFnSc1d1	technická
památka	památka	k1gFnSc1	památka
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
v	v	k7c6	v
Kurdějově	kurdějově	k6eAd1	kurdějově
<g/>
:	:	kIx,	:
opevněný	opevněný	k2eAgMnSc1d1	opevněný
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc4d1	gotický
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
a	a	k8xC	a
nejzachovalejších	zachovalý	k2eAgInPc2d3	nejzachovalejší
opevněných	opevněný	k2eAgInPc2d1	opevněný
kostelů	kostel	k1gInPc2	kostel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Těšany	Těšana	k1gFnSc2	Těšana
<g/>
:	:	kIx,	:
barokní	barokní	k2eAgInSc1d1	barokní
zámek	zámek	k1gInSc1	zámek
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Kovárna	kovárna	k1gFnSc1	kovárna
v	v	k7c6	v
Těšanech	Těšan	k1gInPc6	Těšan
<g/>
:	:	kIx,	:
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
selského	selský	k2eAgNnSc2d1	selské
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
vybavení	vybavení	k1gNnSc2	vybavení
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Zámek	zámek	k1gInSc4	zámek
Ždánice	Ždánice	k1gFnPc4	Ždánice
<g/>
:	:	kIx,	:
původně	původně	k6eAd1	původně
tvrz	tvrz	k1gFnSc1	tvrz
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
<g/>
,	,	kIx,	,
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Přebudovaná	přebudovaný	k2eAgFnSc1d1	přebudovaná
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1762	[number]	k4	1762
<g/>
–	–	k?	–
<g/>
1789	[number]	k4	1789
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
