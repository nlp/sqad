<p>
<s>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
(	(	kIx(	(
<g/>
BIS	BIS	kA	BIS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
státní	státní	k2eAgFnSc1d1	státní
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
služba	služba	k1gFnSc1	služba
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
polem	pole	k1gNnSc7	pole
působnosti	působnost	k1gFnSc2	působnost
(	(	kIx(	(
<g/>
kontrarozvědka	kontrarozvědka	k1gFnSc1	kontrarozvědka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
sbor	sbor	k1gInSc1	sbor
získává	získávat	k5eAaImIp3nS	získávat
<g/>
,	,	kIx,	,
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
a	a	k8xC	a
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
informace	informace	k1gFnPc4	informace
důležité	důležitý	k2eAgFnPc4d1	důležitá
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
ochranu	ochrana	k1gFnSc4	ochrana
ústavního	ústavní	k2eAgNnSc2d1	ústavní
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
,	,	kIx,	,
demokratických	demokratický	k2eAgMnPc2d1	demokratický
principů	princip	k1gInPc2	princip
a	a	k8xC	a
významných	významný	k2eAgInPc2d1	významný
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
zájmů	zájem	k1gInPc2	zájem
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
BIS	BIS	kA	BIS
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
Vláda	vláda	k1gFnSc1	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zároveň	zároveň	k6eAd1	zároveň
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
její	její	k3xOp3gFnSc4	její
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Oprávnění	oprávnění	k1gNnSc4	oprávnění
ukládat	ukládat	k5eAaImF	ukládat
úkoly	úkol	k1gInPc4	úkol
BIS	BIS	kA	BIS
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
její	její	k3xOp3gFnSc2	její
působnosti	působnost	k1gFnSc2	působnost
náleží	náležet	k5eAaImIp3nS	náležet
i	i	k9	i
prezidentu	prezident	k1gMnSc3	prezident
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
BIS	BIS	kA	BIS
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaImNgInS	jmenovat
a	a	k8xC	a
případně	případně	k6eAd1	případně
odvolán	odvolán	k2eAgInSc4d1	odvolán
Vládou	vláda	k1gFnSc7	vláda
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
BIS	BIS	kA	BIS
nespadá	spadat	k5eNaImIp3nS	spadat
pod	pod	k7c4	pod
žádné	žádný	k3yNgNnSc4	žádný
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
samostatným	samostatný	k2eAgInSc7d1	samostatný
státním	státní	k2eAgInSc7d1	státní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
BIS	BIS	kA	BIS
je	být	k5eAaImIp3nS	být
přímou	přímý	k2eAgFnSc7d1	přímá
součástí	součást	k1gFnSc7	součást
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
výše	výše	k1gFnSc2	výše
1	[number]	k4	1
186	[number]	k4	186
111	[number]	k4	111
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Náplň	náplň	k1gFnSc1	náplň
činnosti	činnost	k1gFnSc2	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
BIS	BIS	kA	BIS
smí	smět	k5eAaImIp3nS	smět
získávat	získávat	k5eAaImF	získávat
pouze	pouze	k6eAd1	pouze
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
terorismu	terorismus	k1gInSc3	terorismus
</s>
</p>
<p>
<s>
bezpečnosti	bezpečnost	k1gFnPc1	bezpečnost
nebo	nebo	k8xC	nebo
významných	významný	k2eAgInPc2d1	významný
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
zájmů	zájem	k1gInPc2	zájem
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
činnosti	činnost	k1gFnPc4	činnost
cizích	cizí	k2eAgFnPc2d1	cizí
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
</s>
</p>
<p>
<s>
záměry	záměr	k1gInPc1	záměr
nebo	nebo	k8xC	nebo
činy	čin	k1gInPc1	čin
mířící	mířící	k2eAgFnSc2d1	mířící
proti	proti	k7c3	proti
demokratickým	demokratický	k2eAgInPc3d1	demokratický
základům	základ	k1gInPc3	základ
<g/>
,	,	kIx,	,
svrchovanosti	svrchovanost	k1gFnSc3	svrchovanost
a	a	k8xC	a
územní	územní	k2eAgFnSc3d1	územní
celistvosti	celistvost	k1gFnSc3	celistvost
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
extremismu	extremismus	k1gInSc2	extremismus
</s>
</p>
<p>
<s>
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
a	a	k8xC	a
terorismu	terorismus	k1gInSc2	terorismus
</s>
</p>
<p>
<s>
ohrožení	ohrožení	k1gNnSc1	ohrožení
utajovaných	utajovaný	k2eAgFnPc2d1	utajovaná
informací	informace	k1gFnPc2	informace
</s>
</p>
<p>
<s>
kybernetické	kybernetický	k2eAgNnSc1d1	kybernetické
bezpečnostiBIS	bezpečnostiBIS	k?	bezpečnostiBIS
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
trestně-právní	trestněrávní	k2eAgFnPc4d1	trestně-právní
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
tuzemskými	tuzemský	k2eAgFnPc7d1	tuzemská
i	i	k8xC	i
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
zpravodajskými	zpravodajský	k2eAgFnPc7d1	zpravodajská
službami	služba	k1gFnPc7	služba
<g/>
,	,	kIx,	,
také	také	k9	také
s	s	k7c7	s
Policií	policie	k1gFnSc7	policie
ČR	ČR	kA	ČR
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
státními	státní	k2eAgInPc7d1	státní
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečné	výjimečný	k2eAgNnSc1d1	výjimečné
postavení	postavení	k1gNnSc1	postavení
policejního	policejní	k2eAgInSc2d1	policejní
orgánu	orgán	k1gInSc2	orgán
má	mít	k5eAaImIp3nS	mít
inspekce	inspekce	k1gFnSc1	inspekce
BIS	BIS	kA	BIS
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
příslušníků	příslušník	k1gMnPc2	příslušník
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
</s>
<s>
BIS	BIS	kA	BIS
je	být	k5eAaImIp3nS	být
vojensky	vojensky	k6eAd1	vojensky
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
<g/>
,	,	kIx,	,
ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
služba	služba	k1gFnSc1	služba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
příslušníci	příslušník	k1gMnPc1	příslušník
jsou	být	k5eAaImIp3nP	být
oprávnění	oprávnění	k1gNnSc4	oprávnění
držet	držet	k5eAaImF	držet
a	a	k8xC	a
nosit	nosit	k5eAaImF	nosit
služební	služební	k2eAgFnSc4d1	služební
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Použít	použít	k5eAaPmF	použít
ji	on	k3xPp3gFnSc4	on
mohou	moct	k5eAaImIp3nP	moct
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
nutné	nutný	k2eAgFnSc2d1	nutná
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
krajní	krajní	k2eAgFnSc2d1	krajní
nouze	nouze	k1gFnSc2	nouze
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
kterýkoliv	kterýkoliv	k3yIgMnSc1	kterýkoliv
jiný	jiný	k2eAgMnSc1d1	jiný
občan	občan	k1gMnSc1	občan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BIS	BIS	kA	BIS
získává	získávat	k5eAaImIp3nS	získávat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
otevřených	otevřený	k2eAgInPc2d1	otevřený
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
z	z	k7c2	z
agenturní	agenturní	k2eAgFnSc2d1	agenturní
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
osoby	osoba	k1gFnPc4	osoba
jednající	jednající	k2eAgFnPc4d1	jednající
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
BIS	BIS	kA	BIS
-	-	kIx~	-
HUMINT	HUMINT	kA	HUMINT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
nebo	nebo	k8xC	nebo
zpravodajskými	zpravodajský	k2eAgInPc7d1	zpravodajský
prostředky	prostředek	k1gInPc7	prostředek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
technika	technika	k1gFnSc1	technika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
odposlech	odposlech	k1gInSc4	odposlech
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
-	-	kIx~	-
SIGINT	SIGINT	kA	SIGINT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sledování	sledování	k1gNnSc1	sledování
</s>
</p>
<p>
<s>
krycí	krycí	k2eAgInPc1d1	krycí
dokladyOdposlechy	dokladyOdposlech	k1gInPc1	dokladyOdposlech
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
BIS	BIS	kA	BIS
musí	muset	k5eAaImIp3nP	muset
nejprve	nejprve	k6eAd1	nejprve
povolit	povolit	k5eAaPmF	povolit
předseda	předseda	k1gMnSc1	předseda
senátu	senát	k1gInSc2	senát
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sledování	sledování	k1gNnSc1	sledování
musí	muset	k5eAaImIp3nS	muset
nejprve	nejprve	k6eAd1	nejprve
odsouhlasit	odsouhlasit	k5eAaPmF	odsouhlasit
ředitel	ředitel	k1gMnSc1	ředitel
BIS	BIS	kA	BIS
nebo	nebo	k8xC	nebo
jím	jíst	k5eAaImIp1nS	jíst
pověřená	pověřený	k2eAgFnSc1d1	pověřená
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Sledování	sledování	k1gNnSc1	sledování
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
nejnákladnější	nákladný	k2eAgInSc4d3	nejnákladnější
a	a	k8xC	a
nejnáročnější	náročný	k2eAgInSc4d3	nejnáročnější
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
nejúčinnější	účinný	k2eAgInSc1d3	nejúčinnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krycí	krycí	k2eAgInPc1d1	krycí
prostředky	prostředek	k1gInPc1	prostředek
a	a	k8xC	a
krycí	krycí	k2eAgInPc1d1	krycí
doklady	doklad	k1gInPc1	doklad
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
utajení	utajení	k1gNnSc3	utajení
skutečné	skutečný	k2eAgFnSc2d1	skutečná
totožnosti	totožnost	k1gFnSc2	totožnost
důstojníka	důstojník	k1gMnSc2	důstojník
BIS	BIS	kA	BIS
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
příslušnosti	příslušnost	k1gFnSc2	příslušnost
ke	k	k7c3	k
Službě	služba	k1gFnSc3	služba
a	a	k8xC	a
k	k	k7c3	k
utajení	utajení	k1gNnSc3	utajení
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
zájmů	zájem	k1gInPc2	zájem
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
</s>
<s>
Krycím	krycí	k2eAgInSc7d1	krycí
dokladem	doklad	k1gInSc7	doklad
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
nikdy	nikdy	k6eAd1	nikdy
použit	použit	k2eAgInSc4d1	použit
průkaz	průkaz	k1gInSc4	průkaz
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
poslance	poslanec	k1gMnSc2	poslanec
nebo	nebo	k8xC	nebo
senátora	senátor	k1gMnSc2	senátor
</s>
</p>
<p>
<s>
člena	člen	k1gMnSc4	člen
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
člena	člen	k1gMnSc4	člen
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
úřadu	úřad	k1gInSc2	úřad
</s>
</p>
<p>
<s>
guvernéra	guvernér	k1gMnSc4	guvernér
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
</s>
</p>
<p>
<s>
služební	služební	k2eAgInSc4d1	služební
průkaz	průkaz	k1gInSc4	průkaz
státního	státní	k2eAgMnSc2d1	státní
zástupce	zástupce	k1gMnSc2	zástupce
a	a	k8xC	a
soudce	soudce	k1gMnSc2	soudce
</s>
</p>
<p>
<s>
diplomatický	diplomatický	k2eAgInSc1d1	diplomatický
pas	pas	k1gInSc1	pas
</s>
</p>
<p>
<s>
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
doklad	doklad	k1gInSc4	doklad
žijící	žijící	k2eAgNnSc4d1	žijící
osobyFungování	osobyFungování	k1gNnSc4	osobyFungování
českých	český	k2eAgFnPc2d1	Česká
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
153	[number]	k4	153
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
zpravodajských	zpravodajský	k2eAgFnPc6d1	zpravodajská
službách	služba	k1gFnPc6	služba
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
BIS	BIS	kA	BIS
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
154	[number]	k4	154
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
Bezpečnostní	bezpečnostní	k2eAgFnSc6d1	bezpečnostní
informační	informační	k2eAgFnSc6d1	informační
službě	služba	k1gFnSc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Služební	služební	k2eAgInSc1d1	služební
poměr	poměr	k1gInSc1	poměr
příslušníků	příslušník	k1gMnPc2	příslušník
BIS	BIS	kA	BIS
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
361	[number]	k4	361
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
služebním	služební	k2eAgInSc6d1	služební
poměru	poměr	k1gInSc6	poměr
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
prvkem	prvek	k1gInSc7	prvek
pro	pro	k7c4	pro
utajenou	utajený	k2eAgFnSc4d1	utajená
činnost	činnost	k1gFnSc4	činnost
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
je	být	k5eAaImIp3nS	být
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
412	[number]	k4	412
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
utajovaných	utajovaný	k2eAgFnPc2d1	utajovaná
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
o	o	k7c6	o
bezpečnostní	bezpečnostní	k2eAgFnSc6d1	bezpečnostní
způsobilosti	způsobilost	k1gFnSc6	způsobilost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
svých	svůj	k3xOyFgInPc2	svůj
úkolů	úkol	k1gInPc2	úkol
je	být	k5eAaImIp3nS	být
BIS	BIS	kA	BIS
oprávněna	oprávnit	k5eAaPmNgFnS	oprávnit
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
zpravodajskými	zpravodajský	k2eAgFnPc7d1	zpravodajská
službami	služba	k1gFnPc7	služba
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
ÚZSI	ÚZSI	kA	ÚZSI
a	a	k8xC	a
VZ	VZ	kA	VZ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
153	[number]	k4	153
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
tuto	tento	k3xDgFnSc4	tento
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
§	§	k?	§
9	[number]	k4	9
podmiňuje	podmiňovat	k5eAaImIp3nS	podmiňovat
dohodami	dohoda	k1gFnPc7	dohoda
uzavíranými	uzavíraný	k2eAgFnPc7d1	uzavíraná
mezi	mezi	k7c7	mezi
zpravodajskými	zpravodajský	k2eAgFnPc7d1	zpravodajská
službami	služba	k1gFnPc7	služba
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráci	spolupráce	k1gFnSc4	spolupráce
se	s	k7c7	s
zpravodajskými	zpravodajský	k2eAgFnPc7d1	zpravodajská
službami	služba	k1gFnPc7	služba
cizí	cizí	k2eAgFnSc2d1	cizí
moci	moc	k1gFnSc2	moc
může	moct	k5eAaImIp3nS	moct
BIS	BIS	kA	BIS
podle	podle	k7c2	podle
§	§	k?	§
10	[number]	k4	10
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
153	[number]	k4	153
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Znak	znak	k1gInSc4	znak
BIS	BIS	kA	BIS
==	==	k?	==
</s>
</p>
<p>
<s>
Znak	znak	k1gInSc1	znak
BIS	BIS	kA	BIS
vznikal	vznikat	k5eAaImAgInS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991	[number]	k4	1991
–	–	k?	–
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
rodící	rodící	k2eAgFnSc3d1	rodící
se	se	k3xPyFc4	se
zpravodajské	zpravodajský	k2eAgFnSc3d1	zpravodajská
službě	služba	k1gFnSc3	služba
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dát	dát	k5eAaPmF	dát
do	do	k7c2	do
vínku	vínek	k1gInSc2	vínek
i	i	k8xC	i
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
symboliku	symbolika	k1gFnSc4	symbolika
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
spatřil	spatřit	k5eAaPmAgMnS	spatřit
v	v	k7c6	v
atelieru	atelier	k1gInSc6	atelier
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Jana	Jan	k1gMnSc2	Jan
Glozara	Glozar	k1gMnSc2	Glozar
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
jako	jako	k8xC	jako
odznak	odznak	k1gInSc1	odznak
Federální	federální	k2eAgFnSc2d1	federální
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
informační	informační	k2eAgFnSc2d1	informační
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
byla	být	k5eAaImAgFnS	být
formulace	formulace	k1gFnSc1	formulace
několika	několik	k4yIc2	několik
základních	základní	k2eAgInPc2d1	základní
požadavků	požadavek	k1gInPc2	požadavek
<g/>
:	:	kIx,	:
naznačení	naznačení	k1gNnSc1	naznačení
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
metody	metoda	k1gFnSc2	metoda
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
vyjádření	vyjádření	k1gNnSc2	vyjádření
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
houževnatosti	houževnatost	k1gFnSc2	houževnatost
<g/>
,	,	kIx,	,
bystrosti	bystrost	k1gFnSc2	bystrost
<g/>
,	,	kIx,	,
ostražitosti	ostražitost	k1gFnSc2	ostražitost
<g/>
,	,	kIx,	,
rychlosti	rychlost	k1gFnSc2	rychlost
a	a	k8xC	a
respektu	respekt	k1gInSc2	respekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
znaku	znak	k1gInSc2	znak
tvoří	tvořit	k5eAaImIp3nS	tvořit
velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
symbol	symbol	k1gInSc1	symbol
umístěný	umístěný	k2eAgInSc1d1	umístěný
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
orlice	orlice	k1gFnSc2	orlice
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
překrytý	překrytý	k2eAgInSc1d1	překrytý
jejím	její	k3xOp3gNnSc7	její
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
Audi	Audi	k1gNnSc1	Audi
<g/>
,	,	kIx,	,
vide	vid	k1gInSc5	vid
<g/>
,	,	kIx,	,
tace	tacus	k1gMnSc5	tacus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
BIS	BIS	kA	BIS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Orlice	Orlice	k1gFnSc1	Orlice
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
bystrost	bystrost	k1gFnSc4	bystrost
<g/>
,	,	kIx,	,
ostražitost	ostražitost	k1gFnSc4	ostražitost
<g/>
,	,	kIx,	,
houževnatost	houževnatost	k1gFnSc4	houževnatost
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
respekt	respekt	k1gInSc4	respekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
ptačí	ptačí	k2eAgFnSc1d1	ptačí
říše	říše	k1gFnSc1	říše
bere	brát	k5eAaImIp3nS	brát
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
orlice	orlice	k1gFnSc1	orlice
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
člověku	člověk	k1gMnSc6	člověk
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
latinské	latinský	k2eAgNnSc4d1	latinské
heslo	heslo	k1gNnSc4	heslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušník	příslušník	k1gMnSc1	příslušník
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
musí	muset	k5eAaImIp3nS	muset
dobře	dobře	k6eAd1	dobře
naslouchat	naslouchat	k5eAaImF	naslouchat
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
a	a	k8xC	a
mlčet	mlčet	k5eAaImF	mlčet
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
musí	muset	k5eAaImIp3nS	muset
dodržovat	dodržovat	k5eAaImF	dodržovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
zásady	zásada	k1gFnPc4	zásada
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušník	příslušník	k1gMnSc1	příslušník
BIS	BIS	kA	BIS
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
službách	služba	k1gFnPc6	služba
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
povinností	povinnost	k1gFnSc7	povinnost
je	být	k5eAaImIp3nS	být
chránit	chránit	k5eAaImF	chránit
státní	státní	k2eAgInPc4d1	státní
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
(	(	kIx(	(
<g/>
srdci	srdce	k1gNnSc6	srdce
<g/>
)	)	kIx)	)
orlice	orlice	k1gFnSc1	orlice
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
nadřazení	nadřazení	k1gNnSc4	nadřazení
státních	státní	k2eAgInPc2d1	státní
zájmů	zájem	k1gInPc2	zájem
nad	nad	k7c4	nad
zájmy	zájem	k1gInPc4	zájem
osobní	osobní	k2eAgInPc4d1	osobní
<g/>
.	.	kIx.	.
</s>
<s>
Částečné	částečný	k2eAgNnSc1d1	částečné
překrytí	překrytí	k1gNnSc1	překrytí
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
křídlem	křídlo	k1gNnSc7	křídlo
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
ochranu	ochrana	k1gFnSc4	ochrana
státnosti	státnost	k1gFnSc2	státnost
a	a	k8xC	a
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
orlice	orlice	k1gFnSc2	orlice
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
každým	každý	k3xTgInSc7	každý
pokusem	pokus	k1gInSc7	pokus
o	o	k7c6	o
narušení	narušení	k1gNnSc6	narušení
ochrany	ochrana	k1gFnSc2	ochrana
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
postih	postih	k1gInSc4	postih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Služební	služební	k2eAgFnSc1d1	služební
přísaha	přísaha	k1gFnSc1	přísaha
===	===	k?	===
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Slibuji	slibovat	k5eAaImIp1nS	slibovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
čest	čest	k1gFnSc4	čest
a	a	k8xC	a
svědomí	svědomí	k1gNnSc4	svědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
služby	služba	k1gFnSc2	služba
budu	být	k5eAaImBp1nS	být
nestranný	nestranný	k2eAgMnSc1d1	nestranný
a	a	k8xC	a
budu	být	k5eAaImBp1nS	být
důsledně	důsledně	k6eAd1	důsledně
dodržovat	dodržovat	k5eAaImF	dodržovat
právní	právní	k2eAgInPc4d1	právní
a	a	k8xC	a
služební	služební	k2eAgInPc4d1	služební
předpisy	předpis	k1gInPc4	předpis
<g/>
,	,	kIx,	,
plnit	plnit	k5eAaImF	plnit
rozkazy	rozkaz	k1gInPc4	rozkaz
svých	svůj	k3xOyFgMnPc2	svůj
nadřízených	nadřízený	k1gMnPc2	nadřízený
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nezneužiji	zneužít	k5eNaPmIp1nS	zneužít
svého	svůj	k3xOyFgNnSc2	svůj
služebního	služební	k2eAgNnSc2d1	služební
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Budu	být	k5eAaImBp1nS	být
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
a	a	k8xC	a
všude	všude	k6eAd1	všude
chovat	chovat	k5eAaImF	chovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
svým	svůj	k3xOyFgMnPc3	svůj
jednáním	jednání	k1gNnPc3	jednání
neohrozil	ohrozit	k5eNaPmAgInS	ohrozit
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pověst	pověst	k1gFnSc4	pověst
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Služební	služební	k2eAgFnPc4d1	služební
povinnosti	povinnost	k1gFnPc4	povinnost
budu	být	k5eAaImBp1nS	být
plnit	plnit	k5eAaImF	plnit
řádně	řádně	k6eAd1	řádně
a	a	k8xC	a
svědomitě	svědomitě	k6eAd1	svědomitě
a	a	k8xC	a
nebudu	být	k5eNaImBp1nS	být
váhat	váhat	k5eAaImF	váhat
při	při	k7c6	při
ochraně	ochrana	k1gFnSc6	ochrana
zájmů	zájem	k1gInPc2	zájem
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
nasadit	nasadit	k5eAaPmF	nasadit
i	i	k9	i
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Zveřejňování	zveřejňování	k1gNnSc4	zveřejňování
výročních	výroční	k2eAgFnPc2d1	výroční
zpráv	zpráva	k1gFnPc2	zpráva
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
většinu	většina	k1gFnSc4	většina
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
musí	muset	k5eAaImIp3nS	muset
BIS	BIS	kA	BIS
vykonávat	vykonávat	k5eAaImF	vykonávat
utajeně	utajeně	k6eAd1	utajeně
–	–	k?	–
protože	protože	k8xS	protože
jedině	jedině	k6eAd1	jedině
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
efektivně	efektivně	k6eAd1	efektivně
plnit	plnit	k5eAaImF	plnit
zákonem	zákon	k1gInSc7	zákon
uložené	uložený	k2eAgInPc4d1	uložený
úkoly	úkol	k1gInPc4	úkol
–	–	k?	–
nelze	lze	k6eNd1	lze
pominout	pominout	k5eAaPmF	pominout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
placena	platit	k5eAaImNgFnS	platit
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
penězi	peníze	k1gInPc7	peníze
daňových	daňový	k2eAgInPc2d1	daňový
poplatníků	poplatník	k1gMnPc2	poplatník
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
pracuje	pracovat	k5eAaImIp3nS	pracovat
a	a	k8xC	a
čím	čí	k3xOyRgNnSc7	čí
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
porušen	porušen	k2eAgInSc1d1	porušen
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
utajovaných	utajovaný	k2eAgFnPc2d1	utajovaná
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
o	o	k7c6	o
bezpečnostní	bezpečnostní	k2eAgFnSc6d1	bezpečnostní
způsobilosti	způsobilost	k1gFnSc6	způsobilost
(	(	kIx(	(
<g/>
č.	č.	k?	č.
412	[number]	k4	412
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
bránil	bránit	k5eAaImAgMnS	bránit
BIS	BIS	kA	BIS
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otevřenosti	otevřenost	k1gFnSc3	otevřenost
se	se	k3xPyFc4	se
Služba	služba	k1gFnSc1	služba
hlásí	hlásit	k5eAaImIp3nS	hlásit
a	a	k8xC	a
odpovídáním	odpovídání	k1gNnSc7	odpovídání
na	na	k7c4	na
dotazy	dotaz	k1gInPc4	dotaz
<g/>
,	,	kIx,	,
poskytováním	poskytování	k1gNnSc7	poskytování
vyjádření	vyjádření	k1gNnSc2	vyjádření
a	a	k8xC	a
dobrovolným	dobrovolný	k2eAgNnSc7d1	dobrovolné
<g/>
,	,	kIx,	,
vstřícným	vstřícný	k2eAgNnSc7d1	vstřícné
publikováním	publikování	k1gNnSc7	publikování
veřejných	veřejný	k2eAgFnPc2d1	veřejná
výročních	výroční	k2eAgFnPc2d1	výroční
zpráv	zpráva	k1gFnPc2	zpráva
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
konkrétně	konkrétně	k6eAd1	konkrétně
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
se	se	k3xPyFc4	se
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
neobejde	obejde	k6eNd1	obejde
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
užitečné	užitečný	k2eAgFnPc4d1	užitečná
a	a	k8xC	a
relevantní	relevantní	k2eAgFnPc4d1	relevantní
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgFnPc4d1	spadající
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
BIS	BIS	kA	BIS
zabývá	zabývat	k5eAaImIp3nS	zabývat
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
kdykoliv	kdykoliv	k6eAd1	kdykoliv
přímo	přímo	k6eAd1	přímo
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
<g/>
.	.	kIx.	.
</s>
<s>
Služba	služba	k1gFnSc1	služba
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
veřejnosti	veřejnost	k1gFnSc3	veřejnost
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vděčna	vděčen	k2eAgFnSc1d1	vděčna
každému	každý	k3xTgMnSc3	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jí	on	k3xPp3gFnSc7	on
pomohl	pomoct	k5eAaPmAgInS	pomoct
–	–	k?	–
nebo	nebo	k8xC	nebo
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
–	–	k?	–
při	při	k7c6	při
ochraně	ochrana	k1gFnSc6	ochrana
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontrola	kontrola	k1gFnSc1	kontrola
a	a	k8xC	a
dohled	dohled	k1gInSc1	dohled
==	==	k?	==
</s>
</p>
<p>
<s>
Kontrola	kontrola	k1gFnSc1	kontrola
činnosti	činnost	k1gFnSc2	činnost
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
informační	informační	k2eAgFnSc2d1	informační
služby	služba	k1gFnSc2	služba
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
rovinách	rovina	k1gFnPc6	rovina
–	–	k?	–
vnější	vnější	k2eAgFnSc6d1	vnější
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc4d1	vnější
rovinu	rovina	k1gFnSc4	rovina
představuje	představovat	k5eAaImIp3nS	představovat
kontrola	kontrola	k1gFnSc1	kontrola
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Službu	služba	k1gFnSc4	služba
řídí	řídit	k5eAaImIp3nS	řídit
<g/>
,	,	kIx,	,
a	a	k8xC	a
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
kontrola	kontrola	k1gFnSc1	kontrola
je	být	k5eAaImIp3nS	být
svěřena	svěřit	k5eAaPmNgFnS	svěřit
interním	interní	k2eAgMnPc3d1	interní
mechanismům	mechanismus	k1gInPc3	mechanismus
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
Inspekce	inspekce	k1gFnSc1	inspekce
(	(	kIx(	(
<g/>
přímo	přímo	k6eAd1	přímo
podléhající	podléhající	k2eAgFnSc1d1	podléhající
řediteli	ředitel	k1gMnSc3	ředitel
BIS	BIS	kA	BIS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Odbor	odbor	k1gInSc1	odbor
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
Skupina	skupina	k1gFnSc1	skupina
interního	interní	k2eAgInSc2d1	interní
auditu	audit	k1gInSc2	audit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vnější	vnější	k2eAgFnSc1d1	vnější
kontrola	kontrola	k1gFnSc1	kontrola
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Role	role	k1gFnSc1	role
vlády	vláda	k1gFnSc2	vláda
====	====	k?	====
</s>
</p>
<p>
<s>
Za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
koordinaci	koordinace	k1gFnSc4	koordinace
všech	všecek	k3xTgFnPc2	všecek
českých	český	k2eAgFnPc2d1	Česká
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
si	se	k3xPyFc3	se
svým	svůj	k3xOyFgNnSc7	svůj
usnesením	usnesení	k1gNnSc7	usnesení
zřídila	zřídit	k5eAaPmAgFnS	zřídit
Výbor	výbor	k1gInSc4	výbor
pro	pro	k7c4	pro
zpravodajskou	zpravodajský	k2eAgFnSc4d1	zpravodajská
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
stálým	stálý	k2eAgMnSc7d1	stálý
pracovním	pracovní	k2eAgInSc7d1	pracovní
orgánem	orgán	k1gInSc7	orgán
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
rady	rada	k1gFnSc2	rada
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
výboru	výbor	k1gInSc2	výbor
je	být	k5eAaImIp3nS	být
premiér	premiér	k1gMnSc1	premiér
<g/>
,	,	kIx,	,
místopředsedou	místopředseda	k1gMnSc7	místopředseda
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
členy	člen	k1gInPc4	člen
jsou	být	k5eAaImIp3nP	být
ministři	ministr	k1gMnPc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
ředitelé	ředitel	k1gMnPc1	ředitel
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
informační	informační	k2eAgFnSc2d1	informační
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
styky	styk	k1gInPc4	styk
a	a	k8xC	a
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
Vojenského	vojenský	k2eAgNnSc2d1	vojenské
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
a	a	k8xC	a
sekce	sekce	k1gFnSc1	sekce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
výbor	výbor	k1gInSc4	výbor
též	též	k9	též
místem	místo	k1gNnSc7	místo
operativní	operativní	k2eAgFnSc2d1	operativní
meziresortní	meziresortní	k2eAgFnSc2d1	meziresortní
koordinace	koordinace	k1gFnSc2	koordinace
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
případné	případný	k2eAgFnSc2d1	případná
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
státními	státní	k2eAgInPc7d1	státní
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Role	role	k1gFnSc1	role
parlamentu	parlament	k1gInSc2	parlament
====	====	k?	====
</s>
</p>
<p>
<s>
Úkolem	úkol	k1gInSc7	úkol
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
kontroly	kontrola	k1gFnSc2	kontrola
je	být	k5eAaImIp3nS	být
–	–	k?	–
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
–	–	k?	–
dohlížet	dohlížet	k5eAaImF	dohlížet
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
BIS	BIS	kA	BIS
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
neporušuje	porušovat	k5eNaImIp3nS	porušovat
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
BIS	BIS	kA	BIS
si	se	k3xPyFc3	se
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
sedmičlenný	sedmičlenný	k2eAgInSc4d1	sedmičlenný
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
orgán	orgán	k1gInSc4	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
kontrola	kontrola	k1gFnSc1	kontrola
===	===	k?	===
</s>
</p>
<p>
<s>
Kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
systém	systém	k1gInSc1	systém
BIS	BIS	kA	BIS
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
základních	základní	k2eAgInPc6d1	základní
pilířích	pilíř	k1gInPc6	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
je	být	k5eAaImIp3nS	být
konkrétní	konkrétní	k2eAgNnPc1d1	konkrétní
<g/>
,	,	kIx,	,
jasně	jasně	k6eAd1	jasně
stanovená	stanovený	k2eAgFnSc1d1	stanovená
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
řídících	řídící	k2eAgMnPc2d1	řídící
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
pověření	pověření	k1gNnSc4	pověření
garančních	garanční	k2eAgInPc2d1	garanční
útvarů	útvar	k1gInPc2	útvar
provádět	provádět	k5eAaImF	provádět
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
<g/>
,	,	kIx,	,
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
kontrolu	kontrola	k1gFnSc4	kontrola
určených	určený	k2eAgFnPc2d1	určená
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
pilíř	pilíř	k1gInSc1	pilíř
představuje	představovat	k5eAaImIp3nS	představovat
decentralizace	decentralizace	k1gFnSc2	decentralizace
zpravodajského	zpravodajský	k2eAgInSc2d1	zpravodajský
výkonu	výkon	k1gInSc2	výkon
mezi	mezi	k7c7	mezi
více	hodně	k6eAd2	hodně
vzájemně	vzájemně	k6eAd1	vzájemně
nezávislých	závislý	k2eNgInPc2d1	nezávislý
subjektů	subjekt	k1gInPc2	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
decentralizace	decentralizace	k1gFnSc1	decentralizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
informačního	informační	k2eAgInSc2d1	informační
toku	tok	k1gInSc2	tok
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gNnPc2	jejich
archivování	archivování	k1gNnPc2	archivování
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zamezit	zamezit	k5eAaPmF	zamezit
případnému	případný	k2eAgNnSc3d1	případné
zneužití	zneužití	k1gNnSc3	zneužití
operativních	operativní	k2eAgInPc2d1	operativní
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
kontrola	kontrola	k1gFnSc1	kontrola
se	s	k7c7	s
mj.	mj.	kA	mj.
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
na	na	k7c4	na
oprávněnost	oprávněnost	k1gFnSc4	oprávněnost
zpravodajských	zpravodajský	k2eAgMnPc2d1	zpravodajský
úkonů	úkon	k1gInPc2	úkon
a	a	k8xC	a
dodržování	dodržování	k1gNnSc4	dodržování
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
na	na	k7c4	na
čerpání	čerpání	k1gNnSc4	čerpání
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
na	na	k7c6	na
používání	používání	k1gNnSc6	používání
materiálně-technických	materiálněechnický	k2eAgInPc2d1	materiálně-technický
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
kontroly	kontrola	k1gFnSc2	kontrola
a	a	k8xC	a
dohledu	dohled	k1gInSc2	dohled
je	být	k5eAaImIp3nS	být
koncipován	koncipovat	k5eAaBmNgInS	koncipovat
jako	jako	k8xS	jako
dynamický	dynamický	k2eAgInSc1d1	dynamický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
aktualizuje	aktualizovat	k5eAaBmIp3nS	aktualizovat
a	a	k8xC	a
optimalizuje	optimalizovat	k5eAaBmIp3nS	optimalizovat
podle	podle	k7c2	podle
vývoje	vývoj	k1gInSc2	vývoj
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
a	a	k8xC	a
vnějšího	vnější	k2eAgNnSc2d1	vnější
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
přitom	přitom	k6eAd1	přitom
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
pouhé	pouhý	k2eAgNnSc1d1	pouhé
zjišťování	zjišťování	k1gNnSc1	zjišťování
chyb	chyba	k1gFnPc2	chyba
a	a	k8xC	a
nedostatků	nedostatek	k1gInPc2	nedostatek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
prevence	prevence	k1gFnSc1	prevence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Inspekce	inspekce	k1gFnSc2	inspekce
====	====	k?	====
</s>
</p>
<p>
<s>
Inspekce	inspekce	k1gFnSc1	inspekce
–	–	k?	–
přesněji	přesně	k6eAd2	přesně
Odbor	odbor	k1gInSc1	odbor
inspekce	inspekce	k1gFnSc2	inspekce
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
informační	informační	k2eAgFnSc2d1	informační
</s>
</p>
<p>
<s>
služby	služba	k1gFnPc4	služba
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
uvnitř	uvnitř	k7c2	uvnitř
BIS	BIS	kA	BIS
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
postavení	postavení	k1gNnSc1	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
podřízena	podřízen	k2eAgFnSc1d1	podřízena
řediteli	ředitel	k1gMnPc7	ředitel
BIS	BIS	kA	BIS
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
útvarů	útvar	k1gInPc2	útvar
a	a	k8xC	a
podle	podle	k7c2	podle
trestního	trestní	k2eAgInSc2d1	trestní
řádu	řád	k1gInSc2	řád
má	mít	k5eAaImIp3nS	mít
oprávnění	oprávnění	k1gNnSc4	oprávnění
fungovat	fungovat	k5eAaImF	fungovat
jako	jako	k8xS	jako
policejní	policejní	k2eAgInSc1d1	policejní
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Zmíněné	zmíněný	k2eAgNnSc1d1	zmíněné
oprávnění	oprávnění	k1gNnSc1	oprávnění
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
hlavního	hlavní	k2eAgNnSc2d1	hlavní
poslání	poslání	k1gNnSc2	poslání
odhalovat	odhalovat	k5eAaImF	odhalovat
a	a	k8xC	a
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
přestupky	přestupek	k1gInPc4	přestupek
a	a	k8xC	a
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
příslušníků	příslušník	k1gMnPc2	příslušník
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
</s>
<s>
Inspekce	inspekce	k1gFnSc1	inspekce
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
<g/>
,	,	kIx,	,
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
stopy	stopa	k1gFnPc4	stopa
<g/>
,	,	kIx,	,
získávat	získávat	k5eAaImF	získávat
důkazy	důkaz	k1gInPc1	důkaz
potřebné	potřebný	k2eAgInPc1d1	potřebný
k	k	k7c3	k
objasnění	objasnění	k1gNnSc3	objasnění
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
případu	případ	k1gInSc2	případ
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
přímo	přímo	k6eAd1	přímo
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
náplně	náplň	k1gFnSc2	náplň
její	její	k3xOp3gFnSc2	její
práce	práce	k1gFnSc2	práce
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
příjem	příjem	k1gInSc1	příjem
a	a	k8xC	a
prověřování	prověřování	k1gNnSc1	prověřování
stížností	stížnost	k1gFnPc2	stížnost
a	a	k8xC	a
oznámení	oznámení	k1gNnPc2	oznámení
občanů	občan	k1gMnPc2	občan
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
Služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Získá	získat	k5eAaPmIp3nS	získat
<g/>
-li	i	k?	-li
Inspekce	inspekce	k1gFnSc1	inspekce
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušník	příslušník	k1gMnSc1	příslušník
BIS	BIS	kA	BIS
spáchal	spáchat	k5eAaPmAgInS	spáchat
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
využije	využít	k5eAaPmIp3nS	využít
své	svůj	k3xOyFgNnSc4	svůj
policejní	policejní	k2eAgNnSc4d1	policejní
oprávnění	oprávnění	k1gNnSc4	oprávnění
a	a	k8xC	a
rozběhne	rozběhnout	k5eAaPmIp3nS	rozběhnout
vlastní	vlastní	k2eAgNnSc4d1	vlastní
šetření	šetření	k1gNnSc4	šetření
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
podezření	podezření	k1gNnSc1	podezření
z	z	k7c2	z
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
<g/>
,	,	kIx,	,
předá	předat	k5eAaPmIp3nS	předat
celou	celý	k2eAgFnSc4d1	celá
věc	věc	k1gFnSc4	věc
státnímu	státní	k2eAgMnSc3d1	státní
zástupci	zástupce	k1gMnSc3	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
Inspekce	inspekce	k1gFnSc2	inspekce
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c4	o
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
nejedná	jednat	k5eNaImIp3nS	jednat
<g/>
,	,	kIx,	,
věc	věc	k1gFnSc1	věc
odloží	odložit	k5eAaPmIp3nS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
-li	i	k?	-li
<g/>
,	,	kIx,	,
že	že	k8xS	že
událost	událost	k1gFnSc1	událost
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
přestupku	přestupek	k1gInSc2	přestupek
<g/>
,	,	kIx,	,
postoupí	postoupit	k5eAaPmIp3nS	postoupit
případ	případ	k1gInSc1	případ
nadřízenému	nadřízený	k1gMnSc3	nadřízený
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
příslušníka	příslušník	k1gMnSc4	příslušník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c6	o
druhu	druh	k1gInSc6	druh
a	a	k8xC	a
výši	výše	k1gFnSc6	výše
kázeňského	kázeňský	k2eAgInSc2d1	kázeňský
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inspekce	inspekce	k1gFnSc1	inspekce
ale	ale	k9	ale
nemá	mít	k5eNaImIp3nS	mít
jen	jen	k9	jen
represivní	represivní	k2eAgFnSc4d1	represivní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
také	také	k9	také
jako	jako	k9	jako
klasický	klasický	k2eAgInSc4d1	klasický
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
útvar	útvar	k1gInSc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpracovaných	zpracovaný	k2eAgInPc2d1	zpracovaný
plánů	plán	k1gInPc2	plán
–	–	k?	–
a	a	k8xC	a
operativně	operativně	k6eAd1	operativně
podle	podle	k7c2	podle
požadavků	požadavek	k1gInPc2	požadavek
a	a	k8xC	a
zadání	zadání	k1gNnSc6	zadání
ředitele	ředitel	k1gMnSc2	ředitel
Služby	služba	k1gFnSc2	služba
–	–	k?	–
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
řadě	řada	k1gFnSc3	řada
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
kontrola	kontrola	k1gFnSc1	kontrola
nasazování	nasazování	k1gNnSc1	nasazování
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
správného	správný	k2eAgNnSc2d1	správné
vedení	vedení	k1gNnSc2	vedení
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
čerpání	čerpání	k1gNnSc1	čerpání
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
služebních	služební	k2eAgNnPc2d1	služební
aut	auto	k1gNnPc2	auto
apod.	apod.	kA	apod.
Nezanedbatelná	zanedbatelný	k2eNgFnSc1d1	nezanedbatelná
je	být	k5eAaImIp3nS	být
i	i	k9	i
její	její	k3xOp3gFnSc1	její
právní	právní	k2eAgFnSc1d1	právní
pomoc	pomoc	k1gFnSc1	pomoc
<g/>
,	,	kIx,	,
poskytovaná	poskytovaný	k2eAgFnSc1d1	poskytovaná
příslušníkům	příslušník	k1gMnPc3	příslušník
BIS	BIS	kA	BIS
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
ocitnou	ocitnout	k5eAaPmIp3nP	ocitnout
ve	v	k7c6	v
složitých	složitý	k2eAgFnPc6d1	složitá
životních	životní	k2eAgFnPc6d1	životní
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Odbor	odbor	k1gInSc1	odbor
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
====	====	k?	====
</s>
</p>
<p>
<s>
S	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
dávkou	dávka	k1gFnSc7	dávka
zjednodušení	zjednodušení	k1gNnSc1	zjednodušení
lze	lze	k6eAd1	lze
Odbor	odbor	k1gInSc1	odbor
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
označit	označit	k5eAaPmF	označit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
útvar	útvar	k1gInSc1	útvar
obrany	obrana	k1gFnSc2	obrana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
hlavním	hlavní	k2eAgNnSc7d1	hlavní
posláním	poslání	k1gNnSc7	poslání
je	být	k5eAaImIp3nS	být
bránit	bránit	k5eAaImF	bránit
Bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
informační	informační	k2eAgFnSc4d1	informační
službu	služba	k1gFnSc4	služba
před	před	k7c7	před
aktivitami	aktivita	k1gFnPc7	aktivita
a	a	k8xC	a
jevy	jev	k1gInPc7	jev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
nebo	nebo	k8xC	nebo
narušit	narušit	k5eAaPmF	narušit
její	její	k3xOp3gFnSc4	její
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
úkoly	úkol	k1gInPc4	úkol
např.	např.	kA	např.
ochrana	ochrana	k1gFnSc1	ochrana
BIS	BIS	kA	BIS
proti	proti	k7c3	proti
případné	případný	k2eAgFnSc3d1	případná
infiltraci	infiltrace	k1gFnSc3	infiltrace
cizích	cizí	k2eAgFnPc2d1	cizí
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odbor	odbor	k1gInSc1	odbor
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
se	se	k3xPyFc4	se
také	také	k9	také
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
odhalování	odhalování	k1gNnSc4	odhalování
postojů	postoj	k1gInPc2	postoj
a	a	k8xC	a
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
odporují	odporovat	k5eAaImIp3nP	odporovat
interním	interní	k2eAgInPc3d1	interní
předpisům	předpis	k1gInPc3	předpis
Služby	služba	k1gFnSc2	služba
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
neslučitelné	slučitelný	k2eNgNnSc4d1	neslučitelné
s	s	k7c7	s
postavením	postavení	k1gNnSc7	postavení
zpravodajského	zpravodajský	k2eAgMnSc2d1	zpravodajský
důstojníka	důstojník	k1gMnSc2	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
působnosti	působnost	k1gFnSc2	působnost
patří	patřit	k5eAaImIp3nS	patřit
objasňování	objasňování	k1gNnSc1	objasňování
případných	případný	k2eAgInPc2d1	případný
úniků	únik	k1gInPc2	únik
utajovaných	utajovaný	k2eAgFnPc2d1	utajovaná
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
prověrky	prověrka	k1gFnSc2	prověrka
uchazečů	uchazeč	k1gMnPc2	uchazeč
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
do	do	k7c2	do
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
Zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
utajovaných	utajovaný	k2eAgFnPc2d1	utajovaná
informací	informace	k1gFnPc2	informace
prověřuje	prověřovat	k5eAaImIp3nS	prověřovat
i	i	k9	i
příslušníky	příslušník	k1gMnPc4	příslušník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
Službě	služba	k1gFnSc6	služba
už	už	k6eAd1	už
pracují	pracovat	k5eAaImIp3nP	pracovat
a	a	k8xC	a
metodicky	metodicky	k6eAd1	metodicky
řídí	řídit	k5eAaImIp3nS	řídit
operativní	operativní	k2eAgFnSc4d1	operativní
ochranu	ochrana	k1gFnSc4	ochrana
celé	celá	k1gFnSc2	celá
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
se	se	k3xPyFc4	se
existence	existence	k1gFnSc1	existence
a	a	k8xC	a
fungování	fungování	k1gNnSc1	fungování
odboru	odbor	k1gInSc2	odbor
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
utajovaných	utajovaný	k2eAgFnPc2d1	utajovaná
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ukládá	ukládat	k5eAaImIp3nS	ukládat
Bezpečnostní	bezpečnostní	k2eAgFnSc3d1	bezpečnostní
informační	informační	k2eAgFnSc3d1	informační
službě	služba	k1gFnSc3	služba
plnit	plnit	k5eAaImF	plnit
úkoly	úkol	k1gInPc4	úkol
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
personální	personální	k2eAgFnSc6d1	personální
<g/>
,	,	kIx,	,
administrativní	administrativní	k2eAgFnSc3d1	administrativní
a	a	k8xC	a
fyzické	fyzický	k2eAgFnSc3d1	fyzická
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
informačních	informační	k2eAgInPc2d1	informační
a	a	k8xC	a
komunikačních	komunikační	k2eAgInPc2d1	komunikační
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
kryptografické	kryptografický	k2eAgFnSc2d1	kryptografická
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Organizačně	organizačně	k6eAd1	organizačně
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
odbor	odbor	k1gInSc1	odbor
zařazen	zařadit	k5eAaPmNgInS	zařadit
mezi	mezi	k7c4	mezi
útvary	útvar	k1gInPc4	útvar
podřízené	podřízený	k2eAgInPc4d1	podřízený
bezpečnostnímu	bezpečnostní	k2eAgMnSc3d1	bezpečnostní
řediteli	ředitel	k1gMnSc3	ředitel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
status	status	k1gInSc4	status
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
náměstka	náměstek	k1gMnSc4	náměstek
ředitele	ředitel	k1gMnSc2	ředitel
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Skupina	skupina	k1gFnSc1	skupina
interního	interní	k2eAgInSc2d1	interní
auditu	audit	k1gInSc2	audit
====	====	k?	====
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
interního	interní	k2eAgInSc2d1	interní
auditu	audit	k1gInSc2	audit
je	být	k5eAaImIp3nS	být
funkčně	funkčně	k6eAd1	funkčně
nezávislý	závislý	k2eNgInSc1d1	nezávislý
a	a	k8xC	a
</s>
</p>
<p>
<s>
organizačně	organizačně	k6eAd1	organizačně
oddělený	oddělený	k2eAgInSc4d1	oddělený
útvar	útvar	k1gInSc4	útvar
přímo	přímo	k6eAd1	přímo
podřízený	podřízený	k2eAgMnSc1d1	podřízený
řediteli	ředitel	k1gMnSc3	ředitel
Služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
působnosti	působnost	k1gFnSc2	působnost
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
v	v	k7c6	v
organizačním	organizační	k2eAgInSc6d1	organizační
řádu	řád	k1gInSc6	řád
BIS	BIS	kA	BIS
a	a	k8xC	a
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
předpisem	předpis	k1gInSc7	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgFnSc1d1	právní
úprava	úprava	k1gFnSc1	úprava
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
320	[number]	k4	320
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
finanční	finanční	k2eAgFnSc6d1	finanční
kontrole	kontrola	k1gFnSc6	kontrola
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
správě	správa	k1gFnSc6	správa
a	a	k8xC	a
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
některých	některý	k3yIgInPc2	některý
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
prováděcí	prováděcí	k2eAgFnSc2d1	prováděcí
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
416	[number]	k4	416
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Některé	některý	k3yIgFnPc4	některý
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
finanční	finanční	k2eAgFnPc4d1	finanční
kontroly	kontrola	k1gFnPc4	kontrola
provádějí	provádět	k5eAaImIp3nP	provádět
též	též	k9	též
odborné	odborný	k2eAgInPc4d1	odborný
útvary	útvar	k1gInPc4	útvar
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnPc4	terminologie
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
==	==	k?	==
</s>
</p>
<p>
<s>
příslušník	příslušník	k1gMnSc1	příslušník
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
je	být	k5eAaImIp3nS	být
jakýkoliv	jakýkoliv	k3yIgMnSc1	jakýkoliv
pracovník	pracovník	k1gMnSc1	pracovník
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
ve	v	k7c6	v
služebním	služební	k2eAgInSc6d1	služební
poměru	poměr	k1gInSc6	poměr
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
operativní	operativní	k2eAgMnSc1d1	operativní
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
také	také	k9	také
"	"	kIx"	"
<g/>
operativec	operativec	k1gMnSc1	operativec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
příslušník	příslušník	k1gMnSc1	příslušník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sbírá	sbírat	k5eAaImIp3nS	sbírat
zpravodajské	zpravodajský	k2eAgFnPc4d1	zpravodajská
informace	informace	k1gFnPc4	informace
nebo	nebo	k8xC	nebo
řídí	řídit	k5eAaImIp3nS	řídit
síť	síť	k1gFnSc4	síť
agentů	agens	k1gInPc2	agens
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
osoba	osoba	k1gFnSc1	osoba
jednající	jednající	k2eAgFnSc1d1	jednající
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
BIS	BIS	kA	BIS
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
příslušník	příslušník	k1gMnSc1	příslušník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
osobou	osoba	k1gFnSc7	osoba
jednající	jednající	k2eAgNnSc4d1	jednající
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
BIS	BIS	kA	BIS
<g/>
,	,	kIx,	,
např.	např.	kA	např.
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
nebo	nebo	k8xC	nebo
ideových	ideový	k2eAgInPc2d1	ideový
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
nepřesně	přesně	k6eNd1	přesně
užívá	užívat	k5eAaImIp3nS	užívat
terminologicky	terminologicky	k6eAd1	terminologicky
neexistující	existující	k2eNgInSc4d1	neexistující
název	název	k1gInSc4	název
agent	agent	k1gMnSc1	agent
<g/>
.	.	kIx.	.
<g/>
Zejména	zejména	k9	zejména
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
prostředí	prostředí	k1gNnSc6	prostředí
termín	termín	k1gInSc1	termín
agent	agent	k1gMnSc1	agent
označuje	označovat	k5eAaImIp3nS	označovat
příslušníka	příslušník	k1gMnSc4	příslušník
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
prostředí	prostředí	k1gNnSc6	prostředí
tomu	ten	k3xDgMnSc3	ten
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zaměstnání	zaměstnání	k1gNnPc4	zaměstnání
u	u	k7c2	u
BIS	BIS	kA	BIS
==	==	k?	==
</s>
</p>
<p>
<s>
Neobvyklá	obvyklý	k2eNgFnSc1d1	neobvyklá
povaha	povaha	k1gFnSc1	povaha
práce	práce	k1gFnSc2	práce
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
klade	klást	k5eAaImIp3nS	klást
na	na	k7c4	na
její	její	k3xOp3gMnPc4	její
příslušníky	příslušník	k1gMnPc4	příslušník
výjimečné	výjimečný	k2eAgInPc1d1	výjimečný
nároky	nárok	k1gInPc1	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
zájemce	zájemce	k1gMnSc1	zájemce
musí	muset	k5eAaImIp3nS	muset
dobrovolně	dobrovolně	k6eAd1	dobrovolně
projít	projít	k5eAaPmF	projít
náročnými	náročný	k2eAgFnPc7d1	náročná
prověrkami	prověrka	k1gFnPc7	prověrka
zaměřenými	zaměřený	k2eAgFnPc7d1	zaměřená
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
<g/>
,	,	kIx,	,
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnPc4d1	osobní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
modely	model	k1gInPc4	model
chování	chování	k1gNnSc2	chování
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
přijímacího	přijímací	k2eAgInSc2d1	přijímací
procesu	proces	k1gInSc2	proces
jsou	být	k5eAaImIp3nP	být
psychologické	psychologický	k2eAgInPc1d1	psychologický
i	i	k8xC	i
fyzické	fyzický	k2eAgInPc4d1	fyzický
testy	test	k1gInPc4	test
a	a	k8xC	a
ověření	ověření	k1gNnSc4	ověření
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
<g/>
,	,	kIx,	,
osobnostní	osobnostní	k2eAgFnSc2d1	osobnostní
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
způsobilosti	způsobilost	k1gFnSc2	způsobilost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Personální	personální	k2eAgInPc4d1	personální
požadavky	požadavek	k1gInPc4	požadavek
===	===	k?	===
</s>
</p>
<p>
<s>
občan	občan	k1gMnSc1	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
starší	starý	k2eAgInPc1d2	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
bezúhonnost	bezúhonnost	k1gFnSc1	bezúhonnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
negativní	negativní	k2eAgNnSc4d1	negativní
lustrační	lustrační	k2eAgNnSc4d1	lustrační
osvědčení	osvědčení	k1gNnSc4	osvědčení
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
fyzické	fyzický	k2eAgNnSc4d1	fyzické
a	a	k8xC	a
duševní	duševní	k2eAgNnSc4d1	duševní
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kvalifikační	kvalifikační	k2eAgInPc1d1	kvalifikační
předpoklady	předpoklad	k1gInPc1	předpoklad
–	–	k?	–
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
povolání	povolání	k1gNnSc2	povolání
zpravodajského	zpravodajský	k2eAgMnSc2d1	zpravodajský
důstojníka	důstojník	k1gMnSc2	důstojník
je	být	k5eAaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
dokončené	dokončený	k2eAgNnSc1d1	dokončené
vysokoškolské	vysokoškolský	k2eAgNnSc1d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
znalost	znalost	k1gFnSc1	znalost
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
<g/>
Základní	základní	k2eAgInPc4d1	základní
předpoklady	předpoklad	k1gInPc4	předpoklad
přesně	přesně	k6eAd1	přesně
formuluje	formulovat	k5eAaImIp3nS	formulovat
§	§	k?	§
13	[number]	k4	13
zákona	zákon	k1gInSc2	zákon
361	[number]	k4	361
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
služebním	služební	k2eAgInSc6d1	služební
poměru	poměr	k1gInSc6	poměr
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uchazeč	uchazeč	k1gMnSc1	uchazeč
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
práce	práce	k1gFnSc1	práce
v	v	k7c6	v
BIS	BIS	kA	BIS
je	být	k5eAaImIp3nS	být
službou	služba	k1gFnSc7	služba
demokracii	demokracie	k1gFnSc3	demokracie
<g/>
,	,	kIx,	,
bezpečnosti	bezpečnost	k1gFnSc3	bezpečnost
a	a	k8xC	a
zájmům	zájem	k1gInPc3	zájem
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Úvodem	úvodem	k7c2	úvodem
svého	svůj	k3xOyFgNnSc2	svůj
rozhodování	rozhodování	k1gNnSc2	rozhodování
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
položit	položit	k5eAaPmF	položit
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
uznává	uznávat	k5eAaImIp3nS	uznávat
potřebu	potřeba	k1gFnSc4	potřeba
existence	existence	k1gFnSc2	existence
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
ve	v	k7c6	v
svobodné	svobodný	k2eAgFnSc6d1	svobodná
<g/>
,	,	kIx,	,
demokratické	demokratický	k2eAgFnSc3d1	demokratická
společnosti	společnost	k1gFnSc3	společnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
a	a	k8xC	a
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
pracovník	pracovník	k1gMnSc1	pracovník
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
vědomě	vědomě	k6eAd1	vědomě
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
dobrovolně	dobrovolně	k6eAd1	dobrovolně
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
svou	svůj	k3xOyFgFnSc7	svůj
příslušností	příslušnost	k1gFnSc7	příslušnost
k	k	k7c3	k
elitní	elitní	k2eAgFnSc3d1	elitní
organizaci	organizace	k1gFnSc3	organizace
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
typu	typ	k1gInSc2	typ
–	–	k?	–
jakou	jaký	k3yIgFnSc7	jaký
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
představuje	představovat	k5eAaImIp3nS	představovat
–	–	k?	–
je	on	k3xPp3gInPc4	on
částečně	částečně	k6eAd1	částečně
omezen	omezit	k5eAaPmNgMnS	omezit
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
občanských	občanský	k2eAgNnPc6d1	občanské
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
svobodách	svoboda	k1gFnPc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Nebude	být	k5eNaImBp3nS	být
např.	např.	kA	např.
možné	možný	k2eAgFnPc1d1	možná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
své	svůj	k3xOyFgInPc4	svůj
politické	politický	k2eAgInPc4d1	politický
postoje	postoj	k1gInPc4	postoj
<g/>
,	,	kIx,	,
názory	názor	k1gInPc4	názor
a	a	k8xC	a
pohledy	pohled	k1gInPc4	pohled
na	na	k7c4	na
celospolečenské	celospolečenský	k2eAgInPc4d1	celospolečenský
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
poutal	poutat	k5eAaImAgMnS	poutat
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Potlačit	potlačit	k5eAaPmF	potlačit
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
veřejném	veřejný	k2eAgNnSc6d1	veřejné
uznání	uznání	k1gNnSc6	uznání
či	či	k8xC	či
satisfakci	satisfakce	k1gFnSc4	satisfakce
a	a	k8xC	a
podřídit	podřídit	k5eAaPmF	podřídit
ji	on	k3xPp3gFnSc4	on
důvěrnosti	důvěrnost	k1gFnSc2	důvěrnost
a	a	k8xC	a
mlčenlivosti	mlčenlivost	k1gFnSc2	mlčenlivost
<g/>
.	.	kIx.	.
</s>
<s>
Přijmout	přijmout	k5eAaPmF	přijmout
musí	muset	k5eAaImIp3nS	muset
i	i	k9	i
nepsanou	psaný	k2eNgFnSc4d1	psaný
zásadu	zásada	k1gFnSc4	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
Služba	služba	k1gFnSc1	služba
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
právo	právo	k1gNnSc4	právo
přiměřeně	přiměřeně	k6eAd1	přiměřeně
nahlížet	nahlížet	k5eAaImF	nahlížet
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
soukromého	soukromý	k2eAgInSc2d1	soukromý
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etickým	etický	k2eAgInSc7d1	etický
imperativem	imperativ	k1gInSc7	imperativ
a	a	k8xC	a
věcí	věc	k1gFnSc7	věc
cti	čest	k1gFnSc2	čest
zpravodajského	zpravodajský	k2eAgMnSc2d1	zpravodajský
důstojníka	důstojník	k1gMnSc2	důstojník
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zjišťování	zjišťování	k1gNnSc4	zjišťování
skutečného	skutečný	k2eAgInSc2d1	skutečný
<g/>
,	,	kIx,	,
objektivního	objektivní	k2eAgInSc2d1	objektivní
stavu	stav	k1gInSc2	stav
věcí	věc	k1gFnPc2	věc
navzdory	navzdory	k7c3	navzdory
osobním	osobní	k2eAgInPc3d1	osobní
zájmům	zájem	k1gInPc3	zájem
a	a	k8xC	a
pohodlí	pohodlí	k1gNnSc3	pohodlí
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
vědom	vědom	k2eAgInSc4d1	vědom
potenciální	potenciální	k2eAgInSc4d1	potenciální
závažnosti	závažnost	k1gFnSc2	závažnost
své	svůj	k3xOyFgFnSc2	svůj
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
produkovaných	produkovaný	k2eAgFnPc2d1	produkovaná
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Nedbalost	nedbalost	k1gFnSc1	nedbalost
v	v	k7c6	v
úsudku	úsudek	k1gInSc6	úsudek
či	či	k8xC	či
zaujatost	zaujatost	k1gFnSc1	zaujatost
může	moct	k5eAaImIp3nS	moct
vážně	vážně	k6eAd1	vážně
poškodit	poškodit	k5eAaPmF	poškodit
toho	ten	k3xDgMnSc4	ten
či	či	k8xC	či
onoho	onen	k3xDgMnSc2	onen
člověka	člověk	k1gMnSc2	člověk
i	i	k8xC	i
chráněné	chráněný	k2eAgInPc4d1	chráněný
zájmy	zájem	k1gInPc4	zájem
Služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajský	zpravodajský	k2eAgMnSc1d1	zpravodajský
důstojník	důstojník	k1gMnSc1	důstojník
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
chránit	chránit	k5eAaImF	chránit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
identitu	identita	k1gFnSc4	identita
<g/>
,	,	kIx,	,
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
pověst	pověst	k1gFnSc4	pověst
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
svých	svůj	k3xOyFgInPc2	svůj
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpravodajský	zpravodajský	k2eAgMnSc1d1	zpravodajský
důstojník	důstojník	k1gMnSc1	důstojník
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zvídavý	zvídavý	k2eAgMnSc1d1	zvídavý
<g/>
,	,	kIx,	,
emocionálně	emocionálně	k6eAd1	emocionálně
stabilní	stabilní	k2eAgMnSc1d1	stabilní
a	a	k8xC	a
trpělivý	trpělivý	k2eAgMnSc1d1	trpělivý
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
podřídit	podřídit	k5eAaPmF	podřídit
se	se	k3xPyFc4	se
autoritě	autorita	k1gFnSc3	autorita
a	a	k8xC	a
týmu	tým	k1gInSc3	tým
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
umět	umět	k5eAaImF	umět
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
<g/>
,	,	kIx,	,
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
vzbudit	vzbudit	k5eAaPmF	vzbudit
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
impulzivní	impulzivní	k2eAgMnSc1d1	impulzivní
<g/>
,	,	kIx,	,
agresivní	agresivní	k2eAgMnSc1d1	agresivní
<g/>
,	,	kIx,	,
nenávistný	nenávistný	k2eAgMnSc1d1	nenávistný
a	a	k8xC	a
výstřední	výstřední	k2eAgMnSc1d1	výstřední
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
kontroverze	kontroverze	k1gFnSc1	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
Senátor	senátor	k1gMnSc1	senátor
Jaromír	Jaromír	k1gMnSc1	Jaromír
Štětina	štětina	k1gFnSc1	štětina
obvinil	obvinit	k5eAaPmAgMnS	obvinit
BIS	BIS	kA	BIS
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
snažila	snažit	k5eAaImAgFnS	snažit
zastrašovat	zastrašovat	k5eAaImF	zastrašovat
a	a	k8xC	a
organizovala	organizovat	k5eAaBmAgFnS	organizovat
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
kampaně	kampaň	k1gFnPc4	kampaň
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
disponoval	disponovat	k5eAaBmAgMnS	disponovat
dokumenty	dokument	k1gInPc7	dokument
o	o	k7c6	o
protiprávní	protiprávní	k2eAgFnSc6d1	protiprávní
činnosti	činnost	k1gFnSc6	činnost
BIS	BIS	kA	BIS
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
bývalého	bývalý	k2eAgMnSc2d1	bývalý
agenta	agent	k1gMnSc2	agent
BIS	BIS	kA	BIS
a	a	k8xC	a
disidenta	disident	k1gMnSc2	disident
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Hučína	Hučín	k1gMnSc2	Hučín
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Štětiny	štětina	k1gFnSc2	štětina
BIS	BIS	kA	BIS
zneužívala	zneužívat	k5eAaImAgFnS	zneužívat
své	svůj	k3xOyFgMnPc4	svůj
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
a	a	k8xC	a
agenty	agent	k1gMnPc4	agent
k	k	k7c3	k
diskreditační	diskreditační	k2eAgFnSc3d1	diskreditační
kampani	kampaň	k1gFnSc3	kampaň
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
<g/>
Prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
práci	práce	k1gFnSc4	práce
BIS	BIS	kA	BIS
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInPc2	jeho
názoru	názor	k1gInSc2	názor
ignoruje	ignorovat	k5eAaImIp3nS	ignorovat
hrozbu	hrozba	k1gFnSc4	hrozba
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
včas	včas	k6eAd1	včas
neodhalila	odhalit	k5eNaPmAgFnS	odhalit
činnost	činnost	k1gFnSc1	činnost
pražského	pražský	k2eAgMnSc2d1	pražský
imáma	imám	k1gMnSc2	imám
Sámera	Sámer	k1gMnSc2	Sámer
Shehadeha	Shehadeh	k1gMnSc2	Shehadeh
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
terorismu	terorismus	k1gInSc2	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
čučkaři	čučkař	k1gMnPc1	čučkař
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
najít	najít	k5eAaPmF	najít
teroristu	terorista	k1gMnSc4	terorista
<g/>
,	,	kIx,	,
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
opravdu	opravdu	k6eAd1	opravdu
existoval	existovat	k5eAaImAgInS	existovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatně	ostatně	k6eAd1	ostatně
i	i	k9	i
Národní	národní	k2eAgFnSc1d1	národní
centrála	centrála	k1gFnSc1	centrála
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
jsou	být	k5eAaImIp3nP	být
teroristé	terorista	k1gMnPc1	terorista
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
ho	on	k3xPp3gNnSc4	on
nenašla	najít	k5eNaPmAgFnS	najít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
veřejné	veřejný	k2eAgFnSc6d1	veřejná
výroční	výroční	k2eAgFnSc6d1	výroční
zprávě	zpráva	k1gFnSc6	zpráva
BIS	BIS	kA	BIS
varovala	varovat	k5eAaImAgFnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vliv	vliv	k1gInSc1	vliv
sovětské	sovětský	k2eAgFnSc2d1	sovětská
verze	verze	k1gFnSc2	verze
moderních	moderní	k2eAgFnPc2d1	moderní
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
proruského	proruský	k2eAgNnSc2d1	proruské
panslovanství	panslovanství	k1gNnSc2	panslovanství
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
školách	škola	k1gFnPc6	škola
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
ruské	ruský	k2eAgFnSc2d1	ruská
hybridní	hybridní	k2eAgFnSc2d1	hybridní
strategie	strategie	k1gFnSc2	strategie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2019	[number]	k4	2019
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
výuce	výuka	k1gFnSc3	výuka
historie	historie	k1gFnSc2	historie
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
BIS	BIS	kA	BIS
Michalem	Michal	k1gMnSc7	Michal
Koudelkou	Koudelka	k1gMnSc7	Koudelka
sešel	sejít	k5eAaPmAgMnS	sejít
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
Robert	Robert	k1gMnSc1	Robert
Plaga	Plag	k1gMnSc2	Plag
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Zeman	Zeman	k1gMnSc1	Zeman
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagoval	reagovat	k5eAaBmAgMnS	reagovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
diskuse	diskuse	k1gFnSc2	diskuse
měla	mít	k5eAaImAgFnS	mít
vměšovat	vměšovat	k5eAaImF	vměšovat
i	i	k9	i
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
neměla	mít	k5eNaImAgFnS	mít
takovou	takový	k3xDgFnSc4	takový
drzost	drzost	k1gFnSc4	drzost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
radila	radit	k5eAaImAgFnS	radit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
učit	učit	k5eAaImF	učit
dějepis	dějepis	k1gInSc4	dějepis
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
služby	služba	k1gFnPc1	služba
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
ředitelé	ředitel	k1gMnPc1	ředitel
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
února	únor	k1gInSc2	únor
1990	[number]	k4	1990
II	II	kA	II
<g/>
.	.	kIx.	.
správa	správa	k1gFnSc1	správa
FMV	FMV	kA	FMV
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
ÚOÚD	ÚOÚD	kA	ÚOÚD
FMV	FMV	kA	FMV
(	(	kIx(	(
<g/>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
ústavy	ústava	k1gFnSc2	ústava
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Formánek	Formánek	k1gMnSc1	Formánek
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
–	–	k?	–
duben	duben	k1gInSc1	duben
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
–	–	k?	–
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Müller	Müller	k1gMnSc1	Müller
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
–	–	k?	–
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
–	–	k?	–
prosinec	prosinec	k1gInSc1	prosinec
<g/>
)	)	kIx)	)
<g/>
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
FIS	FIS	kA	FIS
FMV	FMV	kA	FMV
(	(	kIx(	(
<g/>
Federální	federální	k2eAgFnSc1d1	federální
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
federálního	federální	k2eAgNnSc2d1	federální
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Novotný	Novotný	k1gMnSc1	Novotný
</s>
</p>
<p>
<s>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
FBIS	FBIS	kA	FBIS
(	(	kIx(	(
<g/>
Federální	federální	k2eAgFnSc1d1	federální
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
244	[number]	k4	244
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1991	[number]	k4	1991
–	–	k?	–
prosinec	prosinec	k1gInSc1	prosinec
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Štefan	Štefan	k1gMnSc1	Štefan
Bačinský	Bačinský	k2eAgMnSc1d1	Bačinský
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
1992	[number]	k4	1992
–	–	k?	–
srpen	srpen	k1gInSc1	srpen
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavol	Pavol	k1gInSc1	Pavol
Slovák	Slovák	k1gMnSc1	Slovák
(	(	kIx(	(
<g/>
září	zářit	k5eAaImIp3nS	zářit
1992	[number]	k4	1992
–	–	k?	–
prosinec	prosinec	k1gInSc1	prosinec
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
BIS	BIS	kA	BIS
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
527	[number]	k4	527
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Devátý	devátý	k4xOgMnSc1	devátý
</s>
</p>
<p>
<s>
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
BIS	BIS	kA	BIS
(	(	kIx(	(
<g/>
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
154	[number]	k4	154
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Devátý	devátý	k4xOgMnSc1	devátý
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1994	[number]	k4	1994
–	–	k?	–
únor	únor	k1gInSc1	únor
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Vulterin	Vulterin	k1gInSc1	Vulterin
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
1997	[number]	k4	1997
–	–	k?	–
leden	leden	k1gInSc1	leden
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Růžek	Růžek	k1gMnSc1	Růžek
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1999	[number]	k4	1999
–	–	k?	–
květen	květen	k1gInSc1	květen
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Lang	Lang	k1gMnSc1	Lang
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
2003	[number]	k4	2003
–	–	k?	–
srpen	srpen	k1gInSc1	srpen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Koudelka	Koudelka	k1gMnSc1	Koudelka
(	(	kIx(	(
<g/>
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
BIS	BIS	kA	BIS
Michal	Michal	k1gMnSc1	Michal
Koudelka	Koudelka	k1gMnSc1	Koudelka
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2019	[number]	k4	2019
cenu	cena	k1gFnSc4	cena
CIA	CIA	kA	CIA
za	za	k7c4	za
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Koudelka	Koudelka	k1gMnSc1	Koudelka
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
CIA	CIA	kA	CIA
dává	dávat	k5eAaImIp3nS	dávat
<g/>
,	,	kIx,	,
beru	brát	k5eAaImIp1nS	brát
nejen	nejen	k6eAd1	nejen
jako	jako	k8xC	jako
ocenění	ocenění	k1gNnSc1	ocenění
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
jako	jako	k9	jako
ocenění	ocenění	k1gNnSc4	ocenění
práce	práce	k1gFnSc2	práce
BIS	BIS	kA	BIS
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Churáň	Churáň	k1gFnSc1	Churáň
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
špionáže	špionáž	k1gFnSc2	špionáž
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
přednášky	přednáška	k1gFnPc1	přednáška
RNDr.	RNDr.	kA	RNDr.
Petra	Petr	k1gMnSc4	Petr
Zemana	Zeman	k1gMnSc4	Zeman
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc4d1	bývalý
ředitele	ředitel	k1gMnSc4	ředitel
ÚZSI	ÚZSI	kA	ÚZSI
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Hodnosti	hodnost	k1gFnPc1	hodnost
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
sborů	sbor	k1gInPc2	sbor
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
styky	styk	k1gInPc4	styk
a	a	k8xC	a
informace	informace	k1gFnPc4	informace
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
