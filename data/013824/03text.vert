<s>
Jozef	Jozef	k1gMnSc1
Levický	levický	k2eAgMnSc1d1
</s>
<s>
Jozef	Jozef	k1gMnSc1
LevickýOsobní	LevickýOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1942	#num#	k4
(	(	kIx(
<g/>
78	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Zlaté	zlatý	k2eAgFnSc3d1
Moravce	Moravka	k1gFnSc3
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
útočník	útočník	k1gMnSc1
Profesionální	profesionální	k2eAgFnSc2d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
1963	#num#	k4
<g/>
–	–	k?
<g/>
19781978	#num#	k4
<g/>
–	–	k?
<g/>
19791979	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
Inter	Intra	k1gFnPc2
Bratislava	Bratislava	k1gFnSc1
Slovan	Slovan	k1gInSc1
VídeňCalex	VídeňCalex	k1gInSc1
Zlaté	zlatá	k1gFnSc2
Moravce	Moravec	k1gMnSc2
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1964	#num#	k4
<g/>
–	–	k?
<g/>
196519661967	#num#	k4
junioři	junior	k1gMnPc1
B-tým	B-tý	k1gMnSc7
A-tým	A-tý	k1gMnSc7
<g/>
2	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jozef	Jozef	k1gMnSc1
Levický	levický	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1942	#num#	k4
Zlaté	zlatý	k2eAgFnSc6d1
Moravce	Moravka	k1gFnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
slovenský	slovenský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
,	,	kIx,
útočník	útočník	k1gMnSc1
<g/>
,	,	kIx,
reprezentant	reprezentant	k1gMnSc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
československé	československý	k2eAgFnSc6d1
reprezentaci	reprezentace	k1gFnSc6
odehrál	odehrát	k5eAaPmAgMnS
roku	rok	k1gInSc2
1967	#num#	k4
jedno	jeden	k4xCgNnSc1
utkání	utkání	k1gNnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
x	x	k?
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
reprezentačním	reprezentační	k2eAgNnSc6d1
B-mužstvu	B-mužstvo	k1gNnSc6
(	(	kIx(
<g/>
3	#num#	k4
góly	gól	k1gInPc4
<g/>
)	)	kIx)
a	a	k8xC
dvakrát	dvakrát	k6eAd1
v	v	k7c6
juniorských	juniorský	k2eAgInPc6d1
výběrech	výběr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrál	hrát	k5eAaImAgMnS
za	za	k7c4
Inter	Inter	k1gInSc4
Bratislava	Bratislava	k1gFnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
–	–	k?
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Slovan	Slovan	k1gMnSc1
Vídeň	Vídeň	k1gFnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Calex	Calex	k1gInSc1
Zlaté	zlatá	k1gFnSc2
Moravce	Moravec	k1gMnSc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
–	–	k?
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
odehrál	odehrát	k5eAaPmAgMnS
340	#num#	k4
utkání	utkání	k1gNnSc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
100	#num#	k4
branek	branka	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
členem	člen	k1gMnSc7
prestižního	prestižní	k2eAgInSc2d1
Klubu	klub	k1gInSc2
ligových	ligový	k2eAgMnPc2d1
kanonýrů	kanonýr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Malé	Malé	k2eAgFnSc6d1
encyklopedii	encyklopedie	k1gFnSc6
fotbalu	fotbal	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
ho	on	k3xPp3gInSc4
autoři	autor	k1gMnPc1
charakterizovali	charakterizovat	k5eAaBmAgMnP
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Nenápadný	nápadný	k2eNgMnSc1d1
útočník	útočník	k1gMnSc1
dlouhodobě	dlouhodobě	k6eAd1
nadprůměrné	nadprůměrný	k2eAgFnSc2d1
výkonnosti	výkonnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
jeden	jeden	k4xCgInSc1
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
ligových	ligový	k2eAgInPc2d1
gólů	gól	k1gInPc2
nevstřelil	vstřelit	k5eNaPmAgInS
z	z	k7c2
pokutového	pokutový	k2eAgInSc2d1
kopu	kop	k1gInSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
V	v	k7c6
Poháru	pohár	k1gInSc6
UEFA	UEFA	kA
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
7	#num#	k4
utkáních	utkání	k1gNnPc6
a	a	k8xC
dal	dát	k5eAaPmAgMnS
3	#num#	k4
góly	gól	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Slovnaft	Slovnaft	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Slovnaft	Slovnaft	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
-	-	kIx~
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Inter	Inter	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
CELKEM	celkem	k6eAd1
</s>
<s>
344	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JEŘÁBEK	Jeřábek	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
a	a	k8xC
československý	československý	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
:	:	kIx,
Lexikon	lexikon	k1gNnSc1
osobností	osobnost	k1gFnPc2
a	a	k8xC
klubů	klub	k1gInPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Grada	Grada	k1gFnSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
1656	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Miloslav	Miloslav	k1gMnSc1
Jenšík	Jenšík	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Atlas	Atlas	k1gInSc1
českého	český	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
–	–	k?
Radovan	Radovan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
2006	#num#	k4
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
−	−	k?
Libri	Libri	k1gNnSc1
1997	#num#	k4
</s>
<s>
VANĚK	Vaněk	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
fotbalu	fotbal	k1gInSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Olympia	Olympia	k1gFnSc1
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Statistiky	statistika	k1gFnPc1
ČMFS	ČMFS	kA
</s>
<s>
70	#num#	k4
<g/>
-ročný	-ročný	k2eAgInSc1d1
Jozef	Jozef	k1gInSc1
Levický	levický	k2eAgInSc1d1
</s>
<s>
Worldfootball	Worldfootball	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
</s>
<s>
National	Nationat	k5eAaPmAgInS,k5eAaImAgInS
Football	Football	k1gMnSc1
Teams	Teamsa	k1gFnPc2
</s>
<s>
EUFootball	EUFootball	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
CS	CS	kA
Fotbal	fotbal	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Klub	klub	k1gInSc1
ligových	ligový	k2eAgInPc2d1
kanonýrů	kanonýr	k1gMnPc2
</s>
<s>
Josef	Josef	k1gMnSc1
Bican	Bican	k1gMnSc1
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Lafata	Lafat	k1gMnSc2
•	•	k?
Antonín	Antonín	k1gMnSc1
Hájek	Hájek	k1gMnSc1
•	•	k?
Oldřich	Oldřich	k1gMnSc1
Nejedlý	Nejedlý	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Koller	Koller	k1gMnSc1
•	•	k?
Horst	Horst	k1gMnSc1
Siegl	Siegl	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Kloz	Kloz	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Wiecek	Wiecka	k1gFnPc2
•	•	k?
Jozef	Jozef	k1gMnSc1
Adamec	Adamec	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Kubala	Kubala	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Pavlovič	Pavlovič	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Kareš	Kareš	k1gMnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Daněk	Daněk	k1gMnSc1
•	•	k?
Vojtěch	Vojtěch	k1gMnSc1
Bradáč	Bradáč	k1gMnSc1
•	•	k?
Radek	Radek	k1gMnSc1
Drulák	Drulák	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Kuka	Kuka	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Jiří	Jiří	k1gMnSc1
Pešek	Pešek	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Rýgr	Rýgr	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Nehoda	nehoda	k1gFnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Hönig	Hönig	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Panenka	panenka	k1gFnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Humpál	Humpál	k1gMnSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Griga	Grig	k1gMnSc2
•	•	k?
Milan	Milan	k1gMnSc1
Luhový	luhový	k2eAgMnSc1d1
•	•	k?
Vratislav	Vratislav	k1gMnSc1
Lokvenc	Lokvenc	k1gFnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
•	•	k?
Ota	Ota	k1gMnSc1
Hemele	Hemel	k1gInSc2
•	•	k?
Jiří	Jiří	k1gMnSc1
Křižák	křižák	k1gMnSc1
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Perk	Perk	k?
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Skuhravý	Skuhravý	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Ludl	Ludl	k1gMnSc1
•	•	k?
Adolf	Adolf	k1gMnSc1
Scherer	Scherer	k1gMnSc1
•	•	k?
Emil	Emil	k1gMnSc1
Pažický	Pažický	k2eAgMnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Antonín	Antonín	k1gMnSc1
Puč	puč	k1gInSc1
•	•	k?
René	René	k1gMnSc1
Wagner	Wagner	k1gMnSc1
•	•	k?
Václav	Václav	k1gMnSc1
Mašek	Mašek	k1gMnSc1
•	•	k?
Raymond	Raymond	k1gMnSc1
Braine	Brain	k1gInSc5
•	•	k?
Jozef	Jozef	k1gMnSc1
Luknár	Luknár	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Kroupa	Kroupa	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Kadraba	Kadraba	k1gMnSc1
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Cejp	Cejp	k1gMnSc1
•	•	k?
Ján	Ján	k1gMnSc1
Strausz	Strausz	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Vízek	Vízek	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Melka	Melka	k1gMnSc1
•	•	k?
Viktor	Viktor	k1gMnSc1
Tegelhoff	Tegelhoff	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Hašek	Hašek	k1gMnSc1
•	•	k?
Libor	Libor	k1gMnSc1
Došek	Došek	k1gMnSc1
•	•	k?
Josef	Josef	k1gMnSc1
Silný	silný	k2eAgMnSc1d1
•	•	k?
Jan	Jan	k1gMnSc1
Říha	Říha	k1gMnSc1
•	•	k?
Anton	Anton	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Moravčík	Moravčík	k1gMnSc1
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Józsa	Józs	k1gMnSc2
•	•	k?
Pavel	Pavel	k1gMnSc1
Černý	Černý	k1gMnSc1
•	•	k?
Ľubomír	Ľubomír	k1gMnSc1
Luhový	luhový	k2eAgMnSc1d1
•	•	k?
Jozef	Jozef	k1gMnSc1
Obert	Oberta	k1gFnPc2
•	•	k?
Peter	Peter	k1gMnSc1
Herda	Herda	k1gMnSc1
•	•	k?
Přemysl	Přemysl	k1gMnSc1
Bičovský	Bičovský	k1gMnSc1
•	•	k?
Pavol	Pavol	k1gInSc1
Diňa	Diňa	k1gMnSc1
•	•	k?
Vlastimil	Vlastimil	k1gMnSc1
Preis	Preis	k1gFnSc2
•	•	k?
Petr	Petr	k1gMnSc1
Janečka	Janečka	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Zmatlík	Zmatlík	k1gInSc1
•	•	k?
Tibor	Tibor	k1gMnSc1
Mičinec	Mičinec	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Seidl	Seidl	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Bradáč	Bradáč	k1gMnSc1
•	•	k?
Verner	Verner	k1gMnSc1
Lička	lička	k1gFnSc1
•	•	k?
Marián	Marián	k1gMnSc1
Masný	masný	k2eAgMnSc1d1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Vlček	Vlček	k1gMnSc1
•	•	k?
Gejza	Gejza	k1gFnSc1
Šimanský	Šimanský	k2eAgMnSc1d1
•	•	k?
Milan	Milan	k1gMnSc1
Škoda	Škoda	k1gMnSc1
•	•	k?
František	František	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
•	•	k?
Ján	Ján	k1gMnSc1
Čapkovič	Čapkovič	k1gMnSc1
•	•	k?
Jozef	Jozef	k1gMnSc1
Levický	levický	k2eAgMnSc1d1
•	•	k?
Ferdinand	Ferdinand	k1gMnSc1
Plánický	Plánický	k2eAgMnSc1d1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Štrunc	Štrunc	k1gMnSc1
•	•	k?
Jan	Jan	k1gMnSc1
Nezmar	nezmar	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Baroš	Baroš	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Štajner	Štajner	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
