<s>
Muži	muž	k1gMnPc1	muž
mají	mít	k5eAaImIp3nP	mít
vlasy	vlas	k1gInPc4	vlas
nejsilnější	silný	k2eAgInPc4d3	nejsilnější
v	v	k7c6	v
týlu	týl	k1gInSc6	týl
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ženské	ženský	k2eAgInPc4d1	ženský
vlasy	vlas	k1gInPc4	vlas
jsou	být	k5eAaImIp3nP	být
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
mužské	mužský	k2eAgNnSc1d1	mužské
<g/>
.	.	kIx.	.
</s>
