<s>
Princip	princip	k1gInSc1	princip
identity	identita	k1gFnSc2	identita
nerozlišeného	rozlišený	k2eNgInSc2d1	nerozlišený
(	(	kIx(	(
<g/>
identitas	identitasit	k5eAaPmRp2nS	identitasit
indiscernibilium	indiscernibilium	k1gNnSc4	indiscernibilium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Leibnizův	Leibnizův	k2eAgInSc1d1	Leibnizův
princip	princip	k1gInSc1	princip
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
metafyzické	metafyzický	k2eAgInPc1d1	metafyzický
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
esenciální	esenciální	k2eAgInPc1d1	esenciální
<g/>
.	.	kIx.	.
</s>
