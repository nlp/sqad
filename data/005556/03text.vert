<s>
Kofi	Kofi	k6eAd1	Kofi
Atta	Atta	k1gMnSc1	Atta
Annan	Annan	k1gMnSc1	Annan
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
Kumasi	Kumas	k1gMnPc1	Kumas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ghanský	ghanský	k2eAgMnSc1d1	ghanský
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
sedmým	sedmý	k4xOgMnSc7	sedmý
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
Radou	rada	k1gFnSc7	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
potvrzen	potvrdit	k5eAaPmNgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
začalo	začít	k5eAaPmAgNnS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
začalo	začít	k5eAaPmAgNnS	začít
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
OSN	OSN	kA	OSN
a	a	k8xC	a
Kofi	Kofe	k1gFnSc4	Kofe
Annan	Annana	k1gFnPc2	Annana
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
za	za	k7c4	za
"	"	kIx"	"
<g/>
práci	práce	k1gFnSc4	práce
za	za	k7c4	za
lépe	dobře	k6eAd2	dobře
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
a	a	k8xC	a
mírovější	mírový	k2eAgInSc4d2	mírovější
svět	svět	k1gInSc4	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Sacharovovu	Sacharovův	k2eAgFnSc4d1	Sacharovova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
Annan	Annan	k1gMnSc1	Annan
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
americkou	americký	k2eAgFnSc4d1	americká
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
nelegální	legální	k2eNgInSc4d1	nelegální
a	a	k8xC	a
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
Chartou	charta	k1gFnSc7	charta
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
byl	být	k5eAaImAgMnS	být
ostře	ostro	k6eAd1	ostro
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
nedostatečný	dostatečný	k2eNgInSc4d1	nedostatečný
dohled	dohled	k1gInSc4	dohled
a	a	k8xC	a
pomalou	pomalý	k2eAgFnSc4d1	pomalá
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
skandál	skandál	k1gInSc4	skandál
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
korupce	korupce	k1gFnSc2	korupce
u	u	k7c2	u
OSN	OSN	kA	OSN
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
programu	program	k1gInSc6	program
"	"	kIx"	"
<g/>
Ropa	ropa	k1gFnSc1	ropa
za	za	k7c4	za
potraviny	potravina	k1gFnPc4	potravina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
započatého	započatý	k2eAgInSc2d1	započatý
za	za	k7c4	za
jeho	jeho	k3xOp3gMnSc4	jeho
předchůdce	předchůdce	k1gMnSc4	předchůdce
Butruse	Butruse	k1gFnSc2	Butruse
Butruse-Ghálího	Butruse-Ghálí	k2eAgInSc2d1	Butruse-Ghálí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
korupce	korupce	k1gFnSc2	korupce
byl	být	k5eAaImAgMnS	být
údajně	údajně	k6eAd1	údajně
zapojen	zapojit	k5eAaPmNgMnS	zapojit
i	i	k8xC	i
Annanův	Annanův	k2eAgMnSc1d1	Annanův
syn	syn	k1gMnSc1	syn
Kojo	Kojo	k1gMnSc1	Kojo
Annan	Annan	k1gMnSc1	Annan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
pověřil	pověřit	k5eAaPmAgMnS	pověřit
Paula	Paul	k1gMnSc4	Paul
Volckera	Volckero	k1gNnSc2	Volckero
předsednictvím	předsednictví	k1gNnSc7	předsednictví
komise	komise	k1gFnSc2	komise
"	"	kIx"	"
<g/>
Independent	independent	k1gMnSc1	independent
Inquiry	Inquira	k1gFnSc2	Inquira
into	into	k1gMnSc1	into
the	the	k?	the
United	United	k1gInSc1	United
Nations	Nations	k1gInSc1	Nations
Oil-for-Food	Oilor-Food	k1gInSc4	Oil-for-Food
Programme	Programme	k1gFnSc2	Programme
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tuto	tento	k3xDgFnSc4	tento
korupci	korupce	k1gFnSc4	korupce
začala	začít	k5eAaPmAgFnS	začít
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
vydala	vydat	k5eAaPmAgFnS	vydat
několik	několik	k4yIc4	několik
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
nejnovější	nový	k2eAgMnSc1d3	Nejnovější
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
při	při	k7c6	při
válce	válka	k1gFnSc6	válka
mezi	mezi	k7c7	mezi
Libanonem	Libanon	k1gInSc7	Libanon
a	a	k8xC	a
Izraelem	Izrael	k1gInSc7	Izrael
se	se	k3xPyFc4	se
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c4	na
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
<g/>
.	.	kIx.	.
</s>
<s>
Izraelské	izraelský	k2eAgMnPc4d1	izraelský
vojáky	voják	k1gMnPc4	voják
na	na	k7c6	na
území	území	k1gNnSc6	území
Libanonu	Libanon	k1gInSc2	Libanon
nahradily	nahradit	k5eAaPmAgFnP	nahradit
jednotky	jednotka	k1gFnPc1	jednotka
mírové	mírový	k2eAgFnSc2d1	mírová
mise	mise	k1gFnSc2	mise
OSN	OSN	kA	OSN
–	–	k?	–
UNIFIL	UNIFIL	kA	UNIFIL
<g/>
.	.	kIx.	.
</s>
<s>
Kofi	Kofi	k1gNnSc1	Kofi
Annan	Annany	k1gInPc2	Annany
se	se	k3xPyFc4	se
přátelí	přátelit	k5eAaImIp3nS	přátelit
s	s	k7c7	s
Madeleine	Madeleine	k1gFnSc7	Madeleine
Albrightovou	Albrightová	k1gFnSc7	Albrightová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
mezi	mezi	k7c7	mezi
keňskou	keňský	k2eAgFnSc7d1	keňská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
opozicí	opozice	k1gFnSc7	opozice
během	během	k7c2	během
povolební	povolební	k2eAgFnSc2d1	povolební
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
vlny	vlna	k1gFnSc2	vlna
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
padlo	padnout	k5eAaImAgNnS	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
asi	asi	k9	asi
700	[number]	k4	700
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
vyhnáno	vyhnat	k5eAaPmNgNnS	vyhnat
z	z	k7c2	z
domovů	domov	k1gInPc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgMnS	být
Annan	Annan	k1gMnSc1	Annan
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
mimořádným	mimořádný	k2eAgMnSc7d1	mimořádný
vyslancem	vyslanec	k1gMnSc7	vyslanec
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
pokusit	pokusit	k5eAaPmF	pokusit
vyjednat	vyjednat	k5eAaPmF	vyjednat
řešení	řešení	k1gNnSc4	řešení
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
velkého	velký	k2eAgNnSc2d1	velké
vnitropolitického	vnitropolitický	k2eAgNnSc2d1	vnitropolitické
a	a	k8xC	a
také	také	k9	také
náboženského	náboženský	k2eAgInSc2d1	náboženský
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
dopady	dopad	k1gInPc7	dopad
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
působí	působit	k5eAaImIp3nS	působit
jednotka	jednotka	k1gFnSc1	jednotka
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
asi	asi	k9	asi
300	[number]	k4	300
vojáků	voják	k1gMnPc2	voják
jako	jako	k8xC	jako
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
účinná	účinný	k2eAgFnSc1d1	účinná
realizace	realizace	k1gFnSc1	realizace
ostatních	ostatní	k2eAgInPc2d1	ostatní
elementů	element	k1gInPc2	element
tzv.	tzv.	kA	tzv.
Annanova	Annanův	k2eAgInSc2d1	Annanův
plánu	plán	k1gInSc2	plán
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
zemi	zem	k1gFnSc4	zem
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
v	v	k7c6	v
nedohlednu	nedohledno	k1gNnSc3	nedohledno
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
misi	mise	k1gFnSc4	mise
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
neúspěšně	úspěšně	k6eNd1	úspěšně
ukončil	ukončit	k5eAaPmAgInS	ukončit
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kofi	Kof	k1gFnSc2	Kof
Annan	Annany	k1gInPc2	Annany
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Kofi	Kofi	k1gNnSc2	Kofi
Annan	Annany	k1gInPc2	Annany
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Kofi	Kofi	k1gNnSc1	Kofi
Annan	Annana	k1gFnPc2	Annana
</s>
