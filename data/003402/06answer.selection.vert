<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
úprava	úprava	k1gFnSc1	úprava
některých	některý	k3yIgInPc2	některý
svalů	sval	k1gInPc2	sval
zadní	zadní	k2eAgFnSc2d1	zadní
končetiny	končetina	k1gFnSc2	končetina
(	(	kIx(	(
<g/>
m.	m.	k?	m.
ambiens	ambiens	k1gInSc1	ambiens
<g/>
,	,	kIx,	,
mm	mm	kA	mm
<g/>
.	.	kIx.	.
flexores	flexores	k1gMnSc1	flexores
perforantes	perforantesa	k1gFnPc2	perforantesa
et	et	k?	et
perforati	perforat	k1gMnPc1	perforat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
automatické	automatický	k2eAgNnSc4d1	automatické
sevření	sevření	k1gNnSc4	sevření
prstů	prst	k1gInPc2	prst
hřadujícího	hřadující	k2eAgMnSc2d1	hřadující
ptáka	pták	k1gMnSc2	pták
<g/>
.	.	kIx.	.
</s>
