<s>
Helena	Helena	k1gFnSc1
Kosková	Kosková	k1gFnSc1
</s>
<s>
Helena	Helena	k1gFnSc1
Kosková	Kosková	k1gFnSc1
Narození	narození	k1gNnPc4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
(	(	kIx(
<g/>
86	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
lektorka	lektorka	k1gFnSc1
<g/>
,	,	kIx,
kritička	kritička	k1gFnSc1
<g/>
,	,	kIx,
recenzentka	recenzentka	k1gFnSc1
a	a	k8xC
literární	literární	k2eAgFnSc1d1
kritička	kritička	k1gFnSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Otto	Otto	k1gMnSc1
Fischl	Fischl	k1gMnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Helena	Helena	k1gFnSc1
Kosková	Kosková	k1gFnSc1
(	(	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1935	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
literární	literární	k2eAgFnSc1d1
kritička	kritička	k1gFnSc1
a	a	k8xC
historička	historička	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
současně	současně	k6eAd1
též	též	k9
přednáší	přednášet	k5eAaImIp3nS
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Kosková	Kosková	k1gFnSc1
<g/>
,	,	kIx,
dívčím	dívčí	k2eAgNnSc7d1
příjmením	příjmení	k1gNnSc7
Fischlová	Fischlová	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
jako	jako	k9
mladší	mladý	k2eAgFnSc1d2
ze	z	k7c2
dvou	dva	k4xCgInPc2
dcer	dcera	k1gFnPc2
do	do	k7c2
smíšené	smíšený	k2eAgFnSc2d1
česko-židovské	česko-židovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
českého	český	k2eAgMnSc2d1
právníka	právník	k1gMnSc2
Otto	Otto	k1gMnSc1
Fischla	Fischl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matka	matka	k1gFnSc1
sice	sice	k8xC
byla	být	k5eAaImAgFnS
katolička	katolička	k1gFnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
otec	otec	k1gMnSc1
Žid	Žid	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
musel	muset	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
právnickou	právnický	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
před	před	k7c7
druhou	druhý	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
přerušit	přerušit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
války	válka	k1gFnSc2
však	však	k9
ani	ani	k8xC
otec	otec	k1gMnSc1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
ani	ani	k8xC
jeho	jeho	k3xOp3gFnPc1
dvě	dva	k4xCgFnPc1
dcera	dcera	k1gFnSc1
nemusely	muset	k5eNaImAgInP
odejít	odejít	k5eAaPmF
do	do	k7c2
transportu	transport	k1gInSc2
do	do	k7c2
koncentračního	koncentrační	k2eAgInSc2d1
tábora	tábor	k1gInSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
ošetřující	ošetřující	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
Otto	Otto	k1gMnSc1
Fischla	Fischla	k1gMnSc1
označil	označit	k5eAaPmAgMnS
za	za	k7c4
práce	práce	k1gFnPc4
neschopného	schopný	k2eNgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vstoupil	vstoupit	k5eAaPmAgMnS
Otto	Otto	k1gMnSc1
Fischl	Fischl	k1gMnSc1
do	do	k7c2
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
KSČ	KSČ	kA
<g/>
)	)	kIx)
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
náměstkem	náměstek	k1gMnSc7
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	léto	k1gNnPc6
1949	#num#	k4
až	až	k9
1951	#num#	k4
zastával	zastávat	k5eAaImAgInS
funkci	funkce	k1gFnSc4
velvyslance	velvyslanec	k1gMnSc2
Československa	Československo	k1gNnSc2
v	v	k7c6
Německé	německý	k2eAgFnSc6d1
demokratické	demokratický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
(	(	kIx(
<g/>
NDR	NDR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
dcery	dcera	k1gFnPc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
starší	starý	k2eAgFnSc1d2
Eva	Eva	k1gFnSc1
(	(	kIx(
<g/>
později	pozdě	k6eAd2
po	po	k7c6
provdání	provdání	k1gNnSc6
Vaňková	Vaňková	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
mladší	mladý	k2eAgFnSc1d2
Helena	Helena	k1gFnSc1
měly	mít	k5eAaImAgFnP
naopak	naopak	k6eAd1
protikomunistické	protikomunistický	k2eAgNnSc4d1
smýšlení	smýšlení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
pak	pak	k6eAd1
Eva	Eva	k1gFnSc1
emigrovala	emigrovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Otto	Otto	k1gMnSc1
Fischl	Fischl	k1gMnSc1
v	v	k7c6
lednu	leden	k1gInSc6
1951	#num#	k4
odvolán	odvolat	k5eAaPmNgInS
z	z	k7c2
diplomatického	diplomatický	k2eAgInSc2d1
postu	post	k1gInSc2
a	a	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
zatčen	zatknout	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souzen	souzen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
jako	jako	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
obžalovaných	obžalovaný	k1gMnPc2
v	v	k7c6
procesu	proces	k1gInSc6
s	s	k7c7
Rudolfem	Rudolf	k1gMnSc7
Slánským	Slánský	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1952	#num#	k4
nad	nad	k7c7
ním	on	k3xPp3gNnSc7
soud	soud	k1gInSc1
vyřkl	vyřknout	k5eAaPmAgInS
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
a	a	k8xC
začátkem	začátkem	k7c2
prosince	prosinec	k1gInSc2
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
byl	být	k5eAaImAgInS
popraven	popraven	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fischlova	Fischlův	k2eAgFnSc1d1
mladší	mladý	k2eAgFnSc1d2
dcera	dcera	k1gFnSc1
Helena	Helena	k1gFnSc1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
studovala	studovat	k5eAaImAgFnS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
něj	on	k3xPp3gInSc2
musela	muset	k5eAaImAgFnS
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
otcovým	otcův	k2eAgInSc7d1
procesem	proces	k1gInSc7
odejít	odejít	k5eAaPmF
a	a	k8xC
roku	rok	k1gInSc2
1953	#num#	k4
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
se	se	k3xPyFc4
svojí	svůj	k3xOyFgFnSc7
matkou	matka	k1gFnSc7
z	z	k7c2
Prahy	Praha	k1gFnSc2
násilně	násilně	k6eAd1
vystěhovat	vystěhovat	k5eAaPmF
do	do	k7c2
Stachovic	Stachovice	k1gFnPc2
<g/>
,	,	kIx,
obce	obec	k1gFnPc1
na	na	k7c6
severní	severní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
ležící	ležící	k2eAgFnSc1d1
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Fulneku	Fulnek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1954	#num#	k4
externě	externě	k6eAd1
odmaturovala	odmaturovat	k5eAaPmAgFnS
na	na	k7c6
jedenáctileté	jedenáctiletý	k2eAgFnSc6d1
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Novém	nový	k2eAgInSc6d1
Jičíně	Jičín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměstnána	zaměstnán	k2eAgFnSc1d1
následně	následně	k6eAd1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
v	v	k7c6
dělnických	dělnický	k2eAgFnPc6d1
profesích	profes	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
v	v	k7c6
papírenském	papírenský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
strojírenství	strojírenství	k1gNnSc6
či	či	k8xC
v	v	k7c6
obchodě	obchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
dálkově	dálkově	k6eAd1
přihlásila	přihlásit	k5eAaPmAgFnS
na	na	k7c4
studia	studio	k1gNnPc4
do	do	k7c2
Bratislavy	Bratislava	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
krátce	krátce	k6eAd1
poté	poté	k6eAd1
přestoupila	přestoupit	k5eAaPmAgFnS
na	na	k7c4
Vysokou	vysoký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
pedagogickou	pedagogický	k2eAgFnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
absolvovala	absolvovat	k5eAaPmAgFnS
obor	obor	k1gInSc4
český	český	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
a	a	k8xC
ruština	ruština	k1gFnSc1
a	a	k8xC
roku	rok	k1gInSc2
1959	#num#	k4
úspěšně	úspěšně	k6eAd1
obhájila	obhájit	k5eAaPmAgFnS
diplomovou	diplomový	k2eAgFnSc4d1
práci	práce	k1gFnSc4
na	na	k7c4
téma	téma	k1gNnSc4
„	„	k?
<g/>
Vývoj	vývoj	k1gInSc1
a	a	k8xC
současný	současný	k2eAgInSc1d1
stav	stav	k1gInSc1
poznání	poznání	k1gNnSc2
života	život	k1gInSc2
a	a	k8xC
díla	dílo	k1gNnSc2
Karla	Karel	k1gMnSc2
Čapka	Čapek	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
navíc	navíc	k6eAd1
v	v	k7c6
létě	léto	k1gNnSc6
1956	#num#	k4
provdala	provdat	k5eAaPmAgFnS
a	a	k8xC
peřstěhovala	peřstěhovat	k5eAaImAgFnS,k5eAaBmAgFnS,k5eAaPmAgFnS
zpět	zpět	k6eAd1
do	do	k7c2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1962	#num#	k4
do	do	k7c2
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
pracovala	pracovat	k5eAaImAgFnS
coby	coby	k?
literární	literární	k2eAgFnSc1d1
lektorka	lektorka	k1gFnSc1
v	v	k7c6
Pražské	pražský	k2eAgFnSc6d1
informační	informační	k2eAgFnSc6d1
službě	služba	k1gFnSc6
a	a	k8xC
mezi	mezi	k7c4
roky	rok	k1gInPc4
1963	#num#	k4
a	a	k8xC
1965	#num#	k4
jako	jako	k8xC,k8xS
odborná	odborný	k2eAgFnSc1d1
asistentka	asistentka	k1gFnSc1
v	v	k7c6
Památníku	památník	k1gInSc6
národního	národní	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
připravovala	připravovat	k5eAaImAgFnS
výstavy	výstava	k1gFnPc4
o	o	k7c4
Karlu	Karla	k1gFnSc4
Čapkovi	Čapek	k1gMnSc3
nebo	nebo	k8xC
o	o	k7c6
Franzi	Franze	k1gFnSc6
Kafkovi	Kafka	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
1965	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
svojí	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
i	i	k9
s	s	k7c7
maminkou	maminka	k1gFnSc7
ilegálně	ilegálně	k6eAd1
přes	přes	k7c4
Jugoslávii	Jugoslávie	k1gFnSc4
emigrovala	emigrovat	k5eAaBmAgFnS
do	do	k7c2
Švédska	Švédsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Usadila	usadit	k5eAaPmAgFnS
se	se	k3xPyFc4
v	v	k7c6
Norrköpingu	Norrköping	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
vyučovala	vyučovat	k5eAaImAgFnS
na	na	k7c6
tamních	tamní	k2eAgFnPc6d1
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
češtinu	čeština	k1gFnSc4
<g/>
,	,	kIx,
ruštinu	ruština	k1gFnSc4
a	a	k8xC
francouzštinu	francouzština	k1gFnSc4
a	a	k8xC
dále	daleko	k6eAd2
na	na	k7c6
Stockholmské	stockholmský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
vedla	vést	k5eAaImAgFnS
jazykové	jazykový	k2eAgInPc4d1
kurzy	kurz	k1gInPc4
pro	pro	k7c4
její	její	k3xOp3gMnPc4
posluchače	posluchač	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
důchodu	důchod	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
i	i	k9
nadále	nadále	k6eAd1
je	být	k5eAaImIp3nS
hostující	hostující	k2eAgFnSc7d1
profesorkou	profesorka	k1gFnSc7
češtiny	čeština	k1gFnSc2
na	na	k7c6
univerzitách	univerzita	k1gFnPc6
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6
a	a	k8xC
v	v	k7c6
Göteborgu	Göteborg	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Řadí	řadit	k5eAaImIp3nS
se	se	k3xPyFc4
mezi	mezi	k7c4
aktivní	aktivní	k2eAgFnPc4d1
propagátorky	propagátorka	k1gFnPc4
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
publikuje	publikovat	k5eAaBmIp3nS
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Helena	Helena	k1gFnSc1
Kosek	Kosek	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
ZELINSKÝ	ZELINSKÝ	kA
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
;	;	kIx,
KOŠNAROVÁ	KOŠNAROVÁ	kA
<g/>
,	,	kIx,
Veronika	Veronika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovník	slovník	k1gInSc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ústav	ústav	k1gInSc1
pro	pro	k7c4
českou	český	k2eAgFnSc4d1
literaturu	literatura	k1gFnSc4
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Helena	Helena	k1gFnSc1
Kosková	Kosková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Helena	Helena	k1gFnSc1
Kosková	Kosková	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Paměť	paměť	k1gFnSc1
národa	národ	k1gInSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Helena	Helena	k1gFnSc1
Kosková	Kosková	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990218051	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
22183141	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Literatura	literatura	k1gFnSc1
</s>
