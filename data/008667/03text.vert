<p>
<s>
Solnice	solnice	k1gFnSc1	solnice
je	být	k5eAaImIp3nS	být
budova	budova	k1gFnSc1	budova
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
skladování	skladování	k1gNnSc4	skladování
a	a	k8xC	a
uchovávání	uchovávání	k1gNnSc4	uchovávání
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
z	z	k7c2	z
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
solnic	solnice	k1gFnPc2	solnice
má	mít	k5eAaImIp3nS	mít
památkovou	památkový	k2eAgFnSc4d1	památková
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
kulturními	kulturní	k2eAgFnPc7d1	kulturní
památkami	památka	k1gFnPc7	památka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Solnice	solnice	k1gFnSc1	solnice
(	(	kIx(	(
<g/>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Solnice	solnice	k1gFnSc1	solnice
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
</s>
</p>
