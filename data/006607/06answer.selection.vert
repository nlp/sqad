<s>
Buňka	buňka	k1gFnSc1	buňka
sinic	sinice	k1gFnPc2	sinice
je	být	k5eAaImIp3nS	být
prokaryotického	prokaryotický	k2eAgInSc2d1	prokaryotický
typu	typ	k1gInSc2	typ
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velikosti	velikost	k1gFnSc2	velikost
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
více	hodně	k6eAd2	hodně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
buňka	buňka	k1gFnSc1	buňka
Chroococcus	Chroococcus	k1gMnSc1	Chroococcus
giganteus	giganteus	k1gMnSc1	giganteus
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
60	[number]	k4	60
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
