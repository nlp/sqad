<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dien	dien	k1gInSc4
Bien	biena	k1gFnPc2
Phu	Phu	k1gFnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dien	dien	k1gInSc4
Bien	biena	k1gFnPc2
Phu	Phu	k1gMnSc1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Indočínská	indočínský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1
parušisté	parušista	k1gMnPc1
vyskakují	vyskakovat	k5eAaImIp3nP
z	z	k7c2
C-119	C-119	k1gFnSc2
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
,	,	kIx,
1954	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Dien	dien	k1gInSc1
Bien	biena	k1gFnPc2
Phu	Phu	k1gFnPc2
<g/>
,	,	kIx,
Vietnam	Vietnam	k1gInSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Rozhodující	rozhodující	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Viet	Vieta	k1gFnPc2
Minhu	Minh	k1gInSc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
</s>
<s>
Vietnamský	vietnamský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Podpora	podpora	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Hmongští	Hmongský	k2eAgMnPc1d1
partyzáni	partyzán	k1gMnPc1
USA	USA	kA
</s>
<s>
Việ	Việ	k2eAgInSc1d1
Minh	Minh	k1gInSc1
</s>
<s>
Podpora	podpora	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
SSSR	SSSR	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
čínští	čínský	k2eAgMnPc1d1
voj.	voj.	k?
pozorovatelé	pozorovatel	k1gMnPc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Christian	Christian	k1gMnSc1
de	de	k?
Castries	Castries	k1gMnSc1
(	(	kIx(
<g/>
zajat	zajat	k2eAgMnSc1d1
<g/>
)	)	kIx)
Pierre	Pierr	k1gInSc5
Langlais	Langlais	k1gFnPc7
(	(	kIx(
<g/>
zajat	zajat	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Võ	Võ	k?
Nguyê	Nguyê	k1gMnSc1
Giáp	Giáp	k1gMnSc1
Wei	Wei	k1gMnSc1
Guoqing	Guoqing	k1gInSc1
(	(	kIx(
<g/>
pozorovatel	pozorovatel	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
K	k	k7c3
13	#num#	k4
<g/>
.	.	kIx.
březnu	březen	k1gInSc6
<g/>
:	:	kIx,
10	#num#	k4
800	#num#	k4
37	#num#	k4
pilotů	pilot	k1gInPc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
13	#num#	k4
<g/>
.	.	kIx.
březnu	březen	k1gInSc6
<g/>
:	:	kIx,
48	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
15	#num#	k4
000	#num#	k4
logistická	logistický	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
1	#num#	k4
571	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
2	#num#	k4
293	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
5	#num#	k4
195	#num#	k4
–	–	k?
6	#num#	k4
650	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
zraněných	zraněný	k2eAgInPc2d1
1	#num#	k4
729	#num#	k4
pohřešovaných	pohřešovaná	k1gFnPc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
721	#num#	k4
zajatců	zajatec	k1gMnPc2
8	#num#	k4
290	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americké	americký	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
2	#num#	k4
mrtví	mrtvit	k5eAaImIp3nS
(	(	kIx(
<g/>
James	James	k1gMnSc1
"	"	kIx"
<g/>
Earthquake	Earthquake	k1gFnSc1
McGoon	McGoon	k1gMnSc1
<g/>
"	"	kIx"
McGovern	McGovern	k1gMnSc1
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Vietnamské	vietnamský	k2eAgInPc1d1
údaje	údaj	k1gInPc1
4	#num#	k4
020	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
9	#num#	k4
118	#num#	k4
zraněných	zraněný	k1gMnPc2
792	#num#	k4
pohřešovaných	pohřešovaný	k2eAgInPc2d1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Francouzské	francouzský	k2eAgInPc1d1
odhady	odhad	k1gInPc1
23	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dien	dien	k1gNnSc2
Bien	Bien	k1gNnSc2
Phu	Phu	k1gNnSc2
(	(	kIx(
<g/>
čti	číst	k5eAaImRp2nS
bitva	bitva	k1gFnSc1
u	u	k7c2
Dien	dien	k1gNnSc2
Bien	Bien	k1gNnSc2
Fu	fu	k1gNnSc2
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
bataille	bataille	k6eAd1
de	de	k?
Diê	Diê	k1gMnSc1
Biê	Biê	k1gMnSc1
Phu	Phu	k1gMnSc1
<g/>
,	,	kIx,
vietnamsky	vietnamsky	k6eAd1
chiế	chiế	k1gMnSc1
dị	dị	k1gMnSc1
Điệ	Điệ	k1gMnSc1
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
mezi	mezi	k7c7
francouzskými	francouzský	k2eAgNnPc7d1
koloniálními	koloniální	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
a	a	k8xC
silami	síla	k1gFnPc7
komunistického	komunistický	k2eAgInSc2d1
Viet	Viet	k1gInSc4
Minhu	Minh	k1gInSc2
<g/>
,	,	kIx,
podporovaných	podporovaný	k2eAgInPc2d1
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
a	a	k8xC
komunistickou	komunistický	k2eAgFnSc7d1
Čínou	Čína	k1gFnSc7
na	na	k7c6
straně	strana	k1gFnSc6
druhé	druhý	k4xOgFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
velkým	velký	k2eAgInSc7d1
triumfem	triumf	k1gInSc7
pro	pro	k7c4
Vietnamce	Vietnamec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Soupeři	soupeř	k1gMnPc1
</s>
<s>
Plk.	plk.	kA
Christian	Christian	k1gMnSc1
de	de	k?
Castries	Castries	k1gMnSc1
<g/>
,	,	kIx,
francouzský	francouzský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
u	u	k7c2
Điệ	Điệ	k1gFnPc2
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
</s>
<s>
Co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
situace	situace	k1gFnSc1
v	v	k7c4
Dien	dien	k1gInSc4
Bien	biena	k1gFnPc2
Phu	Phu	k1gFnPc2
<g/>
,	,	kIx,
Francouzi	Francouz	k1gMnPc1
tomuto	tento	k3xDgNnSc3
místu	místo	k1gNnSc3
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
takovou	takový	k3xDgFnSc4
pozornost	pozornost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
velením	velení	k1gNnSc7
byli	být	k5eAaImAgMnP
pověřeni	pověřit	k5eAaPmNgMnP
hned	hned	k6eAd1
tři	tři	k4xCgMnPc1
generálové	generál	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc4
gen.	gen.	kA
Henri	Henr	k1gFnPc4
Navarre	Navarr	k1gInSc5
(	(	kIx(
<g/>
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
ve	v	k7c6
Francouzské	francouzský	k2eAgFnSc6d1
Indočíně	Indočína	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
gen.	gen.	kA
René	René	k1gFnSc1
Cogny	Cogna	k1gFnSc2
a	a	k8xC
plk.	plk.	kA
Christian	Christian	k1gMnSc1
de	de	k?
Castries	Castries	k1gMnSc1
(	(	kIx(
<g/>
povýšen	povýšen	k2eAgMnSc1d1
na	na	k7c4
generála	generál	k1gMnSc4
během	během	k7c2
bitvy	bitva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
francouzské	francouzský	k2eAgFnSc2d1
koloniální	koloniální	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
bylo	být	k5eAaImAgNnS
utkat	utkat	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
Viet	Viet	k1gInSc1
Minhem	Minh	k1gInSc7
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
rozhodující	rozhodující	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
a	a	k8xC
samozřejmě	samozřejmě	k6eAd1
zvítězit	zvítězit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
poučení	poučení	k1gNnSc6
z	z	k7c2
předchozích	předchozí	k2eAgInPc2d1
střetů	střet	k1gInPc2
s	s	k7c7
Vietnamci	Vietnamec	k1gMnPc7
<g/>
,	,	kIx,
Francouzi	Francouz	k1gMnPc1
potřebovali	potřebovat	k5eAaImAgMnP
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
nepříteli	nepřítel	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
proti	proti	k7c3
kterému	který	k3yQgNnSc3,k3yRgNnSc3,k3yIgNnSc3
mohli	moct	k5eAaImAgMnP
efektivně	efektivně	k6eAd1
nasadit	nasadit	k5eAaPmF
letectvo	letectvo	k1gNnSc4
<g/>
,	,	kIx,
techniku	technika	k1gFnSc4
a	a	k8xC
dělostřelectvo	dělostřelectvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
famózní	famózní	k2eAgNnSc1d1
zakončení	zakončení	k1gNnSc1
indočínské	indočínský	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
uskutečnit	uskutečnit	k5eAaPmF
právě	právě	k9
v	v	k7c4
Điệ	Điệ	k1gInSc4
Biê	Biê	k1gNnSc4
Phủ	Phủ	k1gFnSc2
<g/>
,	,	kIx,
vesnici	vesnice	k1gFnSc6
ležící	ležící	k2eAgFnSc2d1
v	v	k7c6
Horním	horní	k2eAgInSc6d1
Tonkinu	Tonkin	k1gInSc6
<g/>
,	,	kIx,
poblíž	poblíž	k6eAd1
hranic	hranice	k1gFnPc2
s	s	k7c7
Laosem	Laos	k1gInSc7
<g/>
,	,	kIx,
vzdálené	vzdálený	k2eAgInPc4d1
od	od	k7c2
Hanoje	Hanoj	k1gFnSc2
cca	cca	kA
300	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
rozlehlá	rozlehlý	k2eAgFnSc1d1
planina	planina	k1gFnSc1
ohraničená	ohraničený	k2eAgFnSc1d1
ze	z	k7c2
stran	strana	k1gFnPc2
vyvýšenými	vyvýšený	k2eAgInPc7d1
body	bod	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
později	pozdě	k6eAd2
Francouzi	Francouz	k1gMnPc1
obsadili	obsadit	k5eAaPmAgMnP
a	a	k8xC
dali	dát	k5eAaPmAgMnP
jim	on	k3xPp3gMnPc3
ženská	ženský	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
Anne-Marie	Anne-Marie	k1gFnSc2
<g/>
,	,	kIx,
Beatrice	Beatrice	k1gFnSc2
<g/>
,	,	kIx,
Claudie	Claudia	k1gFnSc2
<g/>
,	,	kIx,
Dominique	Dominiqu	k1gFnSc2
<g/>
,	,	kIx,
Eliane	Elian	k1gMnSc5
<g/>
,	,	kIx,
Gabriele	Gabriel	k1gMnSc5
<g/>
,	,	kIx,
Huguette	Huguett	k1gMnSc5
a	a	k8xC
Isabelle	Isabelle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1953	#num#	k4
byla	být	k5eAaImAgFnS
vesnice	vesnice	k1gFnPc4
Điệ	Điệ	k1gNnPc2
Biê	Biê	k1gFnPc2
Phủ	Phủ	k1gFnPc2
obsazena	obsazen	k2eAgFnSc1d1
francouzskými	francouzský	k2eAgMnPc7d1
parašutisty	parašutista	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
během	během	k7c2
výsadku	výsadek	k1gInSc2
na	na	k7c4
ně	on	k3xPp3gInPc4
Viet	Viet	k2eAgMnSc1d1
Minh	Minh	k1gMnSc1
střílel	střílet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dopadu	dopad	k1gInSc6
na	na	k7c4
zem	zem	k1gFnSc4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
drobné	drobný	k2eAgFnSc3d1
přestřelce	přestřelka	k1gFnSc3
<g/>
,	,	kIx,
po	po	k7c4
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
následoval	následovat	k5eAaImAgInS
relativní	relativní	k2eAgInSc1d1
klid	klid	k1gInSc1
na	na	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
Navarre	Navarr	k1gInSc5
později	pozdě	k6eAd2
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
cílem	cíl	k1gInSc7
zbudování	zbudování	k1gNnSc2
opevněné	opevněný	k2eAgFnSc2d1
základny	základna	k1gFnSc2
v	v	k7c4
Điệ	Điệ	k1gInSc4
Biê	Biê	k1gNnSc4
Phủ	Phủ	k1gFnSc2
je	být	k5eAaImIp3nS
ochrana	ochrana	k1gFnSc1
Laosu	Laos	k1gInSc2
před	před	k7c7
Viet	Viet	k1gInSc1
Minhem	Minh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
tábor	tábor	k1gInSc1
nemohl	moct	k5eNaImAgInS
být	být	k5eAaImF
zásobován	zásobovat	k5eAaImNgInS
pozemními	pozemní	k2eAgFnPc7d1
komunikacemi	komunikace	k1gFnPc7
<g/>
,	,	kIx,
vznikl	vzniknout	k5eAaPmAgInS
letecký	letecký	k2eAgInSc1d1
most	most	k1gInSc1
z	z	k7c2
Hanoje	Hanoj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
rozkaz	rozkaz	k1gInSc4
„	„	k?
<g/>
zakopat	zakopat	k5eAaPmF
se	se	k3xPyFc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
vojáci	voják	k1gMnPc1
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Điệ	Điệ	k1gMnSc1
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
brzy	brzy	k6eAd1
opustí	opustit	k5eAaPmIp3nS
<g/>
,	,	kIx,
zakopali	zakopat	k5eAaPmAgMnP
se	se	k3xPyFc4
jen	jen	k9
povrchně	povrchně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdo	nikdo	k3yNnSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
nevěděl	vědět	k5eNaImAgInS
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
tam	tam	k6eAd1
vlastně	vlastně	k9
dělají	dělat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1
výsadkáři	výsadkář	k1gMnPc1
a	a	k8xC
příslušníci	příslušník	k1gMnPc1
cizinecké	cizinecký	k2eAgFnSc2d1
legie	legie	k1gFnSc2
se	se	k3xPyFc4
vydávali	vydávat	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c4
hlídky	hlídka	k1gFnPc4
do	do	k7c2
okolních	okolní	k2eAgInPc2d1
kopců	kopec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
operace	operace	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legionáři	legionář	k1gMnPc1
měli	mít	k5eAaImAgMnP
za	za	k7c4
úkol	úkol	k1gInSc4
vyhledat	vyhledat	k5eAaPmF
vojáky	voják	k1gMnPc4
z	z	k7c2
8	#num#	k4
<g/>
.	.	kIx.
výsadkového	výsadkový	k2eAgInSc2d1
praporu	prapor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
je	on	k3xPp3gMnPc4
našli	najít	k5eAaPmAgMnP
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
jim	on	k3xPp3gMnPc3
v	v	k7c6
patách	pata	k1gFnPc6
Việ	Việ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
bylo	být	k5eAaImAgNnS
časté	častý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
hlídky	hlídka	k1gFnPc1
byly	být	k5eAaImAgFnP
napadány	napadán	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokaždé	pokaždé	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
nějaká	nějaký	k3yIgFnSc1
jednotka	jednotka	k1gFnSc1
vyrazila	vyrazit	k5eAaPmAgFnS
na	na	k7c4
průzkum	průzkum	k1gInSc4
<g/>
,	,	kIx,
stačilo	stačit	k5eAaBmAgNnS
na	na	k7c4
okraj	okraj	k1gInSc4
údolí	údolí	k1gNnSc2
<g/>
,	,	kIx,
narazila	narazit	k5eAaPmAgFnS
na	na	k7c4
Việ	Việ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
<g/>
,	,	kIx,
díky	díky	k7c3
svým	svůj	k3xOyFgMnPc3
radistům	radista	k1gMnPc3
<g/>
,	,	kIx,
poznat	poznat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
k	k	k7c3
Điệ	Điệ	k1gFnPc2
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
blíží	blížit	k5eAaImIp3nS
Việ	Việ	k1gMnSc1
<g/>
,	,	kIx,
gen.	gen.	kA
Navarre	Navarr	k1gInSc5
však	však	k9
nevěří	věřit	k5eNaImIp3nS
gen.	gen.	kA
Cognymu	Cognym	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
okolních	okolní	k2eAgInPc6d1
kopcích	kopec	k1gInPc6
se	se	k3xPyFc4
již	již	k6eAd1
soustřeďuje	soustřeďovat	k5eAaImIp3nS
Việ	Việ	k1gInSc4
a	a	k8xC
rozmisťuje	rozmisťovat	k5eAaImIp3nS
tam	tam	k6eAd1
dělostřelecké	dělostřelecký	k2eAgFnPc4d1
baterie	baterie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
Việ	Việ	k1gInSc2
<g/>
,	,	kIx,
celé	celý	k2eAgFnSc6d1
operaci	operace	k1gFnSc6
velel	velet	k5eAaImAgMnS
zkušený	zkušený	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
a	a	k8xC
blízký	blízký	k2eAgInSc1d1
Ho	on	k3xPp3gNnSc4
Či	či	k8xC
Minův	Minův	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
<g/>
,	,	kIx,
gen.	gen.	kA
Võ	Võ	k1gFnSc1
Nguyê	Nguyê	k1gMnSc1
Giáp	Giáp	k1gInSc1
(	(	kIx(
<g/>
celý	celý	k2eAgInSc4d1
průběh	průběh	k1gInSc4
bitvy	bitva	k1gFnSc2
popsal	popsat	k5eAaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Vietnamská	vietnamský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
samozřejmě	samozřejmě	k6eAd1
na	na	k7c6
straně	strana	k1gFnSc6
Việ	Việ	k1gInSc2
nechyběla	chybět	k5eNaImAgFnS
pomoc	pomoc	k1gFnSc4
spřátelených	spřátelený	k2eAgFnPc2d1
komunistických	komunistický	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
podobě	podoba	k1gFnSc6
sovětských	sovětský	k2eAgMnPc2d1
a	a	k8xC
čínských	čínský	k2eAgMnPc2d1
poradců	poradce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Gen.	gen.	kA
Cogny	Cogna	k1gFnSc2
byl	být	k5eAaImAgInS
pro	pro	k7c4
evakuaci	evakuace	k1gFnSc4
z	z	k7c2
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
Gen.	gen.	kA
Navarre	Navarr	k1gMnSc5
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
hlavní	hlavní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tábor	tábor	k1gInSc1
však	však	k9
evakuován	evakuován	k2eAgMnSc1d1
nebude	být	k5eNaImBp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Việ	Việ	k1gMnSc1
bude	být	k5eAaImBp3nS
rozprášen	rozprášit	k5eAaPmNgMnS
letectvem	letectvo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
již	již	k6eAd1
měsíc	měsíc	k1gInSc1
bombardovalo	bombardovat	k5eAaImAgNnS
přístupové	přístupový	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
nepočítali	počítat	k5eNaImAgMnP
s	s	k7c7
houževnatostí	houževnatost	k1gFnSc7
nepřítele	nepřítel	k1gMnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
francouzské	francouzský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
nestíhalo	stíhat	k5eNaImAgNnS
znovu	znovu	k6eAd1
ničit	ničit	k5eAaImF
opravované	opravovaný	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
bitvy	bitva	k1gFnSc2
</s>
<s>
Francouzi	Francouz	k1gMnPc1
operovali	operovat	k5eAaImAgMnP
s	s	k7c7
několika	několik	k4yIc7
americkými	americký	k2eAgInPc7d1
lehkými	lehký	k2eAgInPc7d1
tanky	tank	k1gInPc7
M24	M24	k1gMnSc2
Chaffee	Chaffe	k1gMnSc2
</s>
<s>
Francouzské	francouzský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
v	v	k7c6
zákopech	zákop	k1gInPc6
</s>
<s>
V	v	k7c4
předvečer	předvečer	k1gInSc4
osudné	osudný	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
byla	být	k5eAaImAgFnS
situace	situace	k1gFnSc1
pro	pro	k7c4
Francouze	Francouz	k1gMnPc4
špatná	špatný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectvo	letectvo	k1gNnSc1
již	již	k6eAd1
nedisponovalo	disponovat	k5eNaBmAgNnS
bombardovací	bombardovací	k2eAgFnSc7d1
eskadrou	eskadra	k1gFnSc7
a	a	k8xC
posily	posila	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
žádal	žádat	k5eAaImAgMnS
gen.	gen.	kA
Navarre	Navarr	k1gInSc5
<g/>
,	,	kIx,
mu	on	k3xPp3gNnSc3
byly	být	k5eAaImAgFnP
odepřeny	odepřen	k2eAgFnPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
vrchní	vrchní	k2eAgNnSc1d1
velení	velení	k1gNnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
nepřítele	nepřítel	k1gMnSc2
podceňovalo	podceňovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Odpoledne	odpoledne	k6eAd1
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1954	#num#	k4
začala	začít	k5eAaPmAgFnS
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
Việ	Việ	k1gInSc2
trvající	trvající	k2eAgFnSc1d1
12	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
výsadkářů	výsadkář	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
chránili	chránit	k5eAaImAgMnP
základnu	základna	k1gFnSc4
Beatrice	Beatrice	k1gFnSc2
<g/>
,	,	kIx,
zahynula	zahynout	k5eAaPmAgFnS
na	na	k7c6
svých	svůj	k3xOyFgInPc6
postech	post	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vietnamci	Vietnamec	k1gMnPc1
použili	použít	k5eAaPmAgMnP
všechny	všechen	k3xTgInPc4
kalibry	kalibr	k1gInPc4
<g/>
:	:	kIx,
105	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
minomety	minomet	k1gInPc7
120	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
81	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
pěti	pět	k4xCc3
hodin	hodina	k1gFnPc2
byl	být	k5eAaImAgInS
rozprášen	rozprášen	k2eAgInSc1d1
celý	celý	k2eAgInSc1d1
prapor	prapor	k1gInSc1
cizinecké	cizinecký	k2eAgFnSc2d1
legie	legie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
špatnému	špatný	k2eAgNnSc3d1
počasí	počasí	k1gNnSc3
bylo	být	k5eAaImAgNnS
vyloučeno	vyloučit	k5eAaPmNgNnS
veškeré	veškerý	k3xTgNnSc1
odvetné	odvetný	k2eAgNnSc1d1
bombardování	bombardování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
ten	ten	k3xDgInSc1
den	den	k1gInSc1
padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
opěrný	opěrný	k2eAgInSc4d1
bod	bod	k1gInSc4
Beatrice	Beatrice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
nebyli	být	k5eNaImAgMnP
schopni	schopen	k2eAgMnPc1d1
vypátrat	vypátrat	k5eAaPmF
dělostřelecké	dělostřelecký	k2eAgFnPc4d1
baterie	baterie	k1gFnPc4
nepřítele	nepřítel	k1gMnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
Việ	Việ	k1gInSc1
děla	dělo	k1gNnSc2
upevnil	upevnit	k5eAaPmAgInS
na	na	k7c4
vysunovací	vysunovací	k2eAgFnPc4d1
rampy	rampa	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
hned	hned	k6eAd1
po	po	k7c6
útoku	útok	k1gInSc6
zatáhl	zatáhnout	k5eAaPmAgMnS
zpět	zpět	k6eAd1
do	do	k7c2
úkrytu	úkryt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
Gabriele	Gabriela	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc1
provedli	provést	k5eAaPmAgMnP
protiútok	protiútok	k1gInSc4
s	s	k7c7
jednotkou	jednotka	k1gFnSc7
nováčků	nováček	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
byli	být	k5eAaImAgMnP
nuceni	nutit	k5eAaImNgMnP
stáhnout	stáhnout	k5eAaPmF
se	se	k3xPyFc4
zpět	zpět	k6eAd1
do	do	k7c2
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
pomoc	pomoc	k1gFnSc4
obleženým	obležený	k2eAgMnPc3d1
vojákům	voják	k1gMnPc3
v	v	k7c4
Điệ	Điệ	k1gInSc4
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
přispěchal	přispěchat	k5eAaPmAgMnS
speciální	speciální	k2eAgInSc4d1
prapor	prapor	k1gInSc4
výsadkářů	výsadkář	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
seskočil	seskočit	k5eAaPmAgMnS
na	na	k7c6
Isabele	Isabela	k1gFnSc6
(	(	kIx(
<g/>
kopec	kopec	k1gInSc1
vzdálený	vzdálený	k2eAgInSc1d1
5	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
Điệ	Điệ	k1gFnPc2
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
dosedlo	dosednout	k5eAaPmAgNnS
na	na	k7c4
letiště	letiště	k1gNnSc4
v	v	k7c4
Điệ	Điệ	k1gInSc4
Biê	Biê	k1gNnSc4
Phủ	Phủ	k1gFnSc2
poslední	poslední	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzské	francouzský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
bombardovalo	bombardovat	k5eAaImAgNnS
nepřítele	nepřítel	k1gMnSc4
napalmem	napalm	k1gInSc7
bez	bez	k7c2
jakékoli	jakýkoli	k3yIgFnSc2
navigace	navigace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	s	k7c7
přesně	přesně	k6eAd1
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
Francouzi	Francouz	k1gMnPc1
nečekali	čekat	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Việ	Việ	k1gMnSc1
obsadil	obsadit	k5eAaPmAgMnS
okolní	okolní	k2eAgInPc4d1
kopce	kopec	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zbudoval	zbudovat	k5eAaPmAgMnS
dělostřelecké	dělostřelecký	k2eAgInPc4d1
posty	post	k1gInPc4
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgInPc2,k3yRgInPc2,k3yIgInPc2
mohl	moct	k5eAaImAgInS
celou	celý	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
velice	velice	k6eAd1
přesně	přesně	k6eAd1
ostřelovat	ostřelovat	k5eAaImF
a	a	k8xC
přitom	přitom	k6eAd1
nebyl	být	k5eNaImAgMnS
vidět	vidět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
po	po	k7c6
několikadenní	několikadenní	k2eAgFnSc6d1
přestávce	přestávka	k1gFnSc6
zahájili	zahájit	k5eAaPmAgMnP
Vietnamci	Vietnamec	k1gMnPc1
útoky	útok	k1gInPc4
proti	proti	k7c3
opěrným	opěrný	k2eAgInPc3d1
bodům	bod	k1gInPc3
Eliane	Elian	k1gMnSc5
<g/>
,	,	kIx,
Dominique	Dominiquus	k1gMnSc5
a	a	k8xC
Huguette	Huguett	k1gMnSc5
–	–	k?
tzv.	tzv.	kA
Bitva	bitva	k1gFnSc1
pěti	pět	k4xCc2
pahorků	pahorek	k1gInPc2
(	(	kIx(
<g/>
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
do	do	k7c2
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1954	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastalo	nastat	k5eAaPmAgNnS
období	období	k1gNnSc1
dešťů	dešť	k1gInPc2
<g/>
,	,	kIx,
balíky	balík	k1gInPc4
s	s	k7c7
příděly	příděl	k1gInPc7
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
munice	munice	k1gFnSc2
a	a	k8xC
lékařského	lékařský	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
padaly	padat	k5eAaImAgFnP
do	do	k7c2
oblasti	oblast	k1gFnSc2
kontrolované	kontrolovaný	k2eAgFnSc2d1
Việ	Việ	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
dokonce	dokonce	k9
i	i	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
Francouzům	Francouz	k1gMnPc3
došly	dojít	k5eAaPmAgInP
padáky	padák	k1gInPc1
určené	určený	k2eAgInPc1d1
k	k	k7c3
zásobovacím	zásobovací	k2eAgFnPc3d1
přepravám	přeprava	k1gFnPc3
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
zahajuje	zahajovat	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
kapitána	kapitán	k1gMnSc2
Sassiho	Sassi	k1gMnSc2
operaci	operace	k1gFnSc4
Kondor	kondor	k1gMnSc1
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
byla	být	k5eAaImAgFnS
likvidace	likvidace	k1gFnSc1
zásobování	zásobování	k1gNnSc2
Việ	Việ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohužel	bohužel	k6eAd1
ke	k	k7c3
schválení	schválení	k1gNnSc3
operace	operace	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
příliš	příliš	k6eAd1
pozdě	pozdě	k6eAd1
<g/>
,	,	kIx,
takže	takže	k8xS
neměla	mít	k5eNaImAgFnS
takový	takový	k3xDgInSc4
efekt	efekt	k1gInSc4
<g/>
,	,	kIx,
jaký	jaký	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
naplánovaný	naplánovaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostor	prostora	k1gFnPc2
Điệ	Điệ	k1gMnSc1
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
kontrolovaný	kontrolovaný	k2eAgMnSc1d1
Francouzi	Francouz	k1gMnPc1
se	se	k3xPyFc4
pomalu	pomalu	k6eAd1
zmenšoval	zmenšovat	k5eAaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
Washington	Washington	k1gInSc1
začal	začít	k5eAaPmAgInS
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
přímé	přímý	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
intervenci	intervence	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
však	však	k9
podmiňoval	podmiňovat	k5eAaImAgMnS
požehnáním	požehnání	k1gNnSc7
amerického	americký	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kongres	kongres	k1gInSc1
jej	on	k3xPp3gMnSc4
podmiňoval	podmiňovat	k5eAaImAgInS
spoluúčastí	spoluúčast	k1gFnSc7
evropských	evropský	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
a	a	k8xC
evropští	evropský	k2eAgMnPc1d1
spojenci	spojenec	k1gMnPc1
nechtěli	chtít	k5eNaImAgMnP
ohrozit	ohrozit	k5eAaPmF
právě	právě	k9
probíhající	probíhající	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
vůbec	vůbec	k9
poprvé	poprvé	k6eAd1
zasedli	zasednout	k5eAaPmAgMnP
k	k	k7c3
jednacímu	jednací	k2eAgInSc3d1
stolu	stol	k1gInSc3
s	s	k7c7
Číňany	Číňan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
navíc	navíc	k6eAd1
riziko	riziko	k1gNnSc1
konfliktu	konflikt	k1gInSc2
s	s	k7c7
Čínou	Čína	k1gFnSc7
bylo	být	k5eAaImAgNnS
vážné	vážné	k1gNnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
nakonec	nakonec	k6eAd1
ze	z	k7c2
všeho	všecek	k3xTgNnSc2
sešlo	sejít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc7
až	až	k6eAd1
do	do	k7c2
poslední	poslední	k2eAgFnSc2d1
chvíle	chvíle	k1gFnSc2
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jim	on	k3xPp3gMnPc3
Američané	Američan	k1gMnPc1
přijdou	přijít	k5eAaPmIp3nP
na	na	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Francii	Francie	k1gFnSc6
začíná	začínat	k5eAaImIp3nS
sílit	sílit	k5eAaImF
hnutí	hnutí	k1gNnSc1
proti	proti	k7c3
válce	válka	k1gFnSc3
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
Francie	Francie	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
vyjednávat	vyjednávat	k5eAaImF
se	s	k7c7
zástupci	zástupce	k1gMnPc7
komunistického	komunistický	k2eAgInSc2d1
Vietnamu	Vietnam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovory	rozhovor	k1gInPc7
vedl	vést	k5eAaImAgMnS
Pierre	Pierr	k1gInSc5
Mendè	Mendè	k1gFnPc2
France	Franc	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
Francie	Francie	k1gFnSc2
bylo	být	k5eAaImAgNnS
vydržet	vydržet	k5eAaPmF
v	v	k7c4
Điệ	Điệ	k1gInSc4
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
do	do	k7c2
skončení	skončení	k1gNnSc2
mírových	mírový	k2eAgInPc2d1
rozhovorů	rozhovor	k1gInPc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
však	však	k9
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
nemožné	možný	k2eNgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
poslední	poslední	k2eAgInSc4d1
den	den	k1gInSc4
bitvy	bitva	k1gFnSc2
nasadil	nasadit	k5eAaPmAgMnS
Việ	Việ	k1gMnSc1
tzv.	tzv.	kA
Stalinovy	Stalinův	k2eAgFnPc4d1
varhany	varhany	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1954	#num#	k4
Francouzi	Francouz	k1gMnPc1
kapitulovali	kapitulovat	k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
bitvy	bitva	k1gFnSc2
</s>
<s>
Bilance	bilance	k1gFnSc1
této	tento	k3xDgFnSc2
bitvy	bitva	k1gFnSc2
byla	být	k5eAaImAgFnS
katastrofální	katastrofální	k2eAgFnSc1d1
hlavně	hlavně	k9
pro	pro	k7c4
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
prohra	prohra	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
nejen	nejen	k6eAd1
konec	konec	k1gInSc4
koloniálního	koloniální	k2eAgNnSc2d1
panství	panství	k1gNnSc2
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
cca	cca	kA
8000	#num#	k4
zajatých	zajatý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztráty	ztráta	k1gFnSc2
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
se	se	k3xPyFc4
pohybovaly	pohybovat	k5eAaImAgFnP
kolem	kolem	k7c2
3000	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
měl	mít	k5eAaImAgInS
Việ	Việ	k1gInSc1
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc2d2
ztráty	ztráta	k1gFnSc2
než	než	k8xS
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgMnS
se	se	k3xPyFc4
radovat	radovat	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
konečně	konečně	k6eAd1
získal	získat	k5eAaPmAgMnS
svobodu	svoboda	k1gFnSc4
na	na	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
čekal	čekat	k5eAaImAgMnS
bezmála	bezmála	k6eAd1
200	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
First	First	k1gMnSc1
Indochina	Indochina	k1gMnSc1
War	War	k1gMnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historyguy	Historygua	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Xiaobing	Xiaobing	k1gInSc1
Li	li	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
history	histor	k1gInPc1
of	of	k?
the	the	k?
modern	modern	k1gInSc1
Chinese	Chinese	k1gFnSc2
Army	Arma	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
University	universita	k1gFnSc2
Press	Press	k1gInSc1
of	of	k?
Kentucky	Kentucka	k1gFnSc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8131	#num#	k4
<g/>
-	-	kIx~
<g/>
2438	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
212	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
French	French	k1gMnSc1
Ambassy	Ambassa	k1gFnSc2
in	in	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
:	:	kIx,
News	News	k1gInSc1
from	from	k1gInSc1
France	Franc	k1gMnSc2
0	#num#	k4
<g/>
5.02	5.02	k4
(	(	kIx(
<g/>
March	March	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
,	,	kIx,
possition	possition	k1gInSc1
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
11	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
U.	U.	kA
<g/>
S.	S.	kA
pilots	pilots	k1gInSc4
honored	honored	k1gInSc4
for	forum	k1gNnPc2
Indochina	Indochin	k1gMnSc2
Service	Service	k1gFnSc2
<g/>
,	,	kIx,
Seven	Seven	k2eAgInSc1d1
American	American	k1gInSc1
Pilots	Pilotsa	k1gFnPc2
were	were	k1gNnSc1
awarded	awarded	k1gMnSc1
the	the	k?
Legion	legion	k1gInSc1
of	of	k?
Honor	honor	k1gInSc1
<g/>
...	...	k?
<g/>
↑	↑	k?
Lam	lama	k1gFnPc2
Quang	Quang	k1gMnSc1
Thi	Thi	k1gMnSc1
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Wiest	Wiest	k1gMnSc1
Hell	Hell	k1gMnSc1
in	in	k?
An	An	k1gMnSc1
Loc	Loc	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
1972	#num#	k4
Easter	Easter	k1gInSc1
Invasion	Invasion	k1gInSc4
<g/>
,	,	kIx,
University	universita	k1gFnPc4
of	of	k?
North	North	k1gInSc1
Texas	Texas	k1gInSc1
Press	Press	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p.	p.	k?
14	#num#	k4
<g/>
↑	↑	k?
Lam	lama	k1gFnPc2
Quang	Quang	k1gMnSc1
Thi	Thi	k1gMnSc1
<g/>
,	,	kIx,
p.	p.	k?
14	#num#	k4
<g/>
↑	↑	k?
Tragic	Tragic	k1gMnSc1
Mountains	Mountains	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Hmong	Hmong	k1gMnSc1
<g/>
,	,	kIx,
the	the	k?
Americans	Americans	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
Secret	Secret	k1gInSc1
Wars	Warsa	k1gFnPc2
for	forum	k1gNnPc2
Laos	Laos	k1gInSc1
<g/>
,	,	kIx,
trang	trang	k1gInSc1
62	#num#	k4
<g/>
,	,	kIx,
Ipossitionndiana	Ipossitionndiana	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
↑	↑	k?
Tổ	Tổ	k1gMnSc1
thố	thố	k1gMnSc1
Pháp	Pháp	k1gMnSc1
Jacques	Jacques	k1gMnSc1
Chirac	Chirac	k1gMnSc1
ca	ca	kA
ngợ	ngợ	k1gFnPc1
lò	lò	k1gMnSc1
dũ	dũ	k1gMnSc1
cả	cả	k1gMnSc1
củ	củ	k1gMnSc1
các	các	k?
binh	binh	k1gMnSc1
sĩ	sĩ	k?
Pháp	Pháp	k1gMnSc1
trong	trong	k1gMnSc1
trậ	trậ	k1gMnSc1
chiế	chiế	k1gMnSc1
Điệ	Điệ	k1gMnSc1
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voanews	Voanews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ban	Ban	k1gMnSc1
tổ	tổ	k1gMnSc1
kế	kế	k1gMnSc1
soạ	soạ	k1gMnSc1
lị	lị	k1gMnSc1
sử	sử	k?
<g/>
,	,	kIx,
BTTM	BTTM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lị	Lị	k1gMnSc1
sử	sử	k?
Bộ	Bộ	k1gMnSc1
Tổ	Tổ	k1gMnSc1
tham	tham	k1gMnSc1
mư	mư	k5eAaPmIp1nS
trong	trong	k1gMnSc1
kháng	kháng	k1gMnSc1
chiế	chiế	k1gMnSc1
chố	chố	k1gMnSc1
Pháp	Pháp	k1gMnSc1
1945	#num#	k4
<g/>
-	-	kIx~
<g/>
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ha	ha	kA
Noi	Noi	k1gMnSc1
<g/>
:	:	kIx,
Nhà	Nhà	k1gMnSc1
xuấ	xuấ	k1gMnSc1
bả	bả	k1gMnSc1
Quân	Quân	k1gMnSc1
Độ	Độ	k1gFnSc2
Nhân	Nhân	k1gMnSc1
Dân	Dân	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
799	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
History	Histor	k1gInPc1
Study	stud	k1gInPc4
Board	Board	k1gMnSc1
of	of	k?
The	The	k1gMnSc1
General	General	k1gMnSc1
Staff	Staff	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
History	Histor	k1gInPc7
of	of	k?
the	the	k?
General	General	k1gMnSc1
Staff	Staff	k1gMnSc1
in	in	k?
the	the	k?
Resistance	Resistance	k1gFnSc2
War	War	k1gMnSc1
against	against	k1gMnSc1
the	the	k?
French	French	k1gMnSc1
1945	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ha	ha	kA
Noi	Noi	k1gMnSc1
<g/>
:	:	kIx,
People	People	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Army	Arm	k1gMnPc7
Publishing	Publishing	k1gInSc4
House	house	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
799	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Vietnamese	Vietnamese	k1gFnSc1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Stone	ston	k1gInSc5
<g/>
,	,	kIx,
p.	p.	k?
109	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Válka	válka	k1gFnSc1
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
</s>
<s>
Việ	Việ	k2eAgInSc1d1
Minh	Minh	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bitva	bitva	k1gFnSc1
u	u	k7c2
Điệ	Điệ	k1gFnPc2
Biê	Biê	k1gFnPc2
Phủ	Phủ	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dien	dien	k1gInSc4
Bien	biena	k1gFnPc2
Phu	Phu	k1gFnSc2
den	den	k1gInSc1
po	po	k7c6
dni	den	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
117506	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85037800	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85037800	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Vietnam	Vietnam	k1gInSc1
</s>
