<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dien	dien	k1gNnSc2
Bien	Bien	k1gNnSc2
Phu	Phu	k1gNnSc2
(	(	kIx(
<g/>
čti	číst	k5eAaImRp2nS
bitva	bitva	k1gFnSc1
u	u	k7c2
Dien	dien	k1gNnSc2
Bien	Bien	k1gNnSc2
Fu	fu	k1gNnSc2
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
bataille	bataille	k6eAd1
de	de	k?
Diê	Diê	k1gMnSc1
Biê	Biê	k1gMnSc1
Phu	Phu	k1gMnSc1
<g/>
,	,	kIx,
vietnamsky	vietnamsky	k6eAd1
chiế	chiế	k1gMnSc1
dị	dị	k1gMnSc1
Điệ	Điệ	k1gMnSc1
Biê	Biê	k1gMnSc1
Phủ	Phủ	k1gMnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
bitva	bitva	k1gFnSc1
mezi	mezi	k7c7
francouzskými	francouzský	k2eAgNnPc7d1
koloniálními	koloniální	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
v	v	k7c6
Indočíně	Indočína	k1gFnSc6
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
a	a	k8xC
silami	síla	k1gFnPc7
komunistického	komunistický	k2eAgInSc2d1
Viet	Viet	k1gInSc4
Minhu	Minh	k1gInSc2
<g/>
,	,	kIx,
podporovaných	podporovaný	k2eAgInPc2d1
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
a	a	k8xC
komunistickou	komunistický	k2eAgFnSc7d1
Čínou	Čína	k1gFnSc7
na	na	k7c6
straně	strana	k1gFnSc6
druhé	druhý	k4xOgFnSc6
<g/>
.	.	kIx.
</s>