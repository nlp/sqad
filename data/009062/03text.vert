<p>
<s>
Elf	elf	k1gMnSc1	elf
je	být	k5eAaImIp3nS	být
bytost	bytost	k1gFnSc4	bytost
z	z	k7c2	z
germánské	germánský	k2eAgFnSc2d1	germánská
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
folklóru	folklór	k1gInSc2	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejznámější	známý	k2eAgFnSc7d3	nejznámější
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
severský	severský	k2eAgInSc4d1	severský
álfr	álfr	k1gInSc4	álfr
<g/>
,	,	kIx,	,
v	v	k7c4	v
plurál	plurál	k1gInSc4	plurál
álfar	álfara	k1gFnPc2	álfara
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgInSc1d1	německý
alp	alpa	k1gFnPc2	alpa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
christianizace	christianizace	k1gFnSc2	christianizace
byli	být	k5eAaImAgMnP	být
elfové	elf	k1gMnPc1	elf
zaměňováni	zaměňován	k2eAgMnPc1d1	zaměňován
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
bytostmi	bytost	k1gFnPc7	bytost
nižšími	nízký	k2eAgFnPc7d2	nižší
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
trpaslíci	trpaslík	k1gMnPc1	trpaslík
nebo	nebo	k8xC	nebo
noční	noční	k2eAgFnPc1d1	noční
můry	můra	k1gFnPc1	můra
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Tolkienova	Tolkienův	k2eAgMnSc2d1	Tolkienův
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
a	a	k8xC	a
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazující	navazující	k2eAgInPc1d1	navazující
fantasy	fantas	k1gInPc1	fantas
literaturu	literatura	k1gFnSc4	literatura
se	se	k3xPyFc4	se
však	však	k9	však
prosadil	prosadit	k5eAaPmAgInS	prosadit
obraz	obraz	k1gInSc1	obraz
elfů	elf	k1gMnPc2	elf
jako	jako	k8xC	jako
dobrotivých	dobrotivý	k2eAgFnPc2d1	dobrotivá
bytostí	bytost	k1gFnPc2	bytost
lidské	lidský	k2eAgFnSc2d1	lidská
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
spjatých	spjatý	k2eAgInPc2d1	spjatý
s	s	k7c7	s
magií	magie	k1gFnSc7	magie
a	a	k8xC	a
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglosaském	anglosaský	k2eAgInSc6d1	anglosaský
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
obraz	obraz	k1gInSc1	obraz
elfů	elf	k1gMnPc2	elf
jako	jako	k8xC	jako
malých	malý	k2eAgFnPc2d1	malá
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vánočních	vánoční	k2eAgMnPc2d1	vánoční
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
tyto	tento	k3xDgFnPc4	tento
bytosti	bytost	k1gFnPc4	bytost
nejsou	být	k5eNaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
překládáni	překládán	k2eAgMnPc1d1	překládán
jako	jako	k8xS	jako
elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
skřítci	skřítek	k1gMnPc1	skřítek
či	či	k8xC	či
víly	víla	k1gFnPc1	víla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
pragermánského	pragermánský	k2eAgMnSc2d1	pragermánský
*	*	kIx~	*
<g/>
albiz	albiz	k1gInSc1	albiz
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
zase	zase	k9	zase
od	od	k7c2	od
praindoevropského	praindoevropský	k2eAgMnSc2d1	praindoevropský
*	*	kIx~	*
<g/>
albhós	albhós	k1gInSc1	albhós
či	či	k8xC	či
*	*	kIx~	*
<g/>
helbhós	helbhós	k6eAd1	helbhós
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
kořene	kořen	k1gInSc2	kořen
můžou	můžou	k?	můžou
pocházet	pocházet	k5eAaImF	pocházet
i	i	k9	i
geografická	geografický	k2eAgNnPc1d1	geografické
jména	jméno	k1gNnPc1	jméno
Alpy	alpa	k1gFnSc2	alpa
a	a	k8xC	a
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
této	tento	k3xDgFnSc2	tento
etymologie	etymologie	k1gFnSc2	etymologie
položil	položit	k5eAaPmAgMnS	položit
již	již	k9	již
Jacob	Jacoba	k1gFnPc2	Jacoba
Grimm	Grimm	k1gMnSc1	Grimm
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussare	Saussar	k1gInSc5	Saussar
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
indoevropský	indoevropský	k2eAgInSc4d1	indoevropský
kořen	kořen	k1gInSc4	kořen
pak	pak	k6eAd1	pak
odvodil	odvodit	k5eAaPmAgMnS	odvodit
Elis	Elis	k1gFnSc3	Elis
Wadstein	Wadstein	k1gMnSc1	Wadstein
<g/>
.	.	kIx.	.
<g/>
Adalbert	Adalbert	k1gMnSc1	Adalbert
Kuhn	Kuhn	k1gMnSc1	Kuhn
spojil	spojit	k5eAaPmAgMnS	spojit
slovo	slovo	k1gNnSc4	slovo
elf	elf	k1gMnSc1	elf
se	s	k7c7	s
sanskrtským	sanskrtský	k2eAgInSc7d1	sanskrtský
rbhu	rbhus	k1gInSc6	rbhus
"	"	kIx"	"
<g/>
obratný	obratný	k2eAgMnSc1d1	obratný
řemeslník	řemeslník	k1gMnSc1	řemeslník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
označovalo	označovat	k5eAaImAgNnS	označovat
démony	démon	k1gMnPc4	démon
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vykovali	vykovat	k5eAaPmAgMnP	vykovat
bohům	bůh	k1gMnPc3	bůh
šperky	šperk	k1gInPc1	šperk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
etymologii	etymologie	k1gFnSc3	etymologie
se	se	k3xPyFc4	se
přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
de	de	k?	de
Vries	Vries	k1gInSc1	Vries
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
však	však	k9	však
že	že	k8xS	že
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
záměny	záměna	k1gFnSc2	záměna
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
s	s	k7c7	s
trpaslíky	trpaslík	k1gMnPc7	trpaslík
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
proběhla	proběhnout	k5eAaPmAgNnP	proběhnout
až	až	k6eAd1	až
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
důkazem	důkaz	k1gInSc7	důkaz
o	o	k7c6	o
germánské	germánský	k2eAgFnSc6d1	germánská
víře	víra	k1gFnSc6	víra
v	v	k7c4	v
elfy	elf	k1gMnPc4	elf
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jméno	jméno	k1gNnSc4	jméno
věštkyně	věštkyně	k1gFnSc2	věštkyně
Albruny	Albrun	k1gInPc1	Albrun
zmiňované	zmiňovaný	k2eAgInPc1d1	zmiňovaný
Tacitem	Tacit	k1gInSc7	Tacit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
elfí	elfí	k2eAgNnPc4d1	elfí
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
s.	s.	k?	s.
91	[number]	k4	91
<g/>
)	)	kIx)	)
V	v	k7c6	v
germánských	germánský	k2eAgInPc6d1	germánský
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
elf	elf	k1gMnSc1	elf
začal	začít	k5eAaPmAgMnS	začít
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
bytostmi	bytost	k1gFnPc7	bytost
nižší	nízký	k2eAgFnSc2d2	nižší
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
především	především	k9	především
trpaslíky	trpaslík	k1gMnPc4	trpaslík
a	a	k8xC	a
můrami	můra	k1gFnPc7	můra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
tato	tento	k3xDgFnSc1	tento
proměna	proměna	k1gFnSc1	proměna
počala	počnout	k5eAaPmAgFnS	počnout
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
elf	elf	k1gMnSc1	elf
objevuje	objevovat	k5eAaImIp3nS	objevovat
až	až	k9	až
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xC	jako
výpůjčka	výpůjčka	k1gFnSc1	výpůjčka
z	z	k7c2	z
germánských	germánský	k2eAgInPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
se	se	k3xPyFc4	se
až	až	k9	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
bytost	bytost	k1gFnSc4	bytost
podobnou	podobný	k2eAgFnSc4d1	podobná
trpaslíkovi	trpaslíkův	k2eAgMnPc1d1	trpaslíkův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povaha	povaha	k1gFnSc1	povaha
==	==	k?	==
</s>
</p>
<p>
<s>
Elfové	elf	k1gMnPc1	elf
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
dobrotivými	dobrotivý	k2eAgFnPc7d1	dobrotivá
bytostmi	bytost	k1gFnPc7	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
řada	řada	k1gFnSc1	řada
osobních	osobní	k2eAgNnPc2d1	osobní
jmen	jméno	k1gNnPc2	jméno
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
odvozených	odvozený	k2eAgMnPc2d1	odvozený
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
svému	svůj	k3xOyFgMnSc3	svůj
nositeli	nositel	k1gMnSc3	nositel
zajistit	zajistit	k5eAaPmF	zajistit
jejich	jejich	k3xOp3gFnSc4	jejich
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
staroanglická	staroanglický	k2eAgNnPc4d1	staroanglické
jména	jméno	k1gNnPc4	jméno
Aelfraed	Aelfraed	k1gInSc1	Aelfraed
"	"	kIx"	"
<g/>
rádce	rádce	k1gMnSc1	rádce
elfů	elf	k1gMnPc2	elf
<g/>
"	"	kIx"	"
Aelfbeorth	Aelfbeorth	k1gMnSc1	Aelfbeorth
"	"	kIx"	"
<g/>
třpytící	třpytící	k2eAgMnSc1d1	třpytící
se	se	k3xPyFc4	se
elf	elf	k1gMnSc1	elf
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
Aelfwine	Aelfwin	k1gInSc5	Aelfwin
"	"	kIx"	"
<g/>
přítel	přítel	k1gMnSc1	přítel
elfů	elf	k1gMnPc2	elf
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInSc4d1	další
taková	takový	k3xDgNnPc1	takový
jména	jméno	k1gNnPc4	jméno
patří	patřit	k5eAaImIp3nP	patřit
Alf	alfa	k1gFnPc2	alfa
<g/>
,	,	kIx,	,
Alva	Alvum	k1gNnSc2	Alvum
a	a	k8xC	a
Alberich	Albericha	k1gFnPc2	Albericha
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
účel	účel	k1gInSc4	účel
měla	mít	k5eAaImAgNnP	mít
jména	jméno	k1gNnPc1	jméno
odvozená	odvozený	k2eAgNnPc1d1	odvozené
od	od	k7c2	od
jmen	jméno	k1gNnPc2	jméno
bohů	bůh	k1gMnPc2	bůh
jako	jako	k8xS	jako
Thorsteinn	Thorsteinna	k1gFnPc2	Thorsteinna
"	"	kIx"	"
<g/>
Thórův	Thórův	k2eAgInSc1d1	Thórův
kámen	kámen	k1gInSc1	kámen
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
Frómundr	Frómundr	k1gInSc1	Frómundr
"	"	kIx"	"
<g/>
Freyem	Freyem	k1gInSc1	Freyem
ochraňovaný	ochraňovaný	k2eAgInSc1d1	ochraňovaný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
například	například	k6eAd1	například
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
dvergr	dvergr	k1gInSc1	dvergr
"	"	kIx"	"
<g/>
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
"	"	kIx"	"
žádná	žádný	k3yNgNnPc4	žádný
známá	známý	k2eAgNnPc4d1	známé
osobní	osobní	k2eAgNnPc4d1	osobní
jména	jméno	k1gNnPc4	jméno
odvozena	odvozen	k2eAgMnSc4d1	odvozen
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
o	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
bytostech	bytost	k1gFnPc6	bytost
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
věřilo	věřit	k5eAaImAgNnS	věřit
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
zlovolné	zlovolný	k2eAgFnPc1d1	zlovolná
povahy	povaha	k1gFnPc1	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
charakter	charakter	k1gInSc4	charakter
elfů	elf	k1gMnPc2	elf
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
i	i	k9	i
samotná	samotný	k2eAgFnSc1d1	samotná
etymologie	etymologie	k1gFnSc1	etymologie
jejich	jejich	k3xOp3gNnSc2	jejich
jména	jméno	k1gNnSc2	jméno
od	od	k7c2	od
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
<g/>
Elfové	elf	k1gMnPc1	elf
byly	být	k5eAaImAgFnP	být
spojeni	spojen	k2eAgMnPc1d1	spojen
s	s	k7c7	s
magií	magie	k1gFnSc7	magie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
škodlivou	škodlivý	k2eAgFnSc7d1	škodlivá
magií	magie	k1gFnSc7	magie
<g/>
.	.	kIx.	.
</s>
<s>
Staroanglický	staroanglický	k2eAgInSc1d1	staroanglický
výraz	výraz	k1gInSc1	výraz
aelfside	aelfsid	k1gInSc5	aelfsid
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
"	"	kIx"	"
<g/>
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
<g/>
,	,	kIx,	,
uhranutí	uhranutí	k1gNnSc1	uhranutí
<g/>
"	"	kIx"	"
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
elfí	elfí	k1gNnSc1	elfí
kouzla	kouzlo	k1gNnSc2	kouzlo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
složka	složka	k1gFnSc1	složka
siden	sidna	k1gFnPc2	sidna
je	být	k5eAaImIp3nS	být
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
severskému	severský	k2eAgNnSc3d1	severské
seid	seid	k1gMnSc1	seid
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
slova	slovo	k1gNnPc4	slovo
spojující	spojující	k2eAgFnSc2d1	spojující
magii	magie	k1gFnSc3	magie
a	a	k8xC	a
elfy	elf	k1gMnPc4	elf
patří	patřit	k5eAaImIp3nS	patřit
staroanglické	staroanglický	k2eAgNnSc4d1	staroanglické
aelfadl	aelfadnout	k5eAaPmAgMnS	aelfadnout
"	"	kIx"	"
<g/>
noční	noční	k2eAgFnSc1d1	noční
můra	můra	k1gFnSc1	můra
<g/>
"	"	kIx"	"
a	a	k8xC	a
aelfsogotha	aelfsogotha	k1gFnSc1	aelfsogotha
"	"	kIx"	"
<g/>
škytavka	škytavka	k1gFnSc1	škytavka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
německé	německý	k2eAgInPc4d1	německý
Alpfuss	Alpfuss	k1gInSc4	Alpfuss
"	"	kIx"	"
<g/>
pentagram	pentagram	k1gInSc1	pentagram
<g/>
,	,	kIx,	,
muří	muří	k2eAgFnSc1d1	muří
noha	noha	k1gFnSc1	noha
<g/>
"	"	kIx"	"
a	a	k8xC	a
Alraun	Alraun	k1gInSc1	Alraun
či	či	k8xC	či
Albruna	Albruna	k1gFnSc1	Albruna
"	"	kIx"	"
<g/>
mandragora	mandragora	k1gFnSc1	mandragora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
kultu	kult	k1gInSc6	kult
elfů	elf	k1gMnPc2	elf
svědčí	svědčit	k5eAaImIp3nS	svědčit
existence	existence	k1gFnSc1	existence
rodinného	rodinný	k2eAgInSc2d1	rodinný
svátku	svátek	k1gInSc2	svátek
Álfablót	Álfablót	k1gInSc1	Álfablót
"	"	kIx"	"
<g/>
elfí	elfí	k2eAgNnPc1d1	elfí
obětování	obětování	k1gNnPc1	obětování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
slaven	slavit	k5eAaImNgInS	slavit
předkřesťanskými	předkřesťanský	k2eAgMnPc7d1	předkřesťanský
Seveřany	Seveřan	k1gMnPc7	Seveřan
<g/>
.	.	kIx.	.
</s>
<s>
Souvislost	souvislost	k1gFnSc1	souvislost
této	tento	k3xDgFnSc2	tento
slavnosti	slavnost	k1gFnSc2	slavnost
s	s	k7c7	s
kultem	kult	k1gInSc7	kult
předků	předek	k1gMnPc2	předek
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
že	že	k8xS	že
se	se	k3xPyFc4	se
zemřelými	zemřelý	k1gMnPc7	zemřelý
byli	být	k5eAaImAgMnP	být
spjati	spjat	k2eAgMnPc1d1	spjat
i	i	k9	i
sami	sám	k3xTgMnPc1	sám
elfové	elf	k1gMnPc1	elf
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Hálfdan	Hálfdan	k1gMnSc1	Hálfdan
Hvítbeinn	Hvítbeinn	k1gMnSc1	Hvítbeinn
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
nazývani	nazývaň	k1gFnSc6	nazývaň
elfy	elf	k1gMnPc4	elf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Clauda	Claudo	k1gNnSc2	Claudo
Lecouteuxe	Lecouteuxe	k1gFnSc2	Lecouteuxe
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
elfové	elf	k1gMnPc1	elf
nižšími	nízký	k2eAgNnPc7d2	nižší
božstvy	božstvo	k1gNnPc7	božstvo
plodnosti	plodnost	k1gFnSc2	plodnost
a	a	k8xC	a
úrody	úroda	k1gFnSc2	úroda
<g/>
,	,	kIx,	,
spjatými	spjatý	k2eAgMnPc7d1	spjatý
s	s	k7c7	s
dobrotivými	dobrotivý	k2eAgMnPc7d1	dobrotivý
předky	předek	k1gMnPc7	předek
a	a	k8xC	a
bohy	bůh	k1gMnPc7	bůh
Freyem	Frey	k1gMnSc7	Frey
a	a	k8xC	a
Thórem	Thór	k1gMnSc7	Thór
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přebývají	přebývat	k5eAaImIp3nP	přebývat
v	v	k7c6	v
Álfheimu	Álfheim	k1gInSc6	Álfheim
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
elfy	elf	k1gMnPc7	elf
zaměňovaní	zaměňovaný	k2eAgMnPc1d1	zaměňovaný
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
jejich	jejich	k3xOp3gFnSc7	jejich
zlovolnou	zlovolný	k2eAgFnSc7d1	zlovolná
analogií	analogie	k1gFnSc7	analogie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elfové	elf	k1gMnPc1	elf
v	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
literatuře	literatura	k1gFnSc6	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
renesanční	renesanční	k2eAgFnSc6d1	renesanční
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
elfové	elf	k1gMnPc1	elf
jako	jako	k8xS	jako
nejen	nejen	k6eAd1	nejen
druh	druh	k1gInSc1	druh
víl	víla	k1gFnPc2	víla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
synonymum	synonymum	k1gNnSc1	synonymum
slova	slovo	k1gNnSc2	slovo
víla	víla	k1gFnSc1	víla
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
(	(	kIx(	(
<g/>
a	a	k8xC	a
možná	možná	k9	možná
nejstarším	starý	k2eAgInPc3d3	nejstarší
<g/>
)	)	kIx)	)
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
pohledem	pohled	k1gInSc7	pohled
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
Sen	sen	k1gInSc4	sen
noci	noc	k1gFnSc2	noc
svatojánské	svatojánský	k2eAgNnSc1d1	svatojánské
Williama	Williama	k1gNnSc1	Williama
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
:	:	kIx,	:
termín	termín	k1gInSc1	termín
elf	elf	k1gMnSc1	elf
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
bytosti	bytost	k1gFnPc4	bytost
téměř	téměř	k6eAd1	téměř
velikosti	velikost	k1gFnPc4	velikost
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
autory	autor	k1gMnPc4	autor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
takto	takto	k6eAd1	takto
popisovali	popisovat	k5eAaImAgMnP	popisovat
elfy	elf	k1gMnPc7	elf
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Michael	Michael	k1gMnSc1	Michael
Drayton	Drayton	k1gInSc1	Drayton
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
Edmund	Edmund	k1gMnSc1	Edmund
Spenser	Spenser	k1gMnSc1	Spenser
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc4	termín
elf	elf	k1gMnSc1	elf
pro	pro	k7c4	pro
bytosti	bytost	k1gFnPc4	bytost
lidské	lidský	k2eAgFnSc2d1	lidská
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Elfové	elf	k1gMnPc1	elf
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
literatuře	literatura	k1gFnSc6	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Postavy	postava	k1gFnPc1	postava
elfů	elf	k1gMnPc2	elf
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
fantasy	fantas	k1gInPc1	fantas
literatuře	literatura	k1gFnSc3	literatura
a	a	k8xC	a
jiných	jiný	k2eAgNnPc6d1	jiné
fantasy	fantas	k1gInPc1	fantas
dílech	díl	k1gInPc6	díl
včetně	včetně	k7c2	včetně
her	hra	k1gFnPc2	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
typologie	typologie	k1gFnSc1	typologie
bývá	bývat	k5eAaImIp3nS	bývat
podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k9	jako
u	u	k7c2	u
mytologických	mytologický	k2eAgMnPc2d1	mytologický
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
díle	dílo	k1gNnSc6	dílo
resp.	resp.	kA	resp.
fantasy	fantas	k1gInPc1	fantas
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Elfové	elf	k1gMnPc1	elf
jsou	být	k5eAaImIp3nP	být
známi	znám	k2eAgMnPc1d1	znám
zejména	zejména	k9	zejména
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkiena	Tolkien	k1gMnSc2	Tolkien
<g/>
,	,	kIx,	,
Raimonda	Raimond	k1gMnSc2	Raimond
E.	E.	kA	E.
Feista	Feist	k1gMnSc2	Feist
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Sapkowského	Sapkowský	k1gMnSc2	Sapkowský
<g/>
,	,	kIx,	,
Poula	Poul	k1gMnSc2	Poul
Andersona	Anderson	k1gMnSc2	Anderson
nebo	nebo	k8xC	nebo
Christophera	Christopher	k1gMnSc2	Christopher
Paoliniho	Paolini	k1gMnSc2	Paolini
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
elfové	elf	k1gMnPc1	elf
podobní	podobný	k2eAgMnPc1d1	podobný
mytologickým	mytologický	k2eAgMnPc3d1	mytologický
elfům	elf	k1gMnPc3	elf
světlým	světlý	k2eAgMnPc3d1	světlý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
o	o	k7c6	o
bytosti	bytost	k1gFnSc6	bytost
vysokého	vysoký	k2eAgInSc2d1	vysoký
štíhlého	štíhlý	k2eAgInSc2d1	štíhlý
vzrůstu	vzrůst	k1gInSc2	vzrůst
s	s	k7c7	s
jemnými	jemný	k2eAgInPc7d1	jemný
rysy	rys	k1gInPc7	rys
obličeje	obličej	k1gInSc2	obličej
a	a	k8xC	a
špičatýma	špičatý	k2eAgNnPc7d1	špičaté
ušima	ucho	k1gNnPc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Špičatost	špičatost	k1gFnSc1	špičatost
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
lehké	lehký	k2eAgFnSc2d1	lehká
(	(	kIx(	(
<g/>
tvar	tvar	k1gInSc4	tvar
zhruba	zhruba	k6eAd1	zhruba
jako	jako	k8xS	jako
polovina	polovina	k1gFnSc1	polovina
lipového	lipový	k2eAgInSc2d1	lipový
listu	list	k1gInSc2	list
<g/>
)	)	kIx)	)
u	u	k7c2	u
Tolkiena	Tolkieno	k1gNnSc2	Tolkieno
až	až	k9	až
po	po	k7c4	po
uši	ucho	k1gNnPc4	ucho
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
jako	jako	k8xC	jako
králičí	králičí	k2eAgFnPc1d1	králičí
například	například	k6eAd1	například
v	v	k7c4	v
japonských	japonský	k2eAgInPc2d1	japonský
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
elfů	elf	k1gMnPc2	elf
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
výrazně	výrazně	k6eAd1	výrazně
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
lidská	lidský	k2eAgFnSc1d1	lidská
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
od	od	k7c2	od
nestárnoucích	stárnoucí	k2eNgFnPc2d1	nestárnoucí
Tolkienových	Tolkienová	k1gFnPc2	Tolkienová
až	až	k9	až
k	k	k7c3	k
"	"	kIx"	"
<g/>
pouhým	pouhý	k2eAgFnPc3d1	pouhá
<g/>
"	"	kIx"	"
několika	několik	k4yIc3	několik
staletím	staletí	k1gNnPc3	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Elfové	elf	k1gMnPc1	elf
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
velkou	velký	k2eAgFnSc7d1	velká
moudrostí	moudrost	k1gFnSc7	moudrost
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
přírodu	příroda	k1gFnSc4	příroda
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bydlí	bydlet	k5eAaImIp3nS	bydlet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
lese	les	k1gInSc6	les
(	(	kIx(	(
<g/>
v	v	k7c6	v
domech	dům	k1gInPc6	dům
postavených	postavený	k2eAgInPc6d1	postavený
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
zbraní	zbraň	k1gFnSc7	zbraň
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
luk	luk	k1gInSc4	luk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elfové	elf	k1gMnPc1	elf
z	z	k7c2	z
fantasy	fantas	k1gInPc1	fantas
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
běžnou	běžný	k2eAgFnSc7d1	běžná
rasou	rasa	k1gFnSc7	rasa
v	v	k7c6	v
hrách	hra	k1gFnPc6	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc4	Dragons
nebo	nebo	k8xC	nebo
české	český	k2eAgNnSc4d1	české
Dračí	dračí	k2eAgNnSc4d1	dračí
doupě	doupě	k1gNnSc4	doupě
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
počítačových	počítačový	k2eAgFnPc6d1	počítačová
hrách	hra	k1gFnPc6	hra
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dračím	dračí	k2eAgNnSc6d1	dračí
doupěti	doupě	k1gNnSc6	doupě
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
zjevně	zjevně	k6eAd1	zjevně
o	o	k7c4	o
elfy	elf	k1gMnPc4	elf
zelené	zelený	k2eAgInPc1d1	zelený
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Tolkienova	Tolkienův	k2eAgNnSc2d1	Tolkienovo
dělení	dělení	k1gNnSc2	dělení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tolkienovi	Tolkienův	k2eAgMnPc1d1	Tolkienův
elfové	elf	k1gMnPc1	elf
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Tolkienově	Tolkienův	k2eAgFnSc6d1	Tolkienova
Středozemi	Středozem	k1gFnSc6	Středozem
jsou	být	k5eAaImIp3nP	být
elfové	elf	k1gMnPc1	elf
(	(	kIx(	(
<g/>
Quendi	Quend	k1gMnPc1	Quend
<g/>
)	)	kIx)	)
ušlechtilé	ušlechtilý	k2eAgFnPc1d1	ušlechtilá
nesmrtelné	smrtelný	k2eNgFnPc1d1	nesmrtelná
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
vládnou	vládnout	k5eAaImIp3nP	vládnout
magií	magie	k1gFnPc2	magie
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
první	první	k4xOgMnPc1	první
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
osídlili	osídlit	k5eAaPmAgMnP	osídlit
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
je	on	k3xPp3gMnPc4	on
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
Vznešené	vznešený	k2eAgMnPc4d1	vznešený
(	(	kIx(	(
<g/>
Calaquendi	Calaquend	k1gMnPc1	Calaquend
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Vanyar	Vanyar	k1gInSc4	Vanyar
<g/>
,	,	kIx,	,
Noldor	Noldor	k1gInSc4	Noldor
a	a	k8xC	a
část	část	k1gFnSc4	část
Teleri	Teler	k1gFnSc2	Teler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šedé	šedý	k2eAgInPc1d1	šedý
(	(	kIx(	(
<g/>
Sindar	Sindar	k1gInSc1	Sindar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zelené	zelené	k1gNnSc1	zelené
(	(	kIx(	(
<g/>
Laiquendi	Laiquend	k1gMnPc1	Laiquend
<g/>
)	)	kIx)	)
a	a	k8xC	a
temné	temný	k2eAgFnPc1d1	temná
(	(	kIx(	(
<g/>
Moriquendi	Moriquend	k1gMnPc1	Moriquend
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paralelně	paralelně	k6eAd1	paralelně
Eldar	Eldar	k1gInSc1	Eldar
označuje	označovat	k5eAaImIp3nS	označovat
Vanyar	Vanyar	k1gInSc4	Vanyar
<g/>
,	,	kIx,	,
Noldor	Noldor	k1gInSc4	Noldor
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
Teleri	Teler	k1gFnPc4	Teler
a	a	k8xC	a
Avari	Avare	k1gFnSc4	Avare
ostatní	ostatní	k2eAgFnSc4d1	ostatní
(	(	kIx(	(
<g/>
podmnožinu	podmnožina	k1gFnSc4	podmnožina
Moriquendi	Moriquend	k1gMnPc1	Moriquend
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
se	s	k7c7	s
základním	základní	k2eAgNnSc7d1	základní
dělením	dělení	k1gNnSc7	dělení
mytologických	mytologický	k2eAgMnPc2d1	mytologický
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
Tolkienovi	Tolkienův	k2eAgMnPc1d1	Tolkienův
elfové	elf	k1gMnPc1	elf
jsou	být	k5eAaImIp3nP	být
stejný	stejný	k2eAgInSc4d1	stejný
druh	druh	k1gInSc4	druh
bytostí	bytost	k1gFnPc2	bytost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tolkienovo	Tolkienův	k2eAgNnSc1d1	Tolkienovo
rozdělení	rozdělení	k1gNnSc1	rozdělení
elfů	elf	k1gMnPc2	elf
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zachovali	zachovat	k5eAaPmAgMnP	zachovat
k	k	k7c3	k
nabídce	nabídka	k1gFnSc3	nabídka
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
Valinoru	Valinor	k1gInSc2	Valinor
–	–	k?	–
Vanyar	Vanyar	k1gMnSc1	Vanyar
do	do	k7c2	do
Valinoru	Valinor	k1gInSc2	Valinor
přišli	přijít	k5eAaPmAgMnP	přijít
a	a	k8xC	a
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
<g/>
,	,	kIx,	,
Noldor	Noldor	k1gMnSc1	Noldor
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
a	a	k8xC	a
přinesli	přinést	k5eAaPmAgMnP	přinést
do	do	k7c2	do
Středozemě	Středozem	k1gFnSc2	Středozem
moudrost	moudrost	k1gFnSc4	moudrost
a	a	k8xC	a
magii	magie	k1gFnSc4	magie
Valinoru	Valinor	k1gInSc2	Valinor
<g/>
,	,	kIx,	,
Teleri	Teler	k1gMnPc1	Teler
zčásti	zčásti	k6eAd1	zčásti
došli	dojít	k5eAaPmAgMnP	dojít
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
–	–	k?	–
Sindar	Sindar	k1gMnSc1	Sindar
<g/>
,	,	kIx,	,
Laiquendi	Laiquend	k1gMnPc1	Laiquend
a	a	k8xC	a
Nandor	Nandor	k1gInSc1	Nandor
–	–	k?	–
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřekročili	překročit	k5eNaPmAgMnP	překročit
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Sindar	Sindar	k1gInSc1	Sindar
a	a	k8xC	a
Laiquendi	Laiquend	k1gMnPc1	Laiquend
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
Beleriandu	Beleriand	k1gInSc6	Beleriand
<g/>
,	,	kIx,	,
Nandor	Nandor	k1gInSc1	Nandor
zůstali	zůstat	k5eAaPmAgMnP	zůstat
východně	východně	k6eAd1	východně
od	od	k7c2	od
Mlžných	mlžný	k2eAgFnPc2d1	mlžná
hor.	hor.	k?	hor.
Avari	Avar	k1gMnPc1	Avar
pozvání	pozvání	k1gNnSc2	pozvání
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Tolkienově	Tolkienův	k2eAgNnSc6d1	Tolkienovo
díle	dílo	k1gNnSc6	dílo
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
jen	jen	k9	jen
málo	málo	k6eAd1	málo
podkladů	podklad	k1gInPc2	podklad
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
konkrétních	konkrétní	k2eAgMnPc2d1	konkrétní
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
odvozených	odvozený	k2eAgInPc2d1	odvozený
jsou	být	k5eAaImIp3nP	být
Vznešení	vznešený	k2eAgMnPc1d1	vznešený
elfové	elf	k1gMnPc1	elf
popisovaní	popisovaný	k2eAgMnPc1d1	popisovaný
(	(	kIx(	(
<g/>
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
ostatními	ostatní	k2eAgFnPc7d1	ostatní
postavami	postava	k1gFnPc7	postava
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
arogantní	arogantní	k2eAgMnPc1d1	arogantní
se	se	k3xPyFc4	se
syndromem	syndrom	k1gInSc7	syndrom
nadřazenosti	nadřazenost	k1gFnSc2	nadřazenost
proti	proti	k7c3	proti
nejen	nejen	k6eAd1	nejen
lidem	člověk	k1gMnPc3	člověk
ale	ale	k9	ale
i	i	k8xC	i
ostatním	ostatní	k2eAgMnPc3d1	ostatní
elfům	elf	k1gMnPc3	elf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sapkowského	Sapkowského	k2eAgMnPc1d1	Sapkowského
elfové	elf	k1gMnPc1	elf
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Sapkowského	Sapkowské	k1gNnSc2	Sapkowské
jsou	být	k5eAaImIp3nP	být
elfové	elf	k1gMnPc1	elf
mizejícím	mizející	k2eAgInSc7d1	mizející
zahořklým	zahořklý	k2eAgInSc7d1	zahořklý
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
spíše	spíše	k9	spíše
konfliktní	konfliktní	k2eAgInSc1d1	konfliktní
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Sapkowkovského	Sapkowkovský	k2eAgInSc2d1	Sapkowkovský
elfové	elf	k1gMnPc1	elf
jsou	být	k5eAaImIp3nP	být
příbuzní	příbuzný	k2eAgMnPc1d1	příbuzný
s	s	k7c7	s
dryádami	dryáda	k1gFnPc7	dryáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
mytologickém	mytologický	k2eAgNnSc6d1	mytologické
dělení	dělení	k1gNnSc6	dělení
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
ke	k	k7c3	k
světlým	světlý	k2eAgMnPc3d1	světlý
elfům	elf	k1gMnPc3	elf
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
trpaslíky	trpaslík	k1gMnPc4	trpaslík
a	a	k8xC	a
elfy	elf	k1gMnPc4	elf
nepanuje	panovat	k5eNaImIp3nS	panovat
taková	takový	k3xDgFnSc1	takový
nenávist	nenávist	k1gFnSc1	nenávist
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
rasy	rasa	k1gFnPc1	rasa
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nepříbuzné	příbuzný	k2eNgFnPc1d1	nepříbuzná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počítají	počítat	k5eAaImIp3nP	počítat
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
starší	starý	k2eAgFnSc6d2	starší
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
podléhají	podléhat	k5eAaImIp3nP	podléhat
nezadržitelné	zadržitelný	k2eNgFnSc3d1	nezadržitelná
záplavě	záplava	k1gFnSc3	záplava
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
staré	starý	k2eAgInPc4d1	starý
spory	spor	k1gInPc4	spor
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
otevřeně	otevřeně	k6eAd1	otevřeně
naráží	narážet	k5eAaPmIp3nS	narážet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
zatlačeny	zatlačit	k5eAaPmNgFnP	zatlačit
do	do	k7c2	do
pozadí	pozadí	k1gNnPc2	pozadí
aktuálním	aktuální	k2eAgMnSc7d1	aktuální
společným	společný	k2eAgMnSc7d1	společný
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Elfové	elf	k1gMnPc1	elf
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Warcraftu	Warcraft	k1gInSc2	Warcraft
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Warcraftu	Warcraft	k1gInSc2	Warcraft
představovali	představovat	k5eAaImAgMnP	představovat
elfové	elf	k1gMnPc1	elf
lid	lido	k1gNnPc2	lido
vysokých	vysoká	k1gFnPc2	vysoká
<g/>
,	,	kIx,	,
velekrásných	velekrásný	k2eAgFnPc2d1	velekrásný
a	a	k8xC	a
inteligentních	inteligentní	k2eAgFnPc2d1	inteligentní
bytostí	bytost	k1gFnPc2	bytost
vyznačující	vyznačující	k2eAgFnSc1d1	vyznačující
se	s	k7c7	s
svýma	svůj	k3xOyFgNnPc7	svůj
dlouhýma	dlouhý	k2eAgNnPc7d1	dlouhé
špičatýma	špičatý	k2eAgNnPc7d1	špičaté
ušima	ucho	k1gNnPc7	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
rasu	rasa	k1gFnSc4	rasa
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Kal	kalit	k5eAaImRp2nS	kalit
<g/>
'	'	kIx"	'
<g/>
dorei	doreit	k5eAaImRp2nS	doreit
(	(	kIx(	(
<g/>
noční	noční	k2eAgMnPc1d1	noční
elfové	elf	k1gMnPc1	elf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
a	a	k8xC	a
žila	žít	k5eAaImAgFnS	žít
původně	původně	k6eAd1	původně
při	při	k7c6	při
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
na	na	k7c6	na
tehdy	tehdy	k6eAd1	tehdy
jediném	jediný	k2eAgInSc6d1	jediný
kontinentě	kontinent	k1gInSc6	kontinent
zvaným	zvaný	k2eAgInSc7d1	zvaný
Kalimdor	Kalimdor	k1gInSc4	Kalimdor
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jezero	jezero	k1gNnSc1	jezero
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
Studna	studna	k1gFnSc1	studna
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
.	.	kIx.	.
</s>
<s>
Elfové	elf	k1gMnPc1	elf
byli	být	k5eAaImAgMnP	být
první	první	k4xOgFnPc4	první
bytosti	bytost	k1gFnPc4	bytost
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
magii	magie	k1gFnSc4	magie
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojovanou	spojovaný	k2eAgFnSc7d1	spojovaná
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Elfy	elf	k1gMnPc4	elf
si	se	k3xPyFc3	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
Cenarius	Cenarius	k1gMnSc1	Cenarius
<g/>
,	,	kIx,	,
mocný	mocný	k2eAgMnSc1d1	mocný
polobůh	polobůh	k1gMnSc1	polobůh
obývající	obývající	k2eAgFnSc2d1	obývající
zalesněné	zalesněný	k2eAgFnSc2d1	zalesněná
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
elfy	elf	k1gMnPc4	elf
učil	učit	k5eAaImAgMnS	učit
o	o	k7c4	o
uspořádání	uspořádání	k1gNnSc4	uspořádání
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Civilizace	civilizace	k1gFnSc1	civilizace
elfů	elf	k1gMnPc2	elf
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Azshara	Azshara	k1gFnSc1	Azshara
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Studny	studna	k1gFnSc2	studna
věčnosti	věčnost	k1gFnSc2	věčnost
velký	velký	k2eAgInSc4d1	velký
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
shromažďovala	shromažďovat	k5eAaImAgFnS	shromažďovat
své	svůj	k3xOyFgMnPc4	svůj
nejvěrnější	věrný	k2eAgMnPc4d3	nejvěrnější
služebníky	služebník	k1gMnPc4	služebník
(	(	kIx(	(
<g/>
nazývala	nazývat	k5eAaImAgFnS	nazývat
je	on	k3xPp3gNnPc4	on
Quel	Quel	k1gInSc1	Quel
<g/>
'	'	kIx"	'
<g/>
dorei	dorei	k1gNnSc1	dorei
neboli	neboli	k8xC	neboli
Urození	urození	k1gNnSc1	urození
<g/>
)	)	kIx)	)
a	a	k8xC	a
zasvěcovala	zasvěcovat	k5eAaImAgFnS	zasvěcovat
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
magii	magie	k1gFnSc6	magie
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
královna	královna	k1gFnSc1	královna
Azshara	Azshara	k1gFnSc1	Azshara
pod	pod	k7c7	pod
nekontrolovatelným	kontrolovatelný	k2eNgNnSc7d1	nekontrolovatelné
používáním	používání	k1gNnSc7	používání
magie	magie	k1gFnSc2	magie
zešílela	zešílet	k5eAaPmAgFnS	zešílet
a	a	k8xC	a
ona	onen	k3xDgFnSc1	onen
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
Urození	urození	k1gNnPc4	urození
přivolali	přivolat	k5eAaPmAgMnP	přivolat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
světa	svět	k1gInSc2	svět
Plamennou	plamenný	k2eAgFnSc4d1	plamenná
legii	legie	k1gFnSc4	legie
<g/>
,	,	kIx,	,
zhoubnou	zhoubný	k2eAgFnSc4d1	zhoubná
armádu	armáda	k1gFnSc4	armáda
démonů	démon	k1gMnPc2	démon
a	a	k8xC	a
přestože	přestože	k8xS	přestože
ji	on	k3xPp3gFnSc4	on
za	za	k7c2	za
obrovského	obrovský	k2eAgNnSc2d1	obrovské
úsilí	úsilí	k1gNnSc2	úsilí
elfové	elf	k1gMnPc1	elf
zahnali	zahnat	k5eAaPmAgMnP	zahnat
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
roztrhnutí	roztrhnutí	k1gNnSc3	roztrhnutí
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zničení	zničení	k1gNnSc1	zničení
Studny	studna	k1gFnSc2	studna
věčnosti	věčnost	k1gFnSc2	věčnost
(	(	kIx(	(
<g/>
dárkyně	dárkyně	k1gFnSc1	dárkyně
magické	magický	k2eAgFnSc2d1	magická
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
elfové	elf	k1gMnPc1	elf
se	se	k3xPyFc4	se
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
na	na	k7c4	na
Nagy	Nagy	k1gInPc4	Nagy
<g/>
,	,	kIx,	,
Noční	noční	k2eAgMnPc1d1	noční
elfy-s	elfy	k6eAd1	elfy-s
vůdci	vůdce	k1gMnPc1	vůdce
Malfurione	Malfurion	k1gInSc5	Malfurion
Stormrage	Stormrage	k1gFnPc7	Stormrage
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
ženou	žena	k1gFnSc7	žena
Tyrande	Tyrand	k1gInSc5	Tyrand
Whisperwind	Whisperwinda	k1gFnPc2	Whisperwinda
a	a	k8xC	a
Vysoké	vysoký	k2eAgMnPc4d1	vysoký
elfy	elf	k1gMnPc4	elf
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
po	po	k7c6	po
zkáze	zkáza	k1gFnSc6	zkáza
jejích	její	k3xOp3gNnPc2	její
království	království	k1gNnPc2	království
Quel	Quel	k1gInSc1	Quel
<g/>
'	'	kIx"	'
<g/>
Thalas	Thalas	k1gMnSc1	Thalas
stali	stát	k5eAaPmAgMnP	stát
Krvaví	krvavý	k2eAgMnPc1d1	krvavý
elfové	elf	k1gMnPc1	elf
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
princem	princ	k1gMnSc7	princ
Kael	Kael	k1gMnSc1	Kael
<g/>
'	'	kIx"	'
<g/>
thasem	thas	k1gMnSc7	thas
Sunstriedrem	Sunstriedr	k1gMnSc7	Sunstriedr
</s>
</p>
<p>
<s>
Noční	noční	k2eAgMnPc1d1	noční
elfové	elf	k1gMnPc1	elf
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
lesích	les	k1gInPc6	les
Kalimdoru	Kalimdor	k1gInSc2	Kalimdor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypěstovali	vypěstovat	k5eAaPmAgMnP	vypěstovat
Strom	strom	k1gInSc4	strom
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
bylo	být	k5eAaImAgNnS	být
zdrojem	zdroj	k1gInSc7	zdroj
jejich	jejich	k3xOp3gFnSc2	jejich
magie	magie	k1gFnSc2	magie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
dárce	dárce	k1gMnSc1	dárce
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
elfové	elf	k1gMnPc1	elf
mají	mít	k5eAaImIp3nP	mít
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
,	,	kIx,	,
fialovomodrou	fialovomodrý	k2eAgFnSc4d1	fialovomodrá
až	až	k8xS	až
úplně	úplně	k6eAd1	úplně
filavou	filavý	k2eAgFnSc4d1	filavý
barvu	barva	k1gFnSc4	barva
pokožky	pokožka	k1gFnSc2	pokožka
a	a	k8xC	a
vlasy	vlas	k1gInPc4	vlas
v	v	k7c6	v
zelených	zelená	k1gFnPc6	zelená
<g/>
,	,	kIx,	,
fialových	fialový	k2eAgInPc6d1	fialový
<g/>
,	,	kIx,	,
bílých	bílý	k2eAgInPc6d1	bílý
či	či	k8xC	či
modrých	modrý	k2eAgInPc6d1	modrý
odstínech	odstín	k1gInPc6	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Specializují	specializovat	k5eAaBmIp3nP	specializovat
se	se	k3xPyFc4	se
na	na	k7c4	na
boj	boj	k1gInSc4	boj
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
se	se	k3xPyFc4	se
dokáží	dokázat	k5eAaPmIp3nP	dokázat
skrýt	skrýt	k5eAaPmF	skrýt
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
magie	magie	k1gFnSc1	magie
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
se	s	k7c7	s
Stromem	strom	k1gInSc7	strom
světa	svět	k1gInSc2	svět
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
měsíčními	měsíční	k2eAgFnPc7d1	měsíční
studnami	studna	k1gFnPc7	studna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
vodu	voda	k1gFnSc4	voda
nabíjí	nabíjet	k5eAaImIp3nP	nabíjet
magií	magie	k1gFnSc7	magie
skrze	skrze	k?	skrze
měsíční	měsíční	k2eAgInSc4d1	měsíční
svit	svit	k1gInSc4	svit
bohyně	bohyně	k1gFnSc2	bohyně
Elune	Elun	k1gInSc5	Elun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Feistově	Feistův	k2eAgFnSc6d1	Feistova
Midkemii	Midkemie	k1gFnSc6	Midkemie
(	(	kIx(	(
<g/>
a	a	k8xC	a
okolních	okolní	k2eAgInPc6d1	okolní
světech	svět	k1gInPc6	svět
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
elfové	elf	k1gMnPc1	elf
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
světlé	světlý	k2eAgMnPc4d1	světlý
(	(	kIx(	(
<g/>
Eledhel	Eledhel	k1gInSc1	Eledhel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
temné	temný	k2eAgFnPc1d1	temná
peklomory	peklomora	k1gFnPc1	peklomora
(	(	kIx(	(
<g/>
Moredhel	Moredhel	k1gInSc1	Moredhel
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
šílené	šílený	k2eAgFnPc4d1	šílená
<g/>
"	"	kIx"	"
rudomory	rudomora	k1gFnPc4	rudomora
(	(	kIx(	(
<g/>
Glamredhel	Glamredhel	k1gMnSc1	Glamredhel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
sluhy	sluha	k1gMnPc4	sluha
Valheru	Valher	k1gInSc2	Valher
(	(	kIx(	(
<g/>
Dračí	dračí	k2eAgMnPc1d1	dračí
jezdci	jezdec	k1gMnPc1	jezdec
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc4	jaký
pojali	pojmout	k5eAaPmAgMnP	pojmout
úmysly	úmysl	k1gInPc4	úmysl
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
porážce	porážka	k1gFnSc6	porážka
novými	nový	k2eAgMnPc7d1	nový
bohy	bůh	k1gMnPc7	bůh
planety	planeta	k1gFnSc2	planeta
Midkemie	Midkemie	k1gFnSc2	Midkemie
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
tedy	tedy	k9	tedy
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
mytologického	mytologický	k2eAgNnSc2d1	mytologické
dělení	dělení	k1gNnSc2	dělení
jde	jít	k5eAaImIp3nS	jít
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
světlé	světlý	k2eAgMnPc4d1	světlý
elfy	elf	k1gMnPc4	elf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vládnou	vládnout	k5eAaImIp3nP	vládnout
sice	sice	k8xC	sice
magií	magie	k1gFnPc2	magie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
talentu	talent	k1gInSc2	talent
a	a	k8xC	a
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
než	než	k8xS	než
jako	jako	k9	jako
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
schopností	schopnost	k1gFnSc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
spřízněni	spřízněn	k2eAgMnPc1d1	spřízněn
s	s	k7c7	s
Přírodou	příroda	k1gFnSc7	příroda
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokáží	dokázat	k5eAaPmIp3nP	dokázat
komunikovat	komunikovat	k5eAaImF	komunikovat
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnSc6d1	dobrá
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Nepohřbívají	pohřbívat	k5eNaImIp3nP	pohřbívat
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc1	jejich
těla	tělo	k1gNnPc1	tělo
nechají	nechat	k5eAaPmIp3nP	nechat
pohltit	pohltit	k5eAaPmF	pohltit
okolní	okolní	k2eAgFnSc7d1	okolní
Přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
víra	víra	k1gFnSc1	víra
jim	on	k3xPp3gMnPc3	on
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
jméno	jméno	k1gNnSc4	jméno
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
duše	duše	k1gFnSc1	duše
zemřelého	zemřelý	k1gMnSc2	zemřelý
zaslechla	zaslechnout	k5eAaPmAgFnS	zaslechnout
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
ze	z	k7c2	z
Svatých	svatý	k2eAgInPc2d1	svatý
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
potěšila	potěšit	k5eAaPmAgFnS	potěšit
truchlící	truchlící	k2eAgFnSc1d1	truchlící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Andersonově	Andersonův	k2eAgInSc6d1	Andersonův
Zlomeném	zlomený	k2eAgInSc6d1	zlomený
meči	meč	k1gInSc6	meč
vychází	vycházet	k5eAaImIp3nS	vycházet
autor	autor	k1gMnSc1	autor
více	hodně	k6eAd2	hodně
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
mytologie	mytologie	k1gFnSc2	mytologie
a	a	k8xC	a
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
boj	boj	k1gInSc1	boj
světlých	světlý	k2eAgMnPc2d1	světlý
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
skřetů	skřet	k1gMnPc2	skřet
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tábory	tábor	k1gInPc1	tábor
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
lidkých	lidký	k2eAgNnPc6d1	lidké
měřítkách	měřítko	k1gNnPc6	měřítko
poněkud	poněkud	k6eAd1	poněkud
nezařaditelné	zařaditelný	k2eNgFnPc1d1	nezařaditelná
a	a	k8xC	a
i	i	k8xC	i
Světlé	světlý	k2eAgInPc1d1	světlý
bychom	by	kYmCp1nP	by
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
bytosti	bytost	k1gFnPc4	bytost
zlé	zlá	k1gFnPc4	zlá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dokonalým	dokonalý	k2eAgInSc7d1	dokonalý
příkladem	příklad	k1gInSc7	příklad
zlých	zlý	k2eAgMnPc2d1	zlý
elfů	elf	k1gMnPc2	elf
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
elfové	elf	k1gMnPc1	elf
z	z	k7c2	z
Pratchettovy	Pratchettův	k2eAgFnPc1d1	Pratchettova
Zeměplochy	Zeměploch	k1gInPc4	Zeměploch
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vznešení	vznešený	k2eAgMnPc1d1	vznešený
a	a	k8xC	a
krásní	krásný	k2eAgMnPc1d1	krásný
<g/>
,	,	kIx,	,
čtou	číst	k5eAaImIp3nP	číst
myšlenky	myšlenka	k1gFnPc1	myšlenka
a	a	k8xC	a
baví	bavit	k5eAaImIp3nS	bavit
je	on	k3xPp3gInPc4	on
jen	jen	k9	jen
tak	tak	k6eAd1	tak
mučit	mučit	k5eAaImF	mučit
formy	forma	k1gFnPc4	forma
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
oni	onen	k3xDgMnPc1	onen
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
sobě	se	k3xPyFc3	se
rovné	rovný	k2eAgFnSc6d1	rovná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
všechny	všechen	k3xTgInPc4	všechen
ostatní	ostatní	k2eAgInPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nesmírně	smírně	k6eNd1	smírně
krutí	krutý	k2eAgMnPc1d1	krutý
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
vlastním	vlastní	k2eAgMnSc6d1	vlastní
<g/>
,	,	kIx,	,
neskutečném	skutečný	k2eNgInSc6d1	neskutečný
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
parazituje	parazitovat	k5eAaImIp3nS	parazitovat
na	na	k7c6	na
skutečných	skutečný	k2eAgFnPc6d1	skutečná
<g/>
,	,	kIx,	,
cizích	cizí	k2eAgFnPc6d1	cizí
realitách	realita	k1gFnPc6	realita
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
světlé	světlý	k2eAgMnPc4d1	světlý
elfy	elf	k1gMnPc4	elf
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jejich	jejich	k3xOp3gFnSc1	jejich
vládkyně	vládkyně	k1gFnSc1	vládkyně
je	být	k5eAaImIp3nS	být
královna	královna	k1gFnSc1	královna
víl	víla	k1gFnPc2	víla
<g/>
.	.	kIx.	.
</s>
<s>
Býval	bývat	k5eAaImAgMnS	bývat
také	také	k9	také
král	král	k1gMnSc1	král
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
královny	královna	k1gFnSc2	královna
víl	víla	k1gFnPc2	víla
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k9	ale
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
vlastní	vlastní	k2eAgFnSc2d1	vlastní
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
nesmírně	smírně	k6eNd1	smírně
panovačnou	panovačný	k2eAgFnSc7d1	panovačná
královnou	královna	k1gFnSc7	královna
víl	víla	k1gFnPc2	víla
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
domluvit	domluvit	k5eAaPmF	domluvit
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
smířllivosti	smířllivost	k1gFnSc3	smířllivost
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgFnPc3d1	ostatní
rasám	rasa	k1gFnPc3	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
čeho	co	k3yRnSc2	co
se	se	k3xPyFc4	se
elfové	elf	k1gMnPc1	elf
obávají	obávat	k5eAaImIp3nP	obávat
je	on	k3xPp3gNnSc4	on
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
feromagnetické	feromagnetický	k2eAgInPc1d1	feromagnetický
materiály	materiál	k1gInPc1	materiál
–	–	k?	–
a	a	k8xC	a
trochu	trochu	k6eAd1	trochu
také	také	k9	také
"	"	kIx"	"
<g/>
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
hlavách	hlava	k1gFnPc6	hlava
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
logického	logický	k2eAgNnSc2d1	logické
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elfové	elf	k1gMnPc1	elf
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
ve	v	k7c6	v
hrách	hra	k1gFnPc6	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Dungeons	Dungeons	k1gInSc1	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
nebo	nebo	k8xC	nebo
českém	český	k2eAgNnSc6d1	české
Dračím	dračí	k2eAgNnSc6d1	dračí
doupěti	doupě	k1gNnSc6	doupě
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
elfy	elf	k1gMnPc4	elf
podobné	podobný	k2eAgMnPc4d1	podobný
Tolkienovým	Tolkienův	k2eAgInSc7d1	Tolkienův
nebo	nebo	k8xC	nebo
Feistovým	Feistův	k2eAgInSc7d1	Feistův
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Dungeons	Dungeonsa	k1gFnPc2	Dungeonsa
&	&	k?	&
Dragons	Dragonsa	k1gFnPc2	Dragonsa
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
knih	kniha	k1gFnPc2	kniha
touto	tento	k3xDgFnSc7	tento
hrou	hra	k1gFnSc7	hra
inspirovaných	inspirovaný	k2eAgMnPc2d1	inspirovaný
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Temní	temnit	k5eAaImIp3nP	temnit
elfové	elf	k1gMnPc1	elf
neboli	neboli	k8xC	neboli
Drow	Drow	k1gMnPc1	Drow
(	(	kIx(	(
<g/>
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
českých	český	k2eAgInPc6d1	český
překladech	překlad	k1gInPc6	překlad
Drov	Drov	k1gInSc1	Drov
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Drow	Drow	k1gFnSc1	Drow
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
světů	svět	k1gInPc2	svět
Dungeons	Dungeons	k1gInSc4	Dungeons
&	&	k?	&
Dragons	Dragons	k1gInSc1	Dragons
Forgotten	Forgotten	k2eAgInSc1d1	Forgotten
Realms	Realms	k1gInSc1	Realms
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
dlouhouchých	dlouhouchý	k2eAgMnPc2d1	dlouhouchý
elfů	elf	k1gMnPc2	elf
jsou	být	k5eAaImIp3nP	být
elfové	elf	k1gMnPc1	elf
z	z	k7c2	z
Record	Recorda	k1gFnPc2	Recorda
of	of	k?	of
Lodoss	Lodossa	k1gFnPc2	Lodossa
War	War	k1gMnSc1	War
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
anime	animat	k5eAaPmIp3nS	animat
seriál	seriál	k1gInSc1	seriál
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
partie	partie	k1gFnSc2	partie
D	D	kA	D
<g/>
&	&	k?	&
<g/>
D	D	kA	D
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
světlí	světlet	k5eAaImIp3nP	světlet
i	i	k9	i
temní	temný	k2eAgMnPc1d1	temný
elfové	elf	k1gMnPc1	elf
Feistova	Feistův	k2eAgInSc2d1	Feistův
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
elfů	elf	k1gMnPc2	elf
poskakovali	poskakovat	k5eAaImAgMnP	poskakovat
po	po	k7c6	po
stromech	strom	k1gInPc6	strom
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
neměli	mít	k5eNaImAgMnP	mít
problém	problém	k1gInSc4	problém
se	s	k7c7	s
skokem	skok	k1gInSc7	skok
přes	přes	k7c4	přes
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
)	)	kIx)	)
a	a	k8xC	a
místy	místy	k6eAd1	místy
nebyly	být	k5eNaImAgFnP	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
ale	ale	k9	ale
jasné	jasný	k2eAgNnSc1d1	jasné
do	do	k7c2	do
jaké	jaký	k3yRgFnSc2	jaký
míry	míra	k1gFnSc2	míra
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vlastnost	vlastnost	k1gFnSc1	vlastnost
elfů	elf	k1gMnPc2	elf
–	–	k?	–
tedy	tedy	k8xC	tedy
nižší	nízký	k2eAgFnSc1d2	nižší
váha	váha	k1gFnSc1	váha
(	(	kIx(	(
<g/>
nižší	nízký	k2eAgFnSc1d2	nižší
i	i	k8xC	i
než	než	k8xS	než
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jejich	jejich	k3xOp3gFnSc6	jejich
útlé	útlý	k2eAgFnSc6d1	útlá
postavě	postava	k1gFnSc6	postava
<g/>
)	)	kIx)	)
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
rychlost	rychlost	k1gFnSc4	rychlost
–	–	k?	–
a	a	k8xC	a
do	do	k7c2	do
jaké	jaký	k3yRgFnSc2	jaký
výsledek	výsledek	k1gInSc4	výsledek
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
na	na	k7c4	na
trénink	trénink	k1gInSc4	trénink
nebo	nebo	k8xC	nebo
použití	použití	k1gNnSc4	použití
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
románech	román	k1gInPc6	román
Christophera	Christopher	k1gMnSc4	Christopher
Paoliniho	Paolini	k1gMnSc4	Paolini
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
klasičtí	klasický	k2eAgMnPc1d1	klasický
zástupci	zástupce	k1gMnPc1	zástupce
světlých	světlý	k2eAgInPc2d1	světlý
elfů	elf	k1gMnPc2	elf
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
vytvořeni	vytvořit	k5eAaPmNgMnP	vytvořit
co	co	k9	co
nejdokonalejší	dokonalý	k2eAgMnSc1d3	nejdokonalejší
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
ani	ani	k8xC	ani
spát	spát	k5eAaImF	spát
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
upadají	upadat	k5eAaPmIp3nP	upadat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
bdělého	bdělý	k2eAgInSc2d1	bdělý
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
naprosto	naprosto	k6eAd1	naprosto
uvědomují	uvědomovat	k5eAaImIp3nP	uvědomovat
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
nejslabší	slabý	k2eAgMnSc1d3	nejslabší
elf	elf	k1gMnSc1	elf
snadno	snadno	k6eAd1	snadno
porazí	porazit	k5eAaPmIp3nS	porazit
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
několikrát	několikrát	k6eAd1	několikrát
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
když	když	k8xS	když
elfská	elfský	k2eAgFnSc1d1	elfská
princezna	princezna	k1gFnSc1	princezna
Arya	Ary	k1gInSc2	Ary
bojuje	bojovat	k5eAaImIp3nS	bojovat
po	po	k7c6	po
těžkém	těžký	k2eAgNnSc6d1	těžké
zranění	zranění	k1gNnSc6	zranění
Eragona	Eragon	k1gMnSc2	Eragon
s	s	k7c7	s
Durzou	Durza	k1gFnSc7	Durza
v	v	k7c6	v
Tronjheimu	Tronjheim	k1gInSc6	Tronjheim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Disponují	disponovat	k5eAaBmIp3nP	disponovat
neobyčejnou	obyčejný	k2eNgFnSc7d1	neobyčejná
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
nesmrtelností	nesmrtelnost	k1gFnSc7	nesmrtelnost
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
fyzickými	fyzický	k2eAgFnPc7d1	fyzická
zraněními	zranění	k1gNnPc7	zranění
<g/>
;	;	kIx,	;
např.	např.	kA	např.
nevyléčitelné	vyléčitelný	k2eNgFnPc4d1	nevyléčitelná
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
bitvy	bitva	k1gFnPc4	bitva
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krásou	krása	k1gFnSc7	krása
a	a	k8xC	a
citem	cit	k1gInSc7	cit
pro	pro	k7c4	pro
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Nesmrtelní	nesmrtelný	k1gMnPc1	nesmrtelný
však	však	k9	však
nebyli	být	k5eNaImAgMnP	být
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
při	při	k7c6	při
ustanovení	ustanovení	k1gNnSc6	ustanovení
Jezdců	jezdec	k1gInPc2	jezdec
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
elfů	elf	k1gMnPc2	elf
s	s	k7c7	s
draky	drak	k1gMnPc7	drak
(	(	kIx(	(
<g/>
draci	drak	k1gMnPc1	drak
od	od	k7c2	od
elfů	elf	k1gMnPc2	elf
získali	získat	k5eAaPmAgMnP	získat
schopnost	schopnost	k1gFnSc4	schopnost
mluvit	mluvit	k5eAaImF	mluvit
jejich	jejich	k3xOp3gFnSc7	jejich
řečí	řeč	k1gFnSc7	řeč
<g/>
,	,	kIx,	,
elfové	elf	k1gMnPc1	elf
zdědili	zdědit	k5eAaPmAgMnP	zdědit
jejich	jejich	k3xOp3gFnSc4	jejich
dlouhověkost	dlouhověkost	k1gFnSc4	dlouhověkost
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píší	psát	k5eAaImIp3nP	psát
nádherné	nádherný	k2eAgFnPc4d1	nádherná
epické	epický	k2eAgFnPc4d1	epická
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
domovy	domov	k1gInPc1	domov
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
nejedí	jíst	k5eNaImIp3nP	jíst
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
ovládají	ovládat	k5eAaImIp3nP	ovládat
kouzla	kouzlo	k1gNnPc4	kouzlo
a	a	k8xC	a
dokáží	dokázat	k5eAaPmIp3nP	dokázat
rostliny	rostlina	k1gFnPc1	rostlina
vyzpívat	vyzpívat	k5eAaPmF	vyzpívat
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sci-fi	scii	k1gNnPc3	sci-fi
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
čistém	čistý	k2eAgNnSc6d1	čisté
sci-fi	scii	k1gNnSc6	sci-fi
se	se	k3xPyFc4	se
elfové	elf	k1gMnPc1	elf
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
typologie	typologie	k1gFnSc1	typologie
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
odráží	odrážet	k5eAaImIp3nS	odrážet
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Star	star	k1gFnSc1	star
Trek	Trek	k1gInSc1	Trek
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
směrech	směr	k1gInPc6	směr
blízko	blízko	k6eAd1	blízko
Vulkánci	Vulkánek	k1gMnPc1	Vulkánek
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
světlí	světlet	k5eAaImIp3nP	světlet
<g/>
)	)	kIx)	)
a	a	k8xC	a
Romulané	Romulaný	k2eAgNnSc1d1	Romulané
(	(	kIx(	(
<g/>
jako	jako	k9	jako
temní	temnit	k5eAaImIp3nS	temnit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
Tolkienově	Tolkienův	k2eAgInSc6d1	Tolkienův
nebo	nebo	k8xC	nebo
možná	možná	k9	možná
spíše	spíše	k9	spíše
Feistově	Feistův	k2eAgInSc6d1	Feistův
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
–	–	k?	–
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
dlouhověkostí	dlouhověkost	k1gFnSc7	dlouhověkost
i	i	k8xC	i
telepatickými	telepatický	k2eAgFnPc7d1	telepatická
schopnostmi	schopnost	k1gFnPc7	schopnost
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
schopností	schopnost	k1gFnPc2	schopnost
magických	magický	k2eAgFnPc2d1	magická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
podobné	podobný	k2eAgNnSc1d1	podobné
elfům	elf	k1gMnPc3	elf
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
i	i	k9	i
krátkověké	krátkověký	k2eAgFnPc4d1	krátkověká
Ocampy	Ocampa	k1gFnPc4	Ocampa
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
seriálu	seriál	k1gInSc2	seriál
–	–	k?	–
jejich	jejich	k3xOp3gFnPc1	jejich
schopnosti	schopnost	k1gFnPc1	schopnost
mají	mít	k5eAaImIp3nP	mít
ještě	ještě	k9	ještě
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
magii	magie	k1gFnSc3	magie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
přímo	přímo	k6eAd1	přímo
hledáte	hledat	k5eAaImIp2nP	hledat
<g/>
,	,	kIx,	,
najdete	najít	k5eAaPmIp2nP	najít
znaky	znak	k1gInPc4	znak
elfů	elf	k1gMnPc2	elf
i	i	k8xC	i
u	u	k7c2	u
Minbarů	Minbar	k1gInPc2	Minbar
z	z	k7c2	z
Babylonu	babylon	k1gInSc2	babylon
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jejich	jejich	k3xOp3gFnSc1	jejich
architektura	architektura	k1gFnSc1	architektura
připomíná	připomínat	k5eAaImIp3nS	připomínat
elfskou	elfská	k1gFnSc4	elfská
a	a	k8xC	a
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
charakteru	charakter	k1gInSc6	charakter
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
stejnou	stejný	k2eAgFnSc4d1	stejná
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
a	a	k8xC	a
aroganci	arogance	k1gFnSc4	arogance
která	který	k3yRgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
připisována	připisovat	k5eAaImNgFnS	připisovat
Vznešeným	vznešený	k2eAgMnPc3d1	vznešený
elfům	elf	k1gMnPc3	elf
<g/>
.	.	kIx.	.
</s>
<s>
Elfská	Elfská	k1gFnSc1	Elfská
je	být	k5eAaImIp3nS	být
i	i	k9	i
"	"	kIx"	"
<g/>
role	role	k1gFnPc4	role
<g/>
"	"	kIx"	"
jejich	jejich	k3xOp3gFnPc1	jejich
rasy	rasa	k1gFnPc1	rasa
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
výrazněji	výrazně	k6eAd2	výrazně
než	než	k8xS	než
ve	v	k7c6	v
Star	star	k1gFnSc6	star
Treku	Trek	k1gInSc2	Trek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wells	k1gInSc1	Wells
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
Stroj	stroj	k1gInSc1	stroj
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
podobnost	podobnost	k1gFnSc1	podobnost
s	s	k7c7	s
rozdělením	rozdělení	k1gNnSc7	rozdělení
elfů	elf	k1gMnPc2	elf
na	na	k7c4	na
světlé	světlý	k2eAgNnSc4d1	světlé
a	a	k8xC	a
tmavé	tmavý	k2eAgNnSc4d1	tmavé
podle	podle	k7c2	podle
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
hry	hra	k1gFnSc2	hra
Shadowrun	Shadowrun	k1gInSc1	Shadowrun
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
fantasy	fantas	k1gInPc4	fantas
a	a	k8xC	a
kyberpunk	kyberpunk	k1gInSc4	kyberpunk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
elfové	elf	k1gMnPc1	elf
vzniklí	vzniklý	k2eAgMnPc1d1	vzniklý
mutací	mutace	k1gFnPc2	mutace
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BANDINI	BANDINI	kA	BANDINI
<g/>
,	,	kIx,	,
Ditte	Ditt	k1gMnSc5	Ditt
<g/>
;	;	kIx,	;
BANDINI	BANDINI	kA	BANDINI
<g/>
,	,	kIx,	,
Giovanni	Giovanen	k2eAgMnPc1d1	Giovanen
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
elfů	elf	k1gMnPc2	elf
a	a	k8xC	a
víl	víla	k1gFnPc2	víla
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Fontána	fontána	k1gFnSc1	fontána
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7336	[number]	k4	7336
<g/>
-	-	kIx~	-
<g/>
263	[number]	k4	263
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ČUDRNÁKOVÁ	ČUDRNÁKOVÁ	kA	ČUDRNÁKOVÁ
<g/>
,	,	kIx,	,
Andrea	Andrea	k1gFnSc1	Andrea
<g/>
.	.	kIx.	.
</s>
<s>
Elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
hobiti	hobit	k1gMnPc1	hobit
<g/>
,	,	kIx,	,
trpaslíci	trpaslík	k1gMnPc1	trpaslík
a	a	k8xC	a
mýty	mýtus	k1gInPc7	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Fontána	fontána	k1gFnSc1	fontána
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
317	[number]	k4	317
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7336	[number]	k4	7336
<g/>
-	-	kIx~	-
<g/>
427	[number]	k4	427
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Tolkienovi	Tolkienův	k2eAgMnPc1d1	Tolkienův
Elfové	elf	k1gMnPc1	elf
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Elves	Elves	k1gMnSc1	Elves
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
39	[number]	k4	39
-	-	kIx~	-
72	[number]	k4	72
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KRAUSE	Kraus	k1gMnSc2	Kraus
<g/>
,	,	kIx,	,
Arnulf	Arnulf	k1gInSc1	Arnulf
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
Středozemě	Středozem	k1gFnPc1	Středozem
:	:	kIx,	:
Tolkienova	Tolkienův	k2eAgFnSc1d1	Tolkienova
mytologie	mytologie	k1gFnSc1	mytologie
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
středověké	středověký	k2eAgInPc1d1	středověký
kořeny	kořen	k1gInPc1	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
4489	[number]	k4	4489
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LECOUTEUX	LECOUTEUX	kA	LECOUTEUX
<g/>
,	,	kIx,	,
Claude	Claud	k1gMnSc5	Claud
<g/>
.	.	kIx.	.
</s>
<s>
Trpaslíci	trpaslík	k1gMnPc1	trpaslík
a	a	k8xC	a
elfové	elf	k1gMnPc1	elf
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
118	[number]	k4	118
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
92	[number]	k4	92
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
Téra	Tér	k2eAgFnSc1d1	Téra
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
STANTON	STANTON	kA	STANTON
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
N.	N.	kA	N.
Hobiti	hobit	k1gMnPc1	hobit
<g/>
,	,	kIx,	,
elfové	elf	k1gMnPc1	elf
a	a	k8xC	a
čarodějové	čaroděj	k1gMnPc1	čaroděj
<g/>
.	.	kIx.	.
</s>
<s>
Objevování	objevování	k1gNnSc1	objevování
divů	div	k1gInPc2	div
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Tolkienova	Tolkienův	k2eAgMnSc2d1	Tolkienův
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Stanislava	Stanislav	k1gMnSc2	Stanislav
Pošustová-Menšíková	Pošustová-Menšíková	k1gFnSc1	Pošustová-Menšíková
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1032	[number]	k4	1032
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Elfové	elf	k1gMnPc1	elf
<g/>
,	,	kIx,	,
s.	s.	k?	s.
125	[number]	k4	125
-	-	kIx~	-
134	[number]	k4	134
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LUKAVEC	LUKAVEC	kA	LUKAVEC
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Proměnliví	proměnlivý	k2eAgMnPc1d1	proměnlivý
elfové	elf	k1gMnPc1	elf
<g/>
:	:	kIx,	:
krása	krása	k1gFnSc1	krása
až	až	k9	až
nelidsky	lidsky	k6eNd1	lidsky
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
<g/>
?	?	kIx.	?
</s>
<s>
In	In	k?	In
Bytosti	bytost	k1gFnPc1	bytost
na	na	k7c4	na
pomezí	pomezí	k1gNnSc4	pomezí
<g/>
:	:	kIx,	:
texty	text	k1gInPc7	text
o	o	k7c6	o
literární	literární	k2eAgFnSc6d1	literární
fantastice	fantastika	k1gFnSc6	fantastika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Pulchra	Pulchra	k1gFnSc1	Pulchra
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
284	[number]	k4	284
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7564	[number]	k4	7564
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Skřítek	skřítek	k1gMnSc1	skřítek
(	(	kIx(	(
<g/>
mytologie	mytologie	k1gFnSc1	mytologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Germánská	germánský	k2eAgFnSc1d1	germánská
mytologie	mytologie	k1gFnSc1	mytologie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
elf	elf	k1gMnSc1	elf
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
elf	elf	k1gMnSc1	elf
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
