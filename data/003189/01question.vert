<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
odnož	odnož	k1gFnSc1	odnož
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
dohromady	dohromady	k6eAd1	dohromady
žánry	žánr	k1gInPc4	žánr
hardcore	hardcor	k1gInSc5	hardcor
a	a	k8xC	a
doom	doomo	k1gNnPc2	doomo
<g/>
?	?	kIx.	?
</s>
