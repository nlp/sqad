<s>
Quentin	Quentin	k1gMnSc1	Quentin
Jerome	Jerom	k1gInSc5	Jerom
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1963	[number]	k4	1963
Knoxville	Knoxville	k1gNnSc2	Knoxville
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gNnSc2	Tennessee
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
začátkem	začátkem	k7c2	začátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
svěží	svěží	k2eAgMnSc1d1	svěží
<g/>
,	,	kIx,	,
drsný	drsný	k2eAgMnSc1d1	drsný
i	i	k8xC	i
černohumorný	černohumorný	k2eAgMnSc1d1	černohumorný
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vnesl	vnést	k5eAaPmAgMnS	vnést
do	do	k7c2	do
tradičních	tradiční	k2eAgInPc2d1	tradiční
amerických	americký	k2eAgInPc2d1	americký
archetypů	archetyp	k1gInPc2	archetyp
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
