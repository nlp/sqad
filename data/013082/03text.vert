<p>
<s>
Quentin	Quentin	k1gMnSc1	Quentin
Jerome	Jerom	k1gInSc5	Jerom
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1963	[number]	k4	1963
Knoxville	Knoxville	k1gNnSc2	Knoxville
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gNnSc2	Tennessee
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
začátkem	začátkem	k7c2	začátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
svěží	svěží	k2eAgMnSc1d1	svěží
<g/>
,	,	kIx,	,
drsný	drsný	k2eAgMnSc1d1	drsný
i	i	k8xC	i
černohumorný	černohumorný	k2eAgMnSc1d1	černohumorný
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vnesl	vnést	k5eAaPmAgMnS	vnést
do	do	k7c2	do
tradičních	tradiční	k2eAgInPc2d1	tradiční
amerických	americký	k2eAgInPc2d1	americký
archetypů	archetyp	k1gInPc2	archetyp
nový	nový	k2eAgInSc4d1	nový
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
filmovou	filmový	k2eAgFnSc4d1	filmová
kariéru	kariéra	k1gFnSc4	kariéra
začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
uvaděč	uvaděč	k1gMnSc1	uvaděč
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
<g/>
.	.	kIx.	.
</s>
<s>
Nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
byl	být	k5eAaImAgMnS	být
i	i	k9	i
uvaděčem	uvaděč	k1gMnSc7	uvaděč
v	v	k7c6	v
pornokině	pornokina	k1gFnSc6	pornokina
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k8xC	jako
úplně	úplně	k6eAd1	úplně
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
režie	režie	k1gFnSc1	režie
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
až	až	k9	až
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
22	[number]	k4	22
let	léto	k1gNnPc2	léto
odešel	odejít	k5eAaPmAgInS	odejít
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
pornokinu	pornokin	k1gInSc6	pornokin
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgInS	pracovat
následujících	následující	k2eAgInPc2d1	následující
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
videopůjčovně	videopůjčovna	k1gFnSc6	videopůjčovna
VideoArchives	VideoArchivesa	k1gFnPc2	VideoArchivesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
slavným	slavný	k2eAgMnSc7d1	slavný
scenáristou	scenárista	k1gMnSc7	scenárista
a	a	k8xC	a
režisérem	režisér	k1gMnSc7	režisér
<g/>
,	,	kIx,	,
pobyl	pobýt	k5eAaPmAgMnS	pobýt
krátce	krátce	k6eAd1	krátce
i	i	k9	i
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončil	skončit	k5eAaPmAgInS	skončit
kvůli	kvůli	k7c3	kvůli
nezaplacenému	zaplacený	k2eNgNnSc3d1	nezaplacené
parkování	parkování	k1gNnSc3	parkování
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
skoro	skoro	k6eAd1	skoro
vždy	vždy	k6eAd1	vždy
objevuje	objevovat	k5eAaImIp3nS	objevovat
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc4	násilí
i	i	k8xC	i
krvavé	krvavý	k2eAgFnPc4d1	krvavá
scény	scéna	k1gFnPc4	scéna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
typický	typický	k2eAgInSc4d1	typický
hororový	hororový	k2eAgInSc4d1	hororový
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
tzv.	tzv.	kA	tzv.
splatter	splatter	k1gMnSc1	splatter
hororů	horor	k1gInPc2	horor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
italského	italský	k2eAgMnSc2d1	italský
hororového	hororový	k2eAgMnSc2d1	hororový
mistra	mistr	k1gMnSc2	mistr
Daria	Daria	k1gFnSc1	Daria
Argenta	argentum	k1gNnSc2	argentum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
Tarantino	Tarantina	k1gFnSc5	Tarantina
podpořil	podpořit	k5eAaPmAgMnS	podpořit
hnutí	hnutí	k1gNnSc4	hnutí
Black	Black	k1gMnSc1	Black
Lives	Lives	k1gMnSc1	Lives
Matter	Matter	k1gMnSc1	Matter
(	(	kIx(	(
<g/>
na	na	k7c6	na
černých	černý	k2eAgInPc6d1	černý
životech	život	k1gInPc6	život
záleží	záležet	k5eAaImIp3nS	záležet
<g/>
)	)	kIx)	)
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
veřejně	veřejně	k6eAd1	veřejně
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
policejní	policejní	k2eAgNnSc4d1	policejní
násilí	násilí	k1gNnSc4	násilí
na	na	k7c6	na
černošském	černošský	k2eAgNnSc6d1	černošské
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vraždu	vražda	k1gFnSc4	vražda
musím	muset	k5eAaImIp1nS	muset
nazývat	nazývat	k5eAaImF	nazývat
vraždou	vražda	k1gFnSc7	vražda
a	a	k8xC	a
vrahům	vrah	k1gMnPc3	vrah
musím	muset	k5eAaImIp1nS	muset
říkat	říkat	k5eAaImF	říkat
vrazi	vrah	k1gMnPc1	vrah
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tarantinova	Tarantinův	k2eAgNnSc2d1	Tarantinovo
slova	slovo	k1gNnSc2	slovo
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
policejní	policejní	k2eAgNnPc4d1	policejní
oddělení	oddělení	k1gNnPc4	oddělení
po	po	k7c6	po
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
k	k	k7c3	k
bojkotu	bojkot	k1gInSc3	bojkot
jeho	on	k3xPp3gInSc2	on
nového	nový	k2eAgInSc2d1	nový
filmu	film	k1gInSc2	film
Osm	osm	k4xCc4	osm
hrozných	hrozný	k2eAgInPc2d1	hrozný
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
předtím	předtím	k6eAd1	předtím
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
policista	policista	k1gMnSc1	policista
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
někteří	některý	k3yIgMnPc1	některý
policisté	policista	k1gMnPc1	policista
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
Tarantinovy	Tarantinův	k2eAgInPc4d1	Tarantinův
výroky	výrok	k1gInPc4	výrok
jako	jako	k8xC	jako
obzvláště	obzvláště	k6eAd1	obzvláště
necitlivé	citlivý	k2eNgFnPc4d1	necitlivá
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
odhalení	odhalení	k1gNnSc6	odhalení
<g/>
,	,	kIx,	,
že	že	k8xS	že
přední	přední	k2eAgMnSc1d1	přední
hollywoodský	hollywoodský	k2eAgMnSc1d1	hollywoodský
producent	producent	k1gMnSc1	producent
Harvey	Harvea	k1gFnSc2	Harvea
Weinstein	Weinstein	k1gMnSc1	Weinstein
měl	mít	k5eAaImAgMnS	mít
sexuálně	sexuálně	k6eAd1	sexuálně
obtěžovat	obtěžovat	k5eAaImF	obtěžovat
desítky	desítka	k1gFnPc4	desítka
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
věděl	vědět	k5eAaImAgMnS	vědět
o	o	k7c6	o
Weinsteinově	Weinsteinův	k2eAgNnSc6d1	Weinsteinův
chování	chování	k1gNnSc6	chování
desítky	desítka	k1gFnSc2	desítka
let	léto	k1gNnPc2	léto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zahanben	zahanben	k2eAgMnSc1d1	zahanben
<g/>
,	,	kIx,	,
že	že	k8xS	že
nic	nic	k3yNnSc1	nic
nepodnikl	podniknout	k5eNaPmAgMnS	podniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
známý	známý	k2eAgMnSc1d1	známý
šéfkuchař	šéfkuchař	k1gMnSc1	šéfkuchař
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Anthony	Anthona	k1gFnSc2	Anthona
Bourdain	Bourdain	k1gMnSc1	Bourdain
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Tarantina	Tarantin	k1gMnSc4	Tarantin
ze	z	k7c2	z
"	"	kIx"	"
<g/>
spoluviny	spoluvina	k1gFnSc2	spoluvina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Scénář	scénář	k1gInSc1	scénář
a	a	k8xC	a
režie	režie	k1gFnSc1	režie
===	===	k?	===
</s>
</p>
<p>
<s>
My	my	k3xPp1nPc1	my
Best	Best	k1gMnSc1	Best
Friend	Friend	k1gInSc1	Friend
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Birthday	Birthday	k1gInPc7	Birthday
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gauneři	gauner	k1gMnPc1	gauner
(	(	kIx(	(
<g/>
Reservoir	Reservoir	k1gInSc1	Reservoir
Dogs	Dogsa	k1gFnPc2	Dogsa
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
<g/>
:	:	kIx,	:
Historky	historka	k1gFnPc1	historka
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
(	(	kIx(	(
<g/>
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
pokoje	pokoj	k1gInPc1	pokoj
(	(	kIx(	(
<g/>
Four	Four	k1gInSc1	Four
Rooms	Roomsa	k1gFnPc2	Roomsa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jackie	Jackie	k1gFnSc1	Jackie
Brownová	Brownová	k1gFnSc1	Brownová
(	(	kIx(	(
<g/>
Jackie	Jackie	k1gFnSc1	Jackie
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kill	Kill	k1gMnSc1	Kill
Bill	Bill	k1gMnSc1	Bill
(	(	kIx(	(
<g/>
Kill	Kill	k1gMnSc1	Kill
Bill	Bill	k1gMnSc1	Bill
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kill	Kill	k1gMnSc1	Kill
Bill	Bill	k1gMnSc1	Bill
2	[number]	k4	2
(	(	kIx(	(
<g/>
Kill	Kill	k1gMnSc1	Kill
Bill	Bill	k1gMnSc1	Bill
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sin	sin	kA	sin
City	city	k1gNnSc1	city
–	–	k?	–
město	město	k1gNnSc1	město
hříchu	hřích	k1gInSc2	hřích
(	(	kIx(	(
<g/>
Sin	sin	kA	sin
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
hostující	hostující	k2eAgMnSc1d1	hostující
režisér	režisér	k1gMnSc1	režisér
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Auto	auto	k1gNnSc1	auto
zabiják	zabiják	k1gMnSc1	zabiják
(	(	kIx(	(
<g/>
Death	Death	k1gMnSc1	Death
Proof	Proof	k1gMnSc1	Proof
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hanebný	hanebný	k2eAgMnSc1d1	hanebný
pancharti	panchart	k1gMnPc1	panchart
(	(	kIx(	(
<g/>
Inglourious	Inglourious	k1gInSc1	Inglourious
Basterds	Basterdsa	k1gFnPc2	Basterdsa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nespoutaný	spoutaný	k2eNgMnSc1d1	nespoutaný
Django	Django	k1gMnSc1	Django
(	(	kIx(	(
<g/>
Django	Django	k1gMnSc1	Django
Unchained	Unchained	k1gMnSc1	Unchained
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Osm	osm	k4xCc1	osm
hrozných	hrozný	k2eAgFnPc2d1	hrozná
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
Hateful	Hatefula	k1gFnPc2	Hatefula
Eight	Eight	k1gInSc1	Eight
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tenkrát	tenkrát	k6eAd1	tenkrát
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
(	(	kIx(	(
<g/>
Once	Once	k1gNnSc1	Once
Upon	Upona	k1gFnPc2	Upona
a	a	k8xC	a
Time	Time	k1gFnPc2	Time
in	in	k?	in
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Scénář	scénář	k1gInSc1	scénář
===	===	k?	===
</s>
</p>
<p>
<s>
Pravdivá	pravdivý	k2eAgFnSc1d1	pravdivá
romance	romance	k1gFnSc1	romance
(	(	kIx(	(
<g/>
True	True	k1gFnSc1	True
Romance	romance	k1gFnSc1	romance
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Takoví	takový	k3xDgMnPc1	takový
normální	normální	k2eAgMnPc1d1	normální
zabijáci	zabiják	k1gMnPc1	zabiják
(	(	kIx(	(
<g/>
Natural	Natural	k?	Natural
Born	Born	k1gInSc1	Born
Killers	Killersa	k1gFnPc2	Killersa
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Od	od	k7c2	od
soumraku	soumrak	k1gInSc2	soumrak
do	do	k7c2	do
úsvitu	úsvit	k1gInSc2	úsvit
(	(	kIx(	(
<g/>
From	From	k1gMnSc1	From
Dusk	Dusk	k1gMnSc1	Dusk
Till	Till	k1gMnSc1	Till
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Herec	herec	k1gMnSc1	herec
===	===	k?	===
</s>
</p>
<p>
<s>
My	my	k3xPp1nPc1	my
Best	Best	k1gMnSc1	Best
Friend	Friend	k1gInSc1	Friend
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Birthday	Birthday	k1gInPc7	Birthday
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
–	–	k?	–
Clarence	Clarenec	k1gInSc2	Clarenec
Pool	Poola	k1gFnPc2	Poola
</s>
</p>
<p>
<s>
Gauneři	gauner	k1gMnPc1	gauner
(	(	kIx(	(
<g/>
Reservoir	Reservoir	k1gInSc1	Reservoir
Dogs	Dogsa	k1gFnPc2	Dogsa
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
–	–	k?	–
pan	pan	k1gMnSc1	pan
Hnědý	hnědý	k2eAgMnSc1d1	hnědý
</s>
</p>
<p>
<s>
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
<g/>
:	:	kIx,	:
Historky	historka	k1gFnPc1	historka
z	z	k7c2	z
podsvětí	podsvětí	k1gNnSc2	podsvětí
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
–	–	k?	–
Jimmie	Jimmie	k1gFnSc2	Jimmie
Dimmick	Dimmick	k1gMnSc1	Dimmick
</s>
</p>
<p>
<s>
Sleep	Sleep	k1gMnSc1	Sleep
with	with	k1gMnSc1	with
Me	Me	k1gMnSc1	Me
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
–	–	k?	–
Syd	Syd	k1gFnSc1	Syd
</s>
</p>
<p>
<s>
Destiny	Destina	k1gFnPc1	Destina
Turns	Turnsa	k1gFnPc2	Turnsa
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Radio	radio	k1gNnSc1	radio
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
Johnny	Johnn	k1gMnPc4	Johnn
Destiny	Destina	k1gFnSc2	Destina
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
pokoje	pokoj	k1gInPc1	pokoj
(	(	kIx(	(
<g/>
Four	Four	k1gInSc1	Four
Rooms	Roomsa	k1gFnPc2	Roomsa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
Chester	Chester	k1gInSc1	Chester
</s>
</p>
<p>
<s>
Desperado	Desperada	k1gFnSc5	Desperada
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
–	–	k?	–
chlápek	chlápek	k?	chlápek
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
vyzvedával	vyzvedávat	k5eAaImAgInS	vyzvedávat
drogy	droga	k1gFnPc4	droga
</s>
</p>
<p>
<s>
Od	od	k7c2	od
soumraku	soumrak	k1gInSc2	soumrak
do	do	k7c2	do
úsvitu	úsvit	k1gInSc2	úsvit
(	(	kIx(	(
<g/>
From	From	k1gMnSc1	From
Dusk	Dusk	k1gMnSc1	Dusk
Till	Till	k1gMnSc1	Till
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
–	–	k?	–
Richard	Richard	k1gMnSc1	Richard
Gecko	Gecko	k1gNnSc1	Gecko
</s>
</p>
<p>
<s>
Girl	girl	k1gFnSc1	girl
6	[number]	k4	6
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
–	–	k?	–
Q.	Q.	kA	Q.
T.	T.	kA	T.
</s>
</p>
<p>
<s>
Little	Little	k1gFnSc1	Little
Nicky	nicka	k1gFnSc2	nicka
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
–	–	k?	–
Deacon	Deacon	k1gInSc1	Deacon
</s>
</p>
<p>
<s>
Planeta	planeta	k1gFnSc1	planeta
Teror	teror	k1gInSc1	teror
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
Voják	voják	k1gMnSc1	voják
</s>
</p>
<p>
<s>
Auto	auto	k1gNnSc1	auto
zabiják	zabiják	k1gMnSc1	zabiják
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
barman	barman	k1gMnSc1	barman
Warren	Warrna	k1gFnPc2	Warrna
</s>
</p>
<p>
<s>
Nespoutaný	spoutaný	k2eNgMnSc1d1	nespoutaný
Django	Django	k1gMnSc1	Django
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
–	–	k?	–
kovboj	kovboj	k1gMnSc1	kovboj
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
dynamit	dynamit	k1gInSc4	dynamit
</s>
</p>
<p>
<s>
Osm	osm	k4xCc1	osm
hrozných	hrozný	k2eAgFnPc2d1	hrozná
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
–	–	k?	–
vypravěč	vypravěč	k1gMnSc1	vypravěč
</s>
</p>
<p>
<s>
===	===	k?	===
Výkonný	výkonný	k2eAgMnSc1d1	výkonný
producent	producent	k1gMnSc1	producent
===	===	k?	===
</s>
</p>
<p>
<s>
Killing	Killing	k1gInSc1	Killing
Zoe	Zoe	k1gFnSc2	Zoe
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
pokoje	pokoj	k1gInPc1	pokoj
(	(	kIx(	(
<g/>
Four	Four	k1gInSc1	Four
Rooms	Roomsa	k1gFnPc2	Roomsa
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Od	od	k7c2	od
soumraku	soumrak	k1gInSc2	soumrak
do	do	k7c2	do
úsvitu	úsvit	k1gInSc2	úsvit
(	(	kIx(	(
<g/>
From	From	k1gMnSc1	From
Dusk	Dusk	k1gMnSc1	Dusk
Till	Till	k1gMnSc1	Till
Dawn	Dawn	k1gMnSc1	Dawn
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Curdled	Curdled	k1gInSc1	Curdled
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
God	God	k?	God
Said	Said	k1gInSc1	Said
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Ha	ha	kA	ha
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Daltry	Daltr	k1gInPc1	Daltr
Calhoun	Calhoun	k1gInSc1	Calhoun
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Freedom	Freedom	k1gInSc1	Freedom
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fury	Fury	k1gInPc7	Fury
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Planeta	planeta	k1gFnSc1	planeta
Teror	teror	k1gInSc1	teror
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Quentin	Quentina	k1gFnPc2	Quentina
Tarantino	Tarantina	k1gFnSc5	Tarantina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Quentin	Quentin	k1gInSc4	Quentin
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Quentin	Quentin	k1gInSc1	Quentin
Tarantino	Tarantin	k2eAgNnSc4d1	Tarantino
</s>
</p>
<p>
<s>
Quentin	Quentin	k1gInSc4	Quentin
Tarantino	Tarantin	k2eAgNnSc1d1	Tarantino
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Quentin	Quentin	k1gMnSc1	Quentin
Tarantino	Tarantin	k2eAgNnSc4d1	Tarantino
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fanouškovské	fanouškovský	k2eAgFnSc2d1	fanouškovská
stránky	stránka	k1gFnSc2	stránka
Qtarantino	Qtarantin	k2eAgNnSc1d1	Qtarantin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
šel	jít	k5eAaImAgMnS	jít
čas	čas	k1gInSc4	čas
s	s	k7c7	s
filmy	film	k1gInPc7	film
Quentina	Quentin	k1gMnSc2	Quentin
Tarantina	Tarantin	k1gMnSc2	Tarantin
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
na	na	k7c4	na
Cinemo	Cinema	k1gFnSc5	Cinema
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
šel	jít	k5eAaImAgMnS	jít
čas	čas	k1gInSc4	čas
s	s	k7c7	s
filmy	film	k1gInPc7	film
Quentina	Quentin	k1gMnSc2	Quentin
Tarantina	Tarantin	k1gMnSc2	Tarantin
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
na	na	k7c4	na
Cinemo	Cinema	k1gFnSc5	Cinema
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
