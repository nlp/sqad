<s>
ABBA	ABBA	kA	ABBA
byla	být	k5eAaImAgFnS	být
švédská	švédský	k2eAgFnSc1d1	švédská
popová	popový	k2eAgFnSc1d1	popová
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
kapel	kapela	k1gFnPc2	kapela
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
této	tento	k3xDgFnSc2	tento
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xC	jako
akronym	akronym	k1gInSc4	akronym
spojením	spojení	k1gNnSc7	spojení
počátečních	počáteční	k2eAgNnPc2d1	počáteční
písmen	písmeno	k1gNnPc2	písmeno
křestních	křestní	k2eAgNnPc2d1	křestní
jmen	jméno	k1gNnPc2	jméno
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
A	A	kA	A
<g/>
*	*	kIx~	*
<g/>
gnetha	gnetha	k1gMnSc1	gnetha
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
*	*	kIx~	*
<g/>
jörn	jörn	k1gMnSc1	jörn
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
*	*	kIx~	*
<g/>
enny	enna	k1gMnSc2	enna
a	a	k8xC	a
A	a	k9	a
<g/>
*	*	kIx~	*
<g/>
nni-Frid	nni-Frid	k1gInSc1	nni-Frid
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
skupina	skupina	k1gFnSc1	skupina
prodala	prodat	k5eAaPmAgFnS	prodat
přes	přes	k7c4	přes
375	[number]	k4	375
miliónů	milión	k4xCgInPc2	milión
desek	deska	k1gFnPc2	deska
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
prodávají	prodávat	k5eAaImIp3nP	prodávat
kolem	kolem	k7c2	kolem
3	[number]	k4	3
miliónů	milión	k4xCgInPc2	milión
alb	alba	k1gFnPc2	alba
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
ABBA	ABBA	kA	ABBA
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
průlomu	průlom	k1gInSc2	průlom
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Eurovize	Eurovize	k1gFnSc1	Eurovize
s	s	k7c7	s
písní	píseň	k1gFnPc2	píseň
"	"	kIx"	"
<g/>
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
"	"	kIx"	"
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
první	první	k4xOgNnSc4	první
ne-anglicky	nglicky	k6eNd1	-anglicky
mluvící	mluvící	k2eAgFnSc7d1	mluvící
popovou	popový	k2eAgFnSc7d1	popová
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dobyla	dobýt	k5eAaPmAgFnS	dobýt
hitparády	hitparáda	k1gFnPc4	hitparáda
v	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Obrovského	obrovský	k2eAgInSc2d1	obrovský
úspěchu	úspěch	k1gInSc2	úspěch
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
také	také	k9	také
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
ABBA	ABBA	kA	ABBA
také	také	k6eAd1	také
nahrála	nahrát	k5eAaPmAgFnS	nahrát
kompilační	kompilační	k2eAgNnSc4d1	kompilační
album	album	k1gNnSc4	album
svých	svůj	k3xOyFgInPc2	svůj
hitů	hit	k1gInPc2	hit
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
s	s	k7c7	s
názvem	název	k1gInSc7	název
Gracias	Gracias	k1gMnSc1	Gracias
Por	Por	k1gMnSc2	Por
La	la	k1gNnSc2	la
Música	Músicus	k1gMnSc2	Músicus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
určené	určený	k2eAgNnSc1d1	určené
pro	pro	k7c4	pro
španělsky	španělsky	k6eAd1	španělsky
mluvící	mluvící	k2eAgFnPc4d1	mluvící
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
nahrávky	nahrávka	k1gFnPc4	nahrávka
čtveřice	čtveřice	k1gFnSc2	čtveřice
byl	být	k5eAaImAgInS	být
i	i	k9	i
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
socialistického	socialistický	k2eAgInSc2d1	socialistický
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tehdejším	tehdejší	k2eAgMnSc6d1	tehdejší
SSSR	SSSR	kA	SSSR
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
40	[number]	k4	40
miliónech	milión	k4xCgInPc6	milión
kusech	kus	k1gInPc6	kus
alba	album	k1gNnSc2	album
ABBA	ABBA	kA	ABBA
The	The	k1gMnSc1	The
Album	album	k1gNnSc1	album
uspokojena	uspokojen	k2eAgFnSc1d1	uspokojena
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
sovětského	sovětský	k2eAgNnSc2d1	sovětské
vedení	vedení	k1gNnSc2	vedení
pouze	pouze	k6eAd1	pouze
200	[number]	k4	200
000	[number]	k4	000
kusy	kus	k1gInPc7	kus
<g/>
.	.	kIx.	.
</s>
<s>
Autory	autor	k1gMnPc7	autor
všech	všecek	k3xTgFnPc2	všecek
skladeb	skladba	k1gFnPc2	skladba
byli	být	k5eAaImAgMnP	být
Benny	Benna	k1gFnSc2	Benna
Andersson	Andersson	k1gNnSc4	Andersson
a	a	k8xC	a
Björn	Björn	k1gNnSc4	Björn
Ulvaeus	Ulvaeus	k1gInSc1	Ulvaeus
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
umisťovalo	umisťovat	k5eAaImAgNnS	umisťovat
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
příčkách	příčka	k1gFnPc6	příčka
světových	světový	k2eAgFnPc2d1	světová
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
skupiny	skupina	k1gFnPc1	skupina
ABBA	ABBA	kA	ABBA
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
povědomí	povědomí	k1gNnSc6	povědomí
posluchačů	posluchač	k1gMnPc2	posluchač
i	i	k8xC	i
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
písně	píseň	k1gFnPc1	píseň
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Mamma	Mammum	k1gNnSc2	Mammum
Mia	Mia	k1gFnSc2	Mia
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
nějak	nějak	k6eAd1	nějak
podíleli	podílet	k5eAaImAgMnP	podílet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
pokusu	pokus	k1gInSc3	pokus
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
jejich	jejich	k3xOp3gInPc2	jejich
talentů	talent	k1gInPc2	talent
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
když	když	k8xS	když
oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
jely	jet	k5eAaImAgInP	jet
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Kypr	Kypr	k1gInSc1	Kypr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
začalo	začít	k5eAaPmAgNnS	začít
jako	jako	k9	jako
zpěv	zpěv	k1gInSc4	zpěv
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
skončilo	skončit	k5eAaPmAgNnS	skončit
jako	jako	k9	jako
improvizované	improvizovaný	k2eAgNnSc1d1	improvizované
živé	živý	k2eAgNnSc1d1	živé
vystoupení	vystoupení	k1gNnSc1	vystoupení
před	před	k7c7	před
vojáky	voják	k1gMnPc7	voják
OSN	OSN	kA	OSN
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Agnetha	Agnetha	k1gFnSc1	Agnetha
<g/>
,	,	kIx,	,
Björn	Björn	k1gInSc1	Björn
<g/>
,	,	kIx,	,
Benny	Benn	k1gInPc1	Benn
a	a	k8xC	a
Anni-Frid	Anni-Frid	k1gInSc1	Anni-Frid
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
kavárně	kavárna	k1gFnSc6	kavárna
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
prvním	první	k4xOgInSc6	první
veřejném	veřejný	k2eAgNnSc6d1	veřejné
vystoupení	vystoupení	k1gNnSc6	vystoupení
následovalo	následovat	k5eAaImAgNnS	následovat
mezi	mezi	k7c7	mezi
prosincem	prosinec	k1gInSc7	prosinec
1970	[number]	k4	1970
a	a	k8xC	a
únorem	únor	k1gInSc7	únor
1971	[number]	k4	1971
turné	turné	k1gNnSc2	turné
po	po	k7c6	po
švédských	švédský	k2eAgInPc6d1	švédský
klubech	klub	k1gInPc6	klub
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
turné	turné	k1gNnSc1	turné
však	však	k9	však
žádný	žádný	k3yNgInSc4	žádný
úspěch	úspěch	k1gInSc4	úspěch
nepřineslo	přinést	k5eNaPmAgNnS	přinést
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jasný	jasný	k2eAgInSc1d1	jasný
propadák	propadák	k1gInSc1	propadák
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zkušenost	zkušenost	k1gFnSc1	zkušenost
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
čas	čas	k1gInSc4	čas
odradila	odradit	k5eAaPmAgFnS	odradit
od	od	k7c2	od
společné	společný	k2eAgFnSc2d1	společná
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Agnetha	Agnetha	k1gFnSc1	Agnetha
i	i	k8xC	i
Anni-Frid	Anni-Frid	k1gInSc1	Anni-Frid
(	(	kIx(	(
<g/>
Frida	Frida	k1gFnSc1	Frida
<g/>
)	)	kIx)	)
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
známými	známý	k2eAgFnPc7d1	známá
zpěvačkami	zpěvačka	k1gFnPc7	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Björn	Björn	k1gInSc1	Björn
s	s	k7c7	s
Bennym	Bennym	k1gInSc1	Bennym
skládali	skládat	k5eAaImAgMnP	skládat
písně	píseň	k1gFnPc4	píseň
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
skupiny	skupina	k1gFnPc4	skupina
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
interprety	interpret	k1gMnPc4	interpret
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yInSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Jezdili	jezdit	k5eAaImAgMnP	jezdit
často	často	k6eAd1	často
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
začali	začít	k5eAaPmAgMnP	začít
brát	brát	k5eAaImF	brát
Agnethu	Agnetha	k1gFnSc4	Agnetha
a	a	k8xC	a
Fridu	Frida	k1gFnSc4	Frida
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Agnetha	Agnetha	k1gFnSc1	Agnetha
a	a	k8xC	a
Björn	Björn	k1gInSc1	Björn
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
Frida	Frida	k1gFnSc1	Frida
byla	být	k5eAaImAgFnS	být
zasnoubena	zasnoubit	k5eAaPmNgFnS	zasnoubit
s	s	k7c7	s
Bennym	Bennymum	k1gNnPc2	Bennymum
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
Agnetha	Agnetha	k1gMnSc1	Agnetha
<g/>
,	,	kIx,	,
Björn	Björn	k1gMnSc1	Björn
<g/>
,	,	kIx,	,
Benny	Benn	k1gMnPc4	Benn
a	a	k8xC	a
Anni-Frid	Anni-Frid	k1gInSc4	Anni-Frid
nahráli	nahrát	k5eAaBmAgMnP	nahrát
první	první	k4xOgFnSc4	první
píseň	píseň	k1gFnSc4	píseň
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
jazyce	jazyk	k1gInSc6	jazyk
"	"	kIx"	"
<g/>
People	People	k1gMnSc1	People
Need	Need	k1gMnSc1	Need
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jasně	jasně	k6eAd1	jasně
upřednostňovala	upřednostňovat	k5eAaImAgFnS	upřednostňovat
vokály	vokál	k1gInPc4	vokál
Agnethy	Agnetha	k1gFnSc2	Agnetha
a	a	k8xC	a
Fridy	Frida	k1gFnSc2	Frida
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
vyšla	vyjít	k5eAaPmAgFnS	vyjít
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1972	[number]	k4	1972
na	na	k7c6	na
singlu	singl	k1gInSc6	singl
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
první	první	k4xOgFnSc4	první
nahrávku	nahrávka	k1gFnSc4	nahrávka
a	a	k8xC	a
singl	singl	k1gInSc4	singl
skupiny	skupina	k1gFnSc2	skupina
ABBA	ABBA	kA	ABBA
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
desky	deska	k1gFnSc2	deska
uvedena	uvést	k5eAaPmNgFnS	uvést
pouze	pouze	k6eAd1	pouze
křestní	křestní	k2eAgNnPc4d1	křestní
jména	jméno	k1gNnPc4	jméno
všech	všecek	k3xTgMnPc2	všecek
čtyř	čtyři	k4xCgMnPc2	čtyři
členů	člen	k1gMnPc2	člen
<g/>
;	;	kIx,	;
BJÖRN	BJÖRN	kA	BJÖRN
&	&	k?	&
BENNY	BENNY	kA	BENNY
AGNETHA	AGNETHA	kA	AGNETHA
&	&	k?	&
ANNI-FRID	ANNI-FRID	k1gMnSc1	ANNI-FRID
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
mezi	mezi	k7c4	mezi
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
deset	deset	k4xCc4	deset
ve	v	k7c6	v
švédských	švédský	k2eAgFnPc6d1	švédská
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
povzbuzena	povzbuzen	k2eAgFnSc1d1	povzbuzena
úspěchem	úspěch	k1gInSc7	úspěch
začala	začít	k5eAaPmAgFnS	začít
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1972	[number]	k4	1972
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
prvním	první	k4xOgNnSc6	první
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1972	[number]	k4	1972
bylo	být	k5eAaImAgNnS	být
vydán	vydat	k5eAaPmNgInS	vydat
jejich	jejich	k3xOp3gInSc1	jejich
druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
He	he	k0	he
Is	Is	k1gMnSc4	Is
Your	Youra	k1gFnPc2	Youra
Brother	Brother	kA	Brother
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
vynesl	vynést	k5eAaPmAgMnS	vynést
na	na	k7c4	na
první	první	k4xOgFnSc4	první
místo	místo	k7c2	místo
švédské	švédský	k2eAgFnSc2d1	švédská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
švédského	švédský	k2eAgNnSc2d1	švédské
národního	národní	k2eAgNnSc2d1	národní
kola	kolo	k1gNnSc2	kolo
hudební	hudební	k2eAgFnSc2d1	hudební
soutěže	soutěž	k1gFnSc2	soutěž
Eurovision	Eurovision	k1gInSc1	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gInSc1	Contest
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Ring	ring	k1gInSc1	ring
Ring	ring	k1gInSc1	ring
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
systému	systém	k1gInSc3	systém
hlasování	hlasování	k1gNnPc2	hlasování
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
uspět	uspět	k5eAaPmF	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Pobouření	pobouřený	k2eAgMnPc1d1	pobouřený
fanoušci	fanoušek	k1gMnPc1	fanoušek
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
vynutili	vynutit	k5eAaPmAgMnP	vynutit
změnu	změna	k1gFnSc4	změna
systému	systém	k1gInSc2	systém
hodnocení	hodnocení	k1gNnSc2	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
ABBA	ABBA	kA	ABBA
se	se	k3xPyFc4	se
o	o	k7c4	o
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
Eurovizi	Eurovize	k1gFnSc6	Eurovize
pokusila	pokusit	k5eAaPmAgFnS	pokusit
znovu	znovu	k6eAd1	znovu
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Waterloo	Waterloo	k1gNnSc7	Waterloo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
byla	být	k5eAaImAgFnS	být
skupina	skupina	k1gFnSc1	skupina
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1974	[number]	k4	1974
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
vítězstvím	vítězství	k1gNnSc7	vítězství
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
svoji	svůj	k3xOyFgFnSc4	svůj
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1975	[number]	k4	1975
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgInS	dostavit
další	další	k2eAgInSc1d1	další
úspěch	úspěch	k1gInSc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
singlu	singl	k1gInSc6	singl
vyšla	vyjít	k5eAaPmAgFnS	vyjít
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
S.	S.	kA	S.
<g/>
O.S.	O.S.	k1gFnSc2	O.S.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
sedmé	sedmý	k4xOgNnSc4	sedmý
místo	místo	k1gNnSc4	místo
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
"	"	kIx"	"
<g/>
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1976	[number]	k4	1976
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stejné	stejný	k2eAgNnSc4d1	stejné
umístění	umístění	k1gNnSc4	umístění
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Fernando	Fernanda	k1gFnSc5	Fernanda
<g/>
"	"	kIx"	"
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
třetí	třetí	k4xOgNnSc1	třetí
nejlépe	dobře	k6eAd3	dobře
prodávanou	prodávaný	k2eAgFnSc7d1	prodávaná
kompilací	kompilace	k1gFnSc7	kompilace
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchy	úspěch	k1gInPc1	úspěch
rostly	růst	k5eAaImAgInP	růst
<g/>
,	,	kIx,	,
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
Dancing	dancing	k1gInSc1	dancing
Queen	Queen	k1gInSc1	Queen
<g/>
"	"	kIx"	"
obsadila	obsadit	k5eAaPmAgFnS	obsadit
ABBA	ABBA	kA	ABBA
opět	opět	k6eAd1	opět
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
i	i	k9	i
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
série	série	k1gFnSc1	série
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
hitů	hit	k1gInPc2	hit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prestižní	prestižní	k2eAgFnSc2d1	prestižní
UK	UK	kA	UK
TOP	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
jich	on	k3xPp3gMnPc2	on
skupina	skupina	k1gFnSc1	skupina
dostala	dostat	k5eAaPmAgFnS	dostat
19	[number]	k4	19
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
devětkrát	devětkrát	k6eAd1	devětkrát
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
vyjela	vyjet	k5eAaPmAgFnS	vyjet
skupina	skupina	k1gFnSc1	skupina
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zároveň	zároveň	k6eAd1	zároveň
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Abba	Abb	k1gInSc2	Abb
-	-	kIx~	-
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
mezinárodní	mezinárodní	k2eAgInPc1d1	mezinárodní
úspěchy	úspěch	k1gInPc1	úspěch
začaly	začít	k5eAaPmAgInP	začít
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
narušovat	narušovat	k5eAaImF	narušovat
osobní	osobní	k2eAgInPc4d1	osobní
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1978	[number]	k4	1978
se	se	k3xPyFc4	se
Frida	Frida	k1gFnSc1	Frida
a	a	k8xC	a
Benny	Benna	k1gFnSc2	Benna
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
požádali	požádat	k5eAaPmAgMnP	požádat
Agnetha	Agnetha	k1gMnSc1	Agnetha
a	a	k8xC	a
Björn	Björn	k1gMnSc1	Björn
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
rozpadu	rozpad	k1gInSc6	rozpad
ukončila	ukončit	k5eAaPmAgFnS	ukončit
ABBA	ABBA	kA	ABBA
novým	nový	k2eAgNnSc7d1	nové
albem	album	k1gNnSc7	album
a	a	k8xC	a
obřím	obří	k2eAgNnSc7d1	obří
turné	turné	k1gNnSc7	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1978	[number]	k4	1978
začala	začít	k5eAaPmAgFnS	začít
ABBA	ABBA	kA	ABBA
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
svém	své	k1gNnSc6	své
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
šestém	šestý	k4xOgNnSc6	šestý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
bylo	být	k5eAaImAgNnS	být
přijít	přijít	k5eAaPmF	přijít
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
deskou	deska	k1gFnSc7	deska
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
natáčení	natáčení	k1gNnSc4	natáčení
komplikoval	komplikovat	k5eAaBmAgMnS	komplikovat
rozpadající	rozpadající	k2eAgMnSc1d1	rozpadající
se	se	k3xPyFc4	se
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
Agnethou	Agnetha	k1gFnSc7	Agnetha
a	a	k8xC	a
Björnem	Björn	k1gInSc7	Björn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1978	[number]	k4	1978
si	se	k3xPyFc3	se
Benny	Benn	k1gInPc4	Benn
bral	brát	k5eAaImAgInS	brát
Fridu	Frid	k1gInSc2	Frid
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
manželství	manželství	k1gNnSc1	manželství
Agnethy	Agnetha	k1gFnSc2	Agnetha
a	a	k8xC	a
Björna	Björno	k1gNnSc2	Björno
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
se	se	k3xPyFc4	se
veřejnost	veřejnost	k1gFnSc1	veřejnost
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
až	až	k9	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
prý	prý	k9	prý
k	k	k7c3	k
"	"	kIx"	"
<g/>
přátelskému	přátelský	k2eAgMnSc3d1	přátelský
<g/>
"	"	kIx"	"
rozchodu	rozchod	k1gInSc3	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Agnethy	Agnetha	k1gFnSc2	Agnetha
a	a	k8xC	a
Björna	Björno	k1gNnSc2	Björno
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
napětí	napětí	k1gNnSc3	napětí
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
a	a	k8xC	a
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
se	se	k3xPyFc4	se
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1979	[number]	k4	1979
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
Voulez-Vous	Voulez-Vous	k1gInSc1	Voulez-Vous
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1980	[number]	k4	1980
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
vrátila	vrátit	k5eAaPmAgFnS	vrátit
po	po	k7c6	po
kratší	krátký	k2eAgFnSc6d2	kratší
odmlce	odmlka	k1gFnSc6	odmlka
opět	opět	k6eAd1	opět
na	na	k7c4	na
první	první	k4xOgNnPc4	první
místa	místo	k1gNnPc4	místo
singlových	singlový	k2eAgFnPc2d1	singlová
hitparád	hitparáda	k1gFnPc2	hitparáda
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
"	"	kIx"	"
<g/>
The	The	k1gMnPc1	The
Winner	Winner	k1gMnSc1	Winner
Takes	Takes	k1gMnSc1	Takes
It	It	k1gMnSc1	It
All	All	k1gMnSc1	All
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Super	super	k2eAgInSc1d1	super
Trouper	Trouper	k1gInSc1	Trouper
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
na	na	k7c6	na
sedmém	sedmý	k4xOgInSc6	sedmý
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
Super	super	k2eAgInSc1d1	super
Trouper	Trouper	k1gInSc1	Trouper
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
alba	alba	k1gFnSc1	alba
si	se	k3xPyFc3	se
nadále	nadále	k6eAd1	nadále
vedla	vést	k5eAaImAgFnS	vést
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
místech	místo	k1gNnPc6	místo
přes	přes	k7c4	přes
pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přelomu	přelom	k1gInSc3	přelom
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Frida	Frida	k1gFnSc1	Frida
a	a	k8xC	a
Benny	Benna	k1gFnSc2	Benna
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Plánované	plánovaný	k2eAgNnSc1d1	plánované
turné	turné	k1gNnSc1	turné
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neuskutečnilo	uskutečnit	k5eNaPmAgNnS	uskutečnit
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
natočit	natočit	k5eAaBmF	natočit
alespoň	alespoň	k9	alespoň
další	další	k2eAgFnSc4d1	další
studiovou	studiový	k2eAgFnSc4d1	studiová
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
The	The	k1gFnSc4	The
Visitors	Visitorsa	k1gFnPc2	Visitorsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
jeden	jeden	k4xCgInSc4	jeden
hit	hit	k1gInSc4	hit
"	"	kIx"	"
<g/>
One	One	k1gMnSc1	One
Of	Of	k1gMnSc1	Of
Us	Us	k1gMnSc1	Us
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
skupina	skupina	k1gFnSc1	skupina
slavila	slavit	k5eAaImAgFnS	slavit
10	[number]	k4	10
let	let	k1gInSc4	let
svého	svůj	k3xOyFgNnSc2	svůj
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
začala	začít	k5eAaPmAgFnS	začít
ABBA	ABBA	kA	ABBA
nahrávat	nahrávat	k5eAaImF	nahrávat
deváté	devátý	k4xOgNnSc4	devátý
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
srpna	srpen	k1gInSc2	srpen
bylo	být	k5eAaImAgNnS	být
nahrávání	nahrávání	k1gNnSc1	nahrávání
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
6	[number]	k4	6
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
Owe	Owe	k1gMnSc1	Owe
Me	Me	k1gMnSc1	Me
One	One	k1gMnSc1	One
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Just	just	k6eAd1	just
Like	Like	k1gNnSc1	Like
That	Thata	k1gFnPc2	Thata
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k8xC	i
Am	Am	k1gFnSc1	Am
The	The	k1gFnSc2	The
City	city	k1gNnSc1	city
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Cassandra	Cassandra	k1gFnSc1	Cassandra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Under	Under	k1gMnSc1	Under
Attack	Attack	k1gMnSc1	Attack
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Day	Day	k1gMnSc5	Day
Before	Befor	k1gMnSc5	Befor
You	You	k1gMnSc5	You
Came	Camus	k1gMnSc5	Camus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
singl	singl	k1gInSc1	singl
The	The	k1gMnSc5	The
Day	Day	k1gMnSc5	Day
Before	Befor	k1gMnSc5	Befor
You	You	k1gMnSc5	You
Came	Camus	k1gMnSc5	Camus
-	-	kIx~	-
Cassandra	Cassandr	k1gMnSc2	Cassandr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1982	[number]	k4	1982
vydala	vydat	k5eAaPmAgFnS	vydat
dvojité	dvojitý	k2eAgNnSc4d1	dvojité
album	album	k1gNnSc4	album
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
The	The	k1gFnSc2	The
Singles-The	Singles-The	k1gFnSc1	Singles-The
First	First	k1gFnSc1	First
Ten	ten	k3xDgInSc4	ten
Years	Years	k1gInSc4	Years
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
posledním	poslední	k2eAgNnSc7d1	poslední
propagačním	propagační	k2eAgNnSc7d1	propagační
turné	turné	k1gNnSc7	turné
ABBY	ABBY	kA	ABBY
po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1982	[number]	k4	1982
ABBA	ABBA	kA	ABBA
naposledy	naposledy	k6eAd1	naposledy
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
TV	TV	kA	TV
pořadu	pořad	k1gInSc6	pořad
"	"	kIx"	"
<g/>
Late	lat	k1gInSc5	lat
Late	lat	k1gInSc5	lat
Breakfast	Breakfast	k1gFnSc1	Breakfast
Show	show	k1gFnPc7	show
<g/>
"	"	kIx"	"
přenášeným	přenášený	k2eAgInPc3d1	přenášený
živě	živě	k6eAd1	živě
přes	přes	k7c4	přes
satelit	satelit	k1gInSc4	satelit
ze	z	k7c2	z
Stockholmu	Stockholm	k1gInSc2	Stockholm
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
vyšel	vyjít	k5eAaPmAgInS	vyjít
poslední	poslední	k2eAgInSc1d1	poslední
singl	singl	k1gInSc1	singl
Under	Under	k1gMnSc1	Under
Attack	Attack	k1gMnSc1	Attack
-	-	kIx~	-
You	You	k1gMnSc1	You
Owe	Owe	k1gMnSc1	Owe
Me	Me	k1gMnSc1	Me
One	One	k1gMnSc1	One
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
ABBA	ABBA	kA	ABBA
dává	dávat	k5eAaImIp3nS	dávat
"	"	kIx"	"
<g/>
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
přestávku	přestávka	k1gFnSc4	přestávka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Agnetha	Agnetha	k1gFnSc1	Agnetha
s	s	k7c7	s
Fridou	Frida	k1gFnSc7	Frida
natočily	natočit	k5eAaBmAgFnP	natočit
několik	několik	k4yIc4	několik
sólových	sólový	k2eAgFnPc2d1	sólová
desek	deska	k1gFnPc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Benny	Benn	k1gInPc1	Benn
a	a	k8xC	a
Björn	Björn	k1gInSc1	Björn
píší	psát	k5eAaImIp3nP	psát
muzikál	muzikál	k1gInSc4	muzikál
"	"	kIx"	"
<g/>
Chess	Chess	k1gInSc4	Chess
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
ABBA	ABBA	kA	ABBA
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
veřejně	veřejně	k6eAd1	veřejně
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1986	[number]	k4	1986
v	v	k7c6	v
TV	TV	kA	TV
show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
skupinu	skupina	k1gFnSc4	skupina
ABBA	ABBA	kA	ABBA
nastal	nastat	k5eAaPmAgInS	nastat
opět	opět	k6eAd1	opět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
díky	díky	k7c3	díky
britskému	britský	k2eAgNnSc3d1	Britské
duu	duo	k1gNnSc3	duo
Erasure	Erasur	k1gMnSc5	Erasur
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
EP	EP	kA	EP
ABBA-Esque	ABBA-Esque	k1gFnSc4	ABBA-Esque
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
hitparádách	hitparáda	k1gFnPc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hold	hold	k1gInSc1	hold
Abbě	Abba	k1gFnSc3	Abba
probudil	probudit	k5eAaPmAgInS	probudit
nebývalý	bývalý	k2eNgInSc1d1	bývalý
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
originální	originální	k2eAgFnPc4d1	originální
nahrávky	nahrávka	k1gFnPc4	nahrávka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
multiplatinového	multiplatinový	k2eAgNnSc2d1	multiplatinové
alba	album	k1gNnSc2	album
ABBA	ABBA	kA	ABBA
Gold	Golda	k1gFnPc2	Golda
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
také	také	k9	také
alba	album	k1gNnPc4	album
More	mor	k1gInSc5	mor
ABBA	ABBA	kA	ABBA
Gold	Gold	k1gInSc1	Gold
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vycházejí	vycházet	k5eAaImIp3nP	vycházet
další	další	k2eAgFnPc1d1	další
nové	nový	k2eAgFnPc1d1	nová
kompilace	kompilace	k1gFnPc1	kompilace
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
ta	ten	k3xDgFnSc1	ten
nejobsáhlejší	obsáhlý	k2eAgInSc4d3	nejobsáhlejší
Thank	Thank	k1gInSc4	Thank
You	You	k1gFnSc2	You
For	forum	k1gNnPc2	forum
The	The	k1gMnSc1	The
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgMnSc1d1	obsahující
4	[number]	k4	4
CD	CD	kA	CD
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
raritního	raritní	k2eAgInSc2d1	raritní
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
skupinu	skupina	k1gFnSc4	skupina
začal	začít	k5eAaPmAgInS	začít
opět	opět	k6eAd1	opět
obnovovat	obnovovat	k5eAaImF	obnovovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2008	[number]	k4	2008
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
filmový	filmový	k2eAgInSc4d1	filmový
přepis	přepis	k1gInSc4	přepis
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
muzikálu	muzikál	k1gInSc2	muzikál
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
!	!	kIx.	!
</s>
<s>
s	s	k7c7	s
Meryl	Meryl	k1gInSc1	Meryl
Streepovou	Streepová	k1gFnSc4	Streepová
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc4d1	obsahující
největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Muzikál	muzikál	k1gInSc1	muzikál
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Madonna	Madonna	k1gFnSc1	Madonna
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
hitem	hit	k1gInSc7	hit
"	"	kIx"	"
<g/>
Hung	Hung	k1gInSc1	Hung
Up	Up	k1gFnSc1	Up
<g/>
"	"	kIx"	"
postaveným	postavený	k2eAgNnSc7d1	postavené
na	na	k7c6	na
úryvcích	úryvek	k1gInPc6	úryvek
z	z	k7c2	z
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Gimme	Gimm	k1gInSc5	Gimm
<g/>
!	!	kIx.	!
</s>
<s>
Gimme	Gimmat	k5eAaPmIp3nS	Gimmat
<g/>
!	!	kIx.	!
</s>
<s>
Gimme	Gimmat	k5eAaPmIp3nS	Gimmat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oznámila	oznámit	k5eAaPmAgFnS	oznámit
svou	svůj	k3xOyFgFnSc4	svůj
touhu	touha	k1gFnSc4	touha
nahrávat	nahrávat	k5eAaImF	nahrávat
s	s	k7c7	s
dámskou	dámský	k2eAgFnSc7d1	dámská
polovinou	polovina	k1gFnSc7	polovina
Abby	Abba	k1gFnSc2	Abba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ačkoli	ačkoli	k8xS	ačkoli
skupina	skupina	k1gFnSc1	skupina
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
miliardovou	miliardový	k2eAgFnSc4d1	miliardová
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc4	uspořádání
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Robbie	Robbie	k1gFnSc2	Robbie
Williams	Williams	k1gInSc1	Williams
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
dát	dát	k5eAaPmF	dát
skupinu	skupina	k1gFnSc4	skupina
opět	opět	k6eAd1	opět
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ještě	ještě	k9	ještě
za	za	k7c2	za
existence	existence	k1gFnSc2	existence
Abby	Abba	k1gFnSc2	Abba
vydala	vydat	k5eAaPmAgFnS	vydat
první	první	k4xOgFnSc4	první
sólovou	sólový	k2eAgFnSc4d1	sólová
desku	deska	k1gFnSc4	deska
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
Frida	Frida	k1gFnSc1	Frida
(	(	kIx(	(
<g/>
Anni-Frid	Anni-Frid	k1gInSc1	Anni-Frid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
producentským	producentský	k2eAgInSc7d1	producentský
dohledem	dohled	k1gInSc7	dohled
Phila	Philo	k1gNnSc2	Philo
Collinse	Collinse	k1gFnSc2	Collinse
nahrála	nahrát	k5eAaBmAgFnS	nahrát
rockovější	rockový	k2eAgNnSc4d2	rockovější
album	album	k1gNnSc4	album
Something	Something	k1gInSc1	Something
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Going	Going	k1gMnSc1	Going
On	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
s	s	k7c7	s
hitem	hit	k1gInSc7	hit
I	i	k9	i
Know	Know	k1gMnSc5	Know
There	Ther	k1gMnSc5	Ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Something	Something	k1gInSc1	Something
Going	Going	k1gInSc1	Going
On	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
přišlo	přijít	k5eAaPmAgNnS	přijít
album	album	k1gNnSc1	album
Shine	Shin	k1gInSc5	Shin
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
odklonila	odklonit	k5eAaPmAgFnS	odklonit
od	od	k7c2	od
popu	pop	k1gInSc2	pop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vydala	vydat	k5eAaPmAgFnS	vydat
švédsky	švédsky	k6eAd1	švédsky
zpívanou	zpívaný	k2eAgFnSc4d1	zpívaná
kolekci	kolekce	k1gFnSc4	kolekce
Djupa	Djupa	k1gFnSc1	Djupa
Andetag	Andetaga	k1gFnPc2	Andetaga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
severských	severský	k2eAgFnPc6d1	severská
zemích	zem	k1gFnPc6	zem
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Frida	Frida	k1gFnSc1	Frida
se	se	k3xPyFc4	se
chystala	chystat	k5eAaImAgFnS	chystat
natočit	natočit	k5eAaBmF	natočit
album	album	k1gNnSc4	album
také	také	k9	také
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
zabránily	zabránit	k5eAaPmAgFnP	zabránit
tragické	tragický	k2eAgFnPc1d1	tragická
události	událost	k1gFnPc1	událost
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
autonehodě	autonehoda	k1gFnSc6	autonehoda
zemřela	zemřít	k5eAaPmAgFnS	zemřít
zpěvaččina	zpěvaččin	k2eAgFnSc1d1	zpěvaččina
dcera	dcera	k1gFnSc1	dcera
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Frida	Frida	k1gFnSc1	Frida
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
a	a	k8xC	a
nepodnikala	podnikat	k5eNaImAgFnS	podnikat
žádné	žádný	k3yNgFnPc4	žádný
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
natočila	natočit	k5eAaBmAgFnS	natočit
novou	nový	k2eAgFnSc4d1	nová
píseň	píseň	k1gFnSc4	píseň
-	-	kIx~	-
duet	duet	k1gInSc4	duet
s	s	k7c7	s
operní	operní	k2eAgFnSc7d1	operní
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Filippou	Filippý	k2eAgFnSc7d1	Filippý
Giordano	Giordana	k1gFnSc5	Giordana
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
na	na	k7c6	na
singlu	singl	k1gInSc6	singl
zpěváka	zpěvák	k1gMnSc2	zpěvák
Dana	Dan	k1gMnSc2	Dan
Daniela	Daniel	k1gMnSc2	Daniel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
udělal	udělat	k5eAaPmAgMnS	udělat
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
hitu	hit	k1gInSc2	hit
Abby	Abba	k1gFnSc2	Abba
I	i	k9	i
Have	Have	k1gFnSc4	Have
a	a	k8xC	a
Dream	Dream	k1gInSc4	Dream
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
hudební	hudební	k2eAgInSc4d1	hudební
počin	počin	k1gInSc4	počin
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Frida	Frida	k1gFnSc1	Frida
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
pro	pro	k7c4	pro
autorské	autorský	k2eAgNnSc4d1	autorské
album	album	k1gNnSc4	album
Jona	Jonus	k1gMnSc2	Jonus
Lorda	lord	k1gMnSc2	lord
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
píseň	píseň	k1gFnSc1	píseň
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
The	The	k1gFnSc2	The
Sun	Sun	kA	Sun
Will	Will	k1gMnSc1	Will
Shine	Shin	k1gInSc5	Shin
Again	Again	k2eAgMnSc1d1	Again
<g/>
.	.	kIx.	.
</s>
<s>
Agnetha	Agnetha	k1gFnSc1	Agnetha
Fältskog	Fältskoga	k1gFnPc2	Fältskoga
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Mikem	Mik	k1gMnSc7	Mik
Chapmanem	Chapman	k1gMnSc7	Chapman
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Wrap	Wrap	k1gMnSc1	Wrap
Your	Your	k1gMnSc1	Your
Arms	Armsa	k1gFnPc2	Armsa
Around	Around	k1gMnSc1	Around
Me	Me	k1gMnSc1	Me
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
desky	deska	k1gFnPc1	deska
Eyes	Eyesa	k1gFnPc2	Eyesa
Of	Of	k1gFnPc2	Of
a	a	k8xC	a
Woman	Womana	k1gFnPc2	Womana
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
a	a	k8xC	a
I	i	k9	i
Stand	Standa	k1gFnPc2	Standa
Alone	Alon	k1gInSc5	Alon
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgFnP	mít
spíše	spíše	k9	spíše
lokální	lokální	k2eAgInSc4d1	lokální
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
do	do	k7c2	do
soukromí	soukromí	k1gNnSc2	soukromí
a	a	k8xC	a
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
už	už	k9	už
jen	jen	k9	jen
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
po	po	k7c6	po
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
s	s	k7c7	s
albem	album	k1gNnSc7	album
My	my	k3xPp1nPc1	my
Colouring	Colouring	k1gInSc1	Colouring
Book	Book	k1gInSc4	Book
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
své	svůj	k3xOyFgFnPc4	svůj
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
coververze	coververze	k1gFnPc4	coververze
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
i	i	k8xC	i
singl	singl	k1gInSc1	singl
If	If	k1gMnPc2	If
I	i	k9	i
Thought	Thought	k2eAgMnSc1d1	Thought
You	You	k1gMnSc1	You
<g/>
́	́	k?	́
<g/>
d	d	k?	d
Ever	Ever	k1gMnSc1	Ever
Change	change	k1gFnSc2	change
Your	Your	k1gMnSc1	Your
Mind	Mind	k1gMnSc1	Mind
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Benny	Benna	k1gMnSc2	Benna
Andersson	Andersson	k1gMnSc1	Andersson
a	a	k8xC	a
Björn	Björn	k1gMnSc1	Björn
Ulvaeus	Ulvaeus	k1gMnSc1	Ulvaeus
se	se	k3xPyFc4	se
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
skupiny	skupina	k1gFnSc2	skupina
věnují	věnovat	k5eAaImIp3nP	věnovat
komponování	komponování	k1gNnPc1	komponování
muzikálů	muzikál	k1gInPc2	muzikál
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
jiné	jiný	k2eAgMnPc4d1	jiný
interprety	interpret	k1gMnPc4	interpret
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
ohlas	ohlas	k1gInSc1	ohlas
měl	mít	k5eAaImAgInS	mít
jejich	jejich	k3xOp3gInSc4	jejich
muzikál	muzikál	k1gInSc4	muzikál
The	The	k1gFnSc1	The
Chess	Chess	k1gInSc1	Chess
(	(	kIx(	(
<g/>
Šachy	šach	k1gInPc1	šach
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
s	s	k7c7	s
hity	hit	k1gInPc7	hit
One	One	k1gMnPc2	One
Night	Night	k1gInSc1	Night
In	In	k1gFnPc2	In
Bangok	Bangok	k1gInSc4	Bangok
a	a	k8xC	a
I	i	k9	i
Know	Know	k1gMnSc1	Know
Him	Him	k1gMnSc1	Him
So	So	kA	So
Well	Well	k1gMnSc1	Well
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
hudební	hudební	k2eAgNnSc4d1	hudební
zpracování	zpracování	k1gNnSc4	zpracování
historického	historický	k2eAgInSc2d1	historický
příběhu	příběh	k1gInSc2	příběh
ze	z	k7c2	z
švédských	švédský	k2eAgFnPc2d1	švédská
dějin	dějiny	k1gFnPc2	dějiny
Kristina	Kristin	k2eAgFnSc1d1	Kristina
frå	frå	k?	frå
Duvemå	Duvemå	k1gFnSc1	Duvemå
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Agnetha	Agnetha	k1gMnSc1	Agnetha
Fältskog	Fältskog	k1gMnSc1	Fältskog
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
duben	duben	k1gInSc4	duben
1950	[number]	k4	1950
Jönköping	Jönköping	k1gInSc4	Jönköping
<g/>
)	)	kIx)	)
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Björn	Björn	k1gMnSc1	Björn
Ulvaeus	Ulvaeus	k1gMnSc1	Ulvaeus
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
duben	duben	k1gInSc4	duben
1945	[number]	k4	1945
Göteborg	Göteborg	k1gInSc4	Göteborg
<g/>
)	)	kIx)	)
-	-	kIx~	-
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
aranžmá	aranžmá	k1gNnSc1	aranžmá
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
benžo	benžo	k1gNnSc1	benžo
<g/>
,	,	kIx,	,
mandolína	mandolína	k1gFnSc1	mandolína
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zpěv	zpěv	k1gInSc1	zpěv
Benny	Benna	k1gFnSc2	Benna
Andersson	Anderssona	k1gFnPc2	Anderssona
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc4	prosinec
1946	[number]	k4	1946
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
)	)	kIx)	)
-	-	kIx~	-
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
aranžmá	aranžmá	k1gNnSc1	aranžmá
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
<g/>
,	,	kIx,	,
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
a	a	k8xC	a
syntezátory	syntezátor	k1gInPc4	syntezátor
<g/>
,	,	kIx,	,
piáno	piáno	k?	piáno
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Anni-Frid	Anni-Frid	k1gInSc1	Anni-Frid
Lyngstad	Lyngstad	k1gInSc4	Lyngstad
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1945	[number]	k4	1945
Björkå	Björkå	k1gFnPc2	Björkå
<g/>
/	/	kIx~	/
<g/>
Ballangen	Ballangen	k1gInSc1	Ballangen
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
ABBY	ABBY	kA	ABBY
<g/>
.	.	kIx.	.
</s>
<s>
Ring	ring	k1gInSc1	ring
Ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Waterloo	Waterloo	k1gNnSc2	Waterloo
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
ABBA	ABBA	kA	ABBA
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Arrival	Arrival	k1gFnSc2	Arrival
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Voulez-Vous	Voulez-Vous	k1gInSc1	Voulez-Vous
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Super	super	k2eAgInSc1d1	super
Trouper	Trouper	k1gInSc1	Trouper
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Visitors	Visitorsa	k1gFnPc2	Visitorsa
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Gracias	Gracias	k1gMnSc1	Gracias
Por	Por	k1gMnSc2	Por
La	la	k1gNnSc2	la
Música	Músicus	k1gMnSc2	Músicus
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Singles	Singles	k1gMnSc1	Singles
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
First	First	k1gMnSc1	First
Ten	ten	k3xDgInSc4	ten
Years	Years	k1gInSc4	Years
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
People	People	k1gMnSc1	People
Need	Need	k1gMnSc1	Need
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
He	he	k0	he
Is	Is	k1gMnSc4	Is
Your	Youra	k1gFnPc2	Youra
Brother	Brother	kA	Brother
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Ring	ring	k1gInSc1	ring
Ring	ring	k1gInSc1	ring
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Love	lov	k1gInSc5	lov
Isn	Isn	k1gFnSc6	Isn
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Easy	Easa	k1gMnSc2	Easa
(	(	kIx(	(
<g/>
But	But	k1gMnSc2	But
It	It	k1gMnSc2	It
Sure	Sur	k1gMnSc2	Sur
Is	Is	k1gMnSc1	Is
Hard	Hard	k1gMnSc1	Hard
Enough	Enough	k1gMnSc1	Enough
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Waterloo	Waterloo	k1gNnSc2	Waterloo
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Honey	Honea	k1gFnPc1	Honea
<g/>
,	,	kIx,	,
Honey	Honey	k1gInPc1	Honey
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
So	So	kA	So
Long	Long	k1gMnSc1	Long
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
People	People	k1gMnSc1	People
Need	Need	k1gMnSc1	Need
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
He	he	k0	he
Is	Is	k1gMnSc4	Is
Your	Youra	k1gFnPc2	Youra
Brother	Brother	kA	Brother
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Ring	ring	k1gInSc1	ring
<g/>
,	,	kIx,	,
Ring	ring	k1gInSc1	ring
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Another	Anothra	k1gFnPc2	Anothra
Town	Towna	k1gFnPc2	Towna
<g/>
,	,	kIx,	,
Another	Anothra	k1gFnPc2	Anothra
Train	Traina	k1gFnPc2	Traina
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nina	Nina	k1gFnSc1	Nina
<g/>
,	,	kIx,	,
Pretty	Pretta	k1gMnSc2	Pretta
Ballerina	Ballerin	k1gMnSc2	Ballerin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Isn	Isn	k1gFnSc6	Isn
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Easy	Easa	k1gMnSc2	Easa
(	(	kIx(	(
<g/>
But	But	k1gMnSc2	But
It	It	k1gMnSc2	It
Sure	Sur	k1gMnSc2	Sur
Is	Is	k1gMnSc1	Is
Hard	Hard	k1gMnSc1	Hard
Enough	Enough	k1gMnSc1	Enough
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Waterloo	Waterloo	k1gNnSc1	Waterloo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Hasta	Hasta	k1gMnSc1	Hasta
Mañ	Mañ	k1gMnSc1	Mañ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Honey	Honea	k1gFnPc1	Honea
<g/>
,	,	kIx,	,
Honey	Honea	k1gFnPc1	Honea
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dance	Danka	k1gFnSc6	Danka
(	(	kIx(	(
<g/>
While	While	k1gFnSc1	While
The	The	k1gMnSc1	The
Music	Music	k1gMnSc1	Music
Still	Still	k1gMnSc1	Still
Goes	Goes	k1gInSc4	Goes
On	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
S.	S.	kA	S.
<g/>
O.S	O.S	k1gFnSc2	O.S
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k9	i
Do	do	k7c2	do
<g/>
,	,	kIx,	,
I	i	k8xC	i
Do	do	k7c2	do
<g/>
,	,	kIx,	,
I	i	k8xC	i
Do	do	k7c2	do
<g/>
,	,	kIx,	,
I	i	k8xC	i
Do	do	k7c2	do
<g/>
,	,	kIx,	,
I	i	k8xC	i
Do	do	k7c2	do
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Bang-A-Boomerang	Bang-A-Boomerang	k1gInSc1	Bang-A-Boomerang
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Rock	rock	k1gInSc1	rock
Me	Me	k1gFnSc2	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
So	So	kA	So
long	long	k1gInSc1	long
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c4	v
Been	Been	k1gInSc4	Been
Waiting	Waiting	k1gInSc4	Waiting
For	forum	k1gNnPc2	forum
You	You	k1gMnSc2	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Fernando	Fernanda	k1gFnSc5	Fernanda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Dancing	dancing	k1gInSc1	dancing
Queen	Queen	k1gInSc1	Queen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Money	Monea	k1gFnPc1	Monea
<g/>
,	,	kIx,	,
Money	Money	k1gInPc1	Money
<g/>
,	,	kIx,	,
Money	Money	k1gInPc1	Money
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Knowing	Knowing	k1gInSc1	Knowing
Me	Me	k1gFnSc2	Me
<g />
.	.	kIx.	.
</s>
<s>
Knowing	Knowing	k1gInSc1	Knowing
You	You	k1gFnSc2	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
That	That	k1gInSc1	That
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Me	Me	k1gFnSc7	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
When	When	k1gInSc1	When
I	i	k8xC	i
Kissed	Kissed	k1gInSc1	Kissed
the	the	k?	the
Teacher	Teachra	k1gFnPc2	Teachra
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Eagle	Eagle	k1gInSc1	Eagle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Name	Nam	k1gFnSc2	Nam
Of	Of	k1gFnSc1	Of
The	The	k1gMnSc1	The
Game	game	k1gInSc1	game
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Take	Take	k1gFnSc6	Take
A	a	k8xC	a
Chance	Chanka	k1gFnSc6	Chanka
On	on	k3xPp3gMnSc1	on
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Thank	Thank	k1gMnSc1	Thank
You	You	k1gFnSc2	You
For	forum	k1gNnPc2	forum
The	The	k1gMnSc1	The
Music	Music	k1gMnSc1	Music
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
One	One	k1gMnSc1	One
Man	Man	k1gMnSc1	Man
<g/>
,	,	kIx,	,
One	One	k1gMnSc1	One
Woman	Woman	k1gMnSc1	Woman
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Summer	Summer	k1gInSc1	Summer
Night	Nighta	k1gFnPc2	Nighta
City	City	k1gFnSc2	City
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Chiquitita	Chiquitita	k1gFnSc1	Chiquitita
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Does	Does	k1gInSc1	Does
Your	Your	k1gMnSc1	Your
Mother	Mothra	k1gFnPc2	Mothra
Know	Know	k1gMnSc1	Know
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
As	as	k1gInSc1	as
Good	Gooda	k1gFnPc2	Gooda
As	as	k9	as
New	New	k1gMnPc2	New
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	i	k9	i
Have	Have	k1gNnSc1	Have
A	a	k8xC	a
Dream	Dream	k1gInSc1	Dream
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Voulez-Vous	Voulez-Vous	k1gInSc1	Voulez-Vous
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Angeleyes	Angeleyes	k1gInSc1	Angeleyes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Gimme	Gimm	k1gInSc5	Gimm
<g/>
!	!	kIx.	!
</s>
<s>
Gimme	Gimmat	k5eAaPmIp3nS	Gimmat
<g/>
!	!	kIx.	!
</s>
<s>
Gimme	Gimmat	k5eAaPmIp3nS	Gimmat
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
A	a	k9	a
Man	Man	k1gMnSc1	Man
After	After	k1gMnSc1	After
Midnight	Midnight	k1gMnSc1	Midnight
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Winner	Winner	k1gMnSc1	Winner
Takes	Takes	k1gMnSc1	Takes
It	It	k1gMnSc1	It
All	All	k1gMnSc1	All
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Super	super	k2eAgInSc1d1	super
Trouper	Trouper	k1gInSc1	Trouper
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Happy	Happa	k1gFnPc1	Happa
New	New	k1gFnSc2	New
Year	Yeara	k1gFnPc2	Yeara
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Lay	Lay	k1gMnSc1	Lay
All	All	k1gMnSc1	All
Your	Your	k1gMnSc1	Your
Love	lov	k1gInSc5	lov
On	on	k3xPp3gMnSc1	on
<g />
.	.	kIx.	.
</s>
<s>
Me	Me	k?	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
On	on	k3xPp3gMnSc1	on
And	Anda	k1gFnPc2	Anda
On	on	k3xPp3gMnSc1	on
And	Anda	k1gFnPc2	Anda
On	on	k3xPp3gMnSc1	on
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Slipping	Slipping	k1gInSc1	Slipping
Through	Througha	k1gFnPc2	Througha
my	my	k3xPp1nPc1	my
Fingers	Fingersa	k1gFnPc2	Fingersa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
One	One	k1gMnSc1	One
Of	Of	k1gMnSc1	Of
Us	Us	k1gMnSc1	Us
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Head	Head	k1gInSc1	Head
Over	Overa	k1gFnPc2	Overa
Heels	Heelsa	k1gFnPc2	Heelsa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
When	When	k1gInSc1	When
<g />
.	.	kIx.	.
</s>
<s>
All	All	k?	All
Is	Is	k1gMnSc1	Is
Said	Said	k1gMnSc1	Said
And	Anda	k1gFnPc2	Anda
Done	Don	k1gMnSc5	Don
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Visitors	Visitors	k1gInSc1	Visitors
(	(	kIx(	(
<g/>
Crackin	Crackin	k1gMnSc1	Crackin
<g/>
'	'	kIx"	'
Up	Up	k1gMnSc1	Up
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Cassandra	Cassandra	k1gFnSc1	Cassandra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc5	The
Day	Day	k1gMnSc5	Day
Before	Befor	k1gMnSc5	Befor
You	You	k1gMnSc5	You
Came	Camus	k1gMnSc5	Camus
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Under	Under	k1gMnSc1	Under
Attack	Attack	k1gMnSc1	Attack
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
OLDHAM	OLDHAM	kA	OLDHAM
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
<g/>
;	;	kIx,	;
CALDER	CALDER	kA	CALDER
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
<g/>
;	;	kIx,	;
IRWIN	IRWIN	kA	IRWIN
<g/>
,	,	kIx,	,
Colin	Colin	k1gMnSc1	Colin
<g/>
.	.	kIx.	.
</s>
<s>
ABBA	ABBA	kA	ABBA
=	=	kIx~	=
ABBA	ABBA	kA	ABBA
<g/>
,	,	kIx,	,
the	the	k?	the
name	name	k1gInSc1	name
of	of	k?	of
the	the	k?	the
game	game	k1gInSc1	game
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Alena	Alena	k1gFnSc1	Alena
Šmídová	Šmídová	k1gFnSc1	Šmídová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86070	[number]	k4	86070
<g/>
-	-	kIx~	-
<g/>
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
PALM	PALM	kA	PALM
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Magnus	Magnus	k1gMnSc1	Magnus
<g/>
.	.	kIx.	.
</s>
<s>
ABBA	ABBA	kA	ABBA
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
superskupiny	superskupina	k1gFnSc2	superskupina
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bojanovský	Bojanovský	k1gMnSc1	Bojanovský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
globator	globator	k1gInSc1	globator
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
734	[number]	k4	734
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ABBA	ABBA	kA	ABBA
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Abba	Abb	k1gInSc2	Abb
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc2	téma
ABBA	ABBA	kA	ABBA
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc2	galerie
ABBA	ABBA	kA	ABBA
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Abba	Abba	k1gFnSc1	Abba
-	-	kIx~	-
The	The	k1gFnSc1	The
Site	Sit	k1gFnSc2	Sit
-	-	kIx~	-
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Vlastněné	vlastněný	k2eAgFnPc1d1	vlastněná
a	a	k8xC	a
udržované	udržovaný	k2eAgFnPc1d1	udržovaná
Universal	Universal	k1gFnPc1	Universal
Music	Musice	k1gInPc2	Musice
AB	AB	kA	AB
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomovy	Tomův	k2eAgFnSc2d1	Tomova
ABBA	ABBA	kA	ABBA
stránky	stránka	k1gFnSc2	stránka
-	-	kIx~	-
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hep	Hep	k1gFnSc1	Hep
Stars	Stars	k1gInSc1	Stars
International	International	k1gMnSc1	International
Official	Official	k1gMnSc1	Official
website	websit	k1gInSc5	websit
-	-	kIx~	-
Benny	Benn	k1gMnPc4	Benn
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
skupinou	skupina	k1gFnSc7	skupina
ABBA	ABBA	kA	ABBA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
ABBA	ABBA	kA	ABBA
Wonder	Wonder	k1gInSc4	Wonder
Elektronický	elektronický	k2eAgInSc1d1	elektronický
časopis	časopis	k1gInSc1	časopis
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
fanklubu	fanklub	k1gInSc2	fanklub
skupiny	skupina	k1gFnSc2	skupina
ABBA	ABBA	kA	ABBA
<g/>
.	.	kIx.	.
</s>
<s>
ABBA	ABBA	kA	ABBA
Stars	Stars	k1gInSc1	Stars
-	-	kIx~	-
fanklub	fanklub	k1gInSc1	fanklub
české	český	k2eAgInPc1d1	český
revival	revival	k1gInSc1	revival
skupiny	skupina	k1gFnSc2	skupina
ABBA	ABBA	kA	ABBA
Stars	Stars	k1gInSc1	Stars
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
kariéru	kariéra	k1gFnSc4	kariéra
skupiny	skupina	k1gFnSc2	skupina
ABBA	ABBA	kA	ABBA
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
online	onlinout	k5eAaPmIp3nS	onlinout
přehrávání	přehrávání	k1gNnSc4	přehrávání
-	-	kIx~	-
pořad	pořad	k1gInSc1	pořad
Slavné	slavný	k2eAgInPc1d1	slavný
dny	den	k1gInPc1	den
na	na	k7c4	na
stream	stream	k1gInSc4	stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
