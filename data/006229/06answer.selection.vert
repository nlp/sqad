<s>
Sofiin	Sofiin	k2eAgInSc1d1	Sofiin
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
román	román	k1gInSc1	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
je	být	k5eAaImIp3nS	být
dílem	díl	k1gInSc7	díl
norského	norský	k2eAgMnSc2d1	norský
autora	autor	k1gMnSc2	autor
Josteina	Jostein	k1gMnSc2	Jostein
Gaardera	Gaarder	k1gMnSc2	Gaarder
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
