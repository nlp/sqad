<s>
Sofiin	Sofiin	k2eAgInSc1d1	Sofiin
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
román	román	k1gInSc1	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
je	být	k5eAaImIp3nS	být
dílem	díl	k1gInSc7	díl
norského	norský	k2eAgMnSc2d1	norský
autora	autor	k1gMnSc2	autor
Josteina	Jostein	k1gMnSc2	Jostein
Gaardera	Gaarder	k1gMnSc2	Gaarder
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
přeložen	přeložen	k2eAgInSc1d1	přeložen
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
světovým	světový	k2eAgInSc7d1	světový
trhákem	trhák	k1gInSc7	trhák
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
román	román	k1gInSc4	román
oscilující	oscilující	k2eAgInSc4d1	oscilující
mezi	mezi	k7c7	mezi
filosofickou	filosofický	k2eAgFnSc7d1	filosofická
příručkou	příručka	k1gFnSc7	příručka
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
a	a	k8xC	a
poutavým	poutavý	k2eAgInSc7d1	poutavý
dětským	dětský	k2eAgInSc7d1	dětský
příběhem	příběh	k1gInSc7	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
přiblížit	přiblížit	k5eAaPmF	přiblížit
základní	základní	k2eAgInPc4d1	základní
filosofické	filosofický	k2eAgInPc4d1	filosofický
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
otázky	otázka	k1gFnPc4	otázka
širokému	široký	k2eAgNnSc3d1	široké
publiku	publikum	k1gNnSc3	publikum
a	a	k8xC	a
snad	snad	k9	snad
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
znalosti	znalost	k1gFnSc2	znalost
rozšířit	rozšířit	k5eAaPmF	rozšířit
i	i	k9	i
humanitu	humanita	k1gFnSc4	humanita
a	a	k8xC	a
lidské	lidský	k2eAgFnPc4d1	lidská
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Sofie	Sofie	k1gFnSc1	Sofie
je	být	k5eAaImIp3nS	být
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
14	[number]	k4	14
<g/>
letá	letý	k2eAgFnSc1d1	letá
dívka	dívka	k1gFnSc1	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
nalezne	naleznout	k5eAaPmIp3nS	naleznout
ve	v	k7c6	v
schránce	schránka	k1gFnSc6	schránka
lístek	lístek	k1gInSc4	lístek
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
jsi	být	k5eAaImIp2nS	být
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
papírek	papírek	k1gInSc1	papírek
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
započal	započnout	k5eAaPmAgInS	započnout
kurz	kurz	k1gInSc1	kurz
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Sofie	Sofie	k1gFnSc1	Sofie
dostávala	dostávat	k5eAaImAgFnS	dostávat
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
obálku	obálka	k1gFnSc4	obálka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
filosofické	filosofický	k2eAgFnSc2d1	filosofická
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Nedostávala	dostávat	k5eNaImAgFnS	dostávat
však	však	k9	však
jen	jen	k9	jen
obálky	obálka	k1gFnPc1	obálka
<g/>
.	.	kIx.	.
</s>
<s>
Dostávala	dostávat	k5eAaImAgFnS	dostávat
také	také	k9	také
dopisy	dopis	k1gInPc1	dopis
pro	pro	k7c4	pro
Hildu	Hilda	k1gFnSc4	Hilda
Knagovou	Knagový	k2eAgFnSc4d1	Knagový
(	(	kIx(	(
<g/>
kterou	který	k3yQgFnSc4	který
neznala	znát	k5eNaImAgFnS	znát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
Hildě	Hilda	k1gFnSc6	Hilda
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
psal	psát	k5eAaImAgMnS	psát
přání	přání	k1gNnSc4	přání
k	k	k7c3	k
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Sofie	Sofie	k1gFnSc1	Sofie
začne	začít	k5eAaPmIp3nS	začít
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
učiteli	učitel	k1gMnSc6	učitel
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Alberto	Alberta	k1gFnSc5	Alberta
Knox	Knox	k1gInSc1	Knox
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
ale	ale	k9	ale
stane	stanout	k5eAaPmIp3nS	stanout
převrat	převrat	k1gInSc1	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Sofie	Sofie	k1gFnSc1	Sofie
a	a	k8xC	a
Alberto	Alberta	k1gFnSc5	Alberta
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
vymyšlené	vymyšlený	k2eAgFnPc1d1	vymyšlená
postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
Alberta	Albert	k1gMnSc2	Albert
Knaga	Knag	k1gMnSc2	Knag
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
Hildě	Hilda	k1gFnSc3	Hilda
k	k	k7c3	k
patnáctým	patnáctý	k4xOgFnPc3	patnáctý
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Alberto	Alberta	k1gFnSc5	Alberta
však	však	k9	však
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
z	z	k7c2	z
příběhu	příběh	k1gInSc2	příběh
dostat	dostat	k5eAaPmF	dostat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
Sofií	Sofia	k1gFnSc7	Sofia
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Setkají	setkat	k5eAaPmIp3nP	setkat
se	se	k3xPyFc4	se
se	s	k7c7	s
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
Hildou	Hilda	k1gFnSc7	Hilda
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
však	však	k9	však
nevidí	vidět	k5eNaImIp3nS	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
jsou	být	k5eAaImIp3nP	být
Alberto	Alberta	k1gFnSc5	Alberta
a	a	k8xC	a
Sofie	Sofia	k1gFnPc4	Sofia
neviditelní	viditelný	k2eNgMnPc1d1	Neviditelný
<g/>
.	.	kIx.	.
</s>
<s>
Sofiin	Sofiin	k2eAgInSc1d1	Sofiin
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
:	:	kIx,	:
Knižná	knižný	k2eAgNnPc1d1	knižný
dielňa	dielňum	k1gNnPc1	dielňum
Timotej	Timotej	k1gFnPc2	Timotej
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
512	[number]	k4	512
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
967294	[number]	k4	967294
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Sofiin	Sofiin	k2eAgInSc1d1	Sofiin
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
:	:	kIx,	:
Knižná	knižný	k2eAgNnPc1d1	knižný
dielňa	dielňum	k1gNnPc1	dielňum
Timotej	Timotej	k1gFnPc2	Timotej
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
427	[number]	k4	427
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
88849	[number]	k4	88849
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Sofiin	Sofiin	k2eAgInSc1d1	Sofiin
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
:	:	kIx,	:
Knižná	knižný	k2eAgNnPc1d1	knižný
dielňa	dielňum	k1gNnPc1	dielňum
Timotej	Timotej	k1gFnPc2	Timotej
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
427	[number]	k4	427
s.	s.	k?	s.
Sofiin	Sofiin	k2eAgInSc4d1	Sofiin
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
430	[number]	k4	430
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1036	[number]	k4	1036
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Sofiin	Sofiin	k2eAgInSc1d1	Sofiin
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
430	[number]	k4	430
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1748	[number]	k4	1748
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
