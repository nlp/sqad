<p>
<s>
Sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
či	či	k8xC	či
také	také	k9	také
vulkanické	vulkanický	k2eAgInPc1d1	vulkanický
plyny	plyn	k1gInPc1	plyn
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
produktů	produkt	k1gInPc2	produkt
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
je	on	k3xPp3gInPc4	on
směsice	směsice	k1gFnPc1	směsice
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
převážně	převážně	k6eAd1	převážně
jedovatých	jedovatý	k2eAgInPc2d1	jedovatý
horkých	horký	k2eAgInPc2d1	horký
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
uvolňováním	uvolňování	k1gNnSc7	uvolňování
z	z	k7c2	z
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
magmatu	magma	k1gNnSc2	magma
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
výstupu	výstup	k1gInSc6	výstup
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
interakcí	interakce	k1gFnPc2	interakce
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
magmatem	magma	k1gNnSc7	magma
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
magma	magma	k1gNnSc1	magma
dostane	dostat	k5eAaPmIp3nS	dostat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
lávou	láva	k1gFnSc7	láva
a	a	k8xC	a
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
volně	volně	k6eAd1	volně
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
významnými	významný	k2eAgInPc7d1	významný
skleníkovými	skleníkový	k2eAgInPc7d1	skleníkový
plyny	plyn	k1gInPc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
ale	ale	k8xC	ale
nadloží	nadloží	k1gNnSc6	nadloží
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
uvolňování	uvolňování	k1gNnSc1	uvolňování
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
akumulaci	akumulace	k1gFnSc3	akumulace
<g/>
,	,	kIx,	,
nárůstu	nárůst	k1gInSc3	nárůst
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
explozivní	explozivní	k2eAgFnSc7d1	explozivní
erupcí	erupce	k1gFnSc7	erupce
sopky	sopka	k1gFnSc2	sopka
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tefry	tefra	k1gFnSc2	tefra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
nejsou	být	k5eNaImIp3nP	být
tlaky	tlak	k1gInPc1	tlak
extrémní	extrémní	k2eAgInPc1d1	extrémní
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
postupného	postupný	k2eAgInSc2d1	postupný
průchodu	průchod	k1gInSc2	průchod
přes	přes	k7c4	přes
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
vznikem	vznik	k1gInSc7	vznik
fumarol	fumarola	k1gFnPc2	fumarola
<g/>
.	.	kIx.	.
<g/>
Sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
převážně	převážně	k6eAd1	převážně
vodní	vodní	k2eAgFnSc7d1	vodní
párou	pára	k1gFnSc7	pára
jakožto	jakožto	k8xS	jakožto
základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
všech	všecek	k3xTgInPc2	všecek
plynů	plyn	k1gInPc2	plyn
produkovaných	produkovaný	k2eAgInPc2d1	produkovaný
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
plyny	plyn	k1gInPc4	plyn
patři	patřit	k5eAaImRp2nS	patřit
různé	různý	k2eAgInPc1d1	různý
oxidy	oxid	k1gInPc1	oxid
síry	síra	k1gFnSc2	síra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fluorovodík	fluorovodík	k1gInSc1	fluorovodík
<g/>
,	,	kIx,	,
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
<g/>
,	,	kIx,	,
methan	methan	k1gInSc1	methan
<g/>
,	,	kIx,	,
fluorid	fluorid	k1gInSc1	fluorid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
,	,	kIx,	,
amoniak	amoniak	k1gInSc1	amoniak
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
jako	jako	k8xS	jako
např.	např.	kA	např.
karbonylsulfid	karbonylsulfid	k1gInSc1	karbonylsulfid
či	či	k8xC	či
radon	radon	k1gInSc1	radon
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
život	život	k1gInSc4	život
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
a	a	k8xC	a
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
uvolňování	uvolňování	k1gNnSc6	uvolňování
hrozí	hrozit	k5eAaImIp3nS	hrozit
zasaženému	zasažený	k2eAgMnSc3d1	zasažený
jedinci	jedinec	k1gMnSc3	jedinec
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
představuje	představovat	k5eAaImIp3nS	představovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyplňují	vyplňovat	k5eAaImIp3nP	vyplňovat
deprese	deprese	k1gFnPc4	deprese
či	či	k8xC	či
tečou	téct	k5eAaImIp3nP	téct
údolími	údolí	k1gNnPc7	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
současně	současně	k6eAd1	současně
i	i	k9	i
bez	bez	k7c2	bez
zápachu	zápach	k1gInSc2	zápach
a	a	k8xC	a
bezbarvé	bezbarvý	k2eAgFnSc2d1	bezbarvá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nejsou	být	k5eNaImIp3nP	být
člověkem	člověk	k1gMnSc7	člověk
rozpoznatelné	rozpoznatelný	k2eAgFnSc2d1	rozpoznatelná
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
výskyt	výskyt	k1gInSc1	výskyt
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jen	jen	k9	jen
určité	určitý	k2eAgFnSc2d1	určitá
výšky	výška	k1gFnSc2	výška
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
sopky	sopka	k1gFnSc2	sopka
Hekla	heknout	k5eAaPmAgFnS	heknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
plyny	plyn	k1gInPc1	plyn
zabíjely	zabíjet	k5eAaImAgInP	zabíjet
zvířata	zvíře	k1gNnPc1	zvíře
jen	jen	k9	jen
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
1	[number]	k4	1
metru	metr	k1gInSc2	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
stojícímu	stojící	k2eAgMnSc3d1	stojící
člověku	člověk	k1gMnSc3	člověk
nehrozilo	hrozit	k5eNaImAgNnS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
.	.	kIx.	.
<g/>
Množství	množství	k1gNnSc1	množství
produkovaných	produkovaný	k2eAgInPc2d1	produkovaný
plynů	plyn	k1gInPc2	plyn
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
rozdílem	rozdíl	k1gInSc7	rozdíl
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
taveninou	tavenina	k1gFnSc7	tavenina
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hloubi	hloub	k1gFnSc6	hloub
planety	planeta	k1gFnSc2	planeta
panují	panovat	k5eAaImIp3nP	panovat
značné	značný	k2eAgInPc4d1	značný
litostatické	litostatický	k2eAgInPc4d1	litostatický
tlaky	tlak	k1gInPc4	tlak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
plynu	plyn	k1gInSc2	plyn
uniknout	uniknout	k5eAaPmF	uniknout
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
litostatický	litostatický	k2eAgInSc1d1	litostatický
tlak	tlak	k1gInSc1	tlak
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
vzniku	vznik	k1gInSc3	vznik
bublin	bublina	k1gFnPc2	bublina
v	v	k7c6	v
tavenině	tavenina	k1gFnSc6	tavenina
tzv.	tzv.	kA	tzv.
vesikule	vesikule	k1gFnPc1	vesikule
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
vesikulí	vesikule	k1gFnPc2	vesikule
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
magma	magma	k1gNnSc1	magma
tak	tak	k6eAd1	tak
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
svoji	svůj	k3xOyFgFnSc4	svůj
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
výstupu	výstup	k1gInSc2	výstup
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
méně	málo	k6eAd2	málo
husté	hustý	k2eAgFnSc3d1	hustá
než	než	k8xS	než
okolní	okolní	k2eAgFnSc2d1	okolní
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
tlak	tlak	k1gInSc4	tlak
vysoký	vysoký	k2eAgInSc4d1	vysoký
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
menší	malý	k2eAgFnSc3d2	menší
produkci	produkce	k1gFnSc3	produkce
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
k	k	k7c3	k
menším	malý	k2eAgInPc3d2	menší
sopečným	sopečný	k2eAgInPc3d1	sopečný
mrakům	mrak	k1gInPc3	mrak
nad	nad	k7c7	nad
sopkou	sopka	k1gFnSc7	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
utuhne	utuhnout	k5eAaPmIp3nS	utuhnout
magma	magma	k1gNnSc4	magma
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
uniknou	uniknout	k5eAaPmIp3nP	uniknout
všechny	všechen	k3xTgInPc1	všechen
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
hornině	hornina	k1gFnSc6	hornina
malé	malý	k2eAgFnSc2d1	malá
vesikuly	vesikula	k1gFnSc2	vesikula
obsahující	obsahující	k2eAgNnSc4d1	obsahující
primární	primární	k2eAgNnSc4d1	primární
složení	složení	k1gNnSc4	složení
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
vesikuly	vesikul	k1gInPc1	vesikul
druhotně	druhotně	k6eAd1	druhotně
vyplněny	vyplněn	k2eAgInPc1d1	vyplněn
roztoky	roztok	k1gInPc1	roztok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zkrystalizují	zkrystalizovat	k5eAaPmIp3nP	zkrystalizovat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
drúzy	drúza	k1gFnPc4	drúza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
plynů	plyn	k1gInPc2	plyn
==	==	k?	==
</s>
</p>
<p>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
sopečné	sopečný	k2eAgFnSc2d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
rozdílně	rozdílně	k6eAd1	rozdílně
v	v	k7c6	v
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc1	údaj
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
hmotnostních	hmotnostní	k2eAgNnPc6d1	hmotnostní
procentech	procento	k1gNnPc6	procento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
patří	patřit	k5eAaImIp3nS	patřit
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
70	[number]	k4	70
až	až	k9	až
95	[number]	k4	95
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
oxidem	oxid	k1gInSc7	oxid
siřičitým	siřičitý	k2eAgInSc7d1	siřičitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
není	být	k5eNaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
metan	metan	k1gInSc1	metan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
taktéž	taktéž	k?	taktéž
produkován	produkován	k2eAgInSc1d1	produkován
a	a	k8xC	a
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c4	mezi
sopečné	sopečný	k2eAgInPc4d1	sopečný
plyny	plyn	k1gInPc4	plyn
také	také	k9	také
řazen	řazen	k2eAgMnSc1d1	řazen
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
fluorid	fluorid	k1gInSc1	fluorid
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
(	(	kIx(	(
<g/>
SiF	SiF	k1gFnSc1	SiF
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
a	a	k8xC	a
amoniak	amoniak	k1gInSc4	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Plyny	plyn	k1gInPc1	plyn
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
erupce	erupce	k1gFnSc2	erupce
žhavé	žhavý	k2eAgFnSc2d1	žhavá
či	či	k8xC	či
horké	horký	k2eAgFnSc2d1	horká
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
a	a	k8xC	a
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
musely	muset	k5eAaImAgInP	muset
urazit	urazit	k5eAaPmF	urazit
<g/>
.	.	kIx.	.
</s>
<s>
Výstup	výstup	k1gInSc1	výstup
magmatu	magma	k1gNnSc2	magma
přináší	přinášet	k5eAaImIp3nS	přinášet
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
taktéž	taktéž	k?	taktéž
radon	radon	k1gInSc1	radon
jako	jako	k8xS	jako
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
produkt	produkt	k1gInSc1	produkt
přírodních	přírodní	k2eAgInPc2d1	přírodní
rozpadů	rozpad	k1gInPc2	rozpad
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
přeměňují	přeměňovat	k5eAaImIp3nP	přeměňovat
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Množství	množství	k1gNnSc1	množství
==	==	k?	==
</s>
</p>
<p>
<s>
Množství	množství	k1gNnSc1	množství
vázaných	vázaný	k2eAgInPc2d1	vázaný
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
magmatu	magma	k1gNnSc6	magma
je	být	k5eAaImIp3nS	být
obrovské	obrovský	k2eAgNnSc1d1	obrovské
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1	[number]	k4	1
až	až	k9	až
5	[number]	k4	5
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
magma	magma	k1gNnSc1	magma
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
,	,	kIx,	,
litostatický	litostatický	k2eAgInSc1d1	litostatický
tlak	tlak	k1gInSc1	tlak
stlačuje	stlačovat	k5eAaImIp3nS	stlačovat
rozpuštěné	rozpuštěný	k2eAgInPc4d1	rozpuštěný
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
ale	ale	k8xC	ale
začne	začít	k5eAaPmIp3nS	začít
stoupat	stoupat	k5eAaImF	stoupat
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
plyny	plyn	k1gInPc1	plyn
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
začít	začít	k5eAaPmF	začít
roztahovat	roztahovat	k5eAaImF	roztahovat
a	a	k8xC	a
magma	magma	k1gNnSc4	magma
tak	tak	k6eAd1	tak
zvětšovat	zvětšovat	k5eAaImF	zvětšovat
svůj	svůj	k3xOyFgInSc4	svůj
objem	objem	k1gInSc4	objem
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
pro	pro	k7c4	pro
představu	představa	k1gFnSc4	představa
krychlový	krychlový	k2eAgInSc4d1	krychlový
metr	metr	k1gInSc4	metr
ryolitového	ryolitový	k2eAgNnSc2d1	ryolitový
magmatu	magma	k1gNnSc2	magma
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
až	až	k9	až
5	[number]	k4	5
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
váhy	váha	k1gFnSc2	váha
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
do	do	k7c2	do
podmínek	podmínka	k1gFnPc2	podmínka
normálního	normální	k2eAgInSc2d1	normální
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
magma	magma	k1gNnSc1	magma
s	s	k7c7	s
plyny	plyn	k1gInPc7	plyn
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c4	na
670	[number]	k4	670
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Krychle	krychle	k1gFnSc1	krychle
magmatu	magma	k1gNnSc2	magma
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zvětšila	zvětšit	k5eAaPmAgNnP	zvětšit
na	na	k7c4	na
krychli	krychle	k1gFnSc4	krychle
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
stran	strana	k1gFnPc2	strana
na	na	k7c4	na
8,75	[number]	k4	8,75
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Transport	transport	k1gInSc1	transport
==	==	k?	==
</s>
</p>
<p>
<s>
Uvolňované	uvolňovaný	k2eAgFnPc1d1	uvolňovaná
látky	látka	k1gFnPc1	látka
z	z	k7c2	z
taveniny	tavenina	k1gFnSc2	tavenina
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
erupce	erupce	k1gFnSc2	erupce
transportovány	transportovat	k5eAaBmNgInP	transportovat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
jako	jako	k9	jako
plyny	plyn	k1gInPc7	plyn
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
kapalinou	kapalina	k1gFnSc7	kapalina
na	na	k7c4	na
aerosol	aerosol	k1gInSc4	aerosol
a	a	k8xC	a
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
ukládány	ukládat	k5eAaImNgFnP	ukládat
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
geotermálním	geotermální	k2eAgInSc7d1	geotermální
tokem	tok	k1gInSc7	tok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k6eAd1	již
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
aktivní	aktivní	k2eAgInSc4d1	aktivní
povrchový	povrchový	k2eAgInSc4d1	povrchový
vulkanismus	vulkanismus	k1gInSc4	vulkanismus
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
sopečné	sopečný	k2eAgInPc4d1	sopečný
plyny	plyn	k1gInPc4	plyn
utíkat	utíkat	k5eAaImF	utíkat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
rozpuštěné	rozpuštěný	k2eAgMnPc4d1	rozpuštěný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikají	vznikat	k5eAaImIp3nP	vznikat
přírodní	přírodní	k2eAgFnSc2d1	přírodní
minerální	minerální	k2eAgFnSc2d1	minerální
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
těchto	tento	k3xDgInPc2	tento
způsobů	způsob	k1gInPc2	způsob
jsou	být	k5eAaImIp3nP	být
plyny	plyn	k1gInPc4	plyn
transportovány	transportován	k2eAgInPc4d1	transportován
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
pyroklastických	pyroklastický	k2eAgInPc6d1	pyroklastický
proudech	proud	k1gInPc6	proud
<g/>
,	,	kIx,	,
laharech	lahar	k1gInPc6	lahar
a	a	k8xC	a
lávových	lávový	k2eAgInPc6d1	lávový
proudech	proud	k1gInPc6	proud
<g/>
.	.	kIx.	.
<g/>
Sopečný	sopečný	k2eAgInSc1d1	sopečný
oblak	oblak	k1gInSc1	oblak
unikající	unikající	k2eAgInSc1d1	unikající
ze	z	k7c2	z
sopky	sopka	k1gFnSc2	sopka
tvořený	tvořený	k2eAgInSc1d1	tvořený
žhavým	žhavý	k2eAgInSc7d1	žhavý
popelem	popel	k1gInSc7	popel
<g/>
,	,	kIx,	,
kousky	kousek	k1gInPc4	kousek
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
sopečnými	sopečný	k2eAgInPc7d1	sopečný
plyny	plyn	k1gInPc7	plyn
může	moct	k5eAaImIp3nS	moct
vystoupat	vystoupat	k5eAaPmF	vystoupat
desítky	desítka	k1gFnPc4	desítka
kilometrů	kilometr	k1gInPc2	kilometr
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
větrem	vítr	k1gInSc7	vítr
rozšířit	rozšířit	k5eAaPmF	rozšířit
nad	nad	k7c4	nad
obrovské	obrovský	k2eAgNnSc4d1	obrovské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
erupci	erupce	k1gFnSc3	erupce
supervulkánu	supervulkán	k2eAgFnSc4d1	supervulkán
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
materiál	materiál	k1gInSc1	materiál
s	s	k7c7	s
plyny	plyn	k1gInPc7	plyn
rozšíří	rozšířit	k5eAaPmIp3nP	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
planetě	planeta	k1gFnSc6	planeta
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
výrazně	výrazně	k6eAd1	výrazně
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgNnSc7d1	velké
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
pro	pro	k7c4	pro
život	život	k1gInSc4	život
je	být	k5eAaImIp3nS	být
i	i	k9	i
interakce	interakce	k1gFnSc1	interakce
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
s	s	k7c7	s
dešťovými	dešťový	k2eAgFnPc7d1	dešťová
srážkami	srážka	k1gFnPc7	srážka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
kyselé	kyselý	k2eAgInPc4d1	kyselý
deště	dešť	k1gInPc4	dešť
nad	nad	k7c7	nad
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
oblasti	oblast	k1gFnPc4	oblast
až	až	k6eAd1	až
tisíce	tisíc	k4xCgInSc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
erupce	erupce	k1gFnSc2	erupce
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
např.	např.	kA	např.
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
během	během	k7c2	během
erupce	erupce	k1gFnSc2	erupce
sopky	sopka	k1gFnSc2	sopka
Katmai	Katma	k1gFnSc2	Katma
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
taktéž	taktéž	k?	taktéž
uvolnit	uvolnit	k5eAaPmF	uvolnit
fluorid	fluorid	k1gInSc4	fluorid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
schopen	schopen	k2eAgMnSc1d1	schopen
popálit	popálit	k5eAaPmF	popálit
vegetaci	vegetace	k1gFnSc4	vegetace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
<g/>
Sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nedostanou	dostat	k5eNaPmIp3nP	dostat
ve	v	k7c6	v
škodlivé	škodlivý	k2eAgFnSc6d1	škodlivá
míře	míra	k1gFnSc6	míra
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
erupce	erupce	k1gFnSc2	erupce
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
sledování	sledování	k1gNnSc4	sledování
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
potřeba	potřeba	k6eAd1	potřeba
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
častých	častý	k2eAgFnPc2d1	častá
erupcích	erupce	k1gFnPc6	erupce
instalovat	instalovat	k5eAaBmF	instalovat
senzory	senzor	k1gInPc1	senzor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
jejich	jejich	k3xOp3gInSc4	jejich
výskyt	výskyt	k1gInSc4	výskyt
zjistit	zjistit	k5eAaPmF	zjistit
a	a	k8xC	a
ohlásit	ohlásit	k5eAaPmF	ohlásit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
soli	sůl	k1gFnPc1	sůl
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
hned	hned	k6eAd1	hned
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
výstupu	výstup	k1gInSc2	výstup
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
sirnaté	sirnatý	k2eAgInPc4d1	sirnatý
útvary	útvar	k1gInPc4	útvar
jako	jako	k8xC	jako
např.	např.	kA	např.
sulfatery	sulfater	k1gMnPc7	sulfater
či	či	k8xC	či
natolik	natolik	k6eAd1	natolik
velká	velký	k2eAgNnPc1d1	velké
ložiska	ložisko	k1gNnPc1	ložisko
například	například	k6eAd1	například
síry	síra	k1gFnSc2	síra
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyplatí	vyplatit	k5eAaPmIp3nS	vyplatit
jejich	jejich	k3xOp3gFnSc1	jejich
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
těžba	těžba	k1gFnSc1	těžba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vyjma	vyjma	k7c2	vyjma
kyslíku	kyslík	k1gInSc2	kyslík
produkovaného	produkovaný	k2eAgInSc2d1	produkovaný
zelenými	zelený	k2eAgFnPc7d1	zelená
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
řasami	řasa	k1gFnPc7	řasa
a	a	k8xC	a
bakteriemi	bakterie	k1gFnPc7	bakterie
fotosyntézou	fotosyntéza	k1gFnSc7	fotosyntéza
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
většina	většina	k1gFnSc1	většina
atmosférických	atmosférický	k2eAgInPc2d1	atmosférický
plynů	plyn	k1gInPc2	plyn
tvořících	tvořící	k2eAgInPc2d1	tvořící
vzduch	vzduch	k1gInSc4	vzduch
ze	z	k7c2	z
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
sopečnými	sopečný	k2eAgInPc7d1	sopečný
plyny	plyn	k1gInPc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
produkované	produkovaný	k2eAgInPc1d1	produkovaný
plyny	plyn	k1gInPc1	plyn
mají	mít	k5eAaImIp3nP	mít
rozdílný	rozdílný	k2eAgInSc4d1	rozdílný
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
atmosféru	atmosféra	k1gFnSc4	atmosféra
respektive	respektive	k9	respektive
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
planetu	planeta	k1gFnSc4	planeta
a	a	k8xC	a
život	život	k1gInSc4	život
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
doklady	doklad	k1gInPc4	doklad
ukazující	ukazující	k2eAgFnSc4d1	ukazující
spojitost	spojitost	k1gFnSc4	spojitost
mezi	mezi	k7c7	mezi
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
erupcí	erupce	k1gFnSc7	erupce
a	a	k8xC	a
krátkodobou	krátkodobý	k2eAgFnSc7d1	krátkodobá
změnou	změna	k1gFnSc7	změna
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
s	s	k7c7	s
dlouhodobými	dlouhodobý	k2eAgFnPc7d1	dlouhodobá
změnami	změna	k1gFnPc7	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
planetách	planeta	k1gFnPc6	planeta
jako	jako	k8xS	jako
například	například	k6eAd1	například
Venuše	Venuše	k1gFnSc2	Venuše
či	či	k8xC	či
Marsu	Mars	k1gInSc2	Mars
se	se	k3xPyFc4	se
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
podílely	podílet	k5eAaImAgInP	podílet
taktéž	taktéž	k?	taktéž
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
atmosféry	atmosféra	k1gFnSc2	atmosféra
těchto	tento	k3xDgFnPc2	tento
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Venuše	Venuše	k1gFnSc2	Venuše
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
planeta	planeta	k1gFnSc1	planeta
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
skleníkovým	skleníkový	k2eAgInSc7d1	skleníkový
jevem	jev	k1gInSc7	jev
zahřívající	zahřívající	k2eAgFnSc4d1	zahřívající
celou	celý	k2eAgFnSc4d1	celá
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Marsu	Mars	k1gInSc2	Mars
atmosféra	atmosféra	k1gFnSc1	atmosféra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k9	jen
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc4d1	aktivní
vulkanismus	vulkanismus	k1gInSc4	vulkanismus
produkující	produkující	k2eAgInPc4d1	produkující
sopečné	sopečný	k2eAgInPc4d1	sopečný
plyny	plyn	k1gInPc4	plyn
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
na	na	k7c6	na
měsících	měsíc	k1gInPc6	měsíc
jako	jako	k8xS	jako
např.	např.	kA	např.
u	u	k7c2	u
měsíce	měsíc	k1gInSc2	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
Io	Io	k1gFnPc2	Io
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
značné	značný	k2eAgNnSc1d1	značné
úsilí	úsilí	k1gNnSc1	úsilí
na	na	k7c4	na
přesnější	přesný	k2eAgNnSc4d2	přesnější
zmapování	zmapování	k1gNnSc4	zmapování
množství	množství	k1gNnSc2	množství
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
produkováno	produkovat	k5eAaImNgNnS	produkovat
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
pochopení	pochopení	k1gNnSc4	pochopení
procesů	proces	k1gInPc2	proces
formující	formující	k2eAgFnSc4d1	formující
atmosféru	atmosféra	k1gFnSc4	atmosféra
těles	těleso	k1gNnPc2	těleso
s	s	k7c7	s
pevným	pevný	k2eAgInSc7d1	pevný
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
srovnání	srovnání	k1gNnSc4	srovnání
míry	míra	k1gFnSc2	míra
zavinění	zavinění	k1gNnSc2	zavinění
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
k	k	k7c3	k
oteplování	oteplování	k1gNnSc3	oteplování
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
<g/>
Sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
stály	stát	k5eAaImAgInP	stát
za	za	k7c7	za
vznikem	vznik	k1gInSc7	vznik
prvotní	prvotní	k2eAgFnSc2d1	prvotní
atmosféry	atmosféra	k1gFnSc2	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
její	její	k3xOp3gNnSc1	její
dnešní	dnešní	k2eAgNnSc1d1	dnešní
složení	složení	k1gNnSc1	složení
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgFnSc1d1	primární
atmosféra	atmosféra	k1gFnSc1	atmosféra
neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
výraznější	výrazný	k2eAgNnSc4d2	výraznější
zastoupení	zastoupení	k1gNnSc4	zastoupení
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k6eAd1	až
jako	jako	k8xS	jako
produkt	produkt	k1gInSc4	produkt
primitivních	primitivní	k2eAgInPc2d1	primitivní
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
napovídají	napovídat	k5eAaBmIp3nP	napovídat
geologické	geologický	k2eAgInPc4d1	geologický
nálezy	nález	k1gInPc4	nález
hornin	hornina	k1gFnPc2	hornina
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc1	kyslík
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
začal	začít	k5eAaPmAgInS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
až	až	k9	až
někdy	někdy	k6eAd1	někdy
před	před	k7c7	před
2,5	[number]	k4	2,5
až	až	k9	až
1,8	[number]	k4	1,8
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
některé	některý	k3yIgInPc4	některý
sopečné	sopečný	k2eAgInPc4d1	sopečný
plyny	plyn	k1gInPc4	plyn
jako	jako	k8xC	jako
např.	např.	kA	např.
karbonylsuflid	karbonylsuflid	k1gInSc1	karbonylsuflid
se	se	k3xPyFc4	se
vznikem	vznik	k1gInSc7	vznik
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plyn	plyn	k1gInSc1	plyn
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
slučování	slučování	k1gNnSc4	slučování
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
složitějších	složitý	k2eAgFnPc2d2	složitější
organických	organický	k2eAgFnPc2d1	organická
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgMnPc2	který
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
===	===	k?	===
</s>
</p>
<p>
<s>
Veliký	veliký	k2eAgInSc1d1	veliký
obsah	obsah	k1gInSc1	obsah
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
v	v	k7c6	v
roztaveném	roztavený	k2eAgNnSc6d1	roztavené
magmatu	magma	k1gNnSc6	magma
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
nachází	nacházet	k5eAaImIp3nS	nacházet
kapalná	kapalný	k2eAgFnSc1d1	kapalná
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc4d1	hlavní
skleníkový	skleníkový	k2eAgInSc4d1	skleníkový
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
erupcí	erupce	k1gFnPc2	erupce
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
již	již	k6eAd1	již
sloučená	sloučený	k2eAgFnSc1d1	sloučená
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
agresivní	agresivní	k2eAgFnSc1d1	agresivní
chemická	chemický	k2eAgFnSc1d1	chemická
směs	směs	k1gFnSc1	směs
urychlující	urychlující	k2eAgFnSc4d1	urychlující
korozi	koroze	k1gFnSc4	koroze
kovových	kovový	k2eAgInPc2d1	kovový
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
kondenzuje	kondenzovat	k5eAaImIp3nS	kondenzovat
a	a	k8xC	a
padá	padat	k5eAaImIp3nS	padat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
jako	jako	k8xC	jako
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
opět	opět	k6eAd1	opět
vypařuje	vypařovat	k5eAaImIp3nS	vypařovat
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
částí	část	k1gFnSc7	část
vodního	vodní	k2eAgInSc2d1	vodní
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
kondenzací	kondenzace	k1gFnSc7	kondenzace
z	z	k7c2	z
vulkanických	vulkanický	k2eAgInPc2d1	vulkanický
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
juvenilní	juvenilní	k2eAgFnSc1d1	juvenilní
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
tvůrcem	tvůrce	k1gMnSc7	tvůrce
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vodní	vodní	k2eAgFnSc6d1	vodní
páře	pára	k1gFnSc6	pára
zabírá	zabírat	k5eAaImIp3nS	zabírat
přední	přední	k2eAgFnSc4d1	přední
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
udržení	udržení	k1gNnSc6	udržení
teploty	teplota	k1gFnSc2	teplota
Země	zem	k1gFnSc2	zem
umožňující	umožňující	k2eAgInSc4d1	umožňující
výskyt	výskyt	k1gInSc4	výskyt
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c4	vyjma
množství	množství	k1gNnSc4	množství
antropogenních	antropogenní	k2eAgInPc2d1	antropogenní
zdrojů	zdroj	k1gInPc2	zdroj
vypouštějících	vypouštějící	k2eAgInPc2d1	vypouštějící
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
během	během	k7c2	během
spalování	spalování	k1gNnSc2	spalování
a	a	k8xC	a
současná	současný	k2eAgFnSc1d1	současná
debata	debata	k1gFnSc1	debata
o	o	k7c6	o
podílu	podíl	k1gInSc6	podíl
člověka	člověk	k1gMnSc2	člověk
na	na	k7c6	na
globálním	globální	k2eAgNnSc6d1	globální
oteplování	oteplování	k1gNnSc6	oteplování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
produkován	produkován	k2eAgInSc4d1	produkován
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
110	[number]	k4	110
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
lidským	lidský	k2eAgInSc7d1	lidský
příspěvkem	příspěvek	k1gInSc7	příspěvek
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
tun	tuna	k1gFnPc2	tuna
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
pak	pak	k6eAd1	pak
propouští	propouštět	k5eAaImIp3nP	propouštět
sluneční	sluneční	k2eAgInPc1d1	sluneční
paprsky	paprsek	k1gInPc1	paprsek
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
jimi	on	k3xPp3gInPc7	on
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
teplu	teplo	k1gNnSc3	teplo
sálat	sálat	k5eAaImF	sálat
radiací	radiace	k1gFnSc7	radiace
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
erupci	erupce	k1gFnSc6	erupce
se	se	k3xPyFc4	se
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
často	často	k6eAd1	často
stává	stávat	k5eAaImIp3nS	stávat
aerosolem	aerosol	k1gInSc7	aerosol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dobrou	dobrý	k2eAgFnSc4d1	dobrá
schopnost	schopnost	k1gFnSc4	schopnost
odrážet	odrážet	k5eAaImF	odrážet
sluneční	sluneční	k2eAgNnSc4d1	sluneční
záření	záření	k1gNnSc4	záření
dopadající	dopadající	k2eAgNnSc4d1	dopadající
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
pronikání	pronikání	k1gNnSc4	pronikání
paprsků	paprsek	k1gInPc2	paprsek
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Země	zem	k1gFnSc2	zem
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
ochlazování	ochlazování	k1gNnSc3	ochlazování
jejího	její	k3xOp3gInSc2	její
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ale	ale	k9	ale
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozpouštění	rozpouštění	k1gNnSc3	rozpouštění
krystalků	krystalek	k1gInPc2	krystalek
a	a	k8xC	a
vzniku	vznik	k1gInSc6	vznik
slabě	slabě	k6eAd1	slabě
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
kyselé	kyselý	k2eAgInPc4d1	kyselý
deště	dešť	k1gInPc4	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
erupcí	erupce	k1gFnSc7	erupce
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
až	až	k9	až
20	[number]	k4	20
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
tohoto	tento	k3xDgInSc2	tento
oxidu	oxid	k1gInSc2	oxid
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
během	během	k7c2	během
erupce	erupce	k1gFnSc2	erupce
sopky	sopka	k1gFnSc2	sopka
Pinatubo	Pinatuba	k1gMnSc5	Pinatuba
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
následné	následný	k2eAgInPc4d1	následný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
ochladilo	ochladit	k5eAaPmAgNnS	ochladit
celou	celý	k2eAgFnSc4d1	celá
planetu	planeta	k1gFnSc4	planeta
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
0,72	[number]	k4	0,72
°	°	k?	°
<g/>
C.	C.	kA	C.
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
úniky	únik	k1gInPc4	únik
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
v	v	k7c6	v
novodobé	novodobý	k2eAgFnSc6d1	novodobá
historii	historie	k1gFnSc6	historie
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
erupce	erupce	k1gFnPc1	erupce
sopky	sopka	k1gFnSc2	sopka
Laki	Lak	k1gFnSc2	Lak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
schopnost	schopnost	k1gFnSc1	schopnost
odrážet	odrážet	k5eAaImF	odrážet
sluneční	sluneční	k2eAgInPc4d1	sluneční
paprsky	paprsek	k1gInPc4	paprsek
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
činitelem	činitel	k1gMnSc7	činitel
v	v	k7c6	v
dlouhodobých	dlouhodobý	k2eAgFnPc6d1	dlouhodobá
změnách	změna	k1gFnPc6	změna
klimatu	klima	k1gNnSc2	klima
vlivem	vlivem	k7c2	vlivem
sopečné	sopečný	k2eAgFnSc2d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
oblastech	oblast	k1gFnPc6	oblast
chladnější	chladný	k2eAgFnSc2d2	chladnější
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
sírovou	sírový	k2eAgFnSc4d1	sírová
za	za	k7c4	za
působení	působení	k1gNnSc4	působení
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
kyselina	kyselina	k1gFnSc1	kyselina
reaguje	reagovat	k5eAaBmIp3nS	reagovat
se	s	k7c7	s
stratosférickou	stratosférický	k2eAgFnSc7d1	stratosférická
vodou	voda	k1gFnSc7	voda
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
aerosolové	aerosolový	k2eAgFnPc1d1	aerosolová
vrstvy	vrstva	k1gFnPc1	vrstva
tvořené	tvořený	k2eAgFnPc1d1	tvořená
kapičkami	kapička	k1gFnPc7	kapička
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnPc4d1	sírová
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
dlouho	dlouho	k6eAd1	dlouho
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
co	co	k9	co
malé	malý	k2eAgFnPc4d1	malá
prachové	prachový	k2eAgFnPc4d1	prachová
částečky	částečka	k1gFnPc4	částečka
z	z	k7c2	z
erupce	erupce	k1gFnSc2	erupce
se	se	k3xPyFc4	se
již	již	k6eAd1	již
usadily	usadit	k5eAaPmAgFnP	usadit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
tělesa	těleso	k1gNnSc2	těleso
většinou	většinou	k6eAd1	většinou
15	[number]	k4	15
až	až	k9	až
25	[number]	k4	25
km	km	kA	km
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Prachové	prachový	k2eAgFnPc1d1	prachová
částečky	částečka	k1gFnPc1	částečka
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
těžké	těžký	k2eAgInPc1d1	těžký
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
setrvaly	setrvat	k5eAaPmAgFnP	setrvat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
skutečným	skutečný	k2eAgInSc7d1	skutečný
důvodem	důvod	k1gInSc7	důvod
globálních	globální	k2eAgFnPc2d1	globální
změn	změna	k1gFnPc2	změna
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
klimatu	klima	k1gNnSc2	klima
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
právě	právě	k9	právě
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
částice	částice	k1gFnSc2	částice
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
aerosolu	aerosol	k1gInSc2	aerosol
zadržují	zadržovat	k5eAaImIp3nP	zadržovat
teplo	teplo	k1gNnSc4	teplo
unikající	unikající	k2eAgNnSc4d1	unikající
radiací	radiace	k1gFnSc7	radiace
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
částice	částice	k1gFnPc1	částice
se	se	k3xPyFc4	se
zahřívají	zahřívat	k5eAaImIp3nP	zahřívat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
teploty	teplota	k1gFnSc2	teplota
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
stratosféry	stratosféra	k1gFnSc2	stratosféra
pak	pak	k6eAd1	pak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snazší	snadný	k2eAgInSc4d2	snadnější
vznik	vznik	k1gInSc4	vznik
oxidu	oxid	k1gInSc2	oxid
chlornatého	chlornatý	k2eAgInSc2d1	chlornatý
ničícího	ničící	k2eAgInSc2d1	ničící
ozón	ozón	k1gInSc1	ozón
<g/>
.	.	kIx.	.
<g/>
Částečky	částečka	k1gFnSc2	částečka
aerosolu	aerosol	k1gInSc2	aerosol
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
sráží	srážet	k5eAaImIp3nP	srážet
a	a	k8xC	a
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
až	až	k8xS	až
přesáhnou	přesáhnout	k5eAaPmIp3nP	přesáhnout
kritickou	kritický	k2eAgFnSc4d1	kritická
mez	mez	k1gFnSc4	mez
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
klesat	klesat	k5eAaImF	klesat
do	do	k7c2	do
troposféry	troposféra	k1gFnSc2	troposféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
kondenzačními	kondenzační	k2eAgNnPc7d1	kondenzační
jádry	jádro	k1gNnPc7	jádro
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
v	v	k7c6	v
cirrusech	cirrus	k1gInPc6	cirrus
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgFnPc7	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
působením	působení	k1gNnSc7	působení
gravitace	gravitace	k1gFnSc2	gravitace
dostanou	dostat	k5eAaPmIp3nP	dostat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
současně	současně	k6eAd1	současně
předají	předat	k5eAaPmIp3nP	předat
získané	získaný	k2eAgNnSc4d1	získané
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
nová	nový	k2eAgFnSc1d1	nová
sopečná	sopečný	k2eAgFnSc1d1	sopečná
erupce	erupce	k1gFnSc1	erupce
tak	tak	k6eAd1	tak
znovu	znovu	k6eAd1	znovu
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
vyplavuje	vyplavovat	k5eAaImIp3nS	vyplavovat
deštěm	dešť	k1gInSc7	dešť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chlor	chlor	k1gInSc4	chlor
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
sopečné	sopečný	k2eAgFnSc2d1	sopečná
erupce	erupce	k1gFnSc2	erupce
je	být	k5eAaImIp3nS	být
produkován	produkovat	k5eAaImNgInS	produkovat
i	i	k9	i
chlor	chlor	k1gInSc1	chlor
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
negativně	negativně	k6eAd1	negativně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
vést	vést	k5eAaImF	vést
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
až	až	k9	až
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
biotopu	biotop	k1gInSc2	biotop
celé	celý	k2eAgFnSc2d1	celá
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
na	na	k7c4	na
chlor	chlor	k1gInSc4	chlor
a	a	k8xC	a
oxid	oxid	k1gInSc4	oxid
chlornatý	chlornatý	k2eAgInSc4d1	chlornatý
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
síranové	síranový	k2eAgInPc1d1	síranový
aerosoly	aerosol	k1gInPc1	aerosol
<g/>
.	.	kIx.	.
</s>
<s>
Chlor	chlor	k1gInSc1	chlor
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
se	se	k3xPyFc4	se
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
přidává	přidávat	k5eAaImIp3nS	přidávat
k	k	k7c3	k
chloru	chlor	k1gInSc3	chlor
vypouštěnému	vypouštěný	k2eAgInSc3d1	vypouštěný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
posiluje	posilovat	k5eAaImIp3nS	posilovat
reaktivní	reaktivní	k2eAgInPc4d1	reaktivní
chlorové	chlorový	k2eAgInPc4d1	chlorový
atomy	atom	k1gInPc4	atom
ničící	ničící	k2eAgInPc4d1	ničící
ozon	ozon	k1gInSc4	ozon
respektive	respektive	k9	respektive
ozonovou	ozonový	k2eAgFnSc4d1	ozonová
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
odhadnutelné	odhadnutelný	k2eAgNnSc1d1	odhadnutelné
<g/>
,	,	kIx,	,
kolik	kolik	k4yRc4	kolik
chloru	chlor	k1gInSc2	chlor
je	být	k5eAaImIp3nS	být
produkováno	produkovat	k5eAaImNgNnS	produkovat
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
lidským	lidský	k2eAgInSc7d1	lidský
příspěvkem	příspěvek	k1gInSc7	příspěvek
je	být	k5eAaImIp3nS	být
chloru	chlor	k1gInSc2	chlor
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
produkce	produkce	k1gFnSc1	produkce
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
regenerační	regenerační	k2eAgFnSc4d1	regenerační
schopnost	schopnost	k1gFnSc4	schopnost
ozonosféry	ozonosféra	k1gFnSc2	ozonosféra
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
nebezpečných	bezpečný	k2eNgFnPc2d1	nebezpečná
sloučenin	sloučenina	k1gFnPc2	sloučenina
chloru	chlor	k1gInSc2	chlor
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
přeměněným	přeměněný	k2eAgInSc7d1	přeměněný
oxidem	oxid	k1gInSc7	oxid
siřičitým	siřičitý	k2eAgInSc7d1	siřičitý
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fluor	fluor	k1gInSc4	fluor
===	===	k?	===
</s>
</p>
<p>
<s>
Uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
fluor	fluor	k1gInSc1	fluor
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
fluorovodíku	fluorovodík	k1gInSc2	fluorovodík
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kyselým	kyselý	k2eAgInSc7d1	kyselý
deštěm	dešť	k1gInSc7	dešť
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kontaminuje	kontaminovat	k5eAaBmIp3nS	kontaminovat
oblast	oblast	k1gFnSc4	oblast
spadu	spad	k1gInSc2	spad
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
transportován	transportovat	k5eAaBmNgMnS	transportovat
říční	říční	k2eAgFnSc7d1	říční
sítí	síť	k1gFnSc7	síť
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
ukládání	ukládání	k1gNnSc3	ukládání
v	v	k7c6	v
jezerech	jezero	k1gNnPc6	jezero
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
kontaminuje	kontaminovat	k5eAaBmIp3nS	kontaminovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
pro	pro	k7c4	pro
zvířata	zvíře	k1gNnPc4	zvíře
spásající	spásající	k2eAgFnSc1d1	spásající
trávu	tráva	k1gFnSc4	tráva
či	či	k8xC	či
pijící	pijící	k2eAgInPc4d1	pijící
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
otrava	otrava	k1gFnSc1	otrava
fluorem	fluor	k1gInSc7	fluor
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
kostní	kostní	k2eAgFnSc2d1	kostní
fluorózy	fluoróza	k1gFnSc2	fluoróza
ničící	ničící	k2eAgFnSc2d1	ničící
kosti	kost	k1gFnSc2	kost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výzkum	výzkum	k1gInSc1	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
pochopení	pochopení	k1gNnSc4	pochopení
změn	změna	k1gFnPc2	změna
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
člověkem	člověk	k1gMnSc7	člověk
se	se	k3xPyFc4	se
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
podrobnější	podrobný	k2eAgInSc1d2	podrobnější
sběr	sběr	k1gInSc1	sběr
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
pozdější	pozdní	k2eAgFnSc4d2	pozdější
analýzu	analýza	k1gFnSc4	analýza
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
Scipione	Scipion	k1gInSc5	Scipion
Breislak	Breislak	k1gInSc4	Breislak
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
výzkum	výzkum	k1gInSc1	výzkum
soustředil	soustředit	k5eAaPmAgInS	soustředit
na	na	k7c4	na
odběr	odběr	k1gInSc4	odběr
vzorků	vzorek	k1gInPc2	vzorek
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
následný	následný	k2eAgInSc4d1	následný
transport	transport	k1gInSc4	transport
do	do	k7c2	do
laboratoří	laboratoř	k1gFnPc2	laboratoř
na	na	k7c4	na
analýzu	analýza	k1gFnSc4	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Nověji	nově	k6eAd2	nově
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
výstupu	výstup	k1gInSc2	výstup
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
instalují	instalovat	k5eAaBmIp3nP	instalovat
automatická	automatický	k2eAgNnPc1d1	automatické
měřící	měřící	k2eAgNnPc1d1	měřící
čidla	čidlo	k1gNnPc1	čidlo
zaměřující	zaměřující	k2eAgNnPc1d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
chemický	chemický	k2eAgInSc4d1	chemický
rozbor	rozbor	k1gInSc4	rozbor
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
<g/>
Sledování	sledování	k1gNnSc1	sledování
výronů	výron	k1gInPc2	výron
plynů	plyn	k1gInPc2	plyn
může	moct	k5eAaImIp3nS	moct
napomoci	napomoct	k5eAaPmF	napomoct
předpovědi	předpověď	k1gFnPc4	předpověď
sopečné	sopečný	k2eAgFnSc2d1	sopečná
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
koncentrace	koncentrace	k1gFnSc1	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
ve	v	k7c6	v
fumarolách	fumarola	k1gFnPc6	fumarola
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
naznačovat	naznačovat	k5eAaImF	naznačovat
opětovné	opětovný	k2eAgNnSc4d1	opětovné
stoupání	stoupání	k1gNnSc4	stoupání
magmatu	magma	k1gNnSc2	magma
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
další	další	k2eAgFnSc2d1	další
sopečné	sopečný	k2eAgFnSc2d1	sopečná
erupce	erupce	k1gFnSc2	erupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdravotní	zdravotní	k2eAgNnPc4d1	zdravotní
rizika	riziko	k1gNnPc4	riziko
==	==	k?	==
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
erupcí	erupce	k1gFnPc2	erupce
můžou	můžou	k?	můžou
způsobit	způsobit	k5eAaPmF	způsobit
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Kyselé	kyselý	k2eAgInPc1d1	kyselý
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
<g/>
,	,	kIx,	,
sulfan	sulfan	k1gInSc1	sulfan
a	a	k8xC	a
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
mohou	moct	k5eAaImIp3nP	moct
vážně	vážně	k6eAd1	vážně
poškodit	poškodit	k5eAaPmF	poškodit
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
sliznice	sliznice	k1gFnPc4	sliznice
dýchacího	dýchací	k2eAgInSc2d1	dýchací
traktu	trakt	k1gInSc2	trakt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případu	případ	k1gInSc6	případ
vést	vést	k5eAaImF	vést
i	i	k9	i
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Interakce	interakce	k1gFnSc1	interakce
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
s	s	k7c7	s
živým	živý	k2eAgInSc7d1	živý
organismem	organismus	k1gInSc7	organismus
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
příliš	příliš	k6eAd1	příliš
dobře	dobře	k6eAd1	dobře
prozkoumána	prozkoumat	k5eAaPmNgNnP	prozkoumat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
slabosti	slabost	k1gFnSc2	slabost
<g/>
,	,	kIx,	,
dýchací	dýchací	k2eAgFnPc4d1	dýchací
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
alergické	alergický	k2eAgFnPc4d1	alergická
reakce	reakce	k1gFnPc4	reakce
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
představuje	představovat	k5eAaImIp3nS	představovat
taktéž	taktéž	k?	taktéž
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
shromažďuje	shromažďovat	k5eAaImIp3nS	shromažďovat
v	v	k7c6	v
depresích	deprese	k1gFnPc6	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
tento	tento	k3xDgInSc1	tento
plyn	plyn	k1gInSc1	plyn
nemá	mít	k5eNaImIp3nS	mít
zápach	zápach	k1gInSc4	zápach
a	a	k8xC	a
ani	ani	k8xC	ani
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
těžké	těžký	k2eAgNnSc1d1	těžké
ho	on	k3xPp3gInSc4	on
detekovat	detekovat	k5eAaImF	detekovat
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
20	[number]	k4	20
let	léto	k1gNnPc2	léto
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
otravu	otrava	k1gFnSc4	otrava
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
okolo	okolo	k7c2	okolo
dvou	dva	k4xCgInPc2	dva
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
úmrtí	úmrť	k1gFnPc2	úmrť
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1900	[number]	k4	1900
až	až	k8xS	až
1986	[number]	k4	1986
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
vulkanismem	vulkanismus	k1gInSc7	vulkanismus
připadá	připadat	k5eAaPmIp3nS	připadat
právě	právě	k9	právě
na	na	k7c4	na
toxické	toxický	k2eAgInPc4d1	toxický
a	a	k8xC	a
jedovaté	jedovatý	k2eAgInPc4d1	jedovatý
sopečné	sopečný	k2eAgInPc4d1	sopečný
plyny	plyn	k1gInPc4	plyn
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k6eAd1	mimo
živočichů	živočich	k1gMnPc2	živočich
mají	mít	k5eAaImIp3nP	mít
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
také	také	k9	také
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšené	zvýšený	k2eAgFnPc4d1	zvýšená
koncentrace	koncentrace	k1gFnPc4	koncentrace
převážně	převážně	k6eAd1	převážně
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
působí	působit	k5eAaImIp3nS	působit
pozitivně	pozitivně	k6eAd1	pozitivně
na	na	k7c4	na
život	život	k1gInSc4	život
zelených	zelený	k2eAgFnPc2d1	zelená
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
rostou	růst	k5eAaImIp3nP	růst
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
plyny	plyn	k1gInPc1	plyn
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obsahující	obsahující	k2eAgFnSc4d1	obsahující
síru	síra	k1gFnSc4	síra
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
toxické	toxický	k2eAgNnSc1d1	toxické
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
také	také	k9	také
dostává	dostávat	k5eAaImIp3nS	dostávat
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
dokonce	dokonce	k9	dokonce
spojují	spojovat	k5eAaImIp3nP	spojovat
masové	masový	k2eAgNnSc1d1	masové
vymírání	vymírání	k1gNnSc4	vymírání
druhů	druh	k1gInPc2	druh
s	s	k7c7	s
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
produkcí	produkce	k1gFnSc7	produkce
jedovatých	jedovatý	k2eAgInPc2d1	jedovatý
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
následně	následně	k6eAd1	následně
vyhubily	vyhubit	k5eAaPmAgFnP	vyhubit
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	k9	by
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
masivní	masivní	k2eAgFnSc3d1	masivní
sopečné	sopečný	k2eAgFnSc3d1	sopečná
činnosti	činnost	k1gFnSc3	činnost
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
výlevného	výlevný	k2eAgInSc2d1	výlevný
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
zničit	zničit	k5eAaPmF	zničit
ozón	ozón	k1gInSc4	ozón
<g/>
,	,	kIx,	,
okyselit	okyselit	k5eAaPmF	okyselit
oceány	oceán	k1gInPc4	oceán
a	a	k8xC	a
půdu	půda	k1gFnSc4	půda
či	či	k8xC	či
změnit	změnit	k5eAaPmF	změnit
globální	globální	k2eAgNnSc4d1	globální
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
<g/>
Sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
při	při	k7c6	při
prudké	prudký	k2eAgFnSc6d1	prudká
sopečné	sopečný	k2eAgFnSc6d1	sopečná
erupci	erupce	k1gFnSc6	erupce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
unášejí	unášet	k5eAaImIp3nP	unášet
pevné	pevný	k2eAgFnPc1d1	pevná
i	i	k8xC	i
tuhnoucí	tuhnoucí	k2eAgFnPc1d1	tuhnoucí
částice	částice	k1gFnPc1	částice
různé	různý	k2eAgFnPc1d1	různá
velikosti	velikost	k1gFnSc3	velikost
(	(	kIx(	(
<g/>
tufový	tufový	k2eAgInSc1d1	tufový
popel	popel	k1gInSc1	popel
<g/>
,	,	kIx,	,
kamínky	kamínek	k1gInPc1	kamínek
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
tuhnoucí	tuhnoucí	k2eAgFnSc4d1	tuhnoucí
lávu	láva	k1gFnSc4	láva
<g/>
,	,	kIx,	,
částice	částice	k1gFnPc1	částice
kouře	kouř	k1gInSc2	kouř
z	z	k7c2	z
požáru	požár	k1gInSc2	požár
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
úniku	únik	k1gInSc6	únik
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
plyny	plyn	k1gInPc7	plyn
atmosférickými	atmosférický	k2eAgInPc7d1	atmosférický
<g/>
;	;	kIx,	;
šíří	šíř	k1gFnSc7	šíř
se	s	k7c7	s
vlastním	vlastní	k2eAgInSc7d1	vlastní
tlakem	tlak	k1gInSc7	tlak
značnou	značný	k2eAgFnSc7d1	značná
rychlostí	rychlost	k1gFnSc7	rychlost
–	–	k?	–
až	až	k9	až
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
160	[number]	k4	160
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
–	–	k?	–
a	a	k8xC	a
spalují	spalovat	k5eAaImIp3nP	spalovat
<g/>
,	,	kIx,	,
dusí	dusit	k5eAaImIp3nP	dusit
<g/>
,	,	kIx,	,
otravují	otravovat	k5eAaImIp3nP	otravovat
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
zabíjejí	zabíjet	k5eAaImIp3nP	zabíjet
vše	všechen	k3xTgNnSc4	všechen
živé	živé	k1gNnSc4	živé
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Únik	únik	k1gInSc1	únik
a	a	k8xC	a
prudké	prudký	k2eAgNnSc1d1	prudké
šíření	šíření	k1gNnSc1	šíření
takového	takový	k3xDgInSc2	takový
žhavého	žhavý	k2eAgInSc2d1	žhavý
<g/>
,	,	kIx,	,
stlačeného	stlačený	k2eAgInSc2d1	stlačený
sopečného	sopečný	k2eAgInSc2d1	sopečný
smogu	smog	k1gInSc2	smog
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
pyroklastická	pyroklastický	k2eAgFnSc1d1	pyroklastická
smršť	smršť	k1gFnSc1	smršť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žhavé	žhavý	k2eAgInPc1d1	žhavý
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
pronikají	pronikat	k5eAaImIp3nP	pronikat
i	i	k9	i
do	do	k7c2	do
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tyto	tento	k3xDgInPc1	tento
nejsou	být	k5eNaImIp3nP	být
uzavřeny	uzavřít	k5eAaPmNgInP	uzavřít
hermeticky	hermeticky	k6eAd1	hermeticky
nebo	nebo	k8xC	nebo
chráněny	chránit	k5eAaImNgFnP	chránit
přetlakem	přetlak	k1gInSc7	přetlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SIGURDSSON	SIGURDSSON	kA	SIGURDSSON
<g/>
,	,	kIx,	,
Haraldur	Haraldur	k1gMnSc1	Haraldur
<g/>
;	;	kIx,	;
DELMELLE	DELMELLE	kA	DELMELLE
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gMnSc5	Pierr
<g/>
;	;	kIx,	;
STIX	STIX	kA	STIX
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Volcanoes	Volcanoes	k1gMnSc1	Volcanoes
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Academic	Academic	k1gMnSc1	Academic
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
643140	[number]	k4	643140
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Volcanic	Volcanice	k1gFnPc2	Volcanice
Gases	Gases	k1gMnSc1	Gases
<g/>
,	,	kIx,	,
s.	s.	k?	s.
803	[number]	k4	803
<g/>
-	-	kIx~	-
<g/>
816	[number]	k4	816
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
USGS	USGS	kA	USGS
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Krátké	Krátké	k2eAgNnSc1d1	Krátké
video	video	k1gNnSc1	video
o	o	k7c6	o
sopečných	sopečný	k2eAgInPc6d1	sopečný
plynech	plyn	k1gInPc6	plyn
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
