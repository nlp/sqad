<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Malý	Malý	k1gMnSc1	Malý
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1951	[number]	k4	1951
Jičín	Jičín	k1gInSc1	Jičín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
advokát	advokát	k1gMnSc1	advokát
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zastupitel	zastupitel	k1gMnSc1	zastupitel
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
právo	právo	k1gNnSc1	právo
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
JUDr.	JUDr.	kA	JUDr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
profesní	profesní	k2eAgInSc4d1	profesní
život	život	k1gInSc4	život
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
advokát	advokát	k1gMnSc1	advokát
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
advokátní	advokátní	k2eAgFnSc3d1	advokátní
poradně	poradna	k1gFnSc3	poradna
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
advokátní	advokátní	k2eAgFnSc4d1	advokátní
kancelář	kancelář	k1gFnSc4	kancelář
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
představenstva	představenstvo	k1gNnSc2	představenstvo
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
Správa	správa	k1gFnSc1	správa
nemovitostí	nemovitost	k1gFnSc7	nemovitost
města	město	k1gNnSc2	město
Jičína	Jičín	k1gInSc2	Jičín
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
představenstva	představenstvo	k1gNnSc2	představenstvo
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
Vodohospodářská	vodohospodářský	k2eAgFnSc1d1	vodohospodářská
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
<g/>
Jan	Jan	k1gMnSc1	Jan
Malý	Malý	k1gMnSc1	Malý
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Jičín	Jičín	k1gInSc1	Jičín
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
části	část	k1gFnSc6	část
Valdické	valdický	k2eAgNnSc4d1	Valdické
Předměstí	předměstí	k1gNnSc4	předměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c2	za
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
zvolen	zvolit	k5eAaPmNgMnS	zvolit
zastupitelem	zastupitel	k1gMnSc7	zastupitel
města	město	k1gNnSc2	město
Jičín	Jičín	k1gInSc1	Jičín
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
hnutí	hnutí	k1gNnSc2	hnutí
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
stal	stát	k5eAaPmAgMnS	stát
starostou	starosta	k1gMnSc7	starosta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c2	za
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c2	za
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
37	[number]	k4	37
-	-	kIx~	-
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
25,11	[number]	k4	25,11
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
postoupil	postoupit	k5eAaPmAgMnS	postoupit
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
místa	místo	k1gNnSc2	místo
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
prohrál	prohrát	k5eAaPmAgMnS	prohrát
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
40,86	[number]	k4	40,86
%	%	kIx~	%
:	:	kIx,	:
59,13	[number]	k4	59,13
%	%	kIx~	%
s	s	k7c7	s
kandidátem	kandidát	k1gMnSc7	kandidát
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
hnutí	hnutí	k1gNnSc4	hnutí
STAN	stan	k1gInSc1	stan
Tomášem	Tomáš	k1gMnSc7	Tomáš
Czerninem	Czernin	k1gInSc7	Czernin
<g/>
.	.	kIx.	.
</s>
<s>
Senátorem	senátor	k1gMnSc7	senátor
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestal	stát	k5eNaPmAgInS	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
