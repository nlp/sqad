<p>
<s>
Kolaborativní	Kolaborativní	k2eAgNnSc1d1	Kolaborativní
učení	učení	k1gNnSc1	učení
je	být	k5eAaImIp3nS	být
učení	učení	k1gNnSc2	učení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
kolaborativní	kolaborativní	k2eAgInPc1d1	kolaborativní
učení	učení	k1gNnSc4	učení
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
běžně	běžně	k6eAd1	běžně
zaměňujeme	zaměňovat	k5eAaImIp1nP	zaměňovat
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
kooperativní	kooperativní	k2eAgNnSc4d1	kooperativní
učení	učení	k1gNnSc4	učení
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
collaborative	collaborativ	k1gInSc5	collaborativ
váže	vázat	k5eAaImIp3nS	vázat
spíše	spíše	k9	spíše
k	k	k7c3	k
učení	učení	k1gNnSc3	učení
-	-	kIx~	-
learning	learning	k1gInSc4	learning
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
cooperative	cooperativ	k1gInSc5	cooperativ
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
častěji	často	k6eAd2	často
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
praktickými	praktický	k2eAgFnPc7d1	praktická
činnostmi	činnost	k1gFnPc7	činnost
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
na	na	k7c4	na
dosažení	dosažení	k1gNnSc4	dosažení
určitého	určitý	k2eAgInSc2d1	určitý
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
splnění	splnění	k1gNnSc6	splnění
složitější	složitý	k2eAgFnSc2d2	složitější
úlohy	úloha	k1gFnSc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
splnění	splnění	k1gNnSc4	splnění
jsou	být	k5eAaImIp3nP	být
zodpovědni	zodpověden	k2eAgMnPc1d1	zodpověden
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
hrají	hrát	k5eAaImIp3nP	hrát
sociální	sociální	k2eAgInPc1d1	sociální
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
rozvoj	rozvoj	k1gInSc1	rozvoj
a	a	k8xC	a
udržování	udržování	k1gNnSc1	udržování
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
prostředkem	prostředek	k1gInSc7	prostředek
ke	k	k7c3	k
splnění	splnění	k1gNnSc3	splnění
úlohy	úloha	k1gFnSc2	úloha
a	a	k8xC	a
současně	současně	k6eAd1	současně
cílem	cíl	k1gInSc7	cíl
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
otevřené	otevřený	k2eAgFnSc2d1	otevřená
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
rovnocennosti	rovnocennost	k1gFnSc2	rovnocennost
<g/>
,	,	kIx,	,
důvěry	důvěra	k1gFnSc2	důvěra
<g/>
,	,	kIx,	,
sdílení	sdílení	k1gNnSc2	sdílení
a	a	k8xC	a
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
práce	práce	k1gFnSc2	práce
skupiny	skupina	k1gFnSc2	skupina
mají	mít	k5eAaImIp3nP	mít
prospěch	prospěch	k1gInSc4	prospěch
všichni	všechen	k3xTgMnPc1	všechen
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
zastávají	zastávat	k5eAaImIp3nP	zastávat
různé	různý	k2eAgFnPc1d1	různá
role	role	k1gFnPc1	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
práce	práce	k1gFnSc1	práce
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
má	mít	k5eAaImIp3nS	mít
individuální	individuální	k2eAgFnSc4d1	individuální
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
spolupráce	spolupráce	k1gFnSc2	spolupráce
naučil	naučit	k5eAaPmAgMnS	naučit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Blíže	blízce	k6eAd2	blízce
o	o	k7c6	o
rozdílech	rozdíl	k1gInPc6	rozdíl
mezi	mezi	k7c7	mezi
kolaborací	kolaborace	k1gFnSc7	kolaborace
a	a	k8xC	a
kooperací	kooperace	k1gFnPc2	kooperace
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
Kolaborace	kolaborace	k1gFnSc1	kolaborace
nebo	nebo	k8xC	nebo
kooperace	kooperace	k1gFnSc1	kooperace
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
==	==	k?	==
Prameny	pramen	k1gInPc1	pramen
==	==	k?	==
</s>
</p>
<p>
<s>
PRŮCHA	Průcha	k1gMnSc1	Průcha
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
WALTEROVÁ	WALTEROVÁ	kA	WALTEROVÁ
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
,	,	kIx,	,
MAREŠ	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Pedagogický	pedagogický	k2eAgInSc1d1	pedagogický
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Portál	portál	k1gInSc1	portál
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
322	[number]	k4	322
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7178	[number]	k4	7178
<g/>
-	-	kIx~	-
<g/>
579	[number]	k4	579
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOŠEK	Hošek	k1gMnSc1	Hošek
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
<g/>
.	.	kIx.	.
</s>
<s>
Kooperativní	kooperativní	k2eAgNnSc1d1	kooperativní
a	a	k8xC	a
kolaborativní	kolaborativní	k2eAgNnSc1d1	kolaborativní
učení	učení	k1gNnSc1	učení
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
