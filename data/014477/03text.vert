<s>
Jan	Jan	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Zavřel	zavřít	k5eAaPmAgMnS
Narození	narození	k1gNnSc4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1879	#num#	k4
<g/>
Třebíč	Třebíč	k1gFnSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1946	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
67	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Brno	Brno	k1gNnSc1
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
(	(	kIx(
<g/>
1898	#num#	k4
<g/>
–	–	k?
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
<g/>
Gymnázium	gymnázium	k1gNnSc1
Třebíč	Třebíč	k1gFnSc1
Povolání	povolání	k1gNnSc1
</s>
<s>
zoolog	zoolog	k1gMnSc1
<g/>
,	,	kIx,
entomolog	entomolog	k1gMnSc1
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
děkan	děkan	k1gMnSc1
a	a	k8xC
rektor	rektor	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
František	František	k1gMnSc1
Zavřel	zavřít	k5eAaPmAgMnS
Funkce	funkce	k1gFnPc4
</s>
<s>
děkan	děkan	k1gMnSc1
(	(	kIx(
<g/>
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
;	;	kIx,
1924	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
<g/>
rektor	rektor	k1gMnSc1
(	(	kIx(
<g/>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
;	;	kIx,
1933	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
<g/>
děkan	děkan	k1gMnSc1
(	(	kIx(
<g/>
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
;	;	kIx,
1936	#num#	k4
<g/>
–	–	k?
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1879	#num#	k4
<g/>
,	,	kIx,
Třebíč	Třebíč	k1gFnSc1
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1946	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
zoolog	zoolog	k1gMnSc1
a	a	k8xC
entomolog	entomolog	k1gMnSc1
<g/>
,	,	kIx,
děkan	děkan	k1gMnSc1
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
zoologického	zoologický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
na	na	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
fakultě	fakulta	k1gFnSc6
a	a	k8xC
rektor	rektor	k1gMnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1933	#num#	k4
a	a	k8xC
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1879	#num#	k4
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
otcem	otec	k1gMnSc7
byl	být	k5eAaImAgMnS
pedagog	pedagog	k1gMnSc1
a	a	k8xC
přírodovědec	přírodovědec	k1gMnSc1
František	František	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jan	Jan	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
vystudoval	vystudovat	k5eAaPmAgMnS
Gymnázium	gymnázium	k1gNnSc1
v	v	k7c6
Třebíči	Třebíč	k1gFnSc6
a	a	k8xC
následně	následně	k6eAd1
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1898	#num#	k4
a	a	k8xC
1903	#num#	k4
vystudoval	vystudovat	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Filozofickou	filozofický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1903	#num#	k4
a	a	k8xC
1904	#num#	k4
působil	působit	k5eAaImAgInS
jako	jako	k9
pedagog	pedagog	k1gMnSc1
na	na	k7c6
reálce	reálka	k1gFnSc6
v	v	k7c6
pražských	pražský	k2eAgFnPc6d1
Holešovicích	Holešovice	k1gFnPc6
a	a	k8xC
následně	následně	k6eAd1
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1904	#num#	k4
a	a	k8xC
1909	#num#	k4
pak	pak	k6eAd1
působil	působit	k5eAaImAgMnS
jako	jako	k9
pedagog	pedagog	k1gMnSc1
na	na	k7c6
zemské	zemský	k2eAgFnSc6d1
reálce	reálka	k1gFnSc6
v	v	k7c6
Hodoníně	Hodonín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1909	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
pak	pak	k6eAd1
působil	působit	k5eAaImAgMnS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
a	a	k8xC
posléze	posléze	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
učil	učit	k5eAaImAgMnS,k5eAaPmAgMnS
na	na	k7c6
státní	státní	k2eAgFnSc6d1
reálce	reálka	k1gFnSc6
v	v	k7c6
Praze-Podskalí	Praze-Podskalý	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
začal	začít	k5eAaPmAgInS
působil	působit	k5eAaImAgMnS
na	na	k7c6
Masarykově	Masarykův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
založil	založit	k5eAaPmAgMnS
zoologický	zoologický	k2eAgInSc4d1
ústav	ústav	k1gInSc4
při	při	k7c6
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
1924	#num#	k4
<g/>
/	/	kIx~
<g/>
1925	#num#	k4
a	a	k8xC
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
1937	#num#	k4
také	také	k9
děkanem	děkan	k1gMnSc7
fakulty	fakulta	k1gFnSc2
a	a	k8xC
ve	v	k7c6
školním	školní	k2eAgInSc6d1
roce	rok	k1gInSc6
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
1934	#num#	k4
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
i	i	k8xC
rektorem	rektor	k1gMnSc7
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Byl	být	k5eAaImAgMnS
žákem	žák	k1gMnSc7
pražského	pražský	k2eAgMnSc2d1
zoologa	zoolog	k1gMnSc2
Václava	Václav	k1gMnSc2
Vejdovského	Vejdovský	k2eAgMnSc2d1
<g/>
,	,	kIx,
při	při	k7c6
studiu	studio	k1gNnSc6
na	na	k7c6
pražské	pražský	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
vystudoval	vystudovat	k5eAaPmAgMnS
přírodní	přírodní	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
aprobaci	aprobace	k1gFnSc4
matematika-fyzika	matematika-fyzikum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stabilně	stabilně	k6eAd1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
anatomii	anatomie	k1gFnSc3
<g/>
,	,	kIx,
morfologii	morfologie	k1gFnSc3
a	a	k8xC
systematiky	systematika	k1gFnSc2
hmyzu	hmyz	k1gInSc2
<g/>
,	,	kIx,
primárně	primárně	k6eAd1
čeledi	čeleď	k1gFnSc2
pakomárovitých	pakomárovitý	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Působil	působit	k5eAaImAgMnS
jako	jako	k9
redaktor	redaktor	k1gMnSc1
časopisu	časopis	k1gInSc6
Naše	náš	k3xOp1gFnSc1
věda	věda	k1gFnSc1
<g/>
,	,	kIx,
redigoval	redigovat	k5eAaImAgInS
časopis	časopis	k1gInSc1
Práce	práce	k1gFnSc2
Moravské	moravský	k2eAgFnSc2d1
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
České	český	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
Královské	královský	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
Masarykovy	Masarykův	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
badatelské	badatelský	k2eAgFnSc2d1
<g/>
,	,	kIx,
Moravské	moravský	k2eAgFnSc2d1
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
Přírodovědeckého	přírodovědecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Biologické	biologický	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Šafaříkovy	Šafaříkův	k2eAgFnSc2d1
učené	učený	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
,	,	kIx,
Československé	československý	k2eAgNnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
společnosti	společnost	k1gFnSc2
entomologické	entomologický	k2eAgFnSc2d1
a	a	k8xC
Spolku	spolek	k1gInSc2
Kounicovy	Kounicův	k2eAgFnSc2d1
studentské	studentský	k2eAgFnSc2d1
koleje	kolej	k1gFnSc2
českých	český	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
práce	práce	k1gFnSc1
byla	být	k5eAaImAgFnS
ceněna	cenit	k5eAaImNgFnS
v	v	k7c6
mezinárodní	mezinárodní	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
jménem	jméno	k1gNnSc7
bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
několik	několik	k4yIc1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
dva	dva	k4xCgInPc1
rody	rod	k1gInPc1
pakomárů	pakomár	k1gMnPc2
–	–	k?
Zavřelia	Zavřelius	k1gMnSc4
a	a	k8xC
Zavřeliella	Zavřeliell	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
rektorů	rektor	k1gMnPc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Významní	významný	k2eAgMnPc1d1
přírodovědci	přírodovědec	k1gMnPc1
Třebíčska	Třebíčsko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Město	město	k1gNnSc1
Třebíč	Třebíč	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Matriky	matrika	k1gFnPc4
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnPc1d1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
–	–	k?
Zavřel	Zavřel	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1879	#num#	k4
<g/>
,	,	kIx,
Třebíč	Třebíč	k1gFnSc1
<g/>
1	#num#	k4
2	#num#	k4
Kal	kala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Město	město	k1gNnSc1
Brno	Brno	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KUBÍČEK	kubíčko	k1gNnPc2
<g/>
,	,	kIx,
F.	F.	kA
Jan	Jan	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jan	Jan	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Zavřel	Zavřel	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Zavřel	zavřít	k5eAaPmAgMnS
na	na	k7c4
Encyklopedii	encyklopedie	k1gFnSc4
Brna	Brno	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1152273	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7000	#num#	k4
0656	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
83228203	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
33383993	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
83228203	#num#	k4
</s>
