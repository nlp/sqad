<s>
Šicí	šicí	k2eAgFnSc1d1	šicí
jehla	jehla	k1gFnSc1	jehla
je	být	k5eAaImIp3nS	být
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
činnosti	činnost	k1gFnSc3	činnost
zvané	zvaný	k2eAgNnSc1d1	zvané
šití	šití	k1gNnSc1	šití
či	či	k8xC	či
sešívání	sešívání	k1gNnSc1	sešívání
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
spojování	spojování	k1gNnSc6	spojování
dvou	dva	k4xCgInPc2	dva
kusů	kus	k1gInPc2	kus
materiálu	materiál	k1gInSc2	materiál
pomocí	pomocí	k7c2	pomocí
nitě	nit	k1gFnSc2	nit
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiného	jiný	k2eAgNnSc2d1	jiné
vhodného	vhodný	k2eAgNnSc2d1	vhodné
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
za	za	k7c2	za
vzniků	vznik	k1gInPc2	vznik
švů	šev	k1gInPc2	šev
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
základní	základní	k2eAgInSc4d1	základní
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
oděvů	oděv	k1gInPc2	oděv
a	a	k8xC	a
pro	pro	k7c4	pro
spojování	spojování	k1gNnPc4	spojování
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
krejčovství	krejčovství	k1gNnSc2	krejčovství
se	se	k3xPyFc4	se
jehel	jehla	k1gFnPc2	jehla
využívá	využívat	k5eAaPmIp3nS	využívat
též	též	k9	též
v	v	k7c6	v
obuvnictví	obuvnictví	k1gNnSc6	obuvnictví
<g/>
,	,	kIx,	,
brašnářství	brašnářství	k1gNnSc1	brašnářství
a	a	k8xC	a
čalounictví	čalounictví	k1gNnSc1	čalounictví
apod.	apod.	kA	apod.
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Ucho	ucho	k1gNnSc1	ucho
jehly	jehla	k1gFnSc2	jehla
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
provlíká	provlíkat	k5eAaImIp3nS	provlíkat
nit	nit	k1gFnSc1	nit
<g/>
,	,	kIx,	,
těla	tělo	k1gNnSc2	tělo
jehly	jehla	k1gFnSc2	jehla
a	a	k8xC	a
špičky	špička	k1gFnSc2	špička
jehly	jehla	k1gFnSc2	jehla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
propichování	propichování	k1gNnSc4	propichování
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jehly	jehla	k1gFnPc1	jehla
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
či	či	k8xC	či
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Jehly	jehla	k1gFnSc2	jehla
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
ruční	ruční	k2eAgNnSc4d1	ruční
šití	šití	k1gNnSc4	šití
tak	tak	k8xC	tak
i	i	k9	i
pro	pro	k7c4	pro
šití	šití	k1gNnSc4	šití
na	na	k7c6	na
šicích	šicí	k2eAgInPc6d1	šicí
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Příbuznými	příbuzný	k2eAgInPc7d1	příbuzný
nástroji	nástroj	k1gInPc7	nástroj
jehly	jehla	k1gFnSc2	jehla
je	být	k5eAaImIp3nS	být
ševcovské	ševcovský	k2eAgNnSc4d1	ševcovské
šídlo	šídlo	k1gNnSc4	šídlo
a	a	k8xC	a
špendlík	špendlík	k1gInSc4	špendlík
<g/>
.	.	kIx.	.
</s>
<s>
Jehla	jehla	k1gFnSc1	jehla
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
starý	starý	k2eAgInSc1d1	starý
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
jako	jako	k9	jako
potřeba	potřeba	k1gFnSc1	potřeba
pro	pro	k7c4	pro
spojování	spojování	k1gNnPc4	spojování
materiálů	materiál	k1gInPc2	materiál
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
jehly	jehla	k1gFnPc1	jehla
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
propichování	propichování	k1gNnSc4	propichování
kožešiny	kožešina	k1gFnSc2	kožešina
a	a	k8xC	a
neměly	mít	k5eNaImAgFnP	mít
ucho	ucho	k1gNnSc4	ucho
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k9	jako
mají	mít	k5eAaImIp3nP	mít
dnešní	dnešní	k2eAgFnSc2d1	dnešní
moderní	moderní	k2eAgFnSc2d1	moderní
jehly	jehla	k1gFnSc2	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Uchem	ucho	k1gNnSc7	ucho
jehly	jehla	k1gFnSc2	jehla
je	být	k5eAaImIp3nS	být
provlečena	provlečen	k2eAgFnSc1d1	provlečena
nit	nit	k1gFnSc1	nit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
při	při	k7c6	při
šití	šití	k1gNnSc6	šití
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
skrze	skrze	k?	skrze
spojované	spojovaný	k2eAgFnSc2d1	spojovaná
látky	látka	k1gFnSc2	látka
respektive	respektive	k9	respektive
sešívané	sešívaný	k2eAgInPc1d1	sešívaný
předměty	předmět	k1gInPc1	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Náprstek	náprstek	k1gInSc1	náprstek
Hříbek	hříbek	k1gInSc1	hříbek
(	(	kIx(	(
<g/>
pomůcka	pomůcka	k1gFnSc1	pomůcka
<g/>
)	)	kIx)	)
Špendlík	špendlík	k1gInSc1	špendlík
Nit	nit	k1gFnSc1	nit
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
šicí	šicí	k2eAgFnSc1d1	šicí
jehla	jehla	k1gFnSc1	jehla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jehla	jehla	k1gFnSc1	jehla
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
