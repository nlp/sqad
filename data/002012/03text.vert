<s>
John	John	k1gMnSc1	John
Anthony	Anthona	k1gFnSc2	Anthona
Frusciante	Frusciant	k1gMnSc5	Frusciant
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
skupiny	skupina	k1gFnSc2	skupina
Red	Red	k1gFnSc2	Red
Hot	hot	k0	hot
Chili	Chil	k1gMnSc3	Chil
Peppers	Peppersa	k1gFnPc2	Peppersa
<g/>
.	.	kIx.	.
</s>
<s>
Frusciante	Frusciant	k1gMnSc5	Frusciant
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc2	jeho
předchůdce	předchůdce	k1gMnSc2	předchůdce
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
Hillel	Hillel	k1gMnSc1	Hillel
Slovak	Slovak	k1gMnSc1	Slovak
<g/>
,	,	kIx,	,
předávkoval	předávkovat	k5eAaPmAgMnS	předávkovat
heroinem	heroin	k1gInSc7	heroin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1989	[number]	k4	1989
natočil	natočit	k5eAaBmAgInS	natočit
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
první	první	k4xOgNnSc4	první
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Mother	Mothra	k1gFnPc2	Mothra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Milk	Milk	k1gInSc1	Milk
a	a	k8xC	a
z	z	k7c2	z
nepříliš	příliš	k6eNd1	příliš
známé	známý	k2eAgFnSc2d1	známá
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
staly	stát	k5eAaPmAgFnP	stát
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
studiovém	studiový	k2eAgInSc6d1	studiový
počinu	počin	k1gInSc6	počin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
desce	deska	k1gFnSc6	deska
Blood	Blood	k1gInSc4	Blood
Sugar	Sugar	k1gInSc1	Sugar
Sex	sex	k1gInSc1	sex
Magik	magika	k1gFnPc2	magika
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
již	již	k9	již
skupina	skupina	k1gFnSc1	skupina
slavná	slavný	k2eAgFnSc1d1	slavná
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
slávu	sláva	k1gFnSc4	sláva
ale	ale	k8xC	ale
Frusciante	Frusciant	k1gMnSc5	Frusciant
nesl	nést	k5eAaImAgMnS	nést
docela	docela	k6eAd1	docela
těžce	těžce	k6eAd1	těžce
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
brát	brát	k5eAaImF	brát
heroin	heroin	k1gInSc4	heroin
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
kapele	kapela	k1gFnSc3	kapela
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
koncertem	koncert	k1gInSc7	koncert
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Trpěl	trpět	k5eAaImAgInS	trpět
zřejmě	zřejmě	k6eAd1	zřejmě
maniodepresivní	maniodepresivní	k2eAgFnSc7d1	maniodepresivní
psychózou	psychóza	k1gFnSc7	psychóza
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
rychle	rychle	k6eAd1	rychle
z	z	k7c2	z
kopce	kopec	k1gInSc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nevycházel	vycházet	k5eNaImAgMnS	vycházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
odřízl	odříznout	k5eAaPmAgMnS	odříznout
se	se	k3xPyFc4	se
od	od	k7c2	od
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
bral	brát	k5eAaImAgMnS	brát
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
natočil	natočit	k5eAaBmAgMnS	natočit
také	také	k9	také
dvě	dva	k4xCgFnPc4	dva
sólové	sólový	k2eAgFnPc4d1	sólová
desky	deska	k1gFnPc4	deska
-	-	kIx~	-
"	"	kIx"	"
<g/>
Niandra	Niandra	k1gFnSc1	Niandra
Lades	Ladesa	k1gFnPc2	Ladesa
and	and	k?	and
Usually	Usuall	k1gInPc1	Usuall
Just	just	k6eAd1	just
a	a	k8xC	a
T-Shirt	T-Shirt	k1gInSc4	T-Shirt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
deska	deska	k1gFnSc1	deska
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gNnSc2	jeho
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
drogové	drogový	k2eAgFnSc2d1	drogová
závislosti	závislost	k1gFnSc2	závislost
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Smile	smil	k1gInSc5	smil
From	From	k1gInSc4	From
the	the	k?	the
Streets	Streets	k1gInSc1	Streets
You	You	k1gMnSc1	You
Hold	hold	k1gInSc1	hold
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
tyto	tento	k3xDgFnPc4	tento
desky	deska	k1gFnPc4	deska
stydí	stydět	k5eAaImIp3nP	stydět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byly	být	k5eAaImAgFnP	být
natočeny	natočit	k5eAaBmNgFnP	natočit
jen	jen	k9	jen
pro	pro	k7c4	pro
potřebu	potřeba	k1gFnSc4	potřeba
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
infekci	infekce	k1gFnSc3	infekce
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
má	mít	k5eAaImIp3nS	mít
ruce	ruka	k1gFnPc4	ruka
plné	plný	k2eAgFnPc4d1	plná
jizev	jizva	k1gFnPc2	jizva
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
bývalá	bývalý	k2eAgFnSc1d1	bývalá
kapela	kapela	k1gFnSc1	kapela
mezitím	mezitím	k6eAd1	mezitím
natočila	natočit	k5eAaBmAgFnS	natočit
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Davem	Dav	k1gInSc7	Dav
Navarrem	Navarrma	k1gFnPc2	Navarrma
poměrně	poměrně	k6eAd1	poměrně
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
One	One	k1gFnSc1	One
Hot	hot	k0	hot
Minute	Minut	k1gInSc5	Minut
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
kapela	kapela	k1gFnSc1	kapela
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
s	s	k7c7	s
výsledkem	výsledek	k1gInSc7	výsledek
spokojena	spokojen	k2eAgFnSc1d1	spokojena
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
Navarro	Navarra	k1gFnSc5	Navarra
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
těžké	těžký	k2eAgFnSc2d1	těžká
drogové	drogový	k2eAgFnSc2d1	drogová
závislosti	závislost	k1gFnSc2	závislost
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Dave	Dav	k1gInSc5	Dav
musel	muset	k5eAaImAgMnS	muset
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
vypadalo	vypadat	k5eAaPmAgNnS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
redhoti	redhot	k1gMnPc1	redhot
<g/>
"	"	kIx"	"
rozpadnou	rozpadnout	k5eAaPmIp3nP	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Frusciante	Frusciant	k1gMnSc5	Frusciant
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
už	už	k6eAd1	už
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
odepisovala	odepisovat	k5eAaImAgFnS	odepisovat
<g/>
,	,	kIx,	,
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
protidrogovou	protidrogový	k2eAgFnSc4d1	protidrogová
léčbu	léčba	k1gFnSc4	léčba
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
po	po	k7c6	po
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
dohodě	dohoda	k1gFnSc6	dohoda
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
JF	JF	kA	JF
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
zachránili	zachránit	k5eAaPmAgMnP	zachránit
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
během	během	k7c2	během
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
natočila	natočit	k5eAaBmAgFnS	natočit
album	album	k1gNnSc4	album
Californication	Californication	k1gInSc1	Californication
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
singlu	singl	k1gInSc3	singl
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Scar	Scara	k1gFnPc2	Scara
Tissue	Tissu	k1gInSc2	Tissu
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
videoklip	videoklip	k1gInSc1	videoklip
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
členy	člen	k1gMnPc4	člen
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xC	jako
čtyři	čtyři	k4xCgInPc4	čtyři
unavené	unavený	k2eAgInPc4d1	unavený
a	a	k8xC	a
poraněné	poraněný	k2eAgMnPc4d1	poraněný
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jedou	jet	k5eAaImIp3nP	jet
autem	aut	k1gInSc7	aut
vyprahlou	vyprahlý	k2eAgFnSc7d1	vyprahlá
pouští	poušť	k1gFnSc7	poušť
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Frusciante	Frusciant	k1gMnSc5	Frusciant
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
klipu	klip	k1gInSc6	klip
na	na	k7c4	na
zlomenou	zlomený	k2eAgFnSc4d1	zlomená
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
také	také	k9	také
řídí	řídit	k5eAaImIp3nS	řídit
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgMnS	mít
řidičské	řidičský	k2eAgNnSc4d1	řidičské
povolení	povolení	k1gNnSc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
obrovským	obrovský	k2eAgInSc7d1	obrovský
hitem	hit	k1gInSc7	hit
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
svůj	svůj	k3xOyFgInSc4	svůj
status	status	k1gInSc4	status
legendy	legenda	k1gFnSc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vydává	vydávat	k5eAaImIp3nS	vydávat
skupina	skupina	k1gFnSc1	skupina
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
By	by	k9	by
The	The	k1gMnSc1	The
Way	Way	k1gMnSc1	Way
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Frusciantoveovi	Frusciantoveus	k1gMnSc3	Frusciantoveus
povedl	povést	k5eAaPmAgInS	povést
husarský	husarský	k2eAgInSc1d1	husarský
kousek	kousek	k1gInSc1	kousek
<g/>
,	,	kIx,	,
když	když	k8xS	když
během	během	k7c2	během
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
vydal	vydat	k5eAaPmAgMnS	vydat
sedm	sedm	k4xCc1	sedm
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
6	[number]	k4	6
během	během	k7c2	během
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Frusciante	Frusciant	k1gMnSc5	Frusciant
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
z	z	k7c2	z
drogové	drogový	k2eAgFnSc2d1	drogová
závislosti	závislost	k1gFnSc2	závislost
vyléčen	vyléčen	k2eAgMnSc1d1	vyléčen
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
zastánce	zastánce	k1gMnSc2	zastánce
zdravé	zdravý	k2eAgFnSc2d1	zdravá
výživy	výživa	k1gFnSc2	výživa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydávají	vydávat	k5eAaImIp3nP	vydávat
Redhoti	Redhot	k1gMnPc1	Redhot
další	další	k2eAgFnSc4d1	další
řadové	řadový	k2eAgNnSc1d1	řadové
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Stadium	stadium	k1gNnSc1	stadium
Arcadium	Arcadium	k1gNnSc4	Arcadium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
definitivně	definitivně	k6eAd1	definitivně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
věnování	věnování	k1gNnSc2	věnování
se	se	k3xPyFc4	se
čistě	čistě	k6eAd1	čistě
sólové	sólový	k2eAgFnSc3d1	sólová
dráze	dráha	k1gFnSc3	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
a	a	k8xC	a
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Josh	Josh	k1gMnSc1	Josh
Klinghoffer	Klinghoffer	k1gMnSc1	Klinghoffer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
podnikl	podniknout	k5eAaPmAgMnS	podniknout
turné	turné	k1gNnSc4	turné
Stadium	stadium	k1gNnSc1	stadium
Arcadium	Arcadium	k1gNnSc1	Arcadium
jako	jako	k8xS	jako
doprovodný	doprovodný	k2eAgMnSc1d1	doprovodný
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
-	-	kIx~	-
Niandra	Niandra	k1gFnSc1	Niandra
Lades	Ladesa	k1gFnPc2	Ladesa
and	and	k?	and
Usually	Usuall	k1gInPc1	Usuall
Just	just	k6eAd1	just
a	a	k8xC	a
T-Shirt	T-Shirt	k1gInSc1	T-Shirt
1997	[number]	k4	1997
-	-	kIx~	-
Smile	smil	k1gInSc5	smil
From	Froma	k1gFnPc2	Froma
the	the	k?	the
Streets	Streetsa	k1gFnPc2	Streetsa
You	You	k1gFnSc1	You
Hold	hold	k1gInSc1	hold
2001	[number]	k4	2001
-	-	kIx~	-
From	From	k1gInSc1	From
the	the	k?	the
Sounds	Sounds	k1gInSc1	Sounds
Inside	Insid	k1gInSc5	Insid
2001	[number]	k4	2001
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
To	ten	k3xDgNnSc1	ten
Record	Record	k1gInSc4	Record
Only	Onla	k1gMnSc2	Onla
Water	Watero	k1gNnPc2	Watero
for	forum	k1gNnPc2	forum
Ten	ten	k3xDgInSc1	ten
Days	Days	k1gInSc1	Days
2004	[number]	k4	2004
-	-	kIx~	-
Shadows	Shadows	k1gInSc1	Shadows
Collide	Collid	k1gInSc5	Collid
with	with	k1gInSc4	with
People	People	k1gFnPc4	People
2004	[number]	k4	2004
-	-	kIx~	-
The	The	k1gMnSc1	The
Will	Will	k1gMnSc1	Will
to	ten	k3xDgNnSc4	ten
Death	Death	k1gInSc1	Death
2004	[number]	k4	2004
-	-	kIx~	-
Automatic	Automatice	k1gFnPc2	Automatice
Writing	Writing	k1gInSc1	Writing
2004	[number]	k4	2004
-	-	kIx~	-
Inside	Insid	k1gInSc5	Insid
of	of	k?	of
Emptiness	Emptiness	k1gInSc1	Emptiness
2004	[number]	k4	2004
-	-	kIx~	-
A	a	k8xC	a
Sphere	Spher	k1gInSc5	Spher
in	in	k?	in
the	the	k?	the
Heart	Heart	k1gInSc1	Heart
of	of	k?	of
Silence	silenka	k1gFnSc6	silenka
2004	[number]	k4	2004
-	-	kIx~	-
DC	DC	kA	DC
EP	EP	kA	EP
2005	[number]	k4	2005
-	-	kIx~	-
Curtains	Curtains	k1gInSc1	Curtains
2007	[number]	k4	2007
-	-	kIx~	-
AW	AW	kA	AW
II	II	kA	II
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
The	The	k1gFnSc1	The
Empyrean	Empyrean	k1gInSc1	Empyrean
2012	[number]	k4	2012
-	-	kIx~	-
Letur-Lefr	Letur-Lefr	k1gMnSc1	Letur-Lefr
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
PBX	PBX	kA	PBX
Funicular	Funicular	k1gMnSc1	Funicular
Intaglio	Intaglio	k1gMnSc1	Intaglio
Zone	Zone	k1gInSc4	Zone
2013	[number]	k4	2013
-	-	kIx~	-
Outsides	Outsides	k1gInSc1	Outsides
EP	EP	kA	EP
1989	[number]	k4	1989
-	-	kIx~	-
Mother	Mothra	k1gFnPc2	Mothra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Milk	Milk	k1gInSc1	Milk
1991	[number]	k4	1991
-	-	kIx~	-
Blood	Blood	k1gInSc1	Blood
Sugar	Sugara	k1gFnPc2	Sugara
Sex	sex	k1gInSc1	sex
Magik	magik	k1gMnSc1	magik
1999	[number]	k4	1999
-	-	kIx~	-
Californication	Californication	k1gInSc1	Californication
2002	[number]	k4	2002
-	-	kIx~	-
By	by	k9	by
the	the	k?	the
Way	Way	k1gFnSc4	Way
2006	[number]	k4	2006
-	-	kIx~	-
Stadium	stadium	k1gNnSc4	stadium
Arcadium	Arcadium	k1gNnSc1	Arcadium
2	[number]	k4	2
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Mars	Mars	k1gInSc1	Mars
2004	[number]	k4	2004
-	-	kIx~	-
The	The	k1gMnPc2	The
Brown	Brown	k1gMnSc1	Brown
Bunny	Bunna	k1gFnSc2	Bunna
Dobrým	dobrý	k2eAgMnSc7d1	dobrý
přítelem	přítel	k1gMnSc7	přítel
Johna	John	k1gMnSc2	John
Frusciantea	Fruscianteum	k1gNnSc2	Fruscianteum
je	být	k5eAaImIp3nS	být
slavný	slavný	k2eAgMnSc1d1	slavný
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
Johnny	Johnna	k1gFnSc2	Johnna
Depp	Depp	k1gMnSc1	Depp
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
podporoval	podporovat	k5eAaImAgMnS	podporovat
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
jeho	jeho	k3xOp3gFnSc2	jeho
první	první	k4xOgFnSc2	první
sólové	sólový	k2eAgFnSc2d1	sólová
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Frusciante	Frusciant	k1gMnSc5	Frusciant
několikrát	několikrát	k6eAd1	několikrát
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
deskách	deska	k1gFnPc6	deska
progresive-rockové	progresiveockový	k2eAgFnSc2d1	progresive-rockový
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Mars	Mars	k1gMnSc1	Mars
Volta	Volta	k1gFnSc1	Volta
<g/>
.	.	kIx.	.
</s>
