<s>
Grafit	grafit	k1gInSc1	grafit
(	(	kIx(	(
<g/>
Werner	Werner	k1gMnSc1	Werner
<g/>
,	,	kIx,	,
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
C	C	kA	C
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šesterečný	šesterečný	k2eAgInSc4d1	šesterečný
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
grafein	grafein	k1gInSc1	grafein
-	-	kIx~	-
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nekovové	kovový	k2eNgInPc4d1	nekovový
minerály	minerál	k1gInPc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
název	název	k1gInSc1	název
pro	pro	k7c4	pro
grafit	grafit	k1gInSc4	grafit
je	být	k5eAaImIp3nS	být
tuha	tuha	k1gFnSc1	tuha
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
pigment	pigment	k1gInSc4	pigment
ve	v	k7c6	v
vápencích	vápenec	k1gInPc6	vápenec
a	a	k8xC	a
jílovitých	jílovitý	k2eAgFnPc6d1	jílovitá
břidlicích	břidlice	k1gFnPc6	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Ložiska	ložisko	k1gNnPc1	ložisko
grafitu	grafit	k1gInSc2	grafit
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
přeměně	přeměna	k1gFnSc6	přeměna
usazených	usazený	k2eAgFnPc2d1	usazená
hornin	hornina	k1gFnPc2	hornina
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
vrstvy	vrstva	k1gFnPc1	vrstva
nebo	nebo	k8xC	nebo
čočkovitá	čočkovitý	k2eAgNnPc1d1	čočkovité
tělesa	těleso	k1gNnPc1	těleso
v	v	k7c6	v
rulách	rula	k1gFnPc6	rula
<g/>
,	,	kIx,	,
svorech	svor	k1gInPc6	svor
<g/>
,	,	kIx,	,
fylitech	fylit	k1gInPc6	fylit
nebo	nebo	k8xC	nebo
mramorech	mramor	k1gInPc6	mramor
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
magmatického	magmatický	k2eAgInSc2d1	magmatický
původu	původ	k1gInSc2	původ
-	-	kIx~	-
nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
v	v	k7c6	v
pegmatitech	pegmatit	k1gInPc6	pegmatit
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
šupinky	šupinka	k1gFnPc1	šupinka
<g/>
,	,	kIx,	,
ploténky	ploténka	k1gFnPc1	ploténka
<g/>
,	,	kIx,	,
sférické	sférický	k2eAgInPc1d1	sférický
agregáty	agregát	k1gInPc1	agregát
<g/>
,	,	kIx,	,
zemité	zemitý	k2eAgFnPc1d1	zemitá
výplně	výplň	k1gFnPc1	výplň
<g/>
.	.	kIx.	.
</s>
<s>
Krystaly	krystal	k1gInPc1	krystal
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vzácně	vzácně	k6eAd1	vzácně
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
hexagonálních	hexagonální	k2eAgFnPc2d1	hexagonální
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Píše	psát	k5eAaImIp3nS	psát
po	po	k7c6	po
papíře	papír	k1gInSc6	papír
<g/>
,	,	kIx,	,
otírá	otírat	k5eAaImIp3nS	otírat
se	se	k3xPyFc4	se
o	o	k7c4	o
prsty	prst	k1gInPc4	prst
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
tvrdost	tvrdost	k1gFnSc1	tvrdost
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
omak	omak	k1gInSc4	omak
mastný	mastný	k2eAgInSc4d1	mastný
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
2,1	[number]	k4	2,1
<g/>
–	–	k?	–
<g/>
2,3	[number]	k4	2,3
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
(	(	kIx(	(
<g/>
kolísá	kolísat	k5eAaImIp3nS	kolísat
vlivem	vliv	k1gInSc7	vliv
přimíšenin	přimíšenina	k1gFnPc2	přimíšenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
nerovný	rovný	k2eNgInSc1d1	nerovný
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
3	[number]	k4	3
000	[number]	k4	000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
vede	vést	k5eAaImIp3nS	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
až	až	k9	až
ocelově	ocelově	k6eAd1	ocelově
šedá	šedý	k2eAgNnPc1d1	šedé
<g/>
.	.	kIx.	.
</s>
<s>
Vryp	vryp	k1gInSc1	vryp
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
ocelově	ocelově	k6eAd1	ocelově
šedý	šedý	k2eAgInSc1d1	šedý
a	a	k8xC	a
lesklý	lesklý	k2eAgInSc1d1	lesklý
<g/>
.	.	kIx.	.
</s>
<s>
Průhlednost	průhlednost	k1gFnSc1	průhlednost
<g/>
:	:	kIx,	:
neprůhledný	průhledný	k2eNgMnSc1d1	neprůhledný
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
<g/>
:	:	kIx,	:
kovový	kovový	k2eAgInSc1d1	kovový
až	až	k8xS	až
matný	matný	k2eAgInSc1d1	matný
Chemické	chemický	k2eAgFnSc3d1	chemická
vlastnosti	vlastnost	k1gFnSc3	vlastnost
<g/>
:	:	kIx,	:
Tvořen	tvořit	k5eAaImNgMnS	tvořit
uhlíkem	uhlík	k1gInSc7	uhlík
s	s	k7c7	s
příměsemi	příměse	k1gFnPc7	příměse
H	H	kA	H
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
aj.	aj.	kA	aj.
V	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
HNO3	HNO3	k1gFnSc7	HNO3
při	při	k7c6	při
povaření	povaření	k1gNnSc6	povaření
<g/>
.	.	kIx.	.
chaoit	chaoit	k1gInSc1	chaoit
<g/>
,	,	kIx,	,
diamant	diamant	k1gInSc1	diamant
<g/>
,	,	kIx,	,
lonsdaleit	lonsdaleit	k1gInSc1	lonsdaleit
Díky	díky	k7c3	díky
vlastnostem	vlastnost	k1gFnPc3	vlastnost
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
-	-	kIx~	-
tuha	tuha	k1gFnSc1	tuha
v	v	k7c6	v
tužkách	tužka	k1gFnPc6	tužka
je	být	k5eAaImIp3nS	být
vypálená	vypálený	k2eAgFnSc1d1	vypálená
směs	směs	k1gFnSc1	směs
grafitu	grafit	k1gInSc2	grafit
a	a	k8xC	a
jílovitých	jílovitý	k2eAgInPc2d1	jílovitý
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
uhlíky	uhlík	k1gInPc1	uhlík
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
mechanických	mechanický	k2eAgInPc2d1	mechanický
komutátorů	komutátor	k1gInPc2	komutátor
v	v	k7c6	v
elektromotorech	elektromotor	k1gInPc6	elektromotor
<g/>
.	.	kIx.	.
</s>
<s>
Grafit	grafit	k1gInSc1	grafit
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
moderátor	moderátor	k1gMnSc1	moderátor
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metalurgickém	metalurgický	k2eAgInSc6d1	metalurgický
průmyslu	průmysl	k1gInSc6	průmysl
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
značné	značný	k2eAgFnSc3d1	značná
tepelné	tepelný	k2eAgFnSc3d1	tepelná
odolnosti	odolnost	k1gFnSc3	odolnost
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
tavicí	tavicí	k2eAgInPc4d1	tavicí
kelímky	kelímek	k1gInPc4	kelímek
<g/>
,	,	kIx,	,
vyzdívky	vyzdívka	k1gFnPc4	vyzdívka
nístěje	nístěj	k1gFnSc2	nístěj
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pece	pec	k1gFnSc2	pec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
separátor	separátor	k1gInSc1	separátor
slouží	sloužit	k5eAaImIp3nS	sloužit
na	na	k7c4	na
vymazávání	vymazávání	k1gNnSc4	vymazávání
slévárenských	slévárenský	k2eAgFnPc2d1	Slévárenská
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
grafitu	grafit	k1gInSc2	grafit
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
elektrody	elektroda	k1gFnPc1	elektroda
pro	pro	k7c4	pro
elektrolytickou	elektrolytický	k2eAgFnSc4d1	elektrolytická
výrobu	výroba	k1gFnSc4	výroba
hliníku	hliník	k1gInSc2	hliník
nebo	nebo	k8xC	nebo
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
elektrody	elektroda	k1gFnPc1	elektroda
do	do	k7c2	do
obloukových	obloukový	k2eAgFnPc2d1	oblouková
pecí	pec	k1gFnPc2	pec
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
suchých	suchý	k2eAgNnPc2d1	suché
i	i	k8xC	i
olejových	olejový	k2eAgNnPc2d1	olejové
maziv	mazivo	k1gNnPc2	mazivo
(	(	kIx(	(
<g/>
plastické	plastický	k2eAgNnSc1d1	plastické
mazivo	mazivo	k1gNnSc1	mazivo
s	s	k7c7	s
grafitem	grafit	k1gInSc7	grafit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
součást	součást	k1gFnSc1	součást
záporných	záporný	k2eAgFnPc2d1	záporná
elektrod	elektroda	k1gFnPc2	elektroda
lithium-iontových	lithiumontův	k2eAgFnPc2d1	lithium-iontův
baterií	baterie	k1gFnPc2	baterie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
Bližná	Bližný	k2eAgFnSc1d1	Bližná
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
v	v	k7c6	v
Pošumaví	Pošumaví	k1gNnSc6	Pošumaví
<g/>
,	,	kIx,	,
Koloděje	Koloděje	k1gFnPc1	Koloděje
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
pod	pod	k7c7	pod
Sněžníkem	Sněžník	k1gInSc7	Sněžník
<g/>
,	,	kIx,	,
Velké	velký	k2eAgInPc4d1	velký
Tresné	Tresný	k2eAgInPc4d1	Tresný
Svět	svět	k1gInSc4	svět
<g/>
:	:	kIx,	:
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Pasova	Pasov	k1gInSc2	Pasov
magmatická	magmatický	k2eAgNnPc1d1	magmatické
ložiska	ložisko	k1gNnPc1	ložisko
<g/>
,	,	kIx,	,
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Harz	Harz	k1gInSc4	Harz
metamorfní	metamorfní	k2eAgNnPc4d1	metamorfní
ložiska	ložisko	k1gNnPc4	ložisko
<g/>
;	;	kIx,	;
USA	USA	kA	USA
–	–	k?	–
státy	stát	k1gInPc1	stát
Alabama	Alabam	k1gMnSc2	Alabam
<g/>
,	,	kIx,	,
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
;	;	kIx,	;
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
Tunguzská	tunguzský	k2eAgFnSc1d1	Tunguzská
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
;	;	kIx,	;
Kanada	Kanada	k1gFnSc1	Kanada
–	–	k?	–
provincie	provincie	k1gFnSc2	provincie
Québec	Québec	k1gInSc4	Québec
žilná	žilný	k2eAgNnPc4d1	žilné
ložiska	ložisko	k1gNnPc4	ložisko
<g/>
;	;	kIx,	;
Srí	Srí	k1gFnSc1	Srí
Lanka	lanko	k1gNnSc2	lanko
–	–	k?	–
žilná	žilný	k2eAgNnPc1d1	žilné
ložiska	ložisko	k1gNnPc1	ložisko
<g/>
;	;	kIx,	;
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
–	–	k?	–
metamorfní	metamorfní	k2eAgNnPc1d1	metamorfní
ložiska	ložisko	k1gNnPc1	ložisko
<g/>
;	;	kIx,	;
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
</s>
