<s>
Napoleon	napoleon	k1gInSc1	napoleon
byl	být	k5eAaImAgInS	být
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
Victoru	Victor	k1gMnSc3	Victor
Hugovi	Hugo	k1gMnSc3	Hugo
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
Bourbounská	Bourbounský	k2eAgFnSc1d1	Bourbounský
Monarchie	monarchie	k1gFnSc1	monarchie
byla	být	k5eAaImAgFnS	být
obnovena	obnovit	k5eAaPmNgFnS	obnovit
před	před	k7c7	před
jeho	jeho	k3xOp3gFnPc7	jeho
třináctinami	třináctina	k1gFnPc7	třináctina
<g/>
.	.	kIx.	.
</s>
