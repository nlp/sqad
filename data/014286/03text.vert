<s>
Amazónek	Amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc1d1
</s>
<s>
Amazónek	Amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc4d1
Amazónek	Amazónek	k1gInSc4
bělobřichý	bělobřichý	k2eAgInSc4d1
<g/>
,	,	kIx,
poddruh	poddruh	k1gInSc4
Pionites	Pionitesa	k1gFnPc2
leucogaster	leucogaster	k1gMnSc1
xanthomerius	xanthomerius	k1gMnSc1
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
ohrožený	ohrožený	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Craniata	Craniat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Aves	Aves	k1gInSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
papoušci	papoušek	k1gMnPc1
(	(	kIx(
<g/>
Psittaciformes	Psittaciformes	k1gMnSc1
<g/>
)	)	kIx)
Podřád	podřád	k1gInSc1
</s>
<s>
papouškovití	papouškovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Psittacidae	Psittacidae	k1gNnSc7
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc4
</s>
<s>
Psittaculidae	Psittaculidae	k1gFnSc1
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
Arinae	Arinae	k6eAd1
Tribus	Tribus	k1gInSc1
</s>
<s>
Arini	Arieň	k1gFnSc3
Rod	rod	k1gInSc4
</s>
<s>
amazónek	amazónek	k1gInSc1
(	(	kIx(
<g/>
Pionites	Pionites	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Pionites	Pionites	k1gMnSc1
leucogasterKuhl	leucogasterKuhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
1820	#num#	k4
</s>
<s>
Mapa	mapa	k1gFnSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Amazónek	Amazónek	k1gInSc1
bělokřídlý	bělokřídlý	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Pionites	Pionites	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ohrožený	ohrožený	k2eAgInSc4d1
druh	druh	k1gInSc4
papouška	papoušek	k1gMnSc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
amazónkem	amazónek	k1gInSc7
černotemenným	černotemenný	k2eAgInSc7d1
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
(	(	kIx(
<g/>
popřípadě	popřípadě	k6eAd1
čtyř	čtyři	k4xCgMnPc2
<g/>
)	)	kIx)
druhů	druh	k1gMnPc2
papoušků	papoušek	k1gMnPc2
z	z	k7c2
rodu	rod	k1gInSc2
Pionites	Pionitesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Dvojice	dvojice	k1gFnSc1
amazónků	amazónek	k1gInPc2
bělobřichých	bělobřichý	k2eAgInPc2d1
v	v	k7c6
zajetí	zajetí	k1gNnSc6
</s>
<s>
Amazónek	Amazónek	k1gInSc1
bělokřídlý	bělokřídlý	k2eAgMnSc1d1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
převážně	převážně	k6eAd1
ve	v	k7c6
vlhkých	vlhký	k2eAgInPc6d1
amazonských	amazonský	k2eAgInPc6d1
pralesích	prales	k1gInPc6
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
od	od	k7c2
Amazonky	Amazonka	k1gFnSc2
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
,	,	kIx,
Peru	Peru	k1gNnSc6
a	a	k8xC
Bolívii	Bolívie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
žije	žít	k5eAaImIp3nS
je	on	k3xPp3gFnPc4
poměrně	poměrně	k6eAd1
běžný	běžný	k2eAgMnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
snadné	snadný	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
spatřit	spatřit	k5eAaPmF
v	v	k7c6
rozsáhlých	rozsáhlý	k2eAgFnPc6d1
chráněných	chráněný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
národní	národní	k2eAgInPc4d1
parky	park	k1gInPc4
Cristalino	Cristalin	k2eAgNnSc1d1
<g/>
,	,	kIx,
Xingu	Xing	k1gInSc2
a	a	k8xC
Amazônia	Amazônium	k1gNnSc2
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Amazónek	Amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc1d1
25	#num#	k4
cm	cm	kA
a	a	k8xC
váží	vážit	k5eAaImIp3nS
170	#num#	k4
g	g	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
charakteristický	charakteristický	k2eAgInSc1d1
především	především	k9
zbarvením	zbarvení	k1gNnSc7
hlavy	hlava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
oranžové	oranžový	k2eAgNnSc1d1
temeno	temeno	k1gNnSc1
<g/>
,	,	kIx,
hlava	hlava	k1gFnSc1
je	být	k5eAaImIp3nS
žlutá	žlutý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křídla	křídlo	k1gNnPc1
<g/>
,	,	kIx,
ocas	ocas	k1gInSc1
a	a	k8xC
stehna	stehno	k1gNnPc1
jsou	být	k5eAaImIp3nP
tmavě	tmavě	k6eAd1
zelené	zelený	k2eAgInPc1d1
<g/>
,	,	kIx,
břicho	břicho	k1gNnSc1
je	být	k5eAaImIp3nS
celé	celý	k2eAgFnPc4d1
bílé	bílý	k2eAgFnPc4d1
(	(	kIx(
<g/>
podle	podle	k7c2
této	tento	k3xDgFnSc2
barvy	barva	k1gFnSc2
břicha	břicho	k1gNnSc2
byl	být	k5eAaImAgMnS
také	také	k9
tento	tento	k3xDgMnSc1
papoušek	papoušek	k1gMnSc1
pojmenován	pojmenovat	k5eAaPmNgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podbřišek	podbřišek	k1gInSc1
je	být	k5eAaImIp3nS
žlutý	žlutý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amazónek	Amazónek	k1gInSc1
má	mít	k5eAaImIp3nS
též	též	k9
růžové	růžový	k2eAgFnPc4d1
nohy	noha	k1gFnPc4
a	a	k8xC
zobák	zobák	k1gInSc4
<g/>
,	,	kIx,
kolem	kolem	k7c2
oka	oko	k1gNnSc2
má	mít	k5eAaImIp3nS
bílý	bílý	k2eAgInSc1d1
až	až	k8xS
růžový	růžový	k2eAgInSc1d1
oční	oční	k2eAgInSc1d1
kroužek	kroužek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oko	oko	k1gNnSc1
je	být	k5eAaImIp3nS
zbarveno	zbarvit	k5eAaPmNgNnS
červeně	červeně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Poddruhy	poddruh	k1gInPc1
</s>
<s>
Amazónek	Amazónek	k1gInSc1
bělokřídly	bělokřídly	k?
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
tři	tři	k4xCgInPc4
poddruhy	poddruh	k1gInPc4
(	(	kIx(
<g/>
Pionites	Pionites	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
<g/>
,	,	kIx,
Pionites	Pionites	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
xanthomerius	xanthomerius	k1gMnSc1
a	a	k8xC
Pionites	Pionites	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
xanthurus	xanthurus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
jsou	být	k5eAaImIp3nP
však	však	k9
někdy	někdy	k6eAd1
považovány	považován	k2eAgInPc1d1
za	za	k7c4
samostatné	samostatný	k2eAgInPc4d1
druhy	druh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Pionites	Pionites	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
(	(	kIx(
<g/>
amazónek	amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc1d1
severní	severní	k2eAgInSc1d1
<g/>
)	)	kIx)
–	–	k?
základní	základní	k2eAgInSc4d1
druh	druh	k1gInSc4
<g/>
,	,	kIx,
popsaný	popsaný	k2eAgInSc4d1
výše	vysoce	k6eAd2
</s>
<s>
Pionites	Pionites	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
xanthomerius	xanthomerius	k1gMnSc1
(	(	kIx(
<g/>
amazónek	amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc1d1
ekvádorský	ekvádorský	k2eAgInSc1d1
<g/>
)	)	kIx)
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
základního	základní	k2eAgInSc2d1
druhu	druh	k1gInSc2
má	mít	k5eAaImIp3nS
žlutá	žlutý	k2eAgNnPc4d1
stehna	stehno	k1gNnPc4
a	a	k8xC
černé	černý	k2eAgInPc4d1
běháky	běhák	k1gInPc4
</s>
<s>
Pionites	Pionites	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
xanthurus	xanthurus	k1gMnSc1
(	(	kIx(
<g/>
amazónek	amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc1d1
západní	západní	k2eAgInSc1d1
<g/>
)	)	kIx)
–	–	k?
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
základního	základní	k2eAgInSc2d1
druhu	druh	k1gInSc2
má	mít	k5eAaImIp3nS
žlutý	žlutý	k2eAgInSc4d1
ocas	ocas	k1gInSc4
a	a	k8xC
růžové	růžový	k2eAgInPc4d1
běháky	běhák	k1gInPc4
</s>
<s>
Ohrožení	ohrožení	k1gNnSc1
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
nepřetržitému	přetržitý	k2eNgNnSc3d1
kácení	kácení	k1gNnSc3
tropických	tropický	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6
se	se	k3xPyFc4
papoušek	papoušek	k1gMnSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
počet	počet	k1gInSc1
značně	značně	k6eAd1
nízký	nízký	k2eAgInSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
ohroženým	ohrožený	k2eAgInSc7d1
druhem	druh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
pořádány	pořádán	k2eAgFnPc1d1
akce	akce	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
reintrodukce	reintrodukce	k1gFnSc1
papouška	papoušek	k1gMnSc2
do	do	k7c2
klidnějších	klidný	k2eAgFnPc2d2
oblastí	oblast	k1gFnPc2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Mladý	mladý	k2eAgInSc1d1
amazónek	amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc1d1
spolu	spolu	k6eAd1
s	s	k7c7
amazónkem	amazónek	k1gInSc7
černotemenným	černotemenný	k2eAgInSc7d1
a	a	k8xC
papoušky	papoušek	k1gMnPc7
senegalskými	senegalský	k2eAgMnPc7d1
v	v	k7c6
obchodu	obchod	k1gInSc2
se	s	k7c7
zvířaty	zvíře	k1gNnPc7
</s>
<s>
Mládě	mládě	k1gNnSc1
amazónka	amazónka	k1gFnSc1
bělobřichého	bělobřichý	k2eAgMnSc2d1
</s>
<s>
Dvojice	dvojice	k1gFnSc1
amazónků	amazónek	k1gInPc2
na	na	k7c6
bidle	bidlo	k1gNnSc6
</s>
<s>
Amazónek	Amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc1d1
</s>
<s>
Pár	pár	k4xCyI
amazónků	amazónek	k1gInPc2
bělobřichých	bělobřichý	k2eAgInPc2d1
</s>
<s>
Detailní	detailní	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
amazónka	amazónka	k1gFnSc1
bělobřichého	bělobřichý	k2eAgNnSc2d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Green-thighed	Green-thighed	k1gMnSc1
parrot	parrot	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Amazónek	Amazónek	k1gInSc1
bělobřichý	bělobřichý	k2eAgInSc1d1
(	(	kIx(
<g/>
Pionites	Pionites	k1gInSc1
leucogaster	leucogaster	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
ChovZvířat	ChovZvířat	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.chovzvirat.cz	www.chovzvirat.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
amazónek	amazónka	k1gFnPc2
bělobřichý	bělobřichý	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
box-sizing	box-sizing	k1gInSc1
<g/>
:	:	kIx,
<g/>
border-box	border-box	k1gInSc1
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
<g/>
;	;	kIx,
<g/>
width	widtha	k1gFnPc2
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
<g/>
clear	clear	k1gInSc1
<g/>
:	:	kIx,
<g/>
both	both	k1gInSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
em	em	k?
auto	auto	k1gNnSc1
0	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-inner	-innra	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
width	width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-group	-group	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-titlat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.25	0.25	k4
<g/>
em	em	k?
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
<g/>
text-align	text-aligno	k1gNnPc2
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc1
<g/>
{	{	kIx(
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gMnSc1
<g/>
{	{	kIx(
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-image	-imagat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gInSc1
<g/>
{	{	kIx(
<g/>
border-top	border-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g />
.	.	kIx.
</s>
<s hack="1">
solid	solid	k1gInSc1
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
th	th	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
f	f	k?
<g/>
0	#num#	k4
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-even	-evna	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
-odd	-odda	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
transparent	transparent	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.125	0.125	k4
<g/>
em	em	k?
0	#num#	k4
<g/>
}	}	kIx)
<g/>
Identifikátory	identifikátor	k1gInPc1
taxonu	taxon	k1gInSc2
</s>
<s>
Wikidata	Wikidata	k1gFnSc1
<g/>
:	:	kIx,
Q775636	Q775636	k1gFnSc1
</s>
<s>
Wikidruhy	Wikidruh	k1gInPc1
<g/>
:	:	kIx,
Pionites	Pionites	k1gMnSc1
leucogaster	leucogaster	k1gMnSc1
</s>
<s>
Avibase	Avibase	k6eAd1
<g/>
:	:	kIx,
7FEDAAABA994FD68	7FEDAAABA994FD68	k4
</s>
<s>
BirdLife	BirdLif	k1gMnSc5
<g/>
:	:	kIx,
62181308	#num#	k4
</s>
<s>
eBird	eBird	k1gInSc1
<g/>
:	:	kIx,
whbpar	whbpar	k1gInSc1
<g/>
1	#num#	k4
</s>
<s>
EoL	EoL	k?
<g/>
:	:	kIx,
1178028	#num#	k4
</s>
<s>
GBIF	GBIF	kA
<g/>
:	:	kIx,
2479441	#num#	k4
</s>
<s>
iNaturalist	iNaturalist	k1gInSc1
<g/>
:	:	kIx,
19254	#num#	k4
</s>
<s>
IRMNG	IRMNG	kA
<g/>
:	:	kIx,
11086738	#num#	k4
</s>
<s>
ITIS	ITIS	kA
<g/>
:	:	kIx,
177757	#num#	k4
</s>
<s>
IUCN	IUCN	kA
<g/>
:	:	kIx,
62181308	#num#	k4
</s>
<s>
NCBI	NCBI	kA
<g/>
:	:	kIx,
345186	#num#	k4
</s>
<s>
Neotropical	Neotropicat	k5eAaPmAgInS
<g/>
:	:	kIx,
whbpar	whbpar	k1gInSc1
<g/>
1	#num#	k4
</s>
<s>
Species	species	k1gFnSc1
<g/>
+	+	kIx~
<g/>
:	:	kIx,
7905	#num#	k4
</s>
<s>
TSA	TSA	kA
<g/>
:	:	kIx,
13910	#num#	k4
</s>
<s>
Xeno-canto	Xeno-canta	k1gFnSc5
<g/>
:	:	kIx,
Pionites-leucogaster	Pionites-leucogaster	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Ptáci	pták	k1gMnPc1
|	|	kIx~
Živočichové	živočich	k1gMnPc1
</s>
