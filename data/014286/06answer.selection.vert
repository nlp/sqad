<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
nepřetržitému	přetržitý	k2eNgNnSc3d1
kácení	kácení	k1gNnSc3
tropických	tropický	k2eAgInPc2d1
lesů	les	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6
se	se	k3xPyFc4
papoušek	papoušek	k1gMnSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gInSc1
počet	počet	k1gInSc1
značně	značně	k6eAd1
nízký	nízký	k2eAgInSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
ohroženým	ohrožený	k2eAgInSc7d1
druhem	druh	k1gInSc7
<g/>
.	.	kIx.
</s>