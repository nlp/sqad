<s>
Isabella	Isabella	k6eAd1	Isabella
Marie	Marie	k1gFnSc1	Marie
Parmská	parmský	k2eAgFnSc1d1	parmská
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Maria	Maria	k1gFnSc1	Maria
Elisabetta	Elisabett	k1gMnSc2	Elisabett
Luisa	Luisa	k1gFnSc1	Luisa
Antonietta	Antonietta	k1gMnSc1	Antonietta
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Giuseppina	Giuseppin	k2eAgNnSc2d1	Giuseppin
Saveria	Saverium	k1gNnSc2	Saverium
Dominica	Dominicus	k1gMnSc2	Dominicus
Giovanna	Giovann	k1gMnSc2	Giovann
Borbone	Borbon	k1gInSc5	Borbon
<g/>
,	,	kIx,	,
principessa	principessa	k1gFnSc1	principessa
di	di	k?	di
Parma	Parma	k1gFnSc1	Parma
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1741	[number]	k4	1741
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Filipa	Filip	k1gMnSc2	Filip
Parmského	parmský	k2eAgMnSc2d1	parmský
a	a	k8xC	a
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
