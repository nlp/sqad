<p>
<s>
Isabella	Isabella	k6eAd1	Isabella
Marie	Marie	k1gFnSc1	Marie
Parmská	parmský	k2eAgFnSc1d1	parmská
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Maria	Maria	k1gFnSc1	Maria
Elisabetta	Elisabett	k1gMnSc2	Elisabett
Luisa	Luisa	k1gFnSc1	Luisa
Antonietta	Antonietta	k1gMnSc1	Antonietta
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Giuseppina	Giuseppin	k2eAgNnSc2d1	Giuseppin
Saveria	Saverium	k1gNnSc2	Saverium
Dominica	Dominicus	k1gMnSc2	Dominicus
Giovanna	Giovann	k1gMnSc2	Giovann
Borbone	Borbon	k1gInSc5	Borbon
<g/>
,	,	kIx,	,
principessa	principessa	k1gFnSc1	principessa
di	di	k?	di
Parma	Parma	k1gFnSc1	Parma
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1741	[number]	k4	1741
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
Filipa	Filip	k1gMnSc2	Filip
Parmského	parmský	k2eAgMnSc2d1	parmský
a	a	k8xC	a
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dětství	dětství	k1gNnSc1	dětství
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Isabellin	Isabellin	k2eAgMnSc1d1	Isabellin
otec	otec	k1gMnSc1	otec
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
princ	princ	k1gMnSc1	princ
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
V.	V.	kA	V.
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Luisa	Luisa	k1gFnSc1	Luisa
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
dcerou	dcera	k1gFnSc7	dcera
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Isabela	Isabela	k1gFnSc1	Isabela
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gFnSc3	její
matce	matka	k1gFnSc3	matka
jen	jen	k6eAd1	jen
čtrnáct	čtrnáct	k4xCc4	čtrnáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
dva	dva	k4xCgMnPc1	dva
sourozenci	sourozenec	k1gMnPc1	sourozenec
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
až	až	k9	až
o	o	k7c4	o
hodně	hodně	k6eAd1	hodně
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
bratr	bratr	k1gMnSc1	bratr
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgMnSc1d1	budoucí
parmský	parmský	k2eAgMnSc1d1	parmský
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
Marie	Marie	k1gFnSc1	Marie
Luisa	Luisa	k1gFnSc1	Luisa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
manželkou	manželka	k1gFnSc7	manželka
španělského	španělský	k2eAgMnSc4d1	španělský
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
princezna	princezna	k1gFnSc1	princezna
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
svého	svůj	k3xOyFgMnSc2	svůj
děda	děd	k1gMnSc2	děd
<g/>
,	,	kIx,	,
španělského	španělský	k2eAgMnSc2d1	španělský
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
V.	V.	kA	V.
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
Filip	Filip	k1gMnSc1	Filip
stal	stát	k5eAaPmAgMnS	stát
parmským	parmský	k2eAgMnSc7d1	parmský
vévodou	vévoda	k1gMnSc7	vévoda
<g/>
,	,	kIx,	,
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
panství	panství	k1gNnSc1	panství
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Isabella	Isabella	k1gFnSc1	Isabella
se	se	k3xPyFc4	se
učila	učit	k5eAaImAgFnS	učit
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
četla	číst	k5eAaImAgFnS	číst
mnoho	mnoho	k4c4	mnoho
knih	kniha	k1gFnPc2	kniha
od	od	k7c2	od
filosofů	filosof	k1gMnPc2	filosof
a	a	k8xC	a
teologů	teolog	k1gMnPc2	teolog
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
Jacques-Bénigne	Jacques-Bénign	k1gInSc5	Jacques-Bénign
Bossuet	Bossuet	k1gMnSc1	Bossuet
nebo	nebo	k8xC	nebo
John	John	k1gMnSc1	John
Law	Law	k1gMnSc1	Law
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
propadala	propadat	k5eAaImAgFnS	propadat
melancholii	melancholie	k1gFnSc4	melancholie
a	a	k8xC	a
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1759	[number]	k4	1759
zemřela	zemřít	k5eAaPmAgFnS	zemřít
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
přepadaly	přepadat	k5eAaImAgFnP	přepadat
myšlenky	myšlenka	k1gFnPc1	myšlenka
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželka	manželka	k1gFnSc1	manželka
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Josefa	Josef	k1gMnSc2	Josef
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1760	[number]	k4	1760
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Isabella	Isabella	k1gFnSc1	Isabella
provdána	provdat	k5eAaPmNgFnS	provdat
za	za	k7c4	za
rakouského	rakouský	k2eAgMnSc4d1	rakouský
arcivévodu	arcivévoda	k1gMnSc4	arcivévoda
Josefa	Josef	k1gMnSc4	Josef
<g/>
,	,	kIx,	,
dědice	dědic	k1gMnPc4	dědic
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Parmská	parmský	k2eAgFnSc1d1	parmská
princezna	princezna	k1gFnSc1	princezna
rychle	rychle	k6eAd1	rychle
okouzlila	okouzlit	k5eAaPmAgFnS	okouzlit
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
dvůr	dvůr	k1gInSc1	dvůr
svým	svůj	k3xOyFgInSc7	svůj
půvabem	půvab	k1gInSc7	půvab
a	a	k8xC	a
inteligencí	inteligence	k1gFnSc7	inteligence
-	-	kIx~	-
dokonce	dokonce	k9	dokonce
prý	prý	k9	prý
řešila	řešit	k5eAaImAgFnS	řešit
složité	složitý	k2eAgInPc4d1	složitý
matematické	matematický	k2eAgInPc4d1	matematický
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Josef	Josef	k1gMnSc1	Josef
si	se	k3xPyFc3	se
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
hluboce	hluboko	k6eAd1	hluboko
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
city	city	k1gFnSc1	city
neopětovala	opětovat	k5eNaImAgFnS	opětovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Isabella	Isabella	k1gFnSc1	Isabella
a	a	k8xC	a
Josefova	Josefův	k2eAgFnSc1d1	Josefova
sestra	sestra	k1gFnSc1	sestra
Marie	Marie	k1gFnSc1	Marie
Kristýna	Kristýna	k1gFnSc1	Kristýna
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
přítelkyněmi	přítelkyně	k1gFnPc7	přítelkyně
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
něčím	něčí	k3xOyIgMnPc3	něčí
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
viděly	vidět	k5eAaImAgFnP	vidět
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
psaly	psát	k5eAaImAgFnP	psát
si	se	k3xPyFc3	se
dokonce	dokonce	k9	dokonce
dopisy	dopis	k1gInPc4	dopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
Isabella	Isabella	k1gFnSc1	Isabella
popisuje	popisovat	k5eAaImIp3nS	popisovat
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
Marii	Maria	k1gFnSc3	Maria
Kristýně	Kristýna	k1gFnSc3	Kristýna
<g/>
,	,	kIx,	,
o	o	k7c6	o
které	který	k3yIgFnSc6	který
Josef	Josef	k1gMnSc1	Josef
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Píši	psát	k5eAaImIp1nS	psát
ti	ty	k3xPp2nSc3	ty
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
krutá	krutý	k2eAgFnSc5d1	krutá
sestro	sestra	k1gFnSc5	sestra
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
zrovna	zrovna	k6eAd1	zrovna
rozloučily	rozloučit	k5eAaPmAgFnP	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Nemohu	moct	k5eNaImIp1nS	moct
snést	snést	k5eAaPmF	snést
to	ten	k3xDgNnSc4	ten
doufání	doufání	k1gNnSc4	doufání
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
mě	já	k3xPp1nSc4	já
uznáš	uznat	k5eAaPmIp2nS	uznat
jako	jako	k8xS	jako
osobu	osoba	k1gFnSc4	osoba
hodnou	hodný	k2eAgFnSc4d1	hodná
tvé	tvůj	k3xOp2gFnSc2	tvůj
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
mě	já	k3xPp1nSc4	já
raději	rád	k6eAd2	rád
srazíš	srazit	k5eAaPmIp2nS	srazit
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
<g/>
....	....	k?	....
Nedokáži	dokázat	k5eNaPmIp1nS	dokázat
myslet	myslet	k5eAaImF	myslet
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
hluboce	hluboko	k6eAd1	hluboko
zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
dopise	dopis	k1gInSc6	dopis
Isabella	Isabella	k1gMnSc1	Isabella
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
mi	já	k3xPp1nSc3	já
<g/>
,	,	kIx,	,
že	že	k8xS	že
den	den	k1gInSc1	den
začíná	začínat	k5eAaImIp3nS	začínat
Bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
ale	ale	k9	ale
začínám	začínat	k5eAaImIp1nS	začínat
den	den	k1gInSc4	den
myšlenkou	myšlenka	k1gFnSc7	myšlenka
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
miluji	milovat	k5eAaImIp1nS	milovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
myslím	myslet	k5eAaImIp1nS	myslet
neustále	neustále	k6eAd1	neustále
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1762	[number]	k4	1762
Isabella	Isabella	k1gFnSc1	Isabella
čekala	čekat	k5eAaImAgFnS	čekat
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
trpěla	trpět	k5eAaImAgFnS	trpět
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
jako	jako	k8xS	jako
správný	správný	k2eAgMnSc1d1	správný
manžel	manžel	k1gMnSc1	manžel
ji	on	k3xPp3gFnSc4	on
držel	držet	k5eAaImAgMnS	držet
za	za	k7c4	za
ruku	ruka	k1gFnSc4	ruka
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
a	a	k8xC	a
dočkal	dočkat	k5eAaPmAgMnS	dočkat
se	se	k3xPyFc4	se
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
-	-	kIx~	-
<g/>
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
šesti	šest	k4xCc6	šest
týdnech	týden	k1gInPc6	týden
byla	být	k5eAaImAgFnS	být
Isabella	Isabella	k1gFnSc1	Isabella
schopná	schopný	k2eAgFnSc1d1	schopná
opustit	opustit	k5eAaPmF	opustit
postel	postel	k1gFnSc4	postel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Její	její	k3xOp3gInPc1	její
dopisy	dopis	k1gInPc1	dopis
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
prozrazovaly	prozrazovat	k5eAaImAgInP	prozrazovat
fascinaci	fascinace	k1gFnSc4	fascinace
smrtí	smrt	k1gFnPc2	smrt
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
jsem	být	k5eAaImIp1nS	být
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
nemyslela	myslet	k5eNaImAgNnP	myslet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
teď	teď	k6eAd1	teď
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
ve	v	k7c6	v
mně	já	k3xPp1nSc6	já
probouzí	probouzet	k5eAaImIp3nS	probouzet
touhu	touha	k1gFnSc4	touha
brzy	brzy	k6eAd1	brzy
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
zná	znát	k5eAaImIp3nS	znát
mé	můj	k3xOp1gNnSc4	můj
přání	přání	k1gNnSc4	přání
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jej	on	k3xPp3gInSc4	on
musí	muset	k5eAaImIp3nS	muset
den	den	k1gInSc4	den
co	co	k9	co
den	den	k1gInSc4	den
urážet	urážet	k5eAaImF	urážet
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
bylo	být	k5eAaImAgNnS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
zabít	zabít	k5eAaPmF	zabít
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
už	už	k9	už
bych	by	kYmCp1nS	by
to	ten	k3xDgNnSc4	ten
udělala	udělat	k5eAaPmAgFnS	udělat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Později	pozdě	k6eAd2	pozdě
Marii	Maria	k1gFnSc4	Maria
Kristýně	Kristýna	k1gFnSc3	Kristýna
napsala	napsat	k5eAaPmAgFnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Smrt	smrt	k1gFnSc1	smrt
ke	k	k7c3	k
mně	já	k3xPp1nSc3	já
promlouvá	promlouvat	k5eAaImIp3nS	promlouvat
jasným	jasný	k2eAgInSc7d1	jasný
hlasem	hlas	k1gInSc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
jsem	být	k5eAaImIp1nS	být
ten	ten	k3xDgInSc4	ten
hlas	hlas	k1gInSc4	hlas
slyšela	slyšet	k5eAaImAgFnS	slyšet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1763	[number]	k4	1763
Isabella	Isabella	k1gFnSc1	Isabella
porodila	porodit	k5eAaPmAgFnS	porodit
druhou	druhý	k4xOgFnSc4	druhý
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
Marii	Maria	k1gFnSc3	Maria
Kristýnu	Kristýna	k1gFnSc4	Kristýna
<g/>
,	,	kIx,	,
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
podle	podle	k7c2	podle
její	její	k3xOp3gFnSc2	její
švagrové	švagrová	k1gFnSc2	švagrová
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
však	však	k9	však
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
<g/>
;	;	kIx,	;
Isabella	Isabella	k1gFnSc1	Isabella
sama	sám	k3xTgMnSc4	sám
je	být	k5eAaImIp3nS	být
brzy	brzy	k6eAd1	brzy
následovala	následovat	k5eAaImAgFnS	následovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
necelých	celý	k2eNgNnPc6d1	necelé
dvaadvaceti	dvaadvacet	k4xCc6	dvaadvacet
letech	léto	k1gNnPc6	léto
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
neštovice	neštovice	k1gFnPc4	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1770	[number]	k4	1770
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Isabella	Isabella	k6eAd1	Isabella
Parmská	parmský	k2eAgFnSc1d1	parmská
je	být	k5eAaImIp3nS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
v	v	k7c6	v
Císařské	císařský	k2eAgFnSc6d1	císařská
hrobce	hrobka	k1gFnSc6	hrobka
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
už	už	k9	už
Josef	Josef	k1gMnSc1	Josef
nebyl	být	k5eNaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
navázat	navázat	k5eAaPmF	navázat
žádný	žádný	k3yNgInSc4	žádný
užší	úzký	k2eAgInSc4d2	užší
citový	citový	k2eAgInSc4d1	citový
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
jiné	jiný	k2eAgFnSc3d1	jiná
ženě	žena	k1gFnSc3	žena
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
sňatku	sňatek	k1gInSc3	sňatek
ho	on	k3xPp3gMnSc4	on
vlastně	vlastně	k9	vlastně
donutila	donutit	k5eAaPmAgFnS	donutit
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josefovou	Josefův	k2eAgFnSc7d1	Josefova
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1765	[number]	k4	1765
stala	stát	k5eAaPmAgFnS	stát
sestřenice	sestřenice	k1gFnSc1	sestřenice
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
VII	VII	kA	VII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Amálie	Amálie	k1gFnSc2	Amálie
Habsburské	habsburský	k2eAgFnSc2d1	habsburská
-	-	kIx~	-
Marie	Maria	k1gFnSc2	Maria
Josefa	Josef	k1gMnSc4	Josef
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
stal	stát	k5eAaPmAgMnS	stát
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
když	když	k8xS	když
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
druhé	druhý	k4xOgNnSc1	druhý
manželství	manželství	k1gNnSc1	manželství
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
příliš	příliš	k6eAd1	příliš
štěstí	štěstí	k1gNnSc1	štěstí
nepřineslo	přinést	k5eNaPmAgNnS	přinést
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
ani	ani	k8xC	ani
nebylo	být	k5eNaImAgNnS	být
konzumováno	konzumovat	k5eAaBmNgNnS	konzumovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývod	vývod	k1gInSc1	vývod
z	z	k7c2	z
předků	předek	k1gInPc2	předek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Brána	brána	k1gFnSc1	brána
;	;	kIx,	;
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
408	[number]	k4	408
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85946	[number]	k4	85946
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
166	[number]	k4	166
<g/>
–	–	k?	–
<g/>
167	[number]	k4	167
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Isabela	Isabela	k1gFnSc1	Isabela
Parmská	parmský	k2eAgFnSc1d1	parmská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Biografie	biografie	k1gFnSc1	biografie
melancholické	melancholický	k2eAgFnSc2d1	melancholická
Isabelly	Isabella	k1gFnSc2	Isabella
Parmské	parmský	k2eAgFnSc2d1	parmská
(	(	kIx(	(
<g/>
1741	[number]	k4	1741
<g/>
-	-	kIx~	-
<g/>
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
</s>
</p>
