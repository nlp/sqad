<s>
Platýsi	platýs	k1gMnPc1	platýs
(	(	kIx(	(
<g/>
Pleuronectiformes	Pleuronectiformes	k1gMnSc1	Pleuronectiformes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řád	řád	k1gInSc4	řád
převážně	převážně	k6eAd1	převážně
mořských	mořský	k2eAgFnPc2d1	mořská
paprskoploutvých	paprskoploutvý	k2eAgFnPc2d1	paprskoploutvý
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
obývajících	obývající	k2eAgFnPc2d1	obývající
zejména	zejména	k9	zejména
mořské	mořský	k2eAgNnSc4d1	mořské
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gNnSc1	jeho
rozšíření	rozšíření	k1gNnSc1	rozšíření
je	být	k5eAaImIp3nS	být
až	až	k9	až
po	po	k7c4	po
nejhlubší	hluboký	k2eAgNnPc4d3	nejhlubší
místa	místo	k1gNnPc4	místo
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
Mariánský	mariánský	k2eAgInSc1d1	mariánský
příkop	příkop	k1gInSc1	příkop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
jinými	jiný	k2eAgFnPc7d1	jiná
rybami	ryba	k1gFnPc7	ryba
a	a	k8xC	a
plži	plž	k1gMnPc7	plž
<g/>
.	.	kIx.	.
</s>
<s>
Dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hmotností	hmotnost	k1gFnSc7	hmotnost
od	od	k7c2	od
několika	několik	k4yIc2	několik
gramů	gram	k1gInPc2	gram
do	do	k7c2	do
desítek	desítka	k1gFnPc2	desítka
kilogramů	kilogram	k1gInPc2	kilogram
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hospodářsky	hospodářsky	k6eAd1	hospodářsky
významnou	významný	k2eAgFnSc7d1	významná
kategorií	kategorie	k1gFnSc7	kategorie
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
v	v	k7c6	v
období	období	k1gNnSc6	období
dospělosti	dospělost	k1gFnSc2	dospělost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
plování	plování	k1gNnSc4	plování
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
diskovitého	diskovitý	k2eAgNnSc2d1	diskovité
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přechodu	přechod	k1gInSc2	přechod
od	od	k7c2	od
svislého	svislý	k2eAgNnSc2d1	svislé
plování	plování	k1gNnSc2	plování
k	k	k7c3	k
obrácenému	obrácený	k2eAgInSc3d1	obrácený
o	o	k7c4	o
90	[number]	k4	90
<g/>
°	°	k?	°
dochází	docházet	k5eAaImIp3nS	docházet
během	během	k7c2	během
změny	změna	k1gFnSc2	změna
z	z	k7c2	z
larválního	larvální	k2eAgNnSc2d1	larvální
stádia	stádium	k1gNnSc2	stádium
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
barevné	barevný	k2eAgFnSc3d1	barevná
změně	změna	k1gFnSc3	změna
<g/>
,	,	kIx,	,
mimikrám	mimikrat	k5eAaImIp1nS	mimikrat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
na	na	k7c6	na
mořském	mořský	k2eAgNnSc6d1	mořské
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Svrchní	svrchní	k2eAgFnSc1d1	svrchní
strana	strana	k1gFnSc1	strana
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
barvě	barva	k1gFnSc3	barva
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
při	při	k7c6	při
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
nebo	nebo	k8xC	nebo
číhání	číhání	k1gNnSc1	číhání
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
často	často	k6eAd1	často
zahrabávají	zahrabávat	k5eAaImIp3nP	zahrabávat
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
strany	strana	k1gFnSc2	strana
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
ke	k	k7c3	k
dnu	dno	k1gNnSc3	dno
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
slepé	slepý	k2eAgFnSc2d1	slepá
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
splynula	splynout	k5eAaPmAgFnS	splynout
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
zespoda	zespoda	k7c2	zespoda
<g/>
,	,	kIx,	,
když	když	k8xS	když
ryba	ryba	k1gFnSc1	ryba
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
výše	vysoce	k6eAd2	vysoce
(	(	kIx(	(
<g/>
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
dokáží	dokázat	k5eAaPmIp3nP	dokázat
měnit	měnit	k5eAaImF	měnit
barvu	barva	k1gFnSc4	barva
i	i	k9	i
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dospívání	dospívání	k1gNnSc2	dospívání
dochází	docházet	k5eAaImIp3nS	docházet
rovněž	rovněž	k9	rovněž
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
přemístění	přemístění	k1gNnSc3	přemístění
jednoho	jeden	k4xCgNnSc2	jeden
oka	oko	k1gNnSc2	oko
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bude	být	k5eAaImBp3nS	být
později	pozdě	k6eAd2	pozdě
přisedlá	přisedlý	k2eAgFnSc1d1	přisedlá
ke	k	k7c3	k
dnu	den	k1gInSc3	den
k	k	k7c3	k
oku	oko	k1gNnSc3	oko
na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Přisedání	přisedání	k1gNnSc1	přisedání
levou	levý	k2eAgFnSc7d1	levá
nebo	nebo	k8xC	nebo
pravou	pravý	k2eAgFnSc7d1	pravá
stranou	strana	k1gFnSc7	strana
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
příslušností	příslušnost	k1gFnSc7	příslušnost
k	k	k7c3	k
čeledi	čeleď	k1gFnSc3	čeleď
<g/>
.	.	kIx.	.
řád	řád	k1gInSc4	řád
platýsi	platýs	k1gMnPc1	platýs
(	(	kIx(	(
<g/>
Pleuronectiformes	Pleuronectiformes	k1gInSc1	Pleuronectiformes
<g/>
)	)	kIx)	)
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
:	:	kIx,	:
kambalovcovití	kambalovcovitý	k2eAgMnPc1d1	kambalovcovitý
(	(	kIx(	(
<g/>
Psettodidae	Psettodidae	k1gNnSc7	Psettodidae
<g/>
)	)	kIx)	)
platušicovití	platušicovitý	k2eAgMnPc1d1	platušicovitý
(	(	kIx(	(
<g/>
Citharidae	Citharidae	k1gNnSc7	Citharidae
<g/>
)	)	kIx)	)
kambalovití	kambalovitý	k2eAgMnPc1d1	kambalovitý
(	(	kIx(	(
<g/>
Bothidae	Bothidae	k1gNnSc7	Bothidae
<g/>
)	)	kIx)	)
rod	rod	k1gInSc1	rod
<g/>
:	:	kIx,	:
kambala	kambala	k1gFnSc1	kambala
(	(	kIx(	(
<g/>
Bothus	Bothus	k1gInSc1	Bothus
<g/>
)	)	kIx)	)
kambalkovití	kambalkovitý	k2eAgMnPc1d1	kambalkovitý
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Achiropsettidae	Achiropsettida	k1gFnPc4	Achiropsettida
<g/>
)	)	kIx)	)
pakambalovití	pakambalovitý	k2eAgMnPc1d1	pakambalovitý
(	(	kIx(	(
<g/>
Scophthalmidae	Scophthalmidae	k1gNnSc3	Scophthalmidae
<g/>
)	)	kIx)	)
pakambala	pakambat	k5eAaImAgFnS	pakambat
velká	velký	k2eAgFnSc1d1	velká
(	(	kIx(	(
<g/>
Psetta	Psetta	k1gFnSc1	Psetta
maxima	maxima	k1gFnSc1	maxima
<g/>
)	)	kIx)	)
platýsovcovití	platýsovcovitý	k2eAgMnPc1d1	platýsovcovitý
(	(	kIx(	(
<g/>
Paralichthyidae	Paralichthyidae	k1gNnSc7	Paralichthyidae
<g/>
)	)	kIx)	)
platýsovití	platýsovitý	k2eAgMnPc1d1	platýsovitý
(	(	kIx(	(
<g/>
Pleuronectidae	Pleuronectidae	k1gNnSc7	Pleuronectidae
<g/>
)	)	kIx)	)
platýs	platýs	k1gMnSc1	platýs
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Pleuronectes	Pleuronectes	k1gMnSc1	Pleuronectes
platessa	platess	k1gMnSc2	platess
<g/>
)	)	kIx)	)
platýs	platýs	k1gMnSc1	platýs
bradavičnatý	bradavičnatý	k2eAgMnSc1d1	bradavičnatý
(	(	kIx(	(
<g/>
Platichthys	Platichthys	k1gInSc1	Platichthys
flesus	flesus	k1gInSc1	flesus
<g/>
)	)	kIx)	)
–	–	k?	–
flundra	flundra	k1gFnSc1	flundra
obecná	obecná	k1gFnSc1	obecná
<g />
.	.	kIx.	.
</s>
<s>
platýs	platýs	k1gMnSc1	platýs
limanda	limando	k1gNnSc2	limando
(	(	kIx(	(
<g/>
Limanda	Limanda	k1gFnSc1	Limanda
limanda	limanda	k1gFnSc1	limanda
<g/>
)	)	kIx)	)
–	–	k?	–
falešný	falešný	k2eAgInSc4d1	falešný
mořský	mořský	k2eAgInSc4d1	mořský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
citrónový	citrónový	k2eAgMnSc1d1	citrónový
jazyk	jazyk	k1gMnSc1	jazyk
platýs	platýs	k1gMnSc1	platýs
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Hippoglossus	Hippoglossus	k1gMnSc1	Hippoglossus
hippoglossus	hippoglossus	k1gMnSc1	hippoglossus
<g/>
)	)	kIx)	)
–	–	k?	–
halibut	halibut	k1gMnSc1	halibut
samaridovití	samaridovitý	k2eAgMnPc1d1	samaridovitý
(	(	kIx(	(
<g/>
Samaridae	Samaridae	k1gNnSc7	Samaridae
<g/>
)	)	kIx)	)
jazykovkovití	jazykovkovitý	k2eAgMnPc1d1	jazykovkovitý
(	(	kIx(	(
<g/>
Achiridae	Achiridae	k1gNnSc7	Achiridae
<g/>
)	)	kIx)	)
jazykovití	jazykovitý	k2eAgMnPc1d1	jazykovitý
(	(	kIx(	(
<g/>
Soleidae	Soleidae	k1gNnSc7	Soleidae
<g/>
)	)	kIx)	)
jazyk	jazyk	k1gInSc1	jazyk
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Solea	Sole	k2eAgFnSc1d1	Sole
solea	solea	k1gFnSc1	solea
<g/>
)	)	kIx)	)
–	–	k?	–
mořský	mořský	k2eAgInSc4d1	mořský
jazyk	jazyk	k1gInSc4	jazyk
jazykovcovití	jazykovcovitý	k2eAgMnPc1d1	jazykovcovitý
(	(	kIx(	(
<g/>
Cynoglossidae	Cynoglossidae	k1gNnSc7	Cynoglossidae
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Platýsi	platýs	k1gMnPc1	platýs
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
http://www.biolib.cz/cz/taxon/id16138/	[url]	k4	http://www.biolib.cz/cz/taxon/id16138/
</s>
