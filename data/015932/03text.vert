<s>
Třída	třída	k1gFnSc1
Izjaslav	Izjaslava	k1gFnPc2
</s>
<s>
Třída	třída	k1gFnSc1
Izjaslav	Izjaslav	k1gMnSc1
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
(	(	kIx(
<g/>
ex	ex	k6eAd1
Izjaslav	Izjaslava	k1gFnPc2
<g/>
)	)	kIx)
<g/>
Obecné	obecný	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
carské	carský	k2eAgNnSc1d1
námořnictvoSovětské	námořnictvoSovětský	k2eAgNnSc1d1
námořnictvoEstonské	námořnictvoEstonský	k2eAgNnSc1d1
námořnictvoPeruánské	námořnictvoPeruánský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
Typ	typ	k1gInSc1
</s>
<s>
torpédoborec	torpédoborec	k1gInSc1
Lodě	loď	k1gFnSc2
</s>
<s>
5	#num#	k4
Osud	osud	k1gInSc1
</s>
<s>
2	#num#	k4
potopeny	potopen	k2eAgFnPc4d1
<g/>
1	#num#	k4
vyřazena	vyřazen	k2eAgFnSc1d1
<g/>
2	#num#	k4
nedostavěny	dostavěn	k2eNgFnPc1d1
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
třída	třída	k1gFnSc1
Gavril	Gavril	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
třída	třída	k1gFnSc1
Kerč	Kerč	k1gInSc1
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
třídy	třída	k1gFnSc2
Izjaslav	Izjaslav	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Výtlak	výtlak	k1gInSc1
</s>
<s>
1350	#num#	k4
t	t	k?
(	(	kIx(
<g/>
standardní	standardní	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
1570	#num#	k4
t	t	k?
(	(	kIx(
<g/>
plný	plný	k2eAgInSc4d1
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
107	#num#	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
9,5	9,5	k4
m	m	kA
Ponor	ponor	k1gInSc1
</s>
<s>
4,1	4,1	k4
m	m	kA
Pohon	pohon	k1gInSc1
</s>
<s>
2	#num#	k4
turbíny	turbína	k1gFnPc4
<g/>
,	,	kIx,
5	#num#	k4
kotlů	kotel	k1gInPc2
Rychlost	rychlost	k1gFnSc1
</s>
<s>
35	#num#	k4
uzlů	uzel	k1gInPc2
Dosah	dosah	k1gInSc1
</s>
<s>
1568	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
při	při	k7c6
16	#num#	k4
uzlech	uzel	k1gInPc6
Posádka	posádka	k1gFnSc1
</s>
<s>
150	#num#	k4
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
102	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
1	#num#	k4
<g/>
×	×	k?
76	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
<g/>
9	#num#	k4
<g/>
×	×	k?
457	#num#	k4
<g/>
mm	mm	kA
torpédomet	torpédomet	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
80	#num#	k4
min	mina	k1gFnPc2
</s>
<s>
Třída	třída	k1gFnSc1
Izjaslav	Izjaslav	k1gMnSc1
byla	být	k5eAaImAgFnS
třída	třída	k1gFnSc1
torpédoborců	torpédoborec	k1gInPc2
ruského	ruský	k2eAgNnSc2d1
carského	carský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
z	z	k7c2
doby	doba	k1gFnSc2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
byly	být	k5eAaImAgFnP
postaveny	postaven	k2eAgInPc1d1
tři	tři	k4xCgFnPc1
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
další	další	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
nedokončeny	dokončit	k5eNaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nasazeny	nasazen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
v	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
boji	boj	k1gInSc6
potopeny	potopit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc4
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
získalo	získat	k5eAaPmAgNnS
Estonsko	Estonsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
jej	on	k3xPp3gMnSc4
později	pozdě	k6eAd2
prodalo	prodat	k5eAaPmAgNnS
Peru	Peru	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Celkem	celkem	k6eAd1
byly	být	k5eAaImAgFnP
postaveny	postaven	k2eAgInPc1d1
tři	tři	k4xCgFnPc1
jednotky	jednotka	k1gFnPc1
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
dalších	další	k2eAgMnPc2d1
dvou	dva	k4xCgMnPc6
byla	být	k5eAaImAgFnS
přerušena	přerušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotky	jednotka	k1gFnPc1
třídy	třída	k1gFnSc2
Izjaslav	Izjaslava	k1gFnPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
JménoZaložený	JménoZaložený	k2eAgInSc1d1
kýluSpuštěnaVstup	kýluSpuštěnaVstup	k1gInSc1
do	do	k7c2
službyStatus	službyStatus	k1gMnSc1
</s>
<s>
Izjaslav	Izjaslav	k1gMnSc1
(	(	kIx(
<g/>
ex	ex	k6eAd1
Gromonosec	Gromonosec	k1gInSc1
<g/>
)	)	kIx)
<g/>
191319141917	#num#	k4
<g/>
Od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
přejmenován	přejmenovat	k5eAaPmNgMnS
na	na	k7c4
Karl	Karl	k1gInSc4
Marx	Marx	k1gMnSc1
<g/>
,	,	kIx,
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1941	#num#	k4
potopen	potopen	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Avtroil	Avtroil	k1gInSc4
<g/>
1913191519171918	#num#	k4
zajat	zajat	k2eAgInSc4d1
Brity	Brit	k1gMnPc4
<g/>
,	,	kIx,
předán	předán	k2eAgInSc1d1
Estonsku	Estonsko	k1gNnSc3
<g/>
,	,	kIx,
sloužil	sloužit	k5eAaImAgMnS
jako	jako	k9
Lennuk	Lennuk	k1gMnSc1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1933	#num#	k4
prodán	prodat	k5eAaPmNgInS
Peru	Peru	k1gNnSc7
<g/>
,	,	kIx,
sloužil	sloužit	k5eAaImAgInS
jako	jako	k9
Almirante	Almirant	k1gMnSc5
Guise	Guis	k1gMnSc5
<g/>
,	,	kIx,
vyřazen	vyřazen	k2eAgMnSc1d1
1947	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kalinin	Kalinin	k2eAgMnSc1d1
(	(	kIx(
<g/>
ex	ex	k6eAd1
Prjamislav	Prjamislava	k1gFnPc2
<g/>
)	)	kIx)
<g/>
191319151927	#num#	k4
<g/>
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1941	#num#	k4
potopen	potopen	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brjačislav	Brjačislava	k1gFnPc2
<g/>
19131915	#num#	k4
<g/>
–	–	k?
<g/>
Nedokončen	dokončen	k2eNgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fedor	Fedora	k1gFnPc2
Stratilat	Stratile	k1gNnPc2
<g/>
19141917	#num#	k4
<g/>
–	–	k?
<g/>
Nedokončen	dokončen	k2eNgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Peruánský	peruánský	k2eAgInSc1d1
torpédoborec	torpédoborec	k1gInSc1
Almirante	Almirant	k1gMnSc5
Guise	Guise	k1gFnPc4
(	(	kIx(
<g/>
ex	ex	k6eAd1
Avtroil	Avtroil	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Torpédoborce	torpédoborec	k1gInPc1
byly	být	k5eAaImAgInP
vyzbrojeny	vyzbrojit	k5eAaPmNgInP
pěti	pět	k4xCc3
102	#num#	k4
<g/>
mm	mm	kA
kanóny	kanón	k1gInPc1
<g/>
,	,	kIx,
jedním	jeden	k4xCgInSc7
76	#num#	k4
<g/>
mm	mm	kA
kanónem	kanón	k1gInSc7
<g/>
,	,	kIx,
třemi	tři	k4xCgNnPc7
trojhlavňovými	trojhlavňový	k2eAgFnPc7d1
457	#num#	k4
<g/>
mm	mm	kA
torpédomety	torpédomet	k1gInPc4
a	a	k8xC
až	až	k9
80	#num#	k4
minami	mina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc1
měl	mít	k5eAaImAgInS
výkon	výkon	k1gInSc4
32	#num#	k4
700	#num#	k4
hp	hp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvořily	tvořit	k5eAaImAgInP
jej	on	k3xPp3gMnSc4
dvě	dva	k4xCgFnPc4
parní	parní	k2eAgFnPc4d1
turbíny	turbína	k1gFnPc4
Brown-Boveri	Brown-Bover	k1gFnSc2
a	a	k8xC
pět	pět	k4xCc4
kotlů	kotel	k1gInPc2
Normand	Normand	k?
<g/>
,	,	kIx,
pohánějících	pohánějící	k2eAgInPc2d1
dva	dva	k4xCgInPc4
lodní	lodní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
rychlost	rychlost	k1gFnSc1
dosahovala	dosahovat	k5eAaImAgFnS
35	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosah	dosah	k1gInSc1
byl	být	k5eAaImAgInS
1568	#num#	k4
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
16	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
IZYASLAV	IZYASLAV	kA
destroyers	destroyersa	k1gFnPc2
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
-	-	kIx~
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navypedia	Navypedium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
HRBEK	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
1917	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
232	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85983	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
176	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Hrbek	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
188	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Třída	třída	k1gFnSc1
Izjaslav	Izjaslava	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
</s>
