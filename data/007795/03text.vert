<s>
Kinoautomat	Kinoautomat	k1gInSc1	Kinoautomat
byl	být	k5eAaImAgInS	být
revoluční	revoluční	k2eAgInSc1d1	revoluční
filmový	filmový	k2eAgInSc1d1	filmový
projekt	projekt	k1gInSc1	projekt
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
českou	český	k2eAgFnSc7d1	Česká
novou	nový	k2eAgFnSc7d1	nová
vlnou	vlna	k1gFnSc7	vlna
představený	představený	k1gMnSc1	představený
světové	světový	k2eAgFnSc3d1	světová
výstavě	výstava	k1gFnSc3	výstava
EXPO	Expo	k1gNnSc4	Expo
<g/>
'	'	kIx"	'
<g/>
67	[number]	k4	67
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
autora	autor	k1gMnSc2	autor
Radúze	Radúz	k1gMnSc2	Radúz
Činčery	Činčera	k1gFnSc2	Činčera
<g/>
,	,	kIx,	,
scenáristy	scenárista	k1gMnSc2	scenárista
Pavla	Pavel	k1gMnSc2	Pavel
Juráčka	Juráček	k1gMnSc2	Juráček
<g/>
,	,	kIx,	,
režisérů	režisér	k1gMnPc2	režisér
Jána	Ján	k1gMnSc2	Ján
Roháče	roháč	k1gMnSc2	roháč
<g/>
,	,	kIx,	,
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Svitáčka	Svitáček	k1gMnSc2	Svitáček
a	a	k8xC	a
technického	technický	k2eAgNnSc2d1	technické
řešení	řešení	k1gNnSc2	řešení
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Friče	Frič	k1gMnSc2	Frič
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
základem	základ	k1gInSc7	základ
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgInSc1	první
interaktivní	interaktivní	k2eAgInSc1d1	interaktivní
film	film	k1gInSc1	film
na	na	k7c6	na
světě	svět	k1gInSc6	svět
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
nadabovaný	nadabovaný	k2eAgMnSc1d1	nadabovaný
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sledoval	sledovat	k5eAaImAgInS	sledovat
osudy	osud	k1gInPc4	osud
poněkud	poněkud	k6eAd1	poněkud
nešikovného	šikovný	k2eNgMnSc2d1	nešikovný
pana	pan	k1gMnSc2	pan
Nováka	Novák	k1gMnSc2	Novák
(	(	kIx(	(
<g/>
ztvárněného	ztvárněný	k2eAgMnSc2d1	ztvárněný
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Horníčkem	Horníček	k1gMnSc7	Horníček
<g/>
)	)	kIx)	)
a	a	k8xC	a
který	který	k3yQgInSc4	který
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
klíčových	klíčový	k2eAgInPc6d1	klíčový
okamžicích	okamžik	k1gInPc6	okamžik
diváci	divák	k1gMnPc1	divák
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
a	a	k8xC	a
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
okamžicích	okamžik	k1gInPc6	okamžik
vystupoval	vystupovat	k5eAaImAgInS	vystupovat
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
právě	právě	k9	právě
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
divákům	divák	k1gMnPc3	divák
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
humornou	humorný	k2eAgFnSc7d1	humorná
formou	forma	k1gFnSc7	forma
důležitost	důležitost	k1gFnSc4	důležitost
každého	každý	k3xTgNnSc2	každý
specifického	specifický	k2eAgNnSc2d1	specifické
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
neuměl	umět	k5eNaImAgMnS	umět
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
komentáře	komentář	k1gInPc4	komentář
měl	mít	k5eAaImAgMnS	mít
naučené	naučený	k2eAgNnSc4d1	naučené
jen	jen	k9	jen
foneticky	foneticky	k6eAd1	foneticky
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
příběhu	příběh	k1gInSc2	příběh
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
pouze	pouze	k6eAd1	pouze
klamná	klamný	k2eAgFnSc1d1	klamná
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
příběhové	příběhový	k2eAgFnPc1d1	příběhová
varianty	varianta	k1gFnPc1	varianta
měly	mít	k5eAaImAgFnP	mít
vždy	vždy	k6eAd1	vždy
stejné	stejný	k2eAgInPc1d1	stejný
důsledky	důsledek	k1gInPc1	důsledek
a	a	k8xC	a
kauzalita	kauzalita	k1gFnSc1	kauzalita
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
tak	tak	k9	tak
setkala	setkat	k5eAaPmAgFnS	setkat
opět	opět	k6eAd1	opět
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
díky	díky	k7c3	díky
geniálnímu	geniální	k2eAgInSc3d1	geniální
nápadu	nápad	k1gInSc3	nápad
a	a	k8xC	a
skvělé	skvělý	k2eAgFnSc3d1	skvělá
zábavě	zábava	k1gFnSc3	zábava
hit	hit	k1gInSc4	hit
celé	celá	k1gFnSc2	celá
výstavy	výstava	k1gFnSc2	výstava
<g/>
,	,	kIx,	,
stály	stát	k5eAaImAgFnP	stát
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
mnohahodinové	mnohahodinový	k2eAgFnPc1d1	mnohahodinová
fronty	fronta	k1gFnPc1	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Projevili	projevit	k5eAaPmAgMnP	projevit
o	o	k7c4	o
něj	on	k3xPp3gInSc4	on
zájem	zájem	k1gInSc4	zájem
i	i	k9	i
Paramount	Paramount	k1gMnSc1	Paramount
Pictures	Pictures	k1gMnSc1	Pictures
a	a	k8xC	a
Universal	Universal	k1gMnSc1	Universal
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
autor	autor	k1gMnSc1	autor
Radúz	Radúz	k1gMnSc1	Radúz
Činčera	Činčero	k1gNnSc2	Činčero
nesměl	smět	k5eNaImAgMnS	smět
výtvor	výtvor	k1gInSc4	výtvor
Československého	československý	k2eAgInSc2d1	československý
státního	státní	k2eAgInSc2d1	státní
filmu	film	k1gInSc2	film
nabízet	nabízet	k5eAaImF	nabízet
osobně	osobně	k6eAd1	osobně
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc4d1	nízká
cenu	cena	k1gFnSc4	cena
<g/>
)	)	kIx)	)
postoupen	postoupen	k2eAgInSc1d1	postoupen
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
produkci	produkce	k1gFnSc3	produkce
s	s	k7c7	s
povinností	povinnost	k1gFnSc7	povinnost
jej	on	k3xPp3gMnSc4	on
uvádět	uvádět	k5eAaImF	uvádět
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
Hemisfér	hemisféra	k1gFnPc2	hemisféra
a	a	k8xC	a
až	až	k9	až
po	po	k7c6	po
soudním	soudní	k2eAgInSc6d1	soudní
procesu	proces	k1gInSc6	proces
v	v	k7c6	v
USA	USA	kA	USA
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
opět	opět	k6eAd1	opět
promítán	promítat	k5eAaImNgInS	promítat
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
EXPO	Expo	k1gNnSc1	Expo
́	́	k?	́
<g/>
74	[number]	k4	74
ve	v	k7c6	v
Spokane	Spokan	k1gMnSc5	Spokan
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
Washington	Washington	k1gInSc1	Washington
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
už	už	k9	už
takovým	takový	k3xDgNnSc7	takový
jako	jako	k9	jako
o	o	k7c4	o
7	[number]	k4	7
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
si	se	k3xPyFc3	se
uvedení	uvedení	k1gNnSc1	uvedení
projektu	projekt	k1gInSc2	projekt
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
nákladnou	nákladný	k2eAgFnSc4d1	nákladná
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
pražského	pražský	k2eAgNnSc2d1	Pražské
kina	kino	k1gNnSc2	kino
Světozor	světozor	k1gInSc1	světozor
<g/>
:	:	kIx,	:
byla	být	k5eAaImAgFnS	být
nainstalována	nainstalován	k2eAgFnSc1d1	nainstalována
speciální	speciální	k2eAgFnSc1d1	speciální
technická	technický	k2eAgFnSc1d1	technická
kabina	kabina	k1gFnSc1	kabina
<g/>
,	,	kIx,	,
šatna	šatna	k1gFnSc1	šatna
za	za	k7c7	za
plátnem	plátno	k1gNnSc7	plátno
a	a	k8xC	a
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
(	(	kIx(	(
<g/>
několika	několik	k4yIc7	několik
set	sto	k4xCgNnPc2	sto
<g/>
)	)	kIx)	)
sedadel	sedadlo	k1gNnPc2	sedadlo
zabudováno	zabudován	k2eAgNnSc1d1	zabudováno
hlasovací	hlasovací	k2eAgNnSc1d1	hlasovací
zařízení	zařízení	k1gNnSc1	zařízení
se	s	k7c7	s
zeleným	zelené	k1gNnSc7	zelené
a	a	k8xC	a
červeným	červený	k2eAgNnSc7d1	červené
tlačítkem	tlačítko	k1gNnSc7	tlačítko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
diváků	divák	k1gMnPc2	divák
mohl	moct	k5eAaImAgMnS	moct
hlasovat	hlasovat	k5eAaImF	hlasovat
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Miroslava	Miroslav	k1gMnSc2	Miroslav
Horníčka	Horníček	k1gMnSc2	Horníček
a	a	k8xC	a
Zuzany	Zuzana	k1gFnSc2	Zuzana
Neubauerové	Neubauerová	k1gFnSc2	Neubauerová
<g/>
.	.	kIx.	.
</s>
<s>
Hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
často	často	k6eAd1	často
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
<g/>
×	×	k?	×
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Horníčka	Horníček	k1gMnSc2	Horníček
ovšem	ovšem	k9	ovšem
i	i	k9	i
s	s	k7c7	s
alternacemi	alternace	k1gFnPc7	alternace
(	(	kIx(	(
<g/>
Eduard	Eduard	k1gMnSc1	Eduard
Hrubeš	Hrubeš	k1gMnSc1	Hrubeš
<g/>
,	,	kIx,	,
Saskia	Saskia	k1gFnSc1	Saskia
Burešová	Burešová	k1gFnSc1	Burešová
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Čapek	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Panýrková	Panýrková	k1gFnSc1	Panýrková
a	a	k8xC	a
Regina	Regina	k1gFnSc1	Regina
Rázlová	Rázlová	k1gFnSc1	Rázlová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
však	však	k9	však
sláva	sláva	k1gFnSc1	sláva
upadala	upadat	k5eAaPmAgFnS	upadat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
už	už	k9	už
byla	být	k5eAaImAgFnS	být
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
10	[number]	k4	10
–	–	k?	–
15	[number]	k4	15
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
představení	představení	k1gNnSc4	představení
<g/>
,	,	kIx,	,
technické	technický	k2eAgNnSc1d1	technické
zařízení	zařízení	k1gNnSc1	zařízení
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
poruchovější	poruchový	k2eAgInSc4d2	poruchovější
a	a	k8xC	a
úřední	úřední	k2eAgInSc4d1	úřední
zákaz	zákaz	k1gInSc4	zákaz
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
z	z	k7c2	z
ideových	ideový	k2eAgInPc2d1	ideový
důvodů	důvod	k1gInPc2	důvod
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
tedy	tedy	k9	tedy
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
"	"	kIx"	"
<g/>
ranou	rána	k1gFnSc7	rána
z	z	k7c2	z
milosti	milost	k1gFnSc2	milost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
film	film	k1gInSc4	film
dvakrát	dvakrát	k6eAd1	dvakrát
uvedla	uvést	k5eAaPmAgFnS	uvést
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Vysílala	vysílat	k5eAaImAgFnS	vysílat
jej	on	k3xPp3gInSc4	on
současně	současně	k6eAd1	současně
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
programech	program	k1gInPc6	program
ČT1	ČT1	k1gMnPc2	ČT1
a	a	k8xC	a
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kanálu	kanál	k1gInSc6	kanál
běžela	běžet	k5eAaImAgFnS	běžet
příslušná	příslušný	k2eAgFnSc1d1	příslušná
paralelní	paralelní	k2eAgFnSc1d1	paralelní
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
diváci	divák	k1gMnPc1	divák
si	se	k3xPyFc3	se
vývoj	vývoj	k1gInSc4	vývoj
filmu	film	k1gInSc2	film
vybírali	vybírat	k5eAaImAgMnP	vybírat
přepínáním	přepínání	k1gNnSc7	přepínání
programů	program	k1gInPc2	program
ve	v	k7c4	v
stanovenou	stanovený	k2eAgFnSc4d1	stanovená
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
Českým	český	k2eAgInSc7d1	český
centrem	centr	k1gInSc7	centr
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
uvedena	uvést	k5eAaPmNgFnS	uvést
obnovená	obnovený	k2eAgFnSc1d1	obnovená
premiéra	premiéra	k1gFnSc1	premiéra
Kinoautomatu	Kinoautomat	k1gInSc2	Kinoautomat
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
bylo	být	k5eAaImAgNnS	být
ohlášeno	ohlášen	k2eAgNnSc4d1	ohlášeno
připravované	připravovaný	k2eAgNnSc4d1	připravované
vydání	vydání	k1gNnSc4	vydání
nového	nový	k2eAgMnSc2d1	nový
DVD	DVD	kA	DVD
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
filmem	film	k1gInSc7	film
a	a	k8xC	a
doprovodnou	doprovodný	k2eAgFnSc7d1	doprovodná
knihou	kniha	k1gFnSc7	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
uvedla	uvést	k5eAaPmAgFnS	uvést
Alena	Alena	k1gFnSc1	Alena
Činčerová	Činčerová	k1gFnSc1	Činčerová
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
vynálezce	vynálezce	k1gMnSc1	vynálezce
Kinoautomatu	Kinoautomat	k1gInSc2	Kinoautomat
<g/>
,	,	kIx,	,
obnovenou	obnovený	k2eAgFnSc4d1	obnovená
premiéru	premiéra	k1gFnSc4	premiéra
Kinoautomatu	Kinoautoma	k1gNnSc2	Kinoautoma
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
Světozor	světozor	k1gInSc1	světozor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zrekonstruovaném	zrekonstruovaný	k2eAgInSc6d1	zrekonstruovaný
biografu	biograf	k1gInSc6	biograf
s	s	k7c7	s
digitální	digitální	k2eAgFnSc7d1	digitální
projekcí	projekce	k1gFnSc7	projekce
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
dálkovými	dálkový	k2eAgInPc7d1	dálkový
ovladači	ovladač	k1gInPc7	ovladač
a	a	k8xC	a
moderátory	moderátor	k1gMnPc7	moderátor
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
střídačku	střídačka	k1gFnSc4	střídačka
Tomáš	Tomáš	k1gMnSc1	Tomáš
Matonoha	Matonoha	k?	Matonoha
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Hrubeš	Hrubeš	k1gMnSc1	Hrubeš
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Polášek	Polášek	k1gMnSc1	Polášek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
výstupem	výstup	k1gInSc7	výstup
moderátora	moderátor	k1gMnSc2	moderátor
v	v	k7c6	v
klíčových	klíčový	k2eAgInPc6d1	klíčový
okamžicích	okamžik	k1gInPc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
divák	divák	k1gMnSc1	divák
možnost	možnost	k1gFnSc4	možnost
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Moderátorem	moderátor	k1gInSc7	moderátor
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
DVD	DVD	kA	DVD
verzi	verze	k1gFnSc4	verze
je	být	k5eAaImIp3nS	být
Eduard	Eduard	k1gMnSc1	Eduard
Hrubeš	Hrubeš	k1gMnSc1	Hrubeš
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rozpaky	rozpak	k1gInPc4	rozpak
kuchaře	kuchař	k1gMnSc2	kuchař
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
Československá	československý	k2eAgFnSc1d1	Československá
televize	televize	k1gFnSc1	televize
na	na	k7c6	na
principu	princip	k1gInSc6	princip
kinoautomatu	kinoautomat	k1gInSc2	kinoautomat
založený	založený	k2eAgInSc1d1	založený
třináctidílný	třináctidílný	k2eAgInSc1d1	třináctidílný
seriál	seriál	k1gInSc1	seriál
Rozpaky	rozpak	k1gInPc1	rozpak
kuchaře	kuchař	k1gMnSc2	kuchař
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
hotelové	hotelový	k2eAgFnSc2d1	hotelová
kuchyně	kuchyně	k1gFnSc2	kuchyně
(	(	kIx(	(
<g/>
scénář	scénář	k1gInSc1	scénář
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dietl	Dietl	k1gMnSc1	Dietl
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
František	František	k1gMnSc1	František
Filip	Filip	k1gMnSc1	Filip
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
několikrát	několikrát	k6eAd1	několikrát
přerušen	přerušit	k5eAaPmNgInS	přerušit
a	a	k8xC	a
asi	asi	k9	asi
stovka	stovka	k1gFnSc1	stovka
diváků	divák	k1gMnPc2	divák
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
pomocí	pomocí	k7c2	pomocí
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
přístrojů	přístroj	k1gInPc2	přístroj
volila	volit	k5eAaImAgNnP	volit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
Josef	Josef	k1gMnSc1	Josef
Dvořák	Dvořák	k1gMnSc1	Dvořák
alias	alias	k9	alias
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Kuřátko	kuřátko	k1gNnSc4	kuřátko
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
původního	původní	k2eAgInSc2d1	původní
nápadu	nápad	k1gInSc2	nápad
Radúz	Radúz	k1gMnSc1	Radúz
Činčera	Činčer	k1gMnSc2	Činčer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
v	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
fázi	fáze	k1gFnSc6	fáze
přizván	přizván	k2eAgInSc1d1	přizván
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
–	–	k?	–
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
velmi	velmi	k6eAd1	velmi
nepovedeného	povedený	k2eNgInSc2d1	nepovedený
–	–	k?	–
seriálu	seriál	k1gInSc2	seriál
distancoval	distancovat	k5eAaBmAgInS	distancovat
<g/>
.	.	kIx.	.
</s>
<s>
Stránky	stránka	k1gFnPc1	stránka
obnoveného	obnovený	k2eAgInSc2d1	obnovený
projektu	projekt	k1gInSc2	projekt
Kinoautomat	Kinoautomat	k1gInSc1	Kinoautomat
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
kině	kino	k1gNnSc6	kino
Světozor	světozor	k1gInSc1	světozor
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
obnoveného	obnovený	k2eAgInSc2d1	obnovený
projektu	projekt	k1gInSc2	projekt
Kinoautomat	Kinoautomat	k1gInSc1	Kinoautomat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
článek	článek	k1gInSc1	článek
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
Kinoautomatu	Kinoautomat	k1gInSc2	Kinoautomat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Reflex	reflex	k1gInSc1	reflex
Laterna	laterna	k1gFnSc1	laterna
magika	magika	k1gFnSc1	magika
Polyekran	polyekran	k1gInSc4	polyekran
</s>
