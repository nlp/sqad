<s>
Povídka	povídka	k1gFnSc1	povídka
Proměna	proměna	k1gFnSc1	proměna
začíná	začínat	k5eAaImIp3nS	začínat
probuzením	probuzení	k1gNnSc7	probuzení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
Řehoře	Řehoř	k1gMnSc2	Řehoř
Samsy	Samsa	k1gFnSc2	Samsa
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Gregor	Gregor	k1gMnSc1	Gregor
Samsa	Samsa	k1gFnSc1	Samsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc3	jeho
zjištěním	zjištění	k1gNnPc3	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
obří	obří	k2eAgInSc4d1	obří
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
základní	základní	k2eAgFnSc1d1	základní
informace	informace	k1gFnSc1	informace
celého	celý	k2eAgInSc2d1	celý
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
čtenáři	čtenář	k1gMnPc7	čtenář
představena	představit	k5eAaPmNgFnS	představit
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
větě	věta	k1gFnSc6	věta
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
–	–	k?	–
"	"	kIx"	"
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Řehoř	Řehoř	k1gMnSc1	Řehoř
Samsa	Samsa	k1gFnSc1	Samsa
jednou	jednou	k6eAd1	jednou
ráno	ráno	k6eAd1	ráno
probudil	probudit	k5eAaPmAgInS	probudit
z	z	k7c2	z
nepokojných	pokojný	k2eNgInPc2d1	nepokojný
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
shledal	shledat	k5eAaPmAgMnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
jakýsi	jakýsi	k3yIgInSc4	jakýsi
odporný	odporný	k2eAgInSc4d1	odporný
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
