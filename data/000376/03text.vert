<s>
Proměna	proměna	k1gFnSc1	proměna
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Die	Die	k1gMnSc1	Die
Verwandlung	Verwandlung	k1gMnSc1	Verwandlung
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
povídka	povídka	k1gFnSc1	povídka
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
tiskem	tisk	k1gInSc7	tisk
v	v	k7c6	v
říjnovém	říjnový	k2eAgNnSc6d1	říjnové
čísle	číslo	k1gNnSc6	číslo
expresionistického	expresionistický	k2eAgInSc2d1	expresionistický
měsíčníku	měsíčník	k1gInSc2	měsíčník
Die	Die	k1gFnSc2	Die
Weißen	Weißna	k1gFnPc2	Weißna
Blätter	Blätter	k1gMnSc1	Blätter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knižní	knižní	k2eAgFnSc6d1	knižní
podobě	podoba	k1gFnSc6	podoba
ji	on	k3xPp3gFnSc4	on
ještě	ještě	k9	ještě
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
publikovalo	publikovat	k5eAaBmAgNnS	publikovat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Kurta	Kurt	k1gMnSc2	Kurt
Wolffa	Wolff	k1gMnSc2	Wolff
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
Proměna	proměna	k1gFnSc1	proměna
začíná	začínat	k5eAaImIp3nS	začínat
probuzením	probuzení	k1gNnSc7	probuzení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
obchodního	obchodní	k2eAgMnSc2d1	obchodní
cestujícího	cestující	k1gMnSc2	cestující
Řehoře	Řehoř	k1gMnSc2	Řehoř
Samsy	Samsa	k1gFnSc2	Samsa
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Gregor	Gregor	k1gMnSc1	Gregor
Samsa	Samsa	k1gFnSc1	Samsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc3	jeho
zjištěním	zjištění	k1gNnPc3	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
obří	obří	k2eAgInSc4d1	obří
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
základní	základní	k2eAgFnSc1d1	základní
informace	informace	k1gFnSc1	informace
celého	celý	k2eAgInSc2d1	celý
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
čtenáři	čtenář	k1gMnPc7	čtenář
představena	představit	k5eAaPmNgFnS	představit
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
větě	věta	k1gFnSc6	věta
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
–	–	k?	–
"	"	kIx"	"
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Řehoř	Řehoř	k1gMnSc1	Řehoř
Samsa	Samsa	k1gFnSc1	Samsa
jednou	jednou	k6eAd1	jednou
ráno	ráno	k6eAd1	ráno
probudil	probudit	k5eAaPmAgInS	probudit
z	z	k7c2	z
nepokojných	pokojný	k2eNgInPc2d1	nepokojný
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
shledal	shledat	k5eAaPmAgMnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
jakýsi	jakýsi	k3yIgInSc4	jakýsi
odporný	odporný	k2eAgInSc4d1	odporný
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
svou	svůj	k3xOyFgFnSc4	svůj
indispozici	indispozice	k1gFnSc4	indispozice
skrýt	skrýt	k5eAaPmF	skrýt
před	před	k7c7	před
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
odhalen	odhalit	k5eAaPmNgInS	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Vyděšení	vyděšený	k2eAgMnPc1d1	vyděšený
rodiče	rodič	k1gMnPc1	rodič
a	a	k8xC	a
sestra	sestra	k1gFnSc1	sestra
Markétka	Markétka	k1gFnSc1	Markétka
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
Řehoře	Řehoř	k1gMnSc4	Řehoř
zavřít	zavřít	k5eAaPmF	zavřít
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
pokoji	pokoj	k1gInSc6	pokoj
v	v	k7c6	v
jakési	jakýsi	k3yIgFnSc6	jakýsi
domácí	domácí	k2eAgFnSc6d1	domácí
internaci	internace	k1gFnSc6	internace
<g/>
.	.	kIx.	.
</s>
<s>
Samsa	Samsa	k1gFnSc1	Samsa
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
změněného	změněný	k2eAgInSc2d1	změněný
stavu	stav	k1gInSc2	stav
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
racionálně	racionálně	k6eAd1	racionálně
<g/>
,	,	kIx,	,
nepropadá	propadat	k5eNaImIp3nS	propadat
depresím	deprese	k1gFnPc3	deprese
a	a	k8xC	a
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
chladnou	chladný	k2eAgFnSc4d1	chladná
hlavu	hlava	k1gFnSc4	hlava
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
starostí	starost	k1gFnSc7	starost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jako	jako	k9	jako
hmyz	hmyz	k1gInSc4	hmyz
půjde	jít	k5eAaImIp3nS	jít
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řehoř	Řehoř	k1gMnSc1	Řehoř
jako	jako	k9	jako
hmyz	hmyz	k1gInSc4	hmyz
rozumí	rozumět	k5eAaImIp3nS	rozumět
nadále	nadále	k6eAd1	nadále
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
jemu	on	k3xPp3gNnSc3	on
samotnému	samotný	k2eAgNnSc3d1	samotné
však	však	k9	však
ostatní	ostatní	k2eAgMnSc1d1	ostatní
příliš	příliš	k6eAd1	příliš
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
tedy	tedy	k9	tedy
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
okolím	okolí	k1gNnSc7	okolí
komunikovat	komunikovat	k5eAaImF	komunikovat
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
se	se	k3xPyFc4	se
od	od	k7c2	od
Řehořovy	Řehořův	k2eAgFnSc2d1	Řehořova
proměny	proměna	k1gFnSc2	proměna
značně	značně	k6eAd1	značně
zhorší	zhoršit	k5eAaPmIp3nS	zhoršit
<g/>
;	;	kIx,	;
jednak	jednak	k8xC	jednak
přijdou	přijít	k5eAaPmIp3nP	přijít
o	o	k7c4	o
Řehořovu	Řehořův	k2eAgFnSc4d1	Řehořova
finanční	finanční	k2eAgFnSc4d1	finanční
podporu	podpora	k1gFnSc4	podpora
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
platu	plat	k1gInSc2	plat
živil	živit	k5eAaImAgMnS	živit
převážně	převážně	k6eAd1	převážně
on	on	k3xPp3gMnSc1	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
strádají	strádat	k5eAaImIp3nP	strádat
rodiče	rodič	k1gMnPc1	rodič
i	i	k8xC	i
sestra	sestra	k1gFnSc1	sestra
psychicky	psychicky	k6eAd1	psychicky
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
navrací	navracet	k5eAaBmIp3nS	navracet
do	do	k7c2	do
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
sluha	sluha	k1gMnSc1	sluha
<g/>
,	,	kIx,	,
Markétka	Markétka	k1gFnSc1	Markétka
prodává	prodávat	k5eAaImIp3nS	prodávat
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
ani	ani	k8xC	ani
otec	otec	k1gMnSc1	otec
do	do	k7c2	do
Řehořova	Řehořův	k2eAgInSc2d1	Řehořův
pokoje	pokoj	k1gInSc2	pokoj
nevstupují	vstupovat	k5eNaImIp3nP	vstupovat
<g/>
,	,	kIx,	,
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gInSc4	on
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
když	když	k8xS	když
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
sestra	sestra	k1gFnSc1	sestra
přichází	přicházet	k5eAaImIp3nS	přicházet
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
Řehoř	Řehoř	k1gMnSc1	Řehoř
schová	schovat	k5eAaPmIp3nS	schovat
pod	pod	k7c4	pod
pohovku	pohovka	k1gFnSc4	pohovka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
neděsil	děsit	k5eNaImAgMnS	děsit
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
situací	situace	k1gFnSc7	situace
smíří	smířit	k5eAaPmIp3nS	smířit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
otrlá	otrlý	k2eAgFnSc1d1	otrlá
posluhovačka	posluhovačka	k1gFnSc1	posluhovačka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
Řehořem-hmyzem	Řehořemmyz	k1gInSc7	Řehořem-hmyz
jedná	jednat	k5eAaImIp3nS	jednat
rázně	rázně	k6eAd1	rázně
a	a	k8xC	a
bez	bez	k7c2	bez
okolků	okolek	k1gInPc2	okolek
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
Markéta	Markéta	k1gFnSc1	Markéta
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
z	z	k7c2	z
Řehořova	Řehořův	k2eAgInSc2d1	Řehořův
pokoje	pokoj	k1gInSc2	pokoj
nábytek	nábytek	k1gInSc1	nábytek
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
úmyslu	úmysl	k1gInSc6	úmysl
<g/>
,	,	kIx,	,
Markétka	Markétka	k1gFnSc1	Markétka
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
lépe	dobře	k6eAd2	dobře
lezlo	lézt	k5eAaImAgNnS	lézt
po	po	k7c6	po
stěnách	stěna	k1gFnPc6	stěna
a	a	k8xC	a
stropu	strop	k1gInSc2	strop
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
Řehoř	Řehoř	k1gMnSc1	Řehoř
skutečně	skutečně	k6eAd1	skutečně
rád	rád	k6eAd1	rád
baví	bavit	k5eAaImIp3nS	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
obává	obávat	k5eAaImIp3nS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
Řehoř	Řehoř	k1gMnSc1	Řehoř
mohl	moct	k5eAaImAgMnS	moct
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jako	jako	k9	jako
definitivní	definitivní	k2eAgNnSc4d1	definitivní
zavrhnutí	zavrhnutí	k1gNnSc4	zavrhnutí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Řehoř	Řehoř	k1gMnSc1	Řehoř
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
mu	on	k3xPp3gMnSc3	on
pokoj	pokoj	k1gInSc4	pokoj
ponechali	ponechat	k5eAaPmAgMnP	ponechat
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Brání	bránit	k5eAaImIp3nS	bránit
tedy	tedy	k9	tedy
svůj	svůj	k3xOyFgInSc4	svůj
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
spatří	spatřit	k5eAaPmIp3nS	spatřit
ho	on	k3xPp3gMnSc4	on
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
upadne	upadnout	k5eAaPmIp3nS	upadnout
do	do	k7c2	do
mdlob	mdloba	k1gFnPc2	mdloba
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrocenou	vyhrocený	k2eAgFnSc4d1	vyhrocená
situaci	situace	k1gFnSc4	situace
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vyřešit	vyřešit	k5eAaPmF	vyřešit
zaskočený	zaskočený	k2eAgMnSc1d1	zaskočený
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
Řehořovi	Řehoř	k1gMnSc6	Řehoř
hází	házet	k5eAaImIp3nS	házet
jablka	jablko	k1gNnPc1	jablko
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
ho	on	k3xPp3gMnSc4	on
těžce	těžce	k6eAd1	těžce
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Řehořův	Řehořův	k2eAgInSc1d1	Řehořův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
se	se	k3xPyFc4	se
i	i	k9	i
atmosféra	atmosféra	k1gFnSc1	atmosféra
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
;	;	kIx,	;
hmotný	hmotný	k2eAgInSc4d1	hmotný
nedostatek	nedostatek	k1gInSc4	nedostatek
a	a	k8xC	a
vypětí	vypětí	k1gNnSc4	vypětí
ze	z	k7c2	z
skrývání	skrývání	k1gNnSc2	skrývání
Řehoře	Řehoř	k1gMnSc2	Řehoř
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
doléhají	doléhat	k5eAaImIp3nP	doléhat
na	na	k7c4	na
jeho	jeho	k3xOp3gMnPc4	jeho
rodiče	rodič	k1gMnPc4	rodič
i	i	k8xC	i
Markétku	Markétka	k1gFnSc4	Markétka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
Řehoře	Řehoř	k1gMnSc4	Řehoř
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
si	se	k3xPyFc3	se
do	do	k7c2	do
bytu	byt	k1gInSc2	byt
nastěhuje	nastěhovat	k5eAaPmIp3nS	nastěhovat
podnájemníky	podnájemník	k1gMnPc4	podnájemník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
o	o	k7c6	o
Řehořovi	Řehoř	k1gMnSc6	Řehoř
nevědí	vědět	k5eNaImIp3nP	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
spatří	spatřit	k5eAaPmIp3nS	spatřit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Řehoř	Řehoř	k1gMnSc1	Řehoř
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
poslechnout	poslechnout	k5eAaPmF	poslechnout
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jim	on	k3xPp3gMnPc3	on
Markétka	Markétka	k1gFnSc1	Markétka
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
opustil	opustit	k5eAaPmAgMnS	opustit
svůj	svůj	k3xOyFgInSc4	svůj
pokoj	pokoj	k1gInSc4	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
snažil	snažit	k5eAaImAgMnS	snažit
zahnat	zahnat	k5eAaPmF	zahnat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stalo	stát	k5eAaPmAgNnS	stát
skladiště	skladiště	k1gNnSc1	skladiště
nepotřebných	potřebný	k2eNgFnPc2d1	nepotřebná
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Nájemníci	nájemník	k1gMnPc1	nájemník
dávají	dávat	k5eAaImIp3nP	dávat
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ránem	ráno	k1gNnSc7	ráno
Řehoř	Řehoř	k1gMnSc1	Řehoř
opuštěný	opuštěný	k2eAgMnSc1d1	opuštěný
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
jej	on	k3xPp3gInSc4	on
nalezne	nalézt	k5eAaBmIp3nS	nalézt
posluhovačka	posluhovačka	k1gFnSc1	posluhovačka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Pojďte	jít	k5eAaImRp2nP	jít
se	se	k3xPyFc4	se
podívat	podívat	k5eAaPmF	podívat
<g/>
,	,	kIx,	,
ono	onen	k3xDgNnSc1	onen
to	ten	k3xDgNnSc1	ten
chcíplo	chcípnout	k5eAaPmAgNnS	chcípnout
<g/>
;	;	kIx,	;
leží	ležet	k5eAaImIp3nS	ležet
to	ten	k3xDgNnSc1	ten
tam	tam	k6eAd1	tam
dočista	dočista	k6eAd1	dočista
chcíplé	chcíplý	k2eAgNnSc1d1	chcíplé
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
mrtvolu	mrtvola	k1gFnSc4	mrtvola
sama	sám	k3xTgNnPc1	sám
odklidí	odklidit	k5eAaPmIp3nS	odklidit
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
uspořádají	uspořádat	k5eAaPmIp3nP	uspořádat
Samsovi	Samsův	k2eAgMnPc1d1	Samsův
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
viditelně	viditelně	k6eAd1	viditelně
ulevilo	ulevit	k5eAaPmAgNnS	ulevit
<g/>
,	,	kIx,	,
výlet	výlet	k1gInSc1	výlet
z	z	k7c2	z
města	město	k1gNnSc2	město
a	a	k8xC	a
probírají	probírat	k5eAaImIp3nP	probírat
nadějné	nadějný	k2eAgInPc1d1	nadějný
plány	plán	k1gInPc1	plán
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Proměna	proměna	k1gFnSc1	proměna
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
typické	typický	k2eAgInPc4d1	typický
kafkovské	kafkovský	k2eAgInPc4d1	kafkovský
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xC	jako
pocit	pocit	k1gInSc4	pocit
bezvýchodnosti	bezvýchodnost	k1gFnSc2	bezvýchodnost
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
Řehoř	Řehoř	k1gMnSc1	Řehoř
do	do	k7c2	do
konce	konec	k1gInSc2	konec
doufá	doufat	k5eAaImIp3nS	doufat
v	v	k7c4	v
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
zvrat	zvrat	k1gInSc4	zvrat
svého	svůj	k3xOyFgInSc2	svůj
osudu	osud	k1gInSc2	osud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
absurditu	absurdita	k1gFnSc4	absurdita
(	(	kIx(	(
<g/>
svou	svůj	k3xOyFgFnSc4	svůj
proměnu	proměna	k1gFnSc4	proměna
přijal	přijmout	k5eAaPmAgMnS	přijmout
Řehoř	Řehoř	k1gMnSc1	Řehoř
s	s	k7c7	s
klidem	klid	k1gInSc7	klid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
komicko-tragické	komickoragický	k2eAgInPc1d1	komicko-tragický
momenty	moment	k1gInPc1	moment
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
bojuje	bojovat	k5eAaImIp3nS	bojovat
proti	proti	k7c3	proti
Řehořovi	Řehoř	k1gMnSc3	Řehoř
házením	házení	k1gNnPc3	házení
jablek	jablko	k1gNnPc2	jablko
<g/>
)	)	kIx)	)
i	i	k8xC	i
humor	humor	k1gInSc4	humor
<g/>
.	.	kIx.	.
</s>
<s>
Proměna	proměna	k1gFnSc1	proměna
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
často	často	k6eAd1	často
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejhumornějších	humorný	k2eAgNnPc2d3	nejhumornější
Kafkových	Kafkových	k2eAgNnPc2d1	Kafkových
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
využíváním	využívání	k1gNnSc7	využívání
kontrastu	kontrast	k1gInSc2	kontrast
racionálního	racionální	k2eAgNnSc2d1	racionální
uvažování	uvažování	k1gNnSc2	uvažování
a	a	k8xC	a
absurdní	absurdní	k2eAgFnSc1d1	absurdní
situace	situace	k1gFnSc1	situace
proměny	proměna	k1gFnSc2	proměna
v	v	k7c4	v
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
užitím	užití	k1gNnSc7	užití
paradoxních	paradoxní	k2eAgNnPc2d1	paradoxní
slovních	slovní	k2eAgNnPc2d1	slovní
spojení	spojení	k1gNnPc2	spojení
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
a	a	k8xC	a
už	už	k6eAd1	už
klepal	klepat	k5eAaImAgInS	klepat
na	na	k7c4	na
jedny	jeden	k4xCgFnPc4	jeden
z	z	k7c2	z
postranních	postranní	k2eAgFnPc2d1	postranní
dveří	dveře	k1gFnPc2	dveře
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
pěstí	pěstit	k5eAaImIp3nS	pěstit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Proměna	proměna	k1gFnSc1	proměna
nabízí	nabízet	k5eAaImIp3nS	nabízet
řadu	řada	k1gFnSc4	řada
rozporuplných	rozporuplný	k2eAgFnPc2d1	rozporuplná
interpretací	interpretace	k1gFnPc2	interpretace
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
co	co	k9	co
vlastně	vlastně	k9	vlastně
Řehořova	Řehořův	k2eAgFnSc1d1	Řehořova
proměna	proměna	k1gFnSc1	proměna
ve	v	k7c4	v
hmyz	hmyz	k1gInSc4	hmyz
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
některé	některý	k3yIgFnPc1	některý
možnosti	možnost	k1gFnPc1	možnost
významu	význam	k1gInSc2	význam
Kafkovy	Kafkův	k2eAgFnSc2d1	Kafkova
povídky	povídka	k1gFnSc2	povídka
<g/>
:	:	kIx,	:
Proměna	proměna	k1gFnSc1	proměna
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
Řehořova	Řehořův	k2eAgFnSc1d1	Řehořova
snová	snový	k2eAgFnSc1d1	snová
<g/>
,	,	kIx,	,
nerealistická	realistický	k2eNgFnSc1d1	nerealistická
vize	vize	k1gFnSc1	vize
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
absurdní	absurdní	k2eAgFnSc4d1	absurdní
grotesku	groteska	k1gFnSc4	groteska
plnou	plný	k2eAgFnSc4d1	plná
nadsázky	nadsázka	k1gFnSc2	nadsázka
a	a	k8xC	a
humoru	humor	k1gInSc2	humor
<g/>
.	.	kIx.	.
</s>
<s>
Řehořova	Řehořův	k2eAgFnSc1d1	Řehořova
proměna	proměna	k1gFnSc1	proměna
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
pobavení	pobavení	k1gNnSc2	pobavení
<g/>
.	.	kIx.	.
</s>
<s>
Proměna	proměna	k1gFnSc1	proměna
je	být	k5eAaImIp3nS	být
symbolické	symbolický	k2eAgNnSc4d1	symbolické
zobrazení	zobrazení	k1gNnSc4	zobrazení
osamělosti	osamělost	k1gFnSc2	osamělost
a	a	k8xC	a
bezradnosti	bezradnost	k1gFnSc2	bezradnost
před	před	k7c7	před
vlastním	vlastní	k2eAgInSc7d1	vlastní
údělem	úděl	k1gInSc7	úděl
<g/>
.	.	kIx.	.
</s>
<s>
Proměna	proměna	k1gFnSc1	proměna
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
strnulost	strnulost	k1gFnSc4	strnulost
a	a	k8xC	a
povrchní	povrchní	k2eAgFnSc4d1	povrchní
morálku	morálka	k1gFnSc4	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
psychoanalytického	psychoanalytický	k2eAgNnSc2d1	psychoanalytické
hlediska	hledisko	k1gNnSc2	hledisko
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
ke	k	k7c3	k
dvojí	dvojí	k4xRgFnSc3	dvojí
proměně	proměna	k1gFnSc3	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
Řehoř	Řehoř	k1gMnSc1	Řehoř
otcovo	otcův	k2eAgNnSc1d1	otcovo
místo	místo	k1gNnSc1	místo
živitele	živitel	k1gMnSc2	živitel
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
nepracoval	pracovat	k5eNaImAgMnS	pracovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
rodinu	rodina	k1gFnSc4	rodina
živil	živit	k5eAaImAgMnS	živit
Řehoř	Řehoř	k1gMnSc1	Řehoř
<g/>
)	)	kIx)	)
a	a	k8xC	a
Řehoř	Řehoř	k1gMnSc1	Řehoř
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
vůdčí	vůdčí	k2eAgMnSc1d1	vůdčí
jedinec	jedinec	k1gMnSc1	jedinec
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
proměně	proměna	k1gFnSc6	proměna
je	být	k5eAaImIp3nS	být
Řehoř	Řehoř	k1gMnSc1	Řehoř
oslaben	oslabit	k5eAaPmNgMnS	oslabit
<g/>
,	,	kIx,	,
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
místo	místo	k1gNnSc4	místo
vůdce	vůdce	k1gMnSc2	vůdce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
znovu	znovu	k6eAd1	znovu
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
(	(	kIx(	(
<g/>
jablky	jablko	k1gNnPc7	jablko
<g/>
)	)	kIx)	)
otec	otec	k1gMnSc1	otec
syna	syn	k1gMnSc2	syn
smrtelně	smrtelně	k6eAd1	smrtelně
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
německé	německý	k2eAgFnSc2d1	německá
literatury	literatura	k1gFnSc2	literatura
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Princetonu	Princeton	k1gInSc6	Princeton
Stanley	Stanlea	k1gMnSc2	Stanlea
Corngold	Corngold	k1gMnSc1	Corngold
uvedl	uvést	k5eAaPmAgMnS	uvést
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gMnSc1	The
Commentator	Commentator	k1gMnSc1	Commentator
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Despair	Despaira	k1gFnPc2	Despaira
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Komentátorova	komentátorův	k2eAgFnSc1d1	komentátorova
beznaděj	beznaděj	k1gFnSc1	beznaděj
<g/>
)	)	kIx)	)
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
třicet	třicet	k4xCc4	třicet
různých	různý	k2eAgFnPc2d1	různá
možností	možnost	k1gFnPc2	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
Verwandlung	Verwandlung	k1gMnSc1	Verwandlung
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
Weissen	Weissna	k1gFnPc2	Weissna
Blätter	Blätter	k1gMnSc1	Blätter
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
č.	č.	k?	č.
10	[number]	k4	10
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
1177-1230	[number]	k4	1177-1230
Pražská	pražský	k2eAgFnSc1d1	Pražská
německá	německý	k2eAgFnSc1d1	německá
literatura	literatura	k1gFnSc1	literatura
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Proměna	proměna	k1gFnSc1	proměna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Český	český	k2eAgInSc1d1	český
text	text	k1gInSc1	text
povídky	povídka	k1gFnSc2	povídka
Celý	celý	k2eAgInSc1d1	celý
německý	německý	k2eAgInSc1d1	německý
text	text	k1gInSc1	text
povídky	povídka	k1gFnSc2	povídka
Proměna	proměna	k1gFnSc1	proměna
Nové	Nová	k1gFnSc2	Nová
vydání	vydání	k1gNnSc2	vydání
Proměny	proměna	k1gFnSc2	proměna
ke	k	k7c3	k
130	[number]	k4	130
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
Vydání	vydání	k1gNnSc2	vydání
Proměny	proměna	k1gFnSc2	proměna
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
argentinského	argentinský	k2eAgMnSc2d1	argentinský
malíře	malíř	k1gMnSc2	malíř
Luise	Luisa	k1gFnSc6	Luisa
Scafatiho	Scafati	k1gMnSc2	Scafati
Originální	originální	k2eAgFnSc2d1	originální
verze	verze	k1gFnSc2	verze
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
-	-	kIx~	-
Die	Die	k1gMnSc1	Die
Verwandlung	Verwandlung	k1gMnSc1	Verwandlung
v	v	k7c6	v
PDF	PDF	kA	PDF
</s>
