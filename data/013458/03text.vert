<p>
<s>
Sporýšovité	Sporýšovitý	k2eAgInPc4d1	Sporýšovitý
(	(	kIx(	(
<g/>
Verbenaceae	Verbenacea	k1gInPc4	Verbenacea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
čeleď	čeleď	k1gFnSc1	čeleď
vyšších	vysoký	k2eAgFnPc2d2	vyšší
dvouděložných	dvouděložný	k2eAgFnPc2d1	dvouděložná
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
hluchavkotvaré	hluchavkotvarý	k2eAgMnPc4d1	hluchavkotvarý
(	(	kIx(	(
<g/>
Lamiales	Lamiales	k1gInSc1	Lamiales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
31	[number]	k4	31
rodech	rod	k1gInPc6	rod
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
květeně	květena	k1gFnSc6	květena
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
pouze	pouze	k6eAd1	pouze
sporýšem	sporýš	k1gInSc7	sporýš
lékařským	lékařský	k2eAgInSc7d1	lékařský
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
jako	jako	k9	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
nebo	nebo	k8xC	nebo
léčivé	léčivý	k2eAgFnPc1d1	léčivá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Sporýšovité	Sporýšovitý	k2eAgFnPc1d1	Sporýšovitý
jsou	být	k5eAaImIp3nP	být
byliny	bylina	k1gFnPc1	bylina
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc1	strom
i	i	k8xC	i
liány	liána	k1gFnPc1	liána
<g/>
.	.	kIx.	.
</s>
<s>
Stonky	stonka	k1gFnPc1	stonka
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
čtyřhranné	čtyřhranný	k2eAgFnPc1d1	čtyřhranná
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mají	mít	k5eAaImIp3nP	mít
rostliny	rostlina	k1gFnPc1	rostlina
trny	trn	k1gInPc7	trn
nebo	nebo	k8xC	nebo
ostny	osten	k1gInPc7	osten
<g/>
.	.	kIx.	.
</s>
<s>
Odění	odění	k1gNnSc1	odění
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
nebo	nebo	k8xC	nebo
žlaznatých	žlaznatý	k2eAgInPc2d1	žlaznatý
chlupů	chlup	k1gInPc2	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
vstřícné	vstřícný	k2eAgInPc1d1	vstřícný
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
přeslenité	přeslenitý	k2eAgFnPc1d1	přeslenitá
<g/>
,	,	kIx,	,
se	s	k7c7	s
zpeřenou	zpeřený	k2eAgFnSc7d1	zpeřená
žilnatinou	žilnatina	k1gFnSc7	žilnatina
a	a	k8xC	a
bez	bez	k7c2	bez
palistů	palist	k1gInPc2	palist
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gInSc1	čepel
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
celokrajná	celokrajný	k2eAgFnSc1d1	celokrajná
nebo	nebo	k8xC	nebo
zubatá	zubatý	k2eAgFnSc1d1	zubatá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květenství	květenství	k1gNnPc1	květenství
jsou	být	k5eAaImIp3nP	být
úžlabní	úžlabní	k2eAgNnPc1d1	úžlabní
nebo	nebo	k8xC	nebo
vrcholová	vrcholový	k2eAgNnPc1d1	vrcholové
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
hrozny	hrozen	k1gInPc4	hrozen
<g/>
,	,	kIx,	,
klasy	klas	k1gInPc4	klas
nebo	nebo	k8xC	nebo
hlávky	hlávka	k1gFnPc4	hlávka
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
<g/>
,	,	kIx,	,
oboupohlavné	oboupohlavný	k2eAgFnPc1d1	oboupohlavná
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
pětičetné	pětičetný	k2eAgFnPc1d1	pětičetná
<g/>
.	.	kIx.	.
</s>
<s>
Kalich	kalich	k1gInSc1	kalich
je	být	k5eAaImIp3nS	být
trubkovitý	trubkovitý	k2eAgInSc1d1	trubkovitý
nebo	nebo	k8xC	nebo
zvonkovitý	zvonkovitý	k2eAgInSc1d1	zvonkovitý
<g/>
,	,	kIx,	,
vytrvalý	vytrvalý	k2eAgInSc1d1	vytrvalý
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
za	za	k7c2	za
plodu	plod	k1gInSc2	plod
zvětšený	zvětšený	k2eAgMnSc1d1	zvětšený
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
dvoupyská	dvoupyský	k2eAgFnSc1d1	dvoupyská
<g/>
,	,	kIx,	,
z	z	k7c2	z
5	[number]	k4	5
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
s	s	k7c7	s
korunní	korunní	k2eAgFnSc7d1	korunní
trubkou	trubka	k1gFnSc7	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
horní	horní	k2eAgInPc4d1	horní
korunní	korunní	k2eAgInPc4d1	korunní
plátky	plátek	k1gInPc4	plátek
srostlé	srostlý	k2eAgInPc4d1	srostlý
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
<g/>
,	,	kIx,	,
dvoumocné	dvoumocný	k2eAgNnSc4d1	dvoumocný
<g/>
,	,	kIx,	,
přirostlé	přirostlý	k2eAgNnSc4d1	přirostlé
ke	k	k7c3	k
koruně	koruna	k1gFnSc3	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
je	být	k5eAaImIp3nS	být
svrchní	svrchní	k2eAgInSc4d1	svrchní
<g/>
,	,	kIx,	,
srostlý	srostlý	k2eAgInSc4d1	srostlý
ze	z	k7c2	z
2	[number]	k4	2
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
dvoupouzdrý	dvoupouzdrý	k2eAgInSc4d1	dvoupouzdrý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zdánlivě	zdánlivě	k6eAd1	zdánlivě
čtyřpouzdrý	čtyřpouzdrý	k2eAgInSc1d1	čtyřpouzdrý
vlivem	vliv	k1gInSc7	vliv
vývinu	vývin	k1gInSc2	vývin
falešných	falešný	k2eAgFnPc2d1	falešná
přehrádek	přehrádka	k1gFnPc2	přehrádka
<g/>
.	.	kIx.	.
</s>
<s>
Čnělka	čnělka	k1gFnSc1	čnělka
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
s	s	k7c7	s
dvoulaločnou	dvoulaločný	k2eAgFnSc7d1	dvoulaločná
bliznou	blizna	k1gFnSc7	blizna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
plodolistu	plodolist	k1gInSc6	plodolist
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
vajíčka	vajíčko	k1gNnSc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
peckovice	peckovice	k1gFnSc1	peckovice
se	s	k7c7	s
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
semeny	semeno	k1gNnPc7	semeno
nebo	nebo	k8xC	nebo
tvrdka	tvrdka	k1gFnSc1	tvrdka
rozpadající	rozpadající	k2eAgFnSc1d1	rozpadající
se	se	k3xPyFc4	se
na	na	k7c4	na
4	[number]	k4	4
oříšky	oříšek	k1gInPc4	oříšek
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
endospermu	endosperm	k1gInSc2	endosperm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Květy	květ	k1gInPc1	květ
sporýšovitých	sporýšovití	k1gMnPc2	sporýšovití
jsou	být	k5eAaImIp3nP	být
opylovány	opylován	k2eAgFnPc1d1	opylována
nejčastěji	často	k6eAd3	často
včelami	včela	k1gFnPc7	včela
<g/>
,	,	kIx,	,
vosami	vosa	k1gFnPc7	vosa
a	a	k8xC	a
mouchami	moucha	k1gFnPc7	moucha
<g/>
.	.	kIx.	.
</s>
<s>
Samoopylení	samoopylení	k1gNnSc1	samoopylení
je	být	k5eAaImIp3nS	být
zabráněno	zabránit	k5eAaPmNgNnS	zabránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyčinky	tyčinka	k1gFnPc1	tyčinka
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
blizna	blizna	k1gFnSc1	blizna
(	(	kIx(	(
<g/>
protandrie	protandrie	k1gFnSc1	protandrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Květy	Květa	k1gFnPc1	Květa
rodu	rod	k1gInSc2	rod
Rhaphithamnus	Rhaphithamnus	k1gInSc1	Rhaphithamnus
jsou	být	k5eAaImIp3nP	být
opylovány	opylován	k2eAgMnPc4d1	opylován
kolibříky	kolibřík	k1gMnPc4	kolibřík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Semena	semeno	k1gNnPc1	semeno
z	z	k7c2	z
dužnatých	dužnatý	k2eAgMnPc2d1	dužnatý
plodů	plod	k1gInPc2	plod
jsou	být	k5eAaImIp3nP	být
šířena	šířen	k2eAgNnPc1d1	šířeno
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Suché	Suché	k2eAgInPc1d1	Suché
oříšky	oříšek	k1gInPc1	oříšek
jsou	být	k5eAaImIp3nP	být
srostlé	srostlý	k2eAgNnSc4d1	srostlé
s	s	k7c7	s
vytrvalým	vytrvalý	k2eAgInSc7d1	vytrvalý
kalichem	kalich	k1gInSc7	kalich
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
šířeny	šířen	k2eAgInPc4d1	šířen
nejčastěji	často	k6eAd3	často
větrem	vítr	k1gInSc7	vítr
či	či	k8xC	či
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
i	i	k9	i
na	na	k7c6	na
srsti	srst	k1gFnSc6	srst
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Priva	Priva	k1gFnSc1	Priva
lappulacea	lappulacea	k1gFnSc1	lappulacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Čeleď	čeleď	k1gFnSc1	čeleď
sporýšovité	sporýšovitý	k2eAgNnSc1d1	sporýšovitý
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
pojetí	pojetí	k1gNnSc6	pojetí
asi	asi	k9	asi
920	[number]	k4	920
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
31	[number]	k4	31
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
rody	rod	k1gInPc1	rod
jsou	být	k5eAaImIp3nP	být
sporýš	sporýš	k1gInSc4	sporýš
(	(	kIx(	(
<g/>
Verbena	verbena	k1gFnSc1	verbena
<g/>
,	,	kIx,	,
asi	asi	k9	asi
300	[number]	k4	300
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stachytarpheta	Stachytarpheta	k1gFnSc1	Stachytarpheta
(	(	kIx(	(
<g/>
130	[number]	k4	130
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houselník	houselník	k1gInSc1	houselník
(	(	kIx(	(
<g/>
Citharexylum	Citharexylum	k1gNnSc1	Citharexylum
<g/>
,	,	kIx,	,
130	[number]	k4	130
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lipie	lipie	k1gFnSc1	lipie
(	(	kIx(	(
<g/>
Lippia	Lippia	k1gFnSc1	Lippia
<g/>
,	,	kIx,	,
120	[number]	k4	120
<g/>
)	)	kIx)	)
a	a	k8xC	a
libora	libora	k1gFnSc1	libora
(	(	kIx(	(
<g/>
Lantana	Lantana	k1gFnSc1	Lantana
<g/>
,	,	kIx,	,
100	[number]	k4	100
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sporýšovité	Sporýšovitý	k2eAgFnPc1d1	Sporýšovitý
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květeně	květena	k1gFnSc6	květena
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgInSc1d1	původní
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
<g/>
,	,	kIx,	,
sporýš	sporýš	k1gInSc1	sporýš
lékařský	lékařský	k2eAgInSc1d1	lékařský
(	(	kIx(	(
<g/>
Verbena	verbena	k1gFnSc1	verbena
officinalis	officinalis	k1gFnSc1	officinalis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
domácí	domácí	k2eAgInPc4d1	domácí
2	[number]	k4	2
druhy	druh	k1gInPc4	druh
sporýšů	sporýš	k1gInPc2	sporýš
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
domácího	domácí	k2eAgInSc2d1	domácí
sporýše	sporýš	k1gInSc2	sporýš
lékařského	lékařský	k2eAgInSc2d1	lékařský
roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
ještě	ještě	k6eAd1	ještě
nízký	nízký	k2eAgInSc4d1	nízký
poléhavý	poléhavý	k2eAgInSc4d1	poléhavý
sporýš	sporýš	k1gInSc4	sporýš
Verbena	verbena	k1gFnSc1	verbena
supina	supinum	k1gNnSc2	supinum
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
bývají	bývat	k5eAaImIp3nP	bývat
zavlékány	zavlékán	k2eAgInPc1d1	zavlékán
nebo	nebo	k8xC	nebo
zplaňují	zplaňovat	k5eAaImIp3nP	zplaňovat
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
druhy	druh	k1gInPc1	druh
sporýšů	sporýš	k1gInPc2	sporýš
a	a	k8xC	a
také	také	k9	také
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
rodu	rod	k1gInSc2	rod
lipie	lipie	k1gFnSc2	lipie
(	(	kIx(	(
<g/>
Lippia	Lippia	k1gFnSc1	Lippia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahové	obsahový	k2eAgFnPc1d1	obsahová
látky	látka	k1gFnPc1	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Charakteristickými	charakteristický	k2eAgFnPc7d1	charakteristická
obsahovými	obsahový	k2eAgFnPc7d1	obsahová
látkami	látka	k1gFnPc7	látka
jsou	být	k5eAaImIp3nP	být
iridoidy	iridoida	k1gFnPc1	iridoida
a	a	k8xC	a
fenolické	fenolický	k2eAgInPc1d1	fenolický
glykosidy	glykosid	k1gInPc1	glykosid
<g/>
.	.	kIx.	.
<g/>
Libora	Libora	k1gFnSc1	Libora
měňavá	měňavý	k2eAgFnSc1d1	měňavá
(	(	kIx(	(
<g/>
Lantana	Lantana	k1gFnSc1	Lantana
camara	camara	k1gFnSc1	camara
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
triterpenoidy	triterpenoida	k1gFnPc4	triterpenoida
zvané	zvaný	k2eAgFnPc4d1	zvaná
lantadeny	lantaden	k2eAgFnPc4d1	lantaden
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnPc4d1	způsobující
forosenzitivitu	forosenzitivita	k1gFnSc4	forosenzitivita
a	a	k8xC	a
poškození	poškození	k1gNnSc4	poškození
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
a	a	k8xC	a
alkaloid	alkaloid	k1gInSc1	alkaloid
lantanin	lantanin	k2eAgInSc1d1	lantanin
s	s	k7c7	s
antimikrobiální	antimikrobiální	k2eAgFnSc7d1	antimikrobiální
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dřívější	dřívější	k2eAgFnSc6d1	dřívější
morfologické	morfologický	k2eAgFnSc6d1	morfologická
taxonomii	taxonomie	k1gFnSc6	taxonomie
byla	být	k5eAaImAgFnS	být
čeleď	čeleď	k1gFnSc1	čeleď
sporýšovité	sporýšovitý	k2eAgFnSc2d1	sporýšovitý
vymezena	vymezit	k5eAaPmNgFnS	vymezit
proti	proti	k7c3	proti
čeledi	čeleď	k1gFnSc3	čeleď
hluchavkovité	hluchavkovitý	k2eAgFnSc2d1	hluchavkovitá
(	(	kIx(	(
<g/>
Lamiaceae	Lamiacea	k1gFnSc2	Lamiacea
<g/>
)	)	kIx)	)
především	především	k9	především
pozicí	pozice	k1gFnSc7	pozice
čnělky	čnělka	k1gFnSc2	čnělka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
sporýšovitých	sporýšovití	k1gMnPc2	sporýšovití
terminální	terminální	k2eAgFnSc1d1	terminální
(	(	kIx(	(
<g/>
umístěná	umístěný	k2eAgFnSc1d1	umístěná
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
semeníku	semeník	k1gInSc2	semeník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
hluchavkovitých	hluchavkovitý	k2eAgInPc2d1	hluchavkovitý
gynobazická	gynobazický	k2eAgFnSc1d1	gynobazický
(	(	kIx(	(
<g/>
vyrůstající	vyrůstající	k2eAgFnSc1d1	vyrůstající
z	z	k7c2	z
hluboce	hluboko	k6eAd1	hluboko
laločnatého	laločnatý	k2eAgInSc2d1	laločnatý
semeníku	semeník	k1gInSc2	semeník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Molekulárními	molekulární	k2eAgFnPc7d1	molekulární
studiemi	studie	k1gFnPc7	studie
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeleď	čeleď	k1gFnSc1	čeleď
sporýšovité	sporýšovitý	k2eAgNnSc1d1	sporýšovitý
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
systému	systém	k1gInSc6	systém
parafyletická	parafyletický	k2eAgFnSc1d1	parafyletická
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hluchavkovité	hluchavkovitý	k2eAgFnPc1d1	hluchavkovitá
jsou	být	k5eAaImIp3nP	být
polyfyletické	polyfyletický	k2eAgFnPc1d1	polyfyletická
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byly	být	k5eAaImAgInP	být
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
rodů	rod	k1gInPc2	rod
přesunuty	přesunut	k2eAgInPc1d1	přesunut
ze	z	k7c2	z
sporýšovitých	sporýšovitý	k2eAgFnPc2d1	sporýšovitý
do	do	k7c2	do
hluchavkovitých	hluchavkovitý	k2eAgFnPc2d1	hluchavkovitá
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
morfologické	morfologický	k2eAgNnSc1d1	morfologické
rozlišení	rozlišení	k1gNnSc1	rozlišení
pozbylo	pozbýt	k5eAaPmAgNnS	pozbýt
platnosti	platnost	k1gFnSc2	platnost
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
sporýšovitých	sporýšovití	k1gMnPc2	sporýšovití
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
do	do	k7c2	do
samostatné	samostatný	k2eAgFnSc2d1	samostatná
čeledi	čeleď	k1gFnSc2	čeleď
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
řazen	řadit	k5eAaImNgInS	řadit
též	též	k9	též
mangrovový	mangrovový	k2eAgInSc1d1	mangrovový
rod	rod	k1gInSc1	rod
Avicennia	Avicennium	k1gNnSc2	Avicennium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
systému	systém	k1gInSc6	systém
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
čeledi	čeleď	k1gFnSc2	čeleď
paznehtníkovité	paznehtníkovitý	k2eAgFnSc2d1	paznehtníkovitý
(	(	kIx(	(
<g/>
Acanthaceae	Acanthacea	k1gFnSc2	Acanthacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
čeleď	čeleď	k1gFnSc1	čeleď
sporýšovité	sporýšovitý	k2eAgFnSc2d1	sporýšovitý
členěna	členit	k5eAaImNgFnS	členit
na	na	k7c4	na
8	[number]	k4	8
podčeledí	podčeleď	k1gFnPc2	podčeleď
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Petreeae	Petreeae	k1gFnSc1	Petreeae
–	–	k?	–
71	[number]	k4	71
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
12	[number]	k4	12
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
tropická	tropický	k2eAgFnSc1d1	tropická
Amerika	Amerika	k1gFnSc1	Amerika
</s>
</p>
<p>
<s>
Duranteae	Duranteae	k1gFnSc1	Duranteae
–	–	k?	–
192	[number]	k4	192
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
6	[number]	k4	6
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
Amerika	Amerika	k1gFnSc1	Amerika
od	od	k7c2	od
USA	USA	kA	USA
po	po	k7c4	po
Argentinu	Argentina	k1gFnSc4	Argentina
<g/>
,	,	kIx,	,
Afrika	Afrika	k1gFnSc1	Afrika
až	až	k8xS	až
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
Stachytarpheta	Stachytarphet	k2eAgFnSc1d1	Stachytarphet
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Casselieae	Casselieae	k1gFnSc1	Casselieae
–	–	k?	–
14	[number]	k4	14
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
3	[number]	k4	3
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
až	až	k8xS	až
Argentina	Argentina	k1gFnSc1	Argentina
</s>
</p>
<p>
<s>
Citharexyleae	Citharexyleae	k1gFnSc1	Citharexyleae
–	–	k?	–
135	[number]	k4	135
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
3	[number]	k4	3
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
j.	j.	k?	j.
USA	USA	kA	USA
až	až	k9	až
Argentina	Argentina	k1gFnSc1	Argentina
(	(	kIx(	(
<g/>
Citharexylum	Citharexylum	k1gInSc1	Citharexylum
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Priveae	Priveae	k1gFnSc1	Priveae
–	–	k?	–
71	[number]	k4	71
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
21	[number]	k4	21
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
tropy	trop	k1gInPc1	trop
a	a	k8xC	a
teplé	teplý	k2eAgFnPc1d1	teplá
oblasti	oblast	k1gFnPc1	oblast
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
</s>
</p>
<p>
<s>
Neospartoneae	Neospartoneae	k1gFnSc1	Neospartoneae
–	–	k?	–
6	[number]	k4	6
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
3	[number]	k4	3
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
</s>
</p>
<p>
<s>
Verbeneae	Verbeneae	k1gFnSc1	Verbeneae
–	–	k?	–
160	[number]	k4	160
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
3	[number]	k4	3
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
Eurasii	Eurasie	k1gFnSc6	Eurasie
</s>
</p>
<p>
<s>
Lantaneae	Lantaneae	k1gFnSc1	Lantaneae
–	–	k?	–
275	[number]	k4	275
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
9	[number]	k4	9
rodech	rod	k1gInPc6	rod
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
Lippia	Lippia	k1gFnSc1	Lippia
<g/>
,	,	kIx,	,
Lantana	Lantana	k1gFnSc1	Lantana
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zástupci	zástupce	k1gMnPc1	zástupce
==	==	k?	==
</s>
</p>
<p>
<s>
duranta	duranta	k1gFnSc1	duranta
(	(	kIx(	(
<g/>
Duranta	Duranta	k1gFnSc1	Duranta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
houselník	houselník	k1gInSc1	houselník
(	(	kIx(	(
<g/>
Citharexylum	Citharexylum	k1gInSc1	Citharexylum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
libora	libora	k1gFnSc1	libora
(	(	kIx(	(
<g/>
Lantana	Lantana	k1gFnSc1	Lantana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lipie	lipie	k1gFnSc1	lipie
(	(	kIx(	(
<g/>
Lippia	Lippia	k1gFnSc1	Lippia
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
petrea	petrea	k1gFnSc1	petrea
(	(	kIx(	(
<g/>
Petrea	Petrea	k1gFnSc1	Petrea
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sporýš	sporýš	k1gInSc1	sporýš
(	(	kIx(	(
<g/>
Verbena	verbena	k1gFnSc1	verbena
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tamonea	tamonea	k1gFnSc1	tamonea
(	(	kIx(	(
<g/>
Tamonea	Tamonea	k1gFnSc1	Tamonea
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vratiklas	vratiklas	k1gInSc1	vratiklas
(	(	kIx(	(
<g/>
Stachytarpheta	Stachytarpheta	k1gFnSc1	Stachytarpheta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Sporýšovité	Sporýšovitý	k2eAgInPc1d1	Sporýšovitý
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
především	především	k6eAd1	především
jako	jako	k8xS	jako
léčivé	léčivý	k2eAgFnPc1d1	léčivá
a	a	k8xC	a
okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
získávány	získáván	k2eAgFnPc1d1	získávána
silice	silice	k1gFnPc1	silice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Léčivé	léčivý	k2eAgFnPc1d1	léčivá
rostliny	rostlina	k1gFnPc1	rostlina
===	===	k?	===
</s>
</p>
<p>
<s>
Sporýš	sporýš	k1gInSc1	sporýš
lékařský	lékařský	k2eAgInSc1d1	lékařský
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
lidové	lidový	k2eAgFnSc6d1	lidová
medicíně	medicína	k1gFnSc6	medicína
při	při	k7c6	při
bolestech	bolest	k1gFnPc6	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
k	k	k7c3	k
detoxikaci	detoxikace	k1gFnSc3	detoxikace
a	a	k8xC	a
na	na	k7c4	na
nehojící	hojící	k2eNgFnPc4d1	nehojící
se	se	k3xPyFc4	se
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
trávení	trávení	k1gNnSc1	trávení
<g/>
,	,	kIx,	,
desinfikuje	desinfikovat	k5eAaBmIp3nS	desinfikovat
střeva	střevo	k1gNnSc2	střevo
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
sekreci	sekrece	k1gFnSc4	sekrece
mateřského	mateřský	k2eAgNnSc2d1	mateřské
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
močopudně	močopudně	k6eAd1	močopudně
<g/>
.	.	kIx.	.
<g/>
Takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
citronová	citronový	k2eAgFnSc1d1	citronová
verbena	verbena	k1gFnSc1	verbena
neboli	neboli	k8xC	neboli
Aloysia	Aloysia	k1gFnSc1	Aloysia
citriodora	citriodora	k1gFnSc1	citriodora
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Lippia	Lippius	k1gMnSc4	Lippius
citriodora	citriodor	k1gMnSc4	citriodor
<g/>
,	,	kIx,	,
Lippia	Lippius	k1gMnSc4	Lippius
triphylla	triphyll	k1gMnSc4	triphyll
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
spasmolytikum	spasmolytikum	k1gNnSc1	spasmolytikum
<g/>
,	,	kIx,	,
stomatikum	stomatikum	k1gNnSc1	stomatikum
<g/>
,	,	kIx,	,
na	na	k7c4	na
dýchací	dýchací	k2eAgFnPc4d1	dýchací
potíže	potíž	k1gFnPc4	potíž
a	a	k8xC	a
proti	proti	k7c3	proti
střevním	střevní	k2eAgMnPc3d1	střevní
parazitům	parazit	k1gMnPc3	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
jako	jako	k8xS	jako
pokojová	pokojový	k2eAgFnSc1d1	pokojová
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
vonnými	vonný	k2eAgInPc7d1	vonný
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
na	na	k7c4	na
bylinné	bylinný	k2eAgInPc4d1	bylinný
čaje	čaj	k1gInPc4	čaj
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
vonných	vonný	k2eAgInPc2d1	vonný
olejů	olej	k1gInPc2	olej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
listů	list	k1gInPc2	list
vratiklasu	vratiklas	k1gInSc2	vratiklas
Stachytarpheta	Stachytarphet	k1gMnSc2	Stachytarphet
jamaicensis	jamaicensis	k1gFnSc2	jamaicensis
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
medicíně	medicína	k1gFnSc6	medicína
tropické	tropický	k2eAgFnSc2d1	tropická
Ameriky	Amerika	k1gFnSc2	Amerika
využíván	využívat	k5eAaImNgInS	využívat
proti	proti	k7c3	proti
střevním	střevní	k2eAgMnPc3d1	střevní
parazitům	parazit	k1gMnPc3	parazit
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
dávidlo	dávidlo	k1gNnSc1	dávidlo
<g/>
,	,	kIx,	,
při	při	k7c6	při
léčení	léčení	k1gNnSc6	léčení
kožních	kožní	k2eAgFnPc2d1	kožní
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
bronchitidy	bronchitida	k1gFnPc1	bronchitida
aj.	aj.	kA	aj.
<g/>
Bylina	bylina	k1gFnSc1	bylina
Lippia	Lippia	k1gFnSc1	Lippia
graveoles	graveoles	k1gInSc1	graveoles
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
jako	jako	k8xS	jako
koření	kořenit	k5eAaImIp3nS	kořenit
obdobné	obdobný	k2eAgFnPc4d1	obdobná
oregánu	oregán	k2eAgFnSc4d1	oregán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
duranty	duranta	k1gFnSc2	duranta
(	(	kIx(	(
<g/>
Duranta	Duranta	k1gFnSc1	Duranta
erecta	erecta	k1gMnSc1	erecta
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
D.	D.	kA	D.
plumieri	plumieri	k6eAd1	plumieri
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
alkaloid	alkaloid	k1gInSc4	alkaloid
blízký	blízký	k2eAgInSc4d1	blízký
narkotinu	narkotin	k1gInSc2	narkotin
a	a	k8xC	a
hubící	hubící	k2eAgFnSc2d1	hubící
larvy	larva	k1gFnSc2	larva
komárů	komár	k1gMnPc2	komár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
Mexika	Mexiko	k1gNnSc2	Mexiko
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
plody	plod	k1gInPc1	plod
používají	používat	k5eAaImIp3nP	používat
proti	proti	k7c3	proti
horečce	horečka	k1gFnSc3	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Odvar	odvar	k1gInSc1	odvar
z	z	k7c2	z
listů	list	k1gInPc2	list
libory	libora	k1gFnSc2	libora
Lantana	Lantana	k1gFnSc1	Lantana
armata	armata	k1gFnSc1	armata
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
tradiční	tradiční	k2eAgNnSc4d1	tradiční
léčivo	léčivo	k1gNnSc4	léčivo
při	při	k7c6	při
bolestech	bolest	k1gFnPc6	bolest
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
průjmech	průjem	k1gInPc6	průjem
aj.	aj.	kA	aj.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Okrasné	okrasný	k2eAgFnPc1d1	okrasná
rostliny	rostlina	k1gFnPc1	rostlina
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
okrasné	okrasný	k2eAgFnPc1d1	okrasná
trvalky	trvalka	k1gFnPc1	trvalka
či	či	k8xC	či
skalničky	skalnička	k1gFnPc1	skalnička
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstován	k2eAgInPc4d1	pěstován
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
rodů	rod	k1gInPc2	rod
sporýš	sporýš	k1gInSc1	sporýš
(	(	kIx(	(
<g/>
Verbena	verbena	k1gFnSc1	verbena
<g/>
)	)	kIx)	)
a	a	k8xC	a
Glandularia	Glandularium	k1gNnPc4	Glandularium
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
často	často	k6eAd1	často
prostě	prostě	k6eAd1	prostě
'	'	kIx"	'
<g/>
verbeny	verbena	k1gFnPc1	verbena
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
teplomilných	teplomilný	k2eAgFnPc2d1	teplomilná
okrasných	okrasný	k2eAgFnPc2d1	okrasná
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
zejména	zejména	k9	zejména
duranta	duranta	k1gFnSc1	duranta
(	(	kIx(	(
<g/>
Duranta	Duranta	k1gFnSc1	Duranta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
libora	libora	k1gFnSc1	libora
(	(	kIx(	(
<g/>
Lantana	Lantana	k1gFnSc1	Lantana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
petrea	petrea	k6eAd1	petrea
ovíjivá	ovíjivý	k2eAgFnSc1d1	ovíjivá
(	(	kIx(	(
<g/>
Petrea	Petre	k2eAgFnSc1d1	Petre
volubilis	volubilis	k1gFnSc1	volubilis
<g/>
)	)	kIx)	)
a	a	k8xC	a
vratiklas	vratiklas	k1gInSc1	vratiklas
(	(	kIx(	(
<g/>
Stachytarpheta	Stachytarpheta	k1gFnSc1	Stachytarpheta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Libora	Libora	k1gFnSc1	Libora
měňavá	měňavý	k2eAgFnSc1d1	měňavá
(	(	kIx(	(
<g/>
Lantana	Lantana	k1gFnSc1	Lantana
camara	camara	k1gFnSc1	camara
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
tropické	tropický	k2eAgFnSc2d1	tropická
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
svými	svůj	k3xOyFgInPc7	svůj
květy	květ	k1gInPc7	květ
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
postupně	postupně	k6eAd1	postupně
mění	měnit	k5eAaImIp3nP	měnit
barvu	barva	k1gFnSc4	barva
od	od	k7c2	od
žluté	žlutý	k2eAgFnSc2d1	žlutá
přes	přes	k7c4	přes
červenou	červená	k1gFnSc4	červená
po	po	k7c4	po
fialovorůžovou	fialovorůžový	k2eAgFnSc4d1	fialovorůžová
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
roste	růst	k5eAaImIp3nS	růst
zplaněná	zplaněný	k2eAgFnSc1d1	zplaněná
v	v	k7c6	v
mnohých	mnohý	k2eAgFnPc6d1	mnohá
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
rodů	rod	k1gInPc2	rod
==	==	k?	==
</s>
</p>
<p>
<s>
Acantholippia	Acantholippia	k1gFnSc1	Acantholippia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Aloysia	Aloysia	k1gFnSc1	Aloysia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Baillonia	Baillonium	k1gNnPc1	Baillonium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bouchea	Bouchea	k6eAd1	Bouchea
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Casselia	Casselia	k1gFnSc1	Casselia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Castelia	Castelia	k1gFnSc1	Castelia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Chascanum	Chascanum	k1gInSc1	Chascanum
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Citharexylum	Citharexylum	k1gInSc1	Citharexylum
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Coelocarpum	Coelocarpum	k1gInSc1	Coelocarpum
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Diostea	Diostea	k6eAd1	Diostea
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dipyrena	Dipyrena	k1gFnSc1	Dipyrena
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Duranta	Duranta	k1gFnSc1	Duranta
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Glandularia	Glandularium	k1gNnPc1	Glandularium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hierobotana	Hierobotana	k1gFnSc1	Hierobotana
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Junellia	Junellia	k1gFnSc1	Junellia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lampayo	Lampayo	k6eAd1	Lampayo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lantana	Lantana	k1gFnSc1	Lantana
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Lippia	Lippia	k1gFnSc1	Lippia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Nashia	Nashia	k1gFnSc1	Nashia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Neosparton	Neosparton	k1gInSc1	Neosparton
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Parodianthus	Parodianthus	k1gMnSc1	Parodianthus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Petrea	Petrea	k6eAd1	Petrea
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Phyla	Phyla	k6eAd1	Phyla
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Priva	Priva	k6eAd1	Priva
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Recordia	Recordium	k1gNnPc1	Recordium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Rehdera	Rehdera	k1gFnSc1	Rehdera
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Rhaphithamnus	Rhaphithamnus	k1gMnSc1	Rhaphithamnus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stachytarpheta	Stachytarpheta	k1gFnSc1	Stachytarpheta
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Stylodon	Stylodon	k1gMnSc1	Stylodon
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tamonea	Tamonea	k6eAd1	Tamonea
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Urbania	Urbanium	k1gNnPc1	Urbanium
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Verbena	verbena	k1gFnSc1	verbena
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Verbenoxylum	Verbenoxylum	k1gInSc1	Verbenoxylum
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Xeroaloysia	Xeroaloysia	k1gFnSc1	Xeroaloysia
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Xolocotzia	Xolocotzia	k1gFnSc1	Xolocotzia
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
