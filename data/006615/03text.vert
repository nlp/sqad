<s>
Manikúra	manikúra	k1gFnSc1	manikúra
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
kosmetické	kosmetický	k2eAgFnSc2d1	kosmetická
procedury	procedura	k1gFnSc2	procedura
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kultivaci	kultivace	k1gFnSc3	kultivace
rukou	ruka	k1gFnPc2	ruka
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
předmětem	předmět	k1gInSc7	předmět
je	být	k5eAaImIp3nS	být
čištění	čištění	k1gNnSc1	čištění
nehtů	nehet	k1gInPc2	nehet
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
zarovnání	zarovnání	k1gNnSc4	zarovnání
a	a	k8xC	a
odstranění	odstranění	k1gNnSc4	odstranění
odumřelé	odumřelý	k2eAgNnSc4d1	odumřelé
kůže	kůže	k1gFnSc1	kůže
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Manikúra	manikúra	k1gFnSc1	manikúra
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jak	jak	k6eAd1	jak
z	z	k7c2	z
čistě	čistě	k6eAd1	čistě
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
z	z	k7c2	z
lékařských	lékařský	k2eAgInPc2d1	lékařský
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
nehtů	nehet	k1gInPc2	nehet
nějaká	nějaký	k3yIgFnSc1	nějaký
virová	virový	k2eAgFnSc1d1	virová
<g/>
,	,	kIx,	,
či	či	k8xC	či
plísňová	plísňový	k2eAgFnSc1d1	plísňová
infekce	infekce	k1gFnSc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Manikúra	manikúra	k1gFnSc1	manikúra
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nabízena	nabízen	k2eAgFnSc1d1	nabízena
ve	v	k7c6	v
specializovaných	specializovaný	k2eAgInPc6d1	specializovaný
kosmetických	kosmetický	k2eAgInPc6d1	kosmetický
salónech	salón	k1gInPc6	salón
a	a	k8xC	a
nehtových	nehtový	k2eAgFnPc6d1	nehtová
studiích	studie	k1gFnPc6	studie
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
manikúry	manikúra	k1gFnSc2	manikúra
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
lakování	lakování	k1gNnSc4	lakování
nehtů	nehet	k1gInPc2	nehet
či	či	k8xC	či
aplikace	aplikace	k1gFnSc2	aplikace
umělých	umělý	k2eAgInPc2d1	umělý
nehtů	nehet	k1gInPc2	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
manikúra	manikúra	k1gFnSc1	manikúra
byla	být	k5eAaImAgFnS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
již	již	k6eAd1	již
před	před	k7c7	před
5000	[number]	k4	5000
lety	léto	k1gNnPc7	léto
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
péče	péče	k1gFnSc1	péče
pro	pro	k7c4	pro
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nazívá	nazívat	k5eAaBmIp3nS	nazívat
pedikúra	pedikúra	k1gFnSc1	pedikúra
<g/>
.	.	kIx.	.
</s>
<s>
Pedikúra	pedikúra	k1gFnSc1	pedikúra
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Manikúra	manikúra	k1gFnSc1	manikúra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
