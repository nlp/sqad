<p>
<s>
Pontifikát	pontifikát	k1gInSc1	pontifikát
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
úřad	úřad	k1gInSc1	úřad
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
označuje	označovat	k5eAaImIp3nS	označovat
i	i	k9	i
délku	délka	k1gFnSc4	délka
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
pontifikem	pontifex	k1gMnSc7	pontifex
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
má	mít	k5eAaImIp3nS	mít
proměnlivou	proměnlivý	k2eAgFnSc4d1	proměnlivá
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
každého	každý	k3xTgMnSc2	každý
jednotlivého	jednotlivý	k2eAgMnSc2d1	jednotlivý
Svatého	svatý	k2eAgMnSc2d1	svatý
otce	otec	k1gMnSc2	otec
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příbuzná	příbuzný	k2eAgNnPc1d1	příbuzné
slova	slovo	k1gNnPc1	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pontifex	pontifex	k1gMnSc1	pontifex
===	===	k?	===
</s>
</p>
<p>
<s>
Příbuzné	příbuzný	k2eAgNnSc1d1	příbuzné
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
pontifex	pontifex	k1gMnSc1	pontifex
(	(	kIx(	(
<g/>
pontifik	pontifik	k1gMnSc1	pontifik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
příslušníka	příslušník	k1gMnSc4	příslušník
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
kněžského	kněžský	k2eAgInSc2d1	kněžský
sboru	sbor	k1gInSc2	sbor
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
církvi	církev	k1gFnSc6	církev
<g/>
,	,	kIx,	,
v	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
mezi	mezi	k7c7	mezi
pontifiky	pontifex	k1gMnPc7	pontifex
patří	patřit	k5eAaImIp3nP	patřit
všichni	všechen	k3xTgMnPc1	všechen
biskupové	biskup	k1gMnPc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
synonymum	synonymum	k1gNnSc4	synonymum
pro	pro	k7c4	pro
sousloví	sousloví	k1gNnSc4	sousloví
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
kněz	kněz	k1gMnSc1	kněz
neboli	neboli	k8xC	neboli
velekněz	velekněz	k1gMnSc1	velekněz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pontifikálie	pontifikálie	k1gFnPc4	pontifikálie
===	===	k?	===
</s>
</p>
<p>
<s>
Slovem	slovem	k6eAd1	slovem
pontifikálie	pontifikálie	k1gFnPc4	pontifikálie
pak	pak	k6eAd1	pak
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
praxi	praxe	k1gFnSc6	praxe
označujeme	označovat	k5eAaImIp1nP	označovat
insignie	insignie	k1gFnPc4	insignie
velekněze	velekněz	k1gMnSc2	velekněz
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
biskupů	biskup	k1gMnPc2	biskup
včetně	včetně	k7c2	včetně
papeže	papež	k1gMnSc2	papež
(	(	kIx(	(
<g/>
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
mitra	mitra	k1gFnSc1	mitra
<g/>
,	,	kIx,	,
berla	berla	k1gFnSc1	berla
<g/>
,	,	kIx,	,
pektorál	pektorál	k1gInSc4	pektorál
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
ještě	ještě	k9	ještě
rukavice	rukavice	k1gFnSc2	rukavice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
také	také	k9	také
některých	některý	k3yIgMnPc2	některý
dalších	další	k2eAgMnPc2d1	další
význačných	význačný	k2eAgMnPc2d1	význačný
hodnostářů	hodnostář	k1gMnPc2	hodnostář
(	(	kIx(	(
<g/>
proboštů	probošt	k1gMnPc2	probošt
<g/>
,	,	kIx,	,
arciděkanů	arciděkan	k1gMnPc2	arciděkan
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
provádí	provádět	k5eAaImIp3nS	provádět
biskup	biskup	k1gMnSc1	biskup
během	během	k7c2	během
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
