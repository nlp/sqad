<s>
Pontifikát	pontifikát	k1gInSc1	pontifikát
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
úřad	úřad	k1gInSc1	úřad
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
označuje	označovat	k5eAaImIp3nS	označovat
i	i	k9	i
délku	délka	k1gFnSc4	délka
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
pontifikem	pontifex	k1gMnSc7	pontifex
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
