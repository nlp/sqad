<s>
První	první	k4xOgMnSc1	první
čínský	čínský	k2eAgMnSc1d1	čínský
císař	císař	k1gMnSc1	císař
Čchin	Čchin	k1gMnSc1	Čchin
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-chuang-ti	huang	k5eAaPmF	-chuang-t
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
pyramidální	pyramidální	k2eAgFnSc4d1	pyramidální
hrobku	hrobka	k1gFnSc4	hrobka
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
Si-an	Siny	k1gInPc2	Si-any
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
