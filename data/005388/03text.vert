<s>
Pyramida	pyramida	k1gFnSc1	pyramida
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
π	π	k?	π
<g/>
,	,	kIx,	,
pyramis	pyramis	k1gFnSc1	pyramis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jehlanovitá	jehlanovitý	k2eAgFnSc1d1	jehlanovitá
stavba	stavba	k1gFnSc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
pyramid	pyramida	k1gFnPc2	pyramida
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
nebo	nebo	k8xC	nebo
trojúhelník	trojúhelník	k1gInSc1	trojúhelník
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
to	ten	k3xDgNnSc1	ten
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
polygon	polygon	k1gInSc4	polygon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pyramida	pyramida	k1gFnSc1	pyramida
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
tři	tři	k4xCgFnPc1	tři
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgFnPc1	čtyři
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
strany	strana	k1gFnPc1	strana
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
trojúhelníkové	trojúhelníkový	k2eAgFnPc1d1	trojúhelníková
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukčně	konstrukčně	k6eAd1	konstrukčně
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
je	být	k5eAaImIp3nS	být
udělat	udělat	k5eAaPmF	udělat
pyramidu	pyramida	k1gFnSc4	pyramida
na	na	k7c6	na
čtvercové	čtvercový	k2eAgFnSc6d1	čtvercová
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
přibližně	přibližně	k6eAd1	přibližně
čtvercové	čtvercový	k2eAgFnSc6d1	čtvercová
<g/>
)	)	kIx)	)
základně	základna	k1gFnSc6	základna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nevzniká	vznikat	k5eNaImIp3nS	vznikat
problém	problém	k1gInSc1	problém
spojit	spojit	k5eAaPmF	spojit
stěny	stěn	k1gInPc4	stěn
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
má	mít	k5eAaImIp3nS	mít
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
starověkých	starověký	k2eAgFnPc2d1	starověká
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pyramidách	pyramida	k1gFnPc6	pyramida
se	se	k3xPyFc4	se
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
bludiště	bludiště	k1gNnSc4	bludiště
proti	proti	k7c3	proti
zlodějům	zloděj	k1gMnPc3	zloděj
hrobek	hrobka	k1gFnPc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
egyptské	egyptský	k2eAgFnPc1d1	egyptská
pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
,	,	kIx,	,
núbijské	núbijský	k2eAgFnPc1d1	núbijská
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
od	od	k7c2	od
egyptských	egyptský	k2eAgNnPc2d1	egyptské
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
čínské	čínský	k2eAgFnPc1d1	čínská
<g/>
,	,	kIx,	,
mezoamerické	mezoamerický	k2eAgFnPc1d1	mezoamerická
<g/>
,	,	kIx,	,
andské	andský	k2eAgFnPc1d1	andská
<g/>
,	,	kIx,	,
francouzské	francouzský	k2eAgFnPc1d1	francouzská
<g/>
,	,	kIx,	,
Cestiova	Cestiův	k2eAgFnSc1d1	Cestiova
pyramida	pyramida	k1gFnSc1	pyramida
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
egyptské	egyptský	k2eAgFnSc2d1	egyptská
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Pyramidy	pyramida	k1gFnPc1	pyramida
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgInPc1d3	nejznámější
a	a	k8xC	a
nejstarší	starý	k2eAgInPc1d3	nejstarší
z	z	k7c2	z
pyramidových	pyramidový	k2eAgFnPc2d1	pyramidová
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
stavbám	stavba	k1gFnPc3	stavba
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Cheopsova	Cheopsův	k2eAgFnSc1d1	Cheopsova
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
sedm	sedm	k4xCc4	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
až	až	k6eAd1	až
do	do	k7c2	do
dostavby	dostavba	k1gFnSc2	dostavba
Washingtonova	Washingtonův	k2eAgInSc2d1	Washingtonův
monumentu	monument	k1gInSc2	monument
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měří	měřit	k5eAaImIp3nS	měřit
169	[number]	k4	169
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Egyptské	egyptský	k2eAgInPc1d1	egyptský
pyramidy	pyramid	k1gInPc1	pyramid
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
z	z	k7c2	z
velkých	velký	k2eAgInPc2d1	velký
kamenných	kamenný	k2eAgInPc2d1	kamenný
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
pyramid	pyramida	k1gFnPc2	pyramida
probíhala	probíhat	k5eAaImAgFnS	probíhat
bez	bez	k7c2	bez
znalosti	znalost	k1gFnSc2	znalost
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
složitějších	složitý	k2eAgFnPc2d2	složitější
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
pyramida	pyramida	k1gFnSc1	pyramida
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
III	III	kA	III
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
pak	pak	k6eAd1	pak
z	z	k7c2	z
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
uváděno	uváděn	k2eAgNnSc4d1	uváděno
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc1	dynastie
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2700	[number]	k4	2700
a	a	k8xC	a
1700	[number]	k4	1700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Přestože	přestože	k8xS	přestože
o	o	k7c6	o
účelu	účel	k1gInSc6	účel
i	i	k8xC	i
vzniku	vznik	k1gInSc6	vznik
egyptských	egyptský	k2eAgFnPc2d1	egyptská
pyramid	pyramida	k1gFnPc2	pyramida
koluje	kolovat	k5eAaImIp3nS	kolovat
řada	řada	k1gFnSc1	řada
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
za	za	k7c4	za
nevědecké	vědecký	k2eNgNnSc4d1	nevědecké
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
vědecká	vědecký	k2eAgFnSc1d1	vědecká
obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
otázce	otázka	k1gFnSc6	otázka
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
<g/>
,	,	kIx,	,
panuje	panovat	k5eAaImIp3nS	panovat
obecně	obecně	k6eAd1	obecně
přijímaná	přijímaný	k2eAgFnSc1d1	přijímaná
teorie	teorie	k1gFnSc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
pyramidy	pyramida	k1gFnPc1	pyramida
sloužily	sloužit	k5eAaImAgFnP	sloužit
za	za	k7c4	za
hroby	hrob	k1gInPc4	hrob
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
pyramid	pyramida	k1gFnPc2	pyramida
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
dalších	další	k2eAgFnPc2d1	další
<g/>
,	,	kIx,	,
menších	malý	k2eAgFnPc2d2	menší
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
hroby	hrob	k1gInPc1	hrob
manželek	manželka	k1gFnPc2	manželka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
staveb	stavba	k1gFnPc2	stavba
sloužily	sloužit	k5eAaImAgFnP	sloužit
k	k	k7c3	k
náboženským	náboženský	k2eAgInPc3d1	náboženský
účelům	účel	k1gInPc3	účel
<g/>
,	,	kIx,	,
egyptologové	egyptolog	k1gMnPc1	egyptolog
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
že	že	k8xS	že
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
přebývala	přebývat	k5eAaImAgFnS	přebývat
králova	králův	k2eAgFnSc1d1	králova
Ba	ba	k9	ba
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
králové	král	k1gMnPc1	král
si	se	k3xPyFc3	se
nechali	nechat	k5eAaPmAgMnP	nechat
postavit	postavit	k5eAaPmF	postavit
dvě	dva	k4xCgFnPc1	dva
pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bývá	bývat	k5eAaImIp3nS	bývat
vysvětlováno	vysvětlován	k2eAgNnSc1d1	vysvětlováno
jako	jako	k9	jako
projev	projev	k1gInSc4	projev
egyptského	egyptský	k2eAgInSc2d1	egyptský
dualismu	dualismus	k1gInSc2	dualismus
(	(	kIx(	(
<g/>
příslušní	příslušný	k2eAgMnPc1d1	příslušný
faraoni	faraon	k1gMnPc1	faraon
byli	být	k5eAaImAgMnP	být
králové	králová	k1gFnPc4	králová
Horního	horní	k2eAgMnSc2d1	horní
i	i	k8xC	i
Dolního	dolní	k2eAgInSc2d1	dolní
Egypta	Egypt	k1gInSc2	Egypt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sice	sice	k8xC	sice
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
hrobkou	hrobka	k1gFnSc7	hrobka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
pohřbít	pohřbít	k5eAaPmF	pohřbít
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Egyptologové	egyptolog	k1gMnPc1	egyptolog
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pyramidy	pyramida	k1gFnPc1	pyramida
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
mastab	mastaba	k1gFnPc2	mastaba
a	a	k8xC	a
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
egyptským	egyptský	k2eAgInSc7d1	egyptský
vynálezem	vynález	k1gInSc7	vynález
<g/>
,	,	kIx,	,
neovlivněným	ovlivněný	k2eNgInSc7d1	neovlivněný
cizími	cizí	k2eAgInPc7d1	cizí
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
núbijské	núbijský	k2eAgFnSc2d1	núbijská
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Núbijské	núbijský	k2eAgInPc1d1	núbijský
pyramidy	pyramid	k1gInPc1	pyramid
byly	být	k5eAaImAgInP	být
postaveny	postaven	k2eAgMnPc4d1	postaven
panovníky	panovník	k1gMnPc4	panovník
říše	říš	k1gFnSc2	říš
Kuš	kuš	k0	kuš
na	na	k7c6	na
třech	tři	k4xCgNnPc6	tři
místech	místo	k1gNnPc6	místo
současného	současný	k2eAgInSc2d1	současný
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pyramidy	pyramid	k1gInPc1	pyramid
jsou	být	k5eAaImIp3nP	být
ovlivněny	ovlivnit	k5eAaPmNgInP	ovlivnit
egyptskými	egyptský	k2eAgFnPc7d1	egyptská
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
strmější	strmý	k2eAgFnPc1d2	strmější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Núbii	Núbie	k1gFnSc6	Núbie
byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
300	[number]	k4	300
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
asi	asi	k9	asi
220	[number]	k4	220
takových	takový	k3xDgFnPc2	takový
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
pyramidy	pyramida	k1gFnPc1	pyramida
nejsou	být	k5eNaImIp3nP	být
hroby	hrob	k1gInPc4	hrob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
pomníkem	pomník	k1gInSc7	pomník
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
mezoamerické	mezoamerický	k2eAgFnSc2d1	mezoamerická
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Pyramidy	pyramida	k1gFnPc1	pyramida
v	v	k7c6	v
Mezoamerice	Mezoamerika	k1gFnSc6	Mezoamerika
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznačnějším	význačný	k2eAgInPc3d3	nejvýznačnější
pozůstatkům	pozůstatek	k1gInPc3	pozůstatek
starověkých	starověký	k2eAgFnPc2d1	starověká
mezoamerických	mezoamerický	k2eAgFnPc2d1	mezoamerická
civilizací	civilizace	k1gFnPc2	civilizace
<g/>
,	,	kIx,	,
především	především	k9	především
Toltéků	Toltéek	k1gMnPc2	Toltéek
<g/>
,	,	kIx,	,
Mayů	May	k1gMnPc2	May
a	a	k8xC	a
Aztéků	Azték	k1gMnPc2	Azték
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
sestaveny	sestavit	k5eAaPmNgInP	sestavit
z	z	k7c2	z
kamenných	kamenný	k2eAgInPc2d1	kamenný
kvádrů	kvádr	k1gInPc2	kvádr
mnohem	mnohem	k6eAd1	mnohem
menších	malý	k2eAgInPc2d2	menší
než	než	k8xS	než
egyptské	egyptský	k2eAgInPc1d1	egyptský
pyramidy	pyramid	k1gInPc1	pyramid
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
uneslo	unést	k5eAaPmAgNnS	unést
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kvádry	kvádr	k1gInPc1	kvádr
byly	být	k5eAaImAgInP	být
spojovány	spojovat	k5eAaImNgInP	spojovat
pomocí	pomocí	k7c2	pomocí
cementové	cementový	k2eAgFnSc2d1	cementová
malty	malta	k1gFnSc2	malta
<g/>
.	.	kIx.	.
</s>
<s>
Stavitelé	stavitel	k1gMnPc1	stavitel
neznali	neznat	k5eAaImAgMnP	neznat
železné	železný	k2eAgInPc4d1	železný
nástroje	nástroj	k1gInPc4	nástroj
ani	ani	k8xC	ani
kolo	kolo	k1gNnSc4	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
čínské	čínský	k2eAgFnSc2d1	čínská
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
čínský	čínský	k2eAgMnSc1d1	čínský
císař	císař	k1gMnSc1	císař
Čchin	Čchin	k1gMnSc1	Čchin
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-chuang-ti	huang	k5eAaBmF	-chuang-t
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
vybudovat	vybudovat	k5eAaPmF	vybudovat
pyramidální	pyramidální	k2eAgFnSc4d1	pyramidální
hrobku	hrobka	k1gFnSc4	hrobka
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
Si-an	Siny	k1gInPc2	Si-any
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
egyptské	egyptský	k2eAgFnSc2d1	egyptská
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
především	především	k9	především
z	z	k7c2	z
navezené	navezený	k2eAgFnSc2d1	navezená
a	a	k8xC	a
upěchované	upěchovaný	k2eAgFnSc2d1	upěchovaná
zeminy	zemina	k1gFnSc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pyramidou	pyramida	k1gFnSc7	pyramida
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nacházet	nacházet	k5eAaImF	nacházet
pohřební	pohřební	k2eAgInSc1d1	pohřební
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
archeologickému	archeologický	k2eAgInSc3d1	archeologický
průzkumu	průzkum	k1gInSc3	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
podobných	podobný	k2eAgFnPc2d1	podobná
pyramid	pyramida	k1gFnPc2	pyramida
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
s	s	k7c7	s
komolým	komolý	k2eAgNnSc7d1	komolý
zakončením	zakončení	k1gNnSc7	zakončení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
hrobek	hrobek	k1gInSc1	hrobek
císařské	císařský	k2eAgFnSc2d1	císařská
dynastie	dynastie	k1gFnSc2	dynastie
Západní	západní	k2eAgMnPc1d1	západní
Chan	Chan	k1gMnSc1	Chan
a	a	k8xC	a
vysokých	vysoký	k2eAgMnPc2d1	vysoký
státních	státní	k2eAgMnPc2d1	státní
funkcionářů	funkcionář	k1gMnPc2	funkcionář
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
linii	linie	k1gFnSc4	linie
oproti	oproti	k7c3	oproti
mongolským	mongolský	k2eAgMnPc3d1	mongolský
nájezdníkům	nájezdník	k1gMnPc3	nájezdník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
existence	existence	k1gFnSc2	existence
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
postaveno	postavit	k5eAaPmNgNnS	postavit
několik	několik	k4yIc1	několik
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejznámější	známý	k2eAgMnPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
existující	existující	k2eAgFnSc1d1	existující
Cestiova	Cestiův	k2eAgFnSc1d1	Cestiova
pyramida	pyramida	k1gFnSc1	pyramida
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
27	[number]	k4	27
metrů	metr	k1gInPc2	metr
vysokou	vysoký	k2eAgFnSc4d1	vysoká
stavbu	stavba	k1gFnSc4	stavba
z	z	k7c2	z
konce	konec	k1gInSc2	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Další	další	k2eAgFnSc1d1	další
římská	římský	k2eAgFnSc1d1	římská
pyramida	pyramida	k1gFnSc1	pyramida
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
ve	v	k7c6	v
Faliconu	Falicon	k1gInSc6	Falicon
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
pyramida	pyramida	k1gFnSc1	pyramida
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
Meta	meta	k1gFnSc1	meta
Romuli	Romule	k1gFnSc4	Romule
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
koncem	koncem	k7c2	koncem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pyramida	pyramida	k1gFnSc1	pyramida
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
tvarem	tvar	k1gInSc7	tvar
i	i	k9	i
pro	pro	k7c4	pro
novověkou	novověký	k2eAgFnSc4d1	novověká
a	a	k8xC	a
moderní	moderní	k2eAgFnSc4d1	moderní
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
příklady	příklad	k1gInPc4	příklad
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
následující	následující	k2eAgFnSc1d1	následující
galerie	galerie	k1gFnSc1	galerie
(	(	kIx(	(
<g/>
chronologicky	chronologicky	k6eAd1	chronologicky
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Kosack	Kosack	k1gMnSc1	Kosack
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
altägyptischen	altägyptischet	k5eAaImNgMnS	altägyptischet
Pyramidentexte	Pyramidentext	k1gMnSc5	Pyramidentext
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
neuer	neuer	k1gInSc1	neuer
deutscher	deutschra	k1gFnPc2	deutschra
Uebersetzung	Uebersetzung	k1gInSc1	Uebersetzung
<g/>
;	;	kIx,	;
vollständig	vollständig	k1gInSc1	vollständig
bearbeitet	bearbeitet	k1gInSc1	bearbeitet
und	und	k?	und
herausgegeben	herausgegeben	k2eAgInSc1d1	herausgegeben
von	von	k1gInSc1	von
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Kosack	Kosack	k1gMnSc1	Kosack
Christoph	Christoph	k1gMnSc1	Christoph
Brunner	Brunner	k1gMnSc1	Brunner
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
9524018	[number]	k4	9524018
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Egyptské	egyptský	k2eAgFnSc2d1	egyptská
pyramidy	pyramida	k1gFnSc2	pyramida
Pyramidové	pyramidový	k2eAgNnSc4d1	pyramidové
číslo	číslo	k1gNnSc4	číslo
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pyramida	pyramida	k1gFnSc1	pyramida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
