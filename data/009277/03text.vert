<p>
<s>
Tommy	Tomm	k1gInPc1	Tomm
Dorsey	Dorsea	k1gFnSc2	Dorsea
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Shenandoah	Shenandoah	k1gInSc1	Shenandoah
<g/>
,	,	kIx,	,
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
Greenwich	Greenwich	k1gInSc1	Greenwich
<g/>
,	,	kIx,	,
Connecticut	Connecticut	k1gMnSc1	Connecticut
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
jazzový	jazzový	k2eAgMnSc1d1	jazzový
trombónista	trombónista	k1gMnSc1	trombónista
<g/>
,	,	kIx,	,
trumpetista	trumpetista	k1gMnSc1	trumpetista
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
big	big	k?	big
band	band	k1gInSc1	band
éry	éra	k1gFnSc2	éra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
přezdívky	přezdívka	k1gFnPc4	přezdívka
patří	patřit	k5eAaImIp3nP	patřit
"	"	kIx"	"
<g/>
The	The	k1gMnPc1	The
Sentimental	Sentimental	k1gMnSc1	Sentimental
Gentleman	gentleman	k1gMnSc1	gentleman
of	of	k?	of
Swing	swing	k1gInSc1	swing
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
TD	TD	kA	TD
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
staršího	starý	k2eAgMnSc4d2	starší
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
Jimmy	Jimm	k1gMnPc4	Jimm
Dorseyho	Dorsey	k1gMnSc4	Dorsey
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
sentimentální	sentimentální	k2eAgInSc1d1	sentimentální
trombonový	trombonový	k2eAgInSc1d1	trombonový
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
typickým	typický	k2eAgInSc7d1	typický
soundem	sound	k1gInSc7	sound
ve	v	k7c6	v
swingové	swingový	k2eAgFnSc6d1	swingová
éře	éra	k1gFnSc6	éra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
americká	americký	k2eAgFnSc1d1	americká
národní	národní	k2eAgFnSc1d1	národní
pošta	pošta	k1gFnSc1	pošta
nechala	nechat	k5eAaPmAgFnS	nechat
vytisknout	vytisknout	k5eAaPmF	vytisknout
poštovní	poštovní	k2eAgFnPc4d1	poštovní
známky	známka	k1gFnPc4	známka
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
podobiznou	podobizna	k1gFnSc7	podobizna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Oženil	oženit	k5eAaPmAgInS	oženit
se	se	k3xPyFc4	se
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmnácti	sedmnáct	k4xCc2	sedmnáct
let	léto	k1gNnPc2	léto
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
šestnáctiletou	šestnáctiletý	k2eAgFnSc7d1	šestnáctiletá
Mildred	Mildred	k1gMnSc1	Mildred
Kraft	Kraft	k2eAgInSc1d1	Kraft
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
Jane	Jan	k1gMnSc5	Jan
Carl	Carl	k1gMnSc1	Carl
New	New	k1gMnPc3	New
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
přečkal	přečkat	k5eAaPmAgMnS	přečkat
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
51	[number]	k4	51
let	léto	k1gNnPc2	léto
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Udusil	udusit	k5eAaPmAgInS	udusit
se	se	k3xPyFc4	se
těžkým	těžký	k2eAgNnSc7d1	těžké
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
měl	mít	k5eAaImAgInS	mít
před	před	k7c7	před
spaním	spaní	k1gNnSc7	spaní
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
si	se	k3xPyFc3	se
ještě	ještě	k9	ještě
vzal	vzít	k5eAaPmAgMnS	vzít
prášky	prášek	k1gInPc4	prášek
na	na	k7c4	na
spaní	spaní	k1gNnSc4	spaní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
negativně	negativně	k6eAd1	negativně
podílelo	podílet	k5eAaImAgNnS	podílet
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
Kategorie	kategorie	k1gFnPc1	kategorie
<g/>
:	:	kIx,	:
<g/>
Úmrtí	úmrtí	k1gNnSc1	úmrtí
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
(	(	kIx(	(
<g/>
Connecticut	Connecticut	k2eAgMnSc1d1	Connecticut
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
</s>
</p>
