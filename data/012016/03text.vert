<p>
<s>
Tábor	Tábor	k1gInSc1	Tábor
smůly	smůla	k1gFnSc2	smůla
je	být	k5eAaImIp3nS	být
humoristická	humoristický	k2eAgFnSc1d1	humoristická
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
soubor	soubor	k1gInSc4	soubor
volně	volně	k6eAd1	volně
provázaných	provázaný	k2eAgFnPc2d1	provázaná
zábavných	zábavný	k2eAgFnPc2d1	zábavná
historek	historka	k1gFnPc2	historka
ze	z	k7c2	z
života	život	k1gInSc2	život
skautského	skautský	k2eAgInSc2d1	skautský
oddílu	oddíl	k1gInSc2	oddíl
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc1d1	doplněný
o	o	k7c4	o
autorovy	autorův	k2eAgInPc4d1	autorův
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
dětského	dětský	k2eAgInSc2d1	dětský
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Psán	psán	k2eAgMnSc1d1	psán
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ich-formě	ichorma	k1gFnSc6	ich-forma
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
skautského	skautský	k2eAgMnSc2d1	skautský
vedoucího	vedoucí	k1gMnSc2	vedoucí
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
samotného	samotný	k2eAgMnSc4d1	samotný
Foglara	Foglar	k1gMnSc4	Foglar
(	(	kIx(	(
<g/>
Jestřába	jestřáb	k1gMnSc4	jestřáb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
některé	některý	k3yIgInPc4	některý
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Foglar	Foglar	k1gInSc1	Foglar
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
komiksu	komiks	k1gInSc6	komiks
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
(	(	kIx(	(
<g/>
historka	historka	k1gFnSc1	historka
se	s	k7c7	s
strašením	strašení	k1gNnSc7	strašení
v	v	k7c6	v
klubovně	klubovna	k1gFnSc6	klubovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
