<s>
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Spencer	Spencer	k1gMnSc1	Spencer
Chaplin	Chaplin	k2eAgMnSc1d1	Chaplin
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1889	[number]	k4	1889
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1977	[number]	k4	1977
Corsier-sur-Vevey	Corsierur-Vevea	k1gFnSc2	Corsier-sur-Vevea
<g/>
,	,	kIx,	,
kanton	kanton	k1gInSc4	kanton
Vaud	Vauda	k1gFnPc2	Vauda
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
světovým	světový	k2eAgMnPc3d1	světový
filmovým	filmový	k2eAgMnPc3d1	filmový
tvůrcům	tvůrce	k1gMnPc3	tvůrce
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
