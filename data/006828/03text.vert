<s>
Kofein	kofein	k1gInSc1	kofein
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
rostliny	rostlina	k1gFnSc2	rostlina
Coffea	Coffeus	k1gMnSc2	Coffeus
arabica	arabicus	k1gMnSc2	arabicus
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
kávovník	kávovník	k1gInSc1	kávovník
arabský	arabský	k2eAgInSc1d1	arabský
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alkaloid	alkaloid	k1gInSc1	alkaloid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
příznivě	příznivě	k6eAd1	příznivě
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
centrální	centrální	k2eAgFnSc4d1	centrální
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
stimulant	stimulant	k1gInSc1	stimulant
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
užíváním	užívání	k1gNnSc7	užívání
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
stává	stávat	k5eAaImIp3nS	stávat
drogou	droga	k1gFnSc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
purinových	purinový	k2eAgInPc2d1	purinový
<g/>
,	,	kIx,	,
methylových	methylový	k2eAgInPc2d1	methylový
derivátů	derivát	k1gInPc2	derivát
xanthinu	xanthin	k1gInSc2	xanthin
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
theobromin	theobromin	k1gInSc4	theobromin
(	(	kIx(	(
<g/>
kakao	kakao	k1gNnSc4	kakao
<g/>
)	)	kIx)	)
a	a	k8xC	a
theofylin	theofylin	k1gInSc1	theofylin
(	(	kIx(	(
<g/>
bronchodilatans	bronchodilatans	k1gInSc1	bronchodilatans
<g/>
,	,	kIx,	,
látka	látka	k1gFnSc1	látka
uvolňující	uvolňující	k2eAgFnSc1d1	uvolňující
průduškové	průduškový	k2eAgNnSc4d1	průduškové
svalstvo	svalstvo	k1gNnSc4	svalstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
hořká	hořký	k2eAgFnSc1d1	hořká
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
–	–	k?	–
xanthinový	xanthinový	k2eAgInSc4d1	xanthinový
alkaloid	alkaloid	k1gInSc4	alkaloid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
psychoaktivní	psychoaktivní	k2eAgFnSc7d1	psychoaktivní
stimulační	stimulační	k2eAgFnSc7d1	stimulační
drogou	droga	k1gFnSc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
německým	německý	k2eAgMnSc7d1	německý
chemikem	chemik	k1gMnSc7	chemik
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Rungem	Rung	k1gMnSc7	Rung
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
látku	látka	k1gFnSc4	látka
jméno	jméno	k1gNnSc4	jméno
kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
aktivní	aktivní	k2eAgFnSc1d1	aktivní
složka	složka	k1gFnSc1	složka
kávy	káva	k1gFnSc2	káva
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
caffeine	caffeinout	k5eAaPmIp3nS	caffeinout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
také	také	k9	také
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
chemické	chemický	k2eAgFnSc6d1	chemická
sloučenině	sloučenina	k1gFnSc6	sloučenina
a	a	k8xC	a
nerozpustném	rozpustný	k2eNgInSc6d1	nerozpustný
komplexu	komplex	k1gInSc6	komplex
guaraninu	guaranina	k1gFnSc4	guaranina
<g/>
,	,	kIx,	,
obsaženém	obsažený	k2eAgInSc6d1	obsažený
v	v	k7c6	v
rostlině	rostlina	k1gFnSc6	rostlina
guarana	guaran	k1gMnSc2	guaran
<g/>
,	,	kIx,	,
v	v	k7c6	v
mateinu	mateino	k1gNnSc6	mateino
obsaženém	obsažený	k2eAgNnSc6d1	obsažené
v	v	k7c6	v
maté	maté	k1gNnSc6	maté
a	a	k8xC	a
v	v	k7c6	v
theinu	thein	k1gInSc6	thein
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
theinem	thein	k1gInSc7	thein
a	a	k8xC	a
kofeinem	kofein	k1gInSc7	kofein
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
výskytu	výskyt	k1gInSc2	výskyt
<g/>
,	,	kIx,	,
kofein	kofein	k1gInSc1	kofein
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
kávě	káva	k1gFnSc6	káva
v	v	k7c6	v
nevázané	vázaný	k2eNgFnSc6d1	nevázaná
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tzv.	tzv.	kA	tzv.
thein	thein	k1gInSc1	thein
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
čajích	čaj	k1gInPc6	čaj
vázaný	vázaný	k2eAgMnSc1d1	vázaný
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
solí	sůl	k1gFnPc2	sůl
přírodních	přírodní	k2eAgFnPc2d1	přírodní
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Guarana	Guarana	k1gFnSc1	Guarana
<g/>
,	,	kIx,	,
mate	mást	k5eAaImIp3nS	mást
i	i	k9	i
čaj	čaj	k1gInSc4	čaj
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
skrývají	skrývat	k5eAaImIp3nP	skrývat
další	další	k2eAgInPc4d1	další
alkaloidy	alkaloid	k1gInPc4	alkaloid
jako	jako	k8xC	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
kardiostimulans	kardiostimulans	k6eAd1	kardiostimulans
theofyllin	theofyllin	k1gInSc1	theofyllin
a	a	k8xC	a
theobromin	theobromin	k1gInSc1	theobromin
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jako	jako	k8xC	jako
polyfenoly	polyfenol	k1gInPc1	polyfenol
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
nerozpustné	rozpustný	k2eNgInPc4d1	nerozpustný
komplexy	komplex	k1gInPc4	komplex
s	s	k7c7	s
kofeinem	kofein	k1gInSc7	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
se	se	k3xPyFc4	se
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
množství	množství	k1gNnSc6	množství
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
ve	v	k7c6	v
fazolích	fazole	k1gFnPc6	fazole
<g/>
,	,	kIx,	,
listech	list	k1gInPc6	list
a	a	k8xC	a
plodech	plod	k1gInPc6	plod
některých	některý	k3yIgFnPc2	některý
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
rostlině	rostlina	k1gFnSc3	rostlina
jako	jako	k8xC	jako
přirozený	přirozený	k2eAgInSc1d1	přirozený
pesticid	pesticid	k1gInSc1	pesticid
–	–	k?	–
paralyzuje	paralyzovat	k5eAaBmIp3nS	paralyzovat
a	a	k8xC	a
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
určitý	určitý	k2eAgInSc4d1	určitý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
částmi	část	k1gFnPc7	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
setkáváme	setkávat	k5eAaImIp1nP	setkávat
v	v	k7c6	v
kávových	kávový	k2eAgInPc6d1	kávový
bobech	bob	k1gInPc6	bob
a	a	k8xC	a
listech	list	k1gInPc6	list
čajovníku	čajovník	k1gInSc2	čajovník
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
produktech	produkt	k1gInPc6	produkt
vyrobených	vyrobený	k2eAgInPc6d1	vyrobený
z	z	k7c2	z
ořechu	ořech	k1gInSc2	ořech
koly	kola	k1gFnSc2	kola
<g/>
.	.	kIx.	.
</s>
<s>
Ostatními	ostatní	k2eAgInPc7d1	ostatní
zdroji	zdroj	k1gInPc7	zdroj
jsou	být	k5eAaImIp3nP	být
yerba	yerba	k1gMnSc1	yerba
mate	mást	k5eAaImIp3nS	mást
<g/>
,	,	kIx,	,
bobule	bobule	k1gFnSc1	bobule
guarany	guarana	k1gFnSc2	guarana
a	a	k8xC	a
Yaupon	Yaupon	k1gNnSc1	Yaupon
holly	holla	k1gFnSc2	holla
(	(	kIx(	(
<g/>
keř	keř	k1gInSc1	keř
podobný	podobný	k2eAgInSc1d1	podobný
brusince	brusinka	k1gFnSc3	brusinka
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgFnSc6d1	rostoucí
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Illex	Illex	k1gInSc1	Illex
vomitoria	vomitorium	k1gNnSc2	vomitorium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
organismu	organismus	k1gInSc6	organismus
funguje	fungovat	k5eAaImIp3nS	fungovat
kofein	kofein	k1gInSc1	kofein
jako	jako	k8xS	jako
stimulátor	stimulátor	k1gInSc1	stimulátor
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
CNS	CNS	kA	CNS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dočasně	dočasně	k6eAd1	dočasně
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
únavu	únava	k1gFnSc4	únava
a	a	k8xC	a
probouzí	probouzet	k5eAaImIp3nS	probouzet
bdělost	bdělost	k1gFnSc1	bdělost
<g/>
.	.	kIx.	.
</s>
<s>
Nápoje	nápoj	k1gInPc4	nápoj
obsahující	obsahující	k2eAgInPc4d1	obsahující
kofein	kofein	k1gInSc4	kofein
jako	jako	k9	jako
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
limonády	limonáda	k1gFnPc1	limonáda
a	a	k8xC	a
energetické	energetický	k2eAgInPc1d1	energetický
nápoje	nápoj	k1gInPc1	nápoj
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nP	těšit
velké	velký	k2eAgFnSc3d1	velká
oblibě	obliba	k1gFnSc3	obliba
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejpopulárnější	populární	k2eAgFnSc4d3	nejpopulárnější
psychoaktivní	psychoaktivní	k2eAgFnSc4d1	psychoaktivní
látku	látka	k1gFnSc4	látka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
je	být	k5eAaImIp3nS	být
legální	legální	k2eAgInSc1d1	legální
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
prodej	prodej	k1gInSc1	prodej
se	se	k3xPyFc4	se
neřídí	řídit	k5eNaImIp3nS	řídit
žádnými	žádný	k3yNgInPc7	žádný
omezeními	omezení	k1gNnPc7	omezení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
kofein	kofein	k1gInSc1	kofein
90	[number]	k4	90
%	%	kIx~	%
dospělé	dospělý	k2eAgFnSc2d1	dospělá
populace	populace	k1gFnSc2	populace
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
americké	americký	k2eAgFnSc2d1	americká
Správy	správa	k1gFnSc2	správa
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
léčiv	léčivo	k1gNnPc2	léčivo
FDA	FDA	kA	FDA
(	(	kIx(	(
<g/>
americká	americký	k2eAgFnSc1d1	americká
obdoba	obdoba	k1gFnSc1	obdoba
českého	český	k2eAgInSc2d1	český
Státního	státní	k2eAgInSc2d1	státní
ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
registrován	registrován	k2eAgMnSc1d1	registrován
jako	jako	k8xC	jako
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
má	mít	k5eAaImIp3nS	mít
diuretické	diuretický	k2eAgInPc4d1	diuretický
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
toleranci	tolerance	k1gFnSc4	tolerance
častým	častý	k2eAgNnSc7d1	časté
užíváním	užívání	k1gNnSc7	užívání
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pravidelných	pravidelný	k2eAgMnPc2d1	pravidelný
uživatelů	uživatel	k1gMnPc2	uživatel
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
tolerance	tolerance	k1gFnSc1	tolerance
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účinku	účinek	k1gInSc3	účinek
a	a	k8xC	a
studie	studie	k1gFnSc1	studie
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
při	při	k7c6	při
pravidelné	pravidelný	k2eAgFnSc6d1	pravidelná
konzumaci	konzumace	k1gFnSc6	konzumace
vedlo	vést	k5eAaImAgNnS	vést
pití	pití	k1gNnSc4	pití
nápojů	nápoj	k1gInPc2	nápoj
obsahujících	obsahující	k2eAgInPc2d1	obsahující
kofein	kofein	k1gInSc4	kofein
k	k	k7c3	k
dehydrataci	dehydratace	k1gFnSc3	dehydratace
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
hebký	hebký	k2eAgInSc1d1	hebký
prášek	prášek	k1gInSc1	prášek
nebo	nebo	k8xC	nebo
lesklé	lesklý	k2eAgFnSc2d1	lesklá
jehličky	jehlička	k1gFnSc2	jehlička
<g/>
,	,	kIx,	,
hořké	hořký	k2eAgFnSc2d1	hořká
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Purinový	purinový	k2eAgInSc1d1	purinový
derivát	derivát	k1gInSc1	derivát
kofein	kofein	k1gInSc1	kofein
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
semenech	semeno	k1gNnPc6	semeno
a	a	k8xC	a
plodech	plod	k1gInPc6	plod
nejméně	málo	k6eAd3	málo
63	[number]	k4	63
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
kávová	kávový	k2eAgNnPc4d1	kávové
zrna	zrno	k1gNnPc4	zrno
(	(	kIx(	(
<g/>
Coffea	Coffea	k1gFnSc1	Coffea
arabica	arabica	k1gMnSc1	arabica
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kakaové	kakaový	k2eAgInPc1d1	kakaový
boby	bob	k1gInPc1	bob
(	(	kIx(	(
<g/>
Theobroma	Theobroma	k1gFnSc1	Theobroma
cacao	cacao	k1gMnSc1	cacao
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cola	cola	k1gFnSc1	cola
ořechy	ořech	k1gInPc1	ořech
(	(	kIx(	(
<g/>
Cola	cola	k1gFnSc1	cola
acuminata	acuminata	k1gFnSc1	acuminata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čajové	čajový	k2eAgInPc1d1	čajový
lístky	lístek	k1gInPc1	lístek
(	(	kIx(	(
<g/>
Camellia	Camellia	k1gFnSc1	Camellia
thea	thea	k1gMnSc1	thea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lístky	lístek	k1gInPc4	lístek
maté	maté	k1gNnPc2	maté
(	(	kIx(	(
<g/>
Ilex	ilex	k1gInSc1	ilex
paragueyensis	paragueyensis	k1gFnSc2	paragueyensis
<g/>
)	)	kIx)	)
a	a	k8xC	a
guarana	guaran	k1gMnSc2	guaran
(	(	kIx(	(
<g/>
Paullinia	Paullinium	k1gNnSc2	Paullinium
cupana	cupana	k1gFnSc1	cupana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ještě	ještě	k6eAd1	ještě
dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
alkaloidy	alkaloid	k1gInPc1	alkaloid
theofylin	theofylin	k1gInSc1	theofylin
a	a	k8xC	a
theobromin	theobromin	k1gInSc1	theobromin
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
nealkoholických	alkoholický	k2eNgInPc2d1	nealkoholický
nápojů	nápoj	k1gInPc2	nápoj
jako	jako	k8xS	jako
Coca-Cola	cocaola	k1gFnSc1	coca-cola
<g/>
,	,	kIx,	,
Kofola	Kofola	k1gFnSc1	Kofola
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
též	též	k9	též
jako	jako	k9	jako
tein	tein	k1gInSc1	tein
<g/>
,	,	kIx,	,
matein	mateina	k1gFnPc2	mateina
či	či	k8xC	či
guaranin	guaranina	k1gFnPc2	guaranina
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
%	%	kIx~	%
koncentrace	koncentrace	k1gFnSc1	koncentrace
kofeinu	kofein	k1gInSc2	kofein
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
plže	plž	k1gMnPc4	plž
smrtelným	smrtelný	k2eAgInSc7d1	smrtelný
nervovým	nervový	k2eAgInSc7d1	nervový
jedem	jed	k1gInSc7	jed
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
použít	použít	k5eAaPmF	použít
jako	jako	k9	jako
moluskocid	moluskocid	k1gInSc4	moluskocid
na	na	k7c6	na
hubení	hubení	k1gNnSc6	hubení
škodlivých	škodlivý	k2eAgMnPc2d1	škodlivý
plžů	plž	k1gMnPc2	plž
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
mitotický	mitotický	k2eAgInSc1d1	mitotický
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc4	kofein
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mnohé	mnohý	k2eAgFnPc1d1	mnohá
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
přírodní	přírodní	k2eAgInSc4d1	přírodní
pesticid	pesticid	k1gInSc4	pesticid
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
obsah	obsah	k1gInSc4	obsah
mají	mít	k5eAaImIp3nP	mít
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
olistěné	olistěný	k2eAgFnPc1d1	olistěná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chybí	chybět	k5eAaImIp3nS	chybět
jim	on	k3xPp3gMnPc3	on
mechanická	mechanický	k2eAgFnSc1d1	mechanická
ochrana	ochrana	k1gFnSc1	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
ochromuje	ochromovat	k5eAaImIp3nS	ochromovat
a	a	k8xC	a
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
určité	určitý	k2eAgInPc4d1	určitý
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
těmito	tento	k3xDgFnPc7	tento
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
rostou	růst	k5eAaImIp3nP	růst
semenáčky	semenáček	k1gInPc1	semenáček
kávovníku	kávovník	k1gInSc2	kávovník
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
tedy	tedy	k9	tedy
slouží	sloužit	k5eAaImIp3nS	sloužit
nejenom	nejenom	k6eAd1	nejenom
jako	jako	k8xS	jako
přírodní	přírodní	k2eAgInSc4d1	přírodní
pesticid	pesticid	k1gInSc4	pesticid
proti	proti	k7c3	proti
hmyzu	hmyz	k1gInSc3	hmyz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
brání	bránit	k5eAaImIp3nP	bránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vysemenila	vysemenit	k5eAaPmAgFnS	vysemenit
jiná	jiný	k2eAgFnSc1d1	jiná
rostlinka	rostlinka	k1gFnSc1	rostlinka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
obírala	obírat	k5eAaImAgFnS	obírat
semenáček	semenáček	k1gInSc4	semenáček
o	o	k7c4	o
živiny	živina	k1gFnPc4	živina
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
tedy	tedy	k8xC	tedy
rostlince	rostlinka	k1gFnSc3	rostlinka
lepší	dobrý	k2eAgFnSc4d2	lepší
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgInPc1d3	nejběžnější
zdroje	zdroj	k1gInPc1	zdroj
kofeinu	kofein	k1gInSc2	kofein
jsou	být	k5eAaImIp3nP	být
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
kakao	kakao	k1gNnSc1	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známé	známý	k2eAgInPc1d1	známý
zdroje	zdroj	k1gInPc1	zdroj
kofeinu	kofein	k1gInSc2	kofein
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
yerba	yerb	k1gMnSc4	yerb
maté	maté	k1gNnSc2	maté
a	a	k8xC	a
guarana	guaran	k1gMnSc2	guaran
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
užívány	užívat	k5eAaImNgInP	užívat
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
čajů	čaj	k1gInPc2	čaj
a	a	k8xC	a
energetických	energetický	k2eAgInPc2d1	energetický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
přezdívky	přezdívka	k1gFnPc1	přezdívka
pro	pro	k7c4	pro
kofein	kofein	k1gInSc4	kofein
jsou	být	k5eAaImIp3nP	být
matein	mateina	k1gFnPc2	mateina
a	a	k8xC	a
guaranin	guaranina	k1gFnPc2	guaranina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
odvezené	odvezený	k2eAgMnPc4d1	odvezený
ze	z	k7c2	z
jmen	jméno	k1gNnPc2	jméno
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
"	"	kIx"	"
<g/>
maté	maté	k1gNnSc3	maté
nadšenci	nadšenec	k1gMnPc1	nadšenec
<g/>
"	"	kIx"	"
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
matein	matein	k1gInSc1	matein
je	být	k5eAaImIp3nS	být
stereoizomer	stereoizomer	k1gInSc4	stereoizomer
kofeinu	kofein	k1gInSc2	kofein
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
dělalo	dělat	k5eAaImAgNnS	dělat
jinou	jiný	k2eAgFnSc4d1	jiná
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
achirální	achirální	k2eAgFnSc1d1	achirální
molekula	molekula	k1gFnSc1	molekula
a	a	k8xC	a
proto	proto	k8xC	proto
tedy	tedy	k9	tedy
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
enantiomery	enantiomera	k1gFnPc4	enantiomera
ani	ani	k8xC	ani
jiné	jiný	k2eAgInPc4d1	jiný
stereoisomery	stereoisomer	k1gInPc4	stereoisomer
<g/>
.	.	kIx.	.
</s>
<s>
Různorodost	různorodost	k1gFnSc1	různorodost
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
požitím	požití	k1gNnSc7	požití
kofeinu	kofein	k1gInSc2	kofein
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobena	způsobit	k5eAaPmNgFnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostliny	rostlina	k1gFnPc1	rostlina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
množství	množství	k1gNnSc4	množství
dalších	další	k2eAgInPc2d1	další
xantinových	xantinový	k2eAgInPc2d1	xantinový
alkaloidů	alkaloid	k1gInPc2	alkaloid
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
kardiostimulans	kardiostimulans	k6eAd1	kardiostimulans
theofylin	theofylin	k1gInSc4	theofylin
a	a	k8xC	a
theobromin	theobromin	k1gInSc4	theobromin
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
látky	látka	k1gFnPc4	látka
jako	jako	k8xC	jako
polyfenoly	polyfenol	k1gInPc4	polyfenol
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
nerozpustné	rozpustný	k2eNgInPc4d1	nerozpustný
komplexy	komplex	k1gInPc4	komplex
s	s	k7c7	s
kofeinem	kofein	k1gInSc7	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Světově	světově	k6eAd1	světově
nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
zdrojem	zdroj	k1gInSc7	zdroj
kofeinu	kofein	k1gInSc2	kofein
jsou	být	k5eAaImIp3nP	být
boby	bob	k1gInPc1	bob
kávovníku	kávovník	k1gInSc2	kávovník
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
káva	káva	k1gFnSc1	káva
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
kofeinu	kofein	k1gInSc2	kofein
v	v	k7c6	v
kávovém	kávový	k2eAgNnSc6d1	kávové
zrnu	zrno	k1gNnSc6	zrno
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
kávovníku	kávovník	k1gInSc2	kávovník
a	a	k8xC	a
metody	metoda	k1gFnSc2	metoda
zpracování	zpracování	k1gNnPc2	zpracování
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
jednoho	jeden	k4xCgInSc2	jeden
keře	keř	k1gInSc2	keř
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
obsahu	obsah	k1gInSc3	obsah
kofeinu	kofein	k1gInSc2	kofein
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
jeden	jeden	k4xCgInSc1	jeden
šálek	šálek	k1gInSc1	šálek
kávy	káva	k1gFnSc2	káva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
od	od	k7c2	od
40	[number]	k4	40
mg	mg	kA	mg
u	u	k7c2	u
espressa	espress	k1gMnSc2	espress
z	z	k7c2	z
odrůdy	odrůda	k1gFnSc2	odrůda
Arabica	Arabic	k1gInSc2	Arabic
(	(	kIx(	(
<g/>
30	[number]	k4	30
ml	ml	kA	ml
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
100	[number]	k4	100
mg	mg	kA	mg
na	na	k7c4	na
120	[number]	k4	120
ml	ml	kA	ml
v	v	k7c6	v
obyčejné	obyčejný	k2eAgFnSc3d1	obyčejná
překapávané	překapávaný	k2eAgFnSc3d1	překapávaná
kávě	káva	k1gFnSc3	káva
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
dlouho	dlouho	k6eAd1	dlouho
pražená	pražený	k2eAgNnPc1d1	pražené
kávová	kávový	k2eAgNnPc1d1	kávové
zrna	zrno	k1gNnPc1	zrno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
méně	málo	k6eAd2	málo
kofeinu	kofein	k1gInSc2	kofein
než	než	k8xS	než
lehce	lehko	k6eAd1	lehko
pražená	pražený	k2eAgNnPc4d1	pražené
kávová	kávový	k2eAgNnPc4d1	kávové
zrna	zrno	k1gNnPc4	zrno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
proces	proces	k1gInSc1	proces
pražení	pražení	k1gNnSc2	pražení
snižuje	snižovat	k5eAaImIp3nS	snižovat
obsah	obsah	k1gInSc1	obsah
kofeinu	kofein	k1gInSc2	kofein
v	v	k7c6	v
zrnu	zrno	k1gNnSc6	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Arabica	Arabica	k1gFnSc1	Arabica
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
kofeinu	kofein	k1gInSc2	kofein
než	než	k8xS	než
robusta	robusta	k1gFnSc1	robusta
<g/>
.	.	kIx.	.
</s>
<s>
Káva	káva	k1gFnSc1	káva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stopy	stopa	k1gFnPc4	stopa
theofylinu	theofylin	k1gInSc2	theofylin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
theobrominu	theobromin	k1gInSc2	theobromin
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgInSc7d1	další
běžným	běžný	k2eAgInSc7d1	běžný
zdrojem	zdroj	k1gInSc7	zdroj
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Vlastně	vlastně	k9	vlastně
čaj	čaj	k1gInSc1	čaj
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
kofeinu	kofein	k1gInSc2	kofein
než	než	k8xS	než
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hrneček	hrneček	k1gInSc1	hrneček
čaje	čaj	k1gInSc2	čaj
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kofeinu	kofein	k1gInSc2	kofein
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
přípravě	příprava	k1gFnSc3	příprava
není	být	k5eNaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
takové	takový	k3xDgNnSc1	takový
množství	množství	k1gNnSc1	množství
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Sílu	síla	k1gFnSc4	síla
nálevu	nálev	k1gInSc2	nálev
také	také	k9	také
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
podmínky	podmínka	k1gFnPc1	podmínka
pěstování	pěstování	k1gNnSc4	pěstování
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
proces	proces	k1gInSc1	proces
zpracování	zpracování	k1gNnSc2	zpracování
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
čajové	čajový	k2eAgFnPc1d1	čajová
odrůdy	odrůda	k1gFnPc1	odrůda
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
významně	významně	k6eAd1	významně
více	hodně	k6eAd2	hodně
kofeinu	kofein	k1gInSc2	kofein
než	než	k8xS	než
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
theobrominu	theobromin	k1gInSc2	theobromin
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
theofylinu	theofylin	k1gInSc2	theofylin
než	než	k8xS	než
káva	káva	k1gFnSc1	káva
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
čaje	čaj	k1gInSc2	čaj
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
nálev	nálev	k1gInSc1	nálev
nakonec	nakonec	k6eAd1	nakonec
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
čaje	čaj	k1gInSc2	čaj
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
obsahu	obsah	k1gInSc3	obsah
kofeinu	kofein	k1gInSc2	kofein
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
<g/>
.	.	kIx.	.
</s>
<s>
Čaje	čaj	k1gInPc1	čaj
jako	jako	k8xS	jako
světlý	světlý	k2eAgInSc1d1	světlý
japonský	japonský	k2eAgInSc1d1	japonský
čaj	čaj	k1gInSc1	čaj
gyokuro	gyokura	k1gFnSc5	gyokura
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
kofeinu	kofein	k1gInSc2	kofein
než	než	k8xS	než
mnohem	mnohem	k6eAd1	mnohem
tmavší	tmavý	k2eAgInPc4d2	tmavší
čaje	čaj	k1gInPc4	čaj
jako	jako	k8xC	jako
lapsang	lapsang	k1gInSc4	lapsang
souchong	souchonga	k1gFnPc2	souchonga
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kofeinu	kofein	k1gInSc3	kofein
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
také	také	k9	také
běžně	běžně	k6eAd1	běžně
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
limonádách	limonáda	k1gFnPc6	limonáda
jako	jako	k8xC	jako
Cola	cola	k1gFnSc1	cola
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
z	z	k7c2	z
kolových	kolový	k2eAgInPc2d1	kolový
ořechů	ořech	k1gInPc2	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Limonády	limonáda	k1gFnPc1	limonáda
obvykle	obvykle	k6eAd1	obvykle
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
50	[number]	k4	50
mg	mg	kA	mg
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
nápoje	nápoj	k1gInSc2	nápoj
jako	jako	k8xS	jako
Red	Red	k1gFnSc2	Red
Bull	bulla	k1gFnPc2	bulla
obvykle	obvykle	k6eAd1	obvykle
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
320	[number]	k4	320
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
80-160	[number]	k4	80-160
mg	mg	kA	mg
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
plechovce	plechovka	k1gFnSc6	plechovka
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
nápojích	nápoj	k1gInPc6	nápoj
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
získáván	získávat	k5eAaImNgInS	získávat
dekafeinací	dekafeinace	k1gFnSc7	dekafeinace
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
syntetizován	syntetizován	k2eAgInSc1d1	syntetizován
<g/>
.	.	kIx.	.
</s>
<s>
Guarana	Guarana	k1gFnSc1	Guarana
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
ingredience	ingredience	k1gFnSc1	ingredience
energetických	energetický	k2eAgInPc2d1	energetický
nápojů	nápoj	k1gInPc2	nápoj
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velká	velká	k1gFnSc1	velká
množství	množství	k1gNnSc2	množství
kofeinu	kofein	k1gInSc2	kofein
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
theofylinu	theofylin	k1gInSc2	theofylin
a	a	k8xC	a
theobrominu	theobromin	k1gInSc2	theobromin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
Čokoláda	čokoláda	k1gFnSc1	čokoláda
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
z	z	k7c2	z
kakaa	kakao	k1gNnSc2	kakao
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
čokoláda	čokoláda	k1gFnSc1	čokoláda
nemá	mít	k5eNaImIp3nS	mít
velké	velký	k2eAgInPc4d1	velký
povzbuzující	povzbuzující	k2eAgInPc4d1	povzbuzující
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kofein	kofein	k1gInSc1	kofein
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
theofylinem	theofylin	k1gInSc7	theofylin
a	a	k8xC	a
theobrominem	theobromin	k1gInSc7	theobromin
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
velká	velký	k2eAgFnSc1d1	velká
tabulka	tabulka	k1gFnSc1	tabulka
mléčné	mléčný	k2eAgFnSc2d1	mléčná
čokolády	čokoláda	k1gFnSc2	čokoláda
(	(	kIx(	(
<g/>
28	[number]	k4	28
g	g	kA	g
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
tolik	tolik	k6eAd1	tolik
kofeinu	kofein	k1gInSc2	kofein
jako	jako	k8xS	jako
šálek	šálek	k1gInSc1	šálek
kávy	káva	k1gFnSc2	káva
bez	bez	k7c2	bez
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
také	také	k9	také
celkem	celkem	k6eAd1	celkem
běžné	běžný	k2eAgNnSc1d1	běžné
přidávat	přidávat	k5eAaImF	přidávat
kofein	kofein	k1gInSc4	kofein
do	do	k7c2	do
mýdel	mýdlo	k1gNnPc2	mýdlo
a	a	k8xC	a
šampónů	šampónů	k?	šampónů
a	a	k8xC	a
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kofein	kofein	k1gInSc1	kofein
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
absorbován	absorbovat	k5eAaBmNgMnS	absorbovat
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgInPc4	žádný
větší	veliký	k2eAgInPc4d2	veliký
stimulační	stimulační	k2eAgInPc4d1	stimulační
účinky	účinek	k1gInPc4	účinek
na	na	k7c4	na
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
není	být	k5eNaImIp3nS	být
absorbován	absorbovat	k5eAaBmNgInS	absorbovat
dost	dost	k6eAd1	dost
rychle	rychle	k6eAd1	rychle
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Některé	některý	k3yIgFnPc4	některý
farmaceutické	farmaceutický	k2eAgFnPc4d1	farmaceutická
firmy	firma	k1gFnPc4	firma
začaly	začít	k5eAaPmAgFnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
kofein	kofein	k1gInSc4	kofein
v	v	k7c6	v
tabletách	tableta	k1gFnPc6	tableta
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
mozkové	mozkový	k2eAgFnPc4d1	mozková
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
snižují	snižovat	k5eAaImIp3nP	snižovat
únavu	únava	k1gFnSc4	únava
a	a	k8xC	a
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	on	k3xPp3gInPc4	on
používají	používat	k5eAaImIp3nP	používat
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nP	učit
ke	k	k7c3	k
zkoušce	zkouška	k1gFnSc3	zkouška
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
řídit	řídit	k5eAaImF	řídit
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
se	se	k3xPyFc4	se
konzumoval	konzumovat	k5eAaBmAgInS	konzumovat
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
už	už	k6eAd1	už
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žvýkání	žvýkání	k1gNnSc4	žvýkání
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
listů	list	k1gInPc2	list
určitých	určitý	k2eAgFnPc2d1	určitá
rostlin	rostlina	k1gFnPc2	rostlina
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
mnohem	mnohem	k6eAd1	mnohem
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
účinek	účinek	k1gInSc1	účinek
kofeinu	kofein	k1gInSc2	kofein
hodně	hodně	k6eAd1	hodně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rostlina	rostlina	k1gFnSc1	rostlina
zalije	zalít	k5eAaPmIp3nS	zalít
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
přírodních	přírodní	k2eAgInPc2d1	přírodní
národů	národ	k1gInPc2	národ
má	mít	k5eAaImIp3nS	mít
legendy	legenda	k1gFnPc4	legenda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
rostliny	rostlina	k1gFnPc1	rostlina
objeveny	objevit	k5eAaPmNgFnP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
známé	známý	k2eAgFnSc2d1	známá
čínské	čínský	k2eAgFnSc2d1	čínská
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
čínskému	čínský	k2eAgMnSc3d1	čínský
císaři	císař	k1gMnSc3	císař
Shennongovi	Shennong	k1gMnSc3	Shennong
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
panoval	panovat	k5eAaImAgInS	panovat
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
lístky	lístek	k1gInPc4	lístek
rostliny	rostlina	k1gFnPc1	rostlina
náhodou	náhodou	k6eAd1	náhodou
spadly	spadnout	k5eAaPmAgFnP	spadnout
do	do	k7c2	do
horké	horký	k2eAgFnSc2d1	horká
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
osvěžující	osvěžující	k2eAgInSc1d1	osvěžující
a	a	k8xC	a
voňavý	voňavý	k2eAgInSc1d1	voňavý
nápoj	nápoj	k1gInSc1	nápoj
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
císaři	císař	k1gMnSc3	císař
Shennongovi	Shennong	k1gMnSc3	Shennong
se	se	k3xPyFc4	se
také	také	k9	také
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
Lu	Lu	k1gFnSc6	Lu
Yuově	Yuova	k1gFnSc6	Yuova
Cha	cha	k0	cha
Jing	Jinga	k1gFnPc2	Jinga
<g/>
,	,	kIx,	,
slavné	slavný	k2eAgFnPc1d1	slavná
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
staré	starý	k2eAgFnSc6d1	stará
knize	kniha	k1gFnSc6	kniha
o	o	k7c6	o
čaji	čaj	k1gInSc6	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
kávy	káva	k1gFnSc2	káva
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
znovu	znovu	k6eAd1	znovu
až	až	k8xS	až
v	v	k7c6	v
devátém	devátý	k4xOgNnSc6	devátý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
kávové	kávový	k2eAgInPc1d1	kávový
boby	bob	k1gInPc1	bob
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgFnSc1d1	populární
legenda	legenda	k1gFnSc1	legenda
připisuje	připisovat	k5eAaImIp3nS	připisovat
jejich	jejich	k3xOp3gNnSc4	jejich
objevení	objevení	k1gNnSc4	objevení
bájnému	bájný	k2eAgInSc3d1	bájný
pasákovi	pasákův	k2eAgMnPc1d1	pasákův
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Kaldi	Kald	k1gMnPc5	Kald
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
kozy	koza	k1gFnPc1	koza
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pásly	pásnout	k5eAaImAgFnP	pásnout
na	na	k7c6	na
kávovníku	kávovník	k1gInSc6	kávovník
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
čilejší	čilý	k2eAgInPc1d2	čilejší
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
pak	pak	k6eAd1	pak
méně	málo	k6eAd2	málo
spaly	spát	k5eAaImAgFnP	spát
<g/>
.	.	kIx.	.
</s>
<s>
Zkusil	zkusit	k5eAaPmAgMnS	zkusit
sníst	sníst	k5eAaPmF	sníst
několik	několik	k4yIc4	několik
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
zažil	zažít	k5eAaPmAgMnS	zažít
stejné	stejný	k2eAgNnSc4d1	stejné
osvěžení	osvěžení	k1gNnSc4	osvěžení
a	a	k8xC	a
povzbuzení	povzbuzení	k1gNnSc4	povzbuzení
jako	jako	k8xC	jako
kozy	koza	k1gFnPc4	koza
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
literární	literární	k2eAgFnPc1d1	literární
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
kávě	káva	k1gFnSc6	káva
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
perského	perský	k2eAgMnSc2d1	perský
lékaře	lékař	k1gMnSc2	lékař
al-Raziho	al-Razi	k1gMnSc2	al-Razi
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1587	[number]	k4	1587
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
práce	práce	k1gFnSc1	práce
Malaye	Malay	k1gFnSc2	Malay
Jaziri	Jazir	k1gFnSc2	Jazir
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
a	a	k8xC	a
právních	právní	k2eAgNnPc6d1	právní
omezeních	omezení	k1gNnPc6	omezení
s	s	k7c7	s
kávou	káva	k1gFnSc7	káva
spojených	spojený	k2eAgMnPc2d1	spojený
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
"	"	kIx"	"
<g/>
Undat	Undat	k1gInSc1	Undat
al	ala	k1gFnPc2	ala
safwa	safwa	k6eAd1	safwa
fi	fi	k0	fi
hill	hillit	k5eAaPmRp2nS	hillit
al-gahwa	alahwa	k1gMnSc1	al-gahwa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
šejkovi	šejk	k1gMnSc6	šejk
Jamal-al-Din	Jamall-Din	k1gInSc1	Jamal-al-Din
al-Dhabhani	al-Dhabhaň	k1gFnSc3	al-Dhabhaň
<g/>
,	,	kIx,	,
muftím	muftí	k1gMnSc7	muftí
z	z	k7c2	z
Adenu	Aden	k1gInSc2	Aden
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
pravidelně	pravidelně	k6eAd1	pravidelně
pít	pít	k5eAaImF	pít
kávu	káva	k1gFnSc4	káva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1454	[number]	k4	1454
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
jemenští	jemenský	k2eAgMnPc1d1	jemenský
súfisté	súfista	k1gMnPc1	súfista
zvykli	zvyknout	k5eAaPmAgMnP	zvyknout
popíjet	popíjet	k5eAaImF	popíjet
kávu	káva	k1gFnSc4	káva
během	během	k7c2	během
modliteb	modlitba	k1gFnPc2	modlitba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neusnuli	usnout	k5eNaPmAgMnP	usnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
pití	pití	k1gNnSc1	pití
kávy	káva	k1gFnSc2	káva
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
evropským	evropský	k2eAgMnSc7d1	evropský
vyslancem	vyslanec	k1gMnSc7	vyslanec
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
také	také	k9	také
běžně	běžně	k6eAd1	běžně
začala	začít	k5eAaPmAgFnS	začít
používat	používat	k5eAaImF	používat
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
byla	být	k5eAaImAgFnS	být
káva	káva	k1gFnSc1	káva
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
arabské	arabský	k2eAgNnSc1d1	arabské
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
odtáhli	odtáhnout	k5eAaPmAgMnP	odtáhnout
od	od	k7c2	od
hradeb	hradba	k1gFnPc2	hradba
nedobyté	dobytý	k2eNgFnSc2d1	nedobytá
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
našlo	najít	k5eAaPmAgNnS	najít
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gNnPc7	jejich
zavazadly	zavazadlo	k1gNnPc7	zavazadlo
velké	velký	k2eAgFnPc1d1	velká
množství	množství	k1gNnSc4	množství
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Evropané	Evropan	k1gMnPc1	Evropan
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
mají	mít	k5eAaImIp3nP	mít
s	s	k7c7	s
kávovými	kávový	k2eAgInPc7d1	kávový
boby	bob	k1gInPc7	bob
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gMnPc4	on
neznali	neznat	k5eAaImAgMnP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
Polák	Polák	k1gMnSc1	Polák
Jerzy	Jerza	k1gFnSc2	Jerza
Franciszek	Franciszka	k1gFnPc2	Franciszka
Kulczycki	Kulczyck	k1gFnSc2	Kulczyck
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
Turky	Turek	k1gMnPc4	Turek
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
vezme	vzít	k5eAaPmIp3nS	vzít
a	a	k8xC	a
naučí	naučit	k5eAaPmIp3nS	naučit
Vídeňany	Vídeňan	k1gMnPc4	Vídeňan
vařit	vařit	k5eAaImF	vařit
kávu	káva	k1gFnSc4	káva
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
kavárna	kavárna	k1gFnSc1	kavárna
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
světě	svět	k1gInSc6	svět
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
byly	být	k5eAaImAgFnP	být
první	první	k4xOgFnPc1	první
kavárny	kavárna	k1gFnPc1	kavárna
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1652	[number]	k4	1652
v	v	k7c6	v
St.	st.	kA	st.
Michael	Michael	k1gMnSc1	Michael
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Alley	Alley	k1gInPc7	Alley
v	v	k7c6	v
Cornhillu	Cornhillo	k1gNnSc6	Cornhillo
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
populárními	populární	k2eAgInPc7d1	populární
a	a	k8xC	a
hrály	hrát	k5eAaImAgInP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
životě	život	k1gInSc6	život
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ořechy	ořech	k1gInPc1	ořech
koly	kola	k1gFnSc2	kola
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
plody	plod	k1gInPc1	plod
kávovníku	kávovník	k1gInSc2	kávovník
a	a	k8xC	a
čajové	čajový	k2eAgInPc1d1	čajový
listy	list	k1gInPc1	list
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
pradávný	pradávný	k2eAgInSc4d1	pradávný
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
žvýkány	žvýkán	k2eAgFnPc1d1	žvýkána
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
západoafrických	západoafrický	k2eAgFnPc6d1	západoafrická
kulturách	kultura	k1gFnPc6	kultura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
sociálních	sociální	k2eAgNnPc2d1	sociální
setkání	setkání	k1gNnPc2	setkání
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
schopnost	schopnost	k1gFnSc4	schopnost
navracet	navracet	k5eAaImF	navracet
lidem	člověk	k1gMnPc3	člověk
vitalitu	vitalita	k1gFnSc4	vitalita
a	a	k8xC	a
snižovat	snižovat	k5eAaImF	snižovat
pocit	pocit	k1gInSc4	pocit
hladu	hlad	k1gInSc2	hlad
během	během	k7c2	během
hladomorů	hladomor	k1gInPc2	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
byla	být	k5eAaImAgFnS	být
Cola	cola	k1gFnSc1	cola
pronásledována	pronásledovat	k5eAaImNgFnS	pronásledovat
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
hrozeb	hrozba	k1gFnPc2	hrozba
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgMnPc2d1	americký
nechala	nechat	k5eAaPmAgFnS	nechat
vylít	vylít	k5eAaPmF	vylít
40	[number]	k4	40
barelů	barel	k1gInPc2	barel
a	a	k8xC	a
20	[number]	k4	20
sudů	sud	k1gInPc2	sud
Coca-colového	Cocaolový	k2eAgInSc2d1	Coca-colový
sirupu	sirup	k1gInSc2	sirup
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Chattanooga	Chattanoog	k1gMnSc2	Chattanoog
<g/>
,	,	kIx,	,
Tennessee	Tennesse	k1gMnSc2	Tennesse
<g/>
,	,	kIx,	,
s	s	k7c7	s
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Cola	cola	k1gFnSc1	cola
je	být	k5eAaImIp3nS	být
zdraví	zdraví	k1gNnSc4	zdraví
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
doufala	doufat	k5eAaImAgFnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstraní	odstranit	k5eAaPmIp3nS	odstranit
kofein	kofein	k1gInSc1	kofein
z	z	k7c2	z
Coca-coly	cocaola	k1gFnSc2	coca-cola
prohlášeními	prohlášení	k1gNnPc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
užívaní	užívaný	k2eAgMnPc1d1	užívaný
Coca-coly	cocaola	k1gFnPc4	coca-cola
vedlo	vést	k5eAaImAgNnS	vést
na	na	k7c6	na
dívčí	dívčí	k2eAgFnSc6d1	dívčí
škole	škola	k1gFnSc6	škola
k	k	k7c3	k
nočním	noční	k2eAgFnPc3d1	noční
výtržnostem	výtržnost	k1gFnPc3	výtržnost
<g/>
,	,	kIx,	,
porušením	porušení	k1gNnSc7	porušení
kolejního	kolejní	k2eAgInSc2d1	kolejní
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
amorálnostem	amorálnost	k1gFnPc3	amorálnost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
soudce	soudce	k1gMnSc1	soudce
se	se	k3xPyFc4	se
Coly	cola	k1gFnSc2	cola
zastal	zastat	k5eAaPmAgMnS	zastat
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
rozsudky	rozsudek	k1gInPc1	rozsudek
byly	být	k5eAaImAgInP	být
vyneseny	vynést	k5eAaPmNgInP	vynést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
vlády	vláda	k1gFnSc2	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
přidání	přidání	k1gNnSc4	přidání
kofeinu	kofein	k1gInSc2	kofein
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
závislost	závislost	k1gFnSc4	závislost
a	a	k8xC	a
které	který	k3yIgFnPc4	který
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
uvedeny	uvést	k5eAaPmNgInP	uvést
na	na	k7c6	na
etiketě	etiketa	k1gFnSc6	etiketa
výrobku	výrobek	k1gInSc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Nejranější	raný	k2eAgFnPc1d3	nejranější
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
používání	používání	k1gNnSc6	používání
kakaa	kakao	k1gNnSc2	kakao
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
Mayské	mayský	k2eAgFnSc6d1	mayská
keramice	keramika	k1gFnSc6	keramika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
byla	být	k5eAaImAgFnS	být
čokoláda	čokoláda	k1gFnSc1	čokoláda
konzumována	konzumován	k2eAgFnSc1d1	konzumována
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hořkého	hořký	k2eAgInSc2d1	hořký
a	a	k8xC	a
pálivého	pálivý	k2eAgInSc2d1	pálivý
nápoje	nápoj	k1gInSc2	nápoj
xocoatlu	xocoatlat	k5eAaPmIp1nS	xocoatlat
často	často	k6eAd1	často
ochuceným	ochucený	k2eAgNnSc7d1	ochucené
vanilkou	vanilka	k1gFnSc7	vanilka
<g/>
,	,	kIx,	,
chilli	chilli	k1gNnSc7	chilli
nebo	nebo	k8xC	nebo
achiotou	achiota	k1gFnSc7	achiota
<g/>
.	.	kIx.	.
</s>
<s>
Xocoatl	Xocoatnout	k5eAaPmAgMnS	Xocoatnout
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potlačoval	potlačovat	k5eAaImAgMnS	potlačovat
mdloby	mdloba	k1gFnPc4	mdloba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
obsahu	obsah	k1gInSc3	obsah
theobrominu	theobromin	k1gInSc2	theobromin
a	a	k8xC	a
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Čokoláda	čokoláda	k1gFnSc1	čokoláda
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgNnSc7d1	důležité
luxusním	luxusní	k2eAgNnSc7d1	luxusní
zbožím	zboží	k1gNnSc7	zboží
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
předkolumbovské	předkolumbovský	k2eAgFnSc6d1	předkolumbovská
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
kakaové	kakaový	k2eAgInPc1d1	kakaový
boby	bob	k1gInPc1	bob
byly	být	k5eAaImAgInP	být
užívány	užívat	k5eAaImNgInP	užívat
jako	jako	k8xS	jako
platidlo	platidlo	k1gNnSc1	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
Xocoatl	Xocoatnout	k5eAaPmAgMnS	Xocoatnout
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
přivezen	přivezen	k2eAgInSc4d1	přivezen
Španěly	Španěly	k1gInPc4	Španěly
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
populárním	populární	k2eAgInSc7d1	populární
nápojem	nápoj	k1gInSc7	nápoj
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
také	také	k9	také
přivezli	přivézt	k5eAaPmAgMnP	přivézt
první	první	k4xOgInPc4	první
kakaové	kakaový	k2eAgInPc4d1	kakaový
stromy	strom	k1gInPc4	strom
do	do	k7c2	do
Západní	západní	k2eAgFnSc2d1	západní
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
na	na	k7c4	na
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
při	při	k7c6	při
alchymii	alchymie	k1gFnSc6	alchymie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
Černá	černý	k2eAgFnSc1d1	černá
fazole	fazole	k1gFnSc1	fazole
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
a	a	k8xC	a
stonky	stonek	k1gInPc1	stonek
Yaupon	Yaupon	k1gNnSc4	Yaupon
Holly	Holla	k1gFnSc2	Holla
(	(	kIx(	(
<g/>
Ilex	ilex	k1gInSc1	ilex
vomitoria	vomitorium	k1gNnSc2	vomitorium
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgInP	používat
domorodými	domorodý	k2eAgInPc7d1	domorodý
Američany	Američan	k1gMnPc4	Američan
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
čaje	čaj	k1gInSc2	čaj
pojmenovaného	pojmenovaný	k2eAgInSc2d1	pojmenovaný
Asi	asi	k9	asi
nebo	nebo	k8xC	nebo
také	také	k9	také
Černý	černý	k2eAgInSc4d1	černý
nápoj	nápoj	k1gInSc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
se	se	k3xPyFc4	se
do	do	k7c2	do
lidského	lidský	k2eAgNnSc2d1	lidské
těla	tělo	k1gNnSc2	tělo
dostává	dostávat	k5eAaImIp3nS	dostávat
převážně	převážně	k6eAd1	převážně
orálně	orálně	k6eAd1	orálně
(	(	kIx(	(
<g/>
ústy	ústa	k1gNnPc7	ústa
<g/>
)	)	kIx)	)
–	–	k?	–
pitím	pití	k1gNnSc7	pití
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
Coly	cola	k1gFnSc2	cola
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
léků	lék	k1gInPc2	lék
proti	proti	k7c3	proti
únavě	únava	k1gFnSc3	únava
nebo	nebo	k8xC	nebo
intravenózně	intravenózně	k6eAd1	intravenózně
(	(	kIx(	(
<g/>
nitrožilně	nitrožilně	k6eAd1	nitrožilně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
se	se	k3xPyFc4	se
z	z	k7c2	z
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
tenkého	tenký	k2eAgNnSc2d1	tenké
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kávy	káva	k1gFnSc2	káva
se	se	k3xPyFc4	se
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
po	po	k7c6	po
několika	několik	k4yIc6	několik
minutách	minuta	k1gFnPc6	minuta
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
čaje	čaj	k1gInSc2	čaj
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
až	až	k9	až
po	po	k7c6	po
40	[number]	k4	40
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jiné	jiný	k2eAgInPc4d1	jiný
alkaloidy	alkaloid	k1gInPc4	alkaloid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vstřebávání	vstřebávání	k1gNnSc4	vstřebávání
kofeinu	kofein	k1gInSc2	kofein
oddalují	oddalovat	k5eAaImIp3nP	oddalovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
čaj	čaj	k1gInSc1	čaj
zdravější	zdravý	k2eAgInSc1d2	zdravější
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
nejprve	nejprve	k6eAd1	nejprve
demethylován	demethylovat	k5eAaImNgInS	demethylovat
v	v	k7c6	v
dimetylxantin	dimetylxantin	k1gMnSc1	dimetylxantin
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c4	v
monometylxantin	monometylxantin	k1gInSc4	monometylxantin
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
štěpí	štěpit	k5eAaImIp3nS	štěpit
v	v	k7c4	v
kyselinu	kyselina	k1gFnSc4	kyselina
močovou	močový	k2eAgFnSc4d1	močová
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
Opouští	opouštět	k5eAaImIp3nS	opouštět
tělo	tělo	k1gNnSc4	tělo
asi	asi	k9	asi
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pravidelné	pravidelný	k2eAgFnSc6d1	pravidelná
aplikaci	aplikace	k1gFnSc6	aplikace
kofeinu	kofein	k1gInSc2	kofein
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
tělo	tělo	k1gNnSc1	tělo
na	na	k7c4	na
kofein	kofein	k1gInSc4	kofein
rezistentní	rezistentní	k2eAgInSc4d1	rezistentní
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
vzniknout	vzniknout	k5eAaPmF	vzniknout
závislost	závislost	k1gFnSc1	závislost
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
neurychluje	urychlovat	k5eNaImIp3nS	urychlovat
vystřízlivění	vystřízlivění	k1gNnSc1	vystřízlivění
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Nemění	měnit	k5eNaImIp3nS	měnit
osobnost	osobnost	k1gFnSc4	osobnost
a	a	k8xC	a
charakter	charakter	k1gInSc4	charakter
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
dětí	dítě	k1gFnPc2	dítě
na	na	k7c4	na
kofein	kofein	k1gInSc4	kofein
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
citlivosti	citlivost	k1gFnSc2	citlivost
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
příznivě	příznivě	k6eAd1	příznivě
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
centrální	centrální	k2eAgInSc4d1	centrální
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
oddaluje	oddalovat	k5eAaImIp3nS	oddalovat
únavu	únava	k1gFnSc4	únava
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
duševní	duševní	k2eAgFnSc4d1	duševní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zbystřuje	zbystřovat	k5eAaImIp3nS	zbystřovat
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
koncentraci	koncentrace	k1gFnSc4	koncentrace
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
jistou	jistý	k2eAgFnSc4d1	jistá
euforii	euforie	k1gFnSc4	euforie
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
tep	tep	k1gInSc1	tep
<g/>
,	,	kIx,	,
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
hladké	hladký	k2eAgNnSc1d1	hladké
svalstvo	svalstvo	k1gNnSc1	svalstvo
<g/>
,	,	kIx,	,
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
tepny	tepna	k1gFnPc4	tepna
a	a	k8xC	a
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
oběhový	oběhový	k2eAgInSc1d1	oběhový
a	a	k8xC	a
respirační	respirační	k2eAgInSc1d1	respirační
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
lidem	lido	k1gNnSc7	lido
postiženým	postižený	k2eAgNnSc7d1	postižené
astmatem	astma	k1gNnSc7	astma
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
obsah	obsah	k1gInSc1	obsah
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
v	v	k7c6	v
oběhovém	oběhový	k2eAgInSc6d1	oběhový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
proto	proto	k8xC	proto
léta	léto	k1gNnPc4	léto
používán	používat	k5eAaImNgMnS	používat
sportovci	sportovec	k1gMnPc7	sportovec
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
účinky	účinek	k1gInPc1	účinek
mohou	moct	k5eAaImIp3nP	moct
trvat	trvat	k5eAaImF	trvat
od	od	k7c2	od
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
až	až	k6eAd1	až
do	do	k7c2	do
dvanácti	dvanáct	k4xCc2	dvanáct
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
užívání	užívání	k1gNnSc2	užívání
se	se	k3xPyFc4	se
tělo	tělo	k1gNnSc1	tělo
stane	stanout	k5eAaPmIp3nS	stanout
rezistentním	rezistentní	k2eAgInSc7d1	rezistentní
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kofein	kofein	k1gInSc1	kofein
stresuje	stresovat	k5eAaImIp3nS	stresovat
tělo	tělo	k1gNnSc4	tělo
působením	působení	k1gNnSc7	působení
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
adenosinové	adenosinový	k2eAgInPc4d1	adenosinový
receptory	receptor	k1gInPc4	receptor
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dočasnému	dočasný	k2eAgNnSc3d1	dočasné
zvýšení	zvýšení	k1gNnSc3	zvýšení
metabolismu	metabolismus	k1gInSc3	metabolismus
a	a	k8xC	a
odbourávání	odbourávání	k1gNnSc3	odbourávání
tuků	tuk	k1gInPc2	tuk
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
němuž	jenž	k3xRgNnSc3	jenž
se	se	k3xPyFc4	se
kofein	kofein	k1gInSc1	kofein
často	často	k6eAd1	často
propaguje	propagovat	k5eAaImIp3nS	propagovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
účinek	účinek	k1gInSc1	účinek
však	však	k9	však
není	být	k5eNaImIp3nS	být
trvalý	trvalý	k2eAgInSc1d1	trvalý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
–	–	k?	–
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
užívání	užívání	k1gNnSc2	užívání
kofeinu	kofein	k1gInSc2	kofein
díky	díky	k7c3	díky
adaptaci	adaptace	k1gFnSc3	adaptace
počtu	počet	k1gInSc2	počet
adenosinových	adenosinův	k2eAgInPc2d1	adenosinův
receptorů	receptor	k1gInPc2	receptor
na	na	k7c6	na
postsynaptické	postsynaptický	k2eAgFnSc6d1	postsynaptická
membráně	membrána	k1gFnSc6	membrána
vyprchá	vyprchat	k5eAaPmIp3nS	vyprchat
a	a	k8xC	a
po	po	k7c6	po
vysazení	vysazení	k1gNnSc6	vysazení
kofeinu	kofein	k1gInSc2	kofein
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
splácet	splácet	k5eAaImF	splácet
stejnou	stejný	k2eAgFnSc7d1	stejná
nebo	nebo	k8xC	nebo
delší	dlouhý	k2eAgFnSc7d2	delší
dobou	doba	k1gFnSc7	doba
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
únavy	únava	k1gFnSc2	únava
<g/>
,	,	kIx,	,
přejídáním	přejídání	k1gNnSc7	přejídání
a	a	k8xC	a
zpomaleného	zpomalený	k2eAgInSc2d1	zpomalený
metabolismu	metabolismus	k1gInSc2	metabolismus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kofein	kofein	k1gInSc1	kofein
dočasně	dočasně	k6eAd1	dočasně
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
mobilizaci	mobilizace	k1gFnSc3	mobilizace
tukových	tukový	k2eAgFnPc2d1	tuková
zásob	zásoba	k1gFnPc2	zásoba
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracující	pracující	k2eAgInSc1d1	pracující
sval	sval	k1gInSc1	sval
tuk	tuk	k1gInSc1	tuk
využije	využít	k5eAaPmIp3nS	využít
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odsouvá	odsouvat	k5eAaImIp3nS	odsouvat
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
glykogenu	glykogen	k1gInSc2	glykogen
a	a	k8xC	a
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
věnovat	věnovat	k5eAaPmF	věnovat
tréninku	trénink	k1gInSc2	trénink
nebo	nebo	k8xC	nebo
požadovanému	požadovaný	k2eAgInSc3d1	požadovaný
výkonu	výkon	k1gInSc3	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Však	však	k9	však
ani	ani	k8xC	ani
tento	tento	k3xDgInSc1	tento
účinek	účinek	k1gInSc1	účinek
není	být	k5eNaImIp3nS	být
trvalý	trvalý	k2eAgInSc1d1	trvalý
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
vyprchá	vyprchat	k5eAaPmIp3nS	vyprchat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
výzkumníci	výzkumník	k1gMnPc1	výzkumník
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kofein	kofein	k1gInSc1	kofein
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
škodlivě	škodlivě	k6eAd1	škodlivě
i	i	k9	i
na	na	k7c4	na
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
žádné	žádný	k3yNgInPc1	žádný
nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
nepozorují	pozorovat	k5eNaImIp3nP	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
tradiční	tradiční	k2eAgFnSc2d1	tradiční
konzumace	konzumace	k1gFnSc2	konzumace
čaje	čaj	k1gInSc2	čaj
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
)	)	kIx)	)
také	také	k9	také
skutečně	skutečně	k6eAd1	skutečně
připravují	připravovat	k5eAaImIp3nP	připravovat
čaj	čaj	k1gInSc4	čaj
metodou	metoda	k1gFnSc7	metoda
druhého	druhý	k4xOgInSc2	druhý
záparu	zápar	k1gInSc2	zápar
a	a	k8xC	a
první	první	k4xOgInSc4	první
zápar	zápar	k1gInSc4	zápar
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
spíše	spíše	k9	spíše
škodlivý	škodlivý	k2eAgInSc4d1	škodlivý
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kofein	kofein	k1gInSc1	kofein
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
látka	látka	k1gFnSc1	látka
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
<g/>
)	)	kIx)	)
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
záparu	zápar	k1gInSc2	zápar
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
při	při	k7c6	při
nižší	nízký	k2eAgFnSc6d2	nižší
teplotě	teplota	k1gFnSc6	teplota
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
při	při	k7c6	při
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
zamíchání	zamíchání	k1gNnSc6	zamíchání
přejde	přejít	k5eAaPmIp3nS	přejít
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
většina	většina	k1gFnSc1	většina
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
zápar	zápar	k1gInSc1	zápar
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
nežádoucími	žádoucí	k2eNgInPc7d1	nežádoucí
účinky	účinek	k1gInPc7	účinek
kofeinu	kofein	k1gInSc2	kofein
postrádá	postrádat	k5eAaImIp3nS	postrádat
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
stimulační	stimulační	k2eAgInSc4d1	stimulační
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
účinky	účinek	k1gInPc1	účinek
čaje	čaj	k1gInSc2	čaj
však	však	k9	však
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
zachovány	zachován	k2eAgInPc1d1	zachován
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
přecitlivělých	přecitlivělý	k2eAgMnPc2d1	přecitlivělý
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
kofein	kofein	k1gInSc1	kofein
vedle	vedle	k7c2	vedle
stimulace	stimulace	k1gFnSc2	stimulace
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
i	i	k8xC	i
účinky	účinek	k1gInPc1	účinek
opačné	opačný	k2eAgInPc1d1	opačný
a	a	k8xC	a
stimulace	stimulace	k1gFnSc1	stimulace
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
vždy	vždy	k6eAd1	vždy
doprovázena	doprovázet	k5eAaImNgFnS	doprovázet
i	i	k8xC	i
přehlušena	přehlušit	k5eAaPmNgFnS	přehlušit
rušivým	rušivý	k2eAgInSc7d1	rušivý
účinkem	účinek	k1gInSc7	účinek
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Negativní	negativní	k2eAgInPc1d1	negativní
projevy	projev	k1gInPc1	projev
jsou	být	k5eAaImIp3nP	být
zrychlení	zrychlení	k1gNnSc4	zrychlení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
či	či	k8xC	či
bolest	bolest	k1gFnSc4	bolest
žaludku	žaludek	k1gInSc2	žaludek
(	(	kIx(	(
<g/>
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
překyselení	překyselení	k1gNnSc2	překyselení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
užívat	užívat	k5eAaImF	užívat
lidem	lid	k1gInSc7	lid
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
pálení	pálení	k1gNnSc3	pálení
žáhy	žáha	k1gFnSc2	žáha
<g/>
,	,	kIx,	,
žaludečními	žaludeční	k2eAgInPc7d1	žaludeční
a	a	k8xC	a
dvanáctníkovými	dvanáctníkový	k2eAgInPc7d1	dvanáctníkový
vředy	vřed	k1gInPc7	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
výskytu	výskyt	k1gInSc2	výskyt
onemocnění	onemocnění	k1gNnSc2	onemocnění
srdce	srdce	k1gNnSc2	srdce
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
průměrná	průměrný	k2eAgFnSc1d1	průměrná
denní	denní	k2eAgFnSc1d1	denní
spotřeba	spotřeba	k1gFnSc1	spotřeba
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
pět	pět	k4xCc4	pět
šálků	šálek	k1gInPc2	šálek
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
srdeční	srdeční	k2eAgFnSc3d1	srdeční
aktivitě	aktivita	k1gFnSc3	aktivita
může	moct	k5eAaImIp3nS	moct
kofein	kofein	k1gInSc1	kofein
v	v	k7c6	v
krajním	krajní	k2eAgInSc6d1	krajní
případě	případ	k1gInSc6	případ
přivodit	přivodit	k5eAaBmF	přivodit
infarkt	infarkt	k1gInSc4	infarkt
nebo	nebo	k8xC	nebo
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
ledvinami	ledvina	k1gFnPc7	ledvina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
diuretikum	diuretikum	k1gNnSc4	diuretikum
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
studie	studie	k1gFnPc4	studie
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kofein	kofein	k1gInSc1	kofein
přechodně	přechodně	k6eAd1	přechodně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
nevytvoří	vytvořit	k5eNaPmIp3nS	vytvořit
tolerance	tolerance	k1gFnSc1	tolerance
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
výchozím	výchozí	k2eAgFnPc3d1	výchozí
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
u	u	k7c2	u
zdravých	zdravý	k2eAgMnPc2d1	zdravý
lidí	člověk	k1gMnPc2	člověk
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
konzumace	konzumace	k1gFnSc2	konzumace
kávy	káva	k1gFnSc2	káva
(	(	kIx(	(
<g/>
3	[number]	k4	3
šálky	šálek	k1gInPc7	šálek
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
hypertenzí	hypertenze	k1gFnSc7	hypertenze
tlak	tlak	k1gInSc4	tlak
nekolísá	kolísat	k5eNaImIp3nS	kolísat
již	již	k6eAd1	již
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
přiměřeném	přiměřený	k2eAgInSc6d1	přiměřený
příjmu	příjem	k1gInSc6	příjem
kofeinu	kofein	k1gInSc2	kofein
(	(	kIx(	(
<g/>
do	do	k7c2	do
pěti	pět	k4xCc2	pět
šálků	šálek	k1gInPc2	šálek
kávy	káva	k1gFnSc2	káva
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nezvyšuje	zvyšovat	k5eNaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
hypertenze	hypertenze	k1gFnSc2	hypertenze
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
stresu	stres	k1gInSc6	stres
a	a	k8xC	a
pije	pít	k5eAaImIp3nS	pít
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
násobí	násobit	k5eAaImIp3nS	násobit
negativní	negativní	k2eAgInPc4d1	negativní
účinky	účinek	k1gInPc4	účinek
stresu	stres	k1gInSc2	stres
na	na	k7c4	na
oběhovou	oběhový	k2eAgFnSc4d1	oběhová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kofein	kofein	k1gInSc1	kofein
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
srdeční	srdeční	k2eAgFnSc4d1	srdeční
arytmii	arytmie	k1gFnSc4	arytmie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
dojít	dojít	k5eAaPmF	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
z	z	k7c2	z
části	část	k1gFnSc2	část
podílet	podílet	k5eAaImF	podílet
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
rizika	riziko	k1gNnSc2	riziko
vzniku	vznik	k1gInSc2	vznik
osteoporózy	osteoporóza	k1gFnSc2	osteoporóza
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
metabolismus	metabolismus	k1gInSc4	metabolismus
vápníku	vápník	k1gInSc2	vápník
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
–	–	k?	–
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jeho	jeho	k3xOp3gNnSc4	jeho
vylučování	vylučování	k1gNnSc4	vylučování
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
pije	pít	k5eAaImIp3nS	pít
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgInPc4	dva
šálky	šálek	k1gInPc4	šálek
kávy	káva	k1gFnSc2	káva
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
vypít	vypít	k5eAaPmF	vypít
sklenici	sklenice	k1gFnSc4	sklenice
mléka	mléko	k1gNnSc2	mléko
nebo	nebo	k8xC	nebo
sníst	sníst	k5eAaPmF	sníst
jogurt	jogurt	k1gInSc4	jogurt
pro	pro	k7c4	pro
doplnění	doplnění	k1gNnSc4	doplnění
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kofein	kofein	k1gInSc1	kofein
u	u	k7c2	u
buněk	buňka	k1gFnPc2	buňka
odpovědných	odpovědný	k2eAgFnPc2d1	odpovědná
za	za	k7c4	za
výstavbu	výstavba	k1gFnSc4	výstavba
kostí	kost	k1gFnPc2	kost
osteoblastech	osteoblast	k1gInPc6	osteoblast
výrazně	výrazně	k6eAd1	výrazně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
aktivitu	aktivita	k1gFnSc4	aktivita
receptoru	receptor	k1gInSc2	receptor
pro	pro	k7c4	pro
glukokortikoidy	glukokortikoid	k1gInPc4	glukokortikoid
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
až	až	k9	až
několikanásobně	několikanásobně	k6eAd1	několikanásobně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jejich	jejich	k3xOp3gInSc4	jejich
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
glukokortikoidů	glukokortikoid	k1gInPc2	glukokortikoid
(	(	kIx(	(
<g/>
při	při	k7c6	při
stresu	stres	k1gInSc6	stres
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc6	některý
poruchách	poruch	k1gInPc6	poruch
hypofýzy	hypofýza	k1gFnSc2	hypofýza
a	a	k8xC	a
nadledvin	nadledvina	k1gFnPc2	nadledvina
nebo	nebo	k8xC	nebo
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
podávání	podávání	k1gNnSc6	podávání
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
imunity	imunita	k1gFnSc2	imunita
<g/>
)	)	kIx)	)
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
kofeinem	kofein	k1gInSc7	kofein
tedy	tedy	k8xC	tedy
kostem	kost	k1gFnPc3	kost
rozhodně	rozhodně	k6eAd1	rozhodně
nesvědčí	svědčit	k5eNaImIp3nS	svědčit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
souvislost	souvislost	k1gFnSc1	souvislost
kofeinu	kofein	k1gInSc2	kofein
s	s	k7c7	s
vyšším	vysoký	k2eAgNnSc7d2	vyšší
rizikem	riziko	k1gNnSc7	riziko
vzniku	vznik	k1gInSc2	vznik
cystickou	cystický	k2eAgFnSc7d1	cystická
mastitidou	mastitida	k1gFnSc7	mastitida
či	či	k8xC	či
mastodynií	mastodynie	k1gFnSc7	mastodynie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
prokázat	prokázat	k5eAaPmF	prokázat
i	i	k9	i
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
rakovinou	rakovina	k1gFnSc7	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
prochází	procházet	k5eAaImIp3nS	procházet
i	i	k9	i
placentou	placenta	k1gFnSc7	placenta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nadměrném	nadměrný	k2eAgInSc6d1	nadměrný
příjmu	příjem	k1gInSc6	příjem
kofeinu	kofein	k1gInSc2	kofein
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
růstu	růst	k1gInSc2	růst
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kojení	kojení	k1gNnSc6	kojení
je	být	k5eAaImIp3nS	být
kofein	kofein	k1gInSc1	kofein
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
mateřském	mateřský	k2eAgNnSc6d1	mateřské
mléku	mléko	k1gNnSc6	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podrážděné	podrážděný	k2eAgNnSc1d1	podrážděné
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
trpět	trpět	k5eAaImF	trpět
nespavostí	nespavost	k1gFnSc7	nespavost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
kojení	kojení	k1gNnSc2	kojení
by	by	kYmCp3nS	by
neměla	mít	k5eNaImAgFnS	mít
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
kofeinu	kofein	k1gInSc2	kofein
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
300	[number]	k4	300
mg	mg	kA	mg
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
šálky	šálka	k1gFnSc2	šálka
kávy	káva	k1gFnSc2	káva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samiček	samička	k1gFnPc2	samička
myší	myš	k1gFnPc2	myš
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
konzumace	konzumace	k1gFnSc1	konzumace
kofeinu	kofein	k1gInSc2	kofein
má	mít	k5eAaImIp3nS	mít
neblahý	blahý	k2eNgInSc4d1	neblahý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
plod	plod	k1gInSc4	plod
(	(	kIx(	(
<g/>
potomstvo	potomstvo	k1gNnSc4	potomstvo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
snižuje	snižovat	k5eAaImIp3nS	snižovat
účinek	účinek	k1gInSc1	účinek
některých	některý	k3yIgInPc2	některý
léků	lék	k1gInPc2	lék
proti	proti	k7c3	proti
epilepsii	epilepsie	k1gFnSc3	epilepsie
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
na	na	k7c6	na
členovcích	členovec	k1gMnPc6	členovec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
pavoucích	pavouk	k1gMnPc6	pavouk
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kofein	kofein	k1gInSc1	kofein
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
alkaloidy	alkaloid	k1gInPc7	alkaloid
působící	působící	k2eAgFnSc2d1	působící
největší	veliký	k2eAgInSc4d3	veliký
chaos	chaos	k1gInSc4	chaos
v	v	k7c6	v
informační	informační	k2eAgFnSc6d1	informační
výměně	výměna	k1gFnSc6	výměna
buněk	buňka	k1gFnPc2	buňka
včetně	včetně	k7c2	včetně
neuronů	neuron	k1gInPc2	neuron
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
ovlivnění	ovlivněný	k2eAgMnPc1d1	ovlivněný
kofeinem	kofein	k1gInSc7	kofein
spřádali	spřádat	k5eAaImAgMnP	spřádat
síť	síť	k1gFnSc4	síť
chaotičtěji	chaoticky	k6eAd2	chaoticky
než	než	k8xS	než
ti	ten	k3xDgMnPc1	ten
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
LSD	LSD	kA	LSD
nebo	nebo	k8xC	nebo
heroinem	heroin	k1gInSc7	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
informací	informace	k1gFnPc2	informace
není	být	k5eNaImIp3nS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dochází	docházet	k5eAaImIp3nS	docházet
častěji	často	k6eAd2	často
k	k	k7c3	k
chybám	chyba	k1gFnPc3	chyba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
působí	působit	k5eAaImIp3nS	působit
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
informačního	informační	k2eAgInSc2d1	informační
šumu	šum	k1gInSc2	šum
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
tohoto	tento	k3xDgInSc2	tento
faktu	fakt	k1gInSc2	fakt
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgInPc1d1	jasný
<g/>
,	,	kIx,	,
určitou	určitý	k2eAgFnSc4d1	určitá
podobnost	podobnost	k1gFnSc4	podobnost
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
kofein	kofein	k1gInSc4	kofein
přecitlivělých	přecitlivělý	k2eAgMnPc2d1	přecitlivělý
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
soubor	soubor	k1gInSc1	soubor
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zjistit	zjistit	k5eAaPmF	zjistit
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
kofein	kofein	k1gInSc4	kofein
přecitlivělých	přecitlivělý	k2eAgMnPc2d1	přecitlivělý
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
přecitlivělých	přecitlivělý	k2eAgFnPc2d1	přecitlivělá
kofein	kofein	k1gInSc4	kofein
jen	jen	k9	jen
spouští	spouštět	k5eAaImIp3nS	spouštět
neurotické	neurotický	k2eAgInPc4d1	neurotický
projevy	projev	k1gInPc4	projev
disponovaného	disponovaný	k2eAgInSc2d1	disponovaný
organizmu	organizmus	k1gInSc2	organizmus
nebo	nebo	k8xC	nebo
také	také	k9	také
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
přímo	přímo	k6eAd1	přímo
více	hodně	k6eAd2	hodně
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
<g/>
.	.	kIx.	.
</s>
<s>
Nejspíš	nejspíš	k9	nejspíš
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kombinaci	kombinace	k1gFnSc4	kombinace
obou	dva	k4xCgInPc2	dva
mechanizmů	mechanizmus	k1gInPc2	mechanizmus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přecitlivělosti	přecitlivělost	k1gFnSc6	přecitlivělost
na	na	k7c4	na
kofein	kofein	k1gInSc4	kofein
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
silněji	silně	k6eAd2	silně
projeví	projevit	k5eAaPmIp3nS	projevit
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
u	u	k7c2	u
běžné	běžný	k2eAgFnSc2d1	běžná
populace	populace	k1gFnSc2	populace
<g/>
:	:	kIx,	:
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
neklid	neklid	k1gInSc1	neklid
<g/>
,	,	kIx,	,
nervozita	nervozita	k1gFnSc1	nervozita
<g/>
,	,	kIx,	,
nesoustředěnost	nesoustředěnost	k1gFnSc1	nesoustředěnost
<g/>
,	,	kIx,	,
únava	únava	k1gFnSc1	únava
<g/>
,	,	kIx,	,
nespavost	nespavost	k1gFnSc1	nespavost
<g/>
,	,	kIx,	,
pocení	pocení	k1gNnSc1	pocení
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
bušení	bušení	k1gNnSc1	bušení
srdce	srdce	k1gNnSc2	srdce
aj.	aj.	kA	aj.
Pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
na	na	k7c4	na
kofein	kofein	k1gInSc4	kofein
přecitlivělé	přecitlivělý	k2eAgFnSc2d1	přecitlivělá
nejsou	být	k5eNaImIp3nP	být
vhodné	vhodný	k2eAgFnPc4d1	vhodná
ani	ani	k8xC	ani
běžné	běžný	k2eAgFnPc4d1	běžná
dávky	dávka	k1gFnPc4	dávka
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
obsažené	obsažený	k2eAgFnSc2d1	obsažená
v	v	k7c6	v
čaji	čaj	k1gInSc6	čaj
<g/>
,	,	kIx,	,
kávě	káva	k1gFnSc6	káva
<g/>
,	,	kIx,	,
čokoládě	čokoláda	k1gFnSc6	čokoláda
<g/>
,	,	kIx,	,
kakau	kakao	k1gNnSc6	kakao
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc6	některý
limonádách	limonáda	k1gFnPc6	limonáda
aj.	aj.	kA	aj.
Přecitlivělé	přecitlivělý	k2eAgFnPc1d1	přecitlivělá
osoby	osoba	k1gFnPc1	osoba
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
příjmu	příjem	k1gInSc2	příjem
kofeinu	kofein	k1gInSc2	kofein
měly	mít	k5eAaImAgFnP	mít
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
zejména	zejména	k9	zejména
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
psychicky	psychicky	k6eAd1	psychicky
náročných	náročný	k2eAgMnPc2d1	náročný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
veřejné	veřejný	k2eAgNnSc1d1	veřejné
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgNnSc1d1	obchodní
jednání	jednání	k1gNnSc1	jednání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyžadujících	vyžadující	k2eAgFnPc2d1	vyžadující
koncentraci	koncentrace	k1gFnSc3	koncentrace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
řízení	řízení	k1gNnSc1	řízení
auta	auto	k1gNnSc2	auto
<g/>
)	)	kIx)	)
a	a	k8xC	a
před	před	k7c7	před
spaním	spaní	k1gNnSc7	spaní
<g/>
.	.	kIx.	.
</s>
<s>
Přecitlivělé	přecitlivělý	k2eAgFnPc1d1	přecitlivělá
osoby	osoba	k1gFnPc1	osoba
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
čínský	čínský	k2eAgInSc4d1	čínský
čaj	čaj	k1gInSc4	čaj
(	(	kIx(	(
<g/>
lhostejno	lhostejno	k6eAd1	lhostejno
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgInSc1d1	žlutý
či	či	k8xC	či
zelený	zelený	k2eAgMnSc1d1	zelený
<g/>
)	)	kIx)	)
připravovat	připravovat	k5eAaImF	připravovat
výhradně	výhradně	k6eAd1	výhradně
metodou	metoda	k1gFnSc7	metoda
druhého	druhý	k4xOgInSc2	druhý
záparu	zápar	k1gInSc2	zápar
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
látka	látka	k1gFnSc1	látka
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
míře	míra	k1gFnSc6	míra
z	z	k7c2	z
lístků	lístek	k1gInPc2	lístek
čaje	čaj	k1gInSc2	čaj
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
již	již	k6eAd1	již
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
krátkém	krátký	k2eAgInSc6d1	krátký
záparu	zápar	k1gInSc6	zápar
<g/>
.	.	kIx.	.
</s>
<s>
Abstinenční	abstinenční	k2eAgInPc1d1	abstinenční
příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
pravidelně	pravidelně	k6eAd1	pravidelně
konzumujících	konzumující	k2eAgFnPc2d1	konzumující
kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
kofeinových	kofeinový	k2eAgInPc2d1	kofeinový
nápojů	nápoj	k1gInPc2	nápoj
či	či	k8xC	či
tablet	tableta	k1gFnPc2	tableta
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
přípravků	přípravek	k1gInPc2	přípravek
obsahujích	obsahuje	k1gFnPc6	obsahuje
kofein	kofein	k1gInSc4	kofein
<g/>
,	,	kIx,	,
objevit	objevit	k5eAaPmF	objevit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
již	již	k6eAd1	již
vůči	vůči	k7c3	vůči
kofeinu	kofein	k1gInSc2	kofein
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
tolerance	tolerance	k1gFnSc1	tolerance
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
jeho	jeho	k3xOp3gFnSc2	jeho
konzumace	konzumace	k1gFnSc2	konzumace
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
příznaky	příznak	k1gInPc1	příznak
mají	mít	k5eAaImIp3nP	mít
tendence	tendence	k1gFnPc4	tendence
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
i	i	k9	i
3	[number]	k4	3
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
příznaky	příznak	k1gInPc1	příznak
ale	ale	k9	ale
mohou	moct	k5eAaImIp3nP	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
déle	dlouho	k6eAd2	dlouho
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
naopak	naopak	k6eAd1	naopak
zase	zase	k9	zase
kratší	krátký	k2eAgFnSc4d2	kratší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgInPc4	takový
příznaky	příznak	k1gInPc4	příznak
patří	patřit	k5eAaImIp3nP	patřit
především	především	k6eAd1	především
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
doba	doba	k1gFnSc1	doba
jejich	jejich	k3xOp3gNnPc2	jejich
trvání	trvání	k1gNnSc2	trvání
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
2-6	[number]	k4	2-6
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nejzávažnější	závažný	k2eAgFnSc4d3	nejzávažnější
z	z	k7c2	z
abstinenčních	abstinenční	k2eAgInPc2d1	abstinenční
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
až	až	k9	až
v	v	k7c4	v
migrénu	migréna	k1gFnSc4	migréna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
dočasné	dočasný	k2eAgNnSc4d1	dočasné
zvýšení	zvýšení	k1gNnSc4	zvýšení
cerebrálního	cerebrální	k2eAgInSc2d1	cerebrální
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Závažnost	závažnost	k1gFnSc1	závažnost
bolestí	bolestit	k5eAaImIp3nS	bolestit
hlavy	hlava	k1gFnPc4	hlava
pozitivně	pozitivně	k6eAd1	pozitivně
koreluje	korelovat	k5eAaImIp3nS	korelovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přímo	přímo	k6eAd1	přímo
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
dávky	dávka	k1gFnSc2	dávka
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
byl	být	k5eAaImAgMnS	být
konzument	konzument	k1gMnSc1	konzument
zvyklý	zvyklý	k2eAgMnSc1d1	zvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
dávka	dávka	k1gFnSc1	dávka
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
následují	následovat	k5eAaImIp3nP	následovat
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
vysazení	vysazení	k1gNnSc6	vysazení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
příznaky	příznak	k1gInPc4	příznak
patří	patřit	k5eAaImIp3nS	patřit
ospalost	ospalost	k1gFnSc1	ospalost
<g/>
,	,	kIx,	,
vyčerpanost	vyčerpanost	k1gFnSc1	vyčerpanost
a	a	k8xC	a
nervozita	nervozita	k1gFnSc1	nervozita
<g/>
.	.	kIx.	.
</s>
<s>
Příznakům	příznak	k1gInPc3	příznak
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
předejít	předejít	k5eAaPmF	předejít
postupným	postupný	k2eAgNnSc7d1	postupné
snižováním	snižování	k1gNnSc7	snižování
dávek	dávka	k1gFnPc2	dávka
během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
ubrat	ubrat	k5eAaPmF	ubrat
půl	půl	k1xP	půl
šálku	šálek	k1gInSc6	šálek
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Abstinenční	abstinenční	k2eAgInPc1d1	abstinenční
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
údajně	údajně	k6eAd1	údajně
způsobeny	způsoben	k2eAgFnPc1d1	způsobena
přecitlivělostí	přecitlivělost	k1gFnSc7	přecitlivělost
organismu	organismus	k1gInSc2	organismus
na	na	k7c4	na
adenosin	adenosin	k1gInSc4	adenosin
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Kofein	kofein	k1gInSc1	kofein
je	být	k5eAaImIp3nS	být
droga	droga	k1gFnSc1	droga
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
si	se	k3xPyFc3	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navyknou	navyknout	k5eAaPmIp3nP	navyknout
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k9	jako
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
zvyklé	zvyklý	k2eAgNnSc1d1	zvyklé
konzumovat	konzumovat	k5eAaBmF	konzumovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
čokolády	čokoláda	k1gFnSc2	čokoláda
a	a	k8xC	a
limonád	limonáda	k1gFnPc2	limonáda
<g/>
,	,	kIx,	,
najednou	najednou	k6eAd1	najednou
připraveno	připravit	k5eAaPmNgNnS	připravit
o	o	k7c4	o
přísun	přísun	k1gInSc4	přísun
kofeinu	kofein	k1gInSc2	kofein
<g/>
,	,	kIx,	,
projeví	projevit	k5eAaPmIp3nS	projevit
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
abstinenční	abstinenční	k2eAgInPc1d1	abstinenční
příznaky	příznak	k1gInPc1	příznak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
bolesti	bolest	k1gFnPc4	bolest
hlavy	hlava	k1gFnPc4	hlava
<g/>
,	,	kIx,	,
žaludeční	žaludeční	k2eAgFnPc4d1	žaludeční
křeče	křeč	k1gFnPc4	křeč
<g/>
,	,	kIx,	,
podrážděnost	podrážděnost	k1gFnSc4	podrážděnost
a	a	k8xC	a
deprese	deprese	k1gFnPc4	deprese
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
silně	silně	k6eAd1	silně
zatěžuje	zatěžovat	k5eAaImIp3nS	zatěžovat
endokrinní	endokrinní	k2eAgInSc1d1	endokrinní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
kofein	kofein	k1gInSc1	kofein
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
účinku	účinek	k1gInSc3	účinek
vitamín	vitamín	k1gInSc1	vitamín
B1	B1	k1gFnSc1	B1
(	(	kIx(	(
<g/>
thiamin	thiamin	k1gInSc1	thiamin
<g/>
)	)	kIx)	)
a	a	k8xC	a
inositol	inositol	k1gInSc4	inositol
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
močopudné	močopudný	k2eAgInPc4d1	močopudný
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
z	z	k7c2	z
organismu	organismus	k1gInSc2	organismus
odplavovat	odplavovat	k5eAaImF	odplavovat
draslík	draslík	k1gInSc4	draslík
a	a	k8xC	a
zinek	zinek	k1gInSc4	zinek
a	a	k8xC	a
zabraňovat	zabraňovat	k5eAaImF	zabraňovat
řádné	řádný	k2eAgFnSc3d1	řádná
asimilaci	asimilace	k1gFnSc3	asimilace
vápníku	vápník	k1gInSc2	vápník
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
!	!	kIx.	!
</s>
<s>
Pokud	pokud	k8xS	pokud
vaše	váš	k3xOp2gNnSc1	váš
dospívající	dospívající	k2eAgNnSc1d1	dospívající
dítě	dítě	k1gNnSc1	dítě
chce	chtít	k5eAaImIp3nS	chtít
šálek	šálek	k1gInSc4	šálek
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
nabídněte	nabídnout	k5eAaPmRp2nP	nabídnout
mu	on	k3xPp3gMnSc3	on
bezkofeinové	bezkofeinový	k2eAgInPc1d1	bezkofeinový
druhy	druh	k1gInPc1	druh
kávy	káva	k1gFnSc2	káva
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Pero	pero	k1gNnSc1	pero
<g/>
,	,	kIx,	,
Caffis	Caffis	k1gFnSc1	Caffis
<g/>
,	,	kIx,	,
Roastaroma	Roastaroma	k1gNnSc1	Roastaroma
a	a	k8xC	a
Postum	Postum	k1gNnSc1	Postum
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
když	když	k8xS	když
vaše	váš	k3xOp2gNnSc1	váš
dítě	dítě	k1gNnSc1	dítě
bude	být	k5eAaImBp3nS	být
chtít	chtít	k5eAaImF	chtít
šálek	šálek	k1gInSc4	šálek
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
široký	široký	k2eAgInSc1d1	široký
výběr	výběr	k1gInSc1	výběr
bylinných	bylinný	k2eAgInPc2d1	bylinný
čajů	čaj	k1gInPc2	čaj
rozmanité	rozmanitý	k2eAgFnSc2d1	rozmanitá
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
čokolády	čokoláda	k1gFnSc2	čokoláda
použijte	použít	k5eAaPmRp2nP	použít
svatojánský	svatojánský	k2eAgInSc4d1	svatojánský
chléb	chléb	k1gInSc4	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Zastavit	zastavit	k5eAaPmF	zastavit
špatné	špatný	k2eAgInPc4d1	špatný
zvyky	zvyk	k1gInPc4	zvyk
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
rozvinou	rozvinout	k5eAaPmIp3nP	rozvinout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
E.	E.	kA	E.
L.	L.	kA	L.
Mindell	Mindell	k1gMnSc1	Mindell
<g/>
)	)	kIx)	)
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
kofeinu	kofein	k1gInSc6	kofein
si	se	k3xPyFc3	se
utvoří	utvořit	k5eAaPmIp3nP	utvořit
nejen	nejen	k6eAd1	nejen
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kofeinu	kofein	k1gInSc2	kofein
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
útlumový	útlumový	k2eAgInSc4d1	útlumový
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
dávkách	dávka	k1gFnPc6	dávka
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
pocitu	pocit	k1gInSc3	pocit
podráždění	podráždění	k1gNnSc2	podráždění
<g/>
,	,	kIx,	,
neklidu	neklid	k1gInSc2	neklid
<g/>
,	,	kIx,	,
nespavosti	nespavost	k1gFnSc2	nespavost
<g/>
,	,	kIx,	,
stresu	stres	k1gInSc2	stres
<g/>
,	,	kIx,	,
rozrušení	rozrušení	k1gNnPc2	rozrušení
<g/>
,	,	kIx,	,
nesouvislému	souvislý	k2eNgInSc3d1	nesouvislý
toku	tok	k1gInSc3	tok
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
ztrátě	ztráta	k1gFnSc3	ztráta
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
i	i	k9	i
křečím	křeč	k1gFnPc3	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
bezpečnou	bezpečný	k2eAgFnSc4d1	bezpečná
denní	denní	k2eAgFnSc4d1	denní
dávku	dávka	k1gFnSc4	dávka
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
0,4	[number]	k4	0,4
g	g	kA	g
kofeinu	kofein	k1gInSc2	kofein
pro	pro	k7c4	pro
osobu	osoba	k1gFnSc4	osoba
vážící	vážící	k2eAgFnSc4d1	vážící
90,7	[number]	k4	90,7
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
individuální	individuální	k2eAgNnSc1d1	individuální
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
,	,	kIx,	,
případné	případný	k2eAgFnSc6d1	případná
přecitlivělosti	přecitlivělost	k1gFnSc6	přecitlivělost
na	na	k7c4	na
kofein	kofein	k1gInSc4	kofein
<g/>
,	,	kIx,	,
na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
ledvin	ledvina	k1gFnPc2	ledvina
i	i	k9	i
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
nebere	brát	k5eNaImIp3nS	brát
určité	určitý	k2eAgInPc4d1	určitý
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
např.	např.	kA	např.
právě	právě	k6eAd1	právě
ovlivňující	ovlivňující	k2eAgFnSc4d1	ovlivňující
činnost	činnost	k1gFnSc4	činnost
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Přecitlivělá	přecitlivělý	k2eAgFnSc1d1	přecitlivělá
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
arytmii	arytmie	k1gFnSc4	arytmie
i	i	k9	i
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
šálcích	šálek	k1gInPc6	šálek
kvalitní	kvalitní	k2eAgFnSc2d1	kvalitní
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
léčitelé	léčitel	k1gMnPc1	léčitel
jsou	být	k5eAaImIp3nP	být
ke	k	k7c3	k
kofeinu	kofein	k1gInSc3	kofein
nedůvěřiví	důvěřivý	k2eNgMnPc1d1	nedůvěřivý
-	-	kIx~	-
umějí	umět	k5eAaImIp3nP	umět
ho	on	k3xPp3gNnSc4	on
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
lék	lék	k1gInSc1	lék
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
toxicitě	toxicita	k1gFnSc6	toxicita
ještě	ještě	k6eAd1	ještě
nevíme	vědět	k5eNaImIp1nP	vědět
všechno	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
s	s	k7c7	s
teobrominem	teobromin	k1gInSc7	teobromin
(	(	kIx(	(
<g/>
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
čokoláda	čokoláda	k1gFnSc1	čokoláda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jsou	být	k5eAaImIp3nP	být
kofeinu	kofein	k1gInSc2	kofein
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Např.	např.	kA	např.
viz	vidět	k5eAaImRp2nS	vidět
Cayce	Cayce	k1gFnSc1	Cayce
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Koncentrace	koncentrace	k1gFnSc1	koncentrace
kofeinu	kofein	k1gInSc2	kofein
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obvykle	obvykle	k6eAd1	obvykle
pokládáme	pokládat	k5eAaImIp1nP	pokládat
za	za	k7c4	za
toxickou	toxický	k2eAgFnSc4d1	toxická
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
200	[number]	k4	200
μ	μ	k?	μ
a	a	k8xC	a
výš	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dávka	dávka	k1gFnSc1	dávka
10	[number]	k4	10
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zvýšení	zvýšení	k1gNnSc1	zvýšení
o	o	k7c4	o
5-10	[number]	k4	5-10
μ	μ	k?	μ
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
hodině	hodina	k1gFnSc6	hodina
od	od	k7c2	od
požití	požití	k1gNnSc2	požití
kofeinu	kofein	k1gInSc2	kofein
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
toxická	toxický	k2eAgFnSc1d1	toxická
dávka	dávka	k1gFnSc1	dávka
kofeinu	kofein	k1gInSc2	kofein
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
rozsahu	rozsah	k1gInSc2	rozsah
20-40	[number]	k4	20-40
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
1,8	[number]	k4	1,8
<g/>
-	-	kIx~	-
<g/>
3,6	[number]	k4	3,6
g	g	kA	g
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
osobu	osoba	k1gFnSc4	osoba
vážící	vážící	k2eAgFnSc4d1	vážící
90,7	[number]	k4	90,7
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Dle	dle	k7c2	dle
více	hodně	k6eAd2	hodně
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
dávce	dávka	k1gFnSc6	dávka
okolo	okolo	k7c2	okolo
10	[number]	k4	10
g.	g.	k?	g.
Důkazem	důkaz	k1gInSc7	důkaz
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
výsledky	výsledek	k1gInPc1	výsledek
forenzních	forenzní	k2eAgFnPc2d1	forenzní
pitev	pitva	k1gFnPc2	pitva
provedených	provedený	k2eAgFnPc2d1	provedená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
až	až	k9	až
2009	[number]	k4	2009
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
vliv	vliv	k1gInSc1	vliv
regulace	regulace	k1gFnSc2	regulace
prodeje	prodej	k1gInSc2	prodej
kofeinových	kofeinový	k2eAgFnPc2d1	kofeinová
tablet	tableta	k1gFnPc2	tableta
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
fatálních	fatální	k2eAgFnPc2d1	fatální
intoxikací	intoxikace	k1gFnPc2	intoxikace
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
zřejmě	zřejmě	k6eAd1	zřejmě
neprospívá	prospívat	k5eNaImIp3nS	prospívat
krátkodobým	krátkodobý	k2eAgInPc3d1	krátkodobý
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
intenzivním	intenzivní	k2eAgFnPc3d1	intenzivní
aktivitám	aktivita	k1gFnPc3	aktivita
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sprint	sprint	k1gInSc4	sprint
<g/>
,	,	kIx,	,
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
trénink	trénink	k1gInSc4	trénink
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kofein	kofein	k1gInSc1	kofein
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
výkonnost	výkonnost	k1gFnSc4	výkonnost
u	u	k7c2	u
vytrvalostních	vytrvalostní	k2eAgInPc2d1	vytrvalostní
sportů	sport	k1gInPc2	sport
(	(	kIx(	(
<g/>
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
využití	využití	k1gNnSc1	využití
tuků	tuk	k1gInPc2	tuk
jako	jako	k8xS	jako
energetického	energetický	k2eAgInSc2d1	energetický
zdroje	zdroj	k1gInSc2	zdroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zařadil	zařadit	k5eAaPmAgInS	zařadit
kofein	kofein	k1gInSc1	kofein
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zjištění	zjištění	k1gNnSc2	zjištění
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
12	[number]	k4	12
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
ml	ml	kA	ml
moči	moč	k1gFnSc6	moč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
však	však	k9	však
již	již	k6eAd1	již
kofein	kofein	k1gInSc1	kofein
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
zakázané	zakázaný	k2eAgFnPc4d1	zakázaná
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
norma	norma	k1gFnSc1	norma
povoluje	povolovat	k5eAaImIp3nS	povolovat
maximálně	maximálně	k6eAd1	maximálně
72	[number]	k4	72
mg	mg	kA	mg
kofeinu	kofein	k1gInSc2	kofein
na	na	k7c6	na
3	[number]	k4	3
dl	dl	k?	dl
limonády	limonáda	k1gFnSc2	limonáda
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
0,24	[number]	k4	0,24
‰	‰	k?	‰
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
