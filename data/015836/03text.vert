<s>
Def	Def	k?
Leppard	Leppard	k1gInSc1
</s>
<s>
Def	Def	k?
Leppard	Leppard	k1gMnSc1
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Původ	původ	k1gInSc1
</s>
<s>
Sheffield	Sheffield	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
Žánry	žánr	k1gInPc5
</s>
<s>
Hard	Hard	k6eAd1
rock	rock	k1gInSc1
<g/>
,	,	kIx,
heavy	heava	k1gFnPc1
metal	metal	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
glam	glam	k1gInSc1
metal	metal	k1gInSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
1977	#num#	k4
-	-	kIx~
současnost	současnost	k1gFnSc4
Vydavatelé	vydavatel	k1gMnPc5
</s>
<s>
Mercury	Mercura	k1gFnPc1
<g/>
,	,	kIx,
Universal	Universal	k1gFnPc1
<g/>
,	,	kIx,
Phonogram	Phonogram	k1gInSc1
<g/>
,	,	kIx,
Vertigo	Vertigo	k1gNnSc1
<g/>
,	,	kIx,
PolyGram	PolyGram	k1gInSc1
<g/>
,	,	kIx,
Bludgeon-Riffola	Bludgeon-Riffola	k1gFnSc1
<g/>
,	,	kIx,
Island	Island	k1gInSc1
(	(	kIx(
<g/>
US	US	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Frontiers	Frontiers	k1gInSc1
(	(	kIx(
<g/>
EU	EU	kA
<g/>
)	)	kIx)
Příbuzná	příbuzný	k2eAgNnPc1d1
témata	téma	k1gNnPc1
</s>
<s>
Atomic	Atomic	k1gMnSc1
MassCybernauts	MassCybernautsa	k1gFnPc2
Web	web	k1gInSc4
</s>
<s>
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Joe	Joe	k?
ElliottRick	ElliottRick	k1gMnSc1
SavageRick	SavageRick	k1gMnSc1
AllenPhil	AllenPhil	k1gMnSc1
CollenVivian	CollenVivian	k1gMnSc1
Campbell	Campbell	k1gMnSc1
Dřívější	dřívější	k2eAgFnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Steve	Steve	k1gMnSc1
ClarkPete	ClarkPe	k1gNnSc2
WillisTony	WillisTona	k1gFnSc2
KenningFrank	KenningFrank	k1gMnSc1
Noon	Noon	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Def	Def	k?
Leppard	Leppard	k1gInSc1
je	být	k5eAaImIp3nS
britská	britský	k2eAgFnSc1d1
hardrocková	hardrockový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zformovala	zformovat	k5eAaPmAgFnS
v	v	k7c6
Sheffieldu	Sheffield	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
své	svůj	k3xOyFgFnSc2
popularity	popularita	k1gFnSc2
byla	být	k5eAaImAgFnS
na	na	k7c6
přelomu	přelom	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgNnSc1
vystoupení	vystoupení	k1gNnSc1
kapely	kapela	k1gFnSc2
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
o	o	k7c6
Vánocích	Vánoce	k1gFnPc6
1977	#num#	k4
pro	pro	k7c4
6	#num#	k4
přátel	přítel	k1gMnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
6	#num#	k4
skladeb	skladba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
vychází	vycházet	k5eAaImIp3nS
debut	debut	k1gInSc4
On	on	k3xPp3gMnSc1
Through	Through	k1gMnSc1
the	the	k?
Night	Night	k1gInSc1
<g/>
,	,	kIx,
rok	rok	k1gInSc1
na	na	k7c4
to	ten	k3xDgNnSc4
High	High	k1gMnSc1
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
Dry	Dry	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
let	léto	k1gNnPc2
se	se	k3xPyFc4
Def	Def	k1gMnPc1
Leppard	Lepparda	k1gFnPc2
dostávali	dostávat	k5eAaImAgMnP
výš	vysoce	k6eAd2
a	a	k8xC
výš	vysoce	k6eAd2
na	na	k7c6
žebříčku	žebříček	k1gInSc6
popularity	popularita	k1gFnSc2
<g/>
,	,	kIx,
velkým	velký	k2eAgInSc7d1
skokem	skok	k1gInSc7
kupředu	kupředu	k6eAd1
bylo	být	k5eAaImAgNnS
album	album	k1gNnSc1
Pyromania	Pyromanium	k1gNnSc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prodalo	prodat	k5eAaPmAgNnS
se	se	k3xPyFc4
přes	přes	k7c4
7	#num#	k4
milionů	milion	k4xCgInPc2
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
nahráním	nahrání	k1gNnSc7
tohoto	tento	k3xDgNnSc2
alba	album	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
personální	personální	k2eAgFnSc3d1
změně	změna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kytarista	kytarista	k1gMnSc1
Pete	Pet	k1gInSc2
Willis	Willis	k1gInSc1
byl	být	k5eAaImAgInS
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
problémy	problém	k1gInPc4
s	s	k7c7
alkoholem	alkohol	k1gInSc7
nahrazen	nahradit	k5eAaPmNgInS
Philem	Phil	k1gMnSc7
Collenem	Collen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Def	Def	k?
Leppard	Leppard	k1gMnSc1
byli	být	k5eAaImAgMnP
několikrát	několikrát	k6eAd1
stíháni	stíhat	k5eAaImNgMnP
nepřízní	nepřízeň	k1gFnSc7
osudu	osud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bubeník	bubeník	k1gMnSc1
Rick	Rick	k1gMnSc1
Allen	Allen	k1gMnSc1
ztratil	ztratit	k5eAaPmAgMnS
při	při	k7c6
autohavárii	autohavárie	k1gFnSc6
levou	levý	k2eAgFnSc4d1
paži	paže	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
ani	ani	k8xC
to	ten	k3xDgNnSc1
nebylo	být	k5eNaImAgNnS
překážkou	překážka	k1gFnSc7
pro	pro	k7c4
další	další	k2eAgNnSc4d1
pokračování	pokračování	k1gNnSc4
ve	v	k7c6
hře	hra	k1gFnSc6
na	na	k7c4
bicí	bicí	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
zpěváka	zpěvák	k1gMnSc2
Joe	Joe	k1gMnSc2
Elliotta	Elliott	k1gMnSc2
se	se	k3xPyFc4
díky	díky	k7c3
elektronickému	elektronický	k2eAgInSc3d1
systému	systém	k1gInSc3
stal	stát	k5eAaPmAgMnS
Rick	Rick	k1gMnSc1
„	„	k?
<g/>
lepším	dobrý	k2eAgMnSc7d2
bubeníkem	bubeník	k1gMnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
než	než	k8xS
byl	být	k5eAaImAgInS
kdy	kdy	k6eAd1
před	před	k7c7
tím	ten	k3xDgNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rickova	Rickův	k2eAgFnSc1d1
souprava	souprava	k1gFnSc1
byla	být	k5eAaImAgFnS
kompletně	kompletně	k6eAd1
elektronická	elektronický	k2eAgFnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1986	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
Rick	Rick	k1gMnSc1
pomalu	pomalu	k6eAd1
vracel	vracet	k5eAaImAgMnS
k	k	k7c3
akustickým	akustický	k2eAgMnPc3d1
bicím	bicí	k2eAgMnPc3d1
<g/>
.	.	kIx.
</s>
<s>
Prvním	první	k4xOgNnSc7
albem	album	k1gNnSc7
nahraným	nahraný	k2eAgInSc7d1
po	po	k7c6
Rickově	Rickův	k2eAgFnSc6d1
nehodě	nehoda	k1gFnSc6
je	být	k5eAaImIp3nS
veleúspěšná	veleúspěšný	k2eAgNnPc4d1
Hysteria	Hysterium	k1gNnPc4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
prodej	prodej	k1gInSc4
tohoto	tento	k3xDgNnSc2
alba	album	k1gNnSc2
se	se	k3xPyFc4
vyšplhal	vyšplhat	k5eAaPmAgMnS
na	na	k7c4
neuvěřitelných	uvěřitelný	k2eNgInPc2d1
17	#num#	k4
milionů	milion	k4xCgInPc2
nosičů	nosič	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
obří	obří	k2eAgNnSc4d1
světové	světový	k2eAgNnSc4d1
turné	turné	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
Americe	Amerika	k1gFnSc6
dokonce	dokonce	k9
hrají	hrát	k5eAaImIp3nP
„	„	k?
<g/>
v	v	k7c6
kruhu	kruh	k1gInSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k9
uprostřed	uprostřed	k7c2
haly	hala	k1gFnSc2
s	s	k7c7
hledištěm	hlediště	k1gNnSc7
kolem	kolem	k6eAd1
dokola	dokola	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
uštědřil	uštědřit	k5eAaPmAgMnS
osud	osud	k1gInSc4
kapele	kapela	k1gFnSc3
další	další	k2eAgInPc4d1
<g/>
,	,	kIx,
tentokráte	tentokráte	k?
nejtvrdší	tvrdý	k2eAgFnSc4d3
ránu	rána	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrtelné	smrtelný	k2eAgFnSc6d1
kombinaci	kombinace	k1gFnSc6
alkoholu	alkohol	k1gInSc2
a	a	k8xC
drog	droga	k1gFnPc2
umírá	umírat	k5eAaImIp3nS
dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
londýnském	londýnský	k2eAgInSc6d1
bytě	byt	k1gInSc6
ve	v	k7c6
věku	věk	k1gInSc6
30	#num#	k4
let	léto	k1gNnPc2
vynikající	vynikající	k2eAgMnSc1d1
kytarista	kytarista	k1gMnSc1
Steve	Steve	k1gMnSc1
Clark	Clark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
ostatní	ostatní	k2eAgMnPc4d1
členy	člen	k1gMnPc4
kapely	kapela	k1gFnSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
obrovská	obrovský	k2eAgFnSc1d1
ztráta	ztráta	k1gFnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
se	se	k3xPyFc4
jen	jen	k9
těžce	těžce	k6eAd1
srovnávají	srovnávat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
album	album	k1gNnSc4
Adrenalize	Adrenalize	k1gFnSc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nahráváno	nahráván	k2eAgNnSc1d1
pouze	pouze	k6eAd1
ve	v	k7c6
čtyřech	čtyři	k4xCgMnPc6
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc4
kytarové	kytarový	k2eAgFnPc4d1
party	parta	k1gFnPc4
má	mít	k5eAaImIp3nS
na	na	k7c6
svých	svůj	k3xOyFgNnPc6
bedrech	bedra	k1gNnPc6
Phil	Phil	k1gMnSc1
Collen	Collen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhostil	zhostit	k5eAaPmAgMnS
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
však	však	k9
výborně	výborně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
albu	album	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
piseň	piseň	k1gFnSc1
White	Whit	k1gInSc5
Lightning	Lightning	k1gInSc1
<g/>
,	,	kIx,
věnovaná	věnovaný	k2eAgFnSc1d1
památce	památka	k1gFnSc3
Steva	Steve	k1gMnSc2
Clarka	Clarek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
poctě	pocta	k1gFnSc6
Freddiemu	Freddiem	k1gInSc2
Mercurymu	Mercurym	k1gInSc2
na	na	k7c6
stadionu	stadion	k1gInSc6
ve	v	k7c4
Wembley	Wemble	k1gMnPc4
byl	být	k5eAaImAgMnS
představen	představen	k2eAgMnSc1d1
nový	nový	k2eAgMnSc1d1
člen	člen	k1gMnSc1
Def	Def	k1gMnSc1
Leppard	Lepparda	k1gFnPc2
–	–	k?
Vivian	Viviana	k1gFnPc2
Campbell	Campbell	k1gMnSc1
(	(	kIx(
<g/>
Dio	Dio	k1gMnSc1
<g/>
,	,	kIx,
Whitesnake	Whitesnake	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vivian	Viviana	k1gFnPc2
do	do	k7c2
kapely	kapela	k1gFnSc2
perfektně	perfektně	k6eAd1
zapadl	zapadnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Americe	Amerika	k1gFnSc6
byla	být	k5eAaImAgFnS
Adrenalize	Adrenalize	k1gFnSc1
číslo	číslo	k1gNnSc1
jedna	jeden	k4xCgFnSc1
a	a	k8xC
zůstala	zůstat	k5eAaPmAgNnP
tam	tam	k6eAd1
dalších	další	k2eAgInPc2d1
pět	pět	k4xCc4
týdnů	týden	k1gInPc2
s	s	k7c7
prodejem	prodej	k1gInSc7
šesti	šest	k4xCc2
miliónů	milión	k4xCgInPc2
nosičů	nosič	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
Mexiku	Mexiko	k1gNnSc6
a	a	k8xC
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nejprodávanějším	prodávaný	k2eAgNnSc7d3
albem	album	k1gNnSc7
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
je	být	k5eAaImIp3nS
vydáno	vydat	k5eAaPmNgNnS
retrospektivní	retrospektivní	k2eAgNnSc1d1
album	album	k1gNnSc1
Retro	retro	k1gNnSc2
Active	Actiev	k1gFnSc2
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgMnSc1d1
starší	starší	k1gMnSc1
dosud	dosud	k6eAd1
nevydané	vydaný	k2eNgFnSc2d1
skladby	skladba	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
spoustě	spousta	k1gFnSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
hraje	hrát	k5eAaImIp3nS
ještě	ještě	k9
Steve	Steve	k1gMnSc1
Clark	Clark	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc1d1
album	album	k1gNnSc1
Vault	Vaulta	k1gFnPc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
přineslo	přinést	k5eAaPmAgNnS
výběr	výběr	k1gInSc1
největších	veliký	k2eAgInPc2d3
hitů	hit	k1gInPc2
kapely	kapela	k1gFnSc2
+	+	kIx~
jednu	jeden	k4xCgFnSc4
novou	nový	k2eAgFnSc4d1
skladbu	skladba	k1gFnSc4
When	Whena	k1gFnPc2
Love	lov	k1gInSc5
&	&	k?
Hate	Hatus	k1gMnSc5
Collide	Collid	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
na	na	k7c4
to	ten	k3xDgNnSc4
vychází	vycházet	k5eAaImIp3nS
Slang	slang	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
netradiční	tradiční	k2eNgNnSc4d1
dílo	dílo	k1gNnSc4
s	s	k7c7
nezvyklým	zvyklý	k2eNgInSc7d1
soundem	sound	k1gInSc7
<g/>
,	,	kIx,
spousta	spousta	k1gFnSc1
fanoušků	fanoušek	k1gMnPc2
byla	být	k5eAaImAgFnS
touto	tento	k3xDgFnSc7
změnou	změna	k1gFnSc7
zaskočena	zaskočit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
1999	#num#	k4
přinesl	přinést	k5eAaPmAgInS
album	album	k1gNnSc4
Euphoria	Euphorium	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
kritiky	kritik	k1gMnPc4
oprávněně	oprávněně	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
návrat	návrat	k1gInSc4
ke	k	k7c3
kořenům	kořen	k1gInPc3
a	a	k8xC
ke	k	k7c3
klasické	klasický	k2eAgFnSc3d1
tvorbě	tvorba	k1gFnSc3
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
vychází	vycházet	k5eAaImIp3nS
desáté	desátý	k4xOgNnSc1
album	album	k1gNnSc1
<g/>
,	,	kIx,
pojmenované	pojmenovaný	k2eAgNnSc1d1
jednoduše	jednoduše	k6eAd1
X	X	kA
(	(	kIx(
<g/>
prostě	prostě	k6eAd1
římských	římský	k2eAgNnPc2d1
10	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
vychází	vycházet	k5eAaImIp3nS
další	další	k2eAgNnSc1d1
dvě	dva	k4xCgNnPc1
výběrová	výběrový	k2eAgNnPc1d1
alba	album	k1gNnPc1
<g/>
,	,	kIx,
Best	Best	k1gInSc1
of	of	k?
<g/>
'	'	kIx"
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Rock	rock	k1gInSc1
of	of	k?
Ages	Ages	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Definitive	Definitiv	k1gInSc5
Collection	Collection	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
pak	pak	k6eAd1
vychází	vycházet	k5eAaImIp3nS
album	album	k1gNnSc4
Yeah	Yeaha	k1gFnPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
sbírka	sbírka	k1gFnSc1
coververzí	coververze	k1gFnPc2
z	z	k7c2
let	léto	k1gNnPc2
sedmdesátých	sedmdesátý	k4xOgNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
kapely	kapela	k1gFnSc2
tímto	tento	k3xDgNnSc7
albem	album	k1gNnSc7
chtěli	chtít	k5eAaImAgMnP
současné	současný	k2eAgFnSc3d1
generaci	generace	k1gFnSc3
připomenout	připomenout	k5eAaPmF
éru	éra	k1gFnSc4
glam	glama	k1gFnPc2
rocku	rock	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
kapela	kapela	k1gFnSc1
vzešla	vzejít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
byla	být	k5eAaImAgFnS
kapela	kapela	k1gFnSc1
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
Rock	rock	k1gInSc1
and	and	k?
Roll	Roll	k1gInSc1
Hall	Hall	k1gInSc1
of	of	k?
Fame	Fame	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvodní	úvodní	k2eAgInSc1d1
slovo	slovo	k1gNnSc4
přednesl	přednést	k5eAaPmAgMnS
Brian	Brian	k1gMnSc1
May	May	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Sestava	sestava	k1gFnSc1
</s>
<s>
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Joe	Joe	k?
Elliott	Elliott	k1gInSc1
–	–	k?
zpěv	zpěv	k1gInSc1
<g/>
,	,	kIx,
klávesy	kláves	k1gInPc1
<g/>
,	,	kIx,
kytara	kytara	k1gFnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
-dosud	-dosudo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Phil	Phil	k1gInSc1
Collen	Collen	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
-dosud	-dosudo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Vivian	Viviana	k1gFnPc2
Campbell	Campbell	k1gMnSc1
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
doprovodný	doprovodný	k2eAgInSc1d1
zpěv	zpěv	k1gInSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
-dosud	-dosudo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Rick	Rick	k1gMnSc1
Savage	Savag	k1gFnSc2
–	–	k?
baskytara	baskytara	k1gFnSc1
<g/>
,	,	kIx,
klávesy	klávesa	k1gFnPc1
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
-dosud	-dosudo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Rick	Rick	k1gMnSc1
Allen	Allen	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgMnSc1d1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
-dosud	-dosudo	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Steve	Steve	k1gMnSc1
Clark	Clark	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
doprovodný	doprovodný	k2eAgInSc1d1
zpěv	zpěv	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pete	Pete	k1gFnSc1
Willis	Willis	k1gFnSc2
–	–	k?
kytara	kytara	k1gFnSc1
<g/>
,	,	kIx,
doprovodný	doprovodný	k2eAgInSc1d1
zpěv	zpěv	k1gInSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
-	-	kIx~
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tony	Tony	k1gMnSc1
Kenning	Kenning	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
-	-	kIx~
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Frank	Frank	k1gMnSc1
Noon	Noon	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgMnSc1d1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Muzikanti	muzikant	k1gMnPc1
na	na	k7c4
tour	tour	k1gInSc4
</s>
<s>
Jeff	Jeff	k1gMnSc1
Rich	Rich	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgMnSc1d1
<g/>
,	,	kIx,
perkuse	perkuse	k1gFnSc1
(	(	kIx(
<g/>
srpen	srpen	k1gInSc1
1986	#num#	k4
–	–	k?
pomocník	pomocník	k1gMnSc1
Ricka	Ricek	k1gMnSc2
Allena	Allen	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Sinéad	Sinéad	k6eAd1
Madden	Maddno	k1gNnPc2
–	–	k?
housle	housle	k1gFnPc4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Časový	časový	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
The	The	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
E.	E.	kA
<g/>
P.	P.	kA
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
On	on	k3xPp3gMnSc1
Through	Through	k1gMnSc1
The	The	k1gMnSc1
Night	Night	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
High	High	k1gMnSc1
'	'	kIx"
<g/>
N	N	kA
<g/>
'	'	kIx"
Dry	Dry	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pyromania	Pyromanium	k1gNnPc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hysteria	Hysterium	k1gNnPc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Adrenalize	Adrenalize	k1gFnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Retro	retro	k1gNnSc1
Active	Actiev	k1gFnSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vault	Vault	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slang	slang	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Euphoria	Euphorium	k1gNnPc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
X	X	kA
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Best	Best	k1gMnSc1
Of	Of	k1gMnSc1
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
(	(	kIx(
<g/>
kompilace	kompilace	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rock	rock	k1gInSc1
Of	Of	k1gFnSc2
Ages	Agesa	k1gFnPc2
(	(	kIx(
<g/>
kompilace	kompilace	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Yeah	Yeah	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
album	album	k1gNnSc4
s	s	k7c7
covery	covero	k1gNnPc7
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hysteria	Hysterium	k1gNnPc1
(	(	kIx(
<g/>
Deluxe	Deluxe	k1gFnSc1
Edition	Edition	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Songs	Songs	k6eAd1
From	From	k1gMnSc1
The	The	k1gMnSc1
Sparkle	Sparkl	k1gInSc5
Loungle	Loungle	k1gNnPc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
World	World	k1gMnSc1
-	-	kIx~
Discography	Discograph	k1gInPc1
</s>
<s>
DVD	DVD	kA
<g/>
/	/	kIx~
<g/>
Video	video	k1gNnSc1
</s>
<s>
Historia	Historium	k1gNnPc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
In	In	k?
The	The	k1gFnSc1
Round	round	k1gInSc1
In	In	k1gFnSc1
Your	Your	k1gMnSc1
Face	Face	k1gInSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Visualize	Visualize	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Video	video	k1gNnSc1
Archive	archiv	k1gInSc5
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Euphoria	Euphorium	k1gNnPc1
Tour	Toura	k1gFnPc2
-	-	kIx~
Live	Live	k1gFnSc1
In	In	k1gMnSc1
Japan	japan	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Best	Best	k1gMnSc1
Of	Of	k1gMnSc1
-	-	kIx~
The	The	k1gMnSc1
Videos	Videos	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
<g/>
:	:	kIx,
Biography	Biograph	k1gInPc1
:	:	kIx,
Rolling	Rolling	k1gInSc1
Stone	ston	k1gInSc5
<g/>
↑	↑	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
–	–	k?
Biography	Biographa	k1gFnSc2
Allmusic	Allmusice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
All	All	k1gMnSc1
Music	Music	k1gMnSc1
Guide	Guid	k1gInSc5
–	–	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,1	,1	k4
2	#num#	k4
Hair	Haira	k1gFnPc2
metal	metat	k5eAaImAgMnS
grows	grows	k6eAd1
back	back	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
'	'	kIx"
<g/>
Net	Net	k1gMnSc1
The	The	k1gMnSc1
Seattle	Seattle	k1gFnSc2
Times	Times	k1gMnSc1
<g/>
↑	↑	k?
Metal	metal	k1gInSc1
–	–	k?
A	a	k8xC
Headbanger	Headbangero	k1gNnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Journey	Journey	k1gInPc7
<g/>
,	,	kIx,
DVD	DVD	kA
<g/>
,	,	kIx,
ASIN	ASIN	kA
B000FS9OZY	B000FS9OZY	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
RHODES	RHODES	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Glam	Glam	k1gInSc1
Metal	metat	k5eAaImAgInS
101	#num#	k4
-	-	kIx~
history	histor	k1gInPc1
<g/>
,	,	kIx,
description	description	k1gInSc1
and	and	k?
examples	examples	k1gInSc1
of	of	k?
glam	glam	k6eAd1
metal	metat	k5eAaImAgInS
rock-	rock-	k?
About	About	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
About	About	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Top	topit	k5eAaImRp2nS
20	#num#	k4
Hair	Hair	k1gMnSc1
Metal	metat	k5eAaImAgMnS
Albums	Albums	k1gInSc4
of	of	k?
the	the	k?
Eighties	Eighties	k1gMnSc1
–	–	k?
Guitar	Guitar	k1gMnSc1
World	World	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guitar	Guitar	k1gMnSc1
World	World	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
22	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sinéad	Sinéad	k1gInSc1
Madden	Maddna	k1gFnPc2
biography	biographa	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
16	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
at	at	k?
Wild	Wild	k1gInSc1
Heart	Heart	k1gInSc1
Management	management	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Def	Def	k1gFnSc2
Leppard	Leppard	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Def	Def	k?
Leppard	Leppard	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
-	-	kIx~
oficiální	oficiální	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
</s>
<s>
The	The	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
World	World	k1gMnSc1
-	-	kIx~
Discography	Discograph	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
Joe	Joe	k1gMnSc1
Elliott	Elliott	k1gMnSc1
•	•	k?
Phil	Phil	k1gInSc1
Collen	Collen	k1gInSc1
•	•	k?
Vivian	Viviana	k1gFnPc2
Campbell	Campbell	k1gMnSc1
•	•	k?
Rick	Rick	k1gMnSc1
Savage	Savag	k1gFnSc2
•	•	k?
Rick	Rick	k1gMnSc1
AllenSteve	AllenSteev	k1gFnSc2
Clark	Clark	k1gInSc1
•	•	k?
Pete	Pete	k1gInSc1
Willis	Willis	k1gFnSc1
•	•	k?
Tony	tonus	k1gInPc1
Kenning	Kenning	k1gInSc1
•	•	k?
Frank	frank	k1gInSc1
Noon	Noon	k1gInSc1
Studiová	studiový	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
On	on	k3xPp3gMnSc1
Through	Through	k1gMnSc1
the	the	k?
Night	Night	k1gMnSc1
•	•	k?
High	High	k1gMnSc1
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
Dry	Dry	k1gFnSc3
•	•	k?
Pyromania	Pyromanium	k1gNnSc2
•	•	k?
Hysteria	Hysterium	k1gNnSc2
•	•	k?
Adrenalize	Adrenalize	k1gFnSc1
•	•	k?
Slang	slang	k1gInSc1
•	•	k?
Euphoria	Euphorium	k1gNnSc2
•	•	k?
X	X	kA
•	•	k?
Songs	Songs	k1gInSc1
from	from	k1gInSc1
the	the	k?
Sparkle	Sparkl	k1gInSc5
Lounge	Lounge	k1gNnPc7
•	•	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
Koncertní	koncertní	k2eAgNnSc4d1
alba	album	k1gNnSc2
</s>
<s>
Mirrorball	Mirrorball	k1gInSc1
Kompilace	kompilace	k1gFnSc2
</s>
<s>
Retro	retro	k1gNnSc1
Active	Actiev	k1gFnSc2
•	•	k?
Vault	Vault	k1gMnSc1
<g/>
:	:	kIx,
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
Greatest	Greatest	k1gMnSc1
Hits	Hitsa	k1gFnPc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
–	–	k?
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Best	Best	k1gMnSc1
of	of	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
•	•	k?
Rock	rock	k1gInSc1
of	of	k?
Ages	Ages	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Definitive	Definitiv	k1gInSc5
Collection	Collection	k1gInSc1
Cover	Covra	k1gFnPc2
alba	album	k1gNnSc2
</s>
<s>
Yeah	Yeah	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
EP	EP	kA
</s>
<s>
The	The	k?
Def	Def	k1gMnSc1
Leppard	Leppard	k1gMnSc1
E.	E.	kA
<g/>
P.	P.	kA
•	•	k?
Live	Live	k1gInSc1
<g/>
:	:	kIx,
In	In	k1gFnSc1
the	the	k?
Clubs	Clubs	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
Your	Your	k1gInSc4
Face	Fac	k1gInSc2
Koncertní	koncertní	k2eAgNnSc4d1
turné	turné	k1gNnSc4
</s>
<s>
Def	Def	k?
Leppard	Leppard	k1gInSc1
Early	earl	k1gMnPc4
Tours	Toursa	k1gFnPc2
1978-1979	1978-1979	k4
•	•	k?
On	on	k3xPp3gMnSc1
Through	Through	k1gMnSc1
the	the	k?
Night	Night	k1gMnSc1
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
High	High	k1gMnSc1
'	'	kIx"
<g/>
n	n	k0
<g/>
'	'	kIx"
Dry	Dry	k1gMnSc1
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Pyromania	Pyromanium	k1gNnSc2
World	Worlda	k1gFnPc2
Tour	Tour	k1gMnSc1
•	•	k?
Hysteria	Hysterium	k1gNnSc2
World	Worlda	k1gFnPc2
Tour	Tour	k1gMnSc1
•	•	k?
Seven	Seven	k1gInSc1
Day	Day	k1gFnPc2
Weekend	weekend	k1gInSc1
Tour	Tour	k1gMnSc1
•	•	k?
Slang	slang	k1gInSc1
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Euphoria	Euphorium	k1gNnSc2
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
1999-2001	1999-2001	k4
•	•	k?
X	X	kA
World	World	k1gInSc1
Tour	Tour	k1gInSc1
2002-2003	2002-2003	k4
•	•	k?
Rock	rock	k1gInSc1
of	of	k?
Ages	Ages	k1gInSc1
Tour	Tour	k1gInSc1
2005	#num#	k4
•	•	k?
Yeah	Yeah	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Tour	Tour	k1gMnSc1
•	•	k?
Downstage	Downstag	k1gFnSc2
Thrust	Thrust	k1gMnSc1
Tour	Tour	k1gMnSc1
•	•	k?
Songs	Songs	k1gInSc1
from	from	k1gInSc1
the	the	k?
Sparkle	Sparkl	k1gInSc5
Lounge	Loungus	k1gInSc5
Tour	Tour	k1gInSc4
Příbuzná	příbuzný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
</s>
<s>
Robert	Robert	k1gMnSc1
John	John	k1gMnSc1
"	"	kIx"
<g/>
Mutt	Mutt	k1gMnSc1
<g/>
"	"	kIx"
Lange	Lange	k1gInSc1
•	•	k?
Jeff	Jeff	k1gInSc1
Rich	Rich	k1gMnSc1
•	•	k?
Ross	Ross	k1gInSc1
Halfin	Halfin	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18306	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
5121011-3	5121011-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2337	#num#	k4
2817	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
86078759	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
146664851	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
86078759	#num#	k4
</s>
