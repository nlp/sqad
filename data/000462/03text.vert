<s>
Cetina	Cetin	k2eAgFnSc1d1	Cetin
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
(	(	kIx(	(
<g/>
Splitsko-dalmatská	splitskoalmatský	k2eAgFnSc1d1	splitsko-dalmatský
župa	župa	k1gFnSc1	župa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
105	[number]	k4	105
km	km	kA	km
<g/>
,	,	kIx,	,
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
oblast	oblast	k1gFnSc4	oblast
3700	[number]	k4	3700
km2	km2	k4	km2
<g/>
,	,	kIx,	,
a	a	k8xC	a
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
385	[number]	k4	385
m	m	kA	m
(	(	kIx(	(
<g/>
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
až	až	k6eAd1	až
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cetina	Cetina	k1gFnSc1	Cetina
pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
severozápadních	severozápadní	k2eAgInPc6d1	severozápadní
svazích	svah	k1gInPc6	svah
Dinárského	dinárský	k2eAgNnSc2d1	Dinárské
pohoří	pohoří	k1gNnSc2	pohoří
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
vesničce	vesnička	k1gFnSc6	vesnička
nazvané	nazvaný	k2eAgFnSc6d1	nazvaná
Cetina	Cetin	k2eAgFnSc1d1	Cetin
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
7	[number]	k4	7
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Vrliky	Vrlika	k1gFnSc2	Vrlika
<g/>
.	.	kIx.	.
</s>
<s>
Cetina	Cetina	k1gFnSc1	Cetina
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
nižší	nízký	k2eAgFnSc2d2	nižší
části	část	k1gFnSc2	část
krasového	krasový	k2eAgNnSc2d1	krasové
pole	pole	k1gNnSc2	pole
u	u	k7c2	u
Sinje	Sinj	k1gFnSc2	Sinj
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
opět	opět	k6eAd1	opět
na	na	k7c4	na
západ	západ	k1gInSc4	západ
kolem	kolem	k7c2	kolem
hory	hora	k1gFnSc2	hora
Mosor	Mosor	k1gInSc1	Mosor
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Omiš	Omiš	k1gInSc4	Omiš
<g/>
,	,	kIx,	,
vtéká	vtékat	k5eAaImIp3nS	vtékat
do	do	k7c2	do
Jadranu	Jadran	k1gInSc2	Jadran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
řeka	řeka	k1gFnSc1	řeka
na	na	k7c4	na
západ	západ	k1gInSc4	západ
u	u	k7c2	u
Zadvarje	Zadvarj	k1gFnSc2	Zadvarj
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
hlubokým	hluboký	k2eAgInSc7d1	hluboký
kaňonem	kaňon	k1gInSc7	kaňon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
malebné	malebný	k2eAgInPc4d1	malebný
vodopády	vodopád	k1gInPc4	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Malebný	malebný	k2eAgInSc1d1	malebný
je	být	k5eAaImIp3nS	být
i	i	k9	i
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
řeku	řeka	k1gFnSc4	řeka
poblíž	poblíž	k7c2	poblíž
Omiše	Omiše	k1gFnSc2	Omiše
z	z	k7c2	z
úbočí	úbočí	k1gNnSc2	úbočí
pohoří	pohoří	k1gNnSc2	pohoří
Mosor	Mosor	k1gMnSc1	Mosor
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc4d1	velký
výškový	výškový	k2eAgInSc4d1	výškový
spád	spád	k1gInSc4	spád
na	na	k7c6	na
konečné	konečný	k2eAgFnSc6d1	konečná
části	část	k1gFnSc6	část
Cetiny	Cetina	k1gFnSc2	Cetina
byl	být	k5eAaImAgMnS	být
využit	využít	k5eAaPmNgMnS	využít
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
několika	několik	k4yIc2	několik
významných	významný	k2eAgFnPc2d1	významná
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
balena	balen	k2eAgFnSc1d1	balena
a	a	k8xC	a
prodávána	prodáván	k2eAgFnSc1d1	prodávána
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Cetina	Cetino	k1gNnSc2	Cetino
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
malebnosti	malebnost	k1gFnSc3	malebnost
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
silnému	silný	k2eAgInSc3d1	silný
proudu	proud	k1gInSc3	proud
a	a	k8xC	a
množství	množství	k1gNnSc6	množství
peřejí	peřej	k1gFnPc2	peřej
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
dolní	dolní	k2eAgInSc1d1	dolní
tok	tok	k1gInSc1	tok
také	také	k9	také
hojně	hojně	k6eAd1	hojně
využíván	využívat	k5eAaPmNgInS	využívat
vodáky	vodák	k1gMnPc7	vodák
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
sjíždí	sjíždět	k5eAaImIp3nS	sjíždět
na	na	k7c4	na
raftu	rafta	k1gFnSc4	rafta
<g/>
.	.	kIx.	.
</s>
<s>
Zájezd	zájezd	k1gInSc1	zájezd
na	na	k7c4	na
raftu	rafta	k1gFnSc4	rafta
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zakoupit	zakoupit	k5eAaPmF	zakoupit
v	v	k7c6	v
Omiši	Omiše	k1gFnSc6	Omiše
<g/>
.	.	kIx.	.
</s>
