<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
105	[number]	k4	105
km	km	kA	km
<g/>
,	,	kIx,	,
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
oblast	oblast	k1gFnSc4	oblast
3700	[number]	k4	3700
km2	km2	k4	km2
<g/>
,	,	kIx,	,
a	a	k8xC	a
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
385	[number]	k4	385
m	m	kA	m
(	(	kIx(	(
<g/>
od	od	k7c2	od
pramene	pramen	k1gInSc2	pramen
až	až	k6eAd1	až
k	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
