<s>
Watthodina	watthodina	k1gFnSc1
</s>
<s>
Watthodina	watthodina	k1gFnSc1
(	(	kIx(
<g/>
značka	značka	k1gFnSc1
Wh	Wh	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
používá	používat	k5eAaImIp3nS
její	její	k3xOp3gInSc4
násobek	násobek	k1gInSc4
kilowatthodina	kilowatthodina	k1gFnSc1
<g/>
,	,	kIx,
kWh	kwh	kA
(	(	kIx(
<g/>
1000	#num#	k4
watthodin	watthodina	k1gFnPc2
<g/>
)	)	kIx)
pro	pro	k7c4
měření	měření	k1gNnSc4
spotřeby	spotřeba	k1gFnSc2
elektřiny	elektřina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Watthodina	watthodina	k1gFnSc1
nepatří	patřit	k5eNaImIp3nS
do	do	k7c2
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
od	od	k7c2
jednotky	jednotka	k1gFnSc2
watt	watt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotkou	jednotka	k1gFnSc7
energie	energie	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	se	k3xPyFc3
stala	stát	k5eAaPmAgFnS
jednotka	jednotka	k1gFnSc1
joule	joule	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
jedné	jeden	k4xCgFnSc3
wattsekundě	wattsekunda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Jedna	jeden	k4xCgFnSc1
watthodina	watthodina	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
práci	práce	k1gFnSc4
stroje	stroj	k1gInSc2
s	s	k7c7
příkonem	příkon	k1gInSc7
jeden	jeden	k4xCgInSc1
watt	watt	k1gInSc1
po	po	k7c4
dobu	doba	k1gFnSc4
jedné	jeden	k4xCgFnSc2
hodiny	hodina	k1gFnSc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
3600	#num#	k4
joulům	joule	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kilowatthodina	kilowatthodina	k1gFnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k8xC
rovna	roven	k2eAgFnSc1d1
3	#num#	k4
600	#num#	k4
000	#num#	k4
joulům	joule	k1gInPc3
<g/>
,	,	kIx,
neboli	neboli	k8xC
3,6	3,6	k4
megajoulům	megajoule	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
1	#num#	k4
spotřebovanou	spotřebovaný	k2eAgFnSc4d1
kilowatthodinu	kilowatthodina	k1gFnSc4
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
zaplatí	zaplatit	k5eAaPmIp3nS
česká	český	k2eAgFnSc1d1
domácnost	domácnost	k1gFnSc1
v	v	k7c6
průměru	průměr	k1gInSc6
4	#num#	k4
až	až	k9
5	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
elektřinou	elektřina	k1gFnSc7
netopí	topit	k5eNaImIp3nS
<g/>
,	,	kIx,
ani	ani	k8xC
neohřívá	ohřívat	k5eNaImIp3nS
vodu	voda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Násobky	násobek	k1gInPc1
</s>
<s>
V	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
velmi	velmi	k6eAd1
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
násobky	násobek	k1gInPc1
a	a	k8xC
díly	díl	k1gInPc1
této	tento	k3xDgFnSc2
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
<g/>
:	:	kIx,
</s>
<s>
kWh	kwh	kA
–	–	k?
kilowatthodina	kilowatthodina	k1gFnSc1
(	(	kIx(
<g/>
103	#num#	k4
Wh	Wh	kA
<g/>
)	)	kIx)
</s>
<s>
MWh	MWh	k?
–	–	k?
megawatthodina	megawatthodina	k1gFnSc1
(	(	kIx(
<g/>
106	#num#	k4
Wh	Wh	kA
<g/>
)	)	kIx)
</s>
<s>
GWh	GWh	k?
–	–	k?
gigawatthodina	gigawatthodina	k1gFnSc1
(	(	kIx(
<g/>
109	#num#	k4
Wh	Wh	kA
<g/>
)	)	kIx)
</s>
<s>
TWh	TWh	k?
–	–	k?
terawatthodina	terawatthodina	k1gFnSc1
(	(	kIx(
<g/>
1012	#num#	k4
Wh	Wh	kA
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Průměrná	průměrný	k2eAgFnSc1d1
cena	cena	k1gFnSc1
elektřiny	elektřina	k1gFnSc2
za	za	k7c4
kWh	kwh	kA
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
zdražila	zdražit	k5eAaPmAgFnS
na	na	k7c4
4,6	4,6	k4
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
ji	on	k3xPp3gFnSc4
prodává	prodávat	k5eAaImIp3nS
levněji	levně	k6eAd2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrina	Elektrina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-18	2019-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
watthodina	watthodina	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
</s>
