<p>
<s>
Praga	Praga	k1gFnSc1	Praga
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
výrobce	výrobce	k1gMnSc1	výrobce
automobilů	automobil	k1gInPc2	automobil
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Automobilka	automobilka	k1gFnSc1	automobilka
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
osobní	osobní	k2eAgFnSc1d1	osobní
a	a	k8xC	a
nákladní	nákladní	k2eAgNnPc1d1	nákladní
auta	auto	k1gNnPc1	auto
<g/>
,	,	kIx,	,
motocykly	motocykl	k1gInPc1	motocykl
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
technické	technický	k2eAgInPc1d1	technický
díly	díl	k1gInPc1	díl
do	do	k7c2	do
aut	auto	k1gNnPc2	auto
<g/>
,	,	kIx,	,
zavedla	zavést	k5eAaPmAgFnS	zavést
desítky	desítka	k1gFnPc1	desítka
patentů	patent	k1gInPc2	patent
do	do	k7c2	do
světa	svět	k1gInSc2	svět
automobilismu	automobilismus	k1gInSc2	automobilismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nynější	nynější	k2eAgFnSc1d1	nynější
firma	firma	k1gFnSc1	firma
Praga-Export	Praga-Export	k1gInSc1	Praga-Export
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
značku	značka	k1gFnSc4	značka
Praga	Prag	k1gMnSc2	Prag
<g/>
,	,	kIx,	,
obnovila	obnovit	k5eAaPmAgFnS	obnovit
výrobu	výroba	k1gFnSc4	výroba
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
motokár	motokára	k1gFnPc2	motokára
<g/>
,	,	kIx,	,
silničních	silniční	k2eAgInPc2d1	silniční
supersportů	supersport	k1gInPc2	supersport
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
automobilovým	automobilový	k2eAgNnSc7d1	automobilové
závoděním	závodění	k1gNnSc7	závodění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1907	[number]	k4	1907
-	-	kIx~	-
První	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Pražská	pražský	k2eAgFnSc1d1	Pražská
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
automobily	automobil	k1gInPc4	automobil
jako	jako	k8xS	jako
společný	společný	k2eAgInSc4d1	společný
projekt	projekt	k1gInSc4	projekt
První	první	k4xOgFnSc2	první
česko-moravské	českooravský	k2eAgFnSc2d1	česko-moravská
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
stroje	stroj	k1gInPc4	stroj
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
sloučené	sloučený	k2eAgInPc1d1	sloučený
jako	jako	k8xS	jako
mateřská	mateřský	k2eAgFnSc1d1	mateřská
společnost	společnost	k1gFnSc1	společnost
Pragovky	Pragovka	k1gFnSc2	Pragovka
v	v	k7c6	v
koncernu	koncern	k1gInSc6	koncern
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
Kolben	Kolbna	k1gFnPc2	Kolbna
Daněk	Daněk	k1gMnSc1	Daněk
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ringhofferových	Ringhofferův	k2eAgInPc2d1	Ringhofferův
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
několik	několik	k4yIc4	několik
vozů	vůz	k1gInPc2	vůz
v	v	k7c4	v
licenci	licence	k1gFnSc4	licence
italské	italský	k2eAgFnSc2d1	italská
firmy	firma	k1gFnSc2	firma
Isotta	Isotta	k1gMnSc1	Isotta
Fraschini	Fraschin	k1gMnPc1	Fraschin
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
licenci	licence	k1gFnSc6	licence
francouzských	francouzský	k2eAgFnPc2d1	francouzská
značek	značka	k1gFnPc2	značka
Charon	Charon	k1gMnSc1	Charon
a	a	k8xC	a
Renault	renault	k1gInSc1	renault
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
značka	značka	k1gFnSc1	značka
Praga	Praga	k1gFnSc1	Praga
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
výrazu	výraz	k1gInSc2	výraz
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
automobilech	automobil	k1gInPc6	automobil
však	však	k9	však
byla	být	k5eAaImAgNnP	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgNnPc1d1	malé
<g/>
,	,	kIx,	,
vozy	vůz	k1gInPc4	vůz
drahé	drahá	k1gFnSc2	drahá
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
neefektivní	efektivní	k2eNgFnSc1d1	neefektivní
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
9	[number]	k4	9
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
však	však	k9	však
prodalo	prodat	k5eAaPmAgNnS	prodat
pouhých	pouhý	k2eAgInPc2d1	pouhý
15	[number]	k4	15
kusů	kus	k1gInPc2	kus
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
s	s	k7c7	s
modely	model	k1gInPc1	model
Praga	Praga	k1gFnSc1	Praga
01	[number]	k4	01
a	a	k8xC	a
Praga	Praga	k1gFnSc1	Praga
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
automobilku	automobilka	k1gFnSc4	automobilka
nenastartovalo	nastartovat	k5eNaPmAgNnS	nastartovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
rok	rok	k1gInSc4	rok
1911	[number]	k4	1911
znamenal	znamenat	k5eAaImAgInS	znamenat
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
českého	český	k2eAgMnSc2d1	český
konstruktéra	konstruktér	k1gMnSc2	konstruktér
Františka	František	k1gMnSc2	František
Kece	kec	k1gInSc5	kec
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
vrchního	vrchní	k2eAgMnSc2d1	vrchní
konstruktéra	konstruktér	k1gMnSc2	konstruktér
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vojenský	vojenský	k2eAgInSc4d1	vojenský
nákladní	nákladní	k2eAgInSc4d1	nákladní
vůz	vůz	k1gInSc4	vůz
typu	typ	k1gInSc2	typ
Praga	Praga	k1gFnSc1	Praga
V	v	k7c6	v
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
autovlak	autovlak	k1gInSc1	autovlak
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
kvalitou	kvalita	k1gFnSc7	kvalita
a	a	k8xC	a
spolehlivostí	spolehlivost	k1gFnSc7	spolehlivost
se	se	k3xPyFc4	se
osvědčil	osvědčit	k5eAaPmAgMnS	osvědčit
a	a	k8xC	a
Pragovka	Pragovka	k1gFnSc1	Pragovka
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
získala	získat	k5eAaPmAgFnS	získat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
zakázku	zakázka	k1gFnSc4	zakázka
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
císařství	císařství	k1gNnSc1	císařství
jeho	jeho	k3xOp3gFnSc4	jeho
prodej	prodej	k1gFnSc4	prodej
subvencovalo	subvencovat	k5eAaImAgNnS	subvencovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výroba	výroba	k1gFnSc1	výroba
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
vlastní	vlastní	k2eAgFnSc2d1	vlastní
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
František	František	k1gMnSc1	František
Kec	kec	k1gInSc4	kec
<g/>
.	.	kIx.	.
</s>
<s>
Vozy	vůz	k1gInPc1	vůz
byly	být	k5eAaImAgInP	být
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
<g/>
,	,	kIx,	,
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
<g/>
,	,	kIx,	,
konkurovaly	konkurovat	k5eAaImAgFnP	konkurovat
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
značkám	značka	k1gFnPc3	značka
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
modely	model	k1gInPc4	model
Praga	Praga	k1gFnSc1	Praga
Mignon	Mignon	k1gNnSc1	Mignon
1911	[number]	k4	1911
(	(	kIx(	(
<g/>
vůz	vůz	k1gInSc1	vůz
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
Grand	grand	k1gMnSc1	grand
1912	[number]	k4	1912
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
1912	[number]	k4	1912
v	v	k7c6	v
Alpské	alpský	k2eAgFnSc6d1	alpská
jízdě	jízda	k1gFnSc6	jízda
<g/>
,	,	kIx,	,
jezdil	jezdit	k5eAaImAgMnS	jezdit
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
poslední	poslední	k2eAgMnSc1d1	poslední
rakousko-uherský	rakouskoherský	k2eAgMnSc1d1	rakousko-uherský
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jezdil	jezdit	k5eAaImAgMnS	jezdit
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
Alfa	alfa	k1gFnSc1	alfa
1913	[number]	k4	1913
(	(	kIx(	(
<g/>
malý	malý	k2eAgInSc1d1	malý
lidový	lidový	k2eAgInSc1d1	lidový
vůz	vůz	k1gInSc1	vůz
za	za	k7c4	za
nízkou	nízký	k2eAgFnSc4d1	nízká
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
dostupný	dostupný	k2eAgInSc1d1	dostupný
širšímu	široký	k2eAgInSc3d2	širší
okruhu	okruh	k1gInSc3	okruh
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jako	jako	k9	jako
první	první	k4xOgNnSc4	první
české	český	k2eAgNnSc4d1	české
auto	auto	k1gNnSc4	auto
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
stanul	stanout	k5eAaPmAgMnS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
automobilky	automobilka	k1gFnSc2	automobilka
František	František	k1gMnSc1	František
Kec	kec	k1gInSc1	kec
jako	jako	k8xS	jako
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
továrna	továrna	k1gFnSc1	továrna
byla	být	k5eAaImAgFnS	být
inovátorskou	inovátorský	k2eAgFnSc7d1	inovátorská
firmou	firma	k1gFnSc7	firma
a	a	k8xC	a
zavedla	zavést	k5eAaPmAgFnS	zavést
řadu	řada	k1gFnSc4	řada
novinek	novinka	k1gFnPc2	novinka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
prodejních	prodejní	k2eAgInPc2d1	prodejní
úspěchů	úspěch	k1gInPc2	úspěch
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
Praga	Praga	k1gFnSc1	Praga
řadu	řad	k1gInSc2	řad
ocenění	ocenění	k1gNnSc2	ocenění
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
sportovních	sportovní	k2eAgFnPc6d1	sportovní
a	a	k8xC	a
konkurzních	konkurzní	k2eAgFnPc6d1	konkurzní
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
Kec	kec	k1gInSc1	kec
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
Praga	Prag	k1gMnSc4	Prag
Piccolo	Piccola	k1gFnSc5	Piccola
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
menší	malý	k2eAgInSc1d2	menší
rodinný	rodinný	k2eAgInSc1d1	rodinný
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc4	titul
nejhezčí	hezký	k2eAgInSc1d3	nejhezčí
vůz	vůz	k1gInSc1	vůz
výstavy	výstava	k1gFnSc2	výstava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Kec	kec	k1gInSc1	kec
Pragovku	Pragovka	k1gFnSc4	Pragovka
opouští	opouštět	k5eAaImIp3nS	opouštět
po	po	k7c6	po
neshodách	neshoda	k1gFnPc6	neshoda
ohledně	ohledně	k7c2	ohledně
strategie	strategie	k1gFnSc2	strategie
automobilky	automobilka	k1gFnSc2	automobilka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
vzpamatování	vzpamatování	k1gNnSc6	vzpamatování
z	z	k7c2	z
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
výroba	výroba	k1gFnSc1	výroba
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
modely	model	k1gInPc1	model
Praga	Praga	k1gFnSc1	Praga
Super	super	k1gInSc1	super
Piccolo	Piccola	k1gFnSc5	Piccola
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
Baby	baby	k1gNnSc1	baby
1934	[number]	k4	1934
a	a	k8xC	a
Praga	Praga	k1gFnSc1	Praga
Golden	Goldna	k1gFnPc2	Goldna
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
Lady	lady	k1gFnSc1	lady
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
po	po	k7c4	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
komunismus	komunismus	k1gInSc1	komunismus
===	===	k?	===
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
zničil	zničit	k5eAaPmAgInS	zničit
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
automobilku	automobilka	k1gFnSc4	automobilka
nálet	nálet	k1gInSc1	nálet
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
rekonstruovaných	rekonstruovaný	k2eAgFnPc6d1	rekonstruovaná
budovách	budova	k1gFnPc6	budova
automobilky	automobilka	k1gFnSc2	automobilka
zahájila	zahájit	k5eAaPmAgNnP	zahájit
výrobu	výroba	k1gFnSc4	výroba
ČKD	ČKD	kA	ČKD
Sokolovo	Sokolovo	k1gNnSc1	Sokolovo
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
ČKD	ČKD	kA	ČKD
Lokomotivka	lokomotivka	k1gFnSc1	lokomotivka
a	a	k8xC	a
ČKD	ČKD	kA	ČKD
Kompresory	kompresor	k1gInPc1	kompresor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
automobilka	automobilka	k1gFnSc1	automobilka
Praga	Praga	k1gFnSc1	Praga
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Vysočan	Vysočany	k1gInPc2	Vysočany
do	do	k7c2	do
bývalé	bývalý	k2eAgFnSc2d1	bývalá
továrny	továrna	k1gFnSc2	továrna
Junkers	Junkersa	k1gFnPc2	Junkersa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
byla	být	k5eAaImAgFnS	být
znárodněna	znárodněn	k2eAgFnSc1d1	znárodněna
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
zařazena	zařazen	k2eAgFnSc1d1	zařazena
jako	jako	k8xC	jako
oddělení	oddělení	k1gNnSc1	oddělení
výroby	výroba	k1gFnSc2	výroba
pod	pod	k7c7	pod
ČKD	ČKD	kA	ČKD
<g/>
,	,	kIx,	,
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
již	již	k9	již
jen	jen	k9	jen
nákladní	nákladní	k2eAgInPc4d1	nákladní
a	a	k8xC	a
vojenské	vojenský	k2eAgInPc4d1	vojenský
vozy	vůz	k1gInPc4	vůz
(	(	kIx(	(
<g/>
známou	známý	k2eAgFnSc7d1	známá
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
používanou	používaný	k2eAgFnSc4d1	používaná
Praga	Praga	k1gFnSc1	Praga
V	V	kA	V
<g/>
3	[number]	k4	3
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vejtřasku	vejtřasek	k1gInSc2	vejtřasek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
přejmenována	přejmenovat	k5eAaPmNgNnP	přejmenovat
na	na	k7c4	na
Automobilové	automobilový	k2eAgInPc4d1	automobilový
závody	závod	k1gInPc4	závod
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
jen	jen	k9	jen
převodovky	převodovka	k1gFnPc4	převodovka
pro	pro	k7c4	pro
nákladní	nákladní	k2eAgNnPc4d1	nákladní
auta	auto	k1gNnPc4	auto
a	a	k8xC	a
autobusy	autobus	k1gInPc4	autobus
(	(	kIx(	(
<g/>
sedmistupňové	sedmistupňový	k2eAgFnPc4d1	sedmistupňová
převodovky	převodovka	k1gFnPc4	převodovka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
kdysi	kdysi	k6eAd1	kdysi
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
František	František	k1gMnSc1	František
Kec	kec	k1gInSc1	kec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgInS	být
podnik	podnik	k1gInSc1	podnik
zprivatizován	zprivatizován	k2eAgInSc1d1	zprivatizován
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
začal	začít	k5eAaPmAgInS	začít
znovu	znovu	k6eAd1	znovu
vyrábět	vyrábět	k5eAaImF	vyrábět
auta	auto	k1gNnPc4	auto
a	a	k8xC	a
motocykly	motocykl	k1gInPc4	motocykl
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
se	se	k3xPyFc4	se
ale	ale	k9	ale
nezdařil	zdařit	k5eNaPmAgMnS	zdařit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
koupila	koupit	k5eAaPmAgFnS	koupit
značku	značka	k1gFnSc4	značka
britská	britský	k2eAgFnSc1d1	britská
firma	firma	k1gFnSc1	firma
International	International	k1gFnSc1	International
Truck	truck	k1gInSc4	truck
Alliance	Alliance	k1gFnSc2	Alliance
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vyrábět	vyrábět	k5eAaImF	vyrábět
nákladní	nákladní	k2eAgInPc4d1	nákladní
vozy	vůz	k1gInPc4	vůz
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Praga	Prag	k1gMnSc2	Prag
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
Lublině	Lublin	k1gInSc6	Lublin
<g/>
.	.	kIx.	.
</s>
<s>
Nynější	nynější	k2eAgMnSc1d1	nynější
majitel	majitel	k1gMnSc1	majitel
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
Export	export	k1gInSc1	export
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Ptáčkem	Ptáček	k1gMnSc7	Ptáček
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
obnovit	obnovit	k5eAaPmF	obnovit
výrobu	výroba	k1gFnSc4	výroba
aut	auto	k1gNnPc2	auto
značky	značka	k1gFnSc2	značka
Praga	Praga	k1gFnSc1	Praga
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
setkávat	setkávat	k5eAaImF	setkávat
v	v	k7c6	v
silničním	silniční	k2eAgInSc6d1	silniční
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c6	na
závodech	závod	k1gInPc6	závod
Dutch	Dutch	k1gInSc4	Dutch
Supercar	Supercara	k1gFnPc2	Supercara
Challenge	Challeng	k1gInSc2	Challeng
GT	GT	kA	GT
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
firma	firma	k1gFnSc1	firma
představila	představit	k5eAaPmAgFnS	představit
nový	nový	k2eAgInSc4d1	nový
vůz	vůz	k1gInSc4	vůz
Praga	Prag	k1gMnSc2	Prag
R4	R4	k1gMnPc2	R4
s	s	k7c7	s
osmiválcovým	osmiválcový	k2eAgInSc7d1	osmiválcový
motorem	motor	k1gInSc7	motor
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
520	[number]	k4	520
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
homologovala	homologovat	k5eAaImAgFnS	homologovat
supersportovní	supersportovní	k2eAgInSc4d1	supersportovní
silniční	silniční	k2eAgInSc4d1	silniční
vůz	vůz	k1gInSc4	vůz
Praga	Praga	k1gFnSc1	Praga
R	R	kA	R
<g/>
1	[number]	k4	1
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
silniční	silniční	k2eAgNnSc1d1	silniční
auto	auto	k1gNnSc1	auto
Pragy	Praga	k1gFnSc2	Praga
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budova	budova	k1gFnSc1	budova
Praga	Praga	k1gFnSc1	Praga
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnPc1d1	původní
budovy	budova	k1gFnPc1	budova
automobilky	automobilka	k1gFnSc2	automobilka
Praga	Praga	k1gFnSc1	Praga
s	s	k7c7	s
železobetonovou	železobetonový	k2eAgFnSc7d1	železobetonová
konstrukcí	konstrukce	k1gFnSc7	konstrukce
postavil	postavit	k5eAaPmAgMnS	postavit
architekt	architekt	k1gMnSc1	architekt
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bechyně	Bechyně	k1gMnSc1	Bechyně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1917	[number]	k4	1917
-	-	kIx~	-
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
věže	věž	k1gFnPc1	věž
s	s	k7c7	s
výtahovými	výtahový	k2eAgFnPc7d1	výtahová
šachtami	šachta	k1gFnPc7	šachta
v	v	k7c6	v
rozích	roh	k1gInPc6	roh
budovy	budova	k1gFnSc2	budova
byly	být	k5eAaImAgFnP	být
působivou	působivý	k2eAgFnSc7d1	působivá
dominantou	dominanta	k1gFnSc7	dominanta
pražské	pražský	k2eAgFnSc2d1	Pražská
Libně	Libeň	k1gFnSc2	Libeň
a	a	k8xC	a
Vysočan	Vysočany	k1gInPc2	Vysočany
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
výroby	výroba	k1gFnSc2	výroba
pod	pod	k7c7	pod
ČKD	ČKD	kA	ČKD
chátraly	chátrat	k5eAaImAgFnP	chátrat
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
podlehly	podlehnout	k5eAaPmAgFnP	podlehnout
demolici	demolice	k1gFnSc4	demolice
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
bývalé	bývalý	k2eAgFnSc2d1	bývalá
největší	veliký	k2eAgFnSc2d3	veliký
československé	československý	k2eAgFnSc2d1	Československá
automobilky	automobilka	k1gFnSc2	automobilka
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
O2	O2	k1gFnSc1	O2
arena	areno	k1gNnSc2	areno
<g/>
,	,	kIx,	,
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
administrativní	administrativní	k2eAgFnSc2d1	administrativní
a	a	k8xC	a
komerční	komerční	k2eAgFnSc2d1	komerční
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Automuzeum	Automuzeum	k1gNnSc1	Automuzeum
Praga	Praga	k1gFnSc1	Praga
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
soukromé	soukromý	k2eAgNnSc1d1	soukromé
Automuzeum	Automuzeum	k1gNnSc1	Automuzeum
Praga	Praga	k1gFnSc1	Praga
sběratelem	sběratel	k1gMnSc7	sběratel
Emilem	Emil	k1gMnSc7	Emil
Příhodou	Příhoda	k1gMnSc7	Příhoda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
Zbuzanech	Zbuzan	k1gInPc6	Zbuzan
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
historii	historie	k1gFnSc4	historie
této	tento	k3xDgFnSc2	tento
české	český	k2eAgFnSc2d1	Česká
automobilky	automobilka	k1gFnSc2	automobilka
<g/>
,	,	kIx,	,
čítá	čítat	k5eAaImIp3nS	čítat
přes	přes	k7c4	přes
70	[number]	k4	70
kusů	kus	k1gInPc2	kus
automobilů	automobil	k1gInPc2	automobil
či	či	k8xC	či
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
značky	značka	k1gFnSc2	značka
Praga	Praga	k1gFnSc1	Praga
<g/>
,	,	kIx,	,
technickou	technický	k2eAgFnSc4d1	technická
dokumentaci	dokumentace	k1gFnSc4	dokumentace
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
materiály	materiál	k1gInPc4	materiál
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
související	související	k2eAgFnSc7d1	související
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Produkce	produkce	k1gFnSc1	produkce
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Osobní	osobní	k2eAgInPc4d1	osobní
automobily	automobil	k1gInPc4	automobil
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
Charon	Charon	k1gMnSc1	Charon
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
Praga	Praga	k1gFnSc1	Praga
Mignon	Mignon	k1gMnSc1	Mignon
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
-	-	kIx~	-
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
Praga	Praga	k1gFnSc1	Praga
Grand	grand	k1gMnSc1	grand
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
-	-	kIx~	-
luxusní	luxusní	k2eAgFnSc1d1	luxusní
limuzína	limuzína	k1gFnSc1	limuzína
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
Alfa	alfa	k1gFnSc1	alfa
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
Baby	baba	k1gFnSc2	baba
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
-	-	kIx~	-
malý	malý	k2eAgInSc4d1	malý
levný	levný	k2eAgInSc4d1	levný
lidový	lidový	k2eAgInSc4d1	lidový
vůz	vůz	k1gInSc4	vůz
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
Piccolo	Piccola	k1gFnSc5	Piccola
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
-	-	kIx~	-
menší	malý	k2eAgInSc1d2	menší
rodinný	rodinný	k2eAgInSc1d1	rodinný
vůz	vůz	k1gInSc1	vůz
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
Super	super	k1gInSc2	super
Piccolo	Piccola	k1gFnSc5	Piccola
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
Golden	Goldna	k1gFnPc2	Goldna
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
Praga	Praga	k1gFnSc1	Praga
Lady	lady	k1gFnSc1	lady
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
-	-	kIx~	-
rodinný	rodinný	k2eAgInSc4d1	rodinný
vůz	vůz	k1gInSc4	vůz
pro	pro	k7c4	pro
vyšší	vysoký	k2eAgNnSc4d2	vyšší
prostorové	prostorový	k2eAgNnSc4d1	prostorové
nárokyPraga	nárokyPraga	k1gFnSc1	nárokyPraga
R4S	R4S	k1gMnSc1	R4S
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
-	-	kIx~	-
okruhový	okruhový	k2eAgInSc1d1	okruhový
speciál	speciál	k1gInSc1	speciál
(	(	kIx(	(
<g/>
firma	firma	k1gFnSc1	firma
plánuje	plánovat	k5eAaImIp3nS	plánovat
homologaci	homologace	k1gFnSc4	homologace
pro	pro	k7c4	pro
běžný	běžný	k2eAgInSc4d1	běžný
provoz	provoz	k1gInSc4	provoz
<g/>
)	)	kIx)	)
<g/>
Praga	Prag	k1gMnSc2	Prag
R1	R1	k1gMnSc2	R1
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
-	-	kIx~	-
okruhový	okruhový	k2eAgInSc4d1	okruhový
speciálVýroba	speciálVýroba	k1gFnSc1	speciálVýroba
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Motocykly	motocykl	k1gInPc4	motocykl
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
BD	BD	kA	BD
500	[number]	k4	500
DOHC	DOHC	kA	DOHC
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
–	–	k?	–
technické	technický	k2eAgFnSc2d1	technická
specifikace	specifikace	k1gFnSc2	specifikace
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
BC	BC	kA	BC
350	[number]	k4	350
OHC	OHC	kA	OHC
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
–	–	k?	–
technické	technický	k2eAgFnSc2d1	technická
specifikace	specifikace	k1gFnSc2	specifikace
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
ED	ED	kA	ED
250	[number]	k4	250
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
enduro	endura	k1gFnSc5	endura
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
ED	ED	kA	ED
610	[number]	k4	610
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
–	–	k?	–
enduro	endura	k1gFnSc5	endura
</s>
</p>
<p>
<s>
===	===	k?	===
Nákladní	nákladní	k2eAgInPc1d1	nákladní
automobily	automobil	k1gInPc1	automobil
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
V	V	kA	V
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
-	-	kIx~	-
nosnost	nosnost	k1gFnSc1	nosnost
5	[number]	k4	5
t	t	k?	t
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
N	N	kA	N
(	(	kIx(	(
<g/>
od	od	k7c2	od
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
A150	A150	k1gFnSc2	A150
-	-	kIx~	-
malý	malý	k2eAgInSc4d1	malý
nákladní	nákladní	k2eAgInSc4d1	nákladní
automobil	automobil	k1gInSc4	automobil
<g/>
,	,	kIx,	,
též	též	k9	též
jako	jako	k9	jako
malý	malý	k2eAgInSc1d1	malý
autobus	autobus	k1gInSc1	autobus
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
RN	RN	kA	RN
-	-	kIx~	-
střední	střední	k2eAgInSc4d1	střední
nákladní	nákladní	k2eAgInSc4d1	nákladní
automobil	automobil	k1gInSc4	automobil
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
benzínovým	benzínový	k2eAgInSc7d1	benzínový
(	(	kIx(	(
<g/>
zážehovým	zážehový	k2eAgInSc7d1	zážehový
<g/>
)	)	kIx)	)
motorem	motor	k1gInSc7	motor
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
RND	RND	kA	RND
-	-	kIx~	-
totéž	týž	k3xTgNnSc4	týž
jako	jako	k9	jako
RN	RN	kA	RN
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
naftovým	naftový	k2eAgInSc7d1	naftový
(	(	kIx(	(
<g/>
vznětovým	vznětový	k2eAgInSc7d1	vznětový
<g/>
)	)	kIx)	)
motorem	motor	k1gInSc7	motor
(	(	kIx(	(
<g/>
diesel	diesel	k1gInSc1	diesel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
jako	jako	k9	jako
menší	malý	k2eAgInSc1d2	menší
autobus	autobus	k1gInSc1	autobus
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
RV	RV	kA	RV
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
lehký	lehký	k2eAgInSc1d1	lehký
nákladní	nákladní	k2eAgInSc1d1	nákladní
automobil	automobil	k1gInSc1	automobil
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
armádu	armáda	k1gFnSc4	armáda
(	(	kIx(	(
<g/>
RV	RV	kA	RV
=	=	kIx~	=
rychlý	rychlý	k2eAgInSc1d1	rychlý
vojenský	vojenský	k2eAgInSc1d1	vojenský
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
ND	ND	kA	ND
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
V3S	V3S	k1gFnSc1	V3S
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
-	-	kIx~	-
střední	střední	k2eAgInSc1d1	střední
terénní	terénní	k2eAgInSc1d1	terénní
automobil	automobil	k1gInSc1	automobil
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
×	×	k?	×
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
S5T	S5T	k1gFnSc1	S5T
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
-	-	kIx~	-
silniční	silniční	k2eAgFnSc1d1	silniční
pětituna	pětituna	k1gFnSc1	pětituna
odvozená	odvozený	k2eAgFnSc1d1	odvozená
od	od	k7c2	od
V3S	V3S	k1gFnSc2	V3S
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
UV100	UV100	k1gFnSc1	UV100
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
univerzální	univerzální	k2eAgNnSc1d1	univerzální
vozidlo	vozidlo	k1gNnSc1	vozidlo
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
UV120	UV120	k1gFnSc1	UV120
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
-	-	kIx~	-
univerzální	univerzální	k2eAgNnSc1d1	univerzální
vozidlo	vozidlo	k1gNnSc1	vozidlo
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
UV80	UV80	k1gFnSc2	UV80
(	(	kIx(	(
<g/>
od	od	k7c2	od
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
-	-	kIx~	-
univerzální	univerzální	k2eAgNnSc1d1	univerzální
vozidlo	vozidlo	k1gNnSc1	vozidlo
</s>
</p>
<p>
<s>
===	===	k?	===
Autobusy	autobus	k1gInPc1	autobus
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
NDO	NDO	kA	NDO
-	-	kIx~	-
autobus	autobus	k1gInSc1	autobus
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
nákladního	nákladní	k2eAgInSc2d1	nákladní
vozu	vůz	k1gInSc2	vůz
ND	ND	kA	ND
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
RN	RN	kA	RN
a	a	k8xC	a
RND	RND	kA	RND
-	-	kIx~	-
menší	malý	k2eAgInPc1d2	menší
autobusy	autobus	k1gInPc1	autobus
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
nákladních	nákladní	k2eAgInPc2d1	nákladní
vozů	vůz	k1gInPc2	vůz
RN	RN	kA	RN
a	a	k8xC	a
RND	RND	kA	RND
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
A150	A150	k1gFnSc1	A150
-	-	kIx~	-
malý	malý	k2eAgInSc1d1	malý
autobus	autobus	k1gInSc1	autobus
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
nákladního	nákladní	k2eAgInSc2d1	nákladní
vozu	vůz	k1gInSc2	vůz
A150	A150	k1gFnSc2	A150
</s>
</p>
<p>
<s>
===	===	k?	===
Trolejbusy	trolejbus	k1gInPc4	trolejbus
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
TOT	toto	k1gNnPc2	toto
</s>
</p>
<p>
<s>
nerealizované	realizovaný	k2eNgInPc1d1	nerealizovaný
projekty	projekt	k1gInPc1	projekt
<g/>
:	:	kIx,	:
Praga	Praga	k1gFnSc1	Praga
TNT	TNT	kA	TNT
a	a	k8xC	a
Praga	Praga	k1gFnSc1	Praga
TB	TB	kA	TB
2	[number]	k4	2
</s>
</p>
<p>
<s>
===	===	k?	===
Tanky	tank	k1gInPc4	tank
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
LT	LT	kA	LT
vz.	vz.	k?	vz.
38	[number]	k4	38
(	(	kIx(	(
<g/>
od	od	k7c2	od
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
lehký	lehký	k2eAgInSc4d1	lehký
tank	tank	k1gInSc4	tank
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
německého	německý	k2eAgInSc2d1	německý
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
jako	jako	k8xC	jako
Pz	Pz	k1gFnSc2	Pz
<g/>
.	.	kIx.	.
<g/>
Kpfw	Kpfw	k1gFnSc1	Kpfw
<g/>
.38	.38	k4	.38
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Panzerwagen	Panzerwagen	k1gInSc1	Panzerwagen
39	[number]	k4	39
</s>
</p>
<p>
<s>
===	===	k?	===
Letadla	letadlo	k1gNnPc1	letadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
BH-39	BH-39	k1gFnSc1	BH-39
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
39	[number]	k4	39
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
BH-44	BH-44	k1gFnSc2	BH-44
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
E-41	E-41	k1gFnSc1	E-41
(	(	kIx(	(
<g/>
BH-	BH-	k1gFnSc1	BH-
<g/>
41	[number]	k4	41
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
E-114	E-114	k1gFnSc1	E-114
(	(	kIx(	(
<g/>
E-	E-	k1gFnSc1	E-
<g/>
115	[number]	k4	115
<g/>
,	,	kIx,	,
E-	E-	k1gFnSc1	E-
<g/>
117	[number]	k4	117
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
E-214	E-214	k1gFnSc1	E-214
(	(	kIx(	(
<g/>
E-	E-	k1gFnSc1	E-
<g/>
215	[number]	k4	215
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
E-241	E-241	k1gFnSc2	E-241
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
E-210	E-210	k1gFnSc1	E-210
(	(	kIx(	(
<g/>
E-	E-	k1gFnSc1	E-
<g/>
211	[number]	k4	211
<g/>
,	,	kIx,	,
E-	E-	k1gFnSc1	E-
<g/>
212	[number]	k4	212
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
E-40	E-40	k1gFnSc2	E-40
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
E-51	E-51	k1gFnSc2	E-51
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
XE-II	XE-II	k1gFnSc2	XE-II
</s>
</p>
<p>
<s>
===	===	k?	===
Dělostřelecké	dělostřelecký	k2eAgInPc1d1	dělostřelecký
tahače	tahač	k1gInPc1	tahač
===	===	k?	===
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
T-3	T-3	k1gFnSc2	T-3
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
T-4	T-4	k1gFnSc2	T-4
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
T-6	T-6	k1gFnSc2	T-6
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
T-7	T-7	k1gFnSc2	T-7
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
T-8	T-8	k1gFnSc2	T-8
</s>
</p>
<p>
<s>
Praga	Praga	k1gFnSc1	Praga
T-9	T-9	k1gFnSc2	T-9
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgInPc2d1	český
automobilů	automobil	k1gInPc2	automobil
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Praga	Prag	k1gMnSc2	Prag
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.praga.cz	[url]	k1gMnSc1	http://www.praga.cz
</s>
</p>
<p>
<s>
https://web.archive.org/web/20090216174943/http://www.praga.cz/vismo/dokumenty2.asp?id_org=600000&	[url]	k?	https://web.archive.org/web/20090216174943/http://www.praga.cz/vismo/dokumenty2.asp?id_org=600000&
</s>
</p>
<p>
<s>
http://exporter.ihned.cz/1-10083640-15843740-r00000_d-61	[url]	k4	http://exporter.ihned.cz/1-10083640-15843740-r00000_d-61
</s>
</p>
<p>
<s>
http://www.auto-praga-grmela.cz/praga.php	[url]	k1gMnSc1	http://www.auto-praga-grmela.cz/praga.php
</s>
</p>
<p>
<s>
http://www.mestskadoprava.net/galeria/picture.php?/13369/category/1356&	[url]	k?	http://www.mestskadoprava.net/galeria/picture.php?/13369/category/1356&
</s>
</p>
<p>
<s>
https://web.archive.org/web/20080601035928/http://www.veteran.cz/pragamuseum/Default.htm	[url]	k6eAd1	https://web.archive.org/web/20080601035928/http://www.veteran.cz/pragamuseum/Default.htm
</s>
</p>
<p>
<s>
https://web.archive.org/web/20080527025452/http://www.ciz.cz/praga.php	[url]	k1gMnSc1	https://web.archive.org/web/20080527025452/http://www.ciz.cz/praga.php
</s>
</p>
<p>
<s>
Pásové	pásový	k2eAgInPc1d1	pásový
dělostřelecké	dělostřelecký	k2eAgInPc1d1	dělostřelecký
tahače	tahač	k1gInPc1	tahač
Praga	Prag	k1gMnSc2	Prag
</s>
</p>
