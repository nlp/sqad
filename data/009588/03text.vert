<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
–	–	k?	–
(	(	kIx(	(
<g/>
současníky	současník	k1gMnPc4	současník
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Belle	bell	k1gInSc5	bell
Alliance	Allianec	k1gMnSc4	Allianec
či	či	k8xC	či
Napoleonem	napoleon	k1gInSc7	napoleon
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Mont-Saint-Jean	Mont-Saint-Jeany	k1gInPc2	Mont-Saint-Jeany
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
a	a	k8xC	a
podvečerních	podvečerní	k2eAgFnPc6d1	podvečerní
hodinách	hodina	k1gFnPc6	hodina
neděle	neděle	k1gFnSc2	neděle
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1815	[number]	k4	1815
asi	asi	k9	asi
20	[number]	k4	20
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
belgické	belgický	k2eAgFnSc2d1	belgická
metropole	metropol	k1gFnSc2	metropol
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
armáda	armáda	k1gFnSc1	armáda
císaře	císař	k1gMnSc2	císař
Napoleona	Napoleon	k1gMnSc2	Napoleon
I.	I.	kA	I.
zde	zde	k6eAd1	zde
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
od	od	k7c2	od
části	část	k1gFnSc2	část
vojsk	vojsko	k1gNnPc2	vojsko
VII	VII	kA	VII
<g/>
.	.	kIx.	.
koalice	koalice	k1gFnSc1	koalice
evropských	evropský	k2eAgMnPc2d1	evropský
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
reprezentované	reprezentovaný	k2eAgInPc1d1	reprezentovaný
sbory	sbor	k1gInPc1	sbor
anglo-batavské	angloatavský	k2eAgFnSc2d1	anglo-batavský
armády	armáda	k1gFnSc2	armáda
polního	polní	k2eAgMnSc2d1	polní
maršála	maršál	k1gMnSc2	maršál
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Wellingtonu	Wellington	k1gInSc2	Wellington
a	a	k8xC	a
pruským	pruský	k2eAgNnSc7d1	pruské
vojskem	vojsko	k1gNnSc7	vojsko
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
polního	polní	k2eAgMnSc2d1	polní
maršála	maršál	k1gMnSc2	maršál
Blüchera	Blücher	k1gMnSc2	Blücher
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
francouzského	francouzský	k2eAgMnSc4d1	francouzský
panovníka	panovník	k1gMnSc4	panovník
se	se	k3xPyFc4	se
bitva	bitva	k1gFnSc1	bitva
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
posledních	poslední	k2eAgNnPc2d1	poslední
vystoupení	vystoupení	k1gNnPc2	vystoupení
v	v	k7c6	v
roli	role	k1gFnSc6	role
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
<g/>
,	,	kIx,	,
stratéga	stratég	k1gMnSc2	stratég
a	a	k8xC	a
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
znamenala	znamenat	k5eAaImAgFnS	znamenat
ukončení	ukončení	k1gNnSc4	ukončení
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
spojencům	spojenec	k1gMnPc3	spojenec
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
generálního	generální	k2eAgNnSc2d1	generální
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
průběh	průběh	k1gInSc1	průběh
tažení	tažení	k1gNnSc2	tažení
hrál	hrát	k5eAaImAgInS	hrát
spíš	spíš	k9	spíš
v	v	k7c4	v
jejich	jejich	k3xOp3gInSc4	jejich
neprospěch	neprospěch	k1gInSc4	neprospěch
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
porážky	porážka	k1gFnPc4	porážka
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
u	u	k7c2	u
Ligny	Ligna	k1gFnSc2	Ligna
a	a	k8xC	a
Quartre-Bras	Quartre-Brasa	k1gFnPc2	Quartre-Brasa
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
se	se	k3xPyFc4	se
odděleně	odděleně	k6eAd1	odděleně
stahovat	stahovat	k5eAaImF	stahovat
k	k	k7c3	k
belgické	belgický	k2eAgFnSc3d1	belgická
metropoli	metropol	k1gFnSc3	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
maršál	maršál	k1gMnSc1	maršál
však	však	k9	však
druhého	druhý	k4xOgMnSc4	druhý
dne	den	k1gInSc2	den
ústup	ústup	k1gInSc1	ústup
zastavil	zastavit	k5eAaPmAgInS	zastavit
a	a	k8xC	a
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
výhodné	výhodný	k2eAgNnSc4d1	výhodné
postavení	postavení	k1gNnSc4	postavení
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
kolem	kolem	k7c2	kolem
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
vyčkal	vyčkat	k5eAaPmAgMnS	vyčkat
na	na	k7c4	na
útok	útok	k1gInSc4	útok
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
promarněném	promarněný	k2eAgNnSc6d1	promarněné
dopoledni	dopoledne	k1gNnSc6	dopoledne
pak	pak	k6eAd1	pak
Napoleon	Napoleon	k1gMnSc1	Napoleon
podnikl	podniknout	k5eAaPmAgMnS	podniknout
opakované	opakovaný	k2eAgInPc4d1	opakovaný
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
střed	střed	k1gInSc4	střed
jeho	jeho	k3xOp3gFnPc2	jeho
pozic	pozice	k1gFnPc2	pozice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
francouzští	francouzský	k2eAgMnPc1d1	francouzský
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
za	za	k7c2	za
oboustranně	oboustranně	k6eAd1	oboustranně
vysokých	vysoký	k2eAgFnPc2d1	vysoká
ztrát	ztráta	k1gFnPc2	ztráta
pravidelně	pravidelně	k6eAd1	pravidelně
obraceni	obracet	k5eAaImNgMnP	obracet
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
k	k	k7c3	k
Britům	Brit	k1gMnPc3	Brit
začaly	začít	k5eAaPmAgFnP	začít
připojovat	připojovat	k5eAaImF	připojovat
první	první	k4xOgFnPc1	první
pruské	pruský	k2eAgFnPc1d1	pruská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
s	s	k7c7	s
přibývajícím	přibývající	k2eAgNnSc7d1	přibývající
množstvím	množství	k1gNnSc7	množství
<g/>
,	,	kIx,	,
po	po	k7c6	po
necelých	celý	k2eNgFnPc6d1	necelá
čtyřech	čtyři	k4xCgFnPc6	čtyři
hodinách	hodina	k1gFnPc6	hodina
tlaku	tlak	k1gInSc2	tlak
na	na	k7c4	na
protivníkovo	protivníkův	k2eAgNnSc4d1	protivníkovo
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
obrátily	obrátit	k5eAaPmAgFnP	obrátit
francouzské	francouzský	k2eAgNnSc4d1	francouzské
vojsko	vojsko	k1gNnSc4	vojsko
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Napoleon	Napoleon	k1gMnSc1	Napoleon
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
memoárech	memoáry	k1gInPc6	memoáry
přičítal	přičítat	k5eAaImAgMnS	přičítat
vinu	vina	k1gFnSc4	vina
za	za	k7c4	za
debakl	debakl	k1gInSc4	debakl
neschopnosti	neschopnost	k1gFnSc2	neschopnost
svých	svůj	k3xOyFgMnPc2	svůj
maršálů	maršál	k1gMnPc2	maršál
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
spíš	spíš	k9	spíš
souhrou	souhra	k1gFnSc7	souhra
desítek	desítka	k1gFnPc2	desítka
selhání	selhání	k1gNnSc2	selhání
jak	jak	k8xS	jak
císaře	císař	k1gMnSc2	císař
samotného	samotný	k2eAgMnSc2d1	samotný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jeho	jeho	k3xOp3gMnPc2	jeho
podřízených	podřízený	k1gMnPc2	podřízený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejnou	stejný	k2eAgFnSc7d1	stejná
měrou	míra	k1gFnSc7wR	míra
i	i	k8xC	i
okolností	okolnost	k1gFnSc7	okolnost
a	a	k8xC	a
schopnostmi	schopnost	k1gFnPc7	schopnost
jeho	jeho	k3xOp3gMnPc2	jeho
protivníků	protivník	k1gMnPc2	protivník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
definitivnosti	definitivnost	k1gFnSc3	definitivnost
pro	pro	k7c4	pro
Napoleona	Napoleon	k1gMnSc4	Napoleon
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
sto	sto	k4xCgNnSc4	sto
dní	den	k1gInPc2	den
trvající	trvající	k2eAgNnSc4d1	trvající
císařství	císařství	k1gNnSc4	císařství
se	se	k3xPyFc4	se
městečko	městečko	k1gNnSc1	městečko
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
bitva	bitva	k1gFnSc1	bitva
odehrála	odehrát	k5eAaPmAgFnS	odehrát
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
porážku	porážka	k1gFnSc4	porážka
nebo	nebo	k8xC	nebo
konečný	konečný	k2eAgInSc4d1	konečný
akt	akt	k1gInSc4	akt
zásadního	zásadní	k2eAgInSc2d1	zásadní
významu	význam	k1gInSc2	význam
s	s	k7c7	s
negativními	negativní	k2eAgInPc7d1	negativní
důsledky	důsledek	k1gInPc7	důsledek
či	či	k8xC	či
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
fatální	fatální	k2eAgNnSc1d1	fatální
selhání	selhání	k1gNnSc1	selhání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Z	z	k7c2	z
Elby	Elba	k1gFnSc2	Elba
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
do	do	k7c2	do
Belgie	Belgie	k1gFnSc2	Belgie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Let	let	k1gInSc1	let
orla	orel	k1gMnSc2	orel
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
armády	armáda	k1gFnSc2	armáda
Prvního	první	k4xOgMnSc2	první
císařství	císařství	k1gNnSc3	císařství
kapitulovaly	kapitulovat	k5eAaBmAgFnP	kapitulovat
před	před	k7c7	před
brannou	branný	k2eAgFnSc7d1	Branná
mocí	moc	k1gFnSc7	moc
VI	VI	kA	VI
<g/>
.	.	kIx.	.
koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1814	[number]	k4	1814
přinucen	přinutit	k5eAaPmNgMnS	přinutit
podepsat	podepsat	k5eAaPmF	podepsat
abdikaci	abdikace	k1gFnSc4	abdikace
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
francouzského	francouzský	k2eAgInSc2d1	francouzský
trůnu	trůn	k1gInSc2	trůn
jak	jak	k8xS	jak
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
za	za	k7c4	za
své	svůj	k3xOyFgMnPc4	svůj
oprávněné	oprávněný	k2eAgMnPc4d1	oprávněný
dědice	dědic	k1gMnPc4	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgInS	být
i	i	k9	i
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
sedmi	sedm	k4xCc2	sedm
stovek	stovka	k1gFnPc2	stovka
vojáků	voják	k1gMnPc2	voják
císařské	císařský	k2eAgFnSc2d1	císařská
gardy	garda	k1gFnSc2	garda
internován	internovat	k5eAaBmNgInS	internovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Elba	Elb	k1gInSc2	Elb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
uvolněný	uvolněný	k2eAgInSc4d1	uvolněný
panovnický	panovnický	k2eAgInSc4d1	panovnický
stolec	stolec	k1gInSc4	stolec
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
král	král	k1gMnSc1	král
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Bourbonů	bourbon	k1gInPc2	bourbon
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
návratu	návrat	k1gInSc6	návrat
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
přibývat	přibývat	k5eAaImF	přibývat
nespokojených	spokojený	k2eNgInPc2d1	nespokojený
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgMnS	líbit
návrat	návrat	k1gInSc4	návrat
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
institucí	instituce	k1gFnPc2	instituce
starého	starý	k2eAgInSc2d1	starý
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Předrevoluční	předrevoluční	k2eAgFnSc1d1	předrevoluční
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
vracela	vracet	k5eAaImAgFnS	vracet
z	z	k7c2	z
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
,	,	kIx,	,
zabrala	zabrat	k5eAaPmAgFnS	zabrat
výnosné	výnosný	k2eAgInPc4d1	výnosný
úřady	úřad	k1gInPc4	úřad
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
císařská	císařský	k2eAgFnSc1d1	císařská
šlechta	šlechta	k1gFnSc1	šlechta
byla	být	k5eAaImAgFnS	být
přehlížena	přehlížet	k5eAaImNgFnS	přehlížet
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
i	i	k8xC	i
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
bouřit	bouřit	k5eAaImF	bouřit
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
odmítal	odmítat	k5eAaImAgInS	odmítat
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
vyplácet	vyplácet	k5eAaImF	vyplácet
slíbenou	slíbený	k2eAgFnSc4d1	slíbená
dvou	dva	k4xCgMnPc6	dva
milionovou	milionový	k2eAgFnSc4d1	milionová
roční	roční	k2eAgFnSc4d1	roční
apanáž	apanáž	k1gFnSc4	apanáž
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
šeptat	šeptat	k5eAaImF	šeptat
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
přemístění	přemístění	k1gNnSc4	přemístění
<g/>
,	,	kIx,	,
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
,	,	kIx,	,
vzdálenější	vzdálený	k2eAgInSc4d2	vzdálenější
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgFnPc3	tento
i	i	k8xC	i
dalším	další	k2eAgFnPc3d1	další
okolnostem	okolnost	k1gFnPc3	okolnost
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1815	[number]	k4	1815
nalodil	nalodit	k5eAaPmAgMnS	nalodit
excísař	excísař	k1gMnSc1	excísař
vojáky	voják	k1gMnPc4	voják
gardy	gard	k1gInPc1	gard
na	na	k7c4	na
předem	předem	k6eAd1	předem
připravené	připravený	k2eAgFnPc4d1	připravená
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
rok	rok	k1gInSc4	rok
trvajícího	trvající	k2eAgNnSc2d1	trvající
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
malá	malý	k2eAgFnSc1d1	malá
flotila	flotila	k1gFnSc1	flotila
shromáždila	shromáždit	k5eAaPmAgFnS	shromáždit
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Juan	Juan	k1gMnSc1	Juan
u	u	k7c2	u
Antibes	Antibesa	k1gFnPc2	Antibesa
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
vylodili	vylodit	k5eAaPmAgMnP	vylodit
na	na	k7c4	na
pláž	pláž	k1gFnSc4	pláž
a	a	k8xC	a
kolem	kolem	k7c2	kolem
páté	pátý	k4xOgFnSc2	pátý
hodiny	hodina	k1gFnSc2	hodina
odpolední	odpolední	k1gNnSc2	odpolední
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Paříž	Paříž	k1gFnSc4	Paříž
skrze	skrze	k?	skrze
alpské	alpský	k2eAgFnSc2d1	alpská
stezky	stezka	k1gFnSc2	stezka
Horní	horní	k2eAgFnSc2d1	horní
Provence	Provence	k1gFnSc2	Provence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímořském	přímořský	k2eAgInSc6d1	přímořský
kraji	kraj	k1gInSc6	kraj
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
přítomnost	přítomnost	k1gFnSc1	přítomnost
mnoho	mnoho	k6eAd1	mnoho
radosti	radost	k1gFnSc2	radost
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
nevyvolávala	vyvolávat	k5eNaImAgFnS	vyvolávat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
čím	čí	k3xOyQgNnSc7	čí
více	hodně	k6eAd2	hodně
postupoval	postupovat	k5eAaImAgInS	postupovat
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgNnSc1d2	veliký
nadšení	nadšení	k1gNnSc1	nadšení
vzbuzoval	vzbuzovat	k5eAaImAgMnS	vzbuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ozbrojených	ozbrojený	k2eAgMnPc2d1	ozbrojený
civilistů	civilista	k1gMnPc2	civilista
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
brzy	brzy	k6eAd1	brzy
začaly	začít	k5eAaPmAgInP	začít
přidávat	přidávat	k5eAaImF	přidávat
první	první	k4xOgInPc4	první
pluky	pluk	k1gInPc4	pluk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
sebou	se	k3xPyFc7	se
strhly	strhnout	k5eAaPmAgFnP	strhnout
další	další	k2eAgFnPc4d1	další
jednotky	jednotka	k1gFnPc4	jednotka
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Grenoblu	Grenoble	k1gInSc2	Grenoble
a	a	k8xC	a
Lyonu	Lyon	k1gInSc2	Lyon
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
přeběhl	přeběhnout	k5eAaPmAgMnS	přeběhnout
i	i	k9	i
maršál	maršál	k1gMnSc1	maršál
Michel	Michel	k1gMnSc1	Michel
Ney	Ney	k1gMnSc1	Ney
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgInS	mít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
armádou	armáda	k1gFnSc7	armáda
shromážděnou	shromážděný	k2eAgFnSc7d1	shromážděná
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Lons-le-Saulnier	Lonse-Saulnier	k1gInSc1	Lons-le-Saulnier
přetnout	přetnout	k5eAaPmF	přetnout
svému	svůj	k3xOyFgMnSc3	svůj
někdejšímu	někdejší	k2eAgMnSc3d1	někdejší
panovníkovi	panovník	k1gMnSc3	panovník
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
francouzské	francouzský	k2eAgFnSc3d1	francouzská
metropoli	metropol	k1gFnSc3	metropol
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
pak	pak	k6eAd1	pak
Napoleon	napoleon	k1gInSc1	napoleon
za	za	k7c2	za
davového	davový	k2eAgNnSc2d1	davové
nadšení	nadšení	k1gNnSc2	nadšení
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Tuileries	Tuileries	k1gInSc1	Tuileries
<g/>
.	.	kIx.	.
</s>
<s>
Opětovné	opětovný	k2eAgFnPc1d1	opětovná
vlády	vláda	k1gFnPc1	vláda
nad	nad	k7c7	nad
Francií	Francie	k1gFnSc7	Francie
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
bez	bez	k7c2	bez
jediného	jediný	k2eAgInSc2d1	jediný
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
===	===	k?	===
Konsolidace	konsolidace	k1gFnSc2	konsolidace
Francie	Francie	k1gFnSc2	Francie
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
uchopení	uchopení	k1gNnSc6	uchopení
moci	moc	k1gFnSc2	moc
bylo	být	k5eAaImAgNnS	být
Napoleonovým	Napoleonův	k2eAgNnSc7d1	Napoleonovo
prvním	první	k4xOgInSc7	první
úkolem	úkol	k1gInSc7	úkol
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
vybudovat	vybudovat	k5eAaPmF	vybudovat
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úmyslem	úmysl	k1gInSc7	úmysl
bylo	být	k5eAaImAgNnS	být
dohodnout	dohodnout	k5eAaPmF	dohodnout
se	se	k3xPyFc4	se
s	s	k7c7	s
evropskými	evropský	k2eAgFnPc7d1	Evropská
mocnostmi	mocnost	k1gFnPc7	mocnost
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
záležitosti	záležitost	k1gFnSc6	záležitost
si	se	k3xPyFc3	se
nedělal	dělat	k5eNaImAgMnS	dělat
žádné	žádný	k3yNgFnPc4	žádný
iluze	iluze	k1gFnPc4	iluze
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
tím	ten	k3xDgNnSc7	ten
hlavním	hlavní	k2eAgNnSc7d1	hlavní
<g/>
,	,	kIx,	,
co	co	k9	co
jej	on	k3xPp3gNnSc4	on
mohlo	moct	k5eAaImAgNnS	moct
udržet	udržet	k5eAaPmF	udržet
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Odvedl	odvést	k5eAaPmAgMnS	odvést
dalších	další	k2eAgNnPc2d1	další
50	[number]	k4	50
000	[number]	k4	000
branců	branec	k1gMnPc2	branec
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
Grouchy	Groucha	k1gFnSc2	Groucha
zlikvidoval	zlikvidovat	k5eAaPmAgMnS	zlikvidovat
poslední	poslední	k2eAgMnSc1d1	poslední
místa	místo	k1gNnPc1	místo
royalistického	royalistický	k2eAgInSc2d1	royalistický
odporu	odpor	k1gInSc2	odpor
(	(	kIx(	(
<g/>
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
počin	počin	k1gInSc4	počin
obdržel	obdržet	k5eAaPmAgMnS	obdržet
maršálskou	maršálský	k2eAgFnSc4d1	maršálská
hůl	hůl	k1gFnSc4	hůl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
též	též	k9	též
o	o	k7c4	o
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
návrhům	návrh	k1gInPc3	návrh
na	na	k7c4	na
trvalý	trvalý	k2eAgInSc4d1	trvalý
mír	mír	k1gInSc4	mír
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
hluchá	hluchý	k2eAgFnSc1d1	hluchá
<g/>
.	.	kIx.	.
</s>
<s>
Rezidenti	rezident	k1gMnPc1	rezident
vítězných	vítězný	k2eAgFnPc2d1	vítězná
mocností	mocnost	k1gFnPc2	mocnost
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
postupu	postup	k1gInSc6	postup
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
osobě	osoba	k1gFnSc3	osoba
a	a	k8xC	a
jako	jako	k8xC	jako
narušitel	narušitel	k1gMnSc1	narušitel
pokoje	pokoj	k1gInSc2	pokoj
světa	svět	k1gInSc2	svět
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
soudnímu	soudní	k2eAgNnSc3d1	soudní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
1	[number]	k4	1
800	[number]	k4	800
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poblíž	poblíž	k7c2	poblíž
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
nacházely	nacházet	k5eAaImAgFnP	nacházet
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
armády	armáda	k1gFnPc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Pruská	pruský	k2eAgFnSc1d1	pruská
dolnorýnská	dolnorýnská	k1gFnSc1	dolnorýnská
maršála	maršál	k1gMnSc2	maršál
Blüchera	Blücher	k1gMnSc2	Blücher
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
anglo-batavská	angloatavský	k2eAgFnSc1d1	anglo-batavský
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Wellingtonu	Wellington	k1gInSc2	Wellington
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
invazi	invaze	k1gFnSc4	invaze
čelit	čelit	k5eAaImF	čelit
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
buď	buď	k8xC	buď
nepřítele	nepřítel	k1gMnSc4	nepřítel
nechat	nechat	k5eAaPmF	nechat
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
protáhnout	protáhnout	k5eAaPmF	protáhnout
tak	tak	k9	tak
jeho	jeho	k3xOp3gFnPc1	jeho
operační	operační	k2eAgFnPc1d1	operační
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
by	by	kYmCp3nS	by
hlavní	hlavní	k2eAgFnSc1d1	hlavní
armáda	armáda	k1gFnSc1	armáda
operovala	operovat	k5eAaImAgFnS	operovat
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Seiny	Seina	k1gFnPc1	Seina
a	a	k8xC	a
mobilizované	mobilizovaný	k2eAgFnPc1d1	mobilizovaná
síly	síla	k1gFnPc1	síla
ozbrojeného	ozbrojený	k2eAgInSc2d1	ozbrojený
lidu	lid	k1gInSc2	lid
by	by	kYmCp3nP	by
bránily	bránit	k5eAaImAgFnP	bránit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
mohl	moct	k5eAaImAgMnS	moct
sám	sám	k3xTgMnSc1	sám
zaútočit	zaútočit	k5eAaPmF	zaútočit
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
likvidovat	likvidovat	k5eAaBmF	likvidovat
armády	armáda	k1gFnSc2	armáda
protivníka	protivník	k1gMnSc2	protivník
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nP	by
přicházely	přicházet	k5eAaImAgFnP	přicházet
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
první	první	k4xOgFnSc1	první
možnost	možnost	k1gFnSc1	možnost
blízce	blízce	k6eAd1	blízce
připomínala	připomínat	k5eAaImAgFnS	připomínat
ne	ne	k9	ne
moc	moc	k6eAd1	moc
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
kampaň	kampaň	k1gFnSc4	kampaň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
eventualita	eventualita	k1gFnSc1	eventualita
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
stávající	stávající	k2eAgFnSc3d1	stávající
situaci	situace	k1gFnSc3	situace
jevila	jevit	k5eAaImAgFnS	jevit
mnohem	mnohem	k6eAd1	mnohem
reálnější	reální	k2eAgFnSc1d2	reální
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečil	zabezpečit	k5eAaPmAgMnS	zabezpečit
tedy	tedy	k9	tedy
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
německými	německý	k2eAgInPc7d1	německý
státy	stát	k1gInPc7	stát
i	i	k8xC	i
pobřeží	pobřeží	k1gNnSc1	pobřeží
a	a	k8xC	a
se	s	k7c7	s
zbývající	zbývající	k2eAgFnSc7d1	zbývající
armádou	armáda	k1gFnSc7	armáda
128	[number]	k4	128
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nazval	nazvat	k5eAaPmAgInS	nazvat
Armée	Armée	k1gInSc1	Armée
du	du	k?	du
Nord	Nord	k1gInSc1	Nord
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
využít	využít	k5eAaPmF	využít
momentu	moment	k1gInSc3	moment
překvapení	překvapení	k1gNnSc2	překvapení
<g/>
,	,	kIx,	,
vklínit	vklínit	k5eAaPmF	vklínit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
nejbližší	blízký	k2eAgFnPc4d3	nejbližší
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
vojska	vojsko	k1gNnPc4	vojsko
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
je	být	k5eAaImIp3nS	být
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
začal	začít	k5eAaPmAgMnS	začít
potají	potají	k6eAd1	potají
<g />
.	.	kIx.	.
</s>
<s>
soustřeďovat	soustřeďovat	k5eAaImF	soustřeďovat
své	svůj	k3xOyFgInPc4	svůj
sbory	sbor	k1gInPc4	sbor
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Sambrou	Sambra	k1gFnSc7	Sambra
a	a	k8xC	a
Mázou	Máza	k1gFnSc7	Máza
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jinými	jiný	k2eAgInPc7d1	jiný
ostentativními	ostentativní	k2eAgInPc7d1	ostentativní
manévry	manévr	k1gInPc7	manévr
snažil	snažit	k5eAaImAgMnS	snažit
vyvolat	vyvolat	k5eAaPmF	vyvolat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
jeho	jeho	k3xOp3gFnPc2	jeho
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
bude	být	k5eAaImBp3nS	být
směřovat	směřovat	k5eAaImF	směřovat
mezi	mezi	k7c4	mezi
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
Brusel	Brusel	k1gInSc4	Brusel
<g/>
.	.	kIx.	.
<g/>
Velitel	velitel	k1gMnSc1	velitel
anglo-batavské	angloatavský	k2eAgFnSc2d1	anglo-batavský
armády	armáda	k1gFnSc2	armáda
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Wellingtonu	Wellington	k1gInSc2	Wellington
reagoval	reagovat	k5eAaBmAgMnS	reagovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
přál	přát	k5eAaImAgMnS	přát
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
rozmístil	rozmístit	k5eAaPmAgMnS	rozmístit
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Brusel-Ostende	Brusel-Ostend	k1gInSc5	Brusel-Ostend
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Prusy	Prus	k1gMnPc4	Prus
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
maršála	maršál	k1gMnSc2	maršál
Blüchera	Blücher	k1gMnSc2	Blücher
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
necelých	celý	k2eNgInPc2d1	necelý
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
široká	široký	k2eAgFnSc1d1	široká
mezera	mezera	k1gFnSc1	mezera
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
střežil	střežit	k5eAaImAgMnS	střežit
pouze	pouze	k6eAd1	pouze
I.	I.	kA	I.
pruský	pruský	k2eAgInSc1d1	pruský
sbor	sbor	k1gInSc1	sbor
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generálmajora	generálmajor	k1gMnSc2	generálmajor
Zietena	Zieten	k2eAgFnSc1d1	Zieten
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
okolností	okolnost	k1gFnPc2	okolnost
Bonaparte	bonapart	k1gInSc5	bonapart
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
opustil	opustit	k5eAaPmAgMnS	opustit
Paříž	Paříž	k1gFnSc4	Paříž
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
do	do	k7c2	do
škvíry	škvíra	k1gFnSc2	škvíra
mezi	mezi	k7c7	mezi
koaličními	koaliční	k2eAgFnPc7d1	koaliční
armádami	armáda	k1gFnPc7	armáda
i	i	k9	i
Armée	Armée	k1gInSc4	Armée
du	du	k?	du
Nord	Nord	k1gInSc1	Nord
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Francouzi	Francouz	k1gMnPc1	Francouz
překročili	překročit	k5eAaPmAgMnP	překročit
belgické	belgický	k2eAgFnPc4d1	belgická
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
za	za	k7c2	za
prvních	první	k4xOgInPc2	první
bojů	boj	k1gInPc2	boj
se	s	k7c7	s
Zietenem	Zieten	k1gInSc7	Zieten
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
města	město	k1gNnSc2	město
Charleroi	Charlero	k1gFnSc2	Charlero
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
pruský	pruský	k2eAgMnSc1d1	pruský
generál	generál	k1gMnSc1	generál
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
hodinách	hodina	k1gFnPc6	hodina
setrvale	setrvale	k6eAd1	setrvale
zatlačován	zatlačován	k2eAgInSc4d1	zatlačován
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vést	vést	k5eAaImF	vést
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
zdržovací	zdržovací	k2eAgInSc4d1	zdržovací
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
během	během	k7c2	během
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
proudech	proud	k1gInPc6	proud
spořádaně	spořádaně	k6eAd1	spořádaně
stahoval	stahovat	k5eAaImAgMnS	stahovat
na	na	k7c4	na
Brusel	Brusel	k1gInSc4	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
ústup	ústup	k1gInSc1	ústup
vedl	vést	k5eAaImAgInS	vést
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
křižovatku	křižovatka	k1gFnSc4	křižovatka
cest	cesta	k1gFnPc2	cesta
u	u	k7c2	u
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
vesničky	vesnička	k1gFnSc2	vesnička
Quatre-Bras	Quatre-Brasa	k1gFnPc2	Quatre-Brasa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
narychlo	narychlo	k6eAd1	narychlo
shromažďoval	shromažďovat	k5eAaImAgMnS	shromažďovat
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Wellingtonu	Wellington	k1gInSc2	Wellington
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
k	k	k7c3	k
vesnici	vesnice	k1gFnSc3	vesnice
Ligny	Ligna	k1gFnSc2	Ligna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
soustřeďovaly	soustřeďovat	k5eAaImAgInP	soustřeďovat
zbylé	zbylý	k2eAgInPc1d1	zbylý
sbory	sbor	k1gInPc1	sbor
maršála	maršál	k1gMnSc2	maršál
Blüchera	Blücher	k1gMnSc2	Blücher
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
okolí	okolí	k1gNnSc2	okolí
těchto	tento	k3xDgNnPc2	tento
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
těžkým	těžký	k2eAgInPc3d1	těžký
bojům	boj	k1gInPc3	boj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Armády	armáda	k1gFnSc2	armáda
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
armáda	armáda	k1gFnSc1	armáda
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
ve	v	k7c6	v
spěchu	spěch	k1gInSc6	spěch
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
většinu	většina	k1gFnSc4	většina
vojáků	voják	k1gMnPc2	voják
tvořili	tvořit	k5eAaImAgMnP	tvořit
císaři	císař	k1gMnPc1	císař
oddaní	oddaný	k2eAgMnPc1d1	oddaný
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
let	léto	k1gNnPc2	léto
minulých	minulý	k2eAgNnPc2d1	Minulé
v	v	k7c6	v
jejích	její	k3xOp3gFnPc6	její
řadách	řada	k1gFnPc6	řada
drtivou	drtivý	k2eAgFnSc7d1	drtivá
měrou	míra	k1gFnSc7wR	míra
převládali	převládat	k5eAaImAgMnP	převládat
rodilí	rodilý	k2eAgMnPc1d1	rodilý
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vnímalo	vnímat	k5eAaImAgNnS	vnímat
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
především	především	k6eAd1	především
jako	jako	k8xS	jako
obranu	obrana	k1gFnSc4	obrana
vlasti	vlast	k1gFnSc2	vlast
před	před	k7c7	před
invazí	invaze	k1gFnSc7	invaze
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
těch	ten	k3xDgNnPc2	ten
lepších	dobrý	k2eAgNnPc2d2	lepší
uskupení	uskupení	k1gNnPc2	uskupení
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
kdy	kdy	k6eAd1	kdy
Napoleon	napoleon	k1gInSc1	napoleon
velel	velet	k5eAaImAgInS	velet
<g/>
.	.	kIx.	.
</s>
<s>
Armée	Armée	k1gFnSc1	Armée
du	du	k?	du
Nord	Norda	k1gFnPc2	Norda
samotná	samotný	k2eAgFnSc1d1	samotná
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
gardu	garda	k1gFnSc4	garda
<g/>
,	,	kIx,	,
šest	šest	k4xCc1	šest
armádních	armádní	k2eAgMnPc2d1	armádní
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc1	čtyři
záložní	záložní	k2eAgInPc1d1	záložní
jezdecké	jezdecký	k2eAgInPc1d1	jezdecký
sbory	sbor	k1gInPc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
nedostatečný	dostatečný	k2eNgInSc4d1	nedostatečný
čas	čas	k1gInSc4	čas
větší	veliký	k2eAgFnSc7d2	veliký
měrou	míra	k1gFnSc7wR	míra
neprojevil	projevit	k5eNaPmAgInS	projevit
na	na	k7c6	na
složení	složení	k1gNnSc6	složení
a	a	k8xC	a
kvalitě	kvalita	k1gFnSc6	kvalita
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
,	,	kIx,	,
projevil	projevit	k5eAaPmAgInS	projevit
se	se	k3xPyFc4	se
na	na	k7c6	na
výstroji	výstroj	k1gFnSc6	výstroj
a	a	k8xC	a
výzbroji	výzbroj	k1gFnSc6	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Několika	několik	k4yIc3	několik
eskadronám	eskadrona	k1gFnPc3	eskadrona
kyrysníků	kyrysník	k1gMnPc2	kyrysník
chyběly	chybět	k5eAaImAgInP	chybět
kyrysy	kyrys	k1gInPc1	kyrys
<g/>
,	,	kIx,	,
jiným	jiný	k2eAgInSc7d1	jiný
jízdním	jízdní	k2eAgInSc7d1	jízdní
jednotkám	jednotka	k1gFnPc3	jednotka
závěsy	závěs	k1gInPc4	závěs
na	na	k7c4	na
pušky	puška	k1gFnPc4	puška
<g/>
,	,	kIx,	,
či	či	k8xC	či
součástky	součástka	k1gFnPc1	součástka
postrojů	postroj	k1gInPc2	postroj
<g/>
.	.	kIx.	.
</s>
<s>
Chyběly	chybět	k5eAaImAgFnP	chybět
také	také	k9	také
torny	torna	k1gFnPc1	torna
<g/>
,	,	kIx,	,
závěsy	závěsa	k1gFnPc1	závěsa
na	na	k7c4	na
šavle	šavle	k1gFnPc4	šavle
a	a	k8xC	a
patrontašky	patrontaška	k1gFnPc4	patrontaška
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
mnoho	mnoho	k4c1	mnoho
vojáků	voják	k1gMnPc2	voják
vyfasovalo	vyfasovat	k5eAaPmAgNnS	vyfasovat
pouze	pouze	k6eAd1	pouze
nastříhanou	nastříhaný	k2eAgFnSc4d1	nastříhaná
látku	látka	k1gFnSc4	látka
na	na	k7c4	na
uniformy	uniforma	k1gFnPc4	uniforma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
si	se	k3xPyFc3	se
sešívali	sešívat	k5eAaImAgMnP	sešívat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Špatná	špatný	k2eAgFnSc1d1	špatná
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
i	i	k9	i
u	u	k7c2	u
ženijního	ženijní	k2eAgInSc2d1	ženijní
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
nedostatek	nedostatek	k1gInSc1	nedostatek
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
projevit	projevit	k5eAaPmF	projevit
zejména	zejména	k9	zejména
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
Nedostávalo	dostávat	k5eNaImAgNnS	dostávat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
pokrývek	pokrývka	k1gFnPc2	pokrývka
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
císařská	císařský	k2eAgFnSc1d1	císařská
garda	garda	k1gFnSc1	garda
postrádala	postrádat	k5eAaImAgFnS	postrádat
své	svůj	k3xOyFgFnPc4	svůj
vysoké	vysoký	k2eAgFnPc4d1	vysoká
medvědice	medvědice	k1gFnPc4	medvědice
<g/>
.	.	kIx.	.
<g/>
Složení	složení	k1gNnSc1	složení
anglo-batavské	angloatavský	k2eAgFnSc2d1	anglo-batavský
armády	armáda	k1gFnSc2	armáda
bylo	být	k5eAaImAgNnS	být
značně	značně	k6eAd1	značně
nehomogenní	homogenní	k2eNgNnSc1d1	nehomogenní
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
samotných	samotný	k2eAgMnPc2d1	samotný
Britů	Brit	k1gMnPc2	Brit
ji	on	k3xPp3gFnSc4	on
tvořilo	tvořit	k5eAaImAgNnS	tvořit
20	[number]	k4	20
000	[number]	k4	000
hannoverských	hannoverský	k2eAgMnPc2d1	hannoverský
<g/>
,	,	kIx,	,
7000	[number]	k4	7000
brunšvických	brunšvický	k2eAgFnPc2d1	Brunšvická
a	a	k8xC	a
3000	[number]	k4	3000
nassavských	nassavský	k2eAgMnPc2d1	nassavský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
30	[number]	k4	30
%	%	kIx~	%
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
nizozemsko-belgických	nizozemskoelgický	k2eAgFnPc2d1	nizozemsko-belgická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
donedávna	donedávna	k6eAd1	donedávna
sloužily	sloužit	k5eAaImAgFnP	sloužit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Francouzů	Francouz	k1gMnPc2	Francouz
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
politická	politický	k2eAgFnSc1d1	politická
náklonnost	náklonnost	k1gFnSc1	náklonnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
Belgičanů	Belgičan	k1gMnPc2	Belgičan
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
značně	značně	k6eAd1	značně
nejistá	jistý	k2eNgFnSc1d1	nejistá
<g/>
.	.	kIx.	.
</s>
<s>
Wellingtonovou	Wellingtonový	k2eAgFnSc7d1	Wellingtonová
oporou	opora	k1gFnSc7	opora
byly	být	k5eAaImAgFnP	být
hlavně	hlavně	k6eAd1	hlavně
britské	britský	k2eAgInPc1d1	britský
regimenty	regiment	k1gInPc1	regiment
a	a	k8xC	a
oddíly	oddíl	k1gInPc1	oddíl
Královské	královský	k2eAgFnSc2d1	královská
německé	německý	k2eAgFnSc2d1	německá
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Slabinou	slabina	k1gFnSc7	slabina
byli	být	k5eAaImAgMnP	být
Hannoveřané	Hannoveřan	k1gMnPc1	Hannoveřan
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
vystrojeni	vystrojen	k2eAgMnPc1d1	vystrojen
v	v	k7c6	v
britských	britský	k2eAgFnPc6d1	britská
uniformách	uniforma	k1gFnPc6	uniforma
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
britský	britský	k2eAgMnSc1d1	britský
vrchní	vrchní	k1gMnSc1	vrchní
velitel	velitel	k1gMnSc1	velitel
rozptýlil	rozptýlit	k5eAaPmAgMnS	rozptýlit
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
veterány	veterán	k1gMnPc4	veterán
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
minimalizoval	minimalizovat	k5eAaBmAgMnS	minimalizovat
dezerce	dezerce	k1gFnSc1	dezerce
a	a	k8xC	a
positivně	positivně	k6eAd1	positivně
zapůsobil	zapůsobit	k5eAaPmAgInS	zapůsobit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
morálku	morálka	k1gFnSc4	morálka
<g/>
.	.	kIx.	.
</s>
<s>
Vojsko	vojsko	k1gNnSc1	vojsko
bylo	být	k5eAaImAgNnS	být
rozčleněno	rozčlenit	k5eAaPmNgNnS	rozčlenit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
armádních	armádní	k2eAgMnPc2d1	armádní
sborů	sbor	k1gInPc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Velením	velení	k1gNnSc7	velení
I.	I.	kA	I.
sboru	sbor	k1gInSc3	sbor
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
Vilém	Vilém	k1gMnSc1	Vilém
<g/>
,	,	kIx,	,
kníže	kníže	k1gMnSc1	kníže
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
II	II	kA	II
<g/>
.	.	kIx.	.
sboru	sbor	k1gInSc6	sbor
stál	stát	k5eAaImAgMnS	stát
generál	generál	k1gMnSc1	generál
lord	lord	k1gMnSc1	lord
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
záložní	záložní	k2eAgInSc1d1	záložní
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
podléhal	podléhat	k5eAaImAgMnS	podléhat
přímému	přímý	k2eAgNnSc3d1	přímé
Wellingtonovu	Wellingtonův	k2eAgNnSc3d1	Wellingtonovo
velení	velení	k1gNnSc3	velení
<g/>
.	.	kIx.	.
</s>
<s>
Jízdnímu	jízdní	k2eAgInSc3d1	jízdní
sboru	sbor	k1gInSc3	sbor
velel	velet	k5eAaImAgMnS	velet
generálporučík	generálporučík	k1gMnSc1	generálporučík
hrabě	hrabě	k1gMnSc1	hrabě
Uxbridge	Uxbridge	k1gFnSc1	Uxbridge
<g/>
.	.	kIx.	.
<g/>
Dolnorýnské	Dolnorýnský	k2eAgNnSc1d1	Dolnorýnský
vojsko	vojsko	k1gNnSc1	vojsko
maršála	maršál	k1gMnSc2	maršál
Blüchera	Blücher	k1gMnSc2	Blücher
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
tvořeno	tvořit	k5eAaImNgNnS	tvořit
jednotkami	jednotka	k1gFnPc7	jednotka
zeměbrany	zeměbrana	k1gFnSc2	zeměbrana
(	(	kIx(	(
<g/>
landwehru	landwehra	k1gFnSc4	landwehra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
špatně	špatně	k6eAd1	špatně
vycvičené	vycvičený	k2eAgFnPc1d1	vycvičená
i	i	k8xC	i
vybavené	vybavený	k2eAgFnPc1d1	vybavená
a	a	k8xC	a
postrádaly	postrádat	k5eAaImAgFnP	postrádat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
bojové	bojový	k2eAgFnPc4d1	bojová
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
vrchního	vrchní	k2eAgMnSc4d1	vrchní
velitele	velitel	k1gMnSc4	velitel
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
vlasteneckým	vlastenecký	k2eAgInSc7d1	vlastenecký
zápalem	zápal	k1gInSc7	zápal
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
mužů	muž	k1gMnPc2	muž
bývalého	bývalý	k2eAgInSc2d1	bývalý
rýnského	rýnský	k2eAgInSc2d1	rýnský
spolku	spolek	k1gInSc2	spolek
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Wellington	Wellington	k1gInSc1	Wellington
tak	tak	k9	tak
i	i	k9	i
Blücher	Blüchra	k1gFnPc2	Blüchra
byl	být	k5eAaImAgInS	být
nucen	nucen	k2eAgMnSc1d1	nucen
tyto	tento	k3xDgInPc4	tento
síly	síl	k1gInPc4	síl
promíchat	promíchat	k5eAaPmF	promíchat
se	s	k7c7	s
spolehlivými	spolehlivý	k2eAgInPc7d1	spolehlivý
pruskými	pruský	k2eAgInPc7d1	pruský
regimenty	regiment	k1gInPc7	regiment
<g/>
.	.	kIx.	.
</s>
<s>
Jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
nemělo	mít	k5eNaImAgNnS	mít
dobré	dobrá	k1gFnSc2	dobrá
koně	kůň	k1gMnPc4	kůň
a	a	k8xC	a
hodilo	hodit	k5eAaImAgNnS	hodit
se	se	k3xPyFc4	se
spíš	spíš	k9	spíš
pro	pro	k7c4	pro
průzkumné	průzkumný	k2eAgInPc4d1	průzkumný
úkoly	úkol	k1gInPc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
u	u	k7c2	u
Prusů	Prus	k1gMnPc2	Prus
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
spíš	spíš	k9	spíš
počet	počet	k1gInSc1	počet
než	než	k8xS	než
skvělá	skvělý	k2eAgFnSc1d1	skvělá
taktika	taktika	k1gFnSc1	taktika
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
výhodu	výhoda	k1gFnSc4	výhoda
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
představoval	představovat	k5eAaImAgInS	představovat
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
měli	mít	k5eAaImAgMnP	mít
nejschopnější	schopný	k2eAgInSc4d3	nejschopnější
štáb	štáb	k1gInSc4	štáb
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
armád	armáda	k1gFnPc2	armáda
bojujících	bojující	k2eAgMnPc2d1	bojující
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
nadané	nadaný	k2eAgMnPc4d1	nadaný
sborové	sborový	k2eAgMnPc4d1	sborový
velitele	velitel	k1gMnPc4	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Sbory	sbor	k1gInPc1	sbor
se	se	k3xPyFc4	se
tažení	tažení	k1gNnSc4	tažení
účastnily	účastnit	k5eAaImAgFnP	účastnit
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
;	;	kIx,	;
I.	I.	kA	I.
Zietenův	Zietenův	k2eAgMnSc1d1	Zietenův
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pirchův	Pirchův	k2eAgMnSc1d1	Pirchův
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Thielmannův	Thielmannův	k2eAgMnSc1d1	Thielmannův
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bülovův	Bülovův	k2eAgInSc1d1	Bülovův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Ligny	Ligna	k1gFnSc2	Ligna
a	a	k8xC	a
Quartre-Bras	Quartre-Brasa	k1gFnPc2	Quartre-Brasa
==	==	k?	==
</s>
</p>
<p>
<s>
Napoleonovým	Napoleonův	k2eAgInSc7d1	Napoleonův
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
postupovat	postupovat	k5eAaImF	postupovat
na	na	k7c4	na
Brusel	Brusel	k1gInSc4	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
musel	muset	k5eAaImAgMnS	muset
obsadit	obsadit	k5eAaPmF	obsadit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
křižovatku	křižovatka	k1gFnSc4	křižovatka
cest	cesta	k1gFnPc2	cesta
u	u	k7c2	u
Quartre-Bras	Quartre-Brasa	k1gFnPc2	Quartre-Brasa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
u	u	k7c2	u
Ligny	Ligna	k1gFnSc2	Ligna
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
Blücherova	Blücherův	k2eAgFnSc1d1	Blücherův
pruská	pruský	k2eAgFnSc1d1	pruská
armáda	armáda	k1gFnSc1	armáda
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
zhruba	zhruba	k6eAd1	zhruba
84	[number]	k4	84
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
71	[number]	k4	71
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
osobně	osobně	k6eAd1	osobně
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
Prusy	Prus	k1gMnPc7	Prus
a	a	k8xC	a
obsazením	obsazení	k1gNnSc7	obsazení
Quatre-Bras	Quatre-Bras	k1gMnSc1	Quatre-Bras
pověřil	pověřit	k5eAaPmAgMnS	pověřit
maršála	maršál	k1gMnSc4	maršál
Neye	Ney	k1gMnSc4	Ney
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
však	však	k9	však
s	s	k7c7	s
útokem	útok	k1gInSc7	útok
dával	dávat	k5eAaImAgInS	dávat
načas	načas	k6eAd1	načas
<g/>
.	.	kIx.	.
</s>
<s>
Dopoledne	dopoledne	k1gNnPc1	dopoledne
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
stálo	stát	k5eAaImAgNnS	stát
pouze	pouze	k6eAd1	pouze
8300	[number]	k4	8300
výše	vysoce	k6eAd2	vysoce
zmiňovaných	zmiňovaný	k2eAgMnPc2d1	zmiňovaný
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
vojáků	voják	k1gMnPc2	voják
s	s	k7c7	s
Černým	černý	k2eAgInSc7d1	černý
sborem	sbor	k1gInSc7	sbor
vévody	vévoda	k1gMnSc2	vévoda
Brunšvického	brunšvický	k2eAgMnSc2d1	brunšvický
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
tedy	tedy	k9	tedy
dvojnásobné	dvojnásobný	k2eAgNnSc4d1	dvojnásobné
přečíslení	přečíslení	k1gNnSc4	přečíslení
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
však	však	k9	však
vyčkával	vyčkávat	k5eAaImAgMnS	vyčkávat
příchodu	příchod	k1gInSc3	příchod
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonova	Erlonův	k2eAgInSc2d1	Erlonův
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
příjezdu	příjezd	k1gInSc2	příjezd
Kellermannova	Kellermannův	k2eAgNnSc2d1	Kellermannův
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
prvním	první	k4xOgNnPc3	první
útokům	útok	k1gInPc3	útok
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
frontách	fronta	k1gFnPc6	fronta
došlo	dojít	k5eAaPmAgNnS	dojít
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
po	po	k7c6	po
první	první	k4xOgFnSc6	první
hodině	hodina	k1gFnSc6	hodina
odpolední	odpolední	k2eAgNnSc1d1	odpolední
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tvrdé	tvrdý	k2eAgInPc4d1	tvrdý
boje	boj	k1gInPc4	boj
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
paralelní	paralelní	k2eAgFnPc1d1	paralelní
bitvy	bitva	k1gFnPc1	bitva
protáhly	protáhnout	k5eAaPmAgFnP	protáhnout
do	do	k7c2	do
večera	večer	k1gInSc2	večer
a	a	k8xC	a
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
ústupem	ústup	k1gInSc7	ústup
obou	dva	k4xCgFnPc2	dva
koaličních	koaliční	k2eAgFnPc2d1	koaliční
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
stahovali	stahovat	k5eAaImAgMnP	stahovat
podél	podél	k7c2	podél
bruselské	bruselský	k2eAgFnSc2d1	bruselská
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
pruské	pruský	k2eAgFnPc1d1	pruská
jednotky	jednotka	k1gFnPc1	jednotka
prchaly	prchat	k5eAaImAgFnP	prchat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Wavre	Wavr	k1gMnSc5	Wavr
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Francouzi	Francouz	k1gMnPc1	Francouz
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
první	první	k4xOgInSc4	první
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
bitvy	bitva	k1gFnPc1	bitva
byly	být	k5eAaImAgFnP	být
poznamenány	poznamenat	k5eAaPmNgFnP	poznamenat
špatnou	špatný	k2eAgFnSc7d1	špatná
komunikací	komunikace	k1gFnSc7	komunikace
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgMnPc7	dva
veliteli	velitel	k1gMnPc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Erlonův	Erlonův	k2eAgInSc1d1	Erlonův
sbor	sbor	k1gInSc1	sbor
se	se	k3xPyFc4	se
vinou	vinou	k7c2	vinou
zmatenosti	zmatenost	k1gFnSc2	zmatenost
rozkazů	rozkaz	k1gInPc2	rozkaz
přesunoval	přesunovat	k5eAaImAgInS	přesunovat
mezi	mezi	k7c7	mezi
Neyovou	Neyový	k2eAgFnSc7d1	Neyový
a	a	k8xC	a
Napoleonovou	Napoleonův	k2eAgFnSc7d1	Napoleonova
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
výrazněji	výrazně	k6eAd2	výrazně
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
celý	celý	k2eAgInSc1d1	celý
sbor	sbor	k1gInSc1	sbor
Droueta	Drouet	k1gMnSc2	Drouet
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlon	Erlon	k1gMnSc1	Erlon
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
útok	útok	k1gInSc4	útok
do	do	k7c2	do
boku	bok	k1gInSc2	bok
Prusů	Prus	k1gMnPc2	Prus
<g/>
,	,	kIx,	,
skončily	skončit	k5eAaPmAgInP	skončit
by	by	kYmCp3nP	by
boje	boj	k1gInPc1	boj
u	u	k7c2	u
Ligny	Ligna	k1gFnSc2	Ligna
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Jeny	jen	k1gInPc4	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
<g/>
Bilance	bilance	k1gFnSc1	bilance
ztrát	ztráta	k1gFnPc2	ztráta
Prusů	Prus	k1gMnPc2	Prus
byla	být	k5eAaImAgFnS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
celých	celý	k2eAgNnPc2d1	celé
16	[number]	k4	16
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
Wellingtonovy	Wellingtonův	k2eAgFnPc1d1	Wellingtonova
ztráty	ztráta	k1gFnPc1	ztráta
byly	být	k5eAaImAgFnP	být
asi	asi	k9	asi
4700	[number]	k4	4700
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
armáda	armáda	k1gFnSc1	armáda
však	však	k9	však
zdaleka	zdaleka	k6eAd1	zdaleka
nebyla	být	k5eNaImAgFnS	být
zničená	zničený	k2eAgFnSc1d1	zničená
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
si	se	k3xPyFc3	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
představoval	představovat	k5eAaImAgMnS	představovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
opaku	opak	k1gInSc2	opak
odmítal	odmítat	k5eAaImAgMnS	odmítat
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
ztratil	ztratit	k5eAaPmAgInS	ztratit
mezi	mezi	k7c7	mezi
8500	[number]	k4	8500
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
000	[number]	k4	000
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
domníval	domnívat	k5eAaImAgMnS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zbývá	zbývat	k5eAaImIp3nS	zbývat
vypořádat	vypořádat	k5eAaPmF	vypořádat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
Angličany	Angličan	k1gMnPc7	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
průzkum	průzkum	k1gInSc1	průzkum
nebyl	být	k5eNaImAgInS	být
sto	sto	k4xCgNnSc4	sto
odhalit	odhalit	k5eAaPmF	odhalit
trasu	trasa	k1gFnSc4	trasa
ústupu	ústup	k1gInSc2	ústup
Prusů	Prus	k1gMnPc2	Prus
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ti	ten	k3xDgMnPc1	ten
dál	daleko	k6eAd2	daleko
kryjí	krýt	k5eAaImIp3nP	krýt
přístupové	přístupový	k2eAgFnPc1d1	přístupová
cesty	cesta	k1gFnPc1	cesta
na	na	k7c4	na
Brusel	Brusel	k1gInSc4	Brusel
a	a	k8xC	a
znova	znova	k6eAd1	znova
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
u	u	k7c2	u
Wavre	Wavr	k1gInSc5	Wavr
(	(	kIx(	(
<g/>
11	[number]	k4	11
km	km	kA	km
od	od	k7c2	od
Ligny	Ligna	k1gFnSc2	Ligna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
zadní	zadní	k2eAgInSc1d1	zadní
voj	voj	k1gInSc1	voj
zmátl	zmást	k5eAaPmAgInS	zmást
francouzské	francouzský	k2eAgNnSc4d1	francouzské
jezdectvo	jezdectvo	k1gNnSc4	jezdectvo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
Napoleonovu	Napoleonův	k2eAgInSc3d1	Napoleonův
chybnému	chybný	k2eAgInSc3d1	chybný
předpokladu	předpoklad	k1gInSc3	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prusové	Prus	k1gMnPc1	Prus
prchají	prchat	k5eAaImIp3nP	prchat
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
a	a	k8xC	a
nepředstavují	představovat	k5eNaImIp3nP	představovat
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
vážnější	vážní	k2eAgFnSc4d2	vážnější
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
promarněném	promarněný	k2eAgInSc6d1	promarněný
půl	půl	k1xP	půl
dni	den	k1gInSc6	den
dal	dát	k5eAaPmAgInS	dát
rozkaz	rozkaz	k1gInSc1	rozkaz
maršálu	maršál	k1gMnSc3	maršál
Emmanuelu	Emmanuel	k1gMnSc3	Emmanuel
de	de	k?	de
Grouchymu	Grouchymum	k1gNnSc6	Grouchymum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
pravým	pravý	k2eAgNnSc7d1	pravé
uskupením	uskupení	k1gNnSc7	uskupení
(	(	kIx(	(
<g/>
asi	asi	k9	asi
33	[number]	k4	33
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
96	[number]	k4	96
děl	dělo	k1gNnPc2	dělo
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
zbytek	zbytek	k1gInSc1	zbytek
pruské	pruský	k2eAgFnSc2d1	pruská
armády	armáda	k1gFnSc2	armáda
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
a	a	k8xC	a
dovršil	dovršit	k5eAaPmAgMnS	dovršit
tak	tak	k9	tak
její	její	k3xOp3gFnSc4	její
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Grouchyho	Grouchyze	k6eAd1	Grouchyze
nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
však	však	k9	však
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
výsledek	výsledek	k1gInSc4	výsledek
fatální	fatální	k2eAgInSc4d1	fatální
<g/>
.	.	kIx.	.
<g/>
Než	než	k8xS	než
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
akci	akce	k1gFnSc3	akce
<g/>
,	,	kIx,	,
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Wellingtonovi	Wellingtonův	k2eAgMnPc1d1	Wellingtonův
Angličané	Angličan	k1gMnPc1	Angličan
ústup	ústup	k1gInSc4	ústup
od	od	k7c2	od
Quatre-Bras	Quatre-Bras	k1gMnSc1	Quatre-Bras
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Brusel	Brusel	k1gInSc4	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vydatných	vydatný	k2eAgInPc2d1	vydatný
přívalů	příval	k1gInPc2	příval
deště	dešť	k1gInSc2	dešť
a	a	k8xC	a
díky	díky	k7c3	díky
schopnému	schopný	k2eAgInSc3d1	schopný
zadnímu	zadní	k2eAgInSc3d1	zadní
voji	voj	k1gInSc3	voj
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
střetl	střetnout	k5eAaPmAgMnS	střetnout
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
na	na	k7c6	na
mostě	most	k1gInSc6	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Dyle	Dyl	k1gInSc2	Dyl
u	u	k7c2	u
Genappe	Genapp	k1gInSc5	Genapp
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
ještě	ještě	k6eAd1	ještě
večer	večer	k6eAd1	večer
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
podařilo	podařit	k5eAaPmAgNnS	podařit
zaujmout	zaujmout	k5eAaPmF	zaujmout
postavení	postavení	k1gNnSc4	postavení
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
již	již	k9	již
britský	britský	k2eAgMnSc1d1	britský
velitel	velitel	k1gMnSc1	velitel
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
definitivně	definitivně	k6eAd1	definitivně
vyčkat	vyčkat	k5eAaPmF	vyčkat
Napoleonova	Napoleonův	k2eAgInSc2d1	Napoleonův
příchodu	příchod	k1gInSc2	příchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Plány	plán	k1gInPc1	plán
a	a	k8xC	a
rozestavení	rozestavení	k1gNnSc1	rozestavení
vojsk	vojsko	k1gNnPc2	vojsko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Napoleonovo	Napoleonův	k2eAgNnSc1d1	Napoleonovo
jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
k	k	k7c3	k
hostinci	hostinec	k1gInSc3	hostinec
Belle	bell	k1gInSc5	bell
Alliance	Allianka	k1gFnSc3	Allianka
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
rozvinovat	rozvinovat	k5eAaImF	rozvinovat
do	do	k7c2	do
bojových	bojový	k2eAgFnPc2d1	bojová
sestav	sestava	k1gFnPc2	sestava
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
již	již	k9	již
osm	osm	k4xCc1	osm
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
promarněného	promarněný	k2eAgNnSc2d1	promarněné
dopoledne	dopoledne	k1gNnSc2	dopoledne
teď	teď	k6eAd1	teď
císaři	císař	k1gMnSc3	císař
Francouzů	Francouz	k1gMnPc2	Francouz
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
bojových	bojový	k2eAgFnPc6d1	bojová
akcích	akce	k1gFnPc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
armády	armáda	k1gFnPc1	armáda
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
utábořily	utábořit	k5eAaPmAgFnP	utábořit
na	na	k7c6	na
protilehlých	protilehlý	k2eAgInPc6d1	protilehlý
svazích	svah	k1gInPc6	svah
budoucího	budoucí	k2eAgNnSc2d1	budoucí
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
posléze	posléze	k6eAd1	posléze
nadiktoval	nadiktovat	k5eAaPmAgMnS	nadiktovat
instrukce	instrukce	k1gFnPc4	instrukce
jak	jak	k8xC	jak
postupovat	postupovat	k5eAaImF	postupovat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nP	kdyby
Angličané	Angličan	k1gMnPc1	Angličan
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
ústupu	ústup	k1gInSc6	ústup
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
již	již	k6eAd1	již
Wellington	Wellington	k1gInSc4	Wellington
nepomýšlel	pomýšlet	k5eNaImAgMnS	pomýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
terén	terén	k1gInSc1	terén
dokonale	dokonale	k6eAd1	dokonale
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
jeho	jeho	k3xOp3gInSc1	jeho
taktice	taktika	k1gFnSc3	taktika
defenzivního	defenzivní	k2eAgNnSc2d1	defenzivní
pojetí	pojetí	k1gNnSc2	pojetí
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
osvědčila	osvědčit	k5eAaPmAgFnS	osvědčit
tzv.	tzv.	kA	tzv.
strategie	strategie	k1gFnSc1	strategie
odvráceného	odvrácený	k2eAgInSc2d1	odvrácený
svahu	svah	k1gInSc2	svah
(	(	kIx(	(
<g/>
reverse	reverse	k1gFnSc1	reverse
slope	slopat	k5eAaImIp3nS	slopat
<g/>
)	)	kIx)	)
a	a	k8xC	a
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
i	i	k8xC	i
tentokrát	tentokrát	k6eAd1	tentokrát
zaznamená	zaznamenat	k5eAaPmIp3nS	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
ráno	ráno	k6eAd1	ráno
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
již	již	k6eAd1	již
dobře	dobře	k6eAd1	dobře
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Prusové	Prus	k1gMnPc1	Prus
stačí	stačit	k5eAaBmIp3nP	stačit
dorazit	dorazit	k5eAaPmF	dorazit
včas	včas	k6eAd1	včas
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnPc1	první
linie	linie	k1gFnPc1	linie
Wellingtonových	Wellingtonový	k2eAgFnPc2d1	Wellingtonová
jednotek	jednotka	k1gFnPc2	jednotka
stála	stát	k5eAaImAgFnS	stát
podél	podél	k7c2	podél
Ohainské	Ohainský	k2eAgFnSc2d1	Ohainský
silnice	silnice	k1gFnSc2	silnice
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
a	a	k8xC	a
půl	půl	k1xP	půl
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Středu	středa	k1gFnSc4	středa
velel	velet	k5eAaImAgMnS	velet
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
Vilém	Vilém	k1gMnSc1	Vilém
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
statku	statek	k1gInSc6	statek
La	la	k1gNnSc2	la
Haye	Hayus	k1gMnSc5	Hayus
Sainte	Saint	k1gMnSc5	Saint
<g/>
,	,	kIx,	,
vysunutém	vysunutý	k2eAgInSc6d1	vysunutý
před	před	k7c7	před
středem	střed	k1gInSc7	střed
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
opevněny	opevněn	k2eAgFnPc1d1	opevněna
jednotky	jednotka	k1gFnPc1	jednotka
královské	královský	k2eAgFnSc2d1	královská
německé	německý	k2eAgFnSc2d1	německá
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Levému	levý	k2eAgNnSc3d1	levé
křídlu	křídlo	k1gNnSc3	křídlo
velel	velet	k5eAaImAgMnS	velet
generálporučík	generálporučík	k1gMnSc1	generálporučík
Thomas	Thomas	k1gMnSc1	Thomas
Picton	Picton	k1gInSc4	Picton
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
uniforma	uniforma	k1gFnSc1	uniforma
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zavazadly	zavazadlo	k1gNnPc7	zavazadlo
zůstala	zůstat	k5eAaPmAgFnS	zůstat
kdesi	kdesi	k6eAd1	kdesi
vzadu	vzadu	k6eAd1	vzadu
a	a	k8xC	a
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
svých	svůj	k3xOyFgMnPc2	svůj
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
civilních	civilní	k2eAgInPc6d1	civilní
šatech	šat	k1gInPc6	šat
<g/>
.	.	kIx.	.
</s>
<s>
Pravému	pravý	k2eAgNnSc3d1	pravé
křídlu	křídlo	k1gNnSc3	křídlo
spojenců	spojenec	k1gMnPc2	spojenec
velel	velet	k5eAaImAgMnS	velet
generál	generál	k1gMnSc1	generál
Rolland	Rolland	k1gMnSc1	Rolland
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
možnosti	možnost	k1gFnSc3	možnost
obchvatu	obchvat	k1gInSc2	obchvat
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
křídlo	křídlo	k1gNnSc1	křídlo
podstatně	podstatně	k6eAd1	podstatně
silnější	silný	k2eAgMnSc1d2	silnější
než	než	k8xS	než
postavení	postavení	k1gNnSc1	postavení
nalevo	nalevo	k6eAd1	nalevo
od	od	k7c2	od
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Předsunutou	předsunutý	k2eAgFnSc4d1	předsunutá
baštu	bašta	k1gFnSc4	bašta
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
britské	britský	k2eAgFnSc2d1	britská
linie	linie	k1gFnSc2	linie
tvořil	tvořit	k5eAaImAgInS	tvořit
zámeček	zámeček	k1gInSc1	zámeček
Hougoumont	Hougoumont	k1gInSc1	Hougoumont
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
i	i	k9	i
s	s	k7c7	s
přilehlým	přilehlý	k2eAgInSc7d1	přilehlý
lesem	les	k1gInSc7	les
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
zhruba	zhruba	k6eAd1	zhruba
1400	[number]	k4	1400
hannoverských	hannoverský	k2eAgMnPc2d1	hannoverský
<g/>
,	,	kIx,	,
nassavských	nassavský	k2eAgMnPc2d1	nassavský
a	a	k8xC	a
britských	britský	k2eAgMnPc2d1	britský
pěšáků	pěšák	k1gMnPc2	pěšák
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Wellingtonova	Wellingtonův	k2eAgFnSc1d1	Wellingtonova
armáda	armáda	k1gFnSc1	armáda
dle	dle	k7c2	dle
dispozic	dispozice	k1gFnPc2	dispozice
vyčkávala	vyčkávat	k5eAaImAgFnS	vyčkávat
za	za	k7c7	za
odvráceným	odvrácený	k2eAgInSc7d1	odvrácený
svahem	svah	k1gInSc7	svah
Ohainské	Ohainský	k2eAgFnSc2d1	Ohainský
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
bylo	být	k5eAaImAgNnS	být
rozmístěno	rozmístit	k5eAaPmNgNnS	rozmístit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
anglo-batavských	angloatavský	k2eAgFnPc2d1	anglo-batavský
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
zády	záda	k1gNnPc7	záda
spojenců	spojenec	k1gMnPc2	spojenec
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
les	les	k1gInSc1	les
Soiges	Soiges	k1gInSc1	Soiges
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
porost	porost	k1gInSc1	porost
byl	být	k5eAaImAgInS	být
dokonale	dokonale	k6eAd1	dokonale
průchodný	průchodný	k2eAgInSc1d1	průchodný
a	a	k8xC	a
neskýtal	skýtat	k5eNaImAgInS	skýtat
proto	proto	k8xC	proto
strategickou	strategický	k2eAgFnSc4d1	strategická
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
si	se	k3xPyFc3	se
naopak	naopak	k6eAd1	naopak
přivodil	přivodit	k5eAaPmAgMnS	přivodit
Napoleon	napoleon	k1gInSc4	napoleon
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
si	se	k3xPyFc3	se
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
nechal	nechat	k5eAaPmAgMnS	nechat
Genappe	Genapp	k1gInSc5	Genapp
se	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
málo	málo	k6eAd1	málo
průchodnými	průchodný	k2eAgInPc7d1	průchodný
mosty	most	k1gInPc7	most
<g/>
.	.	kIx.	.
<g/>
Napoleonův	Napoleonův	k2eAgInSc4d1	Napoleonův
záměr	záměr	k1gInSc4	záměr
pro	pro	k7c4	pro
nadcházející	nadcházející	k2eAgFnPc4d1	nadcházející
hodiny	hodina	k1gFnPc4	hodina
byl	být	k5eAaImAgInS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Hougoumont	Hougoumont	k1gInSc4	Hougoumont
napadnout	napadnout	k5eAaPmF	napadnout
anglo-batavské	angloatavský	k2eAgNnSc4d1	anglo-batavský
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
přinutit	přinutit	k5eAaPmF	přinutit
generála	generál	k1gMnSc4	generál
Hilla	Hill	k1gMnSc4	Hill
k	k	k7c3	k
přeskupení	přeskupení	k1gNnSc3	přeskupení
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
odvrátit	odvrátit	k5eAaPmF	odvrátit
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
střed	střed	k1gInSc4	střed
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
také	také	k9	také
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
slabší	slabý	k2eAgNnSc1d2	slabší
Hillovo	Hillův	k2eAgNnSc1d1	Hillovo
křídlo	křídlo	k1gNnSc1	křídlo
bude	být	k5eAaImBp3nS	být
posíleno	posílit	k5eAaPmNgNnS	posílit
právě	právě	k9	právě
jednotkami	jednotka	k1gFnPc7	jednotka
středu	střed	k1gInSc2	střed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
útok	útok	k1gInSc1	útok
na	na	k7c4	na
oslabenou	oslabený	k2eAgFnSc4d1	oslabená
centrální	centrální	k2eAgFnSc4d1	centrální
část	část	k1gFnSc4	část
Wellingtonových	Wellingtonový	k2eAgFnPc2d1	Wellingtonová
pozic	pozice	k1gFnPc2	pozice
měl	mít	k5eAaImAgInS	mít
posléze	posléze	k6eAd1	posléze
prorazit	prorazit	k5eAaPmF	prorazit
obranu	obrana	k1gFnSc4	obrana
jeho	jeho	k3xOp3gFnSc2	jeho
sestavy	sestava	k1gFnSc2	sestava
a	a	k8xC	a
rozetnout	rozetnout	k5eAaPmF	rozetnout
ji	on	k3xPp3gFnSc4	on
vedví	vedví	k6eAd1	vedví
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
útok	útok	k1gInSc1	útok
na	na	k7c4	na
levé	levý	k2eAgNnSc4d1	levé
anglo-batavské	angloatavský	k2eAgNnSc4d1	anglo-batavský
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
snad	snad	k9	snad
také	také	k9	také
odvrátit	odvrátit	k5eAaPmF	odvrátit
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
směru	směr	k1gInSc2	směr
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
posléze	posléze	k6eAd1	posléze
mohl	moct	k5eAaImAgInS	moct
přerůst	přerůst	k5eAaPmF	přerůst
v	v	k7c4	v
obchvat	obchvat	k1gInSc4	obchvat
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
přetnul	přetnout	k5eAaPmAgInS	přetnout
Wellingtonovu	Wellingtonův	k2eAgFnSc4d1	Wellingtonova
ústupovou	ústupový	k2eAgFnSc4d1	ústupová
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
a	a	k8xC	a
přispěl	přispět	k5eAaPmAgMnS	přispět
by	by	kYmCp3nS	by
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
konečnému	konečný	k2eAgNnSc3d1	konečné
zničení	zničení	k1gNnSc3	zničení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poslední	poslední	k2eAgFnPc1d1	poslední
přípravy	příprava	k1gFnPc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Maršál	maršál	k1gMnSc1	maršál
Blücher	Blüchra	k1gFnPc2	Blüchra
<g/>
,	,	kIx,	,
shromažďující	shromažďující	k2eAgNnSc4d1	shromažďující
své	své	k1gNnSc4	své
muže	muž	k1gMnSc2	muž
u	u	k7c2	u
Wavre	Wavr	k1gInSc5	Wavr
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
Wellesleyovi	Wellesleya	k1gMnSc3	Wellesleya
své	svůj	k3xOyFgNnSc4	svůj
slovo	slovo	k1gNnSc4	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gInSc3	on
u	u	k7c2	u
Mont-Saint-Jean	Mont-Saint-Jeany	k1gInPc2	Mont-Saint-Jeany
pomůže	pomoct	k5eAaPmIp3nS	pomoct
minimálně	minimálně	k6eAd1	minimálně
dvěma	dva	k4xCgInPc7	dva
sbory	sbor	k1gInPc7	sbor
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
slib	slib	k1gInSc4	slib
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
dodržet	dodržet	k5eAaPmF	dodržet
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k1gFnSc2	ranní
proto	proto	k8xC	proto
vydal	vydat	k5eAaPmAgInS	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
generálu	generál	k1gMnSc3	generál
Büllowovi	Büllowa	k1gMnSc3	Büllowa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
sborem	sbor	k1gInSc7	sbor
vydal	vydat	k5eAaPmAgMnS	vydat
přes	přes	k7c4	přes
Wavre	Wavr	k1gInSc5	Wavr
k	k	k7c3	k
Chapel	Chapela	k1gFnPc2	Chapela
<g/>
–	–	k?	–
<g/>
le	le	k?	le
<g/>
–	–	k?	–
<g/>
Saint	Saint	k1gMnSc1	Saint
<g/>
–	–	k?	–
<g/>
Lambert	Lambert	k1gMnSc1	Lambert
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
pruský	pruský	k2eAgMnSc1d1	pruský
velitel	velitel	k1gMnSc1	velitel
dorazil	dorazit	k5eAaPmAgMnS	dorazit
kolem	kolem	k7c2	kolem
desáté	desátá	k1gFnSc2	desátá
hodiny	hodina	k1gFnPc4	hodina
dopolední	dopolední	k2eAgFnPc4d1	dopolední
<g/>
.	.	kIx.	.
</s>
<s>
Pirchův	Pirchův	k2eAgMnSc1d1	Pirchův
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Zietenův	Zietenův	k2eAgInSc1d1	Zietenův
I.	I.	kA	I.
sbor	sbor	k1gInSc1	sbor
vyrazily	vyrazit	k5eAaPmAgInP	vyrazit
k	k	k7c3	k
Waterloo	Waterloo	k1gNnSc3	Waterloo
teprve	teprve	k6eAd1	teprve
kolem	kolem	k6eAd1	kolem
poledne	poledne	k1gNnSc4	poledne
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
pruský	pruský	k2eAgInSc1d1	pruský
průzkum	průzkum	k1gInSc1	průzkum
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
stojí	stát	k5eAaImIp3nS	stát
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
000	[number]	k4	000
Grouchyho	Grouchy	k1gMnSc2	Grouchy
Francouzů	Francouz	k1gMnPc2	Francouz
(	(	kIx(	(
<g/>
pruský	pruský	k2eAgInSc1d1	pruský
odhad	odhad	k1gInSc1	odhad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zadržením	zadržení	k1gNnSc7	zadržení
této	tento	k3xDgFnSc2	tento
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
kolony	kolona	k1gFnSc2	kolona
bylo	být	k5eAaImAgNnS	být
pověřeno	pověřit	k5eAaPmNgNnS	pověřit
15	[number]	k4	15
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
III	III	kA	III
<g/>
.	.	kIx.	.
sboru	sbor	k1gInSc2	sbor
generála	generál	k1gMnSc4	generál
Thielmanna	Thielmann	k1gMnSc4	Thielmann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
Napoleon	napoleon	k1gInSc1	napoleon
kolem	kolem	k7c2	kolem
páté	pátý	k4xOgFnSc2	pátý
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k1gFnSc2	ranní
nadiktoval	nadiktovat	k5eAaPmAgInS	nadiktovat
maršálu	maršál	k1gMnSc3	maršál
Soultovi	Soult	k1gMnSc3	Soult
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
armáda	armáda	k1gFnSc1	armáda
připravena	připravit	k5eAaPmNgFnS	připravit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pozicích	pozice	k1gFnPc6	pozice
v	v	k7c4	v
devět	devět	k4xCc4	devět
hodin	hodina	k1gFnPc2	hodina
dopoledne	dopoledne	k6eAd1	dopoledne
<g/>
,	,	kIx,	,
v	v	k7c4	v
určenou	určený	k2eAgFnSc4d1	určená
dobu	doba	k1gFnSc4	doba
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
rovině	rovina	k1gFnSc6	rovina
u	u	k7c2	u
La	la	k1gNnSc2	la
Belle	bell	k1gInSc5	bell
Alliance	Allianka	k1gFnSc6	Allianka
připraveny	připravit	k5eAaPmNgInP	připravit
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
pěší	pěší	k2eAgFnPc4d1	pěší
divize	divize	k1gFnPc4	divize
I.	I.	kA	I.
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonova	Erlonův	k2eAgInSc2d1	Erlonův
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
divize	divize	k1gFnPc4	divize
jízdy	jízda	k1gFnSc2	jízda
generálů	generál	k1gMnPc2	generál
Domona	Domon	k1gMnSc2	Domon
<g/>
,	,	kIx,	,
Subervieho	Subervie	k1gMnSc2	Subervie
a	a	k8xC	a
Jacquinota	Jacquinota	k1gFnSc1	Jacquinota
<g/>
,	,	kIx,	,
kavalerie	kavalerie	k1gFnSc1	kavalerie
císařské	císařský	k2eAgFnSc2d1	císařská
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
kyrysníci	kyrysník	k1gMnPc1	kyrysník
Milhaudova	Milhaudův	k2eAgNnSc2d1	Milhaudův
4	[number]	k4	4
<g/>
.	.	kIx.	.
jezdeckého	jezdecký	k2eAgInSc2d1	jezdecký
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
armády	armáda	k1gFnSc2	armáda
kvůli	kvůli	k7c3	kvůli
celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
trvajícímu	trvající	k2eAgInSc3d1	trvající
dešti	dešť	k1gInSc3	dešť
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
rozmáčel	rozmáčet	k5eAaBmAgMnS	rozmáčet
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
znesnadnil	znesnadnit	k5eAaPmAgInS	znesnadnit
zbytku	zbytek	k1gInSc3	zbytek
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
pochod	pochod	k1gInSc1	pochod
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
dělostřelectvu	dělostřelectvo	k1gNnSc3	dělostřelectvo
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
sešikovat	sešikovat	k5eAaPmF	sešikovat
do	do	k7c2	do
určených	určený	k2eAgFnPc2d1	určená
pozic	pozice	k1gFnPc2	pozice
teprve	teprve	k6eAd1	teprve
v	v	k7c4	v
jedenáct	jedenáct	k4xCc4	jedenáct
hodin	hodina	k1gFnPc2	hodina
dopoledne	dopoledne	k6eAd1	dopoledne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
hodin	hodina	k1gFnPc2	hodina
zazněly	zaznět	k5eAaImAgFnP	zaznět
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
strany	strana	k1gFnSc2	strana
tři	tři	k4xCgInPc1	tři
dělové	dělový	k2eAgInPc1d1	dělový
výstřely	výstřel	k1gInPc1	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
signálem	signál	k1gInSc7	signál
pro	pro	k7c4	pro
8000	[number]	k4	8000
mužů	muž	k1gMnPc2	muž
6	[number]	k4	6
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
Jérôma	Jérôm	k1gMnSc4	Jérôm
Bonaparta	Bonapart	k1gMnSc4	Bonapart
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zahájili	zahájit	k5eAaPmAgMnP	zahájit
pozvolný	pozvolný	k2eAgInSc4d1	pozvolný
postup	postup	k1gInSc4	postup
k	k	k7c3	k
lesíku	lesík	k1gInSc3	lesík
<g/>
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
skrýval	skrývat	k5eAaImAgMnS	skrývat
zámeček	zámeček	k1gInSc4	zámeček
Hougoumont	Hougoumonta	k1gFnPc2	Hougoumonta
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
jednotkami	jednotka	k1gFnPc7	jednotka
se	se	k3xPyFc4	se
pohnuly	pohnout	k5eAaPmAgFnP	pohnout
vpřed	vpřed	k6eAd1	vpřed
Foyova	Foyovo	k1gNnSc2	Foyovo
a	a	k8xC	a
Bacheluho	Bachelu	k1gMnSc2	Bachelu
divize	divize	k1gFnSc2	divize
Reilleho	Reille	k1gMnSc2	Reille
II	II	kA	II
<g/>
.	.	kIx.	.
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
I.	I.	kA	I.
sbor	sbor	k1gInSc1	sbor
divizního	divizní	k2eAgMnSc2d1	divizní
generála	generál	k1gMnSc2	generál
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlona	Erlon	k1gMnSc2	Erlon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
stovkách	stovka	k1gFnPc6	stovka
metrů	metr	k1gInPc2	metr
dolů	dolů	k6eAd1	dolů
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
se	se	k3xPyFc4	se
však	však	k9	však
tyto	tento	k3xDgFnPc1	tento
síly	síla	k1gFnPc1	síla
zastavily	zastavit	k5eAaPmAgFnP	zastavit
(	(	kIx(	(
<g/>
asi	asi	k9	asi
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vpřed	vpřed	k6eAd1	vpřed
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
pouze	pouze	k6eAd1	pouze
jednotky	jednotka	k1gFnPc1	jednotka
směřující	směřující	k2eAgFnPc1d1	směřující
na	na	k7c4	na
Hougoumont	Hougoumont	k1gInSc4	Hougoumont
<g/>
.	.	kIx.	.
</s>
<s>
Prodlení	prodlení	k1gNnSc1	prodlení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
francouzské	francouzský	k2eAgFnPc1d1	francouzská
jednotky	jednotka	k1gFnPc1	jednotka
nabraly	nabrat	k5eAaPmAgFnP	nabrat
oproti	oproti	k7c3	oproti
dispozicím	dispozice	k1gFnPc3	dispozice
<g/>
,	,	kIx,	,
nahrávalo	nahrávat	k5eAaImAgNnS	nahrávat
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pruská	pruský	k2eAgFnSc1d1	pruská
armáda	armáda	k1gFnSc1	armáda
každou	každý	k3xTgFnSc7	každý
hodinou	hodina	k1gFnSc7	hodina
zkracovala	zkracovat	k5eAaImAgFnS	zkracovat
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jí	on	k3xPp3gFnSc3	on
zbývala	zbývat	k5eAaImAgFnS	zbývat
k	k	k7c3	k
budoucímu	budoucí	k2eAgNnSc3d1	budoucí
bojišti	bojiště	k1gNnSc3	bojiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bitva	bitva	k1gFnSc1	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hougoumont	Hougoumonto	k1gNnPc2	Hougoumonto
===	===	k?	===
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nP	aby
muži	muž	k1gMnPc1	muž
císařova	císařův	k2eAgMnSc2d1	císařův
bratra	bratr	k1gMnSc2	bratr
Jérôma	Jérôm	k1gMnSc2	Jérôm
mohli	moct	k5eAaImAgMnP	moct
zahájit	zahájit	k5eAaPmF	zahájit
útok	útok	k1gInSc4	útok
na	na	k7c4	na
anglické	anglický	k2eAgNnSc4d1	anglické
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
nejprve	nejprve	k6eAd1	nejprve
proniknout	proniknout	k5eAaPmF	proniknout
kolem	kolem	k7c2	kolem
komplexu	komplex	k1gInSc2	komplex
budov	budova	k1gFnPc2	budova
zámečku	zámeček	k1gInSc2	zámeček
Hougoumont	Hougoumonta	k1gFnPc2	Hougoumonta
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
předpokladem	předpoklad	k1gInSc7	předpoklad
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
tohoto	tento	k3xDgInSc2	tento
úspěchu	úspěch	k1gInSc2	úspěch
bylo	být	k5eAaImAgNnS	být
obsazení	obsazení	k1gNnSc1	obsazení
lesíka	lesík	k1gInSc2	lesík
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
celou	celý	k2eAgFnSc4d1	celá
pozici	pozice	k1gFnSc4	pozice
skrýval	skrývat	k5eAaImAgMnS	skrývat
jejich	jejich	k3xOp3gNnPc3	jejich
očím	oko	k1gNnPc3	oko
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
nassavským	nassavský	k2eAgInSc7d1	nassavský
pěším	pěší	k2eAgInSc7d1	pěší
praporem	prapor	k1gInSc7	prapor
a	a	k8xC	a
rotou	rota	k1gFnSc7	rota
hannoverských	hannoverský	k2eAgMnPc2d1	hannoverský
karabiníků	karabiník	k1gMnPc2	karabiník
<g/>
.	.	kIx.	.
</s>
<s>
Probít	probít	k5eAaPmF	probít
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
jednotky	jednotka	k1gFnPc4	jednotka
jim	on	k3xPp3gFnPc3	on
trvalo	trvat	k5eAaImAgNnS	trvat
zhruba	zhruba	k6eAd1	zhruba
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
vrhli	vrhnout	k5eAaImAgMnP	vrhnout
proti	proti	k7c3	proti
jižní	jižní	k2eAgFnSc3d1	jižní
bráně	brána	k1gFnSc3	brána
zámečku	zámeček	k1gInSc2	zámeček
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
prudkou	prudký	k2eAgFnSc7d1	prudká
palbou	palba	k1gFnSc7	palba
odražen	odrazit	k5eAaPmNgMnS	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vše	všechen	k3xTgNnSc4	všechen
sledoval	sledovat	k5eAaImAgInS	sledovat
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
velitelského	velitelský	k2eAgNnSc2d1	velitelské
stanoviště	stanoviště	k1gNnSc2	stanoviště
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgInS	nařídit
majorovi	major	k1gMnSc3	major
Bullovi	Bull	k1gMnSc3	Bull
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podpořil	podpořit	k5eAaPmAgMnS	podpořit
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
na	na	k7c4	na
Francouze	Francouz	k1gMnSc4	Francouz
u	u	k7c2	u
Hougoumontu	Hougoumont	k1gInSc2	Hougoumont
dělostřeleckou	dělostřelecký	k2eAgFnSc4d1	dělostřelecká
palbu	palba	k1gFnSc4	palba
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
však	však	k9	však
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
angličtí	anglický	k2eAgMnPc1d1	anglický
dělostřelci	dělostřelec	k1gMnPc1	dělostřelec
přilákali	přilákat	k5eAaPmAgMnP	přilákat
pozornost	pozornost	k1gFnSc4	pozornost
jízdních	jízdní	k2eAgMnPc2d1	jízdní
dělostřelců	dělostřelec	k1gMnPc2	dělostřelec
Pirého	Pirého	k2eAgFnSc2d1	Pirého
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
generál	generál	k1gMnSc1	generál
Reille	Reille	k1gFnSc1	Reille
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
štábu	štáb	k1gInSc2	štáb
II	II	kA	II
<g/>
.	.	kIx.	.
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
náčelník	náčelník	k1gInSc1	náčelník
jeho	jeho	k3xOp3gInSc2	jeho
štábu	štáb	k1gInSc2	štáb
Guillemont	Guillemont	k1gMnSc1	Guillemont
další	další	k2eAgFnPc4d1	další
akce	akce	k1gFnPc4	akce
proti	proti	k7c3	proti
Hougoumontu	Hougoumont	k1gMnSc3	Hougoumont
zakázali	zakázat	k5eAaPmAgMnP	zakázat
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
Bonaparte	bonapart	k1gInSc5	bonapart
jejich	jejich	k3xOp3gMnSc1	jejich
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nerespektoval	respektovat	k5eNaImAgMnS	respektovat
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
svých	svůj	k3xOyFgFnPc2	svůj
jednotek	jednotka	k1gFnPc2	jednotka
se	se	k3xPyFc4	se
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
své	svůj	k3xOyFgMnPc4	svůj
muže	muž	k1gMnPc4	muž
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
a	a	k8xC	a
podnikl	podniknout	k5eAaPmAgMnS	podniknout
útok	útok	k1gInSc4	útok
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
brány	brána	k1gFnPc4	brána
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
však	však	k9	však
neměli	mít	k5eNaImAgMnP	mít
páčidla	páčidlo	k1gNnPc4	páčidlo
ani	ani	k8xC	ani
miny	mina	k1gFnPc4	mina
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
byli	být	k5eAaImAgMnP	být
i	i	k9	i
přes	přes	k7c4	přes
tvrdé	tvrdé	k1gNnSc4	tvrdé
boje	boj	k1gInSc2	boj
brzy	brzy	k6eAd1	brzy
odraženi	odražen	k2eAgMnPc1d1	odražen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
úseku	úsek	k1gInSc6	úsek
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
brigádě	brigáda	k1gFnSc6	brigáda
již	již	k6eAd1	již
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
generála	generál	k1gMnSc2	generál
Bauduina	Bauduin	k1gMnSc2	Bauduin
bránu	brána	k1gFnSc4	brána
podařilo	podařit	k5eAaPmAgNnS	podařit
vyrazit	vyrazit	k5eAaPmF	vyrazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
záhy	záhy	k6eAd1	záhy
byla	být	k5eAaImAgFnS	být
prudkým	prudký	k2eAgInSc7d1	prudký
bodákovým	bodákový	k2eAgInSc7d1	bodákový
protiútokem	protiútok	k1gInSc7	protiútok
z	z	k7c2	z
objektu	objekt	k1gInSc2	objekt
vytlačena	vytlačen	k2eAgFnSc1d1	vytlačena
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
neúspěchy	neúspěch	k1gInPc4	neúspěch
Jérômovi	Jérôm	k1gMnSc6	Jérôm
muži	muž	k1gMnSc6	muž
v	v	k7c6	v
obléhání	obléhání	k1gNnSc6	obléhání
neustávali	ustávat	k5eNaImAgMnP	ustávat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vše	všechen	k3xTgNnSc1	všechen
došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
zkušený	zkušený	k2eAgMnSc1d1	zkušený
generál	generál	k1gMnSc1	generál
Reille	Reill	k1gMnSc2	Reill
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
plýtvání	plýtvání	k1gNnSc1	plýtvání
lidskými	lidský	k2eAgFnPc7d1	lidská
životy	život	k1gInPc4	život
a	a	k8xC	a
neúčelné	účelný	k2eNgNnSc1d1	neúčelné
počínání	počínání	k1gNnSc1	počínání
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
na	na	k7c4	na
úsek	úsek	k1gInSc4	úsek
vrhl	vrhnout	k5eAaPmAgMnS	vrhnout
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Foyovu	Foyov	k1gInSc2	Foyov
divizí	divize	k1gFnPc2	divize
(	(	kIx(	(
<g/>
4700	[number]	k4	4700
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
celé	celý	k2eAgFnSc2d1	celá
akce	akce	k1gFnSc2	akce
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
2500	[number]	k4	2500
anglo	anglo	k1gNnSc4	anglo
<g/>
–	–	k?	–
<g/>
batavských	batavský	k2eAgMnPc2d1	batavský
vojáků	voják	k1gMnPc2	voják
vázalo	vázat	k5eAaImAgNnS	vázat
10	[number]	k4	10
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
Napoleonových	Napoleonových	k2eAgMnPc2d1	Napoleonových
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
si	se	k3xPyFc3	se
brzy	brzy	k6eAd1	brzy
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
císař	císař	k1gMnSc1	císař
Francouzů	Francouz	k1gMnPc2	Francouz
hlavní	hlavní	k2eAgInSc4d1	hlavní
útok	útok	k1gInSc4	útok
nepovede	vést	k5eNaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Začínal	začínat	k5eAaImAgMnS	začínat
si	se	k3xPyFc3	se
být	být	k5eAaImF	být
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
bude	být	k5eAaImBp3nS	být
směřovat	směřovat	k5eAaImF	směřovat
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gInSc3	jeho
středu	střed	k1gInSc3	střed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
průlom	průlom	k1gInSc4	průlom
===	===	k?	===
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
odpolední	odpolední	k2eAgNnPc1d1	odpolední
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Wellington	Wellington	k1gInSc1	Wellington
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
je	být	k5eAaImIp3nS	být
prozatím	prozatím	k6eAd1	prozatím
oddáleno	oddálit	k5eAaPmNgNnS	oddálit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
Mont-Saint-Jean	Mont-Saint-Jeana	k1gFnPc2	Mont-Saint-Jeana
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
dělostřelci	dělostřelec	k1gMnPc1	dělostřelec
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnPc4d1	velká
baterie	baterie	k1gFnPc4	baterie
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
vrchním	vrchní	k2eAgNnSc7d1	vrchní
velením	velení	k1gNnSc7	velení
Michela	Michel	k1gMnSc2	Michel
Neye	Ney	k1gMnSc2	Ney
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
palby	palba	k1gFnSc2	palba
na	na	k7c4	na
anglo-batavské	angloatavský	k2eAgFnPc4d1	anglo-batavský
pozice	pozice	k1gFnPc4	pozice
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
údolí	údolí	k1gNnSc2	údolí
záhy	záhy	k6eAd1	záhy
zahalilo	zahalit	k5eAaPmAgNnS	zahalit
dýmem	dým	k1gInSc7	dým
ze	z	k7c2	z
spáleného	spálený	k2eAgInSc2d1	spálený
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
přesunul	přesunout	k5eAaPmAgInS	přesunout
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
okolní	okolní	k2eAgFnSc4d1	okolní
pozici	pozice	k1gFnSc4	pozice
u	u	k7c2	u
statku	statek	k1gInSc2	statek
Rossomne	Rossomn	k1gMnSc5	Rossomn
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
předveden	předveden	k2eAgMnSc1d1	předveden
zajatý	zajatý	k2eAgMnSc1d1	zajatý
důstojník	důstojník	k1gMnSc1	důstojník
pruského	pruský	k2eAgInSc2d1	pruský
jízdního	jízdní	k2eAgInSc2d1	jízdní
Landwehru	Landwehra	k1gFnSc4	Landwehra
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
poslal	poslat	k5eAaPmAgMnS	poslat
brigádní	brigádní	k2eAgMnSc1d1	brigádní
generál	generál	k1gMnSc1	generál
Marcellin	Marcellina	k1gFnPc2	Marcellina
de	de	k?	de
Marbot	Marbota	k1gFnPc2	Marbota
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
černých	černý	k2eAgMnPc2d1	černý
husarů	husar	k1gMnPc2	husar
Bonapartovi	Bonapartův	k2eAgMnPc1d1	Bonapartův
prozradil	prozradit	k5eAaPmAgMnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
císař	císař	k1gMnSc1	císař
vidí	vidět	k5eAaImIp3nS	vidět
na	na	k7c6	na
horizontu	horizont	k1gInSc6	horizont
svého	svůj	k3xOyFgNnSc2	svůj
pravého	pravý	k2eAgNnSc2d1	pravé
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
částí	část	k1gFnSc7	část
předvoje	předvoj	k1gInSc2	předvoj
Bülovova	Bülovův	k2eAgInSc2d1	Bülovův
IV	IV	kA	IV
<g/>
.	.	kIx.	.
pruského	pruský	k2eAgInSc2d1	pruský
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
okamžitě	okamžitě	k6eAd1	okamžitě
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
na	na	k7c6	na
ohroženém	ohrožený	k2eAgNnSc6d1	ohrožené
křídle	křídlo	k1gNnSc6	křídlo
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Plancenoit	Plancenoita	k1gFnPc2	Plancenoita
dvě	dva	k4xCgFnPc4	dva
jezdecké	jezdecký	k2eAgFnPc4d1	jezdecká
divize	divize	k1gFnPc4	divize
(	(	kIx(	(
<g/>
3000	[number]	k4	3000
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
7000	[number]	k4	7000
mužů	muž	k1gMnPc2	muž
VI	VI	kA	VI
<g/>
.	.	kIx.	.
pěšího	pěší	k1gMnSc2	pěší
sboru	sbor	k1gInSc2	sbor
generála	generál	k1gMnSc4	generál
Moutona	Mouton	k1gMnSc4	Mouton
de	de	k?	de
Lobau	Lobaa	k1gMnSc4	Lobaa
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
zaujmout	zaujmout	k5eAaPmF	zaujmout
obranné	obranný	k2eAgNnSc4d1	obranné
postavení	postavení	k1gNnSc4	postavení
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
linie	linie	k1gFnSc2	linie
levým	levý	k2eAgInSc7d1	levý
bokem	bok	k1gInSc7	bok
opírala	opírat	k5eAaImAgFnS	opírat
o	o	k7c4	o
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
I.	I.	kA	I.
sboru	sbor	k1gInSc2	sbor
generála	generál	k1gMnSc4	generál
Droueta	Drouet	k1gMnSc4	Drouet
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlon	Erlon	k1gMnSc1	Erlon
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
odeslal	odeslat	k5eAaPmAgMnS	odeslat
depeši	depeše	k1gFnSc4	depeše
maršálu	maršál	k1gMnSc3	maršál
Grouchymu	Grouchym	k1gInSc2	Grouchym
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
apeloval	apelovat	k5eAaImAgMnS	apelovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c4	po
dalších	další	k2eAgNnPc2d1	další
třicet	třicet	k4xCc4	třicet
minut	minuta	k1gFnPc2	minuta
byl	být	k5eAaImAgInS	být
střed	střed	k1gInSc1	střed
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
pozic	pozice	k1gFnPc2	pozice
bombardován	bombardovat	k5eAaImNgInS	bombardovat
dělostřelbou	dělostřelba	k1gFnSc7	dělostřelba
velké	velký	k2eAgFnSc2d1	velká
baterie	baterie	k1gFnSc2	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Wellington	Wellington	k1gInSc1	Wellington
zamezil	zamezit	k5eAaPmAgInS	zamezit
velkým	velký	k2eAgFnPc3d1	velká
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgInS	nechat
pěchotu	pěchota	k1gFnSc4	pěchota
rozestavenou	rozestavený	k2eAgFnSc4d1	rozestavená
za	za	k7c7	za
hřebenem	hřeben	k1gInSc7	hřeben
svahu	svah	k1gInSc2	svah
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
linií	linie	k1gFnPc2	linie
ve	v	k7c6	v
dvouřadech	dvouřad	k1gInPc6	dvouřad
zalehnout	zalehnout	k5eAaPmF	zalehnout
a	a	k8xC	a
využít	využít	k5eAaPmF	využít
všech	všecek	k3xTgInPc2	všecek
terénních	terénní	k2eAgInPc2d1	terénní
krytů	kryt	k1gInPc2	kryt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
dělostřelecké	dělostřelecký	k2eAgFnSc6d1	dělostřelecká
přípravě	příprava	k1gFnSc6	příprava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
teoreticky	teoreticky	k6eAd1	teoreticky
výrazně	výrazně	k6eAd1	výrazně
narušit	narušit	k5eAaPmF	narušit
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
Napoleon	napoleon	k1gInSc1	napoleon
povel	povel	k1gInSc1	povel
I.	I.	kA	I.
sboru	sbor	k1gInSc3	sbor
Droueta	Droueta	k1gFnSc1	Droueta
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlon	Erlon	k1gMnSc1	Erlon
pod	pod	k7c7	pod
Neyovým	Neyův	k2eAgNnSc7d1	Neyův
velením	velení	k1gNnSc7	velení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
spojenecký	spojenecký	k2eAgInSc4d1	spojenecký
střed	střed	k1gInSc4	střed
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c4	v
císařův	císařův	k2eAgInSc4d1	císařův
neprospěch	neprospěch	k1gInSc4	neprospěch
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
musel	muset	k5eAaImAgMnS	muset
podniknout	podniknout	k5eAaPmF	podniknout
něco	něco	k3yInSc4	něco
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objeví	objevit	k5eAaPmIp3nS	objevit
celá	celý	k2eAgFnSc1d1	celá
pruská	pruský	k2eAgFnSc1d1	pruská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1	čtyři
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonovy	Erlonův	k2eAgFnSc2d1	Erlonův
divize	divize	k1gFnSc2	divize
vyrazily	vyrazit	k5eAaPmAgFnP	vyrazit
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
různých	různý	k2eAgFnPc6d1	různá
sestavách	sestava	k1gFnPc6	sestava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všech	všecek	k3xTgMnPc2	všecek
16	[number]	k4	16
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
čekala	čekat	k5eAaImAgFnS	čekat
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
museli	muset	k5eAaImAgMnP	muset
v	v	k7c6	v
sevřených	sevřený	k2eAgFnPc6d1	sevřená
masách	masa	k1gFnPc6	masa
urazit	urazit	k5eAaPmF	urazit
1200	[number]	k4	1200
metrů	metr	k1gInPc2	metr
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
otevřeného	otevřený	k2eAgInSc2d1	otevřený
terénu	terén	k1gInSc2	terén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
cesty	cesta	k1gFnSc2	cesta
dolů	dolů	k6eAd1	dolů
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
pozvolného	pozvolný	k2eAgNnSc2d1	pozvolné
stoupání	stoupání	k1gNnSc2	stoupání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
pod	pod	k7c7	pod
těžkou	těžký	k2eAgFnSc7d1	těžká
dělostřelbou	dělostřelba	k1gFnSc7	dělostřelba
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
baterií	baterie	k1gFnPc2	baterie
ukrytých	ukrytý	k2eAgNnPc2d1	ukryté
v	v	k7c6	v
zásecích	zásek	k1gInPc6	zásek
a	a	k8xC	a
za	za	k7c7	za
pevnými	pevný	k2eAgFnPc7d1	pevná
ohradami	ohrada	k1gFnPc7	ohrada
<g/>
.	.	kIx.	.
</s>
<s>
Masa	masa	k1gFnSc1	masa
postupující	postupující	k2eAgFnSc2d1	postupující
pěchoty	pěchota	k1gFnSc2	pěchota
skýtala	skýtat	k5eAaImAgFnS	skýtat
snadný	snadný	k2eAgInSc4d1	snadný
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
těžké	těžký	k2eAgFnPc1d1	těžká
ztráty	ztráta	k1gFnPc1	ztráta
nezabránily	zabránit	k5eNaPmAgFnP	zabránit
Francouzům	Francouz	k1gMnPc3	Francouz
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
určených	určený	k2eAgInPc2d1	určený
úseků	úsek	k1gInPc2	úsek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
přímé	přímý	k2eAgInPc1d1	přímý
boje	boj	k1gInPc1	boj
se	se	k3xPyFc4	se
rozhořely	rozhořet	k5eAaPmAgInP	rozhořet
u	u	k7c2	u
La	la	k1gNnSc2	la
Haye-Sainte	Haye-Saint	k1gMnSc5	Haye-Saint
<g/>
.	.	kIx.	.
</s>
<s>
Statek	statek	k1gInSc1	statek
byl	být	k5eAaImAgInS	být
bráněn	bránit	k5eAaImNgInS	bránit
2	[number]	k4	2
<g/>
.	.	kIx.	.
lehkým	lehký	k2eAgInSc7d1	lehký
praporem	prapor	k1gInSc7	prapor
královské	královský	k2eAgFnSc2d1	královská
německé	německý	k2eAgFnSc2d1	německá
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
majora	major	k1gMnSc2	major
Georga	Georg	k1gMnSc2	Georg
Baringa	Baring	k1gMnSc2	Baring
<g/>
,	,	kIx,	,
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
posíleným	posílený	k2eAgInSc7d1	posílený
jedním	jeden	k4xCgInSc7	jeden
praporem	prapor	k1gInSc7	prapor
Hannoveřanů	Hannoveřan	k1gMnPc2	Hannoveřan
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
výš	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
u	u	k7c2	u
pískovcového	pískovcový	k2eAgInSc2d1	pískovcový
lomu	lom	k1gInSc2	lom
<g/>
,	,	kIx,	,
zaujali	zaujmout	k5eAaPmAgMnP	zaujmout
pozice	pozice	k1gFnPc4	pozice
ostrostřelci	ostrostřelec	k1gMnPc1	ostrostřelec
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
praporu	prapor	k1gInSc2	prapor
95	[number]	k4	95
<g/>
.	.	kIx.	.
střeleckého	střelecký	k2eAgInSc2d1	střelecký
pluku	pluk	k1gInSc2	pluk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Donzelotova	Donzelotův	k2eAgFnSc1d1	Donzelotův
divize	divize	k1gFnSc1	divize
<g/>
,	,	kIx,	,
útočící	útočící	k2eAgFnSc1d1	útočící
více	hodně	k6eAd2	hodně
vpravo	vpravo	k6eAd1	vpravo
od	od	k7c2	od
statku	statek	k1gInSc2	statek
<g/>
,	,	kIx,	,
postoupila	postoupit	k5eAaPmAgFnS	postoupit
přes	přes	k7c4	přes
odvrácený	odvrácený	k2eAgInSc4d1	odvrácený
svah	svah	k1gInSc4	svah
v	v	k7c6	v
anglo-batavských	angloatavský	k2eAgFnPc6d1	anglo-batavský
pozicích	pozice	k1gFnPc6	pozice
<g/>
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgMnSc7	jenž
v	v	k7c6	v
obilí	obilí	k1gNnSc6	obilí
čekalo	čekat	k5eAaImAgNnS	čekat
ukrytých	ukrytý	k2eAgNnPc2d1	ukryté
3000	[number]	k4	3000
Pictonových	Pictonový	k2eAgMnPc2d1	Pictonový
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
zvedli	zvednout	k5eAaPmAgMnP	zvednout
z	z	k7c2	z
obilného	obilný	k2eAgInSc2d1	obilný
porostu	porost	k1gInSc2	porost
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
čelo	čelo	k1gNnSc4	čelo
francouzských	francouzský	k2eAgNnPc2d1	francouzské
kolon	kolon	k1gNnPc2	kolon
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
čtyřicet	čtyřicet	k4xCc4	čtyřicet
kroků	krok	k1gInPc2	krok
a	a	k8xC	a
zahájili	zahájit	k5eAaPmAgMnP	zahájit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
valivou	valivý	k2eAgFnSc4d1	valivá
palbu	palba	k1gFnSc4	palba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
entrée	entrée	k1gNnSc6	entrée
Angličané	Angličan	k1gMnPc1	Angličan
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
do	do	k7c2	do
bodákového	bodákový	k2eAgInSc2d1	bodákový
protiútoku	protiútok	k1gInSc2	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
jich	on	k3xPp3gInPc2	on
však	však	k9	však
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
počet	počet	k1gInSc1	počet
a	a	k8xC	a
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
tudíž	tudíž	k8xC	tudíž
masu	masa	k1gFnSc4	masa
francouzské	francouzský	k2eAgFnSc2d1	francouzská
pěchoty	pěchota	k1gFnSc2	pěchota
vrhnout	vrhnout	k5eAaImF	vrhnout
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Donzelotovým	Donzelotův	k2eAgFnPc3d1	Donzelotův
jednotkám	jednotka	k1gFnPc3	jednotka
přicházeli	přicházet	k5eAaImAgMnP	přicházet
vojáci	voják	k1gMnPc1	voják
Marcognetovy	Marcognetův	k2eAgFnSc2d1	Marcognetův
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
anglického	anglický	k2eAgNnSc2d1	anglické
levého	levý	k2eAgNnSc2d1	levé
křídla	křídlo	k1gNnSc2	křídlo
generál	generál	k1gMnSc1	generál
Picton	Picton	k1gInSc1	Picton
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
povolat	povolat	k5eAaPmF	povolat
do	do	k7c2	do
protiútoku	protiútok	k1gInSc2	protiútok
další	další	k2eAgFnSc2d1	další
Rally	Ralla	k1gFnSc2	Ralla
the	the	k?	the
Highlanders	Highlanders	k1gInSc1	Highlanders
(	(	kIx(	(
<g/>
Skotské	skotský	k2eAgMnPc4d1	skotský
horaly	horal	k1gMnPc4	horal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
kulí	kule	k1gFnSc7	kule
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
Blížila	blížit	k5eAaImAgFnS	blížit
se	se	k3xPyFc4	se
třetí	třetí	k4xOgFnSc1	třetí
hodina	hodina	k1gFnSc1	hodina
odpolední	odpolední	k1gNnSc2	odpolední
a	a	k8xC	a
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozice	pozice	k1gFnSc1	pozice
spojenců	spojenec	k1gMnPc2	spojenec
budou	být	k5eAaImBp3nP	být
brzy	brzy	k6eAd1	brzy
proraženy	proražen	k2eAgFnPc1d1	proražena
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
statek	statek	k1gInSc4	statek
Papelotte	Papelott	k1gInSc5	Papelott
a	a	k8xC	a
Milhaudovi	Milhaudův	k2eAgMnPc1d1	Milhaudův
kyrysníci	kyrysník	k1gMnPc1	kyrysník
samostatně	samostatně	k6eAd1	samostatně
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
Angličany	Angličan	k1gMnPc4	Angličan
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
bruselské	bruselský	k2eAgFnSc2d1	bruselská
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jejich	jejich	k3xOp3gInSc7	jejich
útokem	útok	k1gInSc7	útok
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
dokonce	dokonce	k9	dokonce
i	i	k9	i
Wellington	Wellington	k1gInSc4	Wellington
ukrýt	ukrýt	k5eAaPmF	ukrýt
uvnitř	uvnitř	k7c2	uvnitř
karé	karé	k1gNnSc2	karé
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
však	však	k9	však
na	na	k7c4	na
Milhaudovy	Milhaudův	k2eAgMnPc4d1	Milhaudův
muže	muž	k1gMnPc4	muž
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
čtyři	čtyři	k4xCgInPc1	čtyři
Somersetovy	Somersetův	k2eAgInPc1d1	Somersetův
jezdecké	jezdecký	k2eAgInPc1d1	jezdecký
gardové	gardový	k2eAgInPc1d1	gardový
pluky	pluk	k1gInPc1	pluk
a	a	k8xC	a
pronásledovaly	pronásledovat	k5eAaImAgFnP	pronásledovat
je	on	k3xPp3gFnPc4	on
dolů	dolů	k6eAd1	dolů
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
k	k	k7c3	k
Belle	bell	k1gInSc5	bell
Alliance	Allianec	k1gInPc1	Allianec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
brigády	brigáda	k1gFnPc4	brigáda
těžké	těžký	k2eAgFnSc2d1	těžká
jízdy	jízda	k1gFnSc2	jízda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
lorda	lord	k1gMnSc2	lord
Uxbridge	Uxbridg	k1gMnSc2	Uxbridg
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
na	na	k7c6	na
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonovy	Erlonův	k2eAgFnPc1d1	Erlonův
pěší	pěší	k2eAgFnPc1d1	pěší
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
nestačily	stačit	k5eNaBmAgInP	stačit
přeskupit	přeskupit	k5eAaPmF	přeskupit
do	do	k7c2	do
čtverců	čtverec	k1gInPc2	čtverec
a	a	k8xC	a
v	v	k7c6	v
panice	panika	k1gFnSc6	panika
začaly	začít	k5eAaPmAgFnP	začít
prchat	prchat	k5eAaImF	prchat
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
pozicím	pozice	k1gFnPc3	pozice
<g/>
.	.	kIx.	.
</s>
<s>
I.	I.	kA	I.
francouzský	francouzský	k2eAgInSc1d1	francouzský
armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
svého	svůj	k3xOyFgInSc2	svůj
početního	početní	k2eAgInSc2d1	početní
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
asi	asi	k9	asi
5000	[number]	k4	5000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
nebo	nebo	k8xC	nebo
zabito	zabít	k5eAaPmNgNnS	zabít
<g/>
,	,	kIx,	,
zbylí	zbylý	k2eAgMnPc1d1	zbylý
padli	padnout	k5eAaImAgMnP	padnout
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
děla	dělo	k1gNnPc4	dělo
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
pluky	pluk	k1gInPc1	pluk
(	(	kIx(	(
<g/>
45	[number]	k4	45
<g/>
.	.	kIx.	.
řadový	řadový	k2eAgMnSc1d1	řadový
a	a	k8xC	a
105	[number]	k4	105
<g/>
.	.	kIx.	.
řadový	řadový	k2eAgInSc1d1	řadový
<g/>
)	)	kIx)	)
o	o	k7c4	o
své	svůj	k3xOyFgFnPc4	svůj
plukovní	plukovní	k2eAgFnPc4d1	plukovní
zástavy	zástava	k1gFnPc4	zástava
(	(	kIx(	(
<g/>
plukovní	plukovní	k2eAgMnPc4d1	plukovní
orly	orel	k1gMnPc4	orel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglickým	anglický	k2eAgInPc3d1	anglický
jezdcům	jezdec	k1gInPc3	jezdec
ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc1	tento
úspěch	úspěch	k1gInSc1	úspěch
nestačil	stačit	k5eNaBmAgInS	stačit
a	a	k8xC	a
v	v	k7c6	v
bojovém	bojový	k2eAgInSc6d1	bojový
zápalu	zápal	k1gInSc6	zápal
dál	daleko	k6eAd2	daleko
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
pronásledování	pronásledování	k1gNnSc6	pronásledování
prchajících	prchající	k2eAgFnPc2d1	prchající
jednotek	jednotka	k1gFnPc2	jednotka
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
k	k	k7c3	k
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnSc3d1	velká
baterii	baterie	k1gFnSc3	baterie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
cíli	cíl	k1gInSc3	cíl
však	však	k9	však
nedorazili	dorazit	k5eNaPmAgMnP	dorazit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
smělost	smělost	k1gFnSc4	smělost
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
francouzská	francouzský	k2eAgFnSc1d1	francouzská
kavalerie	kavalerie	k1gFnSc1	kavalerie
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
polovina	polovina	k1gFnSc1	polovina
Delortovy	Delortův	k2eAgFnSc2d1	Delortův
14	[number]	k4	14
<g/>
.	.	kIx.	.
kyrysnické	kyrysnický	k2eAgFnSc2d1	kyrysnický
divize	divize	k1gFnSc2	divize
a	a	k8xC	a
Jacquinotova	Jacquinotův	k2eAgFnSc1d1	Jacquinotův
1	[number]	k4	1
<g/>
.	.	kIx.	.
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
divize	divize	k1gFnSc1	divize
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgMnS	být
masakr	masakr	k1gInSc4	masakr
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
stál	stát	k5eAaImAgInS	stát
jen	jen	k9	jen
Somersetovy	Somersetův	k2eAgInPc4d1	Somersetův
dragouny	dragoun	k1gInPc4	dragoun
na	na	k7c4	na
osm	osm	k4xCc4	osm
stovek	stovka	k1gFnPc2	stovka
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Angličanů	Angličan	k1gMnPc2	Angličan
se	se	k3xPyFc4	se
francouzská	francouzský	k2eAgFnSc1d1	francouzská
jízda	jízda	k1gFnSc1	jízda
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
úspěchem	úspěch	k1gInSc7	úspěch
spokojila	spokojit	k5eAaPmAgFnS	spokojit
a	a	k8xC	a
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
výchozích	výchozí	k2eAgFnPc2d1	výchozí
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Útok	útok	k1gInSc1	útok
kavalerie	kavalerie	k1gFnSc2	kavalerie
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ústupu	ústup	k1gInSc6	ústup
Francouzů	Francouz	k1gMnPc2	Francouz
a	a	k8xC	a
odražení	odražení	k1gNnSc2	odražení
anglické	anglický	k2eAgFnSc2d1	anglická
kavalerie	kavalerie	k1gFnSc2	kavalerie
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
krátce	krátce	k6eAd1	krátce
konsolidovaly	konsolidovat	k5eAaBmAgInP	konsolidovat
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
posílil	posílit	k5eAaPmAgInS	posílit
první	první	k4xOgInSc4	první
linie	linie	k1gFnPc1	linie
jednotkami	jednotka	k1gFnPc7	jednotka
druhého	druhý	k4xOgInSc2	druhý
sledu	sled	k1gInSc2	sled
a	a	k8xC	a
k	k	k7c3	k
Hougoumontu	Hougoumont	k1gMnSc3	Hougoumont
vyslal	vyslat	k5eAaPmAgMnS	vyslat
prapor	prapor	k1gInSc4	prapor
královské	královský	k2eAgFnSc2d1	královská
německé	německý	k2eAgFnSc2d1	německá
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
plamenech	plamen	k1gInPc6	plamen
a	a	k8xC	a
soustavně	soustavně	k6eAd1	soustavně
ostřelován	ostřelovat	k5eAaImNgInS	ostřelovat
z	z	k7c2	z
osmi	osm	k4xCc2	osm
francouzských	francouzský	k2eAgFnPc2d1	francouzská
houfnic	houfnice	k1gFnPc2	houfnice
<g/>
.	.	kIx.	.
<g/>
Kolem	kolem	k7c2	kolem
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
obdržel	obdržet	k5eAaPmAgMnS	obdržet
maršál	maršál	k1gMnSc1	maršál
Ney	Ney	k1gMnSc3	Ney
rozkaz	rozkaz	k1gInSc4	rozkaz
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
ovládnout	ovládnout	k5eAaPmF	ovládnout
statek	statek	k1gInSc4	statek
La	la	k0	la
Haye-Sainte	Haye-Saint	k1gMnSc5	Haye-Saint
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
stále	stále	k6eAd1	stále
marně	marně	k6eAd1	marně
dobýt	dobýt	k5eAaPmF	dobýt
jeho	jeho	k3xOp3gFnSc1	jeho
řadová	řadový	k2eAgFnSc1d1	řadová
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
zde	zde	k6eAd1	zde
probíhal	probíhat	k5eAaImAgInS	probíhat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
Hougoumontu	Hougoumont	k1gInSc2	Hougoumont
<g/>
.	.	kIx.	.
500	[number]	k4	500
zbývajících	zbývající	k2eAgMnPc2d1	zbývající
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
vojáků	voják	k1gMnPc2	voják
vázalo	vázat	k5eAaImAgNnS	vázat
4000	[number]	k4	4000
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
děla	dělo	k1gNnPc1	dělo
navíc	navíc	k6eAd1	navíc
zapadla	zapadnout	k5eAaPmAgNnP	zapadnout
do	do	k7c2	do
mokřadů	mokřad	k1gInPc2	mokřad
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
a	a	k8xC	a
obléhající	obléhající	k2eAgFnPc1d1	obléhající
jednotky	jednotka	k1gFnPc1	jednotka
neměly	mít	k5eNaImAgFnP	mít
ani	ani	k8xC	ani
výbušniny	výbušnina	k1gFnPc1	výbušnina
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
by	by	kYmCp3nS	by
vyhodily	vyhodit	k5eAaPmAgInP	vyhodit
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
dalšímu	další	k2eAgInSc3d1	další
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
La	la	k1gNnSc4	la
Haye-Sainte	Haye-Saint	k1gInSc5	Haye-Saint
velel	velet	k5eAaImAgMnS	velet
Ney	Ney	k1gFnPc2	Ney
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
muži	muž	k1gMnPc1	muž
však	však	k9	však
byli	být	k5eAaImAgMnP	být
opět	opět	k6eAd1	opět
odraženi	odrazit	k5eAaPmNgMnP	odrazit
a	a	k8xC	a
přinuceni	přinutit	k5eAaPmNgMnP	přinutit
stáhnout	stáhnout	k5eAaPmF	stáhnout
se	se	k3xPyFc4	se
z	z	k7c2	z
dostřelu	dostřel	k1gInSc2	dostřel
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
za	za	k7c7	za
Neyovými	Neyův	k2eAgFnPc7d1	Neyův
jednotkami	jednotka	k1gFnPc7	jednotka
začalo	začít	k5eAaPmAgNnS	začít
šikovat	šikovat	k5eAaImF	šikovat
jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
zásadních	zásadní	k2eAgFnPc2d1	zásadní
událostí	událost	k1gFnPc2	událost
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
vedou	vést	k5eAaImIp3nP	vést
diskuse	diskuse	k1gFnPc1	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
již	již	k9	již
asi	asi	k9	asi
nelze	lze	k6eNd1	lze
naprosto	naprosto	k6eAd1	naprosto
přesně	přesně	k6eAd1	přesně
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
vedlo	vést	k5eAaImAgNnS	vést
maršála	maršál	k1gMnSc4	maršál
Neye	Ney	k1gMnSc4	Ney
k	k	k7c3	k
rozkazům	rozkaz	k1gInPc3	rozkaz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
vydal	vydat	k5eAaPmAgInS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
částečný	částečný	k2eAgInSc1d1	částečný
ústup	ústup	k1gInSc1	ústup
spojenců	spojenec	k1gMnPc2	spojenec
na	na	k7c6	na
hřebenu	hřeben	k1gInSc6	hřeben
svahu	svah	k1gInSc2	svah
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
dozadu	dozadu	k6eAd1	dozadu
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
mohl	moct	k5eAaImAgMnS	moct
vzbudit	vzbudit	k5eAaPmF	vzbudit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
anglo-batavská	angloatavský	k2eAgFnSc1d1	anglo-batavský
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
dává	dávat	k5eAaImIp3nS	dávat
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
a	a	k8xC	a
dozrál	dozrát	k5eAaPmAgMnS	dozrát
čas	čas	k1gInSc4	čas
pro	pro	k7c4	pro
útok	útok	k1gInSc4	útok
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
odpor	odpor	k1gInSc4	odpor
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Haye-Sainte	Haye-Saint	k1gMnSc5	Haye-Saint
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
do	do	k7c2	do
boje	boj	k1gInSc2	boj
povolat	povolat	k5eAaPmF	povolat
pouze	pouze	k6eAd1	pouze
kyrysnickou	kyrysnický	k2eAgFnSc4d1	kyrysnický
brigádu	brigáda	k1gFnSc4	brigáda
generála	generál	k1gMnSc2	generál
Farina	Farin	k1gMnSc2	Farin
de	de	k?	de
Creuz	Creuz	k1gInSc1	Creuz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
nadřízený	nadřízený	k1gMnSc1	nadřízený
<g/>
,	,	kIx,	,
velitel	velitel	k1gMnSc1	velitel
kyrysnické	kyrysnický	k2eAgFnSc2d1	kyrysnický
divize	divize	k1gFnSc2	divize
Dolort	Dolort	k1gInSc4	Dolort
<g/>
,	,	kIx,	,
útočit	útočit	k5eAaImF	útočit
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
ještě	ještě	k9	ještě
neměl	mít	k5eNaImAgMnS	mít
tušení	tušení	k1gNnSc4	tušení
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
nedávného	dávný	k2eNgNnSc2d1	nedávné
Napoleonova	Napoleonův	k2eAgNnSc2d1	Napoleonovo
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
má	mít	k5eAaImIp3nS	mít
maršál	maršál	k1gMnSc1	maršál
Ney	Ney	k1gMnSc1	Ney
nad	nad	k7c7	nad
divizí	divize	k1gFnSc7	divize
přímé	přímý	k2eAgNnSc4d1	přímé
velení	velení	k1gNnSc4	velení
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
nejasnost	nejasnost	k1gFnSc1	nejasnost
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
rozkázal	rozkázat	k5eAaPmAgMnS	rozkázat
rozzuřený	rozzuřený	k2eAgMnSc1d1	rozzuřený
Ney	Ney	k1gMnSc1	Ney
Dolortovi	Dolort	k1gMnSc3	Dolort
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
rozkaz	rozkaz	k1gInSc1	rozkaz
přijel	přijet	k5eAaPmAgInS	přijet
tlumočit	tlumočit	k5eAaImF	tlumočit
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Dochovala	dochovat	k5eAaPmAgNnP	dochovat
se	se	k3xPyFc4	se
protichůdná	protichůdný	k2eAgNnPc1d1	protichůdné
svědectví	svědectví	k1gNnPc1	svědectví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
se	se	k3xPyFc4	se
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
shodnout	shodnout	k5eAaBmF	shodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
velel	velet	k5eAaImAgInS	velet
aby	aby	kYmCp3nS	aby
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
celá	celý	k2eAgFnSc1d1	celá
jeho	jeho	k3xOp3gFnSc1	jeho
divize	divize	k1gFnSc1	divize
nebo	nebo	k8xC	nebo
celý	celý	k2eAgInSc1d1	celý
Milhaudův	Milhaudův	k2eAgInSc1d1	Milhaudův
sbor	sbor	k1gInSc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Faktem	fakt	k1gInSc7	fakt
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokořený	pokořený	k2eAgMnSc1d1	pokořený
generál	generál	k1gMnSc1	generál
Dolort	Dolort	k1gInSc4	Dolort
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
s	s	k7c7	s
kompletní	kompletní	k2eAgFnSc7d1	kompletní
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
kyrysnickou	kyrysnický	k2eAgFnSc7d1	kyrysnický
divizí	divize	k1gFnSc7	divize
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Milhaud	Milhaud	k1gMnSc1	Milhaud
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgInS	mít
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
sboru	sbor	k1gInSc2	sbor
tvořit	tvořit	k5eAaImF	tvořit
druhý	druhý	k4xOgInSc4	druhý
sled	sled	k1gInSc4	sled
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
prý	prý	k9	prý
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
svých	svůj	k3xOyFgFnPc2	svůj
jednotek	jednotka	k1gFnPc2	jednotka
vykřikl	vykřiknout	k5eAaPmAgInS	vykřiknout
na	na	k7c4	na
velitele	velitel	k1gMnSc4	velitel
lehkého	lehký	k2eAgNnSc2d1	lehké
gardového	gardový	k2eAgNnSc2d1	gardové
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
generála	generál	k1gMnSc2	generál
Lefebvra-Desnouettese	Lefebvra-Desnouettese	k1gFnSc2	Lefebvra-Desnouettese
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jdu	jít	k5eAaImIp1nS	jít
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
<g/>
!	!	kIx.	!
</s>
<s>
Podpoř	podpořit	k5eAaPmRp2nS	podpořit
mě	já	k3xPp1nSc2	já
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
A	a	k9	a
generál	generál	k1gMnSc1	generál
Lefebvr-Desnouettes	Lefebvr-Desnouettes	k1gMnSc1	Lefebvr-Desnouettes
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
mohl	moct	k5eAaImAgMnS	moct
přijímat	přijímat	k5eAaImF	přijímat
rozkazy	rozkaz	k1gInPc4	rozkaz
pouze	pouze	k6eAd1	pouze
od	od	k7c2	od
Napoleona	Napoleon	k1gMnSc2	Napoleon
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
Milhauda	Milhauda	k1gMnSc1	Milhauda
spontánně	spontánně	k6eAd1	spontánně
následoval	následovat	k5eAaImAgMnS	následovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
šestnácté	šestnáctý	k4xOgFnSc6	šestnáctý
hodině	hodina	k1gFnSc6	hodina
<g/>
,	,	kIx,	,
když	když	k8xS	když
generál	generál	k1gMnSc1	generál
Milhaud	Milhaud	k1gMnSc1	Milhaud
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
5000	[number]	k4	5000
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
roztažených	roztažený	k2eAgFnPc2d1	roztažená
takřka	takřka	k6eAd1	takřka
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
šíři	šíř	k1gFnSc6	šíř
bojiště	bojiště	k1gNnSc2	bojiště
od	od	k7c2	od
Hougoumontu	Hougoumont	k1gInSc2	Hougoumont
až	až	k9	až
po	po	k7c6	po
Haye-Sainte	Haye-Saint	k1gInSc5	Haye-Saint
(	(	kIx(	(
<g/>
bezmála	bezmála	k6eAd1	bezmála
750	[number]	k4	750
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
proti	proti	k7c3	proti
nepřátelské	přátelský	k2eNgFnSc3d1	nepřátelská
pěchotě	pěchota	k1gFnSc3	pěchota
za	za	k7c7	za
Ohalinskou	Ohalinský	k2eAgFnSc7d1	Ohalinský
silnicí	silnice	k1gFnSc7	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
se	se	k3xPyFc4	se
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
železnou	železný	k2eAgFnSc4d1	železná
lavinu	lavina	k1gFnSc4	lavina
šachovnicově	šachovnicově	k6eAd1	šachovnicově
semkli	semknout	k5eAaPmAgMnP	semknout
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
karé	karé	k1gNnPc2	karé
a	a	k8xC	a
vyčkávali	vyčkávat	k5eAaImAgMnP	vyčkávat
<g/>
.	.	kIx.	.
</s>
<s>
Dělostřelci	dělostřelec	k1gMnPc1	dělostřelec
stříleli	střílet	k5eAaImAgMnP	střílet
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
způsobili	způsobit	k5eAaPmAgMnP	způsobit
Francouzům	Francouz	k1gMnPc3	Francouz
značné	značný	k2eAgFnPc4d1	značná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
také	také	k9	také
ukryli	ukrýt	k5eAaPmAgMnP	ukrýt
uvnitř	uvnitř	k7c2	uvnitř
pěchotních	pěchotní	k2eAgInPc2d1	pěchotní
čtverců	čtverec	k1gInPc2	čtverec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
útok	útok	k1gInSc4	útok
se	se	k3xPyFc4	se
Francouzům	Francouz	k1gMnPc3	Francouz
příliš	příliš	k6eAd1	příliš
nevyvedl	vyvést	k5eNaPmAgMnS	vyvést
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
anglická	anglický	k2eAgNnPc4d1	anglické
děla	dělo	k1gNnPc4	dělo
jejich	jejich	k3xOp3gFnSc6	jejich
formace	formace	k1gFnPc1	formace
přijely	přijet	k5eAaPmAgFnP	přijet
značně	značně	k6eAd1	značně
narušené	narušený	k2eAgFnPc1d1	narušená
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zde	zde	k6eAd1	zde
svou	svůj	k3xOyFgFnSc4	svůj
úlohu	úloha	k1gFnSc4	úloha
sehrál	sehrát	k5eAaPmAgInS	sehrát
rozmočený	rozmočený	k2eAgInSc1d1	rozmočený
terén	terén	k1gInSc1	terén
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgNnSc1d1	vysoké
obilí	obilí	k1gNnSc1	obilí
a	a	k8xC	a
palba	palba	k1gFnSc1	palba
z	z	k7c2	z
anglických	anglický	k2eAgNnPc2d1	anglické
karé	karé	k1gNnPc2	karé
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
dopadala	dopadat	k5eAaImAgFnS	dopadat
z	z	k7c2	z
několika	několik	k4yIc3	několik
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
na	na	k7c4	na
čtverhrany	čtverhrana	k1gFnPc4	čtverhrana
ježící	ježící	k2eAgFnPc4d1	ježící
se	se	k3xPyFc4	se
bodáky	bodák	k1gInPc1	bodák
zaútočily	zaútočit	k5eAaPmAgInP	zaútočit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
boji	boj	k1gInSc6	boj
však	však	k9	však
byly	být	k5eAaImAgFnP	být
Milhaudovy	Milhaudův	k2eAgFnPc1d1	Milhaudův
jednotky	jednotka	k1gFnPc1	jednotka
napadeny	napaden	k2eAgFnPc1d1	napadena
zálohou	záloha	k1gFnSc7	záloha
anglického	anglický	k2eAgNnSc2d1	anglické
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
a	a	k8xC	a
maršál	maršál	k1gMnSc1	maršál
Ney	Ney	k1gMnSc1	Ney
nechal	nechat	k5eAaPmAgMnS	nechat
své	svůj	k3xOyFgFnPc4	svůj
jednotky	jednotka	k1gFnPc4	jednotka
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
znova	znova	k6eAd1	znova
shromáždit	shromáždit	k5eAaPmF	shromáždit
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
jezdci	jezdec	k1gMnPc1	jezdec
seskupili	seskupit	k5eAaPmAgMnP	seskupit
mezi	mezi	k7c7	mezi
Hougoumontem	Hougoumont	k1gInSc7	Hougoumont
a	a	k8xC	a
Haye-Sainte	Haye-Saint	k1gInSc5	Haye-Saint
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
nový	nový	k2eAgInSc1d1	nový
povel	povel	k1gInSc1	povel
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc1	jeho
muži	muž	k1gMnPc1	muž
neustále	neustále	k6eAd1	neustále
decimováni	decimován	k2eAgMnPc1d1	decimován
děly	dělo	k1gNnPc7	dělo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
po	po	k7c4	po
jejich	jejich	k3xOp3gNnSc4	jejich
dobytí	dobytí	k1gNnSc4	dobytí
neodtáhli	odtáhnout	k5eNaPmAgMnP	odtáhnout
ani	ani	k8xC	ani
nezatloukli	zatlouct	k5eNaPmAgMnP	zatlouct
hřeby	hřeb	k1gInPc4	hřeb
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
zbrklého	zbrklý	k2eAgInSc2d1	zbrklý
útoku	útok	k1gInSc2	útok
všiml	všimnout	k5eAaPmAgMnS	všimnout
až	až	k6eAd1	až
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
pozdě	pozdě	k6eAd1	pozdě
ho	on	k3xPp3gInSc4	on
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
Neyovu	Neyův	k2eAgFnSc4d1	Neyův
iniciativu	iniciativa	k1gFnSc4	iniciativa
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ihned	ihned	k6eAd1	ihned
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
úspěch	úspěch	k1gInSc4	úspěch
akce	akce	k1gFnSc2	akce
bude	být	k5eAaImBp3nS	být
nutno	nutno	k6eAd1	nutno
kavalerii	kavalerie	k1gFnSc4	kavalerie
podpořit	podpořit	k5eAaPmF	podpořit
a	a	k8xC	a
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
mu	on	k3xPp3gMnSc3	on
vyslal	vyslat	k5eAaPmAgMnS	vyslat
3	[number]	k4	3
<g/>
.	.	kIx.	.
jezdecký	jezdecký	k2eAgInSc1d1	jezdecký
sbor	sbor	k1gInSc1	sbor
generála	generál	k1gMnSc2	generál
Kellermana	Kellerman	k1gMnSc2	Kellerman
<g/>
.	.	kIx.	.
</s>
<s>
Blížilo	blížit	k5eAaImAgNnS	blížit
se	se	k3xPyFc4	se
k	k	k7c3	k
páté	pátý	k4xOgFnSc3	pátý
hodině	hodina	k1gFnSc3	hodina
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
,	,	kIx,	,
na	na	k7c4	na
Angličany	Angličan	k1gMnPc4	Angličan
se	se	k3xPyFc4	se
řítilo	řítit	k5eAaImAgNnS	řítit
9000	[number]	k4	9000
jezdců	jezdec	k1gInPc2	jezdec
všech	všecek	k3xTgFnPc2	všecek
složek	složka	k1gFnPc2	složka
kavalerie	kavalerie	k1gFnSc2	kavalerie
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
druhého	druhý	k4xOgInSc2	druhý
útoku	útok	k1gInSc2	útok
byl	být	k5eAaImAgMnS	být
mnohem	mnohem	k6eAd1	mnohem
úspěšnější	úspěšný	k2eAgMnSc1d2	úspěšnější
než	než	k8xS	než
ten	ten	k3xDgMnSc1	ten
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
zmasakrovali	zmasakrovat	k5eAaPmAgMnP	zmasakrovat
tři	tři	k4xCgInPc4	tři
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
karé	karé	k1gNnPc2	karé
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
další	další	k2eAgFnSc1d1	další
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
své	svůj	k3xOyFgInPc4	svůj
prapory	prapor	k1gInPc4	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
však	však	k9	však
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
maxima	maximum	k1gNnPc4	maximum
celého	celý	k2eAgInSc2d1	celý
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInPc1	jejich
další	další	k2eAgInPc1d1	další
nájezdy	nájezd	k1gInPc1	nájezd
se	se	k3xPyFc4	se
o	o	k7c4	o
zbylé	zbylý	k2eAgInPc4d1	zbylý
pevně	pevně	k6eAd1	pevně
stojící	stojící	k2eAgInPc4d1	stojící
pěchotní	pěchotní	k2eAgInPc4d1	pěchotní
čtverce	čtverec	k1gInPc4	čtverec
pouze	pouze	k6eAd1	pouze
rozbíjely	rozbíjet	k5eAaImAgInP	rozbíjet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bezvýsledných	bezvýsledný	k2eAgInPc6d1	bezvýsledný
pokusech	pokus	k1gInPc6	pokus
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
o	o	k7c4	o
půl	půl	k1xP	půl
šesté	šestý	k4xOgFnSc2	šestý
dán	dát	k5eAaPmNgInS	dát
povel	povel	k1gInSc1	povel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
unavené	unavený	k2eAgNnSc1d1	unavené
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
<g/>
%	%	kIx~	%
ztrátami	ztráta	k1gFnPc7	ztráta
oslabené	oslabený	k2eAgFnSc2d1	oslabená
<g/>
,	,	kIx,	,
brigády	brigáda	k1gFnSc2	brigáda
a	a	k8xC	a
jízdní	jízdní	k2eAgFnSc2d1	jízdní
divize	divize	k1gFnSc2	divize
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
výchozích	výchozí	k2eAgNnPc2d1	výchozí
postavení	postavení	k1gNnPc2	postavení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pruský	pruský	k2eAgInSc1d1	pruský
nástup	nástup	k1gInSc1	nástup
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
střety	střet	k1gInPc1	střet
francouzské	francouzský	k2eAgFnSc2d1	francouzská
jízdy	jízda	k1gFnSc2	jízda
s	s	k7c7	s
předsunutým	předsunutý	k2eAgInSc7d1	předsunutý
Bülovovým	Bülovův	k2eAgInSc7d1	Bülovův
IV	IV	kA	IV
<g/>
.	.	kIx.	.
sborem	sborem	k6eAd1	sborem
propukly	propuknout	k5eAaPmAgFnP	propuknout
kolem	kolem	k7c2	kolem
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
hodiny	hodina	k1gFnPc4	hodina
odpolední	odpolední	k2eAgFnPc4d1	odpolední
u	u	k7c2	u
lesa	les	k1gInSc2	les
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
rozkládal	rozkládat	k5eAaImAgMnS	rozkládat
před	před	k7c7	před
vesnicí	vesnice	k1gFnSc7	vesnice
Plancenoit	Plancenoita	k1gFnPc2	Plancenoita
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
půl	půl	k1xP	půl
páté	pátá	k1gFnSc2	pátá
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
lesa	les	k1gInSc2	les
i	i	k8xC	i
maršál	maršál	k1gMnSc1	maršál
Blücher	Blüchra	k1gFnPc2	Blüchra
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neváhal	váhat	k5eNaImAgMnS	váhat
s	s	k7c7	s
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
nasadit	nasadit	k5eAaPmF	nasadit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
dvě	dva	k4xCgFnPc4	dva
Bülovovy	Bülovův	k2eAgFnPc4d1	Bülovův
brigády	brigáda	k1gFnPc4	brigáda
a	a	k8xC	a
jezdectvo	jezdectvo	k1gNnSc4	jezdectvo
prince	princ	k1gMnSc2	princ
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Pruského	pruský	k2eAgMnSc2d1	pruský
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
páté	pátý	k4xOgFnSc2	pátý
hodiny	hodina	k1gFnSc2	hodina
Prusové	Prus	k1gMnPc1	Prus
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c4	na
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
dvěma	dva	k4xCgNnPc7	dva
tisíci	tisíc	k4xCgInPc7	tisíc
Francouzů	Francouz	k1gMnPc2	Francouz
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
před	před	k7c7	před
vesnicí	vesnice	k1gFnSc7	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
byli	být	k5eAaImAgMnP	být
muži	muž	k1gMnPc1	muž
Moutona	Moutona	k1gFnSc1	Moutona
de	de	k?	de
Lobau	Lobaus	k1gInSc2	Lobaus
v	v	k7c6	v
početní	početní	k2eAgFnSc6d1	početní
nevýhodě	nevýhoda	k1gFnSc6	nevýhoda
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
nepřátelský	přátelský	k2eNgInSc4d1	nepřátelský
nápor	nápor	k1gInSc4	nápor
odrazili	odrazit	k5eAaPmAgMnP	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc4d1	další
útok	útok	k1gInSc4	útok
již	již	k6eAd1	již
vedlo	vést	k5eAaImAgNnS	vést
6000	[number]	k4	6000
Prusů	Prus	k1gMnPc2	Prus
s	s	k7c7	s
24	[number]	k4	24
děly	dělo	k1gNnPc7	dělo
a	a	k8xC	a
tentokrát	tentokrát	k6eAd1	tentokrát
byli	být	k5eAaImAgMnP	být
Francouzi	Francouz	k1gMnPc1	Francouz
nuceni	nutit	k5eAaImNgMnP	nutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
až	až	k6eAd1	až
za	za	k7c4	za
okraj	okraj	k1gInSc4	okraj
Plancenoit	Plancenoit	k2eAgInSc4d1	Plancenoit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
vedlo	vést	k5eAaImAgNnS	vést
i	i	k8xC	i
zbylým	zbylý	k2eAgFnPc3d1	zbylá
jednotkám	jednotka	k1gFnPc3	jednotka
VI	VI	kA	VI
<g/>
.	.	kIx.	.
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
držely	držet	k5eAaImAgFnP	držet
pozice	pozice	k1gFnPc1	pozice
severně	severně	k6eAd1	severně
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
tvořily	tvořit	k5eAaImAgFnP	tvořit
spojnici	spojnice	k1gFnSc4	spojnice
k	k	k7c3	k
I.	I.	kA	I.
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonovu	Erlonův	k2eAgInSc3d1	Erlonův
sboru	sbor	k1gInSc3	sbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
7000	[number]	k4	7000
Lobauových	Lobauův	k2eAgMnPc2d1	Lobauův
pěšáků	pěšák	k1gMnPc2	pěšák
s	s	k7c7	s
24	[number]	k4	24
děly	dělo	k1gNnPc7	dělo
čelilo	čelit	k5eAaImAgNnS	čelit
18	[number]	k4	18
tisícům	tisíc	k4xCgInPc3	tisíc
mužům	muž	k1gMnPc3	muž
a	a	k8xC	a
64	[number]	k4	64
dělům	dělo	k1gNnPc3	dělo
generála	generál	k1gMnSc4	generál
Bülova	Bülův	k2eAgFnSc1d1	Bülův
<g/>
.	.	kIx.	.
</s>
<s>
Prusové	Prus	k1gMnPc1	Prus
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
stáčeli	stáčet	k5eAaImAgMnP	stáčet
k	k	k7c3	k
říčce	říčka	k1gFnSc3	říčka
Lasne	Lasn	k1gInSc5	Lasn
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
Francouzům	Francouz	k1gMnPc3	Francouz
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
a	a	k8xC	a
přetnout	přetnout	k5eAaPmF	přetnout
jejich	jejich	k3xOp3gFnPc4	jejich
ústupové	ústupový	k2eAgFnPc4d1	ústupová
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
na	na	k7c4	na
dostřel	dostřel	k1gInSc4	dostřel
jejich	jejich	k3xOp3gNnPc2	jejich
děl	dělo	k1gNnPc2	dělo
dostaly	dostat	k5eAaPmAgFnP	dostat
i	i	k8xC	i
pozice	pozice	k1gFnSc1	pozice
dosud	dosud	k6eAd1	dosud
nenasazené	nasazený	k2eNgFnSc2d1	nenasazená
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
farma	farma	k1gFnSc1	farma
Rossome	Rossom	k1gInSc5	Rossom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zřídil	zřídit	k5eAaPmAgMnS	zřídit
pozorovatelnu	pozorovatelna	k1gFnSc4	pozorovatelna
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
neváhal	váhat	k5eNaImAgMnS	váhat
proti	proti	k7c3	proti
dobytému	dobytý	k2eAgInSc3d1	dobytý
Plancenoit	Plancenoit	k2eAgMnSc1d1	Plancenoit
vyslat	vyslat	k5eAaPmF	vyslat
divizi	divize	k1gFnSc4	divize
mladé	mladý	k2eAgFnSc2d1	mladá
gardy	garda	k1gFnSc2	garda
(	(	kIx(	(
<g/>
3800	[number]	k4	3800
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vesnici	vesnice	k1gFnSc4	vesnice
i	i	k9	i
přilehlý	přilehlý	k2eAgInSc4d1	přilehlý
hřbitov	hřbitov	k1gInSc4	hřbitov
zanedlouho	zanedlouho	k6eAd1	zanedlouho
získala	získat	k5eAaPmAgFnS	získat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
neúspěch	neúspěch	k1gInSc4	neúspěch
reagoval	reagovat	k5eAaBmAgMnS	reagovat
Blücher	Blüchra	k1gFnPc2	Blüchra
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
vyslal	vyslat	k5eAaPmAgMnS	vyslat
dalších	další	k2eAgMnPc2d1	další
9000	[number]	k4	9000
mužů	muž	k1gMnPc2	muž
landwehru	landwehra	k1gFnSc4	landwehra
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
garda	garda	k1gFnSc1	garda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
převzala	převzít	k5eAaPmAgFnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
vesnicí	vesnice	k1gFnSc7	vesnice
–	–	k?	–
Lobauovy	Lobauův	k2eAgFnSc2d1	Lobauův
jednotky	jednotka	k1gFnSc2	jednotka
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
staraly	starat	k5eAaImAgFnP	starat
o	o	k7c4	o
ochranu	ochrana	k1gFnSc4	ochrana
jejího	její	k3xOp3gNnSc2	její
pravého	pravý	k2eAgNnSc2d1	pravé
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
kryly	krýt	k5eAaImAgFnP	krýt
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
Prusové	Prus	k1gMnPc1	Prus
o	o	k7c4	o
obchvat	obchvat	k1gInSc4	obchvat
–	–	k?	–
své	svůj	k3xOyFgFnSc6	svůj
pozice	pozice	k1gFnSc1	pozice
tvrdě	tvrdě	k6eAd1	tvrdě
bránila	bránit	k5eAaImAgFnS	bránit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
těžce	těžce	k6eAd1	těžce
zraněn	zranit	k5eAaPmNgMnS	zranit
její	její	k3xOp3gMnSc1	její
velitel	velitel	k1gMnSc1	velitel
generál	generál	k1gMnSc1	generál
Duhesme	Duhesme	k1gMnSc1	Duhesme
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
musel	muset	k5eAaImAgMnS	muset
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
podpoře	podpora	k1gFnSc3	podpora
vyslal	vyslat	k5eAaPmAgMnS	vyslat
dva	dva	k4xCgInPc4	dva
prapory	prapor	k1gInPc4	prapor
staré	starý	k2eAgFnSc2d1	stará
gardy	garda	k1gFnSc2	garda
(	(	kIx(	(
<g/>
1100	[number]	k4	1100
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
útoků	útok	k1gInPc2	útok
gardových	gardový	k2eAgMnPc2d1	gardový
myslivců	myslivec	k1gMnPc2	myslivec
se	se	k3xPyFc4	se
do	do	k7c2	do
boje	boj	k1gInSc2	boj
zapojili	zapojit	k5eAaPmAgMnP	zapojit
gardoví	gardový	k2eAgMnPc1d1	gardový
granátníci	granátník	k1gMnPc1	granátník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
14	[number]	k4	14
pruských	pruský	k2eAgInPc2d1	pruský
praporů	prapor	k1gInPc2	prapor
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
minutách	minuta	k1gFnPc6	minuta
z	z	k7c2	z
Plancenoit	Plancenoita	k1gFnPc2	Plancenoita
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Odvetou	odveta	k1gFnSc7	odveta
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
27	[number]	k4	27
praporů	prapor	k1gInPc2	prapor
pruské	pruský	k2eAgFnSc2d1	pruská
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
stará	starý	k2eAgFnSc1d1	stará
garda	garda	k1gFnSc1	garda
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
na	na	k7c4	na
západní	západní	k2eAgInSc4d1	západní
okraj	okraj	k1gInSc4	okraj
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
mladí	mladý	k2eAgMnPc1d1	mladý
a	a	k8xC	a
staří	starý	k2eAgMnPc1d1	starý
gardisté	gardista	k1gMnPc1	gardista
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
maximálně	maximálně	k6eAd1	maximálně
3000	[number]	k4	3000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
vzdorovali	vzdorovat	k5eAaImAgMnP	vzdorovat
další	další	k2eAgFnPc4d1	další
tři	tři	k4xCgFnPc4	tři
hodiny	hodina	k1gFnPc4	hodina
bezmála	bezmála	k6eAd1	bezmála
20	[number]	k4	20
tisícové	tisícový	k2eAgFnSc6d1	tisícová
přesile	přesila	k1gFnSc6	přesila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
již	již	k6eAd1	již
začaly	začít	k5eAaPmAgFnP	začít
zapojovat	zapojovat	k5eAaImF	zapojovat
první	první	k4xOgFnPc1	první
jednotky	jednotka	k1gFnPc1	jednotka
II	II	kA	II
<g/>
.	.	kIx.	.
pruského	pruský	k2eAgInSc2d1	pruský
armádního	armádní	k2eAgInSc2d1	armádní
sboru	sbor	k1gInSc2	sbor
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generálporučíka	generálporučík	k1gMnSc2	generálporučík
von	von	k1gInSc1	von
Pircha	Pircha	k1gFnSc1	Pircha
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
bojišti	bojiště	k1gNnSc3	bojiště
po	po	k7c6	po
šesté	šestý	k4xOgFnSc6	šestý
hodině	hodina	k1gFnSc6	hodina
přicházel	přicházet	k5eAaImAgMnS	přicházet
i	i	k9	i
I.	I.	kA	I.
pruský	pruský	k2eAgInSc1d1	pruský
armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálporučíkem	generálporučík	k1gMnSc7	generálporučík
von	von	k1gInSc4	von
Zietenem	Zieten	k1gInSc7	Zieten
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
20	[number]	k4	20
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
mělo	mít	k5eAaImAgNnS	mít
podpořit	podpořit	k5eAaPmF	podpořit
Wellingtonovo	Wellingtonův	k2eAgNnSc4d1	Wellingtonovo
levé	levý	k2eAgNnSc4d1	levé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vinou	vina	k1gFnSc7	vina
nedorozumění	nedorozumění	k1gNnSc2	nedorozumění
kvůli	kvůli	k7c3	kvůli
zprávám	zpráva	k1gFnPc3	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Angličané	Angličan	k1gMnPc1	Angličan
ustupují	ustupovat	k5eAaImIp3nP	ustupovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
zapojil	zapojit	k5eAaPmAgMnS	zapojit
až	až	k9	až
po	po	k7c4	po
půl	půl	k1xP	půl
sedmé	sedmý	k4xOgFnSc2	sedmý
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pruská	pruský	k2eAgFnSc1d1	pruská
podpora	podpora	k1gFnSc1	podpora
dovolila	dovolit	k5eAaPmAgFnS	dovolit
anglickému	anglický	k2eAgMnSc3d1	anglický
vrchnímu	vrchní	k2eAgMnSc3d1	vrchní
veliteli	velitel	k1gMnSc3	velitel
odvolat	odvolat	k5eAaPmF	odvolat
muže	muž	k1gMnPc4	muž
z	z	k7c2	z
posilovaných	posilovaný	k2eAgFnPc2d1	posilovaná
pozic	pozice	k1gFnPc2	pozice
a	a	k8xC	a
nasadit	nasadit	k5eAaPmF	nasadit
je	on	k3xPp3gNnSc4	on
na	na	k7c4	na
exponovaný	exponovaný	k2eAgInSc4d1	exponovaný
střed	střed	k1gInSc4	střed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgInSc1	třetí
útok	útok	k1gInSc1	útok
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
střed	střed	k1gInSc4	střed
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgInP	být
odraženy	odražen	k2eAgInPc1d1	odražen
oba	dva	k4xCgInPc1	dva
jezdecké	jezdecký	k2eAgInPc1d1	jezdecký
útoky	útok	k1gInPc1	útok
<g/>
,	,	kIx,	,
nemínil	mínit	k5eNaImAgMnS	mínit
se	se	k3xPyFc4	se
maršál	maršál	k1gMnSc1	maršál
Ney	Ney	k1gMnSc1	Ney
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
neúspěchem	neúspěch	k1gInSc7	neúspěch
smířit	smířit	k5eAaPmF	smířit
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
stupňovat	stupňovat	k5eAaImF	stupňovat
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
střed	střed	k1gInSc4	střed
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
nasadil	nasadit	k5eAaPmAgMnS	nasadit
tři	tři	k4xCgFnPc4	tři
brigády	brigáda	k1gFnPc4	brigáda
Reilleho	Reille	k1gMnSc2	Reille
II	II	kA	II
<g/>
.	.	kIx.	.
sboru	sbor	k1gInSc2	sbor
s	s	k7c7	s
polovinou	polovina	k1gFnSc7	polovina
Foyovy	Foyův	k2eAgFnSc2d1	Foyův
9	[number]	k4	9
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
a	a	k8xC	a
Bacheluovou	Bacheluová	k1gFnSc7	Bacheluová
5	[number]	k4	5
<g/>
.	.	kIx.	.
divizí	divize	k1gFnPc2	divize
(	(	kIx(	(
<g/>
6500	[number]	k4	6500
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
vytrvale	vytrvale	k6eAd1	vytrvale
exponovaný	exponovaný	k2eAgInSc4d1	exponovaný
úsek	úsek	k1gInSc4	úsek
narychlo	narychlo	k6eAd1	narychlo
stáhnul	stáhnout	k5eAaPmAgInS	stáhnout
3	[number]	k4	3
<g/>
.	.	kIx.	.
divizi	divize	k1gFnSc4	divize
generálmajora	generálmajor	k1gMnSc2	generálmajor
Fredericka	Fredericko	k1gNnSc2	Fredericko
Adama	Adam	k1gMnSc2	Adam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
víc	hodně	k6eAd2	hodně
už	už	k6eAd1	už
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
neměl	mít	k5eNaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
byli	být	k5eAaImAgMnP	být
Francouzi	Francouz	k1gMnPc1	Francouz
přivítáni	přivítat	k5eAaPmNgMnP	přivítat
s	s	k7c7	s
chladným	chladný	k2eAgNnSc7d1	chladné
odhodláním	odhodlání	k1gNnSc7	odhodlání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
deseti	deset	k4xCc6	deset
minutách	minuta	k1gFnPc6	minuta
soustředěné	soustředěný	k2eAgFnSc2d1	soustředěná
palby	palba	k1gFnSc2	palba
jich	on	k3xPp3gFnPc2	on
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
ležet	ležet	k5eAaImF	ležet
mrtvých	mrtvý	k1gMnPc2	mrtvý
či	či	k8xC	či
zraněných	zraněný	k1gMnPc2	zraněný
na	na	k7c4	na
1500	[number]	k4	1500
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
obrovské	obrovský	k2eAgFnPc4d1	obrovská
ztráty	ztráta	k1gFnPc4	ztráta
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
proti	proti	k7c3	proti
anglo-batavským	angloatavský	k2eAgFnPc3d1	anglo-batavský
formacím	formace	k1gFnPc3	formace
na	na	k7c4	na
bodák	bodák	k1gInSc4	bodák
<g/>
.	.	kIx.	.
</s>
<s>
Prorazit	prorazit	k5eAaPmF	prorazit
však	však	k9	však
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
neúspěchu	neúspěch	k1gInSc6	neúspěch
Napoleon	Napoleon	k1gMnSc1	Napoleon
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Neyovi	Neya	k1gMnSc3	Neya
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vedl	vést	k5eAaImAgInS	vést
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
část	část	k1gFnSc1	část
I.	I.	kA	I.
ďErlonova	ďErlonův	k2eAgInSc2d1	ďErlonův
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
dobyl	dobýt	k5eAaPmAgInS	dobýt
farmu	farma	k1gFnSc4	farma
La	la	k1gNnSc2	la
Haye-Sainte	Haye-Saint	k1gInSc5	Haye-Saint
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
asi	asi	k9	asi
s	s	k7c7	s
tisícovkou	tisícovka	k1gFnSc7	tisícovka
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kombinovaného	kombinovaný	k2eAgInSc2d1	kombinovaný
útoku	útok	k1gInSc2	útok
jízdy	jízda	k1gFnSc2	jízda
a	a	k8xC	a
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
úkol	úkol	k1gInSc1	úkol
konečně	konečně	k6eAd1	konečně
splnil	splnit	k5eAaPmAgInS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
tisíce	tisíc	k4xCgInSc2	tisíc
obránců	obránce	k1gMnPc2	obránce
jich	on	k3xPp3gMnPc2	on
přežilo	přežít	k5eAaPmAgNnS	přežít
45	[number]	k4	45
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
útokem	útok	k1gInSc7	útok
francouzský	francouzský	k2eAgMnSc1d1	francouzský
maršál	maršál	k1gMnSc1	maršál
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
spojence	spojenec	k1gMnSc4	spojenec
z	z	k7c2	z
písečného	písečný	k2eAgInSc2d1	písečný
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
šesti	šest	k4xCc7	šest
děly	dělo	k1gNnPc7	dělo
jízdní	jízdní	k2eAgMnSc1d1	jízdní
baterie	baterie	k1gFnPc4	baterie
zahájil	zahájit	k5eAaPmAgMnS	zahájit
palbu	palba	k1gFnSc4	palba
na	na	k7c4	na
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
měl	mít	k5eAaImAgMnS	mít
Ney	Ney	k1gMnSc1	Ney
La	la	k1gNnSc2	la
Haye-Sainte	Haye-Saint	k1gInSc5	Haye-Saint
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgMnS	nařídit
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonovu	Erlonův	k2eAgFnSc4d1	Erlonův
sboru	sbor	k1gInSc2	sbor
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
anglo-batavský	angloatavský	k2eAgInSc4d1	anglo-batavský
střed	střed	k1gInSc4	střed
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
dařit	dařit	k5eAaImF	dařit
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
šíři	šíř	k1gFnSc6	šíř
a	a	k8xC	a
spojenci	spojenec	k1gMnPc1	spojenec
byli	být	k5eAaImAgMnP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
pomalu	pomalu	k6eAd1	pomalu
ustupovat	ustupovat	k5eAaImF	ustupovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Soyova	Soyův	k2eAgFnSc1d1	Soyův
brigáda	brigáda	k1gFnSc1	brigáda
obešla	obejít	k5eAaPmAgFnS	obejít
Hougoumont	Hougoumont	k1gInSc4	Hougoumont
a	a	k8xC	a
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Pirého	Pirého	k2eAgNnSc2d1	Pirého
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
mířila	mířit	k5eAaImAgFnS	mířit
do	do	k7c2	do
pravého	pravý	k2eAgInSc2d1	pravý
boku	bok	k1gInSc2	bok
lorda	lord	k1gMnSc4	lord
Hilla	Hill	k1gMnSc4	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
půl	půl	k1xP	půl
sedmé	sedmý	k4xOgFnSc2	sedmý
připomínaly	připomínat	k5eAaImAgFnP	připomínat
anglo-batavské	angloatavský	k2eAgFnPc1d1	anglo-batavský
linie	linie	k1gFnPc1	linie
tělo	tělo	k1gNnSc1	tělo
napnutého	napnutý	k2eAgInSc2d1	napnutý
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
středové	středový	k2eAgFnPc1d1	středová
jednotky	jednotka	k1gFnPc1	jednotka
ustoupily	ustoupit	k5eAaPmAgFnP	ustoupit
asi	asi	k9	asi
o	o	k7c4	o
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
odolávali	odolávat	k5eAaImAgMnP	odolávat
Francouzům	Francouz	k1gMnPc3	Francouz
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
si	se	k3xPyFc3	se
Wellington	Wellington	k1gInSc1	Wellington
povzdechnul	povzdechnout	k5eAaPmAgInS	povzdechnout
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kéž	kéž	k9	kéž
přijde	přijít	k5eAaPmIp3nS	přijít
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Blücher	Blüchra	k1gFnPc2	Blüchra
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Neyovi	Neya	k1gMnSc3	Neya
scházel	scházet	k5eAaImAgInS	scházet
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
už	už	k6eAd1	už
jen	jen	k9	jen
kousek	kousek	k1gInSc1	kousek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
dosažení	dosažení	k1gNnSc3	dosažení
požadoval	požadovat	k5eAaImAgInS	požadovat
další	další	k2eAgMnSc1d1	další
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
jen	jen	k9	jen
odseknul	odseknout	k5eAaPmAgMnS	odseknout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
mám	mít	k5eAaImIp1nS	mít
vzít	vzít	k5eAaPmF	vzít
<g/>
?	?	kIx.	?
</s>
<s>
Mám	mít	k5eAaImIp1nS	mít
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
snad	snad	k9	snad
nadělat	nadělat	k5eAaBmF	nadělat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Ve	v	k7c4	v
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
sice	sice	k8xC	sice
Nejstatečnější	statečný	k2eAgFnPc1d3	nejstatečnější
ze	z	k7c2	z
statečných	statečný	k2eAgFnPc2d1	statečná
(	(	kIx(	(
<g/>
Neyova	Neyův	k2eAgFnSc1d1	Neyův
přezdívka	přezdívka	k1gFnSc1	přezdívka
<g/>
)	)	kIx)	)
zmobilizoval	zmobilizovat	k5eAaPmAgMnS	zmobilizovat
několik	několik	k4yIc1	několik
nejméně	málo	k6eAd3	málo
prořídlých	prořídlý	k2eAgFnPc2d1	prořídlá
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
posil	posila	k1gFnPc2	posila
nebyl	být	k5eNaImAgInS	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
dotáhnout	dotáhnout	k5eAaPmF	dotáhnout
útok	útok	k1gInSc1	útok
do	do	k7c2	do
zdárného	zdárný	k2eAgNnSc2d1	zdárné
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
zacelil	zacelit	k5eAaPmAgMnS	zacelit
obranu	obrana	k1gFnSc4	obrana
jednotkami	jednotka	k1gFnPc7	jednotka
mladičkých	mladičký	k2eAgMnPc2d1	mladičký
Brunšvičanů	Brunšvičan	k1gMnPc2	Brunšvičan
a	a	k8xC	a
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
proti	proti	k7c3	proti
Francouzům	Francouz	k1gMnPc3	Francouz
vyrazily	vyrazit	k5eAaPmAgInP	vyrazit
tři	tři	k4xCgInPc1	tři
husarské	husarský	k2eAgInPc1d1	husarský
pluky	pluk	k1gInPc1	pluk
podpořené	podpořený	k2eAgInPc1d1	podpořený
třemi	tři	k4xCgInPc7	tři
nassavskými	nassavský	k2eAgInPc7d1	nassavský
prapory	prapor	k1gInPc7	prapor
<g/>
.	.	kIx.	.
</s>
<s>
D	D	kA	D
<g/>
́	́	k?	́
<g/>
Erlonův	Erlonův	k2eAgInSc4d1	Erlonův
sbor	sbor	k1gInSc4	sbor
byl	být	k5eAaImAgInS	být
přinucen	přinucen	k2eAgMnSc1d1	přinucen
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
ústupu	ústup	k1gInSc3	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
jej	on	k3xPp3gMnSc4	on
spojenci	spojenec	k1gMnPc1	spojenec
nepronásledovali	pronásledovat	k5eNaImAgMnP	pronásledovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Útok	útok	k1gInSc1	útok
staré	starý	k2eAgFnSc2d1	stará
gardy	garda	k1gFnSc2	garda
===	===	k?	===
</s>
</p>
<p>
<s>
Bylo	být	k5eAaImAgNnS	být
něco	něco	k3yInSc1	něco
málo	málo	k6eAd1	málo
před	před	k7c7	před
sedmou	sedmý	k4xOgFnSc7	sedmý
hodinou	hodina	k1gFnSc7	hodina
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
konečně	konečně	k6eAd1	konečně
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
nasadit	nasadit	k5eAaPmF	nasadit
starou	starý	k2eAgFnSc4d1	stará
gardu	garda	k1gFnSc4	garda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
útoku	útok	k1gInSc2	útok
proti	proti	k7c3	proti
spojeneckému	spojenecký	k2eAgInSc3d1	spojenecký
středu	střed	k1gInSc3	střed
však	však	k9	však
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
celá	celý	k2eAgFnSc1d1	celá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
jejích	její	k3xOp3gInPc2	její
6	[number]	k4	6
praporů	prapor	k1gInPc2	prapor
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
maximálně	maximálně	k6eAd1	maximálně
2600	[number]	k4	2600
mužů	muž	k1gMnPc2	muž
pochodujících	pochodující	k2eAgMnPc2d1	pochodující
v	v	k7c6	v
pěti	pět	k4xCc2	pět
kolonách	kolona	k1gFnPc6	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
gardových	gardový	k2eAgFnPc2d1	gardová
jednotek	jednotka	k1gFnPc2	jednotka
rozmístil	rozmístit	k5eAaPmAgMnS	rozmístit
podél	podél	k7c2	podél
bruselské	bruselský	k2eAgFnSc2d1	bruselská
silnice	silnice	k1gFnSc2	silnice
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
kryly	krýt	k5eAaImAgFnP	krýt
jeho	jeho	k3xOp3gFnSc4	jeho
jedinou	jediný	k2eAgFnSc4d1	jediná
ústupovou	ústupový	k2eAgFnSc4d1	ústupová
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zabránily	zabránit	k5eAaPmAgInP	zabránit
Prusům	Prus	k1gMnPc3	Prus
obchvátit	obchvátit	k5eAaPmF	obchvátit
jeho	jeho	k3xOp3gNnSc4	jeho
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
také	také	k9	také
učinil	učinit	k5eAaImAgMnS	učinit
zásadní	zásadní	k2eAgNnSc4d1	zásadní
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
když	když	k8xS	když
nechal	nechat	k5eAaPmAgMnS	nechat
mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgFnPc4d1	ostatní
řadové	řadový	k2eAgFnPc4d1	řadová
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
morálka	morálka	k1gFnSc1	morálka
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
nezdařených	zdařený	k2eNgInPc6d1	nezdařený
útocích	útok	k1gInPc6	útok
na	na	k7c6	na
nevalné	valný	k2eNgFnSc6d1	nevalná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
rozhlásit	rozhlásit	k5eAaPmF	rozhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
masa	masa	k1gFnSc1	masa
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
jízdy	jízda	k1gFnSc2	jízda
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
je	být	k5eAaImIp3nS	být
všemi	všecek	k3xTgFnPc7	všecek
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
Grouchyho	Grouchy	k1gMnSc4	Grouchy
sbor	sbor	k1gInSc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
prohlášení	prohlášení	k1gNnSc4	prohlášení
podpořil	podpořit	k5eAaPmAgInS	podpořit
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
pruské	pruský	k2eAgFnPc1d1	pruská
jednotky	jednotka	k1gFnPc1	jednotka
Zietenova	Zietenův	k2eAgInSc2d1	Zietenův
I.	I.	kA	I.
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
blížili	blížit	k5eAaImAgMnP	blížit
k	k	k7c3	k
farmě	farma	k1gFnSc3	farma
Papelotte	Papelott	k1gInSc5	Papelott
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
přivítány	přivítat	k5eAaPmNgInP	přivítat
palbou	palba	k1gFnSc7	palba
mušket	mušket	k1gInSc4	mušket
nassavské	nassavský	k2eAgFnSc2d1	nassavský
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
útokem	útok	k1gInSc7	útok
jejich	jejich	k3xOp3gNnSc2	jejich
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
jen	jen	k9	jen
díky	díky	k7c3	díky
záměně	záměna	k1gFnSc3	záměna
pruských	pruský	k2eAgFnPc2d1	pruská
čák	čáka	k1gFnPc2	čáka
(	(	kIx(	(
<g/>
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vojenská	vojenský	k2eAgFnSc1d1	vojenská
čepice	čepice	k1gFnSc1	čepice
<g/>
)	)	kIx)	)
s	s	k7c7	s
francouzskými	francouzský	k2eAgFnPc7d1	francouzská
<g/>
.	.	kIx.	.
</s>
<s>
Wellington	Wellington	k1gInSc1	Wellington
neměl	mít	k5eNaImAgInS	mít
kvůli	kvůli	k7c3	kvůli
dýmu	dým	k1gInSc3	dým
nad	nad	k7c7	nad
bojištěm	bojiště	k1gNnSc7	bojiště
o	o	k7c6	o
přípravách	příprava	k1gFnPc6	příprava
protivníka	protivník	k1gMnSc2	protivník
ponětí	ponětí	k1gNnSc4	ponětí
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
asi	asi	k9	asi
půl	půl	k6eAd1	půl
hodinu	hodina	k1gFnSc4	hodina
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
útokem	útok	k1gInSc7	útok
dezertoval	dezertovat	k5eAaBmAgInS	dezertovat
z	z	k7c2	z
francouzských	francouzský	k2eAgFnPc2d1	francouzská
řad	řada	k1gFnPc2	řada
roajalistický	roajalistický	k2eAgMnSc1d1	roajalistický
důstojník	důstojník	k1gMnSc1	důstojník
kapitán	kapitán	k1gMnSc1	kapitán
du	du	k?	du
Barail	Barail	k1gMnSc1	Barail
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ho	on	k3xPp3gMnSc4	on
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
včas	včas	k6eAd1	včas
informoval	informovat	k5eAaBmAgMnS	informovat
a	a	k8xC	a
anglický	anglický	k2eAgMnSc1d1	anglický
velitel	velitel	k1gMnSc1	velitel
stačil	stačit	k5eAaBmAgMnS	stačit
ohrožené	ohrožený	k2eAgNnSc4d1	ohrožené
místo	místo	k1gNnSc4	místo
posílit	posílit	k5eAaPmF	posílit
pěchotou	pěchota	k1gFnSc7	pěchota
i	i	k8xC	i
dělostřelectvem	dělostřelectvo	k1gNnSc7	dělostřelectvo
<g/>
.	.	kIx.	.
<g/>
Francouzskými	francouzský	k2eAgFnPc7d1	francouzská
liniemi	linie	k1gFnPc7	linie
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
nesly	nést	k5eAaImAgFnP	nést
dvě	dva	k4xCgFnPc1	dva
morálku	morálka	k1gFnSc4	morálka
povzbuzující	povzbuzující	k2eAgNnPc4d1	povzbuzující
hesla	heslo	k1gNnPc4	heslo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Grouchy	Grouch	k1gInPc1	Grouch
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nP	blížit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Garda	garda	k1gFnSc1	garda
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
gardy	garda	k1gFnSc2	garda
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
postavil	postavit	k5eAaPmAgMnS	postavit
maršál	maršál	k1gMnSc1	maršál
Ney	Ney	k1gMnSc1	Ney
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
pravé	pravý	k2eAgFnSc6d1	pravá
ruce	ruka	k1gFnSc6	ruka
se	se	k3xPyFc4	se
chystala	chystat	k5eAaImAgFnS	chystat
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonova	Erlonův	k2eAgFnSc1d1	Erlonův
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
levé	levá	k1gFnSc6	levá
pěchota	pěchota	k1gFnSc1	pěchota
Reilleho	Reille	k1gMnSc2	Reille
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
útok	útok	k1gInSc4	útok
se	se	k3xPyFc4	se
chystalo	chystat	k5eAaImAgNnS	chystat
podpořit	podpořit	k5eAaPmF	podpořit
i	i	k9	i
jezdectvo	jezdectvo	k1gNnSc4	jezdectvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
koně	kůň	k1gMnPc1	kůň
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
chvíli	chvíle	k1gFnSc6	chvíle
již	již	k6eAd1	již
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Gardové	gardový	k2eAgFnPc1d1	gardová
jednotky	jednotka	k1gFnPc1	jednotka
pozvolna	pozvolna	k6eAd1	pozvolna
postupovaly	postupovat	k5eAaImAgFnP	postupovat
šikmým	šikmý	k2eAgInSc7d1	šikmý
pochodem	pochod	k1gInSc7	pochod
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
bruselské	bruselský	k2eAgFnSc2d1	bruselská
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
nepřetržitou	přetržitý	k2eNgFnSc4d1	nepřetržitá
palbu	palba	k1gFnSc4	palba
spojeneckých	spojenecký	k2eAgNnPc2d1	spojenecké
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
smetly	smetnout	k5eAaPmAgFnP	smetnout
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
postavilo	postavit	k5eAaPmAgNnS	postavit
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
však	však	k9	však
narazily	narazit	k5eAaPmAgFnP	narazit
na	na	k7c4	na
2800	[number]	k4	2800
pěšáků	pěšák	k1gMnPc2	pěšák
1	[number]	k4	1
<g/>
.	.	kIx.	.
brigády	brigáda	k1gFnPc4	brigáda
nizozemsko-belgické	nizozemskoelgický	k2eAgFnSc2d1	nizozemsko-belgická
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zastavit	zastavit	k5eAaPmF	zastavit
postup	postup	k1gInSc4	postup
gardové	gardový	k2eAgFnSc2d1	gardová
kolony	kolona	k1gFnSc2	kolona
nejblíže	blízce	k6eAd3	blízce
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Kolona	kolona	k1gFnSc1	kolona
útočící	útočící	k2eAgFnSc1d1	útočící
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
byla	být	k5eAaImAgFnS	být
přivítána	přivítat	k5eAaPmNgFnS	přivítat
ničivou	ničivý	k2eAgFnSc7d1	ničivá
střelbou	střelba	k1gFnSc7	střelba
anglických	anglický	k2eAgFnPc2d1	anglická
mušket	mušketa	k1gFnPc2	mušketa
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
nedostali	dostat	k5eNaPmAgMnP	dostat
ani	ani	k8xC	ani
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
bodákovému	bodákový	k2eAgInSc3d1	bodákový
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
kolona	kolona	k1gFnSc1	kolona
<g/>
,	,	kIx,	,
útočící	útočící	k2eAgFnSc1d1	útočící
dále	daleko	k6eAd2	daleko
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
palby	palba	k1gFnSc2	palba
dvou	dva	k4xCgInPc2	dva
tisíc	tisíc	k4xCgInPc2	tisíc
anglických	anglický	k2eAgMnPc2d1	anglický
gardistů	gardista	k1gMnPc2	gardista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
dvacet	dvacet	k4xCc1	dvacet
metrů	metr	k1gInPc2	metr
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
Wellingtonův	Wellingtonův	k2eAgInSc4d1	Wellingtonův
povel	povel	k1gInSc4	povel
zvedli	zvednout	k5eAaPmAgMnP	zvednout
z	z	k7c2	z
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Následným	následný	k2eAgInSc7d1	následný
bodákovým	bodákový	k2eAgInSc7d1	bodákový
protiútokem	protiútok	k1gInSc7	protiútok
pak	pak	k6eAd1	pak
postup	postup	k1gInSc1	postup
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
kolon	kolona	k1gFnPc2	kolona
definitivně	definitivně	k6eAd1	definitivně
zastavili	zastavit	k5eAaPmAgMnP	zastavit
a	a	k8xC	a
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
elita	elita	k1gFnSc1	elita
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
za	za	k7c2	za
neustálého	neustálý	k2eAgInSc2d1	neustálý
boje	boj	k1gInSc2	boj
začít	začít	k5eAaPmF	začít
pomalu	pomalu	k6eAd1	pomalu
stahovat	stahovat	k5eAaImF	stahovat
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
(	(	kIx(	(
<g/>
pátá	pátý	k4xOgFnSc1	pátý
<g/>
)	)	kIx)	)
kolona	kolona	k1gFnSc1	kolona
myslivců	myslivec	k1gMnPc2	myslivec
střední	střední	k2eAgFnSc2d1	střední
gardy	garda	k1gFnSc2	garda
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
druhům	druh	k1gMnPc3	druh
ve	v	k7c6	v
zbrani	zbraň	k1gFnSc6	zbraň
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
zahnala	zahnat	k5eAaPmAgFnS	zahnat
britské	britský	k2eAgMnPc4d1	britský
gardisty	gardista	k1gMnPc4	gardista
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
do	do	k7c2	do
levého	levý	k2eAgInSc2d1	levý
boku	bok	k1gInSc2	bok
vpadly	vpadnout	k5eAaPmAgFnP	vpadnout
britské	britský	k2eAgFnPc1d1	britská
jednotky	jednotka	k1gFnPc1	jednotka
Adamovy	Adamův	k2eAgFnSc2d1	Adamova
brigády	brigáda	k1gFnSc2	brigáda
a	a	k8xC	a
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
postupujících	postupující	k2eAgMnPc2d1	postupující
1050	[number]	k4	1050
mužů	muž	k1gMnPc2	muž
lehké	lehký	k2eAgFnSc2d1	lehká
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
spolu	spolu	k6eAd1	spolu
glasgowskými	glasgowský	k2eAgMnPc7d1	glasgowský
horaly	horal	k1gMnPc7	horal
na	na	k7c4	na
Wellingtonův	Wellingtonův	k2eAgInSc4d1	Wellingtonův
povel	povel	k1gInSc4	povel
zahájili	zahájit	k5eAaPmAgMnP	zahájit
na	na	k7c4	na
Francouze	Francouz	k1gMnSc4	Francouz
další	další	k2eAgFnSc4d1	další
střelbu	střelba	k1gFnSc4	střelba
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
do	do	k7c2	do
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
bodák	bodák	k1gInSc4	bodák
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c4	před
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
na	na	k7c4	na
osm	osm	k4xCc4	osm
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
<g/>
.	.	kIx.	.
</s>
<s>
Garda	garda	k1gFnSc1	garda
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
neustupuje	ustupovat	k5eNaImIp3nS	ustupovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nucena	nucen	k2eAgFnSc1d1	nucena
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtverhranech	čtverhran	k1gInPc6	čtverhran
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
stahovat	stahovat	k5eAaImF	stahovat
z	z	k7c2	z
těžce	těžce	k6eAd1	těžce
dobytých	dobytý	k2eAgFnPc2d1	dobytá
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
před	před	k7c7	před
očima	oko	k1gNnPc7	oko
mužů	muž	k1gMnPc2	muž
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonova	Erlonův	k2eAgMnSc2d1	Erlonův
a	a	k8xC	a
Reilleho	Reille	k1gMnSc2	Reille
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Proud	proud	k1gInSc1	proud
raněných	raněný	k1gMnPc2	raněný
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
odpojovali	odpojovat	k5eAaImAgMnP	odpojovat
od	od	k7c2	od
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
karé	karé	k1gNnPc2	karé
<g/>
,	,	kIx,	,
budil	budit	k5eAaImAgMnS	budit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
garda	garda	k1gFnSc1	garda
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
panice	panice	k1gFnPc4	panice
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
bojištěm	bojiště	k1gNnSc7	bojiště
ozývaly	ozývat	k5eAaImAgFnP	ozývat
výkřiky	výkřik	k1gInPc4	výkřik
"	"	kIx"	"
<g/>
Garda	garda	k1gFnSc1	garda
couvá	couvat	k5eAaImIp3nS	couvat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Navíc	navíc	k6eAd1	navíc
někdy	někdy	k6eAd1	někdy
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
Zietenův	Zietenův	k2eAgInSc4d1	Zietenův
1	[number]	k4	1
<g/>
.	.	kIx.	.
sbor	sbor	k1gInSc1	sbor
prolomil	prolomit	k5eAaPmAgInS	prolomit
obranu	obrana	k1gFnSc4	obrana
Durutteho	Durutte	k1gMnSc2	Durutte
divize	divize	k1gFnSc2	divize
na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
francouzští	francouzský	k2eAgMnPc1d1	francouzský
vojáci	voják	k1gMnPc1	voják
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
nejde	jít	k5eNaImIp3nS	jít
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Grouchy	Groucha	k1gFnSc2	Groucha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
útočí	útočit	k5eAaImIp3nP	útočit
Prusové	Prus	k1gMnPc1	Prus
<g/>
.	.	kIx.	.
</s>
<s>
Bojovou	bojový	k2eAgFnSc7d1	bojová
vřavou	vřava	k1gFnSc7	vřava
se	se	k3xPyFc4	se
nesly	nést	k5eAaImAgInP	nést
další	další	k2eAgInPc1d1	další
výkřiky	výkřik	k1gInPc1	výkřik
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Zrada	zrada	k1gFnSc1	zrada
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Zachraň	zachránit	k5eAaPmRp2nS	zachránit
se	se	k3xPyFc4	se
kdo	kdo	k3yQnSc1	kdo
můžeš	moct	k5eAaImIp2nS	moct
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
I	i	k9	i
když	když	k8xS	když
většina	většina	k1gFnSc1	většina
jednotek	jednotka	k1gFnPc2	jednotka
prvního	první	k4xOgInSc2	první
sledu	sled	k1gInSc2	sled
bojovala	bojovat	k5eAaImAgFnS	bojovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Napoleonově	Napoleonův	k2eAgFnSc6d1	Napoleonova
armádě	armáda	k1gFnSc6	armáda
začala	začít	k5eAaPmAgFnS	začít
propukat	propukat	k5eAaImF	propukat
panika	panika	k1gFnSc1	panika
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
soustředěným	soustředěný	k2eAgInSc7d1	soustředěný
náporem	nápor	k1gInSc7	nápor
anglické	anglický	k2eAgFnSc2d1	anglická
jízdy	jízda	k1gFnSc2	jízda
podporovaným	podporovaný	k2eAgInSc7d1	podporovaný
útočící	útočící	k2eAgFnSc7d1	útočící
pěchotou	pěchota	k1gFnSc7	pěchota
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
pěchota	pěchota	k1gFnSc1	pěchota
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Erlonova	Erlonův	k2eAgInSc2d1	Erlonův
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Relliho	Rellize	k6eAd1	Rellize
sbor	sbor	k1gInSc1	sbor
ustupoval	ustupovat	k5eAaImAgInS	ustupovat
poměrně	poměrně	k6eAd1	poměrně
spořádaně	spořádaně	k6eAd1	spořádaně
<g/>
,	,	kIx,	,
prchat	prchat	k5eAaImF	prchat
začaly	začít	k5eAaPmAgInP	začít
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gNnSc7	jeho
dvě	dva	k4xCgFnPc1	dva
či	či	k8xC	či
tři	tři	k4xCgFnPc1	tři
pěší	pěší	k2eAgFnPc1d1	pěší
roty	rota	k1gFnPc1	rota
<g/>
.	.	kIx.	.
</s>
<s>
Ústup	ústup	k1gInSc1	ústup
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dařil	dařit	k5eAaImAgInS	dařit
až	až	k9	až
k	k	k7c3	k
Belle	bell	k1gInSc5	bell
Aliance	aliance	k1gFnSc1	aliance
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
jeho	jeho	k3xOp3gNnSc3	jeho
uskupení	uskupení	k1gNnSc3	uskupení
roztrhal	roztrhat	k5eAaPmAgInS	roztrhat
proud	proud	k1gInSc1	proud
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Francouzský	francouzský	k2eAgInSc1d1	francouzský
ústup	ústup	k1gInSc1	ústup
===	===	k?	===
</s>
</p>
<p>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
špatně	špatně	k6eAd1	špatně
informován	informovat	k5eAaBmNgMnS	informovat
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
zbývajícími	zbývající	k2eAgInPc7d1	zbývající
gardovými	gardový	k2eAgInPc7d1	gardový
prapory	prapor	k1gInPc7	prapor
po	po	k7c6	po
Bruselské	bruselský	k2eAgFnSc6d1	bruselská
silnici	silnice	k1gFnSc6	silnice
vpřed	vpřed	k6eAd1	vpřed
k	k	k7c3	k
La	la	k1gNnSc3	la
Haye-Sainte	Haye-Saint	k1gMnSc5	Haye-Saint
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
cesty	cesta	k1gFnSc2	cesta
prchajícím	prchající	k2eAgFnPc3d1	prchající
jednotkám	jednotka	k1gFnPc3	jednotka
a	a	k8xC	a
anglickému	anglický	k2eAgNnSc3d1	anglické
jezdectvu	jezdectvo	k1gNnSc3	jezdectvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tyto	tento	k3xDgFnPc4	tento
prchající	prchající	k2eAgFnPc4d1	prchající
masy	masa	k1gFnPc4	masa
masakrovalo	masakrovat	k5eAaBmAgNnS	masakrovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
tři	tři	k4xCgInPc1	tři
gardové	gardový	k2eAgInPc1d1	gardový
prapory	prapor	k1gInPc1	prapor
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
zformovaly	zformovat	k5eAaPmAgInP	zformovat
do	do	k7c2	do
karé	karé	k1gNnSc2	karé
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nebyla	být	k5eNaImAgFnS	být
spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
jízda	jízda	k1gFnSc1	jízda
schopna	schopen	k2eAgFnSc1d1	schopna
prorazit	prorazit	k5eAaPmF	prorazit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
pevné	pevný	k2eAgFnSc3d1	pevná
hrázi	hráz	k1gFnSc3	hráz
se	se	k3xPyFc4	se
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc3	jeho
osobnímu	osobní	k2eAgInSc3d1	osobní
doprovodu	doprovod	k1gInSc3	doprovod
podařilo	podařit	k5eAaPmAgNnS	podařit
ustoupit	ustoupit	k5eAaPmF	ustoupit
k	k	k7c3	k
Belle	bell	k1gInSc5	bell
Aliance	aliance	k1gFnSc1	aliance
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
shromažďovaly	shromažďovat	k5eAaImAgInP	shromažďovat
poslední	poslední	k2eAgInPc1d1	poslední
zálohy	záloh	k1gInPc1	záloh
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
zde	zde	k6eAd1	zde
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
útočícím	útočící	k2eAgMnPc3d1	útočící
spojencům	spojenec	k1gMnPc3	spojenec
a	a	k8xC	a
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
jeho	jeho	k3xOp3gNnSc4	jeho
muži	muž	k1gMnPc1	muž
nedovolili	dovolit	k5eNaPmAgMnP	dovolit
a	a	k8xC	a
donutili	donutit	k5eAaPmAgMnP	donutit
jej	on	k3xPp3gMnSc4	on
pod	pod	k7c4	pod
ochranou	ochrana	k1gFnSc7	ochrana
těchto	tento	k3xDgFnPc2	tento
jednotek	jednotka	k1gFnPc2	jednotka
ustoupit	ustoupit	k5eAaPmF	ustoupit
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázen	doprovázen	k2eAgInSc1d1	doprovázen
některými	některý	k3yIgFnPc7	některý
svými	svůj	k3xOyFgMnPc7	svůj
generály	generál	k1gMnPc7	generál
<g/>
,	,	kIx,	,
maršálem	maršál	k1gMnSc7	maršál
Soultem	Soult	k1gInSc7	Soult
a	a	k8xC	a
několika	několik	k4yIc7	několik
myslivci	myslivec	k1gMnPc7	myslivec
uprostřed	uprostřed	k7c2	uprostřed
karé	karé	k1gNnSc2	karé
I.	I.	kA	I.
praporu	prapor	k1gInSc2	prapor
1	[number]	k4	1
<g/>
.	.	kIx.	.
granátnického	granátnický	k2eAgInSc2d1	granátnický
gardového	gardový	k2eAgInSc2d1	gardový
pluku	pluk	k1gInSc2	pluk
takto	takto	k6eAd1	takto
dorazil	dorazit	k5eAaPmAgMnS	dorazit
až	až	k9	až
do	do	k7c2	do
Genappe	Genapp	k1gInSc5	Genapp
<g/>
.	.	kIx.	.
<g/>
Osud	osud	k1gInSc1	osud
zbývajících	zbývající	k2eAgInPc2d1	zbývající
tří	tři	k4xCgInPc2	tři
karé	karé	k1gNnPc2	karé
u	u	k7c2	u
bruselské	bruselský	k2eAgFnSc2d1	bruselská
silnice	silnice	k1gFnSc2	silnice
byl	být	k5eAaImAgInS	být
neméně	málo	k6eNd2	málo
dramatický	dramatický	k2eAgInSc1d1	dramatický
<g/>
.	.	kIx.	.
</s>
<s>
Obklopeny	obklopen	k2eAgInPc1d1	obklopen
drtivou	drtivý	k2eAgFnSc7d1	drtivá
přesilou	přesila	k1gFnSc7	přesila
anglo-batavské	angloatavský	k2eAgFnSc2d1	anglo-batavský
i	i	k8xC	i
pruské	pruský	k2eAgFnSc2d1	pruská
armády	armáda	k1gFnSc2	armáda
pomalu	pomalu	k6eAd1	pomalu
ustupovaly	ustupovat	k5eAaImAgFnP	ustupovat
k	k	k7c3	k
Maison-du-Roi	Maisonu-Ro	k1gFnSc3	Maison-du-Ro
a	a	k8xC	a
Rossomme	Rossomme	k1gFnSc3	Rossomme
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byli	být	k5eAaImAgMnP	být
gardisté	gardista	k1gMnPc1	gardista
několikrát	několikrát	k6eAd1	několikrát
vyzýváni	vyzýván	k2eAgMnPc1d1	vyzýván
aby	aby	kYmCp3nP	aby
složili	složit	k5eAaPmAgMnP	složit
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jakými	jaký	k3yIgNnPc7	jaký
slovy	slovo	k1gNnPc7	slovo
si	se	k3xPyFc3	se
nebyli	být	k5eNaImAgMnP	být
jisti	jist	k2eAgMnPc1d1	jist
ani	ani	k8xC	ani
současníci	současník	k1gMnPc1	současník
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
události	událost	k1gFnSc6	událost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
legenda	legenda	k1gFnSc1	legenda
spjatá	spjatý	k2eAgFnSc1d1	spjatá
s	s	k7c7	s
bitvou	bitva	k1gFnSc7	bitva
praví	pravit	k5eAaImIp3nS	pravit
<g/>
,	,	kIx,	,
že	že	k8xS	že
generál	generál	k1gMnSc1	generál
Cambronne	Cambronn	k1gInSc5	Cambronn
vykřikl	vykřiknout	k5eAaPmAgInS	vykřiknout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Císařská	císařský	k2eAgFnSc1d1	císařská
garda	garda	k1gFnSc1	garda
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevzdává	vzdávat	k5eNaImIp3nS	vzdávat
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Jiní	jiný	k2eAgMnPc1d1	jiný
pamětníci	pamětník	k1gMnPc1	pamětník
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpověděl	odpovědět	k5eAaPmAgInS	odpovědět
prostým	prostý	k2eAgNnSc7d1	prosté
vojáckým	vojácký	k2eAgNnSc7d1	vojácké
provoláním	provolání	k1gNnSc7	provolání
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Merde	Merd	k1gMnSc5	Merd
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Hovno	hovno	k1gNnSc1	hovno
<g/>
!	!	kIx.	!
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
opakované	opakovaný	k2eAgInPc4d1	opakovaný
útoky	útok	k1gInPc4	útok
jak	jak	k8xS	jak
jízdy	jízda	k1gFnPc4	jízda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
anglické	anglický	k2eAgFnSc2d1	anglická
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
gardisté	gardista	k1gMnPc1	gardista
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
stahovali	stahovat	k5eAaImAgMnP	stahovat
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Pěchotní	pěchotní	k2eAgInPc1d1	pěchotní
čtverce	čtverec	k1gInPc1	čtverec
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgInP	změnit
na	na	k7c4	na
trojúhelníky	trojúhelník	k1gInPc4	trojúhelník
a	a	k8xC	a
kolem	kolem	k7c2	kolem
deváté	devátý	k4xOgFnSc2	devátý
hodiny	hodina	k1gFnSc2	hodina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
tmě	tma	k1gFnSc6	tma
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zmizet	zmizet	k5eAaPmF	zmizet
z	z	k7c2	z
utuchající	utuchající	k2eAgFnSc2d1	utuchající
vřavy	vřava	k1gFnSc2	vřava
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
epizodě	epizoda	k1gFnSc6	epizoda
bojů	boj	k1gInPc2	boj
urvala	urvat	k5eAaPmAgFnS	urvat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
francouzských	francouzský	k2eAgFnPc2d1	francouzská
dělových	dělový	k2eAgFnPc2d1	dělová
koulí	koule	k1gFnPc2	koule
nohu	noha	k1gFnSc4	noha
veliteli	velitel	k1gMnPc7	velitel
anglické	anglický	k2eAgFnSc2d1	anglická
jízdy	jízda	k1gFnSc2	jízda
lordu	lord	k1gMnSc3	lord
Uxbridgovi	Uxbridg	k1gMnSc3	Uxbridg
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
pravém	pravý	k2eAgNnSc6d1	pravé
křídle	křídlo	k1gNnSc6	křídlo
u	u	k7c2	u
Plancenoit	Plancenoita	k1gFnPc2	Plancenoita
kladly	klást	k5eAaImAgFnP	klást
vytrvalý	vytrvalý	k2eAgInSc4d1	vytrvalý
odpor	odpor	k1gInSc4	odpor
Prusům	Prus	k1gMnPc3	Prus
zbytky	zbytek	k1gInPc7	zbytek
Loubauových	Loubauův	k2eAgFnPc2d1	Loubauův
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
mladé	mladý	k2eAgFnSc2d1	mladá
i	i	k8xC	i
staré	starý	k2eAgFnSc2d1	stará
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
jejich	jejich	k3xOp3gFnPc4	jejich
houževnatosti	houževnatost	k1gFnPc4	houževnatost
mohla	moct	k5eAaImAgFnS	moct
prchající	prchající	k2eAgFnSc1d1	prchající
francouzská	francouzský	k2eAgFnSc1d1	francouzská
armáda	armáda	k1gFnSc1	armáda
děkovat	děkovat	k5eAaImF	děkovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
nebyla	být	k5eNaImAgFnS	být
odříznuta	odříznut	k2eAgFnSc1d1	odříznuta
cesta	cesta	k1gFnSc1	cesta
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
Genappe	Genapp	k1gInSc5	Genapp
a	a	k8xC	a
Quatre-Bras	Quatre-Brasa	k1gFnPc2	Quatre-Brasa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
bojích	boj	k1gInPc6	boj
a	a	k8xC	a
s	s	k7c7	s
nastávající	nastávající	k2eAgFnSc7d1	nastávající
tmou	tma	k1gFnSc7	tma
však	však	k9	však
byly	být	k5eAaImAgInP	být
i	i	k9	i
tyto	tento	k3xDgFnPc4	tento
jednotky	jednotka	k1gFnPc4	jednotka
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
pozic	pozice	k1gFnPc2	pozice
vytlačeny	vytlačit	k5eAaPmNgInP	vytlačit
a	a	k8xC	a
donuceny	donutit	k5eAaPmNgInP	donutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
údolím	údolí	k1gNnSc7	údolí
Lasne	Lasn	k1gInSc5	Lasn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Epilog	epilog	k1gInSc1	epilog
==	==	k?	==
</s>
</p>
<p>
<s>
Wellington	Wellington	k1gInSc1	Wellington
nechal	nechat	k5eAaPmAgInS	nechat
zastavit	zastavit	k5eAaPmF	zastavit
britský	britský	k2eAgInSc1d1	britský
postup	postup	k1gInSc1	postup
u	u	k7c2	u
Maison-du-Roi	Maisonu-Ro	k1gFnSc2	Maison-du-Ro
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
farmě	farma	k1gFnSc3	farma
Belle-Aliance	Belle-Aliance	k1gFnSc2	Belle-Aliance
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
maršálem	maršál	k1gMnSc7	maršál
Blücherem	Blücher	k1gMnSc7	Blücher
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
spolu	spolu	k6eAd1	spolu
porozprávěli	porozprávět	k5eAaPmAgMnP	porozprávět
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
pruský	pruský	k2eAgMnSc1d1	pruský
maršál	maršál	k1gMnSc1	maršál
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
svých	svůj	k3xOyFgMnPc2	svůj
mužů	muž	k1gMnPc2	muž
odjel	odjet	k5eAaPmAgMnS	odjet
pronásledovat	pronásledovat	k5eAaImF	pronásledovat
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Genappe	Genapp	k1gMnSc5	Genapp
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
muži	muž	k1gMnPc1	muž
vybili	vybít	k5eAaPmAgMnP	vybít
zbylé	zbylý	k2eAgMnPc4d1	zbylý
Francouze	Francouz	k1gMnPc4	Francouz
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
hotelu	hotel	k1gInSc6	hotel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
suitou	suita	k1gFnSc7	suita
dorazil	dorazit	k5eAaPmAgInS	dorazit
ke	k	k7c3	k
Genappe	Genapp	k1gInSc5	Genapp
kolem	kolem	k7c2	kolem
deváté	devátý	k4xOgFnSc2	devátý
hodiny	hodina	k1gFnSc2	hodina
večerní	večerní	k2eAgFnSc2d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
odjel	odjet	k5eAaPmAgMnS	odjet
ke	k	k7c3	k
Quatre-Bras	Quatre-Bras	k1gInSc4	Quatre-Bras
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prý	prý	k9	prý
dlouho	dlouho	k6eAd1	dlouho
stál	stát	k5eAaImAgMnS	stát
nad	nad	k7c7	nad
nepohřbenými	pohřbený	k2eNgNnPc7d1	nepohřbené
těly	tělo	k1gNnPc7	tělo
mrtvých	mrtvý	k1gMnPc2	mrtvý
z	z	k7c2	z
předešlé	předešlý	k2eAgFnSc2d1	předešlá
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
soukromém	soukromý	k2eAgInSc6d1	soukromý
obřadu	obřad	k1gInSc6	obřad
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgInS	odebrat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Branami	brána	k1gFnPc7	brána
francouzské	francouzský	k2eAgFnSc2d1	francouzská
metropole	metropol	k1gFnSc2	metropol
projel	projet	k5eAaPmAgMnS	projet
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
cestu	cesta	k1gFnSc4	cesta
prý	prý	k9	prý
dřímal	dřímat	k5eAaImAgMnS	dřímat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
již	již	k9	již
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
příjezdem	příjezd	k1gInSc7	příjezd
začal	začít	k5eAaPmAgInS	začít
Joseph	Joseph	k1gInSc4	Joseph
Fouché	Fouchý	k2eAgInPc1d1	Fouchý
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
i	i	k8xC	i
sněmovnu	sněmovna	k1gFnSc4	sněmovna
pairů	pair	k1gMnPc2	pair
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
opačné	opačný	k2eAgInPc4d1	opačný
požadavky	požadavek	k1gInPc4	požadavek
části	část	k1gFnSc2	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
požadovaly	požadovat	k5eAaImAgFnP	požadovat
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
rezignaci	rezignace	k1gFnSc4	rezignace
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
vše	všechen	k3xTgNnSc4	všechen
přijal	přijmout	k5eAaPmAgMnS	přijmout
se	s	k7c7	s
zdánlivým	zdánlivý	k2eAgInSc7d1	zdánlivý
klidem	klid	k1gInSc7	klid
<g/>
.	.	kIx.	.
</s>
<s>
Abdikaci	abdikace	k1gFnSc4	abdikace
podepsal	podepsat	k5eAaPmAgMnS	podepsat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Napoleona	Napoleon	k1gMnSc2	Napoleon
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Elysejském	elysejský	k2eAgInSc6d1	elysejský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Rochefortu	Rochefort	k1gInSc2	Rochefort
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
odplul	odplout	k5eAaPmAgMnS	odplout
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
Anglie	Anglie	k1gFnSc2	Anglie
nikdy	nikdy	k6eAd1	nikdy
nevstoupil	vstoupit	k5eNaPmAgMnS	vstoupit
a	a	k8xC	a
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1815	[number]	k4	1815
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
internaci	internace	k1gFnSc6	internace
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Svaté	svatý	k2eAgFnSc2d1	svatá
Heleny	Helena	k1gFnSc2	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jednapadesáti	jednapadesát	k4xCc2	jednapadesát
let	léto	k1gNnPc2	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bojiště	bojiště	k1gNnSc4	bojiště
dnes	dnes	k6eAd1	dnes
==	==	k?	==
</s>
</p>
<p>
<s>
Současná	současný	k2eAgFnSc1d1	současná
tvář	tvář	k1gFnSc1	tvář
bitevního	bitevní	k2eAgNnSc2d1	bitevní
pole	pole	k1gNnSc2	pole
připomíná	připomínat	k5eAaImIp3nS	připomínat
krajinu	krajina	k1gFnSc4	krajina
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
jen	jen	k9	jen
vzdáleně	vzdáleně	k6eAd1	vzdáleně
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
prý	prý	k9	prý
vévoda	vévoda	k1gMnSc1	vévoda
Wellington	Wellington	k1gInSc4	Wellington
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vzali	vzít	k5eAaPmAgMnP	vzít
mi	já	k3xPp1nSc3	já
mé	můj	k3xOp1gNnSc1	můj
bojiště	bojiště	k1gNnSc1	bojiště
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Příčinou	příčina	k1gFnSc7	příčina
takové	takový	k3xDgFnPc4	takový
změny	změna	k1gFnPc4	změna
byla	být	k5eAaImAgFnS	být
výstavba	výstavba	k1gFnSc1	výstavba
dominanty	dominanta	k1gFnSc2	dominanta
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
čtyřicet	čtyřicet	k4xCc4	čtyřicet
pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
vysokého	vysoký	k2eAgInSc2d1	vysoký
pahorku	pahorek	k1gInSc2	pahorek
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
železná	železný	k2eAgFnSc1d1	železná
socha	socha	k1gFnSc1	socha
lva	lev	k1gMnSc2	lev
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
cení	cenit	k5eAaImIp3nS	cenit
zuby	zub	k1gInPc4	zub
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
(	(	kIx(	(
<g/>
stavbě	stavba	k1gFnSc3	stavba
je	být	k5eAaImIp3nS	být
též	též	k9	též
přezdíváno	přezdíván	k2eAgNnSc4d1	přezdíváno
Lví	lví	k2eAgInSc4d1	lví
kopec	kopec	k1gInSc4	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
památníků	památník	k1gInPc2	památník
je	být	k5eAaImIp3nS	být
Jean-François	Jean-François	k1gFnSc1	Jean-François
Van	van	k1gInSc4	van
Geel	Geel	k1gInSc1	Geel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlína	hlína	k1gFnSc1	hlína
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
stojí	stát	k5eAaImIp3nS	stát
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
někdejších	někdejší	k2eAgFnPc2d1	někdejší
anglo-batavských	angloatavský	k2eAgFnPc2d1	anglo-batavský
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
svážena	svážit	k5eAaPmNgFnS	svážit
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
zemní	zemní	k2eAgFnPc1d1	zemní
práce	práce	k1gFnPc1	práce
neminuly	minout	k5eNaImAgFnP	minout
ani	ani	k8xC	ani
svah	svah	k1gInSc4	svah
mezi	mezi	k7c7	mezi
La	la	k1gNnSc7	la
Haye-Sainte	Haye-Saint	k1gInSc5	Haye-Saint
a	a	k8xC	a
Hougoumontem	Hougoumont	k1gMnSc7	Hougoumont
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
mohyle	mohyla	k1gFnSc3	mohyla
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
vrchol	vrchol	k1gInSc4	vrchol
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vystoupat	vystoupat	k5eAaPmF	vystoupat
po	po	k7c6	po
kamenném	kamenný	k2eAgNnSc6d1	kamenné
schodišti	schodiště	k1gNnSc6	schodiště
<g/>
,	,	kIx,	,
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
návštěvnické	návštěvnický	k2eAgNnSc1d1	návštěvnické
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
promítány	promítán	k2eAgFnPc1d1	promítána
ukázky	ukázka	k1gFnPc1	ukázka
z	z	k7c2	z
filmu	film	k1gInSc2	film
Waterloo	Waterloo	k1gNnSc2	Waterloo
a	a	k8xC	a
lze	lze	k6eAd1	lze
si	se	k3xPyFc3	se
zakoupit	zakoupit	k5eAaPmF	zakoupit
upomínkové	upomínkový	k2eAgInPc4d1	upomínkový
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
stavbou	stavba	k1gFnSc7	stavba
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
Lvího	lví	k2eAgInSc2d1	lví
kopce	kopec	k1gInSc2	kopec
je	být	k5eAaImIp3nS	být
kruhové	kruhový	k2eAgNnSc1d1	kruhové
panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
malíř	malíř	k1gMnSc1	malíř
Louis	Louis	k1gMnSc1	Louis
Dumoulin	Dumoulin	k2eAgMnSc1d1	Dumoulin
<g/>
.	.	kIx.	.
</s>
<s>
Malba	malba	k1gFnSc1	malba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
Maroldovu	Maroldův	k2eAgNnSc3d1	Maroldovo
panoramatu	panorama	k1gNnSc3	panorama
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
<g/>
,	,	kIx,	,
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
momenty	moment	k1gInPc4	moment
kolem	kolem	k7c2	kolem
šestnácté	šestnáctý	k4xOgFnSc2	šestnáctý
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
9000	[number]	k4	9000
francouzských	francouzský	k2eAgMnPc2d1	francouzský
jezdců	jezdec	k1gMnPc2	jezdec
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
na	na	k7c4	na
spojenecká	spojenecký	k2eAgNnPc4d1	spojenecké
karé	karé	k1gNnPc4	karé
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
120	[number]	k4	120
x	x	k?	x
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Lví	lví	k2eAgFnSc2d1	lví
mohyly	mohyla	k1gFnSc2	mohyla
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
bojiště	bojiště	k1gNnSc1	bojiště
poseto	posít	k5eAaPmNgNnS	posít
množstvím	množství	k1gNnSc7	množství
památníků	památník	k1gInPc2	památník
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
připomínají	připomínat	k5eAaImIp3nP	připomínat
oběti	oběť	k1gFnPc4	oběť
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
epizod	epizoda	k1gFnPc2	epizoda
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
městečku	městečko	k1gNnSc6	městečko
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
asi	asi	k9	asi
4	[number]	k4	4
km	km	kA	km
od	od	k7c2	od
křižovatky	křižovatka	k1gFnSc2	křižovatka
Bruselské	bruselský	k2eAgFnSc2d1	bruselská
a	a	k8xC	a
Ohalinské	Ohalinský	k2eAgFnSc2d1	Ohalinský
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
Wellingtonovo	Wellingtonův	k2eAgNnSc1d1	Wellingtonovo
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
lze	lze	k6eAd1	lze
kromě	kromě	k7c2	kromě
předmětů	předmět	k1gInPc2	předmět
nalezených	nalezený	k2eAgInPc2d1	nalezený
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k9	i
autentické	autentický	k2eAgFnPc4d1	autentická
uniformy	uniforma	k1gFnPc4	uniforma
a	a	k8xC	a
detailní	detailní	k2eAgFnPc4d1	detailní
mapy	mapa	k1gFnPc4	mapa
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
navštívit	navštívit	k5eAaPmF	navštívit
okolí	okolí	k1gNnSc4	okolí
všech	všecek	k3xTgInPc2	všecek
významných	významný	k2eAgInPc2d1	významný
civilních	civilní	k2eAgInPc2d1	civilní
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
není	být	k5eNaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
události	událost	k1gFnSc2	událost
je	být	k5eAaImIp3nS	být
poblíž	poblíž	k6eAd1	poblíž
Plancenoit	Plancenoit	k1gInSc4	Plancenoit
a	a	k8xC	a
Hougoumontu	Hougoumonta	k1gFnSc4	Hougoumonta
každoročně	každoročně	k6eAd1	každoročně
pořádána	pořádán	k2eAgFnSc1d1	pořádána
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
bitvy	bitva	k1gFnSc2	bitva
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
i	i	k9	i
řada	řada	k1gFnSc1	řada
klubů	klub	k1gInPc2	klub
vojenské	vojenský	k2eAgFnSc2d1	vojenská
historie	historie	k1gFnSc2	historie
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Sto	sto	k4xCgNnSc4	sto
dní	den	k1gInPc2	den
z	z	k7c2	z
Elby	Elba	k1gFnSc2	Elba
k	k	k7c3	k
Waterloo	Waterloo	k1gNnSc3	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Elka	Elka	k1gFnSc1	Elka
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
358	[number]	k4	358
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902353	[number]	k4	902353
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgNnPc1d1	Napoleonovo
tažení	tažení	k1gNnPc1	tažení
IV	IV	kA	IV
<g/>
.	.	kIx.	.
–	–	k?	–
Pád	Pád	k1gInSc1	Pád
orla	orel	k1gMnSc2	orel
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
307	[number]	k4	307
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Waterloo	Waterloo	k1gNnSc7	Waterloo
Směr	směr	k1gInSc1	směr
Brusel	Brusel	k1gInSc1	Brusel
–	–	k?	–
Bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Quatre	Quatr	k1gInSc5	Quatr
Bras	Brasa	k1gFnPc2	Brasa
a	a	k8xC	a
Ligny	Ligna	k1gFnSc2	Ligna
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
438	[number]	k4	438
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
831	[number]	k4	831
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Waterloo	Waterloo	k1gNnPc4	Waterloo
Poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
–	–	k?	–
Bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
a	a	k8xC	a
Wavre	Wavr	k1gInSc5	Wavr
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
479	[number]	k4	479
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
870	[number]	k4	870
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TARABA	TARABA	kA	TARABA
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Waterloo	Waterloo	k1gNnSc1	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
440	[number]	k4	440
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7260	[number]	k4	7260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WOOTTEN	WOOTTEN	kA	WOOTTEN
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc7	Geoffrey
<g/>
.	.	kIx.	.
</s>
<s>
Waterloo	Waterloo	k1gNnSc1	Waterloo
1815	[number]	k4	1815
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jiří	Jiří	k1gMnSc1	Jiří
Hutečka	Hutečka	k1gFnSc1	Hutečka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Amercom	Amercom	k1gInSc1	Amercom
SA	SA	kA	SA
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
96	[number]	k4	96
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
261	[number]	k4	261
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
384	[number]	k4	384
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Waterloo	Waterloo	k1gNnSc2	Waterloo
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
