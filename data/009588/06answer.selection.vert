<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
–	–	k?	–
(	(	kIx(	(
<g/>
současníky	současník	k1gMnPc4	současník
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Belle	bell	k1gInSc5	bell
Alliance	Allianec	k1gMnSc4	Allianec
či	či	k8xC	či
Napoleonem	napoleon	k1gInSc7	napoleon
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Mont-Saint-Jean	Mont-Saint-Jeany	k1gInPc2	Mont-Saint-Jeany
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
a	a	k8xC	a
podvečerních	podvečerní	k2eAgFnPc6d1	podvečerní
hodinách	hodina	k1gFnPc6	hodina
neděle	neděle	k1gFnSc2	neděle
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1815	[number]	k4	1815
asi	asi	k9	asi
20	[number]	k4	20
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
belgické	belgický	k2eAgFnSc2d1	belgická
metropole	metropol	k1gFnSc2	metropol
Bruselu	Brusel	k1gInSc2	Brusel
<g/>
.	.	kIx.	.
</s>
