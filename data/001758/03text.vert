<s>
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
originále	originál	k1gInSc6	originál
Coco	Coco	k1gMnSc1	Coco
avant	avant	k1gMnSc1	avant
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc1d1	francouzský
životopisný	životopisný	k2eAgInSc1d1	životopisný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
o	o	k7c6	o
francouzské	francouzský	k2eAgFnSc6d1	francouzská
módní	módní	k2eAgFnSc6d1	módní
návrhářce	návrhářka	k1gFnSc6	návrhářka
Coco	Coco	k6eAd1	Coco
Chanel	Chanel	k1gInSc4	Chanel
režisérky	režisérka	k1gFnSc2	režisérka
Anne	Ann	k1gFnSc2	Ann
Fontaine	Fontain	k1gInSc5	Fontain
s	s	k7c7	s
Audrey	Audrey	k1gInPc7	Audrey
Tautou	Tautá	k1gFnSc4	Tautá
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
víceméně	víceméně	k9	víceméně
o	o	k7c6	o
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
života	život	k1gInSc2	život
známé	známý	k2eAgFnSc2d1	známá
zakladatelské	zakladatelský	k2eAgFnSc2d1	zakladatelská
osobnosti	osobnost	k1gFnSc2	osobnost
světového	světový	k2eAgNnSc2d1	světové
módního	módní	k2eAgNnSc2d1	módní
odívání	odívání	k1gNnSc2	odívání
<g/>
,	,	kIx,	,
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
období	období	k1gNnSc4	období
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgNnP	být
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
v	v	k7c6	v
sirotčinci	sirotčinec	k1gInSc6	sirotčinec
přibližně	přibližně	k6eAd1	přibližně
až	až	k9	až
do	do	k7c2	do
40	[number]	k4	40
let	léto	k1gNnPc2	léto
jejího	její	k3xOp3gInSc2	její
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
renomovanou	renomovaný	k2eAgFnSc7d1	renomovaná
módní	módní	k2eAgFnSc7d1	módní
návrhářkou	návrhářka	k1gFnSc7	návrhářka
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
Coco	Coco	k6eAd1	Coco
Chanel	Chanel	k1gInSc4	Chanel
jakožto	jakožto	k8xS	jakožto
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
nejen	nejen	k6eAd1	nejen
velmi	velmi	k6eAd1	velmi
moderní	moderní	k2eAgFnSc4d1	moderní
a	a	k8xC	a
emancipovanou	emancipovaný	k2eAgFnSc4d1	emancipovaná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
citlivou	citlivý	k2eAgFnSc4d1	citlivá
i	i	k8xC	i
vnímavou	vnímavý	k2eAgFnSc4d1	vnímavá
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
rozpolcena	rozpolcen	k2eAgFnSc1d1	rozpolcena
především	především	k6eAd1	především
díky	díky	k7c3	díky
době	doba	k1gFnSc3	doba
a	a	k8xC	a
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
ženu	hnát	k5eAaImIp1nS	hnát
která	který	k3yIgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
především	především	k6eAd1	především
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
zcela	zcela	k6eAd1	zcela
ovládaném	ovládaný	k2eAgInSc6d1	ovládaný
vlivnými	vlivný	k2eAgMnPc7d1	vlivný
a	a	k8xC	a
bohatými	bohatý	k2eAgMnPc7d1	bohatý
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úvodních	úvodní	k2eAgFnPc6d1	úvodní
scénách	scéna	k1gFnPc6	scéna
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
jejího	její	k3xOp3gNnSc2	její
dětství	dětství	k1gNnSc1	dětství
v	v	k7c6	v
sirotčinci	sirotčinec	k1gInSc6	sirotčinec
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
příběh	příběh	k1gInSc1	příběh
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
jejími	její	k3xOp3gInPc7	její
prvopočátečními	prvopočáteční	k2eAgInPc7d1	prvopočáteční
uměleckými	umělecký	k2eAgInPc7d1	umělecký
sklony	sklon	k1gInPc7	sklon
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
společně	společně	k6eAd1	společně
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
Gillain	Gillain	k1gMnSc1	Gillain
<g/>
)	)	kIx)	)
začínala	začínat	k5eAaImAgFnS	začínat
jakožto	jakožto	k8xS	jakožto
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
v	v	k7c6	v
nepříliš	příliš	k6eNd1	příliš
významném	významný	k2eAgInSc6d1	významný
kabaretu	kabaret	k1gInSc6	kabaret
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
švadlena	švadlena	k1gFnSc1	švadlena
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
seznámila	seznámit	k5eAaPmAgFnS	seznámit
také	také	k9	také
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
bohatým	bohatý	k2eAgMnSc7d1	bohatý
a	a	k8xC	a
vlivným	vlivný	k2eAgMnSc7d1	vlivný
mecenášem	mecenáš	k1gMnSc7	mecenáš
i	i	k8xC	i
ochráncem	ochránce	k1gMnSc7	ochránce
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
osobě	osoba	k1gFnSc6	osoba
Étiennem	Étienn	k1gMnSc7	Étienn
Balsanem	Balsan	k1gMnSc7	Balsan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yIgMnPc3	který
později	pozdě	k6eAd2	pozdě
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
zámku	zámek	k1gInSc6	zámek
žila	žít	k5eAaImAgFnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
pak	pak	k8xC	pak
poznala	poznat	k5eAaPmAgFnS	poznat
jeho	on	k3xPp3gMnSc4	on
společníka	společník	k1gMnSc4	společník
vojáka	voják	k1gMnSc4	voják
<g/>
,	,	kIx,	,
hráče	hráč	k1gMnSc2	hráč
pola	pola	k1gFnSc1	pola
a	a	k8xC	a
obchodníka	obchodník	k1gMnSc2	obchodník
Artura	Artur	k1gMnSc2	Artur
Boye	boy	k1gMnSc2	boy
Capela	Capel	k1gMnSc2	Capel
(	(	kIx(	(
<g/>
Alessandro	Alessandra	k1gFnSc5	Alessandra
Nivola	Nivola	k1gFnSc1	Nivola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
Angličan	Angličan	k1gMnSc1	Angličan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
ochráncem	ochránce	k1gMnSc7	ochránce
společně	společně	k6eAd1	společně
obchodoval	obchodovat	k5eAaImAgMnS	obchodovat
a	a	k8xC	a
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
její	její	k3xOp3gFnSc7	její
velkou	velký	k2eAgFnSc7d1	velká
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Americké	americký	k2eAgFnSc2d1	americká
akademie	akademie	k1gFnSc2	akademie
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
Oscar	Oscara	k1gFnPc2	Oscara
za	za	k7c4	za
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
na	na	k7c4	na
britskou	britský	k2eAgFnSc4d1	britská
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
,	,	kIx,	,
pětkrát	pětkrát	k6eAd1	pětkrát
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
César	César	k1gMnSc1	César
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
na	na	k7c4	na
Evropskou	evropský	k2eAgFnSc4d1	Evropská
filmovou	filmový	k2eAgFnSc4d1	filmová
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
všeho	všecek	k3xTgNnSc2	všecek
ovšem	ovšem	k9	ovšem
získal	získat	k5eAaPmAgMnS	získat
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgMnSc4	jeden
Césara	César	k1gMnSc4	César
<g/>
,	,	kIx,	,
Catherine	Catherin	k1gInSc5	Catherin
Leterrierová	Leterrierová	k1gFnSc1	Leterrierová
obdržela	obdržet	k5eAaPmAgFnS	obdržet
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
kostýmy	kostým	k1gInPc4	kostým
<g/>
.	.	kIx.	.
</s>
<s>
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
&	&	k?	&
Igor	Igor	k1gMnSc1	Igor
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
-	-	kIx~	-
francouzský	francouzský	k2eAgInSc1d1	francouzský
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Kounen	Kounen	k1gInSc1	Kounen
</s>
