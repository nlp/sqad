<p>
<s>
Scherzo	scherzo	k1gNnSc1	scherzo
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
žertovný	žertovný	k2eAgInSc1d1	žertovný
ráz	ráz	k1gInSc1	ráz
(	(	kIx(	(
<g/>
scherzo	scherzo	k1gNnSc1	scherzo
=	=	kIx~	=
italsky	italsky	k6eAd1	italsky
žert	žert	k1gInSc4	žert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlé	rychlý	k2eAgNnSc4d1	rychlé
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
živý	živý	k2eAgInSc4d1	živý
rytmus	rytmus	k1gInSc4	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
klasicisimu	klasicisim	k1gInSc2	klasicisim
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
v	v	k7c6	v
cyklických	cyklický	k2eAgFnPc6d1	cyklická
skladbách	skladba	k1gFnPc6	skladba
typu	typ	k1gInSc2	typ
sonát	sonáta	k1gFnPc2	sonáta
a	a	k8xC	a
symfonií	symfonie	k1gFnPc2	symfonie
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
běžných	běžný	k2eAgFnPc2d1	běžná
součástí	součást	k1gFnPc2	součást
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
byl	být	k5eAaImAgInS	být
menuet	menuet	k1gInSc1	menuet
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
i	i	k9	i
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
skladbou	skladba	k1gFnSc7	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Capriccio	capriccio	k1gNnSc1	capriccio
</s>
</p>
