<s>
Scherzo	scherzo	k1gNnSc1	scherzo
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
žertovný	žertovný	k2eAgInSc1d1	žertovný
ráz	ráz	k1gInSc1	ráz
(	(	kIx(	(
<g/>
scherzo	scherzo	k1gNnSc1	scherzo
=	=	kIx~	=
italsky	italsky	k6eAd1	italsky
žert	žert	k1gInSc4	žert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlé	rychlý	k2eAgNnSc4d1	rychlé
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
živý	živý	k2eAgInSc4d1	živý
rytmus	rytmus	k1gInSc4	rytmus
<g/>
.	.	kIx.	.
</s>
