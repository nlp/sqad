<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
nazývá	nazývat	k5eAaImIp3nS	nazývat
stopa	stopa	k1gFnSc1	stopa
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
přízvučné	přízvučný	k2eAgFnSc2d1	přízvučná
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
nepřízvučných	přízvučný	k2eNgFnPc2d1	nepřízvučná
slabik	slabika	k1gFnPc2	slabika
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
?	?	kIx.	?
</s>
