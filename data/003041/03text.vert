<s>
Daktyl	daktyl	k1gInSc1	daktyl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
stopa	stopa	k1gFnSc1	stopa
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
přízvučné	přízvučný	k2eAgFnSc2d1	přízvučná
(	(	kIx(	(
<g/>
či	či	k8xC	či
v	v	k7c6	v
časoměrném	časoměrný	k2eAgInSc6d1	časoměrný
verši	verš	k1gInSc6	verš
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
)	)	kIx)	)
slabiky	slabika	k1gFnPc1	slabika
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
nepřízvučných	přízvučný	k2eNgFnPc2d1	nepřízvučná
(	(	kIx(	(
<g/>
či	či	k8xC	či
krátkých	krátký	k2eAgInPc2d1	krátký
<g/>
)	)	kIx)	)
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Daktyl	daktyl	k1gInSc1	daktyl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
velmi	velmi	k6eAd1	velmi
hojný	hojný	k2eAgMnSc1d1	hojný
a	a	k8xC	a
používaný	používaný	k2eAgInSc1d1	používaný
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k6eAd1	také
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
stopami	stopa	k1gFnPc7	stopa
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
s	s	k7c7	s
trochejemi	trochej	k1gFnPc7	trochej
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
daktylotrochej	daktylotrochej	k1gInSc1	daktylotrochej
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
snadnou	snadný	k2eAgFnSc7d1	snadná
vytvořitelností	vytvořitelnost	k1gFnSc7	vytvořitelnost
<g/>
,	,	kIx,	,
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
také	také	k9	také
tzv.	tzv.	kA	tzv.
hexametr	hexametr	k1gInSc1	hexametr
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
bor	bor	k1gInSc1	bor
utíká	utíkat	k5eAaImIp3nS	utíkat
s	s	k7c7	s
kopečka	kopeček	k1gInSc2	kopeček
<g/>
,	,	kIx,	,
<g/>
široké	široký	k2eAgNnSc1d1	široké
jezero	jezero	k1gNnSc1	jezero
pod	pod	k7c7	pod
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
<g/>
nad	nad	k7c7	nad
vodou	voda	k1gFnSc7	voda
létají	létat	k5eAaImIp3nP	létat
rybárky	rybárek	k1gInPc4	rybárek
<g/>
,	,	kIx,	,
<g/>
podobny	podoben	k2eAgInPc4d1	podoben
plachtám	plachta	k1gFnPc3	plachta
jsou	být	k5eAaImIp3nP	být
lodním	lodní	k2eAgNnSc6d1	lodní
<g/>
.	.	kIx.	.
</s>
<s>
Hradisko	hradisko	k1gNnSc1	hradisko
sivé	sivý	k2eAgFnSc2d1	sivá
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
<g/>
,	,	kIx,	,
<g/>
na	na	k7c4	na
příkré	příkrý	k2eAgNnSc4d1	příkré
vyhnáno	vyhnán	k2eAgNnSc4d1	vyhnáno
skále	skála	k1gFnSc6	skála
-dnes	nes	k1gInSc1	-dnes
tam	tam	k6eAd1	tam
jen	jen	k9	jen
Themis	Themis	k1gFnSc1	Themis
promlouváve	promlouvávat	k5eAaPmIp3nS	promlouvávat
jménu	jméno	k1gNnSc6	jméno
boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Bezruč	Bezruč	k1gMnSc1	Bezruč
-	-	kIx~	-
Plumlov	Plumlov	k1gInSc1	Plumlov
(	(	kIx(	(
<g/>
Pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
srozumitelnost	srozumitelnost	k1gFnSc4	srozumitelnost
jsou	být	k5eAaImIp3nP	být
přízvučné	přízvučný	k2eAgFnPc1d1	přízvučná
slabiky	slabika	k1gFnPc1	slabika
vyznačeny	vyznačit	k5eAaPmNgFnP	vyznačit
tučně	tučně	k6eAd1	tučně
<g/>
,	,	kIx,	,
v	v	k7c6	v
citované	citovaný	k2eAgFnSc6d1	citovaná
ukázce	ukázka	k1gFnSc6	ukázka
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnSc1d1	poslední
stopa	stopa	k1gFnSc1	stopa
sudých	sudý	k2eAgInPc2d1	sudý
veršů	verš	k1gInPc2	verš
neúplná	úplný	k2eNgFnSc1d1	neúplná
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
