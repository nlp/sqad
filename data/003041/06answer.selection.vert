<s>
Daktyl	daktyl	k1gInSc1	daktyl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
metrice	metrika	k1gFnSc6	metrika
stopa	stopa	k1gFnSc1	stopa
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
přízvučné	přízvučný	k2eAgFnSc2d1	přízvučná
(	(	kIx(	(
<g/>
či	či	k8xC	či
v	v	k7c6	v
časoměrném	časoměrný	k2eAgInSc6d1	časoměrný
verši	verš	k1gInSc6	verš
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
)	)	kIx)	)
slabiky	slabika	k1gFnPc1	slabika
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
nepřízvučných	přízvučný	k2eNgFnPc2d1	nepřízvučná
(	(	kIx(	(
<g/>
či	či	k8xC	či
krátkých	krátký	k2eAgInPc2d1	krátký
<g/>
)	)	kIx)	)
v	v	k7c6	v
uvedeném	uvedený	k2eAgNnSc6d1	uvedené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
