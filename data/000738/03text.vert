<s>
Hérostratos	Hérostratos	k1gMnSc1	Hérostratos
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Hérostratés	Hérostratés	k1gInSc1	Hérostratés
<g/>
,	,	kIx,	,
Herostrates	Herostrates	k1gInSc1	Herostrates
či	či	k8xC	či
latinizovaně	latinizovaně	k6eAd1	latinizovaně
Herostratus	Herostratus	k1gInSc4	Herostratus
<g/>
)	)	kIx)	)
z	z	k7c2	z
Efesu	Efes	k1gInSc2	Efes
(	(	kIx(	(
<g/>
starořecky	starořecky	k6eAd1	starořecky
Η	Η	k?	Η
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
356	[number]	k4	356
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
podpálil	podpálit	k5eAaPmAgInS	podpálit
Artemidin	Artemidin	k2eAgInSc1d1	Artemidin
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Efesu	Efes	k1gInSc6	Efes
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
čin	čin	k1gInSc4	čin
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
jeho	on	k3xPp3gInSc4	on
záměr	záměr	k1gInSc4	záměr
nepodařil	podařit	k5eNaPmAgMnS	podařit
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Efezu	Efez	k1gInSc6	Efez
pod	pod	k7c7	pod
sankcí	sankce	k1gFnSc7	sankce
smrti	smrt	k1gFnSc2	smrt
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
zmiňovat	zmiňovat	k5eAaImF	zmiňovat
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
damnatio	damnatio	k1gNnSc4	damnatio
memoriae	memoriaat	k5eAaPmIp3nS	memoriaat
se	se	k3xPyFc4	se
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
a	a	k8xC	a
Hérostratově	Hérostratův	k2eAgNnSc6d1	Hérostratův
jménu	jméno	k1gNnSc6	jméno
zmínil	zmínit	k5eAaPmAgMnS	zmínit
historik	historik	k1gMnSc1	historik
Theopompos	Theopompos	k1gMnSc1	Theopompos
z	z	k7c2	z
Chiu	Chius	k1gInSc2	Chius
a	a	k8xC	a
Hérostratos	Hérostratos	k1gMnSc1	Hérostratos
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
světoznámým	světoznámý	k2eAgMnSc7d1	světoznámý
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
.	.	kIx.	.
</s>
<s>
Hérostratův	Hérostratův	k2eAgInSc4d1	Hérostratův
čin	čin	k1gInSc4	čin
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
frazémem	frazém	k1gInSc7	frazém
(	(	kIx(	(
<g/>
herostratovský	herostratovský	k2eAgInSc4d1	herostratovský
čin	čin	k1gInSc4	čin
<g/>
)	)	kIx)	)
a	a	k8xC	a
námětem	námět	k1gInSc7	námět
řady	řada	k1gFnSc2	řada
úvah	úvaha	k1gFnPc2	úvaha
a	a	k8xC	a
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
Sartre	Sartr	k1gInSc5	Sartr
napsal	napsat	k5eAaBmAgInS	napsat
povídku	povídka	k1gFnSc4	povídka
Herostrates	Herostrates	k1gInSc1	Herostrates
(	(	kIx(	(
<g/>
Erostratus	Erostratus	k1gInSc1	Erostratus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gFnSc2	jeho
knihy	kniha	k1gFnSc2	kniha
Zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Reyer	Reyer	k1gMnSc1	Reyer
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
operu	oprat	k5eAaPmIp1nS	oprat
Erostrate	Erostrat	k1gInSc5	Erostrat
<g/>
,	,	kIx,	,
Georg	Georg	k1gMnSc1	Georg
Heym	Heym	k1gMnSc1	Heym
báseň	báseň	k1gFnSc1	báseň
Der	drát	k5eAaImRp2nS	drát
Wahnsinn	Wahnsinn	k1gMnSc1	Wahnsinn
des	des	k1gNnSc1	des
Herostrat	Herostrat	k1gMnSc1	Herostrat
či	či	k8xC	či
Juan	Juan	k1gMnSc1	Juan
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
operu	oprat	k5eAaPmIp1nS	oprat
Celos	Celos	k1gInSc4	Celos
aun	aun	k?	aun
del	del	k?	del
aire	airat	k5eAaPmIp3nS	airat
matan	matan	k1gInSc1	matan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
psychologie	psychologie	k1gFnSc2	psychologie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
termínu	termín	k1gInSc2	termín
herostratismus	herostratismus	k1gInSc4	herostratismus
pro	pro	k7c4	pro
chorobnou	chorobný	k2eAgFnSc4d1	chorobná
touhu	touha	k1gFnSc4	touha
se	se	k3xPyFc4	se
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
stát	stát	k5eAaImF	stát
slavným	slavný	k2eAgInSc7d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
Drobné	drobný	k2eAgFnSc2d1	drobná
povídky	povídka	k1gFnSc2	povídka
dějepisné	dějepisný	k2eAgInPc1d1	dějepisný
<g/>
/	/	kIx~	/
<g/>
O	o	k7c6	o
slávě	sláva	k1gFnSc6	sláva
Herostratově	Herostratův	k2eAgFnSc6d1	Herostratův
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
