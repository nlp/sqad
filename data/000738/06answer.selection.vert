<s>
Hérostratos	Hérostratos	k1gMnSc1	Hérostratos
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Hérostratés	Hérostratés	k1gInSc1	Hérostratés
<g/>
,	,	kIx,	,
Herostrates	Herostrates	k1gInSc1	Herostrates
či	či	k8xC	či
latinizovaně	latinizovaně	k6eAd1	latinizovaně
Herostratus	Herostratus	k1gInSc4	Herostratus
<g/>
)	)	kIx)	)
z	z	k7c2	z
Efesu	Efes	k1gInSc2	Efes
(	(	kIx(	(
<g/>
starořecky	starořecky	k6eAd1	starořecky
Η	Η	k?	Η
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xS	jako
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
356	[number]	k4	356
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
podpálil	podpálit	k5eAaPmAgInS	podpálit
Artemidin	Artemidin	k2eAgInSc1d1	Artemidin
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Efesu	Efes	k1gInSc6	Efes
<g/>
.	.	kIx.	.
</s>
