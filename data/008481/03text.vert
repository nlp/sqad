<p>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
je	být	k5eAaImIp3nS	být
slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
zpěvohra	zpěvohra	k1gFnSc1	zpěvohra
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
jednáních	jednání	k1gNnPc6	jednání
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
koncept	koncept	k1gInSc1	koncept
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Autorem	autor	k1gMnSc7	autor
libreta	libreto	k1gNnSc2	libreto
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Wenzig	Wenzig	k1gMnSc1	Wenzig
(	(	kIx(	(
<g/>
také	také	k9	také
autor	autor	k1gMnSc1	autor
libreta	libreto	k1gNnSc2	libreto
k	k	k7c3	k
Daliboru	Dalibor	k1gMnSc6	Dalibor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
napsal	napsat	k5eAaPmAgMnS	napsat
německy	německy	k6eAd1	německy
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
ho	on	k3xPp3gMnSc4	on
přebásnil	přebásnit	k5eAaPmAgMnS	přebásnit
Ervín	Ervín	k1gMnSc1	Ervín
Špindler	Špindler	k1gMnSc1	Špindler
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Smetana	Smetana	k1gMnSc1	Smetana
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
komponovat	komponovat	k5eAaImF	komponovat
na	na	k7c4	na
německý	německý	k2eAgInSc4d1	německý
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
ale	ale	k9	ale
se	s	k7c7	s
Špindlerem	Špindler	k1gMnSc7	Špindler
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
libreta	libreto	k1gNnSc2	libreto
bude	být	k5eAaImBp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jen	jen	k9	jen
Wenzig	Wenzig	k1gInSc1	Wenzig
a	a	k8xC	a
fakt	fakt	k1gInSc1	fakt
jeho	on	k3xPp3gInSc2	on
překladu	překlad	k1gInSc2	překlad
bude	být	k5eAaImBp3nS	být
utajen	utajit	k5eAaPmNgInS	utajit
<g/>
.	.	kIx.	.
<g/>
Námětem	námět	k1gInSc7	námět
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
ději	děj	k1gInSc3	děj
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
kněžně	kněžna	k1gFnSc6	kněžna
Libuši	Libuše	k1gFnSc6	Libuše
<g/>
,	,	kIx,	,
připomínaná	připomínaný	k2eAgFnSc1d1	připomínaná
již	již	k9	již
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přejatá	přejatý	k2eAgFnSc1d1	přejatá
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
zpracování	zpracování	k1gNnSc2	zpracování
v	v	k7c6	v
Rukopisu	rukopis	k1gInSc6	rukopis
zelenohorském	zelenohorský	k2eAgInSc6d1	zelenohorský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
zamýšlena	zamýšlen	k2eAgFnSc1d1	zamýšlena
jako	jako	k8xC	jako
korunovační	korunovační	k2eAgFnSc1d1	korunovační
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ale	ale	k9	ale
ke	k	k7c3	k
korunovaci	korunovace	k1gFnSc3	korunovace
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
uvedena	uvést	k5eAaPmNgFnS	uvést
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
otevření	otevření	k1gNnSc6	otevření
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
poté	poté	k6eAd1	poté
po	po	k7c6	po
ničivém	ničivý	k2eAgInSc6d1	ničivý
požáru	požár	k1gInSc6	požár
i	i	k8xC	i
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
znovuotevření	znovuotevření	k1gNnSc6	znovuotevření
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smetana	smetana	k1gFnSc1	smetana
a	a	k8xC	a
Wenzig	Wenzig	k1gInSc4	Wenzig
koncipovali	koncipovat	k5eAaBmAgMnP	koncipovat
operu	opera	k1gFnSc4	opera
nikoli	nikoli	k9	nikoli
jako	jako	k9	jako
dramatické	dramatický	k2eAgNnSc4d1	dramatické
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
historické	historický	k2eAgFnPc1d1	historická
tableau	tableau	k6eAd1	tableau
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Wagnerova	Wagnerův	k2eAgInSc2d1	Wagnerův
operního	operní	k2eAgInSc2d1	operní
cyklu	cyklus	k1gInSc2	cyklus
Prsten	prsten	k1gInSc1	prsten
Nibelungův	Nibelungův	k2eAgInSc1d1	Nibelungův
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc1	tři
jednání	jednání	k1gNnPc1	jednání
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
děj	děj	k1gInSc4	děj
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
rozuzlení	rozuzlení	k1gNnSc3	rozuzlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
jednání	jednání	k1gNnSc6	jednání
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
Libušiným	Libušin	k2eAgInSc7d1	Libušin
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
a	a	k8xC	a
třetí	třetí	k4xOgNnSc1	třetí
jednání	jednání	k1gNnSc1	jednání
představují	představovat	k5eAaImIp3nP	představovat
Libušin	Libušin	k2eAgInSc4d1	Libušin
sňatek	sňatek	k1gInSc4	sňatek
a	a	k8xC	a
proroctví	proroctví	k1gNnSc4	proroctví
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
nedějové	dějový	k2eNgNnSc1d1	nedějové
proroctví	proroctví	k1gNnSc1	proroctví
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
koncept	koncept	k1gInSc1	koncept
opery	opera	k1gFnSc2	opera
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
stává	stávat	k5eAaImIp3nS	stávat
velkou	velký	k2eAgFnSc7d1	velká
překážkou	překážka	k1gFnSc7	překážka
k	k	k7c3	k
dramaturgickému	dramaturgický	k2eAgNnSc3d1	dramaturgické
uchopení	uchopení	k1gNnSc3	uchopení
<g/>
.	.	kIx.	.
<g/>
Proroctví	proroctví	k1gNnSc3	proroctví
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
Wenzig	Wenzig	k1gInSc1	Wenzig
ujal	ujmout	k5eAaPmAgInS	ujmout
nejoriginálněji	originálně	k6eAd3	originálně
<g/>
.	.	kIx.	.
</s>
<s>
Zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
kapitoly	kapitola	k1gFnPc4	kapitola
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
považoval	považovat	k5eAaImAgMnS	považovat
sám	sám	k3xTgMnSc1	sám
za	za	k7c2	za
nejcennější	cenný	k2eAgFnSc2d3	nejcennější
–	–	k?	–
včetně	včetně	k7c2	včetně
nehistorické	historický	k2eNgFnSc2d1	nehistorická
a	a	k8xC	a
smyšlené	smyšlený	k2eAgFnSc2d1	smyšlená
postavy	postava	k1gFnSc2	postava
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
<g/>
,	,	kIx,	,
moravského	moravský	k2eAgMnSc2d1	moravský
obránce	obránce	k1gMnSc2	obránce
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
před	před	k7c7	před
náporem	nápor	k1gInSc7	nápor
Mongolů	mongol	k1gInPc2	mongol
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
za	za	k7c4	za
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
považoval	považovat	k5eAaImAgMnS	považovat
i	i	k9	i
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
)	)	kIx)	)
a	a	k8xC	a
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
významu	význam	k1gInSc2	význam
husitů	husita	k1gMnPc2	husita
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
stalo	stát	k5eAaPmAgNnS	stát
zdrojem	zdroj	k1gInSc7	zdroj
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osoby	osoba	k1gFnPc4	osoba
a	a	k8xC	a
první	první	k4xOgNnPc4	první
obsazení	obsazení	k1gNnPc4	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	dít	k5eAaImRp2nS	dít
opery	opera	k1gFnSc2	opera
==	==	k?	==
</s>
</p>
<p>
<s>
I.	I.	kA	I.
jednání	jednání	k1gNnSc1	jednání
<g/>
:	:	kIx,	:
Libušin	Libušin	k2eAgInSc1d1	Libušin
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Velmožové	velmož	k1gMnPc1	velmož
Chrudoš	Chrudoš	k1gMnSc1	Chrudoš
a	a	k8xC	a
Šťáhlav	Šťáhlav	k1gMnPc1	Šťáhlav
se	se	k3xPyFc4	se
neshodli	shodnout	k5eNaPmAgMnP	shodnout
o	o	k7c4	o
otcovo	otcův	k2eAgNnSc4d1	otcovo
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
spor	spor	k1gInSc1	spor
má	mít	k5eAaImIp3nS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
kněžna	kněžna	k1gFnSc1	kněžna
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Chrudoš	Chrudoš	k1gMnSc1	Chrudoš
požaduje	požadovat	k5eAaImIp3nS	požadovat
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
Šťáhlav	Šťáhlav	k1gMnSc1	Šťáhlav
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
podrobit	podrobit	k5eAaPmF	podrobit
se	se	k3xPyFc4	se
kněžninu	kněžnin	k2eAgInSc3d1	kněžnin
verdiktu	verdikt	k1gInSc3	verdikt
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dědictví	dědictví	k1gNnSc1	dědictví
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
mezi	mezi	k7c4	mezi
oba	dva	k4xCgMnPc4	dva
stejným	stejný	k2eAgNnSc7d1	stejné
dílem	dílo	k1gNnSc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Chrudoš	Chrudoš	k1gMnSc1	Chrudoš
to	ten	k3xDgNnSc4	ten
označí	označit	k5eAaPmIp3nS	označit
za	za	k7c4	za
slabošské	slabošský	k2eAgNnSc4d1	slabošské
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
a	a	k8xC	a
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
ostatní	ostatní	k2eAgNnSc1d1	ostatní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dosadili	dosadit	k5eAaPmAgMnP	dosadit
na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
trůn	trůn	k1gInSc4	trůn
znovu	znovu	k6eAd1	znovu
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgNnSc7	tento
dotčena	dotčen	k2eAgFnSc1d1	dotčena
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vybere	vybrat	k5eAaPmIp3nS	vybrat
budoucího	budoucí	k2eAgMnSc4d1	budoucí
manžela	manžel	k1gMnSc4	manžel
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dá	dát	k5eAaPmIp3nS	dát
zemi	zem	k1gFnSc4	zem
znovu	znovu	k6eAd1	znovu
muže	muž	k1gMnSc4	muž
panovníka	panovník	k1gMnSc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
posly	posel	k1gMnPc4	posel
vyšle	vyslat	k5eAaPmIp3nS	vyslat
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
milým	milý	k2eAgMnSc7d1	milý
Přemyslem	Přemysl	k1gMnSc7	Přemysl
do	do	k7c2	do
Stadic	Stadice	k1gInPc2	Stadice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
II	II	kA	II
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc1	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Krasava	Krasava	k1gFnSc1	Krasava
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
Chrudošovi	Chrudoš	k1gMnSc3	Chrudoš
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
spor	spor	k1gInSc4	spor
obou	dva	k4xCgMnPc2	dva
bratří	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Chtěla	chtít	k5eAaImAgFnS	chtít
vyprovokovat	vyprovokovat	k5eAaPmF	vyprovokovat
Chrudoše	Chrudoš	k1gMnSc4	Chrudoš
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předstírala	předstírat	k5eAaImAgFnS	předstírat
náklonnost	náklonnost	k1gFnSc4	náklonnost
ke	k	k7c3	k
Šťáhlavovi	Šťáhlava	k1gMnSc3	Šťáhlava
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
jí	on	k3xPp3gFnSc3	on
odpouští	odpouštět	k5eAaImIp3nS	odpouštět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
povede	vést	k5eAaImIp3nS	vést
spor	spor	k1gInSc1	spor
bratří	bratr	k1gMnPc2	bratr
uklidnit	uklidnit	k5eAaPmF	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Stadic	Stadice	k1gInPc2	Stadice
přijíždí	přijíždět	k5eAaImIp3nP	přijíždět
poslové	posel	k1gMnPc1	posel
z	z	k7c2	z
Vyšehradu	Vyšehrad	k1gInSc2	Vyšehrad
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zrovna	zrovna	k6eAd1	zrovna
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
na	na	k7c6	na
Libuši	Libuše	k1gFnSc6	Libuše
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
překvapen	překvapen	k2eAgMnSc1d1	překvapen
jejich	jejich	k3xOp3gFnSc7	jejich
žádostí	žádost	k1gFnSc7	žádost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydává	vydávat	k5eAaPmIp3nS	vydávat
se	se	k3xPyFc4	se
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
III	III	kA	III
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc1	jednání
<g/>
:	:	kIx,	:
Libušin	Libušin	k2eAgInSc4d1	Libušin
sňatek	sňatek	k1gInSc4	sňatek
a	a	k8xC	a
Libušino	Libušin	k2eAgNnSc4d1	Libušino
proroctví	proroctví	k1gNnSc4	proroctví
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
vyslechne	vyslechnout	k5eAaPmIp3nS	vyslechnout
uklidňující	uklidňující	k2eAgFnSc4d1	uklidňující
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
smíření	smíření	k1gNnSc6	smíření
bratří	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
slavnosti	slavnost	k1gFnSc6	slavnost
předá	předat	k5eAaPmIp3nS	předat
Libuše	Libuše	k1gFnSc1	Libuše
trůn	trůn	k1gInSc1	trůn
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žádá	žádat	k5eAaImIp3nS	žádat
Chrudoše	Chrudoš	k1gMnSc4	Chrudoš
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
kněžně	kněžna	k1gFnSc3	kněžna
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
tak	tak	k6eAd1	tak
učiní	učinit	k5eAaImIp3nS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slavnostní	slavnostní	k2eAgFnSc6d1	slavnostní
chvíli	chvíle	k1gFnSc6	chvíle
dostává	dostávat	k5eAaImIp3nS	dostávat
Libuše	Libuše	k1gFnSc1	Libuše
vidění	vidění	k1gNnSc2	vidění
slavné	slavný	k2eAgFnSc2d1	slavná
budoucnosti	budoucnost	k1gFnSc2	budoucnost
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
obrazem	obraz	k1gInSc7	obraz
knížete	kníže	k1gMnSc2	kníže
Břetislava	Břetislav	k1gMnSc2	Břetislav
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
smyšleného	smyšlený	k2eAgMnSc4d1	smyšlený
moravského	moravský	k2eAgMnSc4d1	moravský
obránce	obránce	k1gMnSc4	obránce
proti	proti	k7c3	proti
Mongolům	Mongol	k1gMnPc3	Mongol
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
<g/>
,	,	kIx,	,
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Elišky	Eliška	k1gFnPc4	Eliška
Přemyslovny	Přemyslovna	k1gFnSc2	Přemyslovna
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
až	až	k9	až
po	po	k7c4	po
husitské	husitský	k2eAgInPc4d1	husitský
války	válek	k1gInPc4	válek
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc4	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
budoucnost	budoucnost	k1gFnSc1	budoucnost
Libuše	Libuše	k1gFnSc2	Libuše
nevidí	vidět	k5eNaImIp3nS	vidět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
končí	končit	k5eAaImIp3nS	končit
optimisticky	optimisticky	k6eAd1	optimisticky
s	s	k7c7	s
předpovědí	předpověď	k1gFnSc7	předpověď
slavného	slavný	k2eAgNnSc2d1	slavné
překonání	překonání	k1gNnSc2	překonání
všech	všecek	k3xTgFnPc2	všecek
hrůz	hrůza	k1gFnPc2	hrůza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Inscenační	inscenační	k2eAgFnSc2d1	inscenační
historie	historie	k1gFnSc2	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
českých	český	k2eAgNnPc6d1	české
jevištích	jeviště	k1gNnPc6	jeviště
===	===	k?	===
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
opera	opera	k1gFnSc1	opera
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1881	[number]	k4	1881
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Sittovou	Sittová	k1gFnSc7	Sittová
jako	jako	k8xC	jako
Libuší	Libuše	k1gFnSc7	Libuše
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
Lvem	Lev	k1gMnSc7	Lev
jako	jako	k8xS	jako
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Premiéru	premiéra	k1gFnSc4	premiéra
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Adolf	Adolf	k1gMnSc1	Adolf
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
režíroval	režírovat	k5eAaImAgMnS	režírovat
František	František	k1gMnSc1	František
Kolár	Kolár	k1gMnSc1	Kolár
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
měla	mít	k5eAaImAgFnS	mít
slavnostní	slavnostní	k2eAgInSc4d1	slavnostní
ráz	ráz	k1gInSc4	ráz
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
návštěvě	návštěva	k1gFnSc3	návštěva
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Rudolfa	Rudolf	k1gMnSc2	Rudolf
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
o	o	k7c6	o
přestávce	přestávka	k1gFnSc6	přestávka
mezi	mezi	k7c7	mezi
prvním	první	k4xOgInSc7	první
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
jednáním	jednání	k1gNnPc3	jednání
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
Smetanovi	Smetanův	k2eAgMnPc1d1	Smetanův
svůj	svůj	k3xOyFgInSc4	svůj
obdiv	obdiv	k1gInSc4	obdiv
a	a	k8xC	a
uznání	uznání	k1gNnSc4	uznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhého	druhý	k4xOgNnSc2	druhý
jednání	jednání	k1gNnSc2	jednání
divadlo	divadlo	k1gNnSc1	divadlo
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Libuše	Libuše	k1gFnSc1	Libuše
dočkala	dočkat	k5eAaPmAgFnS	dočkat
ještě	ještě	k6eAd1	ještě
čtyř	čtyři	k4xCgNnPc2	čtyři
dalších	další	k2eAgNnPc2d1	další
uvedení	uvedení	k1gNnPc2	uvedení
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
obsazení	obsazení	k1gNnSc6	obsazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
vrátila	vrátit	k5eAaPmAgFnS	vrátit
opera	opera	k1gFnSc1	opera
při	při	k7c6	při
znovuotevření	znovuotevření	k1gNnSc6	znovuotevření
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1883	[number]	k4	1883
zásluhou	zásluhou	k7c2	zásluhou
stejných	stejná	k1gFnPc2	stejná
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
na	na	k7c6	na
repertoáru	repertoár	k1gInSc6	repertoár
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
prakticky	prakticky	k6eAd1	prakticky
nepřetržitě	přetržitě	k6eNd1	přetržitě
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
pražskou	pražský	k2eAgFnSc7d1	Pražská
Libuší	Libuše	k1gFnSc7	Libuše
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
Anna	Anna	k1gFnSc1	Anna
Veselá	Veselá	k1gFnSc1	Veselá
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
roli	role	k1gFnSc6	role
excelovaly	excelovat	k5eAaImAgFnP	excelovat
Růžena	Růžena	k1gFnSc1	Růžena
Maturová	Maturový	k2eAgFnSc1d1	Maturová
<g/>
,	,	kIx,	,
Ema	Ema	k1gFnSc1	Ema
Destinnová	Destinnová	k1gFnSc1	Destinnová
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Horvátová	Horvátový	k2eAgFnSc1d1	Horvátová
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
předválečné	předválečný	k2eAgNnSc1d1	předválečné
nastudování	nastudování	k1gNnSc1	nastudování
obsadily	obsadit	k5eAaPmAgFnP	obsadit
Ada	Ada	kA	Ada
Nordenová	Nordenová	k1gFnSc1	Nordenová
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Podvalová	Podvalová	k1gFnSc1	Podvalová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znovu	znovu	k6eAd1	znovu
byla	být	k5eAaImAgFnS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
opera	opera	k1gFnSc1	opera
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
v	v	k7c6	v
inscenaci	inscenace	k1gFnSc6	inscenace
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Pujmana	Pujman	k1gMnSc2	Pujman
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Podvalovou	Podvalová	k1gFnSc7	Podvalová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
a	a	k8xC	a
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
nehrála	hrát	k5eNaImAgFnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
70	[number]	k4	70
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
znovuotevření	znovuotevření	k1gNnSc2	znovuotevření
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Ladislav	Ladislav	k1gMnSc1	Ladislav
Boháč	Boháč	k1gMnSc1	Boháč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nahrazena	nahrazen	k2eAgFnSc1d1	nahrazena
inscenací	inscenace	k1gFnPc2	inscenace
Karla	Karel	k1gMnSc4	Karel
Palouše	Palouš	k1gMnSc4	Palouš
po	po	k7c6	po
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byly	být	k5eAaImAgFnP	být
nejslavnějšími	slavný	k2eAgFnPc7d3	nejslavnější
Libušemi	Libuše	k1gFnPc7	Libuše
kromě	kromě	k7c2	kromě
Podvalové	Podvalová	k1gFnSc2	Podvalová
Naděžda	Naděžda	k1gFnSc1	Naděžda
Kniplová	Kniplová	k1gFnSc1	Kniplová
či	či	k8xC	či
Milada	Milada	k1gFnSc1	Milada
Šubrtová	Šubrtová	k1gFnSc1	Šubrtová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
přestávka	přestávka	k1gFnSc1	přestávka
v	v	k7c6	v
uvádění	uvádění	k1gNnSc6	uvádění
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
následovala	následovat	k5eAaImAgFnS	následovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977	[number]	k4	1977
až	až	k9	až
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
znovuotevření	znovuotevření	k1gNnSc2	znovuotevření
režíroval	režírovat	k5eAaImAgMnS	režírovat
operu	opera	k1gFnSc4	opera
Karel	Karel	k1gMnSc1	Karel
Jernek	Jernek	k1gMnSc1	Jernek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
při	při	k7c6	při
premiéře	premiéra	k1gFnSc6	premiéra
Gabriela	Gabriela	k1gFnSc1	Gabriela
Beňačková	Beňačková	k1gFnSc1	Beňačková
a	a	k8xC	a
za	za	k7c4	za
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
uvádění	uvádění	k1gNnSc2	uvádění
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
objevily	objevit	k5eAaPmAgInP	objevit
i	i	k9	i
Naděžda	Naděžda	k1gFnSc1	Naděžda
Kniplová	Kniplová	k1gFnSc1	Kniplová
či	či	k8xC	či
Eva	Eva	k1gFnSc1	Eva
Děpoltová	Děpoltová	k1gFnSc1	Děpoltová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
uvádí	uvádět	k5eAaImIp3nS	uvádět
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
operu	oprat	k5eAaPmIp1nS	oprat
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Petra	Petr	k1gMnSc2	Petr
Novotného	Novotný	k1gMnSc2	Novotný
a	a	k8xC	a
hudebním	hudební	k2eAgNnSc6d1	hudební
nastudování	nastudování	k1gNnSc6	nastudování
Olivera	Oliver	k1gMnSc2	Oliver
Dohnányiho	Dohnányi	k1gMnSc2	Dohnányi
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Urbanovou	Urbanův	k2eAgFnSc7d1	Urbanova
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
premiérového	premiérový	k2eAgNnSc2d1	premiérové
obsazení	obsazení	k1gNnSc2	obsazení
<g/>
.	.	kIx.	.
<g/>
Nejbližší	blízký	k2eAgNnSc1d3	nejbližší
uvedení	uvedení	k1gNnSc1	uvedení
této	tento	k3xDgFnSc2	tento
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
opery	opera	k1gFnSc2	opera
je	být	k5eAaImIp3nS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2018	[number]	k4	2018
ku	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
100	[number]	k4	100
let	léto	k1gNnPc2	léto
výročí	výročí	k1gNnSc2	výročí
založení	založení	k1gNnSc2	založení
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
je	být	k5eAaImIp3nS	být
inscenačně	inscenačně	k6eAd1	inscenačně
nejnáročnější	náročný	k2eAgFnSc7d3	nejnáročnější
Smetanovou	Smetanův	k2eAgFnSc7d1	Smetanova
operou	opera	k1gFnSc7	opera
a	a	k8xC	a
i	i	k9	i
její	její	k3xOp3gInSc4	její
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
mimopražská	mimopražský	k2eAgNnPc4d1	mimopražské
česká	český	k2eAgNnPc4d1	české
jeviště	jeviště	k1gNnPc4	jeviště
byl	být	k5eAaImAgMnS	být
proto	proto	k8xC	proto
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ji	on	k3xPp3gFnSc4	on
poprvé	poprvé	k6eAd1	poprvé
uvedlo	uvést	k5eAaPmAgNnS	uvést
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
ji	on	k3xPp3gFnSc4	on
uvedl	uvést	k5eAaPmAgInS	uvést
soubor	soubor	k1gInSc1	soubor
Vendelína	Vendelín	k1gMnSc2	Vendelín
Budila	Budil	k1gMnSc2	Budil
poprvé	poprvé	k6eAd1	poprvé
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1902	[number]	k4	1902
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
otevření	otevření	k1gNnSc2	otevření
nové	nový	k2eAgFnSc2d1	nová
budovy	budova	k1gFnSc2	budova
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
P.	P.	kA	P.
Pražák	Pražák	k1gMnSc1	Pražák
napočítal	napočítat	k5eAaPmAgMnS	napočítat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
1947	[number]	k4	1947
<g/>
/	/	kIx~	/
<g/>
48	[number]	k4	48
celkem	celkem	k6eAd1	celkem
690	[number]	k4	690
představení	představení	k1gNnPc2	představení
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
20	[number]	k4	20
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
eviduje	evidovat	k5eAaImIp3nS	evidovat
databáze	databáze	k1gFnSc1	databáze
Divadelního	divadelní	k2eAgInSc2d1	divadelní
ústavu	ústav	k1gInSc2	ústav
30	[number]	k4	30
inscenací	inscenace	k1gFnPc2	inscenace
v	v	k7c6	v
tradičních	tradiční	k2eAgNnPc6d1	tradiční
divadlech	divadlo	k1gNnPc6	divadlo
<g/>
:	:	kIx,	:
vedle	vedle	k7c2	vedle
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc3	Liberec
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
a	a	k8xC	a
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
českých	český	k2eAgNnPc2d1	české
operních	operní	k2eAgNnPc2d1	operní
divadel	divadlo	k1gNnPc2	divadlo
tak	tak	k6eAd1	tak
nebyla	být	k5eNaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
titulní	titulní	k2eAgFnSc6d1	titulní
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
scénách	scéna	k1gFnPc6	scéna
objevily	objevit	k5eAaPmAgFnP	objevit
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
Květa	Květa	k1gFnSc1	Květa
Belanová	Belanová	k1gFnSc1	Belanová
<g/>
,	,	kIx,	,
Marcela	Marcela	k1gFnSc1	Marcela
Machotková	Machotková	k1gFnSc1	Machotková
<g/>
,	,	kIx,	,
Zdenka	Zdenka	k1gFnSc1	Zdenka
Kareninová	Kareninový	k2eAgFnSc1d1	Kareninová
<g/>
,	,	kIx,	,
Gita	Gita	k1gFnSc1	Gita
Abrahámová	Abrahámová	k1gFnSc1	Abrahámová
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
Hrubá	Hrubá	k1gFnSc1	Hrubá
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
Krásová	Krásová	k1gFnSc1	Krásová
nebo	nebo	k8xC	nebo
Marie	Marie	k1gFnSc1	Marie
Kremerová	Kremerová	k1gFnSc1	Kremerová
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
opera	opera	k1gFnSc1	opera
hrát	hrát	k5eAaImF	hrát
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
projevilo	projevit	k5eAaPmAgNnS	projevit
na	na	k7c4	na
šest	šest	k4xCc4	šest
tisíc	tisíc	k4xCgInPc2	tisíc
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
počasí	počasí	k1gNnSc2	počasí
se	se	k3xPyFc4	se
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
scéně	scéna	k1gFnSc6	scéna
představení	představení	k1gNnSc1	představení
neuskutečnilo	uskutečnit	k5eNaPmAgNnS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
pokus	pokus	k1gInSc1	pokus
–	–	k?	–
zařazený	zařazený	k2eAgInSc1d1	zařazený
do	do	k7c2	do
programu	program	k1gInSc2	program
Krajské	krajský	k2eAgFnSc2d1	krajská
slavnosti	slavnost	k1gFnSc2	slavnost
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
pro	pro	k7c4	pro
masové	masový	k2eAgNnSc4d1	masové
a	a	k8xC	a
na	na	k7c4	na
operu	opera	k1gFnSc4	opera
nezvyklé	zvyklý	k2eNgNnSc1d1	nezvyklé
publikum	publikum	k1gNnSc1	publikum
–	–	k?	–
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
nezdarem	nezdar	k1gInSc7	nezdar
(	(	kIx(	(
<g/>
Libuši	Libuše	k1gFnSc4	Libuše
zpívala	zpívat	k5eAaImAgFnS	zpívat
Marie	Marie	k1gFnSc1	Marie
Podvalová	Podvalová	k1gFnSc1	Podvalová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
Libuše	Libuše	k1gFnSc1	Libuše
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
různých	různý	k2eAgInPc2d1	různý
souborů	soubor	k1gInPc2	soubor
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
jediným	jediný	k2eAgNnSc7d1	jediné
stálým	stálý	k2eAgNnSc7d1	stálé
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tuto	tento	k3xDgFnSc4	tento
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
operu	opera	k1gFnSc4	opera
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
se	se	k3xPyFc4	se
však	však	k9	však
–	–	k?	–
i	i	k8xC	i
díky	díky	k7c3	díky
její	její	k3xOp3gFnSc3	její
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jediné	jediný	k2eAgFnSc3d1	jediná
představitelce	představitelka	k1gFnSc3	představitelka
Evě	Eva	k1gFnSc3	Eva
Urbanové	Urbanová	k1gFnSc3	Urbanová
–	–	k?	–
dostává	dostávat	k5eAaImIp3nS	dostávat
k	k	k7c3	k
divákům	divák	k1gMnPc3	divák
i	i	k8xC	i
mimo	mimo	k7c4	mimo
kamenná	kamenný	k2eAgNnPc4d1	kamenné
divadla	divadlo	k1gNnPc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2010	[number]	k4	2010
ji	on	k3xPp3gFnSc4	on
Originální	originální	k2eAgFnSc4d1	originální
hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
Praha	Praha	k1gFnSc1	Praha
uvedlo	uvést	k5eAaPmAgNnS	uvést
v	v	k7c6	v
Přírodním	přírodní	k2eAgNnSc6d1	přírodní
divadle	divadlo	k1gNnSc6	divadlo
Divoká	divoký	k2eAgFnSc1d1	divoká
Šárka	Šárka	k1gFnSc1	Šárka
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2012	[number]	k4	2012
zazněla	zaznět	k5eAaImAgFnS	zaznět
(	(	kIx(	(
<g/>
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
čtyřručního	čtyřruční	k2eAgInSc2d1	čtyřruční
klavíru	klavír	k1gInSc2	klavír
<g/>
)	)	kIx)	)
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Grabštejně	Grabštejně	k1gFnSc2	Grabštejně
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
slavnostně	slavnostně	k6eAd1	slavnostně
uvádí	uvádět	k5eAaImIp3nS	uvádět
operu	opera	k1gFnSc4	opera
Libuše	Libuše	k1gFnSc1	Libuše
Divadlo	divadlo	k1gNnSc1	divadlo
J.K.	J.K.	k1gMnSc2	J.K.
Tyla	Tyl	k1gMnSc2	Tyl
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
Novém	nový	k2eAgNnSc6d1	nové
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropského	evropský	k2eAgNnSc2d1	Evropské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
kultury	kultura	k1gFnSc2	kultura
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Dirigent	dirigent	k1gMnSc1	dirigent
<g/>
:	:	kIx,	:
Oliver	Oliver	k1gInSc1	Oliver
Dohnányi	Dohnányi	k1gNnSc2	Dohnányi
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
DJKT	DJKT	kA	DJKT
<g/>
;	;	kIx,	;
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Eva	Eva	k1gFnSc1	Eva
Urbanová	Urbanová	k1gFnSc1	Urbanová
v	v	k7c6	v
alternaci	alternace	k1gFnSc6	alternace
s	s	k7c7	s
Ivanou	Ivana	k1gFnSc7	Ivana
Veberovou	Veberová	k1gFnSc7	Veberová
(	(	kIx(	(
<g/>
Cena	cena	k1gFnSc1	cena
Thálie	Thálie	k1gFnSc1	Thálie
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mimo	mimo	k7c4	mimo
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
specifický	specifický	k2eAgInSc1d1	specifický
charakter	charakter	k1gInSc1	charakter
opery	opera	k1gFnSc2	opera
prakticky	prakticky	k6eAd1	prakticky
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
její	její	k3xOp3gNnSc4	její
uvádění	uvádění	k1gNnSc4	uvádění
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
Libuší	Libuše	k1gFnSc7	Libuše
může	moct	k5eAaImIp3nS	moct
proto	proto	k8xC	proto
seznámit	seznámit	k5eAaPmF	seznámit
jen	jen	k9	jen
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
nahrávek	nahrávka	k1gFnPc2	nahrávka
nebo	nebo	k8xC	nebo
řídkých	řídký	k2eAgNnPc2d1	řídké
pohostinských	pohostinský	k2eAgNnPc2d1	pohostinské
vystoupení	vystoupení	k1gNnPc2	vystoupení
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc7	první
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Libuši	Libuše	k1gFnSc4	Libuše
tam	tam	k6eAd1	tam
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
cyklu	cyklus	k1gInSc2	cyklus
všech	všecek	k3xTgFnPc2	všecek
Smetanových	Smetanových	k2eAgFnPc2d1	Smetanových
oper	opera	k1gFnPc2	opera
hrálo	hrát	k5eAaImAgNnS	hrát
české	český	k2eAgNnSc1d1	české
divadlo	divadlo	k1gNnSc1	divadlo
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
bylo	být	k5eAaImAgNnS	být
nastudování	nastudování	k1gNnSc1	nastudování
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
v	v	k7c6	v
záhřebském	záhřebský	k2eAgNnSc6d1	Záhřebské
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Unikátem	unikát	k1gInSc7	unikát
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
koncertní	koncertní	k2eAgNnSc1d1	koncertní
představení	představení	k1gNnSc1	představení
na	na	k7c6	na
divadelním	divadelní	k2eAgInSc6d1	divadelní
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Edinburgu	Edinburg	k1gInSc6	Edinburg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Urbanovou	Urbanová	k1gFnSc7	Urbanová
a	a	k8xC	a
Ivanem	Ivan	k1gMnSc7	Ivan
Kusnjerem	Kusnjer	k1gMnSc7	Kusnjer
(	(	kIx(	(
<g/>
dirigentem	dirigent	k1gMnSc7	dirigent
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
Oliver	Oliver	k1gMnSc1	Oliver
Dohnányi	Dohnány	k1gFnSc2	Dohnány
či	či	k8xC	či
v	v	k7c4	v
newyorské	newyorský	k2eAgFnPc4d1	newyorská
Carnegie	Carnegie	k1gFnPc4	Carnegie
Hall	Halla	k1gFnPc2	Halla
dirigované	dirigovaný	k2eAgFnSc3d1	dirigovaná
Eve	Eve	k1gFnSc3	Eve
Quelerovou	Quelerová	k1gFnSc4	Quelerová
s	s	k7c7	s
Gabrielou	Gabriela	k1gFnSc7	Gabriela
Beňačkovou	Beňačkův	k2eAgFnSc7d1	Beňačkův
v	v	k7c6	v
titulní	titulní	k2eAgFnSc6d1	titulní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
období	období	k1gNnSc6	období
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
Libuše	Libuše	k1gFnSc1	Libuše
ovšem	ovšem	k9	ovšem
hrána	hrát	k5eAaImNgFnS	hrát
i	i	k9	i
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
Slovenském	slovenský	k2eAgNnSc6d1	slovenské
národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nastudována	nastudován	k2eAgFnSc1d1	nastudována
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nahrávky	nahrávka	k1gFnSc2	nahrávka
==	==	k?	==
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
byla	být	k5eAaImAgFnS	být
několikrát	několikrát	k6eAd1	několikrát
vcelku	vcelku	k6eAd1	vcelku
nahrána	nahrát	k5eAaBmNgFnS	nahrát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
<g/>
:	:	kIx,	:
dirigent	dirigent	k1gMnSc1	dirigent
Alois	Alois	k1gMnSc1	Alois
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
Marie	Marie	k1gFnSc1	Marie
Podvalová	Podvalová	k1gFnSc1	Podvalová
<g/>
,	,	kIx,	,
Teodor	Teodor	k1gMnSc1	Teodor
Šrubař	Šrubař	k1gMnSc1	Šrubař
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Kalaš	Kalaš	k1gMnSc1	Kalaš
<g/>
,	,	kIx,	,
Beno	Beno	k1gMnSc1	Beno
Blachut	Blachut	k1gMnSc1	Blachut
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Veverka	Veverka	k1gMnSc1	Veverka
<g/>
,	,	kIx,	,
Borek	Borek	k1gMnSc1	Borek
Rujan	Rujan	k1gMnSc1	Rujan
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Červinková	Červinková	k1gFnSc1	Červinková
<g/>
,	,	kIx,	,
Marta	Marta	k1gFnSc1	Marta
<g />
.	.	kIx.	.
</s>
<s>
Krásová	Krásová	k1gFnSc1	Krásová
<g/>
,	,	kIx,	,
Miluše	Miluše	k1gFnSc1	Miluše
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
<g/>
,	,	kIx,	,
Miloslava	Miloslava	k1gFnSc1	Miloslava
Fidlerová	Fidlerová	k1gFnSc1	Fidlerová
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
Krilová	Krilová	k1gFnSc1	Krilová
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Gleich	Gleich	k1gMnSc1	Gleich
<g/>
;	;	kIx,	;
Cantus	Cantus	k1gMnSc1	Cantus
Classics	Classics	k1gInSc4	Classics
500168	[number]	k4	500168
(	(	kIx(	(
<g/>
2	[number]	k4	2
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
163	[number]	k4	163
<g/>
'	'	kIx"	'
<g/>
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EDITIO	EDITIO	kA	EDITIO
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
CR	cr	k0	cr
0	[number]	k4	0
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
633	[number]	k4	633
<g/>
,	,	kIx,	,
2	[number]	k4	2
CD	CD	kA	CD
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
167	[number]	k4	167
<g/>
'	'	kIx"	'
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
<g/>
:	:	kIx,	:
dirigent	dirigent	k1gMnSc1	dirigent
Alois	Alois	k1gMnSc1	Alois
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Československého	československý	k2eAgInSc2d1	československý
rozhlasu	rozhlas	k1gInSc2	rozhlas
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
Drahomíra	Drahomíra	k1gFnSc1	Drahomíra
Tikalová	Tikalová	k1gFnSc1	Tikalová
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Jindrák	Jindrák	k1gMnSc1	Jindrák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Žídek	Žídek	k1gMnSc1	Žídek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Berman	Berman	k1gMnSc1	Berman
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Joran	Joran	k1gMnSc1	Joran
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
Šubrtová	Šubrtová	k1gFnSc1	Šubrtová
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
Mixová	Mixová	k1gFnSc1	Mixová
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g />
.	.	kIx.	.
</s>
<s>
Tattermuschová	Tattermuschová	k1gFnSc1	Tattermuschová
<g/>
,	,	kIx,	,
Sylvie	Sylvie	k1gFnSc1	Sylvie
Kodetová	Kodetový	k2eAgFnSc1d1	Kodetová
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Hlobilová	Hlobilová	k1gFnSc1	Hlobilová
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Votava	Votava	k1gMnSc1	Votava
(	(	kIx(	(
<g/>
167	[number]	k4	167
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
nevydána	vydán	k2eNgFnSc1d1	nevydána
<g/>
)	)	kIx)	)
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
<g/>
:	:	kIx,	:
dirigent	dirigent	k1gMnSc1	dirigent
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Krombholc	Krombholc	k1gFnSc1	Krombholc
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
Naděžda	Naděžda	k1gFnSc1	Naděžda
Kniplová	Kniplová	k1gFnSc1	Kniplová
<g/>
,	,	kIx,	,
Vacláv	Vacláva	k1gFnPc2	Vacláva
Bednář	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kroupa	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Žídek	Žídek	k1gMnSc1	Žídek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Berman	Berman	k1gMnSc1	Berman
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Jindrák	Jindrák	k1gMnSc1	Jindrák
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Milada	Milada	k1gFnSc1	Milada
Šubrtová	Šubrtová	k1gFnSc1	Šubrtová
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
Soukupová	Soukupová	k1gFnSc1	Soukupová
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
Tattermuschová	Tattermuschová	k1gFnSc1	Tattermuschová
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Vymazalová	Vymazalová	k1gFnSc1	Vymazalová
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Hanzalíková	Hanzalíková	k1gFnSc1	Hanzalíková
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Votava	Votava	k1gMnSc1	Votava
<g/>
;	;	kIx,	;
Supraphon	supraphon	k1gInSc1	supraphon
1116	[number]	k4	1116
4321-33	[number]	k4	4321-33
(	(	kIx(	(
<g/>
LP	LP	kA	LP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
SU	SU	k?	SU
3982-2	[number]	k4	3982-2
(	(	kIx(	(
<g/>
2	[number]	k4	2
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
76	[number]	k4	76
<g/>
'	'	kIx"	'
<g/>
31	[number]	k4	31
+	+	kIx~	+
80	[number]	k4	80
<g/>
'	'	kIx"	'
<g/>
31	[number]	k4	31
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1983	[number]	k4	1983
<g/>
:	:	kIx,	:
dirigent	dirigent	k1gMnSc1	dirigent
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Košler	Košler	k1gMnSc1	Košler
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
Gabriela	Gabriela	k1gFnSc1	Gabriela
Beňačková-Čápová	Beňačková-Čápová	k1gFnSc1	Beňačková-Čápová
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Zítek	Zítek	k1gMnSc1	Zítek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Švorc	Švorc	k?	Švorc
<g/>
,	,	kIx,	,
Leo	Leo	k1gMnSc1	Leo
Marian	Marian	k1gMnSc1	Marian
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Průša	Průša	k1gMnSc1	Průša
<g/>
,	,	kIx,	,
René	René	k1gMnSc1	René
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Děpoltová	Děpoltová	k1gFnSc1	Děpoltová
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g />
.	.	kIx.	.
</s>
<s>
Soukupová	Soukupová	k1gFnSc1	Soukupová
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Jonášová	Jonášová	k1gFnSc1	Jonášová
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Marková	Marková	k1gFnSc1	Marková
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Veselá	Veselá	k1gFnSc1	Veselá
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Švejda	Švejda	k1gMnSc1	Švejda
<g/>
;	;	kIx,	;
Supraphon	supraphon	k1gInSc1	supraphon
1116	[number]	k4	1116
4211-14	[number]	k4	4211-14
(	(	kIx(	(
<g/>
3	[number]	k4	3
LP	LP	kA	LP
<g/>
)	)	kIx)	)
/	/	kIx~	/
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Supraphon	supraphon	k1gInSc1	supraphon
11	[number]	k4	11
1276-2	[number]	k4	1276-2
633	[number]	k4	633
(	(	kIx(	(
<g/>
3	[number]	k4	3
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
165	[number]	k4	165
<g/>
'	'	kIx"	'
<g/>
44	[number]	k4	44
-	-	kIx~	-
live	liv	k1gInSc2	liv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
dirigent	dirigent	k1gMnSc1	dirigent
Oliver	Oliver	k1gMnSc1	Oliver
Dohnányi	Dohnánye	k1gFnSc4	Dohnánye
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
Eva	Eva	k1gFnSc1	Eva
Urbanová	Urbanová	k1gFnSc1	Urbanová
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Vele	velo	k1gNnSc6	velo
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Markvart	Markvart	k1gInSc1	Markvart
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
Podskalský	podskalský	k2eAgMnSc1d1	podskalský
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
Červinka	Červinka	k1gMnSc1	Červinka
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
Kaupová	Kaupová	k1gFnSc1	Kaupová
<g/>
,	,	kIx,	,
Miroslava	Miroslava	k1gFnSc1	Miroslava
Volková	Volková	k1gFnSc1	Volková
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Jonášová	Jonášová	k1gFnSc1	Jonášová
<g/>
,	,	kIx,	,
Jitka	Jitka	k1gFnSc1	Jitka
Soběhartová	Soběhartová	k1gFnSc1	Soběhartová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Veselá	Veselá	k1gFnSc1	Veselá
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Švejda	Švejda	k1gMnSc1	Švejda
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Supraphon	supraphon	k1gInSc1	supraphon
SU	SU	k?	SU
3200-2	[number]	k4	3200-2
632	[number]	k4	632
(	(	kIx(	(
<g/>
2	[number]	k4	2
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
150	[number]	k4	150
<g/>
'	'	kIx"	'
<g/>
37	[number]	k4	37
-	-	kIx~	-
live	live	k1gNnSc4	live
<g/>
,	,	kIx,	,
kráceno	krácen	k2eAgNnSc4d1	kráceno
<g/>
)	)	kIx)	)
<g/>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
několik	několik	k4yIc1	několik
televizních	televizní	k2eAgFnPc2d1	televizní
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
záznamu	záznam	k1gInSc6	záznam
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
uvedení	uvedení	k1gNnSc2	uvedení
inscenace	inscenace	k1gFnSc2	inscenace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
s	s	k7c7	s
Evou	Eva	k1gFnSc7	Eva
Urbanovou	Urbanův	k2eAgFnSc7d1	Urbanova
v	v	k7c6	v
titulní	titulní	k2eAgFnSc6d1	titulní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Libuše	Libuše	k1gFnSc2	Libuše
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Wenzig	Wenzig	k1gMnSc1	Wenzig
</s>
</p>
<p>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
zelenohorský	zelenohorský	k2eAgInSc1d1	zelenohorský
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SMETANA	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
.	.	kIx.	.
</s>
<s>
Studijní	studijní	k2eAgNnSc1d1	studijní
vydání	vydání	k1gNnSc1	vydání
děl	dělo	k1gNnPc2	dělo
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
(	(	kIx(	(
<g/>
kritická	kritický	k2eAgFnSc1d1	kritická
edice	edice	k1gFnSc1	edice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Společnost	společnost	k1gFnSc1	společnost
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NEJEDLÝ	Nejedlý	k1gMnSc1	Nejedlý
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvohry	zpěvohra	k1gFnPc1	zpěvohra
Smetanovy	Smetanův	k2eAgFnPc1d1	Smetanova
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OČADLÍK	OČADLÍK	kA	OČADLÍK
<g/>
,	,	kIx,	,
Mirko	Mirko	k1gMnSc1	Mirko
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
:	:	kIx,	:
vznik	vznik	k1gInSc1	vznik
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
zpěvohry	zpěvohra	k1gFnSc2	zpěvohra
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
202	[number]	k4	202
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PRAŽÁK	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovy	Smetanův	k2eAgFnPc1d1	Smetanova
zpěvohry	zpěvohra	k1gFnPc1	zpěvohra
II	II	kA	II
:	:	kIx,	:
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
s.	s.	k?	s.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
255	[number]	k4	255
s.	s.	k?	s.
</s>
</p>
<p>
<s>
JIRÁNEK	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
hudebně	hudebně	k6eAd1	hudebně
dramatické	dramatický	k2eAgFnSc2d1	dramatická
reprezentace	reprezentace	k1gFnSc2	reprezentace
Krasavy	Krasava	k1gFnSc2	Krasava
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgFnSc6d1	Smetanova
Libuši	Libuše	k1gFnSc6	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
250	[number]	k4	250
<g/>
-	-	kIx~	-
<g/>
273	[number]	k4	273
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
7003	[number]	k4	7003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁNEK	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Krystalizace	krystalizace	k1gFnSc1	krystalizace
významového	významový	k2eAgNnSc2d1	významové
pole	pole	k1gNnSc2	pole
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
Libuše	Libuše	k1gFnSc2	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
54	[number]	k4	54
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
7003	[number]	k4	7003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VÍT	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
:	:	kIx,	:
proměny	proměna	k1gFnSc2	proměna
mýtu	mýtus	k1gInSc2	mýtus
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
269	[number]	k4	269
<g/>
-	-	kIx~	-
<g/>
273	[number]	k4	273
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
7003	[number]	k4	7003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIRÁNEK	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
operní	operní	k2eAgFnSc1d1	operní
tvorba	tvorba	k1gFnSc1	tvorba
I.	I.	kA	I.
Od	od	k7c2	od
Braniborů	Branibor	k1gMnPc2	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
k	k	k7c3	k
Libuši	Libuše	k1gFnSc3	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Editio	Editio	k6eAd1	Editio
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OTTLOVÁ	OTTLOVÁ	kA	OTTLOVÁ
<g/>
,	,	kIx,	,
Marta	Marta	k1gFnSc1	Marta
<g/>
;	;	kIx,	;
POSPÍŠIL	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
150	[number]	k4	150
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
256	[number]	k4	256
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
historismus	historismus	k1gInSc1	historismus
<g/>
:	:	kIx,	:
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
,	,	kIx,	,
s.	s.	k?	s.
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TYRRELL	TYRRELL	kA	TYRRELL
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Czech	Czech	k1gMnSc1	Czech
Opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
34713	[number]	k4	34713
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
JANOTA	Janota	k1gMnSc1	Janota
<g/>
,	,	kIx,	,
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
;	;	kIx,	;
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
P.	P.	kA	P.
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
české	český	k2eAgFnSc2d1	Česká
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
347	[number]	k4	347
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
236	[number]	k4	236
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
,	,	kIx,	,
s.	s.	k?	s.
153	[number]	k4	153
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Penguin	Penguin	k1gInSc1	Penguin
Concise	Concise	k1gFnSc1	Concise
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Penguin	Penguin	k2eAgInSc1d1	Penguin
Books	Books	k1gInSc1	Books
Ltd	ltd	kA	ltd
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
593	[number]	k4	593
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
141	[number]	k4	141
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1682	[number]	k4	1682
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Tyrrell	Tyrrell	k1gMnSc1	Tyrrell
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
:	:	kIx,	:
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
s.	s.	k?	s.
411	[number]	k4	411
<g/>
-	-	kIx~	-
<g/>
414	[number]	k4	414
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WENZIG	WENZIG	kA	WENZIG
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
ŠPINDLER	Špindler	k1gMnSc1	Špindler
<g/>
,	,	kIx,	,
Ervín	Ervín	k1gMnSc1	Ervín
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Úvod	úvod	k1gInSc1	úvod
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOSTOMSKÁ	HOSTOMSKÁ	kA	HOSTOMSKÁ
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
–	–	k?	–
Průvodce	průvodka	k1gFnSc6	průvodka
operní	operní	k2eAgFnSc7d1	operní
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NS	NS	kA	NS
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
1466	[number]	k4	1466
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
637	[number]	k4	637
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
674	[number]	k4	674
<g/>
–	–	k?	–
<g/>
677	[number]	k4	677
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
public	publicum	k1gNnPc2	publicum
domain	domain	k1gMnSc1	domain
hudebnin	hudebnina	k1gFnPc2	hudebnina
IMSLP	IMSLP	kA	IMSLP
(	(	kIx(	(
<g/>
Petrucci	Petrucce	k1gFnSc4	Petrucce
Music	Musice	k1gFnPc2	Musice
Library	Librara	k1gFnSc2	Librara
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
partitura	partitura	k1gFnSc1	partitura
a	a	k8xC	a
klavírní	klavírní	k2eAgInSc1d1	klavírní
výtah	výtah	k1gInSc1	výtah
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
-	-	kIx~	-
ND	ND	kA	ND
1.1	[number]	k4	1.1
<g/>
.2007	.2007	k4	.2007
[	[	kIx(	[
<g/>
Reportáž	reportáž	k1gFnSc1	reportáž
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Hulka	Hulka	k1gFnSc1	Hulka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc6	jednání
</s>
</p>
<p>
<s>
WENZIG	WENZIG	kA	WENZIG
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
:	:	kIx,	:
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
zpěvohra	zpěvohra	k1gFnSc1	zpěvohra
o	o	k7c6	o
3	[number]	k4	3
jednáních	jednání	k1gNnPc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Ervín	Ervín	k1gMnSc1	Ervín
Špindler	Špindler	k1gMnSc1	Špindler
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Fr.	Fr.	k1gMnSc1	Fr.
A.	A.	kA	A.
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
38	[number]	k4	38
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc1	libreto
<g/>
.	.	kIx.	.
</s>
</p>
