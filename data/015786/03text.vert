<s>
Boje	boj	k1gInPc1
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tažení	tažení	k1gNnSc1
v	v	k7c6
Nizozemské	nizozemský	k2eAgFnSc6d1
východní	východní	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Japonští	japonský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
se	se	k3xPyFc4
vyloďují	vyloďovat	k5eAaImIp3nP
na	na	k7c6
ostrově	ostrov	k1gInSc6
Java	Jav	k1gInSc2
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
<g/>
,	,	kIx,
1941	#num#	k4
–	–	k?
9	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
<g/>
,	,	kIx,
1942	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Nizozemská	nizozemský	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
Okupace	okupace	k1gFnSc2
téměř	téměř	k6eAd1
celé	celý	k2eAgFnSc2d1
Nizozemské	nizozemský	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
ABDACOM	ABDACOM	kA
Nizozemské	nizozemský	k2eAgNnSc1d1
království	království	k1gNnSc1
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
Austrálie	Austrálie	k1gFnSc2
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Alidius	Alidius	k1gMnSc1
Tjarda	Tjarda	k1gMnSc1
van	vana	k1gFnPc2
Starkenborgh	Starkenborgh	k1gMnSc1
Stachouwer	Stachouwer	k1gMnSc1
Archibald	Archibald	k1gMnSc1
Wavell	Wavell	k1gMnSc1
George	Georg	k1gMnSc2
Brett	Brett	k1gMnSc1
Henry	Henry	k1gMnSc1
Royds	Roydsa	k1gFnPc2
Pownall	Pownall	k1gMnSc1
Thomas	Thomas	k1gMnSc1
C.	C.	kA
Hart	Hart	k1gMnSc1
Hein	Hein	k1gMnSc1
ter	ter	k?
Poorten	Poorten	k2eAgMnSc1d1
Richard	Richard	k1gMnSc1
Peirse	Peirse	k1gFnSc2
Conrad	Conrad	k1gInSc1
Emil	Emil	k1gMnSc1
Lambert	Lambert	k1gMnSc1
Helfrich	Helfrich	k1gMnSc1
</s>
<s>
Hisaiči	Hisaič	k1gMnPc1
Terauči	Terauč	k1gFnSc3
Kijotake	Kijotak	k1gInSc2
Kawaguči	Kawaguč	k1gFnSc6
Ibó	Ibó	k1gFnSc6
Takahaši	Takahaše	k1gFnSc4
Hitoši	Hitoš	k1gMnSc3
Imamura	Imamur	k1gMnSc2
Šódži	Šódž	k1gFnSc6
Nišimura	Nišimura	k1gFnSc1
Masao	Masao	k1gNnSc4
Marujama	Marujam	k1gMnSc2
Takeo	Takeo	k1gMnSc1
Itó	Itó	k1gMnSc1
Šizuo	Šizuo	k1gMnSc1
Sakaguči	Sakaguč	k1gFnSc3
Júicu	Júic	k2eAgFnSc4d1
Cučihaši	Cučihaše	k1gFnSc4
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
33	#num#	k4
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
41	#num#	k4
ponorek	ponorka	k1gFnPc2
<g/>
234	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
67	#num#	k4
000	#num#	k4
nizozemských	nizozemský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
8	#num#	k4
000	#num#	k4
anglo-amerických	anglo-americký	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
</s>
<s>
52	#num#	k4
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
18	#num#	k4
ponorek	ponorka	k1gFnPc2
<g/>
50	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
62	#num#	k4
116	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
383	#num#	k4
zabito	zabít	k5eAaPmNgNnS
<g/>
59	#num#	k4
733	#num#	k4
zajato	zajat	k2eAgNnSc4d1
</s>
<s>
671	#num#	k4
zabito	zabít	k5eAaPmNgNnS
</s>
<s>
Tažení	tažení	k1gNnSc1
v	v	k7c6
Nizozemské	nizozemský	k2eAgFnSc6d1
východní	východní	k2eAgFnSc6d1
Indii	Indie	k1gFnSc6
byla	být	k5eAaImAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
vedená	vedený	k2eAgFnSc1d1
Japonci	Japonec	k1gMnPc1
proti	proti	k7c3
Nizozemské	nizozemský	k2eAgFnSc3d1
východní	východní	k2eAgFnSc3d1
Indii	Indie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
tažení	tažení	k1gNnSc2
bylo	být	k5eAaImAgNnS
ovládnout	ovládnout	k5eAaPmF
souostroví	souostroví	k1gNnSc2
a	a	k8xC
tamější	tamější	k2eAgFnSc2d1
bohaté	bohatý	k2eAgFnSc2d1
nerostné	nerostný	k2eAgFnSc2d1
suroviny	surovina	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
ropu	ropa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japoncům	Japonec	k1gMnPc3
se	se	k3xPyFc4
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3
nizozemskou	nizozemský	k2eAgFnSc4d1
kolonii	kolonie	k1gFnSc4
podařilo	podařit	k5eAaPmAgNnS
dobýt	dobýt	k5eAaPmF
de	de	k?
facto	fact	k2eAgNnSc1d1
do	do	k7c2
března	březen	k1gInSc2
1942	#num#	k4
za	za	k7c4
cenu	cena	k1gFnSc4
„	„	k?
<g/>
pouhých	pouhý	k2eAgInPc2d1
<g/>
“	“	k?
671	#num#	k4
padlých	padlý	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k6eAd1
tomu	ten	k3xDgMnSc3
Spojenci	spojenec	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
více	hodně	k6eAd2
než	než	k8xS
2380	#num#	k4
mrtvých	mrtvý	k1gMnPc2
a	a	k8xC
o	o	k7c4
59	#num#	k4
733	#num#	k4
zajatých	zajatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
japonském	japonský	k2eAgInSc6d1
úvodním	úvodní	k2eAgInSc6d1
útoku	útok	k1gInSc6
na	na	k7c4
Borneo	Borneo	k1gNnSc4
<g/>
,	,	kIx,
Celebes	Celebes	k1gInSc1
a	a	k8xC
Moluky	Moluky	k1gFnPc1
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
v	v	k7c6
únoru	únor	k1gInSc6
1942	#num#	k4
vylodili	vylodit	k5eAaPmAgMnP
na	na	k7c6
Sumatře	Sumatra	k1gFnSc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
ohrozili	ohrozit	k5eAaPmAgMnP
západní	západní	k2eAgFnSc4d1
Jávu	Jáva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následné	následný	k2eAgInPc1d1
vylodění	vylodění	k1gNnSc4
na	na	k7c6
Bali	Bal	k1gInSc6
a	a	k8xC
Timoru	Timor	k1gInSc6
odřízlo	odříznout	k5eAaPmAgNnS
Jávu	Jáva	k1gFnSc4
od	od	k7c2
Austrálie	Austrálie	k1gFnSc2
a	a	k8xC
ohrozilo	ohrozit	k5eAaPmAgNnS
východní	východní	k2eAgFnSc4d1
Jávu	Jáva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyvrcholením	vyvrcholení	k1gNnSc7
kampaně	kampaň	k1gFnSc2
byla	být	k5eAaImAgFnS
námořní	námořní	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
v	v	k7c6
Jávském	jávský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
a	a	k8xC
japonské	japonský	k2eAgNnSc1d1
vylodění	vylodění	k1gNnSc1
na	na	k7c6
Jávě	Jáva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
pohledu	pohled	k1gInSc2
Japonců	Japonec	k1gMnPc2
to	ten	k3xDgNnSc4
bylo	být	k5eAaImAgNnS
kolosální	kolosální	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
<g/>
,	,	kIx,
z	z	k7c2
pohledu	pohled	k1gInSc2
Spojenců	spojenec	k1gMnPc2
a	a	k8xC
obzvláště	obzvláště	k6eAd1
Nizozemců	Nizozemec	k1gMnPc2
katastrofa	katastrofa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nizozemci	Nizozemec	k1gMnSc6
touto	tento	k3xDgFnSc7
porážkou	porážka	k1gFnSc7
definitivně	definitivně	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
pozici	pozice	k1gFnSc4
někdejší	někdejší	k2eAgFnSc2d1
světové	světový	k2eAgFnSc2d1
velmoci	velmoc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
kolonii	kolonie	k1gFnSc4
Spojenců	spojenec	k1gMnPc2
dobytou	dobytý	k2eAgFnSc4d1
Japonci	Japonec	k1gMnPc1
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
japonských	japonský	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
je	být	k5eAaImIp3nS
rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
uváděno	uvádět	k5eAaImNgNnS
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
a	a	k8xC
rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
na	na	k7c6
druhém	druhý	k4xOgNnSc6
</s>
<s>
Při	při	k7c6
přepisu	přepis	k1gInSc6
japonštiny	japonština	k1gFnSc2
byla	být	k5eAaImAgFnS
použita	použít	k5eAaPmNgFnS
česká	český	k2eAgFnSc1d1
transkripce	transkripce	k1gFnSc1
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
Válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
<g/>
,	,	kIx,
Bitva	bitva	k1gFnSc1
o	o	k7c6
Nizozemsko	Nizozemsko	k1gNnSc1
a	a	k8xC
Nizozemská	nizozemský	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
bylo	být	k5eAaImAgNnS
v	v	k7c6
květnu	květen	k1gInSc6
1940	#num#	k4
Nizozemsko	Nizozemsko	k1gNnSc1
obsazeno	obsazen	k2eAgNnSc1d1
Německem	Německo	k1gNnSc7
<g/>
,	,	kIx,
zůstaly	zůstat	k5eAaPmAgFnP
nizozemské	nizozemský	k2eAgFnPc1d1
kolonie	kolonie	k1gFnPc1
osamocené	osamocený	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
věrné	věrný	k2eAgFnSc6d1
nizozemské	nizozemský	k2eAgFnSc6d1
exilové	exilový	k2eAgFnSc6d1
vládě	vláda	k1gFnSc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
na	na	k7c4
hrozbu	hrozba	k1gFnSc4
útoku	útok	k1gInSc2
pomocných	pomocný	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
zůstala	zůstat	k5eAaPmAgFnS
Nizozemská	nizozemský	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
Indie	Indie	k1gFnSc1
mimo	mimo	k7c4
dosah	dosah	k1gInSc4
nacistického	nacistický	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
a	a	k8xC
fašistické	fašistický	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
japonském	japonský	k2eAgNnSc6d1
císařství	císařství	k1gNnSc6
ale	ale	k8xC
po	po	k7c6
debaklu	debakl	k1gInSc6
u	u	k7c2
Chalchyn	Chalchyna	k1gFnPc2
golu	golu	k5eAaPmIp1nS
v	v	k7c6
létě	léto	k1gNnSc6
1939	#num#	k4
získala	získat	k5eAaPmAgFnS
nad	nad	k7c7
frakcí	frakce	k1gFnSc7
hokušin	hokušin	k2eAgInSc1d1
ron	ron	k1gInSc1
(	(	kIx(
<g/>
北	北	k?
~	~	kIx~
doktrína	doktrína	k1gFnSc1
severní	severní	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
<g/>
)	)	kIx)
převahu	převaha	k1gFnSc4
frakce	frakce	k1gFnSc2
nanšin	nanšin	k2eAgInSc1d1
ron	ron	k1gInSc1
(	(	kIx(
<g/>
南	南	k?
~	~	kIx~
doktrína	doktrína	k1gFnSc1
jižní	jižní	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
obhajovala	obhajovat	k5eAaImAgFnS
útok	útok	k1gInSc4
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c7
účelem	účel	k1gInSc7
obsazení	obsazení	k1gNnSc2
nerostných	nerostný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
britského	britský	k2eAgNnSc2d1
Malajska	Malajsko	k1gNnSc2
<g/>
,	,	kIx,
britského	britský	k2eAgNnSc2d1
Bornea	Borneo	k1gNnSc2
a	a	k8xC
ostrovů	ostrov	k1gInPc2
Nizozemské	nizozemský	k2eAgFnSc2d1
východní	východní	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Dutch	Dutch	k1gMnSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
campaign	campaign	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ALFORD	ALFORD	kA
<g/>
,	,	kIx,
Lodwick	Lodwick	k1gInSc1
H.	H.	kA
Playing	Playing	k1gInSc1
for	forum	k1gNnPc2
Time	Tim	k1gFnSc2
<g/>
:	:	kIx,
War	War	k1gMnSc1
on	on	k3xPp3gMnSc1
an	an	k?
Asiatic	Asiatice	k1gFnPc2
Fleet	Fleet	k1gMnSc1
Destroyer	Destroyer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bennington	Bennington	k1gInSc1
<g/>
,	,	kIx,
Vermont	Vermont	k1gInSc1
<g/>
:	:	kIx,
Merriam	Merriam	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4357	#num#	k4
<g/>
-	-	kIx~
<g/>
5548	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BARTSCH	BARTSCH	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
H.	H.	kA
Every	Evera	k1gMnSc2
Day	Day	k1gMnSc2
A	a	k9
Nightmare	Nightmar	k1gMnSc5
<g/>
:	:	kIx,
American	American	k1gInSc4
Pursuit	Pursuit	k2eAgInSc4d1
Pilots	Pilots	k1gInSc4
in	in	k?
the	the	k?
Defense	defense	k1gFnSc2
of	of	k?
Java	Java	k1gMnSc1
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1942	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Texas	Texas	k1gInSc1
A	A	kA
<g/>
&	&	k?
<g/>
M	M	kA
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
60344	#num#	k4
<g/>
-	-	kIx~
<g/>
176	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BOER	BOER	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
C.	C.	kA
The	The	k1gFnPc2
Loss	Lossa	k1gFnPc2
of	of	k?
Java	Java	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Final	Final	k1gMnSc1
Battles	Battles	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
Possession	Possession	k1gInSc1
of	of	k?
Java	Java	k1gFnSc1
Fought	Fought	k1gInSc1
by	by	kYmCp3nS
Allied	Allied	k1gInSc4
Air	Air	k1gFnPc2
<g/>
,	,	kIx,
Naval	navalit	k5eAaPmRp2nS
and	and	k?
Land	Land	k1gMnSc1
Forces	Forces	k1gMnSc1
in	in	k?
the	the	k?
Period	perioda	k1gFnPc2
of	of	k?
18	#num#	k4
February-	February-	k1gFnSc1
<g/>
7	#num#	k4
March	March	k1gInSc1
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Singapore	Singapor	k1gInSc5
<g/>
:	:	kIx,
NUS	NUS	kA
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
9971	#num#	k4
<g/>
-	-	kIx~
<g/>
69	#num#	k4
<g/>
-	-	kIx~
<g/>
513	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
COX	COX	kA
<g/>
,	,	kIx,
Jeffrey	Jeffrea	k1gMnSc2
R.	R.	kA
Rising	Rising	k1gInSc1
Sun	Sun	kA
<g/>
,	,	kIx,
Falling	Falling	k1gInSc1
Skies	Skies	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Disastrous	Disastrous	k1gMnSc1
Java	Java	k1gMnSc1
Sea	Sea	k1gMnSc1
Campaign	Campaign	k1gMnSc1
of	of	k?
World	World	k1gMnSc1
War	War	k1gFnSc3
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Osprey	Osprea	k1gFnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
78096	#num#	k4
<g/>
-	-	kIx~
<g/>
726	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
DULL	DULL	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
S.	S.	kA
A	a	k9
Battle	Battle	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Imperial	Imperial	k1gInSc1
Japanese	Japanese	k1gFnSc1
Navy	Navy	k?
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1591142199	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HARA	HARA	kA
<g/>
,	,	kIx,
Tameichi	Tameichi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japanese	Japanese	k1gFnSc1
Destroyer	Destroyra	k1gFnPc2
Captain	Captain	k1gMnSc1
(	(	kIx(
<g/>
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
,	,	kIx,
Guadalcanal	Guadalcanal	k1gMnSc1
<g/>
,	,	kIx,
Midway	Midwaa	k1gFnPc1
–	–	k?
The	The	k1gMnSc1
Great	Great	k2eAgMnSc1d1
Naval	navalit	k5eAaPmRp2nS
battles	battles	k1gMnSc1
as	as	k1gNnSc2
Seen	Seen	k1gMnSc1
Through	Through	k1gMnSc1
Japanese	Japanese	k1gFnSc2
Eyes	Eyes	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1591143543	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
JUNGSLAGER	JUNGSLAGER	kA
<g/>
,	,	kIx,
Gerard	Gerard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forlorn	Forlorn	k1gInSc1
Hope	Hope	k1gInSc4
<g/>
:	:	kIx,
The	The	k1gMnSc5
desperate	desperat	k1gMnSc5
battle	battle	k1gFnSc2
of	of	k?
the	the	k?
Dutch	Dutch	k1gInSc1
against	against	k1gInSc1
Japan	japan	k1gInSc4
<g/>
,	,	kIx,
December	December	k1gInSc4
1941	#num#	k4
<g/>
-March	-March	k1gInSc1
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amsterdam	Amsterdam	k1gInSc1
<g/>
:	:	kIx,
De	De	k?
Bataafsche	Bataafsch	k1gMnPc4
Leeuw	Leeuw	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
906707660	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KEHN	KEHN	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
M.	M.	kA
(	(	kIx(
<g/>
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Blue	Blue	k1gInSc1
Sea	Sea	k1gFnSc2
of	of	k?
Blood	Blood	k1gInSc1
<g/>
:	:	kIx,
Deciphering	Deciphering	k1gInSc1
the	the	k?
Mysterious	Mysterious	k1gInSc1
Fate	Fate	k1gFnSc1
of	of	k?
the	the	k?
USS	USS	kA
Edsall	Edsall	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
St.	st.	kA
Paul	Paula	k1gFnPc2
<g/>
,	,	kIx,
MN	MN	kA
<g/>
:	:	kIx,
Zenith	Zenith	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7603	#num#	k4
<g/>
-	-	kIx~
<g/>
3353	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KELLY	KELLY	kA
<g/>
,	,	kIx,
Terence	Terence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hurricanes	Hurricanes	k1gInSc1
Versus	versus	k7c1
Zeros	Zeros	k1gMnSc1
<g/>
:	:	kIx,
Air	Air	k1gMnSc1
Battles	Battles	k1gMnSc1
over	over	k1gMnSc1
Singapore	Singapor	k1gInSc5
<g/>
,	,	kIx,
Sumatra	Sumatra	k1gFnSc1
&	&	k?
Java	Java	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barnsley	Barnslea	k1gFnPc1
<g/>
:	:	kIx,
Pen	Pen	k1gFnSc1
&	&	k?
Sword	Sword	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84415	#num#	k4
<g/>
-	-	kIx~
<g/>
622	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MESSIMER	MESSIMER	kA
<g/>
,	,	kIx,
Dwight	Dwight	k1gMnSc1
R.	R.	kA
In	In	k1gMnSc1
the	the	k?
hands	hands	k1gInSc1
of	of	k?
fate	fate	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
story	story	k1gFnSc2
of	of	k?
Patrol	Patrol	k?
Wing	Wing	k1gInSc1
Ten	ten	k3xDgInSc1
<g/>
,	,	kIx,
8	#num#	k4
December	December	k1gInSc1
1941-11	1941-11	k4
May	May	k1gMnSc1
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781557505477	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SHORES	SHORES	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
CULL	CULL	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
;	;	kIx,
IZAVA	IZAVA	kA
<g/>
,	,	kIx,
Jasuho	Jasuha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
I.	I.	kA
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85831	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
–	–	k?
odkazováno	odkazovat	k5eAaImNgNnS
jako	jako	k9
„	„	k?
<g/>
SCI	SCI	kA
<g/>
1994	#num#	k4
<g/>
“	“	k?
</s>
<s>
SHORES	SHORES	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
CULL	CULL	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
;	;	kIx,
IZAVA	IZAVA	kA
<g/>
,	,	kIx,
Jasuho	Jasuha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85831	#num#	k4
<g/>
-	-	kIx~
<g/>
73	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
–	–	k?
odkazováno	odkazovat	k5eAaImNgNnS
jako	jako	k9
„	„	k?
<g/>
SCI	SCI	kA
<g/>
1995	#num#	k4
<g/>
“	“	k?
</s>
<s>
WILLMOTT	WILLMOTT	kA
<g/>
,	,	kIx,
H.	H.	kA
P.	P.	kA
Empires	Empires	k1gMnSc1
in	in	k?
the	the	k?
Balance	balance	k1gFnSc1
<g/>
:	:	kIx,
Japanese	Japanese	k1gFnSc1
and	and	k?
Allied	Allied	k1gMnSc1
Pacific	Pacific	k1gMnSc1
Strategies	Strategies	k1gMnSc1
To	to	k9
April	April	k1gInSc4
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
,	,	kIx,
Maryland	Marylanda	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
59114	#num#	k4
<g/>
-	-	kIx~
<g/>
948	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WINSLOW	WINSLOW	kA
<g/>
,	,	kIx,
Walter	Walter	k1gMnSc1
G.	G.	kA
The	The	k1gMnSc1
Fleet	Fleet	k1gMnSc1
The	The	k1gMnSc1
Gods	Godsa	k1gFnPc2
Forgot	Forgot	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
U.	U.	kA
<g/>
S.	S.	kA
Asiatic	Asiatice	k1gFnPc2
Fleet	Fleet	k1gMnSc1
in	in	k?
World	World	k1gMnSc1
War	War	k1gFnSc3
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annapolis	Annapolis	k1gFnPc2
<g/>
:	:	kIx,
Naval	navalit	k5eAaPmRp2nS
Institute	institut	k1gInSc5
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87021	#num#	k4
<g/>
-	-	kIx~
<g/>
188	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WOMACK	WOMACK	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dutch	Dutch	k1gMnSc1
Naval	navalit	k5eAaPmRp2nS
Air	Air	k1gFnSc3
Force	force	k1gFnSc1
Against	Against	k1gMnSc1
Japan	japan	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Defense	defense	k1gFnSc2
of	of	k?
the	the	k?
Netherlands	Netherlands	k1gInSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1942	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Jefferson	Jefferson	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
:	:	kIx,
McFarland	McFarland	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780786423651	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WOMACK	WOMACK	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Allied	Allied	k1gMnSc1
Defence	Defence	k1gFnSc2
of	of	k?
the	the	k?
Malay	Malaa	k1gFnSc2
Barrier	Barrira	k1gFnPc2
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jefferson	Jefferson	k1gInSc1
<g/>
:	:	kIx,
McFarland	McFarland	k1gInSc1
&	&	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4766	#num#	k4
<g/>
-	-	kIx~
<g/>
6293	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Combat	Combat	k5eAaImF,k5eAaPmF
Narrative	Narrativ	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gMnSc1
Java	Java	k1gMnSc1
Sea	Sea	k1gMnSc1
Campaign	Campaign	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Publication	Publication	k1gInSc1
Branch	Brancha	k1gFnPc2
<g/>
,	,	kIx,
Office	Office	kA
of	of	k?
Naval	navalit	k5eAaPmRp2nS
Intelligence	Intelligence	k1gFnSc2
<g/>
,	,	kIx,
United	United	k1gMnSc1
States	States	k1gMnSc1
Navy	Navy	k?
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Japanese	Japanese	k1gFnSc1
Monographs	Monographsa	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
The	The	k?
Invasion	Invasion	k1gInSc1
of	of	k?
the	the	k?
Netherlands	Netherlands	k1gInSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
(	(	kIx(
<g/>
November	November	k1gMnSc1
1941	#num#	k4
<g/>
-March	-March	k1gInSc1
1942	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Army	Arm	k2eAgFnPc1d1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
66	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Japanese	Japanese	k1gFnSc1
Monographs	Monographs	k1gInSc1
<g/>
;	;	kIx,
sv.	sv.	kA
66	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Java-Sumatra	Java-Sumatra	k1gFnSc1
Area	area	k1gFnSc1
Air	Air	k1gFnSc1
Operations	Operations	k1gInSc4
Record	Record	k1gInSc1
(	(	kIx(
<g/>
December	December	k1gMnSc1
1941	#num#	k4
<g/>
-March	-March	k1gInSc1
1942	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Army	Arm	k2eAgFnPc1d1
Air	Air	k1gFnPc1
Force	force	k1gFnSc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1946	#num#	k4
<g/>
.	.	kIx.
107	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Japanese	Japanese	k1gFnSc1
Monographs	Monographs	k1gInSc1
<g/>
;	;	kIx,
sv.	sv.	kA
69	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Naval	navalit	k5eAaPmRp2nS
Operations	Operations	k1gInSc1
in	in	k?
the	the	k?
Invasion	Invasion	k1gInSc1
of	of	k?
Netherlands	Netherlands	k1gInSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
(	(	kIx(
<g/>
December	December	k1gMnSc1
1941	#num#	k4
<g/>
-March	-March	k1gInSc1
1942	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Navy	Navy	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
40	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Japanese	Japanese	k1gFnSc1
Monographs	Monographs	k1gInSc1
<g/>
;	;	kIx,
sv.	sv.	kA
101	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Task	Task	k1gMnSc1
Force	force	k1gFnSc2
Operations	Operations	k1gInSc1
(	(	kIx(
<g/>
November	November	k1gInSc1
1941	#num#	k4
<g/>
-April	-Aprit	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
1942	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Naval	navalit	k5eAaPmRp2nS
Air	Air	k1gFnSc2
Force	force	k1gFnSc7
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
90	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Japanese	Japanese	k1gFnSc1
Monographs	Monographs	k1gInSc1
<g/>
;	;	kIx,
sv.	sv.	kA
113	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Invasion	Invasion	k1gInSc1
of	of	k?
the	the	k?
Dutch	Dutch	k1gMnSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
The	The	k1gFnSc2
War	War	k1gFnSc4
History	Histor	k1gInPc1
Office	Office	kA
of	of	k?
the	the	k?
National	National	k1gFnSc1
Defense	defense	k1gFnSc1
College	College	k1gFnSc1
of	of	k?
Japan	japan	k1gInSc1
&	&	k?
Willem	Will	k1gInSc7
Remmelink	Remmelink	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leiden	Leidna	k1gFnPc2
<g/>
:	:	kIx,
Leiden	Leidna	k1gFnPc2
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
90	#num#	k4
8728	#num#	k4
237	#num#	k4
0	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Překlad	překlad	k1gInSc1
3	#num#	k4
<g/>
.	.	kIx.
svazku	svazek	k1gInSc3
Senši	Senše	k1gFnSc4
sóšo	sóšo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
operations	operations	k1gInSc1
of	of	k?
the	the	k?
Navy	Navy	k?
in	in	k?
the	the	k?
Dutch	Dutch	k1gMnSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
and	and	k?
the	the	k?
Bay	Bay	k1gMnSc1
of	of	k?
Bengal	Bengal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
The	The	k1gFnSc2
War	War	k1gFnSc4
History	Histor	k1gInPc1
Office	Office	kA
of	of	k?
the	the	k?
National	National	k1gFnSc1
Defense	defense	k1gFnSc1
College	College	k1gFnSc1
of	of	k?
Japan	japan	k1gInSc1
&	&	k?
Willem	Will	k1gInSc7
Remmelink	Remmelink	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leiden	Leidna	k1gFnPc2
<g/>
:	:	kIx,
Leiden	Leidna	k1gFnPc2
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9789087282806	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Překlad	překlad	k1gInSc1
26	#num#	k4
<g/>
.	.	kIx.
svazku	svazek	k1gInSc3
Senši	Senše	k1gFnSc4
sóšo	sóšo	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Boje	boj	k1gInSc2
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
GOUGH	GOUGH	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Aerial	Aerial	k1gMnSc1
Defense	defense	k1gFnSc2
of	of	k?
the	the	k?
Netherlands	Netherlands	k1gInSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
and	and	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
Army	Arma	k1gFnSc2
Air	Air	k1gMnSc1
Force	force	k1gFnSc2
in	in	k?
the	the	k?
Defense	defense	k1gFnSc2
of	of	k?
Java	Java	k1gMnSc1
<g/>
,	,	kIx,
1942	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
militaryhistoryonline	militaryhistoryonlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2008-04-06	2008-04-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KLEMEN	KLEMEN	kA
<g/>
,	,	kIx,
L.	L.	kA
Forgotten	Forgotten	k2eAgInSc1d1
Campaign	Campaign	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Dutch	Dutch	k1gMnSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
Campaign	Campaign	k1gMnSc1
1941-1942	1941-1942	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
Tichomoří	Tichomoří	k1gNnSc6
</s>
<s>
Útok	útok	k1gInSc1
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbor	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Malajsii	Malajsie	k1gFnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Singapur	Singapur	k1gInSc4
•	•	k?
Boje	boj	k1gInPc4
o	o	k7c4
Nizozemskou	nizozemský	k2eAgFnSc4d1
východní	východní	k2eAgFnSc4d1
Indii	Indie	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korálovém	korálový	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Midway	Midwaa	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
Guadalcanal	Guadalcanal	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrova	ostrov	k1gInSc2
Savo	Savo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
východních	východní	k2eAgMnPc2d1
Šalomounů	Šalomoun	k1gMnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
mysu	mys	k1gInSc2
Esperance	Esperance	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
ostrovů	ostrov	k1gInPc2
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Guadalcanalu	Guadalcanal	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tassafarongy	Tassafarong	k1gInPc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Rennellova	Rennellův	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Bismarckově	Bismarckův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Komandorských	Komandorský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Tarawu	Tarawus	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
ve	v	k7c6
Filipínském	filipínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
•	•	k?
Operace	operace	k1gFnSc1
Forager	Forager	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Saipan	Saipan	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Guam	Guam	k1gMnSc1
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Tinian	Tinian	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Peleliu	Pelelium	k1gNnSc6
•	•	k?
Boj	boj	k1gInSc1
o	o	k7c4
Filipíny	Filipíny	k1gFnPc4
(	(	kIx(
<g/>
1944	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Leyte	Leyt	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Manilu	Manila	k1gFnSc4
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c6
Iwodžimu	Iwodžimo	k1gNnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Okinawu	Okinawa	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Nizozemsko	Nizozemsko	k1gNnSc1
|	|	kIx~
Japonsko	Japonsko	k1gNnSc1
</s>
