<s>
Zlato	zlato	k1gNnSc1	zlato
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Aurum	Aurum	k1gNnSc1	Aurum
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
prvku	prvek	k1gInSc2	prvek
Au	au	k0	au
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
tepelně	tepelně	k6eAd1	tepelně
i	i	k8xC	i
elektricky	elektricky	k6eAd1	elektricky
vodivý	vodivý	k2eAgMnSc1d1	vodivý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
měkký	měkký	k2eAgInSc1d1	měkký
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
dávnověku	dávnověk	k1gInSc2	dávnověk
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
dekorativních	dekorativní	k2eAgInPc2d1	dekorativní
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
jako	jako	k9	jako
platidlo	platidlo	k1gNnSc1	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
důležitým	důležitý	k2eAgInSc7d1	důležitý
materiálem	materiál	k1gInSc7	materiál
v	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ceněn	ceněn	k2eAgInSc4d1	ceněn
jeho	on	k3xPp3gInSc4	on
nízký	nízký	k2eAgInSc4d1	nízký
přechodový	přechodový	k2eAgInSc4d1	přechodový
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
ryzí	ryzí	k2eAgMnPc4d1	ryzí
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
zlata	zlato	k1gNnSc2	zlato
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
s	s	k7c7	s
explozí	exploze	k1gFnSc7	exploze
supernov	supernova	k1gFnPc2	supernova
a	a	k8xC	a
ve	v	k7c4	v
větší	veliký	k2eAgFnSc4d2	veliký
pak	pak	k6eAd1	pak
s	s	k7c7	s
kolizí	kolize	k1gFnSc7	kolize
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kosmické	kosmický	k2eAgInPc1d1	kosmický
procesy	proces	k1gInPc1	proces
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
mohly	moct	k5eAaImAgInP	moct
sloučit	sloučit	k5eAaPmF	sloučit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
těžkého	těžký	k2eAgInSc2d1	těžký
atomu	atom	k1gInSc2	atom
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
chemicky	chemicky	k6eAd1	chemicky
velmi	velmi	k6eAd1	velmi
odolný	odolný	k2eAgInSc1d1	odolný
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
běžných	běžný	k2eAgFnPc2d1	běžná
anorganických	anorganický	k2eAgFnPc2d1	anorganická
kyselin	kyselina	k1gFnPc2	kyselina
reaguje	reagovat	k5eAaBmIp3nS	reagovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
lučavkou	lučavka	k1gFnSc7	lučavka
královskou	královský	k2eAgFnSc7d1	královská
(	(	kIx(	(
<g/>
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
:	:	kIx,	:
<g/>
HCl	HCl	k1gFnSc1	HCl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
tetrachlorozlatitého	tetrachlorozlatitý	k2eAgInSc2d1	tetrachlorozlatitý
aniontu	anion	k1gInSc2	anion
[	[	kIx(	[
<g/>
Au	au	k0	au
<g/>
(	(	kIx(	(
<g/>
Cl	Cl	k1gMnSc6	Cl
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
alkalickém	alkalický	k2eAgNnSc6d1	alkalické
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
zlato	zlato	k1gNnSc1	zlato
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
kyanidových	kyanidový	k2eAgInPc2d1	kyanidový
iontů	ion	k1gInPc2	ion
(	(	kIx(	(
<g/>
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzniká	vznikat	k5eAaImIp3nS	vznikat
komplexní	komplexní	k2eAgInSc4d1	komplexní
kyanozlatnan	kyanozlatnan	k1gInSc4	kyanozlatnan
[	[	kIx(	[
<g/>
Au	au	k0	au
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
představuje	představovat	k5eAaImIp3nS	představovat
rozpouštění	rozpouštění	k1gNnSc3	rozpouštění
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
elementární	elementární	k2eAgFnSc6d1	elementární
rtuti	rtuť	k1gFnSc6	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
středověcí	středověký	k2eAgMnPc1d1	středověký
alchymisté	alchymista	k1gMnPc1	alchymista
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
zlata	zlato	k1gNnSc2	zlato
se	s	k7c7	s
rtutí	rtuť	k1gFnSc7	rtuť
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
vzniká	vznikat	k5eAaImIp3nS	vznikat
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
roztok	roztok	k1gInSc1	roztok
zlata	zlato	k1gNnSc2	zlato
ve	v	k7c6	v
rtuti	rtuť	k1gFnSc6	rtuť
<g/>
,	,	kIx,	,
amalgám	amalgám	k1gInSc1	amalgám
<g/>
.	.	kIx.	.
</s>
<s>
Amalgám	amalgám	k1gInSc1	amalgám
přitom	přitom	k6eAd1	přitom
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kapalný	kapalný	k2eAgInSc1d1	kapalný
i	i	k9	i
při	při	k7c6	při
poměrně	poměrně	k6eAd1	poměrně
vysokých	vysoký	k2eAgInPc6d1	vysoký
obsazích	obsah	k1gInPc6	obsah
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Zahřátím	zahřátí	k1gNnSc7	zahřátí
amalgámu	amalgám	k1gInSc2	amalgám
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
nad	nad	k7c7	nad
300	[number]	k4	300
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
rtuť	rtuť	k1gFnSc1	rtuť
odpaří	odpařit	k5eAaPmIp3nS	odpařit
a	a	k8xC	a
zbude	zbýt	k5eAaPmIp3nS	zbýt
ryzí	ryzí	k2eAgNnSc4d1	ryzí
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
objevili	objevit	k5eAaPmAgMnP	objevit
japonští	japonský	k2eAgMnPc1d1	japonský
chemici	chemik	k1gMnPc1	chemik
směs	směsa	k1gFnPc2	směsa
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
údajně	údajně	k6eAd1	údajně
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
směs	směs	k1gFnSc4	směs
jodu	jod	k1gInSc2	jod
<g/>
,	,	kIx,	,
tetraetylamoniumjodidu	tetraetylamoniumjodid	k1gInSc2	tetraetylamoniumjodid
a	a	k8xC	a
acetonitrilu	acetonitril	k1gInSc2	acetonitril
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
varu	var	k1gInSc2	var
(	(	kIx(	(
<g/>
82	[number]	k4	82
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
nasycený	nasycený	k2eAgInSc1d1	nasycený
roztok	roztok	k1gInSc1	roztok
<g/>
.	.	kIx.	.
</s>
<s>
Snížením	snížení	k1gNnSc7	snížení
teploty	teplota	k1gFnSc2	teplota
roztoku	roztok	k1gInSc2	roztok
pod	pod	k7c7	pod
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
čistý	čistý	k2eAgInSc1d1	čistý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
také	také	k9	také
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
jodidu	jodid	k1gInSc2	jodid
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
jodu	jod	k1gInSc2	jod
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
tohoto	tento	k3xDgInSc2	tento
roztoku	roztok	k1gInSc2	roztok
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
především	především	k9	především
tenké	tenký	k2eAgFnPc4d1	tenká
vrstvy	vrstva	k1gFnPc4	vrstva
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
trvanlivé	trvanlivý	k2eAgFnSc2d1	trvanlivá
a	a	k8xC	a
odolné	odolný	k2eAgFnSc2d1	odolná
vůči	vůči	k7c3	vůči
povětrnostním	povětrnostní	k2eAgInPc3d1	povětrnostní
i	i	k8xC	i
chemickým	chemický	k2eAgInPc3d1	chemický
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
a	a	k8xC	a
tvrdost	tvrdost	k1gFnSc1	tvrdost
zlata	zlato	k1gNnSc2	zlato
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zvýšit	zvýšit	k5eAaPmF	zvýšit
přidáním	přidání	k1gNnSc7	přidání
jiných	jiný	k2eAgInPc2d1	jiný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Pozlacené	pozlacený	k2eAgFnPc1d1	pozlacená
průhledné	průhledný	k2eAgFnPc1d1	průhledná
plastické	plastický	k2eAgFnPc1d1	plastická
fólie	fólie	k1gFnPc1	fólie
mají	mít	k5eAaImIp3nP	mít
vynikající	vynikající	k2eAgFnSc4d1	vynikající
odrazivost	odrazivost	k1gFnSc4	odrazivost
světelných	světelný	k2eAgFnPc2d1	světelná
a	a	k8xC	a
tepelných	tepelný	k2eAgFnPc2d1	tepelná
(	(	kIx(	(
<g/>
infra-	infra-	k?	infra-
<g/>
)	)	kIx)	)
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
fólie	fólie	k1gFnSc1	fólie
může	moct	k5eAaImIp3nS	moct
chránit	chránit	k5eAaImF	chránit
před	před	k7c7	před
únikem	únik	k1gInSc7	únik
tělesného	tělesný	k2eAgNnSc2d1	tělesné
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
porodnictví	porodnictví	k1gNnSc6	porodnictví
nebo	nebo	k8xC	nebo
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
přírodních	přírodní	k2eAgFnPc6d1	přírodní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
značně	značně	k6eAd1	značně
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
-	-	kIx~	-
5	[number]	k4	5
ppb	ppb	k?	ppb
(	(	kIx(	(
<g/>
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
značně	značně	k6eAd1	značně
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
koncentraci	koncentrace	k1gFnSc3	koncentrace
chloridových	chloridový	k2eAgInPc2d1	chloridový
iontů	ion	k1gInPc2	ion
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
-	-	kIx~	-
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
hodnota	hodnota	k1gFnSc1	hodnota
0,011	[number]	k4	0,011
μ	μ	k?	μ
Au	au	k0	au
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
zlata	zlato	k1gNnSc2	zlato
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
inertnosti	inertnost	k1gFnSc3	inertnost
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
ryzí	ryzí	k2eAgInSc1d1	ryzí
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Krychlový	krychlový	k2eAgInSc4d1	krychlový
nerost	nerost	k1gInSc4	nerost
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
plíšky	plíška	k1gFnPc1	plíška
a	a	k8xC	a
zrna	zrno	k1gNnPc1	zrno
uzavřená	uzavřený	k2eAgNnPc1d1	uzavřené
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
křemenné	křemenný	k2eAgFnSc6d1	křemenná
výplni	výplň	k1gFnSc6	výplň
žil	žít	k5eAaImAgMnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Krystaly	krystal	k1gInPc1	krystal
nejsou	být	k5eNaImIp3nP	být
hojné	hojný	k2eAgFnPc1d1	hojná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mikroskopicky	mikroskopicky	k6eAd1	mikroskopicky
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
v	v	k7c6	v
šedém	šedý	k2eAgInSc6d1	šedý
žilném	žilný	k2eAgInSc6d1	žilný
křemeni	křemen	k1gInSc6	křemen
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ryzí	ryzí	k2eAgMnSc1d1	ryzí
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
slitině	slitina	k1gFnSc6	slitina
se	s	k7c7	s
stříbrem	stříbro	k1gNnSc7	stříbro
(	(	kIx(	(
<g/>
elektrum	elektrum	k1gNnSc1	elektrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozrušení	rozrušení	k1gNnSc6	rozrušení
žil	žíla	k1gFnPc2	žíla
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
náplavů	náplav	k1gInPc2	náplav
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
rýžuje	rýžovat	k5eAaImIp3nS	rýžovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejbohatší	bohatý	k2eAgNnPc1d3	nejbohatší
světová	světový	k2eAgNnPc1d1	světové
naleziště	naleziště	k1gNnPc1	naleziště
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Uralu	Ural	k1gInSc6	Ural
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
;	;	kIx,	;
valouny	valoun	k1gInPc1	valoun
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
nugety	nugeta	k1gFnSc2	nugeta
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
kilogramové	kilogramový	k2eAgInPc1d1	kilogramový
<g/>
)	)	kIx)	)
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
zlato	zlato	k1gNnSc4	zlato
(	(	kIx(	(
<g/>
minerál	minerál	k1gInSc1	minerál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
producenti	producent	k1gMnPc1	producent
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
The	The	k1gFnPc2	The
Atlantic	Atlantice	k1gFnPc2	Atlantice
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
11,0	[number]	k4	11,0
<g/>
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
10,5	[number]	k4	10,5
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
10,1	[number]	k4	10,1
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
9,7	[number]	k4	9,7
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
8,2	[number]	k4	8,2
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
6,2	[number]	k4	6,2
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
4,2	[number]	k4	4,2
<g/>
%	%	kIx~	%
Těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
v	v	k7c6	v
tunách	tuna	k1gFnPc6	tuna
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
U.	U.	kA	U.
S.	S.	kA	S.
Geological	Geological	k1gMnSc2	Geological
Survey	Survea	k1gMnSc2	Survea
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
420	[number]	k4	420
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
255	[number]	k4	255
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
USA	USA	kA	USA
227	[number]	k4	227
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
220	[number]	k4	220
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
150	[number]	k4	150
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
JAR	jar	k1gFnSc1	jar
145	[number]	k4	145
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
120	[number]	k4	120
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
100	[number]	k4	100
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
93	[number]	k4	93
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Ghana	Ghana	k1gFnSc1	Ghana
85	[number]	k4	85
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
rýžovatelná	rýžovatelný	k2eAgNnPc4d1	rýžovatelný
ložiska	ložisko	k1gNnPc4	ložisko
zlata	zlato	k1gNnSc2	zlato
již	již	k6eAd1	již
většinou	většinou	k6eAd1	většinou
vyčerpána	vyčerpán	k2eAgFnSc1d1	vyčerpána
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
bylo	být	k5eAaImAgNnS	být
rýžování	rýžování	k1gNnSc1	rýžování
první	první	k4xOgFnSc7	první
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
metod	metoda	k1gFnPc2	metoda
získávání	získávání	k1gNnSc4	získávání
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
metody	metoda	k1gFnPc1	metoda
rýžování	rýžování	k1gNnSc2	rýžování
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
gravitační	gravitační	k2eAgFnSc1d1	gravitační
separace	separace	k1gFnSc1	separace
lehčích	lehký	k2eAgFnPc2d2	lehčí
částic	částice	k1gFnPc2	částice
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
těží	těžet	k5eAaImIp3nS	těžet
primární	primární	k2eAgNnPc4d1	primární
ložiska	ložisko	k1gNnPc4	ložisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zlato	zlato	k1gNnSc4	zlato
velmi	velmi	k6eAd1	velmi
jemně	jemně	k6eAd1	jemně
rozptýleno	rozptýlit	k5eAaPmNgNnS	rozptýlit
v	v	k7c6	v
hornině	hornina	k1gFnSc6	hornina
a	a	k8xC	a
kov	kov	k1gInSc1	kov
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
horniny	hornina	k1gFnSc2	hornina
získáván	získávat	k5eAaImNgInS	získávat
hydrometalurgicky	hydrometalurgicky	k6eAd1	hydrometalurgicky
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
jemném	jemný	k2eAgNnSc6d1	jemné
namletí	namletí	k1gNnSc6	namletí
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
loužicím	loužicí	k2eAgInSc7d1	loužicí
roztokem	roztok	k1gInSc7	roztok
mohla	moct	k5eAaImAgFnS	moct
dostat	dostat	k5eAaPmF	dostat
většina	většina	k1gFnSc1	většina
přítomných	přítomný	k2eAgFnPc2d1	přítomná
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
zlatých	zlatá	k1gFnPc2	zlatá
zrnek	zrnko	k1gNnPc2	zrnko
<g/>
.	.	kIx.	.
</s>
<s>
Namletá	namletý	k2eAgFnSc1d1	namletá
hornina	hornina	k1gFnSc1	hornina
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
louží	loužit	k5eAaImIp3nS	loužit
buď	buď	k8xC	buď
kyselým	kyselý	k2eAgInSc7d1	kyselý
roztokem	roztok	k1gInSc7	roztok
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
chloridových	chloridový	k2eAgInPc2d1	chloridový
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
prostředím	prostředí	k1gNnSc7	prostředí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sycení	sycení	k1gNnSc1	sycení
plynným	plynný	k2eAgInSc7d1	plynný
chlorem	chlor	k1gInSc7	chlor
nebo	nebo	k8xC	nebo
přídavky	přídavek	k1gInPc1	přídavek
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgInPc1d1	dusičný
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
roztokem	roztok	k1gInSc7	roztok
alkalických	alkalický	k2eAgInPc2d1	alkalický
kyanidů	kyanid	k1gInPc2	kyanid
za	za	k7c2	za
probublávání	probublávání	k1gNnSc2	probublávání
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
loužicího	loužicí	k2eAgInSc2d1	loužicí
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
zlato	zlato	k1gNnSc1	zlato
získává	získávat	k5eAaImIp3nS	získávat
redukcí	redukce	k1gFnSc7	redukce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
průchodem	průchod	k1gInSc7	průchod
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
roztokem	roztok	k1gInSc7	roztok
-	-	kIx~	-
elektrochemicky	elektrochemicky	k6eAd1	elektrochemicky
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
kovové	kovový	k2eAgNnSc1d1	kovové
zlato	zlato	k1gNnSc1	zlato
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
na	na	k7c6	na
záporné	záporný	k2eAgFnSc6d1	záporná
elektrodě	elektroda	k1gFnSc6	elektroda
-	-	kIx~	-
katodě	katoda	k1gFnSc6	katoda
<g/>
.	.	kIx.	.
</s>
<s>
Redukci	redukce	k1gFnSc4	redukce
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
provést	provést	k5eAaPmF	provést
i	i	k8xC	i
chemicky	chemicky	k6eAd1	chemicky
přídavkem	přídavek	k1gInSc7	přídavek
vhodného	vhodný	k2eAgNnSc2d1	vhodné
redukčního	redukční	k2eAgNnSc2d1	redukční
činidla	činidlo	k1gNnSc2	činidlo
(	(	kIx(	(
<g/>
hydrazin	hydrazin	k1gInSc1	hydrazin
<g/>
,	,	kIx,	,
kovový	kovový	k2eAgInSc1d1	kovový
hliník	hliník	k1gInSc1	hliník
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amalgamační	amalgamační	k2eAgInSc1d1	amalgamační
způsob	způsob	k1gInSc1	způsob
těžby	těžba	k1gFnSc2	těžba
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
rud	ruda	k1gFnPc2	ruda
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
pro	pro	k7c4	pro
těžení	těžení	k1gNnSc4	těžení
náplavů	náplav	k1gInPc2	náplav
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
bylo	být	k5eAaImAgNnS	být
zlato	zlato	k1gNnSc1	zlato
přítomno	přítomen	k2eAgNnSc1d1	přítomno
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
větších	veliký	k2eAgNnPc2d2	veliký
oddělených	oddělený	k2eAgNnPc2d1	oddělené
zrnek	zrnko	k1gNnPc2	zrnko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
obtížně	obtížně	k6eAd1	obtížně
získávala	získávat	k5eAaImAgFnS	získávat
rýžováním	rýžování	k1gNnSc7	rýžování
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
byla	být	k5eAaImAgFnS	být
zlatonosná	zlatonosný	k2eAgFnSc1d1	zlatonosná
hornina	hornina	k1gFnSc1	hornina
kontaktována	kontaktován	k2eAgFnSc1d1	kontaktována
s	s	k7c7	s
kovovou	kovový	k2eAgFnSc7d1	kovová
elementární	elementární	k2eAgFnSc7d1	elementární
rtutí	rtuť	k1gFnSc7	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
amalgám	amalgám	k1gInSc1	amalgám
zlata	zlato	k1gNnSc2	zlato
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
horniny	hornina	k1gFnSc2	hornina
obvykle	obvykle	k6eAd1	obvykle
prostě	prostě	k6eAd1	prostě
pyrolyzován	pyrolyzován	k2eAgInSc1d1	pyrolyzován
a	a	k8xC	a
rtuť	rtuť	k1gFnSc1	rtuť
byla	být	k5eAaImAgFnS	být
jednoduše	jednoduše	k6eAd1	jednoduše
odpařena	odpařit	k5eAaPmNgFnS	odpařit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
téměř	téměř	k6eAd1	téměř
nepoužívá	používat	k5eNaImIp3nS	používat
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ano	ano	k9	ano
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
amalgámu	amalgám	k1gInSc2	amalgám
získáváno	získáván	k2eAgNnSc1d1	získáváno
šetrnějším	šetrný	k2eAgInSc7d2	šetrnější
způsobem	způsob	k1gInSc7	způsob
bez	bez	k7c2	bez
kontaminace	kontaminace	k1gFnSc2	kontaminace
atmosféry	atmosféra	k1gFnSc2	atmosféra
parami	para	k1gFnPc7	para
rtuti	rtuť	k1gFnSc2	rtuť
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
vytěženo	vytěžit	k5eAaPmNgNnS	vytěžit
přibližně	přibližně	k6eAd1	přibližně
175	[number]	k4	175
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
krychle	krychle	k1gFnPc1	krychle
o	o	k7c6	o
hraně	hrana	k1gFnSc6	hrana
asi	asi	k9	asi
21	[number]	k4	21
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
vlastníkem	vlastník	k1gMnSc7	vlastník
zlata	zlato	k1gNnSc2	zlato
jsou	být	k5eAaImIp3nP	být
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
261	[number]	k4	261
498	[number]	k4	498
926	[number]	k4	926
trojských	trojský	k2eAgFnPc2d1	Trojská
uncí	unce	k1gFnPc2	unce
(	(	kIx(	(
<g/>
8	[number]	k4	8
133,5	[number]	k4	133,5
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
mají	mít	k5eAaImIp3nP	mít
uloženu	uložen	k2eAgFnSc4d1	uložena
v	v	k7c6	v
kentuckém	kentucký	k2eAgInSc6d1	kentucký
Fort	Fort	k?	Fort
Knoxu	Knox	k1gInSc6	Knox
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c4	v
West	West	k1gInSc4	West
Pointu	pointa	k1gFnSc4	pointa
a	a	k8xC	a
Denveru	Denver	k1gInSc6	Denver
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
zlatonosné	zlatonosný	k2eAgFnPc1d1	zlatonosná
žíly	žíla	k1gFnPc1	žíla
mj.	mj.	kA	mj.
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jílové	Jílové	k1gNnSc1	Jílové
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Roudný	Roudný	k2eAgInSc1d1	Roudný
<g/>
,	,	kIx,	,
Veselý	veselý	k2eAgInSc1d1	veselý
kopec	kopec	k1gInSc1	kopec
u	u	k7c2	u
Mokrska	Mokrsko	k1gNnSc2	Mokrsko
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc2	okolí
Rožmitálu	Rožmitál	k1gInSc2	Rožmitál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
(	(	kIx(	(
<g/>
Zlaté	zlatý	k1gInPc1	zlatý
Hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kašperských	kašperský	k2eAgMnPc2d1	kašperský
hor.	hor.	k?	hor.
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
snaze	snaha	k1gFnSc3	snaha
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
firem	firma	k1gFnPc2	firma
o	o	k7c4	o
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
těžbu	těžba	k1gFnSc4	těžba
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
sdružení	sdružení	k1gNnPc2	sdružení
Čechy	Čechy	k1gFnPc4	Čechy
nad	nad	k7c4	nad
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
převážně	převážně	k6eAd1	převážně
města	město	k1gNnSc2	město
a	a	k8xC	a
obce	obec	k1gFnSc2	obec
z	z	k7c2	z
potenciálně	potenciálně	k6eAd1	potenciálně
ohrožených	ohrožený	k2eAgFnPc2d1	ohrožená
lokalit	lokalita	k1gFnPc2	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Hydrometalurgický	Hydrometalurgický	k2eAgInSc1d1	Hydrometalurgický
postup	postup	k1gInSc1	postup
dobývání	dobývání	k1gNnSc2	dobývání
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
nízkoryzostních	nízkoryzostní	k2eAgFnPc2d1	nízkoryzostní
rud	ruda	k1gFnPc2	ruda
představuje	představovat	k5eAaImIp3nS	představovat
značně	značně	k6eAd1	značně
rizikový	rizikový	k2eAgInSc4d1	rizikový
proces	proces	k1gInSc4	proces
z	z	k7c2	z
ekologického	ekologický	k2eAgNnSc2d1	ekologické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Nasazení	nasazení	k1gNnSc1	nasazení
kyanidových	kyanidový	k2eAgInPc2d1	kyanidový
roztoků	roztok	k1gInPc2	roztok
v	v	k7c6	v
tunových	tunový	k2eAgFnPc6d1	tunová
až	až	k8xS	až
stotunových	stotunový	k2eAgFnPc6d1	stotunová
šaržích	šarže	k1gFnPc6	šarže
představuje	představovat	k5eAaImIp3nS	představovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
riziko	riziko	k1gNnSc4	riziko
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
katastrofální	katastrofální	k2eAgNnSc1d1	katastrofální
zamoření	zamoření	k1gNnSc1	zamoření
Dunaje	Dunaj	k1gInSc2	Dunaj
kyanidy	kyanid	k1gInPc4	kyanid
a	a	k8xC	a
těžkými	těžký	k2eAgInPc7d1	těžký
kovy	kov	k1gInPc7	kov
z	z	k7c2	z
rumunského	rumunský	k2eAgInSc2d1	rumunský
hydrometalurgického	hydrometalurgický	k2eAgInSc2d1	hydrometalurgický
provozu	provoz	k1gInSc2	provoz
Baia	Baium	k1gNnSc2	Baium
Mare	Mar	k1gFnSc2	Mar
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
přírodní	přírodní	k2eAgFnSc1d1	přírodní
katastrofa	katastrofa	k1gFnSc1	katastrofa
-	-	kIx~	-
stovky	stovka	k1gFnPc1	stovka
tun	tuna	k1gFnPc2	tuna
mrtvých	mrtvý	k2eAgFnPc2d1	mrtvá
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
porušení	porušení	k1gNnSc2	porušení
životní	životní	k2eAgFnSc2d1	životní
rovnováhy	rovnováha	k1gFnSc2	rovnováha
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
území	území	k1gNnSc2	území
na	na	k7c4	na
desítky	desítka	k1gFnPc4	desítka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
haváriím	havárie	k1gFnPc3	havárie
podobného	podobný	k2eAgInSc2d1	podobný
druhu	druh	k1gInSc2	druh
došlo	dojít	k5eAaPmAgNnS	dojít
několikrát	několikrát	k6eAd1	několikrát
i	i	k9	i
v	v	k7c6	v
USA	USA	kA	USA
nebo	nebo	k8xC	nebo
jihoamerické	jihoamerický	k2eAgFnSc6d1	jihoamerická
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zamořena	zamořen	k2eAgFnSc1d1	zamořena
řeka	řeka	k1gFnSc1	řeka
Amazonka	Amazonka	k1gFnSc1	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
také	také	k9	také
používání	používání	k1gNnSc1	používání
kovové	kovový	k2eAgFnSc2d1	kovová
rtuti	rtuť	k1gFnSc2	rtuť
pro	pro	k7c4	pro
tzv.	tzv.	kA	tzv.
amalgamační	amalgamační	k2eAgInSc4d1	amalgamační
způsob	způsob	k1gInSc4	způsob
těžby	těžba	k1gFnSc2	těžba
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbatelné	zanedbatelný	k2eNgInPc1d1	nezanedbatelný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
vhodným	vhodný	k2eAgNnSc7d1	vhodné
uložením	uložení	k1gNnSc7	uložení
tisícitunových	tisícitunový	k2eAgNnPc2d1	tisícitunový
kvant	kvantum	k1gNnPc2	kvantum
vyloužené	vyloužený	k2eAgFnSc2d1	vyloužený
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
prakticky	prakticky	k6eAd1	prakticky
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
potenciálním	potenciální	k2eAgNnPc3d1	potenciální
rizikům	riziko	k1gNnPc3	riziko
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
kyanidů	kyanid	k1gInPc2	kyanid
jsou	být	k5eAaImIp3nP	být
vyvíjeny	vyvíjen	k2eAgFnPc1d1	vyvíjena
nové	nový	k2eAgFnPc1d1	nová
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
loužení	loužení	k1gNnSc1	loužení
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
thiomočoviny	thiomočovina	k1gFnSc2	thiomočovina
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlejšímu	rozsáhlý	k2eAgNnSc3d2	rozsáhlejší
nasazení	nasazení	k1gNnSc3	nasazení
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
zatím	zatím	k6eAd1	zatím
brání	bránit	k5eAaImIp3nS	bránit
její	její	k3xOp3gFnSc1	její
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
slitin	slitina	k1gFnPc2	slitina
se	s	k7c7	s
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
,	,	kIx,	,
mědí	měď	k1gFnSc7	měď
<g/>
,	,	kIx,	,
zinkem	zinek	k1gInSc7	zinek
<g/>
,	,	kIx,	,
palladiem	palladion	k1gNnSc7	palladion
či	či	k8xC	či
niklem	nikl	k1gInSc7	nikl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
ryzí	ryzí	k2eAgNnSc1d1	ryzí
zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
měkké	měkký	k2eAgNnSc1d1	měkké
a	a	k8xC	a
šperky	šperk	k1gInPc7	šperk
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
zhotovené	zhotovený	k2eAgNnSc1d1	zhotovené
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nehodily	hodit	k5eNaPmAgFnP	hodit
pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Příměsi	příměs	k1gFnPc1	příměs
palladia	palladion	k1gNnSc2	palladion
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
navíc	navíc	k6eAd1	navíc
zbarvují	zbarvovat	k5eAaImIp3nP	zbarvovat
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
slitinu	slitina	k1gFnSc4	slitina
-	-	kIx~	-
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
dosti	dosti	k6eAd1	dosti
moderní	moderní	k2eAgNnSc1d1	moderní
bílé	bílý	k2eAgNnSc1d1	bílé
zlato	zlato	k1gNnSc1	zlato
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
bílé	bílý	k2eAgNnSc1d1	bílé
zlato	zlato	k1gNnSc1	zlato
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
přeneseně	přeneseně	k6eAd1	přeneseně
označoval	označovat	k5eAaImAgInS	označovat
např.	např.	kA	např.
porcelán	porcelán	k1gInSc4	porcelán
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
či	či	k8xC	či
sůl	sůl	k1gFnSc4	sůl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
klenotnických	klenotnický	k2eAgFnPc6d1	klenotnická
slitinách	slitina	k1gFnPc6	slitina
neboli	neboli	k8xC	neboli
ryzost	ryzost	k1gFnSc4	ryzost
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
v	v	k7c6	v
karátech	karát	k1gInPc6	karát
(	(	kIx(	(
<g/>
ryzí	ryzí	k2eAgNnSc1d1	ryzí
zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
24	[number]	k4	24
<g/>
karátové	karátový	k2eAgFnSc2d1	karátová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
dovoz	dovoz	k1gInSc1	dovoz
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
výrobků	výrobek	k1gInPc2	výrobek
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
podléhá	podléhat	k5eAaImIp3nS	podléhat
puncovnímu	puncovní	k2eAgInSc3d1	puncovní
zákonu	zákon	k1gInSc3	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Šperky	šperk	k1gInPc1	šperk
a	a	k8xC	a
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
nové	nový	k2eAgNnSc1d1	nové
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
staré	starý	k2eAgInPc4d1	starý
určené	určený	k2eAgInPc4d1	určený
k	k	k7c3	k
prodeji	prodej	k1gInSc3	prodej
jsou	být	k5eAaImIp3nP	být
opatřené	opatřený	k2eAgInPc1d1	opatřený
puncem	punc	k1gInSc7	punc
<g/>
.	.	kIx.	.
</s>
<s>
Punc	punc	k1gInSc1	punc
je	být	k5eAaImIp3nS	být
rozdílný	rozdílný	k2eAgInSc1d1	rozdílný
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
ryzost	ryzost	k1gFnSc4	ryzost
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
velmi	velmi	k6eAd1	velmi
tenký	tenký	k2eAgInSc4d1	tenký
zlatý	zlatý	k2eAgInSc4d1	zlatý
film	film	k1gInSc4	film
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
neušlechtilého	ušlechtilý	k2eNgInSc2d1	neušlechtilý
kovu	kov	k1gInSc2	kov
jej	on	k3xPp3gMnSc4	on
dokáže	dokázat	k5eAaPmIp3nS	dokázat
účinně	účinně	k6eAd1	účinně
ochránit	ochránit	k5eAaPmF	ochránit
před	před	k7c7	před
korozí	koroze	k1gFnSc7	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Pozlacování	pozlacování	k1gNnSc1	pozlacování
kovových	kovový	k2eAgInPc2d1	kovový
materiálů	materiál	k1gInPc2	materiál
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
elektrolytickým	elektrolytický	k2eAgNnSc7d1	elektrolytické
vylučováním	vylučování	k1gNnSc7	vylučování
zlata	zlato	k1gNnSc2	zlato
na	na	k7c6	na
příslušném	příslušný	k2eAgInSc6d1	příslušný
kovu	kov	k1gInSc6	kov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ponořen	ponořit	k5eAaPmNgInS	ponořit
do	do	k7c2	do
zlatící	zlatící	k2eAgFnSc2d1	zlatící
lázně	lázeň	k1gFnSc2	lázeň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
vloženo	vložen	k2eAgNnSc4d1	vloženo
záporné	záporný	k2eAgNnSc4d1	záporné
napětí	napětí	k1gNnSc4	napětí
(	(	kIx(	(
<g/>
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
katoda	katoda	k1gFnSc1	katoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zlacení	zlacení	k1gNnSc2	zlacení
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hodnotu	hodnota	k1gFnSc4	hodnota
pokoveného	pokovený	k2eAgInSc2d1	pokovený
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
různé	různý	k2eAgFnPc4d1	různá
sportovní	sportovní	k2eAgFnPc4d1	sportovní
a	a	k8xC	a
příležitostné	příležitostný	k2eAgFnPc4d1	příležitostná
medaile	medaile	k1gFnPc4	medaile
<g/>
,	,	kIx,	,
pamětní	pamětní	k2eAgFnPc4d1	pamětní
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
bižuterie	bižuterie	k1gFnPc4	bižuterie
apod.	apod.	kA	apod.
Na	na	k7c4	na
nekovové	kovový	k2eNgInPc4d1	nekovový
povrchy	povrch	k1gInPc4	povrch
(	(	kIx(	(
<g/>
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zlato	zlato	k1gNnSc1	zlato
nanáší	nanášet	k5eAaImIp3nS	nanášet
mechanicky	mechanicky	k6eAd1	mechanicky
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
kovové	kovový	k2eAgNnSc4d1	kovové
zlato	zlato	k1gNnSc4	zlato
lze	lze	k6eAd1	lze
rozválcovat	rozválcovat	k5eAaPmF	rozválcovat
nebo	nebo	k8xC	nebo
vyklepat	vyklepat	k5eAaPmF	vyklepat
do	do	k7c2	do
mimořádně	mimořádně	k6eAd1	mimořádně
tenkých	tenký	k2eAgFnPc2d1	tenká
fólií	fólie	k1gFnPc2	fólie
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
pouze	pouze	k6eAd1	pouze
několika	několik	k4yIc2	několik
mikrometrů	mikrometr	k1gInPc2	mikrometr
(	(	kIx(	(
<g/>
z	z	k7c2	z
1	[number]	k4	1
g	g	kA	g
zlata	zlato	k1gNnSc2	zlato
lze	lze	k6eAd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
fólii	fólie	k1gFnSc3	fólie
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
až	až	k9	až
1	[number]	k4	1
m2	m2	k4	m2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgFnPc4d1	tenká
fólie	fólie	k1gFnPc4	fólie
mají	mít	k5eAaImIp3nP	mít
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
proti	proti	k7c3	proti
světlu	světlo	k1gNnSc3	světlo
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
má	mít	k5eAaImIp3nS	mít
zlatá	zlatý	k2eAgFnSc1d1	zlatá
fólie	fólie	k1gFnSc1	fólie
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
pozlacovaného	pozlacovaný	k2eAgInSc2d1	pozlacovaný
předmětu	předmět	k1gInSc2	předmět
funkci	funkce	k1gFnSc4	funkce
nejen	nejen	k6eAd1	nejen
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
estetickou	estetický	k2eAgFnSc4d1	estetická
(	(	kIx(	(
<g/>
pozlacené	pozlacený	k2eAgFnSc2d1	pozlacená
sochy	socha	k1gFnSc2	socha
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
staveb	stavba	k1gFnPc2	stavba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
k	k	k7c3	k
pozlacování	pozlacování	k1gNnSc3	pozlacování
těla	tělo	k1gNnSc2	tělo
či	či	k8xC	či
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xC	jako
emulgátor	emulgátor	k1gInSc1	emulgátor
E	E	kA	E
<g/>
175	[number]	k4	175
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
vynikající	vynikající	k2eAgFnSc3d1	vynikající
elektrické	elektrický	k2eAgFnSc3d1	elektrická
vodivosti	vodivost	k1gFnSc3	vodivost
a	a	k8xC	a
inertnosti	inertnost	k1gFnSc3	inertnost
vůči	vůči	k7c3	vůči
vlivům	vliv	k1gInPc3	vliv
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
mikroelektronice	mikroelektronika	k1gFnSc6	mikroelektronika
a	a	k8xC	a
počítačovém	počítačový	k2eAgInSc6d1	počítačový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
oborem	obor	k1gInSc7	obor
využití	využití	k1gNnSc2	využití
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
především	především	k9	především
zajištění	zajištění	k1gNnSc4	zajištění
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
a	a	k8xC	a
bezproblémové	bezproblémový	k2eAgFnSc2d1	bezproblémová
vodivosti	vodivost	k1gFnSc2	vodivost
důležitých	důležitý	k2eAgInPc2d1	důležitý
spojů	spoj	k1gInPc2	spoj
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kontakty	kontakt	k1gInPc4	kontakt
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
příslušné	příslušný	k2eAgInPc1d1	příslušný
kontaktní	kontaktní	k2eAgInPc1d1	kontaktní
povrchy	povrch	k1gInPc1	povrch
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
tenkou	tenký	k2eAgFnSc7d1	tenká
zlatou	zlatý	k2eAgFnSc7d1	zlatá
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
nebo	nebo	k8xC	nebo
zlacení	zlacení	k1gNnSc3	zlacení
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
skleněného	skleněný	k2eAgInSc2d1	skleněný
předmětu	předmět	k1gInSc2	předmět
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
nejprve	nejprve	k6eAd1	nejprve
štětečkem	štěteček	k1gInSc7	štěteček
nanáší	nanášet	k5eAaImIp3nS	nanášet
roztok	roztok	k1gInSc1	roztok
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
matrici	matrice	k1gFnSc6	matrice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyžíhání	vyžíhání	k1gNnSc6	vyžíhání
se	se	k3xPyFc4	se
organické	organický	k2eAgNnSc1d1	organické
rozpouštědlo	rozpouštědlo	k1gNnSc1	rozpouštědlo
odpaří	odpařit	k5eAaPmIp3nS	odpařit
a	a	k8xC	a
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
skla	sklo	k1gNnSc2	sklo
zůstane	zůstat	k5eAaPmIp3nS	zůstat
trvalá	trvalý	k2eAgFnSc1d1	trvalá
zlatá	zlatý	k2eAgFnSc1d1	zlatá
kresba	kresba	k1gFnSc1	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Přídavky	přídavek	k1gInPc1	přídavek
malých	malý	k1gMnPc2	malý
množství	množství	k1gNnSc2	množství
zlata	zlato	k1gNnSc2	zlato
do	do	k7c2	do
hmoty	hmota	k1gFnSc2	hmota
skloviny	sklovina	k1gFnSc2	sklovina
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zbarvení	zbarvení	k1gNnSc1	zbarvení
skla	sklo	k1gNnSc2	sklo
různými	různý	k2eAgInPc7d1	různý
odstíny	odstín	k1gInPc7	odstín
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
součástí	součást	k1gFnSc7	součást
většiny	většina	k1gFnSc2	většina
dentálních	dentální	k2eAgFnPc2d1	dentální
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
materiálů	materiál	k1gInPc2	materiál
sloužících	sloužící	k2eAgInPc2d1	sloužící
v	v	k7c6	v
zubním	zubní	k2eAgNnSc6d1	zubní
lékařství	lékařství	k1gNnSc6	lékařství
jako	jako	k8xS	jako
výplně	výplň	k1gFnSc2	výplň
zubů	zub	k1gInPc2	zub
napadených	napadený	k2eAgInPc2d1	napadený
zubním	zubní	k2eAgInSc7d1	zubní
kazem	kaz	k1gInSc7	kaz
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
můstků	můstek	k1gInPc2	můstek
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
především	především	k9	především
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc1	nezávadnost
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
chemicky	chemicky	k6eAd1	chemicky
inertní	inertní	k2eAgNnPc1d1	inertní
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
po	po	k7c6	po
mnohaletém	mnohaletý	k2eAgNnSc6d1	mnohaleté
působení	působení	k1gNnSc6	působení
poměrně	poměrně	k6eAd1	poměrně
agresivního	agresivní	k2eAgNnSc2d1	agresivní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
dutině	dutina	k1gFnSc6	dutina
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
korozi	koroze	k1gFnSc4	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Čisté	čistý	k2eAgNnSc1d1	čisté
zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
však	však	k9	však
příliš	příliš	k6eAd1	příliš
měkké	měkký	k2eAgNnSc1d1	měkké
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
aplikují	aplikovat	k5eAaBmIp3nP	aplikovat
jeho	jeho	k3xOp3gFnPc1	jeho
slitiny	slitina	k1gFnPc1	slitina
především	především	k9	především
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
<g/>
,	,	kIx,	,
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
,	,	kIx,	,
palladiem	palladion	k1gNnSc7	palladion
<g/>
,	,	kIx,	,
zinkem	zinek	k1gInSc7	zinek
<g/>
,	,	kIx,	,
cínem	cín	k1gInSc7	cín
<g/>
,	,	kIx,	,
antimonem	antimon	k1gInSc7	antimon
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
dentální	dentální	k2eAgFnSc2d1	dentální
slitiny	slitina	k1gFnSc2	slitina
také	také	k9	také
indium	indium	k1gNnSc4	indium
<g/>
,	,	kIx,	,
iridium	iridium	k1gNnSc1	iridium
<g/>
,	,	kIx,	,
rhodium	rhodium	k1gNnSc1	rhodium
nebo	nebo	k8xC	nebo
platina	platina	k1gFnSc1	platina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
sloužilo	sloužit	k5eAaImAgNnS	sloužit
zlato	zlato	k1gNnSc1	zlato
uložené	uložený	k2eAgNnSc1d1	uložené
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
bankách	banka	k1gFnPc6	banka
jako	jako	k8xC	jako
zlatý	zlatý	k2eAgInSc4d1	zlatý
standard	standard	k1gInSc4	standard
<g/>
,	,	kIx,	,
garantující	garantující	k2eAgFnSc4d1	garantující
hodnotu	hodnota	k1gFnSc4	hodnota
státem	stát	k1gInSc7	stát
vydávaného	vydávaný	k2eAgNnSc2d1	vydávané
oběživa	oběživo	k1gNnSc2	oběživo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
význam	význam	k1gInSc4	význam
zlata	zlato	k1gNnSc2	zlato
jako	jako	k8xS	jako
devizy	deviza	k1gFnSc2	deviza
postupně	postupně	k6eAd1	postupně
klesal	klesat	k5eAaImAgInS	klesat
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
přestalo	přestat	k5eAaPmAgNnS	přestat
plnit	plnit	k5eAaImF	plnit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obchodování	obchodování	k1gNnSc6	obchodování
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
pro	pro	k7c4	pro
bankovní	bankovní	k2eAgInPc4d1	bankovní
účely	účel	k1gInPc4	účel
bývá	bývat	k5eAaImIp3nS	bývat
zvykem	zvyk	k1gInSc7	zvyk
označovat	označovat	k5eAaImF	označovat
jeho	jeho	k3xOp3gFnSc4	jeho
hmotnost	hmotnost	k1gFnSc4	hmotnost
v	v	k7c6	v
trojských	trojský	k2eAgFnPc6d1	Trojská
uncích	unce	k1gFnPc6	unce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
jednotka	jednotka	k1gFnSc1	jednotka
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
31,103	[number]	k4	31,103
<g/>
5	[number]	k4	5
g.	g.	k?	g.
Zlato	zlato	k1gNnSc1	zlato
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
investiční	investiční	k2eAgInSc4d1	investiční
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
nakoupit	nakoupit	k5eAaPmF	nakoupit
tzv.	tzv.	kA	tzv.
investiční	investiční	k2eAgNnSc1d1	investiční
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
evropské	evropský	k2eAgFnSc2d1	Evropská
legislativy	legislativa	k1gFnSc2	legislativa
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
od	od	k7c2	od
DPH	DPH	kA	DPH
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
vydávají	vydávat	k5eAaImIp3nP	vydávat
světové	světový	k2eAgFnPc4d1	světová
mincovny	mincovna	k1gFnPc4	mincovna
novoražby	novoražba	k1gFnSc2	novoražba
a	a	k8xC	a
zlaté	zlatý	k2eAgInPc1d1	zlatý
slitky	slitek	k1gInPc1	slitek
(	(	kIx(	(
<g/>
cihly	cihla	k1gFnPc1	cihla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vynikají	vynikat	k5eAaImIp3nP	vynikat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
malém	malé	k1gNnSc6	malé
objemu	objem	k1gInSc2	objem
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
velkou	velký	k2eAgFnSc4d1	velká
finanční	finanční	k2eAgFnSc4d1	finanční
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
i	i	k9	i
značnou	značný	k2eAgFnSc4d1	značná
estetickou	estetický	k2eAgFnSc4d1	estetická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jiné	jiný	k2eAgInPc4d1	jiný
drahé	drahý	k2eAgInPc4d1	drahý
kovy	kov	k1gInPc4	kov
je	být	k5eAaImIp3nS	být
komoditou	komodita	k1gFnSc7	komodita
s	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
na	na	k7c6	na
světových	světový	k2eAgFnPc6d1	světová
burzách	burza	k1gFnPc6	burza
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
burza	burza	k1gFnSc1	burza
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
zveřejňuje	zveřejňovat	k5eAaImIp3nS	zveřejňovat
průběžně	průběžně	k6eAd1	průběžně
výsledky	výsledek	k1gInPc4	výsledek
obchodování	obchodování	k1gNnSc2	obchodování
tzv.	tzv.	kA	tzv.
London	London	k1gMnSc1	London
FIX	fix	k1gInSc1	fix
a	a	k8xC	a
London	London	k1gMnSc1	London
SPOT	spot	k1gInSc1	spot
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
cena	cena	k1gFnSc1	cena
zlata	zlato	k1gNnSc2	zlato
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
udávaná	udávaný	k2eAgFnSc1d1	udávaná
v	v	k7c6	v
dolarech	dolar	k1gInPc6	dolar
za	za	k7c4	za
trojskou	trojský	k2eAgFnSc4d1	Trojská
unci	unce	k1gFnSc4	unce
(	(	kIx(	(
<g/>
USD	USD	kA	USD
<g/>
/	/	kIx~	/
<g/>
oz	oz	k?	oz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
a	a	k8xC	a
mince	mince	k1gFnSc1	mince
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
ražené	ražený	k2eAgFnPc1d1	ražená
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
rozšířeným	rozšířený	k2eAgNnSc7d1	rozšířené
platidlem	platidlo	k1gNnSc7	platidlo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
měkkost	měkkost	k1gFnSc4	měkkost
zlata	zlato	k1gNnSc2	zlato
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
razily	razit	k5eAaImAgFnP	razit
zlaté	zlatý	k2eAgFnPc4d1	zlatá
mince	mince	k1gFnPc4	mince
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
a	a	k8xC	a
nejrozšířenější	rozšířený	k2eAgFnSc7d3	nejrozšířenější
zlatou	zlatý	k2eAgFnSc7d1	zlatá
mincí	mince	k1gFnSc7	mince
byl	být	k5eAaImAgInS	být
dukát	dukát	k1gInSc1	dukát
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
dukát	dukát	k1gInSc1	dukát
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
královstvích	království	k1gNnPc6	království
a	a	k8xC	a
císařstvích	císařství	k1gNnPc6	císařství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
oblíbenost	oblíbenost	k1gFnSc4	oblíbenost
se	se	k3xPyFc4	se
razil	razit	k5eAaImAgInS	razit
a	a	k8xC	a
razí	razit	k5eAaImIp3nS	razit
dál	daleko	k6eAd2	daleko
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
nejznámější	známý	k2eAgFnPc4d3	nejznámější
zlaté	zlatý	k2eAgFnPc4d1	zlatá
mince	mince	k1gFnPc4	mince
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
jsou	být	k5eAaImIp3nP	být
zlaté	zlatý	k2eAgFnPc4d1	zlatá
keltské	keltský	k2eAgFnPc4d1	keltská
mince	mince	k1gFnPc4	mince
statéry	statér	k1gInPc1	statér
známé	známá	k1gFnSc2	známá
jako	jako	k8xC	jako
duhovky	duhovka	k1gFnSc2	duhovka
(	(	kIx(	(
<g/>
nacházené	nacházený	k2eAgFnSc2d1	nacházená
po	po	k7c6	po
dešti	dešť	k1gInSc6	dešť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
XAU	XAU	kA	XAU
je	být	k5eAaImIp3nS	být
kód	kód	k1gInSc4	kód
pro	pro	k7c4	pro
1	[number]	k4	1
trojskou	trojský	k2eAgFnSc4d1	Trojská
unci	unce	k1gFnSc4	unce
zlata	zlato	k1gNnSc2	zlato
jako	jako	k8xS	jako
platidla	platidlo	k1gNnSc2	platidlo
podle	podle	k7c2	podle
standardu	standard	k1gInSc2	standard
ISO	ISO	kA	ISO
4217	[number]	k4	4217
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
egyptské	egyptský	k2eAgFnSc6d1	egyptská
mytologii	mytologie	k1gFnSc6	mytologie
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bohové	bůh	k1gMnPc1	bůh
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc4	jejich
maso	maso	k1gNnSc4	maso
<g/>
)	)	kIx)	)
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
řecké	řecký	k2eAgNnSc4d1	řecké
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
zlato	zlato	k1gNnSc4	zlato
jako	jako	k8xC	jako
ichor	ichor	k1gInSc1	ichor
(	(	kIx(	(
<g/>
krev	krev	k1gFnSc1	krev
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
nesmrtelných	nesmrtelný	k1gMnPc2	nesmrtelný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
jablka	jablko	k1gNnPc4	jablko
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
hlídaly	hlídat	k5eAaImAgFnP	hlídat
Hesperidky	Hesperidka	k1gFnPc1	Hesperidka
<g/>
)	)	kIx)	)
či	či	k8xC	či
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Inky	Ink	k1gMnPc4	Ink
bylo	být	k5eAaImAgNnS	být
zlato	zlato	k1gNnSc4	zlato
symbolem	symbol	k1gInSc7	symbol
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
bůh	bůh	k1gMnSc1	bůh
Inti	Int	k1gFnSc2	Int
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Hélios	Hélios	k1gMnSc1	Hélios
putoval	putovat	k5eAaImAgMnS	putovat
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
člunu	člun	k1gInSc6	člun
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
zlatitý	zlatitý	k2eAgInSc1d1	zlatitý
Oxid	oxid	k1gInSc1	oxid
zlatitý	zlatitý	k2eAgInSc1d1	zlatitý
Kyselina	kyselina	k1gFnSc1	kyselina
chlorozlatitá	chlorozlatitat	k5eAaPmIp3nS	chlorozlatitat
Dikyanozlatnan	Dikyanozlatnan	k1gInSc4	Dikyanozlatnan
sodný	sodný	k2eAgInSc4d1	sodný
</s>
