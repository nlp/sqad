<s>
Kritičnost	kritičnost	k1gFnSc1	kritičnost
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
často	často	k6eAd1	často
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
idylizující	idylizující	k2eAgInSc4d1	idylizující
nebo	nebo	k8xC	nebo
harmonizující	harmonizující	k2eAgInSc4d1	harmonizující
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
vrcholných	vrcholný	k2eAgInPc6d1	vrcholný
dílech	díl	k1gInPc6	díl
<g/>
[	[	kIx(	[
<g/>
ujasnit	ujasnit	k5eAaPmF	ujasnit
<g/>
]	]	kIx)	]
tuto	tento	k3xDgFnSc4	tento
tendenci	tendence	k1gFnSc4	tendence
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
