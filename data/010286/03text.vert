<p>
<s>
Bor	bor	k1gInSc1	bor
(	(	kIx(	(
<g/>
též	též	k9	též
bór	bór	k1gInSc1	bór
<g/>
;	;	kIx,	;
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
B	B	kA	B
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Borum	Borum	k1gInSc1	Borum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlehčím	lehký	k2eAgFnPc3d3	nejlehčí
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
prvků	prvek	k1gInPc2	prvek
III	III	kA	III
<g/>
.	.	kIx.	.
hlavní	hlavní	k2eAgFnPc1d1	hlavní
skupiny	skupina	k1gFnPc1	skupina
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
polokovy	polokův	k2eAgMnPc4d1	polokův
vysokým	vysoký	k2eAgInSc7d1	vysoký
bodem	bod	k1gInSc7	bod
tání	tání	k1gNnSc2	tání
i	i	k8xC	i
varu	var	k1gInSc2	var
–	–	k?	–
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
kovy	kov	k1gInPc7	kov
a	a	k8xC	a
nekovy	nekov	k1gInPc7	nekov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
izolován	izolován	k2eAgInSc1d1	izolován
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
sirem	sir	k1gMnSc7	sir
Humphrym	Humphrym	k1gInSc1	Humphrym
Davym	Davymum	k1gNnPc2	Davymum
<g/>
,	,	kIx,	,
Gay-Lusacem	Gay-Lusace	k1gNnSc7	Gay-Lusace
a	a	k8xC	a
L.	L.	kA	L.
J.	J.	kA	J.
Thénardem	Thénard	k1gInSc7	Thénard
v	v	k7c4	v
nepříliš	příliš	k6eNd1	příliš
vysoké	vysoký	k2eAgFnSc6d1	vysoká
čistotě	čistota	k1gFnSc6	čistota
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
ho	on	k3xPp3gInSc4	on
Jakob	Jakob	k1gMnSc1	Jakob
Berzelius	Berzelius	k1gMnSc1	Berzelius
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
samotný	samotný	k2eAgInSc4d1	samotný
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
modifikacích	modifikace	k1gFnPc6	modifikace
–	–	k?	–
amorfní	amorfní	k2eAgInPc1d1	amorfní
a	a	k8xC	a
kovové	kovový	k2eAgInPc1d1	kovový
<g/>
.	.	kIx.	.
</s>
<s>
Kovová	kovový	k2eAgFnSc1d1	kovová
modifikace	modifikace	k1gFnSc1	modifikace
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
látky	látka	k1gFnPc4	látka
–	–	k?	–
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
9,3	[number]	k4	9,3
v	v	k7c6	v
Mohsově	Mohsův	k2eAgFnSc6d1	Mohsova
stupnici	stupnice	k1gFnSc6	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgInSc4d1	elementární
bor	bor	k1gInSc4	bor
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
boritého	boritý	k2eAgInSc2d1	boritý
kovovým	kovový	k2eAgInSc7d1	kovový
hořčíkem	hořčík	k1gInSc7	hořčík
nebo	nebo	k8xC	nebo
hliníkem	hliník	k1gInSc7	hliník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
B2O3	B2O3	k1gMnSc1	B2O3
+	+	kIx~	+
3	[number]	k4	3
<g/>
Mg	mg	kA	mg
→	→	k?	→
2B	[number]	k4	2B
+	+	kIx~	+
3	[number]	k4	3
<g/>
MgOPro	MgOPro	k1gNnSc1	MgOPro
zisk	zisk	k1gInSc1	zisk
velmi	velmi	k6eAd1	velmi
čistého	čistý	k2eAgNnSc2d1	čisté
polokovu	polokův	k2eAgFnSc4d1	polokův
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
redukce	redukce	k1gFnSc1	redukce
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
boru	bor	k1gInSc2	bor
je	být	k5eAaImIp3nS	být
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
obtížná	obtížný	k2eAgFnSc1d1	obtížná
procedura	procedura	k1gFnSc1	procedura
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
bor	bor	k1gInSc1	bor
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
používá	používat	k5eAaImIp3nS	používat
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
BBr	BBr	k1gFnSc1	BBr
<g/>
3	[number]	k4	3
+	+	kIx~	+
3	[number]	k4	3
H2	H2	k1gFnSc2	H2
→	→	k?	→
2	[number]	k4	2
B	B	kA	B
+	+	kIx~	+
6	[number]	k4	6
HBrPoužívá	HBrPoužívá	k1gFnPc2	HBrPoužívá
se	se	k3xPyFc4	se
také	také	k9	také
elektrolytická	elektrolytický	k2eAgFnSc1d1	elektrolytická
výroba	výroba	k1gFnSc1	výroba
boru	bor	k1gInSc2	bor
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
elektrolýza	elektrolýza	k1gFnSc1	elektrolýza
roztavených	roztavený	k2eAgInPc2d1	roztavený
boritanů	boritan	k1gInPc2	boritan
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
bor	bor	k1gInSc1	bor
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
prakticky	prakticky	k6eAd1	prakticky
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
setkat	setkat	k5eAaPmF	setkat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
světová	světový	k2eAgFnSc1d1	světová
naleziště	naleziště	k1gNnSc1	naleziště
surovin	surovina	k1gFnPc2	surovina
boru	bor	k1gInSc2	bor
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc6	Peru
<g/>
,	,	kIx,	,
Tibetu	Tibet	k1gInSc6	Tibet
a	a	k8xC	a
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
boru	bor	k1gInSc2	bor
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
obsaženy	obsažen	k2eAgInPc1d1	obsažen
i	i	k8xC	i
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
minerálních	minerální	k2eAgInPc6d1	minerální
pramenech	pramen	k1gInPc6	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
boritá	boritý	k2eAgFnSc1d1	boritá
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
přítomna	přítomen	k2eAgFnSc1d1	přítomna
v	v	k7c6	v
sopečných	sopečný	k2eAgInPc6d1	sopečný
plynech	plyn	k1gInPc6	plyn
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
získávána	získáván	k2eAgFnSc1d1	získávána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
potravin	potravina	k1gFnPc2	potravina
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
především	především	k9	především
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
,	,	kIx,	,
zelenině	zelenina	k1gFnSc6	zelenina
<g/>
,	,	kIx,	,
luštěninách	luštěnina	k1gFnPc6	luštěnina
a	a	k8xC	a
oříškách	oříškách	k?	oříškách
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Biologický	biologický	k2eAgInSc4d1	biologický
význam	význam	k1gInSc4	význam
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
je	být	k5eAaImIp3nS	být
bor	bor	k1gInSc4	bor
mikrobiogenním	mikrobiogenní	k2eAgInSc7d1	mikrobiogenní
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
minerály	minerál	k1gInPc4	minerál
je	být	k5eAaImIp3nS	být
přijímán	přijímat	k5eAaImNgMnS	přijímat
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
nikoli	nikoli	k9	nikoli
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
elektroneutrální	elektroneutrální	k2eAgFnSc1d1	elektroneutrální
kyselina	kyselina	k1gFnSc1	kyselina
boritá	boritý	k2eAgFnSc1d1	boritá
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
BO	BO	k?	BO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bor	bor	k1gInSc1	bor
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c6	na
cis-hydroxylové	cisydroxylový	k2eAgFnSc6d1	cis-hydroxylový
(	(	kIx(	(
<g/>
diolové	diolová	k1gFnSc6	diolová
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
pektinu	pektin	k1gInSc2	pektin
rhamnogalakturonanu	rhamnogalakturonanout	k5eAaPmIp1nS	rhamnogalakturonanout
II	II	kA	II
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
polysacharid	polysacharid	k1gInSc4	polysacharid
důležitý	důležitý	k2eAgInSc4d1	důležitý
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
a	a	k8xC	a
především	především	k9	především
její	její	k3xOp3gFnSc1	její
pružnost	pružnost	k1gFnSc1	pružnost
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnSc1d1	související
schopnost	schopnost	k1gFnSc1	schopnost
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
i	i	k9	i
u	u	k7c2	u
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
pylové	pylový	k2eAgFnSc2d1	pylová
láčky	láčka	k1gFnSc2	láčka
nebo	nebo	k8xC	nebo
u	u	k7c2	u
kořenových	kořenový	k2eAgFnPc2d1	kořenová
špiček	špička	k1gFnPc2	špička
a	a	k8xC	a
právě	právě	k9	právě
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
nedostatek	nedostatek	k1gInSc1	nedostatek
boru	bor	k1gInSc2	bor
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
projevuje	projevovat	k5eAaImIp3nS	projevovat
nejdříve	dříve	k6eAd3	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Pylové	pylový	k2eAgFnPc1d1	pylová
láčky	láčka	k1gFnPc1	láčka
nejsou	být	k5eNaImIp3nP	být
bez	bez	k7c2	bez
boru	bor	k1gInSc2	bor
schopné	schopný	k2eAgFnSc2d1	schopná
normálního	normální	k2eAgInSc2d1	normální
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
<g/>
Živočichové	živočich	k1gMnPc1	živočich
zpravidla	zpravidla	k6eAd1	zpravidla
nedostatkem	nedostatek	k1gInSc7	nedostatek
boru	bor	k1gInSc2	bor
netrpí	trpět	k5eNaImIp3nS	trpět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
fyziologických	fyziologický	k2eAgInPc2d1	fyziologický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
kuřat	kuře	k1gNnPc2	kuře
či	či	k8xC	či
krys	krysa	k1gFnPc2	krysa
potravou	potrava	k1gFnSc7	potrava
bez	bez	k7c2	bez
boru	bor	k1gInSc2	bor
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poruchám	porucha	k1gFnPc3	porucha
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
metabolismu	metabolismus	k1gInSc3	metabolismus
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
vývoji	vývoj	k1gInPc7	vývoj
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc4	funkce
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
či	či	k8xC	či
uvolňování	uvolňování	k1gNnSc1	uvolňování
inzulinu	inzulin	k1gInSc2	inzulin
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgInPc1d3	nejvýraznější
následky	následek	k1gInPc1	následek
nedostatku	nedostatek	k1gInSc2	nedostatek
boru	bor	k1gInSc2	bor
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
při	při	k7c6	při
současném	současný	k2eAgInSc6d1	současný
nedostatku	nedostatek	k1gInSc6	nedostatek
vápníku	vápník	k1gInSc2	vápník
či	či	k8xC	či
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Bor	bor	k1gInSc1	bor
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
ve	v	k7c6	v
sklářství	sklářství	k1gNnSc6	sklářství
jako	jako	k8xC	jako
přísada	přísada	k1gFnSc1	přísada
do	do	k7c2	do
skelných	skelný	k2eAgNnPc2d1	skelné
vláken	vlákno	k1gNnPc2	vlákno
a	a	k8xC	a
borokřemičitanových	borokřemičitanový	k2eAgNnPc2d1	borokřemičitanový
skel	sklo	k1gNnPc2	sklo
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
vysoké	vysoký	k2eAgFnSc2d1	vysoká
tepelné	tepelný	k2eAgFnSc2d1	tepelná
odolnosti	odolnost	k1gFnSc2	odolnost
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
keramice	keramika	k1gFnSc6	keramika
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
emailů	email	k1gInPc2	email
a	a	k8xC	a
glazur	glazura	k1gFnPc2	glazura
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
mýdel	mýdlo	k1gNnPc2	mýdlo
a	a	k8xC	a
detergentů	detergent	k1gInPc2	detergent
<g/>
,	,	kIx,	,
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
neželezných	železný	k2eNgInPc2d1	neželezný
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
žáruvzdorných	žáruvzdorný	k2eAgInPc2d1	žáruvzdorný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
energetice	energetika	k1gFnSc6	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jedinečné	jedinečný	k2eAgNnSc1d1	jedinečné
jaderné	jaderný	k2eAgNnSc1d1	jaderné
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
velkém	velký	k2eAgInSc6d1	velký
účinném	účinný	k2eAgInSc6d1	účinný
průřezu	průřez	k1gInSc6	průřez
izotopu	izotop	k1gInSc2	izotop
10B	[number]	k4	10B
vůči	vůči	k7c3	vůči
tepelným	tepelný	k2eAgInPc3d1	tepelný
neutronům	neutron	k1gInPc3	neutron
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
produkty	produkt	k1gInPc1	produkt
reakce	reakce	k1gFnSc2	reakce
jsou	být	k5eAaImIp3nP	být
stálé	stálý	k2eAgFnPc1d1	stálá
neradioaktivní	radioaktivní	k2eNgFnPc1d1	neradioaktivní
Li	li	k8xS	li
a	a	k8xC	a
He	he	k0	he
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
beryllium	beryllium	k1gNnSc1	beryllium
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
řídicích	řídicí	k2eAgFnPc2d1	řídicí
tyčí	tyč	k1gFnPc2	tyč
v	v	k7c6	v
reaktorech	reaktor	k1gInPc6	reaktor
a	a	k8xC	a
neutronových	neutronový	k2eAgNnPc2d1	neutronové
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Bor	bor	k1gInSc1	bor
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přicházejí	přicházet	k5eAaImIp3nP	přicházet
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
jako	jako	k8xC	jako
palivo	palivo	k1gNnSc4	palivo
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
fúzi	fúze	k1gFnSc4	fúze
<g/>
.	.	kIx.	.
</s>
<s>
Bor	bor	k1gInSc1	bor
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
barví	barvit	k5eAaImIp3nP	barvit
plamen	plamen	k1gInSc4	plamen
intenzivně	intenzivně	k6eAd1	intenzivně
zeleně	zeleně	k6eAd1	zeleně
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
směsí	směs	k1gFnPc2	směs
pro	pro	k7c4	pro
pyrotechnické	pyrotechnický	k2eAgInPc4d1	pyrotechnický
účely	účel	k1gInPc4	účel
a	a	k8xC	a
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
důkaz	důkaz	k1gInSc1	důkaz
přítomnosti	přítomnost	k1gFnSc2	přítomnost
boru	bor	k1gInSc2	bor
v	v	k7c6	v
analyzovaném	analyzovaný	k2eAgInSc6d1	analyzovaný
vzorku	vzorek	k1gInSc6	vzorek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
patří	patřit	k5eAaImIp3nS	patřit
sloučeninám	sloučenina	k1gFnPc3	sloučenina
boru	bor	k1gInSc2	bor
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
a	a	k8xC	a
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
borosilikátová	borosilikátový	k2eAgNnPc1d1	borosilikátové
skla	sklo	k1gNnPc1	sklo
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
odolností	odolnost	k1gFnSc7	odolnost
a	a	k8xC	a
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Pyrex	pyrex	k1gInSc1	pyrex
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Simax	Simax	k1gInSc1	Simax
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
chemického	chemický	k2eAgMnSc2d1	chemický
i	i	k8xC	i
kuchyňského	kuchyňský	k2eAgNnSc2d1	kuchyňské
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
keramice	keramika	k1gFnSc6	keramika
nalézá	nalézat	k5eAaImIp3nS	nalézat
bor	bor	k1gInSc4	bor
uplatnění	uplatnění	k1gNnSc2	uplatnění
především	především	k9	především
jako	jako	k8xC	jako
složka	složka	k1gFnSc1	složka
glazur	glazura	k1gFnPc2	glazura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Směs	směs	k1gFnSc1	směs
neodymu	neodym	k1gInSc2	neodym
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
boru	bor	k1gInSc2	bor
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
permanentních	permanentní	k2eAgFnPc2d1	permanentní
NdFeB	NdFeB	k1gFnPc2	NdFeB
magnetů	magnet	k1gInPc2	magnet
s	s	k7c7	s
vynikajícími	vynikající	k2eAgFnPc7d1	vynikající
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Boridy	Borid	k1gInPc1	Borid
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
boru	bor	k1gInSc2	bor
s	s	k7c7	s
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
široká	široký	k2eAgFnSc1d1	široká
škála	škála	k1gFnSc1	škála
boridů	borid	k1gInPc2	borid
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
stechiometrií	stechiometrie	k1gFnSc7	stechiometrie
a	a	k8xC	a
krystalickou	krystalický	k2eAgFnSc7d1	krystalická
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
mimořádně	mimořádně	k6eAd1	mimořádně
elektricky	elektricky	k6eAd1	elektricky
i	i	k8xC	i
tepelně	tepelně	k6eAd1	tepelně
vodivé	vodivý	k2eAgFnPc4d1	vodivá
<g/>
,	,	kIx,	,
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
<g/>
,	,	kIx,	,
žáruvzdorné	žáruvzdorný	k2eAgFnPc4d1	žáruvzdorná
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
netečné	netečný	k2eAgInPc1d1	netečný
a	a	k8xC	a
netěkavé	těkavý	k2eNgInPc1d1	těkavý
materiály	materiál	k1gInPc1	materiál
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mimořádně	mimořádně	k6eAd1	mimořádně
vodivé	vodivý	k2eAgInPc4d1	vodivý
diboridy	diborid	k1gInPc4	diborid
Zr	Zr	k1gMnPc2	Zr
<g/>
,	,	kIx,	,
Hf	Hf	k1gMnPc2	Hf
<g/>
,	,	kIx,	,
Nb	Nb	k1gMnPc2	Nb
a	a	k8xC	a
Ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tají	tajit	k5eAaImIp3nP	tajit
vesměs	vesměs	k6eAd1	vesměs
až	až	k9	až
nad	nad	k7c7	nad
3	[number]	k4	3
000	[number]	k4	000
°	°	k?	°
<g/>
C.	C.	kA	C.
TiB	tiba	k1gFnPc2	tiba
<g/>
2	[number]	k4	2
má	mít	k5eAaImIp3nS	mít
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
a	a	k8xC	a
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
vodivost	vodivost	k1gFnSc4	vodivost
5	[number]	k4	5
<g/>
krtát	krtát	k1gInSc1	krtát
vyšší	vysoký	k2eAgInSc1d2	vyšší
než	než	k8xS	než
kovový	kovový	k2eAgInSc1d1	kovový
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
borid	borid	k1gInSc4	borid
zirkonia	zirkonium	k1gNnSc2	zirkonium
ZrB	ZrB	k1gFnSc2	ZrB
<g/>
2	[number]	k4	2
dokonce	dokonce	k9	dokonce
10	[number]	k4	10
<g/>
×	×	k?	×
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Boridy	Borida	k1gFnPc1	Borida
TiB	tiba	k1gFnPc2	tiba
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ZrB	ZrB	k1gFnSc1	ZrB
<g/>
2	[number]	k4	2
a	a	k8xC	a
CrB	CrB	k1gMnSc1	CrB
<g/>
2	[number]	k4	2
našly	najít	k5eAaPmAgInP	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
lopatky	lopatka	k1gFnPc4	lopatka
turbín	turbína	k1gFnPc2	turbína
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
povrchy	povrch	k1gInPc4	povrch
spalovacích	spalovací	k2eAgFnPc2d1	spalovací
komor	komora	k1gFnPc2	komora
a	a	k8xC	a
raketových	raketový	k2eAgFnPc2d1	raketová
trysek	tryska	k1gFnPc2	tryska
<g/>
.	.	kIx.	.
</s>
<s>
Schopnosti	schopnost	k1gFnSc2	schopnost
odolávat	odolávat	k5eAaImF	odolávat
roztaveným	roztavený	k2eAgInPc3d1	roztavený
kovům	kov	k1gInPc3	kov
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vysokoteplotních	vysokoteplotní	k2eAgFnPc2d1	vysokoteplotní
reakčních	reakční	k2eAgFnPc2d1	reakční
nádob	nádoba	k1gFnPc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
jaderných	jaderný	k2eAgFnPc6d1	jaderná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
jako	jako	k8xC	jako
neutronové	neutronový	k2eAgInPc4d1	neutronový
štíty	štít	k1gInPc4	štít
a	a	k8xC	a
kontrolní	kontrolní	k2eAgFnPc4d1	kontrolní
tyče	tyč	k1gFnPc4	tyč
v	v	k7c6	v
reaktorech	reaktor	k1gInPc6	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
Borid	Borid	k1gInSc4	Borid
hořečnatý	hořečnatý	k2eAgInSc4d1	hořečnatý
Mg	mg	kA	mg
<g/>
3	[number]	k4	3
<g/>
B	B	kA	B
<g/>
2	[number]	k4	2
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
velmi	velmi	k6eAd1	velmi
perspektivní	perspektivní	k2eAgInPc4d1	perspektivní
materiály	materiál	k1gInPc4	materiál
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vývoje	vývoj	k1gInSc2	vývoj
supravodičů	supravodič	k1gInPc2	supravodič
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hodnotu	hodnota	k1gFnSc4	hodnota
kritické	kritický	k2eAgFnSc2d1	kritická
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Boridy	Borid	k1gInPc1	Borid
fosforu	fosfor	k1gInSc2	fosfor
a	a	k8xC	a
arsenu	arsen	k1gInSc2	arsen
jsou	být	k5eAaImIp3nP	být
slibné	slibný	k2eAgInPc1d1	slibný
vysokoteplotní	vysokoteplotní	k2eAgInPc1d1	vysokoteplotní
polovodiče	polovodič	k1gInPc1	polovodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nitrid	nitrid	k1gInSc1	nitrid
boritý	boritý	k2eAgInSc1d1	boritý
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
stálá	stálý	k2eAgFnSc1d1	stálá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
strukturu	struktura	k1gFnSc4	struktura
jako	jako	k8xS	jako
grafit	grafit	k1gInSc4	grafit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
patří	patřit	k5eAaImIp3nS	patřit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
diamantem	diamant	k1gInSc7	diamant
k	k	k7c3	k
nejtvrdším	tvrdý	k2eAgFnPc3d3	nejtvrdší
známým	známý	k2eAgFnPc3d1	známá
látkám	látka	k1gFnPc3	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
technologické	technologický	k2eAgInPc4d1	technologický
procesy	proces	k1gInPc4	proces
pro	pro	k7c4	pro
pokrytí	pokrytí	k1gNnSc4	pokrytí
kovových	kovový	k2eAgInPc2d1	kovový
povrchů	povrch	k1gInPc2	povrch
tímto	tento	k3xDgInSc7	tento
nitridem	nitrid	k1gInSc7	nitrid
a	a	k8xC	a
kovoobráběcí	kovoobráběcí	k2eAgInPc1d1	kovoobráběcí
nástroje	nástroj	k1gInPc1	nástroj
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
povlakem	povlak	k1gInSc7	povlak
jsou	být	k5eAaImIp3nP	být
výrazně	výrazně	k6eAd1	výrazně
tvrdší	tvrdý	k2eAgInSc4d2	tvrdší
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
odolnější	odolný	k2eAgMnSc1d2	odolnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
materiálem	materiál	k1gInSc7	materiál
je	být	k5eAaImIp3nS	být
také	také	k9	také
karbid	karbid	k1gInSc1	karbid
boru	bor	k1gInSc2	bor
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgMnSc1d1	používaný
jako	jako	k9	jako
brusivo	brusivo	k1gNnSc1	brusivo
a	a	k8xC	a
leštič	leštič	k1gInSc1	leštič
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c4	v
obložení	obložení	k1gNnSc4	obložení
brzd	brzda	k1gFnPc2	brzda
a	a	k8xC	a
spojek	spojka	k1gFnPc2	spojka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
materiálem	materiál	k1gInSc7	materiál
v	v	k7c6	v
neprůstřelných	průstřelný	k2eNgFnPc6d1	neprůstřelná
vestách	vesta	k1gFnPc6	vesta
a	a	k8xC	a
ochranných	ochranný	k2eAgInPc6d1	ochranný
štítech	štít	k1gInPc6	štít
bojových	bojový	k2eAgFnPc2d1	bojová
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
boru	bor	k1gInSc2	bor
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
borany	boran	k1gInPc1	boran
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
značně	značně	k6eAd1	značně
reaktivní	reaktivní	k2eAgFnPc1d1	reaktivní
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgFnPc2d1	další
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
borohydrid	borohydrid	k1gInSc4	borohydrid
lithný	lithný	k2eAgInSc4d1	lithný
LiBH	LiBH	k1gFnSc7	LiBH
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
mimořádně	mimořádně	k6eAd1	mimořádně
silné	silný	k2eAgNnSc4d1	silné
redukční	redukční	k2eAgNnSc4d1	redukční
činidlo	činidlo	k1gNnSc4	činidlo
a	a	k8xC	a
zdroj	zdroj	k1gInSc4	zdroj
nascentního	nascentní	k2eAgInSc2d1	nascentní
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
a	a	k8xC	a
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
boránem	borán	k1gInSc7	borán
je	být	k5eAaImIp3nS	být
diboran	diboran	k1gInSc1	diboran
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
samozápalný	samozápalný	k2eAgInSc1d1	samozápalný
plyn	plyn	k1gInSc1	plyn
o	o	k7c6	o
bodu	bod	k1gInSc6	bod
varu	var	k1gInSc2	var
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Vyšší	vysoký	k2eAgInPc1d2	vyšší
borany	boran	k1gInPc1	boran
mají	mít	k5eAaImIp3nP	mít
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
pevné	pevný	k2eAgNnSc4d1	pevné
skupenství	skupenství	k1gNnSc4	skupenství
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
stálejší	stálý	k2eAgFnPc1d2	stálejší
vůči	vůči	k7c3	vůči
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
trihydrogenboritá	trihydrogenboritý	k2eAgFnSc1d1	trihydrogenboritá
H3BO3	H3BO3	k1gFnSc1	H3BO3
je	být	k5eAaImIp3nS	být
slabá	slabý	k2eAgFnSc1d1	slabá
kyselina	kyselina	k1gFnSc1	kyselina
tvořící	tvořící	k2eAgFnPc4d1	tvořící
šupinkové	šupinkový	k2eAgFnPc4d1	šupinková
průhledné	průhledný	k2eAgFnPc4d1	průhledná
krystalky	krystalka	k1gFnPc4	krystalka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
málo	málo	k6eAd1	málo
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
a	a	k8xC	a
ve	v	k7c6	v
farmacii	farmacie	k1gFnSc6	farmacie
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
solemi	sůl	k1gFnPc7	sůl
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
ošetřováni	ošetřovat	k5eAaImNgMnP	ošetřovat
očních	oční	k2eAgFnPc2d1	oční
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc4d1	další
uplatnění	uplatnění	k1gNnSc4	uplatnění
nacházejí	nacházet	k5eAaImIp3nP	nacházet
boritany	boritan	k1gInPc4	boritan
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
přípravků	přípravek	k1gInPc2	přípravek
pro	pro	k7c4	pro
impregnaci	impregnace	k1gFnSc4	impregnace
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
rozkladem	rozklad	k1gInSc7	rozklad
boraxu	borax	k1gInSc2	borax
kyselinami	kyselina	k1gFnPc7	kyselina
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vziknout	vziknout	k5eAaPmF	vziknout
také	také	k9	také
silně	silně	k6eAd1	silně
exotermickou	exotermický	k2eAgFnSc7d1	exotermická
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
boritého	boritý	k2eAgMnSc2d1	boritý
B2O3	B2O3	k1gMnSc2	B2O3
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
praxi	praxe	k1gFnSc6	praxe
nejpoužívanější	používaný	k2eAgFnSc7d3	nejpoužívanější
sloučeninou	sloučenina	k1gFnSc7	sloučenina
boru	bor	k1gInSc2	bor
je	být	k5eAaImIp3nS	být
borax	borax	k1gInSc1	borax
neboli	neboli	k8xC	neboli
dekahydrát	dekahydrát	k1gInSc1	dekahydrát
tetraboritanu	tetraboritan	k1gInSc2	tetraboritan
sodného	sodný	k2eAgInSc2d1	sodný
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
<g/>
·	·	k?	·
<g/>
10	[number]	k4	10
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
správnější	správní	k2eAgNnPc4d2	správnější
složení	složení	k1gNnPc4	složení
ale	ale	k8xC	ale
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
název	název	k1gInSc4	název
oktahydrát	oktahydrát	k1gInSc1	oktahydrát
tetrahydroxotetraboritanu	tetrahydroxotetraboritan	k1gInSc2	tetrahydroxotetraboritan
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
vzorcem	vzorec	k1gInSc7	vzorec
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
[	[	kIx(	[
<g/>
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
·	·	k?	·
<g/>
8	[number]	k4	8
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Bezvodý	bezvodý	k2eAgInSc1d1	bezvodý
borax	borax	k1gInSc1	borax
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
tavenina	tavenina	k1gFnSc1	tavenina
překrývá	překrývat	k5eAaImIp3nS	překrývat
roztavený	roztavený	k2eAgInSc4d1	roztavený
kov	kov	k1gInSc4	kov
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
ochranný	ochranný	k2eAgInSc1d1	ochranný
prvek	prvek	k1gInSc1	prvek
proti	proti	k7c3	proti
oxidaci	oxidace	k1gFnSc3	oxidace
zpracovávané	zpracovávaný	k2eAgFnSc2d1	zpracovávaná
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
boraxu	borax	k1gInSc2	borax
s	s	k7c7	s
uhličitanem	uhličitan	k1gInSc7	uhličitan
sodným	sodný	k2eAgNnSc7d1	sodné
univerzálním	univerzální	k2eAgNnSc7d1	univerzální
tavidlem	tavidlo	k1gNnSc7	tavidlo
<g/>
,	,	kIx,	,
používaným	používaný	k2eAgNnSc7d1	používané
pro	pro	k7c4	pro
rozklady	rozklad	k1gInPc4	rozklad
geologických	geologický	k2eAgInPc2d1	geologický
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
obtížně	obtížně	k6eAd1	obtížně
rozpustných	rozpustný	k2eAgInPc2d1	rozpustný
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
pájení	pájení	k1gNnSc6	pájení
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
slitin	slitina	k1gFnPc2	slitina
(	(	kIx(	(
<g/>
mosazi	mosaz	k1gFnSc2	mosaz
<g/>
,	,	kIx,	,
Cu	Cu	k1gFnSc2	Cu
<g/>
,	,	kIx,	,
bronzu	bronz	k1gInSc2	bronz
<g/>
)	)	kIx)	)
plamenem	plamen	k1gInSc7	plamen
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
smaltovaného	smaltovaný	k2eAgNnSc2d1	smaltované
nádobí	nádobí	k1gNnSc2	nádobí
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
ochranný	ochranný	k2eAgInSc1d1	ochranný
prvek	prvek	k1gInSc1	prvek
proti	proti	k7c3	proti
oxidaci	oxidace	k1gFnSc3	oxidace
zpracovávané	zpracovávaný	k2eAgFnSc2d1	zpracovávaná
slitiny	slitina	k1gFnSc2	slitina
<g/>
)	)	kIx)	)
a	a	k8xC	a
speciálních	speciální	k2eAgNnPc2d1	speciální
optických	optický	k2eAgNnPc2d1	optické
skel	sklo	k1gNnPc2	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NaBO	NaBO	k?	NaBO
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
peroxotrihydrát	peroxotrihydrát	k1gInSc4	peroxotrihydrát
tetraboritanu	tetraboritan	k1gInSc2	tetraboritan
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
oxidační	oxidační	k2eAgNnSc1d1	oxidační
činidlo	činidlo	k1gNnSc1	činidlo
s	s	k7c7	s
bělícími	bělící	k2eAgInPc7d1	bělící
účinky	účinek	k1gInPc7	účinek
v	v	k7c6	v
textilním	textilní	k2eAgInSc6d1	textilní
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
peroxid	peroxid	k1gInSc1	peroxid
vodíku	vodík	k1gInSc2	vodík
H2O2	H2O2	k1gFnSc2	H2O2
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bor	bor	k1gInSc1	bor
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bor	bor	k1gInSc1	bor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Boron	Boron	k1gInSc1	Boron
</s>
</p>
