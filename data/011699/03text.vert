<p>
<s>
Fritz	Fritz	k1gMnSc1	Fritz
Haber	Haber	k1gMnSc1	Haber
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1868	[number]	k4	1868
Breslau	Breslaus	k1gInSc2	Breslaus
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1934	[number]	k4	1934
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzikální	fyzikální	k2eAgMnSc1d1	fyzikální
chemik	chemik	k1gMnSc1	chemik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
získal	získat	k5eAaPmAgInS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
Haber-Boschova	Haber-Boschův	k2eAgInSc2d1	Haber-Boschův
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
postupu	postup	k1gInSc2	postup
syntézy	syntéza	k1gFnSc2	syntéza
amoniaku	amoniak	k1gInSc2	amoniak
z	z	k7c2	z
plynného	plynný	k2eAgInSc2d1	plynný
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vynález	vynález	k1gInSc1	vynález
umožnil	umožnit	k5eAaPmAgInS	umožnit
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
velkovýrobu	velkovýroba	k1gFnSc4	velkovýroba
syntetických	syntetický	k2eAgNnPc2d1	syntetické
hnojiv	hnojivo	k1gNnPc2	hnojivo
a	a	k8xC	a
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Haber	Haber	k1gMnSc1	Haber
společně	společně	k6eAd1	společně
s	s	k7c7	s
Maxem	Max	k1gMnSc7	Max
Bornem	Born	k1gMnSc7	Born
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Bornův	Bornův	k2eAgMnSc1d1	Bornův
<g/>
–	–	k?	–
<g/>
Haberův	Haberův	k2eAgInSc1d1	Haberův
cyklus	cyklus	k1gInSc1	cyklus
jako	jako	k9	jako
metodu	metoda	k1gFnSc4	metoda
pro	pro	k7c4	pro
zjišťování	zjišťování	k1gNnSc4	zjišťování
mřížkové	mřížkový	k2eAgFnSc2d1	mřížková
energie	energie	k1gFnSc2	energie
iontových	iontový	k2eAgFnPc2d1	iontová
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haber	Haber	k1gInSc1	Haber
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
"	"	kIx"	"
<g/>
otce	otec	k1gMnSc2	otec
chemické	chemický	k2eAgFnSc2d1	chemická
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
dlouholeté	dlouholetý	k2eAgFnSc3d1	dlouholetá
průkopnické	průkopnický	k2eAgFnSc3d1	průkopnická
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
způsobech	způsob	k1gInPc6	způsob
bojového	bojový	k2eAgNnSc2d1	bojové
využití	využití	k1gNnSc2	využití
chlóru	chlór	k1gInSc2	chlór
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
jedovatých	jedovatý	k2eAgInPc2d1	jedovatý
plynů	plyn	k1gInPc2	plyn
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gNnSc1	jeho
nasazení	nasazení	k1gNnSc1	nasazení
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Yper	Ypera	k1gFnPc2	Ypera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
pruského	pruský	k2eAgMnSc2d1	pruský
velkoobchodníka	velkoobchodník	k1gMnSc2	velkoobchodník
s	s	k7c7	s
drogistickým	drogistický	k2eAgNnSc7d1	drogistické
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zvídavým	zvídavý	k2eAgNnSc7d1	zvídavé
gymnaziálním	gymnaziální	k2eAgNnSc7d1	gymnaziální
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
universitním	universitní	k2eAgMnSc7d1	universitní
studentem	student	k1gMnSc7	student
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
vzdělání	vzdělání	k1gNnSc6	vzdělání
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
s	s	k7c7	s
nijak	nijak	k6eAd1	nijak
skvělými	skvělý	k2eAgInPc7d1	skvělý
studijními	studijní	k2eAgInPc7d1	studijní
výsledky	výsledek	k1gInPc7	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
největším	veliký	k2eAgInSc7d3	veliký
zájmem	zájem	k1gInSc7	zájem
byla	být	k5eAaImAgFnS	být
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Váhal	váhat	k5eAaImAgMnS	váhat
mezi	mezi	k7c7	mezi
chemií	chemie	k1gFnSc7	chemie
organickou	organický	k2eAgFnSc7d1	organická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
chemie	chemie	k1gFnSc1	chemie
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
Haber	Haber	k1gInSc1	Haber
zajímal	zajímat	k5eAaImAgInS	zajímat
i	i	k9	i
o	o	k7c4	o
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
poezii	poezie	k1gFnSc4	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
uplatnit	uplatnit	k5eAaPmF	uplatnit
v	v	k7c6	v
otcově	otcův	k2eAgInSc6d1	otcův
obchodě	obchod	k1gInSc6	obchod
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
pokusy	pokus	k1gInPc1	pokus
však	však	k9	však
skončily	skončit	k5eAaPmAgInP	skončit
neslavně	slavně	k6eNd1	slavně
<g/>
.	.	kIx.	.
</s>
<s>
Uplatnění	uplatnění	k1gNnSc4	uplatnění
našel	najít	k5eAaPmAgMnS	najít
až	až	k9	až
v	v	k7c6	v
univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
výzkumné	výzkumný	k2eAgFnSc3d1	výzkumná
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
pedagogické	pedagogický	k2eAgFnSc3d1	pedagogická
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Institutu	institut	k1gInSc2	institut
císaře	císař	k1gMnSc2	císař
Viléma	Vilém	k1gMnSc2	Vilém
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Haber	Haber	k1gInSc1	Haber
byl	být	k5eAaImAgInS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
univerzitním	univerzitní	k2eAgMnSc7d1	univerzitní
profesorem	profesor	k1gMnSc7	profesor
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
studenty	student	k1gMnPc7	student
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
i	i	k8xC	i
když	když	k8xS	když
obávaným	obávaný	k2eAgMnSc7d1	obávaný
břitkým	břitký	k2eAgMnSc7d1	břitký
diskutérem	diskutér	k1gMnSc7	diskutér
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgInS	dokázat
bavit	bavit	k5eAaImF	bavit
své	svůj	k3xOyFgMnPc4	svůj
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
jistou	jistý	k2eAgFnSc7d1	jistá
dávkou	dávka	k1gFnSc7	dávka
komediálního	komediální	k2eAgNnSc2d1	komediální
a	a	k8xC	a
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
nadání	nadání	k1gNnSc2	nadání
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
významných	významný	k2eAgFnPc2d1	významná
německých	německý	k2eAgFnPc2d1	německá
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
Haber	Haber	k1gMnSc1	Haber
nebyl	být	k5eNaImAgMnS	být
žádný	žádný	k3yNgMnSc1	žádný
brutální	brutální	k2eAgMnSc1d1	brutální
válečník	válečník	k1gMnSc1	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ctižádostivým	ctižádostivý	k2eAgMnSc7d1	ctižádostivý
vlastencem	vlastenec	k1gMnSc7	vlastenec
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
oddaným	oddaný	k2eAgInSc7d1	oddaný
německému	německý	k2eAgInSc3d1	německý
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
kariéře	kariéra	k1gFnSc6	kariéra
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
německého	německý	k2eAgInSc2d1	německý
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
ho	on	k3xPp3gInSc4	on
jeho	on	k3xPp3gInSc4	on
židovský	židovský	k2eAgInSc4d1	židovský
původ	původ	k1gInSc4	původ
citelně	citelně	k6eAd1	citelně
znevýhodňoval	znevýhodňovat	k5eAaImAgMnS	znevýhodňovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
narůstajícího	narůstající	k2eAgInSc2d1	narůstající
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ypres	Ypres	k1gInSc4	Ypres
==	==	k?	==
</s>
</p>
<p>
<s>
Haber	Haber	k1gInSc1	Haber
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
"	"	kIx"	"
<g/>
otce	otec	k1gMnSc2	otec
<g/>
"	"	kIx"	"
skutečné	skutečný	k2eAgFnPc1d1	skutečná
chemické	chemický	k2eAgFnPc1d1	chemická
války	válka	k1gFnPc1	válka
<g/>
,	,	kIx,	,
zrozené	zrozený	k2eAgFnPc1d1	zrozená
při	při	k7c6	při
chemickém	chemický	k2eAgInSc6d1	chemický
útoku	útok	k1gInSc6	útok
Němců	Němec	k1gMnPc2	Němec
u	u	k7c2	u
belgické	belgický	k2eAgFnSc2d1	belgická
obce	obec	k1gFnSc2	obec
Ypres	Ypres	k1gInSc4	Ypres
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útoku	útok	k1gInSc6	útok
bylo	být	k5eAaImAgNnS	být
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
asi	asi	k9	asi
170	[number]	k4	170
tun	tuna	k1gFnPc2	tuna
chlóru	chlór	k1gInSc2	chlór
z	z	k7c2	z
5	[number]	k4	5
700	[number]	k4	700
tlakových	tlakový	k2eAgFnPc2d1	tlaková
lahví	lahev	k1gFnPc2	lahev
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
šestikilometrovém	šestikilometrový	k2eAgInSc6d1	šestikilometrový
úseku	úsek	k1gInSc6	úsek
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
lahví	lahev	k1gFnPc2	lahev
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
kolem	kolem	k7c2	kolem
30	[number]	k4	30
kg	kg	kA	kg
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ocelové	ocelový	k2eAgFnPc1d1	ocelová
lahve	lahev	k1gFnPc1	lahev
byly	být	k5eAaImAgFnP	být
opatřeny	opatřit	k5eAaPmNgFnP	opatřit
sifonovou	sifonový	k2eAgFnSc7d1	sifonová
trubkou	trubka	k1gFnSc7	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
unikat	unikat	k5eAaImF	unikat
mohla	moct	k5eAaImAgFnS	moct
kapalná	kapalný	k2eAgFnSc1d1	kapalná
fáze	fáze	k1gFnSc1	fáze
chlóru	chlór	k1gInSc2	chlór
<g/>
.	.	kIx.	.
</s>
<s>
Tlakové	tlakový	k2eAgFnPc1d1	tlaková
lahve	lahev	k1gFnPc1	lahev
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
svislé	svislý	k2eAgFnSc6d1	svislá
poloze	poloha	k1gFnSc6	poloha
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
rozestupech	rozestup	k1gInPc6	rozestup
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
do	do	k7c2	do
německých	německý	k2eAgInPc2d1	německý
zákopů	zákop	k1gInPc2	zákop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
uzávěry	uzávěra	k1gFnPc4	uzávěra
lahví	lahev	k1gFnPc2	lahev
byly	být	k5eAaImAgFnP	být
našroubovány	našroubován	k2eAgFnPc1d1	našroubována
olověné	olověný	k2eAgFnPc1d1	olověná
trubice	trubice	k1gFnPc1	trubice
<g/>
,	,	kIx,	,
sahající	sahající	k2eAgFnPc1d1	sahající
nad	nad	k7c4	nad
okraj	okraj	k1gInSc4	okraj
zákopu	zákop	k1gInSc2	zákop
a	a	k8xC	a
směřující	směřující	k2eAgFnPc1d1	směřující
k	k	k7c3	k
zákopům	zákop	k1gInPc3	zákop
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Zkapalněný	zkapalněný	k2eAgInSc1d1	zkapalněný
chlór	chlór	k1gInSc1	chlór
po	po	k7c4	po
vypuštění	vypuštění	k1gNnSc4	vypuštění
začal	začít	k5eAaPmAgInS	začít
okamžitě	okamžitě	k6eAd1	okamžitě
vytvářet	vytvářet	k5eAaImF	vytvářet
těžká	těžký	k2eAgNnPc4d1	těžké
žlutozelená	žlutozelený	k2eAgNnPc4d1	žlutozelené
mračna	mračno	k1gNnPc4	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
zvolna	zvolna	k6eAd1	zvolna
mísila	mísit	k5eAaImAgFnS	mísit
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Příhodný	příhodný	k2eAgInSc1d1	příhodný
vítr	vítr	k1gInSc1	vítr
hnal	hnát	k5eAaImAgInS	hnát
oblaka	oblaka	k1gNnPc4	oblaka
k	k	k7c3	k
pozicím	pozice	k1gFnPc3	pozice
nepřítele	nepřítel	k1gMnSc2	nepřítel
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
plynný	plynný	k2eAgInSc1d1	plynný
chlór	chlór	k1gInSc1	chlór
je	být	k5eAaImIp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
plyn	plyn	k1gInSc1	plyn
zatékal	zatékat	k5eAaImAgInS	zatékat
do	do	k7c2	do
zákopů	zákop	k1gInPc2	zákop
a	a	k8xC	a
veškerých	veškerý	k3xTgFnPc2	veškerý
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Chlór	chlór	k1gInSc1	chlór
leptá	leptat	k5eAaImIp3nS	leptat
veškeré	veškerý	k3xTgFnPc4	veškerý
sliznice	sliznice	k1gFnPc4	sliznice
včetně	včetně	k7c2	včetně
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
napadených	napadený	k2eAgInPc2d1	napadený
orgánů	orgán	k1gInPc2	orgán
se	se	k3xPyFc4	se
vylévá	vylévat	k5eAaImIp3nS	vylévat
krev	krev	k1gFnSc1	krev
<g/>
.	.	kIx.	.
</s>
<s>
Krví	krev	k1gFnSc7	krev
se	se	k3xPyFc4	se
zalévá	zalévat	k5eAaImIp3nS	zalévat
i	i	k9	i
vnitřek	vnitřek	k1gInSc1	vnitřek
zasažených	zasažený	k2eAgFnPc2d1	zasažená
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
úst	ústa	k1gNnPc2	ústa
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
krvavá	krvavý	k2eAgFnSc1d1	krvavá
pěna	pěna	k1gFnSc1	pěna
<g/>
.	.	kIx.	.
</s>
<s>
Krutá	krutý	k2eAgFnSc1d1	krutá
a	a	k8xC	a
bolestivá	bolestivý	k2eAgFnSc1d1	bolestivá
smrt	smrt	k1gFnSc1	smrt
trvá	trvat	k5eAaImIp3nS	trvat
i	i	k9	i
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
koncentraci	koncentrace	k1gFnSc6	koncentrace
plynu	plynout	k5eAaImIp1nS	plynout
až	až	k9	až
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
i	i	k8xC	i
dny	den	k1gInPc1	den
pomalého	pomalý	k2eAgNnSc2d1	pomalé
umírání	umírání	k1gNnSc2	umírání
čekaly	čekat	k5eAaImAgFnP	čekat
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
byli	být	k5eAaImAgMnP	být
zasaženi	zasáhnout	k5eAaPmNgMnP	zasáhnout
plynem	plyn	k1gInSc7	plyn
o	o	k7c6	o
menší	malý	k2eAgFnSc6d2	menší
koncentraci	koncentrace	k1gFnSc6	koncentrace
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
těžce	těžce	k6eAd1	těžce
zraněných	zraněný	k2eAgMnPc2d1	zraněný
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
7	[number]	k4	7
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
zasažených	zasažený	k2eAgFnPc2d1	zasažená
chlórem	chlór	k1gInSc7	chlór
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
350	[number]	k4	350
takřka	takřka	k6eAd1	takřka
okamžitě	okamžitě	k6eAd1	okamžitě
</s>
</p>
<p>
<s>
další	další	k2eAgMnPc1d1	další
zemřelí	zemřelý	k1gMnPc1	zemřelý
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
než	než	k8xS	než
vojenských	vojenský	k2eAgFnPc2d1	vojenská
příčin	příčina	k1gFnPc2	příčina
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
opustili	opustit	k5eAaPmAgMnP	opustit
zákopy	zákop	k1gInPc4	zákop
a	a	k8xC	a
prchali	prchat	k5eAaImAgMnP	prchat
před	před	k7c7	před
smrtícím	smrtící	k2eAgInSc7d1	smrtící
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
byli	být	k5eAaImAgMnP	být
koseni	kosen	k2eAgMnPc1d1	kosen
palbou	palba	k1gFnSc7	palba
německých	německý	k2eAgFnPc2d1	německá
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
postupujících	postupující	k2eAgMnPc2d1	postupující
za	za	k7c7	za
jedovatým	jedovatý	k2eAgInSc7d1	jedovatý
oblakem	oblak	k1gInSc7	oblak
</s>
</p>
<p>
<s>
==	==	k?	==
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
syntézu	syntéza	k1gFnSc4	syntéza
amoniaku	amoniak	k1gInSc2	amoniak
z	z	k7c2	z
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
syntetických	syntetický	k2eAgNnPc2d1	syntetické
hnojiv	hnojivo	k1gNnPc2	hnojivo
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
dusičnanu	dusičnan	k1gInSc2	dusičnan
amonného	amonný	k2eAgInSc2d1	amonný
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
k	k	k7c3	k
ryze	ryze	k6eAd1	ryze
humánním	humánní	k2eAgInPc3d1	humánní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Využit	využít	k5eAaPmNgInS	využít
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
obrovských	obrovský	k2eAgFnPc2d1	obrovská
materiálních	materiální	k2eAgFnPc2d1	materiální
potřeb	potřeba	k1gFnPc2	potřeba
německé	německý	k2eAgFnSc2d1	německá
válečné	válečný	k2eAgFnSc2d1	válečná
výroby	výroba	k1gFnSc2	výroba
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Haber	Haber	k1gMnSc1	Haber
si	se	k3xPyFc3	se
cenu	cena	k1gFnSc4	cena
převzal	převzít	k5eAaPmAgInS	převzít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
chlórovém	chlórový	k2eAgInSc6d1	chlórový
útoku	útok	k1gInSc6	útok
v	v	k7c4	v
Ypres	Ypres	k1gInSc4	Ypres
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
válečných	válečný	k2eAgMnPc2d1	válečný
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
hledaných	hledaný	k2eAgInPc2d1	hledaný
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
dokonce	dokonce	k9	dokonce
skrývat	skrývat	k5eAaImF	skrývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Haber	Haber	k1gMnSc1	Haber
cítil	cítit	k5eAaImAgMnS	cítit
jako	jako	k9	jako
německý	německý	k2eAgMnSc1d1	německý
vlastenec	vlastenec	k1gMnSc1	vlastenec
povinnost	povinnost	k1gFnSc4	povinnost
pomoci	pomoct	k5eAaPmF	pomoct
své	svůj	k3xOyFgFnSc3	svůj
zemi	zem	k1gFnSc3	zem
s	s	k7c7	s
placením	placení	k1gNnSc7	placení
válečných	válečný	k2eAgFnPc2d1	válečná
reparací	reparace	k1gFnPc2	reparace
formou	forma	k1gFnSc7	forma
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
analýz	analýza	k1gFnPc2	analýza
švédského	švédský	k2eAgMnSc2d1	švédský
chemika	chemik	k1gMnSc2	chemik
Svante	Svant	k1gInSc5	Svant
Arrhenia	Arrhenium	k1gNnSc2	Arrhenium
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
měla	mít	k5eAaImAgFnS	mít
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
obsahovat	obsahovat	k5eAaImF	obsahovat
na	na	k7c4	na
6	[number]	k4	6
mg	mg	kA	mg
zlata	zlato	k1gNnSc2	zlato
na	na	k7c4	na
tunu	tuna	k1gFnSc4	tuna
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
6	[number]	k4	6
ppb	ppb	k?	ppb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
koncentraci	koncentrace	k1gFnSc6	koncentrace
by	by	kYmCp3nP	by
celkové	celkový	k2eAgFnPc1d1	celková
zásoby	zásoba	k1gFnPc1	zásoba
zlata	zlato	k1gNnSc2	zlato
ve	v	k7c6	v
světových	světový	k2eAgInPc6d1	světový
oceánech	oceán	k1gInPc6	oceán
činily	činit	k5eAaImAgInP	činit
8	[number]	k4	8
miliard	miliarda	k4xCgFnPc2	miliarda
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
roční	roční	k2eAgFnSc1d1	roční
světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
obnášela	obnášet	k5eAaImAgFnS	obnášet
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Haber	Haber	k1gMnSc1	Haber
nejprve	nejprve	k6eAd1	nejprve
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
vzorky	vzorek	k1gInPc7	vzorek
v	v	k7c6	v
berlínské	berlínský	k2eAgFnSc6d1	Berlínská
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
plul	plout	k5eAaImAgInS	plout
po	po	k7c6	po
světových	světový	k2eAgInPc6d1	světový
oceánech	oceán	k1gInPc6	oceán
<g/>
;	;	kIx,	;
analýzy	analýza	k1gFnSc2	analýza
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
vycházely	vycházet	k5eAaImAgInP	vycházet
nižší	nízký	k2eAgInPc1d2	nižší
a	a	k8xC	a
Haber	Haber	k1gInSc1	Haber
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nerentability	nerentabilita	k1gFnSc2	nerentabilita
projekt	projekt	k1gInSc1	projekt
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cyklon	cyklon	k1gInSc4	cyklon
B	B	kA	B
==	==	k?	==
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Haber	Haber	k1gInSc1	Haber
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
relativně	relativně	k6eAd1	relativně
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
vysoce	vysoce	k6eAd1	vysoce
toxického	toxický	k2eAgInSc2d1	toxický
kyanovodíku	kyanovodík	k1gInSc2	kyanovodík
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
záměr	záměr	k1gInSc1	záměr
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
vysoce	vysoce	k6eAd1	vysoce
humánní	humánní	k2eAgNnSc1d1	humánní
<g/>
:	:	kIx,	:
zbavit	zbavit	k5eAaPmF	zbavit
ubytovací	ubytovací	k2eAgFnPc4d1	ubytovací
a	a	k8xC	a
skladové	skladový	k2eAgFnPc4d1	skladová
prostory	prostora	k1gFnPc4	prostora
zejména	zejména	k9	zejména
ve	v	k7c6	v
vojenských	vojenský	k2eAgInPc6d1	vojenský
prostorech	prostor	k1gInPc6	prostor
obtížného	obtížný	k2eAgInSc2d1	obtížný
hmyzu	hmyz	k1gInSc2	hmyz
či	či	k8xC	či
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc4	využití
našel	najít	k5eAaPmAgMnS	najít
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
opět	opět	k6eAd1	opět
u	u	k7c2	u
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
jméno	jméno	k1gNnSc1	jméno
nového	nový	k2eAgInSc2d1	nový
prostředku	prostředek	k1gInSc2	prostředek
<g/>
:	:	kIx,	:
Cyklon	cyklon	k1gInSc1	cyklon
B.	B.	kA	B.
Bylo	být	k5eAaImAgNnS	být
jím	on	k3xPp3gMnSc7	on
zavražděno	zavraždit	k5eAaPmNgNnS	zavraždit
i	i	k9	i
několik	několik	k4yIc1	několik
blízkých	blízký	k2eAgMnPc2d1	blízký
Haberových	Haberův	k2eAgMnPc2d1	Haberův
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akademická	akademický	k2eAgFnSc1d1	akademická
fundovanost	fundovanost	k1gFnSc1	fundovanost
kolektivu	kolektiv	k1gInSc2	kolektiv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
Haber	Haber	k1gInSc4	Haber
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
chemického	chemický	k2eAgNnSc2d1	chemické
válčení	válčení	k1gNnSc2	válčení
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
udivující	udivující	k2eAgFnSc1d1	udivující
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
Haberova	Haberův	k2eAgInSc2d1	Haberův
vědeckého	vědecký	k2eAgInSc2d1	vědecký
kolektivu	kolektiv	k1gInSc2	kolektiv
Franz	Franz	k1gMnSc1	Franz
Richardt	Richardt	k1gMnSc1	Richardt
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Haber	Haber	k1gMnSc1	Haber
pátral	pátrat	k5eAaImAgMnS	pátrat
po	po	k7c6	po
všech	všecek	k3xTgInPc6	všecek
odbornících	odborník	k1gMnPc6	odborník
na	na	k7c4	na
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
chemii	chemie	k1gFnSc4	chemie
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
kdy	kdy	k6eAd1	kdy
doslechl	doslechnout	k5eAaPmAgMnS	doslechnout
...	...	k?	...
V	v	k7c6	v
době	doba	k1gFnSc6	doba
mého	můj	k3xOp1gInSc2	můj
příchodu	příchod	k1gInSc2	příchod
už	už	k6eAd1	už
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
Kerschbaum	Kerschbaum	k1gInSc1	Kerschbaum
<g/>
,	,	kIx,	,
Haberův	Haberův	k2eAgMnSc1d1	Haberův
technický	technický	k2eAgMnSc1d1	technický
pobočník	pobočník	k1gMnSc1	pobočník
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
<g />
.	.	kIx.	.
</s>
<s>
Otto	Otto	k1gMnSc1	Otto
Hahn	Hahn	k1gMnSc1	Hahn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
později	pozdě	k6eAd2	pozdě
rozbil	rozbít	k5eAaPmAgMnS	rozbít
atomové	atomový	k2eAgNnSc4d1	atomové
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
James	James	k1gMnSc1	James
Franck	Franck	k1gMnSc1	Franck
(	(	kIx(	(
<g/>
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
později	pozdě	k6eAd2	pozdě
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Hans	Hans	k1gMnSc1	Hans
Geiger	Geiger	k1gMnSc1	Geiger
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
později	pozdě	k6eAd2	pozdě
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
Geiger-Müllerův	Geiger-Müllerův	k2eAgInSc1d1	Geiger-Müllerův
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Walter	Walter	k1gMnSc1	Walter
Madelung	Madelung	k1gMnSc1	Madelung
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pracující	pracující	k2eAgMnSc1d1	pracující
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
ve	v	k7c6	v
Freiburgu	Freiburg	k1gInSc6	Freiburg
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
barvářské	barvářský	k2eAgFnSc2d1	barvářská
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Westphal	Westphal	k1gMnSc1	Westphal
<g/>
,	,	kIx,	,
neurolog	neurolog	k1gMnSc1	neurolog
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
činnosti	činnost	k1gFnSc6	činnost
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
další	další	k2eAgMnPc1d1	další
pozdější	pozdní	k2eAgMnPc1d2	pozdější
nositelé	nositel	k1gMnPc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
jako	jako	k8xC	jako
fyzik	fyzik	k1gMnSc1	fyzik
Max	Max	k1gMnSc1	Max
Planck	Planck	k1gMnSc1	Planck
nebo	nebo	k8xC	nebo
chemik	chemik	k1gMnSc1	chemik
Emil	Emil	k1gMnSc1	Emil
Hermann	Hermann	k1gMnSc1	Hermann
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tragédie	tragédie	k1gFnSc1	tragédie
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
válečném	válečný	k2eAgNnSc6d1	válečné
dění	dění	k1gNnSc6	dění
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
i	i	k8xC	i
jeho	on	k3xPp3gInSc4	on
soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1915	[number]	k4	1915
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
doma	doma	k6eAd1	doma
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
dubnovém	dubnový	k2eAgInSc6d1	dubnový
"	"	kIx"	"
<g/>
chemickém	chemický	k2eAgInSc6d1	chemický
triumfu	triumf	k1gInSc6	triumf
<g/>
"	"	kIx"	"
spáchala	spáchat	k5eAaPmAgFnS	spáchat
jeho	jeho	k3xOp3gFnSc4	jeho
žena	žena	k1gFnSc1	žena
Clara	Clara	k1gFnSc1	Clara
sebevraždu	sebevražda	k1gFnSc4	sebevražda
dvěma	dva	k4xCgInPc7	dva
výstřely	výstřel	k1gInPc7	výstřel
z	z	k7c2	z
Haberovy	Haberův	k2eAgFnSc2d1	Haberova
armádní	armádní	k2eAgFnSc2d1	armádní
pistole	pistol	k1gFnSc2	pistol
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
náručí	náručí	k1gNnSc6	náručí
třináctiletého	třináctiletý	k2eAgMnSc2d1	třináctiletý
syna	syn	k1gMnSc2	syn
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
jejich	jejich	k3xOp3gFnSc2	jejich
honosné	honosný	k2eAgFnSc2d1	honosná
služební	služební	k2eAgFnSc2d1	služební
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
Berlíně-Dahlemu	Berlíně-Dahlem	k1gInSc6	Berlíně-Dahlem
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
rasovým	rasový	k2eAgInPc3d1	rasový
zákonům	zákon	k1gInPc3	zákon
opustil	opustit	k5eAaPmAgMnS	opustit
nacistické	nacistický	k2eAgNnSc4d1	nacistické
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
klima	klima	k1gNnSc1	klima
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nesvědčilo	svědčit	k5eNaImAgNnS	svědčit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
měsíční	měsíční	k2eAgInSc1d1	měsíční
kráter	kráter	k1gInSc1	kráter
Haber	Habra	k1gFnPc2	Habra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
Master	master	k1gMnSc1	master
mind	mind	k1gMnSc1	mind
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Rise	Ris	k1gFnSc2	Ris
and	and	k?	and
Fall	Fall	k1gMnSc1	Fall
of	of	k?	of
Fritz	Fritz	k1gMnSc1	Fritz
Haber	Haber	k1gMnSc1	Haber
<g/>
,	,	kIx,	,
the	the	k?	the
Nobel	Nobel	k1gMnSc1	Nobel
Laureate	Laureat	k1gInSc5	Laureat
Who	Who	k1gFnPc1	Who
Launched	Launched	k1gMnSc1	Launched
the	the	k?	the
Age	Age	k1gMnSc1	Age
of	of	k?	of
Chemical	Chemical	k1gMnSc5	Chemical
Warfare	Warfar	k1gMnSc5	Warfar
(	(	kIx(	(
<g/>
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Ecco	Ecco	k1gNnSc1	Ecco
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
56272	[number]	k4	56272
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dietrich	Dietrich	k1gMnSc1	Dietrich
Stoltzenberg	Stoltzenberg	k1gMnSc1	Stoltzenberg
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Haber	Haber	k1gMnSc1	Haber
<g/>
:	:	kIx,	:
Chemist	Chemist	k1gMnSc1	Chemist
<g/>
,	,	kIx,	,
Nobel	Nobel	k1gMnSc1	Nobel
Laureate	Laureat	k1gInSc5	Laureat
<g/>
,	,	kIx,	,
German	German	k1gMnSc1	German
<g/>
,	,	kIx,	,
Jew	Jew	k1gMnSc1	Jew
<g/>
:	:	kIx,	:
A	A	kA	A
Biography	Biograph	k1gInPc1	Biograph
(	(	kIx(	(
<g/>
Chemical	Chemical	k1gFnSc1	Chemical
Heritage	Heritag	k1gFnSc2	Heritag
Foundation	Foundation	k1gInSc1	Foundation
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
941901	[number]	k4	941901
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vaclav	Vaclat	k5eAaPmDgInS	Vaclat
Smil	smil	k1gInSc1	smil
<g/>
,	,	kIx,	,
Enriching	Enriching	k1gInSc1	Enriching
the	the	k?	the
Earth	Earth	k1gInSc1	Earth
<g/>
:	:	kIx,	:
Fritz	Fritz	k1gMnSc1	Fritz
Haber	Haber	k1gMnSc1	Haber
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Bosch	Bosch	kA	Bosch
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Transformation	Transformation	k1gInSc1	Transformation
of	of	k?	of
World	World	k1gInSc1	World
Food	Food	k1gMnSc1	Food
Production	Production	k1gInSc1	Production
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
0-262-19449-X	[number]	k4	0-262-19449-X
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Kolár	Kolár	k1gMnSc1	Kolár
<g/>
,	,	kIx,	,
Homo	Homo	k1gMnSc1	Homo
Faber	Faber	k1gMnSc1	Faber
(	(	kIx(	(
<g/>
Aura-Pont	Aura-Pont	k1gMnSc1	Aura-Pont
<g/>
;	;	kIx,	;
nepublikováno	publikován	k2eNgNnSc1d1	nepublikováno
<g/>
)	)	kIx)	)
-	-	kIx~	-
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
dramatické	dramatický	k2eAgFnSc6d1	dramatická
soutěži	soutěž	k1gFnSc6	soutěž
o	o	k7c4	o
Cenu	cena	k1gFnSc4	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fritz	Fritza	k1gFnPc2	Fritza
Haber	Habero	k1gNnPc2	Habero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
nobel	nobel	k1gInSc1	nobel
<g/>
.	.	kIx.	.
<g/>
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
ze	z	k7c2	z
života	život	k1gInSc2	život
Fritze	Fritze	k1gFnSc1	Fritze
Habera	Habera	k1gFnSc1	Habera
</s>
</p>
