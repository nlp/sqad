<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
získal	získat	k5eAaPmAgInS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
Haber-Boschova	Haber-Boschův	k2eAgInSc2d1	Haber-Boschův
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
postupu	postup	k1gInSc2	postup
syntézy	syntéza	k1gFnSc2	syntéza
amoniaku	amoniak	k1gInSc2	amoniak
z	z	k7c2	z
plynného	plynný	k2eAgInSc2d1	plynný
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
