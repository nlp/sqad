<s>
Livermorium	Livermorium	k1gNnSc1	Livermorium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Lv	Lv	k1gFnSc1	Lv
<g/>
,	,	kIx,	,
pův	pův	k?	pův
<g/>
.	.	kIx.	.
ununhexium	ununhexium	k1gNnSc1	ununhexium
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Livermorium	Livermorium	k1gNnSc1	Livermorium
je	být	k5eAaImIp3nS	být
transuran	transuran	k1gInSc4	transuran
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
116	[number]	k4	116
<g/>
.	.	kIx.	.
</s>
