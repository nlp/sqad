<s>
Einstein	Einstein	k1gMnSc1	Einstein
pak	pak	k8xC	pak
vysvětlil	vysvětlit	k5eAaPmAgInS	vysvětlit
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
fotoelektrický	fotoelektrický	k2eAgInSc4d1	fotoelektrický
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
