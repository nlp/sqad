<s>
Hydrosféra	hydrosféra	k1gFnSc1	hydrosféra
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
71	[number]	k4	71
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
97	[number]	k4	97
%	%	kIx~	%
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
3	[number]	k4	3
%	%	kIx~	%
sladká	sladký	k2eAgFnSc1d1	sladká
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
oceány	oceán	k1gInPc7	oceán
a	a	k8xC	a
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
dohromady	dohromady	k6eAd1	dohromady
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
světový	světový	k2eAgInSc1d1	světový
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kontinentech	kontinent	k1gInPc6	kontinent
pak	pak	k6eAd1	pak
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
