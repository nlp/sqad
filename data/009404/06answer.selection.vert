<s>
Čachtická	čachtický	k2eAgFnSc1d1	Čachtická
paní	paní	k1gFnSc1	paní
je	být	k5eAaImIp3nS	být
historicko-dobrodružný	historickoobrodružný	k2eAgInSc4d1	historicko-dobrodružný
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
napsal	napsat	k5eAaBmAgInS	napsat
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
slovenský	slovenský	k2eAgInSc1d1	slovenský
romanopisec	romanopisec	k1gMnSc1	romanopisec
Jožo	Joža	k1gMnSc5	Joža
Nižnánsky	Nižnánsky	k1gMnSc5	Nižnánsky
<g/>
.	.	kIx.	.
</s>
