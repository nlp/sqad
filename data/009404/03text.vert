<p>
<s>
Čachtická	čachtický	k2eAgFnSc1d1	Čachtická
paní	paní	k1gFnSc1	paní
je	být	k5eAaImIp3nS	být
historicko-dobrodružný	historickoobrodružný	k2eAgInSc4d1	historicko-dobrodružný
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napsal	napsat	k5eAaPmAgInS	napsat
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
slovenský	slovenský	k2eAgInSc1d1	slovenský
romanopisec	romanopisec	k1gMnSc1	romanopisec
Jožo	Joža	k1gMnSc5	Joža
Nižnánsky	Nižnánsky	k1gMnSc5	Nižnánsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Čachtic	Čachtice	k1gFnPc2	Čachtice
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
situace	situace	k1gFnPc1	situace
se	se	k3xPyFc4	se
však	však	k9	však
řeší	řešit	k5eAaImIp3nS	řešit
i	i	k9	i
v	v	k7c6	v
Prešpurku	Prešpurku	k?	Prešpurku
(	(	kIx(	(
<g/>
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
období	období	k1gNnSc3	období
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Román	román	k1gInSc1	román
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
o	o	k7c6	o
Alžbětě	Alžběta	k1gFnSc6	Alžběta
Báthoryové	Báthoryová	k1gFnSc2	Báthoryová
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
sadistické	sadistický	k2eAgFnSc6d1	sadistická
hraběnce	hraběnka	k1gFnSc6	hraběnka
koupající	koupající	k2eAgFnPc1d1	koupající
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
krásy	krása	k1gFnSc2	krása
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
mladých	mladý	k2eAgFnPc2d1	mladá
dívek	dívka	k1gFnPc2	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
odvážných	odvážný	k2eAgMnPc6d1	odvážný
zbojnících	zbojník	k1gMnPc6	zbojník
bojující	bojující	k2eAgMnSc1d1	bojující
proti	proti	k7c3	proti
krutostem	krutost	k1gFnPc3	krutost
zlé	zlý	k2eAgFnSc2d1	zlá
hraběnky	hraběnka	k1gFnSc2	hraběnka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
útrapách	útrapa	k1gFnPc6	útrapa
<g/>
,	,	kIx,	,
střetech	střet	k1gInPc6	střet
s	s	k7c7	s
pandury	pandur	k1gMnPc7	pandur
a	a	k8xC	a
hajduky	hajduk	k1gMnPc7	hajduk
<g/>
,	,	kIx,	,
záchranných	záchranný	k2eAgFnPc6d1	záchranná
akcích	akce	k1gFnPc6	akce
unesených	unesený	k2eAgFnPc2d1	unesená
dívek	dívka	k1gFnPc2	dívka
a	a	k8xC	a
úniků	únik	k1gInPc2	únik
před	před	k7c7	před
oprátkou	oprátka	k1gFnSc7	oprátka
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomocí	pomoc	k1gFnPc2	pomoc
jejich	jejich	k3xOp3gMnPc2	jejich
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
podaří	podařit	k5eAaPmIp3nS	podařit
přimět	přimět	k5eAaPmF	přimět
palatina	palatin	k1gMnSc4	palatin
Juraje	Juraj	k1gInSc2	Juraj
Thurza	Thurza	k1gFnSc1	Thurza
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
tuto	tento	k3xDgFnSc4	tento
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Palatin	Palatin	k1gInSc1	Palatin
neočekávaně	očekávaně	k6eNd1	očekávaně
přijede	přijet	k5eAaPmIp3nS	přijet
do	do	k7c2	do
Čachtic	Čachtice	k1gFnPc2	Čachtice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přistihne	přistihnout	k5eAaPmIp3nS	přistihnout
Alžbětu	Alžběta	k1gFnSc4	Alžběta
Báthoryovou	Báthoryový	k2eAgFnSc4d1	Báthoryová
při	při	k7c6	při
činu	čin	k1gInSc6	čin
a	a	k8xC	a
nakáže	nakázat	k5eAaBmIp3nS	nakázat
jí	on	k3xPp3gFnSc3	on
doživotní	doživotní	k2eAgNnSc1d1	doživotní
uvěznění	uvěznění	k1gNnSc1	uvěznění
v	v	k7c4	v
podzemí	podzemí	k1gNnSc4	podzemí
jejího	její	k3xOp3gInSc2	její
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
jakožto	jakožto	k8xS	jakožto
k	k	k7c3	k
faktům	fakt	k1gInPc3	fakt
bohatě	bohatě	k6eAd1	bohatě
rozvitému	rozvitý	k2eAgInSc3d1	rozvitý
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
fantazií	fantazie	k1gFnSc7	fantazie
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
vytvořit	vytvořit	k5eAaPmF	vytvořit
vhodný	vhodný	k2eAgInSc4d1	vhodný
dobový	dobový	k2eAgInSc4d1	dobový
historický	historický	k2eAgInSc4d1	historický
rámec	rámec	k1gInSc4	rámec
do	do	k7c2	do
něhož	jenž	k3xRgNnSc2	jenž
zasazuje	zasazovat	k5eAaImIp3nS	zasazovat
děj	děj	k1gInSc1	děj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
skutečně	skutečně	k6eAd1	skutečně
existovaly	existovat	k5eAaImAgFnP	existovat
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
okrajových	okrajový	k2eAgFnPc2d1	okrajová
postav	postava	k1gFnPc2	postava
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
právě	právě	k9	právě
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
lepší	dobrý	k2eAgFnSc2d2	lepší
konkretizace	konkretizace	k1gFnSc2	konkretizace
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
rozlišit	rozlišit	k5eAaPmF	rozlišit
důležitost	důležitost	k1gFnSc4	důležitost
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
existují	existovat	k5eAaImIp3nP	existovat
spletité	spletitý	k2eAgInPc4d1	spletitý
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
===	===	k?	===
</s>
</p>
<p>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Báthoryová	Báthoryová	k1gFnSc1	Báthoryová
je	být	k5eAaImIp3nS	být
brutální	brutální	k2eAgFnSc1d1	brutální
žena	žena	k1gFnSc1	žena
bez	bez	k7c2	bez
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
bez	bez	k7c2	bez
skrupulí	skrupule	k1gFnPc2	skrupule
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vyžívá	vyžívat	k5eAaImIp3nS	vyžívat
v	v	k7c6	v
týrání	týrání	k1gNnSc6	týrání
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
moc	moc	k6eAd1	moc
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
se	se	k3xPyFc4	se
obávat	obávat	k5eAaImF	obávat
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
krvavé	krvavý	k2eAgInPc4d1	krvavý
zločiny	zločin	k1gInPc4	zločin
jen	jen	k9	jen
tak	tak	k6eAd1	tak
přišlo	přijít	k5eAaPmAgNnS	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
služebníků	služebník	k1gMnPc2	služebník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jí	on	k3xPp3gFnSc3	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
páchat	páchat	k5eAaImF	páchat
její	její	k3xOp3gInPc4	její
hrůzné	hrůzný	k2eAgInPc4d1	hrůzný
činy	čin	k1gInPc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
například	například	k6eAd1	například
Ficko	ficka	k1gFnSc5	ficka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podlý	podlý	k2eAgMnSc1d1	podlý
a	a	k8xC	a
zrádný	zrádný	k2eAgMnSc1d1	zrádný
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
velmi	velmi	k6eAd1	velmi
odporný	odporný	k2eAgInSc4d1	odporný
<g/>
,	,	kIx,	,
zakrslý	zakrslý	k2eAgInSc4d1	zakrslý
<g/>
,	,	kIx,	,
chlupatý	chlupatý	k2eAgInSc4d1	chlupatý
s	s	k7c7	s
hrbem	hrb	k1gInSc7	hrb
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hraběnčiny	hraběnčin	k2eAgInPc4d1	hraběnčin
rozkazy	rozkaz	k1gInPc4	rozkaz
podniká	podnikat	k5eAaImIp3nS	podnikat
množství	množství	k1gNnSc1	množství
honů	hon	k1gInPc2	hon
na	na	k7c4	na
zbojníky	zbojník	k1gMnPc4	zbojník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
vůdcem	vůdce	k1gMnSc7	vůdce
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
urostlý	urostlý	k2eAgInSc1d1	urostlý
a	a	k8xC	a
odvážný	odvážný	k2eAgMnSc1d1	odvážný
Andrej	Andrej	k1gMnSc1	Andrej
Drozd	Drozd	k1gMnSc1	Drozd
<g/>
.	.	kIx.	.
</s>
<s>
Přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Kalina	Kalina	k1gMnSc1	Kalina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
wittenberské	wittenberský	k2eAgFnSc6d1	wittenberský
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
zaslechl	zaslechnout	k5eAaPmAgMnS	zaslechnout
zvěsti	zvěst	k1gFnPc4	zvěst
o	o	k7c6	o
čachtickém	čachtický	k2eAgNnSc6d1	čachtický
krveprolévání	krveprolévání	k1gNnSc6	krveprolévání
<g/>
,	,	kIx,	,
přijel	přijet	k5eAaPmAgMnS	přijet
chránit	chránit	k5eAaImF	chránit
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
a	a	k8xC	a
vypátrat	vypátrat	k5eAaPmF	vypátrat
svou	svůj	k3xOyFgFnSc4	svůj
sestru	sestra	k1gFnSc4	sestra
Magdu	Magda	k1gFnSc4	Magda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zmizela	zmizet	k5eAaPmAgFnS	zmizet
za	za	k7c2	za
podivných	podivný	k2eAgFnPc2d1	podivná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
chytrý	chytrý	k2eAgInSc1d1	chytrý
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
druhy	druh	k1gInPc7	druh
zinscenují	zinscenovat	k5eAaPmIp3nP	zinscenovat
řadu	řada	k1gFnSc4	řada
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
a	a	k8xC	a
promyšlených	promyšlený	k2eAgInPc2d1	promyšlený
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Daří	dařit	k5eAaImIp3nS	dařit
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
spojenců	spojenec	k1gMnPc2	spojenec
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Ponicenus	Ponicenus	k1gMnSc1	Ponicenus
<g/>
,	,	kIx,	,
vážený	vážený	k2eAgMnSc1d1	vážený
a	a	k8xC	a
moudrý	moudrý	k2eAgMnSc1d1	moudrý
čachtický	čachtický	k2eAgMnSc1d1	čachtický
protestantský	protestantský	k2eAgMnSc1d1	protestantský
farář	farář	k1gMnSc1	farář
<g/>
,	,	kIx,	,
a	a	k8xC	a
Janův	Janův	k2eAgMnSc1d1	Janův
kamarád	kamarád	k1gMnSc1	kamarád
Pavel	Pavel	k1gMnSc1	Pavel
Lederer	Lederer	k1gMnSc1	Lederer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
jako	jako	k9	jako
hradní	hradní	k2eAgMnSc1d1	hradní
zámečník	zámečník	k1gMnSc1	zámečník
a	a	k8xC	a
získá	získat	k5eAaPmIp3nS	získat
si	se	k3xPyFc3	se
Fickovu	Fickův	k2eAgFnSc4d1	Fickova
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
cenným	cenný	k2eAgFnPc3d1	cenná
informacím	informace	k1gFnPc3	informace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
si	se	k3xPyFc3	se
získají	získat	k5eAaPmIp3nP	získat
i	i	k9	i
mladou	mladý	k2eAgFnSc4d1	mladá
krásku	kráska	k1gFnSc4	kráska
Eržiku	Eržika	k1gFnSc4	Eržika
Príborskou	Príborský	k2eAgFnSc4d1	Príborský
<g/>
,	,	kIx,	,
utajenou	utajený	k2eAgFnSc4d1	utajená
nemanželskou	manželský	k2eNgFnSc4d1	nemanželská
dceru	dcera	k1gFnSc4	dcera
Alžběty	Alžběta	k1gFnSc2	Alžběta
Báthoryové	Báthoryová	k1gFnSc2	Báthoryová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Andreje	Andrej	k1gMnSc2	Andrej
Drozda	Drozd	k1gMnSc2	Drozd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vypravěč	vypravěč	k1gMnSc1	vypravěč
===	===	k?	===
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
vyprávěn	vyprávět	k5eAaImNgInS	vyprávět
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
občasně	občasně	k6eAd1	občasně
vložená	vložený	k2eAgFnSc1d1	vložená
stránka	stránka	k1gFnSc1	stránka
z	z	k7c2	z
čachtické	čachtický	k2eAgFnSc2d1	Čachtická
farské	farský	k2eAgFnSc2d1	Farská
kroniky	kronika	k1gFnSc2	kronika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
můžeme	moct	k5eAaImIp1nP	moct
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
ich-formě	ichorma	k1gFnSc6	ich-forma
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vypravěčem	vypravěč	k1gMnSc7	vypravěč
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
farář	farář	k1gMnSc1	farář
Jan	Jan	k1gMnSc1	Jan
Poniceus	Poniceus	k1gMnSc1	Poniceus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kompozice	kompozice	k1gFnSc2	kompozice
===	===	k?	===
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
21	[number]	k4	21
hlavních	hlavní	k2eAgFnPc2d1	hlavní
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
skládá	skládat	k5eAaImIp3nS	skládat
ještě	ještě	k9	ještě
z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
podkapitol	podkapitola	k1gFnPc2	podkapitola
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
10	[number]	k4	10
na	na	k7c4	na
kapitolu	kapitola	k1gFnSc4	kapitola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c4	o
paralelní	paralelní	k2eAgFnSc4d1	paralelní
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
příběhu	příběh	k1gInSc6	příběh
objevuje	objevovat	k5eAaImIp3nS	objevovat
spousta	spousta	k1gFnSc1	spousta
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
osudů	osud	k1gInPc2	osud
<g/>
,	,	kIx,	,
zápletek	zápletka	k1gFnPc2	zápletka
a	a	k8xC	a
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
rozuzlení	rozuzlení	k1gNnSc2	rozuzlení
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
nevzniká	vznikat	k5eNaImIp3nS	vznikat
zmatek	zmatek	k1gInSc1	zmatek
<g/>
,	,	kIx,	,
děj	děj	k1gInSc1	děj
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc4d1	rychlý
spád	spád	k1gInSc4	spád
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
poutavý	poutavý	k2eAgInSc4d1	poutavý
a	a	k8xC	a
napínavý	napínavý	k2eAgInSc4d1	napínavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
motivů	motiv	k1gInPc2	motiv
týče	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
nejvýznamněji	významně	k6eAd3	významně
figuruje	figurovat	k5eAaImIp3nS	figurovat
motiv	motiv	k1gInSc4	motiv
usilovného	usilovný	k2eAgInSc2d1	usilovný
boje	boj	k1gInSc2	boj
dobra	dobro	k1gNnSc2	dobro
proti	proti	k7c3	proti
zlu	zlo	k1gNnSc3	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
ještě	ještě	k9	ještě
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
dobrodružnými	dobrodružný	k2eAgInPc7d1	dobrodružný
a	a	k8xC	a
také	také	k6eAd1	také
romantickými	romantický	k2eAgInPc7d1	romantický
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
jazykové	jazykový	k2eAgInPc1d1	jazykový
prostředky	prostředek	k1gInPc1	prostředek
===	===	k?	===
</s>
</p>
<p>
<s>
Stylový	stylový	k2eAgInSc1d1	stylový
ráz	ráz	k1gInSc1	ráz
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
spisovně	spisovně	k6eAd1	spisovně
<g/>
,	,	kIx,	,
občasně	občasně	k6eAd1	občasně
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
knižním	knižní	k2eAgInSc7d1	knižní
výrazem	výraz	k1gInSc7	výraz
či	či	k8xC	či
historismem	historismus	k1gInSc7	historismus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
lepšího	dobrý	k2eAgNnSc2d2	lepší
nastínění	nastínění	k1gNnSc2	nastínění
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
také	také	k9	také
někdy	někdy	k6eAd1	někdy
užita	užit	k2eAgFnSc1d1	užita
inverze	inverze	k1gFnSc1	inverze
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
řeči	řeč	k1gFnSc6	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
textu	text	k1gInSc6	text
mnoho	mnoho	k4c1	mnoho
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
užití	užití	k1gNnSc1	užití
přímé	přímý	k2eAgFnSc2d1	přímá
řeči	řeč	k1gFnSc2	řeč
četné	četný	k2eAgFnSc2d1	četná
<g/>
.	.	kIx.	.
</s>
<s>
Užívána	užíván	k2eAgFnSc1d1	užívána
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
i	i	k9	i
řeč	řeč	k1gFnSc1	řeč
nepřímá	přímý	k2eNgFnSc1d1	nepřímá
<g/>
,	,	kIx,	,
nevlastní	vlastnit	k5eNaImIp3nP	vlastnit
přímá	přímý	k2eAgNnPc4d1	přímé
a	a	k8xC	a
autorská	autorský	k2eAgNnPc4d1	autorské
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
krátké	krátký	k2eAgFnPc1d1	krátká
věty	věta	k1gFnPc1	věta
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
složitá	složitý	k2eAgNnPc1d1	složité
souvětí	souvětí	k1gNnPc1	souvětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tropy	trop	k1gInPc1	trop
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
textu	text	k1gInSc6	text
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
přirovnání	přirovnání	k1gNnSc1	přirovnání
<g/>
,	,	kIx,	,
ironie	ironie	k1gFnSc1	ironie
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
řeči	řeč	k1gFnSc6	řeč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
personifikace	personifikace	k1gFnSc1	personifikace
(	(	kIx(	(
<g/>
při	při	k7c6	při
popisu	popis	k1gInSc6	popis
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dysfemismus	dysfemismus	k1gInSc1	dysfemismus
<g/>
,	,	kIx,	,
litotes	litotes	k1gNnSc1	litotes
aj.	aj.	kA	aj.
V	v	k7c6	v
částech	část	k1gFnPc6	část
textu	text	k1gInSc2	text
o	o	k7c6	o
zbojnících	zbojník	k1gMnPc6	zbojník
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
silně	silně	k6eAd1	silně
meliorativní	meliorativní	k2eAgNnSc1d1	meliorativní
zabarvení	zabarvení	k1gNnSc1	zabarvení
<g/>
,	,	kIx,	,
pejorativní	pejorativní	k2eAgNnSc1d1	pejorativní
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
užito	užít	k5eAaPmNgNnS	užít
např.	např.	kA	např.
při	při	k7c6	při
popisu	popis	k1gInSc6	popis
krveprolévání	krveprolévání	k1gNnSc2	krveprolévání
či	či	k8xC	či
Fickovy	Fickův	k2eAgFnSc2d1	Fickova
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
použití	použití	k1gNnSc1	použití
figur	figura	k1gFnPc2	figura
setkat	setkat	k5eAaPmF	setkat
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
např.	např.	kA	např.
s	s	k7c7	s
asyndetonem	asyndeton	k1gInSc7	asyndeton
<g/>
,	,	kIx,	,
zeugmou	zeugma	k1gFnSc7	zeugma
<g/>
,	,	kIx,	,
kakofonií	kakofonie	k1gFnSc7	kakofonie
<g/>
,	,	kIx,	,
eufonií	eufonie	k1gFnSc7	eufonie
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Účelem	účel	k1gInSc7	účel
těchto	tento	k3xDgInPc2	tento
uměleckých	umělecký	k2eAgInPc2d1	umělecký
prostředků	prostředek	k1gInPc2	prostředek
je	být	k5eAaImIp3nS	být
vyvolat	vyvolat	k5eAaPmF	vyvolat
silnější	silný	k2eAgInPc4d2	silnější
pocity	pocit	k1gInPc4	pocit
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc4d2	lepší
představitelnost	představitelnost	k1gFnSc4	představitelnost
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pokračování	pokračování	k1gNnSc1	pokračování
==	==	k?	==
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Nižnánsky	Nižnánsky	k1gMnSc1	Nižnánsky
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
historických	historický	k2eAgInPc2d1	historický
dobrodružných	dobrodružný	k2eAgInPc2d1	dobrodružný
románů	román	k1gInPc2	román
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
také	také	k9	také
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
Žena	žena	k1gFnSc1	žena
dvou	dva	k4xCgMnPc2	dva
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
volným	volný	k2eAgNnSc7d1	volné
pokračováním	pokračování	k1gNnSc7	pokračování
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
Čachtická	čachtický	k2eAgFnSc1d1	Čachtická
paní	paní	k1gFnSc1	paní
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
nešťastném	šťastný	k2eNgNnSc6d1	nešťastné
manželství	manželství	k1gNnSc6	manželství
Zuzany	Zuzana	k1gFnSc2	Zuzana
Forgáčové	Forgáčová	k1gFnSc2	Forgáčová
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Revayem	Revay	k1gMnSc7	Revay
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
odhalení	odhalení	k1gNnSc6	odhalení
zločinů	zločin	k1gInPc2	zločin
Alžběty	Alžběta	k1gFnSc2	Alžběta
Báthoryové	Báthoryová	k1gFnSc2	Báthoryová
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgNnSc4d1	stejné
pozadí	pozadí	k1gNnSc4	pozadí
a	a	k8xC	a
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
osudy	osud	k1gInPc4	osud
některých	některý	k3yIgFnPc2	některý
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Čachtické	čachtický	k2eAgFnSc2d1	Čachtická
paní	paní	k1gFnSc2	paní
<g/>
.	.	kIx.	.
</s>
</p>
