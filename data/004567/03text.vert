<s>
Motýli	motýl	k1gMnPc1	motýl
(	(	kIx(	(
<g/>
Lepidoptera	Lepidopter	k1gMnSc4	Lepidopter
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
řád	řád	k1gInSc1	řád
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
broucích	brouk	k1gMnPc6	brouk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
rozšířeni	rozšířen	k2eAgMnPc1d1	rozšířen
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
kromě	kromě	k7c2	kromě
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
,	,	kIx,	,
řád	řád	k1gInSc1	řád
čítá	čítat	k5eAaImIp3nS	čítat
kolem	kolem	k7c2	kolem
180	[number]	k4	180
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Motýly	motýl	k1gMnPc4	motýl
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
lepidopterologie	lepidopterologie	k1gFnSc1	lepidopterologie
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
motýlů	motýl	k1gMnPc2	motýl
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
páry	pár	k1gInPc4	pár
vzdušnicemi	vzdušnice	k1gFnPc7	vzdušnice
protkaných	protkaný	k2eAgFnPc2d1	protkaná
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
drobnými	drobný	k2eAgFnPc7d1	drobná
šupinkami	šupinka	k1gFnPc7	šupinka
(	(	kIx(	(
<g/>
squamulae	squamulae	k1gInSc1	squamulae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šupinky	šupinka	k1gFnPc1	šupinka
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
hustě	hustě	k6eAd1	hustě
rub	rub	k1gInSc4	rub
i	i	k8xC	i
líc	líc	k1gInSc4	líc
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
překrývají	překrývat	k5eAaImIp3nP	překrývat
se	se	k3xPyFc4	se
jako	jako	k9	jako
tašky	taška	k1gFnPc1	taška
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Lesklé	lesklý	k2eAgNnSc1d1	lesklé
zbarvení	zbarvení	k1gNnSc1	zbarvení
motýlích	motýlí	k2eAgNnPc2d1	motýlí
křídel	křídlo	k1gNnPc2	křídlo
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
lomem	lom	k1gInSc7	lom
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
šupinkách	šupinka	k1gFnPc6	šupinka
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
pigmenty	pigment	k1gInPc7	pigment
pteriny	pterin	k1gInPc1	pterin
dávají	dávat	k5eAaImIp3nP	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
nádherným	nádherný	k2eAgFnPc3d1	nádherná
barvám	barva	k1gFnPc3	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vlastní	vlastní	k2eAgFnSc4d1	vlastní
tisícům	tisíc	k4xCgInPc3	tisíc
motýlích	motýlí	k2eAgMnPc2d1	motýlí
druhů	druh	k1gMnPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
název	název	k1gInSc1	název
motýlů	motýl	k1gMnPc2	motýl
<g/>
,	,	kIx,	,
lepidoptera	lepidopter	k1gMnSc2	lepidopter
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
šupinokřídlí	šupinokřídlý	k2eAgMnPc1d1	šupinokřídlý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starší	starý	k2eAgFnSc6d2	starší
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
motýli	motýl	k1gMnPc1	motýl
skutečně	skutečně	k6eAd1	skutečně
označování	označování	k1gNnSc4	označování
jako	jako	k8xC	jako
šupinokřídlý	šupinokřídlý	k2eAgInSc4d1	šupinokřídlý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Motýli	motýl	k1gMnPc1	motýl
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
nesytkovitých	sytkovitý	k2eNgFnPc2d1	sytkovitý
(	(	kIx(	(
<g/>
Sesiidae	Sesiidae	k1gFnPc2	Sesiidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
křídla	křídlo	k1gNnPc4	křídlo
krytá	krytý	k2eAgNnPc4d1	kryté
šupinkami	šupinka	k1gFnPc7	šupinka
jen	jen	k9	jen
první	první	k4xOgInPc4	první
dny	den	k1gInPc4	den
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
z	z	k7c2	z
kukly	kukla	k1gFnSc2	kukla
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
šupinky	šupinka	k1gFnPc1	šupinka
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
mají	mít	k5eAaImIp3nP	mít
křídla	křídlo	k1gNnPc1	křídlo
průhledná	průhledný	k2eAgNnPc1d1	průhledné
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
šupinek	šupinka	k1gFnPc2	šupinka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
to	ten	k3xDgNnSc1	ten
prostý	prostý	k2eAgInSc1d1	prostý
důvod	důvod	k1gInSc1	důvod
–	–	k?	–
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Připomínají	připomínat	k5eAaImIp3nP	připomínat
totiž	totiž	k9	totiž
blanokřídlé	blanokřídlý	k2eAgInPc1d1	blanokřídlý
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vosy	vosa	k1gFnSc2	vosa
a	a	k8xC	a
sršně	sršeň	k1gFnSc2	sršeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
znakem	znak	k1gInSc7	znak
motýlů	motýl	k1gMnPc2	motýl
je	být	k5eAaImIp3nS	být
savé	savý	k2eAgNnSc4d1	savé
ústní	ústní	k2eAgNnSc4d1	ústní
ústrojí	ústrojí	k1gNnSc4	ústrojí
dospělců	dospělec	k1gMnPc2	dospělec
(	(	kIx(	(
<g/>
sosák	sosák	k1gInSc1	sosák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klidu	klid	k1gInSc6	klid
je	být	k5eAaImIp3nS	být
stočené	stočený	k2eAgNnSc1d1	stočené
a	a	k8xC	a
když	když	k8xS	když
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
podráždění	podráždění	k1gNnSc3	podráždění
chuťového	chuťový	k2eAgNnSc2d1	chuťové
ústrojí	ústrojí	k1gNnSc2	ústrojí
na	na	k7c6	na
chodidlech	chodidlo	k1gNnPc6	chodidlo
<g/>
,	,	kIx,	,
reflexivně	reflexivně	k6eAd1	reflexivně
se	se	k3xPyFc4	se
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Primitivní	primitivní	k2eAgFnSc1d1	primitivní
čeleď	čeleď	k1gFnSc1	čeleď
chrostíkovníkovitých	chrostíkovníkovitý	k2eAgInPc2d1	chrostíkovníkovitý
sosák	sosák	k1gInSc4	sosák
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
motýlů	motýl	k1gMnPc2	motýl
mají	mít	k5eAaImIp3nP	mít
ústní	ústní	k2eAgNnSc4d1	ústní
ústrojí	ústrojí	k1gNnSc4	ústrojí
zakrnělé	zakrnělý	k2eAgNnSc4d1	zakrnělé
a	a	k8xC	a
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
nepřijímají	přijímat	k5eNaImIp3nP	přijímat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
ale	ale	k9	ale
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dospělí	dospělý	k2eAgMnPc1d1	dospělý
motýli	motýl	k1gMnPc1	motýl
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
nektarem	nektar	k1gInSc7	nektar
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
užiteční	užitečný	k2eAgMnPc1d1	užitečný
jako	jako	k8xS	jako
opylovači	opylovač	k1gMnPc1	opylovač
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Motýli	motýl	k1gMnPc1	motýl
jsou	být	k5eAaImIp3nP	být
gonochoristé	gonochorista	k1gMnPc1	gonochorista
s	s	k7c7	s
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
proměnou	proměna	k1gFnSc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
larvy	larva	k1gFnPc1	larva
<g/>
,	,	kIx,	,
housenky	housenka	k1gFnPc1	housenka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
kousací	kousací	k2eAgNnSc4d1	kousací
ústní	ústní	k2eAgNnSc4d1	ústní
ústrojí	ústrojí	k1gNnSc4	ústrojí
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
jsou	být	k5eAaImIp3nP	být
býložravé	býložravý	k2eAgInPc1d1	býložravý
a	a	k8xC	a
specializované	specializovaný	k2eAgInPc1d1	specializovaný
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
rostliny	rostlina	k1gFnPc4	rostlina
nebo	nebo	k8xC	nebo
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
housenky	housenka	k1gFnSc2	housenka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
hrudi	hruď	k1gFnSc2	hruď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
tři	tři	k4xCgInPc4	tři
páry	pár	k1gInPc4	pár
článkovaných	článkovaný	k2eAgFnPc2d1	článkovaná
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
a	a	k8xC	a
zadečku	zadeček	k1gInSc6	zadeček
tvořeného	tvořený	k2eAgInSc2d1	tvořený
deseti	deset	k4xCc7	deset
články	článek	k1gInPc1	článek
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
počtem	počet	k1gInSc7	počet
panožek	panožka	k1gFnPc2	panožka
<g/>
.	.	kIx.	.
</s>
<s>
Housenky	housenka	k1gFnPc1	housenka
jsou	být	k5eAaImIp3nP	být
vítanou	vítaný	k2eAgFnSc7d1	vítaná
potravou	potrava	k1gFnSc7	potrava
mnoha	mnoho	k4c2	mnoho
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
pasivně	pasivně	k6eAd1	pasivně
chrání	chránit	k5eAaImIp3nS	chránit
pomocí	pomocí	k7c2	pomocí
trnů	trn	k1gInPc2	trn
<g/>
,	,	kIx,	,
chlupů	chlup	k1gInPc2	chlup
<g/>
,	,	kIx,	,
nepříjemné	příjemný	k2eNgFnSc2d1	nepříjemná
chuti	chuť	k1gFnSc2	chuť
nebo	nebo	k8xC	nebo
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
či	či	k8xC	či
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
ochranného	ochranný	k2eAgNnSc2d1	ochranné
zbarvení	zbarvení	k1gNnSc2	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
kladou	klást	k5eAaImIp3nP	klást
až	až	k9	až
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
rostlinu	rostlina	k1gFnSc4	rostlina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
housenky	housenka	k1gFnSc2	housenka
<g/>
.	.	kIx.	.
</s>
<s>
Housenky	housenka	k1gFnPc1	housenka
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
rostlinnou	rostlinný	k2eAgFnSc7d1	rostlinná
potravou	potrava	k1gFnSc7	potrava
a	a	k8xC	a
po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zakuklí	zakuklit	k5eAaPmIp3nS	zakuklit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
housenek	housenka	k1gFnPc2	housenka
spřádá	spřádat	k5eAaImIp3nS	spřádat
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
předou	příst	k5eAaImIp3nP	příst
společná	společný	k2eAgNnPc4d1	společné
hnízda	hnízdo	k1gNnPc4	hnízdo
(	(	kIx(	(
<g/>
bourec	bourec	k1gMnSc1	bourec
prstenčitý	prstenčitý	k2eAgMnSc1d1	prstenčitý
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kokony	kokon	k1gInPc1	kokon
<g/>
.	.	kIx.	.
</s>
<s>
Motýli	motýl	k1gMnPc1	motýl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nežijí	žít	k5eNaImIp3nP	žít
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
vyřešit	vyřešit	k5eAaPmF	vyřešit
problém	problém	k1gInSc4	problém
přežití	přežití	k1gNnSc2	přežití
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
přezimují	přezimovat	k5eAaBmIp3nP	přezimovat
pouze	pouze	k6eAd1	pouze
vývojová	vývojový	k2eAgNnPc1d1	vývojové
stadia	stadion	k1gNnPc1	stadion
<g/>
,	,	kIx,	,
housenky	housenka	k1gFnPc1	housenka
nebo	nebo	k8xC	nebo
kukly	kukla	k1gFnPc1	kukla
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
přezimují	přezimovat	k5eAaBmIp3nP	přezimovat
dospělí	dospělý	k2eAgMnPc1d1	dospělý
motýli	motýl	k1gMnPc1	motýl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
babočka	babočka	k1gFnSc1	babočka
kopřivová	kopřivový	k2eAgFnSc1d1	kopřivová
<g/>
,	,	kIx,	,
žluťásek	žluťásek	k1gInSc1	žluťásek
řešetlákový	řešetlákový	k2eAgInSc1d1	řešetlákový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
tažné	tažný	k2eAgInPc1d1	tažný
(	(	kIx(	(
<g/>
babočka	babočka	k1gFnSc1	babočka
admirál	admirál	k1gMnSc1	admirál
<g/>
,	,	kIx,	,
babočka	babočka	k1gFnSc1	babočka
bodláková	bodlákový	k2eAgFnSc1d1	Bodláková
<g/>
,	,	kIx,	,
monarcha	monarcha	k1gMnSc1	monarcha
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
dospělých	dospělý	k2eAgMnPc2d1	dospělý
motýlů	motýl	k1gMnPc2	motýl
opylovat	opylovat	k5eAaImF	opylovat
květy	květ	k1gInPc4	květ
je	být	k5eAaImIp3nS	být
člověkem	člověk	k1gMnSc7	člověk
vítána	vítat	k5eAaImNgFnS	vítat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
housenky	housenka	k1gFnPc1	housenka
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
značné	značný	k2eAgFnPc4d1	značná
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
kulturními	kulturní	k2eAgFnPc7d1	kulturní
rostlinami	rostlina	k1gFnPc7	rostlina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bělásek	bělásek	k1gMnSc1	bělásek
zelný	zelný	k2eAgMnSc1d1	zelný
<g/>
,	,	kIx,	,
píďalka	píďalka	k1gFnSc1	píďalka
zhoubná	zhoubný	k2eAgFnSc1d1	zhoubná
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
"	"	kIx"	"
<g/>
červivost	červivost	k1gFnSc1	červivost
<g/>
"	"	kIx"	"
ovoce	ovoce	k1gNnSc1	ovoce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obaleč	obaleč	k1gMnSc1	obaleč
jablečný	jablečný	k2eAgMnSc1d1	jablečný
<g/>
,	,	kIx,	,
obaleč	obaleč	k1gMnSc1	obaleč
švestkový	švestkový	k2eAgMnSc1d1	švestkový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
znehodnocují	znehodnocovat	k5eAaImIp3nP	znehodnocovat
potraviny	potravina	k1gFnPc1	potravina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mol	mol	k1gInSc4	mol
obilný	obilný	k2eAgInSc1d1	obilný
<g/>
,	,	kIx,	,
zavíječ	zavíječ	k1gInSc1	zavíječ
moučný	moučný	k2eAgInSc1d1	moučný
<g/>
,	,	kIx,	,
zavíječ	zavíječ	k1gInSc1	zavíječ
domácí	domácí	k2eAgInSc1d1	domácí
<g/>
)	)	kIx)	)
či	či	k8xC	či
tkaniny	tkanina	k1gFnSc2	tkanina
(	(	kIx(	(
<g/>
mol	mol	k1gInSc1	mol
šatní	šatní	k2eAgInSc1d1	šatní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nechvalně	chvalně	k6eNd1	chvalně
známá	známý	k2eAgFnSc1d1	známá
bekyně	bekyně	k1gFnSc1	bekyně
mniška	mniška	k1gFnSc1	mniška
při	při	k7c6	při
přemnožení	přemnožení	k1gNnSc6	přemnožení
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
holožíry	holožír	k1gInPc4	holožír
a	a	k8xC	a
zničení	zničení	k1gNnSc4	zničení
tisíců	tisíc	k4xCgInPc2	tisíc
hektarů	hektar	k1gInPc2	hektar
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
nepůvodní	původní	k2eNgFnSc1d1	nepůvodní
klíněnka	klíněnka	k1gFnSc1	klíněnka
jírovcová	jírovcový	k2eAgFnSc1d1	jírovcová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
listy	list	k1gInPc4	list
jírovce	jírovec	k1gInSc2	jírovec
maďalu	maďal	k1gInSc2	maďal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
zatím	zatím	k6eAd1	zatím
nemá	mít	k5eNaImIp3nS	mít
přirozeného	přirozený	k2eAgMnSc4d1	přirozený
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Bourec	bourec	k1gMnSc1	bourec
morušový	morušový	k2eAgMnSc1d1	morušový
je	být	k5eAaImIp3nS	být
domestikovaným	domestikovaný	k2eAgInSc7d1	domestikovaný
druhem	druh	k1gInSc7	druh
motýla	motýl	k1gMnSc2	motýl
<g/>
,	,	kIx,	,
dospělci	dospělec	k1gMnPc1	dospělec
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
přežít	přežít	k5eAaPmF	přežít
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vlákno	vlákno	k1gNnSc1	vlákno
ze	z	k7c2	z
zámotků	zámotek	k1gInPc2	zámotek
se	se	k3xPyFc4	se
spřádá	spřádat	k5eAaImIp3nS	spřádat
na	na	k7c4	na
surové	surový	k2eAgNnSc4d1	surové
hedvábí	hedvábí	k1gNnSc4	hedvábí
<g/>
,	,	kIx,	,
samotné	samotný	k2eAgFnPc4d1	samotná
kukly	kukla	k1gFnPc4	kukla
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
potravina	potravina	k1gFnSc1	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Motýli	motýl	k1gMnPc1	motýl
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
denní	denní	k2eAgInPc4d1	denní
a	a	k8xC	a
noční	noční	k2eAgMnPc4d1	noční
motýly	motýl	k1gMnPc4	motýl
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
soumračné	soumračný	k2eAgNnSc4d1	Soumračné
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
účelové	účelový	k2eAgNnSc1d1	účelové
a	a	k8xC	a
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
vědeckému	vědecký	k2eAgInSc3d1	vědecký
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
denním	denní	k2eAgInSc7d1	denní
a	a	k8xC	a
nočním	noční	k2eAgMnSc7d1	noční
motýlem	motýl	k1gMnSc7	motýl
<g/>
:	:	kIx,	:
Noční	noční	k2eAgMnPc1d1	noční
motýli	motýl	k1gMnPc1	motýl
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
aktivní	aktivní	k2eAgMnPc1d1	aktivní
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
noční	noční	k2eAgMnPc1d1	noční
motýli	motýl	k1gMnPc1	motýl
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
denní	denní	k2eAgMnPc1d1	denní
motýli	motýl	k1gMnPc1	motýl
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Sedící	sedící	k2eAgMnPc1d1	sedící
noční	noční	k2eAgMnPc1d1	noční
motýli	motýl	k1gMnPc1	motýl
většinou	většinou	k6eAd1	většinou
drží	držet	k5eAaImIp3nP	držet
křídla	křídlo	k1gNnPc4	křídlo
střechovitě	střechovitě	k6eAd1	střechovitě
složená	složený	k2eAgNnPc4d1	složené
<g/>
,	,	kIx,	,
denní	denní	k2eAgMnPc1d1	denní
motýli	motýl	k1gMnPc1	motýl
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
s	s	k7c7	s
křídly	křídlo	k1gNnPc7	křídlo
složenými	složený	k2eAgInPc7d1	složený
nad	nad	k7c7	nad
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Lidový	lidový	k2eAgInSc1d1	lidový
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
můra	můra	k1gFnSc1	můra
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
noční	noční	k2eAgMnPc4d1	noční
motýly	motýl	k1gMnPc4	motýl
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
biology	biolog	k1gMnPc4	biolog
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
–	–	k?	–
jako	jako	k9	jako
národní	národní	k2eAgNnSc4d1	národní
rodové	rodový	k2eAgNnSc4d1	rodové
jméno	jméno	k1gNnSc4	jméno
mnoha	mnoho	k4c2	mnoho
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
můrovitých	můrovitý	k2eAgMnPc2d1	můrovitý
(	(	kIx(	(
<g/>
Noctuidae	Noctuida	k1gMnSc2	Noctuida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc4	řád
motýli	motýl	k1gMnPc1	motýl
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
asi	asi	k9	asi
130	[number]	k4	130
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
,	,	kIx,	,
v	v	k7c6	v
systému	systém	k1gInSc6	systém
motýlů	motýl	k1gMnPc2	motýl
ale	ale	k8xC	ale
nepanuje	panovat	k5eNaImIp3nS	panovat
přílišná	přílišný	k2eAgFnSc1d1	přílišná
shoda	shoda	k1gFnSc1	shoda
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
autory	autor	k1gMnPc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
Mnichovské	mnichovský	k2eAgNnSc1d1	mnichovské
Muzeum	muzeum	k1gNnSc1	muzeum
Witt	Witta	k1gFnPc2	Witta
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc4d3	veliký
sbírku	sbírka	k1gFnSc4	sbírka
můr	můra	k1gFnPc2	můra
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
EXLER	EXLER	kA	EXLER
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Motýlové	motýl	k1gMnPc1	motýl
<g/>
:	:	kIx,	:
soustavný	soustavný	k2eAgInSc4d1	soustavný
popis	popis	k1gInSc4	popis
motýlů	motýl	k1gMnPc2	motýl
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
žijících	žijící	k2eAgMnPc2d1	žijící
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
I.L.	I.L.	k1gFnSc1	I.L.
<g/>
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
215	[number]	k4	215
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
JOHN	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
motýlů	motýl	k1gMnPc2	motýl
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
:	:	kIx,	:
50	[number]	k4	50
tabulí	tabule	k1gFnPc2	tabule
barvotiskových	barvotiskový	k2eAgFnPc2d1	barvotisková
s	s	k7c7	s
1300	[number]	k4	1300
obrazy	obraz	k1gInPc7	obraz
motýlů	motýl	k1gMnPc2	motýl
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
I.L.	I.L.	k1gFnSc1	I.L.
<g/>
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
motýli	motýl	k1gMnPc1	motýl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
motýl	motýl	k1gMnSc1	motýl
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Lepidoptera	Lepidopter	k1gMnSc2	Lepidopter
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
Seznam	seznam	k1gInSc4	seznam
motýlů	motýl	k1gMnPc2	motýl
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Mapování	mapování	k1gNnSc1	mapování
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
motýlů	motýl	k1gMnPc2	motýl
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
