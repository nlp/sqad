<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
(	(	kIx(	(
<g/>
stručně	stručně	k6eAd1	stručně
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
nebo	nebo	k8xC	nebo
nepřesně	přesně	k6eNd1	přesně
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
United	United	k1gMnSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
of	of	k?	of
Great	Great	k2eAgInSc1d1	Great
Britain	Britain	k1gInSc1	Britain
and	and	k?	and
Northern	Northern	k1gInSc1	Northern
Ireland	Ireland	k1gInSc1	Ireland
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
stát	stát	k1gInSc1	stát
přiléhající	přiléhající	k2eAgInSc1d1	přiléhající
k	k	k7c3	k
západu	západ	k1gInSc3	západ
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Geograficky	geograficky	k6eAd1	geograficky
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ostrov	ostrov	k1gInSc1	ostrov
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Irskou	irský	k2eAgFnSc7d1	irská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ohraničeno	ohraničen	k2eAgNnSc1d1	ohraničeno
jednak	jednak	k8xC	jednak
hranicí	hranice	k1gFnSc7	hranice
s	s	k7c7	s
Irskem	Irsko	k1gNnSc7	Irsko
a	a	k8xC	a
jednak	jednak	k8xC	jednak
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gFnPc7	jeho
lokálními	lokální	k2eAgFnPc7d1	lokální
částmi	část	k1gFnPc7	část
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Severním	severní	k2eAgNnSc7d1	severní
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
Lamanšským	lamanšský	k2eAgInSc7d1	lamanšský
průlivem	průliv	k1gInSc7	průliv
<g/>
,	,	kIx,	,
Keltským	keltský	k2eAgNnSc7d1	keltské
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
Průlivem	průliv	k1gInSc7	průliv
svatého	svatý	k2eAgMnSc4d1	svatý
Jiří	Jiří	k1gMnSc4	Jiří
a	a	k8xC	a
Irským	irský	k2eAgNnSc7d1	irské
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kontinentální	kontinentální	k2eAgFnSc7d1	kontinentální
Evropou	Evropa	k1gFnSc7	Evropa
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
Eurotunelem	Eurotunel	k1gInSc7	Eurotunel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
monarchií	monarchie	k1gFnSc7	monarchie
složenou	složený	k2eAgFnSc7d1	složená
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
a	a	k8xC	a
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
provincii	provincie	k1gFnSc4	provincie
nebo	nebo	k8xC	nebo
region	region	k1gInSc4	region
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
za	za	k7c4	za
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
suverenitu	suverenita	k1gFnSc4	suverenita
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
spadá	spadat	k5eAaImIp3nS	spadat
též	též	k9	též
jeho	jeho	k3xOp3gNnPc2	jeho
14	[number]	k4	14
zámořských	zámořský	k2eAgNnPc2d1	zámořské
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
jsou	být	k5eAaImIp3nP	být
zbytky	zbytek	k1gInPc4	zbytek
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Britské	britský	k2eAgFnSc2d1	britská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
Britská	britský	k2eAgNnPc1d1	Britské
korunní	korunní	k2eAgNnPc1d1	korunní
závislá	závislý	k2eAgNnPc1d1	závislé
území	území	k1gNnPc1	území
jsou	být	k5eAaImIp3nP	být
vedeny	veden	k2eAgFnPc1d1	vedena
ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
a	a	k8xC	a
Normanské	normanský	k2eAgInPc4d1	normanský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
přímým	přímý	k2eAgNnSc7d1	přímé
vlastnictvím	vlastnictví	k1gNnSc7	vlastnictví
Britské	britský	k2eAgFnSc2d1	britská
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
tedy	tedy	k9	tedy
součástí	součást	k1gFnSc7	součást
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spojeny	spojit	k5eAaPmNgFnP	spojit
federací	federace	k1gFnSc7	federace
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xC	jako
Britské	britský	k2eAgInPc4d1	britský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
členem	člen	k1gInSc7	člen
Společenství	společenství	k1gNnSc2	společenství
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
Commonwealth	Commonwealth	k1gInSc1	Commonwealth
of	of	k?	of
Nations	Nations	k1gInSc1	Nations
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1931	[number]	k4	1931
až	až	k9	až
1947	[number]	k4	1947
Britské	britský	k2eAgNnSc1d1	Britské
společenství	společenství	k1gNnSc1	společenství
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
volné	volný	k2eAgNnSc1d1	volné
sdružení	sdružení	k1gNnSc1	sdružení
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
bývalých	bývalý	k2eAgFnPc2d1	bývalá
dominií	dominie	k1gFnPc2	dominie
a	a	k8xC	a
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
53	[number]	k4	53
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
<g/>
,	,	kIx,	,
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
,	,	kIx,	,
Singapur	Singapur	k1gInSc1	Singapur
a	a	k8xC	a
Jamajka	Jamajka	k1gFnSc1	Jamajka
<g/>
.	.	kIx.	.
15	[number]	k4	15
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
společnou	společný	k2eAgFnSc4d1	společná
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
britskou	britský	k2eAgFnSc4d1	britská
královnu	královna	k1gFnSc4	královna
Alžbětu	Alžběta	k1gFnSc4	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
světovou	světový	k2eAgFnSc7d1	světová
velmocí	velmoc	k1gFnSc7	velmoc
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
-	-	kIx~	-
obě	dva	k4xCgFnPc1	dva
světové	světový	k2eAgFnPc1d1	světová
války	válka	k1gFnPc1	válka
a	a	k8xC	a
především	především	k9	především
pak	pak	k6eAd1	pak
rozpad	rozpad	k1gInSc1	rozpad
koloniální	koloniální	k2eAgFnSc2d1	koloniální
říše	říš	k1gFnSc2	říš
-	-	kIx~	-
jeho	jeho	k3xOp3gInSc1	jeho
vliv	vliv	k1gInSc1	vliv
značně	značně	k6eAd1	značně
oslabil	oslabit	k5eAaPmAgInS	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
stálým	stálý	k2eAgMnSc7d1	stálý
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
jadernou	jaderný	k2eAgFnSc7d1	jaderná
velmocí	velmoc	k1gFnSc7	velmoc
i	i	k8xC	i
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
sedmá	sedmý	k4xOgFnSc1	sedmý
největší	veliký	k2eAgFnSc1d3	veliký
ekonomika	ekonomika	k1gFnSc1	ekonomika
světa	svět	k1gInSc2	svět
s	s	k7c7	s
druhými	druhý	k4xOgInPc7	druhý
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
výdaji	výdaj	k1gInPc7	výdaj
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
důležitou	důležitý	k2eAgFnSc7d1	důležitá
politickou	politický	k2eAgFnSc7d1	politická
<g/>
,	,	kIx,	,
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
i	i	k8xC	i
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
členů	člen	k1gMnPc2	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
se	se	k3xPyFc4	se
těsná	těsný	k2eAgFnSc1d1	těsná
většina	většina	k1gFnSc1	většina
voličů	volič	k1gMnPc2	volič
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
pro	pro	k7c4	pro
ukončení	ukončení	k1gNnSc4	ukončení
členství	členství	k1gNnSc2	členství
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
aktivován	aktivovat	k5eAaBmNgInS	aktivovat
článek	článek	k1gInSc1	článek
50	[number]	k4	50
Lisabonské	lisabonský	k2eAgFnSc2d1	Lisabonská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
spouští	spouštět	k5eAaImIp3nS	spouštět
odchod	odchod	k1gInSc4	odchod
země	zem	k1gFnSc2	zem
z	z	k7c2	z
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
opouští	opouštět	k5eAaImIp3nS	opouštět
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
trvat	trvat	k5eAaImF	trvat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
konec	konec	k1gInSc4	konec
jednání	jednání	k1gNnSc2	jednání
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
nastat	nastat	k5eAaPmF	nastat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Současnou	současný	k2eAgFnSc7d1	současná
premiérkou	premiérka	k1gFnSc7	premiérka
(	(	kIx(	(
<g/>
Prime	prim	k1gInSc5	prim
Minister	Minister	k1gMnSc1	Minister
<g/>
,	,	kIx,	,
PM	PM	kA	PM
<g/>
)	)	kIx)	)
vlády	vláda	k1gFnSc2	vláda
Jejího	její	k3xOp3gNnSc2	její
Veličenstva	veličenstvo	k1gNnSc2	veličenstvo
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
název	název	k1gInSc1	název
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vůdkyně	vůdkyně	k1gFnPc4	vůdkyně
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
Theresa	Theresa	k1gFnSc1	Theresa
Mayová	Mayová	k1gFnSc1	Mayová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
mělo	mít	k5eAaImAgNnS	mít
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgInSc2d1	oficiální
odhadu	odhad	k1gInSc2	odhad
63,7	[number]	k4	63,7
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
64,1	[number]	k4	64,1
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
Dějiny	dějiny	k1gFnPc4	dějiny
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
70	[number]	k4	70
n.	n.	k?	n.
l.	l.	k?	l.
spadala	spadat	k5eAaImAgFnS	spadat
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
pod	pod	k7c4	pod
vládu	vláda	k1gFnSc4	vláda
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
jejím	její	k3xOp3gInSc7	její
pádem	pád	k1gInSc7	pád
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
410	[number]	k4	410
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
významné	významný	k2eAgInPc1d1	významný
středověké	středověký	k2eAgInPc1d1	středověký
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Anglické	anglický	k2eAgNnSc1d1	anglické
království	království	k1gNnSc1	království
a	a	k8xC	a
Skotské	skotský	k2eAgNnSc1d1	skotské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
existovaly	existovat	k5eAaImAgInP	existovat
jako	jako	k9	jako
oddělené	oddělený	k2eAgInPc1d1	oddělený
nezávislé	závislý	k2eNgInPc1d1	nezávislý
státy	stát	k1gInPc1	stát
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vlastními	vlastní	k2eAgMnPc7d1	vlastní
panovníky	panovník	k1gMnPc7	panovník
a	a	k8xC	a
politickým	politický	k2eAgNnSc7d1	politické
uspořádáním	uspořádání	k1gNnSc7	uspořádání
již	již	k6eAd1	již
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Knížectví	knížectví	k1gNnSc1	knížectví
velšské	velšský	k2eAgNnSc1d1	Velšské
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
anglických	anglický	k2eAgMnPc2d1	anglický
králů	král	k1gMnPc2	král
roku	rok	k1gInSc2	rok
1284	[number]	k4	1284
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Walesu	Wales	k1gInSc2	Wales
<g/>
)	)	kIx)	)
a	a	k8xC	a
Skotsko	Skotsko	k1gNnSc1	Skotsko
byly	být	k5eAaImAgInP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1603	[number]	k4	1603
spojeny	spojit	k5eAaPmNgFnP	spojit
personální	personální	k2eAgFnSc7d1	personální
unií	unie	k1gFnSc7	unie
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nástupcem	nástupce	k1gMnSc7	nástupce
bezdětné	bezdětný	k2eAgFnSc2d1	bezdětná
anglické	anglický	k2eAgFnSc2d1	anglická
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
stal	stát	k5eAaPmAgMnS	stát
její	její	k3xOp3gMnSc1	její
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
pokrevný	pokrevný	k1gMnSc1	pokrevný
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
,	,	kIx,	,
skotský	skotský	k2eAgMnSc1d1	skotský
král	král	k1gMnSc1	král
Jakub	Jakub	k1gMnSc1	Jakub
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
proklamován	proklamovat	k5eAaBmNgMnS	proklamovat
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
Podle	podle	k7c2	podle
Zákonů	zákon	k1gInPc2	zákon
o	o	k7c6	o
unii	unie	k1gFnSc6	unie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1707	[number]	k4	1707
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
království	království	k1gNnSc2	království
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
do	do	k7c2	do
politické	politický	k2eAgFnSc2d1	politická
unie	unie	k1gFnSc2	unie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
jednotného	jednotný	k2eAgNnSc2d1	jednotné
Království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Irské	irský	k2eAgNnSc1d1	irské
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
ovládnuto	ovládnut	k2eAgNnSc1d1	ovládnut
Anglií	Anglie	k1gFnSc7	Anglie
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1541	[number]	k4	1541
a	a	k8xC	a
1691	[number]	k4	1691
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
bojích	boj	k1gInPc6	boj
mezi	mezi	k7c7	mezi
irskými	irský	k2eAgMnPc7d1	irský
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
novým	nový	k2eAgMnSc7d1	nový
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
protestantem	protestant	k1gMnSc7	protestant
Vilémem	Vilém	k1gMnSc7	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Oranžským	oranžský	k2eAgInSc7d1	oranžský
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1691	[number]	k4	1691
uzavřen	uzavřen	k2eAgInSc4d1	uzavřen
tzv.	tzv.	kA	tzv.
Limerický	Limerický	k2eAgInSc4d1	Limerický
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
de	de	k?	de
facto	facto	k1gNnSc1	facto
znamenal	znamenat	k5eAaImAgInS	znamenat
úplný	úplný	k2eAgInSc1d1	úplný
konec	konec	k1gInSc1	konec
irské	irský	k2eAgFnSc2d1	irská
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Zákony	zákon	k1gInPc1	zákon
o	o	k7c6	o
unii	unie	k1gFnSc6	unie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
spojily	spojit	k5eAaPmAgFnP	spojit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
státoprávně	státoprávně	k6eAd1	státoprávně
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
de	de	k?	de
iure	iure	k1gInSc1	iure
<g/>
,	,	kIx,	,
oddělená	oddělený	k2eAgNnPc1d1	oddělené
Království	království	k1gNnPc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irské	irský	k2eAgNnSc1d1	irské
království	království	k1gNnSc1	království
do	do	k7c2	do
jednotného	jednotný	k2eAgInSc2d1	jednotný
státního	státní	k2eAgInSc2d1	státní
celku	celek	k1gInSc2	celek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1	nezávislost
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
po	po	k7c6	po
irským	irský	k2eAgMnSc7d1	irský
povstáním	povstání	k1gNnSc7	povstání
vynuceném	vynucený	k2eAgNnSc6d1	vynucené
přijetí	přijetí	k1gNnSc6	přijetí
zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
Irska	Irsko	k1gNnSc2	Irsko
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
administrativní	administrativní	k2eAgFnPc4d1	administrativní
části	část	k1gFnPc4	část
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
většina	většina	k1gFnSc1	většina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
šest	šest	k4xCc1	šest
z	z	k7c2	z
devíti	devět	k4xCc2	devět
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc6	hrabství
tzv.	tzv.	kA	tzv.
Ulsterské	ulsterský	k2eAgFnPc4d1	Ulsterská
provincie	provincie	k1gFnPc4	provincie
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
tzv.	tzv.	kA	tzv.
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jižních	jižní	k2eAgFnPc2d1	jižní
částí	část	k1gFnPc2	část
ostrova	ostrov	k1gInSc2	ostrov
Irsko	Irsko	k1gNnSc4	Irsko
pak	pak	k6eAd1	pak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Irská	irský	k2eAgFnSc1d1	irská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgInS	být
změněn	změněn	k2eAgInSc1d1	změněn
název	název	k1gInSc1	název
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c4	na
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
tak	tak	k6eAd1	tak
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc4	Skotsko
a	a	k8xC	a
Wales	Wales	k1gInSc1	Wales
<g/>
)	)	kIx)	)
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pojem	pojem	k1gInSc4	pojem
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
součást	součást	k1gFnSc4	součást
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
důležitým	důležitý	k2eAgInSc7d1	důležitý
centrem	centr	k1gInSc7	centr
osvícenství	osvícenství	k1gNnSc2	osvícenství
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
filozofickým	filozofický	k2eAgInSc7d1	filozofický
a	a	k8xC	a
vědeckým	vědecký	k2eAgInSc7d1	vědecký
potenciálem	potenciál	k1gInSc7	potenciál
a	a	k8xC	a
významnou	významný	k2eAgFnSc7d1	významná
literární	literární	k2eAgFnSc7d1	literární
a	a	k8xC	a
divadelní	divadelní	k2eAgFnSc7d1	divadelní
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Bohatství	bohatství	k1gNnSc1	bohatství
Britské	britský	k2eAgFnSc2d1	britská
říše	říš	k1gFnSc2	říš
bylo	být	k5eAaImAgNnS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
bohatství	bohatství	k1gNnSc4	bohatství
dalších	další	k2eAgFnPc2d1	další
velmocí	velmoc	k1gFnPc2	velmoc
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dáno	dát	k5eAaPmNgNnS	dát
využíváním	využívání	k1gNnSc7	využívání
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
a	a	k8xC	a
industrializací	industrializace	k1gFnSc7	industrializace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nastala	nastat	k5eAaPmAgFnS	nastat
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1750	[number]	k4	1750
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
tomu	ten	k3xDgNnSc3	ten
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
největší	veliký	k2eAgNnSc4d3	veliký
obchodní	obchodní	k2eAgNnSc4d1	obchodní
loďstvo	loďstvo	k1gNnSc4	loďstvo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
nicméně	nicméně	k8xC	nicméně
schválen	schválit	k5eAaPmNgInS	schválit
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
otroctví	otroctví	k1gNnSc1	otroctví
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tento	tento	k3xDgInSc1	tento
obchod	obchod	k1gInSc1	obchod
trvale	trvale	k6eAd1	trvale
zakázal	zakázat	k5eAaPmAgInS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
revoluci	revoluce	k1gFnSc6	revoluce
a	a	k8xC	a
porážce	porážka	k1gFnSc6	porážka
Napoleona	Napoleon	k1gMnSc2	Napoleon
v	v	k7c6	v
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
se	se	k3xPyFc4	se
Britská	britský	k2eAgFnSc1d1	britská
říše	říše	k1gFnSc1	říše
stala	stát	k5eAaPmAgFnS	stát
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
velmocí	velmoc	k1gFnSc7	velmoc
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
svého	svůj	k3xOyFgInSc2	svůj
největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
se	se	k3xPyFc4	se
rozprostírala	rozprostírat	k5eAaImAgFnS	rozprostírat
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
jedné	jeden	k4xCgFnSc6	jeden
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
třetinu	třetina	k1gFnSc4	třetina
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tak	tak	k9	tak
patrně	patrně	k6eAd1	patrně
největším	veliký	k2eAgInSc7d3	veliký
"	"	kIx"	"
<g/>
státním	státní	k2eAgInSc7d1	státní
<g/>
"	"	kIx"	"
územním	územní	k2eAgInSc7d1	územní
celkem	celek	k1gInSc7	celek
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Srovnatelný	srovnatelný	k2eAgInSc4d1	srovnatelný
rozsah	rozsah	k1gInSc4	rozsah
měla	mít	k5eAaImAgFnS	mít
jen	jen	k9	jen
Mongolská	mongolský	k2eAgFnSc1d1	mongolská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
mělo	mít	k5eAaImAgNnS	mít
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
rozvoji	rozvoj	k1gInSc6	rozvoj
parlamentarismu	parlamentarismus	k1gInSc2	parlamentarismus
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
fungujícímu	fungující	k2eAgInSc3d1	fungující
systému	systém	k1gInSc3	systém
více	hodně	k6eAd2	hodně
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
postupnému	postupný	k2eAgNnSc3d1	postupné
udělování	udělování	k1gNnSc3	udělování
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
širším	široký	k2eAgFnPc3d2	širší
vrstvám	vrstva	k1gFnPc3	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
viktoriánské	viktoriánský	k2eAgFnSc2d1	viktoriánská
éry	éra	k1gFnSc2	éra
začalo	začít	k5eAaPmAgNnS	začít
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
průmyslu	průmysl	k1gInSc2	průmysl
nejvyvinutější	vyvinutý	k2eAgFnSc4d3	nejvyvinutější
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
velmoci	velmoc	k1gFnSc2	velmoc
světa	svět	k1gInSc2	svět
ztrácet	ztrácet	k5eAaImF	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
částečně	částečně	k6eAd1	částečně
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
překonaly	překonat	k5eAaPmAgInP	překonat
Spojené	spojený	k2eAgInPc1d1	spojený
království	království	k1gNnSc4	království
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
již	již	k6eAd1	již
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
však	však	k9	však
stále	stále	k6eAd1	stále
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
přední	přední	k2eAgFnSc7d1	přední
světovou	světový	k2eAgFnSc7d1	světová
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
velmocí	velmoc	k1gFnSc7	velmoc
<g/>
.	.	kIx.	.
</s>
<s>
Územně	územně	k6eAd1	územně
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
své	svůj	k3xOyFgFnPc4	svůj
největší	veliký	k2eAgFnPc4d3	veliký
velikosti	velikost	k1gFnPc4	velikost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
získáním	získání	k1gNnSc7	získání
mandátu	mandát	k1gInSc2	mandát
nad	nad	k7c7	nad
bývalými	bývalý	k2eAgFnPc7d1	bývalá
německými	německý	k2eAgFnPc7d1	německá
koloniemi	kolonie	k1gFnPc7	kolonie
a	a	k8xC	a
územími	území	k1gNnPc7	území
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
ztratila	ztratit	k5eAaPmAgFnS	ztratit
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
British	British	k1gInSc4	British
Broadcasting	Broadcasting	k1gInSc1	Broadcasting
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
BBC	BBC	kA	BBC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
vysílací	vysílací	k2eAgFnSc1d1	vysílací
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
se	se	k3xPyFc4	se
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
postavilo	postavit	k5eAaPmAgNnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Spojenců	spojenec	k1gMnPc2	spojenec
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
vítězů	vítěz	k1gMnPc2	vítěz
nad	nad	k7c7	nad
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Britští	britský	k2eAgMnPc1d1	britský
ministerští	ministerský	k2eAgMnPc1d1	ministerský
předsedové	předseda	k1gMnPc1	předseda
Winston	Winston	k1gInSc4	Winston
Churchill	Churchill	k1gMnSc1	Churchill
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Clement	Clement	k1gMnSc1	Clement
Attlee	Attlee	k1gNnSc2	Attlee
se	se	k3xPyFc4	se
výrazným	výrazný	k2eAgInSc7d1	výrazný
způsobem	způsob	k1gInSc7	způsob
podíleli	podílet	k5eAaImAgMnP	podílet
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
poválečného	poválečný	k2eAgNnSc2d1	poválečné
uspořádání	uspořádání	k1gNnSc2	uspořádání
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
válka	válka	k1gFnSc1	válka
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
finančně	finančně	k6eAd1	finančně
i	i	k9	i
fyzicky	fyzicky	k6eAd1	fyzicky
poškodila	poškodit	k5eAaPmAgFnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
obnovy	obnova	k1gFnSc2	obnova
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
nastartován	nastartován	k2eAgInSc1d1	nastartován
až	až	k9	až
díky	díky	k7c3	díky
kanadským	kanadský	k2eAgFnPc3d1	kanadská
a	a	k8xC	a
americkým	americký	k2eAgFnPc3d1	americká
půjčkám	půjčka	k1gFnPc3	půjčka
a	a	k8xC	a
především	především	k9	především
díky	díky	k7c3	díky
Marshallovu	Marshallův	k2eAgInSc3d1	Marshallův
plánu	plán	k1gInSc3	plán
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
komplexních	komplexní	k2eAgInPc2d1	komplexní
sociálních	sociální	k2eAgInPc2d1	sociální
a	a	k8xC	a
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
systémů	systém	k1gInPc2	systém
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
začali	začít	k5eAaPmAgMnP	začít
do	do	k7c2	do
země	zem	k1gFnSc2	zem
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
obnovou	obnova	k1gFnSc7	obnova
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
rozpadem	rozpad	k1gInSc7	rozpad
britské	britský	k2eAgFnSc2d1	britská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
říše	říš	k1gFnSc2	říš
přicházet	přicházet	k5eAaImF	přicházet
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Omezené	omezený	k2eAgFnPc1d1	omezená
možnosti	možnost	k1gFnPc1	možnost
vlivu	vliv	k1gInSc2	vliv
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Suezské	suezský	k2eAgFnSc2d1	Suezská
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
má	mít	k5eAaImIp3nS	mít
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
díky	díky	k7c3	díky
rozšíření	rozšíření	k1gNnSc3	rozšíření
angličtiny	angličtina	k1gFnSc2	angličtina
jako	jako	k8xS	jako
světového	světový	k2eAgInSc2d1	světový
jazyka	jazyk	k1gInSc2	jazyk
nadále	nadále	k6eAd1	nadále
značný	značný	k2eAgInSc1d1	značný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
kulturní	kulturní	k2eAgInSc4d1	kulturní
vývoj	vývoj	k1gInSc4	vývoj
daleko	daleko	k6eAd1	daleko
mimo	mimo	k7c4	mimo
své	svůj	k3xOyFgFnPc4	svůj
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
stagnace	stagnace	k1gFnSc2	stagnace
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
propadu	propad	k1gInSc2	propad
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
premiérského	premiérský	k2eAgNnSc2d1	premiérské
křesla	křeslo	k1gNnSc2	křeslo
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
politička	politička	k1gFnSc1	politička
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
reformy	reforma	k1gFnSc2	reforma
byly	být	k5eAaImAgFnP	být
jejími	její	k3xOp3gNnPc7	její
příznivci	příznivec	k1gMnPc7	příznivec
slaveny	slavit	k5eAaImNgFnP	slavit
jako	jako	k8xC	jako
příčina	příčina	k1gFnSc1	příčina
obnovy	obnova	k1gFnSc2	obnova
britské	britský	k2eAgFnSc2d1	britská
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnPc1	její
odpůrci	odpůrce	k1gMnPc1	odpůrce
však	však	k9	však
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
zostření	zostření	k1gNnSc3	zostření
sociálních	sociální	k2eAgMnPc2d1	sociální
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vlády	vláda	k1gFnPc4	vláda
labourističtí	labouristický	k2eAgMnPc1d1	labouristický
politici	politik	k1gMnPc1	politik
Tony	Tony	k1gMnSc1	Tony
Blair	Blair	k1gMnSc1	Blair
a	a	k8xC	a
Gordon	Gordon	k1gMnSc1	Gordon
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
již	již	k6eAd1	již
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
tzv.	tzv.	kA	tzv.
thatcherismu	thatcherismus	k1gInSc6	thatcherismus
nepokračovali	pokračovat	k5eNaImAgMnP	pokračovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ani	ani	k8xC	ani
premiér	premiér	k1gMnSc1	premiér
David	David	k1gMnSc1	David
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Browna	Brown	k1gMnSc4	Brown
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
Konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
thatcherismu	thatcherismus	k1gInSc3	thatcherismus
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Geografie	geografie	k1gFnSc2	geografie
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
Podnebí	podnebí	k1gNnSc2	podnebí
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
většinu	většina	k1gFnSc4	většina
rozlohy	rozloha	k1gFnSc2	rozloha
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
skupiny	skupina	k1gFnPc4	skupina
ostrovů	ostrov	k1gInPc2	ostrov
ležících	ležící	k2eAgInPc2d1	ležící
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dvou	dva	k4xCgInPc2	dva
hlavních	hlavní	k2eAgInPc2d1	hlavní
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
řada	řada	k1gFnSc1	řada
ostrovů	ostrov	k1gInPc2	ostrov
menších	malý	k2eAgInPc2d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mírné	mírný	k2eAgNnSc1d1	mírné
podnebí	podnebí	k1gNnSc1	podnebí
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
severní	severní	k2eAgFnSc7d1	severní
větví	větev	k1gFnSc7	větev
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
Severoatlantický	severoatlantický	k2eAgInSc1d1	severoatlantický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
vysoké	vysoký	k2eAgInPc1d1	vysoký
úhrny	úhrn	k1gInPc1	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
díky	díky	k7c3	díky
větrům	vítr	k1gInPc3	vítr
vanoucím	vanoucí	k2eAgInPc3d1	vanoucí
od	od	k7c2	od
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
však	však	k9	však
jihovýchodní	jihovýchodní	k2eAgNnSc4d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc4	pobřeží
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
výrazně	výrazně	k6eAd1	výrazně
sušší	suchý	k2eAgFnSc1d2	sušší
<g/>
.	.	kIx.	.
</s>
<s>
Skotská	skotský	k2eAgFnSc1d1	skotská
vysočina	vysočina	k1gFnSc1	vysočina
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
divokou	divoký	k2eAgFnSc7d1	divoká
a	a	k8xC	a
velkolepou	velkolepý	k2eAgFnSc7d1	velkolepá
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
hory	hora	k1gFnPc1	hora
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
1344	[number]	k4	1344
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
protáhlá	protáhlý	k2eAgNnPc1d1	protáhlé
jezera	jezero	k1gNnPc1	jezero
zvaná	zvaný	k2eAgFnSc1d1	zvaná
loch	loch	k1gInSc1	loch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Skotska	Skotsko	k1gNnSc2	Skotsko
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
dva	dva	k4xCgInPc4	dva
řetězce	řetězec	k1gInPc4	řetězec
ostrovů	ostrov	k1gInPc2	ostrov
zvaných	zvaný	k2eAgInPc2d1	zvaný
Vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
a	a	k8xC	a
Vnější	vnější	k2eAgFnPc1d1	vnější
Hebridy	Hebridy	k1gFnPc1	Hebridy
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
se	se	k3xPyFc4	se
země	zem	k1gFnSc2	zem
svažuje	svažovat	k5eAaImIp3nS	svažovat
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
mohutné	mohutný	k2eAgFnSc2d1	mohutná
řeky	řeka	k1gFnSc2	řeka
Clyde	Clyd	k1gMnSc5	Clyd
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Středoskotská	Středoskotský	k2eAgFnSc1d1	Středoskotský
nížina	nížina	k1gFnSc1	nížina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
nízkými	nízký	k2eAgFnPc7d1	nízká
zvlněnými	zvlněný	k2eAgFnPc7d1	zvlněná
pahorkatinami	pahorkatina	k1gFnPc7	pahorkatina
s	s	k7c7	s
kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
vrchoviny	vrchovina	k1gFnPc1	vrchovina
a	a	k8xC	a
mokřiny	mokřina	k1gFnPc1	mokřina
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
Severní	severní	k2eAgNnSc4d1	severní
Irsko	Irsko	k1gNnSc4	Irsko
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dělí	dělit	k5eAaImIp3nS	dělit
od	od	k7c2	od
Skotska	Skotsko	k1gNnSc2	Skotsko
Irské	irský	k2eAgNnSc1d1	irské
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
hluboké	hluboký	k2eAgFnPc4d1	hluboká
zátoky	zátoka	k1gFnPc4	zátoka
a	a	k8xC	a
Lough	Lough	k1gMnSc1	Lough
Neagh	Neagh	k1gMnSc1	Neagh
–	–	k?	–
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
Britských	britský	k2eAgInPc2d1	britský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
Anglie	Anglie	k1gFnSc1	Anglie
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
vápencovými	vápencový	k2eAgInPc7d1	vápencový
a	a	k8xC	a
žulovými	žulový	k2eAgInPc7d1	žulový
hřbety	hřbet	k1gInPc7	hřbet
horského	horský	k2eAgNnSc2d1	horské
pásma	pásmo	k1gNnSc2	pásmo
Pennin	Pennina	k1gFnPc2	Pennina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
jsou	být	k5eAaImIp3nP	být
hory	hora	k1gFnPc1	hora
a	a	k8xC	a
třpytivá	třpytivý	k2eAgNnPc1d1	třpytivé
jezera	jezero	k1gNnPc1	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Wales	Wales	k1gInSc1	Wales
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc4	zem
zelených	zelený	k2eAgNnPc2d1	zelené
údolí	údolí	k1gNnPc2	údolí
<g/>
,	,	kIx,	,
klikatících	klikatící	k2eAgFnPc2d1	klikatící
se	se	k3xPyFc4	se
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
travnatých	travnatý	k2eAgFnPc2d1	travnatá
planin	planina	k1gFnPc2	planina
<g/>
,	,	kIx,	,
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
usedlostí	usedlost	k1gFnPc2	usedlost
v	v	k7c6	v
kopcovitém	kopcovitý	k2eAgInSc6d1	kopcovitý
terénu	terén	k1gInSc6	terén
a	a	k8xC	a
holých	holý	k2eAgInPc2d1	holý
hor.	hor.	k?	hor.
Většinu	většina	k1gFnSc4	většina
země	zem	k1gFnSc2	zem
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
Kambrické	kambrický	k2eAgNnSc4d1	kambrický
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
poseté	posetý	k2eAgInPc4d1	posetý
malými	malý	k2eAgFnPc7d1	malá
jezery	jezero	k1gNnPc7	jezero
a	a	k8xC	a
vodopády	vodopád	k1gInPc7	vodopád
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
střední	střední	k2eAgFnSc2d1	střední
Anglie	Anglie	k1gFnSc2	Anglie
tvoří	tvořit	k5eAaImIp3nS	tvořit
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
rovina	rovina	k1gFnSc1	rovina
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hranici	hranice	k1gFnSc4	hranice
se	s	k7c7	s
Severním	severní	k2eAgNnSc7d1	severní
mořem	moře	k1gNnSc7	moře
na	na	k7c6	na
východě	východ	k1gInSc6	východ
představuje	představovat	k5eAaImIp3nS	představovat
plochá	plochý	k2eAgFnSc1d1	plochá
nížina	nížina	k1gFnSc1	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hustě	hustě	k6eAd1	hustě
zalidněném	zalidněný	k2eAgInSc6d1	zalidněný
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
úrodnými	úrodný	k2eAgFnPc7d1	úrodná
oblastmi	oblast	k1gFnPc7	oblast
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
tyčí	tyčit	k5eAaImIp3nP	tyčit
nízké	nízký	k2eAgFnPc1d1	nízká
pahorkatiny	pahorkatina	k1gFnPc1	pahorkatina
z	z	k7c2	z
křídy	křída	k1gFnSc2	křída
zvané	zvaný	k2eAgFnSc2d1	zvaná
Downs	Downs	k1gInSc4	Downs
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
plochy	plocha	k1gFnPc4	plocha
mokřin	mokřina	k1gFnPc2	mokřina
pokryté	pokrytý	k2eAgFnSc2d1	pokrytá
vřesovišti	vřesoviště	k1gNnSc3	vřesoviště
<g/>
.	.	kIx.	.
</s>
<s>
Vlny	vlna	k1gFnPc1	vlna
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
bičují	bičovat	k5eAaImIp3nP	bičovat
skaliska	skalisko	k1gNnPc4	skalisko
rozeklaného	rozeklaný	k2eAgInSc2d1	rozeklaný
jihozápadního	jihozápadní	k2eAgInSc2d1	jihozápadní
poloostrova	poloostrov	k1gInSc2	poloostrov
Cornwallu	Cornwall	k1gInSc2	Cornwall
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
celé	celý	k2eAgNnSc1d1	celé
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
lemují	lemovat	k5eAaImIp3nP	lemovat
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
oblázkové	oblázkový	k2eAgFnPc4d1	oblázková
pláže	pláž	k1gFnPc4	pláž
či	či	k8xC	či
vysoké	vysoký	k2eAgInPc4d1	vysoký
svislé	svislý	k2eAgInPc4d1	svislý
útesy	útes	k1gInPc4	útes
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vápencové	vápencový	k2eAgInPc1d1	vápencový
útesy	útes	k1gInPc4	útes
u	u	k7c2	u
přístavu	přístav	k1gInSc2	přístav
Dover	Dover	k1gInSc1	Dover
nebo	nebo	k8xC	nebo
Seven	Seven	k2eAgInSc1d1	Seven
Sisters	Sisters	k1gInSc1	Sisters
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Správní	správní	k2eAgNnPc4d1	správní
členění	členění	k1gNnPc4	členění
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
a	a	k8xC	a
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
tvoří	tvořit	k5eAaImIp3nS	tvořit
čtyři	čtyři	k4xCgFnPc4	čtyři
země	zem	k1gFnPc4	zem
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
constituent	constituent	k1gInSc1	constituent
country	country	k2eAgInSc1d1	country
<g/>
)	)	kIx)	)
–	–	k?	–
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
a	a	k8xC	a
Wales	Wales	k1gInSc1	Wales
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
spravována	spravovat	k5eAaImNgFnS	spravovat
přímo	přímo	k6eAd1	přímo
centrálními	centrální	k2eAgFnPc7d1	centrální
institucemi	instituce	k1gFnPc7	instituce
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ostatní	ostatní	k2eAgInPc1d1	ostatní
tři	tři	k4xCgFnPc1	tři
země	zem	k1gFnPc1	zem
těší	těšit	k5eAaImIp3nP	těšit
různé	různý	k2eAgFnSc3d1	různá
míře	míra	k1gFnSc3	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
tzv.	tzv.	kA	tzv.
devoluce	devoluce	k1gFnPc4	devoluce
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
čtyřstupňový	čtyřstupňový	k2eAgInSc4d1	čtyřstupňový
systém	systém	k1gInSc4	systém
správního	správní	k2eAgNnSc2d1	správní
členění	členění	k1gNnSc2	členění
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
ostatní	ostatní	k2eAgFnPc1d1	ostatní
tři	tři	k4xCgFnPc1	tři
země	zem	k1gFnPc1	zem
podstatně	podstatně	k6eAd1	podstatně
jednodušší	jednoduchý	k2eAgInSc4d2	jednodušší
dvoustupňový	dvoustupňový	k2eAgInSc4d1	dvoustupňový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Britské	britský	k2eAgFnSc2d1	britská
korunní	korunní	k2eAgMnSc1d1	korunní
závislé	závislý	k2eAgNnSc4d1	závislé
území	území	k1gNnSc4	území
a	a	k8xC	a
Zámořské	zámořský	k2eAgNnSc4d1	zámořské
území	území	k1gNnSc4	území
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
koruna	koruna	k1gFnSc1	koruna
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
suverenitu	suverenita	k1gFnSc4	suverenita
nad	nad	k7c7	nad
tzv.	tzv.	kA	tzv.
korunními	korunní	k2eAgFnPc7d1	korunní
dependencemi	dependence	k1gFnPc7	dependence
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
Ostrov	ostrov	k1gInSc4	ostrov
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
Guernsey	Guernsea	k1gFnSc2	Guernsea
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
země	zem	k1gFnPc4	zem
patří	patřit	k5eAaImIp3nS	patřit
britskému	britský	k2eAgMnSc3d1	britský
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
ani	ani	k8xC	ani
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
právo	právo	k1gNnSc4	právo
vydávat	vydávat	k5eAaPmF	vydávat
zákony	zákon	k1gInPc4	zákon
pro	pro	k7c4	pro
dependence	dependence	k1gFnPc4	dependence
a	a	k8xC	a
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
spravuje	spravovat	k5eAaImIp3nS	spravovat
jejich	jejich	k3xOp3gInPc4	jejich
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
čtrnáct	čtrnáct	k4xCc4	čtrnáct
zámořských	zámořský	k2eAgNnPc2d1	zámořské
teritorií	teritorium	k1gNnPc2	teritorium
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
poslední	poslední	k2eAgInPc1d1	poslední
zbytky	zbytek	k1gInPc1	zbytek
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
zámořská	zámořský	k2eAgNnPc1d1	zámořské
území	území	k1gNnPc1	území
se	se	k3xPyFc4	se
nepovažují	považovat	k5eNaImIp3nP	považovat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
mají	mít	k5eAaImIp3nP	mít
jejich	jejich	k3xOp3gMnPc1	jejich
obyvatelé	obyvatel	k1gMnPc1	obyvatel
britské	britský	k2eAgFnSc2d1	britská
občanství	občanství	k1gNnSc3	občanství
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
bydlet	bydlet	k5eAaImF	bydlet
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
(	(	kIx(	(
<g/>
platí	platit	k5eAaImIp3nS	platit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Britskými	britský	k2eAgFnPc7d1	britská
zámořskými	zámořský	k2eAgFnPc7d1	zámořská
teritorii	teritorium	k1gNnPc7	teritorium
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Anguilla	Anguillo	k1gNnPc4	Anguillo
<g/>
,	,	kIx,	,
Bermudy	Bermudy	k1gFnPc4	Bermudy
<g/>
,	,	kIx,	,
Britské	britský	k2eAgNnSc4d1	Britské
antarktické	antarktický	k2eAgNnSc4d1	antarktické
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
Britské	britský	k2eAgNnSc4d1	Britské
indickooceánské	indickooceánský	k2eAgNnSc4d1	indickooceánský
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc1d1	britský
Panenské	panenský	k2eAgInPc1d1	panenský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Falklandy	Falklanda	k1gFnPc1	Falklanda
<g/>
,	,	kIx,	,
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Georgie	Georgie	k1gFnSc1	Georgie
a	a	k8xC	a
Jižní	jižní	k2eAgInPc1d1	jižní
Sandwichovy	Sandwichův	k2eAgInPc1d1	Sandwichův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Kajmanské	Kajmanský	k2eAgInPc1d1	Kajmanský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
kyperské	kyperský	k2eAgFnPc1d1	Kyperská
vojenské	vojenský	k2eAgFnPc4d1	vojenská
základny	základna	k1gFnPc4	základna
Akrotiri	Akrotiri	k1gNnSc2	Akrotiri
a	a	k8xC	a
Dhekelia	Dhekelium	k1gNnSc2	Dhekelium
<g/>
,	,	kIx,	,
Montserrat	Montserrat	k1gInSc1	Montserrat
<g/>
,	,	kIx,	,
Pitcairnovy	Pitcairnův	k2eAgInPc1d1	Pitcairnův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
Ascension	Ascension	k1gInSc1	Ascension
a	a	k8xC	a
Tristan	Tristan	k1gInSc1	Tristan
da	da	k?	da
Cunha	Cunha	k1gFnSc1	Cunha
<g/>
,	,	kIx,	,
Turks	Turks	k1gInSc1	Turks
a	a	k8xC	a
Caicos	Caicos	k1gInSc1	Caicos
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
moc	moc	k1gFnSc4	moc
jménem	jméno	k1gNnSc7	jméno
panovníka	panovník	k1gMnSc2	panovník
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
jménem	jméno	k1gNnSc7	jméno
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
drží	držet	k5eAaImIp3nP	držet
veškerou	veškerý	k3xTgFnSc4	veškerý
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Zákona	zákon	k1gInSc2	zákon
o	o	k7c4	o
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1701	[number]	k4	1701
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c4	na
britský	britský	k2eAgInSc4d1	britský
trůn	trůn	k1gInSc4	trůn
nastoupit	nastoupit	k5eAaPmF	nastoupit
pouze	pouze	k6eAd1	pouze
potomci	potomek	k1gMnPc1	potomek
Žofie	Žofie	k1gFnSc2	Žofie
Falcké	falcký	k2eAgFnSc2d1	Falcká
(	(	kIx(	(
<g/>
Hanoverské	Hanoverský	k2eAgFnSc2d1	Hanoverská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
anglikánského	anglikánský	k2eAgMnSc2d1	anglikánský
nebo	nebo	k8xC	nebo
protestantského	protestantský	k2eAgNnSc2d1	protestantské
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
římskokatolického	římskokatolický	k2eAgNnSc2d1	římskokatolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Naprosto	naprosto	k6eAd1	naprosto
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
panovníka	panovník	k1gMnSc2	panovník
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
tradičně	tradičně	k6eAd1	tradičně
"	"	kIx"	"
<g/>
Vláda	vláda	k1gFnSc1	vláda
jeho	jeho	k3xOp3gFnSc1	jeho
<g/>
/	/	kIx~	/
<g/>
jejího	její	k3xOp3gNnSc2	její
Veličenstva	veličenstvo	k1gNnSc2	veličenstvo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
His	his	k1gNnSc1	his
<g/>
/	/	kIx~	/
<g/>
Her	hra	k1gFnPc2	hra
Majesty	Majest	k1gInPc4	Majest
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Government	Government	k1gInSc1	Government
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
Prime	prim	k1gInSc5	prim
Minister	Ministrum	k1gNnPc2	Ministrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Radě	rada	k1gFnSc3	rada
nejdůležitějších	důležitý	k2eAgMnPc2d3	nejdůležitější
ministrů	ministr	k1gMnPc2	ministr
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
kabinet	kabinet	k1gInSc1	kabinet
(	(	kIx(	(
<g/>
Cabinet	Cabinet	k1gInSc1	Cabinet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
parlamentu	parlament	k1gInSc3	parlament
a	a	k8xC	a
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
ji	on	k3xPp3gFnSc4	on
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
vůdcem	vůdce	k1gMnSc7	vůdce
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
komoře	komora	k1gFnSc6	komora
parlamentu	parlament	k1gInSc2	parlament
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
tuto	tento	k3xDgFnSc4	tento
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
koalice	koalice	k1gFnSc2	koalice
více	hodně	k6eAd2	hodně
stran	strana	k1gFnPc2	strana
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
členy	člen	k1gMnPc4	člen
parlamentu	parlament	k1gInSc2	parlament
(	(	kIx(	(
<g/>
Member	Member	k1gInSc1	Member
of	of	k?	of
Parliament	Parliament	k1gInSc1	Parliament
<g/>
,	,	kIx,	,
MP	MP	kA	MP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
britský	britský	k2eAgInSc1d1	britský
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
víceméně	víceméně	k9	víceméně
převzat	převzít	k5eAaPmNgInS	převzít
do	do	k7c2	do
ústavního	ústavní	k2eAgNnSc2d1	ústavní
zřízení	zřízení	k1gNnSc2	zřízení
mnoha	mnoho	k4c2	mnoho
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
členů	člen	k1gInPc2	člen
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
také	také	k9	také
převzalo	převzít	k5eAaPmAgNnS	převzít
britský	britský	k2eAgInSc4d1	britský
právní	právní	k2eAgInSc4d1	právní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc4d1	známý
jako	jako	k8xC	jako
Angloamerické	angloamerický	k2eAgNnSc4d1	angloamerické
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
USA	USA	kA	USA
není	být	k5eNaImIp3nS	být
však	však	k9	však
právo	právo	k1gNnSc4	právo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
psané	psaný	k2eAgFnSc6d1	psaná
ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k8xS	tak
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
s	s	k7c7	s
demokratickým	demokratický	k2eAgNnSc7d1	demokratické
státním	státní	k2eAgNnSc7d1	státní
zřízením	zřízení	k1gNnSc7	zřízení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
a	a	k8xC	a
Izrael	Izrael	k1gInSc4	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
má	mít	k5eAaImIp3nS	mít
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
formální	formální	k2eAgFnPc4d1	formální
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
role	role	k1gFnSc1	role
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
ne	ne	k9	ne
výhradně	výhradně	k6eAd1	výhradně
<g/>
)	)	kIx)	)
ceremoniální	ceremoniální	k2eAgFnSc1d1	ceremoniální
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
integrálním	integrální	k2eAgInSc7d1	integrální
elementem	element	k1gInSc7	element
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
mu	on	k3xPp3gMnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
ve	v	k7c6	v
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
aktu	akt	k1gInSc6	akt
pravomoc	pravomoc	k1gFnSc4	pravomoc
scházet	scházet	k5eAaImF	scházet
se	se	k3xPyFc4	se
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
parlamentní	parlamentní	k2eAgInSc1d1	parlamentní
zákon	zákon	k1gInSc1	zákon
nevejde	vejít	k5eNaPmIp3nS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
panovníkem	panovník	k1gMnSc7	panovník
podepsán	podepsán	k2eAgMnSc1d1	podepsán
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
zato	zato	k6eAd1	zato
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1708	[number]	k4	1708
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
královna	královna	k1gFnSc1	královna
Anna	Anna	k1gFnSc1	Anna
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
nějaký	nějaký	k3yIgInSc1	nějaký
zákon	zákon	k1gInSc1	zákon
přijatý	přijatý	k2eAgInSc1d1	přijatý
parlamentem	parlament	k1gInSc7	parlament
panovníkem	panovník	k1gMnSc7	panovník
odmítnut	odmítnut	k2eAgMnSc1d1	odmítnut
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
listu	list	k1gInSc2	list
The	The	k1gFnSc2	The
Guardian	Guardiana	k1gFnPc2	Guardiana
z	z	k7c2	z
důvěrných	důvěrný	k2eAgInPc2d1	důvěrný
materiálů	materiál	k1gInPc2	materiál
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
však	však	k8xC	však
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
má	mít	k5eAaImIp3nS	mít
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
současná	současný	k2eAgFnSc1d1	současná
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
<g/>
/	/	kIx~	/
<g/>
její	její	k3xOp3gMnSc1	její
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
princ	princ	k1gMnSc1	princ
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
vůči	vůči	k7c3	vůči
zákonům	zákon	k1gInPc3	zákon
projednávaným	projednávaný	k2eAgInPc3d1	projednávaný
v	v	k7c6	v
parlamentě	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
zákonů	zákon	k1gInPc2	zákon
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gMnPc3	on
předkládány	předkládán	k2eAgInPc4d1	předkládán
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
definitivním	definitivní	k2eAgNnSc7d1	definitivní
přijetím	přijetí	k1gNnSc7	přijetí
a	a	k8xC	a
list	list	k1gInSc1	list
The	The	k1gFnPc2	The
Guardian	Guardiana	k1gFnPc2	Guardiana
přinesl	přinést	k5eAaPmAgInS	přinést
údajný	údajný	k2eAgInSc1d1	údajný
seznam	seznam	k1gInSc1	seznam
38	[number]	k4	38
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
princ	princ	k1gMnSc1	princ
Charles	Charles	k1gMnSc1	Charles
prosadili	prosadit	k5eAaPmAgMnP	prosadit
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
přenesení	přenesení	k1gNnSc4	přenesení
pravomoci	pravomoc	k1gFnSc2	pravomoc
panovnice	panovnice	k1gFnSc2	panovnice
povolit	povolit	k5eAaPmF	povolit
nebo	nebo	k8xC	nebo
zamítnout	zamítnout	k5eAaPmF	zamítnout
britské	britský	k2eAgFnSc2d1	britská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
operace	operace	k1gFnSc2	operace
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Military	Militara	k1gFnSc2	Militara
Actions	Actions	k1gInSc1	Actions
Against	Against	k1gMnSc1	Against
Iraq	Iraq	k1gMnSc1	Iraq
Bill	Bill	k1gMnSc1	Bill
<g/>
)	)	kIx)	)
na	na	k7c4	na
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
vetovala	vetovat	k5eAaBmAgFnS	vetovat
<g/>
..	..	k?	..
Ačkoliv	ačkoliv	k8xS	ačkoliv
bylo	být	k5eAaImAgNnS	být
zrušení	zrušení	k1gNnSc4	zrušení
monarchie	monarchie	k1gFnSc2	monarchie
několikrát	několikrát	k6eAd1	několikrát
navrhováno	navrhovat	k5eAaImNgNnS	navrhovat
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
popularita	popularita	k1gFnSc1	popularita
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
15	[number]	k4	15
a	a	k8xC	a
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
korunována	korunován	k2eAgFnSc1d1	korunována
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
mocí	moc	k1gFnSc7	moc
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
je	být	k5eAaImIp3nS	být
Parlament	parlament	k1gInSc1	parlament
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
doktríny	doktrína	k1gFnSc2	doktrína
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
suverenity	suverenita	k1gFnSc2	suverenita
je	být	k5eAaImIp3nS	být
parlament	parlament	k1gInSc1	parlament
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
autoritou	autorita	k1gFnSc7	autorita
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgMnSc1d1	dvoukomorový
<g/>
,	,	kIx,	,
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
volené	volený	k2eAgFnSc2d1	volená
Dolní	dolní	k2eAgFnSc2d1	dolní
sněmovny	sněmovna	k1gFnSc2	sněmovna
(	(	kIx(	(
<g/>
House	house	k1gNnSc1	house
of	of	k?	of
Commons	Commonsa	k1gFnPc2	Commonsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
nevolené	volený	k2eNgFnPc1d1	nevolená
Sněmovny	sněmovna	k1gFnPc1	sněmovna
lordů	lord	k1gMnPc2	lord
(	(	kIx(	(
<g/>
House	house	k1gNnSc1	house
of	of	k?	of
Lords	Lordsa	k1gFnPc2	Lordsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
má	mít	k5eAaImIp3nS	mít
646	[number]	k4	646
přímo	přímo	k6eAd1	přímo
volených	volený	k2eAgMnPc2d1	volený
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
lordů	lord	k1gMnPc2	lord
pak	pak	k6eAd1	pak
přibližně	přibližně	k6eAd1	přibližně
700	[number]	k4	700
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
stanoven	stanovit	k5eAaPmNgInS	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
komora	komora	k1gFnSc1	komora
se	se	k3xPyFc4	se
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
dědičných	dědičný	k2eAgMnPc2d1	dědičný
i	i	k8xC	i
nedědičných	dědičný	k2eNgMnPc2d1	nedědičný
peerů	peer	k1gMnPc2	peer
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
šlechticů	šlechtic	k1gMnPc2	šlechtic
nebo	nebo	k8xC	nebo
šlechtičen	šlechtična	k1gFnPc2	šlechtična
nejméně	málo	k6eAd3	málo
s	s	k7c7	s
hodností	hodnost	k1gFnSc7	hodnost
barona	baron	k1gMnSc2	baron
nebo	nebo	k8xC	nebo
baronky	baronka	k1gFnSc2	baronka
<g/>
)	)	kIx)	)
a	a	k8xC	a
biskupů	biskup	k1gMnPc2	biskup
anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Bývalou	bývalý	k2eAgFnSc4d1	bývalá
dědičnost	dědičnost	k1gFnSc4	dědičnost
křesel	křeslo	k1gNnPc2	křeslo
zrušil	zrušit	k5eAaPmAgMnS	zrušit
Zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
sněmovně	sněmovna	k1gFnSc6	sněmovna
lordů	lord	k1gMnPc2	lord
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
povolil	povolit	k5eAaPmAgInS	povolit
dědičnost	dědičnost	k1gFnSc4	dědičnost
místa	místo	k1gNnSc2	místo
jen	jen	k9	jen
92	[number]	k4	92
peerům	peer	k1gMnPc3	peer
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
existence	existence	k1gFnSc1	existence
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
privilegia	privilegium	k1gNnPc4	privilegium
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
role	role	k1gFnPc1	role
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
životě	život	k1gInSc6	život
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
vyhrazeny	vyhradit	k5eAaPmNgInP	vyhradit
společenské	společenský	k2eAgFnSc3d1	společenská
elitě	elita	k1gFnSc3	elita
<g/>
.	.	kIx.	.
</s>
<s>
Transparency	Transparency	k1gInPc1	Transparency
International	International	k1gFnSc1	International
také	také	k9	také
poukázala	poukázat	k5eAaPmAgFnS	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
Brexit	Brexit	k1gInSc1	Brexit
nebyl	být	k5eNaImAgInS	být
příliš	příliš	k6eAd1	příliš
demokratický	demokratický	k2eAgInSc1d1	demokratický
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
stálým	stálý	k2eAgInSc7d1	stálý
členem	člen	k1gInSc7	člen
rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnSc2	skupina
ministrů	ministr	k1gMnPc2	ministr
financí	finance	k1gFnPc2	finance
zemí	zem	k1gFnPc2	zem
G7	G7	k1gFnSc2	G7
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
G	G	kA	G
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
OECD	OECD	kA	OECD
<g/>
,	,	kIx,	,
WTO	WTO	kA	WTO
<g/>
,	,	kIx,	,
rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
OSCE	OSCE	kA	OSCE
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
členský	členský	k2eAgInSc1d1	členský
stát	stát	k1gInSc1	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
však	však	k9	však
Britové	Brit	k1gMnPc1	Brit
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
odhlasovali	odhlasovat	k5eAaPmAgMnP	odhlasovat
vystoupení	vystoupení	k1gNnPc4	vystoupení
z	z	k7c2	z
EU	EU	kA	EU
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
brexit	brexit	k1gInSc1	brexit
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
podle	podle	k7c2	podle
platných	platný	k2eAgNnPc2d1	platné
pravidel	pravidlo	k1gNnPc2	pravidlo
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dojednáno	dojednán	k2eAgNnSc1d1	dojednáno
a	a	k8xC	a
uskutečněno	uskutečněn	k2eAgNnSc1d1	uskutečněno
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
od	od	k7c2	od
oficiální	oficiální	k2eAgFnSc2d1	oficiální
žádosti	žádost	k1gFnSc2	žádost
o	o	k7c4	o
vystoupení	vystoupení	k1gNnSc4	vystoupení
z	z	k7c2	z
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
udržuje	udržovat	k5eAaImIp3nS	udržovat
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
vztahy	vztah	k1gInPc4	vztah
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Special	Special	k1gMnSc1	Special
Relationship	Relationship	k1gMnSc1	Relationship
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
s	s	k7c7	s
USA	USA	kA	USA
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
–	–	k?	–
<g/>
"	"	kIx"	"
<g/>
srdečná	srdečný	k2eAgFnSc1d1	srdečná
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
"	"	kIx"	"
<g/>
–	–	k?	–
<g/>
a	a	k8xC	a
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
technologiích	technologie	k1gFnPc6	technologie
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
také	také	k9	také
úzce	úzko	k6eAd1	úzko
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
Irskem	Irsko	k1gNnSc7	Irsko
<g/>
;	;	kIx,	;
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
sdílejí	sdílet	k5eAaImIp3nP	sdílet
společný	společný	k2eAgInSc4d1	společný
prostor	prostor	k1gInSc4	prostor
cestování	cestování	k1gNnSc2	cestování
(	(	kIx(	(
<g/>
Common	Common	k1gInSc1	Common
Travel	Travel	k1gInSc1	Travel
Area	area	k1gFnSc1	area
<g/>
)	)	kIx)	)
a	a	k8xC	a
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Britsko-irské	britskorský	k2eAgFnSc2d1	britsko-irská
mezivládní	mezivládní	k2eAgFnSc2d1	mezivládní
konference	konference	k1gFnSc2	konference
(	(	kIx(	(
<g/>
BIIGC	BIIGC	kA	BIIGC
<g/>
)	)	kIx)	)
a	a	k8xC	a
britsko-irské	britskorský	k2eAgFnSc2d1	britsko-irská
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
BIC	BIC	kA	BIC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
přítomnost	přítomnost	k1gFnSc1	přítomnost
a	a	k8xC	a
britský	britský	k2eAgInSc1d1	britský
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
zesílen	zesílit	k5eAaPmNgInS	zesílit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
obchodních	obchodní	k2eAgInPc2d1	obchodní
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
a	a	k8xC	a
vojenských	vojenský	k2eAgInPc2d1	vojenský
závazků	závazek	k1gInPc2	závazek
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgFnPc1d1	britská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
Jejího	její	k3xOp3gNnSc2	její
Veličenstva	veličenstvo	k1gNnSc2	veličenstvo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
profesionálních	profesionální	k2eAgFnPc2d1	profesionální
odnoží	odnož	k1gFnPc2	odnož
<g/>
:	:	kIx,	:
Royal	Royal	k1gInSc1	Royal
Navy	Navy	k?	Navy
a	a	k8xC	a
Royal	Royal	k1gMnSc1	Royal
Marines	Marines	k1gMnSc1	Marines
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgFnSc4d1	tvořící
námořní	námořní	k2eAgFnSc4d1	námořní
službu	služba	k1gFnSc4	služba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
královského	královský	k2eAgNnSc2d1	královské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Síly	síl	k1gInPc1	síl
jsou	být	k5eAaImIp3nP	být
organizovány	organizovat	k5eAaBmNgInP	organizovat
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
řízeny	řízen	k2eAgFnPc1d1	řízena
radou	rada	k1gFnSc7	rada
obrany	obrana	k1gFnSc2	obrana
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
předsedá	předsedat	k5eAaImIp3nS	předsedat
státní	státní	k2eAgMnSc1d1	státní
tajemník	tajemník	k1gMnSc1	tajemník
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
monarcha	monarcha	k1gMnSc1	monarcha
<g/>
,	,	kIx,	,
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
příslušníci	příslušník	k1gMnPc1	příslušník
sil	síla	k1gFnPc2	síla
skládají	skládat	k5eAaImIp3nP	skládat
přísahu	přísaha	k1gFnSc4	přísaha
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
G8	G8	k1gFnSc2	G8
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejrozvinutějším	rozvinutý	k2eAgFnPc3d3	nejrozvinutější
zemím	zem	k1gFnPc3	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
okolo	okolo	k7c2	okolo
35	[number]	k4	35
100	[number]	k4	100
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
335	[number]	k4	335
%	%	kIx~	%
průměru	průměr	k1gInSc6	průměr
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
Britové	Brit	k1gMnPc1	Brit
členy	člen	k1gMnPc4	člen
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
si	se	k3xPyFc3	se
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
britskou	britský	k2eAgFnSc4d1	britská
libru	libra	k1gFnSc4	libra
jako	jako	k8xS	jako
svoji	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
k	k	k7c3	k
EU	EU	kA	EU
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
velice	velice	k6eAd1	velice
skepticky	skepticky	k6eAd1	skepticky
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc7	první
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
od	od	k7c2	od
zemědělství	zemědělství	k1gNnSc2	zemědělství
k	k	k7c3	k
průmyslu	průmysl	k1gInSc3	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
začal	začít	k5eAaPmAgInS	začít
koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	s	k7c7	s
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
taková	takový	k3xDgNnPc4	takový
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
odvětví	odvětví	k1gNnPc4	odvětví
jako	jako	k8xC	jako
textilní	textilní	k2eAgInSc4d1	textilní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
těžké	těžký	k2eAgNnSc1d1	těžké
strojírenství	strojírenství	k1gNnSc1	strojírenství
<g/>
.	.	kIx.	.
</s>
<s>
Palivem	palivo	k1gNnSc7	palivo
pro	pro	k7c4	pro
tato	tento	k3xDgNnPc4	tento
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
odvětví	odvětví	k1gNnPc4	odvětví
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
největším	veliký	k2eAgInSc7d3	veliký
nerostným	nerostný	k2eAgInSc7d1	nerostný
zdrojem	zdroj	k1gInSc7	zdroj
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc1	století
tvořila	tvořit	k5eAaImAgNnP	tvořit
tato	tento	k3xDgNnPc1	tento
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
páteř	páteř	k1gFnSc1	páteř
celého	celý	k2eAgNnSc2d1	celé
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
významným	významný	k2eAgInSc7d1	významný
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
produkuje	produkovat	k5eAaImIp3nS	produkovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
změnily	změnit	k5eAaPmAgFnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Důležitosti	důležitost	k1gFnPc4	důležitost
nabyla	nabýt	k5eAaPmAgFnS	nabýt
novější	nový	k2eAgFnSc1d2	novější
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
odvětví	odvětví	k1gNnSc1	odvětví
<g/>
,	,	kIx,	,
např.	např.	kA	např.
elektrotechnický	elektrotechnický	k2eAgMnSc1d1	elektrotechnický
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
a	a	k8xC	a
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
dnes	dnes	k6eAd1	dnes
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
finančnictví	finančnictví	k1gNnSc2	finančnictví
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
a	a	k8xC	a
cestovnímu	cestovní	k2eAgInSc3d1	cestovní
ruchu	ruch	k1gInSc3	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
nerostným	nerostný	k2eAgNnSc7d1	nerostné
bohatstvím	bohatství	k1gNnSc7	bohatství
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
velká	velká	k1gFnSc1	velká
naleziště	naleziště	k1gNnSc2	naleziště
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
ležící	ležící	k2eAgFnSc1d1	ležící
pod	pod	k7c7	pod
dnem	den	k1gInSc7	den
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
2	[number]	k4	2
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
země	země	k1gFnSc1	země
intenzivně	intenzivně	k6eAd1	intenzivně
obhospodařována	obhospodařován	k2eAgFnSc1d1	obhospodařována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
potraviny	potravina	k1gFnPc4	potravina
jak	jak	k8xS	jak
pro	pro	k7c4	pro
domácí	domácí	k2eAgFnSc4d1	domácí
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
<g/>
.	.	kIx.	.
</s>
<s>
Rozlehlé	rozlehlý	k2eAgFnPc1d1	rozlehlá
oblasti	oblast	k1gFnPc1	oblast
úrodné	úrodný	k2eAgFnPc1d1	úrodná
půdy	půda	k1gFnSc2	půda
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Anglii	Anglie	k1gFnSc6	Anglie
produkují	produkovat	k5eAaImIp3nP	produkovat
obilí	obilí	k1gNnSc4	obilí
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zelených	zelený	k2eAgFnPc6d1	zelená
pastvinách	pastvina	k1gFnPc6	pastvina
západní	západní	k2eAgFnSc2d1	západní
Anglie	Anglie	k1gFnSc2	Anglie
se	se	k3xPyFc4	se
pase	pást	k5eAaImIp3nS	pást
mléčný	mléčný	k2eAgInSc1d1	mléčný
skot	skot	k1gInSc1	skot
a	a	k8xC	a
na	na	k7c6	na
skotské	skotský	k2eAgFnSc6d1	skotská
a	a	k8xC	a
velšské	velšský	k2eAgFnSc6d1	velšská
vrchovině	vrchovina	k1gFnSc6	vrchovina
ovce	ovce	k1gFnSc2	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
starých	starý	k2eAgInPc2d1	starý
lesů	les	k1gInPc2	les
a	a	k8xC	a
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
rozlohy	rozloha	k1gFnSc2	rozloha
osázené	osázený	k2eAgInPc4d1	osázený
jehličnany	jehličnan	k1gInPc7	jehličnan
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
produkují	produkovat	k5eAaImIp3nP	produkovat
dřevo	dřevo	k1gNnSc4	dřevo
pro	pro	k7c4	pro
stavebnictví	stavebnictví	k1gNnSc4	stavebnictví
a	a	k8xC	a
papírenství	papírenství	k1gNnSc4	papírenství
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
přístavů	přístav	k1gInPc2	přístav
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
pobřeží	pobřeží	k1gNnSc6	pobřeží
vyplouvají	vyplouvat	k5eAaImIp3nP	vyplouvat
rybářské	rybářský	k2eAgFnPc1d1	rybářská
lodě	loď	k1gFnPc1	loď
a	a	k8xC	a
rybářská	rybářský	k2eAgFnSc1d1	rybářská
flotila	flotila	k1gFnSc1	flotila
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
je	být	k5eAaImIp3nS	být
pátá	pátý	k4xOgFnSc1	pátý
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc1	zalidnění
–	–	k?	–
250	[number]	k4	250
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
km2	km2	k4	km2
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
žijí	žít	k5eAaImIp3nP	žít
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
asi	asi	k9	asi
92	[number]	k4	92
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
není	být	k5eNaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
novějších	nový	k2eAgFnPc2d2	novější
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
domovem	domov	k1gInSc7	domov
pestré	pestrý	k2eAgFnSc2d1	pestrá
směsice	směsice	k1gFnSc2	směsice
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgFnPc4	čtyři
pětiny	pětina	k1gFnPc1	pětina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
velkoměstech	velkoměsto	k1gNnPc6	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prázdnin	prázdniny	k1gFnPc2	prázdniny
a	a	k8xC	a
dovolených	dovolená	k1gFnPc2	dovolená
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnPc4d1	hlavní
silnice	silnice	k1gFnPc4	silnice
ucpány	ucpán	k2eAgFnPc4d1	ucpána
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jedou	jet	k5eAaImIp3nP	jet
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
či	či	k8xC	či
výlet	výlet	k1gInSc4	výlet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
činnosti	činnost	k1gFnPc4	činnost
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
provozované	provozovaný	k2eAgFnPc4d1	provozovaná
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
patří	patřit	k5eAaImIp3nP	patřit
zahradničení	zahradničený	k2eAgMnPc1d1	zahradničený
a	a	k8xC	a
sledování	sledování	k1gNnSc3	sledování
sportů	sport	k1gInPc2	sport
či	či	k8xC	či
aktivní	aktivní	k2eAgNnSc1d1	aktivní
sportování	sportování	k1gNnSc1	sportování
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
má	mít	k5eAaImIp3nS	mít
bohaté	bohatý	k2eAgInPc4d1	bohatý
literární	literární	k2eAgInPc4d1	literární
a	a	k8xC	a
umělecké	umělecký	k2eAgNnSc4d1	umělecké
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
jeho	jeho	k3xOp3gFnSc6	jeho
země	zem	k1gFnPc1	zem
plodily	plodit	k5eAaImAgFnP	plodit
řadu	řada	k1gFnSc4	řada
velkých	velký	k2eAgInPc2d1	velký
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
dramatiků	dramatik	k1gMnPc2	dramatik
<g/>
,	,	kIx,	,
romanopisců	romanopisec	k1gMnPc2	romanopisec
<g/>
,	,	kIx,	,
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
architektů	architekt	k1gMnPc2	architekt
a	a	k8xC	a
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
od	od	k7c2	od
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
až	až	k9	až
po	po	k7c6	po
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
indexem	index	k1gInSc7	index
soukromí	soukromí	k1gNnPc2	soukromí
1,5	[number]	k4	1,5
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejhorší	zlý	k2eAgFnSc1d3	nejhorší
hodnota	hodnota	k1gFnSc1	hodnota
z	z	k7c2	z
36	[number]	k4	36
vyhodnocovaných	vyhodnocovaný	k2eAgFnPc2d1	vyhodnocovaná
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nP	patřit
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
zásahy	zásah	k1gInPc7	zásah
do	do	k7c2	do
soukromí	soukromí	k1gNnSc2	soukromí
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
instalováno	instalován	k2eAgNnSc4d1	instalováno
přes	přes	k7c4	přes
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
kamer	kamera	k1gFnPc2	kamera
(	(	kIx(	(
<g/>
1	[number]	k4	1
na	na	k7c4	na
14	[number]	k4	14
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
7	[number]	k4	7
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc3	červenec
2005	[number]	k4	2005
stal	stát	k5eAaPmAgInS	stát
městem	město	k1gNnSc7	město
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
kamerovým	kamerový	k2eAgInSc7d1	kamerový
dohledem	dohled	k1gInSc7	dohled
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgFnSc7d3	veliký
skupinou	skupina	k1gFnSc7	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
patří	patřit	k5eAaImIp3nS	patřit
Velšané	Velšan	k1gMnPc1	Velšan
<g/>
,	,	kIx,	,
Skotové	Skot	k1gMnPc1	Skot
<g/>
,	,	kIx,	,
Irové	Ir	k1gMnPc1	Ir
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
terčem	terč	k1gInSc7	terč
masivní	masivní	k2eAgFnSc2d1	masivní
imigrace	imigrace	k1gFnSc2	imigrace
neevropského	evropský	k2eNgNnSc2d1	neevropské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
imigrantů	imigrant	k1gMnPc2	imigrant
či	či	k8xC	či
jejich	jejich	k3xOp3gMnPc2	jejich
potomků	potomek	k1gMnPc2	potomek
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
–	–	k?	–
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
Hongkongu	Hongkong	k1gInSc2	Hongkong
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
nemá	mít	k5eNaImIp3nS	mít
uzákoněný	uzákoněný	k2eAgInSc4d1	uzákoněný
oficiální	oficiální	k2eAgInSc4d1	oficiální
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
de	de	k?	de
facto	facto	k1gNnSc4	facto
jím	on	k3xPp3gNnSc7	on
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
na	na	k7c6	na
celých	celý	k2eAgInPc6d1	celý
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
včetně	včetně	k7c2	včetně
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
různými	různý	k2eAgInPc7d1	různý
regionálními	regionální	k2eAgInPc7d1	regionální
dialekty	dialekt	k1gInPc7	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
mluveným	mluvený	k2eAgMnPc3d1	mluvený
jazykům	jazyk	k1gMnPc3	jazyk
patří	patřit	k5eAaImIp3nS	patřit
velština	velština	k1gFnSc1	velština
<g/>
,	,	kIx,	,
skotská	skotský	k2eAgFnSc1d1	skotská
gaelština	gaelština	k1gFnSc1	gaelština
<g/>
,	,	kIx,	,
irská	irský	k2eAgFnSc1d1	irská
gaelština	gaelština	k1gFnSc1	gaelština
a	a	k8xC	a
kornština	kornština	k1gFnSc1	kornština
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
keltských	keltský	k2eAgInPc2d1	keltský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Odhadem	odhad	k1gInSc7	odhad
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
95	[number]	k4	95
<g/>
%	%	kIx~	%
britské	britský	k2eAgFnSc2d1	britská
populace	populace	k1gFnSc2	populace
anglických	anglický	k2eAgMnPc2d1	anglický
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
5,5	[number]	k4	5,5
<g/>
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
mluví	mluvit	k5eAaImIp3nP	mluvit
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
zavlekly	zavleknout	k5eAaPmAgInP	zavleknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
relativně	relativně	k6eAd1	relativně
nedávné	dávný	k2eNgFnSc2d1	nedávná
imigrace	imigrace	k1gFnSc2	imigrace
<g/>
.	.	kIx.	.
</s>
<s>
Takovou	takový	k3xDgFnSc7	takový
největší	veliký	k2eAgFnSc7d3	veliký
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
jazyky	jazyk	k1gInPc4	jazyk
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
jako	jako	k8xC	jako
paňdžábština	paňdžábština	k1gFnSc1	paňdžábština
<g/>
,	,	kIx,	,
urdština	urdština	k1gFnSc1	urdština
<g/>
,	,	kIx,	,
hindština	hindština	k1gFnSc1	hindština
<g/>
,	,	kIx,	,
bengálština	bengálština	k1gFnSc1	bengálština
<g/>
,	,	kIx,	,
tamilština	tamilština	k1gFnSc1	tamilština
a	a	k8xC	a
gudžarátština	gudžarátština	k1gFnSc1	gudžarátština
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupen	k2eAgFnPc4d1	zastoupena
2,7	[number]	k4	2,7
<g/>
%	%	kIx~	%
britské	britský	k2eAgFnSc2d1	britská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
polština	polština	k1gFnSc1	polština
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
druhým	druhý	k4xOgInSc7	druhý
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
546	[number]	k4	546
000	[number]	k4	000
mluvčími	mluvčí	k1gMnPc7	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Převažujícím	převažující	k2eAgNnSc7d1	převažující
náboženstvím	náboženství	k1gNnSc7	náboženství
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Dominují	dominovat	k5eAaImIp3nP	dominovat
<g/>
:	:	kIx,	:
Církev	církev	k1gFnSc1	církev
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Církev	církev	k1gFnSc1	církev
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
Skotská	skotský	k2eAgFnSc1d1	skotská
episkopální	episkopální	k2eAgFnSc1d1	episkopální
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
Církev	církev	k1gFnSc1	církev
Irska	Irsko	k1gNnSc2	Irsko
tj.	tj.	kA	tj.
Anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
asi	asi	k9	asi
45	[number]	k4	45
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
protestantské	protestantský	k2eAgFnSc2d1	protestantská
církve	církev	k1gFnSc2	církev
-	-	kIx~	-
nejvíce	nejvíce	k6eAd1	nejvíce
skotští	skotský	k2eAgMnPc1d1	skotský
presbyteriáni	presbyterián	k1gMnPc1	presbyterián
(	(	kIx(	(
<g/>
Církev	církev	k1gFnSc1	církev
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
asi	asi	k9	asi
5,5	[number]	k4	5,5
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metodisté	metodista	k1gMnPc1	metodista
(	(	kIx(	(
<g/>
asi	asi	k9	asi
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
církve	církev	k1gFnPc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hlavně	hlavně	k9	hlavně
katolíci	katolík	k1gMnPc1	katolík
(	(	kIx(	(
<g/>
asi	asi	k9	asi
13	[number]	k4	13
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
včetně	včetně	k7c2	včetně
východních	východní	k2eAgMnPc2d1	východní
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
i	i	k9	i
personální	personální	k2eAgInSc1d1	personální
Ordinariát	ordinariát	k1gInSc1	ordinariát
Our	Our	k1gFnSc2	Our
Lady	Lada	k1gFnSc2	Lada
(	(	kIx(	(
<g/>
Naší	náš	k3xOp1gFnSc7	náš
Paní	paní	k1gFnSc7	paní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
)	)	kIx)	)
z	z	k7c2	z
Walsinghamu	Walsingham	k1gInSc2	Walsingham
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
měl	mít	k5eAaImAgInS	mít
1	[number]	k4	1
500	[number]	k4	500
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
57	[number]	k4	57
kněží	kněz	k1gMnPc2	kněz
ve	v	k7c6	v
31	[number]	k4	31
farnostech	farnost	k1gFnPc6	farnost
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
asi	asi	k9	asi
12	[number]	k4	12
500	[number]	k4	500
věřících	věřící	k2eAgMnPc2d1	věřící
<g/>
,	,	kIx,	,
81	[number]	k4	81
kněží	kněz	k1gMnPc2	kněz
ve	v	k7c6	v
40	[number]	k4	40
farnostech	farnost	k1gFnPc6	farnost
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
asi	asi	k9	asi
13	[number]	k4	13
500	[number]	k4	500
věřících	věřící	k1gMnPc2	věřící
a	a	k8xC	a
86	[number]	k4	86
kněží	kněz	k1gMnPc2	kněz
ve	v	k7c6	v
40	[number]	k4	40
farnostech	farnost	k1gFnPc6	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Ordinář	ordinář	k1gMnSc1	ordinář
<g/>
:	:	kIx,	:
Keith	Keith	k1gMnSc1	Keith
Newton	Newton	k1gMnSc1	Newton
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
konvertoval	konvertovat	k5eAaBmAgInS	konvertovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ordinářem	ordinář	k1gMnSc7	ordinář
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
syrských	syrský	k2eAgMnPc2d1	syrský
jakobitů	jakobita	k1gMnPc2	jakobita
<g/>
:	:	kIx,	:
Patriarchální	patriarchální	k2eAgInSc1d1	patriarchální
vikariát	vikariát	k1gInSc1	vikariát
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
1	[number]	k4	1
farnost	farnost	k1gFnSc4	farnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
Malankarský	Malankarský	k2eAgInSc4d1	Malankarský
patriarchální	patriarchální	k2eAgInSc4d1	patriarchální
vikariát-	vikariát-	k?	vikariát-
<g/>
24	[number]	k4	24
farností	farnost	k1gFnPc2	farnost
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
4	[number]	k4	4
farnosti	farnost	k1gFnSc2	farnost
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
působí	působit	k5eAaImIp3nP	působit
i	i	k9	i
koptové	kopt	k1gMnPc1	kopt
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
4	[number]	k4	4
koptské	koptský	k2eAgFnSc2d1	koptská
jurisdikce	jurisdikce	k1gFnSc2	jurisdikce
<g/>
:	:	kIx,	:
diecéze	diecéze	k1gFnSc1	diecéze
Midlands	Midlands	k1gInSc1	Midlands
(	(	kIx(	(
<g/>
sídlo	sídlo	k1gNnSc1	sídlo
v	v	k7c6	v
Solihullu	Solihullo	k1gNnSc6	Solihullo
<g/>
,	,	kIx,	,
hrabství	hrabství	k1gNnSc6	hrabství
Warwickshire	Warwickshir	k1gMnSc5	Warwickshir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diecéze	diecéze	k1gFnSc1	diecéze
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Anglie	Anglie	k1gFnSc2	Anglie
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koptové	koptový	k2eAgFnSc2d1	Koptová
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
Anglie	Anglie	k1gFnSc2	Anglie
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
diecéze	diecéze	k1gFnSc2	diecéze
pod	pod	k7c7	pod
jurisdikcí	jurisdikce	k1gFnSc7	jurisdikce
koptského	koptský	k2eAgMnSc2d1	koptský
papeže	papež	k1gMnSc2	papež
<g/>
/	/	kIx~	/
<g/>
patriarchy	patriarcha	k1gMnSc2	patriarcha
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
generální	generální	k2eAgMnSc1d1	generální
biskup	biskup	k1gMnSc1	biskup
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Stevenage	Stevenage	k1gFnSc6	Stevenage
v	v	k7c4	v
Hearts	Hearts	k1gInSc4	Hearts
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
celkem	celkem	k6eAd1	celkem
32	[number]	k4	32
farností	farnost	k1gFnPc2	farnost
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
samostatná	samostatný	k2eAgFnSc1d1	samostatná
diecéze	diecéze	k1gFnSc1	diecéze
pro	pro	k7c4	pro
britské	britský	k2eAgMnPc4d1	britský
konvertity	konvertita	k1gMnPc4	konvertita
<g/>
:	:	kIx,	:
Britská	britský	k2eAgFnSc1d1	britská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
metropolitou	metropolita	k1gMnSc7	metropolita
(	(	kIx(	(
<g/>
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
má	mít	k5eAaImIp3nS	mít
13	[number]	k4	13
farností	farnost	k1gFnPc2	farnost
a	a	k8xC	a
misií	misie	k1gFnPc2	misie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slouží	sloužit	k5eAaImIp3nS	sloužit
5	[number]	k4	5
kněží	kněz	k1gMnPc2	kněz
a	a	k8xC	a
7	[number]	k4	7
jáhnů	jáhen	k1gMnPc2	jáhen
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
neustále	neustále	k6eAd1	neustále
početně	početně	k6eAd1	početně
narůstající	narůstající	k2eAgFnSc1d1	narůstající
menšina	menšina	k1gFnSc1	menšina
muslimů	muslim	k1gMnPc2	muslim
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
asi	asi	k9	asi
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
4,4	[number]	k4	4,4
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
též	též	k9	též
početné	početný	k2eAgFnPc4d1	početná
minority	minorita	k1gFnPc4	minorita
hinduistů	hinduista	k1gMnPc2	hinduista
a	a	k8xC	a
sikhů	sikh	k1gMnPc2	sikh
(	(	kIx(	(
<g/>
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
asi	asi	k9	asi
2,5	[number]	k4	2,5
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
také	také	k9	také
menšina	menšina	k1gFnSc1	menšina
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
ekonomika	ekonomika	k1gFnSc1	ekonomika
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
svou	svůj	k3xOyFgFnSc4	svůj
potřebu	potřeba	k1gFnSc4	potřeba
dalších	další	k2eAgFnPc2d1	další
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
především	především	k9	především
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
migračních	migrační	k2eAgFnPc2d1	migrační
tradic	tradice	k1gFnPc2	tradice
směřujících	směřující	k2eAgFnPc2d1	směřující
z	z	k7c2	z
Irské	irský	k2eAgFnSc2d1	irská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945-1959	[number]	k4	1945-1959
se	se	k3xPyFc4	se
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
asi	asi	k9	asi
350	[number]	k4	350
000	[number]	k4	000
Irů	Ir	k1gMnPc2	Ir
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
téměř	téměř	k6eAd1	téměř
milion	milion	k4xCgInSc1	milion
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
zdrojem	zdroj	k1gInSc7	zdroj
příchozích	příchozí	k1gFnPc2	příchozí
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
-	-	kIx~	-
převážně	převážně	k6eAd1	převážně
afrokaribského	afrokaribský	k2eAgInSc2d1	afrokaribský
a	a	k8xC	a
asijského	asijský	k2eAgInSc2d1	asijský
původu	původ	k1gInSc2	původ
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
pocházelo	pocházet	k5eAaImAgNnS	pocházet
z	z	k7c2	z
karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
asi	asi	k9	asi
265	[number]	k4	265
000	[number]	k4	000
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
,	,	kIx,	,
z	z	k7c2	z
Pákistánu	Pákistán	k1gInSc2	Pákistán
asi	asi	k9	asi
128	[number]	k4	128
000	[number]	k4	000
a	a	k8xC	a
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
asi	asi	k9	asi
241	[number]	k4	241
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
postkoloniální	postkoloniální	k2eAgFnSc2d1	postkoloniální
migrace	migrace	k1gFnSc2	migrace
tvořili	tvořit	k5eAaImAgMnP	tvořit
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
kolonie	kolonie	k1gFnSc2	kolonie
Kypru	Kypr	k1gInSc2	Kypr
-	-	kIx~	-
počty	počet	k1gInPc4	počet
imigrantů	imigrant	k1gMnPc2	imigrant
(	(	kIx(	(
<g/>
Řeků	Řek	k1gMnPc2	Řek
a	a	k8xC	a
Turků	Turek	k1gMnPc2	Turek
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951-1971	[number]	k4	1951-1971
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
z	z	k7c2	z
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
na	na	k7c4	na
73	[number]	k4	73
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
tvořily	tvořit	k5eAaImAgInP	tvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
osoby	osoba	k1gFnSc2	osoba
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
asi	asi	k9	asi
104	[number]	k4	104
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
imigrační	imigrační	k2eAgFnSc6d1	imigrační
politice	politika	k1gFnSc6	politika
nešlo	jít	k5eNaImAgNnS	jít
primárně	primárně	k6eAd1	primárně
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
nebo	nebo	k8xC	nebo
omezení	omezení	k1gNnSc4	omezení
pracovní	pracovní	k2eAgFnSc2d1	pracovní
migrace	migrace	k1gFnSc2	migrace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
otázky	otázka	k1gFnPc4	otázka
imigrace	imigrace	k1gFnSc2	imigrace
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
koloniálních	koloniální	k2eAgFnPc2d1	koloniální
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
mechanismem	mechanismus	k1gInSc7	mechanismus
tu	tu	k6eAd1	tu
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
národního	národní	k2eAgNnSc2d1	národní
občanství	občanství	k1gNnSc2	občanství
místo	místo	k7c2	místo
členství	členství	k1gNnSc2	členství
v	v	k7c6	v
Commonwealthu	Commonwealth	k1gInSc6	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
ze	z	k7c2	z
států	stát	k1gInPc2	stát
ES	es	k1gNnPc2	es
tím	ten	k3xDgNnSc7	ten
nebylo	být	k5eNaImAgNnS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
dostali	dostat	k5eAaPmAgMnP	dostat
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
asi	asi	k9	asi
216	[number]	k4	216
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
druhé	druhý	k4xOgFnSc2	druhý
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
skupiny	skupina	k1gFnSc2	skupina
hned	hned	k6eAd1	hned
za	za	k7c4	za
Iry	Ir	k1gMnPc4	Ir
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
určitá	určitý	k2eAgFnSc1d1	určitá
struktura	struktura	k1gFnSc1	struktura
přistěhovalectví	přistěhovalectví	k1gNnSc2	přistěhovalectví
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stabilizovala	stabilizovat	k5eAaBmAgFnS	stabilizovat
také	také	k9	také
cestou	cestou	k7c2	cestou
slučování	slučování	k1gNnSc2	slučování
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
tu	tu	k6eAd1	tu
byla	být	k5eAaImAgFnS	být
imigrační	imigrační	k2eAgNnSc4d1	imigrační
omezení	omezení	k1gNnSc4	omezení
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
pracovní	pracovní	k2eAgFnPc4d1	pracovní
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
neustálého	neustálý	k2eAgInSc2d1	neustálý
příchodu	příchod	k1gInSc2	příchod
dalších	další	k2eAgMnPc2d1	další
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
odvíjelo	odvíjet	k5eAaImAgNnS	odvíjet
úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c4	o
podporu	podpora	k1gFnSc4	podpora
integrace	integrace	k1gFnSc2	integrace
již	již	k6eAd1	již
trvale	trvale	k6eAd1	trvale
usazených	usazený	k2eAgFnPc2d1	usazená
přistěhovaleckých	přistěhovalecký	k2eAgFnPc2d1	přistěhovalecká
populací	populace	k1gFnPc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
omezení	omezení	k1gNnSc4	omezení
imigrace	imigrace	k1gFnSc2	imigrace
a	a	k8xC	a
integrační	integrační	k2eAgFnSc1d1	integrační
podpora	podpora	k1gFnSc1	podpora
na	na	k7c6	na
národní	národní	k2eAgFnSc6d1	národní
úrovni	úroveň	k1gFnSc6	úroveň
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
měřítku	měřítko	k1gNnSc6	měřítko
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
úsilím	úsilí	k1gNnSc7	úsilí
o	o	k7c4	o
nadnárodní	nadnárodní	k2eAgInPc4d1	nadnárodní
regulační	regulační	k2eAgInPc4d1	regulační
systémy	systém	k1gInPc4	systém
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
sociálních	sociální	k2eAgFnPc2d1	sociální
jistot	jistota	k1gFnPc2	jistota
u	u	k7c2	u
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
migrace	migrace	k1gFnSc2	migrace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
imigrovalo	imigrovat	k5eAaBmAgNnS	imigrovat
624	[number]	k4	624
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
rok	rok	k1gInSc4	rok
odstěhovalo	odstěhovat	k5eAaPmAgNnS	odstěhovat
327	[number]	k4	327
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nových	nový	k2eAgMnPc2d1	nový
imigrantů	imigrant	k1gMnPc2	imigrant
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
také	také	k9	také
migrace	migrace	k1gFnSc1	migrace
z	z	k7c2	z
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
je	být	k5eAaImIp3nS	být
nejrychleji	rychle	k6eAd3	rychle
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
náboženstvím	náboženství	k1gNnSc7	náboženství
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
počet	počet	k1gInSc4	počet
muslimů	muslim	k1gMnPc2	muslim
stoupl	stoupnout	k5eAaPmAgMnS	stoupnout
z	z	k7c2	z
méně	málo	k6eAd2	málo
než	než	k8xS	než
22	[number]	k4	22
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
na	na	k7c4	na
1,6	[number]	k4	1,6
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
na	na	k7c4	na
2,8	[number]	k4	2,8
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
imigrace	imigrace	k1gFnSc2	imigrace
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc2d2	vyšší
porodnosti	porodnost	k1gFnSc2	porodnost
menšin	menšina	k1gFnPc2	menšina
se	se	k3xPyFc4	se
etnická	etnický	k2eAgFnSc1d1	etnická
skladba	skladba	k1gFnSc1	skladba
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
rychle	rychle	k6eAd1	rychle
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
patřilo	patřit	k5eAaImAgNnS	patřit
už	už	k6eAd1	už
24,3	[number]	k4	24,3
<g/>
%	%	kIx~	%
dětí	dítě	k1gFnPc2	dítě
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
k	k	k7c3	k
etnickým	etnický	k2eAgFnPc3d1	etnická
menšinám	menšina	k1gFnPc3	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
vysoce	vysoce	k6eAd1	vysoce
třídní	třídní	k2eAgFnSc1d1	třídní
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
používají	používat	k5eAaImIp3nP	používat
škatulky	škatulka	k1gFnPc4	škatulka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
bílé	bílý	k2eAgInPc4d1	bílý
a	a	k8xC	a
modré	modrý	k2eAgInPc4d1	modrý
límečky	límeček	k1gInPc4	límeček
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
dívají	dívat	k5eAaImIp3nP	dívat
přezíravě	přezíravě	k6eAd1	přezíravě
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
ze	z	k7c2	z
Skotska	Skotsko	k1gNnSc2	Skotsko
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc2	Wales
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dokonce	dokonce	k9	dokonce
dokáží	dokázat	k5eAaPmIp3nP	dokázat
odhadnout	odhadnout	k5eAaPmF	odhadnout
třídu	třída	k1gFnSc4	třída
podle	podle	k7c2	podle
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
akcentu	akcent	k1gInSc2	akcent
<g/>
,	,	kIx,	,
přízvuku	přízvuk	k1gInSc2	přízvuk
a	a	k8xC	a
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nemá	mít	k5eNaImIp3nS	mít
emoce	emoce	k1gFnSc1	emoce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
nedávají	dávat	k5eNaImIp3nP	dávat
najevo	najevo	k6eAd1	najevo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
někoho	někdo	k3yInSc4	někdo
urazíte	urazit	k5eAaPmIp2nP	urazit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
nezmění	změnit	k5eNaPmIp3nS	změnit
výraz	výraz	k1gInSc1	výraz
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
vysoce	vysoce	k6eAd1	vysoce
multikulturní	multikulturní	k2eAgFnSc6d1	multikulturní
společnosti	společnost	k1gFnSc6	společnost
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dosti	dosti	k6eAd1	dosti
divné	divný	k2eAgNnSc1d1	divné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
starousedlíci	starousedlík	k1gMnPc1	starousedlík
lpí	lpět	k5eAaImIp3nP	lpět
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
rodokmenu	rodokmen	k1gInSc6	rodokmen
<g/>
,	,	kIx,	,
chování	chování	k1gNnSc6	chování
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
byt	byt	k1gInSc4	byt
tím	ten	k3xDgMnSc7	ten
pravým	pravý	k2eAgMnSc7d1	pravý
Angličanem	Angličan	k1gMnSc7	Angličan
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
upouštějí	upouštět	k5eAaImIp3nP	upouštět
od	od	k7c2	od
starých	starý	k2eAgInPc2d1	starý
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
interagují	interagovat	k5eAaImIp3nP	interagovat
s	s	k7c7	s
novými	nový	k2eAgMnPc7d1	nový
přistěhovalci	přistěhovalec	k1gMnPc7	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
je	být	k5eAaImIp3nS	být
početně	početně	k6eAd1	početně
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc1	každý
světový	světový	k2eAgInSc1d1	světový
národ	národ	k1gInSc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
zemi	zem	k1gFnSc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
svátků	svátek	k1gInPc2	svátek
a	a	k8xC	a
oslav	oslava	k1gFnPc2	oslava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nazývají	nazývat	k5eAaImIp3nP	nazývat
bank	bank	k1gInSc4	bank
holidays	holidays	k6eAd1	holidays
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c4	v
tyto	tento	k3xDgInPc4	tento
dny	den	k1gInPc4	den
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
banky	banka	k1gFnPc1	banka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zavřené	zavřený	k2eAgFnSc6d1	zavřená
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
ze	z	k7c2	z
svátků	svátek	k1gInPc2	svátek
jsou	být	k5eAaImIp3nP	být
Vánoce	Vánoce	k1gFnPc1	Vánoce
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
<g/>
.	.	kIx.	.
</s>
<s>
Vánoce	Vánoce	k1gFnPc1	Vánoce
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
zdobí	zdobit	k5eAaImIp3nS	zdobit
množstvím	množství	k1gNnSc7	množství
světel	světlo	k1gNnPc2	světlo
a	a	k8xC	a
typických	typický	k2eAgFnPc2d1	typická
vánočních	vánoční	k2eAgFnPc2d1	vánoční
ozdob	ozdoba	k1gFnPc2	ozdoba
osvětlujících	osvětlující	k2eAgFnPc2d1	osvětlující
ulice	ulice	k1gFnPc4	ulice
a	a	k8xC	a
tradičními	tradiční	k2eAgFnPc7d1	tradiční
koledami	koleda	k1gFnPc7	koleda
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
netrpělivě	trpělivě	k6eNd1	trpělivě
očekávají	očekávat	k5eAaImIp3nP	očekávat
příchod	příchod	k1gInSc4	příchod
Otce	otec	k1gMnSc2	otec
Vánoc	Vánoce	k1gFnPc2	Vánoce
(	(	kIx(	(
<g/>
Father	Fathra	k1gFnPc2	Fathra
Christmas	Christmas	k1gInSc1	Christmas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přistane	přistat	k5eAaPmIp3nS	přistat
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
sobím	sobí	k2eAgNnSc7d1	sobí
spřežením	spřežení	k1gNnSc7	spřežení
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
za	za	k7c7	za
komínem	komín	k1gInSc7	komín
<g/>
.	.	kIx.	.
</s>
<s>
Vyvěšují	vyvěšovat	k5eAaImIp3nP	vyvěšovat
svou	svůj	k3xOyFgFnSc4	svůj
vánoční	vánoční	k2eAgFnSc4d1	vánoční
punčochu	punčocha	k1gFnSc4	punčocha
k	k	k7c3	k
nohám	noha	k1gFnPc3	noha
postele	postel	k1gFnSc2	postel
a	a	k8xC	a
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c4	na
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
rozbalují	rozbalovat	k5eAaImIp3nP	rozbalovat
pouze	pouze	k6eAd1	pouze
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
v	v	k7c6	v
čase	čas	k1gInSc6	čas
snídaně	snídaně	k1gFnSc2	snídaně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
tradiční	tradiční	k2eAgInSc4d1	tradiční
vánoční	vánoční	k2eAgInSc4d1	vánoční
oběd	oběd	k1gInSc4	oběd
–	–	k?	–
pečeného	pečený	k2eAgMnSc4d1	pečený
krocana	krocan	k1gMnSc4	krocan
a	a	k8xC	a
vánoční	vánoční	k2eAgInSc4d1	vánoční
pudding	pudding	k1gInSc4	pudding
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Boxing	boxing	k1gInSc1	boxing
Day	Day	k1gFnSc2	Day
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
chodí	chodit	k5eAaImIp3nP	chodit
rodiče	rodič	k1gMnPc1	rodič
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
na	na	k7c4	na
představení	představení	k1gNnSc4	představení
pantomimy	pantomima	k1gFnSc2	pantomima
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
před	před	k7c7	před
Novým	nový	k2eAgInSc7d1	nový
rokem	rok	k1gInSc7	rok
(	(	kIx(	(
<g/>
Silvestr	Silvestr	k1gInSc1	Silvestr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
noc	noc	k1gFnSc1	noc
plná	plný	k2eAgFnSc1d1	plná
veselí	veselit	k5eAaImIp3nS	veselit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
rodinné	rodinný	k2eAgFnPc4d1	rodinná
oslavy	oslava	k1gFnPc4	oslava
a	a	k8xC	a
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
spojujíce	spojovat	k5eAaImSgMnP	spojovat
své	svůj	k3xOyFgInPc4	svůj
paže	paže	k1gNnSc4	paže
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
zpívají	zpívat	k5eAaImIp3nP	zpívat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Auld	Auld	k1gMnSc1	Auld
lang	lang	k1gMnSc1	lang
syne	synat	k5eAaPmIp3nS	synat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
se	se	k3xPyFc4	se
poslední	poslední	k2eAgInSc1d1	poslední
den	den	k1gInSc1	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
nazývá	nazývat	k5eAaImIp3nS	nazývat
Hogmanay	Hogmanay	k1gInPc4	Hogmanay
a	a	k8xC	a
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
se	se	k3xPyFc4	se
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgMnSc1	první
návštěvník	návštěvník	k1gMnSc1	návštěvník
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
roce	rok	k1gInSc6	rok
přináší	přinášet	k5eAaImIp3nS	přinášet
do	do	k7c2	do
domu	dům	k1gInSc2	dům
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
dávají	dávat	k5eAaImIp3nP	dávat
kousek	kousek	k1gInSc4	kousek
uhlí	uhlí	k1gNnSc2	uhlí
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jejich	jejich	k3xOp3gInSc1	jejich
komín	komín	k1gInSc1	komín
kouřil	kouřit	k5eAaImAgInS	kouřit
co	co	k3yInSc4	co
nejdéle	dlouho	k6eAd3	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
jako	jako	k9	jako
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
prastarý	prastarý	k2eAgInSc1d1	prastarý
symbol	symbol	k1gInSc1	symbol
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
nového	nový	k2eAgInSc2d1	nový
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
křesťané	křesťan	k1gMnPc1	křesťan
připomínají	připomínat	k5eAaImIp3nP	připomínat
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
zmrtvýchvstání	zmrtvýchvstání	k1gNnSc4	zmrtvýchvstání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
další	další	k2eAgInSc4d1	další
bank	bank	k1gInSc4	bank
holidays	holidays	k6eAd1	holidays
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
a	a	k8xC	a
poslední	poslední	k2eAgNnPc1d1	poslední
pondělí	pondělí	k1gNnPc1	pondělí
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
pondělí	pondělí	k1gNnSc4	pondělí
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
významné	významný	k2eAgInPc4d1	významný
dny	den	k1gInPc4	den
–	–	k?	–
Den	den	k1gInSc4	den
svatého	svatý	k1gMnSc4	svatý
Patrika	Patrik	k1gMnSc4	Patrik
(	(	kIx(	(
<g/>
St.	st.	kA	st.
Patrick	Patrick	k1gMnSc1	Patrick
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Day	Day	k1gFnSc7	Day
<g/>
)	)	kIx)	)
a	a	k8xC	a
protestantský	protestantský	k2eAgInSc1d1	protestantský
12	[number]	k4	12
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
čili	čili	k8xC	čili
The	The	k1gMnSc1	The
Twelfth	Twelfth	k1gMnSc1	Twelfth
nebo	nebo	k8xC	nebo
Orangemen	Orangemen	k2eAgMnSc1d1	Orangemen
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Day	Day	k1gFnSc7	Day
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
sv.	sv.	kA	sv.
Valentýna	Valentýna	k1gFnSc1	Valentýna
–	–	k?	–
St.	st.	kA	st.
Valentines	Valentines	k1gMnSc1	Valentines
Day	Day	k1gMnSc1	Day
–	–	k?	–
den	den	k1gInSc1	den
zamilovaných	zamilovaná	k1gFnPc2	zamilovaná
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
máj	máj	k1gFnSc1	máj
–	–	k?	–
May	May	k1gMnSc1	May
Day	Day	k1gMnSc1	Day
–	–	k?	–
<g/>
oslava	oslava	k1gFnSc1	oslava
plodnosti	plodnost	k1gFnSc2	plodnost
s	s	k7c7	s
tancem	tanec	k1gInSc7	tanec
okolo	okolo	k7c2	okolo
májky	májka	k1gFnSc2	májka
<g/>
.	.	kIx.	.
</s>
<s>
Svatojánská	svatojánský	k2eAgFnSc1d1	Svatojánská
noc	noc	k1gFnSc1	noc
–	–	k?	–
Midsummer	Midsummer	k1gInSc1	Midsummer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Day	Day	k1gFnSc7	Day
–	–	k?	–
různé	různý	k2eAgFnSc2d1	různá
oslavy	oslava	k1gFnSc2	oslava
<g/>
,	,	kIx,	,
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
vítaní	vítaný	k2eAgMnPc1d1	vítaný
východu	východ	k1gInSc3	východ
slunce	slunce	k1gNnSc4	slunce
ve	v	k7c6	v
Stonehenge	Stonehenge	k1gFnSc6	Stonehenge
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
oslava	oslava	k1gFnSc1	oslava
královniných	královnin	k2eAgFnPc2d1	Královnina
narozenin	narozeniny	k1gFnPc2	narozeniny
–	–	k?	–
červen	červen	k1gInSc1	červen
<g/>
.	.	kIx.	.
</s>
<s>
Guy	Guy	k?	Guy
Fawkes	Fawkes	k1gMnSc1	Fawkes
Night	Night	k1gMnSc1	Night
–	–	k?	–
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
ohňostroje	ohňostroj	k1gInPc1	ohňostroj
připomínají	připomínat	k5eAaImIp3nP	připomínat
neúspěšný	úspěšný	k2eNgInSc4d1	neúspěšný
pokus	pokus	k1gInSc4	pokus
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
katolických	katolický	k2eAgMnPc2d1	katolický
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
(	(	kIx(	(
<g/>
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Guy	Guy	k1gMnSc7	Guy
Fawkesem	Fawkes	k1gInSc7	Fawkes
<g/>
)	)	kIx)	)
vyhodit	vyhodit	k5eAaPmF	vyhodit
do	do	k7c2	do
povětří	povětří	k1gNnSc2	povětří
budovy	budova	k1gFnSc2	budova
Parlamentu	parlament	k1gInSc2	parlament
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
výbušnin	výbušnina	k1gFnPc2	výbušnina
a	a	k8xC	a
třaskavin	třaskavina	k1gFnPc2	třaskavina
<g/>
.	.	kIx.	.
</s>
<s>
Halloween	Halloween	k2eAgInSc4d1	Halloween
–	–	k?	–
předvečer	předvečer	k1gInSc4	předvečer
svátku	svátek	k1gInSc2	svátek
Všech	všecek	k3xTgMnPc2	všecek
svatých	svatý	k1gMnPc2	svatý
–	–	k?	–
noc	noc	k1gFnSc1	noc
tradičních	tradiční	k2eAgFnPc2d1	tradiční
her	hra	k1gFnPc2	hra
a	a	k8xC	a
veselí	veselí	k1gNnSc2	veselí
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
příměří	příměří	k1gNnSc2	příměří
(	(	kIx(	(
<g/>
Remembrance	Remembrance	k1gFnSc1	Remembrance
Day	Day	k1gFnSc1	Day
<g/>
)	)	kIx)	)
–	–	k?	–
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
padlé	padlý	k1gMnPc4	padlý
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
světových	světový	k2eAgFnPc6d1	světová
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
britská	britský	k2eAgFnSc1d1	britská
literatura	literatura	k1gFnSc1	literatura
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
spojenou	spojený	k2eAgFnSc4d1	spojená
se	se	k3xPyFc4	se
Spojeným	spojený	k2eAgNnSc7d1	spojené
královstvím	království	k1gNnSc7	království
<g/>
,	,	kIx,	,
ostrovem	ostrov	k1gInSc7	ostrov
Man	mana	k1gFnPc2	mana
a	a	k8xC	a
Normanskými	normanský	k2eAgInPc7d1	normanský
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
britské	britský	k2eAgFnSc2d1	britská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
206	[number]	k4	206
000	[number]	k4	000
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
nevětším	veliký	k2eNgMnSc7d2	veliký
vydavatelem	vydavatel	k1gMnSc7	vydavatel
knih	kniha	k1gFnPc2	kniha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
William	William	k1gInSc4	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
dramatika	dramatika	k1gFnSc1	dramatika
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
současníci	současník	k1gMnPc1	současník
Christopher	Christophra	k1gFnPc2	Christophra
Marlowe	Marlow	k1gInSc2	Marlow
a	a	k8xC	a
Ben	Ben	k1gInSc1	Ben
Jonson	Jonsona	k1gFnPc2	Jonsona
se	se	k3xPyFc4	se
také	také	k9	také
těší	těšit	k5eAaImIp3nP	těšit
velké	velký	k2eAgFnSc3d1	velká
úctě	úcta	k1gFnSc3	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dramatiky	dramatika	k1gFnPc4	dramatika
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
patří	patřit	k5eAaImIp3nS	patřit
Alan	Alan	k1gMnSc1	Alan
Ayckbourn	Ayckbourn	k1gMnSc1	Ayckbourn
<g/>
,	,	kIx,	,
Harold	Harold	k1gMnSc1	Harold
Pinter	Pinter	k1gMnSc1	Pinter
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Frayn	Frayn	k1gMnSc1	Frayn
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Stoppard	Stoppard	k1gMnSc1	Stoppard
a	a	k8xC	a
David	David	k1gMnSc1	David
Edgar	Edgar	k1gMnSc1	Edgar
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
prvky	prvek	k1gInPc4	prvek
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
radikalismu	radikalismus	k1gInSc2	radikalismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
zachovanou	zachovaný	k2eAgFnSc7d1	zachovaná
památkou	památka	k1gFnSc7	památka
britské	britský	k2eAgFnSc2d1	britská
poezie	poezie	k1gFnSc2	poezie
je	být	k5eAaImIp3nS	být
Y	Y	kA	Y
Gododdin	Gododdina	k1gFnPc2	Gododdina
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
v	v	k7c6	v
Hen	hena	k1gFnPc2	hena
Ogledd	Ogledd	k1gInSc1	Ogledd
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Old	Olda	k1gFnPc2	Olda
North	Northa	k1gFnPc2	Northa
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Starý	starý	k2eAgInSc1d1	starý
sever	sever	k1gInSc1	sever
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Napsána	napsán	k2eAgFnSc1d1	napsána
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
cumbrijštině	cumbrijština	k1gFnSc6	cumbrijština
nebo	nebo	k8xC	nebo
staré	starý	k2eAgFnSc6d1	stará
velštině	velština	k1gFnSc6	velština
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
raný	raný	k2eAgInSc4d1	raný
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
krále	král	k1gMnSc4	král
Artuše	Artuš	k1gMnSc4	Artuš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
cca	cca	kA	cca
sedmého	sedmý	k4xOgNnSc2	sedmý
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
přerušen	přerušit	k5eAaPmNgInS	přerušit
kontakt	kontakt	k1gInSc4	kontakt
mezi	mezi	k7c7	mezi
Walesem	Wales	k1gInSc7	Wales
a	a	k8xC	a
Starým	starý	k2eAgInSc7d1	starý
severem	sever	k1gInSc7	sever
a	a	k8xC	a
pozornost	pozornost	k1gFnSc1	pozornost
velšské	velšský	k2eAgFnSc2d1	velšská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Artušovi	Artuš	k1gMnSc6	Artuš
dále	daleko	k6eAd2	daleko
rozvinuta	rozvinut	k2eAgFnSc1d1	rozvinuta
Geoffreyem	Geoffrey	k1gMnSc7	Geoffrey
z	z	k7c2	z
Monmouthu	Monmouth	k1gInSc2	Monmouth
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
středověké	středověký	k2eAgNnSc4d1	středověké
a	a	k8xC	a
raně	raně	k6eAd1	raně
novověké	novověký	k2eAgFnSc2d1	novověká
anglické	anglický	k2eAgFnSc2d1	anglická
spisovatelé	spisovatel	k1gMnPc1	spisovatel
patří	patřit	k5eAaImIp3nP	patřit
Geoffrey	Geoffrey	k1gInPc4	Geoffrey
Chaucer	Chaucra	k1gFnPc2	Chaucra
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Malory	Malora	k1gFnSc2	Malora
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
Thomas	Thomas	k1gMnSc1	Thomas
More	mor	k1gInSc5	mor
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Bunyan	Bunyan	k1gMnSc1	Bunyan
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Dryden	Drydna	k1gFnPc2	Drydna
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Addison	Addison	k1gMnSc1	Addison
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Donne	Donn	k1gInSc5	Donn
a	a	k8xC	a
John	John	k1gMnSc1	John
Milton	Milton	k1gInSc1	Milton
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
Daniel	Daniel	k1gMnSc1	Daniel
Defoe	Defo	k1gFnSc2	Defo
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
Robinsona	Robinson	k1gMnSc2	Robinson
Crusoe	Crusoe	k1gNnSc2	Crusoe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Fielding	Fielding	k1gInSc1	Fielding
a	a	k8xC	a
Samuel	Samuel	k1gMnSc1	Samuel
Richardson	Richardson	k1gMnSc1	Richardson
průkopníci	průkopník	k1gMnPc1	průkopník
moderního	moderní	k2eAgInSc2d1	moderní
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Johnson	Johnson	k1gMnSc1	Johnson
kodifikoval	kodifikovat	k5eAaBmAgMnS	kodifikovat
moderní	moderní	k2eAgFnSc4d1	moderní
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
básníků	básník	k1gMnPc2	básník
vynikl	vyniknout	k5eAaPmAgInS	vyniknout
Alexander	Alexandra	k1gFnPc2	Alexandra
Pope	pop	k1gInSc5	pop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prózu	próza	k1gFnSc4	próza
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
Jane	Jan	k1gMnSc5	Jan
Austen	Austen	k2eAgMnSc1d1	Austen
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
Shelley	Shellea	k1gFnSc2	Shellea
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
Lewis	Lewis	k1gFnSc2	Lewis
Carroll	Carrolla	k1gFnPc2	Carrolla
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnPc1	sestra
Brontëovy	Brontëův	k2eAgFnPc1d1	Brontëův
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickens	k1gInSc1	Dickens
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Makepeace	Makepeace	k1gFnSc2	Makepeace
Thackeray	Thackeraa	k1gFnSc2	Thackeraa
<g/>
,	,	kIx,	,
naturalista	naturalista	k1gMnSc1	naturalista
Thomas	Thomas	k1gMnSc1	Thomas
Hardy	Harda	k1gFnSc2	Harda
<g/>
,	,	kIx,	,
realistka	realistka	k1gFnSc1	realistka
George	George	k1gFnSc1	George
Eliotová	Eliotový	k2eAgFnSc1d1	Eliotová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
vizionářský	vizionářský	k2eAgMnSc1d1	vizionářský
básník	básník	k1gMnSc1	básník
William	William	k1gInSc4	William
Blake	Blak	k1gInSc2	Blak
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
vlna	vlna	k1gFnSc1	vlna
romantismu	romantismus	k1gInSc2	romantismus
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postavila	postavit	k5eAaPmAgFnS	postavit
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
básnictví	básnictví	k1gNnSc2	básnictví
a	a	k8xC	a
proslavila	proslavit	k5eAaPmAgFnS	proslavit
autory	autor	k1gMnPc4	autor
jako	jako	k9	jako
George	Georg	k1gMnSc4	Georg
Gordon	Gordon	k1gMnSc1	Gordon
Byron	Byron	k1gMnSc1	Byron
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Keats	Keatsa	k1gFnPc2	Keatsa
<g/>
,	,	kIx,	,
Percy	Perc	k2eAgFnPc1d1	Perca
Bysshe	Byssh	k1gFnPc1	Byssh
Shelley	Shellea	k1gFnSc2	Shellea
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Taylor	Taylor	k1gMnSc1	Taylor
Coleridge	Coleridge	k1gNnSc4	Coleridge
či	či	k8xC	či
William	William	k1gInSc4	William
Wordsworth	Wordswortha	k1gFnPc2	Wordswortha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
navazovala	navazovat	k5eAaImAgFnS	navazovat
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Barrettová-Browningová	Barrettová-Browningový	k2eAgFnSc1d1	Barrettová-Browningový
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
století	století	k1gNnSc6	století
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
třeba	třeba	k6eAd1	třeba
Wystan	Wystan	k1gInSc1	Wystan
Hugh	Hugha	k1gFnPc2	Hugha
Auden	Audna	k1gFnPc2	Audna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
próze	próza	k1gFnSc6	próza
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
představitelé	představitel	k1gMnPc1	představitel
realismu	realismus	k1gInSc2	realismus
D.	D.	kA	D.
H.	H.	kA	H.
Lawrence	Lawrence	k1gFnSc1	Lawrence
a	a	k8xC	a
Edward	Edward	k1gMnSc1	Edward
Morgan	morgan	k1gMnSc1	morgan
Forster	Forster	k1gMnSc1	Forster
<g/>
,	,	kIx,	,
modernistická	modernistický	k2eAgNnPc1d1	modernistické
Virginia	Virginium	k1gNnPc1	Virginium
Woolfová	Woolfový	k2eAgNnPc1d1	Woolfové
<g/>
,	,	kIx,	,
satirik	satirik	k1gMnSc1	satirik
Evelyn	Evelyn	k1gMnSc1	Evelyn
Waugh	Waugh	k1gMnSc1	Waugh
<g/>
,	,	kIx,	,
romanopisec	romanopisec	k1gMnSc1	romanopisec
a	a	k8xC	a
prorok	prorok	k1gMnSc1	prorok
George	Georg	k1gFnSc2	Georg
Orwell	Orwell	k1gInSc1	Orwell
<g/>
,	,	kIx,	,
populární	populární	k2eAgInSc1d1	populární
W.	W.	kA	W.
Somerset	Somerset	k1gInSc1	Somerset
Maugham	Maugham	k1gInSc1	Maugham
a	a	k8xC	a
Graham	graham	k1gInSc1	graham
Greene	Green	k1gInSc5	Green
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
získali	získat	k5eAaPmAgMnP	získat
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
některých	některý	k3yIgFnPc2	některý
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
<g/>
,	,	kIx,	,
též	též	k9	též
Doris	Doris	k1gFnSc1	Doris
Lessingová	Lessingový	k2eAgFnSc1d1	Lessingová
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Golding	Golding	k1gInSc1	Golding
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gFnSc2	Galswortha
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Stearns	Stearnsa	k1gFnPc2	Stearnsa
Eliot	Eliot	k1gMnSc1	Eliot
a	a	k8xC	a
Patrick	Patrick	k1gMnSc1	Patrick
White	Whit	k1gMnSc5	Whit
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
básníky	básník	k1gMnPc7	básník
jsou	být	k5eAaImIp3nP	být
Philip	Philip	k1gMnSc1	Philip
Larkin	Larkin	k1gMnSc1	Larkin
a	a	k8xC	a
Ted	Ted	k1gMnSc1	Ted
Hughes	Hughes	k1gMnSc1	Hughes
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
i	i	k9	i
žánrová	žánrový	k2eAgFnSc1d1	žánrová
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
klasiky	klasik	k1gMnPc7	klasik
sci-fi	scii	k1gFnSc2	sci-fi
patří	patřit	k5eAaImIp3nS	patřit
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
C.	C.	kA	C.
Clarke	Clarke	k1gInSc1	Clarke
<g/>
,	,	kIx,	,
Aldous	Aldous	k1gInSc1	Aldous
Huxley	Huxlea	k1gFnSc2	Huxlea
či	či	k8xC	či
Anthony	Anthona	k1gFnSc2	Anthona
Burgess	Burgessa	k1gFnPc2	Burgessa
(	(	kIx(	(
<g/>
Mechanický	mechanický	k2eAgInSc1d1	mechanický
pomeranč	pomeranč	k1gInSc1	pomeranč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasiky	klasika	k1gFnPc1	klasika
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Rudyard	Rudyard	k1gMnSc1	Rudyard
Kipling	Kipling	k1gInSc4	Kipling
<g/>
,	,	kIx,	,
A.	A.	kA	A.
A.	A.	kA	A.
Milne	Miln	k1gInSc5	Miln
(	(	kIx(	(
<g/>
tvůrce	tvůrce	k1gMnSc4	tvůrce
medvídka	medvídek	k1gMnSc4	medvídek
Pú	Pú	k1gMnSc4	Pú
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velšan	Velšan	k1gMnSc1	Velšan
Roald	Roalda	k1gFnPc2	Roalda
Dahl	dahl	k1gInSc1	dahl
<g/>
,	,	kIx,	,
Enid	Enid	k1gInSc1	Enid
Blyton	Blyton	k1gInSc1	Blyton
<g/>
,	,	kIx,	,
Beatrix	Beatrix	k1gInSc1	Beatrix
Potterová	Potterová	k1gFnSc1	Potterová
či	či	k8xC	či
Skot	Skot	k1gMnSc1	Skot
Kenneth	Kenneth	k1gMnSc1	Kenneth
Grahame	graham	k1gInSc5	graham
<g/>
.	.	kIx.	.
</s>
<s>
Autorka	autorka	k1gFnSc1	autorka
detektivek	detektivka	k1gFnPc2	detektivka
Agatha	Agatha	k1gFnSc1	Agatha
Christie	Christie	k1gFnSc2	Christie
je	být	k5eAaImIp3nS	být
nejprodávanější	prodávaný	k2eAgMnSc1d3	nejprodávanější
romanopisec	romanopisec	k1gMnSc1	romanopisec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
K	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
žánru	žánr	k1gInSc2	žánr
detektivky	detektivka	k1gFnSc2	detektivka
patří	patřit	k5eAaImIp3nS	patřit
Gilbert	Gilbert	k1gMnSc1	Gilbert
Keith	Keith	k1gMnSc1	Keith
Chesterton	Chesterton	k1gInSc4	Chesterton
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
dalším	další	k2eAgMnPc3d1	další
významným	významný	k2eAgMnPc3d1	významný
představitelům	představitel	k1gMnPc3	představitel
Raymond	Raymond	k1gMnSc1	Raymond
Chandler	Chandler	k1gMnSc1	Chandler
<g/>
.	.	kIx.	.
</s>
<s>
Ian	Ian	k?	Ian
Fleming	Fleming	k1gInSc1	Fleming
stvořil	stvořit	k5eAaPmAgInS	stvořit
postavu	postava	k1gFnSc4	postava
Jamese	Jamesa	k1gFnSc3	Jamesa
Bonda	Bond	k1gMnSc2	Bond
<g/>
,	,	kIx,	,
špionážní	špionážní	k2eAgInPc1d1	špionážní
romány	román	k1gInPc1	román
proslavily	proslavit	k5eAaPmAgInP	proslavit
ovšem	ovšem	k9	ovšem
i	i	k8xC	i
Johna	John	k1gMnSc4	John
le	le	k?	le
Carré	Carrý	k2eAgNnSc1d1	Carré
<g/>
.	.	kIx.	.
</s>
<s>
Alistair	Alistair	k1gMnSc1	Alistair
MacLean	MacLean	k1gMnSc1	MacLean
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
thrillerů	thriller	k1gInPc2	thriller
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Británie	Británie	k1gFnSc2	Británie
pocházejí	pocházet	k5eAaImIp3nP	pocházet
také	také	k9	také
nejslavnější	slavný	k2eAgMnPc1d3	nejslavnější
autoři	autor	k1gMnPc1	autor
fantasy	fantas	k1gInPc1	fantas
<g/>
:	:	kIx,	:
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
,	,	kIx,	,
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnPc2	Lewis
<g/>
,	,	kIx,	,
Terry	Terra	k1gFnPc1	Terra
Pratchett	Pratchett	k1gMnSc1	Pratchett
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
Pullman	Pullman	k1gMnSc1	Pullman
a	a	k8xC	a
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgNnPc1d1	Rowlingové
<g/>
.	.	kIx.	.
</s>
<s>
Mytologií	mytologie	k1gFnSc7	mytologie
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
i	i	k9	i
Robert	Robert	k1gMnSc1	Robert
Graves	Graves	k1gMnSc1	Graves
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
humoristické	humoristický	k2eAgFnSc2d1	humoristická
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
Douglas	Douglas	k1gInSc1	Douglas
Adams	Adams	k1gInSc1	Adams
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
proslulého	proslulý	k2eAgMnSc2d1	proslulý
Stopařova	stopařův	k2eAgMnSc2d1	stopařův
průvodce	průvodce	k1gMnSc2	průvodce
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
humoristické	humoristický	k2eAgFnSc6d1	humoristická
literatuře	literatura	k1gFnSc6	literatura
nelze	lze	k6eNd1	lze
přehlédnout	přehlédnout	k5eAaPmF	přehlédnout
ani	ani	k8xC	ani
Jerome	Jerom	k1gInSc5	Jerom
Klapku	klapka	k1gFnSc4	klapka
Jeromeho	Jerome	k1gMnSc4	Jerome
<g/>
,	,	kIx,	,
Pelhama	Pelham	k1gMnSc4	Pelham
Grenville	Grenville	k1gFnSc2	Grenville
Wodehouse	Wodehouse	k1gFnSc2	Wodehouse
či	či	k8xC	či
Kingsleyho	Kingsley	k1gMnSc2	Kingsley
Amise	Amis	k1gMnSc2	Amis
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
tak	tak	k9	tak
dávno	dávno	k6eAd1	dávno
přibyla	přibýt	k5eAaPmAgFnS	přibýt
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
sestavy	sestava	k1gFnSc2	sestava
Helen	Helena	k1gFnPc2	Helena
Fieldingová	Fieldingový	k2eAgFnSc1d1	Fieldingová
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
Brigitt	Brigitt	k1gInSc1	Brigitt
Jonesovou	Jonesový	k2eAgFnSc4d1	Jonesová
<g/>
.	.	kIx.	.
</s>
<s>
Známí	známý	k2eAgMnPc1d1	známý
tvůrci	tvůrce	k1gMnPc1	tvůrce
komixů	komix	k1gInPc2	komix
jsou	být	k5eAaImIp3nP	být
Alan	alan	k1gInSc4	alan
Moore	Moor	k1gInSc5	Moor
a	a	k8xC	a
Neil	Neil	k1gMnSc1	Neil
Gaiman	Gaiman	k1gMnSc1	Gaiman
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
skotské	skotský	k2eAgMnPc4d1	skotský
autory	autor	k1gMnPc4	autor
patří	patřit	k5eAaImIp3nS	patřit
spisovatel	spisovatel	k1gMnSc1	spisovatel
detektivek	detektivka	k1gFnPc2	detektivka
Arthur	Arthur	k1gMnSc1	Arthur
Conan	Conan	k1gMnSc1	Conan
Doyle	Doyl	k1gMnSc2	Doyl
(	(	kIx(	(
<g/>
tvůrce	tvůrce	k1gMnSc2	tvůrce
Sherlocka	Sherlocka	k1gFnSc1	Sherlocka
Holmese	Holmese	k1gFnSc1	Holmese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
romantická	romantický	k2eAgFnSc1d1	romantická
literatura	literatura	k1gFnSc1	literatura
Sira	sir	k1gMnSc2	sir
Waltera	Walter	k1gMnSc2	Walter
Scotta	Scott	k1gMnSc2	Scott
<g/>
,	,	kIx,	,
dětský	dětský	k2eAgMnSc1d1	dětský
spisovatel	spisovatel	k1gMnSc1	spisovatel
J.	J.	kA	J.
M.	M.	kA	M.
Barrie	Barrie	k1gFnSc1	Barrie
<g/>
,	,	kIx,	,
dobrodružná	dobrodružný	k2eAgFnSc1d1	dobrodružná
literatura	literatura	k1gFnSc1	literatura
Roberta	Robert	k1gMnSc2	Robert
Louise	Louis	k1gMnSc2	Louis
Stevensona	Stevenson	k1gMnSc2	Stevenson
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Robert	Robert	k1gMnSc1	Robert
Burns	Burns	k1gInSc4	Burns
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
poměrně	poměrně	k6eAd1	poměrně
nedávné	dávný	k2eNgNnSc4d1	nedávné
patří	patřit	k5eAaImIp3nS	patřit
Thomas	Thomas	k1gMnSc1	Thomas
Carlyle	Carlyl	k1gInSc5	Carlyl
<g/>
,	,	kIx,	,
Archibald	Archibald	k1gInSc4	Archibald
Joseph	Josepha	k1gFnPc2	Josepha
Cronin	Cronina	k1gFnPc2	Cronina
či	či	k8xC	či
Irvine	Irvin	k1gInSc5	Irvin
Welsh	Welsh	k1gInSc4	Welsh
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
modernističtí	modernistický	k2eAgMnPc1d1	modernistický
a	a	k8xC	a
nacionalističtí	nacionalistický	k2eAgMnPc1d1	nacionalistický
Hugh	Hugh	k1gInSc4	Hugh
MacDiarmid	MacDiarmida	k1gFnPc2	MacDiarmida
a	a	k8xC	a
Neil	Neil	k1gMnSc1	Neil
M.	M.	kA	M.
Gunn	Gunn	k1gMnSc1	Gunn
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
krimi	krimi	k1gFnSc7	krimi
příběhy	příběh	k1gInPc4	příběh
Iana	Ianus	k1gMnSc4	Ianus
Rankina	Rankin	k1gMnSc4	Rankin
nebo	nebo	k8xC	nebo
příběhy	příběh	k1gInPc4	příběh
psychologické	psychologický	k2eAgFnSc2d1	psychologická
horor-komedie	hororomedie	k1gFnSc2	horor-komedie
Iaina	Iain	k1gMnSc2	Iain
Bankse	Banks	k1gMnSc2	Banks
<g/>
.	.	kIx.	.
</s>
<s>
Skotské	skotský	k2eAgNnSc1d1	skotské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Edinburgh	Edinburgha	k1gFnPc2	Edinburgha
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
UNESCO	UNESCO	kA	UNESCO
první	první	k4xOgNnSc4	první
světové	světový	k2eAgNnSc4d1	světové
město	město	k1gNnSc4	město
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velšské	velšský	k2eAgFnSc2d1	velšská
literatury	literatura	k1gFnSc2	literatura
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
básníka	básník	k1gMnSc2	básník
Dzlana	Dzlan	k1gMnSc2	Dzlan
Thomase	Thomas	k1gMnSc2	Thomas
či	či	k8xC	či
metafyzického	metafyzický	k2eAgMnSc2d1	metafyzický
básníka	básník	k1gMnSc2	básník
George	Georg	k1gMnSc2	Georg
Herberta	Herbert	k1gMnSc2	Herbert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
populární	populární	k2eAgFnSc2d1	populární
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
znám	znám	k2eAgMnSc1d1	znám
autor	autor	k1gMnSc1	autor
thrillerů	thriller	k1gInPc2	thriller
Ken	Ken	k1gMnSc1	Ken
Follett	Follett	k1gMnSc1	Follett
<g/>
.	.	kIx.	.
</s>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Watersová	Watersová	k1gFnSc1	Watersová
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
romány	román	k1gInPc4	román
s	s	k7c7	s
lesbickou	lesbický	k2eAgFnSc7d1	lesbická
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
autory	autor	k1gMnPc7	autor
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
Irska	Irsko	k1gNnSc2	Irsko
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
a	a	k8xC	a
pracovali	pracovat	k5eAaImAgMnP	pracovat
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
patří	patřit	k5eAaImIp3nS	patřit
Jonathan	Jonathan	k1gMnSc1	Jonathan
Swift	Swift	k1gMnSc1	Swift
<g/>
,	,	kIx,	,
Oscar	Oscar	k1gMnSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
<g/>
,	,	kIx,	,
Bram	brama	k1gFnPc2	brama
Stoker	Stokra	k1gFnPc2	Stokra
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Conrad	Conrad	k1gInSc1	Conrad
<g/>
,	,	kIx,	,
T.	T.	kA	T.
<g/>
S.	S.	kA	S.
Eliot	Eliot	k1gMnSc1	Eliot
a	a	k8xC	a
Ezra	Ezra	k1gMnSc1	Ezra
Pound	pound	k1gInSc1	pound
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nedávné	dávný	k2eNgMnPc4d1	nedávný
britstké	britstký	k2eAgMnPc4d1	britstký
autory	autor	k1gMnPc4	autor
narozené	narozený	k2eAgMnPc4d1	narozený
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
patří	patřit	k5eAaImIp3nS	patřit
Kazuo	Kazuo	k6eAd1	Kazuo
Ishiguro	Ishigura	k1gFnSc5	Ishigura
a	a	k8xC	a
Sir	sir	k1gMnSc1	sir
Salman	Salman	k1gMnSc1	Salman
Rushdie	Rushdie	k1gFnSc1	Rushdie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Britský	britský	k2eAgInSc4d1	britský
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgInPc1d1	populární
různé	různý	k2eAgInPc1d1	různý
styly	styl	k1gInPc1	styl
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
od	od	k7c2	od
domorodé	domorodý	k2eAgFnSc2d1	domorodá
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
až	až	k9	až
po	po	k7c4	po
heavy	heav	k1gInPc4	heav
metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
skladatele	skladatel	k1gMnPc4	skladatel
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
ze	z	k7c2	z
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgMnPc2	který
sem	sem	k6eAd1	sem
přicházeli	přicházet	k5eAaImAgMnP	přicházet
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
William	William	k1gInSc4	William
Byrd	Byrd	k1gMnSc1	Byrd
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Purcell	Purcell	k1gMnSc1	Purcell
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
Edward	Edward	k1gMnSc1	Edward
Elgar	Elgar	k1gMnSc1	Elgar
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Holst	Holst	k1gMnSc1	Holst
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
Arthur	Arthur	k1gMnSc1	Arthur
Sullivan	Sullivan	k1gMnSc1	Sullivan
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
libretistou	libretista	k1gMnSc7	libretista
Sirem	sir	k1gMnSc7	sir
W.	W.	kA	W.
S.	S.	kA	S.
Gilbertem	Gilbert	k1gMnSc7	Gilbert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
Vaughan	Vaughany	k1gInPc2	Vaughany
Williams	Williams	k1gInSc1	Williams
a	a	k8xC	a
Benjamin	Benjamin	k1gMnSc1	Benjamin
Britten	Britten	k2eAgMnSc1d1	Britten
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
moderní	moderní	k2eAgFnSc2d1	moderní
britské	britský	k2eAgFnSc2d1	britská
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Georg	Georg	k1gMnSc1	Georg
Friedrich	Friedrich	k1gMnSc1	Friedrich
Händel	Händlo	k1gNnPc2	Händlo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
naturalizovaným	naturalizovaný	k2eAgMnSc7d1	naturalizovaný
britským	britský	k2eAgMnSc7d1	britský
občanem	občan	k1gMnSc7	občan
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
britskou	britský	k2eAgFnSc4d1	britská
korunovační	korunovační	k2eAgFnSc4d1	korunovační
hymnu	hymna	k1gFnSc4	hymna
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Mesiáš	Mesiáš	k1gMnSc1	Mesiáš
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
psána	psát	k5eAaImNgFnS	psát
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
je	být	k5eAaImIp3nS	být
také	také	k9	také
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
světově	světově	k6eAd1	světově
proslulé	proslulý	k2eAgInPc4d1	proslulý
symfonické	symfonický	k2eAgInPc4d1	symfonický
orchestry	orchestr	k1gInPc4	orchestr
a	a	k8xC	a
pěvecké	pěvecký	k2eAgInPc4d1	pěvecký
sbory	sbor	k1gInPc4	sbor
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
BBC	BBC	kA	BBC
Symphony	Symphona	k1gFnPc1	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
a	a	k8xC	a
Symphony	Symphona	k1gFnSc2	Symphona
Chorus	chorus	k1gInSc1	chorus
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
dirigenty	dirigent	k1gMnPc4	dirigent
patří	patřit	k5eAaImIp3nS	patřit
Sir	sir	k1gMnSc1	sir
Simon	Simon	k1gMnSc1	Simon
Rattle	Rattle	k1gFnSc1	Rattle
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Barbirolli	Barbirolle	k1gFnSc4	Barbirolle
a	a	k8xC	a
Sir	sir	k1gMnSc1	sir
Malcolm	Malcolm	k1gMnSc1	Malcolm
Sargent	Sargent	k1gMnSc1	Sargent
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
skladatele	skladatel	k1gMnPc4	skladatel
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
patří	patřit	k5eAaImIp3nS	patřit
John	John	k1gMnSc1	John
Barry	Barra	k1gFnSc2	Barra
<g/>
,	,	kIx,	,
Clint	Clint	k1gMnSc1	Clint
Mansell	Mansell	k1gMnSc1	Mansell
<g/>
,	,	kIx,	,
Mike	Mike	k1gFnSc1	Mike
Oldfield	Oldfield	k1gMnSc1	Oldfield
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Powell	Powell	k1gMnSc1	Powell
<g/>
,	,	kIx,	,
Craig	Craig	k1gMnSc1	Craig
Armstrong	Armstrong	k1gMnSc1	Armstrong
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Murphy	Murpha	k1gMnSc2	Murpha
<g/>
,	,	kIx,	,
Monty	Monta	k1gMnSc2	Monta
Norman	Norman	k1gMnSc1	Norman
a	a	k8xC	a
Harry	Harra	k1gFnPc1	Harra
Gregson-Williams	Gregson-Williamsa	k1gFnPc2	Gregson-Williamsa
<g/>
.	.	kIx.	.
</s>
<s>
Andrew	Andrew	k?	Andrew
Lloyd	Lloyd	k1gMnSc1	Lloyd
Webber	Webber	k1gMnSc1	Webber
je	být	k5eAaImIp3nS	být
produktivní	produktivní	k2eAgMnSc1d1	produktivní
muzikálový	muzikálový	k2eAgMnSc1d1	muzikálový
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dominují	dominovat	k5eAaImIp3nP	dominovat
londýnským	londýnský	k2eAgNnPc3d1	Londýnské
divadlům	divadlo	k1gNnPc3	divadlo
ve	v	k7c4	v
West	West	k1gInSc4	West
Endu	Endus	k1gInSc2	Endus
a	a	k8xC	a
slavila	slavit	k5eAaImAgFnS	slavit
i	i	k9	i
úspěch	úspěch	k1gInSc4	úspěch
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Beatles	Beatles	k1gFnPc1	Beatles
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
prodáno	prodat	k5eAaPmNgNnS	prodat
přes	přes	k7c4	přes
miliardu	miliarda	k4xCgFnSc4	miliarda
nahrávek	nahrávka	k1gFnPc2	nahrávka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nejprodávanější	prodávaný	k2eAgMnSc1d3	nejprodávanější
a	a	k8xC	a
nejvlivnější	vlivný	k2eAgFnSc7d3	nejvlivnější
kapelou	kapela	k1gFnSc7	kapela
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
a	a	k8xC	a
vlivné	vlivný	k2eAgMnPc4d1	vlivný
britské	britský	k2eAgMnPc4d1	britský
umělce	umělec	k1gMnPc4	umělec
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
50	[number]	k4	50
let	léto	k1gNnPc2	léto
patří	patřit	k5eAaImIp3nS	patřit
The	The	k1gMnPc3	The
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
,	,	kIx,	,
Led	led	k1gInSc1	led
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
,	,	kIx,	,
Pink	pink	k2eAgInSc1d1	pink
Floyd	Floyd	k1gInSc1	Floyd
<g/>
,	,	kIx,	,
Queen	Queen	k1gInSc1	Queen
<g/>
,	,	kIx,	,
Bee	Bee	k1gFnSc1	Bee
Gees	Geesa	k1gFnPc2	Geesa
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
spíše	spíše	k9	spíše
Australané	Australan	k1gMnPc1	Australan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
,	,	kIx,	,
Dire	Dire	k1gFnSc1	Dire
Straits	Straits	k1gInSc1	Straits
<g/>
,	,	kIx,	,
Def	Def	k1gMnSc1	Def
Leppard	Leppard	k1gMnSc1	Leppard
<g/>
,	,	kIx,	,
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
a	a	k8xC	a
Depeche	Depeche	k1gFnSc1	Depeche
Mode	modus	k1gInSc5	modus
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
rekordně	rekordně	k6eAd1	rekordně
prodáno	prodat	k5eAaPmNgNnS	prodat
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
a	a	k8xC	a
více	hodně	k6eAd2	hodně
nahrávek	nahrávka	k1gFnPc2	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Brit	Brit	k1gMnSc1	Brit
Awards	Awardsa	k1gFnPc2	Awardsa
jsou	být	k5eAaImIp3nP	být
výroční	výroční	k2eAgFnPc1d1	výroční
hudební	hudební	k2eAgFnPc1d1	hudební
ceny	cena	k1gFnPc1	cena
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
BPI	BPI	kA	BPI
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
některé	některý	k3yIgMnPc4	některý
z	z	k7c2	z
oceněných	oceněný	k2eAgFnPc2d1	oceněná
z	z	k7c2	z
Británie	Británie	k1gFnSc2	Británie
za	za	k7c4	za
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
přínos	přínos	k1gInSc4	přínos
hudbě	hudba	k1gFnSc3	hudba
patří	patřit	k5eAaImIp3nS	patřit
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc1	Bowie
<g/>
,	,	kIx,	,
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc4	Clapton
<g/>
,	,	kIx,	,
Rod	rod	k1gInSc4	rod
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
Joe	Joe	k1gMnSc1	Joe
Cocker	Cocker	k1gMnSc1	Cocker
<g/>
,	,	kIx,	,
Sting	Sting	k1gMnSc1	Sting
a	a	k8xC	a
The	The	k1gMnSc1	The
Police	police	k1gFnSc2	police
<g/>
,	,	kIx,	,
Cliff	Cliff	k1gMnSc1	Cliff
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Michael	Michael	k1gMnSc1	Michael
a	a	k8xC	a
Mike	Mike	k1gFnSc1	Mike
Oldfield	Oldfielda	k1gFnPc2	Oldfielda
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníky	průkopník	k1gMnPc7	průkopník
žánru	žánr	k1gInSc2	žánr
heavy	heav	k1gMnPc7	heav
metal	metal	k1gInSc4	metal
byli	být	k5eAaImAgMnP	být
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
a	a	k8xC	a
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInSc4d1	další
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
patří	patřit	k5eAaImIp3nP	patřit
Coldplay	Coldplaa	k1gFnPc1	Coldplaa
<g/>
,	,	kIx,	,
Radiohead	Radiohead	k1gInSc1	Radiohead
<g/>
,	,	kIx,	,
Oasis	Oasis	k1gFnSc1	Oasis
<g/>
,	,	kIx,	,
Spice	Spice	k1gFnSc1	Spice
Girls	girl	k1gFnPc1	girl
<g/>
,	,	kIx,	,
Robbie	Robbie	k1gFnPc1	Robbie
Williams	Williams	k1gInSc1	Williams
<g/>
,	,	kIx,	,
Amy	Amy	k1gFnSc1	Amy
Winehouse	Winehouse	k1gFnSc1	Winehouse
a	a	k8xC	a
Adele	Adele	k1gFnSc1	Adele
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Skotů	Skot	k1gMnPc2	Skot
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
prosadila	prosadit	k5eAaPmAgFnS	prosadit
Annie	Annie	k1gFnSc1	Annie
Lennoxová	Lennoxový	k2eAgFnSc1d1	Lennoxová
<g/>
,	,	kIx,	,
z	z	k7c2	z
Velšanů	Velšan	k1gMnPc2	Velšan
John	John	k1gMnSc1	John
Cale	Cal	k1gFnSc2	Cal
(	(	kIx(	(
<g/>
svého	své	k1gNnSc2	své
času	čas	k1gInSc2	čas
člen	člen	k1gMnSc1	člen
The	The	k1gMnSc1	The
Velvet	Velvet	k1gMnSc1	Velvet
Underground	underground	k1gInSc1	underground
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
Bonnie	Bonnie	k1gFnSc1	Bonnie
Tyler	Tyler	k1gMnSc1	Tyler
a	a	k8xC	a
Manic	Manic	k1gMnSc1	Manic
Street	Streeta	k1gFnPc2	Streeta
Preachers	Preachersa	k1gFnPc2	Preachersa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
anglickým	anglický	k2eAgMnPc3d1	anglický
malířům	malíř	k1gMnPc3	malíř
patří	patřit	k5eAaImIp3nS	patřit
romantický	romantický	k2eAgMnSc1d1	romantický
krajinář	krajinář	k1gMnSc1	krajinář
William	William	k1gInSc4	William
Turner	turner	k1gMnSc1	turner
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Hogarth	Hogarth	k1gMnSc1	Hogarth
založil	založit	k5eAaPmAgMnS	založit
žánr	žánr	k1gInSc4	žánr
karikatury	karikatura	k1gFnSc2	karikatura
<g/>
.	.	kIx.	.
</s>
<s>
Nejvlivnější	vlivný	k2eAgFnSc7d3	nejvlivnější
výtvarnou	výtvarný	k2eAgFnSc7d1	výtvarná
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
anglických	anglický	k2eAgFnPc6d1	anglická
dějinách	dějiny	k1gFnPc6	dějiny
byli	být	k5eAaImAgMnP	být
tzv.	tzv.	kA	tzv.
prerafaelité	prerafaelita	k1gMnPc1	prerafaelita
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
členům	člen	k1gMnPc3	člen
tohoto	tento	k3xDgInSc2	tento
bratrstva	bratrstvo	k1gNnSc2	bratrstvo
patřili	patřit	k5eAaImAgMnP	patřit
Dante	Dante	k1gMnSc1	Dante
Gabriel	Gabriel	k1gMnSc1	Gabriel
Rossetti	Rossett	k2eAgMnPc1d1	Rossett
nebo	nebo	k8xC	nebo
John	John	k1gMnSc1	John
Everett	Everett	k2eAgInSc4d1	Everett
Millais	Millais	k1gInSc4	Millais
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
bratrstvem	bratrstvo	k1gNnSc7	bratrstvo
byl	být	k5eAaImAgInS	být
úzce	úzko	k6eAd1	úzko
spjat	spjat	k2eAgInSc1d1	spjat
dyzajnér	dyzajnér	k1gInSc1	dyzajnér
William	William	k1gInSc4	William
Morris	Morris	k1gFnSc2	Morris
<g/>
.	.	kIx.	.
</s>
<s>
Neslavnějším	slavný	k2eNgMnSc7d2	neslavnější
anglickým	anglický	k2eAgMnSc7d1	anglický
sochařem	sochař	k1gMnSc7	sochař
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
Henry	Henry	k1gMnSc5	Henry
Moore	Moor	k1gMnSc5	Moor
<g/>
.	.	kIx.	.
</s>
<s>
Nejproslulejším	proslulý	k2eAgMnSc7d3	nejproslulejší
anglickým	anglický	k2eAgMnSc7d1	anglický
architektem	architekt	k1gMnSc7	architekt
je	být	k5eAaImIp3nS	být
Christopher	Christophra	k1gFnPc2	Christophra
Wren	Wren	k1gMnSc1	Wren
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
stavitel	stavitel	k1gMnSc1	stavitel
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Pavla	Pavel	k1gMnSc2	Pavel
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
z	z	k7c2	z
moderních	moderní	k2eAgMnPc2d1	moderní
architektů	architekt	k1gMnPc2	architekt
pak	pak	k6eAd1	pak
Norman	Norman	k1gMnSc1	Norman
Foster	Foster	k1gMnSc1	Foster
<g/>
.	.	kIx.	.
</s>
<s>
Skotský	skotský	k2eAgMnSc1d1	skotský
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
dyzajnér	dyzajnér	k1gMnSc1	dyzajnér
Charles	Charles	k1gMnSc1	Charles
Rennie	Rennie	k1gFnSc2	Rennie
Mackintosh	Mackintosh	k1gInSc1	Mackintosh
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
představitelem	představitel	k1gMnSc7	představitel
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Adam	Adam	k1gMnSc1	Adam
neoklasicismu	neoklasicismus	k1gInSc2	neoklasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Proslulým	proslulý	k2eAgMnSc7d1	proslulý
stavitelem	stavitel	k1gMnSc7	stavitel
kanálů	kanál	k1gInPc2	kanál
byl	být	k5eAaImAgMnS	být
Skot	Skot	k1gMnSc1	Skot
Thomas	Thomas	k1gMnSc1	Thomas
Telford	Telford	k1gMnSc1	Telford
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
anglickým	anglický	k2eAgMnSc7d1	anglický
filmovým	filmový	k2eAgMnSc7d1	filmový
režisérem	režisér	k1gMnSc7	režisér
je	být	k5eAaImIp3nS	být
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
Oscary	Oscar	k1gInPc4	Oscar
má	mít	k5eAaImIp3nS	mít
režisér	režisér	k1gMnSc1	režisér
Richard	Richard	k1gMnSc1	Richard
Attenborough	Attenborough	k1gMnSc1	Attenborough
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgMnS	odnést
Anthony	Anthona	k1gFnPc4	Anthona
Minghella	Minghello	k1gNnSc2	Minghello
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
<g/>
,	,	kIx,	,
Ken	Ken	k1gMnSc1	Ken
Russell	Russell	k1gMnSc1	Russell
a	a	k8xC	a
Tony	Tony	k1gMnSc1	Tony
Richardson	Richardson	k1gMnSc1	Richardson
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
britské	britský	k2eAgFnSc2d1	britská
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
byl	být	k5eAaImAgMnS	být
Lindsay	Lindsaa	k1gFnPc4	Lindsaa
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
populárních	populární	k2eAgInPc2d1	populární
snímků	snímek	k1gInPc2	snímek
natočil	natočit	k5eAaBmAgInS	natočit
Ridley	Ridlea	k1gFnPc4	Ridlea
Scott	Scott	k1gMnSc1	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Terry	Terra	k1gMnSc2	Terra
Gilliam	Gilliam	k1gInSc1	Gilliam
vždy	vždy	k6eAd1	vždy
nalezl	naleznout	k5eAaPmAgInS	naleznout
zcela	zcela	k6eAd1	zcela
osobitou	osobitý	k2eAgFnSc4d1	osobitá
poetiku	poetika	k1gFnSc4	poetika
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Curtis	Curtis	k1gFnSc2	Curtis
je	být	k5eAaImIp3nS	být
králem	král	k1gMnSc7	král
romantických	romantický	k2eAgFnPc2d1	romantická
komedií	komedie	k1gFnPc2	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Ken	Ken	k?	Ken
Loach	Loach	k1gInSc1	Loach
točil	točit	k5eAaImAgInS	točit
především	především	k9	především
sociální	sociální	k2eAgNnPc1d1	sociální
dramata	drama	k1gNnPc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejúspěšnějším	úspěšný	k2eAgMnPc3d3	nejúspěšnější
režisérům	režisér	k1gMnPc3	režisér
současnosti	současnost	k1gFnSc2	současnost
patří	patřit	k5eAaImIp3nS	patřit
Christopher	Christophra	k1gFnPc2	Christophra
Nolan	Nolan	k1gMnSc1	Nolan
či	či	k8xC	či
Danny	Danen	k2eAgFnPc4d1	Danna
Boyle	Boyle	k1gFnPc4	Boyle
<g/>
.	.	kIx.	.
</s>
<s>
Guy	Guy	k?	Guy
Ritchie	Ritchie	k1gFnSc2	Ritchie
proslul	proslout	k5eAaPmAgMnS	proslout
novým	nový	k2eAgMnSc7d1	nový
<g/>
,	,	kIx,	,
dosti	dosti	k6eAd1	dosti
hollywoodským	hollywoodský	k2eAgNnSc7d1	hollywoodské
pojetím	pojetí	k1gNnSc7	pojetí
Sherlocka	Sherlocko	k1gNnSc2	Sherlocko
Holmese	Holmese	k1gFnSc2	Holmese
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
hercem	herec	k1gMnSc7	herec
němé	němý	k2eAgFnSc2d1	němá
éry	éra	k1gFnSc2	éra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplina	k1gFnPc2	Chaplina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
ikonám	ikona	k1gFnPc3	ikona
světového	světový	k2eAgNnSc2d1	světové
filmového	filmový	k2eAgNnSc2d1	filmové
umění	umění	k1gNnSc2	umění
patří	patřit	k5eAaImIp3nS	patřit
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Taylorová	Taylorová	k1gFnSc1	Taylorová
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Caine	Cain	k1gInSc5	Cain
<g/>
,	,	kIx,	,
Laurence	Laurenka	k1gFnSc3	Laurenka
Olivier	Olivira	k1gFnPc2	Olivira
<g/>
,	,	kIx,	,
Cary	car	k1gMnPc4	car
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Ustinov	Ustinov	k1gInSc1	Ustinov
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc4	tři
Oscary	Oscar	k1gMnPc4	Oscar
má	mít	k5eAaImIp3nS	mít
Daniel	Daniel	k1gMnSc1	Daniel
Day-Lewis	Day-Lewis	k1gFnSc2	Day-Lewis
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
inspektora	inspektor	k1gMnSc2	inspektor
Clouseaua	Clouseauus	k1gMnSc2	Clouseauus
proslavila	proslavit	k5eAaPmAgFnS	proslavit
Petera	Peter	k1gMnSc4	Peter
Sellerse	Sellerse	k1gFnSc1	Sellerse
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
v	v	k7c6	v
Pánu	pán	k1gMnSc6	pán
prstenů	prsten	k1gInPc2	prsten
udělala	udělat	k5eAaPmAgFnS	udělat
globální	globální	k2eAgFnSc4d1	globální
celebritu	celebrita	k1gFnSc4	celebrita
z	z	k7c2	z
Christophera	Christopher	k1gMnSc4	Christopher
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
Orlando	Orlanda	k1gFnSc5	Orlanda
Blooma	Bloom	k1gMnSc4	Bloom
či	či	k8xC	či
Iana	Ianus	k1gMnSc4	Ianus
McKellena	McKellena	k1gFnSc1	McKellena
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
o	o	k7c4	o
Harry	Harra	k1gFnPc4	Harra
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
proslavila	proslavit	k5eAaPmAgFnS	proslavit
Emmu	Emma	k1gFnSc4	Emma
Watsonovou	Watsonový	k2eAgFnSc4d1	Watsonová
<g/>
,	,	kIx,	,
Daniela	Daniel	k1gMnSc2	Daniel
Radcliffa	Radcliff	k1gMnSc2	Radcliff
<g/>
,	,	kIx,	,
Gary	Gara	k1gMnSc2	Gara
Oldmana	Oldman	k1gMnSc2	Oldman
<g/>
,	,	kIx,	,
Richarda	Richard	k1gMnSc2	Richard
Griffithse	Griffiths	k1gMnSc2	Griffiths
a	a	k8xC	a
Alana	Alan	k1gMnSc2	Alan
Rickmana	Rickman	k1gMnSc2	Rickman
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
Matrix	Matrix	k1gInSc1	Matrix
Hugo	Hugo	k1gMnSc1	Hugo
Weavinga	Weavinga	k1gFnSc1	Weavinga
<g/>
,	,	kIx,	,
velkofilm	velkofilm	k1gInSc1	velkofilm
Titanic	Titanic	k1gInSc1	Titanic
Kate	kat	k1gInSc5	kat
Winsletovou	Winsletový	k2eAgFnSc7d1	Winsletová
<g/>
,	,	kIx,	,
komediální	komediální	k2eAgFnSc7d1	komediální
cyklus	cyklus	k1gInSc4	cyklus
o	o	k7c6	o
Mr	Mr	k1gFnSc6	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Beanovi	Bean	k1gMnSc6	Bean
Rowana	Rowana	k1gFnSc1	Rowana
Atkinsona	Atkinsona	k1gFnSc1	Atkinsona
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
o	o	k7c6	o
Jamesi	Jamese	k1gFnSc6	Jamese
Bondovi	Bondův	k2eAgMnPc1d1	Bondův
především	především	k6eAd1	především
Daniela	Daniel	k1gMnSc4	Daniel
Craiga	Craig	k1gMnSc4	Craig
<g/>
,	,	kIx,	,
Judi	Jude	k1gFnSc4	Jude
Denchovou	Denchová	k1gFnSc4	Denchová
a	a	k8xC	a
Rogera	Rogera	k1gFnSc1	Rogera
Moorea	Moorea	k1gFnSc1	Moorea
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
Doktor	doktor	k1gMnSc1	doktor
House	house	k1gNnSc1	house
Hugh	Hugh	k1gMnSc1	Hugh
Laurieho	Laurie	k1gMnSc2	Laurie
<g/>
,	,	kIx,	,
sága	sága	k1gFnSc1	sága
Stmívání	stmívání	k1gNnSc1	stmívání
Roberta	Robert	k1gMnSc2	Robert
Pattinsona	Pattinson	k1gMnSc2	Pattinson
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
legendám	legenda	k1gFnPc3	legenda
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Bob	Bob	k1gMnSc1	Bob
Hoskins	Hoskins	k1gInSc1	Hoskins
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
Pleasence	Pleasence	k1gFnSc2	Pleasence
<g/>
,	,	kIx,	,
Malcolm	Malcolm	k1gMnSc1	Malcolm
McDowell	McDowell	k1gMnSc1	McDowell
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
Bale	bal	k1gInSc5	bal
<g/>
,	,	kIx,	,
Ben	Ben	k1gInSc4	Ben
Kingsley	Kingslea	k1gFnSc2	Kingslea
<g/>
,	,	kIx,	,
Jeremy	Jerema	k1gFnPc1	Jerema
Irons	Irons	k1gInSc1	Irons
<g/>
,	,	kIx,	,
Alec	Alec	k1gInSc1	Alec
Guinness	Guinness	k1gInSc1	Guinness
<g/>
,	,	kIx,	,
Colin	Colin	k2eAgInSc1d1	Colin
Firth	Firth	k1gInSc1	Firth
<g/>
,	,	kIx,	,
Emma	Emma	k1gFnSc1	Emma
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
<g/>
,	,	kIx,	,
Helen	Helena	k1gFnPc2	Helena
Mirrenová	Mirrenový	k2eAgFnSc1d1	Mirrenová
<g/>
,	,	kIx,	,
Hugh	Hugh	k1gMnSc1	Hugh
Grant	grant	k1gInSc1	grant
či	či	k8xC	či
David	David	k1gMnSc1	David
Niven	Nivna	k1gFnPc2	Nivna
<g/>
.	.	kIx.	.
</s>
<s>
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
Bissetová	Bissetový	k2eAgFnSc1d1	Bissetová
zůstane	zůstat	k5eAaPmIp3nS	zůstat
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
Středoevropanů	Středoevropan	k1gMnPc2	Středoevropan
už	už	k6eAd1	už
asi	asi	k9	asi
navždy	navždy	k6eAd1	navždy
především	především	k9	především
kráskou	kráska	k1gFnSc7	kráska
z	z	k7c2	z
belmondovské	belmondovský	k2eAgFnSc2d1	belmondovský
klasiky	klasika	k1gFnSc2	klasika
Muž	muž	k1gMnSc1	muž
z	z	k7c2	z
Acapulca	Acapulc	k1gInSc2	Acapulc
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
proslulé	proslulý	k2eAgFnSc3d1	proslulá
komické	komický	k2eAgFnSc3d1	komická
skupině	skupina	k1gFnSc3	skupina
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
patřili	patřit	k5eAaImAgMnP	patřit
herci	herec	k1gMnSc6	herec
John	John	k1gMnSc1	John
Cleese	Cleese	k1gFnSc2	Cleese
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Palin	Palin	k1gMnSc1	Palin
<g/>
,	,	kIx,	,
Graham	Graham	k1gMnSc1	Graham
Chapman	Chapman	k1gMnSc1	Chapman
či	či	k8xC	či
Eric	Eric	k1gFnSc1	Eric
Idle	Idle	k1gFnSc1	Idle
<g/>
.	.	kIx.	.
</s>
<s>
Megahvězdou	Megahvězda	k1gFnSc7	Megahvězda
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
Keira	Keira	k1gFnSc1	Keira
Knightleyová	Knightleyová	k1gFnSc1	Knightleyová
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
Spiderman	Spiderman	k1gMnSc1	Spiderman
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Andrew	Andrew	k1gMnPc4	Andrew
Garfield	Garfield	k1gInSc4	Garfield
<g/>
,	,	kIx,	,
v	v	k7c6	v
sérii	série	k1gFnSc6	série
Mission	Mission	k1gInSc1	Mission
<g/>
:	:	kIx,	:
Impossible	Impossible	k1gFnSc1	Impossible
a	a	k8xC	a
Star	Star	kA	Star
Trek	Trek	k1gMnSc1	Trek
Simon	Simon	k1gMnSc1	Simon
Pegg	Pegg	k1gMnSc1	Pegg
<g/>
.	.	kIx.	.
</s>
<s>
Brutální	brutální	k2eAgFnSc1d1	brutální
komika	komika	k1gFnSc1	komika
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
Sachu	Sacha	k1gMnSc4	Sacha
Barona	baron	k1gMnSc4	baron
Cohena	Cohen	k1gMnSc4	Cohen
<g/>
.	.	kIx.	.
</s>
<s>
Skotem	skot	k1gInSc7	skot
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
představitel	představitel	k1gMnSc1	představitel
Jamese	Jamese	k1gFnSc2	Jamese
Bonda	Bonda	k1gMnSc1	Bonda
a	a	k8xC	a
herecká	herecký	k2eAgFnSc1d1	herecká
legenda	legenda	k1gFnSc1	legenda
Sean	Seana	k1gFnPc2	Seana
Connery	Conner	k1gMnPc4	Conner
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
o	o	k7c4	o
Harry	Harra	k1gFnPc4	Harra
Potterovi	Potter	k1gMnSc3	Potter
proslavila	proslavit	k5eAaPmAgFnS	proslavit
Robbie	Robbie	k1gFnSc1	Robbie
Coltranea	Coltranea	k1gFnSc1	Coltranea
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
Pán	pán	k1gMnSc1	pán
prstenů	prsten	k1gInPc2	prsten
Billy	Bill	k1gMnPc7	Bill
Boyda	Boydo	k1gNnSc2	Boydo
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
Doctor	Doctor	k1gInSc1	Doctor
Who	Who	k1gFnPc2	Who
Karen	Karna	k1gFnPc2	Karna
Gillanovou	Gillanový	k2eAgFnSc7d1	Gillanový
a	a	k8xC	a
Petera	Peter	k1gMnSc2	Peter
Capaldiho	Capaldi	k1gMnSc2	Capaldi
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Velšanů	Velšan	k1gMnPc2	Velšan
proslul	proslout	k5eAaPmAgMnS	proslout
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Terry	Terra	k1gFnSc2	Terra
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
komediální	komediální	k2eAgFnSc2d1	komediální
skupiny	skupina	k1gFnSc2	skupina
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějšími	slavný	k2eAgMnPc7d3	nejslavnější
velšskými	velšský	k2eAgMnPc7d1	velšský
herci	herec	k1gMnPc7	herec
jsou	být	k5eAaImIp3nP	být
Catherine	Catherin	k1gInSc5	Catherin
Zeta-Jonesová	Zeta-Jonesový	k2eAgFnSc1d1	Zeta-Jonesová
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Burton	Burton	k1gInSc1	Burton
a	a	k8xC	a
Anthony	Anthona	k1gFnPc1	Anthona
Hopkins	Hopkinsa	k1gFnPc2	Hopkinsa
<g/>
.	.	kIx.	.
</s>
<s>
Ray	Ray	k?	Ray
Milland	Milland	k1gInSc1	Milland
získal	získat	k5eAaPmAgMnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Velkofilm	velkofilm	k1gInSc1	velkofilm
Titanic	Titanic	k1gInSc1	Titanic
učinil	učinit	k5eAaImAgInS	učinit
všeobecně	všeobecně	k6eAd1	všeobecně
známým	známý	k1gMnSc7	známý
Ioana	Ioan	k1gInSc2	Ioan
Gruffudda	Gruffudda	k1gFnSc1	Gruffudda
<g/>
,	,	kIx,	,
sága	sága	k1gFnSc1	sága
Twilight	Twilighta	k1gFnPc2	Twilighta
Michaela	Michael	k1gMnSc2	Michael
Sheena	Sheen	k1gMnSc2	Sheen
<g/>
,	,	kIx,	,
romantická	romantický	k2eAgFnSc1d1	romantická
klasika	klasika	k1gFnSc1	klasika
Notting	Notting	k1gInSc1	Notting
Hill	Hill	k1gMnSc1	Hill
Rhyse	Rhyse	k1gFnSc1	Rhyse
Ifanse	Ifanse	k1gFnSc1	Ifanse
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
o	o	k7c4	o
Jamesi	Jamese	k1gFnSc4	Jamese
Bondovi	Bonda	k1gMnSc6	Bonda
Desmonda	Desmonda	k1gFnSc1	Desmonda
Llewelyna	Llewelyna	k1gFnSc1	Llewelyna
(	(	kIx(	(
<g/>
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
výstrojního	výstrojní	k2eAgMnSc4d1	výstrojní
důstojník	důstojník	k1gMnSc1	důstojník
Q	Q	kA	Q
v	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
bondovkách	bondovkách	k?	bondovkách
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
série	série	k1gFnPc4	série
Piráti	pirát	k1gMnPc1	pirát
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
Jonathana	Jonathan	k1gMnSc2	Jonathan
Pryce	Pryce	k1gMnSc2	Pryce
<g/>
,	,	kIx,	,
seriály	seriál	k1gInPc1	seriál
Misfits	Misfits	k1gInSc1	Misfits
a	a	k8xC	a
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
trůny	trůn	k1gInPc4	trůn
Iwana	Iwan	k1gMnSc2	Iwan
Rheona	Rheon	k1gMnSc2	Rheon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
modelingu	modeling	k1gInSc6	modeling
proslula	proslout	k5eAaPmAgFnS	proslout
Naomi	Nao	k1gFnPc7	Nao
Campbellová	Campbellová	k1gFnSc1	Campbellová
či	či	k8xC	či
Kate	kat	k1gInSc5	kat
Mossová	Mossový	k2eAgFnSc1d1	Mossová
<g/>
.	.	kIx.	.
</s>
<s>
Legendou	legenda	k1gFnSc7	legenda
dokumentárních	dokumentární	k2eAgInPc2d1	dokumentární
pořadů	pořad	k1gInPc2	pořad
BBC	BBC	kA	BBC
je	být	k5eAaImIp3nS	být
zoolog	zoolog	k1gMnSc1	zoolog
David	David	k1gMnSc1	David
Attenborough	Attenborough	k1gMnSc1	Attenborough
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
televizní	televizní	k2eAgFnSc7d1	televizní
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
překročila	překročit	k5eAaPmAgFnS	překročit
hranice	hranice	k1gFnPc4	hranice
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kuchař	kuchař	k1gMnSc1	kuchař
Jamie	Jamie	k1gFnSc2	Jamie
Oliver	Oliver	k1gMnSc1	Oliver
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
motoristického	motoristický	k2eAgInSc2d1	motoristický
pořadu	pořad	k1gInSc2	pořad
Jeremy	Jerema	k1gFnSc2	Jerema
Clarkson	Clarkson	k1gMnSc1	Clarkson
či	či	k8xC	či
porotce	porotce	k1gMnSc1	porotce
Simon	Simon	k1gMnSc1	Simon
Cowell	Cowell	k1gMnSc1	Cowell
z	z	k7c2	z
formátů	formát	k1gInPc2	formát
typu	typ	k1gInSc2	typ
Pop	pop	k1gInSc1	pop
Idol	idol	k1gInSc1	idol
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
moderní	moderní	k2eAgFnSc2d1	moderní
fyziky	fyzika	k1gFnSc2	fyzika
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
Isaac	Isaac	k1gFnSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc1	biologie
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Studiemi	studie	k1gFnPc7	studie
elektromagnetismu	elektromagnetismus	k1gInSc2	elektromagnetismus
otevřel	otevřít	k5eAaPmAgMnS	otevřít
před	před	k7c7	před
moderní	moderní	k2eAgFnSc7d1	moderní
vědou	věda	k1gFnSc7	věda
široké	široký	k2eAgFnSc2d1	široká
obzory	obzor	k1gInPc4	obzor
Michael	Michael	k1gMnSc1	Michael
Faraday	Faradaa	k1gFnSc2	Faradaa
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgInPc4d1	rozhodující
pokroky	pokrok	k1gInPc4	pokrok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
informatiky	informatika	k1gFnSc2	informatika
učinil	učinit	k5eAaPmAgMnS	učinit
Alan	Alan	k1gMnSc1	Alan
Turing	Turing	k1gInSc4	Turing
<g/>
.	.	kIx.	.
</s>
<s>
Tim	Tim	k?	Tim
Berners-Lee	Berners-Lee	k1gNnSc1	Berners-Lee
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
systém	systém	k1gInSc4	systém
World	Worlda	k1gFnPc2	Worlda
Wide	Wid	k1gInSc2	Wid
Web	web	k1gInSc4	web
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Prescott	Prescott	k1gMnSc1	Prescott
Joule	joule	k1gInSc4	joule
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
vztah	vztah	k1gInSc4	vztah
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
vědcem	vědec	k1gMnSc7	vědec
současnosti	současnost	k1gFnSc2	současnost
je	být	k5eAaImIp3nS	být
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
<g/>
.	.	kIx.	.
</s>
<s>
Otcem	otec	k1gMnSc7	otec
imunologie	imunologie	k1gFnSc2	imunologie
a	a	k8xC	a
objevitelem	objevitel	k1gMnSc7	objevitel
první	první	k4xOgFnSc2	první
vakcíny	vakcína	k1gFnSc2	vakcína
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
vakcíny	vakcína	k1gFnPc1	vakcína
proti	proti	k7c3	proti
pravým	pravý	k2eAgFnPc3d1	pravá
neštovicím	neštovice	k1gFnPc3	neštovice
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Edward	Edward	k1gMnSc1	Edward
Jenner	Jenner	k1gMnSc1	Jenner
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Dalton	Dalton	k1gInSc4	Dalton
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
moderní	moderní	k2eAgFnSc4d1	moderní
atomovou	atomový	k2eAgFnSc4d1	atomová
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Babbage	Babbag	k1gFnSc2	Babbag
a	a	k8xC	a
Ada	Ada	kA	Ada
Lovelaceová	Lovelaceová	k1gFnSc1	Lovelaceová
jsou	být	k5eAaImIp3nP	být
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
vylézců	vylézce	k1gMnPc2	vylézce
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
vlivné	vlivný	k2eAgFnSc2d1	vlivná
teorie	teorie	k1gFnSc2	teorie
"	"	kIx"	"
<g/>
sobeckého	sobecký	k2eAgInSc2d1	sobecký
genu	gen	k1gInSc2	gen
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkins	k1gInSc1	Dawkins
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
John	John	k1gMnSc1	John
Thomson	Thomson	k1gMnSc1	Thomson
objevil	objevit	k5eAaPmAgMnS	objevit
elektron	elektron	k1gInSc4	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Astronom	astronom	k1gMnSc1	astronom
Edmond	Edmond	k1gMnSc1	Edmond
Halley	Halley	k1gMnSc1	Halley
dal	dát	k5eAaPmAgMnS	dát
jméno	jméno	k1gNnSc4	jméno
nejznámější	známý	k2eAgMnSc1d3	nejznámější
kometě	kometa	k1gFnSc3	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Hooke	Hooke	k1gNnSc2	Hooke
formuloval	formulovat	k5eAaImAgMnS	formulovat
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
přímé	přímý	k2eAgFnSc6d1	přímá
úměrnosti	úměrnost	k1gFnSc6	úměrnost
velikosti	velikost	k1gFnSc2	velikost
deformace	deformace	k1gFnSc2	deformace
a	a	k8xC	a
napětí	napětí	k1gNnSc2	napětí
v	v	k7c6	v
deformovaném	deformovaný	k2eAgNnSc6d1	deformované
tělese	těleso	k1gNnSc6	těleso
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Harvey	Harvea	k1gFnSc2	Harvea
objevil	objevit	k5eAaPmAgInS	objevit
krevní	krevní	k2eAgInSc1d1	krevní
oběh	oběh	k1gInSc1	oběh
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Cavendish	Cavendish	k1gMnSc1	Cavendish
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
elektrochemie	elektrochemie	k1gFnSc2	elektrochemie
byl	být	k5eAaImAgMnS	být
Humphry	Humphra	k1gFnSc2	Humphra
Davy	Dav	k1gInPc1	Dav
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
byl	být	k5eAaImAgInS	být
William	William	k1gInSc1	William
Lawrence	Lawrence	k1gFnSc1	Lawrence
Bragg	Bragg	k1gMnSc1	Bragg
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gInSc1	Joseph
Priestley	Priestlea	k1gFnSc2	Priestlea
objevil	objevit	k5eAaPmAgInS	objevit
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
a	a	k8xC	a
kyslík	kyslík	k1gInSc1	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
informatiky	informatika	k1gFnSc2	informatika
byl	být	k5eAaImAgMnS	být
matematik	matematik	k1gMnSc1	matematik
George	Georg	k1gMnSc2	Georg
Boole	Boole	k1gFnSc2	Boole
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
objevu	objev	k1gInSc3	objev
struktury	struktura	k1gFnSc2	struktura
DNA	dno	k1gNnSc2	dno
výrazně	výrazně	k6eAd1	výrazně
přispěla	přispět	k5eAaPmAgFnS	přispět
Rosalind	Rosalinda	k1gFnPc2	Rosalinda
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Stephenson	Stephenson	k1gMnSc1	Stephenson
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
parní	parní	k2eAgFnSc4d1	parní
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prosazení	prosazení	k1gNnSc4	prosazení
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
rval	rvát	k5eAaImAgMnS	rvát
Thomas	Thomas	k1gMnSc1	Thomas
Henry	henry	k1gInSc2	henry
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
fotografie	fotografia	k1gFnSc2	fotografia
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
možný	možný	k2eAgInSc1d1	možný
bez	bez	k1gInSc1	bez
díla	dílo	k1gNnSc2	dílo
chemika	chemik	k1gMnSc2	chemik
a	a	k8xC	a
fyzika	fyzik	k1gMnSc2	fyzik
Johna	John	k1gMnSc2	John
Herschela	Herschel	k1gMnSc2	Herschel
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
G.	G.	kA	G.
H.	H.	kA	H.
Hardy	Harda	k1gMnSc2	Harda
předložil	předložit	k5eAaPmAgMnS	předložit
zásadní	zásadní	k2eAgInPc4d1	zásadní
příspěvky	příspěvek	k1gInPc4	příspěvek
o	o	k7c4	o
teorii	teorie	k1gFnSc4	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Jane	Jan	k1gMnSc5	Jan
Goodallová	Goodallová	k1gFnSc1	Goodallová
proslula	proslout	k5eAaPmAgFnS	proslout
svým	svůj	k3xOyFgInSc7	svůj
výzkumem	výzkum	k1gInSc7	výzkum
šimpanzů	šimpanz	k1gMnPc2	šimpanz
<g/>
.	.	kIx.	.
</s>
<s>
Jev	jev	k1gInSc1	jev
interference	interference	k1gFnSc2	interference
světla	světlo	k1gNnSc2	světlo
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
Thomas	Thomas	k1gMnSc1	Thomas
Young	Young	k1gMnSc1	Young
<g/>
.	.	kIx.	.
</s>
<s>
Antisepsi	antisepse	k1gFnSc4	antisepse
do	do	k7c2	do
lékařství	lékařství	k1gNnSc2	lékařství
zavedl	zavést	k5eAaPmAgMnS	zavést
Joseph	Joseph	k1gMnSc1	Joseph
Lister	Lister	k1gMnSc1	Lister
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
John	John	k1gMnSc1	John
Couch	Couch	k1gMnSc1	Couch
Adams	Adams	k1gInSc4	Adams
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
čistě	čistě	k6eAd1	čistě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výpočtů	výpočet	k1gInPc2	výpočet
objev	objev	k1gInSc4	objev
planety	planeta	k1gFnSc2	planeta
Neptun	Neptun	k1gInSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
Andrew	Andrew	k1gMnSc1	Andrew
Wiles	Wiles	k1gMnSc1	Wiles
podal	podat	k5eAaPmAgMnS	podat
důkaz	důkaz	k1gInSc4	důkaz
Velké	velký	k2eAgFnSc2d1	velká
Fermatovy	Fermatův	k2eAgFnSc2d1	Fermatova
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Oughtred	Oughtred	k1gMnSc1	Oughtred
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
logaritmické	logaritmický	k2eAgNnSc4d1	logaritmické
pravítko	pravítko	k1gNnSc4	pravítko
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
Eddington	Eddington	k1gInSc4	Eddington
svými	svůj	k3xOyFgMnPc7	svůj
měřeními	měření	k1gNnPc7	měření
během	běh	k1gInSc7	běh
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
prokázal	prokázat	k5eAaPmAgMnS	prokázat
platnost	platnost	k1gFnSc4	platnost
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
-	-	kIx~	-
tu	tu	k6eAd1	tu
dnes	dnes	k6eAd1	dnes
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
například	například	k6eAd1	například
Roger	Rogra	k1gFnPc2	Rogra
Penrose	Penrosa	k1gFnSc3	Penrosa
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
sporné	sporný	k2eAgFnSc2d1	sporná
eugeniky	eugenika	k1gFnSc2	eugenika
byl	být	k5eAaImAgInS	být
Francis	Francis	k1gInSc1	Francis
Galton	Galton	k1gInSc1	Galton
<g/>
.	.	kIx.	.
</s>
<s>
Fred	Fred	k1gMnSc1	Fred
Hoyle	Hoyl	k1gMnSc2	Hoyl
plamenně	plamenně	k6eAd1	plamenně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
teorii	teorie	k1gFnSc4	teorie
Velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
dal	dát	k5eAaPmAgMnS	dát
jí	jíst	k5eAaImIp3nS	jíst
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
světla	světlo	k1gNnSc2	světlo
jako	jako	k8xC	jako
zdroje	zdroj	k1gInSc2	zdroj
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
William	William	k1gInSc4	William
Crookes	Crookesa	k1gFnPc2	Crookesa
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
magnetismu	magnetismus	k1gInSc2	magnetismus
a	a	k8xC	a
elektřiny	elektřina	k1gFnSc2	elektřina
zahajoval	zahajovat	k5eAaImAgInS	zahajovat
William	William	k1gInSc1	William
Gilbert	gilbert	k1gInSc1	gilbert
<g/>
.	.	kIx.	.
</s>
<s>
Možnosti	možnost	k1gFnPc1	možnost
vektorové	vektorový	k2eAgFnSc2d1	vektorová
analýzy	analýza	k1gFnSc2	analýza
objevil	objevit	k5eAaPmAgInS	objevit
Oliver	Oliver	k1gInSc1	Oliver
Heaviside	Heavisid	k1gMnSc5	Heavisid
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
<g/>
"	"	kIx"	"
zavedl	zavést	k5eAaPmAgMnS	zavést
Richard	Richard	k1gMnSc1	Richard
Owen	Owen	k1gMnSc1	Owen
<g/>
.	.	kIx.	.
</s>
<s>
Chronofotografickou	Chronofotografický	k2eAgFnSc4d1	Chronofotografický
techniku	technika	k1gFnSc4	technika
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
Eadweard	Eadweard	k1gInSc1	Eadweard
Muybridge	Muybridg	k1gInSc2	Muybridg
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
John	John	k1gMnSc1	John
Venn	Venn	k1gMnSc1	Venn
stvořil	stvořit	k5eAaPmAgMnS	stvořit
tzv.	tzv.	kA	tzv.
Vennovy	Vennův	k2eAgInPc4d1	Vennův
diagramy	diagram	k1gInPc4	diagram
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
použitelné	použitelný	k2eAgInPc4d1	použitelný
napříč	napříč	k7c7	napříč
obory	obor	k1gInPc7	obor
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Newcomen	Newcomen	k2eAgMnSc1d1	Newcomen
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
stroj	stroj	k1gInSc4	stroj
pro	pro	k7c4	pro
čerpání	čerpání	k1gNnSc4	čerpání
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
funkční	funkční	k2eAgFnSc4d1	funkční
parní	parní	k2eAgFnSc4d1	parní
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
Richard	Richard	k1gMnSc1	Richard
Trevithick	Trevithick	k1gMnSc1	Trevithick
<g/>
.	.	kIx.	.
</s>
<s>
Matematickou	matematický	k2eAgFnSc4d1	matematická
statistiku	statistika	k1gFnSc4	statistika
zakládal	zakládat	k5eAaImAgInS	zakládat
Ronald	Ronald	k1gMnSc1	Ronald
Fisher	Fishra	k1gFnPc2	Fishra
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Lassell	Lassell	k1gMnSc1	Lassell
objevil	objevit	k5eAaPmAgMnS	objevit
Saturnův	Saturnův	k2eAgInSc4d1	Saturnův
měsíc	měsíc	k1gInSc4	měsíc
Triton	triton	k1gMnSc1	triton
<g/>
.	.	kIx.	.
</s>
<s>
Neurolog	neurolog	k1gMnSc1	neurolog
Oliver	Olivra	k1gFnPc2	Olivra
Sacks	Sacksa	k1gFnPc2	Sacksa
proslul	proslout	k5eAaPmAgInS	proslout
literárním	literární	k2eAgNnSc7d1	literární
zpracováním	zpracování	k1gNnSc7	zpracování
svých	svůj	k3xOyFgInPc2	svůj
případů	případ	k1gInPc2	případ
a	a	k8xC	a
popularizací	popularizace	k1gFnPc2	popularizace
výzkumu	výzkum	k1gInSc2	výzkum
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
získali	získat	k5eAaPmAgMnP	získat
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gInSc4	Dirac
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Chadwick	Chadwick	k1gMnSc1	Chadwick
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
William	William	k1gInSc4	William
Strutt	Strutt	k2eAgInSc4d1	Strutt
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Henry	Henry	k1gMnSc1	Henry
Bragg	Bragg	k1gMnSc1	Bragg
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Higgs	Higgs	k1gInSc1	Higgs
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Glover	Glover	k1gMnSc1	Glover
Barkla	Barkla	k1gMnSc1	Barkla
<g/>
,	,	kIx,	,
Owen	Owen	k1gMnSc1	Owen
Willans	Willansa	k1gFnPc2	Willansa
Richardson	Richardson	k1gMnSc1	Richardson
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Victor	Victor	k1gMnSc1	Victor
Appleton	Appleton	k1gInSc1	Appleton
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Paget	Paget	k1gMnSc1	Paget
Thomson	Thomson	k1gMnSc1	Thomson
<g/>
,	,	kIx,	,
Patrick	Patrick	k1gMnSc1	Patrick
Blackett	Blackett	k1gMnSc1	Blackett
<g/>
,	,	kIx,	,
Cecil	Cecil	k1gMnSc1	Cecil
Powell	Powell	k1gMnSc1	Powell
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Ryle	Ryl	k1gFnSc2	Ryl
<g/>
,	,	kIx,	,
Nevill	Nevill	k1gMnSc1	Nevill
Francis	Francis	k1gFnSc2	Francis
Mott	motto	k1gNnPc2	motto
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnPc1	Anthona
James	James	k1gMnSc1	James
Leggett	Leggett	k1gMnSc1	Leggett
a	a	k8xC	a
Antony	anton	k1gInPc7	anton
Hewish	Hewisha	k1gFnPc2	Hewisha
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Francis	Francis	k1gFnSc2	Francis
William	William	k1gInSc1	William
Aston	Aston	k1gInSc1	Aston
<g/>
,	,	kIx,	,
Dorothy	Dorotha	k1gMnSc2	Dorotha
Hodgkinová	Hodgkinová	k1gFnSc1	Hodgkinová
<g/>
,	,	kIx,	,
Frederick	Frederick	k1gMnSc1	Frederick
Soddy	Sodda	k1gFnSc2	Sodda
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
Haworth	Haworth	k1gMnSc1	Haworth
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
Harry	Harra	k1gMnSc2	Harra
Kroto	Kroto	k1gNnSc1	Kroto
a	a	k8xC	a
John	John	k1gMnSc1	John
Kendrew	Kendrew	k1gMnSc1	Kendrew
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
Francis	Francis	k1gFnSc2	Francis
Crick	Cricko	k1gNnPc2	Cricko
<g/>
,	,	kIx,	,
Maurice	Maurika	k1gFnSc3	Maurika
Wilkins	Wilkinsa	k1gFnPc2	Wilkinsa
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gFnPc1	Andrew
Fielding	Fielding	k1gInSc1	Fielding
Huxley	Huxlea	k1gFnSc2	Huxlea
<g/>
,	,	kIx,	,
Alan	Alan	k1gMnSc1	Alan
Lloyd	Lloyd	k1gMnSc1	Lloyd
Hodgkin	Hodgkin	k1gMnSc1	Hodgkin
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Scott	Scott	k1gMnSc1	Scott
Sherrington	Sherrington	k1gInSc4	Sherrington
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc3	henry
Hallett	Hallett	k2eAgInSc1d1	Hallett
Dale	Dale	k1gInSc1	Dale
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Mansfield	Mansfield	k1gMnSc1	Mansfield
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Sulston	Sulston	k1gInSc1	Sulston
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Nurse	Nurse	k1gFnSc2	Nurse
<g/>
,	,	kIx,	,
Tim	Tim	k?	Tim
Hunt	hunt	k1gInSc1	hunt
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
John	John	k1gMnSc1	John
Roberts	Roberts	k1gInSc1	Roberts
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Vane	vanout	k5eAaImIp3nS	vanout
<g/>
,	,	kIx,	,
Rodney	Rodnea	k1gFnSc2	Rodnea
Robert	Roberta	k1gFnPc2	Roberta
Porter	porter	k1gInSc1	porter
a	a	k8xC	a
Edgar	Edgar	k1gMnSc1	Edgar
Douglas	Douglas	k1gMnSc1	Douglas
Adrian	Adrian	k1gMnSc1	Adrian
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
britské	britský	k2eAgFnSc2d1	britská
vědy	věda	k1gFnSc2	věda
ovšem	ovšem	k9	ovšem
významně	významně	k6eAd1	významně
přispěli	přispět	k5eAaPmAgMnP	přispět
i	i	k9	i
Skotové	Skot	k1gMnPc1	Skot
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
úpravou	úprava	k1gFnSc7	úprava
parního	parní	k2eAgInSc2d1	parní
mechanismu	mechanismus	k1gInSc2	mechanismus
rozpoutal	rozpoutat	k5eAaPmAgMnS	rozpoutat
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
revoluci	revoluce	k1gFnSc4	revoluce
James	James	k1gMnSc1	James
Watt	watt	k1gInSc1	watt
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
výzkumu	výzkum	k1gInSc2	výzkum
elektromagnetismu	elektromagnetismus	k1gInSc2	elektromagnetismus
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nepředstavitelné	představitelný	k2eNgInPc1d1	nepředstavitelný
bez	bez	k7c2	bez
Jamese	Jamese	k1gFnSc2	Jamese
Clerka	Clerek	k1gMnSc2	Clerek
Maxwella	Maxwell	k1gMnSc2	Maxwell
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
vynálezce	vynálezce	k1gMnSc1	vynálezce
telefonu	telefon	k1gInSc2	telefon
Alexander	Alexandra	k1gFnPc2	Alexandra
Graham	graham	k1gInSc1	graham
Bell	bell	k1gInSc1	bell
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Fleming	Fleming	k1gInSc4	Fleming
objevil	objevit	k5eAaPmAgInS	objevit
penicilin	penicilin	k1gInSc1	penicilin
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Skota	Skot	k1gMnSc4	Skot
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
fyzika	fyzika	k1gFnSc1	fyzika
Williama	William	k1gMnSc2	William
Thomsona	Thomson	k1gMnSc2	Thomson
(	(	kIx(	(
<g/>
Kelvina	Kelvin	k2eAgMnSc2d1	Kelvin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Logie	Logie	k1gFnSc2	Logie
Baird	Baird	k1gMnSc1	Baird
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
vynálezců	vynálezce	k1gMnPc2	vynálezce
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Napier	Napier	k1gMnSc1	Napier
objevil	objevit	k5eAaPmAgMnS	objevit
logaritmy	logaritmus	k1gInPc4	logaritmus
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgInS	zavést
desetinnou	desetinný	k2eAgFnSc4d1	desetinná
čárku	čárka	k1gFnSc4	čárka
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Lyell	Lyell	k1gMnSc1	Lyell
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
geologie	geologie	k1gFnSc2	geologie
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Dewar	Dewar	k1gMnSc1	Dewar
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
termosku	termoska	k1gFnSc4	termoska
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Brown	Brown	k1gMnSc1	Brown
definoval	definovat	k5eAaBmAgMnS	definovat
tzv.	tzv.	kA	tzv.
Brownův	Brownův	k2eAgInSc4d1	Brownův
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
termodynamiky	termodynamika	k1gFnSc2	termodynamika
byli	být	k5eAaImAgMnP	být
Joseph	Joseph	k1gMnSc1	Joseph
Black	Black	k1gMnSc1	Black
či	či	k8xC	či
William	William	k1gInSc1	William
John	John	k1gMnSc1	John
Macquorn	Macquorn	k1gMnSc1	Macquorn
Rankine	Rankin	k1gMnSc5	Rankin
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
matematikem	matematik	k1gMnSc7	matematik
byl	být	k5eAaImAgInS	být
Colin	Colin	k2eAgInSc1d1	Colin
Maclaurin	Maclaurin	k1gInSc1	Maclaurin
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Boyd	Boyd	k1gMnSc1	Boyd
Dunlop	Dunlop	k1gInSc4	Dunlop
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
pneumatiku	pneumatika	k1gFnSc4	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Brewster	Brewster	k1gMnSc1	Brewster
byl	být	k5eAaImAgMnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Gregory	Gregor	k1gMnPc7	Gregor
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
spolutvůrcům	spolutvůrce	k1gMnPc3	spolutvůrce
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Rutherford	Rutherford	k1gMnSc1	Rutherford
dokázal	dokázat	k5eAaPmAgMnS	dokázat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
izolovat	izolovat	k5eAaBmF	izolovat
dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Vynálezcem	vynálezce	k1gMnSc7	vynálezce
procesu	proces	k1gInSc2	proces
dialýzy	dialýza	k1gFnSc2	dialýza
byl	být	k5eAaImAgMnS	být
Thomas	Thomas	k1gMnSc1	Thomas
Graham	Graham	k1gMnSc1	Graham
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vynálezu	vynález	k1gInSc6	vynález
radaru	radar	k1gInSc2	radar
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
Robert	Robert	k1gMnSc1	Robert
Watson-Watt	Watson-Watt	k1gMnSc1	Watson-Watt
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Thomson	Thomsona	k1gFnPc2	Thomsona
Rees	Rees	k1gInSc1	Rees
Wilson	Wilson	k1gMnSc1	Wilson
získali	získat	k5eAaPmAgMnP	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Alexander	Alexandra	k1gFnPc2	Alexandra
Robertus	Robertus	k1gMnSc1	Robertus
Todd	Todd	k1gMnSc1	Todd
<g/>
,	,	kIx,	,
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
John	John	k1gMnSc1	John
James	James	k1gMnSc1	James
Rickard	Rickard	k1gMnSc1	Rickard
Macleod	Macleod	k1gInSc1	Macleod
a	a	k8xC	a
James	James	k1gMnSc1	James
W.	W.	kA	W.
Black	Black	k1gMnSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Velšanů	Velšan	k1gMnPc2	Velšan
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Alfred	Alfred	k1gMnSc1	Alfred
Russel	Russel	k1gMnSc1	Russel
Wallace	Wallace	k1gFnPc4	Wallace
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
otců	otec	k1gMnPc2	otec
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Brian	Brian	k1gMnSc1	Brian
Josephson	Josephson	k1gMnSc1	Josephson
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Martin	Martin	k1gInSc1	Martin
Evans	Evans	k1gInSc1	Evans
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Matematik	matematik	k1gMnSc1	matematik
Robert	Robert	k1gMnSc1	Robert
Recorde	Record	k1gInSc5	Record
zavedl	zavést	k5eAaPmAgInS	zavést
symbol	symbol	k1gInSc1	symbol
=	=	kIx~	=
<g/>
.	.	kIx.	.
</s>
<s>
Vynálezce	vynálezce	k1gMnSc1	vynálezce
David	David	k1gMnSc1	David
Edward	Edward	k1gMnSc1	Edward
Hughes	Hughes	k1gMnSc1	Hughes
mj.	mj.	kA	mj.
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
mikrofon	mikrofon	k1gInSc4	mikrofon
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
pocházel	pocházet	k5eAaImAgMnS	pocházet
zakladatel	zakladatel	k1gMnSc1	zakladatel
středověkých	středověký	k2eAgFnPc2d1	středověká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Alcuin	Alcuin	k1gMnSc1	Alcuin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
Anglickou	anglický	k2eAgFnSc4d1	anglická
scholastiku	scholastika	k1gFnSc4	scholastika
tvořili	tvořit	k5eAaImAgMnP	tvořit
zakladatel	zakladatel	k1gMnSc1	zakladatel
dialektiky	dialektika	k1gFnSc2	dialektika
Anselm	Anselm	k1gMnSc1	Anselm
z	z	k7c2	z
Canterbury	Canterbura	k1gFnSc2	Canterbura
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
politologie	politologie	k1gFnSc2	politologie
Jan	Jan	k1gMnSc1	Jan
ze	z	k7c2	z
Salisbury	Salisbura	k1gFnSc2	Salisbura
<g/>
,	,	kIx,	,
Roger	Roger	k1gMnSc1	Roger
Bacon	Bacon	k1gMnSc1	Bacon
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Grosseteste	Grossetest	k1gInSc5	Grossetest
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Duns	Dunsa	k1gFnPc2	Dunsa
Scotus	Scotus	k1gInSc1	Scotus
nebo	nebo	k8xC	nebo
William	William	k1gInSc1	William
Ockham	Ockham	k1gInSc1	Ockham
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
filozofy	filozof	k1gMnPc7	filozof
raného	raný	k2eAgInSc2d1	raný
novověku	novověk	k1gInSc2	novověk
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
zejména	zejména	k9	zejména
Francis	Francis	k1gFnSc4	Francis
Bacon	Bacona	k1gFnPc2	Bacona
<g/>
,	,	kIx,	,
empiristé	empirista	k1gMnPc1	empirista
Thomas	Thomas	k1gMnSc1	Thomas
Hobbes	Hobbes	k1gMnSc1	Hobbes
a	a	k8xC	a
John	John	k1gMnSc1	John
Locke	Lock	k1gFnSc2	Lock
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgInPc4	jenž
navázali	navázat	k5eAaPmAgMnP	navázat
idealistický	idealistický	k2eAgMnSc1d1	idealistický
filosof	filosof	k1gMnSc1	filosof
George	Georg	k1gMnSc2	Georg
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
osvícenec	osvícenec	k1gMnSc1	osvícenec
Thomas	Thomas	k1gMnSc1	Thomas
Paine	Pain	k1gInSc5	Pain
nebo	nebo	k8xC	nebo
skeptik	skeptik	k1gMnSc1	skeptik
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc1	Hume
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
odnoží	odnož	k1gFnSc7	odnož
britského	britský	k2eAgInSc2d1	britský
empirismu	empirismus	k1gInSc2	empirismus
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
skotská	skotský	k2eAgFnSc1d1	skotská
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
popisovaná	popisovaný	k2eAgNnPc1d1	popisované
jako	jako	k8xS	jako
Scottish	Scottish	k1gMnSc1	Scottish
School	Schoola	k1gFnPc2	Schoola
of	of	k?	of
Common	Common	k1gNnSc4	Common
Sense	Sense	k1gFnSc2	Sense
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
patřil	patřit	k5eAaImAgMnS	patřit
například	například	k6eAd1	například
Adam	Adam	k1gMnSc1	Adam
Smith	Smith	k1gMnSc1	Smith
či	či	k8xC	či
Thomas	Thomas	k1gMnSc1	Thomas
Reid	Reid	k1gMnSc1	Reid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
působili	působit	k5eAaImAgMnP	působit
utilitaristé	utilitarista	k1gMnPc1	utilitarista
Jeremy	Jerema	k1gFnSc2	Jerema
Bentham	Bentham	k1gInSc1	Bentham
a	a	k8xC	a
John	John	k1gMnSc1	John
Stuart	Stuarta	k1gFnPc2	Stuarta
Mill	Mill	k1gMnSc1	Mill
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
James	James	k1gMnSc1	James
Mill	Mill	k1gMnSc1	Mill
byl	být	k5eAaImAgMnS	být
významným	významný	k2eAgMnSc7d1	významný
ekonomem	ekonom	k1gMnSc7	ekonom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
sociolog	sociolog	k1gMnSc1	sociolog
Herbert	Herbert	k1gMnSc1	Herbert
Spencer	Spencer	k1gMnSc1	Spencer
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
filozofii	filozofie	k1gFnSc4	filozofie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
významní	významný	k2eAgMnPc1d1	významný
logik	logik	k1gMnSc1	logik
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Bertrand	Bertranda	k1gFnPc2	Bertranda
Russell	Russell	k1gMnSc1	Russell
<g/>
,	,	kIx,	,
originální	originální	k2eAgMnSc1d1	originální
platonik	platonik	k1gMnSc1	platonik
Alfred	Alfred	k1gMnSc1	Alfred
North	North	k1gMnSc1	North
Whitehead	Whitehead	k1gInSc4	Whitehead
<g/>
,	,	kIx,	,
logici	logik	k1gMnPc1	logik
George	Georg	k1gMnSc2	Georg
Boole	Boole	k1gNnSc1	Boole
nebo	nebo	k8xC	nebo
Alan	Alan	k1gMnSc1	Alan
Turing	Turing	k1gInSc1	Turing
<g/>
,	,	kIx,	,
filozofický	filozofický	k2eAgMnSc1d1	filozofický
antropolog	antropolog	k1gMnSc1	antropolog
Gregory	Gregor	k1gMnPc4	Gregor
Bateson	Bateson	k1gInSc4	Bateson
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
filozof	filozof	k1gMnSc1	filozof
Isaiah	Isaiaha	k1gFnPc2	Isaiaha
Berlin	berlina	k1gFnPc2	berlina
nebo	nebo	k8xC	nebo
morální	morální	k2eAgMnPc1d1	morální
filozofové	filozof	k1gMnPc1	filozof
Bernard	Bernard	k1gMnSc1	Bernard
Williams	Williams	k1gInSc1	Williams
a	a	k8xC	a
Alasdair	Alasdair	k1gInSc1	Alasdair
MacIntyre	MacIntyr	k1gInSc5	MacIntyr
<g/>
.	.	kIx.	.
</s>
<s>
Britskou	britský	k2eAgFnSc4d1	britská
filozofii	filozofie	k1gFnSc4	filozofie
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
významně	významně	k6eAd1	významně
spolutvořili	spolutvořit	k5eAaImAgMnP	spolutvořit
také	také	k9	také
emigranti	emigrant	k1gMnPc1	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
přišel	přijít	k5eAaPmAgMnS	přijít
Ludwig	Ludwig	k1gMnSc1	Ludwig
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
a	a	k8xC	a
političtí	politický	k2eAgMnPc1d1	politický
exulanti	exulant	k1gMnPc1	exulant
Arthur	Arthur	k1gMnSc1	Arthur
Koestler	Koestler	k1gMnSc1	Koestler
a	a	k8xC	a
Karl	Karl	k1gMnSc1	Karl
Raimund	Raimunda	k1gFnPc2	Raimunda
Popper	Popper	k1gMnSc1	Popper
<g/>
,	,	kIx,	,
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
Ernest	Ernest	k1gMnSc1	Ernest
Gellner	Gellner	k1gMnSc1	Gellner
<g/>
,	,	kIx,	,
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
filozof	filozof	k1gMnSc1	filozof
matematiky	matematika	k1gFnSc2	matematika
Imre	Imr	k1gFnSc2	Imr
Lakatos	Lakatosa	k1gFnPc2	Lakatosa
a	a	k8xC	a
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
Leszek	Leszka	k1gFnPc2	Leszka
Kołakowski	Kołakowsk	k1gFnSc2	Kołakowsk
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
ekonomů	ekonom	k1gMnPc2	ekonom
a	a	k8xC	a
mužem	muž	k1gMnSc7	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
euro-americkou	euromerický	k2eAgFnSc4d1	euro-americká
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
politiku	politika	k1gFnSc4	politika
třiceti	třicet	k4xCc2	třicet
zázračných	zázračný	k2eAgNnPc2d1	zázračné
let	léto	k1gNnPc2	léto
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Maynard	Maynard	k1gMnSc1	Maynard
Keynes	Keynes	k1gMnSc1	Keynes
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelkou	zakladatelka	k1gFnSc7	zakladatelka
feministické	feministický	k2eAgFnSc2d1	feministická
filozofie	filozofie	k1gFnSc2	filozofie
byla	být	k5eAaImAgFnS	být
Mary	Mary	k1gFnSc1	Mary
Wollstonecraftová	Wollstonecraftová	k1gFnSc1	Wollstonecraftová
<g/>
.	.	kIx.	.
</s>
<s>
Klasikem	klasik	k1gMnSc7	klasik
demografie	demografie	k1gFnSc2	demografie
je	být	k5eAaImIp3nS	být
Thomas	Thomas	k1gMnSc1	Thomas
Robert	Robert	k1gMnSc1	Robert
Malthus	Malthus	k1gMnSc1	Malthus
<g/>
,	,	kIx,	,
ekonomie	ekonomie	k1gFnSc1	ekonomie
David	David	k1gMnSc1	David
Ricardo	Ricardo	k1gNnSc1	Ricardo
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
autor	autor	k1gMnSc1	autor
teorie	teorie	k1gFnSc2	teorie
komparativní	komparativní	k2eAgFnPc4d1	komparativní
výhody	výhoda	k1gFnPc4	výhoda
tvrdící	tvrdící	k2eAgFnPc4d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
obchod	obchod	k1gInSc1	obchod
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
výhodný	výhodný	k2eAgMnSc1d1	výhodný
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
či	či	k8xC	či
Alfred	Alfred	k1gMnSc1	Alfred
Marshall	Marshall	k1gMnSc1	Marshall
<g/>
.	.	kIx.	.
</s>
<s>
Archeolog	archeolog	k1gMnSc1	archeolog
Howard	Howard	k1gMnSc1	Howard
Carter	Carter	k1gMnSc1	Carter
objevil	objevit	k5eAaPmAgMnS	objevit
Tutanchamonovu	Tutanchamonův	k2eAgFnSc4d1	Tutanchamonova
hrobku	hrobka	k1gFnSc4	hrobka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
kolega	kolega	k1gMnSc1	kolega
Arthur	Arthur	k1gMnSc1	Arthur
Evans	Evans	k1gInSc4	Evans
vykopal	vykopat	k5eAaPmAgMnS	vykopat
palác	palác	k1gInSc4	palác
v	v	k7c6	v
Knóssu	Knóss	k1gInSc6	Knóss
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
<g/>
.	.	kIx.	.
</s>
<s>
Beda	Beda	k1gMnSc1	Beda
Ctihodný	ctihodný	k2eAgMnSc1d1	ctihodný
založil	založit	k5eAaPmAgMnS	založit
anglickou	anglický	k2eAgFnSc4d1	anglická
historiografii	historiografie	k1gFnSc4	historiografie
<g/>
,	,	kIx,	,
klasickou	klasický	k2eAgFnSc4d1	klasická
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
starého	starý	k2eAgInSc2d1	starý
Říma	Řím	k1gInSc2	Řím
předložil	předložit	k5eAaPmAgMnS	předložit
Edward	Edward	k1gMnSc1	Edward
Gibbon	gibbon	k1gMnSc1	gibbon
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
historikem	historik	k1gMnSc7	historik
umění	umění	k1gNnSc2	umění
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Ruskin	Ruskin	k1gMnSc1	Ruskin
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
získal	získat	k5eAaPmAgMnS	získat
Ronald	Ronald	k1gMnSc1	Ronald
Coase	Coase	k1gFnSc2	Coase
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
Bradford	Bradford	k1gMnSc1	Bradford
Titchener	Titchener	k1gMnSc1	Titchener
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Skotů	Skot	k1gMnPc2	Skot
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
James	James	k1gMnSc1	James
Frazer	Frazer	k1gMnSc1	Frazer
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sepsal	sepsat	k5eAaPmAgMnS	sepsat
proslavenou	proslavený	k2eAgFnSc4d1	proslavená
knihu	kniha	k1gFnSc4	kniha
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
ratolest	ratolest	k1gFnSc1	ratolest
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc4d1	základní
práci	práce	k1gFnSc4	práce
moderní	moderní	k2eAgFnSc2d1	moderní
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
,	,	kIx,	,
religionistiky	religionistika	k1gFnSc2	religionistika
a	a	k8xC	a
etnografie	etnografie	k1gFnSc2	etnografie
<g/>
.	.	kIx.	.
</s>
<s>
Ronald	Ronald	k1gMnSc1	Ronald
David	David	k1gMnSc1	David
Laing	Laing	k1gMnSc1	Laing
byl	být	k5eAaImAgMnS	být
představitelem	představitel	k1gMnSc7	představitel
tzv.	tzv.	kA	tzv.
antipsychiatrie	antipsychiatrie	k1gFnPc1	antipsychiatrie
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
Alasdair	Alasdair	k1gMnSc1	Alasdair
MacIntyre	MacIntyr	k1gInSc5	MacIntyr
komunitarismu	komunitarismus	k1gInSc2	komunitarismus
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Muir	Muir	k1gMnSc1	Muir
byl	být	k5eAaImAgMnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
environmentalismu	environmentalismus	k1gInSc2	environmentalismus
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
free	freat	k5eAaPmIp3nS	freat
school	school	k1gInSc4	school
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
filozof	filozof	k1gMnSc1	filozof
Alexander	Alexandra	k1gFnPc2	Alexandra
Sutherland	Sutherlanda	k1gFnPc2	Sutherlanda
Neill	Neill	k1gInSc1	Neill
<g/>
.	.	kIx.	.
</s>
<s>
Angus	Angus	k1gInSc1	Angus
Deaton	Deaton	k1gInSc1	Deaton
a	a	k8xC	a
James	James	k1gMnSc1	James
Mirrlees	Mirrlees	k1gMnSc1	Mirrlees
získali	získat	k5eAaPmAgMnP	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Velšan	Velšan	k1gMnSc1	Velšan
Geoffrey	Geoffrea	k1gFnSc2	Geoffrea
z	z	k7c2	z
Monmouthu	Monmouth	k1gInSc2	Monmouth
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
britských	britský	k2eAgMnPc2d1	britský
historiků	historik	k1gMnPc2	historik
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
popularitou	popularita	k1gFnSc7	popularita
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
králi	král	k1gMnSc6	král
Artušovi	Artuš	k1gMnSc6	Artuš
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
kulturálních	kulturální	k2eAgFnPc2d1	kulturální
studií	studie	k1gFnPc2	studie
byl	být	k5eAaImAgInS	být
Raymond	Raymond	k1gInSc1	Raymond
Williams	Williams	k1gInSc1	Williams
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
psychoanalýzy	psychoanalýza	k1gFnSc2	psychoanalýza
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
prvním	první	k4xOgMnPc3	první
Freudovým	Freudový	k2eAgMnPc3d1	Freudový
životopiscem	životopisec	k1gMnSc7	životopisec
byl	být	k5eAaImAgMnS	být
jiný	jiný	k2eAgMnSc1d1	jiný
Velšan	Velšan	k1gMnSc1	Velšan
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
Jones	Jones	k1gMnSc1	Jones
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
je	být	k5eAaImIp3nS	být
kolébkou	kolébka	k1gFnSc7	kolébka
fotbalu	fotbal	k1gInSc2	fotbal
<g/>
,	,	kIx,	,
liga	liga	k1gFnSc1	liga
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějšími	slavný	k2eAgInPc7d3	nejslavnější
kluby	klub	k1gInPc7	klub
Chelsea	Chelse	k1gInSc2	Chelse
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
<g/>
,	,	kIx,	,
Liverpool	Liverpool	k1gInSc1	Liverpool
<g/>
,	,	kIx,	,
Arsenal	Arsenal	k1gFnSc1	Arsenal
FC	FC	kA	FC
<g/>
,	,	kIx,	,
Glasgow	Glasgow	k1gNnSc1	Glasgow
Rangers	Rangersa	k1gFnPc2	Rangersa
<g/>
,	,	kIx,	,
Celtic	Celtice	k1gFnPc2	Celtice
FC	FC	kA	FC
a	a	k8xC	a
národní	národní	k2eAgNnSc1d1	národní
mužstvo	mužstvo	k1gNnSc1	mužstvo
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
světové	světový	k2eAgFnSc3d1	světová
špičce	špička	k1gFnSc3	špička
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
nejoblíbenějším	oblíbený	k2eAgInSc7d3	nejoblíbenější
sportem	sport	k1gInSc7	sport
je	být	k5eAaImIp3nS	být
ragby	ragby	k1gNnSc1	ragby
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
zápasy	zápas	k1gInPc1	zápas
ragbyové	ragbyový	k2eAgFnSc2d1	ragbyová
ligy	liga	k1gFnSc2	liga
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c6	na
stadionech	stadion	k1gInPc6	stadion
Twickenham	Twickenham	k1gInSc4	Twickenham
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Millennium	millennium	k1gNnSc1	millennium
v	v	k7c6	v
Cardiffu	Cardiff	k1gInSc6	Cardiff
a	a	k8xC	a
Murrayfield	Murrayfield	k1gMnSc1	Murrayfield
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Skotska	Skotsko	k1gNnSc2	Skotsko
pochází	pocházet	k5eAaImIp3nS	pocházet
golf	golf	k1gInSc4	golf
<g/>
,	,	kIx,	,
v	v	k7c6	v
r.	r.	kA	r.
1650	[number]	k4	1650
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgInS	hrát
už	už	k6eAd1	už
na	na	k7c6	na
12	[number]	k4	12
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnPc4d1	oficiální
pravidla	pravidlo	k1gNnPc4	pravidlo
ustavil	ustavit	k5eAaPmAgMnS	ustavit
v	v	k7c6	v
r.	r.	kA	r.
1897	[number]	k4	1897
Královský	královský	k2eAgInSc1d1	královský
golfový	golfový	k2eAgInSc1d1	golfový
klub	klub	k1gInSc1	klub
sv.	sv.	kA	sv.
Andrewa	Andrew	k1gInSc2	Andrew
a	a	k8xC	a
poté	poté	k6eAd1	poté
golf	golf	k1gInSc1	golf
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
Scottish	Scottish	k1gInSc1	Scottish
Open	Opena	k1gFnPc2	Opena
Golf	golf	k1gInSc1	golf
a	a	k8xC	a
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
mnoho	mnoho	k6eAd1	mnoho
větších	veliký	k2eAgInPc2d2	veliký
i	i	k8xC	i
lokálních	lokální	k2eAgInPc2d1	lokální
turnajů	turnaj	k1gInPc2	turnaj
profesionálů	profesionál	k1gMnPc2	profesionál
i	i	k8xC	i
amatérů	amatér	k1gMnPc2	amatér
<g/>
.	.	kIx.	.
</s>
<s>
Kriket	kriket	k1gInSc4	kriket
hrají	hrát	k5eAaImIp3nP	hrát
od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
všichni	všechen	k3xTgMnPc1	všechen
a	a	k8xC	a
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
po	po	k7c6	po
víkendech	víkend	k1gInPc6	víkend
před	před	k7c7	před
každým	každý	k3xTgInSc7	každý
domem	dům	k1gInSc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
kriketovou	kriketový	k2eAgFnSc7d1	kriketová
arénou	aréna	k1gFnSc7	aréna
je	být	k5eAaImIp3nS	být
Lord	lord	k1gMnSc1	lord
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jezdectví	jezdectví	k1gNnSc4	jezdectví
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
sport	sport	k1gInSc4	sport
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Stuart	Stuart	k1gInSc1	Stuart
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
pravidelně	pravidelně	k6eAd1	pravidelně
např.	např.	kA	např.
Grand	grand	k1gMnSc1	grand
National	National	k1gMnSc1	National
v	v	k7c6	v
Aintree	Aintree	k1gFnSc6	Aintree
v	v	k7c6	v
Liverpoolu	Liverpool	k1gInSc6	Liverpool
<g/>
,	,	kIx,	,
Scottish	Scottish	k1gMnSc1	Scottish
Grand	grand	k1gMnSc1	grand
National	National	k1gMnSc1	National
v	v	k7c6	v
Ayru	Ayrus	k1gInSc6	Ayrus
a	a	k8xC	a
Derby	derby	k1gNnSc1	derby
a	a	k8xC	a
Oaks	Oaks	k1gInSc1	Oaks
v	v	k7c6	v
Epsomu	Epsom	k1gInSc6	Epsom
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
tenisovým	tenisový	k2eAgInSc7d1	tenisový
turnajem	turnaj	k1gInSc7	turnaj
je	být	k5eAaImIp3nS	být
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
je	být	k5eAaImIp3nS	být
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
lehkoatletických	lehkoatletický	k2eAgInPc2d1	lehkoatletický
mítinků	mítink	k1gInPc2	mítink
<g/>
,	,	kIx,	,
turnajů	turnaj	k1gInPc2	turnaj
v	v	k7c6	v
baseballu	baseball	k1gInSc6	baseball
i	i	k8xC	i
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgNnPc1d1	Britské
města	město	k1gNnPc1	město
se	se	k3xPyFc4	se
příkladně	příkladně	k6eAd1	příkladně
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
sportování	sportování	k1gNnSc4	sportování
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
je	být	k5eAaImIp3nS	být
aspoň	aspoň	k9	aspoň
jeden	jeden	k4xCgInSc1	jeden
bazén	bazén	k1gInSc1	bazén
a	a	k8xC	a
sportovní	sportovní	k2eAgNnSc1d1	sportovní
středisko	středisko	k1gNnSc1	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
aerobik	aerobik	k1gInSc1	aerobik
<g/>
,	,	kIx,	,
jóga	jóga	k1gFnSc1	jóga
<g/>
,	,	kIx,	,
bojové	bojový	k2eAgNnSc1d1	bojové
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parcích	park	k1gInPc6	park
jsou	být	k5eAaImIp3nP	být
cyklotrasy	cyklotrasa	k1gFnPc1	cyklotrasa
i	i	k8xC	i
chodníky	chodník	k1gInPc1	chodník
pro	pro	k7c4	pro
jogging	jogging	k1gInSc4	jogging
<g/>
.	.	kIx.	.
</s>
<s>
Romantický	romantický	k2eAgInSc1d1	romantický
venkov	venkov	k1gInSc1	venkov
Východní	východní	k2eAgFnSc2d1	východní
Anglie	Anglie	k1gFnSc2	Anglie
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
oblastí	oblast	k1gFnPc2	oblast
pro	pro	k7c4	pro
cykloturistiku	cykloturistika	k1gFnSc4	cykloturistika
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
jachting	jachting	k1gInSc1	jachting
<g/>
,	,	kIx,	,
túry	túra	k1gFnPc1	túra
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
,	,	kIx,	,
horské	horský	k2eAgFnPc1d1	horská
túry	túra	k1gFnPc1	túra
<g/>
,	,	kIx,	,
alpinismus	alpinismus	k1gInSc1	alpinismus
<g/>
,	,	kIx,	,
rafting	rafting	k1gInSc1	rafting
<g/>
,	,	kIx,	,
létání	létání	k1gNnSc1	létání
na	na	k7c6	na
rogalech	rogalo	k1gNnPc6	rogalo
či	či	k8xC	či
v	v	k7c6	v
balonu	balon	k1gInSc6	balon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
se	se	k3xPyFc4	se
chodí	chodit	k5eAaImIp3nP	chodit
i	i	k9	i
rybařit	rybařit	k5eAaImF	rybařit
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
však	však	k9	však
různě	různě	k6eAd1	různě
přísná	přísný	k2eAgNnPc1d1	přísné
místní	místní	k2eAgNnPc1d1	místní
omezení	omezení	k1gNnPc1	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
tu	tu	k6eAd1	tu
jsou	být	k5eAaImIp3nP	být
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgNnPc4d1	přístupné
golfová	golfový	k2eAgNnPc4d1	golfové
hřiště	hřiště	k1gNnPc4	hřiště
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
je	být	k5eAaImIp3nS	být
běh	běh	k1gInSc1	běh
po	po	k7c6	po
centru	centrum	k1gNnSc6	centrum
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Lewis	Lewis	k1gInSc1	Lewis
Hamilton	Hamilton	k1gInSc1	Hamilton
je	být	k5eAaImIp3nS	být
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
titul	titul	k1gInSc4	titul
má	mít	k5eAaImIp3nS	mít
James	James	k1gMnSc1	James
Hunt	hunt	k1gInSc1	hunt
<g/>
,	,	kIx,	,
Nigel	Nigel	k1gMnSc1	Nigel
Mansell	Mansell	k1gMnSc1	Mansell
<g/>
,	,	kIx,	,
Jenson	Jenson	k1gMnSc1	Jenson
Button	Button	k1gInSc1	Button
a	a	k8xC	a
Damon	Damon	k1gMnSc1	Damon
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Fotbalovými	fotbalový	k2eAgFnPc7d1	fotbalová
legendami	legenda	k1gFnPc7	legenda
jsou	být	k5eAaImIp3nP	být
Bobby	Bobb	k1gInPc1	Bobb
Charlton	Charlton	k1gInSc1	Charlton
a	a	k8xC	a
Bobby	Bobba	k1gFnPc1	Bobba
Moore	Moor	k1gMnSc5	Moor
<g/>
.	.	kIx.	.
</s>
<s>
Skotskou	skotský	k2eAgFnSc7d1	skotská
trenérskou	trenérský	k2eAgFnSc7d1	trenérská
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
legendou	legenda	k1gFnSc7	legenda
je	být	k5eAaImIp3nS	být
Alex	Alex	k1gMnSc1	Alex
Ferguson	Ferguson	k1gMnSc1	Ferguson
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
slavným	slavný	k2eAgMnSc7d1	slavný
předchůdcem	předchůdce	k1gMnSc7	předchůdce
v	v	k7c6	v
Manchesteru	Manchester	k1gInSc6	Manchester
United	United	k1gInSc1	United
byl	být	k5eAaImAgMnS	být
Matt	Matt	k1gMnSc1	Matt
Busby	Busba	k1gFnSc2	Busba
<g/>
.	.	kIx.	.
</s>
<s>
Držitelem	držitel	k1gMnSc7	držitel
Zlatého	zlatý	k2eAgInSc2d1	zlatý
míče	míč	k1gInSc2	míč
je	být	k5eAaImIp3nS	být
Denis	Denisa	k1gFnPc2	Denisa
Law	Law	k1gFnSc2	Law
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
skotským	skotský	k2eAgMnSc7d1	skotský
tenistou	tenista	k1gMnSc7	tenista
je	být	k5eAaImIp3nS	být
vítěz	vítěz	k1gMnSc1	vítěz
Wimbledonu	Wimbledon	k1gInSc2	Wimbledon
Andy	Anda	k1gFnSc2	Anda
Murray	Murraa	k1gFnSc2	Murraa
<g/>
.	.	kIx.	.
</s>
<s>
Trojnásobným	trojnásobný	k2eAgMnSc7d1	trojnásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
Formuli	formule	k1gFnSc6	formule
1	[number]	k4	1
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Jackie	Jackie	k1gFnSc2	Jackie
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
Jim	on	k3xPp3gMnPc3	on
Clark	Clark	k1gInSc4	Clark
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodech	závod	k1gInPc6	závod
rally	ralla	k1gFnSc2	ralla
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Colin	Colin	k1gMnSc1	Colin
McRae	McRa	k1gInSc2	McRa
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
má	mít	k5eAaImIp3nS	mít
rychlostní	rychlostní	k2eAgMnSc1d1	rychlostní
cyklista	cyklista	k1gMnSc1	cyklista
Chris	Chris	k1gFnSc2	Chris
Hoy	Hoy	k1gFnSc2	Hoy
<g/>
.	.	kIx.	.
</s>
<s>
Velšskými	velšský	k2eAgFnPc7d1	velšská
fotbalovými	fotbalový	k2eAgFnPc7d1	fotbalová
legendami	legenda	k1gFnPc7	legenda
jsou	být	k5eAaImIp3nP	být
John	John	k1gMnSc1	John
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
Ryan	Ryan	k1gMnSc1	Ryan
Giggs	Giggs	k1gInSc1	Giggs
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Hughes	Hughes	k1gMnSc1	Hughes
či	či	k8xC	či
Ian	Ian	k1gMnSc1	Ian
Rush	Rush	k1gMnSc1	Rush
<g/>
,	,	kIx,	,
ragbyovou	ragbyový	k2eAgFnSc7d1	ragbyová
legendou	legenda	k1gFnSc7	legenda
je	být	k5eAaImIp3nS	být
Gareth	Gareth	k1gInSc1	Gareth
Edwards	Edwardsa	k1gFnPc2	Edwardsa
<g/>
.	.	kIx.	.
</s>
<s>
MORGAN	morgan	k1gMnSc1	morgan
<g/>
,	,	kIx,	,
K.	K.	kA	K.
O	O	kA	O
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
432	[number]	k4	432
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgNnSc1d1	Britské
impérium	impérium	k1gNnSc1	impérium
Seznam	seznam	k1gInSc4	seznam
československých	československý	k2eAgMnPc2d1	československý
a	a	k8xC	a
českých	český	k2eAgMnPc2d1	český
vyslanců	vyslanec	k1gMnPc2	vyslanec
a	a	k8xC	a
velvyslanců	velvyslanec	k1gMnPc2	velvyslanec
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spojené	spojený	k2eAgFnSc2d1	spojená
království	království	k1gNnSc4	království
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kategorie	kategorie	k1gFnSc2	kategorie
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
gov	gov	k?	gov
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
–	–	k?	–
government	government	k1gMnSc1	government
services	services	k1gMnSc1	services
and	and	k?	and
information	information	k1gInSc1	information
<g />
.	.	kIx.	.
</s>
<s>
royal	royal	k1gInSc1	royal
<g/>
.	.	kIx.	.
<g/>
gov	gov	k?	gov
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
–	–	k?	–
The	The	k1gMnSc1	The
Official	Official	k1gMnSc1	Official
Website	Websit	k1gInSc5	Websit
of	of	k?	of
the	the	k?	the
British	British	k1gInSc1	British
Monarchy	monarcha	k1gMnSc2	monarcha
www.parliament.uk	www.parliament.uk	k1gInSc1	www.parliament.uk
–	–	k?	–
UK	UK	kA	UK
Parliament	Parliament	k1gMnSc1	Parliament
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
turistické	turistický	k2eAgFnSc2d1	turistická
informace	informace	k1gFnSc2	informace
–	–	k?	–
Official	Official	k1gMnSc1	Official
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Britain	Britain	k2eAgInSc1d1	Britain
–	–	k?	–
Visit	visita	k1gFnPc2	visita
Britain	Britain	k1gMnSc1	Britain
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Answers	Answers	k1gInSc1	Answers
–	–	k?	–
United	United	k1gInSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
velka-britanie	velkaritanie	k1gFnSc1	velka-britanie
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
-	-	kIx~	-
Zpravodajství	zpravodajství	k1gNnPc4	zpravodajství
a	a	k8xC	a
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Magazín	magazín	k1gInSc1	magazín
pro	pro	k7c4	pro
krajany	krajan	k1gMnPc4	krajan
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
UK	UK	kA	UK
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
lochneska	lochneska	k1gFnSc1	lochneska
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
praktické	praktický	k2eAgFnPc4d1	praktická
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
pohyby	pohyb	k1gInPc1	pohyb
<g/>
.	.	kIx.	.
<g/>
co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
uk	uk	k?	uk
–	–	k?	–
praktické	praktický	k2eAgFnPc4d1	praktická
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
anglie	anglie	k1gFnSc1	anglie
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
praktické	praktický	k2eAgFnPc4d1	praktická
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
United	United	k1gMnSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
–	–	k?	–
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
United	United	k1gInSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
-	-	kIx~	-
o	o	k7c6	o
českých	český	k2eAgMnPc6d1	český
krajanech	krajan	k1gMnPc6	krajan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
United	United	k1gInSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
United	United	k1gInSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
ATKINS	ATKINS	kA	ATKINS
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gInSc1	United
Kingdom	Kingdom	k1gInSc1	Kingdom
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
příručka	příručka	k1gFnSc1	příručka
na	na	k7c6	na
Wikivoyage	Wikivoyage	k1gFnSc6	Wikivoyage
</s>
