<s>
Dvouhodinový	dvouhodinový	k2eAgInSc1d1	dvouhodinový
pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
epizody	epizoda	k1gFnPc4	epizoda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nejlépe	dobře	k6eAd3	dobře
sledovaným	sledovaný	k2eAgNnSc7d1	sledované
premiérovým	premiérový	k2eAgNnSc7d1	premiérové
dílem	dílo	k1gNnSc7	dílo
seriálu	seriál	k1gInSc2	seriál
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
od	od	k7c2	od
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dívalo	dívat	k5eAaImAgNnS	dívat
přibližně	přibližně	k6eAd1	přibližně
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
