<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
Stargate	Stargat	k1gInSc5	Stargat
SG-	SG-	k1gMnSc7	SG-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zkráceně	zkráceně	k6eAd1	zkráceně
SG-	SG-	k1gFnPc1	SG-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americko-kanadský	americkoanadský	k2eAgInSc1d1	americko-kanadský
televizní	televizní	k2eAgFnSc7d1	televizní
sci-fi	scii	k1gFnSc7	sci-fi
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
franšízy	franšíza	k1gFnSc2	franšíza
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
společnosti	společnost	k1gFnSc2	společnost
Metro-Goldwyn-Mayer	Metro-Goldwyn-Mayra	k1gFnPc2	Metro-Goldwyn-Mayra
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
Bradem	Bradem	k?	Bradem
Wrightem	Wright	k1gMnSc7	Wright
a	a	k8xC	a
Jonathanem	Jonathan	k1gMnSc7	Jonathan
Glassnerem	Glassner	k1gMnSc7	Glassner
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
celovečerním	celovečerní	k2eAgInSc6d1	celovečerní
filmu	film	k1gInSc6	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
od	od	k7c2	od
Deana	Dean	k1gInSc2	Dean
Devlina	Devlina	k1gFnSc1	Devlina
a	a	k8xC	a
Rolanda	Rolanda	k1gFnSc1	Rolanda
Emmericha	Emmerich	k1gMnSc2	Emmerich
<g/>
.	.	kIx.	.
</s>
<s>
Natáčen	natáčen	k2eAgMnSc1d1	natáčen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
městě	město	k1gNnSc6	město
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1997	[number]	k4	1997
a	a	k8xC	a
2007	[number]	k4	2007
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
odvysíláno	odvysílat	k5eAaPmNgNnS	odvysílat
10	[number]	k4	10
řad	řada	k1gFnPc2	řada
(	(	kIx(	(
<g/>
prvních	první	k4xOgNnPc6	první
pět	pět	k4xCc1	pět
na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
Showtime	Showtim	k1gInSc5	Showtim
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc4d1	ostatní
na	na	k7c4	na
Sci-Fi	scii	k1gNnSc4	sci-fi
Channel	Channela	k1gFnPc2	Channela
<g/>
)	)	kIx)	)
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
počtem	počet	k1gInSc7	počet
214	[number]	k4	214
epizod	epizoda	k1gFnPc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
seriál	seriál	k1gInSc1	seriál
uveden	uvést	k5eAaPmNgInS	uvést
premiérově	premiérově	k6eAd1	premiérově
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
překonala	překonat	k5eAaPmAgFnS	překonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
počtem	počet	k1gInSc7	počet
epizod	epizoda	k1gFnPc2	epizoda
Akta	akta	k1gNnPc4	akta
X	X	kA	X
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
držela	držet	k5eAaImAgFnS	držet
titul	titul	k1gInSc4	titul
nejdéle	dlouho	k6eAd3	dlouho
vysílaného	vysílaný	k2eAgNnSc2d1	vysílané
amerického	americký	k2eAgNnSc2d1	americké
sci-fi	scii	k1gNnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
sama	sám	k3xTgFnSc1	sám
předstižena	předstižen	k2eAgFnSc1d1	předstižena
seriálem	seriál	k1gInSc7	seriál
Smallville	Smallville	k1gNnSc2	Smallville
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
začíná	začínat	k5eAaImIp3nS	začínat
přibližně	přibližně	k6eAd1	přibližně
rok	rok	k1gInSc4	rok
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
síť	síť	k1gFnSc1	síť
prastarých	prastarý	k2eAgNnPc2d1	prastaré
mimozemských	mimozemský	k2eAgNnPc2d1	mimozemské
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
pojmenovaných	pojmenovaný	k2eAgNnPc2d1	pojmenované
jako	jako	k8xC	jako
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
propojuje	propojovat	k5eAaImIp3nS	propojovat
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
téměř	téměř	k6eAd1	téměř
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
mezihvězdné	mezihvězdný	k2eAgNnSc4d1	mezihvězdné
cestování	cestování	k1gNnSc4	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc4d2	pozdější
epizody	epizoda	k1gFnPc4	epizoda
odhalí	odhalit	k5eAaPmIp3nP	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
fungovat	fungovat	k5eAaImF	fungovat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Mléčné	mléčný	k2eAgFnSc6d1	mléčná
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
při	při	k7c6	při
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
dodané	dodaný	k2eAgFnSc6d1	dodaná
energii	energie	k1gFnSc6	energie
<g/>
,	,	kIx,	,
překlenout	překlenout	k5eAaPmF	překlenout
i	i	k9	i
mezigalaktické	mezigalaktický	k2eAgFnPc4d1	mezigalaktická
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
popisuje	popisovat	k5eAaImIp3nS	popisovat
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
elitní	elitní	k2eAgFnSc2d1	elitní
speciální	speciální	k2eAgFnSc2d1	speciální
jednotky	jednotka	k1gFnSc2	jednotka
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
označené	označený	k2eAgFnSc2d1	označená
SG-	SG-	k1gFnSc2	SG-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
vlajkového	vlajkový	k2eAgInSc2d1	vlajkový
týmu	tým	k1gInSc2	tým
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvaceti	dvacet	k4xCc2	dvacet
podobných	podobný	k2eAgFnPc2d1	podobná
pozemských	pozemský	k2eAgFnPc2d1	pozemská
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prozkoumává	prozkoumávat	k5eAaImIp3nS	prozkoumávat
galaxii	galaxie	k1gFnSc4	galaxie
a	a	k8xC	a
brání	bránit	k5eAaImIp3nP	bránit
Zemi	zem	k1gFnSc4	zem
proti	proti	k7c3	proti
mimozemských	mimozemský	k2eAgFnPc2d1	mimozemská
hrozbám	hrozba	k1gFnPc3	hrozba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldi	uld	k1gMnPc1	uld
<g/>
,	,	kIx,	,
Replikátoři	Replikátor	k1gMnPc1	Replikátor
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Oriové	Oriové	k2eAgFnSc1d1	Oriové
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
týmu	tým	k1gInSc2	tým
SG-1	SG-1	k1gFnSc2	SG-1
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
pěti	pět	k4xCc6	pět
sezónách	sezóna	k1gFnPc6	sezóna
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
řadách	řada	k1gFnPc6	řada
však	však	k9	však
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
několika	několik	k4yIc3	několik
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
také	také	k9	také
využívá	využívat	k5eAaImIp3nS	využívat
a	a	k8xC	a
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
některá	některý	k3yIgNnPc4	některý
náboženství	náboženství	k1gNnPc4	náboženství
ze	z	k7c2	z
starověké	starověký	k2eAgFnSc2d1	starověká
i	i	k8xC	i
středověké	středověký	k2eAgFnSc2d1	středověká
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
egyptské	egyptský	k2eAgFnSc2d1	egyptská
a	a	k8xC	a
severské	severský	k2eAgFnSc2d1	severská
mytologie	mytologie	k1gFnSc2	mytologie
či	či	k8xC	či
artušovských	artušovský	k2eAgFnPc2d1	artušovská
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
kanálech	kanál	k1gInPc6	kanál
Showtime	Showtim	k1gInSc5	Showtim
a	a	k8xC	a
Sci-Fi	scii	k1gFnPc4	sci-fi
Channel	Channlo	k1gNnPc2	Channlo
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
sledovanost	sledovanost	k1gFnSc4	sledovanost
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
populární	populární	k2eAgFnSc1d1	populární
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
některé	některý	k3yIgFnPc4	některý
menší	malý	k2eAgFnPc4d2	menší
negativní	negativní	k2eAgFnPc4d1	negativní
kritické	kritický	k2eAgFnPc4d1	kritická
odezvy	odezva	k1gFnPc4	odezva
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svých	svůj	k3xOyFgMnPc2	svůj
deseti	deset	k4xCc2	deset
sezón	sezóna	k1gFnPc2	sezóna
oceněn	ocenit	k5eAaPmNgInS	ocenit
řadou	řada	k1gFnSc7	řada
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
úspěchu	úspěch	k1gInSc3	úspěch
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
další	další	k2eAgInPc1d1	další
seriály	seriál	k1gInPc1	seriál
franšízy	franšíza	k1gFnSc2	franšíza
<g/>
:	:	kIx,	:
Stargate	Stargat	k1gInSc5	Stargat
Infinity	Infinit	k1gInPc1	Infinit
(	(	kIx(	(
<g/>
animovaný	animovaný	k2eAgMnSc1d1	animovaný
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
(	(	kIx(	(
<g/>
spin-off	spinff	k1gInSc1	spin-off
volně	volně	k6eAd1	volně
navazující	navazující	k2eAgNnSc4d1	navazující
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
a	a	k8xC	a
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
také	také	k9	také
přímo	přímo	k6eAd1	přímo
navazují	navazovat	k5eAaImIp3nP	navazovat
dva	dva	k4xCgInPc1	dva
DVD	DVD	kA	DVD
filmy	film	k1gInPc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
–	–	k?	–
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Archa	archa	k1gFnSc1	archa
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
odvozená	odvozený	k2eAgNnPc4d1	odvozené
díla	dílo	k1gNnPc4	dílo
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
seriálu	seriál	k1gInSc2	seriál
patří	patřit	k5eAaImIp3nP	patřit
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
hračky	hračka	k1gFnPc1	hračka
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
audioknihy	audioknih	k1gInPc1	audioknih
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
Mytologie	mytologie	k1gFnSc1	mytologie
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
ději	děj	k1gInSc6	děj
původního	původní	k2eAgInSc2d1	původní
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
a	a	k8xC	a
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
dobrodružství	dobrodružství	k1gNnSc3	dobrodružství
SG-	SG-	k1gFnSc7	SG-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
vojenského	vojenský	k2eAgInSc2d1	vojenský
týmu	tým	k1gInSc2	tým
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
SG-1	SG-1	k4	SG-1
a	a	k8xC	a
24	[number]	k4	24
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
sestavených	sestavený	k2eAgInPc2d1	sestavený
SG	SG	kA	SG
týmů	tým	k1gInPc2	tým
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c4	na
vzdálené	vzdálený	k2eAgFnPc4d1	vzdálená
planety	planeta	k1gFnPc4	planeta
pomocí	pomocí	k7c2	pomocí
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
portálu	portál	k1gInSc2	portál
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kruhu	kruh	k1gInSc2	kruh
známého	známý	k2eAgInSc2d1	známý
jako	jako	k8xC	jako
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
přísně	přísně	k6eAd1	přísně
tajné	tajný	k2eAgFnSc6d1	tajná
vojenské	vojenský	k2eAgFnSc6d1	vojenská
základně	základna	k1gFnSc6	základna
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
označené	označený	k2eAgNnSc1d1	označené
jako	jako	k8xC	jako
Velitelství	velitelství	k1gNnSc1	velitelství
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Stargate	Stargat	k1gInSc5	Stargat
Command	Commanda	k1gFnPc2	Commanda
<g/>
,	,	kIx,	,
zkratkou	zkratka	k1gFnSc7	zkratka
SGC	SGC	kA	SGC
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
Mountain	Mountain	k1gMnSc1	Mountain
u	u	k7c2	u
města	město	k1gNnSc2	město
Colorado	Colorado	k1gNnSc1	Colorado
Springs	Springs	k1gInSc1	Springs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
osmi	osm	k4xCc6	osm
sezónách	sezóna	k1gFnPc6	sezóna
se	se	k3xPyFc4	se
SG	SG	kA	SG
týmy	tým	k1gInPc1	tým
vydávají	vydávat	k5eAaPmIp3nP	vydávat
na	na	k7c4	na
mise	mise	k1gFnPc4	mise
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
galaxii	galaxie	k1gFnSc4	galaxie
a	a	k8xC	a
hledat	hledat	k5eAaImF	hledat
mimozemské	mimozemský	k2eAgFnPc4d1	mimozemská
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
spojence	spojenec	k1gMnPc4	spojenec
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
Země	zem	k1gFnSc2	zem
před	před	k7c7	před
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldy	ulda	k1gFnPc1	ulda
<g/>
,	,	kIx,	,
hadům	had	k1gMnPc3	had
podobné	podobný	k2eAgFnSc2d1	podobná
parazitické	parazitický	k2eAgFnSc3d1	parazitická
mimozemské	mimozemský	k2eAgFnSc3d1	mimozemská
rase	rasa	k1gFnSc3	rasa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
lidi	člověk	k1gMnPc4	člověk
jako	jako	k8xS	jako
nedobrovolné	dobrovolný	k2eNgMnPc4d1	nedobrovolný
hostitele	hostitel	k1gMnPc4	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
vysvětleno	vysvětlit	k5eAaPmNgNnS	vysvětlit
<g/>
,	,	kIx,	,
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldi	uldi	k6eAd1	uldi
přepravovali	přepravovat	k5eAaImAgMnP	přepravovat
před	před	k7c7	před
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
lidské	lidský	k2eAgMnPc4d1	lidský
otroky	otrok	k1gMnPc4	otrok
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
obyvatelné	obyvatelný	k2eAgFnPc4d1	obyvatelná
planety	planeta	k1gFnPc4	planeta
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
galaxii	galaxie	k1gFnSc6	galaxie
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
představují	představovat	k5eAaImIp3nP	představovat
jako	jako	k9	jako
bohové	bůh	k1gMnPc1	bůh
pradávných	pradávný	k2eAgNnPc2d1	pradávné
pozemských	pozemský	k2eAgNnPc2d1	pozemské
náboženství	náboženství	k1gNnPc2	náboženství
včetně	včetně	k7c2	včetně
egyptské	egyptský	k2eAgFnSc2d1	egyptská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
SG-1	SG-1	k4	SG-1
také	také	k6eAd1	také
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
síť	síť	k1gFnSc1	síť
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
bran	brána	k1gFnPc2	brána
postavili	postavit	k5eAaPmAgMnP	postavit
před	před	k7c7	před
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
takzvaní	takzvaný	k2eAgMnPc1d1	takzvaný
Antikové	Antik	k1gMnPc1	Antik
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Ancients	Ancients	k1gInSc1	Ancients
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysoce	vysoce	k6eAd1	vysoce
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
rasa	rasa	k1gFnSc1	rasa
podobná	podobný	k2eAgFnSc1d1	podobná
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
Antikové	Antik	k1gMnPc1	Antik
využili	využít	k5eAaPmAgMnP	využít
svých	svůj	k3xOyFgFnPc2	svůj
mimořádných	mimořádný	k2eAgFnPc2d1	mimořádná
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
povznesli	povznést	k5eAaPmAgMnP	povznést
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
úroveň	úroveň	k1gFnSc4	úroveň
existence	existence	k1gFnSc2	existence
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zavázali	zavázat	k5eAaPmAgMnP	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebudou	být	k5eNaImBp3nP	být
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
života	život	k1gInSc2	život
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Oriové	Orius	k1gMnPc1	Orius
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
ke	k	k7c3	k
stejné	stejný	k2eAgFnSc3d1	stejná
rase	rasa	k1gFnSc3	rasa
povznesených	povznesený	k2eAgFnPc2d1	povznesená
bytostí	bytost	k1gFnPc2	bytost
jako	jako	k8xC	jako
Antikové	Antikový	k2eAgFnPc1d1	Antikový
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaImIp3nP	využívat
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
k	k	k7c3	k
podrobení	podrobení	k1gNnSc3	podrobení
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nutí	nutit	k5eAaImIp3nP	nutit
k	k	k7c3	k
uctívání	uctívání	k1gNnSc3	uctívání
náboženství	náboženství	k1gNnSc2	náboženství
Ori	Ori	k1gFnSc2	Ori
a	a	k8xC	a
k	k	k7c3	k
víře	víra	k1gFnSc3	víra
v	v	k7c4	v
náboženský	náboženský	k2eAgInSc4d1	náboženský
fundamentalismus	fundamentalismus	k1gInSc4	fundamentalismus
<g/>
.	.	kIx.	.
</s>
<s>
Oriové	Orius	k1gMnPc1	Orius
proto	proto	k8xC	proto
převezmou	převzít	k5eAaPmIp3nP	převzít
roli	role	k1gFnSc4	role
hlavního	hlavní	k2eAgMnSc2d1	hlavní
padoucha	padouch	k1gMnSc2	padouch
v	v	k7c6	v
deváté	devátý	k4xOgFnSc6	devátý
a	a	k8xC	a
desáté	desátý	k4xOgFnSc3	desátý
řadě	řada	k1gFnSc3	řada
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldi	uldi	k1gNnSc1	uldi
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgFnSc1d1	pilotní
epizoda	epizoda	k1gFnSc1	epizoda
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
rok	rok	k1gInSc1	rok
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
filmu	film	k1gInSc2	film
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
na	na	k7c4	na
obrazovky	obrazovka	k1gFnPc4	obrazovka
goa	goa	k?	goa
<g/>
'	'	kIx"	'
<g/>
uldského	uldský	k1gMnSc2	uldský
vládce	vládce	k1gMnSc2	vládce
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
hlavního	hlavní	k2eAgMnSc2d1	hlavní
nepřítele	nepřítel	k1gMnSc2	nepřítel
Apophise	Apophise	k1gFnSc2	Apophise
(	(	kIx(	(
<g/>
Peter	Peter	k1gMnSc1	Peter
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
skrz	skrz	k7c4	skrz
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
na	na	k7c4	na
zakonzervovanou	zakonzervovaný	k2eAgFnSc4d1	zakonzervovaná
základnu	základna	k1gFnSc4	základna
SGC	SGC	kA	SGC
a	a	k8xC	a
unese	unést	k5eAaPmIp3nS	unést
vojáka	voják	k1gMnSc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Velitelství	velitelství	k1gNnSc1	velitelství
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
do	do	k7c2	do
aktivní	aktivní	k2eAgFnSc2d1	aktivní
služby	služba	k1gFnSc2	služba
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
meziplanetární	meziplanetární	k2eAgFnSc2d1	meziplanetární
sítě	síť	k1gFnSc2	síť
propojující	propojující	k2eAgFnSc1d1	propojující
červí	červí	k2eAgFnSc7d1	červí
dírou	díra	k1gFnSc7	díra
bezpočet	bezpočet	k1gInSc1	bezpočet
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
Země	zem	k1gFnSc2	zem
proti	proti	k7c3	proti
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldům	uld	k1gMnPc3	uld
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
disponují	disponovat	k5eAaBmIp3nP	disponovat
mezihvězdnými	mezihvězdný	k2eAgFnPc7d1	mezihvězdná
loděmi	loď	k1gFnPc7	loď
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pyramid	pyramida	k1gFnPc2	pyramida
a	a	k8xC	a
obrovskými	obrovský	k2eAgFnPc7d1	obrovská
armádami	armáda	k1gFnPc7	armáda
Jaffů	Jaff	k1gMnPc2	Jaff
<g/>
,	,	kIx,	,
dědičných	dědičný	k2eAgMnPc2d1	dědičný
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
lidských	lidský	k2eAgInPc2d1	lidský
inkubátorů	inkubátor	k1gInPc2	inkubátor
pro	pro	k7c4	pro
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldy	ulda	k1gFnPc1	ulda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vytvořeny	vytvořen	k2eAgInPc4d1	vytvořen
tzv.	tzv.	kA	tzv.
SG	SG	kA	SG
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkový	vlajkový	k2eAgInSc1d1	vlajkový
tým	tým	k1gInSc1	tým
SG-	SG-	k1gFnSc1	SG-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
členem	člen	k1gMnSc7	člen
je	být	k5eAaImIp3nS	být
i	i	k9	i
přeběhlík	přeběhlík	k1gMnSc1	přeběhlík
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Apophisův	Apophisův	k2eAgMnSc1d1	Apophisův
první	první	k4xOgMnSc1	první
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
založí	založit	k5eAaPmIp3nP	založit
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
několik	několik	k4yIc4	několik
aliancí	aliance	k1gFnPc2	aliance
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
kulturami	kultura	k1gFnPc7	kultura
i	i	k8xC	i
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
Tok	tok	k1gInSc1	tok
<g/>
'	'	kIx"	'
<g/>
rové	rové	k1gNnSc1	rové
<g/>
,	,	kIx,	,
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldům	uld	k1gInPc3	uld
podobná	podobný	k2eAgNnPc4d1	podobné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skutečně	skutečně	k6eAd1	skutečně
symbiotická	symbiotický	k2eAgFnSc1d1	symbiotická
rasa	rasa	k1gFnSc1	rasa
<g/>
,	,	kIx,	,
vyspělí	vyspělý	k2eAgMnPc1d1	vyspělý
lidé	člověk	k1gMnPc1	člověk
Tolláni	Tollán	k2eAgMnPc1d1	Tollán
<g/>
,	,	kIx,	,
pacifističtí	pacifistický	k2eAgMnPc1d1	pacifistický
Noxové	Noxus	k1gMnPc1	Noxus
<g/>
,	,	kIx,	,
laskaví	laskavý	k2eAgMnPc1d1	laskavý
Asgardi	Asgard	k1gMnPc1	Asgard
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
mimozemšťané	mimozemšťan	k1gMnPc1	mimozemšťan
z	z	k7c2	z
Roswellu	Roswell	k1gInSc2	Roswell
<g/>
,	,	kIx,	,
a	a	k8xC	a
zbylí	zbylý	k2eAgMnPc1d1	zbylý
mocní	mocný	k2eAgMnPc1d1	mocný
Antikové	Antik	k1gMnPc1	Antik
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
mimozemská	mimozemský	k2eAgFnSc1d1	mimozemská
hrozba	hrozba	k1gFnSc1	hrozba
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
ve	v	k7c6	v
finální	finální	k2eAgFnSc6d1	finální
epizodě	epizoda	k1gFnSc6	epizoda
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
"	"	kIx"	"
<g/>
Nemesis	Nemesis	k1gFnSc1	Nemesis
<g/>
"	"	kIx"	"
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vnímajících	vnímající	k2eAgInPc2d1	vnímající
strojů	stroj	k1gInPc2	stroj
zvaných	zvaný	k2eAgInPc2d1	zvaný
Replikátoři	Replikátor	k1gMnPc5	Replikátor
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
odpadlí	odpadlý	k2eAgMnPc1d1	odpadlý
agenti	agent	k1gMnPc1	agent
kontroverzní	kontroverzní	k2eAgFnSc2d1	kontroverzní
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
NID	NID	kA	NID
opakovaně	opakovaně	k6eAd1	opakovaně
pokusí	pokusit	k5eAaPmIp3nS	pokusit
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
převzít	převzít	k5eAaPmF	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
bránou	brána	k1gFnSc7	brána
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
mimozemskou	mimozemský	k2eAgFnSc7d1	mimozemská
technologií	technologie	k1gFnSc7	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Apophis	Apophis	k1gFnSc1	Apophis
zemře	zemřít	k5eAaPmIp3nS	zemřít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
páté	pátý	k4xOgFnSc2	pátý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
goa	goa	k?	goa
<g/>
'	'	kIx"	'
<g/>
uldské	uldský	k2eAgNnSc4d1	uldský
impérium	impérium	k1gNnSc4	impérium
hlavním	hlavní	k2eAgMnSc7d1	hlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
až	až	k8xS	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
dvou	dva	k4xCgFnPc6	dva
sezónách	sezóna	k1gFnPc6	sezóna
seriálu	seriál	k1gInSc2	seriál
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jediným	jediné	k1gNnSc7	jediné
vlivným	vlivný	k2eAgNnSc7d1	vlivné
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldem	uldem	k6eAd1	uldem
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
vládce	vládce	k1gMnSc1	vládce
soustavy	soustava	k1gFnSc2	soustava
Baal	Baal	k1gMnSc1	Baal
(	(	kIx(	(
<g/>
Cliff	Cliff	k1gMnSc1	Cliff
Simon	Simon	k1gMnSc1	Simon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
definitivně	definitivně	k6eAd1	definitivně
poražen	porazit	k5eAaPmNgInS	porazit
v	v	k7c6	v
DVD	DVD	kA	DVD
filmu	film	k1gInSc6	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Anubis	Anubis	k1gFnSc2	Anubis
(	(	kIx(	(
<g/>
goa	goa	k?	goa
<g/>
'	'	kIx"	'
<g/>
uld	uld	k?	uld
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Apophisově	Apophisův	k2eAgFnSc6d1	Apophisova
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
první	první	k4xOgFnSc6	první
epizodě	epizoda	k1gFnSc6	epizoda
páté	pátá	k1gFnSc2	pátá
řady	řada	k1gFnSc2	řada
"	"	kIx"	"
<g/>
Nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
"	"	kIx"	"
se	s	k7c7	s
hlavním	hlavní	k2eAgMnSc7d1	hlavní
padouchem	padouch	k1gMnSc7	padouch
stane	stanout	k5eAaPmIp3nS	stanout
napůl	napůl	k6eAd1	napůl
povznesený	povznesený	k2eAgInSc1d1	povznesený
goa	goa	k?	goa
<g/>
'	'	kIx"	'
<g/>
uldský	uldský	k1gMnSc1	uldský
vládce	vládce	k1gMnSc2	vládce
soustavy	soustava	k1gFnSc2	soustava
Anubis	Anubis	k1gFnSc1	Anubis
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Palffy	Palff	k1gMnPc7	Palff
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
znalostí	znalost	k1gFnPc2	znalost
Antiků	Antik	k1gInPc2	Antik
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc4	jejich
technologii	technologie	k1gFnSc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Země	země	k1gFnSc1	země
staví	stavit	k5eAaImIp3nS	stavit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
šesté	šestý	k4xOgFnSc2	šestý
a	a	k8xC	a
sedmé	sedmý	k4xOgFnSc2	sedmý
sezóny	sezóna	k1gFnSc2	sezóna
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
mezihvězdnou	mezihvězdný	k2eAgFnSc4d1	mezihvězdná
loď	loď	k1gFnSc4	loď
Prometheus	Prometheus	k1gMnSc1	Prometheus
<g/>
,	,	kIx,	,
Anubis	Anubis	k1gFnSc1	Anubis
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
armádu	armáda	k1gFnSc4	armáda
téměř	téměř	k6eAd1	téměř
neporazitelných	porazitelný	k2eNgFnPc2d1	neporazitelná
Kull	Kulla	k1gFnPc2	Kulla
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yRgFnPc3	který
smete	smést	k5eAaPmIp3nS	smést
mnoho	mnoho	k4c1	mnoho
ostatních	ostatní	k2eAgMnPc2d1	ostatní
vládců	vládce	k1gMnPc2	vládce
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
sedmé	sedmý	k4xOgFnSc2	sedmý
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
dvojepizodě	dvojepizodě	k6eAd1	dvojepizodě
"	"	kIx"	"
<g/>
Ztracené	ztracený	k2eAgNnSc1d1	ztracené
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nS	objevit
SG-1	SG-1	k1gMnSc1	SG-1
mocnou	mocný	k2eAgFnSc4d1	mocná
zbraň	zbraň	k1gFnSc4	zbraň
v	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
základně	základna	k1gFnSc6	základna
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
které	který	k3yRgFnSc2	který
zničí	zničit	k5eAaPmIp3nS	zničit
celou	celý	k2eAgFnSc4d1	celá
Anubisovu	Anubisův	k2eAgFnSc4d1	Anubisova
flotilu	flotila	k1gFnSc4	flotila
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
a	a	k8xC	a
úvodní	úvodní	k2eAgInSc4d1	úvodní
dvojdíl	dvojdíl	k1gInSc4	dvojdíl
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
"	"	kIx"	"
<g/>
Časy	čas	k1gInPc1	čas
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spin-off	spinff	k1gInSc1	spin-off
seriál	seriál	k1gInSc1	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
osmou	osmý	k4xOgFnSc7	osmý
až	až	k8xS	až
desátou	desátý	k4xOgFnSc7	desátý
řadou	řada	k1gFnSc7	řada
Hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
brány	brána	k1gFnPc1	brána
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ba	ba	k9	ba
<g/>
'	'	kIx"	'
<g/>
al	ala	k1gFnPc2	ala
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
osmé	osmý	k4xOgFnSc2	osmý
sezóny	sezóna	k1gFnSc2	sezóna
získá	získat	k5eAaPmIp3nS	získat
část	část	k1gFnSc1	část
Anubisovy	Anubisův	k2eAgFnSc2d1	Anubisova
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ten	ten	k3xDgMnSc1	ten
tajně	tajně	k6eAd1	tajně
získává	získávat	k5eAaImIp3nS	získávat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c4	nad
Ba	ba	k9	ba
<g/>
'	'	kIx"	'
<g/>
alovými	alový	k2eAgFnPc7d1	alový
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vládce	vládce	k1gMnSc4	vládce
soustavy	soustava	k1gFnSc2	soustava
také	také	k9	také
začnou	začít	k5eAaPmIp3nP	začít
útočit	útočit	k5eAaImF	útočit
Replikátoři	Replikátor	k1gMnPc1	Replikátor
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
formě	forma	k1gFnSc6	forma
snažící	snažící	k2eAgFnPc1d1	snažící
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
ovládnout	ovládnout	k5eAaPmF	ovládnout
galaxii	galaxie	k1gFnSc4	galaxie
<g/>
,	,	kIx,	,
SG-1	SG-1	k1gFnSc4	SG-1
ale	ale	k8xC	ale
nalezne	naleznout	k5eAaPmIp3nS	naleznout
a	a	k8xC	a
upraví	upravit	k5eAaPmIp3nS	upravit
antickou	antický	k2eAgFnSc4d1	antická
zbraň	zbraň	k1gFnSc4	zbraň
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
Replikátory	replikátor	k1gInPc1	replikátor
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Mléčné	mléčný	k2eAgFnSc6d1	mléčná
dráze	dráha	k1gFnSc6	dráha
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
koncem	konec	k1gInSc7	konec
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Osobní	osobní	k2eAgFnPc4d1	osobní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
původní	původní	k2eAgNnSc4d1	původní
Anubisovo	Anubisův	k2eAgNnSc4d1	Anubisův
povznesení	povznesení	k1gNnSc4	povznesení
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
laskavá	laskavý	k2eAgFnSc1d1	laskavá
Oma	Oma	k1gFnSc1	Oma
Desala	Desala	k1gFnSc1	Desala
(	(	kIx(	(
<g/>
Mel	mlít	k5eAaImRp2nS	mlít
Harris	Harris	k1gInSc1	Harris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povznesená	povznesený	k2eAgFnSc1d1	povznesená
Antička	Antička	k1gFnSc1	Antička
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
rozhodně	rozhodně	k6eAd1	rozhodně
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
a	a	k8xC	a
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
souboje	souboj	k1gInSc2	souboj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uvázne	uváznout	k5eAaPmIp3nS	uváznout
na	na	k7c6	na
věčném	věčný	k2eAgInSc6d1	věčný
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
zabrání	zabránit	k5eAaPmIp3nS	zabránit
existovat	existovat	k5eAaImF	existovat
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
už	už	k9	už
většina	většina	k1gFnSc1	většina
vládců	vládce	k1gMnPc2	vládce
soustav	soustava	k1gFnPc2	soustava
i	i	k9	i
Replikátoři	Replikátor	k1gMnPc1	Replikátor
zničeni	zničen	k2eAgMnPc1d1	zničen
a	a	k8xC	a
Jaffové	Jaff	k1gMnPc1	Jaff
získávají	získávat	k5eAaImIp3nP	získávat
svobodu	svoboda	k1gFnSc4	svoboda
nad	nad	k7c7	nad
goa	goa	k?	goa
<g/>
'	'	kIx"	'
<g/>
uldskou	uldský	k2eAgFnSc7d1	uldská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Oriové	Oriová	k1gFnSc2	Oriová
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
tým	tým	k1gInSc1	tým
SG-1	SG-1	k1gFnSc2	SG-1
se	se	k3xPyFc4	se
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
deváté	devátý	k4xOgFnSc2	devátý
sezóny	sezóna	k1gFnSc2	sezóna
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
částečně	částečně	k6eAd1	částečně
obnoven	obnoven	k2eAgInSc1d1	obnoven
pod	pod	k7c7	pod
novým	nový	k2eAgMnSc7d1	nový
velitelem	velitel	k1gMnSc7	velitel
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
SGC	SGC	kA	SGC
neúmyslně	úmyslně	k6eNd1	úmyslně
upozorní	upozornit	k5eAaPmIp3nP	upozornit
Orie	Ori	k1gFnPc1	Ori
<g/>
,	,	kIx,	,
bytosti	bytost	k1gFnPc1	bytost
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
galaxie	galaxie	k1gFnSc2	galaxie
podobné	podobný	k2eAgFnSc2d1	podobná
Antikům	Antik	k1gMnPc3	Antik
<g/>
,	,	kIx,	,
na	na	k7c4	na
existenci	existence	k1gFnSc4	existence
vnímajícího	vnímající	k2eAgInSc2d1	vnímající
života	život	k1gInSc2	život
v	v	k7c6	v
Mléčné	mléčný	k2eAgFnSc6d1	mléčná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Oriové	Orius	k1gMnPc1	Orius
vyšlou	vyslat	k5eAaPmIp3nP	vyslat
do	do	k7c2	do
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
vylepšené	vylepšený	k2eAgFnSc2d1	vylepšená
lidské	lidský	k2eAgFnSc2d1	lidská
bytosti	bytost	k1gFnSc2	bytost
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Převorové	převor	k1gMnPc1	převor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
šířily	šířit	k5eAaImAgFnP	šířit
jejich	jejich	k3xOp3gFnSc4	jejich
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
síla	síla	k1gFnSc1	síla
Oriů	Ori	k1gMnPc2	Ori
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
<g/>
,	,	kIx,	,
Ba	ba	k9	ba
<g/>
'	'	kIx"	'
<g/>
al	ala	k1gFnPc2	ala
a	a	k8xC	a
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldů	uld	k1gMnPc2	uld
infiltruje	infiltrovat	k5eAaBmIp3nS	infiltrovat
Zemi	zem	k1gFnSc4	zem
přes	přes	k7c4	přes
Společnost	společnost	k1gFnSc4	společnost
(	(	kIx(	(
<g/>
koalici	koalice	k1gFnSc4	koalice
s	s	k7c7	s
odpadlými	odpadlý	k2eAgInPc7d1	odpadlý
agenty	agens	k1gInPc7	agens
NID	NID	kA	NID
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
znovu	znovu	k6eAd1	znovu
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
svoji	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
deváté	devátý	k4xOgFnSc2	devátý
řady	řada	k1gFnSc2	řada
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kamelot	kamelot	k1gInSc1	kamelot
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
zahájí	zahájit	k5eAaPmIp3nS	zahájit
Oriové	Oriové	k2eAgFnSc1d1	Oriové
s	s	k7c7	s
válečnými	válečný	k2eAgFnPc7d1	válečná
loděmi	loď	k1gFnPc7	loď
svoji	svůj	k3xOyFgFnSc4	svůj
evangelizační	evangelizační	k2eAgFnSc4d1	evangelizační
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bez	bez	k7c2	bez
námahy	námaha	k1gFnSc2	námaha
porazí	porazit	k5eAaPmIp3nS	porazit
flotilu	flotila	k1gFnSc4	flotila
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
jejích	její	k3xOp3gMnPc2	její
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
Oriů	Ori	k1gMnPc2	Ori
Adria	Adria	k1gFnSc1	Adria
(	(	kIx(	(
<g/>
Morena	Morena	k1gFnSc1	Morena
Baccarin	Baccarin	k1gInSc1	Baccarin
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
dílu	díl	k1gInSc6	díl
desáté	desátá	k1gFnSc2	desátá
řady	řada	k1gFnSc2	řada
"	"	kIx"	"
<g/>
Z	z	k7c2	z
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
kostí	kost	k1gFnPc2	kost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
SG-1	SG-1	k4	SG-1
hledá	hledat	k5eAaImIp3nS	hledat
Sangraal	Sangraal	k1gInSc4	Sangraal
<g/>
,	,	kIx,	,
antickou	antický	k2eAgFnSc4d1	antická
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
porazit	porazit	k5eAaPmF	porazit
Orie	Orie	k1gFnSc1	Orie
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
Ba	ba	k9	ba
<g/>
'	'	kIx"	'
<g/>
al	ala	k1gFnPc2	ala
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
klony	klon	k1gInPc7	klon
pátrá	pátrat	k5eAaImIp3nS	pátrat
po	po	k7c6	po
téže	tenže	k3xDgFnSc6	tenže
zbrani	zbraň	k1gFnSc6	zbraň
z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
mocného	mocný	k2eAgNnSc2d1	mocné
Antika	antika	k1gFnSc1	antika
Merlina	Merlina	k1gFnSc1	Merlina
(	(	kIx(	(
<g/>
Matthew	Matthew	k1gMnSc1	Matthew
Walker	Walker	k1gMnSc1	Walker
<g/>
)	)	kIx)	)
najde	najít	k5eAaPmIp3nS	najít
SG-1	SG-1	k1gMnSc1	SG-1
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
plány	plán	k1gInPc4	plán
k	k	k7c3	k
Sangraalu	Sangraal	k1gInSc3	Sangraal
a	a	k8xC	a
pošle	poslat	k5eAaPmIp3nS	poslat
zařízení	zařízení	k1gNnSc4	zařízení
do	do	k7c2	do
orijské	orijský	k2eAgFnSc2d1	orijský
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Adria	Adria	k1gFnSc1	Adria
povznese	povznést	k5eAaPmIp3nS	povznést
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
v	v	k7c6	v
DVD	DVD	kA	DVD
filmu	film	k1gInSc6	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Archa	archa	k1gFnSc1	archa
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
postav	postava	k1gFnPc2	postava
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Dean	Dean	k1gMnSc1	Dean
Anderson	Anderson	k1gMnSc1	Anderson
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Miloslav	Miloslav	k1gMnSc1	Miloslav
Mejzlík	Mejzlík	k1gMnSc1	Mejzlík
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Jonathan	Jonathan	k1gMnSc1	Jonathan
"	"	kIx"	"
<g/>
Jack	Jack	k1gMnSc1	Jack
<g/>
"	"	kIx"	"
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neill	Neill	k1gMnSc1	Neill
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
host	host	k1gMnSc1	host
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Plukovník	plukovník	k1gMnSc1	plukovník
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
veterán	veterán	k1gMnSc1	veterán
speciálních	speciální	k2eAgFnPc2d1	speciální
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vedl	vést	k5eAaImAgInS	vést
původní	původní	k2eAgFnSc4d1	původní
misi	mise	k1gFnSc4	mise
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
branou	brána	k1gFnSc7	brána
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Kurt	Kurt	k1gMnSc1	Kurt
Russell	Russell	k1gMnSc1	Russell
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
výslužby	výslužba	k1gFnSc2	výslužba
povolán	povolat	k5eAaPmNgMnS	povolat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
aktivní	aktivní	k2eAgFnSc2d1	aktivní
služby	služba	k1gFnSc2	služba
a	a	k8xC	a
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
sedmi	sedm	k4xCc6	sedm
sezónách	sezóna	k1gFnPc6	sezóna
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
týmu	tým	k1gInSc2	tým
SG-	SG-	k1gFnSc2	SG-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
brigádního	brigádní	k2eAgMnSc4d1	brigádní
generála	generál	k1gMnSc4	generál
a	a	k8xC	a
převezme	převézt	k5eAaPmRp1nP	převézt
velení	velení	k1gNnSc4	velení
na	na	k7c6	na
Velitelství	velitelství	k1gNnSc6	velitelství
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
opakovaně	opakovaně	k6eAd1	opakovaně
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
romantické	romantický	k2eAgInPc4d1	romantický
city	cit	k1gInPc4	cit
mezi	mezi	k7c7	mezi
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neillem	Neill	k1gMnSc7	Neill
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
zástupcem	zástupce	k1gMnSc7	zástupce
Carterovou	Carterův	k2eAgFnSc7d1	Carterova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
příběhy	příběh	k1gInPc4	příběh
z	z	k7c2	z
alternativní	alternativní	k2eAgFnSc2d1	alternativní
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
naplněn	naplněn	k2eAgMnSc1d1	naplněn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neill	Neill	k1gInSc4	Neill
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
deváté	devátý	k4xOgFnSc2	devátý
řady	řada	k1gFnSc2	řada
převelen	převelen	k2eAgInSc4d1	převelen
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc2	D.C.
a	a	k8xC	a
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
generálmajora	generálmajor	k1gMnSc4	generálmajor
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
epizodách	epizoda	k1gFnPc6	epizoda
deváté	devátý	k4xOgFnSc2	devátý
a	a	k8xC	a
desáté	desátý	k4xOgFnSc2	desátý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Návrat	návrat	k1gInSc1	návrat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
dílech	díl	k1gInPc6	díl
seriálu	seriál	k1gInSc2	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
a	a	k8xC	a
jako	jako	k9	jako
generálporučík	generálporučík	k1gMnSc1	generálporučík
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
Obrany	obrana	k1gFnSc2	obrana
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
šesti	šest	k4xCc6	šest
epizodách	epizoda	k1gFnPc6	epizoda
seriálu	seriál	k1gInSc6	seriál
Hluboký	hluboký	k2eAgInSc4d1	hluboký
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Shanks	Shanks	k1gInSc1	Shanks
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Prachař	Prachař	k1gMnSc1	Prachař
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Daniel	Daniel	k1gMnSc1	Daniel
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
host	host	k1gMnSc1	host
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Vynikající	vynikající	k2eAgMnSc1d1	vynikající
egyptolog	egyptolog	k1gMnSc1	egyptolog
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
nepravděpodobné	pravděpodobný	k2eNgFnPc1d1	nepravděpodobná
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
egyptských	egyptský	k2eAgFnPc6d1	egyptská
pyramidách	pyramida	k1gFnPc6	pyramida
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
pode	pod	k7c4	pod
něj	on	k3xPp3gInSc4	on
postaveny	postaven	k2eAgMnPc4d1	postaven
mimozemšťany	mimozemšťan	k1gMnPc4	mimozemšťan
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
účasti	účast	k1gFnSc3	účast
na	na	k7c4	na
první	první	k4xOgFnSc4	první
misi	mise	k1gFnSc4	mise
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
branou	brána	k1gFnSc7	brána
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
filmu	film	k1gInSc6	film
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
hraje	hrát	k5eAaImIp3nS	hrát
James	James	k1gMnSc1	James
Spader	Spader	k1gMnSc1	Spader
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
SG-	SG-	k1gFnPc2	SG-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
najít	najít	k5eAaPmF	najít
svoji	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
unesenou	unesený	k2eAgFnSc4d1	unesená
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
Apophisem	Apophis	k1gInSc7	Apophis
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
naivita	naivita	k1gFnSc1	naivita
a	a	k8xC	a
zvědavost	zvědavost	k1gFnSc1	zvědavost
často	často	k6eAd1	často
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pro	pro	k7c4	pro
jednotku	jednotka	k1gFnSc4	jednotka
řadu	řada	k1gFnSc4	řada
překážek	překážka	k1gFnPc2	překážka
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
z	z	k7c2	z
archeologa	archeolog	k1gMnSc2	archeolog
a	a	k8xC	a
překladatele	překladatel	k1gMnSc2	překladatel
v	v	k7c6	v
morální	morální	k2eAgNnSc4d1	morální
svědomí	svědomí	k1gNnSc4	svědomí
týmu	tým	k1gInSc2	tým
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
povznesení	povznesení	k1gNnSc2	povznesení
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
úroveň	úroveň	k1gFnSc4	úroveň
bytí	bytí	k1gNnSc2	bytí
na	na	k7c6	na
konci	konec	k1gInSc6	konec
páté	pátý	k4xOgFnSc2	pátý
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
šesté	šestý	k4xOgFnSc2	šestý
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
objeví	objevit	k5eAaPmIp3nS	objevit
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
lidské	lidský	k2eAgFnSc2d1	lidská
podoby	podoba	k1gFnSc2	podoba
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sedmé	sedmý	k4xOgFnSc2	sedmý
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
připojí	připojit	k5eAaPmIp3nS	připojit
k	k	k7c3	k
SG-	SG-	k1gFnSc3	SG-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zůstane	zůstat	k5eAaPmIp3nS	zůstat
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
třech	tři	k4xCgFnPc6	tři
řadách	řada	k1gFnPc6	řada
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jeho	jeho	k3xOp3gInSc1	jeho
flirtovací	flirtovací	k2eAgInSc1d1	flirtovací
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
protikladnou	protikladný	k2eAgFnSc7d1	protikladná
Valou	Vala	k1gMnSc7	Vala
Mal	málit	k5eAaImRp2nS	málit
Doran	Doran	k1gInSc4	Doran
<g/>
.	.	kIx.	.
</s>
<s>
Jackson	Jackson	k1gMnSc1	Jackson
účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
také	také	k9	také
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
navazujících	navazující	k2eAgNnPc6d1	navazující
DVD	DVD	kA	DVD
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
epizodách	epizoda	k1gFnPc6	epizoda
seriálu	seriál	k1gInSc2	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
a	a	k8xC	a
ve	v	k7c6	v
čtyřech	čtyři	k4xCgNnPc6	čtyři
dílech	dílo	k1gNnPc6	dílo
seriálu	seriál	k1gInSc2	seriál
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Amanda	Amanda	k1gFnSc1	Amanda
Tapping	Tapping	k1gInSc1	Tapping
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Simona	Simona	k1gFnSc1	Simona
Postlerová	Postlerová	k1gFnSc1	Postlerová
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Samantha	Samantha	k1gMnSc1	Samantha
"	"	kIx"	"
<g/>
Sam	Sam	k1gMnSc1	Sam
<g/>
"	"	kIx"	"
Carterová	Carterová	k1gFnSc1	Carterová
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Kapitán	kapitán	k1gMnSc1	kapitán
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
skvělá	skvělý	k2eAgFnSc1d1	skvělá
mladá	mladý	k2eAgFnSc1d1	mladá
astrofyzička	astrofyzička	k1gFnSc1	astrofyzička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
k	k	k7c3	k
týmu	tým	k1gInSc3	tým
SG-1	SG-1	k1gFnSc2	SG-1
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
řadě	řada	k1gFnSc6	řada
je	být	k5eAaImIp3nS	být
povýšena	povýšen	k2eAgFnSc1d1	povýšena
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
majora	major	k1gMnSc2	major
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
osmé	osmý	k4xOgFnSc2	osmý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
převezme	převzít	k5eAaPmIp3nS	převzít
po	po	k7c6	po
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neillovi	Neill	k1gMnSc6	Neill
velení	velení	k1gNnSc1	velení
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
na	na	k7c4	na
podplukovníka	podplukovník	k1gMnSc4	podplukovník
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
dabingu	dabing	k1gInSc6	dabing
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc4	plukovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náznaky	náznak	k1gInPc1	náznak
romantických	romantický	k2eAgInPc2d1	romantický
citů	cit	k1gInPc2	cit
mezí	mez	k1gFnPc2	mez
ní	on	k3xPp3gFnSc6	on
a	a	k8xC	a
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neillem	Neill	k1gMnSc7	Neill
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
více	hodně	k6eAd2	hodně
rozvinuty	rozvinut	k2eAgFnPc1d1	rozvinuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
deváté	devátý	k4xOgFnSc2	devátý
a	a	k8xC	a
desáté	desátý	k4xOgFnSc2	desátý
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
zástupcem	zástupce	k1gMnSc7	zástupce
nového	nový	k2eAgNnSc2d1	nové
velitele	velitel	k1gMnSc2	velitel
týmu	tým	k1gInSc2	tým
Mitchella	Mitchello	k1gNnSc2	Mitchello
<g/>
.	.	kIx.	.
</s>
<s>
Účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
též	též	k9	též
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Archa	archa	k1gFnSc1	archa
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
povýšena	povýšen	k2eAgFnSc1d1	povýšena
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc4	plukovník
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
velitelkou	velitelka	k1gFnSc7	velitelka
expedice	expedice	k1gFnSc1	expedice
Atlantida	Atlantida	k1gFnSc1	Atlantida
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
sezóně	sezóna	k1gFnSc6	sezóna
seriálu	seriál	k1gInSc2	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
však	však	k9	však
znovu	znovu	k6eAd1	znovu
připojí	připojit	k5eAaPmIp3nS	připojit
k	k	k7c3	k
SG-1	SG-1	k1gFnSc3	SG-1
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Návrat	návrat	k1gInSc1	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Vyjma	vyjma	k7c2	vyjma
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
řady	řada	k1gFnSc2	řada
Atlantidy	Atlantida	k1gFnSc2	Atlantida
se	se	k3xPyFc4	se
jako	jako	k9	jako
host	host	k1gMnSc1	host
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
pěti	pět	k4xCc6	pět
epizodách	epizoda	k1gFnPc6	epizoda
tohoto	tento	k3xDgInSc2	tento
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
dílech	dílo	k1gNnPc6	dílo
seriálu	seriál	k1gInSc2	seriál
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
velitelka	velitelka	k1gFnSc1	velitelka
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
lodi	loď	k1gFnSc2	loď
George	Georg	k1gMnSc4	Georg
Hammond	Hammond	k1gMnSc1	Hammond
<g/>
.	.	kIx.	.
</s>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Judge	Judge	k1gFnPc2	Judge
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Rímský	Rímský	k1gMnSc1	Rímský
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Teal	Teal	k1gMnSc1	Teal
<g/>
'	'	kIx"	'
<g/>
c	c	k0	c
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Tichý	tichý	k2eAgInSc4d1	tichý
a	a	k8xC	a
silný	silný	k2eAgInSc4d1	silný
Jaffa	Jaffa	k1gFnSc1	Jaffa
<g/>
,	,	kIx,	,
mimozemšťan	mimozemšťan	k1gMnSc1	mimozemšťan
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
Chulak	Chulak	k1gMnSc1	Chulak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
opustil	opustit	k5eAaPmAgMnS	opustit
svoji	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
prvního	první	k4xOgMnSc2	první
muže	muž	k1gMnSc2	muž
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
ulda	uld	k2eAgFnSc1d1	ulda
Apophise	Apophise	k1gFnSc1	Apophise
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
týmu	tým	k1gInSc3	tým
SG-1	SG-1	k1gFnSc2	SG-1
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
po	po	k7c6	po
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
svůj	svůj	k3xOyFgInSc4	svůj
lid	lid	k1gInSc4	lid
z	z	k7c2	z
područí	područí	k1gNnSc2	područí
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldů	uld	k1gInPc2	uld
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
dosažení	dosažení	k1gNnSc4	dosažení
tohoto	tento	k3xDgInSc2	tento
cíle	cíl	k1gInSc2	cíl
na	na	k7c6	na
konci	konec	k1gInSc6	konec
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dále	daleko	k6eAd2	daleko
členem	člen	k1gMnSc7	člen
jednotky	jednotka	k1gFnSc2	jednotka
až	až	k8xS	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
DVD	DVD	kA	DVD
filmech	film	k1gInPc6	film
a	a	k8xC	a
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
epizodách	epizoda	k1gFnPc6	epizoda
seriálu	seriál	k1gInSc2	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
S.	S.	kA	S.
Davis	Davis	k1gFnSc1	Davis
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Samek	Samek	k1gMnSc1	Samek
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
George	Georg	k1gMnSc2	Georg
Hammond	Hammonda	k1gFnPc2	Hammonda
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
host	host	k1gMnSc1	host
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Generálmajor	generálmajor	k1gMnSc1	generálmajor
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
velí	velet	k5eAaImIp3nS	velet
Velitelství	velitelství	k1gNnSc4	velitelství
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
sedmi	sedm	k4xCc6	sedm
sezónách	sezóna	k1gFnPc6	sezóna
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sedmé	sedmý	k4xOgFnSc2	sedmý
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
generálporučíka	generálporučík	k1gMnSc2	generálporučík
a	a	k8xC	a
převelen	převelet	k5eAaPmNgMnS	převelet
do	do	k7c2	do
Washingtonu	Washington	k1gInSc2	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gFnSc2	D.C.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dále	daleko	k6eAd2	daleko
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
velitel	velitel	k1gMnSc1	velitel
Obrany	obrana	k1gFnSc2	obrana
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
objeví	objevit	k5eAaPmIp3nS	objevit
ještě	ještě	k9	ještě
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
epizodách	epizoda	k1gFnPc6	epizoda
osmé	osmý	k4xOgNnSc4	osmý
<g/>
,	,	kIx,	,
deváté	devátý	k4xOgFnPc4	devátý
a	a	k8xC	a
desáté	desátý	k4xOgFnSc2	desátý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
též	též	k9	též
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Návrat	návrat	k1gInSc1	návrat
a	a	k8xC	a
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
díle	dílo	k1gNnSc6	dílo
seriálu	seriál	k1gInSc2	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Corin	Corin	k2eAgInSc1d1	Corin
Nemec	Nemec	k1gInSc1	Nemec
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Saša	Saša	k1gFnSc1	Saša
Rašilov	Rašilov	k1gInSc1	Rašilov
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Libert	Libert	k1gMnSc1	Libert
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Jonas	Jonas	k1gMnSc1	Jonas
Quinn	Quinn	k1gMnSc1	Quinn
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
host	host	k1gMnSc1	host
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Mimozemský	mimozemský	k2eAgMnSc1d1	mimozemský
vědec	vědec	k1gMnSc1	vědec
pocházející	pocházející	k2eAgFnSc2d1	pocházející
ze	z	k7c2	z
státu	stát	k1gInSc2	stát
Kelowna	Kelown	k1gInSc2	Kelown
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Langara	Langar	k1gMnSc2	Langar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
páté	pátý	k4xOgFnSc2	pátý
řady	řada	k1gFnSc2	řada
obětuje	obětovat	k5eAaBmIp3nS	obětovat
Daniel	Daniel	k1gMnSc1	Daniel
Jackson	Jackson	k1gMnSc1	Jackson
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
povznesení	povznesení	k1gNnSc4	povznesení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachránil	zachránit	k5eAaPmAgMnS	zachránit
Kelownu	Kelowna	k1gFnSc4	Kelowna
<g/>
,	,	kIx,	,
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
reakce	reakce	k1gFnSc1	reakce
jejích	její	k3xOp3gMnPc2	její
představitelů	představitel	k1gMnPc2	představitel
ale	ale	k8xC	ale
přiměje	přimět	k5eAaPmIp3nS	přimět
Quinna	Quinen	k2eAgFnSc1d1	Quinna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
z	z	k7c2	z
Langary	Langara	k1gFnSc2	Langara
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Jonas	Jonas	k1gMnSc1	Jonas
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
učí	učit	k5eAaImIp3nS	učit
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
zaplní	zaplnit	k5eAaPmIp3nP	zaplnit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
šesté	šestý	k4xOgFnSc2	šestý
řady	řada	k1gFnSc2	řada
Jacksonovo	Jacksonův	k2eAgNnSc4d1	Jacksonovo
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
SG-	SG-	k1gFnSc2	SG-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Danielově	Danielův	k2eAgInSc6d1	Danielův
návratu	návrat	k1gInSc6	návrat
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Hostuje	hostovat	k5eAaImIp3nS	hostovat
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
epizodách	epizoda	k1gFnPc6	epizoda
páté	pátá	k1gFnSc2	pátá
a	a	k8xC	a
sedmé	sedmý	k4xOgFnSc2	sedmý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
Browder	Browder	k1gInSc1	Browder
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Písařík	písařík	k1gMnSc1	písařík
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Cameron	Cameron	k1gMnSc1	Cameron
"	"	kIx"	"
<g/>
Cam	Cam	k1gMnSc1	Cam
<g/>
"	"	kIx"	"
Mitchell	Mitchell	k1gMnSc1	Mitchell
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Podplukovník	podplukovník	k1gMnSc1	podplukovník
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
dabingu	dabing	k1gInSc6	dabing
plukovník	plukovník	k1gMnSc1	plukovník
<g/>
)	)	kIx)	)
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
deváté	devátý	k4xOgFnSc2	devátý
řady	řada	k1gFnSc2	řada
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
novým	nový	k2eAgMnSc7d1	nový
velitelem	velitel	k1gMnSc7	velitel
týmu	tým	k1gInSc2	tým
SG-	SG-	k1gFnSc1	SG-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
bývalé	bývalý	k2eAgInPc1d1	bývalý
členy	člen	k1gInPc1	člen
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
přemluvit	přemluvit	k5eAaPmF	přemluvit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
účinkováním	účinkování	k1gNnSc7	účinkování
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Archa	archa	k1gFnSc1	archa
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
Návrat	návrat	k1gInSc1	návrat
je	být	k5eAaImIp3nS	být
povýšen	povýšen	k2eAgInSc1d1	povýšen
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc4	plukovník
<g/>
.	.	kIx.	.
</s>
<s>
Beau	Beau	k5eAaPmIp1nS	Beau
Bridges	Bridges	k1gInSc1	Bridges
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Čapka	Čapka	k1gMnSc1	Čapka
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Henry	Henry	k1gMnSc1	Henry
"	"	kIx"	"
<g/>
Hank	Hank	k1gMnSc1	Hank
<g/>
"	"	kIx"	"
Landry	Landr	k1gInPc1	Landr
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Generálmajor	generálmajor	k1gMnSc1	generálmajor
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
Velitelství	velitelství	k1gNnSc2	velitelství
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
(	(	kIx(	(
<g/>
SGC	SGC	kA	SGC
<g/>
)	)	kIx)	)
v	v	k7c6	v
deváté	devátý	k4xOgFnSc6	devátý
a	a	k8xC	a
desáté	desátý	k4xOgFnSc3	desátý
řadě	řada	k1gFnSc3	řada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otcem	otec	k1gMnSc7	otec
doktorky	doktorka	k1gFnSc2	doktorka
Carolyn	Carolyn	k1gNnSc4	Carolyn
Lamové	lama	k1gMnPc1	lama
působící	působící	k2eAgFnSc2d1	působící
též	též	k9	též
v	v	k7c6	v
SGC	SGC	kA	SGC
<g/>
.	.	kIx.	.
</s>
<s>
Objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
DVD	DVD	kA	DVD
filmech	film	k1gInPc6	film
a	a	k8xC	a
v	v	k7c6	v
pěti	pět	k4xCc6	pět
epizodách	epizoda	k1gFnPc6	epizoda
seriálu	seriál	k1gInSc2	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Claudia	Claudia	k1gFnSc1	Claudia
Black	Blacka	k1gFnPc2	Blacka
(	(	kIx(	(
<g/>
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
<g/>
:	:	kIx,	:
Kateřina	Kateřina	k1gFnSc1	Kateřina
Lojdová	Lojdová	k1gFnSc1	Lojdová
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
Vala	Vala	k1gMnSc1	Vala
Mal	málit	k5eAaImRp2nS	málit
Doran	Doran	k1gMnSc1	Doran
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
host	host	k1gMnSc1	host
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
–	–	k?	–
Podvodnice	podvodnice	k1gFnSc1	podvodnice
a	a	k8xC	a
zlodějka	zlodějka	k1gFnSc1	zlodějka
z	z	k7c2	z
nejmenované	jmenovaný	k2eNgFnSc2d1	nejmenovaná
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
bývalá	bývalý	k2eAgFnSc1d1	bývalá
lidská	lidský	k2eAgFnSc1d1	lidská
hostitelka	hostitelka	k1gFnSc1	hostitelka
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
ulda	ulda	k1gMnSc1	ulda
Qetesh	Qetesh	k1gMnSc1	Qetesh
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
osmé	osmý	k4xOgFnSc6	osmý
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začne	začít	k5eAaPmIp3nS	začít
flirtovat	flirtovat	k5eAaImF	flirtovat
se	s	k7c7	s
zcela	zcela	k6eAd1	zcela
protikladným	protikladný	k2eAgMnSc7d1	protikladný
Danielem	Daniel	k1gMnSc7	Daniel
Jacksonem	Jackson	k1gMnSc7	Jackson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deváté	devátý	k4xOgFnSc6	devátý
sezóně	sezóna	k1gFnSc6	sezóna
společně	společně	k6eAd1	společně
s	s	k7c7	s
Danielem	Daniel	k1gMnSc7	Daniel
neúmyslně	úmyslně	k6eNd1	úmyslně
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
orijskou	orijský	k2eAgFnSc4d1	orijský
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porodu	porod	k1gInSc6	porod
nové	nový	k2eAgFnSc2d1	nová
orijské	orijský	k2eAgFnSc2d1	orijský
vůdkyně	vůdkyně	k1gFnSc2	vůdkyně
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
desáté	desátá	k1gFnSc2	desátá
řady	řada	k1gFnSc2	řada
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nP	připojit
k	k	k7c3	k
SG-	SG-	k1gFnSc3	SG-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
též	též	k9	též
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
DVD	DVD	kA	DVD
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
a	a	k8xC	a
Jonathan	Jonathan	k1gMnSc1	Jonathan
Glassner	Glassner	k1gMnSc1	Glassner
pracovali	pracovat	k5eAaImAgMnP	pracovat
společně	společně	k6eAd1	společně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
na	na	k7c6	na
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
společnosti	společnost	k1gFnSc2	společnost
Metro-Goldwyn-Mayer	Metro-Goldwyn-Mayer	k1gMnSc1	Metro-Goldwyn-Mayer
(	(	kIx(	(
<g/>
MGM	MGM	kA	MGM
<g/>
)	)	kIx)	)
Krajní	krajní	k2eAgFnPc4d1	krajní
meze	mez	k1gFnPc4	mez
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
doslechli	doslechnout	k5eAaPmAgMnP	doslechnout
o	o	k7c6	o
plánu	plán	k1gInSc6	plán
MGM	MGM	kA	MGM
vytvořit	vytvořit	k5eAaPmF	vytvořit
spin-off	spinff	k1gInSc4	spin-off
seriál	seriál	k1gInSc1	seriál
k	k	k7c3	k
celovečernímu	celovečerní	k2eAgInSc3d1	celovečerní
filmu	film	k1gInSc3	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
a	a	k8xC	a
bez	bez	k7c2	bez
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
vědomí	vědomí	k1gNnSc2	vědomí
oslovili	oslovit	k5eAaPmAgMnP	oslovit
MGM	MGM	kA	MGM
a	a	k8xC	a
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
seriálu	seriál	k1gInSc3	seriál
svoje	svůj	k3xOyFgInPc4	svůj
koncepty	koncept	k1gInPc4	koncept
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
MGM	MGM	kA	MGM
John	John	k1gMnSc1	John
Symes	Symes	k1gMnSc1	Symes
dal	dát	k5eAaPmAgMnS	dát
projektu	projekt	k1gInSc2	projekt
zelenou	zelená	k1gFnSc4	zelená
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Wright	Wright	k1gInSc4	Wright
a	a	k8xC	a
Glassner	Glassner	k1gInSc4	Glassner
budou	být	k5eAaImBp3nP	být
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
díle	díl	k1gInSc6	díl
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
jako	jako	k8xS	jako
výkonní	výkonný	k2eAgMnPc1d1	výkonný
producenti	producent	k1gMnPc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Stargate	Stargat	k1gInSc5	Stargat
SG-1	SG-1	k1gMnSc7	SG-1
(	(	kIx(	(
<g/>
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
a	a	k8xC	a
dabingu	dabing	k1gInSc6	dabing
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Wright	Wright	k1gMnSc1	Wright
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
se	s	k7c7	s
Symesovou	Symesový	k2eAgFnSc7d1	Symesový
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nS	by
tým	tým	k1gInSc1	tým
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
označený	označený	k2eAgInSc1d1	označený
jako	jako	k9	jako
"	"	kIx"	"
<g/>
SG-	SG-	k1gFnSc1	SG-
<g/>
1	[number]	k4	1
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
MGM	MGM	kA	MGM
potom	potom	k6eAd1	potom
během	během	k7c2	během
týdne	týden	k1gInSc2	týden
a	a	k8xC	a
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
Wrighta	Wright	k1gMnSc2	Wright
a	a	k8xC	a
Glassnera	Glassner	k1gMnSc2	Glassner
vydala	vydat	k5eAaPmAgFnS	vydat
plakáty	plakát	k1gInPc4	plakát
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
Stargate	Stargat	k1gInSc5	Stargat
SG-	SG-	k1gFnPc7	SG-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Symes	Symes	k1gMnSc1	Symes
oslovil	oslovit	k5eAaPmAgMnS	oslovit
Michaela	Michael	k1gMnSc4	Michael
Greenburga	Greenburg	k1gMnSc4	Greenburg
a	a	k8xC	a
Richarda	Richard	k1gMnSc4	Richard
Deana	Dean	k1gMnSc4	Dean
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
bývalou	bývalý	k2eAgFnSc4d1	bývalá
hvězdu	hvězda	k1gFnSc4	hvězda
seriálu	seriál	k1gInSc2	seriál
MacGyver	MacGyvero	k1gNnPc2	MacGyvero
<g/>
.	.	kIx.	.
</s>
<s>
Anderson	Anderson	k1gMnSc1	Anderson
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
Jack	Jack	k1gMnSc1	Jack
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neill	Neill	k1gInSc1	Neill
bude	být	k5eAaImBp3nS	být
humornějšího	humorný	k2eAgInSc2d2	humorný
charakteru	charakter	k1gInSc2	charakter
než	než	k8xS	než
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
hrál	hrát	k5eAaImAgMnS	hrát
Kurt	Kurt	k1gMnSc1	Kurt
Russell	Russell	k1gMnSc1	Russell
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
také	také	k9	také
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
Hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
bráně	brána	k1gFnSc6	brána
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
příběh	příběh	k1gInSc1	příběh
tak	tak	k6eAd1	tak
nemusel	muset	k5eNaImAgInS	muset
být	být	k5eAaImF	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
jediném	jediný	k2eAgInSc6d1	jediný
charakteru	charakter	k1gInSc6	charakter
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
MacGyvera	MacGyvero	k1gNnSc2	MacGyvero
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
kanál	kanál	k1gInSc1	kanál
pro	pro	k7c4	pro
předplatitele	předplatitel	k1gMnPc4	předplatitel
Showtime	Showtim	k1gInSc5	Showtim
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
zavázal	zavázat	k5eAaPmAgInS	zavázat
odvysílat	odvysílat	k5eAaPmF	odvysílat
44	[number]	k4	44
epizod	epizoda	k1gFnPc2	epizoda
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
řadách	řada	k1gFnPc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Natáčení	natáčení	k1gNnSc1	natáčení
seriálu	seriál	k1gInSc2	seriál
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Anderson	Anderson	k1gMnSc1	Anderson
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
zhlédli	zhlédnout	k5eAaPmAgMnP	zhlédnout
Brad	brada	k1gFnPc2	brada
Wright	Wright	k2eAgMnSc1d1	Wright
a	a	k8xC	a
Jonathan	Jonathan	k1gMnSc1	Jonathan
Glassner	Glassnra	k1gFnPc2	Glassnra
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
pásek	páska	k1gFnPc2	páska
a	a	k8xC	a
pozvali	pozvat	k5eAaPmAgMnP	pozvat
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
slibných	slibný	k2eAgMnPc2d1	slibný
herců	herec	k1gMnPc2	herec
na	na	k7c4	na
kamerové	kamerový	k2eAgFnPc4d1	kamerová
zkoušky	zkouška	k1gFnPc4	zkouška
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Shanks	Shanksa	k1gFnPc2	Shanksa
<g/>
,	,	kIx,	,
Amanda	Amanda	k1gFnSc1	Amanda
Tapping	Tapping	k1gInSc1	Tapping
a	a	k8xC	a
Christopher	Christophra	k1gFnPc2	Christophra
Judge	Judg	k1gFnSc2	Judg
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
během	během	k7c2	během
castingu	casting	k1gInSc2	casting
k	k	k7c3	k
sobě	se	k3xPyFc3	se
táhli	táhnout	k5eAaImAgMnP	táhnout
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
dosud	dosud	k6eAd1	dosud
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
vybráni	vybrat	k5eAaPmNgMnP	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Judge	Judg	k1gInSc2	Judg
měli	mít	k5eAaImAgMnP	mít
producenti	producent	k1gMnPc1	producent
výběr	výběr	k1gInSc4	výběr
jednodušší	jednoduchý	k2eAgInPc4d2	jednodušší
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
svalnaté	svalnatý	k2eAgFnSc3d1	svalnatá
postavě	postava	k1gFnSc3	postava
<g/>
.	.	kIx.	.
</s>
<s>
Shanks	Shanks	k1gInSc1	Shanks
byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
podle	podle	k7c2	podle
Wrighta	Wrighto	k1gNnSc2	Wrighto
"	"	kIx"	"
<g/>
perfektně	perfektně	k6eAd1	perfektně
napodoboval	napodobovat	k5eAaImAgInS	napodobovat
Jamese	Jamese	k1gFnSc2	Jamese
Spadera	Spader	k1gMnSc2	Spader
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dona	dona	k1gFnSc1	dona
S.	S.	kA	S.
Davise	Davise	k1gFnSc2	Davise
producenti	producent	k1gMnPc1	producent
znali	znát	k5eAaImAgMnP	znát
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
předchozí	předchozí	k2eAgFnSc2d1	předchozí
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
MacGyverovi	MacGyver	k1gMnSc6	MacGyver
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
dvojník	dvojník	k1gMnSc1	dvojník
a	a	k8xC	a
kaskadér	kaskadér	k1gMnSc1	kaskadér
Dana	Dana	k1gFnSc1	Dana
Elcara	Elcara	k1gFnSc1	Elcara
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
roli	role	k1gFnSc4	role
George	Georg	k1gMnSc2	Georg
Hammonda	Hammond	k1gMnSc2	Hammond
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
Showtime	Showtim	k1gInSc5	Showtim
oznámil	oznámit	k5eAaPmAgMnS	oznámit
neobnovení	neobnovení	k1gNnSc2	neobnovení
seriálu	seriál	k1gInSc2	seriál
po	po	k7c6	po
páté	pátý	k4xOgFnSc6	pátý
sezóně	sezóna	k1gFnSc6	sezóna
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
Micheal	Micheal	k1gInSc1	Micheal
Shanks	Shanksa	k1gFnPc2	Shanksa
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
kvůli	kvůli	k7c3	kvůli
obavám	obava	k1gFnPc3	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
postava	postava	k1gFnSc1	postava
plně	plně	k6eAd1	plně
využívána	využívat	k5eAaImNgFnS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
převzal	převzít	k5eAaPmAgInS	převzít
Sci-Fi	scii	k1gFnSc3	sci-fi
Channel	Channel	k1gInSc1	Channel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
místo	místo	k1gNnSc4	místo
novou	nový	k2eAgFnSc7d1	nová
postavou	postava	k1gFnSc7	postava
hranou	hrana	k1gFnSc7	hrana
Corinem	Corin	k1gMnSc7	Corin
Nemecem	Nemece	k1gMnSc7	Nemece
<g/>
.	.	kIx.	.
</s>
<s>
Castingoví	Castingový	k2eAgMnPc1d1	Castingový
agenti	agent	k1gMnPc1	agent
se	se	k3xPyFc4	se
s	s	k7c7	s
Nemecem	Nemeec	k1gInSc7	Nemeec
náhodou	náhodou	k6eAd1	náhodou
setkali	setkat	k5eAaPmAgMnP	setkat
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
kanceláří	kancelář	k1gFnPc2	kancelář
MGM	MGM	kA	MGM
v	v	k7c6	v
Santa	Sant	k1gMnSc2	Sant
Monice	Monika	k1gFnSc6	Monika
a	a	k8xC	a
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
mu	on	k3xPp3gMnSc3	on
roli	role	k1gFnSc4	role
Jonase	Jonasa	k1gFnSc3	Jonasa
Quinna	Quinn	k1gInSc2	Quinn
<g/>
.	.	kIx.	.
</s>
<s>
Zvěsti	zvěst	k1gFnPc1	zvěst
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
posílilo	posílit	k5eAaPmAgNnS	posílit
Shanksovo	Shanksův	k2eAgNnSc1d1	Shanksův
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
Sci-Fi	scii	k1gNnSc1	sci-fi
Channel	Channela	k1gFnPc2	Channela
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2002	[number]	k4	2002
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Nemecovo	Nemecův	k2eAgNnSc4d1	Nemecův
počáteční	počáteční	k2eAgNnSc4d1	počáteční
vystupování	vystupování	k1gNnSc4	vystupování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
cesta	cesta	k1gFnSc1	cesta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
předposlední	předposlední	k2eAgFnSc3d1	předposlední
epizodě	epizoda	k1gFnSc3	epizoda
páté	pátá	k1gFnSc2	pátá
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
některých	některý	k3yIgMnPc2	některý
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
herec	herec	k1gMnSc1	herec
byl	být	k5eAaImAgMnS	být
ochotný	ochotný	k2eAgMnSc1d1	ochotný
hrát	hrát	k5eAaImF	hrát
tuto	tento	k3xDgFnSc4	tento
postavu	postava	k1gFnSc4	postava
i	i	k9	i
po	po	k7c6	po
šesté	šestý	k4xOgFnSc6	šestý
řadě	řada	k1gFnSc6	řada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
či	či	k8xC	či
ve	v	k7c4	v
spin-off	spinff	k1gInSc4	spin-off
seriálu	seriál	k1gInSc2	seriál
<g/>
..	..	k?	..
Producenti	producent	k1gMnPc1	producent
ale	ale	k9	ale
dojednali	dojednat	k5eAaPmAgMnP	dojednat
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
se	s	k7c7	s
Shanksem	Shanks	k1gInSc7	Shanks
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
Jackson	Jackson	k1gInSc1	Jackson
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
řadě	řada	k1gFnSc6	řada
vrátit	vrátit	k5eAaPmF	vrátit
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Nemecův	Nemecův	k2eAgMnSc1d1	Nemecův
Quinn	Quinn	k1gMnSc1	Quinn
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vracející	vracející	k2eAgMnSc1d1	vracející
se	se	k3xPyFc4	se
vedlejší	vedlejší	k2eAgFnSc7d1	vedlejší
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
S.	S.	kA	S.
Davis	Davis	k1gInSc1	Davis
odešel	odejít	k5eAaPmAgInS	odejít
z	z	k7c2	z
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
po	po	k7c6	po
sedmé	sedmý	k4xOgFnSc6	sedmý
sezóně	sezóna	k1gFnSc6	sezóna
kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgMnPc3d1	zdravotní
důvodům	důvod	k1gInPc3	důvod
<g/>
,	,	kIx,	,
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepravidelně	pravidelně	k6eNd1	pravidelně
objevoval	objevovat	k5eAaImAgInS	objevovat
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
jiným	jiný	k2eAgFnPc3d1	jiná
povinnostem	povinnost	k1gFnPc3	povinnost
nemohla	moct	k5eNaImAgFnS	moct
Claudia	Claudia	k1gFnSc1	Claudia
Black	Black	k1gInSc1	Black
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
se	se	k3xPyFc4	se
seriálu	seriál	k1gInSc2	seriál
Farscape	Farscap	k1gMnSc5	Farscap
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
přijmout	přijmout	k5eAaPmF	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
hostování	hostování	k1gNnSc4	hostování
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
až	až	k9	až
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Upoutaný	upoutaný	k2eAgMnSc1d1	upoutaný
Prométheus	Prométheus	k1gMnSc1	Prométheus
<g/>
"	"	kIx"	"
v	v	k7c6	v
osmé	osmý	k4xOgFnSc6	osmý
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Producentům	producent	k1gMnPc3	producent
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
interakce	interakce	k1gFnSc1	interakce
mezi	mezi	k7c7	mezi
Valou	Vala	k1gMnSc7	Vala
Mal	málit	k5eAaImRp2nS	málit
Doran	Doran	k1gInSc1	Doran
(	(	kIx(	(
<g/>
Claudia	Claudia	k1gFnSc1	Claudia
Black	Black	k1gMnSc1	Black
<g/>
)	)	kIx)	)
a	a	k8xC	a
Shanksovým	Shanksův	k2eAgMnSc7d1	Shanksův
Danielem	Daniel	k1gMnSc7	Daniel
Jacksonem	Jackson	k1gMnSc7	Jackson
natolik	natolik	k6eAd1	natolik
líbily	líbit	k5eAaImAgFnP	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Vala	Vala	k1gMnSc1	Vala
vrátila	vrátit	k5eAaPmAgFnS	vrátit
v	v	k7c6	v
šestidílném	šestidílný	k2eAgInSc6d1	šestidílný
příběhu	příběh	k1gInSc6	příběh
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
deváté	devátý	k4xOgFnSc2	devátý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
mohli	moct	k5eAaImAgMnP	moct
také	také	k9	také
zamaskovat	zamaskovat	k5eAaPmF	zamaskovat
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
dovolenou	dovolená	k1gFnSc4	dovolená
Amandy	Amanda	k1gFnSc2	Amanda
Tapping	Tapping	k1gInSc1	Tapping
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
odešel	odejít	k5eAaPmAgInS	odejít
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Richard	Richard	k1gMnSc1	Richard
Dean	Dean	k1gMnSc1	Dean
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
trávit	trávit	k5eAaImF	trávit
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
vůdce	vůdce	k1gMnSc2	vůdce
týmu	tým	k1gInSc2	tým
převzal	převzít	k5eAaPmAgInS	převzít
Ben	Ben	k1gInSc1	Ben
Browder	Browder	k1gInSc1	Browder
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
Farscape	Farscap	k1gMnSc5	Farscap
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
producenty	producent	k1gMnPc7	producent
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
na	na	k7c4	na
sci-fi	scii	k1gNnSc4	sci-fi
conu	conus	k1gInSc2	conus
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
nové	nový	k2eAgFnSc2d1	nová
postavy	postava	k1gFnSc2	postava
teprve	teprve	k6eAd1	teprve
diskutovala	diskutovat	k5eAaImAgFnS	diskutovat
<g/>
.	.	kIx.	.
</s>
<s>
Browder	Browder	k1gInSc1	Browder
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
uvažován	uvažován	k2eAgInSc1d1	uvažován
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
rolí	role	k1gFnPc2	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
rovněž	rovněž	k9	rovněž
oslovili	oslovit	k5eAaPmAgMnP	oslovit
držitele	držitel	k1gMnSc4	držitel
ceny	cena	k1gFnSc2	cena
Emmy	Emma	k1gFnSc2	Emma
Beau	Beaus	k1gInSc2	Beaus
Bridgesese	Bridgesese	k1gFnSc2	Bridgesese
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c6	o
roli	role	k1gFnSc6	role
Hanka	Hanka	k1gFnSc1	Hanka
Landryho	Landry	k1gMnSc2	Landry
<g/>
.	.	kIx.	.
</s>
<s>
Hostování	hostování	k1gNnSc1	hostování
Claudie	Claudia	k1gFnSc2	Claudia
Black	Blacka	k1gFnPc2	Blacka
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
štábem	štáb	k1gInSc7	štáb
i	i	k9	i
diváky	divák	k1gMnPc4	divák
tak	tak	k6eAd1	tak
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
i	i	k9	i
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
těhotenstvím	těhotenství	k1gNnSc7	těhotenství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
poslední	poslední	k2eAgFnPc4d1	poslední
dvě	dva	k4xCgFnPc4	dva
epizody	epizoda	k1gFnPc4	epizoda
deváté	devátý	k4xOgFnSc2	devátý
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
v	v	k7c6	v
desáté	desátý	k4xOgFnSc6	desátý
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
producentů	producent	k1gMnPc2	producent
<g/>
,	,	kIx,	,
členů	člen	k1gMnPc2	člen
štábu	štáb	k1gInSc2	štáb
a	a	k8xC	a
hostujících	hostující	k2eAgInPc2d1	hostující
herců	herc	k1gInPc2	herc
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnými	výkonný	k2eAgMnPc7d1	výkonný
producenty	producent	k1gMnPc7	producent
a	a	k8xC	a
showrunnery	showrunner	k1gMnPc7	showrunner
seriálu	seriál	k1gInSc6	seriál
byli	být	k5eAaImAgMnP	být
během	během	k7c2	během
prvních	první	k4xOgFnPc2	první
tří	tři	k4xCgFnPc2	tři
řad	řada	k1gFnPc2	řada
jeho	jeho	k3xOp3gMnPc1	jeho
tvůrci	tvůrce	k1gMnPc1	tvůrce
Brad	brada	k1gFnPc2	brada
Wright	Wrightum	k1gNnPc2	Wrightum
a	a	k8xC	a
Jonathan	Jonathan	k1gMnSc1	Jonathan
Glassner	Glassner	k1gMnSc1	Glassner
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
konečné	konečný	k2eAgNnSc4d1	konečné
slovo	slovo	k1gNnSc4	slovo
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
MGM	MGM	kA	MGM
a	a	k8xC	a
televizního	televizní	k2eAgInSc2d1	televizní
kanálu	kanál	k1gInSc2	kanál
<g/>
)	)	kIx)	)
v	v	k7c6	v
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
výpravě	výprava	k1gFnSc6	výprava
<g/>
,	,	kIx,	,
efektech	efekt	k1gInPc6	efekt
<g/>
,	,	kIx,	,
obsazení	obsazení	k1gNnSc6	obsazení
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
střihu	střih	k1gInSc2	střih
a	a	k8xC	a
v	v	k7c6	v
rozpočtech	rozpočet	k1gInPc6	rozpočet
epizod	epizoda	k1gFnPc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Glassnerově	Glassnerův	k2eAgInSc6d1	Glassnerův
odchodu	odchod	k1gInSc6	odchod
se	se	k3xPyFc4	se
o	o	k7c4	o
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
staral	starat	k5eAaImAgMnS	starat
po	po	k7c4	po
další	další	k2eAgInPc4d1	další
tři	tři	k4xCgInPc4	tři
řady	řada	k1gFnSc2	řada
sám	sám	k3xTgMnSc1	sám
Wright	Wright	k1gMnSc1	Wright
<g/>
.	.	kIx.	.
</s>
<s>
Výkonný	výkonný	k2eAgMnSc1d1	výkonný
producent	producent	k1gMnSc1	producent
Robert	Robert	k1gMnSc1	Robert
C.	C.	kA	C.
Cooper	Cooper	k1gMnSc1	Cooper
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
showrunnerem	showrunner	k1gMnSc7	showrunner
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c4	na
koncepci	koncepce	k1gFnSc4	koncepce
spin-off	spinff	k1gMnSc1	spin-off
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Cooper	Cooper	k1gMnSc1	Cooper
a	a	k8xC	a
Wright	Wright	k1gMnSc1	Wright
poté	poté	k6eAd1	poté
zůstali	zůstat	k5eAaPmAgMnP	zůstat
showrunnery	showrunner	k1gMnPc7	showrunner
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
výkonní	výkonný	k2eAgMnPc1d1	výkonný
producenti	producent	k1gMnPc1	producent
a	a	k8xC	a
koproducenti	koproducent	k1gMnPc1	koproducent
seriálu	seriál	k1gInSc2	seriál
působili	působit	k5eAaImAgMnP	působit
také	také	k9	také
Michael	Michael	k1gMnSc1	Michael
Greenburg	Greenburg	k1gMnSc1	Greenburg
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Dean	Dean	k1gMnSc1	Dean
Anderson	Anderson	k1gMnSc1	Anderson
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
N.	N.	kA	N.
John	John	k1gMnSc1	John
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
a	a	k8xC	a
scenáristický	scenáristický	k2eAgInSc1d1	scenáristický
tým	tým	k1gInSc1	tým
Joseph	Josepha	k1gFnPc2	Josepha
Mallozzi	Mallozh	k1gMnPc1	Mallozh
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Mullie	Mullie	k1gFnSc2	Mullie
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
využívala	využívat	k5eAaImAgFnS	využívat
i	i	k8xC	i
scenáristů	scenárista	k1gMnPc2	scenárista
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
jejích	její	k3xOp3gFnPc2	její
214	[number]	k4	214
epizod	epizoda	k1gFnPc2	epizoda
napsali	napsat	k5eAaPmAgMnP	napsat
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jonathan	Jonathan	k1gMnSc1	Jonathan
Glassner	Glassner	k1gMnSc1	Glassner
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Katharyn	Katharyn	k1gNnSc1	Katharyn
Powers	Powersa	k1gFnPc2	Powersa
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
C.	C.	kA	C.
Cooper	Cooper	k1gMnSc1	Cooper
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
DeLuise	DeLuise	k1gFnSc2	DeLuise
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
Mallozzi	Mallozze	k1gFnSc4	Mallozze
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Mullie	Mullie	k1gFnSc2	Mullie
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Damian	Damian	k1gMnSc1	Damian
Kindler	Kindler	k1gMnSc1	Kindler
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
a	a	k8xC	a
Alan	Alan	k1gMnSc1	Alan
McCullough	McCullough	k1gMnSc1	McCullough
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
epizod	epizoda	k1gFnPc2	epizoda
režírovali	režírovat	k5eAaImAgMnP	režírovat
Martin	Martin	k1gMnSc1	Martin
Wood	Wood	k1gMnSc1	Wood
(	(	kIx(	(
<g/>
46	[number]	k4	46
dílů	díl	k1gInPc2	díl
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
DeLuise	DeLuise	k1gFnSc2	DeLuise
(	(	kIx(	(
<g/>
57	[number]	k4	57
dílů	díl	k1gInPc2	díl
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wood	Wood	k1gMnSc1	Wood
a	a	k8xC	a
DeLuise	DeLuise	k1gFnSc1	DeLuise
běžně	běžně	k6eAd1	běžně
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
epizodách	epizoda	k1gFnPc6	epizoda
také	také	k9	také
cameo	cameo	k6eAd1	cameo
role	role	k1gFnPc4	role
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejznámější	známý	k2eAgFnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
role	role	k1gFnPc1	role
režisérů	režisér	k1gMnPc2	režisér
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
"	"	kIx"	"
<g/>
Seriál	seriál	k1gInSc1	seriál
Červí	červí	k2eAgFnSc1d1	červí
díra	díra	k1gFnSc1	díra
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Dvoustá	Dvoustý	k2eAgFnSc1d1	Dvoustý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
účinkovalo	účinkovat	k5eAaImAgNnS	účinkovat
v	v	k7c6	v
rolích	role	k1gFnPc6	role
fiktivního	fiktivní	k2eAgInSc2d1	fiktivní
televizního	televizní	k2eAgInSc2d1	televizní
štábu	štáb	k1gInSc2	štáb
mnoho	mnoho	k4c1	mnoho
pracovníků	pracovník	k1gMnPc2	pracovník
reálného	reálný	k2eAgInSc2d1	reálný
štábu	štáb	k1gInSc2	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc1	Anda
Mikita	Mikitum	k1gNnSc2	Mikitum
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
seriálu	seriál	k1gInSc2	seriál
asistentem	asistent	k1gMnSc7	asistent
režie	režie	k1gFnSc2	režie
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
režíroval	režírovat	k5eAaImAgMnS	režírovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
třetí	třetí	k4xOgFnSc2	třetí
a	a	k8xC	a
desáté	desátý	k4xOgFnSc2	desátý
řady	řada	k1gFnSc2	řada
29	[number]	k4	29
epizod	epizoda	k1gFnPc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
kameraman	kameraman	k1gMnSc1	kameraman
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
Peter	Peter	k1gMnSc1	Peter
Woeste	Woest	k1gMnSc5	Woest
a	a	k8xC	a
kameraman	kameraman	k1gMnSc1	kameraman
William	William	k1gInSc4	William
Waring	Waring	k1gInSc4	Waring
režírovali	režírovat	k5eAaImAgMnP	režírovat
každý	každý	k3xTgInSc4	každý
po	po	k7c6	po
13	[number]	k4	13
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
stálých	stálý	k2eAgMnPc2d1	stálý
scenáristů	scenárista	k1gMnPc2	scenárista
a	a	k8xC	a
režisérů	režisér	k1gMnPc2	režisér
působila	působit	k5eAaImAgFnS	působit
také	také	k9	také
v	v	k7c6	v
roli	role	k1gFnSc6	role
producentů	producent	k1gMnPc2	producent
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
herců	herec	k1gMnPc2	herec
naopak	naopak	k6eAd1	naopak
přispívala	přispívat	k5eAaImAgFnS	přispívat
svými	svůj	k3xOyFgInPc7	svůj
nápady	nápad	k1gInPc7	nápad
do	do	k7c2	do
příběhů	příběh	k1gInPc2	příběh
či	či	k8xC	či
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
epizodách	epizoda	k1gFnPc6	epizoda
stala	stát	k5eAaPmAgFnS	stát
rovnou	rovnou	k6eAd1	rovnou
režiséry	režisér	k1gMnPc4	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
natáčena	natáčet	k5eAaImNgFnS	natáčet
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
(	(	kIx(	(
<g/>
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
studiích	studie	k1gFnPc6	studie
The	The	k1gFnSc2	The
Bridge	Bridg	k1gFnSc2	Bridg
Studios	Studios	k?	Studios
a	a	k8xC	a
NORCO	NORCO	kA	NORCO
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
seriálu	seriál	k1gInSc3	seriál
daňové	daňový	k2eAgFnSc2d1	daňová
úlevy	úleva	k1gFnSc2	úleva
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
jedné	jeden	k4xCgFnSc2	jeden
epizody	epizoda	k1gFnSc2	epizoda
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
vzrůstala	vzrůstat	k5eAaImAgFnS	vzrůstat
z	z	k7c2	z
1,3	[number]	k4	1,3
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
řadách	řada	k1gFnPc6	řada
na	na	k7c4	na
částku	částka	k1gFnSc4	částka
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
desáté	desátý	k4xOgFnSc6	desátý
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
i	i	k9	i
kvůli	kvůli	k7c3	kvůli
nepříznivým	příznivý	k2eNgInPc3d1	nepříznivý
měnovým	měnový	k2eAgInPc3d1	měnový
kurzům	kurz	k1gInPc3	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dílů	díl	k1gInPc2	díl
bylo	být	k5eAaImAgNnS	být
zakomponováno	zakomponovat	k5eAaPmNgNnS	zakomponovat
mnoho	mnoho	k4c1	mnoho
význačných	význačný	k2eAgInPc2d1	význačný
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Vacouveru	Vacouver	k1gInSc2	Vacouver
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kampus	kampus	k1gInSc1	kampus
Simon	Simona	k1gFnPc2	Simona
Fraser	Frasra	k1gFnPc2	Frasra
University	universita	k1gFnSc2	universita
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
se	se	k3xPyFc4	se
potýkala	potýkat	k5eAaImAgFnS	potýkat
kvůli	kvůli	k7c3	kvůli
mírnému	mírný	k2eAgNnSc3d1	mírné
přímořskému	přímořský	k2eAgNnSc3d1	přímořské
podnebí	podnebí	k1gNnSc3	podnebí
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
s	s	k7c7	s
počasím	počasí	k1gNnSc7	počasí
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
déšť	déšť	k1gInSc1	déšť
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
natočeného	natočený	k2eAgInSc2d1	natočený
materiálu	materiál	k1gInSc2	materiál
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Epizoda	epizoda	k1gFnSc1	epizoda
"	"	kIx"	"
<g/>
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
lebka	lebka	k1gFnSc1	lebka
<g/>
"	"	kIx"	"
z	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
natáčena	natáčet	k5eAaImNgFnS	natáčet
ve	v	k7c6	v
virtuálním	virtuální	k2eAgNnSc6d1	virtuální
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
kulisy	kulisa	k1gFnPc1	kulisa
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgNnSc1d1	fiktivní
Velitelství	velitelství	k1gNnSc1	velitelství
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Stargate	Stargat	k1gInSc5	Stargat
Command	Commando	k1gNnPc2	Commando
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
SGC	SGC	kA	SGC
<g/>
)	)	kIx)	)
ve	v	k7c6	v
skutečné	skutečný	k2eAgFnSc6d1	skutečná
základně	základna	k1gFnSc6	základna
Letectva	letectvo	k1gNnSc2	letectvo
spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
Mountain	Mountain	k2eAgInSc1d1	Mountain
poblíž	poblíž	k6eAd1	poblíž
Colorado	Colorado	k1gNnSc4	Colorado
Springs	Springsa	k1gFnPc2	Springsa
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
5	[number]	k4	5
v	v	k7c6	v
The	The	k1gFnSc6	The
Bridge	Bridg	k1gInSc2	Bridg
Studios	Studios	k?	Studios
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
Martin	Martin	k1gMnSc1	Martin
Wood	Wood	k1gMnSc1	Wood
natočil	natočit	k5eAaBmAgMnS	natočit
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
šest	šest	k4xCc4	šest
různých	různý	k2eAgInPc2d1	různý
záběrů	záběr	k1gInPc2	záběr
skutečného	skutečný	k2eAgInSc2d1	skutečný
komplexu	komplex	k1gInSc2	komplex
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
Mountain	Mountain	k2eAgInSc1d1	Mountain
pouze	pouze	k6eAd1	pouze
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
před	před	k7c7	před
premiérou	premiéra	k1gFnSc7	premiéra
pilotní	pilotní	k2eAgFnSc2d1	pilotní
epizody	epizoda	k1gFnSc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
záběry	záběr	k1gInPc1	záběr
byly	být	k5eAaImAgInP	být
poté	poté	k6eAd1	poté
využívány	využívat	k5eAaImNgInP	využívat
po	po	k7c6	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
pro	pro	k7c4	pro
začátek	začátek	k1gInSc4	začátek
deváté	devátý	k4xOgFnSc2	devátý
řady	řada	k1gFnSc2	řada
nechali	nechat	k5eAaPmAgMnP	nechat
producenti	producent	k1gMnPc1	producent
udělat	udělat	k5eAaPmF	udělat
novou	nový	k2eAgFnSc4d1	nová
sérii	série	k1gFnSc4	série
záběrů	záběr	k1gInPc2	záběr
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
si	se	k3xPyFc3	se
předtím	předtím	k6eAd1	předtím
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
bude	být	k5eAaImBp3nS	být
každá	každý	k3xTgFnSc1	každý
sezóna	sezóna	k1gFnSc1	sezóna
tou	ten	k3xDgFnSc7	ten
poslední	poslední	k2eAgFnSc7d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Otázky	otázka	k1gFnPc1	otázka
návštěvníků	návštěvník	k1gMnPc2	návštěvník
a	a	k8xC	a
fanouškovské	fanouškovský	k2eAgFnSc2d1	fanouškovská
teorie	teorie	k1gFnSc2	teorie
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
ve	v	k7c6	v
skutečném	skutečný	k2eAgInSc6d1	skutečný
komplexu	komplex	k1gInSc6	komplex
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
Mountain	Mountain	k1gMnSc1	Mountain
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
staly	stát	k5eAaPmAgFnP	stát
tak	tak	k9	tak
běžné	běžný	k2eAgFnPc1d1	běžná
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
skladišť	skladiště	k1gNnPc2	skladiště
pro	pro	k7c4	pro
košťata	koště	k1gNnPc4	koště
a	a	k8xC	a
mycí	mycí	k2eAgFnPc4d1	mycí
potřeby	potřeba	k1gFnPc4	potřeba
nainstalovány	nainstalován	k2eAgFnPc4d1	nainstalována
dveře	dveře	k1gFnPc4	dveře
s	s	k7c7	s
replikou	replika	k1gFnSc7	replika
přísného	přísný	k2eAgNnSc2d1	přísné
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
a	a	k8xC	a
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Stargate	Stargat	k1gInSc5	Stargat
Command	Commando	k1gNnPc2	Commando
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
sedmi	sedm	k4xCc2	sedm
sezón	sezóna	k1gFnPc2	sezóna
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
22	[number]	k4	22
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgInPc1d1	zbylý
tři	tři	k4xCgInPc1	tři
řady	řad	k1gInPc1	řad
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
20	[number]	k4	20
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Epizody	epizoda	k1gFnPc1	epizoda
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
sezónách	sezóna	k1gFnPc6	sezóna
byly	být	k5eAaImAgFnP	být
natáčeny	natáčet	k5eAaImNgFnP	natáčet
během	během	k7c2	během
7,5	[number]	k4	7,5
pracovních	pracovní	k2eAgInPc2d1	pracovní
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
zredukovalo	zredukovat	k5eAaPmAgNnS	zredukovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
na	na	k7c4	na
šest	šest	k4xCc4	šest
pracovních	pracovní	k2eAgInPc2d1	pracovní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
epizody	epizoda	k1gFnPc1	epizoda
byly	být	k5eAaImAgFnP	být
natáčeny	natáčet	k5eAaImNgFnP	natáčet
v	v	k7c6	v
širokoúhlém	širokoúhlý	k2eAgInSc6d1	širokoúhlý
formátu	formát	k1gInSc6	formát
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
byly	být	k5eAaImAgFnP	být
první	první	k4xOgFnPc1	první
řady	řada	k1gFnPc1	řada
vysílány	vysílán	k2eAgFnPc1d1	vysílána
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
vysílání	vysílání	k1gNnSc4	vysílání
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
dal	dát	k5eAaPmAgMnS	dát
režisérům	režisér	k1gMnPc3	režisér
větší	veliký	k2eAgFnSc4d2	veliký
svobodu	svoboda	k1gFnSc4	svoboda
v	v	k7c6	v
kompozici	kompozice	k1gFnSc6	kompozice
záběrů	záběr	k1gInPc2	záběr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
tři	tři	k4xCgFnPc1	tři
sezóny	sezóna	k1gFnPc1	sezóna
seriálu	seriál	k1gInSc2	seriál
byly	být	k5eAaImAgFnP	být
natáčeny	natáčen	k2eAgFnPc1d1	natáčena
na	na	k7c4	na
16	[number]	k4	16
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
scény	scéna	k1gFnPc1	scéna
s	s	k7c7	s
vizuálními	vizuální	k2eAgInPc7d1	vizuální
efekty	efekt	k1gInPc7	efekt
byly	být	k5eAaImAgFnP	být
vždy	vždy	k6eAd1	vždy
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
technických	technický	k2eAgInPc2d1	technický
důvodů	důvod	k1gInPc2	důvod
natáčeny	natáčet	k5eAaImNgFnP	natáčet
na	na	k7c4	na
35	[number]	k4	35
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zkoušce	zkouška	k1gFnSc6	zkouška
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
Nemesis	Nemesis	k1gFnSc1	Nemesis
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
finále	finále	k1gNnSc6	finále
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
přešla	přejít	k5eAaPmAgFnS	přejít
produkce	produkce	k1gFnSc1	produkce
od	od	k7c2	od
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
sezóny	sezóna	k1gFnSc2	sezóna
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
35	[number]	k4	35
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgFnSc1d1	digitální
HD	HD	kA	HD
kamery	kamera	k1gFnPc1	kamera
byly	být	k5eAaImAgFnP	být
používány	používat	k5eAaImNgFnP	používat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vesmírné	vesmírný	k2eAgFnPc1d1	vesmírná
lodě	loď	k1gFnPc1	loď
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
Technologie	technologie	k1gFnSc1	technologie
ve	v	k7c6	v
Hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
bráně	brána	k1gFnSc6	brána
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
oddělení	oddělení	k1gNnSc1	oddělení
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
všechny	všechen	k3xTgInPc4	všechen
koncepty	koncept	k1gInPc4	koncept
a	a	k8xC	a
kresby	kresba	k1gFnPc4	kresba
pro	pro	k7c4	pro
oddělení	oddělení	k1gNnSc4	oddělení
rekvizit	rekvizita	k1gFnPc2	rekvizita
<g/>
,	,	kIx,	,
dekorace	dekorace	k1gFnPc1	dekorace
pro	pro	k7c4	pro
dekorační	dekorační	k2eAgNnSc4d1	dekorační
oddělení	oddělení	k1gNnSc4	oddělení
<g/>
,	,	kIx,	,
konstrukční	konstrukční	k2eAgNnSc1d1	konstrukční
oddělení	oddělení	k1gNnSc1	oddělení
a	a	k8xC	a
malířské	malířský	k2eAgNnSc1d1	malířské
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
též	též	k9	též
s	s	k7c7	s
oddělením	oddělení	k1gNnSc7	oddělení
vizuálních	vizuální	k2eAgInPc2d1	vizuální
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
zaměstnala	zaměstnat	k5eAaPmAgFnS	zaměstnat
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
kanadských	kanadský	k2eAgMnPc2d1	kanadský
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
mohl	moct	k5eAaImAgInS	moct
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
i	i	k9	i
300	[number]	k4	300
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgFnP	stavět
nové	nový	k2eAgFnPc1d1	nová
dekorace	dekorace	k1gFnPc1	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
výpravy	výprava	k1gFnSc2	výprava
Richard	Richard	k1gMnSc1	Richard
Hudolin	Hudolin	k2eAgMnSc1d1	Hudolin
se	se	k3xPyFc4	se
k	k	k7c3	k
projektu	projekt	k1gInSc2	projekt
připojil	připojit	k5eAaPmAgInS	připojit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Bridget	Bridget	k1gMnSc1	Bridget
McGuire	McGuir	k1gMnSc5	McGuir
<g/>
,	,	kIx,	,
art	art	k?	art
directorka	directorka	k1gFnSc1	directorka
seriálu	seriál	k1gInSc2	seriál
od	od	k7c2	od
jeho	jeho	k3xOp3gInSc2	jeho
počátku	počátek	k1gInSc2	počátek
<g/>
,	,	kIx,	,
převzala	převzít	k5eAaPmAgFnS	převzít
funkci	funkce	k1gFnSc4	funkce
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
výpravy	výprava	k1gFnSc2	výprava
v	v	k7c6	v
šesté	šestý	k4xOgFnSc6	šestý
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Hudolin	Hudolin	k1gInSc1	Hudolin
letěl	letět	k5eAaImAgInS	letět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
do	do	k7c2	do
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc4	Angeles
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
materiál	materiál	k1gInSc1	materiál
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
najít	najít	k5eAaPmF	najít
originální	originální	k2eAgFnPc4d1	originální
rekvizity	rekvizita	k1gFnPc4	rekvizita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
ve	v	k7c6	v
venkovním	venkovní	k2eAgNnSc6d1	venkovní
prostředí	prostředí	k1gNnSc6	prostředí
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
jejich	jejich	k3xOp3gNnSc4	jejich
značné	značný	k2eAgNnSc4d1	značné
poškození	poškození	k1gNnSc4	poškození
je	být	k5eAaImIp3nS	být
dokázal	dokázat	k5eAaPmAgMnS	dokázat
využít	využít	k5eAaPmF	využít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
posloužily	posloužit	k5eAaPmAgInP	posloužit
jako	jako	k8xS	jako
vzor	vzor	k1gInSc1	vzor
pro	pro	k7c4	pro
nově	nově	k6eAd1	nově
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
rekvizity	rekvizita	k1gFnPc4	rekvizita
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
ovládaná	ovládaný	k2eAgNnPc4d1	ovládané
počítačovým	počítačový	k2eAgInSc7d1	počítačový
programem	program	k1gInSc7	program
na	na	k7c6	na
zadávání	zadávání	k1gNnSc6	zadávání
adres	adresa	k1gFnPc2	adresa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zkonstruována	zkonstruovat	k5eAaPmNgFnS	zkonstruovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
otáčet	otáčet	k5eAaImF	otáčet
<g/>
,	,	kIx,	,
symboly	symbol	k1gInPc1	symbol
zapadaly	zapadat	k5eAaImAgInP	zapadat
<g/>
.	.	kIx.	.
</s>
<s>
Přenosná	přenosný	k2eAgFnSc1d1	přenosná
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
natáčení	natáčení	k1gNnSc4	natáčení
v	v	k7c6	v
exteriérech	exteriér	k1gInPc6	exteriér
a	a	k8xC	a
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
na	na	k7c4	na
sestavení	sestavení	k1gNnSc4	sestavení
jeden	jeden	k4xCgInSc1	jeden
den	den	k1gInSc1	den
a	a	k8xC	a
šest	šest	k4xCc1	šest
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vizuálním	vizuální	k2eAgInPc3d1	vizuální
efektům	efekt	k1gInPc3	efekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
levnější	levný	k2eAgFnSc1d2	levnější
a	a	k8xC	a
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
pozdjších	pozdjší	k2eAgFnPc6d1	pozdjší
sezónách	sezóna	k1gFnPc6	sezóna
příležitostně	příležitostně	k6eAd1	příležitostně
využívána	využívat	k5eAaImNgFnS	využívat
i	i	k9	i
počítačově	počítačově	k6eAd1	počítačově
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Dekorace	dekorace	k1gFnSc1	dekorace
SGC	SGC	kA	SGC
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
natáčení	natáčení	k1gNnSc4	natáčení
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
6,7	[number]	k4	6,7
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
maketa	maketa	k1gFnSc1	maketa
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
Hudolinových	Hudolinový	k2eAgInPc2d1	Hudolinový
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
třípodlažní	třípodlažní	k2eAgInSc4d1	třípodlažní
set	set	k1gInSc4	set
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
dvoupodlažním	dvoupodlažní	k2eAgInSc7d1	dvoupodlažní
setem	set	k1gInSc7	set
<g/>
.	.	kIx.	.
</s>
<s>
Místnost	místnost	k1gFnSc1	místnost
s	s	k7c7	s
bránou	brána	k1gFnSc7	brána
byla	být	k5eAaImAgFnS	být
největším	veliký	k2eAgInSc7d3	veliký
prostorem	prostor	k1gInSc7	prostor
při	při	k7c6	při
natáčení	natáčení	k1gNnSc6	natáčení
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
a	a	k8xC	a
upravena	upravit	k5eAaPmNgFnS	upravit
pro	pro	k7c4	pro
jiné	jiný	k2eAgFnPc4d1	jiná
scény	scéna	k1gFnPc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
víceúčelové	víceúčelový	k2eAgFnPc1d1	víceúčelová
místnosti	místnost	k1gFnPc1	místnost
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
přestavovány	přestavován	k2eAgFnPc1d1	přestavována
a	a	k8xC	a
využívány	využíván	k2eAgFnPc1d1	využívána
jako	jako	k9	jako
ošetřovna	ošetřovna	k1gFnSc1	ošetřovna
<g/>
,	,	kIx,	,
Jacksonova	Jacksonův	k2eAgFnSc1d1	Jacksonova
laboratoř	laboratoř	k1gFnSc1	laboratoř
<g/>
,	,	kIx,	,
jídelna	jídelna	k1gFnSc1	jídelna
nebo	nebo	k8xC	nebo
tělocvična	tělocvična	k1gFnSc1	tělocvična
<g/>
.	.	kIx.	.
</s>
<s>
Dekorace	dekorace	k1gFnSc1	dekorace
SGC	SGC	kA	SGC
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
sety	set	k1gInPc1	set
použité	použitý	k2eAgInPc1d1	použitý
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
během	během	k7c2	během
šesti	šest	k4xCc2	šest
týdnů	týden	k1gInPc2	týden
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
začleněny	začlenit	k5eAaPmNgFnP	začlenit
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
dekorací	dekorace	k1gFnPc2	dekorace
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Set	set	k1gInSc1	set
SGC	SGC	kA	SGC
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
demontován	demontovat	k5eAaBmNgInS	demontovat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
prostor	prostor	k1gInSc1	prostor
byl	být	k5eAaImAgInS	být
využit	využít	k5eAaPmNgInS	využít
pro	pro	k7c4	pro
dekorace	dekorace	k1gFnPc4	dekorace
základny	základna	k1gFnSc2	základna
Icarus	Icarus	k1gInSc4	Icarus
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
seriálu	seriál	k1gInSc2	seriál
jsou	být	k5eAaImIp3nP	být
američtí	americký	k2eAgMnPc1d1	americký
letci	letec	k1gMnPc1	letec
a	a	k8xC	a
nosí	nosit	k5eAaImIp3nP	nosit
autentické	autentický	k2eAgFnPc1d1	autentická
uniformy	uniforma	k1gFnPc1	uniforma
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
misí	mise	k1gFnPc2	mise
jsou	být	k5eAaImIp3nP	být
členové	člen	k1gMnPc1	člen
SG-1	SG-1	k1gFnSc2	SG-1
běžně	běžně	k6eAd1	běžně
oblečeni	oblečen	k2eAgMnPc1d1	oblečen
do	do	k7c2	do
olivově	olivově	k6eAd1	olivově
zelených	zelený	k2eAgFnPc2d1	zelená
uniforem	uniforma	k1gFnPc2	uniforma
typu	typ	k1gInSc2	typ
Battle	Battle	k1gFnSc2	Battle
Dress	Dressa	k1gFnPc2	Dressa
<g/>
.	.	kIx.	.
</s>
<s>
Richardu	Richard	k1gMnSc3	Richard
Deanu	Dean	k1gMnSc3	Dean
Andersonovi	Anderson	k1gMnSc3	Anderson
a	a	k8xC	a
Donu	Don	k1gMnSc3	Don
S.	S.	kA	S.
Davisovi	Davis	k1gMnSc3	Davis
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
natáčením	natáčení	k1gNnSc7	natáčení
prováděn	prováděn	k2eAgInSc1d1	prováděn
sestřih	sestřih	k1gInSc1	sestřih
vlasů	vlas	k1gInPc2	vlas
do	do	k7c2	do
řádného	řádný	k2eAgInSc2d1	řádný
vojenského	vojenský	k2eAgInSc2d1	vojenský
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Amanda	Amanda	k1gFnSc1	Amanda
Tapping	Tapping	k1gInSc1	Tapping
měla	mít	k5eAaImAgFnS	mít
vlasy	vlas	k1gInPc4	vlas
podobně	podobně	k6eAd1	podobně
krátké	krátký	k2eAgInPc4d1	krátký
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
natáčení	natáčení	k1gNnSc2	natáčení
obou	dva	k4xCgFnPc2	dva
DVD	DVD	kA	DVD
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Michael	Michael	k1gMnSc1	Michael
Shanks	Shanksa	k1gFnPc2	Shanksa
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
civilistu	civilista	k1gMnSc4	civilista
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgInS	převzít
účes	účes	k1gInSc1	účes
od	od	k7c2	od
Jamese	Jamese	k1gFnSc2	Jamese
Spadera	Spadero	k1gNnSc2	Spadero
z	z	k7c2	z
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
snímku	snímek	k1gInSc2	snímek
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
si	se	k3xPyFc3	se
ale	ale	k9	ale
vlasy	vlas	k1gInPc1	vlas
natrvalo	natrvalo	k6eAd1	natrvalo
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
<g/>
.	.	kIx.	.
</s>
<s>
Jaffa	Jaffa	k1gFnSc1	Jaffa
Teal	Teala	k1gFnPc2	Teala
<g/>
'	'	kIx"	'
<g/>
c	c	k0	c
(	(	kIx(	(
<g/>
Christopher	Christophra	k1gFnPc2	Christophra
Judge	Judge	k1gInSc1	Judge
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
jedinou	jediný	k2eAgFnSc4d1	jediná
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
základní	základní	k2eAgInSc4d1	základní
make-up	makep	k1gInSc4	make-up
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
egyptská	egyptský	k2eAgFnSc1d1	egyptská
vizáž	vizáž	k1gFnSc1	vizáž
odráží	odrážet	k5eAaImIp3nS	odrážet
vzhled	vzhled	k1gInSc4	vzhled
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
ulda	ulda	k1gFnSc1	ulda
Ra	ra	k0	ra
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
doplněna	doplnit	k5eAaPmNgFnS	doplnit
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
symbol	symbol	k1gInSc4	symbol
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
a	a	k8xC	a
nazlátlý	nazlátlý	k2eAgInSc1d1	nazlátlý
odstín	odstín	k1gInSc1	odstín
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
make-up	makep	k1gInSc1	make-up
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
zjednodušen	zjednodušit	k5eAaPmNgInS	zjednodušit
<g/>
.	.	kIx.	.
</s>
<s>
Judge	Judge	k6eAd1	Judge
si	se	k3xPyFc3	se
také	také	k9	také
doma	doma	k6eAd1	doma
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
holil	holit	k5eAaImAgMnS	holit
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
osmé	osmý	k4xOgFnSc6	osmý
řadě	řada	k1gFnSc6	řada
mu	on	k3xPp3gMnSc3	on
producenti	producent	k1gMnPc1	producent
povolili	povolit	k5eAaPmAgMnP	povolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
vlasy	vlas	k1gInPc4	vlas
ponechal	ponechat	k5eAaPmAgInS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
vyškolená	vyškolený	k2eAgFnSc1d1	vyškolená
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
sestra	sestra	k1gFnSc1	sestra
dokázala	dokázat	k5eAaPmAgFnS	dokázat
hlavní	hlavní	k2eAgFnSc1d1	hlavní
maskérka	maskérka	k1gFnSc1	maskérka
Jan	Jan	k1gMnSc1	Jan
Newman	Newman	k1gMnSc1	Newman
vytvořit	vytvořit	k5eAaPmF	vytvořit
popáleniny	popálenina	k1gFnPc4	popálenina
<g/>
,	,	kIx,	,
řezné	řezný	k2eAgFnPc4d1	řezná
rány	rána	k1gFnPc4	rána
či	či	k8xC	či
otlaky	otlak	k1gInPc4	otlak
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vypadala	vypadat	k5eAaPmAgFnS	vypadat
zranění	zranění	k1gNnSc3	zranění
týmu	tým	k1gInSc2	tým
SG-1	SG-1	k1gFnSc7	SG-1
autenticky	autenticky	k6eAd1	autenticky
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vzhled	vzhled	k1gInSc4	vzhled
mimozemšťanů	mimozemšťan	k1gMnPc2	mimozemšťan
spolupracovalo	spolupracovat	k5eAaImAgNnS	spolupracovat
maskérské	maskérský	k2eAgNnSc4d1	maskérský
oddělení	oddělení	k1gNnSc4	oddělení
s	s	k7c7	s
protetickými	protetický	k2eAgFnPc7d1	protetická
společnostmi	společnost	k1gFnPc7	společnost
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Todd	Todda	k1gFnPc2	Todda
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
)	)	kIx)	)
z	z	k7c2	z
Vancouveru	Vancouver	k1gInSc2	Vancouver
a	a	k8xC	a
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
lidský	lidský	k2eAgInSc1d1	lidský
původ	původ	k1gInSc1	původ
mnoha	mnoho	k4c2	mnoho
mimozemských	mimozemský	k2eAgFnPc2d1	mimozemská
ras	rasa	k1gFnPc2	rasa
a	a	k8xC	a
lidských	lidský	k2eAgFnPc2d1	lidská
civilizací	civilizace	k1gFnPc2	civilizace
byl	být	k5eAaImAgInS	být
lehce	lehko	k6eAd1	lehko
rozeznatelný	rozeznatelný	k2eAgMnSc1d1	rozeznatelný
<g/>
,	,	kIx,	,
rasa	rasa	k1gFnSc1	rasa
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Unasové	Unasové	k2eAgFnSc1d1	Unasové
potřebovala	potřebovat	k5eAaImAgFnS	potřebovat
komplikované	komplikovaný	k2eAgFnPc4d1	komplikovaná
masky	maska	k1gFnPc4	maska
a	a	k8xC	a
make-up	makep	k1gInSc4	make-up
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ukázání	ukázání	k1gNnSc4	ukázání
nezávislého	závislý	k2eNgInSc2d1	nezávislý
vývoje	vývoj	k1gInSc2	vývoj
lidských	lidský	k2eAgFnPc2d1	lidská
civilizací	civilizace	k1gFnPc2	civilizace
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
přemístění	přemístění	k1gNnSc6	přemístění
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
použili	použít	k5eAaPmAgMnP	použít
kostyméři	kostymér	k1gMnPc1	kostymér
prvky	prvek	k1gInPc7	prvek
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
pozemských	pozemský	k2eAgFnPc2d1	pozemská
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
zkombinovali	zkombinovat	k5eAaPmAgMnP	zkombinovat
je	on	k3xPp3gInPc4	on
s	s	k7c7	s
moderními	moderní	k2eAgFnPc7d1	moderní
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
komplikovanými	komplikovaný	k2eAgFnPc7d1	komplikovaná
ozdobami	ozdoba	k1gFnPc7	ozdoba
a	a	k8xC	a
řetízky	řetízek	k1gInPc7	řetízek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
dojem	dojem	k1gInSc4	dojem
historických	historický	k2eAgInPc2d1	historický
oděvů	oděv	k1gInPc2	oděv
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldů	uld	k1gInPc2	uld
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Apophise	Apophise	k1gFnSc2	Apophise
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
vzhledu	vzhled	k1gInSc2	vzhled
Ra	ra	k0	ra
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
vizáže	vizáž	k1gFnSc2	vizáž
Oriů	Ori	k1gMnPc2	Ori
a	a	k8xC	a
Převorů	převor	k1gMnPc2	převor
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
kostýmů	kostým	k1gInPc2	kostým
v	v	k7c6	v
deváté	devátý	k4xOgFnSc6	devátý
řadě	řada	k1gFnSc6	řada
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
od	od	k7c2	od
samurajů	samuraj	k1gMnPc2	samuraj
<g/>
.	.	kIx.	.
</s>
<s>
Art	Art	k?	Art
director	director	k1gInSc1	director
James	James	k1gMnSc1	James
Robbins	Robbinsa	k1gFnPc2	Robbinsa
také	také	k9	také
našel	najít	k5eAaPmAgInS	najít
u	u	k7c2	u
malých	malý	k2eAgInPc2d1	malý
kmenů	kmen	k1gInPc2	kmen
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
mystické	mystický	k2eAgFnSc2d1	mystická
kresby	kresba	k1gFnSc2	kresba
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
<g/>
,	,	kIx,	,
jizvy	jizva	k1gFnPc1	jizva
i	i	k8xC	i
popáleniny	popálenina	k1gFnPc1	popálenina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
použil	použít	k5eAaPmAgMnS	použít
jako	jako	k8xC	jako
inspiraci	inspirace	k1gFnSc4	inspirace
pro	pro	k7c4	pro
pozměněné	pozměněný	k2eAgFnPc4d1	pozměněná
tváře	tvář	k1gFnPc4	tvář
Převorů	převor	k1gMnPc2	převor
a	a	k8xC	a
Dociho	Doci	k1gMnSc2	Doci
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
prodloužení	prodloužení	k1gNnSc4	prodloužení
jejich	jejich	k3xOp3gInPc2	jejich
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
zjizvení	zjizvení	k1gNnSc1	zjizvení
rukou	ruka	k1gFnPc2	ruka
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
nepraktické	praktický	k2eNgFnPc1d1	nepraktická
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
zaměstnavatelů	zaměstnavatel	k1gMnPc2	zaměstnavatel
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
vizuálních	vizuální	k2eAgInPc2d1	vizuální
efektů	efekt	k1gInPc2	efekt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
produkce	produkce	k1gFnSc1	produkce
utrácela	utrácet	k5eAaImAgFnS	utrácet
400	[number]	k4	400
000	[number]	k4	000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
dodavatelem	dodavatel	k1gMnSc7	dodavatel
byla	být	k5eAaImAgFnS	být
společnost	společnost	k1gFnSc4	společnost
Rainmaker	Rainmaker	k1gInSc1	Rainmaker
Digital	Digital	kA	Digital
Effects	Effects	k1gInSc1	Effects
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
tvůrce	tvůrce	k1gMnSc1	tvůrce
digitálních	digitální	k2eAgInPc2d1	digitální
efektů	efekt	k1gInPc2	efekt
Bruce	Bruce	k1gMnSc1	Bruce
Woloshyn	Woloshyn	k1gMnSc1	Woloshyn
pracoval	pracovat	k5eAaImAgMnS	pracovat
průměrně	průměrně	k6eAd1	průměrně
deset	deset	k4xCc4	deset
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
ze	z	k7c2	z
štábu	štáb	k1gInSc2	štáb
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
supervizorem	supervizor	k1gInSc7	supervizor
vizuálních	vizuální	k2eAgInPc2d1	vizuální
efektů	efekt	k1gInPc2	efekt
a	a	k8xC	a
producentem	producent	k1gMnSc7	producent
Jamesem	James	k1gMnSc7	James
Tichenorem	Tichenor	k1gMnSc7	Tichenor
a	a	k8xC	a
supervizorem	supervizor	k1gMnSc7	supervizor
vizuálních	vizuální	k2eAgInPc2d1	vizuální
efektů	efekt	k1gInPc2	efekt
Michellem	Michell	k1gMnSc7	Michell
Comensem	Comens	k1gMnSc7	Comens
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
najali	najmout	k5eAaPmAgMnP	najmout
několik	několik	k4yIc4	několik
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
vodě	voda	k1gFnSc6	voda
podobný	podobný	k2eAgInSc1d1	podobný
horizont	horizont	k1gInSc1	horizont
událostí	událost	k1gFnPc2	událost
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rainmaker	Rainmaker	k1gInSc4	Rainmaker
byla	být	k5eAaImAgFnS	být
jediná	jediný	k2eAgFnSc1d1	jediná
firma	firma	k1gFnSc1	firma
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
běžné	běžný	k2eAgInPc1d1	běžný
záběry	záběr	k1gInPc1	záběr
s	s	k7c7	s
efekty	efekt	k1gInPc7	efekt
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
seriálu	seriál	k1gInSc2	seriál
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
aktivaci	aktivace	k1gFnSc4	aktivace
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
samotné	samotný	k2eAgFnSc2d1	samotná
brány	brána	k1gFnSc2	brána
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
300	[number]	k4	300
horizontů	horizont	k1gInPc2	horizont
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
několika	několik	k4yIc6	několik
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
transportní	transportní	k2eAgInPc1d1	transportní
kruhy	kruh	k1gInPc1	kruh
a	a	k8xC	a
paprskové	paprskový	k2eAgInPc1d1	paprskový
výstřely	výstřel	k1gInPc1	výstřel
tyčových	tyčový	k2eAgFnPc2d1	tyčová
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
zat	zat	k?	zat
<g/>
'	'	kIx"	'
<g/>
nik	nika	k1gFnPc2	nika
<g/>
'	'	kIx"	'
<g/>
atelů	atel	k1gInPc2	atel
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
pravidelně	pravidelně	k6eAd1	pravidelně
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
také	také	k9	také
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
goa	goa	k?	goa
<g/>
'	'	kIx"	'
<g/>
uldských	uldský	k2eAgFnPc2d1	uldská
nákladních	nákladní	k2eAgFnPc2d1	nákladní
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
útočných	útočný	k2eAgInPc2d1	útočný
kluzáků	kluzák	k1gInPc2	kluzák
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
seriálu	seriál	k1gInSc2	seriál
do	do	k7c2	do
konce	konec	k1gInSc2	konec
páté	pátý	k4xOgFnSc2	pátý
řady	řada	k1gFnSc2	řada
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
některé	některý	k3yIgInPc4	některý
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
společnost	společnost	k1gFnSc1	společnost
Lost	Losta	k1gFnPc2	Losta
Boys	boy	k1gMnPc2	boy
Studios	Studios	k?	Studios
<g/>
,	,	kIx,	,
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
se	se	k3xPyFc4	se
na	na	k7c6	na
Hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
bráně	brána	k1gFnSc6	brána
podílela	podílet	k5eAaImAgFnS	podílet
také	také	k9	také
firma	firma	k1gFnSc1	firma
Image	image	k1gFnSc1	image
Engine	Engin	k1gMnSc5	Engin
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
navazujícím	navazující	k2eAgInSc7d1	navazující
seriálem	seriál	k1gInSc7	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
tvořila	tvořit	k5eAaImAgFnS	tvořit
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
30	[number]	k4	30
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
obchodu	obchod	k1gInSc2	obchod
společnosti	společnost	k1gFnSc2	společnost
Atmosphere	Atmospher	k1gInSc5	Atmospher
Visual	Visual	k1gMnSc1	Visual
Effects	Effects	k1gInSc1	Effects
<g/>
.	.	kIx.	.
</s>
<s>
Supervizor	supervizor	k1gMnSc1	supervizor
James	James	k1gMnSc1	James
Tichenor	Tichenor	k1gMnSc1	Tichenor
považoval	považovat	k5eAaImAgMnS	považovat
několik	několik	k4yIc4	několik
epizod	epizoda	k1gFnPc2	epizoda
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
rozpočtem	rozpočet	k1gInSc7	rozpočet
pro	pro	k7c4	pro
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
za	za	k7c4	za
oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgNnP	moct
zaujmout	zaujmout	k5eAaPmF	zaujmout
i	i	k8xC	i
poroty	porota	k1gFnSc2	porota
různých	různý	k2eAgFnPc2d1	různá
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
pomohl	pomoct	k5eAaPmAgInS	pomoct
vejít	vejít	k5eAaPmF	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
místním	místní	k2eAgFnPc3d1	místní
postprodukčním	postprodukční	k2eAgFnPc3d1	postprodukční
firmám	firma	k1gFnPc3	firma
<g/>
,	,	kIx,	,
epizody	epizoda	k1gFnPc4	epizoda
"	"	kIx"	"
<g/>
Malá	malý	k2eAgNnPc4d1	malé
vítězství	vítězství	k1gNnPc4	vítězství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Překvapivá	překvapivý	k2eAgNnPc4d1	překvapivé
odhalení	odhalení	k1gNnPc4	odhalení
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Ztracené	ztracený	k2eAgNnSc1d1	ztracené
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
obdržely	obdržet	k5eAaPmAgFnP	obdržet
několik	několik	k4yIc4	několik
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
nominací	nominace	k1gFnPc2	nominace
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
vizuálních	vizuální	k2eAgInPc2d1	vizuální
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
skladatele	skladatel	k1gMnSc2	skladatel
Joela	Joel	k1gMnSc4	Joel
Goldsmithe	Goldsmith	k1gMnSc4	Goldsmith
měla	mít	k5eAaImAgFnS	mít
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
tradiční	tradiční	k2eAgFnSc4d1	tradiční
akčně-dobrodružnou	akčněobrodružný	k2eAgFnSc4d1	akčně-dobrodružný
hudbu	hudba	k1gFnSc4	hudba
se	s	k7c7	s
"	"	kIx"	"
<g/>
sci-fi	scii	k1gFnPc4	sci-fi
<g/>
,	,	kIx,	,
fantasy	fantas	k1gInPc4	fantas
stylem	styl	k1gInSc7	styl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
"	"	kIx"	"
<g/>
od	od	k7c2	od
komedie	komedie	k1gFnSc2	komedie
k	k	k7c3	k
dramatu	drama	k1gNnSc3	drama
<g/>
,	,	kIx,	,
k	k	k7c3	k
ohromnosti	ohromnost	k1gFnSc3	ohromnost
<g/>
,	,	kIx,	,
k	k	k7c3	k
napětí	napětí	k1gNnSc3	napětí
<g/>
,	,	kIx,	,
k	k	k7c3	k
akčnosti	akčnost	k1gFnSc3	akčnost
<g/>
,	,	kIx,	,
k	k	k7c3	k
éteričnosti	éteričnost	k1gFnSc3	éteričnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Wright	Wrightum	k1gNnPc2	Wrightum
s	s	k7c7	s
Jonathanem	Jonathan	k1gMnSc7	Jonathan
Glassnerem	Glassner	k1gMnSc7	Glassner
znali	znát	k5eAaImAgMnP	znát
Goldsmithe	Goldsmithe	k1gFnSc4	Goldsmithe
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
sezóny	sezóna	k1gFnSc2	sezóna
seriálu	seriál	k1gInSc2	seriál
Krajní	krajní	k2eAgFnPc4d1	krajní
meze	mez	k1gFnPc4	mez
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
přizvali	přizvat	k5eAaPmAgMnP	přizvat
i	i	k9	i
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
připravovaného	připravovaný	k2eAgInSc2d1	připravovaný
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Goldsmith	Goldsmith	k1gMnSc1	Goldsmith
a	a	k8xC	a
David	David	k1gMnSc1	David
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
bavili	bavit	k5eAaImAgMnP	bavit
o	o	k7c6	o
hudebních	hudební	k2eAgNnPc6d1	hudební
tématech	téma	k1gNnPc6	téma
pro	pro	k7c4	pro
televizní	televizní	k2eAgFnSc4d1	televizní
adaptaci	adaptace	k1gFnSc4	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
znělku	znělka	k1gFnSc4	znělka
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
jako	jako	k9	jako
medley	medlea	k1gFnSc2	medlea
několika	několik	k4yIc2	několik
témat	téma	k1gNnPc2	téma
z	z	k7c2	z
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
snímku	snímek	k1gInSc2	snímek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Goldsmith	Goldsmith	k1gInSc1	Goldsmith
napsal	napsat	k5eAaPmAgInS	napsat
i	i	k9	i
vlastní	vlastní	k2eAgFnSc4d1	vlastní
koncovou	koncový	k2eAgFnSc4d1	koncová
znělku	znělka	k1gFnSc4	znělka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
seriál	seriál	k1gInSc1	seriál
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k8xS	jako
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
entita	entita	k1gFnSc1	entita
<g/>
.	.	kIx.	.
</s>
<s>
MGM	MGM	kA	MGM
trvalo	trvat	k5eAaImAgNnS	trvat
na	na	k7c6	na
použití	použití	k1gNnSc6	použití
Arnoldovy	Arnoldův	k2eAgFnSc2d1	Arnoldova
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
použil	použít	k5eAaPmAgMnS	použít
Goldsmithovu	Goldsmithův	k2eAgFnSc4d1	Goldsmithova
hudbu	hudba	k1gFnSc4	hudba
v	v	k7c6	v
nově	nově	k6eAd1	nově
sestříhané	sestříhaný	k2eAgFnSc6d1	sestříhaná
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
na	na	k7c6	na
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
rozpočtových	rozpočtový	k2eAgInPc2d1	rozpočtový
důvodů	důvod	k1gInPc2	důvod
využit	využit	k2eAgInSc1d1	využit
skutečný	skutečný	k2eAgInSc1d1	skutečný
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
epizodu	epizoda	k1gFnSc4	epizoda
Goldsmith	Goldsmith	k1gInSc1	Goldsmith
nasimuloval	nasimulovat	k5eAaPmAgInS	nasimulovat
na	na	k7c6	na
syntezátorech	syntezátor	k1gInPc6	syntezátor
80	[number]	k4	80
<g/>
členný	členný	k2eAgInSc1d1	členný
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
ale	ale	k9	ale
využil	využít	k5eAaPmAgInS	využít
i	i	k9	i
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgMnPc4	tři
hudebníky	hudebník	k1gMnPc4	hudebník
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
orchestrální	orchestrální	k2eAgFnSc2d1	orchestrální
autenticity	autenticita	k1gFnSc2	autenticita
<g/>
.	.	kIx.	.
</s>
<s>
Goldsmithův	Goldsmithův	k2eAgMnSc1d1	Goldsmithův
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
asistent	asistent	k1gMnSc1	asistent
Neal	Neal	k1gMnSc1	Neal
Acree	Acree	k1gInSc4	Acree
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
osmé	osmý	k4xOgFnSc6	osmý
sezóně	sezóna	k1gFnSc6	sezóna
seriálu	seriál	k1gInSc2	seriál
skládat	skládat	k5eAaImF	skládat
doprovodnou	doprovodný	k2eAgFnSc4d1	doprovodná
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
zkomponované	zkomponovaný	k2eAgFnSc2d1	zkomponovaná
hudby	hudba	k1gFnSc2	hudba
pro	pro	k7c4	pro
jednu	jeden	k4xCgFnSc4	jeden
44	[number]	k4	44
<g/>
minutovou	minutový	k2eAgFnSc4d1	minutová
epizodu	epizoda	k1gFnSc4	epizoda
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
mezi	mezi	k7c7	mezi
12	[number]	k4	12
a	a	k8xC	a
33	[number]	k4	33
minutami	minuta	k1gFnPc7	minuta
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
kolem	kolem	k7c2	kolem
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
běžných	běžný	k2eAgInPc2d1	běžný
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
pro	pro	k7c4	pro
skladatele	skladatel	k1gMnSc4	skladatel
tak	tak	k6eAd1	tak
činilo	činit	k5eAaImAgNnS	činit
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
časově	časově	k6eAd1	časově
náročnější	náročný	k2eAgMnSc1d2	náročnější
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Goldsmith	Goldsmith	k1gMnSc1	Goldsmith
bydlel	bydlet	k5eAaImAgMnS	bydlet
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
,	,	kIx,	,
domlouval	domlouvat	k5eAaImAgMnS	domlouvat
se	se	k3xPyFc4	se
s	s	k7c7	s
producenty	producent	k1gMnPc7	producent
pomocí	pomocí	k7c2	pomocí
telefonu	telefon	k1gInSc2	telefon
a	a	k8xC	a
výměnu	výměna	k1gFnSc4	výměna
nahraných	nahraný	k2eAgInPc2d1	nahraný
pásků	pásek	k1gInPc2	pásek
realizoval	realizovat	k5eAaBmAgMnS	realizovat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
přes	přes	k7c4	přes
Federal	Federal	k1gFnSc4	Federal
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebylo	být	k5eNaImAgNnS	být
přistoupeno	přistoupen	k2eAgNnSc1d1	přistoupeno
k	k	k7c3	k
přenosům	přenos	k1gInPc3	přenos
souborů	soubor	k1gInPc2	soubor
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
<g/>
.	.	kIx.	.
</s>
<s>
Goldsmithova	Goldsmithův	k2eAgFnSc1d1	Goldsmithova
důvěra	důvěra	k1gFnSc1	důvěra
v	v	k7c4	v
Arnoldovu	Arnoldův	k2eAgFnSc4d1	Arnoldova
hudbu	hudba	k1gFnSc4	hudba
se	se	k3xPyFc4	se
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
snižovala	snižovat	k5eAaImAgFnS	snižovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
seriál	seriál	k1gInSc1	seriál
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
odpoutával	odpoutávat	k5eAaImAgMnS	odpoutávat
od	od	k7c2	od
tématu	téma	k1gNnSc2	téma
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldů	uld	k1gInPc2	uld
a	a	k8xC	a
uváděl	uvádět	k5eAaImAgMnS	uvádět
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
nové	nový	k2eAgFnSc2d1	nová
postavy	postava	k1gFnSc2	postava
a	a	k8xC	a
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
však	však	k9	však
k	k	k7c3	k
rasám	rasa	k1gFnPc3	rasa
a	a	k8xC	a
vesmírným	vesmírný	k2eAgFnPc3d1	vesmírná
lodím	loď	k1gFnPc3	loď
tematický	tematický	k2eAgInSc4d1	tematický
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
chtěl	chtít	k5eAaImAgInS	chtít
mechanický	mechanický	k2eAgInSc1d1	mechanický
<g/>
,	,	kIx,	,
repetitivní	repetitivní	k2eAgInSc1d1	repetitivní
hudební	hudební	k2eAgInSc4d1	hudební
motiv	motiv	k1gInSc4	motiv
pro	pro	k7c4	pro
Replikátory	replikátor	k1gInPc4	replikátor
<g/>
,	,	kIx,	,
gotická	gotický	k2eAgNnPc1d1	gotické
<g/>
,	,	kIx,	,
gregoriánská	gregoriánský	k2eAgNnPc1d1	gregoriánské
a	a	k8xC	a
křesťanská	křesťanský	k2eAgNnPc1d1	křesťanské
hudební	hudební	k2eAgNnPc1d1	hudební
témata	téma	k1gNnPc1	téma
byla	být	k5eAaImAgNnP	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
orijský	orijský	k2eAgInSc4d1	orijský
motiv	motiv	k1gInSc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Antické	antický	k2eAgNnSc1d1	antické
téma	téma	k1gNnSc1	téma
bylo	být	k5eAaImAgNnS	být
záměrně	záměrně	k6eAd1	záměrně
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
základní	základní	k2eAgFnSc1d1	základní
melodie	melodie	k1gFnSc1	melodie
z	z	k7c2	z
konce	konec	k1gInSc2	konec
dvojepizody	dvojepizoda	k1gFnSc2	dvojepizoda
"	"	kIx"	"
<g/>
Ztracené	ztracený	k2eAgNnSc4d1	ztracené
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
stala	stát	k5eAaPmAgFnS	stát
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Goldsmithova	Goldsmithův	k2eAgMnSc2d1	Goldsmithův
asistenta	asistent	k1gMnSc2	asistent
částí	část	k1gFnPc2	část
hlavní	hlavní	k2eAgFnSc2d1	hlavní
znělky	znělka	k1gFnSc2	znělka
Atlantidy	Atlantida	k1gFnSc2	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Nepůvodní	původní	k2eNgFnSc1d1	nepůvodní
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
bráně	brána	k1gFnSc6	brána
použita	použít	k5eAaPmNgFnS	použít
pouze	pouze	k6eAd1	pouze
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Goldsmith	Goldsmith	k1gMnSc1	Goldsmith
vybral	vybrat	k5eAaPmAgMnS	vybrat
pro	pro	k7c4	pro
epizodu	epizoda	k1gFnSc4	epizoda
"	"	kIx"	"
<g/>
Odstíny	odstín	k1gInPc1	odstín
šedi	šeď	k1gFnSc2	šeď
<g/>
"	"	kIx"	"
z	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
árii	árie	k1gFnSc4	árie
"	"	kIx"	"
<g/>
Vesti	Vesti	k1gNnSc4	Vesti
la	la	k1gNnSc2	la
giubba	giubb	k1gMnSc2	giubb
<g/>
"	"	kIx"	"
z	z	k7c2	z
Leoncavallových	Leoncavallův	k2eAgMnPc2d1	Leoncavallův
Komediantů	komediant	k1gMnPc2	komediant
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
využil	využít	k5eAaPmAgMnS	využít
také	také	k9	také
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Who	Who	k1gFnSc1	Who
am	am	k?	am
I	i	k9	i
<g/>
"	"	kIx"	"
od	od	k7c2	od
Lily	lít	k5eAaImAgFnP	lít
Frost	Frost	k1gFnSc1	Frost
(	(	kIx(	(
<g/>
epizoda	epizoda	k1gFnSc1	epizoda
"	"	kIx"	"
<g/>
Křehká	křehký	k2eAgFnSc1d1	křehká
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
"	"	kIx"	"
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
epizodě	epizoda	k1gFnSc6	epizoda
seriálu	seriál	k1gInSc2	seriál
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
<g/>
"	"	kIx"	"
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Have	Have	k1gFnSc1	Have
You	You	k1gMnSc1	You
Ever	Ever	k1gMnSc1	Ever
Seen	Seen	k1gMnSc1	Seen
the	the	k?	the
Rain	Rain	k1gMnSc1	Rain
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
od	od	k7c2	od
Creedence	Creedence	k1gFnSc2	Creedence
Clearwater	Clearwater	k1gInSc1	Clearwater
Revival	revival	k1gInSc1	revival
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
soundtrack	soundtrack	k1gInSc1	soundtrack
s	s	k7c7	s
Goldsmithem	Goldsmith	k1gInSc7	Goldsmith
upravenou	upravený	k2eAgFnSc7d1	upravená
hudbou	hudba	k1gFnSc7	hudba
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
je	být	k5eAaImIp3nS	být
následoval	následovat	k5eAaImAgInS	následovat
best	best	k1gInSc4	best
of	of	k?	of
výběr	výběr	k1gInSc1	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
měl	mít	k5eAaImAgMnS	mít
několik	několik	k4yIc4	několik
variant	varianta	k1gFnPc2	varianta
sekvencí	sekvence	k1gFnPc2	sekvence
úvodních	úvodní	k2eAgInPc2d1	úvodní
titulků	titulek	k1gInPc2	titulek
se	s	k7c7	s
znělkou	znělka	k1gFnSc7	znělka
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
obvykle	obvykle	k6eAd1	obvykle
předcházel	předcházet	k5eAaImAgInS	předcházet
teaser	teaser	k1gInSc1	teaser
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
60	[number]	k4	60
sekund	sekunda	k1gFnPc2	sekunda
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Dean	Dean	k1gMnSc1	Dean
Anderson	Anderson	k1gMnSc1	Anderson
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
hercem	herec	k1gMnSc7	herec
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
jméno	jméno	k1gNnSc4	jméno
se	se	k3xPyFc4	se
na	na	k7c6	na
obrazovce	obrazovka	k1gFnSc6	obrazovka
objevilo	objevit	k5eAaPmAgNnS	objevit
ještě	ještě	k9	ještě
před	před	k7c7	před
názvem	název	k1gInSc7	název
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Michaela	Michael	k1gMnSc2	Michael
Shankse	Shanks	k1gMnSc2	Shanks
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
návratu	návrat	k1gInSc6	návrat
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
řadě	řada	k1gFnSc6	řada
přemístěno	přemístěn	k2eAgNnSc1d1	přemístěno
na	na	k7c4	na
konec	konec	k1gInSc4	konec
titulků	titulek	k1gInPc2	titulek
s	s	k7c7	s
doplňkem	doplněk	k1gInSc7	doplněk
"	"	kIx"	"
<g/>
as	as	k1gInSc1	as
Daniel	Daniel	k1gMnSc1	Daniel
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
jako	jako	k8xS	jako
Daniel	Daniel	k1gMnSc1	Daniel
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgNnSc1	některý
DVD	DVD	kA	DVD
verze	verze	k1gFnSc1	verze
prvních	první	k4xOgFnPc2	první
sezón	sezóna	k1gFnPc2	sezóna
seriálu	seriál	k1gInSc2	seriál
mají	mít	k5eAaImIp3nP	mít
úvodní	úvodní	k2eAgInPc1d1	úvodní
titulky	titulek	k1gInPc1	titulek
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
televizní	televizní	k2eAgFnSc2d1	televizní
verze	verze	k1gFnSc2	verze
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
DVD	DVD	kA	DVD
filmy	film	k1gInPc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
Joel	Joel	k1gMnSc1	Joel
Goldmith	Goldmith	k1gMnSc1	Goldmith
upravil	upravit	k5eAaPmAgMnS	upravit
pro	pro	k7c4	pro
úvodní	úvodní	k2eAgInPc4d1	úvodní
titulky	titulek	k1gInPc4	titulek
hudbu	hudba	k1gFnSc4	hudba
David	David	k1gMnSc1	David
Arnolda	Arnold	k1gMnSc2	Arnold
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
filmu	film	k1gInSc2	film
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
zůstala	zůstat	k5eAaPmAgFnS	zůstat
stejná	stejný	k2eAgFnSc1d1	stejná
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
vysílání	vysílání	k1gNnSc2	vysílání
seriálu	seriál	k1gInSc2	seriál
i	i	k8xC	i
na	na	k7c6	na
DVD	DVD	kA	DVD
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
úvodní	úvodní	k2eAgFnSc1d1	úvodní
sekvence	sekvence	k1gFnSc1	sekvence
titulků	titulek	k1gInPc2	titulek
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
pěti	pět	k4xCc6	pět
řadách	řada	k1gFnPc6	řada
a	a	k8xC	a
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
zblízka	zblízka	k6eAd1	zblízka
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
pohybující	pohybující	k2eAgFnSc7d1	pohybující
kamerou	kamera	k1gFnSc7	kamera
Raovou	Raova	k1gFnSc7	Raova
maskou	maska	k1gFnSc7	maska
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
seriálu	seriál	k1gInSc2	seriál
již	již	k6eAd1	již
před	před	k7c7	před
premiérou	premiéra	k1gFnSc7	premiéra
první	první	k4xOgFnSc2	první
sezóny	sezóna	k1gFnSc2	sezóna
neměli	mít	k5eNaImAgMnP	mít
čas	čas	k1gInSc4	čas
a	a	k8xC	a
tak	tak	k6eAd1	tak
jednoduše	jednoduše	k6eAd1	jednoduše
použili	použít	k5eAaPmAgMnP	použít
lehce	lehko	k6eAd1	lehko
zrychlenou	zrychlený	k2eAgFnSc4d1	zrychlená
sekvenci	sekvence	k1gFnSc4	sekvence
záběrů	záběr	k1gInPc2	záběr
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Raova	Raova	k6eAd1	Raova
maska	maska	k1gFnSc1	maska
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
a	a	k8xC	a
původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
kamerou	kamera	k1gFnSc7	kamera
s	s	k7c7	s
kontrolou	kontrola	k1gFnSc7	kontrola
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
kvůli	kvůli	k7c3	kvůli
šilhajícím	šilhající	k2eAgNnPc3d1	šilhající
očím	oko	k1gNnPc3	oko
masky	maska	k1gFnSc2	maska
se	se	k3xPyFc4	se
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gInSc1	Wright
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
umělecké	umělecký	k2eAgNnSc4d1	umělecké
oddělení	oddělení	k1gNnSc4	oddělení
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
nové	nový	k2eAgFnSc2d1	nová
úvodní	úvodní	k2eAgFnSc2d1	úvodní
sekvence	sekvence	k1gFnSc2	sekvence
s	s	k7c7	s
titulky	titulek	k1gInPc7	titulek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ta	ten	k3xDgFnSc1	ten
původní	původní	k2eAgFnSc1d1	původní
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
seriál	seriál	k1gInSc1	seriál
přešel	přejít	k5eAaPmAgInS	přejít
pod	pod	k7c7	pod
Sci-Fi	scii	k1gFnSc7	sci-fi
Channel	Channela	k1gFnPc2	Channela
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
seriál	seriál	k1gInSc4	seriál
prodána	prodat	k5eAaPmNgFnS	prodat
dalších	další	k2eAgNnPc6d1	další
televizím	televize	k1gFnPc3	televize
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
prvních	první	k4xOgInPc2	první
pět	pět	k4xCc4	pět
sezón	sezóna	k1gFnPc2	sezóna
použit	použít	k5eAaPmNgInS	použít
odlišný	odlišný	k2eAgInSc1d1	odlišný
úvod	úvod	k1gInSc1	úvod
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
akční	akční	k2eAgInPc4d1	akční
záběry	záběr	k1gInPc4	záběr
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Sekvence	sekvence	k1gFnSc1	sekvence
úvodních	úvodní	k2eAgInPc2d1	úvodní
titulků	titulek	k1gInPc2	titulek
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
dvou	dva	k4xCgFnPc6	dva
epizodách	epizoda	k1gFnPc6	epizoda
šesté	šestý	k4xOgFnSc2	šestý
řady	řada	k1gFnSc2	řada
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
otáčející	otáčející	k2eAgNnSc4d1	otáčející
se	se	k3xPyFc4	se
rekvizitu	rekvizit	k1gInSc2	rekvizit
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
3	[number]	k4	3
mm	mm	kA	mm
snímána	snímat	k5eAaImNgFnS	snímat
Frazierovým	Frazierův	k2eAgInSc7d1	Frazierův
objektivem	objektiv	k1gInSc7	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
epizodách	epizoda	k1gFnPc6	epizoda
částečně	částečně	k6eAd1	částečně
využily	využít	k5eAaPmAgFnP	využít
tento	tento	k3xDgInSc4	tento
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
sestřihem	sestřih	k1gInSc7	sestřih
akčních	akční	k2eAgInPc2d1	akční
záběrů	záběr	k1gInPc2	záběr
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
předchozích	předchozí	k2eAgInPc2d1	předchozí
dílů	díl	k1gInPc2	díl
a	a	k8xC	a
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
vstupem	vstup	k1gInSc7	vstup
týmu	tým	k1gInSc2	tým
SG-1	SG-1	k1gFnSc2	SG-1
do	do	k7c2	do
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
s	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
úpravami	úprava	k1gFnPc7	úprava
záběrů	záběr	k1gInPc2	záběr
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
obsazení	obsazení	k1gNnSc6	obsazení
<g/>
,	,	kIx,	,
vydržela	vydržet	k5eAaPmAgFnS	vydržet
úvodní	úvodní	k2eAgFnSc1d1	úvodní
sekvence	sekvence	k1gFnSc1	sekvence
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc1	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
deváté	devátý	k4xOgFnSc2	devátý
řady	řada	k1gFnSc2	řada
byly	být	k5eAaImAgInP	být
titulky	titulek	k1gInPc1	titulek
podobné	podobný	k2eAgFnSc2d1	podobná
předchozím	předchozí	k2eAgNnSc7d1	předchozí
<g/>
,	,	kIx,	,
odlišné	odlišný	k2eAgInPc1d1	odlišný
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
záběry	záběr	k1gInPc1	záběr
otáčející	otáčející	k2eAgInPc1d1	otáčející
se	se	k3xPyFc4	se
brány	brána	k1gFnPc1	brána
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
viditelně	viditelně	k6eAd1	viditelně
provedeny	provést	k5eAaPmNgFnP	provést
počítačovou	počítačový	k2eAgFnSc7d1	počítačová
grafikou	grafika	k1gFnSc7	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Sci-Fi	scii	k1gFnSc1	sci-fi
Channel	Channela	k1gFnPc2	Channela
také	také	k9	také
při	při	k7c6	při
premiéře	premiéra	k1gFnSc6	premiéra
epizod	epizoda	k1gFnPc2	epizoda
první	první	k4xOgFnSc6	první
poloviny	polovina	k1gFnSc2	polovina
deváté	devátý	k4xOgFnSc2	devátý
sezóny	sezóna	k1gFnSc2	sezóna
zkrátil	zkrátit	k5eAaPmAgInS	zkrátit
úvodní	úvodní	k2eAgFnSc4d1	úvodní
sekvenci	sekvence	k1gFnSc4	sekvence
ze	z	k7c2	z
60	[number]	k4	60
na	na	k7c4	na
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
silně	silně	k6eAd1	silně
negativní	negativní	k2eAgFnPc4d1	negativní
reakce	reakce	k1gFnPc4	reakce
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
fanoušky	fanoušek	k1gMnPc4	fanoušek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stanice	stanice	k1gFnSc1	stanice
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
řady	řada	k1gFnSc2	řada
vrátila	vrátit	k5eAaPmAgFnS	vrátit
klasickou	klasický	k2eAgFnSc4d1	klasická
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Scenáristé	scenárista	k1gMnPc1	scenárista
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
vtip	vtip	k1gInSc4	vtip
do	do	k7c2	do
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
Dvoustá	Dvoustý	k2eAgFnSc1d1	Dvoustý
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
pouze	pouze	k6eAd1	pouze
pětisekundový	pětisekundový	k2eAgInSc4d1	pětisekundový
titulek	titulek	k1gInSc4	titulek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
epizody	epizoda	k1gFnSc2	epizoda
desáté	desátá	k1gFnSc2	desátá
řady	řada	k1gFnSc2	řada
"	"	kIx"	"
<g/>
Spolek	spolek	k1gInSc1	spolek
zlodějů	zloděj	k1gMnPc2	zloděj
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
úvodní	úvodní	k2eAgFnSc2d1	úvodní
sekvence	sekvence	k1gFnSc2	sekvence
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Vala	Vala	k1gMnSc1	Vala
Mal	málit	k5eAaImRp2nS	málit
Doran	Doran	k1gInSc1	Doran
téměř	téměř	k6eAd1	téměř
nestihne	stihnout	k5eNaPmIp3nS	stihnout
SG-1	SG-1	k1gFnSc1	SG-1
vstupující	vstupující	k2eAgFnSc1d1	vstupující
do	do	k7c2	do
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
producenty	producent	k1gMnPc7	producent
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
přes	přes	k7c4	přes
Kancelář	kancelář	k1gFnSc4	kancelář
letectva	letectvo	k1gNnSc2	letectvo
pro	pro	k7c4	pro
styk	styk	k1gInSc4	styk
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
a	a	k8xC	a
zábavním	zábavní	k2eAgInSc7d1	zábavní
průmyslem	průmysl	k1gInSc7	průmysl
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
odbor	odbor	k1gInSc1	odbor
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
výroby	výroba	k1gFnSc2	výroba
seriálu	seriál	k1gInSc2	seriál
garantovalo	garantovat	k5eAaBmAgNnS	garantovat
letectvo	letectvo	k1gNnSc1	letectvo
produkci	produkce	k1gFnSc4	produkce
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
komplexu	komplex	k1gInSc3	komplex
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
Mountain	Mountain	k1gInSc4	Mountain
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
pořízeny	pořídit	k5eAaPmNgInP	pořídit
jeho	jeho	k3xOp3gInPc1	jeho
reálné	reálný	k2eAgInPc1d1	reálný
záběry	záběr	k1gInPc1	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Letectvo	letectvo	k1gNnSc1	letectvo
(	(	kIx(	(
<g/>
USAF	USAF	kA	USAF
<g/>
)	)	kIx)	)
rovněž	rovněž	k9	rovněž
četlo	číst	k5eAaImAgNnS	číst
každý	každý	k3xTgInSc4	každý
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nebyly	být	k5eNaImAgFnP	být
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
a	a	k8xC	a
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
pomoc	pomoc	k1gFnSc4	pomoc
s	s	k7c7	s
realistickými	realistický	k2eAgInPc7d1	realistický
životními	životní	k2eAgInPc7d1	životní
příběhy	příběh	k1gInPc7	příběh
všech	všecek	k3xTgFnPc2	všecek
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
se	s	k7c7	s
stuhami	stuha	k1gFnPc7	stuha
<g/>
,	,	kIx,	,
uniformami	uniforma	k1gFnPc7	uniforma
<g/>
,	,	kIx,	,
sestřihy	sestřih	k1gInPc7	sestřih
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
dějovými	dějový	k2eAgFnPc7d1	dějová
linkami	linka	k1gFnPc7	linka
a	a	k8xC	a
ostatními	ostatní	k2eAgFnPc7d1	ostatní
věcmi	věc	k1gFnPc7	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
souvisejí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
USAF	USAF	kA	USAF
také	také	k9	také
nechalo	nechat	k5eAaPmAgNnS	nechat
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
k	k	k7c3	k
Vancouveru	Vancouvero	k1gNnSc3	Vancouvero
několik	několik	k4yIc4	několik
letounů	letoun	k1gInPc2	letoun
T-	T-	k1gFnSc2	T-
<g/>
38	[number]	k4	38
<g/>
,	,	kIx,	,
F-15	F-15	k1gFnSc1	F-15
a	a	k8xC	a
F-	F-	k1gFnSc1	F-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
záběry	záběr	k1gInPc1	záběr
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
epizodách	epizoda	k1gFnPc6	epizoda
a	a	k8xC	a
DVD	DVD	kA	DVD
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
postav	postava	k1gFnPc2	postava
ztvárňujících	ztvárňující	k2eAgFnPc2d1	ztvárňující
personál	personál	k1gInSc4	personál
USAF	USAF	kA	USAF
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc1	jeho
skuteční	skutečný	k2eAgMnPc1d1	skutečný
příslušníci	příslušník	k1gMnPc1	příslušník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
bráně	brána	k1gFnSc6	brána
ztvárnili	ztvárnit	k5eAaPmAgMnP	ztvárnit
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
dva	dva	k4xCgMnPc1	dva
náčelníci	náčelník	k1gMnPc1	náčelník
štábu	štáb	k1gInSc2	štáb
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Michael	Michael	k1gMnSc1	Michael
E.	E.	kA	E.
Ryan	Ryan	k1gInSc1	Ryan
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Zázračné	zázračný	k2eAgNnSc1d1	zázračné
dítě	dítě	k1gNnSc1	dítě
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
John	John	k1gMnSc1	John
P.	P.	kA	P.
Jumper	jumper	k1gInSc1	jumper
hrál	hrát	k5eAaImAgInS	hrát
ve	v	k7c6	v
dvojepizodě	dvojepizod	k1gInSc6	dvojepizod
"	"	kIx"	"
<g/>
Ztracené	ztracený	k2eAgNnSc1d1	ztracené
město	město	k1gNnSc1	město
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
vystoupení	vystoupení	k1gNnSc1	vystoupení
generála	generál	k1gMnSc2	generál
Jumpera	Jumper	k1gMnSc2	Jumper
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
pro	pro	k7c4	pro
dvojepizodu	dvojepizoda	k1gFnSc4	dvojepizoda
"	"	kIx"	"
<g/>
Čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
jezdec	jezdec	k1gMnSc1	jezdec
apokalypsy	apokalypsa	k1gFnSc2	apokalypsa
<g/>
"	"	kIx"	"
deváté	devátý	k4xOgFnSc2	devátý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
trvajícímu	trvající	k2eAgInSc3d1	trvající
konfliktu	konflikt	k1gInSc3	konflikt
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
ale	ale	k8xC	ale
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc1	force
Association	Association	k1gInSc1	Association
pozvala	pozvat	k5eAaPmAgFnS	pozvat
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
57	[number]	k4	57
<g/>
.	.	kIx.	.
výroční	výroční	k2eAgFnSc6d1	výroční
večeři	večeře	k1gFnSc6	večeře
konanou	konaný	k2eAgFnSc4d1	konaná
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2004	[number]	k4	2004
Richarda	Richard	k1gMnSc4	Richard
Deana	Dean	k1gMnSc4	Dean
Andersona	Anderson	k1gMnSc4	Anderson
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
oceněn	ocenit	k5eAaPmNgInS	ocenit
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
herce	herec	k1gMnSc2	herec
a	a	k8xC	a
výkonného	výkonný	k2eAgMnSc2d1	výkonný
producenta	producent	k1gMnSc2	producent
v	v	k7c6	v
Hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
bráně	brána	k1gFnSc6	brána
a	a	k8xC	a
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
popis	popis	k1gInSc4	popis
Letectva	letectvo	k1gNnSc2	letectvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
scény	scéna	k1gFnPc1	scéna
z	z	k7c2	z
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
Malá	malý	k2eAgNnPc4d1	malé
vítězství	vítězství	k1gNnPc4	vítězství
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
natáčeny	natáčet	k5eAaImNgFnP	natáčet
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
a	a	k8xC	a
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vyřazené	vyřazený	k2eAgFnSc2d1	vyřazená
ruské	ruský	k2eAgFnSc2d1	ruská
ponorky	ponorka	k1gFnSc2	ponorka
třídy	třída	k1gFnSc2	třída
Foxtrot	foxtrot	k1gInSc1	foxtrot
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
soukromý	soukromý	k2eAgMnSc1d1	soukromý
majitel	majitel	k1gMnSc1	majitel
koupil	koupit	k5eAaPmAgMnS	koupit
ve	v	k7c6	v
Vladivostoku	Vladivostok	k1gInSc6	Vladivostok
a	a	k8xC	a
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
také	také	k9	také
pro	pro	k7c4	pro
natáčení	natáčení	k1gNnSc4	natáčení
DVD	DVD	kA	DVD
filmu	film	k1gInSc2	film
Návrat	návrat	k1gInSc1	návrat
pozvalo	pozvat	k5eAaPmAgNnS	pozvat
herce	herec	k1gMnSc2	herec
a	a	k8xC	a
štáb	štáb	k1gInSc1	štáb
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
ponorky	ponorka	k1gFnSc2	ponorka
USS	USS	kA	USS
Alexandria	Alexandrium	k1gNnSc2	Alexandrium
a	a	k8xC	a
do	do	k7c2	do
polární	polární	k2eAgFnSc2d1	polární
stanice	stanice	k1gFnSc2	stanice
Applied	Applied	k1gMnSc1	Applied
Physics	Physics	k1gInSc1	Physics
Laboratory	Laborator	k1gInPc1	Laborator
Ice	Ice	k1gFnSc1	Ice
Station	station	k1gInSc1	station
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc1	dabing
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2000	[number]	k4	2000
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
šest	šest	k4xCc4	šest
řad	řad	k1gInSc4	řad
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
v	v	k7c6	v
letech	let	k1gInPc6	let
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
pro	pro	k7c4	pro
CET	ceta	k1gFnPc2	ceta
21	[number]	k4	21
společnost	společnost	k1gFnSc4	společnost
Česká	český	k2eAgFnSc1d1	Česká
produkční	produkční	k2eAgFnSc1d1	produkční
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
servisní	servisní	k2eAgFnSc1d1	servisní
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnSc2d1	ostatní
sezóny	sezóna	k1gFnSc2	sezóna
produkovala	produkovat	k5eAaImAgFnS	produkovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
firma	firma	k1gFnSc1	firma
DW	DW	kA	DW
Agentura	agentura	k1gFnSc1	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Dabing	dabing	k1gInSc1	dabing
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
deseti	deset	k4xCc6	deset
řadách	řada	k1gFnPc6	řada
režíroval	režírovat	k5eAaImAgMnS	režírovat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Sezóny	sezóna	k1gFnPc1	sezóna
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
přeložili	přeložit	k5eAaPmAgMnP	přeložit
společně	společně	k6eAd1	společně
Renáta	Renáta	k1gFnSc1	Renáta
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Studení	studenět	k5eAaImIp3nS	studenět
<g/>
,	,	kIx,	,
sezóny	sezóna	k1gFnSc2	sezóna
5	[number]	k4	5
a	a	k8xC	a
6	[number]	k4	6
pouze	pouze	k6eAd1	pouze
Tomáš	Tomáš	k1gMnSc1	Tomáš
Studený	Studený	k1gMnSc1	Studený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zbylých	zbylý	k2eAgFnPc6d1	zbylá
řadách	řada	k1gFnPc6	řada
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
více	hodně	k6eAd2	hodně
překladatelů	překladatel	k1gMnPc2	překladatel
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Havlíček	Havlíček	k1gMnSc1	Havlíček
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Chalupecký	Chalupecký	k2eAgMnSc1d1	Chalupecký
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kostiha	Kostiha	k1gMnSc1	Kostiha
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Urbánek	Urbánek	k1gMnSc1	Urbánek
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Valentinová	Valentinová	k1gFnSc1	Valentinová
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zenáhlík	Zenáhlík	k1gMnSc1	Zenáhlík
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlasové	hlasový	k2eAgNnSc1d1	hlasové
obsazení	obsazení	k1gNnSc1	obsazení
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
seriálu	seriál	k1gInSc2	seriál
zachováno	zachován	k2eAgNnSc1d1	zachováno
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
výjimkou	výjimka	k1gFnSc7	výjimka
byla	být	k5eAaImAgFnS	být
postava	postava	k1gFnSc1	postava
Jonase	Jonasa	k1gFnSc3	Jonasa
Quinna	Quinn	k1gInSc2	Quinn
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
při	při	k7c6	při
hostování	hostování	k1gNnPc4	hostování
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
uvedené	uvedený	k2eAgFnSc2d1	uvedená
po	po	k7c6	po
čtyřleté	čtyřletý	k2eAgFnSc6d1	čtyřletá
přestávce	přestávka	k1gFnSc6	přestávka
od	od	k7c2	od
předchozí	předchozí	k2eAgFnSc2d1	předchozí
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
daboval	dabovat	k5eAaBmAgMnS	dabovat
Marek	Marek	k1gMnSc1	Marek
Libert	Libert	k1gMnSc1	Libert
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
páté	pátá	k1gFnSc6	pátá
a	a	k8xC	a
šesté	šestý	k4xOgFnSc3	šestý
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
Saša	Saša	k1gMnSc1	Saša
Rašilov	Rašilov	k1gInSc4	Rašilov
<g/>
.	.	kIx.	.
</s>
<s>
Dodržovány	dodržován	k2eAgFnPc1d1	dodržována
však	však	k9	však
byly	být	k5eAaImAgInP	být
hlasy	hlas	k1gInPc1	hlas
i	i	k9	i
důležitých	důležitý	k2eAgFnPc2d1	důležitá
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
výměně	výměna	k1gFnSc6	výměna
překladatelů	překladatel	k1gMnPc2	překladatel
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
část	část	k1gFnSc1	část
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
využity	využit	k2eAgInPc1d1	využit
originální	originální	k2eAgInPc1d1	originální
názvy	název	k1gInPc1	název
(	(	kIx(	(
<g/>
sonda	sonda	k1gFnSc1	sonda
×	×	k?	×
MALP	malpa	k1gFnPc2	malpa
<g/>
,	,	kIx,	,
symbol	symbol	k1gInSc1	symbol
×	×	k?	×
chevron	chevron	k1gInSc1	chevron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
goa	goa	k?	goa
<g/>
'	'	kIx"	'
<g/>
uldského	uldský	k1gMnSc2	uldský
lorda	lord	k1gMnSc2	lord
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dabingu	dabing	k1gInSc6	dabing
předchozích	předchozí	k2eAgFnPc2d1	předchozí
sezón	sezóna	k1gFnPc2	sezóna
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
tvar	tvar	k1gInSc4	tvar
změněno	změnit	k5eAaPmNgNnS	změnit
kvůli	kvůli	k7c3	kvůli
zachování	zachování	k1gNnSc3	zachování
vtipu	vtip	k1gInSc2	vtip
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
lord	lord	k1gMnSc1	lord
Ju	ju	k0	ju
podle	podle	k7c2	podle
původního	původní	k2eAgNnSc2d1	původní
anglického	anglický	k2eAgNnSc2d1	anglické
jména	jméno	k1gNnSc2	jméno
Yu	Yu	k1gFnSc2	Yu
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
pět	pět	k4xCc4	pět
dílů	díl	k1gInPc2	díl
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
reprízu	repríza	k1gFnSc4	repríza
dodatečně	dodatečně	k6eAd1	dodatečně
dabingově	dabingově	k6eAd1	dabingově
upraveno	upravit	k5eAaPmNgNnS	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mytologie	mytologie	k1gFnSc2	mytologie
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vojenské	vojenský	k2eAgFnSc2d1	vojenská
sci-fi	scii	k1gFnSc2	sci-fi
a	a	k8xC	a
využila	využít	k5eAaPmAgFnS	využít
běžných	běžný	k2eAgFnPc2d1	běžná
sci-fi	scii	k1gFnPc2	sci-fi
konceptů	koncept	k1gInPc2	koncept
o	o	k7c6	o
silných	silný	k2eAgFnPc6d1	silná
a	a	k8xC	a
rozdílných	rozdílný	k2eAgFnPc6d1	rozdílná
postavách	postava	k1gFnPc6	postava
bojujících	bojující	k2eAgFnPc6d1	bojující
proti	proti	k7c3	proti
jednoznačnému	jednoznačný	k2eAgMnSc3d1	jednoznačný
zlému	zlý	k1gMnSc3	zlý
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
,	,	kIx,	,
Goa	Goa	k1gFnSc1	Goa
<g/>
'	'	kIx"	'
<g/>
uldům	uld	k1gInPc3	uld
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
mimozemských	mimozemský	k2eAgFnPc2d1	mimozemská
ras	rasa	k1gFnPc2	rasa
s	s	k7c7	s
divákům	divák	k1gMnPc3	divák
známou	známý	k2eAgFnSc7d1	známá
pozemskou	pozemský	k2eAgFnSc7d1	pozemská
mytologií	mytologie	k1gFnSc7	mytologie
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
díky	díky	k7c3	díky
samotné	samotný	k2eAgFnSc3d1	samotná
hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
bráně	brána	k1gFnSc3	brána
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
okamžité	okamžitý	k2eAgNnSc1d1	okamžité
meziplanetární	meziplanetární	k2eAgNnSc1d1	meziplanetární
cestování	cestování	k1gNnSc1	cestování
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlé	rychlý	k2eAgFnPc4d1	rychlá
změny	změna	k1gFnPc4	změna
ve	v	k7c6	v
vyprávění	vyprávění	k1gNnSc6	vyprávění
mezi	mezi	k7c7	mezi
politikou	politika	k1gFnSc7	politika
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
realitou	realita	k1gFnSc7	realita
boje	boj	k1gInSc2	boj
v	v	k7c6	v
mezihvězdné	mezihvězdný	k2eAgFnSc6d1	mezihvězdná
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
postupně	postupně	k6eAd1	postupně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
základní	základní	k2eAgFnSc4d1	základní
premisu	premisa	k1gFnSc4	premisa
původního	původní	k2eAgInSc2d1	původní
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
filmu	film	k1gInSc2	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
do	do	k7c2	do
vlastní	vlastní	k2eAgFnSc2d1	vlastní
unikátní	unikátní	k2eAgFnSc2d1	unikátní
mytologické	mytologický	k2eAgFnSc2d1	mytologická
superstruktury	superstruktura	k1gFnSc2	superstruktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
na	na	k7c4	na
egyptské	egyptský	k2eAgNnSc4d1	egyptské
náboženství	náboženství	k1gNnSc4	náboženství
(	(	kIx(	(
<g/>
především	především	k9	především
bohové	bůh	k1gMnPc1	bůh
Apop	Apop	k1gMnSc1	Apop
<g/>
/	/	kIx~	/
<g/>
Apophis	Apophis	k1gInSc1	Apophis
a	a	k8xC	a
Anup	Anup	k1gInSc1	Anup
<g/>
/	/	kIx~	/
<g/>
Anubis	Anubis	k1gInSc1	Anubis
jako	jako	k8xS	jako
goa	goa	k?	goa
<g/>
'	'	kIx"	'
<g/>
uldští	uldský	k2eAgMnPc1d1	uldský
protivníci	protivník	k1gMnPc1	protivník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severskou	severský	k2eAgFnSc4d1	severská
mytologii	mytologie	k1gFnSc4	mytologie
(	(	kIx(	(
<g/>
především	především	k9	především
bůh	bůh	k1gMnSc1	bůh
Thór	Thór	k1gMnSc1	Thór
jako	jako	k8xC	jako
asgardský	asgardský	k2eAgMnSc1d1	asgardský
spojenec	spojenec	k1gMnSc1	spojenec
<g/>
)	)	kIx)	)
a	a	k8xC	a
artušovské	artušovský	k2eAgFnPc1d1	artušovská
legendy	legenda	k1gFnPc1	legenda
(	(	kIx(	(
<g/>
především	především	k9	především
Merlin	Merlin	k2eAgMnSc1d1	Merlin
jako	jako	k8xS	jako
antický	antický	k2eAgMnSc1d1	antický
spojenec	spojenec	k1gMnSc1	spojenec
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c6	po
bohům	bůh	k1gMnPc3	bůh
podobné	podobný	k2eAgFnPc4d1	podobná
Orie	Ori	k1gFnPc4	Ori
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
uvádí	uvádět	k5eAaImIp3nS	uvádět
nové	nový	k2eAgFnSc2d1	nová
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
častým	častý	k2eAgFnPc3d1	častá
mimozemským	mimozemský	k2eAgFnPc3d1	mimozemská
lidským	lidský	k2eAgFnPc3d1	lidská
civilizacím	civilizace	k1gFnPc3	civilizace
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
televizních	televizní	k2eAgFnPc6d1	televizní
sci-fi	scii	k1gFnPc6	sci-fi
seriálech	seriál	k1gInPc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
nově	nově	k6eAd1	nově
objevené	objevený	k2eAgFnPc1d1	objevená
rasy	rasa	k1gFnPc1	rasa
i	i	k8xC	i
civilizace	civilizace	k1gFnPc1	civilizace
či	či	k8xC	či
navštívené	navštívený	k2eAgFnPc1d1	navštívená
planety	planeta	k1gFnPc1	planeta
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
epizodách	epizoda	k1gFnPc6	epizoda
bývají	bývat	k5eAaImIp3nP	bývat
poté	poté	k6eAd1	poté
zařazeny	zařadit	k5eAaPmNgInP	zařadit
do	do	k7c2	do
už	už	k6eAd1	už
existující	existující	k2eAgFnSc2d1	existující
mytologie	mytologie	k1gFnSc2	mytologie
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
opouští	opouštět	k5eAaImIp3nS	opouštět
příběhy	příběh	k1gInPc4	příběh
jednoduše	jednoduše	k6eAd1	jednoduše
přístupné	přístupný	k2eAgFnPc4d1	přístupná
pro	pro	k7c4	pro
nové	nový	k2eAgMnPc4d1	nový
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
mezigalaktickou	mezigalaktický	k2eAgFnSc4d1	mezigalaktická
mytologii	mytologie	k1gFnSc4	mytologie
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
sci-fi	scii	k1gFnSc1	sci-fi
prvky	prvek	k1gInPc4	prvek
považuje	považovat	k5eAaImIp3nS	považovat
M.	M.	kA	M.
Keith	Keith	k1gInSc1	Keith
Booker	Booker	k1gInSc1	Booker
seriál	seriál	k1gInSc4	seriál
za	za	k7c4	za
silně	silně	k6eAd1	silně
závislý	závislý	k2eAgInSc4d1	závislý
na	na	k7c6	na
postavách	postava	k1gFnPc6	postava
a	a	k8xC	a
kamarádství	kamarádství	k1gNnSc4	kamarádství
mezi	mezi	k7c7	mezi
členy	člen	k1gMnPc7	člen
SG-	SG-	k1gFnSc2	SG-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
si	se	k3xPyFc3	se
osvojili	osvojit	k5eAaPmAgMnP	osvojit
i	i	k8xC	i
humor	humor	k1gInSc4	humor
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
zábavnou	zábavný	k2eAgFnSc4d1	zábavná
show	show	k1gFnSc4	show
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
nebere	brát	k5eNaImIp3nS	brát
příliš	příliš	k6eAd1	příliš
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Wright	Wrighta	k1gFnPc2	Wrighta
považoval	považovat	k5eAaImAgInS	považovat
seriál	seriál	k1gInSc1	seriál
za	za	k7c4	za
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
show	show	k1gFnSc4	show
s	s	k7c7	s
přiměřeným	přiměřený	k2eAgNnSc7d1	přiměřené
násilím	násilí	k1gNnSc7	násilí
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
náhodného	náhodný	k2eAgNnSc2d1	náhodné
nebo	nebo	k8xC	nebo
bezdůvodného	bezdůvodný	k2eAgNnSc2d1	bezdůvodné
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
Judge	Judge	k1gNnPc2	Judge
si	se	k3xPyFc3	se
nemyslel	myslet	k5eNaImAgInS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
by	by	kYmCp3nS	by
"	"	kIx"	"
<g/>
i	i	k9	i
přes	přes	k7c4	přes
bujnou	bujný	k2eAgFnSc4d1	bujná
fantazii	fantazie	k1gFnSc4	fantazie
byla	být	k5eAaImAgFnS	být
seriálem	seriál	k1gInSc7	seriál
s	s	k7c7	s
poselstvím	poselství	k1gNnSc7	poselství
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nějaká	nějaký	k3yIgFnSc1	nějaký
sdělení	sdělení	k1gNnSc3	sdělení
objevila	objevit	k5eAaPmAgFnS	objevit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dílo	dílo	k1gNnSc1	dílo
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
na	na	k7c4	na
široké	široký	k2eAgNnSc4d1	široké
publikum	publikum	k1gNnSc4	publikum
odkazoval	odkazovat	k5eAaImAgInS	odkazovat
seriál	seriál	k1gInSc1	seriál
často	často	k6eAd1	často
na	na	k7c4	na
populární	populární	k2eAgFnSc4d1	populární
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
svoje	svůj	k3xOyFgInPc4	svůj
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
děj	děj	k1gInSc1	děj
probíhal	probíhat	k5eAaImAgInS	probíhat
na	na	k7c6	na
současné	současný	k2eAgFnSc6d1	současná
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
například	například	k6eAd1	například
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
Akta	akta	k1gNnPc1	akta
X	X	kA	X
a	a	k8xC	a
Buffy	buffa	k1gFnSc2	buffa
<g/>
,	,	kIx,	,
přemožitelka	přemožitelka	k1gFnSc1	přemožitelka
upírů	upír	k1gMnPc2	upír
<g/>
.	.	kIx.	.
</s>
<s>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Glassner	Glassner	k1gMnSc1	Glassner
psal	psát	k5eAaImAgMnS	psát
do	do	k7c2	do
vlastních	vlastní	k2eAgInPc2d1	vlastní
scénářů	scénář	k1gInPc2	scénář
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
sezónách	sezóna	k1gFnPc6	sezóna
odkazy	odkaz	k1gInPc7	odkaz
na	na	k7c4	na
Čaroděje	čaroděj	k1gMnPc4	čaroděj
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
Oz	Oz	k1gFnSc2	Oz
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
ostatní	ostatní	k2eAgMnPc1d1	ostatní
scenáristé	scenárista	k1gMnPc1	scenárista
přijali	přijmout	k5eAaPmAgMnP	přijmout
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Richard	Richard	k1gMnSc1	Richard
Dean	Dean	k1gMnSc1	Dean
Anderson	Anderson	k1gMnSc1	Anderson
začal	začít	k5eAaPmAgMnS	začít
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
i	i	k9	i
sám	sám	k3xTgInSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neilla	Neilla	k1gFnSc1	Neilla
se	se	k3xPyFc4	se
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
také	také	k9	také
často	často	k6eAd1	často
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
Simpsonových	Simpsonových	k2eAgMnSc6d1	Simpsonových
<g/>
,	,	kIx,	,
Andersonově	Andersonův	k2eAgInSc6d1	Andersonův
oblíbeném	oblíbený	k2eAgInSc6d1	oblíbený
seriálu	seriál	k1gInSc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
v	v	k7c6	v
několika	několik	k4yIc6	několik
epizodách	epizoda	k1gFnPc6	epizoda
rovněž	rovněž	k9	rovněž
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
proces	proces	k1gInSc4	proces
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
natáčení	natáčení	k1gNnSc2	natáčení
televizních	televizní	k2eAgNnPc2d1	televizní
sci-fi	scii	k1gNnPc2	sci-fi
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
či	či	k8xC	či
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
předchozí	předchozí	k2eAgFnPc4d1	předchozí
role	role	k1gFnPc4	role
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
(	(	kIx(	(
<g/>
zmínka	zmínka	k1gFnSc1	zmínka
Carterové	Carterová	k1gFnSc2	Carterová
o	o	k7c6	o
MacGyverovi	MacGyver	k1gMnSc6	MacGyver
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
dabingu	dabing	k1gInSc6	dabing
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
krátké	krátký	k2eAgFnSc6d1	krátká
ukázce	ukázka	k1gFnSc6	ukázka
seriálu	seriál	k1gInSc2	seriál
Farscape	Farscap	k1gInSc5	Farscap
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Dvoustá	Dvoustý	k2eAgFnSc1d1	Dvoustý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
kanál	kanál	k1gInSc1	kanál
pro	pro	k7c4	pro
předplatitele	předplatitel	k1gMnPc4	předplatitel
Showtime	Showtim	k1gInSc5	Showtim
objednal	objednat	k5eAaPmAgMnS	objednat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
dvě	dva	k4xCgFnPc4	dva
řady	řada	k1gFnPc4	řada
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
44	[number]	k4	44
epizodami	epizoda	k1gFnPc7	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Dvouhodinový	dvouhodinový	k2eAgInSc1d1	dvouhodinový
pilotní	pilotní	k2eAgInSc1d1	pilotní
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
epizody	epizoda	k1gFnPc4	epizoda
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nejlépe	dobře	k6eAd3	dobře
sledovaným	sledovaný	k2eAgNnSc7d1	sledované
premiérovým	premiérový	k2eAgNnSc7d1	premiérové
dílem	dílo	k1gNnSc7	dílo
seriálu	seriál	k1gInSc2	seriál
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
od	od	k7c2	od
20	[number]	k4	20
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dívalo	dívat	k5eAaImAgNnS	dívat
přibližně	přibližně	k6eAd1	přibližně
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
producentů	producent	k1gMnPc2	producent
seriálu	seriál	k1gInSc2	seriál
chtěla	chtít	k5eAaImAgFnS	chtít
vysílací	vysílací	k2eAgFnSc1d1	vysílací
síť	síť	k1gFnSc1	síť
zrušit	zrušit	k5eAaPmF	zrušit
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
po	po	k7c6	po
několika	několik	k4yIc6	několik
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
kanál	kanál	k1gInSc1	kanál
ale	ale	k8xC	ale
na	na	k7c4	na
seriál	seriál	k1gInSc4	seriál
nijak	nijak	k6eAd1	nijak
netlačil	tlačit	k5eNaImAgMnS	tlačit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
"	"	kIx"	"
<g/>
získat	získat	k5eAaPmF	získat
pořádnou	pořádný	k2eAgFnSc4d1	pořádná
sledovanost	sledovanost	k1gFnSc4	sledovanost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
síti	síť	k1gFnSc3	síť
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
umí	umět	k5eAaImIp3nS	umět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
nejsledovanějším	sledovaný	k2eAgInSc7d3	nejsledovanější
pořadem	pořad	k1gInSc7	pořad
kanálu	kanál	k1gInSc2	kanál
(	(	kIx(	(
<g/>
i	i	k9	i
včetně	včetně	k7c2	včetně
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Showtime	Showtim	k1gInSc5	Showtim
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1998	[number]	k4	1998
objednal	objednat	k5eAaPmAgMnS	objednat
třetí	třetí	k4xOgMnSc1	třetí
a	a	k8xC	a
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
,	,	kIx,	,
každou	každý	k3xTgFnSc4	každý
po	po	k7c6	po
22	[number]	k4	22
epizodách	epizoda	k1gFnPc6	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
výroba	výroba	k1gFnSc1	výroba
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
stala	stát	k5eAaPmAgFnS	stát
příliš	příliš	k6eAd1	příliš
drahou	drahý	k2eAgFnSc7d1	drahá
<g/>
,	,	kIx,	,
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
se	se	k3xPyFc4	se
MGM	MGM	kA	MGM
se	s	k7c7	s
Showtimem	Showtimo	k1gNnSc7	Showtimo
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vysílán	vysílat	k5eAaImNgInS	vysílat
v	v	k7c6	v
syndikaci	syndikace	k1gFnSc6	syndikace
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
stanicích	stanice	k1gFnPc6	stanice
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
na	na	k7c4	na
Showtimu	Showtima	k1gFnSc4	Showtima
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgNnPc2	všecek
22	[number]	k4	22
stanic	stanice	k1gFnPc2	stanice
ze	z	k7c2	z
sítě	síť	k1gFnSc2	síť
Fox	fox	k1gInSc1	fox
vysílalo	vysílat	k5eAaImAgNnS	vysílat
první	první	k4xOgFnPc4	první
řady	řada	k1gFnPc4	řada
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
debutu	debut	k1gInSc6	debut
na	na	k7c6	na
Showtimu	Showtim	k1gInSc6	Showtim
s	s	k7c7	s
podmínkou	podmínka	k1gFnSc7	podmínka
dosažení	dosažení	k1gNnSc4	dosažení
pokrytí	pokrytí	k1gNnSc1	pokrytí
41	[number]	k4	41
%	%	kIx~	%
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
nakoupil	nakoupit	k5eAaPmAgInS	nakoupit
svoji	svůj	k3xOyFgFnSc4	svůj
největší	veliký	k2eAgFnSc4d3	veliký
programovou	programový	k2eAgFnSc4d1	programová
akvizici	akvizice	k1gFnSc4	akvizice
Sci-Fi	scii	k1gFnSc2	sci-fi
Channel	Channel	k1gInSc1	Channel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
balík	balík	k1gInSc1	balík
exkluzivních	exkluzivní	k2eAgNnPc2d1	exkluzivní
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
kabelové	kabelový	k2eAgNnSc4d1	kabelové
vysílání	vysílání	k1gNnSc4	vysílání
za	za	k7c4	za
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
seriály	seriál	k1gInPc4	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Krajní	krajní	k2eAgFnPc4d1	krajní
meze	mez	k1gFnPc4	mez
a	a	k8xC	a
Poltergeist	Poltergeist	k1gInSc4	Poltergeist
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
páté	pátý	k4xOgFnSc2	pátý
řady	řada	k1gFnSc2	řada
se	se	k3xPyFc4	se
Showtime	Showtim	k1gInSc5	Showtim
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
MGM	MGM	kA	MGM
s	s	k7c7	s
prohlášením	prohlášení	k1gNnSc7	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
má	mít	k5eAaImIp3nS	mít
stále	stále	k6eAd1	stále
značnou	značný	k2eAgFnSc4d1	značná
sledovanost	sledovanost	k1gFnSc4	sledovanost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
syndikaci	syndikace	k1gFnSc3	syndikace
už	už	k6eAd1	už
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
pro	pro	k7c4	pro
kanál	kanál	k1gInSc4	kanál
získat	získat	k5eAaPmF	získat
nové	nový	k2eAgMnPc4d1	nový
předplatitele	předplatitel	k1gMnPc4	předplatitel
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sledovanost	sledovanost	k1gFnSc1	sledovanost
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
finančního	finanční	k2eAgNnSc2d1	finanční
hlediska	hledisko	k1gNnSc2	hledisko
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
podal	podat	k5eAaPmAgMnS	podat
Sci-Fi	scii	k1gFnSc4	sci-fi
Channel	Channel	k1gMnSc1	Channel
nabídku	nabídka	k1gFnSc4	nabídka
MGM	MGM	kA	MGM
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
zájem	zájem	k1gInSc1	zájem
pokračovat	pokračovat	k5eAaImF	pokračovat
šestou	šestý	k4xOgFnSc7	šestý
sezónou	sezóna	k1gFnSc7	sezóna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
o	o	k7c4	o
trochu	trochu	k6eAd1	trochu
nižší	nízký	k2eAgInSc4d2	nižší
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kanál	kanál	k1gInSc1	kanál
poté	poté	k6eAd1	poté
vysílal	vysílat	k5eAaImAgInS	vysílat
novou	nový	k2eAgFnSc4d1	nová
řadu	řada	k1gFnSc4	řada
každý	každý	k3xTgInSc4	každý
pátek	pátek	k1gInSc4	pátek
v	v	k7c4	v
21	[number]	k4	21
hodin	hodina	k1gFnPc2	hodina
mezi	mezi	k7c4	mezi
seriály	seriál	k1gInPc4	seriál
The	The	k1gFnSc2	The
Dead	Deada	k1gFnPc2	Deada
Zone	Zon	k1gFnSc2	Zon
a	a	k8xC	a
Farscape	Farscap	k1gMnSc5	Farscap
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
staré	starý	k2eAgFnPc1d1	stará
epizody	epizoda	k1gFnPc1	epizoda
běžely	běžet	k5eAaImAgFnP	běžet
ve	v	k7c6	v
čtyřhodinovém	čtyřhodinový	k2eAgInSc6d1	čtyřhodinový
bloku	blok	k1gInSc6	blok
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
od	od	k7c2	od
19	[number]	k4	19
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
díly	díl	k1gInPc4	díl
šesté	šestý	k4xOgFnSc2	šestý
řady	řada	k1gFnSc2	řada
byly	být	k5eAaImAgInP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
syndikaci	syndikace	k1gFnSc6	syndikace
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
premiéře	premiéra	k1gFnSc6	premiéra
na	na	k7c6	na
Sci-Fi	scii	k1gFnSc6	sci-fi
Channelu	Channel	k1gInSc2	Channel
<g/>
.	.	kIx.	.
</s>
<s>
Šestá	šestý	k4xOgFnSc1	šestý
a	a	k8xC	a
sedmá	sedmý	k4xOgFnSc1	sedmý
sezóna	sezóna	k1gFnSc1	sezóna
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
Sci-Fi	scii	k1gFnSc4	sci-fi
Channel	Channel	k1gMnSc1	Channel
nejsledovanějším	sledovaný	k2eAgInSc7d3	nejsledovanější
původním	původní	k2eAgInSc7d1	původní
seriálem	seriál	k1gInSc7	seriál
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
diváků	divák	k1gMnPc2	divák
v	v	k7c6	v
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyneslo	vynést	k5eAaPmAgNnS	vynést
kanál	kanál	k1gInSc4	kanál
do	do	k7c2	do
top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
kabelových	kabelový	k2eAgFnPc2d1	kabelová
stanic	stanice	k1gFnPc2	stanice
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
producenti	producent	k1gMnPc1	producent
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
probíhající	probíhající	k2eAgFnSc1d1	probíhající
sezóna	sezóna	k1gFnSc1	sezóna
seriálu	seriál	k1gInSc2	seriál
bude	být	k5eAaImBp3nS	být
tou	ten	k3xDgFnSc7	ten
poslední	poslední	k2eAgFnSc7d1	poslední
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
tedy	tedy	k9	tedy
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
velké	velký	k2eAgFnPc4d1	velká
finálové	finálový	k2eAgFnPc4d1	finálová
epizody	epizoda	k1gFnPc4	epizoda
<g/>
,	,	kIx,	,
úspěch	úspěch	k1gInSc4	úspěch
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
jejich	jejich	k3xOp3gInPc4	jejich
předpoklady	předpoklad	k1gInPc4	předpoklad
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
odložil	odložit	k5eAaPmAgInS	odložit
jejich	jejich	k3xOp3gInPc4	jejich
plány	plán	k1gInPc4	plán
na	na	k7c4	na
napsání	napsání	k1gNnSc4	napsání
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Sci-Fi	scii	k1gFnSc7	sci-fi
Channel	Channela	k1gFnPc2	Channela
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
od	od	k7c2	od
osmé	osmý	k4xOgFnSc2	osmý
řady	řada	k1gFnSc2	řada
délku	délka	k1gFnSc4	délka
sezóny	sezóna	k1gFnSc2	sezóna
z	z	k7c2	z
22	[number]	k4	22
na	na	k7c4	na
20	[number]	k4	20
epizod	epizoda	k1gFnPc2	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInPc1d1	původní
představy	představ	k1gInPc1	představ
byly	být	k5eAaImAgInP	být
nahradit	nahradit	k5eAaPmF	nahradit
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
spin-offovým	spinffův	k2eAgInSc7d1	spin-offův
seriálem	seriál	k1gInSc7	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
vysílání	vysílání	k1gNnSc1	vysílání
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2004	[number]	k4	2004
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
osmou	osmý	k4xOgFnSc7	osmý
řadou	řada	k1gFnSc7	řada
původního	původní	k2eAgInSc2d1	původní
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
rekord	rekord	k1gInSc1	rekord
ve	v	k7c6	v
sledovanosti	sledovanost	k1gFnSc6	sledovanost
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
milionu	milion	k4xCgInSc2	milion
diváků	divák	k1gMnPc2	divák
<g/>
)	)	kIx)	)
a	a	k8xC	a
rekord	rekord	k1gInSc4	rekord
ve	v	k7c6	v
sledovanosti	sledovanost	k1gFnSc6	sledovanost
epizody	epizoda	k1gFnSc2	epizoda
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
seriálu	seriál	k1gInSc2	seriál
na	na	k7c4	na
Sci-Fi	scii	k1gFnSc4	sci-fi
Channelu	Channel	k1gInSc2	Channel
(	(	kIx(	(
<g/>
Atlantida	Atlantida	k1gFnSc1	Atlantida
–	–	k?	–
4,2	[number]	k4	4,2
milionu	milion	k4xCgInSc2	milion
diváků	divák	k1gMnPc2	divák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kanále	kanál	k1gInSc6	kanál
se	se	k3xPyFc4	se
ke	k	k7c3	k
dvěma	dva	k4xCgFnPc3	dva
Hvězdným	hvězdný	k2eAgFnPc3d1	hvězdná
branám	brána	k1gFnPc3	brána
připojila	připojit	k5eAaPmAgFnS	připojit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
ještě	ještě	k9	ještě
Battlestar	Battlestar	k1gInSc1	Battlestar
Galactica	Galacticum	k1gNnSc2	Galacticum
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
byl	být	k5eAaImAgInS	být
Sci-Fi	scii	k1gNnSc7	sci-fi
Channel	Channlo	k1gNnPc2	Channlo
o	o	k7c6	o
pátečních	páteční	k2eAgInPc6d1	páteční
večerech	večer	k1gInPc6	večer
až	až	k9	až
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
2005	[number]	k4	2005
lídrem	lídr	k1gMnSc7	lídr
mezi	mezi	k7c7	mezi
základními	základní	k2eAgInPc7d1	základní
kabelovými	kabelový	k2eAgInPc7d1	kabelový
kanály	kanál	k1gInPc7	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
zvažovali	zvažovat	k5eAaImAgMnP	zvažovat
po	po	k7c6	po
osmé	osmý	k4xOgFnSc6	osmý
sezóně	sezóna	k1gFnSc6	sezóna
ukončit	ukončit	k5eAaPmF	ukončit
seriál	seriál	k1gInSc4	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
nahradit	nahradit	k5eAaPmF	nahradit
jej	on	k3xPp3gMnSc4	on
novým	nový	k2eAgInSc7d1	nový
s	s	k7c7	s
názvem	název	k1gInSc7	název
Stargate	Stargat	k1gInSc5	Stargat
Command	Command	k1gInSc1	Command
<g/>
,	,	kIx,	,
Sci-Fi	scii	k1gNnSc1	sci-fi
Channel	Channela	k1gFnPc2	Channela
ale	ale	k8xC	ale
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
deváté	devátý	k4xOgFnSc6	devátý
řadě	řada	k1gFnSc6	řada
původního	původní	k2eAgInSc2d1	původní
seriálu	seriál	k1gInSc2	seriál
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
upraveným	upravený	k2eAgNnSc7d1	upravené
obsazením	obsazení	k1gNnSc7	obsazení
<g/>
.	.	kIx.	.
</s>
<s>
Sledovanost	sledovanost	k1gFnSc1	sledovanost
deváté	devátý	k4xOgFnSc2	devátý
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
z	z	k7c2	z
2,4	[number]	k4	2,4
milionu	milion	k4xCgInSc2	milion
diváků	divák	k1gMnPc2	divák
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
na	na	k7c4	na
2,1	[number]	k4	2,1
milionu	milion	k4xCgInSc2	milion
diváků	divák	k1gMnPc2	divák
v	v	k7c6	v
1,8	[number]	k4	1,8
milionech	milion	k4xCgInPc6	milion
domácností	domácnost	k1gFnPc2	domácnost
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
sledovanosti	sledovanost	k1gFnSc2	sledovanost
v	v	k7c6	v
syndikaci	syndikace	k1gFnSc6	syndikace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
se	se	k3xPyFc4	se
shodoval	shodovat	k5eAaImAgMnS	shodovat
se	s	k7c7	s
snížením	snížení	k1gNnSc7	snížení
sledovanosti	sledovanost	k1gFnSc2	sledovanost
u	u	k7c2	u
všech	všecek	k3xTgNnPc2	všecek
syndikovaných	syndikovaný	k2eAgNnPc2d1	syndikovaný
sci-fi	scii	k1gNnPc2	sci-fi
akčních	akční	k2eAgInPc2d1	akční
seriálů	seriál	k1gInPc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Sci-Fi	scii	k1gFnPc2	sci-fi
Channel	Channel	k1gMnSc1	Channel
si	se	k3xPyFc3	se
sice	sice	k8xC	sice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
objednal	objednat	k5eAaPmAgMnS	objednat
rekordní	rekordní	k2eAgFnSc4d1	rekordní
desátou	desátý	k4xOgFnSc4	desátý
řadu	řada	k1gFnSc4	řada
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2006	[number]	k4	2006
ale	ale	k9	ale
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnPc1d1	další
sezóny	sezóna	k1gFnPc1	sezóna
již	již	k9	již
nebudou	být	k5eNaImBp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
epizoda	epizoda	k1gFnSc1	epizoda
seriálu	seriál	k1gInSc2	seriál
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
<g/>
"	"	kIx"	"
měla	mít	k5eAaImAgFnS	mít
světovou	světový	k2eAgFnSc4d1	světová
premiéru	premiéra	k1gFnSc4	premiéra
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
kanále	kanál	k1gInSc6	kanál
Sky	Sky	k1gFnPc2	Sky
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
při	při	k7c6	při
americké	americký	k2eAgFnSc6d1	americká
premiéře	premiéra	k1gFnSc6	premiéra
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
Sci-Fi	scii	k1gFnSc4	sci-fi
Channelu	Channel	k1gInSc2	Channel
sledovalo	sledovat	k5eAaImAgNnS	sledovat
přibližně	přibližně	k6eAd1	přibližně
2,2	[number]	k4	2,2
milionu	milion	k4xCgInSc2	milion
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Wrighta	Wright	k1gInSc2	Wright
a	a	k8xC	a
Coopera	Coopera	k1gFnSc1	Coopera
byla	být	k5eAaImAgFnS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
úspěchu	úspěch	k1gInSc2	úspěch
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
popularita	popularita	k1gFnSc1	popularita
sci-fi	scii	k1gNnPc2	sci-fi
a	a	k8xC	a
právě	právě	k6eAd1	právě
dobré	dobrý	k2eAgNnSc1d1	dobré
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
přijetí	přijetí	k1gNnSc1	přijetí
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
udržet	udržet	k5eAaPmF	udržet
seriál	seriál	k1gInSc4	seriál
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
novin	novina	k1gFnPc2	novina
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
100	[number]	k4	100
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
a	a	k8xC	a
týdenní	týdenní	k2eAgFnSc1d1	týdenní
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
sledovanost	sledovanost	k1gFnSc1	sledovanost
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
okolo	okolo	k7c2	okolo
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Times	Times	k1gMnSc1	Times
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
uvedly	uvést	k5eAaPmAgInP	uvést
jiná	jiný	k2eAgNnPc1d1	jiné
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
vysílán	vysílat	k5eAaImNgInS	vysílat
v	v	k7c6	v
64	[number]	k4	64
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
17	[number]	k4	17
miliony	milion	k4xCgInPc7	milion
diváky	divák	k1gMnPc7	divák
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
měl	mít	k5eAaImAgInS	mít
dobré	dobrý	k2eAgNnSc4d1	dobré
přijetí	přijetí	k1gNnSc4	přijetí
zejména	zejména	k9	zejména
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
běžela	běžet	k5eAaImAgFnS	běžet
na	na	k7c4	na
Sky	Sky	k1gFnSc4	Sky
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
reprízy	repríza	k1gFnPc1	repríza
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
na	na	k7c6	na
kanálech	kanál	k1gInPc6	kanál
Sky	Sky	k1gFnSc2	Sky
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Sky	Sky	k1gFnSc1	Sky
<g/>
3	[number]	k4	3
a	a	k8xC	a
Channel	Channel	k1gInSc1	Channel
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Sky	Sky	k?	Sky
<g/>
1	[number]	k4	1
vysílala	vysílat	k5eAaImAgFnS	vysílat
nové	nový	k2eAgInPc4d1	nový
epizody	epizoda	k1gFnSc2	epizoda
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
většiny	většina	k1gFnSc2	většina
řad	řada	k1gFnPc2	řada
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
americká	americký	k2eAgFnSc1d1	americká
premiéra	premiéra	k1gFnSc1	premiéra
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
posledního	poslední	k2eAgInSc2d1	poslední
dílu	díl	k1gInSc2	díl
seriálu	seriál	k1gInSc2	seriál
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
téměř	téměř	k6eAd1	téměř
trapné	trapný	k2eAgFnPc1d1	trapná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
populárnější	populární	k2eAgMnSc1d2	populárnější
než	než	k8xS	než
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
na	na	k7c6	na
stanicích	stanice	k1gFnPc6	stanice
Space	Space	k1gMnSc1	Space
<g/>
,	,	kIx,	,
Citytv	Citytv	k1gMnSc1	Citytv
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
,	,	kIx,	,
Movie	Movie	k1gFnSc1	Movie
Central	Central	k1gFnSc1	Central
a	a	k8xC	a
na	na	k7c6	na
francouzskojazyčných	francouzskojazyčný	k2eAgInPc6d1	francouzskojazyčný
kanálech	kanál	k1gInPc6	kanál
TQS	TQS	kA	TQS
a	a	k8xC	a
Ztélé	Ztél	k1gMnPc1	Ztél
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byla	být	k5eAaImAgFnS	být
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
premiérově	premiérově	k6eAd1	premiérově
vysílána	vysílat	k5eAaImNgFnS	vysílat
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
pilotní	pilotní	k2eAgFnSc2d1	pilotní
dvojepizody	dvojepizoda	k1gFnSc2	dvojepizoda
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
díly	díl	k1gInPc1	díl
následovaly	následovat	k5eAaImAgInP	následovat
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
První	první	k4xOgInSc4	první
přikázání	přikázání	k1gNnPc2	přikázání
<g/>
"	"	kIx"	"
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přesunula	přesunout	k5eAaPmAgFnS	přesunout
stanice	stanice	k1gFnSc1	stanice
seriál	seriál	k1gInSc4	seriál
do	do	k7c2	do
každodenního	každodenní	k2eAgNnSc2d1	každodenní
vysílání	vysílání	k1gNnSc2	vysílání
v	v	k7c4	v
pracovní	pracovní	k2eAgInPc4d1	pracovní
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
viděli	vidět	k5eAaImAgMnP	vidět
čeští	český	k2eAgMnPc1d1	český
diváci	divák	k1gMnPc1	divák
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
řady	řada	k1gFnPc4	řada
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc7	třetí
následovala	následovat	k5eAaImAgFnS	následovat
během	během	k7c2	během
září	září	k1gNnSc2	září
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
října	říjen	k1gInSc2	říjen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
sezóna	sezóna	k1gFnSc1	sezóna
se	se	k3xPyFc4	se
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
obrazovkách	obrazovka	k1gFnPc6	obrazovka
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
pátá	pátá	k1gFnSc1	pátá
a	a	k8xC	a
šestá	šestý	k4xOgFnSc1	šestý
série	série	k1gFnSc1	série
byla	být	k5eAaImAgFnS	být
vysílána	vysílat	k5eAaImNgFnS	vysílat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
května	květen	k1gInSc2	květen
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
sedmé	sedmý	k4xOgFnSc2	sedmý
až	až	k8xS	až
desáté	desátý	k4xOgFnSc2	desátý
řady	řada	k1gFnSc2	řada
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
souvislém	souvislý	k2eAgNnSc6d1	souvislé
každodenním	každodenní	k2eAgNnSc6d1	každodenní
vysílání	vysílání	k1gNnSc6	vysílání
během	během	k7c2	během
pracovních	pracovní	k2eAgInPc2d1	pracovní
dnů	den	k1gInPc2	den
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
díl	díl	k1gInSc4	díl
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
<g/>
"	"	kIx"	"
zhlédli	zhlédnout	k5eAaPmAgMnP	zhlédnout
diváci	divák	k1gMnPc1	divák
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
řady	řada	k1gFnPc1	řada
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
byly	být	k5eAaImAgInP	být
reprízovány	reprízovat	k5eAaImNgInP	reprízovat
na	na	k7c6	na
TV	TV	kA	TV
Prima	prima	k6eAd1	prima
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
seriál	seriál	k1gInSc4	seriál
potom	potom	k6eAd1	potom
jak	jak	k8xC	jak
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
sesterském	sesterský	k2eAgInSc6d1	sesterský
kanálu	kanál	k1gInSc6	kanál
Nova	nova	k1gFnSc1	nova
Cinema	Cinemum	k1gNnSc2	Cinemum
<g/>
,	,	kIx,	,
kanálu	kanál	k1gInSc2	kanál
Prima	prima	k2eAgInSc1d1	prima
Cool	Cool	k1gInSc1	Cool
a	a	k8xC	a
na	na	k7c6	na
kabelových	kabelový	k2eAgFnPc6d1	kabelová
stanicích	stanice	k1gFnPc6	stanice
AXN	AXN	kA	AXN
a	a	k8xC	a
AXN	AXN	kA	AXN
Sci-Fi	scii	k1gFnSc1	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
Dvoustá	Dvoustý	k2eAgFnSc1d1	Dvoustý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Sci-Fi	scii	k1gFnSc4	sci-fi
Channel	Channel	k1gMnSc1	Channel
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedenáctá	jedenáctý	k4xOgFnSc1	jedenáctý
sezóna	sezóna	k1gFnSc1	sezóna
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
nevznikne	vzniknout	k5eNaPmIp3nS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
publikované	publikovaný	k2eAgFnPc1d1	publikovaná
zprávy	zpráva	k1gFnPc1	zpráva
se	se	k3xPyFc4	se
zmiňovaly	zmiňovat	k5eAaImAgFnP	zmiňovat
o	o	k7c6	o
klesající	klesající	k2eAgFnSc6d1	klesající
sledovanosti	sledovanost	k1gFnSc6	sledovanost
<g/>
,	,	kIx,	,
drahé	drahý	k2eAgFnSc3d1	drahá
výrobě	výroba	k1gFnSc3	výroba
a	a	k8xC	a
nedostatku	nedostatek	k1gInSc3	nedostatek
propagace	propagace	k1gFnSc2	propagace
<g/>
,	,	kIx,	,
Mark	Mark	k1gMnSc1	Mark
Stern	sternum	k1gNnPc2	sternum
ze	z	k7c2	z
Sci-Fi	scii	k1gFnSc2	sci-fi
Channelu	Channel	k1gInSc2	Channel
pouze	pouze	k6eAd1	pouze
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
nebylo	být	k5eNaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
kvůli	kvůli	k7c3	kvůli
sledovanosti	sledovanost	k1gFnSc3	sledovanost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
produkčnímu	produkční	k2eAgInSc3d1	produkční
štábu	štáb	k1gInSc3	štáb
byl	být	k5eAaImAgInS	být
dán	dán	k2eAgInSc1d1	dán
dostatek	dostatek	k1gInSc1	dostatek
času	čas	k1gInSc2	čas
na	na	k7c6	na
vyřešeních	vyřešení	k1gNnPc6	vyřešení
všech	všecek	k3xTgFnPc2	všecek
příběhových	příběhový	k2eAgFnPc2d1	příběhová
linií	linie	k1gFnPc2	linie
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
začlenit	začlenit	k5eAaPmF	začlenit
herce	herc	k1gInPc4	herc
do	do	k7c2	do
pokračujícího	pokračující	k2eAgInSc2d1	pokračující
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
MGM	MGM	kA	MGM
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
držitel	držitel	k1gMnSc1	držitel
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
producenti	producent	k1gMnPc1	producent
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
seriál	seriál	k1gInSc1	seriál
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dále	daleko	k6eAd2	daleko
jako	jako	k9	jako
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
minisérie	minisérie	k1gFnSc1	minisérie
či	či	k8xC	či
jako	jako	k8xS	jako
jedenáctá	jedenáctý	k4xOgFnSc1	jedenáctý
řada	řada	k1gFnSc1	řada
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
kanálu	kanál	k1gInSc6	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
výrobu	výroba	k1gFnSc4	výroba
dvou	dva	k4xCgMnPc6	dva
DVD	DVD	kA	DVD
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
Amanda	Amanda	k1gFnSc1	Amanda
Tapping	Tapping	k1gInSc1	Tapping
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
natáčené	natáčený	k2eAgFnSc3d1	natáčená
čtvrté	čtvrtá	k1gFnSc3	čtvrtá
sezóně	sezóna	k1gFnSc3	sezóna
Atlantidy	Atlantida	k1gFnSc2	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
film	film	k1gInSc4	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Archa	archa	k1gFnSc1	archa
pravdy	pravda	k1gFnSc2	pravda
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
a	a	k8xC	a
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
příběh	příběh	k1gInSc1	příběh
s	s	k7c7	s
Orii	Orii	k1gNnSc7	Orii
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
film	film	k1gInSc4	film
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Návrat	návrat	k1gInSc1	návrat
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
o	o	k7c4	o
cestování	cestování	k1gNnSc4	cestování
v	v	k7c6	v
čase	čas	k1gInSc6	čas
do	do	k7c2	do
alternativní	alternativní	k2eAgFnSc2d1	alternativní
časové	časový	k2eAgFnSc2d1	časová
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
DVD	DVD	kA	DVD
speciální	speciální	k2eAgFnSc1d1	speciální
edice	edice	k1gFnSc1	edice
pilotní	pilotní	k2eAgFnSc2d1	pilotní
dvouhodinové	dvouhodinový	k2eAgFnSc2d1	dvouhodinová
epizody	epizoda	k1gFnSc2	epizoda
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
s	s	k7c7	s
nově	nově	k6eAd1	nově
sestříhanými	sestříhaný	k2eAgFnPc7d1	sestříhaná
scénami	scéna	k1gFnPc7	scéna
a	a	k8xC	a
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2009	[number]	k4	2009
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
společnost	společnost	k1gFnSc1	společnost
MGM	MGM	kA	MGM
třetí	třetí	k4xOgInSc4	třetí
film	film	k1gInSc4	film
k	k	k7c3	k
Hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
bráně	brána	k1gFnSc3	brána
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
Brad	brada	k1gFnPc2	brada
Wright	Wrighta	k1gFnPc2	Wrighta
poprvé	poprvé	k6eAd1	poprvé
mluvil	mluvit	k5eAaImAgMnS	mluvit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
Mallozzi	Mallozze	k1gFnSc4	Mallozze
prozradil	prozradit	k5eAaPmAgMnS	prozradit
pracovní	pracovní	k2eAgInSc4d1	pracovní
název	název	k1gInSc4	název
snímku	snímek	k1gInSc2	snímek
<g/>
,	,	kIx,	,
Stargate	Stargat	k1gMnSc5	Stargat
<g/>
:	:	kIx,	:
Revolution	Revolution	k1gInSc4	Revolution
<g/>
,	,	kIx,	,
napsat	napsat	k5eAaBmF	napsat
jej	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
Wright	Wright	k1gInSc1	Wright
společně	společně	k6eAd1	společně
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
výkonným	výkonný	k2eAgMnSc7d1	výkonný
producentem	producent	k1gMnSc7	producent
Atlantidy	Atlantida	k1gFnSc2	Atlantida
Carlem	Carl	k1gMnSc7	Carl
Binderem	Binder	k1gMnSc7	Binder
<g/>
,	,	kIx,	,
režisérem	režisér	k1gMnSc7	režisér
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
Martin	Martin	k1gMnSc1	Martin
Wood	Wood	k1gMnSc1	Wood
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
film	film	k1gInSc1	film
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
o	o	k7c4	o
"	"	kIx"	"
<g/>
možnosti	možnost	k1gFnPc4	možnost
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
programu	program	k1gInSc2	program
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Wrighta	Wright	k1gInSc2	Wright
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
postavu	postava	k1gFnSc4	postava
Jacka	Jacko	k1gNnSc2	Jacko
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Neilla	Neilla	k1gMnSc1	Neilla
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
podle	podle	k7c2	podle
možností	možnost	k1gFnPc2	možnost
opět	opět	k6eAd1	opět
spojit	spojit	k5eAaPmF	spojit
herce	herec	k1gMnPc4	herec
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
záleželo	záležet	k5eAaImAgNnS	záležet
na	na	k7c6	na
ceně	cena	k1gFnSc6	cena
filmu	film	k1gInSc2	film
a	a	k8xC	a
dostupnosti	dostupnost	k1gFnSc2	dostupnost
herců	herec	k1gMnPc2	herec
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Valy	Vala	k1gMnSc2	Vala
Mal	málit	k5eAaImRp2nS	málit
Doran	Dorany	k1gInPc2	Dorany
se	se	k3xPyFc4	se
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
objevit	objevit	k5eAaPmF	objevit
neměla	mít	k5eNaImAgFnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Amanda	Amanda	k1gFnSc1	Amanda
Tapping	Tapping	k1gInSc1	Tapping
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
svoji	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
v	v	k7c6	v
září	září	k1gNnSc6	září
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
se	s	k7c7	s
zapojením	zapojení	k1gNnSc7	zapojení
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
chystaného	chystaný	k2eAgInSc2d1	chystaný
filmu	film	k1gInSc2	film
Atlantidy	Atlantida	k1gFnSc2	Atlantida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
Michael	Michael	k1gMnSc1	Michael
Shanks	Shanks	k1gInSc4	Shanks
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
společně	společně	k6eAd1	společně
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Deanem	Dean	k1gMnSc7	Dean
Andersonem	Anderson	k1gMnSc7	Anderson
budou	být	k5eAaImBp3nP	být
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
také	také	k9	také
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
nebyly	být	k5eNaImAgFnP	být
podepsány	podepsat	k5eAaPmNgFnP	podepsat
žádné	žádný	k3yNgFnPc1	žádný
smlouvy	smlouva	k1gFnPc1	smlouva
<g/>
,	,	kIx,	,
Wright	Wright	k1gMnSc1	Wright
ale	ale	k8xC	ale
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
může	moct	k5eAaImIp3nS	moct
téměř	téměř	k6eAd1	téměř
zaručit	zaručit	k5eAaPmF	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
letos	letos	k6eAd1	letos
[	[	kIx(	[
<g/>
2009	[number]	k4	2009
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
k	k	k7c3	k
filmu	film	k1gInSc3	film
dostaneme	dostat	k5eAaPmIp1nP	dostat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Wrighta	Wrighto	k1gNnSc2	Wrighto
byly	být	k5eAaImAgFnP	být
kvůli	kvůli	k7c3	kvůli
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
krizi	krize	k1gFnSc3	krize
DVD	DVD	kA	DVD
premiéry	premiéra	k1gFnSc2	premiéra
pro	pro	k7c4	pro
MGM	MGM	kA	MGM
méně	málo	k6eAd2	málo
výnosné	výnosný	k2eAgNnSc4d1	výnosné
<g/>
,	,	kIx,	,
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
společně	společně	k6eAd1	společně
s	s	k7c7	s
finančními	finanční	k2eAgInPc7d1	finanční
problémy	problém	k1gInPc7	problém
MGM	MGM	kA	MGM
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
zrušení	zrušení	k1gNnSc4	zrušení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
Wright	Wright	k1gMnSc1	Wright
a	a	k8xC	a
Mallozzi	Mallozh	k1gMnPc1	Mallozh
optimisticky	optimisticky	k6eAd1	optimisticky
uváděli	uvádět	k5eAaImAgMnP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
případná	případný	k2eAgFnSc1d1	případná
produkce	produkce	k1gFnSc1	produkce
mohla	moct	k5eAaImAgFnS	moct
začít	začít	k5eAaPmF	začít
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
Wright	Wright	k1gMnSc1	Wright
neoznámil	oznámit	k5eNaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
projekt	projekt	k1gInSc1	projekt
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
definitivně	definitivně	k6eAd1	definitivně
odložen	odložit	k5eAaPmNgInS	odložit
společně	společně	k6eAd1	společně
s	s	k7c7	s
plány	plán	k1gInPc7	plán
na	na	k7c4	na
filmy	film	k1gInPc4	film
k	k	k7c3	k
seriálům	seriál	k1gInPc3	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
a	a	k8xC	a
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
a	a	k8xC	a
cross-overovému	crossverový	k2eAgInSc3d1	cross-overový
snímku	snímek	k1gInSc3	snímek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
prvky	prvek	k1gInPc1	prvek
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
seriálů	seriál	k1gInPc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
už	už	k6eAd1	už
nebyly	být	k5eNaImAgInP	být
natáčeny	natáčet	k5eAaImNgInP	natáčet
ani	ani	k9	ani
seriály	seriál	k1gInPc4	seriál
Atlantida	Atlantida	k1gFnSc1	Atlantida
a	a	k8xC	a
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
nicméně	nicméně	k8xC	nicméně
nevyloučil	vyloučit	k5eNaPmAgMnS	vyloučit
budoucnost	budoucnost	k1gFnSc4	budoucnost
filmů	film	k1gInPc2	film
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
franšíza	franšíza	k1gFnSc1	franšíza
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
nekončí	končit	k5eNaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
<s>
Někdo	někdo	k3yInSc1	někdo
chytrý	chytrý	k2eAgMnSc1d1	chytrý
z	z	k7c2	z
MGM	MGM	kA	MGM
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
přijde	přijít	k5eAaPmIp3nS	přijít
a	a	k8xC	a
něco	něco	k3yInSc1	něco
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Seriál	seriál	k1gInSc1	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
DVD	DVD	kA	DVD
poprvé	poprvé	k6eAd1	poprvé
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
na	na	k7c6	na
několika	několik	k4yIc6	několik
discích	disk	k1gInPc6	disk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
typicky	typicky	k6eAd1	typicky
obsahoval	obsahovat	k5eAaImAgMnS	obsahovat
čtyři	čtyři	k4xCgFnPc4	čtyři
epizody	epizoda	k1gFnPc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
disk	disk	k1gInSc1	disk
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Season	Season	k1gMnSc1	Season
1	[number]	k4	1
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
To	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
první	první	k4xOgFnSc2	první
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
)	)	kIx)	)
vydán	vydán	k2eAgInSc1d1	vydán
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
další	další	k2eAgFnSc1d1	další
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
prvními	první	k4xOgInPc7	první
čtyřmi	čtyři	k4xCgInPc7	čtyři
díly	díl	k1gInPc7	díl
druhé	druhý	k4xOgFnSc2	druhý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
vyšla	vyjít	k5eAaPmAgFnS	vyjít
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
šesti	šest	k4xCc6	šest
samostatných	samostatný	k2eAgInPc6d1	samostatný
discích	disk	k1gInPc6	disk
(	(	kIx(	(
<g/>
desátá	desátý	k4xOgFnSc1	desátý
řada	řada	k1gFnSc1	řada
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
pěti	pět	k4xCc6	pět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
až	až	k6eAd1	až
osmá	osmý	k4xOgFnSc1	osmý
sezóna	sezóna	k1gFnSc1	sezóna
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
DVD	DVD	kA	DVD
v	v	k7c6	v
pětidiskových	pětidiskový	k2eAgInPc6d1	pětidiskový
plastových	plastový	k2eAgInPc6d1	plastový
obalech	obal	k1gInPc6	obal
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
pobočka	pobočka	k1gFnSc1	pobočka
MGM	MGM	kA	MGM
Home	Home	k1gFnSc1	Home
Entertainment	Entertainment	k1gInSc1	Entertainment
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vydávat	vydávat	k5eAaPmF	vydávat
box	box	k1gInSc4	box
sety	set	k1gInPc4	set
kompletních	kompletní	k2eAgFnPc2d1	kompletní
sezón	sezóna	k1gFnPc2	sezóna
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
první	první	k4xOgFnSc2	první
<g/>
)	)	kIx)	)
souběžně	souběžně	k6eAd1	souběžně
se	s	k7c7	s
samostatnými	samostatný	k2eAgNnPc7d1	samostatné
vydáními	vydání	k1gNnPc7	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Britské	britský	k2eAgFnPc1d1	britská
box	box	k1gInSc4	box
sety	set	k1gInPc4	set
se	se	k3xPyFc4	se
řadami	řada	k1gFnPc7	řada
byly	být	k5eAaImAgInP	být
obvykle	obvykle	k6eAd1	obvykle
vydány	vydat	k5eAaPmNgInP	vydat
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
po	po	k7c6	po
britské	britský	k2eAgFnSc6d1	britská
premiéře	premiéra	k1gFnSc6	premiéra
poslední	poslední	k2eAgFnSc6d1	poslední
epizodě	epizoda	k1gFnSc6	epizoda
té	ten	k3xDgFnSc2	ten
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
DVD	DVD	kA	DVD
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bonusy	bonus	k1gInPc4	bonus
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
záběrů	záběr	k1gInPc2	záběr
ze	z	k7c2	z
zákulisí	zákulisí	k1gNnSc2	zákulisí
<g/>
,	,	kIx,	,
audio	audio	k2eAgInPc2d1	audio
komentářů	komentář	k1gInPc2	komentář
pro	pro	k7c4	pro
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
epizody	epizoda	k1gFnPc4	epizoda
počínaje	počínaje	k7c7	počínaje
čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
sezónou	sezóna	k1gFnSc7	sezóna
a	a	k8xC	a
fotogalerií	fotogalerie	k1gFnSc7	fotogalerie
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
<g/>
.	.	kIx.	.
</s>
<s>
Box	box	k1gInSc1	box
sety	set	k1gInPc4	set
prvních	první	k4xOgNnPc6	první
osmi	osm	k4xCc2	osm
řad	řada	k1gFnPc2	řada
byly	být	k5eAaImAgFnP	být
opětovně	opětovně	k6eAd1	opětovně
vydány	vydat	k5eAaPmNgFnP	vydat
ve	v	k7c6	v
slim	slim	k6eAd1	slim
obalech	obal	k1gInPc6	obal
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
regionech	region	k1gInPc6	region
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
takový	takový	k3xDgMnSc1	takový
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2006	[number]	k4	2006
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vydán	vydat	k5eAaPmNgInS	vydat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
balení	balení	k1gNnSc1	balení
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
50	[number]	k4	50
disků	disk	k1gInPc2	disk
s	s	k7c7	s
deseti	deset	k4xCc7	deset
sezónami	sezóna	k1gFnPc7	sezóna
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
disky	disk	k1gInPc4	disk
s	s	k7c7	s
bonusy	bonus	k1gInPc7	bonus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
celkově	celkově	k6eAd1	celkově
prodáno	prodat	k5eAaPmNgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
epizody	epizoda	k1gFnPc1	epizoda
Hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
brány	brána	k1gFnPc1	brána
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
vydány	vydat	k5eAaPmNgFnP	vydat
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
iTunes	iTunes	k1gInSc1	iTunes
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
premiéře	premiéra	k1gFnSc6	premiéra
na	na	k7c6	na
Sci-Fi	scii	k1gFnSc6	sci-fi
Channelu	Channel	k1gInSc2	Channel
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
díl	díl	k1gInSc1	díl
bez	bez	k7c2	bez
reklam	reklama	k1gFnPc2	reklama
stály	stát	k5eAaImAgInP	stát
1,99	[number]	k4	1,99
amerického	americký	k2eAgInSc2d1	americký
dolaru	dolar	k1gInSc2	dolar
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
sezónu	sezóna	k1gFnSc4	sezóna
(	(	kIx(	(
<g/>
20	[number]	k4	20
epizod	epizoda	k1gFnPc2	epizoda
<g/>
)	)	kIx)	)
činila	činit	k5eAaImAgFnS	činit
37,99	[number]	k4	37,99
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
iTunes	iTunes	k1gInSc4	iTunes
následovalo	následovat	k5eAaImAgNnS	následovat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
všech	všecek	k3xTgFnPc2	všecek
deset	deset	k4xCc4	deset
řad	řada	k1gFnPc2	řada
dostupných	dostupný	k2eAgFnPc2d1	dostupná
na	na	k7c4	na
iTunes	iTunes	k1gInSc4	iTunes
a	a	k8xC	a
na	na	k7c6	na
Amazon	amazona	k1gFnPc2	amazona
Unbox	Unbox	k1gInSc1	Unbox
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Hulu	Hulus	k1gInSc6	Hulus
se	se	k3xPyFc4	se
první	první	k4xOgFnSc1	první
sezóna	sezóna	k1gFnSc1	sezóna
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
diváci	divák	k1gMnPc1	divák
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
mohli	moct	k5eAaImAgMnP	moct
sledovat	sledovat	k5eAaImF	sledovat
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnPc1	první
řady	řada	k1gFnPc1	řada
<g/>
,	,	kIx,	,
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgInPc1	všechen
díly	díl	k1gInPc1	díl
všech	všecek	k3xTgMnPc2	všecek
deseti	deset	k4xCc2	deset
sezón	sezóna	k1gFnPc2	sezóna
dostupné	dostupný	k2eAgInPc1d1	dostupný
bezplatně	bezplatně	k6eAd1	bezplatně
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
reklam	reklama	k1gFnPc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgInS	být
zajištěn	zajistit	k5eAaPmNgInS	zajistit
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
epizody	epizoda	k1gFnPc1	epizoda
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
USA	USA	kA	USA
všechny	všechen	k3xTgFnPc4	všechen
epizody	epizoda	k1gFnPc4	epizoda
celé	celý	k2eAgFnSc2d1	celá
franšízy	franšíza	k1gFnSc2	franšíza
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
dostupné	dostupný	k2eAgFnPc1d1	dostupná
na	na	k7c4	na
předplatitelské	předplatitelský	k2eAgNnSc4d1	předplatitelské
video	video	k1gNnSc4	video
streamovací	streamovací	k2eAgFnSc6d1	streamovací
službě	služba	k1gFnSc3	služba
Netflixu	Netflix	k1gInSc2	Netflix
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byla	být	k5eAaImAgFnS	být
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
zdarma	zdarma	k6eAd1	zdarma
dostupná	dostupný	k2eAgFnSc1d1	dostupná
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
PrimaPLAY	PrimaPLAY	k1gFnSc2	PrimaPLAY
televize	televize	k1gFnSc1	televize
Prima	prima	k1gFnSc1	prima
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
nacházelo	nacházet	k5eAaImAgNnS	nacházet
posledních	poslední	k2eAgFnPc2d1	poslední
pět	pět	k4xCc1	pět
epizod	epizoda	k1gFnPc2	epizoda
vysílaných	vysílaný	k2eAgFnPc2d1	vysílaná
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
<g/>
.	.	kIx.	.
</s>
<s>
Will	Will	k1gMnSc1	Will
Joyener	Joyener	k1gMnSc1	Joyener
z	z	k7c2	z
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
považoval	považovat	k5eAaImAgInS	považovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
recenzi	recenze	k1gFnSc6	recenze
na	na	k7c4	na
pilotní	pilotní	k2eAgFnSc4d1	pilotní
epizodu	epizoda	k1gFnSc4	epizoda
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
bohů	bůh	k1gMnPc2	bůh
<g/>
"	"	kIx"	"
seriál	seriál	k1gInSc1	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
za	za	k7c4	za
"	"	kIx"	"
<g/>
náročný	náročný	k2eAgInSc4d1	náročný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vůbec	vůbec	k9	vůbec
odvozený	odvozený	k2eAgInSc1d1	odvozený
mix	mix	k1gInSc1	mix
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
fanoušek	fanoušek	k1gMnSc1	fanoušek
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
[	[	kIx(	[
<g/>
filmu	film	k1gInSc2	film
<g/>
]	]	kIx)	]
mohl	moct	k5eAaImAgMnS	moct
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
méně	málo	k6eAd2	málo
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c4	v
co	co	k3yQnSc4	co
mohl	moct	k5eAaImAgInS	moct
člověk	člověk	k1gMnSc1	člověk
doufat	doufat	k5eAaImF	doufat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c4	na
obsazení	obsazení	k1gNnSc4	obsazení
herců	herec	k1gMnPc2	herec
měl	mít	k5eAaImAgMnS	mít
smíšené	smíšený	k2eAgInPc4d1	smíšený
názory	názor	k1gInPc4	názor
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
rozčílen	rozčílit	k5eAaPmNgMnS	rozčílit
použitím	použití	k1gNnSc7	použití
taktiky	taktika	k1gFnSc2	taktika
vizuálního	vizuální	k2eAgInSc2d1	vizuální
šoku	šok	k1gInSc2	šok
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
kvůli	kvůli	k7c3	kvůli
nízkému	nízký	k2eAgInSc3d1	nízký
televiznímu	televizní	k2eAgInSc3d1	televizní
rozpočtu	rozpočet	k1gInSc3	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zhlédnutí	zhlédnutí	k1gNnSc6	zhlédnutí
téže	tenže	k3xDgFnSc2	tenže
epizody	epizoda	k1gFnSc2	epizoda
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Tony	Tony	k1gMnSc1	Tony
Scott	Scott	k1gMnSc1	Scott
z	z	k7c2	z
magazínu	magazín	k1gInSc2	magazín
Variety	varieta	k1gFnSc2	varieta
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
bloudí	bloudit	k5eAaImIp3nP	bloudit
povrchní	povrchní	k2eAgFnPc1d1	povrchní
postavy	postava	k1gFnPc1	postava
svými	svůj	k3xOyFgFnPc7	svůj
rolemi	role	k1gFnPc7	role
bez	bez	k7c2	bez
špetky	špetka	k1gFnSc2	špetka
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dělal	dělat	k5eAaImAgMnS	dělat
si	se	k3xPyFc3	se
legraci	legrace	k1gFnSc4	legrace
z	z	k7c2	z
prkenných	prkenný	k2eAgInPc2d1	prkenný
herců	herc	k1gInPc2	herc
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nezáživný	záživný	k2eNgInSc1d1	nezáživný
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
zápletky	zápletka	k1gFnPc1	zápletka
z	z	k7c2	z
pulp	pulpa	k1gFnPc2	pulpa
magazínů	magazín	k1gInPc2	magazín
<g/>
,	,	kIx,	,
postavy	postava	k1gFnPc4	postava
oblečené	oblečený	k2eAgFnPc4d1	oblečená
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
,	,	kIx,	,
banální	banální	k2eAgInPc1d1	banální
dialogy	dialog	k1gInPc1	dialog
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
a	a	k8xC	a
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
režie	režie	k1gFnSc1	režie
Maria	Mario	k1gMnSc2	Mario
Azzopardiho	Azzopardi	k1gMnSc2	Azzopardi
nepochybně	pochybně	k6eNd1	pochybně
potěší	potěšit	k5eAaPmIp3nS	potěšit
miliardy	miliarda	k4xCgFnPc4	miliarda
a	a	k8xC	a
miliardy	miliarda	k4xCgFnPc4	miliarda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mnoho	mnoho	k4c1	mnoho
kritiků	kritik	k1gMnPc2	kritik
v	v	k7c6	v
pilotní	pilotní	k2eAgFnSc6d1	pilotní
epizodě	epizoda	k1gFnSc6	epizoda
negativně	negativně	k6eAd1	negativně
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
na	na	k7c4	na
bezdůvodné	bezdůvodný	k2eAgNnSc4d1	bezdůvodné
použití	použití	k1gNnSc4	použití
sexuální	sexuální	k2eAgFnSc2d1	sexuální
narážky	narážka	k1gFnSc2	narážka
a	a	k8xC	a
ženské	ženský	k2eAgFnSc2d1	ženská
nahoty	nahota	k1gFnSc2	nahota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
Wrighta	Wrighto	k1gNnSc2	Wrighto
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
kanálu	kanál	k1gInSc2	kanál
Showtime	Showtim	k1gInSc5	Showtim
navzdory	navzdory	k7c3	navzdory
jeho	jeho	k3xOp3gInPc3	jeho
protestům	protest	k1gInPc3	protest
a	a	k8xC	a
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vystřižena	vystřihnout	k5eAaPmNgFnS	vystřihnout
z	z	k7c2	z
DVD	DVD	kA	DVD
verze	verze	k1gFnPc4	verze
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Sharon	Sharon	k1gMnSc1	Sharon
Eberson	Eberson	k1gNnSc4	Eberson
z	z	k7c2	z
novin	novina	k1gFnPc2	novina
Pittsburgh	Pittsburgha	k1gFnPc2	Pittsburgha
Post-Gazette	Post-Gazett	k1gInSc5	Post-Gazett
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
místo	místo	k1gNnSc4	místo
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sci-fi	scii	k1gFnSc2	sci-fi
poměřováno	poměřovat	k5eAaImNgNnS	poměřovat
dlouhověkostí	dlouhověkost	k1gFnSc7	dlouhověkost
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
přesnou	přesný	k2eAgFnSc7d1	přesná
chemií	chemie	k1gFnSc7	chemie
mezi	mezi	k7c7	mezi
herci	herec	k1gMnPc7	herec
<g/>
,	,	kIx,	,
fanatickými	fanatický	k2eAgMnPc7d1	fanatický
fanoušky	fanoušek	k1gMnPc7	fanoušek
a	a	k8xC	a
silnými	silný	k2eAgNnPc7d1	silné
tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
také	také	k9	také
ale	ale	k9	ale
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
zřídka	zřídka	k6eAd1	zřídka
miláčkem	miláček	k1gMnSc7	miláček
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc4	seriál
nejsledovanějším	sledovaný	k2eAgInSc7d3	nejsledovanější
programem	program	k1gInSc7	program
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
Showtime	Showtim	k1gInSc5	Showtim
<g/>
,	,	kIx,	,
média	médium	k1gNnPc1	médium
mimo	mimo	k7c4	mimo
sci-fi	scii	k1gNnSc4	sci-fi
kruhy	kruh	k1gInPc1	kruh
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
řadách	řada	k1gFnPc6	řada
prakticky	prakticky	k6eAd1	prakticky
nevěnovala	věnovat	k5eNaImAgFnS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
získala	získat	k5eAaPmAgFnS	získat
pozornost	pozornost	k1gFnSc4	pozornost
médií	médium	k1gNnPc2	médium
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
dostala	dostat	k5eAaPmAgFnS	dostat
zelenou	zelená	k1gFnSc7	zelená
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
(	(	kIx(	(
<g/>
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
přebal	přebal	k1gInSc1	přebal
časopisu	časopis	k1gInSc2	časopis
TV	TV	kA	TV
Guide	Guid	k1gInSc5	Guid
z	z	k7c2	z
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
uváděl	uvádět	k5eAaImAgInS	uvádět
titulek	titulek	k1gInSc4	titulek
"	"	kIx"	"
<g/>
Zapomeňte	zapomenout	k5eAaPmRp2nP	zapomenout
na	na	k7c4	na
Trek	Trek	k1gInSc4	Trek
<g/>
!	!	kIx.	!
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
největším	veliký	k2eAgMnSc7d3	veliký
sci-fi	scii	k1gFnSc7	sci-fi
hitem	hit	k1gInSc7	hit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
deník	deník	k1gInSc1	deník
Toronto	Toronto	k1gNnSc1	Toronto
Star	star	k1gFnPc2	star
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
se	se	k3xPyFc4	se
nezdálo	zdát	k5eNaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
někomu	někdo	k3yInSc3	někdo
kromě	kromě	k7c2	kromě
fanoušků	fanoušek	k1gMnPc2	fanoušek
[	[	kIx(	[
<g/>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
]	]	kIx)	]
líbila	líbit	k5eAaImAgFnS	líbit
<g/>
;	;	kIx,	;
většině	většina	k1gFnSc6	většina
televizních	televizní	k2eAgMnPc2d1	televizní
kritiků	kritik	k1gMnPc2	kritik
vždy	vždy	k6eAd1	vždy
nějak	nějak	k6eAd1	nějak
proklouzla	proklouznout	k5eAaPmAgFnS	proklouznout
pod	pod	k7c7	pod
prsty	prst	k1gInPc7	prst
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Melanie	Melanie	k1gFnSc2	Melanie
McFarland	McFarlanda	k1gFnPc2	McFarlanda
z	z	k7c2	z
deníku	deník	k1gInSc2	deník
Seattle	Seattle	k1gFnSc2	Seattle
Post-Intelligencer	Post-Intelligencra	k1gFnPc2	Post-Intelligencra
si	se	k3xPyFc3	se
dílo	dílo	k1gNnSc1	dílo
nezískalo	získat	k5eNaPmAgNnS	získat
"	"	kIx"	"
<g/>
ten	ten	k3xDgInSc1	ten
typ	typ	k1gInSc1	typ
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
respektu	respekt	k1gInSc2	respekt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
seriál	seriál	k1gInSc1	seriál
s	s	k7c7	s
200	[number]	k4	200
epizodami	epizoda	k1gFnPc7	epizoda
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
pouze	pouze	k6eAd1	pouze
výjimečně	výjimečně	k6eAd1	výjimečně
zaujala	zaujmout	k5eAaPmAgNnP	zaujmout
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
seznamech	seznam	k1gInPc6	seznam
"	"	kIx"	"
<g/>
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
seriálů	seriál	k1gInPc2	seriál
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zůstala	zůstat	k5eAaPmAgFnS	zůstat
"	"	kIx"	"
<g/>
odsunuta	odsunut	k2eAgFnSc1d1	odsunuta
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
popularitu	popularita	k1gFnSc4	popularita
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
za	za	k7c7	za
slávou	sláva	k1gFnSc7	sláva
seriálu	seriál	k1gInSc2	seriál
Battlestar	Battlestar	k1gMnSc1	Battlestar
Galactica	Galactica	k1gMnSc1	Galactica
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
každý	každý	k3xTgInSc4	každý
týden	týden	k1gInSc4	týden
útočila	útočit	k5eAaImAgFnS	útočit
její	její	k3xOp3gFnSc1	její
sledovanost	sledovanost	k1gFnSc1	sledovanost
na	na	k7c4	na
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
diváků	divák	k1gMnPc2	divák
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Producent	producent	k1gMnSc1	producent
a	a	k8xC	a
showrunner	showrunner	k1gMnSc1	showrunner
Robert	Robert	k1gMnSc1	Robert
C.	C.	kA	C.
Cooper	Cooper	k1gMnSc1	Cooper
také	také	k6eAd1	také
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
mimochodem	mimochodem	k6eAd1	mimochodem
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
netočí	točit	k5eNaImIp3nS	točit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
původní	původní	k2eAgInSc4d1	původní
Star	star	k1gInSc4	star
Trek	Treka	k1gFnPc2	Treka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
30	[number]	k4	30
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
ohlížet	ohlížet	k5eAaImF	ohlížet
a	a	k8xC	a
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
si	se	k3xPyFc3	se
ten	ten	k3xDgInSc4	ten
milník	milník	k1gInSc4	milník
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
my	my	k3xPp1nPc1	my
všichni	všechen	k3xTgMnPc1	všechen
skrytě	skrytě	k6eAd1	skrytě
doufáme	doufat	k5eAaImIp1nP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
10	[number]	k4	10
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
,	,	kIx,	,
20	[number]	k4	20
let	léto	k1gNnPc2	léto
bude	být	k5eAaImBp3nS	být
na	na	k7c4	na
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
pohlíženo	pohlížen	k2eAgNnSc4d1	pohlíženo
stejně	stejně	k6eAd1	stejně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k1gMnSc1	Scott
D.	D.	kA	D.
Pierce	Pierce	k1gMnSc1	Pierce
z	z	k7c2	z
deníku	deník	k1gInSc2	deník
Deseret	Deseret	k1gMnSc1	Deseret
News	Newsa	k1gFnPc2	Newsa
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
nikdy	nikdy	k6eAd1	nikdy
neměl	mít	k5eNaImAgInS	mít
"	"	kIx"	"
<g/>
takový	takový	k3xDgInSc1	takový
typ	typ	k1gInSc1	typ
kulturního	kulturní	k2eAgInSc2d1	kulturní
dopadu	dopad	k1gInSc2	dopad
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
hezkou	hezký	k2eAgFnSc4d1	hezká
odvozeninou	odvozenina	k1gFnSc7	odvozenina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
stával	stávat	k5eAaImAgInS	stávat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Robert	Robert	k1gMnSc1	Robert
Hanks	Hanksa	k1gFnPc2	Hanksa
z	z	k7c2	z
britských	britský	k2eAgFnPc2d1	britská
novin	novina	k1gFnPc2	novina
The	The	k1gMnSc1	The
Independent	independent	k1gMnSc1	independent
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
seriál	seriál	k1gInSc1	seriál
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
řady	řada	k1gFnSc2	řada
"	"	kIx"	"
<g/>
udělal	udělat	k5eAaPmAgInS	udělat
kus	kus	k1gInSc1	kus
práce	práce	k1gFnSc2	práce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
desetiletého	desetiletý	k2eAgNnSc2d1	desetileté
vysílání	vysílání	k1gNnSc2	vysílání
nominována	nominován	k2eAgFnSc1d1	nominována
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
ceny	cena	k1gFnPc4	cena
Emmy	Emma	k1gFnSc2	Emma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgInPc4d3	nejlepší
vizuální	vizuální	k2eAgInPc4d1	vizuální
efekty	efekt	k1gInPc4	efekt
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
a	a	k8xC	a
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
Emmy	Emma	k1gFnSc2	Emma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
hudební	hudební	k2eAgFnSc2d1	hudební
kompozice	kompozice	k1gFnSc2	kompozice
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
(	(	kIx(	(
<g/>
drama	drama	k1gNnSc1	drama
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
nebyla	být	k5eNaImAgFnS	být
proměněna	proměnit	k5eAaPmNgFnS	proměnit
ve	v	k7c6	v
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc4	seriál
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc2	třicet
nominací	nominace	k1gFnPc2	nominace
získal	získat	k5eAaPmAgMnS	získat
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
Gemini	Gemin	k2eAgMnPc1d1	Gemin
<g/>
,	,	kIx,	,
dvanáct	dvanáct	k4xCc4	dvanáct
cen	cena	k1gFnPc2	cena
Leo	Leo	k1gMnSc1	Leo
a	a	k8xC	a
pět	pět	k4xCc4	pět
cen	cena	k1gFnPc2	cena
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
VES	ves	k1gFnSc4	ves
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
Hugo	Hugo	k1gMnSc1	Hugo
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
použil	použít	k5eAaPmAgMnS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
fanoušků	fanoušek	k1gMnPc2	fanoušek
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
pojem	pojem	k1gInSc1	pojem
"	"	kIx"	"
<g/>
Gaters	Gaters	k1gInSc1	Gaters
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
šířeji	šířej	k1gInPc7	šířej
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
fanoušci	fanoušek	k1gMnPc1	fanoušek
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pod	pod	k7c7	pod
komplexem	komplex	k1gInSc7	komplex
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
Mountain	Mountain	k1gInSc1	Mountain
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
skutečná	skutečný	k2eAgFnSc1d1	skutečná
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
Josepha	Joseph	k1gMnSc4	Joseph
Mallozziho	Mallozzi	k1gMnSc4	Mallozzi
a	a	k8xC	a
Paula	Paul	k1gMnSc4	Paul
Mullieho	Mullie	k1gMnSc4	Mullie
k	k	k7c3	k
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
konspiračnímu	konspirační	k2eAgInSc3d1	konspirační
příběhu	příběh	k1gInSc3	příběh
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
sezóny	sezóna	k1gFnSc2	sezóna
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
cesty	cesta	k1gFnPc4	cesta
zpět	zpět	k6eAd1	zpět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
webem	web	k1gInSc7	web
franšízy	franšíza	k1gFnSc2	franšíza
se	se	k3xPyFc4	se
díky	dík	k1gInPc7	dík
speciálním	speciální	k2eAgNnSc7d1	speciální
ujednáním	ujednání	k1gNnSc7	ujednání
s	s	k7c7	s
MGM	MGM	kA	MGM
stala	stát	k5eAaPmAgFnS	stát
fanouškovská	fanouškovský	k2eAgFnSc1d1	fanouškovská
stránka	stránka	k1gFnSc1	stránka
GateWorld	GateWorld	k1gMnSc1	GateWorld
<g/>
;	;	kIx,	;
její	její	k3xOp3gMnSc1	její
zakladatel	zakladatel	k1gMnSc1	zakladatel
Darren	Darrna	k1gFnPc2	Darrna
Sumner	Sumnra	k1gFnPc2	Sumnra
později	pozdě	k6eAd2	pozdě
také	také	k9	také
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
editor	editor	k1gInSc1	editor
oficiálního	oficiální	k2eAgInSc2d1	oficiální
magazínu	magazín	k1gInSc2	magazín
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
a	a	k8xC	a
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
komiksy	komiks	k1gInPc4	komiks
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
neobsahovaly	obsahovat	k5eNaImAgFnP	obsahovat
chyby	chyba	k1gFnPc1	chyba
v	v	k7c6	v
kontinuitě	kontinuita	k1gFnSc6	kontinuita
vůči	vůči	k7c3	vůči
seriálu	seriál	k1gInSc3	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Komik	komik	k1gMnSc1	komik
Pierre	Pierr	k1gInSc5	Pierr
Bernard	Bernard	k1gMnSc1	Bernard
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
fanoušky	fanoušek	k1gMnPc7	fanoušek
stal	stát	k5eAaPmAgMnS	stát
známým	známý	k1gMnSc7	známý
díky	díky	k7c3	díky
svým	svůj	k3xOyFgInPc3	svůj
skečům	skeč	k1gInPc3	skeč
"	"	kIx"	"
<g/>
Recliner	Recliner	k1gInSc1	Recliner
of	of	k?	of
Rage	Rage	k1gInSc1	Rage
<g/>
"	"	kIx"	"
v	v	k7c4	v
talk	talk	k1gInSc4	talk
show	show	k1gFnSc2	show
Late	lat	k1gInSc5	lat
Night	Nighta	k1gFnPc2	Nighta
with	with	k1gMnSc1	with
Conan	Conan	k1gMnSc1	Conan
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Brien	Brien	k1gInSc4	Brien
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
věnoval	věnovat	k5eAaImAgMnS	věnovat
Hvězdné	hvězdný	k2eAgFnSc3d1	hvězdná
bráně	brána	k1gFnSc3	brána
<g/>
.	.	kIx.	.
</s>
<s>
Producenti	producent	k1gMnPc1	producent
seriálu	seriál	k1gInSc2	seriál
jej	on	k3xPp3gInSc4	on
později	pozdě	k6eAd2	pozdě
pozvali	pozvat	k5eAaPmAgMnP	pozvat
pro	pro	k7c4	pro
cameo	cameo	k1gNnSc4	cameo
role	role	k1gFnSc2	role
v	v	k7c6	v
epizodách	epizoda	k1gFnPc6	epizoda
"	"	kIx"	"
<g/>
Hodina	hodina	k1gFnSc1	hodina
H	H	kA	H
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Dvoustá	Dvoustý	k2eAgFnSc1d1	Dvoustý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
fanouškovským	fanouškovský	k2eAgNnSc7d1	fanouškovské
setkáním	setkání	k1gNnSc7	setkání
Gatecon	Gatecon	k1gMnSc1	Gatecon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
pořádán	pořádán	k2eAgInSc1d1	pořádán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Vancouveru	Vancouver	k1gInSc2	Vancouver
a	a	k8xC	a
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgMnPc3d1	ostatní
conům	con	k1gMnPc3	con
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
účastnili	účastnit	k5eAaImAgMnP	účastnit
herci	herec	k1gMnPc1	herec
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
štábu	štáb	k1gInSc2	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Cony	Con	k2eAgFnPc1d1	Con
Hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
brány	brána	k1gFnPc1	brána
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Creation	Creation	k1gInSc1	Creation
Entertainment	Entertainment	k1gInSc1	Entertainment
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Official	Official	k1gMnSc1	Official
Stargate	Stargat	k1gInSc5	Stargat
SG-1	SG-1	k1gMnSc1	SG-1
and	and	k?	and
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
Tour	Tour	k1gMnSc1	Tour
<g/>
"	"	kIx"	"
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
probíhaly	probíhat	k5eAaImAgFnP	probíhat
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
Creation	Creation	k1gInSc4	Creation
Entertainment	Entertainment	k1gInSc4	Entertainment
nezískala	získat	k5eNaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
licenci	licence	k1gFnSc4	licence
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Wolf	Wolf	k1gMnSc1	Wolf
Events	Eventsa	k1gFnPc2	Eventsa
organizuje	organizovat	k5eAaBmIp3nS	organizovat
mnoho	mnoho	k4c1	mnoho
conů	con	k1gInPc2	con
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
českým	český	k2eAgNnSc7d1	české
setkáním	setkání	k1gNnSc7	setkání
je	být	k5eAaImIp3nS	být
akce	akce	k1gFnSc1	akce
rovněž	rovněž	k9	rovněž
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Gatecon	Gatecon	k1gInSc1	Gatecon
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
pořádá	pořádat	k5eAaImIp3nS	pořádat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Festivalu	festival	k1gInSc2	festival
fantazie	fantazie	k1gFnSc2	fantazie
v	v	k7c6	v
Chotěboři	Chotěboř	k1gFnSc6	Chotěboř
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
bylo	být	k5eAaImAgNnS	být
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
mnoho	mnoho	k4c1	mnoho
spin-offových	spinffův	k2eAgInPc2d1	spin-offův
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
ROC	ROC	kA	ROC
čtyři	čtyři	k4xCgInPc1	čtyři
romány	román	k1gInPc1	román
od	od	k7c2	od
Ashley	Ashlea	k1gFnSc2	Ashlea
McConnell	McConnella	k1gFnPc2	McConnella
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
začalo	začít	k5eAaPmAgNnS	začít
britské	britský	k2eAgNnSc1d1	Britské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Fandemonium	Fandemonium	k1gNnSc1	Fandemonium
Press	Press	k1gInSc4	Press
vydávat	vydávat	k5eAaPmF	vydávat
novou	nový	k2eAgFnSc4d1	nová
sérii	série	k1gFnSc4	série
licencovaných	licencovaný	k2eAgInPc2d1	licencovaný
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tyto	tento	k3xDgFnPc1	tento
knihy	kniha	k1gFnPc1	kniha
nebyly	být	k5eNaImAgFnP	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skončil	skončit	k5eAaPmAgInS	skončit
konflikt	konflikt	k1gInSc1	konflikt
licencí	licence	k1gFnPc2	licence
s	s	k7c7	s
ROC	ROC	kA	ROC
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgInPc1d1	dostupný
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
časopis	časopis	k1gInSc1	časopis
Stargate	Stargat	k1gInSc5	Stargat
Magazine	Magazin	k1gInSc5	Magazin
byl	být	k5eAaImAgInS	být
vydáván	vydávat	k5eAaImNgMnS	vydávat
v	v	k7c4	v
Titan	titan	k1gInSc4	titan
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
Avatar	Avatar	k1gInSc1	Avatar
Press	Press	k1gInSc1	Press
vydával	vydávat	k5eAaPmAgInS	vydávat
sérii	série	k1gFnSc4	série
komiksů	komiks	k1gInPc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
společnost	společnost	k1gFnSc1	společnost
Big	Big	k1gMnPc2	Big
Finish	Finish	k1gInSc4	Finish
Productions	Productionsa	k1gFnPc2	Productionsa
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
vyrábět	vyrábět	k5eAaImF	vyrábět
audioknihy	audioknih	k1gInPc4	audioknih
s	s	k7c7	s
novými	nový	k2eAgNnPc7d1	nové
dobrodružstvími	dobrodružství	k1gNnPc7	dobrodružství
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
namluveny	namluvit	k5eAaBmNgInP	namluvit
herci	herc	k1gInPc7	herc
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
role-playing	rolelaying	k1gInSc1	role-playing
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
sběratelská	sběratelský	k2eAgFnSc1d1	sběratelská
karetní	karetní	k2eAgFnSc1d1	karetní
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Firmy	firma	k1gFnPc1	firma
Diamond	Diamonda	k1gFnPc2	Diamonda
Select	Select	k2eAgInSc1d1	Select
Toys	Toys	k1gInSc1	Toys
a	a	k8xC	a
Hasbro	Hasbro	k1gNnSc1	Hasbro
uvedly	uvést	k5eAaPmAgFnP	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kolekce	kolekce	k1gFnSc1	kolekce
hraček	hračka	k1gFnPc2	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
Stargate	Stargat	k1gInSc5	Stargat
SG-	SG-	k1gMnSc7	SG-
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Alliance	Alliance	k1gFnSc1	Alliance
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
budoucnost	budoucnost	k1gFnSc1	budoucnost
MMORPG	MMORPG	kA	MMORPG
Stargate	Stargat	k1gInSc5	Stargat
Worlds	Worldsa	k1gFnPc2	Worldsa
a	a	k8xC	a
střílečky	střílečka	k1gFnSc2	střílečka
Stargate	Stargat	k1gInSc5	Stargat
Resistance	Resistanec	k1gInPc4	Resistanec
od	od	k7c2	od
téhož	týž	k3xTgNnSc2	týž
studia	studio	k1gNnSc2	studio
Cheyenne	Cheyenn	k1gInSc5	Cheyenn
Mountain	Mountain	k1gMnSc1	Mountain
Entertainment	Entertainment	k1gMnSc1	Entertainment
(	(	kIx(	(
<g/>
CME	CME	kA	CME
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyjasnila	vyjasnit	k5eAaPmAgFnS	vyjasnit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
po	po	k7c6	po
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
MGM	MGM	kA	MGM
neprodloužit	prodloužit	k5eNaPmF	prodloužit
CME	CME	kA	CME
dále	daleko	k6eAd2	daleko
licenci	licence	k1gFnSc4	licence
na	na	k7c4	na
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
Arkalis	Arkalis	k1gFnSc2	Arkalis
Interactive	Interactiv	k1gInSc5	Interactiv
vydalo	vydat	k5eAaPmAgNnS	vydat
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
MGM	MGM	kA	MGM
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
videohry	videohra	k1gFnSc2	videohra
Stargate	Stargat	k1gInSc5	Stargat
SG-	SG-	k1gMnSc7	SG-
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
Unleashed	Unleashed	k1gInSc1	Unleashed
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
"	"	kIx"	"
<g/>
epizod	epizoda	k1gFnPc2	epizoda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
s	s	k7c7	s
operačními	operační	k2eAgInPc7d1	operační
systémy	systém	k1gInPc7	systém
iOS	iOS	k?	iOS
a	a	k8xC	a
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgFnPc3d1	hlavní
postavám	postava	k1gFnPc3	postava
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
týmu	tým	k1gInSc2	tým
SG-	SG-	k1gFnSc2	SG-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
propůjčili	propůjčit	k5eAaPmAgMnP	propůjčit
své	svůj	k3xOyFgInPc4	svůj
hlasy	hlas	k1gInPc4	hlas
herci	herec	k1gMnSc6	herec
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
Dean	Dean	k1gMnSc1	Dean
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Shanks	Shanks	k1gInSc1	Shanks
<g/>
,	,	kIx,	,
Amanda	Amanda	k1gFnSc1	Amanda
Tapping	Tapping	k1gInSc1	Tapping
a	a	k8xC	a
Christopher	Christophra	k1gFnPc2	Christophra
Judge	Judg	k1gFnSc2	Judg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
Hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
brány	brána	k1gFnPc1	brána
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
čtyři	čtyři	k4xCgFnPc1	čtyři
zábavní	zábavní	k2eAgFnPc1d1	zábavní
atrakce	atrakce	k1gFnPc1	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Simulátory	simulátor	k1gInPc1	simulátor
Stargate	Stargat	k1gInSc5	Stargat
SG-3000	SG-3000	k1gMnSc2	SG-3000
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
ve	v	k7c6	v
Space	Spaka	k1gFnSc6	Spaka
Parku	park	k1gInSc2	park
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
Brémách	Brémy	k1gFnPc6	Brémy
a	a	k8xC	a
v	v	k7c6	v
parcích	park	k1gInPc6	park
Six	Six	k1gFnSc1	Six
Flags	Flags	k1gInSc1	Flags
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
a	a	k8xC	a
Louisvillu	Louisvill	k1gInSc2	Louisvill
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
seriálu	seriál	k1gInSc3	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byly	být	k5eAaImAgFnP	být
natočeny	natočen	k2eAgInPc4d1	natočen
spin-offové	spinffový	k2eAgInPc4d1	spin-offový
seriály	seriál	k1gInPc4	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
,	,	kIx,	,
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
a	a	k8xC	a
animovaný	animovaný	k2eAgInSc1d1	animovaný
Stargate	Stargat	k1gInSc5	Stargat
Infinity	Infinit	k1gInPc4	Infinit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
přinesla	přinést	k5eAaPmAgFnS	přinést
výroba	výroba	k1gFnSc1	výroba
deseti	deset	k4xCc2	deset
řad	řada	k1gFnPc2	řada
seriálu	seriál	k1gInSc2	seriál
a	a	k8xC	a
několika	několik	k4yIc2	několik
řad	řada	k1gFnPc2	řada
Atlantidy	Atlantida	k1gFnSc2	Atlantida
Britské	britský	k2eAgFnSc2d1	britská
Kolumbii	Kolumbie	k1gFnSc3	Kolumbie
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
milionů	milion	k4xCgInPc2	milion
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
Cohen	Cohen	k2eAgMnSc1d1	Cohen
<g/>
,	,	kIx,	,
výkonný	výkonný	k2eAgMnSc1d1	výkonný
viceprezident	viceprezident	k1gMnSc1	viceprezident
MGM	MGM	kA	MGM
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
bránu	brána	k1gFnSc4	brána
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
spin-offy	spinff	k1gInPc1	spin-off
jako	jako	k8xS	jako
televizní	televizní	k2eAgInSc1d1	televizní
protějšek	protějšek	k1gInSc1	protějšek
franšízy	franšíza	k1gFnSc2	franšíza
James	James	k1gMnSc1	James
Bond	bond	k1gInSc1	bond
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
rovněž	rovněž	k9	rovněž
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
MGM	MGM	kA	MGM
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
ziskovou	ziskový	k2eAgFnSc4d1	zisková
a	a	k8xC	a
zlepšující	zlepšující	k2eAgFnSc4d1	zlepšující
jejich	jejich	k3xOp3gFnSc4	jejich
image	image	k1gFnSc4	image
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Reading	Reading	k1gInSc1	Reading
Stargate	Stargat	k1gInSc5	Stargat
SG-1	SG-1	k1gFnPc3	SG-1
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
od	od	k7c2	od
Stana	Stan	k1gMnSc2	Stan
Beelera	Beeler	k1gMnSc2	Beeler
a	a	k8xC	a
Lisy	lis	k1gInPc7	lis
Dickson	Dicksona	k1gFnPc2	Dicksona
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgNnPc7d1	jediné
sci-fi	scii	k1gNnPc7	sci-fi
seriály	seriál	k1gInPc1	seriál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
překračují	překračovat	k5eAaImIp3nP	překračovat
výdrž	výdrž	k1gFnSc4	výdrž
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
Pán	pán	k1gMnSc1	pán
času	čas	k1gInSc2	čas
a	a	k8xC	a
franšíza	franšíz	k1gMnSc2	franšíz
Star	Star	kA	Star
Trek	Trek	k1gInSc1	Trek
<g/>
;	;	kIx,	;
nicméně	nicméně	k8xC	nicméně
Akta	akta	k1gNnPc4	akta
X	X	kA	X
a	a	k8xC	a
Buffy	buffa	k1gFnSc2	buffa
<g/>
/	/	kIx~	/
<g/>
Angel	angel	k1gMnSc1	angel
mají	mít	k5eAaImIp3nP	mít
též	též	k9	též
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
uvedl	uvést	k5eAaPmAgMnS	uvést
jako	jako	k8xS	jako
důvody	důvod	k1gInPc4	důvod
dlouhověkosti	dlouhověkost	k1gFnSc2	dlouhověkost
seriálu	seriál	k1gInSc2	seriál
kontinuitu	kontinuita	k1gFnSc4	kontinuita
ve	v	k7c6	v
výrobním	výrobní	k2eAgInSc6d1	výrobní
štábu	štáb	k1gInSc6	štáb
a	a	k8xC	a
oddanost	oddanost	k1gFnSc4	oddanost
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
202	[number]	k4	202
<g/>
.	.	kIx.	.
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Spolek	spolek	k1gInSc1	spolek
zlodějů	zloděj	k1gMnPc2	zloděj
<g/>
"	"	kIx"	"
překonala	překonat	k5eAaPmAgFnS	překonat
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
seriál	seriál	k1gInSc1	seriál
Akta	akta	k1gNnPc4	akta
X	X	kA	X
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejdéle	dlouho	k6eAd3	dlouho
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
severoamerický	severoamerický	k2eAgInSc4d1	severoamerický
televizní	televizní	k2eAgFnSc4d1	televizní
sci-fi	scii	k1gFnSc4	sci-fi
seriál	seriál	k1gInSc1	seriál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
samotná	samotný	k2eAgFnSc1d1	samotná
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
předstižena	předstižen	k2eAgFnSc1d1	předstižena
seriálem	seriál	k1gInSc7	seriál
Smallville	Smallville	k1gNnSc2	Smallville
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
Pána	pán	k1gMnSc2	pán
času	čas	k1gInSc2	čas
ale	ale	k8xC	ale
protestovali	protestovat	k5eAaBmAgMnP	protestovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
proti	proti	k7c3	proti
zařazení	zařazení	k1gNnSc3	zařazení
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
mezi	mezi	k7c4	mezi
Guinnessovy	Guinnessův	k2eAgInPc4d1	Guinnessův
světové	světový	k2eAgInPc4d1	světový
rekordy	rekord	k1gInPc4	rekord
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
nejdéle	dlouho	k6eAd3	dlouho
souvisle	souvisle	k6eAd1	souvisle
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
sci-fi	scii	k1gNnPc2	sci-fi
seriál	seriál	k1gInSc1	seriál
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1963	[number]	k4	1963
a	a	k8xC	a
1989	[number]	k4	1989
bylo	být	k5eAaImAgNnS	být
souvisle	souvisle	k6eAd1	souvisle
vysíláno	vysílat	k5eAaImNgNnS	vysílat
695	[number]	k4	695
epizod	epizoda	k1gFnPc2	epizoda
tohoto	tento	k3xDgInSc2	tento
britského	britský	k2eAgInSc2d1	britský
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
kultovních	kultovní	k2eAgInPc2d1	kultovní
seriálů	seriál	k1gInPc2	seriál
žebříčku	žebříček	k1gInSc2	žebříček
časopisu	časopis	k1gInSc2	časopis
TV	TV	kA	TV
Guide	Guid	k1gInSc5	Guid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
obsadily	obsadit	k5eAaPmAgFnP	obsadit
seriály	seriál	k1gInPc4	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
a	a	k8xC	a
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c4	o
nejpopulárnější	populární	k2eAgInSc4d3	nejpopulárnější
kultovní	kultovní	k2eAgInSc4d1	kultovní
seriál	seriál	k1gInSc4	seriál
na	na	k7c6	na
britské	britský	k2eAgFnSc6d1	britská
webové	webový	k2eAgFnSc6d1	webová
stránce	stránka	k1gFnSc6	stránka
Cult	Cult	k1gMnSc1	Cult
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
byla	být	k5eAaImAgFnS	být
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
zahrnuta	zahrnout	k5eAaPmNgFnS	zahrnout
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
"	"	kIx"	"
<g/>
17	[number]	k4	17
nejkultovnějších	kultovný	k2eAgInPc2d3	nejkultovnější
seriálů	seriál	k1gInPc2	seriál
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
jsme	být	k5eAaImIp1nP	být
vynechali	vynechat	k5eAaPmAgMnP	vynechat
<g/>
"	"	kIx"	"
magazínu	magazín	k1gInSc2	magazín
Entertainment	Entertainment	k1gInSc1	Entertainment
Weekly	Weekl	k1gInPc1	Weekl
<g/>
.	.	kIx.	.
</s>
<s>
Astronomům	astronom	k1gMnPc3	astronom
David	David	k1gMnSc1	David
J.	J.	kA	J.
Tholen	Tholen	k1gInSc1	Tholen
a	a	k8xC	a
Roy	Roy	k1gMnSc1	Roy
A.	A.	kA	A.
Tucker	Tucker	k1gInSc1	Tucker
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
moc	moc	k6eAd1	moc
zalíbil	zalíbit	k5eAaPmAgMnS	zalíbit
hlavní	hlavní	k2eAgMnSc1d1	hlavní
padouch	padouch	k1gMnSc1	padouch
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
Apophis	Apophis	k1gFnSc2	Apophis
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
planetku	planetka	k1gFnSc4	planetka
Apophis	Apophis	k1gFnSc2	Apophis
<g/>
.	.	kIx.	.
</s>
