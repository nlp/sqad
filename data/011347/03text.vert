<p>
<s>
Bhútán	Bhútán	k1gInSc1	Bhútán
(	(	kIx(	(
<g/>
v	v	k7c6	v
dzongkä	dzongkä	k?	dzongkä
འ	འ	k?	འ
<g/>
ྲ	ྲ	k?	ྲ
<g/>
ུ	ུ	k?	ུ
<g/>
ག	ག	k?	ག
<g/>
་	་	k?	་
<g/>
ཡ	ཡ	k?	ཡ
<g/>
ུ	ུ	k?	ུ
<g/>
ལ	ལ	k?	ལ
<g/>
་	་	k?	་
Dugjul	Dugjul	k1gInSc1	Dugjul
<g/>
,	,	kIx,	,
Wylie	Wylie	k1gFnSc1	Wylie
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
brug-yul	brugul	k1gInSc1	brug-yul
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
konci	konec	k1gInSc6	konec
himálajských	himálajský	k2eAgFnPc2d1	himálajská
hor.	hor.	k?	hor.
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
východě	východ	k1gInSc6	východ
a	a	k8xC	a
západě	západ	k1gInSc6	západ
sousedí	sousedit	k5eAaImIp3nP	sousedit
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Tibetskou	tibetský	k2eAgFnSc7d1	tibetská
autonomní	autonomní	k2eAgFnSc7d1	autonomní
oblastí	oblast	k1gFnSc7	oblast
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nedalekého	daleký	k2eNgInSc2d1	nedaleký
Nepálu	Nepál	k1gInSc2	Nepál
je	být	k5eAaImIp3nS	být
Bhútán	Bhútán	k1gInSc1	Bhútán
oddělen	oddělit	k5eAaPmNgInS	oddělit
indickým	indický	k2eAgInSc7d1	indický
státem	stát	k1gInSc7	stát
Sikkim	Sikkima	k1gFnPc2	Sikkima
<g/>
.	.	kIx.	.
</s>
<s>
Bhútánu	Bhútán	k1gInSc3	Bhútán
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
hřmícího	hřmící	k2eAgMnSc2d1	hřmící
draka	drak	k1gMnSc2	drak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
navázala	navázat	k5eAaPmAgFnS	navázat
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Bhútánem	Bhútán	k1gInSc7	Bhútán
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
ty	ten	k3xDgFnPc4	ten
usilovala	usilovat	k5eAaImAgFnS	usilovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Česka	Česko	k1gNnSc2	Česko
navázal	navázat	k5eAaPmAgInS	navázat
Bhútán	Bhútán	k1gInSc4	Bhútán
diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
styky	styk	k1gInPc1	styk
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
devíti	devět	k4xCc7	devět
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
země	země	k1gFnSc1	země
oficiální	oficiální	k2eAgInPc1d1	oficiální
diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
styky	styk	k1gInPc1	styk
navázány	navázán	k2eAgInPc1d1	navázán
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
26	[number]	k4	26
zeměmi	zem	k1gFnPc7	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
nebylo	být	k5eNaImAgNnS	být
ani	ani	k8xC	ani
pět	pět	k4xCc1	pět
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
Rady	rada	k1gFnSc2	rada
Bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
má	mít	k5eAaImIp3nS	mít
Bhútán	Bhútán	k1gInSc1	Bhútán
navázány	navázán	k2eAgInPc1d1	navázán
styky	styk	k1gInPc1	styk
s	s	k7c7	s
osmi	osm	k4xCc7	osm
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
a	a	k8xC	a
Bruselu	Brusel	k1gInSc6	Brusel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
dějin	dějiny	k1gFnPc2	dějiny
Bhútánu	Bhútán	k1gInSc2	Bhútán
jsou	být	k5eAaImIp3nP	být
obestřeny	obestřít	k5eAaPmNgInP	obestřít
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
území	území	k1gNnSc2	území
postupně	postupně	k6eAd1	postupně
osidlovaly	osidlovat	k5eAaImAgInP	osidlovat
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přicházely	přicházet	k5eAaImAgFnP	přicházet
z	z	k7c2	z
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
vlastní	vlastní	k2eAgInSc4d1	vlastní
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Bhútán	Bhútán	k1gInSc1	Bhútán
stal	stát	k5eAaPmAgInS	stát
knížectvím	knížectví	k1gNnSc7	knížectví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
objevil	objevit	k5eAaPmAgMnS	objevit
nový	nový	k2eAgMnSc1d1	nový
sjednotitel	sjednotitel	k1gMnSc1	sjednotitel
<g/>
,	,	kIx,	,
Namgjal	Namgjal	k1gMnSc1	Namgjal
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zkonsolidoval	zkonsolidovat	k5eAaPmAgMnS	zkonsolidovat
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgMnS	zavést
právní	právní	k2eAgInSc4d1	právní
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
vynikal	vynikat	k5eAaImAgInS	vynikat
častými	častý	k2eAgInPc7d1	častý
nájezdy	nájezd	k1gInPc7	nájezd
a	a	k8xC	a
únosy	únos	k1gInPc1	únos
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
území	území	k1gNnSc6	území
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
tohoto	tento	k3xDgMnSc4	tento
pro	pro	k7c4	pro
Bhútán	Bhútán	k1gInSc4	Bhútán
výjimečného	výjimečný	k2eAgMnSc2d1	výjimečný
vládce	vládce	k1gMnSc2	vládce
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
po	po	k7c4	po
mnohá	mnohý	k2eAgNnPc4d1	mnohé
staletí	staletí	k1gNnPc4	staletí
zmítala	zmítat	k5eAaImAgFnS	zmítat
v	v	k7c6	v
kmenových	kmenový	k2eAgInPc6d1	kmenový
rozbrojích	rozbroj	k1gInPc6	rozbroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1772	[number]	k4	1772
se	se	k3xPyFc4	se
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
spor	spor	k1gInSc1	spor
s	s	k7c7	s
Východoindickou	východoindický	k2eAgFnSc7d1	Východoindická
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
i	i	k9	i
přes	přes	k7c4	přes
mír	mír	k1gInSc4	mír
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
vyvlastnění	vyvlastnění	k1gNnSc6	vyvlastnění
osmnácti	osmnáct	k4xCc2	osmnáct
údolí	údolí	k1gNnPc2	údolí
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
a	a	k8xC	a
hlavních	hlavní	k2eAgFnPc2d1	hlavní
dopravních	dopravní	k2eAgFnPc2d1	dopravní
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
fakticky	fakticky	k6eAd1	fakticky
anektována	anektovat	k5eAaBmNgFnS	anektovat
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Kmenové	kmenový	k2eAgInPc1d1	kmenový
boje	boj	k1gInPc1	boj
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostal	dostat	k5eAaPmAgMnS	dostat
po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
Ugjan	Ugjan	k1gMnSc1	Ugjan
Wangčhug	Wangčhug	k1gMnSc1	Wangčhug
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
posléze	posléze	k6eAd1	posléze
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgMnSc1d1	jmenován
světskými	světský	k2eAgMnPc7d1	světský
velmoži	velmož	k1gMnPc7	velmož
a	a	k8xC	a
vysokými	vysoký	k2eAgMnPc7d1	vysoký
lámy	láma	k1gMnPc7	láma
dědičným	dědičný	k2eAgMnSc7d1	dědičný
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
Bhútán	Bhútán	k1gInSc1	Bhútán
stal	stát	k5eAaPmAgInS	stát
britským	britský	k2eAgInSc7d1	britský
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
a	a	k8xC	a
obrannou	obranný	k2eAgFnSc7d1	obranná
politikou	politika	k1gFnSc7	politika
země	zem	k1gFnSc2	zem
převzala	převzít	k5eAaPmAgFnS	převzít
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
v	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
je	být	k5eAaImIp3nS	být
Bhútán	Bhútán	k1gInSc4	Bhútán
členem	člen	k1gInSc7	člen
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
zde	zde	k6eAd1	zde
platil	platit	k5eAaImAgInS	platit
zákaz	zákaz	k1gInSc1	zákaz
sledování	sledování	k1gNnSc2	sledování
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
internet	internet	k1gInSc1	internet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zakázán	zakázat	k5eAaPmNgInS	zakázat
prodej	prodej	k1gInSc1	prodej
tabáku	tabák	k1gInSc2	tabák
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
zde	zde	k6eAd1	zde
zákaz	zákaz	k1gInSc4	zákaz
kouření	kouření	k1gNnSc2	kouření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Bhútán	Bhútán	k1gInSc1	Bhútán
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
hornaté	hornatý	k2eAgFnSc2d1	hornatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
50	[number]	k4	50
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
přes	přes	k7c4	přes
3	[number]	k4	3
000	[number]	k4	000
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
této	tento	k3xDgFnSc2	tento
malé	malý	k2eAgFnSc2d1	malá
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Kula	kula	k1gFnSc1	kula
Kangri	Kangr	k1gFnSc2	Kangr
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
7	[number]	k4	7
554	[number]	k4	554
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
nad	nad	k7c7	nad
4	[number]	k4	4
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
leží	ležet	k5eAaImIp3nS	ležet
trvale	trvale	k6eAd1	trvale
sníh	sníh	k1gInSc4	sníh
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
horské	horský	k2eAgInPc1d1	horský
ledovce	ledovec	k1gInPc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
<g/>
,	,	kIx,	,
nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Amo	Amo	k1gMnPc1	Amo
<g/>
,	,	kIx,	,
Kuri	Kur	k1gMnPc1	Kur
<g/>
,	,	kIx,	,
Manas	Manas	k1gMnSc1	Manas
a	a	k8xC	a
Sankoš	Sankoš	k1gMnSc1	Sankoš
(	(	kIx(	(
<g/>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
řeka	řeka	k1gFnSc1	řeka
země	země	k1gFnSc1	země
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
190	[number]	k4	190
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
55	[number]	k4	55
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
monzunové	monzunový	k2eAgInPc4d1	monzunový
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
jsou	být	k5eAaImIp3nP	být
lesy	les	k1gInPc1	les
smíšené	smíšený	k2eAgInPc1d1	smíšený
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
borovic	borovice	k1gFnPc2	borovice
<g/>
,	,	kIx,	,
jedlí	jedle	k1gFnPc2	jedle
<g/>
,	,	kIx,	,
dubů	dub	k1gInPc2	dub
a	a	k8xC	a
pěnišníků	pěnišník	k1gInPc2	pěnišník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
a	a	k8xC	a
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c4	mnoho
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
sloni	slon	k1gMnPc1	slon
<g/>
,	,	kIx,	,
takini	takin	k1gMnPc1	takin
<g/>
,	,	kIx,	,
tygři	tygr	k1gMnPc1	tygr
<g/>
,	,	kIx,	,
irbisové	irbisový	k2eAgFnPc1d1	irbisový
<g/>
,	,	kIx,	,
levharti	levhart	k1gMnPc1	levhart
<g/>
,	,	kIx,	,
medvědi	medvěd	k1gMnPc1	medvěd
pyskatí	pyskatý	k2eAgMnPc1d1	pyskatý
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
jelenů	jelen	k1gMnPc2	jelen
<g/>
,	,	kIx,	,
divokých	divoký	k2eAgFnPc2d1	divoká
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
hlodavců	hlodavec	k1gMnPc2	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
i	i	k9	i
různí	různý	k2eAgMnPc1d1	různý
primáti	primát	k1gMnPc1	primát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vzácný	vzácný	k2eAgMnSc1d1	vzácný
hulman	hulman	k1gMnSc1	hulman
zlatý	zlatý	k2eAgMnSc1d1	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Bohatá	bohatý	k2eAgFnSc1d1	bohatá
je	být	k5eAaImIp3nS	být
též	též	k9	též
ptačí	ptačí	k2eAgFnSc1d1	ptačí
fauna	fauna	k1gFnSc1	fauna
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
více	hodně	k6eAd2	hodně
než	než	k8xS	než
770	[number]	k4	770
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
převládá	převládat	k5eAaImIp3nS	převládat
chladné	chladný	k2eAgNnSc1d1	chladné
a	a	k8xC	a
suché	suchý	k2eAgNnSc1d1	suché
vysokohorské	vysokohorský	k2eAgNnSc1d1	vysokohorské
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ale	ale	k8xC	ale
převládá	převládat	k5eAaImIp3nS	převládat
subtropické	subtropický	k2eAgNnSc1d1	subtropické
klima	klima	k1gNnSc1	klima
vlhkého	vlhký	k2eAgInSc2d1	vlhký
monzunového	monzunový	k2eAgInSc2d1	monzunový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
(	(	kIx(	(
<g/>
či	či	k8xC	či
od	od	k7c2	od
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
do	do	k7c2	do
září	září	k1gNnSc2	září
až	až	k8xS	až
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
v	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
ročně	ročně	k6eAd1	ročně
spadne	spadnout	k5eAaPmIp3nS	spadnout
1	[number]	k4	1
500	[number]	k4	500
až	až	k9	až
3	[number]	k4	3
000	[number]	k4	000
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
země	zem	k1gFnSc2	zem
Thimbú	Thimbú	k1gFnSc2	Thimbú
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
úhrn	úhrn	k1gInSc4	úhrn
srážek	srážka	k1gFnPc2	srážka
15	[number]	k4	15
mm	mm	kA	mm
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pak	pak	k6eAd1	pak
15	[number]	k4	15
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
373	[number]	k4	373
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
bhútánský	bhútánský	k2eAgMnSc1d1	bhútánský
ngultrum	ngultrum	k1gNnSc4	ngultrum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
navázán	navázán	k2eAgInSc1d1	navázán
na	na	k7c4	na
indickou	indický	k2eAgFnSc4d1	indická
rupii	rupie	k1gFnSc4	rupie
<g/>
.	.	kIx.	.
</s>
<s>
Rupie	rupie	k1gFnSc1	rupie
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
ngultrumu	ngultrum	k1gInSc2	ngultrum
také	také	k9	také
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k8xS	jako
zákonné	zákonný	k2eAgNnSc1d1	zákonné
platidlo	platidlo	k1gNnSc1	platidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgFnPc2d3	nejmenší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
HDP	HDP	kA	HDP
narostl	narůst	k5eAaPmAgInS	narůst
o	o	k7c4	o
8	[number]	k4	8
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
o	o	k7c4	o
14	[number]	k4	14
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dokonce	dokonce	k9	dokonce
o	o	k7c4	o
22,4	[number]	k4	22,4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
především	především	k9	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dokončení	dokončení	k1gNnSc2	dokončení
stavby	stavba	k1gFnSc2	stavba
hydroelektrárny	hydroelektrárna	k1gFnSc2	hydroelektrárna
Tala	Tal	k1gInSc2	Tal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
HDP	HDP	kA	HDP
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
paritu	parita	k1gFnSc4	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
8	[number]	k4	8
253	[number]	k4	253
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
by	by	kYmCp3nP	by
toto	tento	k3xDgNnSc4	tento
číslo	číslo	k1gNnSc4	číslo
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
MMF	MMF	kA	MMF
mělo	mít	k5eAaImAgNnS	mít
narůst	narůst	k5eAaPmF	narůst
na	na	k7c4	na
12	[number]	k4	12
tisíc	tisíc	k4xCgInSc4	tisíc
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
odvětvím	odvětví	k1gNnSc7	odvětví
ekonomiky	ekonomika	k1gFnSc2	ekonomika
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
hydroelektrárnách	hydroelektrárna	k1gFnPc6	hydroelektrárna
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
prodej	prodej	k1gInSc4	prodej
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
neméně	málo	k6eNd2	málo
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
obživu	obživa	k1gFnSc4	obživa
55,4	[number]	k4	55,4
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
je	být	k5eAaImIp3nS	být
lesnictví	lesnictví	k1gNnSc4	lesnictví
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
týkové	týkový	k2eAgNnSc1d1	týkové
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
lak	lak	k1gInSc1	lak
<g/>
,	,	kIx,	,
šelak	šelak	k1gInSc1	šelak
a	a	k8xC	a
vosk	vosk	k1gInSc1	vosk
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
nepříliš	příliš	k6eNd1	příliš
velkou	velký	k2eAgFnSc4d1	velká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
čím	co	k3yQnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
významnější	významný	k2eAgFnSc4d2	významnější
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
i	i	k9	i
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
návštěvníci	návštěvník	k1gMnPc1	návštěvník
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
u	u	k7c2	u
bhútánské	bhútánský	k2eAgFnSc2d1	bhútánská
cestovní	cestovní	k2eAgFnSc2d1	cestovní
kanceláře	kancelář	k1gFnSc2	kancelář
a	a	k8xC	a
zaplatit	zaplatit	k5eAaPmF	zaplatit
poplatek	poplatek	k1gInSc4	poplatek
250	[number]	k4	250
$	$	kIx~	$
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
návštěvy	návštěva	k1gFnSc2	návštěva
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
jsou	být	k5eAaImIp3nP	být
služby	služba	k1gFnPc1	služba
jako	jako	k8xS	jako
ubytování	ubytování	k1gNnSc1	ubytování
<g/>
,	,	kIx,	,
jídlo	jídlo	k1gNnSc1	jídlo
a	a	k8xC	a
cestování	cestování	k1gNnSc1	cestování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
může	moct	k5eAaImIp3nS	moct
mnoho	mnoho	k6eAd1	mnoho
turistů	turist	k1gMnPc2	turist
odradit	odradit	k5eAaPmF	odradit
<g/>
.	.	kIx.	.
</s>
<s>
Bhútán	Bhútán	k1gInSc1	Bhútán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
37	[number]	k4	37
482	[number]	k4	482
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	Hranice	k1gFnPc1	Hranice
byly	být	k5eAaImAgFnP	být
cizincům	cizinec	k1gMnPc3	cizinec
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
exportním	exportní	k2eAgMnSc7d1	exportní
partnerem	partner	k1gMnSc7	partner
Bhútánu	Bhútán	k1gInSc2	Bhútán
je	být	k5eAaImIp3nS	být
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
58,6	[number]	k4	58,6
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
Hongkong	Hongkong	k1gInSc4	Hongkong
(	(	kIx(	(
<g/>
30,1	[number]	k4	30,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
(	(	kIx(	(
<g/>
7,3	[number]	k4	7,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Tibetem	Tibet	k1gInSc7	Tibet
je	být	k5eAaImIp3nS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
je	být	k5eAaImIp3nS	být
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
<s>
Importní	importní	k2eAgMnPc1d1	importní
partneři	partner	k1gMnPc1	partner
jsou	být	k5eAaImIp3nP	být
Indie	Indie	k1gFnPc4	Indie
(	(	kIx(	(
<g/>
74,5	[number]	k4	74,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
7,4	[number]	k4	7,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Import	import	k1gInSc1	import
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
export	export	k1gInSc4	export
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgFnSc4d1	negativní
obchodní	obchodní	k2eAgFnSc4d1	obchodní
bilanci	bilance	k1gFnSc4	bilance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
pouhých	pouhý	k2eAgInPc2d1	pouhý
8	[number]	k4	8
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
pastviny	pastvina	k1gFnPc1	pastvina
pak	pak	k6eAd1	pak
dalších	další	k2eAgInPc2d1	další
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
proso	proso	k1gNnSc1	proso
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
juta	juta	k1gFnSc1	juta
<g/>
,	,	kIx,	,
pohanka	pohanka	k1gFnSc1	pohanka
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnPc1	luštěnina
a	a	k8xC	a
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
sadařství	sadařství	k1gNnSc4	sadařství
<g/>
.	.	kIx.	.
</s>
<s>
Kočovníci	kočovník	k1gMnPc1	kočovník
pasou	pást	k5eAaImIp3nP	pást
koně	kůň	k1gMnPc4	kůň
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnPc4	ovce
<g/>
,	,	kIx,	,
jaky	jak	k1gMnPc4	jak
<g/>
,	,	kIx,	,
muly	mul	k1gMnPc4	mul
a	a	k8xC	a
kozy	koza	k1gFnPc4	koza
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
prasata	prase	k1gNnPc1	prase
a	a	k8xC	a
skot	skot	k1gInSc1	skot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
===	===	k?	===
</s>
</p>
<p>
<s>
Zásadní	zásadní	k2eAgFnSc1d1	zásadní
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
Bhútánu	Bhútán	k1gInSc2	Bhútán
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
hydroenergie	hydroenergie	k1gFnSc1	hydroenergie
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zde	zde	k6eAd1	zde
vznikaly	vznikat	k5eAaImAgInP	vznikat
první	první	k4xOgInPc1	první
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
závody	závod	k1gInPc1	závod
<g/>
:	:	kIx,	:
textilní	textilní	k2eAgNnPc1d1	textilní
<g/>
,	,	kIx,	,
zpracovávající	zpracovávající	k2eAgNnPc1d1	zpracovávající
ovoce	ovoce	k1gNnPc1	ovoce
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
cementárny	cementárna	k1gFnSc2	cementárna
<g/>
,	,	kIx,	,
sirkárny	sirkárna	k1gFnSc2	sirkárna
a	a	k8xC	a
pily	pila	k1gFnSc2	pila
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
hlavně	hlavně	k9	hlavně
ocel	ocel	k1gFnSc4	ocel
<g/>
,	,	kIx,	,
feroslitiny	feroslitina	k1gFnPc4	feroslitina
a	a	k8xC	a
cement	cement	k1gInSc4	cement
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
umělecká	umělecký	k2eAgNnPc4d1	umělecké
řemesla	řemeslo	k1gNnPc4	řemeslo
jako	jako	k8xC	jako
tkalcovství	tkalcovství	k1gNnPc4	tkalcovství
<g/>
,	,	kIx,	,
řezbářství	řezbářství	k1gNnSc4	řezbářství
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
ručních	ruční	k2eAgFnPc2d1	ruční
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
kovových	kovový	k2eAgInPc2d1	kovový
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
také	také	k9	také
technologický	technologický	k2eAgInSc1d1	technologický
sektor	sektor	k1gInSc1	sektor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zelené	zelený	k2eAgFnPc4d1	zelená
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
elektronické	elektronický	k2eAgNnSc4d1	elektronické
obchodování	obchodování	k1gNnSc4	obchodování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
Thimphu	Thimph	k1gMnSc3	Thimph
TechPark	TechPark	k1gInSc1	TechPark
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
startupy	startup	k1gInPc4	startup
<g/>
.	.	kIx.	.
<g/>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
černé	černý	k2eAgNnSc1d1	černé
uhlí	uhlí	k1gNnSc1	uhlí
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
mramor	mramor	k1gInSc1	mramor
<g/>
,	,	kIx,	,
sádrovec	sádrovec	k1gInSc1	sádrovec
a	a	k8xC	a
grafit	grafit	k1gInSc1	grafit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Bhútánu	Bhútán	k1gInSc2	Bhútán
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
absolutní	absolutní	k2eAgFnSc2d1	absolutní
monarchie	monarchie	k1gFnSc2	monarchie
v	v	k7c6	v
konstituční	konstituční	k2eAgFnSc6d1	konstituční
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
král	král	k1gMnSc1	král
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
,	,	kIx,	,
Džigme	Džigm	k1gInSc5	Džigm
Singgjä	Singgjä	k1gMnPc4	Singgjä
Wangčhug	Wangčhug	k1gInSc4	Wangčhug
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Radu	rada	k1gFnSc4	rada
ministrů	ministr	k1gMnPc2	ministr
(	(	kIx(	(
<g/>
Lhengye	Lhengye	k1gNnSc1	Lhengye
Zhungtshog	Zhungtshoga	k1gFnPc2	Zhungtshoga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
Duggjalpo	Duggjalpa	k1gFnSc5	Duggjalpa
(	(	kIx(	(
<g/>
Wylie	Wylie	k1gFnPc4	Wylie
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
brug	brug	k1gInSc1	brug
rgyal-po	rgyala	k1gFnSc5	rgyal-pa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Dračí	dračí	k2eAgMnSc1d1	dračí
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Pátým	pátý	k4xOgMnSc7	pátý
Dračím	dračí	k2eAgMnSc7d1	dračí
králem	král	k1gMnSc7	král
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Džigme	Džigm	k1gInSc5	Džigm
Khesar	Khesara	k1gFnPc2	Khesara
Namgjal	Namgjal	k1gMnSc1	Namgjal
Wangčhug	Wangčhug	k1gMnSc1	Wangčhug
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značné	značný	k2eAgFnPc1d1	značná
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
politickém	politický	k2eAgNnSc6d1	politické
uspořádání	uspořádání	k1gNnSc6	uspořádání
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
král	král	k1gMnSc1	král
Džigme	Džigm	k1gInSc5	Džigm
Singgjä	Singgjä	k1gMnPc7	Singgjä
Wangčhug	Wangčhug	k1gMnSc1	Wangčhug
neočekávaně	očekávaně	k6eNd1	očekávaně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
abdikaci	abdikace	k1gFnSc4	abdikace
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Džigme	Džigm	k1gInSc5	Džigm
Khesara	Khesara	k1gFnSc1	Khesara
Namgjala	Namgjala	k1gMnSc4	Namgjala
Wangčhuga	Wangčhug	k1gMnSc4	Wangčhug
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
oznámil	oznámit	k5eAaPmAgMnS	oznámit
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
a	a	k8xC	a
vypsání	vypsání	k1gNnSc4	vypsání
vůbec	vůbec	k9	vůbec
prvních	první	k4xOgFnPc2	první
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
.	.	kIx.	.
</s>
<s>
Jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
parlament	parlament	k1gInSc1	parlament
Tshogdu	Tshogdo	k1gNnSc3	Tshogdo
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
namísto	namísto	k7c2	namísto
něj	on	k3xPp3gMnSc2	on
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
Parlament	parlament	k1gInSc1	parlament
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc7d1	horní
komorou	komora	k1gFnSc7	komora
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
dolní	dolní	k2eAgFnSc1d1	dolní
komorou	komora	k1gFnSc7	komora
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
zúčastnily	zúčastnit	k5eAaPmAgFnP	zúčastnit
dvě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
Lidová	lidový	k2eAgFnSc1d1	lidová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
a	a	k8xC	a
Druk	druk	k1gInSc1	druk
Phuensum	Phuensum	k1gInSc1	Phuensum
Tshogpa	Tshogpa	k1gFnSc1	Tshogpa
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Druk	druk	k1gInSc1	druk
Phuensum	Phuensum	k1gInSc1	Phuensum
Tshogpa	Tshogpa	k1gFnSc1	Tshogpa
získala	získat	k5eAaPmAgFnS	získat
45	[number]	k4	45
z	z	k7c2	z
47	[number]	k4	47
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
naopak	naopak	k6eAd1	naopak
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
Lidová	lidový	k2eAgFnSc1d1	lidová
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
54,88	[number]	k4	54,88
%	%	kIx~	%
a	a	k8xC	a
32	[number]	k4	32
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
organizacích	organizace	k1gFnPc6	organizace
===	===	k?	===
</s>
</p>
<p>
<s>
AsDB	AsDB	k?	AsDB
<g/>
,	,	kIx,	,
BIMSTEC	BIMSTEC	kA	BIMSTEC
<g/>
,	,	kIx,	,
CP	CP	kA	CP
<g/>
,	,	kIx,	,
FAO	FAO	kA	FAO
<g/>
,	,	kIx,	,
G-	G-	k1gFnSc1	G-
<g/>
77	[number]	k4	77
<g/>
,	,	kIx,	,
IBRD	IBRD	kA	IBRD
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
,	,	kIx,	,
IDA	ido	k1gNnSc2	ido
<g/>
,	,	kIx,	,
IFAD	IFAD	kA	IFAD
<g/>
,	,	kIx,	,
IFC	IFC	kA	IFC
<g/>
,	,	kIx,	,
IMF	IMF	kA	IMF
<g/>
,	,	kIx,	,
Interpol	interpol	k1gInSc1	interpol
<g/>
,	,	kIx,	,
IOC	IOC	kA	IOC
<g/>
,	,	kIx,	,
IOM	IOM	kA	IOM
(	(	kIx(	(
<g/>
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISO	ISO	kA	ISO
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ITU	ITU	kA	ITU
<g/>
,	,	kIx,	,
NAM	NAM	kA	NAM
<g/>
,	,	kIx,	,
OPCW	OPCW	kA	OPCW
<g/>
,	,	kIx,	,
SAARC	SAARC	kA	SAARC
<g/>
,	,	kIx,	,
SACEP	SACEP	kA	SACEP
<g/>
,	,	kIx,	,
UN	UN	kA	UN
<g/>
,	,	kIx,	,
UNCTAD	UNCTAD	kA	UNCTAD
<g/>
,	,	kIx,	,
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
UNIDO	UNIDO	kA	UNIDO
<g/>
,	,	kIx,	,
UPU	UPU	kA	UPU
<g/>
,	,	kIx,	,
WCO	WCO	kA	WCO
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
WIPO	WIPO	kA	WIPO
<g/>
,	,	kIx,	,
WMO	WMO	kA	WMO
<g/>
,	,	kIx,	,
WToO	WToO	k1gFnSc1	WToO
<g/>
,	,	kIx,	,
WTO	WTO	kA	WTO
(	(	kIx(	(
<g/>
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
měl	mít	k5eAaImAgInS	mít
Bhútán	Bhútán	k1gInSc1	Bhútán
634	[number]	k4	634
982	[number]	k4	982
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
742	[number]	k4	742
737	[number]	k4	737
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
etnickými	etnický	k2eAgFnPc7d1	etnická
skupinami	skupina	k1gFnPc7	skupina
jsou	být	k5eAaImIp3nP	být
Ngalopové	Ngalop	k1gMnPc1	Ngalop
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
Bhútánu	Bhútán	k1gInSc6	Bhútán
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
národním	národní	k2eAgInSc7d1	národní
jazykem	jazyk	k1gInSc7	jazyk
dzongkä	dzongkä	k?	dzongkä
<g/>
,	,	kIx,	,
Šarlopové	Šarlop	k1gMnPc1	Šarlop
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Bhútánu	Bhútán	k1gInSc6	Bhútán
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
jazykem	jazyk	k1gInSc7	jazyk
tšangla	tšanglo	k1gNnSc2	tšanglo
<g/>
,	,	kIx,	,
a	a	k8xC	a
Lhotšampové	Lhotšamp	k1gMnPc1	Lhotšamp
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Bhútánu	Bhútán	k1gInSc2	Bhútán
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
nepálsky	nepálsky	k6eAd1	nepálsky
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
různé	různý	k2eAgInPc4d1	různý
menší	malý	k2eAgInPc4d2	menší
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
jazyky	jazyk	k1gInPc4	jazyk
Bhútánu	Bhútán	k1gInSc2	Bhútán
kromě	kromě	k7c2	kromě
nepálštiny	nepálština	k1gFnSc2	nepálština
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
tibetobarmské	tibetobarmský	k2eAgFnSc2d1	tibetobarmský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lhotšampové	Lhotšampová	k1gFnPc1	Lhotšampová
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
hinduisté	hinduista	k1gMnPc1	hinduista
indického	indický	k2eAgMnSc2d1	indický
a	a	k8xC	a
nepálského	nepálský	k2eAgInSc2d1	nepálský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
zbytku	zbytek	k1gInSc3	zbytek
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
mongoloidního	mongoloidní	k2eAgInSc2d1	mongoloidní
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
teplejší	teplý	k2eAgNnSc4d2	teplejší
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
jiné	jiný	k2eAgInPc4d1	jiný
zvyky	zvyk	k1gInPc4	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
bhútánskou	bhútánský	k2eAgFnSc4d1	bhútánská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
majoritní	majoritní	k2eAgFnSc7d1	majoritní
buddhistickou	buddhistický	k2eAgFnSc7d1	buddhistická
společností	společnost	k1gFnSc7	společnost
diskriminováni	diskriminován	k2eAgMnPc1d1	diskriminován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
Lhotšampů	Lhotšamp	k1gMnPc2	Lhotšamp
vyhnáno	vyhnat	k5eAaPmNgNnS	vyhnat
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	on	k3xPp3gFnPc4	on
vláda	vláda	k1gFnSc1	vláda
obvinila	obvinit	k5eAaPmAgFnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
ilegální	ilegální	k2eAgMnSc1d1	ilegální
imigranti	imigrant	k1gMnPc1	imigrant
a	a	k8xC	a
zabavila	zabavit	k5eAaPmAgFnS	zabavit
jim	on	k3xPp3gMnPc3	on
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
tisíce	tisíc	k4xCgInPc1	tisíc
jich	on	k3xPp3gFnPc2	on
později	pozdě	k6eAd2	pozdě
emigrovaly	emigrovat	k5eAaBmAgFnP	emigrovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
diskriminace	diskriminace	k1gFnSc2	diskriminace
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
jich	on	k3xPp3gMnPc2	on
dodnes	dodnes	k6eAd1	dodnes
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
uprchlických	uprchlický	k2eAgInPc6d1	uprchlický
táborech	tábor	k1gInPc6	tábor
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
vláda	vláda	k1gFnSc1	vláda
Bhútánu	Bhútán	k1gInSc2	Bhútán
nedovolila	dovolit	k5eNaPmAgFnS	dovolit
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
byli	být	k5eAaImAgMnP	být
přesídleni	přesídlet	k5eAaPmNgMnP	přesídlet
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Státní	státní	k2eAgInPc1d1	státní
svátky	svátek	k1gInPc1	svátek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Bhútán	Bhútán	k1gInSc1	Bhútán
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
20	[number]	k4	20
distriktů	distrikt	k1gInPc2	distrikt
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bumthang	Bumthang	k1gMnSc1	Bumthang
<g/>
,	,	kIx,	,
Chhukha	Chhukha	k1gMnSc1	Chhukha
<g/>
,	,	kIx,	,
Chirang	Chirang	k1gMnSc1	Chirang
<g/>
,	,	kIx,	,
Dagana	Dagana	k1gFnSc1	Dagana
<g/>
,	,	kIx,	,
Geylegphug	Geylegphug	k1gInSc1	Geylegphug
<g/>
,	,	kIx,	,
Ha	ha	kA	ha
<g/>
,	,	kIx,	,
Lhuntshi	Lhuntshi	k1gNnSc1	Lhuntshi
<g/>
,	,	kIx,	,
Mongar	Mongar	k1gInSc1	Mongar
<g/>
,	,	kIx,	,
Paro	para	k1gFnSc5	para
<g/>
,	,	kIx,	,
Pemagatsel	Pemagatsel	k1gMnSc1	Pemagatsel
<g/>
,	,	kIx,	,
Punakha	Punakha	k1gMnSc1	Punakha
<g/>
,	,	kIx,	,
Samchi	Samch	k1gMnPc1	Samch
<g/>
,	,	kIx,	,
Samdrup	Samdrup	k1gMnSc1	Samdrup
Jongkhar	Jongkhar	k1gMnSc1	Jongkhar
<g/>
,	,	kIx,	,
Shemgang	Shemgang	k1gMnSc1	Shemgang
<g/>
,	,	kIx,	,
Tashigang	Tashigang	k1gMnSc1	Tashigang
<g/>
,	,	kIx,	,
Thimphu	Thimph	k1gInSc2	Thimph
<g/>
,	,	kIx,	,
Tongsa	Tongsa	k1gFnSc1	Tongsa
<g/>
,	,	kIx,	,
Wangdi	Wangd	k1gMnPc1	Wangd
Phodrang	Phodranga	k1gFnPc2	Phodranga
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bhútánské	bhútánský	k2eAgFnSc6d1	bhútánská
ústavě	ústava	k1gFnSc6	ústava
je	být	k5eAaImIp3nS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lesy	les	k1gInPc1	les
musí	muset	k5eAaImIp3nP	muset
zabírat	zabírat	k5eAaImF	zabírat
minimálně	minimálně	k6eAd1	minimálně
60	[number]	k4	60
%	%	kIx~	%
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nebylo	být	k5eNaImAgNnS	být
překvapením	překvapení	k1gNnSc7	překvapení
vysázení	vysázení	k1gNnSc2	vysázení
přesně	přesně	k6eAd1	přesně
108	[number]	k4	108
000	[number]	k4	000
stromků	stromek	k1gInPc2	stromek
jako	jako	k8xC	jako
připomínka	připomínka	k1gFnSc1	připomínka
narození	narození	k1gNnSc2	narození
prince	princ	k1gMnSc2	princ
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
Bhútánci	Bhútánek	k1gMnPc1	Bhútánek
zapsali	zapsat	k5eAaPmAgMnP	zapsat
do	do	k7c2	do
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vysadili	vysadit	k5eAaPmAgMnP	vysadit
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
50	[number]	k4	50
000	[number]	k4	000
stromků	stromek	k1gInPc2	stromek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Bhútánu	Bhútán	k1gInSc6	Bhútán
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hlavního	hlavní	k2eAgMnSc4d1	hlavní
ukazatele	ukazatel	k1gMnSc4	ukazatel
vyspělosti	vyspělost	k1gFnSc2	vyspělost
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
hrubé	hrubý	k2eAgNnSc4d1	hrubé
národní	národní	k2eAgNnSc4d1	národní
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
Bhútán	Bhútán	k1gInSc1	Bhútán
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
okraj	okraj	k1gInSc4	okraj
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otroctví	otroctví	k1gNnSc1	otroctví
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Televize	televize	k1gFnSc1	televize
a	a	k8xC	a
internet	internet	k1gInSc1	internet
byly	být	k5eAaImAgFnP	být
povoleny	povolit	k5eAaPmNgInP	povolit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FILIPSKÝ	FILIPSKÝ	kA	FILIPSKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
,	,	kIx,	,
Malediv	Maledivy	k1gFnPc2	Maledivy
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
Šrí	Šrí	k1gFnSc1	Šrí
Lanky	lanko	k1gNnPc7	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
647	[number]	k4	647
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VAVROUŠKOVÁ	VAVROUŠKOVÁ	kA	VAVROUŠKOVÁ
<g/>
,	,	kIx,	,
Stanislava	Stanislav	k1gMnSc2	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Bhútán	Bhútán	k1gInSc1	Bhútán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
421	[number]	k4	421
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bhútán	Bhútán	k1gInSc1	Bhútán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bhútán	Bhútán	k1gInSc1	Bhútán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
významných	významný	k2eAgFnPc2d1	významná
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Bhútánu	Bhútán	k1gInSc2	Bhútán
</s>
</p>
<p>
<s>
Bhútán	Bhútán	k1gInSc1	Bhútán
na	na	k7c4	na
buddhismus	buddhismus	k1gInSc4	buddhismus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
https://www.obhutanu.cz	[url]	k1gMnSc1	https://www.obhutanu.cz
</s>
</p>
<p>
<s>
Bhutan	Bhutan	k1gInSc1	Bhutan
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Bhútán	Bhútán	k1gInSc1	Bhútán
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2012-01-09	[number]	k4	2012-01-09
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Bhutan	Bhutan	k1gInSc1	Bhutan
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
South	South	k1gMnSc1	South
and	and	k?	and
Central	Central	k1gMnSc1	Central
Asian	Asian	k1gMnSc1	Asian
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Bhutan	Bhutan	k1gInSc1	Bhutan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2010-02-02	[number]	k4	2010-02-02
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Bhutan	Bhutan	k1gInSc1	Bhutan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-05-26	[number]	k4	2011-05-26
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KARAN	KARAN	kA	KARAN
<g/>
,	,	kIx,	,
Pradyumna	Pradyumen	k2eAgFnSc1d1	Pradyumen
P	P	kA	P
<g/>
;	;	kIx,	;
NORBU	NORBU	kA	NORBU
<g/>
,	,	kIx,	,
Dawa	Dawum	k1gNnSc2	Dawum
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Bhutan	Bhutan	k1gInSc1	Bhutan
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ústava	ústava	k1gFnSc1	ústava
Bhútánu	Bhútán	k1gInSc2	Bhútán
</s>
</p>
