<s>
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
je	být	k5eAaImIp3nS	být
komická	komický	k2eAgFnSc1d1	komická
opera	opera	k1gFnSc1	opera
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
dějstvích	dějství	k1gNnPc6	dějství
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
Karla	Karel	k1gMnSc2	Karel
Sabiny	Sabina	k1gMnSc2	Sabina
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
ji	on	k3xPp3gFnSc4	on
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
letech	let	k1gInPc6	let
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
premiéru	premiér	k1gMnSc3	premiér
měla	mít	k5eAaImAgFnS	mít
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1866	[number]	k4	1866
v	v	k7c6	v
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhou	zásluhou	k7c2	zásluhou
Emmy	Emma	k1gFnSc2	Emma
Destinnové	Destinnová	k1gFnSc2	Destinnová
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
opera	opera	k1gFnSc1	opera
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
opeře	opera	k1gFnSc6	opera
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Premiéru	premiéra	k1gFnSc4	premiéra
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1909	[number]	k4	1909
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
.	.	kIx.	.
</s>
<s>
Opera	opera	k1gFnSc1	opera
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Maxe	Max	k1gMnSc2	Max
Broda	Brod	k1gMnSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
začíná	začínat	k5eAaImIp3nS	začínat
pouť	pouť	k1gFnSc1	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
scházejí	scházet	k5eAaImIp3nP	scházet
a	a	k8xC	a
radují	radovat	k5eAaImIp3nP	radovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastává	nastávat	k5eAaImIp3nS	nastávat
sváteční	sváteční	k2eAgInSc4d1	sváteční
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Mařenka	Mařenka	k1gFnSc1	Mařenka
s	s	k7c7	s
Jeníkem	Jeník	k1gMnSc7	Jeník
však	však	k8xC	však
mají	mít	k5eAaImIp3nP	mít
jiné	jiný	k2eAgFnPc4d1	jiná
starosti	starost	k1gFnPc4	starost
–	–	k?	–
<g/>
(	(	kIx(	(
<g/>
právě	právě	k9	právě
dnes	dnes	k6eAd1	dnes
má	mít	k5eAaImIp3nS	mít
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
námluvy	námluva	k1gFnPc4	námluva
ženich	ženich	k1gMnSc1	ženich
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Mařence	Mařenka	k1gFnSc6	Mařenka
rodiče	rodič	k1gMnPc4	rodič
vybrali	vybrat	k5eAaPmAgMnP	vybrat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mařenka	Mařenka	k1gFnSc1	Mařenka
ujišťuje	ujišťovat	k5eAaImIp3nS	ujišťovat
Jeníka	Jeník	k1gMnSc4	Jeník
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
přesto	přesto	k6eAd1	přesto
nevzdá	vzdát	k5eNaPmIp3nS	vzdát
<g/>
,	,	kIx,	,
a	a	k8xC	a
stejné	stejný	k2eAgNnSc1d1	stejné
vyznání	vyznání	k1gNnSc1	vyznání
chce	chtít	k5eAaImIp3nS	chtít
také	také	k9	také
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
bůhvíproč	bůhvíproč	k6eAd1	bůhvíproč
váhá	váhat	k5eAaImIp3nS	váhat
a	a	k8xC	a
snad	snad	k9	snad
dokonce	dokonce	k9	dokonce
myslí	myslet	k5eAaImIp3nS	myslet
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Vyptává	vyptávat	k5eAaImIp3nS	vyptávat
se	se	k3xPyFc4	se
Jeníka	Jeník	k1gMnSc2	Jeník
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
minulost	minulost	k1gFnSc4	minulost
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgFnPc4	který
toho	ten	k3xDgNnSc2	ten
nikdo	nikdo	k3yNnSc1	nikdo
moc	moc	k6eAd1	moc
neví	vědět	k5eNaImIp3nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Jeník	Jeník	k1gMnSc1	Jeník
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
kdysi	kdysi	k6eAd1	kdysi
macecha	macecha	k1gFnSc1	macecha
vyhnala	vyhnat	k5eAaPmAgFnS	vyhnat
z	z	k7c2	z
domova	domov	k1gInSc2	domov
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
sám	sám	k3xTgMnSc1	sám
protloukat	protloukat	k5eAaImF	protloukat
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
s	s	k7c7	s
Mařenkou	Mařenka	k1gFnSc7	Mařenka
slibují	slibovat	k5eAaImIp3nP	slibovat
věrnost	věrnost	k1gFnSc4	věrnost
navzdory	navzdory	k7c3	navzdory
všemu	všecek	k3xTgNnSc3	všecek
zlému	zlé	k1gNnSc3	zlé
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
už	už	k9	už
se	se	k3xPyFc4	se
však	však	k9	však
blíží	blížit	k5eAaImIp3nS	blížit
dohazovač	dohazovač	k1gMnSc1	dohazovač
Kecal	kecal	k1gMnSc1	kecal
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mařenčinými	Mařenčin	k2eAgMnPc7d1	Mařenčin
rodiči	rodič	k1gMnPc7	rodič
<g/>
,	,	kIx,	,
Krušinovými	krušinový	k2eAgMnPc7d1	krušinový
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
jejich	jejich	k3xOp3gFnSc4	jejich
důvěru	důvěra	k1gFnSc4	důvěra
a	a	k8xC	a
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
syn	syn	k1gMnSc1	syn
bohatého	bohatý	k2eAgInSc2d1	bohatý
sedláka	sedlák	k1gInSc2	sedlák
Míchy	mícha	k1gFnSc2	mícha
ze	z	k7c2	z
sousední	sousední	k2eAgFnSc2d1	sousední
vsi	ves	k1gFnSc2	ves
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Mařenku	Mařenka	k1gFnSc4	Mařenka
ten	ten	k3xDgMnSc1	ten
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
ženich	ženich	k1gMnSc1	ženich
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
připomíná	připomínat	k5eAaImIp3nS	připomínat
Krušinovi	Krušin	k1gMnSc3	Krušin
jeho	jeho	k3xOp3gFnSc1	jeho
letitý	letitý	k2eAgInSc4d1	letitý
dluh	dluh	k1gInSc4	dluh
vůči	vůči	k7c3	vůči
Míchovi	Mích	k1gMnSc3	Mích
a	a	k8xC	a
závazek	závazek	k1gInSc4	závazek
<g/>
,	,	kIx,	,
že	že	k8xS	že
dá	dát	k5eAaPmIp3nS	dát
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
jeho	jeho	k3xOp3gMnSc3	jeho
synovi	syn	k1gMnSc3	syn
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Krušina	krušina	k1gFnSc1	krušina
si	se	k3xPyFc3	se
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mícha	mícha	k1gFnSc1	mícha
měl	mít	k5eAaImAgMnS	mít
syny	syn	k1gMnPc4	syn
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Kecal	kecal	k1gMnSc1	kecal
ho	on	k3xPp3gNnSc4	on
ujišťuje	ujišťovat	k5eAaImIp3nS	ujišťovat
že	že	k8xS	že
ten	ten	k3xDgMnSc1	ten
první	první	k4xOgMnSc1	první
se	se	k3xPyFc4	se
nevydařil	vydařit	k5eNaPmAgInS	vydařit
a	a	k8xC	a
ani	ani	k8xC	ani
doma	doma	k6eAd1	doma
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ženichem	ženich	k1gMnSc7	ženich
že	že	k8xS	že
rozhodně	rozhodně	k6eAd1	rozhodně
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
Vašek	Vašek	k1gMnSc1	Vašek
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
Kecal	kecal	k1gMnSc1	kecal
Krušinovým	krušinový	k2eAgMnPc3d1	krušinový
vylíčí	vylíčit	k5eAaPmIp3nS	vylíčit
jako	jako	k8xC	jako
dokonalého	dokonalý	k2eAgMnSc4d1	dokonalý
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
vzor	vzor	k1gInSc1	vzor
všech	všecek	k3xTgFnPc2	všecek
ctností	ctnost	k1gFnPc2	ctnost
<g/>
.	.	kIx.	.
</s>
<s>
Přemoženi	přemoct	k5eAaPmNgMnP	přemoct
Kecalovou	kecalův	k2eAgFnSc7d1	kecalův
výřečností	výřečnost	k1gFnSc7	výřečnost
Krušinovi	Krušinův	k2eAgMnPc1d1	Krušinův
konečně	konečně	k6eAd1	konečně
souhlasí	souhlasit	k5eAaImIp3nP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
tak	tak	k9	tak
ovšem	ovšem	k9	ovšem
Mařenka	Mařenka	k1gFnSc1	Mařenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vmísí	vmísit	k5eAaPmIp3nS	vmísit
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
rozhovoru	rozhovor	k1gInSc2	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
úmluvy	úmluva	k1gFnSc2	úmluva
rodičů	rodič	k1gMnPc2	rodič
s	s	k7c7	s
Kecalem	kecal	k1gMnSc7	kecal
si	se	k3xPyFc3	se
nic	nic	k3yNnSc1	nic
nedělá	dělat	k5eNaImIp3nS	dělat
a	a	k8xC	a
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc4	svůj
milého	milý	k1gMnSc4	milý
a	a	k8xC	a
toho	ten	k3xDgMnSc4	ten
se	se	k3xPyFc4	se
nevzdá	vzdát	k5eNaPmIp3nS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nepomůže	pomoct	k5eNaPmIp3nS	pomoct
chlácholení	chlácholení	k1gNnSc4	chlácholení
ani	ani	k8xC	ani
výhrůžky	výhrůžka	k1gFnPc4	výhrůžka
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
Kecal	kecal	k1gMnSc1	kecal
věc	věc	k1gFnSc4	věc
domluvit	domluvit	k5eAaPmF	domluvit
s	s	k7c7	s
Jeníkem	Jeník	k1gMnSc7	Jeník
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Vesnická	vesnický	k2eAgFnSc1d1	vesnická
slavnost	slavnost	k1gFnSc1	slavnost
je	být	k5eAaImIp3nS	být
mezitím	mezitím	k6eAd1	mezitím
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
proudu	proud	k1gInSc6	proud
<g/>
,	,	kIx,	,
tančí	tančit	k5eAaImIp3nS	tančit
se	se	k3xPyFc4	se
veselá	veselý	k2eAgFnSc1d1	veselá
polka	polka	k1gFnSc1	polka
<g/>
.	.	kIx.	.
</s>
<s>
Zábava	zábava	k1gFnSc1	zábava
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
u	u	k7c2	u
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Pijáci	piják	k1gMnPc1	piják
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
nemohou	moct	k5eNaImIp3nP	moct
vynachválit	vynachválit	k5eAaPmF	vynachválit
<g/>
,	,	kIx,	,
Jeník	Jeník	k1gMnSc1	Jeník
však	však	k9	však
nade	nad	k7c4	nad
všechno	všechen	k3xTgNnSc4	všechen
řadí	řadit	k5eAaImIp3nS	řadit
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Kecal	kecal	k1gMnSc1	kecal
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
o	o	k7c6	o
všemocných	všemocný	k2eAgInPc6d1	všemocný
penězích	peníze	k1gInPc6	peníze
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
Jeníka	Jeník	k1gMnSc2	Jeník
nalákat	nalákat	k5eAaPmF	nalákat
<g/>
.	.	kIx.	.
</s>
<s>
Kecal	kecat	k5eAaImAgMnS	kecat
si	se	k3xPyFc3	se
Jeníka	Jeník	k1gMnSc4	Jeník
odvádí	odvádět	k5eAaImIp3nS	odvádět
stranou	stranou	k6eAd1	stranou
a	a	k8xC	a
pijáci	piják	k1gMnPc1	piják
se	se	k3xPyFc4	se
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vsi	ves	k1gFnSc2	ves
přichází	přicházet	k5eAaImIp3nS	přicházet
Vašek	Vašek	k1gMnSc1	Vašek
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
onen	onen	k3xDgInSc1	onen
"	"	kIx"	"
<g/>
vážený	vážený	k2eAgInSc1d1	vážený
a	a	k8xC	a
vzácný	vzácný	k2eAgMnSc1d1	vzácný
ženich	ženich	k1gMnSc1	ženich
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
Kecalových	kecalův	k2eAgNnPc2d1	kecalův
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nesmělý	smělý	k2eNgMnSc1d1	nesmělý
<g/>
,	,	kIx,	,
dětsky	dětsky	k6eAd1	dětsky
naivní	naivní	k2eAgMnSc1d1	naivní
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
koktá	koktat	k5eAaImIp3nS	koktat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadšení	nadšení	k1gNnSc6	nadšení
i	i	k8xC	i
rozpacích	rozpak	k1gInPc6	rozpak
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
rodiče	rodič	k1gMnPc1	rodič
chystají	chystat	k5eAaImIp3nP	chystat
svatbu	svatba	k1gFnSc4	svatba
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ho	on	k3xPp3gMnSc4	on
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlédne	vyhlédnout	k5eAaPmIp3nS	vyhlédnout
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
Mařenka	Mařenka	k1gFnSc1	Mařenka
–	–	k?	–
kterou	který	k3yQgFnSc7	který
Vašek	Vašek	k1gMnSc1	Vašek
ještě	ještě	k9	ještě
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
neviděl	vidět	k5eNaImAgMnS	vidět
–	–	k?	–
a	a	k8xC	a
namluví	namluvit	k5eAaBmIp3nS	namluvit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Krušinovic	Krušinovice	k1gFnPc2	Krušinovice
dcerou	dcera	k1gFnSc7	dcera
ho	on	k3xPp3gMnSc4	on
čeká	čekat	k5eAaImIp3nS	čekat
peklo	peklo	k1gNnSc1	peklo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kterési	kterýsi	k3yIgNnSc1	kterýsi
děvče	děvče	k1gNnSc1	děvče
prý	prý	k9	prý
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
touží	toužit	k5eAaImIp3nP	toužit
<g/>
.	.	kIx.	.
</s>
<s>
Prostomyslný	prostomyslný	k2eAgMnSc1d1	prostomyslný
Vašek	Vašek	k1gMnSc1	Vašek
uvěří	uvěřit	k5eAaPmIp3nS	uvěřit
a	a	k8xC	a
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
ona	onen	k3xDgFnSc1	onen
dívka	dívka	k1gFnSc1	dívka
tak	tak	k6eAd1	tak
hezká	hezký	k2eAgFnSc1d1	hezká
jako	jako	k8xC	jako
Mařenka	Mařenka	k1gFnSc1	Mařenka
<g/>
,	,	kIx,	,
vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Kecal	kecal	k1gMnSc1	kecal
zatím	zatím	k6eAd1	zatím
přesvědčuje	přesvědčovat	k5eAaImIp3nS	přesvědčovat
Jeníka	Jeník	k1gMnSc4	Jeník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
Mařenky	Mařenka	k1gFnSc2	Mařenka
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
zisku	zisk	k1gInSc2	zisk
a	a	k8xC	a
jisté	jistý	k2eAgFnSc2d1	jistá
bohaté	bohatý	k2eAgFnSc2d1	bohatá
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
almaru	almara	k1gFnSc4	almara
<g/>
,	,	kIx,	,
dukáty	dukát	k1gInPc4	dukát
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
krávy	kráva	k1gFnPc1	kráva
<g/>
,	,	kIx,	,
selátko	selátko	k1gNnSc1	selátko
<g/>
,	,	kIx,	,
husy	husa	k1gFnPc1	husa
a	a	k8xC	a
kachny	kachna	k1gFnPc1	kachna
<g/>
,	,	kIx,	,
a	a	k8xC	a
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
Jeníkovi	Jeník	k1gMnSc3	Jeník
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
tu	tu	k6eAd1	tu
svou	svůj	k3xOyFgFnSc4	svůj
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
jen	jen	k6eAd1	jen
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
vidí	vidět	k5eAaImIp3nS	vidět
všechny	všechen	k3xTgFnPc4	všechen
krásy	krása	k1gFnPc4	krása
světa	svět	k1gInSc2	svět
a	a	k8xC	a
rozumný	rozumný	k2eAgInSc1d1	rozumný
že	že	k8xS	že
ustoupí	ustoupit	k5eAaPmIp3nS	ustoupit
před	před	k7c7	před
Mařenkou	Mařenka	k1gFnSc7	Mařenka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Jeník	Jeník	k1gMnSc1	Jeník
odmítá	odmítat	k5eAaImIp3nS	odmítat
všechny	všechen	k3xTgInPc4	všechen
návrhy	návrh	k1gInPc4	návrh
odstupného	odstupné	k1gNnSc2	odstupné
<g/>
,	,	kIx,	,
Kecal	kecal	k1gMnSc1	kecal
mu	on	k3xPp3gMnSc3	on
vyhrožuje	vyhrožovat	k5eAaImIp3nS	vyhrožovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
přespolního	přespolní	k2eAgMnSc4d1	přespolní
vyženou	vyhnat	k5eAaPmIp3nP	vyhnat
ze	z	k7c2	z
vsi	ves	k1gFnSc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
dohadování	dohadování	k1gNnSc2	dohadování
Jeník	Jeník	k1gMnSc1	Jeník
náhle	náhle	k6eAd1	náhle
obrátí	obrátit	k5eAaPmIp3nS	obrátit
a	a	k8xC	a
s	s	k7c7	s
odstoupením	odstoupení	k1gNnSc7	odstoupení
Mařenky	Mařenka	k1gFnSc2	Mařenka
za	za	k7c4	za
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
zlatých	zlatá	k1gFnPc2	zlatá
nakonec	nakonec	k6eAd1	nakonec
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
chce	chtít	k5eAaImIp3nS	chtít
s	s	k7c7	s
Kecalem	kecal	k1gMnSc7	kecal
sepsat	sepsat	k5eAaPmF	sepsat
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
výslovně	výslovně	k6eAd1	výslovně
napsáno	napsat	k5eAaPmNgNnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mařenku	Mařenka	k1gFnSc4	Mařenka
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
pouze	pouze	k6eAd1	pouze
syn	syn	k1gMnSc1	syn
sedláka	sedlák	k1gMnSc2	sedlák
Míchy	mícha	k1gFnSc2	mícha
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
zároveň	zároveň	k6eAd1	zároveň
smaže	smazat	k5eAaPmIp3nS	smazat
dluh	dluh	k1gInSc4	dluh
Krušinových	krušinový	k2eAgMnPc2d1	krušinový
<g/>
.	.	kIx.	.
</s>
<s>
Kecal	kecal	k1gMnSc1	kecal
odchází	odcházet	k5eAaImIp3nS	odcházet
připravit	připravit	k5eAaPmF	připravit
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
Jeník	Jeník	k1gMnSc1	Jeník
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
zpovzdálí	zpovzdálí	k6eAd1	zpovzdálí
vysmívá	vysmívat	k5eAaImIp3nS	vysmívat
<g/>
.	.	kIx.	.
</s>
<s>
Mařenky	Mařenka	k1gFnPc1	Mařenka
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
přece	přece	k9	přece
nikdy	nikdy	k6eAd1	nikdy
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nevzdal	vzdát	k5eNaPmAgMnS	vzdát
<g/>
!	!	kIx.	!
</s>
<s>
Smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Kecalem	kecal	k1gMnSc7	kecal
nicméně	nicméně	k8xC	nicméně
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
svědky	svědek	k1gMnPc7	svědek
podepíše	podepsat	k5eAaPmIp3nS	podepsat
a	a	k8xC	a
ke	k	k7c3	k
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
pohoršení	pohoršení	k1gNnSc1	pohoršení
Mařenku	Mařenka	k1gFnSc4	Mařenka
za	za	k7c4	za
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
zlatých	zlatý	k2eAgInPc2d1	zlatý
prodá	prodat	k5eAaPmIp3nS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Popletený	popletený	k2eAgMnSc1d1	popletený
Vašek	Vašek	k1gMnSc1	Vašek
si	se	k3xPyFc3	se
zoufá	zoufat	k5eAaBmIp3nS	zoufat
–	–	k?	–
podle	podle	k7c2	podle
přání	přání	k1gNnPc2	přání
rodičů	rodič	k1gMnPc2	rodič
si	se	k3xPyFc3	se
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
vzít	vzít	k5eAaPmF	vzít
Mařenku	Mařenka	k1gFnSc4	Mařenka
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
přece	přece	k9	přece
má	mít	k5eAaImIp3nS	mít
usoužit	usoužit	k5eAaPmF	usoužit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
!	!	kIx.	!
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
chmur	chmura	k1gFnPc2	chmura
ho	on	k3xPp3gInSc4	on
vysvobodí	vysvobodit	k5eAaPmIp3nS	vysvobodit
kočovní	kočovní	k2eAgMnPc1d1	kočovní
komedianti	komediant	k1gMnPc1	komediant
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
právě	právě	k6eAd1	právě
přijeli	přijet	k5eAaPmAgMnP	přijet
do	do	k7c2	do
vsi	ves	k1gFnSc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
principál	principál	k1gInSc1	principál
svolává	svolávat	k5eAaImIp3nS	svolávat
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
ohlašuje	ohlašovat	k5eAaImIp3nS	ohlašovat
nevídanou	vídaný	k2eNgFnSc4d1	nevídaná
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
část	část	k1gFnSc4	část
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
hned	hned	k6eAd1	hned
předvede	předvést	k5eAaPmIp3nS	předvést
<g/>
.	.	kIx.	.
</s>
<s>
Vašek	Vašek	k1gMnSc1	Vašek
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
komediantů	komediant	k1gMnPc2	komediant
celý	celý	k2eAgInSc1d1	celý
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
se	se	k3xPyFc4	se
do	do	k7c2	do
řeči	řeč	k1gFnSc2	řeč
s	s	k7c7	s
tanečnicí	tanečnice	k1gFnSc7	tanečnice
Esmeraldou	Esmeralda	k1gFnSc7	Esmeralda
<g/>
.	.	kIx.	.
</s>
<s>
Principál	principál	k1gMnSc1	principál
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
dovídá	dovídat	k5eAaImIp3nS	dovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnPc1	jejich
hlavní	hlavní	k2eAgFnPc1d1	hlavní
atrakce	atrakce	k1gFnPc1	atrakce
<g/>
,	,	kIx,	,
kankán	kankán	k1gInSc1	kankán
s	s	k7c7	s
"	"	kIx"	"
<g/>
živým	živý	k2eAgMnSc7d1	živý
<g/>
"	"	kIx"	"
medvědem	medvěd	k1gMnSc7	medvěd
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
konat	konat	k5eAaImF	konat
–	–	k?	–
komediant	komediant	k1gMnSc1	komediant
Franta	Franta	k1gMnSc1	Franta
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
obléká	oblékat	k5eAaImIp3nS	oblékat
do	do	k7c2	do
medvědí	medvědí	k2eAgFnSc2d1	medvědí
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
opil	opít	k5eAaPmAgMnS	opít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neudrží	udržet	k5eNaPmIp3nS	udržet
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Principál	principál	k1gInSc1	principál
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
navléknout	navléknout	k5eAaPmF	navléknout
do	do	k7c2	do
kůže	kůže	k1gFnSc2	kůže
Vaška	Vašek	k1gMnSc2	Vašek
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Esmeraldou	Esmeralda	k1gFnSc7	Esmeralda
ho	on	k3xPp3gMnSc4	on
přemluví	přemluvit	k5eAaPmIp3nS	přemluvit
<g/>
.	.	kIx.	.
</s>
<s>
Vašek	Vašek	k1gMnSc1	Vašek
se	se	k3xPyFc4	se
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
Hátě	Háta	k1gFnSc3	Háta
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Mařenky	Mařenka	k1gFnPc1	Mařenka
bojí	bát	k5eAaImIp3nP	bát
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesvědčí	přesvědčit	k5eNaPmIp3nP	přesvědčit
ho	on	k3xPp3gMnSc4	on
ani	ani	k8xC	ani
Kecal	kecat	k5eAaImAgMnS	kecat
s	s	k7c7	s
Míchou	mícha	k1gFnSc7	mícha
–	–	k?	–
Vašek	Vašek	k1gMnSc1	Vašek
Mařenku	Mařenka	k1gFnSc4	Mařenka
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Kecal	kecal	k1gMnSc1	kecal
s	s	k7c7	s
Míchovými	Míchová	k1gFnPc7	Míchová
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vaška	Vašek	k1gMnSc4	Vašek
kdosi	kdosi	k3yInSc1	kdosi
navedl	navést	k5eAaPmAgInS	navést
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
obchod	obchod	k1gInSc1	obchod
je	být	k5eAaImIp3nS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
sám	sám	k3xTgMnSc1	sám
Vašek	Vašek	k1gMnSc1	Vašek
<g/>
,	,	kIx,	,
když	když	k8xS	když
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
Mařence	Mařenka	k1gFnSc3	Mařenka
prozradí	prozradit	k5eAaPmIp3nS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ho	on	k3xPp3gNnSc4	on
před	před	k7c7	před
"	"	kIx"	"
<g/>
Mařenkou	Mařenka	k1gFnSc7	Mařenka
<g/>
"	"	kIx"	"
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
a	a	k8xC	a
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k9	jen
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Mařenku	Mařenka	k1gFnSc4	Mařenka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
chce	chtít	k5eAaImIp3nS	chtít
ale	ale	k9	ale
zůstat	zůstat	k5eAaPmF	zůstat
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
a	a	k8xC	a
všechno	všechen	k3xTgNnSc1	všechen
si	se	k3xPyFc3	se
rozmyslet	rozmyslet	k5eAaPmF	rozmyslet
<g/>
.	.	kIx.	.
</s>
<s>
Nemůže	moct	k5eNaImIp3nS	moct
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
její	její	k3xOp3gMnSc1	její
milý	milý	k1gMnSc1	milý
mohl	moct	k5eAaImAgMnS	moct
zradit	zradit	k5eAaPmF	zradit
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
za	za	k7c7	za
Mařenkou	Mařenka	k1gFnSc7	Mařenka
Jeník	Jeník	k1gMnSc1	Jeník
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
jí	on	k3xPp3gFnSc3	on
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
prodejem	prodej	k1gInSc7	prodej
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Uražená	uražený	k2eAgFnSc1d1	uražená
Mařenka	Mařenka	k1gFnSc1	Mařenka
o	o	k7c6	o
vysvětlování	vysvětlování	k1gNnSc6	vysvětlování
nestojí	stát	k5eNaImIp3nS	stát
a	a	k8xC	a
nastává	nastávat	k5eAaImIp3nS	nastávat
hádka	hádka	k1gFnSc1	hádka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
vmísí	vmísit	k5eAaPmIp3nS	vmísit
Kecal	kecal	k1gMnSc1	kecal
<g/>
.	.	kIx.	.
</s>
<s>
Jeník	Jeník	k1gMnSc1	Jeník
mu	on	k3xPp3gMnSc3	on
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mařenku	Mařenka	k1gFnSc4	Mařenka
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
a	a	k8xC	a
Mařence	Mařenka	k1gFnSc3	Mařenka
pak	pak	k6eAd1	pak
líčí	líčit	k5eAaImIp3nS	líčit
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc4	jaký
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
"	"	kIx"	"
<g/>
Míchovým	Míchův	k2eAgMnSc7d1	Míchův
synem	syn	k1gMnSc7	syn
<g/>
"	"	kIx"	"
čeká	čekat	k5eAaImIp3nS	čekat
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
očekávají	očekávat	k5eAaImIp3nP	očekávat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
Mařenka	Mařenka	k1gFnSc1	Mařenka
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
překvapivě	překvapivě	k6eAd1	překvapivě
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
dobrovolně	dobrovolně	k6eAd1	dobrovolně
poslechne	poslechnout	k5eAaPmIp3nS	poslechnout
<g/>
.	.	kIx.	.
</s>
<s>
Sedlák	sedlák	k1gInSc1	sedlák
Mícha	mícha	k1gFnSc1	mícha
však	však	k9	však
v	v	k7c6	v
Jeníkovi	Jeník	k1gMnSc6	Jeník
pozná	poznat	k5eAaPmIp3nS	poznat
svého	svůj	k3xOyFgMnSc4	svůj
prvorozeného	prvorozený	k2eAgMnSc4d1	prvorozený
syna	syn	k1gMnSc4	syn
–	–	k?	–
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
mu	on	k3xPp3gMnSc3	on
přece	přece	k9	přece
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Míchovu	Míchův	k2eAgMnSc3d1	Míchův
synovi	syn	k1gMnSc3	syn
<g/>
"	"	kIx"	"
patří	patřit	k5eAaImIp3nS	patřit
Mařenka	Mařenka	k1gFnSc1	Mařenka
<g/>
.	.	kIx.	.
</s>
<s>
Oklamaný	oklamaný	k2eAgMnSc1d1	oklamaný
Kecal	kecal	k1gMnSc1	kecal
hanbou	hanba	k1gFnSc7	hanba
uteče	utéct	k5eAaPmIp3nS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Vtom	vtom	k6eAd1	vtom
nastane	nastat	k5eAaPmIp3nS	nastat
zděšení	zděšení	k1gNnSc3	zděšení
–	–	k?	–
komediantům	komediant	k1gMnPc3	komediant
prý	prý	k9	prý
utekl	utéct	k5eAaPmAgMnS	utéct
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
!	!	kIx.	!
</s>
<s>
Z	z	k7c2	z
medvědí	medvědí	k2eAgFnSc2d1	medvědí
kůže	kůže	k1gFnSc2	kůže
se	se	k3xPyFc4	se
však	však	k9	však
vyklube	vyklubat	k5eAaPmIp3nS	vyklubat
Vašek	Vašek	k1gMnSc1	Vašek
<g/>
,	,	kIx,	,
rozzlobená	rozzlobený	k2eAgFnSc1d1	rozzlobená
Háta	Háta	k1gFnSc1	Háta
ho	on	k3xPp3gNnSc4	on
žene	hnát	k5eAaImIp3nS	hnát
lidem	člověk	k1gMnPc3	člověk
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
Mícha	mícha	k1gFnSc1	mícha
se	se	k3xPyFc4	se
smíří	smířit	k5eAaPmIp3nS	smířit
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
radují	radovat	k5eAaImIp3nP	radovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jeník	Jeník	k1gMnSc1	Jeník
dostal	dostat	k5eAaPmAgMnS	dostat
svou	svůj	k3xOyFgFnSc4	svůj
Mařenku	Mařenka	k1gFnSc4	Mařenka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
švédského	švédský	k2eAgInSc2d1	švédský
Göteborgu	Göteborg	k1gInSc2	Göteborg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
původní	původní	k2eAgFnSc2d1	původní
české	český	k2eAgFnSc2d1	Česká
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
libreto	libreto	k1gNnSc1	libreto
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
roce	rok	k1gInSc6	rok
od	od	k7c2	od
všestranného	všestranný	k2eAgMnSc2d1	všestranný
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
novináře	novinář	k1gMnSc2	novinář
Karla	Karel	k1gMnSc2	Karel
Sabiny	Sabina	k1gMnSc2	Sabina
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
libretem	libreto	k1gNnSc7	libreto
bylo	být	k5eAaImAgNnS	být
historické	historický	k2eAgNnSc1d1	historické
drama	drama	k1gNnSc1	drama
Braniboři	Branibor	k1gMnPc1	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
nacionalistickou	nacionalistický	k2eAgFnSc7d1	nacionalistická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc7d1	sociální
tendencí	tendence	k1gFnSc7	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
libreto	libreto	k1gNnSc1	libreto
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
dramatické	dramatický	k2eAgFnSc2d1	dramatická
výstavby	výstavba	k1gFnSc2	výstavba
velmi	velmi	k6eAd1	velmi
nedokonalé	dokonalý	k2eNgFnSc2d1	nedokonalá
<g/>
,	,	kIx,	,
Smetanovi	Smetana	k1gMnSc3	Smetana
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
naplnit	naplnit	k5eAaPmF	naplnit
hutným	hutný	k2eAgMnPc3d1	hutný
<g/>
,	,	kIx,	,
patetickým	patetický	k2eAgMnPc3d1	patetický
a	a	k8xC	a
pěvecky	pěvecky	k6eAd1	pěvecky
nesmírně	smírně	k6eNd1	smírně
náročným	náročný	k2eAgInSc7d1	náročný
hudebním	hudební	k2eAgInSc7d1	hudební
výrazem	výraz	k1gInSc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
nedostatkům	nedostatek	k1gInPc3	nedostatek
však	však	k8xC	však
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
patří	patřit	k5eAaImIp3nS	patřit
formální	formální	k2eAgFnSc1d1	formální
nevyhraněnost	nevyhraněnost	k1gFnSc1	nevyhraněnost
(	(	kIx(	(
<g/>
střídání	střídání	k1gNnSc1	střídání
principů	princip	k1gInPc2	princip
prokomponované	prokomponovaný	k2eAgFnSc2d1	prokomponovaná
a	a	k8xC	a
číslové	číslový	k2eAgFnSc2d1	číslová
opery	opera	k1gFnSc2	opera
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepřirozená	přirozený	k2eNgFnSc1d1	nepřirozená
hudební	hudební	k2eAgFnSc1d1	hudební
deklamace	deklamace	k1gFnSc1	deklamace
českého	český	k2eAgInSc2d1	český
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c4	po
dokončení	dokončení	k1gNnSc4	dokončení
Braniborů	Branibor	k1gMnPc2	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
kompozici	kompozice	k1gFnSc4	kompozice
nové	nový	k2eAgFnSc2d1	nová
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
z	z	k7c2	z
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
přiměly	přimět	k5eAaPmAgFnP	přimět
výtky	výtka	k1gFnPc1	výtka
wagnerianismu	wagnerianismus	k1gInSc2	wagnerianismus
jeho	jeho	k3xOp3gFnSc2	jeho
předchozí	předchozí	k2eAgFnSc2d1	předchozí
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
psát	psát	k5eAaImF	psát
v	v	k7c6	v
lehčím	lehký	k2eAgInSc6d2	lehčí
"	"	kIx"	"
<g/>
národním	národní	k2eAgInSc6d1	národní
<g/>
"	"	kIx"	"
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
ovšem	ovšem	k9	ovšem
pojal	pojmout	k5eAaPmAgMnS	pojmout
úmysl	úmysl	k1gInSc4	úmysl
vytvořit	vytvořit	k5eAaPmF	vytvořit
národní	národní	k2eAgFnSc4d1	národní
komickou	komický	k2eAgFnSc4d1	komická
operu	opera	k1gFnSc4	opera
velice	velice	k6eAd1	velice
vážně	vážně	k6eAd1	vážně
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc1	libreto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
tentokrát	tentokrát	k6eAd1	tentokrát
napsal	napsat	k5eAaBmAgMnS	napsat
Karel	Karel	k1gMnSc1	Karel
Sabina	Sabina	k1gMnSc1	Sabina
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
zevrubně	zevrubně	k6eAd1	zevrubně
přepracovat	přepracovat	k5eAaPmF	přepracovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
postup	postup	k1gInSc1	postup
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
opera	opera	k1gFnSc1	opera
rodila	rodit	k5eAaImAgFnS	rodit
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
že	že	k8xS	že
Smetana	Smetana	k1gMnSc1	Smetana
pečlivě	pečlivě	k6eAd1	pečlivě
hledal	hledat	k5eAaImAgMnS	hledat
adekvátní	adekvátní	k2eAgInSc4d1	adekvátní
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc4	první
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
opery	opera	k1gFnSc2	opera
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
-	-	kIx~	-
netypicky	typicky	k6eNd1	typicky
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
dobových	dobový	k2eAgFnPc2d1	dobová
zvyklostí	zvyklost	k1gFnPc2	zvyklost
-	-	kIx~	-
předehra	předehra	k1gFnSc1	předehra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
úpravě	úprava	k1gFnSc6	úprava
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
ruce	ruka	k1gFnPc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Tužková	tužkový	k2eAgFnSc1d1	tužková
skica	skica	k1gFnSc1	skica
opery	opera	k1gFnSc2	opera
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k9	až
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Partituru	partitura	k1gFnSc4	partitura
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
dopsal	dopsat	k5eAaPmAgMnS	dopsat
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
původními	původní	k2eAgFnPc7d1	původní
verzemi	verze	k1gFnPc7	verze
některých	některý	k3yIgNnPc2	některý
čísel	číslo	k1gNnPc2	číslo
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
konečným	konečný	k2eAgNnSc7d1	konečné
zněním	znění	k1gNnSc7	znění
v	v	k7c6	v
partituře	partitura	k1gFnSc6	partitura
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
,	,	kIx,	,
svědčící	svědčící	k2eAgFnPc1d1	svědčící
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Smetana	Smetana	k1gMnSc1	Smetana
hledal	hledat	k5eAaImAgMnS	hledat
jejich	jejich	k3xOp3gFnSc4	jejich
optimální	optimální	k2eAgFnSc4d1	optimální
podobu	podoba	k1gFnSc4	podoba
průběžně	průběžně	k6eAd1	průběžně
až	až	k9	až
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
.	.	kIx.	.
</s>
<s>
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
si	se	k3xPyFc3	se
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
premiéry	premiéra	k1gFnSc2	premiéra
vydobyla	vydobýt	k5eAaPmAgFnS	vydobýt
zasloužený	zasloužený	k2eAgInSc4d1	zasloužený
obdiv	obdiv	k1gInSc4	obdiv
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
světové	světový	k2eAgNnSc4d1	světové
renomé	renomé	k1gNnSc4	renomé
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
stálicí	stálice	k1gFnSc7	stálice
repertoáru	repertoár	k1gInSc2	repertoár
českých	český	k2eAgNnPc2d1	české
operních	operní	k2eAgNnPc2d1	operní
divadel	divadlo	k1gNnPc2	divadlo
a	a	k8xC	a
trvalou	trvalý	k2eAgFnSc7d1	trvalá
součástí	součást	k1gFnSc7	součást
zlatého	zlatý	k2eAgInSc2d1	zlatý
fondu	fond	k1gInSc2	fond
světové	světový	k2eAgFnSc2d1	světová
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
finální	finální	k2eAgFnSc1d1	finální
podobě	podoba	k1gFnSc3	podoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
již	již	k9	již
po	po	k7c4	po
desetiletí	desetiletí	k1gNnPc4	desetiletí
uznávána	uznáván	k2eAgNnPc4d1	uznáváno
<g/>
,	,	kIx,	,
však	však	k9	však
předcházel	předcházet	k5eAaImAgInS	předcházet
poměrně	poměrně	k6eAd1	poměrně
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
vývoj	vývoj	k1gInSc4	vývoj
iniciovaný	iniciovaný	k2eAgInSc4d1	iniciovaný
samotným	samotný	k2eAgMnSc7d1	samotný
autorem	autor	k1gMnSc7	autor
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
totiž	totiž	k9	totiž
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
aktů	akt	k1gInPc2	akt
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
dnes	dnes	k6eAd1	dnes
velmi	velmi	k6eAd1	velmi
známých	známý	k2eAgNnPc2d1	známé
a	a	k8xC	a
populárních	populární	k2eAgNnPc2d1	populární
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
výstupů	výstup	k1gInPc2	výstup
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
původně	původně	k6eAd1	původně
vůbec	vůbec	k9	vůbec
nefigurovala	figurovat	k5eNaImAgFnS	figurovat
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
psána	psát	k5eAaImNgFnS	psát
jako	jako	k8xS	jako
singspiel	singspiel	k1gInSc1	singspiel
-	-	kIx~	-
místo	místo	k1gNnSc1	místo
zpívaných	zpívaný	k2eAgInPc2d1	zpívaný
recitativů	recitativ	k1gInPc2	recitativ
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
prózu	próza	k1gFnSc4	próza
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
opery	opera	k1gFnSc2	opera
k	k	k7c3	k
definitivnímu	definitivní	k2eAgInSc3d1	definitivní
tvaru	tvar	k1gInSc3	tvar
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
přehledu	přehled	k1gInSc6	přehled
<g/>
:	:	kIx,	:
Dvě	dva	k4xCgNnPc4	dva
jednání	jednání	k1gNnPc4	jednání
<g/>
,	,	kIx,	,
mluvená	mluvený	k2eAgFnSc1d1	mluvená
próza	próza	k1gFnSc1	próza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
1	[number]	k4	1
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc6	jednání
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
scéně	scéna	k1gFnSc6	scéna
Krušinových	krušinový	k2eAgInPc2d1	krušinový
s	s	k7c7	s
Mařenkou	Mařenka	k1gFnSc7	Mařenka
a	a	k8xC	a
Kecalem	kecal	k1gMnSc7	kecal
zařazena	zařadit	k5eAaPmNgFnS	zařadit
Vaškova	Vaškův	k2eAgFnSc1d1	Vaškova
árie	árie	k1gFnSc1	árie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
próza	próza	k1gFnSc1	próza
a	a	k8xC	a
duet	duet	k1gInSc1	duet
Mařenky	Mařenka	k1gFnSc2	Mařenka
a	a	k8xC	a
Vaška	Vašek	k1gMnSc2	Vašek
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
scéna	scéna	k1gFnSc1	scéna
Jeníka	Jeník	k1gMnSc2	Jeník
s	s	k7c7	s
Kecalem	kecal	k1gMnSc7	kecal
(	(	kIx(	(
<g/>
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
"	"	kIx"	"
<g/>
Nuže	nuže	k9	nuže
<g/>
,	,	kIx,	,
milý	milý	k1gMnSc5	milý
chasníku	chasník	k1gMnSc5	chasník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Znám	znát	k5eAaImIp1nS	znát
jednu	jeden	k4xCgFnSc4	jeden
dívku	dívka	k1gFnSc4	dívka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
próza	próza	k1gFnSc1	próza
"	"	kIx"	"
<g/>
Jak	jak	k6eAd1	jak
pravím	pravit	k5eAaBmIp1nS	pravit
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc1	jednání
bylo	být	k5eAaImAgNnS	být
stejné	stejný	k2eAgNnSc1d1	stejné
jako	jako	k8xC	jako
dnešní	dnešní	k2eAgNnSc1d1	dnešní
třetí	třetí	k4xOgNnSc4	třetí
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
skočné	skočná	k1gFnSc2	skočná
a	a	k8xC	a
bez	bez	k7c2	bez
árie	árie	k1gFnSc2	árie
Mařenky	Mařenka	k1gFnSc2	Mařenka
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
kupletem	kuplet	k1gInSc7	kuplet
Principála	principál	k1gMnSc2	principál
a	a	k8xC	a
Esmeraldy	Esmeralda	k1gFnSc2	Esmeralda
"	"	kIx"	"
<g/>
Ten	ten	k3xDgMnSc1	ten
staví	stavit	k5eAaPmIp3nS	stavit
se	s	k7c7	s
svatouškem	svatoušek	k1gMnSc7	svatoušek
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
po	po	k7c6	po
slovech	slovo	k1gNnPc6	slovo
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
prováděti	provádět	k5eAaImF	provádět
umí	umět	k5eAaImIp3nS	umět
jako	jako	k8xS	jako
my	my	k3xPp1nPc1	my
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1869	[number]	k4	1869
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
Smetanové	Smetanové	k2eAgFnSc6d1	Smetanové
úpravě	úprava	k1gFnSc6	úprava
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc1	jednání
bylo	být	k5eAaImAgNnS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
proměny	proměna	k1gFnPc4	proměna
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
končila	končit	k5eAaImAgFnS	končit
duetem	dueto	k1gNnSc7	dueto
Mařenky	Mařenka	k1gFnSc2	Mařenka
a	a	k8xC	a
Vaška	Vašek	k1gMnSc2	Vašek
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
začínala	začínat	k5eAaImAgFnS	začínat
nově	nově	k6eAd1	nově
připsaným	připsaný	k2eAgInSc7d1	připsaný
mužským	mužský	k2eAgInSc7d1	mužský
sborem	sbor	k1gInSc7	sbor
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
pivečko	pivečko	k1gNnSc1	pivečko
to	ten	k3xDgNnSc4	ten
věru	věra	k1gFnSc4	věra
je	být	k5eAaImIp3nS	být
nebeský	nebeský	k2eAgInSc1d1	nebeský
dar	dar	k1gInSc1	dar
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
následoval	následovat	k5eAaImAgInS	následovat
duet	duet	k1gInSc1	duet
Jeníka	Jeník	k1gMnSc2	Jeník
s	s	k7c7	s
Kecalem	kecal	k1gMnSc7	kecal
<g/>
,	,	kIx,	,
árie	árie	k1gFnSc1	árie
Jeníka	Jeník	k1gMnSc2	Jeník
a	a	k8xC	a
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc6	jednání
Smetana	Smetana	k1gMnSc1	Smetana
vložil	vložit	k5eAaPmAgMnS	vložit
polku	polka	k1gFnSc4	polka
a	a	k8xC	a
k	k	k7c3	k
recitativu	recitativ	k1gInSc3	recitativ
Mařenky	Mařenka	k1gFnSc2	Mařenka
"	"	kIx"	"
<g/>
Ó	Ó	kA	Ó
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
žal	žal	k1gInSc1	žal
<g/>
"	"	kIx"	"
připojil	připojit	k5eAaPmAgInS	připojit
novou	nový	k2eAgFnSc4d1	nová
árii	árie	k1gFnSc4	árie
"	"	kIx"	"
<g/>
Ten	ten	k3xDgInSc4	ten
lásky	láska	k1gFnSc2	láska
sen	sen	k1gInSc1	sen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1869	[number]	k4	1869
se	se	k3xPyFc4	se
opera	opera	k1gFnSc1	opera
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
autorské	autorský	k2eAgFnSc6d1	autorská
úpravě	úprava	k1gFnSc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgNnPc4	tři
jednání	jednání	k1gNnPc4	jednání
<g/>
,	,	kIx,	,
Polka	Polka	k1gFnSc1	Polka
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
na	na	k7c4	na
konec	konec	k1gInSc4	konec
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
jednání	jednání	k1gNnSc6	jednání
navazoval	navazovat	k5eAaImAgMnS	navazovat
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
nový	nový	k2eAgInSc1d1	nový
tanec	tanec	k1gInSc1	tanec
furiant	furiant	k1gInSc1	furiant
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc6	jednání
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
Vaškova	Vaškův	k2eAgFnSc1d1	Vaškova
první	první	k4xOgFnSc1	první
árie	árie	k1gFnSc1	árie
a	a	k8xC	a
scéna	scéna	k1gFnSc1	scéna
s	s	k7c7	s
Mařenkou	Mařenka	k1gFnSc7	Mařenka
a	a	k8xC	a
Vaška	Vašek	k1gMnSc4	Vašek
(	(	kIx(	(
<g/>
na	na	k7c4	na
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgNnSc4d1	známé
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
pijácké	pijácký	k2eAgFnSc6d1	pijácká
scéně	scéna	k1gFnSc6	scéna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
jednání	jednání	k1gNnSc6	jednání
Smetana	Smetana	k1gMnSc1	Smetana
zařadil	zařadit	k5eAaPmAgMnS	zařadit
novou	nový	k2eAgFnSc4d1	nová
Skočnou	skočná	k1gFnSc4	skočná
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
revize	revize	k1gFnSc1	revize
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
poprvé	poprvé	k6eAd1	poprvé
zazněla	zaznít	k5eAaPmAgFnS	zaznít
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
změnou	změna	k1gFnSc7	změna
bylo	být	k5eAaImAgNnS	být
nahrazení	nahrazení	k1gNnSc1	nahrazení
mluvené	mluvený	k2eAgFnSc2d1	mluvená
prózy	próza	k1gFnSc2	próza
zpívanými	zpívaný	k2eAgInPc7d1	zpívaný
recitativy	recitativ	k1gInPc7	recitativ
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgNnPc1d1	hudební
čísla	číslo	k1gNnPc1	číslo
byla	být	k5eAaImAgNnP	být
seřazena	seřadit	k5eAaPmNgNnP	seřadit
do	do	k7c2	do
dnes	dnes	k6eAd1	dnes
známého	známý	k2eAgNnSc2d1	známé
pořadí	pořadí	k1gNnSc2	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
komediantské	komediantský	k2eAgFnSc2d1	komediantská
scény	scéna	k1gFnSc2	scéna
definitivně	definitivně	k6eAd1	definitivně
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
kuplet	kuplet	k1gInSc4	kuplet
principála	principál	k1gMnSc2	principál
a	a	k8xC	a
Esmeraldy	Esmeralda	k1gFnSc2	Esmeralda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
při	při	k7c6	při
předchozích	předchozí	k2eAgNnPc6d1	předchozí
uvedeních	uvedení	k1gNnPc6	uvedení
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nevhodný	vhodný	k2eNgInSc4d1	nevhodný
<g/>
.	.	kIx.	.
</s>
<s>
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
–	–	k?	–
zkrácený	zkrácený	k2eAgInSc4d1	zkrácený
záznam	záznam	k1gInSc4	záznam
provedení	provedení	k1gNnSc2	provedení
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
přírodním	přírodní	k2eAgNnSc6d1	přírodní
jevišti	jeviště	k1gNnSc6	jeviště
v	v	k7c6	v
Šárce	Šárka	k1gFnSc6	Šárka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Max	Max	k1gMnSc1	Max
Urban	Urban	k1gMnSc1	Urban
Prodaná	Prodaná	k1gFnSc1	Prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
–	–	k?	–
krátký	krátký	k2eAgInSc1d1	krátký
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kmínek	kmínek	k1gInSc1	kmínek
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
–	–	k?	–
filmový	filmový	k2eAgInSc1d1	filmový
záznam	záznam	k1gInSc1	záznam
představení	představení	k1gNnSc2	představení
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
<g />
.	.	kIx.	.
</s>
<s>
Innemann	Innemann	k1gMnSc1	Innemann
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kvapil	Kvapil	k1gMnSc1	Kvapil
a	a	k8xC	a
Emil	Emil	k1gMnSc1	Emil
Pollert	Pollert	k1gMnSc1	Pollert
Prodaná	Prodaná	k1gFnSc1	Prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
–	–	k?	–
historický	historický	k2eAgInSc1d1	historický
unikát	unikát	k1gInSc1	unikát
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
televizní	televizní	k2eAgFnSc1d1	televizní
inscenace	inscenace	k1gFnSc1	inscenace
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Václav	Václav	k1gMnSc1	Václav
Kašlík	kašlík	k1gInSc1	kašlík
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
–	–	k?	–
televizní	televizní	k2eAgInSc4d1	televizní
záznam	záznam	k1gInSc4	záznam
představení	představení	k1gNnSc2	představení
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Viktor	Viktor	k1gMnSc1	Viktor
Kočí	Kočí	k1gMnSc1	Kočí
<g/>
,	,	kIx,	,
???	???	k?	???
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
–	–	k?	–
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
natočený	natočený	k2eAgInSc1d1	natočený
v	v	k7c6	v
česko-německé	českoěmecký	k2eAgFnSc6d1	česko-německá
koprodukci	koprodukce	k1gFnSc6	koprodukce
<g/>
,	,	kIx,	,
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
a	a	k8xC	a
německé	německý	k2eAgFnSc6d1	německá
verzi	verze	k1gFnSc6	verze
<g/>
,	,	kIx,	,
činoherce	činoherec	k1gMnSc4	činoherec
dabují	dabovat	k5eAaBmIp3nP	dabovat
operní	operní	k2eAgMnPc1d1	operní
pěvci	pěvec	k1gMnPc1	pěvec
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Václav	Václav	k1gMnSc1	Václav
Kašlík	kašlík	k1gInSc1	kašlík
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
–	–	k?	–
český	český	k2eAgInSc1d1	český
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Františka	František	k1gMnSc2	František
Filipa	Filip	k1gMnSc2	Filip
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
operní	operní	k2eAgMnPc1d1	operní
pěvci	pěvec	k1gMnPc1	pěvec
SMETANA	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
.	.	kIx.	.
</s>
<s>
Studijní	studijní	k2eAgNnSc1d1	studijní
vydání	vydání	k1gNnSc1	vydání
děl	dělo	k1gNnPc2	dělo
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
I.	I.	kA	I.
Prodaná	Prodaná	k1gFnSc1	Prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
(	(	kIx(	(
<g/>
kritická	kritický	k2eAgFnSc1d1	kritická
edice	edice	k1gFnSc1	edice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Společnost	společnost	k1gFnSc1	společnost
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
NEJEDLÝ	Nejedlý	k1gMnSc1	Nejedlý
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvohry	zpěvohra	k1gFnPc1	zpěvohra
Smetanovy	Smetanův	k2eAgFnPc1d1	Smetanova
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
PRAŽÁK	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovy	Smetanův	k2eAgFnPc4d1	Smetanova
zpěvohry	zpěvohra	k1gFnPc4	zpěvohra
I	i	k9	i
:	:	kIx,	:
Braniboři	Branibor	k1gMnPc1	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
s.	s.	k?	s.
s	s	k7c7	s
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
320	[number]	k4	320
s.	s.	k?	s.
OČADLÍK	OČADLÍK	kA	OČADLÍK
<g/>
,	,	kIx,	,
Mirko	Mirko	k1gMnSc1	Mirko
<g/>
.	.	kIx.	.
</s>
<s>
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
47	[number]	k4	47
s.	s.	k?	s.
BARTOŠ	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
genesi	genese	k1gFnSc3	genese
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Musikologie	Musikologie	k1gFnSc1	Musikologie
<g/>
.	.	kIx.	.
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgFnPc1wB	čís
<g/>
.	.	kIx.	.
iv	iv	k?	iv
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
JIRÁNEK	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
v	v	k7c6	v
Prodané	prodaný	k2eAgFnSc6d1	prodaná
nevěstě	nevěsta	k1gFnSc6	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
hudební	hudební	k2eAgFnSc2d1	hudební
deklamace	deklamace	k1gFnSc2	deklamace
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
7003	[number]	k4	7003
<g/>
.	.	kIx.	.
</s>
<s>
JAREŠ	JAREŠ	kA	JAREŠ
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Obrazová	obrazový	k2eAgFnSc1d1	obrazová
dokumentace	dokumentace	k1gFnSc1	dokumentace
nejstarších	starý	k2eAgFnPc2d3	nejstarší
inscenací	inscenace	k1gFnPc2	inscenace
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
195	[number]	k4	195
<g/>
-	-	kIx~	-
<g/>
198	[number]	k4	198
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
7003	[number]	k4	7003
<g/>
.	.	kIx.	.
</s>
<s>
STOFFELS	STOFFELS	kA	STOFFELS
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
verkaufte	verkaufte	k5eAaPmIp2nP	verkaufte
Braut	Braut	k2eAgInSc4d1	Braut
von	von	k1gInSc4	von
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
JIRÁNEK	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
operní	operní	k2eAgFnSc1d1	operní
tvorba	tvorba	k1gFnSc1	tvorba
I.	I.	kA	I.
Od	od	k7c2	od
Braniborů	Branibor	k1gMnPc2	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
k	k	k7c3	k
Libuši	Libuše	k1gFnSc3	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Editio	Editio	k6eAd1	Editio
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
VOJTĚCH	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dramaturgii	dramaturgie	k1gFnSc3	dramaturgie
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Opus	opus	k1gInSc1	opus
musicum	musicum	k1gInSc1	musicum
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
,	,	kIx,	,
s.	s.	k?	s.
195	[number]	k4	195
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
PANENKA	panenka	k1gFnSc1	panenka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
SOUČKOVÁ	Součková	k1gFnSc1	Součková
<g/>
,	,	kIx,	,
Taťána	Taťána	k1gFnSc1	Taťána
<g/>
.	.	kIx.	.
</s>
<s>
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
na	na	k7c6	na
jevištích	jeviště	k1gNnPc6	jeviště
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
a	a	k8xC	a
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
1866	[number]	k4	1866
–	–	k?	–
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Gallery	Galler	k1gInPc1	Galler
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
216	[number]	k4	216
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86010	[number]	k4	86010
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
TYRRELL	TYRRELL	kA	TYRRELL
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Czech	Czech	k1gMnSc1	Czech
Opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
34713	[number]	k4	34713
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
FRIPPIAT	FRIPPIAT	kA	FRIPPIAT
<g/>
,	,	kIx,	,
Marianne	Mariann	k1gMnSc5	Mariann
<g/>
;	;	kIx,	;
YON	YON	kA	YON
<g/>
,	,	kIx,	,
Jean-Claude	Jean-Claud	k1gMnSc5	Jean-Claud
<g/>
;	;	kIx,	;
POSPÍŠIL	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
;	;	kIx,	;
JAMEK	jamka	k1gFnPc2	jamka
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
MOJŽÍŠOVÁ	Mojžíšová	k1gFnSc1	Mojžíšová
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
<g/>
;	;	kIx,	;
CAO	CAO	kA	CAO
<g/>
,	,	kIx,	,
Hélè	Hélè	k1gFnSc1	Hélè
<g/>
;	;	kIx,	;
VAN	van	k1gInSc1	van
MOERE	MOERE	kA	MOERE
<g/>
,	,	kIx,	,
Didier	Didier	k1gInSc1	Didier
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
Fiancée	Fiancé	k1gInSc2	Fiancé
vendue	vendu	k1gFnSc2	vendu
<g/>
.	.	kIx.	.
</s>
<s>
Paris	Paris	k1gMnSc1	Paris
:	:	kIx,	:
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Avant-Scè	Avant-Scè	k1gMnSc1	Avant-Scè
Opéra	Opéra	k1gMnSc1	Opéra
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
120	[number]	k4	120
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
84385	[number]	k4	84385
<g/>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc4	dílo
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
public	publicum	k1gNnPc2	publicum
domain	domain	k1gMnSc1	domain
hudebnin	hudebnina	k1gFnPc2	hudebnina
IMSLP	IMSLP	kA	IMSLP
(	(	kIx(	(
<g/>
Petrucci	Petrucce	k1gFnSc4	Petrucce
Music	Musice	k1gFnPc2	Musice
Library	Librara	k1gFnSc2	Librara
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
partitura	partitura	k1gFnSc1	partitura
a	a	k8xC	a
klavírní	klavírní	k2eAgInSc1d1	klavírní
výtah	výtah	k1gInSc1	výtah
<g/>
)	)	kIx)	)
Téma	téma	k1gNnSc1	téma
Prodaná	Prodaná	k1gFnSc1	Prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
