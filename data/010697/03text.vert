<p>
<s>
Relační	relační	k2eAgFnSc1d1	relační
vrstva	vrstva	k1gFnSc1	vrstva
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
vrstva	vrstva	k1gFnSc1	vrstva
modelu	model	k1gInSc2	model
vrstvové	vrstvový	k2eAgFnSc2d1	vrstvová
síťové	síťový	k2eAgFnSc2d1	síťová
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
OSI	OSI	kA	OSI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
session	session	k1gInSc1	session
layer	layra	k1gFnPc2	layra
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
vrstvy	vrstva	k1gFnSc2	vrstva
je	být	k5eAaImIp3nS	být
organizovat	organizovat	k5eAaBmF	organizovat
a	a	k8xC	a
synchronizovat	synchronizovat	k5eAaBmF	synchronizovat
dialog	dialog	k1gInSc4	dialog
mezi	mezi	k7c7	mezi
spolupracujícími	spolupracující	k2eAgFnPc7d1	spolupracující
relačními	relační	k2eAgFnPc7d1	relační
vrstvami	vrstva	k1gFnPc7	vrstva
obou	dva	k4xCgInPc2	dva
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
řídit	řídit	k5eAaImF	řídit
výměnu	výměna	k1gFnSc4	výměna
dat	datum	k1gNnPc2	datum
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
a	a	k8xC	a
ukončení	ukončení	k1gNnSc4	ukončení
relačního	relační	k2eAgNnSc2d1	relační
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
synchronizaci	synchronizace	k1gFnSc3	synchronizace
a	a	k8xC	a
obnovení	obnovení	k1gNnSc3	obnovení
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
oznamovaní	oznamovaný	k2eAgMnPc1d1	oznamovaný
výjimečných	výjimečný	k2eAgMnPc2d1	výjimečný
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
NetWare	NetWar	k1gMnSc5	NetWar
Core	Corus	k1gMnSc5	Corus
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
NCP	NCP	kA	NCP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Server	server	k1gInSc1	server
Message	Message	k1gFnSc1	Message
Block	Block	k1gMnSc1	Block
(	(	kIx(	(
<g/>
SMB	SMB	kA	SMB
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Network	network	k1gInSc1	network
File	File	k1gNnSc2	File
System	Syst	k1gInSc7	Syst
(	(	kIx(	(
<g/>
NFS	NFS	kA	NFS
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
AppleTalk	AppleTalk	k6eAd1	AppleTalk
Session	Session	k1gInSc1	Session
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
ASP	ASP	kA	ASP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
AppleTalk	AppleTalk	k1gMnSc1	AppleTalk
Data	datum	k1gNnSc2	datum
Stream	Stream	k1gInSc1	Stream
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
ADSP	ADSP	kA	ADSP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Data	datum	k1gNnPc1	datum
Link	Linko	k1gNnPc2	Linko
Control	Control	k1gInSc1	Control
(	(	kIx(	(
<g/>
DLC	DLC	kA	DLC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Named	Named	k1gMnSc1	Named
Pipes	Pipes	k1gMnSc1	Pipes
</s>
</p>
<p>
<s>
NBT	NBT	kA	NBT
</s>
</p>
<p>
<s>
NetBIOS	NetBIOS	k?	NetBIOS
</s>
</p>
<p>
<s>
NWLink	NWLink	k1gInSc1	NWLink
</s>
</p>
<p>
<s>
Printer	printer	k1gInSc1	printer
Access	Access	k1gInSc1	Access
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
PAP	PAP	kA	PAP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zone	Zone	k6eAd1	Zone
Information	Information	k1gInSc1	Information
Protocol	Protocol	k1gInSc1	Protocol
(	(	kIx(	(
<g/>
ZIP	zip	k1gInSc1	zip
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RPC	RPC	kA	RPC
</s>
</p>
<p>
<s>
SSL	SSL	kA	SSL
</s>
</p>
<p>
<s>
SPDY	SPDY	kA	SPDY
</s>
</p>
