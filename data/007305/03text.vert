<s>
Jehličnany	jehličnan	k1gInPc1	jehličnan
(	(	kIx(	(
<g/>
Pinopsida	Pinopsida	k1gFnSc1	Pinopsida
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
třída	třída	k1gFnSc1	třída
nahosemenných	nahosemenný	k2eAgFnPc2d1	nahosemenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
konifery	konifera	k1gFnPc1	konifera
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
v	v	k7c6	v
doslovném	doslovný	k2eAgInSc6d1	doslovný
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
šiškonoši	šiškonoš	k1gMnPc1	šiškonoš
<g/>
"	"	kIx"	"
–	–	k?	–
conus	conus	k1gMnSc1	conus
=	=	kIx~	=
šiška	šiška	k1gMnSc1	šiška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
překonání	překonání	k1gNnSc6	překonání
morfologického	morfologický	k2eAgInSc2d1	morfologický
způsobu	způsob	k1gInSc2	způsob
zatříďování	zatříďování	k1gNnSc2	zatříďování
flory	flora	k1gFnSc2	flora
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skupin	skupina	k1gFnPc2	skupina
řadily	řadit	k5eAaImAgFnP	řadit
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
taxony	taxon	k1gInPc1	taxon
podobné	podobný	k2eAgFnSc2d1	podobná
vzhledem	vzhled	k1gInSc7	vzhled
nebo	nebo	k8xC	nebo
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
nová	nový	k2eAgFnSc1d1	nová
klasifikace	klasifikace	k1gFnSc1	klasifikace
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
kladistickém	kladistický	k2eAgNnSc6d1	kladistický
<g/>
,	,	kIx,	,
monofyletickém	monofyletický	k2eAgNnSc6d1	monofyletický
hledisku	hledisko	k1gNnSc6	hledisko
vycházejícím	vycházející	k2eAgInSc7d1	vycházející
z	z	k7c2	z
genových	genový	k2eAgFnPc2d1	genová
sekvencí	sekvence	k1gFnPc2	sekvence
určitých	určitý	k2eAgInPc2d1	určitý
úseků	úsek	k1gInPc2	úsek
DNA	DNA	kA	DNA
nebo	nebo	k8xC	nebo
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
skupiny	skupina	k1gFnSc2	skupina
náleží	náležet	k5eAaImIp3nS	náležet
rostliny	rostlina	k1gFnPc4	rostlina
pocházející	pocházející	k2eAgFnPc4d1	pocházející
ze	z	k7c2	z
stejného	stejný	k2eAgMnSc2d1	stejný
dohledatelného	dohledatelný	k2eAgMnSc2d1	dohledatelný
praprapředka	praprapředek	k1gMnSc2	praprapředek
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
která	který	k3yQgFnSc1	který
rostlina	rostlina	k1gFnSc1	rostlina
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k8xC	i
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
od	od	k7c2	od
rozdělení	rozdělení	k1gNnSc2	rozdělení
pozměnila	pozměnit	k5eAaPmAgFnS	pozměnit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
moderní	moderní	k2eAgInSc1d1	moderní
způsob	způsob	k1gInSc1	způsob
zatřiďování	zatřiďování	k1gNnSc2	zatřiďování
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
krytosemenné	krytosemenný	k2eAgNnSc4d1	krytosemenné
rozpracován	rozpracován	k2eAgInSc4d1	rozpracován
v	v	k7c6	v
taxonomickém	taxonomický	k2eAgInSc6d1	taxonomický
systému	systém	k1gInSc6	systém
APG	APG	kA	APG
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
navázat	navázat	k5eAaPmF	navázat
systém	systém	k1gInSc4	systém
nahosemenných	nahosemenný	k2eAgFnPc2d1	nahosemenná
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
na	na	k7c6	na
APG	APG	kA	APG
však	však	k9	však
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nutnosti	nutnost	k1gFnSc3	nutnost
posunu	posun	k1gInSc2	posun
zavedených	zavedený	k2eAgInPc2d1	zavedený
klasifikačních	klasifikační	k2eAgInPc2d1	klasifikační
ranků	rank	k1gInPc2	rank
<g/>
:	:	kIx,	:
V	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
Chase	chasa	k1gFnSc3	chasa
a	a	k8xC	a
Reveala	Reveala	k1gFnSc1	Reveala
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
skupina	skupina	k1gFnSc1	skupina
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
embryofytům	embryofyt	k1gInPc3	embryofyt
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
jméno	jméno	k1gNnSc1	jméno
Equisetopsida	Equisetopsid	k1gMnSc2	Equisetopsid
<g/>
,	,	kIx,	,
a	a	k8xC	a
tradiční	tradiční	k2eAgNnPc1d1	tradiční
oddělení	oddělení	k1gNnPc1	oddělení
mají	mít	k5eAaImIp3nP	mít
úroveň	úroveň	k1gFnSc4	úroveň
podtříd	podtřída	k1gFnPc2	podtřída
<g/>
.	.	kIx.	.
</s>
<s>
Christenhusz	Christenhusz	k1gMnSc1	Christenhusz
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
rozpracovali	rozpracovat	k5eAaPmAgMnP	rozpracovat
a	a	k8xC	a
aktualizovali	aktualizovat	k5eAaBmAgMnP	aktualizovat
systém	systém	k1gInSc4	systém
recentních	recentní	k2eAgInPc2d1	recentní
nahosemenných	nahosemenný	k2eAgInPc2d1	nahosemenný
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
systémy	systém	k1gInPc1	systém
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
nahosemenné	nahosemenný	k2eAgFnPc4d1	nahosemenná
rostliny	rostlina	k1gFnPc4	rostlina
ustálené	ustálený	k2eAgFnPc4d1	ustálená
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejsou	být	k5eNaImIp3nP	být
dosud	dosud	k6eAd1	dosud
dostatečně	dostatečně	k6eAd1	dostatečně
vyjasněné	vyjasněný	k2eAgInPc4d1	vyjasněný
a	a	k8xC	a
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
fylogenetické	fylogenetický	k2eAgInPc4d1	fylogenetický
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
jehličnany	jehličnan	k1gInPc7	jehličnan
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
liánovcotvarých	liánovcotvarý	k2eAgFnPc2d1	liánovcotvarý
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
napojení	napojení	k1gNnSc4	napojení
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
skupiny	skupina	k1gFnPc4	skupina
(	(	kIx(	(
<g/>
cykasotvaré	cykasotvarý	k2eAgFnPc1d1	cykasotvarý
<g/>
,	,	kIx,	,
jinanotvaré	jinanotvarý	k2eAgFnPc1d1	jinanotvarý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
i	i	k9	i
klasifikace	klasifikace	k1gFnSc1	klasifikace
v	v	k7c6	v
učebnicích	učebnice	k1gFnPc6	učebnice
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
sekce	sekce	k1gFnSc2	sekce
odkazy	odkaz	k1gInPc1	odkaz
-	-	kIx~	-
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
učebnice	učebnice	k1gFnPc1	učebnice
z	z	k7c2	z
r.	r.	kA	r.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
stratigrafické	stratigrafický	k2eAgInPc4d1	stratigrafický
nálezy	nález	k1gInPc4	nález
nám	my	k3xPp1nPc3	my
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejpůvodnější	původní	k2eAgInPc1d3	nejpůvodnější
jehličnany	jehličnan	k1gInPc1	jehličnan
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
koncem	koncem	k7c2	koncem
geologického	geologický	k2eAgNnSc2d1	geologické
období	období	k1gNnSc2	období
karbonu	karbon	k1gInSc2	karbon
v	v	k7c6	v
prvohorách	prvohory	k1gFnPc6	prvohory
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
tvarová	tvarový	k2eAgFnSc1d1	tvarová
a	a	k8xC	a
druhová	druhový	k2eAgFnSc1d1	druhová
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
druhohorách	druhohory	k1gFnPc6	druhohory
v	v	k7c6	v
juře	jura	k1gFnSc6	jura
a	a	k8xC	a
vrcholu	vrchol	k1gInSc2	vrchol
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
křídě	křída	k1gFnSc6	křída
<g/>
.	.	kIx.	.
</s>
<s>
Evoluci	evoluce	k1gFnSc4	evoluce
vývoje	vývoj	k1gInSc2	vývoj
jehličnanů	jehličnan	k1gInPc2	jehličnan
lze	lze	k6eAd1	lze
sledovat	sledovat	k5eAaImF	sledovat
porovnáním	porovnání	k1gNnSc7	porovnání
nejstarších	starý	k2eAgFnPc2d3	nejstarší
primitivních	primitivní	k2eAgFnPc2d1	primitivní
rostlin	rostlina	k1gFnPc2	rostlina
řádů	řád	k1gInPc2	řád
cykasotvarých	cykasotvarý	k2eAgInPc2d1	cykasotvarý
a	a	k8xC	a
jinanotvarých	jinanotvarý	k2eAgInPc2d1	jinanotvarý
s	s	k7c7	s
mladšími	mladý	k2eAgMnPc7d2	mladší
borovicotvarými	borovicotvarý	k2eAgMnPc7d1	borovicotvarý
nebo	nebo	k8xC	nebo
s	s	k7c7	s
liánovcotvarými	liánovcotvarý	k2eAgInPc7d1	liánovcotvarý
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
blíží	blížit	k5eAaImIp3nS	blížit
v	v	k7c6	v
několika	několik	k4yIc6	několik
znacích	znak	k1gInPc6	znak
k	k	k7c3	k
rostlinám	rostlina	k1gFnPc3	rostlina
krytosemenným	krytosemenný	k2eAgFnPc3d1	krytosemenná
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnany	jehličnan	k1gInPc1	jehličnan
jsou	být	k5eAaImIp3nP	být
vývojově	vývojově	k6eAd1	vývojově
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgInPc1d2	starší
než	než	k8xS	než
listnaté	listnatý	k2eAgInPc1d1	listnatý
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
v	v	k7c6	v
prehistorii	prehistorie	k1gFnSc6	prehistorie
byly	být	k5eAaImAgFnP	být
dominantními	dominantní	k2eAgFnPc7d1	dominantní
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mají	mít	k5eAaImIp3nP	mít
borovicotvaré	borovicotvarý	k2eAgInPc1d1	borovicotvarý
dominantním	dominantní	k2eAgInSc7d1	dominantní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
a	a	k8xC	a
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
cykasotvaré	cykasotvarý	k2eAgFnPc1d1	cykasotvarý
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
všech	všecek	k3xTgInPc2	všecek
světadílů	světadíl	k1gInPc2	světadíl
<g/>
,	,	kIx,	,
jinanotvaré	jinanotvarý	k2eAgFnSc2d1	jinanotvarý
se	se	k3xPyFc4	se
štěstím	štěstí	k1gNnSc7	štěstí
přežily	přežít	k5eAaPmAgFnP	přežít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
druhu	druh	k1gInSc6	druh
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
liánovcotvaré	liánovcotvarý	k2eAgInPc1d1	liánovcotvarý
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
Starého	Starého	k2eAgInSc2d1	Starého
i	i	k8xC	i
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Třída	třída	k1gFnSc1	třída
jehličnany	jehličnan	k1gInPc1	jehličnan
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
stala	stát	k5eAaPmAgFnS	stát
velice	velice	k6eAd1	velice
různorodou	různorodý	k2eAgFnSc4d1	různorodá
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
ji	on	k3xPp3gFnSc4	on
tvoří	tvořit	k5eAaImIp3nP	tvořit
stálezelené	stálezelený	k2eAgNnSc1d1	stálezelené
neopadavé	opadavý	k2eNgInPc1d1	neopadavý
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
keře	keř	k1gInPc1	keř
nebo	nebo	k8xC	nebo
liány	liána	k1gFnPc1	liána
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
rostliny	rostlina	k1gFnPc1	rostlina
statnějšího	statný	k2eAgInSc2d2	statnější
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
choulostivé	choulostivý	k2eAgFnPc1d1	choulostivá
na	na	k7c4	na
krátkodobý	krátkodobý	k2eAgInSc4d1	krátkodobý
nedostatek	nedostatek	k1gInSc4	nedostatek
vláhy	vláha	k1gFnSc2	vláha
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
mají	mít	k5eAaImIp3nP	mít
jehlicovité	jehlicovitý	k2eAgInPc1d1	jehlicovitý
<g/>
,	,	kIx,	,
šupinovité	šupinovitý	k2eAgInPc1d1	šupinovitý
<g/>
,	,	kIx,	,
hladké	hladký	k2eAgInPc1d1	hladký
nebo	nebo	k8xC	nebo
zpeřené	zpeřený	k2eAgInPc1d1	zpeřený
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
nepřetržitě	přetržitě	k6eNd1	přetržitě
přirůstající	přirůstající	k2eAgNnSc1d1	přirůstající
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
listech	list	k1gInPc6	list
i	i	k8xC	i
kmenech	kmen	k1gInPc6	kmen
jsou	být	k5eAaImIp3nP	být
pryskyřičné	pryskyřičný	k2eAgFnPc1d1	pryskyřičná
kanálky	kanálka	k1gFnPc1	kanálka
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnaté	jehličnatý	k2eAgFnPc1d1	jehličnatá
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
jednodomé	jednodomý	k2eAgInPc1d1	jednodomý
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
rostlina	rostlina	k1gFnSc1	rostlina
má	mít	k5eAaImIp3nS	mít
samčí	samčí	k2eAgInPc4d1	samčí
i	i	k8xC	i
samičí	samičí	k2eAgInPc4d1	samičí
rozmnožovací	rozmnožovací	k2eAgInPc4d1	rozmnožovací
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
šištic	šištice	k1gFnPc2	šištice
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
rostliny	rostlina	k1gFnPc1	rostlina
nahosemenné	nahosemenný	k2eAgFnPc1d1	nahosemenná
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
květy	květ	k1gInPc4	květ
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
vajíčka	vajíčko	k1gNnPc1	vajíčko
nejsou	být	k5eNaImIp3nP	být
skryta	skryt	k2eAgMnSc4d1	skryt
v	v	k7c6	v
květech	květ	k1gInPc6	květ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
za	za	k7c7	za
šupinami	šupina	k1gFnPc7	šupina
samičích	samičí	k2eAgFnPc2d1	samičí
šištic	šištice	k1gFnPc2	šištice
<g/>
.	.	kIx.	.
</s>
<s>
Pyl	pyl	k1gInSc1	pyl
je	být	k5eAaImIp3nS	být
větrem	vítr	k1gInSc7	vítr
nebo	nebo	k8xC	nebo
hmyzem	hmyz	k1gInSc7	hmyz
přenesen	přenesen	k2eAgInSc1d1	přenesen
ze	z	k7c2	z
samčích	samčí	k2eAgFnPc2d1	samčí
šištic	šištice	k1gFnPc2	šištice
na	na	k7c4	na
vajíčka	vajíčko	k1gNnPc4	vajíčko
samičí	samičí	k2eAgFnSc2d1	samičí
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
následně	následně	k6eAd1	následně
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
splynutí	splynutí	k1gNnSc3	splynutí
samčí	samčí	k2eAgFnSc2d1	samčí
a	a	k8xC	a
samičí	samičí	k2eAgFnSc2d1	samičí
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oplodnění	oplodnění	k1gNnSc6	oplodnění
vznikají	vznikat	k5eAaImIp3nP	vznikat
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
chráněna	chránit	k5eAaImNgFnS	chránit
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
volně	volně	k6eAd1	volně
uložená	uložený	k2eAgFnSc1d1	uložená
v	v	k7c6	v
šiškách	šiška	k1gFnPc6	šiška
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
obaleny	obalen	k2eAgMnPc4d1	obalen
dužnatým	dužnatý	k2eAgInSc7d1	dužnatý
obalem	obal	k1gInSc7	obal
připomínajícím	připomínající	k2eAgInSc7d1	připomínající
peckovici	peckovice	k1gFnSc4	peckovice
<g/>
.	.	kIx.	.
</s>
<s>
Cykasotvaré	Cykasotvarý	k2eAgInPc1d1	Cykasotvarý
–	–	k?	–
11	[number]	k4	11
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
nízké	nízký	k2eAgInPc1d1	nízký
dvoudomé	dvoudomý	k2eAgInPc1d1	dvoudomý
stromy	strom	k1gInPc1	strom
s	s	k7c7	s
nevětveným	větvený	k2eNgInSc7d1	nevětvený
kmenem	kmen	k1gInSc7	kmen
a	a	k8xC	a
velkými	velký	k2eAgInPc7d1	velký
zpeřenými	zpeřený	k2eAgInPc7d1	zpeřený
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
až	až	k9	až
6	[number]	k4	6
m	m	kA	m
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
<g/>
,	,	kIx,	,
nahlučenými	nahlučený	k2eAgInPc7d1	nahlučený
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
samčí	samčí	k2eAgFnPc1d1	samčí
nebo	nebo	k8xC	nebo
samičí	samičí	k2eAgFnPc1d1	samičí
jednopohlavné	jednopohlavný	k2eAgFnPc1d1	jednopohlavný
šištice	šištice	k1gFnPc1	šištice
<g/>
.	.	kIx.	.
</s>
<s>
Zralá	zralý	k2eAgNnPc1d1	zralé
semena	semeno	k1gNnPc1	semeno
mají	mít	k5eAaImIp3nP	mít
vnější	vnější	k2eAgFnSc4d1	vnější
dužnatou	dužnatý	k2eAgFnSc4d1	dužnatá
vrstvu	vrstva	k1gFnSc4	vrstva
připomínající	připomínající	k2eAgInSc4d1	připomínající
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
"	"	kIx"	"
<g/>
plody	plod	k1gInPc4	plod
<g/>
"	"	kIx"	"
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
dřeň	dřeň	k1gFnSc1	dřeň
kmenů	kmen	k1gInPc2	kmen
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
však	však	k9	však
škodlivé	škodlivý	k2eAgInPc1d1	škodlivý
toxiny	toxin	k1gInPc1	toxin
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
upravují	upravovat	k5eAaImIp3nP	upravovat
varem	var	k1gInSc7	var
<g/>
,	,	kIx,	,
kvašením	kvašení	k1gNnSc7	kvašení
nebo	nebo	k8xC	nebo
vyluhováním	vyluhování	k1gNnSc7	vyluhování
<g/>
.	.	kIx.	.
</s>
<s>
Jinanotvaré	Jinanotvarý	k2eAgInPc1d1	Jinanotvarý
–	–	k?	–
1	[number]	k4	1
rod	rod	k1gInSc1	rod
<g/>
,	,	kIx,	,
dlouhověké	dlouhověký	k2eAgInPc1d1	dlouhověký
opadavé	opadavý	k2eAgInPc1d1	opadavý
dvoudomé	dvoudomý	k2eAgInPc1d1	dvoudomý
stromy	strom	k1gInPc1	strom
s	s	k7c7	s
kůlovitým	kůlovitý	k2eAgInSc7d1	kůlovitý
kořenem	kořen	k1gInSc7	kořen
<g/>
,	,	kIx,	,
snášejí	snášet	k5eAaImIp3nP	snášet
sucho	sucho	k1gNnSc1	sucho
i	i	k8xC	i
mráz	mráz	k1gInSc1	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Ploché	plochý	k2eAgInPc4d1	plochý
<g/>
,	,	kIx,	,
klínovité	klínovitý	k2eAgInPc4d1	klínovitý
listy	list	k1gInPc4	list
(	(	kIx(	(
<g/>
jehlice	jehlice	k1gFnPc4	jehlice
<g/>
)	)	kIx)	)
s	s	k7c7	s
vějířovitou	vějířovitý	k2eAgFnSc7d1	vějířovitá
žilnatinou	žilnatina	k1gFnSc7	žilnatina
<g/>
,	,	kIx,	,
<g/>
jsou	být	k5eAaImIp3nP	být
zářezem	zářez	k1gInSc7	zářez
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
laloky	lalok	k1gInPc4	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Pyl	pyl	k1gInSc1	pyl
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
samčích	samčí	k2eAgInPc6d1	samčí
stromech	strom	k1gInPc6	strom
v	v	k7c6	v
šišticích	šištice	k1gFnPc6	šištice
podobných	podobný	k2eAgFnPc6d1	podobná
jehnědám	jehněda	k1gFnPc3	jehněda
<g/>
,	,	kIx,	,
na	na	k7c6	na
samičích	samičí	k2eAgInPc6d1	samičí
vznikají	vznikat	k5eAaImIp3nP	vznikat
shluky	shluk	k1gInPc1	shluk
stopkatých	stopkatý	k2eAgInPc2d1	stopkatý
semeníků	semeník	k1gInPc2	semeník
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivé	zdánlivý	k2eAgInPc1d1	zdánlivý
plody	plod	k1gInPc1	plod
připomínají	připomínat	k5eAaImIp3nP	připomínat
zelené	zelený	k2eAgFnPc1d1	zelená
třešně	třešeň	k1gFnPc1	třešeň
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
dužnatým	dužnatý	k2eAgInSc7d1	dužnatý
obalem	obal	k1gInSc7	obal
vždy	vždy	k6eAd1	vždy
jedno	jeden	k4xCgNnSc4	jeden
semeno	semeno	k1gNnSc4	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Dužina	dužina	k1gFnSc1	dužina
je	být	k5eAaImIp3nS	být
hořká	hořký	k2eAgFnSc1d1	hořká
<g/>
,	,	kIx,	,
semena	semeno	k1gNnPc1	semeno
se	se	k3xPyFc4	se
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc4	semeno
a	a	k8xC	a
listy	list	k1gInPc4	list
jinanu	jinan	k1gInSc2	jinan
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
obsahu	obsah	k1gInSc3	obsah
flavonoidů	flavonoid	k1gInPc2	flavonoid
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
léčivo	léčivo	k1gNnSc4	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Borovicotvaré	Borovicotvarý	k2eAgInPc1d1	Borovicotvarý
–	–	k?	–
70	[number]	k4	70
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
stálezelené	stálezelený	k2eAgInPc1d1	stálezelený
jednodomé	jednodomý	k2eAgInPc1d1	jednodomý
stromy	strom	k1gInPc1	strom
nebo	nebo	k8xC	nebo
keře	keř	k1gInPc1	keř
s	s	k7c7	s
jehlicovými	jehlicův	k2eAgInPc7d1	jehlicův
nebo	nebo	k8xC	nebo
šupinovými	šupinový	k2eAgInPc7d1	šupinový
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgInPc4d1	samčí
přetvořené	přetvořený	k2eAgInPc4d1	přetvořený
listy	list	k1gInPc4	list
nesoucí	nesoucí	k2eAgInPc4d1	nesoucí
pylotvorné	pylotvorný	k2eAgInPc4d1	pylotvorný
váčky	váček	k1gInPc4	váček
(	(	kIx(	(
<g/>
mikrosporofyly	mikrosporofyl	k1gInPc4	mikrosporofyl
<g/>
)	)	kIx)	)
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
samičí	samičí	k2eAgInPc1d1	samičí
plodolisty	plodolist	k1gInPc1	plodolist
(	(	kIx(	(
<g/>
megasporofyly	megasporofyl	k1gInPc1	megasporofyl
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
oddělených	oddělený	k2eAgFnPc6d1	oddělená
jednopohlavných	jednopohlavný	k2eAgFnPc6d1	jednopohlavný
šišticích	šištice	k1gFnPc6	šištice
<g/>
.	.	kIx.	.
</s>
<s>
Oplozená	oplozený	k2eAgNnPc1d1	oplozené
dozrávající	dozrávající	k2eAgNnPc1d1	dozrávající
vajíčka	vajíčko	k1gNnPc1	vajíčko
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
šišce	šiška	k1gFnSc6	šiška
volně	volně	k6eAd1	volně
na	na	k7c6	na
plodolistu	plodolist	k1gInSc6	plodolist
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většina	k1gFnSc7	většina
křídlatá	křídlatý	k2eAgFnSc1d1	křídlatá
a	a	k8xC	a
po	po	k7c4	po
dozrání	dozrání	k1gNnSc4	dozrání
se	se	k3xPyFc4	se
šišky	šiška	k1gFnSc2	šiška
rozpadnou	rozpadnout	k5eAaPmIp3nP	rozpadnout
a	a	k8xC	a
semena	semeno	k1gNnPc1	semeno
vítr	vítr	k1gInSc1	vítr
rozfouká	rozfoukat	k5eAaPmIp3nS	rozfoukat
<g/>
.	.	kIx.	.
</s>
<s>
Liánovcotvaré	Liánovcotvarý	k2eAgInPc1d1	Liánovcotvarý
–	–	k?	–
3	[number]	k4	3
rody	rod	k1gInPc7	rod
<g/>
,	,	kIx,	,
dvoudomé	dvoudomý	k2eAgFnPc4d1	dvoudomá
stálezelené	stálezelený	k2eAgFnPc4d1	stálezelená
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
keře	keř	k1gInPc4	keř
nebo	nebo	k8xC	nebo
liány	liána	k1gFnPc4	liána
<g/>
.	.	kIx.	.
</s>
<s>
Chvojník	chvojník	k1gInSc1	chvojník
je	být	k5eAaImIp3nS	být
metlovitý	metlovitý	k2eAgInSc1d1	metlovitý
keř	keř	k1gInSc1	keř
se	s	k7c7	s
šupinkovými	šupinkový	k2eAgInPc7d1	šupinkový
listy	list	k1gInPc7	list
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
aridních	aridní	k2eAgFnPc6d1	aridní
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
větvičky	větvička	k1gFnPc4	větvička
i	i	k8xC	i
dužnaté	dužnatý	k2eAgInPc4d1	dužnatý
obaly	obal	k1gInPc4	obal
semen	semeno	k1gNnPc2	semeno
připomínající	připomínající	k2eAgFnSc2d1	připomínající
bobule	bobule	k1gFnSc2	bobule
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
efedrin	efedrin	k1gInSc4	efedrin
<g/>
.	.	kIx.	.
</s>
<s>
Liánovec	Liánovec	k1gMnSc1	Liánovec
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
keř	keř	k1gInSc1	keř
nebo	nebo	k8xC	nebo
liána	liána	k1gFnSc1	liána
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnPc4	jeho
semena	semeno	k1gNnPc4	semeno
s	s	k7c7	s
dužnatými	dužnatý	k2eAgInPc7d1	dužnatý
obaly	obal	k1gInPc7	obal
i	i	k8xC	i
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgFnPc4d1	významná
poživatiny	poživatina	k1gFnPc4	poživatina
<g/>
.	.	kIx.	.
</s>
<s>
Welwitschia	Welwitschia	k1gFnSc1	Welwitschia
je	být	k5eAaImIp3nS	být
atypická	atypický	k2eAgFnSc1d1	atypická
dlouhověká	dlouhověký	k2eAgFnSc1d1	dlouhověká
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hluboký	hluboký	k2eAgInSc4d1	hluboký
kotvící	kotvící	k2eAgInSc4d1	kotvící
kořen	kořen	k1gInSc4	kořen
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc4	dva
široké	široký	k2eAgInPc4d1	široký
ploché	plochý	k2eAgInPc4d1	plochý
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
stále	stále	k6eAd1	stále
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
a	a	k8xC	a
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
usychají	usychat	k5eAaImIp3nP	usychat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
nahosemenných	nahosemenný	k2eAgFnPc2d1	nahosemenná
rostlin	rostlina	k1gFnPc2	rostlina
opylovaných	opylovaný	k2eAgMnPc2d1	opylovaný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
rostlin	rostlina	k1gFnPc2	rostlina
patřících	patřící	k2eAgFnPc2d1	patřící
do	do	k7c2	do
třídy	třída	k1gFnSc2	třída
jehličnanů	jehličnan	k1gInPc2	jehličnan
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
popsatelný	popsatelný	k2eAgInSc1d1	popsatelný
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
po	po	k7c6	po
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
stavebních	stavební	k2eAgInPc2d1	stavební
a	a	k8xC	a
konstrukčních	konstrukční	k2eAgInPc2d1	konstrukční
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
prostředkem	prostředek	k1gInSc7	prostředek
umožňující	umožňující	k2eAgInPc4d1	umožňující
kontakty	kontakt	k1gInPc4	kontakt
mezi	mezi	k7c7	mezi
civilizacemi	civilizace	k1gFnPc7	civilizace
(	(	kIx(	(
<g/>
lodě	loď	k1gFnPc1	loď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pomůckou	pomůcka	k1gFnSc7	pomůcka
pro	pro	k7c4	pro
sdělování	sdělování	k1gNnSc4	sdělování
vědomostí	vědomost	k1gFnPc2	vědomost
(	(	kIx(	(
<g/>
papír	papír	k1gInSc1	papír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
topivem	topivo	k1gNnSc7	topivo
umožňující	umožňující	k2eAgNnSc4d1	umožňující
lidskému	lidský	k2eAgNnSc3d1	lidské
pokolení	pokolení	k1gNnSc3	pokolení
přežití	přežití	k1gNnSc2	přežití
v	v	k7c6	v
nehostinných	hostinný	k2eNgFnPc6d1	nehostinná
krajinách	krajina	k1gFnPc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
tento	tento	k3xDgInSc4	tento
význam	význam	k1gInSc4	význam
již	již	k6eAd1	již
"	"	kIx"	"
<g/>
mrtvých	mrtvý	k2eAgInPc2d1	mrtvý
<g/>
"	"	kIx"	"
jehličnanů	jehličnan	k1gInPc2	jehličnan
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
nezměrný	změrný	k2eNgInSc4d1	změrný
přínos	přínos	k1gInSc4	přínos
"	"	kIx"	"
<g/>
živých	živý	k2eAgFnPc2d1	živá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
zachování	zachování	k1gNnSc6	zachování
obyvatelné	obyvatelný	k2eAgFnSc2d1	obyvatelná
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
chrání	chránit	k5eAaImIp3nS	chránit
půdu	půda	k1gFnSc4	půda
před	před	k7c7	před
erozí	eroze	k1gFnSc7	eroze
<g/>
,	,	kIx,	,
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
místo	místo	k1gNnSc4	místo
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
i	i	k9	i
obživu	obživa	k1gFnSc4	obživa
tisícům	tisíc	k4xCgInPc3	tisíc
živočichů	živočich	k1gMnPc2	živočich
včetně	včetně	k7c2	včetně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
lapají	lapat	k5eAaImIp3nP	lapat
tisíce	tisíc	k4xCgInPc1	tisíc
tun	tuna	k1gFnPc2	tuna
emisí	emise	k1gFnPc2	emise
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
majestátným	majestátný	k2eAgInSc7d1	majestátný
vzhledem	vzhled	k1gInSc7	vzhled
a	a	k8xC	a
zakletým	zakletý	k2eAgInSc7d1	zakletý
klidem	klid	k1gInSc7	klid
působí	působit	k5eAaImIp3nS	působit
blahodárně	blahodárně	k6eAd1	blahodárně
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
psychiku	psychika	k1gFnSc4	psychika
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
pro	pro	k7c4	pro
potěchu	potěcha	k1gFnSc4	potěcha
duše	duše	k1gFnSc2	duše
vysazujeme	vysazovat	k5eAaImIp1nP	vysazovat
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
nebo	nebo	k8xC	nebo
je	on	k3xPp3gNnSc4	on
navštěvujeme	navštěvovat	k5eAaImIp1nP	navštěvovat
na	na	k7c6	na
původních	původní	k2eAgNnPc6d1	původní
stanovištích	stanoviště	k1gNnPc6	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Trvalost	trvalost	k1gFnSc1	trvalost
a	a	k8xC	a
pravidelnost	pravidelnost	k1gFnSc1	pravidelnost
tvarů	tvar	k1gInPc2	tvar
jehličnanů	jehličnan	k1gInPc2	jehličnan
i	i	k8xC	i
možnost	možnost	k1gFnSc4	možnost
tvarování	tvarování	k1gNnSc2	tvarování
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
odrůd	odrůda	k1gFnPc2	odrůda
jehličnanů	jehličnan	k1gInPc2	jehličnan
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
častému	častý	k2eAgNnSc3d1	časté
používání	používání	k1gNnSc3	používání
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
a	a	k8xC	a
historizujících	historizující	k2eAgFnPc6d1	historizující
zahradách	zahrada	k1gFnPc6	zahrada
s	s	k7c7	s
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
kompozicí	kompozice	k1gFnSc7	kompozice
(	(	kIx(	(
<g/>
francouzský	francouzský	k2eAgInSc1d1	francouzský
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obliba	obliba	k1gFnSc1	obliba
ve	v	k7c6	v
vytváření	vytváření	k1gNnSc6	vytváření
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
kompozic	kompozice	k1gFnPc2	kompozice
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jehličnatých	jehličnatý	k2eAgFnPc2d1	jehličnatá
dřevin	dřevina	k1gFnPc2	dřevina
trvá	trvat	k5eAaImIp3nS	trvat
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
laické	laický	k2eAgFnSc6d1	laická
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
doposud	doposud	k6eAd1	doposud
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgInPc1d1	barevný
kultivary	kultivar	k1gInPc1	kultivar
stálezelených	stálezelený	k2eAgInPc2d1	stálezelený
jehličnanů	jehličnan	k1gInPc2	jehličnan
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejpoužívanější	používaný	k2eAgFnPc4d3	nejpoužívanější
rostliny	rostlina	k1gFnPc4	rostlina
sadovnické	sadovnický	k2eAgFnSc2d1	Sadovnická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
malých	malý	k2eAgFnPc6d1	malá
úpravách	úprava	k1gFnPc6	úprava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
použití	použití	k1gNnSc3	použití
pro	pro	k7c4	pro
velkou	velká	k1gFnSc4	velká
celoroční	celoroční	k2eAgFnSc1d1	celoroční
výraznost	výraznost	k1gFnSc1	výraznost
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pečlivého	pečlivý	k2eAgNnSc2d1	pečlivé
promyšlení	promyšlení	k1gNnSc2	promyšlení
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Jehličnany	jehličnan	k1gInPc1	jehličnan
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
estetickém	estetický	k2eAgInSc6d1	estetický
účinku	účinek	k1gInSc6	účinek
tak	tak	k9	tak
svérázné	svérázný	k2eAgNnSc1d1	svérázné
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
promyšlené	promyšlený	k2eAgNnSc4d1	promyšlené
umístění	umístění	k1gNnSc4	umístění
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
použitím	použití	k1gNnSc7	použití
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
jednotvárné	jednotvárný	k2eAgInPc1d1	jednotvárný
<g/>
,	,	kIx,	,
strnulé	strnulý	k2eAgInPc1d1	strnulý
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
příliš	příliš	k6eAd1	příliš
neklidné	klidný	k2eNgFnSc2d1	neklidná
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
olistění	olistěný	k2eAgMnPc1d1	olistěný
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
negativní	negativní	k2eAgFnSc1d1	negativní
stránka	stránka	k1gFnSc1	stránka
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
výhod	výhoda	k1gFnPc2	výhoda
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
.	.	kIx.	.
</s>
<s>
Větve	větev	k1gFnPc1	větev
a	a	k8xC	a
listy	list	k1gInPc1	list
jehličnanů	jehličnan	k1gInPc2	jehličnan
se	se	k3xPyFc4	se
ve	v	k7c6	v
vánku	vánek	k1gInSc6	vánek
a	a	k8xC	a
větru	vítr	k1gInSc6	vítr
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc1	jejich
tvary	tvar	k1gInPc1	tvar
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
až	až	k9	až
přísné	přísný	k2eAgFnPc1d1	přísná
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
jehličnany	jehličnan	k1gInPc1	jehličnan
tedy	tedy	k9	tedy
působí	působit	k5eAaImIp3nP	působit
strnule	strnule	k6eAd1	strnule
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
barevné	barevný	k2eAgInPc1d1	barevný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k9	tak
nepříliš	příliš	k6eNd1	příliš
nápadné	nápadný	k2eAgNnSc1d1	nápadné
<g/>
,	,	kIx,	,
plody	plod	k1gInPc1	plod
<g/>
.	.	kIx.	.
</s>
<s>
Výraznost	výraznost	k1gFnSc1	výraznost
jehličnanů	jehličnan	k1gInPc2	jehličnan
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
tvarováním	tvarování	k1gNnSc7	tvarování
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
třídy	třída	k1gFnSc2	třída
jehličnanů	jehličnan	k1gInPc2	jehličnan
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
tato	tento	k3xDgFnSc1	tento
(	(	kIx(	(
<g/>
jen	jen	k6eAd1	jen
recentní	recentní	k2eAgInSc1d1	recentní
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Jehličnany	jehličnan	k1gInPc1	jehličnan
(	(	kIx(	(
<g/>
Pinopsida	Pinopsida	k1gFnSc1	Pinopsida
<g/>
)	)	kIx)	)
│	│	k?	│
┌	┌	k?	┌
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
cykasovité	cykasovitý	k2eAgInPc1d1	cykasovitý
(	(	kIx(	(
<g/>
Cycadaceae	Cycadacea	k1gInPc1	Cycadacea
<g/>
)	)	kIx)	)
├	├	k?	├
<g/>
─	─	k?	─
řád	řád	k1gInSc4	řád
cykasotvaré	cykasotvarý	k2eAgFnPc1d1	cykasotvarý
(	(	kIx(	(
<g/>
Cycadales	Cycadales	k1gInSc1	Cycadales
<g/>
)	)	kIx)	)
<g/>
─	─	k?	─
<g/>
│	│	k?	│
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
<g />
.	.	kIx.	.
</s>
<s>
kejákovité	kejákovitý	k2eAgInPc4d1	kejákovitý
(	(	kIx(	(
<g/>
Zamiaceae	Zamiacea	k1gInPc4	Zamiacea
<g/>
)	)	kIx)	)
│	│	k?	│
├	├	k?	├
<g/>
─	─	k?	─
řád	řád	k1gInSc4	řád
jinanotvaré	jinanotvarý	k2eAgFnPc1d1	jinanotvarý
(	(	kIx(	(
<g/>
Ginkgoales	Ginkgoales	k1gInSc1	Ginkgoales
<g/>
)	)	kIx)	)
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
jinanovité	jinanovitý	k2eAgInPc1d1	jinanovitý
(	(	kIx(	(
<g/>
Ginkgoacea	Ginkgoacea	k1gFnSc1	Ginkgoacea
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
┌	┌	k?	┌
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
liánovcovité	liánovcovitý	k2eAgInPc1d1	liánovcovitý
(	(	kIx(	(
<g/>
Gnetaceae	Gnetacea	k1gInPc1	Gnetacea
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
│	│	k?	│
┌	┌	k?	┌
<g/>
─	─	k?	─
řád	řád	k1gInSc4	řád
liánovcotvaré	liánovcotvarý	k2eAgFnPc1d1	liánovcotvarý
(	(	kIx(	(
<g/>
Gnetales	Gnetales	k1gInSc1	Gnetales
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
─	─	k?	─
<g/>
│	│	k?	│
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
chvojníkovité	chvojníkovitý	k2eAgInPc1d1	chvojníkovitý
(	(	kIx(	(
<g/>
Ephedracea	Ephedracea	k1gFnSc1	Ephedracea
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
│	│	k?	│
│	│	k?	│
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
welwitschiovité	welwitschiovitý	k2eAgInPc1d1	welwitschiovitý
(	(	kIx(	(
<g/>
Welwitschiaceae	Welwitschiacea	k1gInPc1	Welwitschiacea
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
<g/>
│	│	k?	│
│	│	k?	│
┌	┌	k?	┌
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
borovicovité	borovicovitý	k2eAgInPc1d1	borovicovitý
(	(	kIx(	(
<g/>
Pinaceae	Pinacea	k1gInPc1	Pinacea
<g/>
)	)	kIx)	)
│	│	k?	│
│	│	k?	│
│	│	k?	│
│	│	k?	│
┌	┌	k?	┌
<g />
.	.	kIx.	.
</s>
<s>
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc1	čeleď
blahočetovité	blahočetovitý	k2eAgFnPc1d1	blahočetovitý
(	(	kIx(	(
<g/>
Araucariaceae	Araucariacea	k1gFnPc1	Araucariacea
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
řád	řád	k1gInSc4	řád
borovicotvaré	borovicotvarý	k2eAgFnPc1d1	borovicotvarý
(	(	kIx(	(
<g/>
Pinales	Pinales	k1gInSc1	Pinales
<g/>
)	)	kIx)	)
<g/>
─	─	k?	─
<g/>
│	│	k?	│
┌	┌	k?	┌
<g/>
─	─	k?	─
<g/>
│	│	k?	│
│	│	k?	│
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
nohoplodovité	nohoplodovitý	k2eAgInPc1d1	nohoplodovitý
(	(	kIx(	(
<g/>
Podocarpaceae	Podocarpacea	k1gInPc1	Podocarpacea
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
<g/>
│	│	k?	│
│	│	k?	│
┌	┌	k?	┌
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
pajehličníkovité	pajehličníkovitý	k2eAgInPc1d1	pajehličníkovitý
(	(	kIx(	(
<g/>
Sciadopityaceae	Sciadopityacea	k1gInPc1	Sciadopityacea
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
<g/>
│	│	k?	│
│	│	k?	│
┌	┌	k?	┌
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
cypřišovité	cypřišovitý	k2eAgInPc1d1	cypřišovitý
(	(	kIx(	(
<g/>
Cupressaceae	Cupressacea	k1gInPc1	Cupressacea
<g/>
)	)	kIx)	)
└	└	k?	└
<g/>
─	─	k?	─
<g/>
│	│	k?	│
└	└	k?	└
<g/>
─	─	k?	─
čeleď	čeleď	k1gFnSc4	čeleď
tisovité	tisovitý	k2eAgInPc1d1	tisovitý
(	(	kIx(	(
<g/>
Taxaceae	Taxacea	k1gInPc1	Taxacea
<g/>
)	)	kIx)	)
</s>
