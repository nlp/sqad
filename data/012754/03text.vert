<p>
<s>
Trdelník	trdelník	k1gMnSc1	trdelník
je	být	k5eAaImIp3nS	být
sladké	sladký	k2eAgNnSc4d1	sladké
pečivo	pečivo	k1gNnSc4	pečivo
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
podobném	podobný	k2eAgInSc6d1	podobný
velké	velký	k2eAgFnSc3d1	velká
duté	dutý	k2eAgFnSc3d1	dutá
kremroli	kremrole	k1gFnSc3	kremrole
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
krémové	krémový	k2eAgFnSc2d1	krémová
náplně	náplň	k1gFnSc2	náplň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
rozválené	rozválený	k2eAgFnPc1d1	rozválená
v	v	k7c6	v
pruzích	pruh	k1gInPc6	pruh
navíjí	navíjet	k5eAaImIp3nS	navíjet
na	na	k7c4	na
bukový	bukový	k2eAgInSc4d1	bukový
válec	válec	k1gInSc4	válec
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
také	také	k9	také
kovový	kovový	k2eAgInSc1d1	kovový
<g/>
)	)	kIx)	)
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
cm	cm	kA	cm
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
trdlo	trdlo	k1gNnSc4	trdlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
peče	péct	k5eAaImIp3nS	péct
se	se	k3xPyFc4	se
v	v	k7c6	v
žáru	žár	k1gInSc6	žár
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pečení	pečení	k1gNnSc6	pečení
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
<g/>
,	,	kIx,	,
potírá	potírat	k5eAaImIp3nS	potírat
mlékem	mléko	k1gNnSc7	mléko
až	až	k6eAd1	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nezíská	získat	k5eNaPmIp3nS	získat
zlatočervenou	zlatočervený	k2eAgFnSc4d1	zlatočervená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
z	z	k7c2	z
válce	válec	k1gInSc2	válec
opatrně	opatrně	k6eAd1	opatrně
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
neporušil	porušit	k5eNaPmAgMnS	porušit
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Skalický	skalický	k2eAgMnSc1d1	skalický
trdelník	trdelník	k1gMnSc1	trdelník
==	==	k?	==
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
trdelníku	trdelník	k1gMnSc3	trdelník
ze	z	k7c2	z
Skalice	Skalice	k1gFnSc2	Skalice
<g/>
,	,	kIx,	,
městečka	městečko	k1gNnSc2	městečko
na	na	k7c6	na
slovensko-moravském	slovenskooravský	k2eAgNnSc6d1	slovensko-moravský
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
s	s	k7c7	s
Maďary	maďar	k1gInPc7	maďar
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
slovenské	slovenský	k2eAgFnSc2d1	slovenská
Skalice	Skalice	k1gFnSc2	Skalice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
Slovensko	Slovensko	k1gNnSc4	Slovensko
pro	pro	k7c4	pro
Skalický	skalický	k2eAgInSc4d1	skalický
trdelník	trdelník	k1gMnSc1	trdelník
registrovanou	registrovaný	k2eAgFnSc4d1	registrovaná
známku	známka	k1gFnSc4	známka
EU	EU	kA	EU
"	"	kIx"	"
<g/>
chráněné	chráněný	k2eAgNnSc1d1	chráněné
zeměpisné	zeměpisný	k2eAgNnSc1d1	zeměpisné
označení	označení	k1gNnSc1	označení
<g/>
"	"	kIx"	"
–	–	k?	–
PGI	PGI	kA	PGI
(	(	kIx(	(
<g/>
protected	protected	k1gMnSc1	protected
geographical	geographicat	k5eAaPmAgMnS	geographicat
indication	indication	k1gInSc4	indication
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Trdelníky	trdelník	k1gMnPc4	trdelník
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
==	==	k?	==
</s>
</p>
<p>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
a	a	k8xC	a
také	také	k9	také
jinde	jinde	k6eAd1	jinde
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc4	Švédsko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
)	)	kIx)	)
registrovánu	registrován	k2eAgFnSc4d1	registrována
značku	značka	k1gFnSc4	značka
PGI	PGI	kA	PGI
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
je	být	k5eAaImIp3nS	být
trdelník	trdelník	k1gMnSc1	trdelník
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
kürtőskalács	kürtőskalács	k1gInSc1	kürtőskalács
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
"	"	kIx"	"
<g/>
národní	národní	k2eAgInSc1d1	národní
symbol	symbol	k1gInSc1	symbol
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
podobné	podobný	k2eAgInPc4d1	podobný
výrobky	výrobek	k1gInPc4	výrobek
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
:	:	kIx,	:
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Prügelkrapfen	Prügelkrapfen	k2eAgMnSc1d1	Prügelkrapfen
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
<g/>
:	:	kIx,	:
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Baumstriezel	Baumstriezel	k1gMnSc1	Baumstriezel
nebo	nebo	k8xC	nebo
Baumkuchen	Baumkuchen	k2eAgMnSc1d1	Baumkuchen
</s>
</p>
<p>
<s>
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Baamkuch	Baamkuch	k1gMnSc1	Baamkuch
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgNnSc7d1	tradiční
jídlem	jídlo	k1gNnSc7	jídlo
na	na	k7c6	na
svatbách	svatba	k1gFnPc6	svatba
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
slavnostech	slavnost	k1gFnPc6	slavnost
</s>
</p>
<p>
<s>
Litva	Litva	k1gFnSc1	Litva
<g/>
:	:	kIx,	:
Šakotis	Šakotis	k1gFnSc1	Šakotis
nebo	nebo	k8xC	nebo
Raguolis	Raguolis	k1gFnSc1	Raguolis
(	(	kIx(	(
<g/>
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Litvě	Litva	k1gFnSc6	Litva
jako	jako	k9	jako
Bamkuchinas	Bamkuchinas	k1gMnSc1	Bamkuchinas
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
:	:	kIx,	:
Sękacz	Sękacz	k1gInSc1	Sękacz
podobný	podobný	k2eAgInSc1d1	podobný
zákusek	zákusek	k1gInSc4	zákusek
</s>
</p>
<p>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
:	:	kIx,	:
Skalický	skalický	k2eAgMnSc1d1	skalický
trdelník	trdelník	k1gMnSc1	trdelník
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
má	mít	k5eAaImIp3nS	mít
Slovensko	Slovensko	k1gNnSc4	Slovensko
registrovánu	registrován	k2eAgFnSc4d1	registrována
unijní	unijní	k2eAgFnSc4d1	unijní
značku	značka	k1gFnSc4	značka
PGI	PGI	kA	PGI
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
název	název	k1gInSc4	název
Spettekaka	Spettekak	k1gMnSc2	Spettekak
má	mít	k5eAaImIp3nS	mít
Švédsko	Švédsko	k1gNnSc4	Švédsko
také	také	k6eAd1	také
registrovánu	registrován	k2eAgFnSc4d1	registrována
unijní	unijní	k2eAgFnSc4d1	unijní
značku	značka	k1gFnSc4	značka
PGI	PGI	kA	PGI
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trdelník	trdelník	k1gMnSc1	trdelník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
trdelník	trdelník	k1gMnSc1	trdelník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Trdelník	trdelník	k1gMnSc1	trdelník
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
.	.	kIx.	.
</s>
<s>
Češi	česat	k5eAaImIp1nS	česat
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
nadchli	nadchnout	k5eAaPmAgMnP	nadchnout
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
</s>
</p>
