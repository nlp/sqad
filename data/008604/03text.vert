<p>
<s>
Armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
pouze	pouze	k6eAd1	pouze
sbor	sbor	k1gInSc1	sbor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
vojenská	vojenský	k2eAgFnSc1d1	vojenská
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
armád	armáda	k1gFnPc2	armáda
tvořena	tvořit	k5eAaImNgNnP	tvořit
dvěma	dva	k4xCgFnPc7	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
divizemi	divize	k1gFnPc7	divize
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
obvykle	obvykle	k6eAd1	obvykle
velí	velet	k5eAaImIp3nS	velet
generálporučík	generálporučík	k1gMnSc1	generálporučík
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
případně	případně	k6eAd1	případně
sborový	sborový	k2eAgMnSc1d1	sborový
generál	generál	k1gMnSc1	generál
<g/>
.	.	kIx.	.
</s>
<s>
Sbory	sbor	k1gInPc1	sbor
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
spojují	spojovat	k5eAaImIp3nP	spojovat
v	v	k7c4	v
armády	armáda	k1gFnPc4	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sbory	sbor	k1gInPc1	sbor
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
značí	značit	k5eAaImIp3nP	značit
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
sbor	sbor	k1gInSc1	sbor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
divize	divize	k1gFnSc1	divize
číslicemi	číslice	k1gFnPc7	číslice
arabskými	arabský	k2eAgFnPc7d1	arabská
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
pěchotní	pěchotní	k2eAgFnSc2d1	pěchotní
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
a	a	k8xC	a
názvy	název	k1gInPc1	název
armád	armáda	k1gFnPc2	armáda
se	se	k3xPyFc4	se
vypisují	vypisovat	k5eAaImIp3nP	vypisovat
slovně	slovně	k6eAd1	slovně
(	(	kIx(	(
<g/>
pátá	pátý	k4xOgFnSc1	pátý
armáda	armáda	k1gFnSc1	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
není	být	k5eNaImIp3nS	být
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
zápisu	zápis	k1gInSc2	zápis
závazný	závazný	k2eAgInSc1d1	závazný
<g/>
.	.	kIx.	.
</s>
<s>
Členění	členění	k1gNnSc1	členění
na	na	k7c4	na
sbory	sbor	k1gInPc4	sbor
používá	používat	k5eAaImIp3nS	používat
většina	většina	k1gFnSc1	většina
armád	armáda	k1gFnPc2	armáda
včetně	včetně	k7c2	včetně
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
spíše	spíše	k9	spíše
ze	z	k7c2	z
symbolických	symbolický	k2eAgInPc2d1	symbolický
důvodů	důvod	k1gInPc2	důvod
i	i	k8xC	i
některé	některý	k3yIgFnSc2	některý
nevojenské	vojenský	k2eNgFnSc2d1	nevojenská
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pacifistická	pacifistický	k2eAgFnSc1d1	pacifistická
organizace	organizace	k1gFnSc1	organizace
Peace	Peace	k1gMnSc1	Peace
Corps	corps	k1gInSc1	corps
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
Mírový	mírový	k2eAgInSc1d1	mírový
sbor	sbor	k1gInSc1	sbor
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
pojetí	pojetí	k1gNnSc6	pojetí
značí	značit	k5eAaImIp3nS	značit
armádní	armádní	k2eAgInSc4d1	armádní
sbor	sbor	k1gInSc4	sbor
jednotku	jednotka	k1gFnSc4	jednotka
převážně	převážně	k6eAd1	převážně
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
pěších	pěší	k2eAgFnPc2d1	pěší
divizí	divize	k1gFnPc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Sbory	sbor	k1gInPc1	sbor
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
států	stát	k1gInPc2	stát
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
armádní	armádní	k2eAgFnPc1d1	armádní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
také	také	k9	také
tankové	tankový	k2eAgFnPc1d1	tanková
<g/>
,	,	kIx,	,
mechanizované	mechanizovaný	k2eAgFnPc1d1	mechanizovaná
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
horské	horský	k2eAgFnPc1d1	horská
<g/>
.	.	kIx.	.
</s>
</p>
