<s>
Armádní	armádní	k2eAgInSc1d1	armádní
sbor	sbor	k1gInSc1	sbor
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
pouze	pouze	k6eAd1	pouze
sbor	sbor	k1gInSc1	sbor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
vojenská	vojenský	k2eAgFnSc1d1	vojenská
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
armád	armáda	k1gFnPc2	armáda
tvořena	tvořit	k5eAaImNgNnP	tvořit
dvěma	dva	k4xCgFnPc7	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
divizemi	divize	k1gFnPc7	divize
<g/>
.	.	kIx.	.
</s>
