<p>
<s>
Amiranty	Amiranty	k1gFnPc4	Amiranty
je	být	k5eAaImIp3nS	být
korálové	korálový	k2eAgNnSc1d1	korálové
souostroví	souostroví	k1gNnSc1	souostroví
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
republice	republika	k1gFnSc6	republika
Seychely	Seychely	k1gFnPc1	Seychely
<g/>
.	.	kIx.	.
</s>
<s>
Řetěz	řetěz	k1gInSc1	řetěz
drobných	drobný	k2eAgInPc2d1	drobný
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
155	[number]	k4	155
km	km	kA	km
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
od	od	k7c2	od
největšího	veliký	k2eAgInSc2d3	veliký
ostrova	ostrov	k1gInSc2	ostrov
Seychel	Seychely	k1gFnPc2	Seychely
Mahé	Mahá	k1gFnSc2	Mahá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
ostrovů	ostrov	k1gInPc2	ostrov
==	==	k?	==
</s>
</p>
<p>
<s>
Lady	lady	k1gFnSc1	lady
Denison-Pender	Denison-Pender	k1gMnSc1	Denison-Pender
Shoal	Shoal	k1gMnSc1	Shoal
</s>
</p>
<p>
<s>
African	African	k1gInSc1	African
Banks	Banksa	k1gFnPc2	Banksa
</s>
</p>
<p>
<s>
Remire	Remir	k1gMnSc5	Remir
Reef	Reef	k1gMnSc1	Reef
</s>
</p>
<p>
<s>
Remire	Remir	k1gMnSc5	Remir
Island	Island	k1gInSc1	Island
</s>
</p>
<p>
<s>
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Arros	Arros	k1gInSc1	Arros
Island	Island	k1gInSc1	Island
</s>
</p>
<p>
<s>
Saint	Saint	k1gMnSc1	Saint
Joseph	Joseph	k1gMnSc1	Joseph
Atoll	Atoll	k1gMnSc1	Atoll
</s>
</p>
<p>
<s>
Bertaut	Bertaut	k2eAgInSc1d1	Bertaut
Reef	Reef	k1gInSc1	Reef
</s>
</p>
<p>
<s>
Île	Île	k?	Île
Desroches	Desroches	k1gInSc1	Desroches
</s>
</p>
<p>
<s>
Poivre	Poivr	k1gMnSc5	Poivr
Islands	Islands	k1gInSc1	Islands
</s>
</p>
<p>
<s>
Étoile	Étoile	k6eAd1	Étoile
Cay	Cay	k1gFnSc1	Cay
</s>
</p>
<p>
<s>
Boudeuse	Boudeuse	k1gFnSc1	Boudeuse
Cay	Cay	k1gFnSc2	Cay
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Louise	Louis	k1gMnSc2	Louis
Island	Island	k1gInSc1	Island
</s>
</p>
<p>
<s>
Île	Île	k?	Île
Desnœ	Desnœ	k1gMnSc1	Desnœ
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
devadesát	devadesát	k4xCc1	devadesát
km	km	kA	km
jižněji	jižně	k6eAd2	jižně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
navzdory	navzdory	k6eAd1	navzdory
vnější	vnější	k2eAgFnSc2d1	vnější
podobnosti	podobnost	k1gFnSc2	podobnost
k	k	k7c3	k
Amirantám	Amiranty	k1gFnPc3	Amiranty
obvykle	obvykle	k6eAd1	obvykle
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neleží	ležet	k5eNaImIp3nS	ležet
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
podmořské	podmořský	k2eAgFnSc6d1	podmořská
platformě	platforma	k1gFnSc6	platforma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
Amirant	Amiranty	k1gFnPc2	Amiranty
je	být	k5eAaImIp3nS	být
9	[number]	k4	9
<g/>
,	,	kIx,	,
91	[number]	k4	91
km2	km2	k4	km2
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
mají	mít	k5eAaImIp3nP	mít
asi	asi	k9	asi
100	[number]	k4	100
stálých	stálý	k2eAgMnPc2d1	stálý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
většina	většina	k1gFnSc1	většina
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
ostrově	ostrov	k1gInSc6	ostrov
Desroches	Desrochesa	k1gFnPc2	Desrochesa
<g/>
,	,	kIx,	,
obydlené	obydlený	k2eAgInPc1d1	obydlený
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Arros	Arros	k1gMnSc1	Arros
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Joseph	Joseph	k1gMnSc1	Joseph
<g/>
,	,	kIx,	,
Poivre	Poivr	k1gInSc5	Poivr
a	a	k8xC	a
Marie	Marie	k1gFnSc1	Marie
Louise	Louis	k1gMnSc2	Louis
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgInPc1d1	nízký
písečné	písečný	k2eAgInPc1d1	písečný
ostrovy	ostrov	k1gInPc1	ostrov
strádající	strádající	k2eAgInPc1d1	strádající
nedostatkem	nedostatek	k1gInSc7	nedostatek
pitně	pitně	k6eAd1	pitně
vody	voda	k1gFnSc2	voda
by	by	kYmCp3nS	by
víc	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
ani	ani	k8xC	ani
neuživily	uživit	k5eNaPmAgInP	uživit
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
rovníkové	rovníkový	k2eAgNnSc1d1	rovníkové
<g/>
,	,	kIx,	,
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
kokosové	kokosový	k2eAgFnPc1d1	kokosová
palmy	palma	k1gFnPc1	palma
<g/>
,	,	kIx,	,
zdrojem	zdroj	k1gInSc7	zdroj
potravy	potrava	k1gFnSc2	potrava
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
mořské	mořský	k2eAgFnPc1d1	mořská
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
želvy	želva	k1gFnPc1	želva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrovy	ostrov	k1gInPc4	ostrov
objevil	objevit	k5eAaPmAgInS	objevit
roku	rok	k1gInSc2	rok
1502	[number]	k4	1502
Vasco	Vasco	k1gMnSc1	Vasco
da	da	k?	da
Gama	gama	k1gNnSc4	gama
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
titulu	titul	k1gInSc2	titul
Almirante	Almirant	k1gMnSc5	Almirant
(	(	kIx(	(
<g/>
admirál	admirál	k1gMnSc1	admirál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1756	[number]	k4	1756
patřily	patřit	k5eAaImAgFnP	patřit
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
přešly	přejít	k5eAaPmAgInP	přejít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Pařížského	pařížský	k2eAgInSc2d1	pařížský
míru	mír	k1gInSc2	mír
pod	pod	k7c4	pod
britskou	britský	k2eAgFnSc4d1	britská
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
samostatných	samostatný	k2eAgFnPc2d1	samostatná
Seychel	Seychely	k1gFnPc2	Seychely
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amiranty	Amiranty	k1gFnPc1	Amiranty
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
Vnějším	vnější	k2eAgInPc3d1	vnější
ostrovům	ostrov	k1gInPc3	ostrov
(	(	kIx(	(
<g/>
seychelsky	seychelsky	k6eAd1	seychelsky
Zil	Zil	k1gFnSc3	Zil
Elwannyen	Elwannyen	k2eAgInSc4d1	Elwannyen
Sesel	sesel	k1gInSc4	sesel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
kvůli	kvůli	k7c3	kvůli
rozptýlenému	rozptýlený	k2eAgNnSc3d1	rozptýlené
a	a	k8xC	a
početně	početně	k6eAd1	početně
zanedbatelnému	zanedbatelný	k2eAgNnSc3d1	zanedbatelné
osídlení	osídlení	k1gNnSc3	osídlení
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
žádného	žádný	k3yNgInSc2	žádný
z	z	k7c2	z
25	[number]	k4	25
seychelských	seychelský	k2eAgInPc2d1	seychelský
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poklidný	poklidný	k2eAgInSc1d1	poklidný
život	život	k1gInSc1	život
na	na	k7c6	na
Amirantách	Amiranty	k1gFnPc6	Amiranty
popisuje	popisovat	k5eAaImIp3nS	popisovat
Eric	Eric	k1gFnSc1	Eric
Herne	Hern	k1gInSc5	Hern
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
strašidla	strašidlo	k1gNnPc1	strašidlo
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Odeon	odeon	k1gInSc1	odeon
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://www.desroches-island.com/en/home	[url]	k1gInSc5	http://www.desroches-island.com/en/home
</s>
</p>
<p>
<s>
http://encyclopedia2.thefreedictionary.com/Amirante+Islands	[url]	k6eAd1	http://encyclopedia2.thefreedictionary.com/Amirante+Islands
</s>
</p>
