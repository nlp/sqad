<p>
<s>
Fiat	fiat	k1gInSc1	fiat
242	[number]	k4	242
je	být	k5eAaImIp3nS	být
dodávka	dodávka	k1gFnSc1	dodávka
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
italskou	italský	k2eAgFnSc7d1	italská
automobilkou	automobilka	k1gFnSc7	automobilka
Fiat	fiat	k1gInSc1	fiat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Dodávka	dodávka	k1gFnSc1	dodávka
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
automobilkou	automobilka	k1gFnSc7	automobilka
Citroën	Citroëna	k1gFnPc2	Citroëna
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
prodával	prodávat	k5eAaImAgInS	prodávat
jako	jako	k9	jako
Citroën	Citroën	k1gInSc1	Citroën
C	C	kA	C
<g/>
35	[number]	k4	35
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
jiné	jiný	k2eAgInPc4d1	jiný
užitkové	užitkový	k2eAgInPc4d1	užitkový
vozy	vůz	k1gInPc4	vůz
byl	být	k5eAaImAgInS	být
vůz	vůz	k1gInSc1	vůz
vyráběn	vyráběn	k2eAgInSc1d1	vyráběn
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgInPc6d1	různý
provedeních	provedení	k1gNnPc6	provedení
<g/>
,	,	kIx,	,
od	od	k7c2	od
běžné	běžný	k2eAgFnSc2d1	běžná
dodávky	dodávka	k1gFnSc2	dodávka
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
pick-up	pickp	k1gInSc4	pick-up
<g/>
,	,	kIx,	,
mikrobus	mikrobus	k1gInSc4	mikrobus
až	až	k9	až
po	po	k7c4	po
různé	různý	k2eAgFnPc4d1	různá
specializované	specializovaný	k2eAgFnPc4d1	specializovaná
zástavby	zástavba	k1gFnPc4	zástavba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
obytný	obytný	k2eAgInSc4d1	obytný
vůz	vůz	k1gInSc4	vůz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
se	se	k3xPyFc4	se
jak	jak	k8xS	jak
Fiat	fiat	k1gInSc1	fiat
242	[number]	k4	242
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Citroën	Citroën	k1gInSc1	Citroën
C35	C35	k1gMnSc2	C35
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
oba	dva	k4xCgInPc4	dva
modely	model	k1gInPc4	model
měly	mít	k5eAaImAgInP	mít
motory	motor	k1gInPc1	motor
Fiat	fiat	k1gInSc1	fiat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
modelem	model	k1gInSc7	model
Fiat	fiat	k1gInSc4	fiat
Ducato	Ducat	k2eAgNnSc1d1	Ducato
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Citroën	Citroën	k1gMnSc1	Citroën
ponechal	ponechat	k5eAaPmAgMnS	ponechat
svou	svůj	k3xOyFgFnSc4	svůj
verzi	verze	k1gFnSc4	verze
modelu	model	k1gInSc2	model
v	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
pro	pro	k7c4	pro
Citroën	Citroën	k1gInSc4	Citroën
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
Chausson	Chausson	k1gMnSc1	Chausson
(	(	kIx(	(
<g/>
Société	Sociétý	k2eAgNnSc1d1	Société
des	des	k1gNnSc1	des
usines	usines	k1gMnSc1	usines
Chausson	Chausson	k1gMnSc1	Chausson
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolupráce	spolupráce	k1gFnSc1	spolupráce
Fiat	fiat	k1gInSc1	fiat
s	s	k7c7	s
Citroën	Citroëna	k1gFnPc2	Citroëna
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
užitkových	užitkový	k2eAgInPc2d1	užitkový
vozů	vůz	k1gInPc2	vůz
se	se	k3xPyFc4	se
však	však	k9	však
nepřerušila	přerušit	k5eNaPmAgFnS	přerušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
skupinu	skupina	k1gFnSc4	skupina
PSA	pes	k1gMnSc4	pes
Peugeot	peugeot	k1gInSc4	peugeot
Citroën	Citroëna	k1gFnPc2	Citroëna
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
společný	společný	k2eAgInSc1d1	společný
podnik	podnik	k1gInSc1	podnik
Sevel	Sevel	k1gInSc1	Sevel
(	(	kIx(	(
<g/>
Evropská	evropský	k2eAgFnSc1d1	Evropská
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
lehká	lehký	k2eAgNnPc4d1	lehké
nákladní	nákladní	k2eAgNnPc4d1	nákladní
auta	auto	k1gNnPc4	auto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Sevel	Sevela	k1gFnPc2	Sevela
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
<g/>
)	)	kIx)	)
právě	právě	k9	právě
Fiat	fiat	k1gInSc4	fiat
Ducato	Ducat	k2eAgNnSc1d1	Ducato
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
prodáván	prodávat	k5eAaImNgInS	prodávat
jako	jako	k9	jako
Citroën	Citroën	k1gInSc1	Citroën
Jumper	jumper	k1gInSc1	jumper
resp.	resp.	kA	resp.
Peugeot	peugeot	k1gInSc1	peugeot
Boxer	boxer	k1gInSc1	boxer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
Fiatu	fiat	k1gInSc2	fiat
242	[number]	k4	242
byl	být	k5eAaImAgInS	být
model	model	k1gInSc1	model
Fiat	fiat	k1gInSc1	fiat
238	[number]	k4	238
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
šasi	šasi	k1gNnSc4	šasi
Autobianchi	Autobianch	k1gFnSc2	Autobianch
Primula	Primula	k?	Primula
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
výroby	výroba	k1gFnSc2	výroba
Fiatu	fiat	k1gInSc2	fiat
242	[number]	k4	242
automobilka	automobilka	k1gFnSc1	automobilka
původně	původně	k6eAd1	původně
plánovala	plánovat	k5eAaImAgFnS	plánovat
výrobu	výroba	k1gFnSc4	výroba
staršího	starý	k2eAgInSc2d2	starší
modelu	model	k1gInSc2	model
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
ani	ani	k8xC	ani
prodeje	prodej	k1gInSc2	prodej
Fiatu	fiat	k1gInSc2	fiat
238	[number]	k4	238
neklesaly	klesat	k5eNaImAgFnP	klesat
<g/>
,	,	kIx,	,
automobilka	automobilka	k1gFnSc1	automobilka
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
ponechat	ponechat	k5eAaPmF	ponechat
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
i	i	k9	i
starší	starý	k2eAgInSc4d2	starší
model	model	k1gInSc4	model
a	a	k8xC	a
vybavit	vybavit	k5eAaPmF	vybavit
ho	on	k3xPp3gMnSc4	on
také	také	k6eAd1	také
větším	veliký	k2eAgMnSc7d2	veliký
a	a	k8xC	a
silnějším	silný	k2eAgInSc7d2	silnější
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
Fiat	fiat	k1gInSc1	fiat
238	[number]	k4	238
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
modelem	model	k1gInSc7	model
Fiat	fiat	k1gInSc4	fiat
242	[number]	k4	242
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Motory	motor	k1gInPc1	motor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
benzínový	benzínový	k2eAgInSc1d1	benzínový
1.6	[number]	k4	1.6
litru	litr	k1gInSc2	litr
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
62	[number]	k4	62
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
cca	cca	kA	cca
46	[number]	k4	46
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
benzínový	benzínový	k2eAgInSc1d1	benzínový
2.0	[number]	k4	2.0
litru	litr	k1gInSc2	litr
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
cm3	cm3	k4	cm3
<g/>
)	)	kIx)	)
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
70	[number]	k4	70
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
cca	cca	kA	cca
51,5	[number]	k4	51,5
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
diesel	diesel	k1gInSc1	diesel
2.2	[number]	k4	2.2
litru	litr	k1gInSc2	litr
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
61,5	[number]	k4	61,5
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
cca	cca	kA	cca
45	[number]	k4	45
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Převodovka	převodovka	k1gFnSc1	převodovka
<g/>
:	:	kIx,	:
čtyřstupňová	čtyřstupňový	k2eAgFnSc1d1	čtyřstupňová
manuální	manuální	k2eAgFnSc3d1	manuální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
110	[number]	k4	110
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
2.0	[number]	k4	2.0
litru	litr	k1gInSc2	litr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Fiat	fiat	k1gInSc1	fiat
242	[number]	k4	242
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
Fiat	fiat	k1gInSc1	fiat
242	[number]	k4	242
na	na	k7c6	na
španělské	španělský	k2eAgFnSc6d1	španělská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Fiat	fiat	k1gInSc1	fiat
238	[number]	k4	238
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Citroën	Citroën	k1gMnSc1	Citroën
C35	C35	k1gMnSc1	C35
</s>
</p>
<p>
<s>
PSA	pes	k1gMnSc4	pes
Peugeot	peugeot	k1gInSc1	peugeot
Citroën	Citroën	k1gNnSc5	Citroën
</s>
</p>
<p>
<s>
Sevel	Sevel	k1gMnSc1	Sevel
</s>
</p>
<p>
<s>
Fiat	fiat	k1gInSc1	fiat
238	[number]	k4	238
</s>
</p>
<p>
<s>
Fiat	fiat	k1gInSc1	fiat
Ducato	Ducat	k2eAgNnSc1d1	Ducato
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fiat	fiat	k1gInSc1	fiat
242	[number]	k4	242
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
