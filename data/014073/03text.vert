<s>
Plutonium	plutonium	k1gNnSc1
</s>
<s>
Plutonium	plutonium	k1gNnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
6	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
244	#num#	k4
</s>
<s>
Pu	Pu	kA
</s>
<s>
94	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Malé	Malé	k2eAgInPc1d1
kousky	kousek	k1gInPc1
plutonia	plutonium	k1gNnSc2
focené	focený	k2eAgFnSc2d1
vůči	vůči	k7c3
pravítku	pravítko	k1gNnSc3
s	s	k7c7
palcovou	palcový	k2eAgFnSc7d1
a	a	k8xC
centimetrovou	centimetrový	k2eAgFnSc7d1
škálou	škála	k1gFnSc7
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Plutonium	plutonium	k1gNnSc1
<g/>
,	,	kIx,
Pu	Pu	k1gFnSc1
<g/>
,	,	kIx,
94	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Plutonium	plutonium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
f	f	k?
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1
v	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
</s>
<s>
2	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
15	#num#	k4
ppm	ppm	k?
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbrobílý	stříbrobílý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-07-5	7440-07-5	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
244,064	244,064	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
150	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
162	#num#	k4
pm	pm	k?
</s>
<s>
Iontový	iontový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
Pu	Pu	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
108	#num#	k4
pm	pm	k?
(	(	kIx(
<g/>
Pu	Pu	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
93	#num#	k4
pm	pm	k?
(	(	kIx(
<g/>
Pu	Pu	k1gFnSc1
<g/>
5	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
80	#num#	k4
pm	pm	k?
(	(	kIx(
<g/>
Pu	Pu	k1gFnSc1
<g/>
6	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
73	#num#	k4
pm	pm	k?
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1
konfigurace	konfigurace	k1gFnSc1
</s>
<s>
[	[	kIx(
<g/>
Rn	Rn	k1gMnSc6
<g/>
]	]	kIx)
5	#num#	k4
<g/>
f	f	k?
<g/>
6	#num#	k4
7	#num#	k4
<g/>
s	s	k7c7
<g/>
2	#num#	k4
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
III	III	kA
<g/>
,	,	kIx,
IV	IV	kA
<g/>
,	,	kIx,
V	V	kA
<g/>
,	,	kIx,
VI	VI	kA
<g/>
,	,	kIx,
VII	VII	kA
<g/>
,	,	kIx,
VIII	VIII	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1,28	1,28	k4
</s>
<s>
Ionizační	ionizační	k2eAgFnSc1d1
energie	energie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
</s>
<s>
5,5	5,5	k4
eV	eV	k?
</s>
<s>
Druhá	druhý	k4xOgFnSc1
</s>
<s>
13,0	13,0	k4
eV	eV	k?
</s>
<s>
Třetí	třetí	k4xOgFnSc1
</s>
<s>
23,5	23,5	k4
eV	eV	k?
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1
</s>
<s>
39,5	39,5	k4
eV	eV	k?
</s>
<s>
Látkové	látkový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Krystalografická	krystalografický	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
</s>
<s>
α	α	k1gFnSc1
jednoklonná	jednoklonný	k2eAgFnSc1d1
β	β	k1gFnSc1
jednoklonná	jednoklonný	k2eAgFnSc1d1
tělesně	tělesně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
γ	γ	k1gFnSc1
kosočtverečná	kosočtverečný	k2eAgFnSc1d1
plošně	plošně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
δ	δ	k1gFnSc1
krychlová	krychlový	k2eAgFnSc1d1
plošně	plošně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
δ	δ	k?
<g/>
´	´	k?
<g/>
-modifikace	-modifikace	k1gFnSc1
čtverečná	čtverečný	k2eAgFnSc1d1
tělesně	tělesně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
ε	ε	k1gFnSc1
krychlová	krychlový	k2eAgFnSc1d1
tělesně	tělesně	k6eAd1
centrovaná	centrovaný	k2eAgFnSc1d1
</s>
<s>
Molární	molární	k2eAgInSc1d1
objem	objem	k1gInSc1
</s>
<s>
12,29	12,29	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
m	m	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
změny	změna	k1gFnSc2
modifikace	modifikace	k1gFnSc2
</s>
<s>
122	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
α	α	k?
→	→	k?
β	β	k?
<g/>
)	)	kIx)
203	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
β	β	k?
→	→	k?
γ	γ	k?
<g/>
)	)	kIx)
317	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
γ	γ	k?
→	→	k?
δ	δ	k?
<g/>
)	)	kIx)
453	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
δ	δ	k?
→	→	k?
δ	δ	k?
<g/>
´	´	k?
<g/>
)	)	kIx)
477	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
δ	δ	k?
<g/>
´	´	k?
→	→	k?
ε	ε	k?
<g/>
)	)	kIx)
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
395,15	395,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
19,816	19,816	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
α	α	k?
<g/>
,	,	kIx,
21	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
17,77	17,77	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
β	β	k?
<g/>
,	,	kIx,
150	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
17,19	17,19	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
mod.	mod.	k?
γ	γ	k?
<g/>
,	,	kIx,
210	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
15,92	15,92	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
δ	δ	k?
<g/>
,	,	kIx,
320	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
15,99	15,99	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
δ	δ	k?
<g/>
´	´	k?
<g/>
,	,	kIx,
465	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
16,48	16,48	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
ε	ε	k?
<g/>
,	,	kIx,
500	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
<g/>
16,185	16,185	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
950	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Povrchové	povrchový	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
</s>
<s>
550	#num#	k4
±	±	k?
55	#num#	k4
mN	mN	k?
<g/>
/	/	kIx~
<g/>
m	m	kA
(	(	kIx(
<g/>
při	při	k7c6
tt	tt	k?
<g/>
)	)	kIx)
</s>
<s>
Rychlost	rychlost	k1gFnSc1
zvuku	zvuk	k1gInSc2
</s>
<s>
2	#num#	k4
260	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
0	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
6,16	6,16	k4
W	W	kA
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
6,70	6,70	k4
W	W	kA
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
77	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
7,90	7,90	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Součinitel	součinitel	k1gInSc1
délkové	délkový	k2eAgFnSc2d1
roztažnosti	roztažnost	k1gFnSc2
</s>
<s>
54	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Molární	molární	k2eAgFnSc1d1
atomizační	atomizační	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
</s>
<s>
364,4	364,4	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
51,5	51,5	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
mod.	mod.	k?
α	α	k?
<g/>
)	)	kIx)
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
641	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
914,15	914,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
3	#num#	k4
232	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
3	#num#	k4
505,15	505,15	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
tání	tání	k1gNnSc2
</s>
<s>
2,828	2,828	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Specifické	specifický	k2eAgNnSc4d1
teplo	teplo	k1gNnSc4
varu	var	k1gInSc2
</s>
<s>
333,5	333,5	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
320	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Entalpie	entalpie	k1gFnSc1
změny	změna	k1gFnSc2
modifikace	modifikace	k1gFnSc2
Δ	Δ	k?
<g/>
→	→	k?
<g/>
β	β	k?
</s>
<s>
3,77	3,77	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
α	α	k?
→	→	k?
β	β	k?
<g/>
)	)	kIx)
0,669	0,669	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
β	β	k?
→	→	k?
γ	γ	k?
<g/>
)	)	kIx)
0,619	0,619	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
γ	γ	k?
→	→	k?
δ	δ	k?
<g/>
)	)	kIx)
0,042	0,042	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
δ	δ	k?
→	→	k?
δ	δ	k?
<g/>
´	´	k?
<g/>
)	)	kIx)
1,858	1,858	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
δ	δ	k?
<g/>
´	´	k?
→	→	k?
ε	ε	k?
<g/>
)	)	kIx)
</s>
<s>
Molární	molární	k2eAgFnSc1d1
tepelná	tepelný	k2eAgFnSc1d1
kapacita	kapacita	k1gFnSc1
</s>
<s>
36,99	36,99	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
α	α	k?
mod.	mod.	k?
<g/>
,	,	kIx,
67	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
34,31	34,31	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
β	β	k?
mod.	mod.	k?
<g/>
,	,	kIx,
200	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
37,02	37,02	k4
J	J	kA
K	k	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
γ	γ	k?
mod.	mod.	k?
<g/>
,	,	kIx,
270	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
37,66	37,66	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
δ	δ	k?
mod.	mod.	k?
<g/>
)	)	kIx)
35,1	35,1	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
ε	ε	k?
mod.	mod.	k?
<g/>
)	)	kIx)
41,8	41,8	k4
J	J	kA
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
kapalina	kapalina	k1gFnSc1
<g/>
,	,	kIx,
675	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
141,4	141,4	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
8	#num#	k4
Ω	Ω	k?
m	m	kA
(	(	kIx(
<g/>
107	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
přechodu	přechod	k1gInSc2
do	do	k7c2
supravodivého	supravodivý	k2eAgInSc2d1
stavu	stav	k1gInSc2
</s>
<s>
0,5	0,5	k4
K	k	k7c3
</s>
<s>
Standardní	standardní	k2eAgInSc1d1
elektrodový	elektrodový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
Pu	Pu	k1gFnPc2
<g/>
3	#num#	k4
<g/>
+	+	kIx~
→	→	k?
Pu	Pu	k1gFnSc7
<g/>
0	#num#	k4
<g/>
)	)	kIx)
-2,031	-2,031	k4
V	V	kA
(	(	kIx(
<g/>
Pu	Pu	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
→	→	k?
Pu	Pu	k1gFnSc7
<g/>
3	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
1,006	1,006	k4
V	V	kA
(	(	kIx(
<g/>
Pu	Pu	k1gFnSc1
<g/>
5	#num#	k4
<g/>
+	+	kIx~
→	→	k?
Pu	Pu	k1gFnSc7
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
1,099	1,099	k4
V	v	k7c6
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
paramagnetický	paramagnetický	k2eAgInSc1d1
</s>
<s>
Měrná	měrný	k2eAgFnSc1d1
magnetická	magnetický	k2eAgFnSc1d1
susceptibilita	susceptibilita	k1gFnSc1
</s>
<s>
2,52	2,52	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
cm	cm	kA
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
g	g	kA
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Radioaktivní	radioaktivní	k2eAgFnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sm	Sm	k?
<g/>
⋏	⋏	k?
</s>
<s>
Neptunium	neptunium	k1gNnSc1
≺	≺	k?
<g/>
Pu	Pu	k1gMnSc2
<g/>
≻	≻	k?
Americium	americium	k1gNnSc1
</s>
<s>
Plutonium	plutonium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Pu	Pu	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šestý	šestý	k4xOgMnSc1
člen	člen	k1gMnSc1
z	z	k7c2
řady	řada	k1gFnSc2
aktinoidů	aktinoid	k1gInPc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
transuran	transuran	k1gInSc1
<g/>
,	,	kIx,
radioaktivní	radioaktivní	k2eAgInSc1d1
<g/>
,	,	kIx,
řetězovou	řetězový	k2eAgFnSc7d1
reakcí	reakce	k1gFnSc7
štěpitelný	štěpitelný	k2eAgInSc1d1
<g/>
,	,	kIx,
toxický	toxický	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
připravovaný	připravovaný	k2eAgInSc1d1
uměle	uměle	k6eAd1
bombardováním	bombardování	k1gNnSc7
uranu	uran	k1gInSc2
v	v	k7c6
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
<g/>
,	,	kIx,
především	především	k9
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
atomových	atomový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
využitelné	využitelný	k2eAgNnSc1d1
rovněž	rovněž	k9
jako	jako	k8xS,k8xC
palivo	palivo	k1gNnSc4
pro	pro	k7c4
jaderné	jaderný	k2eAgInPc4d1
reaktory	reaktor	k1gInPc4
a	a	k8xC
jako	jako	k9
zdroj	zdroj	k1gInSc4
energie	energie	k1gFnSc2
pro	pro	k7c4
radioizotopový	radioizotopový	k2eAgInSc4d1
termoelektrický	termoelektrický	k2eAgInSc4d1
generátor	generátor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Zabarvení	zabarvení	k1gNnSc1
roztoků	roztok	k1gInPc2
solí	sůl	k1gFnPc2
plutonia	plutonium	k1gNnSc2
v	v	k7c6
různých	různý	k2eAgNnPc6d1
mocenstvích	mocenství	k1gNnPc6
</s>
<s>
Plutonium	plutonium	k1gNnSc1
je	být	k5eAaImIp3nS
radioaktivní	radioaktivní	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
stříbřitě	stříbřitě	k6eAd1
bílé	bílý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
působením	působení	k1gNnSc7
vzdušného	vzdušný	k2eAgInSc2d1
kyslíku	kyslík	k1gInSc2
mění	měnit	k5eAaImIp3nS
na	na	k7c4
šedavou	šedavý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čisté	čistá	k1gFnSc3
plutonium	plutonium	k1gNnSc1
je	být	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
uran	uran	k1gInSc4
ve	v	k7c6
formě	forma	k1gFnSc6
hoblin	hoblina	k1gFnPc2
a	a	k8xC
pilin	pilina	k1gFnPc2
samozápalné	samozápalný	k2eAgNnSc1d1
<g/>
,	,	kIx,
proto	proto	k8xC
jeho	jeho	k3xOp3gNnSc4
zpracování	zpracování	k1gNnSc4
vyžaduje	vyžadovat	k5eAaImIp3nS
extrémní	extrémní	k2eAgFnSc1d1
opatrnost	opatrnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
mocenství	mocenství	k1gNnSc6
od	od	k7c2
Pu	Pu	k1gFnSc2
<g/>
+	+	kIx~
<g/>
3	#num#	k4
po	po	k7c6
Pu	Pu	k1gFnSc6
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
stálejší	stálý	k2eAgFnPc1d2
jsou	být	k5eAaImIp3nP
sloučeniny	sloučenina	k1gFnPc1
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
valencí	valence	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soli	sůl	k1gFnSc2
plutonia	plutonium	k1gNnSc2
vykazují	vykazovat	k5eAaImIp3nP
v	v	k7c6
roztoku	roztok	k1gInSc6
rozdílné	rozdílný	k2eAgNnSc1d1
zabarvení	zabarvení	k1gNnSc1
podle	podle	k7c2
mocenství	mocenství	k1gNnSc2
plutoniového	plutoniový	k2eAgInSc2d1
iontu	ion	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejběžnější	běžný	k2eAgFnSc1d3
oxidační	oxidační	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
plutonia	plutonium	k1gNnSc2
je	být	k5eAaImIp3nS
+	+	kIx~
<g/>
IV	IV	kA
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
oxidační	oxidační	k2eAgFnSc1d1
stupni	stupeň	k1gInSc3
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
plutonium	plutonium	k1gNnSc4
amfoterní	amfoterní	k2eAgInSc1d1
oxid	oxid	k1gInSc1
plutoničitý	plutoničitý	k2eAgMnSc1d1
PuO	PuO	k1gMnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
oxid	oxid	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
roztocích	roztok	k1gInPc6
kyselin	kyselina	k1gFnPc2
rozpouští	rozpouštět	k5eAaImIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
plutoničitých	plutoničitý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
a	a	k8xC
v	v	k7c6
zásadách	zásada	k1gFnPc6
za	za	k7c2
vzniku	vznik	k1gInSc2
komplexního	komplexní	k2eAgInSc2d1
aniontu	anion	k1gInSc2
a	a	k8xC
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
plutoničitany	plutoničitan	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plutoničité	Plutoničitý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
nejsou	být	k5eNaImIp3nP
v	v	k7c6
roztocích	roztok	k1gInPc6
úplně	úplně	k6eAd1
stabilní	stabilní	k2eAgFnSc1d1
a	a	k8xC
částečně	částečně	k6eAd1
nebo	nebo	k8xC
úplně	úplně	k6eAd1
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
potlačit	potlačit	k5eAaPmF
snížením	snížení	k1gNnSc7
pH	ph	kA
(	(	kIx(
<g/>
přidáním	přidání	k1gNnSc7
kyseliny	kyselina	k1gFnSc2
<g/>
)	)	kIx)
do	do	k7c2
roztoku	roztok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1
kov	kov	k1gInSc1
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
redukcí	redukce	k1gFnSc7
fluoridu	fluorid	k1gInSc2
plutonia	plutonium	k1gNnSc2
kovovým	kovový	k2eAgNnSc7d1
lithiem	lithium	k1gNnSc7
nebo	nebo	k8xC
baryem	baryum	k1gNnSc7
při	při	k7c6
teplotě	teplota	k1gFnSc6
kolem	kolem	k7c2
1200	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
PuF	puf	k1gInSc1
<g/>
6	#num#	k4
+	+	kIx~
6	#num#	k4
Li	li	k8xS
→	→	k?
Pu	Pu	k1gFnSc1
+	+	kIx~
6	#num#	k4
LiF	LiF	k1gFnPc2
</s>
<s>
PuF	puf	k1gInSc1
<g/>
6	#num#	k4
+	+	kIx~
3	#num#	k4
Ba	ba	k9
→	→	k?
Pu	Pu	k1gFnSc1
+	+	kIx~
3	#num#	k4
BaF	baf	k0
<g/>
2	#num#	k4
</s>
<s>
Historie	historie	k1gFnSc1
objevu	objev	k1gInSc2
</s>
<s>
Plutonium	plutonium	k1gNnSc1
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
připraveno	připravit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1940	#num#	k4
dvěma	dva	k4xCgInPc7
vědeckými	vědecký	k2eAgInPc7d1
týmy	tým	k1gInPc7
bombardováním	bombardování	k1gNnPc3
238U	238U	k4
neutrony	neutron	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jaderné	jaderný	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
v	v	k7c4
Berkeley	Berkeley	k1gInPc4
na	na	k7c6
kalifornské	kalifornský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
ho	on	k3xPp3gNnSc2
připravili	připravit	k5eAaPmAgMnP
Edwin	Edwin	k1gInSc4
M.	M.	kA
McMillan	McMillan	k1gInSc1
a	a	k8xC
Philip	Philip	k1gInSc1
Abelson	Abelsona	k1gFnPc2
a	a	k8xC
v	v	k7c6
britské	britský	k2eAgFnSc6d1
Cambridgi	Cambridge	k1gFnSc6
ohlásili	ohlásit	k5eAaPmAgMnP
jeho	jeho	k3xOp3gFnSc4
přípravu	příprava	k1gFnSc4
Norman	Norman	k1gMnSc1
Feather	Feathra	k1gFnPc2
a	a	k8xC
Egon	Egon	k1gMnSc1
Bretscher	Bretschra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
a	a	k8xC
izolace	izolace	k1gFnSc1
čistého	čistý	k2eAgInSc2d1
kovu	kov	k1gInSc2
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
23	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1941	#num#	k4
v	v	k7c4
Berkeley	Berkele	k1gMnPc4
bombardováním	bombardování	k1gNnSc7
uranu	uran	k1gInSc2
jádry	jádro	k1gNnPc7
deuteria	deuterium	k1gNnSc2
v	v	k7c6
cyklotronu	cyklotron	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
autoři	autor	k1gMnPc1
tohoto	tento	k3xDgInSc2
experimentu	experiment	k1gInSc2
jsou	být	k5eAaImIp3nP
označováni	označovat	k5eAaImNgMnP
Glenn	Glenna	k1gFnPc2
T.	T.	kA
Seaborg	Seaborg	k1gMnSc1
<g/>
,	,	kIx,
Edwin	Edwin	k1gMnSc1
McMillan	McMillan	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
W.	W.	kA
Kennedy	Kenneda	k1gMnSc2
a	a	k8xC
A.	A.	kA
C.	C.	kA
Wahl	Wahl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
probíhající	probíhající	k2eAgFnSc3d1
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
byly	být	k5eAaImAgInP
výsledky	výsledek	k1gInPc1
tohoto	tento	k3xDgNnSc2
bádání	bádání	k1gNnSc2
udržovány	udržovat	k5eAaImNgFnP
v	v	k7c4
tajnosti	tajnost	k1gFnPc4
<g/>
,	,	kIx,
zvláště	zvláště	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
atomových	atomový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
svržených	svržený	k2eAgInPc2d1
USA	USA	kA
na	na	k7c4
Japonsko	Japonsko	k1gNnSc4
byla	být	k5eAaImAgFnS
vyrobena	vyroben	k2eAgFnSc1d1
právě	právě	k9
z	z	k7c2
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
a	a	k8xC
izotopy	izotop	k1gInPc1
</s>
<s>
Plutonium	plutonium	k1gNnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
uměle	uměle	k6eAd1
připravené	připravený	k2eAgInPc4d1
prvky	prvek	k1gInPc4
a	a	k8xC
v	v	k7c6
přírodě	příroda	k1gFnSc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
jen	jen	k9
se	s	k7c7
skutečně	skutečně	k6eAd1
ultrastopovými	ultrastopový	k2eAgNnPc7d1
množstvími	množství	k1gNnPc7
v	v	k7c6
uranových	uranový	k2eAgFnPc6d1
rudách	ruda	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
mohou	moct	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc4d1
atomy	atom	k1gInPc4
vzniknout	vzniknout	k5eAaPmF
z	z	k7c2
238U	238U	k4
po	po	k7c6
záchytu	záchyt	k1gInSc6
neutronu	neutron	k1gInSc6
a	a	k8xC
následných	následný	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
rozpadech	rozpad	k1gInPc6
β	β	k?
</s>
<s>
Mezi	mezi	k7c7
20	#num#	k4
známými	známý	k2eAgInPc7d1
izotopy	izotop	k1gInPc7
plutonia	plutonium	k1gNnSc2
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
vykazují	vykazovat	k5eAaImIp3nP
dostatečně	dostatečně	k6eAd1
velký	velký	k2eAgInSc4d1
poločas	poločas	k1gInSc4
rozpadu	rozpad	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
prakticky	prakticky	k6eAd1
využít	využít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdelší	dlouhý	k2eAgInSc4d3
poločas	poločas	k1gInSc4
(	(	kIx(
<g/>
asi	asi	k9
80	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
244	#num#	k4
<g/>
Pu	Pu	k1gMnPc2
<g/>
,	,	kIx,
nejdůležitější	důležitý	k2eAgInSc4d3
izotop	izotop	k1gInSc4
239	#num#	k4
<g/>
Pu	Pu	k1gMnSc1
se	se	k3xPyFc4
rozpadá	rozpadat	k5eAaPmIp3nS,k5eAaImIp3nS
s	s	k7c7
poločasem	poločas	k1gInSc7
24	#num#	k4
110	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
240	#num#	k4
<g/>
Pu	Pu	k1gMnPc2
6561	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
241	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
14,325	14,325	k4
roků	rok	k1gInPc2
<g/>
,	,	kIx,
242	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
375	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
238	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
má	mít	k5eAaImIp3nS
poločas	poločas	k1gInSc1
rozpadu	rozpad	k1gInSc6
87,7	87,7	k4
let	léto	k1gNnPc2
<g/>
;	;	kIx,
existuje	existovat	k5eAaImIp3nS
však	však	k9
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dalších	další	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Izotoppoločas	Izotoppoločas	k1gMnSc1
rozpadudruh	rozpadudruh	k1gMnSc1
rozpaduprodukt	rozpaduprodukt	k1gInSc4
rozpadu	rozpad	k1gInSc2
</s>
<s>
228	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
1,1	1,1	k4
sα	sα	k?
<g/>
224	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
229	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
67	#num#	k4
sα	sα	k?
(	(	kIx(
<g/>
50	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
ε	ε	k?
(	(	kIx(
<g/>
50	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
225	#num#	k4
<g/>
U	U	kA
/	/	kIx~
229	#num#	k4
<g/>
Np	Np	k1gFnPc2
</s>
<s>
230	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
102	#num#	k4
sα	sα	k?
<g/>
226	#num#	k4
<g/>
U	u	k7c2
</s>
<s>
231	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
8,6	8,6	k4
minε	minε	k?
(	(	kIx(
<g/>
90	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
231	#num#	k4
<g/>
Np	Np	k1gFnPc2
/	/	kIx~
227U	227U	k4
</s>
<s>
232	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
33,8	33,8	k4
minε	minε	k?
(	(	kIx(
<g/>
90	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
232	#num#	k4
<g/>
Np	Np	k1gFnPc2
/	/	kIx~
228U	228U	k4
</s>
<s>
233	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
20,9	20,9	k4
minε	minε	k?
(	(	kIx(
<g/>
99,88	99,88	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
0,12	0,12	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
233	#num#	k4
<g/>
Np	Np	k1gFnPc2
/	/	kIx~
229U	229U	k4
</s>
<s>
234	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
8,8	8,8	k4
hε	hε	k?
(	(	kIx(
<g/>
94	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
6	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
234	#num#	k4
<g/>
Np	Np	k1gFnPc2
/	/	kIx~
230U	230U	k4
</s>
<s>
235	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
25,3	25,3	k4
minε	minε	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
2,8	2,8	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
235	#num#	k4
<g/>
Np	Np	k1gFnPc2
/	/	kIx~
231U	231U	k4
</s>
<s>
236	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
2,858	2,858	k4
rα	rα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
1,9	1,9	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
232	#num#	k4
<g/>
U	U	kA
/	/	kIx~
různé	různý	k2eAgFnPc1d1
</s>
<s>
237	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
45,64	45,64	k4
dε	dε	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
4,2	4,2	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
237	#num#	k4
<g/>
Np	Np	k1gFnPc2
<g/>
/	/	kIx~
233U	233U	k4
</s>
<s>
238	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
87,7	87,7	k4
rα	rα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
1,9	1,9	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
7	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
234	#num#	k4
<g/>
U	U	kA
/	/	kIx~
různé	různý	k2eAgFnPc1d1
</s>
<s>
239	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
24	#num#	k4
110	#num#	k4
rα	rα	k?
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
10	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
235	#num#	k4
<g/>
U	U	kA
/	/	kIx~
různé	různý	k2eAgFnPc1d1
</s>
<s>
240	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
6	#num#	k4
561	#num#	k4
rα	rα	k?
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
5,7	5,7	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
6	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
236	#num#	k4
<g/>
U	U	kA
/	/	kIx~
různé	různý	k2eAgFnPc1d1
</s>
<s>
241	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
14,329	14,329	k4
rβ	rβ	k?
<g/>
−	−	k?
(	(	kIx(
<g/>
100,00	100,00	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
α	α	k?
(	(	kIx(
<g/>
2,5	2,5	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
3	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
SF	SF	kA
(	(	kIx(
<g/>
<	<	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
14	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
241	#num#	k4
<g/>
Am	Am	k1gFnPc2
/	/	kIx~
237U	237U	k4
/	/	kIx~
různé	různý	k2eAgInPc1d1
</s>
<s>
242	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
375	#num#	k4
000	#num#	k4
rα	rα	k?
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
5,5	5,5	k4
<g/>
×	×	k?
<g/>
10	#num#	k4
<g/>
−	−	k?
<g/>
4	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
238	#num#	k4
<g/>
U	U	kA
/	/	kIx~
různé	různý	k2eAgFnPc1d1
</s>
<s>
243	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
4,956	4,956	k4
hβ	hβ	k?
<g/>
−	−	k?
<g/>
243	#num#	k4
<g/>
Am	Am	k1gFnSc1
</s>
<s>
244	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
8	#num#	k4
<g/>
×	×	k?
<g/>
107	#num#	k4
rα	rα	k?
(	(	kIx(
<g/>
99,88	99,88	k4
%	%	kIx~
<g/>
)	)	kIx)
/	/	kIx~
SF	SF	kA
(	(	kIx(
<g/>
0,12	0,12	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
240	#num#	k4
<g/>
U	U	kA
/	/	kIx~
různé	různý	k2eAgFnPc1d1
</s>
<s>
245	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
10,5	10,5	k4
hβ	hβ	k?
<g/>
−	−	k?
<g/>
245	#num#	k4
<g/>
Am	Am	k1gFnPc2
</s>
<s>
246	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
10,84	10,84	k4
dβ	dβ	k?
<g/>
−	−	k?
<g/>
246	#num#	k4
<g/>
Am	Am	k1gFnPc2
</s>
<s>
247	#num#	k4
<g/>
Pu	Pu	k1gFnSc1
<g/>
2,27	2,27	k4
dβ	dβ	k?
<g/>
−	−	k?
<g/>
247	#num#	k4
<g/>
Am	Am	k1gFnSc1
</s>
<s>
Výroba	výroba	k1gFnSc1
a	a	k8xC
využití	využití	k1gNnSc1
</s>
<s>
Jaderné	jaderný	k2eAgFnPc1d1
zbraně	zbraň	k1gFnPc1
</s>
<s>
Výbuch	výbuch	k1gInSc1
plutoniové	plutoniový	k2eAgFnSc2d1
jaderné	jaderný	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
svržené	svržený	k2eAgFnSc2d1
na	na	k7c6
Nagasaki	Nagasaki	k1gNnSc6
</s>
<s>
Plutonium	plutonium	k1gNnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
nejvíce	hodně	k6eAd3,k6eAd1
vyráběným	vyráběný	k2eAgInSc7d1
umělým	umělý	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
izotop	izotop	k1gInSc1
239	#num#	k4
<g/>
Pu	Pu	k1gMnPc2
je	být	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
atomové	atomový	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
případě	případ	k1gInSc6
235U	235U	k4
dochází	docházet	k5eAaImIp3nS
při	při	k7c6
nahromadění	nahromadění	k1gNnSc6
větších	veliký	k2eAgNnPc2d2
kvant	kvantum	k1gNnPc2
čistého	čistý	k2eAgInSc2d1
izotopu	izotop	k1gInSc2
k	k	k7c3
nastartování	nastartování	k1gNnSc3
řetězové	řetězový	k2eAgFnSc2d1
štěpné	štěpný	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
po	po	k7c6
rozpadu	rozpad	k1gInSc6
jednoho	jeden	k4xCgNnSc2
atomového	atomový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
vznikají	vznikat	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
tři	tři	k4xCgInPc1
neutrony	neutron	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
působí	působit	k5eAaImIp3nP
rozpady	rozpad	k1gInPc4
dalších	další	k2eAgFnPc2d1
okolních	okolní	k2eAgFnPc2d1
jader	jádro	k1gNnPc2
a	a	k8xC
rozpad	rozpad	k1gInSc1
se	se	k3xPyFc4
nekontrolovaně	kontrolovaně	k6eNd1
rozrůstá	rozrůstat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritické	kritický	k2eAgFnPc4d1
množství	množství	k1gNnSc1
čistého	čistý	k2eAgNnSc2d1
kovového	kovový	k2eAgNnSc2d1
plutonia	plutonium	k1gNnSc2
239	#num#	k4
<g/>
Pu	Pu	k1gMnPc7
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
10,5	10,5	k4
kg	kg	kA
<g/>
,	,	kIx,
s	s	k7c7
použitím	použití	k1gNnSc7
neutronového	neutronový	k2eAgInSc2d1
odrážeče	odrážeč	k1gInSc2
lze	lze	k6eAd1
toto	tento	k3xDgNnSc4
množství	množství	k1gNnSc4
snížit	snížit	k5eAaPmF
až	až	k9
kolem	kolem	k7c2
2,5	2,5	k4
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plutoniová	Plutoniový	k2eAgFnSc1d1
jaderná	jaderný	k2eAgFnSc1d1
puma	puma	k1gFnSc1
má	mít	k5eAaImIp3nS
sílu	síla	k1gFnSc4
výbuchu	výbuch	k1gInSc2
přibližně	přibližně	k6eAd1
20	#num#	k4
kilotun	kilotuna	k1gFnPc2
TNT	TNT	kA
na	na	k7c4
každý	každý	k3xTgInSc4
kilogram	kilogram	k1gInSc4
rozštěpeného	rozštěpený	k2eAgNnSc2d1
plutonia	plutonium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Princip	princip	k1gInSc1
výroby	výroba	k1gFnSc2
239	#num#	k4
<g/>
Pu	Pu	k1gMnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
reakci	reakce	k1gFnSc6
238U	238U	k4
s	s	k7c7
neutronem	neutron	k1gInSc7
za	za	k7c2
vzniku	vznik	k1gInSc2
239U	239U	k4
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádro	jádro	k1gNnSc1
239U	239U	k4
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
nestabilní	stabilní	k2eNgInSc1d1
a	a	k8xC
rozpadem	rozpad	k1gInSc7
β	β	k?
rychle	rychle	k6eAd1
vzniká	vznikat	k5eAaImIp3nS
izotop	izotop	k1gInSc4
neptunia	neptunium	k1gNnSc2
239	#num#	k4
<g/>
Np	Np	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
opět	opět	k6eAd1
rychle	rychle	k6eAd1
dalším	další	k2eAgInSc7d1
β	β	k1gInSc7
mění	měnit	k5eAaImIp3nS
na	na	k7c4
239	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
izotop	izotop	k1gInSc1
plutonia	plutonium	k1gNnSc2
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
jako	jako	k9
α	α	k?
zářič	zářič	k1gInSc1
a	a	k8xC
relativně	relativně	k6eAd1
snadno	snadno	k6eAd1
se	se	k3xPyFc4
dále	daleko	k6eAd2
zpracovává	zpracovávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
23892	#num#	k4
U	u	k7c2
+	+	kIx~
10	#num#	k4
n	n	k0
→	→	k?
23992	#num#	k4
U	u	k7c2
→	→	k?
23993	#num#	k4
Np	Np	k1gFnPc2
+	+	kIx~
0	#num#	k4
<g/>
−	−	k?
<g/>
1	#num#	k4
e	e	k0
→	→	k?
23994	#num#	k4
Pu	Pu	k1gFnPc2
+	+	kIx~
0	#num#	k4
<g/>
−	−	k?
<g/>
1	#num#	k4
e	e	k0
</s>
<s>
Při	při	k7c6
výrobě	výroba	k1gFnSc6
izotopu	izotop	k1gInSc2
238	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
se	se	k3xPyFc4
uran	uran	k1gInSc1
238U	238U	k4
bombarduje	bombardovat	k5eAaImIp3nS
jádry	jádro	k1gNnPc7
deuteria	deuterium	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
23892	#num#	k4
U	u	k7c2
+	+	kIx~
21	#num#	k4
D	D	kA
→	→	k?
23893	#num#	k4
Np	Np	k1gFnPc2
+	+	kIx~
2	#num#	k4
10	#num#	k4
n	n	k0
→	→	k?
23894	#num#	k4
Pu	Pu	k1gFnPc2
+	+	kIx~
0	#num#	k4
<g/>
−	−	k?
<g/>
1	#num#	k4
e	e	k0
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
jádro	jádro	k1gNnSc1
239	#num#	k4
<g/>
Pu	Pu	k1gMnPc2
zachytí	zachytit	k5eAaPmIp3nS
další	další	k2eAgInSc4d1
neutron	neutron	k1gInSc4
<g/>
,	,	kIx,
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
přeměně	přeměna	k1gFnSc3
na	na	k7c4
240	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
izotop	izotop	k1gInSc1
je	být	k5eAaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
další	další	k2eAgFnSc2d1
manipulace	manipulace	k1gFnSc2
a	a	k8xC
zpracování	zpracování	k1gNnSc2
rizikový	rizikový	k2eAgMnSc1d1
protože	protože	k8xS
je	být	k5eAaImIp3nS
β	β	k?
<g/>
–	–	k?
<g/>
zářičem	zářič	k1gInSc7
a	a	k8xC
zároveň	zároveň	k6eAd1
není	být	k5eNaImIp3nS
vhodný	vhodný	k2eAgInSc1d1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
atomové	atomový	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
izotopy	izotop	k1gInPc1
239	#num#	k4
<g/>
Pu	Pu	k1gFnSc2
a	a	k8xC
240	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
lze	lze	k6eAd1
jen	jen	k9
velmi	velmi	k6eAd1
obtížně	obtížně	k6eAd1
oddělovat	oddělovat	k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
při	při	k7c6
výrobě	výroba	k1gFnSc6
239	#num#	k4
<g/>
Pu	Pu	k1gMnPc2
z	z	k7c2
238U	238U	k4
v	v	k7c6
jaderném	jaderný	k2eAgInSc6d1
reaktoru	reaktor	k1gInSc6
nutno	nutno	k6eAd1
pečlivě	pečlivě	k6eAd1
volit	volit	k5eAaImF
dobu	doba	k1gFnSc4
setrvání	setrvání	k1gNnSc4
238U	238U	k4
v	v	k7c6
reaktoru	reaktor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
dlouhodobém	dlouhodobý	k2eAgNnSc6d1
ozařování	ozařování	k1gNnSc6
sice	sice	k8xC
roste	růst	k5eAaImIp3nS
množství	množství	k1gNnSc1
239	#num#	k4
<g/>
Pu	Pu	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
současně	současně	k6eAd1
narůstá	narůstat	k5eAaImIp3nS
i	i	k9
podíl	podíl	k1gInSc4
nechtěného	chtěný	k2eNgInSc2d1
izotopu	izotop	k1gInSc2
240	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
ekonomicky	ekonomicky	k6eAd1
značně	značně	k6eAd1
nákladné	nákladný	k2eAgNnSc1d1
oddělovat	oddělovat	k5eAaImF
izotopy	izotop	k1gInPc4
plutonia	plutonium	k1gNnSc2
od	od	k7c2
zbytku	zbytek	k1gInSc2
jaderného	jaderný	k2eAgNnSc2d1
paliva	palivo	k1gNnSc2
z	z	k7c2
reaktoru	reaktor	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
proto	proto	k8xC
otázkou	otázka	k1gFnSc7
komplikovaných	komplikovaný	k2eAgInPc2d1
výpočtů	výpočet	k1gInPc2
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yQgNnSc6,k3yRgNnSc6,k3yIgNnSc6
stádiu	stádium	k1gNnSc6
výroby	výroba	k1gFnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
reakce	reakce	k1gFnSc1
přerušena	přerušen	k2eAgFnSc1d1
a	a	k8xC
materiál	materiál	k1gInSc1
chemicky	chemicky	k6eAd1
přepracován	přepracovat	k5eAaPmNgInS
na	na	k7c4
čisté	čistý	k2eAgNnSc4d1
plutonium	plutonium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
důležitou	důležitý	k2eAgFnSc7d1
proměnnou	proměnný	k2eAgFnSc7d1
veličinou	veličina	k1gFnSc7
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
pochopitelně	pochopitelně	k6eAd1
i	i	k9
procentuální	procentuální	k2eAgFnSc1d1
vsázka	vsázka	k1gFnSc1
238U	238U	k4
do	do	k7c2
jaderné	jaderný	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc1d1
využití	využití	k1gNnSc1
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
případě	případ	k1gInSc6
uranu	uran	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
řetězová	řetězový	k2eAgFnSc1d1
reakce	reakce	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
po	po	k7c6
záchytu	záchyt	k1gInSc6
neutronu	neutron	k1gInSc2
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
rozpadu	rozpad	k1gInSc3
atomového	atomový	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
prvku	prvek	k1gInSc2
za	za	k7c4
uvolnění	uvolnění	k1gNnSc4
dalších	další	k2eAgInPc2d1
neutronů	neutron	k1gInPc2
a	a	k8xC
kinetické	kinetický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
štěpných	štěpný	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
,	,	kIx,
využita	využít	k5eAaPmNgFnS
i	i	k9
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
energie	energie	k1gFnSc2
v	v	k7c6
upravených	upravený	k2eAgInPc6d1
jaderných	jaderný	k2eAgInPc6d1
reaktorech	reaktor	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c6
světě	svět	k1gInSc6
několik	několik	k4yIc4
jaderných	jaderný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
na	na	k7c4
bázi	báze	k1gFnSc4
směsi	směs	k1gFnSc2
239	#num#	k4
<g/>
Pu	Pu	k1gMnPc2
a	a	k8xC
240	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
jsou	být	k5eAaImIp3nP
však	však	k9
tyto	tento	k3xDgInPc1
reaktory	reaktor	k1gInPc1
pokládány	pokládán	k2eAgInPc1d1
za	za	k7c4
méně	málo	k6eAd2
bezpečné	bezpečný	k2eAgFnPc4d1
než	než	k8xS
klasické	klasický	k2eAgFnPc4d1
uranové	uranový	k2eAgFnPc4d1
vzhledem	vzhledem	k7c3
k	k	k7c3
vysoké	vysoký	k2eAgFnSc3d1
toxicitě	toxicita	k1gFnSc3
plutonia	plutonium	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
sloučenin	sloučenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Izotop	izotop	k1gInSc1
238	#num#	k4
<g/>
Pu	Pu	k1gFnPc2
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
88	#num#	k4
let	léto	k1gNnPc2
slouží	sloužit	k5eAaImIp3nS
často	často	k6eAd1
jako	jako	k8xS,k8xC
energetický	energetický	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
především	především	k9
v	v	k7c6
kosmických	kosmický	k2eAgFnPc6d1
sondách	sonda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tzv.	tzv.	kA
radioizotopovém	radioizotopový	k2eAgInSc6d1
termoelektrickém	termoelektrický	k2eAgInSc6d1
generátoru	generátor	k1gInSc6
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
tepelná	tepelný	k2eAgFnSc1d1
energie	energie	k1gFnSc1
uvolněná	uvolněný	k2eAgFnSc1d1
samovolným	samovolný	k2eAgInSc7d1
jaderným	jaderný	k2eAgInSc7d1
rozpadem	rozpad	k1gInSc7
na	na	k7c4
elektrickou	elektrický	k2eAgFnSc4d1
pomocí	pomocí	k7c2
termočlánků	termočlánek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgInPc4
účely	účel	k1gInPc4
jsou	být	k5eAaImIp3nP
vhodné	vhodný	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
s	s	k7c7
poločasem	poločas	k1gInSc7
rozpadu	rozpad	k1gInSc2
v	v	k7c6
řádu	řád	k1gInSc6
desítek	desítka	k1gFnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
uvolněná	uvolněný	k2eAgFnSc1d1
energie	energie	k1gFnSc1
je	být	k5eAaImIp3nS
dostatečně	dostatečně	k6eAd1
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
prakticky	prakticky	k6eAd1
využita	využít	k5eAaPmNgFnS
a	a	k8xC
zároveň	zároveň	k6eAd1
zaručuje	zaručovat	k5eAaImIp3nS
použitelnost	použitelnost	k1gFnSc4
zdroje	zdroj	k1gInSc2
po	po	k7c4
dobu	doba	k1gFnSc4
minimálně	minimálně	k6eAd1
50	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Produkovaný	produkovaný	k2eAgInSc1d1
výkon	výkon	k1gInSc1
generátoru	generátor	k1gInSc2
dosahuje	dosahovat	k5eAaImIp3nS
stovek	stovka	k1gFnPc2
wattů	watt	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
postačuje	postačovat	k5eAaImIp3nS
pro	pro	k7c4
udržení	udržení	k1gNnSc4
provozu	provoz	k1gInSc2
základních	základní	k2eAgInPc2d1
elektrických	elektrický	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
vesmírné	vesmírný	k2eAgFnSc2d1
sondy	sonda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plutoniové	Plutoniový	k2eAgInPc1d1
generátory	generátor	k1gInPc1
zásobují	zásobovat	k5eAaImIp3nP
energií	energie	k1gFnSc7
např.	např.	kA
sondy	sonda	k1gFnPc4
Galileo	Galilea	k1gFnSc5
nebo	nebo	k8xC
Cassini	Cassin	k2eAgMnPc1d1
a	a	k8xC
udržovaly	udržovat	k5eAaImAgFnP
několik	několik	k4yIc4
let	léto	k1gNnPc2
v	v	k7c6
provozu	provoz	k1gInSc6
vědecké	vědecký	k2eAgInPc4d1
přístroje	přístroj	k1gInPc4
<g/>
,	,	kIx,
zanechané	zanechaný	k2eAgInPc4d1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
kosmonauty	kosmonaut	k1gMnPc7
v	v	k7c6
rámci	rámec	k1gInSc6
projektu	projekt	k1gInSc2
Apollo	Apollo	k1gMnSc1
<g/>
.	.	kIx.
238	#num#	k4
<g/>
PuO	PuO	k1gFnPc2
<g/>
2	#num#	k4
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
také	také	k9
jako	jako	k8xS,k8xC
palivo	palivo	k1gNnSc1
pro	pro	k7c4
sondu	sonda	k1gFnSc4
New	New	k1gFnSc2
Horizons	Horizonsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Spíše	spíše	k9
jako	jako	k8xS,k8xC
zajímavost	zajímavost	k1gFnSc1
může	moct	k5eAaImIp3nS
sloužit	sloužit	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
výše	vysoce	k6eAd2
uvedené	uvedený	k2eAgInPc1d1
generátory	generátor	k1gInPc1
elektrické	elektrický	k2eAgFnSc2d1
energie	energie	k1gFnSc2
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
energetický	energetický	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
pro	pro	k7c4
kardiostimulátory	kardiostimulátor	k1gInPc4
<g/>
,	,	kIx,
medicínské	medicínský	k2eAgInPc1d1
přístroje	přístroj	k1gInPc1
sloužící	sloužící	k2eAgInPc1d1
ke	k	k7c3
zklidnění	zklidnění	k1gNnSc3
srdečního	srdeční	k2eAgInSc2d1
rytmu	rytmus	k1gInSc2
u	u	k7c2
osob	osoba	k1gFnPc2
s	s	k7c7
rizikem	riziko	k1gNnSc7
infarktu	infarkt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
tyto	tento	k3xDgInPc4
účely	účel	k1gInPc4
používány	používán	k2eAgInPc4d1
především	především	k9
lithiové	lithiový	k2eAgFnSc2d1
baterie	baterie	k1gFnSc2
s	s	k7c7
dlouhou	dlouhý	k2eAgFnSc7d1
životností	životnost	k1gFnSc7
a	a	k8xC
indukčním	indukční	k2eAgNnSc7d1
dobíjením	dobíjení	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
před	před	k7c7
jejich	jejich	k3xOp3gInSc7
vynálezem	vynález	k1gInSc7
byl	být	k5eAaImAgInS
energetický	energetický	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
fungující	fungující	k2eAgInSc1d1
na	na	k7c6
principu	princip	k1gInSc6
jaderného	jaderný	k2eAgInSc2d1
rozpadu	rozpad	k1gInSc2
238	#num#	k4
<g/>
Pu	Pu	k1gFnSc7
přijatelnou	přijatelný	k2eAgFnSc7d1
alternativou	alternativa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Zdravotní	zdravotní	k2eAgNnPc1d1
rizika	riziko	k1gNnPc1
</s>
<s>
Plutonium	plutonium	k1gNnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
těžký	těžký	k2eAgInSc1d1
kov	kov	k1gInSc1
extrémně	extrémně	k6eAd1
toxický	toxický	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
lze	lze	k6eAd1
však	však	k9
u	u	k7c2
něho	on	k3xPp3gMnSc2
těžko	těžko	k6eAd1
rozlišit	rozlišit	k5eAaPmF
škodlivé	škodlivý	k2eAgInPc4d1
účinky	účinek	k1gInPc4
způsobené	způsobený	k2eAgNnSc4d1
radiací	radiace	k1gFnSc7
od	od	k7c2
škodlivých	škodlivý	k2eAgInPc2d1
účinků	účinek	k1gInPc2
chemických	chemický	k2eAgInPc2d1
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
často	často	k6eAd1
pokládáno	pokládat	k5eAaImNgNnS
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejtoxičtějších	toxický	k2eAgFnPc2d3
anorganických	anorganický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgInPc2
údajů	údaj	k1gInPc2
můžou	můžou	k?
být	být	k5eAaImF
už	už	k6eAd1
mikrogramová	mikrogramový	k2eAgNnPc4d1
množství	množství	k1gNnPc4
tohoto	tento	k3xDgInSc2
prvku	prvek	k1gInSc2
smrtelně	smrtelně	k6eAd1
jedovatá	jedovatý	k2eAgFnSc1d1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
dostanou	dostat	k5eAaPmIp3nP
do	do	k7c2
krevního	krevní	k2eAgInSc2d1
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
však	však	k9
považují	považovat	k5eAaImIp3nP
údaje	údaj	k1gInPc1
o	o	k7c6
extrémní	extrémní	k2eAgFnSc6d1
toxicitě	toxicita	k1gFnSc6
plutonia	plutonium	k1gNnSc2
za	za	k7c4
nadhodnocené	nadhodnocený	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Marhold	Marholda	k1gFnPc2
cituje	citovat	k5eAaBmIp3nS
z	z	k7c2
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
uvedena	uvést	k5eAaPmNgFnS
dávka	dávka	k1gFnSc1
LD50	LD50	k1gFnSc1
u	u	k7c2
psa	pes	k1gMnSc2
i.	i.	k?
v.	v.	k?
0,3	0,3	k4
mg	mg	kA
<g/>
/	/	kIx~
<g/>
kg	kg	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toxikologie	toxikologie	k1gFnSc1
zná	znát	k5eAaImIp3nS
i	i	k9
mnohem	mnohem	k6eAd1
prudší	prudký	k2eAgInPc1d2
jedy	jed	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
hlavní	hlavní	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
depozice	depozice	k1gFnSc1
v	v	k7c6
kostech	kost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Radiačně	radiačně	k6eAd1
nebezpečný	bezpečný	k2eNgInSc1d1
je	být	k5eAaImIp3nS
především	především	k9
izotop	izotop	k1gInSc1
241	#num#	k4
<g/>
Pu	Pu	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
jako	jako	k9
β	β	k1gMnSc1
má	mít	k5eAaImIp3nS
daleko	daleko	k6eAd1
negativnější	negativní	k2eAgInSc4d2
dopad	dopad	k1gInSc4
na	na	k7c4
lidské	lidský	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
než	než	k8xS
zbylé	zbylý	k2eAgInPc1d1
izotopy	izotop	k1gInPc1
plutonia	plutonium	k1gNnSc2
<g/>
,	,	kIx,
zářiče	zářič	k1gInSc2
α	α	k?
Je	být	k5eAaImIp3nS
pochopitelné	pochopitelný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
mimořádné	mimořádný	k2eAgNnSc1d1
nebezpečí	nebezpečí	k1gNnSc1
by	by	kYmCp3nS
hrozilo	hrozit	k5eAaImAgNnS
při	při	k7c6
nastartování	nastartování	k1gNnSc6
řetězové	řetězový	k2eAgFnSc2d1
štěpné	štěpný	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
plutonium	plutonium	k1gNnSc1
stává	stávat	k5eAaImIp3nS
zdrojem	zdroj	k1gInSc7
silného	silný	k2eAgInSc2d1
neutronového	neutronový	k2eAgInSc2d1
toku	tok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
však	však	k9
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
pouze	pouze	k6eAd1
při	při	k7c6
neopatrné	opatrný	k2eNgFnSc6d1
manipulaci	manipulace	k1gFnSc6
s	s	k7c7
většími	veliký	k2eAgInPc7d2
objemy	objem	k1gInPc7
čistého	čistý	k2eAgNnSc2d1
plutonia	plutonium	k1gNnSc2
<g/>
;	;	kIx,
v	v	k7c6
počátcích	počátek	k1gInPc6
výzkumu	výzkum	k1gInSc2
plutonia	plutonium	k1gNnSc2
v	v	k7c6
laboratořích	laboratoř	k1gFnPc6
v	v	k7c4
Berkeley	Berkelea	k1gFnPc4
skutečně	skutečně	k6eAd1
několikrát	několikrát	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vysoké	vysoký	k2eAgFnSc3d1
expozici	expozice	k1gFnSc3
pracovníků	pracovník	k1gMnPc2
neutrony	neutron	k1gInPc1
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
nehody	nehoda	k1gFnPc1
při	při	k7c6
experimentech	experiment	k1gInPc6
s	s	k7c7
kritickým	kritický	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1945	#num#	k4
nehoda	nehoda	k1gFnSc1
Harryho	Harry	k1gMnSc4
K.	K.	kA
Daghliana	Daghlian	k1gMnSc4
Jr	Jr	k1gMnSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1921	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1946	#num#	k4
nehoda	nehoda	k1gFnSc1
Louise	Louis	k1gMnSc2
P.	P.	kA
Slotina	Slotin	k1gMnSc2
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
těchto	tento	k3xDgFnPc6
nehodách	nehoda	k1gFnPc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vysokému	vysoký	k2eAgNnSc3d1
ozáření	ozáření	k1gNnSc3
menšího	malý	k2eAgInSc2d2
počtu	počet	k1gInSc2
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
H.	H.	kA
K.	K.	kA
Daghlian	Daghlian	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
26	#num#	k4
dní	den	k1gInPc2
po	po	k7c6
nehodě	nehoda	k1gFnSc6
<g/>
,	,	kIx,
L.	L.	kA
P.	P.	kA
Slotin	Slotin	k1gMnSc1
po	po	k7c6
9	#num#	k4
dnech	den	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
http://www.ptable.com/#Property/Valence	http://www.ptable.com/#Property/Valence	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwooda	k1gFnPc2
<g/>
,	,	kIx,
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc1
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
II	II	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Radioizotopový	radioizotopový	k2eAgInSc1d1
termoelektrický	termoelektrický	k2eAgInSc1d1
generátor	generátor	k1gInSc1
</s>
<s>
Uran	Uran	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
plutonium	plutonium	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
plutonium	plutonium	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
smíšené	smíšený	k2eAgNnSc4d1
oxidové	oxidový	k2eAgNnSc4d1
palivo	palivo	k1gNnSc4
(	(	kIx(
<g/>
MOX	MOX	kA
<g/>
)	)	kIx)
a	a	k8xC
kde	kde	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
nukleární	nukleární	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
Zpravodaj	zpravodaj	k1gInSc1
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
IARC	IARC	kA
MONOGRAPHS	MONOGRAPHS	kA
ON	on	k3xPp3gMnSc1
THE	THE	kA
EVALUATION	EVALUATION	kA
OF	OF	kA
CARCINOGENIC	CARCINOGENIC	kA
RISKS	RISKS	kA
TO	to	k9
HUMANS	HUMANS	kA
<g/>
,	,	kIx,
VOLUME	volum	k1gInSc5
78	#num#	k4
IONIZING	IONIZING	kA
RADIATION	RADIATION	kA
<g/>
,	,	kIx,
PART	part	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
SOME	SOME	kA
INTERNALLY	INTERNALLY	kA
DEPOSITED	DEPOSITED	kA
RADIONUCLIDES	RADIONUCLIDES	kA
IARC	IARC	kA
2001	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Izrael	Izrael	k1gInSc1
vyvinul	vyvinout	k5eAaPmAgInS
plutonium	plutonium	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
nelze	lze	k6eNd1
použít	použít	k5eAaPmF
na	na	k7c4
atomovou	atomový	k2eAgFnSc4d1
bombu	bomba	k1gFnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4136381-4	4136381-4	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5778	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85103582	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85103582	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
