<p>
<s>
Naše	náš	k3xOp1gNnSc1	náš
násilí	násilí	k1gNnSc1	násilí
a	a	k8xC	a
vaše	váš	k3xOp2gNnSc4	váš
násilí	násilí	k1gNnSc4	násilí
(	(	kIx(	(
<g/>
chorvatsky	chorvatsky	k6eAd1	chorvatsky
Naše	náš	k3xOp1gFnSc1	náš
nasilje	nasilje	k1gFnSc1	nasilje
i	i	k9	i
vaše	váš	k3xOp2gFnSc1	váš
nasilje	nasilje	k1gFnSc1	nasilje
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Naše	náš	k3xOp1gNnSc4	náš
násilí	násilí	k1gNnSc4	násilí
<g/>
,	,	kIx,	,
vaše	váš	k3xOp2gNnSc4	váš
násilí	násilí	k1gNnSc4	násilí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
chorvatského	chorvatský	k2eAgMnSc2d1	chorvatský
režiséra	režisér	k1gMnSc2	režisér
Olivera	Oliver	k1gMnSc2	Oliver
Frljiče	Frljič	k1gMnSc2	Frljič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
hra	hra	k1gFnSc1	hra
představena	představit	k5eAaPmNgFnS	představit
divadelním	divadelní	k2eAgInSc7d1	divadelní
souborem	soubor	k1gInSc7	soubor
Mladinsko	Mladinsko	k1gNnSc1	Mladinsko
Gledališče	Gledališč	k1gFnSc2	Gledališč
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Divadelní	divadelní	k2eAgInSc1d1	divadelní
svět	svět	k1gInSc1	svět
Brno	Brno	k1gNnSc1	Brno
2018	[number]	k4	2018
a	a	k8xC	a
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
jak	jak	k6eAd1	jak
demonstrace	demonstrace	k1gFnSc1	demonstrace
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
i	i	k8xC	i
jiných	jiný	k2eAgMnPc2d1	jiný
aktivistů	aktivista	k1gMnPc2	aktivista
před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
fyzické	fyzický	k2eAgNnSc1d1	fyzické
narušení	narušení	k1gNnSc1	narušení
průběhu	průběh	k1gInSc2	průběh
hry	hra	k1gFnSc2	hra
skupinou	skupina	k1gFnSc7	skupina
tzv.	tzv.	kA	tzv.
Slušných	slušný	k2eAgMnPc2d1	slušný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
uvedení	uvedení	k1gNnSc3	uvedení
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
i	i	k8xC	i
hry	hra	k1gFnSc2	hra
Prokletí	prokletí	k1gNnSc2	prokletí
od	od	k7c2	od
stejného	stejné	k1gNnSc2	stejné
režiséra	režisér	k1gMnSc4	režisér
podal	podat	k5eAaPmAgInS	podat
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Dominik	Dominik	k1gMnSc1	Dominik
Duka	Duka	k1gMnSc1	Duka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
advokátem	advokát	k1gMnSc7	advokát
Ronaldem	Ronald	k1gMnSc7	Ronald
Němcem	Němec	k1gMnSc7	Němec
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
brněnské	brněnský	k2eAgNnSc4d1	brněnské
Centrum	centrum	k1gNnSc4	centrum
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
hrubě	hrubě	k6eAd1	hrubě
dotknout	dotknout	k5eAaPmF	dotknout
jejich	jejich	k3xOp3gNnPc2	jejich
práv	právo	k1gNnPc2	právo
jako	jako	k8xS	jako
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
