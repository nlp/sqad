<s>
Kvůli	kvůli	k7c3	kvůli
uvedení	uvedení	k1gNnSc3	uvedení
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
i	i	k8xC	i
hry	hra	k1gFnSc2	hra
Prokletí	prokletí	k1gNnSc2	prokletí
od	od	k7c2	od
stejného	stejné	k1gNnSc2	stejné
režiséra	režisér	k1gMnSc4	režisér
podal	podat	k5eAaPmAgInS	podat
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2018	[number]	k4	2018
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Dominik	Dominik	k1gMnSc1	Dominik
Duka	Duka	k1gMnSc1	Duka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
advokátem	advokát	k1gMnSc7	advokát
Ronaldem	Ronald	k1gMnSc7	Ronald
Němcem	Němec	k1gMnSc7	Němec
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
brněnské	brněnský	k2eAgNnSc4d1	brněnské
Centrum	centrum	k1gNnSc4	centrum
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
hrubě	hrubě	k6eAd1	hrubě
dotknout	dotknout	k5eAaPmF	dotknout
jejich	jejich	k3xOp3gNnPc2	jejich
práv	právo	k1gNnPc2	právo
jako	jako	k8xC	jako
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
