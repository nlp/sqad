<p>
<s>
Dammám	Dammat	k5eAaPmIp1nS	Dammat
(	(	kIx(	(
<g/>
Ad	ad	k7c4	ad
Dammam	Dammam	k1gInSc4	Dammam
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
903	[number]	k4	903
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
sedmé	sedmý	k4xOgNnSc4	sedmý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Bahrajnem	Bahrajn	k1gInSc7	Bahrajn
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
známým	známý	k2eAgNnSc7d1	známé
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
centrem	centrum	k1gNnSc7	centrum
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
ropnému	ropný	k2eAgInSc3d1	ropný
průmyslu	průmysl	k1gInSc3	průmysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Dammámu	Dammám	k1gInSc2	Dammám
leží	ležet	k5eAaImIp3nS	ležet
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
Krále	Král	k1gMnSc2	Král
Fahda	Fahd	k1gMnSc2	Fahd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dammám	Damma	k1gFnPc3	Damma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
