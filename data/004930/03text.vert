<s>
Republika	republika	k1gFnSc1	republika
Dagestán	Dagestán	k1gInSc1	Dagestán
neboli	neboli	k8xC	neboli
Dagestánská	dagestánský	k2eAgFnSc1d1	Dagestánská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Р	Р	k?	Р
Д	Д	k?	Д
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejjižnější	jižní	k2eAgFnSc1d3	nejjižnější
autonomní	autonomní	k2eAgFnSc1d1	autonomní
republika	republika	k1gFnSc1	republika
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
Ázerbájdžánem	Ázerbájdžán	k1gInSc7	Ázerbájdžán
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Gruzií	Gruzie	k1gFnPc2	Gruzie
<g/>
;	;	kIx,	;
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
pak	pak	k6eAd1	pak
s	s	k7c7	s
Kalmyckou	kalmycký	k2eAgFnSc7d1	Kalmycká
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
Čečenskem	Čečensko	k1gNnSc7	Čečensko
a	a	k8xC	a
Stavropolským	stavropolský	k2eAgInSc7d1	stavropolský
krajem	kraj	k1gInSc7	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rozloha	rozloha	k1gFnSc1	rozloha
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
300	[number]	k4	300
km2	km2	k4	km2
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Dagestán	Dagestán	k1gInSc1	Dagestán
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
asijské	asijský	k2eAgFnSc6d1	asijská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
Kuma	kuma	k1gFnSc1	kuma
tvoří	tvořit	k5eAaImIp3nP	tvořit
jeho	jeho	k3xOp3gFnSc4	jeho
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
vedení	vedení	k1gNnSc4	vedení
evropské	evropský	k2eAgFnSc2d1	Evropská
hranice	hranice	k1gFnSc2	hranice
by	by	kYmCp3nS	by
však	však	k9	však
patřil	patřit	k5eAaImAgInS	patřit
ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
kavkazské	kavkazský	k2eAgFnPc4d1	kavkazská
republiky	republika	k1gFnPc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Machačkala	Machačkala	k1gFnSc1	Machačkala
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Dagestánu	Dagestán	k1gInSc2	Dagestán
je	být	k5eAaImIp3nS	být
obydlené	obydlený	k2eAgNnSc1d1	obydlené
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
Kavkazské	kavkazský	k2eAgFnSc2d1	kavkazská
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
mu	on	k3xPp3gMnSc3	on
vládli	vládnout	k5eAaImAgMnP	vládnout
Sásánovci	Sásánovec	k1gMnPc1	Sásánovec
<g/>
,	,	kIx,	,
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
chazarský	chazarský	k2eAgMnSc1d1	chazarský
chán	chán	k1gMnSc1	chán
<g/>
;	;	kIx,	;
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
chazarské	chazarský	k2eAgFnSc2d1	Chazarská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
969	[number]	k4	969
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
postupně	postupně	k6eAd1	postupně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
země	zem	k1gFnSc2	zem
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
ovládali	ovládat	k5eAaImAgMnP	ovládat
Seldžučtí	Seldžucký	k2eAgMnPc1d1	Seldžucký
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
avarský	avarský	k2eAgInSc1d1	avarský
chanát	chanát	k1gInSc1	chanát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vpádu	vpád	k1gInSc6	vpád
Mongolů	Mongol	k1gMnPc2	Mongol
byl	být	k5eAaImAgMnS	být
začleněn	začlenit	k5eAaPmNgMnS	začlenit
do	do	k7c2	do
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
hordy	horda	k1gFnSc2	horda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
úpadkem	úpadek	k1gInSc7	úpadek
mongolské	mongolský	k2eAgFnSc2d1	mongolská
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
oblast	oblast	k1gFnSc1	oblast
Dagestánu	Dagestán	k1gInSc2	Dagestán
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
sousední	sousední	k2eAgInSc1d1	sousední
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
předmětem	předmět	k1gInSc7	předmět
soupeření	soupeření	k1gNnSc1	soupeření
mezi	mezi	k7c7	mezi
Persií	Persie	k1gFnSc7	Persie
a	a	k8xC	a
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
(	(	kIx(	(
<g/>
z	z	k7c2	z
turečtiny	turečtina	k1gFnSc2	turečtina
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
perštiny	perština	k1gFnSc2	perština
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
území	území	k1gNnSc2	území
<g/>
:	:	kIx,	:
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
Země	země	k1gFnSc1	země
hor	hora	k1gFnPc2	hora
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
pronikalo	pronikat	k5eAaImAgNnS	pronikat
na	na	k7c6	na
území	území	k1gNnSc6	území
Dagestánu	Dagestán	k1gInSc2	Dagestán
i	i	k8xC	i
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jej	on	k3xPp3gMnSc4	on
roku	rok	k1gInSc3	rok
1813	[number]	k4	1813
i	i	k9	i
formálně	formálně	k6eAd1	formálně
připojilo	připojit	k5eAaPmAgNnS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tu	tu	k6eAd1	tu
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
řada	řada	k1gFnSc1	řada
protifeudálních	protifeudální	k2eAgNnPc2d1	protifeudální
povstání	povstání	k1gNnPc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
horské	horský	k2eAgFnSc2d1	horská
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
však	však	k9	však
Rusku	Rusko	k1gNnSc3	Rusko
podařilo	podařit	k5eAaPmAgNnS	podařit
až	až	k9	až
po	po	k7c6	po
potlačení	potlačení	k1gNnSc6	potlačení
Šamilova	Šamilův	k2eAgNnSc2d1	Šamilův
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostali	dostat	k5eAaPmAgMnP	dostat
bolševici	bolševik	k1gMnPc1	bolševik
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ustavena	ustavit	k5eAaPmNgFnS	ustavit
sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
říjnové	říjnový	k2eAgFnSc6d1	říjnová
revoluci	revoluce	k1gFnSc6	revoluce
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918-1920	[number]	k4	1918-1920
na	na	k7c6	na
území	území	k1gNnSc6	území
Dagestánu	Dagestán	k1gInSc2	Dagestán
řada	řada	k1gFnSc1	řada
státních	státní	k2eAgMnPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
vesměs	vesměs	k6eAd1	vesměs
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
německá	německý	k2eAgFnSc1d1	německá
<g/>
,	,	kIx,	,
turecká	turecký	k2eAgFnSc1d1	turecká
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
bílá	bílý	k2eAgNnPc4d1	bílé
<g/>
"	"	kIx"	"
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
bolševické	bolševický	k2eAgFnSc2d1	bolševická
moci	moc	k1gFnSc2	moc
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1921	[number]	k4	1921
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
Dagestánská	dagestánský	k2eAgFnSc1d1	Dagestánská
ASSR	ASSR	kA	ASSR
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Dagestánskou	dagestánský	k2eAgFnSc4d1	Dagestánská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
roste	růst	k5eAaImIp3nS	růst
vlastenecké	vlastenecký	k2eAgNnSc4d1	vlastenecké
povědomí	povědomí	k1gNnSc4	povědomí
a	a	k8xC	a
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
odtržení	odtržení	k1gNnSc4	odtržení
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
představuje	představovat	k5eAaImIp3nS	představovat
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Dagestánu	Dagestán	k1gInSc2	Dagestán
pestrou	pestrý	k2eAgFnSc4d1	pestrá
směsici	směsice	k1gFnSc4	směsice
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
na	na	k7c4	na
30	[number]	k4	30
místních	místní	k2eAgNnPc2d1	místní
etnik	etnikum	k1gNnPc2	etnikum
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
početně	početně	k6eAd1	početně
vynikají	vynikat	k5eAaImIp3nP	vynikat
Avarové	Avarové	k?	Avarové
(	(	kIx(	(
<g/>
29	[number]	k4	29
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dargové	Darg	k1gMnPc1	Darg
(	(	kIx(	(
<g/>
16	[number]	k4	16
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kumykové	Kumykové	k2eAgFnSc1d1	Kumykové
(	(	kIx(	(
<g/>
14	[number]	k4	14
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lezgové	Lezg	k1gMnPc1	Lezg
(	(	kIx(	(
<g/>
13	[number]	k4	13
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lakové	lakový	k2eAgNnSc4d1	lakové
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Azerové	Azerové	k2eAgFnSc1d1	Azerové
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tabasaránci	Tabasaránek	k1gMnPc1	Tabasaránek
(	(	kIx(	(
<g/>
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohá	mnohé	k1gNnPc1	mnohé
další	další	k2eAgNnPc1d1	další
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
doplnili	doplnit	k5eAaPmAgMnP	doplnit
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgMnPc2	svůj
přesunů	přesun	k1gInPc2	přesun
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
koloniální	koloniální	k2eAgFnSc2d1	koloniální
politiky	politika	k1gFnSc2	politika
<g/>
)	)	kIx)	)
Rusové	Rus	k1gMnPc1	Rus
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Ázerbájdžánci	Ázerbájdžánec	k1gMnPc1	Ázerbájdžánec
<g/>
,	,	kIx,	,
Nogajci	Nogajce	k1gMnPc1	Nogajce
<g/>
,	,	kIx,	,
Kalmyci	Kalmyk	k1gMnPc1	Kalmyk
<g/>
,	,	kIx,	,
Čečenci	Čečence	k1gFnSc4	Čečence
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
sunnitskému	sunnitský	k2eAgInSc3d1	sunnitský
islámu	islám	k1gInSc3	islám
<g/>
,	,	kIx,	,
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
i	i	k9	i
některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
súfismu	súfismus	k1gInSc2	súfismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
navíc	navíc	k6eAd1	navíc
šíří	šířit	k5eAaImIp3nS	šířit
wahhábismus	wahhábismus	k1gInSc1	wahhábismus
a	a	k8xC	a
militantní	militantní	k2eAgInSc1d1	militantní
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Etnickou	etnický	k2eAgFnSc4d1	etnická
situaci	situace	k1gFnSc4	situace
ještě	ještě	k6eAd1	ještě
zkomplikoval	zkomplikovat	k5eAaPmAgMnS	zkomplikovat
konflikt	konflikt	k1gInSc4	konflikt
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Čečensku	Čečensko	k1gNnSc6	Čečensko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přinesl	přinést	k5eAaPmAgInS	přinést
jen	jen	k6eAd1	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
islámských	islámský	k2eAgMnPc2d1	islámský
fundamentalistů	fundamentalista	k1gMnPc2	fundamentalista
ze	z	k7c2	z
sousedního	sousední	k2eAgNnSc2d1	sousední
Čečenska	Čečensko	k1gNnSc2	Čečensko
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
části	část	k1gFnSc2	část
Dagestánu	Dagestán	k1gInSc2	Dagestán
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1999	[number]	k4	1999
krvavý	krvavý	k2eAgInSc4d1	krvavý
střet	střet	k1gInSc4	střet
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
federální	federální	k2eAgFnSc7d1	federální
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
druhou	druhý	k4xOgFnSc4	druhý
čečenskou	čečenský	k2eAgFnSc4d1	čečenská
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Dagestánu	Dagestán	k1gInSc2	Dagestán
je	být	k5eAaImIp3nS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
součást	součást	k1gFnSc4	součást
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
step	step	k1gFnSc1	step
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
dobře	dobře	k6eAd1	dobře
daří	dařit	k5eAaImIp3nS	dařit
zemědělství	zemědělství	k1gNnSc1	zemědělství
(	(	kIx(	(
<g/>
pěstování	pěstování	k1gNnSc1	pěstování
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
pěstování	pěstování	k1gNnSc1	pěstování
obilovin	obilovina	k1gFnPc2	obilovina
a	a	k8xC	a
slunečnice	slunečnice	k1gFnSc2	slunečnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
nerostným	nerostný	k2eAgNnSc7d1	nerostné
bohatstvím	bohatství	k1gNnSc7	bohatství
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
pod	pod	k7c7	pod
samotným	samotný	k2eAgInSc7d1	samotný
Dagestánem	Dagestán	k1gInSc7	Dagestán
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pod	pod	k7c7	pod
Kaspickým	kaspický	k2eAgNnSc7d1	Kaspické
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
zpracování	zpracování	k1gNnSc1	zpracování
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
dagestánskou	dagestánský	k2eAgFnSc4d1	Dagestánská
ekonomiku	ekonomika	k1gFnSc4	ekonomika
klíčové	klíčový	k2eAgFnPc4d1	klíčová
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Machačkala	Machačkala	k1gFnSc1	Machačkala
se	se	k3xPyFc4	se
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
do	do	k7c2	do
celého	celý	k2eAgNnSc2d1	celé
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Machačkala	Machačkat	k5eAaPmAgFnS	Machačkat
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnSc1d1	hlavní
<g/>
)	)	kIx)	)
Kaspijsk	Kaspijsk	k1gInSc1	Kaspijsk
(	(	kIx(	(
<g/>
přístav	přístav	k1gInSc1	přístav
<g/>
)	)	kIx)	)
Derbent	Derbent	k1gInSc1	Derbent
(	(	kIx(	(
<g/>
přístav	přístav	k1gInSc1	přístav
<g/>
)	)	kIx)	)
Chasavjurt	Chasavjurt	k1gInSc1	Chasavjurt
Kizljar	Kizljar	k1gMnSc1	Kizljar
Izberbaš	Izberbaš	k1gMnSc1	Izberbaš
</s>
