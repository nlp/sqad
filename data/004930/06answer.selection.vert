<s>
Republika	republika	k1gFnSc1	republika
Dagestán	Dagestán	k1gInSc1	Dagestán
neboli	neboli	k8xC	neboli
Dagestánská	dagestánský	k2eAgFnSc1d1	Dagestánská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
:	:	kIx,	:
Р	Р	k?	Р
Д	Д	k?	Д
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejjižnější	jižní	k2eAgFnSc1d3	nejjižnější
autonomní	autonomní	k2eAgFnSc1d1	autonomní
republika	republika	k1gFnSc1	republika
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
