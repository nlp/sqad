<p>
<s>
Bronchitida	bronchitida	k1gFnSc1	bronchitida
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc4	zánět
průdušek	průduška	k1gFnPc2	průduška
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
spadá	spadat	k5eAaPmIp3nS	spadat
mezi	mezi	k7c7	mezi
záněty	zánět	k1gInPc7	zánět
dolních	dolní	k2eAgFnPc2d1	dolní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejběžnějších	běžný	k2eAgInPc2d3	nejběžnější
patologických	patologický	k2eAgInPc2d1	patologický
jevů	jev	k1gInPc2	jev
postihující	postihující	k2eAgFnSc4d1	postihující
dýchací	dýchací	k2eAgFnSc4d1	dýchací
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
průdušek	průduška	k1gFnPc2	průduška
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
zánětlivého	zánětlivý	k2eAgInSc2d1	zánětlivý
edému	edém	k1gInSc2	edém
a	a	k8xC	a
tvorbě	tvorba	k1gFnSc3	tvorba
exsudátu	exsudát	k1gInSc2	exsudát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnPc4	příčina
==	==	k?	==
</s>
</p>
<p>
<s>
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
<g/>
:	:	kIx,	:
respirační	respirační	k2eAgFnPc1d1	respirační
viry	vira	k1gFnPc1	vira
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
chlamydie	chlamydie	k1gFnPc1	chlamydie
<g/>
,	,	kIx,	,
kvasinky	kvasinka	k1gFnPc1	kvasinka
<g/>
,	,	kIx,	,
plísně	plíseň	k1gFnPc1	plíseň
</s>
</p>
<p>
<s>
inhalace	inhalace	k1gFnSc1	inhalace
dráždivých	dráždivý	k2eAgInPc2d1	dráždivý
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
par	para	k1gFnPc2	para
(	(	kIx(	(
<g/>
kouření	kouření	k1gNnSc1	kouření
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
okolnosti	okolnost	k1gFnPc4	okolnost
podporující	podporující	k2eAgInSc1d1	podporující
vznik	vznik	k1gInSc1	vznik
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
:	:	kIx,	:
prochlazení	prochlazení	k1gNnSc1	prochlazení
<g/>
,	,	kIx,	,
suchý	suchý	k2eAgInSc4d1	suchý
vzduch	vzduch	k1gInSc4	vzduch
<g/>
,	,	kIx,	,
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
celkový	celkový	k2eAgInSc4d1	celkový
stav	stav	k1gInSc4	stav
</s>
</p>
<p>
<s>
==	==	k?	==
Příznaky	příznak	k1gInPc4	příznak
==	==	k?	==
</s>
</p>
<p>
<s>
kašel	kašel	k1gInSc1	kašel
–	–	k?	–
ranní	ranní	k1gFnSc2	ranní
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
suchý	suchý	k2eAgInSc4d1	suchý
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
expentorací	expentorace	k1gFnSc7	expentorace
(	(	kIx(	(
<g/>
vykašláváním	vykašlávání	k1gNnSc7	vykašlávání
<g/>
)	)	kIx)	)
hlenů	hlen	k1gInPc2	hlen
-	-	kIx~	-
produktivní	produktivní	k2eAgInSc1d1	produktivní
<g/>
,	,	kIx,	,
záchvatovitý	záchvatovitý	k2eAgInSc1d1	záchvatovitý
<g/>
.	.	kIx.	.
</s>
<s>
Dráždivý	dráždivý	k2eAgInSc1d1	dráždivý
kašel	kašel	k1gInSc1	kašel
a	a	k8xC	a
bolest	bolest	k1gFnSc1	bolest
za	za	k7c7	za
hrudní	hrudní	k2eAgFnSc7d1	hrudní
kostí	kost	k1gFnSc7	kost
značí	značit	k5eAaImIp3nS	značit
akutní	akutní	k2eAgFnSc4d1	akutní
bronchitidu	bronchitida	k1gFnSc4	bronchitida
<g/>
.	.	kIx.	.
</s>
<s>
Trvalý	trvalý	k2eAgInSc1d1	trvalý
kašel	kašel	k1gInSc1	kašel
s	s	k7c7	s
vykašláváním	vykašlávání	k1gNnSc7	vykašlávání
žlutého	žlutý	k2eAgMnSc2d1	žlutý
až	až	k8xS	až
zeleného	zelený	k2eAgInSc2d1	zelený
hlenu	hlen	k1gInSc2	hlen
značí	značit	k5eAaImIp3nS	značit
bronchitidu	bronchitida	k1gFnSc4	bronchitida
chronickou	chronický	k2eAgFnSc4d1	chronická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sputum	sputum	k1gNnSc1	sputum
–	–	k?	–
hlenové	hlenový	k2eAgFnSc2d1	hlenová
až	až	k8xS	až
hleno-hnisavé	hlenonisavý	k2eAgFnSc2d1	hleno-hnisavý
</s>
</p>
<p>
<s>
teplota	teplota	k1gFnSc1	teplota
<g/>
:	:	kIx,	:
akutní	akutní	k2eAgFnSc2d1	akutní
-	-	kIx~	-
subfebrilie	subfebrilie	k1gFnSc2	subfebrilie
až	až	k9	až
febris	febris	k1gFnPc1	febris
<g/>
,	,	kIx,	,
chronické	chronický	k2eAgFnPc1d1	chronická
<g/>
:	:	kIx,	:
bez	bez	k7c2	bez
teploty	teplota	k1gFnSc2	teplota
</s>
</p>
<p>
<s>
únava	únava	k1gFnSc1	únava
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc1	bolest
hlavy	hlava	k1gFnSc2	hlava
</s>
</p>
<p>
<s>
==	==	k?	==
Komplikace	komplikace	k1gFnPc4	komplikace
==	==	k?	==
</s>
</p>
<p>
<s>
CHOPN	CHOPN	kA	CHOPN
-	-	kIx~	-
vystupňovaná	vystupňovaný	k2eAgFnSc1d1	vystupňovaná
bronchitis	bronchitis	k1gFnSc1	bronchitis
<g/>
,	,	kIx,	,
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
</s>
</p>
<p>
<s>
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
plíce	plíce	k1gFnPc4	plíce
-	-	kIx~	-
pneumonie	pneumonie	k1gFnPc4	pneumonie
</s>
</p>
<p>
<s>
přechod	přechod	k1gInSc1	přechod
do	do	k7c2	do
chronicity	chronicita	k1gFnSc2	chronicita
</s>
</p>
<p>
<s>
zhoršení	zhoršení	k1gNnSc1	zhoršení
základní	základní	k2eAgFnSc2d1	základní
choroby	choroba	k1gFnSc2	choroba
u	u	k7c2	u
kardiaků	kardiak	k1gMnPc2	kardiak
a	a	k8xC	a
diabetiků	diabetik	k1gMnPc2	diabetik
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
mukolytika	mukolytika	k1gFnSc1	mukolytika
=	=	kIx~	=
léky	lék	k1gInPc4	lék
usnadňující	usnadňující	k2eAgNnSc4d1	usnadňující
vykašlávání	vykašlávání	k1gNnSc4	vykašlávání
<g/>
,	,	kIx,	,
rozpouštějící	rozpouštějící	k2eAgInPc4d1	rozpouštějící
hleny	hlen	k1gInPc4	hlen
</s>
</p>
<p>
<s>
expectorancia	expectorancia	k1gFnSc1	expectorancia
=	=	kIx~	=
léky	lék	k1gInPc1	lék
usnadňující	usnadňující	k2eAgFnSc2d1	usnadňující
a	a	k8xC	a
napomáhající	napomáhající	k2eAgFnSc2d1	napomáhající
odkašlávání	odkašlávání	k1gNnSc1	odkašlávání
</s>
</p>
<p>
<s>
vitamín	vitamín	k1gInSc1	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
-	-	kIx~	-
zvýšení	zvýšení	k1gNnSc1	zvýšení
imunity	imunita	k1gFnSc2	imunita
</s>
</p>
<p>
<s>
ATB	ATB	kA	ATB
při	při	k7c6	při
bakteriální	bakteriální	k2eAgFnSc6d1	bakteriální
nákaze	nákaza	k1gFnSc6	nákaza
</s>
</p>
<p>
<s>
RHB	RHB	kA	RHB
–	–	k?	–
polohování	polohování	k1gNnSc1	polohování
(	(	kIx(	(
<g/>
Fowlerova	Fowlerův	k2eAgFnSc1d1	Fowlerova
poloha	poloha	k1gFnSc1	poloha
<g/>
,	,	kIx,	,
Ortopnoická	Ortopnoický	k2eAgFnSc1d1	Ortopnoický
poloha	poloha	k1gFnSc1	poloha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poklepová	poklepový	k2eAgFnSc1d1	poklepová
masáž	masáž	k1gFnSc1	masáž
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
bronchitid	bronchitida	k1gFnPc2	bronchitida
==	==	k?	==
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
průběhu	průběh	k1gInSc2	průběh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
akutní	akutní	k2eAgMnSc1d1	akutní
</s>
</p>
<p>
<s>
chronickáDle	chronickáDle	k1gFnSc1	chronickáDle
etiologie	etiologie	k1gFnSc1	etiologie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
virová	virový	k2eAgFnSc1d1	virová
</s>
</p>
<p>
<s>
bakteriální	bakteriální	k2eAgFnSc7d1	bakteriální
</s>
</p>
<p>
<s>
mykotická	mykotický	k2eAgFnSc1d1	mykotická
</s>
</p>
<p>
<s>
parazitární	parazitární	k2eAgFnSc4d1	parazitární
</s>
</p>
<p>
<s>
autoimunitní	autoimunitní	k2eAgFnSc1d1	autoimunitní
(	(	kIx(	(
<g/>
alergie	alergie	k1gFnSc1	alergie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
bronchitida	bronchitida	k1gFnSc1	bronchitida
drůbeže	drůbež	k1gFnSc2	drůbež
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bronchitida	bronchitida	k1gFnSc1	bronchitida
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Výukový	výukový	k2eAgInSc4d1	výukový
kurs	kurs	k1gInSc4	kurs
bronchitida	bronchitida	k1gFnSc1	bronchitida
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Zánět	zánět	k1gInSc1	zánět
průdušek	průduška	k1gFnPc2	průduška
-	-	kIx~	-
příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
</s>
</p>
