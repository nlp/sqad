<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
California	Californium	k1gNnSc2	Californium
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
California	Californium	k1gNnSc2	Californium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pacifických	pacifický	k2eAgInPc2d1	pacifický
států	stát	k1gInPc2	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Oregonem	Oregon	k1gInSc7	Oregon
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Nevadou	Nevada	k1gFnSc7	Nevada
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
mexickým	mexický	k2eAgInSc7d1	mexický
státem	stát	k1gInSc7	stát
Baja	Bajum	k1gNnSc2	Bajum
California	Californium	k1gNnSc2	Californium
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
ohraničení	ohraničení	k1gNnSc1	ohraničení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
423	[number]	k4	423
970	[number]	k4	970
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
39,3	[number]	k4	39,3
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
93	[number]	k4	93
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Sacramento	Sacramento	k1gNnSc1	Sacramento
se	s	k7c7	s
490	[number]	k4	490
tisíci	tisíc	k4xCgInPc7	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
s	s	k7c7	s
4,0	[number]	k4	4,0
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
milionu	milion	k4xCgInSc2	milion
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
José	Josá	k1gFnSc2	Josá
(	(	kIx(	(
<g/>
1,0	[number]	k4	1,0
milionu	milion	k4xCgInSc2	milion
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
(	(	kIx(	(
<g/>
860	[number]	k4	860
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fresno	Fresno	k6eAd1	Fresno
(	(	kIx(	(
<g/>
520	[number]	k4	520
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornii	Kalifornie	k1gFnSc4	Kalifornie
patří	patřit	k5eAaImIp3nS	patřit
1352	[number]	k4	1352
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Mount	Mounta	k1gFnPc2	Mounta
Whitney	Whitnea	k1gFnSc2	Whitnea
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
4421	[number]	k4	4421
m	m	kA	m
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pláň	pláň	k1gFnSc1	pláň
Badwater	Badwatra	k1gFnPc2	Badwatra
v	v	k7c6	v
Údolí	údolí	k1gNnSc6	údolí
smrti	smrt	k1gFnSc2	smrt
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
-	-	kIx~	-
m	m	kA	m
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgNnSc7d1	položené
místem	místo	k1gNnSc7	místo
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Arizonou	Arizona	k1gFnSc7	Arizona
<g/>
,	,	kIx,	,
a	a	k8xC	a
San	San	k1gMnSc1	San
Joaquin	Joaquin	k1gMnSc1	Joaquin
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornské	kalifornský	k2eAgNnSc1d1	kalifornské
pobřeží	pobřeží	k1gNnSc1	pobřeží
bylo	být	k5eAaImAgNnS	být
Evropany	Evropan	k1gMnPc4	Evropan
poprvé	poprvé	k6eAd1	poprvé
prozkoumáno	prozkoumán	k2eAgNnSc1d1	prozkoumáno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1542	[number]	k4	1542
<g/>
,	,	kIx,	,
pozemní	pozemní	k2eAgFnPc1d1	pozemní
španělské	španělský	k2eAgFnPc1d1	španělská
expedice	expedice	k1gFnPc1	expedice
posléze	posléze	k6eAd1	posléze
probíhaly	probíhat	k5eAaImAgFnP	probíhat
během	během	k7c2	během
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
zřejmě	zřejmě	k6eAd1	zřejmě
podle	podle	k7c2	podle
mýtické	mýtický	k2eAgFnSc2d1	mýtická
země	zem	k1gFnSc2	zem
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
populárního	populární	k2eAgInSc2d1	populární
španělského	španělský	k2eAgInSc2d1	španělský
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
ke	k	k7c3	k
Kalifornskému	kalifornský	k2eAgInSc3d1	kalifornský
poloostrovu	poloostrov	k1gInSc3	poloostrov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
na	na	k7c4	na
severnější	severní	k2eAgFnSc4d2	severnější
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
vznikaly	vznikat	k5eAaImAgFnP	vznikat
první	první	k4xOgFnPc1	první
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
misie	misie	k1gFnPc1	misie
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Horní	horní	k2eAgFnSc1d1	horní
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
stala	stát	k5eAaPmAgFnS	stát
částí	část	k1gFnSc7	část
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
kterou	který	k3yQgFnSc4	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledku	výsledek	k1gInSc2	výsledek
mexicko-americké	mexickomerický	k2eAgFnSc2d1	mexicko-americká
války	válka	k1gFnSc2	válka
získaly	získat	k5eAaPmAgFnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zde	zde	k6eAd1	zde
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
díky	díky	k7c3	díky
nárůstu	nárůst	k1gInSc3	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1850	[number]	k4	1850
stala	stát	k5eAaPmAgFnS	stát
31	[number]	k4	31
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejbohatším	bohatý	k2eAgNnPc3d3	nejbohatší
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
nejaktivnějším	aktivní	k2eAgInPc3d3	nejaktivnější
státům	stát	k1gInPc3	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
oblastí	oblast	k1gFnSc7	oblast
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
a	a	k8xC	a
také	také	k9	také
Silicon	Silicon	kA	Silicon
Valley	Vallea	k1gFnPc1	Vallea
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
elektronického	elektronický	k2eAgInSc2d1	elektronický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Losangeleská	losangeleský	k2eAgFnSc1d1	losangeleská
čtvrť	čtvrť	k1gFnSc1	čtvrť
Hollywood	Hollywood	k1gInSc1	Hollywood
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
amerického	americký	k2eAgInSc2d1	americký
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
suché	suchý	k2eAgNnSc1d1	suché
a	a	k8xC	a
horké	horký	k2eAgNnSc1d1	horké
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
častá	častý	k2eAgNnPc1d1	časté
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
-	-	kIx~	-
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
a	a	k8xC	a
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhnání	vyhnání	k1gNnSc6	vyhnání
nepočetných	početný	k2eNgInPc2d1	nepočetný
indiánských	indiánský	k2eAgInPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
se	se	k3xPyFc4	se
území	území	k1gNnSc1	území
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
stalo	stát	k5eAaPmAgNnS	stát
severní	severní	k2eAgFnSc7d1	severní
výspou	výspa	k1gFnSc7	výspa
španělské	španělský	k2eAgFnSc2d1	španělská
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
severoamerickém	severoamerický	k2eAgInSc6d1	severoamerický
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
spadala	spadat	k5eAaPmAgFnS	spadat
pod	pod	k7c4	pod
místokrálovství	místokrálovství	k1gNnSc4	místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
kolonie	kolonie	k1gFnSc2	kolonie
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
Mexiko	Mexiko	k1gNnSc1	Mexiko
(	(	kIx(	(
<g/>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
mexické	mexický	k2eAgFnSc2d1	mexická
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
teritorií	teritorium	k1gNnPc2	teritorium
byla	být	k5eAaImAgFnS	být
i	i	k9	i
Alta	Alta	k1gFnSc1	Alta
California	Californium	k1gNnSc2	Californium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1846	[number]	k4	1846
<g/>
-	-	kIx~	-
<g/>
1848	[number]	k4	1848
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
mexicko-americká	mexickomerický	k2eAgFnSc1d1	mexicko-americká
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
historické	historický	k2eAgFnSc2d1	historická
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
oba	dva	k4xCgInPc4	dva
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
(	(	kIx(	(
<g/>
americká	americký	k2eAgFnSc1d1	americká
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
státy	stát	k1gInPc1	stát
Kalifornii	Kalifornie	k1gFnSc3	Kalifornie
a	a	k8xC	a
teritoria	teritorium	k1gNnPc1	teritorium
Nové	Nové	k2eAgNnPc1d1	Nové
Mexiko	Mexiko	k1gNnSc4	Mexiko
a	a	k8xC	a
Utah	Utah	k1gInSc4	Utah
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jižní	jižní	k2eAgFnSc1d1	jižní
(	(	kIx(	(
<g/>
mexická	mexický	k2eAgFnSc1d1	mexická
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
tvořila	tvořit	k5eAaImAgFnS	tvořit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
jeden	jeden	k4xCgInSc4	jeden
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přeměněná	přeměněný	k2eAgFnSc1d1	přeměněná
v	v	k7c4	v
mexické	mexický	k2eAgInPc4d1	mexický
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
postihly	postihnout	k5eAaPmAgInP	postihnout
ničivé	ničivý	k2eAgInPc1d1	ničivý
požáry	požár	k1gInPc1	požár
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyhnaly	vyhnat	k5eAaPmAgFnP	vyhnat
z	z	k7c2	z
domovů	domov	k1gInPc2	domov
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
včetně	včetně	k7c2	včetně
hollywoodských	hollywoodský	k2eAgFnPc2d1	hollywoodská
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
26	[number]	k4	26
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
12	[number]	k4	12
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
omývá	omývat	k5eAaImIp3nS	omývat
Tichý	tichý	k2eAgInSc1d1	tichý
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
státem	stát	k1gInSc7	stát
Oregon	Oregona	k1gFnPc2	Oregona
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
má	mít	k5eAaImIp3nS	mít
společnou	společný	k2eAgFnSc4d1	společná
s	s	k7c7	s
Nevadou	Nevada	k1gFnSc7	Nevada
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
leží	ležet	k5eAaImIp3nS	ležet
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
třetí	třetí	k4xOgInSc4	třetí
největší	veliký	k2eAgInSc4d3	veliký
stát	stát	k1gInSc4	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
má	mít	k5eAaImIp3nS	mít
úrodné	úrodný	k2eAgNnSc4d1	úrodné
Centrální	centrální	k2eAgNnSc4d1	centrální
údolí	údolí	k1gNnSc4	údolí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zavlažování	zavlažování	k1gNnSc3	zavlažování
daří	dařit	k5eAaImIp3nS	dařit
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
převážně	převážně	k6eAd1	převážně
hornaté	hornatý	k2eAgFnSc6d1	hornatá
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
pohoří	pohoří	k1gNnSc1	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
souvislých	souvislý	k2eAgInPc2d1	souvislý
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Mount	Mount	k1gMnSc1	Mount
Whitney	Whitnea	k1gMnSc2	Whitnea
(	(	kIx(	(
<g/>
4421	[number]	k4	4421
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
horké	horký	k2eAgFnPc4d1	horká
suché	suchý	k2eAgFnPc4d1	suchá
pouště	poušť	k1gFnPc4	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Sierra	Sierra	k1gFnSc1	Sierra
Nevada	Nevada	k1gFnSc1	Nevada
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Yosemitský	Yosemitský	k2eAgInSc1d1	Yosemitský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
a	a	k8xC	a
hluboké	hluboký	k2eAgNnSc1d1	hluboké
sladkovodní	sladkovodní	k2eAgNnSc1d1	sladkovodní
jezero	jezero	k1gNnSc1	jezero
Tahoe	Taho	k1gFnSc2	Taho
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
leží	ležet	k5eAaImIp3nS	ležet
velké	velký	k2eAgNnSc1d1	velké
slané	slaný	k2eAgNnSc1d1	slané
jezero	jezero	k1gNnSc1	jezero
Salton	Salton	k1gInSc1	Salton
Sea	Sea	k1gMnSc1	Sea
a	a	k8xC	a
Mohavská	Mohavský	k2eAgFnSc1d1	Mohavská
poušť	poušť	k1gFnSc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
této	tento	k3xDgFnSc2	tento
pouště	poušť	k1gFnSc2	poušť
leží	ležet	k5eAaImIp3nS	ležet
Údolí	údolí	k1gNnSc1	údolí
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejníže	nízce	k6eAd3	nízce
ležící	ležící	k2eAgInSc4d1	ležící
bod	bod	k1gInSc4	bod
(	(	kIx(	(
<g/>
Badwater	Badwater	k1gMnSc1	Badwater
<g/>
,	,	kIx,	,
85	[number]	k4	85
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
hladiny	hladina	k1gFnSc2	hladina
oceánu	oceán	k1gInSc2	oceán
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejteplejší	teplý	k2eAgNnSc4d3	nejteplejší
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
je	být	k5eAaImIp3nS	být
i	i	k9	i
známá	známá	k1gFnSc1	známá
výskytem	výskyt	k1gInSc7	výskyt
častých	častý	k2eAgInPc2d1	častý
a	a	k8xC	a
velice	velice	k6eAd1	velice
intenzivních	intenzivní	k2eAgNnPc2d1	intenzivní
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
zlomem	zlom	k1gInSc7	zlom
San	San	k1gMnSc1	San
Andreas	Andreas	k1gMnSc1	Andreas
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
postihlo	postihnout	k5eAaPmAgNnS	postihnout
San	San	k1gMnSc4	San
Francisco	Francisco	k6eAd1	Francisco
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
u	u	k7c2	u
pacifického	pacifický	k2eAgNnSc2d1	pacifické
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
panuje	panovat	k5eAaImIp3nS	panovat
středomořské	středomořský	k2eAgNnSc1d1	středomořské
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
.	.	kIx.	.
průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
884	[number]	k4	884
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
403	[number]	k4	403
km	km	kA	km
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
1240	[number]	k4	1240
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
San	San	k1gMnSc1	San
José	Josá	k1gFnSc2	Josá
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
Fresno	Fresen	k2eAgNnSc4d1	Fresno
Sacramento	Sacramento	k1gNnSc4	Sacramento
Long	Long	k1gMnSc1	Long
Beach	Beach	k1gMnSc1	Beach
Oakland	Oaklanda	k1gFnPc2	Oaklanda
Bakersfield	Bakersfield	k1gMnSc1	Bakersfield
Anaheim	Anaheim	k1gMnSc1	Anaheim
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
HDP	HDP	kA	HDP
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
celých	celý	k2eAgNnPc2d1	celé
14	[number]	k4	14
%	%	kIx~	%
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
činí	činit	k5eAaImIp3nS	činit
1,4	[number]	k4	1,4
bilionu	bilion	k4xCgInSc2	bilion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc4d3	veliký
HDP	HDP	kA	HDP
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
je	být	k5eAaImIp3nS	být
47	[number]	k4	47
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
3	[number]	k4	3
916	[number]	k4	916
dolarů	dolar	k1gInPc2	dolar
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
celostátní	celostátní	k2eAgInSc1d1	celostátní
průměr	průměr	k1gInSc1	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
třikrát	třikrát	k6eAd1	třikrát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
centrem	centrum	k1gNnSc7	centrum
počítačového	počítačový	k2eAgInSc2d1	počítačový
průmyslu	průmysl	k1gInSc2	průmysl
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
soustředěn	soustředit	k5eAaPmNgInS	soustředit
v	v	k7c6	v
Silicon	Silicon	kA	Silicon
Valley	Valle	k2eAgFnPc4d1	Valle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nejvíce	hodně	k6eAd3	hodně
filmů	film	k1gInPc2	film
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
indického	indický	k2eAgInSc2d1	indický
Bollywoodu	Bollywood	k1gInSc2	Bollywood
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
soustředěn	soustředit	k5eAaPmNgInS	soustředit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
rozpočet	rozpočet	k1gInSc1	rozpočet
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
20	[number]	k4	20
let	léto	k1gNnPc2	léto
rapidně	rapidně	k6eAd1	rapidně
klesla	klesnout	k5eAaPmAgFnS	klesnout
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
4,9	[number]	k4	4,9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
ekonomika	ekonomika	k1gFnSc1	ekonomika
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
velmi	velmi	k6eAd1	velmi
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
chudoby	chudoba	k1gFnSc2	chudoba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
bezdomovců	bezdomovec	k1gMnPc2	bezdomovec
<g/>
,	,	kIx,	,
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bydlení	bydlení	k1gNnSc1	bydlení
je	být	k5eAaImIp3nS	být
levné	levný	k2eAgNnSc1d1	levné
a	a	k8xC	a
volných	volný	k2eAgNnPc2d1	volné
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
z	z	k7c2	z
chudšího	chudý	k2eAgNnSc2d2	chudší
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
nejen	nejen	k6eAd1	nejen
rasovou	rasový	k2eAgFnSc7d1	rasová
diskriminací	diskriminace	k1gFnSc7	diskriminace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
negramotností	negramotnost	k1gFnSc7	negramotnost
a	a	k8xC	a
složitou	složitý	k2eAgFnSc7d1	složitá
integrací	integrace	k1gFnSc7	integrace
těchto	tento	k3xDgFnPc2	tento
menšin	menšina	k1gFnPc2	menšina
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
zemědělské	zemědělský	k2eAgInPc1d1	zemědělský
produkty	produkt	k1gInPc1	produkt
<g/>
:	:	kIx,	:
ovoce	ovoce	k1gNnSc1	ovoce
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
chov	chov	k1gInSc1	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
vinařství	vinařství	k1gNnPc1	vinařství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
<g/>
:	:	kIx,	:
počítačový	počítačový	k2eAgInSc1d1	počítačový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
letecký	letecký	k2eAgInSc1d1	letecký
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc4	zpracování
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
strojní	strojní	k2eAgFnSc1d1	strojní
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
elektrických	elektrický	k2eAgInPc2d1	elektrický
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
státům	stát	k1gInPc3	stát
velkou	velký	k2eAgFnSc4d1	velká
spotřebu	spotřeba	k1gFnSc4	spotřeba
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
GW	GW	kA	GW
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnPc1	její
elektrárny	elektrárna	k1gFnPc1	elektrárna
mají	mít	k5eAaImIp3nP	mít
kapacitu	kapacita	k1gFnSc4	kapacita
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
GW	GW	kA	GW
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
připravoval	připravovat	k5eAaImAgMnS	připravovat
guvernér	guvernér	k1gMnSc1	guvernér
Pete	Pet	k1gFnSc2	Pet
Wilson	Wilson	k1gInSc1	Wilson
legislativu	legislativa	k1gFnSc4	legislativa
umožňující	umožňující	k2eAgFnSc1d1	umožňující
deregulaci	deregulace	k1gFnSc4	deregulace
trhu	trh	k1gInSc2	trh
s	s	k7c7	s
energií	energie	k1gFnSc7	energie
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
principy	princip	k1gInPc4	princip
volného	volný	k2eAgInSc2d1	volný
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Energetický	energetický	k2eAgMnSc1d1	energetický
gigant	gigant	k1gMnSc1	gigant
Enron	Enron	k1gMnSc1	Enron
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
akvizicí	akvizice	k1gFnPc2	akvizice
získal	získat	k5eAaPmAgMnS	získat
firmu	firma	k1gFnSc4	firma
PGE	PGE	kA	PGE
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
kalifornskému	kalifornský	k2eAgInSc3d1	kalifornský
deregulovanému	deregulovaný	k2eAgInSc3d1	deregulovaný
trhu	trh	k1gInSc3	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2000	[number]	k4	2000
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
zažila	zažít	k5eAaPmAgFnS	zažít
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
opakované	opakovaný	k2eAgInPc4d1	opakovaný
výpadky	výpadek	k1gInPc4	výpadek
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
přešla	přejít	k5eAaPmAgFnS	přejít
ze	z	k7c2	z
státu	stát	k1gInSc2	stát
do	do	k7c2	do
soukromých	soukromý	k2eAgFnPc2d1	soukromá
rukou	ruka	k1gFnPc2	ruka
řada	řada	k1gFnSc1	řada
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
se	se	k3xPyFc4	se
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
století	století	k1gNnSc2	století
zněkolikanásobila	zněkolikanásobit	k5eAaPmAgFnS	zněkolikanásobit
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
37	[number]	k4	37
253	[number]	k4	253
956	[number]	k4	956
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
38	[number]	k4	38
340	[number]	k4	340
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
nejvíc	hodně	k6eAd3	hodně
urbanizovaný	urbanizovaný	k2eAgInSc1d1	urbanizovaný
stát	stát	k1gInSc1	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městech	město	k1gNnPc6	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
žilo	žít	k5eAaImAgNnS	žít
95	[number]	k4	95
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
je	být	k5eAaImIp3nS	být
95	[number]	k4	95
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
kilometr	kilometr	k1gInSc4	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
skupinou	skupina	k1gFnSc7	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Hispánci	Hispánek	k1gMnPc1	Hispánek
<g/>
,	,	kIx,	,
následovaní	následovaný	k2eAgMnPc1d1	následovaný
bělochy	běloch	k1gMnPc4	běloch
<g/>
,	,	kIx,	,
Asiaty	Asiat	k1gMnPc4	Asiat
a	a	k8xC	a
černochy	černoch	k1gMnPc4	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
prošla	projít	k5eAaPmAgFnS	projít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledních	poslední	k2eAgNnPc2d1	poslední
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
významnou	významný	k2eAgFnSc7d1	významná
demografickou	demografický	k2eAgFnSc7d1	demografická
proměnou	proměna	k1gFnSc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
tvořili	tvořit	k5eAaImAgMnP	tvořit
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pouhých	pouhý	k2eAgInPc2d1	pouhý
38,8	[number]	k4	38,8
%	%	kIx~	%
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
porodnosti	porodnost	k1gFnSc3	porodnost
jen	jen	k9	jen
26	[number]	k4	26
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
novorozenců	novorozenec	k1gMnPc2	novorozenec
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
skupinou	skupina	k1gFnSc7	skupina
obyvatel	obyvatel	k1gMnSc1	obyvatel
Hispánci	Hispánek	k1gMnPc1	Hispánek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
jejich	jejich	k3xOp3gFnSc1	jejich
vysoká	vysoký	k2eAgFnSc1d1	vysoká
porodnost	porodnost	k1gFnSc1	porodnost
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
imigrace	imigrace	k1gFnSc1	imigrace
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgNnSc1d3	nejrozšířenější
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
římskokatolické	římskokatolický	k2eAgNnSc1d1	římskokatolické
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
je	být	k5eAaImIp3nS	být
kriminalita	kriminalita	k1gFnSc1	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
chudého	chudý	k2eAgInSc2d1	chudý
amerického	americký	k2eAgInSc2d1	americký
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
či	či	k8xC	či
ilegálních	ilegální	k2eAgMnPc2d1	ilegální
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
obtížně	obtížně	k6eAd1	obtížně
začleňuje	začleňovat	k5eAaImIp3nS	začleňovat
do	do	k7c2	do
majoritní	majoritní	k2eAgFnSc2d1	majoritní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
kriminality	kriminalita	k1gFnSc2	kriminalita
a	a	k8xC	a
spotřeby	spotřeba	k1gFnSc2	spotřeba
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
ghett	ghetto	k1gNnPc2	ghetto
<g/>
)	)	kIx)	)
proslulých	proslulý	k2eAgInPc2d1	proslulý
zločinem	zločin	k1gInSc7	zločin
<g/>
,	,	kIx,	,
prodejem	prodej	k1gInSc7	prodej
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
prostitucí	prostituce	k1gFnPc2	prostituce
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgNnPc4d1	významné
sídla	sídlo	k1gNnPc4	sídlo
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
:	:	kIx,	:
Sacramento	Sacramento	k1gNnSc4	Sacramento
(	(	kIx(	(
<g/>
502	[number]	k4	502
743	[number]	k4	743
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
<g/>
:	:	kIx,	:
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
(	(	kIx(	(
<g/>
3	[number]	k4	3
694	[number]	k4	694
820	[number]	k4	820
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc4d1	další
významná	významný	k2eAgNnPc4d1	významné
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Diego	Diego	k1gMnSc1	Diego
(	(	kIx(	(
<g/>
1	[number]	k4	1
223	[number]	k4	223
<g />
.	.	kIx.	.
</s>
<s>
400	[number]	k4	400
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
José	Josá	k1gFnSc2	Josá
(	(	kIx(	(
<g/>
925	[number]	k4	925
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
(	(	kIx(	(
<g/>
776	[number]	k4	776
773	[number]	k4	773
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fresno	Fresno	k1gNnSc1	Fresno
(	(	kIx(	(
<g/>
427	[number]	k4	427
652	[number]	k4	652
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oakland	Oakland	k1gInSc1	Oakland
(	(	kIx(	(
<g/>
399	[number]	k4	399
484	[number]	k4	484
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
Ana	Ana	k1gMnSc1	Ana
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
337	[number]	k4	337
977	[number]	k4	977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
57,6	[number]	k4	57,6
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
40,1	[number]	k4	40,1
%	%	kIx~	%
+	+	kIx~	+
běloši	běloch	k1gMnPc1	běloch
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
17,4	[number]	k4	17,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
6,2	[number]	k4	6,2
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,0	[number]	k4	1,0
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
13,0	[number]	k4	13,0
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,4	[number]	k4	0,4
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
17,0	[number]	k4	17,0
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,9	[number]	k4	4,9
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
37,6	[number]	k4	37,6
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Eureka	Eureka	k1gFnSc1	Eureka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
sluncovka	sluncovka	k1gFnSc1	sluncovka
kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
sekvoj	sekvoj	k1gInSc1	sekvoj
vždyzelená	vždyzelený	k2eAgFnSc1d1	vždyzelená
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
křepel	křepel	k1gMnSc1	křepel
kalifornský	kalifornský	k2eAgMnSc1d1	kalifornský
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
I	i	k8xC	i
Love	lov	k1gInSc5	lov
You	You	k1gFnSc2	You
<g/>
,	,	kIx,	,
California	Californium	k1gNnSc2	Californium
<g/>
.	.	kIx.	.
</s>
