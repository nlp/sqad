<s>
Generál	generál	k1gMnSc1	generál
PhDr.	PhDr.	kA	PhDr.
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
souhlásky	souhláska	k1gFnPc1	souhláska
"	"	kIx"	"
<g/>
t	t	k?	t
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
n	n	k0	n
<g/>
"	"	kIx"	"
vyslovovány	vyslovovat	k5eAaImNgFnP	vyslovovat
měkce	měkko	k6eAd1	měkko
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1880	[number]	k4	1880
Košariská	Košariská	k1gFnSc1	Košariská
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
Ivanka	Ivanka	k1gFnSc1	Ivanka
pri	pri	k?	pri
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
také	také	k9	také
astronom	astronom	k1gMnSc1	astronom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
organizoval	organizovat	k5eAaBmAgMnS	organizovat
československé	československý	k2eAgFnPc4d1	Československá
legie	legie	k1gFnPc4	legie
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
<g/>
,	,	kIx,	,
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918-1919	[number]	k4	1918-1919
československým	československý	k2eAgMnSc7d1	československý
ministrem	ministr	k1gMnSc7	ministr
vojenství	vojenství	k1gNnSc2	vojenství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Garriguem	Garrigu	k1gMnSc7	Garrigu
Masarykem	Masaryk	k1gMnSc7	Masaryk
a	a	k8xC	a
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Zahynul	zahynout	k5eAaPmAgMnS	zahynout
při	při	k7c6	při
letecké	letecký	k2eAgFnSc6d1	letecká
nehodě	nehoda	k1gFnSc6	nehoda
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Košariskách	Košariska	k1gFnPc6	Košariska
u	u	k7c2	u
Myjavy	Myjava	k1gFnSc2	Myjava
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
Slovensku	Slovensko	k1gNnSc6	Slovensko
jako	jako	k9	jako
šesté	šestý	k4xOgFnSc2	šestý
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
evangelického	evangelický	k2eAgMnSc2d1	evangelický
faráře	farář	k1gMnSc2	farář
Pavla	Pavel	k1gMnSc2	Pavel
Štefánika	Štefánik	k1gMnSc2	Štefánik
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
rodičům	rodič	k1gMnPc3	rodič
podařilo	podařit	k5eAaPmAgNnS	podařit
vychovat	vychovat	k5eAaPmF	vychovat
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
faráře	farář	k1gMnSc2	farář
<g/>
,	,	kIx,	,
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
chudém	chudý	k2eAgNnSc6d1	chudé
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
život	život	k1gInSc1	život
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nelišil	lišit	k5eNaImAgInS	lišit
od	od	k7c2	od
života	život	k1gInSc2	život
rolnických	rolnický	k2eAgFnPc2d1	rolnická
dětí	dítě	k1gFnPc2	dítě
ze	z	k7c2	z
sousedství	sousedství	k1gNnSc2	sousedství
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k8xS	co
mu	on	k3xPp3gNnSc3	on
však	však	k9	však
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
nechybělo	chybět	k5eNaImAgNnS	chybět
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
slovenské	slovenský	k2eAgFnPc1d1	slovenská
knihy	kniha	k1gFnPc1	kniha
a	a	k8xC	a
časopisy	časopis	k1gInPc1	časopis
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
Pavol	Pavola	k1gFnPc2	Pavola
Štefánik	Štefánik	k1gInSc1	Štefánik
jako	jako	k8xS	jako
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
národovec	národovec	k1gMnSc1	národovec
snažil	snažit	k5eAaImAgMnS	snažit
vychovávat	vychovávat	k5eAaImF	vychovávat
svoje	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
třídy	třída	k1gFnPc4	třída
obecné	obecný	k2eAgFnSc2d1	obecná
školy	škola	k1gFnSc2	škola
vychodil	vychodit	k5eAaImAgMnS	vychodit
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
vsi	ves	k1gFnSc6	ves
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
slovenský	slovenský	k2eAgMnSc1d1	slovenský
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
slovenského	slovenský	k2eAgNnSc2d1	slovenské
evangelického	evangelický	k2eAgNnSc2d1	evangelické
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Revúci	Revúce	k1gFnSc6	Revúce
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Kostelný	Kostelný	k2eAgMnSc1d1	Kostelný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svědectví	svědectví	k1gNnSc2	svědectví
učitele	učitel	k1gMnSc2	učitel
byl	být	k5eAaImAgMnS	být
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
žákem	žák	k1gMnSc7	žák
na	na	k7c6	na
košarišské	košarišský	k2eAgFnSc6d1	košarišský
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Pavol	Pavola	k1gFnPc2	Pavola
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
poskytnout	poskytnout	k5eAaPmF	poskytnout
synovi	syn	k1gMnSc3	syn
co	co	k3yQnSc1	co
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
vzdělání	vzdělání	k1gNnSc1	vzdělání
–	–	k?	–
aby	aby	kYmCp3nP	aby
mohl	moct	k5eAaImAgMnS	moct
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
důkladně	důkladně	k6eAd1	důkladně
naučit	naučit	k5eAaPmF	naučit
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
už	už	k6eAd1	už
v	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
domu	dům	k1gInSc2	dům
do	do	k7c2	do
Šamorína	Šamorín	k1gInSc2	Šamorín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připravil	připravit	k5eAaPmAgInS	připravit
na	na	k7c4	na
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Středoškolské	středoškolský	k2eAgNnSc1d1	středoškolské
studium	studium	k1gNnSc1	studium
započal	započnout	k5eAaPmAgInS	započnout
na	na	k7c6	na
evangelickém	evangelický	k2eAgNnSc6d1	evangelické
lyceu	lyceum	k1gNnSc6	lyceum
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Prešpurku	Prešpurku	k?	Prešpurku
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
už	už	k6eAd1	už
studovali	studovat	k5eAaImAgMnP	studovat
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
–	–	k?	–
Igor	Igor	k1gMnSc1	Igor
a	a	k8xC	a
Pavol	Pavol	k1gInSc1	Pavol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oné	onen	k3xDgFnSc6	onen
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
lyceum	lyceum	k1gNnSc1	lyceum
maďarizované	maďarizovaný	k2eAgNnSc1d1	maďarizovaný
a	a	k8xC	a
ze	z	k7c2	z
slovenských	slovenský	k2eAgMnPc2d1	slovenský
vlasteneckých	vlastenecký	k2eAgMnPc2d1	vlastenecký
profesorů	profesor	k1gMnPc2	profesor
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
učil	učít	k5eAaPmAgMnS	učít
jen	jen	k6eAd1	jen
Ján	Ján	k1gMnSc1	Ján
Kvačala	Kvačal	k1gMnSc2	Kvačal
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vyučujících	vyučující	k2eAgInPc2d1	vyučující
si	se	k3xPyFc3	se
Štefánik	Štefánik	k1gMnSc1	Štefánik
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
profesora	profesor	k1gMnSc4	profesor
matematiky	matematika	k1gFnSc2	matematika
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
Hirschmanna	Hirschmann	k1gMnSc4	Hirschmann
a	a	k8xC	a
třídního	třídní	k1gMnSc4	třídní
profesora	profesor	k1gMnSc4	profesor
Samuela	Samuel	k1gMnSc4	Samuel
Markusovszkého	Markusovszký	k1gMnSc4	Markusovszký
<g/>
.	.	kIx.	.
</s>
<s>
Štefánik	Štefánik	k1gMnSc1	Štefánik
studoval	studovat	k5eAaImAgMnS	studovat
s	s	k7c7	s
výborným	výborný	k2eAgInSc7d1	výborný
prospěchem	prospěch	k1gInSc7	prospěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
letech	let	k1gInPc6	let
musel	muset	k5eAaImAgMnS	muset
odejít	odejít	k5eAaPmF	odejít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Pavlem	Pavel	k1gMnSc7	Pavel
do	do	k7c2	do
Šoproně	Šoproň	k1gFnSc2	Šoproň
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Sarvaše	Sarvaš	k1gInSc2	Sarvaš
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Pavol	Pavol	k1gInSc1	Pavol
prospíval	prospívat	k5eAaImAgInS	prospívat
slabě	slabě	k6eAd1	slabě
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
i	i	k9	i
po	po	k7c6	po
přechodu	přechod	k1gInSc6	přechod
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
školu	škola	k1gFnSc4	škola
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
výborného	výborný	k2eAgInSc2d1	výborný
prospěchu	prospěch	k1gInSc2	prospěch
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
obdržel	obdržet	k5eAaPmAgInS	obdržet
jednorázové	jednorázový	k2eAgNnSc4d1	jednorázové
Telekiho	Telekiha	k1gFnSc5	Telekiha
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zde	zde	k6eAd1	zde
poznal	poznat	k5eAaPmAgMnS	poznat
i	i	k8xC	i
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
Emílii	Emílie	k1gFnSc4	Emílie
Chovanovou	Chovanův	k2eAgFnSc7d1	Chovanův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sarvaši	Sarvaše	k1gFnSc6	Sarvaše
nakonec	nakonec	k6eAd1	nakonec
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
odejít	odejít	k5eAaPmF	odejít
studovat	studovat	k5eAaImF	studovat
stavební	stavební	k2eAgNnSc4d1	stavební
inženýrství	inženýrství	k1gNnSc4	inženýrství
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
spolek	spolek	k1gInSc1	spolek
Detvan	Detvan	k1gMnSc1	Detvan
(	(	kIx(	(
<g/>
hlavním	hlavní	k2eAgMnSc7d1	hlavní
aktivistou	aktivista	k1gMnSc7	aktivista
byl	být	k5eAaImAgMnS	být
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
medik	medik	k1gMnSc1	medik
Vavro	Vavro	k1gNnSc1	Vavro
Šrobár	Šrobár	k1gMnSc1	Šrobár
<g/>
)	)	kIx)	)
a	a	k8xC	a
Štefánik	Štefánik	k1gInSc1	Štefánik
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
stipendium	stipendium	k1gNnSc4	stipendium
od	od	k7c2	od
Českoslovanské	českoslovanský	k2eAgFnSc2d1	českoslovanská
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
studií	studie	k1gFnPc2	studie
začal	začít	k5eAaPmAgMnS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Janem	Jan	k1gMnSc7	Jan
Kraiczem	Kraicz	k1gMnSc7	Kraicz
i	i	k8xC	i
spolek	spolek	k1gInSc4	spolek
evangelických	evangelický	k2eAgInPc2d1	evangelický
akademiků	akademik	k1gMnPc2	akademik
Jeroným	Jeroným	k1gMnSc1	Jeroným
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
členem	člen	k1gMnSc7	člen
podpůrného	podpůrný	k2eAgInSc2d1	podpůrný
spolku	spolek	k1gInSc2	spolek
Radhošť	Radhošť	k1gMnSc1	Radhošť
<g/>
.	.	kIx.	.
</s>
<s>
Štefánik	Štefánik	k1gMnSc1	Štefánik
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stal	stát	k5eAaPmAgInS	stát
hlasistou	hlasista	k1gMnSc7	hlasista
a	a	k8xC	a
stoupencem	stoupenec	k1gMnSc7	stoupenec
myšlenek	myšlenka	k1gFnPc2	myšlenka
profesora	profesor	k1gMnSc2	profesor
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1898	[number]	k4	1898
se	se	k3xPyFc4	se
Štefánik	Štefánik	k1gMnSc1	Štefánik
stal	stát	k5eAaPmAgMnS	stát
tajemníkem	tajemník	k1gMnSc7	tajemník
Detvanu	Detvan	k1gMnSc3	Detvan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
zlom	zlom	k1gInSc1	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prudké	prudký	k2eAgFnSc6d1	prudká
roztržce	roztržka	k1gFnSc6	roztržka
o	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
otec	otec	k1gMnSc1	otec
mladého	mladý	k2eAgMnSc2d1	mladý
Štefánika	Štefánik	k1gMnSc2	Štefánik
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
příteli	přítel	k1gMnSc3	přítel
Vavrovi	Vavr	k1gMnSc3	Vavr
Šrobárovi	Šrobár	k1gMnSc3	Šrobár
do	do	k7c2	do
Ružomberka	Ružomberk	k1gInSc2	Ružomberk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
zapsat	zapsat	k5eAaPmF	zapsat
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
astronomie	astronomie	k1gFnSc2	astronomie
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
obor	obor	k1gInSc1	obor
se	se	k3xPyFc4	se
onehdy	onehdy	k6eAd1	onehdy
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
)	)	kIx)	)
a	a	k8xC	a
přes	přes	k7c4	přes
počínající	počínající	k2eAgInPc4d1	počínající
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
žaludkem	žaludek	k1gInSc7	žaludek
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
i	i	k9	i
usmířil	usmířit	k5eAaPmAgInS	usmířit
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
mezi	mezi	k7c7	mezi
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
studenty	student	k1gMnPc7	student
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
získával	získávat	k5eAaImAgMnS	získávat
autoritu	autorita	k1gFnSc4	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1901	[number]	k4	1901
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Detvanu	Detvan	k1gMnSc3	Detvan
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
neshodám	neshoda	k1gFnPc3	neshoda
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
spolku	spolek	k1gInSc2	spolek
však	však	k9	však
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
nerad	nerad	k2eAgMnSc1d1	nerad
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
Jurajem	Juraj	k1gMnSc7	Juraj
Nerádem	Nerád	k1gMnSc7	Nerád
a	a	k8xC	a
Zigem	Zig	k1gMnSc7	Zig
Zigmundíkem	Zigmundík	k1gMnSc7	Zigmundík
z	z	k7c2	z
Detvanu	Detvan	k1gMnSc3	Detvan
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letním	letní	k2eAgInSc6d1	letní
semestru	semestr	k1gInSc6	semestr
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
odešel	odejít	k5eAaPmAgMnS	odejít
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Curychu	Curych	k1gInSc2	Curych
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
prostředí	prostředí	k1gNnSc6	prostředí
získal	získat	k5eAaPmAgMnS	získat
i	i	k9	i
nové	nový	k2eAgInPc4d1	nový
kontakty	kontakt	k1gInPc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1902	[number]	k4	1902
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
Detvanu	Detvan	k1gMnSc6	Detvan
a	a	k8xC	a
na	na	k7c6	na
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
začal	začít	k5eAaPmAgMnS	začít
V.	V.	kA	V.
Šrobár	Šrobár	k1gMnSc1	Šrobár
vydávat	vydávat	k5eAaImF	vydávat
časopis	časopis	k1gInSc4	časopis
Hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
mu	on	k3xPp3gMnSc3	on
Štefánik	Štefánik	k1gInSc4	Štefánik
při	při	k7c6	při
redigování	redigování	k1gNnSc6	redigování
jeho	jeho	k3xOp3gFnSc2	jeho
přílohy	příloha	k1gFnSc2	příloha
Umelecký	Umelecký	k2eAgInSc4d1	Umelecký
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
literatuře	literatura	k1gFnSc3	literatura
a	a	k8xC	a
umění	umění	k1gNnSc3	umění
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
publicista	publicista	k1gMnSc1	publicista
byl	být	k5eAaImAgMnS	být
činný	činný	k2eAgMnSc1d1	činný
i	i	k9	i
jinde	jinde	k6eAd1	jinde
–	–	k?	–
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
Jana	Jana	k1gFnSc1	Jana
Herbena	Herben	k2eAgFnSc1d1	Herbena
psal	psát	k5eAaImAgInS	psát
do	do	k7c2	do
realistického	realistický	k2eAgInSc2d1	realistický
časopisu	časopis	k1gInSc2	časopis
Čas	čas	k1gInSc4	čas
pravidelné	pravidelný	k2eAgInPc4d1	pravidelný
pondělní	pondělní	k2eAgInPc4d1	pondělní
úvodníky	úvodník	k1gInPc4	úvodník
o	o	k7c6	o
slovenské	slovenský	k2eAgFnSc6d1	slovenská
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
informovat	informovat	k5eAaBmF	informovat
českou	český	k2eAgFnSc4d1	Česká
veřejnost	veřejnost	k1gFnSc4	veřejnost
o	o	k7c6	o
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Upozorňoval	upozorňovat	k5eAaImAgMnS	upozorňovat
především	především	k9	především
na	na	k7c4	na
postupující	postupující	k2eAgFnSc4d1	postupující
maďarizaci	maďarizace	k1gFnSc4	maďarizace
a	a	k8xC	a
nabádal	nabádat	k5eAaImAgInS	nabádat
českou	český	k2eAgFnSc4d1	Česká
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
Slovensku	Slovensko	k1gNnSc3	Slovensko
konkrétními	konkrétní	k2eAgInPc7d1	konkrétní
činy	čin	k1gInPc7	čin
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
předseda	předseda	k1gMnSc1	předseda
Detvanu	Detvan	k1gMnSc3	Detvan
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
sjezdu	sjezd	k1gInSc2	sjezd
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
studentské	studentský	k2eAgFnSc2d1	studentská
organizace	organizace	k1gFnSc2	organizace
Corda	Corda	k1gMnSc1	Corda
Fratres	Fratres	k1gMnSc1	Fratres
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
Palermu	Palermo	k1gNnSc6	Palermo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
potom	potom	k6eAd1	potom
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
Luhačovicích	Luhačovice	k1gFnPc6	Luhačovice
Pavla	Pavel	k1gMnSc4	Pavel
Blahu	Blaha	k1gMnSc4	Blaha
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
také	také	k9	také
otevření	otevření	k1gNnSc3	otevření
první	první	k4xOgFnSc2	první
výstavy	výstava	k1gFnSc2	výstava
Grupy	grupa	k1gFnSc2	grupa
uhorskoslovenských	uhorskoslovenský	k2eAgNnPc2d1	uhorskoslovenský
maliarov	maliarovo	k1gNnPc2	maliarovo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
výsledkem	výsledek	k1gInSc7	výsledek
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
kulturní	kulturní	k2eAgFnSc2d1	kulturní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Štefánikových	Štefánikův	k2eAgFnPc2d1	Štefánikova
aktivit	aktivita	k1gFnPc2	aktivita
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
i	i	k8xC	i
návštěvy	návštěva	k1gFnPc4	návštěva
u	u	k7c2	u
historika	historik	k1gMnSc2	historik
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Golla	Goll	k1gMnSc2	Goll
a	a	k8xC	a
básníka	básník	k1gMnSc2	básník
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
plně	plně	k6eAd1	plně
věnoval	věnovat	k5eAaPmAgInS	věnovat
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
disertační	disertační	k2eAgFnSc1d1	disertační
práce	práce	k1gFnSc1	práce
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
O	o	k7c6	o
nové	nový	k2eAgFnSc6d1	nová
hvězdě	hvězda	k1gFnSc6	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Cassiopea	Cassiope	k1gInSc2	Cassiope
objevené	objevený	k2eAgInPc1d1	objevený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1572	[number]	k4	1572
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
,	,	kIx,	,
složil	složit	k5eAaPmAgMnS	složit
předepsané	předepsaný	k2eAgFnPc4d1	předepsaná
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1904	[number]	k4	1904
byl	být	k5eAaImAgMnS	být
promován	promovat	k5eAaBmNgMnS	promovat
na	na	k7c4	na
doktora	doktor	k1gMnSc4	doktor
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
dalšího	další	k2eAgNnSc2d1	další
Štefánikova	Štefánikův	k2eAgNnSc2d1	Štefánikovo
působení	působení	k1gNnSc2	působení
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Začátky	začátek	k1gInPc1	začátek
byly	být	k5eAaImAgInP	být
těžké	těžký	k2eAgInPc4d1	těžký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
V.	V.	kA	V.
Šrobár	Šrobár	k1gMnSc1	Šrobár
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
získat	získat	k5eAaPmF	získat
půjčku	půjčka	k1gFnSc4	půjčka
v	v	k7c6	v
ružomberské	ružomberský	k2eAgFnSc6d1	Ružomberská
bance	banka	k1gFnSc6	banka
<g/>
.	.	kIx.	.
</s>
<s>
Štefánikovým	Štefánikův	k2eAgInSc7d1	Štefánikův
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
ke	k	k7c3	k
dvěma	dva	k4xCgMnPc3	dva
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
astronomům	astronom	k1gMnPc3	astronom
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Flammarionovi	Flammarion	k1gMnSc3	Flammarion
a	a	k8xC	a
Janssenovi	Janssen	k1gMnSc3	Janssen
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
naděje	naděje	k1gFnPc1	naděje
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nesplnila	splnit	k5eNaPmAgFnS	splnit
a	a	k8xC	a
musel	muset	k5eAaImAgInS	muset
čekat	čekat	k5eAaImF	čekat
až	až	k9	až
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	s	k7c7	s
Štefánikovými	Štefánikův	k2eAgInPc7d1	Štefánikův
druhy	druh	k1gInPc7	druh
stali	stát	k5eAaPmAgMnP	stát
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
českých	český	k2eAgMnPc2d1	český
umělců	umělec	k1gMnPc2	umělec
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
sochaři	sochař	k1gMnPc1	sochař
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kafka	Kafka	k1gMnSc1	Kafka
a	a	k8xC	a
Otakar	Otakar	k1gMnSc1	Otakar
Španiel	Španiel	k1gMnSc1	Španiel
<g/>
,	,	kIx,	,
malíři	malíř	k1gMnPc1	malíř
Ludvík	Ludvík	k1gMnSc1	Ludvík
Strimpl	Strimpl	k1gMnSc1	Strimpl
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
František	František	k1gMnSc1	František
Šimon	Šimon	k1gMnSc1	Šimon
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Boettinger	Boettinger	k1gMnSc1	Boettinger
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spřátelil	spřátelit	k5eAaPmAgInS	spřátelit
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
hrabětem	hrabě	k1gMnSc7	hrabě
Hanušem	Hanuš	k1gMnSc7	Hanuš
Kolowratem	Kolowrat	k1gMnSc7	Kolowrat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
právě	právě	k9	právě
stal	stát	k5eAaPmAgInS	stát
rakousko-uherským	rakouskoherský	k2eAgMnSc7d1	rakousko-uherský
vojenským	vojenský	k2eAgMnSc7d1	vojenský
atašé	atašé	k1gMnSc7	atašé
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
května	květen	k1gInSc2	květen
1905	[number]	k4	1905
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
profesor	profesor	k1gMnSc1	profesor
Janssen	Janssen	k1gInSc1	Janssen
<g/>
.	.	kIx.	.
</s>
<s>
Štefánikovi	Štefánik	k1gMnSc3	Štefánik
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
hvězdárnu	hvězdárna	k1gFnSc4	hvězdárna
v	v	k7c6	v
Meudonu	Meudon	k1gInSc6	Meudon
<g/>
.	.	kIx.	.
</s>
<s>
Janssen	Janssen	k1gInSc1	Janssen
byl	být	k5eAaImAgInS	být
Štefánikem	Štefánik	k1gInSc7	Štefánik
upoután	upoutat	k5eAaPmNgInS	upoutat
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
jeho	jeho	k3xOp3gInSc4	jeho
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
host	host	k1gMnSc1	host
v	v	k7c6	v
Meudonské	Meudonský	k2eAgFnSc6d1	Meudonský
hvězdárně	hvězdárna	k1gFnSc6	hvězdárna
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Štefánik	Štefánik	k1gMnSc1	Štefánik
různé	různý	k2eAgFnSc2d1	různá
výpravy	výprava	k1gFnSc2	výprava
(	(	kIx(	(
<g/>
např.	např.	kA	např.
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1905	[number]	k4	1905
podnikl	podniknout	k5eAaPmAgMnS	podniknout
výstup	výstup	k1gInSc4	výstup
do	do	k7c2	do
observatoře	observatoř	k1gFnSc2	observatoř
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
Mont	Monta	k1gFnPc2	Monta
Blanku	Blanka	k1gFnSc4	Blanka
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Alcosebře	Alcosebra	k1gFnSc6	Alcosebra
úplné	úplný	k2eAgNnSc4d1	úplné
zatmění	zatmění	k1gNnSc4	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
přednesl	přednést	k5eAaPmAgMnS	přednést
Janssen	Janssen	k1gInSc4	Janssen
Štefánikovu	Štefánikův	k2eAgFnSc4d1	Štefánikova
studii	studie	k1gFnSc4	studie
Spektroskopické	spektroskopický	k2eAgNnSc1d1	spektroskopické
zkoumání	zkoumání	k1gNnSc1	zkoumání
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
Alcosebře	Alcosebra	k1gFnSc6	Alcosebra
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
ji	on	k3xPp3gFnSc4	on
i	i	k8xC	i
časopis	časopis	k1gInSc4	časopis
Comptes	Comptesa	k1gFnPc2	Comptesa
Rendus	Rendus	k1gMnSc1	Rendus
Hebdomadaires	Hebdomadaires	k1gMnSc1	Hebdomadaires
des	des	k1gNnSc2	des
Sciences	Sciences	k1gMnSc1	Sciences
de	de	k?	de
l	l	kA	l
<g/>
́	́	k?	́
<g/>
Academie	academia	k1gFnSc2	academia
des	des	k1gNnSc2	des
Sciences	Sciencesa	k1gFnPc2	Sciencesa
<g/>
.	.	kIx.	.
</s>
<s>
Vědecky	vědecky	k6eAd1	vědecky
nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Štefánika	Štefánik	k1gMnSc4	Štefánik
rok	rok	k1gInSc4	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
když	když	k8xS	když
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
sedm	sedm	k4xCc4	sedm
svých	svůj	k3xOyFgFnPc2	svůj
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgInS	zařadit
do	do	k7c2	do
pařížského	pařížský	k2eAgInSc2d1	pařížský
vědeckého	vědecký	k2eAgInSc2d1	vědecký
života	život	k1gInSc2	život
a	a	k8xC	a
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
studentkou	studentka	k1gFnSc7	studentka
Marií	Maria	k1gFnSc7	Maria
Neumanovou	Neumanová	k1gFnSc7	Neumanová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
důvěrnou	důvěrný	k2eAgFnSc7d1	důvěrná
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
osmdesátiletého	osmdesátiletý	k2eAgMnSc4d1	osmdesátiletý
Janssena	Janssen	k2eAgMnSc4d1	Janssen
však	však	k9	však
nový	nový	k2eAgMnSc1d1	nový
ředitel	ředitel	k1gMnSc1	ředitel
donutil	donutit	k5eAaPmAgMnS	donutit
Štefánika	Štefánik	k1gMnSc4	Štefánik
z	z	k7c2	z
Meudonské	Meudonský	k2eAgFnSc2d1	Meudonský
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
dostal	dostat	k5eAaPmAgInS	dostat
Štefánik	Štefánik	k1gInSc1	Štefánik
pověření	pověření	k1gNnSc2	pověření
od	od	k7c2	od
Bureau	Bureaus	k1gInSc2	Bureaus
des	des	k1gNnPc2	des
Longitudes	Longitudes	k1gInSc4	Longitudes
vést	vést	k5eAaImF	vést
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Turkestánu	Turkestán	k1gInSc2	Turkestán
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
pozorovat	pozorovat	k5eAaImF	pozorovat
zatmění	zatmění	k1gNnSc1	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
do	do	k7c2	do
Turkestánu	Turkestán	k1gInSc2	Turkestán
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgMnS	zastavit
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
Pulkovskou	Pulkovský	k2eAgFnSc4d1	Pulkovský
hvězdárnu	hvězdárna	k1gFnSc4	hvězdárna
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Cestu	cesta	k1gFnSc4	cesta
využil	využít	k5eAaPmAgMnS	využít
i	i	k9	i
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Jasné	jasný	k2eAgFnSc6d1	jasná
Poljaně	Poljana	k1gFnSc6	Poljana
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Lva	lev	k1gInSc2	lev
Nikolajeviče	Nikolajevič	k1gMnSc2	Nikolajevič
Tolstého	Tolstý	k2eAgMnSc2d1	Tolstý
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
lékaře	lékař	k1gMnSc2	lékař
Dušana	Dušan	k1gMnSc2	Dušan
Makovického	Makovický	k2eAgMnSc2d1	Makovický
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
Tolstého	Tolstý	k2eAgMnSc4d1	Tolstý
výrok	výrok	k1gInSc4	výrok
o	o	k7c6	o
Štefánikovi	Štefánik	k1gMnSc6	Štefánik
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
úžasně	úžasně	k6eAd1	úžasně
srdečný	srdečný	k2eAgMnSc1d1	srdečný
<g/>
,	,	kIx,	,
milý	milý	k2eAgMnSc1d1	milý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
mu	on	k3xPp3gMnSc3	on
valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
astronomické	astronomický	k2eAgFnSc2d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
udělilo	udělit	k5eAaPmAgNnS	udělit
Janssenovu	Janssenův	k2eAgFnSc4d1	Janssenova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
se	se	k3xPyFc4	se
však	však	k9	však
Štefánikův	Štefánikův	k2eAgInSc1d1	Štefánikův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
velmi	velmi	k6eAd1	velmi
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
převezen	převézt	k5eAaPmNgMnS	převézt
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
do	do	k7c2	do
Chamonix	Chamonix	k1gNnSc2	Chamonix
<g/>
,	,	kIx,	,
lázeňského	lázeňský	k2eAgNnSc2d1	lázeňské
města	město	k1gNnSc2	město
pod	pod	k7c4	pod
Mont	Mont	k1gInSc4	Mont
Blankem	Blanek	k1gInSc7	Blanek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
léčil	léčit	k5eAaImAgMnS	léčit
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léčení	léčení	k1gNnSc2	léčení
ho	on	k3xPp3gMnSc4	on
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
profesor	profesor	k1gMnSc1	profesor
Janssen	Janssen	k1gInSc4	Janssen
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
bojoval	bojovat	k5eAaImAgMnS	bojovat
dlouho	dlouho	k6eAd1	dlouho
s	s	k7c7	s
existenčními	existenční	k2eAgInPc7d1	existenční
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
také	také	k9	také
snažil	snažit	k5eAaImAgMnS	snažit
zachránit	zachránit	k5eAaPmF	zachránit
Janssenovu	Janssenův	k2eAgFnSc4d1	Janssenova
observatoř	observatoř	k1gFnSc4	observatoř
na	na	k7c4	na
Mont	Mont	k1gInSc4	Mont
Blanku	Blanka	k1gFnSc4	Blanka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
observatoř	observatoř	k1gFnSc1	observatoř
rozebrána	rozebrán	k2eAgFnSc1d1	rozebrána
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Štefánik	Štefánik	k1gMnSc1	Štefánik
snažil	snažit	k5eAaImAgMnS	snažit
vybudovat	vybudovat	k5eAaPmF	vybudovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
observatoř	observatoř	k1gFnSc4	observatoř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnPc1	jeho
finanční	finanční	k2eAgFnPc1d1	finanční
situace	situace	k1gFnPc1	situace
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
nedovolovala	dovolovat	k5eNaImAgFnS	dovolovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
problémem	problém	k1gInSc7	problém
mu	on	k3xPp3gMnSc3	on
tehdy	tehdy	k6eAd1	tehdy
nejvíce	nejvíce	k6eAd1	nejvíce
pomohl	pomoct	k5eAaPmAgMnS	pomoct
senátor	senátor	k1gMnSc1	senátor
Émile	Émile	k1gFnSc2	Émile
Chautemps	Chautemps	k1gInSc1	Chautemps
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
zorganizoval	zorganizovat	k5eAaPmAgInS	zorganizovat
Štefánik	Štefánik	k1gInSc1	Štefánik
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chtěl	chtít	k5eAaImAgMnS	chtít
najít	najít	k5eAaPmF	najít
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
hvězdárnu	hvězdárna	k1gFnSc4	hvězdárna
<g/>
.	.	kIx.	.
</s>
<s>
Procestoval	procestovat	k5eAaPmAgMnS	procestovat
Alžírsko	Alžírsko	k1gNnSc4	Alžírsko
<g/>
,	,	kIx,	,
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Atlas	Atlas	k1gInSc1	Atlas
<g/>
,	,	kIx,	,
Saharu	Sahara	k1gFnSc4	Sahara
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc4	Tunisko
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
neměla	mít	k5eNaImAgFnS	mít
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
dostal	dostat	k5eAaPmAgMnS	dostat
novou	nový	k2eAgFnSc4d1	nová
šanci	šance	k1gFnSc4	šance
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
ústav	ústav	k1gInSc1	ústav
Bureau	Bureaus	k1gInSc2	Bureaus
des	des	k1gNnSc2	des
Longitudes	Longitudesa	k1gFnPc2	Longitudesa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
meteorologickým	meteorologický	k2eAgInSc7d1	meteorologický
ústavem	ústav	k1gInSc7	ústav
(	(	kIx(	(
<g/>
Bureau	Bureaa	k1gFnSc4	Bureaa
Central	Central	k1gFnSc2	Central
Météorologique	Météorologiqu	k1gFnSc2	Météorologiqu
<g/>
)	)	kIx)	)
jej	on	k3xPp3gMnSc4	on
vyslaly	vyslat	k5eAaPmAgInP	vyslat
na	na	k7c4	na
Tahiti	Tahiti	k1gNnSc4	Tahiti
pozorovat	pozorovat	k5eAaImF	pozorovat
Halleyovu	Halleyův	k2eAgFnSc4d1	Halleyova
kometu	kometa	k1gFnSc4	kometa
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přistála	přistát	k5eAaImAgFnS	přistát
jeho	jeho	k3xOp3gFnSc4	jeho
loď	loď	k1gFnSc4	loď
v	v	k7c6	v
tahitském	tahitský	k2eAgInSc6d1	tahitský
přístavu	přístav	k1gInSc6	přístav
Papeete	Papee	k1gNnSc2	Papee
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
strávil	strávit	k5eAaPmAgMnS	strávit
následujících	následující	k2eAgNnPc2d1	následující
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
úplného	úplný	k2eAgNnSc2d1	úplné
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1911	[number]	k4	1911
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Vavau	Vavaus	k1gInSc2	Vavaus
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
právě	právě	k9	právě
Štefánikova	Štefánikův	k2eAgFnSc1d1	Štefánikova
výprava	výprava	k1gFnSc1	výprava
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ocenila	ocenit	k5eAaPmAgFnS	ocenit
i	i	k9	i
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
ho	on	k3xPp3gMnSc4	on
Bureau	Bureaa	k1gMnSc4	Bureaa
des	des	k1gNnSc7	des
Longitudes	Longitudes	k1gMnSc1	Longitudes
vyslal	vyslat	k5eAaPmAgMnS	vyslat
na	na	k7c4	na
pozorování	pozorování	k1gNnSc4	pozorování
zatmění	zatmění	k1gNnSc2	zatmění
Slunce	slunce	k1gNnSc2	slunce
do	do	k7c2	do
Passa	Passa	k1gFnSc1	Passa
Quatro	Quatro	k1gNnSc4	Quatro
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
výpravy	výprava	k1gFnSc2	výprava
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
poté	poté	k6eAd1	poté
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
rodnou	rodný	k2eAgFnSc4d1	rodná
obec	obec	k1gFnSc4	obec
Košariská	Košariská	k1gFnSc1	Košariská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
umírá	umírat	k5eAaImIp3nS	umírat
jeho	jeho	k3xOp3gMnSc7	jeho
otec	otec	k1gMnSc1	otec
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
Štefánik	Štefánik	k1gMnSc1	Štefánik
plánoval	plánovat	k5eAaImAgMnS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
trvale	trvale	k6eAd1	trvale
usídlí	usídlit	k5eAaPmIp3nS	usídlit
na	na	k7c4	na
Tahiti	Tahiti	k1gNnSc4	Tahiti
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pověření	pověření	k1gNnSc3	pověření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
francouzské	francouzský	k2eAgFnSc2d1	francouzská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
změnil	změnit	k5eAaPmAgMnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
totiž	totiž	k9	totiž
chtěli	chtít	k5eAaImAgMnP	chtít
vybudovat	vybudovat	k5eAaPmF	vybudovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
telegrafickou	telegrafický	k2eAgFnSc4d1	telegrafická
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
soustavu	soustava	k1gFnSc4	soustava
meteorologických	meteorologický	k2eAgFnPc2d1	meteorologická
stanic	stanice	k1gFnPc2	stanice
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
a	a	k8xC	a
na	na	k7c6	na
Galapágách	Galapágy	k1gFnPc6	Galapágy
a	a	k8xC	a
Štefánik	Štefánik	k1gInSc4	Štefánik
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
měl	mít	k5eAaImAgMnS	mít
francouzské	francouzský	k2eAgNnSc4d1	francouzské
občanství	občanství	k1gNnSc4	občanství
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
získat	získat	k5eAaPmF	získat
povolení	povolení	k1gNnSc4	povolení
od	od	k7c2	od
ekvádorské	ekvádorský	k2eAgFnSc2d1	ekvádorská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
podařilo	podařit	k5eAaPmAgNnS	podařit
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
námořnictví	námořnictví	k1gNnSc2	námořnictví
udělila	udělit	k5eAaPmAgFnS	udělit
kříž	kříž	k1gInSc4	kříž
Rytíře	Rytíř	k1gMnSc2	Rytíř
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
úspěchy	úspěch	k1gInPc1	úspěch
však	však	k9	však
opět	opět	k6eAd1	opět
zastavila	zastavit	k5eAaPmAgFnS	zastavit
choroba	choroba	k1gFnSc1	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1914	[number]	k4	1914
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
podrobit	podrobit	k5eAaPmF	podrobit
operaci	operace	k1gFnSc4	operace
žaludku	žaludek	k1gInSc2	žaludek
v	v	k7c6	v
sanatoriu	sanatorium	k1gNnSc6	sanatorium
u	u	k7c2	u
prof.	prof.	kA	prof.
Monprofita	Monprofit	k2eAgFnSc1d1	Monprofit
v	v	k7c4	v
Yngerse	Yngerse	k1gFnPc4	Yngerse
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzdravení	uzdravení	k1gNnSc6	uzdravení
ho	on	k3xPp3gMnSc4	on
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
vypuknutí	vypuknutí	k1gNnSc4	vypuknutí
války	válka	k1gFnSc2	válka
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
vojenským	vojenský	k2eAgInSc7d1	vojenský
transportem	transport	k1gInSc7	transport
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
války	válka	k1gFnSc2	válka
Štefánika	Štefánik	k1gMnSc2	Štefánik
nepřekvapil	překvapit	k5eNaPmAgMnS	překvapit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
předvídal	předvídat	k5eAaImAgMnS	předvídat
už	už	k9	už
několik	několik	k4yIc4	několik
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
viděl	vidět	k5eAaImAgMnS	vidět
hlavně	hlavně	k9	hlavně
možnost	možnost	k1gFnSc4	možnost
osamostatnění	osamostatnění	k1gNnSc2	osamostatnění
Slováků	Slováky	k1gInPc2	Slováky
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
hned	hned	k6eAd1	hned
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
spojoval	spojovat	k5eAaImAgInS	spojovat
i	i	k9	i
s	s	k7c7	s
Čechy	Čech	k1gMnPc7	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
špatnému	špatný	k2eAgInSc3d1	špatný
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
však	však	k9	však
nemohl	moct	k5eNaImAgInS	moct
hned	hned	k6eAd1	hned
odejít	odejít	k5eAaPmF	odejít
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
až	až	k6eAd1	až
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
vojenské	vojenský	k2eAgFnSc2d1	vojenská
letecké	letecký	k2eAgFnSc2d1	letecká
školy	škola	k1gFnSc2	škola
v	v	k7c4	v
Chartres	Chartres	k1gInSc4	Chartres
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
získal	získat	k5eAaPmAgMnS	získat
diplom	diplom	k1gInSc4	diplom
pilota	pilot	k1gMnSc2	pilot
a	a	k8xC	a
hodnost	hodnost	k1gFnSc4	hodnost
desátníka	desátník	k1gMnSc2	desátník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
podporučíka	podporučík	k1gMnSc2	podporučík
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dělal	dělat	k5eAaImAgMnS	dělat
průzkumné	průzkumný	k2eAgInPc4d1	průzkumný
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Operoval	operovat	k5eAaImAgMnS	operovat
mj.	mj.	kA	mj.
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Artois	Artois	k1gFnSc2	Artois
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
u	u	k7c2	u
Arrasu	Arras	k1gInSc2	Arras
rozprášené	rozprášený	k2eAgFnSc2d1	rozprášená
roty	rota	k1gFnSc2	rota
Nazdar	nazdar	k0	nazdar
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jako	jako	k8xC	jako
letec	letec	k1gMnSc1	letec
měl	mít	k5eAaImAgMnS	mít
neustále	neustále	k6eAd1	neustále
na	na	k7c6	na
zřeteli	zřetel	k1gInSc6	zřetel
osamostatnění	osamostatnění	k1gNnSc2	osamostatnění
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
dobrovolnické	dobrovolnický	k2eAgFnSc2d1	dobrovolnická
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1915	[number]	k4	1915
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
bojovým	bojový	k2eAgInPc3d1	bojový
úspěchům	úspěch	k1gInPc3	úspěch
a	a	k8xC	a
průkopnické	průkopnický	k2eAgFnSc3d1	průkopnická
práci	práce	k1gFnSc3	práce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vojenské	vojenský	k2eAgFnSc2d1	vojenská
meteorologie	meteorologie	k1gFnSc2	meteorologie
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
místo	místo	k1gNnSc1	místo
velitele	velitel	k1gMnSc2	velitel
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
služby	služba	k1gFnSc2	služba
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Štefánik	Štefánik	k1gMnSc1	Štefánik
to	ten	k3xDgNnSc4	ten
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
opět	opět	k6eAd1	opět
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
přeložení	přeložení	k1gNnSc4	přeložení
do	do	k7c2	do
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
září	září	k1gNnSc2	září
1915	[number]	k4	1915
ho	on	k3xPp3gMnSc4	on
poslali	poslat	k5eAaPmAgMnP	poslat
na	na	k7c4	na
srbskou	srbský	k2eAgFnSc4d1	Srbská
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
toto	tento	k3xDgNnSc4	tento
své	svůj	k3xOyFgNnSc4	svůj
snažení	snažení	k1gNnSc4	snažení
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
evakuaci	evakuace	k1gFnSc6	evakuace
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Niši	Niš	k1gFnSc6	Niš
však	však	k9	však
s	s	k7c7	s
letadlem	letadlo	k1gNnSc7	letadlo
havaroval	havarovat	k5eAaPmAgMnS	havarovat
a	a	k8xC	a
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
ho	on	k3xPp3gNnSc4	on
opět	opět	k6eAd1	opět
přepadla	přepadnout	k5eAaPmAgFnS	přepadnout
žaludeční	žaludeční	k2eAgFnSc1d1	žaludeční
choroba	choroba	k1gFnSc1	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
mu	on	k3xPp3gMnSc3	on
tehdy	tehdy	k6eAd1	tehdy
zachránili	zachránit	k5eAaPmAgMnP	zachránit
přátelé	přítel	k1gMnPc1	přítel
Raoul	Raoula	k1gFnPc2	Raoula
Labry	Labra	k1gFnSc2	Labra
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Bourdon	bourdon	k1gInSc1	bourdon
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ho	on	k3xPp3gMnSc4	on
dopravili	dopravit	k5eAaPmAgMnP	dopravit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
do	do	k7c2	do
tamní	tamní	k2eAgFnSc2d1	tamní
Nemocnice	nemocnice	k1gFnSc2	nemocnice
královny	královna	k1gFnSc2	královna
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
Štefánik	Štefánik	k1gMnSc1	Štefánik
poznal	poznat	k5eAaPmAgMnS	poznat
paní	paní	k1gFnSc4	paní
Claire	Clair	k1gInSc5	Clair
de	de	k?	de
Jouvenel	Jouvenlo	k1gNnPc2	Jouvenlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
horlivě	horlivě	k6eAd1	horlivě
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
boji	boj	k1gInSc6	boj
za	za	k7c4	za
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Československá	československý	k2eAgFnSc1d1	Československá
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Štefánik	Štefánik	k1gMnSc1	Štefánik
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gInSc4	on
Jouvenel	Jouvenel	k1gInSc4	Jouvenel
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
politiky	politik	k1gMnPc7	politik
<g/>
:	:	kIx,	:
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
Aristide	Aristid	k1gInSc5	Aristid
Briandem	Briando	k1gNnSc7	Briando
a	a	k8xC	a
nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
mužem	muž	k1gMnSc7	muž
na	na	k7c6	na
francouzském	francouzský	k2eAgNnSc6d1	francouzské
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
Philippem	Philipp	k1gMnSc7	Philipp
Berthelotem	Berthelot	k1gMnSc7	Berthelot
<g/>
.	.	kIx.	.
</s>
<s>
Štefánik	Štefánik	k1gMnSc1	Štefánik
tu	tu	k6eAd1	tu
nadále	nadále	k6eAd1	nadále
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
plán	plán	k1gInSc4	plán
vytvoření	vytvoření	k1gNnSc2	vytvoření
česko-slovenského	českolovenský	k2eAgInSc2d1	česko-slovenský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1915	[number]	k4	1915
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
E.	E.	kA	E.
Benešem	Beneš	k1gMnSc7	Beneš
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaBmAgMnP	shodnout
na	na	k7c6	na
Štefánikově	Štefánikův	k2eAgInSc6d1	Štefánikův
a	a	k8xC	a
Masarykově	Masarykův	k2eAgInSc6d1	Masarykův
koncepcí	koncepce	k1gFnSc7	koncepce
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Štefánikovou	Štefánikův	k2eAgFnSc7d1	Štefánikova
novou	nový	k2eAgFnSc7d1	nová
úlohou	úloha	k1gFnSc7	úloha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
stanovil	stanovit	k5eAaPmAgMnS	stanovit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
jednoho	jeden	k4xCgNnSc2	jeden
řídícího	řídící	k2eAgNnSc2d1	řídící
centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
společný	společný	k2eAgInSc4d1	společný
odboj	odboj	k1gInSc4	odboj
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
a	a	k8xC	a
též	též	k9	též
vytvoření	vytvoření	k1gNnSc6	vytvoření
samostatného	samostatný	k2eAgNnSc2d1	samostatné
česko-slovenského	českolovenský	k2eAgNnSc2d1	česko-slovenské
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
prosazení	prosazení	k1gNnSc1	prosazení
mezi	mezi	k7c7	mezi
politiky	politik	k1gMnPc7	politik
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těchto	tento	k3xDgInPc6	tento
plánech	plán	k1gInPc6	plán
informoval	informovat	k5eAaBmAgMnS	informovat
také	také	k6eAd1	také
ministerského	ministerský	k2eAgMnSc4d1	ministerský
předsedu	předseda	k1gMnSc4	předseda
Aristida	Aristid	k1gMnSc2	Aristid
Brianda	Briando	k1gNnSc2	Briando
a	a	k8xC	a
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
jeho	jeho	k3xOp3gNnSc4	jeho
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
Masarykem	Masaryk	k1gMnSc7	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
opět	opět	k6eAd1	opět
začala	začít	k5eAaPmAgFnS	začít
Štefánika	Štefánik	k1gMnSc4	Štefánik
trápit	trápit	k5eAaImF	trápit
žaludeční	žaludeční	k2eAgFnSc1d1	žaludeční
choroba	choroba	k1gFnSc1	choroba
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
Masaryka	Masaryk	k1gMnSc2	Masaryk
s	s	k7c7	s
Briandem	Briando	k1gNnSc7	Briando
bylo	být	k5eAaImAgNnS	být
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
a	a	k8xC	a
Masaryk	Masaryk	k1gMnSc1	Masaryk
Brianda	Briando	k1gNnSc2	Briando
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
koncepci	koncepce	k1gFnSc4	koncepce
řešení	řešení	k1gNnSc2	řešení
středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
otázky	otázka	k1gFnSc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Štefánik	Štefánik	k1gMnSc1	Štefánik
mezi	mezi	k7c7	mezi
tím	ten	k3xDgNnSc7	ten
neustále	neustále	k6eAd1	neustále
podporoval	podporovat	k5eAaImAgInS	podporovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
ústředního	ústřední	k2eAgInSc2d1	ústřední
reprezentativního	reprezentativní	k2eAgInSc2d1	reprezentativní
orgánu	orgán	k1gInSc2	orgán
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
odboje	odboj	k1gInSc2	odboj
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1916	[number]	k4	1916
Československá	československý	k2eAgFnSc1d1	Československá
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
místopředsedy	místopředseda	k1gMnPc4	místopředseda
Josef	Josef	k1gMnSc1	Josef
Dürich	Dürich	k1gMnSc1	Dürich
a	a	k8xC	a
M.	M.	kA	M.
R.	R.	kA	R.
Štefánik	Štefánik	k1gMnSc1	Štefánik
a	a	k8xC	a
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
E.	E.	kA	E.
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
sídlo	sídlo	k1gNnSc1	sídlo
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Rue	Rue	k1gFnSc6	Rue
Bonaparte	bonapart	k1gInSc5	bonapart
18	[number]	k4	18
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
hlavním	hlavní	k2eAgInSc7d1	hlavní
tiskovým	tiskový	k2eAgInSc7d1	tiskový
orgánem	orgán	k1gInSc7	orgán
byly	být	k5eAaImAgInP	být
časopisy	časopis	k1gInPc1	časopis
La	la	k1gNnPc6	la
Nation	Nation	k1gInSc4	Nation
Tchéque	Tchéque	k1gFnSc1	Tchéque
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
národ	národ	k1gInSc1	národ
<g/>
)	)	kIx)	)
a	a	k8xC	a
Československá	československý	k2eAgFnSc1d1	Československá
samostatnost	samostatnost	k1gFnSc1	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
začala	začít	k5eAaPmAgFnS	začít
rada	rada	k1gFnSc1	rada
organizovat	organizovat	k5eAaBmF	organizovat
česko-slovenské	českolovenský	k2eAgNnSc4d1	česko-slovenské
vojsko	vojsko	k1gNnSc4	vojsko
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Štefánikův	Štefánikův	k2eAgInSc1d1	Štefánikův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
zlepšil	zlepšit	k5eAaPmAgInS	zlepšit
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jako	jako	k9	jako
letec	letec	k1gMnSc1	letec
na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
frontě	fronta	k1gFnSc6	fronta
rozhazoval	rozhazovat	k5eAaImAgMnS	rozhazovat
z	z	k7c2	z
letadla	letadlo	k1gNnSc2	letadlo
letáky	leták	k1gInPc4	leták
určené	určený	k2eAgInPc4d1	určený
především	především	k9	především
pro	pro	k7c4	pro
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
chtěl	chtít	k5eAaImAgInS	chtít
získat	získat	k5eAaPmF	získat
italské	italský	k2eAgInPc4d1	italský
vojenské	vojenský	k2eAgInPc4d1	vojenský
a	a	k8xC	a
politické	politický	k2eAgInPc4d1	politický
kruhy	kruh	k1gInPc4	kruh
pro	pro	k7c4	pro
česko-slovenskou	českolovenský	k2eAgFnSc4d1	česko-slovenská
koncepci	koncepce	k1gFnSc4	koncepce
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
i	i	k9	i
s	s	k7c7	s
vytvořením	vytvoření	k1gNnSc7	vytvoření
jugoslávského	jugoslávský	k2eAgInSc2d1	jugoslávský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Postoj	postoj	k1gInSc1	postoj
Itálie	Itálie	k1gFnSc2	Itálie
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
jihoslovanského	jihoslovanský	k2eAgInSc2d1	jihoslovanský
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgInS	být
však	však	k9	však
odmítavý	odmítavý	k2eAgInSc1d1	odmítavý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
Štefánik	Štefánik	k1gMnSc1	Štefánik
plně	plně	k6eAd1	plně
věnoval	věnovat	k5eAaPmAgMnS	věnovat
ustanovení	ustanovení	k1gNnSc4	ustanovení
samostatného	samostatný	k2eAgNnSc2d1	samostatné
česko-slovenského	českolovenský	k2eAgNnSc2d1	česko-slovenské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1916	[number]	k4	1916
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
Mogiľove	Mogiľov	k1gInSc5	Mogiľov
k	k	k7c3	k
Maurice	Maurika	k1gFnSc3	Maurika
Janinovi	Janinův	k2eAgMnPc1d1	Janinův
<g/>
,	,	kIx,	,
šéfovi	šéf	k1gMnSc3	šéf
francouzské	francouzský	k2eAgFnSc2d1	francouzská
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Janin	Janin	k2eAgInSc1d1	Janin
Štefánika	Štefánik	k1gMnSc2	Štefánik
zavedl	zavést	k5eAaPmAgMnS	zavést
k	k	k7c3	k
náčelníkovi	náčelník	k1gMnSc3	náčelník
generálního	generální	k2eAgMnSc2d1	generální
štábu	štáb	k1gInSc6	štáb
Alexejevovi	Alexejevův	k2eAgMnPc1d1	Alexejevův
a	a	k8xC	a
i	i	k9	i
k	k	k7c3	k
caru	car	k1gMnSc3	car
Mikulášovi	Mikuláš	k1gMnSc3	Mikuláš
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
posílit	posílit	k5eAaPmF	posílit
postavení	postavení	k1gNnSc4	postavení
ČSNR	ČSNR	kA	ČSNR
ve	v	k7c6	v
vojenských	vojenský	k2eAgInPc6d1	vojenský
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
podepsali	podepsat	k5eAaPmAgMnP	podepsat
Dürich	Dürich	k1gMnSc1	Dürich
a	a	k8xC	a
Štefánik	Štefánik	k1gMnSc1	Štefánik
tzv.	tzv.	kA	tzv.
Kyjevskou	kyjevský	k2eAgFnSc4d1	Kyjevská
dohodu	dohoda	k1gFnSc4	dohoda
společně	společně	k6eAd1	společně
s	s	k7c7	s
představitelem	představitel	k1gMnSc7	představitel
amerických	americký	k2eAgMnPc2d1	americký
Slováků	Slovák	k1gMnPc2	Slovák
G.	G.	kA	G.
Košíkem	Košík	k1gMnSc7	Košík
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
společně	společně	k6eAd1	společně
uznali	uznat	k5eAaPmAgMnP	uznat
ČSNR	ČSNR	kA	ČSNR
za	za	k7c2	za
vedoucí	vedoucí	k1gFnSc2	vedoucí
orgán	orgány	k1gFnPc2	orgány
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
slovenského	slovenský	k2eAgNnSc2d1	slovenské
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
velení	velení	k1gNnSc1	velení
potom	potom	k6eAd1	potom
poslalo	poslat	k5eAaPmAgNnS	poslat
Štefánika	Štefánik	k1gMnSc4	Štefánik
do	do	k7c2	do
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
1500	[number]	k4	1500
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
počátečních	počáteční	k2eAgFnPc6d1	počáteční
komplikacích	komplikace	k1gFnPc6	komplikace
s	s	k7c7	s
Dürichem	Dürich	k1gMnSc7	Dürich
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nerespektoval	respektovat	k5eNaImAgMnS	respektovat
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
téměř	téměř	k6eAd1	téměř
pro-carskou	proarský	k2eAgFnSc4d1	pro-carský
orientaci	orientace	k1gFnSc4	orientace
<g/>
,	,	kIx,	,
však	však	k9	však
přece	přece	k9	přece
měla	mít	k5eAaImAgFnS	mít
Štefánikova	Štefánikův	k2eAgFnSc1d1	Štefánikova
mise	mise	k1gFnSc1	mise
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
napomohla	napomoct	k5eAaPmAgFnS	napomoct
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
carismu	carismus	k1gInSc2	carismus
a	a	k8xC	a
postavení	postavení	k1gNnSc2	postavení
ČSNR	ČSNR	kA	ČSNR
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1917	[number]	k4	1917
odplul	odplout	k5eAaPmAgInS	odplout
Štefánik	Štefánik	k1gInSc1	Štefánik
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
úkolem	úkol	k1gInSc7	úkol
byl	být	k5eAaImAgInS	být
nábor	nábor	k1gInSc4	nábor
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
<s>
Štefánikovi	Štefánik	k1gMnSc3	Štefánik
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
na	na	k7c4	na
3000	[number]	k4	3000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
byla	být	k5eAaImAgFnS	být
konsolidace	konsolidace	k1gFnSc2	konsolidace
krajanů	krajan	k1gMnPc2	krajan
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
získání	získání	k1gNnSc4	získání
jejich	jejich	k3xOp3gFnSc2	jejich
podpory	podpora	k1gFnSc2	podpora
pro	pro	k7c4	pro
ČSNR	ČSNR	kA	ČSNR
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
politickou	politický	k2eAgFnSc4d1	politická
aktivitu	aktivita	k1gFnSc4	aktivita
mezi	mezi	k7c7	mezi
Američany	Američan	k1gMnPc7	Američan
ocenily	ocenit	k5eAaPmAgFnP	ocenit
i	i	k9	i
francouzské	francouzský	k2eAgInPc4d1	francouzský
kruhy	kruh	k1gInPc4	kruh
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgInS	být
Štefánik	Štefánik	k1gInSc1	Štefánik
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
křížem	křížem	k6eAd1	křížem
důstojníka	důstojník	k1gMnSc4	důstojník
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
se	se	k3xPyFc4	se
Štefánik	Štefánik	k1gInSc1	Štefánik
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
diplomatických	diplomatický	k2eAgNnPc2d1	diplomatické
jednání	jednání	k1gNnPc2	jednání
o	o	k7c6	o
ustavení	ustavení	k1gNnSc6	ustavení
samostatné	samostatný	k2eAgFnSc2d1	samostatná
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
dekret	dekret	k1gInSc4	dekret
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
Česko-Slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
armády	armáda	k1gFnSc2	armáda
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
dekretu	dekret	k1gInSc2	dekret
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Česko-Slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podléhala	podléhat	k5eAaImAgFnS	podléhat
velení	velení	k1gNnSc4	velení
ČSNR	ČSNR	kA	ČSNR
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
Štefánik	Štefánik	k1gMnSc1	Štefánik
opět	opět	k6eAd1	opět
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Janinem	Janin	k1gInSc7	Janin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
potom	potom	k6eAd1	potom
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
francouzského	francouzský	k2eAgNnSc2d1	francouzské
velení	velení	k1gNnSc2	velení
přijal	přijmout	k5eAaPmAgInS	přijmout
funkci	funkce	k1gFnSc4	funkce
velitele	velitel	k1gMnSc2	velitel
vytvářejícího	vytvářející	k2eAgMnSc2d1	vytvářející
se	se	k3xPyFc4	se
československého	československý	k2eAgNnSc2d1	Československé
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
čítalo	čítat	k5eAaImAgNnS	čítat
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ministrem	ministr	k1gMnSc7	ministr
vojenství	vojenství	k1gNnSc2	vojenství
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Karla	Karel	k1gMnSc2	Karel
Kramáře	kramář	k1gMnSc2	kramář
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
působil	působit	k5eAaImAgMnS	působit
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vojenství	vojenství	k1gNnSc2	vojenství
existovalo	existovat	k5eAaImAgNnS	existovat
ještě	ještě	k9	ještě
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
řídil	řídit	k5eAaImAgMnS	řídit
Václav	Václav	k1gMnSc1	Václav
Klofáč	klofáč	k1gInSc4	klofáč
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Klofáčovo	Klofáčův	k2eAgNnSc1d1	Klofáčův
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
domácí	domácí	k2eAgNnSc4d1	domácí
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
do	do	k7c2	do
gesce	gesce	k1gFnSc2	gesce
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vojenství	vojenství	k1gNnSc2	vojenství
spadaly	spadat	k5eAaImAgFnP	spadat
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
legie	legie	k1gFnPc1	legie
<g/>
.	.	kIx.	.
</s>
<s>
Nehodě	nehoda	k1gFnSc3	nehoda
se	se	k3xPyFc4	se
podrobně	podrobně	k6eAd1	podrobně
věnuje	věnovat	k5eAaImIp3nS	věnovat
samostatný	samostatný	k2eAgInSc4d1	samostatný
článek	článek	k1gInSc4	článek
Letecká	letecký	k2eAgFnSc1d1	letecká
nehoda	nehoda	k1gFnSc1	nehoda
generála	generál	k1gMnSc2	generál
Štefánika	Štefánik	k1gMnSc2	Štefánik
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
zahynul	zahynout	k5eAaPmAgMnS	zahynout
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1919	[number]	k4	1919
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
domů	domů	k6eAd1	domů
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
při	při	k7c6	při
leteckém	letecký	k2eAgNnSc6d1	letecké
neštěstí	neštěstí	k1gNnSc6	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pomník	pomník	k1gInSc4	pomník
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Bradlo	bradlo	k1gNnSc1	bradlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Brezová	Brezová	k1gFnSc1	Brezová
pod	pod	k7c4	pod
Bradlom	Bradlom	k1gInSc4	Bradlom
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Emila	Emil	k1gMnSc2	Emil
Karla	Karel	k1gMnSc2	Karel
Kautského	Kautský	k2eAgMnSc2d1	Kautský
zabývajícího	zabývající	k2eAgMnSc2d1	zabývající
se	se	k3xPyFc4	se
analýzou	analýza	k1gFnSc7	analýza
historických	historický	k2eAgFnPc2d1	historická
okolností	okolnost	k1gFnPc2	okolnost
Štefánikovy	Štefánikův	k2eAgFnSc2d1	Štefánikova
smrti	smrt	k1gFnSc2	smrt
mají	mít	k5eAaImIp3nP	mít
nepřímé	přímý	k2eNgInPc4d1	nepřímý
důkazy	důkaz	k1gInPc4	důkaz
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
poválečného	poválečný	k2eAgInSc2d1	poválečný
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
naznačovat	naznačovat	k5eAaImF	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
obětí	oběť	k1gFnSc7	oběť
střetu	střet	k1gInSc2	střet
francouzských	francouzský	k2eAgInPc2d1	francouzský
a	a	k8xC	a
italských	italský	k2eAgInPc2d1	italský
geopolitických	geopolitický	k2eAgInPc2d1	geopolitický
zájmů	zájem	k1gInPc2	zájem
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
či	či	k8xC	či
mocenských	mocenský	k2eAgFnPc2d1	mocenská
ambicí	ambice	k1gFnPc2	ambice
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
faktu	fakt	k1gInSc3	fakt
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
prudké	prudký	k2eAgNnSc1d1	prudké
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
Štefánikem	Štefánik	k1gMnSc7	Štefánik
a	a	k8xC	a
Benešem	Beneš	k1gMnSc7	Beneš
po	po	k7c6	po
odhalení	odhalení	k1gNnSc6	odhalení
Benešovy	Benešův	k2eAgFnSc2d1	Benešova
tajně	tajně	k6eAd1	tajně
podepsané	podepsaný	k2eAgFnSc2d1	podepsaná
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c4	po
zjištění	zjištění	k1gNnSc4	zjištění
zpronevěry	zpronevěra	k1gFnSc2	zpronevěra
darů	dar	k1gInPc2	dar
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
Čechů	Čech	k1gMnPc2	Čech
na	na	k7c4	na
chod	chod	k1gInSc4	chod
česko-slovenských	českolovenský	k2eAgFnPc2d1	česko-slovenská
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zastánců	zastánce	k1gMnPc2	zastánce
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
jsou	být	k5eAaImIp3nP	být
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c6	o
atentátu	atentát	k1gInSc6	atentát
pitevní	pitevní	k2eAgFnSc1d1	pitevní
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
průběh	průběh	k1gInSc1	průběh
nehody	nehoda	k1gFnSc2	nehoda
i	i	k8xC	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
svědecké	svědecký	k2eAgFnSc2d1	svědecká
výpovědi	výpověď	k1gFnSc2	výpověď
obsahující	obsahující	k2eAgFnSc4d1	obsahující
zmínku	zmínka	k1gFnSc4	zmínka
o	o	k7c6	o
střelbě	střelba	k1gFnSc6	střelba
byly	být	k5eAaImAgFnP	být
vyšetřujícími	vyšetřující	k2eAgFnPc7d1	vyšetřující
orgány	orgán	k1gInPc1	orgán
ignorovány	ignorovat	k5eAaImNgInP	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
svědci	svědek	k1gMnPc1	svědek
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
slyšeli	slyšet	k5eAaImAgMnP	slyšet
střelbu	střelba	k1gFnSc4	střelba
nebo	nebo	k8xC	nebo
viděli	vidět	k5eAaImAgMnP	vidět
vojáky	voják	k1gMnPc4	voják
z	z	k7c2	z
kasáren	kasárna	k1gNnPc2	kasárna
pálit	pálit	k5eAaImF	pálit
na	na	k7c4	na
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výpovědi	výpověď	k1gFnSc2	výpověď
pplk.	pplk.	kA	pplk.
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Fabiána	Fabián	k1gMnSc2	Fabián
se	se	k3xPyFc4	se
na	na	k7c4	na
letadlo	letadlo	k1gNnSc4	letadlo
střílelo	střílet	k5eAaImAgNnS	střílet
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
letci	letec	k1gMnPc1	letec
letěli	letět	k5eAaImAgMnP	letět
nízko	nízko	k6eAd1	nízko
<g/>
,	,	kIx,	,
křičeli	křičet	k5eAaImAgMnP	křičet
a	a	k8xC	a
mávali	mávat	k5eAaImAgMnP	mávat
bílými	bílý	k2eAgInPc7d1	bílý
kapesníčky	kapesníček	k1gInPc7	kapesníček
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
možné	možný	k2eAgNnSc1d1	možné
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastní	vlastní	k2eAgFnPc4d1	vlastní
hlídky	hlídka	k1gFnPc4	hlídka
rozestavěné	rozestavěný	k2eAgFnPc4d1	rozestavěná
u	u	k7c2	u
Ivanky	Ivanka	k1gFnSc2	Ivanka
na	na	k7c4	na
letadlo	letadlo	k1gNnSc4	letadlo
střílely	střílet	k5eAaImAgFnP	střílet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Při	při	k7c6	při
různých	různý	k2eAgNnPc6d1	různé
šetřeních	šetření	k1gNnPc6	šetření
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
televize	televize	k1gFnSc2	televize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
kontaktován	kontaktován	k2eAgMnSc1d1	kontaktován
svědek	svědek	k1gMnSc1	svědek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
viděl	vidět	k5eAaImAgMnS	vidět
Štefánikovu	Štefánikův	k2eAgFnSc4d1	Štefánikova
prostřílenou	prostřílený	k2eAgFnSc4d1	prostřílená
leteckou	letecký	k2eAgFnSc4d1	letecká
bundu	bunda	k1gFnSc4	bunda
(	(	kIx(	(
<g/>
redakce	redakce	k1gFnSc2	redakce
HRPD	HRPD	kA	HRPD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
podobných	podobný	k2eAgNnPc6d1	podobné
zjištěních	zjištění	k1gNnPc6	zjištění
byla	být	k5eAaImAgFnS	být
příprava	příprava	k1gFnSc1	příprava
dokumentu	dokument	k1gInSc2	dokument
zastavena	zastaven	k2eAgFnSc1d1	zastavena
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
lživá	lživý	k2eAgFnSc1d1	lživá
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tuto	tento	k3xDgFnSc4	tento
originální	originální	k2eAgFnSc4d1	originální
leteckou	letecký	k2eAgFnSc4d1	letecká
bundu	bunda	k1gFnSc4	bunda
lze	lze	k6eAd1	lze
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodné	rodný	k2eAgFnSc6d1	rodná
obci	obec	k1gFnSc6	obec
Košariská	Košariská	k1gFnSc1	Košariská
<g/>
.	.	kIx.	.
</s>
<s>
Bunda	bunda	k1gFnSc1	bunda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
části	část	k1gFnSc6	část
ožehnuta	ožehnut	k2eAgFnSc1d1	ožehnut
požárem	požár	k1gInSc7	požár
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikde	nikde	k6eAd1	nikde
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
místo	místo	k1gNnSc1	místo
s	s	k7c7	s
průstřelem	průstřel	k1gInSc7	průstřel
<g/>
.	.	kIx.	.
</s>
<s>
Samotní	samotný	k2eAgMnPc1d1	samotný
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Muzea	muzeum	k1gNnSc2	muzeum
se	se	k3xPyFc4	se
ohrazují	ohrazovat	k5eAaImIp3nP	ohrazovat
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
šířené	šířený	k2eAgFnSc3d1	šířená
lži	lež	k1gFnSc3	lež
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInSc4d1	další
důkaz	důkaz	k1gInSc4	důkaz
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
příběh	příběh	k1gInSc1	příběh
radiotelegrafisty	radiotelegrafista	k1gMnSc2	radiotelegrafista
Jiřího	Jiří	k1gMnSc2	Jiří
Formana	Forman	k1gMnSc2	Forman
<g/>
,	,	kIx,	,
uveřejněný	uveřejněný	k2eAgInSc4d1	uveřejněný
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1996	[number]	k4	1996
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
novinách	novina	k1gFnPc6	novina
Nedělní	nedělní	k2eAgFnSc4d1	nedělní
Hlasatel	hlasatel	k1gMnSc1	hlasatel
vydávaných	vydávaný	k2eAgInPc2d1	vydávaný
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc4	on
dostali	dostat	k5eAaPmAgMnP	dostat
první	první	k4xOgNnSc4	první
hlášení	hlášení	k1gNnSc4	hlášení
o	o	k7c6	o
Štefánikově	Štefánikův	k2eAgInSc6d1	Štefánikův
příletu	přílet	k1gInSc6	přílet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
hlášení	hlášení	k1gNnSc6	hlášení
bylo	být	k5eAaImAgNnS	být
letadlo	letadlo	k1gNnSc1	letadlo
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
nepřátelské	přátelský	k2eNgInPc4d1	nepřátelský
a	a	k8xC	a
vydán	vydán	k2eAgInSc4d1	vydán
rozkaz	rozkaz	k1gInSc4	rozkaz
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
sestřelení	sestřelení	k1gNnSc4	sestřelení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
o	o	k7c4	o
stíhání	stíhání	k1gNnSc4	stíhání
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kolegou	kolega	k1gMnSc7	kolega
<g/>
,	,	kIx,	,
také	také	k9	také
svědkem	svědek	k1gMnSc7	svědek
incidentu	incident	k1gInSc2	incident
<g/>
,	,	kIx,	,
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
se	se	k3xPyFc4	se
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
z	z	k7c2	z
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
už	už	k6eAd1	už
nežili	žít	k5eNaImAgMnP	žít
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
příbuzných	příbuzná	k1gFnPc2	příbuzná
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
oběťmi	oběť	k1gFnPc7	oběť
havárie	havárie	k1gFnSc2	havárie
<g/>
,	,	kIx,	,
našli	najít	k5eAaPmAgMnP	najít
je	on	k3xPp3gNnSc4	on
oběšené	oběšený	k2eAgNnSc4d1	oběšené
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
proto	proto	k8xC	proto
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepohodlní	pohodlný	k2eNgMnPc1d1	nepohodlný
svědci	svědek	k1gMnPc1	svědek
byli	být	k5eAaImAgMnP	být
postupně	postupně	k6eAd1	postupně
odstraněni	odstranit	k5eAaPmNgMnP	odstranit
tajnou	tajný	k2eAgFnSc7d1	tajná
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
pravděpodobným	pravděpodobný	k2eAgInSc7d1	pravděpodobný
důvodem	důvod	k1gInSc7	důvod
nehody	nehoda	k1gFnSc2	nehoda
je	být	k5eAaImIp3nS	být
nešťastný	šťastný	k2eNgInSc1d1	nešťastný
omyl	omyl	k1gInSc1	omyl
obsluhy	obsluha	k1gFnSc2	obsluha
protiletadlové	protiletadlový	k2eAgFnSc2d1	protiletadlová
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
popisuje	popisovat	k5eAaImIp3nS	popisovat
sestřelení	sestřelení	k1gNnSc1	sestřelení
letadla	letadlo	k1gNnSc2	letadlo
vlastní	vlastní	k2eAgFnSc7d1	vlastní
protivzdušnou	protivzdušný	k2eAgFnSc7d1	protivzdušná
obranou	obrana	k1gFnSc7	obrana
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
letadlo	letadlo	k1gNnSc1	letadlo
se	s	k7c7	s
Štefánikem	Štefánik	k1gInSc7	Štefánik
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
italskou	italský	k2eAgFnSc7d1	italská
trikolórou	trikolóra	k1gFnSc7	trikolóra
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
identická	identický	k2eAgFnSc1d1	identická
s	s	k7c7	s
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
maďarská	maďarský	k2eAgNnPc4d1	Maďarské
letadla	letadlo	k1gNnPc4	letadlo
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
označována	označovat	k5eAaImNgFnS	označovat
rudou	rudý	k2eAgFnSc7d1	rudá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
rodokmen	rodokmen	k1gInSc1	rodokmen
rodiny	rodina	k1gFnSc2	rodina
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
sestavil	sestavit	k5eAaPmAgMnS	sestavit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
knižně	knižně	k6eAd1	knižně
uveřejnil	uveřejnit	k5eAaPmAgMnS	uveřejnit
Branislav	Branislav	k1gMnSc1	Branislav
Varsik	Varsik	k1gMnSc1	Varsik
<g/>
:	:	kIx,	:
Michal	Michal	k1gMnSc1	Michal
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1724	[number]	k4	1724
"	"	kIx"	"
<g/>
iuvenis	iuvenis	k1gFnSc1	iuvenis
Pravecensis	Pravecensis	k1gFnSc1	Pravecensis
<g/>
"	"	kIx"	"
–	–	k?	–
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1778	[number]	k4	1778
Senica	Senicum	k1gNnSc2	Senicum
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Katarína	Katarína	k1gFnSc1	Katarína
Adamiš	Adamiš	k1gMnSc1	Adamiš
Pavol	Pavol	k1gInSc1	Pavol
<g />
.	.	kIx.	.
</s>
<s>
Štefánik	Štefánik	k1gInSc1	Štefánik
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1761	[number]	k4	1761
Senica	Senic	k1gInSc2	Senic
–	–	k?	–
†	†	k?	†
5	[number]	k4	5
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1831	[number]	k4	1831
Krajné	krajný	k2eAgFnPc1d1	Krajná
<g/>
)	)	kIx)	)
Michal	Michal	k1gMnSc1	Michal
Štefánik	Štefánik	k1gMnSc1	Štefánik
Ján	Ján	k1gMnSc1	Ján
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1765	[number]	k4	1765
Senica	Senicum	k1gNnSc2	Senicum
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Terézia	Terézia	k1gFnSc1	Terézia
Sitár	Sitár	k1gMnSc1	Sitár
Samuel	Samuel	k1gMnSc1	Samuel
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1767	[number]	k4	1767
Senica	Senica	k1gFnSc1	Senica
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Zuzana	Zuzana	k1gFnSc1	Zuzana
Vaníček	Vaníček	k1gMnSc1	Vaníček
Pavol	Pavola	k1gFnPc2	Pavola
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1798	[number]	k4	1798
Senica	Senic	k1gInSc2	Senic
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1861	[number]	k4	1861
Krajne	Krajn	k1gInSc5	Krajn
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Ľudovíta	Ľudovít	k1gInSc2	Ľudovít
Šulek	šulka	k1gFnPc2	šulka
Pavol	Pavol	k1gInSc1	Pavol
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1844	[number]	k4	1844
Krajne	Krajn	k1gInSc5	Krajn
–	–	k?	–
15	[number]	k4	15
<g/>
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
1913	[number]	k4	1913
Košariská	Košariská	k1gFnSc1	Košariská
<g/>
)	)	kIx)	)
∞	∞	k?	∞
Albertína	Albertín	k1gInSc2	Albertín
Jurenka	Jurenka	k1gFnSc1	Jurenka
Milan	Milan	k1gMnSc1	Milan
Rastislav	Rastislav	k1gMnSc1	Rastislav
Štefánik	Štefánik	k1gMnSc1	Štefánik
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1880	[number]	k4	1880
Košariská	Košariská	k1gFnSc1	Košariská
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1919	[number]	k4	1919
Ivanka	Ivanka	k1gFnSc1	Ivanka
pri	pri	k?	pri
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
)	)	kIx)	)
Dle	dle	k7c2	dle
závěrů	závěr	k1gInPc2	závěr
učiněných	učiněný	k2eAgInPc2d1	učiněný
Branislavem	Branislav	k1gMnSc7	Branislav
Varsikem	Varsik	k1gMnSc7	Varsik
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
Štefániků	Štefánik	k1gInPc2	Štefánik
slovenského	slovenský	k2eAgInSc2d1	slovenský
luteránského	luteránský	k2eAgInSc2d1	luteránský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
předka	předek	k1gMnSc4	předek
udané	udaný	k2eAgFnSc2d1	udaná
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Pravecensis	Pravecensis	k1gFnSc1	Pravecensis
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
legendám	legenda	k1gFnPc3	legenda
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
o	o	k7c4	o
zemany	zeman	k1gMnPc4	zeman
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
o	o	k7c4	o
české	český	k2eAgMnPc4d1	český
pobělohorské	pobělohorský	k2eAgMnPc4d1	pobělohorský
exulanty	exulant	k1gMnPc4	exulant
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
existenci	existence	k1gFnSc6	existence
zemanských	zemanský	k2eAgMnPc2d1	zemanský
a	a	k8xC	a
českých	český	k2eAgMnPc2d1	český
předků	předek	k1gMnPc2	předek
naopak	naopak	k6eAd1	naopak
Varsik	Varsik	k1gMnSc1	Varsik
usoudil	usoudit	k5eAaPmAgMnS	usoudit
u	u	k7c2	u
rodiny	rodina	k1gFnSc2	rodina
matky	matka	k1gFnSc2	matka
Miroslav	Miroslav	k1gMnSc1	Miroslav
Rastislava	Rastislav	k1gMnSc4	Rastislav
Štefánika	Štefánik	k1gMnSc4	Štefánik
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
M.	M.	kA	M.
R.	R.	kA	R.
Štefánikovi	Štefánik	k1gMnSc6	Štefánik
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	jeho	k3xOp3gMnPc2	jeho
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
Masaryka	Masaryk	k1gMnSc2	Masaryk
a	a	k8xC	a
Beneše	Beneš	k1gMnSc2	Beneš
nezachovalo	zachovat	k5eNaPmAgNnS	zachovat
žádné	žádný	k3yNgNnSc4	žádný
ucelené	ucelený	k2eAgNnSc4d1	ucelené
větší	veliký	k2eAgNnSc4d2	veliký
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
osobní	osobní	k2eAgInPc4d1	osobní
deníky	deník	k1gInPc4	deník
(	(	kIx(	(
<g/>
zápisníky	zápisník	k1gInPc4	zápisník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
korespondenci	korespondence	k1gFnSc4	korespondence
a	a	k8xC	a
astronomické	astronomický	k2eAgFnPc4d1	astronomická
a	a	k8xC	a
publicistické	publicistický	k2eAgFnPc4d1	publicistická
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Hlasistické	hlasistický	k2eAgFnPc1d1	hlasistický
práce	práce	k1gFnPc1	práce
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
Korešpondencia	Korešpondencia	k1gFnSc1	Korešpondencia
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milana	Milan	k1gMnSc2	Milan
Rastislava	Rastislav	k1gMnSc2	Rastislav
Štefánika	Štefánik	k1gMnSc2	Štefánik
-	-	kIx~	-
editor	editor	k1gInSc1	editor
V.	V.	kA	V.
Polívka	polívka	k1gFnSc1	polívka
<g/>
,	,	kIx,	,
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
vydání	vydání	k1gNnSc1	vydání
RECO	RECO	kA	RECO
2003	[number]	k4	2003
Listy	lista	k1gFnSc2	lista
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
adresované	adresovaný	k2eAgFnSc6d1	adresovaná
Lidmile	Lidmila	k1gFnSc6	Lidmila
Vrchlické	Vrchlické	k2eAgInSc1d1	Vrchlické
<g/>
,	,	kIx,	,
Historický	historický	k2eAgInSc1d1	historický
časopis	časopis	k1gInSc1	časopis
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
č	č	k0	č
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Pol	pola	k1gFnPc2	pola
mesiaca	mesiac	k1gInSc2	mesiac
na	na	k7c6	na
vrchole	vrchol	k1gInSc6	vrchol
Mont	Monta	k1gFnPc2	Monta
Blnacu	Blnacus	k1gInSc2	Blnacus
<g/>
,	,	kIx,	,
Tranvoský	Tranvoský	k2eAgInSc4d1	Tranvoský
evanjelický	evanjelický	k2eAgInSc4d1	evanjelický
kalendár	kalendár	k1gInSc4	kalendár
1910	[number]	k4	1910
Z	z	k7c2	z
cesty	cesta	k1gFnSc2	cesta
po	po	k7c6	po
severnej	severnat	k5eAaPmRp2nS	severnat
Afrike	Afrik	k1gFnSc2	Afrik
<g/>
,	,	kIx,	,
Tranvoský	Tranvoský	k2eAgMnSc1d1	Tranvoský
evanjelický	evanjelický	k2eAgMnSc1d1	evanjelický
kalendár	kalendár	k1gMnSc1	kalendár
1910	[number]	k4	1910
Zápisník	zápisník	k1gInSc1	zápisník
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
z	z	k7c2	z
Equadora	Equador	k1gMnSc2	Equador
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
-	-	kIx~	-
editor	editor	k1gInSc1	editor
V.	V.	kA	V.
Polívka	polívka	k1gFnSc1	polívka
<g/>
,	,	kIx,	,
<g/>
Bánská	bánský	k2eAgFnSc1d1	Bánská
Bystrica	Bystrica	k1gFnSc1	Bystrica
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
vydání	vydání	k1gNnSc1	vydání
Vydavateľstvo	Vydavateľstvo	k1gNnSc1	Vydavateľstvo
SSS	SSS	kA	SSS
2006	[number]	k4	2006
Zápisníky	zápisník	k1gInPc1	zápisník
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
-	-	kIx~	-
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Bartůšek	Bartůšek	k1gMnSc1	Bartůšek
a	a	k8xC	a
J.	J.	kA	J.
Boháč	Boháč	k1gMnSc1	Boháč
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
Hvezdárske	Hvezdárske	k1gFnPc2	Hvezdárske
články	článek	k1gInPc1	článek
Milana	Milan	k1gMnSc2	Milan
Štefánika	Štefánik	k1gMnSc2	Štefánik
SNM-Múzeum	SNM-Múzeum	k1gNnSc1	SNM-Múzeum
Slovenských	slovenský	k2eAgMnPc2d1	slovenský
národných	národný	k2eAgMnPc2d1	národný
rád	rád	k2eAgMnSc1d1	rád
Myjave	Myjav	k1gInSc5	Myjav
2003	[number]	k4	2003
</s>
