<s>
Generál	generál	k1gMnSc1
PhDr.	PhDr.	kA
Milan	Milan	k1gMnSc1
Rastislav	Rastislav	k1gMnSc1
Štefánik	Štefánik	k1gMnSc1
(	(	kIx(
<g/>
souhlásky	souhláska	k1gFnPc1
"	"	kIx"
<g/>
t	t	k?
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
n	n	k0
<g/>
"	"	kIx"
vyslovovány	vyslovovat	k5eAaImNgFnP
měkce	měkko	k6eAd1
<g/>
,	,	kIx,
21	[number]	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1880	[number]	k4
Košariská	Košariská	k1gFnSc1
–	–	k?
4	[number]	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1919	[number]	k4
Ivanka	Ivanka	k1gFnSc1
pri	pri	k?
Dunaji	Dunaj	k1gInSc6
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
slovenský	slovenský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
generál	generál	k1gMnSc1
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
také	také	k9
astronom	astronom	k1gMnSc1
<g/>
.	.	kIx.
</s>