<p>
<s>
2D	[number]	k4	2D
či	či	k8xC	či
2	[number]	k4	2
<g/>
−	−	k?	−
<g/>
D	D	kA	D
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
dvoudimenzionální	dvoudimenzionální	k2eAgMnSc1d1	dvoudimenzionální
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dvourozměrný	dvourozměrný	k2eAgMnSc1d1	dvourozměrný
<g/>
"	"	kIx"	"
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
popsat	popsat	k5eAaPmF	popsat
dvěma	dva	k4xCgInPc7	dva
rozměry	rozměr	k1gInPc7	rozměr
<g/>
;	;	kIx,	;
předměty	předmět	k1gInPc7	předmět
ve	v	k7c6	v
dvourozměrném	dvourozměrný	k2eAgInSc6d1	dvourozměrný
světě	svět	k1gInSc6	svět
mají	mít	k5eAaImIp3nP	mít
obsah	obsah	k1gInSc4	obsah
a	a	k8xC	a
např.	např.	kA	např.
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
šířku	šířka	k1gFnSc4	šířka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nemají	mít	k5eNaImIp3nP	mít
objem	objem	k1gInSc4	objem
<g/>
.	.	kIx.	.
2D	[number]	k4	2D
obrazec	obrazec	k1gInSc1	obrazec
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2D	[number]	k4	2D
jsou	být	k5eAaImIp3nP	být
hlavně	hlavně	k9	hlavně
základní	základní	k2eAgInPc4d1	základní
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
čtverec	čtverec	k1gInSc1	čtverec
</s>
</p>
<p>
<s>
obdélník	obdélník	k1gInSc1	obdélník
</s>
</p>
<p>
<s>
kruh	kruh	k1gInSc1	kruh
</s>
</p>
<p>
<s>
trojúhelníkVěda	trojúhelníkVěda	k1gFnSc1	trojúhelníkVěda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
zobrazením	zobrazení	k1gNnSc7	zobrazení
trojrozměrných	trojrozměrný	k2eAgInPc2d1	trojrozměrný
objektů	objekt	k1gInPc2	objekt
do	do	k7c2	do
dvojrozměrného	dvojrozměrný	k2eAgInSc2d1	dvojrozměrný
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rovina	rovina	k1gFnSc1	rovina
</s>
</p>
<p>
<s>
3D	[number]	k4	3D
</s>
</p>
<p>
<s>
Dimenze	dimenze	k1gFnSc1	dimenze
</s>
</p>
<p>
<s>
Deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
geometrie	geometrie	k1gFnSc1	geometrie
</s>
</p>
