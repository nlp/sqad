<s desamb="1">
Kvůli	kvůli	k7c3
stavbě	stavba	k1gFnSc3
železnice	železnice	k1gFnSc2
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1848	#num#	k4
areál	areál	k1gInSc1
zbořen	zbořen	k2eAgInSc1d1
a	a	k8xC
na	na	k7c6
okraji	okraj	k1gInSc6
Podmokel	Podmokly	k1gInPc2
další	další	k2eAgInPc1d1
z	z	k7c2
rodiny	rodina	k1gFnSc2
Thunů	Thun	k1gInPc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
Antonín	Antonín	k1gMnSc1
hrabě	hrabě	k1gMnSc1
Thun	Thun	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
pivovar	pivovar	k1gInSc1
nový	nový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>