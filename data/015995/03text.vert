<s>
Pivovar	pivovar	k1gInSc1
Děčín	Děčín	k1gInSc1
</s>
<s>
Pivovar	pivovar	k1gInSc4
Děčín	Děčín	k1gInSc1
Místo	místo	k1gNnSc1
</s>
<s>
Děčín	Děčín	k1gInSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
19,92	19,92	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
34,73	34,73	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Kód	kód	k1gInSc1
památky	památka	k1gFnSc2
</s>
<s>
11408	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
5775	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pivovar	pivovar	k1gInSc1
v	v	k7c6
Podmoklech	Podmokly	k1gInPc6
byl	být	k5eAaImAgMnS
v	v	k7c6
provozu	provoz	k1gInSc6
v	v	k7c6
letech	léto	k1gNnPc6
1703	#num#	k4
až	až	k9
1995	#num#	k4
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
areál	areál	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
jiným	jiný	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
v	v	k7c6
malém	malý	k2eAgNnSc6d1
množství	množství	k1gNnSc6
opět	opět	k6eAd1
vaří	vařit	k5eAaImIp3nP
pivo	pivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pivovar	pivovar	k1gInSc1
nechal	nechat	k5eAaPmAgInS
v	v	k7c6
Podmoklech	Podmokly	k1gInPc6
postavit	postavit	k5eAaPmF
hrabě	hrabě	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
Thun-Hohenstein	Thun-Hohenstein	k1gMnSc1
a	a	k8xC
první	první	k4xOgFnSc1
várka	várka	k1gFnSc1
piva	pivo	k1gNnSc2
v	v	k7c6
něm	on	k3xPp3gInSc6
byla	být	k5eAaImAgFnS
vyrobena	vyroben	k2eAgFnSc1d1
již	již	k6eAd1
roku	rok	k1gInSc2
1703	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
dokončena	dokončit	k5eAaPmNgFnS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
tedy	tedy	k9
roku	rok	k1gInSc2
1707	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
stavbě	stavba	k1gFnSc3
železnice	železnice	k1gFnSc2
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1848	#num#	k4
areál	areál	k1gInSc1
zbořen	zbořen	k2eAgInSc1d1
a	a	k8xC
na	na	k7c6
okraji	okraj	k1gInSc6
Podmokel	Podmokly	k1gInPc2
další	další	k2eAgInPc1d1
z	z	k7c2
rodiny	rodina	k1gFnSc2
Thunů	Thun	k1gInPc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
Antonín	Antonín	k1gMnSc1
hrabě	hrabě	k1gMnSc1
Thun	Thun	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
postavit	postavit	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
pivovar	pivovar	k1gInSc1
nový	nový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivovarnický	pivovarnický	k2eAgInSc1d1
areál	areál	k1gInSc1
byl	být	k5eAaImAgInS
několikrát	několikrát	k6eAd1
rozšířen	rozšířit	k5eAaPmNgInS
<g/>
,	,	kIx,
přesto	přesto	k8xC
zůstal	zůstat	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
secesní	secesní	k2eAgInSc1d1
sloh	sloh	k1gInSc1
zachován	zachovat	k5eAaPmNgInS
a	a	k8xC
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
chráněnou	chráněný	k2eAgFnSc7d1
stavební	stavební	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1926	#num#	k4
pivovar	pivovar	k1gInSc1
od	od	k7c2
Thunů	Thun	k1gMnPc2
odkoupila	odkoupit	k5eAaPmAgFnS
pivovarská	pivovarský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pak	pak	k6eAd1
používala	používat	k5eAaImAgFnS
stáčírnu	stáčírna	k1gFnSc4
i	i	k9
v	v	k7c6
Rumburku	Rumburk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
se	se	k3xPyFc4
pivovar	pivovar	k1gInSc1
jmenoval	jmenovat	k5eAaImAgInS,k5eAaBmAgInS
Severočeské	severočeský	k2eAgInPc4d1
pivovary	pivovar	k1gInPc4
n.	n.	k?
<g/>
p.	p.	k?
<g/>
,	,	kIx,
závod	závod	k1gInSc1
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Pivovary	pivovar	k1gInPc1
Louny	Louny	k1gInPc1
s.	s.	k?
<g/>
p.	p.	k?
<g/>
,	,	kIx,
závod	závod	k1gInSc1
Děčín	Děčín	k1gInSc1
a	a	k8xC
Pivovar	pivovar	k1gInSc1
Děčín	Děčín	k1gInSc1
s.	s.	k?
<g/>
p.	p.	k?
V	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
byl	být	k5eAaImAgInS
podnik	podnik	k1gInSc1
vydražen	vydražit	k5eAaPmNgInS
<g/>
,	,	kIx,
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
zde	zde	k6eAd1
pivo	pivo	k1gNnSc1
přestalo	přestat	k5eAaPmAgNnS
vyrábět	vyrábět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byl	být	k5eAaImAgInS
přejmenován	přejmenovat	k5eAaPmNgInS
z	z	k7c2
Admirál	admirál	k1gMnSc1
na	na	k7c4
Pivovar	pivovar	k1gInSc4
Děčín	Děčín	k1gInSc1
a.s.	a.s.	k?
<g/>
,	,	kIx,
pivo	pivo	k1gNnSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
vařilo	vařit	k5eAaImAgNnS
jen	jen	k9
do	do	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
a	a	k8xC
pak	pak	k6eAd1
nový	nový	k2eAgMnSc1d1
vlastník	vlastník	k1gMnSc1
<g/>
,	,	kIx,
firma	firma	k1gFnSc1
BASH	BASH	kA
areál	areál	k1gInSc1
přestavěla	přestavět	k5eAaPmAgFnS
na	na	k7c4
jiné	jiný	k2eAgInPc4d1
účely	účel	k1gInPc4
se	s	k7c7
zachováním	zachování	k1gNnSc7
chráněných	chráněný	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestavbou	přestavba	k1gFnSc7
městského	městský	k2eAgInSc2d1
pivovaru	pivovar	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
již	již	k6eAd1
léta	léto	k1gNnSc2
nevyužívaný	využívaný	k2eNgMnSc1d1
a	a	k8xC
chátral	chátrat	k5eAaImAgInS
<g/>
,	,	kIx,
vzniklo	vzniknout	k5eAaPmAgNnS
nové	nový	k2eAgNnSc1d1
multifunkční	multifunkční	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
byl	být	k5eAaImAgInS
iniciován	iniciovat	k5eAaBmNgInS
Vojtěchem	Vojtěch	k1gMnSc7
Ryvolou	Ryvola	k1gMnSc7
a	a	k8xC
Martinem	Martin	k1gMnSc7
Králem	Král	k1gMnSc7
a	a	k8xC
na	na	k7c6
základě	základ	k1gInSc6
architektonického	architektonický	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
studia	studio	k1gNnSc2
Studio	studio	k1gNnSc1
acht	acht	k1gInSc1
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
společnosti	společnost	k1gFnSc3
EDIFICE	EDIFICE	kA
construction	construction	k1gInSc1
&	&	k?
consulting	consulting	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o	o	k7c4
celé	celý	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
zkolaudovat	zkolaudovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gMnPc4
piv	pivo	k1gNnPc2
</s>
<s>
Ve	v	k7c6
značce	značka	k1gFnSc6
piv	pivo	k1gNnPc2
byl	být	k5eAaImAgMnS
zobrazen	zobrazen	k2eAgMnSc1d1
mnich	mnich	k1gMnSc1
s	s	k7c7
půllitrem	půllitr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
bylo	být	k5eAaImAgNnS
hlavním	hlavní	k2eAgNnSc7d1
vyráběným	vyráběný	k2eAgNnSc7d1
pivem	pivo	k1gNnSc7
Bodenbacher	Bodenbachra	k1gFnPc2
Bier	Bier	k1gInSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
bylo	být	k5eAaImAgNnS
vyváženo	vyvážet	k5eAaImNgNnS,k5eAaPmNgNnS
i	i	k9
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
byly	být	k5eAaImAgFnP
vyráběny	vyrábět	k5eAaImNgInP
pro	pro	k7c4
trh	trh	k1gInSc4
značky	značka	k1gFnSc2
Kapitán	kapitán	k1gMnSc1
<g/>
,	,	kIx,
Admirál	admirál	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
L.	L.	kA
<g/>
A.S.	A.S.	k1gFnPc2
<g/>
K	K	kA
<g/>
,	,	kIx,
Děčan	Děčan	k1gMnSc1
<g/>
,	,	kIx,
Bukanýr	bukanýr	k1gMnSc1
<g/>
,	,	kIx,
Plavec	plavec	k1gMnSc1
<g/>
,	,	kIx,
Generál	generál	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Pivovar	pivovar	k1gInSc1
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
budovy	budova	k1gFnPc1
zvenčí	zvenčí	k6eAd1
roku	rok	k1gInSc2
2010	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pivovar	pivovar	k1gInSc1
Děčín	Děčín	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Web	web	k1gInSc1
Pivovary	pivovar	k1gInPc1
</s>
<s>
Web	web	k1gInSc1
Děčína	Děčín	k1gInSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Web	web	k1gInSc1
Pivní	pivní	k2eAgInSc4d1
obzor	obzor	k1gInSc4
</s>
<s>
Katalog	katalog	k1gInSc1
pivních	pivní	k2eAgInPc2d1
tácků	tácek	k1gInPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Děčín	Děčín	k1gInSc1
Doprava	doprava	k1gFnSc1
</s>
<s>
Děčín	Děčín	k1gInSc4
hlavní	hlavní	k2eAgNnPc4d1
nádraží	nádraží	k1gNnSc4
•	•	k?
Děčín	Děčín	k1gInSc1
východ	východ	k1gInSc1
•	•	k?
Trolejbusová	trolejbusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
v	v	k7c6
Děčíně	Děčín	k1gInSc6
Historie	historie	k1gFnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Děčína	Děčín	k1gInSc2
•	•	k?
Jezuitský	jezuitský	k2eAgInSc1d1
filosofický	filosofický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
(	(	kIx(
<g/>
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Loretánská	loretánský	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
v	v	k7c6
Děčíně	Děčín	k1gInSc6
•	•	k?
Starokatolický	starokatolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
(	(	kIx(
<g/>
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Tyršův	Tyršův	k2eAgInSc4d1
most	most	k1gInSc4
(	(	kIx(
<g/>
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
Kultura	kultura	k1gFnSc1
</s>
<s>
Masarykovo	Masarykův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
(	(	kIx(
<g/>
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Městské	městský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
Děčín	Děčín	k1gInSc1
•	•	k?
Oblastní	oblastní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Děčíně	Děčín	k1gInSc6
•	•	k?
Zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
Děčín	Děčín	k1gInSc1
–	–	k?
Pastýřská	pastýřský	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
Synagoga	synagoga	k1gFnSc1
v	v	k7c6
Podmoklech	Podmokly	k1gInPc6
(	(	kIx(
<g/>
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Františka	František	k1gMnSc2
Xaverského	xaverský	k2eAgMnSc2d1
(	(	kIx(
<g/>
Bělá	bělat	k5eAaImIp3nS
u	u	k7c2
Děčína	Děčín	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
(	(	kIx(
<g/>
Rozbělesy	Rozbělesa	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Františka	František	k1gMnSc2
z	z	k7c2
Assisi	Assise	k1gFnSc3
(	(	kIx(
<g/>
Podmokly	Podmokly	k1gInPc4
<g/>
)	)	kIx)
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
(	(	kIx(
<g/>
Podmokly	Podmokly	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Kostel	kostel	k1gInSc1
Povýšení	povýšení	k1gNnSc2
svatého	svatý	k2eAgInSc2d1
Kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
Děčín	Děčín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
a	a	k8xC
Blažeje	Blažej	k1gMnSc2
Pamětihodnosti	pamětihodnost	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
v	v	k7c6
Děčíně	Děčín	k1gInSc6
•	•	k?
Staroměstský	staroměstský	k2eAgInSc1d1
most	most	k1gInSc1
•	•	k?
Zámek	zámek	k1gInSc1
Děčín	Děčín	k1gInSc1
Sport	sport	k1gInSc1
</s>
<s>
HC	HC	kA
Děčín	Děčín	k1gInSc1
•	•	k?
BK	BK	kA
Děčín	Děčín	k1gInSc1
Studium	studium	k1gNnSc1
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
Děčín	Děčín	k1gInSc1
•	•	k?
Vyšší	vysoký	k2eAgFnSc1d2
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
strojní	strojní	k2eAgFnSc2d1
<g/>
,	,	kIx,
stavební	stavební	k2eAgFnSc2d1
a	a	k8xC
dopravní	dopravní	k2eAgFnSc2d1
<g/>
,	,	kIx,
Děčín	Děčín	k1gInSc1
Osobnosti	osobnost	k1gFnSc2
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Tyrš	Tyrš	k1gMnSc1
Ostatní	ostatní	k2eAgMnSc1d1
</s>
<s>
Nemocnice	nemocnice	k1gFnSc1
Děčín	Děčín	k1gInSc1
•	•	k?
Okresní	okresní	k2eAgInSc1d1
soud	soud	k1gInSc1
v	v	k7c6
Děčíně	Děčín	k1gInSc6
•	•	k?
Pivovar	pivovar	k1gInSc1
Děčín	Děčín	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Pivo	pivo	k1gNnSc1
</s>
