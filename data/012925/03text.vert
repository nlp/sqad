<p>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Hearne	Hearn	k1gInSc5	Hearn
(	(	kIx(	(
<g/>
1745	[number]	k4	1745
<g/>
–	–	k?	–
<g/>
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
sloužil	sloužit	k5eAaImAgInS	sloužit
u	u	k7c2	u
Royal	Royal	k1gMnSc1	Royal
Navy	Navy	k?	Navy
pod	pod	k7c7	pod
admirálem	admirál	k1gMnSc7	admirál
Samuelem	Samuel	k1gMnSc7	Samuel
Hoodem	Hood	k1gMnSc7	Hood
<g/>
,	,	kIx,	,
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1766	[number]	k4	1766
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
Společnost	společnost	k1gFnSc4	společnost
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
vyslala	vyslat	k5eAaPmAgFnS	vyslat
objevit	objevit	k5eAaPmF	objevit
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
domorodci	domorodec	k1gMnPc1	domorodec
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
zálivu	záliv	k1gInSc2	záliv
berou	brát	k5eAaImIp3nP	brát
měď	měď	k1gFnSc4	měď
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1769	[number]	k4	1769
až	až	k9	až
1772	[number]	k4	1772
podnikl	podniknout	k5eAaPmAgInS	podniknout
Hearne	Hearn	k1gInSc5	Hearn
s	s	k7c7	s
indiánskými	indiánský	k2eAgMnPc7d1	indiánský
průvodci	průvodce	k1gMnPc7	průvodce
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
člunech	člun	k1gInPc6	člun
tři	tři	k4xCgFnPc4	tři
výzkumné	výzkumný	k2eAgFnPc4d1	výzkumná
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
severní	severní	k2eAgFnSc2d1	severní
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
dokázal	dokázat	k5eAaPmAgMnS	dokázat
proniknout	proniknout	k5eAaPmF	proniknout
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Coppermine	Coppermin	k1gInSc5	Coppermin
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Korunovačního	korunovační	k2eAgInSc2d1	korunovační
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
založil	založit	k5eAaPmAgInS	založit
Cumberland	Cumberland	k1gInSc1	Cumberland
House	house	k1gNnSc1	house
jako	jako	k8xS	jako
první	první	k4xOgFnSc4	první
stálou	stálý	k2eAgFnSc4d1	stálá
osadu	osada	k1gFnSc4	osada
na	na	k7c6	na
území	území	k1gNnSc6	území
Saskatchewanu	Saskatchewan	k1gInSc2	Saskatchewan
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
guvernérem	guvernér	k1gMnSc7	guvernér
pevnosti	pevnost	k1gFnSc2	pevnost
Fort	Fort	k?	Fort
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
napsal	napsat	k5eAaPmAgMnS	napsat
o	o	k7c6	o
svých	svůj	k3xOyFgNnPc6	svůj
dobrodružstvích	dobrodružství	k1gNnPc6	dobrodružství
knihu	kniha	k1gFnSc4	kniha
(	(	kIx(	(
<g/>
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
Fort	Fort	k?	Fort
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
k	k	k7c3	k
Severnímu	severní	k2eAgInSc3d1	severní
ledovému	ledový	k2eAgInSc3d1	ledový
oceánu	oceán	k1gInSc3	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
důkladným	důkladný	k2eAgInSc7d1	důkladný
popisem	popis	k1gInSc7	popis
kraje	kraj	k1gInSc2	kraj
ležícího	ležící	k2eAgMnSc4d1	ležící
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Samuel	Samuel	k1gMnSc1	Samuel
Hearne	Hearn	k1gInSc5	Hearn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.biographi.ca/en/bio/hearne_samuel_4E.html	[url]	k1gMnSc1	http://www.biographi.ca/en/bio/hearne_samuel_4E.html
</s>
</p>
