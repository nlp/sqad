<s>
Pátek	pátek	k1gInSc1	pátek
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
normy	norma	k1gFnSc2	norma
ISO	ISO	kA	ISO
8601	[number]	k4	8601
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
pátý	pátý	k4xOgInSc4	pátý
den	den	k1gInSc4	den
po	po	k7c6	po
neděli	neděle	k1gFnSc6	neděle
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
počítán	počítat	k5eAaImNgInS	počítat
za	za	k7c4	za
pátý	pátý	k4xOgInSc4	pátý
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
židovském	židovský	k2eAgMnSc6d1	židovský
a	a	k8xC	a
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
kalendáři	kalendář	k1gInSc6	kalendář
za	za	k7c4	za
den	den	k1gInSc4	den
šestý	šestý	k4xOgInSc1	šestý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
posledním	poslední	k2eAgMnSc6d1	poslední
z	z	k7c2	z
pěti	pět	k4xCc2	pět
pracovních	pracovní	k2eAgInPc2d1	pracovní
dní	den	k1gInPc2	den
před	před	k7c7	před
víkendem	víkend	k1gInSc7	víkend
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
jako	jako	k9	jako
šestý	šestý	k4xOgInSc4	šestý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
týden	týden	k1gInSc1	týden
začíná	začínat	k5eAaImIp3nS	začínat
nedělí	neděle	k1gFnSc7	neděle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
pátek	pátek	k1gInSc4	pátek
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
víkendu	víkend	k1gInSc2	víkend
a	a	k8xC	a
neděle	neděle	k1gFnSc2	neděle
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
pracovní	pracovní	k2eAgInSc4d1	pracovní
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
a	a	k8xC	a
Íránu	Írán	k1gInSc2	Írán
je	být	k5eAaImIp3nS	být
pátek	pátek	k1gInSc4	pátek
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
víkendu	víkend	k1gInSc2	víkend
(	(	kIx(	(
<g/>
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
pracovní	pracovní	k2eAgInSc1d1	pracovní
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tatáž	týž	k3xTgFnSc1	týž
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Bahrajnu	Bahrajno	k1gNnSc6	Bahrajno
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
arabských	arabský	k2eAgInPc6d1	arabský
emirátech	emirát	k1gInPc6	emirát
a	a	k8xC	a
Kuvajtu	Kuvajt	k1gInSc6	Kuvajt
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
pátek	pátek	k1gInSc4	pátek
první	první	k4xOgFnSc6	první
den	den	k1gInSc1	den
víkendu	víkend	k1gInSc2	víkend
a	a	k8xC	a
neděle	neděle	k1gFnSc2	neděle
je	být	k5eAaImIp3nS	být
začátek	začátek	k1gInSc4	začátek
pracovního	pracovní	k2eAgInSc2d1	pracovní
týdne	týden	k1gInSc2	týden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pátek	pátek	k1gInSc1	pátek
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
pro	pro	k7c4	pro
islám	islám	k1gInSc4	islám
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
den	den	k1gInSc1	den
společné	společný	k2eAgFnSc2d1	společná
týdenní	týdenní	k2eAgFnSc2d1	týdenní
modlitby	modlitba	k1gFnSc2	modlitba
v	v	k7c6	v
mešitě	mešita	k1gFnSc6	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
křesťanství	křesťanství	k1gNnSc4	křesťanství
pátek	pátek	k1gInSc4	pátek
dnem	den	k1gInSc7	den
smrti	smrt	k1gFnSc2	smrt
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
církvích	církev	k1gFnPc6	církev
den	den	k1gInSc4	den
pokání	pokání	k1gNnSc2	pokání
a	a	k8xC	a
postu	post	k1gInSc2	post
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
byzantském	byzantský	k2eAgInSc6d1	byzantský
ritu	rit	k1gInSc6	rit
se	se	k3xPyFc4	se
zvlášť	zvlášť	k6eAd1	zvlášť
připomíná	připomínat	k5eAaImIp3nS	připomínat
sv.	sv.	kA	sv.
Kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Pátek	Pátek	k1gMnSc1	Pátek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
počítán	počítán	k2eAgInSc1d1	počítán
i	i	k9	i
jako	jako	k9	jako
šestý	šestý	k4xOgInSc4	šestý
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
neděli	neděle	k1gFnSc4	neděle
chápeme	chápat	k5eAaImIp1nP	chápat
jako	jako	k9	jako
první	první	k4xOgFnPc1	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
jako	jako	k9	jako
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
název	název	k1gInSc4	název
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
latině	latina	k1gFnSc6	latina
(	(	kIx(	(
<g/>
feria	feria	k1gFnSc1	feria
sexta	sexta	k1gFnSc1	sexta
<g/>
)	)	kIx)	)
a	a	k8xC	a
portugalštině	portugalština	k1gFnSc6	portugalština
(	(	kIx(	(
<g/>
Sexta-feira	Sextaeira	k1gFnSc1	Sexta-feira
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
latině	latina	k1gFnSc6	latina
se	se	k3xPyFc4	se
pátek	pátek	k1gInSc1	pátek
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
Dies	Dies	k1gInSc1	Dies
Veneris	Veneris	k1gInSc1	Veneris
(	(	kIx(	(
<g/>
Venušin	Venušin	k2eAgInSc1d1	Venušin
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
názvy	název	k1gInPc7	název
pátku	pátek	k1gInSc2	pátek
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
románských	románský	k2eAgInPc2d1	románský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
it.	it.	k?	it.
venerdì	venerdì	k?	venerdì
<g/>
,	,	kIx,	,
špan.	špan.	k?	špan.
viernes	viernes	k1gInSc1	viernes
<g/>
,	,	kIx,	,
fr.	fr.	k?	fr.
vendredi	vendredit	k5eAaPmRp2nS	vendredit
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgFnPc1d1	anglická
Friday	Fridaa	k1gFnPc1	Fridaa
či	či	k8xC	či
německé	německý	k2eAgInPc1d1	německý
Freitag	Freitag	k1gInSc1	Freitag
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
germánské	germánský	k2eAgFnSc2d1	germánská
bohyně	bohyně	k1gFnSc2	bohyně
Freya	Frey	k1gInSc2	Frey
(	(	kIx(	(
<g/>
jinak	jinak	k6eAd1	jinak
psáno	psát	k5eAaImNgNnS	psát
Frija	Frijum	k1gNnSc2	Frijum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
zasvěcen	zasvětit	k5eAaPmNgMnS	zasvětit
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
představovala	představovat	k5eAaImAgFnS	představovat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
a	a	k8xC	a
římské	římský	k2eAgFnSc3d1	římská
mytologii	mytologie	k1gFnSc3	mytologie
<g/>
,	,	kIx,	,
bohyni	bohyně	k1gFnSc3	bohyně
krásy	krása	k1gFnSc2	krása
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podobného	podobný	k2eAgInSc2d1	podobný
základu	základ	k1gInSc2	základ
vychází	vycházet	k5eAaImIp3nS	vycházet
název	název	k1gInSc1	název
pátku	pátek	k1gInSc2	pátek
i	i	k9	i
v	v	k7c6	v
holandštině	holandština	k1gFnSc6	holandština
(	(	kIx(	(
<g/>
vrijdag	vrijdag	k1gInSc1	vrijdag
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
švédštině	švédština	k1gFnSc3	švédština
<g/>
,	,	kIx,	,
norštině	norština	k1gFnSc3	norština
a	a	k8xC	a
dánštine	dánštin	k1gInSc5	dánštin
(	(	kIx(	(
<g/>
fredag	fredaga	k1gFnPc2	fredaga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hindštině	hindština	k1gFnSc6	hindština
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pátku	pátek	k1gInSc2	pátek
shukravar	shukravar	k1gInSc1	shukravar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
Shukra	Shukr	k1gInSc2	Shukr
<g/>
,	,	kIx,	,
názvu	název	k1gInSc2	název
planety	planeta	k1gFnSc2	planeta
Venuše	Venuše	k1gFnSc2	Venuše
v	v	k7c6	v
sanskrtu	sanskrt	k1gInSc6	sanskrt
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1	ruský
výraz	výraz	k1gInSc1	výraz
п	п	k?	п
znamená	znamenat	k5eAaImIp3nS	znamenat
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pátý	pátý	k4xOgInSc4	pátý
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
pátek	pátek	k1gInSc4	pátek
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
řečtině	řečtina	k1gFnSc6	řečtina
Π	Π	k?	Π
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
přípravy	příprava	k1gFnPc4	příprava
na	na	k7c4	na
židovský	židovský	k2eAgInSc4d1	židovský
svátek	svátek	k1gInSc4	svátek
sabat	sabat	k1gInSc1	sabat
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
večer	večer	k6eAd1	večer
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
pětidenním	pětidenní	k2eAgInSc7d1	pětidenní
pracovním	pracovní	k2eAgInSc7d1	pracovní
týdnem	týden	k1gInSc7	týden
je	být	k5eAaImIp3nS	být
pátek	pátek	k1gInSc1	pátek
posledním	poslední	k2eAgNnSc7d1	poslední
pracovním	pracovní	k2eAgNnSc7d1	pracovní
dnem	dno	k1gNnSc7	dno
před	před	k7c7	před
víkendem	víkend	k1gInSc7	víkend
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nahlížen	nahlížen	k2eAgInSc1d1	nahlížen
jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
pro	pro	k7c4	pro
oslavu	oslava	k1gFnSc4	oslava
nebo	nebo	k8xC	nebo
rozptýlení	rozptýlení	k1gNnSc4	rozptýlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
úřadech	úřad	k1gInPc6	úřad
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
povoleno	povolen	k2eAgNnSc1d1	povoleno
nošení	nošení	k1gNnSc1	nošení
méně	málo	k6eAd2	málo
oficiálního	oficiální	k2eAgInSc2d1	oficiální
oděvu	oděv	k1gInSc2	oděv
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
pátky	pátek	k1gInPc1	pátek
také	také	k6eAd1	také
známy	znám	k2eAgInPc1d1	znám
jako	jako	k8xC	jako
Casual	Casual	k1gInSc1	Casual
Friday	Fridaa	k1gFnSc2	Fridaa
(	(	kIx(	(
<g/>
Ležérní	ležérní	k2eAgInSc4d1	ležérní
pátek	pátek	k1gInSc4	pátek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Dress-Down	Dress-Down	k1gInSc4	Dress-Down
Friday	Fridaa	k1gFnSc2	Fridaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
se	se	k3xPyFc4	se
s	s	k7c7	s
pátkem	pátek	k1gInSc7	pátek
spojuje	spojovat	k5eAaImIp3nS	spojovat
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
Thajský	thajský	k2eAgInSc1d1	thajský
solární	solární	k2eAgInSc1d1	solární
kalendář	kalendář	k1gInSc1	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Spojené	spojený	k2eAgInPc1d1	spojený
Státy	stát	k1gInPc1	stát
získaly	získat	k5eAaPmAgInP	získat
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
Aljašku	Aljaška	k1gFnSc4	Aljaška
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
také	také	k9	také
nutná	nutný	k2eAgFnSc1d1	nutná
změna	změna	k1gFnSc1	změna
data	datum	k1gNnSc2	datum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
používán	používat	k5eAaImNgInS	používat
Juliánský	juliánský	k2eAgInSc1d1	juliánský
kalendář	kalendář	k1gInSc1	kalendář
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
britských	britský	k2eAgFnPc6d1	britská
koloniích	kolonie	k1gFnPc6	kolonie
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1752	[number]	k4	1752
kalendář	kalendář	k1gInSc1	kalendář
Gregoriánský	gregoriánský	k2eAgInSc1d1	gregoriánský
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1867	[number]	k4	1867
změnilo	změnit	k5eAaPmAgNnS	změnit
datum	datum	k1gNnSc1	datum
na	na	k7c4	na
pátek	pátek	k1gInSc4	pátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souviselo	souviset	k5eAaImAgNnS	souviset
i	i	k9	i
posunutí	posunutí	k1gNnSc1	posunutí
datové	datový	k2eAgFnSc2d1	datová
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
dříve	dříve	k6eAd2	dříve
vedena	vést	k5eAaImNgFnS	vést
po	po	k7c6	po
hranicích	hranice	k1gFnPc6	hranice
mezi	mezi	k7c7	mezi
Aljaškou	Aljaška	k1gFnSc7	Aljaška
a	a	k8xC	a
britskou	britský	k2eAgFnSc7d1	britská
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kolonie	kolonie	k1gFnSc2	kolonie
Britské	britský	k2eAgFnSc2d1	britská
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Židovský	židovský	k2eAgInSc1d1	židovský
Sabat	sabat	k1gInSc1	sabat
začíná	začínat	k5eAaImIp3nS	začínat
se	s	k7c7	s
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
do	do	k7c2	do
sobotního	sobotní	k2eAgInSc2d1	sobotní
soumraku	soumrak	k1gInSc2	soumrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
je	být	k5eAaImIp3nS	být
Velký	velký	k2eAgInSc4d1	velký
pátek	pátek	k1gInSc4	pátek
pátkem	pátek	k1gInSc7	pátek
před	před	k7c7	před
Velikonocemi	Velikonoce	k1gFnPc7	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
připomínkou	připomínka	k1gFnSc7	připomínka
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
katolíci	katolík	k1gMnPc1	katolík
a	a	k8xC	a
pravověrní	pravověrný	k2eAgMnPc1d1	pravověrný
anglikáni	anglikán	k1gMnPc1	anglikán
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
konzumace	konzumace	k1gFnSc1	konzumace
masa	masa	k1gFnSc1	masa
z	z	k7c2	z
teplokrevných	teplokrevný	k2eAgNnPc2d1	teplokrevné
ušlechtilých	ušlechtilý	k2eAgNnPc2d1	ušlechtilé
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
spokojují	spokojovat	k5eAaImIp3nP	spokojovat
s	s	k7c7	s
rybou	ryba	k1gFnSc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Kvakeři	kvaker	k1gMnPc1	kvaker
tradičně	tradičně	k6eAd1	tradičně
nazývají	nazývat	k5eAaImIp3nP	nazývat
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
šestý	šestý	k4xOgInSc1	šestý
den	den	k1gInSc1	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
vyhnuli	vyhnout	k5eAaPmAgMnP	vyhnout
pohanskému	pohanský	k2eAgInSc3d1	pohanský
původu	původ	k1gInSc3	původ
anglického	anglický	k2eAgInSc2d1	anglický
názvu	název	k1gInSc2	název
pátku	pátek	k1gInSc2	pátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
islámu	islám	k1gInSc6	islám
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pátek	pátek	k1gInSc4	pátek
dnem	dnem	k7c2	dnem
veřejné	veřejný	k2eAgFnSc2d1	veřejná
pobožnosti	pobožnost	k1gFnSc2	pobožnost
v	v	k7c6	v
mešitách	mešita	k1gFnPc6	mešita
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
páteční	páteční	k2eAgFnSc1d1	páteční
modlitba	modlitba	k1gFnSc1	modlitba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
islámských	islámský	k2eAgFnPc6d1	islámská
zemích	zem	k1gFnPc6	zem
týden	týden	k1gInSc4	týden
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
židovský	židovský	k2eAgInSc4d1	židovský
a	a	k8xC	a
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
dalších	další	k2eAgFnPc6d1	další
islámských	islámský	k2eAgFnPc6d1	islámská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
nebo	nebo	k8xC	nebo
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
však	však	k9	však
týden	týden	k1gInSc4	týden
začíná	začínat	k5eAaImIp3nS	začínat
sobotou	sobota	k1gFnSc7	sobota
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
</s>
<s>
Parasceve	Parascevat	k5eAaPmIp3nS	Parascevat
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
paraskeué	paraskeué	k1gNnSc2	paraskeué
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
nahrazující	nahrazující	k2eAgInSc1d1	nahrazující
starší	starý	k2eAgInSc1d2	starší
výraz	výraz	k1gInSc1	výraz
prosábbaton	prosábbaton	k1gInSc1	prosábbaton
'	'	kIx"	'
<g/>
pre-sabbath	preabbath	k1gInSc1	pre-sabbath
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
použitý	použitý	k2eAgInSc4d1	použitý
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Júdit	Júdit	k1gInSc1	Júdit
(	(	kIx(	(
<g/>
viii	viii	k1gNnSc1	viii
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
titulu	titul	k1gInSc6	titul
žalmu	žalm	k1gInSc2	žalm
92	[number]	k4	92
(	(	kIx(	(
<g/>
93	[number]	k4	93
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
helénskými	helénský	k2eAgMnPc7d1	helénský
Židy	Žid	k1gMnPc7	Žid
<g/>
,	,	kIx,	,
jménem	jméno	k1gNnSc7	jméno
pro	pro	k7c4	pro
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
řeckými	řecký	k2eAgInPc7d1	řecký
církevními	církevní	k2eAgInPc7d1	církevní
spisovateli	spisovatel	k1gMnSc6	spisovatel
po	po	k7c6	po
napsání	napsání	k1gNnSc6	napsání
"	"	kIx"	"
<g/>
Učení	učení	k1gNnSc6	učení
dvanácti	dvanáct	k4xCc2	dvanáct
apoštolů	apoštol	k1gMnPc2	apoštol
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
nejdříve	dříve	k6eAd3	dříve
označoval	označovat	k5eAaImAgInS	označovat
u	u	k7c2	u
Židů	Žid	k1gMnPc2	Žid
páteční	páteční	k2eAgNnSc1d1	páteční
odpoledne	odpoledne	k1gNnSc1	odpoledne
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vztahovat	vztahovat	k5eAaImF	vztahovat
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
prováděly	provádět	k5eAaImAgInP	provádět
"	"	kIx"	"
<g/>
přípravy	příprava	k1gFnSc2	příprava
<g/>
"	"	kIx"	"
na	na	k7c4	na
sabat	sabat	k1gInSc4	sabat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
bibli	bible	k1gFnSc6	bible
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
řecké	řecký	k2eAgNnSc1d1	řecké
slovo	slovo	k1gNnSc1	slovo
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Den	den	k1gInSc1	den
přípravy	příprava	k1gFnSc2	příprava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zákonné	zákonný	k2eAgInPc1d1	zákonný
předpisy	předpis	k1gInPc1	předpis
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
pečlivě	pečlivě	k6eAd1	pečlivě
dodržovány	dodržovat	k5eAaImNgInP	dodržovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přikázáno	přikázat	k5eAaPmNgNnS	přikázat
na	na	k7c4	na
Parasceve	Parasceev	k1gFnPc4	Parasceev
připravit	připravit	k5eAaPmF	připravit
před	před	k7c7	před
západem	západ	k1gInSc7	západ
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
sabat	sabat	k1gInSc1	sabat
začínal	začínat	k5eAaImAgInS	začínat
v	v	k7c4	v
páteční	páteční	k2eAgFnPc4d1	páteční
nocí	noc	k1gFnSc7	noc
<g/>
)	)	kIx)	)
tři	tři	k4xCgInPc4	tři
pokrmy	pokrm	k1gInPc4	pokrm
z	z	k7c2	z
vybraných	vybraný	k2eAgFnPc2d1	vybraná
potravin	potravina	k1gFnPc2	potravina
<g/>
;	;	kIx,	;
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
pustit	pustit	k5eAaPmF	pustit
se	se	k3xPyFc4	se
šestého	šestý	k4xOgInSc2	šestý
dne	den	k1gInSc2	den
odpoledne	odpoledne	k1gNnSc2	odpoledne
do	do	k7c2	do
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
do	do	k7c2	do
sabatu	sabat	k1gInSc2	sabat
<g/>
;	;	kIx,	;
Augustus	Augustus	k1gInSc1	Augustus
oprostil	oprostit	k5eAaPmAgInS	oprostit
Židy	Žid	k1gMnPc4	Žid
od	od	k7c2	od
určitých	určitý	k2eAgFnPc2d1	určitá
povinností	povinnost	k1gFnPc2	povinnost
od	od	k7c2	od
deváté	devátý	k4xOgFnSc2	devátý
hodiny	hodina	k1gFnSc2	hodina
(	(	kIx(	(
<g/>
Flavius	Flavius	k1gMnSc1	Flavius
Iosephus	Iosephus	k1gMnSc1	Iosephus
<g/>
,	,	kIx,	,
Židovské	židovský	k2eAgFnPc1d1	židovská
starožitnosti	starožitnost	k1gFnPc1	starožitnost
<g/>
,	,	kIx,	,
XVI	XVI	kA	XVI
<g/>
,	,	kIx,	,
vi	vi	k?	vi
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Parasceve	Parascevat	k5eAaPmIp3nS	Parascevat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
také	také	k9	také
aplikovalo	aplikovat	k5eAaBmAgNnS	aplikovat
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
určitých	určitý	k2eAgInPc2d1	určitý
svátků	svátek	k1gInPc2	svátek
charakteru	charakter	k1gInSc2	charakter
sabatu	sabat	k1gInSc2	sabat
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
především	především	k9	především
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
svátku	svátek	k1gInSc2	svátek
nekvašených	kvašený	k2eNgInPc2d1	nekvašený
chlebů	chléb	k1gInPc2	chléb
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Nisan	nisan	k1gInSc1	nisan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Mišny	Mišna	k1gFnSc2	Mišna
(	(	kIx(	(
<g/>
Pesach	pesach	k1gInSc1	pesach
<g/>
,	,	kIx,	,
iv	iv	k?	iv
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Parasceve	Parasceev	k1gFnPc1	Parasceev
Pesachu	pesach	k1gInSc2	pesach
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
již	již	k6eAd1	již
připadlo	připadnout	k5eAaPmAgNnS	připadnout
na	na	k7c4	na
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
dodržováno	dodržovat	k5eAaImNgNnS	dodržovat
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
nábožensky	nábožensky	k6eAd1	nábožensky
než	než	k8xS	než
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
v	v	k7c6	v
Judei	Judei	k1gNnSc6	Judei
se	se	k3xPyFc4	se
končilo	končit	k5eAaImAgNnS	končit
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
a	a	k8xC	a
v	v	k7c6	v
Galilei	Galile	k1gInSc6	Galile
byl	být	k5eAaImAgMnS	být
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
volný	volný	k2eAgInSc4d1	volný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
byly	být	k5eAaImAgFnP	být
probírány	probírat	k5eAaImNgFnP	probírat
pouze	pouze	k6eAd1	pouze
otázky	otázka	k1gFnPc1	otázka
diskutované	diskutovaný	k2eAgFnPc1d1	diskutovaná
ohledně	ohledně	k7c2	ohledně
tohoto	tento	k3xDgInSc2	tento
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
Parasceve	Parasceev	k1gFnSc2	Parasceev
<g/>
,	,	kIx,	,
odkdy	odkdy	k6eAd1	odkdy
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
odpočinek	odpočinek	k1gInSc1	odpočinek
<g/>
:	:	kIx,	:
Shammai	Shammai	k1gNnSc1	Shammai
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
začátku	začátek	k1gInSc6	začátek
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
(	(	kIx(	(
<g/>
večer	večer	k6eAd1	večer
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Nisanu	nisan	k1gInSc2	nisan
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Hillel	Hillel	k1gMnPc1	Hillel
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
východu	východ	k1gInSc6	východ
Slunce	slunce	k1gNnSc2	slunce
(	(	kIx(	(
<g/>
ráno	ráno	k6eAd1	ráno
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Nisanu	nisan	k1gInSc2	nisan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
slova	slovo	k1gNnSc2	slovo
Parasceve	Parasceev	k1gFnSc2	Parasceev
v	v	k7c6	v
evangeliích	evangelium	k1gNnPc6	evangelium
navozuje	navozovat	k5eAaImIp3nS	navozovat
otázku	otázka	k1gFnSc4	otázka
ohledně	ohledně	k7c2	ohledně
skutečné	skutečný	k2eAgFnSc2d1	skutečná
dne	den	k1gInSc2	den
Kristova	Kristův	k2eAgNnSc2d1	Kristovo
ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
evangelisté	evangelista	k1gMnPc1	evangelista
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c4	v
den	den	k1gInSc4	den
Parasceve	Parasceev	k1gFnSc2	Parasceev
(	(	kIx(	(
<g/>
Matouš	Matouš	k1gMnSc1	Matouš
27	[number]	k4	27
<g/>
:	:	kIx,	:
<g/>
62	[number]	k4	62
<g/>
;	;	kIx,	;
Marek	Marek	k1gMnSc1	Marek
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
42	[number]	k4	42
<g/>
;	;	kIx,	;
Lukáš	Lukáš	k1gMnSc1	Lukáš
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
<g/>
;	;	kIx,	;
Jan	Jan	k1gMnSc1	Jan
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
14	[number]	k4	14
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
z	z	k7c2	z
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
,	,	kIx,	,
xxiii	xxiius	k1gMnPc7	xxiius
<g/>
,	,	kIx,	,
54-56	[number]	k4	54-56
a	a	k8xC	a
Jana	Jan	k1gMnSc4	Jan
<g/>
,	,	kIx,	,
xix	xix	k?	xix
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
,	,	kIx,	,
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
otázkou	otázka	k1gFnSc7	otázka
na	na	k7c4	na
který	který	k3yIgInSc4	který
den	den	k1gInSc4	den
měsíce	měsíc	k1gInSc2	měsíc
Nisan	nisan	k1gInSc1	nisan
tento	tento	k3xDgInSc4	tento
pátek	pátek	k1gInSc4	pátek
připadl	připadnout	k5eAaPmAgInS	připadnout
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
zřetelně	zřetelně	k6eAd1	zřetelně
uvádí	uvádět	k5eAaImIp3nS	uvádět
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Nisan	nisan	k1gInSc1	nisan
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Synoptici	synoptik	k1gMnPc1	synoptik
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
byla	být	k5eAaImAgFnS	být
pesachovským	pesachovský	k2eAgNnSc7d1	pesachovský
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
byl	být	k5eAaImAgMnS	být
ukřižován	ukřižovat	k5eAaPmNgMnS	ukřižovat
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Nisan	nisan	k1gInSc1	nisan
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
těžko	těžko	k6eAd1	těžko
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
následujícími	následující	k2eAgInPc7d1	následující
fakty	fakt	k1gInPc7	fakt
<g/>
:	:	kIx,	:
když	když	k8xS	když
Jidáš	Jidáš	k1gMnSc1	Jidáš
opustil	opustit	k5eAaPmAgMnS	opustit
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
učedníci	učedník	k1gMnPc1	učedník
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
koupit	koupit	k5eAaPmF	koupit
věci	věc	k1gFnPc1	věc
potřebné	potřebný	k2eAgFnPc1d1	potřebná
ke	k	k7c3	k
svátku	svátek	k1gInSc3	svátek
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
<g/>
)	)	kIx)	)
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
již	již	k9	již
svátek	svátek	k1gInSc1	svátek
začal	začít	k5eAaPmAgInS	začít
<g/>
;	;	kIx,	;
po	po	k7c6	po
večeři	večeře	k1gFnSc6	večeře
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
učedníci	učedník	k1gMnPc1	učedník
opustili	opustit	k5eAaPmAgMnP	opustit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
udělali	udělat	k5eAaPmAgMnP	udělat
lidé	člověk	k1gMnPc1	člověk
detailně	detailně	k6eAd1	detailně
seznámení	seznámení	k1gNnSc4	seznámení
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
zatčení	zatčení	k1gNnSc4	zatčení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Nisan	nisan	k1gInSc1	nisan
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
Exodu	Exodus	k1gInSc3	Exodus
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
<g/>
;	;	kIx,	;
následující	následující	k2eAgInSc1d1	následující
den	den	k1gInSc1	den
ráno	ráno	k6eAd1	ráno
Židé	Žid	k1gMnPc1	Žid
ještě	ještě	k9	ještě
nejedli	jíst	k5eNaImAgMnP	jíst
beránka	beránek	k1gMnSc4	beránek
a	a	k8xC	a
nadto	nadto	k6eAd1	nadto
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
dne	den	k1gInSc2	den
svolána	svolat	k5eAaPmNgFnS	svolat
rada	rada	k1gFnSc1	rada
<g/>
;	;	kIx,	;
Šimon	Šimon	k1gMnSc1	Šimon
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nesl	nést	k5eAaImAgMnS	nést
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přišel	přijít	k5eAaPmAgMnS	přijít
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
Lukáš	Lukáš	k1gMnSc1	Lukáš
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
26	[number]	k4	26
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Ježíš	Ježíš	k1gMnSc1	Ježíš
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
lotrové	lotrové	k?	lotrové
byli	být	k5eAaImAgMnP	být
ukřižováni	ukřižovat	k5eAaPmNgMnP	ukřižovat
a	a	k8xC	a
pak	pak	k6eAd1	pak
sňati	snít	k5eAaPmNgMnP	snít
z	z	k7c2	z
křížů	kříž	k1gInPc2	kříž
<g/>
;	;	kIx,	;
Josef	Josef	k1gMnSc1	Josef
z	z	k7c2	z
Arimatie	Arimatie	k1gFnSc2	Arimatie
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
jemné	jemný	k2eAgNnSc4d1	jemné
plátno	plátno	k1gNnSc4	plátno
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
46	[number]	k4	46
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nikodém	Nikodém	k1gMnSc1	Nikodém
přinesl	přinést	k5eAaPmAgMnS	přinést
asi	asi	k9	asi
sto	sto	k4xCgNnSc4	sto
liber	libra	k1gFnPc2	libra
směsi	směs	k1gFnSc2	směs
myrhy	myrha	k1gFnSc2	myrha
a	a	k8xC	a
aloe	aloe	k1gFnSc2	aloe
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
<g/>
)	)	kIx)	)
k	k	k7c3	k
pohřbu	pohřeb	k1gInSc3	pohřeb
<g/>
;	;	kIx,	;
konečně	konečně	k6eAd1	konečně
ženy	žena	k1gFnPc1	žena
připravovaly	připravovat	k5eAaImAgFnP	připravovat
koření	koření	k1gNnPc1	koření
pro	pro	k7c4	pro
balzamování	balzamování	k1gNnSc4	balzamování
spasitelova	spasitelův	k2eAgNnSc2d1	Spasitelovo
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
Lukáš	Lukáš	k1gMnSc1	Lukáš
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
)	)	kIx)	)
-	-	kIx~	-
všechno	všechen	k3xTgNnSc1	všechen
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
věci	věc	k1gFnPc1	věc
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
znesvětily	znesvětit	k5eAaPmAgInP	znesvětit
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Nisan	nisan	k1gInSc1	nisan
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vykladačů	vykladač	k1gMnPc2	vykladač
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Poslední	poslední	k2eAgFnSc1d1	poslední
večeře	večeře	k1gFnSc1	večeře
byla	být	k5eAaImAgFnS	být
jídlem	jídlo	k1gNnSc7	jídlo
velikonočním	velikonoční	k2eAgNnSc7d1	velikonoční
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
předcházejícím	předcházející	k2eAgMnPc3d1	předcházející
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ježíš	Ježíš	k1gMnSc1	Ježíš
byl	být	k5eAaImAgMnS	být
ukřižován	ukřižovat	k5eAaPmNgMnS	ukřižovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Nisanu	nisan	k1gInSc3	nisan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
pátek	pátek	k1gInSc4	pátek
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
planetou	planeta	k1gFnSc7	planeta
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
spojuje	spojovat	k5eAaImIp3nS	spojovat
pátek	pátek	k1gInSc4	pátek
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
mírem	mír	k1gInSc7	mír
a	a	k8xC	a
relaxací	relaxace	k1gFnSc7	relaxace
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
s	s	k7c7	s
citovou	citový	k2eAgFnSc7d1	citová
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
potlačenými	potlačený	k2eAgInPc7d1	potlačený
sny	sen	k1gInPc7	sen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
kulturách	kultura	k1gFnPc6	kultura
je	být	k5eAaImIp3nS	být
pátek	pátek	k1gInSc1	pátek
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nešťastný	šťastný	k2eNgInSc4d1	nešťastný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pátek	pátek	k1gInSc4	pátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pověra	pověra	k1gFnSc1	pověra
je	být	k5eAaImIp3nS	být
zvlášť	zvlášť	k6eAd1	zvlášť
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
v	v	k7c6	v
námořnických	námořnický	k2eAgInPc6d1	námořnický
kruzích	kruh	k1gInPc6	kruh
<g/>
;	;	kIx,	;
snad	snad	k9	snad
nejvíce	nejvíce	k6eAd1	nejvíce
známá	známý	k2eAgFnSc1d1	známá
byla	být	k5eAaImAgFnS	být
pověra	pověra	k1gFnSc1	pověra
<g/>
,	,	kIx,	,
že	že	k8xS	že
zahájení	zahájení	k1gNnSc1	zahájení
plavby	plavba	k1gFnSc2	plavba
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
přináší	přinášet	k5eAaImIp3nS	přinášet
smůlu	smůla	k1gFnSc4	smůla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
příběhů	příběh	k1gInPc2	příběh
loď	loď	k1gFnSc1	loď
Královského	královský	k2eAgNnSc2d1	královské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
(	(	kIx(	(
<g/>
HMS	HMS	kA	HMS
Friday	Frida	k2eAgInPc1d1	Frida
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
velel	velet	k5eAaImAgMnS	velet
jí	on	k3xPp3gFnSc3	on
kapitán	kapitán	k1gMnSc1	kapitán
Friday	Fridaa	k1gFnPc4	Fridaa
<g/>
,	,	kIx,	,
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
více	hodně	k6eAd2	hodně
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
slyšet	slyšet	k5eAaImF	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
systému	systém	k1gInSc6	systém
přestupného	přestupný	k2eAgInSc2d1	přestupný
roku	rok	k1gInSc2	rok
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
malou	malý	k2eAgFnSc4d1	malá
statickou	statický	k2eAgFnSc4d1	statická
neobvyklost	neobvyklost	k1gFnSc4	neobvyklost
<g/>
,	,	kIx,	,
že	že	k8xS	že
13	[number]	k4	13
<g/>
.	.	kIx.	.
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
měsíce	měsíc	k1gInSc2	měsíc
připadá	připadat	k5eAaPmIp3nS	připadat
s	s	k7c7	s
mírně	mírně	k6eAd1	mírně
větší	veliký	k2eAgFnSc7d2	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
na	na	k7c4	na
pátek	pátek	k1gInSc4	pátek
než	než	k8xS	než
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
688	[number]	k4	688
<g/>
/	/	kIx~	/
<g/>
4800	[number]	k4	4800
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
dává	dávat	k5eAaImIp3nS	dávat
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
hodnotu	hodnota	k1gFnSc4	hodnota
0,1433333	[number]	k4	0,1433333
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
málo	málo	k6eAd1	málo
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
pátek	pátek	k1gInSc1	pátek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
kalendáři	kalendář	k1gInSc6	kalendář
pátkem	pátek	k1gInSc7	pátek
před	před	k7c7	před
velikonocemi	velikonoce	k1gFnPc7	velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
připomínkou	připomínka	k1gFnSc7	připomínka
ukřižování	ukřižování	k1gNnPc2	ukřižování
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
pátek	pátek	k1gInSc1	pátek
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
historické	historický	k2eAgFnPc4d1	historická
pohromy	pohroma	k1gFnPc4	pohroma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
vždy	vždy	k6eAd1	vždy
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc4d1	černý
pátek	pátek	k1gInSc4	pátek
také	také	k9	také
přezdívka	přezdívka	k1gFnSc1	přezdívka
pro	pro	k7c4	pro
den	den	k1gInSc4	den
po	po	k7c6	po
dni	den	k1gInSc6	den
díkuvzdání	díkuvzdání	k1gNnSc2	díkuvzdání
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
oficiální	oficiální	k2eAgInSc4d1	oficiální
den	den	k1gInSc4	den
vánočního	vánoční	k2eAgInSc2d1	vánoční
sezónního	sezónní	k2eAgInSc2d1	sezónní
prodeje	prodej	k1gInSc2	prodej
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většina	většina	k1gFnSc1	většina
obchodů	obchod	k1gInPc2	obchod
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
ziskům	zisk	k1gInPc3	zisk
dostává	dostávat	k5eAaImIp3nS	dostávat
ze	z	k7c2	z
ztrát	ztráta	k1gFnPc2	ztráta
(	(	kIx(	(
<g/>
z	z	k7c2	z
"	"	kIx"	"
<g/>
červených	červený	k2eAgInPc2d1	červený
<g/>
"	"	kIx"	"
do	do	k7c2	do
"	"	kIx"	"
<g/>
černých	černý	k2eAgInPc2d1	černý
<g/>
"	"	kIx"	"
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ležérní	ležérní	k2eAgInSc4d1	ležérní
pátek	pátek	k1gInSc4	pátek
(	(	kIx(	(
<g/>
také	také	k9	také
zvaný	zvaný	k2eAgInSc1d1	zvaný
Dress-down	Dressown	k1gInSc1	Dress-down
Friday	Fridaa	k1gMnSc2	Fridaa
nebo	nebo	k8xC	nebo
Aloha	Aloh	k1gMnSc2	Aloh
Friday	Fridaa	k1gMnSc2	Fridaa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
některých	některý	k3yIgFnPc2	některý
firem	firma	k1gFnPc2	firma
nošení	nošení	k1gNnSc2	nošení
ležérních	ležérní	k2eAgInPc2d1	ležérní
oděvů	oděv	k1gInPc2	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Pátek	pátek	k1gInSc1	pátek
třináctého	třináctý	k4xOgNnSc2	třináctý
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pátek	pátek	k1gInSc4	pátek
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
pátek	pátek	k1gInSc4	pátek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
