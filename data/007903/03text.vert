<s>
Milena	Milena	k1gFnSc1	Milena
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
jméno	jméno	k1gNnSc4	jméno
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
kalendáři	kalendář	k1gInSc6	kalendář
má	mít	k5eAaImIp3nS	mít
jmeniny	jmeniny	k1gFnPc4	jmeniny
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Mužským	mužský	k2eAgInSc7d1	mužský
protějškem	protějšek	k1gInSc7	protějšek
je	být	k5eAaImIp3nS	být
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
příponou	přípona	k1gFnSc7	přípona
-ena	n	k1gInSc2	-en
od	od	k7c2	od
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
milý	milý	k2eAgMnSc1d1	milý
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
tedy	tedy	k9	tedy
milující	milující	k2eAgFnSc1d1	milující
<g/>
,	,	kIx,	,
milá	milý	k2eAgFnSc1d1	Milá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obdobou	obdoba	k1gFnSc7	obdoba
latinského	latinský	k2eAgInSc2d1	latinský
Amáta	Amát	k1gInSc2	Amát
nebo	nebo	k8xC	nebo
Amanda	Amanda	k1gFnSc1	Amanda
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
největší	veliký	k2eAgFnPc4d3	veliký
obliby	obliba	k1gFnPc4	obliba
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
-0,3	-0,3	k4	-0,3
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Miléna	Miléna	k1gFnSc1	Miléna
–	–	k?	–
maďarsky	maďarsky	k6eAd1	maďarsky
Milena	Milena	k1gFnSc1	Milena
–	–	k?	–
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
,	,	kIx,	,
srbsky	srbsky	k6eAd1	srbsky
Mylene	Mylen	k1gInSc5	Mylen
–	–	k?	–
švédsky	švédsky	k6eAd1	švédsky
Aimée	Aimé	k1gMnPc4	Aimé
Mylè	Mylè	k1gMnSc1	Mylè
Demongeot	Demongeot	k1gMnSc1	Demongeot
–	–	k?	–
francouzská	francouzský	k2eAgFnSc1d1	francouzská
herečka	herečka	k1gFnSc1	herečka
Milena	Milena	k1gFnSc1	Milena
Dvorská	Dvorská	k1gFnSc1	Dvorská
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Milena	Milena	k1gFnSc1	Milena
Duchková	Duchková	k1gFnSc1	Duchková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
sportovkyně	sportovkyně	k1gFnSc1	sportovkyně
<g/>
,	,	kIx,	,
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
ve	v	k7c6	v
skocích	skok	k1gInPc6	skok
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
Milena	Milena	k1gFnSc1	Milena
Fucimanová	Fucimanová	k1gFnSc1	Fucimanová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Milena	Milena	k1gFnSc1	Milena
Hübschmannová	Hübschmannová	k1gFnSc1	Hübschmannová
–	–	k?	–
zakladatelka	zakladatelka	k1gFnSc1	zakladatelka
české	český	k2eAgFnPc1d1	Česká
romistiky	romistika	k1gFnPc1	romistika
Milena	Milena	k1gFnSc1	Milena
Jesenská	Jesenská	k1gFnSc1	Jesenská
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
novinářka	novinářka	k1gFnSc1	novinářka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Milena	Milena	k1gFnSc1	Milena
Steinmasslová	Steinmasslový	k2eAgFnSc1d1	Steinmasslová
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Milena	Milena	k1gFnSc1	Milena
Vostřáková	Vostřáková	k1gFnSc1	Vostřáková
–	–	k?	–
česká	český	k2eAgFnSc1d1	Česká
televizní	televizní	k2eAgFnSc1d1	televizní
hlasatelka	hlasatelka	k1gFnSc1	hlasatelka
a	a	k8xC	a
moderátorka	moderátorka	k1gFnSc1	moderátorka
Milena	Milena	k1gFnSc1	Milena
Vukotić	Vukotić	k1gFnSc1	Vukotić
–	–	k?	–
černohorská	černohorský	k2eAgFnSc1d1	černohorská
královna	královna	k1gFnSc1	královna
Milena	Milena	k1gFnSc1	Milena
Markovna	Markovna	k1gFnSc1	Markovna
Kunis	Kunis	k1gFnSc1	Kunis
–	–	k?	–
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
původu	původ	k1gInSc2	původ
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Milena	Milena	k1gFnSc1	Milena
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
