<p>
<s>
Hrobník	hrobník	k1gMnSc1	hrobník
je	být	k5eAaImIp3nS	být
novela	novela	k1gFnSc1	novela
Karla	Karel	k1gMnSc2	Karel
Sabiny	Sabina	k1gMnSc2	Sabina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
(	(	kIx(	(
<g/>
časopis	časopis	k1gInSc1	časopis
Květy	Květa	k1gFnSc2	Květa
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
knižní	knižní	k2eAgMnSc1d1	knižní
pak	pak	k6eAd1	pak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
u	u	k7c2	u
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pospíšila	Pospíšil	k1gMnSc2	Pospíšil
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
novele	novela	k1gFnSc6	novela
je	být	k5eAaImIp3nS	být
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
život	život	k1gInSc1	život
hrobníka	hrobník	k1gMnSc2	hrobník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kvůli	kvůli	k7c3	kvůli
nenaplněné	naplněný	k2eNgFnSc3d1	nenaplněná
lásce	láska	k1gFnSc3	láska
mladé	mladý	k2eAgFnSc2d1	mladá
hraběnky	hraběnka	k1gFnSc2	hraběnka
a	a	k8xC	a
ústrkům	ústrk	k1gInPc3	ústrk
od	od	k7c2	od
jejího	její	k3xOp3gMnSc2	její
bratra	bratr	k1gMnSc2	bratr
spáchá	spáchat	k5eAaPmIp3nS	spáchat
zločin	zločin	k1gInSc1	zločin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
relativně	relativně	k6eAd1	relativně
krátký	krátký	k2eAgInSc4d1	krátký
rozsah	rozsah	k1gInSc4	rozsah
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
novela	novela	k1gFnSc1	novela
komplikované	komplikovaný	k2eAgInPc1d1	komplikovaný
osudy	osud	k1gInPc1	osud
nejen	nejen	k6eAd1	nejen
hrobníka	hrobník	k1gMnSc4	hrobník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jeho	jeho	k3xOp3gFnPc4	jeho
nešťastně	šťastně	k6eNd1	šťastně
zamilované	zamilovaný	k2eAgFnPc4d1	zamilovaná
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příběh	příběh	k1gInSc1	příběh
==	==	k?	==
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
neurčené	určený	k2eNgFnSc6d1	neurčená
vesnici	vesnice	k1gFnSc6	vesnice
K.	K.	kA	K.
<g/>
..	..	k?	..
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
několik	několik	k4yIc4	několik
občanů	občan	k1gMnPc2	občan
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
pověst	pověst	k1gFnSc4	pověst
o	o	k7c6	o
tamějším	tamější	k2eAgInSc6d1	tamější
rybníku	rybník	k1gInSc6	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
stal	stát	k5eAaPmAgInS	stát
místem	místo	k1gNnSc7	místo
smrti	smrt	k1gFnSc2	smrt
mnoha	mnoho	k4c2	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
hrázi	hráz	k1gFnSc6	hráz
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
16	[number]	k4	16
lety	léto	k1gNnPc7	léto
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
i	i	k8xC	i
bezhlavé	bezhlavý	k2eAgNnSc1d1	bezhlavé
tělo	tělo	k1gNnSc1	tělo
hraběte	hrabě	k1gMnSc2	hrabě
Věnceslava	Věnceslava	k1gFnSc1	Věnceslava
<g/>
.	.	kIx.	.
</s>
<s>
Vrah	vrah	k1gMnSc1	vrah
ani	ani	k8xC	ani
motiv	motiv	k1gInSc1	motiv
činu	čin	k1gInSc2	čin
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
vypátrán	vypátrat	k5eAaPmNgInS	vypátrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
vypravování	vypravování	k1gNnSc2	vypravování
přichází	přicházet	k5eAaImIp3nS	přicházet
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
smrti	smrt	k1gFnSc6	smrt
klášterní	klášterní	k2eAgFnSc2d1	klášterní
panny	panna	k1gFnSc2	panna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnSc2	sestra
Věnceslava	Věnceslava	k1gFnSc1	Věnceslava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
se	se	k3xPyFc4	se
o	o	k7c6	o
novém	nový	k2eAgMnSc6d1	nový
pánovi	pán	k1gMnSc6	pán
<g/>
,	,	kIx,	,
knížeti	kníže	k1gMnSc6	kníže
Vilímovi	Vilím	k1gMnSc6	Vilím
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Věnceslavův	Věnceslavův	k2eAgMnSc1d1	Věnceslavův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nikým	nikdo	k3yNnSc7	nikdo
příliš	příliš	k6eAd1	příliš
nekomunikuje	komunikovat	k5eNaImIp3nS	komunikovat
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
místním	místní	k2eAgMnSc7d1	místní
hrobníkem	hrobník	k1gMnSc7	hrobník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
po	po	k7c6	po
nocích	noc	k1gFnPc6	noc
často	často	k6eAd1	často
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnSc1d1	místní
hrobník	hrobník	k1gMnSc1	hrobník
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
vesničanům	vesničan	k1gMnPc3	vesničan
také	také	k9	také
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nikomu	nikdo	k3yNnSc3	nikdo
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zjistit	zjistit	k5eAaPmF	zjistit
nic	nic	k3yNnSc1	nic
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
dcerou	dcera	k1gFnSc7	dcera
Klárou	Klára	k1gFnSc7	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Hrobník	hrobník	k1gMnSc1	hrobník
je	být	k5eAaImIp3nS	být
popisován	popisován	k2eAgMnSc1d1	popisován
jako	jako	k8xC	jako
chytrý	chytrý	k2eAgMnSc1d1	chytrý
a	a	k8xC	a
tajemný	tajemný	k2eAgMnSc1d1	tajemný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
tajemné	tajemný	k2eAgFnSc3d1	tajemná
povaze	povaha	k1gFnSc3	povaha
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
mají	mít	k5eAaImIp3nP	mít
lidé	člověk	k1gMnPc1	člověk
trochu	trochu	k6eAd1	trochu
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
však	však	k9	však
vychovat	vychovat	k5eAaPmF	vychovat
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
k	k	k7c3	k
nábožnosti	nábožnost	k1gFnSc3	nábožnost
a	a	k8xC	a
počestnosti	počestnost	k1gFnSc3	počestnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
příběhu	příběh	k1gInSc2	příběh
přemlouvá	přemlouvat	k5eAaImIp3nS	přemlouvat
hrobník	hrobník	k1gMnSc1	hrobník
Kláru	Klára	k1gFnSc4	Klára
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
ochránil	ochránit	k5eAaPmAgMnS	ochránit
před	před	k7c7	před
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
už	už	k6eAd1	už
sám	sám	k3xTgMnSc1	sám
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Klára	Klára	k1gFnSc1	Klára
je	být	k5eAaImIp3nS	být
zamilována	zamilován	k2eAgFnSc1d1	zamilována
do	do	k7c2	do
Vilíma	Vilím	k1gMnSc2	Vilím
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
citu	cit	k1gInSc6	cit
vůbec	vůbec	k9	vůbec
neví	vědět	k5eNaImIp3nS	vědět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrobník	hrobník	k1gMnSc1	hrobník
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
lásce	láska	k1gFnSc3	láska
snaží	snažit	k5eAaImIp3nP	snažit
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Donutí	donutit	k5eAaPmIp3nS	donutit
Kláru	Klára	k1gFnSc4	Klára
přísahat	přísahat	k5eAaImF	přísahat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
citu	cit	k1gInSc6	cit
k	k	k7c3	k
Vilímovi	Vilím	k1gMnSc3	Vilím
zřekla	zřeknout	k5eAaPmAgFnS	zřeknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
rozhovoru	rozhovor	k1gInSc2	rozhovor
hrobníka	hrobník	k1gMnSc2	hrobník
s	s	k7c7	s
Vilímem	Vilím	k1gMnSc7	Vilím
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
byl	být	k5eAaImAgMnS	být
asi	asi	k9	asi
svůdcem	svůdce	k1gMnSc7	svůdce
hrobníkovy	hrobníkův	k2eAgFnSc2d1	hrobníkova
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
že	že	k8xS	že
Vilím	Vilím	k1gMnSc1	Vilím
je	být	k5eAaImIp3nS	být
rozhodnut	rozhodnout	k5eAaPmNgMnS	rozhodnout
najít	najít	k5eAaPmF	najít
vraha	vrah	k1gMnSc4	vrah
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Vilím	Vilím	k1gMnSc1	Vilím
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
hrobníkovu	hrobníkův	k2eAgFnSc4d1	hrobníkova
snahu	snaha	k1gFnSc4	snaha
poslat	poslat	k5eAaPmF	poslat
Kláru	Klára	k1gFnSc4	Klára
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
;	;	kIx,	;
pozemská	pozemský	k2eAgFnSc1d1	pozemská
láska	láska	k1gFnSc1	láska
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc1	smysl
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
malicherností	malichernost	k1gFnSc7	malichernost
<g/>
.	.	kIx.	.
</s>
<s>
Svědkem	svědek	k1gMnSc7	svědek
tohoto	tento	k3xDgInSc2	tento
rozhovoru	rozhovor	k1gInSc2	rozhovor
je	být	k5eAaImIp3nS	být
Klára	Klára	k1gFnSc1	Klára
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
zamlčet	zamlčet	k5eAaPmF	zamlčet
své	svůj	k3xOyFgInPc4	svůj
pocity	pocit	k1gInPc4	pocit
vůči	vůči	k7c3	vůči
knížeti	kníže	k1gNnSc3wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohřbu	pohřeb	k1gInSc6	pohřeb
Alžběty	Alžběta	k1gFnSc2	Alžběta
hrobník	hrobník	k1gMnSc1	hrobník
duševně	duševně	k6eAd1	duševně
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vrací	vracet	k5eAaImIp3nS	vracet
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gInSc2	on
projevují	projevovat	k5eAaImIp3nP	projevovat
noční	noční	k2eAgInPc1d1	noční
záchvaty	záchvat	k1gInPc1	záchvat
šílenosti	šílenost	k1gFnSc2	šílenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedné	jeden	k4xCgFnSc6	jeden
bouřkové	bouřkový	k2eAgFnSc6d1	bouřková
noci	noc	k1gFnSc6	noc
je	být	k5eAaImIp3nS	být
Vilím	Vilím	k1gMnSc1	Vilím
svědkem	svědek	k1gMnSc7	svědek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hrobník	hrobník	k1gMnSc1	hrobník
vykopává	vykopávat	k5eAaImIp3nS	vykopávat
Alžbětin	Alžbětin	k2eAgInSc4d1	Alžbětin
hrob	hrob	k1gInSc4	hrob
<g/>
,	,	kIx,	,
pláče	plakat	k5eAaImIp3nS	plakat
nad	nad	k7c7	nad
jejím	její	k3xOp3gNnSc7	její
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
na	na	k7c4	na
konec	konec	k1gInSc4	konec
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
šperky	šperk	k1gInPc7	šperk
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
Vilím	Vilím	k1gMnSc1	Vilím
těžce	těžce	k6eAd1	těžce
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Vyprosí	vyprosit	k5eAaPmIp3nP	vyprosit
si	se	k3xPyFc3	se
za	za	k7c4	za
ošetřovatelku	ošetřovatelka	k1gFnSc4	ošetřovatelka
Kláru	Klára	k1gFnSc4	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
skrz	skrz	k7c4	skrz
ni	on	k3xPp3gFnSc4	on
zjistit	zjistit	k5eAaPmF	zjistit
hrobníkovo	hrobníkův	k2eAgNnSc4d1	hrobníkův
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Tuší	tušit	k5eAaImIp3nS	tušit
blížící	blížící	k2eAgFnSc1d1	blížící
se	se	k3xPyFc4	se
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
Kláře	Klára	k1gFnSc3	Klára
zaplatit	zaplatit	k5eAaPmF	zaplatit
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
péči	péče	k1gFnSc4	péče
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
ji	on	k3xPp3gFnSc4	on
velmi	velmi	k6eAd1	velmi
raní	ranit	k5eAaPmIp3nS	ranit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Vilíma	Vilím	k1gMnSc4	Vilím
stále	stále	k6eAd1	stále
miluje	milovat	k5eAaImIp3nS	milovat
a	a	k8xC	a
starala	starat	k5eAaImAgFnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Vilím	Vilím	k1gMnSc1	Vilím
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
několik	několik	k4yIc4	několik
okamžiků	okamžik	k1gInPc2	okamžik
později	pozdě	k6eAd2	pozdě
lékařem	lékař	k1gMnSc7	lékař
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
rodinné	rodinný	k2eAgFnSc2d1	rodinná
hrobky	hrobka	k1gFnSc2	hrobka
<g/>
.	.	kIx.	.
</s>
<s>
Vilím	Vilím	k1gMnSc1	Vilím
však	však	k9	však
není	být	k5eNaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
<g/>
,	,	kIx,	,
prodělal	prodělat	k5eAaPmAgMnS	prodělat
pouze	pouze	k6eAd1	pouze
jakýsi	jakýsi	k3yIgInSc4	jakýsi
záchvat	záchvat	k1gInSc4	záchvat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
důsledku	důsledek	k1gInSc6	důsledek
upadl	upadnout	k5eAaPmAgMnS	upadnout
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
srdečního	srdeční	k2eAgInSc2d1	srdeční
pulsu	puls	k1gInSc2	puls
a	a	k8xC	a
tělesný	tělesný	k2eAgInSc4d1	tělesný
chlad	chlad	k1gInSc4	chlad
oklamaly	oklamat	k5eAaPmAgFnP	oklamat
lékaře	lékař	k1gMnPc4	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Vilím	Vilím	k1gMnSc1	Vilím
není	být	k5eNaImIp3nS	být
schopný	schopný	k2eAgMnSc1d1	schopný
žádného	žádný	k3yNgInSc2	žádný
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
vnímá	vnímat	k5eAaImIp3nS	vnímat
dění	dění	k1gNnSc4	dění
okolo	okolo	k7c2	okolo
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
v	v	k7c6	v
rakvi	rakev	k1gFnSc6	rakev
doufá	doufat	k5eAaImIp3nS	doufat
v	v	k7c4	v
rychlou	rychlý	k2eAgFnSc4d1	rychlá
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rakve	rakev	k1gFnSc2	rakev
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
Klára	Klára	k1gFnSc1	Klára
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyzná	vyznat	k5eAaPmIp3nS	vyznat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
Vilím	Vilím	k1gMnSc1	Vilím
pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
,	,	kIx,	,
co	co	k8xS	co
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
cítila	cítit	k5eAaImAgFnS	cítit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
přichází	přicházet	k5eAaImIp3nS	přicházet
i	i	k9	i
hrobník	hrobník	k1gMnSc1	hrobník
<g/>
.	.	kIx.	.
</s>
<s>
Klára	Klára	k1gFnSc1	Klára
se	se	k3xPyFc4	se
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
a	a	k8xC	a
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
otevře	otevřít	k5eAaPmIp3nS	otevřít
rakev	rakev	k1gFnSc4	rakev
a	a	k8xC	a
i	i	k9	i
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
lety	léto	k1gNnPc7	léto
zabil	zabít	k5eAaPmAgMnS	zabít
Vilímova	Vilímův	k2eAgMnSc4d1	Vilímův
otce	otec	k1gMnSc4	otec
Věnceslava	Věnceslava	k1gFnSc1	Věnceslava
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
neustále	neustále	k6eAd1	neustále
ubližoval	ubližovat	k5eAaImAgMnS	ubližovat
<g/>
.	.	kIx.	.
</s>
<s>
Prosí	prosit	k5eAaImIp3nS	prosit
mrtvého	mrtvý	k1gMnSc4	mrtvý
za	za	k7c4	za
odpuštění	odpuštění	k1gNnSc4	odpuštění
a	a	k8xC	a
proklíná	proklínat	k5eAaImIp3nS	proklínat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
zbavit	zbavit	k5eAaPmF	zbavit
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
šperků	šperk	k1gInPc2	šperk
poraní	poranit	k5eAaPmIp3nS	poranit
Vilíma	Vilím	k1gMnSc4	Vilím
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
bolestí	bolest	k1gFnSc7	bolest
probouzí	probouzet	k5eAaImIp3nP	probouzet
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
končí	končit	k5eAaImIp3nS	končit
výkřikem	výkřik	k1gInSc7	výkřik
Kláry	Klára	k1gFnSc2	Klára
a	a	k8xC	a
prchajícím	prchající	k2eAgMnSc7d1	prchající
hrobníkem	hrobník	k1gMnSc7	hrobník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
po	po	k7c6	po
hrobníkovi	hrobník	k1gMnSc6	hrobník
pátrá	pátrat	k5eAaImIp3nS	pátrat
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
viděn	vidět	k5eAaImNgMnS	vidět
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Zešílel	zešílet	k5eAaPmAgMnS	zešílet
<g/>
.	.	kIx.	.
</s>
<s>
Prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
se	se	k3xPyFc4	se
za	za	k7c4	za
tygra	tygr	k1gMnSc4	tygr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zabije	zabít	k5eAaPmIp3nS	zabít
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
pokusí	pokusit	k5eAaPmIp3nS	pokusit
zadržet	zadržet	k5eAaPmF	zadržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vilímovo	Vilímův	k2eAgNnSc1d1	Vilímův
chování	chování	k1gNnSc1	chování
ke	k	k7c3	k
Kláře	Klára	k1gFnSc3	Klára
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
ji	on	k3xPp3gFnSc4	on
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
ji	on	k3xPp3gFnSc4	on
přísahy	přísaha	k1gFnSc2	přísaha
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dala	dát	k5eAaPmAgFnS	dát
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Klára	Klára	k1gFnSc1	Klára
se	se	k3xPyFc4	se
podvolí	podvolit	k5eAaPmIp3nS	podvolit
svým	svůj	k3xOyFgInPc3	svůj
citům	cit	k1gInPc3	cit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
objetí	objetí	k1gNnSc6	objetí
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
přistiženi	přistihnout	k5eAaPmNgMnP	přistihnout
hrobníkem	hrobník	k1gMnSc7	hrobník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hodí	hodit	k5eAaPmIp3nS	hodit
po	po	k7c6	po
Vilímovi	Vilím	k1gMnSc3	Vilím
kyjem	kyj	k1gInSc7	kyj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mine	minout	k5eAaImIp3nS	minout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
šíleným	šílený	k2eAgInSc7d1	šílený
smíchem	smích	k1gInSc7	smích
hrobník	hrobník	k1gMnSc1	hrobník
utíká	utíkat	k5eAaImIp3nS	utíkat
a	a	k8xC	a
křičí	křičet	k5eAaImIp3nS	křičet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
už	už	k6eAd1	už
pomstil	pomstit	k5eAaImAgMnS	pomstit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dáli	dál	k1gFnSc6	dál
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
hořící	hořící	k2eAgFnSc1d1	hořící
vesnice	vesnice	k1gFnSc1	vesnice
–	–	k?	–
hrobníkova	hrobníkův	k2eAgFnSc1d1	hrobníkova
pomsta	pomsta	k1gFnSc1	pomsta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
další	další	k2eAgFnSc6d1	další
části	část	k1gFnSc6	část
příběhu	příběh	k1gInSc2	příběh
jsou	být	k5eAaImIp3nP	být
prozkoumány	prozkoumat	k5eAaPmNgInP	prozkoumat
hrobníkovy	hrobníkův	k2eAgInPc1d1	hrobníkův
osobní	osobní	k2eAgInPc1d1	osobní
zápisky	zápisek	k1gInPc1	zápisek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
jeho	jeho	k3xOp3gInSc1	jeho
dřívější	dřívější	k2eAgInSc1d1	dřívější
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studentských	studentský	k2eAgNnPc2d1	studentské
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
povahu	povaha	k1gFnSc4	povaha
přezdívku	přezdívka	k1gFnSc4	přezdívka
Smutný	smutný	k2eAgInSc1d1	smutný
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
pilný	pilný	k2eAgMnSc1d1	pilný
student	student	k1gMnSc1	student
a	a	k8xC	a
zajímal	zajímat	k5eAaImAgMnS	zajímat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
mladé	mladý	k2eAgFnSc2d1	mladá
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
Luisy	Luisa	k1gFnSc2	Luisa
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
ona	onen	k3xDgFnSc1	onen
mu	on	k3xPp3gMnSc3	on
projevila	projevit	k5eAaPmAgFnS	projevit
náklonnost	náklonnost	k1gFnSc4	náklonnost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
nepřál	přát	k5eNaImAgMnS	přát
Luisin	Luisin	k2eAgMnSc1d1	Luisin
bratr	bratr	k1gMnSc1	bratr
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
několikrát	několikrát	k6eAd1	několikrát
zesměšnil	zesměšnit	k5eAaPmAgMnS	zesměšnit
Smutného	Smutný	k1gMnSc4	Smutný
<g/>
.	.	kIx.	.
</s>
<s>
Nevraživost	nevraživost	k1gFnSc1	nevraživost
vyvrcholí	vyvrcholit	k5eAaPmIp3nS	vyvrcholit
v	v	k7c4	v
den	den	k1gInSc4	den
Věnceslavovy	Věnceslavův	k2eAgFnSc2d1	Věnceslavův
svatby	svatba	k1gFnSc2	svatba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
pomsty	pomsta	k1gFnSc2	pomsta
a	a	k8xC	a
vzteku	vztek	k1gInSc2	vztek
Smutný	Smutný	k1gMnSc1	Smutný
bodne	bodnout	k5eAaPmIp3nS	bodnout
hraběte	hrabě	k1gMnSc4	hrabě
dýkou	dýka	k1gFnSc7	dýka
do	do	k7c2	do
prsou	prsa	k1gNnPc2	prsa
<g/>
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
se	se	k3xPyFc4	se
z	z	k7c2	z
vážného	vážný	k2eAgNnSc2d1	vážné
zranění	zranění	k1gNnSc2	zranění
uzdravuje	uzdravovat	k5eAaImIp3nS	uzdravovat
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
novou	nový	k2eAgFnSc7d1	nová
ženou	žena	k1gFnSc7	žena
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
se	se	k3xPyFc4	se
navrátí	navrátit	k5eAaPmIp3nS	navrátit
až	až	k9	až
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
syna	syn	k1gMnSc2	syn
Vilíma	Vilím	k1gMnSc2	Vilím
<g/>
.	.	kIx.	.
</s>
<s>
Smutný	Smutný	k1gMnSc1	Smutný
utekl	utéct	k5eAaPmAgMnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Luisa	Luisa	k1gFnSc1	Luisa
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přijme	přijmout	k5eAaPmIp3nS	přijmout
nové	nový	k2eAgNnSc4d1	nové
jméno	jméno	k1gNnSc4	jméno
–	–	k?	–
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Smutný	Smutný	k1gMnSc1	Smutný
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
,	,	kIx,	,
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
Alžbětu	Alžběta	k1gFnSc4	Alžběta
(	(	kIx(	(
<g/>
Luisu	Luisa	k1gFnSc4	Luisa
<g/>
)	)	kIx)	)
a	a	k8xC	a
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
jí	on	k3xPp3gFnSc7	on
společný	společný	k2eAgInSc1d1	společný
odchod	odchod	k1gInSc1	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
city	cit	k1gInPc1	cit
už	už	k6eAd1	už
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
silné	silný	k2eAgFnPc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Nalezla	naleznout	k5eAaPmAgFnS	naleznout
své	svůj	k3xOyFgNnSc4	svůj
poslání	poslání	k1gNnSc4	poslání
v	v	k7c6	v
odpírání	odpírání	k1gNnSc6	odpírání
světského	světský	k2eAgInSc2d1	světský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Smutný	Smutný	k1gMnSc1	Smutný
nachází	nacházet	k5eAaImIp3nS	nacházet
útěchu	útěcha	k1gFnSc4	útěcha
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
lásku	láska	k1gFnSc4	láska
v	v	k7c6	v
dceři	dcera	k1gFnSc6	dcera
nedávno	nedávno	k6eAd1	nedávno
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
hrobníka	hrobník	k1gMnSc4	hrobník
<g/>
,	,	kIx,	,
Marince	Marinka	k1gFnSc3	Marinka
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stává	stávat	k5eAaImIp3nS	stávat
místním	místní	k2eAgMnSc7d1	místní
hrobníkem	hrobník	k1gMnSc7	hrobník
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
o	o	k7c4	o
Marinku	Marinka	k1gFnSc4	Marinka
začíná	začínat	k5eAaImIp3nS	začínat
zajímat	zajímat	k5eAaImF	zajímat
Věnceslav	Věnceslava	k1gFnPc2	Věnceslava
a	a	k8xC	a
tuší	tušit	k5eAaImIp3nS	tušit
jeho	jeho	k3xOp3gInPc4	jeho
záměry	záměr	k1gInPc4	záměr
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nezasáhne	zasáhnout	k5eNaPmIp3nS	zasáhnout
a	a	k8xC	a
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zareaguje	zareagovat	k5eAaPmIp3nS	zareagovat
Marinka	Marinka	k1gFnSc1	Marinka
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
se	se	k3xPyFc4	se
upíná	upínat	k5eAaImIp3nS	upínat
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
oddanosti	oddanost	k1gFnSc3	oddanost
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
Marinka	Marinka	k1gFnSc1	Marinka
hraběti	hrabě	k1gMnSc6	hrabě
podvolí	podvolit	k5eAaPmIp3nS	podvolit
<g/>
.	.	kIx.	.
</s>
<s>
Zdrcený	zdrcený	k2eAgMnSc1d1	zdrcený
hrobník	hrobník	k1gMnSc1	hrobník
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
Věnceslava	Věnceslava	k1gFnSc1	Věnceslava
zabije	zabít	k5eAaPmIp3nS	zabít
na	na	k7c6	na
hrázi	hráz	k1gFnSc6	hráz
rybníka	rybník	k1gInSc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Nechtěným	chtěný	k2eNgMnSc7d1	nechtěný
svědkem	svědek	k1gMnSc7	svědek
tohoto	tento	k3xDgInSc2	tento
činu	čin	k1gInSc2	čin
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
mlčí	mlčet	k5eAaImIp3nS	mlčet
<g/>
.	.	kIx.	.
</s>
<s>
Hrobník	hrobník	k1gMnSc1	hrobník
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
–	–	k?	–
svou	svůj	k3xOyFgFnSc7	svůj
bývalou	bývalý	k2eAgFnSc7d1	bývalá
láskou	láska	k1gFnSc7	láska
–	–	k?	–
už	už	k9	už
ale	ale	k9	ale
za	za	k7c2	za
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
nikdy	nikdy	k6eAd1	nikdy
nespatří	spatřit	k5eNaPmIp3nS	spatřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marince	Marinka	k1gFnSc3	Marinka
se	se	k3xPyFc4	se
narodí	narodit	k5eAaPmIp3nS	narodit
Věnceslavova	Věnceslavův	k2eAgFnSc1d1	Věnceslavův
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Marinka	Marinka	k1gFnSc1	Marinka
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Kláru	Klára	k1gFnSc4	Klára
přijme	přijmout	k5eAaPmIp3nS	přijmout
hrobník	hrobník	k1gMnSc1	hrobník
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
a	a	k8xC	a
vychová	vychovat	k5eAaPmIp3nS	vychovat
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
pádem	pád	k1gInSc7	pád
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
byla	být	k5eAaImAgFnS	být
bída	bída	k1gFnSc1	bída
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
zajistit	zajistit	k5eAaPmF	zajistit
Kláře	Klára	k1gFnSc3	Klára
budoucnost	budoucnost	k1gFnSc4	budoucnost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
okrádá	okrádat	k5eAaImIp3nS	okrádat
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
poklady	poklad	k1gInPc4	poklad
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
schovává	schovávat	k5eAaImIp3nS	schovávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
časová	časový	k2eAgFnSc1d1	časová
linie	linie	k1gFnSc1	linie
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Klára	Klára	k1gFnSc1	Klára
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Loučí	loučit	k5eAaImIp3nS	loučit
se	se	k3xPyFc4	se
s	s	k7c7	s
Vilímem	Vilím	k1gMnSc7	Vilím
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
už	už	k9	už
vědí	vědět	k5eAaImIp3nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Zničený	zničený	k2eAgMnSc1d1	zničený
Vilím	Vilím	k1gMnSc1	Vilím
natrvalo	natrvalo	k6eAd1	natrvalo
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
obřadu	obřad	k1gInSc2	obřad
Klářina	Klářin	k2eAgNnSc2d1	Klářino
přijetí	přijetí	k1gNnSc2	přijetí
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
je	být	k5eAaImIp3nS	být
přineseno	přinesen	k2eAgNnSc1d1	přineseno
hrobníkovo	hrobníkův	k2eAgNnSc1d1	hrobníkův
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
v	v	k7c6	v
rybníce	rybník	k1gInSc6	rybník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
fázi	fáze	k1gFnSc6	fáze
obřadu	obřad	k1gInSc2	obřad
se	se	k3xPyFc4	se
Klára	Klára	k1gFnSc1	Klára
pokládá	pokládat	k5eAaImIp3nS	pokládat
obličejem	obličej	k1gInSc7	obličej
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
nadobro	nadobro	k6eAd1	nadobro
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
klášterní	klášterní	k2eAgFnSc7d1	klášterní
sestrou	sestra	k1gFnSc7	sestra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
Klára	Klára	k1gFnSc1	Klára
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Percepce	percepce	k1gFnSc1	percepce
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Hrobník	hrobník	k1gMnSc1	hrobník
patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgNnPc4d3	nejúspěšnější
novelistická	novelistický	k2eAgNnPc4d1	novelistický
díla	dílo	k1gNnPc4	dílo
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Sabina	Sabina	k1gFnSc1	Sabina
skrz	skrz	k7c4	skrz
něj	on	k3xPp3gMnSc4	on
přivedl	přivést	k5eAaPmAgMnS	přivést
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
prvky	prvek	k1gInPc1	prvek
frenetické	frenetický	k2eAgFnSc2d1	frenetická
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
i	i	k8xC	i
některé	některý	k3yIgInPc4	některý
rysy	rys	k1gInPc4	rys
gotického	gotický	k2eAgInSc2d1	gotický
románu	román	k1gInSc2	román
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aplikací	aplikace	k1gFnSc7	aplikace
tohoto	tento	k3xDgInSc2	tento
cizího	cizí	k2eAgInSc2d1	cizí
žánru	žánr	k1gInSc2	žánr
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
prostředí	prostředí	k1gNnSc4	prostředí
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nezvyklá	zvyklý	k2eNgFnSc1d1	nezvyklá
podoba	podoba	k1gFnSc1	podoba
frenetismu	frenetismus	k1gInSc2	frenetismus
(	(	kIx(	(
<g/>
zapříčiněná	zapříčiněný	k2eAgNnPc1d1	zapříčiněné
především	především	k6eAd1	především
rozdílností	rozdílnost	k1gFnSc7	rozdílnost
charakteru	charakter	k1gInSc2	charakter
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
francouzského	francouzský	k2eAgNnSc2d1	francouzské
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
anglického	anglický	k2eAgInSc2d1	anglický
<g/>
)	)	kIx)	)
přírodního	přírodní	k2eAgInSc2d1	přírodní
rázu	ráz	k1gInSc2	ráz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k6eAd1	mimo
frenetismu	frenetismus	k1gInSc2	frenetismus
je	být	k5eAaImIp3nS	být
Hrobník	hrobník	k1gMnSc1	hrobník
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
romantismem	romantismus	k1gInSc7	romantismus
–	–	k?	–
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
,	,	kIx,	,
tajemný	tajemný	k2eAgInSc1d1	tajemný
rybník	rybník	k1gInSc1	rybník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
často	často	k6eAd1	často
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
za	za	k7c4	za
bouře	bouř	k1gFnPc4	bouř
<g/>
)	)	kIx)	)
i	i	k8xC	i
romantický	romantický	k2eAgInSc1d1	romantický
typ	typ	k1gInSc1	typ
hrdiny	hrdina	k1gMnSc2	hrdina
(	(	kIx(	(
<g/>
hrobník	hrobník	k1gMnSc1	hrobník
i	i	k8xC	i
Vilím	Vilím	k1gMnSc1	Vilím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
novely	novela	k1gFnSc2	novela
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
názory	názor	k1gInPc1	názor
obviňující	obviňující	k2eAgFnSc4d1	obviňující
Sabinu	Sabina	k1gFnSc4	Sabina
z	z	k7c2	z
krádeže	krádež	k1gFnSc2	krádež
rukopisu	rukopis	k1gInSc2	rukopis
Hrobníka	hrobník	k1gMnSc2	hrobník
z	z	k7c2	z
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
K.	K.	kA	K.
H.	H.	kA	H.
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
.	.	kIx.	.
</s>
<s>
Sabina	Sabina	k1gMnSc1	Sabina
měl	mít	k5eAaImAgMnS	mít
jako	jako	k8xS	jako
vydavatel	vydavatel	k1gMnSc1	vydavatel
Máchových	Máchových	k2eAgInPc2d1	Máchových
spisů	spis	k1gInPc2	spis
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
dosud	dosud	k6eAd1	dosud
nepublikovaným	publikovaný	k2eNgNnPc3d1	nepublikované
dílům	dílo	k1gNnPc3	dílo
tohoto	tento	k3xDgMnSc2	tento
básníka	básník	k1gMnSc2	básník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podpora	podpora	k1gFnSc1	podpora
těchto	tento	k3xDgInPc2	tento
kritických	kritický	k2eAgInPc2d1	kritický
názorů	názor	k1gInPc2	názor
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
podobnosti	podobnost	k1gFnSc6	podobnost
Hrobníka	hrobník	k1gMnSc2	hrobník
a	a	k8xC	a
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Nápadná	nápadný	k2eAgFnSc1d1	nápadná
je	být	k5eAaImIp3nS	být
především	především	k9	především
podoba	podoba	k1gFnSc1	podoba
s	s	k7c7	s
Máchovými	Máchův	k2eAgMnPc7d1	Máchův
Cikány	cikán	k1gMnPc7	cikán
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
díla	dílo	k1gNnPc4	dílo
spojují	spojovat	k5eAaImIp3nP	spojovat
prvky	prvek	k1gInPc1	prvek
dávné	dávný	k2eAgFnSc2d1	dávná
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
msty	msta	k1gFnSc2	msta
a	a	k8xC	a
trestu	trest	k1gInSc2	trest
i	i	k8xC	i
zasazení	zasazení	k1gNnSc2	zasazení
příběhu	příběh	k1gInSc2	příběh
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
Kokořínska	Kokořínsko	k1gNnSc2	Kokořínsko
<g/>
.	.	kIx.	.
</s>
<s>
Podobu	podoba	k1gFnSc4	podoba
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
také	také	k9	také
v	v	k7c6	v
postavě	postava	k1gFnSc6	postava
hrobníka	hrobník	k1gMnSc2	hrobník
a	a	k8xC	a
kata	kat	k1gMnSc2	kat
(	(	kIx(	(
<g/>
Máchův	Máchův	k2eAgInSc1d1	Máchův
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
historický	historický	k2eAgInSc1d1	historický
cyklus	cyklus	k1gInSc1	cyklus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KREJČÍ	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Frenetický	frenetický	k2eAgInSc1d1	frenetický
žánr	žánr	k1gInSc1	žánr
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
SABINA	Sabina	k1gMnSc1	Sabina
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Hrobník	hrobník	k1gMnSc1	hrobník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
HRBATA	HRBATA	kA	HRBATA
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
vesnice	vesnice	k1gFnSc1	vesnice
a	a	k8xC	a
"	"	kIx"	"
<g/>
zuřivý	zuřivý	k2eAgMnSc1d1	zuřivý
<g/>
"	"	kIx"	"
romantismus	romantismus	k1gInSc1	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Sabinova	Sabinův	k2eAgFnSc1d1	Sabinova
novela	novela	k1gFnSc1	novela
Hrobník	hrobník	k1gMnSc1	hrobník
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
367	[number]	k4	367
<g/>
–	–	k?	–
<g/>
375	[number]	k4	375
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
