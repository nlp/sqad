<s>
Premisa	premisa	k1gFnSc1	premisa
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
praemittere	praemitter	k1gInSc5	praemitter
napřed	napřed	k6eAd1	napřed
posílat	posílat	k5eAaImF	posílat
<g/>
,	,	kIx,	,
předesílat	předesílat	k5eAaImF	předesílat
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
předpoklad	předpoklad	k1gInSc4	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Premisa	premisa	k1gFnSc1	premisa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
a	a	k8xC	a
filozofii	filozofie	k1gFnSc6	filozofie
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
logického	logický	k2eAgInSc2d1	logický
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Logickým	logický	k2eAgInSc7d1	logický
postupem	postup	k1gInSc7	postup
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
souhrn	souhrn	k1gInSc1	souhrn
premis	premisa	k1gFnPc2	premisa
implikuje	implikovat	k5eAaImIp3nS	implikovat
určitý	určitý	k2eAgInSc4d1	určitý
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
soudu	soud	k1gInSc2	soud
však	však	k9	však
není	být	k5eNaImIp3nS	být
zkoumání	zkoumání	k1gNnSc4	zkoumání
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
premisy	premisa	k1gFnPc1	premisa
jsou	být	k5eAaImIp3nP	být
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Pravdivostní	pravdivostní	k2eAgFnSc1d1	pravdivostní
hodnota	hodnota	k1gFnSc1	hodnota
závěru	závěr	k1gInSc2	závěr
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
závislá	závislý	k2eAgFnSc1d1	závislá
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
nutném	nutný	k2eAgNnSc6d1	nutné
vyústění	vyústění	k1gNnSc6	vyústění
z	z	k7c2	z
daných	daný	k2eAgFnPc2d1	daná
premis	premisa	k1gFnPc2	premisa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
pravdivostní	pravdivostní	k2eAgFnSc7d1	pravdivostní
hodnotou	hodnota	k1gFnSc7	hodnota
těchto	tento	k3xDgFnPc2	tento
premis	premisa	k1gFnPc2	premisa
<g/>
.	.	kIx.	.
</s>
<s>
Axiom	axiom	k1gInSc1	axiom
Logický	logický	k2eAgInSc1d1	logický
soud	soud	k1gInSc1	soud
Implikace	implikace	k1gFnSc2	implikace
Pravdivostní	pravdivostní	k2eAgFnSc1d1	pravdivostní
hodnota	hodnota	k1gFnSc1	hodnota
Logika	logika	k1gFnSc1	logika
</s>
