<p>
<s>
Měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
Pivovar	pivovar	k1gInSc1	pivovar
v	v	k7c6	v
Poličce	Polička	k1gFnSc6	Polička
je	být	k5eAaImIp3nS	být
měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
ve	v	k7c6	v
stejnojmenném	stejnojmenný	k2eAgNnSc6d1	stejnojmenné
městě	město	k1gNnSc6	město
Polička	Polička	k1gFnSc1	Polička
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
pivovaru	pivovar	k1gInSc2	pivovar
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
znárodněna	znárodněn	k2eAgFnSc1d1	znárodněna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
po	po	k7c6	po
restituci	restituce	k1gFnSc6	restituce
a	a	k8xC	a
navrácení	navrácení	k1gNnSc4	navrácení
potomkům	potomek	k1gMnPc3	potomek
původních	původní	k2eAgNnPc2d1	původní
majitelů	majitel	k1gMnPc2	majitel
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
Pivovar	pivovar	k1gInSc1	pivovar
Polička	Polička	k1gFnSc1	Polička
Měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
v	v	k7c6	v
Poličce	Polička	k1gFnSc6	Polička
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
</s>
</p>
<p>
<s>
==	==	k?	==
Produkty	produkt	k1gInPc1	produkt
pivovaru	pivovar	k1gInSc2	pivovar
==	==	k?	==
</s>
</p>
<p>
<s>
světlé	světlý	k2eAgFnPc1d1	světlá
výčepní	výčepní	k1gFnPc1	výčepní
10	[number]	k4	10
<g/>
°	°	k?	°
–	–	k?	–
Hradební	hradební	k2eAgFnSc1d1	hradební
</s>
</p>
<p>
<s>
tmavé	tmavý	k2eAgFnPc1d1	tmavá
výčepní	výčepní	k1gFnPc1	výčepní
10	[number]	k4	10
<g/>
°	°	k?	°
–	–	k?	–
Hradební	hradební	k2eAgFnSc1d1	hradební
</s>
</p>
<p>
<s>
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
11	[number]	k4	11
<g/>
°	°	k?	°
–	–	k?	–
Otakar	Otakar	k1gMnSc1	Otakar
</s>
</p>
<p>
<s>
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
12	[number]	k4	12
<g/>
°	°	k?	°
–	–	k?	–
Záviš	Záviš	k1gMnSc1	Záviš
</s>
</p>
<p>
<s>
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
11	[number]	k4	11
<g/>
°	°	k?	°
–	–	k?	–
Otakar	Otakar	k1gMnSc1	Otakar
–	–	k?	–
kvasnicový	kvasnicový	k2eAgMnSc1d1	kvasnicový
</s>
</p>
<p>
<s>
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
12	[number]	k4	12
<g/>
°	°	k?	°
–	–	k?	–
Záviš	Záviš	k1gMnSc1	Záviš
–	–	k?	–
kvasnicový	kvasnicový	k2eAgMnSc1d1	kvasnicový
</s>
</p>
<p>
<s>
světlé	světlý	k2eAgInPc1d1	světlý
speciální	speciální	k2eAgInPc1d1	speciální
13	[number]	k4	13
<g/>
°	°	k?	°
–	–	k?	–
František	František	k1gMnSc1	František
Bittner	Bittner	k1gMnSc1	Bittner
</s>
</p>
<p>
<s>
tmavé	tmavý	k2eAgInPc1d1	tmavý
speciální	speciální	k2eAgInPc1d1	speciální
13	[number]	k4	13
<g/>
°	°	k?	°
–	–	k?	–
Eliška	Eliška	k1gFnSc1	Eliška
/	/	kIx~	/
<g/>
vaří	vařit	k5eAaImIp3nS	vařit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
<g/>
x	x	k?	x
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
pivní	pivní	k2eAgInPc1d1	pivní
tácky	tácek	k1gInPc1	tácek
Měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
pivovaru	pivovar	k1gInSc2	pivovar
Polička	Polička	k1gFnSc1	Polička
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
pivovaru	pivovar	k1gInSc2	pivovar
Polička	Polička	k1gFnSc1	Polička
</s>
</p>
