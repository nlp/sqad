<s>
James	James	k1gMnSc1	James
Marshall	Marshall	k1gMnSc1	Marshall
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
rozený	rozený	k2eAgInSc4d1	rozený
jako	jako	k8xS	jako
Johnny	Johnna	k1gFnSc2	Johnna
Allen	Allen	k1gMnSc1	Allen
Hendrix	Hendrix	k1gInSc1	Hendrix
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1942	[number]	k4	1942
Seattle	Seattle	k1gFnSc2	Seattle
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1970	[number]	k4	1970
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
kytaristů	kytarista	k1gMnPc2	kytarista
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
počátečním	počáteční	k2eAgInSc6d1	počáteční
úspěchu	úspěch	k1gInSc6	úspěch
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
světového	světový	k2eAgInSc2d1	světový
věhlasu	věhlas	k1gInSc2	věhlas
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
vystoupení	vystoupení	k1gNnSc3	vystoupení
na	na	k7c6	na
popovém	popový	k2eAgInSc6d1	popový
festivalu	festival	k1gInSc6	festival
v	v	k7c4	v
Monterey	Monterea	k1gFnPc4	Monterea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
byl	být	k5eAaImAgInS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hvězdou	hvězda	k1gFnSc7	hvězda
festivalu	festival	k1gInSc2	festival
Woodstock	Woodstock	k1gInSc1	Woodstock
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pouhých	pouhý	k2eAgNnPc2d1	pouhé
27	[number]	k4	27
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1942	[number]	k4	1942
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Seattlu	Seattl	k1gInSc6	Seattl
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Al	ala	k1gFnPc2	ala
Hendrix	Hendrix	k1gInSc1	Hendrix
<g/>
,	,	kIx,	,
tč	tč	kA	tč
<g/>
.	.	kIx.	.
příslušník	příslušník	k1gMnSc1	příslušník
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Oklahomě	Oklahomě	k1gFnSc6	Oklahomě
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
Hendrixově	Hendrixově	k1gFnSc6	Hendrixově
narození	narození	k1gNnSc2	narození
byl	být	k5eAaImAgMnS	být
převelen	převelet	k5eAaPmNgMnS	převelet
do	do	k7c2	do
jižního	jižní	k2eAgInSc2d1	jižní
Pacifiku	Pacifik	k1gInSc2	Pacifik
a	a	k8xC	a
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
až	až	k6eAd1	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Lucille	Lucille	k1gFnSc1	Lucille
Hendrixová	Hendrixový	k2eAgFnSc1d1	Hendrixová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Jeterová	Jeterová	k1gFnSc1	Jeterová
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gNnSc2	on
narození	narození	k1gNnSc2	narození
pouhých	pouhý	k2eAgFnPc2d1	pouhá
17	[number]	k4	17
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hendrixovi	Hendrixův	k2eAgMnPc1d1	Hendrixův
předci	předek	k1gMnPc1	předek
byli	být	k5eAaImAgMnP	být
černošští	černošský	k2eAgMnPc1d1	černošský
otroci	otrok	k1gMnPc1	otrok
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
bílí	bílý	k2eAgMnPc1d1	bílý
majitelé	majitel	k1gMnPc1	majitel
i	i	k8xC	i
indiáni	indián	k1gMnPc1	indián
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Čerokézů	Čerokéz	k1gInPc2	Čerokéz
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jejich	jejich	k3xOp3gNnSc2	jejich
seznámení	seznámení	k1gNnSc2	seznámení
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
o	o	k7c4	o
malého	malý	k1gMnSc4	malý
Hendrixe	Hendrixe	k1gFnSc2	Hendrixe
střídavě	střídavě	k6eAd1	střídavě
staraly	starat	k5eAaImAgInP	starat
jeho	jeho	k3xOp3gFnSc4	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
tety	teta	k1gFnPc1	teta
a	a	k8xC	a
rodinné	rodinný	k2eAgNnSc1d1	rodinné
známé	známý	k2eAgInPc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
nechal	nechat	k5eAaPmAgMnS	nechat
Hendrixův	Hendrixův	k2eAgMnSc1d1	Hendrixův
otec	otec	k1gMnSc1	otec
změnit	změnit	k5eAaPmF	změnit
synovo	synův	k2eAgNnSc4d1	synovo
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
James	James	k1gInSc4	James
Marshall	Marshalla	k1gFnPc2	Marshalla
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
Jimiho	Jimi	k1gMnSc2	Jimi
rodiče	rodič	k1gMnSc2	rodič
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
stačili	stačit	k5eAaBmAgMnP	stačit
spolu	spolu	k6eAd1	spolu
mít	mít	k5eAaImF	mít
šest	šest	k4xCc4	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kromě	kromě	k7c2	kromě
dvou	dva	k4xCgMnPc2	dva
nejstarších	starý	k2eAgMnPc2d3	nejstarší
bratrů	bratr	k1gMnPc2	bratr
-	-	kIx~	-
Jimiho	Jimi	k1gMnSc2	Jimi
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
mladšího	mladý	k2eAgMnSc2d2	mladší
Leona	Leo	k1gMnSc2	Leo
-	-	kIx~	-
umístili	umístit	k5eAaPmAgMnP	umístit
Al	ala	k1gFnPc2	ala
a	a	k8xC	a
Lucille	Lucille	k1gFnSc2	Lucille
Hendrixovi	Hendrixa	k1gMnSc3	Hendrixa
všechny	všechen	k3xTgFnPc4	všechen
zbylé	zbylý	k2eAgFnPc4d1	zbylá
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
sirotčinců	sirotčinec	k1gInPc2	sirotčinec
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
finančními	finanční	k2eAgInPc7d1	finanční
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
dozorem	dozor	k1gInSc7	dozor
sociálních	sociální	k2eAgMnPc2d1	sociální
pracovníků	pracovník	k1gMnPc2	pracovník
a	a	k8xC	a
mnohokrát	mnohokrát	k6eAd1	mnohokrát
hrozilo	hrozit	k5eAaImAgNnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
oba	dva	k4xCgMnPc1	dva
sourozenci	sourozenec	k1gMnPc1	sourozenec
umístěni	umístit	k5eAaPmNgMnP	umístit
do	do	k7c2	do
dětského	dětský	k2eAgInSc2d1	dětský
domova	domov	k1gInSc2	domov
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Leona	Leo	k1gMnSc4	Leo
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
opravdu	opravdu	k6eAd1	opravdu
stalo	stát	k5eAaPmAgNnS	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dočasné	dočasný	k2eAgNnSc1d1	dočasné
stěhování	stěhování	k1gNnSc1	stěhování
buď	buď	k8xC	buď
celé	celý	k2eAgFnPc1d1	celá
rodiny	rodina	k1gFnPc1	rodina
anebo	anebo	k8xC	anebo
jen	jen	k9	jen
Jimiho	Jimi	k1gMnSc4	Jimi
a	a	k8xC	a
Leona	Leona	k1gFnSc1	Leona
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
příbuzným	příbuzná	k1gFnPc3	příbuzná
či	či	k8xC	či
rodinným	rodinný	k2eAgFnPc3d1	rodinná
známým	známá	k1gFnPc3	známá
nebylo	být	k5eNaImAgNnS	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Hendrixovi	Hendrixův	k2eAgMnPc1d1	Hendrixův
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
často	často	k6eAd1	často
hádali	hádat	k5eAaImAgMnP	hádat
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
nezřídkakdy	nezřídkakda	k1gFnSc2	nezřídkakda
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
čas	čas	k1gInSc4	čas
odcházela	odcházet	k5eAaImAgFnS	odcházet
po	po	k7c6	po
hádce	hádka	k1gFnSc6	hádka
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
za	za	k7c4	za
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
pohádala	pohádat	k5eAaPmAgFnS	pohádat
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
začalo	začít	k5eAaPmAgNnS	začít
nanovo	nanovo	k6eAd1	nanovo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
hádce	hádka	k1gFnSc6	hádka
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
domů	dům	k1gInPc2	dům
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nevrátila	vrátit	k5eNaPmAgFnS	vrátit
a	a	k8xC	a
Hendrix	Hendrix	k1gInSc1	Hendrix
tak	tak	k6eAd1	tak
začal	začít	k5eAaPmAgInS	začít
žít	žít	k5eAaImF	žít
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
často	často	k6eAd1	často
střídal	střídat	k5eAaImAgMnS	střídat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Jimi	on	k3xPp3gInPc7	on
prošel	projít	k5eAaPmAgInS	projít
postupně	postupně	k6eAd1	postupně
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
chudinských	chudinský	k2eAgFnPc6d1	chudinská
černošských	černošský	k2eAgFnPc6d1	černošská
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Hendrix	Hendrix	k1gInSc1	Hendrix
poprvé	poprvé	k6eAd1	poprvé
přičichnul	přičichnout	k5eAaPmAgInS	přičichnout
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
-	-	kIx~	-
při	při	k7c6	při
toulkách	toulka	k1gFnPc6	toulka
ulicemi	ulice	k1gFnPc7	ulice
často	často	k6eAd1	často
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
domech	dům	k1gInPc6	dům
kamarádů	kamarád	k1gMnPc2	kamarád
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
různé	různý	k2eAgInPc4d1	různý
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
také	také	k6eAd1	také
rád	rád	k6eAd1	rád
napodoboval	napodobovat	k5eAaImAgMnS	napodobovat
pohyby	pohyb	k1gInPc4	pohyb
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
zahlédl	zahlédnout	k5eAaPmAgInS	zahlédnout
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
-	-	kIx~	-
Elvise	Elvis	k1gMnSc4	Elvis
Presleyho	Presley	k1gMnSc4	Presley
<g/>
,	,	kIx,	,
Chucka	Chucka	k1gFnSc1	Chucka
Berryho	Berry	k1gMnSc2	Berry
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
zatoužil	zatoužit	k5eAaPmAgMnS	zatoužit
po	po	k7c6	po
elektrické	elektrický	k2eAgFnSc6d1	elektrická
kytaře	kytara	k1gFnSc6	kytara
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mu	on	k3xPp3gMnSc3	on
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
jeho	jeho	k3xOp3gNnSc2	jeho
otec	otec	k1gMnSc1	otec
koupil	koupit	k5eAaPmAgMnS	koupit
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
Myer	Myera	k1gFnPc2	Myera
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Music	Musice	k1gFnPc2	Musice
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bílý	bílý	k2eAgInSc4d1	bílý
pravoruký	pravoruký	k2eAgInSc4d1	pravoruký
model	model	k1gInSc4	model
značky	značka	k1gFnSc2	značka
Supro	Supro	k1gNnSc1	Supro
Ozark	Ozark	k1gInSc1	Ozark
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
popřeházel	popřeházet	k5eAaPmAgInS	popřeházet
struny	struna	k1gFnPc4	struna
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
jako	jako	k9	jako
levák	levák	k1gMnSc1	levák
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
hmatník	hmatník	k1gInSc1	hmatník
vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
musela	muset	k5eAaImAgFnS	muset
hraní	hraní	k1gNnSc4	hraní
dosti	dosti	k6eAd1	dosti
ztěžovat	ztěžovat	k5eAaImF	ztěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
levoruké	levoruký	k2eAgFnPc4d1	levoruká
kytary	kytara	k1gFnPc4	kytara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
pravorukým	pravoruký	k2eAgMnPc3d1	pravoruký
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
učil	učit	k5eAaImAgInS	učit
hrát	hrát	k5eAaImF	hrát
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
už	už	k6eAd1	už
zvyklý	zvyklý	k2eAgInSc1d1	zvyklý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
zemřela	zemřít	k5eAaPmAgFnS	zemřít
Hendrixova	Hendrixův	k2eAgFnSc1d1	Hendrixova
matka	matka	k1gFnSc1	matka
Lucille	Lucille	k1gFnSc2	Lucille
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
tehdy	tehdy	k6eAd1	tehdy
patnáctiletého	patnáctiletý	k2eAgMnSc4d1	patnáctiletý
hocha	hoch	k1gMnSc4	hoch
pochopitelně	pochopitelně	k6eAd1	pochopitelně
velmi	velmi	k6eAd1	velmi
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
zamyšlený	zamyšlený	k2eAgMnSc1d1	zamyšlený
a	a	k8xC	a
introvertní	introvertní	k2eAgMnSc1d1	introvertní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
najít	najít	k5eAaPmF	najít
si	se	k3xPyFc3	se
nějaké	nějaký	k3yIgMnPc4	nějaký
spoluhráče	spoluhráč	k1gMnPc4	spoluhráč
a	a	k8xC	a
začlenit	začlenit	k5eAaPmF	začlenit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místem	k6eAd1	místem
jeho	jeho	k3xOp3gNnSc2	jeho
prvního	první	k4xOgNnSc2	první
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
sklepení	sklepení	k1gNnSc1	sklepení
seattleské	seattleský	k2eAgFnSc2d1	seattleská
synagogy	synagoga	k1gFnSc2	synagoga
Temple	templ	k1gInSc5	templ
De	De	k?	De
Hirsch	Hirs	k1gFnPc6	Hirs
Sinai	Sina	k1gFnSc2	Sina
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
pojat	pojmout	k5eAaPmNgInS	pojmout
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
jako	jako	k9	jako
konkurz	konkurz	k1gInSc4	konkurz
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
kytaristy	kytarista	k1gMnSc2	kytarista
zatím	zatím	k6eAd1	zatím
bezejmenné	bezejmenný	k2eAgFnSc2d1	bezejmenná
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
půlce	půlka	k1gFnSc6	půlka
koncertu	koncert	k1gInSc2	koncert
byl	být	k5eAaImAgInS	být
Hendrix	Hendrix	k1gInSc1	Hendrix
poslán	poslat	k5eAaPmNgInS	poslat
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
ostatním	ostatní	k2eAgMnPc3d1	ostatní
členům	člen	k1gMnPc3	člen
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgInS	líbit
jeho	jeho	k3xOp3gInSc4	jeho
divoký	divoký	k2eAgInSc4d1	divoký
a	a	k8xC	a
nesvázaný	svázaný	k2eNgInSc4d1	nesvázaný
styl	styl	k1gInSc4	styl
hraní	hraní	k1gNnSc2	hraní
a	a	k8xC	a
obávali	obávat	k5eAaImAgMnP	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
takovýmto	takovýto	k3xDgInSc7	takovýto
stylem	styl	k1gInSc7	styl
nemůže	moct	k5eNaImIp3nS	moct
najít	najít	k5eAaPmF	najít
Hendrix	Hendrix	k1gInSc4	Hendrix
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
uplatnění	uplatnění	k1gNnSc2	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Hendrixovou	Hendrixový	k2eAgFnSc4d1	Hendrixová
první	první	k4xOgFnSc7	první
významnou	významný	k2eAgFnSc7d1	významná
štací	štace	k1gFnSc7	štace
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
až	až	k9	až
kapela	kapela	k1gFnSc1	kapela
The	The	k1gMnSc1	The
Velvetones	Velvetones	k1gMnSc1	Velvetones
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
revuální	revuální	k2eAgNnSc4d1	revuální
uskupení	uskupení	k1gNnSc4	uskupení
čítající	čítající	k2eAgNnSc4d1	čítající
kytaristy	kytarista	k1gMnSc2	kytarista
<g/>
,	,	kIx,	,
pianisty	pianista	k1gMnSc2	pianista
<g/>
,	,	kIx,	,
dechovou	dechový	k2eAgFnSc4d1	dechová
sekci	sekce	k1gFnSc4	sekce
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc4	bubeník
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
hrálo	hrát	k5eAaImAgNnS	hrát
směsici	směsice	k1gFnSc4	směsice
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
blues	blues	k1gNnSc2	blues
a	a	k8xC	a
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B.	B.	kA	B.
V	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
byl	být	k5eAaImAgInS	být
Hendrix	Hendrix	k1gInSc4	Hendrix
vyhozen	vyhozen	k2eAgInSc4d1	vyhozen
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgMnSc7	svůj
známým	známý	k1gMnSc7	známý
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
namluvit	namluvit	k5eAaPmF	namluvit
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
přistižen	přistižen	k2eAgMnSc1d1	přistižen
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
bílé	bílý	k2eAgFnSc2d1	bílá
pleti	pleť	k1gFnSc2	pleť
ve	v	k7c6	v
studovně	studovna	k1gFnSc6	studovna
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
ale	ale	k9	ale
byla	být	k5eAaImAgFnS	být
jiná	jiný	k2eAgFnSc1d1	jiná
-	-	kIx~	-
Hendrix	Hendrix	k1gInSc1	Hendrix
byl	být	k5eAaImAgInS	být
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednoduše	jednoduše	k6eAd1	jednoduše
flákal	flákat	k5eAaImAgMnS	flákat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhazovu	vyhazov	k1gInSc6	vyhazov
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
plně	plně	k6eAd1	plně
věnovat	věnovat	k5eAaPmF	věnovat
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pár	pár	k4xCyI	pár
měsících	měsíc	k1gInPc6	měsíc
si	se	k3xPyFc3	se
našel	najít	k5eAaPmAgMnS	najít
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Rocking	Rocking	k1gInSc1	Rocking
Kings	Kingsa	k1gFnPc2	Kingsa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
The	The	k1gMnSc1	The
Velvetones	Velvetones	k1gMnSc1	Velvetones
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
tu	ten	k3xDgFnSc4	ten
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrála	hrát	k5eAaImAgFnS	hrát
více	hodně	k6eAd2	hodně
placených	placený	k2eAgMnPc2d1	placený
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
disponovala	disponovat	k5eAaBmAgFnS	disponovat
lepším	dobrý	k2eAgNnSc7d2	lepší
technickým	technický	k2eAgNnSc7d1	technické
zázemím	zázemí	k1gNnSc7	zázemí
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
Rocking	Rocking	k1gInSc1	Rocking
Kings	Kings	k1gInSc4	Kings
získali	získat	k5eAaPmAgMnP	získat
stálé	stálý	k2eAgNnSc4d1	stálé
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Spanish	Spanisha	k1gFnPc2	Spanisha
Castle	Castle	k1gFnSc2	Castle
<g/>
,	,	kIx,	,
kterému	který	k3yQgNnSc3	který
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
složil	složit	k5eAaPmAgInS	složit
Hendrix	Hendrix	k1gInSc1	Hendrix
hold	hold	k1gInSc4	hold
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
písni	píseň	k1gFnSc6	píseň
Spanish	Spanish	k1gMnSc1	Spanish
Castle	Castle	k1gFnSc2	Castle
Magic	Magic	k1gMnSc1	Magic
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
vystoupení	vystoupení	k1gNnPc2	vystoupení
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgInS	nechat
Hendrix	Hendrix	k1gInSc1	Hendrix
svou	svůj	k3xOyFgFnSc4	svůj
kytaru	kytara	k1gFnSc4	kytara
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
a	a	k8xC	a
když	když	k8xS	když
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
ráno	ráno	k6eAd1	ráno
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
někdo	někdo	k3yInSc1	někdo
ukradl	ukradnout	k5eAaPmAgMnS	ukradnout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
kytaru	kytara	k1gFnSc4	kytara
neměl	mít	k5eNaImAgMnS	mít
a	a	k8xC	a
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
si	se	k3xPyFc3	se
počít	počít	k5eAaPmF	počít
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
Rocking	Rocking	k1gInSc4	Rocking
Kings	Kings	k1gInSc1	Kings
složili	složit	k5eAaPmAgMnP	složit
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
kytaru	kytara	k1gFnSc4	kytara
Danelectro	Danelectra	k1gFnSc5	Danelectra
Silvertone	Silverton	k1gInSc5	Silverton
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
i	i	k9	i
se	s	k7c7	s
zesilovačem	zesilovač	k1gInSc7	zesilovač
49,95	[number]	k4	49,95
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgMnS	začít
vymýšlet	vymýšlet	k5eAaImF	vymýšlet
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kytaru	kytara	k1gFnSc4	kytara
i	i	k8xC	i
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Nejraději	rád	k6eAd3	rád
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
Lucille	Lucille	k1gFnSc2	Lucille
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
si	se	k3xPyFc3	se
už	už	k6eAd1	už
obsadil	obsadit	k5eAaPmAgMnS	obsadit
B.B.	B.B.	k1gMnSc1	B.B.
King	King	k1gMnSc1	King
<g/>
.	.	kIx.	.
</s>
<s>
Rocking	Rocking	k1gInSc1	Rocking
Kings	Kingsa	k1gFnPc2	Kingsa
občas	občas	k6eAd1	občas
vyjížděli	vyjíždět	k5eAaImAgMnP	vyjíždět
ze	z	k7c2	z
Seattlu	Seattl	k1gInSc2	Seattl
na	na	k7c4	na
rádobyturné	rádobyturný	k2eAgNnSc4d1	rádobyturný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
je	on	k3xPp3gFnPc4	on
zrazovala	zrazovat	k5eAaImAgFnS	zrazovat
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
nepovedené	povedený	k2eNgFnSc6d1	nepovedená
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Vancouveru	Vancouver	k1gInSc2	Vancouver
přestalo	přestat	k5eAaPmAgNnS	přestat
kapele	kapela	k1gFnSc3	kapela
před	před	k7c7	před
kanadskými	kanadský	k2eAgFnPc7d1	kanadská
hranicemi	hranice	k1gFnPc7	hranice
fungovat	fungovat	k5eAaImF	fungovat
auto	auto	k1gNnSc4	auto
<g/>
.	.	kIx.	.
</s>
<s>
Odehráli	odehrát	k5eAaPmAgMnP	odehrát
narychlo	narychlo	k6eAd1	narychlo
domluvený	domluvený	k2eAgInSc4d1	domluvený
koncert	koncert	k1gInSc4	koncert
na	na	k7c6	na
washingtonském	washingtonský	k2eAgInSc6d1	washingtonský
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
jim	on	k3xPp3gMnPc3	on
sotva	sotva	k9	sotva
zbyly	zbýt	k5eAaPmAgInP	zbýt
peníze	peníz	k1gInPc1	peníz
na	na	k7c4	na
autobus	autobus	k1gInSc4	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
mnoho	mnoho	k6eAd1	mnoho
členů	člen	k1gMnPc2	člen
odešlo	odejít	k5eAaPmAgNnS	odejít
<g/>
,	,	kIx,	,
manažer	manažer	k1gMnSc1	manažer
James	James	k1gMnSc1	James
Thomas	Thomas	k1gMnSc1	Thomas
skupinu	skupina	k1gFnSc4	skupina
ale	ale	k9	ale
obnovil	obnovit	k5eAaPmAgInS	obnovit
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
jí	on	k3xPp3gFnSc7	on
nový	nový	k2eAgInSc1d1	nový
název	název	k1gInSc1	název
Thomas	Thomas	k1gMnSc1	Thomas
and	and	k?	and
the	the	k?	the
Tomcats	Tomcats	k1gInSc1	Tomcats
<g/>
.	.	kIx.	.
</s>
<s>
Hendrixovi	Hendrixa	k1gMnSc3	Hendrixa
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
oproti	oproti	k7c3	oproti
dřívějšku	dřívějšek	k1gInSc3	dřívějšek
významnější	významný	k2eAgFnSc2d2	významnější
role	role	k1gFnSc2	role
-	-	kIx~	-
začal	začít	k5eAaPmAgInS	začít
zpívat	zpívat	k5eAaImF	zpívat
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
vokály	vokál	k1gInPc4	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
měla	mít	k5eAaImAgFnS	mít
už	už	k9	už
většina	většina	k1gFnSc1	většina
Hendrixových	Hendrixový	k2eAgMnPc2d1	Hendrixový
kamarádů	kamarád	k1gMnPc2	kamarád
odmaturováno	odmaturován	k2eAgNnSc1d1	odmaturováno
<g/>
,	,	kIx,	,
Hendrix	Hendrix	k1gInSc1	Hendrix
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vyhazovu	vyhazov	k1gInSc6	vyhazov
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
živil	živit	k5eAaImAgMnS	živit
společně	společně	k6eAd1	společně
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
upravoval	upravovat	k5eAaImAgMnS	upravovat
zeleň	zeleň	k1gFnSc4	zeleň
na	na	k7c6	na
veřejných	veřejný	k2eAgNnPc6d1	veřejné
prostranstvích	prostranství	k1gNnPc6	prostranství
a	a	k8xC	a
na	na	k7c6	na
zahradách	zahrada	k1gFnPc6	zahrada
seattleských	seattleský	k2eAgMnPc2d1	seattleský
boháčů	boháč	k1gMnPc2	boháč
a	a	k8xC	a
občasným	občasný	k2eAgFnPc3d1	občasná
hraním	hraň	k1gFnPc3	hraň
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Jimi	on	k3xPp3gInPc7	on
o	o	k7c4	o
sobě	se	k3xPyFc3	se
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
živí	živit	k5eAaImIp3nS	živit
hraní	hraní	k1gNnSc1	hraní
s	s	k7c7	s
Tomcats	Tomcatsa	k1gFnPc2	Tomcatsa
<g/>
.	.	kIx.	.
</s>
<s>
Sekání	sekání	k1gNnSc1	sekání
a	a	k8xC	a
zalévání	zalévání	k1gNnSc1	zalévání
trávníků	trávník	k1gInPc2	trávník
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
neměl	mít	k5eNaImAgMnS	mít
rád	rád	k2eAgMnSc1d1	rád
a	a	k8xC	a
nebavilo	bavit	k5eNaImAgNnS	bavit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dokonce	dokonce	k9	dokonce
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
spolu	spolu	k6eAd1	spolu
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
,	,	kIx,	,
vyplatil	vyplatit	k5eAaPmAgMnS	vyplatit
otec	otec	k1gMnSc1	otec
jediný	jediný	k2eAgInSc4d1	jediný
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
Al	ala	k1gFnPc2	ala
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
udeřil	udeřit	k5eAaPmAgMnS	udeřit
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
utekl	utéct	k5eAaPmAgMnS	utéct
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
bylo	být	k5eAaImAgNnS	být
Hendrixovi	Hendrixa	k1gMnSc3	Hendrixa
tehdy	tehdy	k6eAd1	tehdy
už	už	k6eAd1	už
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
ho	on	k3xPp3gMnSc4	on
stále	stále	k6eAd1	stále
"	"	kIx"	"
<g/>
vyplácel	vyplácet	k5eAaImAgMnS	vyplácet
<g/>
"	"	kIx"	"
výprasky	výprask	k1gInPc1	výprask
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
chodil	chodit	k5eAaImAgMnS	chodit
za	za	k7c7	za
jedním	jeden	k4xCgMnSc7	jeden
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
z	z	k7c2	z
Tomcats	Tomcatsa	k1gFnPc2	Tomcatsa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prodával	prodávat	k5eAaImAgInS	prodávat
naproti	naproti	k7c3	naproti
bývalé	bývalý	k2eAgFnSc3d1	bývalá
Hendrixově	Hendrixově	k1gFnSc3	Hendrixově
škole	škola	k1gFnSc3	škola
na	na	k7c6	na
stánku	stánek	k1gInSc6	stánek
hamburgery	hamburger	k1gInPc1	hamburger
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavírací	zavírací	k2eAgFnSc6d1	zavírací
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
Jimi	on	k3xPp3gInPc7	on
vzít	vzít	k5eAaPmF	vzít
neprodané	prodaný	k2eNgInPc4d1	neprodaný
hamburgery	hamburger	k1gInPc4	hamburger
a	a	k8xC	a
hranolky	hranolek	k1gInPc4	hranolek
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
své	svůj	k3xOyFgFnSc3	svůj
chudobě	chudoba	k1gFnSc3	chudoba
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
neutěšené	utěšený	k2eNgFnSc6d1	neutěšená
životní	životní	k2eAgFnSc6d1	životní
situaci	situace	k1gFnSc6	situace
myslel	myslet	k5eAaImAgInS	myslet
Hendrix	Hendrix	k1gInSc1	Hendrix
na	na	k7c4	na
svatbu	svatba	k1gFnSc4	svatba
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
láskou	láska	k1gFnSc7	láska
Betty	Betty	k1gFnSc1	Betty
Jean	Jean	k1gMnSc1	Jean
Morganovou	morganův	k2eAgFnSc7d1	Morganova
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
a	a	k8xC	a
po	po	k7c6	po
které	který	k3yQgFnSc6	který
nakonec	nakonec	k6eAd1	nakonec
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
myšlenkám	myšlenka	k1gFnPc3	myšlenka
ale	ale	k8xC	ale
učinil	učinit	k5eAaImAgMnS	učinit
ráznou	rázný	k2eAgFnSc4d1	rázná
přítrž	přítrž	k1gFnSc4	přítrž
Hendrixův	Hendrixův	k2eAgInSc4d1	Hendrixův
nástup	nástup	k1gInSc4	nástup
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
důvodech	důvod	k1gInPc6	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Hendrix	Hendrix	k1gInSc1	Hendrix
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
a	a	k8xC	a
po	po	k7c6	po
necelém	celý	k2eNgInSc6d1	necelý
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
koluje	kolovat	k5eAaImIp3nS	kolovat
povícero	povícero	k6eAd1	povícero
historek	historka	k1gFnPc2	historka
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Hendrix	Hendrix	k1gInSc4	Hendrix
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
armádě	armáda	k1gFnSc3	armáda
narukoval	narukovat	k5eAaPmAgInS	narukovat
zcela	zcela	k6eAd1	zcela
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vůle	vůle	k1gFnSc2	vůle
a	a	k8xC	a
po	po	k7c6	po
necelém	celý	k2eNgInSc6d1	necelý
roce	rok	k1gInSc6	rok
předčasně	předčasně	k6eAd1	předčasně
ukončil	ukončit	k5eAaPmAgMnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
službu	služba	k1gFnSc4	služba
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
poranění	poranění	k1gNnSc2	poranění
kotníku	kotník	k1gInSc2	kotník
a	a	k8xC	a
páteře	páteř	k1gFnSc2	páteř
při	při	k7c6	při
seskoku	seskok	k1gInSc6	seskok
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
historku	historka	k1gFnSc4	historka
si	se	k3xPyFc3	se
ale	ale	k9	ale
nejspíš	nejspíš	k9	nejspíš
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
pro	pro	k7c4	pro
pera	pero	k1gNnPc4	pero
žurnalistů	žurnalist	k1gMnPc2	žurnalist
<g/>
,	,	kIx,	,
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2	pravděpodobnější
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Pokoj	pokoj	k1gInSc4	pokoj
plný	plný	k2eAgInSc4d1	plný
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1961	[number]	k4	1961
byl	být	k5eAaImAgInS	být
Jimi	on	k3xPp3gInPc7	on
přistižen	přistižen	k2eAgMnSc1d1	přistižen
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jel	jet	k5eAaImAgMnS	jet
v	v	k7c6	v
kradeném	kradený	k2eAgNnSc6d1	kradené
vozidle	vozidlo	k1gNnSc6	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Strávil	strávit	k5eAaPmAgMnS	strávit
den	den	k1gInSc4	den
v	v	k7c6	v
krizovém	krizový	k2eAgNnSc6d1	krizové
centru	centrum	k1gNnSc6	centrum
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vyzvedl	vyzvednout	k5eAaPmAgMnS	vyzvednout
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
se	se	k3xPyFc4	se
stejná	stejný	k2eAgFnSc1d1	stejná
situace	situace	k1gFnSc1	situace
opakovala	opakovat	k5eAaImAgFnS	opakovat
a	a	k8xC	a
Hendrix	Hendrix	k1gInSc1	Hendrix
strávil	strávit	k5eAaPmAgInS	strávit
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ho	on	k3xPp3gMnSc4	on
čekalo	čekat	k5eAaImAgNnS	čekat
soudní	soudní	k2eAgNnSc1d1	soudní
přelíčení	přelíčení	k1gNnSc1	přelíčení
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
tučné	tučný	k2eAgFnSc2d1	tučná
pokuty	pokuta	k1gFnSc2	pokuta
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
dušoval	dušovat	k5eAaImAgMnS	dušovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgNnSc4	žádný
z	z	k7c2	z
aut	auto	k1gNnPc2	auto
neukradl	ukradnout	k5eNaPmAgMnS	ukradnout
a	a	k8xC	a
neměl	mít	k5eNaImAgMnS	mít
ani	ani	k8xC	ani
tušení	tušení	k1gNnSc4	tušení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
kradená	kradený	k2eAgNnPc1d1	kradené
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
auta	auto	k1gNnPc4	auto
ani	ani	k8xC	ani
neřídil	řídit	k5eNaImAgMnS	řídit
<g/>
,	,	kIx,	,
prý	prý	k9	prý
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
jen	jen	k6eAd1	jen
seděl	sedět	k5eAaImAgMnS	sedět
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
mu	on	k3xPp3gMnSc3	on
hrozilo	hrozit	k5eAaImAgNnS	hrozit
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
za	za	k7c7	za
mřížemi	mříž	k1gFnPc7	mříž
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dostal	dostat	k5eAaPmAgMnS	dostat
dvouletý	dvouletý	k2eAgInSc4d1	dvouletý
trest	trest	k1gInSc4	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
prominut	prominut	k2eAgInSc1d1	prominut
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
upsal	upsat	k5eAaPmAgMnS	upsat
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vykreslován	vykreslován	k2eAgMnSc1d1	vykreslován
jako	jako	k8xC	jako
hipísácký	hipísácký	k2eAgMnSc1d1	hipísácký
antimilitaristicky	antimilitaristicky	k6eAd1	antimilitaristicky
smýšlející	smýšlející	k2eAgMnSc1d1	smýšlející
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
choval	chovat	k5eAaImAgMnS	chovat
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
úctě	úcta	k1gFnSc6	úcta
a	a	k8xC	a
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
projevil	projevit	k5eAaPmAgMnS	projevit
s	s	k7c7	s
kamarádem	kamarád	k1gMnSc7	kamarád
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
narukování	narukování	k1gNnSc4	narukování
k	k	k7c3	k
letectvu	letectvo	k1gNnSc3	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
nebyl	být	k5eNaImAgInS	být
přijat	přijat	k2eAgInSc1d1	přijat
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
fyzického	fyzický	k2eAgNnSc2d1	fyzické
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
odjel	odjet	k5eAaPmAgMnS	odjet
Jimi	on	k3xPp3gInPc7	on
vlakem	vlak	k1gInSc7	vlak
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
Fort	Fort	k?	Fort
Ord	orda	k1gFnPc2	orda
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
svého	svůj	k3xOyFgInSc2	svůj
odjezdu	odjezd	k1gInSc2	odjezd
ještě	ještě	k9	ještě
stihl	stihnout	k5eAaPmAgInS	stihnout
odehrát	odehrát	k5eAaPmF	odehrát
poslední	poslední	k2eAgInSc1d1	poslední
koncert	koncert	k1gInSc1	koncert
s	s	k7c7	s
Tomcats	Tomcats	k1gInSc4	Tomcats
a	a	k8xC	a
věnovat	věnovat	k5eAaImF	věnovat
Betty	Betty	k1gFnSc4	Betty
Jean	Jean	k1gMnSc1	Jean
levný	levný	k2eAgInSc4d1	levný
zásnubní	zásnubní	k2eAgInSc4d1	zásnubní
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
dorazil	dorazit	k5eAaPmAgMnS	dorazit
Hendrix	Hendrix	k1gInSc4	Hendrix
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
zažádal	zažádat	k5eAaPmAgMnS	zažádat
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
písaře	písař	k1gMnSc2	písař
u	u	k7c2	u
101	[number]	k4	101
<g/>
.	.	kIx.	.
výsadkové	výsadkový	k2eAgFnSc2d1	výsadková
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
psal	psát	k5eAaImAgInS	psát
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
(	(	kIx(	(
<g/>
jednou	jednou	k6eAd1	jednou
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ztratil	ztratit	k5eAaPmAgMnS	ztratit
lístek	lístek	k1gInSc4	lístek
na	na	k7c4	na
autobus	autobus	k1gInSc4	autobus
<g/>
)	)	kIx)	)
a	a	k8xC	a
Betty	Betty	k1gFnSc7	Betty
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
jednou	jednou	k6eAd1	jednou
poslal	poslat	k5eAaPmAgMnS	poslat
ateliérovou	ateliérový	k2eAgFnSc4d1	ateliérová
fotku	fotka	k1gFnSc4	fotka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
uniformě	uniforma	k1gFnSc6	uniforma
před	před	k7c7	před
umělým	umělý	k2eAgNnSc7d1	umělé
tropickým	tropický	k2eAgNnSc7d1	tropické
pozadím	pozadí	k1gNnSc7	pozadí
a	a	k8xC	a
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
drží	držet	k5eAaImIp3nS	držet
svou	svůj	k3xOyFgFnSc4	svůj
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
podpis	podpis	k1gInSc4	podpis
vždy	vždy	k6eAd1	vždy
ještě	ještě	k6eAd1	ještě
nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
malý	malý	k2eAgInSc1d1	malý
obrázek	obrázek	k1gInSc1	obrázek
své	svůj	k3xOyFgFnSc2	svůj
kytary	kytara	k1gFnSc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dopisů	dopis	k1gInPc2	dopis
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
tuhý	tuhý	k2eAgInSc4d1	tuhý
vojenský	vojenský	k2eAgInSc4d1	vojenský
řád	řád	k1gInSc4	řád
snad	snad	k9	snad
i	i	k9	i
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
ulice	ulice	k1gFnSc2	ulice
zde	zde	k6eAd1	zde
muselo	muset	k5eAaImAgNnS	muset
dodržovat	dodržovat	k5eAaImF	dodržovat
pevný	pevný	k2eAgInSc4d1	pevný
denní	denní	k2eAgInSc4d1	denní
režim	režim	k1gInSc4	režim
a	a	k8xC	a
dostávalo	dostávat	k5eAaImAgNnS	dostávat
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
třikrát	třikrát	k6eAd1	třikrát
denně	denně	k6eAd1	denně
najíst	najíst	k5eAaBmF	najíst
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
základního	základní	k2eAgInSc2d1	základní
výcviku	výcvik	k1gInSc2	výcvik
obdržel	obdržet	k5eAaPmAgInS	obdržet
hodnost	hodnost	k1gFnSc4	hodnost
vojína	vojín	k1gMnSc2	vojín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
dostal	dostat	k5eAaPmAgMnS	dostat
týdenní	týdenní	k2eAgFnSc4d1	týdenní
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
zajel	zajet	k5eAaPmAgMnS	zajet
se	se	k3xPyFc4	se
podívat	podívat	k5eAaImF	podívat
domů	domů	k6eAd1	domů
do	do	k7c2	do
Seattlu	Seattl	k1gInSc2	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jeho	jeho	k3xOp3gMnPc1	jeho
známí	známý	k1gMnPc1	známý
<g/>
,	,	kIx,	,
především	především	k9	především
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
Hendrixe	Hendrixe	k1gFnPc4	Hendrixe
v	v	k7c6	v
uniformě	uniforma	k1gFnSc6	uniforma
hrdí	hrdý	k2eAgMnPc1d1	hrdý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
schůzkách	schůzka	k1gFnPc6	schůzka
s	s	k7c7	s
Betty	Betty	k1gFnSc7	Betty
Jean	Jean	k1gMnSc1	Jean
ji	on	k3xPp3gFnSc4	on
stále	stále	k6eAd1	stále
přemlouval	přemlouvat	k5eAaImAgMnS	přemlouvat
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Halloween	Halloween	k1gInSc4	Halloween
byl	být	k5eAaImAgInS	být
odvelen	odvelen	k2eAgMnSc1d1	odvelen
jako	jako	k8xS	jako
pomocný	pomocný	k2eAgMnSc1d1	pomocný
písař	písař	k1gMnSc1	písař
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vysněné	vysněný	k2eAgNnSc4d1	vysněné
101	[number]	k4	101
<g/>
.	.	kIx.	.
výsadkové	výsadkový	k2eAgFnSc3d1	výsadková
divizi	divize	k1gFnSc3	divize
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
Fort	Fort	k?	Fort
Campbell	Campbella	k1gFnPc2	Campbella
v	v	k7c4	v
Kentucky	Kentucka	k1gFnPc4	Kentucka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
otci	otec	k1gMnSc3	otec
psal	psát	k5eAaImAgMnS	psát
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
a	a	k8xC	a
udělá	udělat	k5eAaPmIp3nS	udělat
vše	všechen	k3xTgNnSc1	všechen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
Hendrixů	Hendrix	k1gInPc2	Hendrix
mohla	moct	k5eAaImAgFnS	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
výsadkářskou	výsadkářský	k2eAgFnSc7d1	výsadkářská
nášivkou	nášivka	k1gFnSc7	nášivka
s	s	k7c7	s
"	"	kIx"	"
<g/>
křičícím	křičící	k2eAgMnSc7d1	křičící
orlem	orel	k1gMnSc7	orel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
cvičil	cvičit	k5eAaImAgInS	cvičit
Hendrix	Hendrix	k1gInSc1	Hendrix
v	v	k7c6	v
klubovně	klubovna	k1gFnSc6	klubovna
základny	základna	k1gFnSc2	základna
Fort	Fort	k?	Fort
Campbell	Campbell	k1gMnSc1	Campbell
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Uslyšel	uslyšet	k5eAaPmAgMnS	uslyšet
ho	on	k3xPp3gNnSc4	on
baskytarista	baskytarista	k1gMnSc1	baskytarista
Billy	Bill	k1gMnPc4	Bill
Cox	Cox	k1gMnSc2	Cox
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
začal	začít	k5eAaPmAgMnS	začít
jamovat	jamovat	k5eAaImF	jamovat
a	a	k8xC	a
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc4	jejich
hudební	hudební	k2eAgNnSc4d1	hudební
i	i	k8xC	i
osobní	osobní	k2eAgNnSc4d1	osobní
přátelství	přátelství	k1gNnSc4	přátelství
jim	on	k3xPp3gFnPc3	on
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k9	až
do	do	k7c2	do
Jimiho	Jimi	k1gMnSc2	Jimi
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Založili	založit	k5eAaPmAgMnP	založit
pětičlennou	pětičlenný	k2eAgFnSc4d1	pětičlenná
kapelu	kapela	k1gFnSc4	kapela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
neměla	mít	k5eNaImAgFnS	mít
název	název	k1gInSc4	název
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
měnili	měnit	k5eAaImAgMnP	měnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jádro	jádro	k1gNnSc1	jádro
-	-	kIx~	-
Hendrix	Hendrix	k1gInSc1	Hendrix
a	a	k8xC	a
Cox	Cox	k1gFnSc1	Cox
-	-	kIx~	-
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
hráli	hrát	k5eAaImAgMnP	hrát
jen	jen	k9	jen
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
Cox	Cox	k1gMnSc4	Cox
nebyl	být	k5eNaImAgMnS	být
žádný	žádný	k3yNgMnSc1	žádný
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
okusil	okusit	k5eAaPmAgMnS	okusit
Hendrix	Hendrix	k1gInSc4	Hendrix
poprvé	poprvé	k6eAd1	poprvé
roli	role	k1gFnSc4	role
frontmana	frontman	k1gMnSc2	frontman
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
rád	rád	k6eAd1	rád
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiné	jiný	k2eAgNnSc1d1	jiné
řešení	řešení	k1gNnSc1	řešení
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
The	The	k1gMnPc1	The
Kasuals	Kasuals	k1gInSc4	Kasuals
začali	začít	k5eAaPmAgMnP	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
Fort	Fort	k?	Fort
Campbell	Campbell	k1gInSc4	Campbell
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
si	se	k3xPyFc3	se
získávat	získávat	k5eAaImF	získávat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Jih	jih	k1gInSc1	jih
USA	USA	kA	USA
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
rasově	rasově	k6eAd1	rasově
segregovaný	segregovaný	k2eAgMnSc1d1	segregovaný
<g/>
,	,	kIx,	,
než	než	k8xS	než
Jimiho	Jimi	k1gMnSc4	Jimi
rodný	rodný	k2eAgInSc1d1	rodný
Seattle	Seattle	k1gFnPc1	Seattle
<g/>
.	.	kIx.	.
</s>
<s>
Hráli	hrát	k5eAaImAgMnP	hrát
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
černošské	černošský	k2eAgNnSc4d1	černošské
publikum	publikum	k1gNnSc4	publikum
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
například	například	k6eAd1	například
nepřipadalo	připadat	k5eNaImAgNnS	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
hrát	hrát	k5eAaImF	hrát
covery	cover	k1gInPc4	cover
Jimiho	Jimi	k1gMnSc2	Jimi
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
písničky	písnička	k1gFnPc1	písnička
Louie	Louie	k1gFnSc2	Louie
<g/>
,	,	kIx,	,
Louie	Louie	k1gFnSc2	Louie
od	od	k7c2	od
bělošské	bělošský	k2eAgFnSc2d1	bělošská
formace	formace	k1gFnSc2	formace
The	The	k1gMnSc1	The
Kingsmen	Kingsmen	k1gInSc1	Kingsmen
<g/>
.	.	kIx.	.
</s>
<s>
Pohledný	pohledný	k2eAgMnSc1d1	pohledný
a	a	k8xC	a
divoce	divoce	k6eAd1	divoce
hrající	hrající	k2eAgInSc1d1	hrající
Hendrix	Hendrix	k1gInSc1	Hendrix
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
poprvé	poprvé	k6eAd1	poprvé
stává	stávat	k5eAaImIp3nS	stávat
miláčkem	miláček	k1gMnSc7	miláček
ženské	ženský	k2eAgFnSc2d1	ženská
části	část	k1gFnSc2	část
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
věhlas	věhlas	k1gInSc1	věhlas
The	The	k1gFnSc2	The
Kasuals	Kasualsa	k1gFnPc2	Kasualsa
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
skupině	skupina	k1gFnSc3	skupina
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
několik	několik	k4yIc1	několik
turné	turné	k1gNnPc2	turné
po	po	k7c6	po
jihu	jih	k1gInSc6	jih
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
narážel	narážet	k5eAaPmAgInS	narážet
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dva	dva	k4xCgInPc4	dva
stavební	stavební	k2eAgInPc4d1	stavební
pilíře	pilíř	k1gInPc4	pilíř
kapely	kapela	k1gFnPc1	kapela
byly	být	k5eAaImAgFnP	být
vojáky	voják	k1gMnPc4	voják
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
úvazek	úvazek	k1gInSc4	úvazek
<g/>
.	.	kIx.	.
</s>
<s>
Coxovi	Coxa	k1gMnSc3	Coxa
zbývalo	zbývat	k5eAaImAgNnS	zbývat
už	už	k6eAd1	už
jen	jen	k9	jen
posledních	poslední	k2eAgInPc2d1	poslední
pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Hendrix	Hendrix	k1gInSc1	Hendrix
měl	mít	k5eAaImAgInS	mít
před	před	k7c7	před
sebou	se	k3xPyFc7	se
ještě	ještě	k6eAd1	ještě
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
začal	začít	k5eAaPmAgMnS	začít
před	před	k7c7	před
vojenským	vojenský	k2eAgMnSc7d1	vojenský
doktorem	doktor	k1gMnSc7	doktor
předstírat	předstírat	k5eAaImF	předstírat
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
,	,	kIx,	,
závislost	závislost	k1gFnSc4	závislost
na	na	k7c4	na
masturbaci	masturbace	k1gFnSc4	masturbace
<g/>
,	,	kIx,	,
pomočování	pomočování	k1gNnSc4	pomočování
<g/>
,	,	kIx,	,
neschopnost	neschopnost	k1gFnSc4	neschopnost
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
závratě	závrať	k1gFnSc2	závrať
<g/>
,	,	kIx,	,
bolest	bolest	k1gFnSc4	bolest
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
části	část	k1gFnSc6	část
prsou	prsa	k1gNnPc6	prsa
atd.	atd.	kA	atd.
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
riskantní	riskantní	k2eAgInSc1d1	riskantní
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
mu	on	k3xPp3gMnSc3	on
psychiatr	psychiatr	k1gMnSc1	psychiatr
tyto	tyt	k2eAgNnSc4d1	tyto
tvrzení	tvrzení	k1gNnSc4	tvrzení
neuvěřil	uvěřit	k5eNaPmAgInS	uvěřit
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgInS	mít
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
černošský	černošský	k2eAgMnSc1d1	černošský
gay	gay	k1gMnSc1	gay
mezi	mezi	k7c7	mezi
vojáky	voják	k1gMnPc7	voják
na	na	k7c6	na
rasistickém	rasistický	k2eAgInSc6d1	rasistický
jihu	jih	k1gInSc6	jih
lehké	lehký	k2eAgNnSc1d1	lehké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
doporučil	doporučit	k5eAaPmAgMnS	doporučit
kapitán	kapitán	k1gMnSc1	kapitán
John	John	k1gMnSc1	John
Haybert	Haybert	k1gInSc4	Haybert
Hendrixe	Hendrixe	k1gFnSc2	Hendrixe
propustit	propustit	k5eAaPmF	propustit
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
"	"	kIx"	"
<g/>
homosexuálních	homosexuální	k2eAgInPc2d1	homosexuální
sklonů	sklon	k1gInPc2	sklon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
před	před	k7c7	před
branou	brána	k1gFnSc7	brána
Fort	Fort	k?	Fort
Campbell	Campbell	k1gMnSc1	Campbell
se	s	k7c7	s
čtyřmi	čtyři	k4xCgNnPc7	čtyři
sty	sto	k4xCgNnPc7	sto
dolary	dolar	k1gInPc7	dolar
v	v	k7c6	v
kapse	kapsa	k1gFnSc6	kapsa
-	-	kIx~	-
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hotovost	hotovost	k1gFnSc4	hotovost
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
kdy	kdy	k6eAd1	kdy
měl	mít	k5eAaImAgInS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
baru	bar	k1gInSc2	bar
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
pitce	pitka	k1gFnSc6	pitka
mu	on	k3xPp3gMnSc3	on
zbylo	zbýt	k5eAaPmAgNnS	zbýt
pouhých	pouhý	k2eAgInPc2d1	pouhý
šestnáct	šestnáct	k4xCc4	šestnáct
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
domů	dům	k1gInPc2	dům
do	do	k7c2	do
Seattlu	Seattl	k1gInSc2	Seattl
nestačily	stačit	k5eNaBmAgFnP	stačit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
noc	noc	k1gFnSc1	noc
v	v	k7c6	v
civilu	civil	k1gMnSc6	civil
strávil	strávit	k5eAaPmAgInS	strávit
Hendrix	Hendrix	k1gInSc1	Hendrix
paradoxně	paradoxně	k6eAd1	paradoxně
tajně	tajně	k6eAd1	tajně
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
kavalci	kavalec	k1gInSc6	kavalec
v	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k1gNnSc4	ráno
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
nevrátí	vrátit	k5eNaPmIp3nP	vrátit
a	a	k8xC	a
zkusí	zkusit	k5eAaPmIp3nP	zkusit
štěstí	štěstí	k1gNnSc4	štěstí
jako	jako	k8xC	jako
muzikant	muzikant	k1gMnSc1	muzikant
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1962	[number]	k4	1962
pustila	pustit	k5eAaPmAgFnS	pustit
armáda	armáda	k1gFnSc1	armáda
Billyho	Billy	k1gMnSc2	Billy
Coxe	Cox	k1gMnSc2	Cox
do	do	k7c2	do
civilu	civil	k1gMnSc3	civil
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
s	s	k7c7	s
Hendrixem	Hendrix	k1gInSc7	Hendrix
začít	začít	k5eAaPmF	začít
naplno	naplno	k6eAd1	naplno
věnovat	věnovat	k5eAaPmF	věnovat
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
štaci	štace	k1gFnSc6	štace
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
do	do	k7c2	do
Indianapolis	Indianapolis	k1gFnSc2	Indianapolis
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
ale	ale	k9	ale
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
místní	místní	k2eAgInSc1d1	místní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
chtěli	chtít	k5eAaImAgMnP	chtít
hrát	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
nenechá	nechat	k5eNaPmIp3nS	nechat
vystoupit	vystoupit	k5eAaPmF	vystoupit
kapelu	kapela	k1gFnSc4	kapela
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
výhradně	výhradně	k6eAd1	výhradně
černochy	černoch	k1gMnPc4	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Zkusili	zkusit	k5eAaPmAgMnP	zkusit
štěstí	štěstit	k5eAaImIp3nP	štěstit
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
klubu	klub	k1gInSc6	klub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
ten	ten	k3xDgInSc4	ten
večer	večer	k1gInSc4	večer
Souboj	souboj	k1gInSc1	souboj
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
King	King	k1gMnSc1	King
Kasuals	Kasualsa	k1gFnPc2	Kasualsa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
nový	nový	k2eAgInSc4d1	nový
název	název	k1gInSc4	název
The	The	k1gFnPc2	The
Kasuals	Kasualsa	k1gFnPc2	Kasualsa
<g/>
,	,	kIx,	,
obsadili	obsadit	k5eAaPmAgMnP	obsadit
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
kytaristu	kytarista	k1gMnSc4	kytarista
vítězné	vítězný	k2eAgFnSc2d1	vítězná
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc2	The
Presidents	Presidentsa	k1gFnPc2	Presidentsa
Alphonse	Alphons	k1gMnSc2	Alphons
Younga	Young	k1gMnSc2	Young
udělali	udělat	k5eAaPmAgMnP	udělat
takový	takový	k3xDgInSc4	takový
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
opustil	opustit	k5eAaPmAgMnS	opustit
svou	svůj	k3xOyFgFnSc4	svůj
dosavadní	dosavadní	k2eAgFnSc4d1	dosavadní
kapelu	kapela	k1gFnSc4	kapela
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
ke	k	k7c3	k
King	King	k1gMnSc1	King
Kasuals	Kasuals	k1gInSc1	Kasuals
<g/>
.	.	kIx.	.
</s>
<s>
Alphonso	Alphonsa	k1gFnSc5	Alphonsa
Young	Young	k1gMnSc1	Young
ovládal	ovládat	k5eAaImAgMnS	ovládat
hraní	hraní	k1gNnSc4	hraní
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
zuby	zub	k1gInPc1	zub
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
kouskem	kousek	k1gInSc7	kousek
setkal	setkat	k5eAaPmAgInS	setkat
ještě	ještě	k9	ještě
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
kousek	kousek	k1gInSc1	kousek
mohl	moct	k5eAaImAgInS	moct
učit	učit	k5eAaImF	učit
přímo	přímo	k6eAd1	přímo
od	od	k7c2	od
spoluhráče	spoluhráč	k1gMnSc2	spoluhráč
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
také	také	k9	také
dělal	dělat	k5eAaImAgMnS	dělat
a	a	k8xC	a
brzo	brzo	k6eAd1	brzo
ovládal	ovládat	k5eAaImAgInS	ovládat
tento	tento	k3xDgInSc4	tento
jevištní	jevištní	k2eAgInSc4d1	jevištní
kousek	kousek	k1gInSc4	kousek
taky	taky	k6eAd1	taky
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Indianapolis	Indianapolis	k1gFnSc6	Indianapolis
si	se	k3xPyFc3	se
našla	najít	k5eAaPmAgFnS	najít
kapela	kapela	k1gFnSc1	kapela
stálé	stálý	k2eAgNnSc1d1	stálé
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Del	Del	k1gMnSc1	Del
Morocco	Morocco	k1gMnSc1	Morocco
v	v	k7c6	v
Nashvillu	Nashvill	k1gInSc6	Nashvill
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
si	se	k3xPyFc3	se
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
kapely	kapela	k1gFnSc2	kapela
nenašel	najít	k5eNaPmAgInS	najít
jinou	jiný	k2eAgFnSc4d1	jiná
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
živil	živit	k5eAaImAgMnS	živit
se	se	k3xPyFc4	se
tak	tak	k9	tak
pouze	pouze	k6eAd1	pouze
hraním	hranit	k5eAaImIp1nS	hranit
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
peněz	peníze	k1gInPc2	peníze
si	se	k3xPyFc3	se
nevydělal	vydělat	k5eNaPmAgMnS	vydělat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
hlediska	hledisko	k1gNnSc2	hledisko
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
skvělý	skvělý	k2eAgInSc1d1	skvělý
tah	tah	k1gInSc1	tah
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
místo	místo	k7c2	místo
práce	práce	k1gFnSc2	práce
mohl	moct	k5eAaImAgMnS	moct
cvičit	cvičit	k5eAaImF	cvičit
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
cvičil	cvičit	k5eAaImAgMnS	cvičit
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohl	moct	k5eAaImAgMnS	moct
-	-	kIx~	-
cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
o	o	k7c6	o
přestávkách	přestávka	k1gFnPc6	přestávka
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
před	před	k7c7	před
spaním	spaní	k1gNnSc7	spaní
<g/>
,	,	kIx,	,
po	po	k7c6	po
probuzení	probuzení	k1gNnSc6	probuzení
<g/>
,	,	kIx,	,
v	v	k7c6	v
autobuse	autobus	k1gInSc6	autobus
atd.	atd.	kA	atd.
Tato	tento	k3xDgFnSc1	tento
dřina	dřina	k1gFnSc1	dřina
ovšem	ovšem	k9	ovšem
začala	začít	k5eAaPmAgFnS	začít
nést	nést	k5eAaImF	nést
své	svůj	k3xOyFgNnSc4	svůj
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
době	doba	k1gFnSc3	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yRgFnSc4	který
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
hrál	hrát	k5eAaImAgInS	hrát
-	-	kIx~	-
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
-	-	kIx~	-
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
již	již	k6eAd1	již
značné	značný	k2eAgFnPc4d1	značná
virtuosity	virtuosita	k1gFnPc4	virtuosita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začíná	začínat	k5eAaImIp3nS	začínat
Hendrix	Hendrix	k1gInSc4	Hendrix
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
užívat	užívat	k5eAaImF	užívat
nelegální	legální	k2eNgFnPc4d1	nelegální
drogy	droga	k1gFnPc4	droga
-	-	kIx~	-
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
marihuanu	marihuana	k1gFnSc4	marihuana
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
ale	ale	k9	ale
moc	moc	k6eAd1	moc
často	často	k6eAd1	často
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
financím	finance	k1gFnPc3	finance
dovolit	dovolit	k5eAaPmF	dovolit
nemohl	moct	k5eNaImAgInS	moct
nebo	nebo	k8xC	nebo
o	o	k7c4	o
amfetaminy	amfetamin	k1gInPc4	amfetamin
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
umožnily	umožnit	k5eAaPmAgInP	umožnit
zůstávat	zůstávat	k5eAaImF	zůstávat
déle	dlouho	k6eAd2	dlouho
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
déle	dlouho	k6eAd2	dlouho
cvičit	cvičit	k5eAaImF	cvičit
<g/>
.	.	kIx.	.
</s>
<s>
Hraní	hranit	k5eAaImIp3nS	hranit
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
neslo	nést	k5eAaImAgNnS	nést
velice	velice	k6eAd1	velice
málo	málo	k4c1	málo
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
Hendrix	Hendrix	k1gInSc4	Hendrix
s	s	k7c7	s
Coxem	Cox	k1gMnSc7	Cox
chtěli	chtít	k5eAaImAgMnP	chtít
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
nějaké	nějaký	k3yIgNnSc4	nějaký
peníze	peníz	k1gInPc1	peníz
jako	jako	k8xC	jako
studioví	studiový	k2eAgMnPc1d1	studiový
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1962	[number]	k4	1962
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
domluvit	domluvit	k5eAaPmF	domluvit
si	se	k3xPyFc3	se
nahrávání	nahrávání	k1gNnSc4	nahrávání
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
King	Kinga	k1gFnPc2	Kinga
Records	Records	k1gInSc1	Records
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikala	vznikat	k5eAaImAgFnS	vznikat
deska	deska	k1gFnSc1	deska
skupiny	skupina	k1gFnSc2	skupina
Commanders	Commandersa	k1gFnPc2	Commandersa
<g/>
.	.	kIx.	.
</s>
<s>
Hendrixovi	Hendrixa	k1gMnSc3	Hendrixa
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
odpojen	odpojen	k2eAgInSc4d1	odpojen
mikrofon	mikrofon	k1gInSc4	mikrofon
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
setkával	setkávat	k5eAaImAgMnS	setkávat
mnohokrát	mnohokrát	k6eAd1	mnohokrát
-	-	kIx~	-
někomu	někdo	k3yInSc3	někdo
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
zase	zase	k9	zase
<g/>
)	)	kIx)	)
zdál	zdát	k5eAaImAgInS	zdát
jeho	jeho	k3xOp3gInSc1	jeho
styl	styl	k1gInSc1	styl
hraní	hranit	k5eAaImIp3nS	hranit
příliš	příliš	k6eAd1	příliš
syrový	syrový	k2eAgMnSc1d1	syrový
a	a	k8xC	a
divoký	divoký	k2eAgMnSc1d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
pravdou	pravda	k1gFnSc7	pravda
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
uměl	umět	k5eAaImAgMnS	umět
Jimi	on	k3xPp3gMnPc7	on
hrát	hrát	k5eAaImF	hrát
sice	sice	k8xC	sice
velice	velice	k6eAd1	velice
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
mu	on	k3xPp3gMnSc3	on
chyběl	chybět	k5eAaImAgInS	chybět
jeho	jeho	k3xOp3gInSc1	jeho
osobitý	osobitý	k2eAgInSc1d1	osobitý
styl	styl	k1gInSc1	styl
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
pyšní	pyšnit	k5eAaImIp3nS	pyšnit
každý	každý	k3xTgMnSc1	každý
velký	velký	k2eAgMnSc1d1	velký
kytarový	kytarový	k2eAgMnSc1d1	kytarový
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
hrál	hrát	k5eAaImAgInS	hrát
Hendrix	Hendrix	k1gInSc1	Hendrix
v	v	k7c6	v
klubech	klub	k1gInPc6	klub
tzv.	tzv.	kA	tzv.
Chitlin	Chitlin	k2eAgMnSc1d1	Chitlin
<g/>
'	'	kIx"	'
Circuitu	Circuita	k1gFnSc4	Circuita
(	(	kIx(	(
<g/>
černošské	černošský	k2eAgInPc1d1	černošský
kluby	klub	k1gInPc1	klub
táhnoucí	táhnoucí	k2eAgMnPc4d1	táhnoucí
se	se	k3xPyFc4	se
jižanskými	jižanský	k2eAgInPc7d1	jižanský
státy	stát	k1gInPc7	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgInS	hrát
buď	buď	k8xC	buď
s	s	k7c7	s
King	Kinga	k1gFnPc2	Kinga
Kasuals	Kasualsa	k1gFnPc2	Kasualsa
(	(	kIx(	(
<g/>
kapelu	kapela	k1gFnSc4	kapela
tehdy	tehdy	k6eAd1	tehdy
tvořili	tvořit	k5eAaImAgMnP	tvořit
mj.	mj.	kA	mj.
i	i	k8xC	i
dechová	dechový	k2eAgFnSc1d1	dechová
sekce	sekce	k1gFnSc1	sekce
a	a	k8xC	a
baviče	bavič	k1gMnSc4	bavič
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
promlouval	promlouvat	k5eAaImAgMnS	promlouvat
k	k	k7c3	k
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
před	před	k7c7	před
samotným	samotný	k2eAgNnSc7d1	samotné
vystoupením	vystoupení	k1gNnSc7	vystoupení
kapely	kapela	k1gFnSc2	kapela
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
nájemný	nájemný	k2eAgMnSc1d1	nájemný
kytarista	kytarista	k1gMnSc1	kytarista
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
mu	on	k3xPp3gMnSc3	on
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
pouze	pouze	k6eAd1	pouze
nájemný	nájemný	k2eAgMnSc1d1	nájemný
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nezastává	zastávat	k5eNaImIp3nS	zastávat
zvlášť	zvlášť	k6eAd1	zvlášť
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
svou	svůj	k3xOyFgFnSc7	svůj
hrou	hra	k1gFnSc7	hra
a	a	k8xC	a
divokými	divoký	k2eAgInPc7d1	divoký
jevištními	jevištní	k2eAgInPc7d1	jevištní
kousky	kousek	k1gInPc7	kousek
poutal	poutat	k5eAaImAgInS	poutat
více	hodně	k6eAd2	hodně
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
,	,	kIx,	,
než	než	k8xS	než
zbytek	zbytek	k1gInSc1	zbytek
kapely	kapela	k1gFnSc2	kapela
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
pochopitelně	pochopitelně	k6eAd1	pochopitelně
zbytku	zbytek	k1gInSc2	zbytek
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
především	především	k9	především
frontmanům	frontman	k1gMnPc3	frontman
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Hendrix	Hendrix	k1gInSc1	Hendrix
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
kapele	kapela	k1gFnSc6	kapela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
najala	najmout	k5eAaPmAgFnS	najmout
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
dlouho	dlouho	k6eAd1	dlouho
nevydržel	vydržet	k5eNaPmAgMnS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Hendrix	Hendrix	k1gInSc1	Hendrix
naučil	naučit	k5eAaPmAgInS	naučit
i	i	k9	i
věcem	věc	k1gFnPc3	věc
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
s	s	k7c7	s
kytarovou	kytarový	k2eAgFnSc7d1	kytarová
hrou	hra	k1gFnSc7	hra
už	už	k6eAd1	už
moc	moc	k6eAd1	moc
nesouvisí	souviset	k5eNaImIp3nS	souviset
-	-	kIx~	-
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
začal	začít	k5eAaPmAgInS	začít
sem	sem	k6eAd1	sem
tam	tam	k6eAd1	tam
pít	pít	k5eAaImF	pít
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
po	po	k7c6	po
koncertech	koncert	k1gInPc6	koncert
flirtovat	flirtovat	k5eAaImF	flirtovat
s	s	k7c7	s
děvčaty	děvče	k1gNnPc7	děvče
(	(	kIx(	(
<g/>
láska	láska	k1gFnSc1	láska
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
Betty	Betty	k1gFnSc1	Betty
Jean	Jean	k1gMnSc1	Jean
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
zapomenuta	zapomnět	k5eAaImNgNnP	zapomnět
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prodal	prodat	k5eAaPmAgInS	prodat
Hendrix	Hendrix	k1gInSc1	Hendrix
do	do	k7c2	do
zastavárny	zastavárna	k1gFnSc2	zastavárna
a	a	k8xC	a
koupil	koupit	k5eAaPmAgMnS	koupit
si	se	k3xPyFc3	se
novou	nový	k2eAgFnSc4d1	nová
<g/>
)	)	kIx)	)
a	a	k8xC	a
stával	stávat	k5eAaImAgInS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
celkově	celkově	k6eAd1	celkově
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
větší	veliký	k2eAgInSc4d2	veliký
šoumen	šoumen	k1gInSc4	šoumen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
projížděl	projíždět	k5eAaImAgInS	projíždět
Nashvillem	Nashvill	k1gMnSc7	Nashvill
promotér	promotér	k1gMnSc1	promotér
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
příležitosti	příležitost	k1gFnSc3	příležitost
chytil	chytit	k5eAaPmAgMnS	chytit
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
odcestovat	odcestovat	k5eAaPmF	odcestovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
z	z	k7c2	z
Nashvillu	Nashvill	k1gInSc2	Nashvill
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
přemluvit	přemluvit	k5eAaPmF	přemluvit
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
King	King	k1gMnSc1	King
Kasuals	Kasuals	k1gInSc1	Kasuals
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
před	před	k7c7	před
zastřelením	zastřelení	k1gNnSc7	zastřelení
JFK	JFK	kA	JFK
si	se	k3xPyFc3	se
sedl	sednout	k5eAaPmAgMnS	sednout
na	na	k7c4	na
zadní	zadní	k2eAgNnPc4d1	zadní
sedadlo	sedadlo	k1gNnSc4	sedadlo
dálkového	dálkový	k2eAgInSc2d1	dálkový
autobusu	autobus	k1gInSc2	autobus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
museli	muset	k5eAaImAgMnP	muset
černoši	černoch	k1gMnPc1	černoch
povinně	povinně	k6eAd1	povinně
sedávat	sedávat	k5eAaImF	sedávat
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
NY	NY	kA	NY
Hendrix	Hendrix	k1gInSc1	Hendrix
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
promotér	promotér	k1gMnSc1	promotér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gInSc4	on
přiměl	přimět	k5eAaPmAgMnS	přimět
k	k	k7c3	k
odjezdu	odjezd	k1gInSc3	odjezd
z	z	k7c2	z
Nashvillu	Nashvill	k1gInSc2	Nashvill
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
tlučhuba	tlučhuba	k1gMnSc1	tlučhuba
a	a	k8xC	a
slibovanou	slibovaný	k2eAgFnSc4d1	slibovaná
práci	práce	k1gFnSc4	práce
Jimimu	Jimima	k1gFnSc4	Jimima
nesehnal	sehnat	k5eNaPmAgMnS	sehnat
<g/>
.	.	kIx.	.
</s>
<s>
Usadil	usadit	k5eAaPmAgInS	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
Harlemu	Harlem	k1gInSc6	Harlem
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
v	v	k7c6	v
NY	NY	kA	NY
nikoho	nikdo	k3yNnSc4	nikdo
neznal	neznat	k5eAaImAgMnS	neznat
<g/>
,	,	kIx,	,
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
mu	on	k3xPp3gMnSc3	on
tedy	tedy	k9	tedy
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
pokoušet	pokoušet	k5eAaImF	pokoušet
najít	najít	k5eAaPmF	najít
kapelu	kapela	k1gFnSc4	kapela
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
nebylo	být	k5eNaImAgNnS	být
vůbec	vůbec	k9	vůbec
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
newyorské	newyorský	k2eAgFnSc2d1	newyorská
scény	scéna	k1gFnSc2	scéna
bylo	být	k5eAaImAgNnS	být
navzdory	navzdory	k7c3	navzdory
její	její	k3xOp3gFnSc3	její
velikosti	velikost	k1gFnSc3	velikost
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgFnPc1d1	těžká
proniknout	proniknout	k5eAaPmF	proniknout
<g/>
.	.	kIx.	.
</s>
<s>
Jimimu	Jimim	k1gInSc3	Jimim
nemohla	moct	k5eNaImAgFnS	moct
vyhovovat	vyhovovat	k5eAaImF	vyhovovat
ani	ani	k8xC	ani
žánrová	žánrový	k2eAgFnSc1d1	žánrová
úzkoprsost	úzkoprsost	k1gFnSc1	úzkoprsost
harlemské	harlemský	k2eAgFnSc2d1	harlemský
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgFnSc1d1	tamní
černošská	černošský	k2eAgFnSc1d1	černošská
populace	populace	k1gFnSc1	populace
nechtěla	chtít	k5eNaImAgFnS	chtít
slyšet	slyšet	k5eAaImF	slyšet
"	"	kIx"	"
<g/>
bílý	bílý	k2eAgInSc4d1	bílý
<g/>
"	"	kIx"	"
rokenrol	rokenrol	k1gInSc4	rokenrol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
své	svůj	k3xOyFgNnSc4	svůj
"	"	kIx"	"
<g/>
černé	černé	k1gNnSc4	černé
<g/>
"	"	kIx"	"
žánry	žánr	k1gInPc7	žánr
-	-	kIx~	-
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
blues	blues	k1gInSc1	blues
a	a	k8xC	a
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B.	B.	kA	B.
První	první	k4xOgFnSc7	první
newyorskou	newyorský	k2eAgFnSc7d1	newyorská
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
navázal	navázat	k5eAaPmAgInS	navázat
Hendrix	Hendrix	k1gInSc1	Hendrix
vztah	vztah	k1gInSc4	vztah
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Lithofayne	Lithofayn	k1gInSc5	Lithofayn
Pridgeonová	Pridgeonová	k1gFnSc1	Pridgeonová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
znala	znát	k5eAaImAgFnS	znát
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
Aleemovými	Aleemův	k2eAgMnPc7d1	Aleemův
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
tři	tři	k4xCgMnPc1	tři
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
prvními	první	k4xOgMnPc7	první
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
si	se	k3xPyFc3	se
Jimi	on	k3xPp3gInPc7	on
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
novém	nový	k2eAgNnSc6d1	nové
městě	město	k1gNnSc6	město
našel	najít	k5eAaPmAgMnS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Aleemovi	Aleemův	k2eAgMnPc1d1	Aleemův
se	se	k3xPyFc4	se
živili	živit	k5eAaImAgMnP	živit
distribucí	distribuce	k1gFnSc7	distribuce
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
otázkou	otázka	k1gFnSc7	otázka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stejnou	stejný	k2eAgFnSc4d1	stejná
možnost	možnost	k1gFnSc4	možnost
přivýdělku	přivýdělek	k1gInSc2	přivýdělek
nabídnou	nabídnout	k5eAaPmIp3nP	nabídnout
i	i	k9	i
Jimimu	Jimima	k1gFnSc4	Jimima
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
tehdy	tehdy	k6eAd1	tehdy
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
věrný	věrný	k2eAgMnSc1d1	věrný
svému	svůj	k3xOyFgNnSc3	svůj
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
živit	živit	k5eAaImF	živit
pouze	pouze	k6eAd1	pouze
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
Hendrixových	Hendrixový	k2eAgNnPc2d1	Hendrixový
prvních	první	k4xOgNnPc2	první
"	"	kIx"	"
<g/>
hudebních	hudební	k2eAgInPc2d1	hudební
<g/>
"	"	kIx"	"
zaměstnání	zaměstnání	k1gNnSc1	zaměstnání
v	v	k7c6	v
NY	NY	kA	NY
bylo	být	k5eAaImAgNnS	být
doprovázení	doprovázení	k1gNnSc1	doprovázení
striptérky	striptérka	k1gFnSc2	striptérka
s	s	k7c7	s
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
jménem	jméno	k1gNnSc7	jméno
Pantera	panter	k1gMnSc2	panter
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
ovšem	ovšem	k9	ovšem
pochopitelně	pochopitelně	k6eAd1	pochopitelně
neuspokojovalo	uspokojovat	k5eNaImAgNnS	uspokojovat
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1964	[number]	k4	1964
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bytě	byt	k1gInSc6	byt
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Isley	Islea	k1gFnSc2	Islea
Brothers	Brothers	k1gInSc1	Brothers
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hledala	hledat	k5eAaImAgFnS	hledat
kytaristu	kytarista	k1gMnSc4	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
jej	on	k3xPp3gMnSc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
a	a	k8xC	a
poté	poté	k6eAd1	poté
spolu	spolu	k6eAd1	spolu
všichni	všechen	k3xTgMnPc1	všechen
sledovali	sledovat	k5eAaImAgMnP	sledovat
legendární	legendární	k2eAgNnSc4d1	legendární
vystoupení	vystoupení	k1gNnSc4	vystoupení
The	The	k1gFnSc2	The
Beatles	Beatles	k1gFnPc2	Beatles
v	v	k7c6	v
Ed	Ed	k1gFnSc6	Ed
Sullivan	Sullivan	k1gMnSc1	Sullivan
Show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
začalo	začít	k5eAaPmAgNnS	začít
Jimimu	Jimima	k1gFnSc4	Jimima
vadit	vadit	k5eAaImF	vadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
souboru	soubor	k1gInSc6	soubor
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
a	a	k8xC	a
tak	tak	k6eAd1	tak
skupinu	skupina	k1gFnSc4	skupina
během	během	k7c2	během
jejího	její	k3xOp3gNnSc2	její
turné	turné	k1gNnSc2	turné
v	v	k7c6	v
Nashvillu	Nashvill	k1gInSc6	Nashvill
opustil	opustit	k5eAaPmAgMnS	opustit
a	a	k8xC	a
našel	najít	k5eAaPmAgMnS	najít
si	se	k3xPyFc3	se
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
formace	formace	k1gFnSc2	formace
Georgeous	Georgeous	k1gMnSc1	Georgeous
George	Georg	k1gMnSc4	Georg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
střídal	střídat	k5eAaImAgMnS	střídat
Hendrix	Hendrix	k1gInSc4	Hendrix
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xS	jako
na	na	k7c6	na
běžícím	běžící	k2eAgInSc6d1	běžící
pásu	pás	k1gInSc6	pás
a	a	k8xC	a
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
nevzpomněl	vzpomnít	k5eNaPmAgMnS	vzpomnít
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
ale	ale	k9	ale
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1964	[number]	k4	1964
si	se	k3xPyFc3	se
našel	najít	k5eAaPmAgMnS	najít
další	další	k2eAgNnSc4d1	další
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
u	u	k7c2	u
kapely	kapela	k1gFnSc2	kapela
The	The	k1gFnSc2	The
Upsetters	Upsettersa	k1gFnPc2	Upsettersa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
Little	Little	k1gFnPc4	Little
Richarda	Richard	k1gMnSc2	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
později	pozdě	k6eAd2	pozdě
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
jako	jako	k9	jako
na	na	k7c4	na
pedanta	pedant	k1gMnSc4	pedant
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dával	dávat	k5eAaImAgMnS	dávat
pokuty	pokuta	k1gFnPc4	pokuta
i	i	k9	i
za	za	k7c4	za
rozvázané	rozvázaný	k2eAgFnPc4d1	rozvázaná
tkaničky	tkanička	k1gFnPc4	tkanička
během	během	k7c2	během
vystoupení	vystoupení	k1gNnSc2	vystoupení
a	a	k8xC	a
egoistu	egoista	k1gMnSc4	egoista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pokaždé	pokaždé	k6eAd1	pokaždé
zuřil	zuřit	k5eAaImAgMnS	zuřit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Hendrix	Hendrix	k1gInSc1	Hendrix
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInPc4	jeho
zákazy	zákaz	k1gInPc4	zákaz
prováděl	provádět	k5eAaImAgInS	provádět
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
své	svůj	k3xOyFgInPc4	svůj
kousky	kousek	k1gInPc4	kousek
jako	jako	k8xS	jako
hraní	hranit	k5eAaImIp3nS	hranit
zuby	zub	k1gInPc4	zub
apod.	apod.	kA	apod.
Hendrix	Hendrix	k1gInSc1	Hendrix
si	se	k3xPyFc3	se
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
lezli	lézt	k5eAaImAgMnP	lézt
na	na	k7c4	na
nervy	nerv	k1gInPc4	nerv
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
Jimiho	Jimi	k1gMnSc2	Jimi
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
The	The	k1gFnSc6	The
Upseters	Upseters	k1gInSc4	Upseters
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
otázkou	otázka	k1gFnSc7	otázka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dá	dát	k5eAaPmIp3nS	dát
této	tento	k3xDgFnSc3	tento
skupině	skupina	k1gFnSc3	skupina
Jimi	on	k3xPp3gInPc7	on
sbohem	sbohem	k0	sbohem
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
se	se	k3xPyFc4	se
necelém	celý	k2eNgInSc6d1	necelý
roce	rok	k1gInSc6	rok
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
s	s	k7c7	s
všemožnými	všemožný	k2eAgFnPc7d1	všemožná
kapelami	kapela	k1gFnPc7	kapela
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Harlemu	Harlem	k1gInSc2	Harlem
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
opět	opět	k6eAd1	opět
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
Isley	Isle	k1gMnPc7	Isle
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
.	.	kIx.	.
</s>
<s>
Několika	několik	k4yIc3	několik
studiím	studio	k1gNnPc3	studio
se	se	k3xPyFc4	se
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
jako	jako	k9	jako
studiový	studiový	k2eAgMnSc1d1	studiový
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
se	se	k3xPyFc4	se
s	s	k7c7	s
labelem	label	k1gMnSc7	label
Sue	Sue	k1gMnSc7	Sue
Records	Records	k1gInSc4	Records
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
a	a	k8xC	a
kromě	kromě	k7c2	kromě
pár	pár	k4xCyI	pár
studiových	studiový	k2eAgFnPc2d1	studiová
frekvencí	frekvence	k1gFnPc2	frekvence
nezanechal	zanechat	k5eNaPmAgInS	zanechat
v	v	k7c6	v
Hendrixově	Hendrixově	k1gFnSc6	Hendrixově
životě	život	k1gInSc6	život
výraznější	výrazný	k2eAgFnSc4d2	výraznější
stopu	stopa	k1gFnSc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1965	[number]	k4	1965
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
Curtise	Curtise	k1gFnSc1	Curtise
Knighta	Knighta	k1gMnSc1	Knighta
The	The	k1gMnSc1	The
Squires	Squires	k1gMnSc1	Squires
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgFnPc3d1	finanční
potížím	potíž	k1gFnPc3	potíž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Hendrixova	Hendrixův	k2eAgFnSc1d1	Hendrixova
vlastní	vlastní	k2eAgFnSc1d1	vlastní
kytara	kytara	k1gFnSc1	kytara
v	v	k7c6	v
zastavárně	zastavárna	k1gFnSc6	zastavárna
a	a	k8xC	a
Knight	Knight	k1gMnSc1	Knight
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
chytře	chytro	k6eAd1	chytro
zavázal	zavázat	k5eAaPmAgMnS	zavázat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
půjčil	půjčit	k5eAaPmAgMnS	půjčit
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
kytar	kytara	k1gFnPc2	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
Hendrixe	Hendrixe	k1gFnSc2	Hendrixe
frontmana	frontman	k1gMnSc2	frontman
a	a	k8xC	a
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Jimi	on	k3xPp3gInPc7	on
hlavním	hlavní	k2eAgInSc7d1	hlavní
tahákem	tahák	k1gInSc7	tahák
The	The	k1gMnSc1	The
Squires	Squires	k1gMnSc1	Squires
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgInS	opustit
ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mnoho	mnoho	k4c1	mnoho
kapel	kapela	k1gFnPc2	kapela
předtím	předtím	k6eAd1	předtím
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
tvůrčí	tvůrčí	k2eAgFnSc1d1	tvůrčí
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
uniformita	uniformita	k1gFnSc1	uniformita
atd.	atd.	kA	atd.
Našel	najít	k5eAaPmAgMnS	najít
si	se	k3xPyFc3	se
další	další	k2eAgNnSc4d1	další
angažmá	angažmá	k1gNnSc4	angažmá
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Joey	Joea	k1gMnSc2	Joea
Dee	Dee	k1gMnSc2	Dee
and	and	k?	and
The	The	k1gFnSc2	The
Starliters	Starlitersa	k1gFnPc2	Starlitersa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgInS	být
přechod	přechod	k1gInSc4	přechod
za	za	k7c7	za
lepším	dobrý	k2eAgMnSc7d2	lepší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
písnička	písnička	k1gFnSc1	písnička
Peppermint	Pepperminta	k1gFnPc2	Pepperminta
Twist	twist	k1gInSc4	twist
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
okupovala	okupovat	k5eAaBmAgFnS	okupovat
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
první	první	k4xOgMnSc1	první
místo	místo	k7c2	místo
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šedesáti	šedesát	k4xCc2	šedesát
dnů	den	k1gInPc2	den
odehrál	odehrát	k5eAaPmAgInS	odehrát
Jimi	on	k3xPp3gInPc7	on
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
padesát	padesát	k4xCc1	padesát
osm	osm	k4xCc1	osm
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
pro	pro	k7c4	pro
až	až	k9	až
desetitisícový	desetitisícový	k2eAgInSc4d1	desetitisícový
dav	dav	k1gInSc4	dav
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
největší	veliký	k2eAgNnSc1d3	veliký
publikum	publikum	k1gNnSc1	publikum
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgMnPc4	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hrál	hrát	k5eAaImAgMnS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
rasově	rasův	k2eAgFnSc6d1	rasova
smíšené	smíšený	k2eAgFnSc6d1	smíšená
kapele	kapela	k1gFnSc6	kapela
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
koncerty	koncert	k1gInPc1	koncert
byly	být	k5eAaImAgInP	být
odehrány	odehrát	k5eAaPmNgInP	odehrát
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
bílé	bílý	k2eAgNnSc4d1	bílé
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
navázal	navázat	k5eAaPmAgMnS	navázat
i	i	k9	i
vážnější	vážní	k2eAgInSc4d2	vážnější
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
šestnáctiletou	šestnáctiletý	k2eAgFnSc7d1	šestnáctiletá
prostitutkou	prostitutka	k1gFnSc7	prostitutka
Dianou	Diana	k1gFnSc7	Diana
Carpenterovou	Carpenterův	k2eAgFnSc7d1	Carpenterova
<g/>
.	.	kIx.	.
</s>
<s>
Carpenterová	Carpenterová	k1gFnSc1	Carpenterová
měla	mít	k5eAaImAgFnS	mít
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
profesi	profes	k1gFnSc3	profes
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
výdělky	výdělek	k1gInPc7	výdělek
než	než	k8xS	než
Hendrix	Hendrix	k1gInSc1	Hendrix
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ho	on	k3xPp3gNnSc4	on
začala	začít	k5eAaPmAgFnS	začít
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
živit	živit	k5eAaImF	živit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nějakém	nějaký	k3yIgInSc6	nějaký
čase	čas	k1gInSc6	čas
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
chtěl	chtít	k5eAaImAgInS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přestala	přestat	k5eAaPmAgFnS	přestat
prodávat	prodávat	k5eAaImF	prodávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Carpenterová	Carpenterový	k2eAgFnSc1d1	Carpenterový
za	za	k7c7	za
jeho	jeho	k3xOp3gNnPc7	jeho
zády	záda	k1gNnPc7	záda
prodávala	prodávat	k5eAaImAgNnP	prodávat
své	svůj	k3xOyFgNnSc4	svůj
tělo	tělo	k1gNnSc4	tělo
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
Hendrix	Hendrix	k1gInSc4	Hendrix
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
zmlátil	zmlátit	k5eAaPmAgMnS	zmlátit
svou	svůj	k3xOyFgFnSc4	svůj
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
řemenem	řemen	k1gInSc7	řemen
<g/>
.	.	kIx.	.
</s>
<s>
Násilnické	násilnický	k2eAgInPc1d1	násilnický
sklony	sklon	k1gInPc1	sklon
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
neprojevovaly	projevovat	k5eNaImAgFnP	projevovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
spíše	spíše	k9	spíše
prchlivé	prchlivý	k2eAgFnPc4d1	prchlivá
povahy	povaha	k1gFnPc4	povaha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
tzv.	tzv.	kA	tzv.
bouchly	bouchnout	k5eAaPmAgFnP	bouchnout
saze	saze	k1gFnPc4	saze
<g/>
,	,	kIx,	,
stálo	stát	k5eAaImAgNnS	stát
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
hroutit	hroutit	k5eAaImF	hroutit
a	a	k8xC	a
Carpenterová	Carpenterová	k1gFnSc1	Carpenterová
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
před	před	k7c7	před
pár	pár	k4xCyI	pár
měsíci	měsíc	k1gInPc7	měsíc
utekla	utéct	k5eAaPmAgFnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1967	[number]	k4	1967
porodila	porodit	k5eAaPmAgFnS	porodit
dceru	dcera	k1gFnSc4	dcera
jménem	jméno	k1gNnSc7	jméno
Tamika	Tamik	k1gMnSc2	Tamik
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
mulatka	mulatka	k1gFnSc1	mulatka
a	a	k8xC	a
Carpenterová	Carpenterová	k1gFnSc1	Carpenterová
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
pleť	pleť	k1gFnSc4	pleť
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
prodávala	prodávat	k5eAaImAgFnS	prodávat
výhradně	výhradně	k6eAd1	výhradně
bělochům	běloch	k1gMnPc3	běloch
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
vše	všechen	k3xTgNnSc1	všechen
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
na	na	k7c4	na
Hendrixe	Hendrixe	k1gFnPc4	Hendrixe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
narození	narození	k1gNnSc2	narození
Tamiky	Tamika	k1gFnSc2	Tamika
neměla	mít	k5eNaImAgFnS	mít
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
na	na	k7c4	na
Hendrixe	Hendrixe	k1gFnPc4	Hendrixe
už	už	k6eAd1	už
žádný	žádný	k3yNgInSc4	žádný
kontakt	kontakt	k1gInSc4	kontakt
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
proslavení	proslavení	k1gNnSc6	proslavení
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
potkala	potkat	k5eAaPmAgFnS	potkat
a	a	k8xC	a
ukázala	ukázat	k5eAaPmAgFnS	ukázat
mu	on	k3xPp3gMnSc3	on
fotku	fotka	k1gFnSc4	fotka
jejich	jejich	k3xOp3gFnSc2	jejich
dcery	dcera	k1gFnSc2	dcera
<g/>
,	,	kIx,	,
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
ale	ale	k8xC	ale
Hendrix	Hendrix	k1gInSc4	Hendrix
Tamiku	Tamik	k1gInSc2	Tamik
nikdy	nikdy	k6eAd1	nikdy
nespatřil	spatřit	k5eNaPmAgInS	spatřit
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
dál	daleko	k6eAd2	daleko
zkoušel	zkoušet	k5eAaImAgInS	zkoušet
štěstí	štěstí	k1gNnSc4	štěstí
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
uskupeních	uskupení	k1gNnPc6	uskupení
<g/>
,	,	kIx,	,
v	v	k7c6	v
žádném	žádný	k3yNgNnSc6	žádný
ale	ale	k9	ale
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
výraznější	výrazný	k2eAgFnSc4d2	výraznější
stopu	stopa	k1gFnSc4	stopa
a	a	k8xC	a
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
kapel	kapela	k1gFnPc2	kapela
nezanechala	zanechat	k5eNaPmAgFnS	zanechat
výraznější	výrazný	k2eAgFnSc4d2	výraznější
stopu	stopa	k1gFnSc4	stopa
v	v	k7c4	v
Jimim	Jimim	k1gInSc4	Jimim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
jara	jaro	k1gNnSc2	jaro
1966	[number]	k4	1966
jej	on	k3xPp3gNnSc4	on
muzikant	muzikant	k1gMnSc1	muzikant
Richie	Richie	k1gFnSc2	Richie
Havens	Havens	k1gInSc1	Havens
zavedl	zavést	k5eAaPmAgInS	zavést
do	do	k7c2	do
newyorské	newyorský	k2eAgFnSc2d1	newyorská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Villag	k1gFnSc2	Villag
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
Hendrixově	Hendrixově	k1gFnSc1	Hendrixově
"	"	kIx"	"
<g/>
bělošskému	bělošský	k2eAgMnSc3d1	bělošský
<g/>
"	"	kIx"	"
rocku	rock	k1gInSc3	rock
mnohem	mnohem	k6eAd1	mnohem
otevřenější	otevřený	k2eAgMnSc1d2	otevřenější
<g/>
,	,	kIx,	,
než	než	k8xS	než
Harlem	Harl	k1gInSc7	Harl
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
Hendrixův	Hendrixův	k2eAgInSc4d1	Hendrixův
další	další	k2eAgInSc4d1	další
život	život	k1gInSc4	život
zdejší	zdejší	k2eAgInSc4d1	zdejší
hudební	hudební	k2eAgInSc4d1	hudební
klub	klub	k1gInSc4	klub
Café	café	k1gNnSc2	café
Wha	Wha	k1gFnSc2	Wha
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
mimochodem	mimochodem	k6eAd1	mimochodem
dostal	dostat	k5eAaPmAgMnS	dostat
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
prvních	první	k4xOgFnPc2	první
příležitostí	příležitost	k1gFnPc2	příležitost
i	i	k9	i
velký	velký	k2eAgInSc4d1	velký
Hendrixův	Hendrixův	k2eAgInSc4d1	Hendrixův
vzor	vzor	k1gInSc4	vzor
Bob	Bob	k1gMnSc1	Bob
Dylan	Dylan	k1gMnSc1	Dylan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
zde	zde	k6eAd1	zde
začal	začít	k5eAaPmAgInS	začít
pravidelně	pravidelně	k6eAd1	pravidelně
vystupovat	vystupovat	k5eAaImF	vystupovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
skupinou	skupina	k1gFnSc7	skupina
Jimmy	Jimma	k1gFnSc2	Jimma
James	James	k1gInSc1	James
and	and	k?	and
The	The	k1gFnSc2	The
Blue	Blue	k1gNnSc2	Blue
Flames	Flamesa	k1gFnPc2	Flamesa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
pár	pár	k4xCyI	pár
dnů	den	k1gInPc2	den
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příchodu	příchod	k1gInSc6	příchod
do	do	k7c2	do
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Village	k1gFnSc4	Village
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
lze	lze	k6eAd1	lze
vyčíst	vyčíst	k5eAaPmF	vyčíst
z	z	k7c2	z
názvu	název	k1gInSc2	název
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
Hendrix	Hendrix	k1gInSc1	Hendrix
tehdy	tehdy	k6eAd1	tehdy
používal	používat	k5eAaImAgInS	používat
umělecké	umělecký	k2eAgNnSc4d1	umělecké
jméno	jméno	k1gNnSc4	jméno
Jimmy	Jimma	k1gFnSc2	Jimma
James	Jamesa	k1gFnPc2	Jamesa
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
pod	pod	k7c7	pod
jmény	jméno	k1gNnPc7	jméno
jako	jako	k8xC	jako
Jimmy	Jimm	k1gInPc7	Jimm
Jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gMnPc3	on
James	James	k1gMnSc1	James
apod.	apod.	kA	apod.
Hrál	hrát	k5eAaImAgMnS	hrát
především	především	k6eAd1	především
coververze	coververze	k1gFnSc2	coververze
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Dylanovu	Dylanův	k2eAgFnSc4d1	Dylanova
Like	Like	k1gFnSc4	Like
a	a	k8xC	a
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
hrál	hrát	k5eAaImAgInS	hrát
i	i	k9	i
zárodky	zárodek	k1gInPc4	zárodek
skladeb	skladba	k1gFnPc2	skladba
Third	Thirda	k1gFnPc2	Thirda
Stone	ston	k1gInSc5	ston
from	fro	k1gNnSc7	fro
the	the	k?	the
Sun	Sun	kA	Sun
a	a	k8xC	a
Foxy	fox	k1gInPc1	fox
Lady	Lada	k1gFnSc2	Lada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tyto	tento	k3xDgFnPc1	tento
informace	informace	k1gFnPc1	informace
nejsou	být	k5eNaImIp3nP	být
potvrzené	potvrzený	k2eAgFnPc1d1	potvrzená
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vystupováním	vystupování	k1gNnSc7	vystupování
si	se	k3xPyFc3	se
Hendrix	Hendrix	k1gInSc1	Hendrix
vydělával	vydělávat	k5eAaImAgInS	vydělávat
10	[number]	k4	10
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
večer	večer	k1gInSc4	večer
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
vystoupeních	vystoupení	k1gNnPc6	vystoupení
Jimi	on	k3xPp3gInPc7	on
také	také	k9	také
velmi	velmi	k6eAd1	velmi
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
s	s	k7c7	s
fuzz	fuzz	k1gInSc1	fuzz
boxem	box	k1gInSc7	box
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
vazbou	vazba	k1gFnSc7	vazba
<g/>
,	,	kIx,	,
přetíženým	přetížený	k2eAgInSc7d1	přetížený
zesilovačem	zesilovač	k1gInSc7	zesilovač
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
Hendrixovou	Hendrixový	k2eAgFnSc7d1	Hendrixová
hrou	hra	k1gFnSc7	hra
dokázal	dokázat	k5eAaPmAgMnS	dokázat
tvořit	tvořit	k5eAaImF	tvořit
velmi	velmi	k6eAd1	velmi
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
byl	být	k5eAaImAgInS	být
Hendrix	Hendrix	k1gInSc1	Hendrix
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
průkopníků	průkopník	k1gMnPc2	průkopník
těchto	tento	k3xDgFnPc2	tento
zvukových	zvukový	k2eAgFnPc2d1	zvuková
"	"	kIx"	"
<g/>
vychytávek	vychytávka	k1gFnPc2	vychytávka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
docela	docela	k6eAd1	docela
běžnou	běžný	k2eAgFnSc7d1	běžná
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zcela	zcela	k6eAd1	zcela
osamostatnil	osamostatnit	k5eAaPmAgInS	osamostatnit
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Greenwich	Greenwich	k1gInSc4	Greenwich
Village	Village	k1gNnSc2	Village
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
klubovém	klubový	k2eAgNnSc6d1	klubové
vystoupení	vystoupení	k1gNnSc6	vystoupení
všiml	všimnout	k5eAaPmAgMnS	všimnout
baskytarista	baskytarista	k1gMnSc1	baskytarista
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnPc2	The
Animals	Animals	k1gInSc1	Animals
Chas	chasa	k1gFnPc2	chasa
Chandler	Chandler	k1gMnSc1	Chandler
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
role	role	k1gFnSc2	role
Hendrixova	Hendrixův	k2eAgMnSc2d1	Hendrixův
manažera	manažer	k1gMnSc2	manažer
a	a	k8xC	a
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
Jimi	on	k3xPp3gInPc7	on
opustil	opustit	k5eAaPmAgInS	opustit
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
a	a	k8xC	a
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
změnil	změnit	k5eAaPmAgMnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vkročil	vkročit	k5eAaPmAgInS	vkročit
na	na	k7c4	na
evropskou	evropský	k2eAgFnSc4d1	Evropská
půdu	půda	k1gFnSc4	půda
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1966	[number]	k4	1966
na	na	k7c6	na
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
letišti	letiště	k1gNnSc6	letiště
Heathrow	Heathrow	k1gFnSc2	Heathrow
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
měsíc	měsíc	k1gInSc4	měsíc
hrál	hrát	k5eAaImAgMnS	hrát
sám	sám	k3xTgMnSc1	sám
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
londýnských	londýnský	k2eAgInPc6d1	londýnský
klubech	klub	k1gInPc6	klub
<g/>
,	,	kIx,	,
povětšinou	povětšinou	k6eAd1	povětšinou
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
bluesové	bluesový	k2eAgInPc4d1	bluesový
jamy	jam	k1gInPc4	jam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
Ericem	Eriec	k1gInSc7	Eriec
Claptonem	Clapton	k1gInSc7	Clapton
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Who	Who	k1gMnPc2	Who
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
umělci	umělec	k1gMnPc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1966	[number]	k4	1966
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
skupina	skupina	k1gFnSc1	skupina
The	The	k1gFnSc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
bubeníkem	bubeník	k1gMnSc7	bubeník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Mitch	Mitch	k1gInSc1	Mitch
Mitchell	Mitchella	k1gFnPc2	Mitchella
a	a	k8xC	a
baskytary	baskytara	k1gFnSc2	baskytara
se	se	k3xPyFc4	se
ujmul	ujmout	k5eAaPmAgMnS	ujmout
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
hrající	hrající	k2eAgMnSc1d1	hrající
Noel	Noel	k1gMnSc1	Noel
Redding	Redding	k1gInSc1	Redding
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
premiéru	premiéra	k1gFnSc4	premiéra
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Novelty	Novelt	k1gInPc4	Novelt
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
městě	město	k1gNnSc6	město
Evreux	Evreux	k1gInSc4	Evreux
jako	jako	k8xC	jako
předkapela	předkapela	k1gFnSc1	předkapela
francouzského	francouzský	k2eAgMnSc2d1	francouzský
zpěváka	zpěvák	k1gMnSc2	zpěvák
Johnnyho	Johnny	k1gMnSc2	Johnny
Hallydaye	Hallyday	k1gMnSc2	Hallyday
<g/>
.	.	kIx.	.
</s>
<s>
Hallyday	Hallyday	k1gInPc4	Hallyday
viděl	vidět	k5eAaImAgMnS	vidět
Hendrixe	Hendrixe	k1gFnPc4	Hendrixe
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
londýnských	londýnský	k2eAgInPc2d1	londýnský
klubových	klubový	k2eAgInPc2d1	klubový
jamů	jam	k1gInPc2	jam
a	a	k8xC	a
natolik	natolik	k6eAd1	natolik
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
zapůsobil	zapůsobit	k5eAaPmAgMnS	zapůsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
místo	místo	k7c2	místo
předskokana	předskokan	k1gMnSc2	předskokan
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
francouzském	francouzský	k2eAgNnSc6d1	francouzské
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
Hendrixe	Hendrixe	k1gFnPc4	Hendrixe
ideální	ideální	k2eAgInSc1d1	ideální
začátek	začátek	k1gInSc1	začátek
jeho	jeho	k3xOp3gFnSc2	jeho
evropské	evropský	k2eAgFnSc2d1	Evropská
štace	štace	k1gFnSc2	štace
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
vystoupení	vystoupení	k1gNnSc6	vystoupení
značně	značně	k6eAd1	značně
nesehraná	sehraný	k2eNgFnSc1d1	nesehraná
a	a	k8xC	a
tisk	tisk	k1gInSc1	tisk
označil	označit	k5eAaPmAgInS	označit
Hendrixe	Hendrixe	k1gFnPc4	Hendrixe
za	za	k7c7	za
"	"	kIx"	"
<g/>
nepovedeného	povedený	k2eNgMnSc2d1	nepovedený
křížence	kříženec	k1gMnSc2	kříženec
Jamese	Jamese	k1gFnSc1	Jamese
Browna	Browna	k1gFnSc1	Browna
a	a	k8xC	a
Chucka	Chucka	k1gFnSc1	Chucka
Berryho	Berry	k1gMnSc2	Berry
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
hrála	hrát	k5eAaImAgFnS	hrát
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
už	už	k6eAd1	už
získali	získat	k5eAaPmAgMnP	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
publikum	publikum	k1gNnSc4	publikum
i	i	k8xC	i
hudební	hudební	k2eAgFnSc2d1	hudební
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
složil	složit	k5eAaPmAgInS	složit
Hendrix	Hendrix	k1gInSc1	Hendrix
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Chase	chasa	k1gFnSc3	chasa
Chandlera	Chandler	k1gMnSc2	Chandler
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc4	píseň
Stone	ston	k1gInSc5	ston
Free	Free	k1gFnSc1	Free
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k9	jako
B-strana	Btrana	k1gFnSc1	B-strana
singlu	singl	k1gInSc2	singl
Hey	Hey	k1gFnSc1	Hey
Joe	Joe	k1gFnSc1	Joe
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
do	do	k7c2	do
studiového	studiový	k2eAgNnSc2d1	studiové
nahrávání	nahrávání	k1gNnSc2	nahrávání
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
do	do	k7c2	do
zběsilého	zběsilý	k2eAgNnSc2d1	zběsilé
koncertního	koncertní	k2eAgNnSc2d1	koncertní
tempa	tempo	k1gNnSc2	tempo
-	-	kIx~	-
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
normou	norma	k1gFnSc7	norma
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
přijeli	přijet	k5eAaPmAgMnP	přijet
hrát	hrát	k5eAaImF	hrát
do	do	k7c2	do
nějakého	nějaký	k3yIgInSc2	nějaký
klubu	klub	k1gInSc2	klub
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odehráli	odehrát	k5eAaPmAgMnP	odehrát
zde	zde	k6eAd1	zde
za	za	k7c4	za
večer	večer	k1gInSc4	večer
dvě	dva	k4xCgNnPc4	dva
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
obdobím	období	k1gNnPc3	období
úžasného	úžasný	k2eAgInSc2d1	úžasný
Hendrixova	Hendrixův	k2eAgInSc2d1	Hendrixův
nástupu	nástup	k1gInSc2	nástup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
velké	velká	k1gFnPc4	velká
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Výstředně	výstředně	k6eAd1	výstředně
se	se	k3xPyFc4	se
oblékal	oblékat	k5eAaImAgInS	oblékat
(	(	kIx(	(
<g/>
nosil	nosit	k5eAaImAgInS	nosit
např.	např.	kA	např.
starý	starý	k2eAgInSc1d1	starý
britský	britský	k2eAgInSc1d1	britský
vojenský	vojenský	k2eAgInSc1d1	vojenský
kabátec	kabátec	k1gInSc1	kabátec
se	s	k7c7	s
stále	stále	k6eAd1	stále
připnutými	připnutý	k2eAgNnPc7d1	připnuté
vyznamenáními	vyznamenání	k1gNnPc7	vyznamenání
<g/>
,	,	kIx,	,
pestrobarevné	pestrobarevný	k2eAgFnPc1d1	pestrobarevná
kalhoty	kalhoty	k1gFnPc1	kalhoty
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Současně	současně	k6eAd1	současně
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
vystupováním	vystupování	k1gNnSc7	vystupování
a	a	k8xC	a
především	především	k9	především
hudbou	hudba	k1gFnSc7	hudba
spoluvytvářel	spoluvytvářet	k5eAaImAgInS	spoluvytvářet
atmosféru	atmosféra	k1gFnSc4	atmosféra
konce	konec	k1gInSc2	konec
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
oslňoval	oslňovat	k5eAaImAgMnS	oslňovat
brilantní	brilantní	k2eAgFnSc7d1	brilantní
technikou	technika	k1gFnSc7	technika
a	a	k8xC	a
leckdy	leckdy	k6eAd1	leckdy
až	až	k9	až
cirkusovými	cirkusový	k2eAgInPc7d1	cirkusový
jevištními	jevištní	k2eAgInPc7d1	jevištní
kousky	kousek	k1gInPc7	kousek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hraní	hranit	k5eAaImIp3nS	hranit
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
za	za	k7c7	za
zády	záda	k1gNnPc7	záda
<g/>
,	,	kIx,	,
rozbíjení	rozbíjení	k1gNnSc1	rozbíjení
nástrojů	nástroj	k1gInPc2	nástroj
na	na	k7c6	na
konci	konec	k1gInSc6	konec
koncertu	koncert	k1gInSc2	koncert
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
však	však	k9	však
také	také	k9	také
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
až	až	k9	až
brutální	brutální	k2eAgInPc4d1	brutální
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
různých	různý	k2eAgFnPc2d1	různá
elektronických	elektronický	k2eAgFnPc2d1	elektronická
pomůcek	pomůcka	k1gFnPc2	pomůcka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
nadšenec	nadšenec	k1gMnSc1	nadšenec
a	a	k8xC	a
fanda	fanda	k?	fanda
efektů	efekt	k1gInPc2	efekt
Roger	Roger	k1gMnSc1	Roger
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
debutová	debutový	k2eAgFnSc1d1	debutová
deska	deska	k1gFnSc1	deska
The	The	k1gFnSc2	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
s	s	k7c7	s
názvem	název	k1gInSc7	název
Are	ar	k1gInSc5	ar
You	You	k1gFnPc7	You
Experienced	Experienced	k1gInSc1	Experienced
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
fanoušky	fanoušek	k1gMnPc4	fanoušek
i	i	k9	i
kritiky	kritik	k1gMnPc4	kritik
přijata	přijat	k2eAgFnSc1d1	přijata
velice	velice	k6eAd1	velice
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
měla	mít	k5eAaImAgFnS	mít
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
své	svůj	k3xOyFgFnSc2	svůj
debutové	debutový	k2eAgFnSc2d1	debutová
desky	deska	k1gFnSc2	deska
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
již	již	k6eAd1	již
vcelku	vcelku	k6eAd1	vcelku
zvučné	zvučný	k2eAgNnSc4d1	zvučné
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ale	ale	k8xC	ale
neměnilo	měnit	k5eNaImAgNnS	měnit
nic	nic	k3yNnSc1	nic
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
Atlantiku	Atlantik	k1gInSc2	Atlantik
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
neznámým	známý	k2eNgNnSc7d1	neznámé
uskupením	uskupení	k1gNnSc7	uskupení
<g/>
.	.	kIx.	.
</s>
<s>
Kapele	kapela	k1gFnSc3	kapela
proto	proto	k8xC	proto
přišel	přijít	k5eAaPmAgMnS	přijít
velmi	velmi	k6eAd1	velmi
vhod	vhod	k6eAd1	vhod
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Monterey	Monterea	k1gFnSc2	Monterea
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
akce	akce	k1gFnSc1	akce
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
akci	akce	k1gFnSc6	akce
vystoupilo	vystoupit	k5eAaPmAgNnS	vystoupit
několik	několik	k4yIc1	několik
předních	přední	k2eAgMnPc2d1	přední
světových	světový	k2eAgMnPc2d1	světový
umělců	umělec	k1gMnPc2	umělec
-	-	kIx~	-
mj.	mj.	kA	mj.
Janis	Janis	k1gInSc1	Janis
Joplin	Joplin	k1gInSc1	Joplin
<g/>
,	,	kIx,	,
Otis	Otis	k1gInSc1	Otis
Redding	Redding	k1gInSc1	Redding
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Byrds	Byrds	k1gInSc1	Byrds
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Animals	Animalsa	k1gFnPc2	Animalsa
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
spoluorganizátorů	spoluorganizátor	k1gMnPc2	spoluorganizátor
koncertu	koncert	k1gInSc2	koncert
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
viděl	vidět	k5eAaImAgMnS	vidět
Hendrixe	Hendrixe	k1gFnPc4	Hendrixe
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
londýnském	londýnský	k2eAgInSc6d1	londýnský
klubu	klub	k1gInSc6	klub
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zasadil	zasadit	k5eAaPmAgInS	zasadit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
Jimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
předvedl	předvést	k5eAaPmAgMnS	předvést
skvělý	skvělý	k2eAgInSc4d1	skvělý
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
klasických	klasický	k2eAgInPc2d1	klasický
výjevů	výjev	k1gInPc2	výjev
dějin	dějiny	k1gFnPc2	dějiny
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
-	-	kIx~	-
na	na	k7c6	na
konci	konec	k1gInSc6	konec
písně	píseň	k1gFnSc2	píseň
Wild	Wild	k1gMnSc1	Wild
Thing	Thing	k1gMnSc1	Thing
polil	polít	k5eAaPmAgMnS	polít
svou	svůj	k3xOyFgFnSc4	svůj
kytaru	kytara	k1gFnSc4	kytara
benzínem	benzín	k1gInSc7	benzín
<g/>
,	,	kIx,	,
zapálil	zapálit	k5eAaPmAgMnS	zapálit
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
hořící	hořící	k2eAgFnSc4d1	hořící
kytaru	kytara	k1gFnSc4	kytara
rozbil	rozbít	k5eAaPmAgInS	rozbít
o	o	k7c4	o
zem	zem	k1gFnSc4	zem
na	na	k7c4	na
několik	několik	k4yIc4	několik
kousků	kousek	k1gInPc2	kousek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
pak	pak	k6eAd1	pak
rozdal	rozdat	k5eAaPmAgInS	rozdat
publiku	publikum	k1gNnSc3	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
něco	něco	k6eAd1	něco
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
trvajícím	trvající	k2eAgInSc6d1	trvající
vystoupením	vystoupení	k1gNnSc7	vystoupení
si	se	k3xPyFc3	se
Hendrix	Hendrix	k1gInSc1	Hendrix
získal	získat	k5eAaPmAgInS	získat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
i	i	k8xC	i
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
celosvětovou	celosvětový	k2eAgFnSc7d1	celosvětová
hudební	hudební	k2eAgFnSc7d1	hudební
celebritou	celebrita	k1gFnSc7	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Melody	Meloda	k1gFnSc2	Meloda
Maker	makro	k1gNnPc2	makro
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Jimiho	Jimi	k1gMnSc4	Jimi
Hendrixe	Hendrixe	k1gFnSc1	Hendrixe
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
světového	světový	k2eAgMnSc4d1	světový
umělce	umělec	k1gMnSc4	umělec
pop-music	popusic	k1gFnSc7	pop-music
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1967	[number]	k4	1967
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
kapele	kapela	k1gFnSc3	kapela
jejich	jejich	k3xOp3gNnSc4	jejich
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
Axis	Axisa	k1gFnPc2	Axisa
<g/>
:	:	kIx,	:
Bold	Bold	k1gInSc1	Bold
as	as	k1gNnSc2	as
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Dostáli	dostát	k5eAaPmAgMnP	dostát
tak	tak	k6eAd1	tak
smluvnímu	smluvní	k2eAgInSc3d1	smluvní
závazku	závazek	k1gInSc3	závazek
vůči	vůči	k7c3	vůči
nahrávací	nahrávací	k2eAgFnSc3d1	nahrávací
společnosti	společnost	k1gFnSc3	společnost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yRgMnSc4	který
museli	muset	k5eAaImAgMnP	muset
vydat	vydat	k5eAaPmF	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
dvě	dva	k4xCgNnPc4	dva
studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
ale	ale	k8xC	ale
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Are	ar	k1gInSc5	ar
You	You	k1gFnSc4	You
Experienced	Experienced	k1gInSc4	Experienced
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
1967	[number]	k4	1967
a	a	k8xC	a
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
obávala	obávat	k5eAaImAgFnS	obávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
desky	deska	k1gFnPc4	deska
od	od	k7c2	od
jednoho	jeden	k4xCgMnSc4	jeden
interpreta	interpret	k1gMnSc4	interpret
vzájemně	vzájemně	k6eAd1	vzájemně
"	"	kIx"	"
<g/>
nekradly	krást	k5eNaImAgInP	krást
<g/>
"	"	kIx"	"
kupce	kupec	k1gMnSc4	kupec
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nahrávání	nahrávání	k1gNnSc2	nahrávání
se	se	k3xPyFc4	se
pustili	pustit	k5eAaPmAgMnP	pustit
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
prací	práce	k1gFnPc2	práce
na	na	k7c4	na
Are	ar	k1gInSc5	ar
You	You	k1gMnPc4	You
Experienced	Experienced	k1gInSc4	Experienced
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
celá	celý	k2eAgFnSc1d1	celá
deska	deska	k1gFnSc1	deska
po	po	k7c6	po
všech	všecek	k3xTgInPc6	všecek
směrech	směr	k1gInPc6	směr
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
technicky	technicky	k6eAd1	technicky
propracovanější	propracovaný	k2eAgInSc1d2	propracovanější
-	-	kIx~	-
Hendrix	Hendrix	k1gInSc1	Hendrix
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
pohrál	pohrát	k5eAaPmAgMnS	pohrát
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
kytarovými	kytarový	k2eAgInPc7d1	kytarový
efekty	efekt	k1gInPc7	efekt
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
tzv.	tzv.	kA	tzv.
kvákadlo	kvákadlo	k1gNnSc4	kvákadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Redding	Redding	k1gInSc1	Redding
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
skladbách	skladba	k1gFnPc6	skladba
osmistrunnou	osmistrunný	k2eAgFnSc4d1	osmistrunná
baskytaru	baskytara	k1gFnSc4	baskytara
a	a	k8xC	a
během	během	k7c2	během
první	první	k4xOgFnSc2	první
skladby	skladba	k1gFnSc2	skladba
EXP	exp	kA	exp
přechází	přecházet	k5eAaImIp3nS	přecházet
díky	díky	k7c3	díky
stereu	stereo	k1gNnSc3	stereo
zvuk	zvuk	k1gInSc1	zvuk
mezi	mezi	k7c7	mezi
pravým	pravý	k2eAgInSc7d1	pravý
a	a	k8xC	a
levým	levý	k2eAgInSc7d1	levý
kanálem	kanál	k1gInSc7	kanál
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
vydala	vydat	k5eAaPmAgFnS	vydat
Experience	Experience	k1gFnSc1	Experience
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
studiovou	studiový	k2eAgFnSc4d1	studiová
desku	deska	k1gFnSc4	deska
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Electric	Electrice	k1gFnPc2	Electrice
Ladyland	Ladylando	k1gNnPc2	Ladylando
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1969	[number]	k4	1969
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
Jimmi	Jim	k1gFnPc7	Jim
ve	v	k7c6	v
Woodstocku	Woodstock	k1gInSc6	Woodstock
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
jako	jako	k8xC	jako
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
i	i	k8xC	i
celý	celý	k2eAgInSc4d1	celý
Woodstock	Woodstock	k1gInSc4	Woodstock
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
zdejší	zdejší	k2eAgNnSc1d1	zdejší
vystoupení	vystoupení	k1gNnSc1	vystoupení
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
<g/>
.	.	kIx.	.
mj.	mj.	kA	mj.
svým	svůj	k3xOyFgInSc7	svůj
osobitým	osobitý	k2eAgInSc7d1	osobitý
způsobem	způsob	k1gInSc7	způsob
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
nezapomenutelné	zapomenutelný	k2eNgInPc1d1	nezapomenutelný
"	"	kIx"	"
<g/>
rozebral	rozebrat	k5eAaPmAgMnS	rozebrat
<g/>
"	"	kIx"	"
americkou	americký	k2eAgFnSc4d1	americká
národní	národní	k2eAgFnSc4d1	národní
hymnu	hymna	k1gFnSc4	hymna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
legendární	legendární	k2eAgFnSc1d1	legendární
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
těžce	těžce	k6eAd1	těžce
nevydařený	vydařený	k2eNgInSc1d1	nevydařený
koncert	koncert	k1gInSc1	koncert
měl	mít	k5eAaImAgInS	mít
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1970	[number]	k4	1970
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Fehmarn	Fehmarna	k1gFnPc2	Fehmarna
v	v	k7c6	v
Německé	německý	k2eAgFnSc6d1	německá
spolkové	spolkový	k2eAgFnSc6d1	spolková
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
silně	silně	k6eAd1	silně
deprimovaném	deprimovaný	k2eAgInSc6d1	deprimovaný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1970	[number]	k4	1970
měl	mít	k5eAaImAgMnS	mít
ještě	ještě	k9	ještě
jedno	jeden	k4xCgNnSc4	jeden
menší	malý	k2eAgNnSc4d2	menší
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Ronnie	Ronnie	k1gFnSc1	Ronnie
Scott	Scott	k1gMnSc1	Scott
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Jazz	jazz	k1gInSc1	jazz
Clubu	club	k1gInSc2	club
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mj.	mj.	kA	mj.
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jeho	jeho	k3xOp3gFnSc4	jeho
poslední	poslední	k2eAgFnSc4d1	poslední
-	-	kIx~	-
amatérská	amatérský	k2eAgFnSc1d1	amatérská
-	-	kIx~	-
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgInSc1d1	příští
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1970	[number]	k4	1970
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
v	v	k7c6	v
apartmá	apartmá	k1gNnSc6	apartmá
hotelu	hotel	k1gInSc2	hotel
Samarkand	Samarkanda	k1gFnPc2	Samarkanda
na	na	k7c4	na
Notting	Notting	k1gInSc4	Notting
Hill	Hill	k1gMnSc1	Hill
Gate	Gat	k1gInSc2	Gat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydlela	bydlet	k5eAaImAgFnS	bydlet
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
životní	životní	k2eAgFnSc1d1	životní
partnerka	partnerka	k1gFnSc1	partnerka
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Monika	Monika	k1gFnSc1	Monika
Dannemannová	Dannemannová	k1gFnSc1	Dannemannová
<g/>
,	,	kIx,	,
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
strávil	strávit	k5eAaPmAgMnS	strávit
čtyři	čtyři	k4xCgFnPc4	čtyři
poslední	poslední	k2eAgFnPc4d1	poslední
noci	noc	k1gFnPc4	noc
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
měl	mít	k5eAaImAgInS	mít
poměr	poměr	k1gInSc1	poměr
s	s	k7c7	s
dánskou	dánský	k2eAgFnSc7d1	dánská
modelkou	modelka	k1gFnSc7	modelka
Kirsten	Kirsten	k2eAgInSc1d1	Kirsten
Nefer	Nefer	k1gInSc1	Nefer
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
ovšem	ovšem	k9	ovšem
předtím	předtím	k6eAd1	předtím
z	z	k7c2	z
pracovních	pracovní	k2eAgInPc2d1	pracovní
důvodů	důvod	k1gInPc2	důvod
opustila	opustit	k5eAaPmAgFnS	opustit
Londýn	Londýn	k1gInSc4	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
zprávy	zpráva	k1gFnSc2	zpráva
skonal	skonat	k5eAaPmAgMnS	skonat
v	v	k7c4	v
nemocnici	nemocnice	k1gFnSc4	nemocnice
St.	st.	kA	st.
Mary	Mary	k1gFnSc1	Mary
Abotts	Abotts	k1gInSc1	Abotts
Hospital	Hospital	k1gMnSc1	Hospital
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
<g/>
,	,	kIx,	,
78	[number]	k4	78
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
ambulance	ambulance	k1gFnSc2	ambulance
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
zavolala	zavolat	k5eAaPmAgFnS	zavolat
Dannemann	Dannemann	k1gNnSc4	Dannemann
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zadušení	zadušení	k1gNnSc2	zadušení
zvratky	zvratek	k1gInPc4	zvratek
po	po	k7c4	po
konzumaci	konzumace	k1gFnSc4	konzumace
amfetaminu	amfetamin	k1gInSc2	amfetamin
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
black	black	k1gMnSc1	black
bomber	bomber	k1gMnSc1	bomber
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
párty	párty	k1gFnSc6	párty
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
zdržel	zdržet	k5eAaPmAgInS	zdržet
před	před	k7c7	před
návratem	návrat	k1gInSc7	návrat
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
k	k	k7c3	k
Dannemann	Dannemanno	k1gNnPc2	Dannemanno
<g/>
)	)	kIx)	)
a	a	k8xC	a
následném	následný	k2eAgMnSc6d1	následný
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
neúmyslném	úmyslný	k2eNgInSc6d1	neúmyslný
předávkování	předávkování	k1gNnSc2	předávkování
uspávacími	uspávací	k2eAgInPc7d1	uspávací
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pochyby	pochyba	k1gFnPc1	pochyba
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gFnPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
už	už	k9	už
když	když	k8xS	když
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
dorazili	dorazit	k5eAaPmAgMnP	dorazit
záchranáři	záchranář	k1gMnPc1	záchranář
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
i	i	k9	i
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hendrix	Hendrix	k1gInSc1	Hendrix
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgInS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
plicích	plíce	k1gFnPc6	plíce
a	a	k8xC	a
na	na	k7c6	na
textilu	textil	k1gInSc2	textil
kolem	kolem	k7c2	kolem
krku	krk	k1gInSc2	krk
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
červeného	červený	k2eAgNnSc2d1	červené
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
někdo	někdo	k3yInSc1	někdo
mohl	moct	k5eAaImAgMnS	moct
pokusit	pokusit	k5eAaPmF	pokusit
Jimmyho	Jimmy	k1gMnSc4	Jimmy
udusit	udusit	k5eAaPmF	udusit
záměrně	záměrně	k6eAd1	záměrně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
možná	možný	k2eAgFnSc1d1	možná
pachatelka	pachatelka	k1gFnSc1	pachatelka
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
spekulací	spekulace	k1gFnPc2	spekulace
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
Monika	Monika	k1gFnSc1	Monika
Dannemann	Dannemann	k1gInSc1	Dannemann
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
strávila	strávit	k5eAaPmAgFnS	strávit
osudnou	osudný	k2eAgFnSc4d1	osudná
noc	noc	k1gFnSc4	noc
<g/>
;	;	kIx,	;
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgMnSc4	sám
zemřela	zemřít	k5eAaPmAgFnS	zemřít
o	o	k7c4	o
necelých	celý	k2eNgInPc2d1	necelý
26	[number]	k4	26
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
ne	ne	k9	ne
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
smrtí	smrt	k1gFnSc7	smrt
za	za	k7c4	za
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
vyjasněných	vyjasněný	k2eAgFnPc2d1	vyjasněná
okolností	okolnost	k1gFnPc2	okolnost
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
životní	životní	k2eAgMnSc1d1	životní
partner	partner	k1gMnSc1	partner
ovšem	ovšem	k9	ovšem
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
předcházejících	předcházející	k2eAgFnPc6d1	předcházející
pohrůžkách	pohrůžka	k1gFnPc6	pohrůžka
vraždou	vražda	k1gFnSc7	vražda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
údajně	údajně	k6eAd1	údajně
dostala	dostat	k5eAaPmAgFnS	dostat
<g/>
)	)	kIx)	)
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
prohraném	prohraný	k2eAgInSc6d1	prohraný
nactiutrhačském	nactiutrhačský	k2eAgInSc6d1	nactiutrhačský
soudním	soudní	k2eAgInSc6d1	soudní
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
Hendrixovou	Hendrixový	k2eAgFnSc7d1	Hendrixová
předchozí	předchozí	k2eAgFnSc7d1	předchozí
partnerkou	partnerka	k1gFnSc7	partnerka
Kathy	Katha	k1gFnSc2	Katha
Etchingham	Etchingham	k1gInSc1	Etchingham
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ji	on	k3xPp3gFnSc4	on
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
předtím	předtím	k6eAd1	předtím
zažalovala	zažalovat	k5eAaPmAgFnS	zažalovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
údajně	údajně	k6eAd1	údajně
nezavolala	zavolat	k5eNaPmAgFnS	zavolat
záchranku	záchranka	k1gFnSc4	záchranka
pro	pro	k7c4	pro
Hendrixe	Hendrixe	k1gFnPc4	Hendrixe
včas	včas	k6eAd1	včas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
obnovení	obnovení	k1gNnSc3	obnovení
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
Hendrixovy	Hendrixův	k2eAgFnSc2d1	Hendrixova
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Dannemann	Dannemann	k1gInSc4	Dannemann
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
tenkrát	tenkrát	k6eAd1	tenkrát
osvobozena	osvobozen	k2eAgFnSc1d1	osvobozena
<g/>
.	.	kIx.	.
</s>
<s>
Označila	označit	k5eAaPmAgFnS	označit
poté	poté	k6eAd1	poté
svoji	svůj	k3xOyFgFnSc4	svůj
protivnici	protivnice	k1gFnSc4	protivnice
za	za	k7c4	za
tvrdošíjnou	tvrdošíjný	k2eAgFnSc4d1	tvrdošíjná
lhářku	lhářka	k1gFnSc4	lhářka
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
následovala	následovat	k5eAaImAgFnS	následovat
opětná	opětný	k2eAgFnSc1d1	opětná
žaloba	žaloba	k1gFnSc1	žaloba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Etchingham	Etchingham	k1gInSc1	Etchingham
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
pak	pak	k6eAd1	pak
obvinil	obvinit	k5eAaPmAgMnS	obvinit
jeden	jeden	k4xCgMnSc1	jeden
Hendrixův	Hendrixův	k2eAgMnSc1d1	Hendrixův
roadie	roadie	k1gFnPc1	roadie
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
jeho	jeho	k3xOp3gMnSc2	jeho
manažera	manažer	k1gMnSc2	manažer
Michaela	Michael	k1gMnSc2	Michael
Jefferyho	Jeffery	k1gMnSc2	Jeffery
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
motivována	motivovat	k5eAaBmNgFnS	motivovat
pojišťovacím	pojišťovací	k2eAgInSc7d1	pojišťovací
podvodem	podvod	k1gInSc7	podvod
přes	přes	k7c4	přes
jím	on	k3xPp3gMnSc7	on
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
Hendrixovu	Hendrixův	k2eAgFnSc4d1	Hendrixova
životní	životní	k2eAgFnSc4d1	životní
pojistku	pojistka	k1gFnSc4	pojistka
na	na	k7c4	na
částku	částka	k1gFnSc4	částka
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
dostal	dostat	k5eAaPmAgMnS	dostat
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
poslední	poslední	k2eAgInPc4d1	poslední
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c4	na
Brook	Brook	k1gInSc4	Brook
Street	Streeta	k1gFnPc2	Streeta
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Mayfair	Mayfaira	k1gFnPc2	Mayfaira
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
250	[number]	k4	250
let	léto	k1gNnPc2	léto
před	před	k7c7	před
tím	ten	k3xDgMnSc7	ten
po	po	k7c4	po
40	[number]	k4	40
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
německo-britský	německoritský	k2eAgMnSc1d1	německo-britský
skladatel	skladatel	k1gMnSc1	skladatel
Georg	Georg	k1gMnSc1	Georg
Friedrich	Friedrich	k1gMnSc1	Friedrich
Händel	Händlo	k1gNnPc2	Händlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnPc6	jehož
prostorách	prostora	k1gFnPc6	prostora
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
kanceláře	kancelář	k1gFnPc4	kancelář
Händelovo	Händelův	k2eAgNnSc4d1	Händelovo
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zařídil	zařídit	k5eAaPmAgMnS	zařídit
se	s	k7c7	s
svoji	svůj	k3xOyFgFnSc4	svůj
partnerkou	partnerka	k1gFnSc7	partnerka
Kathy	Katha	k1gFnSc2	Katha
Etchingham	Etchingham	k1gInSc1	Etchingham
(	(	kIx(	(
<g/>
s	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
již	již	k6eAd1	již
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
letech	léto	k1gNnPc6	léto
známosti	známost	k1gFnSc2	známost
rozešel	rozejít	k5eAaPmAgInS	rozejít
<g/>
)	)	kIx)	)
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgInSc1	sám
tento	tento	k3xDgInSc1	tento
příbytek	příbytek	k1gInSc1	příbytek
označil	označit	k5eAaPmAgInS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
můj	můj	k3xOp1gInSc4	můj
první	první	k4xOgInSc4	první
pravý	pravý	k2eAgInSc4d1	pravý
vlastní	vlastní	k2eAgInSc4d1	vlastní
domov	domov	k1gInSc4	domov
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
nacházel	nacházet	k5eAaImAgMnS	nacházet
zde	zde	k6eAd1	zde
potřebný	potřebný	k2eAgInSc4d1	potřebný
odpočinek	odpočinek	k1gInSc4	odpočinek
od	od	k7c2	od
rušného	rušný	k2eAgInSc2d1	rušný
uměleckého	umělecký	k2eAgInSc2d1	umělecký
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
autobiografie	autobiografie	k1gFnSc2	autobiografie
Kathy	Katha	k1gFnSc2	Katha
Etchingham	Etchingham	k1gInSc4	Etchingham
spolu	spolu	k6eAd1	spolu
pili	pít	k5eAaImAgMnP	pít
"	"	kIx"	"
<g/>
víc	hodně	k6eAd2	hodně
čaj	čaj	k1gInSc1	čaj
s	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
než	než	k8xS	než
whisky	whisky	k1gFnSc7	whisky
s	s	k7c7	s
colou	cola	k1gFnSc7	cola
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
i	i	k9	i
zadokumentovaný	zadokumentovaný	k2eAgInSc1d1	zadokumentovaný
Hendrixův	Hendrixův	k2eAgInSc1d1	Hendrixův
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
Händela	Händel	k1gMnSc4	Händel
<g/>
;	;	kIx,	;
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
nastěhování	nastěhování	k1gNnSc6	nastěhování
měl	mít	k5eAaImAgMnS	mít
prohledat	prohledat	k5eAaPmF	prohledat
všechny	všechen	k3xTgInPc4	všechen
okolní	okolní	k2eAgInPc4d1	okolní
obchody	obchod	k1gInPc4	obchod
s	s	k7c7	s
gramofonovými	gramofonový	k2eAgFnPc7d1	gramofonová
deskami	deska	k1gFnPc7	deska
po	po	k7c6	po
materiálech	materiál	k1gInPc6	materiál
k	k	k7c3	k
tomuto	tento	k3xDgMnSc3	tento
skladateli	skladatel	k1gMnSc3	skladatel
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
pozůstalosti	pozůstalost	k1gFnSc6	pozůstalost
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
gramofonové	gramofonový	k2eAgFnPc1d1	gramofonová
desky	deska	k1gFnPc1	deska
s	s	k7c7	s
Händelovou	Händelův	k2eAgFnSc7d1	Händelova
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
měl	mít	k5eAaImAgInS	mít
tou	ten	k3xDgFnSc7	ten
dobu	doba	k1gFnSc4	doba
také	také	k9	také
v	v	k7c6	v
nahrávacím	nahrávací	k2eAgNnSc6d1	nahrávací
studiu	studio	k1gNnSc6	studio
na	na	k7c4	na
Abbey	Abbea	k1gFnPc4	Abbea
Road	Roada	k1gFnPc2	Roada
improvizovat	improvizovat	k5eAaImF	improvizovat
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
"	"	kIx"	"
<g/>
Hot	hot	k0	hot
Händela	Händel	k1gMnSc4	Händel
<g/>
"	"	kIx"	"
na	na	k7c6	na
cemballu	cemballo	k1gNnSc6	cemballo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
tam	tam	k6eAd1	tam
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
po	po	k7c4	po
nahrávání	nahrávání	k1gNnSc4	nahrávání
Beatles	Beatles	k1gFnPc2	Beatles
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
Magical	Magical	k1gFnSc3	Magical
Mystery	Myster	k1gMnPc4	Myster
Tour	Toura	k1gFnPc2	Toura
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
ředitelky	ředitelka	k1gFnSc2	ředitelka
Händelova	Händelův	k2eAgNnSc2d1	Händelovo
muzea	muzeum	k1gNnSc2	muzeum
Sarah	Sarah	k1gFnSc2	Sarah
Bardwell	Bardwella	k1gFnPc2	Bardwella
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
příprav	příprava	k1gFnPc2	příprava
k	k	k7c3	k
vzpomínkovým	vzpomínkový	k2eAgFnPc3d1	vzpomínková
oslavám	oslava	k1gFnPc3	oslava
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
Hendrixova	Hendrixův	k2eAgNnSc2d1	Hendrixův
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
magnetofonových	magnetofonový	k2eAgFnPc6d1	magnetofonová
páskách	páska	k1gFnPc6	páska
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
performancí	performance	k1gFnSc7	performance
zatím	zatím	k6eAd1	zatím
jen	jen	k9	jen
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
pátrá	pátrat	k5eAaImIp3nS	pátrat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
rockového	rockový	k2eAgMnSc4d1	rockový
kytaristu	kytarista	k1gMnSc4	kytarista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kdy	kdy	k6eAd1	kdy
žil	žít	k5eAaImAgMnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
běžnou	běžný	k2eAgFnSc4d1	běžná
kytaru	kytara	k1gFnSc4	kytara
hrál	hrát	k5eAaImAgInS	hrát
levou	levá	k1gFnSc4	levá
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Neuměl	neuměl	k1gMnSc1	neuměl
noty	nota	k1gFnSc2	nota
a	a	k8xC	a
proto	proto	k8xC	proto
místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
používal	používat	k5eAaImAgInS	používat
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
hraní	hraní	k1gNnSc2	hraní
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
samouk	samouk	k1gMnSc1	samouk
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
typu	typ	k1gInSc2	typ
Fender	Fender	k1gMnSc1	Fender
Stratocaster	Stratocaster	k1gMnSc1	Stratocaster
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
otočil	otočit	k5eAaPmAgMnS	otočit
a	a	k8xC	a
držel	držet	k5eAaImAgMnS	držet
obráceně	obráceně	k6eAd1	obráceně
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc4	Hendrix
byl	být	k5eAaImAgMnS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
elektroakustického	elektroakustický	k2eAgInSc2d1	elektroakustický
efektu	efekt	k1gInSc2	efekt
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
"	"	kIx"	"
<g/>
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
feedback	feedback	k6eAd1	feedback
(	(	kIx(	(
<g/>
dosl.	dosl.	k?	dosl.
překrmení	překrmení	k1gNnSc1	překrmení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Efekt	efekt	k1gInSc1	efekt
vzniká	vznikat	k5eAaImIp3nS	vznikat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvuk	zvuk	k1gInSc1	zvuk
vycházející	vycházející	k2eAgInSc1d1	vycházející
z	z	k7c2	z
reproduktoru	reproduktor	k1gInSc2	reproduktor
je	být	k5eAaImIp3nS	být
znovu	znovu	k6eAd1	znovu
posílán	posílán	k2eAgInSc1d1	posílán
snímacím	snímací	k2eAgNnSc7d1	snímací
zařízením	zařízení	k1gNnSc7	zařízení
do	do	k7c2	do
zesilovače	zesilovač	k1gInSc2	zesilovač
vybuzeného	vybuzený	k2eAgInSc2d1	vybuzený
na	na	k7c6	na
maximum	maximum	k1gNnSc4	maximum
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc4	Hendrix
tak	tak	k9	tak
docílil	docílit	k5eAaPmAgMnS	docílit
zkresleného	zkreslený	k2eAgInSc2d1	zkreslený
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
při	při	k7c6	při
hraní	hraní	k1gNnSc6	hraní
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
nežádoucí	žádoucí	k2eNgNnSc4d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
jej	on	k3xPp3gInSc4	on
tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
inovace	inovace	k1gFnSc1	inovace
bluesových	bluesový	k2eAgMnPc2d1	bluesový
hudebníků	hudebník	k1gMnPc2	hudebník
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
B.	B.	kA	B.
B.	B.	kA	B.
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
Muddy	Mudda	k1gFnPc1	Mudda
Waters	Waters	k1gInSc1	Waters
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
King	King	k1gMnSc1	King
a	a	k8xC	a
T-Bone	T-Bon	k1gInSc5	T-Bon
Walker	Walker	k1gInSc1	Walker
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
inspirovat	inspirovat	k5eAaBmF	inspirovat
kytaristy	kytarista	k1gMnPc4	kytarista
hrajícími	hrající	k2eAgFnPc7d1	hrající
rhythm	rhythm	k1gFnPc3	rhythm
and	and	k?	and
blues	blues	k1gFnSc4	blues
a	a	k8xC	a
soul	soul	k1gInSc4	soul
jako	jako	k8xC	jako
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
Curtis	Curtis	k1gFnPc1	Curtis
Mayfield	Mayfield	k1gMnSc1	Mayfield
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Cropper	Cropper	k1gMnSc1	Cropper
či	či	k8xC	či
Cornell	Cornell	k1gMnSc1	Cornell
Dupree	Dupre	k1gFnSc2	Dupre
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
tradičním	tradiční	k2eAgInSc7d1	tradiční
jazzem	jazz	k1gInSc7	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Hendrixovo	Hendrixův	k2eAgNnSc4d1	Hendrixův
vystupování	vystupování	k1gNnSc4	vystupování
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
stylem	styl	k1gInSc7	styl
rockového	rockový	k2eAgMnSc2d1	rockový
průkopníka	průkopník	k1gMnSc2	průkopník
Little	Little	k1gFnSc2	Little
Richarda	Richard	k1gMnSc2	Richard
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
kapelou	kapela	k1gFnSc7	kapela
The	The	k1gFnSc2	The
Upsetters	Upsetters	k1gInSc1	Upsetters
byl	být	k5eAaImAgMnS	být
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
propojit	propojit	k5eAaPmF	propojit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
nazýval	nazývat	k5eAaImAgInS	nazývat
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
což	což	k3yQnSc1	což
byly	být	k5eAaImAgFnP	být
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
bluesové	bluesový	k2eAgInPc4d1	bluesový
<g/>
,	,	kIx,	,
jazzové	jazzový	k2eAgInPc4d1	jazzový
a	a	k8xC	a
funky	funk	k1gInPc1	funk
rytmy	rytmus	k1gInPc1	rytmus
<g/>
,	,	kIx,	,
s	s	k7c7	s
"	"	kIx"	"
<g/>
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
což	což	k3yQnSc4	což
naopak	naopak	k6eAd1	naopak
byly	být	k5eAaImAgFnP	být
pronikavé	pronikavý	k2eAgInPc4d1	pronikavý
psychedelické	psychedelický	k2eAgInPc4d1	psychedelický
zvuky	zvuk	k1gInPc4	zvuk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikaly	vznikat	k5eAaImAgInP	vznikat
jeho	jeho	k3xOp3gNnSc7	jeho
improvizováním	improvizování	k1gNnSc7	improvizování
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
udělal	udělat	k5eAaPmAgInS	udělat
další	další	k2eAgInSc1d1	další
novátorský	novátorský	k2eAgInSc1d1	novátorský
krok	krok	k1gInSc1	krok
-	-	kIx~	-
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
nahrávací	nahrávací	k2eAgNnSc4d1	nahrávací
studio	studio	k1gNnSc4	studio
jako	jako	k8xS	jako
zkušební	zkušební	k2eAgInSc4d1	zkušební
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
invence	invence	k1gFnPc4	invence
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
se	s	k7c7	s
stereofonními	stereofonní	k2eAgInPc7d1	stereofonní
a	a	k8xC	a
fázovacími	fázovací	k2eAgInPc7d1	fázovací
efekty	efekt	k1gInPc7	efekt
přímo	přímo	k6eAd1	přímo
během	během	k7c2	během
nahrávání	nahrávání	k1gNnSc2	nahrávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
Hendrix	Hendrix	k1gInSc1	Hendrix
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
rock	rock	k1gInSc4	rock
<g/>
́	́	k?	́
<g/>
n	n	k0	n
<g/>
́	́	k?	́
<g/>
rollové	rollový	k2eAgFnSc2d1	rollová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
do	do	k7c2	do
Britské	britský	k2eAgFnSc2d1	britská
hudební	hudební	k2eAgFnSc2d1	hudební
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
zdobí	zdobit	k5eAaImIp3nS	zdobit
jeho	jeho	k3xOp3gFnSc1	jeho
hvězda	hvězda	k1gFnSc1	hvězda
Hollywoodský	hollywoodský	k2eAgInSc4d1	hollywoodský
chodník	chodník	k1gInSc4	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
Are	ar	k1gInSc5	ar
You	You	k1gMnSc1	You
Experienced	Experienced	k1gInSc1	Experienced
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
Národního	národní	k2eAgInSc2d1	národní
registru	registr	k1gInSc2	registr
nahrávek	nahrávka	k1gFnPc2	nahrávka
Knihovny	knihovna	k1gFnSc2	knihovna
amerického	americký	k2eAgInSc2d1	americký
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInSc1d1	hudební
magazín	magazín	k1gInSc1	magazín
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
označil	označit	k5eAaPmAgMnS	označit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
Hendrixe	Hendrixe	k1gFnSc1	Hendrixe
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kytaristu	kytarista	k1gMnSc4	kytarista
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
o	o	k7c4	o
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
umělce	umělec	k1gMnPc4	umělec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
zařadil	zařadit	k5eAaPmAgInS	zařadit
na	na	k7c4	na
šesté	šestý	k4xOgNnSc4	šestý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
hudební	hudební	k2eAgFnSc1d1	hudební
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
VH1	VH1	k1gFnSc1	VH1
jej	on	k3xPp3gMnSc4	on
rovněž	rovněž	k9	rovněž
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
třetího	třetí	k4xOgMnSc4	třetí
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hard	hard	k1gMnSc1	hard
rockového	rockový	k2eAgMnSc2d1	rockový
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Hendrixů	Hendrix	k1gInPc2	Hendrix
nosila	nosit	k5eAaImAgFnS	nosit
dříve	dříve	k6eAd2	dříve
tvar	tvar	k1gInSc4	tvar
Hendricks	Hendricksa	k1gFnPc2	Hendricksa
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
své	svůj	k3xOyFgNnSc4	svůj
příjmení	příjmení	k1gNnSc4	příjmení
na	na	k7c4	na
Hendrix	Hendrix	k1gInSc4	Hendrix
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgInS	nechat
Jimiho	Jimi	k1gMnSc4	Jimi
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
a	a	k8xC	a
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
známí	známý	k1gMnPc1	známý
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
mu	on	k3xPp3gMnSc3	on
nikdy	nikdy	k6eAd1	nikdy
neřekli	říct	k5eNaPmAgMnP	říct
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
Buster	Buster	k1gMnSc1	Buster
-	-	kIx~	-
Cvalík	cvalík	k1gMnSc1	cvalík
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
přezdívku	přezdívka	k1gFnSc4	přezdívka
dostal	dostat	k5eAaPmAgMnS	dostat
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
postavy	postava	k1gFnSc2	postava
ještě	ještě	k9	ještě
jako	jako	k8xS	jako
nemluvně	nemluvně	k1gNnSc1	nemluvně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písnička	písnička	k1gFnSc1	písnička
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
uměl	umět	k5eAaImAgMnS	umět
zahrát	zahrát	k5eAaPmF	zahrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skladba	skladba	k1gFnSc1	skladba
Tall	Tall	k1gMnSc1	Tall
Cool	Cool	k1gMnSc1	Cool
One	One	k1gMnSc1	One
od	od	k7c2	od
kapely	kapela	k1gFnSc2	kapela
Fabulous	Fabulous	k1gInSc1	Fabulous
Wailers	Wailers	k1gInSc1	Wailers
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
měřil	měřit	k5eAaImAgInS	měřit
178	[number]	k4	178
centimetrů	centimetr	k1gInPc2	centimetr
a	a	k8xC	a
vážil	vážit	k5eAaImAgMnS	vážit
70	[number]	k4	70
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgInS	být
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
kytaristou	kytarista	k1gMnSc7	kytarista
v	v	k7c6	v
Nashvillu	Nashvill	k1gInSc6	Nashvill
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Hendrix	Hendrix	k1gInSc4	Hendrix
tehdy	tehdy	k6eAd1	tehdy
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Imperials	Imperials	k1gInSc1	Imperials
Johnny	Johnna	k1gFnSc2	Johnna
Jones	Jonesa	k1gFnPc2	Jonesa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
se	se	k3xPyFc4	se
Hendrix	Hendrix	k1gInSc1	Hendrix
setkal	setkat	k5eAaPmAgInS	setkat
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
idoly	idol	k1gInPc7	idol
B.B.	B.B.	k1gFnPc2	B.B.
Kingem	King	k1gInSc7	King
a	a	k8xC	a
Albertem	Albert	k1gMnSc7	Albert
Kingem	King	k1gMnSc7	King
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
slov	slovo	k1gNnPc2	slovo
obdivu	obdiv	k1gInSc2	obdiv
a	a	k8xC	a
chvály	chvála	k1gFnSc2	chvála
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
začal	začít	k5eAaPmAgMnS	začít
vyptávat	vyptávat	k5eAaImF	vyptávat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
kytarovou	kytarový	k2eAgFnSc4d1	kytarová
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
ochotně	ochotně	k6eAd1	ochotně
podělili	podělit	k5eAaPmAgMnP	podělit
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
tajemství	tajemství	k1gNnPc4	tajemství
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
Hendrix	Hendrix	k1gInSc1	Hendrix
ještě	ještě	k6eAd1	ještě
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
paf	paf	k2eAgFnSc1d1	paf
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Johnnym	Johnnym	k1gInSc1	Johnnym
Jonesem	Jones	k1gInSc7	Jones
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgInS	dát
jednoho	jeden	k4xCgInSc2	jeden
večera	večer	k1gInSc2	večer
i	i	k9	i
kytarový	kytarový	k2eAgInSc4d1	kytarový
souboj	souboj	k1gInSc4	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
prohrál	prohrát	k5eAaPmAgInS	prohrát
na	na	k7c6	na
plné	plný	k2eAgFnSc6d1	plná
čáře	čára	k1gFnSc6	čára
<g/>
,	,	kIx,	,
některá	některý	k3yIgFnSc1	některý
jeho	jeho	k3xOp3gFnSc4	jeho
sóla	sólo	k1gNnPc1	sólo
zněla	znět	k5eAaImAgNnP	znět
tak	tak	k9	tak
podobně	podobně	k6eAd1	podobně
B.	B.	kA	B.
B.	B.	kA	B.
Kingovi	King	k1gMnSc3	King
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
obecenstvo	obecenstvo	k1gNnSc1	obecenstvo
místy	místy	k6eAd1	místy
dokonce	dokonce	k9	dokonce
smálo	smát	k5eAaImAgNnS	smát
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
rovněž	rovněž	k9	rovněž
zvažoval	zvažovat	k5eAaImAgInS	zvažovat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
jazzovým	jazzový	k2eAgMnSc7d1	jazzový
trumpetistou	trumpetista	k1gMnSc7	trumpetista
Milesem	Miles	k1gMnSc7	Miles
Davisem	Davis	k1gInSc7	Davis
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
složení	složení	k1gNnSc1	složení
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kromě	kromě	k7c2	kromě
Davise	Davise	k1gFnSc2	Davise
a	a	k8xC	a
Hendrixe	Hendrixe	k1gFnSc2	Hendrixe
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
hrát	hrát	k5eAaImF	hrát
Ahmad	Ahmad	k1gInSc4	Ahmad
Jamal	Jamal	k1gMnSc1	Jamal
a	a	k8xC	a
na	na	k7c4	na
bicí	bicí	k2eAgInSc4d1	bicí
Carl	Carl	k1gInSc4	Carl
Palmer	Palmra	k1gFnPc2	Palmra
z	z	k7c2	z
britského	britský	k2eAgNnSc2d1	Britské
progresivního	progresivní	k2eAgNnSc2d1	progresivní
tria	trio	k1gNnSc2	trio
Emerson	Emersona	k1gFnPc2	Emersona
<g/>
,	,	kIx,	,
Lake	Lake	k1gFnPc2	Lake
&	&	k?	&
Palmer	Palmer	k1gMnSc1	Palmer
<g/>
.	.	kIx.	.
</s>
<s>
Miles	Miles	k1gInSc1	Miles
Davis	Davis	k1gFnSc2	Davis
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Hendrixe	Hendrixe	k1gFnSc2	Hendrixe
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
scénu	scéna	k1gFnSc4	scéna
rockovou	rockový	k2eAgFnSc4d1	rocková
hudbu	hudba	k1gFnSc4	hudba
neposlouchal	poslouchat	k5eNaImAgInS	poslouchat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
zhlédnutí	zhlédnutí	k1gNnSc6	zhlédnutí
záznamu	záznam	k1gInSc2	záznam
z	z	k7c2	z
festivalu	festival	k1gInSc2	festival
v	v	k7c4	v
Monterey	Monterea	k1gFnPc4	Monterea
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgInS	změnit
názor	názor	k1gInSc1	názor
a	a	k8xC	a
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
velmi	velmi	k6eAd1	velmi
stál	stát	k5eAaImAgMnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
bytě	byt	k1gInSc6	byt
několikrát	několikrát	k6eAd1	několikrát
zajamovali	zajamovat	k5eAaPmAgMnP	zajamovat
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
záznamů	záznam	k1gInPc2	záznam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgInPc3d1	finanční
požadavkům	požadavek	k1gInPc3	požadavek
Davisova	Davisův	k2eAgMnSc2d1	Davisův
manažera	manažer	k1gMnSc2	manažer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nechtěl	chtít	k5eNaImAgMnS	chtít
akceptovat	akceptovat	k5eAaBmF	akceptovat
Hendrixovy	Hendrixův	k2eAgInPc4d1	Hendrixův
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
většinový	většinový	k2eAgInSc4d1	většinový
podíl	podíl	k1gInSc4	podíl
z	z	k7c2	z
koncertních	koncertní	k2eAgNnPc2d1	koncertní
turné	turné	k1gNnPc2	turné
či	či	k8xC	či
tantiém	tantiéma	k1gFnPc2	tantiéma
za	za	k7c4	za
prodané	prodaný	k2eAgInPc4d1	prodaný
hudební	hudební	k2eAgInPc4d1	hudební
nosiče	nosič	k1gInPc4	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Davis	Davis	k1gInSc1	Davis
byl	být	k5eAaImAgInS	být
Hendrixem	Hendrix	k1gInSc7	Hendrix
ovšem	ovšem	k9	ovšem
natolik	natolik	k6eAd1	natolik
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
přešel	přejít	k5eAaPmAgMnS	přejít
k	k	k7c3	k
jazz-rocku	jazzock	k1gInSc3	jazz-rock
(	(	kIx(	(
<g/>
slavná	slavný	k2eAgFnSc1d1	slavná
sestava	sestava	k1gFnSc1	sestava
Davis	Davis	k1gFnSc2	Davis
-	-	kIx~	-
Zawinul	Zawinul	k1gMnSc1	Zawinul
-	-	kIx~	-
Shorter	Shorter	k1gMnSc1	Shorter
-	-	kIx~	-
McLaughlin	McLaughlina	k1gFnPc2	McLaughlina
-	-	kIx~	-
Corea	Corea	k1gFnSc1	Corea
-	-	kIx~	-
Holland	Holland	k1gInSc1	Holland
-	-	kIx~	-
De	De	k?	De
Johnette	Johnett	k1gInSc5	Johnett
-	-	kIx~	-
Maupin	Maupin	k2eAgInSc4d1	Maupin
-	-	kIx~	-
Brooks	Brooks	k1gInSc4	Brooks
+	+	kIx~	+
hosté	host	k1gMnPc1	host
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Lenny	Lenny	k?	Lenny
White	Whit	k1gInSc5	Whit
na	na	k7c4	na
bicí	bicí	k2eAgMnPc4d1	bicí
<g/>
)	)	kIx)	)
a	a	k8xC	a
nahrál	nahrát	k5eAaPmAgMnS	nahrát
takové	takový	k3xDgInPc4	takový
skvosty	skvost	k1gInPc4	skvost
jako	jako	k8xS	jako
In	In	k1gFnPc4	In
a	a	k8xC	a
Silent	Silent	k1gMnSc1	Silent
Way	Way	k1gMnSc1	Way
či	či	k8xC	či
Bitches	Bitches	k1gMnSc1	Bitches
Brew	Brew	k1gMnSc1	Brew
<g/>
.	.	kIx.	.
</s>
<s>
Are	ar	k1gInSc5	ar
You	You	k1gMnSc7	You
Experienced	Experienced	k1gMnSc1	Experienced
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Axis	Axis	k1gInSc1	Axis
<g/>
:	:	kIx,	:
Bold	Bold	k1gInSc1	Bold
as	as	k1gNnSc2	as
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Electric	Electrice	k1gFnPc2	Electrice
Ladyland	Ladyland	k1gInSc1	Ladyland
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Band	banda	k1gFnPc2	banda
of	of	k?	of
Gypsys	Gypsysa	k1gFnPc2	Gypsysa
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Cry	Cry	k1gFnSc2	Cry
of	of	k?	of
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Rainbow	Rainbow	k1gFnSc1	Rainbow
Bridge	Bridge	k1gFnSc1	Bridge
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
War	War	k?	War
Heroes	Heroes	k1gInSc1	Heroes
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Loose	Loose	k1gFnSc1	Loose
Ends	Endsa	k1gFnPc2	Endsa
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Crash	Crash	k1gInSc1	Crash
Landing	Landing	k1gInSc1	Landing
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Midnight	Midnight	k2eAgInSc1d1	Midnight
Lightning	Lightning	k1gInSc1	Lightning
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Nine	Nine	k1gFnSc4	Nine
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Universe	Universe	k1gFnPc1	Universe
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Valleys	Valleys	k1gInSc1	Valleys
of	of	k?	of
Neptune	Neptun	k1gInSc5	Neptun
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
People	People	k1gMnSc1	People
<g/>
,	,	kIx,	,
Hell	Hell	k1gMnSc1	Hell
&	&	k?	&
<g />
.	.	kIx.	.
</s>
<s>
Angels	Angels	k1gInSc1	Angels
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Smash	Smash	k1gInSc1	Smash
Hits	Hits	k1gInSc1	Hits
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Electric	Electrice	k1gFnPc2	Electrice
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Sound	Sound	k1gInSc1	Sound
Track	Track	k1gMnSc1	Track
Recordings	Recordingsa	k1gFnPc2	Recordingsa
from	from	k1gMnSc1	from
the	the	k?	the
Film	film	k1gInSc1	film
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Musique	Musiqu	k1gInSc2	Musiqu
Originale	Originale	k1gFnSc2	Originale
du	du	k?	du
Film	film	k1gInSc1	film
Jimi	on	k3xPp3gInPc7	on
Plays	Plays	k1gInSc1	Plays
Berkeley	Berkeley	k1gInPc1	Berkeley
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Re-Experienced	Re-Experienced	k1gInSc1	Re-Experienced
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Essential	Essential	k1gMnSc1	Essential
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Essential	Essential	k1gMnSc1	Essential
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
Volume	volum	k1gInSc5	volum
Two	Two	k1gMnSc6	Two
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Stone	ston	k1gInSc5	ston
Free	Free	k1gNnPc1	Free
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
The	The	k1gFnPc4	The
Singles	Singles	k1gInSc4	Singles
Album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Kiss	Kissa	k1gFnPc2	Kissa
the	the	k?	the
Sky	Sky	k1gMnSc1	Sky
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Essential	Essential	k1gMnSc1	Essential
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
Volumes	Volumes	k1gMnSc1	Volumes
<g />
.	.	kIx.	.
</s>
<s>
One	One	k?	One
and	and	k?	and
Two	Two	k1gFnSc2	Two
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
&	&	k?	&
Unreleased	Unreleased	k1gInSc1	Unreleased
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Radio	radio	k1gNnSc1	radio
Show	show	k1gFnSc2	show
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Cornerstones	Cornerstonesa	k1gFnPc2	Cornerstonesa
<g/>
:	:	kIx,	:
1967-1970	[number]	k4	1967-1970
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Lifelines	Lifelines	k1gMnSc1	Lifelines
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Story	story	k1gFnPc4	story
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Sessions	Sessionsa	k1gFnPc2	Sessionsa
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Footlights	Footlightsa	k1gFnPc2	Footlightsa
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Stages	Stagesa	k1gFnPc2	Stagesa
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Ultimate	Ultimat	k1gInSc5	Ultimat
Experience	Experienec	k1gMnSc2	Experienec
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Blues	blues	k1gNnSc1	blues
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Voodoo	Voodoo	k1gMnSc1	Voodoo
Soup	Soup	k1gMnSc1	Soup
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
First	First	k1gFnSc1	First
Rays	Rays	k1gInSc1	Rays
of	of	k?	of
the	the	k?	the
New	New	k1gFnSc1	New
Rising	Rising	k1gInSc1	Rising
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
South	South	k1gInSc1	South
Saturn	Saturn	k1gInSc1	Saturn
Delta	delta	k1gFnSc1	delta
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
BBC	BBC	kA	BBC
Sessions	Sessions	k1gInSc1	Sessions
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Experience	Experienec	k1gInSc2	Experienec
Hendrix	Hendrix	k1gInSc1	Hendrix
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Voodoo	Voodoo	k1gMnSc1	Voodoo
Child	Child	k1gMnSc1	Child
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Singles	Singles	k1gMnSc1	Singles
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Martin	Martin	k1gMnSc1	Martin
Scorsese	Scorsese	k1gFnSc2	Scorsese
Presents	Presents	k1gInSc1	Presents
the	the	k?	the
Blues	blues	k1gNnSc1	blues
<g/>
:	:	kIx,	:
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Astro	astra	k1gFnSc5	astra
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
West	West	k2eAgMnSc1d1	West
Coast	Coast	k1gMnSc1	Coast
Seattle	Seattle	k1gFnSc2	Seattle
Boy	boy	k1gMnSc1	boy
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
Anthology	Antholog	k1gMnPc7	Antholog
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
Experience	Experienec	k1gInSc2	Experienec
Hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Woodstock	Woodstock	k1gInSc4	Woodstock
27	[number]	k4	27
Club	club	k1gInSc4	club
CROSS	CROSS	kA	CROSS
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
R.	R.	kA	R.
<g/>
.	.	kIx.	.
</s>
<s>
Pokoj	pokoj	k1gInSc1	pokoj
plný	plný	k2eAgInSc1d1	plný
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
Životopis	životopis	k1gInSc1	životopis
Jimmiho	Jimmi	k1gMnSc4	Jimmi
Hendrixe	Hendrixe	k1gFnSc2	Hendrixe
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
693	[number]	k4	693
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
WELCH	WELCH	kA	WELCH
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
:	:	kIx,	:
život	život	k1gInSc4	život
a	a	k8xC	a
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86515	[number]	k4	86515
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
MCDERMOTT	MCDERMOTT	kA	MCDERMOTT
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
;	;	kIx,	;
KRAMER	KRAMER	kA	KRAMER
<g/>
,	,	kIx,	,
Eddie	Eddie	k1gFnSc1	Eddie
<g/>
.	.	kIx.	.
</s>
<s>
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
-	-	kIx~	-
nadoraz	nadoraz	k?	nadoraz
-	-	kIx~	-
elektrický	elektrický	k2eAgInSc1d1	elektrický
chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
288	[number]	k4	288
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
TAST	TAST	kA	TAST
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
;	;	kIx,	;
TAST	TAST	kA	TAST
<g/>
,	,	kIx,	,
Hans-Juergen	Hans-Juergen	k1gInSc1	Hans-Juergen
<g/>
.	.	kIx.	.
</s>
<s>
Still	Still	k1gMnSc1	Still
the	the	k?	the
wind	wind	k1gMnSc1	wind
cries	cries	k1gMnSc1	cries
Jimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Hendrix	Hendrix	k1gInSc1	Hendrix
in	in	k?	in
Marokko	Marokko	k1gNnSc1	Marokko
<g/>
,	,	kIx,	,
Schellerten	Schellertno	k1gNnPc2	Schellertno
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-3-88842-040-5	[number]	k4	978-3-88842-040-5
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Jimi	on	k3xPp3gFnPc7	on
Hendrix	Hendrix	k1gInSc1	Hendrix
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Experience	Experience	k1gFnSc1	Experience
Hendrix	Hendrix	k1gInSc1	Hendrix
magazine	magazinout	k5eAaPmIp3nS	magazinout
<g/>
;	;	kIx,	;
Seattle	Seattle	k1gFnSc1	Seattle
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
