<s>
Hmat	hmat	k1gInSc1	hmat
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
lidských	lidský	k2eAgInPc2d1	lidský
smyslů	smysl	k1gInPc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
hmat	hmat	k1gInSc1	hmat
spíše	spíše	k9	spíše
soubor	soubor	k1gInSc1	soubor
několika	několik	k4yIc2	několik
různých	různý	k2eAgInPc2d1	různý
smyslů	smysl	k1gInPc2	smysl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pomocí	pomocí	k7c2	pomocí
receptorů	receptor	k1gInPc2	receptor
v	v	k7c6	v
kůži	kůže	k1gFnSc6	kůže
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
získávat	získávat	k5eAaImF	získávat
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
bezprostředního	bezprostřední	k2eAgNnSc2d1	bezprostřední
okolí	okolí	k1gNnSc2	okolí
-	-	kIx~	-
<g/>
,	,	kIx,	,
te	te	k?	te
vpichu	vpich	k1gInSc2	vpich
<g/>
,	,	kIx,	,
vibrací	vibrace	k1gFnSc7	vibrace
atd.	atd.	kA	atd.
-	-	kIx~	-
souhrnně	souhrnně	k6eAd1	souhrnně
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
stimulace	stimulace	k1gFnPc1	stimulace
nazývají	nazývat	k5eAaImIp3nP	nazývat
taktilní	taktilní	k2eAgInSc4d1	taktilní
kontakt	kontakt	k1gInSc4	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Mezilidský	mezilidský	k2eAgInSc4d1	mezilidský
kontakt	kontakt	k1gInSc4	kontakt
pomocí	pomocí	k7c2	pomocí
doteku	dotek	k1gInSc2	dotek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
haptika	haptika	k1gFnSc1	haptika
<g/>
.	.	kIx.	.
</s>
<s>
Hmatové	hmatový	k2eAgInPc1d1	hmatový
receptory	receptor	k1gInPc1	receptor
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
kůži	kůže	k1gFnSc4	kůže
rozprostřeny	rozprostřen	k2eAgFnPc4d1	rozprostřena
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
hustotou	hustota	k1gFnSc7	hustota
-	-	kIx~	-
nejcitlivější	citlivý	k2eAgNnSc1d3	nejcitlivější
místo	místo	k1gNnSc1	místo
hmatu	hmat	k1gInSc2	hmat
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konečcích	koneček	k1gInPc6	koneček
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nejméně	málo	k6eAd3	málo
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
<g/>
.	.	kIx.	.
</s>
<s>
Slepí	slepý	k2eAgMnPc1d1	slepý
lidé	člověk	k1gMnPc1	člověk
nemohou	moct	k5eNaImIp3nP	moct
číst	číst	k5eAaImF	číst
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
abecedu	abeceda	k1gFnSc4	abeceda
z	z	k7c2	z
vyvýšených	vyvýšený	k2eAgInPc2d1	vyvýšený
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Braillovo	Braillův	k2eAgNnSc4d1	Braillovo
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc1	bod
nahmatávají	nahmatávat	k5eAaImIp3nP	nahmatávat
špičkami	špička	k1gFnPc7	špička
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
