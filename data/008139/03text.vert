<s>
Mládí	mládí	k1gNnSc1	mládí
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Youth	Youth	k1gInSc1	Youth
in	in	k?	in
Revolt	revolta	k1gFnPc2	revolta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
heptalogie	heptalogie	k1gFnSc1	heptalogie
od	od	k7c2	od
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
C.	C.	kA	C.
D.	D.	kA	D.
Payna	Payn	k1gMnSc2	Payn
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Tamara	Tamara	k1gFnSc1	Tamara
Váňová	Váňová	k1gFnSc1	Váňová
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
až	až	k9	až
6	[number]	k4	6
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
a	a	k8xC	a
Naďa	Naďa	k1gFnSc1	Naďa
Funioková	Funiokový	k2eAgFnSc1d1	Funioková
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
díl	díl	k1gInSc4	díl
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
především	především	k9	především
pro	pro	k7c4	pro
dospívající	dospívající	k2eAgFnPc4d1	dospívající
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ich-formě	ichorma	k1gFnSc6	ich-forma
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
fiktivním	fiktivní	k2eAgMnSc6d1	fiktivní
pubertálním	pubertální	k2eAgMnSc6d1	pubertální
chlapci	chlapec	k1gMnSc6	chlapec
Nicku	nicka	k1gFnSc4	nicka
Twispovi	Twisp	k1gMnSc3	Twisp
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Oaklandu	Oakland	k1gInSc2	Oakland
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
Mladík	mladík	k1gMnSc1	mladík
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
okrajovou	okrajový	k2eAgFnSc7d1	okrajová
postavou	postava	k1gFnSc7	postava
–	–	k?	–
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
jeho	jeho	k3xOp3gNnSc4	jeho
bratr	bratr	k1gMnSc1	bratr
(	(	kIx(	(
<g/>
Noel	Noel	k1gMnSc1	Noel
Lance	lance	k1gNnSc2	lance
Wescott	Wescott	k1gMnSc1	Wescott
–	–	k?	–
později	pozdě	k6eAd2	pozdě
Jake	Jake	k1gNnSc1	Jake
Twisp	Twisp	k1gInSc1	Twisp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Winnemucca	Winnemucc	k1gInSc2	Winnemucc
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
díle	díl	k1gInSc6	díl
Mládí	mládí	k1gNnSc2	mládí
furt	furt	k?	furt
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
čtenář	čtenář	k1gMnSc1	čtenář
prochází	procházet	k5eAaImIp3nS	procházet
deníkem	deník	k1gInSc7	deník
Nickova	Nickův	k2eAgMnSc2d1	Nickův
syna	syn	k1gMnSc2	syn
Scotta	Scott	k1gMnSc2	Scott
Twispa	Twisp	k1gMnSc2	Twisp
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
v	v	k7c4	v
osmý	osmý	k4xOgInSc4	osmý
díl	díl	k1gInSc4	díl
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
Nicka	nicka	k1gFnSc1	nicka
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
druhého	druhý	k4xOgMnSc2	druhý
syna	syn	k1gMnSc2	syn
Nicka	nicka	k1gFnSc1	nicka
Twispa	Twispa	k1gFnSc1	Twispa
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
heptalogie	heptalogie	k1gFnSc1	heptalogie
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
eskapáda	eskapáda	k1gFnSc1	eskapáda
Nickových	Nickův	k2eAgFnPc2d1	Nickova
pohrom	pohroma	k1gFnPc2	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Nick	Nick	k6eAd1	Nick
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
rozvedené	rozvedený	k2eAgFnSc6d1	rozvedená
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mívá	mívat	k5eAaImIp3nS	mívat
konflikty	konflikt	k1gInPc4	konflikt
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dovolené	dovolená	k1gFnSc6	dovolená
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Clear	Cleara	k1gFnPc2	Cleara
Lake	Lake	k1gNnSc2	Lake
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
krásnou	krásný	k2eAgFnSc7d1	krásná
a	a	k8xC	a
chytrou	chytrý	k2eAgFnSc7d1	chytrá
Sheeni	Sheen	k2eAgMnPc1d1	Sheen
Saundersovou	Saundersový	k2eAgFnSc4d1	Saundersový
(	(	kIx(	(
<g/>
žijící	žijící	k2eAgFnSc4d1	žijící
v	v	k7c6	v
Ukiahu	Ukiah	k1gInSc6	Ukiah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nick	Nick	k6eAd1	Nick
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
ihned	ihned	k6eAd1	ihned
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
radost	radost	k1gFnSc1	radost
mu	on	k3xPp3gMnSc3	on
kazí	kazit	k5eAaImIp3nS	kazit
jeho	jeho	k3xOp3gMnSc1	jeho
sok	sok	k1gMnSc1	sok
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
–	–	k?	–
Trent	Trent	k1gInSc1	Trent
Preston	Preston	k1gInSc1	Preston
<g/>
.	.	kIx.	.
</s>
<s>
Trent	Trent	k1gInSc1	Trent
se	se	k3xPyFc4	se
se	s	k7c7	s
Sheeni	Sheen	k2eAgMnPc1d1	Sheen
zná	znát	k5eAaImIp3nS	znát
už	už	k6eAd1	už
od	od	k7c2	od
útlého	útlý	k2eAgNnSc2d1	útlé
dětství	dětství	k1gNnSc2	dětství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ona	onen	k3xDgFnSc1	onen
pohledný	pohledný	k2eAgMnSc1d1	pohledný
a	a	k8xC	a
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
dovolené	dovolená	k1gFnSc2	dovolená
se	se	k3xPyFc4	se
Nick	Nick	k1gMnSc1	Nick
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
společného	společný	k2eAgMnSc4d1	společný
psa	pes	k1gMnSc4	pes
Alberta	Albert	k1gMnSc4	Albert
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Sheeni	Sheen	k2eAgMnPc1d1	Sheen
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
Nickovi	Nickův	k2eAgMnPc1d1	Nickův
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gFnSc2	jeho
zamilovanosti	zamilovanost	k1gFnSc2	zamilovanost
hroutí	hroutit	k5eAaImIp3nS	hroutit
svět	svět	k1gInSc1	svět
pod	pod	k7c7	pod
rukama	ruka	k1gFnPc7	ruka
<g/>
:	:	kIx,	:
musí	muset	k5eAaImIp3nP	muset
začít	začít	k5eAaPmF	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
zapálí	zapálit	k5eAaPmIp3nP	zapálit
půlku	půlka	k1gFnSc4	půlka
Berkley	Berklea	k1gFnSc2	Berklea
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
–	–	k?	–
Leroy	Leroa	k1gFnSc2	Leroa
(	(	kIx(	(
<g/>
avšak	avšak	k8xC	avšak
každý	každý	k3xTgMnSc1	každý
mu	on	k3xPp3gMnSc3	on
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gInSc2	jeho
hendikepu	hendikep	k1gInSc2	hendikep
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
ústrojí	ústrojí	k1gNnSc2	ústrojí
říká	říkat	k5eAaImIp3nS	říkat
Levák	levák	k1gMnSc1	levák
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc4	on
zradí	zradit	k5eAaPmIp3nS	zradit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
stíhán	stíhán	k2eAgMnSc1d1	stíhán
FBI	FBI	kA	FBI
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
převléká	převlékat	k5eAaImIp3nS	převlékat
za	za	k7c4	za
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
nechá	nechat	k5eAaPmIp3nS	nechat
pomocí	pomocí	k7c2	pomocí
plastické	plastický	k2eAgFnSc2d1	plastická
operace	operace	k1gFnSc2	operace
změnit	změnit	k5eAaPmF	změnit
obličej	obličej	k1gInSc4	obličej
a	a	k8xC	a
utíká	utíkat	k5eAaImIp3nS	utíkat
s	s	k7c7	s
Sheeni	Sheen	k2eAgMnPc1d1	Sheen
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
musí	muset	k5eAaImIp3nP	muset
začít	začít	k5eAaPmF	začít
vydávat	vydávat	k5eAaImF	vydávat
za	za	k7c4	za
postarší	postarší	k2eAgFnSc4d1	postarší
dámu	dáma	k1gFnSc4	dáma
a	a	k8xC	a
cestovat	cestovat	k5eAaImF	cestovat
s	s	k7c7	s
cirkusem	cirkus	k1gInSc7	cirkus
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
přece	přece	k9	přece
jen	jen	k9	jen
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
v	v	k7c4	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
řádku	řádek	k1gInSc6	řádek
let	léto	k1gNnPc2	léto
zavřen	zavřít	k5eAaPmNgInS	zavřít
do	do	k7c2	do
nápravného	nápravný	k2eAgNnSc2d1	nápravné
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
české	český	k2eAgFnPc1d1	Česká
postavy	postava	k1gFnPc1	postava
–	–	k?	–
například	například	k6eAd1	například
dívka	dívka	k1gFnSc1	dívka
od	od	k7c2	od
cirkusu	cirkus	k1gInSc2	cirkus
Reina	Rein	k2eAgFnSc1d1	Reina
Veselá	Veselá	k1gFnSc1	Veselá
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Šestý	šestý	k4xOgInSc1	šestý
díl	díl	k1gInSc1	díl
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
vývoj	vývoj	k1gInSc4	vývoj
po	po	k7c4	po
Nickovo	Nickův	k2eAgNnSc4d1	Nickovo
chycení	chycení	k1gNnSc4	chycení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
deníku	deník	k1gInSc2	deník
jeho	jeho	k3xOp3gMnSc2	jeho
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Jakea	Jakeus	k1gMnSc2	Jakeus
Twispa	Twisp	k1gMnSc2	Twisp
<g/>
.	.	kIx.	.
</s>
<s>
Sedmý	sedmý	k4xOgInSc1	sedmý
díl	díl	k1gInSc1	díl
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
–	–	k?	–
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
deníku	deník	k1gInSc2	deník
Nickova	Nickův	k2eAgMnSc2d1	Nickův
syna	syn	k1gMnSc2	syn
Scotta	Scott	k1gMnSc2	Scott
Twispa	Twisp	k1gMnSc2	Twisp
–	–	k?	–
Nickova	Nickov	k1gInSc2	Nickov
otcovská	otcovský	k2eAgNnPc1d1	otcovské
léta	léto	k1gNnPc1	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
do	do	k7c2	do
děje	děj	k1gInSc2	děj
přímo	přímo	k6eAd1	přímo
vrací	vracet	k5eAaImIp3nS	vracet
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
věčná	věčný	k2eAgFnSc1d1	věčná
láska	láska	k1gFnSc1	láska
Sheeni	Sheen	k2eAgMnPc1d1	Sheen
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
osmý	osmý	k4xOgInSc4	osmý
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
odehrávající	odehrávající	k2eAgMnSc1d1	odehrávající
se	s	k7c7	s
38	[number]	k4	38
let	léto	k1gNnPc2	léto
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
celé	celý	k2eAgFnSc2d1	celá
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
snahy	snaha	k1gFnPc4	snaha
starého	starý	k1gMnSc2	starý
Nicka	nicka	k1gFnSc1	nicka
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
druhého	druhý	k4xOgMnSc2	druhý
syna	syn	k1gMnSc2	syn
Nicka	nicka	k1gFnSc1	nicka
Twispa	Twispa	k1gFnSc1	Twispa
II	II	kA	II
o	o	k7c4	o
natočení	natočení	k1gNnSc4	natočení
filmu	film	k1gInSc2	film
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
vlastním	vlastní	k2eAgInSc6d1	vlastní
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
Mládí	mládí	k1gNnSc1	mládí
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
<g/>
:	:	kIx,	:
Mladík	mladík	k1gMnSc1	mladík
v	v	k7c6	v
odboji	odboj	k1gInSc6	odboj
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Youth	Youth	k1gInSc1	Youth
in	in	k?	in
Revolt	revolta	k1gFnPc2	revolta
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
Mládí	mládí	k1gNnSc1	mládí
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
<g/>
:	:	kIx,	:
Mladík	mladík	k1gMnSc1	mladík
v	v	k7c6	v
okovech	okov	k1gInPc6	okov
(	(	kIx(	(
<g/>
Youth	Youth	k1gMnSc1	Youth
in	in	k?	in
Bondage	Bondage	k1gInSc1	Bondage
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
Mládí	mládí	k1gNnSc1	mládí
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
<g/>
:	:	kIx,	:
Mladík	mladík	k1gMnSc1	mladík
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
(	(	kIx(	(
<g/>
Youth	Youth	k1gInSc1	Youth
in	in	k?	in
Exile	exil	k1gInSc5	exil
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Mládí	mládí	k1gNnSc1	mládí
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
<g/>
:	:	kIx,	:
Mladík	mladík	k1gMnSc1	mladík
v	v	k7c6	v
chomoutu	chomout	k1gInSc6	chomout
(	(	kIx(	(
<g/>
Revolting	Revolting	k1gInSc1	Revolting
Youth	Youth	k1gInSc1	Youth
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
Mládí	mládí	k1gNnSc1	mládí
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
<g/>
:	:	kIx,	:
Mladík	mladík	k1gMnSc1	mladík
pod	pod	k7c7	pod
pantoflem	pantoflem	k?	pantoflem
(	(	kIx(	(
<g/>
Young	Young	k1gMnSc1	Young
and	and	k?	and
Revolting	Revolting	k1gInSc1	Revolting
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
Mládí	mládí	k1gNnSc1	mládí
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
<g/>
:	:	kIx,	:
Mladík	mladík	k1gMnSc1	mladík
v	v	k7c6	v
Nevadě	Nevada	k1gFnSc6	Nevada
(	(	kIx(	(
<g/>
Revoltingly	Revoltingla	k1gFnSc2	Revoltingla
<g />
.	.	kIx.	.
</s>
<s>
Young	Young	k1gMnSc1	Young
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
Mládí	mládí	k1gNnSc1	mládí
furt	furt	k?	furt
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
(	(	kIx(	(
<g/>
Son	son	k1gInSc1	son
of	of	k?	of
Youth	Youth	k1gInSc1	Youth
in	in	k?	in
Revolt	revolta	k1gFnPc2	revolta
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
–	–	k?	–
Mládí	mládí	k1gNnSc1	mládí
imrvére	imrvér	k1gMnSc5	imrvér
v	v	k7c6	v
hajzlu	hajzl	k1gInSc2	hajzl
<g/>
:	:	kIx,	:
aneb	aneb	k?	aneb
Geny	gen	k1gInPc1	gen
nezapřeš	zapřít	k5eNaPmIp2nS	zapřít
(	(	kIx(	(
<g/>
Revolt	revolta	k1gFnPc2	revolta
at	at	k?	at
the	the	k?	the
Beach	Beach	k1gInSc1	Beach
<g/>
:	:	kIx,	:
More	mor	k1gInSc5	mor
Twisp	Twisp	k1gMnSc1	Twisp
Family	Famila	k1gFnSc2	Famila
Chronicles	Chronicles	k1gMnSc1	Chronicles
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
Mládí	mládí	k1gNnSc1	mládí
v	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
(	(	kIx(	(
<g/>
Youth	Youth	k1gInSc1	Youth
in	in	k?	in
Revolt	revolta	k1gFnPc2	revolta
<g/>
)	)	kIx)	)
americká	americký	k2eAgFnSc1d1	americká
komedie	komedie	k1gFnSc1	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Miguel	Miguel	k1gInSc1	Miguel
Arteta	Arteto	k1gNnSc2	Arteto
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Cera	Cera	k1gMnSc1	Cera
<g/>
,	,	kIx,	,
Justin	Justin	k1gMnSc1	Justin
Long	Long	k1gMnSc1	Long
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
Buscemi	Busce	k1gFnPc7	Busce
</s>
