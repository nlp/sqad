<s>
Armáda	armáda	k1gFnSc1	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
AČR	AČR	kA	AČR
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
řadí	řadit	k5eAaImIp3nS	řadit
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Hradní	hradní	k2eAgFnSc1d1	hradní
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
organizační	organizační	k2eAgFnSc2d1	organizační
struktury	struktura	k1gFnSc2	struktura
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
AČR	AČR	kA	AČR
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
generální	generální	k2eAgInSc4d1	generální
štáb	štáb	k1gInSc4	štáb
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
který	který	k3yQgInSc4	který
spadají	spadat	k5eAaPmIp3nP	spadat
<g/>
:	:	kIx,	:
Pozemní	pozemní	k2eAgFnPc1d1	pozemní
síly	síla	k1gFnPc1	síla
AČR	AČR	kA	AČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
základem	základ	k1gInSc7	základ
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
brigády	brigáda	k1gFnPc4	brigáda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
jednotkami	jednotka	k1gFnPc7	jednotka
pozemního	pozemní	k2eAgNnSc2d1	pozemní
vojska	vojsko	k1gNnSc2	vojsko
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
plnění	plnění	k1gNnSc4	plnění
úkolů	úkol	k1gInPc2	úkol
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
i	i	k9	i
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
;	;	kIx,	;
Vzdušné	vzdušný	k2eAgFnPc4d1	vzdušná
síly	síla	k1gFnPc4	síla
AČR	AČR	kA	AČR
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgNnSc1d1	vojenské
letectvo	letectvo	k1gNnSc1	letectvo
zabezpečující	zabezpečující	k2eAgFnSc4d1	zabezpečující
suverenitu	suverenita	k1gFnSc4	suverenita
a	a	k8xC	a
obranyschopnost	obranyschopnost	k1gFnSc4	obranyschopnost
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
prostoru	prostor	k1gInSc2	prostor
ČR	ČR	kA	ČR
<g/>
;	;	kIx,	;
Velitelství	velitelství	k1gNnSc1	velitelství
výcviku	výcvik	k1gInSc2	výcvik
–	–	k?	–
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
resortní	resortní	k2eAgFnSc3d1	resortní
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
a	a	k8xC	a
výcvikové	výcvikový	k2eAgNnSc1d1	výcvikové
zařízení	zařízení	k1gNnSc1	zařízení
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
úkoly	úkol	k1gInPc1	úkol
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
219	[number]	k4	219
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
obranu	obrana	k1gFnSc4	obrana
ČR	ČR	kA	ČR
proti	proti	k7c3	proti
vnějšímu	vnější	k2eAgNnSc3d1	vnější
napadení	napadení	k1gNnSc3	napadení
a	a	k8xC	a
plnění	plnění	k1gNnSc4	plnění
úkolů	úkol	k1gInPc2	úkol
vyplývajících	vyplývající	k2eAgInPc2d1	vyplývající
z	z	k7c2	z
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
smluvních	smluvní	k2eAgInPc2d1	smluvní
závazků	závazek	k1gInPc2	závazek
ČR	ČR	kA	ČR
o	o	k7c6	o
společné	společný	k2eAgFnSc6d1	společná
obraně	obrana	k1gFnSc6	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
NATO	NATO	kA	NATO
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
AČR	AČR	kA	AČR
se	se	k3xPyFc4	se
primárně	primárně	k6eAd1	primárně
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
obrany	obrana	k1gFnSc2	obrana
dané	daný	k2eAgFnSc2d1	daná
článkem	článek	k1gInSc7	článek
5	[number]	k4	5
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
Společnou	společný	k2eAgFnSc7d1	společná
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
a	a	k8xC	a
obrannou	obranný	k2eAgFnSc7d1	obranná
politikou	politika	k1gFnSc7	politika
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1954	[number]	k4	1954
Československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
Československá	československý	k2eAgFnSc1d1	Československá
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
ČSLA	ČSLA	kA	ČSLA
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
opět	opět	k6eAd1	opět
Československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
armády	armáda	k1gFnPc4	armáda
<g/>
:	:	kIx,	:
Armádu	armáda	k1gFnSc4	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Armádu	armáda	k1gFnSc4	armáda
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
Armáda	armáda	k1gFnSc1	armáda
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
Ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
síly	síla	k1gFnSc2	síla
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
sily	sít	k5eAaImAgFnP	sít
Slovenskej	Slovenskej	k?	Slovenskej
republiky	republika	k1gFnPc1	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
AČR	AČR	kA	AČR
plně	plně	k6eAd1	plně
profesionální	profesionální	k2eAgFnSc7d1	profesionální
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současné	současný	k2eAgFnSc2d1	současná
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
vyžadována	vyžadovat	k5eAaImNgFnS	vyžadovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
státu	stát	k1gInSc2	stát
či	či	k8xC	či
za	za	k7c2	za
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
názvy	název	k1gInPc1	název
armády	armáda	k1gFnSc2	armáda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
:	:	kIx,	:
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1950	[number]	k4	1950
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
branná	branný	k2eAgFnSc1d1	Branná
moc	moc	k1gFnSc1	moc
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
dostala	dostat	k5eAaPmAgFnS	dostat
československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
na	na	k7c6	na
základě	základ	k1gInSc6	základ
branného	branný	k2eAgInSc2d1	branný
zákona	zákon	k1gInSc2	zákon
<g/>
)	)	kIx)	)
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
–	–	k?	–
Československá	československý	k2eAgFnSc1d1	Československá
armáda	armáda	k1gFnSc1	armáda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
–	–	k?	–
Armáda	armáda	k1gFnSc1	armáda
České	český	k2eAgFnPc1d1	Česká
republiky	republika	k1gFnPc1	republika
(	(	kIx(	(
<g/>
AČR	AČR	kA	AČR
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
výzbroje	výzbroj	k1gFnSc2	výzbroj
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Seznam	seznam	k1gInSc4	seznam
vojenských	vojenský	k2eAgNnPc2d1	vojenské
letadel	letadlo	k1gNnPc2	letadlo
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
druhy	druh	k1gInPc1	druh
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
výzbroje	výzbroj	k1gFnSc2	výzbroj
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
sledované	sledovaný	k2eAgInPc1d1	sledovaný
Smlouvou	smlouva	k1gFnSc7	smlouva
o	o	k7c6	o
konvenčních	konvenční	k2eAgFnPc6d1	konvenční
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
armáda	armáda	k1gFnSc1	armáda
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
smlouvy	smlouva	k1gFnSc2	smlouva
ve	v	k7c6	v
výzbroji	výzbroj	k1gFnSc6	výzbroj
maximálně	maximálně	k6eAd1	maximálně
957	[number]	k4	957
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
1	[number]	k4	1
367	[number]	k4	367
bojových	bojový	k2eAgNnPc2d1	bojové
obrněných	obrněný	k2eAgNnPc2d1	obrněné
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
767	[number]	k4	767
dělostřeleckých	dělostřelecký	k2eAgInPc2d1	dělostřelecký
systémů	systém	k1gInPc2	systém
nad	nad	k7c7	nad
100	[number]	k4	100
mm	mm	kA	mm
<g/>
,	,	kIx,	,
230	[number]	k4	230
bojových	bojový	k2eAgInPc2d1	bojový
letounů	letoun	k1gInPc2	letoun
a	a	k8xC	a
50	[number]	k4	50
bojových	bojový	k2eAgInPc2d1	bojový
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
armáda	armáda	k1gFnSc1	armáda
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
celkem	celkem	k6eAd1	celkem
120	[number]	k4	120
tanků	tank	k1gInPc2	tank
T-72	T-72	k1gFnPc2	T-72
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
30	[number]	k4	30
modernizovaných	modernizovaný	k2eAgFnPc2d1	modernizovaná
T-72M4	T-72M4	k1gFnPc2	T-72M4
CZ	CZ	kA	CZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
439	[number]	k4	439
bojových	bojový	k2eAgNnPc2d1	bojové
obrněných	obrněný	k2eAgNnPc2d1	obrněné
vozidel	vozidlo	k1gNnPc2	vozidlo
BVP-	BVP-	k1gFnSc1	BVP-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
BVP-2	BVP-2	k1gMnSc1	BVP-2
a	a	k8xC	a
Pandur	pandur	k1gMnSc1	pandur
II	II	kA	II
a	a	k8xC	a
179	[number]	k4	179
dělostřeleckých	dělostřelecký	k2eAgInPc2d1	dělostřelecký
systémů	systém	k1gInPc2	systém
ShKH	ShKH	k1gFnSc2	ShKH
vz.	vz.	k?	vz.
77	[number]	k4	77
DANA	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
vz.	vz.	k?	vz.
82	[number]	k4	82
PRÁM-L	PRÁM-L	k1gFnPc2	PRÁM-L
a	a	k8xC	a
ShM	ShM	k1gFnPc2	ShM
vz.	vz.	k?	vz.
85	[number]	k4	85
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
smluvně	smluvně	k6eAd1	smluvně
omezované	omezovaný	k2eAgFnSc2d1	omezovaná
výzbroje	výzbroj	k1gFnSc2	výzbroj
měla	mít	k5eAaImAgFnS	mít
AČR	AČR	kA	AČR
také	také	k6eAd1	také
174	[number]	k4	174
lehkých	lehký	k2eAgNnPc2d1	lehké
obrněných	obrněný	k2eAgNnPc2d1	obrněné
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
149	[number]	k4	149
vozidel	vozidlo	k1gNnPc2	vozidlo
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
nástavbou	nástavba	k1gFnSc7	nástavba
na	na	k7c6	na
podvozcích	podvozek	k1gInPc6	podvozek
bojových	bojový	k2eAgFnPc2d1	bojová
vozidel	vozidlo	k1gNnPc2	vozidlo
pěchoty	pěchota	k1gFnSc2	pěchota
a	a	k8xC	a
obrněných	obrněný	k2eAgInPc2d1	obrněný
transportérů	transportér	k1gInPc2	transportér
nebo	nebo	k8xC	nebo
40	[number]	k4	40
obrněných	obrněný	k2eAgNnPc2d1	obrněné
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
nakoupit	nakoupit	k5eAaPmF	nakoupit
až	až	k9	až
300	[number]	k4	300
obrněných	obrněný	k2eAgNnPc2d1	obrněné
vozidel	vozidlo	k1gNnPc2	vozidlo
Titus	Titus	k1gInSc1	Titus
za	za	k7c2	za
20	[number]	k4	20
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
v	v	k7c6	v
několika	několik	k4yIc6	několik
vlnách	vlna	k1gFnPc6	vlna
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2019	[number]	k4	2019
a	a	k8xC	a
2023	[number]	k4	2023
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
62	[number]	k4	62
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
AČR	AČR	kA	AČR
disponovaly	disponovat	k5eAaBmAgFnP	disponovat
35	[number]	k4	35
bojovými	bojový	k2eAgInPc7d1	bojový
letouny	letoun	k1gInPc7	letoun
typu	typ	k1gInSc2	typ
Gripen	Gripen	k2eAgMnSc1d1	Gripen
a	a	k8xC	a
ALCA	ALCA	kA	ALCA
<g/>
,	,	kIx,	,
9	[number]	k4	9
cvičnými	cvičný	k2eAgInPc7d1	cvičný
letouny	letoun	k1gInPc7	letoun
L-39	L-39	k1gMnSc1	L-39
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
17	[number]	k4	17
dopravními	dopravní	k2eAgInPc7d1	dopravní
a	a	k8xC	a
pozorovacími	pozorovací	k2eAgInPc7d1	pozorovací
letouny	letoun	k1gInPc7	letoun
<g/>
,	,	kIx,	,
17	[number]	k4	17
bitevními	bitevní	k2eAgInPc7d1	bitevní
vrtulníky	vrtulník	k1gInPc7	vrtulník
Mi-	Mi-	k1gMnSc2	Mi-
<g/>
24	[number]	k4	24
<g/>
V	V	kA	V
a	a	k8xC	a
35	[number]	k4	35
dopravními	dopravní	k2eAgInPc7d1	dopravní
a	a	k8xC	a
víceúčelovými	víceúčelový	k2eAgInPc7d1	víceúčelový
vrtulníky	vrtulník	k1gInPc7	vrtulník
typové	typový	k2eAgFnSc2d1	typová
řady	řada	k1gFnSc2	řada
Mi-	Mi-	k1gFnSc2	Mi-
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
a	a	k8xC	a
W-3	W-3	k1gMnSc1	W-3
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
organizační	organizační	k2eAgFnSc1d1	organizační
struktura	struktura	k1gFnSc1	struktura
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
Generální	generální	k2eAgInSc1d1	generální
štáb	štáb	k1gInSc1	štáb
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Velitelství	velitelství	k1gNnSc1	velitelství
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
velitelství	velitelství	k1gNnSc1	velitelství
pozemních	pozemní	k2eAgFnPc2d1	pozemní
a	a	k8xC	a
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
jsou	být	k5eAaImIp3nP	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
<g/>
)	)	kIx)	)
Velitelství	velitelství	k1gNnSc4	velitelství
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
Některé	některý	k3yIgInPc1	některý
útvary	útvar	k1gInPc1	útvar
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
podřízené	podřízená	k1gFnSc6	podřízená
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
obrany	obrana	k1gFnSc2	obrana
–	–	k?	–
včetně	včetně	k7c2	včetně
jednotek	jednotka	k1gFnPc2	jednotka
patřících	patřící	k2eAgFnPc2d1	patřící
k	k	k7c3	k
nejlépe	dobře	k6eAd3	dobře
vycvičeným	vycvičený	k2eAgMnPc3d1	vycvičený
a	a	k8xC	a
vybaveným	vybavený	k2eAgMnSc7d1	vybavený
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Armády	armáda	k1gFnSc2	armáda
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
součástí	součást	k1gFnSc7	součást
Vojenského	vojenský	k2eAgNnSc2d1	vojenské
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
i	i	k9	i
601	[number]	k4	601
<g/>
.	.	kIx.	.
skupina	skupina	k1gFnSc1	skupina
speciálních	speciální	k2eAgFnPc2d1	speciální
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
bude	být	k5eAaImBp3nS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
podřízenou	podřízený	k2eAgFnSc7d1	podřízená
jednotkou	jednotka	k1gFnSc7	jednotka
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
Ředitelství	ředitelství	k1gNnSc2	ředitelství
speciálních	speciální	k2eAgFnPc2d1	speciální
sil	síla	k1gFnPc2	síla
MO	MO	kA	MO
a	a	k8xC	a
přejde	přejít	k5eAaPmIp3nS	přejít
pod	pod	k7c4	pod
řízení	řízení	k1gNnSc4	řízení
GŠ	GŠ	kA	GŠ
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pozemní	pozemní	k2eAgFnSc2d1	pozemní
síly	síla	k1gFnSc2	síla
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
brigáda	brigáda	k1gFnSc1	brigáda
rychlého	rychlý	k2eAgNnSc2d1	rychlé
nasazení	nasazení	k1gNnSc2	nasazení
(	(	kIx(	(
<g/>
Žatec	Žatec	k1gInSc1	Žatec
<g/>
)	)	kIx)	)
41	[number]	k4	41
<g/>
.	.	kIx.	.
mechanizovaný	mechanizovaný	k2eAgInSc1d1	mechanizovaný
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Žatec	Žatec	k1gInSc1	Žatec
<g/>
)	)	kIx)	)
42	[number]	k4	42
<g/>
.	.	kIx.	.
mechanizovaný	mechanizovaný	k2eAgInSc1d1	mechanizovaný
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Tábor	Tábor	k1gInSc1	Tábor
<g/>
)	)	kIx)	)
43	[number]	k4	43
<g/>
.	.	kIx.	.
výsadkový	výsadkový	k2eAgInSc1d1	výsadkový
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
)	)	kIx)	)
44	[number]	k4	44
<g/>
.	.	kIx.	.
lehký	lehký	k2eAgInSc1d1	lehký
motorizovaný	motorizovaný	k2eAgInSc1d1	motorizovaný
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
mechanizovaná	mechanizovaný	k2eAgFnSc1d1	mechanizovaná
brigáda	brigáda	k1gFnSc1	brigáda
(	(	kIx(	(
<g/>
Hranice	hranice	k1gFnSc1	hranice
<g/>
)	)	kIx)	)
71	[number]	k4	71
<g/>
.	.	kIx.	.
mechanizovaný	mechanizovaný	k2eAgInSc1d1	mechanizovaný
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Hranice	hranice	k1gFnSc1	hranice
<g/>
)	)	kIx)	)
72	[number]	k4	72
<g/>
.	.	kIx.	.
mechanizovaný	mechanizovaný	k2eAgInSc1d1	mechanizovaný
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Přáslavice	Přáslavice	k1gFnSc1	Přáslavice
<g/>
)	)	kIx)	)
73	[number]	k4	73
<g/>
.	.	kIx.	.
tankový	tankový	k2eAgInSc1d1	tankový
prapor	prapor	k1gInSc1	prapor
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Přáslavice	Přáslavice	k1gFnSc2	Přáslavice
<g/>
)	)	kIx)	)
74	[number]	k4	74
<g/>
.	.	kIx.	.
lehký	lehký	k2eAgInSc1d1	lehký
motorizovaný	motorizovaný	k2eAgInSc1d1	motorizovaný
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Bučovice	Bučovice	k1gFnPc1	Bučovice
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
dělostřelecký	dělostřelecký	k2eAgInSc1d1	dělostřelecký
pluk	pluk	k1gInSc1	pluk
(	(	kIx(	(
<g/>
Jince	Jince	k1gInPc1	Jince
<g/>
)	)	kIx)	)
131	[number]	k4	131
<g/>
.	.	kIx.	.
dělostřelecký	dělostřelecký	k2eAgInSc1d1	dělostřelecký
oddíl	oddíl	k1gInSc1	oddíl
(	(	kIx(	(
<g/>
Jince	Jince	k1gInPc1	Jince
<g/>
)	)	kIx)	)
132	[number]	k4	132
<g/>
.	.	kIx.	.
dělostřelecký	dělostřelecký	k2eAgInSc1d1	dělostřelecký
oddíl	oddíl	k1gInSc1	oddíl
(	(	kIx(	(
<g/>
Jince	Jince	k1gInPc1	Jince
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
ženijní	ženijní	k2eAgFnSc2d1	ženijní
<g />
.	.	kIx.	.
</s>
<s>
pluk	pluk	k1gInSc1	pluk
(	(	kIx(	(
<g/>
Bechyně	Bechyně	k1gFnSc1	Bechyně
<g/>
)	)	kIx)	)
151	[number]	k4	151
<g/>
.	.	kIx.	.
ženijní	ženijní	k2eAgInSc1d1	ženijní
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Bechyně	Bechyně	k1gFnSc1	Bechyně
<g/>
)	)	kIx)	)
153	[number]	k4	153
<g/>
.	.	kIx.	.
ženijní	ženijní	k2eAgInSc1d1	ženijní
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
31	[number]	k4	31
<g/>
.	.	kIx.	.
pluk	pluk	k1gInSc1	pluk
radiační	radiační	k2eAgInSc1d1	radiační
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnSc2d1	chemická
<g/>
,	,	kIx,	,
biologické	biologický	k2eAgFnSc2d1	biologická
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
311	[number]	k4	311
<g/>
.	.	kIx.	.
prapor	prapor	k1gInSc1	prapor
radiační	radiační	k2eAgInSc1d1	radiační
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
biologické	biologický	k2eAgFnPc1d1	biologická
ochrany	ochrana	k1gFnPc1	ochrana
(	(	kIx(	(
<g/>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
312	[number]	k4	312
<g/>
.	.	kIx.	.
prapor	prapor	k1gInSc1	prapor
radiační	radiační	k2eAgInSc1d1	radiační
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnSc2d1	chemická
a	a	k8xC	a
biologické	biologický	k2eAgFnSc2d1	biologická
ochrany	ochrana	k1gFnSc2	ochrana
(	(	kIx(	(
<g/>
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
314	[number]	k4	314
<g/>
.	.	kIx.	.
centrum	centrum	k1gNnSc1	centrum
výstrahy	výstraha	k1gFnSc2	výstraha
zbraní	zbraň	k1gFnPc2	zbraň
hromadného	hromadný	k2eAgNnSc2d1	hromadné
ničení	ničení	k1gNnSc2	ničení
(	(	kIx(	(
<g/>
Hostivice	Hostivice	k1gFnSc1	Hostivice
<g/>
)	)	kIx)	)
53	[number]	k4	53
<g/>
.	.	kIx.	.
pluk	pluk	k1gInSc1	pluk
průzkumu	průzkum	k1gInSc2	průzkum
a	a	k8xC	a
elektronického	elektronický	k2eAgInSc2d1	elektronický
boje	boj	k1gInSc2	boj
(	(	kIx(	(
<g/>
Opava	Opava	k1gFnSc1	Opava
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
532	[number]	k4	532
<g/>
.	.	kIx.	.
prapor	prapor	k1gInSc1	prapor
elektronického	elektronický	k2eAgInSc2d1	elektronický
boje	boj	k1gInSc2	boj
(	(	kIx(	(
<g/>
Opava	Opava	k1gFnSc1	Opava
<g/>
)	)	kIx)	)
102	[number]	k4	102
<g/>
.	.	kIx.	.
průzkumný	průzkumný	k2eAgInSc1d1	průzkumný
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
)	)	kIx)	)
103	[number]	k4	103
<g/>
.	.	kIx.	.
centrum	centrum	k1gNnSc1	centrum
CIMIC	CIMIC	kA	CIMIC
<g/>
/	/	kIx~	/
<g/>
PSYOPS	PSYOPS	kA	PSYOPS
(	(	kIx(	(
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
14	[number]	k4	14
<g/>
.	.	kIx.	.
pluk	pluk	k1gInSc1	pluk
logistické	logistický	k2eAgFnSc2d1	logistická
podpory	podpora	k1gFnSc2	podpora
(	(	kIx(	(
<g/>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
141	[number]	k4	141
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
zásobovací	zásobovací	k2eAgInSc1d1	zásobovací
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
142	[number]	k4	142
<g/>
.	.	kIx.	.
prapor	prapor	k1gInSc1	prapor
oprav	oprava	k1gFnPc2	oprava
(	(	kIx(	(
<g/>
Klatovy	Klatovy	k1gInPc1	Klatovy
<g/>
)	)	kIx)	)
143	[number]	k4	143
<g/>
.	.	kIx.	.
zásobovací	zásobovací	k2eAgInSc1d1	zásobovací
prapor	prapor	k1gInSc1	prapor
(	(	kIx(	(
<g/>
Lipník	Lipník	k1gInSc1	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
síly	síla	k1gFnSc2	síla
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
základna	základna	k1gFnSc1	základna
taktického	taktický	k2eAgNnSc2d1	taktické
letectva	letectvo	k1gNnSc2	letectvo
(	(	kIx(	(
<g/>
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
211	[number]	k4	211
<g/>
.	.	kIx.	.
taktická	taktický	k2eAgFnSc1d1	taktická
letka	letka	k1gFnSc1	letka
212	[number]	k4	212
<g/>
.	.	kIx.	.
taktická	taktický	k2eAgFnSc1d1	taktická
letka	letka	k1gFnSc1	letka
213	[number]	k4	213
<g/>
.	.	kIx.	.
výcviková	výcvikový	k2eAgFnSc1d1	výcviková
letka	letka	k1gFnSc1	letka
22	[number]	k4	22
<g/>
.	.	kIx.	.
základna	základna	k1gFnSc1	základna
vrtulníkového	vrtulníkový	k2eAgNnSc2d1	vrtulníkové
letectva	letectvo	k1gNnSc2	letectvo
(	(	kIx(	(
<g/>
Náměšť	Náměšť	k1gFnSc1	Náměšť
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
<g/>
)	)	kIx)	)
221	[number]	k4	221
<g/>
.	.	kIx.	.
vrtulníková	vrtulníkový	k2eAgFnSc1d1	vrtulníková
letka	letka	k1gFnSc1	letka
222	[number]	k4	222
<g/>
.	.	kIx.	.
vrtulníková	vrtulníkový	k2eAgFnSc1d1	vrtulníková
letka	letka	k1gFnSc1	letka
24	[number]	k4	24
<g/>
.	.	kIx.	.
základna	základna	k1gFnSc1	základna
dopravního	dopravní	k2eAgNnSc2d1	dopravní
letectva	letectvo	k1gNnSc2	letectvo
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Kbely	Kbely	k1gInPc1	Kbely
<g/>
)	)	kIx)	)
241	[number]	k4	241
<g/>
.	.	kIx.	.
dopravní	dopravní	k2eAgFnSc1d1	dopravní
letka	letka	k1gFnSc1	letka
242	[number]	k4	242
<g/>
.	.	kIx.	.
transportní	transportní	k2eAgFnSc1d1	transportní
a	a	k8xC	a
speciální	speciální	k2eAgFnSc1d1	speciální
letka	letka	k1gFnSc1	letka
243	[number]	k4	243
<g/>
.	.	kIx.	.
vrtulníková	vrtulníkový	k2eAgFnSc1d1	vrtulníková
letka	letka	k1gFnSc1	letka
25	[number]	k4	25
<g/>
.	.	kIx.	.
protiletadlový	protiletadlový	k2eAgInSc1d1	protiletadlový
raketový	raketový	k2eAgInSc1d1	raketový
pluk	pluk	k1gInSc1	pluk
(	(	kIx(	(
<g/>
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
)	)	kIx)	)
251	[number]	k4	251
<g/>
.	.	kIx.	.
protiletadlový	protiletadlový	k2eAgInSc1d1	protiletadlový
raketový	raketový	k2eAgInSc1d1	raketový
oddíl	oddíl	k1gInSc1	oddíl
252	[number]	k4	252
<g/>
.	.	kIx.	.
protiletadlový	protiletadlový	k2eAgInSc1d1	protiletadlový
raketový	raketový	k2eAgInSc1d1	raketový
oddíl	oddíl	k1gInSc1	oddíl
26	[number]	k4	26
<g/>
.	.	kIx.	.
pluk	pluk	k1gInSc1	pluk
velení	velení	k1gNnSc2	velení
<g/>
,	,	kIx,	,
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
průzkumu	průzkum	k1gInSc2	průzkum
(	(	kIx(	(
<g/>
Stará	starat	k5eAaImIp3nS	starat
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
)	)	kIx)	)
261	[number]	k4	261
<g/>
.	.	kIx.	.
středisko	středisko	k1gNnSc1	středisko
řízení	řízení	k1gNnSc1	řízení
a	a	k8xC	a
uvědomování	uvědomování	k1gNnSc1	uvědomování
262	[number]	k4	262
<g/>
.	.	kIx.	.
radiotechnický	radiotechnický	k2eAgInSc4d1	radiotechnický
prapor	prapor	k1gInSc4	prapor
Správa	správa	k1gFnSc1	správa
letiště	letiště	k1gNnSc2	letiště
Pardubice	Pardubice	k1gInPc1	Pardubice
Povinnost	povinnost	k1gFnSc1	povinnost
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
misí	mise	k1gFnPc2	mise
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
ze	z	k7c2	z
závazků	závazek	k1gInPc2	závazek
vůči	vůči	k7c3	vůči
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
vojáci	voják	k1gMnPc1	voják
rovněž	rovněž	k9	rovněž
pracují	pracovat	k5eAaImIp3nP	pracovat
jako	jako	k9	jako
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
OSN	OSN	kA	OSN
A	a	k8xC	a
OBSE	OBSE	kA	OBSE
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
probíhají	probíhat	k5eAaImIp3nP	probíhat
mise	mise	k1gFnPc4	mise
<g/>
:	:	kIx,	:
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Operace	operace	k1gFnSc1	operace
Althea	Althea	k1gFnSc1	Althea
<g/>
:	:	kIx,	:
Cílem	cíl	k1gInSc7	cíl
operace	operace	k1gFnSc2	operace
Althea	Althe	k1gInSc2	Althe
je	být	k5eAaImIp3nS	být
zachovat	zachovat	k5eAaPmF	zachovat
bezpečí	bezpečí	k1gNnSc4	bezpečí
a	a	k8xC	a
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mise	mise	k1gFnSc1	mise
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
většímu	veliký	k2eAgInSc3d2	veliký
pokroku	pokrok	k1gInSc3	pokrok
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
evropské	evropský	k2eAgFnSc3d1	Evropská
integraci	integrace	k1gFnSc3	integrace
a	a	k8xC	a
k	k	k7c3	k
potírání	potírání	k1gNnSc3	potírání
korupce	korupce	k1gFnSc2	korupce
a	a	k8xC	a
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
V	v	k7c6	v
Afghánistánské	Afghánistánský	k2eAgFnSc6d1	Afghánistánský
provincii	provincie	k1gFnSc6	provincie
Lógar	Lógara	k1gFnPc2	Lógara
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
Provinční	provinční	k2eAgInSc1d1	provinční
rekonstrukční	rekonstrukční	k2eAgInSc1d1	rekonstrukční
tým	tým	k1gInSc1	tým
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mise	mise	k1gFnSc2	mise
NATO	NATO	kA	NATO
ISAF	ISAF	kA	ISAF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lógaru	Lógar	k1gInSc6	Lógar
také	také	k9	také
působí	působit	k5eAaImIp3nS	působit
jednotka	jednotka	k1gFnSc1	jednotka
AČR	AČR	kA	AČR
MAT	mat	k6eAd1	mat
(	(	kIx(	(
<g/>
Military	Militara	k1gFnPc1	Militara
Advisory	Advisor	k1gInPc1	Advisor
Team	team	k1gInSc1	team
-	-	kIx~	-
Armádní	armádní	k2eAgInSc1d1	armádní
poradní	poradní	k2eAgInSc1d1	poradní
tým	tým	k1gInSc1	tým
<g/>
)	)	kIx)	)
dislokovaná	dislokovaný	k2eAgFnSc1d1	dislokovaná
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Shank	Shanka	k1gFnPc2	Shanka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Vardak	Vardak	k1gMnSc1	Vardak
se	se	k3xPyFc4	se
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
střídají	střídat	k5eAaImIp3nP	střídat
jednotky	jednotka	k1gFnPc1	jednotka
poradního	poradní	k2eAgInSc2d1	poradní
a	a	k8xC	a
výcvikového	výcvikový	k2eAgInSc2d1	výcvikový
týmu	tým	k1gInSc2	tým
(	(	kIx(	(
<g/>
OMLT	OMLT	kA	OMLT
-	-	kIx~	-
Operational	Operational	k1gMnSc2	Operational
Mentoring	Mentoring	k1gInSc1	Mentoring
and	and	k?	and
Liaison	liaison	k1gInSc1	liaison
Team	team	k1gInSc1	team
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
pátá	pátá	k1gFnSc1	pátá
jednotka	jednotka	k1gFnSc1	jednotka
bude	být	k5eAaImBp3nS	být
působit	působit	k5eAaImF	působit
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc4	jednotka
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
května	květen	k1gInSc2	květen
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
nasazena	nasazen	k2eAgFnSc1d1	nasazena
6	[number]	k4	6
<g/>
.	.	kIx.	.
polní	polní	k2eAgFnSc2d1	polní
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyslání	vyslání	k1gNnSc3	vyslání
dalších	další	k2eAgFnPc2d1	další
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Provinčního	provinční	k2eAgInSc2d1	provinční
rekonstrukčního	rekonstrukční	k2eAgInSc2d1	rekonstrukční
týmu	tým	k1gInSc2	tým
(	(	kIx(	(
<g/>
PRT	PRT	kA	PRT
<g/>
)	)	kIx)	)
ve	v	k7c6	v
východoafghánské	východoafghánský	k2eAgFnSc6d1	východoafghánský
provincii	provincie	k1gFnSc6	provincie
Lógar	Lógara	k1gFnPc2	Lógara
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
událostí	událost	k1gFnPc2	událost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
považovat	považovat	k5eAaImF	považovat
zformování	zformování	k1gNnSc4	zformování
tzv.	tzv.	kA	tzv.
Úkolového	úkolový	k2eAgNnSc2d1	úkolové
uskupení	uskupení	k1gNnSc2	uskupení
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ISAF	ISAF	kA	ISAF
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
založení	založení	k1gNnSc3	založení
tohoto	tento	k3xDgNnSc2	tento
uskupení	uskupení	k1gNnSc2	uskupení
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
veškerého	veškerý	k3xTgInSc2	veškerý
personálu	personál	k1gInSc2	personál
a	a	k8xC	a
jednotek	jednotka	k1gFnPc2	jednotka
českých	český	k2eAgFnPc2d1	Česká
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
nasazených	nasazený	k2eAgFnPc2d1	nasazená
v	v	k7c6	v
operaci	operace	k1gFnSc6	operace
ISAF	ISAF	kA	ISAF
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
úkolového	úkolový	k2eAgNnSc2d1	úkolové
uskupení	uskupení	k1gNnSc2	uskupení
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
nová	nový	k2eAgFnSc1d1	nová
struktura	struktura	k1gFnSc1	struktura
velení	velení	k1gNnSc2	velení
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
funkce	funkce	k1gFnSc1	funkce
velitele	velitel	k1gMnSc2	velitel
ÚU	ÚU	kA	ÚU
AČR	AČR	kA	AČR
ISAF	ISAF	kA	ISAF
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
působilo	působit	k5eAaImAgNnS	působit
na	na	k7c6	na
území	území	k1gNnSc6	území
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
již	již	k6eAd1	již
5	[number]	k4	5
<g/>
.	.	kIx.	.
úkolové	úkolový	k2eAgNnSc1d1	úkolové
uskupení	uskupení	k1gNnSc1	uskupení
AČR	AČR	kA	AČR
ISAF	ISAF	kA	ISAF
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
doba	doba	k1gFnSc1	doba
působnosti	působnost	k1gFnSc2	působnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ÚU	ÚU	kA	ÚU
je	být	k5eAaImIp3nS	být
omezena	omezen	k2eAgFnSc1d1	omezena
na	na	k7c4	na
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jednotným	jednotný	k2eAgNnSc7d1	jednotné
velením	velení	k1gNnSc7	velení
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
útvarů	útvar	k1gInPc2	útvar
se	se	k3xPyFc4	se
specifickou	specifický	k2eAgFnSc7d1	specifická
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
jednotky	jednotka	k1gFnPc4	jednotka
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
ÚU	ÚU	kA	ÚU
tak	tak	k9	tak
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
patřily	patřit	k5eAaImAgInP	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Velitelství	velitelství	k1gNnSc1	velitelství
ÚU	ÚU	kA	ÚU
AČR	AČR	kA	AČR
ISAF	ISAF	kA	ISAF
Národní	národní	k2eAgInSc1d1	národní
podpůrný	podpůrný	k2eAgInSc1d1	podpůrný
prvek	prvek	k1gInSc1	prvek
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
NSE	NSE	kA	NSE
<g/>
)	)	kIx)	)
Jednotka	jednotka	k1gFnSc1	jednotka
chemické	chemický	k2eAgFnSc2d1	chemická
a	a	k8xC	a
biologické	biologický	k2eAgFnSc2d1	biologická
ochrany	ochrana	k1gFnSc2	ochrana
Jednotka	jednotka	k1gFnSc1	jednotka
AAT	AAT	kA	AAT
v	v	k7c6	v
Kábulu	Kábul	k1gInSc6	Kábul
Polní	polní	k2eAgInSc1d1	polní
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
tým	tým	k1gInSc1	tým
Zastoupení	zastoupení	k1gNnSc2	zastoupení
na	na	k7c6	na
velitelstvích	velitelství	k1gNnPc6	velitelství
ISAF	ISAF	kA	ISAF
v	v	k7c6	v
Kábulu	Kábul	k1gInSc6	Kábul
a	a	k8xC	a
Bagramu	Bagram	k1gInSc6	Bagram
Provinční	provinční	k2eAgInSc1d1	provinční
rekonstrukční	rekonstrukční	k2eAgInSc1d1	rekonstrukční
tým	tým	k1gInSc1	tým
(	(	kIx(	(
<g/>
PRT	PRT	kA	PRT
<g/>
)	)	kIx)	)
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Lógar	Lógara	k1gFnPc2	Lógara
Poradní	poradní	k2eAgInSc1d1	poradní
a	a	k8xC	a
výcvikový	výcvikový	k2eAgInSc1d1	výcvikový
tým	tým	k1gInSc1	tým
OMLT	OMLT	kA	OMLT
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Vardak	Vardak	k1gInSc1	Vardak
Výcviková	výcvikový	k2eAgFnSc1d1	výcviková
jednotka	jednotka	k1gFnSc1	jednotka
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
policie	policie	k1gFnSc2	policie
V	v	k7c6	v
afghánské	afghánský	k2eAgFnSc6d1	afghánská
misi	mise	k1gFnSc6	mise
byly	být	k5eAaImAgInP	být
nasazeny	nasadit	k5eAaPmNgInP	nasadit
také	také	k9	také
útvary	útvar	k1gInPc1	útvar
a	a	k8xC	a
technika	technika	k1gFnSc1	technika
Vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
AČR	AČR	kA	AČR
<g/>
.	.	kIx.	.
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
Mise	mise	k1gFnSc1	mise
KFOR	KFOR	kA	KFOR
<g/>
:	:	kIx,	:
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
velitelství	velitelství	k1gNnSc6	velitelství
KFOR	KFOR	kA	KFOR
jedenáct	jedenáct	k4xCc1	jedenáct
českých	český	k2eAgMnPc2d1	český
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
štábu	štáb	k1gInSc6	štáb
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
plnění	plnění	k1gNnSc6	plnění
operačního	operační	k2eAgInSc2d1	operační
úkolu	úkol	k1gInSc2	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Somálsko	Somálsko	k1gNnSc1	Somálsko
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
EU-NAVFOR	EU-NAVFOR	k1gFnSc2	EU-NAVFOR
-	-	kIx~	-
Atalanta	Atalant	k1gMnSc2	Atalant
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
tři	tři	k4xCgMnPc4	tři
čeští	český	k2eAgMnPc1d1	český
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
má	mít	k5eAaImIp3nS	mít
mandát	mandát	k1gInSc4	mandát
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Světového	světový	k2eAgInSc2d1	světový
potravinového	potravinový	k2eAgInSc2d1	potravinový
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
WFP	WFP	kA	WFP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sinaj	Sinaj	k1gInSc1	Sinaj
Na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
podmínek	podmínka	k1gFnPc2	podmínka
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
Izraelem	Izrael	k1gInSc7	Izrael
působí	působit	k5eAaImIp3nP	působit
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
tři	tři	k4xCgMnPc1	tři
důstojníci	důstojník	k1gMnPc1	důstojník
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mali	Mali	k1gNnSc1	Mali
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Mise	mise	k1gFnSc2	mise
EU	EU	kA	EU
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Mali	Mali	k1gNnSc6	Mali
nebojová	bojový	k2eNgFnSc1d1	nebojová
jednotka	jednotka	k1gFnSc1	jednotka
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
výcvik	výcvik	k1gInSc4	výcvik
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
ochrana	ochrana	k1gFnSc1	ochrana
velitelství	velitelství	k1gNnSc2	velitelství
výcvikové	výcvikový	k2eAgFnSc2d1	výcviková
mise	mise	k1gFnSc2	mise
EUTM	EUTM	kA	EUTM
Mali	Mali	k1gNnSc4	Mali
(	(	kIx(	(
<g/>
EU	EU	kA	EU
Training	Training	k1gInSc1	Training
Mission	Mission	k1gInSc1	Mission
<g/>
)	)	kIx)	)
v	v	k7c6	v
Bamaku	Bamak	k1gInSc6	Bamak
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgFnPc1d1	vojenská
a	a	k8xC	a
mírové	mírový	k2eAgFnPc1d1	mírová
operace	operace	k1gFnPc1	operace
a	a	k8xC	a
mise	mise	k1gFnPc1	mise
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
účastnila	účastnit	k5eAaImAgFnS	účastnit
Armáda	armáda	k1gFnSc1	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
uniforem	uniforma	k1gFnPc2	uniforma
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vojenské	vojenský	k2eAgFnSc2d1	vojenská
hodnosti	hodnost	k1gFnSc2	hodnost
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
vojáků	voják	k1gMnPc2	voják
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
silách	síla	k1gFnPc6	síla
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazující	navazující	k2eAgInPc1d1	navazující
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
člení	členit	k5eAaImIp3nP	členit
do	do	k7c2	do
hodnostních	hodnostní	k2eAgInPc2d1	hodnostní
sborů	sbor	k1gInPc2	sbor
podle	podle	k7c2	podle
hodnosti	hodnost	k1gFnSc2	hodnost
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
Od	od	k7c2	od
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
upravilo	upravit	k5eAaPmAgNnS	upravit
hodnostní	hodnostní	k2eAgNnSc1d1	hodnostní
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
označení	označení	k1gNnSc1	označení
na	na	k7c6	na
výložkách	výložka	k1gFnPc6	výložka
udává	udávat	k5eAaImIp3nS	udávat
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kauza	kauza	k1gFnSc1	kauza
Pandury	pandur	k1gMnPc4	pandur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
česká	český	k2eAgNnPc1d1	české
média	médium	k1gNnPc1	médium
začala	začít	k5eAaPmAgNnP	začít
spekulovat	spekulovat	k5eAaImF	spekulovat
o	o	k7c6	o
nejasnostech	nejasnost	k1gFnPc6	nejasnost
a	a	k8xC	a
možné	možný	k2eAgFnSc6d1	možná
korupci	korupce	k1gFnSc6	korupce
ohledně	ohledně	k7c2	ohledně
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
obrněných	obrněný	k2eAgInPc2d1	obrněný
transportérů	transportér	k1gInPc2	transportér
pro	pro	k7c4	pro
Armádu	armáda	k1gFnSc4	armáda
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrobce	výrobce	k1gMnSc1	výrobce
Pandurů	pandur	k1gMnPc2	pandur
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
firma	firma	k1gFnSc1	firma
Steyr-Daimler-Puch	Steyr-Daimler-Puch	k1gMnSc1	Steyr-Daimler-Puch
Spezialfahrzeug	Spezialfahrzeug	k1gMnSc1	Spezialfahrzeug
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
General	General	k1gMnSc1	General
Dynamics	Dynamics	k1gInSc1	Dynamics
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
tajnou	tajný	k2eAgFnSc4d1	tajná
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
lobbistou	lobbista	k1gMnSc7	lobbista
Janem	Jan	k1gMnSc7	Jan
Vlčkem	Vlček	k1gMnSc7	Vlček
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
kontakt	kontakt	k1gInSc4	kontakt
mezi	mezi	k7c7	mezi
firmou	firma	k1gFnSc7	firma
a	a	k8xC	a
českými	český	k2eAgMnPc7d1	český
politiky	politik	k1gMnPc7	politik
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
vyplatit	vyplatit	k5eAaPmF	vyplatit
lobbistům	lobbista	k1gMnPc3	lobbista
za	za	k7c2	za
jejich	jejich	k3xOp3gFnSc2	jejich
služby	služba	k1gFnSc2	služba
provizi	provize	k1gFnSc4	provize
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
7	[number]	k4	7
%	%	kIx~	%
ceny	cena	k1gFnSc2	cena
zakázky	zakázka	k1gFnSc2	zakázka
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
jedno	jeden	k4xCgNnSc1	jeden
obrněné	obrněný	k2eAgNnSc1d1	obrněné
vozidlo	vozidlo	k1gNnSc1	vozidlo
Pandur	pandur	k1gMnSc1	pandur
údajně	údajně	k6eAd1	údajně
na	na	k7c4	na
134	[number]	k4	134
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
mělo	mít	k5eAaImAgNnS	mít
platit	platit	k5eAaImF	platit
pouze	pouze	k6eAd1	pouze
cca	cca	kA	cca
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
vozidlo	vozidlo	k1gNnSc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
následně	následně	k6eAd1	následně
vydalo	vydat	k5eAaPmAgNnS	vydat
dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
tiskové	tiskový	k2eAgNnSc1d1	tiskové
prohlášení	prohlášení	k1gNnSc3	prohlášení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
informace	informace	k1gFnPc4	informace
uváděné	uváděný	k2eAgFnPc4d1	uváděná
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
srovnání	srovnání	k1gNnSc4	srovnání
ceny	cena	k1gFnSc2	cena
českých	český	k2eAgNnPc2d1	české
a	a	k8xC	a
portugalských	portugalský	k2eAgNnPc2d1	portugalské
vozidel	vozidlo	k1gNnPc2	vozidlo
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
výbavou	výbava	k1gFnSc7	výbava
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgFnPc1d1	pozemní
síly	síla	k1gFnPc1	síla
AČR	AČR	kA	AČR
obdrží	obdržet	k5eAaPmIp3nP	obdržet
celkem	celkem	k6eAd1	celkem
107	[number]	k4	107
Pandurů	pandur	k1gMnPc2	pandur
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
bude	být	k5eAaImBp3nS	být
99	[number]	k4	99
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
93	[number]	k4	93
%	%	kIx~	%
<g/>
)	)	kIx)	)
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
s	s	k7c7	s
dálkově	dálkově	k6eAd1	dálkově
ovládanou	ovládaný	k2eAgFnSc7d1	ovládaná
věží	věž	k1gFnSc7	věž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
portugalská	portugalský	k2eAgFnSc1d1	portugalská
armáda	armáda	k1gFnSc1	armáda
objednala	objednat	k5eAaPmAgFnS	objednat
260	[number]	k4	260
Pandurů	pandur	k1gMnPc2	pandur
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
32	[number]	k4	32
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
12	[number]	k4	12
%	%	kIx~	%
<g/>
)	)	kIx)	)
vyzbrojeno	vyzbrojen	k2eAgNnSc1d1	vyzbrojeno
podobným	podobný	k2eAgInSc7d1	podobný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
věžového	věžový	k2eAgInSc2d1	věžový
kompletu	komplet	k1gInSc2	komplet
přitom	přitom	k6eAd1	přitom
podle	podle	k7c2	podle
AČR	AČR	kA	AČR
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
40	[number]	k4	40
%	%	kIx~	%
ceny	cena	k1gFnSc2	cena
celého	celý	k2eAgNnSc2d1	celé
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Nepoměr	nepoměr	k1gInSc1	nepoměr
uváděných	uváděný	k2eAgFnPc2d1	uváděná
cen	cena	k1gFnPc2	cena
měl	mít	k5eAaImAgInS	mít
dále	daleko	k6eAd2	daleko
vzniknout	vzniknout	k5eAaPmF	vzniknout
nezapočítáním	nezapočítání	k1gNnSc7	nezapočítání
DPH	DPH	kA	DPH
v	v	k7c6	v
případě	případ	k1gInSc6	případ
portugalské	portugalský	k2eAgFnSc2d1	portugalská
objednávky	objednávka	k1gFnSc2	objednávka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
20	[number]	k4	20
%	%	kIx~	%
rozdíl	rozdíl	k1gInSc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Portugalský	portugalský	k2eAgInSc1d1	portugalský
kontrakt	kontrakt	k1gInSc1	kontrakt
rovněž	rovněž	k9	rovněž
nezahrnoval	zahrnovat	k5eNaImAgInS	zahrnovat
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
logistickou	logistický	k2eAgFnSc4d1	logistická
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
učebně	učebně	k6eAd1	učebně
výcvikovou	výcvikový	k2eAgFnSc4d1	výcviková
základnu	základna	k1gFnSc4	základna
<g/>
,	,	kIx,	,
školení	školení	k1gNnSc4	školení
a	a	k8xC	a
munici	munice	k1gFnSc4	munice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2007	[number]	k4	2007
švédská	švédský	k2eAgFnSc1d1	švédská
veřejnoprávní	veřejnoprávní	k2eAgFnSc1d1	veřejnoprávní
televize	televize	k1gFnSc1	televize
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
sérii	série	k1gFnSc4	série
pořadů	pořad	k1gInPc2	pořad
o	o	k7c6	o
korupci	korupce	k1gFnSc6	korupce
při	při	k7c6	při
nákupech	nákup	k1gInPc6	nákup
nadzvukových	nadzvukový	k2eAgNnPc2d1	nadzvukové
letadel	letadlo	k1gNnPc2	letadlo
Saab	Saab	k1gMnSc1	Saab
JAS-39	JAS-39	k1gMnSc1	JAS-39
Gripen	Gripen	k2eAgInSc4d1	Gripen
pro	pro	k7c4	pro
AČR	AČR	kA	AČR
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
provázela	provázet	k5eAaImAgFnS	provázet
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
i	i	k8xC	i
o	o	k7c6	o
následném	následný	k2eAgInSc6d1	následný
pronájmu	pronájem	k1gInSc6	pronájem
letounů	letoun	k1gInPc2	letoun
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc1	peníz
od	od	k7c2	od
konsorcia	konsorcium	k1gNnSc2	konsorcium
měli	mít	k5eAaImAgMnP	mít
postupně	postupně	k6eAd1	postupně
dorazit	dorazit	k5eAaPmF	dorazit
ke	k	k7c3	k
třem	tři	k4xCgInPc3	tři
prostředníkům	prostředník	k1gInPc3	prostředník
(	(	kIx(	(
<g/>
rakouský	rakouský	k2eAgMnSc1d1	rakouský
obchodník	obchodník	k1gMnSc1	obchodník
Alfons	Alfons	k1gMnSc1	Alfons
Mensdorff-Pouilly	Mensdorff-Pouilla	k1gFnSc2	Mensdorff-Pouilla
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
kanadský	kanadský	k2eAgMnSc1d1	kanadský
politik	politik	k1gMnSc1	politik
Otto	Otto	k1gMnSc1	Otto
Jelinek	Jelinko	k1gNnPc2	Jelinko
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
Omnipolu	omnipol	k1gInSc2	omnipol
Richard	Richard	k1gMnSc1	Richard
Háva	Háva	k1gMnSc1	Háva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
zajistit	zajistit	k5eAaPmF	zajistit
přízeň	přízeň	k1gFnSc4	přízeň
českých	český	k2eAgMnPc2d1	český
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
věci	věc	k1gFnSc2	věc
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
zapleten	zaplést	k5eAaPmNgMnS	zaplést
i	i	k9	i
politik	politik	k1gMnSc1	politik
Jan	Jan	k1gMnSc1	Jan
Kavan	Kavan	k1gMnSc1	Kavan
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
případ	případ	k1gInSc4	případ
následně	následně	k6eAd1	následně
začalo	začít	k5eAaPmAgNnS	začít
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
švédské	švédský	k2eAgNnSc1d1	švédské
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
a	a	k8xC	a
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
obnovila	obnovit	k5eAaPmAgFnS	obnovit
i	i	k9	i
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
obdobné	obdobný	k2eAgFnPc1d1	obdobná
instituce	instituce	k1gFnPc1	instituce
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
uvalil	uvalit	k5eAaPmAgInS	uvalit
rakouský	rakouský	k2eAgInSc1d1	rakouský
soud	soud	k1gInSc1	soud
koluzní	koluznit	k5eAaPmIp3nS	koluznit
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
Alfonse	Alfonso	k1gMnPc4	Alfonso
Mensdorff-Pouilly	Mensdorff-Pouilla	k1gFnSc2	Mensdorff-Pouilla
<g/>
,	,	kIx,	,
o	o	k7c4	o
pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
rakouský	rakouský	k2eAgInSc1d1	rakouský
časopis	časopis	k1gInSc1	časopis
Format	Format	k1gInSc1	Format
část	část	k1gFnSc1	část
tajné	tajný	k2eAgFnPc1d1	tajná
zprávy	zpráva	k1gFnPc1	zpráva
britské	britský	k2eAgFnSc2d1	britská
protikorupční	protikorupční	k2eAgFnSc2d1	protikorupční
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
Mensdorff-Pouilly	Mensdorff-Pouilla	k1gFnSc2	Mensdorff-Pouilla
korupčně	korupčně	k6eAd1	korupčně
manipuloval	manipulovat	k5eAaImAgMnS	manipulovat
politický	politický	k2eAgInSc4d1	politický
proces	proces	k1gInSc4	proces
v	v	k7c6	v
případech	případ	k1gInPc6	případ
nákupů	nákup	k1gInPc2	nákup
letounů	letoun	k1gInPc2	letoun
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
a	a	k8xC	a
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
asi	asi	k9	asi
107,6	[number]	k4	107,6
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
2,9	[number]	k4	2,9
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
misích	mise	k1gFnPc6	mise
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
záloha	záloha	k1gFnSc1	záloha
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Vojenské	vojenský	k2eAgNnSc4d1	vojenské
zařízení	zařízení	k1gNnSc4	zařízení
Army	Arma	k1gMnSc2	Arma
Run	run	k1gInSc4	run
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Armáda	armáda	k1gFnSc1	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
kariéra	kariéra	k1gFnSc1	kariéra
v	v	k7c6	v
Armádě	armáda	k1gFnSc6	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
historie	historie	k1gFnSc2	historie
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
hodnosti	hodnost	k1gFnSc2	hodnost
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Svaz	svaz	k1gInSc4	svaz
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
praporčíků	praporčík	k1gMnPc2	praporčík
Armády	armáda	k1gFnSc2	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Výrobce	výrobce	k1gMnSc1	výrobce
pandurů	pandur	k1gMnPc2	pandur
si	se	k3xPyFc3	se
platil	platit	k5eAaImAgInS	platit
schůzky	schůzka	k1gFnPc4	schůzka
s	s	k7c7	s
politiky	politik	k1gMnPc7	politik
<g/>
.	.	kIx.	.
</s>
<s>
Miliardovou	miliardový	k2eAgFnSc4d1	miliardová
zakázku	zakázka	k1gFnSc4	zakázka
získal	získat	k5eAaPmAgMnS	získat
</s>
