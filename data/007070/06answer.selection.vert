<s>
Armáda	armáda	k1gFnSc1	armáda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
AČR	AČR	kA	AČR
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
částí	část	k1gFnPc2	část
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
řadí	řadit	k5eAaImIp3nS	řadit
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
kancelář	kancelář	k1gFnSc1	kancelář
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Hradní	hradní	k2eAgFnSc1d1	hradní
stráž	stráž	k1gFnSc1	stráž
<g/>
.	.	kIx.	.
</s>
