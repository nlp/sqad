<s>
Kolečkové	kolečkový	k2eAgFnPc1d1	kolečková
brusle	brusle	k1gFnPc1	brusle
jsou	být	k5eAaImIp3nP	být
brusle	brusle	k1gFnPc4	brusle
oblékané	oblékaný	k2eAgFnPc4d1	oblékaná
na	na	k7c4	na
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
místo	místo	k7c2	místo
nožů	nůž	k1gInPc2	nůž
kolečka	kolečko	k1gNnSc2	kolečko
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzným	příbuzný	k2eAgInSc7d1	příbuzný
sportovním	sportovní	k2eAgInSc7d1	sportovní
načiním	načinit	k5eAaPmIp1nS	načinit
jsou	být	k5eAaImIp3nP	být
kolečkové	kolečkový	k2eAgFnSc2d1	kolečková
lyže	lyže	k1gFnSc2	lyže
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
patentované	patentovaný	k2eAgFnPc1d1	patentovaná
kolečkové	kolečkový	k2eAgFnPc1d1	kolečková
brusle	brusle	k1gFnPc1	brusle
byly	být	k5eAaImAgFnP	být
představeny	představit	k5eAaPmNgFnP	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
je	on	k3xPp3gNnSc4	on
belgičan	belgičan	k1gMnSc1	belgičan
John	John	k1gMnSc1	John
Joseph	Joseph	k1gMnSc1	Joseph
Merlin	Merlin	k2eAgMnSc1d1	Merlin
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
typ	typ	k1gInSc4	typ
inline	inlinout	k5eAaPmIp3nS	inlinout
bruslí	brusle	k1gFnSc7	brusle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zprvu	zprvu	k6eAd1	zprvu
tyto	tento	k3xDgFnPc1	tento
brusle	brusle	k1gFnPc1	brusle
nebyly	být	k5eNaImAgFnP	být
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
byly	být	k5eAaImAgInP	být
Jamesem	James	k1gMnSc7	James
Plimptonem	Plimpton	k1gInSc7	Plimpton
vynalezeny	vynalezen	k2eAgInPc1d1	vynalezen
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rocking	rocking	k1gInSc1	rocking
<g/>
"	"	kIx"	"
brusle	brusle	k1gFnSc1	brusle
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vylepšenými	vylepšený	k2eAgFnPc7d1	vylepšená
kolečkovými	kolečkový	k2eAgFnPc7d1	kolečková
bruslemi	brusle	k1gFnPc7	brusle
a	a	k8xC	a
umožnily	umožnit	k5eAaPmAgFnP	umožnit
lepší	dobrý	k2eAgFnPc4d2	lepší
změny	změna	k1gFnPc4	změna
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
brusle	brusle	k1gFnPc1	brusle
již	již	k6eAd1	již
umožnily	umožnit	k5eAaPmAgFnP	umožnit
lepší	dobrý	k2eAgNnSc4d2	lepší
zatáčení	zatáčení	k1gNnSc4	zatáčení
a	a	k8xC	a
projíždění	projíždění	k1gNnSc4	projíždění
okolo	okolo	k7c2	okolo
překážek	překážka	k1gFnPc2	překážka
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
dostávat	dostávat	k5eAaImF	dostávat
mezi	mezi	k7c4	mezi
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Asociace	asociace	k1gFnSc1	asociace
obsluhujících	obsluhující	k2eAgInPc2d1	obsluhující
umělá	umělý	k2eAgNnPc4d1	umělé
kluziště	kluziště	k1gNnPc4	kluziště
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Asociace	asociace	k1gFnSc1	asociace
kolečkového	kolečkový	k2eAgNnSc2d1	kolečkové
bruslení	bruslení	k1gNnSc2	bruslení
<g/>
.	.	kIx.	.
</s>
<s>
Asociace	asociace	k1gFnSc1	asociace
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
zájmy	zájem	k1gInPc4	zájem
kolečkových	kolečkový	k2eAgMnPc2d1	kolečkový
bruslařů	bruslař	k1gMnPc2	bruslař
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
hodiny	hodina	k1gFnPc4	hodina
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
šíření	šíření	k1gNnSc4	šíření
kolečkové	kolečkový	k2eAgNnSc1d1	kolečkové
bruslení	bruslení	k1gNnSc1	bruslení
mezi	mezi	k7c4	mezi
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Nynějším	nynější	k2eAgMnSc7d1	nynější
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
Joe	Joe	k1gFnSc1	Joe
Champa	Champa	k1gFnSc1	Champa
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
Bobby	Bobba	k1gFnPc4	Bobba
Braun	Braun	k1gMnSc1	Braun
<g/>
.	.	kIx.	.
</s>
<s>
Asociace	asociace	k1gFnSc1	asociace
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Indianapolis	Indianapolis	k1gFnSc2	Indianapolis
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
<g/>
,	,	kIx,	,
kolečkové	kolečkový	k2eAgNnSc1d1	kolečkové
bruslení	bruslení	k1gNnSc1	bruslení
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
ze	z	k7c2	z
sportu	sport	k1gInSc2	sport
sloužícího	sloužící	k2eAgInSc2d1	sloužící
k	k	k7c3	k
trávení	trávení	k1gNnSc3	trávení
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
v	v	k7c4	v
soutěžní	soutěžní	k2eAgInSc4d1	soutěžní
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
prvním	první	k4xOgInSc7	první
olympijským	olympijský	k2eAgInSc7d1	olympijský
sportem	sport	k1gInSc7	sport
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hokej	hokej	k1gInSc1	hokej
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
sporty	sport	k1gInPc7	sport
jsou	být	k5eAaImIp3nP	být
rychlobruslení	rychlobruslení	k1gNnSc1	rychlobruslení
<g/>
,	,	kIx,	,
krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
a	a	k8xC	a
roller	roller	k1gInSc1	roller
derby	derby	k1gNnSc2	derby
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
příkladů	příklad	k1gInPc2	příklad
kolečkových	kolečkový	k2eAgFnPc2d1	kolečková
bruslí	brusle	k1gFnPc2	brusle
v	v	k7c6	v
zábavním	zábavní	k2eAgInSc6d1	zábavní
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
videoklip	videoklip	k1gInSc4	videoklip
Madonny	Madonna	k1gFnSc2	Madonna
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Madonna	Madonen	k2eAgFnSc1d1	Madonna
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
sportovci	sportovec	k1gMnPc1	sportovec
bruslí	bruslit	k5eAaImIp3nP	bruslit
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
nedávný	dávný	k2eNgInSc1d1	nedávný
příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
videoklip	videoklip	k1gInSc4	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
A	a	k9	a
Public	publicum	k1gNnPc2	publicum
Affair	Affaira	k1gFnPc2	Affaira
Jessicy	Jessica	k1gFnSc2	Jessica
Simpson	Simpson	k1gNnSc1	Simpson
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
videoklipu	videoklip	k1gInSc6	videoklip
hrají	hrát	k5eAaImIp3nP	hrát
Jessica	Jessicum	k1gNnPc1	Jessicum
Simpson	Simpson	k1gMnSc1	Simpson
<g/>
,	,	kIx,	,
Christina	Christina	k1gFnSc1	Christina
Applegate	Applegat	k1gInSc5	Applegat
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Longoria	Longorium	k1gNnSc2	Longorium
<g/>
,	,	kIx,	,
Christina	Christina	k1gFnSc1	Christina
Milian	Miliana	k1gFnPc2	Miliana
<g/>
,	,	kIx,	,
Andy	Anda	k1gFnSc2	Anda
Dick	Dicka	k1gFnPc2	Dicka
a	a	k8xC	a
Ryan	Ryan	k1gMnSc1	Ryan
Seacrest	Seacrest	k1gMnSc1	Seacrest
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
čtyři	čtyři	k4xCgFnPc1	čtyři
ženy	žena	k1gFnPc1	žena
tančí	tančit	k5eAaImIp3nP	tančit
v	v	k7c6	v
umělém	umělý	k2eAgNnSc6d1	umělé
kluzišti	kluziště	k1gNnSc6	kluziště
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Funny	Funna	k1gFnSc2	Funna
Girl	girl	k1gFnSc2	girl
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
s	s	k7c7	s
Barbrou	Barbra	k1gFnSc7	Barbra
Streisand	Streisanda	k1gFnPc2	Streisanda
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
scéna	scéna	k1gFnSc1	scéna
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
žen	žena	k1gFnPc2	žena
tančících	tančící	k2eAgFnPc2d1	tančící
a	a	k8xC	a
zpívajích	zpívaj	k1gInPc6	zpívaj
sestavu	sestava	k1gFnSc4	sestava
v	v	k7c6	v
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
zasazený	zasazený	k2eAgInSc1d1	zasazený
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Roller	Roller	k1gMnSc1	Roller
skates	skates	k1gMnSc1	skates
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Inline	Inlinout	k5eAaPmIp3nS	Inlinout
brusle	brusle	k1gFnSc1	brusle
Kolečkové	kolečkový	k2eAgFnSc2d1	kolečková
lyže	lyže	k1gFnSc2	lyže
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kolečkové	kolečkový	k2eAgFnSc2d1	kolečková
brusle	brusle	k1gFnSc2	brusle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
InLine	InLin	k1gInSc5	InLin
Skates	Skates	k1gMnSc1	Skates
TV	TV	kA	TV
Historie	historie	k1gFnSc1	historie
kolečkového	kolečkový	k2eAgNnSc2d1	kolečkové
bruslení	bruslení	k1gNnSc2	bruslení
Historie	historie	k1gFnSc1	historie
Předpisy	předpis	k1gInPc1	předpis
Historie	historie	k1gFnSc1	historie
RollerSkating	RollerSkating	k1gInSc1	RollerSkating
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
USA	USA	kA	USA
Roller	Roller	k1gInSc1	Roller
Sports	Sports	k1gInSc1	Sports
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Muzeum	muzeum	k1gNnSc1	muzeum
kolečkového	kolečkový	k2eAgNnSc2d1	kolečkové
bruslení	bruslení	k1gNnSc2	bruslení
Jak	jak	k6eAd1	jak
vybrat	vybrat	k5eAaPmF	vybrat
kolečkové	kolečkový	k2eAgFnPc4d1	kolečková
brusle	brusle	k1gFnPc4	brusle
</s>
