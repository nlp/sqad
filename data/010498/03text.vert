<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Michael	Michael	k1gMnSc1	Michael
Friedek	Friedka	k1gFnPc2	Friedka
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
Gießen	Gießna	k1gFnPc2	Gießna
<g/>
,	,	kIx,	,
Hesensko	Hesensko	k1gNnSc1	Hesensko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
německý	německý	k2eAgMnSc1d1	německý
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
halový	halový	k2eAgMnSc1d1	halový
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
halový	halový	k2eAgMnSc1d1	halový
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
trojskoku	trojskok	k1gInSc6	trojskok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
úspěchy	úspěch	k1gInPc4	úspěch
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
MS	MS	kA	MS
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Maebaši	Maebaš	k1gInSc6	Maebaš
jako	jako	k9	jako
jediný	jediný	k2eAgInSc4d1	jediný
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
překonal	překonat	k5eAaPmAgMnS	překonat
sedmnáctimetrovou	sedmnáctimetrový	k2eAgFnSc4d1	sedmnáctimetrová
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
výkonem	výkon	k1gInSc7	výkon
17,18	[number]	k4	17,18
metru	metr	k1gInSc2	metr
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
uspěl	uspět	k5eAaPmAgMnS	uspět
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
výkonem	výkon	k1gInSc7	výkon
17,59	[number]	k4	17,59
m	m	kA	m
vyrovnal	vyrovnat	k5eAaBmAgMnS	vyrovnat
osobní	osobní	k2eAgInSc4d1	osobní
rekord	rekord	k1gInSc4	rekord
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
zlatý	zlatý	k2eAgInSc1d1	zlatý
úspěch	úspěch	k1gInSc1	úspěch
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
halovém	halový	k2eAgInSc6d1	halový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
belgickém	belgický	k2eAgInSc6d1	belgický
Gentu	Gent	k1gInSc6	Gent
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
titul	titul	k1gInSc1	titul
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
osobním	osobní	k2eAgInSc6d1	osobní
maximu	maxim	k1gInSc6	maxim
17,26	[number]	k4	17,26
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
pozdější	pozdní	k2eAgFnSc4d2	pozdější
kariéru	kariéra	k1gFnSc4	kariéra
narušila	narušit	k5eAaPmAgFnS	narušit
série	série	k1gFnSc1	série
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2000	[number]	k4	2000
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
skončil	skončit	k5eAaPmAgMnS	skončit
bez	bez	k7c2	bez
jediného	jediný	k2eAgInSc2d1	jediný
platného	platný	k2eAgInSc2d1	platný
pokusu	pokus	k1gInSc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
stupni	stupeň	k1gInPc7	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
MS	MS	kA	MS
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c4	o
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k6eAd1	až
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
sérii	série	k1gFnSc6	série
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
cenný	cenný	k2eAgInSc4d1	cenný
kov	kov	k1gInSc4	kov
ho	on	k3xPp3gMnSc4	on
připravil	připravit	k5eAaPmAgMnS	připravit
Brit	Brit	k1gMnSc1	Brit
Jonathan	Jonathan	k1gMnSc1	Jonathan
Edwards	Edwards	k1gInSc4	Edwards
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
před	před	k7c7	před
šestou	šestý	k4xOgFnSc7	šestý
sérií	série	k1gFnSc7	série
figuroval	figurovat	k5eAaImAgInS	figurovat
na	na	k7c6	na
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
sérii	série	k1gFnSc6	série
shodně	shodně	k6eAd1	shodně
doskočil	doskočit	k5eAaPmAgMnS	doskočit
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
17,12	[number]	k4	17,12
m	m	kA	m
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Friedek	Friedek	k1gInSc1	Friedek
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
sérii	série	k1gFnSc6	série
skočil	skočit	k5eAaPmAgMnS	skočit
o	o	k7c4	o
jediný	jediný	k2eAgInSc4d1	jediný
centimetr	centimetr	k1gInSc4	centimetr
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
sérii	série	k1gFnSc6	série
se	se	k3xPyFc4	se
však	však	k9	však
Edwards	Edwards	k1gInSc1	Edwards
zlepšil	zlepšit	k5eAaPmAgInS	zlepšit
na	na	k7c4	na
17,26	[number]	k4	17,26
m	m	kA	m
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
výkonem	výkon	k1gInSc7	výkon
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
obhajovat	obhajovat	k5eAaImF	obhajovat
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Edmontonu	Edmonton	k1gInSc6	Edmonton
<g/>
.	.	kIx.	.
<g/>
Posledního	poslední	k2eAgInSc2d1	poslední
medailového	medailový	k2eAgInSc2d1	medailový
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
na	na	k7c6	na
ME	ME	kA	ME
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
prohrál	prohrát	k5eAaPmAgMnS	prohrát
jen	jen	k9	jen
se	s	k7c7	s
Švédem	Švéd	k1gMnSc7	Švéd
Christianem	Christian	k1gMnSc7	Christian
Olssonem	Olsson	k1gMnSc7	Olsson
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Charles	Charles	k1gMnSc1	Charles
Friedek	Friedek	k1gInSc4	Friedek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
Sports-Reference	Sports-Referenka	k1gFnSc6	Sports-Referenka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
