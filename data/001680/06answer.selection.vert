<s>
René-François-Armand	René-François-Armand	k1gInSc1	René-François-Armand
(	(	kIx(	(
<g/>
Sully	Sulla	k1gFnPc1	Sulla
<g/>
)	)	kIx)	)
Prudhomme	Prudhomme	k1gFnPc1	Prudhomme
[	[	kIx(	[
<g/>
syly	syl	k2eAgFnPc1d1	syl
prydomm	prydomm	k6eAd1	prydomm
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Châtenay-Malabry	Châtenay-Malabr	k1gInPc4	Châtenay-Malabr
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
první	první	k4xOgFnSc2	první
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
