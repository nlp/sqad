<p>
<s>
Bakalář	bakalář	k1gMnSc1	bakalář
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
baccalaureus	baccalaureus	k1gInSc1	baccalaureus
artis	artis	k1gInSc1	artis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
označující	označující	k2eAgMnSc1d1	označující
absolventa	absolvent	k1gMnSc4	absolvent
vysoké	vysoká	k1gFnSc2	vysoká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Zkratka	zkratka	k1gFnSc1	zkratka
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
BcA.	BcA.	k1gFnSc1	BcA.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
existuje	existovat	k5eAaImIp3nS	existovat
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
bakalář	bakalář	k1gMnSc1	bakalář
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
baccalaureus	baccalaureus	k1gInSc1	baccalaureus
<g/>
)	)	kIx)	)
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Bc.	Bc.	k1gFnSc2	Bc.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc1	dva
zkratky	zkratka	k1gFnPc1	zkratka
titulů	titul	k1gInPc2	titul
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
před	před	k7c4	před
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Dosažený	dosažený	k2eAgInSc1d1	dosažený
stupeň	stupeň	k1gInSc1	stupeň
vzdělání	vzdělání	k1gNnSc2	vzdělání
dle	dle	k7c2	dle
ISCED	ISCED	kA	ISCED
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
(	(	kIx(	(
<g/>
bachelor	bachelor	k1gInSc1	bachelor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gNnSc7	degree
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Udělování	udělování	k1gNnSc1	udělování
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
bakalář	bakalář	k1gMnSc1	bakalář
umění	umění	k1gNnSc2	umění
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
i	i	k8xC	i
titulu	titul	k1gInSc2	titul
"	"	kIx"	"
<g/>
bakalář	bakalář	k1gMnSc1	bakalář
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
111	[number]	k4	111
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
pozdějších	pozdní	k2eAgInPc2d2	pozdější
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bakalář	bakalář	k1gMnSc1	bakalář
umění	umění	k1gNnSc2	umění
je	být	k5eAaImIp3nS	být
udělován	udělovat	k5eAaImNgInS	udělovat
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
na	na	k7c6	na
fakultách	fakulta	k1gFnPc6	fakulta
<g/>
,	,	kIx,	,
ústavech	ústav	k1gInPc6	ústav
a	a	k8xC	a
institutech	institut	k1gInPc6	institut
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
akreditovaný	akreditovaný	k2eAgInSc4d1	akreditovaný
některý	některý	k3yIgInSc4	některý
z	z	k7c2	z
uměleckých	umělecký	k2eAgInPc2d1	umělecký
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
standardní	standardní	k2eAgNnSc4d1	standardní
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
bakalářském	bakalářský	k2eAgInSc6d1	bakalářský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
většinou	většinou	k6eAd1	většinou
trvá	trvat	k5eAaImIp3nS	trvat
3-4	[number]	k4	3-4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
výjimečně	výjimečně	k6eAd1	výjimečně
přijati	přijmout	k5eAaPmNgMnP	přijmout
i	i	k9	i
uchazeči	uchazeč	k1gMnPc1	uchazeč
bez	bez	k7c2	bez
dosažení	dosažení	k1gNnSc2	dosažení
středního	střední	k2eAgNnSc2d1	střední
vzdělání	vzdělání	k1gNnSc2	vzdělání
s	s	k7c7	s
maturitní	maturitní	k2eAgFnSc7d1	maturitní
zkouškou	zkouška	k1gFnSc7	zkouška
nebo	nebo	k8xC	nebo
vyššího	vysoký	k2eAgNnSc2d2	vyšší
odborného	odborný	k2eAgNnSc2d1	odborné
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
absolventům	absolvent	k1gMnPc3	absolvent
takto	takto	k6eAd1	takto
přijatým	přijatý	k2eAgMnPc3d1	přijatý
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
až	až	k9	až
po	po	k7c4	po
dosažení	dosažení	k1gNnSc4	dosažení
středního	střední	k2eAgNnSc2d1	střední
vzdělání	vzdělání	k1gNnSc2	vzdělání
s	s	k7c7	s
maturitní	maturitní	k2eAgFnSc7d1	maturitní
zkouškou	zkouška	k1gFnSc7	zkouška
nebo	nebo	k8xC	nebo
vyššího	vysoký	k2eAgNnSc2d2	vyšší
odborného	odborný	k2eAgNnSc2d1	odborné
vzdělání	vzdělání	k1gNnSc2	vzdělání
v	v	k7c6	v
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
studium	studium	k1gNnSc4	studium
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
udělení	udělení	k1gNnSc3	udělení
tohoto	tento	k3xDgInSc2	tento
gradu	grad	k1gInSc2	grad
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
především	především	k9	především
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
povolání	povolání	k1gNnSc2	povolání
a	a	k8xC	a
též	též	k9	též
i	i	k9	i
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
absolvent	absolvent	k1gMnSc1	absolvent
<g/>
,	,	kIx,	,
bakalář	bakalář	k1gMnSc1	bakalář
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
případně	případně	k6eAd1	případně
může	moct	k5eAaImIp3nS	moct
přihlásit	přihlásit	k5eAaPmF	přihlásit
i	i	k9	i
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
vysokoškolskému	vysokoškolský	k2eAgNnSc3d1	vysokoškolské
studiu	studio	k1gNnSc3	studio
v	v	k7c6	v
(	(	kIx(	(
<g/>
navazujícím	navazující	k2eAgInSc6d1	navazující
<g/>
)	)	kIx)	)
magisterském	magisterský	k2eAgInSc6d1	magisterský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
následně	následně	k6eAd1	následně
zpravidla	zpravidla	k6eAd1	zpravidla
trvá	trvat	k5eAaImIp3nS	trvat
1-3	[number]	k4	1-3
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
bakalant	bakalant	k1gMnSc1	bakalant
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
student	student	k1gMnSc1	student
bakalářského	bakalářský	k2eAgInSc2d1	bakalářský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
student	student	k1gMnSc1	student
pracující	pracující	k1gMnSc1	pracující
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
bakalářské	bakalářský	k2eAgFnSc6d1	Bakalářská
práci	práce	k1gFnSc6	práce
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářské	bakalářský	k2eAgNnSc1d1	bakalářské
studium	studium	k1gNnSc1	studium
se	se	k3xPyFc4	se
řádné	řádný	k2eAgFnSc2d1	řádná
ukončuje	ukončovat	k5eAaImIp3nS	ukončovat
státní	státní	k2eAgFnSc7d1	státní
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
(	(	kIx(	(
<g/>
státnice	státnice	k1gFnSc1	státnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
většinou	většina	k1gFnSc7	většina
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
bakalářské	bakalářský	k2eAgFnSc2d1	Bakalářská
(	(	kIx(	(
<g/>
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
umělecké	umělecký	k2eAgFnPc4d1	umělecká
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
bývá	bývat	k5eAaImIp3nS	bývat
typický	typický	k2eAgInSc1d1	typický
určitý	určitý	k2eAgInSc4d1	určitý
umělecký	umělecký	k2eAgInSc4d1	umělecký
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
základním	základní	k2eAgInSc7d1	základní
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
titulem	titul	k1gInSc7	titul
–	–	k?	–
vysokoškolskou	vysokoškolský	k2eAgFnSc7d1	vysokoškolská
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
–	–	k?	–
udělovanou	udělovaný	k2eAgFnSc4d1	udělovaná
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Bakalář	bakalář	k1gMnSc1	bakalář
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
tyto	tento	k3xDgInPc4	tento
tituly	titul	k1gInPc4	titul
nižší	nízký	k2eAgFnSc2d2	nižší
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
běžně	běžně	k6eAd1	běžně
neužívají	užívat	k5eNaImIp3nP	užívat
(	(	kIx(	(
<g/>
nepíší	psát	k5eNaImIp3nP	psát
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jejich	jejich	k3xOp3gNnSc4	jejich
formální	formální	k2eAgNnSc4d1	formální
užívání	užívání	k1gNnSc4	užívání
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
častější	častý	k2eAgNnSc1d2	častější
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ani	ani	k8xC	ani
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
formální	formální	k2eAgNnSc1d1	formální
oslovování	oslovování	k1gNnSc1	oslovování
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pane	pan	k1gMnSc5	pan
bakaláři	bakalář	k1gMnPc5	bakalář
/	/	kIx~	/
paní	paní	k1gFnSc1	paní
bakalářko	bakalářka	k1gFnSc5	bakalářka
zpravidla	zpravidla	k6eAd1	zpravidla
běžně	běžně	k6eAd1	běžně
neužívá	užívat	k5eNaImIp3nS	užívat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Baccalaureatus	Baccalaureatus	k1gInSc1	Baccalaureatus
(	(	kIx(	(
<g/>
vavřínem	vavřín	k1gInSc7	vavřín
ověnčený	ověnčený	k2eAgMnSc1d1	ověnčený
<g/>
)	)	kIx)	)
označoval	označovat	k5eAaImAgMnS	označovat
původně	původně	k6eAd1	původně
na	na	k7c6	na
středověké	středověký	k2eAgFnSc6d1	středověká
univerzitě	univerzita	k1gFnSc6	univerzita
akademický	akademický	k2eAgInSc4d1	akademický
gradus	gradus	k1gInSc4	gradus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
student	student	k1gMnSc1	student
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
trivia	trivium	k1gNnSc2	trivium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tvořila	tvořit	k5eAaImAgFnS	tvořit
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
dialektika	dialektika	k1gFnSc1	dialektika
a	a	k8xC	a
rétorika	rétorika	k1gFnSc1	rétorika
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
absolvent	absolvent	k1gMnSc1	absolvent
vypomáhal	vypomáhat	k5eAaImAgMnS	vypomáhat
s	s	k7c7	s
učením	učení	k1gNnSc7	učení
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
kvadriviu	kvadrivium	k1gNnSc3	kvadrivium
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
titul	titul	k1gInSc1	titul
získával	získávat	k5eAaImAgInS	získávat
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
artistické	artistický	k2eAgFnSc2d1	artistická
fakulty	fakulta	k1gFnSc2	fakulta
a	a	k8xC	a
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
ostatní	ostatní	k1gNnSc4	ostatní
fakulty	fakulta	k1gFnSc2	fakulta
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Bakalář	bakalář	k1gMnSc1	bakalář
následně	následně	k6eAd1	následně
po	po	k7c6	po
dalším	další	k2eAgNnSc6d1	další
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
obhajobě	obhajoba	k1gFnSc3	obhajoba
samostatné	samostatný	k2eAgFnSc2d1	samostatná
odborné	odborný	k2eAgFnSc2d1	odborná
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
these	these	k1gFnPc1	these
<g/>
)	)	kIx)	)
mohl	moct	k5eAaImAgInS	moct
získat	získat	k5eAaPmF	získat
gradus	gradus	k1gInSc4	gradus
mistra	mistr	k1gMnSc4	mistr
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
magister	magister	k1gMnSc1	magister
–	–	k?	–
magistra	magistra	k1gFnSc1	magistra
<g/>
)	)	kIx)	)
na	na	k7c6	na
artistické	artistický	k2eAgFnSc6d1	artistická
fakultě	fakulta	k1gFnSc6	fakulta
(	(	kIx(	(
<g/>
fakultě	fakulta	k1gFnSc6	fakulta
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
jeho	jeho	k3xOp3gInSc1	jeho
celý	celý	k2eAgInSc1d1	celý
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
magistr	magistr	k1gMnSc1	magistr
svobodných	svobodný	k2eAgNnPc2d1	svobodné
umění	umění	k1gNnPc2	umění
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
byl	být	k5eAaImAgMnS	být
titul	titul	k1gInSc4	titul
bakaláře	bakalář	k1gMnSc2	bakalář
znovu	znovu	k6eAd1	znovu
zaveden	zaveden	k2eAgInSc1d1	zaveden
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
také	také	k9	také
i	i	k9	i
české	český	k2eAgInPc1d1	český
akademické	akademický	k2eAgInPc1d1	akademický
tituly	titul	k1gInPc1	titul
lépe	dobře	k6eAd2	dobře
odpovídaly	odpovídat	k5eAaImAgInP	odpovídat
titulům	titul	k1gInPc3	titul
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
titulům	titul	k1gInPc3	titul
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
<g/>
:	:	kIx,	:
určitým	určitý	k2eAgInPc3d1	určitý
stupňům	stupeň	k1gInPc3	stupeň
formální	formální	k2eAgFnSc2d1	formální
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
)	)	kIx)	)
z	z	k7c2	z
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
revoluce	revoluce	k1gFnSc2	revoluce
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
socialistickém	socialistický	k2eAgNnSc6d1	socialistické
Československu	Československo	k1gNnSc6	Československo
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
"	"	kIx"	"
<g/>
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
"	"	kIx"	"
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
studium	studium	k1gNnSc4	studium
(	(	kIx(	(
<g/>
blíže	blízce	k6eAd2	blízce
<g/>
:	:	kIx,	:
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
architekt	architekt	k1gMnSc1	architekt
<g/>
)	)	kIx)	)
a	a	k8xC	a
případně	případně	k6eAd1	případně
následná	následný	k2eAgFnSc1d1	následná
aspirantura	aspirantura	k1gFnSc1	aspirantura
(	(	kIx(	(
<g/>
blíže	blíže	k1gFnSc1	blíže
<g/>
:	:	kIx,	:
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
dvoustupňový	dvoustupňový	k2eAgInSc4d1	dvoustupňový
systém	systém	k1gInSc4	systém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
po	po	k7c6	po
přechodu	přechod	k1gInSc6	přechod
od	od	k7c2	od
státem	stát	k1gInSc7	stát
centrálně	centrálně	k6eAd1	centrálně
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
hospodářství	hospodářství	k1gNnSc2	hospodářství
k	k	k7c3	k
tržní	tržní	k2eAgFnSc3d1	tržní
ekonomice	ekonomika	k1gFnSc3	ekonomika
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
zavedena	zaveden	k2eAgFnSc1d1	zavedena
standardní	standardní	k2eAgFnSc1d1	standardní
třístupňová	třístupňový	k2eAgFnSc1d1	třístupňová
vysokoškolská	vysokoškolský	k2eAgFnSc1d1	vysokoškolská
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tato	tento	k3xDgFnSc1	tento
soustava	soustava	k1gFnSc1	soustava
bývá	bývat	k5eAaImIp3nS	bývat
obecně	obecně	k6eAd1	obecně
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
ekonomicky	ekonomicky	k6eAd1	ekonomicky
únosnější	únosný	k2eAgInPc1d2	únosnější
pro	pro	k7c4	pro
nekomunistické	komunistický	k2eNgInPc4d1	nekomunistický
(	(	kIx(	(
<g/>
kapitalistické	kapitalistický	k2eAgInPc4d1	kapitalistický
<g/>
)	)	kIx)	)
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
rozdělení	rozdělení	k1gNnSc3	rozdělení
"	"	kIx"	"
<g/>
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
magisterského	magisterský	k2eAgMnSc2d1	magisterský
<g/>
)	)	kIx)	)
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
172	[number]	k4	172
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vedlo	vést	k5eAaImAgNnS	vést
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
bakalářského	bakalářský	k2eAgInSc2d1	bakalářský
gradu	grad	k1gInSc2	grad
<g/>
,	,	kIx,	,
označovalo	označovat	k5eAaImAgNnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
obsahově	obsahově	k6eAd1	obsahově
<g/>
)	)	kIx)	)
ucelená	ucelený	k2eAgFnSc1d1	ucelená
část	část	k1gFnSc1	část
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
ještě	ještě	k9	ještě
variantu	varianta	k1gFnSc4	varianta
BcA.	BcA.	k1gFnSc2	BcA.
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
MgA.	MgA.	k1gFnSc4	MgA.
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
umělce	umělec	k1gMnPc4	umělec
neodlišoval	odlišovat	k5eNaImAgInS	odlišovat
<g/>
,	,	kIx,	,
udílen	udílen	k2eAgInSc1d1	udílen
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
bakalář	bakalář	k1gMnSc1	bakalář
<g/>
"	"	kIx"	"
–	–	k?	–
Bc.	Bc.	k1gMnSc1	Bc.
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
zavedení	zavedení	k1gNnSc4	zavedení
BcA.	BcA.	k1gFnSc2	BcA.
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
též	též	k9	též
po	po	k7c6	po
Boloňském	boloňský	k2eAgInSc6d1	boloňský
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stal	stát	k5eAaPmAgInS	stát
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
"	"	kIx"	"
<g/>
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
studium	studium	k1gNnSc4	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
transformovalo	transformovat	k5eAaBmAgNnS	transformovat
do	do	k7c2	do
magisterského	magisterský	k2eAgInSc2d1	magisterský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
označované	označovaný	k2eAgFnSc2d1	označovaná
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
jakožto	jakožto	k8xS	jakožto
doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
dle	dle	k7c2	dle
§	§	k?	§
99	[number]	k4	99
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
téhož	týž	k3xTgInSc2	týž
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
titulem	titul	k1gInSc7	titul
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
i	i	k9	i
titul	titul	k1gInSc1	titul
bakalář	bakalář	k1gMnSc1	bakalář
(	(	kIx(	(
<g/>
Bc.	Bc.	k1gMnSc1	Bc.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
získali	získat	k5eAaPmAgMnP	získat
podle	podle	k7c2	podle
§	§	k?	§
21	[number]	k4	21
staršího	starý	k2eAgInSc2d2	starší
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
172	[number]	k4	172
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
absolventi	absolvent	k1gMnPc1	absolvent
obsahově	obsahově	k6eAd1	obsahově
ucelené	ucelený	k2eAgFnSc2d1	ucelená
části	část	k1gFnSc2	část
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
bakalářského	bakalářský	k2eAgNnSc2d1	bakalářské
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Osvědčení	osvědčení	k1gNnSc4	osvědčení
o	o	k7c4	o
nahrazení	nahrazení	k1gNnSc4	nahrazení
tohoto	tento	k3xDgInSc2	tento
akademického	akademický	k2eAgInSc2d1	akademický
titulu	titul	k1gInSc2	titul
jim	on	k3xPp3gMnPc3	on
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
vydá	vydat	k5eAaPmIp3nS	vydat
příslušná	příslušný	k2eAgFnSc1d1	příslušná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
se	se	k3xPyFc4	se
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
BcA.	BcA.	k1gFnSc1	BcA.
<g/>
)	)	kIx)	)
neuděluje	udělovat	k5eNaImIp3nS	udělovat
–	–	k?	–
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
zde	zde	k6eAd1	zde
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
titul	titul	k1gInSc4	titul
(	(	kIx(	(
<g/>
Bc.	Bc.	k1gFnSc4	Bc.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obdobné	obdobný	k2eAgInPc4d1	obdobný
tituly	titul	k1gInPc4	titul
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c4	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postupně	postupně	k6eAd1	postupně
sjednocoval	sjednocovat	k5eAaImAgInS	sjednocovat
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
systém	systém	k1gInSc4	systém
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
cyklů	cyklus	k1gInPc2	cyklus
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
světa	svět	k1gInSc2	svět
tzv.	tzv.	kA	tzv.
Boloňský	boloňský	k2eAgInSc4d1	boloňský
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
uděluje	udělovat	k5eAaImIp3nS	udělovat
de	de	k?	de
facto	facto	k1gNnSc1	facto
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
standardní	standardní	k2eAgInSc4d1	standardní
<g/>
,	,	kIx,	,
základní	základní	k2eAgInSc4d1	základní
vysokoškolský	vysokoškolský	k2eAgInSc4d1	vysokoškolský
titul	titul	k1gInSc4	titul
(	(	kIx(	(
<g/>
stupeň	stupeň	k1gInSc1	stupeň
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
označuje	označovat	k5eAaImIp3nS	označovat
absolventa	absolvent	k1gMnSc4	absolvent
prvního	první	k4xOgNnSc2	první
cyklu	cyklus	k1gInSc2	cyklus
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
blíže	blíž	k1gFnSc2	blíž
<g/>
:	:	kIx,	:
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bakalářského	bakalářský	k2eAgInSc2d1	bakalářský
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
trvání	trvání	k1gNnSc6	trvání
tří	tři	k4xCgInPc2	tři
až	až	k8xS	až
čtyřletého	čtyřletý	k2eAgNnSc2d1	čtyřleté
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
bakaláře	bakalář	k1gMnSc2	bakalář
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
uděluje	udělovat	k5eAaImIp3nS	udělovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
podobách	podoba	k1gFnPc6	podoba
(	(	kIx(	(
<g/>
či	či	k8xC	či
zkratkách	zkratka	k1gFnPc6	zkratka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
B.A.	B.A.	k1gFnSc1	B.A.
(	(	kIx(	(
<g/>
či	či	k8xC	či
BA	ba	k9	ba
<g/>
,	,	kIx,	,
z	z	k7c2	z
lat.	lat.	k?	lat.
baccalaureus	baccalaureus	k1gInSc1	baccalaureus
artium	artium	k1gNnSc1	artium
<g/>
)	)	kIx)	)
–	–	k?	–
Bachelor	Bachelor	k1gInSc1	Bachelor
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
značí	značit	k5eAaImIp3nS	značit
bakaláře	bakalář	k1gMnSc2	bakalář
humanitních	humanitní	k2eAgFnPc2d1	humanitní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
B.	B.	kA	B.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
S.	S.	kA	S.
(	(	kIx(	(
<g/>
BS	BS	kA	BS
<g/>
,	,	kIx,	,
B.	B.	kA	B.
<g/>
Sc	Sc	k1gFnSc1	Sc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
BSc	BSc	k1gFnSc1	BSc
<g/>
,	,	kIx,	,
či	či	k8xC	či
BSc	BSc	k1gFnSc1	BSc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
z	z	k7c2	z
lat.	lat.	k?	lat.
baccalaureus	baccalaureus	k1gInSc1	baccalaureus
scientia	scientia	k1gFnSc1	scientia
<g/>
)	)	kIx)	)
–	–	k?	–
Bachelor	Bachelor	k1gInSc1	Bachelor
of	of	k?	of
Science	Science	k1gFnSc1	Science
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značí	značit	k5eAaImIp3nS	značit
bakaláře	bakalář	k1gMnSc2	bakalář
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
udělují	udělovat	k5eAaImIp3nP	udělovat
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Bachelor	Bachelor	k1gInSc1	Bachelor
of	of	k?	of
Engineering	Engineering	k1gInSc1	Engineering
<g/>
,	,	kIx,	,
Bachelor	Bachelor	k1gInSc1	Bachelor
of	of	k?	of
Laws	Laws	k1gInSc1	Laws
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Bachelor	Bachelor	k1gInSc1	Bachelor
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Arts	k1gInSc1	Arts
(	(	kIx(	(
<g/>
BFA	BFA	kA	BFA
<g/>
)	)	kIx)	)
–	–	k?	–
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
bakalariát	bakalariát	k1gInSc1	bakalariát
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
umění	umění	k1gNnSc2	umění
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
</s>
</p>
<p>
<s>
Akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Akademický	akademický	k2eAgMnSc1d1	akademický
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Bakalář	bakalář	k1gMnSc1	bakalář
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bakalář	bakalář	k1gMnSc1	bakalář
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Bakalář	bakalář	k1gMnSc1	bakalář
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
