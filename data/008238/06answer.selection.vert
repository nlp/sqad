<s>
Zkratka	zkratka	k1gFnSc1	zkratka
tohoto	tento	k3xDgInSc2	tento
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
BcA.	BcA.	k1gFnSc1	BcA.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
existuje	existovat	k5eAaImIp3nS	existovat
varianta	varianta	k1gFnSc1	varianta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
<g/>
,	,	kIx,	,
bakalář	bakalář	k1gMnSc1	bakalář
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
baccalaureus	baccalaureus	k1gInSc1	baccalaureus
<g/>
)	)	kIx)	)
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
Bc.	Bc.	k1gFnSc2	Bc.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc1	dva
zkratky	zkratka	k1gFnPc1	zkratka
titulů	titul	k1gInPc2	titul
se	se	k3xPyFc4	se
případně	případně	k6eAd1	případně
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
před	před	k7c4	před
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
