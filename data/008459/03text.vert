<p>
<s>
Halti	Halti	k1gNnSc1	Halti
(	(	kIx(	(
<g/>
též	též	k9	též
Hálditšohkka	Hálditšohkko	k1gNnSc2	Hálditšohkko
nebo	nebo	k8xC	nebo
Haltitunturi	Haltitunturi	k1gNnSc2	Haltitunturi
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
ve	v	k7c6	v
Skandinávském	skandinávský	k2eAgNnSc6d1	skandinávské
pohoří	pohoří	k1gNnSc6	pohoří
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrcholek	vrcholek	k1gInSc1	vrcholek
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(	(
<g/>
1324	[number]	k4	1324
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
z	z	k7c2	z
finských	finský	k2eAgFnPc2d1	finská
tisícovek	tisícovka	k1gFnPc2	tisícovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vrchol	vrchol	k1gInSc1	vrchol
<g/>
(	(	kIx(	(
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vrchol	vrchol	k1gInSc1	vrchol
hory	hora	k1gFnSc2	hora
Halti	Halť	k1gFnSc2	Halť
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1,2	[number]	k4	1,2
km	km	kA	km
od	od	k7c2	od
finské	finský	k2eAgFnSc2d1	finská
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
katastrů	katastr	k1gInPc2	katastr
obcí	obec	k1gFnPc2	obec
Kå	Kå	k1gFnSc2	Kå
a	a	k8xC	a
Nordreisa	Nordreisa	k1gFnSc1	Nordreisa
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1361	[number]	k4	1361
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Ráisduattarháldi	Ráisduattarháld	k1gMnPc1	Ráisduattarháld
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
vrchol	vrchol	k1gInSc1	vrchol
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
finské	finský	k2eAgFnSc2d1	finská
hranice	hranice	k1gFnSc2	hranice
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
1328	[number]	k4	1328
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
prominence	prominence	k1gFnSc1	prominence
38	[number]	k4	38
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
sedla	sedlo	k1gNnSc2	sedlo
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
vrcholem	vrchol	k1gInSc7	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
vrcholem	vrchol	k1gInSc7	vrchol
u	u	k7c2	u
hraničního	hraniční	k2eAgInSc2d1	hraniční
kamene	kámen	k1gInSc2	kámen
303B	[number]	k4	303B
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
místo	místo	k1gNnSc1	místo
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
1324	[number]	k4	1324
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Tento	tento	k3xDgInSc4	tento
vrchol	vrchol	k1gInSc4	vrchol
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
Enontekiö	Enontekiö	k1gFnSc2	Enontekiö
v	v	k7c6	v
Laponsku	Laponsko	k1gNnSc6	Laponsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
souřadnicích	souřadnice	k1gFnPc6	souřadnice
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
na	na	k7c6	na
území	území	k1gNnSc6	území
Finska	Finsko	k1gNnSc2	Finsko
je	být	k5eAaImIp3nS	být
necelé	celý	k2eNgNnSc4d1	necelé
4	[number]	k4	4
km	km	kA	km
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
Ritničohkka	Ritničohkka	k1gFnSc1	Ritničohkka
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
1316	[number]	k4	1316
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
Halti	Halť	k1gFnSc2	Halť
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
z	z	k7c2	z
finské	finský	k2eAgFnSc2d1	finská
i	i	k8xC	i
z	z	k7c2	z
norské	norský	k2eAgFnSc2d1	norská
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
norské	norský	k2eAgFnSc2d1	norská
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
,	,	kIx,	,
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
lze	lze	k6eAd1	lze
dojet	dojet	k5eAaPmF	dojet
autem	auto	k1gNnSc7	auto
asi	asi	k9	asi
5	[number]	k4	5
km	km	kA	km
pod	pod	k7c4	pod
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
<g/>
)	)	kIx)	)
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
nastoupat	nastoupat	k5eAaPmF	nastoupat
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
svahem	svah	k1gInSc7	svah
asi	asi	k9	asi
600	[number]	k4	600
výškových	výškový	k2eAgInPc2d1	výškový
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
finské	finský	k2eAgFnSc2d1	finská
strany	strana	k1gFnSc2	strana
začíná	začínat	k5eAaImIp3nS	začínat
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Kilpisjärvi	Kilpisjärev	k1gFnSc6	Kilpisjärev
pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
Saana	Saano	k1gNnSc2	Saano
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc1d2	delší
-	-	kIx~	-
zpáteční	zpáteční	k2eAgFnSc1d1	zpáteční
cesta	cesta	k1gFnSc1	cesta
měří	měřit	k5eAaImIp3nS	měřit
kolem	kolem	k7c2	kolem
100	[number]	k4	100
km	km	kA	km
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
pět	pět	k4xCc4	pět
až	až	k9	až
šest	šest	k4xCc4	šest
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
jsou	být	k5eAaImIp3nP	být
chatky	chatka	k1gFnPc1	chatka
na	na	k7c6	na
přespání	přespání	k1gNnSc6	přespání
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Laavua	Laavua	k1gFnSc1	Laavua
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
doba	doba	k1gFnSc1	doba
pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
je	být	k5eAaImIp3nS	být
přelom	přelom	k1gInSc4	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Halti	Halť	k1gFnSc2	Halť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Podrobný	podrobný	k2eAgInSc4d1	podrobný
popis	popis	k1gInSc4	popis
výstupu	výstup	k1gInSc2	výstup
na	na	k7c4	na
HoryEvropy	HoryEvrop	k1gInPc4	HoryEvrop
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
z	z	k7c2	z
norské	norský	k2eAgFnSc2d1	norská
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podrobný	podrobný	k2eAgInSc4d1	podrobný
popis	popis	k1gInSc4	popis
výstupu	výstup	k1gInSc2	výstup
na	na	k7c4	na
Blok	blok	k1gInSc4	blok
<g/>
.	.	kIx.	.
<g/>
v	v	k7c4	v
<g/>
0	[number]	k4	0
<g/>
174	[number]	k4	174
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
(	(	kIx(	(
<g/>
z	z	k7c2	z
finské	finský	k2eAgFnSc2d1	finská
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Halti	Halti	k1gNnSc1	Halti
na	na	k7c4	na
SummitPost	SummitPost	k1gFnSc4	SummitPost
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Halti	Halť	k1gFnPc1	Halť
na	na	k7c4	na
Peakbagger	Peakbagger	k1gInSc4	Peakbagger
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Halti	Halť	k1gFnPc1	Halť
na	na	k7c6	na
topografické	topografický	k2eAgFnSc6d1	topografická
mapě	mapa	k1gFnSc6	mapa
Finska	Finsko	k1gNnSc2	Finsko
</s>
</p>
