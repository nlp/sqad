<s>
Halti	Halti	k1gNnSc1	Halti
(	(	kIx(	(
<g/>
též	též	k9	též
Hálditšohkka	Hálditšohkko	k1gNnSc2	Hálditšohkko
nebo	nebo	k8xC	nebo
Haltitunturi	Haltitunturi	k1gNnSc2	Haltitunturi
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
Norska	Norsko	k1gNnSc2	Norsko
ve	v	k7c6	v
Skandinávském	skandinávský	k2eAgNnSc6d1	skandinávské
pohoří	pohoří	k1gNnSc6	pohoří
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrcholek	vrcholek	k1gInSc1	vrcholek
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(	(
<g/>
1324	[number]	k4	1324
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
z	z	k7c2	z
finských	finský	k2eAgFnPc2d1	finská
tisícovek	tisícovka	k1gFnPc2	tisícovka
<g/>
.	.	kIx.	.
</s>
