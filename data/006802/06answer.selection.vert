<s>
Letopisy	letopis	k1gInPc1	letopis
Narnie	Narnie	k1gFnSc2	Narnie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Chronicles	Chronicles	k1gMnSc1	Chronicles
of	of	k?	of
Narnia	Narnium	k1gNnSc2	Narnium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sedmidílný	sedmidílný	k2eAgInSc1d1	sedmidílný
cyklus	cyklus	k1gInSc1	cyklus
fantasy	fantas	k1gInPc4	fantas
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
britského	britský	k2eAgMnSc2d1	britský
autora	autor	k1gMnSc2	autor
Clive	Cliev	k1gFnSc2	Cliev
Staples	Staplesa	k1gFnPc2	Staplesa
Lewise	Lewise	k1gFnSc2	Lewise
<g/>
.	.	kIx.	.
</s>
