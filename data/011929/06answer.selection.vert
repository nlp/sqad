<s>
Chuť	chuť	k1gFnSc1
je	být	k5eAaImIp3nS
smysl	smysl	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1
dovoluje	dovolovat	k5eAaImIp3nS
vnímat	vnímat	k5eAaImF
chemické	chemický	k2eAgFnSc2d1
látky	látka	k1gFnSc2
rozpuštěné	rozpuštěný	k2eAgFnSc2d1
ve	v	k7c6
slinách	slina	k1gFnPc6
nebo	nebo	k8xC
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>