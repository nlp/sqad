<s>
Hector	Hector	k1gMnSc1	Hector
Samuel	Samuel	k1gMnSc1	Samuel
Juan	Juan	k1gMnSc1	Juan
"	"	kIx"	"
<g/>
Tico	Tico	k1gMnSc1	Tico
<g/>
"	"	kIx"	"
Torres	Torres	k1gMnSc1	Torres
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1953	[number]	k4	1953
New	New	k1gMnSc2	New
York	York	k1gInSc4	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
Bon	bon	k1gInSc1	bon
Jovi	Jove	k1gFnSc4	Jove
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1953	[number]	k4	1953
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
Emma	Emma	k1gFnSc1	Emma
a	a	k8xC	a
Hector	Hector	k1gMnSc1	Hector
se	se	k3xPyFc4	se
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
-	-	kIx~	-
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc1	Jovi
1985	[number]	k4	1985
-	-	kIx~	-
7800	[number]	k4	7800
<g/>
°	°	k?	°
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
1986	[number]	k4	1986
-	-	kIx~	-
Slippery	Slippera	k1gFnSc2	Slippera
When	When	k1gInSc1	When
Wet	Wet	k1gFnSc4	Wet
1988	[number]	k4	1988
-	-	kIx~	-
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
1992	[number]	k4	1992
-	-	kIx~	-
Keep	Keep	k1gMnSc1	Keep
The	The	k1gMnSc1	The
Faith	Faith	k1gMnSc1	Faith
1994	[number]	k4	1994
-	-	kIx~	-
Crossroad	Crossroad	k1gInSc1	Crossroad
-	-	kIx~	-
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Bon	bon	k1gInSc1	bon
Jovi	Jovi	k1gNnSc1	Jovi
1995	[number]	k4	1995
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
These	these	k1gFnSc1	these
Days	Days	k1gInSc1	Days
2000	[number]	k4	2000
-	-	kIx~	-
Crush	Crush	k1gInSc1	Crush
2001	[number]	k4	2001
-	-	kIx~	-
One	One	k1gMnSc1	One
Wild	Wild	k1gMnSc1	Wild
Night	Night	k1gMnSc1	Night
Live	Live	k1gInSc4	Live
1985-2001	[number]	k4	1985-2001
2002	[number]	k4	2002
-	-	kIx~	-
Bounce	Bounec	k1gInSc2	Bounec
2003	[number]	k4	2003
-	-	kIx~	-
This	This	k1gInSc4	This
Left	Left	k2eAgInSc4d1	Left
Feels	Feels	k1gInSc4	Feels
Right	Right	k1gInSc1	Right
2004	[number]	k4	2004
-	-	kIx~	-
100,000,000	[number]	k4	100,000,000
Bon	bon	k1gInSc1	bon
Jovi	Jov	k1gFnSc2	Jov
Fans	Fansa	k1gFnPc2	Fansa
Can	Can	k1gFnSc7	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gMnSc1	Be
Wrong	Wrong	k1gMnSc1	Wrong
<g/>
...	...	k?	...
2005	[number]	k4	2005
-	-	kIx~	-
Have	Hav	k1gInSc2	Hav
A	a	k9	a
Nice	Nice	k1gFnSc1	Nice
Day	Day	k1gFnSc1	Day
2007	[number]	k4	2007
-	-	kIx~	-
Lost	Lost	k1gInSc1	Lost
Highway	Highwaa	k1gFnSc2	Highwaa
2009	[number]	k4	2009
-	-	kIx~	-
The	The	k1gFnPc2	The
Circle	Circle	k1gNnPc2	Circle
2010	[number]	k4	2010
-	-	kIx~	-
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
1991	[number]	k4	1991
-	-	kIx~	-
Strangers	Strangers	k1gInSc1	Strangers
in	in	k?	in
This	This	k1gInSc1	This
Town	Town	k1gInSc1	Town
</s>
