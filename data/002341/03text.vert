<s>
Chlamydióza	Chlamydióza	k1gFnSc1	Chlamydióza
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
onemocnění	onemocnění	k1gNnSc4	onemocnění
lidí	člověk	k1gMnPc2	člověk
i	i	k8xC	i
zvířat	zvíře	k1gNnPc2	zvíře
způsobované	způsobovaný	k2eAgInPc1d1	způsobovaný
obligátně	obligátně	k6eAd1	obligátně
intracelulárními	intracelulární	k2eAgFnPc7d1	intracelulární
bakteriemi	bakterie	k1gFnPc7	bakterie
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
Chlamydiales	Chlamydialesa	k1gFnPc2	Chlamydialesa
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
člověka	člověk	k1gMnSc2	člověk
bakterií	bakterie	k1gFnPc2	bakterie
Chlamydophila	Chlamydophil	k1gMnSc2	Chlamydophil
psittaci	psittace	k1gFnSc4	psittace
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
příznaků	příznak	k1gInPc2	příznak
atypických	atypický	k2eAgInPc2d1	atypický
zánětů	zánět	k1gInPc2	zánět
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
lidí	člověk	k1gMnPc2	člověk
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
při	při	k7c6	při
chovu	chov	k1gInSc6	chov
a	a	k8xC	a
ošetřování	ošetřování	k1gNnSc1	ošetřování
krůt	krůta	k1gFnPc2	krůta
a	a	k8xC	a
kachen	kachna	k1gFnPc2	kachna
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
porážce	porážka	k1gFnSc6	porážka
i	i	k8xC	i
zpracovávání	zpracovávání	k1gNnSc6	zpracovávání
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
tak	tak	k9	tak
u	u	k7c2	u
chovatelů	chovatel	k1gMnPc2	chovatel
holubů	holub	k1gMnPc2	holub
nebo	nebo	k8xC	nebo
exotických	exotický	k2eAgMnPc2d1	exotický
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Potenciálním	potenciální	k2eAgInSc7d1	potenciální
zdrojem	zdroj	k1gInSc7	zdroj
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
městští	městský	k2eAgMnPc1d1	městský
holubi	holub	k1gMnPc1	holub
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
promořenost	promořenost	k1gFnSc4	promořenost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nakažení	nakažení	k1gNnSc3	nakažení
obvykle	obvykle	k6eAd1	obvykle
dojde	dojít	k5eAaPmIp3nS	dojít
inhalací	inhalace	k1gFnSc7	inhalace
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sporadické	sporadický	k2eAgNnSc4d1	sporadické
nebo	nebo	k8xC	nebo
familiární	familiární	k2eAgNnSc4d1	familiární
onemocnění	onemocnění	k1gNnSc4	onemocnění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
chovatelé	chovatel	k1gMnPc1	chovatel
exotických	exotický	k2eAgMnPc2d1	exotický
ptáků	pták	k1gMnPc2	pták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
u	u	k7c2	u
veterinárních	veterinární	k2eAgMnPc2d1	veterinární
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
pracovníků	pracovník	k1gMnPc2	pracovník
na	na	k7c6	na
drůbežích	drůbeží	k2eAgFnPc6d1	drůbeží
porážkách	porážka	k1gFnPc6	porážka
nebo	nebo	k8xC	nebo
ošetřovatelů	ošetřovatel	k1gMnPc2	ošetřovatel
drůbeže	drůbež	k1gFnSc2	drůbež
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
farmách	farma	k1gFnPc6	farma
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
až	až	k9	až
epidemie	epidemie	k1gFnSc1	epidemie
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc1	onemocnění
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
charakter	charakter	k1gInSc4	charakter
profesionální	profesionální	k2eAgFnSc2d1	profesionální
zoonózy	zoonóza	k1gFnSc2	zoonóza
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
C.	C.	kA	C.
psittaci	psittace	k1gFnSc4	psittace
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
ptáka	pták	k1gMnSc4	pták
nebo	nebo	k8xC	nebo
drůbežím	drůbeží	k2eAgNnSc7d1	drůbeží
masem	maso	k1gNnSc7	maso
nebyl	být	k5eNaImAgInS	být
zatím	zatím	k6eAd1	zatím
popsán	popsat	k5eAaPmNgInS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
nejdříve	dříve	k6eAd3	dříve
u	u	k7c2	u
papoušků	papoušek	k1gMnPc2	papoušek
a	a	k8xC	a
nepochybovalo	pochybovat	k5eNaImAgNnS	pochybovat
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
z	z	k7c2	z
papoušků	papoušek	k1gMnPc2	papoušek
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
onemocnění	onemocnění	k1gNnSc1	onemocnění
pozorované	pozorovaný	k2eAgNnSc1d1	pozorované
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
vyvolané	vyvolaný	k2eAgInPc4d1	vyvolaný
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
papouškovitými	papouškovitý	k2eAgInPc7d1	papouškovitý
(	(	kIx(	(
<g/>
Psittacidae	Psittacidae	k1gInSc1	Psittacidae
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
nazváno	nazvat	k5eAaPmNgNnS	nazvat
psitakózou	psitakóza	k1gFnSc7	psitakóza
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgNnSc1d1	značné
rozšíření	rozšíření	k1gNnSc1	rozšíření
chlamydií	chlamydie	k1gFnPc2	chlamydie
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
než	než	k8xS	než
papouškovitých	papouškovitý	k2eAgMnPc2d1	papouškovitý
ptáků	pták	k1gMnPc2	pták
přimělo	přimět	k5eAaPmAgNnS	přimět
Meyera	Meyera	k1gFnSc1	Meyera
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
název	název	k1gInSc4	název
psitakóza	psitakóza	k1gFnSc1	psitakóza
nahradil	nahradit	k5eAaPmAgInS	nahradit
názvem	název	k1gInSc7	název
ornitóza	ornitóza	k1gFnSc1	ornitóza
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
návrh	návrh	k1gInSc4	návrh
pozměnil	pozměnit	k5eAaPmAgMnS	pozměnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
výrazem	výraz	k1gInSc7	výraz
psitakóza	psitakóza	k1gFnSc1	psitakóza
označovalo	označovat	k5eAaImAgNnS	označovat
onemocnění	onemocnění	k1gNnSc1	onemocnění
lidí	člověk	k1gMnPc2	člověk
způsobené	způsobený	k2eAgFnPc4d1	způsobená
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
papoušky	papoušek	k1gMnPc7	papoušek
a	a	k8xC	a
názvem	název	k1gInSc7	název
ornitóza	ornitóza	k1gFnSc1	ornitóza
onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobovaná	způsobovaný	k2eAgFnSc1d1	způsobovaná
jinými	jiný	k2eAgMnPc7d1	jiný
než	než	k8xS	než
papouškovitými	papouškovitý	k2eAgMnPc7d1	papouškovitý
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
dále	daleko	k6eAd2	daleko
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
rozlišení	rozlišení	k1gNnSc4	rozlišení
hovořilo	hovořit	k5eAaImAgNnS	hovořit
o	o	k7c6	o
ornitóze	ornitóza	k1gFnSc6	ornitóza
holubů	holub	k1gMnPc2	holub
<g/>
,	,	kIx,	,
krůt	krůta	k1gFnPc2	krůta
apod.	apod.	kA	apod.
Protože	protože	k8xS	protože
tyto	tento	k3xDgInPc1	tento
návrhy	návrh	k1gInPc1	návrh
měly	mít	k5eAaImAgInP	mít
určité	určitý	k2eAgFnPc4d1	určitá
epidemiologické	epidemiologický	k2eAgFnPc4d1	epidemiologická
i	i	k8xC	i
praktické	praktický	k2eAgFnPc4d1	praktická
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
vžily	vžít	k5eAaPmAgFnP	vžít
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
etiologického	etiologický	k2eAgNnSc2d1	etiologické
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nesprávné	správný	k2eNgFnPc1d1	nesprávná
(	(	kIx(	(
<g/>
původce	původce	k1gMnSc1	původce
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
i	i	k8xC	i
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
doporučován	doporučován	k2eAgInSc1d1	doporučován
název	název	k1gInSc1	název
chlamydióza	chlamydióza	k1gFnSc1	chlamydióza
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
lidí	člověk	k1gMnPc2	člověk
způsobená	způsobený	k2eAgFnSc1d1	způsobená
chlamydiemi	chlamydie	k1gFnPc7	chlamydie
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k9	již
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Trachom	trachom	k1gInSc1	trachom
je	být	k5eAaImIp3nS	být
popisován	popisovat	k5eAaImNgInS	popisovat
v	v	k7c6	v
egyptských	egyptský	k2eAgInPc6d1	egyptský
papyrech	papyr	k1gInPc6	papyr
<g/>
,	,	kIx,	,
obraz	obraz	k1gInSc1	obraz
lymfogranuloma	lymfogranuloma	k1gNnSc1	lymfogranuloma
venereum	venereum	k1gInSc1	venereum
popsal	popsat	k5eAaPmAgMnS	popsat
John	John	k1gMnSc1	John
Hunter	Hunter	k1gMnSc1	Hunter
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
jiné	jiný	k2eAgFnPc1d1	jiná
chlamydiové	chlamydiový	k2eAgFnPc1d1	chlamydiová
infekce	infekce	k1gFnPc1	infekce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
urogenitální	urogenitální	k2eAgFnSc1d1	urogenitální
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
rozpoznány	rozpoznat	k5eAaPmNgFnP	rozpoznat
až	až	k9	až
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
onemocnění	onemocnění	k1gNnPc2	onemocnění
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
Ritter	Rittra	k1gFnPc2	Rittra
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
7	[number]	k4	7
členů	člen	k1gMnPc2	člen
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
3	[number]	k4	3
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Choroba	choroba	k1gFnSc1	choroba
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
za	za	k7c4	za
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
zakoupení	zakoupení	k1gNnSc6	zakoupení
exotických	exotický	k2eAgMnPc2d1	exotický
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
již	již	k6eAd1	již
vykazovali	vykazovat	k5eAaImAgMnP	vykazovat
klinické	klinický	k2eAgInPc4d1	klinický
příznaky	příznak	k1gInPc4	příznak
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
množily	množit	k5eAaImAgInP	množit
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
onemocněních	onemocnění	k1gNnPc6	onemocnění
po	po	k7c6	po
zakoupení	zakoupení	k1gNnSc6	zakoupení
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
první	první	k4xOgFnSc1	první
větší	veliký	k2eAgFnSc1d2	veliký
epidemie	epidemie	k1gFnSc1	epidemie
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
dovozu	dovoz	k1gInSc6	dovoz
papoušků	papoušek	k1gMnPc2	papoušek
z	z	k7c2	z
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
;	;	kIx,	;
do	do	k7c2	do
r.	r.	kA	r.
1896	[number]	k4	1896
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
ve	v	k7c6	v
větším	veliký	k2eAgInSc6d2	veliký
počtu	počet	k1gInSc6	počet
domácností	domácnost	k1gFnPc2	domácnost
celkem	celkem	k6eAd1	celkem
70	[number]	k4	70
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
24	[number]	k4	24
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
epidemie	epidemie	k1gFnSc1	epidemie
psitakózy	psitakóza	k1gFnSc2	psitakóza
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
;	;	kIx,	;
v	v	k7c6	v
r.	r.	kA	r.
1909	[number]	k4	1909
se	se	k3xPyFc4	se
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
epidemie	epidemie	k1gFnSc1	epidemie
v	v	k7c6	v
Zülpichu	Zülpich	k1gInSc6	Zülpich
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivě	zdánlivě	k6eAd1	zdánlivě
zdravý	zdravý	k2eAgInSc1d1	zdravý
pár	pár	k1gInSc1	pár
papoušků	papoušek	k1gMnPc2	papoušek
způsobil	způsobit	k5eAaPmAgInS	způsobit
smrt	smrt	k1gFnSc1	smrt
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
novomanželů	novomanžel	k1gMnPc2	novomanžel
a	a	k8xC	a
potom	potom	k6eAd1	potom
postupně	postupně	k6eAd1	postupně
onemocnění	onemocnění	k1gNnSc1	onemocnění
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemocného	nemocný	k1gMnSc4	nemocný
ošetřovali	ošetřovat	k5eAaImAgMnP	ošetřovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
lidí	člověk	k1gMnPc2	člověk
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pohřbu	pohřeb	k1gInSc6	pohřeb
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zdrželi	zdržet	k5eAaPmAgMnP	zdržet
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byla	být	k5eAaImAgFnS	být
klec	klec	k1gFnSc1	klec
s	s	k7c7	s
papoušky	papoušek	k1gMnPc7	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
26	[number]	k4	26
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
5	[number]	k4	5
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1914-28	[number]	k4	1914-28
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
jen	jen	k9	jen
ojedinělé	ojedinělý	k2eAgFnPc1d1	ojedinělá
menší	malý	k2eAgFnPc1d2	menší
epidemie	epidemie	k1gFnPc1	epidemie
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
států	stát	k1gInPc2	stát
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
ochranná	ochranný	k2eAgNnPc1d1	ochranné
opatření	opatření	k1gNnPc1	opatření
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
chorobě	choroba	k1gFnSc3	choroba
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
zákazu	zákaz	k1gInSc6	zákaz
a	a	k8xC	a
kontrole	kontrola	k1gFnSc6	kontrola
dovozu	dovoz	k1gInSc2	dovoz
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
epidemie	epidemie	k1gFnSc1	epidemie
až	až	k8xS	až
pandemie	pandemie	k1gFnSc1	pandemie
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
nejprve	nejprve	k6eAd1	nejprve
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Cordoby	Cordoba	k1gFnSc2	Cordoba
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
za	za	k7c2	za
příznaků	příznak	k1gInPc2	příznak
atypické	atypický	k2eAgFnSc2d1	atypická
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
.	.	kIx.	.
</s>
<s>
Epidemiologické	epidemiologický	k2eAgNnSc1d1	epidemiologické
šetření	šetření	k1gNnSc1	šetření
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všichni	všechen	k3xTgMnPc1	všechen
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
se	s	k7c7	s
zásilkou	zásilka	k1gFnSc7	zásilka
5000	[number]	k4	5000
papoušků	papoušek	k1gMnPc2	papoušek
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
hromadně	hromadně	k6eAd1	hromadně
hynuli	hynout	k5eAaImAgMnP	hynout
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
majitelé	majitel	k1gMnPc1	majitel
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
snažili	snažit	k5eAaImAgMnP	snažit
je	on	k3xPp3gMnPc4	on
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
levně	levně	k6eAd1	levně
rozprodat	rozprodat	k5eAaPmF	rozprodat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
nemocných	nemocný	k2eAgMnPc2d1	nemocný
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
prodavači	prodavač	k1gMnPc7	prodavač
a	a	k8xC	a
překupníky	překupník	k1gMnPc7	překupník
<g/>
.	.	kIx.	.
</s>
<s>
Aukce	aukce	k1gFnSc1	aukce
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
Tucumanu	Tucuman	k1gInSc6	Tucuman
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
onemocnění	onemocnění	k1gNnSc4	onemocnění
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
r.	r.	kA	r.
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
choroba	choroba	k1gFnSc1	choroba
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
konečně	konečně	k6eAd1	konečně
zburcována	zburcován	k2eAgFnSc1d1	zburcována
veřejná	veřejný	k2eAgFnSc1d1	veřejná
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
i	i	k9	i
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
těžkému	těžký	k2eAgInSc3d1	těžký
průběhu	průběh	k1gInSc3	průběh
epidemie	epidemie	k1gFnSc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
prodej	prodej	k1gInSc1	prodej
a	a	k8xC	a
přesun	přesun	k1gInSc1	přesun
papoušků	papoušek	k1gMnPc2	papoušek
z	z	k7c2	z
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
nákaza	nákaza	k1gFnSc1	nákaza
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
zavlečena	zavleknout	k5eAaPmNgFnS	zavleknout
nejméně	málo	k6eAd3	málo
do	do	k7c2	do
12	[number]	k4	12
států	stát	k1gInPc2	stát
-	-	kIx~	-
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
,	,	kIx,	,
Holandska	Holandsko	k1gNnSc2	Holandsko
<g/>
,	,	kIx,	,
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkem	celkem	k6eAd1	celkem
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
750-800	[number]	k4	750-800
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
asi	asi	k9	asi
40	[number]	k4	40
%	%	kIx~	%
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
byla	být	k5eAaImAgFnS	být
nákaza	nákaza	k1gFnSc1	nákaza
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
zavlečena	zavleknout	k5eAaPmNgFnS	zavleknout
papoušky	papoušek	k1gMnPc7	papoušek
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
chovech	chov	k1gInPc6	chov
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
prokázány	prokázán	k2eAgInPc1d1	prokázán
latentní	latentní	k2eAgInPc1d1	latentní
rezervoáry	rezervoár	k1gInPc1	rezervoár
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
byly	být	k5eAaImAgFnP	být
zdrojem	zdroj	k1gInSc7	zdroj
infekcí	infekce	k1gFnPc2	infekce
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
případ	případ	k1gInSc1	případ
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
u	u	k7c2	u
importovaných	importovaný	k2eAgMnPc2d1	importovaný
ptáků	pták	k1gMnPc2	pták
popsali	popsat	k5eAaPmAgMnP	popsat
Galia	galium	k1gNnSc2	galium
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vážná	vážný	k2eAgFnSc1d1	vážná
epizootie	epizootie	k1gFnSc1	epizootie
kachen	kachna	k1gFnPc2	kachna
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
až	až	k9	až
1963	[number]	k4	1963
(	(	kIx(	(
<g/>
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
kontagiozitu	kontagiozita	k1gFnSc4	kontagiozita
původce	původce	k1gMnSc2	původce
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
případ	případ	k1gInSc4	případ
jedné	jeden	k4xCgFnSc2	jeden
kočovné	kočovný	k2eAgFnSc2d1	kočovná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
vypůjčila	vypůjčit	k5eAaPmAgFnS	vypůjčit
papouška	papoušek	k1gMnSc4	papoušek
od	od	k7c2	od
chovatele	chovatel	k1gMnSc4	chovatel
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
představení	představení	k1gNnSc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	se	k3xPyFc4	se
nakazilo	nakazit	k5eAaPmAgNnS	nakazit
12	[number]	k4	12
herců	herec	k1gMnPc2	herec
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
papoušek	papoušek	k1gMnSc1	papoušek
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
jen	jen	k9	jen
v	v	k7c6	v
kleci	klec	k1gFnSc6	klec
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
epidemie	epidemie	k1gFnSc1	epidemie
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
mezi	mezi	k7c7	mezi
lovci	lovec	k1gMnPc7	lovec
a	a	k8xC	a
škubačkami	škubačka	k1gFnPc7	škubačka
peří	peří	k1gNnSc2	peří
buřňáků	buřňák	k1gMnPc2	buřňák
ledních	lední	k2eAgMnPc2d1	lední
(	(	kIx(	(
<g/>
Fulmarus	Fulmarus	k1gMnSc1	Fulmarus
gracialis	gracialis	k1gFnSc2	gracialis
<g/>
)	)	kIx)	)
na	na	k7c6	na
Faerských	Faerský	k2eAgInPc6d1	Faerský
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Pandemie	pandemie	k1gFnSc1	pandemie
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929-30	[number]	k4	1929-30
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
rozpoznání	rozpoznání	k1gNnSc3	rozpoznání
etiologie	etiologie	k1gFnSc2	etiologie
<g/>
,	,	kIx,	,
epizootologie	epizootologie	k1gFnSc2	epizootologie
i	i	k8xC	i
diagnostiky	diagnostika	k1gFnSc2	diagnostika
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Bedson	Bedson	k1gNnSc1	Bedson
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
prokázali	prokázat	k5eAaPmAgMnP	prokázat
filtrovatelnost	filtrovatelnost	k1gFnSc4	filtrovatelnost
původce	původce	k1gMnSc2	původce
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
popsali	popsat	k5eAaPmAgMnP	popsat
způsob	způsob	k1gInSc4	způsob
mikroskopického	mikroskopický	k2eAgInSc2d1	mikroskopický
průkazu	průkaz	k1gInSc2	průkaz
původce	původce	k1gMnSc2	původce
ve	v	k7c6	v
vyšetřovaném	vyšetřovaný	k2eAgInSc6d1	vyšetřovaný
materiálu	materiál	k1gInSc6	materiál
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
ze	z	k7c2	z
zvířat	zvíře	k1gNnPc2	zvíře
Levinthal	Levinthal	k1gMnSc1	Levinthal
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Coles	Coles	k1gMnSc1	Coles
(	(	kIx(	(
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lillie	Lillie	k1gFnSc1	Lillie
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
elementární	elementární	k2eAgNnPc4d1	elementární
tělíska	tělísko	k1gNnPc4	tělísko
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
také	také	k9	také
někdy	někdy	k6eAd1	někdy
popisují	popisovat	k5eAaImIp3nP	popisovat
jako	jako	k8xC	jako
tělíska	tělísko	k1gNnPc4	tělísko
LCL	LCL	kA	LCL
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydia	Chlamydium	k1gNnPc1	Chlamydium
trachomatis	trachomatis	k1gFnSc2	trachomatis
sérotypy	sérotyp	k1gInPc1	sérotyp
A-C	A-C	k1gFnSc2	A-C
-	-	kIx~	-
trachom	trachom	k1gInSc1	trachom
(	(	kIx(	(
<g/>
oční	oční	k2eAgFnSc2d1	oční
infekce	infekce	k1gFnSc2	infekce
<g/>
)	)	kIx)	)
sérotypy	sérotyp	k1gInPc1	sérotyp
D-K	D-K	k1gFnSc2	D-K
-	-	kIx~	-
pohlavně	pohlavně	k6eAd1	pohlavně
přenosné	přenosný	k2eAgFnSc2d1	přenosná
infekce	infekce	k1gFnSc2	infekce
močopohlavního	močopohlavní	k2eAgNnSc2d1	močopohlavní
ústrojí	ústrojí	k1gNnSc2	ústrojí
(	(	kIx(	(
<g/>
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ale	ale	k9	ale
infikovány	infikován	k2eAgInPc1d1	infikován
i	i	k8xC	i
např.	např.	kA	např.
oči	oko	k1gNnPc1	oko
nebo	nebo	k8xC	nebo
hrtan	hrtan	k1gInSc1	hrtan
<g/>
)	)	kIx)	)
sérotypy	sérotyp	k1gInPc1	sérotyp
L1-L3	L1-L3	k1gFnSc2	L1-L3
-	-	kIx~	-
lymphogranuloma	lymphogranuloma	k1gFnSc1	lymphogranuloma
venereum	venereum	k1gInSc1	venereum
(	(	kIx(	(
<g/>
pohlavní	pohlavní	k2eAgFnSc1d1	pohlavní
infekce	infekce	k1gFnSc1	infekce
<g/>
)	)	kIx)	)
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
pneumoniae	pneumoniaat	k5eAaPmIp3nS	pneumoniaat
bezpříznaková	bezpříznakový	k2eAgFnSc1d1	bezpříznaková
kolonizace	kolonizace	k1gFnSc1	kolonizace
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
běžené	běžený	k2eAgFnSc2d1	běžený
infekce	infekce	k1gFnSc2	infekce
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
-	-	kIx~	-
rýma	rýma	k1gFnSc1	rýma
<g/>
,	,	kIx,	,
faryngitida	faryngitida	k1gFnSc1	faryngitida
<g/>
,	,	kIx,	,
pneumonie	pneumonie	k1gFnSc1	pneumonie
meningoencefalitida	meningoencefalitida	k1gFnSc1	meningoencefalitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
mozkových	mozkový	k2eAgFnPc2d1	mozková
blan	blána	k1gFnPc2	blána
<g/>
)	)	kIx)	)
-	-	kIx~	-
vzácně	vzácně	k6eAd1	vzácně
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
psittaci	psittace	k1gFnSc4	psittace
-	-	kIx~	-
původce	původce	k1gMnSc1	původce
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
abortu	abort	k1gInSc2	abort
hovězího	hovězí	k2eAgInSc2d1	hovězí
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
encefalomyelitidy	encefalomyelitis	k1gFnSc2	encefalomyelitis
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
polyartritidy	polyartritis	k1gFnSc2	polyartritis
jehňat	jehně	k1gNnPc2	jehně
a	a	k8xC	a
telat	tele	k1gNnPc2	tele
a	a	k8xC	a
pneumonie	pneumonie	k1gFnSc2	pneumonie
koček	kočka	k1gFnPc2	kočka
</s>
