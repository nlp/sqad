<s>
Další	další	k2eAgFnSc1d1	další
epidemie	epidemie	k1gFnSc1	epidemie
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
mezi	mezi	k7c7	mezi
lovci	lovec	k1gMnPc7	lovec
a	a	k8xC	a
škubačkami	škubačka	k1gFnPc7	škubačka
peří	peří	k1gNnSc2	peří
buřňáků	buřňák	k1gMnPc2	buřňák
ledních	lední	k2eAgMnPc2d1	lední
(	(	kIx(	(
<g/>
Fulmarus	Fulmarus	k1gMnSc1	Fulmarus
gracialis	gracialis	k1gFnSc2	gracialis
<g/>
)	)	kIx)	)
na	na	k7c6	na
Faerských	Faerský	k2eAgInPc6d1	Faerský
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
