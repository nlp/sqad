<p>
<s>
Továrna	továrna	k1gFnSc1	továrna
K.	K.	kA	K.
Morstadt	Morstadta	k1gFnPc2	Morstadta
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
je	být	k5eAaImIp3nS	být
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
objekt	objekt	k1gInSc1	objekt
č.	č.	k?	č.
p.	p.	k?	p.
475	[number]	k4	475
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
7	[number]	k4	7
-	-	kIx~	-
Holešovicích	Holešovice	k1gFnPc6	Holešovice
<g/>
,	,	kIx,	,
Dělnická	dělnický	k2eAgFnSc1d1	Dělnická
ulice	ulice	k1gFnSc1	ulice
č.	č.	k?	č.
o.	o.	k?	o.
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
přestavěn	přestavět	k5eAaPmNgMnS	přestavět
na	na	k7c4	na
kancelářskou	kancelářský	k2eAgFnSc4d1	kancelářská
budovu	budova	k1gFnSc4	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
továrny	továrna	k1gFnSc2	továrna
stála	stát	k5eAaImAgFnS	stát
Perutzova	Perutzův	k2eAgFnSc1d1	Perutzův
přádelna	přádelna	k1gFnSc1	přádelna
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
pro	pro	k7c4	pro
farmaceuty	farmaceut	k1gMnPc4	farmaceut
F.	F.	kA	F.
Ševčíka	Ševčík	k1gMnSc2	Ševčík
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
A.	A.	kA	A.
Vomáčku	Vomáčka	k1gMnSc4	Vomáčka
a	a	k8xC	a
K.	K.	kA	K.
Morstadta	Morstadta	k1gFnSc1	Morstadta
dvoupatrová	dvoupatrový	k2eAgFnSc1d1	dvoupatrová
továrna	továrna	k1gFnSc1	továrna
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architekta	architekt	k1gMnSc2	architekt
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Pollerta	Pollert	k1gMnSc2	Pollert
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
provedla	provést	k5eAaPmAgFnS	provést
firma	firma	k1gFnSc1	firma
Hrůza	hrůza	k1gFnSc1	hrůza
a	a	k8xC	a
Rosenberg	Rosenberg	k1gInSc1	Rosenberg
<g/>
,	,	kIx,	,
podnikatelství	podnikatelství	k1gNnSc1	podnikatelství
betonových	betonový	k2eAgFnPc2d1	betonová
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
stavebních	stavební	k2eAgInPc2d1	stavební
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
oplatkové	oplatkový	k2eAgFnPc1d1	Oplatková
tobolky	tobolka	k1gFnPc1	tobolka
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
léků	lék	k1gInPc2	lék
podle	podle	k7c2	podle
Ševčíkových	Ševčíkových	k2eAgInPc2d1	Ševčíkových
a	a	k8xC	a
Morstadtových	Morstadtův	k2eAgInPc2d1	Morstadtův
patentů	patent	k1gInPc2	patent
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
zde	zde	k6eAd1	zde
sídlila	sídlit	k5eAaImAgFnS	sídlit
firma	firma	k1gFnSc1	firma
Kutov	Kutovo	k1gNnPc2	Kutovo
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
národní	národní	k2eAgInSc1d1	národní
podnik	podnik	k1gInSc1	podnik
Interiér	interiér	k1gInSc1	interiér
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
přestavba	přestavba	k1gFnSc1	přestavba
objektu	objekt	k1gInSc2	objekt
na	na	k7c4	na
kancelářskou	kancelářský	k2eAgFnSc4d1	kancelářská
budovu	budova	k1gFnSc4	budova
Podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Tomáše	Tomáš	k1gMnSc4	Tomáš
Novotného	Novotný	k1gMnSc4	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Vinárnu	vinárna	k1gFnSc4	vinárna
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
bývalé	bývalý	k2eAgFnSc2d1	bývalá
trafostanice	trafostanice	k1gFnSc2	trafostanice
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
sochař	sochař	k1gMnSc1	sochař
Stefan	Stefan	k1gMnSc1	Stefan
Milkov	Milkov	k1gInSc1	Milkov
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
institut	institut	k1gInSc1	institut
Paralelní	paralelní	k2eAgFnSc2d1	paralelní
Polis	Polis	k1gFnSc2	Polis
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
založila	založit	k5eAaPmAgFnS	založit
umělecká	umělecký	k2eAgFnSc1d1	umělecká
skupina	skupina	k1gFnSc1	skupina
Ztohoven	Ztohovna	k1gFnPc2	Ztohovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Holešovické	holešovický	k2eAgInPc1d1	holešovický
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s.	s.	k?	s.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Továrna	továrna	k1gFnSc1	továrna
K.	K.	kA	K.
Morstadt	Morstadt	k1gInSc1	Morstadt
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
BERAN	Beran	k1gMnSc1	Beran
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
;	;	kIx,	;
VALCHÁŘOVÁ	VALCHÁŘOVÁ	kA	VALCHÁŘOVÁ
<g/>
,	,	kIx,	,
Vladislava	Vladislava	k1gFnSc1	Vladislava
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
industriál	industriál	k1gInSc1	industriál
:	:	kIx,	:
technické	technický	k2eAgFnSc2d1	technická
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
architektura	architektura	k1gFnSc1	architektura
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
dědictví	dědictví	k1gNnSc2	dědictví
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3586	[number]	k4	3586
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
76	[number]	k4	76
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Administrativní	administrativní	k2eAgFnSc1d1	administrativní
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
vinotéka	vinotéka	k1gFnSc1	vinotéka
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
FRAGNER	FRAGNER	kA	FRAGNER
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
<g/>
;	;	kIx,	;
HANZLOVÁ	Hanzlová	k1gFnSc1	Hanzlová
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
<g/>
.	.	kIx.	.
</s>
<s>
Industriální	industriální	k2eAgFnPc4d1	industriální
stopy	stopa	k1gFnPc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
dědictví	dědictví	k1gNnSc2	dědictví
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
5440	[number]	k4	5440
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
26	[number]	k4	26
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
</p>
