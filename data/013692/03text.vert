<s>
Diracova	Diracův	k2eAgFnSc1d1
struna	struna	k1gFnSc1
</s>
<s>
Ve	v	k7c6
fyzice	fyzika	k1gFnSc6
je	být	k5eAaImIp3nS
Diracova	Diracův	k2eAgFnSc1d1
struna	struna	k1gFnSc1
hypotetická	hypotetický	k2eAgFnSc1d1
jednorozměrná	jednorozměrný	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
v	v	k7c6
prostoru	prostor	k1gInSc6
<g/>
,	,	kIx,
postulovaná	postulovaný	k2eAgFnSc1d1
Paulem	Paul	k1gMnSc7
Diracem	Dirace	k1gMnSc7
<g/>
,	,	kIx,
táhnoucí	táhnoucí	k2eAgNnSc1d1
se	se	k3xPyFc4
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
Diracovými	Diracův	k2eAgInPc7d1
magnetickými	magnetický	k2eAgInPc7d1
monopóly	monopól	k1gInPc7
s	s	k7c7
opačným	opačný	k2eAgInSc7d1
magnetickým	magnetický	k2eAgInSc7d1
nábojem	náboj	k1gInSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
z	z	k7c2
jedné	jeden	k4xCgFnSc2
magnetické	magnetický	k2eAgFnSc2d1
částice	částice	k1gFnSc2
do	do	k7c2
nekonečna	nekonečno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kalibrační	kalibrační	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
definován	definovat	k5eAaBmNgInS
pro	pro	k7c4
Diracovu	Diracův	k2eAgFnSc4d1
strunu	struna	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
všude	všude	k6eAd1
kolem	kolem	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diracova	Diracův	k2eAgFnSc1d1
struna	struna	k1gFnSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
jako	jako	k9
elektromagnet	elektromagnet	k1gInSc1
v	v	k7c6
Aharonově	Aharonův	k2eAgInSc6d1
<g/>
–	–	k?
<g/>
Bohmově	Bohmův	k2eAgInSc6d1
jevu	jev	k1gInSc6
a	a	k8xC
požadavek	požadavek	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
pozice	pozice	k1gFnSc1
Diracovy	Diracův	k2eAgFnSc2d1
struny	struna	k1gFnSc2
neměla	mít	k5eNaImAgFnS
být	být	k5eAaImF
pozorovatelná	pozorovatelný	k2eAgFnSc1d1
implikuje	implikovat	k5eAaImIp3nS
Diracovo	Diracův	k2eAgNnSc4d1
pravidlo	pravidlo	k1gNnSc4
kvantování	kvantování	k1gNnSc2
<g/>
:	:	kIx,
produkt	produkt	k1gInSc1
magnetického	magnetický	k2eAgInSc2d1
náboje	náboj	k1gInSc2
a	a	k8xC
elektrického	elektrický	k2eAgInSc2d1
náboje	náboj	k1gInSc2
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vždy	vždy	k6eAd1
celočíselným	celočíselný	k2eAgInSc7d1
násobkem	násobek	k1gInSc7
</s>
<s>
2	#num#	k4
</s>
<s>
π	π	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
2	#num#	k4
<g/>
\	\	kIx~
<g/>
pi	pi	k0
}	}	kIx)
</s>
<s>
Také	také	k9
změna	změna	k1gFnSc1
pozice	pozice	k1gFnSc2
Diracovy	Diracův	k2eAgFnSc2d1
struny	struna	k1gFnSc2
odpovídá	odpovídat	k5eAaImIp3nS
kalibrační	kalibrační	k2eAgFnSc4d1
transformaci	transformace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Diracovy	Diracův	k2eAgFnPc1d1
struny	struna	k1gFnPc1
nejsou	být	k5eNaImIp3nP
kalibračně	kalibračně	k6eAd1
invariantní	invariantní	k2eAgInPc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
faktem	fakt	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
pozorovatelné	pozorovatelný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Diracova	Diracův	k2eAgFnSc1d1
struna	struna	k1gFnSc1
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
začlenit	začlenit	k5eAaPmF
magnetické	magnetický	k2eAgFnPc4d1
monopóly	monopóla	k1gFnPc4
do	do	k7c2
Maxwellových	Maxwellův	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
magnetický	magnetický	k2eAgInSc1d1
tok	tok	k1gInSc1
tekoucí	tekoucí	k2eAgInSc1d1
podél	podél	k7c2
interiéru	interiér	k1gInSc2
strun	struna	k1gFnPc2
udržuje	udržovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
platnost	platnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
Maxwellovy	Maxwellův	k2eAgFnPc1d1
rovnice	rovnice	k1gFnPc1
upraveny	upravit	k5eAaPmNgFnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
umožňovaly	umožňovat	k5eAaImAgFnP
magnetické	magnetický	k2eAgFnPc1d1
monopóly	monopóla	k1gFnPc1
na	na	k7c6
základní	základní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
magnetické	magnetický	k2eAgFnPc1d1
monopóly	monopóla	k1gFnPc1
již	již	k6eAd1
již	již	k6eAd1
nejsou	být	k5eNaImIp3nP
Diracovými	Diracův	k2eAgNnPc7d1
monopóly	monopólo	k1gNnPc7
<g/>
,	,	kIx,
a	a	k8xC
nevyžadují	vyžadovat	k5eNaImIp3nP
připojení	připojení	k1gNnSc4
Diracových	Diracův	k2eAgFnPc2d1
strun	struna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1
</s>
<s>
Kvantování	kvantování	k1gNnSc1
vynucené	vynucený	k2eAgNnSc1d1
Diracovou	Diracův	k2eAgFnSc7d1
strunou	struna	k1gFnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
chápáno	chápat	k5eAaImNgNnS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
kohomologie	kohomologie	k1gFnSc2
fibrovaného	fibrovaný	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
představujícího	představující	k2eAgInSc2d1
kalibrační	kalibrační	k2eAgNnSc4d1
pole	pole	k1gNnSc4
nad	nad	k7c7
základní	základní	k2eAgFnSc7d1
varietou	varieta	k1gFnSc7
prostoročasu	prostoročas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magnetické	magnetický	k2eAgInPc1d1
náboje	náboj	k1gInPc1
kalibrační	kalibrační	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
pole	pole	k1gNnSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
chápány	chápat	k5eAaImNgInP
jako	jako	k9
generátory	generátor	k1gInPc1
grup	grupa	k1gFnPc2
kohomologické	kohomologický	k2eAgFnSc2d1
grupy	grupa	k1gFnSc2
</s>
<s>
H	H	kA
</s>
<s>
1	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
M	M	kA
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
H_	H_	k1gMnSc6
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
M	M	kA
<g/>
)	)	kIx)
<g/>
}	}	kIx)
</s>
<s>
pro	pro	k7c4
svazek	svazek	k1gInSc4
vláken	vlákna	k1gFnPc2
M.	M.	kA
Kohomologie	Kohomologie	k1gFnSc2
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
myšlenky	myšlenka	k1gFnSc2
klasifikace	klasifikace	k1gFnSc2
všech	všecek	k3xTgFnPc2
možných	možný	k2eAgFnPc2d1
kalibračních	kalibrační	k2eAgFnPc2d1
polí	pole	k1gFnPc2
intenzity	intenzita	k1gFnSc2
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
d	d	k?
</s>
<s>
A	a	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
F	F	kA
<g/>
=	=	kIx~
<g/>
dA	dA	k?
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
zjevně	zjevně	k6eAd1
přesné	přesný	k2eAgFnPc1d1
formy	forma	k1gFnPc1
<g/>
,	,	kIx,
modulo	modout	k5eAaPmAgNnS
všechny	všechen	k3xTgFnPc4
možné	možný	k2eAgFnPc4d1
kalibrační	kalibrační	k2eAgFnPc4d1
transformace	transformace	k1gFnPc4
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
intenzita	intenzita	k1gFnSc1
pole	pole	k1gNnSc2
F	F	kA
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
uzavřenou	uzavřený	k2eAgFnSc4d1
formu	forma	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
d	d	k?
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
dF	dF	k?
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
A	a	k9
vektorový	vektorový	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
<g/>
,	,	kIx,
d	d	k?
představuje	představovat	k5eAaImIp3nS
kalibrační-kovariantní	kalibrační-kovariantní	k2eAgFnSc1d1
derivace	derivace	k1gFnSc1
a	a	k8xC
F	F	kA
sílu	síla	k1gFnSc4
pole	pole	k1gNnSc4
nebo	nebo	k8xC
formu	forma	k1gFnSc4
zakřivení	zakřivení	k1gNnSc2
na	na	k7c6
svazku	svazek	k1gInSc6
vláken	vlákno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neformálně	formálně	k6eNd1
by	by	kYmCp3nS
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
Diracova	Diracův	k2eAgFnSc1d1
struna	struna	k1gFnSc1
nese	nést	k5eAaImIp3nS
pryč	pryč	k6eAd1
"	"	kIx"
<g/>
nadměrné	nadměrný	k2eAgNnSc4d1
zakřivení	zakřivení	k1gNnSc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
jinak	jinak	k6eAd1
zabránilo	zabránit	k5eAaPmAgNnS
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
F	F	kA
mělo	mít	k5eAaImAgNnS
uzavřenou	uzavřený	k2eAgFnSc4d1
formu	forma	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
</s>
<s>
d	d	k?
</s>
<s>
F	F	kA
</s>
<s>
=	=	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
dF	dF	k?
<g/>
=	=	kIx~
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
všude	všude	k6eAd1
<g/>
,	,	kIx,
kromě	kromě	k7c2
místa	místo	k1gNnSc2
monopólu	monopól	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Dirac	Dirac	k1gInSc1
string	string	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
DIRAC	DIRAC	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
A.	A.	kA
<g/>
M.	M.	kA
Quantized	Quantized	k1gMnSc1
Singularities	Singularities	k1gMnSc1
in	in	k?
the	the	k?
Electromagnetic	Electromagnetice	k1gFnPc2
Field	Field	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
A.	A.	kA
September	September	k1gInSc4
1931	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
60	#num#	k4
<g/>
–	–	k?
<g/>
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rspa	rspa	k6eAd1
<g/>
.1931	.1931	k4
<g/>
.0130	.0130	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1931	#num#	k4
<g/>
RSPSA	RSPSA	kA
<g/>
.133	.133	k4
<g/>
..	..	k?
<g/>
.60	.60	k4
<g/>
D.	D.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
6	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
</s>
