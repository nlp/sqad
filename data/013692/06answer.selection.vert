<s>
Diracova	Diracův	k2eAgFnSc1d1
struna	struna	k1gFnSc1
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
začlenit	začlenit	k5eAaPmF
magnetické	magnetický	k2eAgFnPc4d1
monopóly	monopóla	k1gFnPc4
do	do	k7c2
Maxwellových	Maxwellův	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
magnetický	magnetický	k2eAgInSc1d1
tok	tok	k1gInSc1
tekoucí	tekoucí	k2eAgInSc1d1
podél	podél	k7c2
interiéru	interiér	k1gInSc2
strun	struna	k1gFnPc2
udržuje	udržovat	k5eAaImIp3nS
jejich	jejich	k3xOp3gFnSc4
platnost	platnost	k1gFnSc4
<g/>
.	.	kIx.
</s>