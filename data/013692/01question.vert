<s>
Proč	proč	k6eAd1
je	být	k5eAaImIp3nS
Diracova	Diracův	k2eAgFnSc1d1
struna	struna	k1gFnSc1
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc4d1
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
začlenit	začlenit	k5eAaPmF
magnetické	magnetický	k2eAgFnPc4d1
monopóly	monopóla	k1gFnPc4
do	do	k7c2
Maxwellových	Maxwellův	k2eAgFnPc2d1
rovnic	rovnice	k1gFnPc2
<g/>
?	?	kIx.
</s>