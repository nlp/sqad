<s>
Jiří	Jiří	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
(	(	kIx(
<g/>
fotbalista	fotbalista	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
JarošOsobní	JarošOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1955	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1999	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
44	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
obránce	obránce	k1gMnSc1
Mládežnické	mládežnický	k2eAgInPc4d1
kluby	klub	k1gInPc4
</s>
<s>
TJ	tj	kA
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1980	#num#	k4
<g/>
–	–	k?
<g/>
19811981	#num#	k4
<g/>
–	–	k?
<g/>
19871992	#num#	k4
<g/>
–	–	k?
<g/>
19931993	#num#	k4
<g/>
–	–	k?
<g/>
19941994	#num#	k4
<g/>
–	–	k?
<g/>
199519951996	#num#	k4
<g/>
TJ	tj	kA
TŽ	TŽ	kA
TřinecTJ	TřinecTJ	k1gFnSc1
Zbrojovka	zbrojovka	k1gFnSc1
BrnoTJ	BrnoTJ	k1gFnSc1
Zetor	zetor	k1gInSc1
Brno	Brno	k1gNnSc1
<g/>
→	→	k?
SK	Sk	kA
TuřanyČAFC	TuřanyČAFC	k1gFnSc2
Židenice	Židenice	k1gFnSc2
Brno	Brno	k1gNnSc1
<g/>
→	→	k?
SK	Sk	kA
PraceSK	PraceSK	k1gFnSc2
Tuřany	Tuřana	k1gFnSc2
<g/>
0	#num#	k4
<g/>
9500	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Josef	Josef	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Příbuzenstvo	příbuzenstvo	k1gNnSc1
</s>
<s>
otec	otec	k1gMnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
</s>
<s>
syn	syn	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
ml.	ml.	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
Jaroš	Jaroš	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1955	#num#	k4
Brno	Brno	k1gNnSc4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1999	#num#	k4
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
československý	československý	k2eAgMnSc1d1
fotbalový	fotbalový	k2eAgMnSc1d1
obránce	obránce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
Zbrojovku	zbrojovka	k1gFnSc4
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastoupil	nastoupit	k5eAaPmAgMnS
ve	v	k7c6
38	#num#	k4
ligových	ligový	k2eAgNnPc6d1
utkáních	utkání	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
otec	otec	k1gMnSc1
Josef	Josef	k1gMnSc1
byl	být	k5eAaImAgMnS
taktéž	taktéž	k?
prvoligovým	prvoligový	k2eAgMnSc7d1
fotbalistou	fotbalista	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Tragicky	tragicky	k6eAd1
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
autonehodě	autonehoda	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
brněnských	brněnský	k2eAgFnPc6d1
Pisárkách	Pisárka	k1gFnPc6
čelně	čelně	k6eAd1
srazil	srazit	k5eAaPmAgMnS
s	s	k7c7
protijedoucím	protijedoucí	k2eAgNnSc7d1
autem	auto	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Ligová	ligový	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
</s>
<s>
Ročník	ročník	k1gInSc1
</s>
<s>
Zápasy	zápas	k1gInPc1
</s>
<s>
Góly	gól	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
československá	československý	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
CELKEM	celkem	k6eAd1
</s>
<s>
38	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ľubomír	Ľubomír	k1gMnSc1
Dávid	Dávida	k1gFnPc2
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Grünner	Grünner	k1gMnSc1
<g/>
,	,	kIx,
Juraj	Juraj	k1gMnSc1
Hrivnák	Hrivnák	k1gMnSc1
<g/>
,	,	kIx,
Jozef	Jozef	k1gMnSc1
Kšiňan	Kšiňan	k1gMnSc1
<g/>
,	,	kIx,
Jindřich	Jindřich	k1gMnSc1
Pejchar	Pejchar	k1gMnSc1
<g/>
:	:	kIx,
Futbal	Futbal	k1gInSc1
81	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
-	-	kIx~
ročenka	ročenka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Player	Player	k1gInSc1
History	Histor	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
