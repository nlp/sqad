<s>
Mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
cerebrum	cerebrum	k1gInSc1	cerebrum
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
encephalon	encephalon	k1gInSc1	encephalon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
organizační	organizační	k2eAgNnSc1d1	organizační
a	a	k8xC	a
řídící	řídící	k2eAgNnSc1d1	řídící
centrum	centrum	k1gNnSc1	centrum
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
