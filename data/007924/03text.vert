<s>
Mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
cerebrum	cerebrum	k1gInSc1	cerebrum
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
encephalon	encephalon	k1gInSc1	encephalon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
organizační	organizační	k2eAgNnSc1d1	organizační
a	a	k8xC	a
řídící	řídící	k2eAgNnSc1d1	řídící
centrum	centrum	k1gNnSc1	centrum
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
mozek	mozek	k1gInSc1	mozek
<g/>
"	"	kIx"	"
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
strunatce	strunatec	k1gMnPc4	strunatec
(	(	kIx(	(
<g/>
bezlebeční	bezlebeční	k2eAgMnSc1d1	bezlebeční
<g/>
,	,	kIx,	,
pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavonožce	hlavonožec	k1gMnSc4	hlavonožec
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
některé	některý	k3yIgMnPc4	některý
další	další	k2eAgMnPc4d1	další
bezobratlé	bezobratlí	k1gMnPc4	bezobratlí
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
však	však	k9	však
u	u	k7c2	u
bezobratlých	bezobratlý	k2eAgFnPc2d1	bezobratlý
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
jistý	jistý	k2eAgInSc1d1	jistý
mozkový	mozkový	k2eAgInSc1d1	mozkový
ganglion	ganglion	k1gNnSc1	ganglion
(	(	kIx(	(
<g/>
zauzlina	zauzlina	k1gFnSc1	zauzlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
obratlovců	obratlovec	k1gMnPc2	obratlovec
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
neurální	neurální	k2eAgFnSc2d1	neurální
ploténky	ploténka	k1gFnSc2	ploténka
a	a	k8xC	a
během	během	k7c2	během
zárodečného	zárodečný	k2eAgInSc2d1	zárodečný
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
objevují	objevovat	k5eAaImIp3nP	objevovat
různé	různý	k2eAgInPc1d1	různý
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
často	často	k6eAd1	často
plní	plnit	k5eAaImIp3nP	plnit
poměrně	poměrně	k6eAd1	poměrně
odlišné	odlišný	k2eAgFnPc1d1	odlišná
funkce	funkce	k1gFnPc1	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
dohromady	dohromady	k6eAd1	dohromady
hlavní	hlavní	k2eAgInSc4d1	hlavní
orgán	orgán	k1gInSc4	orgán
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
částem	část	k1gFnPc3	část
mozku	mozek	k1gInSc2	mozek
patří	patřit	k5eAaImIp3nP	patřit
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
<g/>
,	,	kIx,	,
mozeček	mozeček	k1gInSc1	mozeček
<g/>
,	,	kIx,	,
střední	střední	k2eAgInSc1d1	střední
mozek	mozek	k1gInSc1	mozek
<g/>
,	,	kIx,	,
mezimozek	mezimozek	k1gInSc1	mezimozek
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
buněčného	buněčný	k2eAgNnSc2d1	buněčné
hlediska	hledisko	k1gNnSc2	hledisko
složen	složit	k5eAaPmNgInS	složit
především	především	k6eAd1	především
z	z	k7c2	z
nervových	nervový	k2eAgFnPc2d1	nervová
a	a	k8xC	a
gliových	gliův	k2eAgFnPc2d1	gliův
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnPc1	místo
s	s	k7c7	s
nahromaděnými	nahromaděný	k2eAgNnPc7d1	nahromaděné
těly	tělo	k1gNnPc7	tělo
neuronů	neuron	k1gInPc2	neuron
představují	představovat	k5eAaImIp3nP	představovat
tzv.	tzv.	kA	tzv.
šedou	šedý	k2eAgFnSc4d1	šedá
hmotu	hmota	k1gFnSc4	hmota
<g/>
,	,	kIx,	,
místa	místo	k1gNnPc4	místo
bohatá	bohatý	k2eAgNnPc4d1	bohaté
na	na	k7c6	na
nervová	nervový	k2eAgNnPc1d1	nervové
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
známa	známo	k1gNnPc4	známo
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
bílá	bílý	k2eAgFnSc1d1	bílá
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
nervové	nervový	k2eAgFnPc1d1	nervová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
aktivní	aktivní	k2eAgFnPc1d1	aktivní
nervové	nervový	k2eAgFnPc1d1	nervová
činnosti	činnost	k1gFnPc1	činnost
spojené	spojený	k2eAgFnPc1d1	spojená
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
a	a	k8xC	a
přenosem	přenos	k1gInSc7	přenos
nervových	nervový	k2eAgInPc2d1	nervový
impulsů	impuls	k1gInPc2	impuls
<g/>
,	,	kIx,	,
nepostradatelné	postradatelný	k2eNgInPc1d1	nepostradatelný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
gliové	gliové	k2eAgFnPc1d1	gliové
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyživují	vyživovat	k5eAaImIp3nP	vyživovat
<g/>
,	,	kIx,	,
chrání	chránit	k5eAaImIp3nS	chránit
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
podporují	podporovat	k5eAaImIp3nP	podporovat
nervovou	nervový	k2eAgFnSc4d1	nervová
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
mozky	mozek	k1gInPc1	mozek
obratlovců	obratlovec	k1gMnPc2	obratlovec
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
homologické	homologický	k2eAgFnPc1d1	homologická
a	a	k8xC	a
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
mozku	mozek	k1gInSc2	mozek
předka	předek	k1gMnSc2	předek
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
značné	značný	k2eAgFnPc1d1	značná
odlišnosti	odlišnost	k1gFnPc1	odlišnost
–	–	k?	–
každý	každý	k3xTgMnSc1	každý
obratlovec	obratlovec	k1gMnSc1	obratlovec
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
jiné	jiný	k2eAgInPc4d1	jiný
požadavky	požadavek	k1gInPc4	požadavek
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
bouřlivě	bouřlivě	k6eAd1	bouřlivě
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
koncový	koncový	k2eAgInSc1d1	koncový
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
jeho	jeho	k3xOp3gFnPc1	jeho
hemisféry	hemisféra	k1gFnPc1	hemisféra
překrývají	překrývat	k5eAaImIp3nP	překrývat
skoro	skoro	k6eAd1	skoro
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
části	část	k1gFnPc1	část
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
zvrásněn	zvrásněn	k2eAgInSc1d1	zvrásněn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
je	být	k5eAaImIp3nS	být
také	také	k9	také
mozeček	mozeček	k1gInSc1	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
obratlovci	obratlovec	k1gMnPc1	obratlovec
sdílí	sdílet	k5eAaImIp3nP	sdílet
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
strunatci	strunatec	k1gMnPc7	strunatec
důležitou	důležitý	k2eAgFnSc4d1	důležitá
evoluční	evoluční	k2eAgFnSc4d1	evoluční
novinku	novinka	k1gFnSc4	novinka
–	–	k?	–
neurální	neurální	k2eAgFnSc4d1	neurální
ploténku	ploténka	k1gFnSc4	ploténka
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
neurální	neurální	k2eAgFnSc1d1	neurální
trubice	trubice	k1gFnSc1	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
ale	ale	k8xC	ale
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
neurální	neurální	k2eAgFnSc2d1	neurální
ploténky	ploténka	k1gFnSc2	ploténka
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
obrovskému	obrovský	k2eAgInSc3d1	obrovský
nárůstu	nárůst	k1gInSc3	nárůst
nervové	nervový	k2eAgFnSc2d1	nervová
hmoty	hmota	k1gFnSc2	hmota
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
regulační	regulační	k2eAgInPc1d1	regulační
geny	gen	k1gInPc1	gen
ovládající	ovládající	k2eAgInPc1d1	ovládající
vývoj	vývoj	k1gInSc4	vývoj
mozku	mozek	k1gInSc2	mozek
jsou	být	k5eAaImIp3nP	být
nicméně	nicméně	k8xC	nicméně
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
přítomné	přítomný	k1gMnPc4	přítomný
už	už	k6eAd1	už
u	u	k7c2	u
pláštěnců	pláštěnec	k1gMnPc2	pláštěnec
a	a	k8xC	a
kopinatců	kopinatec	k1gMnPc2	kopinatec
<g/>
:	:	kIx,	:
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
trubice	trubice	k1gFnSc2	trubice
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgInPc1d1	aktivní
geny	gen	k1gInPc1	gen
Otx	Otx	k1gMnPc2	Otx
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
se	se	k3xPyFc4	se
angažují	angažovat	k5eAaBmIp3nP	angažovat
známé	známý	k2eAgFnPc1d1	známá
Hox	Hox	k1gFnPc1	Hox
geny	gen	k1gInPc4	gen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
Otx	Otx	k1gMnPc3	Otx
geny	gen	k1gInPc1	gen
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
předním	přední	k2eAgInSc6d1	přední
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
středním	střední	k2eAgInSc6d1	střední
mozku	mozek	k1gInSc6	mozek
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hox	Hox	k1gMnPc1	Hox
geny	gen	k1gInPc4	gen
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
většinu	většina	k1gFnSc4	většina
zadního	zadní	k2eAgInSc2d1	zadní
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
míchu	mícha	k1gFnSc4	mícha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
nachází	nacházet	k5eAaImIp3nS	nacházet
Pax	Pax	k1gFnSc1	Pax
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
zejména	zejména	k9	zejména
vývoj	vývoj	k1gInSc4	vývoj
mozečku	mozeček	k1gInSc2	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc4d1	celý
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
z	z	k7c2	z
embryonálního	embryonální	k2eAgInSc2d1	embryonální
neuroektodermu	neuroektoderm	k1gInSc2	neuroektoderm
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
nervové	nervový	k2eAgFnSc2d1	nervová
trubice	trubice	k1gFnSc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgNnSc7d1	důležité
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
orientaci	orientace	k1gFnSc4	orientace
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
mozku	mozek	k1gInSc2	mozek
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgInSc4d1	přední
konec	konec	k1gInSc4	konec
struny	struna	k1gFnSc2	struna
hřbetní	hřbetní	k2eAgFnSc2d1	hřbetní
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
tímto	tento	k3xDgInSc7	tento
koncem	konec	k1gInSc7	konec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
stádiích	stádium	k1gNnPc6	stádium
vývoje	vývoj	k1gInSc2	vývoj
mozku	mozek	k1gInSc2	mozek
jako	jako	k8xS	jako
archencephalon	archencephalon	k1gInSc1	archencephalon
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
nad	nad	k7c7	nad
chordou	chorda	k1gFnSc7	chorda
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
deuteroencephalon	deuteroencephalon	k1gInSc1	deuteroencephalon
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
částmi	část	k1gFnPc7	část
mozku	mozek	k1gInSc2	mozek
je	být	k5eAaImIp3nS	být
hluboká	hluboký	k2eAgFnSc1d1	hluboká
rýha	rýha	k1gFnSc1	rýha
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
z	z	k7c2	z
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
vzniká	vznikat	k5eAaImIp3nS	vznikat
prosencephalon	prosencephalon	k1gInSc1	prosencephalon
(	(	kIx(	(
<g/>
přední	přední	k2eAgInSc1d1	přední
mozek	mozek	k1gInSc1	mozek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ze	z	k7c2	z
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
mesencephalon	mesencephalon	k1gInSc1	mesencephalon
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
mozek	mozek	k1gInSc1	mozek
<g/>
)	)	kIx)	)
a	a	k8xC	a
rhombencephalon	rhombencephalon	k1gInSc1	rhombencephalon
(	(	kIx(	(
<g/>
zadní	zadní	k2eAgInSc1d1	zadní
mozek	mozek	k1gInSc1	mozek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
myelencephalon	myelencephalon	k1gInSc4	myelencephalon
(	(	kIx(	(
<g/>
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
<g/>
)	)	kIx)	)
a	a	k8xC	a
metencephalon	metencephalon	k1gInSc1	metencephalon
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
hlavně	hlavně	k9	hlavně
mozeček	mozeček	k1gInSc4	mozeček
a	a	k8xC	a
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
i	i	k9	i
Varolův	Varolův	k2eAgInSc1d1	Varolův
most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
za	za	k7c7	za
chordou	chorda	k1gFnSc7	chorda
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
deuteroencephalon	deuteroencephalon	k1gInSc1	deuteroencephalon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
medulární	medulární	k2eAgMnPc1d1	medulární
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
evolučně	evolučně	k6eAd1	evolučně
nejstarší	starý	k2eAgFnPc4d3	nejstarší
části	část	k1gFnPc4	část
mozku	mozek	k1gInSc2	mozek
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
již	již	k6eAd1	již
u	u	k7c2	u
předků	předek	k1gInPc2	předek
obratlovců	obratlovec	k1gMnPc2	obratlovec
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
u	u	k7c2	u
současných	současný	k2eAgMnPc2d1	současný
bezlebečných	bezlebeční	k1gMnPc2	bezlebeční
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
střední	střední	k2eAgInSc4d1	střední
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgInSc1d1	zadní
mozek	mozek	k1gInSc1	mozek
(	(	kIx(	(
<g/>
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
součást	součást	k1gFnSc1	součást
–	–	k?	–
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
části	část	k1gFnPc1	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
předním	přední	k2eAgInSc6d1	přední
konci	konec	k1gInSc6	konec
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
chordy	chorda	k1gFnSc2	chorda
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
premedulární	premedulární	k2eAgFnPc4d1	premedulární
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
se	se	k3xPyFc4	se
až	až	k9	až
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
mezimozek	mezimozek	k1gInSc4	mezimozek
(	(	kIx(	(
<g/>
diencephalon	diencephalon	k1gInSc4	diencephalon
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
(	(	kIx(	(
<g/>
telencephalon	telencephalon	k1gInSc4	telencephalon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
obratlovců	obratlovec	k1gMnPc2	obratlovec
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
hemisférám	hemisféra	k1gFnPc3	hemisféra
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Neurozobrazování	Neurozobrazování	k1gNnSc2	Neurozobrazování
<g/>
.	.	kIx.	.
</s>
<s>
Anatomický	anatomický	k2eAgInSc1d1	anatomický
popis	popis	k1gInSc1	popis
oddílů	oddíl	k1gInPc2	oddíl
mozku	mozek	k1gInSc2	mozek
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
rysech	rys	k1gInPc6	rys
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
embryologickým	embryologický	k2eAgNnSc7d1	embryologický
a	a	k8xC	a
fetálním	fetální	k2eAgNnSc7d1	fetální
stádiem	stádium	k1gNnSc7	stádium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
lecčem	lecco	k3yInSc6	lecco
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
obratlovců	obratlovec	k1gMnPc2	obratlovec
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
mozek	mozek	k1gInSc4	mozek
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Telencephalon	Telencephalon	k1gInSc4	Telencephalon
(	(	kIx(	(
<g/>
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
<g/>
)	)	kIx)	)
–	–	k?	–
u	u	k7c2	u
raných	raný	k2eAgMnPc2d1	raný
obratlovců	obratlovec	k1gMnPc2	obratlovec
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
zejména	zejména	k9	zejména
čichové	čichový	k2eAgNnSc1d1	čichové
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
čichový	čichový	k2eAgInSc1d1	čichový
mozek	mozek	k1gInSc1	mozek
<g/>
)	)	kIx)	)
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
ještě	ještě	k6eAd1	ještě
příliš	příliš	k6eAd1	příliš
členěn	členit	k5eAaImNgInS	členit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
patrné	patrný	k2eAgFnPc1d1	patrná
dvě	dva	k4xCgFnPc1	dva
hemisféry	hemisféra	k1gFnPc1	hemisféra
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
u	u	k7c2	u
obojživelníků	obojživelník	k1gMnPc2	obojživelník
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
uvnitř	uvnitř	k7c2	uvnitř
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
bazální	bazální	k2eAgNnPc1d1	bazální
ganglia	ganglion	k1gNnPc1	ganglion
(	(	kIx(	(
<g/>
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
corpus	corpus	k1gInSc4	corpus
striatum	striatum	k1gNnSc1	striatum
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
jádra	jádro	k1gNnPc1	jádro
šedé	šedý	k2eAgFnSc2d1	šedá
hmoty	hmota	k1gFnSc2	hmota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
šedé	šedý	k2eAgFnSc2d1	šedá
hmoty	hmota	k1gFnSc2	hmota
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
vynořovat	vynořovat	k5eAaImF	vynořovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
bazální	bazální	k2eAgNnPc1d1	bazální
ganglia	ganglion	k1gNnPc1	ganglion
naopak	naopak	k6eAd1	naopak
klesají	klesat	k5eAaImIp3nP	klesat
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
plazů	plaz	k1gMnPc2	plaz
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tzv.	tzv.	kA	tzv.
šedá	šedá	k1gFnSc1	šedá
kůra	kůra	k1gFnSc1	kůra
(	(	kIx(	(
<g/>
neokortex	neokortex	k1gInSc1	neokortex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
přejímá	přejímat	k5eAaImIp3nS	přejímat
tato	tento	k3xDgFnSc1	tento
kůra	kůra	k1gFnSc1	kůra
řadu	řad	k1gInSc2	řad
funkcí	funkce	k1gFnPc2	funkce
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Diencephalon	Diencephalon	k1gInSc1	Diencephalon
(	(	kIx(	(
<g/>
mezimozek	mezimozek	k1gInSc1	mezimozek
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
funkčních	funkční	k2eAgInPc2d1	funkční
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
mezimozku	mezimozek	k1gInSc2	mezimozek
se	se	k3xPyFc4	se
během	během	k7c2	během
zárodečného	zárodečný	k2eAgInSc2d1	zárodečný
vývoje	vývoj	k1gInSc2	vývoj
stává	stávat	k5eAaImIp3nS	stávat
zadní	zadní	k2eAgFnSc7d1	zadní
částí	část	k1gFnSc7	část
očí	oko	k1gNnPc2	oko
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k8xC	i
některým	některý	k3yIgNnPc3	některý
očním	oční	k2eAgNnPc3d1	oční
centrům	centrum	k1gNnPc3	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgNnPc1d1	důležité
mozková	mozkový	k2eAgNnPc1d1	mozkové
jádra	jádro	k1gNnPc1	jádro
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
thalamus	thalamus	k1gInSc1	thalamus
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
u	u	k7c2	u
vyšších	vysoký	k2eAgMnPc2d2	vyšší
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
střecha	střecha	k1gFnSc1	střecha
mezimozku	mezimozek	k1gInSc2	mezimozek
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
cévní	cévní	k2eAgFnSc7d1	cévní
pletení	pleteň	k1gFnSc7	pleteň
plexus	plexus	k1gInSc1	plexus
chorioideus	chorioideus	k1gInSc4	chorioideus
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
obnovuje	obnovovat	k5eAaImIp3nS	obnovovat
mozkomíšní	mozkomíšní	k2eAgInSc4d1	mozkomíšní
mok	mok	k1gInSc4	mok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
součástí	součást	k1gFnSc7	součást
mezimozku	mezimozek	k1gInSc2	mezimozek
je	být	k5eAaImIp3nS	být
hypothalamus	hypothalamus	k1gMnSc1	hypothalamus
a	a	k8xC	a
vychlipuje	vychlipovat	k5eAaImIp3nS	vychlipovat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
také	také	k9	také
neuroepifýza	neuroepifýz	k1gMnSc2	neuroepifýz
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xS	jako
světločivný	světločivný	k2eAgInSc1d1	světločivný
orgán	orgán	k1gInSc1	orgán
(	(	kIx(	(
<g/>
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
obratlovců	obratlovec	k1gMnPc2	obratlovec
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
třetí	třetí	k4xOgNnSc1	třetí
oko	oko	k1gNnSc1	oko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hypothalamus	Hypothalamus	k1gMnSc1	Hypothalamus
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
čichová	čichový	k2eAgNnPc4d1	čichové
centra	centrum	k1gNnPc4	centrum
a	a	k8xC	a
u	u	k7c2	u
vyšších	vysoký	k2eAgMnPc2d2	vyšší
obratlovců	obratlovec	k1gMnPc2	obratlovec
také	také	k6eAd1	také
centrum	centrum	k1gNnSc4	centrum
termoregulace	termoregulace	k1gFnSc2	termoregulace
<g/>
.	.	kIx.	.
</s>
<s>
Výchlipkou	výchlipka	k1gFnSc7	výchlipka
mezimozku	mezimozek	k1gInSc2	mezimozek
je	být	k5eAaImIp3nS	být
i	i	k9	i
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
hypofýzy	hypofýza	k1gFnSc2	hypofýza
(	(	kIx(	(
<g/>
neurohypofýza	neurohypofýza	k1gFnSc1	neurohypofýza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
hypofýzy	hypofýza	k1gFnSc2	hypofýza
(	(	kIx(	(
<g/>
adenohypofýza	adenohypofýza	k1gFnSc1	adenohypofýza
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
zcela	zcela	k6eAd1	zcela
odlišný	odlišný	k2eAgInSc1d1	odlišný
původ	původ	k1gInSc1	původ
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
endokrinní	endokrinní	k2eAgFnSc4d1	endokrinní
žlázu	žláza	k1gFnSc4	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Mesencephalon	Mesencephalon	k1gInSc1	Mesencephalon
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
mozek	mozek	k1gInSc1	mozek
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
zesílená	zesílený	k2eAgFnSc1d1	zesílená
stěna	stěna	k1gFnSc1	stěna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tzv.	tzv.	kA	tzv.
tectum	tectum	k1gNnSc1	tectum
s	s	k7c7	s
významnými	významný	k2eAgInPc7d1	významný
zrakovými	zrakový	k2eAgInPc7d1	zrakový
a	a	k8xC	a
sluchovými	sluchový	k2eAgInPc7d1	sluchový
centry	centr	k1gInPc7	centr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ale	ale	k9	ale
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
i	i	k9	i
třeba	třeba	k6eAd1	třeba
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
postranní	postranní	k2eAgFnSc2d1	postranní
čáry	čára	k1gFnSc2	čára
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgMnPc2d1	mnohý
obratlovců	obratlovec	k1gMnPc2	obratlovec
tectum	tectum	k1gNnSc4	tectum
víceméně	víceméně	k9	víceméně
řídí	řídit	k5eAaImIp3nS	řídit
veškerou	veškerý	k3xTgFnSc4	veškerý
pohybovou	pohybový	k2eAgFnSc4d1	pohybová
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
tectum	tectum	k1gNnSc4	tectum
zakrňuje	zakrňovat	k5eAaImIp3nS	zakrňovat
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
na	na	k7c6	na
čtverohrbolí	čtverohrbolí	k1gNnSc6	čtverohrbolí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
středního	střední	k2eAgInSc2d1	střední
mozku	mozek	k1gInSc2	mozek
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tegmentum	tegmentum	k1gNnSc1	tegmentum
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vedou	vést	k5eAaImIp3nP	vést
motorické	motorický	k2eAgFnPc1d1	motorická
dráhy	dráha	k1gFnPc1	dráha
do	do	k7c2	do
míchy	mícha	k1gFnSc2	mícha
i	i	k8xC	i
vlákna	vlákno	k1gNnPc1	vlákno
do	do	k7c2	do
dalších	další	k2eAgNnPc2d1	další
míst	místo	k1gNnPc2	místo
CNS	CNS	kA	CNS
<g/>
.	.	kIx.	.
</s>
<s>
Metencephalon	Metencephalon	k1gInSc1	Metencephalon
–	–	k?	–
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
obratlovců	obratlovec	k1gMnPc2	obratlovec
vydutý	vydutý	k2eAgInSc4d1	vydutý
útvar	útvar	k1gInSc4	útvar
zvaný	zvaný	k2eAgInSc1d1	zvaný
mozeček	mozeček	k1gInSc1	mozeček
(	(	kIx(	(
<g/>
cerebellum	cerebellum	k1gInSc1	cerebellum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zodpovídá	zodpovídat	k5eAaPmIp3nS	zodpovídat
za	za	k7c4	za
udržování	udržování	k1gNnSc4	udržování
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
primitivních	primitivní	k2eAgMnPc2d1	primitivní
obratlovců	obratlovec	k1gMnPc2	obratlovec
má	mít	k5eAaImIp3nS	mít
nepatrné	patrný	k2eNgInPc4d1	patrný
rozměry	rozměr	k1gInPc4	rozměr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výrazný	výrazný	k2eAgInSc1d1	výrazný
váček	váček	k1gInSc1	váček
vystupující	vystupující	k2eAgInSc1d1	vystupující
ze	z	k7c2	z
zadního	zadní	k2eAgInSc2d1	zadní
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
tvoří	tvořit	k5eAaImIp3nS	tvořit
hemisférovité	hemisférovitý	k2eAgNnSc1d1	hemisférovitý
uspořádání	uspořádání	k1gNnSc1	uspořádání
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
podobné	podobný	k2eAgInPc4d1	podobný
koncovému	koncový	k2eAgInSc3d1	koncový
mozku	mozek	k1gInSc3	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
mozečku	mozeček	k1gInSc2	mozeček
(	(	kIx(	(
<g/>
corpus	corpus	k1gInSc1	corpus
cerebelli	cerebelle	k1gFnSc4	cerebelle
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
evolučně	evolučně	k6eAd1	evolučně
prastaré	prastarý	k2eAgFnPc1d1	prastará
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ouška	ouško	k1gNnSc2	ouško
(	(	kIx(	(
<g/>
floculli	flocull	k1gMnPc1	flocull
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
mozeček	mozeček	k1gInSc4	mozeček
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
vzniká	vznikat	k5eAaImIp3nS	vznikat
ještě	ještě	k9	ještě
tzv.	tzv.	kA	tzv.
Varolův	Varolův	k2eAgInSc4d1	Varolův
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
křížení	křížení	k1gNnSc2	křížení
vláken	vlákna	k1gFnPc2	vlákna
spojujících	spojující	k2eAgFnPc2d1	spojující
mozeček	mozeček	k1gInSc4	mozeček
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
částmi	část	k1gFnPc7	část
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Myelencephalon	Myelencephalon	k1gInSc1	Myelencephalon
(	(	kIx(	(
<g/>
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
<g/>
)	)	kIx)	)
–	–	k?	–
funkčně	funkčně	k6eAd1	funkčně
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
morfologicky	morfologicky	k6eAd1	morfologicky
tvoří	tvořit	k5eAaImIp3nS	tvořit
spíše	spíše	k9	spíše
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
část	část	k1gFnSc4	část
mezi	mezi	k7c7	mezi
mozkem	mozek	k1gInSc7	mozek
a	a	k8xC	a
míchou	mícha	k1gFnSc7	mícha
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
evolučně	evolučně	k6eAd1	evolučně
nejstarší	starý	k2eAgInSc4d3	nejstarší
oddíl	oddíl	k1gInSc4	oddíl
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
sem	sem	k6eAd1	sem
přichází	přicházet	k5eAaImIp3nS	přicházet
řada	řada	k1gFnSc1	řada
drah	draha	k1gFnPc2	draha
ze	z	k7c2	z
zadního	zadní	k2eAgMnSc2d1	zadní
a	a	k8xC	a
středního	střední	k2eAgInSc2d1	střední
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Mozkový	mozkový	k2eAgInSc1d1	mozkový
kmen	kmen	k1gInSc1	kmen
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc4d1	společné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
střední	střední	k2eAgInSc4d1	střední
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
míchu	mícha	k1gFnSc4	mícha
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
Varolův	Varolův	k2eAgInSc4d1	Varolův
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
uložena	uložen	k2eAgNnPc4d1	uloženo
centra	centrum	k1gNnPc4	centrum
základních	základní	k2eAgInPc2d1	základní
nepodmíněných	podmíněný	k2eNgInPc2d1	nepodmíněný
reflexů	reflex	k1gInPc2	reflex
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
mozku	mozek	k1gInSc2	mozek
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řada	řada	k1gFnSc1	řada
dutin	dutina	k1gFnPc2	dutina
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mozkových	mozkový	k2eAgFnPc2d1	mozková
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
připomínku	připomínka	k1gFnSc4	připomínka
evolučního	evoluční	k2eAgInSc2d1	evoluční
původu	původ	k1gInSc2	původ
mozku	mozek	k1gInSc2	mozek
–	–	k?	–
komory	komora	k1gFnSc2	komora
totiž	totiž	k9	totiž
nejsou	být	k5eNaImIp3nP	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
zbytnělé	zbytnělý	k2eAgInPc1d1	zbytnělý
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
neurální	neurální	k2eAgFnSc2d1	neurální
trubice	trubice	k1gFnSc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
obratlovců	obratlovec	k1gMnPc2	obratlovec
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
jedna	jeden	k4xCgFnSc1	jeden
mozková	mozkový	k2eAgFnSc1d1	mozková
komora	komora	k1gFnSc1	komora
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
hemisfér	hemisféra	k1gFnPc2	hemisféra
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
propojeny	propojit	k5eAaPmNgFnP	propojit
s	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
mozkovou	mozkový	k2eAgFnSc7d1	mozková
komorou	komora	k1gFnSc7	komora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
mezimozku	mezimozek	k1gInSc6	mezimozek
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
mozková	mozkový	k2eAgFnSc1d1	mozková
komora	komora	k1gFnSc1	komora
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
v	v	k7c6	v
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
míše	mícha	k1gFnSc6	mícha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
komorou	komora	k1gFnSc7	komora
propojena	propojit	k5eAaPmNgFnS	propojit
tzv.	tzv.	kA	tzv.
Sylviovým	Sylviový	k2eAgInSc7d1	Sylviový
kanálkem	kanálek	k1gInSc7	kanálek
a	a	k8xC	a
opačnou	opačný	k2eAgFnSc7d1	opačná
stranou	strana	k1gFnSc7	strana
vyúsťuje	vyúsťovat	k5eAaImIp3nS	vyúsťovat
do	do	k7c2	do
míšního	míšní	k2eAgInSc2d1	míšní
kanálku	kanálek	k1gInSc2	kanálek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
mozkových	mozkový	k2eAgFnPc2d1	mozková
komor	komora	k1gFnPc2	komora
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
mozkomíšní	mozkomíšní	k2eAgInSc4d1	mozkomíšní
mok	mok	k1gInSc4	mok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nadnáší	nadnášet	k5eAaImIp3nS	nadnášet
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nP	chránit
ho	on	k3xPp3gNnSc4	on
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Mok	mok	k1gInSc1	mok
obíhá	obíhat	k5eAaImIp3nS	obíhat
všemi	všecek	k3xTgFnPc7	všecek
mozkovými	mozkový	k2eAgFnPc7d1	mozková
komorami	komora	k1gFnPc7	komora
i	i	k8xC	i
prostorem	prostor	k1gInSc7	prostor
pod	pod	k7c7	pod
mozkovou	mozkový	k2eAgFnSc7d1	mozková
blanou	blána	k1gFnSc7	blána
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Záhyby	záhyba	k1gFnPc1	záhyba
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
při	při	k7c6	při
růstu	růst	k1gInSc6	růst
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
převážně	převážně	k6eAd1	převážně
jen	jen	k9	jen
u	u	k7c2	u
jistých	jistý	k2eAgInPc2d1	jistý
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
primáti	primát	k1gMnPc1	primát
<g/>
,	,	kIx,	,
delfíni	delfín	k1gMnPc1	delfín
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
míše	mícha	k1gFnSc6	mícha
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
anatomicky	anatomicky	k6eAd1	anatomicky
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povrchových	povrchový	k2eAgFnPc6d1	povrchová
strukturách	struktura	k1gFnPc6	struktura
tzv.	tzv.	kA	tzv.
šedá	šedý	k2eAgFnSc1d1	šedá
hmota	hmota	k1gFnSc1	hmota
tvořená	tvořený	k2eAgFnSc1d1	tvořená
těly	tělo	k1gNnPc7	tělo
neuronů	neuron	k1gInPc2	neuron
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kůru	kůra	k1gFnSc4	kůra
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
také	také	k9	také
různá	různý	k2eAgNnPc1d1	různé
jádra	jádro	k1gNnPc1	jádro
šedé	šedý	k2eAgFnSc2d1	šedá
hmoty	hmota	k1gFnSc2	hmota
uvnitř	uvnitř	k7c2	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
tvořena	tvořit	k5eAaImNgNnP	tvořit
nervovými	nervový	k2eAgNnPc7d1	nervové
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
v	v	k7c4	v
různé	různý	k2eAgFnPc4d1	různá
nervové	nervový	k2eAgFnPc4d1	nervová
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mikroskopické	mikroskopický	k2eAgFnSc6d1	mikroskopická
úrovni	úroveň	k1gFnSc6	úroveň
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozlišit	rozlišit	k5eAaPmF	rozlišit
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
spolupodílí	spolupodílet	k5eAaImIp3nP	spolupodílet
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
neurony	neuron	k1gInPc1	neuron
(	(	kIx(	(
<g/>
nervové	nervový	k2eAgFnPc1d1	nervová
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
a	a	k8xC	a
gliové	gliové	k2eAgFnPc1d1	gliové
buňky	buňka	k1gFnPc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnPc1	druhý
jmenované	jmenovaná	k1gFnPc1	jmenovaná
mají	mít	k5eAaImIp3nP	mít
podpůrnou	podpůrný	k2eAgFnSc4d1	podpůrná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
převyšují	převyšovat	k5eAaImIp3nP	převyšovat
počet	počet	k1gInSc4	počet
neuronů	neuron	k1gInPc2	neuron
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
údajně	údajně	k6eAd1	údajně
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
<g/>
×	×	k?	×
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
větší	veliký	k2eAgNnSc4d2	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
gliových	gliův	k2eAgFnPc2d1	gliův
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
tělem	tělo	k1gNnSc7	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
obratlovců	obratlovec	k1gMnPc2	obratlovec
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
gliových	gliův	k2eAgFnPc2d1	gliův
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
gliové	gliové	k2eAgFnPc1d1	gliové
buňky	buňka	k1gFnPc1	buňka
plní	plnit	k5eAaImIp3nP	plnit
odlišné	odlišný	k2eAgFnPc1d1	odlišná
funkce	funkce	k1gFnPc1	funkce
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
např.	např.	kA	např.
oligodendrocyty	oligodendrocyt	k1gInPc1	oligodendrocyt
<g/>
,	,	kIx,	,
astrocyty	astrocyt	k1gInPc1	astrocyt
<g/>
,	,	kIx,	,
mikroglie	mikroglie	k1gFnPc1	mikroglie
<g/>
,	,	kIx,	,
ependymální	ependymální	k2eAgFnPc1d1	ependymální
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
další	další	k2eAgInPc4d1	další
typy	typ	k1gInPc4	typ
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Nervové	nervový	k2eAgFnPc4d1	nervová
buňky	buňka	k1gFnPc4	buňka
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
těžší	těžký	k2eAgMnSc1d2	těžší
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
neurony	neuron	k1gInPc1	neuron
mají	mít	k5eAaImIp3nP	mít
svá	svůj	k3xOyFgNnPc4	svůj
těla	tělo	k1gNnPc4	tělo
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
axony	axona	k1gFnPc1	axona
vysílají	vysílat	k5eAaImIp3nP	vysílat
eferentně	eferentně	k6eAd1	eferentně
(	(	kIx(	(
<g/>
odstředivě	odstředivě	k6eAd1	odstředivě
<g/>
)	)	kIx)	)
do	do	k7c2	do
periferie	periferie	k1gFnSc2	periferie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
neurony	neuron	k1gInPc1	neuron
(	(	kIx(	(
<g/>
a	a	k8xC	a
těch	ten	k3xDgFnPc2	ten
je	být	k5eAaImIp3nS	být
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
celé	celá	k1gFnPc1	celá
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
větvící	větvící	k2eAgFnPc1d1	větvící
axony	axona	k1gFnPc1	axona
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Golgi	Golgi	k1gNnSc2	Golgi
II	II	kA	II
neurony	neuron	k1gInPc1	neuron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
mají	mít	k5eAaImIp3nP	mít
axon	axon	k1gInSc1	axon
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
(	(	kIx(	(
<g/>
Golgi	Golgi	k1gNnSc1	Golgi
I	i	k8xC	i
typ	typ	k1gInSc1	typ
neuronů	neuron	k1gInPc2	neuron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgInSc1d1	dospělý
mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
schopen	schopen	k2eAgMnSc1d1	schopen
obnovovat	obnovovat	k5eAaImF	obnovovat
tkáň	tkáň	k1gFnSc4	tkáň
z	z	k7c2	z
populace	populace	k1gFnSc2	populace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
mozku	mozek	k1gInSc2	mozek
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
cévy	céva	k1gFnPc4	céva
a	a	k8xC	a
vlásečnice	vlásečnice	k1gFnPc4	vlásečnice
<g/>
.	.	kIx.	.
</s>
<s>
Stěna	stěna	k1gFnSc1	stěna
vlásečnic	vlásečnice	k1gFnPc2	vlásečnice
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
paryb	paryba	k1gFnPc2	paryba
tvořena	tvořit	k5eAaImNgNnP	tvořit
výběžky	výběžek	k1gInPc1	výběžek
gliových	gliův	k2eAgFnPc2d1	gliův
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
stěnu	stěna	k1gFnSc4	stěna
vlásečnic	vlásečnice	k1gFnPc2	vlásečnice
endotelové	endotelový	k2eAgFnSc2d1	endotelový
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Gliové	Gliové	k2eAgFnPc1d1	Gliové
buňky	buňka	k1gFnPc1	buňka
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
mohly	moct	k5eAaImAgFnP	moct
specializovat	specializovat	k5eAaBmF	specializovat
na	na	k7c4	na
odlišné	odlišný	k2eAgFnPc4d1	odlišná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
část	část	k1gFnSc1	část
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
se	s	k7c7	s
stěnami	stěna	k1gFnPc7	stěna
vlásečnic	vlásečnice	k1gFnPc2	vlásečnice
doposud	doposud	k6eAd1	doposud
<g/>
.	.	kIx.	.
</s>
<s>
Endotelové	Endotelový	k2eAgFnPc1d1	Endotelový
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
výrazné	výrazný	k2eAgFnPc4d1	výrazná
odlišnosti	odlišnost	k1gFnPc4	odlišnost
od	od	k7c2	od
endotelových	endotelův	k2eAgFnPc2d1	endotelův
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářet	k5eAaImIp3nS	vytvářet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
těsných	těsný	k2eAgInPc2d1	těsný
spojů	spoj	k1gInPc2	spoj
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
nimž	jenž	k3xRgInPc3	jenž
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
nepropustné	propustný	k2eNgInPc1d1	nepropustný
<g/>
)	)	kIx)	)
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
látky	látka	k1gFnPc1	látka
jsou	být	k5eAaImIp3nP	být
přes	přes	k7c4	přes
stěnu	stěna	k1gFnSc4	stěna
cév	céva	k1gFnPc2	céva
vyměňovány	vyměňovat	k5eAaImNgInP	vyměňovat
regulovaně	regulovaně	k6eAd1	regulovaně
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
specifických	specifický	k2eAgInPc2d1	specifický
transportérů	transportér	k1gInPc2	transportér
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
unikátní	unikátní	k2eAgFnSc3d1	unikátní
struktuře	struktura	k1gFnSc3	struktura
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
prostředím	prostředí	k1gNnSc7	prostředí
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
okolními	okolní	k2eAgFnPc7d1	okolní
tkáněmi	tkáň	k1gFnPc7	tkáň
udržuje	udržovat	k5eAaImIp3nS	udržovat
tzv.	tzv.	kA	tzv.
hematoencefalická	hematoencefalický	k2eAgFnSc1d1	hematoencefalická
bariéra	bariéra	k1gFnSc1	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
mozky	mozek	k1gInPc1	mozek
obratlovců	obratlovec	k1gMnPc2	obratlovec
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
homologické	homologický	k2eAgFnPc1d1	homologická
a	a	k8xC	a
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
mozku	mozek	k1gInSc2	mozek
předka	předek	k1gMnSc2	předek
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
značné	značný	k2eAgFnPc1d1	značná
odlišnosti	odlišnost	k1gFnPc1	odlišnost
–	–	k?	–
každý	každý	k3xTgMnSc1	každý
obratlovec	obratlovec	k1gMnSc1	obratlovec
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
jiné	jiný	k2eAgInPc4d1	jiný
požadavky	požadavek	k1gInPc4	požadavek
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Kruhoústí	kruhoústí	k1gMnPc1	kruhoústí
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
mihule	mihule	k1gFnPc1	mihule
a	a	k8xC	a
sliznatky	sliznatka	k1gFnPc1	sliznatka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
drobný	drobný	k2eAgInSc4d1	drobný
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
již	již	k9	již
patrné	patrný	k2eAgInPc1d1	patrný
všechny	všechen	k3xTgInPc1	všechen
hlavní	hlavní	k2eAgInPc1d1	hlavní
anatomické	anatomický	k2eAgInPc1d1	anatomický
rysy	rys	k1gInPc1	rys
<g/>
.	.	kIx.	.
</s>
<s>
Koncový	koncový	k2eAgInSc1d1	koncový
mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dokonce	dokonce	k9	dokonce
již	již	k6eAd1	již
i	i	k9	i
hemisféry	hemisféra	k1gFnSc2	hemisféra
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
především	především	k6eAd1	především
čichový	čichový	k2eAgInSc1d1	čichový
kyj	kyj	k1gInSc1	kyj
(	(	kIx(	(
<g/>
bulbus	bulbus	k1gInSc1	bulbus
olfactorius	olfactorius	k1gInSc1	olfactorius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
mihulí	mihule	k1gFnPc2	mihule
je	být	k5eAaImIp3nS	být
vyvinut	vyvinut	k2eAgInSc1d1	vyvinut
pineální	pineální	k2eAgInSc1d1	pineální
(	(	kIx(	(
<g/>
epifýza	epifýza	k1gFnSc1	epifýza
<g/>
)	)	kIx)	)
a	a	k8xC	a
parapineální	parapineální	k2eAgInSc1d1	parapineální
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
orgány	orgán	k1gInPc1	orgán
monitorují	monitorovat	k5eAaImIp3nP	monitorovat
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
mozku	mozek	k1gInSc6	mozek
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgNnSc1d1	vyvinuté
tectum	tectum	k1gNnSc1	tectum
<g/>
,	,	kIx,	,
ústředí	ústředí	k1gNnSc1	ústředí
smyslů	smysl	k1gInPc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Mozeček	mozeček	k1gInSc1	mozeček
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
výrazný	výrazný	k2eAgInSc1d1	výrazný
a	a	k8xC	a
možná	možná	k9	možná
úplně	úplně	k6eAd1	úplně
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
vodních	vodní	k2eAgMnPc2d1	vodní
čelistnatců	čelistnatec	k1gMnPc2	čelistnatec
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
koncový	koncový	k2eAgInSc1d1	koncový
mozek	mozek	k1gInSc1	mozek
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k9	především
z	z	k7c2	z
čichového	čichový	k2eAgNnSc2d1	čichové
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hemisféry	hemisféra	k1gFnPc1	hemisféra
jsou	být	k5eAaImIp3nP	být
nepatrné	nepatrný	k2eAgFnPc1d1	nepatrná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
epifýza	epifýza	k1gFnSc1	epifýza
(	(	kIx(	(
<g/>
fotoreceptor	fotoreceptor	k1gInSc1	fotoreceptor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
i	i	k9	i
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
parietálního	parietální	k2eAgNnSc2d1	parietální
oka	oko	k1gNnSc2	oko
(	(	kIx(	(
<g/>
parapineálního	parapineální	k2eAgInSc2d1	parapineální
orgánu	orgán	k1gInSc2	orgán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
rozvoji	rozvoj	k1gInSc3	rozvoj
mozečku	mozeček	k1gInSc2	mozeček
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
držení	držení	k1gNnSc4	držení
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
rovnováhu	rovnováha	k1gFnSc4	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
mozeček	mozeček	k1gInSc1	mozeček
překrývá	překrývat	k5eAaImIp3nS	překrývat
střední	střední	k2eAgInSc4d1	střední
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
míchu	mícha	k1gFnSc4	mícha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
mozku	mozek	k1gInSc6	mozek
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
žraloků	žralok	k1gMnPc2	žralok
nápadné	nápadný	k2eAgNnSc1d1	nápadné
zrakové	zrakový	k2eAgNnSc1d1	zrakové
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
tectum	tectum	k1gNnSc1	tectum
opticum	opticum	k1gInSc1	opticum
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
obaluje	obalovat	k5eAaImIp3nS	obalovat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
vnější	vnější	k2eAgFnSc1d1	vnější
tenká	tenký	k2eAgFnSc1d1	tenká
plena	plena	k1gFnSc1	plena
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
ectomeninx	ectomeninx	k1gInSc1	ectomeninx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
obojživelníků	obojživelník	k1gMnPc2	obojživelník
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
nejnápadnější	nápadní	k2eAgInPc1d3	nápadní
čichové	čichový	k2eAgInPc1d1	čichový
laloky	lalok	k1gInPc1	lalok
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
i	i	k9	i
hemisféry	hemisféra	k1gFnPc1	hemisféra
<g/>
.	.	kIx.	.
</s>
<s>
Šedá	šedý	k2eAgFnSc1d1	šedá
hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
valné	valný	k2eAgFnSc2d1	valná
většiny	většina	k1gFnSc2	většina
uvnitř	uvnitř	k7c2	uvnitř
hemisfér	hemisféra	k1gFnPc2	hemisféra
<g/>
.	.	kIx.	.
</s>
<s>
Pineální	pineální	k2eAgInPc1d1	pineální
a	a	k8xC	a
parapineální	parapineální	k2eAgInPc1d1	parapineální
orgány	orgán	k1gInPc1	orgán
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
obvykle	obvykle	k6eAd1	obvykle
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
redukci	redukce	k1gFnSc3	redukce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
mozku	mozek	k1gInSc6	mozek
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
zrakové	zrakový	k2eAgInPc1d1	zrakový
laloky	lalok	k1gInPc1	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Mozeček	mozeček	k1gInSc1	mozeček
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
pohybovou	pohybový	k2eAgFnSc7d1	pohybová
aktivitou	aktivita	k1gFnSc7	aktivita
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgNnSc1d2	menší
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
postrádají	postrádat	k5eAaImIp3nP	postrádat
postranní	postranní	k2eAgFnSc4d1	postranní
čáru	čára	k1gFnSc4	čára
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
obalen	obalit	k5eAaPmNgInS	obalit
dvěma	dva	k4xCgFnPc7	dva
mozkovými	mozkový	k2eAgFnPc7d1	mozková
plenami	plena	k1gFnPc7	plena
(	(	kIx(	(
<g/>
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
dura	dur	k2eAgFnSc1d1	dura
mater	mater	k1gFnSc1	mater
a	a	k8xC	a
jemnější	jemný	k2eAgInSc1d2	jemnější
leptomeninx	leptomeninx	k1gInSc1	leptomeninx
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
asi	asi	k9	asi
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
osídlování	osídlování	k1gNnSc2	osídlování
souše	souš	k1gFnPc1	souš
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
plazů	plaz	k1gMnPc2	plaz
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
obrovskému	obrovský	k2eAgInSc3d1	obrovský
rozvoji	rozvoj	k1gInSc3	rozvoj
mozkových	mozkový	k2eAgFnPc2d1	mozková
hemisfér	hemisféra	k1gFnPc2	hemisféra
a	a	k8xC	a
mozečku	mozeček	k1gInSc2	mozeček
<g/>
.	.	kIx.	.
</s>
<s>
Hemisféry	hemisféra	k1gFnPc1	hemisféra
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
takových	takový	k3xDgInPc2	takový
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vydouvají	vydouvat	k5eAaImIp3nP	vydouvat
všemi	všecek	k3xTgInPc7	všecek
směry	směr	k1gInPc7	směr
a	a	k8xC	a
překrývají	překrývat	k5eAaImIp3nP	překrývat
jiné	jiný	k2eAgFnPc4d1	jiná
části	část	k1gFnPc4	část
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
mezimozek	mezimozek	k1gInSc1	mezimozek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náznacích	náznak	k1gInPc6	náznak
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
objevuje	objevovat	k5eAaImIp3nS	objevovat
neokortex	neokortex	k1gInSc1	neokortex
<g/>
,	,	kIx,	,
šedá	šedý	k2eAgFnSc1d1	šedá
kůra	kůra	k1gFnSc1	kůra
mozková	mozkový	k2eAgFnSc1d1	mozková
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
nápadně	nápadně	k6eAd1	nápadně
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
hypothalamus	hypothalamus	k1gInSc1	hypothalamus
a	a	k8xC	a
thalamus	thalamus	k1gInSc1	thalamus
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
koordinují	koordinovat	k5eAaBmIp3nP	koordinovat
především	především	k6eAd1	především
metabolismus	metabolismus	k1gInSc4	metabolismus
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tělo	tělo	k1gNnSc1	tělo
vychladlé	vychladlý	k2eAgNnSc1d1	vychladlé
<g/>
,	,	kIx,	,
hypothalamus	hypothalamus	k1gInSc1	hypothalamus
dává	dávat	k5eAaImIp3nS	dávat
podnět	podnět	k1gInSc4	podnět
tělu	tělo	k1gNnSc3	tělo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhřívalo	vyhřívat	k5eAaImAgNnS	vyhřívat
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
mozku	mozek	k1gInSc2	mozek
vznikají	vznikat	k5eAaImIp3nP	vznikat
sluchové	sluchový	k2eAgInPc1d1	sluchový
laloky	lalok	k1gInPc1	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
mezimozku	mezimozek	k1gInSc2	mezimozek
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
ještěrů	ještěr	k1gMnPc2	ještěr
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
parietální	parietální	k2eAgNnSc1d1	parietální
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
epifýza	epifýza	k1gFnSc1	epifýza
již	již	k6eAd1	již
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
endokrinní	endokrinní	k2eAgInSc1d1	endokrinní
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Mozeček	mozeček	k1gInSc1	mozeček
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
u	u	k7c2	u
krokodýlů	krokodýl	k1gMnPc2	krokodýl
a	a	k8xC	a
želv	želva	k1gFnPc2	želva
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
i	i	k9	i
u	u	k7c2	u
příbuzných	příbuzný	k2eAgMnPc2d1	příbuzný
krokodýlů	krokodýl	k1gMnPc2	krokodýl
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
hemisfér	hemisféra	k1gFnPc2	hemisféra
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
sluchová	sluchový	k2eAgNnPc4d1	sluchové
a	a	k8xC	a
zraková	zrakový	k2eAgNnPc4d1	zrakové
centra	centrum	k1gNnPc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Čichové	čichový	k2eAgNnSc1d1	čichové
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
přítomné	přítomný	k1gMnPc4	přítomný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
redukované	redukovaný	k2eAgInPc1d1	redukovaný
(	(	kIx(	(
<g/>
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
špatný	špatný	k2eAgInSc4d1	špatný
čich	čich	k1gInSc4	čich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
zrakové	zrakový	k2eAgNnSc1d1	zrakové
centrum	centrum	k1gNnSc1	centrum
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tectu	tect	k1gInSc6	tect
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
mozeček	mozeček	k1gInSc1	mozeček
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
koordinací	koordinace	k1gFnSc7	koordinace
letu	let	k1gInSc2	let
–	–	k?	–
u	u	k7c2	u
nelétavých	létavý	k2eNgMnPc2d1	nelétavý
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
kryt	kryt	k2eAgMnSc1d1	kryt
obvykle	obvykle	k6eAd1	obvykle
dvěma	dva	k4xCgFnPc7	dva
plenami	plena	k1gFnPc7	plena
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
ale	ale	k9	ale
leptomeninx	leptomeninx	k1gInSc1	leptomeninx
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
pleny	plena	k1gFnPc4	plena
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lidský	lidský	k2eAgInSc1d1	lidský
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
velikosti	velikost	k1gFnSc2	velikost
mozku	mozek	k1gInSc2	mozek
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
zpravidla	zpravidla	k6eAd1	zpravidla
největší	veliký	k2eAgInSc1d3	veliký
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
obratlovců	obratlovec	k1gMnPc2	obratlovec
hraje	hrát	k5eAaImIp3nS	hrát
mozek	mozek	k1gInSc1	mozek
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc4	tento
snaží	snažit	k5eAaImIp3nS	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
není	být	k5eNaImIp3nS	být
jasně	jasně	k6eAd1	jasně
preferovaná	preferovaný	k2eAgFnSc1d1	preferovaná
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
nových	nový	k2eAgFnPc2d1	nová
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
obratlovců	obratlovec	k1gMnPc2	obratlovec
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jen	jen	k9	jen
ochranné	ochranný	k2eAgFnSc2d1	ochranná
mozkové	mozkový	k2eAgFnSc2d1	mozková
pleny	plena	k1gFnSc2	plena
prochází	procházet	k5eAaImIp3nS	procházet
změnou	změna	k1gFnSc7	změna
–	–	k?	–
mimo	mimo	k7c4	mimo
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
mozkovou	mozkový	k2eAgFnSc4d1	mozková
plenu	plena	k1gFnSc4	plena
(	(	kIx(	(
<g/>
dura	dur	k2eAgFnSc1d1	dura
mater	mater	k1gFnSc1	mater
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
dvě	dva	k4xCgFnPc4	dva
další	další	k2eAgFnPc4d1	další
<g/>
:	:	kIx,	:
vnější	vnější	k2eAgFnPc4d1	vnější
pavučnice	pavučnice	k1gFnPc4	pavučnice
(	(	kIx(	(
<g/>
arachnoidea	arachnoidea	k6eAd1	arachnoidea
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
omozečnice	omozečnice	k1gFnSc1	omozečnice
(	(	kIx(	(
<g/>
pia	pia	k?	pia
mater	mater	k1gFnSc1	mater
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
mícha	mícha	k1gFnSc1	mícha
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
míchou	mícha	k1gFnSc7	mícha
a	a	k8xC	a
mozkem	mozek	k1gInSc7	mozek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
důležitá	důležitý	k2eAgNnPc4d1	důležité
kardiovaskulární	kardiovaskulární	k2eAgNnPc4d1	kardiovaskulární
a	a	k8xC	a
dýchací	dýchací	k2eAgNnPc4d1	dýchací
centra	centrum	k1gNnPc4	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
prodlouženou	prodloužený	k2eAgFnSc4d1	prodloužená
míchu	mícha	k1gFnSc4	mícha
navazuje	navazovat	k5eAaImIp3nS	navazovat
Varolův	Varolův	k2eAgInSc1d1	Varolův
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
evoluční	evoluční	k2eAgFnSc1d1	evoluční
novinka	novinka	k1gFnSc1	novinka
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
s	s	k7c7	s
řízením	řízení	k1gNnSc7	řízení
dýchání	dýchání	k1gNnSc2	dýchání
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
převod	převod	k1gInSc4	převod
nervových	nervový	k2eAgInPc2d1	nervový
signálů	signál	k1gInPc2	signál
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
částí	část	k1gFnPc2	část
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Převodní	převodní	k2eAgFnSc4d1	převodní
funkci	funkce	k1gFnSc4	funkce
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
thalamus	thalamus	k1gInSc1	thalamus
(	(	kIx(	(
<g/>
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
tectum	tectum	k1gNnSc1	tectum
jako	jako	k8xC	jako
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
jiných	jiný	k2eAgMnPc2d1	jiný
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
vnímání	vnímání	k1gNnSc6	vnímání
bolesti	bolest	k1gFnSc2	bolest
a	a	k8xC	a
libosti	libost	k1gFnSc2	libost
<g/>
.	.	kIx.	.
</s>
<s>
Hypothalamus	Hypothalamus	k1gInSc1	Hypothalamus
řídí	řídit	k5eAaImIp3nS	řídit
vegetativní	vegetativní	k2eAgFnPc4d1	vegetativní
funkce	funkce	k1gFnPc4	funkce
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgFnSc1d1	vodní
a	a	k8xC	a
iontová	iontový	k2eAgFnSc1d1	iontová
rovnováha	rovnováha	k1gFnSc1	rovnováha
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgNnSc1d1	sexuální
chování	chování	k1gNnSc1	chování
<g/>
)	)	kIx)	)
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
i	i	k9	i
jako	jako	k9	jako
endokrinní	endokrinní	k2eAgFnSc1d1	endokrinní
žláza	žláza	k1gFnSc1	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Vylučování	vylučování	k1gNnSc1	vylučování
hormonů	hormon	k1gInPc2	hormon
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
i	i	k9	i
hypofýza	hypofýza	k1gFnSc1	hypofýza
a	a	k8xC	a
epifýza	epifýza	k1gFnSc1	epifýza
<g/>
.	.	kIx.	.
</s>
<s>
Koncový	koncový	k2eAgInSc1d1	koncový
mozek	mozek	k1gInSc1	mozek
zezadu	zezadu	k6eAd1	zezadu
zcela	zcela	k6eAd1	zcela
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
střední	střední	k2eAgInSc4d1	střední
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
mezimozek	mezimozek	k1gInSc4	mezimozek
a	a	k8xC	a
doznává	doznávat	k5eAaImIp3nS	doznávat
velkých	velký	k2eAgFnPc2d1	velká
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncovém	koncový	k2eAgInSc6d1	koncový
mozku	mozek	k1gInSc6	mozek
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
mnohá	mnohý	k2eAgNnPc1d1	mnohé
smyslová	smyslový	k2eAgNnPc1d1	smyslové
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
ústředí	ústředí	k1gNnSc1	ústředí
vědomých	vědomý	k2eAgInPc2d1	vědomý
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc2	myšlení
i	i	k8xC	i
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
mozku	mozek	k1gInSc6	mozek
tvořeném	tvořený	k2eAgInSc6d1	tvořený
tzv.	tzv.	kA	tzv.
neokortexem	neokortex	k1gInSc7	neokortex
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
mozkovou	mozkový	k2eAgFnSc7d1	mozková
kůrou	kůra	k1gFnSc7	kůra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
výrazné	výrazný	k2eAgNnSc4d1	výrazné
zvětšování	zvětšování	k1gNnSc4	zvětšování
hemisfér	hemisféra	k1gFnPc2	hemisféra
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
bazální	bazální	k2eAgNnPc1d1	bazální
ganglia	ganglion	k1gNnPc1	ganglion
se	se	k3xPyFc4	se
zanořují	zanořovat	k5eAaImIp3nP	zanořovat
dovnitř	dovnitř	k6eAd1	dovnitř
hemisfér	hemisféra	k1gFnPc2	hemisféra
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
zde	zde	k6eAd1	zde
tzv.	tzv.	kA	tzv.
corpus	corpus	k1gNnSc1	corpus
striatum	striatum	k1gNnSc1	striatum
<g/>
.	.	kIx.	.
</s>
<s>
Komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
polokoulemi	polokoule	k1gFnPc7	polokoule
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
kalózní	kalózní	k2eAgNnSc1d1	kalózní
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Hipokampus	Hipokampus	k1gInSc1	Hipokampus
(	(	kIx(	(
<g/>
uvnitř	uvnitř	k7c2	uvnitř
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
<g/>
)	)	kIx)	)
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
prostorovou	prostorový	k2eAgFnSc4d1	prostorová
informaci	informace	k1gFnSc4	informace
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
částí	část	k1gFnSc7	část
savčího	savčí	k2eAgInSc2d1	savčí
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
po	po	k7c6	po
koncovém	koncový	k2eAgInSc6d1	koncový
mozku	mozek	k1gInSc6	mozek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
mozeček	mozeček	k1gInSc4	mozeček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
reguluje	regulovat	k5eAaImIp3nS	regulovat
koordinaci	koordinace	k1gFnSc4	koordinace
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
neuronů	neuron	k1gInPc2	neuron
(	(	kIx(	(
<g/>
neurogeneze	neurogeneze	k1gFnSc1	neurogeneze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
altriciálních	altriciální	k2eAgNnPc2d1	altriciální
mláďat	mládě	k1gNnPc2	mládě
částečně	částečně	k6eAd1	částečně
posunuta	posunout	k5eAaPmNgFnS	posunout
do	do	k7c2	do
období	období	k1gNnSc2	období
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
čichových	čichový	k2eAgInPc2d1	čichový
bulbů	bulbus	k1gInPc2	bulbus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
laického	laický	k2eAgNnSc2d1	laické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
mozek	mozek	k1gInSc1	mozek
různých	různý	k2eAgMnPc2d1	různý
obratlovců	obratlovec	k1gMnPc2	obratlovec
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
rozmanitou	rozmanitý	k2eAgFnSc4d1	rozmanitá
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
pochopitelné	pochopitelný	k2eAgNnSc1d1	pochopitelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
velký	velký	k2eAgInSc4d1	velký
mozek	mozek	k1gInSc4	mozek
mají	mít	k5eAaImIp3nP	mít
živočichové	živočich	k1gMnPc1	živočich
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
tělem	tělo	k1gNnSc7	tělo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
i	i	k9	i
mozek	mozek	k1gInSc1	mozek
musí	muset	k5eAaImIp3nS	muset
zachovávat	zachovávat	k5eAaImF	zachovávat
určité	určitý	k2eAgNnSc4d1	určité
měřítko	měřítko	k1gNnSc4	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
alometrických	alometrický	k2eAgFnPc2d1	alometrický
studií	studie	k1gFnPc2	studie
však	však	k9	však
lze	lze	k6eAd1	lze
vztáhnout	vztáhnout	k5eAaPmF	vztáhnout
objem	objem	k1gInSc4	objem
mozku	mozek	k1gInSc2	mozek
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
daný	daný	k2eAgMnSc1d1	daný
jedinec	jedinec	k1gMnSc1	jedinec
větší	veliký	k2eAgInSc4d2	veliký
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
čekat	čekat	k5eAaImF	čekat
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
rozměry	rozměr	k1gInPc4	rozměr
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
takových	takový	k3xDgFnPc2	takový
studií	studie	k1gFnPc2	studie
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
tříd	třída	k1gFnPc2	třída
obratlovců	obratlovec	k1gMnPc2	obratlovec
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
jak	jak	k6eAd1	jak
druhy	druh	k1gInPc1	druh
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
mozkem	mozek	k1gInSc7	mozek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
druhy	druh	k1gInPc1	druh
s	s	k7c7	s
mozkem	mozek	k1gInSc7	mozek
relativně	relativně	k6eAd1	relativně
malým	malý	k2eAgInSc7d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
mnohé	mnohý	k2eAgFnPc1d1	mnohá
paryby	paryba	k1gFnPc1	paryba
(	(	kIx(	(
<g/>
Chondrichthyes	Chondrichthyes	k1gInSc1	Chondrichthyes
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
mozek	mozek	k1gInSc4	mozek
"	"	kIx"	"
<g/>
relativně	relativně	k6eAd1	relativně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
než	než	k8xS	než
třeba	třeba	k6eAd1	třeba
obojživelníci	obojživelník	k1gMnPc1	obojživelník
(	(	kIx(	(
<g/>
Amphibia	Amphibia	k1gFnSc1	Amphibia
<g/>
)	)	kIx)	)
a	a	k8xC	a
paprskoploutvé	paprskoploutvý	k2eAgFnPc1d1	paprskoploutvý
ryby	ryba	k1gFnPc1	ryba
(	(	kIx(	(
<g/>
Actinopterygii	Actinopterygie	k1gFnSc4	Actinopterygie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
srovnatelně	srovnatelně	k6eAd1	srovnatelně
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mozek	mozek	k1gInSc1	mozek
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
Mammalia	Mammalia	k1gFnSc1	Mammalia
<g/>
)	)	kIx)	)
či	či	k8xC	či
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
Aves	Aves	k1gInSc1	Aves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samé	samý	k3xTgNnSc1	samý
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
paprskoploutvé	paprskoploutvý	k2eAgFnPc4d1	paprskoploutvý
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
mozek	mozek	k1gInSc1	mozek
rovněž	rovněž	k9	rovněž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
mozkem	mozek	k1gInSc7	mozek
ostatních	ostatní	k2eAgFnPc2d1	ostatní
tříd	třída	k1gFnPc2	třída
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
savců	savec	k1gMnPc2	savec
mají	mít	k5eAaImIp3nP	mít
mírně	mírně	k6eAd1	mírně
nadprůměrně	nadprůměrně	k6eAd1	nadprůměrně
velký	velký	k2eAgInSc4d1	velký
mozek	mozek	k1gInSc4	mozek
primáti	primát	k1gMnPc1	primát
(	(	kIx(	(
<g/>
Primates	Primates	k1gMnSc1	Primates
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bychom	by	kYmCp1nP	by
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
myslet	myslet	k5eAaImF	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
lidoopi	lidoop	k1gMnPc1	lidoop
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
šimpanz	šimpanz	k1gMnSc1	šimpanz
nebo	nebo	k8xC	nebo
gorila	gorila	k1gFnSc1	gorila
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
považujeme	považovat	k5eAaImIp1nP	považovat
za	za	k7c4	za
velice	velice	k6eAd1	velice
inteligentní	inteligentní	k2eAgMnPc4d1	inteligentní
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
mozek	mozek	k1gInSc4	mozek
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
savců	savec	k1gMnPc2	savec
spíše	spíše	k9	spíše
průměrně	průměrně	k6eAd1	průměrně
velký	velký	k2eAgInSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
špici	špice	k1gFnSc6	špice
žebříčku	žebříček	k1gInSc2	žebříček
savců	savec	k1gMnPc2	savec
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
sviňuchy	sviňucha	k1gFnPc1	sviňucha
<g/>
,	,	kIx,	,
sloni	slon	k1gMnPc1	slon
a	a	k8xC	a
velryby	velryba	k1gFnPc1	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
savcům	savec	k1gMnPc3	savec
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
například	například	k6eAd1	například
papoušci	papoušek	k1gMnPc1	papoušek
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
holub	holub	k1gMnSc1	holub
má	mít	k5eAaImIp3nS	mít
mozek	mozek	k1gInSc4	mozek
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
průměrný	průměrný	k2eAgInSc1d1	průměrný
i	i	k9	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInPc4	jeho
poměrně	poměrně	k6eAd1	poměrně
složité	složitý	k2eAgInPc4d1	složitý
vzorce	vzorec	k1gInPc4	vzorec
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Srovnávání	srovnávání	k1gNnSc1	srovnávání
velikostí	velikost	k1gFnPc2	velikost
mozků	mozek	k1gInPc2	mozek
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
několik	několik	k4yIc4	několik
úskalí	úskalí	k1gNnPc2	úskalí
<g/>
.	.	kIx.	.
</s>
<s>
Předně	předně	k6eAd1	předně
závisí	záviset	k5eAaImIp3nS	záviset
výsledek	výsledek	k1gInSc1	výsledek
porovnávání	porovnávání	k1gNnSc2	porovnávání
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
zvířete	zvíře	k1gNnSc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
například	například	k6eAd1	například
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
lehké	lehký	k2eAgNnSc4d1	lehké
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
mozek	mozek	k1gInSc1	mozek
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
alometrických	alometrický	k2eAgFnPc6d1	alometrický
analýzách	analýza	k1gFnPc6	analýza
zdát	zdát	k5eAaImF	zdát
velmi	velmi	k6eAd1	velmi
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
vodní	vodní	k2eAgMnPc1d1	vodní
živočichové	živočich	k1gMnPc1	živočich
si	se	k3xPyFc3	se
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
nemusí	muset	k5eNaImIp3nS	muset
dělat	dělat	k5eAaImF	dělat
takovou	takový	k3xDgFnSc4	takový
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gFnPc4	on
voda	voda	k1gFnSc1	voda
nadnáší	nadnášet	k5eAaImIp3nS	nadnášet
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
mozek	mozek	k1gInSc1	mozek
není	být	k5eNaImIp3nS	být
jednotlivý	jednotlivý	k2eAgInSc1d1	jednotlivý
orgán	orgán	k1gInSc1	orgán
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
jeho	jeho	k3xOp3gFnSc1	jeho
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
v	v	k7c6	v
evoluci	evoluce	k1gFnSc6	evoluce
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
samostatně	samostatně	k6eAd1	samostatně
<g/>
:	:	kIx,	:
třeba	třeba	k6eAd1	třeba
zrakové	zrakový	k2eAgNnSc1d1	zrakové
centrum	centrum	k1gNnSc1	centrum
středního	střední	k2eAgInSc2d1	střední
mozku	mozek	k1gInSc2	mozek
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vyvinuté	vyvinutý	k2eAgNnSc1d1	vyvinuté
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
obratlovec	obratlovec	k1gMnSc1	obratlovec
nemůže	moct	k5eNaImIp3nS	moct
srovnávat	srovnávat	k5eAaImF	srovnávat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
obratlovec	obratlovec	k1gMnSc1	obratlovec
velký	velký	k2eAgInSc4d1	velký
mozek	mozek	k1gInSc4	mozek
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
ale	ale	k9	ale
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
než	než	k8xS	než
o	o	k7c4	o
absolutní	absolutní	k2eAgFnSc4d1	absolutní
velikost	velikost	k1gFnSc4	velikost
mozku	mozek	k1gInSc2	mozek
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
relativní	relativní	k2eAgFnSc4d1	relativní
velikost	velikost	k1gFnSc4	velikost
mozku	mozek	k1gInSc2	mozek
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ptáci	pták	k1gMnPc1	pták
rodu	rod	k1gInSc2	rod
corvus	corvus	k1gInSc4	corvus
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
mozkem	mozek	k1gInSc7	mozek
dokáží	dokázat	k5eAaPmIp3nP	dokázat
být	být	k5eAaImF	být
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
jako	jako	k8xS	jako
šimpanz	šimpanz	k1gMnSc1	šimpanz
<g/>
.	.	kIx.	.
</s>
<s>
Určitý	určitý	k2eAgInSc4d1	určitý
vztah	vztah	k1gInSc4	vztah
tu	tu	k6eAd1	tu
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
obratlovec	obratlovec	k1gMnSc1	obratlovec
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
mozkem	mozek	k1gInSc7	mozek
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
tendenci	tendence	k1gFnSc4	tendence
mít	mít	k5eAaImF	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
inteligenci	inteligence	k1gFnSc4	inteligence
než	než	k8xS	než
srovnatelně	srovnatelně	k6eAd1	srovnatelně
velký	velký	k2eAgMnSc1d1	velký
obratlovec	obratlovec	k1gMnSc1	obratlovec
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
zobecňovat	zobecňovat	k5eAaImF	zobecňovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
nebyl	být	k5eNaImAgInS	být
nalezen	nalezen	k2eAgInSc1d1	nalezen
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
velikostí	velikost	k1gFnSc7	velikost
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
funkčního	funkční	k2eAgNnSc2d1	funkční
hlediska	hledisko	k1gNnSc2	hledisko
velmi	velmi	k6eAd1	velmi
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
dnešní	dnešní	k2eAgFnSc2d1	dnešní
moderní	moderní	k2eAgFnSc2d1	moderní
neurobiologie	neurobiologie	k1gFnSc2	neurobiologie
je	být	k5eAaImIp3nS	být
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
mozek	mozek	k1gInSc1	mozek
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
poměrně	poměrně	k6eAd1	poměrně
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
poněkud	poněkud	k6eAd1	poněkud
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
však	však	k9	však
různé	různý	k2eAgFnSc2d1	různá
části	část	k1gFnSc2	část
mozku	mozek	k1gInSc2	mozek
jistým	jistý	k2eAgInSc7d1	jistý
způsobem	způsob	k1gInSc7	způsob
ovládají	ovládat	k5eAaImIp3nP	ovládat
téměř	téměř	k6eAd1	téměř
veškeré	veškerý	k3xTgNnSc4	veškerý
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
například	například	k6eAd1	například
centra	centrum	k1gNnSc2	centrum
tak	tak	k8xC	tak
základních	základní	k2eAgFnPc2d1	základní
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
trávení	trávení	k1gNnSc1	trávení
<g/>
,	,	kIx,	,
dýchání	dýchání	k1gNnSc1	dýchání
a	a	k8xC	a
srdeční	srdeční	k2eAgInSc1d1	srdeční
tep	tep	k1gInSc1	tep
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
míše	mícha	k1gFnSc6	mícha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hypothalamu	hypothalam	k1gInSc6	hypothalam
se	se	k3xPyFc4	se
zase	zase	k9	zase
nachází	nacházet	k5eAaImIp3nS	nacházet
ústředí	ústředí	k1gNnSc1	ústředí
autonomní	autonomní	k2eAgFnSc2d1	autonomní
nervové	nervový	k2eAgFnSc2d1	nervová
a	a	k8xC	a
také	také	k9	také
hormonální	hormonální	k2eAgFnSc2d1	hormonální
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
producentem	producent	k1gMnSc7	producent
hormonů	hormon	k1gInPc2	hormon
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
hypofýza	hypofýza	k1gFnSc1	hypofýza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
reguluje	regulovat	k5eAaImIp3nS	regulovat
i	i	k9	i
např.	např.	kA	např.
růst	růst	k1gInSc1	růst
těla	tělo	k1gNnSc2	tělo
nebo	nebo	k8xC	nebo
vývoj	vývoj	k1gInSc1	vývoj
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
stěžejní	stěžejní	k2eAgFnSc4d1	stěžejní
roli	role	k1gFnSc4	role
v	v	k7c6	v
ovládání	ovládání	k1gNnSc6	ovládání
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Mozeček	mozeček	k1gInSc1	mozeček
např.	např.	kA	např.
reguluje	regulovat	k5eAaImIp3nS	regulovat
razanci	razance	k1gFnSc4	razance
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
učení	učení	k1gNnSc4	učení
některých	některý	k3yIgInPc2	některý
pohybových	pohybový	k2eAgInPc2d1	pohybový
reflexů	reflex	k1gInPc2	reflex
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
mozku	mozek	k1gInSc6	mozek
je	být	k5eAaImIp3nS	být
centrum	centrum	k1gNnSc1	centrum
určitých	určitý	k2eAgInPc2d1	určitý
očních	oční	k2eAgInPc2d1	oční
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
pohyby	pohyb	k1gInPc1	pohyb
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
i	i	k9	i
činnost	činnost	k1gFnSc4	činnost
bazálních	bazální	k2eAgFnPc2d1	bazální
ganglií	ganglie	k1gFnPc2	ganglie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
většina	většina	k1gFnSc1	většina
volních	volní	k2eAgInPc2d1	volní
pohybů	pohyb	k1gInPc2	pohyb
je	být	k5eAaImIp3nS	být
řízena	řídit	k5eAaImNgFnS	řídit
mozkovou	mozkový	k2eAgFnSc7d1	mozková
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Smysly	smysl	k1gInPc1	smysl
jsou	být	k5eAaImIp3nP	být
vyhodnocovány	vyhodnocovat	k5eAaImNgInP	vyhodnocovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
na	na	k7c6	na
studovaném	studovaný	k2eAgInSc6d1	studovaný
druhu	druh	k1gInSc6	druh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
savci	savec	k1gMnPc1	savec
mají	mít	k5eAaImIp3nP	mít
většinu	většina	k1gFnSc4	většina
smyslových	smyslový	k2eAgNnPc2d1	smyslové
ústředí	ústředí	k1gNnPc2	ústředí
v	v	k7c6	v
mozkové	mozkový	k2eAgFnSc6d1	mozková
kůře	kůra	k1gFnSc6	kůra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
veškerá	veškerý	k3xTgFnSc1	veškerý
kognitivní	kognitivní	k2eAgFnSc1d1	kognitivní
činnost	činnost	k1gFnSc1	činnost
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
činnost	činnost	k1gFnSc1	činnost
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
vůlí	vůle	k1gFnSc7	vůle
<g/>
)	)	kIx)	)
totiž	totiž	k9	totiž
bývá	bývat	k5eAaImIp3nS	bývat
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
do	do	k7c2	do
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
šedé	šedý	k2eAgFnSc2d1	šedá
hmoty	hmota	k1gFnSc2	hmota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
koncového	koncový	k2eAgInSc2d1	koncový
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Mysl	mysl	k1gFnSc1	mysl
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
záhad	záhada	k1gFnPc2	záhada
moderní	moderní	k2eAgFnSc2d1	moderní
neurobiologie	neurobiologie	k1gFnSc2	neurobiologie
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
intenzívně	intenzívně	k6eAd1	intenzívně
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
objasnění	objasnění	k1gNnSc6	objasnění
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
miliony	milion	k4xCgInPc7	milion
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
spoluvytváří	spoluvytvářet	k5eAaImIp3nS	spoluvytvářet
všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
však	však	k9	však
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
funkce	funkce	k1gFnSc2	funkce
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vystopovat	vystopovat	k5eAaPmF	vystopovat
do	do	k7c2	do
určitých	určitý	k2eAgFnPc2d1	určitá
specifických	specifický	k2eAgFnPc2d1	specifická
částí	část	k1gFnPc2	část
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
již	již	k6eAd1	již
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
hemisfér	hemisféra	k1gFnPc2	hemisféra
-	-	kIx~	-
levá	levý	k2eAgFnSc1d1	levá
a	a	k8xC	a
pravá	pravý	k2eAgFnSc1d1	pravá
hemisféra	hemisféra	k1gFnSc1	hemisféra
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
potkana	potkan	k1gMnSc4	potkan
či	či	k8xC	či
kuřete	kuře	k1gNnSc2	kuře
<g/>
)	)	kIx)	)
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
poněkud	poněkud	k6eAd1	poněkud
odlišné	odlišný	k2eAgFnPc1d1	odlišná
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
smyslové	smyslový	k2eAgFnPc4d1	smyslová
a	a	k8xC	a
motorické	motorický	k2eAgFnPc4d1	motorická
(	(	kIx(	(
<g/>
pohybové	pohybový	k2eAgFnPc4d1	pohybová
<g/>
)	)	kIx)	)
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
těla	tělo	k1gNnSc2	tělo
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
hemisféry	hemisféra	k1gFnSc2	hemisféra
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
mozková	mozkový	k2eAgFnSc1d1	mozková
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejsložitější	složitý	k2eAgMnSc1d3	nejsložitější
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
obratlovců	obratlovec	k1gMnPc2	obratlovec
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zraková	zrakový	k2eAgNnPc1d1	zrakové
<g/>
,	,	kIx,	,
sluchová	sluchový	k2eAgNnPc1d1	sluchové
<g/>
,	,	kIx,	,
somatosenzorická	somatosenzorický	k2eAgNnPc1d1	somatosenzorický
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
smyslová	smyslový	k2eAgNnPc1d1	smyslové
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
centra	centrum	k1gNnSc2	centrum
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
limbickém	limbický	k2eAgInSc6d1	limbický
systému	systém	k1gInSc6	systém
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
centra	centrum	k1gNnSc2	centrum
emocí	emoce	k1gFnPc2	emoce
a	a	k8xC	a
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
lidské	lidský	k2eAgFnSc2d1	lidská
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
sídlí	sídlet	k5eAaImIp3nS	sídlet
schopnost	schopnost	k1gFnSc4	schopnost
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc7d1	základní
funkční	funkční	k2eAgFnSc7d1	funkční
jednotkou	jednotka	k1gFnSc7	jednotka
mozku	mozek	k1gInSc2	mozek
je	být	k5eAaImIp3nS	být
neuron	neuron	k1gInSc1	neuron
čili	čili	k8xC	čili
nervová	nervový	k2eAgFnSc1d1	nervová
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dendritech	dendrit	k1gInPc6	dendrit
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
neuronu	neuron	k1gInSc2	neuron
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
příjmu	příjem	k1gInSc3	příjem
signálů	signál	k1gInPc2	signál
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
nervových	nervový	k2eAgFnPc2d1	nervová
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
následně	následně	k6eAd1	následně
generují	generovat	k5eAaImIp3nP	generovat
nervový	nervový	k2eAgInSc4d1	nervový
impuls	impuls	k1gInSc4	impuls
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
putuje	putovat	k5eAaImIp3nS	putovat
tzv.	tzv.	kA	tzv.
axonem	axon	k1gInSc7	axon
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
nervovým	nervový	k2eAgFnPc3d1	nervová
buňkám	buňka	k1gFnPc3	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
nervová	nervový	k2eAgFnSc1d1	nervová
buňka	buňka	k1gFnSc1	buňka
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
až	až	k9	až
200	[number]	k4	200
000	[number]	k4	000
nervových	nervový	k2eAgNnPc2d1	nervové
spojení	spojení	k1gNnPc2	spojení
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
signálů	signál	k1gInPc2	signál
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
na	na	k7c4	na
buňku	buňka	k1gFnSc4	buňka
probíhá	probíhat	k5eAaImIp3nS	probíhat
přes	přes	k7c4	přes
tzv.	tzv.	kA	tzv.
synaptická	synaptický	k2eAgNnPc4d1	synaptické
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
elektrické	elektrický	k2eAgFnSc2d1	elektrická
synapse	synapse	k1gFnSc2	synapse
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
elektrický	elektrický	k2eAgInSc1d1	elektrický
impuls	impuls	k1gInSc1	impuls
protéká	protékat	k5eAaImIp3nS	protékat
skrz	skrz	k7c4	skrz
mezerové	mezerový	k2eAgInPc4d1	mezerový
spoje	spoj	k1gInPc4	spoj
mezi	mezi	k7c7	mezi
neurony	neuron	k1gInPc7	neuron
<g/>
,	,	kIx,	,
a	a	k8xC	a
chemické	chemický	k2eAgFnPc1d1	chemická
synapse	synapse	k1gFnPc1	synapse
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
spolu	spolu	k6eAd1	spolu
buňky	buňka	k1gFnPc1	buňka
komunikují	komunikovat	k5eAaImIp3nP	komunikovat
pomocí	pomocí	k7c2	pomocí
neurotransmiterů	neurotransmiter	k1gInPc2	neurotransmiter
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
malých	malý	k2eAgFnPc2d1	malá
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
vylučovaných	vylučovaný	k2eAgFnPc2d1	vylučovaná
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
mezi	mezi	k7c4	mezi
nervové	nervový	k2eAgFnPc4d1	nervová
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
