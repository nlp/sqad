<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wien	Wien	k1gNnSc1	Wien
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
také	také	k9	také
statutární	statutární	k2eAgNnSc4d1	statutární
město	město	k1gNnSc4	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1922	[number]	k4	1922
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
obklopená	obklopený	k2eAgFnSc1d1	obklopená
územím	území	k1gNnSc7	území
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
rakouským	rakouský	k2eAgNnSc7d1	rakouské
městem	město	k1gNnSc7	město
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
politickým	politický	k2eAgNnSc7d1	politické
<g/>
,	,	kIx,	,
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
Vídně	Vídeň	k1gFnSc2	Vídeň
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celosvětovém	celosvětový	k2eAgInSc6d1	celosvětový
seznamu	seznam	k1gInSc6	seznam
měst	město	k1gNnPc2	město
podle	podle	k7c2	podle
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
sestaveném	sestavený	k2eAgInSc6d1	sestavený
firmou	firma	k1gFnSc7	firma
Mercer	Mercer	k1gMnSc1	Mercer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Vídeň	Vídeň	k1gFnSc1	Vídeň
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
Světové	světový	k2eAgNnSc4d1	světové
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2017	[number]	k4	2017
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
zařazení	zařazení	k1gNnSc4	zařazení
historického	historický	k2eAgNnSc2d1	historické
centra	centrum	k1gNnSc2	centrum
Vídně	Vídeň	k1gFnSc2	Vídeň
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
památek	památka	k1gFnPc2	památka
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rakouska	Rakousko	k1gNnSc2	Rakousko
dosti	dosti	k6eAd1	dosti
excentricky	excentricky	k6eAd1	excentricky
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
okraji	okraj	k1gInSc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
od	od	k7c2	od
nejvzdálenější	vzdálený	k2eAgFnSc2d3	nejvzdálenější
části	část	k1gFnSc2	část
rakouského	rakouský	k2eAgNnSc2d1	rakouské
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
Vorarlbersko	Vorarlbersko	k1gNnSc1	Vorarlbersko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
čarou	čára	k1gFnSc7	čára
přes	přes	k7c4	přes
500	[number]	k4	500
km	km	kA	km
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
centrum	centrum	k1gNnSc1	centrum
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
pouze	pouze	k6eAd1	pouze
kolem	kolem	k7c2	kolem
55	[number]	k4	55
km	km	kA	km
od	od	k7c2	od
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
jen	jen	k9	jen
40	[number]	k4	40
km	km	kA	km
od	od	k7c2	od
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
historický	historický	k2eAgInSc4d1	historický
územní	územní	k2eAgInSc4d1	územní
vývoj	vývoj	k1gInSc4	vývoj
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
jádro	jádro	k1gNnSc1	jádro
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousích	Rakousy	k1gInPc6	Rakousy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
někdejších	někdejší	k2eAgFnPc2d1	někdejší
držav	država	k1gFnPc2	država
se	se	k3xPyFc4	se
udržely	udržet	k5eAaPmAgInP	udržet
prakticky	prakticky	k6eAd1	prakticky
jen	jen	k9	jen
ty	ten	k3xDgInPc1	ten
alpské	alpský	k2eAgInPc4d1	alpský
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
Vídeň	Vídeň	k1gFnSc1	Vídeň
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
úbočí	úbočí	k1gNnSc6	úbočí
Vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Dunaj	Dunaj	k1gInSc1	Dunaj
protéká	protékat	k5eAaImIp3nS	protékat
Korneuburskou	Korneuburský	k2eAgFnSc7d1	Korneuburský
bránou	brána	k1gFnSc7	brána
a	a	k8xC	a
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
pánve	pánev	k1gFnSc2	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
položena	položit	k5eAaPmNgFnS	položit
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
Bratislava	Bratislava	k1gFnSc1	Bratislava
nebo	nebo	k8xC	nebo
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
Vídně	Vídeň	k1gFnSc2	Vídeň
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
ostrohu	ostroha	k1gFnSc4	ostroha
nad	nad	k7c7	nad
ústím	ústí	k1gNnSc7	ústí
říčky	říčka	k1gFnSc2	říčka
Vídeňky	Vídeňka	k1gFnSc2	Vídeňka
(	(	kIx(	(
<g/>
Wien	Wien	k1gInSc1	Wien
<g/>
)	)	kIx)	)
do	do	k7c2	do
ramene	rameno	k1gNnSc2	rameno
Dunaje	Dunaj	k1gInSc2	Dunaj
zvaného	zvaný	k2eAgInSc2d1	zvaný
dnes	dnes	k6eAd1	dnes
Donaukanal	Donaukanal	k1gMnSc2	Donaukanal
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Dunajský	dunajský	k2eAgInSc1d1	dunajský
kanál	kanál	k1gInSc1	kanál
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
pánev	pánev	k1gFnSc1	pánev
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
výhodné	výhodný	k2eAgFnSc3d1	výhodná
poloze	poloha	k1gFnSc3	poloha
a	a	k8xC	a
příznivému	příznivý	k2eAgNnSc3d1	příznivé
klimatu	klima	k1gNnSc3	klima
nepřetržitě	přetržitě	k6eNd1	přetržitě
osídlena	osídlit	k5eAaPmNgNnP	osídlit
už	už	k6eAd1	už
od	od	k7c2	od
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
dostalo	dostat	k5eAaPmAgNnS	dostat
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
keltské	keltský	k2eAgFnSc6d1	keltská
osadě	osada	k1gFnSc6	osada
Vedunia	Vedunium	k1gNnSc2	Vedunium
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
lesní	lesní	k2eAgInSc4d1	lesní
potok	potok	k1gInSc4	potok
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
přelomu	přelom	k1gInSc2	přelom
letopočtu	letopočet	k1gInSc2	letopočet
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
dobyto	dobyt	k2eAgNnSc1d1	dobyto
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
vojenský	vojenský	k2eAgInSc4d1	vojenský
tábor	tábor	k1gInSc4	tábor
s	s	k7c7	s
přilehlým	přilehlý	k2eAgNnSc7d1	přilehlé
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Neslo	nést	k5eAaImAgNnS	nést
jméno	jméno	k1gNnSc1	jméno
Vindobona	Vindobon	k1gMnSc2	Vindobon
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
byla	být	k5eAaImAgFnS	být
obrana	obrana	k1gFnSc1	obrana
severní	severní	k2eAgFnSc2d1	severní
hranice	hranice	k1gFnSc2	hranice
říše	říš	k1gFnSc2	říš
před	před	k7c7	před
germánskými	germánský	k2eAgInPc7d1	germánský
kmeny	kmen	k1gInPc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
180	[number]	k4	180
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Markomanům	Markoman	k1gMnPc3	Markoman
zde	zde	k6eAd1	zde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
císař	císař	k1gMnSc1	císař
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
opustili	opustit	k5eAaPmAgMnP	opustit
město	město	k1gNnSc4	město
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
osídlení	osídlení	k1gNnSc1	osídlení
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
nezaniklo	zaniknout	k5eNaPmAgNnS	zaniknout
a	a	k8xC	a
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
římských	římský	k2eAgInPc6d1	římský
základech	základ	k1gInPc6	základ
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
opět	opět	k6eAd1	opět
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1155	[number]	k4	1155
učinil	učinit	k5eAaImAgMnS	učinit
markrabě	markrabě	k1gMnSc1	markrabě
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jasomirgott	Jasomirgott	k5eAaPmF	Jasomirgott
Vídeň	Vídeň	k1gFnSc4	Vídeň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Privilegia	privilegium	k1gNnSc2	privilegium
minus	minus	k1gNnSc4	minus
(	(	kIx(	(
<g/>
1156	[number]	k4	1156
<g/>
)	)	kIx)	)
povýšených	povýšený	k2eAgInPc2d1	povýšený
na	na	k7c6	na
vévodství	vévodství	k1gNnSc6	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
zajat	zajat	k2eAgMnSc1d1	zajat
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
I.	I.	kA	I.
Lví	lví	k2eAgNnSc1d1	lví
srdce	srdce	k1gNnSc1	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Výkupné	výkupný	k2eAgFnPc1d1	výkupná
složené	složený	k2eAgFnPc1d1	složená
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
propuštění	propuštění	k1gNnSc4	propuštění
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
stimulem	stimul	k1gInSc7	stimul
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
jako	jako	k8xS	jako
významného	významný	k2eAgNnSc2d1	významné
obchodního	obchodní	k2eAgNnSc2d1	obchodní
střediska	středisko	k1gNnSc2	středisko
v	v	k7c6	v
Podunají	Podunají	k1gNnSc6	Podunají
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
prvních	první	k4xOgMnPc2	první
Habsburků	Habsburk	k1gMnPc2	Habsburk
se	se	k3xPyFc4	se
Vídeň	Vídeň	k1gFnSc1	Vídeň
jako	jako	k8xS	jako
jejich	jejich	k3xOp3gNnSc4	jejich
sídelní	sídelní	k2eAgNnSc4d1	sídelní
město	město	k1gNnSc4	město
stala	stát	k5eAaPmAgFnS	stát
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
centrem	centr	k1gInSc7	centr
Svaté	svatá	k1gFnSc2	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
na	na	k7c4	na
císařský	císařský	k2eAgInSc4d1	císařský
trůn	trůn	k1gInSc4	trůn
převzalo	převzít	k5eAaPmAgNnS	převzít
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
době	doba	k1gFnSc6	doba
husitské	husitský	k2eAgFnSc2d1	husitská
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
opět	opět	k6eAd1	opět
až	až	k9	až
do	do	k7c2	do
panování	panování	k1gNnSc2	panování
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
severně	severně	k6eAd1	severně
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1438	[number]	k4	1438
se	se	k3xPyFc4	se
Vídeň	Vídeň	k1gFnSc1	Vídeň
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
–	–	k?	–
s	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
za	za	k7c2	za
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
opět	opět	k6eAd1	opět
stala	stát	k5eAaPmAgFnS	stát
rezidenčním	rezidenční	k2eAgNnSc7d1	rezidenční
městem	město	k1gNnSc7	město
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
(	(	kIx(	(
<g/>
1469	[number]	k4	1469
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Vídeň	Vídeň	k1gFnSc1	Vídeň
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
podunajského	podunajský	k2eAgNnSc2d1	podunajské
soustátí	soustátí	k1gNnSc2	soustátí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
rakouské	rakouský	k2eAgNnSc1d1	rakouské
<g/>
,	,	kIx,	,
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
uherské	uherský	k2eAgFnSc2d1	uherská
země	zem	k1gFnSc2	zem
a	a	k8xC	a
s	s	k7c7	s
krátkými	krátký	k2eAgFnPc7d1	krátká
přestávkami	přestávka	k1gFnPc7	přestávka
jím	jíst	k5eAaImIp1nS	jíst
zůstala	zůstat	k5eAaPmAgFnS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1529	[number]	k4	1529
a	a	k8xC	a
1683	[number]	k4	1683
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
Vídně	Vídeň	k1gFnSc2	Vídeň
odražena	odražen	k2eAgNnPc1d1	odraženo
turecká	turecký	k2eAgNnPc1d1	turecké
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
úspěšně	úspěšně	k6eAd1	úspěšně
postupovala	postupovat	k5eAaImAgFnS	postupovat
přes	přes	k7c4	přes
celý	celý	k2eAgInSc4d1	celý
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
centrem	centr	k1gMnSc7	centr
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
velmocí	velmoc	k1gFnPc2	velmoc
a	a	k8xC	a
rezidencí	rezidence	k1gFnPc2	rezidence
císaře	císař	k1gMnSc2	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
byla	být	k5eAaImAgFnS	být
role	role	k1gFnSc1	role
Vídně	Vídeň	k1gFnSc2	Vídeň
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
císaře	císař	k1gMnSc4	císař
Francouzů	Francouz	k1gMnPc2	Francouz
Napoleona	Napoleon	k1gMnSc4	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
po	po	k7c6	po
jeho	jeho	k3xOp3gNnPc6	jeho
vítězstvích	vítězství	k1gNnPc6	vítězství
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
nově	nově	k6eAd1	nově
vytvořeného	vytvořený	k2eAgNnSc2d1	vytvořené
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
svolán	svolat	k5eAaPmNgInS	svolat
kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
řešil	řešit	k5eAaImAgMnS	řešit
nové	nový	k2eAgNnSc4d1	nové
poválečné	poválečný	k2eAgNnSc4d1	poválečné
uspořádání	uspořádání	k1gNnSc4	uspořádání
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
pádu	pád	k1gInSc6	pád
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
počátek	počátek	k1gInSc1	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
poznamenány	poznamenán	k2eAgMnPc4d1	poznamenán
bouřlivým	bouřlivý	k2eAgInSc7d1	bouřlivý
rozvojem	rozvoj	k1gInSc7	rozvoj
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
hradeb	hradba	k1gFnPc2	hradba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
okružní	okružní	k2eAgFnSc1d1	okružní
třída	třída	k1gFnSc1	třída
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
veřejnými	veřejný	k2eAgFnPc7d1	veřejná
budovami	budova	k1gFnPc7	budova
-	-	kIx~	-
Ringstraße	Ringstraße	k1gFnSc7	Ringstraße
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
a	a	k8xC	a
dřívější	dřívější	k2eAgNnSc4d1	dřívější
předměstí	předměstí	k1gNnSc4	předměstí
se	se	k3xPyFc4	se
propojila	propojit	k5eAaPmAgFnS	propojit
s	s	k7c7	s
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byla	být	k5eAaImAgFnS	být
Vídeň	Vídeň	k1gFnSc1	Vídeň
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
i	i	k9	i
významným	významný	k2eAgInSc7d1	významný
centrem	centr	k1gInSc7	centr
uměleckého	umělecký	k2eAgInSc2d1	umělecký
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
ranou	rána	k1gFnSc7	rána
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
byl	být	k5eAaImAgInS	být
rozpad	rozpad	k1gInSc1	rozpad
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
své	své	k1gNnSc4	své
zázemí	zázemí	k1gNnSc2	zázemí
v	v	k7c6	v
obrovské	obrovský	k2eAgFnSc6d1	obrovská
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
předimenzovaným	předimenzovaný	k2eAgNnSc7d1	předimenzované
centrem	centrum	k1gNnSc7	centrum
malé	malá	k1gFnSc2	malá
první	první	k4xOgFnSc2	první
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Postihl	postihnout	k5eAaPmAgMnS	postihnout
ho	on	k3xPp3gNnSc4	on
i	i	k9	i
odchod	odchod	k1gInSc4	odchod
části	část	k1gFnSc2	část
českého	český	k2eAgMnSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgNnSc2d1	slovenské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
nového	nový	k2eAgNnSc2d1	nové
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
zhruba	zhruba	k6eAd1	zhruba
čtvrt	čtvrt	k1xP	čtvrt
milionu	milion	k4xCgInSc2	milion
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
Vídeň	Vídeň	k1gFnSc1	Vídeň
stala	stát	k5eAaPmAgFnS	stát
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc1	její
obecní	obecní	k2eAgFnSc1d1	obecní
rada	rada	k1gFnSc1	rada
tak	tak	k6eAd1	tak
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
status	status	k1gInSc4	status
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
byla	být	k5eAaImAgFnS	být
Vídeň	Vídeň	k1gFnSc1	Vídeň
baštou	bašta	k1gFnSc7	bašta
socialistického	socialistický	k2eAgNnSc2d1	socialistické
hnutí	hnutí	k1gNnSc2	hnutí
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
při	při	k7c6	při
krátké	krátký	k2eAgFnSc6d1	krátká
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
však	však	k9	však
již	již	k9	již
její	její	k3xOp3gMnPc1	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
připravili	připravit	k5eAaPmAgMnP	připravit
triumfální	triumfální	k2eAgNnSc4d1	triumfální
uvítání	uvítání	k1gNnSc4	uvítání
Adolfu	Adolf	k1gMnSc3	Adolf
Hitlerovi	Hitler	k1gMnSc3	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
Vídeň	Vídeň	k1gFnSc1	Vídeň
ztratila	ztratit	k5eAaPmAgFnS	ztratit
pozici	pozice	k1gFnSc4	pozice
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
samostatného	samostatný	k2eAgInSc2d1	samostatný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
okupační	okupační	k2eAgFnPc4d1	okupační
zóny	zóna	k1gFnPc4	zóna
a	a	k8xC	a
vítězné	vítězný	k2eAgFnPc4d1	vítězná
velmoci	velmoc	k1gFnPc4	velmoc
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
a	a	k8xC	a
Francie	Francie	k1gFnPc1	Francie
spravovaly	spravovat	k5eAaImAgFnP	spravovat
Vídeň	Vídeň	k1gFnSc4	Vídeň
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
až	až	k6eAd1	až
do	do	k7c2	do
obnovení	obnovení	k1gNnSc2	obnovení
plné	plný	k2eAgFnSc2d1	plná
rakouské	rakouský	k2eAgFnSc2d1	rakouská
suverenity	suverenita	k1gFnSc2	suverenita
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
zažívá	zažívat	k5eAaImIp3nS	zažívat
Vídeň	Vídeň	k1gFnSc4	Vídeň
prudký	prudký	k2eAgInSc4d1	prudký
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rakouské	rakouský	k2eAgFnSc3d1	rakouská
neutralitě	neutralita	k1gFnSc3	neutralita
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
UNIDO	UNIDO	kA	UNIDO
<g/>
,	,	kIx,	,
UNOV	UNOV	kA	UNOV
<g/>
,	,	kIx,	,
CTBTO	CTBTO	kA	CTBTO
<g/>
,	,	kIx,	,
UNODC	UNODC	kA	UNODC
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
IAEA	IAEA	kA	IAEA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
OBSE	OBSE	kA	OBSE
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
Organizace	organizace	k1gFnSc2	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgMnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
(	(	kIx(	(
<g/>
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc1d1	kulturní
aktivity	aktivita	k1gFnPc1	aktivita
ve	v	k7c6	v
městě	město	k1gNnSc6	město
dopomohly	dopomoct	k5eAaPmAgInP	dopomoct
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
atraktivitě	atraktivita	k1gFnSc3	atraktivita
Vídně	Vídeň	k1gFnSc2	Vídeň
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
srovnání	srovnání	k1gNnSc6	srovnání
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
svobodného	svobodný	k2eAgNnSc2d1	svobodné
podnikání	podnikání	k1gNnSc2	podnikání
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
brzy	brzy	k6eAd1	brzy
pozitivně	pozitivně	k6eAd1	pozitivně
odlišila	odlišit	k5eAaPmAgFnS	odlišit
stupeň	stupeň	k1gInSc4	stupeň
rozvoje	rozvoj	k1gInSc2	rozvoj
hospodářství	hospodářství	k1gNnSc2	hospodářství
Vídně	Vídeň	k1gFnSc2	Vídeň
a	a	k8xC	a
celého	celý	k2eAgNnSc2d1	celé
Rakouska	Rakousko	k1gNnSc2	Rakousko
od	od	k7c2	od
sousedících	sousedící	k2eAgInPc2d1	sousedící
socialistických	socialistický	k2eAgInPc2d1	socialistický
států	stát	k1gInPc2	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
usadily	usadit	k5eAaPmAgFnP	usadit
pobočky	pobočka	k1gFnPc1	pobočka
velkých	velký	k2eAgFnPc2d1	velká
světových	světový	k2eAgFnPc2d1	světová
firem	firma	k1gFnPc2	firma
zaměřené	zaměřený	k2eAgFnSc2d1	zaměřená
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
<g/>
,	,	kIx,	,
Jugoslávií	Jugoslávie	k1gFnSc7	Jugoslávie
a	a	k8xC	a
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Varšavského	varšavský	k2eAgInSc2d1	varšavský
paktu	pakt	k1gInSc2	pakt
se	se	k3xPyFc4	se
podpůrná	podpůrný	k2eAgFnSc1d1	podpůrná
a	a	k8xC	a
zprostředkovatelská	zprostředkovatelský	k2eAgFnSc1d1	zprostředkovatelská
role	role	k1gFnSc1	role
vídeňských	vídeňský	k2eAgFnPc2d1	Vídeňská
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
obchodních	obchodní	k2eAgFnPc2d1	obchodní
a	a	k8xC	a
poradenských	poradenský	k2eAgFnPc2d1	poradenská
firem	firma	k1gFnPc2	firma
pro	pro	k7c4	pro
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozvoj	rozvoj	k1gInSc4	rozvoj
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
Východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
ještě	ještě	k6eAd1	ještě
umocnila	umocnit	k5eAaPmAgFnS	umocnit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
Vídeň	Vídeň	k1gFnSc1	Vídeň
1	[number]	k4	1
713	[number]	k4	713
957	[number]	k4	957
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
byla	být	k5eAaImAgFnS	být
Vídeň	Vídeň	k1gFnSc1	Vídeň
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvěma	dva	k4xCgNnPc7	dva
miliony	milion	k4xCgInPc7	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
centrem	centrum	k1gNnSc7	centrum
velké	velký	k2eAgFnSc2d1	velká
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc2d1	mnohonárodnostní
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc7	on
vtiskly	vtisknout	k5eAaPmAgInP	vtisknout
charakter	charakter	k1gInSc4	charakter
sídla	sídlo	k1gNnSc2	sídlo
tvořeného	tvořený	k2eAgInSc2d1	tvořený
lidmi	člověk	k1gMnPc7	člověk
různého	různý	k2eAgInSc2d1	různý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
byla	být	k5eAaImAgFnS	být
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvořena	tvořit	k5eAaImNgFnS	tvořit
rodáky	rodák	k1gMnPc7	rodák
z	z	k7c2	z
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Češi	Čech	k1gMnPc1	Čech
i	i	k8xC	i
čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
přistěhovalecké	přistěhovalecký	k2eAgFnPc1d1	přistěhovalecká
vlny	vlna	k1gFnPc1	vlna
přišly	přijít	k5eAaPmAgFnP	přijít
z	z	k7c2	z
Haliče	Halič	k1gFnSc2	Halič
(	(	kIx(	(
<g/>
především	především	k9	především
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
Poláci	Polák	k1gMnPc1	Polák
<g/>
)	)	kIx)	)
a	a	k8xC	a
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
město	město	k1gNnSc1	město
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
rodných	rodný	k2eAgFnPc2d1	rodná
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
Čechy	Čech	k1gMnPc4	Čech
<g/>
,	,	kIx,	,
Slováky	Slovák	k1gMnPc4	Slovák
a	a	k8xC	a
Maďary	Maďar	k1gMnPc4	Maďar
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
vytvořením	vytvoření	k1gNnSc7	vytvoření
Velké	velká	k1gFnSc2	velká
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
ale	ale	k9	ale
zase	zase	k9	zase
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
odchodu	odchod	k1gInSc2	odchod
140	[number]	k4	140
000	[number]	k4	000
a	a	k8xC	a
vyvraždění	vyvraždění	k1gNnSc4	vyvraždění
dalších	další	k2eAgInPc2d1	další
60	[number]	k4	60
000	[number]	k4	000
vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Zanikl	zaniknout	k5eAaPmAgMnS	zaniknout
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
osobitý	osobitý	k2eAgInSc4d1	osobitý
charakter	charakter	k1gInSc4	charakter
II	II	kA	II
<g/>
.	.	kIx.	.
okresu	okres	k1gInSc2	okres
(	(	kIx(	(
<g/>
Leopoldstadtu	Leopoldstadt	k1gInSc2	Leopoldstadt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
určen	určit	k5eAaPmNgInS	určit
právě	právě	k9	právě
touto	tento	k3xDgFnSc7	tento
menšinou	menšina	k1gFnSc7	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
až	až	k9	až
na	na	k7c4	na
1,6	[number]	k4	1,6
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
padlo	padnout	k5eAaImAgNnS	padnout
mnoho	mnoho	k4c1	mnoho
vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
vojáků	voják	k1gMnPc2	voják
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
20	[number]	k4	20
%	%	kIx~	%
zničeno	zničen	k2eAgNnSc1d1	zničeno
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
okupováno	okupovat	k5eAaBmNgNnS	okupovat
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
vojsky	vojsky	k6eAd1	vojsky
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
posléze	posléze	k6eAd1	posléze
klesl	klesnout	k5eAaPmAgInS	klesnout
až	až	k9	až
pod	pod	k7c7	pod
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
demografickém	demografický	k2eAgInSc6d1	demografický
propadu	propad	k1gInSc6	propad
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
změna	změna	k1gFnSc1	změna
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
vystěhovalectví	vystěhovalectví	k1gNnSc2	vystěhovalectví
na	na	k7c4	na
dolnorakouský	dolnorakouský	k2eAgInSc4d1	dolnorakouský
venkov	venkov	k1gInSc4	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
opětovný	opětovný	k2eAgInSc1d1	opětovný
nárůst	nárůst	k1gInSc1	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Vídně	Vídeň	k1gFnSc2	Vídeň
nebyl	být	k5eNaImAgInS	být
důsledkem	důsledek	k1gInSc7	důsledek
zvýšení	zvýšení	k1gNnSc2	zvýšení
porodnosti	porodnost	k1gFnSc2	porodnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
zde	zde	k6eAd1	zde
našlo	najít	k5eAaPmAgNnS	najít
azyl	azyl	k1gInSc4	azyl
60	[number]	k4	60
000	[number]	k4	000
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
se	se	k3xPyFc4	se
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
dalších	další	k2eAgInPc2d1	další
85	[number]	k4	85
000	[number]	k4	000
nových	nový	k2eAgMnPc2d1	nový
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Pestrý	pestrý	k2eAgInSc1d1	pestrý
původ	původ	k1gInSc1	původ
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
podílu	podíl	k1gInSc6	podíl
příjmení	příjmení	k1gNnSc2	příjmení
slovanského	slovanský	k2eAgInSc2d1	slovanský
a	a	k8xC	a
maďarského	maďarský	k2eAgInSc2d1	maďarský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
spolkových	spolkový	k2eAgFnPc6d1	spolková
zemích	zem	k1gFnPc6	zem
Rakouska	Rakousko	k1gNnSc2	Rakousko
často	často	k6eAd1	často
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
původ	původ	k1gInSc4	původ
nositele	nositel	k1gMnSc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Vídně	Vídeň	k1gFnSc2	Vídeň
doposud	doposud	k6eAd1	doposud
používají	používat	k5eAaImIp3nP	používat
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
dialekt	dialekt	k1gInSc4	dialekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
u	u	k7c2	u
mladší	mladý	k2eAgFnSc2d2	mladší
generace	generace	k1gFnSc2	generace
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
na	na	k7c4	na
několik	několik	k4yIc4	několik
základních	základní	k2eAgNnPc2d1	základní
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
slovních	slovní	k2eAgInPc2d1	slovní
obratů	obrat	k1gInPc2	obrat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jen	jen	k9	jen
1,55	[number]	k4	1,55
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
necelá	celý	k2eNgFnSc1d1	necelá
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
nenarodila	narodit	k5eNaPmAgFnS	narodit
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
necelá	celý	k2eNgFnSc1d1	necelá
pětina	pětina	k1gFnSc1	pětina
neměla	mít	k5eNaImAgFnS	mít
rakouské	rakouský	k2eAgNnSc4d1	rakouské
občanství	občanství	k1gNnSc4	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Polovinu	polovina	k1gFnSc4	polovina
cizinců	cizinec	k1gMnPc2	cizinec
tvořili	tvořit	k5eAaImAgMnP	tvořit
občané	občan	k1gMnPc1	občan
států	stát	k1gInPc2	stát
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
(	(	kIx(	(
<g/>
120	[number]	k4	120
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
Turecka	Turecko	k1gNnSc2	Turecko
(	(	kIx(	(
<g/>
48	[number]	k4	48
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
26	[number]	k4	26
000	[number]	k4	000
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
25	[number]	k4	25
000	[number]	k4	000
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
21	[number]	k4	21
000	[number]	k4	000
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
12	[number]	k4	12
000	[number]	k4	000
Maďarů	Maďar	k1gMnPc2	Maďar
a	a	k8xC	a
10	[number]	k4	10
000	[number]	k4	000
Rumunů	Rumun	k1gMnPc2	Rumun
<g/>
.	.	kIx.	.
</s>
<s>
Úředně	úředně	k6eAd1	úředně
uznaný	uznaný	k2eAgInSc1d1	uznaný
statut	statut	k1gInSc1	statut
jazykové	jazykový	k2eAgFnSc2d1	jazyková
menšiny	menšina	k1gFnSc2	menšina
mají	mít	k5eAaImIp3nP	mít
mluvčí	mluvčí	k1gMnSc1	mluvčí
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
maďarského	maďarský	k2eAgInSc2d1	maďarský
<g/>
,	,	kIx,	,
slovenského	slovenský	k2eAgInSc2d1	slovenský
a	a	k8xC	a
romského	romský	k2eAgInSc2d1	romský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
již	již	k6eAd1	již
1,69	[number]	k4	1,69
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
porodnosti	porodnost	k1gFnSc2	porodnost
i	i	k8xC	i
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
může	moct	k5eAaImIp3nS	moct
Vídeň	Vídeň	k1gFnSc1	Vídeň
opět	opět	k6eAd1	opět
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
počtu	počet	k1gInSc2	počet
dvou	dva	k4xCgInPc2	dva
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
Vídeň	Vídeň	k1gFnSc1	Vídeň
společně	společně	k6eAd1	společně
s	s	k7c7	s
Bratislavou	Bratislava	k1gFnSc7	Bratislava
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
"	"	kIx"	"
<g/>
Twin	Twin	k1gInSc1	Twin
City	city	k1gNnSc1	city
<g/>
"	"	kIx"	"
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
regionu	region	k1gInSc2	region
CENTROPE	CENTROPE	kA	CENTROPE
a	a	k8xC	a
obě	dva	k4xCgNnPc4	dva
města	město	k1gNnPc4	město
tak	tak	k9	tak
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
společný	společný	k2eAgInSc4d1	společný
region	region	k1gInSc4	region
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
třemi	tři	k4xCgInPc7	tři
miliony	milion	k4xCgInPc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
činnosti	činnost	k1gFnPc4	činnost
projektu	projekt	k1gInSc2	projekt
Twin	Twin	k1gInSc1	Twin
City	City	k1gFnSc2	City
patří	patřit	k5eAaImIp3nS	patřit
společný	společný	k2eAgInSc4d1	společný
rozvoj	rozvoj	k1gInSc4	rozvoj
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
propojení	propojení	k1gNnSc4	propojení
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
obou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
hlubší	hluboký	k2eAgFnSc1d2	hlubší
politická	politický	k2eAgFnSc1d1	politická
spolupráce	spolupráce	k1gFnSc1	spolupráce
a	a	k8xC	a
společný	společný	k2eAgInSc1d1	společný
marketing	marketing	k1gInSc1	marketing
obou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
největší	veliký	k2eAgInSc4d3	veliký
podíl	podíl	k1gInSc4	podíl
Vídeňanů	Vídeňan	k1gMnPc2	Vídeňan
a	a	k8xC	a
Vídeňanek	Vídeňanka	k1gFnPc2	Vídeňanka
hlásil	hlásit	k5eAaImAgInS	hlásit
k	k	k7c3	k
římsko-katolickému	římskoatolický	k2eAgNnSc3d1	římsko-katolické
křesťanství	křesťanství	k1gNnSc3	křesťanství
–	–	k?	–
41	[number]	k4	41
%	%	kIx~	%
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
82	[number]	k4	82
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
katolické	katolický	k2eAgFnSc2d1	katolická
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
třetina	třetina	k1gFnSc1	třetina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
podíl	podíl	k1gInSc1	podíl
12	[number]	k4	12
%	%	kIx~	%
<g/>
,	,	kIx,	,
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
křesťané	křesťan	k1gMnPc1	křesťan
9	[number]	k4	9
%	%	kIx~	%
a	a	k8xC	a
evangelíci	evangelík	k1gMnPc1	evangelík
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
významná	významný	k2eAgFnSc1d1	významná
židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
jen	jen	k9	jen
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
mělo	mít	k5eAaImAgNnS	mít
přistěhovalecký	přistěhovalecký	k2eAgInSc4d1	přistěhovalecký
původ	původ	k1gInSc4	původ
49	[number]	k4	49
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
islámských	islámský	k2eAgFnPc2d1	islámská
mateřských	mateřský	k2eAgFnPc2d1	mateřská
školek	školka	k1gFnPc2	školka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
muslimská	muslimský	k2eAgFnSc1d1	muslimská
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vídeňské	vídeňský	k2eAgInPc4d1	vídeňský
městské	městský	k2eAgInPc4d1	městský
okresy	okres	k1gInPc4	okres
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
23	[number]	k4	23
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
tradičně	tradičně	k6eAd1	tradičně
označovaných	označovaný	k2eAgFnPc2d1	označovaná
jako	jako	k8xS	jako
vídeňské	vídeňský	k2eAgInPc1d1	vídeňský
městské	městský	k2eAgInPc1d1	městský
okresy	okres	k1gInPc1	okres
(	(	kIx(	(
<g/>
Wiener	Wiener	k1gInSc1	Wiener
Stadtbezirke	Stadtbezirk	k1gInSc2	Stadtbezirk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
byly	být	k5eAaImAgFnP	být
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
roku	rok	k1gInSc3	rok
1850	[number]	k4	1850
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
rozšíření	rozšíření	k1gNnSc6	rozšíření
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
nejen	nejen	k6eAd1	nejen
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rozloha	rozloha	k1gFnSc1	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc2	ten
docházelo	docházet	k5eAaImAgNnS	docházet
i	i	k9	i
k	k	k7c3	k
jistým	jistý	k2eAgFnPc3d1	jistá
změnám	změna	k1gFnPc3	změna
hranic	hranice	k1gFnPc2	hranice
těchto	tento	k3xDgInPc2	tento
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgInPc1d1	městský
okresy	okres	k1gInPc1	okres
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nS	členit
celkem	celkem	k6eAd1	celkem
na	na	k7c4	na
89	[number]	k4	89
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
nekryjí	krýt	k5eNaImIp3nP	krýt
s	s	k7c7	s
hranicemi	hranice	k1gFnPc7	hranice
městských	městský	k2eAgMnPc2d1	městský
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
bohatou	bohatý	k2eAgFnSc7d1	bohatá
hudební	hudební	k2eAgFnSc7d1	hudební
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
vrchol	vrchol	k1gInSc1	vrchol
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
na	na	k7c4	na
přelom	přelom	k1gInSc4	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
městě	město	k1gNnSc6	město
tvořili	tvořit	k5eAaImAgMnP	tvořit
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
a	a	k8xC	a
Ludwig	Ludwig	k1gMnSc1	Ludwig
van	vana	k1gFnPc2	vana
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
,	,	kIx,	,
Johannes	Johannes	k1gMnSc1	Johannes
Brahms	Brahms	k1gMnSc1	Brahms
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
Bruckner	Bruckner	k1gMnSc1	Bruckner
a	a	k8xC	a
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
příslušníci	příslušník	k1gMnPc1	příslušník
Nové	Nová	k1gFnSc2	Nová
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
školy	škola	k1gFnSc2	škola
kolem	kolem	k7c2	kolem
Arnolda	Arnold	k1gMnSc2	Arnold
Schönberga	Schönberg	k1gMnSc2	Schönberg
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
interpretaci	interpretace	k1gFnSc4	interpretace
Mozartových	Mozartových	k2eAgNnPc2d1	Mozartových
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
akademií	akademie	k1gFnPc2	akademie
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
udělována	udělován	k2eAgFnSc1d1	udělována
cena	cena	k1gFnSc1	cena
Wiener	Wiener	k1gMnSc1	Wiener
Flötenuhr	Flötenuhr	k1gMnSc1	Flötenuhr
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňští	vídeňský	k2eAgMnPc1d1	vídeňský
filharmonikové	filharmonik	k1gMnPc1	filharmonik
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
symfonické	symfonický	k2eAgInPc4d1	symfonický
orchestry	orchestr	k1gInPc4	orchestr
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
hudebních	hudební	k2eAgNnPc2d1	hudební
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
komorní	komorní	k2eAgInPc1d1	komorní
orchestry	orchestr	k1gInPc1	orchestr
<g/>
,	,	kIx,	,
smyčcové	smyčcový	k2eAgInPc1d1	smyčcový
kvartety	kvartet	k1gInPc1	kvartet
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
Vídeň	Vídeň	k1gFnSc1	Vídeň
významným	významný	k2eAgMnSc7d1	významný
centrem	centr	k1gMnSc7	centr
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jsou	být	k5eAaImIp3nP	být
usazeny	usadit	k5eAaPmNgFnP	usadit
také	také	k9	také
hudební	hudební	k2eAgFnPc1d1	hudební
agentury	agentura	k1gFnPc1	agentura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
angažmá	angažmá	k1gNnSc1	angažmá
pěvcům	pěvec	k1gMnPc3	pěvec
<g/>
,	,	kIx,	,
dirigentům	dirigent	k1gMnPc3	dirigent
a	a	k8xC	a
jiným	jiný	k2eAgMnPc3d1	jiný
hudebníkům	hudebník	k1gMnPc3	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dalo	dát	k5eAaPmAgNnS	dát
jméno	jméno	k1gNnSc4	jméno
tanci	tanec	k1gInPc7	tanec
z	z	k7c2	z
období	období	k1gNnSc2	období
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
města	město	k1gNnSc2	město
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
vídeňskému	vídeňský	k2eAgInSc3d1	vídeňský
valčíku	valčík	k1gInSc3	valčík
<g/>
.	.	kIx.	.
</s>
<s>
Nerozlučně	rozlučně	k6eNd1	rozlučně
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
valčíkem	valčík	k1gInSc7	valčík
spojena	spojen	k2eAgNnPc4d1	spojeno
jména	jméno	k1gNnPc4	jméno
skladatelů	skladatel	k1gMnPc2	skladatel
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
Johann	Johann	k1gMnSc1	Johann
Strauss	Straussa	k1gFnPc2	Straussa
starší	starší	k1gMnSc1	starší
a	a	k8xC	a
Johann	Johann	k1gMnSc1	Johann
Strauss	Strauss	k1gInSc4	Strauss
mladší	mladý	k2eAgInSc4d2	mladší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Josef	Josef	k1gMnSc1	Josef
Lanner	Lanner	k1gMnSc1	Lanner
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gInSc1	Johann
Strauss	Straussa	k1gFnPc2	Straussa
mladší	mladý	k2eAgMnSc1d2	mladší
složil	složit	k5eAaPmAgInS	složit
i	i	k9	i
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
operet	opereta	k1gFnPc2	opereta
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
operetními	operetní	k2eAgMnPc7d1	operetní
skladateli	skladatel	k1gMnPc7	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Franz	Franz	k1gMnSc1	Franz
von	von	k1gInSc4	von
Suppé	Suppý	k2eAgFnPc1d1	Suppý
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Lehár	Lehár	k1gMnSc1	Lehár
a	a	k8xC	a
po	po	k7c4	po
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
rovněž	rovněž	k9	rovněž
Emmerich	Emmerich	k1gInSc4	Emmerich
Kálmán	Kálmán	k2eAgInSc4d1	Kálmán
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Burgtheater	Burgtheatra	k1gFnPc2	Burgtheatra
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgNnPc4d1	přední
německojazyčná	německojazyčný	k2eAgNnPc4d1	německojazyčné
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc4d1	dobrá
pověst	pověst	k1gFnSc4	pověst
si	se	k3xPyFc3	se
drží	držet	k5eAaImIp3nS	držet
i	i	k9	i
Volkstheater	Volkstheater	k1gMnSc1	Volkstheater
a	a	k8xC	a
Theater	Theater	k1gMnSc1	Theater
in	in	k?	in
der	drát	k5eAaImRp2nS	drát
Josephstadt	Josephstadt	k1gInSc4	Josephstadt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
malých	malý	k2eAgFnPc2d1	malá
scén	scéna	k1gFnPc2	scéna
a	a	k8xC	a
kabaretů	kabaret	k1gInPc2	kabaret
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
předávána	předáván	k2eAgFnSc1d1	předávána
Nestroyova	Nestroyův	k2eAgFnSc1d1	Nestroyův
divadelní	divadelní	k2eAgFnSc1d1	divadelní
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgInPc1d3	nejvýznamnější
ocenění	ocenění	k1gNnSc4	ocenění
v	v	k7c6	v
německojazyčné	německojazyčný	k2eAgFnSc6d1	německojazyčná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
státní	státní	k2eAgFnSc1d1	státní
opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
uvádění	uvádění	k1gNnSc4	uvádění
klasických	klasický	k2eAgNnPc2d1	klasické
operních	operní	k2eAgNnPc2d1	operní
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
světových	světový	k2eAgFnPc2d1	světová
operních	operní	k2eAgFnPc2d1	operní
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Volksoper	Volksoper	k1gInSc1	Volksoper
nabízí	nabízet	k5eAaImIp3nS	nabízet
širší	široký	k2eAgInSc1d2	širší
repertoár	repertoár	k1gInSc1	repertoár
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
jak	jak	k8xS	jak
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
tak	tak	k9	tak
operety	opereta	k1gFnPc1	opereta
a	a	k8xC	a
muzikály	muzikál	k1gInPc1	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
třetí	třetí	k4xOgFnSc1	třetí
operní	operní	k2eAgFnSc1d1	operní
scéna	scéna	k1gFnSc1	scéna
slouží	sloužit	k5eAaImIp3nS	sloužit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Theater	Theater	k1gMnSc1	Theater
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Wien	Wieno	k1gNnPc2	Wieno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
premiéře	premiéra	k1gFnSc6	premiéra
uvedena	uveden	k2eAgFnSc1d1	uvedena
Beethovenova	Beethovenův	k2eAgFnSc1d1	Beethovenova
opera	opera	k1gFnSc1	opera
Fidelio	Fidelio	k6eAd1	Fidelio
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
komorní	komorní	k2eAgFnSc1d1	komorní
opera	opera	k1gFnSc1	opera
nabízí	nabízet	k5eAaImIp3nS	nabízet
netradiční	tradiční	k2eNgFnSc2d1	netradiční
inscenace	inscenace	k1gFnSc2	inscenace
klasických	klasický	k2eAgNnPc2d1	klasické
i	i	k8xC	i
moderních	moderní	k2eAgNnPc2d1	moderní
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
muzejním	muzejní	k2eAgInSc7d1	muzejní
komplexem	komplex	k1gInSc7	komplex
je	být	k5eAaImIp3nS	být
císařský	císařský	k2eAgInSc1d1	císařský
palác	palác	k1gInSc1	palác
Hofburg	Hofburg	k1gInSc1	Hofburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
interiérových	interiérový	k2eAgFnPc2d1	interiérová
instalací	instalace	k1gFnPc2	instalace
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vystaveny	vystavit	k5eAaPmNgInP	vystavit
některé	některý	k3yIgInPc1	některý
exponáty	exponát	k1gInPc1	exponát
z	z	k7c2	z
panovnických	panovnický	k2eAgFnPc2d1	panovnická
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
však	však	k9	však
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nedalekých	daleký	k2eNgFnPc6d1	nedaleká
monumentálních	monumentální	k2eAgFnPc6d1	monumentální
muzejních	muzejní	k2eAgFnPc6d1	muzejní
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
ležících	ležící	k2eAgFnPc6d1	ležící
naproti	naproti	k7c3	naproti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
Uměleckohistorickém	uměleckohistorický	k2eAgNnSc6d1	Uměleckohistorické
muzeu	muzeum	k1gNnSc6	muzeum
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
galerií	galerie	k1gFnSc7	galerie
starých	starý	k2eAgMnPc2d1	starý
mistrů	mistr	k1gMnPc2	mistr
a	a	k8xC	a
v	v	k7c6	v
Přírodovědném	přírodovědný	k2eAgNnSc6d1	Přírodovědné
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Muzejní	muzejní	k2eAgInSc1d1	muzejní
areál	areál	k1gInSc1	areál
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
doplněn	doplnit	k5eAaPmNgInS	doplnit
souborem	soubor	k1gInSc7	soubor
zvaným	zvaný	k2eAgInSc7d1	zvaný
MuseumsQuartier	MuseumsQuartier	k1gInSc4	MuseumsQuartier
<g/>
,	,	kIx,	,
vytvořeným	vytvořený	k2eAgNnSc7d1	vytvořené
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
bývalých	bývalý	k2eAgFnPc2d1	bývalá
císařských	císařský	k2eAgFnPc2d1	císařská
koníren	konírna	k1gFnPc2	konírna
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
hlavní	hlavní	k2eAgFnSc2d1	hlavní
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
Kunsthalle	Kunsthalle	k1gFnSc2	Kunsthalle
plní	plnit	k5eAaImIp3nS	plnit
Albertina	Albertin	k2eAgFnSc1d1	Albertina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stavebně	stavebně	k6eAd1	stavebně
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
komplex	komplex	k1gInSc4	komplex
Hofburgu	Hofburg	k1gInSc2	Hofburg
<g/>
,	,	kIx,	,
výstavními	výstavní	k2eAgInPc7d1	výstavní
prostory	prostor	k1gInPc7	prostor
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
Pavilon	pavilon	k1gInSc4	pavilon
Secese	secese	k1gFnSc1	secese
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
mnohé	mnohé	k1gNnSc4	mnohé
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
muzea	muzeum	k1gNnPc1	muzeum
slouží	sloužit	k5eAaImIp3nP	sloužit
také	také	k9	také
hlavní	hlavní	k2eAgInPc4d1	hlavní
zahradní	zahradní	k2eAgInPc4d1	zahradní
paláce	palác	k1gInPc4	palác
-	-	kIx~	-
císařská	císařský	k2eAgFnSc1d1	císařská
rezidence	rezidence	k1gFnSc1	rezidence
v	v	k7c6	v
Schönbrunnu	Schönbrunno	k1gNnSc6	Schönbrunno
<g/>
,	,	kIx,	,
Belveder	Belveder	k1gMnSc1	Belveder
Evžena	Evžen	k1gMnSc2	Evžen
Savojského	savojský	k2eAgInSc2d1	savojský
i	i	k9	i
Lichtenštejnský	lichtenštejnský	k2eAgInSc1d1	lichtenštejnský
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Roßau	Roßaus	k1gInSc6	Roßaus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
desítky	desítka	k1gFnPc4	desítka
vídeňských	vídeňský	k2eAgNnPc2d1	Vídeňské
muzeí	muzeum	k1gNnPc2	muzeum
patří	patřit	k5eAaImIp3nS	patřit
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
historické	historický	k2eAgNnSc1d1	historické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Arzenálu	arzenál	k1gInSc2	arzenál
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
medicínských	medicínský	k2eAgNnPc2d1	medicínské
muzeí	muzeum	k1gNnPc2	muzeum
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Staré	Staré	k2eAgFnSc2d1	Staré
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
nemocnice	nemocnice	k1gFnSc2	nemocnice
nebo	nebo	k8xC	nebo
Muzeum	muzeum	k1gNnSc1	muzeum
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Památky	památka	k1gFnPc1	památka
z	z	k7c2	z
římského	římský	k2eAgNnSc2d1	římské
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
v	v	k7c6	v
pouhých	pouhý	k2eAgInPc6d1	pouhý
fragmentech	fragment	k1gInPc6	fragment
<g/>
.	.	kIx.	.
</s>
<s>
Architektonické	architektonický	k2eAgFnPc1d1	architektonická
stopy	stopa	k1gFnPc1	stopa
středověku	středověk	k1gInSc2	středověk
představují	představovat	k5eAaImIp3nP	představovat
především	především	k9	především
chrámové	chrámový	k2eAgFnPc1d1	chrámová
stavby	stavba	k1gFnPc1	stavba
od	od	k7c2	od
románského	románský	k2eAgInSc2d1	románský
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Ruprechta	Ruprecht	k1gInSc2	Ruprecht
až	až	k9	až
po	po	k7c4	po
gotický	gotický	k2eAgInSc4d1	gotický
svatoštěpánský	svatoštěpánský	k2eAgInSc4d1	svatoštěpánský
dóm	dóm	k1gInSc4	dóm
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
sochařskou	sochařský	k2eAgFnSc7d1	sochařská
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Baroko	baroko	k1gNnSc1	baroko
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
množství	množství	k1gNnSc4	množství
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
paláců	palác	k1gInPc2	palác
i	i	k9	i
známé	známý	k2eAgFnPc1d1	známá
kostelní	kostelní	k2eAgFnPc1d1	kostelní
stavby	stavba	k1gFnPc1	stavba
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Karel	Karel	k1gMnSc1	Karel
Boromejský	Boromejský	k2eAgMnSc1d1	Boromejský
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
kostel	kostel	k1gInSc1	kostel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
urbanistický	urbanistický	k2eAgInSc4d1	urbanistický
význam	význam	k1gInSc4	význam
mělo	mít	k5eAaImAgNnS	mít
vytvoření	vytvoření	k1gNnSc1	vytvoření
reprezentativní	reprezentativní	k2eAgFnSc2d1	reprezentativní
okružní	okružní	k2eAgFnSc2d1	okružní
třídy	třída	k1gFnSc2	třída
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
-	-	kIx~	-
Ringstraße	Ringstraße	k1gFnSc1	Ringstraße
-	-	kIx~	-
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
postavena	postaven	k2eAgFnSc1d1	postavena
řada	řada	k1gFnSc1	řada
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
severu	sever	k1gInSc6	sever
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
turistické	turistický	k2eAgFnPc4d1	turistická
atrakce	atrakce	k1gFnPc4	atrakce
patří	patřit	k5eAaImIp3nS	patřit
Hundertwasserhaus	Hundertwasserhaus	k1gInSc1	Hundertwasserhaus
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
okrese	okres	k1gInSc6	okres
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
založena	založen	k2eAgFnSc1d1	založena
nejstarší	starý	k2eAgFnSc1d3	nejstarší
německá	německý	k2eAgFnSc1d1	německá
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
prestižní	prestižní	k2eAgFnPc4d1	prestižní
evropské	evropský	k2eAgFnPc4d1	Evropská
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
knihovna	knihovna	k1gFnSc1	knihovna
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
knihovnou	knihovna	k1gFnSc7	knihovna
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Trojici	trojice	k1gFnSc4	trojice
zdejších	zdejší	k2eAgFnPc2d1	zdejší
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
knihoven	knihovna	k1gFnPc2	knihovna
dále	daleko	k6eAd2	daleko
tvoří	tvořit	k5eAaImIp3nS	tvořit
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
rodištěm	rodiště	k1gNnSc7	rodiště
a	a	k8xC	a
působištěm	působiště	k1gNnSc7	působiště
mnoha	mnoho	k4c2	mnoho
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
kapacit	kapacita	k1gFnPc2	kapacita
–	–	k?	–
filosofů	filosof	k1gMnPc2	filosof
Karla	Karel	k1gMnSc2	Karel
Poppera	Popper	k1gMnSc2	Popper
<g/>
,	,	kIx,	,
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Wittgensteina	Wittgensteino	k1gNnSc2	Wittgensteino
<g/>
,	,	kIx,	,
ekonomů	ekonom	k1gMnPc2	ekonom
tzv.	tzv.	kA	tzv.
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
Eugen	Eugen	k2eAgInSc1d1	Eugen
von	von	k1gInSc1	von
Böhm-Bawerk	Böhm-Bawerk	k1gInSc1	Böhm-Bawerk
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gInSc1	Ludwig
von	von	k1gInSc1	von
Mises	Mises	k1gInSc1	Mises
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
August	August	k1gMnSc1	August
von	von	k1gInSc4	von
Hayek	Hayek	k1gInSc1	Hayek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psychologů	psycholog	k1gMnPc2	psycholog
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
,	,	kIx,	,
Alfreda	Alfred	k1gMnSc2	Alfred
Adlera	Adler	k1gMnSc2	Adler
nebo	nebo	k8xC	nebo
Viktora	Viktor	k1gMnSc2	Viktor
Frankla	Frankla	k1gMnSc2	Frankla
<g/>
,	,	kIx,	,
fyziků	fyzik	k1gMnPc2	fyzik
Erwina	Erwin	k1gMnSc2	Erwin
Schrödingera	Schrödinger	k1gMnSc2	Schrödinger
<g/>
,	,	kIx,	,
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Boltzmanna	Boltzmann	k1gMnSc2	Boltzmann
či	či	k8xC	či
Ernsta	Ernst	k1gMnSc2	Ernst
Macha	Mach	k1gMnSc2	Mach
nebo	nebo	k8xC	nebo
inženýra	inženýr	k1gMnSc2	inženýr
Viktora	Viktor	k1gMnSc2	Viktor
Kaplana	Kaplan	k1gMnSc2	Kaplan
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
bydlel	bydlet	k5eAaImAgMnS	bydlet
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
Tomáš	Tomáš	k1gMnSc1	Tomáš
Garrigue	Garrigue	k1gFnPc2	Garrigue
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
strávili	strávit	k5eAaPmAgMnP	strávit
spisovatelé	spisovatel	k1gMnPc1	spisovatel
a	a	k8xC	a
spisovatelky	spisovatelka	k1gFnPc1	spisovatelka
Ingeborg	Ingeborg	k1gInSc1	Ingeborg
Bachmannová	Bachmannová	k1gFnSc1	Bachmannová
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Bernhard	Bernhard	k1gMnSc1	Bernhard
<g/>
,	,	kIx,	,
Elias	Elias	k1gMnSc1	Elias
Canetti	Canetť	k1gFnSc2	Canetť
<g/>
,	,	kIx,	,
Elfriede	Elfried	k1gMnSc5	Elfried
Jelineková	Jelineková	k1gFnSc5	Jelineková
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Musil	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Heimito	Heimit	k2eAgNnSc1d1	Heimito
von	von	k1gInSc4	von
Doderer	Doderer	k1gMnSc1	Doderer
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Grillparzer	Grillparzer	k1gMnSc1	Grillparzer
nebo	nebo	k8xC	nebo
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweig	k1gMnSc1	Zweig
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
svou	svůj	k3xOyFgFnSc7	svůj
specifickou	specifický	k2eAgFnSc7d1	specifická
kuchyní	kuchyně	k1gFnSc7	kuchyně
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
mnohonárodnostní	mnohonárodnostní	k2eAgFnSc2d1	mnohonárodnostní
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
zdejší	zdejší	k2eAgInSc4d1	zdejší
jídelníček	jídelníček	k1gInSc4	jídelníček
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
upravené	upravený	k2eAgFnSc6d1	upravená
podobě	podoba	k1gFnSc6	podoba
přejaty	přejat	k2eAgFnPc1d1	přejata
guláš	guláš	k1gInSc4	guláš
a	a	k8xC	a
palačinky	palačinka	k1gFnPc4	palačinka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
kromě	kromě	k7c2	kromě
italské	italský	k2eAgFnSc2d1	italská
kuchyně	kuchyně	k1gFnSc2	kuchyně
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
koláče	koláč	k1gInPc4	koláč
<g/>
,	,	kIx,	,
buchty	buchta	k1gFnPc4	buchta
a	a	k8xC	a
knedlíky	knedlík	k1gInPc4	knedlík
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
typické	typický	k2eAgInPc4d1	typický
pokrmy	pokrm	k1gInPc4	pokrm
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
vídeňský	vídeňský	k2eAgInSc4d1	vídeňský
řízek	řízek	k1gInSc4	řízek
<g/>
.	.	kIx.	.
</s>
<s>
Stánky	stánka	k1gFnPc1	stánka
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
nabízejí	nabízet	k5eAaImIp3nP	nabízet
velký	velký	k2eAgInSc4d1	velký
výběr	výběr	k1gInSc4	výběr
párků	párek	k1gInPc2	párek
a	a	k8xC	a
klobás	klobása	k1gFnPc2	klobása
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
obohatili	obohatit	k5eAaPmAgMnP	obohatit
Vídeň	Vídeň	k1gFnSc4	Vídeň
mj.	mj.	kA	mj.
Turci	Turek	k1gMnPc1	Turek
a	a	k8xC	a
Bosňáci	Bosňáci	k?	Bosňáci
rychlým	rychlý	k2eAgNnSc7d1	rychlé
občerstvením	občerstvení	k1gNnSc7	občerstvení
tureckého	turecký	k2eAgMnSc2d1	turecký
a	a	k8xC	a
arabského	arabský	k2eAgInSc2d1	arabský
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
kebab	kebab	k1gInSc1	kebab
<g/>
,	,	kIx,	,
burek	burka	k1gFnPc2	burka
<g/>
,	,	kIx,	,
falafel	falafela	k1gFnPc2	falafela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
vídeňských	vídeňský	k2eAgFnPc2d1	Vídeňská
atrakcí	atrakce	k1gFnPc2	atrakce
je	být	k5eAaImIp3nS	být
světoznámý	světoznámý	k2eAgInSc1d1	světoznámý
zábavní	zábavní	k2eAgInSc1d1	zábavní
park	park	k1gInSc1	park
Prátr	Prátr	k1gInSc1	Prátr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1766	[number]	k4	1766
<g/>
,	,	kIx,	,
když	když	k8xS	když
císař	císař	k1gMnSc1	císař
Josef	Josef	k1gMnSc1	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
otevřel	otevřít	k5eAaPmAgInS	otevřít
bývalý	bývalý	k2eAgInSc1d1	bývalý
lovecký	lovecký	k2eAgInSc1d1	lovecký
revír	revír	k1gInSc1	revír
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
před	před	k7c7	před
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročím	výročí	k1gNnSc7	výročí
nástupu	nástup	k1gInSc2	nástup
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
obří	obří	k2eAgNnSc1d1	obří
vyhlídkové	vyhlídkový	k2eAgNnSc1d1	vyhlídkové
kolo	kolo	k1gNnSc1	kolo
Riesenrad	Riesenrada	k1gFnPc2	Riesenrada
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
oslav	oslava	k1gFnPc2	oslava
bylo	být	k5eAaImAgNnS	být
hercům	herec	k1gMnPc3	herec
<g/>
,	,	kIx,	,
kejklířům	kejklíř	k1gMnPc3	kejklíř
a	a	k8xC	a
komediantům	komediant	k1gMnPc3	komediant
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
postavit	postavit	k5eAaPmF	postavit
si	se	k3xPyFc3	se
v	v	k7c6	v
parku	park	k1gInSc6	park
své	svůj	k3xOyFgInPc4	svůj
stany	stan	k1gInPc4	stan
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
občerstvení	občerstvení	k1gNnSc4	občerstvení
návštěvníků	návštěvník	k1gMnPc2	návštěvník
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
stánků	stánek	k1gInPc2	stánek
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
provozovatelé	provozovatel	k1gMnPc1	provozovatel
atrakcí	atrakce	k1gFnPc2	atrakce
v	v	k7c6	v
parku	park	k1gInSc6	park
zůstali	zůstat	k5eAaPmAgMnP	zůstat
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
tak	tak	k6eAd1	tak
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgInSc4d1	dnešní
Prátr	Prátr	k1gInSc4	Prátr
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgMnSc1d1	typický
pro	pro	k7c4	pro
Vídeň	Vídeň	k1gFnSc4	Vídeň
je	být	k5eAaImIp3nS	být
také	také	k9	také
výskyt	výskyt	k1gInSc1	výskyt
tržnic	tržnice	k1gFnPc2	tržnice
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
i	i	k8xC	i
na	na	k7c6	na
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
zejména	zejména	k9	zejména
na	na	k7c4	na
prodej	prodej	k1gInSc4	prodej
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
bleší	bleší	k2eAgInPc4d1	bleší
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
na	na	k7c4	na
turisty	turist	k1gMnPc4	turist
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
trh	trh	k1gInSc1	trh
Naschmarkt	Naschmarkt	k1gInSc1	Naschmarkt
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
obchodní	obchodní	k2eAgFnSc7d1	obchodní
třídou	třída	k1gFnSc7	třída
je	být	k5eAaImIp3nS	být
Mariahilfer	Mariahilfer	k1gInSc1	Mariahilfer
Straße	Straß	k1gFnSc2	Straß
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgInSc1d1	kulturní
podnik	podnik	k1gInSc1	podnik
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
představují	představovat	k5eAaImIp3nP	představovat
i	i	k9	i
vídeňské	vídeňský	k2eAgFnPc1d1	Vídeňská
kavárny	kavárna	k1gFnPc1	kavárna
<g/>
,	,	kIx,	,
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dějiště	dějiště	k1gNnSc2	dějiště
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nabídky	nabídka	k1gFnSc2	nabídka
novin	novina	k1gFnPc2	novina
bývá	bývat	k5eAaImIp3nS	bývat
ke	k	k7c3	k
kávě	káva	k1gFnSc3	káva
podáván	podáván	k2eAgInSc1d1	podáván
i	i	k8xC	i
čokoládový	čokoládový	k2eAgInSc1d1	čokoládový
dort	dort	k1gInSc1	dort
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
podle	podle	k7c2	podle
svého	svůj	k3xOyFgMnSc4	svůj
tvůrce	tvůrce	k1gMnSc4	tvůrce
Sacher	Sachra	k1gFnPc2	Sachra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čtvrtích	čtvrt	k1gFnPc6	čtvrt
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
západních	západní	k2eAgInPc2d1	západní
vídeňských	vídeňský	k2eAgInPc2d1	vídeňský
okresů	okres	k1gInPc2	okres
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
dodnes	dodnes	k6eAd1	dodnes
významnou	významný	k2eAgFnSc7d1	významná
vinařskou	vinařský	k2eAgFnSc7d1	vinařská
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
značné	značný	k2eAgFnSc3d1	značná
oblibě	obliba	k1gFnSc3	obliba
malé	malý	k2eAgInPc1d1	malý
vinné	vinný	k2eAgInPc1d1	vinný
lokály	lokál	k1gInPc1	lokál
zvané	zvaný	k2eAgFnSc2d1	zvaná
Heurige	Heurig	k1gFnSc2	Heurig
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
časté	častý	k2eAgNnSc1d1	časté
sněžení	sněžení	k1gNnSc1	sněžení
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
i	i	k9	i
mrazy	mráz	k1gInPc1	mráz
pod	pod	k7c7	pod
-10	-10	k4	-10
°	°	k?	°
<g/>
C.	C.	kA	C.
Jaro	jaro	k1gNnSc1	jaro
začíná	začínat	k5eAaImIp3nS	začínat
koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
uprostřed	uprostřed	k7c2	uprostřed
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mrazy	mráz	k1gInPc1	mráz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
letní	letní	k2eAgNnPc4d1	letní
horka	horko	k1gNnPc4	horko
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
i	i	k9	i
velmi	velmi	k6eAd1	velmi
teplé	teplý	k2eAgFnPc1d1	teplá
a	a	k8xC	a
suché	suchý	k2eAgFnPc1d1	suchá
<g/>
.	.	kIx.	.
</s>
<s>
Podzim	podzim	k1gInSc1	podzim
přichází	přicházet	k5eAaImIp3nS	přicházet
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
i	i	k8xC	i
střed	střed	k1gInSc1	střed
podzimu	podzim	k1gInSc2	podzim
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
častými	častý	k2eAgInPc7d1	častý
návraty	návrat	k1gInPc7	návrat
teplého	teplé	k1gNnSc2	teplé
a	a	k8xC	a
suchého	suchý	k2eAgNnSc2d1	suché
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Koncový	koncový	k2eAgInSc1d1	koncový
podzim	podzim	k1gInSc1	podzim
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
mírnou	mírný	k2eAgFnSc4d1	mírná
zimu	zima	k1gFnSc4	zima
koncem	koncem	k7c2	koncem
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
slunečné	slunečný	k2eAgNnSc4d1	slunečné
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
ročních	roční	k2eAgNnPc6d1	roční
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
naměřená	naměřený	k2eAgFnSc1d1	naměřená
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
byla	být	k5eAaImAgFnS	být
-26	-26	k4	-26
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
byla	být	k5eAaImAgFnS	být
38,3	[number]	k4	38,3
°	°	k?	°
<g/>
C.	C.	kA	C.
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
skoro	skoro	k6eAd1	skoro
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
každý	každý	k3xTgInSc1	každý
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
zástupce	zástupce	k1gMnPc4	zástupce
patří	patřit	k5eAaImIp3nS	patřit
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
SK	Sk	kA	Sk
Rapid	rapid	k1gInSc1	rapid
Wien	Wien	k1gInSc1	Wien
a	a	k8xC	a
FK	FK	kA	FK
Austria	Austrium	k1gNnPc1	Austrium
Wien	Wien	k1gNnSc1	Wien
<g/>
.	.	kIx.	.
</s>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jím	on	k3xPp3gNnSc7	on
Vienna	Vienna	k1gFnSc1	Vienna
Capitals	Capitalsa	k1gFnPc2	Capitalsa
<g/>
.	.	kIx.	.
</s>
<s>
Volejbal	volejbal	k1gInSc1	volejbal
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
hrají	hrát	k5eAaImIp3nP	hrát
Aon	Aon	k1gMnPc1	Aon
hotVolleys	hotVolleys	k1gInSc4	hotVolleys
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Metro	metro	k1gNnSc1	metro
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
S-Bahn	S-Bahn	k1gInSc4	S-Bahn
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
se	s	k7c7	s
35	[number]	k4	35
%	%	kIx~	%
přepravy	přeprava	k1gFnSc2	přeprava
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
32	[number]	k4	32
%	%	kIx~	%
automobilem	automobil	k1gInSc7	automobil
<g/>
,	,	kIx,	,
28	[number]	k4	28
%	%	kIx~	%
pěšky	pěšky	k6eAd1	pěšky
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Páteří	páteř	k1gFnSc7	páteř
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
U-Bahn	U-Bahn	k1gInSc1	U-Bahn
<g/>
)	)	kIx)	)
a	a	k8xC	a
městská	městský	k2eAgFnSc1d1	městská
železnice	železnice	k1gFnSc1	železnice
(	(	kIx(	(
<g/>
S-Bahn	S-Bahn	k1gInSc1	S-Bahn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
počátky	počátek	k1gInPc1	počátek
spadají	spadat	k5eAaImIp3nP	spadat
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
základní	základní	k2eAgFnSc1d1	základní
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
tramvajemi	tramvaj	k1gFnPc7	tramvaj
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
(	(	kIx(	(
<g/>
od	od	k7c2	od
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rakouském	rakouský	k2eAgNnSc6d1	rakouské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
také	také	k9	také
několik	několik	k4yIc1	několik
samostatných	samostatný	k2eAgFnPc2d1	samostatná
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
prostředky	prostředek	k1gInPc1	prostředek
hromadné	hromadný	k2eAgFnSc2d1	hromadná
přepravy	přeprava	k1gFnSc2	přeprava
používají	používat	k5eAaImIp3nP	používat
jednotný	jednotný	k2eAgInSc4d1	jednotný
tarif	tarif	k1gInSc4	tarif
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
provozovány	provozován	k2eAgFnPc1d1	provozována
převážně	převážně	k6eAd1	převážně
firmami	firma	k1gFnPc7	firma
Wiener	Wienero	k1gNnPc2	Wienero
Linien	Linien	k2eAgMnSc1d1	Linien
<g/>
,	,	kIx,	,
ÖBB	ÖBB	kA	ÖBB
<g/>
,	,	kIx,	,
Badner	Badner	k1gMnSc1	Badner
Bahn	Bahn	k1gMnSc1	Bahn
a	a	k8xC	a
také	také	k9	také
řadou	řada	k1gFnSc7	řada
soukromých	soukromý	k2eAgMnPc2d1	soukromý
autobusových	autobusový	k2eAgMnPc2d1	autobusový
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Dálkovou	dálkový	k2eAgFnSc4d1	dálková
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
převážně	převážně	k6eAd1	převážně
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
se	se	k3xPyFc4	se
paprskovitě	paprskovitě	k6eAd1	paprskovitě
sbíhaly	sbíhat	k5eAaImAgFnP	sbíhat
tratě	trať	k1gFnPc1	trať
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
existence	existence	k1gFnSc2	existence
několika	několik	k4yIc2	několik
hlavových	hlavový	k2eAgNnPc2d1	hlavové
nádraží	nádraží	k1gNnPc2	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2009	[number]	k4	2009
a	a	k8xC	a
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zbořeno	zbořen	k2eAgNnSc1d1	zbořeno
největší	veliký	k2eAgNnSc1d3	veliký
hlavové	hlavový	k2eAgNnSc1d1	hlavové
nádraží	nádraží	k1gNnSc1	nádraží
Südbahnhof	Südbahnhof	k1gMnSc1	Südbahnhof
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
místech	místo	k1gNnPc6	místo
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
nové	nový	k2eAgNnSc4d1	nové
nádraží	nádraží	k1gNnSc4	nádraží
Wien	Wien	k1gMnSc1	Wien
Hauptbahnhof	Hauptbahnhof	k1gMnSc1	Hauptbahnhof
(	(	kIx(	(
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
sem	sem	k6eAd1	sem
zajíždějí	zajíždět	k5eAaImIp3nP	zajíždět
veškeré	veškerý	k3xTgInPc4	veškerý
dálkové	dálkový	k2eAgInPc4d1	dálkový
vlaky	vlak	k1gInPc4	vlak
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
ÖBB	ÖBB	kA	ÖBB
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
významné	významný	k2eAgNnSc1d1	významné
nádraží	nádraží	k1gNnSc1	nádraží
Westbahnhof	Westbahnhof	k1gInSc1	Westbahnhof
slouží	sloužit	k5eAaImIp3nS	sloužit
jen	jen	k9	jen
pro	pro	k7c4	pro
regionální	regionální	k2eAgFnSc4d1	regionální
dopravu	doprava	k1gFnSc4	doprava
ÖBB	ÖBB	kA	ÖBB
a	a	k8xC	a
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
soukromého	soukromý	k2eAgMnSc4d1	soukromý
přepravce	přepravce	k1gMnSc4	přepravce
<g/>
.	.	kIx.	.
</s>
<s>
Automobilová	automobilový	k2eAgFnSc1d1	automobilová
doprava	doprava	k1gFnSc1	doprava
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
okruhy	okruh	k1gInPc4	okruh
Ringstraße	Ringstraße	k1gFnPc2	Ringstraße
a	a	k8xC	a
Gürtel	Gürtela	k1gFnPc2	Gürtela
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dálkovou	dálkový	k2eAgFnSc4d1	dálková
přepravu	přeprava	k1gFnSc4	přeprava
slouží	sloužit	k5eAaImIp3nS	sloužit
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
budovaný	budovaný	k2eAgInSc4d1	budovaný
obchvat	obchvat	k1gInSc4	obchvat
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
Dunaje	Dunaj	k1gInSc2	Dunaj
překonává	překonávat	k5eAaImIp3nS	překonávat
celkem	celkem	k6eAd1	celkem
35	[number]	k4	35
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
individuální	individuální	k2eAgFnSc2d1	individuální
automobilové	automobilový	k2eAgFnSc2d1	automobilová
přepravy	přeprava	k1gFnSc2	přeprava
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
správou	správa	k1gFnSc7	správa
cíleně	cíleně	k6eAd1	cíleně
omezován	omezovat	k5eAaImNgMnS	omezovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
hromadných	hromadný	k2eAgInPc2d1	hromadný
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
budována	budován	k2eAgFnSc1d1	budována
síť	síť	k1gFnSc1	síť
půjčoven	půjčovna	k1gFnPc2	půjčovna
jízdních	jízdní	k2eAgFnPc2d1	jízdní
kol	kolo	k1gNnPc2	kolo
Citybike	Citybik	k1gFnSc2	Citybik
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
husté	hustý	k2eAgFnSc2d1	hustá
sítě	síť	k1gFnSc2	síť
vyhrazených	vyhrazený	k2eAgFnPc2d1	vyhrazená
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
tras	trasa	k1gFnPc2	trasa
a	a	k8xC	a
zklidňováním	zklidňování	k1gNnSc7	zklidňování
dopravy	doprava	k1gFnSc2	doprava
díky	díky	k7c3	díky
zavádění	zavádění	k1gNnSc3	zavádění
jednosměrného	jednosměrný	k2eAgInSc2d1	jednosměrný
provozu	provoz	k1gInSc2	provoz
postupně	postupně	k6eAd1	postupně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
podíl	podíl	k1gInSc1	podíl
cyklistiky	cyklistika	k1gFnSc2	cyklistika
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
dopravě	doprava	k1gFnSc6	doprava
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Vídně	Vídeň	k1gFnSc2	Vídeň
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Wien-Schwechat	Wien-Schwechat	k1gFnSc1	Wien-Schwechat
<g/>
,	,	kIx,	,
domovská	domovský	k2eAgFnSc1d1	domovská
stanice	stanice	k1gFnSc1	stanice
Rakouských	rakouský	k2eAgFnPc2d1	rakouská
aerolinií	aerolinie	k1gFnPc2	aerolinie
<g/>
.	.	kIx.	.
</s>
<s>
Nízkorozpočtové	nízkorozpočtový	k2eAgFnPc1d1	nízkorozpočtová
linky	linka	k1gFnPc1	linka
odlétají	odlétat	k5eAaImIp3nP	odlétat
z	z	k7c2	z
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Bratislavou	Bratislava	k1gFnSc7	Bratislava
a	a	k8xC	a
Budapeští	Budapešť	k1gFnSc7	Budapešť
je	být	k5eAaImIp3nS	být
Vídeň	Vídeň	k1gFnSc1	Vídeň
propojena	propojit	k5eAaPmNgFnS	propojit
také	také	k9	také
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
je	být	k5eAaImIp3nS	být
sídlo	sídlo	k1gNnSc1	sídlo
mnoha	mnoho	k4c2	mnoho
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
úřadovna	úřadovna	k1gFnSc1	úřadovna
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
následující	následující	k2eAgFnSc1d1	následující
organizace	organizace	k1gFnSc1	organizace
<g/>
:	:	kIx,	:
FRA	FRA	kA	FRA
-	-	kIx~	-
Agentura	agentura	k1gFnSc1	agentura
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
pro	pro	k7c4	pro
základní	základní	k2eAgNnPc4d1	základní
práva	právo	k1gNnPc4	právo
IAEO	IAEO	kA	IAEO
-	-	kIx~	-
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
OBSE	OBSE	kA	OBSE
-	-	kIx~	-
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
OPEC	opéct	k5eAaPmRp2nSwK	opéct
–	–	k?	–
Organizace	organizace	k1gFnSc2	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgFnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
UNDCP	UNDCP	kA	UNDCP
-	-	kIx~	-
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
program	program	k1gInSc4	program
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
drog	droga	k1gFnPc2	droga
<g />
.	.	kIx.	.
</s>
<s>
UNHCR	UNHCR	kA	UNHCR
-	-	kIx~	-
Úřad	úřad	k1gInSc1	úřad
Vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
UNOOSA	UNOOSA	kA	UNOOSA
-	-	kIx~	-
Úřad	úřad	k1gInSc1	úřad
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
vesmírné	vesmírný	k2eAgFnPc4d1	vesmírná
záležitosti	záležitost	k1gFnPc4	záležitost
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc4	Srbsko
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Istanbul	Istanbul	k1gInSc1	Istanbul
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
Lublaň	Lublaň	k1gFnSc1	Lublaň
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Tabríz	tabríz	k1gInSc1	tabríz
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Tunis	Tunis	k1gInSc1	Tunis
<g/>
,	,	kIx,	,
Tunisko	Tunisko	k1gNnSc1	Tunisko
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
</s>
