<s>
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pechlák	Pechlák	k1gMnSc1	Pechlák
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1978	[number]	k4	1978
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
DJ	DJ	kA	DJ
česko-slovenské	českolovenský	k2eAgFnSc2d1	česko-slovenská
hip-hopové	hipopový	k2eAgFnSc2d1	hip-hopová
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
DJingu	DJing	k1gInSc3	DJing
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Indym	Indym	k1gInSc1	Indym
a	a	k8xC	a
LA	la	k1gNnSc1	la
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
začínali	začínat	k5eAaImAgMnP	začínat
všichni	všechen	k3xTgMnPc1	všechen
na	na	k7c4	na
pražské	pražský	k2eAgNnSc4d1	Pražské
graffiti	graffiti	k1gNnSc4	graffiti
scéně	scéna	k1gFnSc3	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
skupina	skupina	k1gFnSc1	skupina
Indy	Indus	k1gInPc1	Indus
&	&	k?	&
Wich	Wich	k1gInSc1	Wich
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Indy	Indus	k1gInPc4	Indus
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
rapper	rapper	k1gMnSc1	rapper
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc4	sám
jako	jako	k9	jako
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
rapperem	rapper	k1gInSc7	rapper
LA	la	k1gNnSc1	la
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
objevoval	objevovat	k5eAaImAgInS	objevovat
i	i	k9	i
na	na	k7c6	na
jejich	jejich	k3xOp3gNnPc6	jejich
vystoupeních	vystoupení	k1gNnPc6	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
album	album	k1gNnSc1	album
My	my	k3xPp1nPc1	my
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Hádej	hádat	k5eAaImRp2nS	hádat
kdo	kdo	k3yQnSc1	kdo
<g/>
...	...	k?	...
bylo	být	k5eAaImAgNnS	být
nominováno	nominovat	k5eAaBmNgNnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Anděl	Anděla	k1gFnPc2	Anděla
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
vydána	vydán	k2eAgMnSc4d1	vydán
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Nironicem	Nironic	k1gMnSc7	Nironic
deska	deska	k1gFnSc1	deska
The	The	k1gMnSc1	The
Chronicles	Chronicles	k1gMnSc1	Chronicles
of	of	k?	of
Nomad	Nomad	k1gInSc1	Nomad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vydal	vydat	k5eAaPmAgInS	vydat
producentskou	producentský	k2eAgFnSc4d1	producentská
desku	deska	k1gFnSc4	deska
The	The	k1gMnSc1	The
Golden	Goldna	k1gFnPc2	Goldna
Touch	Touch	k1gMnSc1	Touch
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
hosté	host	k1gMnPc1	host
jako	jako	k8xC	jako
Lil	lít	k5eAaImAgMnS	lít
Wayne	Wayn	k1gInSc5	Wayn
<g/>
,	,	kIx,	,
Talib	Talib	k1gInSc1	Talib
Kweli	Kwele	k1gFnSc4	Kwele
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
O.P	O.P	k1gMnSc1	O.P
<g/>
,	,	kIx,	,
Kurupt	Kurupt	k1gMnSc1	Kurupt
<g/>
,	,	kIx,	,
Raekwon	Raekwon	k1gMnSc1	Raekwon
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
produkce	produkce	k1gFnPc1	produkce
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
albech	album	k1gNnPc6	album
všech	všecek	k3xTgMnPc2	všecek
předních	přední	k2eAgMnPc2d1	přední
československých	československý	k2eAgMnPc2d1	československý
a	a	k8xC	a
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
rapperů	rapper	k1gMnPc2	rapper
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmům	film	k1gInPc3	film
Gympl	gympl	k1gInSc4	gympl
a	a	k8xC	a
Ulovit	ulovit	k5eAaPmF	ulovit
miliardáře	miliardář	k1gMnSc4	miliardář
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Anděl	Anděl	k1gMnSc1	Anděl
-	-	kIx~	-
DJ	DJ	kA	DJ
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
-	-	kIx~	-
Dance	Danka	k1gFnSc6	Danka
Music	Musice	k1gFnPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
-	-	kIx~	-
My	my	k3xPp1nPc1	my
<g/>
3	[number]	k4	3
-	-	kIx~	-
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
-	-	kIx~	-
Hudební	hudební	k2eAgFnSc2d1	hudební
ceny	cena	k1gFnSc2	cena
Óčka	Óčkus	k1gMnSc2	Óčkus
-	-	kIx~	-
Time	Tim	k1gMnSc2	Tim
Is	Is	k1gMnSc2	Is
Now	Now	k1gMnSc2	Now
-	-	kIx~	-
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
Hip	hip	k0	hip
Hop	hop	k0	hop
<g/>
/	/	kIx~	/
<g/>
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
N	N	kA	N
<g/>
'	'	kIx"	'
<g/>
B	B	kA	B
album	album	k1gNnSc4	album
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
-	-	kIx~	-
Žebřík	žebřík	k1gInSc1	žebřík
Music	Music	k1gMnSc1	Music
Awards	Awards	k1gInSc1	Awards
-	-	kIx~	-
DJ	DJ	kA	DJ
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
-	-	kIx~	-
Youtube	Youtub	k1gInSc5	Youtub
awards	awards	k1gInSc1	awards
-	-	kIx~	-
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
nejvíc	nejvíc	k6eAd1	nejvíc
odvěratelů	odvěratel	k1gMnPc2	odvěratel
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
hudba	hudba	k1gFnSc1	hudba
2013	[number]	k4	2013
-	-	kIx~	-
Ruka	ruka	k1gFnSc1	ruka
Hore	Hore	k?	Hore
awards	awards	k1gInSc1	awards
-	-	kIx~	-
DJ	DJ	kA	DJ
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
-	-	kIx~	-
Ruka	ruka	k1gFnSc1	ruka
Hore	Hore	k?	Hore
awards	awards	k1gInSc1	awards
-	-	kIx~	-
DJ	DJ	kA	DJ
roku	rok	k1gInSc2	rok
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
-	-	kIx~	-
Ruka	ruka	k1gFnSc1	ruka
Hore	Hore	k?	Hore
awards	awards	k1gInSc1	awards
-	-	kIx~	-
DJ	DJ	kA	DJ
roku	rok	k1gInSc2	rok
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
Indy	Indus	k1gInPc7	Indus
&	&	k?	&
Wich	Wich	k1gMnSc1	Wich
ft	ft	k?	ft
<g/>
.	.	kIx.	.
</s>
<s>
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
<g/>
4	[number]	k4	4
-	-	kIx~	-
Pohyb	pohyb	k1gInSc1	pohyb
Nehybnyho	Nehybnyho	k?	Nehybnyho
/	/	kIx~	/
Indyvidual	Indyvidual	k1gMnSc1	Indyvidual
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
"	"	kIx"	"
vinyl	vinyl	k1gInSc1	vinyl
singl	singl	k1gInSc1	singl
<g/>
)	)	kIx)	)
2001	[number]	k4	2001
Indy	Indus	k1gInPc7	Indus
&	&	k?	&
Wich	Wich	k1gInSc1	Wich
-	-	kIx~	-
Cesta	cesta	k1gFnSc1	cesta
Štrýtu	Štrýt	k1gInSc2	Štrýt
2002	[number]	k4	2002
Indy	Indus	k1gInPc7	Indus
&	&	k?	&
Wich	Wich	k1gInSc1	Wich
-	-	kIx~	-
My	my	k3xPp1nPc1	my
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
2LP	[number]	k4	2LP
vinyl	vinyl	k1gInSc1	vinyl
<g/>
,	,	kIx,	,
1LP	[number]	k4	1LP
instrumental	instrumentat	k5eAaPmAgMnS	instrumentat
vinyl	vinyl	k1gInSc4	vinyl
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
<g />
.	.	kIx.	.
</s>
<s>
Indy	Indus	k1gInPc1	Indus
&	&	k?	&
Wich	Wich	k1gInSc1	Wich
-	-	kIx~	-
Ještě	ještě	k9	ještě
pořád	pořád	k6eAd1	pořád
(	(	kIx(	(
<g/>
vinyl	vinyl	k1gInSc1	vinyl
<g/>
,	,	kIx,	,
cd	cd	kA	cd
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
DJ	DJ	kA	DJ
Wich	Wicha	k1gFnPc2	Wicha
-	-	kIx~	-
Time	Time	k1gFnSc1	Time
Is	Is	k1gMnSc1	Is
Now	Now	k1gMnSc1	Now
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
x	x	k?	x
LP	LP	kA	LP
vinyl	vinyl	k1gInSc1	vinyl
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
-	-	kIx~	-
Work	Work	k1gMnSc1	Work
Affair	Affair	k1gMnSc1	Affair
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
2006	[number]	k4	2006
Indy	Indus	k1gInPc7	Indus
&	&	k?	&
Wich	Wicha	k1gFnPc2	Wicha
-	-	kIx~	-
Hádej	hádat	k5eAaImRp2nS	hádat
Kdo	kdo	k3yInSc1	kdo
<g/>
...	...	k?	...
2007	[number]	k4	2007
<g />
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
presents	presents	k1gInSc1	presents
Nironic	Nironice	k1gFnPc2	Nironice
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Chronicles	Chroniclesa	k1gFnPc2	Chroniclesa
of	of	k?	of
a	a	k8xC	a
Nomad	Nomad	k1gInSc1	Nomad
2008	[number]	k4	2008
DJ	DJ	kA	DJ
Wich	Wicha	k1gFnPc2	Wicha
-	-	kIx~	-
The	The	k1gFnSc1	The
Yearbook	Yearbook	k1gInSc1	Yearbook
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
2008	[number]	k4	2008
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
-	-	kIx~	-
The	The	k1gMnSc1	The
Golden	Goldna	k1gFnPc2	Goldna
Touch	Touch	k1gMnSc1	Touch
2009	[number]	k4	2009
Hi-Def	Hi-Def	k1gInSc1	Hi-Def
&	&	k?	&
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
-	-	kIx~	-
Human	Human	k1gMnSc1	Human
Writes	Writes	k1gMnSc1	Writes
2010	[number]	k4	2010
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
&	&	k?	&
Rasco	Rasco	k1gMnSc1	Rasco
are	ar	k1gInSc5	ar
The	The	k1gMnSc2	The
Untouchables	Untouchablesa	k1gFnPc2	Untouchablesa
-	-	kIx~	-
Al	ala	k1gFnPc2	ala
Capone	Capon	k1gInSc5	Capon
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Vault	Vault	k1gMnSc1	Vault
2011	[number]	k4	2011
<g />
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
&	&	k?	&
Nironic	Nironice	k1gFnPc2	Nironice
-	-	kIx~	-
Nomad	Nomad	k1gInSc1	Nomad
2	[number]	k4	2
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Long	Long	k1gMnSc1	Long
Way	Way	k1gMnSc1	Way
Home	Home	k1gFnSc1	Home
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
&	&	k?	&
Ektor	Ektor	k1gMnSc1	Ektor
-	-	kIx~	-
Tetris	Tetris	k1gFnSc1	Tetris
2013	[number]	k4	2013
DJ	DJ	kA	DJ
Wich	Wicha	k1gFnPc2	Wicha
-	-	kIx~	-
Yearbook	Yearbook	k1gInSc1	Yearbook
2013	[number]	k4	2013
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
2014	[number]	k4	2014
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
&	&	k?	&
LA4	LA4	k1gFnSc1	LA4
-	-	kIx~	-
Panorama	panorama	k1gNnSc1	panorama
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
x	x	k?	x
vinyl	vinyl	k1gInSc1	vinyl
<g/>
)	)	kIx)	)
2016	[number]	k4	2016
DJ	DJ	kA	DJ
Wich	Wicha	k1gFnPc2	Wicha
-	-	kIx~	-
Veni	Ven	k1gMnPc1	Ven
Vidi	vidi	kA	vidi
Wich	Wich	k1gMnSc1	Wich
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
