<s>
BLAST	BLAST	kA	BLAST
(	(	kIx(	(
<g/>
Basic	Basic	kA	Basic
Local	Local	k1gMnSc1	Local
Alignment	Alignment	k1gMnSc1	Alignment
Search	Search	k1gMnSc1	Search
Tool	Tool	k1gMnSc1	Tool
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
algoritmus	algoritmus	k1gInSc1	algoritmus
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
bioinformatice	bioinformatika	k1gFnSc6	bioinformatika
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
srovnávání	srovnávání	k1gNnSc2	srovnávání
primárních	primární	k2eAgFnPc2d1	primární
sekvenčních	sekvenční	k2eAgFnPc2d1	sekvenční
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nukleotidů	nukleotid	k1gInPc2	nukleotid
DNA	DNA	kA	DNA
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
sekvencí	sekvence	k1gFnPc2	sekvence
nebo	nebo	k8xC	nebo
sekvencí	sekvence	k1gFnPc2	sekvence
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
