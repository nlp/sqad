<p>
<s>
BLAST	BLAST	kA	BLAST
(	(	kIx(	(
<g/>
Basic	Basic	kA	Basic
Local	Local	k1gMnSc1	Local
Alignment	Alignment	k1gMnSc1	Alignment
Search	Search	k1gMnSc1	Search
Tool	Tool	k1gMnSc1	Tool
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
algoritmus	algoritmus	k1gInSc1	algoritmus
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
bioinformatice	bioinformatika	k1gFnSc6	bioinformatika
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
srovnávání	srovnávání	k1gNnSc2	srovnávání
primárních	primární	k2eAgFnPc2d1	primární
sekvenčních	sekvenční	k2eAgFnPc2d1	sekvenční
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nukleotidů	nukleotid	k1gInPc2	nukleotid
DNA	DNA	kA	DNA
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
sekvencí	sekvence	k1gFnPc2	sekvence
nebo	nebo	k8xC	nebo
sekvencí	sekvence	k1gFnPc2	sekvence
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
BLAST	BLAST	kA	BLAST
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
srovnání	srovnání	k1gNnSc4	srovnání
dotazované	dotazovaný	k2eAgFnSc2d1	dotazovaná
(	(	kIx(	(
<g/>
zadávané	zadávaný	k2eAgFnSc2d1	zadávaná
<g/>
)	)	kIx)	)
sekvence	sekvence	k1gFnPc1	sekvence
se	s	k7c7	s
sekvencemi	sekvence	k1gFnPc7	sekvence
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
rozpoznání	rozpoznání	k1gNnSc3	rozpoznání
obdobných	obdobný	k2eAgFnPc2d1	obdobná
sekvencí	sekvence	k1gFnPc2	sekvence
nad	nad	k7c7	nad
definovanou	definovaný	k2eAgFnSc7d1	definovaná
hranicí	hranice	k1gFnSc7	hranice
podobnosti	podobnost	k1gFnSc2	podobnost
<g/>
.	.	kIx.	.
</s>
<s>
BLAST	BLAST	kA	BLAST
jako	jako	k8xC	jako
program	program	k1gInSc4	program
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
Stephen	Stephen	k1gInSc4	Stephen
Altschul	Altschula	k1gFnPc2	Altschula
<g/>
,	,	kIx,	,
Warren	Warrna	k1gFnPc2	Warrna
Gish	Gisha	k1gFnPc2	Gisha
<g/>
,	,	kIx,	,
Webb	Webb	k1gMnSc1	Webb
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
Eugene	Eugen	k1gInSc5	Eugen
Myers	Myers	k1gInSc1	Myers
<g/>
,	,	kIx,	,
and	and	k?	and
David	David	k1gMnSc1	David
J.	J.	kA	J.
Lipman	Lipman	k1gMnSc1	Lipman
z	z	k7c2	z
NIH	NIH	kA	NIH
(	(	kIx(	(
<g/>
National	National	k1gMnSc1	National
Institutes	Institutes	k1gMnSc1	Institutes
of	of	k?	of
Health	Health	k1gMnSc1	Health
<g/>
)	)	kIx)	)
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
publikovali	publikovat	k5eAaBmAgMnP	publikovat
v	v	k7c6	v
Journal	Journal	k1gFnSc6	Journal
of	of	k?	of
Molecular	Moleculara	k1gFnPc2	Moleculara
Biology	biolog	k1gMnPc7	biolog
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Algoritmus	algoritmus	k1gInSc1	algoritmus
==	==	k?	==
</s>
</p>
<p>
<s>
BLAST	BLAST	kA	BLAST
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
průběh	průběh	k1gInSc4	průběh
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dotazovanou	dotazovaný	k2eAgFnSc4d1	dotazovaná
sekvenci	sekvence	k1gFnSc4	sekvence
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc4	který
hledáme	hledat	k5eAaImIp1nP	hledat
<g/>
)	)	kIx)	)
a	a	k8xC	a
cílovou	cílový	k2eAgFnSc4d1	cílová
sekvenci	sekvence	k1gFnSc4	sekvence
nebo	nebo	k8xC	nebo
sekvenční	sekvenční	k2eAgFnSc4d1	sekvenční
databázi	databáze	k1gFnSc4	databáze
(	(	kIx(	(
<g/>
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
hledáme	hledat	k5eAaImIp1nP	hledat
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yIgFnSc3	který
hledáme	hledat	k5eAaImIp1nP	hledat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
BLAST	BLAST	kA	BLAST
hledá	hledat	k5eAaImIp3nS	hledat
subsekvence	subsekvence	k1gFnPc4	subsekvence
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnSc3d1	podobná
subsekvenci	subsekvence	k1gFnSc3	subsekvence
dotazované	dotazovaný	k2eAgFnSc3d1	dotazovaná
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
dotazovaná	dotazovaný	k2eAgFnSc1d1	dotazovaná
sekvence	sekvence	k1gFnSc1	sekvence
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
databáze	databáze	k1gFnSc1	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
myšlenek	myšlenka	k1gFnPc2	myšlenka
BLASTu	BLASTus	k1gInSc2	BLASTus
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
statisticky	statisticky	k6eAd1	statisticky
signifikantní	signifikantní	k2eAgInSc1d1	signifikantní
alignment	alignment	k1gInSc1	alignment
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tzv.	tzv.	kA	tzv.
high-scoring	highcoring	k1gInSc1	high-scoring
segment	segment	k1gInSc1	segment
páry	pára	k1gFnSc2	pára
(	(	kIx(	(
<g/>
HSP	HSP	kA	HSP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
alignmenty	alignment	k1gInPc1	alignment
(	(	kIx(	(
<g/>
přiřazení	přiřazení	k1gNnSc1	přiřazení
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
dotazovanou	dotazovaný	k2eAgFnSc7d1	dotazovaná
sekvencí	sekvence	k1gFnSc7	sekvence
a	a	k8xC	a
sekvencemi	sekvence	k1gFnPc7	sekvence
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
pomocí	pomocí	k7c2	pomocí
heuristických	heuristický	k2eAgInPc2d1	heuristický
přístupů	přístup	k1gInPc2	přístup
blízkým	blízký	k2eAgMnPc3d1	blízký
přesnějšímu	přesný	k2eAgInSc3d2	přesnější
algoritmu	algoritmus	k1gInSc6	algoritmus
Smith-Waterman	Smith-Waterman	k1gMnSc1	Smith-Waterman
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
prohledávání	prohledávání	k1gNnSc4	prohledávání
velkých	velký	k2eAgFnPc2d1	velká
genomových	genomový	k2eAgFnPc2d1	genomová
databází	databáze	k1gFnPc2	databáze
příliš	příliš	k6eAd1	příliš
pomalý	pomalý	k2eAgInSc1d1	pomalý
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesátkrát	padesátkrát	k6eAd1	padesátkrát
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
heuristický	heuristický	k2eAgInSc4d1	heuristický
přístup	přístup	k1gInSc4	přístup
dává	dávat	k5eAaImIp3nS	dávat
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
relativně	relativně	k6eAd1	relativně
dobrou	dobrý	k2eAgFnSc7d1	dobrá
přesností	přesnost	k1gFnSc7	přesnost
programům	program	k1gInPc3	program
založených	založený	k2eAgInPc2d1	založený
na	na	k7c4	na
BLAST	BLAST	kA	BLAST
velkou	velký	k2eAgFnSc4d1	velká
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průběh	průběh	k1gInSc1	průběh
algoritmu	algoritmus	k1gInSc2	algoritmus
===	===	k?	===
</s>
</p>
<p>
<s>
BLAST	BLAST	kA	BLAST
provádí	provádět	k5eAaImIp3nS	provádět
sekvenční	sekvenční	k2eAgInSc4d1	sekvenční
alignment	alignment	k1gInSc4	alignment
v	v	k7c6	v
několika	několik	k4yIc6	několik
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
dotazované	dotazovaný	k2eAgFnSc2d1	dotazovaná
sekvence	sekvence	k1gFnSc2	sekvence
odfiltrovány	odfiltrován	k2eAgInPc4d1	odfiltrován
low-complexity	lowomplexit	k1gInPc4	low-complexit
regions	regions	k6eAd1	regions
a	a	k8xC	a
repetice	repetice	k1gFnPc1	repetice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
negativně	negativně	k6eAd1	negativně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
nalezení	nalezení	k1gNnSc4	nalezení
signifikantních	signifikantní	k2eAgFnPc2d1	signifikantní
sekvencí	sekvence	k1gFnPc2	sekvence
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vzniká	vznikat	k5eAaImIp3nS	vznikat
k-písmenný	kísmenný	k2eAgInSc1d1	k-písmenný
seznam	seznam	k1gInSc1	seznam
úseků	úsek	k1gInPc2	úsek
z	z	k7c2	z
dotazované	dotazovaný	k2eAgFnSc2d1	dotazovaná
sekvence	sekvence	k1gFnSc2	sekvence
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
označuje	označovat	k5eAaImIp3nS	označovat
délku	délka	k1gFnSc4	délka
úseku	úsek	k1gInSc2	úsek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
pro	pro	k7c4	pro
proteinové	proteinový	k2eAgFnPc4d1	proteinová
sekvence	sekvence	k1gFnPc4	sekvence
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
DNA	dna	k1gFnSc1	dna
sekvence	sekvence	k1gFnSc1	sekvence
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
úseky	úsek	k1gInPc1	úsek
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
přiřazovány	přiřazován	k2eAgFnPc1d1	přiřazován
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c4	na
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
tedy	tedy	k9	tedy
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
varianty	varianta	k1gFnPc4	varianta
úseků	úsek	k1gInPc2	úsek
dotazované	dotazovaný	k2eAgFnPc1d1	dotazovaná
sekvence	sekvence	k1gFnPc1	sekvence
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
úseky	úsek	k1gInPc1	úsek
následně	následně	k6eAd1	následně
hledáme	hledat	k5eAaImIp1nP	hledat
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
úsek	úsek	k1gInSc1	úsek
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
databázi	databáze	k1gFnSc4	databáze
je	být	k5eAaImIp3nS	být
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
pomocí	pomoc	k1gFnSc7	pomoc
skórující	skórující	k2eAgInSc4d1	skórující
(	(	kIx(	(
<g/>
substituční	substituční	k2eAgInSc4d1	substituční
<g/>
)	)	kIx)	)
matrix	matrix	k1gInSc4	matrix
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
skóre	skóre	k1gNnSc4	skóre
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
body	bod	k1gInPc4	bod
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
kombinace	kombinace	k1gFnPc4	kombinace
v	v	k7c6	v
páru	pár	k1gInSc6	pár
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc4	úsek
hodnocen	hodnotit	k5eAaImNgMnS	hodnotit
jako	jako	k9	jako
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
přesáhne	přesáhnout	k5eAaPmIp3nS	přesáhnout
předem	předem	k6eAd1	předem
definovanou	definovaný	k2eAgFnSc4d1	definovaná
prahovou	prahový	k2eAgFnSc4d1	prahová
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
threshold	threshold	k6eAd1	threshold
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
odpovídající	odpovídající	k2eAgInPc1d1	odpovídající
úseky	úsek	k1gInPc1	úsek
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
high-scoring	highcoring	k1gInSc1	high-scoring
páry	pára	k1gFnSc2	pára
(	(	kIx(	(
<g/>
HSP	HSP	kA	HSP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
kroku	krok	k1gInSc6	krok
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
alignment	alignment	k1gInSc4	alignment
za	za	k7c4	za
užití	užití	k1gNnSc4	užití
stále	stále	k6eAd1	stále
stejné	stejné	k1gNnSc4	stejné
skórující	skórující	k2eAgInSc1d1	skórující
matrix	matrix	k1gInSc1	matrix
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
až	až	k6eAd1	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
skóre	skóre	k1gNnSc1	skóre
přestane	přestat	k5eAaPmIp3nS	přestat
dosahovat	dosahovat	k5eAaImF	dosahovat
prahové	prahový	k2eAgFnPc4d1	prahová
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dokud	dokud	k6eAd1	dokud
celkový	celkový	k2eAgInSc1d1	celkový
součet	součet	k1gInSc1	součet
skóre	skóre	k1gNnSc6	skóre
high-scoring	highcoring	k1gInSc1	high-scoring
párů	pár	k1gInPc2	pár
HSP	HSP	kA	HSP
nezačne	začít	k5eNaPmIp3nS	začít
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
následných	následný	k2eAgNnPc2d1	následné
vylepšení	vylepšení	k1gNnPc2	vylepšení
BLASTu	BLASTus	k1gInSc2	BLASTus
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možnost	možnost	k1gFnSc4	možnost
alignmentu	alignment	k1gInSc2	alignment
zahrnujícího	zahrnující	k2eAgInSc2d1	zahrnující
mezery	mezera	k1gFnPc4	mezera
(	(	kIx(	(
<g/>
gaps	gaps	k6eAd1	gaps
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc1	úsek
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
skóre	skóre	k1gNnSc7	skóre
oboustranně	oboustranně	k6eAd1	oboustranně
rozšiřován	rozšiřován	k2eAgMnSc1d1	rozšiřován
pomocí	pomocí	k7c2	pomocí
metod	metoda	k1gFnPc2	metoda
dynamického	dynamický	k2eAgNnSc2d1	dynamické
programování	programování	k1gNnSc2	programování
</s>
</p>
<p>
<s>
===	===	k?	===
BLAST	BLAST	kA	BLAST
vs	vs	k?	vs
FASTA	FASTA	kA	FASTA
===	===	k?	===
</s>
</p>
<p>
<s>
BLAST	BLAST	kA	BLAST
i	i	k8xC	i
FASTA	FASTA	kA	FASTA
algoritmy	algoritmus	k1gInPc1	algoritmus
v	v	k7c6	v
bioinformatice	bioinformatika	k1gFnSc6	bioinformatika
používají	používat	k5eAaImIp3nP	používat
metodu	metoda	k1gFnSc4	metoda
heuristického	heuristický	k2eAgNnSc2d1	heuristické
hledání	hledání	k1gNnSc2	hledání
pro	pro	k7c4	pro
rychlý	rychlý	k2eAgInSc4d1	rychlý
párový	párový	k2eAgInSc4d1	párový
sekvenční	sekvenční	k2eAgInSc4d1	sekvenční
alignment	alignment	k1gInSc4	alignment
<g/>
,	,	kIx,	,
hledající	hledající	k2eAgInPc4d1	hledající
krátké	krátký	k2eAgInPc4d1	krátký
úseky	úsek	k1gInPc4	úsek
identických	identický	k2eAgInPc2d1	identický
nebo	nebo	k8xC	nebo
vysoce	vysoce	k6eAd1	vysoce
podobných	podobný	k2eAgInPc2d1	podobný
záznamů	záznam	k1gInPc2	záznam
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
sekvencích	sekvence	k1gFnPc6	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zaručeno	zaručen	k2eAgNnSc1d1	zaručeno
nalezení	nalezení	k1gNnSc1	nalezení
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
(	(	kIx(	(
<g/>
optimálního	optimální	k2eAgInSc2d1	optimální
<g/>
)	)	kIx)	)
alignmentu	alignment	k1gInSc2	alignment
nebo	nebo	k8xC	nebo
skutečných	skutečný	k2eAgMnPc2d1	skutečný
homologů	homolog	k1gMnPc2	homolog
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
50	[number]	k4	50
<g/>
×	×	k?	×
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
×	×	k?	×
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
dynamické	dynamický	k2eAgNnSc1d1	dynamické
programování	programování	k1gNnSc1	programování
<g/>
.	.	kIx.	.
</s>
<s>
BLAST	BLAST	kA	BLAST
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
hledání	hledání	k1gNnSc3	hledání
pouze	pouze	k6eAd1	pouze
vysoce	vysoce	k6eAd1	vysoce
signifikantních	signifikantní	k2eAgInPc2d1	signifikantní
záznamů	záznam	k1gInPc2	záznam
v	v	k7c6	v
sekvencích	sekvence	k1gFnPc6	sekvence
časově	časově	k6eAd1	časově
efektivnější	efektivní	k2eAgFnSc1d2	efektivnější
než	než	k8xS	než
FASTA	FASTA	kA	FASTA
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
citlivost	citlivost	k1gFnSc1	citlivost
je	být	k5eAaImIp3nS	být
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Program	program	k1gInSc1	program
==	==	k?	==
</s>
</p>
<p>
<s>
BLAST	BLAST	kA	BLAST
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spustit	spustit	k5eAaPmF	spustit
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
a	a	k8xC	a
sice	sice	k8xC	sice
<g/>
,	,	kIx,	,
stáhnout	stáhnout	k5eAaPmF	stáhnout
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
blastall	blastall	k1gInSc1	blastall
<g/>
"	"	kIx"	"
a	a	k8xC	a
spustit	spustit	k5eAaPmF	spustit
jako	jako	k9	jako
nástroj	nástroj	k1gInSc4	nástroj
příkazového	příkazový	k2eAgInSc2d1	příkazový
řádku	řádek	k1gInSc2	řádek
nebo	nebo	k8xC	nebo
používat	používat	k5eAaImF	používat
přístupem	přístup	k1gInSc7	přístup
přes	přes	k7c4	přes
web	web	k1gInSc4	web
<g/>
.	.	kIx.	.
</s>
<s>
Webový	webový	k2eAgInSc1d1	webový
server	server	k1gInSc1	server
BLASTu	BLASTus	k1gInSc2	BLASTus
provozovaný	provozovaný	k2eAgInSc1d1	provozovaný
NCBI	NCBI	kA	NCBI
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
komukoliv	kdokoliv	k3yInSc3	kdokoliv
s	s	k7c7	s
webovým	webový	k2eAgInSc7d1	webový
prohlížečem	prohlížeč	k1gInSc7	prohlížeč
prohledávat	prohledávat	k5eAaImF	prohledávat
databáze	databáze	k1gFnPc4	databáze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
většinu	většina	k1gFnSc4	většina
osekvenovaných	osekvenovaný	k2eAgInPc2d1	osekvenovaný
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
BLAST	BLAST	kA	BLAST
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
jako	jako	k9	jako
open-source	openourka	k1gFnSc3	open-sourka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dává	dávat	k5eAaImIp3nS	dávat
možnost	možnost	k1gFnSc4	možnost
modifikace	modifikace	k1gFnSc2	modifikace
programového	programový	k2eAgInSc2d1	programový
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc4	několik
BLASTových	BLASTová	k1gFnPc2	BLASTová
vylepšení	vylepšení	k1gNnPc2	vylepšení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BLAST	BLAST	kA	BLAST
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
cíli	cíl	k1gInSc6	cíl
hledání	hledání	k1gNnSc2	hledání
a	a	k8xC	a
vstupních	vstupní	k2eAgNnPc6d1	vstupní
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
verze	verze	k1gFnPc1	verze
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
zadávaných	zadávaný	k2eAgNnPc6d1	zadávané
vstupních	vstupní	k2eAgNnPc6d1	vstupní
datech	datum	k1gNnPc6	datum
<g/>
,	,	kIx,	,
prohledávaných	prohledávaný	k2eAgFnPc6d1	prohledávaná
databázích	databáze	k1gFnPc6	databáze
a	a	k8xC	a
ve	v	k7c6	v
vlastním	vlastní	k2eAgNnSc6d1	vlastní
srovnávání	srovnávání	k1gNnSc1	srovnávání
<g/>
.	.	kIx.	.
<g/>
Nukleotid-nukleotid	Nukleotidukleotid	k1gInSc1	Nukleotid-nukleotid
BLAST	BLAST	kA	BLAST
(	(	kIx(	(
<g/>
blastn	blastn	k1gMnSc1	blastn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
zadané	zadaný	k2eAgFnSc3d1	zadaná
dotazované	dotazovaný	k2eAgFnSc3d1	dotazovaná
sekvenci	sekvence	k1gFnSc3	sekvence
DNA	dno	k1gNnSc2	dno
nejvíce	nejvíce	k6eAd1	nejvíce
podobné	podobný	k2eAgFnPc1d1	podobná
sekvence	sekvence	k1gFnPc1	sekvence
DNA	DNA	kA	DNA
z	z	k7c2	z
databáze	databáze	k1gFnSc2	databáze
DNA	dno	k1gNnSc2	dno
vybrané	vybraný	k2eAgNnSc1d1	vybrané
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protein-protein	Proteinrotein	k1gMnSc1	Protein-protein
BLAST	BLAST	kA	BLAST
(	(	kIx(	(
<g/>
blastp	blastp	k1gMnSc1	blastp
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
zadané	zadaný	k2eAgFnSc3d1	zadaná
dotazované	dotazovaný	k2eAgFnSc3d1	dotazovaná
proteinové	proteinový	k2eAgFnSc3d1	proteinová
sekvenci	sekvence	k1gFnSc3	sekvence
nejvíce	hodně	k6eAd3	hodně
podobné	podobný	k2eAgFnPc4d1	podobná
proteinové	proteinový	k2eAgFnPc4d1	proteinová
sekence	sekence	k1gFnPc4	sekence
z	z	k7c2	z
databáze	databáze	k1gFnSc2	databáze
proteinů	protein	k1gInPc2	protein
vybrané	vybraný	k2eAgNnSc1d1	vybrané
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Position-Specific	Position-Specific	k1gMnSc1	Position-Specific
Iterative	Iterativ	k1gInSc5	Iterativ
BLAST	BLAST	kA	BLAST
(	(	kIx(	(
<g/>
PSI-BLAST	PSI-BLAST	k1gMnSc1	PSI-BLAST
<g/>
,	,	kIx,	,
blastpgp	blastpgp	k1gMnSc1	blastpgp
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
používaný	používaný	k2eAgInSc1d1	používaný
k	k	k7c3	k
hledání	hledání	k1gNnSc3	hledání
vzdáleně	vzdáleně	k6eAd1	vzdáleně
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
je	být	k5eAaImIp3nS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
seznam	seznam	k1gInSc1	seznam
blízce	blízce	k6eAd1	blízce
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
zkombinovány	zkombinován	k2eAgFnPc1d1	zkombinována
jako	jako	k8xC	jako
obecná	obecný	k2eAgFnSc1d1	obecná
"	"	kIx"	"
<g/>
profilová	profilový	k2eAgFnSc1d1	profilová
<g/>
"	"	kIx"	"
sekvence	sekvence	k1gFnSc1	sekvence
shrnující	shrnující	k2eAgNnSc1d1	shrnující
signifikantní	signifikantní	k2eAgInPc1d1	signifikantní
znaky	znak	k1gInPc1	znak
těchto	tento	k3xDgFnPc2	tento
sekvencí	sekvence	k1gFnPc2	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sekvence	sekvence	k1gFnSc1	sekvence
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
srovnávána	srovnávat	k5eAaImNgFnS	srovnávat
s	s	k7c7	s
databází	databáze	k1gFnSc7	databáze
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
proteiny	protein	k1gInPc1	protein
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
použity	použít	k5eAaPmNgFnP	použít
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
"	"	kIx"	"
<g/>
profilové	profilový	k2eAgFnPc1d1	profilová
<g/>
"	"	kIx"	"
sekvence	sekvence	k1gFnPc1	sekvence
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
proteinů	protein	k1gInPc2	protein
má	mít	k5eAaImIp3nS	mít
PSI-BLAST	PSI-BLAST	k1gMnSc1	PSI-BLAST
vyšší	vysoký	k2eAgFnSc4d2	vyšší
citlivost	citlivost	k1gFnSc4	citlivost
zachycení	zachycení	k1gNnSc3	zachycení
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
najít	najít	k5eAaPmF	najít
i	i	k9	i
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
evoluční	evoluční	k2eAgInPc4d1	evoluční
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nucleotide	Nucleotid	k1gMnSc5	Nucleotid
6	[number]	k4	6
<g/>
-frame	ram	k1gInSc5	-fram
translation-protein	translationrotein	k2eAgInSc4d1	translation-protein
(	(	kIx(	(
<g/>
blastx	blastx	k1gInSc4	blastx
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
6	[number]	k4	6
<g/>
-rámcové	ámcový	k2eAgInPc4d1	-rámcový
abstraktní	abstraktní	k2eAgInPc4d1	abstraktní
translační	translační	k2eAgInPc4d1	translační
produkty	produkt	k1gInPc4	produkt
dotazované	dotazovaný	k2eAgFnSc2d1	dotazovaná
sekvence	sekvence	k1gFnSc2	sekvence
nukleotidů	nukleotid	k1gInPc2	nukleotid
(	(	kIx(	(
<g/>
obě	dva	k4xCgNnPc4	dva
vlákna	vlákno	k1gNnPc4	vlákno
<g/>
)	)	kIx)	)
s	s	k7c7	s
proteinovou	proteinový	k2eAgFnSc7d1	proteinová
sekvenční	sekvenční	k2eAgFnSc7d1	sekvenční
databází	databáze	k1gFnSc7	databáze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nucleotide	Nucleotid	k1gMnSc5	Nucleotid
6	[number]	k4	6
<g/>
-frame	ram	k1gInSc5	-fram
translation-nucleotide	translationucleotid	k1gInSc5	translation-nucleotid
6	[number]	k4	6
<g/>
-frame	ram	k1gInSc5	-fram
translation	translation	k1gInSc1	translation
(	(	kIx(	(
<g/>
tblastx	tblastx	k1gInSc1	tblastx
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejpomalejší	pomalý	k2eAgMnSc1d3	nejpomalejší
z	z	k7c2	z
programů	program	k1gInPc2	program
BLASTu	BLASTus	k1gInSc2	BLASTus
překládá	překládat	k5eAaImIp3nS	překládat
dotazovanou	dotazovaný	k2eAgFnSc4d1	dotazovaná
nukleotidovou	nukleotidový	k2eAgFnSc4d1	nukleotidová
sekvenci	sekvence	k1gFnSc4	sekvence
do	do	k7c2	do
všech	všecek	k3xTgMnPc2	všecek
šesti	šest	k4xCc2	šest
možných	možný	k2eAgInPc2d1	možný
čtecích	čtecí	k2eAgInPc2d1	čtecí
rámců	rámec	k1gInPc2	rámec
a	a	k8xC	a
srovnává	srovnávat	k5eAaImIp3nS	srovnávat
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
6	[number]	k4	6
rámcovou	rámcový	k2eAgFnSc7d1	rámcová
translací	translace	k1gFnSc7	translace
v	v	k7c6	v
nukleotidové	nukleotidový	k2eAgFnSc6d1	nukleotidová
sekvenční	sekvenční	k2eAgFnSc6d1	sekvenční
databázi	databáze	k1gFnSc6	databáze
<g/>
.	.	kIx.	.
</s>
<s>
TBLASTX	TBLASTX	kA	TBLASTX
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
nalézt	nalézt	k5eAaBmF	nalézt
velmi	velmi	k6eAd1	velmi
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
nukleotidovými	nukleotidový	k2eAgFnPc7d1	nukleotidová
sekvencemi	sekvence	k1gFnPc7	sekvence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protein-nucleotide	Proteinucleotid	k1gMnSc5	Protein-nucleotid
6	[number]	k4	6
<g/>
-frame	ram	k1gInSc5	-fram
translation	translation	k1gInSc1	translation
(	(	kIx(	(
<g/>
tblastn	tblastn	k1gInSc1	tblastn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
porovnává	porovnávat	k5eAaImIp3nS	porovnávat
dotazovanou	dotazovaný	k2eAgFnSc4d1	dotazovaná
sekvenci	sekvence	k1gFnSc4	sekvence
proteinu	protein	k1gInSc2	protein
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
šesti	šest	k4xCc7	šest
čtecími	čtecí	k2eAgInPc7d1	čtecí
rámci	rámec	k1gInPc7	rámec
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
sekvence	sekvence	k1gFnPc1	sekvence
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
dotazovaných	dotazovaný	k2eAgFnPc2d1	dotazovaná
sekvencí	sekvence	k1gFnPc2	sekvence
–	–	k?	–
Large	Larg	k1gInSc2	Larg
numbers	numbersa	k1gFnPc2	numbersa
of	of	k?	of
query	quer	k1gInPc1	quer
sequences	sequences	k1gInSc1	sequences
(	(	kIx(	(
<g/>
megablast	megablast	k1gInSc1	megablast
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Při	při	k7c6	při
porovnávání	porovnávání	k1gNnSc6	porovnávání
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
dotazovaných	dotazovaný	k2eAgFnPc2d1	dotazovaná
sekvencí	sekvence	k1gFnPc2	sekvence
pomocí	pomocí	k7c2	pomocí
BLASTu	BLASTus	k1gInSc2	BLASTus
přes	přes	k7c4	přes
příkazový	příkazový	k2eAgInSc4d1	příkazový
řádek	řádek	k1gInSc4	řádek
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
megablast	megablast	k1gInSc4	megablast
<g/>
"	"	kIx"	"
mnohem	mnohem	k6eAd1	mnohem
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
než	než	k8xS	než
několikanásobné	několikanásobný	k2eAgNnSc4d1	několikanásobné
spuštění	spuštění	k1gNnSc4	spuštění
BLASTu	BLASTus	k1gInSc2	BLASTus
<g/>
.	.	kIx.	.
</s>
<s>
Megablast	Megablast	k1gInSc1	Megablast
zřetězí	zřetězit	k5eAaPmIp3nS	zřetězit
několik	několik	k4yIc4	několik
vstupních	vstupní	k2eAgFnPc2d1	vstupní
sekvencí	sekvence	k1gFnPc2	sekvence
dohromady	dohromady	k6eAd1	dohromady
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
prohledáváním	prohledávání	k1gNnSc7	prohledávání
databáze	databáze	k1gFnSc2	databáze
a	a	k8xC	a
následně	následně	k6eAd1	následně
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
výsledky	výsledek	k1gInPc1	výsledek
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
alignmentů	alignment	k1gInPc2	alignment
a	a	k8xC	a
statistických	statistický	k2eAgFnPc2d1	statistická
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Užití	užití	k1gNnSc3	užití
BLASTu	BLASTum	k1gNnSc3	BLASTum
==	==	k?	==
</s>
</p>
<p>
<s>
BLAST	BLAST	kA	BLAST
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
procesů	proces	k1gInPc2	proces
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
identifikace	identifikace	k1gFnPc1	identifikace
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
lokalizace	lokalizace	k1gFnSc2	lokalizace
domén	doména	k1gFnPc2	doména
<g/>
,	,	kIx,	,
stanovování	stanovování	k1gNnSc4	stanovování
fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
a	a	k8xC	a
DNA	dno	k1gNnSc2	dno
mapování	mapování	k1gNnSc2	mapování
či	či	k8xC	či
porovnávání	porovnávání	k1gNnSc2	porovnávání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Identifikace	identifikace	k1gFnSc1	identifikace
druhů	druh	k1gInPc2	druh
</s>
</p>
<p>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
BLASTu	BLASTus	k1gInSc2	BLASTus
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
identifikace	identifikace	k1gFnSc1	identifikace
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
identity	identita	k1gFnSc2	identita
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
nalezení	nalezení	k1gNnSc4	nalezení
druhů	druh	k1gInPc2	druh
homologních	homologní	k1gMnPc2	homologní
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
například	například	k6eAd1	například
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
DNA	DNA	kA	DNA
neznámých	známý	k2eNgInPc2d1	neznámý
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lokalizace	lokalizace	k1gFnSc1	lokalizace
domén	doména	k1gFnPc2	doména
</s>
</p>
<p>
<s>
Možnost	možnost	k1gFnSc4	možnost
lokalizace	lokalizace	k1gFnSc2	lokalizace
známých	známý	k2eAgFnPc2d1	známá
domén	doména	k1gFnPc2	doména
v	v	k7c6	v
sekvenci	sekvence	k1gFnSc6	sekvence
proteinu	protein	k1gInSc2	protein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stanovení	stanovení	k1gNnSc1	stanovení
fylogeneze	fylogeneze	k1gFnSc2	fylogeneze
</s>
</p>
<p>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
BLAST	BLAST	kA	BLAST
web-stránky	webtránka	k1gFnSc2	web-stránka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sestavit	sestavit	k5eAaPmF	sestavit
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
BLASTu	BLASTus	k1gInSc2	BLASTus
fylogenetický	fylogenetický	k2eAgInSc1d1	fylogenetický
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
fylogeneze	fylogeneze	k1gFnSc1	fylogeneze
založená	založený	k2eAgFnSc1d1	založená
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
BLAST	BLAST	kA	BLAST
samotném	samotný	k2eAgNnSc6d1	samotné
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
spolehlivá	spolehlivý	k2eAgFnSc1d1	spolehlivá
než	než	k8xS	než
další	další	k2eAgFnSc1d1	další
výpočetní	výpočetní	k2eAgFnPc4d1	výpočetní
fylogenetické	fylogenetický	k2eAgFnPc4d1	fylogenetická
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
prvotní	prvotní	k2eAgFnSc4d1	prvotní
analýzu	analýza	k1gFnSc4	analýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DNA	dna	k1gFnSc1	dna
mapování	mapování	k1gNnSc2	mapování
</s>
</p>
<p>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
se	s	k7c7	s
známými	známý	k2eAgInPc7d1	známý
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dotazujeme	dotazovat	k5eAaImIp1nP	dotazovat
sekvenci	sekvence	k1gFnSc4	sekvence
genu	gen	k1gInSc2	gen
s	s	k7c7	s
neznámou	známý	k2eNgFnSc7d1	neznámá
lokalizací	lokalizace	k1gFnSc7	lokalizace
může	moct	k5eAaImIp3nS	moct
BLAST	BLAST	kA	BLAST
porovnat	porovnat	k5eAaPmF	porovnat
pozice	pozice	k1gFnPc4	pozice
dotazovaných	dotazovaný	k2eAgFnPc2d1	dotazovaná
sekvencí	sekvence	k1gFnPc2	sekvence
na	na	k7c6	na
chromozomu	chromozom	k1gInSc6	chromozom
s	s	k7c7	s
relevantními	relevantní	k2eAgFnPc7d1	relevantní
sekvencemi	sekvence	k1gFnPc7	sekvence
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
(	(	kIx(	(
<g/>
databázích	databáze	k1gFnPc6	databáze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Srovnávání	srovnávání	k1gNnSc1	srovnávání
</s>
</p>
<p>
<s>
Při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
geny	gen	k1gInPc7	gen
může	moct	k5eAaImIp3nS	moct
BLAST	BLAST	kA	BLAST
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
společné	společný	k2eAgInPc4d1	společný
geny	gen	k1gInPc4	gen
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
druzích	druh	k1gInPc6	druh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
pro	pro	k7c4	pro
mapování	mapování	k1gNnSc4	mapování
anotací	anotace	k1gFnPc2	anotace
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
organismu	organismus	k1gInSc2	organismus
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
BLAST	BLAST	kA	BLAST
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
