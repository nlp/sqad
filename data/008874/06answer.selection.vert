<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
korejštině	korejština	k1gFnSc6	korejština
oficiálně	oficiálně	k6eAd1	oficiálně
nazývaná	nazývaný	k2eAgNnPc1d1	nazývané
Tchägukki	Tchägukki	k1gNnPc1	Tchägukki
(	(	kIx(	(
<g/>
태	태	k?	태
/	/	kIx~	/
太	太	k?	太
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tchäguk	tchäguk	k1gInSc1	tchäguk
(	(	kIx(	(
<g/>
태	태	k?	태
<g/>
)	)	kIx)	)
t.j.	t.j.	k?	t.j.
červeno-modrý	červenoodrý	k2eAgInSc4d1	červeno-modrý
symbol	symbol	k1gInSc4	symbol
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
ležaté	ležatý	k2eAgFnSc2d1	ležatá
ho	on	k3xPp3gNnSc2	on
písmene	písmeno	k1gNnSc2	písmeno
S	s	k7c7	s
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
dole	dole	k6eAd1	dole
a	a	k8xC	a
červená	červenat	k5eAaImIp3nS	červenat
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
