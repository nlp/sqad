<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
korejštině	korejština	k1gFnSc6	korejština
oficiálně	oficiálně	k6eAd1	oficiálně
nazývaná	nazývaný	k2eAgNnPc1d1	nazývané
Tchägukki	Tchägukki	k1gNnPc1	Tchägukki
(	(	kIx(	(
<g/>
태	태	k?	태
/	/	kIx~	/
太	太	k?	太
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
tchäguk	tchäguk	k1gInSc1	tchäguk
(	(	kIx(	(
<g/>
태	태	k?	태
<g/>
)	)	kIx)	)
t.j.	t.j.	k?	t.j.
červeno-modrý	červenoodrý	k2eAgInSc4d1	červeno-modrý
symbol	symbol	k1gInSc4	symbol
jin	jin	k?	jin
a	a	k8xC	a
jang	jang	k1gMnSc1	jang
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
ležaté	ležatý	k2eAgFnSc2d1	ležatá
ho	on	k3xPp3gNnSc2	on
písmene	písmeno	k1gNnSc2	písmeno
S	s	k7c7	s
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
dole	dole	k6eAd1	dole
a	a	k8xC	a
červená	červenat	k5eAaImIp3nS	červenat
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
rohů	roh	k1gInPc2	roh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jeden	jeden	k4xCgInSc1	jeden
trigram	trigram	k1gInSc1	trigram
<g/>
,	,	kIx,	,
kombinace	kombinace	k1gFnSc1	kombinace
tří	tři	k4xCgFnPc2	tři
plných	plný	k2eAgFnPc2d1	plná
a	a	k8xC	a
přerušovaných	přerušovaný	k2eAgFnPc2d1	přerušovaná
černých	černá	k1gFnPc2	černá
<g/>
,	,	kIx,	,
rovnoběžných	rovnoběžný	k2eAgFnPc2d1	rovnoběžná
čar	čára	k1gFnPc2	čára
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Symbolika	symbolika	k1gFnSc1	symbolika
==	==	k?	==
</s>
</p>
<p>
<s>
Symbol	symbol	k1gInSc1	symbol
tchäguk	tchäguk	k6eAd1	tchäguk
odráží	odrážet	k5eAaImIp3nS	odrážet
staletou	staletý	k2eAgFnSc4d1	staletá
tradici	tradice	k1gFnSc4	tradice
harmonie	harmonie	k1gFnSc2	harmonie
dvou	dva	k4xCgInPc2	dva
protikladů	protiklad	k1gInPc2	protiklad
jin	jin	k?	jin
–	–	k?	–
jang	jang	k1gInSc1	jang
(	(	kIx(	(
<g/>
světlo	světlo	k1gNnSc1	světlo
–	–	k?	–
tma	tma	k1gFnSc1	tma
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc1d1	mužský
aspekt	aspekt	k1gInSc1	aspekt
–	–	k?	–
ženský	ženský	k2eAgInSc4d1	ženský
aspekt	aspekt	k1gInSc4	aspekt
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
–	–	k?	–
vnější	vnější	k2eAgNnSc1d1	vnější
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
taoismu	taoismus	k1gInSc2	taoismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
přítomný	přítomný	k2eAgInSc1d1	přítomný
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
myšlenkových	myšlenkový	k2eAgInPc6d1	myšlenkový
směrech	směr	k1gInPc6	směr
přítomných	přítomný	k1gMnPc2	přítomný
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
–	–	k?	–
v	v	k7c6	v
konfucianismu	konfucianismus	k1gInSc6	konfucianismus
<g/>
,	,	kIx,	,
buddhismu	buddhismus	k1gInSc6	buddhismus
<g/>
,	,	kIx,	,
šamanismu	šamanismus	k1gInSc6	šamanismus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bílé	bílý	k2eAgNnSc1d1	bílé
pozadí	pozadí	k1gNnSc1	pozadí
vlajky	vlajka	k1gFnSc2	vlajka
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
čistotu	čistota	k1gFnSc4	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
touhu	touha	k1gFnSc4	touha
korejského	korejský	k2eAgInSc2d1	korejský
národu	národ	k1gInSc2	národ
po	po	k7c6	po
míru	mír	k1gInSc6	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korejské	korejský	k2eAgInPc1d1	korejský
trigramy	trigram	k1gInPc1	trigram
(	(	kIx(	(
<g/>
kwae	kwae	k1gFnSc1	kwae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
kultovní	kultovní	k2eAgInPc1d1	kultovní
znaky	znak	k1gInPc1	znak
se	s	k7c7	s
světonázorovým	světonázorový	k2eAgInSc7d1	světonázorový
obsahem	obsah	k1gInSc7	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
harmonii	harmonie	k1gFnSc4	harmonie
a	a	k8xC	a
jednotu	jednota	k1gFnSc4	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ideál	ideál	k1gInSc1	ideál
národní	národní	k2eAgFnSc2d1	národní
prosperity	prosperita	k1gFnSc2	prosperita
a	a	k8xC	a
univerzální	univerzální	k2eAgFnSc2d1	univerzální
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
všichni	všechen	k3xTgMnPc1	všechen
Korejci	Korejec	k1gMnPc1	Korejec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
existovaly	existovat	k5eAaImAgInP	existovat
na	na	k7c6	na
Korejském	korejský	k2eAgInSc6d1	korejský
poloostrově	poloostrov	k1gInSc6	poloostrov
první	první	k4xOgInPc4	první
státy	stát	k1gInPc4	stát
-	-	kIx~	-
např.	např.	kA	např.
Tři	tři	k4xCgNnPc4	tři
království	království	k1gNnPc4	království
Koreje	Korea	k1gFnSc2	Korea
(	(	kIx(	(
<g/>
Kogurjo	Kogurjo	k1gNnSc1	Kogurjo
<g/>
,	,	kIx,	,
Pekče	Pekče	k1gNnSc1	Pekče
a	a	k8xC	a
Silla	Silla	k1gFnSc1	Silla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
668	[number]	k4	668
sjednoceny	sjednotit	k5eAaPmNgFnP	sjednotit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
Sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
Silly	Silla	k1gFnSc2	Silla
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
království	království	k1gNnSc1	království
roku	rok	k1gInSc2	rok
935	[number]	k4	935
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
stát	stát	k1gInSc1	stát
Korjo	Korjo	k6eAd1	Korjo
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
evropský	evropský	k2eAgInSc1d1	evropský
název	název	k1gInSc1	název
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
země	zem	k1gFnPc4	zem
po	po	k7c6	po
nájezdech	nájezd	k1gInPc6	nájezd
mongolů	mongol	k1gMnPc2	mongol
vazalským	vazalský	k2eAgInSc7d1	vazalský
státem	stát	k1gInSc7	stát
dynastie	dynastie	k1gFnSc2	dynastie
Jüan	jüan	k1gInSc1	jüan
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1392	[number]	k4	1392
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
posledního	poslední	k2eAgMnSc4d1	poslední
krále	král	k1gMnSc4	král
Korja	Korja	k1gMnSc1	Korja
dynastie	dynastie	k1gFnSc2	dynastie
Čoson	Čoson	k1gMnSc1	Čoson
(	(	kIx(	(
<g/>
Ri	Ri	k1gMnSc1	Ri
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1393	[number]	k4	1393
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Velký	velký	k2eAgInSc1d1	velký
čosonský	čosonský	k2eAgInSc1d1	čosonský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1636	[number]	k4	1636
<g/>
–	–	k?	–
<g/>
1637	[number]	k4	1637
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
na	na	k7c4	na
poloostrov	poloostrov	k1gInSc4	poloostrov
Mandžuové	Mandžu	k1gMnPc1	Mandžu
a	a	k8xC	a
Korea	Korea	k1gFnSc1	Korea
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
čínským	čínský	k2eAgMnSc7d1	čínský
(	(	kIx(	(
<g/>
Říše	říše	k1gFnSc1	říše
Čching	Čching	k1gInSc1	Čching
<g/>
)	)	kIx)	)
vazalem	vazal	k1gMnSc7	vazal
<g/>
.	.	kIx.	.
<g/>
Královská	královský	k2eAgFnSc1d1	královská
vlajka	vlajka	k1gFnSc1	vlajka
dynastie	dynastie	k1gFnSc2	dynastie
Čoson	Čosona	k1gFnPc2	Čosona
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
tmavě	tmavě	k6eAd1	tmavě
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
s	s	k7c7	s
úzkým	úzký	k2eAgInSc7d1	úzký
<g/>
,	,	kIx,	,
žlutým	žlutý	k2eAgInSc7d1	žlutý
lemem	lem	k1gInSc7	lem
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
další	další	k2eAgInSc1d1	další
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
,	,	kIx,	,
pilovitý	pilovitý	k2eAgInSc1d1	pilovitý
lem	lem	k1gInSc1	lem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k6eAd1	uprostřed
byl	být	k5eAaImAgInS	být
starý	starý	k2eAgInSc1d1	starý
žluto-bílý	žlutoílý	k2eAgInSc1d1	žluto-bílý
symbol	symbol	k1gInSc1	symbol
kosmu	kosmos	k1gInSc2	kosmos
um-yang	umanga	k1gFnPc2	um-yanga
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
yin-yang	yinang	k1gInSc1	yin-yang
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obklopený	obklopený	k2eAgInSc1d1	obklopený
osmi	osm	k4xCc2	osm
trigramy	trigram	k1gInPc7	trigram
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
kosmu	kosmos	k1gInSc2	kosmos
vyjadřoval	vyjadřovat	k5eAaImAgInS	vyjadřovat
dualismus	dualismus	k1gInSc4	dualismus
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
harmonií	harmonie	k1gFnSc7	harmonie
<g/>
,	,	kIx,	,
rovnováhu	rovnováha	k1gFnSc4	rovnováha
mezi	mezi	k7c4	mezi
protiklady	protiklad	k1gInPc4	protiklad
a	a	k8xC	a
neustálý	neustálý	k2eAgInSc4d1	neustálý
pohyb	pohyb	k1gInSc4	pohyb
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
izolovala	izolovat	k5eAaBmAgFnS	izolovat
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
izolace	izolace	k1gFnSc1	izolace
prolomena	prolomit	k5eAaPmNgFnS	prolomit
evropskými	evropský	k2eAgInPc7d1	evropský
koloniálními	koloniální	k2eAgInPc7d1	koloniální
státy	stát	k1gInPc7	stát
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
neužívala	užívat	k5eNaImAgFnS	užívat
(	(	kIx(	(
<g/>
neměla	mít	k5eNaImAgFnS	mít
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
)	)	kIx)	)
žádnou	žádný	k3yNgFnSc4	žádný
národní	národní	k2eAgFnSc4d1	národní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
mezi	mezi	k7c7	mezi
Koreou	Korea	k1gFnSc7	Korea
a	a	k8xC	a
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Japonsko	Japonsko	k1gNnSc1	Japonsko
přitom	přitom	k6eAd1	přitom
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
také	také	k9	také
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
(	(	kIx(	(
<g/>
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
neužívalo	užívat	k5eNaImAgNnS	užívat
žádnou	žádný	k3yNgFnSc4	žádný
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
jednání	jednání	k1gNnSc1	jednání
se	se	k3xPyFc4	se
zahraničím	zahraničit	k5eAaPmIp1nS	zahraničit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
potřeba	potřeba	k6eAd1	potřeba
vlastní	vlastní	k2eAgFnPc4d1	vlastní
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
,	,	kIx,	,
nastala	nastat	k5eAaPmAgFnS	nastat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1880	[number]	k4	1880
a	a	k8xC	a
1882	[number]	k4	1882
(	(	kIx(	(
<g/>
Shufeldtova	Shufeldtův	k2eAgFnSc1d1	Shufeldtův
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
korejské	korejský	k2eAgFnSc2d1	Korejská
vlajky	vlajka	k1gFnSc2	vlajka
jsou	být	k5eAaImIp3nP	být
nejasné	jasný	k2eNgInPc1d1	nejasný
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
variant	varianta	k1gFnPc2	varianta
vzniku	vznik	k1gInSc2	vznik
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
duben	duben	k1gInSc1	duben
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
čínsko-korejská	čínskoorejský	k2eAgFnSc1d1	čínsko-korejský
schůzka	schůzka	k1gFnSc1	schůzka
<g/>
,	,	kIx,	,
čínský	čínský	k2eAgMnSc1d1	čínský
delegát	delegát	k1gMnSc1	delegát
Ma	Ma	k1gMnSc1	Ma
Čien-Čung	Čien-Čung	k1gMnSc1	Čien-Čung
doporučil	doporučit	k5eAaPmAgMnS	doporučit
Korejcům	Korejec	k1gMnPc3	Korejec
červeno	červeno	k1gNnSc1	červeno
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
modrý	modrý	k2eAgMnSc1d1	modrý
(	(	kIx(	(
<g/>
poddaní	poddaný	k1gMnPc1	poddaný
<g/>
)	)	kIx)	)
emblém	emblém	k1gInSc1	emblém
a	a	k8xC	a
kolem	kolo	k1gNnSc7	kolo
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
osm	osm	k4xCc4	osm
trojic	trojice	k1gFnPc2	trojice
čar	čára	k1gFnPc2	čára
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
tehdejšímu	tehdejší	k2eAgInSc3d1	tehdejší
počtu	počet	k1gInSc3	počet
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
trigramů	trigram	k1gInPc2	trigram
byl	být	k5eAaImAgInS	být
zredukován	zredukovat	k5eAaPmNgInS	zredukovat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1882	[number]	k4	1882
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vlajku	vlajka	k1gFnSc4	vlajka
ukázal	ukázat	k5eAaPmAgInS	ukázat
(	(	kIx(	(
<g/>
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
)	)	kIx)	)
korejský	korejský	k2eAgMnSc1d1	korejský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
Pak	pak	k6eAd1	pak
Jong-Hjo	Jong-Hjo	k6eAd1	Jong-Hjo
britskému	britský	k2eAgMnSc3d1	britský
kapitánovi	kapitán	k1gMnSc3	kapitán
Jamesovi	James	k1gMnSc3	James
a	a	k8xC	a
tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
zdála	zdát	k5eAaImAgFnS	zdát
příliš	příliš	k6eAd1	příliš
složitá	složitý	k2eAgFnSc1d1	složitá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
září	září	k1gNnSc1	září
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
loď	loď	k1gFnSc1	loď
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
Pak	pak	k8xC	pak
Jong-Hjo	Jong-Hjo	k1gMnSc1	Jong-Hjo
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nutné	nutný	k2eAgNnSc4d1	nutné
uzavřít	uzavřít	k5eAaPmF	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
pod	pod	k7c7	pod
korejským	korejský	k2eAgInSc7d1	korejský
symbolem	symbol	k1gInSc7	symbol
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
začal	začít	k5eAaPmAgMnS	začít
vlajku	vlajka	k1gFnSc4	vlajka
užívat	užívat	k5eAaImF	užívat
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
oznámil	oznámit	k5eAaPmAgMnS	oznámit
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
králi	král	k1gMnSc3	král
Kodžongovi	Kodžong	k1gMnSc3	Kodžong
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1883	[number]	k4	1883
za	za	k7c4	za
oficiální	oficiální	k2eAgFnSc4d1	oficiální
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
zavedena	zaveden	k2eAgFnSc1d1	zavedena
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
<g/>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
bílým	bílý	k2eAgInSc7d1	bílý
listem	list	k1gInSc7	list
<g/>
,	,	kIx,	,
s	s	k7c7	s
uprostřed	uprostřed	k6eAd1	uprostřed
umístěným	umístěný	k2eAgInSc7d1	umístěný
kruhovým	kruhový	k2eAgInSc7d1	kruhový
<g/>
,	,	kIx,	,
červeno-modrým	červenoodrý	k2eAgInSc7d1	červeno-modrý
piktogramem	piktogram	k1gInSc7	piktogram
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
emblém	emblém	k1gInSc4	emblém
obklopovaly	obklopovat	k5eAaImAgFnP	obklopovat
čtyři	čtyři	k4xCgFnPc4	čtyři
trojice	trojice	k1gFnPc4	trojice
modrých	modré	k1gNnPc2	modré
trigramů	trigram	k1gInPc2	trigram
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
byl	být	k5eAaImAgInS	být
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
zdroje	zdroj	k1gInPc1	zdroj
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
vlajku	vlajka	k1gFnSc4	vlajka
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
pozicemi	pozice	k1gFnPc7	pozice
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
trigramů	trigram	k1gInPc2	trigram
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
emblémem	emblém	k1gInSc7	emblém
a	a	k8xC	a
obrácenými	obrácený	k2eAgFnPc7d1	obrácená
barvami	barva	k1gFnPc7	barva
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
jinými	jiný	k2eAgFnPc7d1	jiná
pozicemi	pozice	k1gFnPc7	pozice
trigramů	trigram	k1gInPc2	trigram
<g/>
.	.	kIx.	.
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
vlajky	vlajka	k1gFnSc2	vlajka
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
čistotu	čistota	k1gFnSc4	čistota
myšlenek	myšlenka	k1gFnPc2	myšlenka
a	a	k8xC	a
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
korejskou	korejský	k2eAgFnSc4d1	Korejská
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
lidskost	lidskost	k1gFnSc4	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Piktogram	piktogram	k1gInSc1	piktogram
symbolizoval	symbolizovat	k5eAaImAgInS	symbolizovat
základní	základní	k2eAgNnSc4d1	základní
krédo	krédo	k1gNnSc4	krédo
buddhismu	buddhismus	k1gInSc2	buddhismus
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
jednoty	jednota	k1gFnSc2	jednota
protikladů	protiklad	k1gInPc2	protiklad
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
kruh	kruh	k1gInSc1	kruh
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
aeguk	aeguk	k1gMnSc1	aeguk
<g/>
)	)	kIx)	)
představoval	představovat	k5eAaImAgInS	představovat
absolutno	absolutno	k1gNnSc4	absolutno
<g/>
,	,	kIx,	,
červená	červená	k1gFnSc1	červená
(	(	kIx(	(
<g/>
yang	yang	k1gInSc1	yang
<g/>
)	)	kIx)	)
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
um	um	k1gInSc1	um
<g/>
)	)	kIx)	)
část	část	k1gFnSc1	část
představovala	představovat	k5eAaImAgFnS	představovat
filozofické	filozofický	k2eAgFnPc4d1	filozofická
kategorie	kategorie	k1gFnPc4	kategorie
<g/>
:	:	kIx,	:
duch-hmota	duchmot	k1gMnSc2	duch-hmot
<g/>
,	,	kIx,	,
dobro-zlo	dobronout	k5eAaPmAgNnS	dobro-znout
<g/>
,	,	kIx,	,
den-noc	denoc	k1gFnSc1	den-noc
<g/>
,	,	kIx,	,
muž-žena	muž-žen	k2eAgFnSc1d1	muž-žena
nebo	nebo	k8xC	nebo
klad-zápor	kladápor	k1gInSc1	klad-zápor
<g/>
.	.	kIx.	.
</s>
<s>
Trigramy	trigram	k1gInPc1	trigram
(	(	kIx(	(
<g/>
kwae	kwa	k1gFnPc1	kwa
<g/>
)	)	kIx)	)
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
nebe	nebe	k1gNnSc4	nebe
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
roh	roh	k1gInSc1	roh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oheň	oheň	k1gInSc1	oheň
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgInSc1d1	dolní
roh	roh	k1gInSc1	roh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vodu	voda	k1gFnSc4	voda
(	(	kIx(	(
<g/>
horní	horní	k2eAgInSc1d1	horní
cíp	cíp	k1gInSc1	cíp
<g/>
)	)	kIx)	)
a	a	k8xC	a
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgInSc1d1	dolní
cíp	cíp	k1gInSc1	cíp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
světové	světový	k2eAgFnPc4d1	světová
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
apod.	apod.	kA	apod.
<g/>
Po	po	k7c6	po
čínsko-japonské	čínskoaponský	k2eAgFnSc6d1	čínsko-japonská
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
Koreu	Korea	k1gFnSc4	Korea
a	a	k8xC	a
podpisu	podpis	k1gInSc3	podpis
Šimonosecké	Šimonosecký	k2eAgFnSc2d1	Šimonosecký
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
čínská	čínský	k2eAgFnSc1d1	čínská
nadvláda	nadvláda	k1gFnSc1	nadvláda
nad	nad	k7c7	nad
Koreou	Korea	k1gFnSc7	Korea
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1897	[number]	k4	1897
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
korejské	korejský	k2eAgNnSc1d1	korejské
císařství	císařství	k1gNnSc1	císařství
Velký	velký	k2eAgMnSc1d1	velký
Han	Hana	k1gFnPc2	Hana
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
absenci	absence	k1gFnSc4	absence
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
však	však	k9	však
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
vexilologických	vexilologický	k2eAgFnPc6d1	vexilologická
publikacích	publikace	k1gFnPc6	publikace
<g/>
)	)	kIx)	)
v	v	k7c6	v
detailech	detail	k1gInPc6	detail
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1900	[number]	k4	1900
vydala	vydat	k5eAaPmAgFnS	vydat
korejská	korejský	k2eAgFnSc1d1	Korejská
vláda	vláda	k1gFnSc1	vláda
směrnici	směrnice	k1gFnSc4	směrnice
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkový	vlajkový	k2eAgInSc1d1	vlajkový
list	list	k1gInSc1	list
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
rozměry	rozměr	k1gInPc4	rozměr
61	[number]	k4	61
×	×	k?	×
55	[number]	k4	55
cm	cm	kA	cm
a	a	k8xC	a
průměr	průměr	k1gInSc1	průměr
emblému	emblém	k1gInSc2	emblém
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rusko-japonské	ruskoaponský	k2eAgFnSc6d1	rusko-japonská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1905	[number]	k4	1905
a	a	k8xC	a
1907	[number]	k4	1907
uzavřeny	uzavřen	k2eAgFnPc4d1	uzavřena
smlouvy	smlouva	k1gFnPc4	smlouva
o	o	k7c6	o
japonském	japonský	k2eAgInSc6d1	japonský
protektorátu	protektorát	k1gInSc6	protektorát
Koreje	Korea	k1gFnSc2	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
generálního	generální	k2eAgMnSc4d1	generální
rezidenta	rezident	k1gMnSc4	rezident
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
japonský	japonský	k2eAgMnSc1d1	japonský
představitel	představitel	k1gMnSc1	představitel
v	v	k7c6	v
protektorátu	protektorát	k1gInSc6	protektorát
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1906	[number]	k4	1906
zavedena	zaveden	k2eAgFnSc1d1	zavedena
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
světle	světle	k6eAd1	světle
modrým	modrý	k2eAgInSc7d1	modrý
listem	list	k1gInSc7	list
s	s	k7c7	s
japonskou	japonský	k2eAgFnSc7d1	japonská
vlajkou	vlajka	k1gFnSc7	vlajka
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Světle	světle	k6eAd1	světle
modrá	modrý	k2eAgFnSc1d1	modrá
vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
zdroji	zdroj	k1gInSc3	zdroj
je	být	k5eAaImIp3nS	být
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
tmavší	tmavý	k2eAgInSc1d2	tmavší
odstín	odstín	k1gInSc1	odstín
<g/>
)	)	kIx)	)
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
krásu	krása	k1gFnSc4	krása
a	a	k8xC	a
filantropii	filantropie	k1gFnSc4	filantropie
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
prakticky	prakticky	k6eAd1	prakticky
nahradila	nahradit	k5eAaPmAgFnS	nahradit
korejskou	korejský	k2eAgFnSc4d1	Korejská
vlajku	vlajka	k1gFnSc4	vlajka
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
zrušení	zrušení	k1gNnSc6	zrušení
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1910	[number]	k4	1910
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgNnSc4d1	oficiální
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
anexe	anexe	k1gFnSc2	anexe
Koreje	Korea	k1gFnSc2	Korea
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyvěšovala	vyvěšovat	k5eAaImAgFnS	vyvěšovat
pouze	pouze	k6eAd1	pouze
vlajka	vlajka	k1gFnSc1	vlajka
Japonského	japonský	k2eAgNnSc2d1	Japonské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Korejská	korejský	k2eAgFnSc1d1	Korejská
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
Japonci	Japonec	k1gMnPc1	Japonec
zakázána	zakázán	k2eAgMnSc4d1	zakázán
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
exilová	exilový	k2eAgFnSc1d1	exilová
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
užívala	užívat	k5eAaImAgFnS	užívat
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
podobu	podoba	k1gFnSc4	podoba
určoval	určovat	k5eAaImAgInS	určovat
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
listu	list	k1gInSc2	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
stran	strana	k1gFnPc2	strana
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
byl	být	k5eAaImAgInS	být
emblém	emblém	k1gInSc1	emblém
(	(	kIx(	(
<g/>
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
obráceného	obrácený	k2eAgNnSc2d1	obrácené
písmene	písmeno	k1gNnSc2	písmeno
S	s	k7c7	s
<g/>
)	)	kIx)	)
dělen	dělit	k5eAaImNgInS	dělit
vertikálně	vertikálně	k6eAd1	vertikálně
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
červená	červený	k2eAgFnSc1d1	červená
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
blíže	blízce	k6eAd2	blízce
žerdi	žerď	k1gFnSc3	žerď
a	a	k8xC	a
trigramy	trigram	k1gInPc4	trigram
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
černé	černý	k2eAgFnPc1d1	černá
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
postavení	postavení	k1gNnSc6	postavení
jako	jako	k8xS	jako
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
konci	konec	k1gInSc6	konec
II	II	kA	II
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
oznámil	oznámit	k5eAaPmAgMnS	oznámit
japonský	japonský	k2eAgMnSc1d1	japonský
císař	císař	k1gMnSc1	císař
Hirohito	Hirohit	k2eAgNnSc4d1	Hirohito
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
)	)	kIx)	)
obsadila	obsadit	k5eAaPmAgFnS	obsadit
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Korejského	korejský	k2eAgInSc2d1	korejský
poloostrova	poloostrov	k1gInSc2	poloostrov
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
armádou	armáda	k1gFnSc7	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
poloostrova	poloostrov	k1gInSc2	poloostrov
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
užívat	užívat	k5eAaImF	užívat
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
vlajka	vlajka	k1gFnSc1	vlajka
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1948	[number]	k4	1948
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
vyvěšovány	vyvěšován	k2eAgFnPc4d1	vyvěšována
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgNnSc4d1	vycházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
japonskou	japonský	k2eAgFnSc7d1	japonská
okupací	okupace	k1gFnSc7	okupace
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
ale	ale	k8xC	ale
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
varianty	varianta	k1gFnPc1	varianta
(	(	kIx(	(
<g/>
horizontální	horizontální	k2eAgNnSc1d1	horizontální
dělení	dělení	k1gNnSc1	dělení
emblému	emblém	k1gInSc2	emblém
-	-	kIx~	-
horní	horní	k2eAgFnSc1d1	horní
červená	červená	k1gFnSc1	červená
nebo	nebo	k8xC	nebo
s	s	k7c7	s
opačným	opačný	k2eAgNnSc7d1	opačné
postavením	postavení	k1gNnSc7	postavení
trigramů	trigram	k1gInPc2	trigram
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
a	a	k8xC	a
cípu	cíp	k1gInSc6	cíp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
proto	proto	k6eAd1	proto
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
kodifikování	kodifikování	k1gNnSc4	kodifikování
způsobu	způsob	k1gInSc2	způsob
výroby	výroba	k1gFnSc2	výroba
národní	národní	k2eAgFnSc2d1	národní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
vlajka	vlajka	k1gFnSc1	vlajka
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
v	v	k7c4	v
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
<g/>
)	)	kIx)	)
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1984	[number]	k4	1984
byly	být	k5eAaImAgFnP	být
mírně	mírně	k6eAd1	mírně
změněny	změněn	k2eAgFnPc4d1	změněna
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
mezi	mezi	k7c7	mezi
čárami	čára	k1gFnPc7	čára
trigramů	trigram	k1gInPc2	trigram
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1997	[number]	k4	1997
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgInP	upravit
barevné	barevný	k2eAgInPc1d1	barevný
odstíny	odstín	k1gInPc1	odstín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zákony	zákon	k1gInPc1	zákon
==	==	k?	==
</s>
</p>
<p>
<s>
Zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgInSc7d1	poslední
právním	právní	k2eAgInSc7d1	právní
předpisem	předpis	k1gInSc7	předpis
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
korejské	korejský	k2eAgFnSc2d1	Korejská
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
–	–	k?	–
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vlajce	vlajka	k1gFnSc6	vlajka
Korejské	korejský	k2eAgFnSc2d1	Korejská
republiky	republika	k1gFnSc2	republika
č.	č.	k?	č.
8272	[number]	k4	8272
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
–	–	k?	–
novelizace	novelizace	k1gFnSc1	novelizace
č.	č.	k?	č.
10741	[number]	k4	10741
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
–	–	k?	–
novelizace	novelizace	k1gFnSc1	novelizace
č.	č.	k?	č.
12342	[number]	k4	12342
<g/>
Prováděcí	prováděcí	k2eAgFnSc1d1	prováděcí
vyhláška	vyhláška	k1gFnSc1	vyhláška
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
zákonu	zákon	k1gInSc3	zákon
(	(	kIx(	(
<g/>
Prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
vyhláška	vyhláška	k1gFnSc1	vyhláška
č.	č.	k?	č.
24425	[number]	k4	24425
ze	z	k7c2	z
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
definuje	definovat	k5eAaBmIp3nS	definovat
přesně	přesně	k6eAd1	přesně
odstíny	odstín	k1gInPc4	odstín
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
Pantone	Panton	k1gInSc5	Panton
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
186	[number]	k4	186
<g/>
c	c	k0	c
a	a	k8xC	a
modrá	modrat	k5eAaImIp3nS	modrat
294	[number]	k4	294
<g/>
c.	c.	k?	c.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc1	vlajka
jihokorejských	jihokorejský	k2eAgFnPc2d1	jihokorejská
provincií	provincie	k1gFnPc2	provincie
==	==	k?	==
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
speciální	speciální	k2eAgNnSc4d1	speciální
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc4d1	hlavní
<g/>
)	)	kIx)	)
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
Soul	Soul	k1gInSc1	Soul
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
speciální	speciální	k2eAgNnSc1d1	speciální
autonomní	autonomní	k2eAgNnSc1d1	autonomní
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Sedžong	Sedžong	k1gInSc1	Sedžong
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šesti	šest	k4xCc2	šest
metropolitních	metropolitní	k2eAgNnPc2d1	metropolitní
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
osmi	osm	k4xCc2	osm
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
speciální	speciální	k2eAgFnSc2d1	speciální
autonomní	autonomní	k2eAgFnSc2d1	autonomní
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
jednotky	jednotka	k1gFnPc1	jednotka
užívají	užívat	k5eAaImIp3nP	užívat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
galerie	galerie	k1gFnSc1	galerie
vlajek	vlajka	k1gFnPc2	vlajka
není	být	k5eNaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
</s>
</p>
<p>
<s>
Hymna	hymna	k1gFnSc1	hymna
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Koreje	Korea	k1gFnSc2	Korea
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
</s>
</p>
<p>
<s>
Severokorejská	severokorejský	k2eAgFnSc1d1	severokorejská
vlajka	vlajka	k1gFnSc1	vlajka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlajka	vlajka	k1gFnSc1	vlajka
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
