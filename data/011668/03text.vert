<p>
<s>
Lykurgův	Lykurgův	k2eAgInSc1d1	Lykurgův
pohár	pohár	k1gInSc1	pohár
je	být	k5eAaImIp3nS	být
skleněný	skleněný	k2eAgInSc4d1	skleněný
pohár	pohár	k1gInSc4	pohár
pocházející	pocházející	k2eAgInSc4d1	pocházející
ze	z	k7c2	z
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
uložený	uložený	k2eAgInSc1d1	uložený
v	v	k7c6	v
Britském	britský	k2eAgNnSc6d1	Britské
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
známým	známý	k2eAgInSc7d1	známý
příkladem	příklad	k1gInSc7	příklad
použití	použití	k1gNnSc4	použití
dichroického	dichroický	k2eAgNnSc2d1	dichroický
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nádoba	nádoba	k1gFnSc1	nádoba
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
typu	typ	k1gInSc3	typ
síťových	síťový	k2eAgInPc2d1	síťový
pohárů	pohár	k1gInPc2	pohár
(	(	kIx(	(
<g/>
vas	vas	k?	vas
diatretum	diatretum	k1gNnSc4	diatretum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdobených	zdobený	k2eAgFnPc2d1	zdobená
na	na	k7c6	na
povrchu	povrch	k1gInSc3	povrch
vybrušovaným	vybrušovaný	k2eAgInSc7d1	vybrušovaný
plastickým	plastický	k2eAgInSc7d1	plastický
vzorem	vzor	k1gInSc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
unikátní	unikátní	k2eAgFnSc3d1	unikátní
použitím	použití	k1gNnSc7	použití
figurálního	figurální	k2eAgInSc2d1	figurální
motivu	motiv	k1gInSc2	motiv
<g/>
,	,	kIx,	,
znázorňujícího	znázorňující	k2eAgMnSc2d1	znázorňující
thráckého	thrácký	k2eAgMnSc2d1	thrácký
krále	král	k1gMnSc2	král
Lykurga	Lykurg	k1gMnSc2	Lykurg
potrestaného	potrestaný	k2eAgMnSc2d1	potrestaný
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
potlačoval	potlačovat	k5eAaImAgInS	potlačovat
kult	kult	k1gInSc1	kult
boha	bůh	k1gMnSc2	bůh
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
<g/>
:	:	kIx,	:
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
populární	populární	k2eAgNnSc1d1	populární
označení	označení	k1gNnSc1	označení
poháru	pohár	k1gInSc2	pohár
i	i	k9	i
předpoklad	předpoklad	k1gInSc1	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
k	k	k7c3	k
pití	pití	k1gNnSc3	pití
vína	víno	k1gNnSc2	víno
při	při	k7c6	při
bakchanáliích	bakchanálie	k1gFnPc6	bakchanálie
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
poháru	pohár	k1gInSc2	pohár
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
roku	rok	k1gInSc3	rok
1845	[number]	k4	1845
jako	jako	k8xC	jako
majetek	majetek	k1gInSc4	majetek
rodiny	rodina	k1gFnSc2	rodina
Rothschildů	Rothschild	k1gMnPc2	Rothschild
<g/>
.	.	kIx.	.
</s>
<s>
Kunsthistorik	kunsthistorik	k1gMnSc1	kunsthistorik
Dyfri	Dyfr	k1gFnSc2	Dyfr
Williams	Williams	k1gInSc1	Williams
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lykurgův	Lykurgův	k2eAgInSc1d1	Lykurgův
pohár	pohár	k1gInSc1	pohár
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nádoba	nádoba	k1gFnSc1	nádoba
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
16,5	[number]	k4	16,5
cm	cm	kA	cm
a	a	k8xC	a
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc1	průměr
13,2	[number]	k4	13,2
cm	cm	kA	cm
<g/>
;	;	kIx,	;
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
bronzové	bronzový	k2eAgFnSc6d1	bronzová
noze	noha	k1gFnSc6	noha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodná	pozoruhodný	k2eAgFnSc1d1	pozoruhodná
je	být	k5eAaImIp3nS	být
schopností	schopnost	k1gFnSc7	schopnost
měnit	měnit	k5eAaImF	měnit
barvu	barva	k1gFnSc4	barva
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
denním	denní	k2eAgNnSc6d1	denní
světle	světlo	k1gNnSc6	světlo
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
při	při	k7c6	při
osvětlení	osvětlení	k1gNnSc6	osvětlení
pod	pod	k7c7	pod
vhodným	vhodný	k2eAgInSc7d1	vhodný
úhlem	úhel	k1gInSc7	úhel
se	se	k3xPyFc4	se
rozzáří	rozzářit	k5eAaPmIp3nS	rozzářit
jasně	jasně	k6eAd1	jasně
červeným	červený	k2eAgNnSc7d1	červené
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
odstín	odstín	k1gInSc1	odstín
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
také	také	k9	také
podle	podle	k7c2	podle
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
pohár	pohár	k1gInSc1	pohár
naplněn	naplnit	k5eAaPmNgInS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc4	efekt
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgMnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
litého	litý	k2eAgNnSc2d1	Lité
skla	sklo	k1gNnSc2	sklo
bylo	být	k5eAaImAgNnS	být
přidáno	přidán	k2eAgNnSc4d1	Přidáno
práškové	práškový	k2eAgNnSc4d1	práškové
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgFnPc1d1	kovová
nanokrystaly	nanokrystat	k5eAaPmAgFnP	nanokrystat
o	o	k7c6	o
rozměru	rozměr	k1gInSc6	rozměr
okolo	okolo	k7c2	okolo
70	[number]	k4	70
nanometrů	nanometr	k1gInPc2	nanometr
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
koloidní	koloidní	k2eAgFnSc4d1	koloidní
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
měnit	měnit	k5eAaImF	měnit
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
světla	světlo	k1gNnSc2	světlo
pomocí	pomocí	k7c2	pomocí
kvazičástic	kvazičástice	k1gFnPc2	kvazičástice
plazmonů	plazmon	k1gMnPc2	plazmon
<g/>
.	.	kIx.	.
</s>
<s>
Lukurgův	Lukurgův	k2eAgInSc1d1	Lukurgův
pohár	pohár	k1gInSc1	pohár
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
literatuře	literatura	k1gFnSc6	literatura
používán	používat	k5eAaImNgInS	používat
jako	jako	k9	jako
důkaz	důkaz	k1gInSc4	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
již	již	k6eAd1	již
znali	znát	k5eAaImAgMnP	znát
nanotechnologie	nanotechnologie	k1gFnSc2	nanotechnologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lykurgův	Lykurgův	k2eAgInSc4d1	Lykurgův
pohár	pohár	k1gInSc4	pohár
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.britishmuseum.org/research/collection_online/collection_object_details.aspx?objectId=61219&	[url]	k?	http://www.britishmuseum.org/research/collection_online/collection_object_details.aspx?objectId=61219&
</s>
</p>
<p>
<s>
https://www.prirodovedci.cz/chemik/clanky/po-stopach-davnych-nanotechnologu	[url]	k1gMnSc3	https://www.prirodovedci.cz/chemik/clanky/po-stopach-davnych-nanotechnologu
</s>
</p>
<p>
<s>
http://www.osel.cz/7090-lykurgos-hraje-barvami-diky-kvazicasticim-zvanym-plazmony.html	[url]	k1gMnSc1	http://www.osel.cz/7090-lykurgos-hraje-barvami-diky-kvazicasticim-zvanym-plazmony.html
</s>
</p>
