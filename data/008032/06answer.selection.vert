<s>
Meningitida	meningitida	k1gFnSc1	meningitida
čili	čili	k8xC	čili
zánět	zánět	k1gInSc1	zánět
mozkových	mozkový	k2eAgFnPc2d1	mozková
blan	blána	k1gFnPc2	blána
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc1	zánět
ochranných	ochranný	k2eAgFnPc2d1	ochranná
membrán	membrána	k1gFnPc2	membrána
pokrývajících	pokrývající	k2eAgInPc2d1	pokrývající
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
míchu	mícha	k1gFnSc4	mícha
<g/>
,	,	kIx,	,
známých	známý	k1gMnPc2	známý
pod	pod	k7c7	pod
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
mozkomíšní	mozkomíšní	k2eAgFnSc2d1	mozkomíšní
pleny	plena	k1gFnSc2	plena
<g/>
.	.	kIx.	.
</s>
